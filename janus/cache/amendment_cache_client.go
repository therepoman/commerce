package cache

import (
	"encoding/json"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
)

const (
	amendmentCacheVersion = "1"
	amendmentCacheService = "dynamoDB"
)

// Amendment metadata struct
type Amendment struct {
	UserID         string
	AmendmentType  string
	AcceptanceDate time.Time
}

// A cache entry with optional amendment.  If empty, the row was not found in Dynamo.
type AmendmentCacheEntry struct {
	Amendment *Amendment
}

type AmendmentCacheClient struct {
	cacheClient IKeyValueCache
	cachedAPI   string
	ttl         time.Duration
}

// IAmendmentCacheClient interface for the blacklist DAO Cache
type IAmendmentCacheClient interface {
	Get(userID string, amendmentType string) (isCacheHit bool, amendment *Amendment)
	Put(amendment *Amendment)
	PutAmendmentNotFound(userID string, amendmentType string)
}

// NewAmendmentCacheClient constructs a AmendmentCacheClient with the passed in kvCacheClient
func NewAmendmentCacheClient(kvCacheClient IKeyValueCache, api string, ttl time.Duration) IAmendmentCacheClient {
	return &AmendmentCacheClient{
		cacheClient: kvCacheClient,
		cachedAPI:   api,
		ttl:         ttl,
	}
}

func (cacheClient *AmendmentCacheClient) Get(userID string, amendmentType string) (bool, *Amendment) {
	cacheKey := buildCacheKey(userID, amendmentType)
	cacheRequest := NewCacheRequest(dynamoDB, cacheClient.cachedAPI, cacheKey, amendmentCacheVersion)
	jsonString := cacheClient.cacheClient.Get(cacheRequest)
	if jsonString == "" {
		return false, nil
	}
	var amendmentCacheEntry *AmendmentCacheEntry
	if err := json.Unmarshal([]byte(jsonString), &amendmentCacheEntry); err != nil {
		log.Errorf("Failed to unmarshal for jsonString: %+v cacheRequest: %+v error: %+v", jsonString, cacheRequest, err)
		return false, nil
	}
	return true, amendmentCacheEntry.Amendment
}

func (cacheClient *AmendmentCacheClient) Put(amendment *Amendment) {
	cacheKey := buildCacheKey(amendment.UserID, amendment.AmendmentType)
	cacheRequest := NewCacheRequest(dynamoDB, cacheClient.cachedAPI, cacheKey, amendmentCacheVersion)

	marshalledValue, err := json.Marshal(&AmendmentCacheEntry{Amendment: amendment})
	if err != nil {
		log.Errorf("Failed to marshal amendment cache entry: %+v cacheRequest: %+v error: %+v", amendment, cacheRequest, err)
		return
	}
	cacheClient.cacheClient.Set(cacheRequest, string(marshalledValue), cacheClient.ttl)
}

func (cacheClient *AmendmentCacheClient) PutAmendmentNotFound(userID string, amendmentType string) {
	cacheKey := buildCacheKey(userID, amendmentType)
	cacheRequest := NewCacheRequest(dynamoDB, cacheClient.cachedAPI, cacheKey, amendmentCacheVersion)

	marshalledValue, err := json.Marshal(&AmendmentCacheEntry{})
	if err != nil {
		log.Errorf("Failed to marshal emtpy amendment cache entry. cacheRequest: %+v error: %+v", cacheRequest, err)
		return
	}
	cacheClient.cacheClient.Set(cacheRequest, string(marshalledValue), cacheClient.ttl)
}

func buildCacheKey(userID string, amendmentType string) string {
	keyParts := []string{userID, amendmentType}
	return strings.Join(keyParts, KeySeparator)
}
