package cache_test

import (
	"errors"
	"testing"

	"encoding/json"

	"reflect"

	"time"

	"code.justin.tv/commerce/janus/cache"
	"code.justin.tv/commerce/janus/mocks"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestMerchandiseLocalizationDAOCache_GetLocalizations(t *testing.T) {
	mockedCacheClient := new(mocks.IKeyValueCache)
	merchLocalizationCache := cache.NewMerchandiseLocalizationDAOCache(mockedCacheClient, "testapi", time.Second)
	Convey("When calling getLocalizations", t, func() {
		Convey("When cache client returns error", func() {
			mockedCacheClient.On("Get", mock.Anything, mock.Anything).Return("", errors.New("Cache client get call failed!")).Once()
			val := merchLocalizationCache.GetLocalizations([]string{"asin"}, "locale")

			So(val, ShouldBeNil)
		})
		Convey("When cache client returns an empty result", func() {
			mockedCacheClient.On("Get", mock.Anything, mock.Anything).Return("", nil).Once()
			val := merchLocalizationCache.GetLocalizations([]string{"asin"}, "locale")

			So(val, ShouldBeNil)
		})
		Convey("When the returned cache value can not be unmarshalled", func() {
			mockedCacheClient.On("Get", mock.Anything, mock.Anything).Return("this string is not a json adg product", nil).Once()
			val := merchLocalizationCache.GetLocalizations([]string{"asin"}, "locale")

			So(val, ShouldBeNil)
		})
		Convey("When cache client returns json value (happy case)", func() {
			expectedResponse := []cache.MerchandiseLocalizationCacheValue{}
			cacheValue := cache.MerchandiseLocalizationCacheValue{
				Asin:  "Asin",
				Title: "Title",
			}
			expectedResponse = append(expectedResponse, cacheValue)
			marshalledBytes, jsonErr := json.Marshal(expectedResponse)
			if jsonErr != nil {
				//This will point us to what went wrong better than t.FailNow()
				So(true, ShouldBeFalse)
			}
			jsonString := string(marshalledBytes)

			mockedCacheClient.On("Get", mock.Anything, mock.Anything).Return(jsonString, nil).Once()
			cacheResponse := merchLocalizationCache.GetLocalizations([]string{"Asin"}, "locale")

			So(reflect.DeepEqual(expectedResponse, cacheResponse), ShouldBeTrue)
		})
	})
}

func TestMerchandiseLocalizationDAOCache_SetLocalizations(t *testing.T) {
	mockedCacheClient := new(mocks.IKeyValueCache)
	merchLocalizationCache := cache.NewMerchandiseLocalizationDAOCache(mockedCacheClient, "testapi", time.Second)
	Convey("When calling setLocalizations", t, func() {
		Convey("When cache client returns error", func() {
			mockedCacheClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("Cache client set failed")).Once()
			merchLocalizationCache.SetLocalizations([]string{"asin"}, "locale", []cache.MerchandiseLocalizationCacheValue{})
		})
		Convey("When cache client call succeeds", func() {
			mockedCacheClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()
			merchLocalizationCache.SetLocalizations([]string{"asin"}, "locale", []cache.MerchandiseLocalizationCacheValue{})
		})
	})
}
