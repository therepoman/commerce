package cache_test

import (
	"testing"

	"fmt"
	"time"

	"code.justin.tv/commerce/janus/cache"
	"code.justin.tv/commerce/janus/mocks"
	"github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestNewCacheRequest(t *testing.T) {
	Convey("When calling NewMetricInfo", t, func() {
		var testRequest cache.CacheRequest
		service := "SERVICE"
		api := "API"
		key := "KEY"
		version := "1"
		Convey("When calling GetMetricString", func() {
			Convey("When empty metric", func() {
				testRequest = cache.NewCacheRequest(service, api, "", "")
				metricString := testRequest.GetMetricString("")
				So(metricString, ShouldEqual, "SERVICE.API.")
			})
			Convey("When empty service", func() {
				testRequest = cache.NewCacheRequest("", api, "", "")
				metricString := testRequest.GetMetricString("")
				So(metricString, ShouldEqual, ".API.")
			})
			Convey("When empty api", func() {
				testRequest = cache.NewCacheRequest(service, "", "", "")
				metricString := testRequest.GetMetricString("")
				So(metricString, ShouldEqual, "SERVICE..")
			})
			Convey("When happy case", func() {
				testRequest = cache.NewCacheRequest(service, api, "", "")
				metricString := testRequest.GetMetricString("HIT")
				So(metricString, ShouldEqual, "SERVICE.API.HIT")
			})
		})
		Convey("When calling GetCacheKey", func() {
			Convey("When empty key", func() {
				testRequest = cache.NewCacheRequest(service, api, "", version)
				key := testRequest.GetCacheKey()
				So(key, ShouldEqual, "SERVICE-API--1")
			})
			Convey("When empty service", func() {
				testRequest = cache.NewCacheRequest("", api, key, version)
				key := testRequest.GetCacheKey()
				So(key, ShouldEqual, "-API-KEY-1")
			})
			Convey("When empty api", func() {
				testRequest = cache.NewCacheRequest(service, "", key, version)
				key := testRequest.GetCacheKey()
				So(key, ShouldEqual, "SERVICE--KEY-1")
			})
			Convey("When empty version", func() {
				testRequest = cache.NewCacheRequest(service, api, key, "")
				key := testRequest.GetCacheKey()
				So(key, ShouldEqual, "SERVICE-API-KEY-")
			})
			Convey("When happy case", func() {
				testRequest = cache.NewCacheRequest(service, api, key, version)
				key := testRequest.GetCacheKey()
				So(key, ShouldEqual, "SERVICE-API-KEY-1")
			})
		})
	})
}

func TestRedisStringCache_Get(t *testing.T) {
	Convey("When calling cache get", t, func() {
		mockedClientWrapper := new(mocks.IRedisClient)
		mockedIMetricLogger := new(mocks.IMetricLogger)
		request := cache.NewCacheRequest("service", "api", "key", "1")
		cache := cache.NewRedisStringCache(mockedClientWrapper, mockedIMetricLogger)
		Convey("When the string is returned", func() {
			mockedClientWrapper.On("Get", mock.Anything).Return("value", nil)
			mockedIMetricLogger.On("LogCountMetric", "service.api.hit", 1).Return()
			mockedIMetricLogger.On("LogDurationSinceMetric", "service.api.getLatency", mock.Anything).Return()
			cacheString := cache.Get(request)

			So(cacheString, ShouldEqual, "value")
		})
		Convey("When the key is missing", func() {
			mockedClientWrapper.On("Get", mock.Anything).Return("", redis.Nil)
			mockedIMetricLogger.On("LogCountMetric", "service.api.miss", 1).Return()
			mockedIMetricLogger.On("LogDurationSinceMetric", "service.api.getLatency", mock.Anything).Return()
			cacheString := cache.Get(request)

			So(cacheString, ShouldBeEmpty)
		})
		Convey("When error", func() {
			mockedClientWrapper.On("Get", mock.Anything).Return("", fmt.Errorf("error"))
			mockedIMetricLogger.On("LogDurationSinceMetric", "service.api.getLatency", mock.Anything).Return()
			cacheString := cache.Get(request)

			So(cacheString, ShouldBeEmpty)
		})
	})
}

func TestRedisStringCache_Set(t *testing.T) {
	Convey("When calling cache set", t, func() {
		mockedClientWrapper := new(mocks.IRedisClient)
		mockedIMetricLogger := new(mocks.IMetricLogger)
		request := cache.NewCacheRequest("service", "api", "key", "1")
		cache := cache.NewRedisStringCache(mockedClientWrapper, mockedIMetricLogger)
		Convey("when happy case", func() {
			mockedClientWrapper.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(nil)
			mockedIMetricLogger.On("LogDurationSinceMetric", "service.api.setLatency", mock.Anything).Return()
			cache.Set(request, "value", time.Minute)
		})
		Convey("When cache client throws error", func() {
			mockedClientWrapper.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(fmt.Errorf("error"))
			mockedIMetricLogger.On("LogDurationSinceMetric", "service.api.setLatency", mock.Anything).Return()
			cache.Set(request, "value", time.Minute)
		})
	})
}
