package cache_test

import (
	"encoding/json"
	"reflect"
	"testing"
	"time"

	"code.justin.tv/commerce/janus/cache"
	"code.justin.tv/commerce/janus/mocks"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestMoneyPennyCache_GetIsAffiliate(t *testing.T) {
	tuid := "tuid"
	mockedCacheClient := new(mocks.IKeyValueCache)
	affiliateTTL := 5 * time.Second
	nonAffiliateTTL := 1 * time.Second
	moneyPennyCache := cache.NewMoneyPennyCache(mockedCacheClient, "testMoneyPenny", affiliateTTL, nonAffiliateTTL)
	Convey("When calling GetIsAffiliate", t, func() {
		Convey("When cache client returns an empty result", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("").Once()
			hit, _ := moneyPennyCache.GetIsAffiliate(tuid)

			So(hit, ShouldBeFalse)
		})
		Convey("When the returned cache value can not be unmarshalled", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("Not Json").Once()
			hit, _ := moneyPennyCache.GetIsAffiliate(tuid)

			So(hit, ShouldBeFalse)
		})
		Convey("When cache client returns json value (happy case)", func() {
			cacheValue := cache.IsAffiliateCacheEntry{
				Tuid:        tuid,
				IsAffiliate: true,
			}
			marshalledBytes, jsonErr := json.Marshal(cacheValue)
			if jsonErr != nil {
				//This will point us to what went wrong better than t.FailNow()
				So(true, ShouldBeFalse)
			}
			jsonString := string(marshalledBytes)
			mockedCacheClient.On("Get", mock.Anything).Return(jsonString).Once()
			hit, val := moneyPennyCache.GetIsAffiliate(tuid)

			So(hit, ShouldBeTrue)
			So(reflect.DeepEqual(cacheValue, val), ShouldBeTrue)
		})
	})
}

func TestMoneyPennyCache_SetIsAffiliate(t *testing.T) {
	tuid := "tuid"
	mockedCacheClient := new(mocks.IKeyValueCache)
	affiliateTTL := 5 * time.Second
	nonAffiliateTTL := 1 * time.Second
	moneyPennyCache := cache.NewMoneyPennyCache(mockedCacheClient, "testMoneyPenny", affiliateTTL, nonAffiliateTTL)

	Convey("When calling SetIsAffiliate", t, func() {
		Convey("When the user isn't an affiliate", func() {
			mockedCacheClient.On("Set", mock.Anything, mock.Anything, nonAffiliateTTL).Return().Once()
			moneyPennyCache.SetIsAffiliate(tuid, false)

			mockedCacheClient.AssertCalled(t, "Set", mock.Anything, mock.Anything, nonAffiliateTTL)
		})
		Convey("When the user is an affiliate", func() {
			mockedCacheClient.On("Set", mock.Anything, mock.Anything, affiliateTTL).Return().Once()
			moneyPennyCache.SetIsAffiliate(tuid, true)

			mockedCacheClient.AssertCalled(t, "Set", mock.Anything, mock.Anything, affiliateTTL)
		})
	})
}
