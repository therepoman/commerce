package cache

import (
	"encoding/json"
	"time"

	log "github.com/sirupsen/logrus"
)

const (
	moneyPennyCacheVersion = "1"
	moneyPennyCacheService = "MoneyPenny"
)

type IsAffiliateCacheEntry struct {
	Tuid        string
	IsAffiliate bool
}

// IADGDiscoCache interface for ADGDiscoCache
type IMoneyPennyCache interface {
	GetIsAffiliate(tuid string) (isCacheHit bool, cacheEntry IsAffiliateCacheEntry)
	SetIsAffiliate(tuid string, isAffiliate bool)
}

// MoneyPennyCache struct for getting and setting cache values
type MoneyPennyCache struct {
	cacheClient          IKeyValueCache
	isAffiliateCachedAPI string
	affiliateTTL         time.Duration
	nonAffiliateTTL      time.Duration
}

func NewMoneyPennyCache(kvCacheClient IKeyValueCache, isAffiliateCachedAPI string, affiliateTTL time.Duration, nonAffiliateTTL time.Duration) IMoneyPennyCache {
	return &MoneyPennyCache{
		cacheClient:          kvCacheClient,
		isAffiliateCachedAPI: isAffiliateCachedAPI,
		affiliateTTL:         affiliateTTL,
		nonAffiliateTTL:      nonAffiliateTTL,
	}
}

func (cache *MoneyPennyCache) GetIsAffiliate(tuid string) (isCacheHit bool, cacheEntry IsAffiliateCacheEntry) {
	cacheRequest := NewCacheRequest(moneyPennyCacheService, cache.isAffiliateCachedAPI, tuid, moneyPennyCacheVersion)
	jsonString := cache.cacheClient.Get(cacheRequest)
	if jsonString == "" {
		return false, IsAffiliateCacheEntry{}
	}
	var isAffiliateCacheEntry IsAffiliateCacheEntry
	if err := json.Unmarshal([]byte(jsonString), &isAffiliateCacheEntry); err != nil {
		log.WithFields(log.Fields{
			"jsonString":   jsonString,
			"cacheRequest": cacheRequest,
			"error":        err,
		}).Error("Failed to unmarshal json")
		return false, IsAffiliateCacheEntry{}
	}
	return true, isAffiliateCacheEntry
}

func (cache *MoneyPennyCache) SetIsAffiliate(tuid string, isAffiliate bool) {
	cacheEntry := IsAffiliateCacheEntry{
		Tuid:        tuid,
		IsAffiliate: isAffiliate,
	}
	cacheKey := cacheEntry.Tuid
	cacheRequest := NewCacheRequest(moneyPennyCacheService, cache.isAffiliateCachedAPI, cacheKey, moneyPennyCacheVersion)
	var cacheTTL time.Duration
	if cacheEntry.IsAffiliate {
		cacheTTL = cache.affiliateTTL
	} else {
		cacheTTL = cache.nonAffiliateTTL
	}

	marshalledValue, err := json.Marshal(cacheEntry)
	if err != nil {
		log.WithFields(log.Fields{
			"cacheRequest": cacheRequest,
			"error":        err,
		}).Error("Failed to marshal moneypenny entry")
		return
	}
	cache.cacheClient.Set(cacheRequest, string(marshalledValue), cacheTTL)
}
