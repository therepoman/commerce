package cache_test

import (
	"encoding/json"
	"testing"
	"time"

	"code.justin.tv/commerce/janus/cache"
	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/mocks"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestAsinMapCacheClient_GetAsinMap(t *testing.T) {
	gameTitle := "gameTitle"
	asin := "asin"
	status := "LIVE"

	mockedCacheClient := new(mocks.IKeyValueCache)
	ttl := 5 * time.Second
	asinMapCacheClient := cache.NewAsinMapCacheClient(mockedCacheClient, "testAsinMap", ttl)
	Convey("When calling GetAsinMap", t, func() {
		Convey("When the cache returns an empty result", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("").Once()
			cacheHit, _ := asinMapCacheClient.GetAsinMap(gameTitle)
			So(cacheHit, ShouldBeFalse)
		})
		Convey("When the returned value can't be unmarshalled", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("Not Json").Once()
			cacheHit, _ := asinMapCacheClient.GetAsinMap(gameTitle)
			So(cacheHit, ShouldBeFalse)
		})
		Convey("When valid JSON is returned", func() {
			cacheValue := janus.GameAsinMap{
				Asin:      asin,
				GameTitle: gameTitle,
				Status:    status,
			}
			marshalledBytes, jsonErr := json.Marshal(cacheValue)
			if jsonErr != nil {
				//This will point us to what went wrong better than t.FailNow()
				So(true, ShouldBeFalse)
			}

			jsonString := string(marshalledBytes)
			mockedCacheClient.On("Get", mock.Anything).Return(jsonString).Once()
			cacheHit, cacheResult := asinMapCacheClient.GetAsinMap(gameTitle)
			So(cacheHit, ShouldBeTrue)
			So(gameTitle, ShouldEqual, cacheResult.GameTitle)
			So(asin, ShouldEqual, cacheResult.Asin)
			So(status, ShouldEqual, cacheResult.Status)
		})
	})
}

func TestAsinMapCacheClient_SetAsinMap(t *testing.T) {
	gameAsinMap := janus.GameAsinMap{
		Asin:      "asin",
		GameTitle: "gameTitle",
		Status:    "status",
	}
	mockedCacheClient := new(mocks.IKeyValueCache)
	ttl := 5 * time.Second
	asinMapCacheClient := cache.NewAsinMapCacheClient(mockedCacheClient, "testAsinMap", ttl)

	Convey("When calling SetAsinMap", t, func() {
		mockedCacheClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return().Once()
		asinMapCacheClient.SetAsinMap(&gameAsinMap)
		mockedCacheClient.AssertCalled(t, "Set", mock.Anything, mock.Anything, mock.Anything)
	})
}
