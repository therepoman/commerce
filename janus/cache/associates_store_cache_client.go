package cache

import (
	"encoding/json"
	"strings"
	"time"

	"code.justin.tv/commerce/janus/models"
	log "github.com/sirupsen/logrus"
)

const (
	associatesStoreCacheVersion = "1"
	associatesStoreCacheService = "associatesStoreService"
)

// AssociatesStoreCacheClient struct for getting and setting cache values
type AssociatesStoreCacheClient struct {
	cacheClient IKeyValueCache
	cachedAPI   string
	ttl         time.Duration
}

type AssociatesStoreCacheEntry struct {
	LinkedAssociatesStore *models.LinkedAssociatesStore
}

// IAssociatesStoreCacheClient interface for AssociatesStoreCacheClient
type IAssociatesStoreCacheClient interface {
	Get(userID string, marketplaceId string) (isCacheHit bool, linkedAssociatesStore *models.LinkedAssociatesStore)
	Put(linkedAssociatesStore *models.LinkedAssociatesStore)
	PutStoreInfoNotFound(userID string, marketplaceId string)
}

// NewAssociatesStoreCacheClient constructs a AssociatesStoreCacheClient with the passed in kvCacheClient
func NewAssociatesStoreCacheClient(kvCacheClient IKeyValueCache, api string, ttl time.Duration) IAssociatesStoreCacheClient {
	return &AssociatesStoreCacheClient{
		cacheClient: kvCacheClient,
		cachedAPI:   api,
		ttl:         ttl,
	}
}

func (cacheClient *AssociatesStoreCacheClient) Get(userID string, marketplaceId string) (bool, *models.LinkedAssociatesStore) {
	cacheKey := buildAssociatesStoreCacheKey(userID, marketplaceId)
	cacheRequest := NewCacheRequest(associatesStoreCacheService, cacheClient.cachedAPI, cacheKey, associatesStoreCacheVersion)
	jsonString := cacheClient.cacheClient.Get(cacheRequest)
	if jsonString == "" {
		return false, nil
	}

	var associatesStoreCacheEntry *AssociatesStoreCacheEntry
	if err := json.Unmarshal([]byte(jsonString), &associatesStoreCacheEntry); err != nil {
		log.Errorf("Failed to unmarshal for jsonString: %+v cacheRequest: %+v error: %+v", jsonString, cacheRequest, err)
		return false, nil
	}

	return true, associatesStoreCacheEntry.LinkedAssociatesStore
}

func (cacheClient *AssociatesStoreCacheClient) Put(linkedAssociatesStore *models.LinkedAssociatesStore) {
	cacheKey := buildAssociatesStoreCacheKey(linkedAssociatesStore.TUID, linkedAssociatesStore.MarketplaceID)
	cacheRequest := NewCacheRequest(associatesStoreCacheService, cacheClient.cachedAPI, cacheKey, associatesStoreCacheVersion)

	marshalledValue, err := json.Marshal(&AssociatesStoreCacheEntry{LinkedAssociatesStore: linkedAssociatesStore})
	if err != nil {
		log.Errorf("Failed to marshal associates store cache entry: %+v cacheRequest: %+v error: %+v",
			linkedAssociatesStore, cacheRequest, err)
		return
	}
	cacheClient.cacheClient.Set(cacheRequest, string(marshalledValue), cacheClient.ttl)
}

func (cacheClient *AssociatesStoreCacheClient) PutStoreInfoNotFound(userID string, marketplaceId string) {
	cacheKey := buildAssociatesStoreCacheKey(userID, marketplaceId)
	cacheRequest := NewCacheRequest(associatesStoreCacheService, cacheClient.cachedAPI, cacheKey, associatesStoreCacheVersion)

	marshalledValue, err := json.Marshal(&AssociatesStoreCacheEntry{})
	if err != nil {
		log.Errorf("Failed to marshal empty associates store cache entry. cacheRequest: %+v error: %+v",
			cacheRequest, err)
		return
	}
	cacheClient.cacheClient.Set(cacheRequest, string(marshalledValue), cacheClient.ttl)
}

func buildAssociatesStoreCacheKey(userID string, marketplaceId string) string {
	keyParts := []string{userID, marketplaceId}
	return strings.Join(keyParts, KeySeparator)
}
