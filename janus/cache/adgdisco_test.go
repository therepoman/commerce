package cache_test

import (
	"errors"
	"testing"

	"encoding/json"

	"reflect"

	"time"

	"code.justin.tv/commerce/janus/cache"
	"code.justin.tv/commerce/janus/mocks"
	"code.justin.tv/commerce/janus/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestADGCache_GetProductDetails(t *testing.T) {
	mockedCacheClient := new(mocks.IKeyValueCache)
	adgCache := cache.NewADGDiscoCache(mockedCacheClient, "testapi", time.Second)
	Convey("When calling getProductDetails", t, func() {
		params := &cache.ADGDiscoCacheParams{
			ASIN:   "asin",
			UserID: "id",
			Docket: "docket",
		}

		Convey("When cache client returns error", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("", errors.New("Cache client get call failed!")).Once()
			val := adgCache.GetProductDetails(params)

			So(val, ShouldBeNil)
		})
		Convey("When cache client returns an empty result", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("", nil).Once()
			val := adgCache.GetProductDetails(params)

			So(val, ShouldBeNil)
		})
		Convey("When the returned cache value can not be unmarshalled", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("this string is not a json adg product", nil).Once()
			val := adgCache.GetProductDetails(params)

			So(val, ShouldBeNil)
		})
		Convey("When cache client returns json value (happy case)", func() {
			var expectedProduct *models.ADGProduct
			expectedProduct = &models.ADGProduct{
				ASIN: "Asin",
			}
			marshalledBytes, jsonErr := json.Marshal(expectedProduct)
			if jsonErr != nil {
				//This will point us to what went wrong better than t.FailNow()
				So(true, ShouldBeFalse)
			}
			jsonString := string(marshalledBytes)

			mockedCacheClient.On("Get", mock.Anything).Return(jsonString, nil).Once()
			returnedProduct := adgCache.GetProductDetails(params)
			So(reflect.DeepEqual(expectedProduct, returnedProduct), ShouldBeTrue)
		})
	})
}

func TestADGCache_GetProductDetailsList(t *testing.T) {
	mockedCacheClient := new(mocks.IKeyValueCache)
	adgCache := cache.NewADGDiscoCache(mockedCacheClient, "testapi", time.Second)
	Convey("When calling getProductDetailsList", t, func() {
		params := &cache.ADGDiscoCacheParams{
			ASINList: []string{"asin"},
			UserID:   "id",
			Docket:   "docket",
		}

		Convey("When cache client returns error", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("", errors.New("Cache client get call failed!")).Once()
			val := adgCache.GetProductDetailsList(params)

			So(val, ShouldBeNil)
		})
		Convey("When cache client returns an empty result", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("", nil).Once()
			val := adgCache.GetProductDetailsList(params)

			So(val, ShouldBeNil)
		})
		Convey("When the returned cache value can not be unmarshalled", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("this string is not json adg products", nil).Once()
			val := adgCache.GetProductDetailsList(params)

			So(val, ShouldBeNil)
		})
		Convey("When cache client returns json value (happy case)", func() {
			var expectedProduct *models.ADGProduct
			expectedProduct = &models.ADGProduct{
				ASIN: "Asin",
			}

			expectedProducts := []models.ADGProduct{*expectedProduct, *expectedProduct}

			marshalledBytes, jsonErr := json.Marshal(expectedProducts)
			if jsonErr != nil {
				//This will point us to what went wrong better than t.FailNow()
				So(true, ShouldBeFalse)
			}
			jsonString := string(marshalledBytes)

			mockedCacheClient.On("Get", mock.Anything).Return(jsonString, nil).Once()
			returnedProducts := adgCache.GetProductDetailsList(params)

			So(reflect.DeepEqual(expectedProducts, returnedProducts), ShouldBeTrue)
		})
	})
}

func TestADGCache_GetDomainProducts(t *testing.T) {
	mockedCacheClient := new(mocks.IKeyValueCache)
	adgCache := cache.NewADGDiscoCache(mockedCacheClient, "testapi", time.Second)
	Convey("When calling getDomainProducts", t, func() {
		params := &cache.ADGDiscoCacheParams{
			ASINList: []string{"asin"},
			UserID:   "id",
			Docket:   "docket",
		}

		Convey("When cache client returns error", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("", errors.New("Cache client get call failed!")).Once()
			val := adgCache.GetDomainProducts(params)

			So(val, ShouldBeNil)
		})
		Convey("When cache client returns an empty result", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("", nil).Once()
			val := adgCache.GetDomainProducts(params)

			So(val, ShouldBeNil)
		})
		Convey("When the returned cache value can not be unmarshalled", func() {
			mockedCacheClient.On("Get", mock.Anything).Return("this string is not json adg products", nil).Once()
			val := adgCache.GetDomainProducts(params)

			So(val, ShouldBeNil)
		})
		Convey("When cache client returns json value (happy case)", func() {
			var expectedProduct *models.ADGProduct
			expectedProduct = &models.ADGProduct{
				ASIN: "Asin",
			}

			expectedProducts := []models.ADGProduct{*expectedProduct, *expectedProduct}

			marshalledBytes, jsonErr := json.Marshal(expectedProducts)
			if jsonErr != nil {
				//This will point us to what went wrong better than t.FailNow()
				So(true, ShouldBeFalse)
			}
			jsonString := string(marshalledBytes)

			mockedCacheClient.On("Get", mock.Anything).Return(jsonString, nil).Once()
			returnedProducts := adgCache.GetDomainProducts(params)

			So(reflect.DeepEqual(expectedProducts, returnedProducts), ShouldBeTrue)
		})
	})
}

func TestADGCache_SetProductDetails(t *testing.T) {
	mockedCacheClient := new(mocks.IKeyValueCache)
	adgCache := cache.NewADGDiscoCache(mockedCacheClient, "testapi", time.Second)
	Convey("When calling setProductDetails", t, func() {
		params := &cache.ADGDiscoCacheParams{
			ASIN:   "asin",
			UserID: "id",
			Docket: "docket",
		}
		product := &models.ADGProduct{
			ASIN: "asin",
		}
		raw, err := json.Marshal(product)
		So(err, ShouldBeNil)

		Convey("When cache client returns error", func() {
			mockedCacheClient.On("Set", mock.Anything, string(raw), time.Second).Return(errors.New("Cache client set failed")).Once()
			adgCache.SetProductDetails(params, product)
		})
		Convey("When cache client call succeeds", func() {
			mockedCacheClient.On("Set", mock.Anything, string(raw), time.Second).Return(nil).Once()
			adgCache.SetProductDetails(params, product)
		})
	})
}

func TestADGCache_SetProductDetailsList(t *testing.T) {
	mockedCacheClient := new(mocks.IKeyValueCache)
	adgCache := cache.NewADGDiscoCache(mockedCacheClient, "testapi", time.Second)
	Convey("When calling setProductDetailsList", t, func() {
		params := &cache.ADGDiscoCacheParams{
			ASINList: []string{"asin"},
			UserID:   "id",
			Docket:   "docket",
		}
		products := []models.ADGProduct{
			models.ADGProduct{
				ASIN: "asin",
			},
		}
		raw, err := json.Marshal(products)
		So(err, ShouldBeNil)

		Convey("When cache client returns error", func() {
			mockedCacheClient.On("Set", mock.Anything, string(raw), time.Second).Return(errors.New("Cache client set failed")).Once()
			adgCache.SetProductDetailsList(params, products)
		})
		Convey("When cache client call succeeds", func() {
			mockedCacheClient.On("Set", mock.Anything, string(raw), time.Second).Return(nil).Once()
			adgCache.SetProductDetailsList(params, products)
		})
	})
}

func TestADGCache_SetDomainProducts(t *testing.T) {
	mockedCacheClient := new(mocks.IKeyValueCache)
	adgCache := cache.NewADGDiscoCache(mockedCacheClient, "testapi", time.Second)
	Convey("When calling setDomainProducts", t, func() {
		params := &cache.ADGDiscoCacheParams{
			ASINList: []string{"asin"},
			UserID:   "id",
			Docket:   "docket",
		}
		products := []models.ADGProduct{
			models.ADGProduct{
				ASIN: "asin",
			},
		}
		raw, err := json.Marshal(products)
		So(err, ShouldBeNil)

		Convey("When cache client returns error", func() {
			mockedCacheClient.On("Set", mock.Anything, string(raw), time.Second).Return(errors.New("Cache client set failed")).Once()
			adgCache.SetDomainProducts(params, products)
		})
		Convey("When cache client call succeeds", func() {
			mockedCacheClient.On("Set", mock.Anything, string(raw), time.Second).Return(nil).Once()
			adgCache.SetDomainProducts(params, products)
		})
	})
}

func TestProductDetailsCacheKey(t *testing.T) {
	Convey("Uses correct fields", t, func() {
		params := &cache.ADGDiscoCacheParams{
			ASIN:   "asin",
			UserID: "id",
			Docket: "docket",
		}
		So(cache.ProductDetailsCacheKey(params), ShouldEqual, "asin-id-docket")
	})
}

func TestProductDetailsListCacheKey(t *testing.T) {
	Convey("Uses correct fields", t, func() {
		params := &cache.ADGDiscoCacheParams{
			ASINList: []string{
				"asin",
				"otherAsin",
				"anotherAsin",
			},
			UserID:   "id",
			DocketID: "docket",
		}
		So(cache.ProductDetailsListCacheKey(params), ShouldEqual, "anotherAsin-asin-otherAsin-id-docket")
	})
}

func TestDomainProductsCacheKey(t *testing.T) {
	Convey("Uses correct fields", t, func() {
		params := &cache.ADGDiscoCacheParams{
			ASIN:       "asin",
			VendorCode: "code",
			SKU:        "sku",
			UserCOR:    "cor",
		}
		So(cache.DomainProductsCacheKey(params), ShouldEqual, "asin-code-sku-cor")
	})

	Convey("Omits missing fields", t, func() {
		params := &cache.ADGDiscoCacheParams{
			VendorCode: "code",
			SKU:        "sku",
		}
		So(cache.DomainProductsCacheKey(params), ShouldEqual, "code-sku")
	})
}
