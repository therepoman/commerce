package cache

import (
	"time"

	"strings"

	"code.justin.tv/commerce/janus/metrics"
	"github.com/go-redis/redis"
	log "github.com/sirupsen/logrus"
)

const (
	KeySeparator    = "-"
	MetricSeparator = "."
	hit             = "hit"
	miss            = "miss"
	getLatency      = "getLatency"
	setLatency      = "setLatency"
)

type IKeyValueCache interface {
	Get(request CacheRequest) string
	Set(request CacheRequest, value string, ttl time.Duration)
}

type IRedisClient interface {
	Get(key string) (string, error)
	Set(key string, value interface{}, expiration time.Duration) error
}

type RedisStringCache struct {
	RedisClient   IRedisClient
	metricsLogger metrics.IMetricLogger
}

type RedisClientWrapper struct {
	RedisClient redis.Cmdable
}

type CacheRequest struct {
	Service string
	API     string
	Key     string
	Version string
}

// NewCacheRequest constructs a CacheRequest with the provided service, api, key, and version
func NewCacheRequest(service string, api string, key string, version string) CacheRequest {
	return CacheRequest{
		Service: service,
		API:     api,
		Key:     key,
		Version: version,
	}
}

// NewRedisStringCache constructs an IKeyValueCache with the given client and metric logger
func NewRedisStringCache(client IRedisClient, metrics metrics.IMetricLogger) IKeyValueCache {
	return &RedisStringCache{
		RedisClient:   client,
		metricsLogger: metrics,
	}
}

// NewRedisClientWrapper constructs an IRedisClient with the given redis cmdable
func NewRedisClientWrapper(client redis.Cmdable) IRedisClient {
	return &RedisClientWrapper{
		RedisClient: client,
	}
}

// Get returns the string stored for the request from the cache
func (rsc *RedisStringCache) Get(request CacheRequest) string {
	startTime := time.Now()
	key := request.GetCacheKey()
	cacheVal, err := rsc.RedisClient.Get(key)
	if err == redis.Nil {
		// Nothing was found for the key given
		rsc.metricsLogger.LogCountMetric(request.GetMetricString(miss), 1)
		rsc.metricsLogger.LogDurationSinceMetric(request.GetMetricString(getLatency), startTime)
		return ""
	}
	if err != nil {
		log.Errorf("Redis client get call failed for cacheKey: %+v error: %+v", key, err)
		rsc.metricsLogger.LogDurationSinceMetric(request.GetMetricString(getLatency), startTime)
		return ""
	}
	rsc.metricsLogger.LogCountMetric(request.GetMetricString(hit), 1)
	rsc.metricsLogger.LogDurationSinceMetric(request.GetMetricString(getLatency), startTime)
	return cacheVal
}

// Set sets the value with a ttl for the given request in the cache
func (rsc *RedisStringCache) Set(request CacheRequest, value string, ttl time.Duration) {
	startTime := time.Now()
	key := request.GetCacheKey()
	err := rsc.RedisClient.Set(key, value, ttl)
	if err != nil {
		log.Errorf("Redis client set call failed for cacheKey: %+v error: %+v", key, err)
	}
	rsc.metricsLogger.LogDurationSinceMetric(request.GetMetricString(setLatency), startTime)
}

// GetMetricString builds the metric string from the relevant parts of the request
func (req *CacheRequest) GetMetricString(metric string) string {
	metricsParts := []string{req.Service, req.API, metric}
	return strings.Join(metricsParts, MetricSeparator)
}

// GetCacheKey builds the cache key for the given request
func (req *CacheRequest) GetCacheKey() string {
	keyParts := []string{req.Service, req.API, req.Key, req.Version}
	return strings.Join(keyParts, KeySeparator)
}

// Get wraps the redis client get call
func (wrapper *RedisClientWrapper) Get(key string) (string, error) {
	return wrapper.RedisClient.Get(key).Result()
}

// Set wraps the redis client set call
func (wrapper *RedisClientWrapper) Set(key string, value interface{}, expiration time.Duration) error {
	return wrapper.RedisClient.Set(key, value, expiration).Err()
}
