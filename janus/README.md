# janus

See full documentation at: [https://w.amazon.com/index.php/Respawn/Store/NewHire/Twitch#Go_Development](https://w.amazon.com/index.php/Respawn/Store/NewHire/Twitch#Go_Development)

## Setting Up the Go Service for Janus 

### Setting up your Github account

 - Go to your [git account settings page](https://git-aws.internal.justin.tv/settings/keys) and follow the instructions there to add a new SSH key to your account.
 - As someone on your team to add you to the [commerce repo on git](https://git-aws.internal.justin.tv/commerce).

### Setting up your workspace

 - Install go:

		brew install go

 - Create a Go workspace folder in your home directory. Ex:

		mkdir /Users/<username>/GoWorkspace

 - Set your environment variable for GOPATH (in your .bashrc, .zshrc, or etc.)

		export GOPATH=/Users/<username>/GoWorkspace

 - Append GOPATH/bin to PATH environment variable

		export PATH=$PATH:$GOPATH/bin

 - Clone down Janus source within the new commerce folder

		git clone git@git-aws.internal.justin.tv:commerce/janus.git $GOPATH/src/code.justin.tv/commerce/janus

 - Directory structure will now look like this:

		/Users/<username>/GoWorkspace/src/code.justin.tv/commerce/janus

### Set up dependencies

 - Navigate to your workspace

		cd /Users/<username>/GoWorkspace/src/code.justin.tv/commerce/janus
		
 - Fetch all needed tools

        make setup

 - OPTIONAL (If you need to alter a dependency): Clone down all the dependencies into your GoWorkspace directory by running

		godep restore

	- You will need to be on the Twitch VPN for this step to work properly. If you are not on the VPN you will see: `godep: error downloading dep (code.justin.tv/chat/timing): unrecognized import path "code.justin.tv/chat/timing" ...`

### Managing Sandstorm Secrets
###### IAM User

 - On Amazon network, go to Isenguard, connect to "twitch-bits-aws@amazon.com" account in AWS.
 - Head over to the [console](https://console.aws.amazon.com/iam/home?#users).
 - Create a new user for yourself. Choose a name that can be easily associate with you (like your LDAP username).
 - Click on "Show User Security Credentials" to access your Access Key ID and Secret Access Key.
 - On your laptop, create the following file and directory if neccessary:

		~/.aws/credentials

 - Edit the file with the following code and fill it with your Access Key ID and Secret Access Key.

	```
	[default]
	aws_access_key_id = <your_access_key_id>
	aws_secret_access_key = <your_secret_access_key>
	region = us-west-2
	```

 - Back on [console](https://console.aws.amazon.com/iam/home?#users), select your newly created user.
 - In Permissions, click on Attach Policy. Search for `janus_local_sandstorm` policy, select it and attach it.
 - On the same page, also attach the `full_dynamo` policy to your user
 - Back on your user page, copy your User ARN.

###### Giving your IAM User Access the Janus Service Role

 - On Twitch network, go to [Roles Dashboard](https://dashboard.internal.justin.tv/sandstorm/manage-roles)
 - Find "commerce\_janus\_local\_sws" role and add yourself on "Allowed ARNs" on a new line.
	- Sometimes, it will say "Error Updating Role: policy:No keys passed to authorize against". You can go around that by copying over a secret name on a new line, add one character to it and save. Once it saved correctly, go back on the role and suppress the secret you "added".

### Building Code

 - In your cloned Janus repo, start by creating a new branch.

		git checkout -b new-branch

 - Edit one or more of the files

	- If you are going to use SWS calls, make sure you modfy "config/development.json" file to specify the path to SWS certs

 - To build your code, run:

		make install

 - To run the tests in the code base:

		make test

	- In order to be able to do SWS calls, be sure to be on Guest Wifi and be on Twitch Network over VPN. Failing to do this could cause problems with SWS since we need to whitelist IPs from where we are calling SWS.

 - This will run the Go project executable:

		make dev
		
### Testing Code

#### Sample API calls
- Use [Postman](https://www.getpostman.com/) as a tool for making API calls
- In Postman, click Import in the top left
- Click Import From Link
- Paste the link to the Janus collection: https://www.getpostman.com/collections/15313196d56fd11bf17e
- You should now have the shared collection, which includes sample API calls for all APIs, for all environments

Want to make these same calls through Visage? We got you! Do the same thing with this collection:
- Visage collection: https://www.getpostman.com/collections/d9c8e23cf5d16e1fea91

#### Sample Chat Notifications

You can use the aws-cli tool to test your SNS messages from the command line. You can get the tool with [Homebrew](https://brew.sh/)

`$ brew install awscli`

Sample Prod SNS Chat notification command:
```
$ aws sns publish --topic-arn "arn:aws:sns:us-west-2:021561903526:FuelPurchase-Prod" --message "{\"asin\":\"B06XKP5YYK\",\"twitch_user_id\":\"YOUR_TUID_HERE\",\"order_id\":\"amzn1.orderId.D01-1234567-7654321\",\"channel_id\":\"139075904\",\"marketplace_id\":\"ATVPDKIKX0DER\"}" --message-attributes "{\"Version\":{\"DataType\":\"Number\",\"StringValue\":\"1\"}}"
```
Insert the tuid, order_id, and channel_id of your choosing to the above. Order id should be be a valid UUID. Sample channel_id above is qa_tw_partner.

Sample Prod SNS Open Crate event command:
```
$ aws sns publish --topic-arn "arn:aws:sns:us-west-2:021561903526:FuelOpenCrate-Prod" --message "{\"crate_id\":\"c123\",\"twitch_user_id\":\"YOUR_TUID_HERE\",\"order_id\":\"amzn1.orderId.D01-1234567-76543992\",\"channel_id\":\"139075904\",\"contents\":[{\"type\":\"emote\",\"quantity\":1,\"content_id\":\"123\"},{\"type\":\"badge\",\"quantity\":1,\"content_id\":\"457\"},{\"type\":\"bits\",\"quantity\":23,\"content_id\":\"\"}]}" --message-attributes "{\"Version\":{\"DataType\":\"Number\",\"StringValue\":\"1\"}}"
```
Insert the tuid and order_id of your choosing to the above. Order id should be be a valid UUID. Sample channel_id above is qa_tw_partner.

Alternatively, you can use this Postman collections for calling TMI::UserNotice directly. The collection contains various samples related to Fuel purchases and crates. Import link:
- TMI::UserNotice collection: https://www.getpostman.com/collections/b9fc3d0add7e70c8c6d2

#### PubSub 3rd party developer integration
There are some PubSub integration points that are commerce related that live in Janus. 
Official documentation: https://dev.twitch.tv/docs/v5/guides/PubSub/

In order to test these, use a tool like [wscat](https://www.npmjs.com/package/wscat) to listen to a PubSub topic then call Janus on localhost to trigger the PubSub event and make sure it gets sent.

Example usage:
```
wscat -c wss://pubsub-edge.twitch.tv

//AUTHED FUEL EVENTS
{"type": "LISTEN", "nonce": "aseeff", "data" : { "topics" : ["channel-commerce-events-v1.<some channel id>"], "auth_token" : "<OAUTH TOKEN GOES HERE>" } }
```

Note: For this PubSub topic, the channel's oauth token is required in order to have access to the topic.

#### Unit Testing
- Use [Testify](https://github.com/stretchr/testify) to mock out behaviour in tests
- Use [Mockery](https://github.com/vektra/mockery) to autogenerate mock classes

#### Integration Testing
Integration tests are a work in progress.  See what is covered in the `integration_tests` directory.

You can run integration tests against your dev instance of Janus by running the command:

        make integ-test-dev

It is also possible to run integration tests against staging/prod by using the make targets `integ-test-staging` and `integ-test-prod`, respectively.

#### Testing SQS Locally
- You will need to set the SQSEnabled config setting to true in development.json
- You will need to give the IAM user you use to run janus locally proper SQS permissions through the AWS console.
You can add `janus_sqs_staging` to your IAM role on twitch-bits-aws@amazon.com.


### Cleaning Code

 - In order to use the tools described in this section, you must install them as a one time setup step.
Use the following commnads to install the tools:

		go get -u github.com/kisielk/errcheck

		go get -u github.com/golang/lint/golint

		go get golang.org/x/tools/cmd/goimports

 - If everything works, double check your code styles and linting by running:

		make lint

 - Many mechanical errors found by golint and errcheck are automatically fixed up by goimports, a standard automated formatter tool for Go. Note that Go standard indentation is tabs, displayed as 8 spaces. Editors can be configured to display a different indentation width. Please use tabs throughout to keep a uniform codebase. Run this command to see the changes goimports will make:

		goimports -d myfile.go

 - Run this command to actually make the changes to the file:

		goimports -w myfile.go

Further reading on Go code style: [https://github.com/golang/go/wiki/CodeReviewComments](https://github.com/golang/go/wiki/CodeReviewComments)

### Pull Request and Pushing Code to Master

 - Push your changes to a remote branch using:

		git push origin new-branch

 - Use Github to create a pull request: https://git-aws.internal.justin.tv/commerce/janus/compare/new-branch
 - Ask people to look over your pull request and approve it!
 - Make sure to get proper sign off before you merge
 - This might take multiple iterations until your team is satisfied ¯\\\_(ツ)\_/¯

### Pushing Code to Janus hosts
- Go to the [deployment page for Janus](https://clean-deploy.internal.justin.tv/#/commerce/janus)
- Verify that your change is the latest change in `master`
- Select `Deploy to...` and then select `staging`
- Once the deployment finishes, verify your change on our staging environment (http://janus-staging-dev-def.us-west2.justin.tv/)
- Once you've verified that everything works in staging, repeat this process for prod (https://janus.prod.us-west2.justin.tv)

### Setting up Intellij

 - Download the Go plugin for Intellij: [https://plugins.jetbrains.com/plugin/5047?pr=idea](https://plugins.jetbrains.com/plugin/5047?pr=idea)
 - Or go to **Intellij IDEA -> Preferences... -> Plugins -> Browse repositories... -> Search 'Go' -> Go Custom Languages -> Install**
 - Set up the Go SDK
   - Go to **File -> Project Structures -> SDK**
   - Click the `+` icon
   - If Go was installed with homebrew, add the Go SDK as `/usr/local/Cellar/go/<version>/libexec`
 - Open your GoWorkspace in Intellij
   - Create a new project
   - Select Go as the language
   - Choose the SDK you configured above
   - Set the project location as the Go src directory (`/Users/<username>/GoWorkspace/src` using the example above)
 - (Optional) Configure GOPATH if not launching IntelliJ from an environment with GOPATH set
   - Preferences -> Appearance and Behavior -> Path Variables

### Making Godep Changes

If you need to add or update a Go dependency, take a look at this [readme](https://github.com/tools/godep). To add a new dependency:

 - Add code that imports the new package
 - run:
 
		godep -t save ./...
	
	- If it says that you are missing a package, make sure to `go get` it and rerun `godep -t save ./...`

If everything works, then godep should have added the new dependencies to `_vendor` and updated Godep.json.

Note that manually updating Godep.json with the new dependency **will not work**. If you updated Godep.json without running `godep -t save ./...`, go undo your change otherwise godep won't play nice.

If all else fails try looking into the instructions [here](https://twitchtv.atlassian.net/wiki/display/ENG/Vendor+Go+dependencies)

### Troubleshooting

#### Brew Install: Agree to license for Xcode

If Brew install says Agree to license for Xcode Builds will fail! run

	sudo xcodebuild -license

#### Brew install: Unprocessable Entity

If Brew install says something like: 422 Unprocessable Entity (GitHub::Error) run

	brew update

#### Brew install: 'require': cannot load such file -- mach (LoadError)

If Brew install says something like (while running brew update) `/System/Library/Frameworks/Ruby.framework/Versions/2.0/usr/lib/ruby/2.0.0/rubygems/core_ext/kernel_require.rb:55:in 'require': cannot load such file -- mach (LoadError)`, try running

	sudo chown -R $(whoami):admin /usr/local

#### Intellij: Empty GOPATH

If Intellij says there is an empty GOPATH, click on Edit configuration and add your GOPATH into the global settings. Ex:

	/Users/<username>/GoWorkspace

#### Intellij: Not a valid home for GO sdk


If Intellij says "the selected directory is not a valid home for GO sdk", try updating the version of IntelliJ and the Go plugin

#### Permission denied on push

```
ryanresi@laptop] git push origin testbranch                                                                         ~GOPATH/src/code.justin.tv/commerce/janus
remote: Permission to commerce/janus.git denied to ryanresi.
fatal: unable to access 'https://git-aws.internal.justin.tv/commerce/janus.git/': The requested URL returned error: 403
```

You need to be added to the git repo permissions list. Ask someone on the team to add you.

