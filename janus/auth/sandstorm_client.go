package auth

import (
	"time"

	"code.justin.tv/systems/sandstorm/manager"

	"code.justin.tv/commerce/janus/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
)

type ISandstormManager interface {
	Get(secretName string) (*manager.Secret, error)
}

func NewSandstormClient(configuration *config.Configuration) ISandstormManager {
	awsConfig := &aws.Config{Region: aws.String(configuration.SandstormAWSRegion)}
	stsclient := sts.New(session.New(awsConfig))
	arp := &stscreds.AssumeRoleProvider{
		Duration:     900 * time.Second,
		ExpiryWindow: 10 * time.Second,
		RoleARN:      configuration.SandstormRoleARN,
		Client:       stsclient,
	}
	creds := credentials.NewCredentials(arp)
	awsConfig.WithCredentials(creds)
	config := manager.Config{
		AWSConfig: awsConfig,
		TableName: configuration.SandstormTableName,
		KeyID:     configuration.SandstormKeyId,
	}

	return manager.New(config)
}
