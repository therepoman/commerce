package auth

import (
	"errors"
	"fmt"

	jwt "github.com/dgrijalva/jwt-go"
	log "github.com/sirupsen/logrus"
)

const (
	HS256 = "HS256"
)

// JWTHelper interface for generating jwts
type JWTHelper struct {
}

// IJWTHelper interface for generating jwts
type IJWTHelper interface {
	GetSignedTokenString(claims jwt.Claims, secret []byte, algorithmType string) (string, error)
}

// GetSignedToken returns a signed jwt using the given claims, secret and algorithm
func (jwtHelper *JWTHelper) GetSignedTokenString(claims jwt.Claims, secret []byte, algorithmType string) (string, error) {
	var token *jwt.Token
	switch algorithmType {
	case HS256:
		token = jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	default:
		return "", errors.New(fmt.Sprintf("Signing method %s not implemented", algorithmType))
	}

	tokenString, err := token.SignedString(secret)
	if err != nil {
		log.Errorf("Error signing jwt with secret")
		return "", err
	}

	return tokenString, nil
}
