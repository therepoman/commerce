package auth_test

import (
	"net/http"
	"testing"
	"time"

	"code.justin.tv/commerce/janus/auth"
	"code.justin.tv/common/goauthorization"

	"io/ioutil"

	"fmt"

	"code.justin.tv/commerce/janus/mocks"
	"code.justin.tv/systems/sandstorm/manager"
	. "github.com/smartystreets/goconvey/convey"
)

var encoder goauthorization.Encoder

const userID = "123"

var sandstorm *mocks.ISandstormManager

func setupCartman() {
	publicKeySecretName := "/test/sandstorm/domain"
	publicKeyFile := "testdata/public.pem"
	publicKey, err := ioutil.ReadFile(publicKeyFile)
	if err != nil {
		panic("Cannot read cartman public key file for testing")
	}
	privateKeyFile := "testdata/private.pem"
	privateKey, err := ioutil.ReadFile(privateKeyFile)
	if err != nil {
		panic("Cannot read cartman private key file for testing")
	}

	publicKeyConfigMap := map[string]string{
		"RS256": publicKeySecretName,
	}

	sandstorm = new(mocks.ISandstormManager)
	sandstorm.On("Get", publicKeySecretName).Return(&manager.Secret{Plaintext: publicKey}, nil)

	err = auth.InitializeDecoders(sandstorm, publicKeyConfigMap)
	if err != nil {
		panic(fmt.Sprintf("Cannot initialize cartman: %s", err))
	}

	encoder, err = goauthorization.NewEncoder("RS256", "code.justin.tv/web/cartman", privateKey)
	if err != nil {
		panic(fmt.Sprintf("Cannot initialize cartman: %s", err))
	}
}

func getCapabilities(tuid string) goauthorization.CapabilityClaims {
	return goauthorization.CapabilityClaims{
		"view_amendment_acceptance": goauthorization.CapabilityClaim{
			"id": tuid,
		},
	}
}

func generateCartmanAudienceToken() *goauthorization.AuthorizationToken {
	return generateToken("code.justin.tv/web/cartman")
}

func generateJanusAudienceToken() *goauthorization.AuthorizationToken {
	return generateToken("code.justin.tv/commerce/janus")
}

func generateToken(audience string) *goauthorization.AuthorizationToken {
	now := time.Now()
	params := goauthorization.TokenParams{
		Exp:    now.Add(10 * time.Minute),
		Nbf:    now,
		Aud:    []string{audience},
		Sub:    "dianers",
		Claims: getCapabilities(userID),
	}
	token := encoder.Encode(params)
	return token
}

func TestCartman(t *testing.T) {
	setupCartman()

	Convey("when validating an auth token", t, func() {
		request, err := http.NewRequest("GET", "foobar", nil)
		cartmanAuthorizer := &auth.CartmanAuthorizer{}
		capabilities := getCapabilities(userID)
		So(err, ShouldBeNil)

		Convey("with valid janus audience token", func() {
			token := generateJanusAudienceToken()
			serialToken, err := token.String()
			So(err, ShouldBeNil)

			request.Header.Set("Twitch-Authorization", serialToken)

			err = cartmanAuthorizer.AuthorizeCartmanToken(request, &capabilities)
			So(err, ShouldBeNil)
		})

		Convey("with valid cartman audience token", func() {
			token := generateCartmanAudienceToken()
			serialToken, err := token.String()
			So(err, ShouldBeNil)

			request.Header.Set("Twitch-Authorization", serialToken)

			err = cartmanAuthorizer.AuthorizeCartmanToken(request, &capabilities)
			So(err, ShouldBeNil)
		})

		Convey("with invalid token", func() {
			serialToken := "foobar"
			request.Header.Set("Twitch-Authorization", serialToken)

			err = cartmanAuthorizer.AuthorizeCartmanToken(request, &capabilities)
			So(err, ShouldNotBeNil)
		})

		Convey("with empty token", func() {
			serialToken := ""
			request.Header.Set("Twitch-Authorization", serialToken)

			err = cartmanAuthorizer.AuthorizeCartmanToken(request, &capabilities)
			So(err, ShouldNotBeNil)
		})

		Convey("with invalid userID", func() {
			userIDInvalid := "foobar"
			capabilities = getCapabilities(userIDInvalid)
			token := generateJanusAudienceToken()
			serialToken, err := token.String()
			So(err, ShouldBeNil)

			request.Header.Set("Twitch-Authorization", serialToken)

			err = cartmanAuthorizer.AuthorizeCartmanToken(request, &capabilities)
			So(err, ShouldNotBeNil)
		})
	})
}
