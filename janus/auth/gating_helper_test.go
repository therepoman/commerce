package auth_test

import (
	"testing"

	"code.justin.tv/commerce/janus/auth"

	"code.justin.tv/commerce/janus/mocks"

	"errors"
	"net/http"

	"code.justin.tv/common/goauthorization"

	"code.justin.tv/web/users-service/models"
	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

var gatingHelper *auth.GatingHelper
var cartmanAuthorizer *mocks.ICartmanAuthorizer
var usersService *mocks.UsersServiceClient

const whitelistedUserID = "12345"
const staffUserID = "54321"
const normalUserID = "5678"
const invalidUserID = "NOT_123_NUMBERS"
const emptyUserID = ""

const staffWhitelistedGameID = "whitelisted"
const staffNotWhitelistedGameID = "notWhitelisted"

var staffUserProperties = models.Properties{
	Admin: create(true),
}
var normalUserProperties = models.Properties{
	Admin: create(false),
}

func setupGatingHelper() {
	cartmanAuthorizer = new(mocks.ICartmanAuthorizer)
	usersService = new(mocks.UsersServiceClient)

	fuelWhitelist := map[string]bool{
		whitelistedUserID: true,
	}

	gamesWhitelist := map[string]bool{
		staffWhitelistedGameID: true,
	}

	gatingHelper = &auth.GatingHelper{
		UsersServiceClient:  usersService,
		CartmanAuthorizer:   cartmanAuthorizer,
		FuelWhitelist:       fuelWhitelist,
		StaffGamesWhitelist: gamesWhitelist,
	}
}

func setupMocks() {
	staffCapabilities := &goauthorization.CapabilityClaims{
		"view_all_details": goauthorization.CapabilityClaim{
			"user_id": 54321,
		},
	}

	cartmanAuthorizer.On("AuthorizeCartmanToken", Anything, staffCapabilities).Return(nil)
	cartmanAuthorizer.On("AuthorizeCartmanToken", Anything, Anything).Return(errors.New(""))

	usersService.On("GetUserByID", Anything, normalUserID, Anything).Return(&normalUserProperties, nil)
	usersService.On("GetUserByID", Anything, staffUserID, Anything).Return(&staffUserProperties, nil)
	usersService.On("GetUserByID", Anything, staffUserID, Anything).Return(nil, errors.New(""))
}

func TestGating(t *testing.T) {
	Convey("CanSeeHiddenInformation test", t, func() {
		setupGatingHelper()
		setupMocks()

		request, err := http.NewRequest("GET", "foobar", nil)

		So(err, ShouldBeNil)
		Convey("with a staff whitelisted game", func() {
			gameID := staffWhitelistedGameID
			Convey("with whitelisted user", func() {
				canSee := gatingHelper.CanSeeHiddenInformation(request, whitelistedUserID, gameID)

				So(canSee, ShouldBeTrue)
			})
			Convey("with non whitelisted, staff user (cartman authorization)", func() {
				canSee := gatingHelper.CanSeeHiddenInformation(request, staffUserID, gameID)

				So(canSee, ShouldBeTrue)
			})
			Convey("with non whitelisted, staff user (users-service authorization)", func() {
				cartmanAuthorizer.On("ExtractActingTuid", request).Return(staffUserID, errors.New(""))
				canSee := gatingHelper.CanSeeHiddenInformation(request, staffUserID, gameID)

				So(canSee, ShouldBeTrue)
			})
			Convey("with non whitelisted, non staff user", func() {
				cartmanAuthorizer.On("ExtractActingTuid", request).Return(normalUserID, errors.New(""))
				canSee := gatingHelper.CanSeeHiddenInformation(request, normalUserID, gameID)

				So(canSee, ShouldBeFalse)
			})
			Convey("With an invalid userID", func() {
				cartmanAuthorizer.On("ExtractActingTuid", Anything).Return(invalidUserID, nil)
				canSee := gatingHelper.CanSeeHiddenInformation(request, invalidUserID, gameID)

				So(canSee, ShouldBeFalse)
			})
			Convey("With an empty userID", func() {
				cartmanAuthorizer.On("ExtractActingTuid", Anything).Return(emptyUserID, nil)
				canSee := gatingHelper.CanSeeHiddenInformation(request, emptyUserID, gameID)

				So(canSee, ShouldBeFalse)
			})
		})

		Convey("with a non staff whitelisted game", func() {
			gameID := staffNotWhitelistedGameID
			Convey("with whitelisted user", func() {
				canSee := gatingHelper.CanSeeHiddenInformation(request, whitelistedUserID, gameID)

				So(canSee, ShouldBeTrue)
			})
			Convey("with non whitelisted, staff user", func() {
				canSee := gatingHelper.CanSeeHiddenInformation(request, staffUserID, gameID)

				So(canSee, ShouldBeFalse)
			})
			Convey("with non whitelisted, non staff user", func() {
				canSee := gatingHelper.CanSeeHiddenInformation(request, normalUserID, gameID)

				So(canSee, ShouldBeFalse)
			})
			Convey("With an invalid userID", func() {
				canSee := gatingHelper.CanSeeHiddenInformation(request, invalidUserID, gameID)

				So(canSee, ShouldBeFalse)
			})
			Convey("With an enpty userID", func() {
				canSee := gatingHelper.CanSeeHiddenInformation(request, emptyUserID, gameID)

				So(canSee, ShouldBeFalse)
			})
		})
	})

	Convey("IsWhitelisted test", t, func() {
		setupGatingHelper()
		setupMocks()

		Convey("with whitelisted user", func() {
			isWhitelisted := gatingHelper.IsWhitelisted(whitelistedUserID)

			So(isWhitelisted, ShouldBeTrue)
		})
		Convey("With a non-whitelisted user", func() {
			isWhitelisted := gatingHelper.IsWhitelisted(normalUserID)

			So(isWhitelisted, ShouldBeFalse)
		})
		Convey("With an empty userID", func() {
			isWhitelisted := gatingHelper.IsWhitelisted(emptyUserID)

			So(isWhitelisted, ShouldBeFalse)
		})
	})
}

func TestExtractActingTuid(t *testing.T) {
	Convey("UserIsId test", t, func() {
		setupGatingHelper()

		request, err := http.NewRequest("GET", "foobar", nil)
		So(err, ShouldBeNil)

		Convey("with no Cartman token", func() {
			cartmanAuthorizer.On("ExtractActingTuid", request).Return("", goauthorization.ErrNoAuthorizationToken)
			matches, err := gatingHelper.UserIsId(request, staffUserID)

			So(err, ShouldNotBeNil)
			So(matches, ShouldBeFalse)
		})
		Convey("with non-valid Cartman token", func() {
			cartmanAuthorizer.On("ExtractActingTuid", request).Return("", errors.New(""))
			matches, err := gatingHelper.UserIsId(request, staffUserID)

			So(err, ShouldNotBeNil)
			So(matches, ShouldBeFalse)
		})
		Convey("with valid Cartman token, does not match argument", func() {
			cartmanAuthorizer.On("ExtractActingTuid", request).Return(normalUserID, nil)
			matches, err := gatingHelper.UserIsId(request, staffUserID)

			So(err, ShouldBeNil)
			So(matches, ShouldBeFalse)
		})
		Convey("with valid Cartman token, does match argument", func() {
			cartmanAuthorizer.On("ExtractActingTuid", request).Return(staffUserID, nil)
			matches, err := gatingHelper.UserIsId(request, staffUserID)

			So(err, ShouldBeNil)
			So(matches, ShouldBeTrue)
		})
	})

	Convey("GetOptionalActingTuid test", t, func() {
		setupGatingHelper()

		request, err := http.NewRequest("GET", "foobar", nil)
		So(err, ShouldBeNil)

		Convey("with no Cartman token", func() {
			cartmanAuthorizer.On("ExtractActingTuid", request).Return("", goauthorization.ErrNoAuthorizationToken)
			tuid, err := gatingHelper.GetOptionalActingTuid(request)

			So(err, ShouldBeNil)
			So(tuid, ShouldEqual, "")
		})
		Convey("with non-valid Cartman token", func() {
			cartmanAuthorizer.On("ExtractActingTuid", request).Return("", errors.New(""))
			tuid, err := gatingHelper.GetOptionalActingTuid(request)

			So(err, ShouldNotBeNil)
			So(tuid, ShouldEqual, "")
		})
		Convey("with valid Cartman token", func() {
			cartmanAuthorizer.On("ExtractActingTuid", request).Return(staffUserID, nil)
			tuid, err := gatingHelper.GetOptionalActingTuid(request)

			So(err, ShouldBeNil)
			So(tuid, ShouldEqual, staffUserID)
		})
	})
}

// Constants and boolean literals are not addressable.  We address a parameter
// here to construct pointers to booleans:
// https://golang.org/ref/spec#Address_operators
func create(x bool) *bool {
	return &x
}
