package auth_test

import (
	"testing"
	"time"

	"code.justin.tv/commerce/janus/auth"
	"code.justin.tv/commerce/janus/models"

	jwt "github.com/dgrijalva/jwt-go"
	. "github.com/smartystreets/goconvey/convey"
)

var jwtHelper *auth.JWTHelper

const (
	algorithmKey = "alg"
	typeKey      = "typ"
	typeJWT      = "JWT"
)

func setupJWTHelper() {
	jwtHelper = &auth.JWTHelper{}
}

func TestJWTGetToken(t *testing.T) {
	Convey("When calling GetTokenString with HS256 signing algorithm", t, func() {
		setupJWTHelper()
		inputClaims := models.GetLinkedStoreForTwitchUserClaims{
			TwitchUserId:     "mockTUID",
			MarketplaceId:    "mockMarketplace",
			Context:          "mockContext",
			RequireStoreData: "true",
			StandardClaims: jwt.StandardClaims{
				ExpiresAt: time.Now().Add(time.Minute * 30).Unix(),
				IssuedAt:  time.Now().Unix(),
			},
		}
		key := []byte("foo")

		tokenString, err := jwtHelper.GetSignedTokenString(inputClaims, key, auth.HS256)
		So(err, ShouldBeNil)

		token, err := jwt.ParseWithClaims(tokenString, &models.GetLinkedStoreForTwitchUserClaims{}, func(token *jwt.Token) (interface{}, error) {
			return []byte("foo"), nil
		})
		So(err, ShouldBeNil)

		claims, ok := token.Claims.(*models.GetLinkedStoreForTwitchUserClaims)
		So(ok, ShouldBeTrue)
		So(token.Valid, ShouldBeTrue)
		So(claims.TwitchUserId, ShouldEqual, "mockTUID")
		So(claims.MarketplaceId, ShouldEqual, "mockMarketplace")
		So(claims.Context, ShouldEqual, "mockContext")
		So(claims.RequireStoreData, ShouldEqual, "true")

		now := time.Now().Unix()
		So(claims.StandardClaims.ExpiresAt, ShouldBeGreaterThan, now)
		So(claims.StandardClaims.IssuedAt, ShouldBeLessThanOrEqualTo, now)
	})

	Convey("When calling GetTokenString with unimplemented signing algorithm", t, func() {
		setupJWTHelper()
		inputClaims := jwt.MapClaims{
			"foo": "bar",
		}
		key := []byte("foo")

		tokenString, err := jwtHelper.GetSignedTokenString(inputClaims, key, "fake algorithm")
		So(tokenString, ShouldBeEmpty)
		So(err, ShouldNotBeNil)
	})
}
