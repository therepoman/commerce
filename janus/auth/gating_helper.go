package auth

import (
	"context"
	"net/http"
	"strconv"

	"strings"

	"code.justin.tv/common/goauthorization"
	"code.justin.tv/web/users-service/client"
	log "github.com/sirupsen/logrus"
)

// GatingHelper interface for gating checks
type GatingHelper struct {
	UsersServiceClient  usersservice.Client
	CartmanAuthorizer   ICartmanAuthorizer
	FuelWhitelist       map[string]bool
	StaffGamesWhitelist map[string]bool
}

// IGatingHelper interface for gating checks
type IGatingHelper interface {
	CanSeeHiddenInformation(r *http.Request, userID string, gameID string) bool
	GetOptionalActingTuid(r *http.Request) (string, error)
	IsAdminOrSubAdmin(r *http.Request, userID string) (bool, error)
	IsAdminOrSubAdminWrapper(r *http.Request, tuid string) AuthChainFunction
	IsWhitelisted(tuid string) bool
	UserIsId(r *http.Request, userID string) (bool, error)
	UserIsIdWrapper(r *http.Request, id string) AuthChainFunction
	UserIsChannel(r *http.Request, channelID string) (bool, error)
	CheckAuthChain(authChainFunctions []AuthChainFunction) (bool, []error)
}

// CanSeeHiddenInformation checks if the user can see hidden information on the server.
// First check on the whitelist (always see everything).
// Then check if game is visible from staff (not in the whitelist)
// Then validates the token to check if access is granted
func (gatingHelper *GatingHelper) CanSeeHiddenInformation(r *http.Request, userID string, gameID string) bool {
	if len(userID) == 0 {
		return false
	}

	// Check whitelist
	_, isWhitelisted := gatingHelper.FuelWhitelist[userID]
	if isWhitelisted {
		log.Infof("Whitelist check successful for user: %s", userID)
		return true
	}

	_, isStaffWhitelisted := gatingHelper.StaffGamesWhitelist[strings.ToLower(gameID)]
	if !isStaffWhitelisted {
		return false
	}

	userIDint, err := strconv.Atoi(userID)
	if err != nil {
		log.Warnf("Could not convert ID for Twitch user %s", userID)
		return false
	}

	// Legacy method: Check capabilities in Cartman token
	// We still need to support this as long as Visage REST endpoints are using
	// Cartman capabilities
	capabilities := goauthorization.CapabilityClaims{
		"view_all_details": goauthorization.CapabilityClaim{
			"user_id": userIDint,
		},
	}
	err = gatingHelper.CartmanAuthorizer.AuthorizeCartmanToken(r, &capabilities)
	if err == nil {
		log.Infof("Staff check successful for user: %s", userID)
		return true
	}

	// Future method: Check users-service to see if the acting user is an Admin
	actingTuid, err := gatingHelper.CartmanAuthorizer.ExtractActingTuid(r)
	if err != nil {
		return false
	}
	adminOrSubadmin, err := gatingHelper.IsAdminOrSubAdmin(r, actingTuid)
	if err != nil {
		log.Warnf("Failure performing Admin check for %s.  Failing closed: %s", actingTuid, err)
		return false
	}

	return adminOrSubadmin
}

// IsAdminOrSubAdmin emulates the Cartman authorization function is_admin_or_subadmin
// by calling users-service for the user data.
func (gatingHelper *GatingHelper) IsAdminOrSubAdmin(r *http.Request, tuid string) (bool, error) {
	if len(tuid) == 0 {
		return false, nil
	}

	userProperties, err := gatingHelper.UsersServiceClient.GetUserByID(context.Background(), tuid, nil)
	if err != nil {
		return false, err
	}

	adminOrSubadmin :=
		((userProperties.Admin != nil) && (*userProperties.Admin)) ||
			((userProperties.Subadmin != nil) && (*userProperties.Subadmin))

	return adminOrSubadmin, nil
}

func (gatingHelper *GatingHelper) IsAdminOrSubAdminWrapper(r *http.Request, tuid string) AuthChainFunction {
	return func() (bool, error) {
		return gatingHelper.IsAdminOrSubAdmin(r, tuid)
	}
}

// UserIsId emulates the Cartman authorization function user_is_id
func (gatingHelper *GatingHelper) UserIsId(r *http.Request, id string) (bool, error) {
	actingTuid, err := gatingHelper.CartmanAuthorizer.ExtractActingTuid(r)
	// Note we're also propagating ErrNoAuthorizationToken to let the caller
	// decide what to do
	if err != nil {
		return false, err
	}
	return actingTuid == id, nil
}

func (gatingHelper *GatingHelper) UserIsIdWrapper(r *http.Request, id string) AuthChainFunction {
	return func() (bool, error) {
		return gatingHelper.UserIsId(r, id)
	}
}

// UserIsChannel is effectively an alias for UserIsId, and emulates the Cartman
// authorization function user_is_channel
func (gatingHelper *GatingHelper) UserIsChannel(r *http.Request, channel string) (bool, error) {
	return gatingHelper.UserIsId(r, channel)
}

// CheckAuthChain takes multiple AuthChainFunctions and resolves them in order.
// When one returns true, we return true immediately with no errors.
//
// If all return false and/or errors, false is returned with an error array.
func (gatingHelper *GatingHelper) CheckAuthChain(authChainFunctions []AuthChainFunction) (bool, []error) {
	var errors []error = []error{}
	for _, authChainFunction := range authChainFunctions {
		authResult, err := authChainFunction()
		if authResult {
			return true, []error{}
		}
		if err != nil {
			errors = append(errors, err)
		}
	}
	return false, errors
}

type AuthChainFunction func() (bool, error)

// IsWhitelisted returns whether a particular tuid exists in the janus whitelist
func (gatingHelper *GatingHelper) IsWhitelisted(tuid string) bool {
	if len(tuid) == 0 {
		return false
	}

	// Check whitelist
	_, isWhitelisted := gatingHelper.FuelWhitelist[tuid]
	if isWhitelisted {
		log.Infof("Whitelist check successful for user: %s", tuid)
		return true
	}

	return false
}

// GetOptionalActingTuid returns the acting tuid for the request. If there is an error or the request
// is not authenticated for a partiucular user, then return the empty string
func (gatingHelper *GatingHelper) GetOptionalActingTuid(r *http.Request) (string, error) {
	tuid, err := gatingHelper.CartmanAuthorizer.ExtractActingTuid(r)
	if err == goauthorization.ErrNoAuthorizationToken {
		return "", nil
	} else if err != nil {
		return "", err
	}
	return tuid, nil
}
