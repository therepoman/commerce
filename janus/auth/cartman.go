package auth

import (
	"fmt"
	"net/http"

	"code.justin.tv/common/goauthorization"
	"code.justin.tv/common/jwt"
)

const (
	cartmanAudience = "code.justin.tv/web/cartman"
	issuer          = "code.justin.tv/web/cartman"

	janusAudience          = "code.justin.tv/commerce/janus"
	janusAudienceAlgorithm = "RS256"
)

var errMissingAuthenticationToken = goauthorization.ErrNoAuthorizationToken

// cartmanScopeDecoders maps from RFC 7518 Algorithm (i.e. "HS512" or "ES256")
// to goauthorization.Decoder implementation for the "code.justin.tv/web/cartman"
// audience
var cartmanScopeDecoders map[string]goauthorization.Decoder = make(map[string]goauthorization.Decoder)

// janusScopeDecoder uses the Cartman authorization audience scope
// "code.justin.tv/common/janus" to verify RS256 Cartman tokens.  At the time of
// writing, this is only used by the REST API in Visage.
//
// It is possible to adjust the Janus Cartman capabilities to advertise the same
// "code.justin.tv/web/cartman" audience.  However, during the interm, Janus
// must support both types of auth for a migration.
//
// RFC 7519 also supports multiple audience values, but at the time of writing
// Cartman capability parser only supports single value audience values.
var janusScopeDecoder goauthorization.Decoder

// CartmanAuthorizer structure for authorizing cartman tokens
type CartmanAuthorizer struct {
}

// ICartmanAuthorizer interface for authorizing cartman tokens
type ICartmanAuthorizer interface {
	AuthorizeCartmanToken(r *http.Request, capabilities *goauthorization.CapabilityClaims) error
	ExtractActingTuid(r *http.Request) (string, error)
}

// InitializeDecoder initializes the goauthorization decoder with the configured keys and settings. This
// must be called prior to attempting to call AuthorizeCartmanToken()
func InitializeDecoders(sandstorm ISandstormManager, algorithmToSecretName map[string]string) error {
	// Initialize janusAudienceAlgorithm Decoder for the janusAudience audience
	if rs256SecretName, ok := algorithmToSecretName[janusAudienceAlgorithm]; !ok {
		return fmt.Errorf("%s configuration is required for %s audience tokens", janusAudienceAlgorithm, janusAudience)
	} else if secret, err := sandstorm.Get(rs256SecretName); err != nil {
		return err
	} else if decoder, err := goauthorization.NewDecoder(janusAudienceAlgorithm, janusAudience, issuer, secret.Plaintext); err != nil {
		return err
	} else {
		janusScopeDecoder = decoder
	}

	// Initialize Decoders for the "code.justin.tv/web/cartman" audience
	for algorithm, secretName := range algorithmToSecretName {
		secret, err := sandstorm.Get(secretName)
		if err != nil {
			return err
		}

		if cartmanScopeDecoders[algorithm], err = goauthorization.NewDecoder(algorithm, cartmanAudience, issuer, secret.Plaintext); err != nil {
			return err
		}
	}

	return nil
}

// AuthorizeCartmanToken validates the input request (and its attached JWT token) against expected
// capabilities for Janus. If the the token is successfully validated, a nil error is returned.
func (cartmanAuthorizer *CartmanAuthorizer) AuthorizeCartmanToken(r *http.Request, capabilities *goauthorization.CapabilityClaims) error {
	decoder, err := cartmanAuthorizer.getDecoder(r)
	if err != nil {
		return err
	}

	token, err := decoder.ParseToken(r)
	if err != nil {
		return err
	}

	return decoder.Validate(token, *capabilities)
}

// ExtractActingTuid validates the input request (and its attached JWT token)
// and returns the tuid associated with the Cartman token
func (cartmanAuthorizer *CartmanAuthorizer) ExtractActingTuid(r *http.Request) (string, error) {
	decoder, err := cartmanAuthorizer.getDecoder(r)
	if err != nil {
		return "", err
	}

	token, err := decoder.ParseToken(r)
	if err != nil {
		return "", err
	}

	err = decoder.Validate(token, goauthorization.CapabilityClaims{})
	if err != nil {
		return "", err
	}

	return token.GetSubject(), nil
}

// getDecoder finds the correct decoder for the given request's Cartman token,
// or an error if there is no suitible decoder
func (cartmanAuthorizer *CartmanAuthorizer) getDecoder(r *http.Request) (goauthorization.Decoder, error) {
	var header jwt.Header
	var claims goauthorization.TokenClaims

	serialized := r.Header.Get("Twitch-Authorization")
	if serialized == "" {
		return nil, errMissingAuthenticationToken
	}

	var f jwt.Fragment
	var err error
	if f, err = jwt.Parse([]byte(serialized)); err != nil {
		return nil, err
	}

	if err = f.Decode(&header, &claims); err != nil {
		return nil, err
	}

	// Special case Janus specific audience.  See comment around janusScopeDecoder
	for _, audience := range claims.Audience {
		if audience == janusAudience {
			return janusScopeDecoder, nil
		}
	}

	// Otherwise, find a Cartman audience decoder
	if decoder, ok := cartmanScopeDecoders[header.Algorithm]; ok {
		return decoder, nil
	}

	return nil, fmt.Errorf("The algorithm %s is not supported", header.Algorithm)
}
