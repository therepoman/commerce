package metrics

import (
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
	"github.com/aws/aws-sdk-go/service/cloudwatch/cloudwatchiface"
	log "github.com/sirupsen/logrus"
)

const (
	janusNamespace string = "janus"
	unitCount             = "Count"
	unitSeconds           = "Seconds"
)

// Config for our metrics flusher
type Config struct {
	Stage                          string
	AwsRegion                      string
	BufferSize                     int
	BatchSize                      int
	FlushInterval                  time.Duration
	FlushPollCheckDelay            time.Duration
	BufferEmergencyFlushPercentage float64
}

// MetricLogger for the metrics, this object takes the actualmetrics requests
type MetricLogger struct {
	metricsBuffer chan *cloudwatch.MetricDatum
	config        Config
}

// MetricFlusher daemon which is responsible for the go routine that flushes metrics to cloudwatch
type MetricFlusher struct {
	cloudWatch    cloudwatchiface.CloudWatchAPI
	metricsBuffer chan *cloudwatch.MetricDatum
	config        Config
	lastFlushTime time.Time
}

// IMetricLogger interface
type IMetricLogger interface {
	LogDurationMetric(name string, duration time.Duration)
	LogDurationSinceMetric(name string, timeOfEvent time.Time)
	LogCountMetric(name string, value int)
}

// IMetricFlusher interface
type IMetricFlusher interface {
	FlushMetrics()
	FlushMetricsAtInterval()
	ShouldFlush() bool
}

var developmentStage = "development"

// New creates a metrics flusher/logger creator
func New(config Config) (IMetricLogger, IMetricFlusher) {
	awsConfig := &aws.Config{
		Region: aws.String(config.AwsRegion),
	}
	client := cloudwatch.New(session.New(), awsConfig)
	return NewFromCloudwatchClient(config, client)
}

// NewFromCloudwatchClient is a Metrics constructor
func NewFromCloudwatchClient(config Config, cloudwatchClient cloudwatchiface.CloudWatchAPI) (IMetricLogger, IMetricFlusher) {
	metricsBufferChannel := make(chan *cloudwatch.MetricDatum, config.BufferSize)

	logger := &MetricLogger{
		config:        config,
		metricsBuffer: metricsBufferChannel,
	}

	flusher := &MetricFlusher{
		cloudWatch:    cloudwatchClient,
		metricsBuffer: metricsBufferChannel,
		config:        config,
		lastFlushTime: time.Now(),
	}

	return logger, flusher
}

// LogDurationSinceMetric logs the amount of time that has occured since the passed in time
func (logger *MetricLogger) LogDurationSinceMetric(name string, timeOfEvent time.Time) {
	delta := time.Since(timeOfEvent)
	logger.LogDurationMetric(name, delta)
}

// LogDurationMetric logs the amount of time that is passed in
func (logger *MetricLogger) LogDurationMetric(name string, duration time.Duration) {
	logger.logMetric(name, unitSeconds, duration.Seconds())
}

// LogCountMetric logs the count passed in for the associated metric name
func (logger *MetricLogger) LogCountMetric(name string, value int) {
	logger.logMetric(name, unitCount, float64(value))
}

func (logger *MetricLogger) logMetric(metricName string, metricUnit string, metricValue float64) {
	if logger.config.Stage == developmentStage {
		log.Infof("Not logging metric in CloudWatch (development stage) - %v : %v", metricName, metricValue)
		return
	}

	metricDatum := &cloudwatch.MetricDatum{
		MetricName: aws.String(metricName),
		Dimensions: []*cloudwatch.Dimension{
			{
				Name:  aws.String("stage"),
				Value: aws.String(logger.config.Stage),
			},
		},
		Timestamp: aws.Time(time.Now().UTC()),
		Unit:      aws.String(metricUnit),
		Value:     aws.Float64(metricValue),
	}
	if len(logger.metricsBuffer) < logger.config.BufferSize {
		logger.metricsBuffer <- metricDatum
	} else {
		log.Errorf("Metrics buffer at capacity. Additional metrics will be dropped")
	}
}

// FlushMetricsAtInterval flushes the metrics to cloudwatch
func (flusher *MetricFlusher) FlushMetricsAtInterval() {
	if flusher.config.Stage == developmentStage {
		log.Info("Not starting routine to flush metrics since application in development stage")
		return
	}

	flusher.lastFlushTime = time.Now()
	for {
		// Start the polling delay timer
		timer := time.After(flusher.config.FlushPollCheckDelay)

		if flusher.ShouldFlush() {
			flusher.FlushMetrics()
			flusher.lastFlushTime = time.Now()
		}

		// Wait until the polling delay timer expires
		<-timer
	}
}

// ShouldFlush determines whether or not the dameon should flush
func (flusher *MetricFlusher) ShouldFlush() bool {
	return flusher.isNextFlushInterval() || flusher.isBufferApproachingCapacity()
}

func (flusher *MetricFlusher) isNextFlushInterval() bool {
	timeSinceLastFlush := time.Now().Sub(flusher.lastFlushTime)
	return timeSinceLastFlush > flusher.config.FlushInterval
}

func (flusher *MetricFlusher) isBufferApproachingCapacity() bool {
	bufferUsage := float64(len(flusher.metricsBuffer)) / float64(flusher.config.BufferSize)
	approachingCapacity := bufferUsage >= flusher.config.BufferEmergencyFlushPercentage
	if approachingCapacity {
		log.Warningf("The metrics buffer is approaching capacity. Should execute emergency flush. Current buffer usage: %f", bufferUsage)
	}
	return approachingCapacity
}

// FlushMetrics determines whether the metrics object should flush
func (flusher *MetricFlusher) FlushMetrics() {
	numMetricsToFlush := len(flusher.metricsBuffer)

	numSucceeded, numFailed := 0, 0
	metricBatch := make([]*cloudwatch.MetricDatum, 0, flusher.config.BatchSize)
	for i := 0; i < numMetricsToFlush; i++ {

		metric := <-flusher.metricsBuffer
		metricBatch = append(metricBatch, metric)

		// Send the batch if it is at capacity or this is the last metric
		if len(metricBatch) >= flusher.config.BatchSize || i == (numMetricsToFlush-1) {
			err := flusher.postMetricBatch(metricBatch)
			if err != nil {
				log.Warningf("Failed to send %d metrics while communicating with cloudwatch. Error was %v", len(metricBatch), err)
				numFailed += len(metricBatch)
			} else {
				numSucceeded += len(metricBatch)
			}
			metricBatch = make([]*cloudwatch.MetricDatum, 0, flusher.config.BatchSize)
		}
	}

	if numFailed > 0 {
		log.Errorf("Finished flushing metrics, but encountered some errors. Succeeded: [%d], Failed: [%d]", numSucceeded, numFailed)
	}
}

func (flusher *MetricFlusher) postMetricBatch(metricsBatch []*cloudwatch.MetricDatum) error {
	request := &cloudwatch.PutMetricDataInput{
		MetricData: metricsBatch,
		Namespace:  aws.String(janusNamespace),
	}
	_, err := flusher.cloudWatch.PutMetricData(request)
	return err
}
