package s3

import (
	log "github.com/sirupsen/logrus"

	"bytes"
	"encoding/csv"
	"io"
)

// ParseDataFromCSV convert data from io.ReadCloser to [][]string format
func ParseDataFromCSV(rawData io.ReadCloser) ([][]string, error) {
	r := csv.NewReader(rawData)

	records, err := r.ReadAll()
	if err != nil {
		log.Errorf("Error convert CSV data into [][]string, %v", err)
		return nil, err
	}

	return records, nil
}

// ParseDataToBytes convert [][]string to []byte
func ParseDataToBytes(content [][]string) ([]byte, error) {
	var buf bytes.Buffer
	w := csv.NewWriter(&buf)
	err := w.WriteAll(content)
	if err != nil {
		log.Errorf("Error write content into writer, %v", err)
		return nil, err
	}

	return buf.Bytes(), nil
}
