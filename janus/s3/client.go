package s3

import (
	"bytes"
	"io"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	log "github.com/sirupsen/logrus"
)

// ClientConfig the S3 client config
type ClientConfig struct {
	AwsRegion string
}

// Client the S3 client
type Client struct {
	S3           *s3.S3
	ClientConfig *ClientConfig
}

// NewClient the constructor for the S3 client
func NewClient(config *ClientConfig) *Client {
	awsConfig := &aws.Config{
		Region: aws.String(config.AwsRegion),
	}
	s3client := s3.New(session.New(), awsConfig)
	return &Client{
		S3:           s3client,
		ClientConfig: config,
	}
}

// GetObject queries S3
func (client *Client) GetObject(bucketName string, objectKey string) (io.ReadCloser, error) {
	params := &s3.GetObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(objectKey),
	}
	response, err := client.S3.GetObject(params)

	if err != nil {
		log.Errorf("s3.Client: Encountered an error calling S3 GetObject, %v", err)
		return nil, err
	}

	return response.Body, nil
}

// PutObject puts the object in the S3 bucket
func (client *Client) PutObject(bucketName string, objectKey string, content []byte) error {
	params := &s3.PutObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(objectKey),
		Body:   bytes.NewReader(content),
	}

	_, err := client.S3.PutObject(params)
	if err != nil {
		log.Errorf("s3.Client: Encountered an error calling S3 PutObject, %v", err)
		return err
	}

	return nil
}
