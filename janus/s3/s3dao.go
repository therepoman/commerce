package s3

import log "github.com/sirupsen/logrus"

// DAO struct contents S3 client
type DAO struct {
	client *Client
}

// IS3DAO is the interface for S3DAO
type IS3DAO interface {
	Get(bucketName string, objectKey string) ([][]string, error)
	Put(bucketName string, objectKey string, content [][]string) error
}

// NewS3DAO initiates an instance of S3DAO
func NewS3DAO(client *Client) IS3DAO {
	dao := &DAO{
		client: client,
	}
	return dao
}

// Get content from s3 file
func (s3DAO *DAO) Get(bucketName string, objectKey string) ([][]string, error) {
	readCloser, getErr := s3DAO.client.GetObject(bucketName, objectKey)
	if getErr != nil {
		log.Errorf("Error get data from CSV file, %v", getErr)
		return nil, getErr
	}

	result, err := ParseDataFromCSV(readCloser)
	if err != nil {
		log.Errorf("Error convert data from CSV file, %v", err)
		return nil, err
	}

	return result, nil
}

// Put content to s3 file
func (s3DAO *DAO) Put(bucketName string, objectKey string, content [][]string) error {
	bytes, err := ParseDataToBytes(content)
	if err != nil {
		log.Errorf("Error convert data from CSV file, %v", err)
		return err
	}

	putErr := s3DAO.client.PutObject(bucketName, objectKey, bytes)
	if putErr != nil {
		log.Errorf("Error PutObject into s3 bucket, %v", putErr)
		return putErr
	}

	return nil
}
