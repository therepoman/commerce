#!/bin/bash

set -e

if [ -z "$1" ]
  then
    echo "No Cartman Auth Header supplied."
    exit 1
fi
cartmanAuthHeader=$1

if [ -z "$2" ]
  then
    echo "No target TPS specified."
    exit 1
fi
tps=$2

if [ -z "$3" ]
  then
    echo "No Vegeta load test input file specified."
    exit 1
fi
inputFile=$3

if [ -z "$4" ]
  then
    echo "No target time in minutes specified."
    exit 1
fi
lengthInMinutes=$4

n=0
while [ $n -lt $lengthInMinutes ]; do
  echo "authenticating"
  JWT=`curl "https://cartman-elb.prod.us-west2.justin.tv/authorization_token?capabilities=cartman%3A%3Aauthenticate_user&key=hmac.key" -H "$cartmanAuthHeader" | jq '.token' | tr -d '"'`
  n=$((n+1))
  header=`echo "Twitch-Authorization:$JWT"`

  echo "starting $n of $lengthInMinutes vegeta attacks at $tps tps"
  outputFile="$inputFile.$tps_tps.minute_$n.bin"
  vegeta attack -duration=1m -rate=$tps -targets=$inputFile -header=$header > $outputFile

  echo "minute finished, printing report"

  vegeta report -inputs=$outputFile
done
