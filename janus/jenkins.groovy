freeStyleJob('commerce-janus') {
  using 'TEMPLATE-autobuild'
  scm {
    git {
      remote {
        github 'commerce/janus', 'ssh', 'git-aws.internal.justin.tv'
        credentials 'git-aws-read-key'
      }
      clean true
    }
  }
  steps {
    shell 'manta -v -f build.json'
    saveDeployArtifact 'commerce/janus', '.manta'
  }
  publishers {
	reportQuality('commerce/janus', '.manta')
  }
}

freeStyleJob('commerce-janus-deploy') {
  using 'TEMPLATE-deploy-aws'
  steps {
    shell 'courier deploy --repo commerce/janus --dir /opt/twitch/janus'
  }
}
 
