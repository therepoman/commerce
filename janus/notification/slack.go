package notification

import (
	"bytes"
	"fmt"

	userModels "code.justin.tv/web/users-service/models"

	"context"

	"strconv"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/clients"
	"code.justin.tv/web/users-service/client"
)

const (
	baseEmoteURL = "https://static-cdn.jtvnw.net/emoticons/v1/%s/1.0"
	bitsURL      = "https://d3aqoihi2n8ty8.cloudfront.net/actions/cheer/light/animated/1/4.gif"
)

// SlackNotifier sends purchase notications to Slack
type SlackNotifier struct {
	Slacker            clients.ISlacker
	UsersServiceClient usersservice.Client
}

// NewSlackNotifier creates new notification for Slack
func NewSlackNotifier(slacker clients.ISlacker, userServiceClient usersservice.Client) INotifier {
	return &SlackNotifier{
		Slacker:            slacker,
		UsersServiceClient: userServiceClient,
	}
}

// Notify sends a purhcase notification to Slack
func (sn *SlackNotifier) Notify(notification janus.ChatNotification, message string) error {
	filter := &userModels.FilterParams{
		IDs: []string{notification.ChannelID, notification.UserID},
	}

	resp, err := sn.UsersServiceClient.GetUsers(context.Background(), filter, nil)
	if err != nil {
		return fmt.Errorf("Error calling Users Service: %s", err)
	}

	idToLogin := make(map[string]string)
	if resp != nil {
		for _, userResp := range resp.Results {
			if userResp != nil && userResp.Login != nil {
				idToLogin[userResp.ID] = *userResp.Login
			}
		}
	}

	channelLogin, hasChannelLogin := idToLogin[notification.ChannelID]
	userLogin, hasUserLogin := idToLogin[notification.UserID]

	var textBuffer bytes.Buffer
	textBuffer.WriteString("```")
	if hasChannelLogin && hasUserLogin {
		textBuffer.WriteString(fmt.Sprintf("%s shared a purchase of %s in %s's channel ( https://twitch.tv/%s ) \n\n", userLogin, notification.Title, channelLogin, channelLogin))
	}
	textBuffer.WriteString(fmt.Sprintf("%s", message))
	textBuffer.WriteString("```")

	attachments, err := buildAttachments(textBuffer.String(), notification)
	if err != nil {
		return fmt.Errorf("Error building attachments: %s", err)
	}

	msg := clients.SlackMsg{
		Attachments: attachments,
	}
	err = sn.Slacker.Post(msg)
	if err != nil {
		return fmt.Errorf("Error posting message to slack: %s", err)
	}
	return nil
}

func buildAttachments(primaryMessage string, notification janus.ChatNotification) ([]clients.SlackAttachment, error) {
	// First attachment is the purchased item
	attachments := []clients.SlackAttachment{
		{
			Fallback:   primaryMessage,
			Text:       primaryMessage,
			ThumbURL:   notification.ImageURL,
			MarkdownIn: []string{"text"},
		},
	}

	attachments = append(attachments, buildBadgeAttachments(notification.Contents.Badges)...)
	attachments = append(attachments, buildEmoteAttachments(notification.Contents.EmoteSets)...)
	bitsAttachments, err := buildBitsAttachments(notification.Contents.Bits)
	if err != nil {
		return attachments, err
	}
	attachments = append(attachments, bitsAttachments...)
	attachments = append(attachments, buildInGameContentAttachments(notification.Contents.InGameContent)...)

	return attachments, nil
}

func buildBadgeAttachments(badges []janus.Badge) []clients.SlackAttachment {
	badgeAttachments := []clients.SlackAttachment{}

	for _, badge := range badges {
		badgeText := fmt.Sprintf("Badge: %s", badge.Title)
		newAttachment := clients.SlackAttachment{
			Fallback:   badgeText,
			Footer:     badgeText,
			FooterIcon: badge.ImageURL1X,
		}
		badgeAttachments = append(badgeAttachments, newAttachment)
	}
	return badgeAttachments
}

func buildEmoteAttachments(emoteSets map[string][]janus.Emoticon) []clients.SlackAttachment {
	emoteAttachments := []clients.SlackAttachment{}

	for _, emoteSet := range emoteSets {
		for _, emote := range emoteSet {
			emoteText := fmt.Sprintf("Emote: %s", emote.Code)
			newAttachment := clients.SlackAttachment{
				Fallback:   emoteText,
				Footer:     emoteText,
				FooterIcon: fmt.Sprintf(baseEmoteURL, emote.ID),
			}
			emoteAttachments = append(emoteAttachments, newAttachment)
		}
	}
	return emoteAttachments
}

func buildBitsAttachments(bitsContents []janus.Bits) ([]clients.SlackAttachment, error) {
	bitsAttachments := []clients.SlackAttachment{}
	if len(bitsContents) <= 0 {
		return bitsAttachments, nil
	}

	bitsCount := 0
	for _, bits := range bitsContents {
		quantityInt, err := strconv.Atoi(bits.Quantity)
		if err != nil {
			return bitsAttachments, fmt.Errorf("Error parsing integer from string. Error: %s, Value: %s", err, bits.Quantity)
		}

		bitsCount += quantityInt
	}

	bitsText := fmt.Sprintf("Bits: %s", strconv.Itoa(bitsCount))
	bitsAttachment := clients.SlackAttachment{
		Fallback:   bitsText,
		Footer:     bitsText,
		FooterIcon: bitsURL,
	}
	bitsAttachments = append(bitsAttachments, bitsAttachment)

	return bitsAttachments, nil
}

func buildInGameContentAttachments(igcList []janus.InGameContentNotification) []clients.SlackAttachment {
	igcAttachments := []clients.SlackAttachment{}

	for _, igc := range igcList {
		igcText := fmt.Sprintf("IGC: %s", igc.Title)
		igcAttachment := clients.SlackAttachment{
			Fallback:   igcText,
			Footer:     igcText,
			FooterIcon: igc.BoxArtURL,
		}
		igcAttachments = append(igcAttachments, igcAttachment)
	}

	return igcAttachments
}
