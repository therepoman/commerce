package notification

import (
	"fmt"
	"strconv"
	"strings"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/clients"
)

const (
	purchaseTMIMsgID = "purchase"
	channelIDKey     = "channelID"
	userIDKey        = "userID"
	asinKey          = "asin"
	titleKey         = "title"
	imageURLKey      = "imageURL"
	crateCountKey    = "crateCount"

	separator = ","

	emoticonsKey     = "emoticons"
	badgesKey        = "badges"
	bitsKey          = "bits"
	inGameContentKey = "inGameContent"
)

// ChatNotifier struct for the TMI client
type ChatNotifier struct {
	TMIClient clients.ITMIClient
}

// NewChatNotifier creates a new TMI client
func NewChatNotifier(tmiClient clients.ITMIClient) INotifier {
	return &ChatNotifier{
		TMIClient: tmiClient,
	}
}

// Notify sends purchase notifications to TMI
func (cn *ChatNotifier) Notify(notification janus.ChatNotification, message string) error {
	numericUserID, err := strconv.Atoi(notification.UserID)
	if err != nil {
		return err
	}

	numericChannelID, err := strconv.Atoi(notification.ChannelID)
	if err != nil {
		return err
	}

	params := clients.UserNoticeParams{
		SenderUserID:    numericUserID,
		TargetChannelID: numericChannelID,
		MsgID:           purchaseTMIMsgID,
		MsgParams: []clients.UserNoticeMsgParam{
			{
				Key:   channelIDKey,
				Value: notification.ChannelID,
			},
			{
				Key:   userIDKey,
				Value: notification.UserID,
			},
			{
				Key:   asinKey,
				Value: notification.ASIN,
			},
			{
				Key:   titleKey,
				Value: notification.Title,
			},
			{
				Key:   imageURLKey,
				Value: notification.ImageURL,
			},
			{
				Key:   crateCountKey,
				Value: strconv.Itoa(notification.Contents.CrateCount),
			},
		},
		DefaultSystemBody: fmt.Sprintf("Purchased %s in channel.", notification.Title),
		Body:              message,
	}

	emoticons := serializeEmoticons(notification.Contents.EmoteSets)
	if emoticons != "" {
		params.MsgParams = append(params.MsgParams, clients.UserNoticeMsgParam{
			Key:   emoticonsKey,
			Value: emoticons,
		})
	}

	badges := serializeBadges(notification.Contents.Badges)
	if badges != "" {
		params.MsgParams = append(params.MsgParams, clients.UserNoticeMsgParam{
			Key:   badgesKey,
			Value: badges,
		})
	}

	bits, err := serializeBits(notification.Contents.Bits)
	if err != nil {
		return err
	}
	if bits != "0" {
		params.MsgParams = append(params.MsgParams, clients.UserNoticeMsgParam{
			Key:   bitsKey,
			Value: bits,
		})
	}

	inGameContent := serializeInGameContent(notification.Contents.InGameContent)
	if inGameContent != "" {
		params.MsgParams = append(params.MsgParams, clients.UserNoticeMsgParam{
			Key:   inGameContentKey,
			Value: inGameContent,
		})
	}

	return cn.TMIClient.SendUserNotice(params)
}

// Convert Emoticon map into a serialized, comma separated string of the list of emote Ids
func serializeEmoticons(emoteSets map[string][]janus.Emoticon) string {
	emoteIds := []string{}
	for _, emoteSet := range emoteSets {
		for _, emote := range emoteSet {
			emoteIds = append(emoteIds, emote.ID)
		}
	}
	return serializeSlice(emoteIds)
}

// Convert Badge list into a serialized, comma separated string of the list of badge urls
func serializeBadges(badges []janus.Badge) string {
	badgeUrls := []string{}
	for _, badge := range badges {
		badgeUrls = append(badgeUrls, badge.ImageURL2X)
	}

	return serializeSlice(badgeUrls)
}

// Convert Bits list into a serialized, aggregated total of bits from all events
func serializeBits(bitsList []janus.Bits) (string, error) {
	bitsCount := 0
	for _, bits := range bitsList {
		quantityInt, err := strconv.Atoi(bits.Quantity)
		if err != nil {
			return "", fmt.Errorf("Error parsing integer from string. Error: %s, Value: %s", err, bits.Quantity)
		}

		bitsCount += quantityInt
	}

	return strconv.Itoa(bitsCount), nil
}

func serializeInGameContent(igcList []janus.InGameContentNotification) string {
	igcUrls := []string{}
	for _, igc := range igcList {
		igcUrls = append(igcUrls, igc.BoxArtURL)
	}

	return serializeSlice(igcUrls)
}

func serializeSlice(slice []string) string {
	return strings.Join(slice, separator)
}
