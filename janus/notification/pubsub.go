package notification

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/janus/models"
	"code.justin.tv/web/users-service/client"

	"errors"

	zumaAPI "code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/commerce/janus/clients"

	"strconv"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/payouts"
)

type PubSubNotifier struct {
	PayoutsEligibilityChecker payouts.IPayoutsEligibilityChecker
	PubSubClient              clients.IPubSubWrapper
	UsersServiceClient        usersservice.Client
	ZumaClient                clients.IZumaWrapper
}

func NewPubSubNotifier(payoutsEligibilityChecker payouts.IPayoutsEligibilityChecker, pubsub clients.IPubSubWrapper, userService usersservice.Client, zuma clients.IZumaWrapper) INotifier {
	return &PubSubNotifier{
		PayoutsEligibilityChecker: payoutsEligibilityChecker,
		PubSubClient:              pubsub,
		UsersServiceClient:        userService,
		ZumaClient:                zuma,
	}
}

func (psn *PubSubNotifier) Notify(notification janus.ChatNotification, message string) error {
	userInfo, err := psn.UsersServiceClient.GetUserByID(context.Background(), notification.UserID, nil)
	if err != nil {
		return fmt.Errorf("Error fetching user info for user: %s, error: %v", notification.UserID, err)
	}
	if userInfo.Login == nil {
		return fmt.Errorf("Could not determine username: %s", notification.UserID)
	}
	if userInfo.Displayname == nil {
		return fmt.Errorf("Could not determine user display name: %s", notification.UserID)
	}

	channelInfo, err := psn.UsersServiceClient.GetUserByID(context.Background(), notification.ChannelID, nil)
	if err != nil {
		return fmt.Errorf("Error fetching channel info for channel: %s, error: %v", notification.ChannelID, err)
	}

	if channelInfo.Login == nil {
		return fmt.Errorf("Could not determine channel login: %s", notification.ChannelID)
	}

	shareMessage, err := psn.getShareMessage(notification.ChannelID, notification.UserID, message)
	if err != nil {
		return err
	}

	// Check if user is eligible for fuel game commerce payout
	isPayoutEligible, err := psn.PayoutsEligibilityChecker.GetGameCommercePayoutEligibility(notification.ChannelID)
	if err != nil {
		return fmt.Errorf("Could not determine payout eligibility: %v", err)
	}

	pubsubMessage := models.PubSubGameCommerceShareMessage{
		ChannelID:       notification.ChannelID,
		ChannelName:     *channelInfo.Login,
		UserID:          notification.UserID,
		UserDisplayName: *userInfo.Displayname,
		UserName:        *userInfo.Login,
		Time:            time.Now(),
		ItemImageURL:    notification.ImageURL,
		ItemTitle:       notification.Title,
		ShareMessage:    shareMessage,
		SupportsChannel: isPayoutEligible,
	}

	messageJSON, err := json.Marshal(pubsubMessage)
	if err != nil {
		msg := fmt.Sprintf("Error marshalling message: %v", err)
		return errors.New(msg)
	}

	topic := fmt.Sprintf("%s.%s", clients.ChannelCommerceEventsTopicPrefix, notification.ChannelID)
	if err := psn.PubSubClient.Publish(context.Background(), []string{topic}, clients.ChannelCommerceEventMessageType, messageJSON, nil); err != nil {
		return fmt.Errorf("Could not send PubSub message for purchase notification to channel: %s, error: %v", notification.ChannelID, err)
	}
	return nil
}

func (psn *PubSubNotifier) getShareMessage(channelID string, userID string, message string) (models.ShareMessage, error) {
	if "" == message {
		return models.ShareMessage{}, nil
	}

	zumaRequest := zumaAPI.ParseMessageRequest{
		ChannelID: channelID,
		UserID:    userID,
		Body:      message,
	}
	zumaResponse, err := psn.ZumaClient.ParseMessage(context.Background(), zumaRequest, nil)
	if err != nil {
		return models.ShareMessage{}, fmt.Errorf("Error parsing message from Zuma: %+v", err)
	}

	if zumaResponse.Message == nil {
		return models.ShareMessage{}, fmt.Errorf("Zuma indicated not to send the message %s, sent by user %s, in channel %s", message, userID, channelID)
	}

	var emotes []models.EmoteInfo
	for _, emote := range zumaResponse.Message.Emoticons {
		// We convert emoteID to int for consistency with other 3rd party APIs
		emoteID, err := strconv.Atoi(emote.ID)
		if err != nil {
			return models.ShareMessage{}, fmt.Errorf("Could not convert emote ID %s to int %v", emote.ID, err.Error())
		}
		emoteInfo := models.EmoteInfo{
			Start:   emote.Begin,
			End:     emote.End,
			EmoteID: emoteID,
		}
		emotes = append(emotes, emoteInfo)
	}
	return models.ShareMessage{
		Message: zumaResponse.Message.Body,
		Emotes:  emotes,
	}, nil
}
