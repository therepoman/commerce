package notification_test

import (
	"testing"

	"code.justin.tv/commerce/janus/mocks"

	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/notification"
	"code.justin.tv/web/users-service/models"
	"github.com/pkg/errors"
)

func TestSlackNotify(t *testing.T) {
	Convey("Test Notify", t, func() {
		slacker := new(mocks.ISlacker)
		userService := new(mocks.UsersServiceClient)
		slackNotifier := notification.NewSlackNotifier(slacker, userService)
		chatNotification := janus.ChatNotification{}

		Convey("with user service call failing", func() {
			userService.On("GetUsers", Anything, Anything, Anything).Return(nil, errors.New("test error"))

			err := slackNotifier.Notify(chatNotification, "foo")
			So(err, ShouldNotBeNil)
		})
		Convey("with user service call succeeding", func() {
			resp := &models.PropertiesResult{}
			userService.On("GetUsers", Anything, Anything, Anything).Return(resp, nil)

			Convey("with invalid bits input", func() {
				chatNotification.Contents.Bits = []janus.Bits{
					{
						Quantity: "notaninteger",
					},
				}
				err := slackNotifier.Notify(chatNotification, "foo")
				So(err, ShouldNotBeNil)
			})

			Convey("with valid bits input", func() {
				Convey("with slack call failing", func() {
					slacker.On("Post", Anything).Return(errors.New("test error"))

					err := slackNotifier.Notify(chatNotification, "foo")
					So(err, ShouldNotBeNil)
				})

				Convey("with slack call succeeding", func() {
					slacker.On("Post", Anything).Return(nil)

					err := slackNotifier.Notify(chatNotification, "foo")
					So(err, ShouldBeNil)
				})
			})
		})
	})
}
