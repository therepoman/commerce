package notification_test

import (
	"testing"

	"code.justin.tv/commerce/janus/mocks"

	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/notification"
	userModels "code.justin.tv/web/users-service/models"
	"github.com/pkg/errors"
)

var (
	userID    = "123"
	channelID = "456"

	displayName = "foo"
	login       = "bar"
)

func TestPubSubNotify(t *testing.T) {
	Convey("Test Notify", t, func() {
		payoutsEligibilityChecker := new(mocks.IPayoutsEligibilityChecker)
		pubsub := new(mocks.IPubSubWrapper)
		userService := new(mocks.UsersServiceClient)
		zuma := new(mocks.IZumaWrapper)

		pubsubNotifier := notification.NewPubSubNotifier(payoutsEligibilityChecker, pubsub, userService, zuma)

		chatNotification := janus.ChatNotification{
			UserID:    userID,
			ChannelID: channelID,
		}

		Convey("with user service call for user failing", func() {
			userService.On("GetUserByID", Anything, userID, Anything).Return(nil, errors.New("test user error"))

			err := pubsubNotifier.Notify(chatNotification, "foo")
			So(err, ShouldNotBeNil)
		})
		Convey("with user service call for user succeeding", func() {
			userResp := &userModels.Properties{}
			userService.On("GetUserByID", Anything, userID, Anything).Return(userResp, nil)

			Convey("with empty login", func() {
				err := pubsubNotifier.Notify(chatNotification, "foo")
				So(err, ShouldNotBeNil)
			})

			Convey("with empty display name", func() {
				userResp.Login = &login

				err := pubsubNotifier.Notify(chatNotification, "foo")
				So(err, ShouldNotBeNil)
			})

			Convey("with valid user info", func() {
				userResp.Login = &login
				userResp.Displayname = &displayName

				Convey("with user service call for channel failing", func() {
					userService.On("GetUserByID", Anything, channelID, Anything).Return(nil, errors.New("test channel error"))

					err := pubsubNotifier.Notify(chatNotification, "foo")
					So(err, ShouldNotBeNil)
				})

				Convey("with user service call for channel succeeding", func() {
					channelResp := &userModels.Properties{}
					userService.On("GetUserByID", Anything, channelID, Anything).Return(channelResp, nil)

					Convey("with empty channel resp", func() {
						err := pubsubNotifier.Notify(chatNotification, "foo")
						So(err, ShouldNotBeNil)
					})

					Convey("with valid channel resp", func() {
						channelResp.Login = &login

						Convey("with zuma call failing", func() {
							zuma.On("ParseMessage", Anything, Anything, Anything).Return(api.ParseMessageResponse{}, errors.New("test zuma error"))
							err := pubsubNotifier.Notify(chatNotification, "foo")
							So(err, ShouldNotBeNil)
						})

						Convey("with zuma call succeeding", func() {
							zumaResp := api.ParseMessageResponse{}

							Convey("with zuma rejecting message", func() {
								zuma.On("ParseMessage", Anything, Anything, Anything).Return(zumaResp, nil)

								err := pubsubNotifier.Notify(chatNotification, "foo")
								So(err, ShouldNotBeNil)
							})

							Convey("with zuma approving message", func() {
								zumaResp.Message = &api.ParseMessageResult{
									Body: "ok",
								}
								zuma.On("ParseMessage", Anything, Anything, Anything).Return(zumaResp, nil)

								Convey("with payouts call failing", func() {
									payoutsEligibilityChecker.On("GetGameCommercePayoutEligibility", Anything).Return(false, errors.New("test error"))
									err := pubsubNotifier.Notify(chatNotification, "foo")
									So(err, ShouldNotBeNil)
								})

								Convey("with payouts call succeding", func() {
									payoutsEligibilityChecker.On("GetGameCommercePayoutEligibility", Anything).Return(true, nil)

									Convey("with pubsub publish failing", func() {
										pubsub.On("Publish", Anything, Anything, Anything, Anything, Anything).Return(errors.New("test pubsub error"))
										err := pubsubNotifier.Notify(chatNotification, "foo")
										So(err, ShouldNotBeNil)
									})

									Convey("with pubsub publish succeeding", func() {
										pubsub.On("Publish", Anything, Anything, Anything, Anything, Anything).Return(nil)
										err := pubsubNotifier.Notify(chatNotification, "foo")
										So(err, ShouldBeNil)
									})
								})
							})
						})
					})
				})
			})
		})
	})
}
