package notification

import "code.justin.tv/commerce/janus/client"

// INotifier is an interface that can be used for sending a Fuel Notification to a destination
// e.g. SlackNotifier implements this interface in order to send the Fuel Notification to the slack channel
type INotifier interface {
	Notify(notification janus.ChatNotification, message string) error
}
