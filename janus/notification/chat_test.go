package notification_test

import (
	"testing"

	"code.justin.tv/commerce/janus/mocks"

	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/janus/client"
	"code.justin.tv/commerce/janus/notification"
	"github.com/pkg/errors"
)

func TestChatNotify(t *testing.T) {
	Convey("Test Notify", t, func() {
		tmiClient := new(mocks.ITMIClient)
		chatNotifier := notification.NewChatNotifier(tmiClient)

		Convey("with invalid userID", func() {
			chatNotification := janus.ChatNotification{
				UserID: "notaninteger",
			}

			err := chatNotifier.Notify(chatNotification, "foo")
			So(err, ShouldNotBeNil)
		})
		Convey("with invalid channelID", func() {
			chatNotification := janus.ChatNotification{
				UserID:    "1",
				ChannelID: "notaninteger",
			}

			err := chatNotifier.Notify(chatNotification, "foo")
			So(err, ShouldNotBeNil)
		})
		Convey("with a valid input notification", func() {

			Convey("with invalid bits quantity", func() {
				chatNotification := janus.ChatNotification{
					UserID:    "1",
					ChannelID: "2",
					Contents: janus.NotificationContents{
						Bits: []janus.Bits{
							{
								Quantity: "notaninteger",
							},
						},
					},
				}

				err := chatNotifier.Notify(chatNotification, "foo")
				So(err, ShouldNotBeNil)
			})

			Convey("with valid bits quantity", func() {
				chatNotification := janus.ChatNotification{
					UserID:    "1",
					ChannelID: "2",
					Contents: janus.NotificationContents{
						Badges: []janus.Badge{
							{
								Title: "mybadge",
							},
						},
						Bits: []janus.Bits{
							{
								Quantity: "1",
							},
						},
						EmoteSets: map[string][]janus.Emoticon{
							"foo": []janus.Emoticon{
								{
									Code: "kappa",
								},
							},
						},
					},
				}

				Convey("with TMI call failing", func() {
					tmiClient.On("SendUserNotice", Anything).Return(errors.New("test error fail"))
					err := chatNotifier.Notify(chatNotification, "foo")
					So(err, ShouldNotBeNil)
				})

				Convey("with TMI call succeeding", func() {
					tmiClient.On("SendUserNotice", Anything).Return(nil)
					err := chatNotifier.Notify(chatNotification, "foo")
					So(err, ShouldBeNil)
				})
			})
		})
	})
}
