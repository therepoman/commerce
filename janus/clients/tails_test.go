package clients_test

import (
	"errors"
	"testing"

	svc "code.justin.tv/commerce/TwitchAmazonIdentityLinkingServiceGoClient/src/tv/justin/tails"
	"code.justin.tv/commerce/janus/clients"
	"code.justin.tv/commerce/janus/coral"
	"code.justin.tv/commerce/janus/mocks"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

var MockedTailsClient *mocks.ITailsClient

var (
	testCOR   = "foo"
	testTrue  = true
	testFalse = false
)

func initTailsClientWrapper(mockedClient *mocks.ITailsClient, mockedLogger *mocks.IMetricLogger) clients.TailsClientWrapper {
	return clients.TailsClientWrapper{
		Client:        mockedClient,
		SWSParams:     coral.SWSParams{SWSEnabled: true},
		RetryStrategy: coral.NewTestingRetryStrategy(),
		MetricsLogger: mockedLogger,
	}
}

func initTailsClientWrapperWithRetryStrategy(mockedClient *mocks.ITailsClient, mockedLogger *mocks.IMetricLogger) clients.TailsClientWrapper {
	return clients.TailsClientWrapper{
		Client:        mockedClient,
		SWSParams:     coral.SWSParams{SWSEnabled: true},
		RetryStrategy: coral.NewDefaultRetryStrategy(),
		MetricsLogger: mockedLogger,
	}
}

func TestGetLinkedAmazonCOR(t *testing.T) {
	Convey("With a linked account", t, func() {
		MockedTailsClient = new(mocks.ITailsClient)
		MockedIMetricLogger = new(mocks.IMetricLogger)

		getResponse := svc.NewGetLinkedAmazonDirectedIdResponse()
		getResponse.SetHasLinkedAccount(&testTrue)
		getResponse.SetCountryOfResidence(&testCOR)

		MockedTailsClient.On("GetLinkedAmazonDirectedId", mock.Anything).Return(getResponse, nil).Once()
		MockedIMetricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()

		tailsClientWrapper := initTailsClientWrapperWithRetryStrategy(MockedTailsClient, MockedIMetricLogger)
		cor, err := tailsClientWrapper.GetLinkedAmazonCOR("userID")

		So(err, ShouldBeNil)
		So(cor, ShouldEqual, testCOR)
	})

	Convey("Without a linked account", t, func() {
		MockedTailsClient = new(mocks.ITailsClient)
		MockedIMetricLogger = new(mocks.IMetricLogger)

		getResponse := svc.NewGetLinkedAmazonDirectedIdResponse()
		getResponse.SetHasLinkedAccount(&testFalse)

		MockedTailsClient.On("GetLinkedAmazonDirectedId", mock.Anything).Return(getResponse, nil).Once()
		MockedIMetricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()

		tailsClientWrapper := initTailsClientWrapperWithRetryStrategy(MockedTailsClient, MockedIMetricLogger)
		cor, err := tailsClientWrapper.GetLinkedAmazonCOR("userID")

		So(err, ShouldNotBeNil)
		So(cor, ShouldBeBlank)
	})

	Convey("Without a linked cor", t, func() {
		MockedTailsClient = new(mocks.ITailsClient)
		MockedIMetricLogger = new(mocks.IMetricLogger)

		getResponse := svc.NewGetLinkedAmazonDirectedIdResponse()
		getResponse.SetHasLinkedAccount(&testTrue)

		MockedTailsClient.On("GetLinkedAmazonDirectedId", mock.Anything).Return(getResponse, nil).Once()
		MockedIMetricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()

		tailsClientWrapper := initTailsClientWrapperWithRetryStrategy(MockedTailsClient, MockedIMetricLogger)
		cor, err := tailsClientWrapper.GetLinkedAmazonCOR("userID")

		So(err, ShouldNotBeNil)
		So(cor, ShouldBeBlank)
	})

	Convey("When Tails returns an error", t, func() {
		MockedTailsClient = new(mocks.ITailsClient)
		MockedIMetricLogger = new(mocks.IMetricLogger)

		MockedTailsClient.On("GetLinkedAmazonDirectedId", mock.Anything).Return(nil, errors.New("tails error"))
		MockedIMetricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()

		tailsClientWrapper := initTailsClientWrapperWithRetryStrategy(MockedTailsClient, MockedIMetricLogger)
		cor, err := tailsClientWrapper.GetLinkedAmazonCOR("userID")

		So(err, ShouldNotBeNil)
		So(cor, ShouldBeBlank)
	})

	Convey("Without a user", t, func() {
		MockedTailsClient = new(mocks.ITailsClient)
		MockedIMetricLogger = new(mocks.IMetricLogger)

		MockedTailsClient.On("GetLinkedAmazonDirectedId", mock.Anything).Return(nil, errors.New("tails error"))
		MockedIMetricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()

		tailsClientWrapper := initTailsClientWrapperWithRetryStrategy(MockedTailsClient, MockedIMetricLogger)
		cor, err := tailsClientWrapper.GetLinkedAmazonCOR("")

		So(err, ShouldNotBeNil)
		So(cor, ShouldBeBlank)
	})
}
