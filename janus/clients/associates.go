package clients

import (
	"net/url"
	"time"

	"code.justin.tv/commerce/janus/associates"
	"code.justin.tv/commerce/janus/auth"
	"code.justin.tv/commerce/janus/metrics"
	"code.justin.tv/commerce/janus/models"
	"code.justin.tv/common/twitchhttp"

	jwt "github.com/dgrijalva/jwt-go"
	log "github.com/sirupsen/logrus"

	"strconv"

	"code.justin.tv/commerce/janus/cache"
	"golang.org/x/net/context"
)

const (
	getLinkedStoreForTwitchUserPath = "/bt/getLinkedStoreForTwitchUser"
	associatesSigningAlgorithm      = "HS256"
	associatesJWTLifespanMinutes    = 30
)

//IAssociatesClientWrapper interface for associates
type IAssociatesClientWrapper interface {
	GetLinkedStoreForTwitchUser(ctx context.Context, tuid string, marketplace string, requiresStoreData bool, context string) (*models.LinkedAssociatesStore, error)
}

// AssociatesClientWrapper structure containing relevant information for the client to call associates
type AssociatesClientWrapper struct {
	AssociatesClient     IAssociatesClient
	JWTHelper            auth.IJWTHelper
	MetricsLogger        metrics.IMetricLogger
	AssociatesSecret     []byte
	LinkedCacheClient    cache.IAssociatesStoreCacheClient
	NotLinkedCacheClient cache.IAssociatesStoreCacheClient
}

//IAssociatesClient interface for chat badges
type IAssociatesClient interface {
	GetLinkedStoreForTwitchUser(ctx context.Context, jwtString string) (*models.LinkedAssociatesStore, error)
}

// AssociatesClientImpl structure containing relevant information for the client to call associates
type AssociatesClientImpl struct {
	twitchhttp.Client
}

// GetLinkedStoreForTwitchUser returns details of the associates store linked with the given twitch user
func (c *AssociatesClientWrapper) GetLinkedStoreForTwitchUser(ctx context.Context, tuid string, marketplace string, requiresStoreData bool, context string) (*models.LinkedAssociatesStore, error) {
	startTime := time.Now()
	defer c.MetricsLogger.LogDurationSinceMetric("associatesClient.GetLinkedStoreForTwitchUser.tuid", startTime)

	isCacheHit, cachedAssociatesStore := c.LinkedCacheClient.Get(tuid, marketplace)
	if isCacheHit {
		return cachedAssociatesStore, nil
	}

	claims := getAssociatesJWTClaims(tuid, marketplace, context, requiresStoreData)
	jwtString, err := c.JWTHelper.GetSignedTokenString(claims, c.AssociatesSecret, auth.HS256)
	if err != nil {
		log.Errorf("Failure creating signed jwt: %s", err)
		return nil, err
	}

	answer, err := c.AssociatesClient.GetLinkedStoreForTwitchUser(ctx, jwtString)

	// Response from Associates gave a 404 so add empty cache entry
	if answer == nil || answer.TUID == "" {
		c.NotLinkedCacheClient.PutStoreInfoNotFound(tuid, marketplace)
	} else if associates.IsStoreStatusRevEnabled(answer.Status) {
		c.LinkedCacheClient.Put(answer)
	} else {
		c.NotLinkedCacheClient.Put(answer)
	}

	return answer, err
}

// GetLinkedStoreForTwitchUser returns details of the associates store linked with the given twitch user
func (c *AssociatesClientImpl) GetLinkedStoreForTwitchUser(ctx context.Context, jwtString string) (*models.LinkedAssociatesStore, error) {
	query := url.Values{}
	query.Set("jwt", jwtString)
	u := url.URL{Path: getLinkedStoreForTwitchUserPath, RawQuery: query.Encode()}
	req, err := c.NewRequest("GET", u.String(), nil)
	if err != nil {
		log.Errorf("Failure creating associates request: %s", err)
		return nil, err
	}

	var getLinkedStoreForTwitchUser models.LinkedAssociatesStore
	resp, err := c.DoJSON(ctx, &getLinkedStoreForTwitchUser, req, twitchhttp.ReqOpts{})

	// Associates will return a 404 in the case that they do not have store data for a tuid.
	// Since we have no way of knowing whether a certain tuid has store data or not before making the
	// call we catch the 404 case and instead return empty data
	if resp != nil && resp.StatusCode == 404 {
		return &getLinkedStoreForTwitchUser, nil
	}
	if resp == nil || err != nil {
		log.WithFields(log.Fields{
			"error": err,
		}).Error("Unexpected response from associates")
		return nil, err
	}

	return &getLinkedStoreForTwitchUser, nil
}

func getAssociatesJWTClaims(tuid string, marketplace string, context string, requiresStoreData bool) models.GetLinkedStoreForTwitchUserClaims {
	return models.GetLinkedStoreForTwitchUserClaims{
		TwitchUserId:     tuid,
		MarketplaceId:    marketplace,
		Context:          context,
		RequireStoreData: strconv.FormatBool(requiresStoreData),
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Minute * associatesJWTLifespanMinutes).Unix(),
			IssuedAt:  time.Now().Unix(),
		},
	}
}

func NewAssociatesExternalClient(endpoint string) (IAssociatesClient, error) {
	conf := twitchhttp.ClientConf{Host: endpoint}

	twitchClient, err := twitchhttp.NewClient(conf)
	if err != nil {
		log.Error("Error creating new client")
		return nil, err
	}
	return &AssociatesClientImpl{twitchClient}, nil
}
