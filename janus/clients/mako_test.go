package clients_test

import (
	"testing"

	"code.justin.tv/commerce/janus/clients"
	"code.justin.tv/commerce/janus/mocks"
	"code.justin.tv/commerce/janus/models"
	ctx "golang.org/x/net/context"

	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

var MockedMakoClient *mocks.IMakoClient
var MockedMakoContext *ctx.Context

var MockedMakoMetricLogger *mocks.IMetricLogger

var emotesResponseWithZeroEntries *models.MakoEntitlementSets
var emotesResponseWithTwoEntries *models.MakoEntitlementSets
var badgesResponseWithZeroEntries *models.MakoEntitlementSets
var badgesResponseWithTwoEntries *models.MakoEntitlementSets

const (
	tuid = "1234"
)

func initMakoClientWrapper(mockedClient *mocks.IMakoClient) clients.MakoClientWrapper {
	return clients.MakoClientWrapper{
		MakoClient:    mockedClient,
		MetricsLogger: MockedMakoMetricLogger,
	}
}

func initMakoMocks() {
	MockedMakoClient = new(mocks.IMakoClient)
	MockedMakoContext = new(ctx.Context)

	MockedMakoMetricLogger = new(mocks.IMetricLogger)
	MockedMakoMetricLogger.On("LogDurationSinceMetric", Anything, Anything).Return()
}

func initResponses() {
	badgesResponseWithZeroEntries = &models.MakoEntitlementSets{}
	badgesResponseWithTwoEntries = &models.MakoEntitlementSets{Keys: []string{"35", "42"}}
	emotesResponseWithZeroEntries = &models.MakoEntitlementSets{}
	emotesResponseWithTwoEntries = &models.MakoEntitlementSets{Keys: []string{"35", "42"}}
}

func TestGetEmoticonSets(t *testing.T) {
	Convey("When calling MakoClientWrapper.GetEntitlements", t, func() {
		initMakoMocks()
		initResponses()

		Convey("with a tuid that has 2 emoticon sets entitlements", func() {
			MockedMakoClient.On("GetEntitlements", Anything, Anything, Anything).Return(emotesResponseWithTwoEntries, nil)
			wrapper := initMakoClientWrapper(MockedMakoClient)

			emoticonSets, err := wrapper.GetEmoticonSets(*MockedMakoContext, tuid)
			So(len(emoticonSets.Keys), ShouldEqual, 2)
			So(err, ShouldBeNil)
		})

		Convey("with a tuid that has 0 emoticon sets entitlements", func() {
			MockedMakoClient.On("GetEntitlements", Anything, Anything, Anything).Return(emotesResponseWithZeroEntries, nil)
			wrapper := initMakoClientWrapper(MockedMakoClient)

			emoticonSets, err := wrapper.GetEmoticonSets(*MockedMakoContext, tuid)
			So(len(emoticonSets.Keys), ShouldEqual, 0)
			So(err, ShouldBeNil)
		})
	})
}

func TestGetBadgesSets(t *testing.T) {
	Convey("When calling MakoClientWrapper.GetEntitlements for badges", t, func() {
		initMakoMocks()
		initResponses()

		Convey("with a tuid that has 2 badges sets entitlements", func() {
			MockedMakoClient.On("GetEntitlements", Anything, Anything, Anything).Return(badgesResponseWithTwoEntries, nil)
			wrapper := initMakoClientWrapper(MockedMakoClient)

			badgeSets, err := wrapper.GetBadgeSets(*MockedMakoContext, tuid)
			So(len(badgeSets.Keys), ShouldEqual, 2)
			So(err, ShouldBeNil)
		})

		Convey("with a tuid that has 0 badges sets entitlements", func() {
			MockedMakoClient.On("GetEntitlements", Anything, Anything, Anything).Return(badgesResponseWithZeroEntries, nil)
			wrapper := initMakoClientWrapper(MockedMakoClient)

			badgeSets, err := wrapper.GetBadgeSets(*MockedMakoContext, tuid)
			So(len(badgeSets.Keys), ShouldEqual, 0)
			So(err, ShouldBeNil)
		})
	})
}
