package clients

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"

	svm "code.justin.tv/commerce/ADGDiscoveryServiceGoClient/src/com/amazon/adg/common/model"
	svc "code.justin.tv/commerce/ADGDiscoveryServiceGoClient/src/com/amazon/adg/discovery/model"
	"code.justin.tv/commerce/janus/cache"
	"code.justin.tv/commerce/janus/coral"
	"code.justin.tv/commerce/janus/metrics"
	"code.justin.tv/commerce/janus/models"

	log "github.com/sirupsen/logrus"
)

const (
	noRetailOffersBool = false
)

var (
	defaultCOR = "US"
	defaultPFM = "ATVPDKIKX0DER"
)

//IADGDiscoClient interface for ADGDiscoClient
type IADGDiscoClient interface {
	GetProductDetails(input svc.GetProductDetailsRequest) (svc.GetProductDetailsResponse, error)
	GetProductListDetails(input svc.GetProductListDetailsRequest) (svc.GetProductListDetailsResponse, error)
	GetProductsInDomains(input svc.GetProductsInDomainsRequest) (svc.GetProductsInDomainsResponse, error)
}

// ADGDiscoClient structure containing relevant information for the ADGDiscoveryServiceGoClient
type ADGDiscoClient struct {
	SwsParams coral.SWSParams
}

// IADGDiscoClientWrapper interface for ADGDiscoClientWrapper
type IADGDiscoClientWrapper interface {
	GetGameProductDetails(asin string, userID string) (*models.ADGProduct, error)
	GetMerchandiseDetails(asinList []string, userID string) ([]models.ADGProduct, error)
	GetAmazonRetailDetails(asinList []string) ([]models.ADGProduct, error)
	GetProductsInDomainByASIN(asin string, userID string) ([]models.ADGProduct, error)
	GetProductsInDomainByVendorSKU(vendorCode string, sku string, userCOR string) ([]models.ADGProduct, error)
}

// ADGDiscoClientWrapper structure containing relevant information for ADGDisco
type ADGDiscoClientWrapper struct {
	Client        IADGDiscoClient
	SwsParams     coral.SWSParams
	MetricsLogger metrics.IMetricLogger
	IsDevelopment bool
	RetryStrategy coral.RetryStrategy
	Cache         cache.IADGDiscoCache
}

// DetailsPointerMapping is a type reference for ADG Details structures
type DetailsPointerMapping map[string]map[string]*string

// SubDetailsPointerMapping is a type reference for ADG sub-Details structures (with pointers)
type SubDetailsPointerMapping map[string]*string

// SubDetailsMapping is a type reference for ADG sub-Details structures (without pointers)
type SubDetailsMapping map[string]string

var (
	gameDocket                = "twitchDetailPage.json"
	inGameContentDocket       = "twitchInGameDetails.json"
	extensionsDocket          = "twitchExtensionsDetails.json"
	merchandiseDocket         = "twitchMerchDetails.json"
	amazonRetailDocket        = "twitchAmazonRetailDetails.json"
	traversalAdjacent         = "ADJACENT"
	pageSize            int32 = 50 // Current ADGDisco max page size is 50
)

const (
	// Map keys
	attributesKey        = "attributes"
	backgroundKey        = "background"
	controllerTypesKey   = "controller_types"
	crateKey             = "crate"
	developerInfoKey     = "developer_info"
	esrbRatingDetailsKey = "esrb_rating_details"
	featureDetailsKey    = "feature_details"
	genericKeywordsKey   = "generic_keywords"
	itemBadgeDetailsKey  = "item_badge_details"
	platformsKey         = "platforms"
	screenshotsKey       = "screenshots"
	technicalDetailsKey  = "technical_details_formatted"
	shortDescriptionKey  = "shortDescription"
	itemTypeKey          = "item_type"
	languageTypeKey      = "language_type"
	languageValueKey     = "language_value"
	videosKey            = "videos"
	primeKey             = "prime"
	isPrimeEligibleKey   = "is_prime_eligible"
)

// ADGDiscoClient Implementation

// GetProductDetails makes a GetProductDetails call on a new ADGDiscoveryServiceGoClient
func (c *ADGDiscoClient) GetProductDetails(input svc.GetProductDetailsRequest) (svc.GetProductDetailsResponse, error) {
	// Payday pointed out that the connection from the Go Client can randomly die.
	// Creating a new client each time to avoid this problem
	// ref: https://git-aws.internal.justin.tv/commerce/payday/blob/master/clients/coral/adgdisco/redisco.go
	client, err := newADGDiscoveryServiceGoClient(c.SwsParams)
	if err != nil {
		log.Errorf("Error creating new ADGDiscoveryService client: %v", err)
		return nil, err
	}
	return client.GetProductDetails(input)
}

// GetProductListDetails makes a GetProductListDetails call on a new ADGDiscoveryServiceGoClient
func (c *ADGDiscoClient) GetProductListDetails(input svc.GetProductListDetailsRequest) (svc.GetProductListDetailsResponse, error) {
	// Payday pointed out that the connection from the Go Client can randomly die.
	// Creating a new client each time to avoid this problem
	// ref: https://git-aws.internal.justin.tv/commerce/payday/blob/master/clients/coral/adgdisco/redisco.go
	client, err := newADGDiscoveryServiceGoClient(c.SwsParams)
	if err != nil {
		log.Errorf("Error creating new ADGDiscoveryService client: %v", err)
		return nil, err
	}
	return client.GetProductListDetails(input)
}

// GetProductsInDomains makes a GetProductsInDomains call on a new ADGDiscoveryServiceGoClient
func (c *ADGDiscoClient) GetProductsInDomains(input svc.GetProductsInDomainsRequest) (svc.GetProductsInDomainsResponse, error) {
	// Payday pointed out that the connection from the Go Client can randomly die.
	// Creating a new client each time to avoid this problem
	// ref: https://git-aws.internal.justin.tv/commerce/payday/blob/master/clients/coral/adgdisco/redisco.go
	client, err := newADGDiscoveryServiceGoClient(c.SwsParams)
	if err != nil {
		log.Errorf("Error creating new ADGDiscoveryService client: %v", err)
		return nil, err
	}
	return client.GetProductsInDomains(input)
}

// newADGDiscoveryServiceGoClient creates a new ADGDiscoveryServiceGoClient
func newADGDiscoveryServiceGoClient(params coral.SWSParams) (*svc.ADGDiscoveryServiceClient, error) {
	codec, dialer, err := coral.InitCodecAndDialer(params)
	if err != nil {
		return nil, err
	}
	client := svc.NewADGDiscoveryServiceClient(dialer, codec)
	return client, nil
}

// ADGDiscoClientWrapper Implementation

// GetProductsInDomainByASIN is given an ASIN (Game) and finds the other digital assets associated with it (In-Game Content)
func (c *ADGDiscoClientWrapper) GetProductsInDomainByASIN(asin, userID string) ([]models.ADGProduct, error) {
	product := getProductAndVendor(asin, "", "")
	locale, amazonAccount := getLocalePrefsAndAmazonAccount(userID, "", noRetailOffersBool)

	cacheParams := &cache.ADGDiscoCacheParams{
		ShouldCache: userID == "",
		ASIN:        asin,
	}
	products, err := c.getProductsInDomain(product, userID, cacheParams, locale, amazonAccount, inGameContentDocket)
	if err != nil {
		return nil, err
	}

	// The given game is also a "product in the domain", so we need to filter it out because we only want the in game content.
	products = removeAsinFromProducts(asin, products)
	return products, nil
}

// GetProductsInDomainByVendorSKU is given a Vendor Code and SKU and finds the other digital assets associated with it (In-Extensions Content)
func (c *ADGDiscoClientWrapper) GetProductsInDomainByVendorSKU(vendorCode, sku, userCOR string) ([]models.ADGProduct, error) {
	if vendorCode == "" || sku == "" {
		return nil, errors.New("Vendor code and SKU are required")
	}

	product := getProductAndVendor("", vendorCode, sku)
	locale, amazonAccount := getLocalePrefsAndAmazonAccount("", userCOR, noRetailOffersBool)

	cacheParams := &cache.ADGDiscoCacheParams{
		ShouldCache: true,
		VendorCode:  vendorCode,
		SKU:         sku,
		UserCOR:     userCOR,
	}
	products, err := c.getProductsInDomain(product, "", cacheParams, locale, amazonAccount, extensionsDocket)
	if err != nil {
		return nil, err
	}

	// Filter out the parent product (Extension product) from the list of digital assets (In-Extensions Content)
	products = removeSKUFromProducts(sku, products)
	return products, nil
}

func (c *ADGDiscoClientWrapper) getProductsInDomain(product svm.Product, userID string, cacheParams *cache.ADGDiscoCacheParams, locale *svm.CustomerLocalePrefs, amazonAccount *svm.AmazonCustomerInfo, docket string) ([]models.ADGProduct, error) {
	startTime := time.Now()
	defer c.MetricsLogger.LogDurationSinceMetric("adgDiscoClient.getProductsInDomains", startTime)

	if !c.SwsParams.SWSEnabled {
		if c.IsDevelopment {
			log.Infof("SWS disabled, returning stubbed ADGDisco response for development")
			adgProducts := []models.ADGProduct{}
			adgProducts = append(adgProducts, *getStubbedADGDiscoProduct())
			return adgProducts, nil
		}

		return nil, errors.New("Sws is disabled, enable it in the configuration file (be sure to have the certs)")
	}

	pageParameters := svm.NewCursorPagingRequestParameters()
	//pageParameters.SetCursor() TODO: [FUEL-2901] Add paging support for Janus API
	pageParameters.SetPageSize(&pageSize)

	productsInDomainRequest := svc.NewProductsInDomainRequest()
	productsInDomainRequest.SetSource(product)
	productsInDomainRequest.SetPagingRequestParameters(pageParameters)
	productsInDomainRequests := []svc.ProductsInDomainRequest{productsInDomainRequest}

	productFilter := svm.NewProductFilter()
	productFilter.SetAcceptIfPassesAll([]svm.ProductRule{}) // Filters

	request := svc.NewGetProductsInDomainsRequest()
	request.SetProductsInDomainRequests(productsInDomainRequests)
	request.SetProductFilter(productFilter)
	request.SetDocketId(&docket)
	request.SetTraversal(&traversalAdjacent)
	request.SetPreferredLocale(*locale)
	request.SetCustomer(*amazonAccount)

	if cacheParams.ShouldCache {
		cacheADGProducts := c.Cache.GetDomainProducts(cacheParams)
		if cacheADGProducts != nil {
			log.Infof("Returning cached response. %+v", *cacheParams)
			return cacheADGProducts, nil
		}
	}

	var getProductsInDomainsResponse svc.GetProductsInDomainsResponse
	coralCall := func() (interface{}, error) { return c.Client.GetProductsInDomains(request) }
	err := c.RetryStrategy.ExecuteCoralCall(coralCall, &getProductsInDomainsResponse)
	if err != nil {
		return nil, fmt.Errorf("Error occurred while calling ADGDiscoveryService: %v", err)
	}

	products := make([]svm.Product, 0)
	for _, response := range getProductsInDomainsResponse.ProductsInDomainResponses() {
		// get all the products in the given domains
		products = append(products, response.Products()...)
	}

	if len(products) >= int(pageSize) {
		log.Errorf("Page size limit has been reached. If you are seeing this error, you need to implement paging for ADGDisco calls. %s", productLogFields(product))
	}

	adgProducts := []models.ADGProduct{}
	for _, product := range products {
		if product.Asin() == nil || len(*product.Asin()) == 0 {
			log.Infof("Received empty ASIN for IGC, skipping. %s", productLogFields(product))
			continue
		}
		adgProduct := parseADGProduct(product)
		adgProducts = append(adgProducts, adgProduct)
	}

	if cacheParams.ShouldCache {
		c.Cache.SetDomainProducts(cacheParams, adgProducts)
	}
	return adgProducts, err
}

func productLogFields(product svm.Product) string {
	var msg string
	if asin := product.Asin(); asin != nil {
		msg = msg + fmt.Sprintf("ASIN: %s. ", *asin)
	}
	if vendor := product.Vendor(); vendor != nil {
		if vendorCode := vendor.Id(); vendorCode != nil {
			msg = msg + fmt.Sprintf("Vendor Code: %s. ", *vendorCode)
		}
	}
	if sku := product.Sku(); sku != nil {
		msg = msg + fmt.Sprintf("SKU: %s. ", *sku)
	}
	return msg
}

// remove the given asin from the list of products
func removeAsinFromProducts(asin string, products []models.ADGProduct) []models.ADGProduct {
	result := []models.ADGProduct{}
	for _, product := range products {
		if product.ASIN != asin {
			result = append(result, product)
		}
	}
	return result
}

// Remove the given SKU from the list of products
func removeSKUFromProducts(sku string, products []models.ADGProduct) []models.ADGProduct {
	result := []models.ADGProduct{}
	for _, product := range products {
		if product.SKU != sku {
			result = append(result, product)
		}
	}
	return result
}

// GetGameProductDetails makes a call to ADGDisco and retrieve information about the asin product for a games specific docket
func (c *ADGDiscoClientWrapper) GetGameProductDetails(asin string, userID string) (*models.ADGProduct, error) {
	return c.getProductDetails(asin, userID, gameDocket)
}

// GetMerchandiseDetails makes a call to ADGDisco to retrieve information about all the merchandise asins
func (c *ADGDiscoClientWrapper) GetMerchandiseDetails(asinList []string, userID string) ([]models.ADGProduct, error) {
	return c.getProductListDetails(asinList, userID, merchandiseDocket, true)
}

// GetAmazonRetailDetails makes a call to ADGDisco to retrieve information of several amazon retail asins
func (c *ADGDiscoClientWrapper) GetAmazonRetailDetails(asinList []string) ([]models.ADGProduct, error) {
	// We don't need to pass a userID for retail information
	return c.getProductListDetails(asinList, "", amazonRetailDocket, true)
}

func (c *ADGDiscoClientWrapper) getProductDetails(asin string, userID string, docket string) (*models.ADGProduct, error) {
	startTime := time.Now()
	defer c.MetricsLogger.LogDurationSinceMetric("adgDiscoClient.getProductDetails", startTime)

	if !c.SwsParams.SWSEnabled {
		if c.IsDevelopment {
			log.Infof("SWS disabled, returning stubbed ADGDisco response for development")
			return getStubbedADGDiscoProduct(), nil
		}

		return nil, errors.New("Sws is disabled, enable it in the configuration file (be sure to have the certs)")
	}

	request := svc.NewGetProductDetailsRequest()

	product := svm.NewProduct()
	product.SetAsin(&asin)
	request.SetProduct(product)

	locale, amazonAccount := getLocalePrefsAndAmazonAccount(userID, "", noRetailOffersBool)
	request.SetPreferredLocale(*locale)
	request.SetCustomer(*amazonAccount)

	request.SetDocketId(&docket)

	// TODO after verifying the results start to return non nil cache results
	cacheParams := &cache.ADGDiscoCacheParams{ASIN: asin, UserID: userID, Docket: docket}
	cacheAdgProduct := c.Cache.GetProductDetails(cacheParams)

	var productDetailsResponse svc.GetProductDetailsResponse
	coralCall := func() (interface{}, error) { return c.Client.GetProductDetails(request) }
	err := c.RetryStrategy.ExecuteCoralCall(coralCall, &productDetailsResponse)
	if err != nil {
		return nil, fmt.Errorf("Error occurred while calling ADGDiscoveryService: %v", err)
	}

	productResponse := productDetailsResponse.Product()
	if productResponse == nil {
		return nil, fmt.Errorf("No product in ADGDisco response")
	}

	productDetails := productResponse.ProductDetails()
	if productDetails == nil {
		return nil, fmt.Errorf("No product details found in ADGDisco response")
	}
	adgProduct := parseADGProduct(productResponse)

	if cacheAdgProduct == nil {
		c.Cache.SetProductDetails(cacheParams, &adgProduct)
	} else {
		// TODO remove else block after the cache results are verified
		// Temporary metric for comparing cache accuracy
		if !reflect.DeepEqual(*cacheAdgProduct, adgProduct) {
			log.Infof("The cache value and coral values are different. CachedAdgProduct: %+v CoralResponse: %+v", *cacheAdgProduct, adgProduct)
			c.MetricsLogger.LogCountMetric("adgDiscoClient.getProductDetails.IncorrectCacheValue", 1)
		}
	}

	return &adgProduct, err
}

func (c *ADGDiscoClientWrapper) getProductListDetails(asinList []string, userID string, docketID string, isRetail bool) ([]models.ADGProduct, error) {
	startTime := time.Now()
	defer c.MetricsLogger.LogDurationSinceMetric("adgDiscoClient.getProductListDetails", startTime)

	if !c.SwsParams.SWSEnabled {
		if c.IsDevelopment {
			log.Infof("SWS disabled, returning stubbed ADGDisco response for development")
			return getStubbedADGDiscoProductList(), nil
		}

		return nil, errors.New("Sws is disabled, enable it in the configuration file (be sure to have the certs)")
	}

	request := svc.NewGetProductListDetailsRequest()

	products := []svm.Product{}
	for _, asin := range asinList {
		var asinp = asin // `asin` is re-used during the loop so we can't use it as a pointer
		product := svm.NewProduct()
		product.SetAsin(&asinp)
		products = append(products, product)
	}
	request.SetProducts(products)

	locale, amazonAccount := getLocalePrefsAndAmazonAccount(userID, "", isRetail)
	request.SetPreferredLocale(*locale)
	request.SetCustomer(*amazonAccount)

	request.SetDocketId(&docketID)

	// There is currently no needed user info, improving caching by using a blank userID
	cacheParams := &cache.ADGDiscoCacheParams{ASINList: asinList, UserID: "", DocketID: docketID}
	cacheADGProducts := c.Cache.GetProductDetailsList(cacheParams)
	if cacheADGProducts != nil {
		log.Infof("Returning cached response for asinList: %+v and docketID: %+v", asinList, docketID)
		return cacheADGProducts, nil
	}

	var productDetailsResponse svc.GetProductListDetailsResponse
	coralCall := func() (interface{}, error) { return c.Client.GetProductListDetails(request) }
	err := c.RetryStrategy.ExecuteCoralCall(coralCall, &productDetailsResponse)
	if err != nil {
		return nil, fmt.Errorf("Error occurred while calling ADGDiscoveryService: %v", err)
	}

	productResponse := productDetailsResponse.Products()
	if productResponse == nil {
		return nil, fmt.Errorf("No products in ADGDisco response")
	}

	productList := []models.ADGProduct{}

	for _, product := range productResponse {
		productDetails := product.ProductDetails()
		if productDetails == nil {
			return nil, fmt.Errorf("No product details found in ADGDisco response")
		}
		adgProduct := parseADGProduct(product)
		productList = append(productList, adgProduct)
	}

	// There is currently no needed user info, improving caching by using a blank userID
	c.Cache.SetProductDetailsList(cacheParams, productList)
	return productList, err
}

func getProductAndVendor(asin, vendorCode, sku string) svm.Product {
	product := svm.NewProduct()
	switch {
	case asin != "":
		product.SetAsin(&asin)
	case vendorCode != "" && sku != "":
		vendor := svm.NewVendor()
		vendor.SetId(&vendorCode)
		product.SetVendor(vendor)
		product.SetSku(&sku)
	}
	return product
}

func getLocalePrefsAndAmazonAccount(userID, userCOR string, isRetail bool) (*svm.CustomerLocalePrefs, *svm.AmazonCustomerInfo) {
	locale := svm.NewCustomerLocalePrefs()
	amazonCustomer := svm.NewAmazonCustomerInfo()
	switch {
	case userID != "":
		// If we have a user, let the docket determine which COR and PFM to use
		amazonCustomer.SetDigitalId(&userID)
	case isRetail:
		// Retail order requests to ADG shouldn't specify COR otherwise ADG can't find the offer information
		locale.SetPfm(&defaultPFM)
	case userCOR != "":
		locale.SetCor(&userCOR)
		// Use default PFM until Fuel launches in other marketplaces
		locale.SetPfm(&defaultPFM)
	default:
		// Fallback to our default COR and PFM
		locale.SetCor(&defaultCOR)
		locale.SetPfm(&defaultPFM)
	}
	return &locale, &amazonCustomer
}

func parseADGProduct(product svm.Product) models.ADGProduct {
	var details models.Details
	var iconURL string
	var price models.Price
	var category string
	productDetails := product.ProductDetails()
	if productDetails != nil {
		details = parseDetails(productDetails.Details())
		if productDetails.ProductIconUrl() != nil {
			iconURL = *productDetails.ProductIconUrl()
		}
		if productDetails.CurrentPrice() != nil {
			price = models.Price{
				Price:        productDetails.CurrentPrice().Amount().FloatString(2),
				CurrencyUnit: *productDetails.CurrentPrice().Unit(),
			}
		}
		if productDetails.Category() != nil {
			category = *productDetails.Category()
		}
	}

	var asin string
	if product.Asin() != nil {
		asin = *product.Asin()
	}

	var description string
	if product.ProductDescription() != nil {
		description = *product.ProductDescription()
	}

	var title string
	if product.ProductTitle() != nil {
		title = *product.ProductTitle()
	}

	var productLine string
	if product.ProductLine() != nil {
		productLine = *product.ProductLine()
	}

	var sku string
	if product.Sku() != nil {
		sku = *product.Sku()
	}

	var vendorId string
	vendor := product.Vendor()
	if vendor != nil {
		if vendor.Id() != nil {
			vendorId = *vendor.Id()
		}
	}

	var productType string
	if product.Type() != nil {
		productType = *product.Type()
	}

	var id string
	if product.Id() != nil {
		id = *product.Id()
	}

	var asinVersion int32
	if product.AsinVersion() != nil {
		asinVersion = *product.AsinVersion()
	}

	return models.ADGProduct{
		ASIN:        asin,
		Description: description,
		Details:     details,
		IconURL:     iconURL,
		Price:       price,
		Title:       title,
		ProductLine: productLine,
		SKU:         sku,
		VendorId:    vendorId,
		Type:        productType,
		ID:          id,
		AsinVersion: asinVersion,
		Category:    category,
	}
}

func parseDetails(details DetailsPointerMapping) models.Details {
	var attributes models.Attributes
	var background models.Background
	var controllerTypes []string
	var crate models.Crate
	var developer models.Developer
	var esrbRatingDetails []string
	var featureDetails []string
	var genericKeywords []string
	var itemBadgeDetails []string
	var platforms []string
	var screenshots []string
	var technicalDetails models.TieredSystemRequirements
	var shortDescription string
	var itemType string
	var languageType map[string]string
	var languageValue map[string]string
	var videos map[string]string
	var prime models.Prime

	for key, value := range details {

		switch key {
		case attributesKey:
			attributes = parseAttributes(value)
		case backgroundKey:
			background = parseBackgroundInfo(value)
		case controllerTypesKey:
			controllerTypes = convertToArray(value)
		case crateKey:
			crate = parseCrate(value)
		case developerInfoKey:
			developer = parseDeveloperInfo(value)
		case esrbRatingDetailsKey:
			esrbRatingDetails = convertToArray(value)
		case featureDetailsKey:
			featureDetails = convertToArray(value)
		case itemBadgeDetailsKey:
			itemBadgeDetails = convertToArray(value)
		case genericKeywordsKey:
			genericKeywords = convertToArray(value)
		case platformsKey:
			platforms = convertToArray(value)
		case screenshotsKey:
			screenshots = convertToArray(value)
		case technicalDetailsKey:
			technicalDetails = parseTechnicalDetails(value)
		case shortDescriptionKey:
			shortDescription = parseShortDescription(value)
		case itemTypeKey:
			itemType = parseItemType(value)
		case languageTypeKey:
			languageType = convertToMapWithoutPointers(value)
		case languageValueKey:
			languageValue = convertToMapWithoutPointers(value)
		case videosKey:
			videos = convertToMapWithoutPointers(value)
		case primeKey:
			prime = parsePrime(value)
		}

	}

	return models.Details{
		Attributes:        attributes,
		Background:        background,
		Crate:             crate,
		ControllerTypes:   controllerTypes,
		Developer:         developer,
		ESRBRatingDetails: esrbRatingDetails,
		FeaturesDetails:   featureDetails,
		GenericKeywords:   genericKeywords,
		ItemBadgeDetails:  itemBadgeDetails,
		Platforms:         platforms,
		Screenshots:       screenshots,
		TechnicalDetails:  technicalDetails,
		ShortDescription:  shortDescription,
		ItemType:          itemType,
		LanguageType:      languageType,
		LanguageValue:     languageValue,
		Videos:            videos,
		Prime:             prime,
	}
}

func parseAttributes(details SubDetailsPointerMapping) models.Attributes {
	attributesMap := convertToMapWithoutPointers(details)

	attributes := &models.Attributes{}
	convertMapToInterface(attributesMap, attributes)

	return *attributes
}

func parseBackgroundInfo(details SubDetailsPointerMapping) models.Background {
	backgroundMap := convertToMapWithoutPointers(details)

	background := &models.Background{}
	convertMapToInterface(backgroundMap, background)

	return *background
}

func parseCrate(details SubDetailsPointerMapping) models.Crate {
	crateMap := convertToMapWithoutPointers(details)

	crate := &models.Crate{}
	convertMapToInterface(crateMap, crate)

	return *crate
}

func parseDeveloperInfo(details SubDetailsPointerMapping) models.Developer {
	devInfoMap := convertToMapWithoutPointers(details)

	developer := &models.Developer{}
	convertMapToInterface(devInfoMap, developer)

	// TODO temp hack to get us thru 9/30. clean this up once we can actually ingest null values correctly via the
	//      dev portal scripts
	if developer.EULAURL == "null" {
		developer.EULAURL = ""
	}

	if developer.SupportURL == "null" {
		developer.SupportURL = ""
	}

	return *developer
}

func parsePrime(pointerMap SubDetailsPointerMapping) models.Prime {
	isPrimeEligible, err := strconv.ParseBool(*pointerMap[isPrimeEligibleKey])
	if err != nil {
		isPrimeEligible = false
	}

	return models.Prime{
		IsPrimeEligible: isPrimeEligible,
	}
}

/* Currently receiving technical details in a non-parsed array
   so, we meed to parse it ourselves */
const minimumDirectX = "PCRequirements-Minimum-DirectX"
const minimumHardDrive = "PCRequirements-Minimum-HardDriveSpace"
const minimumOS = "PCRequirements-Minimum-OS"
const minimumProcesser = "PCRequirements-Minimum-Processer"
const minimumRAM = "PCRequirements-Minimum-RAM"
const minimumVideoCard = "PCRequirements-Minimum-VideoCard"
const recommendedDirectX = "PCRequirements-Recommended-DirectX"
const recommendedHardDrive = "PCRequirements-Recommended-HardDriveSpace"
const recommendedOS = "PCRequirements-Recommended-OS"
const recommendedProcesser = "PCRequirements-Recommended-Processer"
const recommendedRAM = "PCRequirements-Recommended-RAM"
const recommendedVideoCard = "PCRequirements-Recommended-VideoCard"
const otherRequirements = "OtherRequirements"

func parseTechnicalDetails(pointerMap SubDetailsPointerMapping) models.TieredSystemRequirements {
	data := convertToArray(pointerMap)
	parsedData := ConvertTechnicalDetailsToMap(data)
	return models.TieredSystemRequirements{
		Minimum: models.SystemRequirements{
			DirectXVersion: parsedData[minimumDirectX],
			HardDriveSpace: parsedData[minimumHardDrive],
			OSVersion: []string{
				parsedData[minimumOS],
			},
			Other: parsedData[otherRequirements],
			Processor: []string{
				parsedData[minimumProcesser],
			},
			RAM: parsedData[minimumRAM],
			VideoCard: []string{
				parsedData[minimumVideoCard],
			},
		},
		Recommended: models.SystemRequirements{
			DirectXVersion: parsedData[recommendedDirectX],
			HardDriveSpace: parsedData[recommendedHardDrive],
			OSVersion: []string{
				parsedData[recommendedOS],
			},
			Other: parsedData[otherRequirements],
			Processor: []string{
				parsedData[recommendedProcesser],
			},
			RAM: parsedData[recommendedRAM],
			VideoCard: []string{
				parsedData[recommendedVideoCard],
			},
		},
	}
}

// ConvertTechnicalDetailsToMap converts TechnicalDetails to a map format
func ConvertTechnicalDetailsToMap(data []string) map[string]string {
	var m = make(map[string]string)
	for _, line := range data {
		splits := strings.SplitN(line, ":", 2)
		if len(splits) < 2 {
			log.Errorf("Invalid Technical Detail while converting ADGDisco data to map: %s", line)
			continue
		}
		m[splits[0]] = strings.TrimSpace(splits[1])
	}
	return m
}

func convertToArray(pointerMap SubDetailsPointerMapping) []string {
	array := make([]string, len(pointerMap))

	for key, value := range pointerMap {
		index, err := strconv.Atoi(key)
		if err != nil {
			log.Errorf("Error converting pointer map key %s to int: %v\n", key, err)
			continue
		}
		if index < 0 || index >= len(array) {
			log.Errorf("Invalid index while creating array for janus response")
			continue
		}
		array[index] = *value
	}
	return array
}

func convertToMapWithoutPointers(pointerMap SubDetailsPointerMapping) map[string]string {
	var m = make(map[string]string)
	for keyPointer, valuePointer := range pointerMap {
		if valuePointer != nil {
			m[keyPointer] = *valuePointer
		}
	}
	return m
}

func convertMapToInterface(mappedInterface SubDetailsMapping, i interface{}) {
	bytes, err := json.Marshal(mappedInterface)

	if err != nil {
		log.Errorf("Error while converting developer map: %v\n", err)
		return
	}

	err = json.Unmarshal(bytes, i)

	if err != nil {
		log.Errorf("Error getting developer object: %v\n", err)
		return
	}
}

func parseShortDescription(value map[string]*string) string {
	return parseSingleEntryMap(value)
}

func parseItemType(value map[string]*string) string {
	return parseSingleEntryMap(value)
}

func parseSingleEntryMap(value map[string]*string) string {
	/* Example ADG response
	"shortDescription":
	{
	"0": "140 Mammoth Coins for purchasing special items such as character skins and taunts from the Malhalla store."
	}
	*/
	var response string
	if value != nil {
		valuePointer := value["0"]
		if valuePointer != nil {
			response = *valuePointer
		}
	}
	return response
}

// Used only in development env.
// Feel free to stub anything you want if testing things!
func getStubbedADGDiscoProduct() *models.ADGProduct {
	product := &models.ADGProduct{}
	product.ASIN = "MockAsin"
	product.Title = "Stubbed Test Product"
	product.Description = "Stubbed Test Description"
	product.IconURL = "stubbediconurl.jpg"
	product.Details.Attributes.ReleaseDate = "2017-01-01T00:00:00Z"
	product.Price.Price = "9.99"
	product.Price.CurrencyUnit = "USD"
	product.Details.LanguageType = map[string]string{
		"0": "manual",
		"1": "manual",
		"2": "manual",
		"3": "subtitled",
		"4": "subtitled",
		"5": "audio_description",
		"6": "manual",
		"7": "manual",
	}
	product.Details.LanguageValue = map[string]string{
		"0": "english",
		"1": "french",
		"2": "spanish",
		"3": "english",
		"4": "french",
		"5": "english",
		"6": "traditional_chinese",
		"7": "simplified_chinese",
	}
	product.Details.Videos = map[string]string{
		"0": "https://www.twitch.tv/videos/129358608",
		"1": "https://www.twitch.tv/videos/129358938",
		"2": "https://www.twitch.tv/videos/129359233",
		"3": "https://www.twitch.tv/videos/129359486",
		"4": "https://www.twitch.tv/videos/129361541",
	}
	return product
}

func getStubbedADGDiscoProductList() []models.ADGProduct {
	products := []models.ADGProduct{}
	products = append(products, *getStubbedADGDiscoProduct())
	products = append(products, *getStubbedADGDiscoProduct())
	return products
}
