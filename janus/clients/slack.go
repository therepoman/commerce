package clients

import (
	"bytes"
	"encoding/json"
	"net/http"
	"time"

	"code.justin.tv/commerce/janus/metrics"
	log "github.com/sirupsen/logrus"
)

type ISlacker interface {
	Post(msg SlackMsg) error
}

type Slacker struct {
	MetricsLogger metrics.IMetricLogger
	WebHookURL    string
}

type SlackMsg struct {
	Text        string            `json:"text"`
	UnfurlLinks bool              `json:"unfurl_links"`
	Parse       string            `json:"parse"`
	Attachments []SlackAttachment `json:"attachments"`
}

type SlackAttachment struct {
	Fallback   string   `json:"fallback"`
	Text       string   `json:"text"`
	Pretext    string   `json:"pretext"`
	ImageURL   string   `json:"image_url"`
	Footer     string   `json:"footer"`
	FooterIcon string   `json:"footer_icon"`
	ThumbURL   string   `json:"thumb_url"`
	MarkdownIn []string `json:"mrkdwn_in"`
}

func NewSlacker(webHookURL string, metricsLogger metrics.IMetricLogger) ISlacker {
	return &Slacker{
		MetricsLogger: metricsLogger,
		WebHookURL:    webHookURL,
	}
}

func (s *Slacker) Post(msg SlackMsg) error {
	startTime := time.Now()
	defer s.MetricsLogger.LogDurationSinceMetric("slacker.post", startTime)

	url := s.WebHookURL
	if url == "" {
		return nil
	}

	requestJson, err := json.Marshal(&msg)
	if err != nil {
		log.Warnf("Could not marshal json for slack message: %v", err)
		return err
	}

	requestJsonReader := bytes.NewReader(requestJson)

	_, err = http.Post(url, jsonBodyType, requestJsonReader)
	if err != nil {
		log.Warnf("Could not send message to slack webhook (%s) : %v", url, err)
		return err
	}
	return nil
}
