package clients_test

import (
	"errors"
	"fmt"
	"testing"

	svm "code.justin.tv/commerce/ADGDiscoveryServiceGoClient/src/com/amazon/adg/common/model"
	svc "code.justin.tv/commerce/ADGDiscoveryServiceGoClient/src/com/amazon/adg/discovery/model"

	"code.justin.tv/commerce/janus/clients"
	"code.justin.tv/commerce/janus/coral"
	"code.justin.tv/commerce/janus/mocks"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

var MockedADGDiscoClient *mocks.IADGDiscoClient
var MockedIMetricLogger *mocks.IMetricLogger
var mockedCache *mocks.IADGDiscoCache

func initADGDiscoClientWrapper(mockedClient *mocks.IADGDiscoClient, mockedLogger *mocks.IMetricLogger) clients.ADGDiscoClientWrapper {
	return clients.ADGDiscoClientWrapper{
		Client:        mockedClient,
		SwsParams:     coral.SWSParams{SWSEnabled: true},
		RetryStrategy: coral.NewTestingRetryStrategy(),
		MetricsLogger: mockedLogger,
	}
}

func initADGDiscoClientWrapperWithRetryStrategy(mockedClient *mocks.IADGDiscoClient, mockedCache *mocks.IADGDiscoCache, mockedLogger *mocks.IMetricLogger) clients.ADGDiscoClientWrapper {
	return clients.ADGDiscoClientWrapper{
		Client:        mockedClient,
		Cache:         mockedCache,
		SwsParams:     coral.SWSParams{SWSEnabled: true},
		RetryStrategy: coral.NewDefaultRetryStrategy(),
		MetricsLogger: mockedLogger,
	}
}

func testRetryableException(exception string) {
	MockedADGDiscoClient = new(mocks.IADGDiscoClient)
	MockedIMetricLogger = new(mocks.IMetricLogger)
	mockedCache = new(mocks.IADGDiscoCache)
	adgDiscoClientWrapper := initADGDiscoClientWrapperWithRetryStrategy(MockedADGDiscoClient, mockedCache, MockedIMetricLogger)
	var asin string = "asin"
	var userID string = "userID"

	emptyResponse := svc.NewGetProductDetailsResponse()

	response := svc.NewGetProductDetailsResponse()
	response.SetProduct(createProductWithTestDetails(asin))

	MockedADGDiscoClient.On("GetProductDetails", mock.Anything).Return(emptyResponse, errors.New(exception)).Once()
	MockedADGDiscoClient.On("GetProductDetails", mock.Anything).Return(response, nil).Once()
	mockedCache.On("GetProductDetails", mock.Anything, mock.Anything, mock.Anything).Return(nil)
	mockedCache.On("SetProductDetails", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return()

	MockedIMetricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()
	MockedIMetricLogger.On("LogCountMetric", mock.Anything, mock.Anything).Return()

	output, err := adgDiscoClientWrapper.GetGameProductDetails(asin, userID)

	So(err, ShouldBeNil)
	So(asin, ShouldEqual, output.ASIN)
	So(2, ShouldEqual, len(MockedADGDiscoClient.Calls))
}

func TestRetryStrategySucceedsOnThirdTry(t *testing.T) {
	Convey("when making adgDisco calls and recieving two failures and one success", t, func() {
		MockedADGDiscoClient = new(mocks.IADGDiscoClient)
		MockedIMetricLogger = new(mocks.IMetricLogger)
		mockedCache = new(mocks.IADGDiscoCache)
		adgDiscoClientWrapper := initADGDiscoClientWrapperWithRetryStrategy(MockedADGDiscoClient, mockedCache, MockedIMetricLogger)
		var asin string = "asin"
		var userID string = "userID"

		emptyResponse := svc.NewGetProductDetailsResponse()

		response := svc.NewGetProductDetailsResponse()
		response.SetProduct(createProductWithTestDetails(asin))

		MockedADGDiscoClient.On("GetProductDetails", mock.Anything).Return(emptyResponse, errors.New(coral.HTTPFailureException)).Once()
		MockedADGDiscoClient.On("GetProductDetails", mock.Anything).Return(emptyResponse, errors.New("dial tcp 111.11.111.11:443: getsockopt: connection timed out")).Once()
		MockedADGDiscoClient.On("GetProductDetails", mock.Anything).Return(response, nil).Once()
		mockedCache.On("GetProductDetails", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		mockedCache.On("SetProductDetails", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return()

		MockedIMetricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()
		MockedIMetricLogger.On("LogCountMetric", mock.Anything, mock.Anything).Return()

		output, err := adgDiscoClientWrapper.GetGameProductDetails(asin, userID)

		So(err, ShouldBeNil)
		So(asin, ShouldEqual, output.ASIN)
		So(3, ShouldEqual, len(MockedADGDiscoClient.Calls))
	})
}

func TestRetryStrategyDependencyException(t *testing.T) {
	Convey("when making adgDisco calls and recieving one DependencyException and one success", t, func() {
		testRetryableException(coral.DependencyException)
	})
}

func TestRetryStrategyHttpTimeoutException(t *testing.T) {
	Convey("when making adgDisco calls and recieving one HttpTimeoutException and one success", t, func() {
		testRetryableException(coral.HttpTimeoutException)
	})
}

func TestConvertTechnicalDetailsToMap(t *testing.T) {
	Convey("when converting ADGDisco technical details data to map", t, func() {
		adgData := []string{"requirement-foo:", "requirement-bar: bar"}
		technicalDetailsMap := clients.ConvertTechnicalDetailsToMap(adgData)

		So(technicalDetailsMap["requirement-foo"], ShouldEqual, "")
		So(technicalDetailsMap["requirement-bar"], ShouldEqual, "bar")
	})
}

func TestGetProductsInDomainByASINEmptyAsin(t *testing.T) {
	Convey("when making GetProductsInDomainByASIN and receive an empty ASIN", t, func() {
		MockedADGDiscoClient = new(mocks.IADGDiscoClient)
		MockedIMetricLogger = new(mocks.IMetricLogger)
		mockedCache = new(mocks.IADGDiscoCache)
		products := make([]svm.Product, 0)
		products = append(products, createProductWithTestDetailsPointerAsin(nil))
		products = append(products, createProductWithTestDetails(""))
		products = append(products, createProductWithTestDetails("ASIN"))

		productInDomain := svc.NewProductsInDomainResponse()
		productInDomain.SetProducts(products)

		productsInDomain := make([]svc.ProductsInDomainResponse, 0)
		productsInDomain = append(productsInDomain, productInDomain)

		getResponse := svc.NewGetProductsInDomainsResponse()
		getResponse.SetProductsInDomainResponses(productsInDomain)

		MockedADGDiscoClient.On("GetProductsInDomains", mock.Anything).Return(getResponse, nil).Once()
		MockedIMetricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()
		mockedCache.On("GetDomainProducts", mock.Anything).Return(nil)
		mockedCache.On("SetDomainProducts", mock.Anything, mock.Anything).Return(nil)

		adgDiscoClientWrapper := initADGDiscoClientWrapperWithRetryStrategy(MockedADGDiscoClient, mockedCache, MockedIMetricLogger)
		responseProduct, err := adgDiscoClientWrapper.GetProductsInDomainByASIN("asin", "tuid")

		So(err, ShouldBeNil)
		So(len(responseProduct), ShouldEqual, 1)
	})
}

func TestGetProductsInDomainByVendorSKU(t *testing.T) {
	Convey("when making GetProductsInDomainByVendorSKU", t, func() {
		MockedADGDiscoClient = new(mocks.IADGDiscoClient)
		MockedIMetricLogger = new(mocks.IMetricLogger)
		mockedCache = new(mocks.IADGDiscoCache)
		products := make([]svm.Product, 0)
		products = append(products, createProductWithTestDetailsByVendorSKU("vendor", "sku"))
		products = append(products, createProductWithTestDetailsByVendorSKU("vendor", "otherSKU"))

		productInDomain := svc.NewProductsInDomainResponse()
		productInDomain.SetProducts(products)

		productsInDomain := make([]svc.ProductsInDomainResponse, 0)
		productsInDomain = append(productsInDomain, productInDomain)

		getResponse := svc.NewGetProductsInDomainsResponse()
		getResponse.SetProductsInDomainResponses(productsInDomain)

		MockedADGDiscoClient.On("GetProductsInDomains", mock.Anything).Return(getResponse, nil).Once()
		MockedIMetricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()
		mockedCache.On("GetDomainProducts", mock.Anything).Return(nil)
		mockedCache.On("SetDomainProducts", mock.Anything, mock.Anything).Return(nil)

		adgDiscoClientWrapper := initADGDiscoClientWrapperWithRetryStrategy(MockedADGDiscoClient, mockedCache, MockedIMetricLogger)
		responseProduct, err := adgDiscoClientWrapper.GetProductsInDomainByVendorSKU("vendor", "sku", "cor")

		So(err, ShouldBeNil)
		// Should return the list of products with the requested SKU filtered out
		So(len(responseProduct), ShouldEqual, 1)
	})
}

func TestGetProductsInDomainByVendorSKUMissingFields(t *testing.T) {
	Convey("when making GetProductsInDomainByVendorSKU", t, func() {
		MockedADGDiscoClient = new(mocks.IADGDiscoClient)
		MockedIMetricLogger = new(mocks.IMetricLogger)
		mockedCache = new(mocks.IADGDiscoCache)
		adgDiscoClientWrapper := initADGDiscoClientWrapperWithRetryStrategy(MockedADGDiscoClient, mockedCache, MockedIMetricLogger)
		_, err := adgDiscoClientWrapper.GetProductsInDomainByVendorSKU("", "", "")
		So(err.Error(), ShouldEqual, "Vendor code and SKU are required")
	})
}

func createProductWithTestDetails(asin string) svm.Product {
	responseProduct := svm.NewProduct()
	responseProduct.SetAsin(&asin)

	responseProductDetail := svm.NewProductDetail()
	responseProduct.SetProductDetails(responseProductDetail)

	return responseProduct
}

func createProductWithTestDetailsPointerAsin(asin *string) svm.Product {
	responseProduct := svm.NewProduct()
	responseProduct.SetAsin(asin)

	responseProductDetail := svm.NewProductDetail()
	responseProduct.SetProductDetails(responseProductDetail)

	return responseProduct
}

func createProductWithTestDetailsByVendorSKU(vendor, sku string) svm.Product {
	responseVendor := svm.NewVendor()
	responseVendor.SetId(&vendor)

	responseProduct := svm.NewProduct()
	responseProduct.SetVendor(responseVendor)
	responseProduct.SetSku(&sku)
	asin := fmt.Sprintf("asin:%s:%s", vendor, sku)
	responseProduct.SetAsin(&asin)

	responseProductDetail := svm.NewProductDetail()
	responseProduct.SetProductDetails(responseProductDetail)

	return responseProduct
}
