package clients

var (
	languageCOR = map[string]string{
		"ar-SA": "SA", // Arabic
		"bg-BG": "BG", // Bulgarian
		"cs-CZ": "CZ", // Czech
		"da-DK": "DK", // Danish
		"de-DE": "DE", // German
		"el-GR": "GR", // Greek
		"en-US": "US", // English
		"en-GB": "UK", // English (British)
		"es-ES": "ES", // Spanish (Spain)
		"es-MX": "MX", // Spanish (Latin-America)
		"fi-FI": "FI", // Suomi
		"fr-FR": "FR", // French
		"hu-HU": "HU", // Magyar
		"it-IT": "IT", // Italian
		"ja-JP": "JP", // Japanese
		"ko-KR": "KR", // Korean
		"nl-NL": "NL", // Dutch
		"no-NL": "NO", // Norwegian
		"pl-PL": "PL", // Polish
		"pt-PT": "PT", // Portugese
		"pt-BR": "BR", // Portugese (Brazil)
		"ru-RU": "RU", // Russian
		"sk-SK": "SI", // Slovenian
		"sv-SE": "SE", // Swedish
		"th-TH": "TH", // Thai
		"tr-TR": "TR", // Turkish
		"vi-VN": "VN", // Vietnamese
		"zh-CN": "US", // Chinese (Simplified)
		"zh-TW": "US", // Chinese (Traditional)
	}
)

func LanguageToCOR(lang string) string {
	if cor := languageCOR[lang]; cor != "" {
		return cor
	}
	return defaultCOR
}
