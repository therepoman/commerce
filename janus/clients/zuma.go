package clients

import (
	"time"

	zumaAPI "code.justin.tv/chat/zuma/app/api"
	zuma "code.justin.tv/chat/zuma/client"
	"code.justin.tv/commerce/janus/metrics"
	"code.justin.tv/common/twitchhttp"
	"golang.org/x/net/context"
)

// IZumaWrapper interface for PubSubWrapper
type IZumaWrapper interface {
	ParseMessage(ctx context.Context, req zumaAPI.ParseMessageRequest, opt *twitchhttp.ReqOpts) (zumaAPI.ParseMessageResponse, error)
}

// ZumaWrapper struct for making pubsub calls
type ZumaWrapper struct {
	MetricsLogger metrics.IMetricLogger
	zuma          zuma.Client
}

// NewZumaWrapper creates a new PubSubWrapper
func NewZumaWrapper(zumaClient zuma.Client, metricsLogger metrics.IMetricLogger) IZumaWrapper {
	return &ZumaWrapper{
		MetricsLogger: metricsLogger,
		zuma:          zumaClient,
	}
}

// ParseMessage publishes a message to pubsub for a specific set of topics
func (z *ZumaWrapper) ParseMessage(ctx context.Context, req zumaAPI.ParseMessageRequest, opt *twitchhttp.ReqOpts) (zumaAPI.ParseMessageResponse, error) {
	startTime := time.Now()
	defer z.MetricsLogger.LogDurationSinceMetric("zuma.parseMessage", startTime)

	response, err := z.zuma.ParseMessage(ctx, req, opt)
	if err != nil {
		return zumaAPI.ParseMessageResponse{}, err
	}
	return response, nil
}
