package clients_test

import (
	"testing"

	"code.justin.tv/commerce/janus/clients"
	"code.justin.tv/commerce/janus/mocks"
	"code.justin.tv/commerce/janus/models"
	ctx "golang.org/x/net/context"

	"errors"

	. "github.com/smartystreets/goconvey/convey"
	. "github.com/stretchr/testify/mock"
)

var MockedAssociatesClient *mocks.IAssociatesClient
var MockedAssociatesContext *ctx.Context
var MockedAssociatesMetricLogger *mocks.IMetricLogger
var MockedJWTHelper *mocks.IJWTHelper
var MockedLinkedCacheClient *mocks.IAssociatesStoreCacheClient
var MockedNotLinkedCacheClient *mocks.IAssociatesStoreCacheClient

func initAssociatesClientWrapper(mockedClient *mocks.IAssociatesClient, mockedJWTHelper *mocks.IJWTHelper,
	mockedLinkedCacheClient *mocks.IAssociatesStoreCacheClient, mockedNotLinkedCacheClient *mocks.IAssociatesStoreCacheClient) clients.AssociatesClientWrapper {
	return clients.AssociatesClientWrapper{
		AssociatesClient:     mockedClient,
		MetricsLogger:        MockedAssociatesMetricLogger,
		JWTHelper:            mockedJWTHelper,
		LinkedCacheClient:    mockedLinkedCacheClient,
		NotLinkedCacheClient: mockedNotLinkedCacheClient,
	}
}

func initAssociatesMocks() {
	MockedAssociatesClient = new(mocks.IAssociatesClient)
	MockedJWTHelper = new(mocks.IJWTHelper)
	MockedAssociatesContext = new(ctx.Context)

	MockedAssociatesMetricLogger = new(mocks.IMetricLogger)
	MockedAssociatesMetricLogger.On("LogDurationSinceMetric", Anything, Anything).Return()

	MockedLinkedCacheClient = new(mocks.IAssociatesStoreCacheClient)
	MockedNotLinkedCacheClient = new(mocks.IAssociatesStoreCacheClient)
}

func TestGetLinkedAssociatesStore(t *testing.T) {
	Convey("When calling AssociatesClientWrapper.GetLinkedStoreForTwitchUser", t, func() {
		initAssociatesMocks()
		initResponses()

		broadcasterID := "12345"
		marketplaceID := "mockMarketplaceID"
		context := "mockContext"
		jwt := "mockedJWT"

		Convey("with a tuid that has one linked store with status Linked", func() {
			clientResponse := &models.LinkedAssociatesStore{
				TUID:          "mockTUID",
				MarketplaceID: "mockMarketplace",
				StoreID:       "mockStoreID",
				Status:        "Linked",
			}

			Convey("Cache Hit", func() {
				MockedLinkedCacheClient.On("Get", broadcasterID, marketplaceID).Return(true, clientResponse).Once()
				MockedJWTHelper.On("GetSignedTokenString", Anything, Anything, Anything).Times(0)
				MockedAssociatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything).Times(0)
				wrapper := initAssociatesClientWrapper(MockedAssociatesClient, MockedJWTHelper, MockedLinkedCacheClient, MockedNotLinkedCacheClient)
				linkedStore, err := wrapper.GetLinkedStoreForTwitchUser(*MockedAssociatesContext, broadcasterID, marketplaceID, true, context)
				So(err, ShouldBeNil)
				So(linkedStore, ShouldNotBeNil)
				So(linkedStore.TUID, ShouldNotBeNil)
				So(linkedStore.MarketplaceID, ShouldNotBeNil)
				So(linkedStore.StoreID, ShouldNotBeNil)
				So(linkedStore.Status, ShouldNotBeNil)
			})

			Convey("Cache Miss", func() {
				MockedLinkedCacheClient.On("Get", broadcasterID, marketplaceID).Return(false, nil).Once()
				MockedAssociatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything).Return(clientResponse, nil)
				MockedJWTHelper.On("GetSignedTokenString", Anything, Anything, Anything).Return(jwt, nil)
				MockedLinkedCacheClient.On("Put", clientResponse).Return().Once()
				wrapper := initAssociatesClientWrapper(MockedAssociatesClient, MockedJWTHelper, MockedLinkedCacheClient, MockedNotLinkedCacheClient)

				linkedStore, err := wrapper.GetLinkedStoreForTwitchUser(*MockedAssociatesContext, broadcasterID, marketplaceID, true, context)
				So(err, ShouldBeNil)
				So(linkedStore, ShouldNotBeNil)
				So(linkedStore.TUID, ShouldNotBeNil)
				So(linkedStore.MarketplaceID, ShouldNotBeNil)
				So(linkedStore.StoreID, ShouldNotBeNil)
				So(linkedStore.Status, ShouldNotBeNil)
			})
		})

		Convey("with a tuid that has one linked store with status Linked_Missing_Information", func() {
			clientResponse := &models.LinkedAssociatesStore{
				TUID:          "mockTUID",
				MarketplaceID: "mockMarketplace",
				StoreID:       "mockStoreID",
				Status:        "Linked_Missing_Information",
			}

			Convey("Cache Hit", func() {
				MockedLinkedCacheClient.On("Get", broadcasterID, marketplaceID).Return(true, clientResponse).Once()
				MockedJWTHelper.On("GetSignedTokenString", Anything, Anything, Anything).Times(0)
				MockedAssociatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything).Times(0)
				wrapper := initAssociatesClientWrapper(MockedAssociatesClient, MockedJWTHelper, MockedLinkedCacheClient, MockedNotLinkedCacheClient)
				linkedStore, err := wrapper.GetLinkedStoreForTwitchUser(*MockedAssociatesContext, broadcasterID, marketplaceID, true, context)
				So(err, ShouldBeNil)
				So(linkedStore, ShouldNotBeNil)
				So(linkedStore.TUID, ShouldNotBeNil)
				So(linkedStore.MarketplaceID, ShouldNotBeNil)
				So(linkedStore.StoreID, ShouldNotBeNil)
				So(linkedStore.Status, ShouldNotBeNil)
			})

			Convey("Cache Miss", func() {
				MockedLinkedCacheClient.On("Get", broadcasterID, marketplaceID).Return(false, nil).Once()
				MockedAssociatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything).Return(clientResponse, nil)
				MockedJWTHelper.On("GetSignedTokenString", Anything, Anything, Anything).Return(jwt, nil)
				MockedLinkedCacheClient.On("Put", clientResponse).Return().Once()
				wrapper := initAssociatesClientWrapper(MockedAssociatesClient, MockedJWTHelper, MockedLinkedCacheClient, MockedNotLinkedCacheClient)

				linkedStore, err := wrapper.GetLinkedStoreForTwitchUser(*MockedAssociatesContext, broadcasterID, marketplaceID, true, context)
				So(err, ShouldBeNil)
				So(linkedStore, ShouldNotBeNil)
				So(linkedStore.TUID, ShouldNotBeNil)
				So(linkedStore.MarketplaceID, ShouldNotBeNil)
				So(linkedStore.StoreID, ShouldNotBeNil)
				So(linkedStore.Status, ShouldNotBeNil)
			})
		})

		Convey("with a tuid that has no linked store", func() {
			Convey("Cache Hit", func() {
				MockedLinkedCacheClient.On("Get", broadcasterID, marketplaceID).Return(true, &models.LinkedAssociatesStore{}).Once()
				MockedJWTHelper.On("GetSignedTokenString", Anything, Anything, Anything).Times(0)
				MockedAssociatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything).Times(0)

				wrapper := initAssociatesClientWrapper(MockedAssociatesClient, MockedJWTHelper, MockedLinkedCacheClient, MockedNotLinkedCacheClient)
				linkedStore, err := wrapper.GetLinkedStoreForTwitchUser(*MockedAssociatesContext, broadcasterID, marketplaceID, true, context)
				So(err, ShouldBeNil)
				So(linkedStore, ShouldNotBeNil)
				So(linkedStore.StoreID, ShouldBeEmpty)
			})

			Convey("Cache Miss", func() {
				MockedLinkedCacheClient.On("Get", broadcasterID, marketplaceID).Return(false, nil).Once()
				MockedAssociatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything).Return(&models.LinkedAssociatesStore{}, nil)
				MockedJWTHelper.On("GetSignedTokenString", Anything, Anything, Anything).Return(jwt, nil)
				MockedNotLinkedCacheClient.On("PutStoreInfoNotFound", broadcasterID, marketplaceID).Return().Once()
				wrapper := initAssociatesClientWrapper(MockedAssociatesClient, MockedJWTHelper, MockedLinkedCacheClient, MockedNotLinkedCacheClient)

				linkedStore, err := wrapper.GetLinkedStoreForTwitchUser(*MockedAssociatesContext, broadcasterID, marketplaceID, true, context)
				So(err, ShouldBeNil)
				So(linkedStore, ShouldNotBeNil)
				So(linkedStore.StoreID, ShouldBeEmpty)
			})

		})

		Convey("with a tuid that has a linked store with status Linked_Requires_Attention", func() {
			clientResponse := &models.LinkedAssociatesStore{
				TUID:          "mockTUID",
				MarketplaceID: "mockMarketplace",
				StoreID:       "mockStoreID",
				Status:        "Linked_Requires_Attention",
			}

			Convey("Cache Hit", func() {
				MockedLinkedCacheClient.On("Get", broadcasterID, marketplaceID).Return(true, clientResponse).Once()
				MockedJWTHelper.On("GetSignedTokenString", Anything, Anything, Anything).Times(0)
				MockedAssociatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything).Times(0)
				wrapper := initAssociatesClientWrapper(MockedAssociatesClient, MockedJWTHelper, MockedLinkedCacheClient, MockedNotLinkedCacheClient)
				linkedStore, err := wrapper.GetLinkedStoreForTwitchUser(*MockedAssociatesContext, broadcasterID, marketplaceID, true, context)
				So(err, ShouldBeNil)
				So(linkedStore, ShouldNotBeNil)
				So(linkedStore.TUID, ShouldNotBeNil)
				So(linkedStore.MarketplaceID, ShouldNotBeNil)
				So(linkedStore.StoreID, ShouldNotBeNil)
				So(linkedStore.Status, ShouldNotBeNil)
			})

			Convey("Cache Miss", func() {
				MockedLinkedCacheClient.On("Get", broadcasterID, marketplaceID).Return(false, nil).Once()
				MockedAssociatesClient.On("GetLinkedStoreForTwitchUser", Anything, Anything).Return(clientResponse, nil)
				MockedJWTHelper.On("GetSignedTokenString", Anything, Anything, Anything).Return(jwt, nil)
				MockedNotLinkedCacheClient.On("Put", clientResponse).Return().Once()
				wrapper := initAssociatesClientWrapper(MockedAssociatesClient, MockedJWTHelper, MockedLinkedCacheClient, MockedNotLinkedCacheClient)

				linkedStore, err := wrapper.GetLinkedStoreForTwitchUser(*MockedAssociatesContext, broadcasterID, marketplaceID, true, context)
				So(err, ShouldBeNil)
				So(linkedStore, ShouldNotBeNil)
				So(linkedStore.TUID, ShouldNotBeNil)
				So(linkedStore.MarketplaceID, ShouldNotBeNil)
				So(linkedStore.StoreID, ShouldNotBeNil)
				So(linkedStore.Status, ShouldNotBeNil)
			})

		})

		Convey("When JWTHelper returns an error", func() {
			MockedLinkedCacheClient.On("Get", broadcasterID, marketplaceID).Return(false, nil).Once()
			MockedJWTHelper.On("GetSignedTokenString", Anything, Anything, Anything).Return("", errors.New("test error"))
			wrapper := initAssociatesClientWrapper(MockedAssociatesClient, MockedJWTHelper, MockedLinkedCacheClient, MockedNotLinkedCacheClient)

			_, err := wrapper.GetLinkedStoreForTwitchUser(*MockedAssociatesContext, broadcasterID, marketplaceID, true, context)
			So(err, ShouldNotBeNil)
		})
	})
}
