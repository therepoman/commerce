package clients

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"

	"code.justin.tv/commerce/janus/metrics"
	log "github.com/sirupsen/logrus"
)

const (
	jsonBodyType    = "application/json"
	userNoticeRoute = "/internal/usernotice"
)

// ITMIClient interface for TMIClient
type ITMIClient interface {
	SendUserNotice(params UserNoticeParams) error
}

// TMIClient structure structure containing relevant information for calling TMI
type TMIClient struct {
	host          string
	metricsLogger metrics.IMetricLogger
}

// UserNoticeParams request params for calling TMI user notice API
type UserNoticeParams struct {
	SenderUserID      int                  `json:"sender_user_id"`
	TargetChannelID   int                  `json:"target_channel_id"`
	Body              string               `json:"body"`
	MsgID             string               `json:"msg_id"`
	MsgParams         []UserNoticeMsgParam `json:"msg_params"`
	DefaultSystemBody string               `json:"default_system_body"`
}

// UserNoticeMsgParam structure for additional params when calling TMI user notice API
type UserNoticeMsgParam struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

// NewTMIClient create a new TMI client
func NewTMIClient(host string, metricsLogger metrics.IMetricLogger) ITMIClient {
	return &TMIClient{
		host:          host,
		metricsLogger: metricsLogger,
	}
}

// SendUserNotice makes a call to TMI to display a notification message in chat
func (c *TMIClient) SendUserNotice(params UserNoticeParams) error {
	startTime := time.Now()
	defer c.metricsLogger.LogDurationSinceMetric("tmiClient.sendUserNotice", startTime)

	body, err := json.Marshal(params)
	if err != nil {
		msg := fmt.Sprintf("TMIClient: Error while marshaling UserNoticeParams %v", err)
		log.Errorf(msg)
		return errors.New(msg)
	}

	url := c.host + userNoticeRoute
	log.Infof("TMIClient: Sending user notice with body: %s", body)
	resp, err := http.Post(url, jsonBodyType, bytes.NewReader(body))
	if err != nil {
		msg := fmt.Sprintf("TMIClient: Error while creating new request for SendUserNotice %v", err)
		log.Errorf(msg)
		return errors.New(msg)
	}

	if resp.StatusCode != http.StatusOK {
		msg := fmt.Sprintf("TMIClient: TMI UserNotice request failed with http status %d while sending payload. Response body: %s", resp.StatusCode, resp.Body)
		log.Errorf(msg)
		return errors.New(msg)
	}

	return nil
}
