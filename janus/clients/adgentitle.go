package clients

import (
	"fmt"

	"errors"

	"time"

	common "code.justin.tv/commerce/ADGEntitlementServiceGoClient/src/com/amazon/adg/common/model"
	model "code.justin.tv/commerce/ADGEntitlementServiceGoClient/src/com/amazon/adg/entitlement/model"
	"code.justin.tv/commerce/janus/coral"
	"code.justin.tv/commerce/janus/metrics"
	"code.justin.tv/commerce/janus/models"
	log "github.com/sirupsen/logrus"
)

//IADGEntitleClient interface for ADGEntitleClient
type IADGEntitleClient interface {
	GetGoods(input model.GetGoodsRequest) (model.GetGoodsResponse, error)
}

// ADGEntitleClient structure containing relevant information for ADGEntitlementServiceGoClient
type ADGEntitleClient struct {
	SwsParams coral.SWSParams
}

// EntitlementStatuses typedef for returns from entitlement lookup
type EntitlementStatuses map[string]bool

// EntitlementFilterFn function to filter out goods from entitlement lookup
type EntitlementFilterFn func(good common.DigitalGood) bool

//IADGEntitleClientWrapper interface for ADGEntitleClientWrapper
type IADGEntitleClientWrapper interface {
	IsEntitled(asin, twitchID string) (bool, error)
	GetEntitledAsins(asins, twitchID string, filter EntitlementFilterFn) (EntitlementStatuses, error)
	GetCrates(cratesDomain, twitchID string) ([]models.CrateGood, error)
}

// FilterConsumables filter method for consumables
func FilterConsumables(good common.DigitalGood) bool {
	if good.Product() == nil {
		return false
	}
	if good.Product().Type() == nil {
		return false
	}
	if *(good.Product().Type()) == "Consumable" {
		return true
	}
	return false
}

// ADGEntitleClientWrapper structure containing relevant information for ADGEntitle
type ADGEntitleClientWrapper struct {
	Client        IADGEntitleClient
	SwsParams     coral.SWSParams
	MetricsLogger metrics.IMetricLogger
	RetryStrategy coral.RetryStrategy
}

var (
	clientID           = "fuel_full"
	customerIDPrefix   = "amzn1.twitch."
	asinFilterName     = "AsinFilter"
	asinListFilterName = "AsinListFilter"
	stateFilterName    = "DigitalGoodStateFilter"
	stateFilterLive    = "LIVE"
)

// GetGoods makes a GetGoods call on a new ADGEntitlementServiceGoClient
func (c *ADGEntitleClient) GetGoods(input model.GetGoodsRequest) (model.GetGoodsResponse, error) {
	// Payday pointed out that the connection from the Go Client can randomly die.
	// Creating a new client each time to avoid this problem
	// ref: https://git-aws.internal.justin.tv/commerce/payday/blob/master/coral/adgdisco/redisco.go
	client, err := newADGEntitlementServiceGoClient(c.SwsParams)
	if err != nil {
		log.Errorf("Error creating new ADGEntitlementService client: %v", err)
		return nil, err
	}
	return client.GetGoods(input)
}

// newADGEntitlementServiceGoClient creates a new ADGEntitlementServiceGoClient
func newADGEntitlementServiceGoClient(params coral.SWSParams) (*model.ADGEntitlementServiceClient, error) {
	codec, dialer, err := coral.InitCodecAndDialer(params)
	if err != nil {
		return nil, err
	}
	client := model.NewADGEntitlementServiceClient(dialer, codec)
	return client, nil
}

// GetEntitledAsins makes a call to ADGEntitlementService and retrieves information from a string of comma seperated ASINS
// IE "BT1LW7EQSG,BT1LVZES7K"
func (c *ADGEntitleClientWrapper) GetEntitledAsins(asins, twitchID string, filter EntitlementFilterFn) (EntitlementStatuses, error) {
	startTime := time.Now()
	defer c.MetricsLogger.LogDurationSinceMetric("adgEntitleClient.GetEntitledList", startTime)

	goods, err := c.newGetGoodsRequestByAsins(asins, twitchID, false)
	if err != nil {
		return nil, err
	}

	var asinMap = make(map[string]bool)
	for _, ent := range goods {
		if filter != nil && filter(ent) {
			continue
		}
		key := *ent.Product().Asin()
		asinMap[key] = true
	}

	return asinMap, nil
}

// IsEntitled makes a call to ADGEntitle and retrieve information about the asin product
func (c *ADGEntitleClientWrapper) IsEntitled(asin string, twitchID string) (bool, error) {
	startTime := time.Now()
	defer c.MetricsLogger.LogDurationSinceMetric("adgEntitleClient.isEntitled", startTime)

	goods, err := c.newGetGoodsRequestByAsins(asin, twitchID, false)
	if err != nil {
		return false, err
	}

	// Using the ASIN filter and LIVE state Filter, ADGES will return in the goods array DigitalGoods
	// for every entitlements for that particular ASIN having a LIVE state (which mean entitlement is
	// currently entitled to that person).
	// Empty array means there is no active entitlements for that ASIN, which means user isn't entitled.
	isEntitled := len(goods) != 0
	return isEntitled, nil
}

// GetCrates makes a call to ADGEntitle and retrieves information about crates entitled to the
// tuid
func (c *ADGEntitleClientWrapper) GetCrates(cratesDomain string, twitchID string) ([]models.CrateGood, error) {
	startTime := time.Now()
	defer c.MetricsLogger.LogDurationSinceMetric("adgEntitleClient.GetCrates", startTime)

	domains := []string{cratesDomain}
	goods, err := c.newGetGoodsRequestByDomains(domains, twitchID)
	if err != nil {
		return nil, err
	}

	crateGoods := make([]models.CrateGood, len(goods))
	for i, ent := range goods {
		crateGood := models.CrateGood{
			CrateID:        *ent.Id(),
			ProductID:      *ent.Product().Id(),
			ProductIconURL: *ent.Product().ProductDetails().ProductIconUrl(),
			ProductTitle:   *ent.Product().ProductTitle(),
		}
		crateGoods[i] = crateGood
	}

	return crateGoods, nil
}

func (c *ADGEntitleClientWrapper) newGetGoodsRequestByAsins(asins string, twitchID string, isList bool) ([]common.DigitalGood, error) {
	if !c.SwsParams.SWSEnabled {
		return nil, errors.New("Sws is disabled, enable it in the configuration file (be sure to have the certs)")
	}

	request := model.NewGetGoodsRequest()

	// Set Filters
	asinFilter := common.NewFilter()
	if isList {
		asinFilter.SetName(&asinFilterName)
	} else {
		asinFilter.SetName(&asinListFilterName)
	}
	asinFilter.SetInput(&asins)

	stateFilter := common.NewFilter()
	stateFilter.SetName(&stateFilterName)
	stateFilter.SetInput(&stateFilterLive)

	filters := []common.Filter{asinFilter, stateFilter}
	request.SetFilters(filters)

	clientInfo := common.NewClientInfo()
	clientInfo.SetClientId(&clientID)
	request.SetClient(clientInfo)

	customerID := fmt.Sprintf("%v%v", customerIDPrefix, twitchID)
	amazonCustomerInfo := common.NewAmazonCustomerInfo()
	amazonCustomerInfo.SetId(&customerID)
	request.SetCustomer(amazonCustomerInfo)

	var getGoodsResponse model.GetGoodsResponse
	coralCall := func() (interface{}, error) { return c.Client.GetGoods(request) }
	if err := c.RetryStrategy.ExecuteCoralCall(coralCall, &getGoodsResponse); err != nil {
		return nil, fmt.Errorf("Error occurred while calling ADGEntitlementService: %v", err)
	}

	goods := getGoodsResponse.Goods()
	if goods == nil {
		return nil, errors.New("Error with ADGEntitlementService response, Goods field is missing")
	}

	return goods, nil
}

func (c *ADGEntitleClientWrapper) newGetGoodsRequestByDomains(domainIDs []string, twitchID string) ([]common.DigitalGood, error) {
	if !c.SwsParams.SWSEnabled {
		return nil, errors.New("Sws is disabled, enable it in the configuration file (be sure to have the certs)")
	}

	request := model.NewGetGoodsRequest()

	c.applyCustomerInfo(request, clientID, twitchID)

	c.applyStateFilter(request, stateFilterName, stateFilterLive)

	var domainFilters []common.Domain
	for _, domainID := range domainIDs {
		domain := common.NewDomain()
		domain.SetId(&domainID)
		domainFilters = append(domainFilters, domain)
	}
	request.Client().SetDomains(domainFilters)

	return c.callGetGoods(request)
}

func (c *ADGEntitleClientWrapper) applyCustomerInfo(request model.GetGoodsRequest, clientID string, twitchID string) {
	clientInfo := common.NewClientInfo()
	clientInfo.SetClientId(&clientID)
	request.SetClient(clientInfo)

	customerID := fmt.Sprintf("%v%v", customerIDPrefix, twitchID)
	amazonCustomerInfo := common.NewAmazonCustomerInfo()
	amazonCustomerInfo.SetId(&customerID)
	request.SetCustomer(amazonCustomerInfo)
}

func (c *ADGEntitleClientWrapper) applyStateFilter(request model.GetGoodsRequest, stateFilterName string, stateFilterLive string) {
	stateFilter := common.NewFilter()
	stateFilter.SetName(&stateFilterName)
	stateFilter.SetInput(&stateFilterLive)

	filters := request.Filters()
	if len(filters) == 0 {
		filters = []common.Filter{stateFilter}
	} else {
		filters = append(filters, stateFilter)
	}
	request.SetFilters(filters)
}

func (c *ADGEntitleClientWrapper) callGetGoods(request model.GetGoodsRequest) ([]common.DigitalGood, error) {
	var getGoodsResponse model.GetGoodsResponse
	coralCall := func() (interface{}, error) { return c.Client.GetGoods(request) }
	if err := c.RetryStrategy.ExecuteCoralCall(coralCall, &getGoodsResponse); err != nil {
		return nil, fmt.Errorf("Error occurred while calling ADGEntitlementService: %v", err)
	}

	goods := getGoodsResponse.Goods()
	if goods == nil {
		return nil, errors.New("Error with ADGEntitlementService response, Goods field is missing")
	}

	return goods, nil
}
