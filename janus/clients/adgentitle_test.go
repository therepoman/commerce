package clients_test

import (
	"errors"
	"testing"

	common "code.justin.tv/commerce/ADGEntitlementServiceGoClient/src/com/amazon/adg/common/model"
	model "code.justin.tv/commerce/ADGEntitlementServiceGoClient/src/com/amazon/adg/entitlement/model"

	"code.justin.tv/commerce/janus/clients"
	"code.justin.tv/commerce/janus/coral"
	"code.justin.tv/commerce/janus/mocks"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

var MockedADGEntitleClient *mocks.IADGEntitleClient

func initADGEntitleClientWrapperWithRetryStrategy(mockedClient *mocks.IADGEntitleClient, mockedLogger *mocks.IMetricLogger) clients.ADGEntitleClientWrapper {
	return clients.ADGEntitleClientWrapper{
		Client:        mockedClient,
		SwsParams:     coral.SWSParams{SWSEnabled: true},
		RetryStrategy: coral.NewDefaultRetryStrategy(),
		MetricsLogger: mockedLogger,
	}
}

func goodWithAsinAndtype(asin, typ string) common.DigitalGood {
	good := common.NewDigitalGood()
	product := common.NewProduct()
	product.SetType(&typ)
	product.SetAsin(&asin)
	good.SetProduct(product)
	return good
}

func TestGetEntitledAsinsFilter(t *testing.T) {
	aec := new(mocks.IADGEntitleClient)
	logger := new(mocks.IMetricLogger)
	logger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()
	out := clients.ADGEntitleClientWrapper{
		Client:        aec,
		SwsParams:     coral.SWSParams{SWSEnabled: true},
		RetryStrategy: coral.NewTestingRetryStrategy(),
		MetricsLogger: logger,
	}
	response := model.NewGetGoodsResponse()
	aec.On("GetGoods", mock.Anything).Return(response, nil)

	Convey("example entitlements", t, func() {
		response.SetGoods([]common.DigitalGood{
			goodWithAsinAndtype("asin", "Entitlement"),
		})
		Convey("no filter function", func() {
			goods, err := out.GetEntitledAsins("asin", "tuid", nil)
			So(err, ShouldBeNil)
			So(goods, ShouldHaveLength, 1)
		})
		Convey("filter function all filter", func() {
			goods, err := out.GetEntitledAsins("asin", "tuid", func(g common.DigitalGood) bool {
				return true
			})
			So(err, ShouldBeNil)
			So(goods, ShouldBeEmpty)
		})
		Convey("filter function all skip filter", func() {
			goods, err := out.GetEntitledAsins("asin", "tuid", func(g common.DigitalGood) bool {
				return false
			})
			So(err, ShouldBeNil)
			So(goods, ShouldHaveLength, 1)
		})
	})
	Convey("consumable and non consumable entitlements", t, func() {

		good2 := common.NewDigitalGood()
		product2 := common.NewProduct()
		strTypeConsumable := "Consumable"
		product2.SetType(&strTypeConsumable)
		good2.SetProduct(product2)

		response.SetGoods([]common.DigitalGood{
			goodWithAsinAndtype("asin1", "Entitlement"),
			goodWithAsinAndtype("asin2", "Consumable"),
		})
		Convey("remove consumables", func() {
			goods, err := out.GetEntitledAsins("", "tuid", clients.FilterConsumables)
			So(err, ShouldBeNil)
			So(goods, ShouldHaveLength, 1)
		})
	})
}

func TestRetryStrategyADGEntitleSucceedsOnThirdTry(t *testing.T) {
	Convey("when making adgEntitle calls and recieving two failures and one success", t, func() {
		MockedADGEntitleClient = new(mocks.IADGEntitleClient)
		MockedIMetricLogger = new(mocks.IMetricLogger)
		adgEntitleClientWrapper := initADGEntitleClientWrapperWithRetryStrategy(MockedADGEntitleClient, MockedIMetricLogger)
		var asin string = "asin"
		var tuid string = "tuid"

		emptyResponse := model.NewGetGoodsResponse()

		response := model.NewGetGoodsResponse()
		response.SetGoods([]common.DigitalGood{})

		MockedADGEntitleClient.On("GetGoods", mock.Anything).Return(emptyResponse, errors.New(coral.HTTPFailureException)).Once()
		MockedADGEntitleClient.On("GetGoods", mock.Anything).Return(emptyResponse, errors.New(coral.HTTPFailureException)).Once()
		MockedADGEntitleClient.On("GetGoods", mock.Anything).Return(response, nil).Once()

		MockedIMetricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()

		output, err := adgEntitleClientWrapper.IsEntitled(asin, tuid)

		So(err, ShouldBeNil)
		So(output, ShouldBeFalse)
		So(MockedADGEntitleClient.AssertNumberOfCalls(t, "GetGoods", 3), ShouldBeTrue)

		Convey("when making adgEntitle calls with a filter list", func() {
			var ASIN = "123"
			var result = clients.EntitlementStatuses{
				"123": true,
			}

			goods := []common.DigitalGood{}

			good := common.NewDigitalGood()
			product := common.NewProduct()
			common.Product.SetAsin(product, &ASIN)
			good.SetProduct(product)

			goods = append(goods, good)
			response.SetGoods(goods)

			MockedADGEntitleClient.On("GetGoods", mock.Anything).Return(response, nil).Once()
			MockedIMetricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()

			output, err := adgEntitleClientWrapper.GetEntitledAsins(asin, tuid, nil)

			So(err, ShouldBeNil)
			So(output, ShouldResemble, result)
			So(MockedADGEntitleClient.AssertNumberOfCalls(t, "GetGoods", 4), ShouldBeTrue)

			Convey("with an error from GetGoods after failing 3 times", func() {
				MockedADGEntitleClient.On("GetGoods", mock.Anything).Return(nil, errors.New(coral.HTTPFailureException))
				MockedIMetricLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()

				output, err := adgEntitleClientWrapper.GetEntitledAsins(asin, tuid, nil)

				So(err, ShouldNotBeNil)
				So(output, ShouldBeNil)
				So(MockedADGEntitleClient.AssertNumberOfCalls(t, "GetGoods", 7), ShouldBeTrue)
			})
		})
	})
}
