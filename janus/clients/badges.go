package clients

// TODO: FUEL-3029 - Add tests
import (
	"time"

	"strconv"

	"code.justin.tv/commerce/janus/metrics"
	"code.justin.tv/commerce/janus/models"
	"code.justin.tv/common/twitchhttp"
	"github.com/pkg/errors"
	"golang.org/x/net/context"
)

const (
	getBadgeDisplayURI = "/v1/badges/global/display"
)

//IChatBadgesClientWrapper interface for chat badges
type IChatBadgesClientWrapper interface {
	GetBadgeDisplay(ctx context.Context) (*models.GetBadgeDisplay, error)
}

// ChatBadgesClientWrapper structure containing relevant information for the client to call chat badges
type ChatBadgesClientWrapper struct {
	ChatBadgesClient IChatBadgesClient
	MetricsLogger    metrics.IMetricLogger
}

//IChatBadgesClient interface for chat badges
type IChatBadgesClient interface {
	GetBadgeDisplay(ctx context.Context) (*models.GetBadgeDisplay, error)
}

// ChatBadgesClient structure containing relevant information for the client to call chat badges
type ChatBadgesClient struct {
	ExternalChatBadgesEndpoint string
	InternalChatBadgesEndpoint string
}

// GetBadgeDisplay get general display information about chat badges
func (c *ChatBadgesClientWrapper) GetBadgeDisplay(ctx context.Context) (*models.GetBadgeDisplay, error) {
	startTime := time.Now()
	defer c.MetricsLogger.LogDurationSinceMetric("chatBadgesClient.GetBadgeDisplay", startTime)

	answer, err := c.ChatBadgesClient.GetBadgeDisplay(ctx)

	return answer, err
}

// GetBadgeDisplay get general display information about chat badges
func (c *ChatBadgesClient) GetBadgeDisplay(ctx context.Context) (*models.GetBadgeDisplay, error) {
	client, err := newExternalChatBadgesClient(c.ExternalChatBadgesEndpoint)
	if err != nil {
		return nil, err
	}

	url := getBadgeDisplayURI
	req, err := client.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	var getBadgeDisplay models.GetBadgeDisplay
	_, err = client.DoJSON(ctx, &getBadgeDisplay, req, twitchhttp.ReqOpts{})
	if err != nil {
		return nil, err
	}

	return &getBadgeDisplay, nil
}

func newExternalChatBadgesClient(endpoint string) (twitchhttp.Client, error) {
	conf := twitchhttp.ClientConf{Host: endpoint}

	twitchClient, err := twitchhttp.NewClient(conf)
	if err != nil {
		return nil, err
	}
	return twitchClient, nil
}

// GetHighestBadgeVersion parses a map of string keys into integers and returns the string key with the highest numeric value
func GetHighestBadgeVersion(badges map[string]models.BadgeVersion) (string, error) {
	// we must be careful to return the exact string that came in as input, so we carry two result variables.
	// e.g. Itoa(Atoi("055 ")) does not equal "055 "
	var resultInt int
	var resultString string

	var found bool
	for keyString := range badges {
		keyInt, err := strconv.Atoi(keyString)
		if err == nil {
			if !found {
				resultString, resultInt = keyString, keyInt
				found = true
			} else {
				if resultInt < keyInt {
					resultString, resultInt = keyString, keyInt
				}
			}
		}
	}
	if !found {
		return "", errors.New("no numeric keys found")
	}
	return resultString, nil
}
