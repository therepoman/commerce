package clients

import (
	"net/http"

	"fmt"

	"time"

	"code.justin.tv/commerce/janus/metrics"
	"code.justin.tv/commerce/janus/models"
	mako "code.justin.tv/commerce/mako-client"
	ctx "golang.org/x/net/context"
)

const (
	emoticonsScope = "/emote_sets/commerce/"
	badgesScope    = "/badge_sets/commerce/"
)

// IMakoClientWrapper interface for MakoClientWrapper
type IMakoClientWrapper interface {
	GetEmoticonSets(context ctx.Context, userID string) (*models.MakoEntitlementSets, error)
	GetBadgeSets(context ctx.Context, userID string) (*models.MakoEntitlementSets, error)
	GetEmoticonsByGroups(context ctx.Context, emoticonGroupKeys []string) (*models.ChatEmoticonDetails, error)
}

// MakoClientWrapper structure containing relevant information for Mako calls
type MakoClientWrapper struct {
	MakoClient    IMakoClient
	MetricsLogger metrics.IMetricLogger
}

//IMakoClient interface for MakoClient
type IMakoClient interface {
	GetEntitlements(context ctx.Context, userID string, scope string) (*models.MakoEntitlementSets, error)
	GetEmoticonsByGroups(context ctx.Context, emoticonGroupKeys []string) (*models.ChatEmoticonDetails, error)
}

// MakoClient structure containing relevant information for the client to call Mako
type MakoClient struct {
	MakoEndpoint string
}

// GetBadgeSets calls Mako to retrieve all badges for this user
func (c *MakoClientWrapper) GetBadgeSets(context ctx.Context, userID string) (*models.MakoEntitlementSets, error) {
	startTime := time.Now()
	defer c.MetricsLogger.LogDurationSinceMetric("makoClient.GetBadgeSets", startTime)

	resp, err := c.MakoClient.GetEntitlements(context, userID, badgesScope)
	if err != nil {
		return nil, fmt.Errorf("Error calling GetEntitlements for badges: %v", err)
	}

	return resp, nil
}

// GetEmoticonSets calls Mako to retrieve all emoticons for this user
func (c *MakoClientWrapper) GetEmoticonSets(context ctx.Context, userID string) (*models.MakoEntitlementSets, error) {
	startTime := time.Now()
	defer c.MetricsLogger.LogDurationSinceMetric("makoClient.GetEmoticonSets", startTime)

	resp, err := c.MakoClient.GetEntitlements(context, userID, emoticonsScope)
	if err != nil {
		return nil, fmt.Errorf("Error calling GetEntitlements for emoticons: %v", err)
	}

	return resp, nil
}

// GetEmoticonsByGroups calls Mako to retrieve all emoticons for the given groups
func (c *MakoClientWrapper) GetEmoticonsByGroups(context ctx.Context, emoticonGroupKeys []string) (*models.ChatEmoticonDetails, error) {
	startTime := time.Now()
	defer c.MetricsLogger.LogDurationSinceMetric("makoClient.GetEmoticonsByGroups", startTime)

	resp, err := c.MakoClient.GetEmoticonsByGroups(context, emoticonGroupKeys)
	if err != nil {
		return nil, fmt.Errorf("Error calling GetEmoticonsByGroups for emoticons: %v", err)
	}

	return resp, nil
}

// GetEmoticonsByGroups calls Mako to retrieve all emoticons for the given groups
func (c *MakoClient) GetEmoticonsByGroups(context ctx.Context, emoticonGroupKeys []string) (*models.ChatEmoticonDetails, error) {
	request := mako.GetEmoticonsByGroupsRequest{
		EmoticonGroupKeys: emoticonGroupKeys,
	}
	client := mako.NewMakoProtobufClient(c.MakoEndpoint, &http.Client{})

	// network call
	response, err := client.GetEmoticonsByGroups(context, &request)
	if err != nil {
		return nil, err
	}

	modelResponse := models.ChatEmoticonDetails{}
	modelResponse.Details = make(map[string][]models.Emoticon)

	for _, emoticonGroup := range response.Groups {
		emoticons := make([]models.Emoticon, 0)
		for _, groupEmoticon := range emoticonGroup.Emoticons {
			emoticon := models.Emoticon{
				ID:      groupEmoticon.Id,
				Pattern: groupEmoticon.Code,
			}
			emoticons = append(emoticons, emoticon)
		}
		modelResponse.Details[emoticonGroup.Id] = emoticons
	}

	return &modelResponse, nil
}

// GetEntitlements calls Mako to retrieve all entitlements for this user
func (c *MakoClient) GetEntitlements(context ctx.Context, userID string, scope string) (*models.MakoEntitlementSets, error) {

	request := mako.GetEntitlementsRequest{
		UserId: userID,
		EntitlementScopedKey: &mako.ScopedKey{
			Scope: scope,
		},
	}
	client := mako.NewMakoProtobufClient(c.MakoEndpoint, &http.Client{})

	// network call
	response, err := client.GetEntitlements(context, &request)
	if err != nil {
		return &models.MakoEntitlementSets{}, err
	}

	var sets []string
	for _, entitlement := range response.GetEntitlements() {
		sets = append(sets, entitlement.ScopedKey.Key)
	}

	return &models.MakoEntitlementSets{
		Keys: sets,
	}, nil
}
