package clients

import (
	"encoding/json"
	"time"

	pubsub "code.justin.tv/chat/pubsub-go-pubclient/client"
	"code.justin.tv/commerce/janus/metrics"
	"code.justin.tv/common/twitchhttp"
	"golang.org/x/net/context"
)

const (
	// ChatNotificationPubSubMessageType message type for chat notification pubsub messages
	ChatNotificationPubSubMessageType = "chat-notification"

	// UserCommerceEventsTopicPrefix topic for pubsub messages involving a user's commerce events (ex: purchases)
	UserCommerceEventsTopicPrefix = "user-commerce-events"

	// ChannelCommerceEventMessageType message type for chat user's share in chat
	ChannelCommerceEventMessageType = "chat-share"

	// ChannelCommerceEventsTopicPrefix topic for pubsub messages involving a user's share in chat of commerce events such as purchase
	ChannelCommerceEventsTopicPrefix = "channel-commerce-events-v1"

	// ExtensionPurchaseMessageType message type for extension purchase pubsub messages
	ExtensionPurchaseMessageType = "user-extensionpurchase"

	// ExtensionPurchaseTopicPrefix topic for pubsub messages involving a user's purchase of an Extension product
	ExtensionPurchaseTopicPrefix = "user-extensionpurchase-events"
)

// PubSubMessage represents a generic pubsub message
type PubSubMessage struct {
	MessageType     string           `json:"type"`
	MessageContents *json.RawMessage `json:"msg"`
	Time            time.Time        `json:"time"`
}

// IPubSubWrapper interface for PubSubWrapper
type IPubSubWrapper interface {
	Publish(ctx context.Context, topics []string, messageType string, messageJSON []byte, opt *twitchhttp.ReqOpts) error
}

// PubSubWrapper struct for making pubsub calls
type PubSubWrapper struct {
	pubsub        pubsub.PubClient
	MetricsLogger metrics.IMetricLogger
}

// NewPubSubWrapper creates a new PubSubWrapper
func NewPubSubWrapper(pubSubClient pubsub.PubClient, metricsLogger metrics.IMetricLogger) IPubSubWrapper {
	return &PubSubWrapper{
		pubsub:        pubSubClient,
		MetricsLogger: metricsLogger,
	}
}

// Publish publishes a message to pubsub for a specific set of topics
func (p *PubSubWrapper) Publish(ctx context.Context, topics []string, messageType string, messageJSON []byte, opt *twitchhttp.ReqOpts) error {
	startTime := time.Now()
	defer p.MetricsLogger.LogDurationSinceMetric("pubsub.publish", startTime)

	rawJSON := json.RawMessage(messageJSON)
	pubsubMessage := PubSubMessage{
		MessageType:     messageType,
		MessageContents: &rawJSON,
		Time:            time.Now(),
	}
	fullJSON, err := json.Marshal(pubsubMessage)
	if err != nil {
		return err
	}

	return p.pubsub.Publish(ctx, topics, string(fullJSON), opt)
}
