package clients

import (
	"errors"
	"fmt"
	"time"

	svc "code.justin.tv/commerce/TwitchAmazonIdentityLinkingServiceGoClient/src/tv/justin/tails"
	"code.justin.tv/commerce/janus/coral"
	"code.justin.tv/commerce/janus/metrics"
	log "github.com/sirupsen/logrus"
)

// ITailsClient interface for TwitchAmazonIdentityLinkingServiceGoClient
type ITailsClient interface {
	GetLinkedAmazonDirectedId(input svc.GetLinkedAmazonDirectedIdRequest) (svc.GetLinkedAmazonDirectedIdResponse, error)
}

// TailsClient structure containing relevant information for TwitchAmazonIdentityLinkingServiceGoClient
type TailsClient struct {
	SWSParams coral.SWSParams
}

// ITailsClientWrapper interface for TailsClientWrapper
type ITailsClientWrapper interface {
	GetLinkedAmazonCOR(userID string) (string, error)
}

// TailsClientWrapper structure containing relevant information for Tails
type TailsClientWrapper struct {
	Client        ITailsClient
	SWSParams     coral.SWSParams
	MetricsLogger metrics.IMetricLogger
	IsDevelopment bool
	RetryStrategy coral.RetryStrategy
}

// GetLinkedAmazonDirectedId makes a GetLinkedAmazonDirectedId call on a new TailsClient
func (c *TailsClient) GetLinkedAmazonDirectedId(input svc.GetLinkedAmazonDirectedIdRequest) (svc.GetLinkedAmazonDirectedIdResponse, error) {
	client, err := newTailsClient(c.SWSParams)
	if err != nil {
		log.Errorf("Error creating new Tails client: %v", err)
		return nil, err
	}
	return client.GetLinkedAmazonDirectedId(input)
}

func newTailsClient(params coral.SWSParams) (*svc.TwitchAmazonIdentityLinkingServiceClient, error) {
	codec, dialer, err := coral.InitCodecAndDialer(params)
	if err != nil {
		return nil, err
	}
	client := svc.NewTwitchAmazonIdentityLinkingServiceClient(dialer, codec)
	return client, nil
}

// GetLinkedAmazonCOR is given a user ID and fetches the COR for the linked Amazon account if an account is linked
func (c *TailsClientWrapper) GetLinkedAmazonCOR(userID string) (string, error) {
	startTime := time.Now()
	defer c.MetricsLogger.LogDurationSinceMetric("tailsClient.getLinkedAmazonAccount", startTime)

	if userID == "" {
		return "", errors.New("No linked account for unknown user")
	}

	if !c.SWSParams.SWSEnabled {
		if c.IsDevelopment {
			log.Infof("SWS disabled, returning stubbed Tails response for development")
			return defaultCOR, nil
		}

		return "", errors.New("SWS disabled, enable it in the configuration file (be sure to have the certs)")
	}

	request := svc.NewGetLinkedAmazonDirectedIdRequest()
	request.SetTwitchUserId(&userID)
	includeCor := true
	request.SetIncludingCOR(&includeCor)

	var getLinkedAmazonDirectedIDResponse svc.GetLinkedAmazonDirectedIdResponse
	coralCall := func() (interface{}, error) { return c.Client.GetLinkedAmazonDirectedId(request) }
	if err := c.RetryStrategy.ExecuteCoralCall(coralCall, &getLinkedAmazonDirectedIDResponse); err != nil {
		return "", fmt.Errorf("Error occurred while calling Tails: %v", err)
	}

	hasLinkedAccount := getLinkedAmazonDirectedIDResponse.HasLinkedAccount()
	if *hasLinkedAccount {
		if cor := getLinkedAmazonDirectedIDResponse.CountryOfResidence(); cor != nil {
			return *cor, nil
		}
		return "", errors.New("No COR in linked account for user")
	}
	return "", errors.New("No linked account for user")
}
