package payouts_test

import (
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/janus/cache"
	"code.justin.tv/commerce/janus/dynamo"
	"code.justin.tv/commerce/janus/mocks"
	"code.justin.tv/commerce/janus/models"
	"code.justin.tv/commerce/janus/payouts"

	"code.justin.tv/revenue/moneypenny/client"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	ctx "golang.org/x/net/context"
)

// Test PayoutsEligibilityChecker GetGameCommercePayoutEligibility
func TestGetGameCommercePayoutEligibility(t *testing.T) {

	Convey("With new PayoutsEligibilityChecker", t, func() {
		amendmentDAO := new(mocks.IAmendmentDAO)
		moneyPennyClient := new(mocks.IMoneyPennyClient)
		associatesClient := new(mocks.IAssociatesClientWrapper)
		mockedPayoutsContext := new(ctx.Context)
		metricsLogger := new(mocks.IMetricLogger)
		moneyPennyCache := new(mocks.IMoneyPennyCache)
		payoutsEligibilityChecker := payouts.NewPayoutsEligibilityChecker(amendmentDAO, associatesClient, "game_commerce", "game_commerce_v2", metricsLogger, moneyPennyClient, moneyPennyCache)

		metricsLogger.On("LogDurationSinceMetric", mock.Anything, mock.Anything).Return()

		isNotAffiliateResponse := &moneypenny.GetUserPayoutTypeResponse{IsAffiliate: false}
		isAffiliateResponse := &moneypenny.GetUserPayoutTypeResponse{IsAffiliate: true}
		cacheEntry := cache.IsAffiliateCacheEntry{IsAffiliate: true}

		Convey("When calling GetGameCommercePayoutEligibility", func() {
			Convey("when the broadcaster has signed amendment", func() {
				amendmentDAO.On("Get", mock.Anything, "game_commerce").Return(&dynamo.Amendment{
					UserID:         "123",
					AmendmentType:  "game_commerce",
					AcceptanceDate: time.Now(),
				}, nil)

				payoutEligible, err := payoutsEligibilityChecker.GetGameCommercePayoutEligibility("123")
				So(err, ShouldBeNil)
				So(payoutEligible, ShouldBeTrue)
			})

			Convey("when the broadcaster has signed v2 amendment", func() {
				amendmentDAO.On("Get", mock.Anything, "game_commerce").Return(nil, nil)
				amendmentDAO.On("Get", mock.Anything, "game_commerce_v2").Return(&dynamo.Amendment{
					UserID:         "123",
					AmendmentType:  "game_commerce_v2",
					AcceptanceDate: time.Now(),
				}, nil)

				payoutEligible, err := payoutsEligibilityChecker.GetGameCommercePayoutEligibility("123")
				So(err, ShouldBeNil)
				So(payoutEligible, ShouldBeTrue)
			})

			Convey("when the broadcaster is an affiliate", func() {
				amendmentDAO.On("Get", mock.Anything, mock.Anything).Return(nil, nil)
				moneyPennyCache.On("GetIsAffiliate", mock.Anything).Return(false, cache.IsAffiliateCacheEntry{})
				moneyPennyCache.On("SetIsAffiliate", mock.Anything, mock.Anything).Return()
				moneyPennyClient.On("GetUserPayoutType", mock.Anything, mock.Anything, mock.Anything).Return(isAffiliateResponse, nil)

				payoutEligible, err := payoutsEligibilityChecker.GetGameCommercePayoutEligibility("123")
				So(err, ShouldBeNil)
				So(payoutEligible, ShouldBeTrue)
				moneyPennyCache.AssertCalled(t, "SetIsAffiliate", mock.Anything, mock.Anything)
			})

			Convey("when the broadcaster is an affiliate and hit the cache", func() {
				amendmentDAO.On("Get", mock.Anything, mock.Anything).Return(nil, nil)
				moneyPennyCache.On("GetIsAffiliate", mock.Anything).Return(true, cacheEntry)
				moneyPennyClient.On("GetUserPayoutType", mock.Anything, mock.Anything, mock.Anything).Return(isAffiliateResponse, nil)

				payoutEligible, err := payoutsEligibilityChecker.GetGameCommercePayoutEligibility("123")
				So(err, ShouldBeNil)
				So(payoutEligible, ShouldBeTrue)
				moneyPennyCache.AssertNotCalled(t, "SetIsAffiliate", mock.Anything, mock.Anything)
			})

			Convey("when the broadcaster has not signed either amendment and is not affiliate", func() {
				amendmentDAO.On("Get", mock.Anything, mock.Anything).Return(nil, nil)
				moneyPennyCache.On("GetIsAffiliate", mock.Anything).Return(false, cache.IsAffiliateCacheEntry{})
				moneyPennyCache.On("SetIsAffiliate", mock.Anything, mock.Anything).Return()
				moneyPennyClient.On("GetUserPayoutType", mock.Anything, mock.Anything, mock.Anything).Return(isNotAffiliateResponse, nil)

				payoutEligible, err := payoutsEligibilityChecker.GetGameCommercePayoutEligibility("123")
				So(err, ShouldBeNil)
				So(payoutEligible, ShouldBeFalse)
				moneyPennyCache.AssertCalled(t, "SetIsAffiliate", mock.Anything, mock.Anything)
			})

			Convey("when AmendmentDAO throws an error", func() {
				amendmentDAO.On("Get", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

				payoutEligible, err := payoutsEligibilityChecker.GetGameCommercePayoutEligibility("123")
				So(err, ShouldNotBeNil)
				So(payoutEligible, ShouldBeFalse)
			})

			Convey("when moneypenny throws an error", func() {
				amendmentDAO.On("Get", mock.Anything, mock.Anything).Return(nil, nil)
				moneyPennyCache.On("GetIsAffiliate", mock.Anything).Return(false, cache.IsAffiliateCacheEntry{})
				moneyPennyClient.On("GetUserPayoutType", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

				payoutEligible, err := payoutsEligibilityChecker.GetGameCommercePayoutEligibility("123")
				So(err, ShouldNotBeNil)
				So(payoutEligible, ShouldBeFalse)
				moneyPennyCache.AssertNotCalled(t, "SetIsAffiliate", mock.Anything, mock.Anything)
			})
		})

		Convey("When calling GetRetailPayoutEligibility", func() {
			Convey("when the broadcaster has signed amendment", func() {
				amendmentDAO.On("Get", mock.Anything, "game_commerce_v2").Return(&dynamo.Amendment{
					UserID:         "123",
					AmendmentType:  "game_commerce_v2",
					AcceptanceDate: time.Now(),
				}, nil)

				Convey("when the broadcaster has valid storeID", func() {
					associatesResponse := &models.LinkedAssociatesStore{TUID: "mockTUID", StoreID: "mockStoreID", Status: "Linked"}
					associatesClient.On("GetLinkedStoreForTwitchUser", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(associatesResponse, nil)
					payoutEligible, err := payoutsEligibilityChecker.GetRetailPayoutEligibility(*mockedPayoutsContext, "123")
					So(err, ShouldBeNil)
					So(payoutEligible, ShouldBeTrue)
				})

				Convey("when the broadcaster has no valid storeID", func() {
					associatesResponse := &models.LinkedAssociatesStore{TUID: "mockTUID", StoreID: "mockStoreID", Status: "Linked_Requires_Attention"}
					associatesClient.On("GetLinkedStoreForTwitchUser", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(associatesResponse, nil)
					payoutEligible, err := payoutsEligibilityChecker.GetRetailPayoutEligibility(*mockedPayoutsContext, "123")
					So(err, ShouldBeNil)
					So(payoutEligible, ShouldBeFalse)
				})
			})

			Convey("when the broadcaster is an affiliate", func() {
				amendmentDAO.On("Get", mock.Anything, mock.Anything).Return(nil, nil)
				moneyPennyCache.On("GetIsAffiliate", mock.Anything).Return(false, cache.IsAffiliateCacheEntry{})
				moneyPennyCache.On("SetIsAffiliate", mock.Anything, mock.Anything).Return()
				moneyPennyClient.On("GetUserPayoutType", mock.Anything, mock.Anything, mock.Anything).Return(isAffiliateResponse, nil)

				Convey("when the broadcaster has valid storeID", func() {
					associatesResponse := &models.LinkedAssociatesStore{TUID: "mockTUID", StoreID: "mockStoreID", Status: "Linked"}
					associatesClient.On("GetLinkedStoreForTwitchUser", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(associatesResponse, nil)
					payoutEligible, err := payoutsEligibilityChecker.GetRetailPayoutEligibility(*mockedPayoutsContext, "123")
					So(err, ShouldBeNil)
					So(payoutEligible, ShouldBeTrue)
					moneyPennyCache.AssertCalled(t, "SetIsAffiliate", mock.Anything, mock.Anything)
				})

				Convey("when the broadcaster has no valid storeID", func() {
					associatesResponse := &models.LinkedAssociatesStore{TUID: "mockTUID", StoreID: "mockStoreID", Status: "Linked_Requires_Attention"}
					associatesClient.On("GetLinkedStoreForTwitchUser", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(associatesResponse, nil)
					payoutEligible, err := payoutsEligibilityChecker.GetRetailPayoutEligibility(*mockedPayoutsContext, "123")
					So(err, ShouldBeNil)
					So(payoutEligible, ShouldBeFalse)
					moneyPennyCache.AssertCalled(t, "SetIsAffiliate", mock.Anything, mock.Anything)
				})
			})

			Convey("when the broadcaster has not signed amendment and is not affiliate", func() {
				amendmentDAO.On("Get", mock.Anything, mock.Anything).Return(nil, nil)
				moneyPennyCache.On("GetIsAffiliate", mock.Anything).Return(false, cache.IsAffiliateCacheEntry{})
				moneyPennyCache.On("SetIsAffiliate", mock.Anything, mock.Anything).Return()
				moneyPennyClient.On("GetUserPayoutType", mock.Anything, mock.Anything, mock.Anything).Return(isNotAffiliateResponse, nil)

				payoutEligible, err := payoutsEligibilityChecker.GetRetailPayoutEligibility(*mockedPayoutsContext, "123")
				So(err, ShouldBeNil)
				So(payoutEligible, ShouldBeFalse)
				moneyPennyCache.AssertCalled(t, "SetIsAffiliate", mock.Anything, mock.Anything)
			})

			Convey("when AmendmentDAO throws an error", func() {
				amendmentDAO.On("Get", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

				payoutEligible, err := payoutsEligibilityChecker.GetRetailPayoutEligibility(*mockedPayoutsContext, "123")
				So(err, ShouldNotBeNil)
				So(payoutEligible, ShouldBeFalse)
			})

			Convey("when moneypenny throws an error", func() {
				amendmentDAO.On("Get", mock.Anything, mock.Anything).Return(nil, nil)
				moneyPennyCache.On("GetIsAffiliate", mock.Anything).Return(false, cache.IsAffiliateCacheEntry{})
				moneyPennyClient.On("GetUserPayoutType", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

				payoutEligible, err := payoutsEligibilityChecker.GetRetailPayoutEligibility(*mockedPayoutsContext, "123")
				So(err, ShouldNotBeNil)
				So(payoutEligible, ShouldBeFalse)
				moneyPennyCache.AssertNotCalled(t, "SetIsAffiliate", mock.Anything, mock.Anything)
			})

			Convey("when the associates client throws an error", func() {
				amendmentDAO.On("Get", mock.Anything, "game_commerce_v2").Return(&dynamo.Amendment{
					UserID:         "123",
					AmendmentType:  "game_commerce_v2",
					AcceptanceDate: time.Now(),
				}, nil)
				associatesClient.On("GetLinkedStoreForTwitchUser", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

				payoutEligible, err := payoutsEligibilityChecker.GetRetailPayoutEligibility(*mockedPayoutsContext, "123")
				So(err, ShouldNotBeNil)
				So(payoutEligible, ShouldBeFalse)
			})
		})
	})
}
