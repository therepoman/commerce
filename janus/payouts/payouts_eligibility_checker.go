package payouts

import (
	"time"

	"code.justin.tv/commerce/janus/cache"
	"code.justin.tv/commerce/janus/dynamo"
	"code.justin.tv/commerce/janus/metrics"

	"context"

	"code.justin.tv/commerce/janus/associates"
	"code.justin.tv/commerce/janus/clients"
	moneypenny "code.justin.tv/revenue/moneypenny/client"
	log "github.com/sirupsen/logrus"

	ctx "golang.org/x/net/context"
)

const (
	defaultAssociatesMarketplace = "ATVPDKIKX0DER"
	registrationContext          = "registration"
)

type IPayoutsEligibilityChecker interface {
	GetGameCommercePayoutEligibility(userID string) (bool, error)
	GetRetailPayoutEligibility(c ctx.Context, userID string) (bool, error)
}

type PayoutsEligibilityChecker struct {
	AmendmentDAO                dynamo.IAmendmentDAO
	AssociatesClient            clients.IAssociatesClientWrapper
	GameCommerceAmendmentType   string
	GameCommerceV2AmendmentType string
	MetricLogger                metrics.IMetricLogger
	MoneyPennyClient            moneypenny.Client
	MoneyPennyCache             cache.IMoneyPennyCache
}

func NewPayoutsEligibilityChecker(amendmentDao dynamo.IAmendmentDAO, associatesClient clients.IAssociatesClientWrapper, GameCommerceAmendmentType string, GameCommerceV2AmendmentType string, metricsLogger metrics.IMetricLogger, moneyPennyClient moneypenny.Client, moneyPennyCache cache.IMoneyPennyCache) *PayoutsEligibilityChecker {

	return &PayoutsEligibilityChecker{
		AmendmentDAO:                amendmentDao,
		AssociatesClient:            associatesClient,
		GameCommerceAmendmentType:   GameCommerceAmendmentType,
		GameCommerceV2AmendmentType: GameCommerceV2AmendmentType,
		MetricLogger:                metricsLogger,
		MoneyPennyClient:            moneyPennyClient,
		MoneyPennyCache:             moneyPennyCache,
	}
}

// GetGameCommercePayoutEligibility determines whether a specific TUID is payout eligibile for fuel game commerce.
func (payoutsEligibilityChecker *PayoutsEligibilityChecker) GetGameCommercePayoutEligibility(userID string) (bool, error) {
	hasSignedGameCommerceAmendment, err := payoutsEligibilityChecker.AmendmentDAO.Get(userID, payoutsEligibilityChecker.GameCommerceAmendmentType)
	if err != nil {
		log.Errorf("Error retrieving amendment from DAO for channel: %s, error: %s", userID, err)
		return false, err
	}
	if hasSignedGameCommerceAmendment != nil {
		return true, nil
	}

	hasSignedGameCommerceV2Amendment, err := payoutsEligibilityChecker.AmendmentDAO.Get(userID, payoutsEligibilityChecker.GameCommerceV2AmendmentType)
	if err != nil {
		log.Errorf("Error retrieving amendment from DAO for channel: %s, error: %s", userID, err)
		return false, err
	}
	if hasSignedGameCommerceV2Amendment != nil {
		return true, nil
	}

	// if user has not signed partner amendment, check if user is an affiliate
	return payoutsEligibilityChecker.isAffiliate(userID)
}

// GetRetailPayoutEligibility determines whether a specific TUID is payout eligibile for amazon retail
func (payoutsEligibilityChecker *PayoutsEligibilityChecker) GetRetailPayoutEligibility(c ctx.Context, userID string) (bool, error) {
	hasSignedAmendment, err := payoutsEligibilityChecker.AmendmentDAO.Get(userID, payoutsEligibilityChecker.GameCommerceV2AmendmentType)
	if err != nil {
		log.Errorf("Error retrieving amendment from DAO for channel: %s, error: %s", userID, err)
		return false, err
	}

	isRetailPayoutEligible := hasSignedAmendment != nil

	// if user has not signed partner amendment, check if user is an affiliate
	if !isRetailPayoutEligible {
		isAffiliate, err := payoutsEligibilityChecker.isAffiliate(userID)
		if err != nil {
			return false, err
		}
		isRetailPayoutEligible = isAffiliate
	}

	hasRevEnabledStoreID := false
	if isRetailPayoutEligible {
		linkedStore, err := payoutsEligibilityChecker.AssociatesClient.GetLinkedStoreForTwitchUser(c, userID, defaultAssociatesMarketplace, true, registrationContext)
		if err != nil {
			log.WithFields(log.Fields{
				"broadcasterId": userID,
				"error":         err,
			}).Error("Error getting associates store for broadcaster")
			return false, err
		}
		if linkedStore != nil && linkedStore.StoreID != "" {
			hasRevEnabledStoreID = associates.IsStoreStatusRevEnabled(linkedStore.Status)
		}
	}

	return hasRevEnabledStoreID, nil
}

func (payoutsEligibilityChecker *PayoutsEligibilityChecker) isAffiliate(userID string) (bool, error) {
	isCacheHit, cacheEntry := payoutsEligibilityChecker.MoneyPennyCache.GetIsAffiliate(userID)
	if isCacheHit {
		return cacheEntry.IsAffiliate, nil
	}

	startTime := time.Now()
	resp, err := payoutsEligibilityChecker.MoneyPennyClient.GetUserPayoutType(context.Background(), userID, nil)
	payoutsEligibilityChecker.MetricLogger.LogDurationSinceMetric("moneyPennyClient.getUserPayoutType", startTime)

	if err != nil {
		log.Errorf("Error calling moneyPennyClient.IsAffiliate: %s", err)
		return false, err
	}

	payoutsEligibilityChecker.MoneyPennyCache.SetIsAffiliate(userID, resp.IsAffiliate)

	return resp.IsAffiliate, nil
}
