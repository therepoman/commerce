package dockerfile

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"sort"
	"strings"

	"code.justin.tv/release/manta-style/pkg/common"
)

const (
	MANTA_BUILD_SCRIPT      = "manta_build.sh"
	DEFAULT_BUNDLER_VERSION = "1.9.9"
	DEFAULT_OS              = "ubuntu"
)

// LoadConfig loads a list of files onto a config struct, overwriting old values if they are duplicated
func LoadConfig(filenames []string) (*common.Config, error) {
	config := &common.Config{
		NetworkedBuild: true, // Set default of network flag to true. Json will use this value if none present.
	}

	if len(filenames) == 0 {
		return config, fmt.Errorf("No files supplied")
	}

	for _, filename := range filenames {
		f, err := os.Open(filename)
		if err != nil {
			return nil, fmt.Errorf("error reading %s file: %s\n", filename, err)
		}

		if err := json.NewDecoder(f).Decode(config); err != nil {
			return config, fmt.Errorf("json parse error while reading %s: %s", filename, err)
		}

		// Migration Support, env's should be a list however they were
		// originally a map. This block will be removed in a future version
		if config.Env == nil {
			config.EnvList = make([]string, 0)
			continue
		}

		switch env := config.Env.(type) {
		case map[string]interface{}:
			log.Println("WARNING: Env as a map is deprecated")
			for k, v := range env {
				config.EnvList = append(config.EnvList, fmt.Sprintf("%s=%s", k, v))
			}
		case []interface{}:
			for _, v := range env {
				vStr, ok := v.(string)
				if !ok {
					log.Println(fmt.Sprintf("WARNING: %s is not a string: %t. Skipping.", v, v))
					continue
				}
				config.EnvList = append(config.EnvList, vStr)
			}
		default:
			log.Printf("Error loading config %q don't know how to handle: %v of type %t", filename, env, env)
		}
	}

	if config.OS == "" {
		config.OS = DEFAULT_OS
	}
	config.OS = strings.ToLower(config.OS)

	if _, err := os.Stat("bower.json"); config.Yarn != nil && err == nil {
		config.Yarn.IncludeBower = true
	}

	return config, nil
}

// SingleImage returns the contents of a Dockerfile meant for a run using one image.
func SingleImage(config common.Config, path string) *bytes.Buffer {
	out := new(bytes.Buffer)

	err := addSetupSteps(out, config, path)
	if err != nil {
		log.Fatalf("setup step failed: %v", err)
	}
	createBuildScript(config, path)
	addBuildSteps(out, config, path)

	return out
}

func ShouldUseMultiImage(config common.Config) (useMultiImage bool, useFakeTimestamp bool) {
	if config.Ruby != nil {
		useMultiImage = true
		useFakeTimestamp = config.Ruby.FakeTimestamp
	}

	if config.Yarn != nil {
		useMultiImage = true
		useFakeTimestamp = config.Yarn.FakeTimestamp
	}

	return
}

// MultiImagePre returns the contents of a Dockerfile, meant for first-phase runs.
func MultiImagePre(config common.Config, path string) (*bytes.Buffer, []string, error) {
	out := new(bytes.Buffer)
	files := []string{}
	var bundlerVersion string
	yarnVersion := "latest"

	addSetupSteps(out, config, path)

	if config.Ruby != nil {

		if config.Ruby.BundlerVersion == "" {
			bundlerVersion = DEFAULT_BUNDLER_VERSION
		} else {
			bundlerVersion = config.Ruby.BundlerVersion
		}

		fmt.Fprintf(out, "RUN gem install bundler -v %s\n", bundlerVersion)

		fmt.Fprint(out, "ADD Gemfile /tmp/manta-bundler/Gemfile\n")
		files = append(files, "Gemfile")

		fmt.Fprint(out, "ADD Gemfile.lock /tmp/manta-bundler/Gemfile.lock\n")
		files = append(files, "Gemfile.lock")

		for _, gemPath := range config.Ruby.VendoredGems {
			fmt.Fprintf(out, "ADD %s /tmp/manta-bundler/%s\n", gemPath, gemPath)
			files = append(files, gemPath)
		}

		fmt.Fprint(out, "CMD bundle install --gemfile=/tmp/manta-bundler/Gemfile\n")
	}

	if config.Yarn != nil {

		if config.Mount == "" {
			return nil, nil, fmt.Errorf("You must include a mount point when using Yarn.")
		}

		if config.Yarn.YarnVersion != "" {
			yarnVersion = config.Yarn.YarnVersion
		}

		fmt.Fprintf(out, "RUN npm install -g yarn@%s\n", yarnVersion)

		fmt.Fprintf(out, "ADD package.json %s/package.json\n", config.Mount)
		files = append(files, "package.json")

		if config.Yarn.IncludeBower {
			fmt.Fprintf(out, "ADD bower.json %s/bower.json\n", config.Mount)
			files = append(files, "bower.json")
		}

		fmt.Fprintf(out, "ADD yarn.lock %s/yarn.lock\n", config.Mount)
		files = append(files, "yarn.lock")

		fmt.Fprintf(out, "CMD cd %s && yarn install", config.Mount)
	}

	return out, files, nil
}

// MultiImagePost returns the contents of a Dockerfile meant for a run using one image.
func MultiImagePost(baseImage string, config common.Config, path string) *bytes.Buffer {
	out := new(bytes.Buffer)

	fmt.Fprintf(out, "FROM %s\n", baseImage)

	addProxyEnv(out)
	createBuildScript(config, path)
	addBuildSteps(out, config, path)

	return out
}

// addEnvVarsList writes a single ENV entry to a buffer given a list of foo=bar variable declarations
func addEnvVarsList(out *bytes.Buffer, envList []string) {
	if len(envList) > 0 {
		fmt.Fprintf(out, "ENV")
		for _, envVar := range envList {
			fmt.Fprintf(out, " %s", envVar)
		}
		fmt.Fprintf(out, "\n")
	}
}

// addProxyEnv inserts proxy-related environment variables into the dockerfile based on the current environment.
// if either the upper or lowercase versions of the proxy vars are set on the host, we set both inside the container.
func addProxyEnv(out *bytes.Buffer) {
	addEnvVarsList(out, getProxyEnvs())
}

// getProxyEnvs retrieves a list of stringified proxy variables
func getProxyEnvs() []string {
	res := []string{}
	vars := []string{"HTTP_PROXY", "HTTPS_PROXY", "NO_PROXY"}

	for _, proxyVar := range vars {
		value := ""
		if os.Getenv(proxyVar) != "" {
			value = os.Getenv(proxyVar)
		}
		if os.Getenv(strings.ToLower(proxyVar)) != "" {
			value = os.Getenv(strings.ToLower(proxyVar))
		}

		if value != "" {
			res = append(res, fmt.Sprintf("%q=%q", proxyVar, value))
			res = append(res, fmt.Sprintf("%q=%q", strings.ToLower(proxyVar), value))
		}
	}
	return res
}

func addSetupSteps(out *bytes.Buffer, config common.Config, path string) error {
	fmt.Fprintf(out, "FROM %s\n", config.Image)

	envList := make([]string, len(config.EnvList))
	tmpBuf := new(bytes.Buffer) // temporary buffer to be appended to `out`
	copy(envList, config.EnvList)

	if config.OS == "debian" || config.OS == "ubuntu" {
		envList = append(envList, "DEBIAN_FRONTEND=noninteractive")
	}
	envList = append(envList, getProxyEnvs()...)

	// Hack: Tarrant (2015-03-08): Disabling twitchAptMirrors until we have new ones

	// // HACK(keith): we should embed the apt mirror config into base build images instead of the Dockerfile here. hacking in precise/lucid switching until we have that.
	// // https://www.pivotaltracker.com/story/show/66910930
	// parts := strings.Split(config.Image, ":")
	// release := "precise"
	// if len(parts) == 2 {
	// 	if parts[0] == "ubuntu" {
	// 		release = parts[1]
	// 	}
	// }
	// // HACK

	// //fmt.Fprintf(buf, "RUN echo '%s' >> /etc/apt/sources.list.d/jtv.list\n", twitchAptMirrors(release))

	// .ssh/config to prevent interactive prompts
	if config.OS == "debian" || config.OS == "ubuntu" {
		fmt.Fprint(tmpBuf, "RUN apt-get update && apt-get -y install ssh netcat\n")
	} else if config.OS == "rhel" || config.OS == "centos" {
		fmt.Fprint(tmpBuf, "RUN yum -y install openssh nc\n")
	} else {
		return fmt.Errorf("Unsupported OS %v", config.OS)
	}
	fmt.Fprint(tmpBuf, "RUN mkdir -p /root/.ssh\n")
	fmt.Fprint(tmpBuf, `RUN echo "Host *\n\tUserKnownHostsFile /dev/null\n\tGlobalKnownHostsFile /dev/null\n\tStrictHostKeyChecking no\n\tBatchMode yes" > /root/.ssh/config
`)
	if common.UseProxy() {
		fmt.Fprintf(tmpBuf, `RUN echo "\nHost github.com\n\tUser git\n\tPort 22\n\tHostname github.com\n\tIdentityFile /home/jtv/.ssh/github_rsa\n\tTCPKeepAlive yes\n\tProxyCommand nc -X 4 -x proxy.internal.justin.tv:1080 %%h %%p" >> /root/.ssh/config
`)
	}
	fmt.Fprint(tmpBuf, "RUN chmod 600 /root/.ssh/*\n")

	if common.DockerInDockerMode() {
		if config.OS == "debian" || config.OS == "ubuntu" {
			fmt.Fprintf(tmpBuf, "RUN apt-get update && apt-get -y install apt-transport-https dmsetup aufs-tools net-tools iproute\n")
			fmt.Fprintf(tmpBuf, "RUN apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D\n")
			fmt.Fprintf(tmpBuf, `RUN echo "deb https://apt.dockerproject.org/repo ubuntu-%s main\n\t" > /etc/apt/sources.list.d/docker.list
	`, common.TestOSRepoName)
			fmt.Fprintf(tmpBuf, "RUN apt-get update && apt-get -y install apt-transport-https docker-engine\n")
		} else if config.OS == "rhel" || config.OS == "centos" {
			fmt.Fprintf(tmpBuf, "RUN rpm -i https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm\n")
			fmt.Fprintf(tmpBuf, "RUN yum -y install lxc\n")
			fmt.Fprintf(tmpBuf, `RUN echo [dockerrepo] >> /etc/yum.repos.d/docker.repo && \
echo name=Docker Repository >> /etc/yum.repos.d/docker.repo && \
echo baseurl=https://yum.dockerproject.org/repo/main/centos/\$releasever/ >> /etc/yum.repos.d/docker.repo && \
echo enabled=1 >> /etc/yum.repos.d/docker.repo && \
echo gpgcheck=1 >> /etc/yum.repos.d/docker.repo && \
echo gpgkey=https://yum.dockerproject.org/gpg >> /etc/yum.repos.d/docker.repo
`)
			fmt.Fprintf(tmpBuf, "RUN yum install -y docker-engine\n")
		} else {
			return fmt.Errorf("Unsupported OS %v", config.OS)
		}

	}

	if config.Dependencies != nil {
		fmt.Fprintf(tmpBuf, "RUN apt-get update && apt-get install -y wget\n")
		fmt.Fprintf(tmpBuf, "RUN wget -O- http://packages.internal.justin.tv/internal/artifactory/api/gpg/key/public | apt-key add -\n")
		fmt.Fprintf(tmpBuf, `RUN echo "deb [arch=amd64] http://packages.internal.justin.tv/internal/twitch precise main\n\t" > /etc/apt/sources.list.d/twitch.list
`)
		fmt.Fprintf(tmpBuf, "RUN apt-get update && apt-get -y install courier sudo lsof\n")
	}

	addSetupFilesSorted(tmpBuf, config.SetupFiles)

	for _, cmd := range config.Setup {
		fmt.Fprintf(tmpBuf, "RUN %s\n", cmd)
	}

	if config.Dependencies != nil {
		access_key := os.Getenv("DIRTY_DEPLOY_ARTIFACT_AWS_ACCESS_KEY")
		if access_key == "" {
			return errors.New("DIRTY_DEPLOY_ARTIFACT_AWS_ACCESS_KEY is not set")
		}
		envList = append(envList, fmt.Sprintf("AWS_ACCESS_KEY_ID=%s", access_key))
		secret_key := os.Getenv("DIRTY_DEPLOY_ARTIFACT_AWS_SECRET_KEY")
		if secret_key == "" {
			return errors.New("DIRTY_DEPLOY_ARTIFACT_AWS_SECRET_KEY is not set")
		}
		envList = append(envList, fmt.Sprintf("AWS_SECRET_ACCESS_KEY=%s", secret_key))
		// fmt.Fprintf(buf, "RUN mkdir -p /tmp/deps/releases\n")
		for _, depend := range config.Dependencies {
			owner := filepath.Dir(depend.Repo)
			name := filepath.Base(depend.Repo)
			targetPath := depend.TargetPath
			sha := depend.SHA

			fmt.Fprintf(tmpBuf, "RUN courier tar install --repo %s/%s --sha %s --dir /tmp/deps --target %s --environment build --skip-symlink\n", owner, name, sha, targetPath)
		}
	}

	// write a single ENV line with the envList and aws vars in the output buffer
	addEnvVarsList(out, envList)

	// now write what we had in the temporary buffer at the end of the output buffer
	fmt.Fprint(out, tmpBuf)

	return nil
}

func addSetupFilesSorted(out *bytes.Buffer, setupFiles map[string]string) {
	var srcs []string
	for src := range setupFiles {
		srcs = append(srcs, src)
	}
	sort.Strings(srcs)
	for _, src := range srcs {
		fmt.Fprintf(out, "ADD %s %s\n", src, setupFiles[src])
	}
}

func addBuildSteps(out *bytes.Buffer, config common.Config, path string) error {
	// set GIT-related environment variables towards the ends since they will change per-run.
	gitCommit, err := extractGitEnv()
	if err != nil {
		log.Printf("could not extract git environment: %s", err)
	}
	if gitCommit != "" {
		fmt.Fprintf(out, "ENV GIT_COMMIT %s\n", gitCommit)
	}

	if common.DockerInDockerMode() {
		fmt.Fprintf(out, "ENTRYPOINT [\"scripts/dind\"]\n")
	}

	// the working directory being a volume allows for disk writes to be as fast as the host system allows
	fmt.Fprintf(out, "VOLUME [\"%v\"]\n", config.Mount)
	fmt.Fprintf(out, "ADD . %s\n", config.Mount)
	fmt.Fprintf(out, "RUN chmod 0755 %s\n", config.Mount)

	if common.DockerInDockerMode() {
		fmt.Fprintf(out, "VOLUME [\"/var/lib/docker\"]\n")
	}
	for _, depend := range config.Dependencies {
		installPath := filepath.Join(config.Mount, depend.TargetPath)
		fmt.Fprintf(out, "RUN mkdir -p %s && mv /tmp/deps/releases/%s %s\n", filepath.Dir(installPath), depend.TargetPath, installPath)
	}

	fmt.Fprintf(out, "CMD ./%s\n", MANTA_BUILD_SCRIPT)

	return nil
}

func createBuildScript(config common.Config, path string) error {
	script := new(bytes.Buffer)

	fmt.Fprint(script, "#!/bin/bash\n")

	if common.Verbose() {
		fmt.Fprint(script, "set -e -x\n")
	} else {
		fmt.Fprint(script, "set -e\n")
	}

	fmt.Fprintln(script, "set -o pipefail")

	fmt.Fprintln(script, "[[ -f /etc/init.d/apparmor ]] && /etc/init.d/apparmor stop")

	if common.DockerInDockerMode() {
		// TODO: in "very verbose mode", show this output
		if common.Verbose() {
			fmt.Fprint(script, "docker daemon -D 2>&1 &\n")
		} else {
			fmt.Fprint(script, "docker daemon -D > /dev/null 2>&1 &\n")
		}

		fmt.Fprintf(script, "sleep 3\n")
	}
	isValidVarName := regexp.MustCompile(`^[a-zA-Z0-9_]+$`).MatchString
	for _, envVar := range config.BuildEnvList {
		envVar := strings.TrimSpace(envVar)
		kv := strings.Split(envVar, "=")
		if !isValidVarName(kv[0]) {
			return fmt.Errorf("Invalid shell variable name: %s", kv[0])
		}
		fmt.Fprintf(script, "export %s\n", envVar)
	}
	if config.Dependencies != nil {
		fmt.Fprintf(script, "mv /tmp/deps/releases/* .\n")
	}

	if !config.NetworkedBuild {
		fmt.Fprintf(script, "ip route add blackhole 0.0.0.0/8\n")
	}

	for _, command := range config.Build {
		fmt.Fprintf(script, "%s\n", command)
	}

	if err := ioutil.WriteFile(filepath.Join(path, MANTA_BUILD_SCRIPT), script.Bytes(), 0755&os.ModePerm); err != nil {
		return fmt.Errorf("could not write manta.sh: %s", err)
	}

	return nil
}

func twitchAptMirrors(release string) string {
	mirror := "http://mirrors.internal.justin.tv/ubuntu"
	targets := []string{"", "-updates", "-backports", "-security"}

	out := new(bytes.Buffer)
	for _, target := range targets {
		fmt.Fprintf(out, "deb %s %s%s main restricted universe multiverse\\n", mirror, release, target)
		fmt.Fprintf(out, "deb-src %s %s%s main restricted universe multiverse\\n", mirror, release, target)
	}
	return out.String()
}

func extractGitEnv() (string, error) {
	var commitID string
	out := new(bytes.Buffer)
	cmd := exec.Command("git", "rev-parse", "HEAD")
	cmd.Stdout = out
	err := cmd.Run()
	if err != nil {
		return "", err
	}
	commitID = strings.TrimSpace(out.String())

	out = new(bytes.Buffer)
	cmd = exec.Command("git", "status", "--porcelain")
	cmd.Stdout = out
	err = cmd.Run()
	if err != nil {
		return "", err
	}

	if out.String() != "" {
		commitID = commitID + "-dirty"
	}

	return commitID, nil
}
