package docker

import (
	"archive/tar"
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"path"
	"path/filepath"
	"regexp"
	"strings"
	"syscall"

	"code.justin.tv/release/manta-style/pkg/common"
	"github.com/docker/docker/pkg/term"
	dockerclient "github.com/fsouza/go-dockerclient"
)

var client *dockerclient.Client

func init() {
	if os.Getenv("SSH_AUTH_SOCK") == "" {
		log.Print("WARN: SSH_AUTH_SOCK is not set! Host will be unable to forward ssh-agent credentials to container. Enable ssh-agent forwarding in your VM if you need authenticated SSH to private git repositories.")
	}

	var err error
	if os.Getenv("DOCKER_HOST") != "" &&
		os.Getenv("DOCKER_TLS_VERIFY") != "" &&
		os.Getenv("DOCKER_CERT_PATH") != "" {
		client, err = dockerclient.NewClientFromEnv()
	} else {
		client, err = dockerclient.NewClient(getEndpoint())
	}
	if err != nil {
		log.Fatalf("error creating docker client: %s", err)
	}
}

func getEndpoint() string {
	endpoint := "unix:///var/run/docker.sock"

	if os.Getenv("MANTA_HOST") != "" {
		endpoint = os.Getenv("MANTA_HOST")
	} else {
		// XXX unsure if this is actually a good idea?
		if os.Getenv("DOCKER_HOST") != "" {
			dockerHost := os.Getenv("DOCKER_HOST")
			if strings.HasPrefix(dockerHost, "tcp://") {
				endpoint = "http://" + strings.Split(dockerHost, "tcp://")[1]
			} else {
				endpoint = dockerHost
			}
		}
	}

	return endpoint
}

// CreateImage builds a new docker image, given the path to a tar file containing a Dockerfile
func CreateImage(tarPath string) (string, error) {
	file, err := os.Open(tarPath)
	if err != nil {
		return "", fmt.Errorf("error opening tar of working directory: %s", err)
	}
	defer file.Close()

	buildOpts := dockerclient.BuildImageOptions{
		InputStream:    file,
		RmTmpContainer: true,
	}

	out := new(bytes.Buffer)
	if common.Verbose() {
		filteredOut := common.NewFilteredWriter(os.Stdout)
		buildOpts.OutputStream = io.MultiWriter(filteredOut, out)
		// buildOpts.OutputStream = io.MultiWriter(os.Stdout, out)
	} else {
		buildOpts.OutputStream = out
	}

	if common.Verbose() {
		fi, err := file.Stat()
		if err != nil {
			return "", err
		}
		log.Printf("streaming files to docker daemon. (%d bytes)", fi.Size())
	}

	err = client.BuildImage(buildOpts)
	if err != nil {
		if !common.Verbose() {
			// if the build fails in non-verbose mode, the user still wants to see the error
			fmt.Fprintf(os.Stdout, out.String())
		}
		return "", err
	}

	lines := strings.Split(strings.TrimSpace(out.String()), "\n")
	lastLine := lines[len(lines)-1:][0]

	re := regexp.MustCompile("Successfully built ([0-9a-z]{12})")
	matches := re.FindSubmatch([]byte(lastLine))
	if len(matches) == 0 {
		return "", errors.New(fmt.Sprintf("could not extract docker image id from: %s", lastLine))
	}

	return string(matches[1]), nil
}

func ImageExists(nameOrID string) (bool, error) {
	_, err := client.InspectImage(nameOrID)
	if err != nil {
		if err == dockerclient.ErrNoSuchImage {
			return false, nil
		} else {
			return false, err
		}
	}

	return true, nil
}

// StartImageAsDaemon starts a container in the background, and returns the new containerID
func StartImageAsDaemon(imageID string, workingDirectory string, networkIsolated bool) (string, error) {
	hostConfig := &dockerclient.HostConfig{}

	containerConfig := &dockerclient.Config{
		Image:        imageID,
		WorkingDir:   workingDirectory,
		AttachStdout: true,
	}

	if !networkIsolated {
		hostConfig.CapAdd = append(hostConfig.CapAdd, "NET_ADMIN")
	}

	applySSHConfig(hostConfig, containerConfig)
	if common.PrivilegedMode() {
		hostConfig.Privileged = true
	}

	options := dockerclient.CreateContainerOptions{Config: containerConfig, HostConfig: hostConfig}
	container, err := client.CreateContainer(options)
	if err != nil {
		return "", fmt.Errorf("error creating container: %s", err)
	}

	err = client.StartContainer(container.ID, hostConfig)
	if err != nil {
		return "", fmt.Errorf("error starting container: %s", err)
	}

	if common.Verbose() {
		log.Printf("Created container %s", container.ID)
	}

	return container.ID, nil
}

func applySSHConfig(hostConfig *dockerclient.HostConfig, containerConfig *dockerclient.Config) {
	sshAuthSock := os.Getenv("SSH_AUTH_SOCK")
	if sshAuthSock != "" {
		sshAuthSockDir := filepath.Dir(sshAuthSock)
		containerConfig.Env = append(containerConfig.Env, fmt.Sprintf("SSH_AUTH_SOCK=%s", sshAuthSock))
		volumes := make(map[string]struct{})
		volumes[sshAuthSockDir] = struct{}{}
		hostConfig.Binds = []string{fmt.Sprintf("%s:%s", sshAuthSockDir, sshAuthSockDir)}
		containerConfig.Volumes = volumes
	}
}

// WatchStdoutUntilDone prints the container logs to Stdout until it has finished processing
func WatchStdoutUntilDone(containerID string) (int, error) {
	err := client.AttachToContainer(dockerclient.AttachToContainerOptions{
		Container:    containerID,
		Stream:       true,
		Stdout:       true,
		Stderr:       true,
		Logs:         true,
		OutputStream: os.Stdout,
		ErrorStream:  os.Stderr,
		RawTerminal:  false,
	})
	if err != nil {
		return 1, err
	}

	exitCode, err := client.WaitContainer(containerID)
	if err != nil {
		return 1, err
	}

	return exitCode, nil
}

// ExtractArtifact copies the configured source file or directory from the container
// and writes it to the host destination directory
func ExtractArtifact(containerID string, source string, destination string) error {
	if common.Verbose() {
		log.Printf("Extracting artifact from '%s' in container to '%s' on host", source, destination)
	}

	err := os.MkdirAll(path.Dir(destination+"/"), 0755&os.ModePerm)
	if err != nil {
		return fmt.Errorf("error creating destination folder %s: %s", path.Dir(destination), err)
	}

	buf := new(bytes.Buffer)
	err = client.DownloadFromContainer(containerID, dockerclient.DownloadFromContainerOptions{
		OutputStream: buf,
		Path:         source,
	})
	if err != nil {
		return fmt.Errorf("error downloading artifact from container: %s", err)
	}

	numFilesCopied := 0

	tr := tar.NewReader(buf)
	prefix := path.Base(source)
	for {
		hdr, err := tr.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return fmt.Errorf("error reading extracted tar: %s", err)
		}

		replaced := strings.Replace(hdr.Name, prefix, "", 1)
		if replaced == "" { // source points to a file, not a directory, on the container
			replaced = hdr.Name
		}
		target := filepath.Join(destination, replaced)
		// TODO: implement multiple verbose levels
		// if common.Verbose() {
		// 	log.Printf("Extracting file: %s", target)
		// }

		if hdr.Typeflag == tar.TypeSymlink {
			err := os.Symlink(hdr.Linkname, target)
			if err != nil {
				return fmt.Errorf("error extracting symlink: %s", err)
			}
		} else if hdr.Typeflag == tar.TypeDir {
			err := os.MkdirAll(target, hdr.FileInfo().Mode())
			if err != nil {
				return fmt.Errorf("error extracting folder: %s", err)
			}
		} else {
			file, err := os.Create(target)
			if err != nil {
				return fmt.Errorf("error creating file: %s. (destination folders need to end in slash)", err)
			}
			io.Copy(file, tr)
			err = file.Chmod(hdr.FileInfo().Mode())
			if err != nil {
				return fmt.Errorf("error setting file mode: %s", err)
			}
			file.Close()
		}
		numFilesCopied += 1
	}

	if numFilesCopied == 0 {
		return fmt.Errorf("no files copied from container!")
	}

	return nil
}

// DeleteContainer deletes a docker container by ID
func DeleteContainer(containerID string) error {
	if common.Verbose() {
		log.Printf("Cleaning up container %s", containerID)
	}

	err := client.RemoveContainer(dockerclient.RemoveContainerOptions{
		ID:            containerID,
		RemoveVolumes: true,
	})
	if err != nil {
		log.Printf("error deleting docker container after build: %s", err)
		return err
	}

	return nil
}

// DeleteImage deletes a docker image by ID
func DeleteImage(imageID string) error {
	if common.Verbose() {
		log.Printf("Cleaning up image %s", imageID)
	}

	// TODO: support remote api 1.10 to pass force parameter
	err := client.RemoveImage(imageID)
	if err != nil {
		log.Printf("error deleting docker image: %s", err)
		return err
	}

	return nil
}

// CommitContainer creates an image from a container
func CommitContainer(containerID string, repo string, tag string) (*dockerclient.Image, error) {
	opts := dockerclient.CommitContainerOptions{
		Container: containerID,
		Run: &dockerclient.Config{
			Cmd: []string{"/bin/bash"},
		},
	}

	if repo != "" {
		opts.Repository = repo
	}

	if tag != "" {
		opts.Tag = tag
	}

	return client.CommitContainer(opts)
}

// AttachTerminalToImage starts an image and runs it in interactive mode
func AttachTerminalToImage(imageID string, workingDirectory string) error {
	hostConfig := &dockerclient.HostConfig{}

	containerConfig := &dockerclient.Config{
		Image:        imageID,
		WorkingDir:   workingDirectory,
		AttachStdout: true,
		Tty:          true,
		OpenStdin:    true,
		AttachStdin:  true,
		Cmd:          []string{"/bin/bash"},
	}

	applySSHConfig(hostConfig, containerConfig)

	if common.PrivilegedMode() {
		hostConfig.Privileged = true
	}

	options := dockerclient.CreateContainerOptions{Config: containerConfig, HostConfig: hostConfig}
	container, err := client.CreateContainer(options)
	if err != nil {
		return fmt.Errorf("error creating container: %s", err)
	}
	if common.Verbose() {
		log.Printf("created interactive container: %s", container.ID)
	}
	defer func() {
		client.StopContainer(container.ID, 1)
		DeleteContainer(container.ID)
	}()

	err = client.StartContainer(container.ID, hostConfig)
	if err != nil {
		return fmt.Errorf("error starting container: %s", err)
	}

	monitorTtySize(container.ID)

	oldState, err := term.SetRawTerminal(os.Stdin.Fd())
	if err != nil {
		return fmt.Errorf("error setting raw terminal: %s", err)
	}
	defer term.RestoreTerminal(os.Stdin.Fd(), oldState)

	attachConfig := dockerclient.AttachToContainerOptions{
		Container:    container.ID,
		Stdin:        true,
		Stdout:       true,
		Stderr:       true,
		InputStream:  os.Stdin,
		OutputStream: os.Stdout,
		ErrorStream:  os.Stderr,
		Stream:       true,
		RawTerminal:  true,
	}
	err = client.AttachToContainer(attachConfig)
	if err != nil {
		return fmt.Errorf("error attaching to container: %s", err)
	}

	return nil
}

func monitorTtySize(containerID string) error {
	resizeTty(containerID)

	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGWINCH)
	go func() {
		for _ = range sigchan {
			resizeTty(containerID)
		}
	}()
	return nil
}

func resizeTty(containerID string) error {
	ws, err := term.GetWinsize(os.Stdin.Fd())
	if err != nil {
		log.Printf("error getting terminal size: %s", err)
		return err
	}
	client.ResizeContainerTTY(containerID, int(ws.Height), int(ws.Width))

	return nil
}

func StopContainer(containerID string, timeout uint) error {
	return client.StopContainer(containerID, timeout)
}

func UpdateImage(image string) error {
	repository, tag := dockerclient.ParseRepositoryTag(image)
	if tag == "" {
		tag = "latest"
		log.Printf("Using default tag: %s", tag)
	}
	pullOpts := dockerclient.PullImageOptions{
		Repository: repository,
		Tag:        tag,
	}

	if common.Verbose() {
		log.Printf("Updating image %v", image)
		pullOpts.OutputStream = os.Stdout
	}

	return client.PullImage(
		pullOpts,
		dockerclient.AuthConfiguration{},
	)
}
