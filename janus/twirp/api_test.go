package twirp_test

import (
	"testing"

	"code.justin.tv/commerce/janus/dynamo"
	"code.justin.tv/commerce/janus/mocks"
	"code.justin.tv/commerce/janus/twirp"
	twirpapi "code.justin.tv/commerce/janus/twirp/generated/dev_portal_auth/twirp"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func Test_all(t *testing.T) {
	Convey("Permissions Service", t, func() {
		mockDAO := new(mocks.IPermissionDAO)
		s := twirp.NewPermissionsService(mockDAO)
		So(s, ShouldNotBeNil)

		Convey("With no permissions", func() {
			mockDAO.On(
				"GetPermission",
				mock.Anything, // ctx
				"uid",
				"pid",
			).Return(nil, nil)
			Convey("GetPermission", func() {
				r, err := s.HasPermission(nil, &twirpapi.HasPermissionRequest{
					UserId:     "uid",
					ProductId:  "pid",
					Permission: "permission",
				})
				So(err, ShouldBeNil)
				So(r, ShouldNotBeNil)
				So(r.Result, ShouldBeFalse)
			})
		})

		Convey("With Permission", func() {
			permission := dynamo.Permission{
				TwitchUserID: "uid",
				GameID:       "pid",
				Permissions: []string{
					"permission",
				},
			}
			mockDAO.On(
				"GetPermission",
				mock.Anything, // ctx
				"uid",
				"pid",
			).Return(&permission, nil)

			Convey("GetPermission", func() {
				r, err := s.HasPermission(nil, &twirpapi.HasPermissionRequest{
					UserId:     "uid",
					ProductId:  "pid",
					Permission: "permission",
				})
				So(err, ShouldBeNil)
				So(r, ShouldNotBeNil)
				So(r.Result, ShouldBeTrue)
			})
		})

		Convey("On DB Error", func() {
			mockDAO.On(
				"GetPermission",
				mock.Anything, // ctx
				"uid",
				"pid",
			).Return(nil, errors.New("Error From Dynamo"))

			Convey("GetPermission", func() {
				r, err := s.HasPermission(nil, &twirpapi.HasPermissionRequest{
					UserId:     "uid",
					ProductId:  "pid",
					Permission: "permission",
				})
				So(err, ShouldNotBeNil)
				So(r, ShouldBeNil)
			})
		})

		Convey("With No Products", func() {
			mockDAO.On(
				"GetAllPermissionsForUser",
				nil,
				"uid").Return([]*dynamo.Permission{}, nil)

			r, err := s.Products(nil, &twirpapi.ProductsRequest{
				UserId: "uid",
			})
			So(err, ShouldBeNil)
			So(r, ShouldNotBeNil)
			So(r.Products, ShouldBeEmpty)
		})

		Convey("With Products", func() {
			mockDAO.On(
				"GetAllPermissionsForUser",
				nil,
				"uid").Return([]*dynamo.Permission{
				&dynamo.Permission{
					TwitchUserID: "1",
				},
			}, nil)

			r, err := s.Products(nil, &twirpapi.ProductsRequest{
				UserId: "uid",
			})
			So(err, ShouldBeNil)
			So(r, ShouldNotBeNil)
			So(r.Products, ShouldNotBeEmpty)
		})

		Convey("With Product Error", func() {
			mockDAO.On(
				"GetAllPermissionsForUser",
				nil,
				"uid").Return(nil, errors.New("error"))
			r, err := s.Products(nil, &twirpapi.ProductsRequest{
				UserId: "uid",
			})
			So(err, ShouldNotBeNil)
			So(r, ShouldBeNil)
		})
	})
}
