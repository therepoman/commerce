package twirp

import (
	"fmt"

	"code.justin.tv/commerce/janus/dynamo"
	"code.justin.tv/commerce/janus/twirp/generated/dev_portal_auth/twirp"
	twirp_core "code.justin.tv/common/twirp"
	"goji.io"
	"golang.org/x/net/context"
)

// PermissionsService object for hanging apis off of
type PermissionsService struct {
	dao dynamo.IPermissionDAO
}

// NewPermissionsService ctor
func NewPermissionsService(dao dynamo.IPermissionDAO) *PermissionsService {
	return &PermissionsService{
		dao: dao,
	}
}

const (
	admin = "admin"
)

// Server hanger
type Server struct {
	*goji.Mux
	service *PermissionsService
}

// HasPermission performs HasPermission query and returns response using twirp
func (service *PermissionsService) HasPermission(ctx context.Context, request *twirp.HasPermissionRequest) (*twirp.HasPermissionResponse, error) {
	permission, err := service.dao.GetPermission(ctx, request.UserId, request.ProductId)

	if err != nil {
		return nil, twirp_core.NewError(twirp_core.Internal, "Database Error")
	}

	hasPermission := false

	if permission == nil {
		return &twirp.HasPermissionResponse{
			Result: false,
		}, nil
	}

	for _, v := range permission.Permissions {
		if v == admin {
			hasPermission = true
			break
		}
		if v == request.Permission {
			hasPermission = true
			break
		}
	}

	return &twirp.HasPermissionResponse{
		Result: hasPermission,
	}, nil
}

func (service *PermissionsService) AddPermissions(ctx context.Context, request *twirp.AddPermissionRequest) (*twirp.AddPermissionResponse, error) {
	err := service.dao.AddPermission(ctx, request.UserId, request.ProductId, request.Permissions)
	return &twirp.AddPermissionResponse{}, err
}

// Products performs Products query and returns response using twirp
func (service *PermissionsService) Products(ctx context.Context, request *twirp.ProductsRequest) (*twirp.ProductsResponse, error) {
	permissions, err := service.dao.GetAllPermissionsForUser(ctx, request.UserId)

	if err != nil {
		return nil, twirp_core.NewError(twirp_core.Internal, "Database Error")
	}

	outProducts := make([]*twirp.Product, 0, len(permissions))
	for _, v := range permissions {
		outProducts = append(outProducts, &twirp.Product{
			Id:   v.GameID,
			Name: fmt.Sprintf("Unknown game for: %s", v.GameID),
		})
	}

	return &twirp.ProductsResponse{
		Products: outProducts,
	}, nil
}
