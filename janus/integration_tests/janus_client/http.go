package janus_client

import (
	"fmt"
	"net/http"

	"encoding/json"
	"io/ioutil"

	"code.justin.tv/commerce/janus/client"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/amendment_provider"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/game"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/identity_provider"
)

const (
	HTTP_BODY_200 = "200 OK"
	HTTP_BODY_403 = "403 Forbidden"
	HTTP_BODY_404 = "404 Not Found"
)

type HttpJanusIntegrationTestClient struct {
	targetJanusEndpoint string
}

func NewHttpJanusIntegClient(targetJanusEndpoint string) JanusIntegrationTestClient {
	return &HttpJanusIntegrationTestClient{
		targetJanusEndpoint: targetJanusEndpoint,
	}
}

func (http *HttpJanusIntegrationTestClient) GetActiveMarketplaces(identity Identity, targetTuid string) (*janus.GetActiveMarketplacesResponse, JanusError) {
	resp, err := HttpGet(identity, fmt.Sprintf("%s/inventory/%s/active_marketplaces", http.targetJanusEndpoint, targetTuid), new(janus.GetActiveMarketplacesResponse))
	return resp.(*janus.GetActiveMarketplacesResponse), err
}

func (http *HttpJanusIntegrationTestClient) GetAmendment(identity Identity, targetTuid string, amendment Amendment) (*janus.GetAmendmentAcceptanceResponse, JanusError) {
	resp, err := HttpPut(identity, fmt.Sprintf("%s/broadcaster/%s/amendment/%s", http.targetJanusEndpoint, targetTuid, amendment.Name), new(janus.GetAmendmentAcceptanceResponse))
	return resp.(*janus.GetAmendmentAcceptanceResponse), err
}

func (http *HttpJanusIntegrationTestClient) GetBadges(identity Identity, targetTuid string) (*janus.GetBadgesResponse, JanusError) {
	resp, err := HttpGet(identity, fmt.Sprintf("%s/inventory/%s/badges", http.targetJanusEndpoint, targetTuid), new(janus.GetBadgesResponse))
	return resp.(*janus.GetBadgesResponse), err
}

func (http *HttpJanusIntegrationTestClient) GetBroadcasterPayouts(identity Identity, targetTuid string) (*janus.GetBroadcasterPayoutDetailsResponse, JanusError) {
	resp, err := HttpGet(identity, fmt.Sprintf("%s/broadcaster/%s/payout", http.targetJanusEndpoint, targetTuid), new(janus.GetBroadcasterPayoutDetailsResponse))
	return resp.(*janus.GetBroadcasterPayoutDetailsResponse), err
}

func (http *HttpJanusIntegrationTestClient) GetChatNotifications(identity Identity, targetTuid string, targetChannel string) (*janus.GetChatNotificationsResponse, JanusError) {
	resp, err := HttpGet(identity, fmt.Sprintf("%s/users/%s/notifications?channelID=%s", http.targetJanusEndpoint, targetTuid, targetChannel), new(janus.GetChatNotificationsResponse))
	return resp.(*janus.GetChatNotificationsResponse), err
}

func (http *HttpJanusIntegrationTestClient) GetCrates(identity Identity, targetTuid string) (*janus.GetCratesResponse, JanusError) {
	resp, err := HttpGet(identity, fmt.Sprintf("%s/inventory/%s/crates", http.targetJanusEndpoint, targetTuid), new(janus.GetCratesResponse))
	return resp.(*janus.GetCratesResponse), err
}

func (http *HttpJanusIntegrationTestClient) GetEmoticons(identity Identity, targetTuid string) (*janus.GetEmoticonsResponse, JanusError) {
	resp, err := HttpGet(identity, fmt.Sprintf("%s/inventory/%s/emoticons", http.targetJanusEndpoint, targetTuid), new(janus.GetEmoticonsResponse))
	return resp.(*janus.GetEmoticonsResponse), err
}

func (http *HttpJanusIntegrationTestClient) GetGameDetails(identity Identity, game Game) (*janus.GetGameDetailsResponse, JanusError) {
	var tuid string
	if identity.Tuid == nil {
		tuid = ""
	} else {
		tuid = *identity.Tuid
	}

	resp, err := HttpGet(identity, fmt.Sprintf("%s/games/%s/details?userid=%s", http.targetJanusEndpoint, *game.Title, tuid), new(janus.GetGameDetailsResponse))

	return resp.(*janus.GetGameDetailsResponse), err
}

func (http *HttpJanusIntegrationTestClient) GetInGameContentDetails(identity Identity, game Game) (*janus.GetInGameContentDetailsResponse, JanusError) {
	var tuid string
	if identity.Tuid == nil {
		tuid = ""
	} else {
		tuid = *identity.Tuid
	}

	resp, err := HttpGet(identity, fmt.Sprintf("%s/games/%s/ingame?userid=%s", http.targetJanusEndpoint, *game.Title, tuid), new(janus.GetInGameContentDetailsResponse))

	return resp.(*janus.GetInGameContentDetailsResponse), err
}

func (http *HttpJanusIntegrationTestClient) GetMerchandise(identity Identity, locale string) (*janus.GetMerchandiseResponse, JanusError) {
	resp, err := HttpGet(identity, fmt.Sprintf("%s/merchandise?locale=%s", http.targetJanusEndpoint, locale), new(janus.GetMerchandiseResponse))
	return resp.(*janus.GetMerchandiseResponse), err
}

func (http *HttpJanusIntegrationTestClient) SetAmendment(identity Identity, targetTuid string, amendment Amendment) (*janus.SetAmendmentAcceptanceResponse, JanusError) {
	resp, err := HttpPut(identity, fmt.Sprintf("%s/broadcaster/%s/amendment/%s", http.targetJanusEndpoint, targetTuid, amendment.Name), new(janus.SetAmendmentAcceptanceResponse))
	return resp.(*janus.SetAmendmentAcceptanceResponse), err
}

// HTTP Actions

func HttpGet(identity Identity, url string, targetStruct interface{}) (interface{}, JanusError) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		panic(fmt.Sprintf("Failure creating request: %s", err))
	}
	if identity.TwitchAuthorization != nil {
		req.Header.Add("Twitch-Authorization", *identity.TwitchAuthorization)
	}
	return requestAndParse(req, targetStruct)
}

func HttpPut(identity Identity, url string, targetStruct interface{}) (interface{}, JanusError) {
	req, err := http.NewRequest("PUT", url, nil)
	if err != nil {
		panic(fmt.Sprintf("Failure creating request: %s", err))
	}
	if identity.TwitchAuthorization != nil {
		req.Header.Add("Twitch-Authorization", *identity.TwitchAuthorization)
	}
	return requestAndParse(req, targetStruct)
}

func requestAndParse(req *http.Request, targetStruct interface{}) (interface{}, JanusError) {
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		panic(fmt.Sprintf("Failure performing HTTP call: %s", err))
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(fmt.Sprintf("Failure reading body: %s", body))
	}

	janusError := translateHttpStatusToJanusError(resp.Status, string(body))

	if janusError == nil {
		err = json.Unmarshal(body, targetStruct)
		if err != nil {
			panic(fmt.Sprintf("Reponse unparsable: %s", body))
		}
	}

	return targetStruct, janusError
}

func translateHttpStatusToJanusError(httpStatus string, httpBody string) JanusError {
	switch httpStatus {
	case HTTP_BODY_200:
		return nil
	case HTTP_BODY_404:
		return NewJanusError(JANUS_NOT_FOUND, httpBody)
	case HTTP_BODY_403:
		return NewJanusError(JANUS_FORBIDDEN, httpBody)
	default:
		return NewJanusError(JANUS_UNKNOWN, httpBody)
	}
}
