package janus_client

import (
	"code.justin.tv/commerce/janus/client"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/amendment_provider"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/game"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/identity_provider"
)

const (
	JANUS_FORBIDDEN = iota
	JANUS_NOT_FOUND
	JANUS_UNKNOWN
)

type JanusIntegrationTestClient interface {
	GetActiveMarketplaces(identity Identity, targetTuid string) (*janus.GetActiveMarketplacesResponse, JanusError)
	GetAmendment(identity Identity, targetTuid string, amendment Amendment) (*janus.GetAmendmentAcceptanceResponse, JanusError)
	GetBadges(identity Identity, targetTuid string) (*janus.GetBadgesResponse, JanusError)
	GetBroadcasterPayouts(identity Identity, targetTuid string) (*janus.GetBroadcasterPayoutDetailsResponse, JanusError)
	GetChatNotifications(identity Identity, targetTuid string, targetChannel string) (*janus.GetChatNotificationsResponse, JanusError)
	GetCrates(identity Identity, targetTuid string) (*janus.GetCratesResponse, JanusError)
	GetEmoticons(identity Identity, targetTuid string) (*janus.GetEmoticonsResponse, JanusError)
	GetGameDetails(identity Identity, game Game) (*janus.GetGameDetailsResponse, JanusError)
	GetInGameContentDetails(identity Identity, game Game) (*janus.GetInGameContentDetailsResponse, JanusError)
	GetMerchandise(identity Identity, locale string) (*janus.GetMerchandiseResponse, JanusError)
	SetAmendment(identity Identity, targetTuid string, amendment Amendment) (*janus.SetAmendmentAcceptanceResponse, JanusError)
}

type JanusError interface {
	Status() int
}

type janusErrorImpl struct {
	status  int
	message string
}

func (je *janusErrorImpl) Status() int {
	return je.status
}

func NewJanusError(status int, message string) JanusError {
	return &janusErrorImpl{status: status, message: message}
}
