package integration_tests

import (
	"code.justin.tv/commerce/janus/client"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/identity_provider"
	. "code.justin.tv/commerce/janus/integration_tests/janus_client"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Test GetCrates", func() {
	var (
		resp *janus.GetCratesResponse
		err  JanusError
	)

	Describe("Getting crates for a user", func() {
		Context("as an unauthenticated user", func() {
			BeforeEach(func() {
				anonymousUser := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_UNAUTHENTICATED))
				actualUser := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN))
				resp, err = janusClient.GetCrates(anonymousUser, *actualUser.Tuid)
			})

			It("fails to authenticate", func() {
				Expect(err).To(Not(BeNil()))
				Expect(err.Status()).To(BeEquivalentTo(JANUS_FORBIDDEN))
			})
		})

		Context("as an authenticated user for their own crates", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN))
				resp, err = janusClient.GetCrates(identity, *identity.Tuid)
			})

			It("succeeds", func() {
				Expect(err).To(BeNil())
			})

			It("returns contents", func() {
				smokeTestGetCratesContents(resp)
			})
		})

		Context("as an authenticated user for someone else's crates", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN).IsWhitelisted(true))
				anotherUser := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN).IsWhitelisted(false))
				resp, err = janusClient.GetCrates(identity, *anotherUser.Tuid)
			})

			It("fails to authenticate", func() {
				Expect(err).To(Not(BeNil()))
				Expect(err.Status()).To(BeEquivalentTo(JANUS_FORBIDDEN))
			})
		})
	})
})

// smokeTestGetCratesContents checks fields all responses are commonly expected to adhere to
func smokeTestGetCratesContents(response *janus.GetCratesResponse) {
	for _, crate := range response.Crates {
		Expect(crate.CrateID).To(Not(BeNil()))
		Expect(crate.OpenCrateURL).To(Not(BeEmpty()))
	}
}
