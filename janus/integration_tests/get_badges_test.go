package integration_tests

import (
	"code.justin.tv/commerce/janus/client"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/identity_provider"
	. "code.justin.tv/commerce/janus/integration_tests/janus_client"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Test GetBadges", func() {
	var (
		resp *janus.GetBadgesResponse
		err  JanusError
	)

	Describe("Getting badges for a user", func() {
		Context("as an unauthenticated user", func() {
			BeforeEach(func() {
				anonymousUser := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_UNAUTHENTICATED))
				actualUser := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN))
				resp, err = janusClient.GetBadges(anonymousUser, *actualUser.Tuid)
			})

			It("fails to authenticate", func() {
				Expect(err).To(Not(BeNil()))
				Expect(err.Status()).To(BeEquivalentTo(JANUS_FORBIDDEN))
			})
		})

		Context("as an authenticated user for their own badges", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN))
				resp, err = janusClient.GetBadges(identity, *identity.Tuid)
			})

			It("succeeds", func() {
				Expect(err).To(BeNil())
			})

			It("returns contents", func() {
				smokeTestGetBadgesContents(resp)
			})
		})

		Context("as an authenticated user for someone else's badges", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN).IsWhitelisted(true))
				anotherUser := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN).IsWhitelisted(false))
				resp, err = janusClient.GetBadges(identity, *anotherUser.Tuid)
			})

			It("fails to authenticate", func() {
				Expect(err).To(Not(BeNil()))
				Expect(err.Status()).To(BeEquivalentTo(JANUS_FORBIDDEN))
			})
		})
	})
})

// smokeTestGetBadgesContents checks fields all responses are commonly expected to adhere to
func smokeTestGetBadgesContents(response *janus.GetBadgesResponse) {
	for _, badges := range response.BadgeSets {
		Expect(badges.Versions).To(Not(BeNil()))
		for _, version := range badges.Versions {
			Expect(version.ID).To(Not(BeNil()))
		}
	}
}
