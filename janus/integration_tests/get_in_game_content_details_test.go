package integration_tests

import (
	"code.justin.tv/commerce/janus/client"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/game"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/identity_provider"
	. "code.justin.tv/commerce/janus/integration_tests/janus_client"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

// Test GetInGameContentDetails
var _ = Describe("Test GetInGameContentDetails", func() {
	var (
		game Game
		resp *janus.GetInGameContentDetailsResponse
		err  JanusError
	)

	Describe("Getting details for a released game", func() {
		BeforeEach(func() {
			game = gameProvider.GetGame(true)
		})

		Context("as an unauthenticated user", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_UNAUTHENTICATED))
				resp, err = janusClient.GetInGameContentDetails(identity, game)
			})

			It("succeeds", func() {
				Expect(err).To(BeNil())
			})

			It("returns contents", func() {
				smokeTestInGameContentDetailsResponse(resp)
			})
		})

		Context("as an authenticated user", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN))
				resp, err = janusClient.GetInGameContentDetails(identity, game)
			})

			It("succeeds", func() {
				Expect(err).To(BeNil())
			})

			It("returns contents", func() {
				smokeTestInGameContentDetailsResponse(resp)
			})
		})

		Context("as an authenticated whitelisted user", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IsWhitelisted(true))
				resp, err = janusClient.GetInGameContentDetails(identity, game)
			})

			It("succeeds", func() {
				Expect(err).To(BeNil())
			})

			It("returns contents", func() {
				smokeTestInGameContentDetailsResponse(resp)
			})
		})
	})

	Describe("Getting details for an unreleased, future game", func() {
		BeforeEach(func() {
			game = gameProvider.GetGame(false)
		})

		Describe("as an unauthenticated user", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_UNAUTHENTICATED))
				resp, err = janusClient.GetInGameContentDetails(identity, game)
			})

			It("does not return results", func() {
				Expect(err).To(Not(BeNil()))
				Expect(err.Status()).To(BeEquivalentTo(JANUS_NOT_FOUND))
			})
		})

		Describe("as an authenticated user", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN))
				resp, err = janusClient.GetInGameContentDetails(identity, game)
			})

			It("does not return results", func() {
				Expect(err).To(Not(BeNil()))
				Expect(err.Status()).To(BeEquivalentTo(JANUS_NOT_FOUND))
			})
		})

		Describe("as an authenticated whitelisted user", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IsWhitelisted(true))
				resp, err = janusClient.GetInGameContentDetails(identity, game)
			})

			It("succeeds", func() {
				Expect(err).To(BeNil())
			})

			It("returns contents", func() {
				smokeTestInGameContentDetailsResponse(resp)
			})
		})
	})
})

// smokeTestGetGameDetailsResponse checks fields responses are commonly expected to adhere to
func smokeTestInGameContentDetailsResponse(response *janus.GetInGameContentDetailsResponse) {
	// TODO: Add checking for size when we actually have IGC in dev/staging
	for _, inGameContent := range response.InGameContent {
		Expect(inGameContent.ActionDetails.DestinationURL).To(ContainSubstring(inGameContent.ContentDetails.ASIN))
	}
}
