package integration_tests

import (
	"code.justin.tv/commerce/janus/client"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/identity_provider"
	. "code.justin.tv/commerce/janus/integration_tests/janus_client"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Test GetEmoticons", func() {
	var (
		resp *janus.GetEmoticonsResponse
		err  JanusError
	)

	Describe("Getting emoticons for a user", func() {
		Context("as an unauthenticated user", func() {
			BeforeEach(func() {
				anonymousUser := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_UNAUTHENTICATED))
				actualUser := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN))
				resp, err = janusClient.GetEmoticons(anonymousUser, *actualUser.Tuid)
			})

			It("fails to authenticate", func() {
				Expect(err).To(Not(BeNil()))
				Expect(err.Status()).To(BeEquivalentTo(JANUS_FORBIDDEN))
			})
		})

		Context("as an authenticated user for their own emoticons", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN))
				resp, err = janusClient.GetEmoticons(identity, *identity.Tuid)
			})

			It("succeeds", func() {
				Expect(err).To(BeNil())
			})

			It("returns contents", func() {
				smokeTestGetEmoticonsContents(resp)
			})
		})

		Context("as an authenticated user for someone else's emoticons", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN).IsWhitelisted(true))
				anotherUser := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN).IsWhitelisted(false))
				resp, err = janusClient.GetEmoticons(identity, *anotherUser.Tuid)
			})

			It("fails to authenticate", func() {
				Expect(err).To(Not(BeNil()))
				Expect(err.Status()).To(BeEquivalentTo(JANUS_FORBIDDEN))
			})
		})
	})
})

// smokeTestGetEmoticonsContents checks fields all responses are commonly expected to adhere to
func smokeTestGetEmoticonsContents(response *janus.GetEmoticonsResponse) {
	Expect(response.EmoticonSets).To(Not(BeNil()))
}
