package integration_tests

import (
	"code.justin.tv/commerce/janus/client"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/identity_provider"
	. "code.justin.tv/commerce/janus/integration_tests/janus_client"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Test GetActiveMarketplaces", func() {
	var (
		resp *janus.GetActiveMarketplacesResponse
		err  JanusError
	)

	Describe("Getting activeMarketplaces for a user", func() {
		Context("as an unauthenticated user", func() {
			BeforeEach(func() {
				anonymousUser := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_UNAUTHENTICATED))
				actualUser := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN))
				resp, err = janusClient.GetActiveMarketplaces(anonymousUser, *actualUser.Tuid)
			})

			It("fails to authorize the call", func() {
				Expect(err).To(Not(BeNil()))
				Expect(err.Status()).To(BeEquivalentTo(JANUS_FORBIDDEN))
			})
		})

		Context("as an authenticated user for their own activeMarketplaces", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN))
				resp, err = janusClient.GetActiveMarketplaces(identity, *identity.Tuid)
			})

			It("succeeds", func() {
				Expect(err).To(BeNil())
			})

			It("returns contents", func() {
				smokeTestGetActiveMarketplacesContents(resp)
			})
		})

		Context("as an authenticated user for someone else's activeMarketplaces", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN).IsWhitelisted(false))
				anotherUser := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN).IsWhitelisted(true))
				resp, err = janusClient.GetActiveMarketplaces(identity, *anotherUser.Tuid)
			})

			It("fails to authorize the call", func() {
				Expect(err).To(Not(BeNil()))
				Expect(err.Status()).To(BeEquivalentTo(JANUS_FORBIDDEN))
			})
		})
	})
})

// smokeTestGetActiveMarketplacesContents checks fields all responses are commonly expected to adhere to
func smokeTestGetActiveMarketplacesContents(response *janus.GetActiveMarketplacesResponse) {
	Expect(response.ActiveMarketplaces).To(Not(BeNil()))
}
