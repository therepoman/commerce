package integration_tests

import (
	"fmt"

	. "code.justin.tv/commerce/janus/integration_tests/data_providers/amendment_provider"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/identity_provider"
	. "code.justin.tv/commerce/janus/integration_tests/janus_client"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

// Test SetAmendmentForUsers
var _ = Describe("Test SetAmendmentForUsers", func() {
	var (
		amendment Amendment
		err       JanusError
	)

	Describe("Set valid amendment for user", func() {
		BeforeEach(func() {
			amendment = amendmentProvider.GetAmendment(true)
		})

		Context("as an unauthenticated user", func() {
			BeforeEach(func() {
				anonymousIdentity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_UNAUTHENTICATED))
				existingIdentity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN))
				_, err = janusClient.SetAmendment(anonymousIdentity, *existingIdentity.Tuid, amendment)
			})

			It("fails to authorize the call", func() {
				Expect(err).To(Not(BeNil()))
				Expect(err.Status()).To(BeEquivalentTo(JANUS_FORBIDDEN))
			})
		})

		Context("as an authenticated non-partnered user", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IsPartnered(false))
				_, err = janusClient.SetAmendment(identity, *identity.Tuid, amendment)
			})

			It("fails to authorize the call", func() {
				Expect(err).To(Not(BeNil()))
				Expect(err.Status()).To(BeEquivalentTo(JANUS_FORBIDDEN))
			})
		})

		Context("as an authenticated partnered user", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IsPartnered(true))
				_, err = janusClient.SetAmendment(identity, *identity.Tuid, amendment)
			})

			It("succeeds", func() {
				Expect(err).To(BeNil())
			})
		})

		Context("as the wrong authenticated user", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN))
				differentTuid := fmt.Sprintf("%s%s", *identity.Tuid, "0")
				_, err = janusClient.SetAmendment(identity, differentTuid, amendment)
			})

			It("fails to authorize the call", func() {
				Expect(err).To(Not(BeNil()))
				Expect(err.Status()).To(BeEquivalentTo(JANUS_FORBIDDEN))
			})
		})
	})
})
