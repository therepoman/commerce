package identity_provider

const (
	IDENTITY_PROVIDER_UNAUTHENTICATED = iota
	IDENTITY_PROVIDER_CARTMAN

	// Cartman signing algorithms
	IDENTITY_PROVIDER_CARTMAN_RS256 = "rsa.key"
	IDENTITY_PROVIDER_CARTMAN_HS512 = "hmac.key"
	IDENTITY_PROVIDER_CARTMAN_ES256 = "ecc.key"
)

// IdentityProvider creates references to authentication tokens that satisfy a set of restrictions
type IdentityProvider interface {
	GetIdentity(identityArgs *identityBuilder) Identity
}

// Identity represents calling identities in the integration tests
type Identity struct {
	TwitchAuthorization *string
	Tuid                *string
}
