package identity_provider

import (
	"fmt"

	"code.justin.tv/commerce/janus/integration_tests/data_providers/sandstorm_secret"
)

const (
	CARTMAN_ENDPOINT_PROD = "https://cartman-elb.prod.us-west2.justin.tv"

	SANDSTORM_NONWHITELISTED_PROD_OAUTH_SECRET_NAME = "commerce/janus/production/integ_test_account_nonwhitelisted_staging_oauth"
	SANDSTORM_WHITELISTED_PROD_OAUTH_SECRET_NAME    = "commerce/janus/production/integ_test_account_whitelisted_staging_oauth"
	SANDSTORM_PARTNERED_PROD_OAUTH_SECRET_NAME      = "commerce/janus/production/integ_test_account_partner_staging_oauth"
)

// prodIdentityProvider creates Cartman tokens that are valid within a prod testing environment
type prodIdentityProvider struct {
	nonWhitelistedOAuth string
	whitelistedOAuth    string
	partneredOAuth      string
}

// NewProdIdentityProvider is the constructor for prodIdentityProvider
func NewProdIdentityProvider(sandstormSecretProvider sandstorm_secret.SandstormSecretProvider) IdentityProvider {
	nonWhitelistedOAuth := sandstormSecretProvider.AssertSecret(SANDSTORM_NONWHITELISTED_PROD_OAUTH_SECRET_NAME)
	whitelistedOAuth := sandstormSecretProvider.AssertSecret(SANDSTORM_WHITELISTED_PROD_OAUTH_SECRET_NAME)
	partneredOAuth := sandstormSecretProvider.AssertSecret(SANDSTORM_PARTNERED_PROD_OAUTH_SECRET_NAME)

	return &prodIdentityProvider{
		nonWhitelistedOAuth: string(nonWhitelistedOAuth.Plaintext),
		whitelistedOAuth:    string(whitelistedOAuth.Plaintext),
		partneredOAuth:      string(partneredOAuth.Plaintext),
	}
}

// GetIdentity creates Cartman tokens that are valid within a prod testing environment
func (prod *prodIdentityProvider) GetIdentity(identityBuilder *identityBuilder) Identity {
	switch identityBuilder.identityType {
	case IDENTITY_PROVIDER_UNAUTHENTICATED:
		return Identity{}
	case IDENTITY_PROVIDER_CARTMAN:
		// Ideally users with matching attributes can be asserted for each test - however this is not possible
		// with our current dependencies (i.e we'd need programmatic method of setting staff accounts and partnered
		// statuses).
		//
		// For now we create a pool of accounts that we can use for testing.  As the following code suggests, this
		// does grow by 2^n until we address this.
		var cartmanResponse AuthenticateResponse
		if identityBuilder.isWhitelisted {
			if identityBuilder.isPartnered {
				panic("No whitelisted partnered account implemented")
			} else {
				cartmanResponse = GetCartmanToken(CARTMAN_ENDPOINT_PROD, prod.whitelistedOAuth, identityBuilder.signatureAlgorithm)
			}
		} else {
			if identityBuilder.isPartnered {
				cartmanResponse = GetCartmanToken(CARTMAN_ENDPOINT_PROD, prod.partneredOAuth, identityBuilder.signatureAlgorithm)
			} else {
				cartmanResponse = GetCartmanToken(CARTMAN_ENDPOINT_PROD, prod.nonWhitelistedOAuth, identityBuilder.signatureAlgorithm)
			}
		}
		return Identity{
			TwitchAuthorization: &cartmanResponse.Token,
			Tuid:                &cartmanResponse.UserId,
		}
	default:
		panic(fmt.Sprintf("unrecognized identity type %d", identityBuilder.identityType))
	}
}
