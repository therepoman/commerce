package identity_provider

// identityBuilder represents a builder for stage-agnostic arguments used in IdentityProvider.GetIdentity
type identityBuilder struct {
	identityType       int
	signatureAlgorithm string
	isWhitelisted      bool
	isPartnered        bool
}

// DefaultIdentity returns a builder with default
func DefaultIdentity() *identityBuilder {
	return &identityBuilder{
		identityType:       IDENTITY_PROVIDER_CARTMAN,
		signatureAlgorithm: IDENTITY_PROVIDER_CARTMAN_RS256,
		isWhitelisted:      false,
		isPartnered:        false,
	}
}

// IdentityType overrides the default identity type
func (ib *identityBuilder) IdentityType(identityType int) *identityBuilder {
	ib.identityType = identityType
	return ib
}

// SignatureAlgorithm overrides the signature algorithm requested from Cartman
func (ib *identityBuilder) SignatureAlgorithm(signatureAlgorithm string) *identityBuilder {
	ib.signatureAlgorithm = signatureAlgorithm
	return ib
}

// IsWhitelisted overrides the default whitelisted status
func (ib *identityBuilder) IsWhitelisted(isWhitelisted bool) *identityBuilder {
	ib.isWhitelisted = isWhitelisted
	return ib
}

// IsPartnered overrides the default partner status
func (ib *identityBuilder) IsPartnered(isPartnered bool) *identityBuilder {
	ib.isPartnered = isPartnered
	return ib
}
