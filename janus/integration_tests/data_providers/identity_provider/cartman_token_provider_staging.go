package identity_provider

import (
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/sandstorm_secret"
)

// NewDevIdentityProvider is the constructor for devIdentityProvider
func NewStagingIdentityProvider(sandstormSecretProvider SandstormSecretProvider) IdentityProvider {
	return NewDevIdentityProvider(sandstormSecretProvider)
}
