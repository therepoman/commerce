package identity_provider

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// Requests a Cartman token from the given endpoint with no scope and then returns the parsed result
func GetCartmanToken(endpoint string, oauth string, algorithm string) AuthenticateResponse {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/authorization_token?capabilities=cartman::authenticate_user&key=%s", endpoint, algorithm), nil)
	if err != nil {
		panic(err)
	}
	req.Header.Add("Authorization", fmt.Sprintf("OAuth %s", oauth))

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		panic(err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	authenticateResponse := &AuthenticateResponse{}
	err = json.Unmarshal(body, authenticateResponse)
	if err != nil {
		panic(err)
	}

	return *authenticateResponse
}

// AuthenticateResponse is the parsed Cartman authorization response
type AuthenticateResponse struct {
	Token  string `json:"token"`
	UserId string `json:"user_id"`
}
