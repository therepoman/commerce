package cartman_token

// DevAmendments provides amendments that are usable in the dev environment
type DevAmendmentProvider struct{}

// GetAmendment returns an amendment in a dev environment
func (devAmendment *DevAmendmentProvider) GetAmendment(valid bool) Amendment {
	if valid {
		return Amendment{
			Name: "game_commerce",
		}
	} else {
		return Amendment{
			Name: "non_existent_name",
		}
	}
}
