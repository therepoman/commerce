package cartman_token

// AmendmentProvider creates references to amendments that satisfy a set of restrictions
type AmendmentProvider interface {
	GetAmendment(valid bool) Amendment
}

// Amendment represents an amendment type used for integration testing
type Amendment struct {
	Name string
}
