package game

// DevGameProvider provides references to games that work in a dev instance of Janus
type DevGameProvider struct{}

func (dev *DevGameProvider) GetGame(released bool) Game {
	if released {
		return Game{
			Title: PtrS("streamline"),
		}
	} else {
		return Game{
			Title: PtrS("space fuel"),
		}
	}
}

func PtrS(s string) *string {
	return &s
}
