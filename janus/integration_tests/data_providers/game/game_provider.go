package game

// GameProvider creates references to games satisfying a set of restrictions
type GameProvider interface {
	GetGame(released bool) Game
}

// Game represents a game used for integration testing
type Game struct {
	Title *string
}
