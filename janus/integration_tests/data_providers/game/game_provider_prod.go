package game

// Prod provides references to games that work in the prod instance of Janus
type ProdGameProvider struct{}

func (prod *ProdGameProvider) GetGame(released bool) Game {
	if released {
		return Game{
			Title: PtrS("love"),
		}
	} else {
		return Game{
			Title: PtrS("space fuel"),
		}
	}
}
