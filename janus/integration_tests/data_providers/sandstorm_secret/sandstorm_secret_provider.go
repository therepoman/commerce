package sandstorm_secret

import (
	"code.justin.tv/systems/sandstorm/manager"
)

// SandstormProvider retrieves secrets from Sandstorm
type SandstormSecretProvider interface {
	AssertSecret(secretName string) manager.Secret
}
