package sandstorm_secret

import (
	"time"

	"code.justin.tv/systems/sandstorm/manager"

	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
)

const (
	SANDSTORM_PROVIDER_JANUS_LOCAL_ROLE_ARN = "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/commerce_janus_local_sws"
	SANDSTORM_PROVIDER_PROD_TABLE_NAME      = "sandstorm-production"
	SANDSTORM_PROVIDER_PROD_KEY_ID          = "alias/sandstorm-production"
	SANDSTORM_PROVIDER_PROD_REGION          = "us-west-2"
)

// NewDevSandstormSecretProvider provides sandstorm secrets that work in a dev instance of Janus
type DevSandstormSecretProvider struct {
	sandstorm manager.Manager
}

func NewDevSandstormSecretProvider() SandstormSecretProvider {
	awsConfig := &aws.Config{Region: aws.String(SANDSTORM_PROVIDER_PROD_REGION)}
	stsclient := sts.New(session.New(awsConfig))
	arp := &stscreds.AssumeRoleProvider{
		Duration:     900 * time.Second,
		ExpiryWindow: 10 * time.Second,
		RoleARN:      SANDSTORM_PROVIDER_JANUS_LOCAL_ROLE_ARN,
		Client:       stsclient,
	}
	creds := credentials.NewCredentials(arp)
	awsConfig.WithCredentials(creds)
	config := manager.Config{
		AWSConfig: awsConfig,
		TableName: SANDSTORM_PROVIDER_PROD_TABLE_NAME,
		KeyID:     SANDSTORM_PROVIDER_PROD_KEY_ID,
	}
	return &DevSandstormSecretProvider{
		*manager.New(config),
	}
}

// AssertSecret asserts a secret exists and returns it
func (dev *DevSandstormSecretProvider) AssertSecret(secretName string) manager.Secret {
	secret, err := dev.sandstorm.Get(secretName)
	if err != nil {
		panic(err)
	}
	if secret == nil {
		panic(fmt.Sprintf("secret not found: %s", secretName))
	}
	return *secret
}
