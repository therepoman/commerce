package sandstorm_secret

// Today Dev/Staging/Prod all use the Prod Sandstorm stack
func NewStagingSandstormSecretProvider() SandstormSecretProvider {
	return NewDevSandstormSecretProvider()
}
