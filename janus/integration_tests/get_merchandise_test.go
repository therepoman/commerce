package integration_tests

import (
	"code.justin.tv/commerce/janus/client"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/identity_provider"
	. "code.justin.tv/commerce/janus/integration_tests/janus_client"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

// Test GetMerchandise
var _ = Describe("Test GetMerchandise", func() {
	var (
		resp   *janus.GetMerchandiseResponse
		err    JanusError
		locale string
	)

	Describe("Getting merchandise", func() {
		BeforeEach(func() {
			locale = "en_US"
		})

		Context("as an unauthenticated user", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_UNAUTHENTICATED))
				resp, err = janusClient.GetMerchandise(identity, locale)
			})

			It("succeeds", func() {
				Expect(err).To(BeNil())
			})

			It("returns contents", func() {
				smokeTestGetMerchandiseContents(resp)
			})
		})

		Context("as an authenticated user", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN))
				resp, err = janusClient.GetMerchandise(identity, locale)
			})

			It("succeeds", func() {
				Expect(err).To(BeNil())
			})

			It("returns contents", func() {
				smokeTestGetMerchandiseContents(resp)
			})
		})
	})
})

// smokeTestGetMerchandiseContents checks fields all responses are commonly expected to adhere to
func smokeTestGetMerchandiseContents(response *janus.GetMerchandiseResponse) {
	for _, product := range response.Products {
		Expect(product.ASIN).ShouldNot(BeNil())
	}
}
