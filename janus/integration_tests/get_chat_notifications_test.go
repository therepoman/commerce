package integration_tests

import (
	"code.justin.tv/commerce/janus/client"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/identity_provider"
	. "code.justin.tv/commerce/janus/integration_tests/janus_client"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Test GetChatNotifications", func() {
	var (
		channel string
		resp    *janus.GetChatNotificationsResponse
		err     JanusError
	)

	Describe("Getting chat notifications in a valid channel", func() {
		BeforeEach(func() {
			channelIdentity := identityProvider.GetIdentity(DefaultIdentity())
			channel = *channelIdentity.Tuid
		})

		Context("as an unauthenticated user", func() {
			BeforeEach(func() {
				anonymousUser := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_UNAUTHENTICATED))
				actualUser := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN))
				resp, err = janusClient.GetChatNotifications(anonymousUser, *actualUser.Tuid, channel)
			})

			It("fails to authenticate", func() {
				Expect(err).To(Not(BeNil()))
				Expect(err.Status()).To(BeEquivalentTo(JANUS_FORBIDDEN))
			})
		})

		Context("as an authenticated user for their own chat notifications", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN))
				resp, err = janusClient.GetChatNotifications(identity, *identity.Tuid, channel)
			})

			It("succeeds", func() {
				Expect(err).To(BeNil())
			})

			It("returns contents", func() {
				smokeTestGetChatNotificationsContents(resp)
			})
		})

		Context("as an authenticated user for someone else's chat notifications", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN).IsWhitelisted(true))
				anotherUser := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN).IsWhitelisted(false))
				resp, err = janusClient.GetChatNotifications(identity, *anotherUser.Tuid, channel)
			})

			It("fails to authenticate", func() {
				Expect(err).To(Not(BeNil()))
				Expect(err.Status()).To(BeEquivalentTo(JANUS_FORBIDDEN))
			})
		})
	})
})

// smokeTestGetChatNotificationsContents checks fields all responses are commonly expected to adhere to
func smokeTestGetChatNotificationsContents(response *janus.GetChatNotificationsResponse) {
	for _, chatNotifications := range response.ChatNotifications {
		Expect(chatNotifications.Token).To(Not(BeNil()))
		Expect(chatNotifications.Status).To(Not(BeNil()))
		for _, igc := range chatNotifications.Contents.InGameContent {
			Expect(igc.ContentKey).To(Not(BeNil()))
		}
		for _, badges := range chatNotifications.Contents.Badges {
			Expect(badges.ContentKey).To(Not(BeNil()))
		}
		for _, emoteSets := range chatNotifications.Contents.EmoteSets {
			for _, emote := range emoteSets {
				Expect(emote.ContentKey).To(Not(BeNil()))
			}
		}
	}
}
