package integration_tests

import (
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/identity_provider"
	. "code.justin.tv/commerce/janus/integration_tests/janus_client"

	"code.justin.tv/commerce/janus/client"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Test GetBroadcasterPayouts", func() {
	var (
		resp *janus.GetBroadcasterPayoutDetailsResponse
		err  JanusError
	)

	Describe("Getting a valid broadcaster payouts for a user", func() {
		Context("about a non-partnered user", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IsPartnered(false))
				resp, err = janusClient.GetBroadcasterPayouts(identity, *identity.Tuid)
			})

			It("returns false", func() {
				Expect(resp.IsEligible).To(BeFalse())
			})
		})

		Context("about a partnered user", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IsPartnered(true))
				resp, err = janusClient.GetBroadcasterPayouts(identity, *identity.Tuid)
			})

			It("returns true", func() {
				Expect(err).To(BeNil())
				Expect(resp.IsEligible).To(BeTrue())
			})
		})

		/*
			TODO: Need staging staff account
		*/
	})
})
