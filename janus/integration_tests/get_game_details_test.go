package integration_tests

import (
	"code.justin.tv/commerce/janus/client"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/game"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/identity_provider"
	. "code.justin.tv/commerce/janus/integration_tests/janus_client"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

// Test GetGameDetails
var _ = Describe("Test GetGameDetails", func() {
	var (
		game Game
		resp *janus.GetGameDetailsResponse
		err  JanusError
	)

	Describe("Getting details for a released game", func() {
		BeforeEach(func() {
			game = gameProvider.GetGame(true)
		})

		Context("as an unauthenticated user", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_UNAUTHENTICATED))
				resp, err = janusClient.GetGameDetails(identity, game)
			})

			It("succeeds", func() {
				Expect(err).To(BeNil())
			})

			It("returns contents", func() {
				smokeTestGetGameDetailsResponse(resp)
			})
		})

		Context("as an authenticated user", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN))
				resp, err = janusClient.GetGameDetails(identity, game)
			})

			It("succeeds", func() {
				Expect(err).To(BeNil())
			})

			It("returns contents", func() {
				smokeTestGetGameDetailsResponse(resp)
			})
		})

		Context("as an authenticated whitelisted user", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IsWhitelisted(true))
				resp, err = janusClient.GetGameDetails(identity, game)
			})

			It("succeeds", func() {
				Expect(err).To(BeNil())
			})

			It("returns contents", func() {
				smokeTestGetGameDetailsResponse(resp)
			})
		})
	})

	Describe("Getting details for an unreleased, future game", func() {
		BeforeEach(func() {
			game = gameProvider.GetGame(false)
		})

		Describe("as an unauthenticated user", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_UNAUTHENTICATED))
				resp, err = janusClient.GetGameDetails(identity, game)
			})

			It("does not return results", func() {
				Expect(err).To(Not(BeNil()))
				Expect(err.Status()).To(BeEquivalentTo(JANUS_NOT_FOUND))
			})

		})

		Describe("as an authenticated user", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN))
				resp, err = janusClient.GetGameDetails(identity, game)
			})

			It("does not return results", func() {
				Expect(err).To(Not(BeNil()))
				Expect(err.Status()).To(BeEquivalentTo(JANUS_NOT_FOUND))
			})

		})

		Describe("as an authenticated whitelisted user", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IsWhitelisted(true))
				resp, err = janusClient.GetGameDetails(identity, game)
			})

			It("succeeds", func() {
				Expect(err).To(BeNil())
			})

			It("returns contents", func() {
				smokeTestGetGameDetailsResponse(resp)
			})
		})
	})
})

// smokeTestGetGameDetailsResponse checks fields all responses are commonly expected to adhere to
func smokeTestGetGameDetailsResponse(response *janus.GetGameDetailsResponse) {
	Expect(response.Product.ASIN).To(Not(BeEmpty()))
	Expect(response.Product.Description).To(Not(BeEmpty()))
	Expect(response.Product.Title).To(Not(BeEmpty()))

	if timeNow.After(response.Product.ReleaseStartDate) && timeNow.Before(response.Product.ReleaseEndDate) {
		Expect(response.Product.Price.Price).To(Not(BeEmpty()))
		Expect(response.ActionDetails.DestinationURL).To(Not(BeEmpty()))
	} else {
		Expect(response.Product.Price.Price).To(BeEmpty())
		Expect(response.ActionDetails.DestinationURL).To(BeEmpty())
	}
}
