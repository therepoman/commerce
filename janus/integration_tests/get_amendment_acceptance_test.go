package integration_tests

import (
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/amendment_provider"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/identity_provider"
	. "code.justin.tv/commerce/janus/integration_tests/janus_client"

	"code.justin.tv/commerce/janus/client"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Test GetAmendmentAcceptance", func() {
	var (
		amendment Amendment
		resp      *janus.GetAmendmentAcceptanceResponse
		err       JanusError
	)

	Describe("Getting a valid amendment acceptance for a user", func() {
		BeforeEach(func() {
			amendment = amendmentProvider.GetAmendment(true)
		})

		Context("as an unauthenticated user", func() {
			BeforeEach(func() {
				anonymousUser := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_UNAUTHENTICATED))
				actualUser := identityProvider.GetIdentity(DefaultIdentity().IdentityType(IDENTITY_PROVIDER_CARTMAN))
				resp, err = janusClient.GetAmendment(anonymousUser, *actualUser.Tuid, amendment)
			})

			It("fails to authenticate", func() {
				Expect(err).To(Not(BeNil()))
				Expect(err.Status()).To(BeEquivalentTo(JANUS_FORBIDDEN))
			})
		})

		Context("as an authenticated non-partnered user for their own amendment acceptance", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IsPartnered(false))
				resp, err = janusClient.GetAmendment(identity, *identity.Tuid, amendment)
			})

			It("fails to authorize", func() {
				Expect(err).To(Not(BeNil()))
				Expect(err.Status()).To(BeEquivalentTo(JANUS_FORBIDDEN))
			})
		})

		Context("as an authenticated partnered user for their own amendment acceptance", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(DefaultIdentity().IsPartnered(true))
				resp, err = janusClient.GetAmendment(identity, *identity.Tuid, amendment)
			})

			It("succeeds", func() {
				Expect(err).To(BeNil())
			})

			It("returns contents", func() {
				smokeTestGetAmendmentAcceptance(resp)
			})
		})

		/*
			TODO: Need staging staff account
		*/
	})
})

// smokeTestGetAmendmentAcceptance checks fields all responses are commonly expected to adhere to
func smokeTestGetAmendmentAcceptance(response *janus.GetAmendmentAcceptanceResponse) {
	Expect(response.AmendmentAccepted).To(Not(BeNil()))
}
