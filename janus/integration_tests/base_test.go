package integration_tests

import (
	"os"
	"time"

	. "code.justin.tv/commerce/janus/integration_tests/data_providers/amendment_provider"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/game"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/identity_provider"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/sandstorm_secret"
	. "code.justin.tv/commerce/janus/integration_tests/janus_client"

	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var (
	timeZero = time.Time{}
	timeNow  = time.Now()

	amendmentProvider AmendmentProvider
	identityProvider  IdentityProvider
	gameProvider      GameProvider
	janusClient       JanusIntegrationTestClient
)

var _ = BeforeSuite(func() {
	SetupDataProviders()
})

// SetupDataProviders configures data providers for the appropriate integration testing environment
func SetupDataProviders() {
	env := os.Getenv("INTEGRATION_TEST_ENVIRONMENT")

	switch env {
	case "DEV":
		sandstormSecretProvider := NewDevSandstormSecretProvider()
		amendmentProvider = &DevAmendmentProvider{}
		identityProvider = NewDevIdentityProvider(sandstormSecretProvider)
		gameProvider = &DevGameProvider{}
		janusClient = NewHttpJanusIntegClient("http://localhost:8000")
	case "STAGING":
		sandstormSecretProvider := NewStagingSandstormSecretProvider()
		amendmentProvider = &StagingAmendmentProvider{}
		identityProvider = NewStagingIdentityProvider(sandstormSecretProvider)
		gameProvider = &StagingGameProvider{}
		janusClient = NewHttpJanusIntegClient("http://janus-staging-dev-def.us-west2.justin.tv")
	case "PROD":
		sandstormSecretProvider := NewProdSandstormSecretProvider()
		amendmentProvider = &ProdAmendmentProvider{}
		identityProvider = NewProdIdentityProvider(sandstormSecretProvider)
		gameProvider = &ProdGameProvider{}
		janusClient = NewHttpJanusIntegClient("https://janus.prod.us-west2.justin.tv")
	default:
		panic("Invalid environment value for INTEGRATION_TEST_ENVIRONMENT")
	}
}

func TestSuite(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Janus Suite")
}
