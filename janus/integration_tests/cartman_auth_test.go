package integration_tests

import (
	"code.justin.tv/commerce/janus/client"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/game"
	. "code.justin.tv/commerce/janus/integration_tests/data_providers/identity_provider"
	. "code.justin.tv/commerce/janus/integration_tests/janus_client"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

// Test Cartman signature validation
var _ = Describe("Test Cartman signature validation", func() {
	var (
		game Game
		resp *janus.GetGameDetailsResponse
		err  JanusError
	)

	Describe("Getting details for an unreleased, future game, as an authenticated whitelisted user", func() {
		BeforeEach(func() {
			game = gameProvider.GetGame(false)
		})

		Describe("signed with RS256", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(
					DefaultIdentity().IsWhitelisted(true).SignatureAlgorithm(IDENTITY_PROVIDER_CARTMAN_RS256))
				resp, err = janusClient.GetGameDetails(identity, game)
			})

			It("succeeds", func() {
				Expect(err).To(BeNil())
			})

			It("returns contents", func() {
				smokeTestGetGameDetailsResponse(resp)
			})
		})

		Describe("signed with ES256", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(
					DefaultIdentity().IsWhitelisted(true).SignatureAlgorithm(IDENTITY_PROVIDER_CARTMAN_ES256))
				resp, err = janusClient.GetGameDetails(identity, game)
			})

			It("succeeds", func() {
				Expect(err).To(BeNil())
			})

			It("returns contents", func() {
				smokeTestGetGameDetailsResponse(resp)
			})
		})

		Describe("signed with HS512", func() {
			BeforeEach(func() {
				identity := identityProvider.GetIdentity(
					DefaultIdentity().IsWhitelisted(true).SignatureAlgorithm(IDENTITY_PROVIDER_CARTMAN_HS512))
				resp, err = janusClient.GetGameDetails(identity, game)
			})

			It("succeeds", func() {
				Expect(err).To(BeNil())
			})

			It("returns contents", func() {
				smokeTestGetGameDetailsResponse(resp)
			})
		})
	})
})
