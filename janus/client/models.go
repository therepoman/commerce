package janus

import "time"

// GetGameDetailsResponse is the response structure for the GetGameDetails API.
type GetGameDetailsResponse struct {
	ActionDetails   *ActionDetails   `json:"action_details"`
	Product         *Product         `json:"product"`
	UserEntitlement *UserEntitlement `json:"user_entitlement"`
}

// GetInGameContentDetailsResponse is the response structure for the GetInGameContentDetails API.
type GetInGameContentDetailsResponse struct {
	InGameContent []InGameContent `json:"in_game_content"`
}

// GetMerchandiseResponse is the response structure for the GetMerchandise API.
type GetMerchandiseResponse struct {
	Products []Product `json:"products"`
}

// GetOffersResponse is the response structure for the GetMerchandise API.
type GetOffersResponse struct {
	RetailProducts []RetailProduct `json:"retail_products"`
}

// GetGameSettingsResponse is the response structure for the GetGameSettings API.
type GetGameSettingsResponse struct {
	DetailsEnabled bool `json:"details_enabled"`
}

// GetOffersSettingsResponse is the response structure for the GetOffersSettings API.
type GetOffersSettingsResponse struct {
	AmazonRetailEnabled bool `json:"amazon_retail_enabled"`
}

// GameAsinMap represents the mapping from game title to asin and the status of the mapping
type GameAsinMap struct {
	Asin      string `json:"asin"`
	GameTitle string `json:"game_title"` // e.g. "Starcraft 2"
	Status    string `json:"status"`     // e.g. "LIVE", "SUPPRESSED"
}

// GetAmendmentAcceptanceResponse is the response structure for the AmendmentAcceptance API.
type GetAmendmentAcceptanceResponse struct {
	AmendmentAccepted bool `json:"amendment_accepted"`
}

// SetAmendmentAcceptanceResponse is the response structure for the AmendmentAcceptance API.
type SetAmendmentAcceptanceResponse struct {
	AcceptDate time.Time `json:"accept_date"`
}

// GetBroadcasterPayoutDetailsResponse is the response structure for the BroadcasterPayout API.
type GetBroadcasterPayoutDetailsResponse struct {
	IsEligible bool `json:"is_eligible"`
}

// GetBroadcasterRetailPayoutDetailsResponse is the response structure for the BroadcasterRetailPayout API.
type GetBroadcasterRetailPayoutDetailsResponse struct {
	IsEligible bool `json:"is_eligible"`
}

// GetGameCommerceRevenueResponse is the response structure for the ExtensionsRevenue API
type GetExtensionsRevenueResponse struct {
	Records []GameCommerceRevenueRecord `json:"records"`
}

// GetGameCommerceRevenueResponse is the response structure for the GameCommerceRevenue API
type GetGameCommerceRevenueResponse struct {
	Records []GameCommerceRevenueRecord `json:"records"`
}

// GetCratesResponse is the response structure for the GetCrates API
type GetCratesResponse struct {
	Crates []CrateGood `json:"crates"`
}

// GetEmoticonsResponse is the response structure for the GetEmoticons API
type GetEmoticonsResponse struct {
	EmoticonSets map[string][]Emoticon `json:"emoticon_sets"`
}

// GetBadgesResponse is the response structure for the GetBadges API
type GetBadgesResponse struct {
	BadgeSets map[string]BadgeSet `json:"badge_sets"`
}

// GetChatNotificationsResponse is the response structure for the GetChatNotifications API
type GetChatNotificationsResponse struct {
	ChatNotifications []ChatNotification `json:"chat_notifications"`
}

// ConsumeChatNotificationsRequest is the request structure for the ConsumeChatNotifications API
type ConsumeChatNotificationsRequest struct {
	Token   string `json:"token"`
	Message string `json:"message"`
	Type    string `json:"type"`
}

// GetActiveMarketplacesResponse provides information related to active marketplaces for a user
type GetActiveMarketplacesResponse struct {
	ActiveMarketplaces []ActiveMarketplace `json:"active_marketplaces"`
}

// ActiveMarketplace provides information related to an active marketplace
type ActiveMarketplace struct {
	DisplayText             string `json:"display_text"`
	OrderHistoryRedirectURL string `json:"order_history_redirect_url"`
}

// GameCommerceRevenueRecord represents a single record of revenue
type GameCommerceRevenueRecord struct {
	TimeOfEvent  time.Time `json:"time_of_event"`
	RevenueCents float64   `json:"revenue_cents"`
}

// Product aggregates product information.
type Product struct {
	ASIN                string                   `json:"asin"`                // Amazon identifier for this product
	DetailPageURL       string                   `json:"detail_page_url"`     // detail page of merchandise product
	Crate               Crate                    `json:"crate"`               // Crate information
	Crates              []Crate                  `json:"crates"`              // Crate information
	Description         string                   `json:"description"`         // HTML Marked-up game description
	DeveloperName       string                   `json:"developer_name"`      // e.g. Blizzard
	Edition             string                   `json:"edition"`             // e.g. Collector's Edition
	EULAURL             string                   `json:"eula_url"`            // developer-provided EULA link for their game
	FeatureDetails      []string                 `json:"feature_details"`     // array of developer-specified bullet points about their game
	FeatureIcons        []FeatureIcon            `json:"feature_icons"`       // array of feature icons like "Multiplayer", etc.
	FulfillmentDetails  FulfillmentDetails       `json:"fulfillment_details"` // Details associated with the fulfillmenet type of the asin
	GameID              string                   `json:"game_id"`             // Twitch identifier for this product
	GameEmbargoDate     time.Time                `json:"game_embargo_date"`   // Date that the game's detail page ends being viewable on twitch.tv
	GamePublicationDate time.Time                `json:"-"`                   // Date that the game's detail page starts being viewable on twitch.tv
	Genres              []string                 `json:"genres"`              // list of genres for this product
	IsOutOfStock        bool                     `json:"is_out_of_stock"`     // the availability status of the item, if applicable
	IsNew               bool                     `json:"is_new"`              // a flag indicating whether the product is new on twitch
	IsFeatured          bool                     `json:"is_featured"`         // a flag indicating whether the product is featured
	MaturityRatings     MaturityRatings          `json:"maturity_ratings"`    // e.g. PEGI and ESRB ratings
	Media               Media                    `json:"media"`               // Screenshots, Videos, Box Art, etc.
	OriginalPrice       Price                    `json:"original_price"`      // Non-sale price of product offer
	Price               Price                    `json:"price"`               // Current price of product offer
	PublisherName       string                   `json:"publisher_name"`      // e.g. Electronic Arts
	Rank                int64                    `json:"rank"`                // Used to order products in a list
	ReleaseEndDate      time.Time                `json:"release_end_date"`    // Date that this game ends being buyable on Fuel
	ReleaseStartDate    time.Time                `json:"-"`                   // Date that this game starts being buyable on Fuel
	SKU                 string                   `json:"sku"`                 // SKU for the product
	SupportedLanguages  []Language               `json:"supported_languages"` // Languages and T/F values for various
	SupportedPlatforms  []Platform               `json:"supported_platforms"` // e.g. PC, XBox One, etc.
	SupportURL          string                   `json:"support_url"`         // e.g. support.valve.com
	SystemRequirements  TieredSystemRequirements `json:"system_requirements"` // System requirements needed to run the game
	Title               string                   `json:"title"`               // e.g. "Overwatch"
	VendorId            string                   `json:"vendor_id"`           // e.g. "JQDG0" for the game "Frozen Cortex"
	WebsiteURL          string                   `json:"website_url"`         // e.g. www.overwatch.com
}

// Retail Product aggreagates retail product information
type RetailProduct struct {
	ASIN            string   `json:"asin"`              // Amazon identifier for this product
	Brand           string   `json:"brand"`             // The brand associated with the product (e.g. Rockstar Games)
	Crates          []Crate  `json:"crates"`            // Crate information
	Description     string   `json:"description"`       // Product description
	DetailBullets   []string `json:"detail_bullets"`    // Array of bullet point details about product
	DetailPageURL   string   `json:"detail_page_url"`   // Detail page of retail product
	IsPrimeEligible bool     `json:"is_prime_eligible"` // Whether the item is available for prime shipping
	Media           Media    `json:"media"`             // Screenshots, Videos, Box Art, etc.
	Price           Price    `json:"price"`             // Current price of product offer
	Title           string   `json:"title"`             // e.g. "Overwatch"
}

// GetExtensionsProductsResponse aggregates all product ASINs associated with an Extension
type GetExtensionsProductsResponse struct {
	VendorCode string             `json:"vendor_code"`
	Products   []ExtensionProduct `json:"products"`
}

// ExtensionProduct aggregates product information
type ExtensionProduct struct {
	ActionDetails    *ActionDetails `json:"action_details"`
	ASIN             string         `json:"asin"`              // Amazon identifier for this product
	Description      string         `json:"description"`       // Description of this product
	DeveloperName    string         `json:"developer_name"`    // Name of the developer of this product
	Price            Price          `json:"price"`             // Current price of this product
	ShortDescription string         `json:"short_description"` // Short description of this product
	SKU              string         `json:"sku"`               // SKU for this product
	Title            string         `json:"title"`             // Name of this product
}

// Crate contains crate information associated with the asin
type Crate struct {
	ASIN string `json:"asin"`
}

// FeatureIcon represents a single feature and it's associate icon URL. e.g. "Multiplayer", "Controller support"
type FeatureIcon struct {
	IconURL string `json:"icon_url"`
	Name    string `json:"name"`
}

// MaturityRatings aggregates maturity ratings from the multiple ratings boards. e.g. ESRB, PEGI.
type MaturityRatings struct {
	ESRB MaturityRating `json:"esrb"`
	PEGI MaturityRating `json:"pegi"`
}

// MaturityRating represents a maturity rating for this product, and its associated icon.
type MaturityRating struct {
	Details []string `json:"details"` // e.g. "Blood", "Violence", "Use of Tobacco"
	IconURL string   `json:"icon_url"`
	Rating  string   `json:"rating"`
}

// Media aggregates all media associated with this product. E.g. screnshots, box art, videos.
type Media struct {
	Images             []Image  `json:"images"`
	BackgroundImageURL string   `json:"background_image_url"`
	BoxArtURL          string   `json:"box_art_url"`
	ScreenshotURLs     []string `json:"screenshot_urls"`
	Videos             []Video  `json:"videos"`
}

// Image contains information of a single image, including it's URL and dimensions
type Image struct {
	ImageURL string `json:"image_url"`
	Width    int    `json:"width"`
	Height   int    `json:"height"`
}

// Platform represents a platform upon which this product is available. e.g. this product is available on "PC"
type Platform struct {
	IconURL  string `json:"icon_url"`
	Platform string `json:"platform"` // e.g. "PC", "XBox One", etc.
}

// Price aggregates price information
type Price struct {
	CurrencyUnit string `json:"currency_unit"`
	Price        string `json:"price"`
}

// TieredSystemRequirements aggregates multiple tiers of system requirements, including minimum and recommended specs
type TieredSystemRequirements struct {
	Minimum     SystemRequirements `json:"minimum"`
	Recommended SystemRequirements `json:"recommended"`
}

// SystemRequirements aggregates information about the hardware needed to run the product
type SystemRequirements struct {
	DirectXVersion string   `json:"direct_x_version"`
	HardDriveSpace string   `json:"hard_drive_space"`
	OSVersion      []string `json:"os_version"`
	Other          string   `json:"other"`
	Processor      []string `json:"processor"`
	RAM            string   `json:"ram"`
	VideoCard      []string `json:"video_card"`
}

// Video contains information of a single video, including its ID and thumbnail screenshot URL
type Video struct {
	VideoID      string `json:"video_id"`
	ThumbnailURL string `json:"thumbnail_url"`
}

// UserEntitlement aggregates information related to a user's ownership of the specified product.
type UserEntitlement struct {
	Entitled bool   `json:"entitled"`
	UserID   string `json:"user_id"`
}

// ActionDetails provides information related to the action that should be taken upon pressing the button on the detail
// page
type ActionDetails struct {
	DestinationURL string `json:"destination_url"` // URL to send the user to after pressing the action button
}

// FulfillmentDetails provides information related to the acquiring and launching the game
type FulfillmentDetails struct {
	AcquisitionDetails AcquisitionDetails `json:"acquisition_details"`
	PlatformDetails    PlatformDetails    `json:"platform_details"`
}

// AcquisitionDetails contains information about how a game is acquired.
// e.g. "Get this Game" link that directs to the developer's external free-to-play download page
type AcquisitionDetails struct {
	IsExternalAcquisition  bool   `json:"is_external_acquisition"`
	AcquisitionDownloadURL string `json:"acquisition_download_url"`
	AcquisitionName        string `json:"acquisition_name"`
	AcquisitionText        string `json:"acquisition_text"`
}

// LaunchDetails contains information about how a game is launched.
// e.g. "Play on UPlay" link that directs to a UPlay external page
type PlatformDetails struct {
	IsExternalPlatform  bool   `json:"is_external_platform"`
	PlatformDownloadURL string `json:"platform_download_url"`
	PlatformName        string `json:"platform_name"`
	PlatformText        string `json:"platform_text"`
}

// InGameContent surfaces information about the IGC
type InGameContent struct {
	ActionDetails   *ActionDetails   `json:"action_details"`
	ContentDetails  *ContentDetails  `json:"content_details"`
	UserEntitlement *UserEntitlement `json:"user_entitlement"`
}

// ContentDetails aggregates information related to a specific IGC
type ContentDetails struct {
	Crate               Crate     `json:"crate"`
	Crates              []Crate   `json:"crates"`
	ASIN                string    `json:"asin"`
	Description         string    `json:"description"`
	ShortDescription    string    `json:"short_description"`
	GameEmbargoDate     time.Time `json:"game_embargo_date"`
	GamePublicationDate time.Time `json:"-"`
	Media               Media     `json:"media"`
	Price               Price     `json:"price"`
	SKU                 string    `json:"sku"`
	Tags                []string  `json:"tags"`
	Title               string    `json:"title"`
	Type                string    `json:"type"`
	VendorCode          string    `json:"vendor_code"`
	IsFeatured          bool      `json:"is_featured"`
}

// CrateGood provides information related to a crate good
type CrateGood struct {
	CrateID         string `json:"crate_id"`          // the associated crate id
	OpenCrateURL    string `json:"open_crate_url"`    // the url to requested when opening the crate
	ProductIconURL  string `json:"product_icon_url"`  // the product icon url for the crate
	ProductIconType string `json:"product_icon_type"` // the product icon type for the crate.  UI will prefer this or fallback to ProductIconURL
	ProductTitle    string `json:"product_title"`     // the product title for the crate
}

// ChatNotification aggregates information related to a user's chat notification entitlement
type ChatNotification struct {
	ChannelID    string               `json:"channel_id"`
	UserID       string               `json:"user_id"`
	Token        string               `json:"token"`
	ASIN         string               `json:"asin"`
	Title        string               `json:"title"`
	ImageURL     string               `json:"image_url"`
	Status       string               `json:"status"`
	TimeReceived time.Time            `json:"time_received"`
	Contents     NotificationContents `json:"contents"`
}

// NotificationContents represents the contents related to a chat notification
type NotificationContents struct {
	CrateCount    int                         `json:"crate_count"`
	EmoteSets     map[string][]Emoticon       `json:"emote_sets"`
	Badges        []Badge                     `json:"badges"`
	Bits          []Bits                      `json:"bits"`
	InGameContent []InGameContentNotification `json:"in_game_content"`
}

// Emoticon provides information related to an emoticon
type Emoticon struct {
	ID         string `json:"id"`   // the emoticon id
	Code       string `json:"code"` // the emoticon code
	ContentKey string `json:"content_key,omitempty"`
}

// BadgeSet provides information related to a badge set
type BadgeSet struct {
	Versions map[string]Badge `json:"versions"`
}

// Badge provides information related to a badge
type Badge struct {
	ID          string `json:"id,omitempty"`
	ImageURL1X  string `json:"image_url_1x"`
	ImageURL2X  string `json:"image_url_2x"`
	ImageURL4X  string `json:"image_url_4x"`
	Description string `json:"description"`
	Title       string `json:"title"`
	ClickAction string `json:"click_action"`
	ClickURL    string `json:"click_url"`
	ContentKey  string `json:"content_key,omitempty"`
	Version     string `json:"version"`
}

// Bits provides information related to bits
type Bits struct {
	Quantity   string `json:"quantity"`
	ContentKey string `json:"content_key"`
}

// InGameContentNotification provides information related to in-game content
type InGameContentNotification struct {
	Asin        string `json:"asin"`
	BoxArtURL   string `json:"box_art_url"`
	Title       string `json:"title"`
	Description string `json:"description"`
	ContentKey  string `json:"content_key"`
	UserID      string `json:"user_id"`
}

// Language provides information about how the language is supported for a product
type Language struct {
	IsAudio     bool   `json:"is_audio"`
	IsInterface bool   `json:"is_interface"`
	IsSubtitles bool   `json:"is_subtitles"`
	Language    string `json:"language"`
}

// GetAssociatesStoreResponse is the response structure for the GetAssociatesStore API.
type GetAssociatesStoreResponse struct {
	StoreID         string `json:"store_id"`
	IsPayoutEnabled bool   `json:"is_payout_enabled"`
}
