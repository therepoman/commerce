package janus

import (
	"fmt"
	"net/http/httptest"
	"testing"

	"net/http"

	"code.justin.tv/foundation/twitchclient"
	. "github.com/smartystreets/goconvey/convey"
	"golang.org/x/net/context"
)

const response = `{
    "product": {
        "asin": "BT00LXO0JY",
        "additional_product_information": "This is a full description with some simple markup applied. <br/><b>Like bold text</b>",
        "bullet_points": {
            "0": "Bullet 1",
            "1": "Bullet 2",
            "2": "Bullet 3"
        },
        "description": "null",
        "developer": {
            "email": "support@twitch.tv",
            "name": "Twitch",
            "privacy_policy": "twitch.tv",
            "url": "amazon.com"
        },
        "distinctive_title": "null",
        "distribution_rights_region": "null",
        "edition": "first",
        "esrb_rating": "null",
        "eula": "null",
        "external_destination_url": "www.mydestination.com",
        "format": "electronic_software_download",
        "game_id": "BT00LXO0JY",
        "generic_keywords": {
            "0": "Twitch test game"
        },
        "genre": "null",
        "is_externally_entitled": true,
        "manufacturer_info": "This product may or may not be real.",
        "new_in_this_version": {
            "0": "null"
        },
        "platform_for_display": "null",
        "price": {
            "curency_unit": "",
            "price": ""
        },
        "product_key_information_type": "null",
        "product_type": "DOWNLOADABLE_VIDEO_GAME",
        "screenshot": [
            "foo.jpg",
            "bar.jpg"
        ],
        "small_screen_description": "null",
        "specifications": {
            "access_method": "Keyboard",
            "hardware_platform": "PC",
            "processor_architecture_format": "null"
        },
        "subject": "null",
        "subject_keyword": {
            "0": "null"
        },
        "synopsis": "Sample Game Synopsis",
        "title": "Twitch Test Game"
    },
    "user_entitlement": {
        "user_id": "12345",
        "entitled": true
    },
    "premium_eligibility": {
        "is_benefit_available": true,
        "is_user_eligible": true,
        "is_user_prime": true
    },
    "action_details": {
        "ajax_action_url": "www.myajaxcall.com",
        "destination_url": "www.mydestination.com"
    }
}`

const getGameSettingsResponse = `{
    "details_enabled":true
}`

const getExtensionsRevenueResponse = `{
    "records": [
        {
            "time_of_event":"2017-02-01T13:00:00-08:00",
            "revenue_cents":100.9
        }
    ]
}`

const getGameCommerceRevenueResponse = `{
    "records": [
        {
            "time_of_event":"2017-02-01T13:00:00-08:00",
            "revenue_cents":100.9
        }
    ]
}`

const getInGameContentDetailsResponse = `{
    "in_game_content": [
        {
            "action_details": {
                 "ajax_action_url": "www.myajaxcall.com",
                 "destination_url": "www.mydestination.com"
            },
            "content_details": {
                "asin": "ASIN1",
                "description": "First description here.",
                "game_embargo_date": "2017-01-24T17:07:06.278213324-08:00",
                "media": {
                    "background_image_url": "",
                    "box_art_url": "https://www.notreal.com/image.png",
                    "screenshot_urls": null,
                    "videos": null
                },
                "price": {
                    "currency_unit": "USD",
                    "price": "19.99"
                },
                "release_end_date": "2017-01-24T17:07:06.278213349-08:00",
                "sku": "SKU1",
                "tags": [
                    "Legendary",
                    "Epic",
                    "Mediocre"
                ],
                "title": "TW Memorial Wood Crate",
                "vendor_code": "RY123"
            },
            "user_entitlement": {
                "entitled": false,
                "user_id": "12345"
            }
        },
        {
            "action_details": {
                "ajax_action_url": "www.myajaxcall.com",
                "destination_url": "www.mydestination.com"
            },
            "content_details": {
                "asin": "ASIN2",
                "description": "Second description here.",
                "game_embargo_date": "2017-01-24T17:07:06.278214518-08:00",
                "media": {
                    "background_image_url": "",
                    "box_art_url": "https://www.notreal.com/image2.png",
                    "screenshot_urls": null,
                    "videos": null
                },
                "price": {
                    "currency_unit": "USD",
                    "price": "9.99"
                },
                "release_end_date": "2017-01-24T17:07:06.278214544-08:00",
                "sku": "SKU2",
                "tags": [
                    "Just Okay",
                    "Common"
                ],
                "title": "GDC Silver Master Platinum Crate",
                "vendor_code": "DU456"
            },
            "user_entitlement": {
                "entitled": false,
                "user_id": "12345"
            }
        }
    ]
}`

const getCratesResponse = `{
   "crates":[
      {
         "crate_id": "0601f2aa-8e78-4e09-ba26-e0f2905fb78c",
         "open_crate_url": "https://www.notreal.com/openCrate/",
         "product_icon_url": "https://www.notreal.com/image.png",
         "product_icon_type":"SVG",
         "product_title": "Test Crate"
      },
      {
         "crate_id": "0601f2aa-8e78-4e09-ba26-e0f2905fb78c",
         "open_crate_url": "https://www.notreal.com/openCrate/",
         "product_icon_url": "https://www.notreal.com/image.png",
         "product_icon_type":"RASTER",
         "product_title": "Test Crate"
      }
   ]
}`

const getEmoticonsResponse = `{
   "emoticon_sets":{
      "35":[
         {
            "id":"92923",
            "code":"lulRTSD1"
         },
         {
            "id":"90067",
            "code":"lulRushdown"
         }
      ],
      "50":[
         {
            "id":"92923",
            "code":"lulRTSD1"
         },
         {
            "id":"90067",
            "code":"lulRushdown"
         }
      ]
   }
}`

const getBadgesResponse = `{
  "badge_sets": {
    "anomaly-2_1": {
      "versions": {
        "1": {
          "image_url_1x": "https://twitch-chat-badges-staging.s3.amazonaws.com/badge-d1d1ad54-40a6-492b-882e-dcbdce5fa81e-1.png",
          "image_url_2x": "https://twitch-chat-badges-staging.s3.amazonaws.com/badge-d1d1ad54-40a6-492b-882e-dcbdce5fa81e-2.png",
          "image_url_4x": "https://twitch-chat-badges-staging.s3.amazonaws.com/badge-d1d1ad54-40a6-492b-882e-dcbdce5fa81e-3.png",
          "description": "none",
          "title": "none",
          "click_action": "none",
          "click_url": ""
        }
      }
    },
    "anomaly-warzone-earth_1": {
      "versions": {
        "1": {
          "image_url_1x": "https://twitch-chat-badges-staging.s3.amazonaws.com/badge-858be873-fb1f-47e5-ad34-657f40d3d156-1.png",
          "image_url_2x": "https://twitch-chat-badges-staging.s3.amazonaws.com/badge-858be873-fb1f-47e5-ad34-657f40d3d156-2.png",
          "image_url_4x": "https://twitch-chat-badges-staging.s3.amazonaws.com/badge-858be873-fb1f-47e5-ad34-657f40d3d156-3.png",
          "description": "none",
          "title": "none",
          "click_action": "none",
          "click_url": ""
        }
      }
    }
  }
}`

const getActiveMarketplacesResponse = `{
    "active_marketplaces":[
        {
            "display_text":"www.amazon.ca",
            "order_history_redirect_url":"www.amazon.ca/gp/your-account/order-history/ref=oh_aui_search?opt=ab\u0026search=Twitch"
        },
        {
            "display_text":"www.amazon.com",
            "order_history_redirect_url":"www.amazon.com/gp/your-account/order-history/ref=oh_aui_search?opt=ab\u0026search=Twitch"
        }
    ]
}`

const getMerchandiseResponse = `{
	"products": [
		{
			"asin": "MockAsin",
			"detail_page_url": "https://www.amazon.com/dp/MockAsin",
			"is_out_of_stock": false,
			"is_new": false,
			"is_featured": false,
			"media": {
				"images": [
					{
						"image_url": "stubbediconurl.jpg",
						"width": 500,
						"height": 500
					}
				]
			},
			"original_price": {
				"currency_unit": "USD",
				"price": "19.99"
			},
			"price": {
				"currency_unit": "USD",
				"price": "19.99"
			},
			"title": "Stubbed Test Product"
		},
		{
			"asin": "MockAsin",
			"detail_page_url": "https://www.amazon.com/dp/MockAsin",
			"is_out_of_stock": false,
			"is_new": false,
			"is_featured": false,
			"media": {
				"images": [
					{
						"image_url": "stubbediconurl.jpg",
						"width": 500,
						"height": 500
					}
				]
			},
			"original_price": {
				"currency_unit": "USD",
				"price": "19.99"
			},
			"price": {
				"currency_unit": "USD",
				"price": "19.99"
			},
			"title": "Stubbed Test Product"
		}
	]
}`

const getExtensionsProductsResponse = `{
	"vendor_code": "MockVendorCode",
	"products": [
		{
			"action_details": {
				"destination_url": "https://twitch.tv/hey/buy/me"
			},
			"asin": "MockAsin1",
			"price": {
				"currency_unit": "USD",
				"price": "99.99"
			},
			"sku": "MockSKU1"
		},
		{
			"action_details": {
				"destination_url": "https://twitch.tv/or/buy/me"
			},
			"asin": "MockAsin2",
			"price": {
				"currency_unit": "USD",
				"price": "9999.99"
			},
			"sku": "MockSKU2"
		}
	]
}`

const getOffersResponse = `{
    "retail_products": [
        {
            "asin": "B071Z2WDP8",
            "detail_page_url": "www.amazon.twitch.com/associates",
            "crates": [
                {
                    "asin": "B01MYF798N"
                }
            ],
            "media": {
                "images": null,
                "background_image_url": "",
                "box_art_url": "https://images-na.ssl-images-amazon.com/images/I/512yLgPgGUL.jpg",
                "screenshot_urls": null,
                "videos": null
            },
            "price": {
                "currency_unit": "USD",
                "price": "29.99"
            },
            "title": "Injustice 2 - PlayStation 4 Standard Edition"
        }
    ]
}`

const getOffersSettingsResponse = `{
	"amazon_retail_enabled": true
}`

const getBroadcasterPayoutDetailsResponse = `{
	"is_eligible": true
}`

const getBroadcasterRetailPayoutDetailsResponse = `{
	"is_eligible": true
}`

const getAssociatesStoreResponse = `{
	"store_id": "mockid-00",
	"is_payout_enabled": true
}`

const (
	gameID            = "BT00LXO0JY"
	userID            = "12345"
	tuid              = "abcdef"
	locale            = "en-us"
	viewerCountryCode = "US"
	vendorCode        = "MockVendorCode"
	sku               = "MockSKU"
	language          = "us"
)

// Test the GetGameDetails API
func TestGetGameDetails(t *testing.T) {
	Convey("when calling GetGameDetails", t, func() {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			reqPath := fmt.Sprintf("/games/%s/details", gameID)
			reqQuery := fmt.Sprintf("userid=%s", userID)
			if r.Method == "GET" && r.URL.Path == reqPath && r.URL.RawQuery == reqQuery {
				fmt.Fprintln(w, response)
			}
		}))
		defer ts.Close()

		j, err := NewClient(twitchclient.ClientConf{Host: ts.URL})
		if err != nil {
			t.Fatal(err)
		}

		reqOpts := twitchclient.ReqOpts{
			StatName:       "service.janus.get_game_details",
			StatSampleRate: defaultStatSampleRate,
		}

		gameDetails, err := j.GetGameDetails(context.Background(), gameID, userID, &reqOpts)

		So(err, ShouldBeNil)
		So(gameDetails.Product.GameID, ShouldEqual, gameID)
		So(gameDetails.Product.Title, ShouldNotBeNil)

		So(err, ShouldBeNil)
		So(gameDetails.UserEntitlement.UserID, ShouldEqual, userID)
		So(gameDetails.UserEntitlement.Entitled, ShouldNotBeNil)
	})
}

// Test the GetGameCommerceRevenue API
func TestGetExtensionsRevenue(t *testing.T) {
	startTime := "2017-01-15"
	endTime := "2017-01-17"
	Convey("when calling GetGameCommerceRevenue", t, func() {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			reqPath := fmt.Sprintf("/broadcaster/%s/extensions/revenue", userID)
			reqQuery := fmt.Sprintf("end_time=%s&start_time=%s", endTime, startTime)
			if r.Method == "GET" && r.URL.Path == reqPath && r.URL.RawQuery == reqQuery {
				fmt.Fprintln(w, getExtensionsProductsResponse)
			}
		}))
		defer ts.Close()

		j, err := NewClient(twitchclient.ClientConf{Host: ts.URL})
		if err != nil {
			t.Fatal(err)
		}

		reqOpts := twitchclient.ReqOpts{
			StatName:       "service.janus.get_extensions_revenue",
			StatSampleRate: defaultStatSampleRate,
		}

		revenueRecords, err := j.GetExtensionsRevenue(context.Background(), userID, startTime, endTime, &reqOpts)

		So(err, ShouldBeNil)
		for _, revenueRecord := range revenueRecords.Records {
			So(revenueRecord.RevenueCents, ShouldEqual, 100.9)
		}
	})
}

// Test the GetGameCommerceRevenue API
func TestGetGameCommerceRevenue(t *testing.T) {
	startTime := "2017-01-15"
	endTime := "2017-01-17"
	Convey("when calling GetGameCommerceRevenue", t, func() {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			reqPath := fmt.Sprintf("/broadcaster/%s/gamecommerce/revenue", userID)
			reqQuery := fmt.Sprintf("end_time=%s&start_time=%s", endTime, startTime)
			if r.Method == "GET" && r.URL.Path == reqPath && r.URL.RawQuery == reqQuery {
				fmt.Fprintln(w, getGameCommerceRevenueResponse)
			}
		}))
		defer ts.Close()

		j, err := NewClient(twitchclient.ClientConf{Host: ts.URL})
		if err != nil {
			t.Fatal(err)
		}

		reqOpts := twitchclient.ReqOpts{
			StatName:       "service.janus.get_fuel_revenue",
			StatSampleRate: defaultStatSampleRate,
		}

		revenueRecords, err := j.GetGameCommerceRevenue(context.Background(), userID, startTime, endTime, &reqOpts)

		So(err, ShouldBeNil)
		for _, revenueRecord := range revenueRecords.Records {
			So(revenueRecord.RevenueCents, ShouldEqual, 100.9)
		}
	})
}

// Test the GetGameSettings API
func TestGetGameSettings(t *testing.T) {
	Convey("when calling GetGameSettings", t, func() {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			reqPath := fmt.Sprintf("/games/%s/settings", gameID)
			reqQuery := fmt.Sprintf("userid=%s", userID)
			if r.Method == "GET" && r.URL.Path == reqPath && r.URL.RawQuery == reqQuery {
				fmt.Fprintln(w, getGameSettingsResponse)
			}
		}))
		defer ts.Close()

		j, err := NewClient(twitchclient.ClientConf{Host: ts.URL})
		if err != nil {
			t.Fatal(err)
		}

		reqOpts := twitchclient.ReqOpts{
			StatName:       "service.janus.get_game_settings",
			StatSampleRate: defaultStatSampleRate,
		}

		gameSettings, err := j.GetGameSettings(context.Background(), gameID, userID, &reqOpts)

		So(err, ShouldBeNil)
		So(gameSettings.DetailsEnabled, ShouldEqual, true)
	})
}

// Test the GetInGameContentDetails API
func TestGetInGameContentDetails(t *testing.T) {
	Convey("when calling GetInGameContentDetails", t, func() {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			reqPath := fmt.Sprintf("/games/%s/ingame", gameID)
			reqQuery := fmt.Sprintf("userid=%s", userID)
			if r.Method == "GET" && r.URL.Path == reqPath && r.URL.RawQuery == reqQuery {
				fmt.Fprintln(w, getInGameContentDetailsResponse)
			}
		}))
		defer ts.Close()

		j, err := NewClient(twitchclient.ClientConf{Host: ts.URL})
		if err != nil {
			t.Fatal(err)
		}

		reqOpts := twitchclient.ReqOpts{
			StatName:       "service.janus.get_in_game_content_details",
			StatSampleRate: defaultStatSampleRate,
		}

		inGameContentDetails, err := j.GetInGameContentDetails(context.Background(), gameID, userID, &reqOpts)

		So(err, ShouldBeNil)
		So(len(inGameContentDetails.InGameContent), ShouldEqual, 2)
		for _, inGameContent := range inGameContentDetails.InGameContent {
			So(inGameContent.ActionDetails, ShouldNotBeNil)
			So(inGameContent.ActionDetails.DestinationURL, ShouldNotBeEmpty)

			So(inGameContent.ContentDetails, ShouldNotBeNil)
			So(inGameContent.ContentDetails.ASIN, ShouldNotBeEmpty)
			So(inGameContent.ContentDetails.Title, ShouldNotBeEmpty)

			So(inGameContent.UserEntitlement, ShouldNotBeNil)
			So(inGameContent.UserEntitlement.UserID, ShouldEqual, userID)
			So(inGameContent.UserEntitlement.Entitled, ShouldNotBeNil)
		}
	})
}

// Test the GetCrates API
func TestGetCrates(t *testing.T) {
	Convey("when calling GetCrates", t, func() {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			reqPath := fmt.Sprintf("/inventory/%s/crates", tuid)
			if r.Method == "GET" && r.URL.Path == reqPath {
				fmt.Fprintln(w, getCratesResponse)
			}
		}))
		defer ts.Close()

		j, err := NewClient(twitchclient.ClientConf{Host: ts.URL})
		if err != nil {
			t.Fatal(err)
		}

		reqOpts := twitchclient.ReqOpts{
			StatName:       "service.janus.get_crates",
			StatSampleRate: defaultStatSampleRate,
		}

		getCratesResponse, err := j.GetCrates(context.Background(), tuid, &reqOpts)

		So(err, ShouldBeNil)
		So(len(getCratesResponse.Crates), ShouldEqual, 2)
		for _, crateGood := range getCratesResponse.Crates {
			So(crateGood.CrateID, ShouldNotBeEmpty)
			So(crateGood.OpenCrateURL, ShouldNotBeEmpty)
			So(crateGood.ProductIconURL, ShouldNotBeEmpty)
			So(crateGood.ProductIconType, ShouldNotBeEmpty)
			So(crateGood.ProductTitle, ShouldNotBeEmpty)
		}
	})
}

// Test the GetEmoticons API
func TestGetEmoticons(t *testing.T) {
	Convey("when calling GetEmoticons", t, func() {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			reqPath := fmt.Sprintf("/inventory/%s/emoticons", tuid)
			if r.Method == "GET" && r.URL.Path == reqPath {
				fmt.Fprintln(w, getEmoticonsResponse)
			}
		}))
		defer ts.Close()

		j, err := NewClient(twitchclient.ClientConf{Host: ts.URL})
		if err != nil {
			t.Fatal(err)
		}

		reqOpts := twitchclient.ReqOpts{
			StatName:       "service.janus.get_emoticons",
			StatSampleRate: defaultStatSampleRate,
		}

		getEmoticonsResponse, err := j.GetEmoticons(context.Background(), tuid, &reqOpts)

		So(err, ShouldBeNil)
		So(len(getEmoticonsResponse.EmoticonSets), ShouldEqual, 2)
		for _, emoticonSet := range getEmoticonsResponse.EmoticonSets {
			for _, emoticon := range emoticonSet {
				So(emoticon.ID, ShouldNotBeEmpty)
				So(emoticon.Code, ShouldNotBeEmpty)
			}
		}
	})
}

// Test the GetBadges API
func TestGetBadges(t *testing.T) {
	Convey("when calling GetBadges", t, func() {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			reqPath := fmt.Sprintf("/inventory/%s/badges", tuid)
			if r.Method == "GET" && r.URL.Path == reqPath {
				fmt.Fprintln(w, getBadgesResponse)
			}
		}))
		defer ts.Close()

		j, err := NewClient(twitchclient.ClientConf{Host: ts.URL})
		if err != nil {
			t.Fatal(err)
		}

		reqOpts := twitchclient.ReqOpts{
			StatName:       "service.janus.get_badges",
			StatSampleRate: defaultStatSampleRate,
		}

		getBadgesResponse, err := j.GetBadges(context.Background(), tuid, &reqOpts)

		So(err, ShouldBeNil)
		So(len(getBadgesResponse.BadgeSets), ShouldEqual, 2)
		for _, badgeSet := range getBadgesResponse.BadgeSets {
			for _, badges := range badgeSet.Versions {
				So(badges.ImageURL1X, ShouldNotBeEmpty)
				So(badges.ImageURL2X, ShouldNotBeEmpty)
				So(badges.ImageURL4X, ShouldNotBeEmpty)
				So(badges.Description, ShouldNotBeEmpty)
				So(badges.Title, ShouldNotBeEmpty)
				So(badges.ClickAction, ShouldNotBeEmpty)
				So(badges.ClickURL, ShouldNotBeNil)
			}
		}
	})
}

// Test the GetActiveMarketplaces API
func TestGetActiveMarketplaces(t *testing.T) {
	Convey("when calling GetActiveMarketplaces", t, func() {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			reqPath := fmt.Sprintf("/inventory/%s/active_marketplaces", tuid)
			if r.Method == "GET" && r.URL.Path == reqPath {
				fmt.Fprintln(w, getActiveMarketplacesResponse)
			}
		}))
		defer ts.Close()

		j, err := NewClient(twitchclient.ClientConf{Host: ts.URL})
		if err != nil {
			t.Fatal(err)
		}

		reqOpts := twitchclient.ReqOpts{
			StatName:       "service.janus.get_active_marketplaces",
			StatSampleRate: defaultStatSampleRate,
		}

		getActiveMarketplacesResponse, err := j.GetActiveMarketplaces(context.Background(), tuid, &reqOpts)

		So(err, ShouldBeNil)
		So(len(getActiveMarketplacesResponse.ActiveMarketplaces), ShouldEqual, 2)
		for _, activeMarketplace := range getActiveMarketplacesResponse.ActiveMarketplaces {
			So(activeMarketplace.DisplayText, ShouldNotBeEmpty)
			So(activeMarketplace.OrderHistoryRedirectURL, ShouldNotBeEmpty)
		}
	})
}

// Test the GetMerchandise API
func TestGetMerchandise(t *testing.T) {
	Convey("when calling GetMerchandise", t, func() {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			reqPath := fmt.Sprintf("/merchandise")
			if r.Method == "GET" && r.URL.Path == reqPath {
				fmt.Fprintln(w, getMerchandiseResponse)
			}
		}))
		defer ts.Close()

		j, err := NewClient(twitchclient.ClientConf{Host: ts.URL})
		if err != nil {
			t.Fatal(err)
		}

		reqOpts := twitchclient.ReqOpts{
			StatName:       "service.janus.get_merchandise",
			StatSampleRate: defaultStatSampleRate,
		}

		merchandise, err := j.GetMerchandise(context.Background(), userID, locale, &reqOpts)

		So(err, ShouldBeNil)
		So(len(merchandise.Products), ShouldEqual, 2)
		for _, product := range merchandise.Products {
			So(product.Title, ShouldNotBeEmpty)
			So(product.DetailPageURL, ShouldNotBeEmpty)

			So(product.Price, ShouldNotBeNil)
			So(product.Price.Price, ShouldNotBeEmpty)
			So(product.Price.CurrencyUnit, ShouldNotBeEmpty)

			So(product.OriginalPrice, ShouldNotBeNil)
			So(product.OriginalPrice.Price, ShouldNotBeEmpty)
			So(product.OriginalPrice.CurrencyUnit, ShouldNotBeEmpty)

			So(product.Media, ShouldNotBeNil)
			for _, image := range product.Media.Images {
				So(image.ImageURL, ShouldNotBeEmpty)
				So(image.Height, ShouldNotBeNil)
				So(image.Width, ShouldNotBeNil)
			}

			So(product.IsOutOfStock, ShouldBeFalse)
			So(product.IsNew, ShouldBeFalse)
			So(product.IsFeatured, ShouldBeFalse)
		}
	})
}

// Test the GetExtensionsProducts API

func TestGetExtensionsProducts(t *testing.T) {
	Convey("when calling GetExtensionsProducts", t, func() {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			reqPath := fmt.Sprintf("/extensions/products")
			if r.Method == "GET" && r.URL.Path == reqPath {
				fmt.Fprintln(w, getExtensionsProductsResponse)
			}
		}))
		defer ts.Close()

		j, err := NewClient(twitchclient.ClientConf{Host: ts.URL})
		if err != nil {
			t.Fatal(err)
		}

		extensionsProducts, err := j.GetExtensionsProducts(context.Background(), vendorCode, sku, userID, language, nil)

		So(err, ShouldBeNil)
		So(extensionsProducts.VendorCode, ShouldNotBeEmpty)
		So(len(extensionsProducts.Products), ShouldEqual, 2)
		for _, product := range extensionsProducts.Products {
			So(product.ActionDetails, ShouldNotBeNil)
			So(product.ActionDetails.DestinationURL, ShouldNotBeEmpty)
			So(product.ASIN, ShouldNotBeEmpty)
			So(product.Price, ShouldNotBeNil)
			So(product.Price.Price, ShouldNotBeEmpty)
			So(product.Price.CurrencyUnit, ShouldNotBeEmpty)
			So(product.SKU, ShouldNotBeEmpty)
		}
	})
}

// Test the GetOffers API
func TestGetOffers(t *testing.T) {
	Convey("when calling GetOffers", t, func() {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			reqPath := fmt.Sprintf("/offers")
			reqQuery := fmt.Sprintf("broadcaster_id=%s&game_id=%s&viewer_country_code=%s", userID, gameID, viewerCountryCode)
			if r.Method == "GET" && r.URL.Path == reqPath && r.URL.RawQuery == reqQuery {
				fmt.Fprintln(w, getOffersResponse)
			}
		}))
		defer ts.Close()

		j, err := NewClient(twitchclient.ClientConf{Host: ts.URL})
		if err != nil {
			t.Fatal(err)
		}

		reqOpts := twitchclient.ReqOpts{
			StatName:       "service.janus.get_offers",
			StatSampleRate: defaultStatSampleRate,
		}

		retailProducts, err := j.GetOffers(context.Background(), gameID, userID, viewerCountryCode, &reqOpts)

		So(err, ShouldBeNil)
		So(len(retailProducts.RetailProducts), ShouldEqual, 1)
		for _, product := range retailProducts.RetailProducts {
			So(product.Title, ShouldNotBeEmpty)
			So(product.DetailPageURL, ShouldNotBeEmpty)

			So(product.Price, ShouldNotBeNil)
			So(product.Price.Price, ShouldNotBeEmpty)
			So(product.Price.CurrencyUnit, ShouldNotBeEmpty)

			So(product.Media, ShouldNotBeNil)
			So(product.Media.BoxArtURL, ShouldNotBeNil)
			So(product.Crates, ShouldNotBeNil)
			So(len(product.Crates), ShouldEqual, 1)
		}
	})
}

// Test the GetOffersSettings API
func TestGetOffersSettings(t *testing.T) {
	Convey("when calling GetOffersSettings", t, func() {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			reqPath := fmt.Sprintf("/offers/settings")
			reqQuery := fmt.Sprintf("broadcaster_id=%s&game_id=%s&viewer_country_code=%s", userID, gameID, viewerCountryCode)
			if r.Method == "GET" && r.URL.Path == reqPath && r.URL.RawQuery == reqQuery {
				fmt.Fprintln(w, getOffersSettingsResponse)
			}
		}))
		defer ts.Close()

		j, err := NewClient(twitchclient.ClientConf{Host: ts.URL})
		if err != nil {
			t.Fatal(err)
		}

		reqOpts := twitchclient.ReqOpts{
			StatName:       "service.janus.get_offer_settings",
			StatSampleRate: defaultStatSampleRate,
		}

		offersSettings, err := j.GetOffersSettings(context.Background(), gameID, userID, viewerCountryCode, &reqOpts)

		So(err, ShouldBeNil)
		So(offersSettings, ShouldNotBeNil)
		So(offersSettings.AmazonRetailEnabled, ShouldBeTrue)
	})
}

// Test the GetBroadcasterPayoutDetails API
func TestBroadcasterPayoutDetails(t *testing.T) {
	Convey("when calling GetBroadcasterPayoutDetails", t, func() {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			reqPath := fmt.Sprintf("/broadcaster/%s/payout", tuid)
			if r.Method == "GET" && r.URL.Path == reqPath {
				fmt.Fprintln(w, getBroadcasterPayoutDetailsResponse)
			}
		}))
		defer ts.Close()

		j, err := NewClient(twitchclient.ClientConf{Host: ts.URL})
		if err != nil {
			t.Fatal(err)
		}

		reqOpts := twitchclient.ReqOpts{
			StatName:       "service.janus.get_broadcaster_payout_details",
			StatSampleRate: defaultStatSampleRate,
		}

		payoutDetails, err := j.GetBroadcasterPayoutDetails(context.Background(), tuid, &reqOpts)

		So(err, ShouldBeNil)
		So(payoutDetails, ShouldNotBeNil)
		So(payoutDetails.IsEligible, ShouldBeTrue)
	})
}

// Test the GetBroadcasterRetailPayoutDetails API
func TestBroadcasterRetailPayoutDetails(t *testing.T) {
	Convey("when calling GetBroadcasterRetailPayoutDetails", t, func() {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			reqPath := fmt.Sprintf("/broadcaster/%s/payout/retail", tuid)
			if r.Method == "GET" && r.URL.Path == reqPath {
				fmt.Fprintln(w, getBroadcasterRetailPayoutDetailsResponse)
			}
		}))
		defer ts.Close()

		j, err := NewClient(twitchclient.ClientConf{Host: ts.URL})
		if err != nil {
			t.Fatal(err)
		}

		reqOpts := twitchclient.ReqOpts{
			StatName:       "service.janus.get_broadcaster_retail_payout_details",
			StatSampleRate: defaultStatSampleRate,
		}

		payoutDetails, err := j.GetBroadcasterRetailPayoutDetails(context.Background(), tuid, &reqOpts)

		So(err, ShouldBeNil)
		So(payoutDetails, ShouldNotBeNil)
		So(payoutDetails.IsEligible, ShouldBeTrue)
	})
}

// Test the GetAssociatesStore API
func TestGetAssociatesStore(t *testing.T) {
	Convey("when calling GetAssociatesStore", t, func() {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			reqPath := fmt.Sprintf("/broadcaster/%s/associates/store", tuid)
			if r.Method == "GET" && r.URL.Path == reqPath {
				fmt.Fprintln(w, getAssociatesStoreResponse)
			}
		}))
		defer ts.Close()

		j, err := NewClient(twitchclient.ClientConf{Host: ts.URL})
		if err != nil {
			t.Fatal(err)
		}

		reqOpts := twitchclient.ReqOpts{
			StatName:       "service.janus.get_linked_associates_store",
			StatSampleRate: defaultStatSampleRate,
		}

		linkedStore, err := j.GetAssociatesStore(context.Background(), tuid, &reqOpts)

		So(err, ShouldBeNil)
		So(linkedStore.StoreID, ShouldNotBeNil)
		So(linkedStore.IsPayoutEnabled, ShouldBeTrue)
	})
}
