// Package janus contains a client for the Janus service for use by Visage and other internal twitch services.
// The package also contains shared models for response objects.
package janus

import (
	"net/url"

	"bytes"
	"encoding/json"

	"code.justin.tv/foundation/twitchclient"
	"golang.org/x/net/context"
)

const (
	defaultStatSampleRate = 0.1
	defaultTimingXactName = "janus"

	// ViewAllDetailsCapabilityKey is the identifier of the Cartman capability used in this service
	ViewAllDetailsCapabilityKey = "janus::view_all_details"
	// ViewAmendmentAcceptanceCapabilityKey is the identifier of the view_amendment_acceptance Cartman capability used in this service
	ViewAmendmentAcceptanceCapabilityKey = "janus::view_amendment_acceptance"
	// SetAmendmentAcceptanceCapabilityKey is the identifier of the set_amendment_acceptance Cartman capability used in this service
	SetAmendmentAcceptanceCapabilityKey = "janus::set_amendment_acceptance"
	// GetRevenueDetailsCapabilityKey is the identifier of the get_revenue_details Cartman capability used in this service
	GetRevenueDetailsCapabilityKey = "janus::get_revenue_details"
)

// ErrorResponse aggregates information related to an error response
type ErrorResponse struct {
	Status  int
	Message string
	Error   string
}

// Client provides an interface for the service client
type Client interface {
	GetGameDetails(ctx context.Context, gameID string, userID string, reqOpts *twitchclient.ReqOpts) (*GetGameDetailsResponse, error)
	GetInGameContentDetails(ctx context.Context, gameID string, userID string, reqOpts *twitchclient.ReqOpts) (*GetInGameContentDetailsResponse, error)
	GetGameSettings(ctx context.Context, gameID string, userID string, reqOpts *twitchclient.ReqOpts) (*GetGameSettingsResponse, error)
	GetAmendmentAcceptance(ctx context.Context, tuid string, amendmentType string, reqOpts *twitchclient.ReqOpts) (*GetAmendmentAcceptanceResponse, error)
	SetAmendmentAcceptance(ctx context.Context, tuid string, amendmentType string, reqOpts *twitchclient.ReqOpts) (*SetAmendmentAcceptanceResponse, error)
	GetDeprecatedAmendmentAcceptance(ctx context.Context, tuid string, reqOpts *twitchclient.ReqOpts) (*GetAmendmentAcceptanceResponse, error)
	SetDeprecatedAmendmentAcceptance(ctx context.Context, tuid string, reqOpts *twitchclient.ReqOpts) (*SetAmendmentAcceptanceResponse, error)
	GetBroadcasterPayoutDetails(ctx context.Context, tuid string, reqOpts *twitchclient.ReqOpts) (*GetBroadcasterPayoutDetailsResponse, error)
	GetBroadcasterRetailPayoutDetails(ctx context.Context, tuid string, reqOpts *twitchclient.ReqOpts) (*GetBroadcasterRetailPayoutDetailsResponse, error)
	GetExtensionsRevenue(ctx context.Context, tuid string, startTime string, endTime string, reqOpts *twitchclient.ReqOpts) (*GetExtensionsRevenueResponse, error)
	GetGameCommerceRevenue(ctx context.Context, tuid string, startTime string, endTime string, reqOpts *twitchclient.ReqOpts) (*GetGameCommerceRevenueResponse, error)
	GetCrates(ctx context.Context, tuid string, reqOpts *twitchclient.ReqOpts) (*GetCratesResponse, error)
	GetChatNotifications(ctx context.Context, channelID string, userID string, reqOpts *twitchclient.ReqOpts) (*GetChatNotificationsResponse, error)
	ConsumeChatNotification(ctx context.Context, channelID string, userID string, request ConsumeChatNotificationsRequest, reqOpts *twitchclient.ReqOpts) error
	GetEmoticons(ctx context.Context, tuid string, reqOpts *twitchclient.ReqOpts) (*GetEmoticonsResponse, error)
	GetBadges(ctx context.Context, tuid string, reqOpts *twitchclient.ReqOpts) (*GetBadgesResponse, error)
	GetActiveMarketplaces(ctx context.Context, tuid string, reqOpts *twitchclient.ReqOpts) (*GetActiveMarketplacesResponse, error)
	GetMerchandise(ctx context.Context, userID string, locale string, reqOpts *twitchclient.ReqOpts) (*GetMerchandiseResponse, error)
	GetExtensionsProducts(ctx context.Context, vendorCode string, sku string, userID string, language string, reqpOpts *twitchclient.ReqOpts) (*GetExtensionsProductsResponse, error)
	GetOffers(ctx context.Context, gameID string, broadcasterID string, viewerCountryCode string, reqOpts *twitchclient.ReqOpts) (*GetOffersResponse, error)
	GetOffersSettings(ctx context.Context, gameID string, broadcasterID string, viewerCountryCode string, reqOpts *twitchclient.ReqOpts) (*GetOffersSettingsResponse, error)
	GetAssociatesStore(ctx context.Context, broadcasterID string, reqOpts *twitchclient.ReqOpts) (*GetAssociatesStoreResponse, error)
}

type client struct {
	twitchclient.Client
}

// NewClient creates a new client for use in making service calls.
func NewClient(conf twitchclient.ClientConf) (Client, error) {
	if conf.TimingXactName == "" {
		conf.TimingXactName = defaultTimingXactName
	}
	twitchClient, err := twitchclient.NewClient(conf)
	return &client{twitchClient}, err
}

func (c *client) GetGameDetails(ctx context.Context, gameID string, userID string, reqOpts *twitchclient.ReqOpts) (*GetGameDetailsResponse, error) {
	query := url.Values{}
	query.Set("userid", userID)
	u := &url.URL{Path: "/games/" + gameID + "/details", RawQuery: query.Encode()}

	req, err := c.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.get_game_details",
		StatSampleRate: defaultStatSampleRate,
	})

	var getGameDetailsResponse GetGameDetailsResponse
	_, err = c.DoJSON(ctx, &getGameDetailsResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getGameDetailsResponse, nil
}

func (c *client) GetExtensionsRevenue(ctx context.Context, tuid string, startTime string, endTime string, reqOpts *twitchclient.ReqOpts) (*GetExtensionsRevenueResponse, error) {
	query := url.Values{}
	query.Set("start_time", startTime)
	query.Set("end_time", endTime)
	u := &url.URL{Path: "/broadcaster/" + tuid + "/extensions/revenue", RawQuery: query.Encode()}

	req, err := c.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.get_extensions_revenue",
		StatSampleRate: defaultStatSampleRate,
	})

	var getExtensionsRevenueResponse GetExtensionsRevenueResponse
	_, err = c.DoJSON(ctx, &getExtensionsRevenueResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getExtensionsRevenueResponse, nil
}

func (c *client) GetGameCommerceRevenue(ctx context.Context, tuid string, startTime string, endTime string, reqOpts *twitchclient.ReqOpts) (*GetGameCommerceRevenueResponse, error) {
	query := url.Values{}
	query.Set("start_time", startTime)
	query.Set("end_time", endTime)
	u := &url.URL{Path: "/broadcaster/" + tuid + "/gamecommerce/revenue", RawQuery: query.Encode()}

	req, err := c.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.get_gamecommerce_revenue",
		StatSampleRate: defaultStatSampleRate,
	})

	var getGameCommerceRevenueResponse GetGameCommerceRevenueResponse
	_, err = c.DoJSON(ctx, &getGameCommerceRevenueResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getGameCommerceRevenueResponse, nil
}

func (c *client) GetInGameContentDetails(ctx context.Context, gameID string, userID string, reqOpts *twitchclient.ReqOpts) (*GetInGameContentDetailsResponse, error) {
	query := url.Values{}
	query.Set("userid", userID)

	u := &url.URL{Path: "/games/" + gameID + "/ingame", RawQuery: query.Encode()}

	req, err := c.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.get_in_game_content_details",
		StatSampleRate: defaultStatSampleRate,
	})

	var getInGameContentDetailsResponse GetInGameContentDetailsResponse
	_, err = c.DoJSON(ctx, &getInGameContentDetailsResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getInGameContentDetailsResponse, nil
}

func (c *client) GetGameSettings(ctx context.Context, gameID string, userID string, reqOpts *twitchclient.ReqOpts) (*GetGameSettingsResponse, error) {
	query := url.Values{}
	query.Set("userid", userID)
	u := &url.URL{Path: "/games/" + gameID + "/settings", RawQuery: query.Encode()}

	req, err := c.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.get_game_settings",
		StatSampleRate: defaultStatSampleRate,
	})

	var getGameSettingsResponse GetGameSettingsResponse
	_, err = c.DoJSON(ctx, &getGameSettingsResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getGameSettingsResponse, nil
}

func (c *client) GetAmendmentAcceptance(ctx context.Context, tuid string, amendmentType string, reqOpts *twitchclient.ReqOpts) (*GetAmendmentAcceptanceResponse, error) {
	u := &url.URL{Path: "/broadcaster/" + tuid + "/amendment/" + amendmentType}

	req, err := c.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.get_amendment_acceptance",
		StatSampleRate: defaultStatSampleRate,
	})

	var getAmendmentAcceptanceResponse GetAmendmentAcceptanceResponse
	_, err = c.DoJSON(ctx, &getAmendmentAcceptanceResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getAmendmentAcceptanceResponse, nil
}

func (c *client) SetAmendmentAcceptance(ctx context.Context, tuid string, amendmentType string, reqOpts *twitchclient.ReqOpts) (*SetAmendmentAcceptanceResponse, error) {
	u := &url.URL{Path: "/broadcaster/" + tuid + "/amendment/" + amendmentType}

	req, err := c.NewRequest("PUT", u.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.set_amendment_acceptance",
		StatSampleRate: defaultStatSampleRate,
	})

	var setAmendmentAcceptanceRequest SetAmendmentAcceptanceResponse
	_, err = c.DoJSON(ctx, &setAmendmentAcceptanceRequest, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &setAmendmentAcceptanceRequest, nil
}

func (c *client) GetDeprecatedAmendmentAcceptance(ctx context.Context, tuid string, reqOpts *twitchclient.ReqOpts) (*GetAmendmentAcceptanceResponse, error) {
	u := &url.URL{Path: "/games/amendment/" + tuid}

	req, err := c.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.get_deprecated_amendment_acceptance",
		StatSampleRate: defaultStatSampleRate,
	})

	var getAmendmentAcceptanceResponse GetAmendmentAcceptanceResponse
	_, err = c.DoJSON(ctx, &getAmendmentAcceptanceResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getAmendmentAcceptanceResponse, nil
}

func (c *client) SetDeprecatedAmendmentAcceptance(ctx context.Context, tuid string, reqOpts *twitchclient.ReqOpts) (*SetAmendmentAcceptanceResponse, error) {
	u := &url.URL{Path: "/games/amendment/" + tuid}

	req, err := c.NewRequest("PUT", u.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.set_deprecated_amendment_acceptance",
		StatSampleRate: defaultStatSampleRate,
	})

	var setAmendmentAcceptanceRequest SetAmendmentAcceptanceResponse
	_, err = c.DoJSON(ctx, &setAmendmentAcceptanceRequest, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &setAmendmentAcceptanceRequest, nil
}

func (c *client) GetBroadcasterPayoutDetails(ctx context.Context, tuid string, reqOpts *twitchclient.ReqOpts) (*GetBroadcasterPayoutDetailsResponse, error) {
	u := &url.URL{Path: "/broadcaster/" + tuid + "/payout"}

	req, err := c.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.get_broadcaster_payout",
		StatSampleRate: defaultStatSampleRate,
	})

	var getBroadcasterPayoutDetailsResponse GetBroadcasterPayoutDetailsResponse
	_, err = c.DoJSON(ctx, &getBroadcasterPayoutDetailsResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getBroadcasterPayoutDetailsResponse, nil
}

func (c *client) GetBroadcasterRetailPayoutDetails(ctx context.Context, tuid string, reqOpts *twitchclient.ReqOpts) (*GetBroadcasterRetailPayoutDetailsResponse, error) {
	u := &url.URL{Path: "/broadcaster/" + tuid + "/payout/retail"}

	req, err := c.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.get_broadcaster_payout",
		StatSampleRate: defaultStatSampleRate,
	})

	var getBroadcasterRetailPayoutDetailsResponse GetBroadcasterRetailPayoutDetailsResponse
	_, err = c.DoJSON(ctx, &getBroadcasterRetailPayoutDetailsResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getBroadcasterRetailPayoutDetailsResponse, nil
}

func (c *client) GetCrates(ctx context.Context, tuid string, reqOpts *twitchclient.ReqOpts) (*GetCratesResponse, error) {
	u := &url.URL{Path: "/inventory/" + tuid + "/crates"}

	req, err := c.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.get_crates",
		StatSampleRate: defaultStatSampleRate,
	})

	var getCratesResponse GetCratesResponse
	_, err = c.DoJSON(ctx, &getCratesResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getCratesResponse, nil
}

func (c *client) GetEmoticons(ctx context.Context, tuid string, reqOpts *twitchclient.ReqOpts) (*GetEmoticonsResponse, error) {
	u := &url.URL{Path: "/inventory/" + tuid + "/emoticons"}

	req, err := c.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.get_emoticons",
		StatSampleRate: defaultStatSampleRate,
	})

	var getEmoticonsResponse GetEmoticonsResponse
	_, err = c.DoJSON(ctx, &getEmoticonsResponse, req, combinedReqOpts)

	if err != nil {
		return nil, err
	}

	return &getEmoticonsResponse, nil
}

func (c *client) GetBadges(ctx context.Context, tuid string, reqOpts *twitchclient.ReqOpts) (*GetBadgesResponse, error) {
	u := &url.URL{Path: "/inventory/" + tuid + "/badges"}

	req, err := c.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.get_badges",
		StatSampleRate: defaultStatSampleRate,
	})

	var getBadgesResponse GetBadgesResponse
	_, err = c.DoJSON(ctx, &getBadgesResponse, req, combinedReqOpts)

	if err != nil {
		return nil, err
	}

	return &getBadgesResponse, nil
}

func (c *client) GetChatNotifications(ctx context.Context, channelID string, userID string, reqOpts *twitchclient.ReqOpts) (*GetChatNotificationsResponse, error) {
	query := url.Values{}
	query.Set("channelID", channelID)
	u := &url.URL{Path: "/users/" + userID + "/notifications", RawQuery: query.Encode()}

	req, err := c.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.get_chat_notifications",
		StatSampleRate: defaultStatSampleRate,
	})

	var getChatNotificationsResponse GetChatNotificationsResponse
	_, err = c.DoJSON(ctx, &getChatNotificationsResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getChatNotificationsResponse, nil
}

func (c *client) ConsumeChatNotification(ctx context.Context, channelID string, userID string, request ConsumeChatNotificationsRequest, reqOpts *twitchclient.ReqOpts) (err error) {
	query := url.Values{}
	query.Set("channelID", channelID)
	u := &url.URL{Path: "/users/" + userID + "/notifications", RawQuery: query.Encode()}

	requestJSON, err := json.Marshal(request)
	if err != nil {
		return err
	}

	requestJSONReader := bytes.NewReader(requestJSON)
	req, err := c.NewRequest("POST", u.String(), requestJSONReader)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.consume_chat_notification",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer func() {
		if cErr := resp.Body.Close(); cErr != nil && err == nil {
			err = cErr
		}
	}()

	if resp.StatusCode != 200 {
		return twitchclient.HandleFailedResponse(resp)
	}
	return nil
}

func (c *client) GetActiveMarketplaces(ctx context.Context, tuid string, reqOpts *twitchclient.ReqOpts) (*GetActiveMarketplacesResponse, error) {
	u := &url.URL{Path: "/inventory/" + tuid + "/active_marketplaces"}

	req, err := c.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.get_active_marketplaces",
		StatSampleRate: defaultStatSampleRate,
	})

	var getActiveMarketplacesResponse GetActiveMarketplacesResponse
	_, err = c.DoJSON(ctx, &getActiveMarketplacesResponse, req, combinedReqOpts)

	if err != nil {
		return nil, err
	}

	return &getActiveMarketplacesResponse, nil
}

func (c *client) GetMerchandise(ctx context.Context, userID string, locale string, reqOpts *twitchclient.ReqOpts) (*GetMerchandiseResponse, error) {
	query := url.Values{}
	query.Set("userid", userID)
	query.Set("locale", locale)
	u := &url.URL{Path: "/merchandise", RawQuery: query.Encode()}

	req, err := c.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.get_merchandise",
		StatSampleRate: defaultStatSampleRate,
	})

	var getMerchandiseResponse GetMerchandiseResponse
	_, err = c.DoJSON(ctx, &getMerchandiseResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getMerchandiseResponse, nil
}

func (c *client) GetExtensionsProducts(ctx context.Context, vendorCode, sku, userID, language string, reqOpts *twitchclient.ReqOpts) (*GetExtensionsProductsResponse, error) {
	query := url.Values{}
	query.Set("vendor_code", vendorCode)
	query.Set("sku", sku)
	query.Set("userid", userID)
	query.Set("language", language)
	u := &url.URL{Path: "/extensions/products", RawQuery: query.Encode()}

	req, err := c.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.get_extensions_products",
		StatSampleRate: defaultStatSampleRate,
	})

	var getExtensionsProductsResponse GetExtensionsProductsResponse
	_, err = c.DoJSON(ctx, &getExtensionsProductsResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getExtensionsProductsResponse, nil
}

func (c *client) GetOffers(ctx context.Context, gameID string, broadcasterID string, viewerCountryCode string, reqOpts *twitchclient.ReqOpts) (*GetOffersResponse, error) {
	query := url.Values{}
	query.Set("game_id", gameID)
	query.Set("broadcaster_id", broadcasterID)
	query.Set("viewer_country_code", viewerCountryCode)
	u := &url.URL{Path: "/offers", RawQuery: query.Encode()}

	req, err := c.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.get_offers",
		StatSampleRate: defaultStatSampleRate,
	})

	var getOffersResponse GetOffersResponse
	_, err = c.DoJSON(ctx, &getOffersResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getOffersResponse, nil
}

func (c *client) GetOffersSettings(ctx context.Context, gameID string, broadcasterID string, viewerCountryCode string, reqOpts *twitchclient.ReqOpts) (*GetOffersSettingsResponse, error) {
	query := url.Values{}
	query.Set("game_id", gameID)
	query.Set("broadcaster_id", broadcasterID)
	query.Set("viewer_country_code", viewerCountryCode)
	u := &url.URL{Path: "/offers/settings", RawQuery: query.Encode()}

	req, err := c.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.get_offer_settings",
		StatSampleRate: defaultStatSampleRate,
	})

	var getOffersSettingsResponse GetOffersSettingsResponse
	_, err = c.DoJSON(ctx, &getOffersSettingsResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getOffersSettingsResponse, nil
}

func (c *client) GetAssociatesStore(ctx context.Context, broadcasterID string, reqOpts *twitchclient.ReqOpts) (*GetAssociatesStoreResponse, error) {
	u := &url.URL{Path: "/broadcaster/" + broadcasterID + "/associates/store"}

	req, err := c.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.janus.get_linked_associates_store",
		StatSampleRate: defaultStatSampleRate,
	})

	var getAssociatesStoreResponse GetAssociatesStoreResponse
	_, err = c.DoJSON(ctx, &getAssociatesStoreResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getAssociatesStoreResponse, nil
}
