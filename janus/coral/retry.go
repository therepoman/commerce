package coral

import (
	"errors"
	"fmt"
	"reflect"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
)

// RetryStrategy structure containing information for retry logic
type RetryStrategy struct {
	NumAttempts         int
	RetryDelay          time.Duration
	RetryableExceptions []string
}

// NewDefaultRetryStrategy creates a new strategy with default logic
func NewDefaultRetryStrategy() RetryStrategy {
	return RetryStrategy{
		NumAttempts: 3,
		RetryDelay:  time.Millisecond * 100,
		RetryableExceptions: []string{
			HTTPFailureException,
			DependencyException,
			HttpTimeoutException,
			EOFException,
			UnexpectedEOFException,
			ConnectionTimedOut,
			ConnectionReset,
			SWSError,
		},
	}
}

// NewTestingRetryStrategy creates a new strategy with no retry logic
func NewTestingRetryStrategy() RetryStrategy {
	return RetryStrategy{
		NumAttempts:         1,
		RetryDelay:          time.Millisecond * 0,
		RetryableExceptions: []string{},
	}
}

const (
	// HTTPFailureException is thrown by coral clients
	HTTPFailureException string = "HttpFailureException"

	// DependencyException is thrown by coral clients
	DependencyException string = "DependencyException"

	// HttpTimeoutException is thrown by coral clients
	HttpTimeoutException string = "HttpTimeoutException"

	// EOFException is thrown by SWS
	EOFException string = "EOF"

	// UnexpectedEOFException is thrown by SWS
	UnexpectedEOFException string = "unexpected EOF"

	// ConnectionTimedOut is thrown by TCP
	ConnectionTimedOut string = "connection timed out"

	// ConnectionReset is thrown by TCP
	ConnectionReset string = "connection reset by peer"

	// I/O timeout errors thrown by SWS
	SWSError string = "dial tcp: lookup sws-na.amazon.com"
)

// ExecuteCoralCall will perform the given coralCall with retry logic and put the output in the destination
func (r RetryStrategy) ExecuteCoralCall(coralCall func() (interface{}, error), destination interface{}) error {
	destinationValue := reflect.ValueOf(destination)
	if destinationValue.Kind() != reflect.Ptr {
		msg := "Destination kind must be a pointer"
		log.Errorf(msg)
		return errors.New(msg)
	}

	destinationValueElem := destinationValue.Elem()
	if destinationValueElem.Kind() != reflect.Interface {
		msg := "Destination pointer's underlying kind must be an interface"
		log.Errorf(msg)
		return errors.New(msg)
	}

	var coralResponse interface{}
	var err error
	var actualAttempts int
	for attempt := 0; attempt < r.NumAttempts; attempt++ {
		actualAttempts++
		coralResponse, err = coralCall()
		if err == nil {
			break
		}

		if r.IsRetryableError(err) {
			log.Infof("Encountered retryable error during coral call: %v", err)
			time.Sleep(r.RetryDelay)
		} else {
			log.Warnf("Encountered non-retryable error during coral call: %v", err)
			break
		}
	}

	if err != nil {
		log.WithFields(log.Fields{
			"Attempts": actualAttempts,
			"error":    err,
		}).Error("Failed to make coral call")
		msg := fmt.Sprintf("Failed making coral call after %d attempt(s). Underlying error: %v", actualAttempts, err)
		return errors.New(msg)
	}

	coralResponseValue := reflect.ValueOf(coralResponse)
	if !coralResponseValue.Type().Implements(destinationValueElem.Type()) {
		msg := fmt.Sprintf("Coral response type must implement destination interface. Coral response type: %s , Destination interfact: %s", coralResponseValue.Type().String(), destinationValueElem.Type().String())
		log.Errorf(msg)
		return errors.New(msg)
	}

	if !destinationValueElem.CanSet() {
		msg := "Destination interface cannot be set"
		log.Errorf(msg)
		return errors.New(msg)
	}

	destinationValueElem.Set(coralResponseValue)
	return nil
}

// IsRetryableError checks if the given error is in the list of retryable errors for this strategy
func (r RetryStrategy) IsRetryableError(err error) bool {
	errorName := fmt.Sprintf("%s", err)
	for _, retryableException := range r.RetryableExceptions {
		if strings.Contains(errorName, retryableException) {
			return true
		}
	}
	return false
}
