package coral

import (
	"code.justin.tv/commerce/CoralGoClient/src/coral/dialer"
	"code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	"code.justin.tv/commerce/CoralRPCGoSupport/src/coral/rpcv1"

	"crypto/tls"
	"crypto/x509"

	log "github.com/sirupsen/logrus"
)

// SWSParams : Parameters for SWS.
type SWSParams struct {
	SWSEnabled      bool
	RootCertStore   []byte
	ClientCertChain []byte
	HostAddress     string
	HostPort        uint16
}

// InitCodecAndDialer : Initialize the code and dialer to make calls to SWS
func InitCodecAndDialer(params SWSParams) (codec.RoundTripper, dialer.Dialer, error) {
	host := params.HostAddress
	codec := rpcv1.New(host)

	// Load client cert and private key
	clientCert, err := tls.X509KeyPair(params.ClientCertChain, params.ClientCertChain)
	if err != nil {
		log.Warningf("Unable to load SWS certificate or key from PEM file: %v\n", err)
		return nil, nil, err
	}

	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(params.RootCertStore)

	tlsconfig := &tls.Config{
		RootCAs:            caCertPool,
		Certificates:       []tls.Certificate{clientCert},
		InsecureSkipVerify: false,
	}

	dialer, err := dialer.TLS(host, params.HostPort, tlsconfig)
	if err != nil {
		log.Warningf("Unable to establish connection to the host: %v\n", err)
		return nil, nil, err
	}

	return codec, dialer, nil
}
