package decorators

import (
	"net/http"

	"code.justin.tv/commerce/janus/api/apidef"
	"goji.io"
	"golang.org/x/net/context"
)

// RequestHandler interface
type RequestHandler interface {
	Handle(context.Context, http.ResponseWriter, *http.Request)
}

// RequestHandlerAdapter allows us to pass the API name (e.g. "getGameDetails") to Goji to be referenced later along
// with the handler itself.
type RequestHandlerAdapter struct {
	handler     RequestHandler
	handlerName apidef.HandlerName
}

// ServeHTTPC wraps the handler's business logic. This is called by Goji.
func (a RequestHandlerAdapter) ServeHTTPC(c context.Context, rw http.ResponseWriter, r *http.Request) {
	a.handler.Handle(c, rw, r)
}

// GetHandlerName returns the name of the handler
func (a RequestHandlerAdapter) GetHandlerName() apidef.HandlerName {
	return a.handlerName
}

// NewRequestHandlingAdapter instantiates a new RequestHandlerAdapter
func NewRequestHandlingAdapter(rh RequestHandler, handlerName apidef.HandlerName) goji.Handler {
	return RequestHandlerAdapter{
		handler:     rh,
		handlerName: handlerName,
	}
}

// DecorateAndAdapt decorates and applies an adapter to the input API handler function
func DecorateAndAdapt(funct func(context.Context, http.ResponseWriter, *http.Request), handlerName apidef.HandlerName) goji.Handler {
	rh := NewRequestLimitingDecorator(funct)
	return NewRequestHandlingAdapter(rh, handlerName)
}
