package decorators

import (
	"net/http"

	"golang.org/x/net/context"
)

// MaxBytesRequestSize represents the largest request size that we permit to be made to Janus. This is enforced in
// the RequestLimitingDecorator
const MaxBytesRequestSize int64 = 1024 * 1024

// RequestLimitingDecorator handles any errors that bubble out of the API handler
type RequestLimitingDecorator struct {
	handlerFunction func(context.Context, http.ResponseWriter, *http.Request)
}

// NewRequestLimitingDecorator instantiates a new RequestLimitingDecorator
func NewRequestLimitingDecorator(funct func(context.Context, http.ResponseWriter, *http.Request)) RequestHandler {
	return RequestLimitingDecorator{funct}
}

// Handle encompasses the business logic of this decorator. The RequestLimitingDecorator limits the request
// size of incoming requests
func (dec RequestLimitingDecorator) Handle(c context.Context, rw http.ResponseWriter, r *http.Request) {
	if r.Body != nil {
		r.Body = http.MaxBytesReader(rw, r.Body, MaxBytesRequestSize)
	}

	dec.handlerFunction(c, rw, r)
}
