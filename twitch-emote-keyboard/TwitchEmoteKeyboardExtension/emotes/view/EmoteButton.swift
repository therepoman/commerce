//
//  Emote.swift
//  TwitchEmoteKeyboardExtension
//
//  Created by Gregg, Jake on 7/19/18.
//  Copyright © 2018 Gregg, Jake. All rights reserved.
//

import UIKit

class EmoteButtonCell: UICollectionViewCell {
    var button: EmoteButton!
    
    func setButton(_ button: EmoteButton) {
        self.button = button
        self.addSubview(button)
    }
    
    func getCode() -> String {
        if button == nil {
            return "empty"
        }
        return button.code
    }
}

class EmoteButton: UIButton {
    //var defaultBackgroundColor: UIColor = .white
    //var highlightBackgroundColor: UIColor = .lightGray
    var image: UIImage!
    
    var code: String = ""
    var id: Int = 0
    
    init(code: String, id: Int) {
        super.init(frame: CGRect(x: 0, y: 0, width: 28, height: 28))
        commonInit()
        
        self.sizeToFit()
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addTarget(self, action: #selector(self.onClick), for: .touchUpInside)
        
        self.id = id
        self.code = code
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    @IBAction func onClick() {
        print("clicked " + self.code + " button")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //backgroundColor = isHighlighted ? highlightBackgroundColor : defaultBackgroundColor
    }
    
    func setEmoteImage(completion: @escaping () -> Void) {
        if self.image != nil {
            completion()
        }
        
        if let url = URL(string: String(format: "https://static-cdn.jtvnw.net/emoticons/v1/%d/2.0", self.id)) {
            self.downloadImage(url: url) { image in
                self.setBackgroundImage(image, for: .normal)
                completion()
            }
        }
    }
    
    func downloadImage(url: URL, completion: @escaping (UIImage) -> Void) {
        getDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async {
                self.image = UIImage(data: data)
                completion(self.image!)
            }
        }
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
        }.resume()
    }
}

private extension EmoteButton {
    func commonInit() {
        layer.cornerRadius = 5.0
        layer.masksToBounds = false
        layer.shadowOffset = CGSize(width: 0, height: 1.0)
        layer.shadowRadius = 0.0
        layer.shadowOpacity = 0.35
    }
}
