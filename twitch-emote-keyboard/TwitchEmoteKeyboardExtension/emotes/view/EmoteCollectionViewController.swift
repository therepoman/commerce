//
//  EmoteCollectionView.swift
//  TwitchEmoteKeyboardExtension
//
//  Created by Gregg, Jake on 7/20/18.
//  Copyright © 2018 Gregg, Jake. All rights reserved.
//

import UIKit

final class EmoteCollectionViewController: UICollectionViewController {
    fileprivate let reuseIdentifier = "emoteCell"
    fileprivate let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    
    var emotes: EmoteSets!
    fileprivate let itemsPerRow: CGFloat = 3
    
    func emoteForIndexPath(_ indexPath: IndexPath) -> Emote {
        print("emoteForIndexPath")
        let emoteSet = self.emotes.map[String(format: "%d", (indexPath as NSIndexPath).section)]!
        for emote in emoteSet {
            if emote.id == (indexPath as NSIndexPath).row {
                return emote
            }
        }
        let defaultReturn = Emote(code: "Kappa", id: 25)
        return defaultReturn
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        print("numberOfSections")
        var count = 0
        for emoteSet in self.emotes.map {
            for _ in emoteSet.value {
                count += 1
            }
        }
        
        return count
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("collectionView1")
        var count = 0
        for _ in self.emotes.map[String(format: "%d", section)]! {
            count += 1
        }
        return count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print("collectionView2")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! EmoteButtonCell
        
        let emote = emoteForIndexPath(indexPath)
        let button = EmoteButton(code: emote.code, id: emote.id)
        
        cell.setButton(button)
        return cell
    }
}

extension EmoteCollectionViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        print("collectionView3")
        //2
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        print("collectionView4")
        return sectionInsets
    }
    
    // 4
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        print("collectionView5")
        return sectionInsets.left
    }
}
