//
//  Emote.swift
//  TwitchEmoteKeyboardExtension
//
//  Created by Gregg, Jake on 7/19/18.
//  Copyright © 2018 Gregg, Jake. All rights reserved.
//

import Foundation

struct Emote {
    let code: String
    let id: Int
}

extension Emote {
    init?(json: [String: Any]) {
        guard let code = json["code"] as? String,
            let id = json["id"] as? Int
            else {
                return nil
        }
        
        self.code = code
        self.id = id
    }
}
