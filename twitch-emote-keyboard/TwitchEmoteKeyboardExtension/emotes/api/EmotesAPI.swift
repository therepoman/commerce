//
//  EmotesAPI.swift
//  TwitchEmoteKeyboardExtension
//
//  Created by Gregg, Jake on 7/19/18.
//  Copyright © 2018 Gregg, Jake. All rights reserved.
//

import Foundation

struct EmoteSets {
    let map: [String: [Emote]]
}

extension EmoteSets {
    static func emoteSets(matching userID: String, completion: @escaping (EmoteSets) -> Void) {
        var query = URLComponents(string: "https://api.twitch.tv")!
        query.path = String(format: "/v5/users/%s/emotes", userID)
        let clientID = URLQueryItem(name: "client_id", value: "jzkbprff40iqj646a697cyrvl0zt2m6")
        query.queryItems = [clientID] as [URLQueryItem]?
        
        var request = URLRequest(url: (query.url)!)
        request.httpMethod = "GET"
        request.setValue("OAuth 0h8fxaaz2yxb5x6dlnyn02yvl0exqn", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            var emoteSetsResult: [String: [Emote]] = [:]
            
            if let data = data,
                let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                for case let emoteSetJson in (json!["emoticon_sets"] as? [String: [Any]])! {
                    var emoteSetItems: [Emote] = []
                    for emoteSetItem in emoteSetJson.value {
                        let emoteSetConv: [String: Any] = emoteSetItem as! [String: Any]
                        emoteSetItems.append(Emote(json: emoteSetConv)!)
                    }
                    emoteSetItems = emoteSetItems.sorted(by: { $0.id < $1.id })
                    emoteSetsResult[emoteSetJson.key] = emoteSetItems
                }
            }
            
            completion(EmoteSets(map: emoteSetsResult))
        }
        
        task.resume()
    }
}
