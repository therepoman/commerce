//
//  KeyboardViewController.swift
//  TwitchEmoteKeyboardExtension
//
//  Created by Dow, Benjamin on 7/19/18.
//  Copyright © 2018 Gregg, Jake. All rights reserved.
//

import UIKit

class KeyboardViewController: UIInputViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var nextKeyboardButton: UIButton!
    @IBOutlet var backspaceButton: UIButton!
    
    var emotes: [Emote] = []
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        // Add custom view sizing constraints here
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        EmoteSets.emoteSets(matching: "240208102") { emoteSets in
            self.emotes = emoteSets.map["0"]!
            // load other emote sets.
//            for (setId, emoteList) in emoteSets.map {
//                if setId != "0" {
//                    self.emotes.append(contentsOf: emoteList)
//                }
//            }
            
            self.collectionView.reloadData()
            print("finished with emote on load")
        }
        
        self.nextKeyboardButton = UIButton(type: .system)
        self.nextKeyboardButton.setTitle(NSLocalizedString("Next Keyboard", comment: "go to next keyboard"), for: [])
        self.nextKeyboardButton.sizeToFit()
        self.nextKeyboardButton.translatesAutoresizingMaskIntoConstraints = false
        self.nextKeyboardButton.addTarget(self, action: #selector(self.handleInputModeList(from:with:)), for: .allTouchEvents)
        self.view.addSubview(self.nextKeyboardButton)
        
        self.nextKeyboardButton.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.nextKeyboardButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        self.backspaceButton = UIButton(type: .system)
        self.backspaceButton.setTitle(NSLocalizedString("Backspace", comment: "delete text"), for: [])
        self.backspaceButton.sizeToFit()
        self.backspaceButton.translatesAutoresizingMaskIntoConstraints = false
        self.backspaceButton.addTarget(self, action: #selector(self.deleteText(sender:)), for: .touchUpInside)
        self.view.addSubview(self.backspaceButton)
        
        self.backspaceButton.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.backspaceButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.backgroundColor = UIColor.black
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: 28, height: 28)
        layout.scrollDirection = .horizontal
        
        let frame = CGRect(x: self.view.frame.minX, y: self.view.frame.minY, width: self.view.frame.width, height: self.view.frame.height - 28)
        
        self.collectionView = UICollectionView(frame:frame, collectionViewLayout: layout)
        self.collectionView.allowsSelection = false
        self.collectionView.allowsMultipleSelection = false
        self.collectionView.sizeToFit()
        self.collectionView.translatesAutoresizingMaskIntoConstraints = false
        self.collectionView.backgroundColor = UIColor.black
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(EmoteButtonCell.self, forCellWithReuseIdentifier: "emoteCell")
        
        self.view.addSubview(self.collectionView)
        print("view did appear")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.emotes.count
    }
    
    func getEmote(_ number: Int) -> Emote? {
        if self.emotes.count == 0 {
            return nil
        }
        return self.emotes[number]
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "emoteCell", for: indexPath as IndexPath) as! EmoteButtonCell
        
        let emote = self.getEmote((indexPath as NSIndexPath).item)
        
        if emote == nil {
            return cell
        }
        
        for view in cell.subviews {
            view.removeFromSuperview()
        }
        
        let button = EmoteButton(code: (emote?.code)!, id: (emote?.id)!)
        button.addTarget(self, action: #selector(self.pushButton(sender:)), for: UIControlEvents.touchUpInside)
        button.setEmoteImage {
            cell.addSubview(button)
        }
        
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated
    }
    
    override func textWillChange(_ textInput: UITextInput?) {
        // The app is about to change the document's contents. Perform any preparation here.
    }
    
    override func textDidChange(_ textInput: UITextInput?) {
        // The app has just changed the document's contents, the document context has been updated.
    }
    
    @IBAction func pushButton(sender: UIButton!) {
        let emoteButton = sender as! EmoteButton
        let proxy = textDocumentProxy as UITextDocumentProxy
        proxy.insertText(emoteButton.code)
        proxy.insertText(" ")
    }
    
    @IBAction func deleteText(sender: UIButton!) {
        let proxy = textDocumentProxy as UITextDocumentProxy
        proxy.deleteBackward()
    }
}

