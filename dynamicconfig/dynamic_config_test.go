package dynamicconfig_test

import (
	"context"
	"reflect"
	"testing"
	"time"

	"code.justin.tv/commerce/dynamicconfig"
	"github.com/pkg/errors"
)

func TestDynamicConfigInitialization(t *testing.T) {
	ctx := context.Background()
	ls := dynamicconfig.NewLocalSource()
	ls.Set(map[string]interface{}{
		"key1": "value1",
	}, nil)
	cfg, err := dynamicconfig.NewDynamicConfig(ctx, ls, nil)
	if err != nil {
		t.Errorf("Expected no error initializing dynamic config, but got %+v instead", err)
	}

	// Test that values were initialized properly
	value1, ok := cfg.Get("key1").(string)
	if !ok {
		t.Errorf("Expected value1 to be a string, but was %+v instead", reflect.TypeOf(value1))
	}
	if value1 != "value1" {
		t.Errorf(`Expected value1 to be "value1", but got %v instead`, value1)
	}
}

func TestDynamicConfigReload(t *testing.T) {
	ctx := context.Background()
	ls := dynamicconfig.NewLocalSource()
	ls.Set(map[string]interface{}{
		"key1": "value1",
	}, nil)
	cfg, err := dynamicconfig.NewDynamicConfig(ctx, ls, nil)
	if err != nil {
		t.Errorf("Expected no error initializing dynamic config, but got %+v instead", err)
	}

	// Test that reload works properly
	ls.Set(map[string]interface{}{
		"key1": "newvalue1",
		"key2": "value2",
	}, nil)
	cfg.Reload(ctx)

	newValue1, ok := cfg.Get("key1").(string)
	if !ok {
		t.Errorf("Expected newValue1 to be a string, but was %+v instead", reflect.TypeOf(newValue1))
	}
	if newValue1 != "newvalue1" {
		t.Errorf(`Expected newValue1 to be "newvalue1", but got %v instead`, newValue1)
	}

	value2, ok := cfg.Get("key2").(string)
	if !ok {
		t.Errorf("Expected value2 to be a string, but was %+v instead", reflect.TypeOf(value2))
	}
	if value2 != "value2" {
		t.Errorf(`Expected value2 to be "value2", but got %v instead`, value2)
	}
}

func TestOnChangeHooks(t *testing.T) {
	ctx := context.Background()
	ls := dynamicconfig.NewLocalSource()
	ls.Set(map[string]interface{}{
		"key1": "value1",
	}, nil)
	cfg, err := dynamicconfig.NewDynamicConfig(ctx, ls, nil)
	if err != nil {
		t.Errorf("Expected no error initializing dynamic config, but got %+v instead", err)
	}

	key1Changes := 0
	key2Changes := 0

	cfg.OnChange("key1", func(val interface{}) {
		key1Changes++
	})
	cfg.OnChange("key2", func(val interface{}) {
		key2Changes++
	})
	cfg.OnChange("key2", func(val interface{}) {
		key2Changes += 10
	})

	// Reload once where key1 and key2 values change
	ls.Set(map[string]interface{}{
		"key1": "newvalue1",
		"key2": "value2",
	}, nil)
	cfg.Reload(ctx)

	// Reload again, only where key2 value changes
	ls.Set(map[string]interface{}{
		"key1": "newvalue1",
		"key2": "newvalue2",
	}, nil)
	cfg.Reload(ctx)

	if key1Changes != 1 {
		t.Errorf("Expected key1Changes to be 1, but was %v instead", key1Changes)

	}
	if key2Changes != 22 {
		t.Errorf("Expected key2Changes to be 22, but was %v instead", key2Changes)
	}
}

func TestTransforms(t *testing.T) {
	ctx := context.Background()
	ls := dynamicconfig.NewLocalSource()
	dateStr := "2019-01-16T18:47:32Z"
	ls.Set(map[string]interface{}{
		"date": dateStr,
	}, nil)

	dateTransform := func(key string, value interface{}) (interface{}, error) {
		str, ok := value.(string)
		if !ok {
			return nil, errors.New("value must be a string")

		}
		t, err := time.Parse(time.RFC3339, str)
		if err != nil {
			err = errors.Wrap(err, "value must be valid rfc3339")
			return nil, err

		}
		return t, nil
	}
	cfg, err := dynamicconfig.NewDynamicConfig(ctx, ls, map[string]dynamicconfig.Transform{
		"date": dateTransform,
	})
	if err != nil {
		t.Errorf("Expected no error initializing dynamic config, but got %+v instead", err)
	}

	date, ok := cfg.Get("date").(time.Time)
	if !ok {
		t.Error("Expected date to be a time.Time, but was not")
	}
	if date.Format(time.RFC3339) != dateStr {
		t.Errorf("Expected date to be %s, but got %s instead", dateStr, date.Format(time.RFC3339))
	}

	// Update store to invalid rfc3339
	ls.Set(map[string]interface{}{
		"date": "invalidRFC3339",
	}, nil)
	cfg.Reload(ctx)

	// Should use last value still
	date, ok = cfg.Get("date").(time.Time)
	if !ok {
		t.Error("Expected date to be a time.Time, but was not")
	}
	if date.Format(time.RFC3339) != dateStr {
		t.Errorf("Expected date to be %s, but got %s instead", dateStr, date.Format(time.RFC3339))
	}
}
