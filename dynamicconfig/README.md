# DynamicConfig

## The Basics

### Initialize Source
The `dynamicconfig` library uses a `Source` to fetch values from the backing store.  A `Source`
implements the following interface:

```golang
// Source is the backing store of the dynamic config.
type Source interface {
	// FetchValues returns a key value map given a list of keys.
	FetchValues(ctx context.Context) (map[string]interface{}, error)
}
```
The `dynamicconfig` will provide common implementations of sources. The current implementations:
1. AWS SSM
2. AWS S3 (multiple files as single source `S3MultiFileSource` or one JSON file `S3JSONSource`)

First we must initialize our source. We will use AWS SSM (systems manager parameter store) in the
following example. AWS SSM requires us to specify what keys we are querying for, so the `SSMSource`
takes a `[]string` of keys.

```golang
// Create AWS SSM client
sess := session.Must(session.NewSession())
ssmClient := ssm.New(sess)


// Create dynamicconfig.SSMSource with list of keys to fetch. For SSM, we must specify a list of
// keys to look for, but if we were using a source like S3 that returns a json blob, we could
// infer the keys from the jsob blob.
keys := []string{"testParam", "testParam2"}
ssmSource := dynamicconfig.NewSSMSource(keys, ssmClient)
```

For S3 JSON source, there is a runnable example in `examples/main.go`.

### Initialize DynamicConfig
Next, create a new instace of `DynamicConfig` with a given source. If `NewDynamicConfig` fails to
fetch values during initialization, it will return an error and a working `DyanmicConfig` object so
that the service can continue to run. In an error case, values from `.Get(key)` will be `nil`, so
having a `nil` check that returns a default value is generally a good idea.

```golang
config, err := dynamicconfig.NewDynamicConfig(ctx, ssmSource, nil)
if err != nil {
	// Failed while initializing values of config
	log.Printf("Error initializing first values: %+v", err)
	// Will still return a usable DynamicConfig (with all values = nil), so our app can continue
	// initializing...
}
```

### Retrieve Values from DynamicConfig
Retrieving a value from a `DynamicConfig` object is simple. We will need to type cast and check for
nil. SSM always returns its values as strings. It is also recommended write a type safe wrapper for
each parameter:

```golang
func IsExperimentEnabled() bool {
	if value, ok := config.Get("is-experiment-enabled").(string); ok {
		if value == "1" {
			return true
		}
		return false
	}
	// If cannot be type cast to string (value could be nil if DynamicConfig
	// failed to initialize), then return default value of false.
	return false
}

```
## Updating the DynamicConfig values
The `DynamicConfig` object exposes a `Reload(context.Context) error` function that will update the
values using the passed in source.

### Polling
The `dynamicconfig` library provides a `Poller` that can call the reload function at a specified
interval. It can be used as follows:

```golang
poller := dynamicconfig.Poller{
	Reloader: dynamicConfig,
	Interval: 10 * time.Second,
	Timeout:  10 * time.Second,
}
go poller.Poll()
defer poller.Stop()
```

## Transforms
Often a config value will need to be transformed before it is usable. For example, we often store
dates in rfc3339 and will need to convert them to a `time.Time` object. To get around having to
perform the transform each time we read the value, the `dynamicconfig` library can transform the
value for you.

A tranform has the following definition:

```golang
// Transform is triggered when a value has changed. It will replace the value with the value returned
// in the transform. If an error is returned it will not update the parameter in the local store.
type Transform func(key string, value interface{}) (interface{}, error)
```

When initializing a `DynamicConfig`, pass in a map of key->transform as the 3rd argument:

```golang
func transformDate(key string, value interface{}) (interface{}, error) {
	str, ok := value.(string)
	if !ok {
		return nil, errors.Errorf("value of %s must be a string", key)
	}
	t, err := time.Parse(time.RFC3339, str)
	if err != nil {
		err = errors.Wrapf(err, "value of %s must be valid rfc3339", key)
		return nil, err
	}
	return t, nil
}

config, err := dynamicconfig.NewDynamicConfig(ctx, ssmSource, map[string]dynamicconfig.Transform{
	"experiment-start-date": transformDate,
})
```
Now we can access "experiment-start-date" as a `time.Time`:

```golang
func ExperimentDate() time.Time {
	if value, ok := config.Get("experiment-start-date").(time.Time); ok {
		return value
	}
	// If cannot be type cast to time.Time (value could be nil if DynamicConfig
	// failed to initialize), then return default value of false.
	return DefaultExperiementDate
}
```

### Errors during transform
If an error is returned during a transform, `DyanmicConfig` will use the last known "good" value.
This means that if someone makes a mistake when updating a config, it will not cause the service to
fail. This also means, if the first value fetched (during initialization) is "bad", `DynamicConfig`
will return `nil` for that parameter.

## OnChange Hooks
Sometimes reinitialization is required when a value is changed. To solve for this, `dynamicconfig`
allows for `OnChange` hooks to be registed on specific keys.

```golang
// OnChangeHook is triggered when a value has changed.
type OnChangeHook func(value interface{})
```

`OnChange` hooks can be registered dynamically:

```golang
myDynamicConfig.OnChange("database-password", func(value interface{}) {
	// Reinitialize database pool here
})
```

`OnChange` can be called multiple times on a single key in order to register multiple hooks.