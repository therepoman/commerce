package dynamicconfig

import (
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/ssm"
	"github.com/pkg/errors"
	"testing"
)

type mockParamGetter struct {
	callCount int
	err error
}

func (m *mockParamGetter) GetParametersWithContext(ctx aws.Context, input *ssm.GetParametersInput, opts ...request.Option) (*ssm.GetParametersOutput, error) {
	m.callCount++

	if len(input.Names) > ssmBatchSize {
		return nil, errors.New("batch too big")
	}

	var out []*ssm.Parameter
	for _, p := range input.Names {
		k := *p
		v := fmt.Sprintf("%s-value", *p)
		out = append(out, &ssm.Parameter{
			Name:             &k,
			Value:            &v,
		})
	}

	return &ssm.GetParametersOutput{
		Parameters:        out,
	}, m.err
}

func getKeyTestData(n int) (keys []string, ret map[string]interface{}) {
	keys = make([]string, n)
	for i := range keys {
		keys[i] = fmt.Sprintf("key%d", i)
	}
	ret = make(map[string]interface{}, n)
	for _, key := range keys {
		ret[key] = fmt.Sprintf("%s-value", key)
	}
	return
}

func TestSSMSource_FetchValues(t *testing.T) {
	testCases := []struct{
		description string
		keyCount int
		expectedCalls int
		wantErr bool
		fetchErr error
	}{
		{
			description: "with empty keys",
		},
		{
			description: "with keyCount < batch limit",
			keyCount: 3,
			expectedCalls: 1,
		},
		{
			description: "with keyCount == batch limit",
			keyCount: ssmBatchSize,
			expectedCalls: 1,
		},
		{
			description: "with keyCount > batch limit, keyCount % batch limit == 0",
			keyCount: ssmBatchSize * 3,
			expectedCalls: 3,
		},
		{
			description: "with keyCount > batch limit, keyCount % batch limit != 0",
			keyCount: ssmBatchSize * 3 + 1,
			expectedCalls: 4,
		},
		{
			description: "with error on fetch",
			keyCount: 5,
			expectedCalls: 1,
			wantErr: true,
			fetchErr: errors.New("your ssm asplode >:c"),
		},
	}

	for _, tt := range testCases {
		t.Run(tt.description, func(t *testing.T) {
			keys, expectedValues := getKeyTestData(tt.keyCount)
			source := &SSMSource{
				ssmClient: &mockParamGetter{err: tt.fetchErr},
				keys:      keys,
			}
			got, err := source.FetchValues(context.Background())
			if !mapsEqual(expectedValues, got) && !tt.wantErr {
				t.Fatalf("maps not equal:\n  expected: %v\n  got: %v\n", expectedValues, got)
			}
			if (err != nil) != tt.wantErr {
				t.Fatalf("unexpected error value. err: %v", err)
			}
			if mock := source.ssmClient.(*mockParamGetter); mock.callCount != tt.expectedCalls {
				t.Fatalf("unexpected number of calls to GetParameters.\n  expected: %d\n  got: %d\n", tt.expectedCalls, mock.callCount)
			}
		})
	}
}

func mapsEqual(a, b map[string]interface{}) bool {
	if len(a) != len(b) {
		return false
	}

	for k,v := range a {
		vb, ok := b[k]
		if !ok || vb != v {
			return false
		}
	}

	return true
}
