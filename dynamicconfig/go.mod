module code.justin.tv/commerce/dynamicconfig

go 1.16

require (
	github.com/aws/aws-sdk-go v1.16.32
	github.com/hashicorp/go-multierror v1.0.0
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d // indirect
)
