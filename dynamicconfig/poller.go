package dynamicconfig

import (
	"context"
	"sync"
	"time"
)

type reloader interface {
	Reload(ctx context.Context) error
}

// Poller is an object that will continously call Reload on a given reloader at a given interval.
// Use NewPoller to create an instance
type Poller struct {
	// Reloader will have its Reload method called at each tick of the Poller.
	Reloader reloader
	// Interval is the interval at which the Poller will trigger the Reloader's Reload method.
	Interval time.Duration
	// Timeout of the context passed into the Reload method. In the case of a timeout, the Reload
	// method will be called again at the next tick of the Poller.
	Timeout time.Duration
	// OnError will be called when the Poller fails to reload the config. Use OnError to send error
	// metrics or trigger alarms that signify that the Poller is failing to reload the config.
	OnError func(error)

	running  bool
	shutdown chan struct{}
	mu       sync.Mutex
}

// Poll is a blocking function that will continuously call the Reload method of the Reloader until
// Stop() is called.
func (p *Poller) Poll() {
	p.mu.Lock()
	if p.Reloader == nil {
		panic("Reloader cannot be nil")
	}
	if p.Interval <= 0 {
		panic("Interval must be greater than 0")
	}
	if p.Timeout <= 0 {
		panic("Timeout must be greater than 0")
	}

	if p.running {
		panic("already polling")
	}
	p.running = true

	// Init shutdown channel
	p.shutdown = make(chan struct{})
	p.mu.Unlock()

	// Begin polling
	ticker := time.NewTicker(p.Interval)
	for {
		select {
		case <-ticker.C:
			ctx, cancel := context.WithTimeout(context.Background(), p.Timeout)
			err := p.Reloader.Reload(ctx)
			if err != nil && p.OnError != nil {
				p.OnError(err)
			}
			cancel()
		case <-p.shutdown:
			return
		}
	}
}

// Stop will break out of the polling loop. Calling Stop on a stopped Poller is idempotent.
func (p *Poller) Stop() {
	p.mu.Lock()
	if !p.running {
		// Manually unlock instead of deferring since we don't want unlocking to wait for the channel send(which could block)
		p.mu.Unlock()
		return
	}
	p.running = false
	p.mu.Unlock()
	p.shutdown <- struct{}{}
}

// Running indicates whether the poller is actively polling or in a stopped state.
func (p *Poller) Running() bool {
	p.mu.Lock()
	defer p.mu.Unlock()
	return p.running
}
