package main

import (
	"context"
	"fmt"
	"os"
	"time"

	"code.justin.tv/commerce/dynamicconfig"
)

// The s3 example requires s3 profile. Currently it uses fortuna bucket.
// AWS_PROFILE=twitch-fortuna-dev go run examples/main.go
func main() {
	ctx := context.Background()
	pollInterval := 10 * time.Second

	// --------------
	// S3 JSON example
	// --------------

	// Edit the following vars for the keys you want to test for
	s3Filepath := "config/dev.json"
	s3Bucket := "fortuna-dev-campaigns"
	testKey := "Version" // key you want to output its value of during the test

	s3JSONSource, err := dynamicconfig.NewS3JSONSource("us-west-2", "s3-us-west-2.amazonaws.com", s3Bucket, s3Filepath)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	// Initialize dynamic config with s3 source
	configFromS3, err := dynamicconfig.NewDynamicConfig(ctx, s3JSONSource, nil)
	if err != nil {
		fmt.Printf("Error initializing first values: %+v\n", err)
		os.Exit(1)
	}

	fmt.Println("DynamicConfig with S3 source successfully initialized.")
	printValue(configFromS3, testKey)

	poller := dynamicconfig.Poller{
		Reloader: configFromS3,
		Interval: pollInterval,
		Timeout:  pollInterval,
	}
	go poller.Poll()
	defer poller.Stop()

	ticker := time.NewTicker(pollInterval)
	for {
		select {
		case <-ticker.C:
			printValue(configFromS3, testKey)
			return
		}
	}
}

// Simply prints out a string representation of the stored value
func printValue(config *dynamicconfig.DynamicConfig, key string) {
	value := config.Get(key)
	fmt.Printf("--Value for %s--\n", key)
	fmt.Printf("%+v\n", value)
}
