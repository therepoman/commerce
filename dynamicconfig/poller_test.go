package dynamicconfig_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/dynamicconfig"
)

type reloadCounter struct {
	reloads int
}

func (rc *reloadCounter) Reload(ctx context.Context) error {
	rc.reloads++
	return nil
}

func TestPoller(t *testing.T) {
	rc := &reloadCounter{}
	poller := dynamicconfig.Poller{
		Reloader: rc,
		Interval: 500 * time.Millisecond,
		Timeout:  1 * time.Second,
	}

	// Start polling every 500ms
	go poller.Poll()

	// Wait about 2.2s
	time.Sleep(2200 * time.Millisecond)

	// Poller should be in active state
	if !poller.Running() {
		t.Error("Expected poller to be running, but it is stopped instead")
	}

	// Stop poller
	poller.Stop()
	// Poller should not be running
	if poller.Running() {
		t.Error("Expected poller to be stopped, but it is running instead")
	}

	// Sleep extra time to confirm poller isn't calling reload anymore
	time.Sleep(50 * time.Millisecond)
	// Confirm reload was called exactly 4 times
	if rc.reloads != 4 {
		t.Errorf("Expected number of reloads to be 4, but got %v instead", rc.reloads)
	}
}

func TestPollerOnError(t *testing.T) {
	ctx := context.Background()

	ls := dynamicconfig.NewLocalSource()
	dc, err := dynamicconfig.NewDynamicConfig(ctx, ls, nil)
	if err != nil {
		t.Fatalf("error creating DynamicConfig: %+v\n", err)
	}

	err = errors.New("an error")
	ls.Set(nil, err)

	errors := make(map[string]int)
	poller := dynamicconfig.Poller{
		Reloader: dc,
		Interval: 500 * time.Millisecond,
		Timeout:  1 * time.Second,
		OnError: func(err error) {
			// Increment the error count for this specific error
			errors[err.Error()] = errors[err.Error()] + 1
		},
	}

	// Start polling every 500ms
	go poller.Poll()

	// Wait about 2.2s
	time.Sleep(2200 * time.Millisecond)

	// Poller should be in active state
	if !poller.Running() {
		t.Error("Expected poller to be running, but it is stopped instead")
	}

	// Stop poller
	poller.Stop()
	// Poller should not be running
	if poller.Running() {
		t.Error("Expected poller to be stopped, but it is running instead")
	}

	// Sleep extra time to confirm poller isn't calling reload anymore
	time.Sleep(50 * time.Millisecond)

	// Confirm reload was called exactly 4 times with the expected error count
	if errors[err.Error()] != 4 {
		t.Errorf("Expected number of error hooks triggered to be 4, but got %v instead", errors)
	}
}
