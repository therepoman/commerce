#pragma once

#include<Windows.h>
#include<fstream>
#include<mutex>

#include "IMessageTransport.h"

namespace Fuel
{
	class PipeCreationFailedException : public std::runtime_error {
	public:
		PipeCreationFailedException(const std::string type) : runtime_error("Message transport failed to create the " + type + " pipes.") {};
	};

	class SetHandleInfoFailedException : public std::runtime_error {
	public:
		SetHandleInfoFailedException(const std::string type) : runtime_error("Message transport failed to set handle info for " + type + " handle.") {};
	};

	struct HandleDeleter
	{
		typedef HANDLE pointer;

		void operator() (HANDLE handle)
		{
			if (handle != INVALID_HANDLE_VALUE)
			{
				CloseHandle(handle);
			}
		}
	};

	typedef struct PROCTHREADATTRIBUTE {
		DWORD_PTR Attribute;
		PVOID lpValue;
		SIZE_T cbSize;
	} PROCTHREADATTRIBUTE;

	typedef std::unique_ptr<HANDLE, HandleDeleter> unique_handle;

	class MessageTransport : public IMessageTransport
	{
	public:
		MessageTransport();
		MessageTransport(HANDLE& readHandle, HANDLE& writeHandle);
		~MessageTransport();

		// disable copying because of handles
		MessageTransport(const MessageTransport& that) = delete;
		void operator=(MessageTransport const &x) = delete;

		void Close();

		void AbortRead() const;
		Message ReadMessage() const;
		void WriteMessage(const Message& m) const;

	private:
		static FILE* HandleToFile(HANDLE handle, char *mode);
		static BOOL CreateProcessWithAttributes(LPCTSTR lpApplicationName, LPTSTR lpCommandLine, LPSECURITY_ATTRIBUTES lpProcessAttributes, LPSECURITY_ATTRIBUTES lpThreadAttributes, BOOL bInheritHandles,	DWORD dwCreationFlags, LPVOID lpEnvironment, LPCTSTR lpCurrentDirectory, LPSTARTUPINFO lpStartupInfo, LPPROCESS_INFORMATION lpProcessInformation, DWORD cAttributes, const PROCTHREADATTRIBUTE rgAttributes[]);
		static HANDLE StartProcess(HANDLE readHandle, HANDLE writeHandle);
		static HMODULE GetCurrentModule();
		static void GetFuelPumpPaths(int maxSize, TCHAR* fuelPumpPath, TCHAR* fuelPumpWorkingDirectory);

		void WriteMessagePart(const MessagePart& part) const;
		MessagePart ReadMessagePart() const;

		// Won't read messages greater than this
		static const uint32_t MAX_SIZE = 50 * 1024 * 1024;

		HANDLE m_process;
		unique_handle m_readHandle, m_writeHandle;

		mutable std::mutex m_read_mutex;
		mutable std::mutex m_write_mutex;

		mutable std::ifstream m_inputStream;
		mutable std::ofstream m_outputStream;
	};
}