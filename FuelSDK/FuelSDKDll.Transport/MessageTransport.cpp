#include<io.h>
#include<tchar.h>
#include "MessageTransport.h"
#include "logger.h"
#include "fuel_impl/signature_validator.h"
#include "Fuel_generated.h"

#include <sstream>

using namespace Fuel;

FILE* MessageTransport::HandleToFile(HANDLE handle, char *mode)
{
	int fd = _open_osfhandle((intptr_t)handle, 0);
	if (fd != -1)
	{
		FILE* file = _fdopen(fd, mode);

		if (file != NULL)
		{
			return file;
		}
	}

	return nullptr;
}

HMODULE MessageTransport::GetCurrentModule()
{
	HMODULE hModule = NULL;

	GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
		(LPCTSTR) (MessageTransport::GetCurrentModule),
		&hModule);

	return hModule;
}

void MessageTransport::GetFuelPumpPaths(int maxSize, TCHAR* fuelPumpPath, TCHAR* fuelPumpWorkingDirectory)
{
	auto handle = GetCurrentModule();

	GetModuleFileName(handle, fuelPumpPath, maxSize);

	TCHAR drive[_MAX_DRIVE];
	TCHAR dir[_MAX_DIR];
	TCHAR fname[_MAX_FNAME];
	TCHAR ext[_MAX_EXT];

	_tsplitpath_s(fuelPumpPath, drive, _MAX_DRIVE, dir, _MAX_DIR, fname, _MAX_FNAME, ext, _MAX_EXT);
	_tmakepath_s(fuelPumpPath, maxSize, drive, dir, TEXT("FuelPump"), TEXT(".exe"));
	_tmakepath_s(fuelPumpWorkingDirectory, maxSize, drive, dir, TEXT(""), TEXT(""));
}

HANDLE MessageTransport::StartProcess(HANDLE readHandle, HANDLE writeHandle)
{
	HANDLE handles[2] = { readHandle, writeHandle };
	const PROCTHREADATTRIBUTE attributes[] = {
		{
			PROC_THREAD_ATTRIBUTE_HANDLE_LIST,
			handles,
			sizeof(handles),
		},
	};

	TCHAR szFuelPumpPath[_MAX_PATH];
	TCHAR szWorkingDirectory[_MAX_PATH];
	MessageTransport::GetFuelPumpPaths(_MAX_PATH, szFuelPumpPath, szWorkingDirectory);

	STARTUPINFO siStartInfo;
	PROCESS_INFORMATION piProcInfo;
	BOOL bSuccess = FALSE;

	// Set up members of the PROCESS_INFORMATION structure. 
	ZeroMemory(&piProcInfo, sizeof(PROCESS_INFORMATION));

	// Set up members of the STARTUPINFO structure. 
	// This structure specifies the STDIN and STDOUT handles for redirection.

	ZeroMemory(&siStartInfo, sizeof(STARTUPINFO));
	siStartInfo.cb = sizeof(STARTUPINFO);
	//siStartInfo.dwFlags |= STARTF_USESTDHANDLES;

	#ifndef _DEBUG
	if (!Fuel::Internal::SignatureValidator::VerifyEmbeddedSignature(szFuelPumpPath))
	{
		std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> utf8_16_converter;
		Logger::Instance(FUEL_MESSAGING_LOGGER)->critical("Invalid signature in file {0}", utf8_16_converter.to_bytes(szFuelPumpPath));
		return INVALID_HANDLE_VALUE;
	}
	#endif

	TCHAR szCommandLine[32];
	_stprintf_s(szCommandLine, 32, TEXT(" %lu %lu"), (unsigned long)readHandle, (unsigned long)writeHandle);

	// Create the child process. 
	bSuccess = CreateProcessWithAttributes(
		szFuelPumpPath,
		szCommandLine,
		NULL,
		NULL,
		TRUE,
		CREATE_NEW_CONSOLE,
		NULL,
		szWorkingDirectory,
		&siStartInfo,
		&piProcInfo,
		ARRAYSIZE(attributes),
		attributes);

	// If an error occurs, exit the application. 
	if (!bSuccess)
	{
		Logger::Instance(FUEL_MESSAGING_LOGGER)->critical("Error starting FuelPump process, error code {0} !!", GetLastError());
		return INVALID_HANDLE_VALUE;
	}
	else
	{
		// Close handles to the child process and its primary thread.
		// Some applications might keep these handles to monitor the status
		// of the child process, for example. 
		CloseHandle(piProcInfo.hThread);
	}

	return piProcInfo.hProcess;
}

BOOL MessageTransport::CreateProcessWithAttributes(LPCTSTR lpApplicationName, LPTSTR lpCommandLine,	LPSECURITY_ATTRIBUTES lpProcessAttributes, LPSECURITY_ATTRIBUTES lpThreadAttributes, BOOL bInheritHandles, DWORD dwCreationFlags, LPVOID lpEnvironment, LPCTSTR lpCurrentDirectory, LPSTARTUPINFO lpStartupInfo, LPPROCESS_INFORMATION lpProcessInformation, DWORD cAttributes,	const PROCTHREADATTRIBUTE rgAttributes[]) {
	BOOL fSuccess;
	BOOL fInitialized = FALSE;
	SIZE_T size = 0;
	LPPROC_THREAD_ATTRIBUTE_LIST lpAttributeList = NULL;

	fSuccess = InitializeProcThreadAttributeList(NULL, cAttributes, 0, &size) ||
		GetLastError() == ERROR_INSUFFICIENT_BUFFER;

	if (fSuccess) {
		lpAttributeList = reinterpret_cast<LPPROC_THREAD_ATTRIBUTE_LIST>
			(HeapAlloc(GetProcessHeap(), 0, size));
		fSuccess = lpAttributeList != NULL;
	}
	if (fSuccess) {
		fSuccess = InitializeProcThreadAttributeList(lpAttributeList,
			cAttributes, 0, &size);
	}
	if (fSuccess) {
		fInitialized = TRUE;
		for (DWORD index = 0; index < cAttributes && fSuccess; index++) {
			fSuccess = UpdateProcThreadAttribute(lpAttributeList,
				0, rgAttributes[index].Attribute,
				rgAttributes[index].lpValue,
				rgAttributes[index].cbSize, NULL, NULL);
		}
	}
	if (fSuccess) {
		STARTUPINFOEX info;
		ZeroMemory(&info, sizeof(info));
		info.StartupInfo = *lpStartupInfo;
		info.StartupInfo.cb = sizeof(info);
		info.lpAttributeList = lpAttributeList;
		fSuccess = CreateProcess(lpApplicationName,
			lpCommandLine,
			lpProcessAttributes,
			lpThreadAttributes,
			bInheritHandles,
			dwCreationFlags | EXTENDED_STARTUPINFO_PRESENT,
			lpEnvironment,
			lpCurrentDirectory,
			&info.StartupInfo,
			lpProcessInformation);
	}

	if (fInitialized) DeleteProcThreadAttributeList(lpAttributeList);
	if (lpAttributeList) HeapFree(GetProcessHeap(), 0, lpAttributeList);
	return fSuccess;
}

MessageTransport::MessageTransport()
{
	HANDLE inputReadHandle, inputWriteHandle;
	HANDLE outputReadHandle, outputWriteHandle;

	SECURITY_ATTRIBUTES saAttr;
	saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
	saAttr.bInheritHandle = TRUE;
	saAttr.lpSecurityDescriptor = NULL;

	// Create output pipe
	if (!CreatePipe(&outputReadHandle, &outputWriteHandle, &saAttr, 0))
	{
		throw PipeCreationFailedException("output");
	}

	unique_handle outputReadPtr(outputReadHandle);
	unique_handle outputWritePtr(outputWriteHandle);

	// Stop the output handle from being inherited
	if (!SetHandleInformation(outputWriteHandle, HANDLE_FLAG_INHERIT, 0))
	{
		throw SetHandleInfoFailedException("outputWrite");
	}

	// Create the input pipe
	if (!CreatePipe(&inputReadHandle, &inputWriteHandle, &saAttr, 0))
	{
		throw PipeCreationFailedException("input");
	}

	unique_handle inputReadPtr(inputReadHandle);
	unique_handle inputWritePtr(inputWriteHandle);

	// Stop the input read handle from being inherited
	if (!SetHandleInformation(inputReadHandle, HANDLE_FLAG_INHERIT, 0))
	{
		throw SetHandleInfoFailedException("inputWrite");
	}

	m_inputStream = std::ifstream(HandleToFile(inputReadPtr.get(), "r"));
	m_outputStream = std::ofstream(HandleToFile(outputWritePtr.get(), "w"));
	m_readHandle = std::move(inputReadPtr);
	m_writeHandle = std::move(outputWritePtr);
	m_process = StartProcess(outputReadPtr.get(), inputWritePtr.get());
}

MessageTransport::~MessageTransport()
{
	m_readHandle.release();
	m_writeHandle.release();
	m_inputStream.close();
	m_outputStream.close();
	CloseHandle(m_process);
}

void MessageTransport::AbortRead() const
{
	CancelIoEx(m_readHandle.get(), NULL);
}

MessagePart MessageTransport::ReadMessagePart() const
{
	uint32_t size;
	m_inputStream.read((char*)&size, sizeof(uint32_t));
	if (m_inputStream) {
		if (size <= MAX_SIZE)
		{
			uint8_t* buffer = (uint8_t*)malloc(sizeof(uint8_t)*size);
			if (buffer != NULL)
			{
				m_inputStream.read((char*)buffer, size);
				if (m_inputStream)
				{
					Logger::Instance(FUEL_MESSAGING_LOGGER)->debug("Received and returning message of size {0}", size);
					return MessagePart(size, buffer);
				}
				Logger::Instance(FUEL_MESSAGING_LOGGER)->debug("Failed to read message into buffer");
				free(buffer);
			}
			else
			{
				Logger::Instance(FUEL_MESSAGING_LOGGER)->error("Failed to allocate buffer for message size {0}", size);
			}
		}
		else
		{
			// Skip the message as it is too large
			Logger::Instance(FUEL_MESSAGING_LOGGER)->error("Message of size {0} is too large!", size);
			m_inputStream.seekg(size);
		}
	}

	if ((m_inputStream.rdstate() & std::ifstream::failbit) != 0) {
		Logger::Instance(FUEL_DLL_LOGGER)->error("FAIL !");
		throw MessageTransportFatalError();
	}

	if ((m_inputStream.rdstate() & std::ifstream::badbit) != 0) {
		Logger::Instance(FUEL_DLL_LOGGER)->error("BAD !");
		throw MessageTransportFatalError();
	}

	if ((m_inputStream.rdstate() & std::ifstream::eofbit) != 0) {
		Logger::Instance(FUEL_DLL_LOGGER)->error("EOF !");
		throw MessageTransportFatalError();
	}

	m_inputStream.rdstate();
	return MessagePart();
}

void MessageTransport::WriteMessagePart(const MessagePart& part) const
{
	uint32_t partSize = part.GetSize();
	Logger::Instance(FUEL_MESSAGING_LOGGER)->debug("Writing message part of size {0}", partSize);
	m_outputStream.write((char*)&partSize, sizeof(uint32_t));
	const uint8_t* partData = part.GetBuffer();
	m_outputStream.write((char*)partData, partSize);
	m_outputStream.flush();
}

Message MessageTransport::ReadMessage() const
{
	MessagePart header = ReadMessagePart();
	MessagePart body = ReadMessagePart();
	return Message(std::move(header), std::move(body));
}

void MessageTransport::WriteMessage(const Message& m) const
{
	// Message writes need to be atomic, so only allow one write at a time. lock_guard auto releases the lock when it goes out of scope.
	std::lock_guard<std::mutex> lock(m_write_mutex);

	WriteMessagePart(m.GetHeader());
	WriteMessagePart(m.GetBody());

	Logger::Instance(FUEL_MESSAGING_LOGGER)->info("Wrote message of type {0} for request {1}", Fuel::Messaging::EnumNameMessage((Fuel::Messaging::Message)m.GetType()), m.GetRequestId());
}
