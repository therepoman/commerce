#pragma once

#include <memory>
#include <string>

#include "MessagePart.h"

namespace Fuel
{
	class Message
	{
	public:
		class HeaderCreationException : public std::exception
		{
		public:
			HeaderCreationException(const char* oReason) :reason(oReason){};
			const char * what() const throw ()
			{
				return reason;
			}
		private:
			const char * reason;
		};

		Message(const Message& source, int type, MessagePart&& body);
		Message(int type, MessagePart&& body);
		Message(int type, uint32_t bodySize, uint8_t* bodyBuffer);
		Message(MessagePart&& headerPart, uint32_t bodySize, uint8_t* bodyBuffer);
		Message(MessagePart&& headerPart, MessagePart&& bodyPart);

		// disable copying, only moving
		Message(const Message&) = delete;
		Message& operator=(const Message& rhs) = delete;

		Message(Message&& rhs);

		Message& operator=(Message&& rhs)
		{
			if (this != &rhs)
			{
				m_bodyPart = std::move(rhs.m_bodyPart);
				m_headerPart = std::move(rhs.m_headerPart);
			}
			return *this;
		}

		bool IsEmpty() const { return m_headerPart.IsEmpty(); }
		const MessagePart& GetHeader() const;
		const MessagePart& GetBody() const;
		const std::string& GetRequestId() const;
		int GetType() const;
	private:
		MessagePart m_bodyPart;
		MessagePart m_headerPart;
		std::string m_requestId;
		int m_type;
		
		void ParseHeader();
		std::string CreateRequestId();
		MessagePart CreateHeader(int type, std::string requestId);
	};
}
