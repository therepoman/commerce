#pragma once

#include <memory>
#include "Message.h"

namespace Fuel
{
	class MessageTransportFatalError : public std::runtime_error {
	public:
		MessageTransportFatalError() :
			std::runtime_error("Unexpected fatal error")
		{}

		MessageTransportFatalError(const char* message) :
			std::runtime_error(message)
		{}
	};

	struct IMessageTransport
	{
		virtual ~IMessageTransport() {}

		// in the event that the backend process can't be started, or recovered, a MessageTransportFatalError error will be thrown
		virtual void AbortRead() const = 0;
		virtual Message ReadMessage() const = 0;
		virtual void WriteMessage(const Message& m) const = 0;
	};
}