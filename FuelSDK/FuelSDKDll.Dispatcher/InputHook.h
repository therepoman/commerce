#pragma once

#include <Windows.h>
#include <memory>

#include "Fuel_generated.h"
#include "LoggingLimiter.h"
#include "OverlayManager.h"
#include "IMessageDispatcher.h"

namespace Fuel {
	namespace Input {
		class InputHook {
		public:
			static std::shared_ptr<InputHook> Instance();

			void SetOverlayManager(const std::shared_ptr<OverlayManager> &overlayManager);
			void SetDispatcher(const std::shared_ptr<Fuel::IMessageDispatcher> oDispatcher);

			void InitHook(HWND hwnd);
			void EndHook();
			LRESULT HandleKeyboardMessage(int code, WPARAM wParam, LPARAM lParam);
			LRESULT HandleMouseMessage(int code, WPARAM wParam, LPARAM lParam);

			void SendKeyboardEvent(int instanceId, Fuel::Messaging::KeyEventType keyEventType, WPARAM keyCode, bool ctrlDown);
			void SendMouseEvent(int x, int y, Fuel::Messaging::MouseButton button, bool mouseUp, int clickCount);
			void SendMouseMoveEvent(int x, int y, bool leftMouseDown);
			void SendMouseWheelEvent(int x, int y, int deltaX, int deltaY);
			void SendCloseOverlayEvent(int instanceId);

			~InputHook();
		private:
			void SendMessage(flatbuffers::FlatBufferBuilder &fbb, flatbuffers::Offset<void> &table, Fuel::Messaging::Message message);
			InputHook();
			bool GetOverlayAtLocation(int x, int y, OverlayDesc &overlayDesc);
			bool GetOverlayWithFocus(OverlayDesc &overlayDesc);
			void CheckDoubleClick(POINT point);
			HHOOK keyboardHook, mouseHook;
			HWND m_hWnd;
			DWORD hookedWindowThreadId;
			int m_overlayInstanceIdWithFocus = -1;
			LoggingLimiter m_coordinateConversionLoggingLimiter;
			std::shared_ptr<OverlayManager> m_overlayManager;
			std::shared_ptr<IMessageDispatcher> m_dispatcher;
			static const int PRESSED_THRESHOLD = 0;
			static const int SINGLE_CLICK = 1;
			static const int TRIPLE_CLICK = 3;
			int lastClickTime = 0, clicks = 0;
			POINT lastClickPoint;
		};
	}
}
