#pragma once

#include <map>

#include "OverlayMessageHandler.h"
#include "OverlayManager.h"
#include "InputHook.h"

#include "MessagePump.h"

namespace Fuel
{
	class Application : public MessagePump
	{
		std::shared_ptr<Fuel::IOverlayFactory> overlayFactory;
		std::shared_ptr<Fuel::OverlayManager> overlayManager;
		std::shared_ptr<Fuel::Input::InputHook> inputHook;
		std::shared_ptr<Fuel::IMessageDispatcher> dispatcher;
		std::shared_ptr<Fuel::DefaultCallbackHandler> defaultCallbackHandler;
		std::unique_ptr<Fuel::OverlayMessageHandler> overlayMessageHandler;
		std::map<Fuel::Messaging::Message, std::unique_ptr<Fuel::IMessageHandler>> handlers;
	public:
		Application(Fuel::RendererType renderType, const char* vendorId, const char* gameVersion, const char* sdkVersion);
		~Application();

		std::unique_ptr<Fuel::IMessageTransport> CreateTransport() override;
		Fuel::IMessageDispatcher& Dispatcher() override;

	private:
		void TransportReset(const Fuel::MessageTransportFatalError& error) override;
	};
}