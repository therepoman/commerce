#pragma once

#include "FuelSDKDll.Dispatcher/CMessageDispatcher.h"
#include <memory>
#include <Windows.h>
#include "CommonOverlayTypes.h"

namespace Fuel
{
	class OverlayMessageHandler
	{
	public:
		OverlayMessageHandler();
		~OverlayMessageHandler();
		void sendWindowResize(BufferSize &bufferSize);
		void sendOverlayFailureEvent(IOverlayInstance &instance);
	};
}
