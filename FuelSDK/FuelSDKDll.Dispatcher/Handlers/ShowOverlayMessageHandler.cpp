#include "ShowOverlayMessageHandler.h"

using namespace Fuel;

ShowOverlayMessageHandler::ShowOverlayMessageHandler(const std::shared_ptr<OverlayManager> &overlayManager) : m_overlayManager(overlayManager) {}

ShowOverlayMessageHandler::~ShowOverlayMessageHandler() {}

void ShowOverlayMessageHandler::Handle(IMessageDispatcher& dispatcher, Message& message)
{
	const Messaging::MessageRoot* root = Messaging::GetMessageRoot(message.GetBody().GetBuffer());
	if (root->message_type() != Messaging::Message_ShowOverlay)
	{
		Logger::Instance(FUEL_DLL_LOGGER)->error("ShowOverlayMessageHandler got invalid message type {0}",
			root->message_type());
		return;
	}

	const Messaging::ShowOverlay* update = (const Messaging::ShowOverlay*) root->message();

	RECT rect;
	SetRect(&rect, update->x(), update->y(), update->x() + update->width(), update->y() + update->height());

	m_overlayManager->Show(update->instanceId(), rect, update->z());
}

const char* ShowOverlayMessageHandler::GetName() {
	return "ShowOverlayHandler";
}
