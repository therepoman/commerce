#include "IMessageHandler.h"
#include "OverlayManager.h"
#include "Fuel_generated.h"

namespace Fuel {
	class UpdateCursorMessageHandler : public IMessageHandler {
	public:
		UpdateCursorMessageHandler(const std::shared_ptr<OverlayManager> &overlayManager);
		UpdateCursorMessageHandler(const UpdateCursorMessageHandler&) = delete; // We should never be copying these.
		UpdateCursorMessageHandler& operator=(UpdateCursorMessageHandler rhs) = delete; // Or assigning these.
		~UpdateCursorMessageHandler();

		void Handle(IMessageDispatcher& dispatcher, Message& message);
		const char * GetName();
	private:
		std::shared_ptr<OverlayManager> m_overlayManager;
	};
}