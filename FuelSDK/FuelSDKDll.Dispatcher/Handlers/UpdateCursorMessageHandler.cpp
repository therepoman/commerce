#include "UpdateCursorMessageHandler.h"
#include "logger.h"

using namespace Fuel;

UpdateCursorMessageHandler::UpdateCursorMessageHandler(const std::shared_ptr<OverlayManager> &overlayManager) : m_overlayManager(overlayManager) {}

UpdateCursorMessageHandler::~UpdateCursorMessageHandler() {}

void UpdateCursorMessageHandler::Handle(IMessageDispatcher& dispatcher, Message& message)
{
	const Messaging::MessageRoot* root = Messaging::GetMessageRoot(message.GetBody().GetBuffer());
	if (root->message_type() != Messaging::Message_UpdateCursor)
	{
		Logger::Instance(FUEL_DLL_LOGGER)->error("UpdateCursorMessageHandler got invalid message type {0}",
			root->message_type());
		return;
	}
	
	const Messaging::UpdateCursor* update = (const Messaging::UpdateCursor*) root->message();

	m_overlayManager->UpdateCursor(update->cursor());
}

const char* UpdateCursorMessageHandler::GetName() {
	return "UpdateCursorHandler";
}
