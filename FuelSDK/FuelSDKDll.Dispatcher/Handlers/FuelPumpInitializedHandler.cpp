#include "FuelPumpInitializedHandler.h"
#include "logger.h"

Fuel::FuelPumpInitializedHandler::FuelPumpInitializedHandler()
{
}

Fuel::FuelPumpInitializedHandler::~FuelPumpInitializedHandler()
{
}

void Fuel::FuelPumpInitializedHandler::Handle(IMessageDispatcher& dispatcher, Message& msg)
{
}

const char* Fuel::FuelPumpInitializedHandler::GetName() {
	return "FuelPumpInitializedHandler";
}
