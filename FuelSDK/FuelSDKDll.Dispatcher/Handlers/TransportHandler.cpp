#include "TransportHandler.h"

#include "logger.h"

using namespace Fuel;

TransportHandler::TransportHandler(std::function<void(const Fuel::Message&)> sendMessage) :
	m_sendMessage(sendMessage)
{}

void TransportHandler::Handle(IMessageDispatcher& dispatcher, Message& message)
{
	m_sendMessage(message);
}