#include "IMessageHandler.h"
#include "OverlayManager.h"
#include "Fuel_generated.h"

namespace Fuel {
	class OverlayUpdateWrapper : public IOverlayUpdate
	{
	public:
		explicit OverlayUpdateWrapper(Message&& message);

		bool isValid() const { return m_isValid; };
		uint32_t getInstanceId() const { return m_instanceId; };
		uint32_t getX() const { return m_x; }
		uint32_t getY() const { return m_y; }
		uint32_t getWidth() const { return m_width; };
		uint32_t getHeight() const { return m_height; };

		uint32_t GetDirtyRectCount() const { return m_dirtyRects.size(); }
		const OverlayDirtyRect& GetDirtyRect(int index) const { return m_dirtyRects[index]; }
	private:
		Message m_message;
		bool m_isValid;
		uint32_t m_instanceId;
		uint32_t m_x;
		uint32_t m_y;
		uint32_t m_width;
		uint32_t m_height;
		std::vector<OverlayDirtyRect> m_dirtyRects;
	};

	class UpdateOverlayMessageHandler : public IMessageHandler {
	public:
		UpdateOverlayMessageHandler(const std::shared_ptr<OverlayManager> &overlayManager);
		UpdateOverlayMessageHandler(const UpdateOverlayMessageHandler&) = delete; // We should never be copying these.
		UpdateOverlayMessageHandler& operator=(UpdateOverlayMessageHandler rhs) = delete; // Or assigning these.
		~UpdateOverlayMessageHandler();

		void Handle(IMessageDispatcher& dispatcher, Message& message);
		const char * GetName();
	private:
		std::shared_ptr<OverlayManager> m_overlayManager;
	};
}