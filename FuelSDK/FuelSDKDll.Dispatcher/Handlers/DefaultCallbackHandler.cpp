#include "DefaultCallbackHandler.h"
#include "logger.h"
#include "Fuel_generated.h"
#include "FuelHeader_generated.h"

using namespace Fuel;

void DefaultCallbackHandler::FatalError()
{
	CancelAllPendingRequests();
}

void DefaultCallbackHandler::CancelAllPendingRequests()
{
	// for each message awaiting a callback, create a fatal error Message
	for (auto it = m_completionMap.begin(); it != m_completionMap.end(); ++it)
	{

		std::unique_ptr<flatbuffers::FlatBufferBuilder> fbb(new flatbuffers::FlatBufferBuilder());

		auto error = Fuel::Messaging::CreateFatalError(*fbb);

		auto messageRootBuilder = Fuel::Messaging::MessageRootBuilder(*fbb);
		messageRootBuilder.add_message_type(Fuel::Messaging::Message::Message_FatalError);
		messageRootBuilder.add_message(error.Union());
		auto messageRoot = messageRootBuilder.Finish();

		Fuel::Messaging::FinishMessageRootBuffer(*fbb, messageRoot);

		auto body = new uint8_t[fbb->GetSize()];
		memcpy_s(body, fbb->GetSize(), fbb->GetBufferPointer(), fbb->GetSize());

		it->second(Message(Fuel::Messaging::Message::Message_FatalError,
			fbb->GetSize(),
			body));
	}

	m_completionMap.clear();
}

void DefaultCallbackHandler::Handle(Fuel::IMessageDispatcher& dispatcher, Message& message) {
	const Fuel::Messaging::MessageHeader* messageHeader = Fuel::Messaging::GetMessageHeader(message.GetHeader().GetBuffer());
	std::string requestId = messageHeader->requestId()->str();

	try {
		auto requestPair = m_completionMap.find(requestId);
		if (requestPair != m_completionMap.end()) {
			Logger::Instance(FUEL_DLL_LOGGER)->debug("Calling request callback !");
			requestPair->second(std::move(message)); // Call the callback (it will be the second value in the pair).
		}
		else {
			Logger::Instance(FUEL_DLL_LOGGER)->error("Could not correlate response with id {0} to a pending requestId. Aborting.", requestId);
			return; // Return pre-emptively, no need to erase from map.
		}
	}
	catch (const std::exception& e) {
		Logger::Instance(FUEL_DLL_LOGGER)->error("Callback for requestId {0} threw an exception: {1}. Aborting.", requestId, e.what());
	}

	// Remove it from the map.
	m_completionMap.erase(requestId);
}

void Fuel::DefaultCallbackHandler::AddCallback(std::string id, MessageReceivedFunction callback) {
	m_completionMap[id] = callback;
}
