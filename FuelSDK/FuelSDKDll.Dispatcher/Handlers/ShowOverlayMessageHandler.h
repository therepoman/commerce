#include "IMessageHandler.h"
#include "OverlayManager.h"

#include "../InputHook.h"

namespace Fuel {
	class ShowOverlayMessageHandler : public IMessageHandler {
	public:
		ShowOverlayMessageHandler(const std::shared_ptr<OverlayManager> &overlayManager);
		ShowOverlayMessageHandler(const ShowOverlayMessageHandler&) = delete; // We should never be copying these.
		ShowOverlayMessageHandler& operator=(ShowOverlayMessageHandler rhs) = delete; // Or assigning these.
		~ShowOverlayMessageHandler();

		void Handle(IMessageDispatcher& dispatcher, Message& message);
		const char * GetName();
	private:
		std::shared_ptr<OverlayManager> m_overlayManager;
	};
}