#pragma once

#include <windows.h>
#include "IMessageHandler.h"

namespace Fuel
{
	class FuelPumpInitializedHandler : public IMessageHandler
	{
	public:
		FuelPumpInitializedHandler();
		~FuelPumpInitializedHandler();
		static void SetHookedWindow(HWND hwnd);
		void Handle(IMessageDispatcher& dispatcher, Message& message);
		const char * GetName();
	};
}
