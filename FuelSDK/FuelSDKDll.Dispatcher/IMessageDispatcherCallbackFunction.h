#include <functional>

#include "Message.h"

namespace Fuel {
	typedef std::function<void(Message& message)> MessageReceivedFunction;
}