#include "OverlayMessageHandler.h"
#include "FuelDll.h"
#include "IOverlayInstance.h"

Fuel::OverlayMessageHandler::OverlayMessageHandler()
{
}

Fuel::OverlayMessageHandler::~OverlayMessageHandler()
{
}

void Fuel::OverlayMessageHandler::sendWindowResize(BufferSize &bufferSize)
{
	flatbuffers::FlatBufferBuilder fbb;

	auto setWindowSize = Fuel::Messaging::CreateSetWindowSize(fbb, bufferSize.width, bufferSize.height);

	auto messageRootBuilder = Fuel::Messaging::MessageRootBuilder(fbb);
	messageRootBuilder.add_message_type(Fuel::Messaging::Message::Message_SetWindowSize);
	messageRootBuilder.add_message(setWindowSize.Union());
	auto messageRoot = messageRootBuilder.Finish();

	Fuel::Messaging::FinishMessageRootBuffer(fbb, messageRoot);

	SendRequest(Fuel::Messaging::Message::Message_SetWindowSize, fbb.GetSize(), fbb.GetBufferPointer());
}

void Fuel::OverlayMessageHandler::sendOverlayFailureEvent(IOverlayInstance &instance)
{
	flatbuffers::FlatBufferBuilder fbb;

	auto overlayFailureEvent = Fuel::Messaging::CreateOverlayFailureEvent(fbb, instance.GetInstanceId());

	auto messageRootBuilder = Fuel::Messaging::MessageRootBuilder(fbb);
	messageRootBuilder.add_message_type(Fuel::Messaging::Message::Message_OverlayFailureEvent);
	messageRootBuilder.add_message(overlayFailureEvent.Union());
	auto messageRoot = messageRootBuilder.Finish();

	Fuel::Messaging::FinishMessageRootBuffer(fbb, messageRoot);

	SendRequest(Fuel::Messaging::Message::Message_OverlayFailureEvent, fbb.GetSize(), fbb.GetBufferPointer());
}
