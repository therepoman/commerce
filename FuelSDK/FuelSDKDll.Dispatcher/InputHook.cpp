#include "InputHook.h"
#include "IMessageDispatcher.h"
#include "logger.h"

#include <windows.h>

// Cast to short to preserve negative locations.
// Windows will return negative numbers for the title bar and upper left corner of the window.
#define DELTA_Y(l) (short) HIWORD(l)

using namespace Fuel::Input;

LRESULT CALLBACK KeyboardHookCallback(int code, WPARAM wParam, LPARAM lParam)
{
	return InputHook::Instance()->HandleKeyboardMessage(code, wParam, lParam);
}

LRESULT CALLBACK MouseHookCallback(int code, WPARAM wParam, LPARAM lParam)
{
	return InputHook::Instance()->HandleMouseMessage(code, wParam, lParam);
}

LRESULT InputHook::HandleKeyboardMessage(int code, WPARAM wParam, LPARAM lParam)
{
	if (code < 0)
	{
		return CallNextHookEx(NULL, code, wParam, lParam);
	}

	using Fuel::Messaging::KeyEventType;

	OverlayDesc overlayDesc;
	if (GetOverlayWithFocus(overlayDesc))
	{
		unsigned long transitionFlag = 1 << 31;
		UINT scanCode = MapVirtualKey((UINT) wParam, MAPVK_VK_TO_VSC);
		BYTE keyboardState[256];
		char asciiCode[2] = { 0, 0 };
		bool isChar, ctrlDown;

		// First half checks the keyboard state to determine the ascii value of a key pressed.
		// Used to determine between lowercase and uppercase letters.
		// Second half ensures that alt+F4 is not blocked and sent to the game.
		if (GetKeyboardState(keyboardState) && !(HIWORD(GetAsyncKeyState(VK_MENU)) > PRESSED_THRESHOLD && HIWORD(GetAsyncKeyState(VK_F4)) > PRESSED_THRESHOLD))
		{
			isChar = (ToAscii((UINT) wParam, scanCode, keyboardState, (LPWORD)asciiCode, 0) > PRESSED_THRESHOLD);
			ctrlDown = (HIWORD(GetAsyncKeyState(VK_CONTROL)) > PRESSED_THRESHOLD);
		}
		else
		{
			return CallNextHookEx(NULL, code, wParam, lParam);
		}

		// Checks the transitionFlag to see if the key is being pressed or released.
		if (HIWORD(GetAsyncKeyState(VK_ESCAPE)) > PRESSED_THRESHOLD)
		{
			SendCloseOverlayEvent(overlayDesc.instanceId);
		}
		else if (lParam & transitionFlag)
		{
			SendKeyboardEvent(overlayDesc.instanceId, KeyEventType::KeyEventType_KeyUp, wParam, ctrlDown);
		}
		else
		{
			SendKeyboardEvent(overlayDesc.instanceId, KeyEventType::KeyEventType_KeyDown, wParam, ctrlDown);

			if (isChar)
			{
				SendKeyboardEvent(overlayDesc.instanceId, KeyEventType::KeyEventType_Char, asciiCode[0], ctrlDown);
			}
		}
	}

	return 1;
}

LRESULT InputHook::HandleMouseMessage(int code, WPARAM wParam, LPARAM lParam)
{
	if (code < 0)
	{
		return CallNextHookEx(NULL, code, wParam, lParam);
	}

	using Fuel::Messaging::MouseButton;

	MOUSEHOOKSTRUCT * mHook = (MOUSEHOOKSTRUCT *)lParam;
	POINT point = mHook->pt;

	if (!ScreenToClient(mHook->hwnd, &point))
	{
		return CallNextHookEx(NULL, code, wParam, lParam);
	}

	RECT rect;
	GetClientRect(mHook->hwnd, &rect);

	// Checks to see if the cursor is within the client window.
	if (point.y < rect.top || point.y > rect.bottom || point.x < rect.left || point.x > rect.right)
		return CallNextHookEx(NULL, code, wParam, lParam);

	if (m_overlayManager)
	{
		BufferSize bufferSize;
		if (m_overlayManager->GetBufferSize(bufferSize))
		{
			LONG width = rect.right - rect.left;
			LONG height = rect.bottom - rect.top;
			
			// If the game doesn't properly resize the DirectX buffer when the window resizes, try to adjust
			// the mouse events to match.  This will fix it for some cases (resize while overlay is visible)
			// and just make it closer for others (show overlay after resize).
			// The difference may be coming from the orthographic projection or something else within DirectX,
			// because the buffer size and vertex positions don't appear to change between the two cases.
			// Might need to cache the client area size at the time the projection is initialized and use that
			// to properly scale around the screen center.
			// TODO: https://twitchtv.atlassian.net/browse/FUEL-1124
			if (bufferSize.width != width || bufferSize.height != height)
			{
				point.x = (LONG)((double)point.x * (double)bufferSize.width / (double)width);
				point.y = (LONG)((double)point.y * (double)bufferSize.height / (double)height);
			}
		}
	}

	switch (wParam)
	{
	case WM_LBUTTONDOWN:
		CheckDoubleClick(point);
		SendMouseEvent(point.x, point.y, MouseButton::MouseButton_Left, false, clicks);
		break;
	case WM_LBUTTONUP:
		SendMouseEvent(point.x, point.y, MouseButton::MouseButton_Left, true, SINGLE_CLICK);
		break;
	case WM_MBUTTONDOWN:
		clicks = 0; // Reset counter as it no longer a left double click
		SendMouseEvent(point.x, point.y, MouseButton::MouseButton_Middle, false, SINGLE_CLICK);
		break;
	case WM_MBUTTONUP:
		clicks = 0; // Reset counter as it no longer a left double click
		SendMouseEvent(point.x, point.y, MouseButton::MouseButton_Middle, true, SINGLE_CLICK);
		break;
	case WM_RBUTTONDOWN:
		clicks = 0; // Reset counter as it no longer a left double click
		SendMouseEvent(point.x, point.y, MouseButton::MouseButton_Right, false, SINGLE_CLICK);
		break;
	case WM_RBUTTONUP:
		clicks = 0; // Reset counter as it no longer a left double click
		SendMouseEvent(point.x, point.y, MouseButton::MouseButton_Right, true, SINGLE_CLICK);
		break;
	case WM_MOUSEMOVE:
		SendMouseMoveEvent(point.x, point.y, (HIWORD(GetAsyncKeyState(VK_LBUTTON)) > PRESSED_THRESHOLD));
		return CallNextHookEx(NULL, code, wParam, lParam);
		break;
	case WM_MOUSEWHEEL:
		SendMouseWheelEvent(point.x, point.y, 0, DELTA_Y(((MOUSEHOOKSTRUCTEX *)lParam)->mouseData));
		break;
	default:
		return CallNextHookEx(NULL, code, wParam, lParam);
	}

	// Gives focus to the window if the focus was lost.
	if (GetFocus() != mHook->hwnd)
		SetFocus(mHook->hwnd);

	return 1;
}

InputHook::InputHook() :
	m_coordinateConversionLoggingLimiter(20, FUEL_DLL_LOGGER,
		"Too many coordinate conversion errors, disabling logging of this message.")
{
}

InputHook::~InputHook()
{
	EndHook();
}

std::shared_ptr<InputHook> InputHook::Instance() {
	static std::weak_ptr<InputHook> s_instance;
	static std::mutex s_initializationMutex;

	std::shared_ptr<InputHook> result = s_instance.lock();
	if (!result)
	{
		std::lock_guard<std::mutex> lock(s_initializationMutex);
		std::shared_ptr<InputHook> instanceAfterLock = s_instance.lock();
		if (instanceAfterLock)
		{
			result = std::move(instanceAfterLock);
		}
		else
		{
			std::shared_ptr<InputHook> newInstance(new InputHook());
			result = std::move(newInstance);
			s_instance = result;
		}
	}
	return std::move(result);
}

void InputHook::SetOverlayManager(const std::shared_ptr<Fuel::OverlayManager> &overlayManager)
{
	m_overlayManager = overlayManager;
}

void InputHook::InitHook(HWND hwnd) {
	EndHook();

	Logger::Instance(FUEL_DLL_LOGGER)->debug("Hooking input !");
	hookedWindowThreadId = GetWindowThreadProcessId(hwnd, NULL);
	keyboardHook = SetWindowsHookEx(WH_KEYBOARD, (HOOKPROC)KeyboardHookCallback, NULL, hookedWindowThreadId);
	mouseHook = SetWindowsHookEx(WH_MOUSE, (HOOKPROC)MouseHookCallback, NULL, hookedWindowThreadId);
	m_hWnd = hwnd;
}

void InputHook::SetDispatcher(const std::shared_ptr<Fuel::IMessageDispatcher> oDispatcher) {
	m_dispatcher = oDispatcher;
}

void InputHook::EndHook() {
	if (keyboardHook || mouseHook)
	{
		Logger::Instance(FUEL_DLL_LOGGER)->debug("Unhooking input.");
	}

	if (keyboardHook)
	{
		UnhookWindowsHookEx(keyboardHook);
		keyboardHook = NULL;
	}

	if (mouseHook)
	{
		UnhookWindowsHookEx(mouseHook);
		mouseHook = NULL;
	}
}

void InputHook::SendKeyboardEvent(int instanceId, Fuel::Messaging::KeyEventType keyEventType, WPARAM keyCode, bool ctrlDown)
{
	flatbuffers::FlatBufferBuilder fbb;
	auto event = Fuel::Messaging::CreateKeyboardEvent(fbb, instanceId, keyEventType, keyCode, ctrlDown);
	SendMessage(fbb, event.Union(), Fuel::Messaging::Message::Message_KeyboardEvent);
}

void InputHook::SendMouseEvent(int x, int y, Fuel::Messaging::MouseButton button, bool mouseUp, int clickCount)
{
	OverlayDesc overlayDesc;
	if (GetOverlayAtLocation(x, y, overlayDesc))
	{
		m_overlayInstanceIdWithFocus = overlayDesc.instanceId;

		flatbuffers::FlatBufferBuilder fbb;
		auto event = Fuel::Messaging::CreateMouseEvent(fbb, overlayDesc.instanceId, x - overlayDesc.location.left, y - overlayDesc.location.top, button, mouseUp, clickCount);
		SendMessage(fbb, event.Union(), Fuel::Messaging::Message::Message_MouseEvent);
	}
}

void InputHook::SendMouseMoveEvent(int x, int y, bool leftMouseDown)
{
	OverlayDesc overlayDesc;
	if (GetOverlayAtLocation(x, y, overlayDesc))
	{
		flatbuffers::FlatBufferBuilder fbb;
		auto event = Fuel::Messaging::CreateMouseMoveEvent(fbb, overlayDesc.instanceId, x - overlayDesc.location.left, y - overlayDesc.location.top, leftMouseDown);
		SendMessage(fbb, event.Union(), Fuel::Messaging::Message::Message_MouseMoveEvent);
	}
}

void InputHook::SendMouseWheelEvent(int x, int y, int deltaX, int deltaY)
{
	OverlayDesc overlayDesc;
	if (GetOverlayAtLocation(x, y, overlayDesc))
	{
		flatbuffers::FlatBufferBuilder fbb;
		auto event = Fuel::Messaging::CreateMouseWheelEvent(fbb, overlayDesc.instanceId, x - overlayDesc.location.left, y - overlayDesc.location.top, deltaX, deltaY);
		SendMessage(fbb, event.Union(), Fuel::Messaging::Message::Message_MouseWheelEvent);
	}
}

void InputHook::SendCloseOverlayEvent(int instanceId)
{
	flatbuffers::FlatBufferBuilder fbb;
	auto event = Fuel::Messaging::CreateCloseOverlayEvent(fbb, instanceId);
	SendMessage(fbb, event.Union(), Fuel::Messaging::Message::Message_CloseOverlayEvent);
}

void InputHook::SendMessage(flatbuffers::FlatBufferBuilder &fbb, flatbuffers::Offset<void> &table, Fuel::Messaging::Message messageType)
{
	auto messageRootBuilder = Fuel::Messaging::MessageRootBuilder(fbb);
	messageRootBuilder.add_message_type(messageType);
	messageRootBuilder.add_message(table);
	auto messageRoot = messageRootBuilder.Finish();

	Fuel::Messaging::FinishMessageRootBuffer(fbb, messageRoot);	

	if (m_dispatcher) {
		uint8_t* buffer = new uint8_t[fbb.GetSize()];
		memcpy(buffer, fbb.GetBufferPointer(), fbb.GetSize());

		m_dispatcher->HandleMessage(Message(messageType, fbb.GetSize(), buffer));
	}
	else {
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("No dispatcher setup to send input hook messages to. Ignoring hooked message");
	}
}

bool InputHook::GetOverlayAtLocation(int x, int y, Fuel::OverlayDesc &overlayDesc)
{
	if (m_overlayManager)
	{
		return m_overlayManager->GetOverlayAtLocation(x, y, overlayDesc);
	}

	return false;
}

bool InputHook::GetOverlayWithFocus(Fuel::OverlayDesc &overlayDescOut)
{
	if (m_overlayManager)
	{
		OverlayDesc overlayDesc;
		if (m_overlayManager->GetOverlay(m_overlayInstanceIdWithFocus, overlayDesc))
		{
			overlayDescOut = overlayDesc;
			return true;
		}

		if (m_overlayManager->GetFirstOverlay(overlayDesc))
		{
			overlayDescOut = overlayDesc;
			return true;
		}
	}

	return false;
}

void InputHook::CheckDoubleClick(POINT point)
{
	int currTime = (int)GetTickCount();
	if ((currTime - lastClickTime) < (int) GetDoubleClickTime() && clicks < TRIPLE_CLICK)
	{
		int doubleClickWidth = GetSystemMetrics(SM_CXDOUBLECLK) / 2;
		int doubleClickHeight = GetSystemMetrics(SM_CYDOUBLECLK) / 2;
		RECT rect = { lastClickPoint.x - doubleClickWidth,
			lastClickPoint.y - doubleClickHeight,
			lastClickPoint.x + doubleClickWidth,
			lastClickPoint.y + doubleClickHeight };

		// Check if the most recent click was in the double click area of the previous click
		if (point.y >= rect.top && point.y <= rect.bottom && point.x >= rect.left && point.x <= rect.right)
		{
			clicks++;
		}
		else
		{
			clicks = SINGLE_CLICK;
		}
	}
	else
	{
		clicks = SINGLE_CLICK;
	}

	lastClickTime = currTime;
	lastClickPoint = point;
}
