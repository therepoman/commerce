#pragma once

#include <thread>
#include <mutex>

#include "IMessageDispatcher.h"

namespace Fuel
{
	struct IMessageTransport;
	class MessageTransportFatalError;

	struct IRestartStrategy
	{
		virtual ~IRestartStrategy() {}

		virtual int InitializeTimeoutMilliseconds() = 0;
		virtual void TransportReset() = 0;
		virtual bool ShouldRestart() const = 0;
	};

	class MessagePump
	{
		Fuel::Message m_initMessage;

		bool m_isPolling;
		std::unique_ptr<Fuel::IMessageTransport> m_transport;
		std::unique_ptr<std::thread> m_pollingThread;

		std::mutex m_transportMutex;

		std::unique_ptr<IRestartStrategy> m_restartStrategy;
	public:
		MessagePump(std::unique_ptr<IRestartStrategy>&& restartStrategy, const char* vendorId, const char* gameVersion, const char* sdkVersion);
		virtual ~MessagePump();

		void SendRequest(const Fuel::Message& message);
		void SendRequestResponse(const Fuel::Message& message, std::function<void(int, uint32_t, const uint8_t*)> callback);

	protected:
		void Start();
		void Stop();

		virtual std::unique_ptr<Fuel::IMessageTransport> CreateTransport() = 0;

		virtual Fuel::IMessageDispatcher& Dispatcher() = 0;

		virtual void TransportReset(const Fuel::MessageTransportFatalError& error) = 0;
	private:
		void StartTransportIfNeeded();
		void StartInternal();
		void TransportFailure(const Fuel::MessageTransportFatalError& error);
		void PollingLoop();

		static Fuel::Message CreateShutdownMessage();
		static Fuel::Message CreateInitializeMessage(const char* vendorId, const char* gameVersion, const char* sdkVersion);
	};
}
