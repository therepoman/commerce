﻿using FuelPump.Metrics;
using FuelPump.Services.Config;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using System;

namespace FuelPump.Tests
{
    public class MockConfigHandler
    {
        public Mock<ConfigHandler> Mock { get; private set; }

        public MockConfigHandler()
        {
            Mock = new Mock<ConfigHandler>(new Lazy<MetricEventFactory>(() => new MockMetricsFactory().Factory.Object));

            var bootstrapConfig = GetRealBootstrapConfig();
            Mock.Protected().Setup<string>("GetBootstrapConfig").Returns(JsonConvert.SerializeObject(bootstrapConfig));
            Mock.Setup(mock => mock.GetConfig()).Returns(bootstrapConfig);
        }

        public void AllowAllHttpsUris()
        {
            var filter = new UriFilter();
            filter.scheme = "https";

            var bootstrapConfig = GetRealBootstrapConfig();
            bootstrapConfig.overlayUriWhitelist.Add(filter);
            Mock.Setup(mock => mock.GetConfig()).Returns(bootstrapConfig);
        }

        public static FuelConfig GetRealBootstrapConfig()
        {
            var config = JsonConvert.DeserializeObject<FuelConfig>(new GetRealBootstrapConfigHelper().GetBootstrapConfig());
            config.AssertValid();
            return config;
        }

        /// <summary>
        /// The FuelPump.Tests project doesn't have access to the FuelPump project's resources,
        /// so use ConfigHandler's protected getter to get the real bootstrap config.
        /// </summary>
        private class GetRealBootstrapConfigHelper : ConfigHandler
        {
            public GetRealBootstrapConfigHelper() : base(new Lazy<MetricEventFactory>(() => new MockMetricsFactory().Factory.Object))
            {
            }

            public new string GetBootstrapConfig()
            {
                return base.GetBootstrapConfig();
            }
        }
    }
}
