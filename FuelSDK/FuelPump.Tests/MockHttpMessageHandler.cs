﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Tests
{
    public class MockHttpMessageHandler : HttpMessageHandler
    {
        public List<HttpRequestMessage> Requests { get; private set; }
        private Queue<TaskCompletionSource<HttpResponseMessage>> responses = new Queue<TaskCompletionSource<HttpResponseMessage>>();

        public MockHttpMessageHandler()
        {
            Requests = new List<HttpRequestMessage>();
        }

        public TaskCompletionSource<HttpResponseMessage> SetupResponse()
        {
            var source = new TaskCompletionSource<HttpResponseMessage>();
            responses.Enqueue(source);
            return source;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            Requests.Add(request);
            if (responses.Count == 0)
            {
                throw new ApplicationException("No response was configured.");
            }
            return responses.Dequeue().Task;
        }
    }
}
