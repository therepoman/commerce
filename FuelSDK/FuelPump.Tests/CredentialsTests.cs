﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;

using FuelPump.Providers.Credentials;
using FuelPump.Helpers;
using Amazon.Auth.Map;

using Messages = Fuel.Messaging;

namespace FuelPump.Tests
{

    [TestClass]
    public class CredentialsTests
    {
        [TestMethod]
        public void BearerCredentials_BearerCredentialsInitialization()
        {
            string[] stringsToTest = { null, "string1" };
            foreach (string s1 in stringsToTest)
            {
                foreach (string s2 in stringsToTest)
                {
                    foreach (string s3 in stringsToTest)
                    {
                        BearerCredentials c = new BearerCredentials(s1, s2, s3);
                        Assert.AreEqual(s1, c.Type);
                        Assert.AreEqual(s2, c.Identifier);
                        Assert.AreEqual(s3, c.Token);
                    }
                }
            }
        }

        [TestMethod]
        public void BearerCredentials_Equality()
        {
            BearerCredentials a = new BearerCredentials("foo", "bar", "baz");
            BearerCredentials b = new BearerCredentials("foo", "bar", "baz");
            Assert.IsTrue(a.Equals(b));

            b = new BearerCredentials("foo", "bar", "argleblargle");
            Assert.IsFalse(a.Equals(b));

            b = new BearerCredentials("foo", "argleblargle", "baz");
            Assert.IsFalse(a.Equals(b));

            b = new BearerCredentials("argleblargle", "bar", "baz");
            Assert.IsFalse(a.Equals(b));
        }

        [TestMethod]
        public void AmazonCredentials_AmazonCredentials()
        {
            string refreshTokenString = "refresh token string";
            AmazonCredentials c = new AmazonCredentials(refreshTokenString);
            Assert.AreEqual(refreshTokenString, c.RefreshToken);
        }
    }

    // Mock the behavior of the token provider.
    public class FakeTokenProvider : ITokenProvider
    {
        public AmazonRefreshToken RefreshTokenToGet { set; private get; }
        public AmazonRefreshToken RefreshTokenToSet { get; private set; }

        public FakeTokenProvider() { }

        public AmazonRefreshToken GetRefreshToken()
        {
            return RefreshTokenToGet;
        }

        public void SetRefreshToken(AmazonRefreshToken token) 
        {
            RefreshTokenToSet = token;
        }

        public AmazonAccessToken GetAccessToken() { return null; }
        public void SetAccessToken(AmazonAccessToken token) { }
        public void RemoveStoredTokens()
        {
            RefreshTokenToSet = null;
        }
    }

    [TestClass]
    public class CredentialsProviderTests
    {
        private FuelPump.Metrics.MetricEventFactory mockMetrics;
        private Mock<RegistryCredentialsProvider> mockRegistryCredentialsProvider;
        private FakeTokenProvider fakeTokenProvider;
        private AppInfo appInfo;

        private CredStoreContents manuallyCreatedFileContents;
        private string serializedManuallyCreatedFileContents;

        private Credentials testCredentials;
        private AmazonCredentials amazonCredentials;
        private string amazonOauthCredentialsString = "amazon oauth credentials";

        private BearerCredentials identifyingCredentials;
        private string identifyingCredentialsTokenString = "other credentials";
        private string identifyingCredentialsTypeString = "other";
        private string identifyingCredentialsIdentifierString = "identifier";

        [TestInitialize]
        public void setup()
        {
            // Set up fake metrics
            var mockMetricsQueue = new Moq.Mock<FuelPump.Metrics.IMetricQueue>();
            mockMetrics = new FuelPump.Metrics.MetricEventFactory("", "", mockMetricsQueue.Object);

            // Set up some fake credentials.
            amazonCredentials = new AmazonCredentials(amazonOauthCredentialsString);
            testCredentials = new Credentials(amazonCredentials);

            identifyingCredentials = new BearerCredentials(identifyingCredentialsTypeString, identifyingCredentialsIdentifierString, identifyingCredentialsTokenString); 

            // Initialize the AmazonAuthMap token provider fake.  Individual unit tests will 
            // specify what it should get, if necessary.
            fakeTokenProvider = new FakeTokenProvider();

            mockRegistryCredentialsProvider = new Mock<RegistryCredentialsProvider>(mockMetrics);
            mockRegistryCredentialsProvider.CallBase = true;

            // Return a default AppInfo unless we want to swap out values later.
            appInfo = new AppInfo();

            // Mock out the method to retrieve the fake token provider.
            mockRegistryCredentialsProvider.Protected().Setup<ITokenProvider>("GetTokenProvider", ItExpr.IsAny<AppInfo>()).Returns(fakeTokenProvider);

            // Also mock the returning of AppInfo so that we can make the current system look different.
            mockRegistryCredentialsProvider.Protected().Setup<AppInfo>("GetAppInfo").Returns(appInfo);

            // Create a testing FileContents object and matching serialized data.
            manuallyCreatedFileContents = new CredStoreContents();
            manuallyCreatedFileContents.AmazonCredentials = amazonCredentials;
            manuallyCreatedFileContents.Location = "file location";
            manuallyCreatedFileContents.SerialNumber = "serial number";
            manuallyCreatedFileContents.Username = "user name";

            serializedManuallyCreatedFileContents = manuallyCreatedFileContents.Serialize();
        }

        [TestMethod]
        [ExpectedException(typeof(CredentialsNotFoundException))]
        public void RegistryCredentialsProvider_GetCredsInvalidIdentifier()
        {
            mockRegistryCredentialsProvider.Object.GetCredentials(null);
        }

        [TestMethod]
        public void RegistryCredentialsProvider_GetCredsHappyCase()
        {
            AmazonRefreshToken refreshToken = new AmazonRefreshToken(serializedManuallyCreatedFileContents);
            fakeTokenProvider.RefreshTokenToGet = refreshToken;

            // Mock things up so that we appear to generate file contents matching what was retrieved in serialized form.
            mockRegistryCredentialsProvider.Protected().Setup<CredStoreContents>("GenerateContents", ItExpr.IsAny<BearerCredentials>(), ItExpr.IsAny<Credentials>()).Returns(manuallyCreatedFileContents);

            // Invoke.
            var returnedCreds = mockRegistryCredentialsProvider.Object.GetCredentials(new BearerCredentials("does not matter", "totally doesn't matter", "still doesn't matter"));

            // Make sure the creds match the mocked FileContents versions.
            Assert.AreEqual(amazonCredentials.RefreshToken, returnedCreds.AmazonCredentials.RefreshToken);
        }

        [TestMethod]
        public void RegistryCredentialsProvider_SetCredsHappyCase()
        {
            // Invoke.
            mockRegistryCredentialsProvider.Object.SetCredentials(identifyingCredentials, testCredentials);

            // Deserialize the FileContents that were passed to the fake token provider.
            string serializedContents = fakeTokenProvider.RefreshTokenToSet.Value;
            CredStoreContents storedContents = JsonConvert.DeserializeObject<CredStoreContents>(serializedContents, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });

            Assert.IsNotNull(storedContents.AmazonCredentials);
        }

        [TestMethod]
        public void RegistryCredentialsProvider_RemoveCredsHappyCase()
        {
            // Store the token that will get removed.
            fakeTokenProvider.SetRefreshToken(new AmazonRefreshToken("test"));
            Assert.IsNotNull(fakeTokenProvider.RefreshTokenToSet);

            // Invoke.
            mockRegistryCredentialsProvider.Object.RemoveCredentials(identifyingCredentials);

            // Verify it was removed.
            Assert.IsNull(fakeTokenProvider.RefreshTokenToSet);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCredentialsException))]
        public void RegistryCredentialsProvider_GetCredsInvalidContent()
        {
            AmazonRefreshToken refreshToken = new AmazonRefreshToken(serializedManuallyCreatedFileContents);
            fakeTokenProvider.RefreshTokenToGet = refreshToken;

            // Make it look like the generated file contents are different from what was looked up.
            manuallyCreatedFileContents.Location = "somewhere else";
            mockRegistryCredentialsProvider.Protected().Setup<CredStoreContents>("GenerateContents", ItExpr.IsAny<BearerCredentials>(), ItExpr.IsAny<Credentials>()).Returns(manuallyCreatedFileContents);

            // Invoke.
            mockRegistryCredentialsProvider.Object.GetCredentials(new BearerCredentials("does not matter", "totally doesn't matter", "still doesn't matter"));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCredentialsException))]
        public void RegistryCredentialsProvider_SetCredsBadIdentifierNull()
        {
            new RegistryCredentialsProvider(null).SetCredentials(null, new Credentials(null));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCredentialsException))]
        public void RegistryCredentialsProvider_SetCredsNullCreds()
        {
            new RegistryCredentialsProvider(null).SetCredentials(new BearerCredentials("does not matter", "totally doesn't matter", "still doesn't matter"), null);
        }
    }

    [TestClass]
    public class CredStoreContentsTests
    {
        private CredStoreContents fc1;
        private CredStoreContents fc2;

        [TestInitialize]
        public void setup()
        {
            // Initialize both FileContents to be identical.
            fc1 = new CredStoreContents();
            fc2 = new CredStoreContents();
            fc1.AmazonCredentials = fc2.AmazonCredentials = new AmazonCredentials("refresh token");
            fc1.Location = fc2.Location = "file location";
            fc1.SerialNumber = fc2.SerialNumber = "serial number";
            fc1.Username = fc2.Username = "user name";
        }

        [TestMethod]
        public void CredStoreContents_VerifyMetadata_HappyCase()
        {
            fc1.VerifyMetadata(fc2);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCredentialsException))]
        public void CredStoreContents_VerifyMetadata_Username()
        {
            fc2.Username = "Jeff Bezos";
            fc1.VerifyMetadata(fc2);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCredentialsException))]
        public void CredStoreContents_VerifyMetadata_FileLocation()
        {
            fc2.Location = "c:\\the_moon";
            fc1.VerifyMetadata(fc2);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCredentialsException))]
        public void CredStoreContents_VerifyMetadata_SerialNumber()
        {
            fc2.SerialNumber = "not actually a serial number";
            fc1.VerifyMetadata(fc2);
        }

        [TestMethod]
        public void CredStoreContents_VerifyCredentials_HappyCase()
        {
            fc1.VerifyCredentials();
        }

        [TestMethod]
        public void CredStoreContents_VerifyCredentials_OnlyRefreshToken()
        {
            fc1.AmazonCredentials = new AmazonCredentials("refresh token");
            fc1.VerifyCredentials();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCredentialsException))]
        public void CredStoreContents_VerifyCredentials_NullAmazonCredentials()
        {
            fc1.AmazonCredentials = null;
            fc1.VerifyCredentials();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCredentialsException))]
        public void CredStoreContents_VerifyCredentials_InvalidRefreshToken()
        {
            fc1.AmazonCredentials = new AmazonCredentials("");
            fc1.VerifyCredentials();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCredentialsException))]
        public void CredStoreContents_VerifyCredentials_WhitespaceRefreshToken()
        {
            fc1.AmazonCredentials = new AmazonCredentials(" ");
            fc1.VerifyCredentials();
        }
    }

    [TestClass]
    public class HandlerCredentialsHelperTests
    {
        private string identifier = "identifier";
        private string type = "type";
        private string token = "token";
        BearerCredentials credentials; 

        [TestInitialize]
        public void setup()
        {
            credentials = new BearerCredentials(type, identifier, token);
        }

        private Messages.CredentialsCache CreateCredentialsCache(BearerCredentials[] creds)
        {
            FlatBuffers.FlatBufferBuilder builder = new FlatBuffers.FlatBufferBuilder(1);

            FlatBuffers.Offset<Messages.Credential>[] credentialsOffsets = new FlatBuffers.Offset<Messages.Credential>[creds.Length];
            for (int i = 0; i < creds.Length; i++)
            {
                FlatBuffers.Offset<Fuel.Messaging.Credential> messageCredentialOffset =
                Fuel.Messaging.Credential.CreateCredential(builder,
                builder.CreateString(creds[i].Type), builder.CreateString(creds[i].Identifier), builder.CreateString(creds[i].Token));

                credentialsOffsets[i] = messageCredentialOffset;
            }

            FlatBuffers.Offset<Messages.CredentialsCache> credsCacheOffset = Messages.CredentialsCache.CreateCredentialsCache(builder,
                    Messages.CredentialsCache.CreateCredentialsVector(builder, credentialsOffsets));

            builder.Finish(credsCacheOffset.Value);

            return Messages.CredentialsCache.GetRootAsCredentialsCache(builder.DataBuffer);
        }

        [TestMethod]
        public void HandlerCredentialsHelperTests_CredentialsExtracted()
        {
            BearerCredentials extractedCreds = HandlerCredentialsHelper.ExtractSingleCredentials(CreateCredentialsCache(new BearerCredentials[] {credentials}));
            Assert.AreEqual(identifier, extractedCreds.Identifier);
            Assert.AreEqual(type, extractedCreds.Type);
            Assert.AreEqual(token, extractedCreds.Token);
        }

        [TestMethod]
        public void HandlerCredentialsHelperTests_ZeroCredentials()
        {
            BearerCredentials extractedCreds = HandlerCredentialsHelper.ExtractSingleCredentials(CreateCredentialsCache(new BearerCredentials[] {}));
            Assert.IsNull(extractedCreds);
        }

        [TestMethod]
        [ExpectedException(typeof(UnsupportedCredentialsException))]
        public void HandlerCredentialsHelperTests_TooManyCredentials()
        {
            BearerCredentials extractedCreds = HandlerCredentialsHelper.ExtractSingleCredentials(CreateCredentialsCache(new BearerCredentials[] { credentials, credentials }));
        }

        [TestMethod]
        [ExpectedException(typeof(UnsupportedCredentialsException))]
        public void HandlerCredentialsHelperTests_MissingType()
        {
            HandlerCredentialsHelper.ExtractSingleCredentials(CreateCredentialsCache(new BearerCredentials[] { new BearerCredentials(" ", "test", "test") }));
        }

        [TestMethod]
        [ExpectedException(typeof(UnsupportedCredentialsException))]
        public void HandlerCredentialsHelperTests_TypeIsTooLong()
        {
            HandlerCredentialsHelper.ExtractSingleCredentials(CreateCredentialsCache(new BearerCredentials[] { new BearerCredentials(new string('x', 101) , "test", "test") }));
        }

        [TestMethod]
        [ExpectedException(typeof(UnsupportedCredentialsException))]
        public void HandlerCredentialsHelperTests_InvalidType()
        {
            HandlerCredentialsHelper.ExtractSingleCredentials(CreateCredentialsCache(new BearerCredentials[] { new BearerCredentials("test\n", "test", "test") }));
        }

        [TestMethod]
        public void HandlerCredentialsHelperTests_ValidType()
        {
            HandlerCredentialsHelper.ExtractSingleCredentials(CreateCredentialsCache(new BearerCredentials[] { new BearerCredentials("Test1!#$%&'*+-.^_`|~", "test", "test") }));
        }

        [TestMethod]
        [ExpectedException(typeof(UnsupportedCredentialsException))]
        public void HandlerCredentialsHelperTests_MissingIdentifier()
        {
            HandlerCredentialsHelper.ExtractSingleCredentials(CreateCredentialsCache(new BearerCredentials[] { new BearerCredentials("test", " ", "test") }));
        }

        [TestMethod]
        [ExpectedException(typeof(UnsupportedCredentialsException))]
        public void HandlerCredentialsHelperTests_MissingToken()
        {
            HandlerCredentialsHelper.ExtractSingleCredentials(CreateCredentialsCache(new BearerCredentials[] { new BearerCredentials("test", "test", " ") }));
        }

        [TestMethod]
        public void HandlerCredentialsHelperTests_ValidToken()
        {
            HandlerCredentialsHelper.ExtractSingleCredentials(CreateCredentialsCache(new BearerCredentials[] { new BearerCredentials("test", "test", "test \t") }));
            for (int i = 0x21; i <= 0x7E; i++)
            {
                HandlerCredentialsHelper.ExtractSingleCredentials(CreateCredentialsCache(new BearerCredentials[] { new BearerCredentials("test", "test", new string((char)i, 1)) }));
            }
        }
    }
}
