﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using System.Threading;
using Messages = Fuel.Messaging;
using System.IO;

namespace FuelPump.Tests
{
    [TestClass]
    public class MessagePumpTests
    {
        MessagePump CreateMessagePump(IMessageTransport transport, MessageDispatcher dispatcher, FuelPump.Metrics.IMetricQueue metricsQueue = null)
        {
            var mockMetrics = metricsQueue ?? (new Mock<FuelPump.Metrics.IMetricQueue>()).Object;
            var mockMonitoring = new Mock<FuelPump.Monitoring.IMonitoringProvider>();
            var context = new ApplicationContext();
            context.VendorId = "MockVendorId";

            return new MessagePump(
                transport, 
                dispatcher, 
                new FuelPump.Metrics.MetricEventFactory("", "", mockMetrics),
                mockMonitoring.Object,
                context);
        }

        [TestMethod]
        public async Task MessagePump_StartAndStop()
        {
            var metricQueue = new Metrics.MockMetricsQueue();
            var exception = new ApplicationException();

            // setup the dispatcher
            var dispatcher = new MessageDispatcher();
            dispatcher.RegisterHandler<Messages.ShowOverlay>((MessageContext context, Message message) =>
            {
                return Task.FromResult(true);
            });

            // setup the read message mock response
            var transport = new Mock<IMessageTransport>();
            var readTaskSource = new TaskCompletionSource<Message>();
            transport
                .Setup(t => t.ReadMessage(Moq.It.IsAny<CancellationToken>()))
                .Returns(readTaskSource.Task);

            // start and stop the fuel pump
            using (var fuelPump = CreateMessagePump(transport.Object, dispatcher, metricQueue))
            {
                var fuelPumpTask = fuelPump.Start();

                fuelPump.Stop();
                
                // simulate a simple read
                readTaskSource.SetResult(CreateTestMessage());

                // wait for the task to complete and verify it was completed
                Assert.IsTrue(await fuelPumpTask);
            }

            // validate metrics
            Assert.IsTrue(metricQueue.HasDataPoint(FuelPumpMetrics.Startup));
            Assert.IsFalse(metricQueue.HasDataPoint(FuelPumpMetrics.UnhandledException));
            Assert.IsTrue(metricQueue.HasDataPoint(FuelPumpMetrics.Shutdown));
        }

        [TestMethod]
        public async Task MessagePump_StartAndStop_FailedHandler()
        {
            var metricQueue = new Metrics.MockMetricsQueue();
            var exception = new ApplicationException();

            // setup the dispatcher
            var dispatcher = new MessageDispatcher();
            dispatcher.RegisterHandler<Fuel.Messaging.ShowOverlay>((MessageContext context, Message message) =>
            {
                throw exception;
            });
            
            // setup the read message mock response
            var transport = new Mock<IMessageTransport>();
            var readTaskSource = new TaskCompletionSource<Message>();
            transport
                .Setup(t => t.ReadMessage(Moq.It.IsAny<CancellationToken>()))
                .Returns(readTaskSource.Task);

            // start and stop the fuel pump
            using (var fuelPump = CreateMessagePump(transport.Object, dispatcher, metricQueue))
            {
                var fuelPumpTask = fuelPump.Start();
                readTaskSource.SetResult(CreateTestMessage());

                // wait for the task to complete and verify it was completed
                Assert.IsFalse(await fuelPumpTask);
            }

            // validate metrics
            Assert.IsTrue(metricQueue.HasDataPoint(FuelPumpMetrics.Startup));
            Assert.IsTrue(metricQueue.HasDataPoint(FuelPumpMetrics.UnhandledException));
            Assert.IsTrue(metricQueue.HasDataPoint(FuelPumpMetrics.Shutdown));
        }

        [TestMethod]
        public async Task MessagePump_StartAndStop_FailedRead()
        {
            // setup the fuel pump
            var metricQueue = new Metrics.MockMetricsQueue();
            var transport = new Mock<IMessageTransport>();
            var dispatcher = new MessageDispatcher();
            
            // setup the read message mock response
            var readTaskSource = new TaskCompletionSource<Message>();
            transport
                .Setup(t => t.ReadMessage(Moq.It.IsAny<CancellationToken>()))
                .Returns(readTaskSource.Task);

            // start and stop the fuel pump
            using (var fuelPump = CreateMessagePump(transport.Object, dispatcher, metricQueue))
            {
                var fuelPumpTask = fuelPump.Start();

                // simulate a cancelled read
                readTaskSource.SetException(new EndOfStreamException());

                // wait for the task to complete and verify it was completed
                Assert.IsTrue(await fuelPumpTask);
            }

            // validate metrics
            Assert.IsTrue(metricQueue.HasDataPoint(FuelPumpMetrics.Startup));
            Assert.IsFalse(metricQueue.HasDataPoint(FuelPumpMetrics.UnhandledException));
            Assert.IsTrue(metricQueue.HasDataPoint(FuelPumpMetrics.Shutdown));
        }

        [TestMethod]
        public void MessagePump_StartAndStop_CancelAfterRead()
        {
            // setup the fuel pump
            var metricQueue = new Metrics.MockMetricsQueue();
            var transport = new Mock<IMessageTransport>();
            var dispatcher = new MessageDispatcher();
            
            // setup the read message mock response
            var readTaskSource = new TaskCompletionSource<Message>();
            transport
                .Setup(t => t.ReadMessage(Moq.It.IsAny<CancellationToken>()))
                .Returns(readTaskSource.Task);

            // start and stop the fuel pump
            using (var fuelPump = CreateMessagePump(transport.Object, dispatcher, metricQueue))
            {
                var fuelPumpTask = fuelPump.Start();
                Thread.Sleep(1000);
                fuelPump.Stop();

                // simulate a cancelled read
                readTaskSource.SetResult(CreateTestMessage());

                // wait for the task to complete and verify it was completed
                fuelPumpTask.Wait(5000);
                Assert.IsTrue(fuelPumpTask.IsCompleted);
            }

            // validate metrics
            Assert.IsTrue(metricQueue.HasDataPoint(FuelPumpMetrics.Startup));
            Assert.IsFalse(metricQueue.HasDataPoint(FuelPumpMetrics.UnhandledException));
            Assert.IsTrue(metricQueue.HasDataPoint(FuelPumpMetrics.Shutdown));
        }

        private Message CreateTestMessage()
        {
            return new Message(
                CreateHeader("100", (int)Fuel.Messaging.Message.ShowOverlay),
                CreateMessage());
        }

        private byte[] CreateHeader(string requestId, int type)
        {
            FlatBuffers.FlatBufferBuilder builder = new FlatBuffers.FlatBufferBuilder(1);
            var header = Messages.MessageHeader.CreateMessageHeader(builder, builder.CreateString(requestId), 0, type);

            builder.Finish(header.Value);

            return builder.SizedByteArray();
        }

        private byte[] CreateMessage()
        {
            FlatBuffers.FlatBufferBuilder builder = new FlatBuffers.FlatBufferBuilder(1);

            var request = Messages.ShowOverlay.CreateShowOverlay(builder);
            var message = Messages.MessageRoot.CreateMessageRoot(builder, Messages.FuelResponseType.Success, Messages.Message.ShowOverlay, request.Value);

            builder.Finish(message.Value);

            return builder.SizedByteArray();
        }
    }
}
