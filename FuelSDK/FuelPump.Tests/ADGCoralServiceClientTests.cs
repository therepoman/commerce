﻿using FuelPump.Metrics;
using FuelPump.Providers.Credentials;
using FuelPump.Services;
using FuelPump.Services.ADG.Common;
using FuelPump.Services.ADG.Common.Model;
using FuelPump.Services.ADG.Discovery;
using FuelPump.Services.ADG.Discovery.Model;
using FuelPump.Services.Config;
using FuelPump.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FuelPump.Tests
{
    [TestClass]
    public class ADGCoralServiceClientTests
    {
        public class FakeADGServiceClient : ADGCoralServiceClient
        {
            public FakeADGServiceClient(AbstractCredentialsProvider credentialsProvider, ConfigHandler configHandler, MockHttpMessageHandler handler) :
                base(credentialsProvider, configHandler, new HttpClient(handler))
            {
            }

            public Task<int> DoWork(int data, BearerCredentials identifyingCredentials, CancellationToken cancellationToken)
            {
                var config = new ApiConfig();
                config.endpoint = "https://some.random.service.com";
                config.serviceNamespace = "namespace";
                config.serviceName = "ServiceName";
                config.operationName = "operation.test";

                return CallAsync(new RequestContext<int, int>(
                    config.operationName,
                    1,
                    (request) => data.ToString(),
                    (response) => 1),
                    config,
                    cancellationToken,
                    identifyingCredentials);
            }
        }

        private BearerCredentials twitchCredentials = TestCredentials.twitchCredentials;
        private AmazonCredentials amazonCredentials = TestCredentials.amazonCredentials;
        private MockHttpMessageHandler m_mockHttpMessageHandler = new MockHttpMessageHandler();
        private FakeADGServiceClient client;
        private Mock<ConfigHandler> configHandler;

        [TestInitialize]
        public void setup()
        {
            var credentialsProvider = new Mock<AbstractCredentialsProvider>();
            credentialsProvider.Setup(x => x.GetCredentials(twitchCredentials)).Returns(new Credentials(amazonCredentials));

            configHandler = new MockConfigHandler().Mock;
            client = new FakeADGServiceClient(credentialsProvider.Object, configHandler.Object, m_mockHttpMessageHandler);
        }

        [TestMethod]
        [ExpectedException(typeof(CoralException))]
        public async Task ADGCoralServiceClient_UnmodeledException()
        {
            m_mockHttpMessageHandler.SetupResponse().SetResult(new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("{ \"__type\": \"com.amazon.example.Exception\", \"Message\": \"Some example error.\"}")
            });

            try
            {
                await client.DoWork(1, null, new CancellationToken());
            }
            finally
            {
                configHandler.Verify(handler => handler.ReportApiCallOutcome("operation.test", true));
            }
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidRequestException))]
        public async Task ADGCoralServiceClient_InvalidRequest()
        {
            m_mockHttpMessageHandler.SetupResponse().SetResult(new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("{ \"__type\": \"com.amazon.adg.model.InvalidRequestException\", \"Message\": \"Some bad request error.\"}")
            });

            try
            {
                await client.DoWork(1, null, new CancellationToken());
            }
            finally
            {
                configHandler.Verify(handler => handler.ReportApiCallOutcome("operation.test", true));
            }
        }

        [TestMethod]
        [ExpectedException(typeof(DependencyException))]
        public async Task ADGCoralServiceClient_DependencyException()
        {
            m_mockHttpMessageHandler.SetupResponse().SetResult(new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("{ \"__type\": \"com.amazon.adg.model.DependencyException\", \"Message\": \"Some dependency error.\"}")
            });

            try
            {
                await client.DoWork(1, null, new CancellationToken());
            }
            finally
            {
                configHandler.Verify(handler => handler.ReportApiCallOutcome("operation.test", true));
            }
        }

        [TestMethod]
        [ExpectedException(typeof(InternalServiceException))]
        public async Task ADGCoralServiceClient_InternalFailure()
        {
            m_mockHttpMessageHandler.SetupResponse().SetResult(new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("{ \"__type\": \"com.amazon.adg.model.InternalServiceException\", \"Message\": \"Some internal error.\"}")
            });

            try
            {
                await client.DoWork(1, null, new CancellationToken());
            }
            finally
            {
                configHandler.Verify(handler => handler.ReportApiCallOutcome("operation.test", true));
            }
        }

        /// <summary>
        /// This shouldn't happen if the Coral service is behaving properly but if we hit an error from the load balancer for example it could still happen
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        [ExpectedException(typeof(CoralException))]
        public async Task ADGCoralServiceClient_CoralErrorEmptyBody()
        {
            m_mockHttpMessageHandler.SetupResponse().SetResult(new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("")
            });

            try
            {
                await client.DoWork(1, null, new CancellationToken());
            }
            finally
            {
                configHandler.Verify(handler => handler.ReportApiCallOutcome("operation.test", true));
            }
        }

        [TestMethod]
        public async Task ADGCoralServiceClient_DNSError()
        {
            var expectedException = new ApplicationException("Parent of DNS exception",
                new WebException("DNS exception", WebExceptionStatus.NameResolutionFailure));
            m_mockHttpMessageHandler.SetupResponse().SetException(expectedException);

            try
            {
                await client.DoWork(1, null, new CancellationToken());
                Assert.Fail("An exception should be thrown");
            }
            catch (Exception e)
            {
                Assert.AreEqual(expectedException, e);
                configHandler.Verify(handler => handler.ReportApiCallOutcome("operation.test", false));
            }
        }

        [TestMethod]
        public async Task ADGCoralServiceClient_TimeoutError()
        {
            var expectedException = new ApplicationException("Parent of timeout exception",
                new WebException("Timeout exception", WebExceptionStatus.Timeout));
            m_mockHttpMessageHandler.SetupResponse().SetException(expectedException);

            try
            {
                await client.DoWork(1, null, new CancellationToken());
                Assert.Fail("An exception should be thrown");
            }
            catch (Exception e)
            {
                Assert.AreEqual(expectedException, e);
                configHandler.Verify(handler => handler.ReportApiCallOutcome("operation.test", false));
            }
        }

        [TestMethod]
        public async Task ADGCoralServiceClient_TransientException()
        {
            var expectedException = new ApplicationException("Parent of transient exception",
                new WebException("Transient exception", WebExceptionStatus.SendFailure));
            m_mockHttpMessageHandler.SetupResponse().SetException(expectedException);

            try
            {
                await client.DoWork(1, null, new CancellationToken());
                Assert.Fail("An exception should be thrown");
            }
            catch (Exception e)
            {
                Assert.AreEqual(expectedException, e);
                configHandler.Verify(handler => handler.ReportApiCallOutcome("operation.test", true));
            }
        }

        [TestMethod]
        public async Task ADGCoralServiceClient_UnknownException()
        {
            var expectedException = new ApplicationException("Unknown exception");
            m_mockHttpMessageHandler.SetupResponse().SetException(expectedException);

            try
            {
                await client.DoWork(1, null, new CancellationToken());
                Assert.Fail("An exception should be thrown");
            }
            catch (Exception e)
            {
                Assert.AreEqual(expectedException, e);
                configHandler.Verify(handler => handler.ReportApiCallOutcome("operation.test", true));
            }
        }
    }
}
