﻿using FuelPump.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace FuelPump.Tests
{
    [TestClass]
    public class SignatureValidatorTests
    {
        [TestMethod]
        public void SignatureValidatorTests_SignedAndVerifiedAmazon()
        {
            Assert.IsTrue(SignatureValidator.VerifyEmbeddedSignature(GetTestFilesDir() + "SignedByAmazon.dll"));
        }

        [TestMethod]
        public void SignatureValidatorTests_SignedAndVerifiedTwitch()
        {
            Assert.IsTrue(SignatureValidator.VerifyEmbeddedSignature(GetTestFilesDir() + "SignedByTwitch.dll"));
        }

        [TestMethod]
        public void SignatureValidatorTests_SignedAndVerifiedInvalidSubject()
        {
            Assert.IsFalse(SignatureValidator.VerifyEmbeddedSignature(GetTestFilesDir() + "InvalidSubject.dll"));
        }

        [TestMethod]
        public void SignatureValidatorTests_SignedBadSignature()
        {
            Assert.IsFalse(SignatureValidator.VerifyEmbeddedSignature(GetTestFilesDir() + "BadSignature.dll"));
        }

        [TestMethod]
        public void SignatureValidatorTests_DoesNotExist()
        {
            Assert.IsFalse(SignatureValidator.VerifyEmbeddedSignature(GetTestFilesDir() + "DoesNotExist.dll"));
        }

        [TestMethod]
        public void SignatureValidatorTests_NoSignature()
        {
            Assert.IsFalse(SignatureValidator.VerifyEmbeddedSignature(GetTestFilesDir() + "NoSignature.dll"));
        }

        string GetTestFilesDir()
        {
            return Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + @"\..\FuelSDKDll.Tests\SignatureTestFiles\";
        }
    }
}
