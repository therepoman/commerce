﻿using FuelPump.Metrics;
using FuelPump.Overlay;
using FuelPump.Services.Config;
using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using Ninject;
using Ninject.Modules;
using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Tests
{
    [TestClass]
    public class CefTests
    {
        private static readonly string BLACKLISTED_URI = "https://www.amazon.co.jp/";
        private static readonly string INVALID_CERT_URI = "https://expired.identrustssl.com/";

        [DllImport("OffscreenBrowser.dll")]
        [return: MarshalAs(UnmanagedType.I1)]
        static extern bool CEFStart([MarshalAs(UnmanagedType.LPWStr)]string userAgent);

        [DllImport("OffscreenBrowser.dll")]
        static extern void CEFStop();

        private MockConfigHandler configHandler;

        [TestInitialize]
        public void Setup()
        {
            // Configure the logger and then add the trace appender so that it logs to the
            // Visual Studio output window when tests are run with debugging.
            BasicConfigurator.Configure();

            var patternLayout = new PatternLayout();
            patternLayout.ConversionPattern = "%d [%t] %-5p %m%n";
            patternLayout.ActivateOptions();

            var traceAppender = new TraceAppender();
            traceAppender.Layout = patternLayout;
            traceAppender.ActivateOptions();

            ((Hierarchy)LogManager.GetRepository()).Root.AddAppender(traceAppender);

            configHandler = new MockConfigHandler();
        }

        [TestMethod]
        [DeploymentItem("OffscreenBrowser.dll")]
        [DeploymentItem("FuelPump.Browser.exe")]
        [DeploymentItem("LibCef.dll")]
        [DeploymentItem("natives_blob.bin")]
        [DeploymentItem("snapshot_blob.bin")]
        [DeploymentItem("widevinecdmadapter.dll")]
        [DeploymentItem("icudtl.dat")]
        [Timeout(20000)]
#if DEBUG
        [Ignore] // This test will fail on debug builds because CEF does not do cert checking.
#endif
        public void CefTests_SSL_Security()
        {
            // Multiple tests are combined into one method to reduce loading overhead.
            using (var loader = new CefLoader(configHandler))
            {
                var instance = loader.Instance;

                // CEF doesn't serialize requets, and it doesn't provide a good way to filter out stale LoadEnd/LoadError events.
                // CEF might not have finished loading the page, wait a while to avoid accepting the blank page load success in the next block.
                instance.WaitForPageLoad().Wait(1000);

                // Test successful page load
                {
                    var events = instance.RegisterOverlayEvents();
                    var successfullyLoadedValidHttps = instance.WaitForPageLoad();
                    instance.Navigate("https://www.amazon.com");
                    CefInstance.WrapPageLoad(successfullyLoadedValidHttps, events).Wait();
                    Assert.IsTrue(successfullyLoadedValidHttps.IsCompleted, "It should successfully load a valid page");
                }

                // Test a bad DNS entry.
                {
                    var events = instance.RegisterOverlayEvents();
                    var task = instance.WaitForPageLoad();
                    instance.Navigate("https://dns-name-does-not-exist.amazon.com/");
                    try
                    {
                        CefInstance.WrapPageLoad(task, events).Wait();
                        Assert.Fail("The page load should fail");
                    }
                    catch (Exception ex)
                    {
                        var pageLoadException = ex.InnerException as CefInstance.PageLoadException;
                        Assert.IsNotNull(pageLoadException, "The thrown exception should have an inner PageLoadException, instead got: " + ex);
                        Assert.AreEqual("https://dns-name-does-not-exist.amazon.com/", pageLoadException.Error.FailedURL);
                        Assert.AreEqual(-105, pageLoadException.Error.ErrorCode,
                            "The exception should have been caused by a bad DNS entry (-105 is CefLoadHandler::ErrorCode::ERR_NAME_NOT_RESOLVED)");
                    }
                }

                // Wait for CEF to load its error page so that the next test doesn't receive events for the error page.
                instance.WaitForPageLoad().Wait(1000);

                // Test that a blacklisted Uri is rejected
                {
                    var events = instance.RegisterOverlayEvents();
                    var task = instance.WaitForPageLoadCancelled();
                    instance.Navigate(BLACKLISTED_URI);
                    CefInstance.WrapPageLoad(task, events).Wait();

                    Assert.IsTrue(task.IsCompleted, "The page load should have been cancelled.");
                    Assert.AreEqual(BLACKLISTED_URI, task.Result.FailedURL);
                    Assert.AreEqual(-3, task.Result.ErrorCode,
                        "The exception should have been cancelled (-3 is CefLoadHandler::ErrorCode::ERR_ABORTED)");
                }

                // Wait for CEF to load its error page so that the next test doesn't receive events for the error page.
                instance.WaitForPageLoad().Wait(1000);

                // The INVALID_CERT_URI is not whitelisted.
                configHandler.AllowAllHttpsUris();

                // Test that the whitelist was successfully updated.
                {
                    var events = instance.RegisterOverlayEvents();
                    var successfullyLoadedValidHttps = instance.WaitForPageLoad();
                    instance.Navigate(BLACKLISTED_URI);
                    CefInstance.WrapPageLoad(successfullyLoadedValidHttps, events).Wait();
                    Assert.IsTrue(successfullyLoadedValidHttps.IsCompleted, "It should successfully load a valid page");
                }

                // Test an invalid cert.
                {
                    var events = instance.RegisterOverlayEvents();
                    var cancelTask = instance.WaitForPageLoadCancelled();
                    var loadedTask = instance.WaitForPageLoad();
                    instance.Navigate(INVALID_CERT_URI);
                    Task.WaitAny(CefInstance.WrapPageLoad(cancelTask, events), loadedTask);
                    if (loadedTask.IsCompleted)
                    {
                        Assert.Fail("The page should not have successfully loaded.");
                    }

                    Assert.IsTrue(cancelTask.IsCompleted, "The page load should have been cancelled.");
                    Assert.AreEqual(INVALID_CERT_URI, cancelTask.Result.FailedURL);
                    Assert.AreEqual(-3, cancelTask.Result.ErrorCode,
                        "The exception should have been cancelled (-3 is CefLoadHandler::ErrorCode::ERR_ABORTED)");
                }
            }
        }

        public class CefLoader : IDisposable
        {
            StandardKernel kernel;
            CefInstanceManager cefInstanceManager;
            public CefInstance Instance { get; private set; }

            public CefLoader(MockConfigHandler configHandler)
            {
                if (!CEFStart("FuelUnitTest"))
                {
                    throw new ApplicationException("Cef failed to initialize.");
                }

                var modules = new NinjectModule[]
                {
                    new MockTransportModule(),
                    new Activation.DependencyModule()
                };

                kernel = new StandardKernel(modules);
                kernel.Unbind<ConfigHandler>();
                kernel.Bind<ConfigHandler>().ToConstant(configHandler.Mock.Object);

                kernel.Get<ConfigHandler>().Initialize();

                cefInstanceManager = kernel.Get<CefInstanceManager>();
                cefInstanceManager.SetSize(800, 600);

                Instance = cefInstanceManager.CreateInstance();
            }

            public void Dispose()
            {
                Instance.Dispose();
                cefInstanceManager.Dispose();
                kernel.Dispose();
                CEFStop();
            }
        }

        public class MockTransportModule : NinjectModule
        {
            private Mock<IMessageTransport> messageTransport;

            public MockTransportModule()
            {
                var messageTransportWriter = new Mock<IMessageTransportWriter>();

                messageTransport = new Mock<IMessageTransport>();
                messageTransport.Setup(mock => mock.WriteUntypedMessge(It.IsAny<byte[]>(), It.IsAny<uint>())).Returns(messageTransportWriter.Object);
            }

            public override void Load()
            {
                Bind<IMessageTransport>()
                    .ToConstant(messageTransport.Object);
            }
        }
    }
}
