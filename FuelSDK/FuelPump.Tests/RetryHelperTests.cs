﻿using FuelPump.Services.Config;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;
using System.Threading.Tasks;

namespace FuelPump.Tests
{
    [TestClass]
    public class RetryHelperTests
    {
        [TestMethod]
        public async Task RetryHelperTests_Success()
        {
            var config = new ApiConfig();
            config.retries = 0;
            config.retryDelaySeconds = 0;
            int attempts = 0;
            await Helpers.RetryHelper.Retry(() =>
            {
                attempts++;
                var source = new TaskCompletionSource<bool>();
                source.SetResult(true);
                return source.Task;
            }, config);
            Assert.AreEqual(1, attempts);
        }

        [TestMethod]
        public async Task RetryHelperTests_SuccessOnLastAttempt()
        {
            var config = new ApiConfig();
            config.retries = 9;
            config.retryDelaySeconds = 0;
            int attempts = 0;
            await Helpers.RetryHelper.Retry(() =>
            {
                var source = new TaskCompletionSource<bool>();
                if (++attempts == 10)
                {
                    source.SetResult(true);
                }
                else
                {
                    source.SetException(new TestException());
                }
                return source.Task;
            }, config);
            Assert.AreEqual(10, attempts);
        }

        [TestMethod]
        [ExpectedException(typeof(TestException))]
        public async Task RetryHelperTests_GiveUpAfterTooManyRetries()
        {
            var config = new ApiConfig();
            config.retries = 99;
            config.retryDelaySeconds = 0;
            int attempts = 0;
            try
            {
                await Helpers.RetryHelper.Retry(() =>
                {
                    attempts++;
                    var source = new TaskCompletionSource<bool>();
                    source.SetException(new TestException());
                    return source.Task;
                }, config);
            }
            finally
            {
                Assert.AreEqual(100, attempts);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(TestException))]
        public async Task RetryHelperTests_FailWithNoRetries()
        {
            var config = new ApiConfig();
            config.retries = 0;
            config.retryDelaySeconds = 0;
            int attempts = 0;
            try
            {
                await Helpers.RetryHelper.Retry(() =>
                {
                    attempts++;
                    var source = new TaskCompletionSource<bool>();
                    source.SetException(new TestException());
                    return source.Task;
                }, config);
            }
            finally
            {
                Assert.AreEqual(1, attempts);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(WebException))]
        public async Task RetryHelperTests_NotRetriable()
        {
            var config = new ApiConfig();
            config.retries = 10;
            config.retryDelaySeconds = 0;
            int attempts = 0;
            try
            {
                await Helpers.RetryHelper.Retry(() =>
                {
                    attempts++;
                    var source = new TaskCompletionSource<bool>();
                    source.SetException(new WebException("Test Exception", WebExceptionStatus.NameResolutionFailure));
                    return source.Task;
                }, config);
            }
            finally
            {
                Assert.AreEqual(1, attempts);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(TestException))]
        public async Task RetryHelperTests_Delay()
        {
            var config = new ApiConfig();
            config.retries = 1;
            config.retryDelaySeconds = 0.1;
            int attempts = 0;
            var start = DateTime.Now;
            try
            {
                await Helpers.RetryHelper.Retry(() =>
                {
                    attempts++;
                    var source = new TaskCompletionSource<bool>();
                    source.SetException(new TestException());
                    return source.Task;
                }, config);
            }
            finally
            {
                var end = DateTime.Now;
                Assert.IsTrue(start.AddMilliseconds(95).CompareTo(end) < 0, "It should delay at least the configured amount");
                Assert.IsTrue(start.AddMilliseconds(1000).CompareTo(end) > 0, "It should not delay too much");
                Assert.AreEqual(2, attempts);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(TimeoutException))]
        public async Task RetryHelperTests_Timeout()
        {
            var config = new ApiConfig();
            config.retries = 100;
            config.retryDelaySeconds = 0.1; // It will probably timeout during this delay.
            config.timeoutSeconds = 0.3;
            int attempts = 0;
            var start = DateTime.Now;
            try
            {
                await Helpers.RetryHelper.Retry(() =>
                {
                    attempts++;
                    var source = new TaskCompletionSource<bool>();
                    source.SetException(new TestException());
                    return source.Task;
                }, config);
            }
            finally
            {
                var end = DateTime.Now;
                Assert.IsTrue(attempts > 1, "It should have made more attempts than " + attempts);
                Assert.IsTrue(attempts < 10, "It should have made fewer attempts than " + attempts);
                Assert.IsTrue(start.AddMilliseconds(295).CompareTo(end) < 0, "It should not time out before the configured time");
                Assert.IsTrue(start.AddMilliseconds(1000).CompareTo(end) > 0, "It should not delay too much");
            }
        }

        [TestMethod]
        [ExpectedException(typeof(TimeoutException))]
        public async Task RetryHelperTests_FailAfterTimeout()
        {
            var config = new ApiConfig();
            config.retries = 100;
            config.retryDelaySeconds = 0;
            config.timeoutSeconds = 0.3;
            int attempts = 0;
            var start = DateTime.Now;
            try
            {
                await Helpers.RetryHelper.Retry(() =>
                {
                    attempts++;
                    // It will probably timeout during this delay.
                    return Task.Delay(100).ContinueWith<bool>((completedTask) => { throw new TestException(); });
                }, config);
            }
            finally
            {
                var end = DateTime.Now;
                Assert.IsTrue(attempts > 1, "It should have made more attempts than " + attempts);
                Assert.IsTrue(attempts < 10, "It should have made fewer attempts than " + attempts);
                Assert.IsTrue(start.AddMilliseconds(295).CompareTo(end) < 0, "It should not time out before the configured time");
                Assert.IsTrue(start.AddMilliseconds(1000).CompareTo(end) > 0, "It should not delay too much");
            }
        }

        public class TestException : ApplicationException
        {
        }
    }
}
