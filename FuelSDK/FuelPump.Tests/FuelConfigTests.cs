﻿using FuelPump.Metrics;
using FuelPump.Services.Config;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace FuelPump.Tests
{
    [TestClass]
    public class FuelConfigTests
    {
        private FuelConfig config1;
        private FuelConfig config2;
        private FuelConfig configBlank;
        private FuelConfig configFeatures;

        [TestInitialize]
        public void setup()
        {
            config1 = JsonConvert.DeserializeObject<FuelConfig>(Encoding.UTF8.GetString(FuelPump.Tests.Properties.Resources.Config1));
            config2 = JsonConvert.DeserializeObject<FuelConfig>(Encoding.UTF8.GetString(FuelPump.Tests.Properties.Resources.Config2));
            configBlank = JsonConvert.DeserializeObject<FuelConfig>(Encoding.UTF8.GetString(FuelPump.Tests.Properties.Resources.ConfigBlank));
            configFeatures = JsonConvert.DeserializeObject<FuelConfig>(Encoding.UTF8.GetString(FuelPump.Tests.Properties.Resources.ConfigFeatures));

            // Verify that the config files were loaded and are not equal.
            config1.AssertValid();
            config2.AssertValid();
            assertEqualsConfig1(config1);
            assertEqualsConfig2(config2);
            Assert.AreNotEqual(config1.configSettings.file, config2.configSettings.file);
            Assert.AreNotEqual(config1.configSettings.file, configBlank.configSettings.file);
        }

        [TestMethod]
        public void FuelConfigTests_MergeFuelConfig()
        {
            config1.Merge(config2);
            assertEqualsConfig2(config1);
        }

        [TestMethod]
        public void FuelConfigTests_MergeNullFuelConfig()
        {
            config1.Merge(null);
            assertEqualsConfig1(config1);
        }

        [TestMethod]
        public void FuelConfigTests_MergeEmptyFuelConfig()
        {
            config1.Merge(new FuelConfig());
            assertEqualsConfig1(config1);
        }

        [TestMethod]
        public void FuelConfigTests_MergeNullApiConfig()
        {
            config1.cblPage.Merge(null);
            assertEqualsConfig1(config1);
        }

        [TestMethod]
        public void FuelConfigTests_MergeEmptyApiConfig()
        {
            config1.cblPage.Merge(new ApiConfig());
            assertEqualsConfig1(config1);
        }

        [TestMethod]
        public void FuelConfigTests_MergeBlank()
        {
            config1.Merge(configBlank);
            assertEqualsConfig1(config1);
        }

        [TestMethod]
        public void FuelConfigTests_MergeIntoEmptyConfig()
        {
            var config = new FuelConfig();
            config.Merge(config1);
            assertEqualsConfig1(config);
        }

        [TestMethod]
        public void FuelConfigTests_getUri()
        {
            var config = new ApiConfig();
            config.endpoint = "https://twitch.tv";
            config.path = "some/path";
            Assert.AreEqual("https://twitch.tv/some/path", config.getUri());
        }

        [TestMethod]
        public void FuelConfigTests_getUriBuilder()
        {
            var config = new ApiConfig();
            config.endpoint = "https://twitch.tv";
            config.path = "some/path";
            Assert.AreEqual("https://twitch.tv/some/path?foo=bar", config.getUri("foo=bar"));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void FuelConfigTests_LocalPathIsRejected()
        {
            var config = new ApiConfig();
            config.endpoint = @"C:\LocalFile.txt";
            config.getUri();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void FuelConfigTests_HttpIsRejected()
        {
            var config = new ApiConfig();
            config.endpoint = "http://twitch.tv";
            config.getUri();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void FuelConfigTests_InvalidEndpointIsRejected()
        {
            var config = new ApiConfig();
            config.endpoint = "      ";
            config.getUri();
        }

        [TestMethod]
        public void FuelConfigTests_KeepPortNumber()
        {
            var config = new ApiConfig();
            config.endpoint = "https://twitch.tv:443";
            Assert.AreEqual("https://twitch.tv:443/", config.getUri());
        }

        [TestMethod]
        public void FuelConfigTests_AddPortRegex()
        {
            // Not URLs.
            Assert.IsFalse(ApiConfig.ADD_PORT_REGEX.IsMatch(""));
            Assert.IsFalse(ApiConfig.ADD_PORT_REGEX.IsMatch("twitch.tv:1"));
            Assert.IsFalse(ApiConfig.ADD_PORT_REGEX.IsMatch("twitch.tv"));

            // Doesn't match URLs with port numbers.
            Assert.IsFalse(ApiConfig.ADD_PORT_REGEX.IsMatch("https://twitch.tv:443/"));
            Assert.IsFalse(ApiConfig.ADD_PORT_REGEX.IsMatch("https://twitch.tv:1/"));
            Assert.IsFalse(ApiConfig.ADD_PORT_REGEX.IsMatch("https://twitch.tv:1/foo"));
            Assert.IsFalse(ApiConfig.ADD_PORT_REGEX.IsMatch("https://twitch.tv:1"));
            Assert.IsFalse(ApiConfig.ADD_PORT_REGEX.IsMatch("https://twitch.tv:1?"));
            Assert.IsFalse(ApiConfig.ADD_PORT_REGEX.IsMatch("https://twitch.tv:1?foo"));
            Assert.IsFalse(ApiConfig.ADD_PORT_REGEX.IsMatch("foo://twitch.tv:1"));

            // Matches URLs without port numbers.
            assertMatch(ApiConfig.ADD_PORT_REGEX, "https://twitch.tv", "https://twitch.tv", "");
            assertMatch(ApiConfig.ADD_PORT_REGEX, "https://twitch.tv/foo", "https://twitch.tv", "/foo");
            assertMatch(ApiConfig.ADD_PORT_REGEX, "https://twitch.tv?", "https://twitch.tv", "?");
            assertMatch(ApiConfig.ADD_PORT_REGEX, "https://twitch.tv?foo", "https://twitch.tv", "?foo");
            assertMatch(ApiConfig.ADD_PORT_REGEX, "foo://twitch.tv", "foo://twitch.tv", "");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void FuelConfigTests_MinimumFallbackDuration()
        {
            config1.configSettings.fallbackDurationSeconds = 10;
            config1.AssertValid();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void FuelConfigTests_MinimumErrorCount()
        {
            config1.configSettings.maxFailuresUntilFallback = 0;
            config1.AssertValid();
        }

        [TestMethod]
        public void FuelConfigTests_MinimumErrorsAndDuration()
        {
            var config = new ConfigSettings();
            config.fallbackDurationSeconds = 123456;
            config.maxFailuresUntilFallback = 1234;

            var invalidConfig = new ConfigSettings();
            invalidConfig.fallbackDurationSeconds = -1;
            invalidConfig.maxFailuresUntilFallback = -1;
            config.Merge(invalidConfig);
            Assert.AreEqual(123456, config.fallbackDurationSeconds);
            Assert.AreEqual(1234, config.maxFailuresUntilFallback);

            invalidConfig.fallbackDurationSeconds = 5;
            invalidConfig.maxFailuresUntilFallback = 0;
            config.Merge(invalidConfig);
            Assert.AreEqual(123456, config.fallbackDurationSeconds);
            Assert.AreEqual(1234, config.maxFailuresUntilFallback);

            invalidConfig.fallbackDurationSeconds = 100000;
            invalidConfig.maxFailuresUntilFallback = 1000;
            config.Merge(invalidConfig);
            Assert.AreEqual(100000, config.fallbackDurationSeconds);
            Assert.AreEqual(1000, config.maxFailuresUntilFallback);
        }

        [TestMethod]
        public void FuelConfigTests_BootstrapConfigUriWhitelist()
        {
            var bootstrapConfig = MockConfigHandler.GetRealBootstrapConfig();
            bootstrapConfig.AssertValid();

            var valid = new List<string>();
            var validForOverlay = new List<string>();
            var validForBrowser = new List<string>();
            var invalid = new List<string>();

            // https only.
            valid.Add("https://amazon.com");
            invalid.Add("http://amazon.com");
            valid.Add("https://twitch.tv");
            invalid.Add("http://twitch.tv");
            validForOverlay.Add("https://ssl-images-amazon.com");
            invalid.Add("http://ssl-images-amazon.com");
            validForOverlay.Add("https://ttvnw.net");
            invalid.Add("http://ttvnw.net");

            // The overlay supports the local scheme, and the external browser doesn't.
            validForOverlay.Add("local://www.amazon.com");

            // The overlay can't load pages that accept Amazon passwords, but the external browser can.
            validForBrowser.Add("https://www.amazon.com/ap/signin");
            validForBrowser.Add("https://www.amazon.com/ap/register");
            validForOverlay.Add("local://www.amazon.com/ap/signin");
            validForOverlay.Add("local://www.amazon.com/ap/register");
            valid.Add("https://twitch.tv/ap/signin");
            valid.Add("https://twitch.tv/ap/register");

            // Other paths are accepted.
            valid.Add("https://amazon.com/foo");
            valid.Add("https://twitch.tv/foo");

            // Port numbers are ignored.
            valid.Add("https://amazon.com:1234");
            valid.Add("https://twitch.tv:1234");

            // Only amazon and twitch domains are accepted.
            valid.Add("https://www.amazon.com");
            invalid.Add("https://fakeamazon.com");
            invalid.Add("https://amazon.com.fake");

            valid.Add("https://www.twitch.tv");
            invalid.Add("https://faketwitch.tv");
            invalid.Add("https://twitch.tv.fake");

            validForOverlay.Add("https://www.ssl-images-amazon.com");
            invalid.Add("https://faketssl-images-amazon.com");
            invalid.Add("https://ssl-images-amazon.com.fake");

            validForOverlay.Add("https://www.ttvnw.net");
            invalid.Add("https://fakettvnw.net");
            invalid.Add("https://ttvnw.net.fake");

            // It should be case insensitive.
            foreach (var uris in new List<List<string>>{ valid, validForOverlay, validForBrowser, invalid })
            {
                uris.AddRange(new List<string>(uris.Select(uri => uri.ToUpper())));
            }

            foreach (var uri in valid)
            {
                Assert.IsTrue(bootstrapConfig.IsWhitelistedForOverlay(new Uri(uri)),
                    "\"" + uri + "\" should be valid for the overlay");
                Assert.IsTrue(bootstrapConfig.IsWhitelistedForOpenInBrowser(new Uri(uri)),
                    "\"" + uri + "\" should be valid for opening in the browser");
            }

            foreach (var uri in validForOverlay)
            {
                Assert.IsTrue(bootstrapConfig.IsWhitelistedForOverlay(new Uri(uri)),
                    "\"" + uri + "\" should be valid for the overlay");
                Assert.IsFalse(bootstrapConfig.IsWhitelistedForOpenInBrowser(new Uri(uri)),
                    "\"" + uri + "\" should not be valid for opening in the browser");
            }

            foreach (var uri in validForBrowser)
            {
                Assert.IsFalse(bootstrapConfig.IsWhitelistedForOverlay(new Uri(uri)),
                    "\"" + uri + "\" should not be valid for the overlay");
                Assert.IsTrue(bootstrapConfig.IsWhitelistedForOpenInBrowser(new Uri(uri)),
                    "\"" + uri + "\" should be valid for opening in the browser");
            }

            foreach (var uri in invalid)
            {
                Assert.IsFalse(bootstrapConfig.IsWhitelistedForOverlay(new Uri(uri)),
                    "\"" + uri + "\" should not be valid for the overlay");
                Assert.IsFalse(bootstrapConfig.IsWhitelistedForOpenInBrowser(new Uri(uri)),
                    "\"" + uri + "\" should not be valid for opening in the browser");
            }
        }

        [TestMethod]
        public void FuelConfigTests_TestFeatureOn()
        {
            Assert.IsTrue(configFeatures.IsFeatureOn("feature_on"));
        }

        [TestMethod]
        public void FuelConfigTests_TestFeatureOff()
        {
            Assert.IsFalse(configFeatures.IsFeatureOn("feature_off"));
        }

        [TestMethod]
        public void FuelConfigTests_TestFeatureUndefined()
        {
            Assert.IsFalse(configFeatures.IsFeatureOn("UNDEF"));
        }

        private void assertEqualsConfig1(FuelConfig config)
        {
            Assert.AreEqual(11000, config.configSettings.fallbackDurationSeconds);
            Assert.AreEqual("https://configFile1", config.configSettings.file);
            Assert.AreEqual(12, config.configSettings.initializationTimeoutSeconds);
            Assert.AreEqual(0.13, config.configSettings.jitterMultiplier);
            Assert.AreEqual(14, config.configSettings.maxFailuresUntilFallback);
            Assert.AreEqual(150000, config.configSettings.refreshIntervalSeconds);

            Assert.AreEqual("cookies1", config.cookiesDomain);
            Assert.AreEqual(1, config.metricsMaxQueueSize);
            Assert.AreEqual(10.5, config.metricsTransmissionIntervalSeconds);
            Assert.AreEqual(10, config.pageLoadTimeoutSeconds);

            Assert.AreEqual(1, config.discoveryProductBlacklist.Count);
            Assert.AreEqual("discoveryProductBlacklist1", config.discoveryProductBlacklist[0]);

            Assert.AreEqual(1, config.entitlementProductBlacklist.Count);
            Assert.AreEqual("entitlementProductBlacklist1", config.entitlementProductBlacklist[0]);

            Assert.AreEqual(1, config.openInBrowserWhitelist.Count);
            Assert.AreEqual("openInBrowserWhitelist1", config.openInBrowserWhitelist[0].path);

            Assert.AreEqual(1, config.overlayUriWhitelist.Count);
            Assert.AreEqual("overlayUriWhitelist1", config.overlayUriWhitelist[0].path);

            Assert.AreEqual(1, config.restartCblBlacklist.Count);
            Assert.AreEqual("restartCblBlacklist1", config.restartCblBlacklist[0].path);

            Assert.AreEqual(1, config.urlPathBlacklistMaxRedirects);

            Assert.AreEqual(true, config.cblPage.IsEnabled());
            Assert.AreEqual("https://cblEndpoint1", config.cblPage.endpoint);
            Assert.AreEqual("cblOperationName1", config.cblPage.operationName);
            Assert.AreEqual("cblPath1", config.cblPage.path);
            Assert.AreEqual(1, config.cblPage.retries);
            Assert.AreEqual(1.1, config.cblPage.retryDelaySeconds);
            Assert.AreEqual("cblServiceName1", config.cblPage.serviceName);
            Assert.AreEqual("cblServiceNamespace1", config.cblPage.serviceNamespace);
            Assert.AreEqual(1.2, config.cblPage.timeoutSeconds);

            Assert.AreEqual("https://getProductListDetailsEndpoint1", config.getProductListDetails.endpoint);
            Assert.AreEqual("https://metricsEndpoint1", config.metrics.endpoint);
            Assert.AreEqual("https://purchasePageEndpoint1", config.purchasePage.endpoint);
            Assert.AreEqual("https://setReceiptsFulfillmentStatusEndpoint1", config.setReceiptsFulfillmentStatus.endpoint);
            Assert.AreEqual("https://syncGoodsEndpoint1", config.syncGoods.endpoint);
            Assert.AreEqual("https://tokenExchangeEndpoint1", config.tokenExchange.endpoint);
            Assert.AreEqual("https://validateTwitchTokenEndpoint1", config.validateTwitchToken.endpoint);
        }

        private void assertEqualsConfig2(FuelConfig config)
        {
            Assert.AreEqual(21000, config.configSettings.fallbackDurationSeconds);
            Assert.AreEqual("https://configFile2", config.configSettings.file);
            Assert.AreEqual(22, config.configSettings.initializationTimeoutSeconds);
            Assert.AreEqual(0.23, config.configSettings.jitterMultiplier);
            Assert.AreEqual(24, config.configSettings.maxFailuresUntilFallback);
            Assert.AreEqual(250000, config.configSettings.refreshIntervalSeconds);

            Assert.AreEqual("cookies2", config.cookiesDomain);
            Assert.AreEqual(2, config.metricsMaxQueueSize);
            Assert.AreEqual(20.5, config.metricsTransmissionIntervalSeconds);
            Assert.AreEqual(20, config.pageLoadTimeoutSeconds);

            Assert.AreEqual(1, config.discoveryProductBlacklist.Count);
            Assert.AreEqual("discoveryProductBlacklist2", config.discoveryProductBlacklist[0]);

            Assert.AreEqual(1, config.entitlementProductBlacklist.Count);
            Assert.AreEqual("entitlementProductBlacklist2", config.entitlementProductBlacklist[0]);

            Assert.AreEqual(1, config.openInBrowserWhitelist.Count);
            Assert.AreEqual("openInBrowserWhitelist2", config.openInBrowserWhitelist[0].path);

            Assert.AreEqual(1, config.overlayUriWhitelist.Count);
            Assert.AreEqual("overlayUriWhitelist2", config.overlayUriWhitelist[0].path);

            Assert.AreEqual(1, config.restartCblBlacklist.Count);
            Assert.AreEqual("restartCblBlacklist2", config.restartCblBlacklist[0].path);

            Assert.AreEqual(2, config.urlPathBlacklistMaxRedirects);

            Assert.AreEqual(false, config.cblPage.IsEnabled());
            Assert.AreEqual("https://cblEndpoint2", config.cblPage.endpoint);
            Assert.AreEqual("cblOperationName2", config.cblPage.operationName);
            Assert.AreEqual("cblPath2", config.cblPage.path);
            Assert.AreEqual(2, config.cblPage.retries);
            Assert.AreEqual(2.1, config.cblPage.retryDelaySeconds);
            Assert.AreEqual("cblServiceName2", config.cblPage.serviceName);
            Assert.AreEqual("cblServiceNamespace2", config.cblPage.serviceNamespace);
            Assert.AreEqual(2.2, config.cblPage.timeoutSeconds);

            Assert.AreEqual("https://getProductListDetailsEndpoint2", config.getProductListDetails.endpoint);
            Assert.AreEqual("https://metricsEndpoint2", config.metrics.endpoint);
            Assert.AreEqual("https://purchasePageEndpoint2", config.purchasePage.endpoint);
            Assert.AreEqual("https://setReceiptsFulfillmentStatusEndpoint2", config.setReceiptsFulfillmentStatus.endpoint);
            Assert.AreEqual("https://syncGoodsEndpoint2", config.syncGoods.endpoint);
            Assert.AreEqual("https://tokenExchangeEndpoint2", config.tokenExchange.endpoint);
            Assert.AreEqual("https://validateTwitchTokenEndpoint2", config.validateTwitchToken.endpoint);
        }

        private void assertMatch(Regex regex, string value, string match1, string match2)
        {
            var match = regex.Match(value);
            Assert.IsTrue(match.Success);
            Assert.AreEqual(match1, match.Groups[1].Value);
            Assert.AreEqual(match2, match.Groups[2].Value);
        }
    }
}
