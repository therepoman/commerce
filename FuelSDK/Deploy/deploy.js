var exec = require('child_process').exec;

console.log("Starting");

var gamePath = process.argv[2]
var product = process.argv[3]
var bucket = process.argv[4]
var channel = process.argv[5]


function hasVersionID(value){
  if(value !== undefined && value.length > 0) {
    return value.indexOf("version-id") >= 0;
  }
  return false;
}

var uploadCmd = "SoftwareDistributionAdminTool.exe ingest-version --path " + gamePath + " --bucket " + bucket + " --product " + product + " --name 1.0.0"
console.log(uploadCmd);
var activateCmd = "SoftwareDistributionAdminTool.exe activate-version --channel " + channel + " --version "
console.log(activateCmd);

function activateResponseFunction(err, data) {
  if(!err){
    console.log("Success");
    console.log(data.toString());
  }
  console.log("Error: " + err);
}

function uploadResponseFunction(err, data) {
  if(!err) {
    console.log("Success!");
  } else {
    console.log("Error: " + err);
  }
  var splitData = data.toString().split("\r\n");
  splitData = splitData.filter(hasVersionID);
  versionNumber = splitData[0].split(":")[1];
  console.log("New Version Number: " + versionNumber);
  console.log("Starting activation");
  exec(activateCmd + versionNumber, activateResponseFunction);
}

var upload =function(){
  console.log("Starting Upload");
  var channelNumber;
  exec(uploadCmd, uploadResponseFunction);
   
}
upload();
