#include "stdafx.h"
#include "gtest/gtest.h"

#include "fuel_impl/signature_validator.h"

#pragma comment (lib, "crypt32")
#pragma comment(lib, "bcrypt") 

#define SIGNATURE_TEST_FILES_DIR GetDirectoryName(__FILE__) + L"\\SignatureTestFiles"

std::wstring GetDirectoryName(std::string path) {
	const size_t last_slash_idx = path.rfind('\\');
	if (std::string::npos != last_slash_idx)
	{
		std::string strDir(path.substr(0, last_slash_idx + 1));
		return std::wstring(strDir.begin(), strDir.end());
	}
	return L"";
}

// Test a file with a correct signature from Amazon
TEST(SignatureValidatorTests, SignedAndVerifiedAmazon)
{
	bool result = Fuel::Internal::SignatureValidator::VerifyEmbeddedSignature(SIGNATURE_TEST_FILES_DIR + L"\\SignedByAmazon.dll");
	EXPECT_TRUE(result);
}

// Test a file with a correct signature from Twitch
TEST(SignatureValidatorTests, SignedAndVerifiedTwitch)
{
	bool result = Fuel::Internal::SignatureValidator::VerifyEmbeddedSignature(SIGNATURE_TEST_FILES_DIR + L"\\SignedByTwitch.dll");
	EXPECT_TRUE(result);
}

// Test a file with a correct signature, but not from Twitch or Amazon
TEST(SignatureValidatorTests, SignedAndVerifiedInvalidSubject)
{
	bool result = Fuel::Internal::SignatureValidator::VerifyEmbeddedSignature(SIGNATURE_TEST_FILES_DIR + L"\\InvalidSubject.dll");
	EXPECT_FALSE(result);
}

// Test a file that was signed by Amazon but has been modified
TEST(SignatureValidatorTests, SignedBadSignature)
{
	bool result = Fuel::Internal::SignatureValidator::VerifyEmbeddedSignature(SIGNATURE_TEST_FILES_DIR + L"\\BadSignature.dll");
	EXPECT_FALSE(result);
}

// Test a file which does not exist
TEST(SignatureValidatorTests, DoesNotExist)
{
	bool result = Fuel::Internal::SignatureValidator::VerifyEmbeddedSignature(SIGNATURE_TEST_FILES_DIR + L"\\DoesNotExist.dll");
	EXPECT_FALSE(result);
}

// Test a file with no signature
TEST(SignatureValidatorTests, NoSignature)
{
	bool result = Fuel::Internal::SignatureValidator::VerifyEmbeddedSignature(SIGNATURE_TEST_FILES_DIR + L"\\NoSignature.dll");
	EXPECT_FALSE(result);
}