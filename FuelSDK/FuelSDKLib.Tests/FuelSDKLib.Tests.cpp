// FuelSDKLib.Tests.cpp : Defines the entry point for the console application.
//

#include <thread>
#include <chrono>
#include <tchar.h>

#include "gtest/gtest.h"

#include <fuel_impl/IapClient.h>

// Testing GetProductData for IapClient (Happy)
// TODO: [FUEL-818] Add case where there are no available skus
TEST(FuelSDKLibTests, TestGetProductDataUnauthenticated)
{
	std::unique_ptr<Fuel::Iap::IapClient> g_pSdk;

	auto sdkClientResponse = Fuel::Iap::IapClient::Create("vendor", "1.0", Fuel::RendererType::DIRECT_X_11);
	ASSERT_TRUE(sdkClientResponse.IsSuccess());
	g_pSdk = sdkClientResponse.GetResponseWithOwnership();

	Fuel::Iap::Model::GetProductDataRequest request;
	std::vector<Fuel::Iap::Model::Sku> skus;
	skus.push_back(L"SKU-1234");
	request.SetSkus(skus);
	auto result = g_pSdk->GetProductData(request);
	ASSERT_EQ(result.GetError().GetType(), Fuel::Iap::IapErrors::INTERNAL_FAILURE);

	g_pSdk.release();
}

// TODO: [FUEL-818] Fix test
// Testing Purchase for IapClient (Happy)
TEST(FuelSDKLibTests, TestPurchase)
{
	// Commented out until the test is fixed
	//std::unique_ptr<Fuel::Iap::IapClient> g_pSdk;

	//auto sdkClientResponse = Fuel::Iap::IapClient::Create("1.0", Fuel::RendererType::DIRECT_X_11);
	//ASSERT_TRUE(sdkClientResponse.IsSuccess());
	//g_pSdk = sdkClientResponse.GetResponseWithOwnership();

	//Fuel::Iap::Model::PurchaseRequest request;
	//auto result = g_pSdk->Purchase(request);

	//ASSERT_TRUE(result.IsSuccess());

	//g_pSdk.release();
}

int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();

	return 0;
}

