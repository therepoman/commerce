#pragma once

#include "include/cef_app.h"

class CEFV8Handler : public CefV8Handler {
public:
	CEFV8Handler(CefRefPtr<CefBrowser> browser);
	~CEFV8Handler() {}

	virtual bool Execute(const CefString& name,
		CefRefPtr<CefV8Value> object,
		const CefV8ValueList& arguments,
		CefRefPtr<CefV8Value>& retval,
		CefString& exception) OVERRIDE;

	bool SendCefEvent(std::string eventName, std::string eventData);
	bool SendCefEvent(std::string eventName);
	bool OpenInBrowser(std::string url);

	IMPLEMENT_REFCOUNTING(CEFV8Handler);
private:
	CefRefPtr<CefBrowser> m_browser;
};