#include "FuelPump.CefApp.h"

void MyApp::OnBeforeCommandLineProcessing(const CefString& process_type, CefRefPtr<CefCommandLine> command_line)
{
	command_line->AppendSwitch("--disable-extensions");
	command_line->AppendSwitch("--disable-gpu");
	command_line->AppendSwitch("--disable-gpu-compositing");
	command_line->AppendSwitch("--transparent-painting-enabled");
	command_line->AppendSwitch("--enable-begin-frame-scheduling");
}

CefRefPtr<CefRenderProcessHandler> MyApp::GetRenderProcessHandler()
{
	return this;
}

void MyApp::OnContextCreated(CefRefPtr<CefBrowser> browser,
	CefRefPtr<CefFrame> frame,
	CefRefPtr<CefV8Context> context)
{
	CefRefPtr<CefV8Value> object = context->GetGlobal();
	CefRefPtr<CefV8Handler> handler = new CEFV8Handler(browser);

	object->SetValue("sendCefEvent",
		CefV8Value::CreateFunction("sendCefEvent", handler),
		V8_PROPERTY_ATTRIBUTE_NONE);

	object->SetValue("openInBrowser",
		CefV8Value::CreateFunction("openInBrowser", handler),
		V8_PROPERTY_ATTRIBUTE_NONE);
}

bool MyApp::OnBeforeNavigation(CefRefPtr<CefBrowser> browser,
	CefRefPtr<CefFrame> frame,
	CefRefPtr<CefRequest> request,
	NavigationType navigation_type,
	bool is_redirect)
{
	if (navigation_type == NAVIGATION_BACK_FORWARD)
	{
		return true;
	}
	return false;
}

void MyApp::OnRegisterCustomSchemes(CefRefPtr<CefSchemeRegistrar> registrar)
{
	registrar->AddCustomScheme("local", true, false, false);
}