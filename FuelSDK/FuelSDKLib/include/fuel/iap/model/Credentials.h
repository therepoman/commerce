#pragma once

#include <vector>
#include <map>
#include <functional>

#include "Types.h"

namespace Fuel
{
	namespace Iap
	{
		namespace Model
		{
			/**
			* Class represents an authentication token and contains:
			*
			* An authentication token for the user - Send to backend Amazon Digital Goods (ADG) services, along with
			* stored credentials.
			*
			* The type of the token - Enables backend ADG to look up the correct endpoint to validate the token and
			* return identity information. The type is standardized and requires service configuration to work.
			*
			* A unique identifier for the token - Uniquely identifies the user whose token is provided. Used to look
			* up the user's stored Amazon credentials.
			*/
			class Credentials
			{
			public:
				Credentials(const Fuel::String& type, const Fuel::String& identifier, const Fuel::String& token)
				{
					m_type = type;
					m_identifier = identifier;
					m_token = token;
				}

				Credentials(const Credentials& that) : m_identifier(that.m_identifier), m_type(that.m_type), m_token(that.m_token)
				{
				}

				Credentials(Credentials&& that) :
					m_identifier(std::move(that.m_identifier)),
					m_type(std::move(that.m_type)),
					m_token(std::move(that.m_token))
				{
				}

				Credentials& operator=(const Credentials& that)
				{
					if (this != &that)
					{
						m_identifier = that.m_identifier;
						m_type = that.m_type;
						m_token = that.m_token;
					}

					return *this;
				}

				Credentials& operator=(Credentials&& that)
				{
					if (this != &that)
					{
						m_identifier = std::move(that.m_identifier);
						m_type = std::move(that.m_type);
						m_token = std::move(that.m_token);
					}

					return *this;
				}

				/**
				* Returns the unique identifer for the token
				* @return Returns the identifier, a string
				*/
				const Fuel::String& GetIdentifier() const { return m_identifier; }

				/**
				* Returns the authentication token
				* @return Returns the token, a string
				*/
				const Fuel::String& GetToken() const { return m_token; }

				/**
				* Returns the type of the token
				* @return Returns the type, a string
				*/
				const Fuel::String& GetType() const { return m_type; }

			protected:

				Fuel::String m_identifier;
				Fuel::String m_type;
				Fuel::String m_token;
			};

			using CredentialsMap = std::map<Fuel::String, Fuel::Iap::Model::Credentials>;

			/**
			* Twitch implementation of Credentials. The name "twitch" is configured in ADG to look up
			* twitch.tv user data using Twitch identity services.
			*/
			inline Credentials CreateTwitchCredentials(const Fuel::String& twitchId, const Fuel::String& token)
			{
				return Credentials(L"twitch", twitchId, token);
			}
		}
	}
}