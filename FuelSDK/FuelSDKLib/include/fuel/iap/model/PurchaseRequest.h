#pragma once

#include "Types.h"
#include "Credentials.h"

namespace Fuel
{
	class Message;

	namespace Iap
	{
		namespace Model
		{
			/**
			* Represents a request to {@link IapClient#Purchase()}.
			*/
			class PurchaseRequest
			{
			public:
				/**
				* Gets the SKU of the item to be purchased
				*
				* @return SKU, a string
				*/
				const Fuel::String& GetSku() const { return m_sku; }

				/**
				* Sets the SKU of the item to be purchased
				*
				* @param sku, a string
				*/
				void SetSku(const Fuel::String& sku) { m_sku = sku; }
			private:
				Fuel::String m_sku;
			};
		}
	}
}
