#pragma once

#include "TwitchUser.h"

namespace Fuel
{
	namespace Iap
	{
		namespace Model
		{
			/**
			* Represents the result of a call to {@link IapClient#GetSession()}. It holds the Twitch user
			* currently logged in and their entitlement ID. The entitlement ID is an identifier for a specific
			* entitlement to a product (game), for a specific Twitch user; this enables the game developer to
			* check if the user is entitled to the game. (This is the user who bought the game from the
			* developer and is using the Twitch Launcher to play the game.)
			*/
			class GetSessionResponse
			{
			public:
				/**
				* Get the Twitch user logged into the Twitch Launcher
				*
				* @return A {@link TwitchUser} object
				*/
				inline const TwitchUser& GetUser() const { return m_user; }
				/**
				* Sets the Twitch user. Internally used.
				*
				* @param user, a {@link TwitchUser} object
				*/
				inline void SetUser(const TwitchUser& user) { m_user = user; }

				/**
				* Gets the entitlement ID, which can be used to verify whether the Twitch user is entitled
				* to the game.
				*
				* @return The entitlement ID, a string
				*/
				inline const Fuel::String GetEntitlement() const { return m_entitlement; }
				/**
				* Sets the entitlement ID. Internally used.
				*
				* @param entitlement, a string
				*/
				inline void SetEntitlement(const Fuel::String entitlement) { m_entitlement = entitlement; }
			private:
				TwitchUser m_user;
				Fuel::String m_entitlement;
			};
		}
	}
}