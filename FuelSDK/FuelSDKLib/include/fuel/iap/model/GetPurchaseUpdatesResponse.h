#pragma once

#include <vector>

#include "Types.h"

#include "Receipt.h"

namespace Fuel
{
	class Message;

	namespace Iap
	{
		namespace Model
		{
			/**
			 * Represents the result of a call to {@link IapClient#GetPurchaseUpdates()}.
			 */
			class GetPurchaseUpdatesResponse
			{
			public:
				/**
				 * Returns the sync point generated from {@link IapClient#GetPurchaseUpdates()}. A sync point
				 * is a specific time when purchase updates were synced with the backend service.
				 *
				 * @return Returns {@link SyncPointData} for the next sync
				 */
				const SyncPointData& GetSyncPoint() const { return m_syncPoint; }
				/**
				 * Sets the sync point data. Internally used.
				 *
				 * @param syncPoint, a {@link SyncPointData} object that contains a recently generated sync point.
				 */
				void SetSyncPoint(const SyncPointData& syncPoint) { m_syncPoint = syncPoint; }

				/**
				 * Returns one or more {@link Receipt} objects or an empty list. An empty list is returned if there
				 * are no receipts or {@link IapClient#GetPurchaseUpdates()} is not successful.
				 *
				 * @return The {@link Receipt} objects (as a vector), or an empty list (if the call is unsuccessful
				 *         or there are no Receipts objects for any purchases).
				 */
				const std::vector<Receipt>& GetReceipts() const { return m_receipts; }
				/**
				 * Sets receipts. Internally used.
				 *
				 * @param receipts, One or more{@link Receipt} objects (a vector) or an empty list (if the call is
				 *        unsuccessful or there are no updates to any purchases.
				 */
				void SetReceipts(const std::vector<Receipt>& receipts) { m_receipts = receipts; }
				void SetReceipts(std::vector<Receipt>&& receipts) { m_receipts = std::move(receipts); }
			private:
				SyncPointData m_syncPoint;
				std::vector<Receipt> m_receipts;
			};
		}
	}
}
