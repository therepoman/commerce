#pragma once

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include <windows.h>

#include <future>
#include "signature_validator.h"

namespace Fuel 
{
	namespace Internal
	{
		class ResponseCallbackWrapper;

		class DllWrapper
		{
		public:
			struct LibraryDeleter
			{
				typedef HINSTANCE pointer;

				void operator() (HINSTANCE library)
				{
					if (library != NULL)
					{
						FreeLibrary(library);
					}
				}
			};

			using unique_library = std::unique_ptr<HINSTANCE, LibraryDeleter>;

			typedef void(*ResponseCallback)(void* userData, int type, int size, void* data);

			class ResponseCallbackWrapper
			{
				std::promise<void> m_ready;
				int m_type;
				int m_size;
				std::unique_ptr<uint8_t> m_data;
			public:
				int GetType() const
				{
					return m_type;
				}

				const uint8_t* GetData() const
				{
					return m_data.get();
				}

				void Wait()
				{
					m_ready.get_future().wait();
				}

				static void Callback(void* userData, int type, int size, void* data)
				{
					((ResponseCallbackWrapper*)userData)->CallbackHandler(type, size, data);
				}
			private:

				void CallbackHandler(int type, int size, void* data)
				{
					m_type = type;
					m_size = size;
					m_data.reset(new uint8_t[size]);
					memcpy(m_data.get(), data, size);
					m_ready.set_value();
				}
			};

			static Fuel::ResultCodes Create(const std::string& vendorId, const std::string& gameVersion, const std::string& sdkVersion, Fuel::RendererType renderType, std::unique_ptr<DllWrapper>& result)
			{
				#ifdef _M_IX86
				std::wstring fuel_dll_name = L"\\FuelSDK_x86";
				#elif _M_AMD64
				std::wstring fuel_dll_name = L"\\FuelSDK_x64";
				#else
				#error Only supports x86 or x64 builds.
				#endif

				if (renderType == Fuel::RendererType::DIRECT_X_12) {
					fuel_dll_name = fuel_dll_name + L"_dx12";
				}

				fuel_dll_name = fuel_dll_name + L".dll";

				wchar_t* fuel_env = NULL;
				size_t numberOfElements;
				_wdupenv_s(&fuel_env, &numberOfElements, L"FUEL_DIR");
				std::wstring fuel_path;
				if (fuel_env == NULL || numberOfElements < 1)
				{
					fuel_path = L"fuel" + fuel_dll_name;
				}
				else
				{
					fuel_path = std::wstring(fuel_env) + fuel_dll_name;
				}

				if (GetFileAttributesW(fuel_path.c_str()) == INVALID_FILE_ATTRIBUTES && GetLastError() == ERROR_FILE_NOT_FOUND)
				{
					return Fuel::ResultCodes::MISSING_DLL;
				}

				#ifndef _DEBUG
				if (!SignatureValidator::VerifyEmbeddedSignature(fuel_path))
				{
					return Fuel::ResultCodes::INVALID_SIGNATURE;
				}
				#endif
				auto library = unique_library(LoadLibraryW(fuel_path.c_str()));

				if (library.get() == 0)
				{
					return Fuel::ResultCodes::MISSING_DLL;
				}

				auto initialize = reinterpret_cast<InitializeType>(reinterpret_cast<void*>(GetProcAddress(library.get(), "Initialize")));
				auto destroy = reinterpret_cast<DestroyType>(reinterpret_cast<void*>(GetProcAddress(library.get(), "Destroy")));
				auto sendRequest = reinterpret_cast<SendRequestType>(reinterpret_cast<void*>(GetProcAddress(library.get(), "SendRequest")));
				auto sendRequestResponse = reinterpret_cast<SendRequestResponseType>(reinterpret_cast<void*>(GetProcAddress(library.get(), "SendRequestResponse")));
				if (initialize == NULL || destroy == NULL || sendRequest == NULL || sendRequestResponse == NULL)
				{
					return Fuel::ResultCodes::CORRUPT_DLL;
				}
				
				auto status = (Fuel::ResultCodes)initialize(vendorId.c_str(), gameVersion.c_str(), sdkVersion.c_str(), (int)renderType);
				if (status != Fuel::ResultCodes::SUCCESS)
				{
					return status;
				}

				result.reset(new DllWrapper(library, sendRequest, sendRequestResponse, destroy));
				return status;
			}

			~DllWrapper()
			{
				m_destroy();
			}

			Fuel::ResultCodes SendRequest(int type, int size, void* data)
			{
				return (Fuel::ResultCodes)m_sendRequest(type, size, data);
			}

			Fuel::ResultCodes SendRequestResponse(int type, int size, void* data, const ResponseCallbackWrapper& responseCallback)
			{
				return (Fuel::ResultCodes)m_sendRequestResponse(type, size, data, ResponseCallbackWrapper::Callback, (void*)&responseCallback);
			}

		private:
			typedef int(*InitializeType)(const char* vendorId, const char* gameVersion, const char* sdkVersion, int renderType);
			typedef int(*SendRequestType)(int type, int size, void* data);
			typedef int(*SendRequestResponseType)(int type, int size, void* data, ResponseCallback callback, void* userData);
			typedef int(*DestroyType)();

			DllWrapper(unique_library& library, SendRequestType sendRequest, SendRequestResponseType sendRequestResponse, DestroyType destroy) :
				m_library(std::move(library)),
				m_sendRequest(sendRequest),
				m_sendRequestResponse(sendRequestResponse),
				m_destroy(destroy)
			{
			}

			SendRequestType m_sendRequest;
			SendRequestResponseType m_sendRequestResponse;
			DestroyType m_destroy;

			std::unique_ptr<HINSTANCE, LibraryDeleter> m_library;
		};
	}
}
