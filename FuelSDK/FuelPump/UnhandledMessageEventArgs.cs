﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump
{
    public class UnhandledMessageEventArgs : EventArgs
    {
        public UnhandledMessageEventArgs(Message message)
        {
            Message = message;
        }

        public Message Message { get; private set; }
    }
}
