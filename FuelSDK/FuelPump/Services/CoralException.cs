﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Services
{
    public class CoralException : Exception
    {
        public CoralException(string type, string message) : 
            base(message)
        {
            CoralErrorType = type;
        }

        public string CoralErrorType { get; set; }
    }
}
