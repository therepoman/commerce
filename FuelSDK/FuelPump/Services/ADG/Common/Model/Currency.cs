﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Services.ADG.Common.Model
{
    public class Currency
    {
        public Decimal amount { get; set; }
        public string unit { get; set; }
    }
}
