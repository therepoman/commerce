﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Services.ADG.Common.Model
{
    public class ADGEntity
    {
        public string id { get; set; } // UUID
        public string name { get; set; }
    }
}
