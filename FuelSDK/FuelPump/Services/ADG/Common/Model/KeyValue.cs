﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Services.ADG.Common.Model
{
    class KeyValue
    {
        public string key { get; set; }
        public string value { get; set; }
    }
}
