﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Services.ADG.Common.Model
{
    class ClientInfo
    {
        public string clientId { get; set; }
        public string clientVersion { get; set; }
        public string clientType { get; set; }
        public Dictionary<string, string> properties { get; set; }
        public List<KeyValue> headers { get; set; }
    }
}
