﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Services.ADG.Common.Model
{
    public class InvalidRequestException : CoralException
    {
        public static readonly string INVALID_REQUEST_EXCEPTION = "InvalidRequestException";
        public InvalidRequestException(string message) : base(INVALID_REQUEST_EXCEPTION, message)
        {
        }
    }

    public class DependencyException : CoralException
    {
        public static readonly string DEPENDENCY_EXCEPTION = "DependencyException";
        public DependencyException(string message) : base(DEPENDENCY_EXCEPTION, message)
        {
        }
    }

    public class InternalServiceException : CoralException
    {
        public static readonly string INTERNAL_SERVICE_EXCEPTION = "InternalServiceException";
        public InternalServiceException(string message) : base(INTERNAL_SERVICE_EXCEPTION, message)
        {
        }
    }
}
