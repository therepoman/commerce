﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Services.ADG.Common.Model
{
    class CustomerLocalePrefs
    {
        public string pfm { get; set; }
        public string cor { get; set; }
    }
}
