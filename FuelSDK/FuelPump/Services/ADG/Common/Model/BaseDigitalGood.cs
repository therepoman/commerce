﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Services.ADG.Common.Model
{
    class BaseDigitalGood
    {
        public Product product { get; set; }
        public string id { get; set; }
        public string customerId { get; set; }
        public string ownerId { get; set; }
        public string receiptId { get; set; }
        public string state { get; set; }
        public Domain channel { get; set; }
        public Domain domain { get; set; }
        public List<Domain> additionalDomains { get; set; }
        public DigitalGoodOrigin origin { get; set; }
        public long? modified { get; set; }
        public TransactionTypeEnum transactionState { get; set; }
        public List<DigitalGoodTransaction> transactions { get; set; }
    }
}
