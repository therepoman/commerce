﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Services.ADG.Common.Model
{
    public enum TransactionTypeEnum
    {
        ORIGIN,
        PENDING_DELIVERY,
        DELIVERY_ATTEMPTED,
        DELIVERED,
        FULFILLED,
        FULFILLED_NO_ACK,
        FULFILLED_BACKFILL,
        CANNOT_FULFILL,
        CANCELLED,
        CANCELLED_BACKFILL,
        CONSUMED,
        REFILLED,
        RENEWED,
        EXPIRED,
        TRANSFERRED,
        LOANED,
        SHARED,
        UNSHARED,
        REVOKED,
        RESTORED,
        DELETED
    }

    public static class Extensions
    {
        public static bool IsPendingDelivery(this TransactionTypeEnum type)
        {
            return type == TransactionTypeEnum.PENDING_DELIVERY || type == TransactionTypeEnum.ORIGIN;
        }

        public static bool IsCancelled(this TransactionTypeEnum type)
        {
            return type == TransactionTypeEnum.CANCELLED || type == TransactionTypeEnum.REVOKED;
        }
    }
}
