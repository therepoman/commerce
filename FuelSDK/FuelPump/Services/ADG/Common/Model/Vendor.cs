﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Services.ADG.Common.Model
{
    public class Vendor : ADGEntity
    {
        public string masVendorId { get; set; }
        public string masVendorCode { get; set; }
    }
}
