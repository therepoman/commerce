﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FuelPump.Providers.Credentials;
using System.Threading;
using FuelPump.Services.ADG.Common.Model;
using FuelPump.Services.Config;

namespace FuelPump.Services.ADG.Common
{
    public class ADGCoralServiceClient : AbstractServiceClient
    {
        private ConfigHandler configHandler;

        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(ADGCoralServiceClient));

        public ADGCoralServiceClient(AbstractCredentialsProvider credentialsProvider, ConfigHandler configHandler, HttpClient client = null)
            : base(credentialsProvider, configHandler, client)
        {
            this.configHandler = configHandler;
        }

        protected override Task<TResponse> CallAsync<TRequest, TResponse>(RequestContext<TRequest, TResponse> context, ApiConfig apiConfig, CancellationToken cancellationToken, BearerCredentials identifyingCredentials)
        {
            if (!apiConfig.IsEnabled())
            {
                string message = "The operation '" + context.Operation + "' is disabled.";
                LOG.Info(message);
                throw new ApiConfig.DisabledOperationException(message);
            }

            return Helpers.RetryHelper.Retry(() => CallAsyncOnce(context, apiConfig, cancellationToken, identifyingCredentials), apiConfig);
        }

        protected async Task<TResponse> CallAsyncOnce<TRequest, TResponse>(RequestContext<TRequest, TResponse> context, ApiConfig apiConfig, CancellationToken cancellationToken, BearerCredentials identifyingCredentials)
        {
            try
            {
                var response = await base.CallAsync<TRequest, TResponse>(context, apiConfig, cancellationToken, identifyingCredentials);
                
                // Successfully connected to the server and got a response from it.  The config is good.
                configHandler.ReportApiCallOutcome(context.Operation, true);

                return response;
            }
            catch (CoralException e)
            {
                // Successfully connected to the server and got a recognizable error from it.  The config is probably good.
                configHandler.ReportApiCallOutcome(context.Operation, true);

                string type = e.CoralErrorType;
                if (type == null)
                {
                    throw e;
                }

                // ADG services only throw one of three modeled exceptions
                if (type.EndsWith(InvalidRequestException.INVALID_REQUEST_EXCEPTION))
                {
                    throw new InvalidRequestException(e.Message);
                }
                else if (type.EndsWith(DependencyException.DEPENDENCY_EXCEPTION))
                {
                    throw new DependencyException(e.Message);
                }
                else if (type.EndsWith(InternalServiceException.INTERNAL_SERVICE_EXCEPTION))
                {
                    throw new InternalServiceException(e.Message);
                }

                throw e;
            }
            catch (Exception e)
            {
                if (ConfigHandler.IsPossiblyBadConfiguration(e))
                {
                    // Failed to connect to the server, the config might be bad.
                    configHandler.ReportApiCallOutcome(context.Operation, false);
                }
                else
                {
                    // Don't know what happened, but the config is probably good.
                    configHandler.ReportApiCallOutcome(context.Operation, true);
                }

                throw e;
            }
        }
    }
}
