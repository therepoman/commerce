﻿using FuelPump.Services.ADG.Entitlement.Model;
using FuelPump.Services.Config;
using FuelPump.Providers.Credentials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FuelPump.Services.ADG.Common;
using FuelPump.Metrics;

namespace FuelPump.Services.ADG.Entitlement
{
    class EntitlementServiceClient : ADGCoralServiceClient
    {
        private ConfigHandler configHandler;

        public EntitlementServiceClient(AbstractCredentialsProvider credentialsProvider, ConfigHandler configHandler) :
            base(credentialsProvider, configHandler)
        {
            this.configHandler = configHandler;
        }

        public async Task<SyncGoodsResponse> SyncGoods(MetricEventFactory.MetricEventScope metrics, SyncGoodsRequest request,
            BearerCredentials identifyingCredentials, CancellationToken cancellationToken = default(CancellationToken))
        {
            var config = configHandler.GetConfig().syncGoods;
            RequestContext<SyncGoodsRequest, SyncGoodsResponse> context = new RequestContext<SyncGoodsRequest, SyncGoodsResponse>(
                config.operationName, request,
                new GenericJsonRequestSerializer<SyncGoodsRequest>().Serialize,
                new GenericJsonResponseDeserializer<SyncGoodsResponse>().Deserialize);
            try
            {
                using (var timer = metrics.MetricEvent.StartTimer(FuelPumpMetrics.ServiceCall.SyncGoods))
                {
                    var response = await CallAsync(context, config, cancellationToken, identifyingCredentials);
                    var blacklist = configHandler.GetConfig().entitlementProductBlacklist;
                    if (response != null && response.goods != null && blacklist != null)
                    {
                        response.goods.RemoveAll(good => blacklist.Contains(good.product.asin) || blacklist.Contains(good.product.sku));
                    }
                    return response;
                }
            }
            catch (Exception ex)
            {
                metrics.MetricEvent.SetCounter(FuelPumpMetrics.ServiceCall.SyncGoodsFailure);
                throw ex;
            }
        }
    }
}
