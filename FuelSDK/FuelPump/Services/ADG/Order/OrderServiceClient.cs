﻿using FuelPump.Metrics;
using FuelPump.Providers.Credentials;
using FuelPump.Services.ADG.Common;
using FuelPump.Services.ADG.Common.Model;
using FuelPump.Services.ADG.Order.Model;
using FuelPump.Services.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FuelPump.Services.ADG.Order
{
    public class OrderServiceClient : ADGCoralServiceClient
    {
        private ConfigHandler configHandler;

        public OrderServiceClient(AbstractCredentialsProvider credentialsProvider, ConfigHandler configHandler) :
            base(credentialsProvider, configHandler)
        {
            this.configHandler = configHandler;
        }

        public async Task<SetReceiptsFulfillmentStatusResponse> SetReceiptsFulfillmentStatus(
            MetricEventFactory.MetricEventScope metrics, List<ReceiptUpdate> receiptUpdates,
            BearerCredentials identifyingCredentials, CancellationToken cancellationToken = default(CancellationToken))
        {
            var config = configHandler.GetConfig().setReceiptsFulfillmentStatus;
            var request = new SetReceiptsFulfillmentStatusRequest();
            request.receiptUpdates = receiptUpdates;

            RequestContext<SetReceiptsFulfillmentStatusRequest, SetReceiptsFulfillmentStatusResponse> requestContext =
                new RequestContext<SetReceiptsFulfillmentStatusRequest, SetReceiptsFulfillmentStatusResponse>(
                    config.operationName, request,
                new GenericJsonRequestSerializer<SetReceiptsFulfillmentStatusRequest>().Serialize,
                new GenericJsonResponseDeserializer<SetReceiptsFulfillmentStatusResponse>().Deserialize);
            try
            {
                using (var timer = metrics.MetricEvent.StartTimer(FuelPumpMetrics.ServiceCall.SetReceiptsFulfillmentStatus))
                {
                    return await CallAsync(requestContext, config, cancellationToken, identifyingCredentials);
                }
            }
            catch (Exception ex)
            {
                metrics.MetricEvent.SetCounter(FuelPumpMetrics.ServiceCall.SetReceiptsFulfillmentStatusFailure);
                throw ex;
            }
        }

        public async Task<Receipt> SetReceiptFulfillmentStatus(MetricEventFactory.MetricEventScope metrics,
            ReceiptUpdate receiptUpdate, BearerCredentials identifyingCredentials,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var receiptUpdates = new List<ReceiptUpdate>();
            receiptUpdates.Add(receiptUpdate);

            var response = await SetReceiptsFulfillmentStatus(metrics, receiptUpdates, identifyingCredentials, cancellationToken);

            if (response == null || response.receipts == null || response.receipts.Count == 0)
            {
                throw new InternalServiceException("SetReceiptsFulfillmentStatus did not return a list of receipts");
            }

            foreach (var receipt in response.receipts)
            {
                if (receipt.receiptId == receiptUpdate.receiptId)
                {
                    return receipt;
                }
            }

            throw new InternalServiceException("SetReceiptsFulfillmentStatus did not return a response for the requested receipt.");
        }
    }
}
