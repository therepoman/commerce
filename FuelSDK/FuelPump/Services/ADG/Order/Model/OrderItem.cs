﻿using FuelPump.Services.ADG.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Services.ADG.Order.Model
{
    public class OrderItem
    {
        public Product product { get; set; }
        // Typo in ADGOrderServiceModel has this as "modifed"
        public DateTime modifiedDate { get; set; }
        public string state { get; set; } // DigitalGoodStateEnum Coral enum
        public string transactionState { get; set; } // TransactionTypeEnum Coral enum
        public string receipt { get; set; }
    }
}
