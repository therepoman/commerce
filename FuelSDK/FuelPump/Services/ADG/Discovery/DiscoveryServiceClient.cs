﻿using FuelPump.Services.ADG.Discovery.Model;
using FuelPump.Providers.Credentials;
using System;
using System.Threading;
using System.Threading.Tasks;
using FuelPump.Services.ADG.Common;
using FuelPump.Services.Config;
using FuelPump.Metrics;

namespace FuelPump.Services.ADG.Discovery
{
    class DiscoveryServiceClient : ADGCoralServiceClient
    {
        private ConfigHandler configHandler;

        public DiscoveryServiceClient(AbstractCredentialsProvider credentialsProvider, ConfigHandler configHandler)
            : base(credentialsProvider, configHandler)
        {
            this.configHandler = configHandler;
        }

        public async Task<GetProductListDetailsResponse> GetProductListDetails(MetricEventFactory.MetricEventScope metrics,
            GetProductListDetailsRequest request, BearerCredentials identifyingCredentials,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var config = configHandler.GetConfig().getProductListDetails;
            RequestContext<GetProductListDetailsRequest, GetProductListDetailsResponse> requestContext =
                new RequestContext<GetProductListDetailsRequest, GetProductListDetailsResponse>(
                config.operationName, request,
                new GenericJsonRequestSerializer<GetProductListDetailsRequest>().Serialize,
                new GenericJsonResponseDeserializer<GetProductListDetailsResponse>().Deserialize);
            try
            {
                using (var timer = metrics.MetricEvent.StartTimer(FuelPumpMetrics.ServiceCall.GetProductData))
                {
                    var response = await CallAsync(requestContext, config, cancellationToken, identifyingCredentials);
                    var blacklist = configHandler.GetConfig().discoveryProductBlacklist;
                    if (response != null && response.products != null && blacklist != null)
                    {
                        response.products.RemoveAll(product => blacklist.Contains(product.asin) || blacklist.Contains(product.sku));
                    }
                    return response;
                }
            }
            catch (Exception ex)
            {
                metrics.MetricEvent.SetCounter(FuelPumpMetrics.ServiceCall.GetProductDataFailure);
                throw ex;
            }
        }
    }
}
