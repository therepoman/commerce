﻿using System;

namespace FuelPump.Services.Config
{
    public class ConfigSettings
    {
        private static readonly double MIN_FAILURE_COUNT = 1;
        private static readonly double MIN_POLL_INTERVAL_SECONDS = 3600;
        private static readonly double MAX_TIMEOUT_SECONDS = 30;

        // Add new fields to the merge below and validation if needed.  Also update FuelConfigTests and the test json files.
        public double? fallbackDurationSeconds { get; set; }
        public string file { get; set; }
        public double? initializationTimeoutSeconds { get; set; }
        public double? jitterMultiplier { get; set; }
        public long? maxFailuresUntilFallback { get; set; }
        public double? refreshIntervalSeconds { get; set; }
        // Add new fields to the merge below and validation if needed.  Also update FuelConfigTests and the test json files.

        public void AssertValid()
        {
            try
            {
                new Uri(file);
            }
            catch (Exception ex)
            {
                throw new InvalidConfigurationException("Invalid config uri '" + file + "'", ex);
            }

            if (!IsValidPollInterval(fallbackDurationSeconds))
            {
                throw new InvalidConfigurationException("Invalid fallbackDurationSeconds '" + fallbackDurationSeconds + "'");
            }

            if (!IsValidInitializationTimeout(initializationTimeoutSeconds))
            {
                throw new InvalidConfigurationException("Invalid initializationTimeoutSeconds '" + initializationTimeoutSeconds + "'");
            }

            if (!IsValidJitter(jitterMultiplier))
            {
                throw new InvalidConfigurationException("Invalid jitterMultiplier '" + jitterMultiplier + "'");
            }

            if (!IsValidMaxFailureCount(maxFailuresUntilFallback))
            {
                throw new InvalidConfigurationException("Invalid maxFailuresUntilFallback '" + maxFailuresUntilFallback + "'");
            }

            if (!IsValidPollInterval(refreshIntervalSeconds))
            {
                throw new InvalidConfigurationException("Invalid refreshIntervalSeconds '" + refreshIntervalSeconds + "'");
            }
        }

        /// <summary>
        /// Copies valid fields from the provided config, and leaves the other fields as-is.
        /// </summary>
        public void Merge(ConfigSettings other)
        {
            if (other == null)
            {
                return;
            }

            if (!string.IsNullOrWhiteSpace(other.file))
            {
                file = other.file;
            }

            if (IsValidPollInterval(other.fallbackDurationSeconds))
            {
                fallbackDurationSeconds = other.fallbackDurationSeconds;
            }

            if (IsValidInitializationTimeout(other.initializationTimeoutSeconds))
            {
                initializationTimeoutSeconds = other.initializationTimeoutSeconds;
            }

            if (IsValidJitter(other.jitterMultiplier))
            {
                jitterMultiplier = other.jitterMultiplier;
            }

            if (IsValidMaxFailureCount(other.maxFailuresUntilFallback))
            {
                maxFailuresUntilFallback = other.maxFailuresUntilFallback;
            }

            if (IsValidPollInterval(other.refreshIntervalSeconds))
            {
                refreshIntervalSeconds = other.refreshIntervalSeconds;
            }
        }

        private bool IsValidPollInterval(double? interval)
        {
            return interval != null && interval > MIN_POLL_INTERVAL_SECONDS;
        }

        private bool IsValidInitializationTimeout(double? timeout)
        {
            return timeout != null && timeout > 0 && timeout <= MAX_TIMEOUT_SECONDS;
        }

        private bool IsValidMaxFailureCount(long? count)
        {
            return count != null && count >= MIN_FAILURE_COUNT;
        }

        private bool IsValidJitter(double? jitter)
        {
            return jitter != null && jitter >= 0;
        }
    }
}
