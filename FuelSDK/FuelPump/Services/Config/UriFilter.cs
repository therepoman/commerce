﻿using System;
using System.Text.RegularExpressions;

namespace FuelPump.Services.Config
{
    public class UriFilter
    {
        public string scheme { get; set; }
        public string domain { get; set; }
        public string path { get; set; }

        public bool IsMatch(Uri uri)
        {
            if (scheme != null && !Regex.IsMatch(uri.Scheme, scheme, RegexOptions.IgnoreCase))
            {
                return false;
            }

            if (domain != null && !Regex.IsMatch(uri.Host, domain, RegexOptions.IgnoreCase))
            {
                return false;
            }

            if (path != null && !Regex.IsMatch(uri.AbsolutePath, path, RegexOptions.IgnoreCase))
            {
                return false;
            }

            return true;
        }
    }
}
