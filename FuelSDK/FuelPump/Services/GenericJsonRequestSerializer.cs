﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Text;

namespace FuelPump.Services
{
    /**
     * Generic serializer that serializes objects to JSON using JSON.NET library
     */
    class GenericJsonRequestSerializer<T>
    { 
        public string Serialize(T obj)
        {
            return JsonConvert.SerializeObject(obj,
                Newtonsoft.Json.Formatting.None,
                new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    Converters = { new StringEnumConverter() }
                });
        }
    }
}
