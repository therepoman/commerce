﻿using Fuel.Messaging;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FuelPump
{
    public class MessageTransport : Disposable, IMessageTransport
    {
        [DllImport("Kernel32.dll")]
        static extern bool CancelIoEx(IntPtr handle, IntPtr overlapped);

        [DllImport("kernel32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool WriteFile(IntPtr hFile, 
            IntPtr lpBuffer,
            uint nNumberOfBytesToWrite, 
            out uint lpNumberOfBytesWritten,
            IntPtr lpOverlapped);

        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(MessageTransport));
        private const int MAX_SIZE = 50 * 1024 * 1024; // 50MB max message size

        private IntPtr m_readHandle;
        private IntPtr m_writeHandle;
        private Stream m_reader;
        private Stream m_writer;
        private object m_lock = new object();

        private class Writer : IMessageTransportWriter
        {
            object m_lockObject;
            Stream m_writeStream;
            IntPtr m_writeHandle;
            uint m_bytesToWrite;
            uint m_bytesWritten = 0;

            public Writer(object lockObject, Stream writeStream, IntPtr writeHandle, byte[] header, uint bytesToWrite)
            {
                m_lockObject = lockObject;
                m_writeStream = writeStream;
                m_writeHandle = writeHandle;
                m_bytesToWrite = bytesToWrite;

                System.Threading.Monitor.Enter(m_lockObject);

                try
                {
                    // First write the header
                    writeStream.Write(BitConverter.GetBytes(header.Length), 0, 4);
                    writeStream.Write(header, 0, header.Length);

                    // Now write the payload.
                    writeStream.Write(BitConverter.GetBytes(bytesToWrite), 0, 4);
                }
                catch
                {
                    System.Threading.Monitor.Exit(m_lockObject);
                    throw;
                }
            }

            public void Dispose()
            {
                System.Threading.Monitor.Exit(m_lockObject);

                if (m_bytesToWrite != m_bytesWritten)
                {
                    throw new IOException("Too few bytes were written for the message.");
                }
            }

            public void Write(int data)
            {

                Write(BitConverter.GetBytes(data));
            }

            public void Write(byte[] data)
            {
                if (m_bytesWritten + data.Length > m_bytesToWrite)
                {
                    throw new IOException("Too many bytes were written for the message");
                }

                m_writeStream.Write(data, 0, data.Length);
                m_bytesWritten += (uint)data.Length;
            }

            public void Write(IntPtr ptr, uint size)
            {
                if (m_bytesWritten + size > m_bytesToWrite)
                {
                    throw new IOException("Too many bytes were written for the message");
                }

                uint bytesWritten = 0;
                WriteFile(m_writeHandle, ptr, size, out bytesWritten, IntPtr.Zero);
                m_bytesWritten += bytesWritten;

                if (bytesWritten != size)
                {
                    throw new IOException("Error writing to pipe.");
                }
            }
        }

        /// <summary>
        /// Creates a new MessageTransport from pipe handle strings
        /// </summary>
        /// <param name="readPipeHandle">Pipe handle for the read pipe</param>
        /// <param name="writePipeHandle">Pipe handle for the write pipe</param>
        public MessageTransport(string readPipeHandle, string writePipeHandle)
        {
            m_readHandle = new IntPtr(long.Parse(readPipeHandle));
            m_writeHandle = new IntPtr(long.Parse(writePipeHandle));
            m_reader = new AnonymousPipeClientStream(PipeDirection.In, readPipeHandle);
            m_writer = new AnonymousPipeClientStream(PipeDirection.Out, writePipeHandle);
        }

        /// <summary>
        /// Indicates the transport has been explicitly disposed or a connection failure occurred
        /// </summary>
        public bool IsDisposed()
        {
            return Disposed;
        }

        /// <summary>
        /// Releases managed resources
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            { 
                m_reader.Dispose();
                m_writer.Dispose();
            }
        }

        /// <summary>
        /// Attempts to read a message from the transport
        /// </summary>
        /// <returns>The message received</returns>
        /// <exception cref="System.IO.EndOfStreamException">Raised when there is a problem reading from the transport or the transport connection is closed.</exception>
        /// <exception cref="System.OperationCancelledException">Raised when the read is cancelled.</exception>
        public async Task<Message> ReadMessage(CancellationToken cancellationToken)
        {
            if (Disposed)
            {
                throw new ObjectDisposedException("");
            }

            try
            {
                var headerData = await ReadBlock(m_reader, cancellationToken);
                var payloadData = await ReadBlock(m_reader, cancellationToken);

                return new Message(headerData, payloadData);
            }
            catch (EndOfStreamException e)
            {
                LOG.Info("Hit end of stream. Pipe closed?", e);
                Dispose();

                throw;
            }
            catch (MessageTooLargeException e)
            {
                LOG.Error("A message was received that is larger than the max message size.", e);
                Dispose();

                throw;
            }
            catch (TaskCanceledException e)
            {
                LOG.Info("Stream read cancelled", e);
                Dispose();

                throw new OperationCanceledException();
            }
            catch (Exception ex)
            {
                LOG.Error("Unexpected exception!", ex);
                Dispose();

                throw;
            }
        }

        /// <summary>
        /// Read a size entry and the corresponding bytes from the pipe
        /// </summary>
        /// <param name="stream">The stream</param>
        /// <param name="cancellationToken">A cancellation token that will abort the read</param>
        /// <returns>A Task of byte array that will contain the block read or indicate cancellation or failure</returns>
        /// <remarks>
        /// The reason a thread is used to perform the read is to allow the main thread to continue running
        /// after a cancel. If the read is done on the same thread as the caller, the cancellation blocks. If the task is
        /// marked as completed on a different thread, the read fails with fewer bytes read which allows the thread doing
        /// the read to become unblocked and shutdown.
        /// </remarks>
        private async Task<byte[]> ReadBlock(Stream stream, CancellationToken cancellationToken)
        { 
            // read the block size and convert the bytes to an int
            var headerSizeBytes = await ReadAsync(m_reader, 4, cancellationToken);
            var headerSize = BitConverter.ToInt32(headerSizeBytes, 0);

            // check if the incoming message is too large
            if (headerSize > MAX_SIZE)
            {
                throw new MessageTooLargeException();
            }

            // read the block data
            return await ReadAsync(m_reader, headerSize, cancellationToken);
        }
       
        /// <summary>
        /// Wrapper around Stream.Read to provide cancellation support since PipeStream doesn't implement ReadAsync
        /// </summary>
        /// <param name="stream">The stream</param>
        /// <param name="size">The number of bytes to read from the stream</param>
        /// <param name="cancellationToken">A cancellation token that will abort the read</param>
        /// <returns>A Task of byte array that will either return the resulting bytes or indicate cancellation or failure</returns>
        private Task<byte[]> ReadAsync(Stream stream, int size, CancellationToken cancellationToken)
        {
            var readTaskSource = new TaskCompletionSource<byte[]>();

            // if the read was cancelled before the read thread was started
            // set the result task to cancelled, otherwise start the read thread
            if (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    var bytes = new byte[size];
                    int bytesRead = 0;

                    // create the action performed when a read is cancelled while blocking on Stream.Read
                    // cancellation causes the IO operation to be cancelled
                    Action cancellation = () =>
                    {
                        readTaskSource.SetCanceled();
                        CancelIoEx(m_readHandle, IntPtr.Zero);
                        LOG.Info("Pipe read cancelled");
                    };

                    // register the cancellation handler and unregister it as soon as the Read is complete
                    using (var registration = cancellationToken.Register(cancellation))
                    {
                        bytesRead = stream.Read(bytes, 0, size);
                    }

                    // if too few bytes were read, it means the client side of the pipe failed or the read was
                    // cancelled.  If it wasn't cancelled and not enough bytes were read, throw an error, otherwise 
                    // return the bytes
                    if (!readTaskSource.Task.IsCanceled)
                    {
                        if (bytesRead != size)
                        {
                            readTaskSource.SetException(new EndOfStreamException());
                        }
                        else
                        {
                            readTaskSource.SetResult(bytes);
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (!(ex is OperationCanceledException))
                    {
                        // forward any exceptions to the caller
                        LOG.Error("Pipe read exception", ex);
                        readTaskSource.SetException(ex);
                    }
                }
            }
            else
            {
                readTaskSource.SetCanceled();
            }

            return readTaskSource.Task;
        }

        /// <summary>
        /// Writes a message to the transport 
        /// </summary>
        /// <param name="message">The message</param>
        /// <exception cref="System.IO.IOException">Raised when the transport connection is closed.</exception>
        public void WriteMessage(Message message)
        {
            if (message.GetPayloadLength() > MAX_SIZE)
            {
                throw new MessageTooLargeException();
            }

            try
            {
                lock (m_lock)
                {
                    // First write the header
                    var headerBuffer = message.GetHeaderBuffer();
                    m_writer.Write(BitConverter.GetBytes(headerBuffer.Length), 0, 4);
                    m_writer.Write(headerBuffer, 0, headerBuffer.Length);

                    // Now write the payload.
                    m_writer.Write(BitConverter.GetBytes(message.GetPayloadLength()), 0, 4);
                    message.WritePayload(m_writer);
                }
            }
            catch (IOException e)
            {
                LOG.Warn("Write failed. Hit IO Exception. Pipe closed?", e);
                Dispose();

                throw;
            }
            catch (Exception e)
            {
                LOG.Error("Unexpected exception!", e);
                Dispose();

                throw;
            }
        }

        public IMessageTransportWriter WriteUntypedMessge(byte[] header, uint size)
        {
            if (header.Length + size > MAX_SIZE)
            {
                throw new MessageTooLargeException();
            }

            try
            {
                return new Writer(m_lock, m_writer, m_writeHandle, header, size);
            }
            catch (IOException e)
            {
                LOG.Warn("Write failed. Hit IO Exception. Pipe closed?", e);
                Dispose();

                throw;
            }
            catch (Exception e)
            {
                LOG.Error("Unexpected exception!", e);
                Dispose();

                throw;
            }
        }
    }
}
