﻿using FuelPump.Services.ADG.Common.Model;
using FuelPump.Services.ADG.Discovery;
using FuelPump.Services.ADG.Discovery.Model;
using FuelPump.Providers.Credentials;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Messages = Fuel.Messaging;
using FuelPump.Services;
using FuelPump.Services.Config;
using FuelPump.Helpers;

namespace FuelPump.Handlers
{

    public class GetProductDataHandler
    {
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(GetProductDataHandler));

        private static readonly string US_MARKETPLACE = "ATVPDKIKX0DER";
        private static readonly string DOCKET_ID = "twitchSdkGetProductDetails.json";

        // See GetProductData in IapClient.h
        private static readonly int MAX_SKUS = 100;

        Metrics.MetricEventFactory m_metrics;

        private IApplicationContext AppContext;
        private ConfigHandler configHandler;
        private ValidationHelper validationHelper;

        public GetProductDataHandler(AbstractCredentialsProvider credentialsProvider, Metrics.MetricEventFactory metrics, IApplicationContext context, ConfigHandler configHandler, ValidationHelper validationHelper)
        {
            CredentialsProvider = credentialsProvider;
            m_metrics = metrics;
            AppContext = context;
            this.configHandler = configHandler;
            this.validationHelper = validationHelper;
        }

        // TODO: [FUEL-824] Refactor credentials provider and its setter to a parent Handler class.
        protected AbstractCredentialsProvider CredentialsProvider;

        public async Task<MessageFactory<Messages.GetProductDataResponse>> GetProductData(MessageContext context, Messages.GetProductDataRequest message)
        {
            using (var metricScope = m_metrics.Create())
            {
                metricScope.MetricEvent.AddCounter(FuelPumpMetrics.APICall.GetProductData);

                if (message.SkusLength > MAX_SKUS)
                {
                    return new MessageFactory<Messages.GetProductDataResponse>(builder =>
                        {
                            return Messages.GetProductDataResponse.CreateGetProductDataResponse(builder, status: Messages.GetProductDataResponseStatus.InvalidArgument);
                        });
                }

                var requestSkus = AsEnumerable<string>(message.SkusLength, message.GetSkus).ToArray();

                foreach (var sku in requestSkus)
                {
                    if (!validationHelper.IsSkuValid(sku))
                    {
                        return new MessageFactory<Messages.GetProductDataResponse>(builder =>
                            {
                                return Messages.GetProductDataResponse.CreateGetProductDataResponse(builder, status: Messages.GetProductDataResponseStatus.InvalidArgument);
                            });
                    }
                }

                DiscoveryServiceClient service = new DiscoveryServiceClient(CredentialsProvider, configHandler);

                GetProductListDetailsRequest srvReq = new GetProductListDetailsRequest();

                srvReq.products = requestSkus.Select(sku => SkuAsProduct(sku)).ToList();

                srvReq.language = CultureInfo.CurrentUICulture.Name;
                srvReq.preferredLocale = new CustomerLocalePrefs();
                srvReq.preferredLocale.cor = RegionInfo.CurrentRegion.Name;
                srvReq.preferredLocale.pfm = US_MARKETPLACE;
                srvReq.docketId = DOCKET_ID;

                try
                {
                    BearerCredentials identifyingCredentials = HandlerCredentialsHelper.ExtractSingleCredentials(message.Credentials);

                    var response = await service.GetProductListDetails(metricScope, srvReq, identifyingCredentials);
                    var products = response.products;

                    return new MessageFactory<Messages.GetProductDataResponse>(
                        builder =>
                        {
                            FlatBuffers.VectorOffset unavailableSkusVector;
                            IEnumerable<string> unavailable = requestSkus.Except(SkusOfProducts(products));
                            unavailableSkusVector = Messages.GetProductDataResponse.CreateUnavailableSkusVector(builder,
                            unavailable.Select(sku => builder.CreateString(sku)).ToArray());

                            var productsVector = Messages.GetProductDataResponse.CreateProductsVector(builder,
                                products.Select(product => Messages.Product.CreateProduct(builder,
                                    builder.CreateString(SafeString(product.sku)),
                                    builder.CreateString(SafeString(product.productDescription)),
                                    builder.CreateString(PriceAsString(product)),
                                    builder.CreateString(product.productDetails == null ? "NULL" : SafeString(product.productDetails.productIconUrl)),
                                    builder.CreateString(SafeString(product.productTitle)),
                                    StringToProductType(product.type))).ToArray());

                            return Messages.GetProductDataResponse.CreateGetProductDataResponse(builder,
                                unavailableSkusVector, productsVector, Messages.GetProductDataResponseStatus.Success);
                        });
                }
                catch (InvalidCredentialsException)
                {
                    // Return a failure response
                    return new MessageFactory<Messages.GetProductDataResponse>(
                        builder =>
                        {
                            return Messages.GetProductDataResponse.CreateGetProductDataResponse(builder, status: Messages.GetProductDataResponseStatus.InvalidCredentials);
                        });
                }
                catch (UnsupportedCredentialsException ex)
                {
                    // There's several different things that can cause this.  Log the message so that it isn't lost.
                    LOG.Error("UnsupportedCredentials: " + ex.Message);

                    // Return a failure response
                    return new MessageFactory<Messages.GetProductDataResponse>(
                        builder =>
                        {
                            return Messages.GetProductDataResponse.CreateGetProductDataResponse(builder, status: Messages.GetProductDataResponseStatus.InvalidCredentials);
                        });
                }
                catch (CredentialsNotFoundException)
                {
                    // Return a failure response
                    return new MessageFactory<Messages.GetProductDataResponse>(
                        builder =>
                        {
                            return Messages.GetProductDataResponse.CreateGetProductDataResponse(builder, status: Messages.GetProductDataResponseStatus.CredentialsRequired);
                        });
                }
                catch (Exception)
                {
                    // Return a failure response
                    return new MessageFactory<Messages.GetProductDataResponse>(
                        builder =>
                        {
                            return Messages.GetProductDataResponse.CreateGetProductDataResponse(builder, status: Messages.GetProductDataResponseStatus.Failed);
                        });
                }


            }
        }

        Messages.ProductType StringToProductType(string productTypeString)
        {
            Messages.ProductType productType;
            LOG.Debug("productType = " + productTypeString);
            if (!Enum.TryParse(productTypeString, true, out productType))
            {
                // Could not parse to enum. Set to unknown.
                productType = Messages.ProductType.Unknown;
            }

            return productType;
        }

        ISet<string> SkusOfProducts(IEnumerable<Product> products)
        {
            HashSet<string> skus = new HashSet<string>();
            foreach (Product p in products)
            {
                if (p.sku != null)
                {
                    skus.Add(p.sku);
                }
            }
            return skus;
        }

        string PriceAsString(Product p)
        {
            if (p == null || p.productDetails == null || p.productDetails.currentPrice == null)
            {
                return "NULL";
            }
            Currency currentPrice = p.productDetails.currentPrice;

            // TODO FUEL-826: localization for currency formatting
            string unit = currentPrice.unit == null ? "" : currentPrice.unit;
            string amount = currentPrice.amount.ToString();
            return unit + amount;
        }

        Product SkuAsProduct(string sku)
        {
            Product product = new Product();
            product.vendor = new Vendor();
            product.vendor.id = AppContext.VendorId;
            product.sku = sku;

            return product;
        }

        IEnumerable<T> AsEnumerable<T>(int length, Func<int, T> accessor)
        {
            for (int i = 0; i < length; i++)
            {
                yield return accessor(i);
            }
        }

        string SafeString(string s)
        {
            return s == null ? "NULL" : s;
        }
    }
}
