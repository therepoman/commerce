﻿using FuelPump.Overlay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages = Fuel.Messaging;

namespace FuelPump.Handlers
{
    class InputHandler
    {
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(InputHandler));

        // Mouse move can generate a lot of events.
        private static readonly bool DEBUG_ENABLED = false;

        CefInstanceManager m_cef_manager;

        public InputHandler(CefInstanceManager manager)
        {
            m_cef_manager = manager;
        }

        public async Task KeyboardEvent(MessageContext context, Messages.KeyboardEvent message)
        {
            if (DEBUG_ENABLED)
            {
                LOG.Debug("Keyboard Event (" + message.InstanceId + ", " + message.Type + ", " + message.KeyCode + ", " + message.CtrlDown + ")");
            }

            CefInstance.KeyEventType eventType;
            switch (message.Type)
            {
                case Messages.KeyEventType.KeyDown:
                    eventType = CefInstance.KeyEventType.KeyDown;
                    break;
                case Messages.KeyEventType.KeyUp:
                    eventType = CefInstance.KeyEventType.KeyUp;
                    break;
                case Messages.KeyEventType.Char:
                    eventType = CefInstance.KeyEventType.Char;
                    break;
                default:
                    eventType = CefInstance.KeyEventType.Char;
                    break;
            }

            try
            {
                m_cef_manager.SendKey(message.InstanceId, eventType, (int)message.KeyCode, message.CtrlDown);
            }
            catch (CefInstanceManager.UnknownCefInstanceException)
            {
                LOG.Debug("KeyboardEvent received for non-existant cef instance " + message.InstanceId);
            }
        }

        public async Task MouseEvent(MessageContext context, Messages.MouseEvent message)
        {
            if (DEBUG_ENABLED)
            {
                LOG.Debug("MouseEvent (" + message.InstanceId + ", " + message.X + ", " + message.Y + ", " + message.Button + ", " + message.MouseUp + ")");
            }

            CefInstance.MouseButtonType buttonType;
            switch (message.Button)
            {
                case Messages.MouseButton.Left:
                    buttonType = CefInstance.MouseButtonType.Left;
                    break;
                case Messages.MouseButton.Middle:
                    buttonType = CefInstance.MouseButtonType.Middle;
                    break;
                case Messages.MouseButton.Right:
                    buttonType = CefInstance.MouseButtonType.Right;
                    break;
                default:
                    buttonType = CefInstance.MouseButtonType.Left;
                    break;
            }

            try
            {
                m_cef_manager.SendMouseEvent(message.InstanceId,
                    message.X, message.Y, buttonType, message.MouseUp, message.ClickCount);
            }
            catch (CefInstanceManager.UnknownCefInstanceException)
            {
                LOG.Debug("MouseEvent received for non-existant cef instance " + message.InstanceId);
            }
        }

        public async Task MouseMoveEvent(MessageContext context, Messages.MouseMoveEvent message)
        {
            if (DEBUG_ENABLED)
            {
                LOG.Debug("MouseMoveEvent (" + message.InstanceId + ", " + message.X + ", " + message.Y + ", " + message.LeftMouseDown + ")");
            }

            try
            {
                m_cef_manager.SendMouseMoveEvent(message.InstanceId, message.X, message.Y, false, message.LeftMouseDown);
            }
            catch (CefInstanceManager.UnknownCefInstanceException)
            {
                LOG.Debug("MouseMoveEvent received for non-existant cef instance " + message.InstanceId);
            }
        }

        public async Task MouseWheelEvent(MessageContext context, Messages.MouseWheelEvent message)
        {
            if (DEBUG_ENABLED)
            {
                LOG.Debug("MouseWheelEvent (" + message.InstanceId + ", " + message.X + ", " + message.Y + ", " + message.DeltaX + ", " + message.DeltaY + ")");
            }

            try
            {
                m_cef_manager.SendMouseWheelEvent(message.InstanceId,
                    message.X, message.Y, message.DeltaX, message.DeltaY);
            }
            catch (CefInstanceManager.UnknownCefInstanceException)
            {
                LOG.Debug("MouseWheelEvent received for non-existant cef instance " + message.InstanceId);
            }
        }

        public async Task CloseOverlayEvent(MessageContext context, Messages.CloseOverlayEvent message)
        {
            if (DEBUG_ENABLED)
            {
                LOG.Debug("CloseOverlayEvent (" + message.InstanceId + ")");
            }

            try
            {
                m_cef_manager.SendCloseOverlayEvent(message.InstanceId);
            }
            catch (CefInstanceManager.UnknownCefInstanceException)
            {
                LOG.Debug("CloseOverlayEvent received for non-existant cef instance " + message.InstanceId);
            }
        }
    }
}
