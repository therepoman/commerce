﻿
using Fuel.Messaging;
using FuelPump.Helpers;
using FuelPump.Providers.Credentials;
using FuelPump.Services;
using FuelPump.Services.Config;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Handlers
{
    public abstract class AbstractHandler
    {
        private static readonly string ACCEPT_HEADER = "application/vnd.twitchtv.v3+json";
        private static readonly string VALID = "True";

        protected readonly ConfigHandler configHandler;

        protected AbstractHandler(ConfigHandler configHandler)
        {
            this.configHandler = configHandler;
        }

        /// <summary>
        /// Convert seconds since January 1, 1970 to a DateTime
        /// </summary>
        /// <param name="epochTime"></param>
        /// <returns></returns>
        protected DateTime ToDateTime(UInt64 epochTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(epochTime);
        }

        /// <summary>
        /// Convert a DateTime to seconds since January 1, 1970
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        protected UInt64 ToEpochTime(DateTime dateTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            if (dateTime < epoch)
            {
                return 0;
            }
            return Convert.ToUInt64((dateTime - epoch).TotalSeconds);
        }

        /// <summary>
        /// Returns BearerCredentials which can be used later on, or null if the Twitch credentials are invalid
        /// </summary>
        /// <param name="credentials">The Message CredentialsCache</param>
        /// <returns>Twitch creds in BearerCredentials form for later use</returns>
        protected async Task<BearerCredentials> IsValidTwitchCredentials(CredentialsCache credentials)
        {
            BearerCredentials identifyingCredentials;
            try
            {
                identifyingCredentials = HandlerCredentialsHelper.ExtractSingleCredentials(credentials);
            }
            catch (UnsupportedCredentialsException)
            {
                return null;
            }

            try
            {
                if (await IsTwitchTokenValid(identifyingCredentials.Token))
                {
                    return identifyingCredentials;
                }
                else
                {
                    return null;
                }
            }
            catch (ApiConfig.DisabledOperationException)
            {
                // The early rejection of bad tokens is disabled.  Failing open.
                // Purchases will still fail if the token is invalid, it will just fail much later on in the process.
                return identifyingCredentials;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Checks with Kraken to see if this token is indeed valid
        /// </summary>
        /// <param name="token">the Twitch OAuth token</param>
        /// <returns>true if valid, false otherwise</returns>
        private async Task<bool> IsTwitchTokenValid(string token)
        {
            var config = configHandler.GetConfig().validateTwitchToken;
            if (!config.IsEnabled())
            {
                throw new ApiConfig.DisabledOperationException("Twitch token validation is disabled");
            }

            HttpClient Client = new HttpClient(new LoggingHandler(new HttpClientHandler()));
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ACCEPT_HEADER));
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("OAuth", token);

            HttpResponseMessage response;
            try
            {
                response = await RetryHelper.Retry(() => Client.GetAsync(config.getUri()), config);
            }
            catch (Exception e)
            {
                if (ConfigHandler.IsPossiblyBadConfiguration(e))
                {
                    // Failed to connect to the server, the config might be bad.
                    configHandler.ReportApiCallOutcome("validateTwitchToken", false);
                }
                else
                {
                    // Don't know what happened, but the config is probably good.
                    configHandler.ReportApiCallOutcome("validateTwitchToken", true);
                }

                throw e;
            }

            if (!response.IsSuccessStatusCode)
            {
                return false;
            }

            string message = await response.Content.ReadAsStringAsync();
            JObject parsedMessage = JObject.Parse(message);
            JObject tokenObject = (JObject)parsedMessage["token"];
            string valid = tokenObject.GetValue("valid").ToString();

            return valid == VALID;
        }
    }
}
