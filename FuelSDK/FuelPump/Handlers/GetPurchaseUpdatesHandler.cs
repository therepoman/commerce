﻿using FuelPump.Helpers;
using FuelPump.Metrics;
using FuelPump.Providers.Credentials;
using FuelPump.Services;
using FuelPump.Services.ADG.Common.Model;
using FuelPump.Services.ADG.Entitlement;
using FuelPump.Services.ADG.Entitlement.Model;
using FuelPump.Services.ADG.Order;
using FuelPump.Services.ADG.Order.Model;
using FuelPump.Services.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Messages = Fuel.Messaging;

namespace FuelPump.Handlers
{
    class GetPurchaseUpdatesHandler : AbstractHandler
    {
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(GetPurchaseUpdatesHandler));

        protected AbstractCredentialsProvider CredentialsProvider;
        protected Metrics.MetricEventFactory Metrics;
        private OrderServiceClient OrderService;

        public GetPurchaseUpdatesHandler(Metrics.MetricEventFactory metrics, AbstractCredentialsProvider credentialsProvider, ConfigHandler configHandler) :
            base(configHandler)
        {
            Metrics = metrics;
            CredentialsProvider = credentialsProvider;
            OrderService = new OrderServiceClient(credentialsProvider, configHandler);
        }

        public async Task<MessageFactory<Messages.GetPurchaseUpdatesResponse>> GetPurchaseUpdates(MessageContext context, Messages.GetPurchaseUpdatesRequest message)
        {
            var identifyingCredentials = await IsValidTwitchCredentials(message.Credentials);
            if (identifyingCredentials == null)
            {
                return new MessageFactory<Messages.GetPurchaseUpdatesResponse>(builder =>
                {
                    return Messages.GetPurchaseUpdatesResponse.CreateGetPurchaseUpdatesResponse(builder,
                        status: Messages.GetPurchaseUpdatesResponseStatus.InvalidCredentials);
                });
            }

            using (var metricScope = Metrics.Create())
            {
                metricScope.MetricEvent.AddCounter(FuelPumpMetrics.APICall.GetPurchaseUpdates);

                EntitlementServiceClient service = new EntitlementServiceClient(CredentialsProvider, configHandler);
                SyncGoodsRequest srvReq = new SyncGoodsRequest();
                srvReq.client = new ClientInfo();
                srvReq.client.clientId = "Fuel";

                Messages.SyncPointData fuelSyncPointData = message.SyncPoint;
                srvReq.startSync = new SyncPointData(); // need to set the structure even if members are null
                if (fuelSyncPointData.SyncToken != null && fuelSyncPointData.SyncToken.Length > 0)
                {
                    srvReq.startSync.syncToken = fuelSyncPointData.SyncToken;
                    srvReq.startSync.syncPoint = fuelSyncPointData.Timestamp;
                }

                try
                {
                    var response = await service.SyncGoods(metricScope, srvReq, identifyingCredentials);

                    var receiptUpdates = new List<ReceiptUpdate>();

                    // Mark goods that are pending delivery as delivered
                    foreach (DigitalGood good in response.goods)
                    {
                        if (good.transactionState.IsPendingDelivery())
                        {
                            var receiptUpdate = new ReceiptUpdate();
                            receiptUpdate.receiptId = good.receiptId;
                            receiptUpdate.status = SetReceiptsFulfillmentStatusEnum.DELIVERED;
                            receiptUpdates.Add(receiptUpdate);
                        }
                    }

                    if (receiptUpdates.Count > 0)
                    {
                        try
                        {
                            await OrderService.SetReceiptsFulfillmentStatus(metricScope, receiptUpdates, identifyingCredentials);
                        }
                        catch (Exception)
                        {
                            // Log an error but let this continue since we can move straight to fulfilled and skip delivered
                            LOG.ErrorFormat("Failed to mark {0} receipts as delivered", receiptUpdates.Count);
                        }
                    }

                    return new MessageFactory<Messages.GetPurchaseUpdatesResponse>(
                        builder =>
                        {
                            FlatBuffers.VectorOffset updatesVectorOffset;
                            if (response.goods != null)
                            {
                                updatesVectorOffset = Messages.GetPurchaseUpdatesResponse.CreateUpdatesVector(builder,
                                    response.goods.Select(good => ToReceipt(builder, good)).ToArray());
                            }
                            else
                            {
                                updatesVectorOffset = Messages.GetPurchaseUpdatesResponse.CreateUpdatesVector(builder, new FlatBuffers.Offset<Messages.Receipt>[0]);
                            }

                            FlatBuffers.Offset<Messages.SyncPointData> nextSyncPointOffset;
                            if (response.nextSync != null)
                            {
                                nextSyncPointOffset = Messages.SyncPointData.CreateSyncPointData(builder,
                                    builder.CreateString(response.nextSync.syncToken),
                                    response.nextSync.syncPoint);
                            }
                            else
                            {
                                nextSyncPointOffset = Messages.SyncPointData.CreateSyncPointData(builder);
                            }
                            return Messages.GetPurchaseUpdatesResponse.CreateGetPurchaseUpdatesResponse(builder,
                                nextSyncPointOffset, updatesVectorOffset, Messages.GetPurchaseUpdatesResponseStatus.Success);
                        });
                }
                catch (UnsupportedCredentialsException ex)
                {
                    // There's several different things that can cause this.  Log the message so that it isn't lost.
                    LOG.Error("UnsupportedCredentials", ex);

                    return new MessageFactory<Messages.GetPurchaseUpdatesResponse>(
                        builder =>
                        {
                            return Messages.GetPurchaseUpdatesResponse.CreateGetPurchaseUpdatesResponse(builder, status: Messages.GetPurchaseUpdatesResponseStatus.InvalidCredentials);
                        });
                }
                catch (CredentialsNotFoundException e)
                {
                    LOG.Error("Missing credentials. Credentials not found", e);
                    return new MessageFactory<Messages.GetPurchaseUpdatesResponse>(
                        builder =>
                        {
                            return Messages.GetPurchaseUpdatesResponse.CreateGetPurchaseUpdatesResponse(builder, status: Messages.GetPurchaseUpdatesResponseStatus.CredentialsRequired);
                        });
                }
                catch (TimeoutException e)
                {
                    LOG.Error("timed out", e);
                    return new MessageFactory<Messages.GetPurchaseUpdatesResponse>(
                        builder =>
                        {
                            return Messages.GetPurchaseUpdatesResponse.CreateGetPurchaseUpdatesResponse(builder, status: Messages.GetPurchaseUpdatesResponseStatus.TimedOut);
                        });
                }
                catch (Exception e)
                {
                    LOG.Error("Encountered exception. Request failed", e);

                    return new MessageFactory<Messages.GetPurchaseUpdatesResponse>(
                        builder =>
                        {
                            return Messages.GetPurchaseUpdatesResponse.CreateGetPurchaseUpdatesResponse(builder, status: Messages.GetPurchaseUpdatesResponseStatus.Failed);
                        });
                }
            }

        }

        private FlatBuffers.Offset<Messages.Receipt> ToReceipt(FlatBuffers.FlatBufferBuilder builder, DigitalGood good)
        {
            ulong purchaseDate = 0;
            if (good.origin != null && good.origin.originTimestamp != null)
            {
                purchaseDate = (ulong) good.origin.originTimestamp;
            }

            ulong cancelDate = 0;
            if (good.transactions != null)
            {
                foreach (var transaction in good.transactions)
                {
                    if (transaction != null && transaction.txType.IsCancelled() && transaction.txTimestamp != null)
                    {
                        cancelDate = ToEpochTime(transaction.txTimestamp);
                        break;
                    }
                }
            }

            Messages.ProductType productType = Fuel.Messaging.ProductType.Unknown;
            FlatBuffers.StringOffset sku = default(FlatBuffers.StringOffset);
            if (good.product != null)
            {
                // See ProductTypeEnum in https://code.amazon.com/packages/ADGCommonModel/blobs/mainline/--/model/adg-common.xml
                if (!Enum.TryParse(good.product.type, true, out productType))
                {
                    productType = Fuel.Messaging.ProductType.Unknown;
                }

                if (good.product.sku != null)
                {
                    sku = builder.CreateString(good.product.sku);
                }
            }

            string receiptId = good.receiptId == null ? "" : good.receiptId;

            return Messages.Receipt.CreateReceipt(builder,
                builder.CreateString(receiptId), sku, productType, purchaseDate,
                cancelDate);
        }
    }
}
