﻿using FlatBuffers;
using FuelPump.Providers.Credentials;
using FuelPump.Services;
using FuelPump.Services.ADG.Order;
using FuelPump.Services.ADG.Order.Model;
using FuelPump.Services.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Messages = Fuel.Messaging;

namespace FuelPump.Handlers
{
    class NotifyFulfillmentHandler
    {
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(NotifyFulfillmentHandler));
        private static readonly int MAX_RECEIPT_ID_LENGTH = 1024;

        private Metrics.MetricEventFactory m_metrics;
        protected AbstractCredentialsProvider CredentialsProvider;
        private static readonly string ERROR = "Error";
        private ConfigHandler configHandler;

        public NotifyFulfillmentHandler(Metrics.MetricEventFactory metrics, AbstractCredentialsProvider credentialsProvider, ConfigHandler configHandler)
        {
            m_metrics = metrics;
            this.CredentialsProvider = credentialsProvider;
            this.configHandler = configHandler;
        }

        public async Task<MessageFactory<Messages.NotifyFulfillmentResponse>> NotifyFulfillment(MessageContext context, Messages.NotifyFulfillmentRequest message)
        {
            using (var metricScope = m_metrics.Create())
            {
                metricScope.MetricEvent.AddCounter(FuelPumpMetrics.APICall.NotifyFulfillment);

                // The request serializer will properly quote the unicode.  This just makes sure the receipt id exists and isn't huge.
                if (string.IsNullOrWhiteSpace(message.ReceiptId) || message.ReceiptId.Length > MAX_RECEIPT_ID_LENGTH)
                {
                    return new MessageFactory<Messages.NotifyFulfillmentResponse>(
                        builder =>
                        {
                            return Messages.NotifyFulfillmentResponse.CreateNotifyFulfillmentResponse(builder, status: Messages.NotifyFulfillmentResponseStatus.InvalidArgument);
                        });
                }

                OrderServiceClient service = new OrderServiceClient(CredentialsProvider, configHandler);

                var receiptUpdate = new ReceiptUpdate();
                receiptUpdate.receiptId = message.ReceiptId;
                receiptUpdate.status = GetFulfillmentStatus(message);

                try
                {
                    BearerCredentials identifyingCredentials = HandlerCredentialsHelper.ExtractSingleCredentials(message.Credentials);

                    var receipt = await service.SetReceiptFulfillmentStatus(metricScope, receiptUpdate, identifyingCredentials);

                    return new MessageFactory<Messages.NotifyFulfillmentResponse>(
                        builder =>
                        {
                            if (receipt.requestResult != ERROR)
                            {
                                return Messages.NotifyFulfillmentResponse.CreateNotifyFulfillmentResponse(builder,
                                    builder.CreateString(receipt.requestResult),
                                    Messages::NotifyFulfillmentResponseStatus.Success);
                            }
                            else
                            {
                                return CreateRefundErrorResponse(builder, receipt.refundError);
                            }
                        });
                }
                catch (UnsupportedCredentialsException ex)
                {
                    // There's several different things that can cause this.  Log the message so that it isn't lost.
                    LOG.Error("UnsupportedCredentials: " + ex.Message);

                    return new MessageFactory<Messages.NotifyFulfillmentResponse>(
                        builder =>
                        {
                            return Messages.NotifyFulfillmentResponse.CreateNotifyFulfillmentResponse(builder, status: Messages.NotifyFulfillmentResponseStatus.InvalidCredentials);
                        });
                }
                catch (CredentialsNotFoundException)
                {
                    return new MessageFactory<Messages.NotifyFulfillmentResponse>(
                        builder =>
                        {
                            return Messages.NotifyFulfillmentResponse.CreateNotifyFulfillmentResponse(builder, status: Messages.NotifyFulfillmentResponseStatus.CredentialsRequired);
                        });
                }
                catch (Exception)
                {
                    return new MessageFactory<Messages.NotifyFulfillmentResponse>(
                        builder =>
                        {
                            return Messages.NotifyFulfillmentResponse.CreateNotifyFulfillmentResponse(builder, status: Messages.NotifyFulfillmentResponseStatus.Failed);
                        });
                }

            }
        }

        private SetReceiptsFulfillmentStatusEnum GetFulfillmentStatus(Messages.NotifyFulfillmentRequest message)
        { 
            switch (message.FulfillmentResult)
            {
                case Messages.FulfillmentResult.Fulfilled:
                    return SetReceiptsFulfillmentStatusEnum.FULFILLED;
                case Messages.FulfillmentResult.Unavailable:
                    return SetReceiptsFulfillmentStatusEnum.CANNOT_FULFILL;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private Offset<Messages.NotifyFulfillmentResponse> CreateRefundErrorResponse(FlatBufferBuilder builder, string error)
        {
            switch(error)
            {
                case "NotFoundError":
                case "AlreadyRefundedError":
                    return Messages.NotifyFulfillmentResponse.CreateNotifyFulfillmentResponse(builder,
                                    status: Messages::NotifyFulfillmentResponseStatus.NonRetryableError);
                default:
                    return Messages.NotifyFulfillmentResponse.CreateNotifyFulfillmentResponse(builder,
                                    status: Messages::NotifyFulfillmentResponseStatus.Failed);
            }
        }
    }
}
