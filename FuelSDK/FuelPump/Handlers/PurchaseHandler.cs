using FuelPump.Helpers;
using FuelPump.Overlay;
using FuelPump.Providers.Credentials;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Messages = Fuel.Messaging;
using System.Diagnostics;
using FuelPump.Services;
using FuelPump.Services.ADG.Order;
using FuelPump.Services.ADG.Order.Model;
using FuelPump.Services.Config;
using FuelPump.Metrics;
using static FuelPump.Helpers.AuthenticationHelper;

namespace FuelPump.Handlers
{
    public class PurchaseHandler : AbstractHandler
    {
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(PurchaseHandler));
        public static readonly string PROCESSING_EVENT_NAME = "Processing";

        private AuthenticationHelper authenticationHelper;
        private ValidationHelper validationHelper;
        private MetricEventFactory metricEventFactory;
        private OrderServiceClient orderService;
        private IApplicationContext AppContext;
        CefInstanceManager cefManager;
        AppInfo appInfo;

        public PurchaseHandler(Metrics.MetricEventFactory metrics, CefInstanceManager manager, AppInfo appInfo,
            AuthenticationHelper authHelper, ValidationHelper validationHelper, OrderServiceClient orderServiceClient,
            IApplicationContext context, ConfigHandler configHandler) : base(configHandler)
        {
            metricEventFactory = metrics;
            cefManager = manager;
            this.appInfo = appInfo;
            authenticationHelper = authHelper;
            this.validationHelper = validationHelper;
            orderService = orderServiceClient;
            AppContext = context;
        }

        public async Task<MessageFactory<Messages.PurchaseResponse>> Purchase(MessageContext context, Messages.PurchaseRequest message)
        {
            var identifyingCredentials = await IsValidTwitchCredentials(message.Credentials);
            if (identifyingCredentials == null)
            {
                return CreateResponse(Messages.PurchaseResponseStatus.InvalidCredentials);
            }

            if (!validationHelper.IsSkuValid(message.Sku))
            {
                return CreateResponse(Messages.PurchaseResponseStatus.InvalidArgument);
            }

            try
            {
                using (var instance = cefManager.CreateInstance())
                using (var metricScope = metricEventFactory.Create())
                {
                    metricScope.MetricEvent.AddCounter(FuelPumpMetrics.APICall.Purchase);

                    AmazonCredentials amazonCredentials = null;
                    try
                    {
                        // This is created early so that events aren't missed if they happen before the main flow
                        // finishes setting up everything, and the overlay will be able to react immediately as long
                        // as all the slow work is done using CefInstance.WrapPageLoad.
                        var overlayEvents = instance.RegisterOverlayEvents();

                        await instance.ShowLoadingPage();
                        ToggleOverlay(context, true, instance);

                        instance.AddDataToHeader("twitch_token", identifyingCredentials.Token);

                        var receiptTask = instance.WaitForAllEvents("Receipt");
                        var wrappedReceiptTask = CefInstance.WrapPageLoad(receiptTask, overlayEvents);

                        for (int i = 0; i <= configHandler.GetConfig().urlPathBlacklistMaxRedirects && !wrappedReceiptTask.IsCompleted; i++)
                        {
                            // Throw out creds and repeat once if the exchange fails
                            for (int j = 0; j < 2; j++)
                            {
                                var getTokenTask = authenticationHelper.GetAmazonCredentials(instance, identifyingCredentials);
                                amazonCredentials = await CefInstance.WrapPageLoad(getTokenTask, overlayEvents);
                                try
                                {
                                    await authenticationHelper.InjectAmazonCookies(instance, amazonCredentials);
                                    break;
                                }
                                catch (AmazonAuthenticationFailedException)
                                {
                                    LOG.Info("Failed to get Amazon cookies. Begin CBL");
                                    // No need to go through ClearCredentialsCacheIfCredentialsAreBad here, as we already know the exchange failed.
                                    authenticationHelper.ClearCredentialsCache(identifyingCredentials);
                                }
                            }

                            instance.AddDataToHeader("vendor_id", AppContext.VendorId);

                            Task<string> onBlacklistedUrl = instance.WaitForBlacklistedUrl();
                            var processingTask = instance.WaitForAllEvents(PROCESSING_EVENT_NAME).ContinueWith(task =>
                            {
                                return task.Result[0];
                            });

                            // User is authenticated, starting purchase workflow
                            instance.Navigate(configHandler.GetConfig().purchasePage.getUri("sku=" + HttpUtility.UrlEncode(message.Sku)));

                            var completedTask = await Task.WhenAny(wrappedReceiptTask, onBlacklistedUrl, processingTask);
                            if (completedTask == onBlacklistedUrl)
                            {
                                var url = onBlacklistedUrl.Result;
                                LOG.InfoFormat("A blacklisted URL was requested. Request URL: {0}", url);

                                bool invalid = await HandleBadCredentials(instance, identifyingCredentials, amazonCredentials, url);

                                // Only return if the credentials were bad. Otherwise, retry.
                                if (invalid)
                                {
                                    return CreateResponse(Messages.PurchaseResponseStatus.InvalidCredentials);
                                }
                            }
                            else if(completedTask == processingTask)
                            {
                                return CreateResponse(Messages.PurchaseResponseStatus.Processing);
                            }
                        } 
                        
                        if (!wrappedReceiptTask.IsCompleted)
                        {
                            LOG.Error("Purchase failed due to bad URLs");
                            return CreateResponse(Messages.PurchaseResponseStatus.Failed);
                        }

                        // await the task so that it can throw exceptions.
                        await wrappedReceiptTask;

                        var receipt = await receiptTask;

                        // Remove header info as it is no longer required
                        instance.RemoveDataFromHeader("twitch_token");
                        instance.RemoveDataFromHeader("vendor_id");

                        ToggleOverlay(context, false, instance);

                        // Parse out all the events we got back from the website.
                        var receiptId = receipt[0].EventData;

                        // Mark as delivered
                        try
                        {
                            await MarkReceiptDelivered(metricScope, receiptId, identifyingCredentials);
                        }
                        catch (Exception)
                        {
                            // Log an error but let this continue since we can move straight to fulfilled and skip delivered
                            LOG.ErrorFormat("Failed to mark receipt '{0}' as delivered", receiptId);
                        }

                        return new MessageFactory<Messages.PurchaseResponse>(
                            builder =>
                            {
                                var receiptIdOffset = builder.CreateString(receiptId);
                                var receiptOffset = Messages.Receipt.CreateReceipt(builder,
                                        receiptId: receiptIdOffset,
                                        cancelDate: 0);
                                return Messages.PurchaseResponse.CreatePurchaseResponse(builder, receiptOffset, Messages.PurchaseResponseStatus.Success);
                            });
                    }
                    catch (Helpers.AuthenticationHelper.TwitchAuthenticationFailedException)
                    {
                        LOG.Error("Purchase failed due to TwitchAuthenticationFailedException");
                        return CreateResponse(Messages.PurchaseResponseStatus.InvalidCredentials);
                    }
                    catch (Helpers.AuthenticationHelper.AmazonAuthenticationFailedException)
                    {
                        LOG.Error("Purchase failed due to AmazonAuthenticationFailedException");

                        if (amazonCredentials != null)
                        {
                            await HandleBadCredentials(instance, identifyingCredentials, amazonCredentials, "");
                        }

                        return CreateResponse(Messages.PurchaseResponseStatus.InvalidCredentials);
                    }
                    catch (Helpers.AuthenticationHelper.UnlinkedAccountsException)
                    {
                        LOG.Error("Purchase failed due to UnlinkedAccountsException");

                        authenticationHelper.ClearCredentialsCache(identifyingCredentials);
                        var overlayEvents = instance.RegisterOverlayEvents();
                        instance.Navigate(configHandler.GetConfig().unlinkedAccountsPage.getUri());
                        try
                        {
                            await CefInstance.WrapPageLoad(overlayEvents.CloseButtonClicked, overlayEvents);
                        }
                        catch (OperationCanceledException)
                        {
                            // Nothing to do, user clicked the close button.
                        }
                        catch (Exception ex)
                        {
                            LOG.Error("The unlinked accounts page also failed", ex);
                        }

                        return CreateResponse(Messages.PurchaseResponseStatus.Failed);
                    }
                    catch (Exception ex) when (ex is Helpers.AuthenticationHelper.CblCancelledException || ex is OperationCanceledException)
                    {
                        return CreateResponse(Messages.PurchaseResponseStatus.Cancelled);
                    }
                    catch (CefInstance.PageLoadException error)
                    {
                        LOG.Error("Purchase failed due to PageLoadException, showing error page");
                        var overlayEvents = instance.RegisterOverlayEvents();
                        await instance.ShowErrorPage(FuelPump.Properties.Resources.ErrorMessage + " (" + error.Error.ErrorCode.ToString() + ").");
                        try
                        {
                            await CefInstance.WrapPageLoad(overlayEvents.CloseButtonClicked, overlayEvents);
                        }
                        catch (OperationCanceledException)
                        {
                            // Nothing to do, user clicked the close button.
                        }
                        catch (Exception ex)
                        {
                            LOG.Error("The error page also failed", ex);
                        }
                        return CreateResponse(Messages.PurchaseResponseStatus.Failed);
                    }
                    catch (CefInstance.PageLoadTimeoutException)
                    {
                        LOG.Error("Page load timeout, showing error page");
                        var overlayEvents = instance.RegisterOverlayEvents();
                        await instance.ShowErrorPage(FuelPump.Properties.Resources.NetworkTimeoutErrorMessage);
                        try
                        {
                            await CefInstance.WrapPageLoad(overlayEvents.CloseButtonClicked, overlayEvents);
                        }
                        catch (OperationCanceledException)
                        {
                            // Nothing to do, user clicked the close button.
                        }
                        catch (Exception ex)
                        {
                            LOG.Error("The error page also failed", ex);
                        }
                        return CreateResponse(Messages.PurchaseResponseStatus.Failed);
                    }
                    catch (CefInstance.OverlayFailureException)
                    {
                        LOG.Error("Purchase failed due to an internal overlay failure");
                        return CreateResponse(Messages.PurchaseResponseStatus.Failed);
                    }
                    finally
                    {
                        ToggleOverlay(context, false, instance);
                    }
                }
            }
            catch (CefInstanceManager.InstanceCreationFailedException)
            {
                return CreateResponse(Messages.PurchaseResponseStatus.Failed);
            }
        }

        private async Task<bool> HandleBadCredentials(CefInstance instance, BearerCredentials identifyingCredentials, AmazonCredentials amazonCredentials, string url)
        {

            if (url.EndsWith("/logout"))
            {
                // The user explicitly logged out. Therefore, we need to clear the cache, so the user can get new ones.
                authenticationHelper.ClearCredentialsCache(identifyingCredentials);
                return false;
            }

            // Clear the credentials cache if the creds are bad.
            Boolean badCreds = await authenticationHelper.ClearCredentialsCacheIfCredentialsAreBad(identifyingCredentials, amazonCredentials);

            var overlayEvents = instance.RegisterOverlayEvents();
            if (!badCreds)
            {
                // The credentials were not bad, so the user should be able to successfully retry.
                instance.Navigate(configHandler.GetConfig().expiredCredentialsPage.getUri());
                try
                {
                    await CefInstance.WrapPageLoad(overlayEvents.CloseButtonClicked, overlayEvents);
                }
                catch (OperationCanceledException)
                {
                    // Nothing to do, user clicked the close button.
                }
                catch (Exception ex)
                {
                    LOG.Error("The token expired page also failed", ex);
                }
            }

            return true;
        }

        private void ToggleOverlay(MessageContext context, bool showOverlay, CefInstance instance)
        {
            FlatBuffers.FlatBufferBuilder headerBuilder = new FlatBuffers.FlatBufferBuilder(1);
            string RequestId = context.RequestId;
            var headerRoot = Fuel.Messaging.MessageHeader.CreateMessageHeader(headerBuilder,
                requestId: headerBuilder.CreateString(RequestId),
                type: (int)(showOverlay ? Fuel.Messaging.Message.ShowOverlay : Fuel.Messaging.Message.HideOverlay));
            headerBuilder.Finish(headerRoot.Value);

            FlatBuffers.FlatBufferBuilder dataBuilder = new FlatBuffers.FlatBufferBuilder(1);

            if (showOverlay)
            {
                Rectangle rect = instance.GetRect();
                var payload = Messages.ShowOverlay.CreateShowOverlay(dataBuilder,
                    instanceId: instance.GetInstanceId(),
                    x: rect.X,
                    y: rect.Y,
                    width: rect.Width,
                    height: rect.Height);
                var payloadRoot = Messages.MessageRoot.CreateMessageRoot(dataBuilder,
                    message_type: Messages.Message.ShowOverlay,
                    message: payload.Value);
                dataBuilder.Finish(payloadRoot.Value);
            }
            else
            {
                var payload = Messages.HideOverlay.CreateHideOverlay(dataBuilder, instanceId: instance.GetInstanceId());
                var payloadRoot = Messages.MessageRoot.CreateMessageRoot(dataBuilder,
                    message_type: Messages.Message.HideOverlay,
                    message: payload.Value);
                dataBuilder.Finish(payloadRoot.Value);
            }

            Message toggleOverlayMessage = new Message(headerBuilder.SizedByteArray(), dataBuilder.SizedByteArray());
            context.ProcessMessage(toggleOverlayMessage);
        }

        private async Task<Receipt> MarkReceiptDelivered(MetricEventFactory.MetricEventScope metrics,
            string receiptId, BearerCredentials identifyingCredentials)
        {
            var receiptUpdate = new ReceiptUpdate();
            receiptUpdate.receiptId = receiptId;
            receiptUpdate.status = SetReceiptsFulfillmentStatusEnum.DELIVERED;
            return await orderService.SetReceiptFulfillmentStatus(metrics, receiptUpdate, identifyingCredentials);
        }

        private MessageFactory<Messages.PurchaseResponse> CreateResponse(Messages.PurchaseResponseStatus status)
        {
            return new MessageFactory<Messages.PurchaseResponse>(
                builder =>
                {
                    return Messages.PurchaseResponse.CreatePurchaseResponse(builder, status: status);
                });
        }
    }
}
