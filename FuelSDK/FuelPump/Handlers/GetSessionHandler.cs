﻿using Amazon.MCP.Common.Contracts;
using System;
using System.Management;
using System.Diagnostics;
using System.Threading.Tasks;

using Messages = Fuel.Messaging;
using FuelPump.Services.Config;
using Clients.Kraken;

namespace FuelPump.Handlers
{
    public class GetSessionHandler : AbstractHandler, ITwitchTokenProvider
    {
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(GetSessionHandler));
        private static readonly TimeSpan ACCESS_TOKEN_LIFESPAN = TimeSpan.FromMinutes(4*60-10);

        private ITwitchUserStoragePlugin _twitchUserStorage;
        private IdentityClient _twitchIdentityClient;
        private readonly IClock _clock;

        private DateTime nextRefresh = DateTime.MinValue;

        private uint parent = 0;
        private uint ParentProcessId
        {
            get
            {
                if (parent != 0) return parent;
                var myId = Process.GetCurrentProcess().Id;
                var query = string.Format("SELECT ParentProcessId FROM Win32_Process WHERE ProcessId = {0}", myId);
                var search = new ManagementObjectSearcher("root\\CIMV2", query);
                var results = search.Get().GetEnumerator();
                if (results == null || !results.MoveNext())
                {
                    return 0;
                }

                var queryObj = results.Current;
                parent = (uint)queryObj["ParentProcessId"];

                return parent;
            }
        }

        public GetSessionHandler(ConfigHandler configHandler, ITwitchUserStoragePlugin twitchUserStorage, IdentityClient twitchIdentityClient, IClock clock) : base(configHandler)
        {
            _twitchUserStorage = twitchUserStorage;
            _twitchIdentityClient = twitchIdentityClient;
            _clock = clock;
        }

        public async Task<MessageFactory<Messages.GetSessionResponse>> GetSession(MessageContext context, Messages.GetSessionRequest message)
        {
            var twitchUser = _twitchUserStorage.User;

            TwitchCredentialSet twitchCreds = null;
            // It may take a bit for the launcher to populate the session information in the registry
            for (int i = 0; i < 20 && twitchCreds == null; i++)
            {
                twitchCreds = _twitchUserStorage.GetTwitchCredentialSet(message.ClientId);
                await Task.Delay(250);
            }

            if (twitchUser == null || twitchCreds == null)
            {
                LOG.DebugFormat("No session found for twitch user and client id {0}", message.ClientId);
                return new MessageFactory<Messages.GetSessionResponse>(builder =>
                {
                    return Messages.GetSessionResponse.CreateGetSessionResponse(builder, status: Fuel.Messaging.GetSessionResponseStatus.Failed);
                });
            }

            if (twitchCreds.Created.Add(ACCESS_TOKEN_LIFESPAN) <= _clock.Now().ToLocalTime())
            {
                // Refresh the token
                // refresh(); // refresh launcher token
                TokenResponse tokenResponse;
                try
                {
                    tokenResponse = await _twitchIdentityClient.FetchGameToken(message.ClientId, this, twitchCreds.Scopes);
                }
                catch (Exception e)
                {
                    LOG.Error("Failed to exchange token.", e);
                    return new MessageFactory<Messages.GetSessionResponse>(builder =>
                    {
                        return Messages.GetSessionResponse.CreateGetSessionResponse(builder, status: Fuel.Messaging.GetSessionResponseStatus.Failed);
                    });
                }
                
                twitchCreds.AccessToken = tokenResponse.token;
                twitchCreds.Created = _clock.Now().ToLocalTime();
                _twitchUserStorage.SetTwitchCredentialSet(message.ClientId, twitchCreds);
            }

            ulong accessTokenCreationTimestamp = ToEpochTime(twitchCreds.Created);

            return new MessageFactory<Messages.GetSessionResponse>(builder =>
            {
                FlatBuffers.Offset<Messages.TwitchUser> twitchUserOffset;
                twitchUserOffset = Messages.TwitchUser.CreateTwitchUser(builder,
                    builder.CreateString(twitchCreds.AccessToken ?? ""),
                    builder.CreateString(twitchUser.Logo ?? ""),
                    builder.CreateString(twitchUser.Username ?? ""),
                    builder.CreateString(twitchUser.DisplayName ?? ""),
                    builder.CreateString(twitchUser.Tuid ?? ""),
                    accessTokenCreationTimestamp);
                var entitlementOffset = builder.CreateString(twitchCreds.Entitlement);
                return Messages.GetSessionResponse.CreateGetSessionResponse(builder, twitchUserOffset, entitlementOffset, Fuel.Messaging.GetSessionResponseStatus.Success);
            });
        }

        private async void refresh()
        {
            var refresh_token = _twitchUserStorage.User?.RefreshToken;
            if (refresh_token == null)
            {
                return;
            }

            var now = _clock.Now();

            if (nextRefresh < now)
            {
                this.nextRefresh = now.Add(ACCESS_TOKEN_LIFESPAN);
                var refreshResponse = await _twitchIdentityClient.Refresh(refresh_token);
                setAuthTokens(refreshResponse.access_token, refreshResponse.refresh_token);
            }
        }

        private void setAuthTokens(string access_token, string refresh_token)
        {
            var user = _twitchUserStorage.User;
            user.RefreshToken = refresh_token;
            user.AccessToken = access_token;
            _twitchUserStorage.User = (user);
        }

        public string OAuthToken()
        {
            return  _twitchUserStorage.User.AccessToken;
        }

        public void AuthFailed()
        {
            //NOOP
        }
    }
}
