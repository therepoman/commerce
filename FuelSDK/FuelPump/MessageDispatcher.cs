﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fuel.Messaging;
using System.Diagnostics;

namespace FuelPump
{
    public class MessageDispatcher
    {
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(MessageDispatcher));

        public delegate Task RequestHandler(MessageContext context, Message message);
        public delegate Task<Message> RequestResponseHandler(MessageContext context, Message message);

        public delegate Task RequestHandler<RequestType>(MessageContext context, RequestType message)
            where RequestType : FlatBuffers.Table;

        public delegate Task<MessageFactory<ResponseType>> RequestResponseHandler<RequestType, ResponseType>(MessageContext context, RequestType message)
            where RequestType : FlatBuffers.Table
            where ResponseType : FlatBuffers.Table;

        public event EventHandler<UnhandledMessageEventArgs> UnhandledMessage;
        public event EventHandler<HandlerExceptionEventArgs> UnhandledException;

        private Dictionary<Type, RequestHandler> m_handlers = new Dictionary<Type, RequestHandler>();

        public Task ProcessMessage(Message message)
        {
            LOG.InfoFormat("Processing message of type {0} for request {1}", message.GetMessageType(), message.RequestId);
            RequestHandler typedHandler;

            if (m_handlers.TryGetValue(message.GetMessageType(), out typedHandler))
            {
                var messageContext = new MessageContext(message.RequestId, message.Retry, this);
                LOG.InfoFormat("Found handler for message type {0} request {1}", message.GetMessageType(), message.RequestId);
                return typedHandler.Invoke(messageContext, message);
            }

            if (UnhandledMessage != null)
            {
                LOG.ErrorFormat("No handler for message type {0} request {1}", message.GetMessageType(), message.RequestId);
                UnhandledMessage(this, new UnhandledMessageEventArgs(message));
            }

            return Task.FromResult(true);
        }

        public void RegisterHandler<RequestType>(RequestHandler handler) 
            where RequestType : FlatBuffers.Table, new()
        {
            m_handlers.Add(typeof(RequestType), async (context, message) => 
            {
                await SafeHandler(context, message, handler(context, message));
            });
        }

        public void RegisterHandler<RequestType, ResponseType>(RequestResponseHandler handler) 
            where RequestType : FlatBuffers.Table, new()
            where ResponseType : FlatBuffers.Table, new()
        {
            m_handlers.Add(typeof(RequestType), async (context, message) =>
            {
                await SafeHandler(context, message, handler(context, message));
            });
        }

        public void RegisterHandler<RequestType>(RequestHandler<RequestType> handler) 
            where RequestType : FlatBuffers.Table, new()
        {
            m_handlers.Add(typeof(RequestType), async (context, message) =>
            {
                var typedMessage = message.GetMessage<RequestType>();
                Debug.Assert(typedMessage != null);

                await SafeHandler(context, message, handler(context, typedMessage));
            });
        }

        public void RegisterHandler<RequestType, ResponseType>(RequestResponseHandler<RequestType, ResponseType> handler)
            where RequestType : FlatBuffers.Table, new()
            where ResponseType : FlatBuffers.Table, new()
        {
            m_handlers.Add(typeof(RequestType), async (context, message) =>
            { 
                var typedMessage = message.GetMessage<RequestType>();
                Debug.Assert(typedMessage != null);

                await SafeHandler<ResponseType>(context, message, handler(context, typedMessage));
            });
        }
        
        private async Task SafeHandler(MessageContext context, Message message, Task task)
        {
            Func<Task, Task<Message>> wrapper = async (t) =>
            {
                await t;
                return null;
            };

            await SafeHandler(context, message, wrapper(task));
        }

        private async Task SafeHandler<ResponseType>(MessageContext context, Message message, Task<MessageFactory<ResponseType>> task)
            where ResponseType: class
        { 
            Func<Task<MessageFactory<ResponseType>>, Task<Message>> wrapper = async (t) =>
            {
                var typedResponse = await t;
                return typedResponse.Create(context);
            };

            await SafeHandler(context, message, wrapper(task));
        }

        private async Task SafeHandler(MessageContext context, Message message, Task<Message> task)
        {
            Message response = null;
            try
            {
                response = await task;
            }
            catch (Exception ex)
            {
                if (UnhandledException != null)
                {
                    UnhandledException(this, new HandlerExceptionEventArgs(message, ex));
                }
            }

            if (response != null)
            { 
                await context.ProcessMessage(response);
            }
        }
    }
}
