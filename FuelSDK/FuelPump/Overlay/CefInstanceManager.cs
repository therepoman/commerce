﻿using FuelPump;
using FuelPump.Services.Config;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Messages = Fuel.Messaging;

namespace FuelPump.Overlay
{

    public class CefInstanceManager : Disposable
    {
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(CefInstanceManager));

        private static readonly int MIN_OVERLAY_SIZE_PIXELS = 100;
        private static readonly int MIN_SCREEN_SIZE_PIXELS = 100;
        private static readonly int MAX_SCREEN_SIZE_PIXELS = 4000;
        private static readonly float MAX_SIZE_RATIO_FOR_ABSOLUTE_MEASUREMENTS = 0.9F;
        private static readonly float DEFAULT_SIZE_RATIO = 0.8F;
        private static readonly int DEFAULT_DPI = 96;
        private static readonly int INSTANCE_CREATION_FAILED = 0;

        public struct DirtyRect
        {
            public int x;
            public int y;
            public int width;
            public int height;
        }

        public delegate void OnDrawDelegate([MarshalAs(UnmanagedType.I4)]int instanceId,
            [MarshalAs(UnmanagedType.I4)]int dirtyRectCount,
            [MarshalAs(UnmanagedType.LPArray, SizeParamIndex=1)]DirtyRect[] dirtyRects,
            [MarshalAs(UnmanagedType.I4)]int byteCount,
            IntPtr bytes,
            [MarshalAs(UnmanagedType.I4)]int width,
            [MarshalAs(UnmanagedType.I4)]int height);

        public delegate void OnEventDelegate([MarshalAs(UnmanagedType.I4)]int instanceId,
            [MarshalAs(UnmanagedType.LPWStr)]string name,
            [MarshalAs(UnmanagedType.LPWStr)]string data);

        public delegate void OnCursorChangedDelegate([MarshalAs(UnmanagedType.I4)]int instanceId,
            [MarshalAs(UnmanagedType.I4)]int type);

        public delegate void OnBeforeBrowseDelegate([MarshalAs(UnmanagedType.I4)]int instanceId);

        [return: MarshalAs(UnmanagedType.I4)]
        public delegate int OnBeforeResourceLoadDelegate([MarshalAs(UnmanagedType.I4)]int instanceId,
            [MarshalAs(UnmanagedType.LPWStr)]string url);

        public delegate void OnLoadEndDelegate([MarshalAs(UnmanagedType.I4)]int instanceId,
            [MarshalAs(UnmanagedType.I4)]int httpStatusCode);

        public delegate void OnLoadErrorDelegate([MarshalAs(UnmanagedType.I4)]int instanceId,
            [MarshalAs(UnmanagedType.I4)]int errorCode,
            [MarshalAs(UnmanagedType.LPWStr)]string errorText,
            [MarshalAs(UnmanagedType.LPWStr)]string failedUrl);

        public delegate void OnOpenInBrowserDelegate([MarshalAs(UnmanagedType.I4)]int instanceId,
            [MarshalAs(UnmanagedType.LPWStr)]string url);

        [return: MarshalAs(UnmanagedType.LPWStr)]
        public delegate string OnLoadLocalFileDelegate([MarshalAs(UnmanagedType.LPWStr)]string file);

        [DllImport("OffscreenBrowser.dll", EntryPoint = "CEFCreate")]
        static extern IntPtr CEFCreate([MarshalAs(UnmanagedType.I4)]int instanceId,
            [MarshalAs(UnmanagedType.I4)]int x,
            [MarshalAs(UnmanagedType.I4)]int y,
            [MarshalAs(UnmanagedType.I4)]int width,
            [MarshalAs(UnmanagedType.I4)]int height);

        [DllImport("OffscreenBrowser.dll", EntryPoint = "CEFOnDraw")]
        static extern void CEFOnDraw(OnDrawDelegate callback);

        [DllImport("OffscreenBrowser.dll")]
        static extern void CEFOnEvent(OnEventDelegate callback);

        [DllImport("OffscreenBrowser.dll")]
        static extern void CEFOnCursorChanged(OnCursorChangedDelegate callback);

        [DllImport("OffscreenBrowser.dll")]
        static extern void CEFOnBeforeBrowse(OnBeforeBrowseDelegate callback);

        [DllImport("OffscreenBrowser.dll")]
        static extern void CEFOnBeforeResourceLoad(OnBeforeResourceLoadDelegate callback);

        [DllImport("OffscreenBrowser.dll")]
        static extern void CEFOnLoadEnd(OnLoadEndDelegate callback);

        [DllImport("OffscreenBrowser.dll")]
        static extern void CEFOnLoadError(OnLoadErrorDelegate callback);

        [DllImport("OffscreenBrowser.dll")]
        static extern void CEFOnOpenInBrowser(OnOpenInBrowserDelegate callback);

        [DllImport("OffscreenBrowser.dll")]
        static extern void CEFOnLoadLocalFile(OnLoadLocalFileDelegate callback);

        public class UnknownCefInstanceException : System.Exception
        {
            public UnknownCefInstanceException() : base() { }
            public UnknownCefInstanceException(string message) : base(message) { }
            public UnknownCefInstanceException(string message, System.Exception inner) : base(message, inner) { }
        }

        public class InstanceCreationFailedException : System.Exception
        {
            public InstanceCreationFailedException() : base() { }
            public InstanceCreationFailedException(string message) : base(message) { }
            public InstanceCreationFailedException(string message, System.Exception inner) : base(message, inner) { }
        }

        Metrics.MetricEventFactory m_metrics;
        IMessageTransport m_transport;
        ConfigHandler m_configHandler;
        OnDrawDelegate m_onDraw;
        OnEventDelegate m_onEvent;
        OnCursorChangedDelegate m_onCursorChanged;
        OnBeforeBrowseDelegate m_onBeforeBrowse;
        OnBeforeResourceLoadDelegate m_onBeforeResourceLoad;
        OnLoadEndDelegate m_onLoadEnd;
        OnLoadErrorDelegate m_onLoadError;
        OnOpenInBrowserDelegate m_onOpenInBrowser;
        OnLoadLocalFileDelegate m_onLoadLocalFile;

        Dictionary<int, CefInstance> m_instances = new Dictionary<int, CefInstance>();
        ReaderWriterLockSlim m_instancesLock = new ReaderWriterLockSlim();

        int m_screenWidth = 640;
        int m_screenHeight = 480;

        private int instanceCtr = 0;

        public CefInstanceManager(IMessageTransport transport, Metrics.MetricEventFactory metrics, ConfigHandler configHandler)
        {
            m_metrics = metrics;
            m_transport = transport;
            m_configHandler = configHandler;
            m_onDraw = OnDraw;
            m_onEvent = OnEvent;
            m_onCursorChanged = OnCursorChanged;
            m_onBeforeBrowse = OnBeforeBrowse;
            m_onBeforeResourceLoad = OnBeforeResourceLoad;
            m_onLoadEnd = OnLoadEnd;
            m_onLoadError = OnLoadError;
            m_onOpenInBrowser = OnOpenInBrowser;
            m_onLoadLocalFile = OnLoadLocalFile;

            CEFOnDraw(m_onDraw);
            CEFOnEvent(m_onEvent);
            CEFOnCursorChanged(m_onCursorChanged);
            CEFOnBeforeBrowse(m_onBeforeBrowse);
            CEFOnBeforeResourceLoad(m_onBeforeResourceLoad);
            CEFOnLoadEnd(m_onLoadEnd);
            CEFOnLoadError(m_onLoadError);
            CEFOnOpenInBrowser(m_onOpenInBrowser);
            CEFOnLoadLocalFile(m_onLoadLocalFile);
        }

        protected override void Dispose(bool disposing)
        {
            // Iterate over a copy of m_instances to avoid modifying it while it's being iterated
            // CefInstance.Dispose will use a write lock since it doesn't know what triggered the dispose.
            // The lock is not reentrant, so this loop does not hold the lock.
            foreach (var item in GetAllInstances())
            {
                item.Value.Dispose();
            }
        }

        /// <summary>
        /// Called by CEF.
        /// If this method throws an exception, the FuelPump will crash from the unhandled exception.
        /// </summary>
        public void OnDraw(int instanceId, int dirtyRectCount, DirtyRect[] dirtyRects, int size, IntPtr bytes, int width, int height)
        {
            var instance = GetInstanceOrNull(instanceId);
            if (instance == null)
            {
                LOG.Error("Unknown instance ID " + instanceId + " in OnDraw");
                return;
            }

            Rectangle rect = instance.GetRect();

            FlatBuffers.FlatBufferBuilder headerBuilder = new FlatBuffers.FlatBufferBuilder(1);
            string RequestId = Guid.NewGuid().ToString();
            var headerRoot = Messages.MessageHeader.CreateMessageHeader(headerBuilder,
                requestId: headerBuilder.CreateString(RequestId),
                type: (int) Messages.Message.UpdateOverlay);
            headerBuilder.Finish(headerRoot.Value);

            // During SDK startup, CEF often provides an image update with the creation size after the resize call has completed.
            // CEF will then immediately send another update with the new size.
            // The width and height appear to be set independently: twice I saw CEF send an image update with the new width and the previous height.
            // CEF may also ignore a size update, and used to consistently do so if the new width matches the old width.
            // After disabling GPU rendering, the resize looks to be more stable.  Even though resize now synchronously calls OnDraw before returning,
            // it still often applies the new width/height afterward and triggers another OnDraw.
            if (rect.Width != width || rect.Height != height)
            {
                // Throw away the update.
                return;
            }


            // write the header to the pipe
            var header = headerBuilder.SizedByteArray();

            var payloadSize = sizeof(int) * 6; // instance id, x, y, width, height, rect count
            foreach (var dirtyRect in dirtyRects)
            {
                payloadSize += (4 * sizeof(int)) + (dirtyRect.width * dirtyRect.height * 4);
            }

            using (var writer = m_transport.WriteUntypedMessge(headerBuilder.SizedByteArray(), (uint)payloadSize))
            {
                // write block size
                writer.Write(instanceId);
                writer.Write(rect.X);
                writer.Write(rect.Y);
                writer.Write(width);
                writer.Write(height);
                writer.Write(dirtyRects.Length);

                foreach (var dirtyRect in dirtyRects)
                {
                    writer.Write(dirtyRect.x);
                    writer.Write(dirtyRect.y);
                    writer.Write(dirtyRect.width);
                    writer.Write(dirtyRect.height);

                    // write the dirty rect
                    var rectByteWidth = dirtyRect.width * 4;
                    var screenByteWidth = width * 4;
                    IntPtr current = IntPtr.Add(bytes, screenByteWidth * dirtyRect.y + (dirtyRect.x * 4)); 
                    for (int y = dirtyRect.y; y != dirtyRect.y + dirtyRect.height; y++)
                    {
                        writer.Write(current, (uint)rectByteWidth);
                        current = IntPtr.Add(current, screenByteWidth);
                    }
                }
            }
        }

        /// <summary>
        /// Called by CEF.
        /// If this method throws an exception, the FuelPump will crash from the unhandled exception.
        /// </summary>
        public void OnEvent(int instanceId, string name, string data)
        {
            var instance = GetInstanceOrNull(instanceId);
            if (instance == null)
            {
                LOG.Error("Unknown instance ID " + instanceId + " in OnEvent");
                return;
            }

            instance.ReceiveEvent(name, data);
        }

        /// <summary>
        /// Called by CEF.
        /// If this method throws an exception, the FuelPump will crash from the unhandled exception.
        /// </summary>
        public void OnCursorChanged(int instanceId, int type)
        {
            if (GetInstanceOrNull(instanceId) == null)
            {
                LOG.Error("Unknown instance ID " + instanceId + " in OnCursorChanged");
                return;
            }

            FlatBuffers.FlatBufferBuilder headerBuilder = new FlatBuffers.FlatBufferBuilder(1);
            string RequestId = Guid.NewGuid().ToString();
            var headerRoot = Messages.MessageHeader.CreateMessageHeader(headerBuilder,
                requestId: headerBuilder.CreateString(RequestId),
                type: (int)Messages.Message.UpdateCursor);
            headerBuilder.Finish(headerRoot.Value);

            FlatBuffers.FlatBufferBuilder payloadBuilder = new FlatBuffers.FlatBufferBuilder(1);
            var payload = Messages.UpdateCursor.CreateUpdateCursor(payloadBuilder, instanceId: instanceId, cursor: type);
            var payloadRoot = Messages.MessageRoot.CreateMessageRoot(payloadBuilder,
                message_type: Messages.Message.UpdateCursor,
                message: payload.Value);
            payloadBuilder.Finish(payloadRoot.Value);

            Message updateCursorMessage = new Message(headerBuilder.SizedByteArray(), payloadBuilder.SizedByteArray());
            m_transport.WriteMessage(updateCursorMessage);
        }

        /// <summary>
        /// Called by CEF.
        /// If this method throws an exception, the FuelPump will crash from the unhandled exception.
        /// </summary>
        public void OnBeforeBrowse(int instanceId)
        {
            var instance = GetInstanceOrNull(instanceId);
            if (instance == null)
            {
                LOG.Error("Unknown instance ID " + instanceId + " in OnBeforeBrowse");
                return;
            }

            instance.OnBeforeBrowse();
        }

        /// <summary>
        /// Called by CEF.
        /// If this method throws an exception, the FuelPump will crash from the unhandled exception.
        /// </summary>
        public int OnBeforeResourceLoad(int instanceId, string url)
        {
            var instance = GetInstanceOrNull(instanceId);
            if (instance == null)
            {
                LOG.Error("Unknown instance ID " + instanceId + " in OnBeforeResourceLoad");
                return 1; // CefRequestHandler::ReturnValue::RV_CONTINUE
            }

            Uri uri;
            try
            {
                uri = new Uri(url);
            }
            catch (Exception)
            {
                LOG.Error("Rejecting invalid url");
                return 0; // CefRequestHandler::ReturnValue::RV_CANCEL
            }

            if (m_configHandler.GetConfig().IsRestartCblBlacklisted(uri))
            {
                instance.OnBlacklistedUrl(uri.AbsolutePath);

                // Cancel the load.
                return 0; // CefRequestHandler::ReturnValue::RV_CANCEL
            }

            if (!m_configHandler.GetConfig().IsWhitelistedForOverlay(uri))
            {
                LOG.Error("A resource for the domain " + uri.Host + " will not be loaded because it is not on the whitelist");

                // Cancel the load.
                return 0; // CefRequestHandler::ReturnValue::RV_CANCEL
            }

            return 1; // CefRequestHandler::ReturnValue::RV_CONTINUE
        }

        /// <summary>
        /// Called by CEF.
        /// If this method throws an exception, the FuelPump will crash from the unhandled exception.
        /// </summary>
        public void OnLoadEnd(int instanceId, int httpStatusCode)
        {
            var instance = GetInstanceOrNull(instanceId);
            if (instance == null)
            {
                LOG.Error("Unknown instance ID " + instanceId + " in OnLoadEnd");
                return;
            }

            instance.OnLoadEnd(httpStatusCode);
        }

        /// <summary>
        /// Called by CEF.
        /// If this method throws an exception, the FuelPump will crash from the unhandled exception.
        /// </summary>
        public void OnLoadError(int instanceId, int errorCode, string errorText, string failedUrl)
        {
            var instance = GetInstanceOrNull(instanceId);
            if (instance == null)
            {
                LOG.Error("Unknown instance ID " + instanceId + " in OnLoadError");
                return;
            }

            instance.OnLoadError(errorCode, errorText, failedUrl);
        }

        /// <summary>
        /// Called by CEF.
        /// If this method throws an exception, the FuelPump will crash from the unhandled exception.
        /// </summary>
        public void OnOpenInBrowser(int instanceId, string url)
        {
            try
            {
                Uri uri = new Uri(url);
                if (m_configHandler.GetConfig().IsWhitelistedForOpenInBrowser(uri))
                {
                    System.Diagnostics.Process.Start(uri.ToString());
                }
                else
                {
                    LOG.ErrorFormat("Attempt to launch browser with non-whitelisted URI with domain '{0}'", uri.Host);
                }
            }
            catch
            {
                LOG.ErrorFormat("Attempt to launch browser with malformed URI '{0}'", url);
            }
        }

        public string OnLoadLocalFile(string file)
        {
            switch(file)
            {
                case "amazon__V273230536_.png":
                    // TODO: [FUEL-1442] Pull the Amazon logo from Resources and return to cef'
                    break;
                case "cef-application.css":
                    return FuelPump.Properties.Resources.cef_application;

            }
            return null;
        }

        public void SendKey(int instanceId, CefInstance.KeyEventType type, int keyCode, bool ctrlDown)
        {
            var instance = GetInstanceOrThrow(instanceId);
            instance.SendKey(type, keyCode, ctrlDown);
        }

        public void SendMouseEvent(int instanceId, int x, int y, CefInstance.MouseButtonType type, bool mouseUp, int clickCount)
        {
            var instance = GetInstanceOrThrow(instanceId);
            instance.SendMouseEvent(x, y, type, mouseUp, clickCount);
        }

        public void SendMouseMoveEvent(int instanceId, int x, int y, bool leaving, bool leftMouseDown)
        {
            var instance = GetInstanceOrThrow(instanceId);
            instance.SendMouseMoveEvent(x, y, leaving, leftMouseDown);
        }

        public void SendMouseWheelEvent(int instanceId, int x, int y, int deltaX, int deltaY)
        {
            var instance = GetInstanceOrThrow(instanceId);
            instance.SendMouseWheelEvent(x, y, deltaX, deltaY);
        }

        public void SendCloseOverlayEvent(int instanceId)
        {
            var instance = GetInstanceOrThrow(instanceId);
            instance.ReceiveEvent(CefInstance.CLOSE_OVERLAY_EVENT_NAME, "");
        }

        public void OnOverlayFailure(int instanceId)
        {
            var instance = GetInstanceOrThrow(instanceId);
            instance.ReceiveEvent(CefInstance.OVERLAY_FAILURE_EVENT_NAME, "");
        }

        public void SetSize(int width, int height)
        {
            m_screenWidth = Clamp(width, MIN_SCREEN_SIZE_PIXELS, MAX_SCREEN_SIZE_PIXELS);
            m_screenHeight = Clamp(height, MIN_SCREEN_SIZE_PIXELS, MAX_SCREEN_SIZE_PIXELS);

            // resize all active instances
            foreach (var instance in GetAllInstances())
            {
                UpdateInstanceSize(instance.Value);
            }
        }

        private int Clamp(int value, int min, int max)
        {
            return (value < min) ? min : ((value > max) ? max : value);
        }

        private void UpdateInstanceSize(CefInstance instance)
        {
            instance.SetSize(CalculateOverlaySize(instance.GetPreferredWidth(), instance.GetPreferredHeight(),
                instance.GetPreferredWidthUnit(), instance.GetPreferredHeightUnit(), DEFAULT_DPI));
        }

        private Rectangle CalculateOverlaySize(float width, float height, CefInstance.SizeUnit widthUnit, CefInstance.SizeUnit heightUnit, int dpi)
        {
            var instanceWidth = ConvertLengthToPixels(m_screenWidth, width, widthUnit, DEFAULT_DPI);
            var instanceHeight = ConvertLengthToPixels(m_screenHeight, height, heightUnit, DEFAULT_DPI);
            var instanceX = Math.Max(0, (int)((m_screenWidth - instanceWidth) / 2));
            var instanceY = Math.Max(0, (int)((m_screenHeight - instanceHeight) / 2));

            return new Rectangle(instanceX, instanceY, instanceWidth, instanceHeight);
        }

        private int ConvertLengthToPixels(int max, float length, CefInstance.SizeUnit units, int dpi)
        {
            int result;

            switch (units)
            {
                case CefInstance.SizeUnit.Percent:
                    result = (int)(max * length / 100);
                    break;
                case CefInstance.SizeUnit.Inches:
                    result = (int)Math.Min(length * dpi, max * MAX_SIZE_RATIO_FOR_ABSOLUTE_MEASUREMENTS);
                    break;
                default:
                    result = (int)(max * DEFAULT_SIZE_RATIO);
                    break;
            }

            return Math.Max(Math.Min(result, max), MIN_OVERLAY_SIZE_PIXELS);
        }

        public CefInstance CreateInstance()
        {
            using (var metricsEvent = m_metrics.Create())
            using (var timerScope = metricsEvent.MetricEvent.StartTimer(FuelPumpMetrics.Cef.InstanceStartup))
            {
                var size = CalculateOverlaySize(80, 80, CefInstance.SizeUnit.Percent, CefInstance.SizeUnit.Percent, DEFAULT_DPI);

                var instanceId = Interlocked.Increment(ref instanceCtr);
                var instance = CEFCreate(instanceId, size.Left, size.Top, size.Width, size.Height);

                if (instance.ToInt32() == INSTANCE_CREATION_FAILED)
                {
                    throw new InstanceCreationFailedException();
                }

                var result = new CefInstance(instance, m_metrics, m_configHandler, () => 
                {
                    m_instancesLock.EnterWriteLock();
                    try
                    {
                        m_instances.Remove(instanceId);
                    }
                    finally
                    {
                        m_instancesLock.ExitWriteLock();
                    }
                }, instanceId, size, m_configHandler.GetConfig().pageLoadTimeoutSeconds.Value);

                m_instancesLock.EnterWriteLock();
                try
                {
                    m_instances.Add(instanceId, result);
                }
                finally
                {
                    m_instancesLock.ExitWriteLock();
                }

                return result;
            }
        }

        private KeyValuePair<int, CefInstance>[] GetAllInstances()
        {
            m_instancesLock.EnterReadLock();
            try
            {
                return m_instances.ToArray();
            }
            finally
            {
                m_instancesLock.ExitReadLock();
            }
        }

        private CefInstance GetInstanceOrNull(int instanceId)
        {
            m_instancesLock.EnterReadLock();
            try
            {
                if (m_instances.ContainsKey(instanceId))
                {
                    return m_instances[instanceId];
                }
                return null;
            }
            finally
            {
                m_instancesLock.ExitReadLock();
            }
        }

        private CefInstance GetInstanceOrThrow(int instanceId)
        {
            var instance = GetInstanceOrNull(instanceId);
            if (instance == null)
            {
                throw new UnknownCefInstanceException();
            }

            return instance;
        }
    }
}
