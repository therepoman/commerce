﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Overlay
{
    public class CefTask
    {
        private string m_eventName;
        private TaskCompletionSource<CefResult> m_task;

        public CefTask(string eventName, TaskCompletionSource<CefResult> task)
        {
            m_eventName = eventName;
            m_task = task;
        }

        public bool isWaiting(string cefEvent)
        {
            return m_eventName == cefEvent;
        }

        public TaskCompletionSource<CefResult> Task
        {
            get { return m_task; }
        }
    }
}
