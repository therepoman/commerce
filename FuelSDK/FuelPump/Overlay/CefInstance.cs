﻿using FuelPump.Services.Config;
using FuelPump.Helpers;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace FuelPump.Overlay
{
    public class CefInstance : Disposable 
    {
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(CefInstance));

        private const string TWITCH_AUTH_REDIRECT = "TWITCH_AUTH";
        private const string AMZN_AUTH_REDIRECT = "AMAZON_AUTH";
        private const string ACCT_LINKING_REDIRECT = "LINKING";
        private string[] ACTIONABLE_REDIRECT_TYPES = { TWITCH_AUTH_REDIRECT, AMZN_AUTH_REDIRECT, ACCT_LINKING_REDIRECT };

        private static readonly string FAKE_URL_FOR_LOCAL_LOADING_PAGE = "local://loading.html";
        private static readonly string FAKE_URL_FOR_LOCAL_ERROR_PAGE = "local://error.html";
        public static readonly string CLOSE_OVERLAY_EVENT_NAME = "Close";
        public static readonly string OVERLAY_FAILURE_EVENT_NAME = "OverlayFailure";
        public static readonly string AUTH_EVENT_NAME = "Auth";
        public static readonly string REDIRECT_EVENT_NAME = "Redirect";

        public class PageLoadException : ApplicationException
        {
            public CefLoadError Error { get; }

            public PageLoadException(CefLoadError loadError) : base("Failed to load " + loadError.FailedURL
                + " with error code " + loadError.ErrorCode + ": " + loadError.ErrorText)
            {
                Error = loadError;
            }
        }

        public class PageLoadTimeoutException : ApplicationException
        {
        }

        public class OverlayFailureException : ApplicationException
        {
        }

        public enum KeyEventType
        {
            RawKeyDown = 0,
            KeyDown = 1,
            KeyUp = 2,
            Char = 3,
        }

        public enum MouseButtonType
        {
            Left = 0,
            Middle = 1,
            Right = 2,
        }

        public enum SizeUnit
        {
            Inches = 0,
            Percent = 1,
        }

        [DllImport("OffscreenBrowser.dll", EntryPoint = "CEFDestroy")]
        static extern void CEFDestroy(IntPtr instance);

        [DllImport("OffscreenBrowser.dll", EntryPoint = "CEFNavigate")]
        static extern void CEFNavigate(IntPtr instance, 
            [MarshalAs(UnmanagedType.LPWStr)]string url);

        [DllImport("OffscreenBrowser.dll", EntryPoint = "CEFLoadStaticContent")]
        static extern void CEFLoadStaticContent(IntPtr instance,
            [MarshalAs(UnmanagedType.LPWStr)]string content,
            [MarshalAs(UnmanagedType.LPWStr)]string url);

        [DllImport("OffscreenBrowser.dll")]
        static extern void CEFResize(IntPtr instance, 
            [MarshalAs(UnmanagedType.I4)]int width,
            [MarshalAs(UnmanagedType.I4)]int height);

        [DllImport("OffscreenBrowser.dll", EntryPoint = "CEFKeyboardEvent")]
        static extern void CEFKeyboardEvent(IntPtr instance,
            [MarshalAs(UnmanagedType.I4)]int type,
            [MarshalAs(UnmanagedType.I4)]int key,
            [MarshalAs(UnmanagedType.Bool)]bool ctrlDown);

        [DllImport("OffscreenBrowser.dll", EntryPoint = "CEFMouseClickEvent")]
        static extern void CEFMouseClickEvent(IntPtr instance,
            [MarshalAs(UnmanagedType.I4)]int x,
            [MarshalAs(UnmanagedType.I4)]int y, 
            [MarshalAs(UnmanagedType.I4)]int type,
            [MarshalAs(UnmanagedType.Bool)]bool mouseUp,
            [MarshalAs(UnmanagedType.I4)]int clickCount);

        [DllImport("OffscreenBrowser.dll", EntryPoint = "CEFMouseMoveEvent")]
        static extern void CEFMouseMoveEvent(IntPtr instance,
            [MarshalAs(UnmanagedType.I4)]int x,
            [MarshalAs(UnmanagedType.I4)]int y,
            [MarshalAs(UnmanagedType.Bool)]bool mouseLeave,
            [MarshalAs(UnmanagedType.Bool)]bool leftMouseDown);

        [DllImport("OffscreenBrowser.dll", EntryPoint = "CEFMouseWheelEvent")]
        static extern void CEFMouseWheelEvent(IntPtr instance,
            [MarshalAs(UnmanagedType.I4)]int x,
            [MarshalAs(UnmanagedType.I4)]int y,
            [MarshalAs(UnmanagedType.I4)]int deltaX,
            [MarshalAs(UnmanagedType.I4)]int deltaY);

        [DllImport("OffscreenBrowser.dll")]
        static extern void CEFAddDataToHeader(IntPtr instance,
            [MarshalAs(UnmanagedType.LPWStr)]string key,
            [MarshalAs(UnmanagedType.LPWStr)]string value);

        [DllImport("OffscreenBrowser.dll")]
        static extern void CEFRemoveDataFromHeader(IntPtr instance,
            [MarshalAs(UnmanagedType.LPWStr)]string key);

        [DllImport("OffscreenBrowser.dll")]
        [return:MarshalAs(UnmanagedType.I1)]
        static extern bool CEFSetCookie(IntPtr instance,
            [MarshalAs(UnmanagedType.LPWStr)]string url,
            [MarshalAs(UnmanagedType.LPWStr)]string cookieName,
            [MarshalAs(UnmanagedType.LPWStr)]string cookieValue,
            [MarshalAs(UnmanagedType.LPWStr)]string cookieDomain,
            [MarshalAs(UnmanagedType.LPWStr)]string cookiePath,
            [MarshalAs(UnmanagedType.Bool)]Boolean secure,
            [MarshalAs(UnmanagedType.Bool)]Boolean httpOnly, 
            [MarshalAs(UnmanagedType.I4)]int expirationDayOfMonth,
            [MarshalAs(UnmanagedType.I4)]int expirationDayOfWeek,
            [MarshalAs(UnmanagedType.I4)]int expirationMonth,
            [MarshalAs(UnmanagedType.I4)]int expirationYear,
            [MarshalAs(UnmanagedType.I4)]int expirationHour,
            [MarshalAs(UnmanagedType.I4)]int expirationMinute,
            [MarshalAs(UnmanagedType.I4)]int expirationSecond,
            [MarshalAs(UnmanagedType.I4)]int expirationMillisecond);

        IntPtr m_instance;
        Action m_onDestroy;
        int m_instanceId;
        Rectangle m_rect;

        public float m_preferredWidth = 80;
        public float m_preferredHeight = 80;
        public SizeUnit m_preferredWidthUnit = SizeUnit.Percent;
        public SizeUnit m_preferredHeightUnit = SizeUnit.Percent;

        Metrics.MetricEventFactory m_metrics;
        Metrics.MetricEventFactory.MetricEventScope m_pageLoadScope;
        Metrics.TimerDataPoint m_pageLoadDataPoint;

        ConfigHandler m_configHandler;

        List<CefTask> m_cefTasks;
        List<TaskCompletionSource<bool>> m_loadStartTasks;
        List<TaskCompletionSource<CefLoadEnd>> m_loadEndTasks;
        List<TaskCompletionSource<CefLoadError>> m_loadErrorTasks;
        List<TaskCompletionSource<CefLoadError>> m_loadCancelledTasks;
        List<TaskCompletionSource<bool>> m_loadTimeoutTasks;
        List<TaskCompletionSource<string>> m_blacklistedUrlTasks;
        System.Timers.Timer m_loadTimeoutTimer;
        Object m_eventLock;
        Object m_loadingLock;

        public CefInstance(IntPtr instance, Metrics.MetricEventFactory metrics, ConfigHandler configHandler, Action onDestroy, int instanceId, Rectangle rect, double pageLoadTimeoutSeconds)
        {
            m_metrics = metrics;
            m_configHandler = configHandler;

            m_pageLoadScope = m_metrics.Create();
            m_pageLoadDataPoint = m_pageLoadScope.MetricEvent.StartTimer(getNavigationLoadTimeMetricName(""));
            m_pageLoadScope.Ignore = true;

            m_instance = instance;
            m_onDestroy = onDestroy;
            m_instanceId = instanceId;
            m_rect = rect;
            m_cefTasks = new List<CefTask>();
            m_loadStartTasks = new List<TaskCompletionSource<bool>>();
            m_loadEndTasks = new List<TaskCompletionSource<CefLoadEnd>>();
            m_loadErrorTasks = new List<TaskCompletionSource<CefLoadError>>();
            m_loadCancelledTasks = new List<TaskCompletionSource<CefLoadError>>();
            m_loadTimeoutTasks = new List<TaskCompletionSource<bool>>();
            m_blacklistedUrlTasks = new List<TaskCompletionSource<string>>();
            m_eventLock = new Object();
            m_loadingLock = new Object();

            m_loadTimeoutTimer = new System.Timers.Timer(pageLoadTimeoutSeconds * 1000.0);
            m_loadTimeoutTimer.Elapsed += (sender, e) => HandleTimeout();
            m_loadTimeoutTimer.AutoReset = false;
        }

        protected override void Dispose(bool disposing)
        {
            // Shut down CEF first to make sure there's no worker threads calling into this instance.
            CEFDestroy(m_instance);
            m_onDestroy();
            m_loadTimeoutTimer.Dispose();
        }

        public void Navigate(string url)
        {
            lock (m_loadingLock)
            {
                // Ignore the prior loading scope, as if it had finished loaded, it would have already been recorded.
                m_pageLoadScope.Ignore = true;

                m_pageLoadScope = m_metrics.Create();
                m_pageLoadDataPoint = m_pageLoadScope.MetricEvent.StartTimer(getNavigationLoadTimeMetricName(url));
            }

            CEFNavigate(m_instance, url);
        }

        public void SetSize(Rectangle size)
        {
            m_rect = size;
            CEFResize(m_instance, m_rect.Width, m_rect.Height);
        }

        public void SendKey(KeyEventType type, int keyCode, bool ctrlDown)
        {
            CEFKeyboardEvent(m_instance, (int)type, keyCode, ctrlDown);
        }

        public void SendMouseEvent(int x, int y, MouseButtonType type, bool mouseUp, int clickCount)
        {
            CEFMouseClickEvent(m_instance, x, y, (int)type, mouseUp, clickCount);
        }

        public void SendMouseMoveEvent(int x, int y, bool leaving, bool leftMouseDown)
        {
            CEFMouseMoveEvent(m_instance, x, y, leaving, leftMouseDown);
        }

        public void SendMouseWheelEvent(int x, int y, int deltaX, int deltaY)
        {
            CEFMouseWheelEvent(m_instance, x, y, deltaX, deltaY);
        }

        public void AddDataToHeader(string key, string value)
        {
            CEFAddDataToHeader(m_instance, key, value);
        }

        public void RemoveDataFromHeader(string key)
        {
            CEFRemoveDataFromHeader(m_instance, key);
        }

        public bool SetCookie(string url, string name, string value, string domain, string path, Boolean secure, Boolean httpOnly, DateTime expiration)
        {
            return CEFSetCookie(m_instance, url, name, value, domain, path, secure, httpOnly, expiration.Day, (int)expiration.DayOfWeek, expiration.Month, expiration.Year, expiration.Hour, expiration.Minute, expiration.Second, expiration.Millisecond);
        }

        public int GetInstanceId()
        {
            return m_instanceId;
        }

        public Rectangle GetRect()
        {
            return m_rect;
        }

        public float GetPreferredWidth()
        {
            return m_preferredWidth;
        }

        public float GetPreferredHeight()
        {
            return m_preferredHeight;
        }

        public SizeUnit GetPreferredWidthUnit()
        {
            return m_preferredWidthUnit;
        }

        public SizeUnit GetPreferredHeightUnit()
        {
            return m_preferredHeightUnit;
        }

        public void SetPreferredWidth(float width, SizeUnit unit)
        {
            m_preferredWidth = width;
            m_preferredWidthUnit = unit;
        }

        public void SetPreferredHeight(float height, SizeUnit unit)
        {
            m_preferredHeight = height;
            m_preferredHeightUnit = unit;
        }

        public Task<CefResult[]> WaitForAllEvents(params string[] eventNames)
        {
            List<Task<CefResult>> tasks = new List<Task<CefResult>>();
            foreach (var eventName in eventNames)
            {
                TaskCompletionSource<CefResult> eventTask = new TaskCompletionSource<CefResult>();

                lock (m_eventLock)
                {
                    m_cefTasks.Add(new CefTask(eventName, eventTask));
                }

                Task<CefResult> completeTask = eventTask.Task.ContinueWith(task =>
                {
                    return task.Result;
                });

                tasks.Add(completeTask);
            }

            return Task.WhenAll<CefResult>(tasks);
        }

        public void ReceiveEvent(string eventName, string eventData)
        {
            lock (m_eventLock)
            {
                if (eventName.Equals(REDIRECT_EVENT_NAME))
                {
                    dynamic data = JsonConvert.DeserializeObject<dynamic>(eventData);
                    string redirectType = Convert.ToString(data.type);
                    string redirectUrl = Convert.ToString(data.url);

                    if(!ACTIONABLE_REDIRECT_TYPES.Contains(redirectType))
                    {
                        // If we don't handle the redirect, simply continue to the URL.
                        Navigate(redirectUrl);
                        return;
                    }
                }

                CefTask[] tasks = m_cefTasks.Where(item => item.isWaiting(eventName)).ToArray();
                foreach (CefTask cTask in tasks)
                {
                    cTask.Task.SetResult(new CefResult(eventName, eventData));
                    m_cefTasks.Remove(cTask);
                }
            }
        }

        public void OnBeforeBrowse()
        {
            lock (m_loadingLock)
            {
                if (m_loadTimeoutTimer.Enabled)
                {
                    m_loadTimeoutTimer.Stop();
                }
                m_loadTimeoutTimer.Start();
            }
        }

        public Task<string> WaitForBlacklistedUrl()
        {
            lock (m_loadingLock)
            {
                var source = new TaskCompletionSource<string>();
                m_blacklistedUrlTasks.Add(source);
                return source.Task;
            }
        }

        public void OnBlacklistedUrl(string url)
        {
            List<TaskCompletionSource<string>> blacklistTasks = null;
            lock (m_loadingLock)
            {
                // Cancel the timeout.  The page load will be aborted by the caller.
                m_loadTimeoutTimer.Stop();

                blacklistTasks = new List<TaskCompletionSource<string>>(m_blacklistedUrlTasks);
                m_blacklistedUrlTasks.Clear();
            }

            if (blacklistTasks != null)
            {
                foreach (var waitingTask in blacklistTasks)
                {
                    waitingTask.SetResult(url);
                }
            }
        }

        public Task<CefLoadEnd> WaitForPageLoad()
        {
            lock (m_loadingLock)
            {
                var source = new TaskCompletionSource<CefLoadEnd>();
                m_loadEndTasks.Add(source);
                return source.Task;
            }
        }

        public void OnLoadEnd(int httpStatusCode)
        {
            List<TaskCompletionSource<CefLoadEnd>> loadEndTasks = null;
            List<TaskCompletionSource<CefLoadError>> loadErrorTasks = null;

            lock (m_loadingLock)
            {
                m_loadTimeoutTimer.Stop();
                
                m_pageLoadDataPoint.Stop();
                m_pageLoadScope.RecordEvents();
                
                if (httpStatusCode < 400)
                {
                    loadEndTasks = new List<TaskCompletionSource<CefLoadEnd>>(m_loadEndTasks);
                    m_loadEndTasks.Clear();
                }
                else
                {
                    LOG.Error("Instance " + m_instanceId + " failed to load a page "
                        + " with httpStatusCode " + httpStatusCode);

                    loadErrorTasks = new List<TaskCompletionSource<CefLoadError>>(m_loadErrorTasks);
                    m_loadErrorTasks.Clear();
                }
            }

            if (loadEndTasks != null)
            {
                foreach (var waitingTask in loadEndTasks)
                {
                    waitingTask.SetResult(new CefLoadEnd(httpStatusCode));
                }
            }

            if (loadErrorTasks != null)
            {
                foreach (var waitingTask in loadErrorTasks)
                {
                    waitingTask.SetResult(new CefLoadError(httpStatusCode, "Error " + httpStatusCode, null));
                }
            }
        }

        public Task WaitForPageTimeout()
        {
            lock (m_loadingLock)
            {
                var source = new TaskCompletionSource<bool>();
                m_loadTimeoutTasks.Add(source);
                return source.Task;
            }
        }

        private void HandleTimeout()
        {
            List<TaskCompletionSource<bool>> loadTimeoutTasks = null;
            lock (m_loadingLock)
            {
                loadTimeoutTasks = new List<TaskCompletionSource<bool>>(m_loadTimeoutTasks);
                m_loadTimeoutTasks.Clear();
            }

            if (loadTimeoutTasks != null)
            {
                foreach (var waitingTask in loadTimeoutTasks)
                {
                    waitingTask.SetResult(true);
                }
            }
        }

        public Task<CefLoadError> WaitForPageLoadError()
        {
            lock (m_loadingLock)
            {
                var source = new TaskCompletionSource<CefLoadError>();
                m_loadErrorTasks.Add(source);
                return source.Task;
            }
        }

        public Task<CefLoadError> WaitForPageLoadCancelled()
        {
            lock (m_loadingLock)
            {
                var source = new TaskCompletionSource<CefLoadError>();
                m_loadCancelledTasks.Add(source);
                return source.Task;
            }
        }

        public void OnLoadError(int errorCode, string errorText, string failedUrl)
        {
            // CEF doesn't serialize requests, and it doesn't provide a good way to filter out stale LoadEnd/LoadError events
            // that arrive late.  If Navigate() is called while a page is being loaded, then the previous page load
            // will at some point afterward be reported as failed with code -3.  These are being ignored since most of
            // the time things are working as expected.  If there is an unexpected problem, such as a bad SSL certificate
            // causes the page load to be cancelled, then the page load will be reported as a time out later.
            if (errorCode == -3) // CefLoadHandler::ErrorCode::ERR_ABORTED = -3
            {
                LOG.Debug("Instance " + m_instanceId + " cancelled loading " + failedUrl);
                List<TaskCompletionSource<CefLoadError>> loadCancelledTasks;

                lock (m_loadingLock)
                {
                    loadCancelledTasks = new List<TaskCompletionSource<CefLoadError>>(m_loadCancelledTasks);
                    m_loadCancelledTasks.Clear();
                }

                foreach (var waitingTask in loadCancelledTasks)
                {
                    waitingTask.SetResult(new CefLoadError(errorCode, errorText, failedUrl));
                }

                return;
            }

            LOG.Error("Instance " + m_instanceId + " failed to load " + failedUrl
                + " with error code " + errorCode + ": " + errorText);

            List<TaskCompletionSource<CefLoadError>> loadErrorTasks = null;

            lock (m_loadingLock)
            {
                m_loadTimeoutTimer.Stop();
                loadErrorTasks = new List<TaskCompletionSource<CefLoadError>>(m_loadErrorTasks);
                m_loadErrorTasks.Clear();
            }

            if (loadErrorTasks != null)
            {
                foreach (var waitingTask in loadErrorTasks)
                {
                    waitingTask.SetResult(new CefLoadError(errorCode, errorText, failedUrl));
                }
            }
        }

        public async Task ShowLoadingPage()
        {
            var html = FuelPump.Properties.Resources.LoadingPage
                .Replace("{LoadingPageText}", FuelPump.Properties.Resources.LoadingPageText)
                .Replace("{CancelText}", FuelPump.Properties.Resources.CancelText);
            CEFLoadStaticContent(m_instance, html, FAKE_URL_FOR_LOCAL_LOADING_PAGE);

            // If CEF recieves a LoadUrl call before it finishes LoadStringW, then it will ignore the call and stay on the loading screen.
            await WaitForPageLoad();
        }

        public async Task ShowErrorPage(string message)
        {
            var html = FuelPump.Properties.Resources.ErrorPage
                .Replace("{ErrorMessage}", message)
                .Replace("{CloseButton}", FuelPump.Properties.Resources.CloseButton)
                .Replace("{HelpText}", FuelPump.Properties.Resources.HelpText)
                .Replace("{PoweredText}", FuelPump.Properties.Resources.PoweredText);
            CEFLoadStaticContent(m_instance, html, FAKE_URL_FOR_LOCAL_ERROR_PAGE);

            // If CEF recieves a LoadUrl call before it finishes LoadStringW, then it will ignore the call and stay on the loading screen.
            await WaitForPageLoad();
        }

        public OverlayEvents RegisterOverlayEvents()
        {
            var closeTask = WaitForAllEvents(CLOSE_OVERLAY_EVENT_NAME).ContinueWith(task =>
                {
                    return task.Result[0];
                });
            var redirectTask = WaitForAllEvents(REDIRECT_EVENT_NAME).ContinueWith(task =>
                {
                    return task.Result[0];
                });
            return new OverlayEvents(closeTask, redirectTask, WaitForPageLoadError(), WaitForPageTimeout(),
                WaitForAllEvents(OVERLAY_FAILURE_EVENT_NAME));
        }

        public static async Task<TResult> WrapPageLoad<TResult>(Task<TResult> task, OverlayEvents overlayEvents)
        {
            var result = await Task.WhenAny(task, overlayEvents.CloseButtonClicked, overlayEvents.RedirectTask, overlayEvents.PageLoadError,
                overlayEvents.PageLoadTimeout, overlayEvents.OverlayFailure);
            if (result == overlayEvents.CloseButtonClicked)
            {
                throw new OperationCanceledException();
            }
            if (result == overlayEvents.RedirectTask)
            {
                var cefResult = overlayEvents.RedirectTask.Result;
                dynamic data = JsonConvert.DeserializeObject<dynamic>(cefResult.EventData);
                string redirectType = Convert.ToString(data.type);
                string redirectUrl = Convert.ToString(data.url);

                switch (redirectType)
                {
                    case TWITCH_AUTH_REDIRECT:
                        throw new AuthenticationHelper.TwitchAuthenticationFailedException();
                    case AMZN_AUTH_REDIRECT:
                        throw new AuthenticationHelper.AmazonAuthenticationFailedException();
                    case ACCT_LINKING_REDIRECT:
                        throw new AuthenticationHelper.UnlinkedAccountsException();
                    default:
                        // This means we somehow got an unsupported type, but somehow the redirect task was completed.
                        throw new ApplicationException("Unknown Redirect Type: " + redirectType);
                }
            }
            if (result == overlayEvents.PageLoadError)
            {
                throw new PageLoadException(await overlayEvents.PageLoadError);
            }
            if (result == overlayEvents.PageLoadTimeout)
            {
                throw new PageLoadTimeoutException();
            }
            if (result == overlayEvents.OverlayFailure)
            {
                throw new OverlayFailureException();
            }
            return await task;
        }

        private string getNavigationLoadTimeMetricName(string url)
        {
            if (url.Contains(m_configHandler.GetConfig().cblPage.path))
            {
                return FuelPumpMetrics.Website.CblLandingPageLoadTime;
            }
            else if (url.Contains(m_configHandler.GetConfig().purchasePage.path))
            {
                return FuelPumpMetrics.Website.PurchasePageLoadTime;
            }
            else
            {
                return FuelPumpMetrics.Website.UnknownPageLoadTime;
            }
        }
    }
}
