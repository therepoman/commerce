﻿using System;
using System.Runtime.CompilerServices;

namespace Amazon.Diagnostics.Spectator.Shared
{
    //  Diagnostic class used for input validation.
    //  This is symmetric to Assert.* except that it alwas returns
    //  the target object of the test for inlining in success path.
    //  if method fails, it logs a fatal and throws an ArgumentException
    internal static partial class Require
    {
        public static void AreEqual<T>(T target, T expected, string paramName, string message = null, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
        {
            message = message ?? String.Format("{0} should be equal to {1}", paramName, expected);

            if (!expected.Equals(target)) {
                throw new ArgumentException(message, paramName);
            }
        }

        public static void AreNotEqual<T>(T target, T expected, string paramName, string message = null, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
        {
            message = message ?? String.Format("{0} should not be equal to {1}", paramName, expected);

            if (expected.Equals(target)) {
                throw new ArgumentException(message, paramName);
            }
        }

        public static void IsAssignableFrom(object target, Type type, string paramName, string message = null, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
        {
            message = message ?? String.Format("{0} is not of type {1}", paramName, type.Name);
            if (!target.GetType().IsAssignableFrom(type) && (target.GetType().BaseType == null || !target.GetType().BaseType.IsAssignableFrom(type)))
            {
                throw new ArgumentException(message, paramName);
            }
        }

        public static void IsNotNull(object target, string paramName, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
        {
            if (target == null) {
                throw new ArgumentNullException(paramName);
            }
        }

        public static void IsNotNullOrEmpty(string target, string paramName, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
        {
            var message = String.Format("{0} should be a non empty string", paramName);

            if (String.IsNullOrEmpty(target)) {
                throw new ArgumentException(message, paramName);
            }
        }

        public static void IsNullOrWhiteSpace(string target, string paramName, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string filePath = "")
        {
            var message = String.Format("{0} should be a non empty string", paramName);

            if (String.IsNullOrWhiteSpace(target)) {
                throw new ArgumentException(message, paramName);
            }
        }
    }
}
