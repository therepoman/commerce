﻿using Amazon.Diagnostics.Spectator;
using log4net.Util;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FuelPump.Logging
{
    public class HardcodedDomainProvider : IDomainProvider
    {
        public string GetDomain()
        {
            return "domain";
        }

        public string GetMarketplaceId()
        {
            return "twitch";
        }

        public string GetAmazonOfficalRegion()
        {
            return "usa";
        }
    }

    public class AsyncSpectatorUploader
    {
        private static readonly Type declaredType = typeof(AsyncSpectatorUploader);

        private readonly string _basedir;
        private ConcurrentQueue<string> _uploads = new ConcurrentQueue<string>();
        private PmetUploader _uploader;
        private HttpClient _client;
        private Timer _timer;
        private string _filePattern;

        public AsyncSpectatorUploader(IAppInfo appInfo, string basedir, string filePattern)
        {
            _basedir = basedir;
            _filePattern = filePattern;
            _client = new HttpClient();
            _uploader = new PmetUploader(_client, appInfo, new HardcodedDomainProvider());

            EnqueuePrevious();

            // Start the timer (do this after the enqueue so we don't enqueue the current one
            _timer = new Timer(_ => Flush(), null, 1000, Timeout.Infinite);
        }

        public void EnqueuePrevious()
        {
            if (!Directory.Exists(_basedir))
            {
                return;
            }
            // Capture rolled over logs that end in a timestamp "YYYY-mm-dd-HH_mm.log"
            var all = Directory.GetFiles(_basedir, _filePattern).OrderByDescending(s => s);
            var upload = all.Take(5);
            var delete = all.Where(l => !upload.Contains(l));
            foreach (var toDelete in delete)
            {
                try
                {
                    File.Delete(toDelete);
                }
                catch (Exception e)
                {
                    LogLog.Error(declaredType, $"Failed to delete file {toDelete}", e);
                }
            }
            foreach (var toUpload in upload)
            {
                Add(toUpload);
            }
        }

        private void Flush()
        {
            string file = null;
            if (!_uploads.TryDequeue(out file))
            {
                return;
            }
            try
            {
                var uploadLogToDet = _uploader.UploadLogToDet(file, true).Result;
                if (Program.Metrics != null)
                {
                        Program.Metrics.AddCounter(uploadLogToDet ? FuelPumpMetrics.LogUploadSucceeded : FuelPumpMetrics.LogUploadFailed);
                }

                _timer.Change(1000, Timeout.Infinite); // Don't Re-queue if there was an upload failure
            }
            catch (Exception e)
            {
                LogLog.Error(declaredType, "Failed to upload", e);
                if (Program.Metrics != null) Program.Metrics.AddCounter(FuelPumpMetrics.LogUploadFailed);
            }
        }


        public void Add(string completedLog)
        {
            _uploads.Enqueue(completedLog);
        }
    }
}
