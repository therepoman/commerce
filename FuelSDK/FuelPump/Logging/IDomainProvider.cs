﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amazon.Diagnostics.Spectator
{
    public interface IDomainProvider
    {
        string GetDomain();
        string GetMarketplaceId();
        string GetAmazonOfficalRegion();
    }
}
