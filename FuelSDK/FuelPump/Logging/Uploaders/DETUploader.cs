﻿using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Amazon.Diagnostics.Spectator.Shared;
using Amazon.Diagnostics.Spectator.Shared.Helpers;
using log4net.Util;

namespace Amazon.Diagnostics.Spectator.Shared
{
    public abstract class DetUploader : IDetUploader
    {
        private readonly static Type declaredType = typeof(DetUploader);
        public const string BaseAddress = "https://det-ta-g7g.amazon.com/DeviceEventProxy/DETLogServlet";

        public const char UnixNewLine = '\n';
        protected readonly IAppInfo _appInfo;
        protected readonly HttpClient _httpClient;

        protected DetUploader(HttpClient httpClient, IAppInfo appInfo)
        {
            Require.IsNotNull(httpClient, "httpClient cannot be null");
            Require.IsNotNull(appInfo, "appInfo cannot be null");

            _httpClient = httpClient;
            _appInfo = appInfo;
        }

        public async Task<bool> UploadLogToDet(string fileName, bool deleteFileAfterUploading = true)
        {
            FileInfo fInfo = new FileInfo(fileName);
            if (!fInfo.Exists)
            {
                throw new FileNotFoundException();
            }
            if (fInfo.Length == 0)
            {
                return false;
            }
            var httpRequestMsg = await CreateRequestAsync(fileName);
            if (httpRequestMsg == null)
            {
            }
            bool fileUploaded = false;
            int counter = 0;
            do
            {
                try
                {
                    var httpResponse = await _httpClient.SendAsync(httpRequestMsg);
                    if (deleteFileAfterUploading && httpResponse.IsSuccessStatusCode)
                    {
                        fileUploaded = true;
                        File.Delete(fileName);
                    }
                }
                catch (HttpRequestException httpException)
                {
                    //Network Exception. No point in retrying
                    LogLog.Error(declaredType, "Http Exception {0} while trying to upload Pmet", httpException);
                    return false;
                }
                catch (Exception exception)
                {
                    LogLog.Error(declaredType, "Exception {0} while trying to upload Pmet", exception);
                }
                counter++;
                if (!fileUploaded && counter > 3)
                {
                    return false;
                }
                if (!fileUploaded)
                {
                    await Task.Delay(2000);
                        //In case request has failed delay it couple of seconds before retrying again.
                }
            } while (fileUploaded == false);
            return true;
        }

        public abstract Task<HttpRequestMessage> CreateRequestAsync(string filePath);
    }
}