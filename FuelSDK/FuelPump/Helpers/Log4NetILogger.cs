﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.Auth.Map;

namespace FuelPump.Helpers
{
    // This implementation wraps our usual log4net logger.
    public class Log4NetILogger : ILogger
    {
        private log4net.ILog logger;

        public Log4NetILogger(log4net.ILog logger)
        {
            this.logger = logger;
        }

        public void WriteLog(string message, Exception ex = null)
        {
            if (ex != null)
            {
                logger.Error(message);
            }
            else
            {
                logger.Info(message);
            }
        }

        public void WritePmetMetric(string counterName, long counterValue)
        {
            // Not yet implemented.
        }
    }
}
