﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;


namespace System
{
    public static class HashHelper
    {
        public static string ToHashString(this string content)
        {
            byte[] buffer = Encoding.Default.GetBytes(content);
            MD5Cng algorithm = new MD5Cng();
            buffer = algorithm.ComputeHash(buffer);

            StringBuilder sb = new StringBuilder(buffer.Length * 2);
            foreach (byte b in buffer)
            {
                sb.Append(b.ToString("x2"));
            }

            return sb.ToString();
        }
    }
}
