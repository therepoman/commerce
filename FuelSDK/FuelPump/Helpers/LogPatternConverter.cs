﻿using log4net.Layout.Pattern;
using log4net.Core;
using System.IO;

namespace FuelPump.Helpers
{
    class ShortLogLevelConverter : PatternLayoutConverter
    {
        protected override void Convert(TextWriter writer, LoggingEvent loggingEvent)
        {
            writer.Write(loggingEvent.Level.DisplayName.Substring(0, 1));
        }
    }

    class ReplaceColonConverter : PatternLayoutConverter
    {
        protected override void Convert(TextWriter writer, LoggingEvent loggingEvent)
        {
            writer.Write(loggingEvent.RenderedMessage.Replace(':', ';'));
        }
    }
}
