﻿using System;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;

namespace FuelPump.Helpers
{
    public class SignatureValidator
    {
        private static int ERROR_SUCCESS = 0;
        private static string[] authorizedSubjects = {
            "CN=\"Twitch Interactive, Inc.\", O=\"Twitch Interactive, Inc.\", L=San Francisco, S=California, C=US",
            "CN=Amazon Services LLC, OU=Software Services, O=Amazon Services LLC, L=Seattle, S=Washington, C=US" };
        private static string[] authorizedRootCA = {
            "0563B8630D62D75ABBC8AB1E4BDFB5A899B24D43", // DigiCert Assured ID Root CA, issues Twitch's cert
            "4EB6D578499B1CCF5F581EAD56BE3D9B6744A5E5" }; // VeriSign Class 3 Public Primary Certification Authority - G5, issues Amazon's cert

        public static bool VerifyEmbeddedSignature(string sourceFile)
        {
            // Don't pop up any windows
            var windowMode = IntPtr.Subtract(IntPtr.Zero, 1);

            // WINTRUST_ACTION_GENERIC_VERIFY_V2
            var guid = new Guid(0xaac56b, unchecked((short) 0xcd44), 0x11d0, new byte[] { 0x8c, 0xc2, 0x0, 0xc0, 0x4f, 0xc2, 0x95, 0xee });

            var data = new WinTrustData(sourceFile);
            int result = WinVerifyTrust(windowMode, guid, data);

            bool verified = result == ERROR_SUCCESS;
            if (verified)
            {
                // Verify that the signature was for Amazon or Twitch
                verified = VerifySigner(data);
            }

            data.dwStateAction = 2; // WTD_STATEACTION_CLOSE
            WinVerifyTrust(IntPtr.Zero, guid, data);

            return verified;
        }

        private static bool VerifySigner(WinTrustData data)
        {
            IntPtr hWVTStateData = data.hWVTStateData;
            IntPtr ptrProvData = WTHelperProvDataFromStateData(hWVTStateData);
            CRYPT_PROVIDER_DATA provData = (CRYPT_PROVIDER_DATA)Marshal.PtrToStructure(ptrProvData, typeof(CRYPT_PROVIDER_DATA));

            // Look down each possible certificate chain
            for (int i = 0; i < provData.csSigners; i++)
            {
                IntPtr ptrProvSigner = WTHelperGetProvSignerFromChain(ptrProvData, i, false, 0);
                CRYPT_PROVIDER_SGNR provSigner = (CRYPT_PROVIDER_SGNR)Marshal.PtrToStructure(ptrProvSigner, typeof(CRYPT_PROVIDER_SGNR));
                if (provSigner.csCertChain <= 0)
                {
                    return false;
                }

                // Grab the first certificate in the chain (Should contain who the certificate is issued to)
                IntPtr ptrCert = WTHelperGetProvCertFromChain(ptrProvSigner, 0);
                CRYPT_PROVIDER_CERT cert = (CRYPT_PROVIDER_CERT)Marshal.PtrToStructure(ptrCert, typeof(CRYPT_PROVIDER_CERT));
                X509Certificate2 certificate = new X509Certificate2(cert.pCert);
                string subject = certificate.Subject;

                if (!VerifyHelper(subject, authorizedSubjects))
                {
                    continue;
                }

                // No need to check the first certificate as it can't be a CA.
                for (int j = 1; j < provSigner.csCertChain; j++)
                {
                    IntPtr ptrRootCert = WTHelperGetProvCertFromChain(ptrProvSigner, j);
                    CRYPT_PROVIDER_CERT rootCert = (CRYPT_PROVIDER_CERT)Marshal.PtrToStructure(ptrRootCert, typeof(CRYPT_PROVIDER_CERT));
                    X509Certificate2 rootCertificate = new X509Certificate2(rootCert.pCert);
                    string thumbprint = rootCertificate.Thumbprint;

                    if (VerifyHelper(thumbprint, authorizedRootCA))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private static bool VerifyHelper(string certInfo, string[] authorized)
        {
            for (int j = 0; j < authorized.Length; j++)
            {
                if (authorized[j].Equals(certInfo))
                {
                    return true;
                }
            }

            return false;
        }

        [DllImport("wintrust.dll")]
        private static extern int WinVerifyTrust(
             [In] IntPtr hwnd,
             [In] [MarshalAs(UnmanagedType.LPStruct)] Guid pgActionID,
             [In] WinTrustData pWVTData
        );

        [DllImport("wintrust.dll")]
        private static extern IntPtr WTHelperProvDataFromStateData(IntPtr hStateData);

        [DllImport("wintrust.dll")]
        private static extern IntPtr WTHelperGetProvSignerFromChain(IntPtr pProvData, int idxSigner, bool fCounterSigner, int idxCounterSigner);

        [DllImport("wintrust.dll")]
        private static extern IntPtr WTHelperGetProvCertFromChain(IntPtr pSgnr, int idxCert);

        [StructLayout(LayoutKind.Sequential)]
        private class WinTrustData
        {
            uint cbStruct = (uint) Marshal.SizeOf(typeof(WinTrustData));
            IntPtr pPolicyCallbackData = IntPtr.Zero; // Use default code signing EKU
            IntPtr pSIPClientData = IntPtr.Zero; // No data to pass to SIP
            uint dwUIChoice = 2; // WTD_UI_NONE   Disable WVT UI
            uint fdwRevocationChecks = 0; // WTD_REVOKE_NONE  No revocation checking
            uint dwUnionChoice = 1; // WTD_CHOICE_FILE
            IntPtr pFile;
            public uint dwStateAction = 1; // WTD_STATEACTION_VERIFY
            public IntPtr hWVTStateData = IntPtr.Zero;
            IntPtr pwszURLReference = IntPtr.Zero; // Not used
            uint dwProvFlags = 0x10; // WTD_REVOCATION_CHECK_NONE  No revocation checking
            uint dwUIContext = 0; // Not applicable if no UI

            public WinTrustData(string file)
            {
                WinTrustFileInfo fileInfo = new WinTrustFileInfo(file);
                pFile = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(WinTrustFileInfo)));
                Marshal.StructureToPtr(fileInfo, pFile, false);
            }

            ~WinTrustData()
            {
                Marshal.FreeCoTaskMem(pFile);
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct CRYPT_PROVIDER_DATA
        {
            int cbStruct;
            IntPtr pWintrustData;
            bool fOpenedFile;
            IntPtr hWndParent;
            IntPtr pgActionID;
            IntPtr hProv;
            int dwError;
            int dwRegSecuritySettings;
            int dwRegPolicySettings;
            IntPtr psPfns;
            int cdwTrustStepErrors;
            IntPtr padwTrustStepErrors;
            int chStores;
            IntPtr pahStores;
            int dwEncoding;
            IntPtr hMsg;
            public int csSigners;
            IntPtr pasSigners;
            int csProvPrivData;
            IntPtr pasProvPrivData;
            int dwSubjectChoice;
            IntPtr pPDSip;
            IntPtr pszUsageOID;
            bool fRecallWithState;
            System.Runtime.InteropServices.ComTypes.FILETIME sftSystemTime;
            IntPtr pszCTLSignerUsageOID;
            int dwProvFlags;
            int dwFinalError;
            IntPtr pRequestUsage;
            int dwTrustPubSettings;
            int dwUIStateFlags;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct CRYPT_PROVIDER_SGNR
        {
            int cbStruct;
            System.Runtime.InteropServices.ComTypes.FILETIME sftVerifyAsOf;
            public int csCertChain;
            IntPtr pasCertChain;
            int dwSignerType;
            IntPtr psSigner;
            int dwError;
            int csCounterSigners;
            IntPtr pasCounterSigners;
            IntPtr pChainContext;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct CRYPT_PROVIDER_CERT
        {
            public int cbStruct;
            public IntPtr pCert;
            bool fCommercial;
            bool fTrustedRoot;
            bool fSelfSigned;
            bool fTestCert;
            int dwRevokedReason;
            int dwConfidence;
            int dwError;
            IntPtr pTrustListContext;
            bool fTrustListSignerCert;
            IntPtr pCtlContext;
            int dwCtlError;
            bool fIsCyclic;
            IntPtr pChainElement;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        private class WinTrustFileInfo
        {
            uint cbStruct = (uint) Marshal.SizeOf(typeof(WinTrustFileInfo));
            string pcwszFilePath;
            IntPtr hFile = IntPtr.Zero;
            IntPtr pgKnownSubject = IntPtr.Zero;

            public WinTrustFileInfo(string file)
            {
                pcwszFilePath = file;
            }
        }
    }
}
