﻿using FuelPump.Services.Config;
using System;
using System.Net;
using System.Threading.Tasks;

namespace FuelPump.Helpers
{
    public class RetryHelper
    {
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(RetryHelper));

        /// <summary>
        /// Returns true if a task should be retried after the given exception.
        /// The return value is a task for delaying the retry.
        /// </summary>
        public delegate Task<bool> ShouldRetry(Exception ex);

        /// <summary>
        /// Retry a task.  Intended for a small number of attempts, and does not have backoff.
        /// If a timeout is configured, it measures from the start of the first attempt and does not reset for each attempt.
        /// </summary>
        /// <param name="task">A function to produce a new task for each attempt.</param>
        /// <param name="apiConfig">The number of retries, delay between retries, and timeout.</param>
        public static Task<TResult> Retry<TResult>(Func<Task<TResult>> task, ApiConfig apiConfig)
        {
            int retriesRemaining = apiConfig.retries != null && apiConfig.retries > 0 ? apiConfig.retries.Value : 0;

            Task timeoutTask = null;
            if (apiConfig.timeoutSeconds > 0)
            {
                timeoutTask = Task.Delay((int)(apiConfig.timeoutSeconds * 1000.0));
            }

            var retryTask = Retry(task, (thrownException) =>
            {
                if (!IsRetriable(thrownException) || retriesRemaining <= 0)
                {
                    LOG.Error(String.Format("Failing task, IsRetriable={0}; retriesRemaining={1}", IsRetriable(thrownException), retriesRemaining), thrownException);
                    return Task.FromResult(false);
                }

                if (timeoutTask != null && timeoutTask.IsCompleted)
                {
                    // The task timed out and the exception won't go anywhere, so log it here.
                    LOG.Error("Attempt failed after timeout", thrownException);
                    
                    // No need to retry.
                    return Task.FromResult(false);
                }

                retriesRemaining--;

                return Task.Delay((int)((apiConfig.retryDelaySeconds ?? 0.0) * 1000.0)).ContinueWith((completedTask2) => true);
            });

            // Task.WhenAny does not allow nulls.
            if (timeoutTask == null)
            {
                return retryTask;
            }

            return Task.WhenAny(retryTask, timeoutTask).ContinueWith((completedTask) =>
            {
                if (!retryTask.IsCompleted)
                {
                    throw new TimeoutException();
                }

                if (retryTask.IsFaulted)
                {
                    throw retryTask.Exception;
                }

                return retryTask.Result;
            });
        }

        /// <summary>
        /// Retry a task.
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="task">A function to produce a new task for each attempt.</param>
        /// <param name="shouldRetry">
        /// A function that returns true if a task should be retried after the given exception.
        /// The return value is a task for delaying the retry.
        /// </param>
        public static async Task<TResult> Retry<TResult>(Func<Task<TResult>> task, ShouldRetry shouldRetry)
        {
            Exception lastException;
            do
            {
                try
                {
                    return await task();
                }
                catch (Exception ex)
                {
                    lastException = ex;
                }
            } while (await shouldRetry(lastException));
            throw lastException;
        }

        private static bool IsRetriable(Exception e)
        {
            // Look through all the nested exceptions for a WebException and check why it failed.
            Exception innerException = e;
            for (int i = 0; i < 10 && innerException != null; i++)
            {
                // Definitely bad configuration.
                if (innerException is InvalidConfigurationException)
                {
                    return false;
                }

                var webException = innerException as WebException;
                if (webException != null)
                {
                    // There's not much point in retrying a DNS failure.
                    return webException.Status != WebExceptionStatus.NameResolutionFailure;
                }
                innerException = innerException.InnerException;
            }

            return true;
        }
    }
}
