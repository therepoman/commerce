﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Activation
{
    public class TransportModule : NinjectModule
    {
        private string m_readHandle;
        private string m_writeHandle;

        public TransportModule(string readHandle, string writeHandle)
        {
            m_readHandle = readHandle;
            m_writeHandle = writeHandle;
        }

        public override void Load()
        {
            Bind<IMessageTransport>()
                .ToMethod(context => new MessageTransport(m_readHandle, m_writeHandle))
                .InSingletonScope();
        }
    }
}
