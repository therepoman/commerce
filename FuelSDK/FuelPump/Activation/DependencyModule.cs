﻿using FuelPump.Metrics;
using FuelPump.Monitoring;
using FuelPump.Overlay;
using FuelPump.Helpers;
using FuelPump.Services.Config;
using Ninject;
using Ninject.Modules;
using System;
using System.Globalization;
using System.Net.Http;
using Amazon.Auth.Map;
using Amazon.Auth.Map.Services;
using Amazon.Auth.Map.Services.Panda;
using Amazon.MCP.Common.Contracts;
using Amazon.MCP.Plugin.TwitchUserStorage;
using Amazon.MCP.Plugin.SecureStorage;
using Amazon.MCP.Common.Implementations;
using Clients.Kraken;

namespace FuelPump.Activation
{
    public class DependencyModule : NinjectModule
    {
        private static readonly AppInfo APP_INFO = new AppInfo();

        public override void Load()
        {
            Bind<ISecureStoragePlugin>().To<RegistryStorage>().InSingletonScope();
            Bind<ITwitchUserStoragePlugin>().To<TwitchUserStoragePlugin>().InSingletonScope();
            Bind<IClock>().To<Clock>().InSingletonScope();
            // TODO FUEL-1736: Endpoint should be configurable
            Bind<IdentityClient>().To<IdentityClient>().WithConstructorArgument("endpoint", "https://api.twitch.tv")
                .WithConstructorArgument("clientid", "5ljfec5t21xl36ats4ufmrc77dgrqkz");

            Bind<CefInstanceManager>().ToSelf().InSingletonScope();

            Bind<IMonitoringProvider>()
                .To<LogMonitoringProvider>()
                .InSingletonScope();

            Bind<IMetricQueue, MetricQueue>()
                .To<MetricQueue>()
                .InSingletonScope();

            Bind<IMetricTransport, ServiceMetricTransport>()
                .To<ServiceMetricTransport>()
                .InSingletonScope()
                .WithConstructorArgument("deviceType", APP_INFO.DeviceTypeId)
                .WithConstructorArgument("deviceSerialNumber", APP_INFO.DeviceSerialId)
                .WithConstructorArgument("softwareVersion", APP_INFO.Version);

            Bind<MetricEventFactory>()
                .ToSelf()
                .InSingletonScope()
                .WithConstructorArgument("program", "Fuel")
                .WithConstructorArgument("source", "FuelPump");

            Bind(typeof(Lazy<MetricEventFactory>))
                .ToMethod(ctx => new Lazy<MetricEventFactory>(() => ctx.Kernel.Get<MetricEventFactory>(),true))
                .InSingletonScope();

            Bind<Providers.Credentials.AbstractCredentialsProvider>()
                .To<Providers.Credentials.RegistryCredentialsProvider>()
                .InSingletonScope();

            Bind<MessageDispatcher>().ToProvider<DispatcherProvider>().InSingletonScope();

            Bind<IApplicationContext>()
                .To<ApplicationContext>()
                .InSingletonScope();

            Bind<PandaService>()
                .ToMethod(context =>
                    new PandaService(
                        new HttpClient(),
                        new Log4NetILogger(log4net.LogManager.GetLogger(typeof(IPandaService))),
                        APP_INFO,
                        new Amazon.Auth.Map.Environment(),
                        new DomainProvider(new Localization()),
                        new Localization(),
                        new PandaSettings()
                    ))
                .InSingletonScope();

            Bind<ConfigHandler>().ToSelf().InSingletonScope();
        }
    }
}
