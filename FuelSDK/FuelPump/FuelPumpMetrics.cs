﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump
{
    public class FuelPumpMetrics
    {
        public const string Startup = "Startup";
        public const string Shutdown = "Shutdown";
        public const string UnhandledException = "UnhandledException";

        public const string CredentialsLookup = "CredentialsLookup";
        public const string CredentialsLookupMiss = "CredentialsLookupMiss";

        public const string LogUploadSucceeded = "LogUploadSucceeded";
        public const string LogUploadFailed = "LogUploadFailed";

        public class APICall
        {
            public const string GetProductData = "API_GetProductData";
            public const string Purchase = "API_Purchase";
            public const string GetPurchaseUpdates = "API_GetPurchaseUpdates";
            public const string NotifyFulfillment = "API_NotifyFulfillment";
        }

        public class ServiceCall
        {
            public const string GetProductData = "Service_GetProductData";
            public const string GetProductDataFailure = "Service_Failure_GetProductData";

            public const string SetReceiptsFulfillmentStatus = "Service_SetReceiptsFulfillmentStatus";
            public const string SetReceiptsFulfillmentStatusFailure = "Service_Failure_SetReceiptsFulfillmentStatus";

            public const string SyncGoods = "Service_SyncGoods";
            public const string SyncGoodsFailure = "Service_Failure_SyncGoods";
        }

        public class Website
        {
            public const string CblLandingPageLoadTime = "Cbl_Landing_Overlay_LoadTime";
            public const string PurchasePageLoadTime = "Purchase_Overlay_LoadTime";
            public const string UnknownPageLoadTime = "UnknownPage_Overlay_LoadTime";
        }

        public class Cef
        {
            public const string InstanceStartup = "CefInstanceStartup";
        }

        public class Config
        {
            public const string LoadConfig = "Service_LoadConfig";
            public const string LoadConfigFailure = "Service_Failure_LoadConfig";
            public const string ConfigFallback = "ConfigFallback";
            public const string LoadConfigTimeout = "LoadConfigTimeout";
        }
    }
}
