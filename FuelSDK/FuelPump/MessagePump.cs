﻿using FuelPump.Helpers;
using Ninject;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace FuelPump
{
    public class MessagePump : Disposable
    {
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(MessagePump));

        private CancellationTokenSource m_cancellationToken = new CancellationTokenSource();
        private List<Task> tasks = new List<Task>();

        private IMessageTransport m_transport;
        private MessageDispatcher m_dispatcher;
        private Metrics.MetricEventFactory m_metrics;
        private Monitoring.IMonitoringProvider m_monitoring;

        public MessagePump(IMessageTransport transport, MessageDispatcher dispatcher, Metrics.MetricEventFactory metrics, Monitoring.IMonitoringProvider monitoring, IApplicationContext appContext)
        {
            m_transport = transport;
            m_dispatcher = dispatcher;
            m_metrics = metrics;
            m_monitoring = monitoring;

            dispatcher.RegisterHandler<Fuel.Messaging.InitializeRequest, Fuel.Messaging.InitializeResponse>((context, message) =>
            {
                LOG.InfoFormat("Received initialize request for Vendor: {0} and Game Version: {1} and SDK Version: {2}", message.VendorId, message.GameVersion, message.SdkVersion);

                appContext.VendorId = message.VendorId;

                return Task.FromResult(new MessageFactory<Fuel.Messaging.InitializeResponse>(builder =>
                {
                    Fuel.Messaging.InitializeResponse.StartInitializeResponse(builder);
                    return Fuel.Messaging.InitializeResponse.EndInitializeResponse(builder);
                }));
            });

            // doesn't use TransportHandler since this message is part of the flow control for MessagePump
            dispatcher.RegisterHandler<Fuel.Messaging.InitializeResponse>((MessageContext context, Message message) =>
            {
                m_transport.WriteMessage(message);

                return Task.FromResult(true); // return a completed task
            });

            // setup the shutdown message handler that stops the application
            dispatcher.RegisterHandler<Fuel.Messaging.ShutdownRequest>((MessageContext context, Message messge) =>
            {
                LOG.Info("Received Shutdown message from client");
                Stop();

                return Task.FromResult(true); // return a completed task
            });

            dispatcher.UnhandledException += (object sender, HandlerExceptionEventArgs e) =>
            {
                monitoring.LogUnhandledHandlerException(e.Exception);
                throw e.Exception;
            };

            dispatcher.UnhandledMessage += (object sender, UnhandledMessageEventArgs e) =>
            {
                monitoring.LogUnhandledMessage(e.Message);
            };
        }

        public async Task<bool> Start()
        {
            try
            {
                m_metrics.AddCounter(FuelPumpMetrics.Startup);

                return await Task.Run<bool>(new Func<Task<bool>>(PollTransport));
            }
            catch (Exception ex)
            {
                m_metrics.AddCounter(FuelPumpMetrics.UnhandledException);
                m_monitoring.LogRuntimeFailure(ex);

                return false;
            }
            finally
            {
                m_metrics.AddCounter(FuelPumpMetrics.Shutdown);
            }
        }

        private async Task<bool> PollTransport()
        {
            var cancellationToken = m_cancellationToken.Token;
            Exception taskException = null;

            try
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    var message = await m_transport.ReadMessage(cancellationToken);

                    tasks.Add(m_dispatcher.ProcessMessage(message).ContinueWith((task) =>
                    {
                        if (task.IsFaulted)
                        {
                            taskException = task.Exception.InnerException;
                            m_cancellationToken.Cancel();
                        }

                        if (tasks.Contains(task))
                        {
                            tasks.Remove(task);
                        }
                    }));
                }

                return WaitForTaskCompletion(taskException);
            }
            catch (EndOfStreamException)
            {
                // Stop the FuelPump and cancel any running tasks
                Stop();

                return WaitForTaskCompletion(taskException);

            }
            catch (OperationCanceledException)
            {
                return WaitForTaskCompletion(taskException);
            }
        }

        public void Stop()
        {
            m_cancellationToken.Cancel();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                m_cancellationToken.Dispose();
            }
        }

        private bool WaitForTaskCompletion(Exception taskException)
        {
            LOG.Debug("Waiting for outstanding messages to complete");
            Task.WaitAll(tasks.ToArray(), 5000);

            if (taskException != null)
            {
                throw taskException;
            }

            return true;
        }
    }
}
