﻿using FuelPump.Services.Config;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Metrics
{
    public class ServiceMetricTransport : Disposable, IMetricTransport
    {
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(ServiceMetricTransport));

        private ConfigHandler m_configHandler;
        private string m_deviceType;
        private string m_deviceSerialNumber;
        private string m_softwareVersion;

        private HttpClient m_client;

        public ServiceMetricTransport(ConfigHandler configHandler, string deviceType, string deviceSerialNumber, string softwareVersion)
        {
            m_configHandler = configHandler;
            m_deviceType = deviceType;
            m_deviceSerialNumber = deviceSerialNumber;
            m_softwareVersion = softwareVersion;

            m_client = new HttpClient();
            m_client.DefaultRequestHeaders.Add("x-credential-token", m_deviceType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                m_client.Dispose();
            }
        }

        public async Task TransmitMetricBatch(IEnumerable<MetricEvent> metricEvents)
        {
            var batch = new JObject(
                new JProperty("_type", "simpleMetricBatch"),
                new JProperty("deviceSerialNumber", m_deviceSerialNumber),
                new JProperty("deviceType", m_deviceType),
                new JProperty("tag", "TEST"),
                new JProperty("metricEntries",
                    new JArray(
                        from metricEvent in metricEvents.Select(a => a.Serialize())
                        select new JObject(
                            new JProperty("_type", "simpleMetricEntry"),
                            new JProperty("program", metricEvent.Program),
                            new JProperty("source", metricEvent.Source),
                            new JProperty("timestamp", metricEvent.Timestamp),
                            new JProperty("dataPoints",
                                new JArray(
                                    from dataPoint in metricEvent.DataPoints
                                    select new JObject(
                                        new JProperty("_type", "simpleDataPoint"),
                                        new JProperty("name", dataPoint.Name),
                                        new JProperty("value", dataPoint.Value),
                                        new JProperty("type", dataPoint.Type),
                                        new JProperty("samples", dataPoint.Samples))))))),
                new JProperty("metadata",
                    new JObject(
                       new JProperty("appVersion", m_softwareVersion))));

            var content = new StringContent(batch.ToString(), Encoding.UTF8, "application/json");
            var result = await m_client.PostAsync(m_configHandler.GetConfig().metrics.getUri(), content);
            
            bool success = result.IsSuccessStatusCode;

            if (!result.IsSuccessStatusCode)
            {
                // TODO: [FUEL-811] implement a retry mechanism with queued transmissions
                LOG.Error("Error transmitting metrics");
            }
        }
    }
}
