﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Metrics
{
    public interface IMetricTransport
    {
        Task TransmitMetricBatch(IEnumerable<MetricEvent> metricEvents);
    }
}
