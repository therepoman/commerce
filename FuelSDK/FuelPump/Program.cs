﻿using FuelPump.Helpers;
using FuelPump.Logging;
using FuelPump.Metrics;
using FuelPump.Monitoring;
using FuelPump.Overlay;
using FuelPump.Services.Config;
using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FuelPump
{
    public class Program
    {
        private const int SUCCESS = (int)Result.TERMINATED_SUCCESSFULLY;
        private const int INSUFFICIENT_ARGS = (int)Result.FAILED_INSUFFICIENT_ARGS;
        private const int CEF_FAILED = (int)Result.CEF_INITIALIZATION_FAILED;

        private const string OFFSCREEN_BROWSER_DLL = "OffscreenBrowser.dll";

        [DllImport(OFFSCREEN_BROWSER_DLL)]
        [return: MarshalAs(UnmanagedType.I1)]
        static extern bool CEFStart([MarshalAs(UnmanagedType.LPWStr)]string userAgent);

        [DllImport(OFFSCREEN_BROWSER_DLL)]
        static extern void CEFStop();

        [DllImport("kernel32")]
        static extern bool AllocConsole();

        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(Program));

        public static AppInfo AppInfo = new AppInfo();

        public static Metrics.MetricEventFactory Metrics;
        private static Ninject.StandardKernel Kernel;

        private enum Result : int
        {
            TERMINATED_SUCCESSFULLY = 0,
            FAILED_INSUFFICIENT_ARGS,
            CEF_INITIALIZATION_FAILED,
            SIGNATURE_VALIDATION_FAILED,
        }

        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.ControlAppDomain)]
        static int Main(string[] args)
        {
#if DEBUG
            AllocConsole();
#endif

            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(UnhandledExceptionLogger);

            if (args.Length < 2)
            {
                LOG.Debug("Insuffient number of arguments.");
                return INSUFFICIENT_ARGS;
            }
            string readHandle = args[0];
            string writeHandle = args[1];

            LOG.InfoFormat("Starting Fuel Pump. Read Handle : '{0}' Write Handle : '{1}' DSN : '{2}'", readHandle, writeHandle, AppInfo.DeviceSerialId);

            var modules = new NinjectModule[]
                    {
                        new Activation.TransportModule(readHandle, writeHandle),
                        new Activation.DependencyModule()
                    };
            Kernel = new Ninject.StandardKernel(modules);
            Metrics = Kernel.Get<Metrics.MetricEventFactory>();

            try
            {
#if !DEBUG
                if (!SignatureValidator.VerifyEmbeddedSignature(OFFSCREEN_BROWSER_DLL))
                {
                    LOG.Error("Signature validation failed for " + OFFSCREEN_BROWSER_DLL);
                    return (int)Result.SIGNATURE_VALIDATION_FAILED;
                }
#endif

                if (!CEFStart(AppInfo.UserAgent))
                {
                    LOG.Error("Cef failed to initialize.");
                    return CEF_FAILED;
                }

                try
                {
                    using (var program = Kernel.Get<MessagePump>())
                    {
                        // Initialize the ConfigHandler after everything else to work around a Ninject bug.
                        // See documentation of ConfigHandler.Initialize.
                        Kernel.Get<ConfigHandler>().Initialize();

                        Timer dllLogTimer = null;
                        var dllLogUploader = new AsyncSpectatorUploader(AppInfo, Constants.LOG_PATH, $"FuelDll_*.log");
                        // We have to poll since we don't know exactly when the dll rolls its logs
                        dllLogTimer = new Timer(_ => {
                            dllLogUploader.EnqueuePrevious();
                            dllLogTimer.Change((int) new TimeSpan(0, 32, 0).TotalMilliseconds, Timeout.Infinite);
                        }, null, (int)new TimeSpan(0, 32, 0).TotalMilliseconds, Timeout.Infinite);

                        var result = program.Start();
                        result.Wait();
                        return result.Result ? SUCCESS : INSUFFICIENT_ARGS;
                    }
                }
                finally
                {
                    CEFStop();
                    LOG.InfoFormat("Stopping Fuel Pump.");
                }
            }
            // catch all logger to make sure it goes to logs as well as console
            catch (Exception ex)
            {
                LOG.Error("Unexpected error", ex);
                return CEF_FAILED;
            }
        }

        static void UnhandledExceptionLogger(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = args.ExceptionObject as Exception;
            LOG.Fatal("Unhandled exception:", e);
            Metrics.AddCounter(FuelPumpMetrics.UnhandledException);
        }
    }
}
