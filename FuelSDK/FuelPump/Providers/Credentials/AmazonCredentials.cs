﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Providers.Credentials
{
    public sealed class AmazonCredentials 
    {
        public AmazonCredentials(string refreshToken)
        {
            RefreshToken = refreshToken;
        }

        public string RefreshToken { get; private set; }
    }
}
