﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Providers.Credentials
{
    public class Credentials
    {
        public Credentials(AmazonCredentials amazonCredentials)
        {
            AmazonCredentials = amazonCredentials;
        }

        public AmazonCredentials AmazonCredentials { get; private set; }
    }

}
