﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Providers.Credentials
{
    public class BearerCredentials : IEquatable<BearerCredentials>
    {
        protected BearerCredentials() { }

        public BearerCredentials(string type, string identifier, string token = null)
        {
            this.Type = type;
            this.Identifier = identifier;
            this.Token = token;
        }

        public virtual string Type { get; private set; }
        public virtual string Identifier { get; private set; }
        public virtual string Token { get; private set; }
        public virtual string Header
        {
            // ADG oauth format unless specified otherwise.
            get { return "X-ADG-Oauth-Token-" + Type; }
        }

        public bool Equals(BearerCredentials that)
        {
            return this.Header == that.Header &&
                this.Type == that.Type &&
                this.Identifier == that.Identifier &&
                this.Token == that.Token;
        }
    }
}
