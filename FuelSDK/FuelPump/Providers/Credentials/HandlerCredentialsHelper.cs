﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Messages = Fuel.Messaging;


namespace FuelPump.Providers.Credentials
{
    // Class to manage common credentials-related tasks in handlers.
    public class HandlerCredentialsHelper
    {
        // https://tools.ietf.org/html/rfc7230#section-3.2
        // https://tools.ietf.org/html/rfc7230#section-3.2.6
        private static readonly Regex HTTP_TOKEN_REGEX = new Regex(@"\A[!#$%&'*+\-.^_`|~0-9A-Za-z]+\z");
        private static readonly Regex HTTP_VALUE_REGEX = new Regex(@"\A[\x21-\x7E \t]+\z");

        // Get a single Credentials object from a Credentials cache. Throw an exception
        // if there isn't exactly one credential object passed along.
        public static BearerCredentials ExtractSingleCredentials(Messages.CredentialsCache cache) 
        {
            if (cache.CredentialsLength > 1)
            {
                throw new UnsupportedCredentialsException("only one bearer credential is currently supported");
            }

            if (cache.CredentialsLength == 0)
            {
                return null;
            }

            var messageCreds = cache.GetCredentials(0);

            if (string.IsNullOrWhiteSpace(messageCreds.Type))
            {
                throw new UnsupportedCredentialsException("Missing credential type");
            }

            if (messageCreds.Type.Length > 100)
            {
                throw new UnsupportedCredentialsException("Credential type is too long");
            }

            // The type is used as part of a header field name.
            if (!HTTP_TOKEN_REGEX.IsMatch(messageCreds.Type))
            {
                throw new UnsupportedCredentialsException("Invalid credential type");
            }

            // The identifier is hashed, but is not used for anything else.
            if (string.IsNullOrWhiteSpace(messageCreds.Identifier))
            {
                throw new UnsupportedCredentialsException("Missing credential identifier");
            }

            if (string.IsNullOrWhiteSpace(messageCreds.Token))
            {
                throw new UnsupportedCredentialsException("Missing credential token");
            }

            // The token is used as a header value.
            if (!HTTP_VALUE_REGEX.IsMatch(messageCreds.Token))
            {
                throw new UnsupportedCredentialsException("Invalid credential token");
            }

            BearerCredentials identifyingCredentials = ConstructCredentials(messageCreds.Type, messageCreds.Identifier, messageCreds.Token);

            return identifyingCredentials;
        }

        private static BearerCredentials ConstructCredentials(string type, string identifier, string token)
        {
            if (TwitchCredentials.TYPE.Equals(type))
            {
                return new TwitchCredentials(identifier, token);
            }

            return new BearerCredentials(type, identifier, token);
        }
    }
}
