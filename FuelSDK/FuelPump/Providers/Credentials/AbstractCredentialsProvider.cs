﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Amazon.Auth.Map;

namespace FuelPump.Providers.Credentials
{
    public abstract class AbstractCredentialsProvider
    {
        private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(AbstractCredentialsProvider));

        /**
         * Produce a name based on the identifying credentials which consistently identifies where they are located.
         * This may be a file path, a registry key, or some other identifier.
         */
        protected abstract string GetStorageLocationForIdentifyingCredentials(BearerCredentials identifyingCredentials);

        /**
         * Override the source of the token for implementations.
         */
        protected abstract ITokenProvider GetTokenProvider(AppInfo appInfo);

        /** 
         * Return ALL credentials retrieved by the provided set of credentials, keyed
         * by the individual credentials' identifiers.
         * 
         * GetCredentials should never return null or empty creds, but should instead
         * throws a CredentialsNotFoundException if they can't be looked up.
         */
        public abstract Credentials GetCredentials(BearerCredentials identifyingCredentials);

        /**
         * Associate a set of credentials with an identifier for later retrieval.  
         */
        public abstract void SetCredentials(BearerCredentials identifyingCredentials, Credentials credentials);

        /**
         * Removes the set of credentials associated with the identifier.
         */
        public abstract void RemoveCredentials(BearerCredentials identifyingCredentials);

        // Generate contents for this instance (user, machine, etc.) and compare them to what was retrieved.
        // Also check that the cred store contains credentials.  
        // Log and throw exceptions if necessary.
        protected void VerifyContents(CredStoreContents contents, BearerCredentials identifyingCredentials)
        {
            CredStoreContents expected = GenerateContents(identifyingCredentials, new Credentials(null));
            try
            {
                expected.VerifyMetadata(contents);
                contents.VerifyCredentials();
            }
            catch (InvalidCredentialsException e)
            {
                LOG.Error("credentials file contents do not match current app configuration: " + e.Message);
                throw (e);
            }
        }

        // Return the non-credentials parts of the cred store.
        // Used both to generate and verify file contents.
        protected virtual CredStoreContents GenerateContents(BearerCredentials identifyingCredentials, Credentials credentials)
        {
            AppInfo appInfo = GetAppInfo();
            CredStoreContents contents = new CredStoreContents();

            contents.Username = appInfo.UserName;
            contents.SerialNumber = appInfo.DeviceSerialId;
            contents.Location = GetStorageLocationForIdentifyingCredentials(identifyingCredentials);
            contents.AmazonCredentials = credentials.AmazonCredentials;

            return contents;
        }

        // Add credentials to a contents object, populate metadata, and serialize.
        protected virtual string GetSerializedContents(BearerCredentials identifyingCredentials, Credentials credentials)
        {
            CredStoreContents fileContents = GenerateContents(identifyingCredentials, credentials);
            return fileContents.Serialize();
        }

        // AmazonAuthMap's file encryption routines assumes we're getting a single credential from a single location.
        // In order to use their stuff, we need to request a "refresh token" which is a string consisting of the unencrypted 
        // but serialized contents.  It's a little hacky, but preferable to forking encryption logic.
        protected CredStoreContents LoadCredsContents(AppInfo appInfo)
        {
            CredStoreContents contents = null;
            try
            {
                ITokenProvider tokenProvider = GetTokenProvider(appInfo);
                AmazonRefreshToken token = tokenProvider.GetRefreshToken();

                if (!token.HasValue)
                {
                    throw new CredentialsNotFoundException("TokenProvider did not return any data");
                }

                contents = CredStoreContents.Deserialize(token.Value); // We fear no NPE while within a try block.
            }
            catch (Exception e)
            {
                string errMsg = "caught exception while attempting to retrieve credentials";

                throw new CredentialsNotFoundException(errMsg, e);
            }

            return contents;
        }

        // For unit tests, so we can override various data reported by AppInfo.
        // Not using a getter because it makes things confusing to read given that we must alter AppInfo state at various points.
        protected virtual AppInfo GetAppInfo()
        {
            return new AppInfo();
        }
    }

    // Structure for encrypted credentials structure.
    public class CredStoreContents
    {
        public string Username { get; set; }
        public string Location { get; set; }
        public string SerialNumber { get; set; }
        public AmazonCredentials AmazonCredentials { get; set; }

        public static CredStoreContents Deserialize(string json)
        {
            var root = JObject.Parse(json);

            return new CredStoreContents
            {
                Username = (string)root["username"],
                Location = (string)root["location"],
                SerialNumber = (string)root["serialNumber"],
                AmazonCredentials = new AmazonCredentials(
                    (string)root["amazonCredentials"]["refreshToken"]),
            };
        }

        public string Serialize()
        {
            var contents = new JObject(
                new JProperty("username", Username),
                new JProperty("location", Location),
                new JProperty("serialNumber", SerialNumber),
                new JProperty("amazonCredentials",
                    new JObject(
                        new JProperty("refreshToken", AmazonCredentials.RefreshToken)
                    )
                 )
             );

            return contents.ToString();
        }

        public void VerifyMetadata(CredStoreContents that)
        {
            if (!String.Equals(this.Username, that.Username)) throw new InvalidCredentialsException(String.Format("expected Username {0}, got {1}", this.Username, that.Username));
            if (!String.Equals(this.Location, that.Location)) throw new InvalidCredentialsException(String.Format("expected Location {0}, got {1}", this.Location, that.Location));
            if (!String.Equals(this.SerialNumber, that.SerialNumber)) throw new InvalidCredentialsException(String.Format("expected SerialNumber {0}, got {1}", this.SerialNumber, that.SerialNumber));            
        }

        public void VerifyCredentials()
        {
            if (AmazonCredentials == null) throw new InvalidCredentialsException("null credentials retrieved from cred store");

            if(String.IsNullOrWhiteSpace(AmazonCredentials.RefreshToken))
            {
                throw new InvalidCredentialsException("neither ADP token/key nor oauth refresh token retrieved from cred store");
            }
        }
    }
}
