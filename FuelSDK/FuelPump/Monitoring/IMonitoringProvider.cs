﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPump.Monitoring
{
    public interface IMonitoringProvider
    {
        void LogRuntimeFailure(Exception ex);

        void LogUnhandledHandlerException(Exception ex);
        void LogUnhandledMessage(Message message);
    }
}
