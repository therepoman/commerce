﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Messages = Fuel.Messaging;

namespace FuelPump
{
    public class MessageFactory<T> where T : class    
    {
        Messages.Message m_type;
        Func<FlatBuffers.FlatBufferBuilder, FlatBuffers.Offset<T>> m_builder;

        public MessageFactory(Func<FlatBuffers.FlatBufferBuilder, FlatBuffers.Offset<T>> builder) 
        {
            // TODO: [FUEL-812] perform the lookup of message types from a dictionary
            m_type = (Messages.Message)Enum.Parse(typeof(Messages.Message), typeof(T).Name);
            m_builder = builder;
        }

        public Message Create(MessageContext context)
        { 
            FlatBuffers.FlatBufferBuilder builder = new FlatBuffers.FlatBufferBuilder(1);

            var messageRoot = Messages.MessageRoot.CreateMessageRoot(builder, Messages.FuelResponseType.Success, m_type, m_builder(builder).Value);
            
            builder.Finish(messageRoot.Value);

            return new Message(context.BuildResponseHeader((int)m_type), builder.SizedByteArray());
        }
    }
}
