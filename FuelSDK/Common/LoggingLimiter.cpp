#include "LoggingLimiter.h"

Fuel::LoggingLimiter::LoggingLimiter(int maxLogLines, const std::string &loggerFile, const char* finalMessage) : m_loggerFile(loggerFile)
{
	m_maxLogLines = maxLogLines;
	m_finalMessage = finalMessage;
}

Fuel::LoggingLimiter::~LoggingLimiter()
{
}

bool Fuel::LoggingLimiter::acquire()
{
	if (m_currentLogLines < m_maxLogLines)
	{
		m_currentLogLines++;
		return true;
	}

	if (m_currentLogLines == m_maxLogLines)
	{
		m_currentLogLines++;
		Logger::Instance(m_loggerFile)->warn(m_finalMessage);
	}

	return false;
}