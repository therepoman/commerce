#pragma once

#include <mutex>

namespace Fuel {
	class MutexTryLock
	{
	public:
		MutexTryLock(std::mutex &mutex);
		~MutexTryLock();
		bool isLocked();
	private:
		std::mutex* m_mutex;
		bool m_locked;
	};
}
