#pragma once

#include "logger.h"

namespace Fuel {
	class LoggingLimiter
	{
	public:
		LoggingLimiter(int maxLogLines, const std::string &loggerFile, const char* finalMessage);
		~LoggingLimiter();
		bool acquire();
	private:
		int m_currentLogLines = 0;
		int m_maxLogLines;
		const std::string &m_loggerFile;
		const char* m_finalMessage;
	};
}
