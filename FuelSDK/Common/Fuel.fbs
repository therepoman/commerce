namespace Fuel.Messaging;

// -------------------------------- Credentials --------------------------------

table Credential {
	Type:string (id: 0);
	Identifier:string (id: 1);
	Token:string (id: 2);
}

table CredentialsCache {
	Credentials:[Credential] (id: 0);
}

// -------------------------------- Enums --------------------------------

enum ProductType:short {
	Consumable = 0,
	Entitlement = 1,
	Subscription = 2,
	Unknown = 1000,
}

enum FuelResponseType:short {
	Success = 0,
	Failed = 1,
	NotLoggedIn = 2,

	// GetProductData
	TooManySkus = 100,
}

enum GetProductDataResponseStatus:short {
	Success = 0,
	Failed = 1,
	CredentialsRequired = 2,
	InvalidCredentials = 3,
	InvalidArgument = 4,
}

enum KeyEventType:short {
	KeyDown = 0,
	KeyUp = 1,
	Char = 2,
}

enum MouseButton:short {
	None = 0,
	Left = 1,
	Middle = 2,
	Right = 3,
}

enum FulfillmentResult:short {
	Fulfilled = 0,
	Unavailable = 1
}

enum GetPurchaseUpdatesResponseStatus:short {
	Success = 0,
	Failed = 1,
	CredentialsRequired = 2,
	InvalidCredentials = 3,
	TimedOut = 4,
}

enum PurchaseResponseStatus:short {
	Success = 0,
	Failed = 1,
	CredentialsRequired = 2,
	InvalidCredentials = 3,
	Cancelled = 4,
	InvalidArgument = 5,
	Processing = 6,
}

enum NotifyFulfillmentResponseStatus:short {
	Success = 0,
	Failed = 1,
	CredentialsRequired = 2,
	InvalidCredentials = 3,
	InvalidArgument = 4,
	NonRetryableError = 5,
}

enum GetSessionResponseStatus:short {
	Success = 0,
	Failed = 1,
	AccessDenied = 2
}

// -------------------------------- Messages --------------------------------

// **DO NOT RE-ORDER** Order is important for backward-compatibility
union Message {
	FatalError,
	InitializeRequest,
	InitializeResponse,
	ShutdownRequest,
	GetProductDataRequest,
	GetProductDataResponse,
	PurchaseRequest,
	PurchaseResponse,
	KeyboardEvent,
	MouseEvent,
	MouseMoveEvent,
	MouseWheelEvent,
	SetWindowSize,
	ShowOverlay,
	HideOverlay,
	UpdateOverlay,
	NotifyFulfillmentRequest,
	NotifyFulfillmentResponse,
	UpdateCursor,
	GetPurchaseUpdatesRequest,
	GetPurchaseUpdatesResponse,
	CloseOverlayEvent,
	DEPRECATED_RefreshOverlayEvent,
	OverlayFailureEvent,
	GetSessionRequest,
	GetSessionResponse
}

table MessageRoot {
	responseType:FuelResponseType (id: 0);
	message:Message (id: 2); // Implicit enum field for union with id:1
}

root_type MessageRoot;

table FatalError {
}

table InitializeRequest {
	gameVersion:string (id: 0);
	sdkVersion:string (id: 1);
	vendorId:string (id: 2);
}

table InitializeResponse {
}

table ShutdownRequest {
}

table GetProductDataRequest {
	credentials:CredentialsCache (id: 0);
	skus:[string] (id: 1);
}

table GetProductDataResponse {
	unavailableSkus:[string] (id: 0);
	products:[Product] (id: 1);
	status:GetProductDataResponseStatus (id: 2);
}

table PurchaseRequest {
	credentials:CredentialsCache (id: 0);
	sku:string (id: 1);
}

table PurchaseResponse {
	receipt:Receipt (id: 0);
	status:PurchaseResponseStatus (id: 1);
}

table KeyboardEvent {
	instanceId:int (id: 0);
	type:KeyEventType (id: 1);
	keyCode:long (id: 2);
	ctrlDown:bool (id: 3);
}

table MouseEvent {
	instanceId:int (id: 0);
	x:int (id: 1);
	y:int (id: 2);
	button:MouseButton (id: 3);
	mouseUp:bool (id: 4);
	clickCount:int (id: 5);
}

table MouseMoveEvent {
	instanceId:int (id: 0);
	x:int (id: 1);
	y:int (id: 2);
	leftMouseDown:bool (id: 3);
}

table MouseWheelEvent {
	instanceId:int (id: 0);
	x:int (id: 1);
	y:int (id: 2);
	deltaX:int (id: 3);
	deltaY:int (id: 4);
}

table SetWindowSize {
	width:int (id: 0);
	height:int (id: 1);
}

table ShowOverlay {
	instanceId:int (id: 0);
	x:int (id: 1);
	y:int (id: 2);
	width:int (id: 3);
	height:int (id: 4);
	z:int (id: 5);
}

table HideOverlay {
	instanceId:int (id: 0);
}

table UpdateOverlay {
// left empty so message type shows up in generated code.  Actual body is sent as raw binary data
}

table NotifyFulfillmentRequest {
	credentials:CredentialsCache (id: 0);
	receiptId:string (id: 1);
	fulfillmentResult:FulfillmentResult (id: 2);
}

table NotifyFulfillmentResponse {
	result:string (id: 0);
	status:NotifyFulfillmentResponseStatus (id: 1);
}

table UpdateCursor {
	instanceId:int (id:0);
	cursor:int (id:1);
}

table GetPurchaseUpdatesRequest {
	credentials:CredentialsCache (id: 0);
	syncPoint:SyncPointData (id: 1);
}

table GetPurchaseUpdatesResponse {
	syncPoint:SyncPointData (id: 0);
	updates:[Receipt] (id: 1);
	status:GetPurchaseUpdatesResponseStatus (id: 2);
}

table GetSessionRequest {
	clientId:string (id: 0);
}

table GetSessionResponse {
	user:TwitchUser (id: 0);
	entitlement:string (id: 1);
	status:GetSessionResponseStatus (id: 2);
}

// The user wants to close the overlay.
table CloseOverlayEvent {
	instanceId:int (id: 0);
}

table DEPRECATED_RefreshOverlayEvent {
	instanceId:int (id: 0);
}

// The overlay had an internal failure.
table OverlayFailureEvent {
	instanceId:int (id: 0);
}

// -------------------------------- Other structures --------------------------------

table Product {
	sku:string (id: 0);
	description:string (id: 1);
	price:string (id: 2);
	smallIconUrl:string (id: 3);
	title:string (id: 4);
	productType:ProductType (id: 5);
}

table Receipt {
	receiptId:string (id: 0);
	sku:string (id: 1);
	productType:ProductType (id: 2);
	purchaseDate:ulong (id: 3);
	cancelDate:ulong (id: 4);
}

table SyncPointData {
	syncToken:string (id: 0);
	timestamp:ulong (id: 1);
}

table TwitchUser {
	accessToken:string (id: 0);
	logo:string (id: 1);
	username:string (id: 2);
	displayName:string (id: 3);
	tuid:string (id: 4);
	accessTokenCreationTimestamp:ulong (id: 5);
}