#pragma once

#include <functional>

namespace Fuel {
	class IOverlayInstance;

	class BufferSize
	{
	public:
		UINT width = 0;
		UINT height = 0;
	};

	typedef std::function<HRESULT(IOverlayInstance &instance)> INSTANCE_HANDLER;
	typedef std::function<void(BufferSize &bufferSize)> RESIZE_CALLBACK;
	typedef std::function<void(HWND hWnd)> ENABLE_INPUT_HOOKS_CALLBACK;
	typedef std::function<void()> DISABLE_INPUT_HOOKS_CALLBACK;
	typedef std::function<void(IOverlayInstance &instance)> OVERLAY_FAILURE_CALLBACK;
}
