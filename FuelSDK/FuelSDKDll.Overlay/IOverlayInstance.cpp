#include "IOverlayInstance.h"

Fuel::IOverlayInstance::IOverlayInstance(int instanceId, const RECT &location, int zIndex)
	: m_instanceId(instanceId), m_location(location), m_zIndex(zIndex)
{
}

Fuel::IOverlayInstance::~IOverlayInstance()
{
}

int Fuel::IOverlayInstance::GetInstanceId() const
{
	return m_instanceId;
}

const RECT& Fuel::IOverlayInstance::GetLocation() const
{
	return m_location;
}

int Fuel::IOverlayInstance::GetZIndex() const
{
	return m_zIndex;
}
