#include "DX11OverlayFactory.h"

#include "dxgi.h"
#include "Hook.h"
#include "logger.h"

using namespace Fuel::DX11;

typedef HRESULT(__stdcall *PresentType)(IDXGISwapChain *, UINT, UINT);
typedef HRESULT(__stdcall *ResizeType)(IDXGISwapChain *, UINT, UINT, UINT, DXGI_FORMAT, UINT);

static HRESULT __stdcall overlayPresent(IDXGISwapChain *pSwapChain, UINT SyncInterval, UINT Flags)
{
	return DX11OverlayFactory::Instance()->Present(pSwapChain, SyncInterval, Flags);
}

static HRESULT __stdcall overlayResize(IDXGISwapChain *pSwapChain, UINT BufferCount, UINT Width, UINT Height, DXGI_FORMAT NewFormat, UINT SwapChainFlags)
{
	return DX11OverlayFactory::Instance()->Resize(pSwapChain, BufferCount, Width, Height, NewFormat, SwapChainFlags);
}

// MinHook requires a function pointer, which has to be static.
// That means a static singleton is needed, and this method.
// Using a static weak pointer allows the SDK to mostly own the instance lifetime by holding onto the primary shared_ptr,
// and allow the static methods to temporarily hold the instance alive while they're busy.
std::shared_ptr<DX11OverlayFactory> DX11OverlayFactory::Instance() {
	static std::weak_ptr<DX11OverlayFactory> s_instance;
	static std::mutex s_initializationMutex;

	std::shared_ptr<DX11OverlayFactory> result = s_instance.lock();
	if (!result)
	{
		std::lock_guard<std::mutex> lock(s_initializationMutex);
		std::shared_ptr<DX11OverlayFactory> instanceAfterLock = s_instance.lock();
		if (instanceAfterLock)
		{
			result = std::move(instanceAfterLock);
		}
		else
		{
			std::shared_ptr<DX11OverlayFactory> newInstance(new DX11OverlayFactory());
			result = std::move(newInstance);
			s_instance = result;
		}
	}
	return std::move(result);
}

DX11OverlayFactory::DX11OverlayFactory() : m_presentHook(), m_resizeHook(),
    m_getHwndErrorLoggingLimiter(20, FUEL_DLL_LOGGER, "Too many errors getting swap chain description, disabling logging of this message")
{
}

DX11OverlayFactory::~DX11OverlayFactory()
{
	Release();
}

void DX11OverlayFactory::Init()
{
	if (m_hooksEnabled)
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Render hooks are already set up.");
	}
	else
	{
		// Save current cursor.
		ULONG_PTR prev = SetClassLongPtr(GetForegroundWindow(), GCLP_HCURSOR, (LONG_PTR)LoadCursor(NULL, IDC_ARROW));
		m_prevCursor = (HCURSOR)prev;

		// install hook
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Setting up DirectX11 hooks.");
		SetupSwapChainHooks(&m_presentHook, reinterpret_cast<voidFunc>(overlayPresent), &m_resizeHook, reinterpret_cast<voidFunc>(overlayResize));
		m_hooksEnabled = true;
	}
}

void DX11OverlayFactory::Release()
{
	if (m_hooksEnabled)
	{
		m_hooksEnabled = false;
		
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Releasing DirectX11 hooks.");
		m_presentHook.restore();
		m_resizeHook.restore();
		m_hWnd = 0;
		m_swapChain.Release();

		// Reload saved cursor.
		SetClassLongPtr(GetForegroundWindow(), GCLP_HCURSOR, (LONG_PTR)m_prevCursor);
	}
	else
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Can't hide overlay, it's not currently visible.");
	}
}

std::unique_ptr<Fuel::IOverlayInstance> DX11OverlayFactory::CreateOverlay(int instanceId, const RECT &location, int zIndex)
{
	std::unique_ptr<IOverlayInstance> instance(new DX11OverlayInstance(instanceId, location, zIndex));
	return std::move(instance);
}

bool DX11OverlayFactory::getBufferSize(Fuel::BufferSize &bufferSize)
{
	return GetBufferSize(m_swapChain, bufferSize);
}

voidFunc DX11OverlayFactory::GetOriginalPresentFunction() {
	return m_presentHook.m_originalFunc;
}

voidFunc DX11OverlayFactory::GetOriginalResizeFunction() {
	return m_resizeHook.m_originalFunc;
}

HRESULT DX11OverlayFactory::Present(IDXGISwapChain *pSwapChain, UINT SyncInterval, UINT Flags)
{
	static std::mutex s_hWndInitializationMutex;
	std::shared_ptr<OverlayManager> overlayManager = m_overlayManager.lock();

	if (overlayManager)
	{
		if (!m_hWnd)
		{
			// init only once
			std::lock_guard<std::mutex> lock(s_hWndInitializationMutex);
			if (!m_hWnd)
			{
				Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Initializing Render hooks.");

				m_swapChain = pSwapChain;

				DXGI_SWAP_CHAIN_DESC swapChainDesc;
				if (SUCCEEDED(pSwapChain->GetDesc(&swapChainDesc)))
				{
					overlayManager->SetHWnd(swapChainDesc.OutputWindow);
					m_hWnd = swapChainDesc.OutputWindow;
					BufferSize bufferSize;
					bufferSize.width = swapChainDesc.BufferDesc.Width;
					bufferSize.height = swapChainDesc.BufferDesc.Height;
					overlayManager->SetBufferSize(bufferSize);
				}
				else if (m_getHwndErrorLoggingLimiter.acquire())
				{
					Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("DX11: Unable to get swap chain description");
				}
			}
		}

		overlayManager->Render([this, pSwapChain](IOverlayInstance &instance) -> HRESULT {
			DX11OverlayInstance *dx11Instance = dynamic_cast<DX11OverlayInstance*>(&instance);
			if (dx11Instance)
			{
				return dx11Instance->Render(pSwapChain);
			}
			return S_OK;
		});
	}

	// call real present
	PresentType oPresent = (PresentType)GetOriginalPresentFunction();
	return oPresent(pSwapChain, SyncInterval, Flags);
}

HRESULT DX11OverlayFactory::Resize(IDXGISwapChain *pSwapChain, UINT BufferCount, UINT Width, UINT Height, DXGI_FORMAT NewFormat, UINT SwapChainFlags)
{
	std::shared_ptr<OverlayManager> overlayManager = m_overlayManager.lock();

	Release();
	if (overlayManager)
	{
		overlayManager->Release();
	}

	ResizeType oResize = (ResizeType)GetOriginalResizeFunction();
	HRESULT result = oResize(pSwapChain, BufferCount, Width, Height, NewFormat, SwapChainFlags);

	Init();

	if (overlayManager)
	{
		overlayManager->Reinitialize();
		BufferSize bufferSize;
		bufferSize.width = Width;
		bufferSize.height = Height;
		overlayManager->Resize(bufferSize);
	}

	return result;
}
