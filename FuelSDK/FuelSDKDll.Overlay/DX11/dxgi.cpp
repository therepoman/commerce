#include "dxgi.h"
#include "logger.h"

#include <d3d11_1.h>
#include <string>
#include <atlbase.h>

typedef HRESULT(__stdcall *D3D11CreateDeviceAndSwapChainType)(IDXGIAdapter *, D3D_DRIVER_TYPE, HMODULE Software, UINT Flags, const D3D_FEATURE_LEVEL *, UINT, UINT, const DXGI_SWAP_CHAIN_DESC *, IDXGISwapChain **, ID3D11Device **, D3D_FEATURE_LEVEL *, ID3D11DeviceContext **);
typedef HRESULT(__stdcall *CreateDXGIFactory1Type)(REFIID, void **);

using namespace Fuel;

int GetFnOffsetInModule(voidFunc fnptr, wchar_t *refmodulepath, unsigned int refmodulepathLen, const std::string &logPrefix, const std::string &fnName) {

	HMODULE hModule = NULL;

	if (!GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT, (LPCWSTR)fnptr, &hModule)) {
		Logger::Instance(FUEL_DLL_LOGGER)->error("Failed to get module for {0} !!", fnName);
		return -1;
	}

	const bool bInit = refmodulepath[0] == '\0';
	if (bInit) {
		GetModuleFileNameW(hModule, refmodulepath, refmodulepathLen);
	}
	else {
		wchar_t modulename[Fuel::DX11::MODULEFILEPATH_BUFLEN];
		GetModuleFileNameW(hModule, modulename, ARRAY_NUM_ELEMENTS(modulename));
		if (_wcsicmp(modulename, refmodulepath) != 0) {
			std::wstring modulenameStr(modulename);
			std::string s1(modulenameStr.begin(), modulenameStr.end());
			std::wstring refmodulepathStr(refmodulepath);
			std::string s2(refmodulepathStr.begin(), refmodulepathStr.end());
			Logger::Instance(FUEL_DLL_LOGGER)->error("{0} function's module path does not match previously found. Now: {1}, Previously: {2} !!", fnName, s1, s2);
			return -2;
		}
	}

	unsigned char *fn = reinterpret_cast<unsigned char *>(fnptr);
	unsigned char *base = reinterpret_cast<unsigned char *>(hModule);
	unsigned long off = static_cast<unsigned long>(fn - base);

	return static_cast<int>(off);
}

// Extract offsets for various functions in the IDXGISwapChain
// interface that need to be hooked.
void GetDXGIData(IDXGIAdapter1* pAdapter, Fuel::DX11::DXGIData* dxgi)
{
	if (!dxgi || !pAdapter)
		return;

	HMODULE hD3D11 = LoadLibrary(L"D3D11.DLL");

	if (hD3D11 != NULL) {

		HWND hwnd = CreateWindowW(L"STATIC", L"SDK DXGI Window", WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT, CW_USEDEFAULT, 640, 480, 0,
			NULL, NULL, 0);

		D3D11CreateDeviceAndSwapChainType pD3D11CreateDeviceAndSwapChain = reinterpret_cast<D3D11CreateDeviceAndSwapChainType>(GetProcAddress(hD3D11, "D3D11CreateDeviceAndSwapChain"));

		DXGI_SWAP_CHAIN_DESC desc;
		ZeroMemory(&desc, sizeof(desc));

		RECT rcWnd;
		BOOL success = GetClientRect(hwnd, &rcWnd);
		if (success) {
			desc.BufferDesc.Width = rcWnd.right - rcWnd.left;
			desc.BufferDesc.Height = rcWnd.bottom - rcWnd.top;

			desc.BufferDesc.RefreshRate.Numerator = 60;
			desc.BufferDesc.RefreshRate.Denominator = 1;
			desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
			desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
			desc.BufferDesc.Scaling = DXGI_MODE_SCALING_CENTERED;

			desc.SampleDesc.Count = 1;
			desc.SampleDesc.Quality = 0;

			desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;

			desc.BufferCount = 2;

			desc.OutputWindow = hwnd;

			desc.Windowed = true;

			desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

			CComPtr<IDXGISwapChain> swapChain;
			CComPtr<ID3D11Device> device;
			D3D_FEATURE_LEVEL featureLevel;
			CComPtr<ID3D11DeviceContext> deviceContext;
			HRESULT hr = pD3D11CreateDeviceAndSwapChain(pAdapter, D3D_DRIVER_TYPE_UNKNOWN, NULL, 0, NULL, 0, D3D11_SDK_VERSION, &desc, &swapChain, &device, &featureLevel, &deviceContext);

			if (device && deviceContext && swapChain) {

				// For VC++ the vtable is located at the base addr. of the object and each function entry is a single pointer. Since p.e. the base classes
				// of IDXGISwapChain have a total of 8 functions the 8+Xth entry points to the Xth added function in the derived interface.

				void ***vtbl = (void ***)swapChain.p;

				void *pPresent = (*vtbl)[8];
				int offset = GetFnOffsetInModule(reinterpret_cast<voidFunc>(pPresent), dxgi->wcFileName, ARRAY_NUM_ELEMENTS(dxgi->wcFileName), "D3D11", "Present");
				if (offset >= 0) {
					dxgi->iOffsetPresent = offset;
				}

				// ResizeBuffers is the 6th function, 5 more than present.
				void *pResizeBuffers = (*vtbl)[8 + 5];
				int offsetResizeBuffers = GetFnOffsetInModule(reinterpret_cast<voidFunc>(pResizeBuffers), dxgi->wcFileName, ARRAY_NUM_ELEMENTS(dxgi->wcFileName), "D3D11", "ResizeBuffers");
				if (offsetResizeBuffers >= 0) {
					dxgi->iOffsetResize = offsetResizeBuffers;
				}
			}
		}
		else {
			Logger::Instance(FUEL_DLL_LOGGER)->error("Failed to get client rect when setting up hook !!");
			FreeLibrary(hD3D11);
		}

		DestroyWindow(hwnd);
	}
	else {
		Logger::Instance(FUEL_DLL_LOGGER)->error("Failed to load D3D11 library to setup hook !!");
		FreeLibrary(hD3D11);
	}
}

void Fuel::DX11::SetupSwapChainHooks(Fuel::Hooking::Hook *presentHook, voidFunc overlayPresent, Fuel::Hooking::Hook *resizeHook, voidFunc overlayResize) {
	Logger::Instance(FUEL_DLL_LOGGER)->debug("Setting up DX11 hook.");
	HMODULE hDXGI = LoadLibrary(L"DXGI.DLL");

	Fuel::DX11::DXGIData dxgi;
	dxgi.iOffsetPresent = 0;
	dxgi.iOffsetResize = 0;

	if (hDXGI != NULL) {
		GetModuleFileNameW(hDXGI, dxgi.wcFileName, ARRAY_NUM_ELEMENTS(dxgi.wcFileName));
		CreateDXGIFactory1Type pCreateDXGIFactory1 = reinterpret_cast<CreateDXGIFactory1Type>(GetProcAddress(hDXGI, "CreateDXGIFactory1"));
		if (pCreateDXGIFactory1) {
			CComPtr<IDXGIFactory1> factory;
			HRESULT hr = pCreateDXGIFactory1(__uuidof(IDXGIFactory1), (void**)(&factory));

			if (factory) {
				CComPtr<IDXGIAdapter1> adapter;
				factory->EnumAdapters1(0, &adapter);
				GetDXGIData(adapter, &dxgi);
			}

			if (dxgi.iOffsetPresent > 0) {
				unsigned char* raw = (unsigned char*)hDXGI;
				presentHook->setup((voidFunc)(raw + dxgi.iOffsetPresent), reinterpret_cast<voidFunc>(overlayPresent));
			}

			if (dxgi.iOffsetResize > 0) {
				unsigned char* raw = (unsigned char*)hDXGI;
				resizeHook->setup((voidFunc)(raw + dxgi.iOffsetResize), reinterpret_cast<voidFunc>(overlayResize));
			}
		}
		else {
			Logger::Instance(FUEL_DLL_LOGGER)->error("Failed get process address of CreateDXGIFactory1 to setup hook !!");
			FreeLibrary(hDXGI);
		}
	}
	else {
		Logger::Instance(FUEL_DLL_LOGGER)->error("Failed to load DXGI library to setup hook !!");
		FreeLibrary(hDXGI);
	}
}

bool Fuel::DX11::GetBufferSize(IDXGISwapChain *swapChain, BufferSize &bufferSize)
{
	if (!swapChain)
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Fuel::DX11::GetBufferSize: No swap chain.");
		return false;
	}

	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	HRESULT getDescResult = swapChain->GetDesc(&swapChainDesc);
	if (FAILED(getDescResult))
	{
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->error("Fuel::DX11::GetBufferSize: GetDesc failed with code {0}", getDescResult);
		return false;
	}

	bufferSize.width = swapChainDesc.BufferDesc.Width;
	bufferSize.height = swapChainDesc.BufferDesc.Height;
	return true;
}
