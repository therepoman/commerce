#pragma once

#include "IOverlayUpdate.h"
#include <memory>
#include <Windows.h>
#include <stdexcept>

namespace Fuel {

	class DirectXHookCreationFailed : public std::runtime_error {
	public:
		DirectXHookCreationFailed() : runtime_error("Failed to setup hooks necessary to draw the overlay with Direct X") {};
	};

	class IOverlayInstance
	{
	public:
		IOverlayInstance(int instanceId, const RECT &location, int zIndex);
		virtual ~IOverlayInstance();

		virtual void Release() = 0;
		virtual HRESULT Reinitialize() = 0;

		virtual void UpdateTexture(std::shared_ptr<Fuel::IOverlayUpdate> update) = 0;

		int GetInstanceId() const;
		const RECT& GetLocation() const;
		int GetZIndex() const;
	protected:
		int m_instanceId;
		RECT m_location;
		int m_zIndex;
	};
}
