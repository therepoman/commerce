#include "HookContext.h"
#include "MinHook/MinHook.h"

#include "logger.h"

using namespace Fuel::Hooking;

HookContext* HookContext::m_pInstance = NULL;

HookContext::~HookContext() {
	if (m_pInstance) {
		delete m_pInstance;
	}
}

HookContext* HookContext::Instance() {
	if (!m_pInstance) {
		m_pInstance = new HookContext();
	}

	return m_pInstance;
}

void HookContext::Initialize() {
	// init only once
	if (InterlockedCompareExchange(&s_Init, 1, 0) == 0)
	{
		MH_STATUS status = MH_Initialize();
		if (status != MH_OK && status != MH_ERROR_ALREADY_INITIALIZED)
		{
			Logger::Instance(FUEL_DLL_LOGGER)->error("Failed to initialize MinHook!");
		}
	}
}