#pragma once

typedef void *(*voidFunc)();

namespace Fuel {
	namespace Hooking {
		class Hook {
		public:
			Hook();
			Hook(voidFunc func, voidFunc replacement);
			void setup(voidFunc func, voidFunc replacement);
			void restore();
			// Pointer to trampoline function used to call original function
			voidFunc m_originalFunc;
		private:
			// Function being hooked
			voidFunc m_func;
			// Function to call instead
			voidFunc m_replacement;
		};
	}
}