#include "HookContext.h"
#include "Hook.h"
#include "logger.h"

#include "MinHook/MinHook.h"

using namespace Fuel;
using namespace Fuel::Hooking;

Hook::Hook() :
m_func(NULL),
m_replacement(NULL),
m_originalFunc(NULL)
{
	HookContext::Instance()->Initialize();
}

Hook::Hook(voidFunc func, voidFunc replacement) :
m_func(NULL),
m_replacement(NULL),
m_originalFunc(NULL)
{
	HookContext::Instance()->Initialize();
	setup(func, replacement);
}

void Hook::setup(voidFunc func, voidFunc replacement)
{
	m_func = func;
	m_replacement = replacement;
	MH_STATUS createStatus = MH_CreateHook((LPVOID)func, (LPVOID)replacement, (LPVOID*)&m_originalFunc);
	if (createStatus != MH_OK)
	{
		Logger::Instance(FUEL_DLL_LOGGER)->error("Create Hook Failed");
	}

	MH_STATUS enableStatus = MH_EnableHook((LPVOID)m_func);
	if (enableStatus != MH_OK)
	{
		Logger::Instance(FUEL_DLL_LOGGER)->error("Enable Hook Failed");
	}
}

void Hook::restore()
{
	MH_STATUS status = MH_DisableHook((LPVOID)m_func);
	if (status != MH_OK)
	{
		Logger::Instance(FUEL_DLL_LOGGER)->error("Disable Hook Failed");
	}
}