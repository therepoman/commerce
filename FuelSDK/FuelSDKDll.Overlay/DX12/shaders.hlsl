//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

struct PSOverlayInput
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD;
};

Texture2D g_texture : register(t0);
SamplerState g_sampler : register(s0);

PSOverlayInput VSOverlay(float4 position : POSITION, float2 uv : TEXCOORD)
{
	PSOverlayInput result;

	result.position = position;
	result.uv = uv;

	return result;
}

float4 PSOverlay(PSOverlayInput input) : SV_TARGET
{
	return g_texture.Sample(g_sampler, input.uv);
}
