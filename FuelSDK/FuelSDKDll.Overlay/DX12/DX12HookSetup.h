#pragma once

#include <stdint.h>
#include "Hook.h"

#define ARRAY_NUM_ELEMENTS(x) (sizeof(x)/sizeof((x)[0]))

#define OFFSET_NOT_FOUND 0

typedef void *(*voidFunc)();

namespace Fuel {
	namespace DX12 {
		struct DX12LibData {
			uint64_t commandQueueOffset;
			voidFunc presentFunction;
			voidFunc resizeFunction;
		};

        void GetLibData(DX12LibData* data);
	}
}