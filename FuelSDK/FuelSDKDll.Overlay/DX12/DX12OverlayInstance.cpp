#include <d3dcompiler.h>
#include "d3dx12.h"
#include "DX12Helper.h"
#include "DX12OverlayInstance.h"
#include "logger.h"

using namespace Fuel::DX12;

void DX12OverlayInstance::Initialize(IDXGISwapChain* pSwapChain) {
	// Get the device off the swap chain (as is done prior to a Present() in the render hook)
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Initializing DX12 Overlay instance.");
	pSwapChain->GetDevice(__uuidof(ID3D12Device), (void**)&m_d3d12Device);

	// For the overlay to work, we need both that we have the same amount of render targets as the swap chain, and that we render to them in the same fashion
	// as the parent application does. We get the current frame index, the number of buffers, and their size here.
	IDXGISwapChain3* typedSwapChain = (IDXGISwapChain3 *)pSwapChain;
	DXGI_SWAP_CHAIN_DESC1 typedSwapChainDescription;
	typedSwapChain->GetDesc1(&typedSwapChainDescription);

	// Get the frame index, count and width and height of the swap chain.
	m_frameIndex = typedSwapChain->GetCurrentBackBufferIndex();
	m_frameCount = typedSwapChainDescription.BufferCount;
	m_width = typedSwapChainDescription.Width;
	m_height = typedSwapChainDescription.Height;
	m_textureWidth = m_width;
	m_textureHeight = m_height;

	// Now that we know the number of frames and the size of the swap chain, we can create the objects that needed those values.
	// First, the fence values for each frame. These should all start at 0.
	m_fenceValues = new UINT64[m_frameCount];
	for (int i = 0; i < m_frameCount; i++) {
		m_fenceValues[i] = 0;
	}

	// Next, the viewport, this is essentially the area we render to.
	m_viewport.Width = static_cast<float>(m_width);
	m_viewport.Height = static_cast<float>(m_height);
	m_viewport.MaxDepth = 1.0f;

	// Finally, the scissor rectangle, another structure used for clipping.
	m_scissorRect.right = static_cast<LONG>(m_width);
	m_scissorRect.bottom = static_cast<LONG>(m_height);

	// Now we try and fetch the command queue from within the swpachain. This is done by offsetting the chain's memory address by 0xd0, which is the location where the
	// associated command queue's address is located.
	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Fetched swap chain data : {0} {1} {2}", m_frameCount, m_width, m_height);
	uint64_t swapChainAddress = (uint64_t)pSwapChain;
	uint64_t offset = m_libData.commandQueueOffset;
	uint64_t fetchQueueAddress = swapChainAddress + offset;

	// Address is written in memory in little endian ordering
	uint32_t upperBits = *reinterpret_cast<uint32_t *>(fetchQueueAddress + 0x4);
	uint32_t lowerBits = *reinterpret_cast<uint32_t *>(fetchQueueAddress);
	uint64_t calculatedQueueAddress = 0;

	// Put in the upper bits of the queue address.
	calculatedQueueAddress += upperBits;
	calculatedQueueAddress = calculatedQueueAddress << 32;
	calculatedQueueAddress += lowerBits;
	void* queueBlob = (void*)calculatedQueueAddress;

	m_fetchedCommandQueue = (ID3D12CommandQueue*)queueBlob;

	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Obtained command queue from swap chain !");

	// Create descriptor heaps.
	CreateDescriptorHeaps();
	// Create frame resources.
	CreateFrameResources(pSwapChain);

	// Create an empty root signature.
	CreateRootSignature();
	// Create the pipeline state, which includes compiling and loading shaders.
	CreatePSO();
	// Create the command lists.
	CreateCommandLists();
	// Create the fences and other sync objects.
	CreateFences();

	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Completed creating DX12 rendering pipeline !");

	// Create the mesh buffer that will house the overlay.
	CreateMeshBuffers();
	// Create texture which will be updated with overlay data.
	CreateTexture();

	Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Completed creating DX12 asset creation !");
}
/*
Wait for pending GPU work on the current frame to complete.
*/
void DX12OverlayInstance::WaitForGpu()
{
	// Schedule a Signal command in the queue.
	ThrowIfFailed(m_fetchedCommandQueue->Signal(m_fence.Get(), m_fenceValues[m_frameIndex]));

	// Wait until the fence has been processed.
	ThrowIfFailed(m_fence->SetEventOnCompletion(m_fenceValues[m_frameIndex], m_fenceEvent));
	WaitForSingleObjectEx(m_fenceEvent, INFINITE, FALSE);

	// Increment the fence value for the current frame.
	m_fenceValues[m_frameIndex]++;
}

void DX12OverlayInstance::UpdateTexture(std::shared_ptr<Fuel::IOverlayUpdate> update) {
	if (m_initialized) {
		WaitForGpu();
		UpdateTextureInternal(update);

		if (!m_updated && update->GetDirtyRectCount() == 1) {
			SetRect(&m_location, update->getX(), update->getY(), update->getX() + update->getWidth(), update->getY() + update->getHeight());
			m_updated = true;
		}
	}
	else {
		Fuel::Logger::Instance(FUEL_DLL_LOGGER)->debug("Can't update texture, overlay not initialized.");
	}
}

HRESULT DX12OverlayInstance::RenderOverlay(IDXGISwapChain* pSwapChain) {
	std::lock_guard<std::mutex> lock(m_render_mutex);

	if (!m_initialized) {
		// Initialize the overlay the first time it is rendered.
		Initialize(pSwapChain);
		m_initialized = true;
	}

	// Record all the commands we need to render the scene into the command list.
	PopulateCommandList();

	// Execute the command list.
	ID3D12CommandList* ppCommandLists[] = { m_commandList.Get() };
	m_fetchedCommandQueue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

	// Move to the next frame.
	MoveToNextFrame();

	return S_OK;
}

/**
Updates the internal overlay texture. This method is synchronized with the render call, so can safely be called at any time.
*/
void DX12OverlayInstance::UpdateTextureInternal(std::shared_ptr<Fuel::IOverlayUpdate> update)
{
	std::lock_guard<std::mutex> lock(m_render_mutex);

	// Reset the command list and allocators to upload the updated texture.
	ThrowIfFailed(m_textureUploadCommandAllocator->Reset());
	ThrowIfFailed(m_textureUploadCommandList->Reset(m_textureUploadCommandAllocator.Get(), m_pipelineState.Get()));

	// Generate the updated texture.
	auto texture = GenerateTexture(update);

	// Copy data to the intermediate upload heap and then schedule a copy
	// from the upload heap to the texture.
	D3D12_SUBRESOURCE_DATA textureData = {};
	textureData.pData = reinterpret_cast<UINT8*>(texture.data());
	textureData.RowPitch = m_textureWidth * TexturePixelSizeInBytes;
	textureData.SlicePitch = textureData.RowPitch * m_textureHeight;

	UpdateSubresources<1>(m_textureUploadCommandList.Get(), m_texture.Get(), m_textureUploadResource.Get(), 0, 0, 1, &textureData);

	ThrowIfFailed(m_textureUploadCommandList->Close());

	// Optimize this out ? We can probably re-use the fence from the texture creation.
	ID3D12CommandList* ppCommandLists[] = { m_textureUploadCommandList.Get() };
	m_fetchedCommandQueue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

	// Create synchronization objects and wait until assets have been uploaded to the GPU.
	ComPtr<ID3D12Fence> uploadfence;
	ThrowIfFailed(m_d3d12Device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&uploadfence)));

	// Create an event handle to use for frame synchronization.
	HANDLE uploadfenceEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);

	if (uploadfenceEvent == nullptr)
	{
		ThrowIfFailed(HRESULT_FROM_WIN32(GetLastError()));
	}

	// Signal the command queue and wait till it completes.
	m_fetchedCommandQueue->Signal(uploadfence.Get(), 1);
	ThrowIfFailed(uploadfence->SetEventOnCompletion(1, uploadfenceEvent));
	WaitForSingleObjectEx(uploadfenceEvent, INFINITE, FALSE);
}

void DX12OverlayInstance::PopulateCommandList()
{
	// Command list allocators can only be reset when the associated 
	// command lists have finished execution on the GPU; apps should use 
	// fences to determine GPU execution progress.
	ThrowIfFailed(m_commandAllocators[m_frameIndex]->Reset());

	// However, when ExecuteCommandList() is called on a particular command 
	// list, that command list can then be reset at any time and must be before 
	// re-recording.
	ThrowIfFailed(m_commandList->Reset(m_commandAllocators[m_frameIndex].Get(), m_pipelineState.Get()));

	// Set our root signature and PSO.
	m_commandList->SetGraphicsRootSignature(m_rootSignature.Get());
	m_commandList->SetPipelineState(m_pipelineState.Get());

	// Set the descriptor heaps needed. In our case, we just need access to the SRV which holds our texture data.
	ID3D12DescriptorHeap* ppHeaps[] = { m_srvHeap.Get() };
	m_commandList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);

	// Set the viewport and scissor rectangle.
	m_commandList->RSSetViewports(1, &m_viewport);
	m_commandList->RSSetScissorRects(1, &m_scissorRect);

	// Indicate that the back buffer will be used as a render target, and shouldn't be used for presenting in the meantime.
	m_commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(m_renderTargets[m_frameIndex].Get(), D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET));

	// Set the render target we will be using.
	CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(m_rtvHeap->GetCPUDescriptorHandleForHeapStart(), m_frameIndex, m_rtvDescriptorSize);
	m_commandList->OMSetRenderTargets(1, &rtvHandle, FALSE, nullptr);

	// Set the vertices we will be drawing, and what kind of geometry they represent.
	m_commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	m_commandList->IASetVertexBuffers(0, 1, &m_vertexBufferView);

	// Draw our geometry, specifying an SRV handle to our texture.
	CD3DX12_GPU_DESCRIPTOR_HANDLE srvHandle(m_srvHeap->GetGPUDescriptorHandleForHeapStart());
	m_commandList->SetGraphicsRootDescriptorTable(0, srvHandle);
	m_commandList->DrawInstanced(6, 1, 0, 0);

	// Transition back into a PRESENT state so it can be presented and close the command list.
	m_commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(m_renderTargets[m_frameIndex].Get(), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT));
	ThrowIfFailed(m_commandList->Close());
}

std::vector<UINT8> DX12OverlayInstance::GenerateTexture(std::shared_ptr<Fuel::IOverlayUpdate> update)
{
	const UINT rowPitch = m_textureWidth *TexturePixelSizeInBytes;
	const UINT textureSize = rowPitch * m_textureHeight;

	std::vector<UINT8> data(textureSize);
	UINT8* pData = &data[0];

	if (update == nullptr) {
		for (UINT n = 0; n < textureSize; n += TexturePixelSizeInBytes)
		{
			pData[n] = 0x00;		// R
			pData[n + 1] = 0x00;	// G
			pData[n + 2] = 0x00;	// B
			pData[n + 3] = 0x00;	// A
		}
	}
	else {
		if (m_textureData.size() == 0) {
			// No previous texture data, fill in with empty pixels.
			for (UINT n = 0; n < textureSize; n += TexturePixelSizeInBytes)
			{
				pData[n] = 0x00;		// R
				pData[n + 1] = 0x00;	// G
				pData[n + 2] = 0x00;	// B
				pData[n + 3] = 0x00;	// A
			}
		}
		else {
			// There is previous data. Copy it over.
			for (UINT n = 0; n < textureSize; n++)
			{
				pData[n] = m_textureData[n];
			}
		}	

		for (int i = 0; i < (int)update->GetDirtyRectCount(); i++)
		{
			// Find the location in the buffer where we need to update.
			const OverlayDirtyRect& dirtyRect = update->GetDirtyRect(i);
			int xOffset = update->getX() + dirtyRect.X;
			int yOffset = update->getY() + dirtyRect.Y;
			int overlayRowSize = m_textureWidth * TexturePixelSizeInBytes;
			int rectangleRowSize = dirtyRect.Width * TexturePixelSizeInBytes;
			int leftOffset = xOffset*TexturePixelSizeInBytes;

			int initialOffset = (yOffset*overlayRowSize + leftOffset);
			int dirtyRectRow = 0;

			for (int j = 0; j < dirtyRect.TextureSize; j += TexturePixelSizeInBytes)
			{
				float f_y = (float)j / (float)TexturePixelSizeInBytes;
				int pixelIndex = (int)f_y;
				int adjustedIndex = j - (dirtyRectRow*rectangleRowSize);

				// Swap the order here a little as we're sent the data in BGRA, when we need RGBA.
				pData[initialOffset + adjustedIndex] = dirtyRect.Texture[j + 2];
				pData[initialOffset + adjustedIndex + 1] = dirtyRect.Texture[j + 1];
				pData[initialOffset + adjustedIndex + 2] = dirtyRect.Texture[j];
				pData[initialOffset + adjustedIndex + 3] = dirtyRect.Texture[j + 3];

				if (j != 0 && ((pixelIndex  % dirtyRect.Width) == 0)) {
					initialOffset += overlayRowSize;
					dirtyRectRow++;
				}
			}
		}
	}

	m_textureData = data;

	return data;
}

void DX12OverlayInstance::MoveToNextFrame()
{
	// Here we basically want to synchronize properly and wait until it is safe to move to the next frame.
	// Schedule a Signal command in the queue.
	const UINT64 currentFenceValue = m_fenceValues[m_frameIndex];
	ThrowIfFailed(m_fetchedCommandQueue->Signal(m_fence.Get(), currentFenceValue));

	// Update the frame index.
	m_frameIndex = (m_frameIndex + 1) % m_frameCount;

	// If the next frame is not ready to be rendered yet, wait until it is ready.
	if (m_fence->GetCompletedValue() < m_fenceValues[m_frameIndex])
	{
		ThrowIfFailed(m_fence->SetEventOnCompletion(m_fenceValues[m_frameIndex], m_fenceEvent));
		WaitForSingleObjectEx(m_fenceEvent, INFINITE, FALSE);
	}

	// Set the fence value for the next frame.
	m_fenceValues[m_frameIndex] = currentFenceValue + 1;
}

void DX12OverlayInstance::CreateFences()
{
	// A fence is a DX12 synchronization object. A command queue can "Signal" a fence with a value, and the fence can then dispatch an event once that value is sent to it.
	// This allows us to add a signal in the command queue after we place all our commands in it to know that a "frame" has completed.
	ThrowIfFailed(m_d3d12Device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(m_fence.GetAddressOf())));
}

/**
Creates the texture resource that the overlay will use to draw CEF content on. This method is NOT synchronized 
with the rendering loop, so it should only be called in the context of a render cycle, when the rendering lock is acquired.
*/
void DX12OverlayInstance::CreateTexture()
{
	// Create copy queue resources (maybe we should make these member and re-use them ?)
	ThrowIfFailed(m_d3d12Device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&m_textureUploadCommandAllocator)));
	ThrowIfFailed(m_d3d12Device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, m_textureUploadCommandAllocator.Get(), nullptr, IID_PPV_ARGS(&m_textureUploadCommandList)));

	CD3DX12_RESOURCE_DESC textureDesc = CD3DX12_RESOURCE_DESC::Tex2D(DXGI_FORMAT_R8G8B8A8_UNORM, m_textureWidth, m_textureHeight, 1, 1);

	ThrowIfFailed(m_d3d12Device->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&textureDesc,
		D3D12_RESOURCE_STATE_COMMON,
		nullptr,
		IID_PPV_ARGS(&m_texture)));

	CD3DX12_CPU_DESCRIPTOR_HANDLE cpuHandle(m_srvHeap->GetCPUDescriptorHandleForHeapStart());

	// Create the upload buffer 
	const UINT64 uploadBufferSize = GetRequiredIntermediateSize(m_texture.Get(), 0, 1) + D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT;
	ThrowIfFailed(m_d3d12Device->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(uploadBufferSize),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&m_textureUploadResource)));

	auto texture = GenerateTexture(nullptr);

	// Copy data to the intermediate upload heap and then schedule a copy
	// from the upload heap to the texture.
	D3D12_SUBRESOURCE_DATA textureData = {};
	textureData.pData = reinterpret_cast<UINT8*>(texture.data());
	textureData.RowPitch = m_textureWidth * TexturePixelSizeInBytes;
	textureData.SlicePitch = textureData.RowPitch * m_textureHeight;

	UpdateSubresources<1>(m_textureUploadCommandList.Get(), m_texture.Get(), m_textureUploadResource.Get(), 0, 0, 1, &textureData);
	NAME_D3D12_OBJECT(m_texture);

	// Describe and create a SRV for the texture.
	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Format = textureDesc.Format;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = textureDesc.MipLevels;

	m_d3d12Device->CreateShaderResourceView(m_texture.Get(), &srvDesc, cpuHandle);
	cpuHandle.Offset(m_srvDescriptorSize);

	ThrowIfFailed(m_textureUploadCommandList->Close());

	ID3D12CommandList* ppCommandLists[] = { m_textureUploadCommandList.Get() };
	m_fetchedCommandQueue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

	// Create synchronization objects and wait until assets have been uploaded to the GPU.
	ComPtr<ID3D12Fence> uploadfence;
	ThrowIfFailed(m_d3d12Device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&uploadfence)));

	// Create an event handle to use for frame synchronization.
	HANDLE uploadfenceEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);
	if (uploadfenceEvent == nullptr)
	{
		ThrowIfFailed(HRESULT_FROM_WIN32(GetLastError()));
	}

	m_fetchedCommandQueue->Signal(uploadfence.Get(), 1);
	ThrowIfFailed(uploadfence->SetEventOnCompletion(1, uploadfenceEvent));
	WaitForSingleObjectEx(uploadfenceEvent, INFINITE, FALSE);
}

void DX12OverlayInstance::CreateCommandLists()
{
	// Here we create our command list that we use for rendering. Commands are sent to this queue to perform various GPU operations. The commands are generated
	// through an allocator, which manages them in memory. An allocator is mapped to a command list until the list is closed (completed). Once the command list is closed,
	// the allocator can then be reset and re-used. Here we create the queue with the frame_index's allocator, but it isn't of much importance as it get closed immediately
	// and reset every time we render.
	ThrowIfFailed(m_d3d12Device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, m_commandAllocators[m_frameIndex].Get(), m_pipelineState.Get(), IID_PPV_ARGS(&m_commandList)));
	NAME_D3D12_OBJECT(m_commandList);
	m_commandList->Close(); // Need to close this as its unused until the first frame. The allocators won't reset if the associated command list isn't closed.
}

void DX12OverlayInstance::CreateDescriptorHeaps()
{
	// Describe and create a render target view (RTV) descriptor heap. These are our view onto the swapchain backbuffers on the GPU.
	D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc = {};
	rtvHeapDesc.NumDescriptors = m_frameCount;
	rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	ThrowIfFailed(m_d3d12Device->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&m_rtvHeap)));

	// Describe and create a constant buffer view (CBV) descriptor heap. This is to create a shader view on the GPU which will be able to access the texture data
	// we upload to it.
	D3D12_DESCRIPTOR_HEAP_DESC cbvHeapDesc = {};
	cbvHeapDesc.NumDescriptors = 1;
	cbvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	cbvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	ThrowIfFailed(m_d3d12Device->CreateDescriptorHeap(&cbvHeapDesc, IID_PPV_ARGS(&m_srvHeap)));

	m_rtvDescriptorSize = m_d3d12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	m_srvDescriptorSize = m_d3d12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
}

void DX12OverlayInstance::CreateFrameResources(IDXGISwapChain* pSwapChain)
{
	// Here we create our RTVs for each of the frames in the swap chain, as well as command allocators for each frame.
	CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(m_rtvHeap->GetCPUDescriptorHandleForHeapStart());

	// Create RTVs for each buffer in the swap chain.
	for (UINT n = 0; n < m_frameCount; n++)
	{
		ComPtr<ID3D12Resource> renderTarget;
		ComPtr<ID3D12CommandAllocator> commandAllocator;
		ThrowIfFailed(pSwapChain->GetBuffer(n, IID_PPV_ARGS(&renderTarget)));
		m_d3d12Device->CreateRenderTargetView(renderTarget.Get(), nullptr, rtvHandle);
		m_renderTargets.push_back(renderTarget);

		rtvHandle.Offset(1, m_rtvDescriptorSize);

		ThrowIfFailed(m_d3d12Device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&commandAllocator)));
		m_commandAllocators.push_back(commandAllocator);
	}
}

void DX12OverlayInstance::CreateRootSignature()
{
	// Here we create our root signature, which outlines how we'll be using the GPU.
	CD3DX12_DESCRIPTOR_RANGE1 ranges[1];
	CD3DX12_ROOT_PARAMETER1 rootParameters[1];

	// Say we will only have one descriptor in our range, namely the view to the shader resource (the texture resource we create later is not sent to the GPU pipeline)
	ranges[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0, 0, D3D12_DESCRIPTOR_RANGE_FLAG_DATA_STATIC);
	// Describe what the descriptor will be.
	rootParameters[0].InitAsDescriptorTable(1, &ranges[0], D3D12_SHADER_VISIBILITY_PIXEL);

	// Allow input layout and deny uneccessary access to certain pipeline stages.
	D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
		D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_VERTEX_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS;

	// The description of our texture sampler. Here we're using basic mip mapping.
	CD3DX12_STATIC_SAMPLER_DESC samplerDesc(0, D3D12_FILTER_MIN_MAG_MIP_LINEAR);

	// Create the root signature.
	CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDesc;
	rootSignatureDesc.Init_1_1(_countof(rootParameters), rootParameters, 1, &samplerDesc, rootSignatureFlags);

	ComPtr<ID3DBlob> signature;
	ComPtr<ID3DBlob> error;
	ThrowIfFailed(D3DX12SerializeVersionedRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &signature, &error));
	ThrowIfFailed(m_d3d12Device->CreateRootSignature(0, signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(&m_rootSignature)));
	NAME_D3D12_OBJECT(m_rootSignature);
}

void DX12OverlayInstance::CreatePSO()
{
	// Compile the shaders used in the overlay.
	ComPtr<ID3DBlob> vertexShader;
	ComPtr<ID3DBlob> pixelShader;

	ThrowIfFailed(D3DCompileFromFile(GetAssetFullPath(L"fuel/shaders.hlsl").c_str(), nullptr, nullptr, "VSOverlay", "vs_5_0", 0, 0, &vertexShader, nullptr));
	ThrowIfFailed(D3DCompileFromFile(GetAssetFullPath(L"fuel/shaders.hlsl").c_str(), nullptr, nullptr, "PSOverlay", "ps_5_0", 0, 0, &pixelShader, nullptr));

	// Define the vertex input layout. This also outlines the content of what will be provided to the shaders.
	D3D12_INPUT_ELEMENT_DESC inputElementDescs[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
	};

	// Describe and create the graphics pipeline state object (PSO).

	// First we create a blend description that will enable transparency rendering. By default transparency is not handled (for the obvious computational benefit)
	CD3DX12_BLEND_DESC blendDesc(D3D12_DEFAULT);
	blendDesc.RenderTarget[0].BlendEnable = true;
	blendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	blendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

	// Describe the PSO. It doesn't need much more theen the blend and shader desscriptions we just created.
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	psoDesc.InputLayout = { inputElementDescs, _countof(inputElementDescs) };
	psoDesc.pRootSignature = m_rootSignature.Get();
	psoDesc.VS = CD3DX12_SHADER_BYTECODE(vertexShader.Get());
	psoDesc.PS = CD3DX12_SHADER_BYTECODE(pixelShader.Get());
	psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	psoDesc.BlendState = blendDesc;
	psoDesc.DepthStencilState.DepthEnable = FALSE;
	psoDesc.DepthStencilState.StencilEnable = FALSE;
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.SampleDesc.Count = 1;

	ThrowIfFailed(m_d3d12Device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_pipelineState)));
	NAME_D3D12_OBJECT(m_pipelineState);
}

void DX12OverlayInstance::CreateMeshBuffers()
{
	// Note: ComPtr's are CPU objects but this resource needs to stay in scope until
	// the command list that references it has finished executing on the GPU.
	// We will flush the GPU at the end of this method to ensure the resource is not
	// prematurely destroyed.
	ComPtr<ID3D12CommandAllocator> uploadCommandAllocator;
	ComPtr<ID3D12GraphicsCommandList> uploadCommandList;

	ComPtr<ID3D12Resource> vertexBufferUpload;

	// Create the allocator and command list for the upload.	
	ThrowIfFailed(m_d3d12Device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT,
		IID_PPV_ARGS(&uploadCommandAllocator)));
	ThrowIfFailed(m_d3d12Device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, uploadCommandAllocator.Get(), m_pipelineState.Get(), IID_PPV_ARGS(&uploadCommandList)));

	// Create the vertex buffer.
	{
		// Create a quad for the browser texture.
		float val = 1.0f;
		Vertex vertices[6] = {
			// Upper Left
			{ { -val, val, 0 },{ 0, 0 } },
			// Upper Right
			{ { val, val, 0 },{ 1, 0 } },
			// Bottom right
			{ { val, -val, 0 },{ 1, 1 } },
			// Bottom right
			{ { val, -val, 0 },{ 1, 1 } },
			// Bottom left
			{ { -val, -val, 0 },{ 0, 1 } },
			// Upper Left
			{ { -val, val, 0 },{ 0, 0 } }
		};

		const UINT vertexBufferSize = sizeof(vertices);

		// Create the vertex buffers.
		ThrowIfFailed(m_d3d12Device->CreateCommittedResource(
			&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
			D3D12_HEAP_FLAG_NONE,
			&CD3DX12_RESOURCE_DESC::Buffer(vertexBufferSize),
			D3D12_RESOURCE_STATE_COPY_DEST,
			nullptr,
			IID_PPV_ARGS(&m_vertexBuffer)));
		NAME_D3D12_OBJECT(m_vertexBuffer);

		ThrowIfFailed(m_d3d12Device->CreateCommittedResource(
			&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
			D3D12_HEAP_FLAG_NONE,
			&CD3DX12_RESOURCE_DESC::Buffer(vertexBufferSize),
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			IID_PPV_ARGS(&vertexBufferUpload)));

		// Copy vertex data to the intermediate upload heap and then schedule a copy 
		// from the upload heap to the vertex buffer.
		D3D12_SUBRESOURCE_DATA vertexData = {};
		vertexData.pData = reinterpret_cast<UINT8*>(vertices);
		vertexData.RowPitch = vertexBufferSize;
		vertexData.SlicePitch = vertexData.RowPitch;

		UpdateSubresources<1>(uploadCommandList.Get(), m_vertexBuffer.Get(), vertexBufferUpload.Get(), 0, 0, 1, &vertexData);
		uploadCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(m_vertexBuffer.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER));

		// Initialize the vertex buffer view.
		m_vertexBufferView.BufferLocation = m_vertexBuffer->GetGPUVirtualAddress();
		m_vertexBufferView.StrideInBytes = sizeof(Vertex);
		m_vertexBufferView.SizeInBytes = sizeof(vertices);
	}

	// Close the command list and execute it to begin the vertex buffer copy into
	// the default heap.
	ThrowIfFailed(uploadCommandList->Close());
	ID3D12CommandList* ppCommandLists[] = { uploadCommandList.Get() };
	m_fetchedCommandQueue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

	// Create and upload fence for synchronization.
	ComPtr<ID3D12Fence> uploadfence;
	ThrowIfFailed(m_d3d12Device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&uploadfence)));

	// Wait for the upload to complete.
	HANDLE uploadfenceEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);
	if (uploadfenceEvent == nullptr)
	{
		ThrowIfFailed(HRESULT_FROM_WIN32(GetLastError()));
	}

	m_fetchedCommandQueue->Signal(uploadfence.Get(), 1);
	ThrowIfFailed(uploadfence->SetEventOnCompletion(1, uploadfenceEvent));
	WaitForSingleObjectEx(uploadfenceEvent, INFINITE, FALSE);
}

HRESULT DX12OverlayInstance::Reinitialize()
{
	// Don't think anything is needed here for now; this is used in resize.
	return S_OK;
}

void DX12OverlayInstance::Release()
{
	// We maybe (?) need to do something here to release the COM interfaces we have.
	// I don't know what happens when we query interfaces, if the ComPtr is reset or not...
	// Also only gets called during resize.
}

DX12OverlayInstance::~DX12OverlayInstance()
{
	// Wait for the GPU prior to destroying everything. If the objects are destroyed while the GPU is still using them, you will find yourself with an unexpected,
	// and very difficult to locate, crash.
	WaitForGpu();

	m_d3d12Device.Detach();

	for (int i = 0; i < m_frameCount; i++) {
		m_renderTargets[i].Detach();
	}
	m_renderTargets.clear();

	for (int i = 0; i < m_frameCount; i++) {
		m_commandAllocators[i].Detach();
	}
	m_commandAllocators.clear();
	m_textureUploadCommandAllocator.Detach();

	m_rootSignature.Detach();
	m_pipelineState.Detach();

	m_commandList.Detach();
	m_textureUploadCommandList.Detach();

	m_rtvHeap.Detach();
	m_srvHeap.Detach();

	m_rtvDescriptorSize = 0;
	m_srvDescriptorSize = 0;

	m_vertexBuffer.Detach();
	m_texture.Detach();
	m_textureUploadResource.Detach();

	m_fetchedCommandQueue.Detach();

	m_fence.Detach();

	if (m_frameCount > 0) {
		delete m_fenceValues;
	}

	CloseHandle(m_fenceEvent);

	m_frameCount = 0;
}

DX12OverlayInstance::DX12OverlayInstance(int instanceId, const RECT &location, int zIndex, DX12LibData libData) :
	IOverlayInstance(instanceId, location, zIndex),
	m_frameIndex(0),
	m_frameCount(0),
	m_viewport(),
	m_scissorRect(),
	m_initialized(false),
	m_updated(false),
	m_libData(libData)
{
	WCHAR assetsPath[512];
	GetAssetsPath(assetsPath, _countof(assetsPath));
	m_assetsPath = assetsPath;
}