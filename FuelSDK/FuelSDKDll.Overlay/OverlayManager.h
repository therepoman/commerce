#pragma once

#include <Windows.h>
#include <memory>
#include <functional>
#include <list>
#include <map>
#include <mutex>
#include "CommonOverlayTypes.h"
#include "IOverlayInstance.h"
#include "IOverlayFactory.h"
#include "IOverlayUpdate.h"

namespace Fuel {
	class IOverlayFactory;

	class OverlayDesc
	{
	public:
		int instanceId;
		RECT location;
	};

	class OverlayManager
	{
	public:
		OverlayManager(const std::shared_ptr<IOverlayFactory> &overlayFactory);
		~OverlayManager();
		void SetResizeCallback(RESIZE_CALLBACK callback);
		void SetEnableInputHooksCallback(ENABLE_INPUT_HOOKS_CALLBACK callback);
		void SetDisableInputHooksCallback(DISABLE_INPUT_HOOKS_CALLBACK callback);
		void SetOverlayFailureCallback(OVERLAY_FAILURE_CALLBACK callback);
		void SetHWnd(HWND hWnd);
		void SetBufferSize(BufferSize &bufferSize);

		// Show a new overlay.
		void Show(int instanceId, RECT rect, int zIndex);
		// Hide an overlay.
		void Hide(int instanceId);
		void HideAll();

		void UpdateTexture(std::shared_ptr<Fuel::IOverlayUpdate> update);
		void UpdateCursor(int type);

		// Release rendering resources, but keep the overlays.
		void Release();
		// Reinitialize the overlays' rendering resources.
		void Reinitialize();

		void Render(INSTANCE_HANDLER handler);
		void Resize(BufferSize &bufferSize);

		bool GetOverlayAtLocation(int x, int y, OverlayDesc &overlayDesc);
		bool GetOverlay(int instanceId, OverlayDesc &overlayDesc);
		bool GetFirstOverlay(OverlayDesc &overlayDesc);
		bool GetBufferSize(BufferSize &bufferSize);
	private:
		void UpdateRenderOrder();
		static bool CompareInstanceZIndex(const IOverlayInstance* first, const IOverlayInstance* second);

		RESIZE_CALLBACK m_resizeCallback;
		ENABLE_INPUT_HOOKS_CALLBACK m_enableInputHooksCallback;
		DISABLE_INPUT_HOOKS_CALLBACK m_disableInputHooksCallback;
		OVERLAY_FAILURE_CALLBACK m_overlayFailureCallback;
		std::shared_ptr<IOverlayFactory> m_overlayFactory;
		std::map<int, std::unique_ptr<IOverlayInstance>> m_instances;
		std::list<IOverlayInstance*> m_instanceRenderOrder;
		std::mutex m_instances_mutex;
		bool m_inputHooksEnabled = false;
		HWND m_hWnd = 0;
		BufferSize m_bufferSize;
	};
}
