#pragma once

#include <cstdint>

namespace Fuel
{
	struct OverlayDirtyRect
	{
		uint32_t X;
		uint32_t Y;
		uint32_t Width;
		uint32_t Height;
		uint32_t TextureSize;
		uint8_t* Texture;
	};

	class IOverlayUpdate
	{
	public:
		virtual bool isValid() const = 0;
		virtual uint32_t getInstanceId() const = 0;
		virtual uint32_t getX() const = 0;
		virtual uint32_t getY() const = 0;
		virtual uint32_t getWidth() const = 0;
		virtual uint32_t getHeight() const = 0;
		uint32_t getTextureSize() const { return this->getWidth() * this->getHeight() * 4; }

		virtual uint32_t GetDirtyRectCount() const = 0;
		virtual const OverlayDirtyRect& GetDirtyRect(int index) const = 0;
	};
}