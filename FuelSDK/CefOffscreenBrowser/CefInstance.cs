﻿using CefSharp;
using CefSharp.OffScreen;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CefOffscreenBrowser
{
    public class CefInstance
    {
        private ChromiumWebBrowser m_browser;
        private string m_url;
        private int m_overlayWidth;
        private int m_overlayHeight;
        public int overlayPositionX { get; set; }
        public int overlayPositionY { get; set; }

        public delegate void NewScreenDataCallback(byte[] bytes, int x, int y, int width, int height);

        public NewScreenDataCallback OnNewScreenData;

        public CefInstance(string name, object jsObject)
        {
            OnNewScreenData = delegate { };
            m_browser = new ChromiumWebBrowser();
            m_overlayWidth = 640;
            m_overlayHeight = 480;
            m_browser.Size = new System.Drawing.Size(m_overlayWidth, m_overlayHeight);
            m_browser.NewScreenshot += Browser_NewScreenshot;
            m_browser.BrowserInitialized += M_browser_BrowserInitialized;
            m_browser.RegisterJsObject(name, jsObject);
            m_browser.ConsoleMessage += (sender, args) =>
                {
                    Debug.WriteLine(string.Format("CefInstance {0}({1}): {2}", args.Source, args.Line, args.Message));
                };
        }

        public CefInstance(string url, string name, object jsObject)
            : this(name, jsObject)
        {
            m_url = url;
        }

        public void Destroy()
        {
            m_browser.Dispose();
        }

        private void M_browser_BrowserInitialized(object sender, EventArgs e)
        {
            if (m_url != default(string))
            {
                m_browser.Load(m_url);
            }
            
        }

        private void Browser_NewScreenshot(object sender, EventArgs e)
        {
            if (!m_browser.IsLoading)
            {
                var image = m_browser.ScreenshotOrNull();
                if (image != null)
                {
                    var imagesize = image.Size;

                    var imagerect = new System.Drawing.Rectangle(0, 0, imagesize.Width, imagesize.Height);

                    var bits = image.LockBits(imagerect, System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                    var byteCount = bits.Stride * imagesize.Height;
                    var bytes = new byte[byteCount];

                    Marshal.Copy(bits.Scan0, bytes, 0, byteCount);

                    image.UnlockBits(bits);
                    image.Dispose();

                    OnNewScreenData(bytes, overlayPositionX, overlayPositionY, imagesize.Width, imagesize.Height);
                }
            }
        }

        public void AddScreenDataUpdateCallback(NewScreenDataCallback callback)
        {
            OnNewScreenData += callback;
        }

        // Send keyboard events to CEF
        public void SendKey(KeyEventType type, int keyCode)
        {
            if (m_browser.IsBrowserInitialized)
            {
                m_browser.GetBrowser().GetHost().SendFocusEvent(true);

                m_browser.GetBrowser().GetHost().SendKeyEvent(new KeyEvent
                    {
                        Type = type,
                        IsSystemKey = false,
                        Modifiers = CefEventFlags.None,
                        WindowsKeyCode = keyCode,
                        NativeKeyCode = keyCode
                    });
            }
        }

        // Send mouse click events to CEF
        public void SendMouseEvent(int x, int y, MouseButtonType type, bool mouseUp, int clickCount)
        {
            if (m_browser.IsBrowserInitialized)
            {
                m_browser.GetBrowser().GetHost().SendMouseClickEvent(x, y, type, mouseUp, clickCount, CefEventFlags.None);
            }
        }

        // Send mouse move events to CEF
        public void SendMouseMoveEvent(int x, int y, bool leaving, CefEventFlags flags)
        {
            if (m_browser.IsBrowserInitialized)
            {
                m_browser.GetBrowser().GetHost().SendMouseMoveEvent(x, y, leaving, flags);
            }
        }

        // Send mouse wheel events to CEF
        public void SendMouseWheelEvent(int x, int y, int deltaX, int deltaY, CefEventFlags flags)
        {
            if (m_browser.IsBrowserInitialized)
            {
                m_browser.GetBrowser().GetHost().SendMouseWheelEvent(x, y, deltaX, deltaY, flags);
            }
        }

        public void SetWindowSize(int w, int h)
        {
            m_overlayWidth = (int)(w * 0.8);
            m_overlayHeight = (int)(h * 0.8);
            overlayPositionX = (w - overlayPositionX) / 2;
            overlayPositionY = (h - overlayPositionY) / 2;
            m_browser.Size = new System.Drawing.Size(m_overlayWidth, m_overlayHeight);
        }

        // Navigate to m_url, OnBeforeBrowse will be called before navigation is complete.
        public void Navigate(string url)
        {
            if (m_browser.IsBrowserInitialized)
            {
                m_browser.Load(url);
            }
            else
            {
                this.m_url = url;
            }
        }
    }
}
