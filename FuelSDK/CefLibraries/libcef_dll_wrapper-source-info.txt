libcef_dll_wrapper.lib was compiled from cef website, 3.2704.1434.gec3e9ed for 32bit windows.
https://cefbuilds.com/

This was done by running cmake (BSD 3-clause https://cmake.org) on the root directory. Look at CMakeLists.txt for cmake compile options and instructions.  For example:
cd to the directory where cef was downloaded to.
mkdir build
cd build
"C:\Program Files\CMake\bin\cmake.exe" -G "Visual Studio 12" ..

Open downloaded_cef_directory\build\cef.sln or build\libcef_dll_wrapper\libcef_dll_wrapper.vcxproj.

Open the libcef_dll_wrapper project properties and change Configuration Properties > C/C++ >  Code Generation > Runtime Library.  Use /MDd for Debug and /MD for Release so that it matces the settings in the FuelPump.Browser project.
Compile the libcef_dll_wrapper project for both debug and release.

Copy downloaded_cef_directory\build\libcef_dll_wrapper\Debug\libcef_dll_wrapper.lib to RespawnSDK\CefLibraries\Debug.
Copy downloaded_cef_directory\build\libcef_dll_wrapper\Release\libcef_dll_wrapper.lib to RespawnSDK\CefLibraries\Release.

Copy downloaded_cef_directory\Debug\libcef.lib to RespawnSDK\CefLibraries\Debug.
Copy downloaded_cef_directory\Release\libcef.lib to RespawnSDK\CefLibraries\Release.

Delete RespawnSDK\CefLibraries\include
Copy downloaded_cef_directory\include to RespawnSDK\CefLibraries\include

Build the RespawnSDK solution.  It might fail if the CEF API has changed.

