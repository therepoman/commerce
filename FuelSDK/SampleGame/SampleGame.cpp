﻿#include "stdafx.h"
#include "SampleGame.h"
#include "TwitchToken.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	SampleGame game(hInstance, nCmdShow);
	return game.run();
}

SampleGame::SampleGame(HINSTANCE hInstance, int nCmdShow) : m_hInstance(hInstance), m_nCmdShow(nCmdShow),
    m_renderer(nullptr, [](IGameRenderer*){throw std::runtime_error("A custom deleter must be provided"); })
{
	static SampleGame *instance = this;

	ZeroMemory(m_keyboardState, sizeof(m_keyboardState));
	ZeroMemory(&m_mouseState, sizeof(m_mouseState));

	WNDCLASSEX wc;
	ZeroMemory(&wc, sizeof(WNDCLASSEX));
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = [](HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) -> LRESULT { return instance->WindowProc(hWnd, message, wParam, lParam); };
	wc.hInstance = hInstance;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wc.lpszClassName = L"SampleGame";
	RegisterClassEx(&wc);

	int windowWidth = 800;
	int windowHeight = 600;

	m_hWnd = CreateWindowEx(NULL, L"SampleGame", L"SampleGame", WS_OVERLAPPEDWINDOW, 200, 200, windowWidth, windowHeight, NULL, NULL, hInstance, NULL);
	ShowWindow(m_hWnd, nCmdShow);

	initRenderer();
	startSdk();
	updateButtonText();
}

SampleGame::~SampleGame()
{
	// Shutdown the SDK first so the purchase thread will finish.
	m_sdk.reset();

	if (m_purchaseThread.joinable())
	{
		m_purchaseThread.join();
	}

	releaseRenderer();
}

int SampleGame::run()
{
	MSG msg = { 0 };
	while (WM_QUIT != msg.message)
	{
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			if (m_needsResize)
			{
				m_needsResize = false;
				resize();
			}
			if (m_lastDisplayedFps != m_fpsTimer.GetMeasuredFps())
			{
				m_lastDisplayedFps = m_fpsTimer.GetMeasuredFps();
				updateButtonText();
			}
			if (m_inputType == DirectInput)
			{
				handleDirectInput();
			}
			render();
			m_fpsTimer.Wait();
		}
	}
	return (int) msg.wParam;
}

void SampleGame::initRenderer()
{
	std::unique_ptr<IGameRenderer, void(*)(IGameRenderer*)> renderer(new DX11GameRenderer(m_hWnd), DX11GameRenderer::Deleter);
	m_renderer = std::move(renderer);
	m_renderer->init();
	int y = 10;
	m_getProductDataButton = m_renderer->createButton(L"Get Product Data", 10, y, 200, 50, 20);
	y += 60;
	m_purchaseButton = m_renderer->createButton(L"Purchase", 10, y, 200, 50, 20);
	y += 60;
	m_getPurchaseUpdatesButton = m_renderer->createButton(L"Purchase Updates", 10, y, 200, 50, 20);
	y += 60;
	m_notifyFulfillmentButton = m_renderer->createButton(L"Notify Fulfillment", 10, y, 200, 50, 20);
	y += 60;
	m_sdkButton = m_renderer->createButton(L"SDK", 10, y, 200, 50, 20);
	y += 60;
	m_fpsLimitButton = m_renderer->createButton(L"FPS", 10, y, 200, 50, 20);
	y += 60;
	m_inputButton = m_renderer->createButton(L"Input", 10, y, 200, 50, 20);
	y += 60;
	m_resizeButton = m_renderer->createButton(L"Resize", 10, y, 200, 50, 20);

	m_logLabel = m_renderer->createLabel(220, 10, 600, 600, 10);
}

void SampleGame::releaseRenderer()
{
	m_renderer.reset();
}

void SampleGame::handleDirectInput()
{
	std::unique_lock<std::mutex> lock(m_inputMutex, std::defer_lock);
	if (std::try_lock(lock) != -1)
	{
		// The mutex is locked, so the overlay is probably visible.  Ignore input.
		return;
	}

	if (!m_directInput)
	{
		HRESULT result = DirectInput8Create(m_hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8W, (LPVOID*)(&m_directInput), nullptr);
		if (FAILED(result))
		{
			throw std::runtime_error("Failed to initialize DirectInput");
		}
	}

	if (!m_directInputKeyboard)
	{
		HRESULT result = m_directInput->CreateDevice(GUID_SysKeyboard, &m_directInputKeyboard, nullptr);
		if (FAILED(result))
		{
			throw std::runtime_error("Failed to create DirectInput keyboard object");
		}

		result = m_directInputKeyboard->SetCooperativeLevel(m_hWnd, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
		if (FAILED(result))
		{
			throw std::runtime_error("Failed to set keyboard cooperative level");
		}

		result = m_directInputKeyboard->SetDataFormat(&c_dfDIKeyboard);
		if (FAILED(result))
		{
			throw std::runtime_error("Failed to set keyboard data format");
		}

		result = m_directInputKeyboard->Acquire();
		if (result == E_ACCESSDENIED)
		{
			// Window probably lost focus.
			return;
		}
		else if (FAILED(result))
		{
			throw std::runtime_error("Failed to acquire keyboard");
		}
	}

	uint8_t keyboard[256];

	HRESULT result = m_directInputKeyboard->GetDeviceState(sizeof(keyboard), keyboard);
	if (FAILED(result))
	{
		m_directInputKeyboard.Release();

		if (result == HRESULT_FROM_WIN32(ERROR_READ_FAULT))
		{
			// The window lost focus and as a result lost access to the keyboard.
		}
		else if (result == HRESULT_FROM_WIN32(ERROR_INVALID_ACCESS))
		{
			// The window still doesn't have focus.
		}
		else
		{
			throw std::runtime_error("Failed to read keyboard data");
		}
	}
	else
	{
		if (m_keyboardState[DIK_P] == 0 && keyboard[DIK_P] != 0)
		{
			beginPurchase();
		}

		if (m_keyboardState[DIK_D] == 0 && keyboard[DIK_D] != 0)
		{
			getProductData();
		}

		memcpy(m_keyboardState, keyboard, sizeof(m_keyboardState));
	}

	if (!m_directInputMouse)
	{
		HRESULT result = m_directInput->CreateDevice(GUID_SysMouse, &m_directInputMouse, nullptr);
		if (FAILED(result))
		{
			throw std::runtime_error("Failed to create DirectInput mouse object");
		}

		result = m_directInputMouse->SetCooperativeLevel(m_hWnd, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
		if (FAILED(result))
		{
			throw std::runtime_error("Failed to set mouse cooperative level");
		}

		result = m_directInputMouse->SetDataFormat(&c_dfDIMouse);
		if (FAILED(result))
		{
			throw std::runtime_error("Failed to set mouse data format");
		}

		result = m_directInputMouse->Acquire();
		if (result == E_ACCESSDENIED)
		{
			// Window probably lost focus.
			return;
		}
		else if (FAILED(result))
		{
			throw std::runtime_error("Failed to acquire mouse");
		}
	}

	DIMOUSESTATE mouse;
	result = m_directInputMouse->GetDeviceState(sizeof(mouse), &mouse);
	if (FAILED(result))
	{
		m_directInputMouse.Release();

		if (result == HRESULT_FROM_WIN32(ERROR_READ_FAULT))
		{
			// The window lost focus and as a result lost access to the mouse.
		}
		else if (result == HRESULT_FROM_WIN32(ERROR_INVALID_ACCESS))
		{
			// The window still doesn't have focus.
		}
		else
		{
			throw std::runtime_error("Failed to read mouse data");
		}
	}
	else
	{
		POINT location;
		if (!GetCursorPos(&location) || !ScreenToClient(m_hWnd, &location))
		{
			throw std::runtime_error("Failed to get mouse location");
		}

		if (m_mouseState.rgbButtons[0] == 0 && mouse.rgbButtons[0] != 0)
		{
			handleMouseInput(m_hWnd, WM_LBUTTONDOWN, 0, (location.x & 0xffff) | ((location.y & 0xffff) << 16));
		}

		if (m_mouseState.rgbButtons[0] != 0 && mouse.rgbButtons[0] == 0)
		{
			handleMouseInput(m_hWnd, WM_LBUTTONUP, 0, (location.x & 0xffff) | ((location.y & 0xffff) << 16));
		}

		memcpy(&m_mouseState, &mouse, sizeof(m_mouseState));
	}
}

void SampleGame::render()
{
	m_renderer->render();
}

void SampleGame::resize()
{
	switch (m_resizeType)
	{
	case None:
		break;
	case FullRestart:
		releaseRenderer();
		initRenderer();
		break;
	case SwapChain:
		m_renderer->resize();
		break;
	}
}

void SampleGame::updateButtonText()
{
	switch (m_inputType)
	{
	case MessageLoop:
		m_inputButton->setTitle(L"Input: MessageLoop");
		break;
	case DirectInput:
		m_inputButton->setTitle(L"Input: DirectInput");
		break;
	}

	switch (m_resizeType)
	{
	case None:
		m_resizeButton->setTitle(L"Resize: None");
		break;
	case FullRestart:
		m_resizeButton->setTitle(L"Resize: Full Restart");
		break;
	case SwapChain:
		m_resizeButton->setTitle(L"Resize: Swap Chain");
		break;
	}

	if (m_sdk)
	{
		m_sdkButton->setTitle(L"SDK: Running");
	}
	else
	{
		m_sdkButton->setTitle(L"SDK: Stopped");
	}

	if (m_fpsTimer.GetFpsLimit() <= 0)
	{
		m_fpsButtonTitle = L"FPS Limit: Off";
	}
	else
	{
		m_fpsButtonTitle = L"FPS Limit: ";
		m_fpsButtonTitle += std::to_wstring(m_fpsTimer.GetFpsLimit());
	}
	m_fpsButtonTitle += L" (";
	m_fpsButtonTitle += std::to_wstring(m_fpsTimer.GetMeasuredFps());
	m_fpsButtonTitle += L")";
	m_fpsLimitButton->setTitle(m_fpsButtonTitle.c_str());
}

void SampleGame::beginPurchase()
{
	if (!m_sdk)
	{
		return;
	}

	if (m_purchaseThread.joinable())
	{
		m_purchaseThread.join();
	}
	m_purchaseThread = std::thread([this]()
	{
		Fuel::Iap::Model::PurchaseRequest request;
		request.SetSku(TEST_SKU_1);
		std::wstring message(L"Purchase complete: ");

		{
			std::lock_guard<std::mutex> lock(m_inputMutex);
			auto result = m_sdk->Purchase(request);

			if (result.IsSuccess())
			{
				message.append(L"Success\n");
				message.append(L"Receipt ID : ");
				message.append(result.GetResponse().GetReceipt().GetReceiptId());
			}
			else
			{
				message.append(L"Failure\r\n");
				message.append(result.GetError().GetMessageW());
			}

			message.append(L"\r\n\r\n");
		}

		m_logLabel->AddLine(message);
		MessageBox(nullptr, message.c_str(), L"Purchase", MB_OK);
	});
}

void SampleGame::getProductData()
{
	if (!m_sdk)
	{
		return;
	}

	Fuel::Iap::Model::GetProductDataRequest request;
	std::vector<Fuel::Iap::Model::Sku> skus;
	skus.push_back(TEST_SKU_1);
	request.SetSkus(skus);
	auto result = m_sdk->GetProductData(request);

	// Temporary code to verify that the sku was returned.
	std::wstring message;
	if (result.IsSuccess())
	{
		auto products = result.GetResponse().GetProducts();
		auto unavailable = result.GetResponse().GetUnavailableSkus();

		message.append(L"GetProductData: ");
		message.append(std::to_wstring(products.size())).append(L" products and ");
		message.append(std::to_wstring(unavailable.size())).append(L" unavailable.\r\n");

		for (auto product = products.begin(); product != products.end(); ++product)
		{
			message.append(L"  Product\r\n");
			message.append(L"    SKU: ").append(product->first).append(L"\r\n");
			message.append(L"    Title: ").append(product->second.GetTitle()).append(L"\r\n");
			message.append(L"    Description: ").append(product->second.GetDescription()).append(L"\r\n");
			message.append(L"    Type: ").append(std::to_wstring((int) product->second.GetProductType())).append(L"\r\n");
			message.append(L"    Price: ").append(product->second.GetPrice()).append(L"\r\n");
			message.append(L"    Icon: ").append(product->second.GetSmallIconUrl()).append(L"\r\n");
		}

		for (auto sku = unavailable.begin(); sku != unavailable.end(); ++sku)
		{
			message.append(L"  Unavailable SKU: ").append(*sku).append(L"\r\n");
		}
	}
	else
	{
		message.append(L"GetProductDataRequest failed.");
		message.append(result.GetError().GetMessageW());
	}
	message.append(L"\r\n\r\n");

	m_logLabel->AddLine(message);
	MessageBox(nullptr, message.c_str(), L"GetProductData", MB_OK);
}

void SampleGame::getPurchaseUpdates()
{
	Fuel::Iap::Model::SyncPointData syncPoint;
	syncPoint.SetSyncToken(L"");

	Fuel::Iap::Model::GetPurchaseUpdatesRequest request;
	request.SetSyncPoint(syncPoint);
	auto result = m_sdk->GetPurchaseUpdates(request);

	std::wstring message;
	appendResponse(message, result);

	if (result.IsSuccess() && result.GetResponse().GetSyncPoint().GetSyncToken().length() > 0)
	{
		syncPoint.SetSyncToken(result.GetResponse().GetSyncPoint().GetSyncToken());
		syncPoint.SetTimestamp(result.GetResponse().GetSyncPoint().GetTimestamp());
		request.SetSyncPoint(syncPoint);
		appendResponse(message, m_sdk->GetPurchaseUpdates(request));
	}

	MessageBox(nullptr, message.c_str(), L"GetPurchaseUpdates", MB_OK);
	m_logLabel->AddLine(message);
}

void SampleGame::appendResponse(std::wstring &message, IapClient::GetPurchaseUpdatesOutcome &result)
{
	message.append(L"GetPurchaseUpdates ");
	if (result.IsSuccess())
	{
		message.append(L"success.\r\n");
		auto receipts = result.GetResponse().GetReceipts();
		for (auto receipt = receipts.begin(); receipt != receipts.end(); ++receipt)
		{
			message.append(L"  Receipt ").append(receipt->GetReceiptId()).append(L"\r\n");
			message.append(L"    SKU: ").append(receipt->GetSku()).append(L"\r\n");
			message.append(L"    Type: ").append(std::to_wstring((int)receipt->GetProductType())).append(L"\r\n");
			message.append(L"    Purchase Date: ").append(toString(receipt->GetPurchaseDate())).append(L"\r\n");
			message.append(L"    Cancel Date: ").append(toString(receipt->GetCancelDate())).append(L"\r\n");
		}
		auto syncPoint = result.GetResponse().GetSyncPoint();
		message.append(L"  Sync Point").append(L"\r\n");
		message.append(L"    Token: ").append(syncPoint.GetSyncToken()).append(L"\r\n");
		message.append(L"    Time stamp: ").append(toString(syncPoint.GetTimestamp()));
	}
	else
	{
		message.append(L"failed: ");
		message.append(result.GetError().GetMessageW());
	}
	message.append(L"\r\n\r\n");
}

void SampleGame::notifyFulfillment()
{
	Fuel::Iap::Model::SyncPointData syncPoint;
	syncPoint.SetSyncToken(L"");

	Fuel::Iap::Model::GetPurchaseUpdatesRequest updatesRequest;
	updatesRequest.SetSyncPoint(syncPoint);
	auto updatesResult = m_sdk->GetPurchaseUpdates(updatesRequest);

	if (!updatesResult.IsSuccess())
	{
		MessageBox(nullptr, L"Failed", L"NotifyFulfillment", MB_OK);
		m_logLabel->AddLine(L"NotifyFulfillment: Failed to get purchase updates, no receipt to fulfill.\r\n\r\n");
		return;
	}

	auto receipts = updatesResult.GetResponse().GetReceipts();
	if (receipts.empty())
	{
		MessageBox(nullptr, L"Failed", L"NotifyFulfillment", MB_OK);
		m_logLabel->AddLine(L"NotifyFulfillment: getPurcahseUpdates did not return any receipts to fulfill.\r\n\r\n");
		return;
	}

	Fuel::Iap::Model::NotifyFulfillmentRequest request;
	request.SetReceipt(receipts.begin()->GetReceiptId());
	request.SetFulfillmentResult(Fuel::Iap::Model::FulfillmentResult::Fulfilled);
	auto result = m_sdk->NotifyFulfillment(request);
	if (result.IsSuccess())
	{
		std::wstring message;
		message.append(L"NotifyFulfillment: Success\r\nReceipt: ");
		message.append(receipts.begin()->GetReceiptId());
		message.append(L"\r\n\r\n");

		MessageBox(nullptr, message.c_str(), L"NotifyFulfillment", MB_OK);
		m_logLabel->AddLine(message.c_str());
	}
	else
	{
		std::wstring message;
		message.append(L"NotifyFulfillment: Failed\r\nReceipt: ");
		message.append(receipts.begin()->GetReceiptId());
		message.append(L"\r\n");
		message.append(result.GetError().GetMessageW());
		message.append(L"\r\n\r\n");
		MessageBox(nullptr, message.c_str(), L"NotifyFulfillment", MB_OK);
		m_logLabel->AddLine(message.c_str());
	}
}

void SampleGame::startSdk()
{
	auto sdkClientResponse = Fuel::Iap::IapClient::Create(VENDOR_ID, "1.1", Fuel::RendererType::DIRECT_X_11);
	if (sdkClientResponse.IsSuccess())
	{
		m_sdk = sdkClientResponse.GetResponseWithOwnership();
		Fuel::Iap::Model::GetSessionRequest request;
		request.SetClientId(TWITCH_CLIENT_ID);
		auto outcome = m_sdk->GetSession(request);
		if (outcome.IsSuccess()) {
			Fuel::Iap::Model::TwitchUser twitchUser = outcome.GetResponse().GetUser();
			m_sdk->AddCredentials(Fuel::Iap::Model::CreateTwitchCredentials(twitchUser.GetTuid(), twitchUser.GetAccessToken()));
		} else {
			m_sdk->AddCredentials(Fuel::Iap::Model::CreateTwitchCredentials(TWITCH_USER_ID, TWITCH_OAUTH_TOKEN));
		}
	}
	else
	{
		MessageBox(nullptr, sdkClientResponse.GetError().GetMessage().c_str(), L"Error", MB_OK);
	}
}

void SampleGame::handleMouseInput(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (m_getProductDataButton->handleClickMessage(hWnd, message, wParam, lParam))
	{
		getProductData();
	}

	if (m_purchaseButton->handleClickMessage(hWnd, message, wParam, lParam))
	{
		beginPurchase();
	}

	if (m_getPurchaseUpdatesButton->handleClickMessage(hWnd, message, wParam, lParam))
	{
		getPurchaseUpdates();
	}

	if (m_notifyFulfillmentButton->handleClickMessage(hWnd, message, wParam, lParam))
	{
		notifyFulfillment();
	}

	if (m_sdkButton->handleClickMessage(hWnd, message, wParam, lParam))
	{
		if (m_sdk)
		{
			m_sdk.reset();
		}
		else
		{
			startSdk();
		}
		updateButtonText();
	}

	if (m_fpsLimitButton->handleClickMessage(hWnd, message, wParam, lParam))
	{
		switch (m_fpsTimer.GetFpsLimit())
		{
		case 0:
			m_fpsTimer.SetFpsLimit(10);
			break;
		case 10:
			m_fpsTimer.SetFpsLimit(60);
			break;
		default:
			m_fpsTimer.SetFpsLimit(0);
			break;
		}
		updateButtonText();
	}

	if (m_inputButton->handleClickMessage(hWnd, message, wParam, lParam))
	{
		m_inputType = (InputType)((int)m_inputType + 1);
		if (m_inputType == LAST_INPUT_TYPE)
		{
			m_inputType = (InputType)0;
		}
		updateButtonText();
	}

	if (m_resizeButton->handleClickMessage(hWnd, message, wParam, lParam))
	{
		m_resizeType = (WindowResizeType)((int)m_resizeType + 1);
		if (m_resizeType == LAST_WINDOW_RESIZE_TYPE)
		{
			m_resizeType = (WindowResizeType)0;
		}
		updateButtonText();
	}
}

LRESULT SampleGame::WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (m_inputType == MessageLoop)
	{
		handleMouseInput(hWnd, message, wParam, lParam);
	}

	switch (message)
	{
	case WM_SIZE:
		m_needsResize = true;
		break;
	case WM_CHAR:
		if (m_inputType == MessageLoop)
		{
			char keyCode = (char)wParam;

			if (keyCode == 'd')
			{
				getProductData();
			}
			else if (keyCode == 'p')
			{
				beginPurchase();
			}
		}
		break;
	case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}
		break;
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}

std::wstring SampleGame::toUTCString(std::chrono::time_point<std::chrono::system_clock, std::chrono::seconds> timePoint) {
	time_t timeType = std::chrono::system_clock::to_time_t(timePoint);
	std::unique_ptr<tm> timeStruct(new tm);
	gmtime_s(timeStruct.get(), &timeType);
	std::unique_ptr<WCHAR> timeBuffer(new WCHAR[1024]);
	auto timeSize = std::wcsftime(timeBuffer.get(), 1024, L"%c", timeStruct.get());
	std::wstring result;
	result.append(timeBuffer.get(), timeBuffer.get() + timeSize);
	return result;
}

std::wstring SampleGame::toString(std::chrono::time_point<std::chrono::system_clock, std::chrono::seconds> timePoint)
{
	time_t timeType = std::chrono::system_clock::to_time_t(timePoint);
	std::unique_ptr<tm> timeStruct(new tm);
	localtime_s(timeStruct.get(), &timeType);
	std::unique_ptr<WCHAR> timeBuffer(new WCHAR[1024]);
	auto timeSize = std::wcsftime(timeBuffer.get(), 1024, L"%c", timeStruct.get());
	std::wstring result;
	result.append(timeBuffer.get(), timeBuffer.get() + timeSize);
	return result;
}
