#include "stdafx.h"
#include "D2DButton.h"
#include <stdexcept>

#define MOUSE_X(l) (short) LOWORD(l)
#define MOUSE_Y(l) (short) HIWORD(l)

D2DButton::D2DButton(const WCHAR *title, int x, int y, int width, int height, int fontSize, float dpiX, float dpiY)
	: IButton(title, x, y, width, height, fontSize, dpiX, dpiY)
{
	m_layout.left = (float)x;
	m_layout.top = (float)y;
	m_layout.right = (float)x + width;
	m_layout.bottom = (float)y + height;
}

D2DButton::~D2DButton()
{
	release();
}

void D2DButton::init(ID2D1RenderTarget *d2dRenderTarget)
{
	HRESULT hr = d2dRenderTarget->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::LightGray), &m_backgroundBrush);
	if (FAILED(hr)) throw std::runtime_error("Failed to create background brush");

	hr = d2dRenderTarget->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Black), &m_textBrush);
	if (FAILED(hr)) throw std::runtime_error("Failed to create text brush");

	hr = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), reinterpret_cast<IUnknown**>(&m_writeFactory));
	if (FAILED(hr)) throw std::runtime_error("Failed to create write factory");

	hr = m_writeFactory->CreateTextFormat(L"Arial", NULL, DWRITE_FONT_WEIGHT_REGULAR, DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL, m_fontSize, L"en-us", &m_textFormat);

	CComPtr<IDWriteTextLayout> layout;
	hr = m_writeFactory->CreateTextLayout(m_title, (UINT32)wcsnlen_s(m_title, 100), m_textFormat, m_layout.right - m_layout.left, m_layout.bottom - m_layout.top, &layout);
	if (FAILED(hr)) throw std::runtime_error("Failed to create text layout");

	DWRITE_TEXT_METRICS textMetrics;
	layout->GetMetrics(&textMetrics);

	float titleWidth = textMetrics.width;
	float titleHeight = textMetrics.height;

	m_titleLayout.left = m_layout.left + ((m_layout.right - m_layout.left) - titleWidth) / 2.0f;
	m_titleLayout.top = m_layout.top + ((m_layout.bottom - m_layout.top) - titleHeight) / 2.0f;
	m_titleLayout.right = m_layout.right + titleWidth;
	m_titleLayout.bottom = m_titleLayout.top + titleHeight;

	m_titleLayoutMouseDown.left = m_titleLayout.left + 2;
	m_titleLayoutMouseDown.top = m_titleLayout.top + 2;
	m_titleLayoutMouseDown.right = m_titleLayout.right + 2;
	m_titleLayoutMouseDown.bottom = m_titleLayout.bottom + 2;
}

void D2DButton::release()
{
	m_backgroundBrush.Release();
	m_textBrush.Release();
	m_writeFactory.Release();
	m_textFormat.Release();
}

void D2DButton::render(ID2D1RenderTarget *d2dRenderTarget)
{
	if (m_titleChanged)
	{
		m_titleChanged = false;
		release();
		init(d2dRenderTarget);
	}
	d2dRenderTarget->FillRectangle(&m_layout, m_backgroundBrush);
	D2D1_RECT_F *layout = m_mouseDown ? &m_titleLayoutMouseDown : &m_titleLayout;
	d2dRenderTarget->DrawTextW(m_title, (UINT32)wcsnlen_s(m_title, 100), m_textFormat, layout, m_textBrush);
}
