#pragma once

class IButton
{
public:
	IButton(const WCHAR *title, int x, int y, int width, int height, int fontSize, float dpiX, float dpiY);
	~IButton();

	void setTitle(const WCHAR *title);

	bool handleClickMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
protected:
	int m_x, m_y, m_width, m_height;
	float m_dpiX, m_dpiY;
	const WCHAR *m_title;
	int m_fontSize;
	bool m_mouseDown = false;
	bool m_titleChanged = false;
};
