#pragma once

#include <windows.h>

#include <d2d1.h>
#include <d2d1helper.h>
#include <dwrite.h>
#include <memory>
#include <vector>
#include "IGameRenderer.h"
#include "D2DButton.h"
#include "D2DLabel.h"
#include <atlbase.h>

class DX11GameRenderer : public IGameRenderer
{
public:
	DX11GameRenderer(HWND hWnd);
	~DX11GameRenderer();
	static void Deleter(IGameRenderer *p);

	std::shared_ptr<IButton> createButton(const WCHAR *m_title, int x, int y, int width, int height, int fontSize) override;
	std::shared_ptr<ILabel> createLabel(int x, int y, int width, int height, int fontSize) override;

	void init();

	void render();
	void resize();
private:
	void initSwapChain();
	void releaseSwapChain();
	void initRenderTarget();
	void releaseRenderTarget();

	CComPtr<IDXGISwapChain> m_swapChain;
	CComPtr<ID3D11Device> m_device;
	CComPtr<ID3D11DeviceContext> m_deviceContext;
	CComPtr<ID3D11RenderTargetView> m_renderTargetView;

	CComPtr<ID2D1Factory> m_d2dFactory;
	CComPtr<ID2D1RenderTarget> m_d2dRenderTarget;

	std::vector<std::shared_ptr<D2DButton>> m_buttons;
	std::vector<std::shared_ptr<D2DLabel>> m_labels;
	float m_dpiX;
	float m_dpiY;
};
