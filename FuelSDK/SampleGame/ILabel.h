#pragma once
#include <list>
#include <Windows.h>

class ILabel
{
public:
	ILabel(int x, int y, int width, int height, int fontSize, float dpiX, float dpiY);
	virtual ~ILabel();

	void AddLine(std::wstring line);
protected:
	std::wstring& GetDisplayContent();

	int m_x, m_y, m_width, m_height;
	float m_dpiX, m_dpiY;
	std::list<std::wstring> m_lines;
	std::wstring m_displayContent;
	int m_fontSize;
	bool m_mouseDown = false;
	bool m_contentChanged = false;
};
