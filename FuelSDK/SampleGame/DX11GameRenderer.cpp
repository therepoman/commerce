#include "stdafx.h"
#include "DX11GameRenderer.h"
#include <stdexcept>

#include <d3d11.h>
#include <d3d11_1.h>
#include <d3d11_2.h>

DX11GameRenderer::DX11GameRenderer(HWND hWnd) : IGameRenderer(hWnd)
{
}

DX11GameRenderer::~DX11GameRenderer()
{
	releaseRenderTarget();
	releaseSwapChain();
	m_buttons.clear();
}

void DX11GameRenderer::Deleter(IGameRenderer *p)
{
	delete (DX11GameRenderer*)p;
}

void DX11GameRenderer::init()
{
	initSwapChain();
	initRenderTarget();
}

void DX11GameRenderer::initSwapChain()
{
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	ZeroMemory(&swapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	swapChainDesc.BufferCount = 1;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.OutputWindow = m_hWnd;
	swapChainDesc.SampleDesc.Count = 4;
	swapChainDesc.Windowed = TRUE;

	UINT flags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;

	HRESULT hr;
	hr = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, flags, NULL, NULL, D3D11_SDK_VERSION,
		&swapChainDesc, &m_swapChain, &m_device, NULL, &m_deviceContext);
	if (FAILED(hr)) throw std::runtime_error("Failed to create swap chain");
}

void DX11GameRenderer::releaseSwapChain()
{
	m_swapChain.Release();
	m_deviceContext.Release();
	m_device.Release();
}

void DX11GameRenderer::initRenderTarget()
{
	CComPtr<ID3D11Texture2D> backBuffer;
	HRESULT hr = m_swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBuffer);
	if (FAILED(hr)) throw std::runtime_error("Failed to get back buffer");

	hr = m_device->CreateRenderTargetView(backBuffer, NULL, &m_renderTargetView);
	if (FAILED(hr)) throw std::runtime_error("Failed to create render target view");

	m_deviceContext->OMSetRenderTargets(1, &(m_renderTargetView.p), NULL);

	hr = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &(m_d2dFactory.p));
	if (FAILED(hr)) throw std::runtime_error("Failed to create 2d factory");

	m_d2dFactory->GetDesktopDpi(&m_dpiX, &m_dpiY);

	D2D1_RENDER_TARGET_PROPERTIES props =
		D2D1::RenderTargetProperties(
		D2D1_RENDER_TARGET_TYPE_DEFAULT,
		D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED),
		m_dpiX,
		m_dpiY
		);

	CComPtr<IDXGISurface> backBuffer2;
	hr = m_swapChain->GetBuffer(0, IID_PPV_ARGS(&backBuffer2));
	if (FAILED(hr)) throw std::runtime_error("Failed to get back buffer for 2d");

	hr = m_d2dFactory->CreateDxgiSurfaceRenderTarget(backBuffer2, &props, &m_d2dRenderTarget);
	if (FAILED(hr)) throw std::runtime_error("Failed to create 2d render target");

	for (auto it = m_buttons.begin(); it != m_buttons.end(); ++it)
	{
		(*it)->init(m_d2dRenderTarget);
	}

	for (auto it = m_labels.begin(); it != m_labels.end(); ++it)
	{
		(*it)->init(m_d2dRenderTarget);
	}
}

void DX11GameRenderer::releaseRenderTarget()
{
	for (auto it = m_buttons.begin(); it != m_buttons.end(); ++it)
	{
		(*it)->release();
	}

	for (auto it = m_labels.begin(); it != m_labels.end(); ++it)
	{
		(*it)->release();
	}

	m_d2dRenderTarget.Release();
	m_d2dFactory.Release();
	m_renderTargetView.Release();
}


std::shared_ptr<IButton> DX11GameRenderer::createButton(const WCHAR *m_title, int x, int y, int width, int height, int fontSize)
{
	std::shared_ptr<D2DButton> button(new D2DButton(m_title, x, y, width, height, fontSize, m_dpiX, m_dpiY));
	m_buttons.push_back(button);
	button->init(m_d2dRenderTarget);
	return button;
}

std::shared_ptr<ILabel> DX11GameRenderer::createLabel(int x, int y, int width, int height, int fontSize)
{
	std::shared_ptr<D2DLabel> label(new D2DLabel(x, y, width, height, fontSize, m_dpiX, m_dpiY));
	m_labels.push_back(label);
	label->init(m_d2dRenderTarget);
	return label;
}

void DX11GameRenderer::render()
{
	float background[4] = {1.0f, 1.0f, 1.0f, 1.0f};
	m_deviceContext->ClearRenderTargetView(m_renderTargetView, background);
	m_d2dRenderTarget->BeginDraw();

	m_d2dRenderTarget->SetTransform(D2D1::Matrix3x2F::Identity());
	m_d2dRenderTarget->Clear(D2D1::ColorF(D2D1::ColorF::White));

	for (auto it = m_labels.begin(); it != m_labels.end(); ++it)
	{
		(*it)->render(m_d2dRenderTarget);
	}

	for (auto it = m_buttons.begin(); it != m_buttons.end(); ++it)
	{
		(*it)->render(m_d2dRenderTarget);
	}

	m_d2dRenderTarget->EndDraw();
	m_swapChain->Present(0, 0);
}

void DX11GameRenderer::resize()
{
	RECT rc;
	GetClientRect(m_hWnd, &rc);
	UINT width = rc.right - rc.left;
	UINT height = rc.bottom - rc.top;

	DXGI_SWAP_CHAIN_DESC desc;
	if (SUCCEEDED(m_swapChain->GetDesc(&desc)))
	{
		if (desc.BufferDesc.Width == width && desc.BufferDesc.Height == height)
		{
			// Buffer is the correct size, no changes needed.
			return;
		}
	}

	releaseRenderTarget();
	
	HRESULT hr = m_swapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_UNKNOWN, 0);
	if (FAILED(hr)) throw std::runtime_error("DX11GameRenderer::resize swap chain failed");

	initRenderTarget();
}
