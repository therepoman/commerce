#include "stdafx.h"
#include "IButton.h"

#define MOUSE_X(l) (short) LOWORD(l)
#define MOUSE_Y(l) (short) HIWORD(l)

IButton::IButton(const WCHAR *title, int x, int y, int width, int height, int fontSize, float dpiX, float dpiY)
	: m_title(title), m_x(x), m_y(y), m_width(width), m_height(height), m_fontSize(fontSize), m_dpiX(dpiX), m_dpiY(dpiY)
{
}

IButton::~IButton()
{
}

void IButton::setTitle(const WCHAR *title)
{
	m_title = title;
	m_titleChanged = true;
}

bool IButton::handleClickMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message != WM_LBUTTONDOWN && message != WM_LBUTTONUP)
	{
		return false;
	}

	int x = MOUSE_X(lParam) * 96 / m_dpiX;
	int y = MOUSE_Y(lParam) * 96 / m_dpiY;
	bool isInsideLayout = m_x <= x && x <= m_x + m_width && m_y <= y && y <= m_y + m_height;

	if (message == WM_LBUTTONDOWN)
	{
		m_mouseDown = isInsideLayout;
	}
	else
	{
		bool previousMouseDown = m_mouseDown;
		m_mouseDown = false;
		return previousMouseDown && isInsideLayout;
	}

	return false;
}
