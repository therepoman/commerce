#pragma once

#include "IButton.h"
#include <d2d1.h>
#include <dwrite.h>
#include <atlbase.h>

class D2DButton : public IButton
{
public:
	D2DButton(const WCHAR *title, int x, int y, int width, int height, int fontSize, float dpiX, float dpiY);
	~D2DButton();

	void init(ID2D1RenderTarget *d2dRenderTarget);
	void release();

	void render(ID2D1RenderTarget *d2dRenderTarget);
private:
	D2D1_RECT_F m_layout;
	D2D1_RECT_F m_titleLayout;
	D2D1_RECT_F m_titleLayoutMouseDown;

	CComPtr<ID2D1SolidColorBrush> m_backgroundBrush;
	CComPtr<ID2D1SolidColorBrush> m_textBrush;
	CComPtr<IDWriteFactory> m_writeFactory;
	CComPtr<IDWriteTextFormat> m_textFormat;
};
