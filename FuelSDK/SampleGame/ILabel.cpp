#include "stdafx.h"
#include "ILabel.h"

#define MOUSE_X(l) (short) LOWORD(l)
#define MOUSE_Y(l) (short) HIWORD(l)

ILabel::ILabel(int x, int y, int width, int height, int fontSize, float dpiX, float dpiY)
	: m_x(x), m_y(y), m_width(width), m_height(height), m_fontSize(fontSize), m_dpiX(dpiX), m_dpiY(dpiY)
{
}

ILabel::~ILabel()
{
}

void ILabel::AddLine(std::wstring line)
{
	m_lines.push_front(line);
	if (m_lines.size() > 50)
	{
		m_lines.pop_back();
	}
	m_contentChanged = true;
}

std::wstring& ILabel::GetDisplayContent()
{
	if (m_contentChanged)
	{
		m_displayContent.clear();
		for (auto it = m_lines.begin(); it != m_lines.end(); ++it)
		{
			m_displayContent.append(*it);
		}
	}

	return m_displayContent;
}
