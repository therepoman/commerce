#pragma once
#include <cstdint>

extern "C"
{
	struct DirtyRect {
		int x;
		int y;
		int width;
		int height;
	};

	typedef void(__stdcall *OnDraw)(int, int dirtyRectCount, DirtyRect* dirtyRects, int size, const void*, int, int);
	typedef void(__stdcall *OnEvent)(int, const wchar_t*, const wchar_t*);
	typedef void(__stdcall *OnCursorChanged)(int, int type);
	typedef void(__stdcall *OnBeforeBrowse)(int instance);
	typedef int(__stdcall *OnBeforeResourceLoad)(int instance, const wchar_t* url);
	typedef void(__stdcall *OnLoadEnd)(int instance, int httpStatusCode);
	typedef void(__stdcall *OnLoadError)(int instance, int errorCode, const wchar_t* errorText, const wchar_t* failedUrl);
	typedef void(__stdcall *OnOpenInBrowser)(int instance, const wchar_t*);
	typedef wchar_t*(__stdcall *OnLoadLocalFile)(const wchar_t* file);
}
