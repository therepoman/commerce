#pragma once

#include "CEFFunctions.h"

using namespace Fuel::CEF;

CEFFunctions& CEFFunctions::Instance() 
{
	static CEFFunctions s_pInstance;
	return s_pInstance;
}

CEFFunctions::CEFFunctions():
m_onDrawCallback(nullptr),
m_onCursorChangedCallback(nullptr),
m_onEventCallback(nullptr),
m_onOpenInBrowser(nullptr),
m_onBeforeBrowseCallback(nullptr),
m_onBeforeResourceLoadCallback(nullptr),
m_onLoadEnd(nullptr),
m_onLoadError(nullptr),
m_onLoadLocalFile(nullptr)
{}

CEFFunctions::~CEFFunctions() 
{
	m_onDrawCallback = nullptr;
	m_onCursorChangedCallback = nullptr;
	m_onEventCallback = nullptr;
	m_onOpenInBrowser = nullptr;
	m_onOpenInBrowser = nullptr;
	m_onBeforeBrowseCallback = nullptr;
	m_onBeforeResourceLoadCallback = nullptr;
	m_onLoadEnd = nullptr;
	m_onLoadError = nullptr;
	m_onLoadLocalFile = nullptr;
}