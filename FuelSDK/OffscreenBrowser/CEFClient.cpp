#include <iostream>

#include "CEFClient.h"
#include "CEFFunctions.h"
#include "include/base/cef_bind.h"
#include "include/wrapper/cef_closure_task.h"

class CEFClientFactory :
	public CefLifeSpanHandler
{

};

bool CEFClient::OnBeforePopup(CefRefPtr<CefBrowser> browser,
	CefRefPtr<CefFrame> frame,
	const CefString& target_url,
	const CefString& target_frame_name,
	CefLifeSpanHandler::WindowOpenDisposition target_disposition,
	bool user_gesture,
	const CefPopupFeatures& popupFeatures,
	CefWindowInfo& windowInfo,
	CefRefPtr<CefClient>& client,
	CefBrowserSettings& settings,
	bool* no_javascript_access) {

	// Open in external browser instead
	Fuel::CEF::CEFFunctions::Instance().CallOnOpenInBrowser(m_instance, target_url.c_str());

	// Cancel the popup CEF window
	return true;
}

CefRefPtr<CEFClient> CEFClient::Create(int instance, OnDrawInternal onDraw, CefRect rect)
{
	CEFClient * client = nullptr;

	try
	{
		client = new CEFClient(instance, onDraw, rect);
		auto newInstance = CefRefPtr<CEFClient>(client);

		newInstance->m_browserReady.get_future().get();

		return newInstance;
	}
	catch (CEFClientCreationFailedException ex)
	{
		throw;
	}
	catch (...)
	{
		if (client)
			client->Destroy();

		throw CEFClientCreationFailedException("CEFClient::Create");
	}
}

void CEFClient::Destroy()
{
	m_browser->GetHost()->CloseBrowser(true);
	m_browser = nullptr;
	m_browserDestroyed.get_future().wait();
}

void CreateBrowser(CefClient* client, CefRect rect, std::promise<void> * m_browserReady)
{
	CefWindowInfo windowInfo;
	windowInfo.SetAsWindowless(NULL, true);
	windowInfo.width = rect.width;
	windowInfo.height = rect.height;
	windowInfo.x = rect.x;
	windowInfo.y = rect.y;

	CefBrowserSettings browserSettings;
	ULONG numLanguages = 0;
	DWORD languageSize = 0;

	//First time you call GetUserPreferredUILanguages, gets the language buffer size.
	//Second time, gets the actual language and stores it.
	if (GetUserPreferredUILanguages(MUI_LANGUAGE_NAME, &numLanguages, NULL, &languageSize))
	{
		WCHAR* language = new WCHAR[languageSize];
		if (GetUserPreferredUILanguages(MUI_LANGUAGE_NAME, &numLanguages, language, &languageSize)) {
			CefString(&browserSettings.accept_language_list).FromString16(language);
		}
		delete language;
	}

	std::string url = "about:blank";

	if (!CefBrowserHost::CreateBrowser(windowInfo, client, url, browserSettings, nullptr))
	{
		m_browserReady->set_exception(std::make_exception_ptr(CEFClientCreationFailedException("CreateBrowser")));
	}
}

CEFClient::CEFClient(int instance, OnDrawInternal onDraw, CefRect rect) :
	m_instance(instance),
	m_onDraw(onDraw),
	m_size(rect),
	m_loading(false)
{
	if (!CefPostTask(TID_UI, base::Bind(&CreateBrowser, this, rect, &m_browserReady)))
	{
		throw CEFClientCreationFailedException("CefPostTask");
	}
}

CefRefPtr<CefRenderHandler> CEFClient::GetRenderHandler()
{
	return this;
}

CefRefPtr<CefLifeSpanHandler> CEFClient::GetLifeSpanHandler()
{
	return this;
}

void CEFClient::OnAfterCreated(CefRefPtr<CefBrowser> browser)
{
	m_browser = browser;
	m_browserReady.set_value();
}

void CEFClient::OnBeforeClose(CefRefPtr<CefBrowser> browser)
{
	m_browserDestroyed.set_value();
}

bool CEFClient::GetViewRect(CefRefPtr<CefBrowser> browser, CefRect& rect)
{
	rect = m_size;
	return true;
}

void CEFClient::OnPaint(CefRefPtr<CefBrowser> browser, PaintElementType type, const RectList& dirtyRects, const void* buffer, int width, int height)
{
	std::unique_ptr<DirtyRect[]> rects(new DirtyRect[dirtyRects.size()]);

	for (int rect = 0; rect != dirtyRects.size(); rect++)
	{
		DirtyRect& destRect = rects[rect];
		const CefRect& srcRect = dirtyRects.at(rect);

		destRect.x = srcRect.x;
		destRect.y = srcRect.y;
		destRect.width = srcRect.width;
		destRect.height = srcRect.height;
	}

	m_onDraw(m_instance, dirtyRects.size(), rects.get(), width * height * 4, buffer, width, height);
}

bool CEFClient::OnProcessMessageReceived(CefRefPtr<CefBrowser> browser,
	CefProcessId source_process,
	CefRefPtr<CefProcessMessage> message)
{
	if (source_process == PID_RENDERER)
	{
		auto name = message->GetName();

		if (name == std::string("open_in_browser"))
		{
			auto url = message->GetArgumentList()->GetString(0).ToString16();
			
			Fuel::CEF::CEFFunctions::Instance().CallOnOpenInBrowser(m_instance, url.c_str());
		}
		else if (name == std::string("javascript_cef_event"))
		{
			auto eventName = message->GetArgumentList()->GetString(0).ToString16();
			auto eventArg = message->GetArgumentList()->GetString(1).ToString16();
			Fuel::CEF::CEFFunctions::Instance().CallOnEvent(m_instance, eventName.c_str(), eventArg.c_str());
		}

		return true;
	}

	return false;
}

void CEFClient::OnCursorChange(CefRefPtr<CefBrowser> browser,
	CefCursorHandle cursor,
	CefRenderHandler::CursorType type,
	const CefCursorInfo& custom_cursor_info)
{
	// Don't override the loading cursor when a page is loading
	if (!m_loading)
	{
		Fuel::CEF::CEFFunctions::Instance().CallOnCursorChanged(m_instance, (int)cursor);
	}
}

void CEFClient::OnLoadEnd(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame, int httpStatusCode)
{
	if (frame->IsMain())
	{
		Fuel::CEF::CEFFunctions::Instance().CallOnLoadEnd(m_instance, httpStatusCode);
		Fuel::CEF::CEFFunctions::Instance().CallOnCursorChanged(m_instance, (int)LoadCursor(NULL, IDC_ARROW));
		m_loading = false;
	}
}

void CEFClient::OnLoadError(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
	CefLoadHandler::ErrorCode errorCode, const CefString& errorText, const CefString& failedUrl)
{
	if (frame->IsMain())
	{
		std::wstring errorTextString(errorText);
		std::wstring failedUrlString(failedUrl);
		Fuel::CEF::CEFFunctions::Instance().CallOnLoadError(m_instance, errorCode,
			errorTextString.c_str(), failedUrlString.c_str());
		Fuel::CEF::CEFFunctions::Instance().CallOnCursorChanged(m_instance, (int)LoadCursor(NULL, IDC_ARROW));
		m_loading = false;
	}
}

bool CEFClient::OnBeforeBrowse(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
	CefRefPtr<CefRequest> request, bool is_redirect)
{
	if (frame->IsMain())
	{
		Fuel::CEF::CEFFunctions::Instance().CallOnBeforeBrowse(m_instance);
		Fuel::CEF::CEFFunctions::Instance().CallOnCursorChanged(m_instance, (int)LoadCursor(NULL, IDC_APPSTARTING));
		m_loading = true;
	}

	// Allow the browser to continue
	return false;
}

CefRequestHandler::ReturnValue CEFClient::OnBeforeResourceLoad(
	CefRefPtr<CefBrowser> browser,
	CefRefPtr<CefFrame> frame,
	CefRefPtr<CefRequest> request,
	CefRefPtr<CefRequestCallback> callback)
{
	if (Fuel::CEF::CEFFunctions::Instance().CallOnBeforeResourceLoad(m_instance, request->GetURL().c_str()) == ReturnValue::RV_CANCEL)
	{
		return ReturnValue::RV_CANCEL;
	}

	CefRequest::HeaderMap hMap;
	request->GetHeaderMap(hMap);

	hMap.insert(m_hMap.begin(), m_hMap.end());
	if (m_browser)
	{
		m_browser->GetHost()->SendFocusEvent(true);
	}

	request->SetHeaderMap(hMap);
	return ReturnValue::RV_CONTINUE;
}

bool CEFClient::OnJSDialog(CefRefPtr<CefBrowser> browser, const CefString& origin_url,
	JSDialogType dialog_type, const CefString& message_text, const CefString& default_prompt_text,
	CefRefPtr<CefJSDialogCallback> callback, bool& suppress_message)
{
	// Javascript dialogs are disabled.
	suppress_message = true;

	// CEF documentation says to return false when setting suppress_message to true.
	return false;
}

bool CEFClient::OnBeforeUnloadDialog(CefRefPtr<CefBrowser> browser, const CefString& message_text,
	bool is_reload, CefRefPtr<CefJSDialogCallback> callback)
{
	// Allow all pages to be unloaded without prompting.
	callback->Continue(true, "");

	// true causes CEF to not show the default dialog popup, and instead wait for the callback.
	return true;
}

void CEFClient::OnBeforeContextMenu(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
	CefRefPtr<CefContextMenuParams> params, CefRefPtr<CefMenuModel> model)
{
	// Disable context menus.
	model->Clear();
}

void CEFClient::Resize(int width, int height)
{
	m_size.width = width;
	m_size.height = height;

	if (m_browser)
	{
		m_browser->GetHost()->WasResized();
	}
}

void CEFClient::AddDataToHeader(CefString key, CefString value)
{
	m_hMap.insert(std::make_pair(key, value));
}

void CEFClient::RemoveDataFromHeader(CefString key)
{
	m_hMap.erase(key);
}
