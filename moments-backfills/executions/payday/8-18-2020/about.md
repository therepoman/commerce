## Overview
About ~100 payday transactions are stuck in-flight (for an unknown reason).
This causes various queues to fail to process the transactions.
To fix this we run the [Payday Fix in Flight Transactions script](https://git.xarth.tv/commerce/payday/blob/master/cmd/fix_in_flight_transactions/main.go).
This script copies the in-flight transactions over into the primary transactions table.

## Pre-Backfill State
Roughly ~100 transactions stuck in payday's in-flight transactions table

## This Backfill
Moves the in-flight transactions records over to the primary record

## Status
- I ran this backfill at ~3:23pm on 8/18/2020 - @seph
- Running again in dryrun shows no changes
