## Overview
Due to several previous issues there are several transactions with incorrect data. This backfill will fix them.

## Previous Issues
- Pokemon Go bit type / product id was too long for pachter, need to be backfilled to a shorter value
- That backfill was messed up, other transactions were erroneously updated
- The lastupdated field was also changed which breaks pachter

## Pre-Backfill State
- We have a set of transaction ids that is a super set of the affected transaction records
- We have restored an old dynamo backup that represents the original values of these transactions

## This Backfill
- Reads the current transaction and the original backedup transaction
- Determines what records should be updated
  - A record should be updated if its old state (from the backup) is not pogo related, but it's new state is.
    In this case the transaction was erroneously updated by a previous backfill.
  - Any records where the lastupdated timestamp doesn't match should be reverted its previous value.
- Updates those records

## Status
- I ran this backfill at ~4:50pm on 8/3/2020 - @seph
- Running again in dry run mode shows no pending changes
