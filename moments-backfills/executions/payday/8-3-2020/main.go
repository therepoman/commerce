package main

import (
	"fmt"
	"strconv"
	"time"

	"code.justin.tv/commerce/moments-backfills/utils/dynamo"
	"code.justin.tv/commerce/moments-backfills/utils/dynamo/payday"
	"code.justin.tv/commerce/moments-backfills/utils/env"
	"code.justin.tv/commerce/moments-backfills/utils/io"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	awsProfile = "payday"
	awsRegion  = "us-west-2"
	dryRun     = true // when this is true, prints planned changes without actually applying them

	fileName = "executions/payday/8-3-2020/transaction_ids.csv"

	table       = "prod_transactions"
	backupTable = "prod_transactions_week_backup_2020-07-20T00-00-45Z"

	oldPogoBitType = "phanto.Niantic.Pokemon Go Battle League.July2020"
	newPogoBitType = "phanto.Niantic.PoGo Battle League.July2020"

	oldPogoProductID = "phanto.Niantic.Pokemon Go Battle League.July2020.100"
	newPogoProductID = "phanto.Niantic.PoGo Battle League.July2020.100"
)

type StringChange struct {
	Key    string
	OldVal string
	NewVal string
}

type Int64Change struct {
	Key    string
	OldVal int64
	NewVal int64
}

type TXPair struct {
	TX       *payday.Transaction
	BackupTX *payday.Transaction
}

func main() {
	env.SetAWSProfile(awsProfile)
	env.SetAWSRegion(awsRegion)
	rows := io.ReadCSV(fileName)

	dynamoClient := dynamo.NewClient()

	transactionPairs := make(map[string]TXPair)

	// load transactions and backup transactions from dynamo
	for _, row := range rows {
		txID := row[0]
		tx := payday.GetTransaction(dynamoClient, table, txID)
		backupTx := payday.GetTransaction(dynamoClient, backupTable, txID)

		if tx == nil {
			// if tx record isn't present, it's in the extensions table and we can ignore
			continue
		}

		if backupTx == nil {
			panic("could not find a backup tx for transaction id")
		}

		if backupTx.BitsType == nil && tx.BitsType == nil {
			// tx has no bit type, we can ignore it
			continue
		}

		if backupTx.ProductId == nil && tx.ProductId == nil {
			// tx has no product id, we can ignore it
			continue
		}

		transactionPairs[txID] = TXPair{
			TX:       tx,
			BackupTX: backupTx,
		}

		time.Sleep(5 * time.Millisecond)
	}

	// identify necessary bit type changes
	bitTypeChanges := make(map[string]StringChange)
	for txID, txPair := range transactionPairs {
		if *txPair.BackupTX.BitsType == oldPogoBitType && *txPair.TX.BitsType == newPogoBitType {
			// correctly backfilled already, no bit type change necessary
			continue
		}

		if *txPair.BackupTX.BitsType == *txPair.TX.BitsType {
			// bit types are equal, no bit type change necessary
			continue
		}

		bitTypeChanges[txID] = StringChange{
			Key:    txID,
			OldVal: *txPair.TX.BitsType,
			NewVal: *txPair.BackupTX.BitsType,
		}
	}

	// identify necessary product id changes
	productIDChanges := make(map[string]StringChange)
	for txID, txPair := range transactionPairs {
		if *txPair.BackupTX.ProductId == oldPogoProductID && *txPair.TX.ProductId == newPogoProductID {
			// correctly backfilled already, no product id change necessary
			continue
		}

		if *txPair.BackupTX.ProductId == *txPair.TX.ProductId {
			// product ids are equal, no product id change necessary
			continue
		}

		productIDChanges[txID] = StringChange{
			Key:    txID,
			OldVal: *txPair.TX.ProductId,
			NewVal: *txPair.BackupTX.ProductId,
		}
	}

	// identify necessary last updated changes
	lastUpdatedChanges := make(map[string]Int64Change)
	for txID, txPair := range transactionPairs {
		if txPair.BackupTX.LastUpdated == txPair.TX.LastUpdated {
			// last updated values are equal, no last updated change necessary
			continue
		}

		lastUpdatedChanges[txID] = Int64Change{
			Key:    txID,
			OldVal: txPair.TX.LastUpdated,
			NewVal: txPair.BackupTX.LastUpdated,
		}
	}

	for txID := range transactionPairs {
		dynamoChange := make(map[string]*dynamodb.AttributeValueUpdate)

		btChange, btWillChange := bitTypeChanges[txID]
		pIDChange, pIDWillChange := productIDChanges[txID]
		luChange, luWillChange := lastUpdatedChanges[txID]
		if !btWillChange && !pIDWillChange && !luWillChange {
			// nothing changing
			continue
		}

		fmt.Println(txID)
		if btWillChange {
			fmt.Printf("Bit Type: %s -> %s \n", btChange.OldVal, btChange.NewVal)
			dynamoChange["bits_type"] = &dynamodb.AttributeValueUpdate{
				Value: &dynamodb.AttributeValue{
					S: aws.String(btChange.NewVal),
				},
			}
		}
		if pIDWillChange {
			fmt.Printf("Product ID: %s -> %s \n", pIDChange.OldVal, pIDChange.NewVal)
			dynamoChange["product_id"] = &dynamodb.AttributeValueUpdate{
				Value: &dynamodb.AttributeValue{
					S: aws.String(pIDChange.NewVal),
				},
			}
		}
		if luWillChange {
			fmt.Printf("Last Updated: %d -> %d \n", luChange.OldVal, luChange.NewVal)
			dynamoChange["last_updated"] = &dynamodb.AttributeValueUpdate{
				Value: &dynamodb.AttributeValue{
					N: aws.String(strconv.FormatInt(luChange.NewVal, 10)),
				},
			}
		}

		if dryRun {
			fmt.Println("Skipped due to dry run...")
		} else {
			payday.UpdateTransaction(dynamoClient, table, txID, dynamoChange)
			fmt.Println("Successfully updated...")
		}

		fmt.Println("")
	}
}
