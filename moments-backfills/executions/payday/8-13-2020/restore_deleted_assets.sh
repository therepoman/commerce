#!/bin/bash

lastModified="2020-08-13T13:20:00.000Z"
bucket="bits-assets"
dryrun="false"
prefix="partner-actions"
output="deleted_assets.txt"

function main() {
  echo -------------------------------
  echo "Welcome to Assets Restore Script"
  echo "1. Get Deleted Assets"
  echo "2. Restore Deleted Assets"
  read -p "What do you want to do (1,2,exit)? " ans

  if [ $ans == 1 ]; then
    get_deleted_assets
  elif [ $ans == 2 ]; then
    restore_deleted_assets
  else
    echo "exiting..."
  fi
  echo -------------------------------
}

function get_deleted_assets() {
  echo "Keep an eye on the output file $output"
  echo "Once it stops getting populated you can terminate early (if the bucket is big)"
  echo "(Note, you may see a few Nones at the end, delete them)"
  aws s3api list-object-versions\
    --output text\
    --query "DeleteMarkers[?LastModified >= \`$lastModified\`].{Key:Key,VersionId:VersionId}"\
    --bucket "$bucket"\
    --prefix "$prefix"\
    > $output
}

function restore_deleted_assets() {
  while IFS="	" read -ra fileinfo; do
    infoArr=()
    for i in "${fileinfo[@]}"; do
      infoArr+=($i)
    done
    filepath=${infoArr[0]}
    verID=${infoArr[1]}
    echo "File to Restore: $filepath"
    echo "File Delete ver: $verID"

    if [ $dryrun == "true" ]; then
      echo "Dryrun"
      restoreOutput="restore_output_dryrun.log"
      echo "${filepath} ${verID}" >> $restoreOutput
    else
      echo "Real Run!"
      restoreOutput="restore_output.log"
      aws s3api delete-object\
        --output text\
        --bucket "$bucket"\
        --key "$filepath"\
        --version-id "$verID"\
        >> $restoreOutput
      sleep .2
    fi

  done < $output
}

main