## Overview
Due to accidental deletion of partner bits-assets, we needed a script that can undo this action. This is possible because the bucket has versioning enabled.

Retro Doc: https://docs.google.com/document/d/170JK3g8Q7xRaGwpv0aRcxO7SRjkJxX5pvRMttBMJhBQ/edit

## Pre-Run State
10 Twitch Partners were impacted by this from **2020-08-13 13:30:00 PDT** till **TBD**

Their viewers would still be able to use the partner's custom cheermotes, but it would appear as a blank image placeholder.

## This Script
- First it uses AWS CLI (`s3api list-object-versions`) to fetch the files and its version ID (marking its deletion) that were deleted after 2020-08-13 13:20:00
  (We widened the time window just in case)
- Then it uses AWS CLI (`s3api delete-object`) to delete the version ID marking the file's deletion, restoring it to pre-delete state.

## Resources

https://aws.amazon.com/premiumsupport/knowledge-center/s3-undelete-configuration/
https://docs.aws.amazon.com/cli/latest/reference/s3api/list-object-versions.html
https://docs.aws.amazon.com/cli/latest/reference/s3api/delete-object.html

## Status
- I ran the dryrun at **~2020-08-13 17:50 PDT** after demoing solution to Seph and Nick
- I ran the dryrun again after removing the entry for partner-assets folder (since it wasn't deleted)
- I plan to do a manual inspection Friday morning in S3 Bucket to make sure impacted partners haven't tried to reupload the assets. If they have, I will skip their assets in the execution.
- ~I created a backup bucket `bits-assets-partner-actions-backup` that stores all the assets before the run in case something goes wrong. This will be deleted after we confirm the run is complete~ copy ignores files marked for deletion, so this step would not have been helpful.
- We may need to look into setting up a periodic backup job for public buckets.