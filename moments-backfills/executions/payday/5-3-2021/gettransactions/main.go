package main

import (
	"code.justin.tv/commerce/moments-backfills/utils/dynamo"
	"code.justin.tv/commerce/moments-backfills/utils/dynamo/payday"
	"code.justin.tv/commerce/moments-backfills/utils/env"
	"context"
	"encoding/csv"
	"fmt"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"golang.org/x/time/rate"
	"os"
)

const (
	awsProfile = "payday"
	awsRegion  = "us-west-2"
	tableName = "prod_extension_transactions"
)

var (
	// 5000 RCUs (5 * 1000 items per scan)
	ddbReadLimiter = rate.NewLimiter(5, 1)
)

func main() {
	env.SetAWSProfile(awsProfile)
	env.SetAWSRegion(awsRegion)

	dynamoClient := dynamo.NewClient()
	ctx := context.Background()

	csvfile, err := os.Create("./outignore/transactions.csv")
	if err != nil {
		panic(err)
	}

	csvwriter := csv.NewWriter(csvfile)
	defer func() {
		_ = csvfile.Close()
	}()

	var cursor map[string]*dynamodb.AttributeValue
	iterations := 0

	for {
		err := ddbReadLimiter.Wait(ctx)
		if err != nil {
			panic(err)
		}

		transactions, newCursor := payday.ScanExtensionTransactions(dynamoClient, context.Background(), tableName, cursor)

		err = csvwriter.WriteAll(transactionsToRows(transactions))
		if err != nil {
			panic(err)
		}
		csvwriter.Flush()

		fmt.Printf("Items Scanned: %d\n", (iterations + 1) * 1000)

		if newCursor == nil {
			break
		}

		cursor = newCursor
		iterations++
	}
}

func transactionsToRows(transactions []payday.ExtensionTransaction) [][]string {
	var rows [][]string

	for _, tx := range transactions {
		// No need to update transactions created after we started writing new fields
		if tx.UserIDStatus != "" && tx.ChannelIDStatus != "" {
			continue
		}

		rows = append(rows, []string{
			tx.TransactionID,
			tx.ChannelID,
			tx.Status,
			tx.UserID,
		})
	}

	return rows
}