package main

import (
	"code.justin.tv/commerce/moments-backfills/utils/dynamo"
	"code.justin.tv/commerce/moments-backfills/utils/dynamo/payday"
	"code.justin.tv/commerce/moments-backfills/utils/env"
	"context"
	"encoding/csv"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"golang.org/x/time/rate"
	"os"
	"strconv"
	"strings"
)

const (
	dryRun = false
	redriveFailed = true

	awsProfile = "payday"
	awsRegion  = "us-west-2"
	tableName = "prod_extension_transactions"
)

var (
	// use 4000 of 5000 WCUs max
	ddbWriteLimiter = rate.NewLimiter(4000, 1)
)

func main() {
	if !dryRun {
		fmt.Println("!starting real run!")
	}

	env.SetAWSProfile(awsProfile)
	env.SetAWSRegion(awsRegion)

	dynamoClient := dynamo.NewClient()

	if redriveFailed {
		erredUpdates := getErredUpdates()
		fmt.Printf("redrive %d failed updates", len(erredUpdates))
		updateTransactions(dynamoClient, erredUpdates)
	} else {
		transactions := getTransactions()
		updateTransactions(dynamoClient, transactions)
	}
}

type UpdateResult struct {
	TransactionID string
	UserID string
	UserIDStatus string
	ChannelIDStatus string
	Success bool
	Error error
}

func updateTransactions(dynamoClient *dynamodb.DynamoDB, transactions []payday.ExtensionTransaction) {
	ctx := context.Background()

	var csvfile *os.File
	var err error

	if redriveFailed {
		csvfile, err = os.OpenFile("./outignore/progress.csv", os.O_APPEND|os.O_WRONLY, 0600)
	} else {
		csvfile, err = os.Create("./outignore/progress.csv")
	}

	if err != nil {
		panic(err)
	}

	csvwriter := csv.NewWriter(csvfile)
	defer func() {
		_ = csvfile.Close()
	}()

	numJobs := len(transactions)
	jobs := make(chan payday.ExtensionTransaction, numJobs)
	results := make(chan UpdateResult, numJobs)

	// 100 workers
	for w := 0; w <= 100; w++ {
		go updateWorker(ctx, dynamoClient, jobs, results)
	}

	go func() {
		for _, job := range transactions {
			jobs <- job
		}
		close(jobs)
	}()

	for a := 0; a < numJobs; a++ {
		if a % 100 == 0 {
			fmt.Printf("Dynamo update progress: %d of %d\n", a, numJobs)
		}

		result := <- results

		err := csvwriter.Write([]string{
			result.TransactionID,
			result.UserID,
			result.ChannelIDStatus,
			result.UserIDStatus,
			strconv.FormatBool(result.Success),
			fmt.Sprintf("%+v", result.Error),
		})
		if err != nil {
			panic(err)
		}
		csvwriter.Flush()
	}
}

func updateWorker(ctx context.Context, dynamoClient *dynamodb.DynamoDB, jobs <-chan payday.ExtensionTransaction, results chan<- UpdateResult) {
	for job := range jobs {
		userIDStatus := fmt.Sprintf("%s.%s", job.UserID, job.Status)
		channelIDStatus := fmt.Sprintf("%s.%s", job.ChannelID, job.Status)

		dynamoChange := make(map[string]*dynamodb.AttributeValueUpdate)
		dynamoChange["userIDStatus"] = &dynamodb.AttributeValueUpdate{
			Value: &dynamodb.AttributeValue{
				S: aws.String(userIDStatus),
			},
		}
		dynamoChange["channelIDStatus"] = &dynamodb.AttributeValueUpdate{
			Value: &dynamodb.AttributeValue{
				S: aws.String(channelIDStatus),
			},
		}

		result := UpdateResult{
			TransactionID: job.TransactionID,
			UserID: job.UserID,
			UserIDStatus: userIDStatus,
			ChannelIDStatus: channelIDStatus,
		}

		if !dryRun {
			err := ddbWriteLimiter.Wait(ctx)
			if err != nil {
				panic(err)
			}

			err = payday.UpdateExtensionTransaction(dynamoClient, tableName, job.UserID, job.TransactionID, dynamoChange)

			if err != nil {
				result.Error = err
			} else {
				result.Success = true
			}
		}

		results <- result
	}
}

func readCsvFile(path string) ([][]string, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer func() {
		_ = f.Close()
	}()

	csvReader := csv.NewReader(f)
	records, err := csvReader.ReadAll()
	if err != nil {
		return nil, err
	}

	return records, nil
}

func getErredUpdates() []payday.ExtensionTransaction {
	lines, err := readCsvFile("./outignore/progress.csv")
	if err != nil {
		panic(err)
	}

	var transactions []payday.ExtensionTransaction

	for _, line := range lines {
		transactionID := line[0]
		userID := line[1]
		channelIDStatus := line[2]
		//userIDStatus := line[3]
		success := line[4]

		if success == "true" {
			continue
		}

		split := strings.Split(channelIDStatus, ".")
		channelID := split[0]
		status := split[1]

		transactions = append(transactions, payday.ExtensionTransaction{
			TransactionID: transactionID,
			ChannelID: channelID,
			Status: status,
			UserID: userID,
		})
	}

	return transactions
}

func getTransactions() []payday.ExtensionTransaction {
	lines, err := readCsvFile("../gettransactions/outignore/transactions.csv")
	if err != nil {
		panic(err)
	}

	var transactions []payday.ExtensionTransaction

	for _, line := range lines {
		transactions = append(transactions, payday.ExtensionTransaction{
			TransactionID: line[0],
			ChannelID: line[1],
			Status: line[2],
			UserID: line[3],
		})
	}

	return transactions
}