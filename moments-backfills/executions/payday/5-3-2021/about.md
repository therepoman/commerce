## Overview

Backfills the ChannelIDStatus and UserIDStatus composite keys on the Extension Transactions table


## Status

* Ran gettransactions/main, output (>1GB uncompressed, too large for git): https://drive.google.com/file/d/1ZBbzidgIbGJThyl4GPshZqZfICMggdas/view?usp=sharing
* Ran updatetransactions/main, output (>1GB uncompressed, too large for git): https://drive.google.com/file/d/1MfYJ1ZdN8DrNccWolNDfpt9HKcaSMk3Z/view?usp=sharing
