## Overview
Majority of BTER entitlements not working for several month, after we implemented the fix, we needed to backfill these failed entitlements.

## Pre-Backfill State
- Users who should have cheered enough to unlock BTER emotes still cannot use them.

## This Backfill
The script is from Payday's [BTER Backfill Script](https://git.xarth.tv/commerce/payday/tree/master/cmd/emote_rewards_backfill)

This script does the following:
- get all Badge Tier Emote Groups
- publishes these groups to SNS topic which is subscribed to by a SQS queue
- SQS queue handler takes these messages, queries [Pantheon](https://git.xarth.tv/commerce/pantheon) to get all users elliglbe for this group
- SQS handler requeues these messages
- The second time it gets it, it tries to call [Mako](https://git.xarth.tv/commerce/mako) 's `CreateGroupEntitlements` endpoint, because Mako is not a service we own, we rate limit on this.

## Additional Notes
- I ran the script remotely on a Payday Prod EC2 instance.
- There is a flag called `ignoreBackfilled`, we had to set this to true because it is possible for users to have failed entitlements since the last backfill.

## Status
@hpokuan
- I ran the dryrun of this backfill at ~3:00pm on 8/6/2020.<br>
  [execution log](./remote_progress/production_dryrun_1596750779.log) |
  [progress file](./remote_progress/progress_production_dryrun.csv)
- I ran a production run on 100 channels at around the same time.<br>
  [execution log](./remote_progress/production_1596750971.log) |
  [progress file](./remote_progress/progress_production_specified_channels.csv)
- I started the full production run at ~9.00am on 8/7/2020. The script crashed at around ~5.00pm which I found out and restarted at ~6:40pm. It finished the next day at around 1:30pm.<br>
  [execution log before crash](./remote_progress/production_1596816431.log) |
  [execution log after crash](./remote_progress/production_1596850995.log) |
  [progress file](./remote_progress/progress_production.csv)

