package main

import (
	"fmt"
	"strconv"

	"code.justin.tv/commerce/moments-backfills/utils/dynamo"
	"code.justin.tv/commerce/moments-backfills/utils/dynamo/payday"
	"code.justin.tv/commerce/moments-backfills/utils/env"
	"code.justin.tv/commerce/moments-backfills/utils/io"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	awsProfile = "payday"
	awsRegion  = "us-west-2"
	dryRun     = true // when this is true, prints planned changes without actually applying them

	table    = "prod_transactions"
	fileName = "executions/payday/8-11-2020/part-2/transaction_ids.csv"
)

func main() {
	env.SetAWSProfile(awsProfile)
	env.SetAWSRegion(awsRegion)

	dynamoClient := dynamo.NewClient()

	rows := io.ReadCSV(fileName)
	for _, row := range rows {
		txID := row[0]

		tx := payday.GetTransaction(dynamoClient, table, txID)
		fmt.Printf("Transaction ID: %s \n", tx.TransactionId)
		fmt.Printf("Current User ID: %s \n", *tx.UserId)
		fmt.Printf("Current Transaction Type: %s \n", *tx.TransactionType)
		fmt.Printf("Current Finalized Transaction Type: %s \n", *tx.FinalizedTransactionType)
		fmt.Printf("Recipients Length: %d \n", len(tx.Recipients))
		if len(tx.Recipients) != 1 {
			panic("expecting exactly 1 recipient")
		}
		fmt.Printf("Recipient: %+v \n", *tx.Recipients[0])

		newNumberOfBits := tx.Recipients[0].RewardAttribution
		fmt.Printf("Current Number of Bits: %d \n", *tx.NumberOfBits)
		fmt.Printf("New Number of Bits: %d \n", newNumberOfBits)
		fmt.Println()

		if !dryRun {
			dynamoChange := make(map[string]*dynamodb.AttributeValueUpdate)
			dynamoChange["number_of_bits"] = &dynamodb.AttributeValueUpdate{
				Value: &dynamodb.AttributeValue{
					N: aws.String(strconv.FormatInt(newNumberOfBits, 10)),
				},
			}
			payday.UpdateTransaction(dynamoClient, table, txID, dynamoChange)
		}
	}
}
