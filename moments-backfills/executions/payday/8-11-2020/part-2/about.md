## Overview
A few challenge transactions without number_of_bits at the root level

## Pre-Backfill State
The transactions are missing number_of_bits at the root level

## This Backfill
Adds number_of_bits to the transaction root level by using the recipient reward attribution

## Status
- I ran this backfill at ~1:42pm on 8/11/2020 - @seph
- Running again in dry run mode shows no pending changes