## Overview
A few transactions with the wrong finalized type

## Pre-Backfill State
The transactions have a finalized type of "GiveBitsToBroadcaster"

## This Backfill
The transaction will be changed to have a finalized type of "GiveBitsToBroadcasterChallenge"

## Status
- I ran this backfill at ~10:58am on 8/11/2020 - @seph
- Running again in dry run mode shows no pending changes
