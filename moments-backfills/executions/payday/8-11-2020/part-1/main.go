package main

import (
	"fmt"

	"code.justin.tv/commerce/moments-backfills/utils/dynamo"
	"code.justin.tv/commerce/moments-backfills/utils/dynamo/payday"
	"code.justin.tv/commerce/moments-backfills/utils/env"
	"code.justin.tv/commerce/moments-backfills/utils/io"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	awsProfile = "payday"
	awsRegion  = "us-west-2"
	dryRun     = true // when this is true, prints planned changes without actually applying them

	table    = "prod_transactions"
	fileName = "executions/payday/8-11-2020/part-1/transaction_ids.csv"

	newFinalizedTransactionType = "GiveBitsToBroadcasterChallenge"
)

func main() {
	env.SetAWSProfile(awsProfile)
	env.SetAWSRegion(awsRegion)

	dynamoClient := dynamo.NewClient()

	rows := io.ReadCSV(fileName)
	for _, row := range rows {
		txID := row[0]

		tx := payday.GetTransaction(dynamoClient, table, txID)
		fmt.Printf("Transaction ID: %s \n", tx.TransactionId)
		fmt.Printf("Current User ID: %s \n", *tx.UserId)
		fmt.Printf("Current Transaction Type: %s \n", *tx.TransactionType)
		fmt.Printf("Current Finalized Transaction Type: %s \n", *tx.FinalizedTransactionType)
		fmt.Printf("New Finalized Transaction Type: %s \n", newFinalizedTransactionType)
		fmt.Println("")

		if !dryRun {
			dynamoChange := make(map[string]*dynamodb.AttributeValueUpdate)
			dynamoChange["finalized_transaction_type"] = &dynamodb.AttributeValueUpdate{
				Value: &dynamodb.AttributeValue{
					S: aws.String(newFinalizedTransactionType),
				},
			}
			payday.UpdateTransaction(dynamoClient, table, txID, dynamoChange)
		}
	}
}
