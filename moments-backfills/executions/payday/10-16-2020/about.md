## Overview
We needed to backfill entitlements of BTER emotes that were created in the period between 10/7/2020 and 10/16/2020. This is because Digital Assets ran another backfill on 10/8/2020 and 10/9/2020 that created all the emote group IDs for all bits onboarded channels, meaning the "new group ID" check for backfilling entitlements was no longer valid. A change was made so that entitlements are backfilled for groups that are not marked "IsBackfilled", and then a backfill of missing entitlements in the week since then was run. 

## Pre-Backfill State
- Some viewers were not entitled to newly created BTER emotes

## This Backfill
The script is from Payday's [BTER Backfill Script](https://git.xarth.tv/commerce/payday/tree/master/cmd/emote_rewards_backfill)

However, I slightly modified it to do the following:
This script does the following:
- get all Badge Tier Emote Group IDs from a supplied CSV file
- query each group ID to get the full Badge Tier Group from Dynamo
- publishes these groups to SNS topic which is subscribed to by a SQS queue
    - If already backfilled, don't even send an SNS message
- SQS queue handler takes these messages, queries [Pantheon](https://git.xarth.tv/commerce/pantheon) to get all users elliglbe for this group
    - If the group has already been backfilled, the SQS handler returns immediately
- SQS handler requeues these messages
- The second time it gets it, it tries to call [Mako](https://git.xarth.tv/commerce/mako) 's `CreateGroupEntitlements` endpoint, because Mako is not a service we own, we rate limit on this.

## Additional Notes
- I ran the script on my local machine because it was a small amount of groups to backfill

## Status
@jelhay
- I started the full production run at ~12:21pm on 10/16/2020. It finished at around 12:28pm.<br>
  [execution log](execution.log) |
  [progress file](progress_production.csv) |
  [list of emote groups to backfill](emote_groups_to_backfill_2020-10-16T11:31:31-07:00.csv)

