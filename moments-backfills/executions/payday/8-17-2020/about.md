## Overview
A few older transactions are stuck in the pachter cheermotes DLQ
These transactions cannot be processed by pachter since they are missing the emote_totals field on their payday dynamo transaction record

## Pre-Backfill State
Transactions in payday with broken emote_totals field

## This Backfill
Sets the emote_totals attribute on the dynamo transaction based on theraw message that was originally sent

## Status
- I ran this script at ~4:29pm on 8/17/2020 - @seph
- Running again in dry run mode shows no pending changes
