package main

import (
	"fmt"
	"strconv"

	"code.justin.tv/commerce/moments-backfills/utils/cheermotes"
	"code.justin.tv/commerce/moments-backfills/utils/dynamo"
	payday_dynamo "code.justin.tv/commerce/moments-backfills/utils/dynamo/payday"
	"code.justin.tv/commerce/moments-backfills/utils/env"
	"code.justin.tv/commerce/moments-backfills/utils/io"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	awsProfile              = "payday"
	awsRegion               = "us-west-2"
	paydayTransactionsTable = "prod_transactions"
	filePath                = "executions/payday/8-17-2020/transaction_ids.csv"
	dryRun                  = true // skip updating the emote totals
)

func main() {
	env.SetAWSProfile(awsProfile)
	env.SetAWSRegion(awsRegion)
	dc := dynamo.NewClient()

	rows := io.ReadCSV(filePath)
	for _, row := range rows {
		txID := row[0]
		tx := payday_dynamo.GetTransaction(dc, paydayTransactionsTable, txID)
		if *tx.TransactionType != "BITS_EVENT" {
			continue
		}

		newEmoteTotals := cheermotes.GetCheermotes(cheermotes.BuildRegexMap(cheermotes.DefaultPrefixes), tx.RawMessage)
		totalBitsFromAllCheermotes := cheermotes.GetTotalFromCheermotes(newEmoteTotals)
		if *tx.NumberOfBits != totalBitsFromAllCheermotes {
			panic(fmt.Sprintf("emote totals don't match: %d VS %d", *tx.NumberOfBits, totalBitsFromAllCheermotes))
		}
		fmt.Printf("Existing Message: %s \n", tx.RawMessage)
		fmt.Printf("Existing Emote Totals: %v \n", tx.EmoteTotals)
		fmt.Printf("New Emote Totals: %v \n", newEmoteTotals)

		if dryRun {
			fmt.Println("Skip update due to dry run...")
		} else {
			dynamoChange := make(map[string]*dynamodb.AttributeValueUpdate)
			dynamoChange["emote_totals"] = &dynamodb.AttributeValueUpdate{
				Value: &dynamodb.AttributeValue{
					M: toAttributeValueMap(newEmoteTotals),
				},
			}

			payday_dynamo.UpdateTransaction(dc, paydayTransactionsTable, txID, dynamoChange)
			fmt.Println("Successfully updated emote totals...")
		}

		fmt.Println("")
	}

}

func toAttributeValueMap(emoteTotals map[string]int64) map[string]*dynamodb.AttributeValue {
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	for emote, bits := range emoteTotals {
		attributeMap[emote] = &dynamodb.AttributeValue{
			N: aws.String(strconv.FormatInt(bits, 10)),
		}
	}
	return attributeMap
}
