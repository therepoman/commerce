package main

import (
	"fmt"
	"strconv"
	"time"

	"code.justin.tv/commerce/moments-backfills/utils/dynamo"
	"code.justin.tv/commerce/moments-backfills/utils/dynamo/payday"
	"code.justin.tv/commerce/moments-backfills/utils/env"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	awsProfile = "payday"
	awsRegion  = "us-west-2"
	dryRun     = true // when this is true, prints planned changes without actually applying them
	table      = "prod_transactions"

	inputTransaction   = "CustomerChargeback:anyTypeRevoke:18120a46-6ee7-4a4f-9004-bee5d186cde9"
	destinationTimeStr = "2020-08-14T10:00:00-07:00"
	annotation         = "Timestamp backfilled by Seph (originally was 2018-08-12 15:39:39 -0700 PDT)"
)

func main() {
	env.SetAWSProfile(awsProfile)
	env.SetAWSRegion(awsRegion)

	dynamoClient := dynamo.NewClient()

	destinationTime, err := time.Parse(time.RFC3339, destinationTimeStr)
	if err != nil {
		panic(err)
	}

	tx := payday.GetTransaction(dynamoClient, table, inputTransaction)

	fmt.Printf("Current TimeOfEvent: %s \n", time.Unix(0, tx.TimeOfEvent))
	fmt.Printf("New TimeOfEvent: %s \n", time.Unix(0, destinationTime.UnixNano()))

	fmt.Printf("Current LastUpdated: %s \n", time.Unix(0, tx.LastUpdated))
	fmt.Printf("New LastUpdated: %s \n", time.Unix(0, destinationTime.UnixNano()))

	fmt.Printf("Current Annotation: %s \n", tx.Annotation)
	fmt.Printf("New Annotation: %s \n", annotation)

	if dryRun {
		fmt.Println("Skipping time travel due to dry run...")
	} else {
		dynamoChange := make(map[string]*dynamodb.AttributeValueUpdate)
		dynamoChange["time_of_event"] = &dynamodb.AttributeValueUpdate{
			Value: &dynamodb.AttributeValue{
				N: aws.String(strconv.FormatInt(destinationTime.UnixNano(), 10)),
			},
		}
		dynamoChange["last_updated"] = &dynamodb.AttributeValueUpdate{
			Value: &dynamodb.AttributeValue{
				N: aws.String(strconv.FormatInt(destinationTime.UnixNano(), 10)),
			},
		}
		dynamoChange["annotation"] = &dynamodb.AttributeValueUpdate{
			Value: &dynamodb.AttributeValue{
				S: aws.String(annotation),
			},
		}
		payday.UpdateTransaction(dynamoClient, table, inputTransaction, dynamoChange)
		fmt.Println("Successfully sent transaction forward in time...")
	}
}
