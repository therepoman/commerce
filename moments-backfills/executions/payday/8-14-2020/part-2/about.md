## Overview
After previous attempts to fix this user's transaction ordering they still remain in a bad state.
This is particularly frustrating because our various sources of data on the user's transactions disagree (pachinko vs payday vs prometheus).
This particular reovke transaction is causing significant problems.
When applied in the context of the transaction history their balance goes negative breaking pacther spend order.
However, we cannot delete this transaction entirely (or zero it out) since then the user's balance will then not match with pachinko.
To avoid spending any more time on this issue, I am just pulling this particular problematic transaction back to the present.
I've confirmed in separate simulations that this will definitively fix this user.

## Pre-Backfill State
User has a revoke transaction from August 2018 of -70 bits that causes them to go negative when replaying transactions in order

## This Backfill
User's revoke transaction now occurs at the end of their transaction history.
Their balance never goes negative and their pachinko wallet matches with the payday transactions total.

## Status
- I ran this script on 8/14/2020 at 12:53pm - @seph
- After running, I confirmed the transaction is in the correct state