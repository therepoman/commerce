## Overview
When attempting to replay this particular user's payday transactions in order, their balance at one point becomes negative.
This is because the timestamps show this user used their bits before they were ever acquired.
This is, of course, a temporal paradox. Bits cannot be used before they have been acquired.
Doing so violates causality and breaks the space time continuum.

## Timeline
This timeline demonstrates the relevant portion of the user's transactions in the pre-backfill paradoxical state.
The user starts at 15 bits and then transactions are applied in order based on the transaction time.
When TX2 is applied 10 bits are deducted and their balance becomes negative (-5). This is because these bits weren't
actually acquired until two minutes later in TX3. This diagarm further shows how this backfill will send TX3 back
in time by about 2 minutes to correct this paradox.

```
2020-07-30 23:58:19:   =15   Current Balance   
2020-07-31 14:50:46:   -10   TX1 (3d636f05-d2e2-4718-967a-9ef217fe5119)
2020-07-31 14:50:46:    =5   Current Balance                                                      
2020-07-31 17:20:00:         [TX3_TIME_TRAVEL_DESTINATION] ← ← ← ← ← ← ← ← ← ← ← ← ← ← ← ← ← ← ← ← ← ← ← ← ← ← ←
2020-07-31 17:21:30:   -10   TX2 (3d6986ef-1196-496b-9c64-39b74c55dc53)                                          ↖
2020-07-31 17:21:30:   =-5   Current Balance ** BALANCE BECAME NEGATIVE **                                       ↑
2020-07-31 17:22:06: +1500   TX 3 (amzn1.twitch.payments.order.09b8d5d1-1c8a-43c1-9098-d0fcb89989e3) → → → → → ↗
2020-07-31 17:22:06: =1495   Current Balance
2020-07-31 17:25:08:   -10   TX 4 (afa18c7a-611a-4bcb-a82a-6be22615514b)
2020-07-31 17:25:08: =1485   Current Balance
```

## Pre-Backfill State
This user spent some of their bits before they ever acquired them

## This Backfill
Sends one particular bits acquired transaction back in time by about two minutes to resolve a temporal paradox

## Status
- I ran this script on 8/14/2020 at 11:05am
- After running, I replayed the users transactions and confirmed there were no longer any violations of causality
- The timestream is safe
