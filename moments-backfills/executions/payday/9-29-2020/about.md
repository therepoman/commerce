## Overview
We needed to backfill failed entitlements again to award streamers with their own emotes.

## Pre-Backfill State
- Some streamers who have uploaded BTER emotes could not use them themselves

## This Backfill
The script is from Payday's [BTER Backfill Script](https://git.xarth.tv/commerce/payday/tree/master/cmd/emote_rewards_backfill)

This script does the following:
- get all Badge Tier Emote Groups
- publishes these groups to SNS topic which is subscribed to by a SQS queue
- SQS queue handler takes these messages, queries [Pantheon](https://git.xarth.tv/commerce/pantheon) to get all users elliglbe for this group
    - If the group has already been backfilled, the SQS handler returns immediately
- SQS handler requeues these messages
- The second time it gets it, it tries to call [Mako](https://git.xarth.tv/commerce/mako) 's `CreateGroupEntitlements` endpoint, because Mako is not a service we own, we rate limit on this.

## Additional Notes
- I ran the script remotely on a Payday Staging EC2 instance (Inside Payday VPC).

## Status
@hpokuan
- I ran the dryrun of this backfill at ~12:00pm on 9/28/2020.<br>
  [execution log](./remote_progress/production_dryrun_1601320577.log) |
  
- I started the full production run at ~1:00pm on 9/28/2020. It finished the next day at around 4:45pm.<br>
  [execution log](./remote_progress/production_1601323553.log) |
  [progress file](./remote_progress/progress_production.csv)

