package main

import (
	"context"
	"fmt"
	"os"
	"regexp"
	"strings"
	"time"

	"code.justin.tv/commerce/moments-backfills/utils/redis"
	"code.justin.tv/commerce/moments-backfills/utils/redis/pantheon"
)

const (
	scanCount  = 5000
	maxRetries = 5

	// When this is true, prints plan changes without actually applying them
	dryRun = true

	// When this is true, output is logged to a file as well as the console
	shouldWriteToFile = true
)

var domains = strings.Join([]string{"bits-usage-by-channel-v1", "sub-gifts-sent", "polls", "trains", "cogo"}, "|")
var timeUnits = strings.Join([]string{"ALLTIME", "YEAR", "MONTH", "WEEK", "DAY"}, "|")

// match leaderboard keys that are of the form <domain>/<groupingKey>/<timeUnit>/<timeBucket>
var regex = fmt.Sprintf(`(%s)\/.*\/(%s)\/\d+$`, domains, timeUnits)

func main() {
	var f *os.File
	if shouldWriteToFile {
		var err error
		f, err = createExecutionLog()
		if err != nil {
			panic(err)
		}
		defer f.Close()
	}

	leaderboardRegex := regexp.MustCompile(regex)
	redisClient := redis.NewClient("localhost:6379")
	cursor := uint64(0)
	start := time.Now()

	for {
		retryCount := 0
		var results []string
		var newCursor uint64
		var err error

		for retryCount < maxRetries {
			printString(f, fmt.Sprintf("Starting scan with cursor: %d\n", cursor))

			// scan through Redis cluster
			results, newCursor, err = pantheon.Scan(context.Background(), redisClient, cursor, "", scanCount)
			if err != nil {
				// if we get tcp timeout, sleep and then retry
				if strings.Contains(err.Error(), "timeout") {
					printString(f, fmt.Sprintf("TCP Timeout with error: %s, retrying...\n\n", err.Error()))
					retryCount++
					time.Sleep(1 * time.Second)
				} else {
					panic(err)
				}
			} else {
				break
			}
		}

		if retryCount >= maxRetries {
			panic("Too many retries for scan, exiting")
		}

		count := 0
		pipeline := pantheon.Pipeline(context.Background(), redisClient)

		for _, value := range results {
			// find leaderboards by matching key to regex
			if leaderboardRegex.MatchString(value) {
				id := pantheon.IdFromKey(value)
				index := fmt.Sprintf("%s-%s-%s", pantheon.GroupingKeyIndexPrefix, id.Domain, id.GroupingKey)
				printString(f, fmt.Sprintf("Updating GroupingKey Index at: %s with: %s\n", index, value))
				count++
				// Update grouping key index with leaderboard key
				if !dryRun {
					_, err = pantheon.SAdd(pipeline, index, value)
					if err != nil {
						panic(err)
					}
				}
			} else {
				printString(f, fmt.Sprintf("NOT updating GroupingKey Index with: %s\n", value))
			}
		}

		if dryRun {
			printString(f, fmt.Sprintf("Skipped adding %d keys due to dry run...\n\n", count))
		} else {
			retryCount = 0
			for retryCount < maxRetries {
				printString(f, fmt.Sprintf("Adding %d keys \n\n", count))
				_, err = pipeline.Exec()

				if err != nil {
					// if we get tcp timeout, sleep and then retry
					if strings.Contains(err.Error(), "timeout") {
						printString(f, fmt.Sprintf("TCP Timeout with error: %s, retrying...\n\n", err.Error()))
						retryCount++
						time.Sleep(1 * time.Second)
					} else {
						panic(err)
					}
				} else {
					break
				}
			}

			if retryCount >= maxRetries {
				panic("Too many retries for Exec, exiting")
			}

			printString(f, fmt.Sprintf("Successfully updated...\n\n"))
		}

		err = pantheon.Close(pipeline)
		if err != nil {
			panic(err)
		}

		cursor = newCursor
		if cursor == 0 {
			break
		}

		// sleep so we don't block other Redis operations for too long
		time.Sleep(100 * time.Millisecond)
	}

	printString(f, fmt.Sprintf("Done in %f seconds!\n", time.Since(start).Seconds()))
}

func createExecutionLog() (*os.File, error) {
	// create execution directory
	executionDateDir := time.Now().Format("01-02-2006")
	if _, err := os.Stat(executionDateDir); os.IsNotExist(err) {
		os.Mkdir(executionDateDir, 0755)
	}

	// create log file
	outputFile := "execution.log"
	if dryRun {
		outputFile = "dry_run_" + outputFile
	}
	return os.Create(fmt.Sprintf("%s/%s", executionDateDir, outputFile))
}

func printString(f *os.File, s string) {
	fmt.Printf(s) // output to console
	if shouldWriteToFile {
		_, err := f.WriteString(s) // output to file
		if err != nil {
			panic(err)
		}
	}
}
