## Overview
During the PublishEvent ingestion flow in Pantheon a new index was created for `grouping_key` -> `set of leaderboard keys in Redis`. 
This is updating all new entries in leaderboards, but we need to backfill the index for all existing leaderboards in Redis.

## This Backfill
Scans the entire Redis cluster looking for leaderboards and adds them to the appropriate GroupingKey Index in Redis

## How to run
To run against Pantheon, you must first create a ssh tunnel to the Pantheon VPC, and forward localhost:6379 to the redis host.
Ensure you are on TwitchVPN since Pantheon is on the network backbone.
```
ssh -f -N -L 6379:<redis_host>:6379 <pantheon_host>
```

The `<pantheon_host>` should be the Private DNS of one of the EC2 instances.
Example for Pantheon staging: `ip-10-201-181-142.us-west-2.compute.internal` 

Find the correct Redis host below (or in the ElasticCache section of the appropriate Pantheon AWS account).

```
// default cluster (includes hypetrains)
stagingPrimaryNode = "pantheon-staging.u7cqeb.ng.0001.usw2.cache.amazonaws.com"
prodPrimaryNode    = "pantheon-prod.45mvzb.ng.0001.usw2.cache.amazonaws.com"

// cogo cluster
stagingPrimaryNode = "cogo-staging.u7cqeb.ng.0001.usw2.cache.amazonaws.com"
prodPrimaryNode    = "cogo-prod.45mvzb.ng.0001.usw2.cache.amazonaws.com"

// polls cluster
stagingPrimaryNode = "polls-staging.u7cqeb.ng.0001.usw2.cache.amazonaws.com"
prodPrimaryNode    = "polls-prod.45mvzb.ng.0001.usw2.cache.amazonaws.com"
```

Then, from `/grouping_key_index_backfill`: 
```
go run main.go
```

After running the script, if you want to kill the tunnel, use `ps aux | grep 6379` to find the PID, then `kill -9 PID`. 

## Previous Issues
We decided to try scanning the entire Redis cluster whenever someone requested deletion, but that caused significant slowdowns for Redis read/write access.
Creating this index means we only need to scan once to backfill it.  

## Pre-Backfill State
Grouping Key Index has been created in Redis and is updating the index for all leaderboard updates during `PublishEvent` ingestion. 
 
## Status
- 08-24-2020 Completed a dry run for Pantheon Staging. Sample output below:
```
Updating GroupingKey Index at: grouping_key_index-bits-usage-by-channel-v1-49849247 with: bits-usage-by-channel-v1/49849247/DAY/1498460400000000000
Updating GroupingKey Index at: grouping_key_index-bits-usage-by-channel-v1-48286022 with: bits-usage-by-channel-v1/48286022/DAY/1515830400000000000
Updating GroupingKey Index at: grouping_key_index-bits-usage-by-channel-v1-21841789 with: bits-usage-by-channel-v1/21841789/DAY/1484899200000000000
Updating GroupingKey Index at: grouping_key_index-bits-usage-by-channel-v1-110149947 with: bits-usage-by-channel-v1/110149947/DAY/1493622000000000000
Updating GroupingKey Index at: grouping_key_index-bits-usage-by-channel-v1-56904017 with: bits-usage-by-channel-v1/56904017/WEEK/1495436400000000000
Adding 118 keys

Successfully updated...

Done in 428.022630 seconds!
```
- 08-26-2020 Completed successful run for cogo, polls, and main prod Redis clusters.
