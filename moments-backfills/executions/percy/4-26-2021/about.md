## Overview
https://jira.xarth.tv/browse/MOMENTS-2071

[Mode query to get Trains to backfill](https://app.mode.com/twitch/reports/654b2359a87e/runs/a8a039ea0141/details/queries/d7df7cfeca15/data)

Requirement:
We need to backfill badges between 2/8 (since [this PR](https://git.xarth.tv/commerce/percy/pull/458) was [deployed to canary](https://deploy.xarth.tv/#/commerce/percy/deploys/270445)) and 4/12 (when [this was fixed](https://git.xarth.tv/commerce/percy/pull/512) and [deployed to production](https://deploy.xarth.tv/#/commerce/percy/deploys/275753))

code logic for each train
```
for each train:
    if train succeeded (gone past level 1):
        if train is most recent train:
            update conductor table from most recent train.
                grant conductors current conductor badge
        else:
            for each participation source (bits, subs)
                if conductor is not the same as most recent train
                (this check is needed b/c we don't want to override a current conductor's badge)
                    grant former conductor badge
                else:
                    no actions, b/c if the train didn't succeed it would not grant badges
```

[PR for the script](https://git.xarth.tv/commerce/percy/pull/519)

## Pre-Backfill State
Users who were Hypetrain conductors between 2/8 and 4/12 may not have been granted former conductor badge.

## This Backfill
Makes sure that conductors for Hypetrains during this period get their former conductor badge. If they are also the conductor for the most recent Hypetrain in a channel, they are granted current conductor badge

## Status
- Dry run on: 4/22/20 @ 1:11PM
- Full execution on: 4/26/20 @ 10:54AM, took 3h26m to finish