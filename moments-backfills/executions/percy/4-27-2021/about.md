## Overview
https://jira.xarth.tv/browse/MOMENTS-2071

Turns out the [original query](https://app.mode.com/twitch/reports/654b2359a87e/runs/a8a039ea0141/details/queries/d7df7cfeca15/data) would not fetch last days data (because "2021-04-12 00:00:01" > "2021-04-12"), so we are running the backfill again w/ [missed data](https://app.mode.com/twitch/reports/654b2359a87e/details/queries/02581bf1c2c8/data) (with buffers on left/right side of date range)

## Pre-Backfill State
Users who were Hypetrain conductors on 4/12 may not have been granted former conductor badge.

## This Backfill
Makes sure that conductors for Hypetrains during this period get their former conductor badge. If they are also the conductor for the most recent Hypetrain in a channel, they are granted current conductor badge

## Status
- Dry run on: 4/27/20 09:59AM
- Full execution on: 4/27/20 @ 10:25AM, took 20m to finish