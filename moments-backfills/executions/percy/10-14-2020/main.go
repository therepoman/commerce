package main

import (
	"fmt"
	"time"

	"code.justin.tv/commerce/moments-backfills/utils/dynamo"
	"code.justin.tv/commerce/moments-backfills/utils/dynamo/percy"
	"code.justin.tv/commerce/moments-backfills/utils/env"
	"code.justin.tv/commerce/moments-backfills/utils/io"
	"github.com/aws/aws-sdk-go/aws"
)

const (
	awsProfile  = "twitch-percy-aws-prod"
	awsRegion   = "us-west-2"
	configTable = "celebrations-channel-purchasable-configs-production"
	filePath    = "executions/percy/10-14-2020/channels.csv"
	dryRun      = true // skip updating the emote totals
)

var configDefaults = map[string]map[percy.CelebrationSize]*percy.CelebrationPurchaseConfig{
	"A": {
		percy.CelebrationSizeSmall: {
			IsDisabled: aws.Bool(false),
			OfferID:    aws.String("amzn1.twitch.commerce.offer.d6360099-1241-4cd3-816f-ad1bc8ee0790"),
		},
		percy.CelebrationSizeMedium: {
			IsDisabled: aws.Bool(false),
			OfferID:    aws.String("amzn1.twitch.commerce.offer.e9bd1583-0f1b-499f-ac2d-acff14dd69ef"),
		},
		percy.CelebrationSizeLarge: {
			IsDisabled: aws.Bool(false),
			OfferID:    aws.String("amzn1.twitch.commerce.offer.9e485603-c0c7-4b7c-85f4-6b85d2f0070c"),
		},
	},
	"B": {
		percy.CelebrationSizeSmall: {
			IsDisabled: aws.Bool(false),
			OfferID:    aws.String("amzn1.twitch.commerce.offer.e9bd1583-0f1b-499f-ac2d-acff14dd69ef"),
		},
		percy.CelebrationSizeMedium: {
			IsDisabled: aws.Bool(false),
			OfferID:    aws.String("amzn1.twitch.commerce.offer.9e485603-c0c7-4b7c-85f4-6b85d2f0070c"),
		},
		percy.CelebrationSizeLarge: {
			IsDisabled: aws.Bool(false),
			OfferID:    aws.String("amzn1.twitch.commerce.offer.2be7b698-b1ca-4b1b-9ccc-81ae5993a91b"),
		},
	},
	"C": {
		percy.CelebrationSizeSmall: {
			IsDisabled: aws.Bool(false),
			OfferID:    aws.String("amzn1.twitch.commerce.offer.9e485603-c0c7-4b7c-85f4-6b85d2f0070c"),
		},
		percy.CelebrationSizeMedium: {
			IsDisabled: aws.Bool(false),
			OfferID:    aws.String("amzn1.twitch.commerce.offer.2be7b698-b1ca-4b1b-9ccc-81ae5993a91b"),
		},
		percy.CelebrationSizeLarge: {
			IsDisabled: aws.Bool(false),
			OfferID:    aws.String("amzn1.twitch.commerce.offer.baec82f1-e979-4566-baac-e85cc0ed301d"),
		},
	},
}

func main() {
	env.SetAWSProfile(awsProfile)
	env.SetAWSRegion(awsRegion)
	dc := dynamo.NewClient()

	rows := io.ReadCSV(filePath)
	for _, row := range rows {
		channelID := row[0]
		group := row[1]

		config, ok := configDefaults[group]
		if !ok {
			panic(fmt.Errorf("no default config for group %s", group))
		}

		fmt.Printf("ChannelID: %s \n", channelID)
		fmt.Printf("Group: %v \n", group)

		if dryRun {
			fmt.Println("Skip update due to dry run...")
		} else {
			percy.UpdateCelebrationPurchaseConfig(dc, configTable, percy.CelebrationChannelPurchasableConfigRecord{
				ChannelID:      channelID,
				LastUpdated:    aws.Time(time.Now()),
				PurchaseConfig: config,
			})
			fmt.Println("Successfully put celebration config")
		}

		fmt.Println("")
	}

}
