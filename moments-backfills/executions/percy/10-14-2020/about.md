## Overview
Leading up to the purchasable celebrations experiment, we want to assign reasonable defaults to the experiment participants (e.g. lower CCU channels should show lower priced celebrations by default)

## Pre-Backfill State
Users in experiment had no custom configuration for purchasable celebrations

## This Backfill
Adds channel-specific purchasable celebration configuration based on buckets provided by PM (@Blake)

## Status
- Dry run on: 10/14/20 @ 1:10PM
- Full execution on: 10/15/20 @ 7:45AM
