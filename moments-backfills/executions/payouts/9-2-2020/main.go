package main

import (
	"fmt"
	"math"
	"strconv"

	"code.justin.tv/commerce/moments-backfills/utils/io"
)

const (
	actualPayouts        = "executions/payouts/9-2-2020/2020-07-01_actual_payouts.csv"
	expectedPayouts      = "executions/payouts/9-2-2020/2020-07-01_expected_payouts.csv"
	acceptableFloatDelta = 0.000001

	bitsMakeGood = "executions/payouts/9-2-2020/2020-07_bits_makegood.csv"
	devMakeGood  = "executions/payouts/9-2-2020/2020-07_dev_makegood.csv"
	extMakeGood  = "executions/payouts/9-2-2020/2020-07_ext_makegood.csv"
)

func main() {
	actualPayoutsRows := io.ReadCSV(actualPayouts)
	actualBitsPayoutsByChannel, actualBitsDevPayoutsByChannel, actualBitsExtPayoutsByChannel := revenueCSVtoMaps(actualPayoutsRows)

	expectedPayoutsRows := io.ReadCSV(expectedPayouts)
	expectedBitsPayoutsByChannel, expectedBitsDevPayoutsByChannel, expectedBitsExtPayoutsByChannel := revenueCSVtoMaps(expectedPayoutsRows)

	allChannels := getAllChannels(actualBitsPayoutsByChannel, actualBitsDevPayoutsByChannel, actualBitsExtPayoutsByChannel,
		expectedBitsPayoutsByChannel, expectedBitsDevPayoutsByChannel, expectedBitsExtPayoutsByChannel)

	totalBitsDelta := float64(0)
	totalDevDelta := float64(0)
	totalExtDelta := float64(0)

	bitsFile, bitsCSV := io.CreateCSV(bitsMakeGood)
	defer bitsCSV.Flush()
	defer bitsFile.Close()

	devFile, devCSV := io.CreateCSV(devMakeGood)
	defer devCSV.Flush()
	defer devFile.Close()

	extFile, extCSV := io.CreateCSV(extMakeGood)
	defer extCSV.Flush()
	defer extFile.Close()

	for channelID := range allChannels {
		actualBitsPayout := actualBitsPayoutsByChannel[channelID]
		actualBitsDevPayout := actualBitsDevPayoutsByChannel[channelID]
		actualBitsExtPayout := actualBitsExtPayoutsByChannel[channelID]

		expectedBitsPayout := expectedBitsPayoutsByChannel[channelID]
		expectedBitsDevPayout := expectedBitsDevPayoutsByChannel[channelID]
		expectedBitsExtPayout := expectedBitsExtPayoutsByChannel[channelID]

		bitsDelta := expectedBitsPayout - actualBitsPayout
		bitsDevDelta := expectedBitsDevPayout - actualBitsDevPayout
		bitsExtDelta := expectedBitsExtPayout - actualBitsExtPayout

		if !floatEqual(bitsDelta, 0) || !floatEqual(bitsDevDelta, 0) || !floatEqual(bitsExtDelta, 0) {
			fmt.Printf("%s :: bits : %.2f , dev : %.2f , ext : %.2f \n", channelID, bitsDelta, bitsDevDelta, bitsExtDelta)
		}

		if !floatEqual(bitsDelta, 0) {
			err := bitsCSV.Write([]string{channelID, fmt.Sprintf("%.2f", bitsDelta)})
			if err != nil {
				panic(err)
			}
			totalBitsDelta += bitsDelta
		}

		if !floatEqual(bitsDevDelta, 0) {
			err := bitsCSV.Write([]string{channelID, fmt.Sprintf("%.2f", bitsDevDelta)})
			if err != nil {
				panic(err)
			}
			totalBitsDelta += bitsDevDelta
		}

		if !floatEqual(bitsExtDelta, 0) {
			err := bitsCSV.Write([]string{channelID, fmt.Sprintf("%.2f", bitsExtDelta)})
			if err != nil {
				panic(err)
			}
			totalBitsDelta += bitsExtDelta
		}
	}

	fmt.Printf("Total Bits: %.2f \n", totalBitsDelta)
	fmt.Printf("Total Dev: %.2f \n", totalDevDelta)
	fmt.Printf("Total Ext: %.2f \n", totalExtDelta)

	bitsCSV.Flush()
	err := bitsFile.Close()
	if err != nil {
		panic(err)
	}

	devCSV.Flush()
	err = devFile.Close()
	if err != nil {
		panic(err)
	}

	extCSV.Flush()
	err = extFile.Close()
	if err != nil {
		panic(err)
	}
}

func floatEqual(a, b float64) bool {
	return math.Abs(a-b) < acceptableFloatDelta
}

func revenueCSVtoMaps(rows [][]string) (map[string]float64, map[string]float64, map[string]float64) {
	bitsPayoutsByChannel := make(map[string]float64)
	bitsDevPayoutsByChannel := make(map[string]float64)
	bitsExtPayoutsByChannel := make(map[string]float64)
	for i, row := range rows {
		if i == 0 {
			continue
		}
		channelID := row[0]
		bitsShareStr := row[4]
		bitsShare, err := strconv.ParseFloat(bitsShareStr, 64)
		if err != nil {
			panic(err)
		}

		bitsDevShareStr := row[5]
		bitsDevShare, err := strconv.ParseFloat(bitsDevShareStr, 64)
		if err != nil {
			panic(err)
		}

		bitsExtShareStr := row[6]
		bitsExtShare, err := strconv.ParseFloat(bitsExtShareStr, 64)
		if err != nil {
			panic(err)
		}

		bitsPayoutsByChannel[channelID] += bitsShare
		bitsDevPayoutsByChannel[channelID] += bitsDevShare
		bitsExtPayoutsByChannel[channelID] += bitsExtShare
	}
	return bitsPayoutsByChannel, bitsDevPayoutsByChannel, bitsExtPayoutsByChannel
}

func getAllChannels(maps ...map[string]float64) map[string]interface{} {
	allChannels := make(map[string]interface{})
	for _, thisMap := range maps {
		for channelID := range thisMap {
			allChannels[channelID] = nil
		}
	}
	return allChannels
}
