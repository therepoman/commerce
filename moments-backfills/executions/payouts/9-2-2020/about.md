## Overview
Due to various problems, several channels were underpaid in July 2020.
To fix this, we will be paying them out with what was missed in September 2020.
This script calculates the "makegood" payout by taking the delta of the current expected payout report
and the older payout report that was actually used by accounting for the July 2020 payouts.

## Pre-Backfill State
Exactly 6,659 channels were underpaid by a total of $18,243.91 in July 2020.

## This Backfill
Calculates the required "makegood" payouts that will be included in September 2020 payouts.
We will pass this data on to finance/accounting.

## Status
I ran this make good calculation at ~2:10pm on 9/2/2020 - @seph