## Overview
Several pachter finalize/release holds transactions are stuck in the audit dlq due to missing spend worker timestamps.
This is because they were processed before we added the logic to properly skip them in the spend worker and add the audit spend timestamp to dynamo.

## Pre-Backfill State
Audit records missing a spend timestamp

## This Backfill
Update the audit records to now have a spend timestamp that is identical to the transactions timetamp

## Status
- I ran this backfill at ~7:25pm on 8/17-2020 - @seph
- Subsequent dry-run runs show no changes
