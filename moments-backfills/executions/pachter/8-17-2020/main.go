package main

import (
	"fmt"
	"time"

	"code.justin.tv/commerce/moments-backfills/utils/dynamo"
	"code.justin.tv/commerce/moments-backfills/utils/dynamo/pachter"
	"code.justin.tv/commerce/moments-backfills/utils/env"
	"code.justin.tv/commerce/moments-backfills/utils/io"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	awsProfile                  = "pachter-prod"
	awsRegion                   = "us-west-2"
	pachterAuditRecordTableName = "worker-audit-prod"
	filePath                    = "executions/pachter/8-17-2020-fix/transaction_ids.csv"
	dryRun                      = true // skip updating the pachter audit records
)

func main() {
	env.SetAWSProfile(awsProfile)
	env.SetAWSRegion(awsRegion)
	dc := dynamo.NewClient()

	rows := io.ReadCSV(filePath)

	for _, row := range rows {
		txID := row[0]
		auditRecord := pachter.GetAuditRecord(dc, pachterAuditRecordTableName, txID)
		fmt.Println(txID)
		fmt.Printf("Transaction Time: %s \n", auditRecord.Transactions)
		fmt.Printf("Spend Time: %s \n", auditRecord.Spend)

		// if the transactions timestamp is missing, update it
		if auditRecord.Transactions.IsZero() {
			panic(fmt.Sprintf("transactions timestamp missing from audit record for: %s", txID))
		}

		// if the spend timestamp is missing, update it from the transactions timestamp
		if auditRecord.Spend.IsZero() {
			fmt.Println("Spend timestamp is missing")
			if dryRun {
				fmt.Println("Skipping spend timestamp update due to dry run...")
			} else {
				dynamoChange := make(map[string]*dynamodb.AttributeValueUpdate)
				dynamoChange["spend"] = &dynamodb.AttributeValueUpdate{
					Value: &dynamodb.AttributeValue{
						S: aws.String(auditRecord.Transactions.Format(time.RFC3339)),
					},
				}
				pachter.UpdateAuditRecord(dc, pachterAuditRecordTableName, txID, dynamoChange)
				fmt.Println("Successfully updated spend timestamp...")
			}
		} else {
			fmt.Println("Spend timestamp is present")
		}

		fmt.Println()
	}
}
