BEGIN;
UPDATE bits_transactions
SET quantity =
    CASE
        WHEN transaction_id = '4dc4c5cfe40a36aa5f5dcb4ac1b81bed' AND quantity = 100 THEN 200
        WHEN transaction_id = '466266510' AND quantity = 100 THEN 200
        WHEN transaction_id = '463728345' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dabc2fcae027d506e3fd04bc1bf679b' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dd071446e418ee30fd27845a381e478' AND quantity = 100 THEN 300
        WHEN transaction_id = '4d9e75106e14348e615a0d4e45adb168' AND quantity = 100 THEN 200
        WHEN transaction_id = '4db49be0bb0f4b0a24601a4e058ecadb' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dd7c3688a7de88de93fbf4a699f87e7' AND quantity = 500 THEN 2000
        WHEN transaction_id = '4e2e769c61282e6dce4a5d4d06b0a699' AND quantity = 100 THEN 200
        WHEN transaction_id = '462617176' AND quantity = 500 THEN 1000
        WHEN transaction_id = '470873465' AND quantity = 100 THEN 200
        WHEN transaction_id = '472245946' AND quantity = 100 THEN 200
        WHEN transaction_id = '4db43749eae4c3578dd1f1461ea53042' AND quantity = 500 THEN 1000
        WHEN transaction_id = '467237099' AND quantity = 100 THEN 300
        WHEN transaction_id = '471051770' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d90c59ac2a49ff8071a9942798d12be' AND quantity = 500 THEN 1000
        WHEN transaction_id = '462922280' AND quantity = 100 THEN 200
        WHEN transaction_id = '462759219' AND quantity = 100 THEN 400
        WHEN transaction_id = '4d9a38b2b2b132566fdf6749c2aec6ce' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dd266bb5a90328f137bb242c28d2cbf' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dd98095643daf3bdc80ee454f9de82e' AND quantity = 100 THEN 300
        WHEN transaction_id = '469870504' AND quantity = 100 THEN 300
        WHEN transaction_id = '472422055' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04836748' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04793197' AND quantity = 100 THEN 200
        WHEN transaction_id = '463052309' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04803262' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4da0012caa0e8b9eb5e2a44369980ada' AND quantity = 100 THEN 1000
        WHEN transaction_id = 'A-S04735497' AND quantity = 100 THEN 200
        WHEN transaction_id = '4de1797ed5ab803da382e44f82825cbe' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04870039' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4dec2b0afc2a7e2fe7232f4bdaa25d7f' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04895122' AND quantity = 100 THEN 1000
        WHEN transaction_id = 'A-S04908640' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04914976' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04740194' AND quantity = 1500 THEN 9000
        WHEN transaction_id = '462903442' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4d8edc93a349cf5613fd5048f7bda710' AND quantity = 100 THEN 400
        WHEN transaction_id = '470472622' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04957651' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04968388' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04759087' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04759478' AND quantity = 1500 THEN 9000
        WHEN transaction_id = 'A-S04795714' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04804917' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04877402' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04857845' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04867088' AND quantity = 100 THEN 800
        WHEN transaction_id = 'A-S04782266' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04768817' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04815952' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d9bf54dd8ec7bc90d1b654a83b56205' AND quantity = 100 THEN 200
        WHEN transaction_id = '471017049' AND quantity = 100 THEN 300
        WHEN transaction_id = '472394382' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4e0616f57ea86590716a3a446bb59376' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4e3ccbf8b50642b93ac9184171869602' AND quantity = 100 THEN 200
        WHEN transaction_id = '469853621' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e3f076ece13ec3eea32764f68b0d207' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04948255' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04954534' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04881669' AND quantity = 100 THEN 200
        WHEN transaction_id = '468726314' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S05013774' AND quantity = 100 THEN 200
        WHEN transaction_id = '462732063' AND quantity = 100 THEN 200
        WHEN transaction_id = '470543339' AND quantity = 500 THEN 1000
        WHEN transaction_id = '466596929' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04846209' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04758650' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04810619' AND quantity = 100 THEN 300
        WHEN transaction_id = '466769386' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04885680' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04903396' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04907282' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04962073' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04971107' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '471519841' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '467295990' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04883400' AND quantity = 100 THEN 200
        WHEN transaction_id = '472792489' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04848733' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e069af9b7babccac1dd4c4f34b0064c' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04893491' AND quantity = 100 THEN 200
        WHEN transaction_id = '4db289011d14c8a1111d514a94b91b97' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4dd3a6683ee9e5e295bfb14342a1bf79' AND quantity = 100 THEN 200
        WHEN transaction_id = '465118777' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04730407' AND quantity = 500 THEN 1000
        WHEN transaction_id = '464673800' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04832204' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04728263' AND quantity = 100 THEN 500
        WHEN transaction_id = 'A-S04792960' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04945107' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04930445' AND quantity = 100 THEN 200
        WHEN transaction_id = '464578024' AND quantity = 100 THEN 300
        WHEN transaction_id = '466625284' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04932494' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04957533' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04880544' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04854258' AND quantity = 100 THEN 200
        WHEN transaction_id = '464837426' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '469282687' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '469891703' AND quantity = 100 THEN 200
        WHEN transaction_id = '4df4415ead8a766c2a59e44a84b0e0ef' AND quantity = 1500 THEN 7500
        WHEN transaction_id = 'A-S04795896' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S05010257' AND quantity = 100 THEN 200
        WHEN transaction_id = '464787873' AND quantity = 100 THEN 300
        WHEN transaction_id = '467483243' AND quantity = 100 THEN 300
        WHEN transaction_id = '4df0a5389a7cfb4fa45b784164bb3d93' AND quantity = 100 THEN 300
        WHEN transaction_id = '4e06b5fe43851d06221b224e5092c170' AND quantity = 100 THEN 300
        WHEN transaction_id = '466606174' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4de8366a392b1c5161e6f24369a35c8f' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S05000454' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S05001293' AND quantity = 100 THEN 1000
        WHEN transaction_id = '4de16a10b0aee7907b29024af2b5174b' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04820780' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04833828' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04999044' AND quantity = 100 THEN 300
        WHEN transaction_id = '4dd388fb3b997a49bf929040e5b60933' AND quantity = 100 THEN 300
        WHEN transaction_id = '4da45324f80c869152162f4c74a4e66a' AND quantity = 100 THEN 300
        WHEN transaction_id = '465281442' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4e155ca7595323d8ad3d454342a02204' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04885810' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dd1ebb40991f2d36b4d79467cba5236' AND quantity = 100 THEN 300
        WHEN transaction_id = '464276673' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04804772' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04857678' AND quantity = 500 THEN 1000
        WHEN transaction_id = '468688718' AND quantity = 100 THEN 200
        WHEN transaction_id = '465974258' AND quantity = 100 THEN 300
        WHEN transaction_id = '4de6d4af8c5cef0bcd8f734acab71a39' AND quantity = 100 THEN 400
        WHEN transaction_id = '471610071' AND quantity = 100 THEN 200
        WHEN transaction_id = '466820909' AND quantity = 100 THEN 500
        WHEN transaction_id = '471983264' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04807897' AND quantity = 100 THEN 200
        WHEN transaction_id = '470282334' AND quantity = 100 THEN 200
        WHEN transaction_id = '468116123' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '469721782' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04744127' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04962973' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04931317' AND quantity = 100 THEN 300
        WHEN transaction_id = '466311874' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e2021d50c4f4fadf9cd4c49fbb16262' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S05011745' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e2915db84bb92fbff50664488b3e598' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04745768' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04867997' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04942027' AND quantity = 100 THEN 200
        WHEN transaction_id = '467403919' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04857605' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e246ddb09cf9d43d92a44441ab51c6d' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '471012062' AND quantity = 500 THEN 1000
        WHEN transaction_id = '465275029' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4dc0094072520cb9e77eca4be39daeea' AND quantity = 100 THEN 200
        WHEN transaction_id = '467218645' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04903245' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04988467' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04837751' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04743302' AND quantity = 1500 THEN 9000
        WHEN transaction_id = 'A-S04929602' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04789707' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04803572' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4d840c80595b811bdfd1ae49a4b589ea' AND quantity = 100 THEN 300
        WHEN transaction_id = '4da8fb3a3a2e02abbf21504d2f8918a3' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04877153' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e2a36df85c6003ac8a1b54415aed1d3' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e2b569d0781f609faee4144c89a2ce3' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04835897' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04867273' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04752419' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d8ef8daaffbc59fe04b5e4cbaacb428' AND quantity = 100 THEN 200
        WHEN transaction_id = '4da381bfb1291b40977f1441c5b9e702' AND quantity = 100 THEN 200
        WHEN transaction_id = '466468005' AND quantity = 100 THEN 200
        WHEN transaction_id = '471634944' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04808773' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04887660' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04740312' AND quantity = 100 THEN 200
        WHEN transaction_id = '462969865' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04763010' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04801809' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04805010' AND quantity = 500 THEN 1000
        WHEN transaction_id = '469819514' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04949644' AND quantity = 1500 THEN 6000
        WHEN transaction_id = 'A-S04989002' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04752409' AND quantity = 1500 THEN 9000
        WHEN transaction_id = 'A-S04953637' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d7f910bada598b4df6d6743bebe8977' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04984044' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04740079' AND quantity = 500 THEN 1000
        WHEN transaction_id = '471104396' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d90ba18baaead0ca4df234c42850108' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04808005' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04751912' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04870189' AND quantity = 500 THEN 1500
        WHEN transaction_id = '4d7fcb656dec90b780bc884cfbb12279' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4db252612710f6eda4fedd424982aae2' AND quantity = 100 THEN 200
        WHEN transaction_id = '465789623' AND quantity = 100 THEN 200
        WHEN transaction_id = '465777885' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e3cfa6b81bf60142ce05c4a0a845aa7' AND quantity = 100 THEN 200
        WHEN transaction_id = '4da753f9bb21bebe25d838441bbc669a' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '471871557' AND quantity = 100 THEN 200
        WHEN transaction_id = '466885837' AND quantity = 100 THEN 200
        WHEN transaction_id = '470172745' AND quantity = 100 THEN 200
        WHEN transaction_id = '470699979' AND quantity = 100 THEN 300
        WHEN transaction_id = '470513623' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dcddf54a0f127e5308376445894e4b5' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e393239868b92e076d3204b26889ff0' AND quantity = 1500 THEN 9000
        WHEN transaction_id = '471743236' AND quantity = 500 THEN 2000
        WHEN transaction_id = '4d8b0505d7d90125f8d6154122ba21b8' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04789198' AND quantity = 100 THEN 200
        WHEN transaction_id = '464551884' AND quantity = 100 THEN 200
        WHEN transaction_id = '467906220' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04978031' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e2f1b00dd893abcf9db92423e86914f' AND quantity = 500 THEN 1000
        WHEN transaction_id = '462493849' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d7e641decca9e11d9bf5645ca8070d0' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04732750' AND quantity = 25000 THEN 50000
        WHEN transaction_id = '4d82ef76593a79e433e74b452a810bab' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4d8dc12c710bd226f31e00461ab09c3e' AND quantity = 1500 THEN 9000
        WHEN transaction_id = 'A-S04793026' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04823904' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '467045043' AND quantity = 100 THEN 300
        WHEN transaction_id = '4dd9b84cfa4e4a4af30786433b8d90f5' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04835689' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04891899' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04914385' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04761299' AND quantity = 100 THEN 1000
        WHEN transaction_id = 'A-S04821308' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04825856' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04948129' AND quantity = 100 THEN 400
        WHEN transaction_id = '470654748' AND quantity = 100 THEN 300
        WHEN transaction_id = '471519800' AND quantity = 100 THEN 200
        WHEN transaction_id = '471595485' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4de315994163f33ad5f18042a8a338bf' AND quantity = 100 THEN 300
        WHEN transaction_id = '467428400' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04886328' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04933085' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04973511' AND quantity = 500 THEN 2000
        WHEN transaction_id = '471737501' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '463418234' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04805508' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '472017092' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04998013' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04890681' AND quantity = 1500 THEN 6000
        WHEN transaction_id = 'A-S04999745' AND quantity = 500 THEN 1000
        WHEN transaction_id = '465537534' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e2b800e7e24f8bf53901549a8ad0565' AND quantity = 500 THEN 1500
        WHEN transaction_id = '471930860' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4e38562ac58a9bf104295845c2a505a4' AND quantity = 100 THEN 200
        WHEN transaction_id = '4ddfe38313d75f34eeb0f14ca3802c63' AND quantity = 100 THEN 200
        WHEN transaction_id = '463700737' AND quantity = 100 THEN 300
        WHEN transaction_id = '464051928' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '465469483' AND quantity = 100 THEN 300
        WHEN transaction_id = '4dcaccbe313dc1924126e140f98d261a' AND quantity = 100 THEN 400
        WHEN transaction_id = '466052282' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4dbc6eff467e098ce49c1546df97bc85' AND quantity = 100 THEN 200
        WHEN transaction_id = '466641877' AND quantity = 100 THEN 200
        WHEN transaction_id = '469553708' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e210dc53e0a445b435a4942d99be0dc' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e2b02b1bbb8f88bdda9d6401b89291e' AND quantity = 100 THEN 400
        WHEN transaction_id = '472681598' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04747322' AND quantity = 500 THEN 1000
        WHEN transaction_id = '463347348' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d93fa01ff30eebb509b074ebc8be320' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04756333' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S05014713' AND quantity = 100 THEN 200
        WHEN transaction_id = '471660772' AND quantity = 100 THEN 200
        WHEN transaction_id = '471804582' AND quantity = 1500 THEN 4500
        WHEN transaction_id = 'A-S04766954' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04889876' AND quantity = 1500 THEN 7500
        WHEN transaction_id = 'A-S04988805' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04774635' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04947768' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04867886' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04964800' AND quantity = 500 THEN 1000
        WHEN transaction_id = '462993008' AND quantity = 100 THEN 200
        WHEN transaction_id = '463440781' AND quantity = 1500 THEN 4500
        WHEN transaction_id = 'A-S04896350' AND quantity = 500 THEN 1000
        WHEN transaction_id = '466160101' AND quantity = 500 THEN 1000
        WHEN transaction_id = '466108980' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04847091' AND quantity = 500 THEN 1000
        WHEN transaction_id = '471922236' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04788312' AND quantity = 1500 THEN 7500
        WHEN transaction_id = '471982920' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04879947' AND quantity = 100 THEN 800
        WHEN transaction_id = '4df3bc25fcf944caa88d5e4271ac083d' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04779149' AND quantity = 500 THEN 2000
        WHEN transaction_id = 'A-S04766755' AND quantity = 500 THEN 1000
        WHEN transaction_id = '469057360' AND quantity = 100 THEN 200
        WHEN transaction_id = '467998319' AND quantity = 100 THEN 300
        WHEN transaction_id = '4d820a34430b34d40a37234250ba9b48' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04883811' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04945683' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4df79a80dce4acfc24579c46fa882526' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04919037' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04810977' AND quantity = 100 THEN 300
        WHEN transaction_id = '469049219' AND quantity = 100 THEN 200
        WHEN transaction_id = '4de7a69a67de8198b84eea4f5e8f6b2b' AND quantity = 100 THEN 500
        WHEN transaction_id = 'A-S04909237' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04915790' AND quantity = 100 THEN 200
        WHEN transaction_id = '462776564' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04766233' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04790769' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04889548' AND quantity = 500 THEN 1000
        WHEN transaction_id = '472291121' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04728131' AND quantity = 1500 THEN 6000
        WHEN transaction_id = '4db9a2f3e340b243b4884a44d8a8bcc1' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04816673' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04873528' AND quantity = 1500 THEN 9000
        WHEN transaction_id = '469873081' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04794657' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4dfaf54245f16cec2a4ac54739bb9398' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4da07e5d97f2199adee9b34705a1e89b' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dca08f80de493d18a1d9b4018a53435' AND quantity = 100 THEN 200
        WHEN transaction_id = '462962403' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4d9163e7bf56603f28921c4d1f8d879c' AND quantity = 100 THEN 300
        WHEN transaction_id = '470428305' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04977463' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04980658' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04957097' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04894898' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04895666' AND quantity = 25000 THEN 50000
        WHEN transaction_id = '466390311' AND quantity = 10000 THEN 20000
        WHEN transaction_id = '4e246fc5dddd2bb11c59344014a1f1f4' AND quantity = 500 THEN 1000
        WHEN transaction_id = '471110609' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04744516' AND quantity = 1500 THEN 9000
        WHEN transaction_id = 'A-S04856557' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04737339' AND quantity = 100 THEN 300
        WHEN transaction_id = '467979115' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04811667' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '471751409' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04839688' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e2522386728cf100d1a7c44329ff8d3' AND quantity = 100 THEN 900
        WHEN transaction_id = '4e260f37a8028546decb0a4ff2b273d0' AND quantity = 100 THEN 300
        WHEN transaction_id = '467931992' AND quantity = 500 THEN 1000
        WHEN transaction_id = '462969722' AND quantity = 100 THEN 200
        WHEN transaction_id = '462925037' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04899201' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e0c3fb044a9c8dbdd87354b11bb16d0' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04787131' AND quantity = 100 THEN 500
        WHEN transaction_id = 'A-S04797307' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04880426' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04982601' AND quantity = 100 THEN 200
        WHEN transaction_id = '466238476' AND quantity = 100 THEN 300
        WHEN transaction_id = '470190178' AND quantity = 100 THEN 200
        WHEN transaction_id = '462984153' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e3c938ac04cc4603a10a54d1b9bf2cf' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04986206' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04808378' AND quantity = 500 THEN 1000
        WHEN transaction_id = '469320052' AND quantity = 100 THEN 300
        WHEN transaction_id = '470156363' AND quantity = 100 THEN 200
        WHEN transaction_id = '471590576' AND quantity = 100 THEN 200
        WHEN transaction_id = '470795972' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04793018' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04826396' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04913715' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04816301' AND quantity = 100 THEN 200
        WHEN transaction_id = '470540818' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04879178' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04919371' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04914913' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04982059' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04987471' AND quantity = 100 THEN 200
        WHEN transaction_id = '472046751' AND quantity = 100 THEN 200
        WHEN transaction_id = '462190730' AND quantity = 100 THEN 200
        WHEN transaction_id = '465739808' AND quantity = 100 THEN 200
        WHEN transaction_id = '4ddb213996977a4d37809e4b9e91ec46' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04736388' AND quantity = 1500 THEN 4500
        WHEN transaction_id = 'A-S04840121' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04745079' AND quantity = 1500 THEN 4500
        WHEN transaction_id = 'A-S04785147' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04774207' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04806562' AND quantity = 500 THEN 1000
        WHEN transaction_id = '466019375' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04920678' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04971344' AND quantity = 100 THEN 300
        WHEN transaction_id = '464044050' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '467899646' AND quantity = 500 THEN 1000
        WHEN transaction_id = '468258056' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e1af84cc686419594f02043d497d93e' AND quantity = 100 THEN 300
        WHEN transaction_id = '4dd1d6064de7fae6b916b2497a9cc716' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e3a49e153a61a245e73d84be0af7ec1' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e3ea902269a7cbe273bf04a9498c63d' AND quantity = 100 THEN 300
        WHEN transaction_id = '463409816' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4d951257ef971e708f82874f99a78d43' AND quantity = 100 THEN 300
        WHEN transaction_id = '470105961' AND quantity = 100 THEN 300
        WHEN transaction_id = '4e07256ac2127de04b0df242b4abbe73' AND quantity = 100 THEN 200
        WHEN transaction_id = '464554461' AND quantity = 10000 THEN 20000
        WHEN transaction_id = 'A-S04786851' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04789333' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04827803' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04871393' AND quantity = 5000 THEN 10000
        WHEN transaction_id = 'A-S04908865' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04947443' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04992950' AND quantity = 500 THEN 1000
        WHEN transaction_id = '472785517' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dfc6d785b734446d1e89546aa814b3c' AND quantity = 100 THEN 400
        WHEN transaction_id = '471775849' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '471949004' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04852486' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04937950' AND quantity = 500 THEN 1000
        WHEN transaction_id = '472603698' AND quantity = 100 THEN 300
        WHEN transaction_id = '467904028' AND quantity = 100 THEN 300
        WHEN transaction_id = '467653243' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '463191487' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04834057' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04996663' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04911804' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e05b4d4167a66581dc981489a964d31' AND quantity = 500 THEN 1000
        WHEN transaction_id = '463738424' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d9d800bc0efa6ae4e623a4d219debf7' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '464515648' AND quantity = 100 THEN 500
        WHEN transaction_id = '470597208' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04732923' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04905828' AND quantity = 100 THEN 200
        WHEN transaction_id = '468180043' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e2f4da9182dcd204e16274965868e64' AND quantity = 100 THEN 500
        WHEN transaction_id = '4dbf8a968f9dc413eb0e424fdb857d3f' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04937198' AND quantity = 500 THEN 1000
        WHEN transaction_id = '471644015' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04782412' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dd1e144eb195fc9e55da74cd8b09cc6' AND quantity = 100 THEN 700
        WHEN transaction_id = '466805274' AND quantity = 1500 THEN 6000
        WHEN transaction_id = 'A-S04837339' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04838928' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04773002' AND quantity = 100 THEN 200
        WHEN transaction_id = '470659510' AND quantity = 100 THEN 300
        WHEN transaction_id = '469832478' AND quantity = 100 THEN 300
        WHEN transaction_id = '4e25d2ea90ab37bfa8a2ce4aedadc5e7' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04838211' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04793754' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04804794' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4dbd149e99b18646e35648416093cec5' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04963336' AND quantity = 100 THEN 200
        WHEN transaction_id = '462503468' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '466269396' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e38c7b6b8507eb3e175ca43cd8c3797' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04892242' AND quantity = 500 THEN 1000
        WHEN transaction_id = '471059113' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04955826' AND quantity = 100 THEN 200
        WHEN transaction_id = '4ddebafe0802126eff856041559bd368' AND quantity = 100 THEN 200
        WHEN transaction_id = '4da26db7129b1a8b01a3f8483da8f229' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d819cf4786eda4d90ebab4683a5d022' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e35177f06a9e4cbd13778472a8106df' AND quantity = 500 THEN 1000
        WHEN transaction_id = '465352318' AND quantity = 100 THEN 200
        WHEN transaction_id = '468028641' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e0cd9bc26e46a2cbe11d942b7888344' AND quantity = 100 THEN 200
        WHEN transaction_id = '470856611' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04744020' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04993029' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04789766' AND quantity = 100 THEN 200
        WHEN transaction_id = '4db210425e2077f79caa2c402fa0aa6b' AND quantity = 100 THEN 300
        WHEN transaction_id = '467671193' AND quantity = 100 THEN 200
        WHEN transaction_id = '467618596' AND quantity = 100 THEN 300
        WHEN transaction_id = '469971783' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04815203' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04954472' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4d8c85681ba6d4feeee9d94791874802' AND quantity = 5000 THEN 15000
        WHEN transaction_id = '464486683' AND quantity = 100 THEN 200
        WHEN transaction_id = '464659235' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4db268f3045ea28217165544f3b802f7' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4db5e43bfd59b70ecc70034de593d3b9' AND quantity = 1500 THEN 9000
        WHEN transaction_id = '466350254' AND quantity = 100 THEN 400
        WHEN transaction_id = '467388958' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e23ab9622459d9a2214c4403f9357e5' AND quantity = 100 THEN 200
        WHEN transaction_id = '472398083' AND quantity = 100 THEN 200
        WHEN transaction_id = '468534562' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '465737361' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e1a4de78add7480d35efc4398841fc1' AND quantity = 100 THEN 200
        WHEN transaction_id = '472463822' AND quantity = 100 THEN 200
        WHEN transaction_id = '466697036' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e0ece3cd0475c7c4192ee4adea30880' AND quantity = 500 THEN 1000
        WHEN transaction_id = '470516167' AND quantity = 5000 THEN 10000
        WHEN transaction_id = '464564918' AND quantity = 100 THEN 200
        WHEN transaction_id = '464662772' AND quantity = 100 THEN 400
        WHEN transaction_id = '4d95628ba2e9ba6296114f4e32acc68b' AND quantity = 100 THEN 200
        WHEN transaction_id = '462831242' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '4e197a3eb1b50539785a9143f895ed1d' AND quantity = 100 THEN 200
        WHEN transaction_id = '468364801' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '465745960' AND quantity = 100 THEN 200
        WHEN transaction_id = '465358215' AND quantity = 100 THEN 200
        WHEN transaction_id = '470467955' AND quantity = 100 THEN 300
        WHEN transaction_id = '4d97ea378b881dc3d5ebe6421694ba70' AND quantity = 100 THEN 200
        WHEN transaction_id = '470289602' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '469777962' AND quantity = 100 THEN 300
        WHEN transaction_id = '470241483' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e3e00945cf498f0a3cffe484696f11b' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4da3f7dbac6af0bc6141e74123823068' AND quantity = 100 THEN 300
        WHEN transaction_id = '465053554' AND quantity = 100 THEN 200
        WHEN transaction_id = '466805102' AND quantity = 100 THEN 1000
        WHEN transaction_id = 'B1C0CC62-8C65-4F66-9AEA-A720F708EBA4' AND quantity = 5 THEN 10
        WHEN transaction_id = '4e06ba3288f3ead9820cf244e0ac4da3' AND quantity = 100 THEN 300
        WHEN transaction_id = '470034349' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '463888715' AND quantity = 100 THEN 200
        WHEN transaction_id = '472160420' AND quantity = 100 THEN 200
        WHEN transaction_id = '466857608' AND quantity = 100 THEN 300
        WHEN transaction_id = '468248741' AND quantity = 500 THEN 1000
        WHEN transaction_id = '462705636' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4dc7bc1c9d0725ea101c434c49b59337' AND quantity = 100 THEN 200
        WHEN transaction_id = '470820501' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dffa39f16fb43a0e0ef0b43f6940e15' AND quantity = 1500 THEN 7500
        WHEN transaction_id = '467402961' AND quantity = 100 THEN 200
        WHEN transaction_id = '471866593' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4d8b1bdc92c68a3f1636dd4df981d228' AND quantity = 100 THEN 200
        WHEN transaction_id = '4df7b71a7bde018b73a305478b80e845' AND quantity = 100 THEN 200
        WHEN transaction_id = '468779000' AND quantity = 100 THEN 300
        WHEN transaction_id = '464465748' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4dc43a6992ed53c091acd742d6845a3c' AND quantity = 100 THEN 300
        WHEN transaction_id = '466427522' AND quantity = 100 THEN 200
        WHEN transaction_id = '471433911' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '463156338' AND quantity = 100 THEN 200
        WHEN transaction_id = '467420880' AND quantity = 100 THEN 200
        WHEN transaction_id = '472395024' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '4d81c1eaf9422a87444d6c40d9be7cbf' AND quantity = 100 THEN 200
        WHEN transaction_id = '470299577' AND quantity = 100 THEN 200
        WHEN transaction_id = '466036498' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '467012389' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4daf5635790cb09ac469be4d69baeed4' AND quantity = 100 THEN 200
        WHEN transaction_id = '467895914' AND quantity = 100 THEN 300
        WHEN transaction_id = '468606968' AND quantity = 100 THEN 200
        WHEN transaction_id = '465216143' AND quantity = 500 THEN 1000
        WHEN transaction_id = '468843829' AND quantity = 100 THEN 200
        WHEN transaction_id = '465131759' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '472431755' AND quantity = 100 THEN 300
        WHEN transaction_id = '465130920' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '4dc56b7e65fe63334deed54598ab8ebc' AND quantity = 100 THEN 300
        WHEN transaction_id = '468269281' AND quantity = 100 THEN 300
        WHEN transaction_id = '4e2fb6067e0541675558c54d21aa0de3' AND quantity = 100 THEN 200
        WHEN transaction_id = '466967060' AND quantity = 500 THEN 1000
        WHEN transaction_id = '462585153' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4e39efc32d65ecec421a634c82958acb' AND quantity = 100 THEN 300
        WHEN transaction_id = '467099831' AND quantity = 100 THEN 200
        WHEN transaction_id = '462719820' AND quantity = 100 THEN 300
        WHEN transaction_id = '4e2a8ff8a37d6614fe354b425d89cb2a' AND quantity = 100 THEN 1000
        WHEN transaction_id = '73069a0d-348f-4b3d-8a4c-36afd61b2930' AND quantity = 5 THEN 10
        WHEN transaction_id = '462720559' AND quantity = 100 THEN 400
        WHEN transaction_id = '4d95be81943f09ea12d6424ce38a7231' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d9de30a3deec23ca01c4341829dc919' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4da00d8912f205c4d52443499bae37bb' AND quantity = 100 THEN 1000
        WHEN transaction_id = '464307042' AND quantity = 100 THEN 200
        WHEN transaction_id = '470467764' AND quantity = 100 THEN 300
        WHEN transaction_id = '470802151' AND quantity = 100 THEN 200
        WHEN transaction_id = '472093756' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e2f5cb2c5a699ddfdbd1c486c96cdcb' AND quantity = 100 THEN 200
        WHEN transaction_id = '464268422' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '465838160' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dbefaea4700a42fd792844f67ac980b' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4ddb9690c6c9cd7aef60a141f9b5f5b7' AND quantity = 500 THEN 1000
        WHEN transaction_id = '463397159' AND quantity = 100 THEN 200
        WHEN transaction_id = '464327428' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e1bd5c75652137cac41bb411583a5c2' AND quantity = 100 THEN 200
        WHEN transaction_id = '469685166' AND quantity = 100 THEN 200
        WHEN transaction_id = '4db8e5e186a372f30f91cd44acad5237' AND quantity = 100 THEN 200
        WHEN transaction_id = '464468874' AND quantity = 100 THEN 300
        WHEN transaction_id = '4dad9ed594ea6e99a70ef240d0b07e1e' AND quantity = 100 THEN 200
        WHEN transaction_id = '469760400' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04730520' AND quantity = 1500 THEN 6000
        WHEN transaction_id = 'A-S04759638' AND quantity = 500 THEN 1000
        WHEN transaction_id = '465324856' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S05001081' AND quantity = 100 THEN 200
        WHEN transaction_id = '468990742' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04924001' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04924595' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04728827' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04743305' AND quantity = 1500 THEN 7500
        WHEN transaction_id = 'A-S04784779' AND quantity = 100 THEN 300
        WHEN transaction_id = '4dce8563c350b9af3ff0bb49a8a3d004' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04753222' AND quantity = 1500 THEN 7500
        WHEN transaction_id = 'A-S04806790' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04809418' AND quantity = 1500 THEN 7500
        WHEN transaction_id = '4e1bf3fd3fc12a2b946fef4efdbbca66' AND quantity = 100 THEN 300
        WHEN transaction_id = '471029478' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04975787' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04856975' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04884780' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04918546' AND quantity = 100 THEN 300
        WHEN transaction_id = '4e01878d8ea6ca5dacf4b0429ca23751' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04964566' AND quantity = 100 THEN 200
        WHEN transaction_id = '463377837' AND quantity = 500 THEN 1000
        WHEN transaction_id = '463482325' AND quantity = 100 THEN 500
        WHEN transaction_id = '466052303' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e39ecb3f444c5570802f24dec9933a8' AND quantity = 100 THEN 200
        WHEN transaction_id = '471858791' AND quantity = 100 THEN 200
        WHEN transaction_id = '465081446' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04753969' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04758808' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04767140' AND quantity = 100 THEN 200
        WHEN transaction_id = '469471227' AND quantity = 100 THEN 300
        WHEN transaction_id = '470827241' AND quantity = 500 THEN 1500
        WHEN transaction_id = 'A-S04933895' AND quantity = 100 THEN 200
        WHEN transaction_id = '470775455' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04768170' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04961939' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04979990' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d886f4c199ddad5c683c9475cb5df65' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04930050' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '470309651' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04776298' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04798045' AND quantity = 100 THEN 200
        WHEN transaction_id = '472605957' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04727665' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04847510' AND quantity = 100 THEN 200
        WHEN transaction_id = '462924675' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04876935' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04853789' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04786799' AND quantity = 1500 THEN 9000
        WHEN transaction_id = '472347848' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '467133390' AND quantity = 100 THEN 200
        WHEN transaction_id = '465252468' AND quantity = 1500 THEN 4500
        WHEN transaction_id = 'A-S04933702' AND quantity = 1500 THEN 6000
        WHEN transaction_id = 'A-S04970175' AND quantity = 100 THEN 300
        WHEN transaction_id = '4e027242312585d2249b0d46d690971d' AND quantity = 500 THEN 1000
        WHEN transaction_id = '471629579' AND quantity = 500 THEN 1000
        WHEN transaction_id = '463132776' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04853999' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04999649' AND quantity = 100 THEN 200
        WHEN transaction_id = '469449251' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04942326' AND quantity = 500 THEN 1000
        WHEN transaction_id = '465207223' AND quantity = 100 THEN 200
        WHEN transaction_id = '468942087' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04733478' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '463697935' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04852443' AND quantity = 100 THEN 200
        WHEN transaction_id = '468943285' AND quantity = 100 THEN 400
        WHEN transaction_id = '4e15796b225251bdcbc7b84a948efe0b' AND quantity = 100 THEN 300
        WHEN transaction_id = '470806383' AND quantity = 100 THEN 1000
        WHEN transaction_id = '471219605' AND quantity = 1500 THEN 4500
        WHEN transaction_id = 'A-S04799470' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04822389' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e06b66f373ca5d67d037a4e3c8cdb20' AND quantity = 500 THEN 1000
        WHEN transaction_id = '467115774' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dc855f878d1fa181eab5146609241d4' AND quantity = 100 THEN 300
        WHEN transaction_id = '466364469' AND quantity = 100 THEN 200
        WHEN transaction_id = '470030777' AND quantity = 500 THEN 1000
        WHEN transaction_id = '471935781' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04786758' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04814693' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04838811' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04873244' AND quantity = 1500 THEN 7500
        WHEN transaction_id = 'A-S04908853' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04747986' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04773277' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04810125' AND quantity = 100 THEN 1000
        WHEN transaction_id = '4e29f7aafb3baf00d7b7954a6bae8fce' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S05000673' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04927080' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04927364' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04938393' AND quantity = 100 THEN 500
        WHEN transaction_id = '4e0c1453cffa8f7df4b4e5440bb2d195' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04947025' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04982709' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4dbdc44d1b93e7198055e8421c918727' AND quantity = 100 THEN 500
        WHEN transaction_id = '465983252' AND quantity = 100 THEN 300
        WHEN transaction_id = '4ddc697eb0afc4ae7153d844f091231f' AND quantity = 100 THEN 200
        WHEN transaction_id = '468288188' AND quantity = 100 THEN 200
        WHEN transaction_id = '468526472' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e0c736eff1e78d42abf334409a1ed03' AND quantity = 500 THEN 2000
        WHEN transaction_id = '4e2b0c5ba0a10c0fb7b2d74416ace3ac' AND quantity = 100 THEN 300
        WHEN transaction_id = '4d812b2a417fa30c8c4869495a9e98ce' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04744588' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04934518' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04803710' AND quantity = 100 THEN 200
        WHEN transaction_id = '4db9432f9b411e45de7c6d4eac8a9541' AND quantity = 100 THEN 200
        WHEN transaction_id = '466010655' AND quantity = 100 THEN 200
        WHEN transaction_id = '472458266' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04807228' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04932621' AND quantity = 100 THEN 300
        WHEN transaction_id = '4e1c731ce897c21aed9e97440b8122f9' AND quantity = 100 THEN 300
        WHEN transaction_id = '467169416' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e13662ec78c31522df025443c9d5ebe' AND quantity = 100 THEN 300
        WHEN transaction_id = '4e2b9fdba7fadd7d8d53584ef1afbcba' AND quantity = 1500 THEN 6000
        WHEN transaction_id = 'A-S04854468' AND quantity = 100 THEN 200
        WHEN transaction_id = '462930301' AND quantity = 100 THEN 200
        WHEN transaction_id = '468967482' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04854941' AND quantity = 100 THEN 200
        WHEN transaction_id = '466895075' AND quantity = 100 THEN 200
        WHEN transaction_id = '462270548' AND quantity = 100 THEN 200
        WHEN transaction_id = '462705813' AND quantity = 1500 THEN 4500
        WHEN transaction_id = 'A-S04726328' AND quantity = 500 THEN 1000
        WHEN transaction_id = '471249119' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04828654' AND quantity = 100 THEN 200
        WHEN transaction_id = '471036364' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e3927feea306889f943fe4c8bb218db' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04839717' AND quantity = 100 THEN 300
        WHEN transaction_id = '465527086' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04992668' AND quantity = 100 THEN 200
        WHEN transaction_id = '4de7805ee6298e60b34e1541bfb258b4' AND quantity = 100 THEN 200
        WHEN transaction_id = '472189131' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S05004202' AND quantity = 100 THEN 300
        WHEN transaction_id = '4ddbbf8dd7a6970cf5c1c0490e9db268' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e312db2b5c61b9f0f4531437a9e34ae' AND quantity = 500 THEN 1000
        WHEN transaction_id = '468447795' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04902207' AND quantity = 500 THEN 1000
        WHEN transaction_id = '470210463' AND quantity = 500 THEN 1000
        WHEN transaction_id = '462477967' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04727302' AND quantity = 1500 THEN 10500
        WHEN transaction_id = 'A-S04851051' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04812248' AND quantity = 100 THEN 1000
        WHEN transaction_id = 'A-S04853572' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04863768' AND quantity = 100 THEN 300
        WHEN transaction_id = '470705807' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04969040' AND quantity = 500 THEN 1000
        WHEN transaction_id = '471225036' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d89a830f3588ced7bbc8e44a2bb9f23' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04863529' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04944332' AND quantity = 100 THEN 200
        WHEN transaction_id = '472006378' AND quantity = 100 THEN 500
        WHEN transaction_id = 'A-S04900470' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04763288' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e3917c7cea5077e804fd8498e86a0bf' AND quantity = 100 THEN 300
        WHEN transaction_id = '4e3986f4784d1cc2f05d1c49bbb65c05' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04920517' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04920956' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04769160' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04793796' AND quantity = 100 THEN 300
        WHEN transaction_id = '467945848' AND quantity = 100 THEN 200
        WHEN transaction_id = '470664445' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04996156' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04904714' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04729403' AND quantity = 1500 THEN 6000
        WHEN transaction_id = '465328195' AND quantity = 100 THEN 300
        WHEN transaction_id = '468230644' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e2fb77f9434fee571ad2043eea3023b' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04814884' AND quantity = 100 THEN 200
        WHEN transaction_id = '467088805' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04729565' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04982223' AND quantity = 100 THEN 300
        WHEN transaction_id = '462847738' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d92903d07293c6bca047c4bb3b8c909' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '465628932' AND quantity = 100 THEN 200
        WHEN transaction_id = '464831393' AND quantity = 100 THEN 200
        WHEN transaction_id = '465432835' AND quantity = 100 THEN 1000
        WHEN transaction_id = '465746308' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dd519d3b9a077a9163df24a9bb16aa8' AND quantity = 500 THEN 1000
        WHEN transaction_id = '469598302' AND quantity = 100 THEN 300
        WHEN transaction_id = '471025715' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e33a66db623c40a3d5af34bce8d91db' AND quantity = 500 THEN 1000
        WHEN transaction_id = '472477088' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e171a25daa36587907e7f41bbb9cba7' AND quantity = 100 THEN 200
        WHEN transaction_id = '471290955' AND quantity = 100 THEN 200
        WHEN transaction_id = '467973333' AND quantity = 100 THEN 200
        WHEN transaction_id = '465775913' AND quantity = 500 THEN 1000
        WHEN transaction_id = '467025102' AND quantity = 500 THEN 1000
        WHEN transaction_id = '470265465' AND quantity = 100 THEN 200
        WHEN transaction_id = '470993096' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d9509a38439f1cd9a310041ef9f1ebe' AND quantity = 100 THEN 200
        WHEN transaction_id = '462930223' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04837244' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04846480' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04768552' AND quantity = 100 THEN 1000
        WHEN transaction_id = 'A-S04794297' AND quantity = 100 THEN 200
        WHEN transaction_id = '466758125' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04853942' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04812030' AND quantity = 1500 THEN 9000
        WHEN transaction_id = 'A-S04882902' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04902424' AND quantity = 100 THEN 200
        WHEN transaction_id = '468816359' AND quantity = 100 THEN 200
        WHEN transaction_id = '472660978' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04857293' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04863291' AND quantity = 100 THEN 200
        WHEN transaction_id = '472564500' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S05009711' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04944692' AND quantity = 100 THEN 400
        WHEN transaction_id = '471359201' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04979215' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04932268' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '470946841' AND quantity = 100 THEN 200
        WHEN transaction_id = '463483570' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '467471601' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '467807964' AND quantity = 100 THEN 200
        WHEN transaction_id = '4de7301681b4bb223b82954f25a11757' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4df273560fe5e4edd20e1c46f6a832e7' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '470989779' AND quantity = 100 THEN 300
        WHEN transaction_id = '465358858' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04922802' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04924012' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04956404' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04986913' AND quantity = 1500 THEN 6000
        WHEN transaction_id = '464792014' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dece73afd7d0175bc3e3f419e9c4382' AND quantity = 100 THEN 300
        WHEN transaction_id = '464248902' AND quantity = 100 THEN 200
        WHEN transaction_id = '465568733' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4df2a490306ae0ee13a50b429b95d419' AND quantity = 500 THEN 1000
        WHEN transaction_id = '469812482' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04864144' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4d886e9b2f1b35d152dd3b4389b59c90' AND quantity = 100 THEN 200
        WHEN transaction_id = '465099444' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '4e30647236bbd4d80ed23e4d1594a536' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04951085' AND quantity = 100 THEN 300
        WHEN transaction_id = '471054044' AND quantity = 100 THEN 300
        WHEN transaction_id = '4deea66543e712dffd73e64a92a5d9dd' AND quantity = 100 THEN 500
        WHEN transaction_id = '462487186' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04876299' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04844729' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04854231' AND quantity = 100 THEN 300
        WHEN transaction_id = '463733103' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04929433' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4d839342e2f942e63a8e8d4a23bd97bb' AND quantity = 100 THEN 200
        WHEN transaction_id = '467132556' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e1b5bc0194f7fa2fab53b4c79b2b40d' AND quantity = 25000 THEN 100000
        WHEN transaction_id = 'A-S04732875' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '467155335' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04875097' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04955672' AND quantity = 500 THEN 1000
        WHEN transaction_id = '465852486' AND quantity = 100 THEN 200
        WHEN transaction_id = '464265885' AND quantity = 100 THEN 500
        WHEN transaction_id = 'A-S04776213' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S05005335' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4de96184c8d529e2cd8c024e26a98fba' AND quantity = 100 THEN 200
        WHEN transaction_id = '469334753' AND quantity = 100 THEN 300
        WHEN transaction_id = '4dc89b38b5da01b3979f0f454186eac5' AND quantity = 100 THEN 400
        WHEN transaction_id = '464491549' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '468554965' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04938281' AND quantity = 500 THEN 1000
        WHEN transaction_id = '464309451' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04801860' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '467927608' AND quantity = 100 THEN 200
        WHEN transaction_id = '472319185' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4dd9cdfafff26b994bff60405e8f776b' AND quantity = 100 THEN 200
        WHEN transaction_id = '465670758' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04745940' AND quantity = 1500 THEN 4500
        WHEN transaction_id = 'A-S04786903' AND quantity = 100 THEN 200
        WHEN transaction_id = '464519677' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04901692' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04920391' AND quantity = 500 THEN 1000
        WHEN transaction_id = '462496830' AND quantity = 100 THEN 200
        WHEN transaction_id = '463481627' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '4e0b587b0f97c9ff34e42746b69fb602' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04841184' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '463655348' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04771911' AND quantity = 100 THEN 300
        WHEN transaction_id = '472375006' AND quantity = 1500 THEN 4500
        WHEN transaction_id = 'A-S05005478' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04936569' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04986708' AND quantity = 500 THEN 1000
        WHEN transaction_id = '467602828' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04819553' AND quantity = 100 THEN 200
        WHEN transaction_id = '467971165' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04740066' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S05013668' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e2e74aaaea185f326301946dda07c49' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04981994' AND quantity = 100 THEN 700
        WHEN transaction_id = '4e0ef265958f30ee4f20f84a61887934' AND quantity = 1500 THEN 9000
        WHEN transaction_id = '464470243' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04798089' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04881695' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04963150' AND quantity = 25000 THEN 100000
        WHEN transaction_id = 'A-S04809803' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04821903' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04998623' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04986625' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04782984' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e11355146e73893364d7f45cba85e95' AND quantity = 500 THEN 1000
        WHEN transaction_id = '462931326' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d8c1eed93132429a0ee7b409da48bdc' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4d9e42c5183348db84f8744e449f94f9' AND quantity = 100 THEN 200
        WHEN transaction_id = '465083124' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4dbe8d2c4f09f3af4aab0447bdaaaafb' AND quantity = 100 THEN 200
        WHEN transaction_id = '463014273' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '468395030' AND quantity = 100 THEN 300
        WHEN transaction_id = '466886263' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '468253095' AND quantity = 500 THEN 1000
        WHEN transaction_id = '468274051' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e0806837654481dcb220043e9a5a1f9' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4e2597da01270db0380a9c45d4b4d4c2' AND quantity = 500 THEN 1000
        WHEN transaction_id = '472395923' AND quantity = 100 THEN 300
        WHEN transaction_id = '467569424' AND quantity = 100 THEN 400
        WHEN transaction_id = '462215374' AND quantity = 100 THEN 200
        WHEN transaction_id = '472263327' AND quantity = 100 THEN 200
        WHEN transaction_id = '4df343ef21511079b4b1f349fc81c6de' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e1094e3f45986a8213948438e949083' AND quantity = 100 THEN 200
        WHEN transaction_id = '468690486' AND quantity = 100 THEN 200
        WHEN transaction_id = '471097875' AND quantity = 100 THEN 300
        WHEN transaction_id = '471846741' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04790841' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04810746' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04810823' AND quantity = 10000 THEN 20000
        WHEN transaction_id = 'A-S04872116' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S05001816' AND quantity = 100 THEN 200
        WHEN transaction_id = '469813501' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04743273' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04943645' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04759118' AND quantity = 100 THEN 200
        WHEN transaction_id = '466647914' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04952707' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04977415' AND quantity = 1500 THEN 6000
        WHEN transaction_id = 'A-S04734551' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '4da48479e4fe1229fb9d73413dbe43b3' AND quantity = 100 THEN 300
        WHEN transaction_id = '4daa78e1118799b810fada4112902dd7' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e250a03af35bf23c0207244abbb36fc' AND quantity = 25000 THEN 50000
        WHEN transaction_id = 'A-S04842757' AND quantity = 100 THEN 200
        WHEN transaction_id = '4da2c3dca5bd7b0349a5f044b49e9ad5' AND quantity = 500 THEN 1000
        WHEN transaction_id = '471958481' AND quantity = 500 THEN 1000
        WHEN transaction_id = '472285772' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dea828e37af1aa9862c2044d6adfb25' AND quantity = 100 THEN 200
        WHEN transaction_id = '462493564' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d9c914af2dc8cfb6c48cf4982a4bbc2' AND quantity = 100 THEN 300
        WHEN transaction_id = '4da82ef544bfd64d9ead584b67a31ef8' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dae466497dc0b03f09e8f4569bc9655' AND quantity = 100 THEN 200
        WHEN transaction_id = '469246012' AND quantity = 100 THEN 200
        WHEN transaction_id = '468383247' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4db43435a4c0fb74b6a5254e1b8dc4db' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4df805ab7e839f0e61adc74ed4934935' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4da03060eda8f1abff4f0142d98a8fc1' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04743599' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04782560' AND quantity = 1500 THEN 6000
        WHEN transaction_id = 'A-S04776536' AND quantity = 1500 THEN 4500
        WHEN transaction_id = 'A-S04878544' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04881074' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04898263' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04950049' AND quantity = 1500 THEN 7500
        WHEN transaction_id = 'A-S04729956' AND quantity = 1500 THEN 9000
        WHEN transaction_id = '466295484' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04758604' AND quantity = 1500 THEN 6000
        WHEN transaction_id = 'A-S04772628' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04801880' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04735190' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04865177' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04898944' AND quantity = 500 THEN 1000
        WHEN transaction_id = '468533128' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '464058447' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04989845' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d9a12e0dfffb44d4de0ee4f218bdffa' AND quantity = 100 THEN 600
        WHEN transaction_id = 'A-S04806324' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04809640' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04811952' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04812851' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04753685' AND quantity = 500 THEN 1000
        WHEN transaction_id = '468679170' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04919004' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04921365' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S05009142' AND quantity = 100 THEN 200
        WHEN transaction_id = '472605364' AND quantity = 100 THEN 300
        WHEN transaction_id = '467311287' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04893240' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04900390' AND quantity = 100 THEN 200
        WHEN transaction_id = '468750009' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e1bb4a6aaea4069f0c22441d09cd809' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e0a70f05cc73fb7334cf249cab77c3a' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4da00c37a862cdf43d32284b05be5365' AND quantity = 100 THEN 300
        WHEN transaction_id = '464598470' AND quantity = 100 THEN 300
        WHEN transaction_id = '4db1c1e84b00ac9b800d344a7c92a5d3' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '470246236' AND quantity = 100 THEN 300
        WHEN transaction_id = '462694048' AND quantity = 100 THEN 300
        WHEN transaction_id = '462256351' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d92f839dc78ed5ad06e6041c2835663' AND quantity = 500 THEN 1000
        WHEN transaction_id = '465314421' AND quantity = 100 THEN 200
        WHEN transaction_id = '465817147' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dc447ecd52dd75d8c4ced44cab438f7' AND quantity = 100 THEN 300
        WHEN transaction_id = '472529521' AND quantity = 100 THEN 200
        WHEN transaction_id = '472481831' AND quantity = 100 THEN 200
        WHEN transaction_id = '4df0ed21bf8b8a9444ffdc42cdae6d9c' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dd9d203e0b362cf3dc82e4d29bec13b' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04882117' AND quantity = 100 THEN 300
        WHEN transaction_id = '468220313' AND quantity = 100 THEN 500
        WHEN transaction_id = 'A-S04905348' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04820520' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S05013862' AND quantity = 100 THEN 200
        WHEN transaction_id = '470237538' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '463409580' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04830121' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04975638' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04781426' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04863483' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04856521' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04899465' AND quantity = 500 THEN 1500
        WHEN transaction_id = '469781029' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04975846' AND quantity = 100 THEN 200
        WHEN transaction_id = '4da0310c62e141767506d84988a1a68a' AND quantity = 100 THEN 200
        WHEN transaction_id = '464741147' AND quantity = 100 THEN 200
        WHEN transaction_id = '464452106' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4dd83dd8b68ed6d3c7ba4f40a198df5a' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e084039c4bcf60806148e4a058c2193' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04731728' AND quantity = 100 THEN 200
        WHEN transaction_id = '464026600' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04913520' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04813099' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04852424' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4d98a3d46e0716c778f0b5451080b09a' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04971502' AND quantity = 100 THEN 200
        WHEN transaction_id = '463676848' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '464046016' AND quantity = 500 THEN 1000
        WHEN transaction_id = '469014956' AND quantity = 100 THEN 400
        WHEN transaction_id = '470586572' AND quantity = 100 THEN 200
        WHEN transaction_id = '472418215' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '464567679' AND quantity = 100 THEN 300
        WHEN transaction_id = '4df68fe495d027a71fffdd4b318f1333' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04927241' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04931434' AND quantity = 500 THEN 1000
        WHEN transaction_id = '464010747' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '467927186' AND quantity = 100 THEN 200
        WHEN transaction_id = '472539474' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04759155' AND quantity = 1500 THEN 9000
        WHEN transaction_id = '463632924' AND quantity = 25000 THEN 100000
        WHEN transaction_id = '472678930' AND quantity = 100 THEN 400
        WHEN transaction_id = '464573751' AND quantity = 100 THEN 300
        WHEN transaction_id = '4dae61ae6626b522c8d2d140b7bcc773' AND quantity = 100 THEN 200
        WHEN transaction_id = '472172472' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d99dd34674b6238e6320441efa60eb5' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e08b7ebaef4772411f4db481db52084' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '472383544' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e24e513421bd0a8654c004e90b8991c' AND quantity = 100 THEN 200
        WHEN transaction_id = '463718064' AND quantity = 100 THEN 300
        WHEN transaction_id = '465980739' AND quantity = 500 THEN 1000
        WHEN transaction_id = '469499245' AND quantity = 100 THEN 200
        WHEN transaction_id = '469914264' AND quantity = 100 THEN 200
        WHEN transaction_id = '464991220' AND quantity = 100 THEN 200
        WHEN transaction_id = '4de740336b315ab73306bf473bab9bb7' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04900707' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04842074' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04731338' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04763761' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04788256' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04885858' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04916910' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dd373018c0a1ccf7d76c8477b985a7b' AND quantity = 100 THEN 200
        WHEN transaction_id = '463718136' AND quantity = 500 THEN 1000
        WHEN transaction_id = '464639911' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4d99ca9f490ff88eb3701547838ce56f' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04801859' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04904907' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04945616' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04952248' AND quantity = 100 THEN 300
        WHEN transaction_id = '462204301' AND quantity = 100 THEN 200
        WHEN transaction_id = '468049938' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d8533ce55389c8f0a293f44ebbfde76' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '471950855' AND quantity = 100 THEN 200
        WHEN transaction_id = '463630260' AND quantity = 25000 THEN 100000
        WHEN transaction_id = 'A-S04872616' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04863822' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04911602' AND quantity = 1500 THEN 9000
        WHEN transaction_id = '4e100f328a13a57daf384d459985afa6' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04901145' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04938861' AND quantity = 100 THEN 200
        WHEN transaction_id = '4ddd47ff8232184247b8a44d7381d3dc' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04944243' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4d8bebd3c966fe1a520e28427a974fc6' AND quantity = 100 THEN 200
        WHEN transaction_id = '4da8154ca222db9522c72146fda8a89d' AND quantity = 500 THEN 1000
        WHEN transaction_id = '464471364' AND quantity = 100 THEN 200
        WHEN transaction_id = '4df1f46dd517735a6a22414888afb74c' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e0b76dbb86fb9ce2bdd074d09a32ec5' AND quantity = 500 THEN 2000
        WHEN transaction_id = '4dc2f008c870cb8ce509f24e6695cfe4' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04751163' AND quantity = 100 THEN 500
        WHEN transaction_id = 'A-S04797945' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04841409' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04933210' AND quantity = 100 THEN 300
        WHEN transaction_id = '471252371' AND quantity = 100 THEN 300
        WHEN transaction_id = '463129778' AND quantity = 100 THEN 300
        WHEN transaction_id = '4dfb2abd2e7bde8fa074d34a6aba3a8e' AND quantity = 100 THEN 200
        WHEN transaction_id = '469621582' AND quantity = 100 THEN 300
        WHEN transaction_id = '4dca5c5c7fdcd56131fbad4e8a8f0155' AND quantity = 100 THEN 400
        WHEN transaction_id = '463534566' AND quantity = 100 THEN 300
        WHEN transaction_id = '466258417' AND quantity = 100 THEN 300
        WHEN transaction_id = '468237035' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4dfb138cfbfd529b9cbabb46069ef80d' AND quantity = 100 THEN 400
        WHEN transaction_id = '4dfb130595b4c9b27408bb4479b1894f' AND quantity = 1500 THEN 9000
        WHEN transaction_id = '4e056ceef9e1f44768174145eca7050d' AND quantity = 500 THEN 1000
        WHEN transaction_id = '471564977' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e24ed40ba13f8ccc4292f4cde9db458' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4d91f649c46bdac749317441ec868448' AND quantity = 1500 THEN 9000
        WHEN transaction_id = '4daa17a18c67d229a8a23044cfb7d972' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d7c128b847267c21a20cd4a2d87813f' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d846cd5912a9939dbef5a4fbc8f063a' AND quantity = 100 THEN 200
        WHEN transaction_id = '464551289' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '469075190' AND quantity = 100 THEN 400
        WHEN transaction_id = '471762796' AND quantity = 500 THEN 1000
        WHEN transaction_id = '466151171' AND quantity = 100 THEN 200
        WHEN transaction_id = '465107277' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '466018554' AND quantity = 100 THEN 700
        WHEN transaction_id = '4dbe2a874b8210170581b24f2391c891' AND quantity = 100 THEN 300
        WHEN transaction_id = '4e2a9c195234f0f438b1154c71a0dec7' AND quantity = 500 THEN 4500
        WHEN transaction_id = '471936558' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e34b4dfbf2fe66e173285402fa3cee8' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04846034' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04746870' AND quantity = 100 THEN 200
        WHEN transaction_id = '467885649' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04897884' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S05006525' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04924112' AND quantity = 100 THEN 200
        WHEN transaction_id = '470041891' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04966734' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04967505' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04733430' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04838209' AND quantity = 500 THEN 1000
        WHEN transaction_id = '466382039' AND quantity = 100 THEN 300
        WHEN transaction_id = '4d9ad71d1f8e41577d65da4c3db41a55' AND quantity = 500 THEN 1000
        WHEN transaction_id = '463739812' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04794926' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04838458' AND quantity = 100 THEN 300
        WHEN transaction_id = '465635945' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dec66a1e43fe6086d0f574fe29ea0d1' AND quantity = 100 THEN 600
        WHEN transaction_id = 'A-S04896434' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04902820' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4d8444d86e21b99241d24c4bf5978511' AND quantity = 100 THEN 200
        WHEN transaction_id = '462655977' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04741254' AND quantity = 100 THEN 300
        WHEN transaction_id = '463342517' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04796108' AND quantity = 1500 THEN 9000
        WHEN transaction_id = 'A-S04820543' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04955100' AND quantity = 100 THEN 200
        WHEN transaction_id = '470753153' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04905325' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04959314' AND quantity = 100 THEN 300
        WHEN transaction_id = '471615096' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04980925' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04839254' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04767119' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d86e18106344994bbc859456892bae2' AND quantity = 500 THEN 1000
        WHEN transaction_id = '462913308' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04786803' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04824462' AND quantity = 100 THEN 200
        WHEN transaction_id = '467295713' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04909994' AND quantity = 10000 THEN 20000
        WHEN transaction_id = 'A-S04820203' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S05005076' AND quantity = 500 THEN 2000
        WHEN transaction_id = 'A-S04916106' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04920450' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04924109' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e07bbbfba7e46b5d5f2f749f1986f13' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04950733' AND quantity = 100 THEN 300
        WHEN transaction_id = '4e1e4cd3367c8c7c15d38d4b4781a1bd' AND quantity = 100 THEN 200
        WHEN transaction_id = '471385085' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S05012533' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04952039' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04965985' AND quantity = 500 THEN 2000
        WHEN transaction_id = '471646207' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04983545' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04756517' AND quantity = 1500 THEN 6000
        WHEN transaction_id = 'A-S04803163' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04856470' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04877202' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04854194' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04828781' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04835541' AND quantity = 100 THEN 200
        WHEN transaction_id = '4ddd1b71393ec195a73bb04b3289c5d0' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04895361' AND quantity = 100 THEN 200
        WHEN transaction_id = '468252694' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04963052' AND quantity = 500 THEN 1500
        WHEN transaction_id = '4d8612428ffa05cd662638499a85903b' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '463685623' AND quantity = 500 THEN 1000
        WHEN transaction_id = '465867896' AND quantity = 100 THEN 200
        WHEN transaction_id = '466072238' AND quantity = 100 THEN 200
        WHEN transaction_id = '467995581' AND quantity = 100 THEN 200
        WHEN transaction_id = '469475070' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e165d2096187f4eccc5be41fe8aa047' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e0b87e4e3170672d14e2c42ab87f1f7' AND quantity = 100 THEN 200
        WHEN transaction_id = '471334261' AND quantity = 100 THEN 200
        WHEN transaction_id = '462998877' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '468806071' AND quantity = 100 THEN 200
        WHEN transaction_id = '469028938' AND quantity = 100 THEN 200
        WHEN transaction_id = '469306683' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04966743' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04779066' AND quantity = 100 THEN 200
        WHEN transaction_id = '4db9c329fbe406dd0c30f84a3abc35ea' AND quantity = 100 THEN 600
        WHEN transaction_id = '4dd354756353db23a3fa464f5bb61dcf' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e3470cb2ce5eb5f11d94545678d23ea' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04912483' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4d80e6bffa7e63487da39c4a248a9801' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '463347730' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dd0ad564dd8095768f4924485a28ed9' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S05007874' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04946211' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04979073' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04762413' AND quantity = 1500 THEN 6000
        WHEN transaction_id = '4d9fdfe17d4fede8fbf2be4faa860d47' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dfbbf718a250934153d894e2bb32ba3' AND quantity = 100 THEN 200
        WHEN transaction_id = '470302028' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e356a889e15843eda6c364d11985ab6' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '469204114' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04805382' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04934067' AND quantity = 500 THEN 2500
        WHEN transaction_id = 'A-S04958131' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04734216' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04734922' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04830312' AND quantity = 100 THEN 200
        WHEN transaction_id = '467940975' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e0792e8395d2b19f39ac544cd9d02a8' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04981385' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04961157' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04981844' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04774502' AND quantity = 100 THEN 1000
        WHEN transaction_id = 'A-S04782733' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04804119' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '462417459' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04794111' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04780898' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04854188' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04964123' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dd9d1c5d59b08ab13528e4ad2bb1573' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04801897' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04812174' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '462319019' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S05008761' AND quantity = 100 THEN 200
        WHEN transaction_id = '472186186' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e0bdc623ca98de3f8983f4801a4fc68' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04871828' AND quantity = 100 THEN 200
        WHEN transaction_id = '462596914' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04838322' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04731114' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '468438359' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04902767' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04905159' AND quantity = 1500 THEN 7500
        WHEN transaction_id = '470334473' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04875824' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04781012' AND quantity = 100 THEN 400
        WHEN transaction_id = '463147520' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04893985' AND quantity = 100 THEN 200
        WHEN transaction_id = '471696070' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04748815' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04776667' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04951686' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04757531' AND quantity = 100 THEN 200
        WHEN transaction_id = '470464473' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04874797' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04752416' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04848879' AND quantity = 100 THEN 200
        WHEN transaction_id = '465155983' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '468149916' AND quantity = 100 THEN 300
        WHEN transaction_id = '472455645' AND quantity = 100 THEN 300
        WHEN transaction_id = '462217810' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4dad6b28eec41a669f4eed46fba07861' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04846041' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04727496' AND quantity = 100 THEN 400
        WHEN transaction_id = '463981610' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S05000841' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S05001373' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4d9b1331e56c42ba5ae94d4dcc8d0c2a' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04933669' AND quantity = 100 THEN 200
        WHEN transaction_id = '470428075' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04978250' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04977853' AND quantity = 10000 THEN 30000
        WHEN transaction_id = '4de31d0143f7bc4861b06149719972bb' AND quantity = 100 THEN 300
        WHEN transaction_id = '469258167' AND quantity = 100 THEN 300
        WHEN transaction_id = '462463584' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04757351' AND quantity = 1500 THEN 6000
        WHEN transaction_id = 'A-S04893516' AND quantity = 500 THEN 2500
        WHEN transaction_id = '464912997' AND quantity = 100 THEN 300
        WHEN transaction_id = '4e09c8bb9607b4593e14c44ab9b868dd' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '472598645' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04937350' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04786458' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04777121' AND quantity = 1500 THEN 9000
        WHEN transaction_id = '4df1a4b36598d256d9ea72419db8e04c' AND quantity = 100 THEN 300
        WHEN transaction_id = '463714248' AND quantity = 100 THEN 200
        WHEN transaction_id = '467552295' AND quantity = 100 THEN 200
        WHEN transaction_id = '470197455' AND quantity = 100 THEN 200
        WHEN transaction_id = '471936343' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04986131' AND quantity = 10000 THEN 20000
        WHEN transaction_id = 'A-S04791632' AND quantity = 100 THEN 200
        WHEN transaction_id = '4ddeac3fc54cad6c2ea9bb4dcca21662' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '462662401' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04932492' AND quantity = 100 THEN 200
        WHEN transaction_id = '463214827' AND quantity = 500 THEN 1000
        WHEN transaction_id = '462754653' AND quantity = 100 THEN 300
        WHEN transaction_id = '4de1c8043f512f1da9a5ad40a5a7847e' AND quantity = 100 THEN 1000
        WHEN transaction_id = '4de9339de46e111299a25a4cdb9e8b48' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '468243597' AND quantity = 100 THEN 200
        WHEN transaction_id = '471911951' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d9f49581aae5acb77276d4bd3a06d7c' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4d906d8fdfcaed1e39030848e987bb4c' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04769641' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '468734689' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04912625' AND quantity = 100 THEN 200
        WHEN transaction_id = '469045576' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S05011689' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04949964' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04968658' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04791070' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04971084' AND quantity = 100 THEN 200
        WHEN transaction_id = '429a7e0d-5316-4c5c-b363-703d99e8533b' AND quantity = 20 THEN 40
        WHEN transaction_id = '472627228' AND quantity = 100 THEN 200
        WHEN transaction_id = '464142920' AND quantity = 100 THEN 200
        WHEN transaction_id = '472081781' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '468269146' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04801061' AND quantity = 100 THEN 1000
        WHEN transaction_id = '465122424' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04774212' AND quantity = 500 THEN 1000
        WHEN transaction_id = '464847661' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04990382' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04788321' AND quantity = 1500 THEN 6000
        WHEN transaction_id = 'A-S04913917' AND quantity = 100 THEN 200
        WHEN transaction_id = '469981085' AND quantity = 100 THEN 300
        WHEN transaction_id = '4e2fb098bdf78330118fbb40729e8b86' AND quantity = 100 THEN 200
        WHEN transaction_id = '467386179' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dc3f03219bc1ee6e6494042a6a3bb6b' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dc6f8c843df5a4e9226794812b99a7f' AND quantity = 500 THEN 1000
        WHEN transaction_id = '471287265' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d7b9c89ad2f00a590317a4b9cab6863' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '462589949' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '464840694' AND quantity = 100 THEN 200
        WHEN transaction_id = '466218394' AND quantity = 100 THEN 200
        WHEN transaction_id = '465571245' AND quantity = 100 THEN 200
        WHEN transaction_id = '465936041' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4dff70c6c4b79df0cd084d42648563eb' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e07749043d7bbc31768ca47a8bc268e' AND quantity = 100 THEN 200
        WHEN transaction_id = '470945681' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4da0c3090a9e39baa99d584966a44ad2' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e0c688ef8d31c9a193cac4da7a3dd8f' AND quantity = 100 THEN 300
        WHEN transaction_id = '4e20cb80e1e16ec4d509d54644aa189c' AND quantity = 500 THEN 1000
        WHEN transaction_id = '462235959' AND quantity = 100 THEN 200
        WHEN transaction_id = '472107539' AND quantity = 100 THEN 200
        WHEN transaction_id = '4da43d4917cb1720a22a7f4174b0625b' AND quantity = 100 THEN 200
        WHEN transaction_id = '465514028' AND quantity = 100 THEN 200
        WHEN transaction_id = '4df0ec95872a3a36304c3c4fb184c889' AND quantity = 100 THEN 200
        WHEN transaction_id = '470748872' AND quantity = 100 THEN 200
        WHEN transaction_id = '471668807' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04855097' AND quantity = 100 THEN 300
        WHEN transaction_id = '4d8b90d9b3b65f93b7e49b4818ad2c2a' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04773375' AND quantity = 1500 THEN 6000
        WHEN transaction_id = 'A-S04792578' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04869968' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04886636' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04997411' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4e18e7cb9a23e793378e8e47eb847d5a' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04733569' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04842152' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04783521' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04813181' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04818759' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04874347' AND quantity = 500 THEN 1000
        WHEN transaction_id = '467973463' AND quantity = 100 THEN 200
        WHEN transaction_id = '4df866b7e115d668557565499db6e44a' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04942640' AND quantity = 100 THEN 300
        WHEN transaction_id = '4daedc0d67afa2c67b115949e2bfab09' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dfc7fc203afdd575325b444b2ad296b' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04961019' AND quantity = 100 THEN 200
        WHEN transaction_id = '471354855' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04841890' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4dd286ee0a7a48d58bd7fb4e9886c4ca' AND quantity = 100 THEN 500
        WHEN transaction_id = 'A-S04811906' AND quantity = 1500 THEN 4500
        WHEN transaction_id = 'A-S04998142' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04896342' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04855659' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04894196' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04958982' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S05011363' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04752038' AND quantity = 100 THEN 300
        WHEN transaction_id = '472514267' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04911482' AND quantity = 100 THEN 200
        WHEN transaction_id = '468435146' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04801013' AND quantity = 100 THEN 200
        WHEN transaction_id = '465318724' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04857249' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04862140' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04740499' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04864647' AND quantity = 100 THEN 300
        WHEN transaction_id = '4d99e41ee185f0a34abf384bd6b10da1' AND quantity = 100 THEN 500
        WHEN transaction_id = '46e0ae52-56ba-46ad-be1d-ead2d835c8db' AND quantity = 5 THEN 10
        WHEN transaction_id = 'A-S04763378' AND quantity = 25000 THEN 50000
        WHEN transaction_id = 'A-S04794293' AND quantity = 100 THEN 200
        WHEN transaction_id = '465993553' AND quantity = 5000 THEN 10000
        WHEN transaction_id = '4e00a6ebb1ed3175c9d1be435e838a28' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04858395' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04910515' AND quantity = 25000 THEN 150000
        WHEN transaction_id = '472536305' AND quantity = 100 THEN 200
        WHEN transaction_id = '463451770' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '465966216' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4dd83e3e39959963fd64b34ad5919c41' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04882212' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04888808' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04953931' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '468656551' AND quantity = 1500 THEN 7500
        WHEN transaction_id = 'A-S04870553' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04885699' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '472275946' AND quantity = 100 THEN 200
        WHEN transaction_id = '467165556' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d964397cd82cedff7195048d7a9df69' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04765453' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04793491' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04803509' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04809497' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S05007411' AND quantity = 500 THEN 2000
        WHEN transaction_id = 'A-S04933777' AND quantity = 100 THEN 200
        WHEN transaction_id = '470797380' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04974124' AND quantity = 100 THEN 500
        WHEN transaction_id = 'A-S04975936' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04981039' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e2a682af514d24e16980a4d0e953b29' AND quantity = 100 THEN 300
        WHEN transaction_id = '4dce39384262f3c3bbacfa495f9ea966' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04968292' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04991667' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04927830' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04735900' AND quantity = 1500 THEN 6000
        WHEN transaction_id = '472321858' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '468160966' AND quantity = 1500 THEN 6000
        WHEN transaction_id = 'A-S04745283' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04763913' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04765380' AND quantity = 1500 THEN 7500
        WHEN transaction_id = 'A-S04805104' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04871527' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04892638' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S05013763' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04736957' AND quantity = 1500 THEN 9000
        WHEN transaction_id = '4dd35f493da17c13b86eb3416a9b8631' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04877329' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04977773' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04978824' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04751381' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04792597' AND quantity = 100 THEN 400
        WHEN transaction_id = '465293882' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04991810' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04762042' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04843653' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04810712' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04810874' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04888306' AND quantity = 25000 THEN 150000
        WHEN transaction_id = 'A-S04997744' AND quantity = 1500 THEN 6000
        WHEN transaction_id = 'A-S04895479' AND quantity = 100 THEN 200
        WHEN transaction_id = '472287533' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S05007190' AND quantity = 500 THEN 2000
        WHEN transaction_id = '472551953' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04936202' AND quantity = 1500 THEN 6000
        WHEN transaction_id = 'A-S04947466' AND quantity = 100 THEN 200
        WHEN transaction_id = '470724031' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04874977' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e356641d6dce11da590cf4bccbd5786' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S05008636' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e14bd463f3a3b9454773f49dbab5c87' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04833695' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04969143' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04809782' AND quantity = 500 THEN 1000
        WHEN transaction_id = '467891360' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04795632' AND quantity = 100 THEN 200
        WHEN transaction_id = '466034689' AND quantity = 100 THEN 300
        WHEN transaction_id = '467601313' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04942657' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e15c5c503fa58eedad11648c6b8deba' AND quantity = 100 THEN 400
        WHEN transaction_id = '4e1c10191afb112dfe7dc140e291cb2c' AND quantity = 100 THEN 200
        WHEN transaction_id = '467598239' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04728435' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04781507' AND quantity = 100 THEN 1000
        WHEN transaction_id = 'A-S04746731' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4d95b28786ed06c0eac83b4569ac57d8' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04815900' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04820437' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04910360' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04921030' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04935417' AND quantity = 500 THEN 1000
        WHEN transaction_id = '472733674' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04958793' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04959389' AND quantity = 100 THEN 200
        WHEN transaction_id = '470874661' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04984288' AND quantity = 100 THEN 200
        WHEN transaction_id = '464561909' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04870813' AND quantity = 100 THEN 300
        WHEN transaction_id = '4df14441801dc708269ec042068f36c5' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04967620' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04925579' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04946382' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e10ce84d56764f9cff01641a4ad6f0b' AND quantity = 1500 THEN 15000
        WHEN transaction_id = '464044399' AND quantity = 1500 THEN 4500
        WHEN transaction_id = 'A-S04825472' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04874496' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e1a0a1837dded6055c6d54fb88cf977' AND quantity = 100 THEN 200
        WHEN transaction_id = '466066309' AND quantity = 500 THEN 1000
        WHEN transaction_id = '467589902' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04884739' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04952668' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04731746' AND quantity = 1500 THEN 9000
        WHEN transaction_id = 'A-S04733244' AND quantity = 1500 THEN 7500
        WHEN transaction_id = 'A-S04741560' AND quantity = 100 THEN 200
        WHEN transaction_id = '469001259' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04933656' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04946553' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04948428' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04969994' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04759480' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04805576' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04993498' AND quantity = 500 THEN 1000
        WHEN transaction_id = '470191250' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04730823' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04838019' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04860923' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04942562' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04953214' AND quantity = 500 THEN 1000
        WHEN transaction_id = '471393204' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04729052' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dca3324413d5fcb6f4de949539610f5' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04809063' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04807084' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04732319' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04733012' AND quantity = 1500 THEN 9000
        WHEN transaction_id = 'A-S04926518' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4d7b715f32a08c599c125a449dbf0f5b' AND quantity = 100 THEN 200
        WHEN transaction_id = '463888594' AND quantity = 100 THEN 200
        WHEN transaction_id = '464309877' AND quantity = 100 THEN 200
        WHEN transaction_id = '465104565' AND quantity = 100 THEN 300
        WHEN transaction_id = '469619861' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e227c2c527fea8bec3d16450987dc9d' AND quantity = 1500 THEN 9000
        WHEN transaction_id = '465299980' AND quantity = 500 THEN 1000
        WHEN transaction_id = '465620056' AND quantity = 100 THEN 200
        WHEN transaction_id = '466006620' AND quantity = 500 THEN 1000
        WHEN transaction_id = '467390517' AND quantity = 100 THEN 300
        WHEN transaction_id = '4e1abff3c81e579e6a5d7c498fb366dc' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dd74b1cb4ecb3da851fc84e8eb3a094' AND quantity = 1500 THEN 9000
        WHEN transaction_id = '470605730' AND quantity = 100 THEN 600
        WHEN transaction_id = '4d84f6216009cc8ed80aed46bf9c5758' AND quantity = 100 THEN 200
        WHEN transaction_id = '472001967' AND quantity = 100 THEN 400
        WHEN transaction_id = '468411959' AND quantity = 500 THEN 1000
        WHEN transaction_id = '470269217' AND quantity = 100 THEN 400
        WHEN transaction_id = '464455884' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '464558595' AND quantity = 500 THEN 1000
        WHEN transaction_id = '465235777' AND quantity = 100 THEN 200
        WHEN transaction_id = '470273729' AND quantity = 100 THEN 400
        WHEN transaction_id = '472786541' AND quantity = 500 THEN 1000
        WHEN transaction_id = '465314737' AND quantity = 100 THEN 200
        WHEN transaction_id = '472764026' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e0c755aec3af9dfb539054413b467bd' AND quantity = 100 THEN 400
        WHEN transaction_id = '465171987' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4dfd43857e3c565a5cf3db46f08142d3' AND quantity = 100 THEN 300
        WHEN transaction_id = '4e0a77823a94745db63ecd4f028ef6ec' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e15515c6b81e4995767ef44778ed176' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04743593' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04796236' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04932505' AND quantity = 500 THEN 1000
        WHEN transaction_id = '470214465' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e2218f364f3895bb5cb324f8895f5fd' AND quantity = 1500 THEN 9000
        WHEN transaction_id = 'A-S04810850' AND quantity = 1500 THEN 9000
        WHEN transaction_id = 'A-S04906286' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04760906' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04866863' AND quantity = 10000 THEN 20000
        WHEN transaction_id = 'A-S04832369' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4e0e596e31e000b5dddd524670bc31e3' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04836971' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04774186' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04874304' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04780891' AND quantity = 100 THEN 200
        WHEN transaction_id = '468529994' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04852324' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S05001205' AND quantity = 500 THEN 2000
        WHEN transaction_id = '462205104' AND quantity = 100 THEN 300
        WHEN transaction_id = '463478612' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dca8648168ab68eedfa6e414ca66cc8' AND quantity = 500 THEN 1000
        WHEN transaction_id = '469978539' AND quantity = 100 THEN 300
        WHEN transaction_id = '472255156' AND quantity = 100 THEN 200
        WHEN transaction_id = '471511680' AND quantity = 100 THEN 300
        WHEN transaction_id = '471897196' AND quantity = 25000 THEN 50000
        WHEN transaction_id = '471262390' AND quantity = 100 THEN 300
        WHEN transaction_id = '4da92866687302c26432c94e1faf6b3d' AND quantity = 100 THEN 200
        WHEN transaction_id = '465361137' AND quantity = 500 THEN 1000
        WHEN transaction_id = '466603627' AND quantity = 100 THEN 300
        WHEN transaction_id = '467676361' AND quantity = 100 THEN 300
        WHEN transaction_id = '469101304' AND quantity = 100 THEN 200
        WHEN transaction_id = '471262069' AND quantity = 100 THEN 300
        WHEN transaction_id = '463015122' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4dcadb22e874df2870e0b0476fb8a77e' AND quantity = 500 THEN 1000
        WHEN transaction_id = '468091622' AND quantity = 100 THEN 300
        WHEN transaction_id = '468397207' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '465986061' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '471404628' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e2f727f4fdad1b0b1ffa24181937dc6' AND quantity = 100 THEN 200
        WHEN transaction_id = '4db4326beee48670d5ded24125a49d90' AND quantity = 100 THEN 200
        WHEN transaction_id = '468607780' AND quantity = 100 THEN 200
        WHEN transaction_id = '471112468' AND quantity = 500 THEN 1000
        WHEN transaction_id = '466545437' AND quantity = 500 THEN 1000
        WHEN transaction_id = '467273524' AND quantity = 100 THEN 200
        WHEN transaction_id = '4db4580c48e8878ec855dc410e963e84' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4da48bf4b821a432d672c84fb9a32f09' AND quantity = 100 THEN 300
        WHEN transaction_id = '4d7f5243cc9271050f2c3144b3852f00' AND quantity = 1500 THEN 9000
        WHEN transaction_id = '4d90c82cde0c954aa8569a43a78f3cf3' AND quantity = 500 THEN 1000
        WHEN transaction_id = '465064334' AND quantity = 100 THEN 300
        WHEN transaction_id = '464240660' AND quantity = 100 THEN 200
        WHEN transaction_id = '471370848' AND quantity = 100 THEN 200
        WHEN transaction_id = '467660647' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e349da05d0eb627e257b7445da36e05' AND quantity = 100 THEN 300
        WHEN transaction_id = '466044636' AND quantity = 100 THEN 600
        WHEN transaction_id = '4dca01f299f9934abd9e9d4e0db46cc4' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04766083' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04820549' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04832355' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04875555' AND quantity = 1500 THEN 7500
        WHEN transaction_id = 'A-S04889090' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04896017' AND quantity = 500 THEN 1000
        WHEN transaction_id = '469879777' AND quantity = 100 THEN 200
        WHEN transaction_id = '471090616' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4e21f46bef2fa821f5ab5948c494889d' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04981398' AND quantity = 100 THEN 200
        WHEN transaction_id = '462168352' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04766158' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04809694' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04851631' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04746227' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04776236' AND quantity = 1500 THEN 6000
        WHEN transaction_id = '467380835' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04930625' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04730703' AND quantity = 1500 THEN 9000
        WHEN transaction_id = 'A-S04753308' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04880853' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04930933' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e2ae4111b90646e7dfaa4480f934dd9' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04754088' AND quantity = 100 THEN 200
        WHEN transaction_id = '464320001' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04973523' AND quantity = 500 THEN 2000
        WHEN transaction_id = 'A-S04738496' AND quantity = 100 THEN 200
        WHEN transaction_id = '472769900' AND quantity = 100 THEN 300
        WHEN transaction_id = '467422216' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04980837' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04742938' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04745303' AND quantity = 1500 THEN 6000
        WHEN transaction_id = 'A-S04934585' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04777585' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '463102582' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04754198' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04756317' AND quantity = 1500 THEN 6000
        WHEN transaction_id = '463333647' AND quantity = 100 THEN 200
        WHEN transaction_id = '4de8d38d1af6312d5d1a954512bc40ff' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e378ffd782ecedac592254137866692' AND quantity = 100 THEN 300
        WHEN transaction_id = '472395721' AND quantity = 100 THEN 300
        WHEN transaction_id = '472604869' AND quantity = 100 THEN 300
        WHEN transaction_id = '4dbd82d68fd2c3a23a9d284a96a3746d' AND quantity = 100 THEN 200
        WHEN transaction_id = '467405309' AND quantity = 500 THEN 1000
        WHEN transaction_id = '467933908' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e39ecddfd25ee8eedef834278aa16c1' AND quantity = 100 THEN 300
        WHEN transaction_id = '463717852' AND quantity = 100 THEN 200
        WHEN transaction_id = '465799746' AND quantity = 100 THEN 200
        WHEN transaction_id = '472083895' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4e2ffaa14c9a3612591bdf4f8b89003e' AND quantity = 100 THEN 200
        WHEN transaction_id = '469167424' AND quantity = 100 THEN 200
        WHEN transaction_id = '4de84f210325f6f5ec206b4c29936127' AND quantity = 100 THEN 500
        WHEN transaction_id = '4df143c25239cd0d2df48443428ffcfd' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e23bc5b5647c5eb1b42554d73bfb70e' AND quantity = 1500 THEN 6000
        WHEN transaction_id = 'A-S04730795' AND quantity = 1500 THEN 7500
        WHEN transaction_id = 'A-S04752386' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04861345' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04871809' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e359cc5f30d6450ae1a9a4da1bef49c' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04909656' AND quantity = 100 THEN 200
        WHEN transaction_id = '471913243' AND quantity = 100 THEN 300
        WHEN transaction_id = '462954538' AND quantity = 100 THEN 300
        WHEN transaction_id = '4d9f63e2d0ec69eac754f843b496eeb7' AND quantity = 100 THEN 300
        WHEN transaction_id = '4db1ea0b4a7902dfb9f4a34f7ca7c71a' AND quantity = 100 THEN 300
        WHEN transaction_id = '4de24ce95ceca7b043246d4075874fd3' AND quantity = 500 THEN 1000
        WHEN transaction_id = '467101253' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '471056569' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04733668' AND quantity = 1500 THEN 4500
        WHEN transaction_id = 'A-S04741618' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04756459' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04874465' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04880705' AND quantity = 1500 THEN 4500
        WHEN transaction_id = 'A-S04911030' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04916274' AND quantity = 100 THEN 300
        WHEN transaction_id = '469319924' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04943766' AND quantity = 1500 THEN 9000
        WHEN transaction_id = 'A-S04943835' AND quantity = 1500 THEN 9000
        WHEN transaction_id = 'A-S04982258' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04746845' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04756043' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04761141' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04761256' AND quantity = 100 THEN 200
        WHEN transaction_id = '464362836' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04810258' AND quantity = 1500 THEN 6000
        WHEN transaction_id = '465350809' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04854244' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04783878' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04808059' AND quantity = 500 THEN 1500
        WHEN transaction_id = 'A-S04881690' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04886470' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04906698' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S05014681' AND quantity = 100 THEN 300
        WHEN transaction_id = '470529557' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04862353' AND quantity = 100 THEN 500
        WHEN transaction_id = 'A-S04895441' AND quantity = 25000 THEN 50000
        WHEN transaction_id = 'A-S04907661' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04915050' AND quantity = 1500 THEN 9000
        WHEN transaction_id = '472478297' AND quantity = 100 THEN 200
        WHEN transaction_id = '469075212' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4d9b9733c76126ffa07c0f4383aa41ef' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '4ddd98d27f792aa5ad579d489ba7bf58' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '468559420' AND quantity = 100 THEN 200
        WHEN transaction_id = '471984493' AND quantity = 500 THEN 1000
        WHEN transaction_id = '471976883' AND quantity = 500 THEN 1000
        WHEN transaction_id = '462647404' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '464707999' AND quantity = 100 THEN 200
        WHEN transaction_id = '465745539' AND quantity = 100 THEN 500
        WHEN transaction_id = '4dd00db6b4e250c54143fd43aca81adb' AND quantity = 100 THEN 400
        WHEN transaction_id = '4dd1d73dede76a601ddf87414baf0eaa' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dde1a81c61e0affe8ec3e406bb1c064' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4e00bf6243702c9d8cc06641f58d9b0c' AND quantity = 100 THEN 300
        WHEN transaction_id = '469294576' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04739367' AND quantity = 1500 THEN 15000
        WHEN transaction_id = 'A-S04905558' AND quantity = 500 THEN 1000
        WHEN transaction_id = '463808323' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '469052077' AND quantity = 100 THEN 200
        WHEN transaction_id = '4decd4399d9c49345985a748418a5e02' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dfc1b0ca78a1c45e7ec464f89bfe8b5' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04843725' AND quantity = 500 THEN 1000
        WHEN transaction_id = '467102448' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04895174' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e3def9b9f25ab85e7ddbc4173bb1060' AND quantity = 100 THEN 200
        WHEN transaction_id = '471114190' AND quantity = 1500 THEN 9000
        WHEN transaction_id = '472702646' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04829465' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04970391' AND quantity = 500 THEN 1500
        WHEN transaction_id = 'A-S04892403' AND quantity = 1500 THEN 7500
        WHEN transaction_id = '470466896' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04941547' AND quantity = 100 THEN 200
        WHEN transaction_id = '470791303' AND quantity = 100 THEN 500
        WHEN transaction_id = '464339188' AND quantity = 100 THEN 200
        WHEN transaction_id = '468759058' AND quantity = 500 THEN 1000
        WHEN transaction_id = '462385021' AND quantity = 100 THEN 200
        WHEN transaction_id = '465079446' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04947601' AND quantity = 100 THEN 200
        WHEN transaction_id = '4db16de58b02eb8050d3b04c798a1c77' AND quantity = 1500 THEN 6000
        WHEN transaction_id = '467069847' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04737139' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dbf2d63f36aeee7435f1e4c818773b9' AND quantity = 100 THEN 300
        WHEN transaction_id = '466806889' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04877147' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04988444' AND quantity = 100 THEN 200
        WHEN transaction_id = '472776885' AND quantity = 500 THEN 2000
        WHEN transaction_id = '470635182' AND quantity = 500 THEN 1000
        WHEN transaction_id = '465077392' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04944959' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04779935' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04796306' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04995798' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04937897' AND quantity = 100 THEN 300
        WHEN transaction_id = '471319729' AND quantity = 100 THEN 200
        WHEN transaction_id = '466754383' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e28ddb760032d427b9eb741c49d17dd' AND quantity = 1500 THEN 9000
        WHEN transaction_id = '464433303' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '469338915' AND quantity = 100 THEN 300
        WHEN transaction_id = '4e17fde5b871ef7e53fff94bbdabb08c' AND quantity = 1500 THEN 9000
        WHEN transaction_id = '471263881' AND quantity = 100 THEN 300
        WHEN transaction_id = '4d93d00ae3e538caeb800044308df210' AND quantity = 100 THEN 200
        WHEN transaction_id = '4da947ccd9c0f8d162c36b49e788ab7a' AND quantity = 100 THEN 700
        WHEN transaction_id = 'A-S04816777' AND quantity = 500 THEN 1000
        WHEN transaction_id = '464712967' AND quantity = 100 THEN 200
        WHEN transaction_id = '471364021' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04782271' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04986646' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04940423' AND quantity = 100 THEN 200
        WHEN transaction_id = '471279408' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dcdce5ffa4d03f050f449417c826501' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '467213438' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04884494' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e305da68fe4cedee7997c4c728df641' AND quantity = 100 THEN 200
        WHEN transaction_id = '4da403fe4c77ef9b41e36f4821ba7fb5' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '465733541' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04900714' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04905066' AND quantity = 1500 THEN 9000
        WHEN transaction_id = 'A-S04905400' AND quantity = 100 THEN 300
        WHEN transaction_id = '472322405' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '467685363' AND quantity = 100 THEN 300
        WHEN transaction_id = '470961168' AND quantity = 100 THEN 200
        WHEN transaction_id = '464740195' AND quantity = 100 THEN 400
        WHEN transaction_id = '468753841' AND quantity = 100 THEN 300
        WHEN transaction_id = '471366287' AND quantity = 100 THEN 200
        WHEN transaction_id = '462377528' AND quantity = 100 THEN 200
        WHEN transaction_id = '4de5206848dcb12cdd89604b1493d266' AND quantity = 100 THEN 200
        WHEN transaction_id = '4de311c5f6fd0187cb299146cb8307c6' AND quantity = 500 THEN 1000
        WHEN transaction_id = '468082888' AND quantity = 100 THEN 200
        WHEN transaction_id = '469492931' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04952090' AND quantity = 100 THEN 300
        WHEN transaction_id = '463131642' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e0689d1a090425544bc3948ca9634c7' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04857703' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04886138' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04784081' AND quantity = 100 THEN 200
        WHEN transaction_id = '4db432ce4e70bf90260f994ca4a49d5f' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '471991797' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4e1f1d3d5b94d0e514859f4a8eaea9b8' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '463068397' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4d9f8b901324cde11ea80c4071ab438a' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e2e356a324d84e93f4f5d47f4a63a7a' AND quantity = 100 THEN 300
        WHEN transaction_id = '4e389ab500f77c4654c3844f98a1153a' AND quantity = 100 THEN 300
        WHEN transaction_id = '465290370' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4dcd0213d8b9b86c58011241ea90c913' AND quantity = 100 THEN 200
        WHEN transaction_id = '470601074' AND quantity = 100 THEN 200
        WHEN transaction_id = '462889478' AND quantity = 100 THEN 200
        WHEN transaction_id = '465085834' AND quantity = 100 THEN 200
        WHEN transaction_id = '462327262' AND quantity = 100 THEN 400
        WHEN transaction_id = '472237633' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04733524' AND quantity = 1500 THEN 4500
        WHEN transaction_id = 'A-S04895615' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04897245' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '471536096' AND quantity = 1500 THEN 7500
        WHEN transaction_id = '4e2ef6b6be826f251684564dc885a301' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04812167' AND quantity = 100 THEN 300
        WHEN transaction_id = '472369318' AND quantity = 1500 THEN 4500
        WHEN transaction_id = 'A-S05010337' AND quantity = 100 THEN 300
        WHEN transaction_id = '462822339' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e21a61e881b5f1fc39b8e4928a50abc' AND quantity = 100 THEN 200
        WHEN transaction_id = '465925341' AND quantity = 100 THEN 400
        WHEN transaction_id = '471252780' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04804446' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '466900276' AND quantity = 100 THEN 200
        WHEN transaction_id = '471666848' AND quantity = 100 THEN 200
        WHEN transaction_id = '470015829' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04831792' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e0b6a2470340e1038586f418d819bf4' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04861934' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04921011' AND quantity = 100 THEN 200
        WHEN transaction_id = '472579598' AND quantity = 100 THEN 200
        WHEN transaction_id = '471941180' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e06a22a22b315ce6241784dde8b74c6' AND quantity = 100 THEN 300
        WHEN transaction_id = '464242848' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04754802' AND quantity = 100 THEN 200
        WHEN transaction_id = '462161543' AND quantity = 100 THEN 300
        WHEN transaction_id = '4e1442a35ca1a57c3dce7a48779e9a42' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '471974238' AND quantity = 100 THEN 200
        WHEN transaction_id = '4df439a491fcbc6e5735254cf087d733' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04756778' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04910158' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04769461' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04832664' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04907727' AND quantity = 100 THEN 200
        WHEN transaction_id = '469955004' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04778236' AND quantity = 1500 THEN 7500
        WHEN transaction_id = 'A-S04805096' AND quantity = 1500 THEN 9000
        WHEN transaction_id = 'A-S04808311' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04810642' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04823264' AND quantity = 100 THEN 600
        WHEN transaction_id = 'A-S04887084' AND quantity = 100 THEN 200
        WHEN transaction_id = '4debf2a9685f302d7b6c134167a7a2cd' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04916733' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04941932' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04981775' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04915091' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04846549' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04732997' AND quantity = 500 THEN 1000
        WHEN transaction_id = '466440613' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04825015' AND quantity = 10000 THEN 20000
        WHEN transaction_id = '471324531' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04745836' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04752216' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04810780' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04970546' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e020031c6ef75f3a25457489cb7ee16' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04988706' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04892204' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04969141' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04981149' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04970345' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S05004445' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04968374' AND quantity = 100 THEN 200
        WHEN transaction_id = '470543768' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04726034' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04978894' AND quantity = 100 THEN 300
        WHEN transaction_id = '467177577' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04967150' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S05012267' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e20b3396ca936625ff0eb494fae3da5' AND quantity = 100 THEN 200
        WHEN transaction_id = '465122064' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4db5e850bd382d0e880db14df88d6b3e' AND quantity = 100 THEN 300
        WHEN transaction_id = '469304628' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'AD00FA60-1AC4-4255-9C7E-9437384D9C06' AND quantity = 30 THEN 60
        WHEN transaction_id = 'be628fb9-630e-4b5f-bdfc-0c00ef9b7206' AND quantity = 1 THEN 2
        WHEN transaction_id = '4d7c6b1d0a522dfe869cec466dbba4f2' AND quantity = 100 THEN 200
        WHEN transaction_id = '464534100' AND quantity = 100 THEN 200
        WHEN transaction_id = '463841079' AND quantity = 100 THEN 200
        WHEN transaction_id = '464836010' AND quantity = 100 THEN 200
        WHEN transaction_id = '467694615' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e0725b54a6c54363d583b413694e087' AND quantity = 100 THEN 300
        WHEN transaction_id = '471583121' AND quantity = 100 THEN 200
        WHEN transaction_id = '470867467' AND quantity = 100 THEN 400
        WHEN transaction_id = '465148018' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '471094175' AND quantity = 100 THEN 200
        WHEN transaction_id = '472663148' AND quantity = 100 THEN 200
        WHEN transaction_id = '469668238' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '463408593' AND quantity = 100 THEN 300
        WHEN transaction_id = '4e029f5fa18251a94008ef46d086545c' AND quantity = 100 THEN 200
        WHEN transaction_id = '470714145' AND quantity = 100 THEN 300
        WHEN transaction_id = '462936729' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04759950' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04799085' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04800132' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04805913' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04806774' AND quantity = 500 THEN 1000
        WHEN transaction_id = '469953708' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4d7ba07130001b09cdfd734086a8f71b' AND quantity = 100 THEN 300
        WHEN transaction_id = '4dbe2afcc2f9bab2a10a354cb094545d' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '471870639' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d7ee48d95ea6f295936fc4c6ab215d5' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dd288265001196048f8324415be6888' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04947217' AND quantity = 100 THEN 300
        WHEN transaction_id = '470798506' AND quantity = 100 THEN 300
        WHEN transaction_id = '467118645' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S05010506' AND quantity = 100 THEN 200
        WHEN transaction_id = '470534906' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e1a29eb98b2c33704c0604e8684bd24' AND quantity = 100 THEN 200
        WHEN transaction_id = '4ddae6d31d08b6e0796d824b9ba99aa6' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'b9c31f1d-012e-4230-a347-9b80f8b7af16' AND quantity = 1 THEN 2
        WHEN transaction_id = '4de459df8e84cbffe260f045dd86dad1' AND quantity = 100 THEN 200
        WHEN transaction_id = '471961965' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04874209' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04969444' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04987699' AND quantity = 100 THEN 300
        WHEN transaction_id = '466668686' AND quantity = 100 THEN 200
        WHEN transaction_id = '472432620' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04891506' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4dcdc188ed61f194aade01409d9f8003' AND quantity = 100 THEN 200
        WHEN transaction_id = '465384048' AND quantity = 100 THEN 200
        WHEN transaction_id = '471231793' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S05008840' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dd2da268e1c3a653ac6bb46f5873efa' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '469580038' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4debd12abc3ca39cccb42a4e7ea17b2d' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04992622' AND quantity = 100 THEN 300
        WHEN transaction_id = '470208164' AND quantity = 100 THEN 300
        WHEN transaction_id = '4da887c082a65ca6e57ea94394991896' AND quantity = 500 THEN 2000
        WHEN transaction_id = '4ddb6583de6de89979eed0440789a1e0' AND quantity = 100 THEN 500
        WHEN transaction_id = '468525489' AND quantity = 100 THEN 200
        WHEN transaction_id = '4de741f701b36f1e7a303848be8f3204' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04872583' AND quantity = 500 THEN 1500
        WHEN transaction_id = '4d8f9503ac2bba5972571f48d0924ed2' AND quantity = 100 THEN 300
        WHEN transaction_id = '472138042' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04727754' AND quantity = 100 THEN 200
        WHEN transaction_id = '467427683' AND quantity = 500 THEN 1000
        WHEN transaction_id = '468504582' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04739777' AND quantity = 100 THEN 400
        WHEN transaction_id = 'A-S04848102' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04845396' AND quantity = 100 THEN 200
        WHEN transaction_id = '467316051' AND quantity = 100 THEN 400
        WHEN transaction_id = '470539088' AND quantity = 25000 THEN 50000
        WHEN transaction_id = 'A-S04929153' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e2c781676cf529cdf123346489b3bd9' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04846491' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04798255' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04809149' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04863241' AND quantity = 100 THEN 200
        WHEN transaction_id = '472193355' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '472447889' AND quantity = 100 THEN 600
        WHEN transaction_id = 'A-S04923147' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e09c93037e7e8930aee3945b78701c8' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04972511' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04974068' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04974874' AND quantity = 100 THEN 300
        WHEN transaction_id = '4db92bd6f2098f61be7d7a459ca642d8' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04794998' AND quantity = 1500 THEN 9000
        WHEN transaction_id = 'A-S04906117' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4dfd0f4adea393957ed9a741a0a9f612' AND quantity = 100 THEN 500
        WHEN transaction_id = '4e07463dfa6b0adda6b61d43ceb2afa3' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04913953' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04777389' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04767945' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04933874' AND quantity = 100 THEN 300
        WHEN transaction_id = '468221000' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04968818' AND quantity = 25000 THEN 50000
        WHEN transaction_id = 'A-S04959409' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04831265' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04985390' AND quantity = 1500 THEN 9000
        WHEN transaction_id = 'A-S04768253' AND quantity = 1500 THEN 4500
        WHEN transaction_id = 'A-S04848682' AND quantity = 100 THEN 200
        WHEN transaction_id = '472756387' AND quantity = 100 THEN 300
        WHEN transaction_id = 'd1c12e8d-4d98-40f8-b059-cc995b761131' AND quantity = 10 THEN 20
        WHEN transaction_id = '4dbfbe19aa02914095da70418ea1b4e7' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dbb3f9acb59aa4ddec15b470798a941' AND quantity = 100 THEN 200
        WHEN transaction_id = '467743121' AND quantity = 100 THEN 300
        WHEN transaction_id = '469506876' AND quantity = 100 THEN 200
        WHEN transaction_id = '471649804' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e3eaba4dda8b584160ef24af195d9ef' AND quantity = 100 THEN 200
        WHEN transaction_id = '4d863e940fdc0e36318f7e457b92827a' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '464513302' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '467122398' AND quantity = 1500 THEN 7500
        WHEN transaction_id = '4de86b6633324810b0489f4a84a9b674' AND quantity = 100 THEN 300
        WHEN transaction_id = '4dfb36006395e7c3545bf64156967916' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e01deb717c192c28dc1cd4e529fce90' AND quantity = 100 THEN 300
        WHEN transaction_id = '471163711' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e36f0545f0fe89505d1014240b7637b' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '472290300' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '465093863' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4da4c2c35f66298845de2c4bcfa6b587' AND quantity = 100 THEN 600
        WHEN transaction_id = '4dd9f837c56827adcfaa9f498393fc47' AND quantity = 100 THEN 200
        WHEN transaction_id = '467000946' AND quantity = 100 THEN 300
        WHEN transaction_id = '468328172' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e3764d1781da4c190422145f4953c49' AND quantity = 100 THEN 300
        WHEN transaction_id = '4da5b5cc9ac8c1d24946c94d6ea53087' AND quantity = 100 THEN 200
        WHEN transaction_id = '463693354' AND quantity = 100 THEN 300
        WHEN transaction_id = '4dfc1b4240b0e7c42b192344498b3f92' AND quantity = 100 THEN 500
        WHEN transaction_id = '469526388' AND quantity = 100 THEN 200
        WHEN transaction_id = '469937853' AND quantity = 25000 THEN 100000
        WHEN transaction_id = '4e2bc509bba07078389b2548dca8b9c9' AND quantity = 100 THEN 200
        WHEN transaction_id = '462195844' AND quantity = 100 THEN 200
        WHEN transaction_id = '462244813' AND quantity = 100 THEN 200
        WHEN transaction_id = '464737540' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '464851966' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4ddd8735fe01d0961eb3ca40adafb5d1' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4ddd78a4bb921a099f370b4a54a01804' AND quantity = 100 THEN 300
        WHEN transaction_id = '469685158' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '471891935' AND quantity = 500 THEN 1000
        WHEN transaction_id = '465377627' AND quantity = 100 THEN 200
        WHEN transaction_id = '462377514' AND quantity = 100 THEN 400
        WHEN transaction_id = '464576493' AND quantity = 500 THEN 1500
        WHEN transaction_id = '465304552' AND quantity = 100 THEN 200
        WHEN transaction_id = '463603666' AND quantity = 100 THEN 300
        WHEN transaction_id = '464475036' AND quantity = 100 THEN 400
        WHEN transaction_id = '470483558' AND quantity = 100 THEN 300
        WHEN transaction_id = '465949632' AND quantity = 100 THEN 200
        WHEN transaction_id = '469507720' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e18c15a81360a1994f88d4e4f88a60b' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e2bb13623f5e7a1ee17b8497493418a' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e1bd7dbe10eb54fb8400143a6a22546' AND quantity = 500 THEN 1000
        WHEN transaction_id = '464427147' AND quantity = 100 THEN 200
        WHEN transaction_id = '467891962' AND quantity = 500 THEN 1000
        WHEN transaction_id = '468417439' AND quantity = 100 THEN 300
        WHEN transaction_id = '471033823' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e11b90a071d0325b63b274ca2a2d18a' AND quantity = 100 THEN 200
        WHEN transaction_id = '470266884' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4de70b19605652bf3259f345cc883dc1' AND quantity = 100 THEN 300
        WHEN transaction_id = '470526680' AND quantity = 100 THEN 300
        WHEN transaction_id = '462966954' AND quantity = 100 THEN 200
        WHEN transaction_id = '466811499' AND quantity = 100 THEN 200
        WHEN transaction_id = '470943683' AND quantity = 100 THEN 200
        WHEN transaction_id = '469290801' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e0f52e7a55b7ab4767db44377a632d4' AND quantity = 25000 THEN 125000
        WHEN transaction_id = '467392294' AND quantity = 500 THEN 1000
        WHEN transaction_id = '462197463' AND quantity = 100 THEN 300
        WHEN transaction_id = '4dbe94c1685c7b1a8e503f4ba6a37cd9' AND quantity = 500 THEN 1500
        WHEN transaction_id = '4e18a2a6e82c37945cbbd64599b13f4e' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4dc42824022f33a929b4e74dd8acfa8e' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e0c5d37492341f99cc94d46ac97051d' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '465673626' AND quantity = 100 THEN 300
        WHEN transaction_id = '467017321' AND quantity = 100 THEN 200
        WHEN transaction_id = '468766166' AND quantity = 100 THEN 200
        WHEN transaction_id = '463371186' AND quantity = 100 THEN 1000
        WHEN transaction_id = '4e1b2017508c0eb59e56c84495b1b66c' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e1f8b3824ceaac5574e144405a7869f' AND quantity = 500 THEN 1000
        WHEN transaction_id = '469058481' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e01bb6627673f3e403d824583be5ca2' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e2f8ce455a06bb66a234a4db4a31f69' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '471736056' AND quantity = 100 THEN 200
        WHEN transaction_id = '464484906' AND quantity = 500 THEN 1000
        WHEN transaction_id = '471400082' AND quantity = 100 THEN 200
        WHEN transaction_id = '471657379' AND quantity = 100 THEN 300
        WHEN transaction_id = '469145256' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '6D3634BF-6A8D-41D4-9804-2E1BBFC19361' AND quantity = 1 THEN 2
        WHEN transaction_id = '4e1b388ba7853cdf9b86f8483e8472db' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e33f7e8a9dddfab9189e84f088f53ec' AND quantity = 100 THEN 1000
        WHEN transaction_id = '4e2ff974699770505b06ff4b11b143cb' AND quantity = 500 THEN 1000
        WHEN transaction_id = '472068615' AND quantity = 1500 THEN 4500
        WHEN transaction_id = '463526745' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4df5937c28628a1fd545b64aeaab5e8e' AND quantity = 100 THEN 200
        WHEN transaction_id = '470632918' AND quantity = 100 THEN 300
        WHEN transaction_id = '471494475' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e25a54d0f071fabf4c8ef4059ad3060' AND quantity = 100 THEN 200
        WHEN transaction_id = '9dc08d08-aac3-4a16-8bff-20ea8a027045' AND quantity = 10 THEN 20
        WHEN transaction_id = '4dbffa6e678c80b328851c434aad2ca5' AND quantity = 100 THEN 200
        WHEN transaction_id = '463043740' AND quantity = 100 THEN 200
        WHEN transaction_id = '464751739' AND quantity = 1500 THEN 3000
        WHEN transaction_id = '4dbbe68f032210685c371444f288cc10' AND quantity = 100 THEN 200
        WHEN transaction_id = '462861473' AND quantity = 100 THEN 200
        WHEN transaction_id = '471966055' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04741111' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04782697' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04748671' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04872854' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04767177' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04773341' AND quantity = 1500 THEN 9000
        WHEN transaction_id = 'A-S04800800' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04801941' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04876853' AND quantity = 100 THEN 200
        WHEN transaction_id = '467945101' AND quantity = 500 THEN 1000
        WHEN transaction_id = '4e1121b81f32d64f28924f4c06b6b04e' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04954117' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04971481' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04983387' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04848153' AND quantity = 100 THEN 200
        WHEN transaction_id = '4e34be107401e63dfc442a4da1991cae' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04921403' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04733620' AND quantity = 1500 THEN 3000
        WHEN transaction_id = 'A-S04962184' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04987499' AND quantity = 500 THEN 1000
        WHEN transaction_id = '462928690' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04752965' AND quantity = 100 THEN 200
        WHEN transaction_id = 'A-S04873694' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dbc72d03bd60d454bf4db444f94fe32' AND quantity = 100 THEN 400
        WHEN transaction_id = '4e2f830e1ff1502f5404314913afc3c7' AND quantity = 100 THEN 200
        WHEN transaction_id = '4dd7880367db43b4aa9edc4297aa4e33' AND quantity = 100 THEN 500
        WHEN transaction_id = 'A-S04744542' AND quantity = 500 THEN 1000
        WHEN transaction_id = 'A-S04885861' AND quantity = 100 THEN 300
        WHEN transaction_id = 'A-S04902856' AND quantity = 1500 THEN 4500
        WHEN transaction_id = 'A-S04932975' AND quantity = 500 THEN 1000
    END
WHERE transaction_id IN (
    '4dc4c5cfe40a36aa5f5dcb4ac1b81bed',
    '466266510',
    '463728345',
    '4dabc2fcae027d506e3fd04bc1bf679b',
    '4dd071446e418ee30fd27845a381e478',
    '4d9e75106e14348e615a0d4e45adb168',
    '4db49be0bb0f4b0a24601a4e058ecadb',
    '4dd7c3688a7de88de93fbf4a699f87e7',
    '4e2e769c61282e6dce4a5d4d06b0a699',
    '462617176',
    '470873465',
    '472245946',
    '4db43749eae4c3578dd1f1461ea53042',
    '467237099',
    '471051770',
    '4d90c59ac2a49ff8071a9942798d12be',
    '462922280',
    '462759219',
    '4d9a38b2b2b132566fdf6749c2aec6ce',
    '4dd266bb5a90328f137bb242c28d2cbf',
    '4dd98095643daf3bdc80ee454f9de82e',
    '469870504',
    '472422055',
    'A-S04836748',
    'A-S04793197',
    '463052309',
    'A-S04803262',
    '4da0012caa0e8b9eb5e2a44369980ada',
    'A-S04735497',
    '4de1797ed5ab803da382e44f82825cbe',
    'A-S04870039',
    '4dec2b0afc2a7e2fe7232f4bdaa25d7f',
    'A-S04895122',
    'A-S04908640',
    'A-S04914976',
    'A-S04740194',
    '462903442',
    '4d8edc93a349cf5613fd5048f7bda710',
    '470472622',
    'A-S04957651',
    'A-S04968388',
    'A-S04759087',
    'A-S04759478',
    'A-S04795714',
    'A-S04804917',
    'A-S04877402',
    'A-S04857845',
    'A-S04867088',
    'A-S04782266',
    'A-S04768817',
    'A-S04815952',
    '4d9bf54dd8ec7bc90d1b654a83b56205',
    '471017049',
    '472394382',
    '4e0616f57ea86590716a3a446bb59376',
    '4e3ccbf8b50642b93ac9184171869602',
    '469853621',
    '4e3f076ece13ec3eea32764f68b0d207',
    'A-S04948255',
    'A-S04954534',
    'A-S04881669',
    '468726314',
    'A-S05013774',
    '462732063',
    '470543339',
    '466596929',
    'A-S04846209',
    'A-S04758650',
    'A-S04810619',
    '466769386',
    'A-S04885680',
    'A-S04903396',
    'A-S04907282',
    'A-S04962073',
    'A-S04971107',
    '471519841',
    '467295990',
    'A-S04883400',
    '472792489',
    'A-S04848733',
    '4e069af9b7babccac1dd4c4f34b0064c',
    'A-S04893491',
    '4db289011d14c8a1111d514a94b91b97',
    '4dd3a6683ee9e5e295bfb14342a1bf79',
    '465118777',
    'A-S04730407',
    '464673800',
    'A-S04832204',
    'A-S04728263',
    'A-S04792960',
    'A-S04945107',
    'A-S04930445',
    '464578024',
    '466625284',
    'A-S04932494',
    'A-S04957533',
    'A-S04880544',
    'A-S04854258',
    '464837426',
    '469282687',
    '469891703',
    '4df4415ead8a766c2a59e44a84b0e0ef',
    'A-S04795896',
    'A-S05010257',
    '464787873',
    '467483243',
    '4df0a5389a7cfb4fa45b784164bb3d93',
    '4e06b5fe43851d06221b224e5092c170',
    '466606174',
    '4de8366a392b1c5161e6f24369a35c8f',
    'A-S05000454',
    'A-S05001293',
    '4de16a10b0aee7907b29024af2b5174b',
    'A-S04820780',
    'A-S04833828',
    'A-S04999044',
    '4dd388fb3b997a49bf929040e5b60933',
    '4da45324f80c869152162f4c74a4e66a',
    '465281442',
    '4e155ca7595323d8ad3d454342a02204',
    'A-S04885810',
    '4dd1ebb40991f2d36b4d79467cba5236',
    '464276673',
    'A-S04804772',
    'A-S04857678',
    '468688718',
    '465974258',
    '4de6d4af8c5cef0bcd8f734acab71a39',
    '471610071',
    '466820909',
    '471983264',
    'A-S04807897',
    '470282334',
    '468116123',
    '469721782',
    'A-S04744127',
    'A-S04962973',
    'A-S04931317',
    '466311874',
    '4e2021d50c4f4fadf9cd4c49fbb16262',
    'A-S05011745',
    '4e2915db84bb92fbff50664488b3e598',
    'A-S04745768',
    'A-S04867997',
    'A-S04942027',
    '467403919',
    'A-S04857605',
    '4e246ddb09cf9d43d92a44441ab51c6d',
    '471012062',
    '465275029',
    '4dc0094072520cb9e77eca4be39daeea',
    '467218645',
    'A-S04903245',
    'A-S04988467',
    'A-S04837751',
    'A-S04743302',
    'A-S04929602',
    'A-S04789707',
    'A-S04803572',
    '4d840c80595b811bdfd1ae49a4b589ea',
    '4da8fb3a3a2e02abbf21504d2f8918a3',
    'A-S04877153',
    '4e2a36df85c6003ac8a1b54415aed1d3',
    '4e2b569d0781f609faee4144c89a2ce3',
    'A-S04835897',
    'A-S04867273',
    'A-S04752419',
    '4d8ef8daaffbc59fe04b5e4cbaacb428',
    '4da381bfb1291b40977f1441c5b9e702',
    '466468005',
    '471634944',
    'A-S04808773',
    'A-S04887660',
    'A-S04740312',
    '462969865',
    'A-S04763010',
    'A-S04801809',
    'A-S04805010',
    '469819514',
    'A-S04949644',
    'A-S04989002',
    'A-S04752409',
    'A-S04953637',
    '4d7f910bada598b4df6d6743bebe8977',
    'A-S04984044',
    'A-S04740079',
    '471104396',
    '4d90ba18baaead0ca4df234c42850108',
    'A-S04808005',
    'A-S04751912',
    'A-S04870189',
    '4d7fcb656dec90b780bc884cfbb12279',
    '4db252612710f6eda4fedd424982aae2',
    '465789623',
    '465777885',
    '4e3cfa6b81bf60142ce05c4a0a845aa7',
    '4da753f9bb21bebe25d838441bbc669a',
    '471871557',
    '466885837',
    '470172745',
    '470699979',
    '470513623',
    '4dcddf54a0f127e5308376445894e4b5',
    '4e393239868b92e076d3204b26889ff0',
    '471743236',
    '4d8b0505d7d90125f8d6154122ba21b8',
    'A-S04789198',
    '464551884',
    '467906220',
    'A-S04978031',
    '4e2f1b00dd893abcf9db92423e86914f',
    '462493849',
    '4d7e641decca9e11d9bf5645ca8070d0',
    'A-S04732750',
    '4d82ef76593a79e433e74b452a810bab',
    '4d8dc12c710bd226f31e00461ab09c3e',
    'A-S04793026',
    'A-S04823904',
    '467045043',
    '4dd9b84cfa4e4a4af30786433b8d90f5',
    'A-S04835689',
    'A-S04891899',
    'A-S04914385',
    'A-S04761299',
    'A-S04821308',
    'A-S04825856',
    'A-S04948129',
    '470654748',
    '471519800',
    '471595485',
    '4de315994163f33ad5f18042a8a338bf',
    '467428400',
    'A-S04886328',
    'A-S04933085',
    'A-S04973511',
    '471737501',
    '463418234',
    'A-S04805508',
    '472017092',
    'A-S04998013',
    'A-S04890681',
    'A-S04999745',
    '465537534',
    '4e2b800e7e24f8bf53901549a8ad0565',
    '471930860',
    '4e38562ac58a9bf104295845c2a505a4',
    '4ddfe38313d75f34eeb0f14ca3802c63',
    '463700737',
    '464051928',
    '465469483',
    '4dcaccbe313dc1924126e140f98d261a',
    '466052282',
    '4dbc6eff467e098ce49c1546df97bc85',
    '466641877',
    '469553708',
    '4e210dc53e0a445b435a4942d99be0dc',
    '4e2b02b1bbb8f88bdda9d6401b89291e',
    '472681598',
    'A-S04747322',
    '463347348',
    '4d93fa01ff30eebb509b074ebc8be320',
    'A-S04756333',
    'A-S05014713',
    '471660772',
    '471804582',
    'A-S04766954',
    'A-S04889876',
    'A-S04988805',
    'A-S04774635',
    'A-S04947768',
    'A-S04867886',
    'A-S04964800',
    '462993008',
    '463440781',
    'A-S04896350',
    '466160101',
    '466108980',
    'A-S04847091',
    '471922236',
    'A-S04788312',
    '471982920',
    'A-S04879947',
    '4df3bc25fcf944caa88d5e4271ac083d',
    'A-S04779149',
    'A-S04766755',
    '469057360',
    '467998319',
    '4d820a34430b34d40a37234250ba9b48',
    'A-S04883811',
    'A-S04945683',
    '4df79a80dce4acfc24579c46fa882526',
    'A-S04919037',
    'A-S04810977',
    '469049219',
    '4de7a69a67de8198b84eea4f5e8f6b2b',
    'A-S04909237',
    'A-S04915790',
    '462776564',
    'A-S04766233',
    'A-S04790769',
    'A-S04889548',
    '472291121',
    'A-S04728131',
    '4db9a2f3e340b243b4884a44d8a8bcc1',
    'A-S04816673',
    'A-S04873528',
    '469873081',
    'A-S04794657',
    '4dfaf54245f16cec2a4ac54739bb9398',
    '4da07e5d97f2199adee9b34705a1e89b',
    '4dca08f80de493d18a1d9b4018a53435',
    '462962403',
    '4d9163e7bf56603f28921c4d1f8d879c',
    '470428305',
    'A-S04977463',
    'A-S04980658',
    'A-S04957097',
    'A-S04894898',
    'A-S04895666',
    '466390311',
    '4e246fc5dddd2bb11c59344014a1f1f4',
    '471110609',
    'A-S04744516',
    'A-S04856557',
    'A-S04737339',
    '467979115',
    'A-S04811667',
    '471751409',
    'A-S04839688',
    '4e2522386728cf100d1a7c44329ff8d3',
    '4e260f37a8028546decb0a4ff2b273d0',
    '467931992',
    '462969722',
    '462925037',
    'A-S04899201',
    '4e0c3fb044a9c8dbdd87354b11bb16d0',
    'A-S04787131',
    'A-S04797307',
    'A-S04880426',
    'A-S04982601',
    '466238476',
    '470190178',
    '462984153',
    '4e3c938ac04cc4603a10a54d1b9bf2cf',
    'A-S04986206',
    'A-S04808378',
    '469320052',
    '470156363',
    '471590576',
    '470795972',
    'A-S04793018',
    'A-S04826396',
    'A-S04913715',
    'A-S04816301',
    '470540818',
    'A-S04879178',
    'A-S04919371',
    'A-S04914913',
    'A-S04982059',
    'A-S04987471',
    '472046751',
    '462190730',
    '465739808',
    '4ddb213996977a4d37809e4b9e91ec46',
    'A-S04736388',
    'A-S04840121',
    'A-S04745079',
    'A-S04785147',
    'A-S04774207',
    'A-S04806562',
    '466019375',
    'A-S04920678',
    'A-S04971344',
    '464044050',
    '467899646',
    '468258056',
    '4e1af84cc686419594f02043d497d93e',
    '4dd1d6064de7fae6b916b2497a9cc716',
    '4e3a49e153a61a245e73d84be0af7ec1',
    '4e3ea902269a7cbe273bf04a9498c63d',
    '463409816',
    '4d951257ef971e708f82874f99a78d43',
    '470105961',
    '4e07256ac2127de04b0df242b4abbe73',
    '464554461',
    'A-S04786851',
    'A-S04789333',
    'A-S04827803',
    'A-S04871393',
    'A-S04908865',
    'A-S04947443',
    'A-S04992950',
    '472785517',
    '4dfc6d785b734446d1e89546aa814b3c',
    '471775849',
    '471949004',
    'A-S04852486',
    'A-S04937950',
    '472603698',
    '467904028',
    '467653243',
    '463191487',
    'A-S04834057',
    'A-S04996663',
    'A-S04911804',
    '4e05b4d4167a66581dc981489a964d31',
    '463738424',
    '4d9d800bc0efa6ae4e623a4d219debf7',
    '464515648',
    '470597208',
    'A-S04732923',
    'A-S04905828',
    '468180043',
    '4e2f4da9182dcd204e16274965868e64',
    '4dbf8a968f9dc413eb0e424fdb857d3f',
    'A-S04937198',
    '471644015',
    'A-S04782412',
    '4dd1e144eb195fc9e55da74cd8b09cc6',
    '466805274',
    'A-S04837339',
    'A-S04838928',
    'A-S04773002',
    '470659510',
    '469832478',
    '4e25d2ea90ab37bfa8a2ce4aedadc5e7',
    'A-S04838211',
    'A-S04793754',
    'A-S04804794',
    '4dbd149e99b18646e35648416093cec5',
    'A-S04963336',
    '462503468',
    '466269396',
    '4e38c7b6b8507eb3e175ca43cd8c3797',
    'A-S04892242',
    '471059113',
    'A-S04955826',
    '4ddebafe0802126eff856041559bd368',
    '4da26db7129b1a8b01a3f8483da8f229',
    '4d819cf4786eda4d90ebab4683a5d022',
    '4e35177f06a9e4cbd13778472a8106df',
    '465352318',
    '468028641',
    '4e0cd9bc26e46a2cbe11d942b7888344',
    '470856611',
    'A-S04744020',
    'A-S04993029',
    'A-S04789766',
    '4db210425e2077f79caa2c402fa0aa6b',
    '467671193',
    '467618596',
    '469971783',
    'A-S04815203',
    'A-S04954472',
    '4d8c85681ba6d4feeee9d94791874802',
    '464486683',
    '464659235',
    '4db268f3045ea28217165544f3b802f7',
    '4db5e43bfd59b70ecc70034de593d3b9',
    '466350254',
    '467388958',
    '4e23ab9622459d9a2214c4403f9357e5',
    '472398083',
    '468534562',
    '465737361',
    '4e1a4de78add7480d35efc4398841fc1',
    '472463822',
    '466697036',
    '4e0ece3cd0475c7c4192ee4adea30880',
    '470516167',
    '464564918',
    '464662772',
    '4d95628ba2e9ba6296114f4e32acc68b',
    '462831242',
    '4e197a3eb1b50539785a9143f895ed1d',
    '468364801',
    '465745960',
    '465358215',
    '470467955',
    '4d97ea378b881dc3d5ebe6421694ba70',
    '470289602',
    '469777962',
    '470241483',
    '4e3e00945cf498f0a3cffe484696f11b',
    '4da3f7dbac6af0bc6141e74123823068',
    '465053554',
    '466805102',
    'B1C0CC62-8C65-4F66-9AEA-A720F708EBA4',
    '4e06ba3288f3ead9820cf244e0ac4da3',
    '470034349',
    '463888715',
    '472160420',
    '466857608',
    '468248741',
    '462705636',
    '4dc7bc1c9d0725ea101c434c49b59337',
    '470820501',
    '4dffa39f16fb43a0e0ef0b43f6940e15',
    '467402961',
    '471866593',
    '4d8b1bdc92c68a3f1636dd4df981d228',
    '4df7b71a7bde018b73a305478b80e845',
    '468779000',
    '464465748',
    '4dc43a6992ed53c091acd742d6845a3c',
    '466427522',
    '471433911',
    '463156338',
    '467420880',
    '472395024',
    '4d81c1eaf9422a87444d6c40d9be7cbf',
    '470299577',
    '466036498',
    '467012389',
    '4daf5635790cb09ac469be4d69baeed4',
    '467895914',
    '468606968',
    '465216143',
    '468843829',
    '465131759',
    '472431755',
    '465130920',
    '4dc56b7e65fe63334deed54598ab8ebc',
    '468269281',
    '4e2fb6067e0541675558c54d21aa0de3',
    '466967060',
    '462585153',
    '4e39efc32d65ecec421a634c82958acb',
    '467099831',
    '462719820',
    '4e2a8ff8a37d6614fe354b425d89cb2a',
    '73069a0d-348f-4b3d-8a4c-36afd61b2930',
    '462720559',
    '4d95be81943f09ea12d6424ce38a7231',
    '4d9de30a3deec23ca01c4341829dc919',
    '4da00d8912f205c4d52443499bae37bb',
    '464307042',
    '470467764',
    '470802151',
    '472093756',
    '4e2f5cb2c5a699ddfdbd1c486c96cdcb',
    '464268422',
    '465838160',
    '4dbefaea4700a42fd792844f67ac980b',
    '4ddb9690c6c9cd7aef60a141f9b5f5b7',
    '463397159',
    '464327428',
    '4e1bd5c75652137cac41bb411583a5c2',
    '469685166',
    '4db8e5e186a372f30f91cd44acad5237',
    '464468874',
    '4dad9ed594ea6e99a70ef240d0b07e1e',
    '469760400',
    'A-S04730520',
    'A-S04759638',
    '465324856',
    'A-S05001081',
    '468990742',
    'A-S04924001',
    'A-S04924595',
    'A-S04728827',
    'A-S04743305',
    'A-S04784779',
    '4dce8563c350b9af3ff0bb49a8a3d004',
    'A-S04753222',
    'A-S04806790',
    'A-S04809418',
    '4e1bf3fd3fc12a2b946fef4efdbbca66',
    '471029478',
    'A-S04975787',
    'A-S04856975',
    'A-S04884780',
    'A-S04918546',
    '4e01878d8ea6ca5dacf4b0429ca23751',
    'A-S04964566',
    '463377837',
    '463482325',
    '466052303',
    '4e39ecb3f444c5570802f24dec9933a8',
    '471858791',
    '465081446',
    'A-S04753969',
    'A-S04758808',
    'A-S04767140',
    '469471227',
    '470827241',
    'A-S04933895',
    '470775455',
    'A-S04768170',
    'A-S04961939',
    'A-S04979990',
    '4d886f4c199ddad5c683c9475cb5df65',
    'A-S04930050',
    '470309651',
    'A-S04776298',
    'A-S04798045',
    '472605957',
    'A-S04727665',
    'A-S04847510',
    '462924675',
    'A-S04876935',
    'A-S04853789',
    'A-S04786799',
    '472347848',
    '467133390',
    '465252468',
    'A-S04933702',
    'A-S04970175',
    '4e027242312585d2249b0d46d690971d',
    '471629579',
    '463132776',
    'A-S04853999',
    'A-S04999649',
    '469449251',
    'A-S04942326',
    '465207223',
    '468942087',
    'A-S04733478',
    '463697935',
    'A-S04852443',
    '468943285',
    '4e15796b225251bdcbc7b84a948efe0b',
    '470806383',
    '471219605',
    'A-S04799470',
    'A-S04822389',
    '4e06b66f373ca5d67d037a4e3c8cdb20',
    '467115774',
    '4dc855f878d1fa181eab5146609241d4',
    '466364469',
    '470030777',
    '471935781',
    'A-S04786758',
    'A-S04814693',
    'A-S04838811',
    'A-S04873244',
    'A-S04908853',
    'A-S04747986',
    'A-S04773277',
    'A-S04810125',
    '4e29f7aafb3baf00d7b7954a6bae8fce',
    'A-S05000673',
    'A-S04927080',
    'A-S04927364',
    'A-S04938393',
    '4e0c1453cffa8f7df4b4e5440bb2d195',
    'A-S04947025',
    'A-S04982709',
    '4dbdc44d1b93e7198055e8421c918727',
    '465983252',
    '4ddc697eb0afc4ae7153d844f091231f',
    '468288188',
    '468526472',
    '4e0c736eff1e78d42abf334409a1ed03',
    '4e2b0c5ba0a10c0fb7b2d74416ace3ac',
    '4d812b2a417fa30c8c4869495a9e98ce',
    'A-S04744588',
    'A-S04934518',
    'A-S04803710',
    '4db9432f9b411e45de7c6d4eac8a9541',
    '466010655',
    '472458266',
    'A-S04807228',
    'A-S04932621',
    '4e1c731ce897c21aed9e97440b8122f9',
    '467169416',
    '4e13662ec78c31522df025443c9d5ebe',
    '4e2b9fdba7fadd7d8d53584ef1afbcba',
    'A-S04854468',
    '462930301',
    '468967482',
    'A-S04854941',
    '466895075',
    '462270548',
    '462705813',
    'A-S04726328',
    '471249119',
    'A-S04828654',
    '471036364',
    '4e3927feea306889f943fe4c8bb218db',
    'A-S04839717',
    '465527086',
    'A-S04992668',
    '4de7805ee6298e60b34e1541bfb258b4',
    '472189131',
    'A-S05004202',
    '4ddbbf8dd7a6970cf5c1c0490e9db268',
    '4e312db2b5c61b9f0f4531437a9e34ae',
    '468447795',
    'A-S04902207',
    '470210463',
    '462477967',
    'A-S04727302',
    'A-S04851051',
    'A-S04812248',
    'A-S04853572',
    'A-S04863768',
    '470705807',
    'A-S04969040',
    '471225036',
    '4d89a830f3588ced7bbc8e44a2bb9f23',
    'A-S04863529',
    'A-S04944332',
    '472006378',
    'A-S04900470',
    'A-S04763288',
    '4e3917c7cea5077e804fd8498e86a0bf',
    '4e3986f4784d1cc2f05d1c49bbb65c05',
    'A-S04920517',
    'A-S04920956',
    'A-S04769160',
    'A-S04793796',
    '467945848',
    '470664445',
    'A-S04996156',
    'A-S04904714',
    'A-S04729403',
    '465328195',
    '468230644',
    '4e2fb77f9434fee571ad2043eea3023b',
    'A-S04814884',
    '467088805',
    'A-S04729565',
    'A-S04982223',
    '462847738',
    '4d92903d07293c6bca047c4bb3b8c909',
    '465628932',
    '464831393',
    '465432835',
    '465746308',
    '4dd519d3b9a077a9163df24a9bb16aa8',
    '469598302',
    '471025715',
    '4e33a66db623c40a3d5af34bce8d91db',
    '472477088',
    '4e171a25daa36587907e7f41bbb9cba7',
    '471290955',
    '467973333',
    '465775913',
    '467025102',
    '470265465',
    '470993096',
    '4d9509a38439f1cd9a310041ef9f1ebe',
    '462930223',
    'A-S04837244',
    'A-S04846480',
    'A-S04768552',
    'A-S04794297',
    '466758125',
    'A-S04853942',
    'A-S04812030',
    'A-S04882902',
    'A-S04902424',
    '468816359',
    '472660978',
    'A-S04857293',
    'A-S04863291',
    '472564500',
    'A-S05009711',
    'A-S04944692',
    '471359201',
    'A-S04979215',
    'A-S04932268',
    '470946841',
    '463483570',
    '467471601',
    '467807964',
    '4de7301681b4bb223b82954f25a11757',
    '4df273560fe5e4edd20e1c46f6a832e7',
    '470989779',
    '465358858',
    'A-S04922802',
    'A-S04924012',
    'A-S04956404',
    'A-S04986913',
    '464792014',
    '4dece73afd7d0175bc3e3f419e9c4382',
    '464248902',
    '465568733',
    '4df2a490306ae0ee13a50b429b95d419',
    '469812482',
    'A-S04864144',
    '4d886e9b2f1b35d152dd3b4389b59c90',
    '465099444',
    '4e30647236bbd4d80ed23e4d1594a536',
    'A-S04951085',
    '471054044',
    '4deea66543e712dffd73e64a92a5d9dd',
    '462487186',
    'A-S04876299',
    'A-S04844729',
    'A-S04854231',
    '463733103',
    'A-S04929433',
    '4d839342e2f942e63a8e8d4a23bd97bb',
    '467132556',
    '4e1b5bc0194f7fa2fab53b4c79b2b40d',
    'A-S04732875',
    '467155335',
    'A-S04875097',
    'A-S04955672',
    '465852486',
    '464265885',
    'A-S04776213',
    'A-S05005335',
    '4de96184c8d529e2cd8c024e26a98fba',
    '469334753',
    '4dc89b38b5da01b3979f0f454186eac5',
    '464491549',
    '468554965',
    'A-S04938281',
    '464309451',
    'A-S04801860',
    '467927608',
    '472319185',
    '4dd9cdfafff26b994bff60405e8f776b',
    '465670758',
    'A-S04745940',
    'A-S04786903',
    '464519677',
    'A-S04901692',
    'A-S04920391',
    '462496830',
    '463481627',
    '4e0b587b0f97c9ff34e42746b69fb602',
    'A-S04841184',
    '463655348',
    'A-S04771911',
    '472375006',
    'A-S05005478',
    'A-S04936569',
    'A-S04986708',
    '467602828',
    'A-S04819553',
    '467971165',
    'A-S04740066',
    'A-S05013668',
    '4e2e74aaaea185f326301946dda07c49',
    'A-S04981994',
    '4e0ef265958f30ee4f20f84a61887934',
    '464470243',
    'A-S04798089',
    'A-S04881695',
    'A-S04963150',
    'A-S04809803',
    'A-S04821903',
    'A-S04998623',
    'A-S04986625',
    'A-S04782984',
    '4e11355146e73893364d7f45cba85e95',
    '462931326',
    '4d8c1eed93132429a0ee7b409da48bdc',
    '4d9e42c5183348db84f8744e449f94f9',
    '465083124',
    '4dbe8d2c4f09f3af4aab0447bdaaaafb',
    '463014273',
    '468395030',
    '466886263',
    '468253095',
    '468274051',
    '4e0806837654481dcb220043e9a5a1f9',
    '4e2597da01270db0380a9c45d4b4d4c2',
    '472395923',
    '467569424',
    '462215374',
    '472263327',
    '4df343ef21511079b4b1f349fc81c6de',
    '4e1094e3f45986a8213948438e949083',
    '468690486',
    '471097875',
    '471846741',
    'A-S04790841',
    'A-S04810746',
    'A-S04810823',
    'A-S04872116',
    'A-S05001816',
    '469813501',
    'A-S04743273',
    'A-S04943645',
    'A-S04759118',
    '466647914',
    'A-S04952707',
    'A-S04977415',
    'A-S04734551',
    '4da48479e4fe1229fb9d73413dbe43b3',
    '4daa78e1118799b810fada4112902dd7',
    '4e250a03af35bf23c0207244abbb36fc',
    'A-S04842757',
    '4da2c3dca5bd7b0349a5f044b49e9ad5',
    '471958481',
    '472285772',
    '4dea828e37af1aa9862c2044d6adfb25',
    '462493564',
    '4d9c914af2dc8cfb6c48cf4982a4bbc2',
    '4da82ef544bfd64d9ead584b67a31ef8',
    '4dae466497dc0b03f09e8f4569bc9655',
    '469246012',
    '468383247',
    '4db43435a4c0fb74b6a5254e1b8dc4db',
    '4df805ab7e839f0e61adc74ed4934935',
    '4da03060eda8f1abff4f0142d98a8fc1',
    'A-S04743599',
    'A-S04782560',
    'A-S04776536',
    'A-S04878544',
    'A-S04881074',
    'A-S04898263',
    'A-S04950049',
    'A-S04729956',
    '466295484',
    'A-S04758604',
    'A-S04772628',
    'A-S04801880',
    'A-S04735190',
    'A-S04865177',
    'A-S04898944',
    '468533128',
    '464058447',
    'A-S04989845',
    '4d9a12e0dfffb44d4de0ee4f218bdffa',
    'A-S04806324',
    'A-S04809640',
    'A-S04811952',
    'A-S04812851',
    'A-S04753685',
    '468679170',
    'A-S04919004',
    'A-S04921365',
    'A-S05009142',
    '472605364',
    '467311287',
    'A-S04893240',
    'A-S04900390',
    '468750009',
    '4e1bb4a6aaea4069f0c22441d09cd809',
    '4e0a70f05cc73fb7334cf249cab77c3a',
    '4da00c37a862cdf43d32284b05be5365',
    '464598470',
    '4db1c1e84b00ac9b800d344a7c92a5d3',
    '470246236',
    '462694048',
    '462256351',
    '4d92f839dc78ed5ad06e6041c2835663',
    '465314421',
    '465817147',
    '4dc447ecd52dd75d8c4ced44cab438f7',
    '472529521',
    '472481831',
    '4df0ed21bf8b8a9444ffdc42cdae6d9c',
    '4dd9d203e0b362cf3dc82e4d29bec13b',
    'A-S04882117',
    '468220313',
    'A-S04905348',
    'A-S04820520',
    'A-S05013862',
    '470237538',
    '463409580',
    'A-S04830121',
    'A-S04975638',
    'A-S04781426',
    'A-S04863483',
    'A-S04856521',
    'A-S04899465',
    '469781029',
    'A-S04975846',
    '4da0310c62e141767506d84988a1a68a',
    '464741147',
    '464452106',
    '4dd83dd8b68ed6d3c7ba4f40a198df5a',
    '4e084039c4bcf60806148e4a058c2193',
    'A-S04731728',
    '464026600',
    'A-S04913520',
    'A-S04813099',
    'A-S04852424',
    '4d98a3d46e0716c778f0b5451080b09a',
    'A-S04971502',
    '463676848',
    '464046016',
    '469014956',
    '470586572',
    '472418215',
    '464567679',
    '4df68fe495d027a71fffdd4b318f1333',
    'A-S04927241',
    'A-S04931434',
    '464010747',
    '467927186',
    '472539474',
    'A-S04759155',
    '463632924',
    '472678930',
    '464573751',
    '4dae61ae6626b522c8d2d140b7bcc773',
    '472172472',
    '4d99dd34674b6238e6320441efa60eb5',
    '4e08b7ebaef4772411f4db481db52084',
    '472383544',
    '4e24e513421bd0a8654c004e90b8991c',
    '463718064',
    '465980739',
    '469499245',
    '469914264',
    '464991220',
    '4de740336b315ab73306bf473bab9bb7',
    'A-S04900707',
    'A-S04842074',
    'A-S04731338',
    'A-S04763761',
    'A-S04788256',
    'A-S04885858',
    'A-S04916910',
    '4dd373018c0a1ccf7d76c8477b985a7b',
    '463718136',
    '464639911',
    '4d99ca9f490ff88eb3701547838ce56f',
    'A-S04801859',
    'A-S04904907',
    'A-S04945616',
    'A-S04952248',
    '462204301',
    '468049938',
    '4d8533ce55389c8f0a293f44ebbfde76',
    '471950855',
    '463630260',
    'A-S04872616',
    'A-S04863822',
    'A-S04911602',
    '4e100f328a13a57daf384d459985afa6',
    'A-S04901145',
    'A-S04938861',
    '4ddd47ff8232184247b8a44d7381d3dc',
    'A-S04944243',
    '4d8bebd3c966fe1a520e28427a974fc6',
    '4da8154ca222db9522c72146fda8a89d',
    '464471364',
    '4df1f46dd517735a6a22414888afb74c',
    '4e0b76dbb86fb9ce2bdd074d09a32ec5',
    '4dc2f008c870cb8ce509f24e6695cfe4',
    'A-S04751163',
    'A-S04797945',
    'A-S04841409',
    'A-S04933210',
    '471252371',
    '463129778',
    '4dfb2abd2e7bde8fa074d34a6aba3a8e',
    '469621582',
    '4dca5c5c7fdcd56131fbad4e8a8f0155',
    '463534566',
    '466258417',
    '468237035',
    '4dfb138cfbfd529b9cbabb46069ef80d',
    '4dfb130595b4c9b27408bb4479b1894f',
    '4e056ceef9e1f44768174145eca7050d',
    '471564977',
    '4e24ed40ba13f8ccc4292f4cde9db458',
    '4d91f649c46bdac749317441ec868448',
    '4daa17a18c67d229a8a23044cfb7d972',
    '4d7c128b847267c21a20cd4a2d87813f',
    '4d846cd5912a9939dbef5a4fbc8f063a',
    '464551289',
    '469075190',
    '471762796',
    '466151171',
    '465107277',
    '466018554',
    '4dbe2a874b8210170581b24f2391c891',
    '4e2a9c195234f0f438b1154c71a0dec7',
    '471936558',
    '4e34b4dfbf2fe66e173285402fa3cee8',
    'A-S04846034',
    'A-S04746870',
    '467885649',
    'A-S04897884',
    'A-S05006525',
    'A-S04924112',
    '470041891',
    'A-S04966734',
    'A-S04967505',
    'A-S04733430',
    'A-S04838209',
    '466382039',
    '4d9ad71d1f8e41577d65da4c3db41a55',
    '463739812',
    'A-S04794926',
    'A-S04838458',
    '465635945',
    '4dec66a1e43fe6086d0f574fe29ea0d1',
    'A-S04896434',
    'A-S04902820',
    '4d8444d86e21b99241d24c4bf5978511',
    '462655977',
    'A-S04741254',
    '463342517',
    'A-S04796108',
    'A-S04820543',
    'A-S04955100',
    '470753153',
    'A-S04905325',
    'A-S04959314',
    '471615096',
    'A-S04980925',
    'A-S04839254',
    'A-S04767119',
    '4d86e18106344994bbc859456892bae2',
    '462913308',
    'A-S04786803',
    'A-S04824462',
    '467295713',
    'A-S04909994',
    'A-S04820203',
    'A-S05005076',
    'A-S04916106',
    'A-S04920450',
    'A-S04924109',
    '4e07bbbfba7e46b5d5f2f749f1986f13',
    'A-S04950733',
    '4e1e4cd3367c8c7c15d38d4b4781a1bd',
    '471385085',
    'A-S05012533',
    'A-S04952039',
    'A-S04965985',
    '471646207',
    'A-S04983545',
    'A-S04756517',
    'A-S04803163',
    'A-S04856470',
    'A-S04877202',
    'A-S04854194',
    'A-S04828781',
    'A-S04835541',
    '4ddd1b71393ec195a73bb04b3289c5d0',
    'A-S04895361',
    '468252694',
    'A-S04963052',
    '4d8612428ffa05cd662638499a85903b',
    '463685623',
    '465867896',
    '466072238',
    '467995581',
    '469475070',
    '4e165d2096187f4eccc5be41fe8aa047',
    '4e0b87e4e3170672d14e2c42ab87f1f7',
    '471334261',
    '462998877',
    '468806071',
    '469028938',
    '469306683',
    'A-S04966743',
    'A-S04779066',
    '4db9c329fbe406dd0c30f84a3abc35ea',
    '4dd354756353db23a3fa464f5bb61dcf',
    '4e3470cb2ce5eb5f11d94545678d23ea',
    'A-S04912483',
    '4d80e6bffa7e63487da39c4a248a9801',
    '463347730',
    '4dd0ad564dd8095768f4924485a28ed9',
    'A-S05007874',
    'A-S04946211',
    'A-S04979073',
    'A-S04762413',
    '4d9fdfe17d4fede8fbf2be4faa860d47',
    '4dfbbf718a250934153d894e2bb32ba3',
    '470302028',
    '4e356a889e15843eda6c364d11985ab6',
    '469204114',
    'A-S04805382',
    'A-S04934067',
    'A-S04958131',
    'A-S04734216',
    'A-S04734922',
    'A-S04830312',
    '467940975',
    '4e0792e8395d2b19f39ac544cd9d02a8',
    'A-S04981385',
    'A-S04961157',
    'A-S04981844',
    'A-S04774502',
    'A-S04782733',
    'A-S04804119',
    '462417459',
    'A-S04794111',
    'A-S04780898',
    'A-S04854188',
    'A-S04964123',
    '4dd9d1c5d59b08ab13528e4ad2bb1573',
    'A-S04801897',
    'A-S04812174',
    '462319019',
    'A-S05008761',
    '472186186',
    '4e0bdc623ca98de3f8983f4801a4fc68',
    'A-S04871828',
    '462596914',
    'A-S04838322',
    'A-S04731114',
    '468438359',
    'A-S04902767',
    'A-S04905159',
    '470334473',
    'A-S04875824',
    'A-S04781012',
    '463147520',
    'A-S04893985',
    '471696070',
    'A-S04748815',
    'A-S04776667',
    'A-S04951686',
    'A-S04757531',
    '470464473',
    'A-S04874797',
    'A-S04752416',
    'A-S04848879',
    '465155983',
    '468149916',
    '472455645',
    '462217810',
    '4dad6b28eec41a669f4eed46fba07861',
    'A-S04846041',
    'A-S04727496',
    '463981610',
    'A-S05000841',
    'A-S05001373',
    '4d9b1331e56c42ba5ae94d4dcc8d0c2a',
    'A-S04933669',
    '470428075',
    'A-S04978250',
    'A-S04977853',
    '4de31d0143f7bc4861b06149719972bb',
    '469258167',
    '462463584',
    'A-S04757351',
    'A-S04893516',
    '464912997',
    '4e09c8bb9607b4593e14c44ab9b868dd',
    '472598645',
    'A-S04937350',
    'A-S04786458',
    'A-S04777121',
    '4df1a4b36598d256d9ea72419db8e04c',
    '463714248',
    '467552295',
    '470197455',
    '471936343',
    'A-S04986131',
    'A-S04791632',
    '4ddeac3fc54cad6c2ea9bb4dcca21662',
    '462662401',
    'A-S04932492',
    '463214827',
    '462754653',
    '4de1c8043f512f1da9a5ad40a5a7847e',
    '4de9339de46e111299a25a4cdb9e8b48',
    '468243597',
    '471911951',
    '4d9f49581aae5acb77276d4bd3a06d7c',
    '4d906d8fdfcaed1e39030848e987bb4c',
    'A-S04769641',
    '468734689',
    'A-S04912625',
    '469045576',
    'A-S05011689',
    'A-S04949964',
    'A-S04968658',
    'A-S04791070',
    'A-S04971084',
    '429a7e0d-5316-4c5c-b363-703d99e8533b',
    '472627228',
    '464142920',
    '472081781',
    '468269146',
    'A-S04801061',
    '465122424',
    'A-S04774212',
    '464847661',
    'A-S04990382',
    'A-S04788321',
    'A-S04913917',
    '469981085',
    '4e2fb098bdf78330118fbb40729e8b86',
    '467386179',
    '4dc3f03219bc1ee6e6494042a6a3bb6b',
    '4dc6f8c843df5a4e9226794812b99a7f',
    '471287265',
    '4d7b9c89ad2f00a590317a4b9cab6863',
    '462589949',
    '464840694',
    '466218394',
    '465571245',
    '465936041',
    '4dff70c6c4b79df0cd084d42648563eb',
    '4e07749043d7bbc31768ca47a8bc268e',
    '470945681',
    '4da0c3090a9e39baa99d584966a44ad2',
    '4e0c688ef8d31c9a193cac4da7a3dd8f',
    '4e20cb80e1e16ec4d509d54644aa189c',
    '462235959',
    '472107539',
    '4da43d4917cb1720a22a7f4174b0625b',
    '465514028',
    '4df0ec95872a3a36304c3c4fb184c889',
    '470748872',
    '471668807',
    'A-S04855097',
    '4d8b90d9b3b65f93b7e49b4818ad2c2a',
    'A-S04773375',
    'A-S04792578',
    'A-S04869968',
    'A-S04886636',
    'A-S04997411',
    '4e18e7cb9a23e793378e8e47eb847d5a',
    'A-S04733569',
    'A-S04842152',
    'A-S04783521',
    'A-S04813181',
    'A-S04818759',
    'A-S04874347',
    '467973463',
    '4df866b7e115d668557565499db6e44a',
    'A-S04942640',
    '4daedc0d67afa2c67b115949e2bfab09',
    '4dfc7fc203afdd575325b444b2ad296b',
    'A-S04961019',
    '471354855',
    'A-S04841890',
    '4dd286ee0a7a48d58bd7fb4e9886c4ca',
    'A-S04811906',
    'A-S04998142',
    'A-S04896342',
    'A-S04855659',
    'A-S04894196',
    'A-S04958982',
    'A-S05011363',
    'A-S04752038',
    '472514267',
    'A-S04911482',
    '468435146',
    'A-S04801013',
    '465318724',
    'A-S04857249',
    'A-S04862140',
    'A-S04740499',
    'A-S04864647',
    '4d99e41ee185f0a34abf384bd6b10da1',
    '46e0ae52-56ba-46ad-be1d-ead2d835c8db',
    'A-S04763378',
    'A-S04794293',
    '465993553',
    '4e00a6ebb1ed3175c9d1be435e838a28',
    'A-S04858395',
    'A-S04910515',
    '472536305',
    '463451770',
    '465966216',
    '4dd83e3e39959963fd64b34ad5919c41',
    'A-S04882212',
    'A-S04888808',
    'A-S04953931',
    '468656551',
    'A-S04870553',
    'A-S04885699',
    '472275946',
    '467165556',
    '4d964397cd82cedff7195048d7a9df69',
    'A-S04765453',
    'A-S04793491',
    'A-S04803509',
    'A-S04809497',
    'A-S05007411',
    'A-S04933777',
    '470797380',
    'A-S04974124',
    'A-S04975936',
    'A-S04981039',
    '4e2a682af514d24e16980a4d0e953b29',
    '4dce39384262f3c3bbacfa495f9ea966',
    'A-S04968292',
    'A-S04991667',
    'A-S04927830',
    'A-S04735900',
    '472321858',
    '468160966',
    'A-S04745283',
    'A-S04763913',
    'A-S04765380',
    'A-S04805104',
    'A-S04871527',
    'A-S04892638',
    'A-S05013763',
    'A-S04736957',
    '4dd35f493da17c13b86eb3416a9b8631',
    'A-S04877329',
    'A-S04977773',
    'A-S04978824',
    'A-S04751381',
    'A-S04792597',
    '465293882',
    'A-S04991810',
    'A-S04762042',
    'A-S04843653',
    'A-S04810712',
    'A-S04810874',
    'A-S04888306',
    'A-S04997744',
    'A-S04895479',
    '472287533',
    'A-S05007190',
    '472551953',
    'A-S04936202',
    'A-S04947466',
    '470724031',
    'A-S04874977',
    '4e356641d6dce11da590cf4bccbd5786',
    'A-S05008636',
    '4e14bd463f3a3b9454773f49dbab5c87',
    'A-S04833695',
    'A-S04969143',
    'A-S04809782',
    '467891360',
    'A-S04795632',
    '466034689',
    '467601313',
    'A-S04942657',
    '4e15c5c503fa58eedad11648c6b8deba',
    '4e1c10191afb112dfe7dc140e291cb2c',
    '467598239',
    'A-S04728435',
    'A-S04781507',
    'A-S04746731',
    '4d95b28786ed06c0eac83b4569ac57d8',
    'A-S04815900',
    'A-S04820437',
    'A-S04910360',
    'A-S04921030',
    'A-S04935417',
    '472733674',
    'A-S04958793',
    'A-S04959389',
    '470874661',
    'A-S04984288',
    '464561909',
    'A-S04870813',
    '4df14441801dc708269ec042068f36c5',
    'A-S04967620',
    'A-S04925579',
    'A-S04946382',
    '4e10ce84d56764f9cff01641a4ad6f0b',
    '464044399',
    'A-S04825472',
    'A-S04874496',
    '4e1a0a1837dded6055c6d54fb88cf977',
    '466066309',
    '467589902',
    'A-S04884739',
    'A-S04952668',
    'A-S04731746',
    'A-S04733244',
    'A-S04741560',
    '469001259',
    'A-S04933656',
    'A-S04946553',
    'A-S04948428',
    'A-S04969994',
    'A-S04759480',
    'A-S04805576',
    'A-S04993498',
    '470191250',
    'A-S04730823',
    'A-S04838019',
    'A-S04860923',
    'A-S04942562',
    'A-S04953214',
    '471393204',
    'A-S04729052',
    '4dca3324413d5fcb6f4de949539610f5',
    'A-S04809063',
    'A-S04807084',
    'A-S04732319',
    'A-S04733012',
    'A-S04926518',
    '4d7b715f32a08c599c125a449dbf0f5b',
    '463888594',
    '464309877',
    '465104565',
    '469619861',
    '4e227c2c527fea8bec3d16450987dc9d',
    '465299980',
    '465620056',
    '466006620',
    '467390517',
    '4e1abff3c81e579e6a5d7c498fb366dc',
    '4dd74b1cb4ecb3da851fc84e8eb3a094',
    '470605730',
    '4d84f6216009cc8ed80aed46bf9c5758',
    '472001967',
    '468411959',
    '470269217',
    '464455884',
    '464558595',
    '465235777',
    '470273729',
    '472786541',
    '465314737',
    '472764026',
    '4e0c755aec3af9dfb539054413b467bd',
    '465171987',
    '4dfd43857e3c565a5cf3db46f08142d3',
    '4e0a77823a94745db63ecd4f028ef6ec',
    '4e15515c6b81e4995767ef44778ed176',
    'A-S04743593',
    'A-S04796236',
    'A-S04932505',
    '470214465',
    '4e2218f364f3895bb5cb324f8895f5fd',
    'A-S04810850',
    'A-S04906286',
    'A-S04760906',
    'A-S04866863',
    'A-S04832369',
    '4e0e596e31e000b5dddd524670bc31e3',
    'A-S04836971',
    'A-S04774186',
    'A-S04874304',
    'A-S04780891',
    '468529994',
    'A-S04852324',
    'A-S05001205',
    '462205104',
    '463478612',
    '4dca8648168ab68eedfa6e414ca66cc8',
    '469978539',
    '472255156',
    '471511680',
    '471897196',
    '471262390',
    '4da92866687302c26432c94e1faf6b3d',
    '465361137',
    '466603627',
    '467676361',
    '469101304',
    '471262069',
    '463015122',
    '4dcadb22e874df2870e0b0476fb8a77e',
    '468091622',
    '468397207',
    '465986061',
    '471404628',
    '4e2f727f4fdad1b0b1ffa24181937dc6',
    '4db4326beee48670d5ded24125a49d90',
    '468607780',
    '471112468',
    '466545437',
    '467273524',
    '4db4580c48e8878ec855dc410e963e84',
    '4da48bf4b821a432d672c84fb9a32f09',
    '4d7f5243cc9271050f2c3144b3852f00',
    '4d90c82cde0c954aa8569a43a78f3cf3',
    '465064334',
    '464240660',
    '471370848',
    '467660647',
    '4e349da05d0eb627e257b7445da36e05',
    '466044636',
    '4dca01f299f9934abd9e9d4e0db46cc4',
    'A-S04766083',
    'A-S04820549',
    'A-S04832355',
    'A-S04875555',
    'A-S04889090',
    'A-S04896017',
    '469879777',
    '471090616',
    '4e21f46bef2fa821f5ab5948c494889d',
    'A-S04981398',
    '462168352',
    'A-S04766158',
    'A-S04809694',
    'A-S04851631',
    'A-S04746227',
    'A-S04776236',
    '467380835',
    'A-S04930625',
    'A-S04730703',
    'A-S04753308',
    'A-S04880853',
    'A-S04930933',
    '4e2ae4111b90646e7dfaa4480f934dd9',
    'A-S04754088',
    '464320001',
    'A-S04973523',
    'A-S04738496',
    '472769900',
    '467422216',
    'A-S04980837',
    'A-S04742938',
    'A-S04745303',
    'A-S04934585',
    'A-S04777585',
    '463102582',
    'A-S04754198',
    'A-S04756317',
    '463333647',
    '4de8d38d1af6312d5d1a954512bc40ff',
    '4e378ffd782ecedac592254137866692',
    '472395721',
    '472604869',
    '4dbd82d68fd2c3a23a9d284a96a3746d',
    '467405309',
    '467933908',
    '4e39ecddfd25ee8eedef834278aa16c1',
    '463717852',
    '465799746',
    '472083895',
    '4e2ffaa14c9a3612591bdf4f8b89003e',
    '469167424',
    '4de84f210325f6f5ec206b4c29936127',
    '4df143c25239cd0d2df48443428ffcfd',
    '4e23bc5b5647c5eb1b42554d73bfb70e',
    'A-S04730795',
    'A-S04752386',
    'A-S04861345',
    'A-S04871809',
    '4e359cc5f30d6450ae1a9a4da1bef49c',
    'A-S04909656',
    '471913243',
    '462954538',
    '4d9f63e2d0ec69eac754f843b496eeb7',
    '4db1ea0b4a7902dfb9f4a34f7ca7c71a',
    '4de24ce95ceca7b043246d4075874fd3',
    '467101253',
    '471056569',
    'A-S04733668',
    'A-S04741618',
    'A-S04756459',
    'A-S04874465',
    'A-S04880705',
    'A-S04911030',
    'A-S04916274',
    '469319924',
    'A-S04943766',
    'A-S04943835',
    'A-S04982258',
    'A-S04746845',
    'A-S04756043',
    'A-S04761141',
    'A-S04761256',
    '464362836',
    'A-S04810258',
    '465350809',
    'A-S04854244',
    'A-S04783878',
    'A-S04808059',
    'A-S04881690',
    'A-S04886470',
    'A-S04906698',
    'A-S05014681',
    '470529557',
    'A-S04862353',
    'A-S04895441',
    'A-S04907661',
    'A-S04915050',
    '472478297',
    '469075212',
    '4d9b9733c76126ffa07c0f4383aa41ef',
    '4ddd98d27f792aa5ad579d489ba7bf58',
    '468559420',
    '471984493',
    '471976883',
    '462647404',
    '464707999',
    '465745539',
    '4dd00db6b4e250c54143fd43aca81adb',
    '4dd1d73dede76a601ddf87414baf0eaa',
    '4dde1a81c61e0affe8ec3e406bb1c064',
    '4e00bf6243702c9d8cc06641f58d9b0c',
    '469294576',
    'A-S04739367',
    'A-S04905558',
    '463808323',
    '469052077',
    '4decd4399d9c49345985a748418a5e02',
    '4dfc1b0ca78a1c45e7ec464f89bfe8b5',
    'A-S04843725',
    '467102448',
    'A-S04895174',
    '4e3def9b9f25ab85e7ddbc4173bb1060',
    '471114190',
    '472702646',
    'A-S04829465',
    'A-S04970391',
    'A-S04892403',
    '470466896',
    'A-S04941547',
    '470791303',
    '464339188',
    '468759058',
    '462385021',
    '465079446',
    'A-S04947601',
    '4db16de58b02eb8050d3b04c798a1c77',
    '467069847',
    'A-S04737139',
    '4dbf2d63f36aeee7435f1e4c818773b9',
    '466806889',
    'A-S04877147',
    'A-S04988444',
    '472776885',
    '470635182',
    '465077392',
    'A-S04944959',
    'A-S04779935',
    'A-S04796306',
    'A-S04995798',
    'A-S04937897',
    '471319729',
    '466754383',
    '4e28ddb760032d427b9eb741c49d17dd',
    '464433303',
    '469338915',
    '4e17fde5b871ef7e53fff94bbdabb08c',
    '471263881',
    '4d93d00ae3e538caeb800044308df210',
    '4da947ccd9c0f8d162c36b49e788ab7a',
    'A-S04816777',
    '464712967',
    '471364021',
    'A-S04782271',
    'A-S04986646',
    'A-S04940423',
    '471279408',
    '4dcdce5ffa4d03f050f449417c826501',
    '467213438',
    'A-S04884494',
    '4e305da68fe4cedee7997c4c728df641',
    '4da403fe4c77ef9b41e36f4821ba7fb5',
    '465733541',
    'A-S04900714',
    'A-S04905066',
    'A-S04905400',
    '472322405',
    '467685363',
    '470961168',
    '464740195',
    '468753841',
    '471366287',
    '462377528',
    '4de5206848dcb12cdd89604b1493d266',
    '4de311c5f6fd0187cb299146cb8307c6',
    '468082888',
    '469492931',
    'A-S04952090',
    '463131642',
    '4e0689d1a090425544bc3948ca9634c7',
    'A-S04857703',
    'A-S04886138',
    'A-S04784081',
    '4db432ce4e70bf90260f994ca4a49d5f',
    '471991797',
    '4e1f1d3d5b94d0e514859f4a8eaea9b8',
    '463068397',
    '4d9f8b901324cde11ea80c4071ab438a',
    '4e2e356a324d84e93f4f5d47f4a63a7a',
    '4e389ab500f77c4654c3844f98a1153a',
    '465290370',
    '4dcd0213d8b9b86c58011241ea90c913',
    '470601074',
    '462889478',
    '465085834',
    '462327262',
    '472237633',
    'A-S04733524',
    'A-S04895615',
    'A-S04897245',
    '471536096',
    '4e2ef6b6be826f251684564dc885a301',
    'A-S04812167',
    '472369318',
    'A-S05010337',
    '462822339',
    '4e21a61e881b5f1fc39b8e4928a50abc',
    '465925341',
    '471252780',
    'A-S04804446',
    '466900276',
    '471666848',
    '470015829',
    'A-S04831792',
    '4e0b6a2470340e1038586f418d819bf4',
    'A-S04861934',
    'A-S04921011',
    '472579598',
    '471941180',
    '4e06a22a22b315ce6241784dde8b74c6',
    '464242848',
    'A-S04754802',
    '462161543',
    '4e1442a35ca1a57c3dce7a48779e9a42',
    '471974238',
    '4df439a491fcbc6e5735254cf087d733',
    'A-S04756778',
    'A-S04910158',
    'A-S04769461',
    'A-S04832664',
    'A-S04907727',
    '469955004',
    'A-S04778236',
    'A-S04805096',
    'A-S04808311',
    'A-S04810642',
    'A-S04823264',
    'A-S04887084',
    '4debf2a9685f302d7b6c134167a7a2cd',
    'A-S04916733',
    'A-S04941932',
    'A-S04981775',
    'A-S04915091',
    'A-S04846549',
    'A-S04732997',
    '466440613',
    'A-S04825015',
    '471324531',
    'A-S04745836',
    'A-S04752216',
    'A-S04810780',
    'A-S04970546',
    '4e020031c6ef75f3a25457489cb7ee16',
    'A-S04988706',
    'A-S04892204',
    'A-S04969141',
    'A-S04981149',
    'A-S04970345',
    'A-S05004445',
    'A-S04968374',
    '470543768',
    'A-S04726034',
    'A-S04978894',
    '467177577',
    'A-S04967150',
    'A-S05012267',
    '4e20b3396ca936625ff0eb494fae3da5',
    '465122064',
    '4db5e850bd382d0e880db14df88d6b3e',
    '469304628',
    'AD00FA60-1AC4-4255-9C7E-9437384D9C06',
    'be628fb9-630e-4b5f-bdfc-0c00ef9b7206',
    '4d7c6b1d0a522dfe869cec466dbba4f2',
    '464534100',
    '463841079',
    '464836010',
    '467694615',
    '4e0725b54a6c54363d583b413694e087',
    '471583121',
    '470867467',
    '465148018',
    '471094175',
    '472663148',
    '469668238',
    '463408593',
    '4e029f5fa18251a94008ef46d086545c',
    '470714145',
    '462936729',
    'A-S04759950',
    'A-S04799085',
    'A-S04800132',
    'A-S04805913',
    'A-S04806774',
    '469953708',
    '4d7ba07130001b09cdfd734086a8f71b',
    '4dbe2afcc2f9bab2a10a354cb094545d',
    '471870639',
    '4d7ee48d95ea6f295936fc4c6ab215d5',
    '4dd288265001196048f8324415be6888',
    'A-S04947217',
    '470798506',
    '467118645',
    'A-S05010506',
    '470534906',
    '4e1a29eb98b2c33704c0604e8684bd24',
    '4ddae6d31d08b6e0796d824b9ba99aa6',
    'b9c31f1d-012e-4230-a347-9b80f8b7af16',
    '4de459df8e84cbffe260f045dd86dad1',
    '471961965',
    'A-S04874209',
    'A-S04969444',
    'A-S04987699',
    '466668686',
    '472432620',
    'A-S04891506',
    '4dcdc188ed61f194aade01409d9f8003',
    '465384048',
    '471231793',
    'A-S05008840',
    '4dd2da268e1c3a653ac6bb46f5873efa',
    '469580038',
    '4debd12abc3ca39cccb42a4e7ea17b2d',
    'A-S04992622',
    '470208164',
    '4da887c082a65ca6e57ea94394991896',
    '4ddb6583de6de89979eed0440789a1e0',
    '468525489',
    '4de741f701b36f1e7a303848be8f3204',
    'A-S04872583',
    '4d8f9503ac2bba5972571f48d0924ed2',
    '472138042',
    'A-S04727754',
    '467427683',
    '468504582',
    'A-S04739777',
    'A-S04848102',
    'A-S04845396',
    '467316051',
    '470539088',
    'A-S04929153',
    '4e2c781676cf529cdf123346489b3bd9',
    'A-S04846491',
    'A-S04798255',
    'A-S04809149',
    'A-S04863241',
    '472193355',
    '472447889',
    'A-S04923147',
    '4e09c93037e7e8930aee3945b78701c8',
    'A-S04972511',
    'A-S04974068',
    'A-S04974874',
    '4db92bd6f2098f61be7d7a459ca642d8',
    'A-S04794998',
    'A-S04906117',
    '4dfd0f4adea393957ed9a741a0a9f612',
    '4e07463dfa6b0adda6b61d43ceb2afa3',
    'A-S04913953',
    'A-S04777389',
    'A-S04767945',
    'A-S04933874',
    '468221000',
    'A-S04968818',
    'A-S04959409',
    'A-S04831265',
    'A-S04985390',
    'A-S04768253',
    'A-S04848682',
    '472756387',
    'd1c12e8d-4d98-40f8-b059-cc995b761131',
    '4dbfbe19aa02914095da70418ea1b4e7',
    '4dbb3f9acb59aa4ddec15b470798a941',
    '467743121',
    '469506876',
    '471649804',
    '4e3eaba4dda8b584160ef24af195d9ef',
    '4d863e940fdc0e36318f7e457b92827a',
    '464513302',
    '467122398',
    '4de86b6633324810b0489f4a84a9b674',
    '4dfb36006395e7c3545bf64156967916',
    '4e01deb717c192c28dc1cd4e529fce90',
    '471163711',
    '4e36f0545f0fe89505d1014240b7637b',
    '472290300',
    '465093863',
    '4da4c2c35f66298845de2c4bcfa6b587',
    '4dd9f837c56827adcfaa9f498393fc47',
    '467000946',
    '468328172',
    '4e3764d1781da4c190422145f4953c49',
    '4da5b5cc9ac8c1d24946c94d6ea53087',
    '463693354',
    '4dfc1b4240b0e7c42b192344498b3f92',
    '469526388',
    '469937853',
    '4e2bc509bba07078389b2548dca8b9c9',
    '462195844',
    '462244813',
    '464737540',
    '464851966',
    '4ddd8735fe01d0961eb3ca40adafb5d1',
    '4ddd78a4bb921a099f370b4a54a01804',
    '469685158',
    '471891935',
    '465377627',
    '462377514',
    '464576493',
    '465304552',
    '463603666',
    '464475036',
    '470483558',
    '465949632',
    '469507720',
    '4e18c15a81360a1994f88d4e4f88a60b',
    '4e2bb13623f5e7a1ee17b8497493418a',
    '4e1bd7dbe10eb54fb8400143a6a22546',
    '464427147',
    '467891962',
    '468417439',
    '471033823',
    '4e11b90a071d0325b63b274ca2a2d18a',
    '470266884',
    '4de70b19605652bf3259f345cc883dc1',
    '470526680',
    '462966954',
    '466811499',
    '470943683',
    '469290801',
    '4e0f52e7a55b7ab4767db44377a632d4',
    '467392294',
    '462197463',
    '4dbe94c1685c7b1a8e503f4ba6a37cd9',
    '4e18a2a6e82c37945cbbd64599b13f4e',
    '4dc42824022f33a929b4e74dd8acfa8e',
    '4e0c5d37492341f99cc94d46ac97051d',
    '465673626',
    '467017321',
    '468766166',
    '463371186',
    '4e1b2017508c0eb59e56c84495b1b66c',
    '4e1f8b3824ceaac5574e144405a7869f',
    '469058481',
    '4e01bb6627673f3e403d824583be5ca2',
    '4e2f8ce455a06bb66a234a4db4a31f69',
    '471736056',
    '464484906',
    '471400082',
    '471657379',
    '469145256',
    '6D3634BF-6A8D-41D4-9804-2E1BBFC19361',
    '4e1b388ba7853cdf9b86f8483e8472db',
    '4e33f7e8a9dddfab9189e84f088f53ec',
    '4e2ff974699770505b06ff4b11b143cb',
    '472068615',
    '463526745',
    '4df5937c28628a1fd545b64aeaab5e8e',
    '470632918',
    '471494475',
    '4e25a54d0f071fabf4c8ef4059ad3060',
    '9dc08d08-aac3-4a16-8bff-20ea8a027045',
    '4dbffa6e678c80b328851c434aad2ca5',
    '463043740',
    '464751739',
    '4dbbe68f032210685c371444f288cc10',
    '462861473',
    '471966055',
    'A-S04741111',
    'A-S04782697',
    'A-S04748671',
    'A-S04872854',
    'A-S04767177',
    'A-S04773341',
    'A-S04800800',
    'A-S04801941',
    'A-S04876853',
    '467945101',
    '4e1121b81f32d64f28924f4c06b6b04e',
    'A-S04954117',
    'A-S04971481',
    'A-S04983387',
    'A-S04848153',
    '4e34be107401e63dfc442a4da1991cae',
    'A-S04921403',
    'A-S04733620',
    'A-S04962184',
    'A-S04987499',
    '462928690',
    'A-S04752965',
    'A-S04873694',
    '4dbc72d03bd60d454bf4db444f94fe32',
    '4e2f830e1ff1502f5404314913afc3c7',
    '4dd7880367db43b4aa9edc4297aa4e33',
    'A-S04744542',
    'A-S04885861',
    'A-S04902856',
    'A-S04932975'
);
COMMIT;