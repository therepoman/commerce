with spends as (
    SELECT
        transaction_id,
        SUM(quantity) as quantity
    FROM dbsnapshots.bits_spends
    GROUP BY transaction_id
)

   , joined as (
    SELECT bits_transactions.transaction_id, bits_transactions.quantity as tranasctions_quantity, spends.quantity as spends_quantity
    FROM dbsnapshots.bits_transactions
             FULL OUTER JOIN spends
                             ON bits_transactions.transaction_id = spends.transaction_id
)

SELECT * FROM joined where tranasctions_quantity != spends_quantity