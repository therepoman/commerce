package main

import (
	"fmt"
	"regexp"
	"strings"

	"code.justin.tv/commerce/moments-backfills/utils/env"
	"code.justin.tv/commerce/moments-backfills/utils/io"
)

const (
	awsProfile                  = "pachter-prod"
	awsRegion                   = "us-west-2"
	pachterAuditRecordTableName = "worker-audit-prod"
	filePath                    = "executions/pachter/8-20-2020/cloudwatch.log"
	dryRun                      = true // skip updating the pachter audit records
)

var transactionIDsRegex = regexp.MustCompile(`transactionIds="\[[\w-,]*\]"`)

func main() {
	env.SetAWSProfile(awsProfile)
	env.SetAWSRegion(awsRegion)
	//dc := dynamo.NewClient()

	lines := io.ReadFileLines(filePath)
	for _, line := range lines {
		//fmt.Println(line)
		fmt.Println(extractTransactionIDs(line))
	}

}

func extractTransactionIDs(logMsg string) []string {
	matches := transactionIDsRegex.FindAllString(logMsg, -1)
	cleanedMatches := make([]string, 0)
	for _, match := range matches {
		match = strings.TrimSpace(match)
		match = strings.TrimPrefix(match, "transactionIds=\"[")
		match = strings.TrimSuffix(match, "]\"")
		cleanedMatches = append(cleanedMatches, match)
	}

	dedupeMap := make(map[string]interface{})
	for _, match := range cleanedMatches {
		dedupeMap[match] = nil
	}

	deduped := make([]string, 0)
	for match := range dedupeMap {
		deduped = append(deduped, match)
	}
	return deduped
}

func extractTimestamp(logMsg string) []string {
	matches := transactionIDsRegex.FindAllString(logMsg, -1)
	cleanedMatches := make([]string, 0)
	for _, match := range matches {
		match = strings.TrimSpace(match)
		match = strings.TrimPrefix(match, "transactionIds=\"[")
		match = strings.TrimSuffix(match, "]\"")
		cleanedMatches = append(cleanedMatches, match)
	}

	dedupeMap := make(map[string]interface{})
	for _, match := range cleanedMatches {
		dedupeMap[match] = nil
	}

	deduped := make([]string, 0)
	for match := range dedupeMap {
		deduped = append(deduped, match)
	}
	return deduped
}
