select datetime, bits_transactions.transaction_id, bits_transactions.transaction_type, bits_transactions.requesting_twitch_user_id, quantity from bits_transactions
where bits_transactions.transaction_type='UseBitsInExtensionChallenge'
and (datetime > '2020-06-01' and datetime < '2020-08-09')
and quantity = 0
order by datetime asc;