## Overview
- Several challenges related transactions were ingested incorrectly into pachter
- These transactions have 0 quantity
- The payday get transaction logic has been fixed, re-ingesting the transactions should 

## Pre-Backfill State
- Broken (zero-quantity) challenge transactions in pachter db

## This Backfill
- Run a pachter script that resets the audit status of these transactions
- Run a pachter script that re-ingests these transactions

## Status
- TODO