## Overview
- Several transactions have been ingested with a type of "8" (Finalize)
- These transaction-ids have the receipient id appended at the end of their transaction-id which breaks things

## Pre-Backfill State
- Broken transactions in pachter db

## This Backfill
- Deletes the broken transactions from pachter

## Status
- I ran this backfill at ~12:00pm on 8/11/2020 - @seph
- Running the select SQL query again shows no affected transactions