## Overview
- Approx 20K transactions were missing in pachter around 7/28/2020 due to a bug that was deployed
- I compared the pachter DB against prometheus for the affected time to identify these transactions

## Pre-Backfill State
- Approx 20K missing transactions in pachter

## This Backfill
- Backfills the transactions worker in pachter
- Backfills the cheermote worker in pachter

## Status
- I ran this backfill around ~11:50am on 8/12/2020 - @seph