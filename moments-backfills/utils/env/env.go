package env

import "os"

func SetAWSProfile(profileName string) {
	err := os.Setenv("AWS_PROFILE", profileName)
	if err != nil {
		panic(err)
	}
}

func SetAWSRegion(region string) {
	err := os.Setenv("AWS_DEFAULT_REGION", region)
	if err != nil {
		panic(err)
	}
}

func GetGoPath() string {
	return os.Getenv("GOPATH")
}
