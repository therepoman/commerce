package io

import (
	"bufio"
	"encoding/csv"
	"io"
	"os"

	"code.justin.tv/commerce/moments-backfills/utils/env"
)

func ReadCSV(filePath string) [][]string {
	fullPath := env.GetGoPath() + "/src/code.justin.tv/commerce/moments-backfills/" + filePath
	csvFile, err := os.Open(fullPath)
	if err != nil {
		panic(err)
	}

	reader := csv.NewReader(bufio.NewReader(csvFile))
	rows, err := reader.ReadAll()
	if err != nil {
		panic(err)
	}

	return rows
}

func ReadFileLines(filePath string) []string {
	fullPath := env.GetGoPath() + "/src/code.justin.tv/commerce/moments-backfills/" + filePath
	file, err := os.Open(fullPath)
	if err != nil {
		panic(err)
	}

	lines := make([]string, 0)
	reader := bufio.NewReader(file)
	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				return lines
			} else {
				panic(err)
			}
		}
		lines = append(lines, line)
	}
}

func CreateCSV(fileName string) (*os.File, *csv.Writer) {
	csvFile, err := os.Create(env.GetGoPath() + "/src/code.justin.tv/commerce/moments-backfills/" + fileName)
	if err != nil {
		panic(err)
	}
	csvWriter := csv.NewWriter(csvFile)
	return csvFile, csvWriter
}
