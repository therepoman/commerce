package cheermotes

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"unicode"
)

const (
	cheermoteRegex = `^%s\d+\s*$`
)

// note: this is an incomplete list
var DefaultPrefixes = []string{
	"cheer",
	"showlove",
	"pride",
	"frankerz",
	"seemsgood",
	"party",
	"kappa",
	"bday",
	"pjsalt",
	"ripcheer",
	"shamrock",
	"pogchamp",
	"heyguys",
	"notlikethis",
	"vohiyo",
	"bleedpurple",
}

func BuildRegexMap(prefixes []string) map[string]*regexp.Regexp {
	regexMap := make(map[string]*regexp.Regexp)
	for _, prefix := range prefixes {
		regexStr := fmt.Sprintf(cheermoteRegex, prefix)
		regex, err := regexp.Compile(regexStr)
		if err != nil {
			panic(err)
		}
		regexMap[prefix] = regex
	}
	return regexMap
}

func GetCheermotes(regexMap map[string]*regexp.Regexp, msg string) map[string]int64 {
	cheermotes := make(map[string]int64)
	tokens := strings.Split(msg, " ")
	for _, token := range tokens {
		token = strings.ToLower(strings.TrimSpace(token))
		for _, regex := range regexMap {
			matches := regex.Match([]byte(token))
			if matches {
				val := GetBitsAmount(token)
				prefix := GetPrefix(token)
				cheermotes[prefix] += val
			}
		}
	}
	return cheermotes
}

func GetTotalFromCheermotes(cheermotes map[string]int64) int64 {
	total := int64(0)
	for _, val := range cheermotes {
		total += val
	}
	return total
}

func GetBitsAmount(token string) int64 {
	token = strings.TrimSpace(token)
	numStr := strings.TrimFunc(token, func(r rune) bool {
		return !unicode.IsDigit(r)
	})
	val, err := strconv.ParseInt(numStr, 10, 64)
	if err != nil {
		panic(err)
	}
	return val
}

func GetPrefix(token string) string {
	token = strings.TrimSpace(token)
	prefixStr := strings.TrimFunc(token, func(r rune) bool {
		return unicode.IsDigit(r)
	})
	return prefixStr
}
