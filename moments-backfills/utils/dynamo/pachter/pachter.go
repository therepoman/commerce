package pachter

import (
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type AuditRecord struct {
	TransactionID string    `dynamodbav:"transactionId"`
	Cheermote     time.Time `dynamodbav:"cheermote"`
	Transactions  time.Time `dynamodbav:"transactions"`
	Spend         time.Time `dynamodbav:"spend"`
}

func GetAuditRecord(dynamoClient *dynamodb.DynamoDB, tableName string, txID string) *AuditRecord {
	out, err := dynamoClient.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"transactionId": {
				S: aws.String(txID),
			},
		},
	})
	if err != nil {
		panic(err)
	}

	if out.Item == nil {
		return nil
	}

	var auditRecord AuditRecord
	err = dynamodbattribute.UnmarshalMap(out.Item, &auditRecord)
	if err != nil {
		panic(err)
	}

	return &auditRecord
}

func UpdateAuditRecord(dynamoClient *dynamodb.DynamoDB, tableName, txID string, attributeUpdates map[string]*dynamodb.AttributeValueUpdate) {
	_, err := dynamoClient.UpdateItem(&dynamodb.UpdateItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"transactionId": {
				S: aws.String(txID),
			},
		},
		AttributeUpdates: attributeUpdates,
	})
	if err != nil {
		panic(err)
	}
}
