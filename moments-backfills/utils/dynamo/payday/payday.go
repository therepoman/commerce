package payday

import (
	"context"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type BitsUsageAttribution struct {
	ID                                  string `dynamodbav:"id"`
	BitsRevenueAttributionToBroadcaster int64  `dynamodbav:"number_of_bits"`
	RewardAttribution                   int64  `dynamodbav:"reward_attribution"`
	TotalWithBroadcasterPrior           int64  `dynamodbav:"total_with_broadcaster"`
}

type Transaction struct {
	TransactionId            string                  `dynamodbav:"transaction_id"`
	ChannelId                *string                 `dynamodbav:"channel_id"`
	UserId                   *string                 `dynamodbav:"user_id"`
	TransactionType          *string                 `dynamodbav:"transaction_type"`
	FinalizedTransactionType *string                 `dynamodbav:"finalized_transaction_type"`
	NumberOfBits             *int64                  `dynamodbav:"number_of_bits"`
	BitsType                 *string                 `dynamodbav:"bits_type"`
	ProductId                *string                 `dynamodbav:"product_id"`
	Platform                 *string                 `dynamodbav:"platform"`
	LastUpdated              int64                   `dynamodbav:"last_updated"`
	TimeOfEvent              int64                   `dynamodbav:"time_of_event"`
	Recipients               []*BitsUsageAttribution `dynamodbav:"recipients"`
	Annotation               string                  `dynamodbav:"annotation"`
	EmoteTotals              map[string]int64        `dynamodbav:"emote_totals"`
	RawMessage               string                  `dynamodbav:"raw_message"`
}

func GetTransaction(dynamoClient *dynamodb.DynamoDB, tableName string, txID string) *Transaction {
	out, err := dynamoClient.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"transaction_id": {
				S: aws.String(txID),
			},
		},
	})
	if err != nil {
		panic(err)
	}

	if out.Item == nil {
		return nil
	}

	var transaction Transaction
	err = dynamodbattribute.UnmarshalMap(out.Item, &transaction)
	if err != nil {
		panic(err)
	}

	return &transaction
}

func UpdateTransaction(dynamoClient *dynamodb.DynamoDB, tableName, txID string, attributeUpdates map[string]*dynamodb.AttributeValueUpdate) {
	_, err := dynamoClient.UpdateItem(&dynamodb.UpdateItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"transaction_id": {
				S: aws.String(txID),
			},
		},
		AttributeUpdates: attributeUpdates,
	})
	if err != nil {
		panic(err)
	}
}

type ExtensionTransaction struct {
	UserID    string `dynamodbav:"userID"`
	ChannelID string `dynamodbav:"channelID"`
	Status    string `dynamodbav:"status"`
	TransactionID string `dynamodbav:"transactionID"`
	ChannelIDStatus string `dynamodbav:"channelIDStatus"`
	UserIDStatus string `dynamodbav:"userIDStatus"`
}

func UpdateExtensionTransaction(dynamoClient *dynamodb.DynamoDB, tableName, userID, transactionID string, attributeUpdates map[string]*dynamodb.AttributeValueUpdate) error {
	_, err := dynamoClient.UpdateItem(&dynamodb.UpdateItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"userID": {
				S: aws.String(userID),
			},
			"transactionID": {
				S: aws.String(transactionID),
			},
		},
		AttributeUpdates: attributeUpdates,
	})
	return err
}

func ScanExtensionTransactions(dynamoClient *dynamodb.DynamoDB, ctx context.Context, tableName string, cursor map[string]*dynamodb.AttributeValue) ([]ExtensionTransaction, map[string]*dynamodb.AttributeValue) {
	out, err := dynamoClient.ScanWithContext(ctx, &dynamodb.ScanInput{
		TableName: aws.String(tableName),
		Limit: aws.Int64(1000),
		ExclusiveStartKey: cursor,
	})
	if err != nil {
		panic(err)
	}

	var transactions []ExtensionTransaction
	err = dynamodbattribute.UnmarshalListOfMaps(out.Items, &transactions)
	if err != nil {
		panic(err)
	}

	return transactions, out.LastEvaluatedKey
}