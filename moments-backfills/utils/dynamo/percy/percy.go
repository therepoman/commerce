package percy

import (
	"context"
	"errors"
	"time"

	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type CelebrationSize string

const (
	CelebrationSizeUnknown = CelebrationSize("UNKNOWN")
	CelebrationSizeSmall   = CelebrationSize("SMALL")
	CelebrationSizeMedium  = CelebrationSize("MEDIUM")
	CelebrationSizeLarge   = CelebrationSize("LARGE")
)

type CelebrationChannelPurchasableConfigRecord struct {
	ChannelID      string                                         `dynamodbav:"channel_id"`
	PurchaseConfig map[CelebrationSize]*CelebrationPurchaseConfig `dynamodbav:"purchase_config"`
	LastUpdated    *time.Time                                     `dynamodbav:"last_updated,omitempty"`
}

type CelebrationPurchaseConfig struct {
	IsDisabled *bool   `dynamodbav:"is_disabled"`
	OfferID    *string `dynamodbav:"offer_id"`
}

func UpdateCelebrationPurchaseConfig(dynamoClient *dynamodb.DynamoDB, tableName string, channelConfig CelebrationChannelPurchasableConfigRecord) {
	marshaledCelebrationChannelPurchaseConfig, err := dynamodbattribute.MarshalMap(&channelConfig)
	if err != nil {
		panic(errors.New("failed to marshal celebration channel purchase config dynamoDB entry"))
	}

	_, err = dynamoClient.PutItemWithContext(context.Background(), &dynamodb.PutItemInput{
		TableName: aws.String(tableName),
		Item:      marshaledCelebrationChannelPurchaseConfig,
	})
	if err != nil {
		panic(err)
	}
}
