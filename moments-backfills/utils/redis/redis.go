package redis

import (
	"code.justin.tv/chat/rediczar"
	"github.com/go-redis/redis/v7"
)

func NewClient(address string) *rediczar.Client {
	client := redis.NewClient(&redis.Options{
		Addr: address,
	})

	return &rediczar.Client{
		Commands: &rediczar.PrefixedCommands{Redis: client},
	}
}
