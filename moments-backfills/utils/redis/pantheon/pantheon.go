package pantheon

import (
	"context"
	"strconv"
	"strings"
	"time"

	"code.justin.tv/chat/rediczar"
	"github.com/go-redis/redis/v7"
)

const (
	GroupingKeyIndexPrefix = "grouping_key_index"
)

type Domain string
type TimeUnit int32
type GroupingKey string
type TimeBucket time.Time

var TimeUnit_value = map[string]int32{
	"ALLTIME": 0,
	"YEAR":    1,
	"MONTH":   2,
	"WEEK":    3,
	"DAY":     4,
}

type Identifier struct {
	Domain              Domain
	GroupingKey         GroupingKey
	TimeAggregationUnit TimeUnit
	TimeBucket          TimeBucket
}

func Pipeline(ctx context.Context, redisClient *rediczar.Client) redis.Pipeliner {
	return redisClient.Pipeline(ctx)
}

func Close(pipeline redis.Pipeliner) error {
	return pipeline.Close()
}

func Scan(ctx context.Context, redisClient *rediczar.Client, cursor uint64, match string, count int64) ([]string, uint64, error) {
	return redisClient.Scan(ctx, cursor, match, count)
}

func SAdd(pipeline redis.Pipeliner, index string, leaderboardKey string) (int64, error) {
	return pipeline.SAdd(index, leaderboardKey).Result()
}

func IdFromKey(key string) Identifier {
	parts := strings.Split(key, "/")
	if len(parts) != 4 {
		panic("invalid identifier key: key must have exactly 4 sections delimited by /")
	}

	domain := parts[0]
	if domain == "" {
		panic("invalid identifier key: domain must not be blank")
	}

	groupingKey := parts[1]
	if groupingKey == "" {
		panic("invalid identifier key: grouping key must not be blank")
	}

	timeAggregationUnitStr := parts[2]
	timeAggregationUnit, valid := TimeUnitFromString(timeAggregationUnitStr)
	if !valid {
		panic("invalid identifier key: invalid time aggregation unit")
	}

	timeBucketStr := parts[3]
	timeBucketNano, err := strconv.ParseInt(timeBucketStr, 10, 64)
	if err != nil {
		panic("invalid identifier key: time bucket must be numeric")
	}

	return Identifier{
		Domain:              Domain(domain),
		GroupingKey:         GroupingKey(groupingKey),
		TimeAggregationUnit: timeAggregationUnit,
		TimeBucket:          TimeBucket(time.Unix(0, timeBucketNano)),
	}
}

func TimeUnitFromString(strTimeUnit string) (TimeUnit, bool) {
	tu, isPresent := TimeUnit_value[strTimeUnit]
	return TimeUnit(tu), isPresent
}
