package rediczar

import (
	"time"

	"github.com/go-redis/redis/v7"
)

// RunMetrics always runs exactly one of the following functions each time a command is called
type RunMetrics interface {
	// Success is called each time a command does not return an error, or if the error is redis.Nil
	Success(name string, dur time.Duration)
	// Error is called each time a command returns an error, and if the error is not redis.Nil
	Error(name string, dur time.Duration)
}

func trackCommand(c RunMetrics, name string, err error, start time.Time) {
	if c == nil {
		return
	}

	dur := time.Since(start)
	if err == nil || err == redis.Nil {
		c.Success(name, dur)
	} else {
		c.Error(name, dur)
	}
}

// MultiRunMetrics chains together multiple RunMetrics
type MultiRunMetrics struct {
	RunMetrics []RunMetrics
}

// Success is called each time a command does not return an error, or if the error is redis.Nil
func (m *MultiRunMetrics) Success(name string, dur time.Duration) {
	for _, r := range m.RunMetrics {
		r.Success(name, dur)
	}
}

// Error is called each time a command returns an error, and if the error is not redis.Nil
func (m *MultiRunMetrics) Error(name string, dur time.Duration) {
	for _, r := range m.RunMetrics {
		r.Error(name, dur)
	}
}

var _ RunMetrics = (*MultiRunMetrics)(nil)
