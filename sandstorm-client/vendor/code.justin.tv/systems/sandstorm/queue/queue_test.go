package queue

import (
	"testing"

	"code.justin.tv/systems/sandstorm/mocks"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
)

type QueueTestSuite struct {
	suite.Suite

	queue   *Queue
	mockSQS *mocks.SQSAPI
}

func (s *QueueTestSuite) SetupTest() {
	s.mockSQS = new(mocks.SQSAPI)
	s.queue = &Queue{
		Config: Config{
			QueueArn: s.QueueArn(),
			QueueURL: s.QueueURL(),
		},
		SQS: s.mockSQS,
	}
}

func (s *QueueTestSuite) TearDownTest() {
	s.mockSQS.AssertExpectations(s.T())
}

func (s *QueueTestSuite) QueueArn() string {
	return "my-queue-arn"
}

func (s *QueueTestSuite) QueueURL() string {
	return "my-queue-url"
}

func (s *QueueTestSuite) TestStatus() {
	s.mockSQS.On("GetQueueAttributes", mock.Anything).
		Return(&sqs.GetQueueAttributesOutput{
			Attributes: map[string]*string{
				"ApproximateNumberOfMessages": aws.String("14"),
			},
		}, nil)

	status, err := s.queue.Status()
	s.Require().NoError(err)
	s.Equal(&Status{
		ApproximateNumberOfMessages: 14,
		QueueArn:                    s.QueueArn(),
		QueueURL:                    s.QueueURL(),
	}, status)
}

func TestQueue(t *testing.T) {
	suite.Run(t, &QueueTestSuite{})
}
