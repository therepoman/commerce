// +build integration

package queue

import (
	"testing"

	"code.justin.tv/systems/sandstorm/pkg/integration"
	"code.justin.tv/systems/sandstorm/pkg/log"
	"code.justin.tv/systems/sandstorm/resource"
	"code.justin.tv/systems/sandstorm/util"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
)

func TestQueueIntegration(t *testing.T) {
	stack := integration.Stack(t)

	Convey("For a configured Queue", t, func() {
		Convey("queue management", func() {
			sess := resource.STSSession(nil, stack.AgentTestingRoleARN())
			q := New(sqs.New(sess), sns.New(sess), Config{
				QueueConfigPath: "/build/go/src/code.justin.tv/systems/sandstorm/queue",
				QueueNamePrefix: stack.SecretsQueueNamePrefix(),
				TopicArn:        stack.SecretsTopicArn(),
			}, &log.TestLogger{t})
			err := q.Setup()
			So(err, ShouldBeNil)

			t.Logf("created queue: %s", q.Config.QueueURL)
			t.Logf("created queue: %s", q.Config.QueueArn)

			Convey("->deleteQueue()", func() {
				err := q.Delete()
				So(err, ShouldBeNil)
				So(util.FileExists(q.Config.QueueConfigFullPath()), ShouldBeFalse)

				_, err = q.getQueueAttributes("QueueArn")
				So(err, ShouldNotBeNil)
			})
		})
	})
}
