package queue

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestBuildQueueIdentifier(t *testing.T) {
	const maxQueueIdentifierLength = 80

	type buildQueueIdentifierTest struct {
		testName                string
		namePrefix              string
		hostName                string
		expectedTruncatedPrefix string
	}

	tests := []buildQueueIdentifierTest{
		{
			"success",
			"sandstorm",
			"some-hostname.internal",
			"sandstorm-some-hostname_internal",
		},
		{
			"truncated hostname",
			"sandstorm",
			"sandstorm-staging-0d3644c39adf34637.dev.us-west2.justin.tv",
			"sandstorm-sandstorm-staging-0d3644c39adf34",
		},
	}

	for _, test := range tests {
		t.Run(test.testName, func(t *testing.T) {
			identifier, err := buildQueueIdentifier(test.namePrefix, test.hostName)
			require.NoError(t, err)
			t.Log("identifier: " + identifier)
			assert.Condition(t,
				func() (ok bool) {
					return strings.HasPrefix(identifier, test.expectedTruncatedPrefix)
				},
			)
			assert.True(t, len(identifier) <= maxQueueIdentifierLength)
			assert.Regexp(t, "^[a-zA-Z0-9-_]{0,80}$", identifier)
		})
	}
}
