package secret

import (
	"testing"
	"time"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestChangeset(t *testing.T) {
	genID := func(t *testing.T) string {
		id, err := uuid.NewV4()
		require.NoError(t, err)
		return id.String()
	}

	t.Run("Update", func(t *testing.T) {
		us := new(Changeset)
		secretName := "test"

		updatedAt := time.Now().Unix()

		us.Update(map[string]int64{secretName: updatedAt})
		assert.True(t, us.SecretHasBeenUpdated(secretName))
		assert.False(t, us.SecretHasBeenUpdated(genID(t)))
		uat := us.GetSecretUpdatedAt(secretName)
		assert.Equal(t, updatedAt, uat)

		newSecretName := "anotherSecret"
		updatedAt = time.Now().Unix()

		us.Update(map[string]int64{secretName: updatedAt})
		us.Update(map[string]int64{newSecretName: updatedAt})

		assert.True(t, us.SecretHasBeenUpdated(newSecretName))
		assert.True(t, us.SecretHasBeenUpdated(secretName))

		uat = us.GetSecretUpdatedAt(newSecretName)
		assert.Equal(t, updatedAt, uat)

		uat = us.GetSecretUpdatedAt(secretName)
		assert.Equal(t, updatedAt, uat)
	})
}
