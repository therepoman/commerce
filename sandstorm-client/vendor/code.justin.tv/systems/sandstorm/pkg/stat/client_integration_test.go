// +build integration

package stat

import (
	"testing"
	"time"

	"code.justin.tv/systems/sandstorm/closer"
	"code.justin.tv/systems/sandstorm/pkg/integration"
	"code.justin.tv/systems/sandstorm/pkg/log"
	"code.justin.tv/systems/sandstorm/resource"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestClientIntegration(t *testing.T) {
	testTime := time.Now().Truncate(time.Minute).UTC()
	t.Logf("logging metrics with testTime: %s", testTime)

	const cloudwatchNamespace = "/twitch/systems/sandstorm/testing"
	testID, err := uuid.NewV4()
	require.NoError(t, err)
	t.Logf("logging metrics with test-id: %s", testID.String())

	stack := integration.Stack(t)

	dimensions := []*cloudwatch.Dimension{
		{
			Name:  aws.String("test-id"),
			Value: aws.String(testID.String()),
		},
	}

	cloudwatchClient := cloudwatch.New(resource.STSSession(
		nil, stack.AdminRoleARN(), stack.CloudwatchRoleARN()))

	c := &Client{
		Namespace:  cloudwatchNamespace,
		CloudWatch: cloudwatchClient,
		Logger:     &log.TestLogger{T: t},
		Closer:     closer.New(),
		FlushRate:  time.Second,
	}
	defer func() {
		assert.NoError(t, c.Closer.Close())
	}()

	t.Run("Increment and confirm metric", func(t *testing.T) {
		t.Parallel()

		metricName := "incremented-metric"

		t0 := testTime.Add(-5 * time.Minute).UTC()
		t1 := testTime.Add(5 * time.Minute).UTC()

		c.Increment(Metric{
			Dimensions: dimensions,
			MetricName: metricName,
		})

		var (
			err     error
			results *cloudwatch.GetMetricStatisticsOutput
		)
		for nTry := 0; nTry < 60; nTry++ {
			results, err = cloudwatchClient.GetMetricStatistics(&cloudwatch.GetMetricStatisticsInput{
				Dimensions: dimensions,
				EndTime:    &t1,
				StartTime:  &t0,
				Statistics: []*string{aws.String("SampleCount")},
				Namespace:  aws.String(cloudwatchNamespace),
				MetricName: aws.String(metricName),
				Period:     aws.Int64(60),
			})

			require.NoError(t, err)
			if len(results.Datapoints) > 0 {
				break
			}

			time.Sleep(time.Second)
		}

		require.Len(t, results.Datapoints, 1)
		assert.Equal(t, 1, int(aws.Float64Value(results.Datapoints[0].SampleCount)))
	})

	t.Run("Measure and confirm metrics", func(t *testing.T) {
		t.Parallel()

		metricName := "measured-metric"

		t0 := testTime.Add(-5 * time.Minute).UTC()
		t1 := testTime.Add(5 * time.Minute).UTC()

		c.Measure(Metric{
			Dimensions: dimensions,
			MetricName: metricName,
		}, 10.0)
		c.Measure(Metric{
			Dimensions: dimensions,
			MetricName: metricName,
		}, 14.0)

		var (
			err     error
			results *cloudwatch.GetMetricStatisticsOutput
		)
		for nTry := 0; nTry < 60; nTry++ {
			results, err = cloudwatchClient.GetMetricStatistics(&cloudwatch.GetMetricStatisticsInput{
				Dimensions: dimensions,
				EndTime:    &t1,
				StartTime:  &t0,
				Statistics: []*string{
					aws.String("SampleCount"),
					aws.String("Minimum"),
					aws.String("Maximum"),
					aws.String("Average"),
					aws.String("Sum"),
				},
				Namespace:  aws.String(cloudwatchNamespace),
				MetricName: aws.String(metricName),
				Period:     aws.Int64(60),
			})

			require.NoError(t, err)
			if len(results.Datapoints) > 0 {
				break
			}

			time.Sleep(time.Second)
		}

		require.Len(t, results.Datapoints, 1)
		pt := results.Datapoints[0]
		assert.Equal(t, 2, int(aws.Float64Value(pt.SampleCount)))
		assert.Equal(t, 10.0, aws.Float64Value(pt.Minimum))
		assert.Equal(t, 14.0, aws.Float64Value(pt.Maximum))
		assert.Equal(t, 12.0, aws.Float64Value(pt.Average))
		assert.Equal(t, 24.0, aws.Float64Value(pt.Sum))
	})
}
