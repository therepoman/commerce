package stat

import (
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestMetric(t *testing.T) {
	t.Run("toKey and fromKey", func(t *testing.T) {
		m := Metric{
			Dimensions: []*cloudwatch.Dimension{
				{
					Name:  aws.String("my-dimension-name"),
					Value: aws.String("my-dimension-value"),
				},
			},
			MetricName:        "my-metric-name",
			StorageResolution: time.Minute,
			timeBucket:        timeBucket(time.Now()),
		}

		key, err := m.toKey()
		require.NoError(t, err)

		var unmarshalled Metric
		require.NoError(t, unmarshalled.fromKey(key))

		assert.Equal(t, m, unmarshalled)
	})
}
