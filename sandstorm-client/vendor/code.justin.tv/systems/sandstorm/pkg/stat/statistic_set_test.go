package stat

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStatisticSet(t *testing.T) {
	t.Run("Record", func(t *testing.T) {
		t.Run("first value", func(t *testing.T) {
			s := &statisticSet{}
			s.Record(14.0)
			assert.Equal(t, 14.0, s.sum)
			assert.Equal(t, int64(1), s.sampleCount)
			assert.Equal(t, 14.0, s.minimum)
			assert.Equal(t, 14.0, s.maximum)
		})

		t.Run("new minimum after first value", func(t *testing.T) {
			s := &statisticSet{}
			s.Record(14.0)
			s.Record(10.0)
			assert.Equal(t, 24.0, s.sum)
			assert.Equal(t, int64(2), s.sampleCount)
			assert.Equal(t, 10.0, s.minimum)
			assert.Equal(t, 14.0, s.maximum)
		})

		t.Run("new maximum after first value", func(t *testing.T) {
			s := &statisticSet{}
			s.Record(14.0)
			s.Record(18.0)
			assert.Equal(t, 32.0, s.sum)
			assert.Equal(t, int64(2), s.sampleCount)
			assert.Equal(t, 14.0, s.minimum)
			assert.Equal(t, 18.0, s.maximum)
		})
	})

	t.Run("Increment", func(t *testing.T) {
		s := &statisticSet{}
		s.Increment()
		assert.Equal(t, 1.0, s.sum)
		assert.Equal(t, int64(1), s.sampleCount)
		assert.Equal(t, 1.0, s.minimum)
		assert.Equal(t, 1.0, s.maximum)
	})
}
