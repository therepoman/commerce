package stat

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/systems/sandstorm/closer"
	"code.justin.tv/systems/sandstorm/mocks"
	"code.justin.tv/systems/sandstorm/pkg/log"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
)

type RunnerSuite struct {
	suite.Suite

	cloudwatch *mocks.CloudWatchAPI
	runner     *runner
}

func (s *RunnerSuite) SetupTest() {
	s.cloudwatch = new(mocks.CloudWatchAPI)
	s.runner = &runner{
		Closer:     closer.New(),
		CloudWatch: s.cloudwatch,
		Logger:     &log.TestLogger{T: s.T()},
		FlushRate:  time.Millisecond,
		Namespace:  s.Namespace(),
	}
}

func (s *RunnerSuite) TearDownTest() {
	s.cloudwatch.AssertExpectations(s.T())
}

func (s *RunnerSuite) Namespace() string {
	return "my-namespace"
}

func (s *RunnerSuite) Metric() Metric {
	return Metric{
		MetricName: "my-metric-name",
	}
}

func (s *RunnerSuite) TestIncrement() {
	ctx := context.Background()

	s.cloudwatch.
		On("PutMetricDataWithContext", ctx, mock.Anything).
		Run(func(args mock.Arguments) {
			input := args.Get(1).(*cloudwatch.PutMetricDataInput)
			s.Require().Len(input.MetricData, 1)
			s.Equal(s.Namespace(), aws.StringValue(input.Namespace))

			pt := input.MetricData[0]
			s.Equal(1.0, aws.Float64Value(pt.StatisticValues.Sum))
		}).
		Return(nil, nil)

	s.runner.Increment(s.Metric())
	s.runner.Flush(ctx)
}

func (s *RunnerSuite) TestMeasure() {
	ctx := context.Background()

	s.cloudwatch.
		On("PutMetricDataWithContext", ctx, mock.Anything).
		Run(func(args mock.Arguments) {
			input := args.Get(1).(*cloudwatch.PutMetricDataInput)
			s.Require().Len(input.MetricData, 1)
			s.Equal(s.Namespace(), aws.StringValue(input.Namespace))

			pt := input.MetricData[0]
			s.Equal(14.0, aws.Float64Value(pt.StatisticValues.Sum))
		}).
		Return(nil, nil)

	s.runner.Measure(s.Metric(), 14)
	s.runner.Flush(ctx)
}

func TestRunner(t *testing.T) {
	suite.Run(t, &RunnerSuite{})
}
