package stat

import (
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestBatcher(t *testing.T) {
	const metricName = "my-metric-name"

	dimensions := []*cloudwatch.Dimension{
		&cloudwatch.Dimension{
			Name:  aws.String("my-dimension-name"),
			Value: aws.String("my-dimension-value"),
		},
	}

	t0 := time.Now().
		UTC().
		Truncate(time.Duration(time.Minute))

	t.Run("Increment", func(t *testing.T) {
		t.Run("increment", func(t *testing.T) {
			b := &batcher{}

			b.Increment(Metric{
				MetricName:        metricName,
				StorageResolution: time.Hour,
			})

			data := b.Pop()
			require.Len(t, data, 1)

			pt := data[0]
			assert.Equal(t, metricName, *pt.MetricName)
			assert.Equal(t, &cloudwatch.StatisticSet{
				Maximum:     aws.Float64(1),
				Minimum:     aws.Float64(1),
				SampleCount: aws.Float64(1),
				Sum:         aws.Float64(1),
			}, pt.StatisticValues)
			assert.Equal(t, pt.Timestamp.Truncate(time.Hour), *pt.Timestamp)

			require.Len(t, b.Pop(), 0)
		})

		t.Run("increment twice", func(t *testing.T) {
			t.Run("same metric, time bucket", func(t *testing.T) {
				b := &batcher{}

				b.Increment(Metric{
					Dimensions:        dimensions,
					MetricName:        metricName,
					StorageResolution: time.Second,
					timeBucket:        timeBucket(t0),
				})
				b.Increment(Metric{
					Dimensions:        dimensions,
					MetricName:        metricName,
					StorageResolution: time.Second,
					timeBucket:        timeBucket(t0),
				})

				assert.Equal(t, []*cloudwatch.MetricDatum{
					&cloudwatch.MetricDatum{
						Dimensions:        dimensions,
						MetricName:        aws.String(metricName),
						Timestamp:         aws.Time(t0),
						StorageResolution: aws.Int64(1),
						StatisticValues: &cloudwatch.StatisticSet{
							Maximum:     aws.Float64(1),
							Minimum:     aws.Float64(1),
							SampleCount: aws.Float64(2),
							Sum:         aws.Float64(2),
						},
						Unit: aws.String(cloudwatch.StandardUnitCount),
					},
				}, b.Pop())

				require.Len(t, b.Pop(), 0)
			})

			t.Run("same metric, different time bucket", func(t *testing.T) {
				b := &batcher{}

				b.Increment(Metric{
					MetricName: metricName,
					timeBucket: timeBucket(t0),
				})
				b.Increment(Metric{
					MetricName: metricName,
					timeBucket: timeBucket(t0.Add(time.Minute)),
				})

				data := b.Pop()
				require.Len(t, data, 2)
				for _, pt := range data {
					assert.Equal(t, metricName, *pt.MetricName)
					assert.Equal(t, &cloudwatch.StatisticSet{
						Maximum:     aws.Float64(1),
						Minimum:     aws.Float64(1),
						SampleCount: aws.Float64(1),
						Sum:         aws.Float64(1),
					}, pt.StatisticValues)
				}

				require.Len(t, b.Pop(), 0)
			})

			t.Run("same metric, different dimensions", func(t *testing.T) {
				b := &batcher{}

				b.Increment(Metric{
					MetricName: metricName,
					timeBucket: timeBucket(t0),
				})
				b.Increment(Metric{
					Dimensions: dimensions,
					MetricName: metricName,
					timeBucket: timeBucket(t0),
				})

				data := b.Pop()
				require.Len(t, data, 2)
				for _, pt := range data {
					assert.Equal(t, metricName, *pt.MetricName)
					assert.Equal(t, &cloudwatch.StatisticSet{
						Maximum:     aws.Float64(1),
						Minimum:     aws.Float64(1),
						SampleCount: aws.Float64(1),
						Sum:         aws.Float64(1),
					}, pt.StatisticValues)
				}

				require.Len(t, b.Pop(), 0)
			})

			t.Run("different metric, same time bucket", func(t *testing.T) {
				b := &batcher{}

				b.Increment(Metric{
					MetricName: "metric0",
					timeBucket: timeBucket(t0),
				})
				b.Increment(Metric{
					MetricName: "metric1",
					timeBucket: timeBucket(t0),
				})

				data := b.Pop()
				require.Len(t, data, 2)
				for _, pt := range data {
					assert.Equal(t, &cloudwatch.StatisticSet{
						Maximum:     aws.Float64(1),
						Minimum:     aws.Float64(1),
						SampleCount: aws.Float64(1),
						Sum:         aws.Float64(1),
					}, pt.StatisticValues)
				}

				require.Len(t, b.Pop(), 0)
			})
		})
	})

	t.Run("Measure", func(t *testing.T) {
		t.Run("once", func(t *testing.T) {
			b := &batcher{}
			b.Measure(Metric{
				Dimensions: dimensions,
				MetricName: metricName,
				timeBucket: timeBucket(t0),
			}, 14.0)

			assert.Equal(t, []*cloudwatch.MetricDatum{
				&cloudwatch.MetricDatum{
					Dimensions:        dimensions,
					MetricName:        aws.String(metricName),
					Timestamp:         aws.Time(t0),
					StorageResolution: aws.Int64(60),
					StatisticValues: &cloudwatch.StatisticSet{
						Maximum:     aws.Float64(14.0),
						Minimum:     aws.Float64(14.0),
						SampleCount: aws.Float64(1),
						Sum:         aws.Float64(14.0),
					},
					Unit: aws.String(cloudwatch.StandardUnitCount),
				},
			}, b.Pop())
		})

		t.Run("twice", func(t *testing.T) {
			b := &batcher{}
			b.Measure(Metric{
				Dimensions: dimensions,
				MetricName: metricName,
				timeBucket: timeBucket(t0),
			}, 14.0)
			b.Measure(Metric{
				Dimensions: dimensions,
				MetricName: metricName,
				timeBucket: timeBucket(t0),
			}, 10.0)

			assert.Equal(t, []*cloudwatch.MetricDatum{
				&cloudwatch.MetricDatum{
					Dimensions:        dimensions,
					MetricName:        aws.String(metricName),
					Timestamp:         aws.Time(t0),
					StorageResolution: aws.Int64(60),
					StatisticValues: &cloudwatch.StatisticSet{
						Maximum:     aws.Float64(14.0),
						Minimum:     aws.Float64(10.0),
						SampleCount: aws.Float64(2),
						Sum:         aws.Float64(24.0),
					},
					Unit: aws.String(cloudwatch.StandardUnitCount),
				},
			}, b.Pop())
		})
	})
}
