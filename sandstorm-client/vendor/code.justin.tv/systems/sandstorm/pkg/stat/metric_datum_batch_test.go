package stat

import (
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
	"github.com/stretchr/testify/assert"
)

func TestMetricDatumBatch(t *testing.T) {
	t.Run("HasMore", func(t *testing.T) {
		t.Run("empty", func(t *testing.T) {
			b := &metricDatumBatch{}
			assert.False(t, b.HasMore())
		})

		t.Run("not empty", func(t *testing.T) {
			b := &metricDatumBatch{[]*cloudwatch.MetricDatum{
				{},
			}}
			assert.True(t, b.HasMore())
		})
	})

	t.Run("Pop", func(t *testing.T) {
		t.Run("empty", func(t *testing.T) {
			b := &metricDatumBatch{[]*cloudwatch.MetricDatum{}}

			assert.Empty(t, b.Pop(10))
		})

		t.Run("less than one batch", func(t *testing.T) {
			metric := &cloudwatch.MetricDatum{MetricName: aws.String("name-0")}

			b := &metricDatumBatch{[]*cloudwatch.MetricDatum{
				metric,
			}}

			assert.Equal(t, []*cloudwatch.MetricDatum{metric}, b.Pop(10))
			assert.Empty(t, b.Pop(10))
		})

		t.Run("exactly one batch", func(t *testing.T) {
			metric := &cloudwatch.MetricDatum{MetricName: aws.String("name-0")}

			b := &metricDatumBatch{[]*cloudwatch.MetricDatum{
				metric,
			}}

			assert.Equal(t, []*cloudwatch.MetricDatum{metric}, b.Pop(1))
			assert.Empty(t, b.Pop(1))
		})

		t.Run("more than one batch", func(t *testing.T) {
			m0 := &cloudwatch.MetricDatum{MetricName: aws.String("name-0")}
			m1 := &cloudwatch.MetricDatum{MetricName: aws.String("name-1")}

			b := &metricDatumBatch{[]*cloudwatch.MetricDatum{
				m0,
				m1,
			}}

			assert.Equal(t, []*cloudwatch.MetricDatum{m0}, b.Pop(1))
			assert.Equal(t, []*cloudwatch.MetricDatum{m1}, b.Pop(1))
			assert.Empty(t, b.Pop(1))
		})
	})
}
