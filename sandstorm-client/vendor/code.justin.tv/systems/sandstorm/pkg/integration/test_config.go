package integration

import (
	"fmt"
	"os"
	"sync"
	"testing"

	"code.justin.tv/systems/sandstorm/resource"
	"github.com/stretchr/testify/require"
)

var (
	testConfigLock  sync.Mutex
	testConfigValue testConfig
)

// Stack returns the current stack
func Stack(t *testing.T) resource.Stack {
	return testConfigValue.Stack(t)
}

// testConfig is the integration test config
type testConfig struct {
	stack resource.Stack
}

// Stack ...
func (tc *testConfig) Stack(t *testing.T) resource.Stack {
	testConfigLock.Lock()
	defer testConfigLock.Unlock()

	if tc.stack != nil {
		return tc.stack
	}

	var s resource.Stack
	var err error
	switch tc.stackType() {
	case "GlobalStack":
		s, err = resource.NewGlobalStack(tc.globalStackEnvironment())
		require.NoError(t, err)
	case "LocalStack":
		s, err = resource.NewLocalStack(
			tc.localStackAccountNumber(t),
			tc.localStackAWSRegion(t),
			tc.localStackStackName(t),
		)
		require.NoError(t, err)
	default:
		require.FailNow(t, fmt.Sprintf("invalid StackType: %s", tc.stackType()))
	}

	t.Logf("using stack: %#v", s)

	tc.stack = s
	return tc.stack
}

func (tc testConfig) getWithDefault(key, def string) string {
	if val := os.Getenv(key); val != "" {
		return val
	}
	return def
}

func (tc testConfig) getAndRequireNotEmpty(t *testing.T, key string) string {
	val := os.Getenv(key)
	require.NotEmpty(t, val)
	return val
}

func (tc testConfig) stackType() string {
	return tc.getWithDefault("INTEGRATION_STACK_TYPE", "GlobalStack")
}

func (tc testConfig) globalStackEnvironment() string {
	return tc.getWithDefault("INTEGRATION_GLOBAL_STACK_ENVIRONMENT", "testing")
}

func (tc testConfig) localStackAccountNumber(t *testing.T) string {
	return tc.getAndRequireNotEmpty(t, "INTEGRATION_LOCAL_STACK_ACCOUNT_NUMBER")
}

func (tc testConfig) localStackAWSRegion(t *testing.T) string {
	return tc.getAndRequireNotEmpty(t, "INTEGRATION_LOCAL_STACK_AWS_REGION")
}

func (tc testConfig) localStackStackName(t *testing.T) string {
	return tc.getAndRequireNotEmpty(t, "INTEGRATION_LOCAL_STACK_STACK_NAME")
}
