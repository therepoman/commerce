package backoff

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestExponentialRetry(t *testing.T) {
	t.Run("Try", func(t *testing.T) {
		myErr := errors.New("my-error")

		t.Run("success should return false", func(t *testing.T) {
			er := &ExponentialRetry{}
			assert.False(t, er.Try(context.Background(), nil))
		})

		t.Run("error should increment but return true if max tries not hit", func(t *testing.T) {
			er := &ExponentialRetry{
				MaxTries: 2,
			}

			assert.True(t, er.Try(context.Background(), myErr))
			assert.Equal(t, &ExponentialRetry{
				MaxTries: 2,
				err:      myErr,
				tries:    1,
			}, er)

		})

		t.Run("negative MaxTries should allow for infinite retries", func(t *testing.T) {
			er := &ExponentialRetry{
				MaxTries: -1,
			}

			for nTry := 0; nTry < 10; nTry++ {
				assert.True(t, er.Try(context.Background(), myErr))
				assert.Equal(t, &ExponentialRetry{
					MaxTries: -1,
					err:      myErr,
					tries:    nTry + 1,
				}, er)
			}

		})

		t.Run("error should increment but return false if max is hit", func(t *testing.T) {
			er := &ExponentialRetry{
				MaxTries: 1,
				Constant: time.Hour,
			}

			assert.False(t, er.Try(context.Background(), myErr))
			assert.Equal(t, &ExponentialRetry{
				MaxTries: 1,
				Constant: time.Hour,
				err:      myErr,
				tries:    1,
			}, er)
		})

		t.Run("error should increment but return not if wait context is cancelled", func(t *testing.T) {
			er := &ExponentialRetry{
				MaxTries: 2,
				Constant: time.Hour,
			}

			ctx, cancel := context.WithCancel(context.Background())
			cancel()

			assert.False(t, er.Try(ctx, myErr))
			assert.Equal(t, &ExponentialRetry{
				MaxTries: 2,
				Constant: time.Hour,
				err:      myErr,
				tries:    1,
			}, er)
		})
	})
}
