package backoff

import (
	"context"
	"time"
)

func ExampleExponentialRetry() {
	var ctx context.Context

	r := ExponentialRetry{
		Constant: time.Second,
		MaxTries: 10,
	}

	for r.Try(ctx, myFunc(ctx)) {
	}

	err := r.Err()
	if err != nil {
		// handle err
	}
}

func myFunc(context.Context) error {
	return nil
}
