# Sandstorm - PLEASE SEE [WIKI](https://wiki.twitch.com/display/SSE/Sandstorm%3A+Secret+Management+System)
[![dududu](https://static-cdn.jtvnw.net/emoticons/v1/62834/3.0)](https://www.youtube.com/watch?v=kelNeLvvEqw)

This repository is managed in GitFarm as
[Twitch-Systems-Sandstorm]. `mainline` of that repo is mirrored to here
automatically.

A service that makes managing, securing, and rotating Secrets fast and simple at Twitch.
Works for anything that shouldn't be stored in plaintext. API Keys, SSL Certs,
Sandstorm handles it all. See the [Presentation](https://docs.google.com/a/justin.tv/presentation/d/19fkfhvB_5LlJuljqRP2G0-4hQRNblJfvS5kil5ReBAw/edit?usp=sharing)
for an in-depth look at the project specification.

## Getting Started - WE'VE MOVED ALL USER DOCS TO THE [WIKI](https://wiki.twitch.com/display/SSE/Sandstorm%3A+Secret+Management+System)
Please use that as a source of canonical documentation. Please let us know if we missed anything on the wiki!

## Reporting issues

Either [open an issue](https://git-aws.internal.justin.tv/systems/sandstorm/issues)
or talk to us in [#secdev](https://twitch.slack.com/messages/secdev/)

## More Information

#### Functionality

Secrets are store encrypted in DynamoDB. Data is encrypted with
AES-256-GCM, with data keys created for each secret with KMS.

#### Database schema

There are three tables: the primary table, the audit table and the
namespaces table. The audit table is named `{primary_table}_audit` and
the namespaces table is named `{primary_table}_namespaces`

The primary table stores the secrets with `name` as the hash key. This
contains all the active secrets. A secret is represented as:

| Field            | Type    | Primary index | name_namespaces index |
|------------------|---------|---------------|-----------------------|
| name             | string  | Hash key      | Range key             |
| namespace        | string  |               | Hash key              |
| updated_at       | number  |               |                       |
| do_not_broadcast | boolean |               |                       |
| key              | string  |               |                       |
| value            | string  |               |                       |

A global secondary index, `namespace_name`, on the primary table uses
`namespace` as hash key and `name` as range key. This index is used to
allow IAM policies to allow access to certain namespaces, but be able
to list the secrets in a namespace. Namespaces are calculated as the
key name up to the first `/`

Encrypted values are stored in the `value` column and the encrypted
key is stored in the `key` column. `updated_at` is stored as a unix
timestamp.

The audit table includes all fields from the primary table, except it
has `name` as the hash key and `updated_at` as the range key. This
table stores all versions. The table also adds a `tombstone` boolean
field, representing when a secret was deleted from the active
table. The audit table also includes a `namespace_name` global
secondary index.

The namespaces table contains a list of possible namespaces. This is
to facilitate listing the namespaces, since an IAM role is not given
neither `dynamodb:Scan` nor `dynamodb:Query` for all hash key prefixes
privileges on the main or audit table.

#### Encryption

Values are encrypted using AES-256-GCM. Encryption keys are generated
from the specified KMS Master Key using `kms.GenerateDataKey`. The
`CiphertextBlob` is stored in the `key` column in DynamoDB. The
`PlaintextKey` is used as the key for `gcm.Seal`. The resulting
ciphertext from `gcm.Seal` is stored in the `value` column.

#### Decryption

Values are decrypted with AES-256-GCM. The value and key are fetched
from DynamoDB. The key is decrypted with the specified KMS Master Key
using `kms.Decrypt`. The resulting key plaintext is used by `gcm.Open` to
decrypt the ciphertext. The value plaintext is returned to the user in
the struct.



## Production API Logging

[Central Logging](https://syslog.internal.twitch.tv/#/discover?_a=(columns:!(_source),filters:!(),index:%5Blogstash-%5DYYYY.MM.DD,interval:auto,query:(query_string:(analyze_wildcard:!t,query:'hostname:sandstorm%20AND%20program:sandstorm-server')),sort:!('@timestamp',desc))&_g=(filters:!()))

Local server logging found at: `/var/log/jtv/sandstorm-server.log`

## Local Installation
[Instructions] (https://wiki.twitch.com/display/SSE/Using+Sandstorm+for+Development)

## Development

### Setup

First, run the following commands to get started.

```
ln -s Godeps/_vendor vendor
```

Development locally should be done using the `sandstorm-dev` user.

To export the correct aws credentials to your environment, install
[henshin](https://git-aws.internal.justin.tv/geoffcha/henshin)
and run the following command:

```
eval $(henshin env)
```

Once you're done, you can set your environment to the default value.

```
eval $(henshin deactivate)
```

You'll need the ability to assume the following role:

```
arn:aws:iam::734326455073:role/sandstorm/production/templated/role/sandstorm-dev
```

### Running tests with manta

All buildable executables are managed in the `./cmd/` folder. To test them,
point [manta](https://git-aws.internal.justin.tv/release/manta-style) to the
`.manta.json` file. They require the following environment variables to passed
into the build container:

- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`
- `PACKAGE_VER`

```
manta -e AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY -e PACKAGE_VER -f cmd/sandstorm-agent/.manta.json
```

[Twitch-Systems-Sandstorm]: https://code.amazon.com/packages/Twitch-Systems-Sandstorm
