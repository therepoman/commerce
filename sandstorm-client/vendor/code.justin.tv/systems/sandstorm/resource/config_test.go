package resource

import (
	"bytes"
	"errors"
	"fmt"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/systems/sandstorm/mocks"
)

type testReadCloser struct {
	*bytes.Buffer
}

func (trc *testReadCloser) Close() (err error) {
	return
}

func getS3KeyName(env string) string {
	return fmt.Sprintf("sandstorm-configs/%s.json", env)
}

func TestGetConfigForEnvironment(t *testing.T) {
	testBucket := "testBucket"
	testKey := "key/test"

	t.Run("default config", func(t *testing.T) {
		for _, env := range []string{"", "production"} {
			t.Run(fmt.Sprintf("is returned for %s env", env), func(t *testing.T) {
				conf, err := GetConfigForEnvironment(env)
				assert.Nil(t, err)
				assert.Equal(t, defaultResources(), conf)
			})
		}
	})

	t.Run("config cache", func(t *testing.T) {
		envName := "testing"

		svc := new(mocks.S3API)
		assert.True(t, svc.AssertExpectations(t))
		testConfigStr := `{
			"environment": "testing",
			"table_name" : "someTableName"
		}`
		testConfig := &s3.GetObjectOutput{
			Body: &testReadCloser{bytes.NewBufferString(testConfigStr)},
		}
		expectedConf := &Config{
			Environment: "testing",
			TableName:   "someTableName",
		}
		t.Run("non production configs are fetched from s3", func(t *testing.T) {
			input := &s3.GetObjectInput{
				Bucket: aws.String(s3SandstormBucketForConfig),
				Key:    aws.String(getS3KeyName(envName)),
			}
			svc.On("GetObject", input).Return(testConfig, nil).Once()
			conf, err := getConfigForEnvironment(svc, envName)
			assert.Nil(t, err)
			assert.Equal(t, expectedConf, conf)

			conf, err = getConfigForEnvironment(svc, envName)
			assert.Nil(t, err)
			assert.Equal(t, expectedConf, conf)
		})
	})

	t.Run("getS3ObjectAsBytes", func(t *testing.T) {
		svc := new(mocks.S3API)
		assert.True(t, svc.AssertExpectations(t))
		t.Run("Object does not exit", func(t *testing.T) {
			input := &s3.GetObjectInput{
				Bucket: aws.String(testBucket),
				Key:    aws.String(testKey),
			}
			awsErr := awserr.New(s3.ErrCodeNoSuchKey, "key does not exist", errors.New("test"))
			svc.On("GetObject", input).Return(&s3.GetObjectOutput{}, awsErr).Once()

			content, err := getS3ObjectAsBytes(svc, testBucket, testKey)
			assert.Empty(t, content)
			assert.NotNil(t, err)
		})

		t.Run("No content", func(t *testing.T) {
			input := &s3.GetObjectInput{
				Bucket: aws.String(testBucket),
				Key:    aws.String(testKey),
			}
			output := &s3.GetObjectOutput{
				Body: &testReadCloser{bytes.NewBufferString("")},
			}
			svc.On("GetObject", input).Return(output, nil).Once()

			content, err := getS3ObjectAsBytes(svc, testBucket, testKey)
			assert.Empty(t, content)
			assert.Nil(t, err)
		})

		t.Run("content", func(t *testing.T) {

			configContent := "testing content"

			input := &s3.GetObjectInput{
				Bucket: aws.String(testBucket),
				Key:    aws.String(testKey),
			}
			output := &s3.GetObjectOutput{
				Body: &testReadCloser{bytes.NewBufferString(configContent)},
			}
			svc.On("GetObject", input).Return(output, nil).Once()

			content, err := getS3ObjectAsBytes(svc, testBucket, testKey)
			assert.Nil(t, err)
			assert.Equal(t, string(content), configContent)
		})
	})

	t.Run("GetConfigForEnvironment", func(t *testing.T) {
		svc := new(mocks.S3API)
		assert.True(t, svc.AssertExpectations(t))
		t.Run("For a random env", func(t *testing.T) {
			configContent := `{ "environment": "testing content"}`
			output := &s3.GetObjectOutput{
				Body: &testReadCloser{bytes.NewBufferString(configContent)},
			}
			svc.On("GetObject", mock.Anything).Return(output, nil).Once()
			res, err := getConfigForEnvironment(svc, "testing")
			assert.Nil(t, err)
			assert.NotNil(t, res)
		})
	})
}
