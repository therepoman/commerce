// +build integration

package resource

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfigIntegration(t *testing.T) {
	envs := []string{
		"testing",
		"staging",
		"production",
	}

	clearCacheConfig := func() {
		for _, env := range envs {
			deleteCacheConfig(env)
		}
	}

	clearCacheConfig()
	defer clearCacheConfig()

	t.Run("read", func(t *testing.T) {

		for _, env := range envs {
			cfg, err := GetConfigForEnvironment(env)
			assert.NoError(t, err)
			assert.NotEmpty(t, cfg)
		}
	})
}
