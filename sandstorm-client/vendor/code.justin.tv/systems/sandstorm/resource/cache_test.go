package resource

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCache(t *testing.T) {
	envKey := "my-env-key"

	setup := func() {
		deleteCacheConfig(envKey)
	}

	teardown := func() {
		deleteCacheConfig(envKey)
	}

	t.Run("TestLoadStoreLoad", func(t *testing.T) {
		setup()
		defer teardown()

		cfg := &Config{Environment: "derp"}

		assert.Nil(t, loadCacheConfig(envKey))
		storeCacheConfig(envKey, cfg)
		assert.Equal(t, cfg, loadCacheConfig(envKey))
	})
}
