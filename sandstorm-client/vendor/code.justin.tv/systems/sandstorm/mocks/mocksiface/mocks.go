package mocksiface

import (
	"net/http"

	"github.com/aws/aws-sdk-go/service/cloudwatch/cloudwatchiface"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/iam/iamiface"
	"github.com/aws/aws-sdk-go/service/kms/kmsiface"
	"github.com/aws/aws-sdk-go/service/lambda/lambdaiface"
	"github.com/aws/aws-sdk-go/service/s3/s3iface"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/sirupsen/logrus"
)

type CloudWatchAPI interface {
	cloudwatchiface.CloudWatchAPI
}

type DynamoDBAPI interface {
	dynamodbiface.DynamoDBAPI
}

type IAMAPI interface {
	iamiface.IAMAPI
}

type LambdaAPI interface {
	lambdaiface.LambdaAPI
}

type KMSAPI interface {
	kmsiface.KMSAPI
}

type S3API interface {
	s3iface.S3API
}

type SQSAPI interface {
	sqsiface.SQSAPI
}

type RoundTripper interface {
	http.RoundTripper
}

type FieldLogger interface {
	logrus.FieldLogger
}
