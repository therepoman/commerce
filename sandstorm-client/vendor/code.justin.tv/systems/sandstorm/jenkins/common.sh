#!/bin/sh
set -eo pipefail

export BUILD="build"
export BUILD_ID=${GIT_COMMIT:-"local"}
export E2E_DOCKER_IMAGE_BASE="docker-registry.internal.justin.tv/sse/sandstorm-e2e-tests"
export E2E_DOCKER_IMAGE="$E2E_DOCKER_IMAGE_BASE:$BUILD_ID"
export SANDSTORM_DEPLOYMENT_ROLE="arn:aws:iam::734326455073:role/sandstorm/production/templated/role/sandstorm-deployment-testing"
