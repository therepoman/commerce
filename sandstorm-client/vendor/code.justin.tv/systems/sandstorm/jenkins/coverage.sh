#!/bin/sh
set -eo pipefail

source ./jenkins/common.sh

rm -rf .manta/ build/
mkdir -p build
echo 'full-test: starting full tests'
manta -update -v -e AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY -f .manta.json
sh ./scripts/lint.sh
