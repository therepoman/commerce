#!/bin/bash
set -eo pipefail

[ -z "$ENVIRONMENT" ] && echo '$ENVIRONMENT is required' && exit 1
[ -z "$GIT_COMMIT" ] && echo '$GIT_COMMIT is required' && exit 1

source ./jenkins/common.sh

DEPLOY_IMAGE="$E2E_DOCKER_IMAGE_BASE:$ENVIRONMENT"

docker pull $E2E_DOCKER_IMAGE
docker tag $E2E_DOCKER_IMAGE $DEPLOY_IMAGE
docker push $DEPLOY_IMAGE
