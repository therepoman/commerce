#!/bin/sh
set -eo pipefail

source ./jenkins/common.sh

rm -rf .manta/ $BUILD
mkdir -p $BUILD

sh ./scripts/lint.sh

manta -v -e AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY -f .manta.json
