package util

import (
	"fmt"
	"os"
	"path"

	log "github.com/sirupsen/logrus"
)

const (
	// DefaultFilePermissions for creating a new file
	DefaultFilePermissions = 0644
	// FolderPermissions for creating a new folder
	FolderPermissions = 0755
	// StateFilePermissions assigned to the agent state file
	StateFilePermissions = 0660
)

// CreateFile creates all missing folders, and a pointer to the end file
func CreateFile(filePath string, filePermissions os.FileMode) (*os.File, error) {
	dirPath := path.Dir(filePath)

	// Create missing dirs
	err := os.MkdirAll(dirPath, FolderPermissions)
	if err != nil {
		return nil, err
	}

	// Create file
	f, err := os.Create(filePath)
	if err != nil {
		return nil, err
	}

	err = os.Chmod(filePath, filePermissions)
	if err != nil {
		return nil, err
	}
	return f, nil
}

// ErrIfNotAbs determines if a file path is an absolute path
func ErrIfNotAbs(absPath string) error {
	if !path.IsAbs(absPath) {
		return fmt.Errorf(
			"path argument must be absolute path. got: %s",
			absPath,
		)
	}
	return nil
}

// FileExists is a convenience function for checking that a file exists
// for a specified path
func FileExists(path string) bool {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false
	}
	return true
}

// HandleFileClose closes an open file
func HandleFileClose(f *os.File) {
	err := f.Close()
	if err != nil {
		log.Errorf("error closing queue config file: %s", err.Error())
	}
}
