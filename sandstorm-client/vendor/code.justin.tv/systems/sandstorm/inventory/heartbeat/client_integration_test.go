// +build integration

package heartbeat

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/systems/sandstorm/pkg/integration"
	"code.justin.tv/systems/sandstorm/pkg/log"
	"code.justin.tv/systems/sandstorm/resource"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/iam"
	"github.com/aws/aws-sdk-go/service/iam/iamiface"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const integrationTestServiceName = "myService"
const integrationTestHostName = "myHostName"
const integrationTestFQDN = "myFQDN"
const integrationTestTTL = 259200

type clientIntegrationTest struct {
	client     *Client
	db         dynamodbiface.DynamoDBAPI
	iam        iamiface.IAMAPI
	secretName string
	callerArn  string
}

func (cit *clientIntegrationTest) getUserArn(t *testing.T) (userArn string) {
	return cit.callerArn
}

func (cit *clientIntegrationTest) deleteHeartbeat(t *testing.T) {
	stack := integration.Stack(t)

	_, err := cit.db.DeleteItem(&dynamodb.DeleteItemInput{
		TableName: aws.String(stack.HeartbeatsTableName()),
		Key: map[string]*dynamodb.AttributeValue{
			"secret": {S: aws.String(cit.secretName)},
			"host":   {S: aws.String(integrationTestHostName)},
		},
	})
	assert.NoError(t, err)
}

func (cit *clientIntegrationTest) assertHeartbeatEmpty(t *testing.T) {
	assert.Nil(t, cit.getHeartbeat(t))
}

func (cit *clientIntegrationTest) getHeartbeat(t *testing.T) (hb *DynamoHeartbeat) {
	stack := integration.Stack(t)

	output, err := cit.db.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(stack.HeartbeatsTableName()),
		Key: map[string]*dynamodb.AttributeValue{
			"secret": {S: aws.String(cit.secretName)},
			"host":   {S: aws.String(integrationTestHostName)},
		},
	})
	require.NoError(t, err)

	if output.Item == nil {
		return
	}

	hb = new(DynamoHeartbeat)
	require.NoError(t, dynamodbattribute.UnmarshalMap(output.Item, hb))

	return
}

func newClientIntegrationTest(t *testing.T) (cit *clientIntegrationTest) {
	stack := integration.Stack(t)
	sess := resource.STSSession(nil, stack.InventoryAdminRoleARN())

	secretID, err := uuid.NewV4()
	require.NoError(t, err)

	return &clientIntegrationTest{
		client: New(
			resource.AWSCredentials(nil, stack.InventoryAdminRoleARN()),
			Config{
				Service:                        integrationTestServiceName,
				Host:                           integrationTestHostName,
				FQDN:                           integrationTestFQDN,
				AWSRegion:                      stack.AWSRegion(),
				PutHeartbeatLambdaFunctionName: stack.PutHeartbeatLambdaFunctionName(),
			},
			&log.TestLogger{t},
		),
		db:         dynamodb.New(sess),
		iam:        iam.New(sess),
		secretName: secretID.String(),
		callerArn:  resource.GetAWSIdentity(nil, stack.InventoryAdminRoleARN()),
	}
}

func (cit *clientIntegrationTest) teardown(t *testing.T) {
	cit.deleteHeartbeat(t)
}

func TestClientIntegration(t *testing.T) {

	t.Run("putHeartbeat", func(t *testing.T) {
		// this will be set in intitialize secret heartbeat
		var firstRetrieval int64

		cit := newClientIntegrationTest(t)
		defer cit.teardown(t)

		t.Run("initial state", func(t *testing.T) {
			cit.assertHeartbeatEmpty(t)
		})

		t.Run("initialize secret heartbeat", func(t *testing.T) {
			const secretUpdatedAt = 101

			assert := assert.New(t)

			cit.client.UpdateHeartbeat(&Secret{
				Name:      cit.secretName,
				UpdatedAt: secretUpdatedAt,
			})

			firstRetrieval = cit.client.heartbeatState.secretsToReport[cit.secretName].FetchedAt

			err := cit.client.putHeartbeat()
			assert.NoError(err)

			hb := cit.getHeartbeat(t)
			require.NotNil(t, hb)

			receivedAt := hb.HeartbeatReceived
			assert.Equal(DynamoHeartbeat{
				Service:           integrationTestServiceName,
				Host:              integrationTestHostName,
				FQDN:              integrationTestFQDN,
				Secret:            cit.secretName,
				UpdatedAt:         secretUpdatedAt,
				FetchedAt:         firstRetrieval,
				ExpiresAt:         receivedAt + integrationTestTTL,
				HeartbeatReceived: receivedAt,
			}, *hb)
		})

		t.Run("updated heartbeat", func(t *testing.T) {
			const secretUpdatedAt = 105

			assert := assert.New(t)

			cit.client.UpdateHeartbeat(&Secret{
				Name:      cit.secretName,
				UpdatedAt: secretUpdatedAt,
			})

			fetchedAt := cit.client.heartbeatState.secretsToReport[cit.secretName].FetchedAt

			err := cit.client.putHeartbeat()
			assert.NoError(err)

			hb := cit.getHeartbeat(t)
			require.NotNil(t, hb)

			heartbeatReceived := hb.HeartbeatReceived

			assert.Equal(DynamoHeartbeat{
				Service:           integrationTestServiceName,
				Host:              integrationTestHostName,
				FQDN:              integrationTestFQDN,
				Secret:            cit.secretName,
				UpdatedAt:         secretUpdatedAt,
				FetchedAt:         fetchedAt,
				HeartbeatReceived: heartbeatReceived,
				ExpiresAt:         heartbeatReceived + integrationTestTTL,
			}, *hb)
		})
	})

	t.Run("start", func(t *testing.T) {
		// this will be set in first fetch
		var firstRetrieval int64

		cit := newClientIntegrationTest(t)
		defer cit.teardown(t)

		cit.client.Config.Interval = 100 * time.Millisecond

		defer func() {
			assert.NoError(t, cit.client.Stop())
		}()
		go cit.client.Start()

		t.Run("heartbeat should not be pushed if empty", func(t *testing.T) {
			cit.assertHeartbeatEmpty(t)
		})

		t.Run("heartbeat should be initialized on UpdateHeartbeat", func(t *testing.T) {
			assert := assert.New(t)
			const secretUpdatedAt = 177
			cit.client.UpdateHeartbeat(&Secret{
				Name:      cit.secretName,
				UpdatedAt: secretUpdatedAt,
			})

			firstRetrieval = cit.client.heartbeatState.secretsToReport[cit.secretName].FetchedAt

			var checked = false
			for nTry := 0; nTry < 50; nTry++ {
				time.Sleep(100 * time.Millisecond)

				hb := cit.getHeartbeat(t)
				if hb == nil {
					continue
				}
				heartbeatReceived := hb.HeartbeatReceived

				checked = true
				assert.Equal(DynamoHeartbeat{
					Service:           integrationTestServiceName,
					Host:              integrationTestHostName,
					FQDN:              integrationTestFQDN,
					Secret:            cit.secretName,
					UpdatedAt:         secretUpdatedAt,
					HeartbeatReceived: heartbeatReceived,
					FetchedAt:         firstRetrieval,
					ExpiresAt:         heartbeatReceived + integrationTestTTL,
				}, *hb)
				break
			}

			assert.True(checked)
		})

		var updateHeartbeatTime int64

		t.Run("heartbeat should be updated on UpdateHeartbeat", func(t *testing.T) {
			assert := assert.New(t)
			const secretUpdatedAt = 197
			cit.client.UpdateHeartbeat(&Secret{
				Name:      cit.secretName,
				UpdatedAt: secretUpdatedAt,
			})

			currentFetchedAt := cit.client.heartbeatState.secretsToReport[cit.secretName].FetchedAt

			var checked = false
			for nTry := 0; nTry < 50; nTry++ {
				time.Sleep(100 * time.Millisecond)

				hb := cit.getHeartbeat(t)
				if hb == nil || secretUpdatedAt != hb.UpdatedAt {
					continue
				}

				heartbeatReceived := hb.HeartbeatReceived
				updateHeartbeatTime = heartbeatReceived

				checked = true
				assert.Equal(DynamoHeartbeat{
					Service:           integrationTestServiceName,
					Host:              integrationTestHostName,
					FQDN:              integrationTestFQDN,
					Secret:            cit.secretName,
					UpdatedAt:         secretUpdatedAt,
					FetchedAt:         currentFetchedAt,
					HeartbeatReceived: heartbeatReceived,
					ExpiresAt:         heartbeatReceived + integrationTestTTL,
				}, *hb)
				break
			}

			assert.True(checked)
		})

		t.Run("heartbeat should be flushed on SendHeartbeat", func(t *testing.T) {
			assert := assert.New(t)
			const secretUpdatedAt = 197

			currentFetchedAt := cit.client.heartbeatState.secretsToReport[cit.secretName].FetchedAt

			ctx, cancel := context.WithTimeout(context.Background(), time.Second)
			defer cancel()
			cit.client.FlushHeartbeat(ctx)

			var checked = false
			for nTry := 0; nTry < 50; nTry++ {
				time.Sleep(100 * time.Millisecond)

				hb := cit.getHeartbeat(t)
				if hb == nil || secretUpdatedAt != hb.UpdatedAt {
					continue
				}

				heartbeatReceived := hb.HeartbeatReceived

				if heartbeatReceived == updateHeartbeatTime {
					continue
				}

				checked = true
				assert.True(heartbeatReceived > updateHeartbeatTime, "flushed heartbeat should be after updated heartbeat")
				assert.Equal(DynamoHeartbeat{
					Service:           integrationTestServiceName,
					Host:              integrationTestHostName,
					FQDN:              integrationTestFQDN,
					Secret:            cit.secretName,
					UpdatedAt:         secretUpdatedAt,
					FetchedAt:         currentFetchedAt,
					HeartbeatReceived: heartbeatReceived,
					ExpiresAt:         heartbeatReceived + integrationTestTTL,
				}, *hb)
				break
			}

			assert.True(checked)
		})
	})
}
