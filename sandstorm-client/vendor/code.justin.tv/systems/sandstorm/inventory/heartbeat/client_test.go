package heartbeat

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/systems/sandstorm/mocks"
	"code.justin.tv/systems/sandstorm/pkg/log"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type clientTest struct {
	client        *Client
	mockLambdaAPI *mocks.LambdaAPI
}

func newClientTest(t *testing.T) (ct *clientTest) {
	mockLambdaAPI := new(mocks.LambdaAPI)
	return &clientTest{
		client: &Client{
			heartbeatState: newHeartbeatState(),
			lambda:         mockLambdaAPI,
			Config: Config{
				Service: "TestService",
				Host:    "TestHost",
			},
			Logger:    &log.TestLogger{T: t},
			flushChan: make(chan struct{}, 1),
		},
		mockLambdaAPI: mockLambdaAPI,
	}
}

func (ct *clientTest) teardown(t *testing.T) {
	ct.mockLambdaAPI.AssertExpectations(t)
}

func TestSendHeartbeat(t *testing.T) {
	t.Run("put should not be called if no secrets", func(t *testing.T) {
		ct := newClientTest(t)
		defer ct.teardown(t)

		ct.client.SendHeartbeat()
	})

	t.Run("put should called if there are secrets", func(t *testing.T) {
		ct := newClientTest(t)
		defer ct.teardown(t)

		ct.client.UpdateHeartbeat(&Secret{
			Name:      "mySecretName",
			UpdatedAt: 123456,
		})

		ct.mockLambdaAPI.On("Invoke", mock.Anything).Return(nil, nil).Once()

		ct.client.SendHeartbeat()
	})
}

func TestUpdateHeartbeat(t *testing.T) {
	t.Run("update heartbeat updates the current heartbeat state", func(t *testing.T) {
		assert := assert.New(t)
		ct := newClientTest(t)
		defer ct.teardown(t)
		invClient := ct.client

		secretName := "test"
		assert.Equal(0, len(invClient.heartbeatState.secretsToReport), "invalid number of secrets to report")

		secret := &Secret{
			Name:      secretName,
			UpdatedAt: 123456,
		}
		invClient.UpdateHeartbeat(secret)
		assert.Equal(1, len(invClient.heartbeatState.secretsToReport), "invalid number of secrets to report")

		//Check FetchedAt timestamp
		ts := time.Now().Unix()
		assert.True(invClient.heartbeatState.secretsToReport[secretName].FetchedAt <= ts, "invalid retrieved at timestamp set")
		assert.True(invClient.heartbeatState.secretsToReport[secretName].FetchedAt > 0, "retrieved at timestamp was not set")
	})
}

func TestFlushHeartbeat(t *testing.T) {
	ct := newClientTest(t)
	defer ct.teardown(t)
	invClient := ct.client

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	invClient.FlushHeartbeat(ctx)

	received := false
	select {
	case <-invClient.flushChan:
		received = true
	case <-ctx.Done():
	}
	assert.True(t, received)
}
