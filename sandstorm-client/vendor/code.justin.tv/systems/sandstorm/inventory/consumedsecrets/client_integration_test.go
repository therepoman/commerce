// +build integration

package consumedsecrets

import (
	"testing"

	"code.justin.tv/systems/sandstorm/inventory/heartbeat"
	"code.justin.tv/systems/sandstorm/pkg/integration"
	"code.justin.tv/systems/sandstorm/resource"

	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const integretionTestTTL = 86400
const integrationCallerArn = "myCallerArn"

var integrationHosts = []string{
	"host1",
	"host2",
}

var integrationSecrets = []string{
	"secret1",
	"secret2",
}

type clientIntegrationTest struct {
	client     *Client
	adminCreds *credentials.Credentials
	testPrefix string
	cb         IterationHandler
	calls      []*ConsumedSecret
	stack      resource.Stack
}

func newClientIntegrationTest(t *testing.T) (cit *clientIntegrationTest) {
	stack := integration.Stack(t)

	cfg := &Config{
		TableName: stack.HeartbeatsTableName(),
	}

	testPrefix, err := uuid.NewV4()
	require.NoError(t, err)

	cit = &clientIntegrationTest{
		client:     New(cfg, resource.STSSession(nil, stack.InventoryAdminRoleARN())),
		adminCreds: resource.AWSCredentials(nil, stack.InventoryAdminRoleARN()),
		testPrefix: testPrefix.String(),
		stack:      stack,
	}

	cit.cb = func(cs *ConsumedSecret) (err error) {
		cit.calls = append(cit.calls, cs)
		return
	}

	return
}

func (cit *clientIntegrationTest) resetCallback() {
	cit.calls = []*ConsumedSecret{}

	cit.cb = func(cs *ConsumedSecret) (err error) {
		cit.calls = append(cit.calls, cs)
		return
	}
}

func (cit *clientIntegrationTest) setup(t *testing.T) {
	cit.putHeartbeatCombos(t)
}

func (cit *clientIntegrationTest) teardown(t *testing.T) {
}

func (cit *clientIntegrationTest) putHeartbeatCombos(t *testing.T) {
	cfg := heartbeat.Config{
		AWSRegion:                      cit.stack.AWSRegion(),
		PutHeartbeatLambdaFunctionName: cit.stack.PutHeartbeatLambdaFunctionName(),
		ManagerVersion:                 "integration-test",
		InstanceID:                     "my-instance-id",
	}

	for _, host := range integrationHosts {
		for _, secret := range integrationSecrets {
			cfg.Service = cit.testPrefix + "my-service-name"
			cfg.Host = cit.testPrefix + host

			hb := heartbeat.New(cit.adminCreds, cfg, nil)
			hb.UpdateHeartbeat(&heartbeat.Secret{
				Name:      cit.testPrefix + secret,
				UpdatedAt: 1948,
			})

			hb.SendHeartbeat()
		}
	}
}

func TestClientIntegration(t *testing.T) {
	cit := newClientIntegrationTest(t)
	cit.setup(t)
	defer cit.teardown(t)

	t.Run("query for existing host", func(t *testing.T) {
		defer cit.resetCallback()
		testHost := cit.testPrefix + "host1"

		assert := assert.New(t)

		err := cit.client.IterateByHost(testHost, cit.cb)
		assert.NoError(err)

		assert.Len(cit.calls, 2)
		for _, consumer := range cit.calls {
			assert.Equal(testHost, consumer.Host)
			assert.NotEmpty(consumer.HeartbeatReceived)
		}
	})

	t.Run("query for existing secret", func(t *testing.T) {
		defer cit.resetCallback()
		secretName := cit.testPrefix + "secret1"

		assert := assert.New(t)

		err := cit.client.IterateBySecret(secretName, cit.cb)
		assert.NoError(err)

		assert.Len(cit.calls, 2)
		for _, consumer := range cit.calls {
			assert.Equal(secretName, consumer.Secret)
			assert.NotEmpty(consumer.HeartbeatReceived)
		}
	})

	t.Run("query for non existing host", func(t *testing.T) {
		defer cit.resetCallback()
		assert := assert.New(t)
		err := cit.client.IterateByHost("muh-entropy", cit.cb)
		assert.Empty(cit.calls)
		assert.NoError(err)
	})

	t.Run("query for non existing secret", func(t *testing.T) {
		defer cit.resetCallback()
		assert := assert.New(t)
		err := cit.client.IterateBySecret("derp", cit.cb)
		assert.Empty(cit.calls)
		assert.NoError(err)
	})
}
