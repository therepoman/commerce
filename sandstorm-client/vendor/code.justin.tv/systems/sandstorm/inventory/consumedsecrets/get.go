package consumedsecrets

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

// dynamodb primary keys
const (
	compositeKeyKey = "composite_key"
	hostKey         = "host"
	serviceKey      = "service"
	secretKey       = "secret"
)

// dynamo other keys
const (
	version          = "version"
	updatedAt        = "updated_at"
	firstRetrievedAt = "first_retrieved_at"
	tombstone        = "tombstone"
	expiresAt        = "expires_at"
)

// indexes
const (
	hostIndex = "host-index"
)

// IterateBySecret returns consumers retrieved from dynamodb
func (c *Client) IterateBySecret(secret string, cb IterationHandler) (err error) {
	queryInput := c.inputForGet(secretKey, "", secret)
	err = c.iterate(queryInput, cb)
	return
}

// IterateByHost retrieves consumers associated with a host
func (c *Client) IterateByHost(host string, cb IterationHandler) (err error) {
	queryInput := c.inputForGet(hostKey, hostIndex, host)
	err = c.iterate(queryInput, cb)
	return
}

func (c *Client) inputForGet(attributeName string, indexName string, attributeValue string) (queryInput *dynamodb.QueryInput) {
	queryInput = &dynamodb.QueryInput{
		TableName: aws.String(c.TableName),
		ExpressionAttributeNames: map[string]*string{
			"#N": aws.String(attributeName),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":value": &dynamodb.AttributeValue{S: aws.String(attributeValue)},
		},
		Limit:                  aws.Int64(1000),
		KeyConditionExpression: aws.String("#N = :value"),
	}

	if indexName != "" {
		queryInput.IndexName = aws.String(indexName)
	}
	return
}

func (c *Client) iterate(queryInput *dynamodb.QueryInput, cb IterationHandler) (err error) {
	h := &iterationHandler{
		cb: cb,
	}
	err = c.DynamoDB.QueryPages(queryInput, h.handlePages)
	if h.err != nil {
		err = h.err
		return
	}
	if err != nil {
		return
	}
	return
}

type iterationHandler struct {
	err error
	cb  IterationHandler
}

func (ih *iterationHandler) handlePages(output *dynamodb.QueryOutput, lastPage bool) (cont bool) {
	var c *ConsumedSecret

	for _, item := range output.Items {
		c = new(ConsumedSecret)
		ih.err = dynamodbattribute.UnmarshalMap(item, c)
		if ih.err != nil {
			return
		}

		ih.err = ih.cb(c)
		if ih.err != nil {
			return
		}
	}

	cont = !lastPage
	return
}
