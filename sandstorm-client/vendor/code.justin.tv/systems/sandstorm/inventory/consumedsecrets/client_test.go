package consumedsecrets

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestClient(t *testing.T) {
	t.Run("Client should implement API", func(t *testing.T) {
		var api API
		api = &Client{}
		assert.NotNil(t, api)
	})
}
