# Inventory

![inventory high level](docs/arch.png)

## Inventory DynamoDB Table

The hash key for the dynamodb table is a composite key made up of `caller arn`, 
`host`, `service`, and `secret`:

`<caller_arn>:<host>:<service>:<secret>`

| Column | Key Type | Type |
| ------ | -------- | ---- |
| `composite_key` | `HASH` | `STRING` |
| `host` | | `STRING` |
| `service` | | `STRING` |
| `secret` | | `STRING` |
| `version` | | `NUMBER` |
| `updated_at` | | `STRING` timestamp represented using `RFC3339` format |

### ServiceIndex

| Column | Key Type | Type |
| ------ | -------- | ---- |
| `service` | `HASH` | `STRING` |
| `composite_key` | `RANGE` | `STRING` |

### SecretIndex

| Column | Key Type | Type |
| ------ | -------- | ---- |
| `secret` | `HASH` | `STRING` |
| `composite_key` | `RANGE` | `STRING` |

### HostIndex

| Column | Key Type | Type |
| ------ | -------- | ---- |
| `host` | `HASH` | `STRING` |
| `composite_key` | `RANGE` | `STRING` |

All other fields are projected onto the index

## Inventory Management

The inventory service receives heartbeats from each sandstorm `Manager`
instance. Each heartbeat reports a service name, which is user-defined and
defaults to the name of the binary, hostname, and list of secrets that have been
retrieved and the versions of those secrets. On receiving the heartbeat, the
inventory service constructs the composite primary key and writes the record
into the dynamodb table.

The data can be sent to the inventory apiserver using a PUT to "/heartbeat"
endpoint.  The body of the request should be a JSON in the following format.

```
PUT /heartbeat
Body:
{
    "service": "serviceNam",
    "host": "hostName",
    "secrets": [
        {
            "name": "secret",
            "version" : 12345678,
            "updated_at": 23456789,
            "tombstone": false,
            "do_not_broadcast": false,
            "key_arn": "key arn used"
        }
    ]
}
```

-  Heartbeat are processed in asynchronous manner and apiserver
will return HTTP Status No Content(204) as soon as it accepts the payload.
- Invalid JSON would return in HTTP Status Unprocessable Entity(422)

### Stale entries

The inventory manages stale entries using the dynamoDB's TTL feature to remove
all the heartbeat that are stale.  The table has an attribute `expires_at` which
stores timestamp as which the heartbeart will be considered stale and removed.
More on [DynamoDB TTL](http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/TTL.html).
