terraform {
  backend "s3" {
    profile = "twitch-sse-dev"
    bucket  = "sse-inventory"
    key     = "api-gateway/state/tf.state"
    region  = "us-west-2"
  }
}
