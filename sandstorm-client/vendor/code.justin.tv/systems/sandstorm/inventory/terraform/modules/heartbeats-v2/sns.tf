resource "aws_sns_topic" "heartbeats" {
  name = "heartbeats-${var.env}"
}

resource "aws_sns_topic_policy" "heartbeats" {
  arn    = "${aws_sns_topic.heartbeats.arn}"
  policy = "${data.aws_iam_policy_document.heartbeats-p.json}"
}

data "aws_iam_policy_document" "heartbeats-p" {
  statement {
    actions = [
      "SNS:Subscribe",
      "SNS:Receive",
      "SNS:GetTopicAttributes",
    ]

    effect = "Allow"

    principals {
      type = "AWS"

      identifiers = ["${var.whitelisted_sns_arns}"]
    }

    resources = [
      "${aws_sns_topic.heartbeats.arn}",
    ]
  }
}
