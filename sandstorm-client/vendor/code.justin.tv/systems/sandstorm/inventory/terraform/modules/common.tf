// allow sandstorm account and sse account to assume any role
data "aws_iam_policy_document" "base-assume-role-policy" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "AWS"

      identifiers = [
        "${formatlist(
          "arn:aws:iam::%s:root",
          concat(
            var.aws_sandstorm_accounts,
            list(var.account_id)
          ),
        )}",
      ]
    }
  }
}
