###################
# Lambda Function #
###################

resource "aws_lambda_function" "put_heartbeat" {
  function_name = "put_heartbeat${var.env_suffix}"
  role          = "${aws_iam_role.lambda_role.arn}"
  handler       = "main.put_heartbeat"
  runtime       = "python3.6"
  s3_bucket     = "${var.lambda_s3_bucket}"
  s3_key        = "lambdas/${var.env}.zip"
  timeout       = 30

  environment {
    variables {
      INVENTORY_V2_TABLE_NAME = "${module.heartbeats-v2.table_name}"
      INVENTORY_TTL           = "259200"
    }
  }
}

resource "aws_lambda_permission" "put_heartbeat_invoke_function" {
  statement_id  = "sse-inventory-allow-gateway-execute-lambda-for-put-heartbeat"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.put_heartbeat.function_name}"
  principal     = "apigateway.amazonaws.com"
  source_arn    = "arn:aws:execute-api:${var.region}:${var.account_id}:${aws_api_gateway_rest_api.api.id}/${var.env}/${aws_api_gateway_method.heartBeatMethod.http_method}/${aws_api_gateway_resource.heartBeat.path_part}"
}

resource "aws_iam_role" "lambda-deploy" {
  name               = "inventory-lambda-deploy${var.env_suffix}"
  assume_role_policy = "${data.aws_iam_policy_document.lambda-deploy-arp.json}"
}

resource "aws_iam_policy" "lambda-deploy" {
  name        = "inventory-lambda-deploy${var.env_suffix}"
  path        = "/"
  description = "gives lambda access to s3 bucket deploy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "UpdateLambda",
      "Effect": "Allow",
      "Resource": [
        "${aws_lambda_function.put_heartbeat.arn}",
        "${module.heartbeats-v2.lambda_arn}"
      ],
      "Action": [
        "lambda:UpdateFunctionCode"
      ]
    }
  ]
}
EOF
}

data "aws_iam_policy_document" "lambda-deploy-arp" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "AWS"

      identifiers = [
        "arn:aws:iam::${var.deploy_account}:root",
      ]
    }
  }
}

resource "aws_iam_policy_attachment" "lambda-deploy" {
  name = "lambda-deploy${var.env_suffix}"

  roles = [
    "${aws_iam_role.admin.name}",
    "${aws_iam_role.lambda-deploy.name}",
  ]

  policy_arn = "${aws_iam_policy.lambda-deploy.arn}"
}

resource "aws_iam_policy" "artifacts_read" {
  name = "inventory-artifacts-read${var.env_suffix}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:ListBucket"
      ],
      "Effect": "Allow",
      "Resource": "${var.lambda_s3_bucket_arn}"
    },
    {
      "Action": [
        "s3:GetObject"
      ],
      "Effect": "Allow",
      "Resource": "${var.lambda_s3_bucket_arn}/lambdas/${var.env}.zip"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "artifacts_read" {
  name = "artifacts-read${var.env_suffix}"

  roles = [
    "${aws_iam_role.admin.name}",
    "${aws_iam_role.lambda-deploy.name}",
  ]

  policy_arn = "${aws_iam_policy.artifacts_read.arn}"
}
