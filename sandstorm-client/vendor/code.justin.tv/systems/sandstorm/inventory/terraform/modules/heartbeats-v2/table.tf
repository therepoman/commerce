resource "aws_dynamodb_table" "heartbeats" {
  name             = "heartbeats-v2-${var.env}"
  read_capacity    = "${var.read_capacity}"
  write_capacity   = "${var.write_capacity}"
  hash_key         = "secret"
  range_key        = "host"
  stream_enabled   = true
  stream_view_type = "OLD_IMAGE"

  attribute {
    name = "host"
    type = "S"
  }

  attribute {
    name = "secret"
    type = "S"
  }

  ttl {
    attribute_name = "expires_at"
    enabled        = true
  }

  global_secondary_index {
    name            = "host-index"
    hash_key        = "host"
    range_key       = "secret"
    read_capacity   = "${var.read_capacity}"
    write_capacity  = "${var.write_capacity}"
    projection_type = "ALL"
  }

  tags {
    Name        = "heartbeats-v2"
    Environment = "${var.env}"
  }

  lifecycle {
    ignore_changes = [
      // TODO: this is the only way to ignore gsi write, read cap settings
      // comment if you actually want to change the GSI and re-add
      "global_secondary_index",

      "read_capacity",
      "write_capacity",
    ]
  }
}
