resource "aws_lambda_function" "heartbeats-update" {
  function_name = "heartbeats-v2-update-${var.env}"
  role          = "${aws_iam_role.heartbeats-update.arn}"
  handler       = "main.heartbeats_update"
  runtime       = "python3.6"
  s3_bucket     = "${var.lambda_s3_bucket}"
  s3_key        = "lambdas/${var.env}.zip"
  timeout       = 30

  environment {
    variables {
      INVENTORY_SNS_TOPIC = "${aws_sns_topic.heartbeats.arn}"
    }
  }
}

resource "aws_lambda_event_source_mapping" "heartbeats-update" {
  batch_size        = 1
  event_source_arn  = "${aws_dynamodb_table.heartbeats.stream_arn}"
  enabled           = true
  function_name     = "${aws_lambda_function.heartbeats-update.arn}"
  starting_position = "LATEST"
}

resource "aws_iam_role" "heartbeats-update" {
  name               = "heartbeats-update-lambda-${var.env}"
  assume_role_policy = "${data.aws_iam_policy_document.heartbeats-update-arp.json}"
}

data "aws_iam_policy_document" "heartbeats-update-arp" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"

      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
  }
}

resource "aws_iam_role_policy" "heartbeats-update" {
  name = "heartbeats-update-lambda-${var.env}"
  role = "${aws_iam_role.heartbeats-update.name}"

  policy = "${data.aws_iam_policy_document.heartbeats-update-rp.json}"
}

data "aws_iam_policy_document" "heartbeats-update-rp" {
  statement {
    actions = [
      "sns:Publish",
    ]

    resources = [
      "${aws_sns_topic.heartbeats.arn}",
    ]
  }

  statement {
    actions = [
      "dynamodb:GetRecords",
      "dynamodb:GetShardIterator",
      "dynamodb:DescribeStream",
      "dynamodb:ListStreams",
    ]

    resources = [
      "${aws_dynamodb_table.heartbeats.stream_arn}",
    ]
  }

  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = [
      "arn:aws:logs:*:*:*",
    ]
  }
}
