provider "aws" {
  region              = "${var.region}"
  profile             = "${var.aws_profile}"
  allowed_account_ids = ["${var.account_id}"]
}

variable "env" {}

variable "account_id" {
  description = "account-id to be used"
}

variable "aws_profile" {
  description = "profile to be used"
}

variable "lambda_s3_bucket" {}

variable "read_capacity" {
  description = "read capacity allocated for the heartbeats table"
  default     = 10
}

variable "region" {
  description = "AWS Secret key for user with IAM credentials"
  default     = "us-west-2"
}

variable "whitelisted_sns_arns" {
  type = "list"
}

variable "write_capacity" {
  description = "write capacity allocated for the heartbeats table"
  default     = 10
}

output "lambda_arn" {
  value = "${aws_lambda_function.heartbeats-update.arn}"
}

output "table_arn" {
  value = "${aws_dynamodb_table.heartbeats.arn}"
}

output "table_name" {
  value = "${aws_dynamodb_table.heartbeats.name}"
}
