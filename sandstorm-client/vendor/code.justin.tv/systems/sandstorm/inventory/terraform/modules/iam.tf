// #############
// # IAM ROLES #
// #############

data "aws_iam_policy_document" "admin-arp" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    effect = "Allow"

    principals {
      type = "AWS"

      identifiers = [
        "${formatlist(
            "arn:aws:iam::%s:root",
            concat(
              list(var.account_id),
              var.aws_sandstorm_accounts,
            ),
          )}",
      ]
    }
  }
}

resource "aws_iam_role" "admin" {
  name               = "inventory-admin${var.env_suffix}"
  description        = "admin user used in sandstorm tests"
  assume_role_policy = "${data.aws_iam_policy_document.admin-arp.json}"
}

resource "aws_iam_role_policy" "admin_assume_lambda_role" {
  name = "admin_role_assume_lambda_role${var.env_suffix}"
  role = "${aws_iam_role.admin.name}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "sts:AssumeRole"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_iam_role.lambda_role.arn}"
      ]
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy" "lambda_invoke" {
  name = "inventory-lambda-invoke-function${var.env_suffix}"
  role = "${var.cloudwatch_role_name}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "lambda:InvokeFunction",
      "Effect": "Allow",
      "Resource": "${aws_lambda_function.put_heartbeat.arn}"
    }
  ]
}
POLICY
}

data "aws_iam_policy_document" "role_execute_api-arp" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    effect = "Allow"

    principals {
      type = "AWS"

      identifiers = [
        "${formatlist(
            "arn:aws:iam::%s:root",
            var.aws_sandstorm_accounts,
        )}",
      ]
    }
  }
}

resource "aws_iam_role" "role_execute_api" {
  name               = "inventory-gateway-execute-api-invoke${var.env_suffix}"
  assume_role_policy = "${data.aws_iam_policy_document.role_execute_api-arp.json}"
}

resource "aws_iam_policy" "policy_execute_api" {
  name = "inventory-gateway-execute-api_invoke${var.env_suffix}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [
          "execute-api:*"
        ],
        "Resource": [
          "arn:aws:execute-api:${var.region}:${var.account_id}:${aws_api_gateway_rest_api.api.id}/*"
        ]
      },
      {
        "Effect": "Allow",
        "Action": [
          "lambda:InvokeFunction"
        ],
        "Resource": [
          "${aws_lambda_function.put_heartbeat.arn}"
        ]
      }
    ]
  }
POLICY
}

resource "aws_iam_policy_attachment" "policy_execute_api" {
  name = "policy_execute_api${var.env_suffix}"

  roles = [
    "${aws_iam_role.role_execute_api.name}",
    "${aws_iam_role.admin.name}",
  ]

  policy_arn = "${aws_iam_policy.policy_execute_api.arn}"
}

##################################################
#####  Grant read permission on dynamodb #########
##################################################

resource "aws_iam_policy" "inventory-read-data" {
  name = "inventory-dynamodb-read${var.env_suffix}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "dynamodb:Query",
                "dynamodb:Scan",
                "dynamodb:GetItem"
            ],
            "Effect": "Allow",
            "Resource": [
                "${module.heartbeats-v2.table_arn}",
                "${module.heartbeats-v2.table_arn}/index/*"
            ]
        }
    ]
}
POLICY
}

resource "aws_iam_policy_attachment" "inventory-read-data" {
  name = "attach_dynamo_read_permission_to_role"

  roles = [
    "${aws_iam_role.stormwatch.name}",
    "${aws_iam_role.admin.name}",
  ]

  policy_arn = "${aws_iam_policy.inventory-read-data.arn}"
}

##################################################
##### Grant write permission on dynamodb #########
##################################################

resource "aws_iam_policy" "inventory-write-data" {
  name = "inventory-lambda-dynamodb${var.env_suffix}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "dynamodb:PutItem",
                "dynamodb:UpdateItem"
            ],
            "Effect": "Allow",
            "Resource": [
                "${module.heartbeats-v2.table_arn}"
            ]
        }
    ]
}
POLICY
}

##################################################
# Grant dynamodb write permission to lambda      #
##################################################

resource "aws_iam_policy_attachment" "inventory-write-data" {
  name = "attach_dynamo_permission_to_role"

  roles = [
    "${aws_iam_role.lambda_role.name}",
    "${aws_iam_role.admin.name}",
  ]

  policy_arn = "${aws_iam_policy.inventory-write-data.arn}"
}

##################################################
##########      IAM role for lambda     ##########
##################################################

resource "aws_iam_role" "lambda_role" {
  name = "inventory-lambda-service${var.env_suffix}"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com",
        "AWS": [
          "arn:aws:iam::${var.account_id}:root"
        ]
      },
      "Effect": "Allow"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy" "cloudwatch_logging" {
  name = "inventory-cloudwatch-logging${var.env_suffix}"
  role = "${aws_iam_role.lambda_role.name}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "arn:aws:logs:*:*:*"
        }
    ]
}
POLICY
}

##################################################
##########    IAM role for stormwatch   ##########
##################################################

resource "aws_iam_role" "stormwatch" {
  name               = "inventory-stormwatch-service${var.env_suffix}"
  assume_role_policy = "${data.aws_iam_policy_document.stormwatch-arp.json}"
}

data "aws_iam_policy_document" "stormwatch-arp" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    effect = "Allow"

    principals {
      type = "AWS"

      identifiers = ["${formatlist(
        "arn:aws:iam::%s:root",
        concat(
          list(var.account_id, var.aws_stormwatch_account),
          var.aws_sandstorm_accounts,
        ),
        )}"]
    }
  }
}

resource "aws_iam_policy" "inventory-delete-data" {
  name = "inventory-dynamodb-delete${var.env_suffix}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "dynamodb:DeleteItem"
            ],
            "Effect": "Allow",
            "Resource": [
                "${module.heartbeats-v2.table_arn}"
            ]
        }
    ]
}
POLICY
}

resource "aws_iam_policy_attachment" "inventory-delete-data" {
  name = "attach_dynamo_delete_permission_to_role"

  roles = [
    "${aws_iam_role.admin.name}",
  ]

  policy_arn = "${aws_iam_policy.inventory-delete-data.arn}"
}
