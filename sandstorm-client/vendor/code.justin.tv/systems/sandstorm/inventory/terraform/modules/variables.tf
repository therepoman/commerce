variable "region" {
  description = "AWS Secret key for user with IAM credentials"
  default     = "us-west-2"
}

variable "aws_profile" {
  description = "profile to be used"
}

variable "env_suffix" {
  description = "suffix to use when creating policies, roles, etc."
}

variable "env" {}

variable "account_id" {
  description = "account-id to be used"
}

variable "cloudwatch_role_name" {}

variable "deploy_account" {
  description = "deployment account id with deployment role"
}

variable "lambda_s3_bucket" {
  default = "sse-inventory"
}

variable "lambda_s3_bucket_arn" {}

variable "aws_sandstorm_accounts" {
  type        = "list"
  description = "aws account id of the sandstorm apiserver user"
}

variable "aws_stormwatch_account" {
  description = "aws account id that stormwatch is running in"
}

variable "heartbeats_read_capacity" {
  description = "read capacity allocated for the heartbeats table"
  default     = 10
}

variable "whitelisted_sns_arns" {
  type = "list"
}

variable "heartbeats_write_capacity" {
  description = "write capacity allocated for the heartbeats table"
  default     = 10
}

output "stormwatch_role_arn" {
  value = "${aws_iam_role.stormwatch.arn}"
}

output "lambda_deploy_role_arn" {
  value = "${aws_iam_role.lambda-deploy.arn}"
}
