provider "aws" {
  region              = "${var.region}"
  profile             = "${var.aws_profile}"
  allowed_account_ids = ["${var.account_id}"]
}

output "inventory-gateway-api" {
  value = "${aws_api_gateway_deployment.inventory.invoke_url}"
}

output "inventory-gateway-api-lambda-execution-arn" {
  value = "${aws_api_gateway_deployment.inventory.execution_arn}"
}

output "inventory-gateway-api-id" {
  value = "${aws_api_gateway_deployment.inventory.id}"
}

module "heartbeats-v2" {
  source = "./heartbeats-v2"

  env                  = "${var.env}"
  account_id           = "${var.account_id}"
  aws_profile          = "${var.aws_profile}"
  lambda_s3_bucket     = "${var.lambda_s3_bucket}"
  read_capacity        = "${var.heartbeats_read_capacity}"
  write_capacity       = "${var.heartbeats_write_capacity}"
  whitelisted_sns_arns = "${var.whitelisted_sns_arns}"
  region               = "${var.region}"
}
