# API Gateway
resource "aws_api_gateway_rest_api" "api" {
  name        = "inventory${var.env_suffix}"
  description = "This api provides gateway for services not running on twitch network which need access to sse-inventory"
}

data "template_file" "request_template" {
  template = "${file("${path.module}/integration_request_template")}"

  vars {
    env_suffix = "${var.env_suffix}"
  }
}

# PUT /heartbeat
resource "aws_api_gateway_resource" "heartBeat" {
  rest_api_id = "${aws_api_gateway_rest_api.api.id}"
  parent_id   = "${aws_api_gateway_rest_api.api.root_resource_id}"
  path_part   = "heartbeat"
}

resource "aws_api_gateway_method" "heartBeatMethod" {
  depends_on = [
    "aws_api_gateway_resource.heartBeat",
  ]

  rest_api_id      = "${aws_api_gateway_rest_api.api.id}"
  resource_id      = "${aws_api_gateway_resource.heartBeat.id}"
  http_method      = "PUT"
  authorization    = "AWS_IAM"
  api_key_required = false
}

resource "aws_api_gateway_integration" "heartBeatIntegration" {
  rest_api_id             = "${aws_api_gateway_rest_api.api.id}"
  resource_id             = "${aws_api_gateway_resource.heartBeat.id}"
  http_method             = "${aws_api_gateway_method.heartBeatMethod.http_method}"
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = "arn:aws:apigateway:${var.region}:lambda:path/2015-03-31/functions/${aws_lambda_function.put_heartbeat.arn}/invocations"

  request_templates = {
    "application/json" = "${data.template_file.request_template.rendered}"
  }

  passthrough_behavior = "WHEN_NO_MATCH"
}

resource "aws_api_gateway_method_response" "heartBeat200" {
  depends_on = [
    "aws_api_gateway_method.heartBeatMethod",
  ]

  rest_api_id = "${aws_api_gateway_rest_api.api.id}"
  resource_id = "${aws_api_gateway_resource.heartBeat.id}"
  http_method = "${aws_api_gateway_method.heartBeatMethod.http_method}"
  status_code = "200"
}

resource "aws_api_gateway_integration_response" "heartBeatIntegrationResponse" {
  depends_on = [
    "aws_api_gateway_integration.heartBeatIntegration",
  ]

  rest_api_id = "${aws_api_gateway_rest_api.api.id}"
  resource_id = "${aws_api_gateway_resource.heartBeat.id}"
  http_method = "${aws_api_gateway_method.heartBeatMethod.http_method}"
  status_code = "${aws_api_gateway_method_response.heartBeat200.status_code}"
}

resource "aws_api_gateway_method_settings" "heartBeat" {
  rest_api_id = "${aws_api_gateway_rest_api.api.id}"
  stage_name  = "${aws_api_gateway_deployment.inventory.stage_name}"
  method_path = "${aws_api_gateway_resource.heartBeat.path_part}/${aws_api_gateway_method.heartBeatMethod.http_method}"

  settings {
    metrics_enabled = true
    logging_level   = "INFO"
  }
}

resource "aws_api_gateway_deployment" "inventory" {
  depends_on = [
    "aws_api_gateway_rest_api.api",
    "aws_api_gateway_resource.heartBeat",
    "aws_api_gateway_method.heartBeatMethod",
    "aws_api_gateway_integration.heartBeatIntegration",
    "aws_api_gateway_integration_response.heartBeatIntegrationResponse",
  ]

  rest_api_id = "${aws_api_gateway_rest_api.api.id}"
  stage_name  = "${var.env}"
  description = "Deployment v1"
}
