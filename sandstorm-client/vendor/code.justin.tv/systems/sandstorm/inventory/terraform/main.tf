provider "aws" {
  version = "~> 1.0.0"
}

variable "aws_sandstorm_accounts" {
  type = "list"

  default = [
    "734326455073",
  ]
}

variable "aws_stormwatch_dev_account" {
  default = "505345203727"
}

variable "aws_stormwatch_prod_account" {
  default = "584671007337"
}

module "artifacts" {
  source      = "./artifacts"
  account_id  = "516651178292"
  aws_profile = "twitch-sse-dev"
  aws_region  = "us-west-2"

  aws_allowed_account_ids = [
    "516651178292",
  ]
}

module "account_dev" {
  source = "./accounts"

  aws_region  = "us-west-2"
  aws_profile = "twitch-sse-dev"

  aws_allowed_account_ids = [
    "516651178292",
  ]
}

module "account_prod" {
  source = "./accounts"

  aws_region  = "us-west-2"
  aws_profile = "twitch-sse-prod"

  aws_allowed_account_ids = [
    "854594403332",
  ]
}

module "testing" {
  source = "./modules"

  region                 = "us-west-2"
  account_id             = "516651178292"
  aws_profile            = "twitch-sse-dev"
  deploy_account         = "516651178292"
  env                    = "testing"
  env_suffix             = "-testing"
  cloudwatch_role_name   = "${module.account_dev.cloudwatch_iam_role_name}"
  lambda_s3_bucket       = "${module.artifacts.bucket}"
  lambda_s3_bucket_arn   = "${module.artifacts.bucket_arn}"
  aws_sandstorm_accounts = ["734326455073", "505345203727"]
  aws_stormwatch_account = "${var.aws_stormwatch_dev_account}"

  whitelisted_sns_arns = [
    // twitch-s2s-dev
    "arn:aws:iam::289423984879:root",
  ]
}

module "staging" {
  source = "./modules"

  region                 = "us-west-2"
  account_id             = "516651178292"
  aws_profile            = "twitch-sse-dev"
  deploy_account         = "516651178292"
  env                    = "staging"
  env_suffix             = "-staging"
  cloudwatch_role_name   = "${module.account_dev.cloudwatch_iam_role_name}"
  lambda_s3_bucket       = "${module.artifacts.bucket}"
  lambda_s3_bucket_arn   = "${module.artifacts.bucket_arn}"
  aws_sandstorm_accounts = ["734326455073", "505345203727"]
  aws_stormwatch_account = "${var.aws_stormwatch_dev_account}"

  whitelisted_sns_arns = [
    // twitch-s2s-dev
    "arn:aws:iam::289423984879:root",
  ]
}

module "production" {
  source = "./modules"

  region                    = "us-west-2"
  account_id                = "854594403332"
  aws_profile               = "twitch-sse-prod"
  deploy_account            = "516651178292"
  env                       = "production"
  env_suffix                = "-production"
  cloudwatch_role_name      = "${module.account_prod.cloudwatch_iam_role_name}"
  heartbeats_write_capacity = 1000
  lambda_s3_bucket          = "${module.artifacts.bucket}"
  lambda_s3_bucket_arn      = "${module.artifacts.bucket_arn}"
  aws_sandstorm_accounts    = "${var.aws_sandstorm_accounts}"
  aws_stormwatch_account    = "${var.aws_stormwatch_prod_account}"

  whitelisted_sns_arns = [
    // twitch-s2s-prod
    "arn:aws:iam::180116294062:root",
  ]
}

// TODO: delete this module - role is assumed directly from the stormwatch
// account now
module "stormwatch-production" {
  source = "./stormwatch"

  aws_region         = "us-west-2"
  aws_account_id     = "734326455073"
  aws_profile        = "twitch-sandstorm-infra-aws"
  env_suffix         = "-production"
  stormwatch_role    = "sandstorm-stormwatch-production"
  inventory_role_arn = "${module.production.stormwatch_role_arn}"
}

module "deployment" {
  source = "./deployment"

  aws_region            = "us-west-2"
  aws_account_id        = "516651178292"
  aws_profile           = "twitch-sse-dev"
  trusted_account_ids   = "${var.aws_sandstorm_accounts}"
  deployment_bucket_arn = "${module.artifacts.bucket_arn}"

  deployment_roles = [
    "${module.testing.lambda_deploy_role_arn}",
    "${module.staging.lambda_deploy_role_arn}",
    "${module.production.lambda_deploy_role_arn}",
  ]
}
