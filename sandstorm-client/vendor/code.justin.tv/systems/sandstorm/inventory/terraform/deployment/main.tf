variable "aws_account_id" {}
variable "aws_profile" {}
variable "aws_region" {}

variable "trusted_account_ids" {
  type = "list"
}

variable "deployment_bucket_arn" {}

variable "deployment_roles" {
  type = "list"
}
