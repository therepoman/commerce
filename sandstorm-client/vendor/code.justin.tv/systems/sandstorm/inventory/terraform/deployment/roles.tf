resource "aws_iam_role" "lambda-deploy" {
  name               = "inventory-lambda-artifact-deploy"
  assume_role_policy = "${data.aws_iam_policy_document.lambda-deploy-arp.json}"
}

// allow sandstorm account and sse account to assume any role
data "aws_iam_policy_document" "lambda-deploy-arp" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "AWS"

      identifiers = [
        "${formatlist(
          "arn:aws:iam::%s:root",
          concat(
            var.trusted_account_ids,
            list(var.aws_account_id),
          ),
        )}",
        "arn:aws:iam::516651178292:user/jenkins",
      ]
    }
  }
}

resource "aws_iam_role_policy" "lambda-deploy" {
  name   = "lambda-deploy"
  role   = "${aws_iam_role.lambda-deploy.id}"
  policy = "${data.aws_iam_policy_document.lambda-deploy.json}"
}

data "aws_iam_policy_document" "lambda-deploy" {
  statement {
    actions = [
      "s3:PutObject",
      "s3:GetObject",
    ]

    resources = [
      "${var.deployment_bucket_arn}/lambdas/*.zip",
    ]
  }

  statement {
    actions = [
      "s3:ListBucket",
    ]

    resources = [
      "${var.deployment_bucket_arn}",
    ]
  }

  statement {
    actions = [
      "sts:AssumeRole",
    ]

    resources = [
      "${var.deployment_roles}",
    ]
  }
}
