// create a cloudwatch role for lambda to assume and
// set as role for apigateway

variable "aws_region" {}

variable "aws_profile" {}

variable "aws_allowed_account_ids" {
  type = "list"
}

resource "aws_iam_role" "cloudwatch" {
  name = "inventory-gateway-lambda"
  path = "/"

  assume_role_policy = <<ASSUME_ROLE
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
ASSUME_ROLE
}

resource "aws_iam_role_policy" "cloudwatch" {
  name = "cloudwatch_log"
  role = "${aws_iam_role.cloudwatch.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:DescribeLogGroups",
                "logs:DescribeLogStreams",
                "logs:PutLogEvents",
                "logs:GetLogEvents",
                "logs:FilterLogEvents"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_api_gateway_account" "inventory" {
  cloudwatch_role_arn = "${aws_iam_role.cloudwatch.arn}"
}

output "cloudwatch_iam_role_name" {
  value = "${aws_iam_role.cloudwatch.name}"
}
