/** stormwatch

give stormwatch the ability to assume the inventory role that gives read access
to the heartbeats table.
**/

variable "env_suffix" {}

variable "stormwatch_role" {
  description = "role that stormwatch process assumes"
}

variable "inventory_role_arn" {
  description = "role that stormwatch needs access to for inventory table read"
}

variable "aws_region" {}
variable "aws_profile" {}
variable "aws_account_id" {}

provider "aws" {
  region              = "${var.aws_region}"
  profile             = "${var.aws_profile}"
  allowed_account_ids = ["${var.aws_account_id}"]
}

resource "aws_iam_role_policy" "stormwatch_inventory_read" {
  name = "stormwatch-inventory-read${var.env_suffix}"
  role = "${var.stormwatch_role}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "sts:AssumeRole"
      ],
      "Effect": "Allow",
      "Resource": [
        "${var.inventory_role_arn}"
      ]
    }
  ]
}
POLICY
}
