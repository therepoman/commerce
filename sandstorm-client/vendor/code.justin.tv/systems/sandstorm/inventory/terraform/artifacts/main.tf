// create permissions for read and write access to artifacts

variable "bucket" {
  default = "sse-inventory"
}

variable "bucket_arn" {
  default = "arn:aws:s3:::sse-inventory"
}

variable "account_id" {}
variable "aws_profile" {}
variable "aws_region" {}

variable "aws_allowed_account_ids" {
  type = "list"
}

// allow all accounts to get from the artifacts bucket key
resource "aws_s3_bucket_policy" "artifacts" {
  bucket = "sse-inventory"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:ListBucket"
      ],
      "Effect": "Allow",
      "Resource": [
        "${var.bucket_arn}"
      ],
      "Principal": {
        "AWS": [
          "arn:aws:iam::854594403332:root",
          "arn:aws:iam::516651178292:root"
        ]
      }
    },
    {
      "Action": [
        "s3:GetObject"
      ],
      "Effect": "Allow",
      "Resource": [
        "${var.bucket_arn}/*.zip"
      ],
      "Principal": {
        "AWS": [
          "arn:aws:iam::854594403332:root",
          "arn:aws:iam::516651178292:root"
        ]
      }
    }
  ]
}
EOF
}

output "bucket" {
  value = "${var.bucket}"
}

output "bucket_arn" {
  value = "${var.bucket_arn}"
}
