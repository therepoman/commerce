provider "aws" {
  alias               = "dev"
  region              = "us-west-2"
  profile             = "twitch-sse-dev"
  allowed_account_ids = ["516651178292"]
}

provider "aws" {
  alias               = "prod"
  region              = "us-west-2"
  profile             = "twitch-sse-prod"
  allowed_account_ids = ["854594403332"]
}
