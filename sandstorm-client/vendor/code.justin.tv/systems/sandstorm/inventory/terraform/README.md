# Inventory Terraform Config

## Modules

### Deployment

This creates the roles needed for deployment. Attachment is done in the base
terraform config at `./terraform`. This creates a role in the sandstorm-dev
account:

```
arn:aws:iam::516651178292:role/inventory-lambda-artifact-deploy
```

This role can assume three other roles for deployment to update the lambda
functions.

```
arn:aws:iam::516651178292:role/inventory-lambda-deploy-testing
arn:aws:iam::516651178292:role/inventory-lambda-deploy-staging
arn:aws:iam::854594403332:role/inventory-lambda-deploy-production
```
