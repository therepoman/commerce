package main

import "C"
import (
	"encoding/json"
	"errors"
	"fmt"

	"code.justin.tv/systems/sandstorm/manager"
)

var (
	managerClient *manager.Manager
	cfg           = &configurer{make(map[configurerKey]manager.API)}
)

var (
	errSecretDoesNotExist       = errors.New("SecretDoesNotExist")
	errManagerAlreadyConfigured = errors.New("manager_init has already been called")
	errManagerNotConfigured     = errors.New("must call manager_init")
)

type managerConfigurer interface {
	GetEnvironment() string
	GetRegion() string
	GetRoleARN() string
	GetAccessKeyID() string
	GetSecretAccessKey() string
	GetSecurityToken() string
	GetSessionToken() string
}

type baseInput struct {
	Environment     string
	Region          string
	RoleARN         string
	AccessKeyID     string
	SecretAccessKey string
	SecurityToken   string
	SessionToken    string
}

func (i *baseInput) GetEnvironment() string {
	return i.Environment
}

func (i *baseInput) GetRegion() string {
	if i.Region == "" {
		return "us-west-2"
	}
	return i.Region
}

func (i *baseInput) GetRoleARN() string {
	return i.RoleARN
}

func (i *baseInput) GetAccessKeyID() string {
	return i.AccessKeyID
}

func (i *baseInput) GetSecretAccessKey() string {
	return i.SecretAccessKey
}

func (i *baseInput) GetSecurityToken() string {
	return i.SecurityToken
}

func (i *baseInput) GetSessionToken() string {
	return i.SessionToken
}

// do is the call signature exported functions should implement, they return two
// strings.
// - outputValue: a JSON unmarshalable struct
// - errorValue: error string
func do(rawInput string, input interface{}, fn func() (interface{}, error)) (outputValue, errorValue *C.char) {
	if err := json.Unmarshal([]byte(rawInput), &input); err != nil {
		return nil, C.CString(fmt.Sprintf("error parsing input: %s", err.Error()))
	}

	output, err := fn()
	if err != nil {
		return nil, C.CString(err.Error())
	}

	outputRaw, err := json.Marshal(output)
	if err != nil {
		return nil, C.CString(err.Error())
	}

	return C.CString(string(outputRaw)), nil
}

func withManager(rawInput string, input managerConfigurer, fn func(m manager.API) (interface{}, error)) (*C.char, *C.char) {
	return do(rawInput, input, func() (interface{}, error) {
		return cfg.WithManager(input, fn)
	})
}

// create_secret creates a secret
//export create_secret
func create_secret(rawInput string) (*C.char, *C.char) {
	input := struct {
		baseInput
		Name      string
		Plaintext string
		Class     string
	}{}

	return withManager(rawInput, &input, func(m manager.API) (interface{}, error) {
		cls, err := manager.ParseSecretClass(input.Class)
		if err != nil {
			return nil, err
		}

		return struct{}{}, m.Post(&manager.Secret{
			Name:      input.Name,
			Plaintext: []byte(input.Plaintext),
			Class:     cls,
		})
	})
}

// get_secret gets a secret value
//export get_secret
func get_secret(rawInput string) (*C.char, *C.char) {
	input := struct {
		baseInput
		Name string
	}{}

	return withManager(rawInput, &input, func(m manager.API) (interface{}, error) {
		secret, err := m.Get(input.Name)
		if err != nil {
			return nil, err
		}

		if secret == nil || secret.Plaintext == nil {
			return nil, errSecretDoesNotExist
		}

		return struct {
			Name      string
			Plaintext string
			UpdatedAt int64
		}{secret.Name, string(secret.Plaintext), secret.UpdatedAt}, nil
	})
}

// update_secret_plaintext puts a secret value
//export update_secret_plaintext
func update_secret_plaintext(rawInput string) (*C.char, *C.char) {
	input := struct {
		baseInput
		Name      string
		Plaintext string
	}{}

	return withManager(rawInput, &input, func(m manager.API) (interface{}, error) {
		return struct{}{}, m.Patch(&manager.PatchInput{
			Name:      input.Name,
			Plaintext: []byte(input.Plaintext),
		})
	})
}

// delete_secret deletes a secret
//export delete_secret
func delete_secret(rawInput string) (*C.char, *C.char) {
	input := struct {
		baseInput
		Name string
	}{}

	return withManager(rawInput, &input, func(m manager.API) (interface{}, error) {
		return struct{}{}, m.Delete(input.Name)
	})
}

func main() {
}
