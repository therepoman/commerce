package main

import (
	"os"

	"code.justin.tv/systems/sandstorm/manager"
	"code.justin.tv/systems/sandstorm/resource"
)

type configurerKey struct {
	Environment string
	Region      string
	RoleARN     string
}

type configurer struct {
	clients map[configurerKey]manager.API
}

func (c *configurer) awsConfigurerForRegion(region string) resource.AWSConfigurer {
	return resource.AWSConfigurer{Region: region}
}

func (c *configurer) getManager(cfg managerConfigurer) manager.API {
	key := configurerKey{
		Environment: cfg.GetEnvironment(),
		Region:      cfg.GetRegion(),
		RoleARN:     cfg.GetRoleARN(),
	}

	if m, ok := c.clients[key]; ok {
		return m
	}

	m := manager.New(manager.Config{
		Environment: cfg.GetEnvironment(),
		AWSConfig: c.awsConfigurerForRegion(cfg.GetRegion()).
			AWSConfig(nil, cfg.GetRoleARN()),
	})

	c.clients[key] = m
	return m
}

func (c *configurer) updateEnvironment(cfg managerConfigurer) error {
	for _, EnvSetting := range []struct {
		Key   string
		Value string
	}{
		{"AWS_ACCESS_KEY_ID", cfg.GetAccessKeyID()},
		{"AWS_SECRET_ACCESS_KEY", cfg.GetSecretAccessKey()},
		{"AWS_SECURITY_TOKEN", cfg.GetSecurityToken()},
		{"AWS_SESSION_TOKEN", cfg.GetSessionToken()},
	} {
		if len(EnvSetting.Value) > 0 {
			if err := os.Setenv(EnvSetting.Key, EnvSetting.Value); err != nil {
				return err
			}
		}
	}
	return nil
}

func (c *configurer) WithManager(
	cfg managerConfigurer,
	fn func(m manager.API) (interface{}, error),
) (interface{}, error) {
	if err := c.updateEnvironment(cfg); err != nil {
		return nil, err
	}
	return fn(c.getManager(cfg))
}
