# Sandstorm Clients

All clients utilize `libsandstorm.so` which is generated from `./libsandstorm/`
using [build mode c-shared]. See the `//export` commented methods in that
package for available methods. The packages built for linux include this library
in all packages.

[build mode c-shared]: https://golang.org/doc/go1.5#link

## Installing libsandstorm on osx.

```bash
brew tap twitch/internal git@git-aws.internal.justin.tv:common/homebrew-custom.git
brew update
brew install libsandstorm
```
