package testutil

import (
	"fmt"
	"testing"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/require"
)

// GetRandomSecretName gives a random secret for usage in tests.
// Please delete after test.
func GetRandomSecretName(t *testing.T) string {
	return GetRandomSecretNameWithPrefix(t, "system/testService/testing")
}

// GetRandomSecretNameWithPrefix gives a random secret for usage in tests.
// Please delete after test.
func GetRandomSecretNameWithPrefix(t *testing.T, prefix string) string {
	uid, err := uuid.NewV4()
	require.NoError(t, err)
	return fmt.Sprintf("%s/%s", prefix, uid.String())
}

// GetRandomGroupName gives a random group name for usage in tests.
// Please delete after test.
func GetRandomGroupName(t *testing.T) string {
	uid, err := uuid.NewV4()
	require.NoError(t, err)
	return fmt.Sprintf("sandstorm-api-test-group-%s", uid.String())
}
