package cryptorand

import (
	"crypto/rand"
	"math/big"
)

// permute runs a permutation of an array with an implementation
// of the Knuth shuffle: https://en.wikipedia.org/wiki/Random_permutation
func permute(items []byte) error {
	length := int64(len(items))
	for i := int64(0); i < length-2; i++ {
		bigJ, err := rand.Int(rand.Reader, big.NewInt(length-i))
		j := i + bigJ.Int64()
		if err != nil {
			return err
		}
		items[i], items[j] = items[j], items[i]
	}
	return nil
}

// GeneratePlaintextRequest is used for GeneratePlaintext
type GeneratePlaintextRequest struct {
	IncludeAlphaLowercase bool `json:"include_alpha_lowercase"`
	IncludeAlphaUppercase bool `json:"include_alpha_uppercase"`
	IncludeDigits         bool `json:"include_digits"`
	IncludeSymbols        bool `json:"include_symbols"`
	Length                int  `json:"length"`
	validated             bool
}

// Validate returns error if there is any issue with the request
func (g GeneratePlaintextRequest) Validate() error {
	if g.validated {
		return nil
	}
	if g.Length < 1 {
		return ErrNonPositiveLengthRequest
	}
	if g.Length < g.requiredCapacity() {
		return ErrNotEnoughCapacity
	}
	if !(g.IncludeAlphaLowercase ||
		g.IncludeAlphaUppercase ||
		g.IncludeDigits ||
		g.IncludeSymbols) {
		return ErrNoCharacterSetsSelected
	}
	g.validated = true
	return nil
}

func (g GeneratePlaintextRequest) requiredCapacity() int {
	allRequirements := []bool{
		g.IncludeAlphaLowercase,
		g.IncludeAlphaUppercase,
		g.IncludeDigits,
		g.IncludeSymbols,
	}
	capReq := 0
	for _, req := range allRequirements {
		if req {
			capReq++
		}
	}
	return capReq
}

func (g GeneratePlaintextRequest) getByteSets() ([]byteSet, error) {
	byteSets := []byteSet{}

	if g.IncludeAlphaUppercase {
		byteSet, err := newByteSet([]byte(ByteSetAlphaUpper))
		if err != nil {
			return nil, err
		}
		byteSets = append(byteSets, *byteSet)
	}
	if g.IncludeAlphaLowercase {
		byteSet, err := newByteSet([]byte(ByteSetAlphaLower))
		if err != nil {
			return nil, err
		}
		byteSets = append(byteSets, *byteSet)
	}
	if g.IncludeDigits {
		byteSet, err := newByteSet([]byte(ByteSetDigits))
		if err != nil {
			return nil, err
		}
		byteSets = append(byteSets, *byteSet)
	}
	if g.IncludeSymbols {
		byteSet, err := newByteSet([]byte(ByteSetSymbols))
		if err != nil {
			return nil, err
		}
		byteSets = append(byteSets, *byteSet)
	}
	return byteSets, nil
}

// GeneratePlaintext returns a random plaintext secret
func GeneratePlaintext(r *GeneratePlaintextRequest) ([]byte, error) {
	if err := r.Validate(); err != nil {
		return nil, err
	}

	plaintext, err := generatePlaintextBytes(r)
	if err != nil {
		return nil, err
	}

	if err := permute(plaintext); err != nil {
		return nil, err
	}

	return plaintext, nil
}

func generatePlaintextBytes(r *GeneratePlaintextRequest) ([]byte, error) {
	plaintext := make([]byte, 0, r.Length)
	byteSets, err := r.getByteSets()
	if err != nil {
		return nil, err
	}

	for _, byteSet := range byteSets {
		b, err := byteSet.RandomByte()
		if err != nil {
			return nil, err
		}
		plaintext = append(plaintext, b)
	}

	bytesLeft := cap(plaintext) - len(plaintext)
	if bytesLeft > 0 {
		byteSet, err := compositeByteSet(byteSets)
		if err != nil {
			return nil, err
		}
		for i := 0; i < bytesLeft; i++ {
			b, err := byteSet.RandomByte()
			if err != nil {
				return nil, err
			}
			plaintext = append(plaintext, b)
		}
	}

	return plaintext, nil
}
