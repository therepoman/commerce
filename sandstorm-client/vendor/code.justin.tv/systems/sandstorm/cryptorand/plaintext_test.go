package cryptorand

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPermute(t *testing.T) {
	assert := assert.New(t)

	t.Run("permute", func(t *testing.T) {
		t.Run("should succeed with len 0", func(t *testing.T) {
			t.Parallel()
			items := []byte{}
			for n := 0; n < 1000; n++ {
				assert.Nil(permute(items))
				assert.Equal([]byte{}, items)
			}
		})

		t.Run("should succeed with len 1", func(t *testing.T) {
			t.Parallel()
			items := []byte{0x4}
			for n := 0; n < 1000; n++ {
				assert.Nil(permute(items))
				assert.Equal([]byte{0x4}, items)
			}
		})

		t.Run("should succeed with len 2", func(t *testing.T) {
			t.Parallel()
			items := []byte{0x4, 0x2}

			for n := 0; n < 1000; n++ {
				assert.Nil(permute(items))
				assert.Contains(items, byte(0x2))
				assert.Contains(items, byte(0x4))
			}
		})

		t.Run("should succeed with len 100", func(t *testing.T) {
			t.Parallel()
			items := make([]byte, 100)
			for n := 0; n < 100; n++ {
				items[n] = byte(n)
			}

			original := make([]byte, 100)
			copy(original, items)

			for n := 0; n < 1000; n++ {
				assert.Nil(permute(items))
				for _, val := range items {
					assert.Contains(original, val)
				}
			}
		})
	})
}

func TestGeneratePlaintextRequest(t *testing.T) {
	assert := assert.New(t)

	t.Run("GeneratePlaintextRequest.Validate", func(t *testing.T) {
		t.Run("cap not high enough for number of bytes sets", func(t *testing.T) {
			t.Parallel()
			r := GeneratePlaintextRequest{
				IncludeAlphaLowercase: true,
				IncludeAlphaUppercase: true,
				IncludeDigits:         true,
				Length:                2,
			}
			err := r.Validate()
			assert.Equal(ErrNotEnoughCapacity, err)
		})

		t.Run("no character sets chosen", func(t *testing.T) {
			t.Parallel()
			r := GeneratePlaintextRequest{
				Length: 400,
			}
			err := r.Validate()
			assert.Equal(ErrNoCharacterSetsSelected, err)
		})

		t.Run("negative length", func(t *testing.T) {
			t.Parallel()
			r := GeneratePlaintextRequest{
				Length: -1,
			}
			err := r.Validate()
			assert.Equal(ErrNonPositiveLengthRequest, err)
		})
	})
}

func TestGeneratePlaintext(t *testing.T) {
	assert := assert.New(t)

	t.Run("GeneratePlaintext", func(t *testing.T) {
		t.Run("test lowercase", func(t *testing.T) {
			t.Parallel()
			for i := 0; i < 1000; i++ {
				plaintext, err := GeneratePlaintext(&GeneratePlaintextRequest{
					IncludeAlphaLowercase: true,
					Length:                20,
				})

				assert.Nil(err)
				assert.Equal(20, len(plaintext))

				for _, b := range plaintext {
					assert.Contains([]byte("abcdefghijklmnopqrstuvwxyz"), b)
				}
			}
		})

		t.Run("test uppercase", func(t *testing.T) {
			t.Parallel()
			for i := 0; i < 1000; i++ {
				plaintext, err := GeneratePlaintext(&GeneratePlaintextRequest{
					IncludeAlphaUppercase: true,
					Length:                20,
				})

				assert.Nil(err)
				assert.Equal(20, len(plaintext))

				for _, b := range plaintext {
					assert.Contains([]byte("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), b)
				}
			}
		})

		t.Run("test digits", func(t *testing.T) {
			t.Parallel()
			for i := 0; i < 1000; i++ {
				plaintext, err := GeneratePlaintext(&GeneratePlaintextRequest{
					IncludeDigits: true,
					Length:        20,
				})

				assert.Nil(err)
				assert.Equal(20, len(plaintext))

				for _, b := range plaintext {
					assert.Contains([]byte("0123456789"), b)
				}
			}
		})

		t.Run("test symbols", func(t *testing.T) {
			t.Parallel()
			for i := 0; i < 1000; i++ {
				plaintext, err := GeneratePlaintext(&GeneratePlaintextRequest{
					IncludeSymbols: true,
					Length:         20,
				})

				assert.Nil(err)
				assert.Equal(20, len(plaintext))

				for _, b := range plaintext {
					assert.Contains([]byte("!#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"), b)
				}
			}
		})

		t.Run("test combined", func(t *testing.T) {
			t.Parallel()
			for i := 0; i < 1000; i++ {
				plaintext, err := GeneratePlaintext(&GeneratePlaintextRequest{
					IncludeAlphaLowercase: true,
					IncludeDigits:         true,
					Length:                20,
				})

				assert.Nil(err)
				assert.Equal(20, len(plaintext))

				lower := "abcdefghijklmnopqrstuvwxyz"
				digits := "0123456789"
				counts := map[string]int{}
				counts[lower] = 0
				counts[digits] = 0

				for _, b := range plaintext {
					for byteSet := range counts {
						for _, v := range []byte(byteSet) {
							if b == v {
								counts[byteSet]++
								break
							}
						}
					}
				}
				for byteSet, count := range counts {
					assert.True(0 < count, fmt.Sprintf("expected at least 1 %s", byteSet))
				}
				assert.Equal(20, counts[lower]+counts[digits])
			}
		})
	})
}
