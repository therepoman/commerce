package cryptorand

import (
	"errors"
)

var (
	// ErrNonPositiveLengthRequest is returned when a non-positive length is requested
	ErrNonPositiveLengthRequest = errors.New("length request is non-positive")
	// ErrNotEnoughCapacity is returned when a length is not big enough to handle requiremenst
	ErrNotEnoughCapacity = errors.New("not enough capacity to handle generate request")
	// ErrNoCharacterSetsSelected is returned when no character sets have been selected
	ErrNoCharacterSetsSelected = errors.New("no character sets have been selected")
)
