package cryptorand

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"sort"
)

// ByteSet represents a subset of 1 byte combinations
type ByteSet interface {
	RandomByte() (byte, error)
}

// NewByteSet creates a new ByteSet
func NewByteSet(bytes []byte) (ByteSet, error) {
	return newByteSet(bytes)
}

func newByteSet(bytes []byte) (*byteSet, error) {
	byteSet := byteSet{
		bytes: bytes,
	}
	sort.Sort(byteSet)
	if err := byteSet.validate(); err != nil {
		return nil, err
	}
	byteSet.length = big.NewInt(int64(len(byteSet.bytes)))
	return &byteSet, nil
}

// compositeByteSet will combine several byte sets. this will error out
// if overlapping byte sets are sent
func compositeByteSet(byteSets []byteSet) (*byteSet, error) {
	if len(byteSets) == 0 {
		return nil, fmt.Errorf("cannot combine zero byte sets")
	}
	newBytes := []byte{}
	for _, byteSet := range byteSets {
		for _, b := range byteSet.bytes {
			newBytes = append(newBytes, b)
		}
	}

	return newByteSet(newBytes)
}

type byteSet struct {
	sort.Interface

	// array of bytes in this byteSet SORTED
	bytes []byte
	// length used as argument to rand.Int
	length *big.Int
}

func (b byteSet) RandomByte() (byte, error) {
	nByte, err := rand.Int(rand.Reader, b.length)
	if err != nil {
		return 0x00, err
	}
	return b.bytes[nByte.Int64()], nil
}

// Len impl for sort
func (b byteSet) Len() int {
	return len(b.bytes)
}

// Less impl for sort
func (b byteSet) Less(i, j int) bool {
	return b.bytes[i] < b.bytes[j]
}

// Swap impl for sort
func (b byteSet) Swap(i, j int) {
	tmp := b.bytes[i]
	b.bytes[i] = b.bytes[j]
	b.bytes[j] = tmp
}

func (b byteSet) equals(other *byteSet) bool {
	if other == nil {
		return false
	}

	if len(b.bytes) != len(other.bytes) {
		return false
	}

	for pos, val := range b.bytes {
		if val != other.bytes[pos] {
			return false
		}
	}

	return true
}

func (b *byteSet) validate() error {
	if err := b.validateDupes(); err != nil {
		return err
	}

	if len(b.bytes) == 0 {
		return fmt.Errorf("must be at least 1 byte in the byte set")
	}
	return nil
}

// validateByteDupes
// assumes already sorted (see NewByteSet)
func (b *byteSet) validateDupes() error {
	var last byte
	for n, b := range b.bytes {
		if n > 0 && last == b {
			return fmt.Errorf("duplicate byte in character set")
		}
		last = b
	}
	return nil
}
