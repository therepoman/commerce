package cryptorand

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestByteSet(t *testing.T) {
	assert := assert.New(t)

	t.Run("ByteSet.validate", func(t *testing.T) {
		t.Run("should error with len 0 byte set", func(t *testing.T) {
			val, err := NewByteSet([]byte{})
			assert.Nil(val)
			assert.NotNil(err)
		})

		t.Run("should error with duplicates", func(t *testing.T) {
			val, err := NewByteSet([]byte{0xb, 0xa, 0xb})
			assert.Nil(val)
			assert.NotNil(err)
		})

		t.Run("should be ok with no duplicates", func(t *testing.T) {
			val, err := NewByteSet([]byte{0xb, 0xa, 0xc})
			assert.NotNil(val)
			assert.Nil(err)
		})

		t.Run("should be ok with single 0 byte", func(t *testing.T) {
			val, err := NewByteSet([]byte{0x0})
			assert.NotNil(val)
			assert.Nil(err)
		})
	})

	t.Run("ByteSet.RandomByte", func(t *testing.T) {
		t.Run("should be fine with a single byte", func(t *testing.T) {
			byteValue := byte(0x4)

			byteSet, err := NewByteSet([]byte{byteValue})
			assert.NotNil(byteSet)
			assert.Nil(err)

			for i := 0; i < 1000; i++ {
				b, err := byteSet.RandomByte()
				assert.Equal(byte(byteValue), b)
				assert.Nil(err)
			}
		})

		t.Run("should write fine with a several bytes", func(t *testing.T) {
			byteValues := []byte{0x41, 0x40, 0x42, 0x87}

			byteSet, err := NewByteSet(byteValues)
			assert.NotNil(byteSet)
			assert.Nil(err)

			for i := 0; i < 1000; i++ {
				b, err := byteSet.RandomByte()
				assert.Contains(byteValues, b)
				assert.Nil(err)
			}
		})
	})
}
