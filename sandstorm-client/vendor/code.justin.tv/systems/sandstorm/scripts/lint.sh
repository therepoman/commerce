#!/bin/sh
set -e

echo "terraform lint: starting"
TF_UNLINTED=$(
docker run --rm \
  -v $(pwd):/target \
  hashicorp/terraform \
  fmt -list=true -write=false /target/
)
echo "$TF_UNLINTED"
[ -z "$TF_UNLINTED" ] && echo "terraform lint: ok"
