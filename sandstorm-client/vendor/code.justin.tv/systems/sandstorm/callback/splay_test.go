package callback

import (
	"testing"
	"time"

	"code.justin.tv/systems/sandstorm/manager"
	"code.justin.tv/systems/sandstorm/pkg/secret"
	"github.com/stretchr/testify/assert"
)

func TestSplay(t *testing.T) {
	t.Run("inner function", func(t *testing.T) {
		splayer := &Splayer{
			Duration: 1 * time.Millisecond,
		}
		called := false
		cbChan := make(chan struct{})
		inner := func(input manager.SecretChangeset) {
			called = true
			cbChan <- struct{}{}
		}
		splayFunc := splayer.Splay(inner)
		splayFunc(&secret.Changeset{})
		<-cbChan
		assert.True(t, called)
	})
}
