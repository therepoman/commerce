package callback

import (
	"math/rand"
	"time"

	"code.justin.tv/systems/sandstorm/closer"
	"code.justin.tv/systems/sandstorm/manager"
)

const (
	defaultSplayDuration = 30 * time.Second
)

// Splayer spreads the function callback over configured Duration
type Splayer struct {
	Duration time.Duration
	Closer   *closer.Closer
}

func (s *Splayer) duration() time.Duration {
	if s.Duration == 0*time.Second {
		s.Duration = defaultSplayDuration
	}
	return s.Duration
}

//Splay add a random sleep time before running the inner function.
func (s *Splayer) Splay(inner func(input manager.SecretChangeset)) func(input manager.SecretChangeset) {

	if s.Closer == nil {
		s.Closer = closer.New()
	}

	notifier := make(chan manager.SecretChangeset, 10)

	splayFn := func(input manager.SecretChangeset) {
		notifier <- input
	}

	go func() {
		for {
			select {
			case cbInput := <-notifier:
				splay := s.duration()
				randSplay := rand.Int63n(int64(splay))
				timer := time.NewTimer(time.Duration(randSplay))
				select {
				case <-timer.C:
					inner(cbInput)
				case <-s.Closer.Done():
					return
				}
			case <-s.Closer.Done():
				return
			}
		}
	}()

	return splayFn
}
