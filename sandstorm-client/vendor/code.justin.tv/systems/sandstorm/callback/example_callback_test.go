package callback_test

import (
	"log"
	"time"

	"code.justin.tv/systems/sandstorm/callback"
	"code.justin.tv/systems/sandstorm/manager"
)

func ExampleDweller_Dwell() {
	cb := &callback.Dweller{}
	m := manager.New(manager.Config{})

	secretCb := func(input manager.SecretChangeset) {
		if input.SecretsHaveChanged() {
			// do things on secret updates
		}
		// rest of things.
	}

	// use dwell for callback
	m.RegisterSecretUpdateCallback("someSecretName", cb.Dwell(secretCb))

	err := m.ListenForUpdates()
	if err != nil {
		// handle error
		log.Fatal(err.Error())
	}
	// Do rest of the things.
}

func ExampleSplayer_Splay() {
	cb := &callback.Splayer{
		Duration: 1 * time.Second,
	}

	m := manager.New(manager.Config{})

	secretCb := func(input manager.SecretChangeset) {
		// some things.
		if input.SecretHasBeenUpdated("someSecretName") {
			// Do something specific to this secret if required.
		}
		// do rest of work.
	}
	// Add splay to callback
	m.RegisterSecretUpdateCallback("someSecretName", cb.Splay(secretCb))

	err := m.ListenForUpdates()
	if err != nil {
		// handle error
		log.Fatal(err.Error())
	}
	// Do rest of the things.

}
