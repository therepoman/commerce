package callback

import (
	"math/rand"
	"testing"
	"time"

	"code.justin.tv/systems/sandstorm/manager"
	"code.justin.tv/systems/sandstorm/pkg/secret"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const (
	dwellUnit     = time.Millisecond
	dwellDuration = 50 * dwellUnit
	splayDuration = 10 * dwellUnit
)

func buildSecretChangeset(secretName string) (us *secret.Changeset) {
	infoMap := make(map[string]int64)
	infoMap[secretName] = 123456
	us = &secret.Changeset{
		SecretInfo: infoMap,
	}
	return
}

func TestDwell(t *testing.T) {
	genID := func(t *testing.T) string {
		id, err := uuid.NewV4()
		require.NoError(t, err)
		return id.String()
	}

	t.Run("Constructor", func(t *testing.T) {
		dw := &Dweller{
			Duration: dwellDuration,
		}
		assert.NotNil(t, dw)
	})

	t.Run("Dwell", func(t *testing.T) {
		t.Run("multiple callbacks", func(t *testing.T) {
			dw := &Dweller{
				Duration: dwellDuration,
			}
			var cb1Count, cb2Count int

			cb1Chan := make(chan struct{})
			cb2Chan := make(chan struct{})
			cb1 := func(input manager.SecretChangeset) {
				cb1Count++
				cb1Chan <- struct{}{}
			}
			cb2 := func(input manager.SecretChangeset) {
				cb2Count++
				cb2Chan <- struct{}{}
			}
			mgrCallback1 := dw.Dwell(cb1)
			mgrCallback2 := dw.Dwell(cb2)

			for i := 0; i < 5; i++ {
				randSplay := rand.Int63n(int64(10))
				time.Sleep(time.Duration(randSplay) * dwellUnit)

				mgrCallback1(buildSecretChangeset("testSecret"))
				mgrCallback2(buildSecretChangeset("testSecret2"))
			}

			<-cb1Chan
			<-cb2Chan

			assert.Equal(t, 1, cb1Count)
			assert.Equal(t, 1, cb2Count)

			mgrCallback1(buildSecretChangeset("testSecret"))

			// give it time to execute the cb's
			<-cb1Chan
			assert.Equal(t, 2, cb1Count)
			assert.Equal(t, 1, cb2Count)
		})
	})

	t.Run("Single callback multiple secrets", func(t *testing.T) {
		dw := &Dweller{
			Duration: dwellDuration,
		}
		var cbCount int
		cbChan := make(chan struct{})
		secretCb := func(input manager.SecretChangeset) {
			cbCount++
			cbChan <- struct{}{}
		}

		mgrCb := dw.Dwell(secretCb)
		randomlyCallManagerCallback(mgrCb, 10, 1*dwellUnit, genID(t))

		// give it time to execute the cb's
		<-cbChan
		assert.Equal(t, 1, cbCount)
		t.Run("stop closer", func(t *testing.T) {

			mgrCb(buildSecretChangeset(genID(t)))

			err := dw.Closer.Close()
			assert.Nil(t, err)
			assert.Equal(t, 1, cbCount)
		})
	})

	t.Run("Concurrent read/write to Changeset", func(t *testing.T) {
		dw := &Dweller{
			Duration: 100 * dwellUnit,
		}

		cbChan := make(chan struct{})
		updatedSecretNames := []string{
			genID(t),
			genID(t),
		}
		cb := func(input manager.SecretChangeset) {
			assert.True(t, input.SecretHasBeenUpdated(updatedSecretNames[0]))
			assert.True(t, input.SecretHasBeenUpdated(updatedSecretNames[1]))
			cbChan <- struct{}{}
		}

		mgrCb := dw.Dwell(cb)
		randomlyCallManagerCallback(mgrCb, 10, dwellUnit, updatedSecretNames[0])
		randomlyCallManagerCallback(mgrCb, 10, dwellUnit, updatedSecretNames[1])

		<-cbChan
		updatedSecretNames = []string{
			genID(t),
			genID(t),
		}
		randomlyCallManagerCallback(mgrCb, 10, dwellUnit, updatedSecretNames[0])
		randomlyCallManagerCallback(mgrCb, 10, dwellUnit, updatedSecretNames[1])
		<-cbChan

	})
}

func randomlyCallManagerCallback(cb func(input manager.SecretChangeset), n int, unit time.Duration, secretName string) {
	for i := 0; i < 3; i++ {
		randSplay := rand.Intn(n)
		time.Sleep(time.Duration(randSplay) * unit)
		// this will call function with random secret name
		cb(buildSecretChangeset(secretName))
	}
	return
}
