package policy

import (
	"fmt"
	"testing"

	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/require"
)

func TestExtractPolicySecretKeys(t *testing.T) {
	genID := func(t *testing.T) string {
		id, err := uuid.NewV4()
		require.NoError(t, err)
		return id.String()
	}

	accountID := "1234567890"
	env := "testing"

	pg := new(IAMPolicyGenerator)
	pg.SetPathPrefixEnvironment(env)
	pg.AccountID = accountID
	Convey("ExtractPolicySecretKeys should return secret keys", t, func() {
		keys := []string{
			"syseng/service/production/key",
			"video/service/production/key",
		}
		namespaces := []string{
			"syseng",
			"video",
		}
		document := pg.PolicyDocument(keys, namespaces, false, "arn doesn't matter")
		extractedKeys := ExtractPolicySecretKeys(document)

		So(len(extractedKeys), ShouldEqual, len(keys))
		for _, v := range extractedKeys {
			So(keys, ShouldContain, v)
		}
	})
	Convey("generating RoleArn should match expectations", t, func() {
		someRoleName := genID(t)
		generatedRoleARN := pg.GenerateRoleARN(someRoleName)
		expectedRoleARN := fmt.Sprintf("arn:aws:iam::%s:role/sandstorm/%s/templated/role/%s", accountID, env, someRoleName)

		So(generatedRoleARN, ShouldEqual, expectedRoleARN)
	})
}
