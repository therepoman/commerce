package policy

import (
	"fmt"
	"testing"

	"code.justin.tv/systems/sandstorm/manager"
	gmocks "code.justin.tv/systems/sandstorm/mocks"
	"code.justin.tv/systems/sandstorm/policy/mocks"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/iam"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/suite"
)

func TestPolicyGenerator(t *testing.T) {
	Convey("PolicyGenerator generates valid policies", t, func() {
		pg := newTestPolicyGenerator()

		Convey("NamespacePolicyStatement", func() {
			namespaces := []string{
				"syseng",
				"video",
			}
			Convey("with write access", func() {
				expectedPolicyStatement := IAMPolicyStatement{
					Action:   RWNamespaceTableActions,
					Resource: []string{pg.DynamoDBNamespaceTableArn},
					Effect:   "Allow",
					Condition: IAMStatementCondition{
						"ForAllValues:StringEquals": map[string]interface{}{
							"dynamodb:LeadingKeys": namespaces,
						},
					},
					Sid: NamespaceTableSid,
				}
				statement := pg.NamespacePolicyStatement(namespaces, true)
				So(statement, ShouldResemble, expectedPolicyStatement)
			})
			Convey("without write access", func() {
				expectedPolicyStatement := IAMPolicyStatement{}
				statement := pg.NamespacePolicyStatement(namespaces, false)
				So(statement, ShouldResemble, expectedPolicyStatement)
			})
		})

		Convey("NamespaceIndexPolicyStatement", func() {
			namespaces := []string{
				"syseng",
				"video",
			}
			Convey("with write access", func() {
				expectedPolicyStatement := IAMPolicyStatement{
					Action:   RWNamespaceIndexActions,
					Resource: []string{fmt.Sprintf("%s/index/namespace_name", pg.DynamoDBSecretsTableArn)},
					Effect:   "Allow",
					Condition: IAMStatementCondition{
						"ForAllValues:StringEquals": map[string]interface{}{
							"dynamodb:LeadingKeys": namespaces,
						},
					},
					Sid: NamespaceIndexSid,
				}
				statement := pg.NamespaceIndexPolicyStatement(namespaces, true)
				So(statement, ShouldResemble, expectedPolicyStatement)
			})
			Convey("without write access", func() {
				expectedPolicyStatement := IAMPolicyStatement{
					Action:   RONamespaceIndexActions,
					Resource: []string{fmt.Sprintf("%s/index/namespace_name", pg.DynamoDBSecretsTableArn)},
					Effect:   "Allow",
					Condition: IAMStatementCondition{
						"ForAllValues:StringEquals": map[string]interface{}{
							"dynamodb:LeadingKeys": namespaces,
						},
					},
					Sid: NamespaceIndexSid,
				}
				statement := pg.NamespaceIndexPolicyStatement(namespaces, false)
				So(statement, ShouldResemble, expectedPolicyStatement)
			})
		})

		Convey("SecretsPolicyStatement", func() {
			secretKeys := []string{
				"syseng/service/environment/secret",
				"syseng/service/environment/secret2",
			}
			Convey("with write access", func() {
				expectedPolicyStatement := IAMPolicyStatement{
					Action: RWSecretsTableActions,
					Resource: []string{
						pg.DynamoDBSecretsTableArn,
						pg.DynamoDBSecretsAuditTableArn,
					},
					Effect: "Allow",
					Condition: IAMStatementCondition{
						"ForAllValues:StringLike": map[string]interface{}{
							"dynamodb:LeadingKeys": secretKeys,
						},
					},
					Sid: SecretsTableSid,
				}
				statement := pg.SecretsPolicyStatement(secretKeys, true)
				So(statement, ShouldResemble, expectedPolicyStatement)
			})
			Convey("without write access", func() {
				expectedPolicyStatement := IAMPolicyStatement{
					Action: ROSecretsTableActions,
					Resource: []string{
						pg.DynamoDBSecretsTableArn,
					},
					Effect: "Allow",
					Condition: IAMStatementCondition{
						"ForAllValues:StringLike": map[string]interface{}{
							"dynamodb:LeadingKeys": secretKeys,
						},
					},
					Sid: SecretsTableSid,
				}
				statement := pg.SecretsPolicyStatement(secretKeys, false)
				So(statement, ShouldResemble, expectedPolicyStatement)
			})
		})
	})
}

func TestValidateNamespaces(t *testing.T) {
	Convey("ValidateNamespaces should correctly parse namespaces from keys", t, func() {
		secretsMock := new(mocks.SecretsManager)
		defer func() {
			So(secretsMock.AssertExpectations(t), ShouldBeTrue)
		}()

		keys := []string{
			"syseng/service/production/key",
			"video/service/production/key",
			"video/service2/production/key",
		}
		namespaces := []string{
			"syseng",
			"video",
		}

		secretsMock.On(
			"CrossEnvironmentSecretsSet", keys,
		).Return(map[string]struct{}{}, nil).Once()

		ns, checkKeysErr := ValidateNamespaces(keys, secretsMock)
		So(checkKeysErr, ShouldBeNil)
		So(len(ns), ShouldEqual, len(namespaces))
		for _, v := range ns {
			So(namespaces, ShouldContain, v)
		}
	})

	Convey("ValidateNamespaces should correctly parse namespaces from keys should ignore cross env secrets for secrets validation", t, func() {
		const crossEnvKey = "video/service2/global/key"

		secretsMock := new(mocks.SecretsManager)
		defer func() {
			So(secretsMock.AssertExpectations(t), ShouldBeTrue)
		}()

		keys := []string{
			"syseng/service/production/key",
			"video/service/production/key",
			"video/service2/production/key",
			crossEnvKey,
		}
		namespaces := []string{
			"syseng",
			"video",
		}

		secretsMock.On(
			"CrossEnvironmentSecretsSet", keys,
		).Return(map[string]struct{}{
			crossEnvKey: struct{}{},
		}, nil).Once()

		ns, checkKeysErr := ValidateNamespaces(keys, secretsMock)
		So(checkKeysErr, ShouldBeNil)
		So(len(ns), ShouldEqual, len(namespaces))
		for _, v := range ns {
			So(namespaces, ShouldContain, v)
		}
	})

	Convey("ValidateNamespaces should return error on mixed environments", t, func() {

		secretsMock := new(mocks.SecretsManager)
		defer func() {
			So(secretsMock.AssertExpectations(t), ShouldBeTrue)
		}()

		keys := []string{
			"syseng/service/production/key",
			"video/service/production/key",
			"video/service2/production/key",
			"video/service2/development/key",
		}

		secretsMock.On(
			"CrossEnvironmentSecretsSet", keys,
		).Return(map[string]struct{}{}, nil).Once()

		_, checkKeysErr := ValidateNamespaces(keys, secretsMock)
		So(checkKeysErr, ShouldResemble, ErrMultipleEnvironments)
	})
}

func TestValidateWildcards(t *testing.T) {
	Convey("ValidateWildcards", t, func() {
		Convey("should allow wildcard on service in development environment", func() {
			ns := &manager.SecretNamespace{
				Team:        "team",
				System:      wildcard,
				Environment: developmentEnvironment,
				Name:        wildcard,
			}
			So(ValidateWildcards(ns), ShouldBeTrue)
		})
		Convey("should not allow wildcards on service in any other environment", func() {
			var environments = []string{
				"staging",
				"production",
				"darklaunch",
			}
			for _, environment := range environments {
				ns := &manager.SecretNamespace{
					Team:        "team",
					System:      wildcard,
					Environment: environment,
					Name:        wildcard,
				}
				So(ValidateWildcards(ns), ShouldBeFalse)
			}
		})
		Convey("should allow wildcards in service name field", func() {
			var secretNames = []string{
				"secrets",
				"secrets*",
				"*secrets",
				"se*crets",
				"*secrets*",
				"se*c*rets",
			}
			for _, secretName := range secretNames {
				ns := &manager.SecretNamespace{
					Team:        "team",
					System:      "system",
					Environment: "production",
					Name:        secretName,
				}
				So(ValidateWildcards(ns), ShouldBeTrue)
			}
		})
		Convey("should not allow wildcards in team or environment or system (not in development environment)", func() {
			var namespaces = []*manager.SecretNamespace{
				&manager.SecretNamespace{
					Team:        wildcard,
					System:      "system",
					Environment: "production",
					Name:        "secret",
				},
				&manager.SecretNamespace{
					Team:        "team*",
					System:      "system",
					Environment: "production",
					Name:        "secret",
				},
				&manager.SecretNamespace{
					Team:        "team",
					System:      "system*",
					Environment: "production",
					Name:        "secret",
				},
				&manager.SecretNamespace{
					Team:        "team",
					System:      wildcard,
					Environment: "production",
					Name:        "secret",
				},
				&manager.SecretNamespace{
					Team:        "team",
					System:      "system",
					Environment: "production*",
					Name:        "secret",
				},
				&manager.SecretNamespace{
					Team:        "team*",
					System:      "system",
					Environment: "production",
					Name:        "secret",
				},
				&manager.SecretNamespace{
					Team:        "team",
					System:      "system",
					Environment: wildcard,
					Name:        "secret",
				},
			}
			for _, ns := range namespaces {
				So(ValidateWildcards(ns), ShouldBeFalse)
			}
		})
	})
}

func TestGetGroupAuxPolicyInput(t *testing.T) {

	Convey("Test getGroupAuxPolicyInput returns correct input.", t, func() {
		groupName := "fakeGroupName"
		pg := newTestPolicyGenerator()
		output := pg.getGroupAuxPolicyInput(groupName)

		expectedOutput := &iam.ListAttachedGroupPoliciesInput{
			GroupName:  aws.String(groupName),
			PathPrefix: aws.String("/sandstorm/testing/aux_policy/"),
		}
		So(output, ShouldResemble, expectedOutput)
	})

}

type PolicyGeneratorTestSuite struct {
	suite.Suite

	MockedDynamoDB     *gmocks.DynamoDBAPI
	MockedIAM          *gmocks.IAMAPI
	IAMPolicyGenerator *IAMPolicyGenerator
}

func (suite *PolicyGeneratorTestSuite) SetupTest() {
	suite.MockedDynamoDB = &gmocks.DynamoDBAPI{}
	suite.MockedIAM = &gmocks.IAMAPI{}
	suite.IAMPolicyGenerator = &IAMPolicyGenerator{
		AuxPolicyArn:                 "arn:aws:iam::734326455073:policy/sandstorm/testing/aux_policy/sandstorm-agent-testing-aux",
		RWAuxPolicyArn:               "arn:aws:iam::734326455073:policy/sandstorm/testing/aux_policy/sandstorm-agent-testing-rw-aux",
		DynamoDBSecretsTableArn:      "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-testing",
		DynamoDBSecretsAuditTableArn: "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-testing_audit",
		DynamoDBNamespaceTableArn:    "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-testing_namespaces",
		IAMPathPrefix:                IAMPathPrefix,
		DynamoDB:                     suite.MockedDynamoDB,
		IAM:                          suite.MockedIAM,
	}
}

func (suite *PolicyGeneratorTestSuite) TearDownTest() {
	suite.MockedDynamoDB.AssertExpectations(suite.T())
	suite.MockedIAM.AssertExpectations(suite.T())
}

func TestPolicyGeneratorTestSuite(t *testing.T) {
	suite.Run(t, &PolicyGeneratorTestSuite{})
}
