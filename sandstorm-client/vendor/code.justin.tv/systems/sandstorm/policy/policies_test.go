package policy

import (
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/iam"
	"github.com/stretchr/testify/assert"

	"code.justin.tv/systems/sandstorm/mocks"
)

const (
	groupName = "testName"
	policyArn = "someFakeARN"
)

func mockPolicyGenerator() (*IAMPolicyGenerator, *mocks.IAMAPI) {
	ddbMock := &mocks.DynamoDBAPI{}
	iamMock := &mocks.IAMAPI{}
	logger := &mocks.FieldLogger{}

	pg := &IAMPolicyGenerator{
		AuxPolicyArn:                 "fakeAuxPolicyArn",
		RWAuxPolicyArn:               "fakeRWAuxPolicyArn",
		DynamoDBSecretsTableArn:      "fakeTableArn",
		DynamoDBSecretsAuditTableArn: "fakeAuditTableArn",
		DynamoDBNamespaceTableArn:    "fakeNamespaceTableArn",
		RoleOwnerTableName:           "fakeRoleOwnerTableName",
		IAM:                          iamMock,
		DynamoDB:                     ddbMock,
		Logger:                       logger,
	}
	return pg, iamMock
}

func TestDetachGroupPolicyCallsAWSWithCorrectPolicyInput(t *testing.T) {

	assert := assert.New(t)
	pg, iamMock := mockPolicyGenerator()

	t.Run("PolicyManager calls AWS IAM with correct input", func(t *testing.T) {
		expectedInput := &iam.DetachGroupPolicyInput{
			PolicyArn: aws.String(policyArn),
			GroupName: aws.String(groupName),
		}

		iamMock.On("DetachGroupPolicy", expectedInput).Return(&iam.DetachGroupPolicyOutput{}, nil).Once()
		_, err := pg.DetachGroupPolicy(policyArn, groupName)
		if assert.NoError(err) {
			assert.Nil(err)
		}
		iamMock.AssertExpectations(t)
	})
}

func TestAttachGroupPolicyCallsAWSWithCorrectPolicyInput(t *testing.T) {

	assert := assert.New(t)
	pg, iamMock := mockPolicyGenerator()

	t.Run("PolicyManager calls AWS IAM with correct input", func(t *testing.T) {
		expectedInput := &iam.AttachGroupPolicyInput{
			PolicyArn: aws.String(policyArn),
			GroupName: aws.String(groupName),
		}

		iamMock.On("AttachGroupPolicy", expectedInput).Return(&iam.AttachGroupPolicyOutput{}, nil).Once()
		_, err := pg.AttachGroupPolicy(policyArn, groupName)
		if assert.NoError(err) {
			assert.Nil(err)
		}
		iamMock.AssertExpectations(t)
	})
}
