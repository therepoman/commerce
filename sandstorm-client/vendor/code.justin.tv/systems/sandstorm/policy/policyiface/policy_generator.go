package policyiface

import (
	"context"

	"code.justin.tv/systems/sandstorm/policy"
	"github.com/aws/aws-sdk-go/service/iam"
)

// IAMPolicyGeneratorAPI is the interface IAMPolicyGenerator implements
type IAMPolicyGeneratorAPI interface {
	Create(
		request *policy.CreateRoleRequest,
		auth policy.Authorizer,
		secrets policy.SecretsManager,
	) (response *policy.CreateRoleResponse, err error)

	Delete(roleName string, auth policy.Authorizer) (err error)

	Get(roleName string) (role *policy.IAMRole, err error)

	List() (roles []string, err error)
	ListPage(context.Context, *policy.ListPageInput) (*policy.ListPageOutput, error)

	Put(
		request *policy.PutRoleRequest,
		auth policy.Authorizer,
		secrets policy.SecretsManager,
	) (response *policy.PutRoleResponse, err error)

	CreateGroup(req policy.CreateGroupRequest) (*policy.Group, error)
	PatchGroup(groupName string, writeAccess bool) error
	GetGroup(groupName string) (*policy.Group, error)
	DeleteGroup(groupName string, auth policy.Authorizer) error
	AddUsersToGroup(input policy.AddUsersToGroupInput) error
	RemoveUsersFromGroup(input policy.RemoveUsersFromGroupInput) error
	CheckGroupExistence(groupName string) bool
	ListGroups() ([]*iam.Group, error)

	GetUser(username string) (policy.User, error)
	ListUsers() ([]*iam.User, error)
}
