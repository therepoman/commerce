package policy

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

type testStruct struct {
	CurrentKeys    []string
	RequestedKeys  []string
	ExpectedResult []string
}

func TestCompareSecretKeys(t *testing.T) {

	var compareTests = []testStruct{
		testStruct{
			CurrentKeys:    []string{"key1", "key2"},
			RequestedKeys:  []string{"key1", "key2", "key3"},
			ExpectedResult: []string{"key3"},
		},
		testStruct{
			CurrentKeys:    []string{"key1", "key2"},
			RequestedKeys:  []string{"key3", "key4"},
			ExpectedResult: []string{"key3", "key4", "key1", "key2"},
		},
		testStruct{
			CurrentKeys:    []string{},
			RequestedKeys:  []string{"key3"},
			ExpectedResult: []string{"key3"},
		},
		testStruct{
			RequestedKeys:  []string{},
			CurrentKeys:    []string{"key3"},
			ExpectedResult: []string{"key3"},
		},
	}

	Convey("Test compareSecretKeys...", t, func() {
		for _, testCase := range compareTests {
			actualKeys := compareSecretKeys(testCase.CurrentKeys, testCase.RequestedKeys)

			So(compareExpectedAndActualSecretKeys(actualKeys, testCase.ExpectedResult), ShouldBeTrue)
		}

	})

}

func compareExpectedAndActualSecretKeys(actualKeys []string, ExpectedKeys []string) bool {

	if actualKeys == nil && ExpectedKeys == nil {
		return true
	}

	if actualKeys == nil || ExpectedKeys == nil {
		return false
	}

	if len(actualKeys) != len(ExpectedKeys) {
		return false
	}

	actualKeySet := make(map[string]bool)
	for _, v := range actualKeys {
		actualKeySet[v] = true
	}

	for _, v := range ExpectedKeys {
		if _, ok := actualKeySet[v]; !ok {
			return false
		}
	}

	return true
}
