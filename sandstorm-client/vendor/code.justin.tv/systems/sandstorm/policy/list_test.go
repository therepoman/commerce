package policy

import (
	"context"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/iam"
	"github.com/stretchr/testify/mock"
)

func (suite *PolicyGeneratorTestSuite) TestListPage() {
	roles := []string{"role1"}
	suite.MockedIAM.On("ListRolesWithContext", mock.Anything, &iam.ListRolesInput{
		PathPrefix: aws.String(suite.IAMPolicyGenerator.PathPrefix(IAMRolePathSuffix)),
		MaxItems:   aws.Int64(1),
	}).Return(&iam.ListRolesOutput{
		Roles: []*iam.Role{
			{
				RoleName: aws.String("role1"),
			},
		},
	}, nil).Once()

	output, err := suite.IAMPolicyGenerator.ListPage(context.Background(), &ListPageInput{First: 1})
	suite.Require().NoError(err)
	suite.Assert().Equal(roles, output.Roles)
	suite.Assert().False(output.HasNextPage)
	suite.Assert().Empty(output.Cursor)
}

func (suite *PolicyGeneratorTestSuite) TestListPageWithAfter() {
	roles := []string{"role1", "role2"}
	after := "after"
	suite.MockedIAM.On("ListRolesWithContext", mock.Anything, &iam.ListRolesInput{
		PathPrefix: aws.String(suite.IAMPolicyGenerator.PathPrefix(IAMRolePathSuffix)),
		MaxItems:   aws.Int64(1),
		Marker:     aws.String(after),
	}).Return(&iam.ListRolesOutput{
		Roles: []*iam.Role{
			{
				RoleName: aws.String("role1"),
			},
			{
				RoleName: aws.String("role2"),
			},
		},
		Marker:      aws.String("marker"),
		IsTruncated: aws.Bool(true),
	}, nil).Once()

	output, err := suite.IAMPolicyGenerator.ListPage(context.Background(), &ListPageInput{First: 1, After: after})
	suite.Require().NoError(err)
	suite.Assert().Equal(roles, output.Roles)
	suite.Assert().True(output.HasNextPage)
	suite.Assert().Equal("marker", output.Cursor)
}
