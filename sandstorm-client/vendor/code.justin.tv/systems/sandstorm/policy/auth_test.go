package policy

import (
	"testing"

	"code.justin.tv/systems/sandstorm/policy/mocks"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestValidateNamespace(t *testing.T) {
	secretsMock := new(mocks.SecretsManager)

	successTestFixtures := [][]string{
		{
			"group/app/stag/sec1",
			"group/app/staging/sec2",
			"group/app/stag/sec3",
			"group/app/staging/sec4",
		},
		{
			"group/app/prod/sec1",
			"group/app/production/sec2",
			"group/app/prod/sec3",
			"group/app/production/sec4",
		},
		{
			"group/app/dev/sec1",
			"group/app/develop/sec2",
			"group/app/development/sec3",
			"group/app/dev/sec4",
		},
		{
			"group/app/random/sec1",
			"group/app/random/sec2",
			"group/app/random/sec3",
			"group/app/random/sec4",
		},
	}

	failTestFixtures := [][]string{
		{
			"group/app/stag/sec1",
			"group/app/staging/sec2",
			"group/app/prod/sec3",
		},
		{
			"group/app/prod/sec1",
			"group/app/production/sec2",
			"group/app/staging/sec3",
		},
		{
			"group/app/dev/sec1",
			"group/app/stag/sec2",
			"group/app/development/sec3",
			"group/app/dev/sec4",
		},
		{
			"group/app/random/sec1",
			"group/app/prod/sec2",
			"group/app/random/sec4",
		},
	}

	t.Run("should match staging and prod env", func(t *testing.T) {
		for _, secretKeys := range successTestFixtures {
			secretsMock.
				On("CrossEnvironmentSecretsSet", mock.Anything).
				Return(map[string]struct{}{}, nil)

			_, err := ValidateNamespaces(secretKeys, secretsMock)
			require.NoError(t, err)

			secretsMock.AssertExpectations(t)
		}
	})

	t.Run("should fail as expected", func(t *testing.T) {
		for _, secretKeys := range failTestFixtures {
			secretsMock.
				On("CrossEnvironmentSecretsSet", mock.Anything).
				Return(map[string]struct{}{}, nil)

			_, err := ValidateNamespaces(secretKeys, secretsMock)
			assert.Equal(t, err, ErrMultipleEnvironments)

			secretsMock.AssertExpectations(t)
		}
	})

}
