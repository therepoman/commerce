package policy

import (
	"errors"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/iam"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestIsPolicyAttachedToAnyOtherEntitiesThanGroup(t *testing.T) {

	type testAttachedEntities struct {
		*AttachedEntities
		GroupName        string
		ExpectedResponse bool
	}
	testPolicyArn := "testArn"
	groupName := "randomGroupName"
	groupID := "1234"
	randomName := "randomRoleName"
	randomID := "123"

	t.Run("Ensure that isPolicyAttachedToAnyOtherEntitiesThanGroup returns correct result. ", func(t *testing.T) {
		assert := assert.New(t)
		tests := []*testAttachedEntities{
			&testAttachedEntities{
				ExpectedResponse: false,
				GroupName:        groupName,
				AttachedEntities: &AttachedEntities{
					PolicyArn:    testPolicyArn,
					PolicyGroups: []*iam.PolicyGroup{},
					PolicyRoles:  []*iam.PolicyRole{},
					PolicyUsers:  []*iam.PolicyUser{},
				},
			},
			&testAttachedEntities{
				ExpectedResponse: false,
				GroupName:        groupName,
				AttachedEntities: nil,
			},
			&testAttachedEntities{
				ExpectedResponse: true,
				GroupName:        groupName,
				AttachedEntities: &AttachedEntities{
					PolicyArn: testPolicyArn,
					PolicyGroups: []*iam.PolicyGroup{
						&iam.PolicyGroup{
							GroupName: &groupName,
							GroupId:   &groupID,
						},
					},
					PolicyRoles: []*iam.PolicyRole{},
					PolicyUsers: []*iam.PolicyUser{},
				},
			},
			&testAttachedEntities{
				ExpectedResponse: true,
				GroupName:        groupName,
				AttachedEntities: &AttachedEntities{
					PolicyArn:    testPolicyArn,
					PolicyGroups: []*iam.PolicyGroup{},
					PolicyRoles: []*iam.PolicyRole{
						&iam.PolicyRole{
							RoleId:   &randomID,
							RoleName: &randomName,
						},
					},
					PolicyUsers: []*iam.PolicyUser{},
				},
			},
			&testAttachedEntities{
				ExpectedResponse: true,
				GroupName:        groupName,
				AttachedEntities: &AttachedEntities{
					PolicyArn:    testPolicyArn,
					PolicyGroups: []*iam.PolicyGroup{},
					PolicyUsers: []*iam.PolicyUser{
						&iam.PolicyUser{
							UserId:   &randomID,
							UserName: &randomName,
						},
					},
					PolicyRoles: []*iam.PolicyRole{},
				},
			},
			&testAttachedEntities{
				ExpectedResponse: true,
				GroupName:        groupName,
				AttachedEntities: &AttachedEntities{
					PolicyArn: testPolicyArn,
					PolicyGroups: []*iam.PolicyGroup{
						&iam.PolicyGroup{
							GroupName: &groupName,
							GroupId:   &groupID,
						},
					},
					PolicyRoles: []*iam.PolicyRole{
						&iam.PolicyRole{
							RoleId:   &randomID,
							RoleName: &randomName,
						},
					},
					PolicyUsers: []*iam.PolicyUser{
						&iam.PolicyUser{
							UserId:   &randomID,
							UserName: &randomName,
						},
					},
				},
			},
			&testAttachedEntities{
				ExpectedResponse: true,
				GroupName:        groupName,
				AttachedEntities: &AttachedEntities{
					PolicyArn: testPolicyArn,
					PolicyGroups: []*iam.PolicyGroup{
						&iam.PolicyGroup{
							GroupName: &groupName,
							GroupId:   &groupID,
						},
					},
					PolicyRoles: []*iam.PolicyRole{
						&iam.PolicyRole{
							RoleId:   &randomID,
							RoleName: &randomName,
						},
					},
					PolicyUsers: []*iam.PolicyUser{},
				},
			},
		}

		for _, test := range tests {
			entities := test.AttachedEntities

			isAttached := isPolicyAttachedToAnyOtherEntities(entities)

			assert.Equal(test.ExpectedResponse, isAttached, "invalid response returned by isPolicyAttachedToAnyOtherEntities")
		}
	})
}

func TestPolicyPatchGroup(t *testing.T) {
	pg, iamMock := mockPolicyGenerator()
	assert := assert.New(t)
	t.Run("PatchGroup returns error on empty group name", func(t *testing.T) {
		err := pg.PatchGroup("", true)
		assert.NotNil(err)

		assert.Contains(err.Error(), "group name cannot be empty", "Error message should explain group name was empty")
	})

	t.Run("PatchGroup returns error if no policies are attached to group being patched ", func(t *testing.T) {

		iamMock.On("ListAttachedGroupPolicies", mock.Anything).Return(nil, errors.New("aws returned an error")).Once()
		err := pg.PatchGroup("someName", true)

		assert.NotNil(err)
		assert.Contains(err.Error(), "aws returned an error", "AWS error message was not returned to the caller")
		iamMock.AssertExpectations(t)
	})

	t.Run("PatchGroup will not update the policy if the aux policy attached to group is the desired policy", func(t *testing.T) {

		policyArn := "fakeAuxPolicyArn"

		expectedListAttachedGroupPolicies := expectedListAttachedGroupPoliciesOutput(policyArn)
		iamMock.On("ListAttachedGroupPolicies", mock.Anything).Return(expectedListAttachedGroupPolicies, nil).Once()

		err := pg.PatchGroup(groupName, false)
		assert.Nil(err, "Error thrown when getting attached aux policy")
		iamMock.AssertExpectations(t)
	})

	t.Run("PatchGroup attaches the desired policy to group before detaching the other aux policy", func(t *testing.T) {
		policyArn := "otherAuxPolicy"

		expectedListAttachedGroupPolicies := expectedListAttachedGroupPoliciesOutput(policyArn)
		iamMock.On("ListAttachedGroupPolicies", mock.Anything).Return(expectedListAttachedGroupPolicies, nil).Once()

		detachPolicyInput := &iam.DetachGroupPolicyInput{
			PolicyArn: aws.String(policyArn),
			GroupName: aws.String(groupName),
		}
		iamMock.On("DetachGroupPolicy", detachPolicyInput).Return(nil, nil).Once()

		attachedPolicyInput := &iam.AttachGroupPolicyInput{
			PolicyArn: aws.String("fakeAuxPolicyArn"),
			GroupName: aws.String(groupName),
		}
		iamMock.On("AttachGroupPolicy", attachedPolicyInput).Return(nil, nil).Once()

		err := pg.PatchGroup(groupName, false)
		assert.Nil(err, "Error thrown when getting attached aux policy")
		iamMock.AssertExpectations(t)

	})

}

func expectedListAttachedGroupPoliciesOutput(policyArn string) *iam.ListAttachedGroupPoliciesOutput {
	var attachedPolicies []*iam.AttachedPolicy
	attachedPolicies = append(attachedPolicies, &iam.AttachedPolicy{
		PolicyArn: aws.String(policyArn),
	})

	return &iam.ListAttachedGroupPoliciesOutput{
		AttachedPolicies: attachedPolicies,
	}
}
