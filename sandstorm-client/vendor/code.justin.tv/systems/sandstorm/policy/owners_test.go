package policy

import (
	"encoding/json"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestOwners(t *testing.T) {
	Convey("Creating NewOwners should..", t, func() {
		Convey("Return nil when nothing passed into initialization", func() {
			owners := NewOwners([]string{})
			So(owners, ShouldBeNil)
		})
		Convey("Build from a []string", func() {
			owners := NewOwners([]string{"Any_string"})
			So(owners, ShouldNotBeNil)
		})
	})
	Convey("Unmarshal Owners should generate necessary data structures", t, func() {
		ownersJSON := []byte(`{"owners": {"ldapgroups": ["team-navi"]}}`)
		type unmarshalHolder struct {
			Owners Owners `json:"owners"`
		}

		var holder unmarshalHolder
		err := json.Unmarshal(ownersJSON, &holder)
		So(err, ShouldBeNil)

		So(len(holder.Owners.set), ShouldBeGreaterThanOrEqualTo, 1)
	})
	Convey("Owners objects can...", t, func() {
		owners := NewOwners([]string{"team-navi", "team-syseng"})
		So(owners, ShouldNotBeNil)

		Convey("use IsElement to determine if ldapGroup is an owner", func() {
			exists := owners.IsElement("team-navi")
			So(exists, ShouldBeTrue)

			nonExistent := owners.IsElement("team-doesn'texist")
			So(nonExistent, ShouldBeFalse)
		})
		Convey("use Contains to determine if any group in slice is an owner", func() {
			exists := owners.Contains([]string{"team-navi", "team-bullshit"})
			So(exists, ShouldBeTrue)

			nonExistent := owners.Contains([]string{"team-bullshit"})
			So(nonExistent, ShouldBeFalse)
		})
	})

}
