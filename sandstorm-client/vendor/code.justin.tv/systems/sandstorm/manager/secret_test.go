package manager

import (
	"encoding/base64"
	"errors"
	"strconv"
	"testing"

	"code.justin.tv/systems/sandstorm/pkg/envelope"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type secretTestData struct {
	Error     bool
	Namespace *SecretNamespace
}

var secretNamespaceTestData = map[string]secretTestData{
	"syseng/sandstorm/production/x509": {
		Error: false,
		Namespace: &SecretNamespace{
			Team:        "syseng",
			System:      "sandstorm",
			Environment: "production",
			Name:        "x509",
		},
	},
	"web/rails/staging/db/pg": {
		Error: false,
		Namespace: &SecretNamespace{
			Team:        "web",
			System:      "rails",
			Environment: "staging",
			Name:        "db/pg",
		},
	},
	"chat/tmi/darklaunch/redis-something-or-other": {
		Error: false,
		Namespace: &SecretNamespace{
			Team:        "chat",
			System:      "tmi",
			Environment: "darklaunch",
			Name:        "redis-something-or-other",
		},
	},
	"not/a/legit-namespace": {
		Error: true,
	},
	"definitely-not-a-namespace": {
		Error: true,
	},
	"台/灣/numba/wan": {
		Error: false,
		Namespace: &SecretNamespace{
			Team:        "台",
			System:      "灣",
			Environment: "numba",
			Name:        "wan",
		},
	},
}

func TestSecret(t *testing.T) {
	Convey("Manager Secret Tests", t, func() {
		Convey("validateSecretName", func() {
			Convey("Should return false if secret name is invalid", func() {
				invalidSecretName := "group/project/env/my$ecretName"

				valid := validateSecretName(invalidSecretName)
				So(valid, ShouldEqual, false)
			})

			Convey("Should return true if secret name is valid", func() {
				validSecretName := "group/project/env/mySecretName"

				valid := validateSecretName(validSecretName)
				So(valid, ShouldEqual, true)
			})
		})
		Convey("ParseSecretNamespace", func() {
			Convey("should correctly parse secret names", func() {
				for name, testData := range secretNamespaceTestData {
					std, parseErr := ParseSecretNamespace(name)
					if testData.Error {
						So(parseErr, ShouldNotBeNil)
						continue
					}
					So(parseErr, ShouldBeNil)
					So(std, ShouldResemble, testData.Namespace)
				}
			})
		})
		Convey("asDynamoSecret", func() {
			Convey("should map all fields", func() {
				s := &Secret{
					Name:           "group/project/env/mySecretName",
					UpdatedAt:      147,
					KeyARN:         "myKeyARN",
					DoNotBroadcast: true,
					Tombstone:      true,
					Class:          ClassPublic,
					key:            []byte("myKey"),
					ciphertext:     []byte("myValue"),
				}

				expected := &dynamoSecret{
					Name:           "group/project/env/mySecretName",
					Namespace:      "group",
					UpdatedAt:      147,
					KeyARN:         "myKeyARN",
					DoNotBroadcast: true,
					Tombstone:      true,
					Class:          1,
					Key:            "bXlLZXk=",
					Value:          "bXlWYWx1ZQ==",
				}

				So(s.asDynamoSecret(), ShouldResemble, expected)
			})
			Convey("should map invalid Classes to ClassDefault", func() {
				for _, class := range []int{-1, 0, 5, 6} {
					s := &Secret{Class: SecretClass(class)}
					expected := &dynamoSecret{Class: int(ClassDefault)}
					So(s.asDynamoSecret(), ShouldResemble, expected)
				}

			})
		})
	})
}

func getValidSecretForUnMarshaling() map[string]*dynamodb.AttributeValue {
	doNotBroadcast := false
	tombstone := false
	keyarn := "somearn"

	secretName, updatedAt, cipherText, key := getSecretRequiredParams()

	validSecret := map[string]*dynamodb.AttributeValue{
		"name":             {S: &secretName},
		"do_not_broadcast": {BOOL: &doNotBroadcast},
		"tombstone":        {BOOL: &tombstone},
		"key_arn":          {S: &keyarn},
		"updated_at":       {N: &updatedAt},
		"value":            {S: &cipherText},
		"key":              {S: &key},
	}
	return validSecret
}

func getSecretRequiredParams() (string, string, string, string) {
	secretName := "somename"
	updatedAt := "1"
	cipherText := base64.StdEncoding.EncodeToString([]byte("cipherTextValue"))
	key := base64.StdEncoding.EncodeToString([]byte("key"))
	return secretName, updatedAt, cipherText, key
}

func TestUnmarshalSecret(t *testing.T) {
	Convey("Manager Unmarshal JSON", t, func() {
		Convey("should successfully parse valid record", func() {
			secretToUnmarshal := getValidSecretForUnMarshaling()

			secret, err := unmarshalSecret(secretToUnmarshal)
			So(err, ShouldBeNil)
			So(secret, ShouldNotBeNil)
			So(secret.Name, ShouldEqual, "somename")
		})

		Convey("Should throw tombstone error if Tombstone is true", func() {
			secretToUnmarshal := getValidSecretForUnMarshaling()
			*secretToUnmarshal["tombstone"].BOOL = true
			_, err := unmarshalSecret(secretToUnmarshal)
			// So(secret, ShouldBeNil)
			So(err, ShouldEqual, ErrSecretTombstoned)
		})

		Convey("should complain about missing required attributes", func() {
			item := map[string]*dynamodb.AttributeValue{
				"name":       {S: aws.String("test-secret-name")},
				"updated_at": {N: aws.String("69")},
				"key":        {S: aws.String("test-key")},
				"value":      {S: aws.String("test-value")},
			}

			for key := range item {
				toUnmarshal := make(map[string]*dynamodb.AttributeValue)
				for k, v := range item {
					if k == key {
						continue
					}
					toUnmarshal[k] = v
				}

				_, err := unmarshalSecret(toUnmarshal)
				So(err, ShouldNotBeEmpty)
				So(err.Error(), ShouldContainSubstring, "value required")
			}
		})

		Convey("Should throw missing field when one or more required fields are missing", func() {

			secretName, updatedAt, cipherText, key := getSecretRequiredParams()

			invalidJSONs := []map[string]*dynamodb.AttributeValue{
				{
					"name": {S: &secretName},
				},
				{
					"name":       {S: &secretName},
					"updated_at": {N: &updatedAt},
				},
				{
					"name":       {S: &secretName},
					"updated_at": {N: &updatedAt},
					"value":      {S: &cipherText},
				},
				{
					"key":        {S: &key},
					"updated_at": {N: &updatedAt},
					"value":      {S: &cipherText},
				},
				{},
			}
			for _, secretToUnmarshal := range invalidJSONs {
				secret, err := unmarshalSecret(secretToUnmarshal)
				So(secret, ShouldBeNil)
				So(err, ShouldNotBeEmpty)
				So(err.Error(), ShouldContainSubstring, `value required`)
			}
		})

		Convey("should return class as 'ClassDefault' for a record with a depreciated class.", func() {
			for _, class := range []string{"0", "5"} {
				secretToUnmarshal := getValidSecretForUnMarshaling()
				secretToUnmarshal["class"] = &dynamodb.AttributeValue{N: aws.String(class)}

				secret, err := unmarshalSecret(secretToUnmarshal)
				So(secret, ShouldNotBeNil)
				So(err, ShouldBeNil)
				So(secret.Class, ShouldEqual, ClassDefault)
			}
		})

		Convey("Should set class to specified value", func() {
			secretToUnmarshal := getValidSecretForUnMarshaling()
			class := strconv.Itoa(int(ClassInternal))
			secretToUnmarshal[jsonClassField] = &dynamodb.AttributeValue{N: &class}
			secret, err := unmarshalSecret(secretToUnmarshal)
			So(secret, ShouldNotBeNil)
			So(err, ShouldBeNil)
			Convey("Secret class was set to Confidential", func() {
				So(secret.Class, ShouldEqual, ClassInternal)
			})
		})
	})
}

func TestFillPlaintext(t *testing.T) {
	Convey("TestfillPlaintext", t, func() {
		Convey("Should 20 character plaintext", func() {
			s := Secret{}
			So(s.FillPlaintext(&FillPlaintextRequest{
				Length: 20,
			}), ShouldBeNil)
			So(len(s.Plaintext), ShouldEqual, 28)
			So(s.Plaintext, ShouldNotResemble, make([]byte, 28))
		})

		Convey("Should error out for less than 2", func() {
			s := Secret{}
			So(s.FillPlaintext(&FillPlaintextRequest{
				Length: 1,
			}), ShouldNotBeNil)
		})

		Convey("Should error out for greater than than 4096", func() {
			s := Secret{}
			So(s.FillPlaintext(&FillPlaintextRequest{
				Length: 4097,
			}), ShouldNotBeNil)
		})
	})
}

func TestSecretEncryptDecrypt(t *testing.T) {

	testKey := []byte("my-key")
	encryptedDataKey := envelope.EncryptedDataKey{
		Region: "us-west-2",
		KeyARN: "randomKeyARN",
		Value:  testKey,
	}

	testSecret := Secret{
		key:        testKey,
		ciphertext: []byte("my-ciphertext"),
		KeyARN:     "randomKeyARN",
		Name:       "my-name",
		UpdatedAt:  1323,
	}

	decryptedPlaintext := []byte("my-decrypted-plaintext")
	encryptionCtx := map[string]string{
		"name":       "my-name",
		"updated_at": "1323",
	}

	encryptedCiphertext := []byte("my-ciphertext")

	t.Run("Decrypt", func(t *testing.T) {
		t.Run("success", func(t *testing.T) {
			mt := newManagerTest(t)
			defer mt.teardown(t)

			secret := new(Secret)
			*secret = testSecret

			mt.mockEnveloper.On(
				"Open", encryptedDataKey, secret.ciphertext, encryptionCtx,
			).Return(
				decryptedPlaintext, nil,
			)

			assert.NoError(t, mt.m.Decrypt(secret))
			assert.Equal(t, decryptedPlaintext, secret.Plaintext)
		})

		t.Run("aws error", func(t *testing.T) {
			mt := newManagerTest(t)
			defer mt.teardown(t)

			secret := new(Secret)
			*secret = testSecret

			awsErr := awserr.New("MyCode", "MyMessage", errors.New("my-original-error"))

			mt.mockEnveloper.On(
				"Open", encryptedDataKey, secret.ciphertext, encryptionCtx,
			).Return(
				nil, awsErr,
			)

			assert.Equal(t, awsErr, mt.m.Decrypt(secret))
			assert.Nil(t, secret.Plaintext)
		})
	})

	t.Run("Seal", func(t *testing.T) {
		testSecret := testSecret
		testSecret.Plaintext = decryptedPlaintext

		t.Run("success", func(t *testing.T) {
			mt := newManagerTest(t)
			defer mt.teardown(t)

			secret := new(Secret)
			*secret = testSecret

			mt.mockEnveloper.On(
				"Seal", mock.Anything, decryptedPlaintext,
			).Return(
				encryptedDataKey, encryptedCiphertext, nil,
			)

			assert.NoError(t, mt.m.Seal(secret))
		})

		t.Run("aws error", func(t *testing.T) {
			mt := newManagerTest(t)
			defer mt.teardown(t)

			secret := new(Secret)
			*secret = testSecret

			awsErr := awserr.New("MyCode", "MyMessage", errors.New("my-original-error"))

			mt.mockEnveloper.On(
				"Seal", mock.Anything, decryptedPlaintext,
			).Return(
				encryptedDataKey, encryptedCiphertext, awsErr,
			)

			assert.Equal(t, awsErr, mt.m.Seal(secret))
		})
	})

	t.Run("getEncryptedDataKey", func(t *testing.T) {
		testSecret := Secret{
			key:        []byte(testKey),
			ciphertext: []byte("my-ciphertext"),
			Name:       "my-name",
			UpdatedAt:  1323,
			KeyARN:     "testKeyARN",
		}
		assert.Equal(t, envelope.EncryptedDataKey{
			Region: awsRegionUsWest2,
			KeyARN: testSecret.KeyARN,
			Value:  testSecret.key,
		}, testSecret.getEncryptedDataKey())
	})

}
