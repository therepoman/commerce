// +build integration

package manager

import (
	"testing"
	"time"

	"code.justin.tv/systems/sandstorm/testutil"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestRevertIntegration(t *testing.T) {
	assert := assert.New(t)

	m := integrationTestCfg.Manager(t)

	secretValue := "mysecret"

	resetSecret := func(secretName string) {
		err := m.Delete(secretName)
		assert.Nil(err)
	}

	t.Run("Revert", func(t *testing.T) {
		t.Run("should raise an error for a nonexistent key", func(t *testing.T) {
			secretName := testutil.GetRandomSecretName(t)
			err := m.Revert(secretName, 0)
			assert.NotNil(err)
		})

		t.Run("should raise an error for a non-existing version", func(t *testing.T) {
			secretName := testutil.GetRandomSecretName(t)
			defer resetSecret(secretName)

			err := m.Post(&Secret{
				Name:      secretName,
				Plaintext: []byte(secretValue),
			})
			assert.Nil(err)

			versions, err := m.GetVersionsEncrypted(secretName, 10, 0)
			assert.Nil(err)
			if assert.NotNil(versions) {
				assert.Len(versions.Secrets, 1)
			}

			err = m.Revert(secretName, versions.Secrets[0].UpdatedAt+1)
			assert.Equal(ErrorSecretVersionDoesNotExist, err)
		})

		t.Run("when reverting should not add additional versions or change UpdatedAt", func(t *testing.T) {
			secretName := testutil.GetRandomSecretName(t)
			defer resetSecret(secretName)

			err := m.Post(&Secret{
				Name:      secretName,
				Plaintext: []byte(secretValue),
			})
			assert.Nil(err)

			// Audit table has seconds accuracy
			time.Sleep(time.Duration(1 * time.Second))

			t.Run("Revert", func(t *testing.T) {
				err = m.Patch(&PatchInput{
					Name:      secretName,
					Plaintext: []byte("mynextsecret"),
				})
				assert.Nil(err)

				versions, err := m.GetVersionsEncrypted(secretName, 10, 0)
				assert.Nil(err)
				if assert.NotNil(versions) {
					assert.Len(versions.Secrets, 2)
				}

				// Sleep to make sure we aren't accidentally updating the audit table
				time.Sleep(time.Duration(1 * time.Second))
				// Versions should be sorted by time descending.
				// Revert to the original value.

				err = m.Revert(secretName, versions.Secrets[1].UpdatedAt)
				assert.Nil(err)

				latestSecret, err := m.GetEncrypted(secretName)
				assert.Nil(err)
				// So(string(latestSecret.Plaintext), ShouldEqual, secretValue)
				if assert.NotNil(latestSecret) {
					assert.EqualValues(versions.Secrets[1], latestSecret)
				}

				latestVersions, err := m.GetVersionsEncrypted(secretName, 10, 0)
				assert.Nil(err)
				if assert.NotNil(latestVersions) {
					for _, v := range latestVersions.Secrets {
						assert.Contains(versions.Secrets, v)
					}
				}

				t.Run("Ensure that previous version was set during revert", func(t *testing.T) {
					auditRecord, err := getSecretAuditRecord(m, latestSecret)
					assert.Nil(err)
					assert.NotNil(auditRecord)

					assert.NotNil(auditRecord.PreviousVersion)
					assert.NotEqual(auditRecord.PreviousVersion, 0)
				})
			})

			t.Run("RevertSecret", func(t *testing.T) {
				err = m.Patch(&PatchInput{
					Name:      secretName,
					Plaintext: []byte("mynextsecret"),
				})
				assert.Nil(err)

				versions, err := m.GetVersionsEncrypted(secretName, 10, 0)
				require.NoError(t, err)
				if assert.NotNil(versions) {
					assert.Len(versions.Secrets, 3)
				}

				// Sleep to make sure we aren't accidentally updating the audit table
				time.Sleep(time.Duration(1 * time.Second))
				// Versions should be sorted by time descending.
				// Revert to the original value.

				_, err = m.RevertSecret(&RevertSecretInput{
					Name:       secretName,
					Version:    versions.Secrets[0].UpdatedAt,
					ActionUser: "test",
				})
				require.NoError(t, err)

				latestSecret, err := m.GetEncrypted(secretName)
				assert.Nil(err)
				if assert.NotNil(latestSecret) {
					assert.EqualValues(versions.Secrets[0], latestSecret)
				}

				latestVersions, err := m.GetVersionsEncrypted(secretName, 10, 0)
				assert.Nil(err)
				if assert.NotNil(latestVersions) {
					for _, v := range latestVersions.Secrets {
						assert.Contains(versions.Secrets, v)
					}
				}

				t.Run("Ensure that previous version and action user was set during revert", func(t *testing.T) {
					auditRecord, err := getSecretAuditRecord(m, latestSecret)
					assert.Nil(err)
					assert.NotNil(auditRecord)

					assert.NotNil(auditRecord.PreviousVersion)
					assert.NotEqual("test", auditRecord.ActionUser)
					assert.NotEqual(auditRecord.PreviousVersion, 0)
				})
			})
		})
	})

}
