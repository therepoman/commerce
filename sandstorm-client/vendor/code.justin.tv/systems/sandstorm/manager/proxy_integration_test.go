// +build integration

package manager

import (
	"errors"
	"testing"
	"time"

	"code.justin.tv/systems/sandstorm/testutil"
	"github.com/cenkalti/backoff"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestManagerWithProxyGetIntegration(t *testing.T) {
	m := integrationTestCfg.ProxyOnlyManager(t)

	t.Run("Manager", func(t *testing.T) {
		assert := assert.New(t)
		secretName := testutil.GetRandomSecretName(t)
		secretValue := "someSecretValue"

		err := m.Post(&Secret{
			Name:      secretName,
			Plaintext: []byte(secretValue),
		})
		require.NoError(t, err)

		t.Run("Get", func(t *testing.T) {
			var secret *Secret
			err := backoff.Retry(func() (err error) {
				secret, err = m.Get(secretName)
				if secret == nil {
					err = errors.New("secret not found")
				}
				return
			}, backoff.WithMaxTries(backoff.NewConstantBackOff(500*time.Millisecond), 10))

			if assert.NoError(err) {
				assert.NotNil(secret)
				assert.Equal(secretValue, string(secret.Plaintext))
			}
		})

		t.Run("Delete", func(t *testing.T) {
			_, err := m.DeleteSecret(&DeleteSecretInput{
				Name: secretName,
			})
			assert.NoError(err)
		})
	})
}

func TestManagerWithProxyListenForUpdates(t *testing.T) {
	mgr := integrationTestCfg.ProxyOnlyManager(t)
	secretName := testutil.GetRandomSecretName(t)
	defer func() {
		err := mgr.Delete(secretName)
		assert.NoError(t, err)

		err = mgr.StopListeningForUpdates()
		assert.NoError(t, err)
	}()

	const (
		originalSecretValue = "plaintextSecret"
		patchedSecretValue  = "patchedPlaintextSecret"
	)

	err := mgr.Post(&Secret{
		Class:     ClassPublic,
		Name:      secretName,
		Plaintext: []byte(originalSecretValue),
	})
	if err != nil {
		t.Fatal(err)
	}

	err = mgr.ListenForUpdates()
	if err != nil {
		t.Fatal(err)
	}

	t.Run("Get should return secret ", func(t *testing.T) {
		var secret *Secret
		err = backoff.Retry(func() (err error) {
			secret, err = mgr.Get(secretName)
			if secret == nil {
				err = errors.New("no secret found")
			}
			return
		}, backoff.WithMaxTries(backoff.NewConstantBackOff(500*time.Millisecond), 10))

		if assert.NoError(t, err) {
			assert.NotNil(t, secret)
			assert.Equal(t, originalSecretValue, string(secret.Plaintext))
		}
	})

	t.Run("rotate secret", func(t *testing.T) {
		err := mgr.Patch(&PatchInput{
			Name:      secretName,
			Plaintext: []byte(patchedSecretValue),
		})
		require.NoError(t, err)
		// cache should be cleared once the update notification is recieved.
		var cachedSecret *Secret
		err = backoff.Retry(func() (err error) {
			cachedSecret := mgr.cache.Get(secretName)
			if cachedSecret != nil {
				err = errors.New("cached is not cleared")
			}
			return
		}, backoff.WithMaxTries(backoff.NewConstantBackOff(500*time.Millisecond), 30))

		if assert.NoError(t, err) {
			assert.Nil(t, cachedSecret)
		}
	})
}
