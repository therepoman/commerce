// +build integration

package manager

import (
	"testing"

	"code.justin.tv/systems/sandstorm/testutil"
	"github.com/stretchr/testify/assert"
)

func TestManagerDeleteIntegration(t *testing.T) {
	assert := assert.New(t)
	secretValue := "mySecretValue"

	m := integrationTestCfg.Manager(t)

	resetSecret := func(secretName string) {
		err := m.Delete(secretName)
		assert.Nil(err)
	}

	t.Run("manager.delete integration tests", func(t *testing.T) {
		secretName := testutil.GetRandomSecretName(t)
		defer resetSecret(secretName)

		assert.Nil(m.Post(&Secret{
			Name:      secretName,
			Plaintext: []byte(secretValue),
			Class:     ClassCustomer,
		}))

		secret, err := m.Get(secretName)
		if err != nil {
			t.Fatal(err)
		}
		assert.Nil(err)
		assert.Equal([]byte(secretValue), secret.Plaintext)

		t.Run("Delete", func(t *testing.T) {
			t.Run("delete should only set tombstone to true and be recoverable", func(t *testing.T) {
				assert.Nil(m.Delete(secret.Name))

				// GetVersion should throw ErrSecretTombstoned
				secretVersion, err := m.GetVersion(secretName, secret.UpdatedAt)
				assert.Equal(ErrSecretTombstoned, err)
				assert.Nil(secretVersion)

				// Tombstone should be removable
				assert.Nil(m.removeTombstone(secret))
				secretVersion, err = m.GetVersion(secretName, secret.UpdatedAt)
				assert.Nil(err)
				assert.Equal(secret, secretVersion)
			})
		})
	})

	t.Run("manager.DeleteSecret", func(t *testing.T) {
		secretName := testutil.GetRandomSecretName(t)
		defer resetSecret(secretName)

		assert.Nil(m.Post(&Secret{
			Name:      secretName,
			Plaintext: []byte(secretValue),
			Class:     ClassCustomer,
		}))

		secret, err := m.Get(secretName)
		if err != nil {
			t.Fatal(err)
		}
		assert.Nil(err)
		assert.Equal([]byte(secretValue), secret.Plaintext)

		t.Run("should set tombstone and active user", func(t *testing.T) {
			testActionUser := "delete_integration_test_user"
			_, err = m.DeleteSecret(&DeleteSecretInput{
				Name:       secret.Name,
				ActionUser: testActionUser,
			})
			assert.Nil(err)

			ddbSecretAudit, err := getSecretAuditRecord(m, secret)
			assert.Nil(err)
			assert.NotNil(ddbSecretAudit)
			assert.NotEmpty(ddbSecretAudit.ActionUser)
			assert.Equal(testActionUser, ddbSecretAudit.ActionUser)
		})
		t.Run("delete sets the actions User", func(t *testing.T) {
			ddbSecretAudit, err := getSecretAuditRecord(m, secret)
			assert.Nil(err)
			assert.NotNil(ddbSecretAudit)
			assert.NotEmpty(ddbSecretAudit.ActionUser)
		})

	})

}
