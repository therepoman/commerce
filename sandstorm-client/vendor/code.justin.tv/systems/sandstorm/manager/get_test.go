package manager

import (
	"fmt"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestManagerGet(t *testing.T) {
	// manager with mocked dynamo
	t.Run("manager.get unit tests", func(t *testing.T) {

		t.Run("Get", func(t *testing.T) {
			t.Run("should not error and return nil on non-existent key", func(t *testing.T) {
				t.Parallel()
				assert := assert.New(t)

				mt := newManagerTest(t)
				defer mt.teardown(t)

				mt.mockDynamoDB.On("Query", mock.Anything).Return(&dynamodb.QueryOutput{
					Count:            aws.Int64(0),
					Items:            []map[string]*dynamodb.AttributeValue{},
					LastEvaluatedKey: map[string]*dynamodb.AttributeValue{},
				}, nil).Once()

				secret, err := mt.m.Get("nonexistentSecret")
				assert.Nil(err)

				assert.Nil(secret)
			})

			t.Run("should not error out for an existing key", func(t *testing.T) {
				t.Parallel()
				assert := assert.New(t)

				mt := newManagerTest(t)
				defer mt.teardown(t)

				mt.mockInventory.On("UpdateHeartbeat", mock.Anything).Once()

				testDynamoSecret := newTestDynamoSecret()
				secretMap, err := dynamodbattribute.MarshalMap(testDynamoSecret)
				assert.Nil(err)

				mt.mockDynamoDB.On("Query", mock.Anything).Return(&dynamodb.QueryOutput{
					Count:            aws.Int64(1),
					Items:            []map[string]*dynamodb.AttributeValue{secretMap},
					LastEvaluatedKey: map[string]*dynamodb.AttributeValue{},
				}, nil).Once()
				mt.mockEnveloper.On("Open", mock.Anything, mock.Anything, mock.Anything).Return([]byte(testDynamoSecret.Value), nil).Once()

				actualSecretValue, err := mt.m.Get(testDynamoSecret.Name)
				assert.Nil(err)
				if assert.NotNil(actualSecretValue) {
					assert.Equal(testDynamoSecret.Value, string(actualSecretValue.Plaintext))
				}
			})

			t.Run("Update heartbeat", func(t *testing.T) {
				t.Run("do not call update heartbeat if dynamo did not find the secret", func(t *testing.T) {
					t.Parallel()
					assert := assert.New(t)

					secretName := "testSecretName"

					mgrTest := newManagerTest(t)
					defer mgrTest.teardown(t)

					output := &dynamodb.QueryOutput{
						Count: aws.Int64(0),
					}

					mgrTest.mockDynamoDB.On("Query", mock.Anything).Return(output, nil).Once()
					_, err := mgrTest.m.Get(secretName)
					assert.NoError(err)
				})

				t.Run("call update heartbeat for a valid secret", func(t *testing.T) {
					t.Parallel()
					assert := assert.New(t)

					mgrTest := newManagerTest(t)
					defer mgrTest.teardown(t)

					testDynamoSecret := newTestDynamoSecret()
					secretMap, err := dynamodbattribute.MarshalMap(testDynamoSecret)
					assert.NoError(err)

					mgrTest.mockDynamoDB.On("Query", mock.Anything).Return(&dynamodb.QueryOutput{
						Count:            aws.Int64(1),
						Items:            []map[string]*dynamodb.AttributeValue{secretMap},
						LastEvaluatedKey: map[string]*dynamodb.AttributeValue{},
					}, nil).Once()

					mgrTest.mockEnveloper.On("Open", mock.Anything, mock.Anything, mock.Anything).Return([]byte(testDynamoSecret.Value), nil).Once()
					mgrTest.mockInventory.On("UpdateHeartbeat", mock.Anything).Once()
					_, err = mgrTest.m.Get("secretName")
					assert.NoError(err)
				})
			})

			t.Run("should return Permission error on not permitted secret", func(t *testing.T) {
				assert := assert.New(t)
				t.Parallel()
				mt := newManagerTest(t)
				defer mt.teardown(t)
				testDynamoSecret := newTestDynamoSecret()

				mt.mockDynamoDB.On("Query", mock.Anything).Return(&dynamodb.QueryOutput{}, fmt.Errorf("error")).Once()
				_, err := mt.m.Get(testDynamoSecret.Name)
				assert.NotNil(err)
			})

		})

		t.Run("GetVersionsEncrypted", func(t *testing.T) {

			t.Run("should not return any versions for a non-existent key", func(t *testing.T) {
				t.Parallel()
				assert := assert.New(t)

				mt := newManagerTest(t)
				defer mt.teardown(t)
				mt.mockDynamoDB.On("Query", mock.Anything).Return(&dynamodb.QueryOutput{
					Count:            aws.Int64(0),
					Items:            []map[string]*dynamodb.AttributeValue{},
					LastEvaluatedKey: map[string]*dynamodb.AttributeValue{},
				}, nil).Once()

				versions, err := mt.m.GetVersionsEncrypted("nonexistentSecret", 10, 0)
				assert.Nil(err, "err should be nil")

				assert.Empty(versions.Secrets, "should not return any secrets")
			})

			t.Run("should return a single version for an existing key", func(t *testing.T) {
				t.Parallel()
				assert := assert.New(t)

				mt := newManagerTest(t)
				defer mt.teardown(t)
				testDynamoSecret := newTestDynamoSecret()
				secretMap, err := dynamodbattribute.MarshalMap(testDynamoSecret)
				assert.Nil(err, "error marshalling secret")

				mt.mockDynamoDB.On("Query", mock.Anything).Return(&dynamodb.QueryOutput{
					Count:            aws.Int64(1),
					Items:            []map[string]*dynamodb.AttributeValue{secretMap},
					LastEvaluatedKey: map[string]*dynamodb.AttributeValue{},
				}, nil).Once()

				versions, err := mt.m.GetVersionsEncrypted(testDynamoSecret.Name, 10, 0)
				assert.Nil(err, "error getting encrypted versions")

				if assert.NotNil(versions) {
					assert.Len(versions.Secrets, 1)
				}
			})

			t.Run("should not error out on deleted secret", func(t *testing.T) {
				t.Parallel()
				assert := assert.New(t)

				mt := newManagerTest(t)
				defer mt.teardown(t)
				testTombstonedDynamoSecret := newTestDynamoSecret()
				testTombstonedDynamoSecret.Tombstone = true

				secretMap, err := dynamodbattribute.MarshalMap(testTombstonedDynamoSecret)
				assert.Nil(err, "error marshalling secret")

				mt.mockDynamoDB.On("Query", mock.Anything).Return(&dynamodb.QueryOutput{
					Count:            aws.Int64(1),
					Items:            []map[string]*dynamodb.AttributeValue{secretMap},
					LastEvaluatedKey: map[string]*dynamodb.AttributeValue{},
				}, nil).Once()

				versions, err := mt.m.GetVersionsEncrypted(testTombstonedDynamoSecret.Name, 10, 0)
				assert.Nil(err, "error marshalling secret")

				if assert.NotNil(versions) && assert.NotNil(versions.Secrets) {
					assert.Empty(versions.Secrets)
				}
			})

			t.Run("negative version should error and not populate versions", func(t *testing.T) {
				t.Parallel()
				assert := assert.New(t)

				mt := newManagerTest(t)
				defer mt.teardown(t)
				versions, err := mt.m.GetVersionsEncrypted("testSecret", -1, 0)
				if assert.NotNil(err) {
					assert.Equal(ErrNegativeLimit, err)
				}
				assert.Nil(versions)
			})
		})

		t.Run("GetVersion", func(t *testing.T) {

			t.Run("should not error on nonexistent key", func(t *testing.T) {
				t.Parallel()
				assert := assert.New(t)

				mt := newManagerTest(t)
				defer mt.teardown(t)
				mt.mockDynamoDB.On("Query", mock.Anything).Return(&dynamodb.QueryOutput{
					Count:            aws.Int64(0),
					Items:            []map[string]*dynamodb.AttributeValue{},
					LastEvaluatedKey: map[string]*dynamodb.AttributeValue{},
				}, nil).Once()

				actualSecretValue, err := mt.m.GetVersion("nonexistentSecret", 420)
				assert.Nil(err)
				assert.Nil(actualSecretValue)
			})

			t.Run("should not error on non-existing version", func(t *testing.T) {
				t.Parallel()
				assert := assert.New(t)

				mt := newManagerTest(t)
				defer mt.teardown(t)
				mt.mockDynamoDB.On("Query", mock.Anything).Return(&dynamodb.QueryOutput{
					Count:            aws.Int64(0),
					Items:            []map[string]*dynamodb.AttributeValue{},
					LastEvaluatedKey: map[string]*dynamodb.AttributeValue{},
				}, nil).Once()

				actualSecretValue, err := mt.m.GetVersion("secretName", 431)
				assert.Nil(err)
				assert.Nil(actualSecretValue)
			})
		})
	})
}
