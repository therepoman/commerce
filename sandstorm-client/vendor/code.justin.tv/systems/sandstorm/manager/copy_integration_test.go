// +build integration

package manager

import (
	"testing"

	"code.justin.tv/systems/sandstorm/testutil"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestManagerCopySecretIntegration(t *testing.T) {
	secretValue := "mySecretValue"

	m := integrationTestCfg.Manager(t)

	resetSecret := func(secretName string) {
		assert.NoError(t, m.Delete(secretName))
	}

	t.Run("CopySecret", func(t *testing.T) {
		secretName := testutil.GetRandomSecretName(t)
		defer resetSecret(secretName)

		require.NoError(t, m.Post(&Secret{
			Name:      secretName,
			Plaintext: []byte(secretValue),
			Class:     ClassCustomer,
		}))

		secret, err := m.Get(secretName)
		if err != nil {
			t.Fatal(err)
		}
		require.Nil(t, err)
		assert.Equal(t, []byte(secretValue), secret.Plaintext)

		t.Run("CopySecret", func(t *testing.T) {
			t.Run("CopySecret should set actionUser correctly", func(t *testing.T) {
				destinationSecret := testutil.GetRandomSecretName(t)
				actionUser := "test"
				defer resetSecret(destinationSecret)
				_, err := m.CopySecret(&CopySecretInput{
					Source:      secretName,
					Destination: destinationSecret,
					ActionUser:  actionUser,
				})
				if err != nil {
					t.Fatal(err)
				}

				t.Run("Check action user was set correctly in audit table", func(t *testing.T) {
					s, err := m.Get(destinationSecret)
					if err != nil {
						t.Fatal(err)
					}
					ddbSecretAudit, err := getSecretAuditRecord(m, s)
					require.NoError(t, err)
					assert.NotNil(t, ddbSecretAudit)
					assert.NotEmpty(t, ddbSecretAudit.ActionUser)
					assert.Equal(t, actionUser, ddbSecretAudit.ActionUser)
				})
			})
		})
	})
}
