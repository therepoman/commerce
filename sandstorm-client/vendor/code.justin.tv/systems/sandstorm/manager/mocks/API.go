package mocks

import context "context"
import manager "code.justin.tv/systems/sandstorm/manager"
import mock "github.com/stretchr/testify/mock"

// API is an autogenerated mock type for the API type
type API struct {
	mock.Mock
}

// AuditTableName provides a mock function with given fields:
func (_m *API) AuditTableName() string {
	ret := _m.Called()

	var r0 string
	if rf, ok := ret.Get(0).(func() string); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(string)
	}

	return r0
}

// CheckKMS provides a mock function with given fields:
func (_m *API) CheckKMS() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// CheckTable provides a mock function with given fields:
func (_m *API) CheckTable() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// CleanUp provides a mock function with given fields:
func (_m *API) CleanUp() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Copy provides a mock function with given fields: source, destination
func (_m *API) Copy(source string, destination string) error {
	ret := _m.Called(source, destination)

	var r0 error
	if rf, ok := ret.Get(0).(func(string, string) error); ok {
		r0 = rf(source, destination)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// CopySecret provides a mock function with given fields: input
func (_m *API) CopySecret(input *manager.CopySecretInput) (*manager.CopySecretOutput, error) {
	ret := _m.Called(input)

	var r0 *manager.CopySecretOutput
	if rf, ok := ret.Get(0).(func(*manager.CopySecretInput) *manager.CopySecretOutput); ok {
		r0 = rf(input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*manager.CopySecretOutput)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(*manager.CopySecretInput) error); ok {
		r1 = rf(input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// CreateTable provides a mock function with given fields:
func (_m *API) CreateTable() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// CrossEnvironmentSecretsSet provides a mock function with given fields: secretNames
func (_m *API) CrossEnvironmentSecretsSet(secretNames []string) (map[string]struct{}, error) {
	ret := _m.Called(secretNames)

	var r0 map[string]struct{}
	if rf, ok := ret.Get(0).(func([]string) map[string]struct{}); ok {
		r0 = rf(secretNames)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(map[string]struct{})
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func([]string) error); ok {
		r1 = rf(secretNames)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Decrypt provides a mock function with given fields: secret
func (_m *API) Decrypt(secret *manager.Secret) error {
	ret := _m.Called(secret)

	var r0 error
	if rf, ok := ret.Get(0).(func(*manager.Secret) error); ok {
		r0 = rf(secret)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Delete provides a mock function with given fields: secretName
func (_m *API) Delete(secretName string) error {
	ret := _m.Called(secretName)

	var r0 error
	if rf, ok := ret.Get(0).(func(string) error); ok {
		r0 = rf(secretName)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// DeleteSecret provides a mock function with given fields: input
func (_m *API) DeleteSecret(input *manager.DeleteSecretInput) (*manager.DeleteSecretOutput, error) {
	ret := _m.Called(input)

	var r0 *manager.DeleteSecretOutput
	if rf, ok := ret.Get(0).(func(*manager.DeleteSecretInput) *manager.DeleteSecretOutput); ok {
		r0 = rf(input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*manager.DeleteSecretOutput)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(*manager.DeleteSecretInput) error); ok {
		r1 = rf(input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Exist provides a mock function with given fields: secretName
func (_m *API) Exist(secretName string) (bool, error) {
	ret := _m.Called(secretName)

	var r0 bool
	if rf, ok := ret.Get(0).(func(string) bool); ok {
		r0 = rf(secretName)
	} else {
		r0 = ret.Get(0).(bool)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(secretName)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// FlushHeartbeat provides a mock function with given fields: ctx
func (_m *API) FlushHeartbeat(ctx context.Context) {
	_m.Called(ctx)
}

// Get provides a mock function with given fields: secretName
func (_m *API) Get(secretName string) (*manager.Secret, error) {
	ret := _m.Called(secretName)

	var r0 *manager.Secret
	if rf, ok := ret.Get(0).(func(string) *manager.Secret); ok {
		r0 = rf(secretName)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*manager.Secret)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(secretName)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetEncrypted provides a mock function with given fields: secretName
func (_m *API) GetEncrypted(secretName string) (*manager.Secret, error) {
	ret := _m.Called(secretName)

	var r0 *manager.Secret
	if rf, ok := ret.Get(0).(func(string) *manager.Secret); ok {
		r0 = rf(secretName)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*manager.Secret)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(secretName)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetVersion provides a mock function with given fields: secretName, version
func (_m *API) GetVersion(secretName string, version int64) (*manager.Secret, error) {
	ret := _m.Called(secretName, version)

	var r0 *manager.Secret
	if rf, ok := ret.Get(0).(func(string, int64) *manager.Secret); ok {
		r0 = rf(secretName, version)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*manager.Secret)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, int64) error); ok {
		r1 = rf(secretName, version)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetVersionsEncrypted provides a mock function with given fields: secretName, limit, offsetKey
func (_m *API) GetVersionsEncrypted(secretName string, limit int64, offsetKey int64) (*manager.VersionsPage, error) {
	ret := _m.Called(secretName, limit, offsetKey)

	var r0 *manager.VersionsPage
	if rf, ok := ret.Get(0).(func(string, int64, int64) *manager.VersionsPage); ok {
		r0 = rf(secretName, limit, offsetKey)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*manager.VersionsPage)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, int64, int64) error); ok {
		r1 = rf(secretName, limit, offsetKey)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// List provides a mock function with given fields:
func (_m *API) List() ([]*manager.Secret, error) {
	ret := _m.Called()

	var r0 []*manager.Secret
	if rf, ok := ret.Get(0).(func() []*manager.Secret); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*manager.Secret)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func() error); ok {
		r1 = rf()
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ListNamespace provides a mock function with given fields: namespace
func (_m *API) ListNamespace(namespace string) ([]*manager.Secret, error) {
	ret := _m.Called(namespace)

	var r0 []*manager.Secret
	if rf, ok := ret.Get(0).(func(string) []*manager.Secret); ok {
		r0 = rf(namespace)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*manager.Secret)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(namespace)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ListNamespaces provides a mock function with given fields:
func (_m *API) ListNamespaces() ([]string, error) {
	ret := _m.Called()

	var r0 []string
	if rf, ok := ret.Get(0).(func() []string); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]string)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func() error); ok {
		r1 = rf()
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ListenForUpdates provides a mock function with given fields:
func (_m *API) ListenForUpdates() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// NamespaceTableName provides a mock function with given fields:
func (_m *API) NamespaceTableName() string {
	ret := _m.Called()

	var r0 string
	if rf, ok := ret.Get(0).(func() string); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(string)
	}

	return r0
}

// Patch provides a mock function with given fields: patchInput
func (_m *API) Patch(patchInput *manager.PatchInput) error {
	ret := _m.Called(patchInput)

	var r0 error
	if rf, ok := ret.Get(0).(func(*manager.PatchInput) error); ok {
		r0 = rf(patchInput)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Post provides a mock function with given fields: secret
func (_m *API) Post(secret *manager.Secret) error {
	ret := _m.Called(secret)

	var r0 error
	if rf, ok := ret.Get(0).(func(*manager.Secret) error); ok {
		r0 = rf(secret)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Put provides a mock function with given fields: secret
func (_m *API) Put(secret *manager.Secret) error {
	ret := _m.Called(secret)

	var r0 error
	if rf, ok := ret.Get(0).(func(*manager.Secret) error); ok {
		r0 = rf(secret)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// RegisterSecretUpdateCallback provides a mock function with given fields: secretName, callback
func (_m *API) RegisterSecretUpdateCallback(secretName string, callback func(manager.SecretChangeset)) {
	_m.Called(secretName, callback)
}

// Revert provides a mock function with given fields: name, version
func (_m *API) Revert(name string, version int64) error {
	ret := _m.Called(name, version)

	var r0 error
	if rf, ok := ret.Get(0).(func(string, int64) error); ok {
		r0 = rf(name, version)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// RevertSecret provides a mock function with given fields: input
func (_m *API) RevertSecret(input *manager.RevertSecretInput) (*manager.RevertSecretOutput, error) {
	ret := _m.Called(input)

	var r0 *manager.RevertSecretOutput
	if rf, ok := ret.Get(0).(func(*manager.RevertSecretInput) *manager.RevertSecretOutput); ok {
		r0 = rf(input)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*manager.RevertSecretOutput)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(*manager.RevertSecretInput) error); ok {
		r1 = rf(input)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Seal provides a mock function with given fields: secret
func (_m *API) Seal(secret *manager.Secret) error {
	ret := _m.Called(secret)

	var r0 error
	if rf, ok := ret.Get(0).(func(*manager.Secret) error); ok {
		r0 = rf(secret)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// StopListeningForUpdates provides a mock function with given fields:
func (_m *API) StopListeningForUpdates() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// TableName provides a mock function with given fields:
func (_m *API) TableName() string {
	ret := _m.Called()

	var r0 string
	if rf, ok := ret.Get(0).(func() string); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(string)
	}

	return r0
}
