package manager

import (
	"errors"
	"fmt"
	"net/http"
	"testing"
	"time"

	"code.justin.tv/systems/sandstorm/mocks"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

type roundTripperTest struct {
	wrt             *wrappedRoundTripper
	mockedDefaultRT *mocks.RoundTripper
	mockedProxyRT   *mocks.RoundTripper
}

type testNetError struct {
	timeout   bool
	temporary bool
}

func (e *testNetError) Error() string   { return fmt.Sprintf("Test network error") }
func (e *testNetError) Temporary() bool { return e.timeout }
func (e *testNetError) Timeout() bool   { return e.temporary }

func newRoundTripperTest(t *testing.T) (rtt *roundTripperTest, mockedDefaultRT, mockedProxyRT *mocks.RoundTripper) {

	mockedDefaultRT = new(mocks.RoundTripper)
	mockedProxyRT = new(mocks.RoundTripper)

	rtt = &roundTripperTest{
		wrt: &wrappedRoundTripper{
			Pop:      "test",
			ProxyURL: "http://proxy.internal.justin.tv:9797/",
		},
		mockedDefaultRT: mockedDefaultRT,
		mockedProxyRT:   mockedProxyRT,
	}
	err := rtt.wrt.init()
	if err != nil {
		t.Fatal(err)
	}
	rtt.wrt.defaultRT = mockedDefaultRT
	rtt.wrt.proxyRT = mockedProxyRT
	return
}

func TestRoundTripper(t *testing.T) {
	req, err := http.NewRequest("GET", "http://example.com", nil)
	assert.NoError(t, err)
	netError := &testNetError{
		temporary: false,
	}

	t.Run("Default", func(t *testing.T) {

		rtt, mockedDefaultRT, _ := newRoundTripperTest(t)
		assert := assert.New(t)

		defer func() {
			mockedDefaultRT.AssertExpectations(t)
		}()

		require.False(t, rtt.wrt.proxyInUse.IsSet())
		mockedDefaultRT.On("RoundTrip", mock.Anything).Return(&http.Response{StatusCode: http.StatusOK}, nil).Once()
		_, err = rtt.wrt.RoundTrip(req)
		assert.NoError(err)
		require.False(t, rtt.wrt.proxyInUse.IsSet())
		assert.NotEqual(rtt.wrt.roundTripper(), rtt.wrt.proxyRT)
	})

	t.Run("Switch to proxy", func(t *testing.T) {
		t.Run("network error", func(t *testing.T) {
			rtt, mockedDefaultRT, mockedProxyRT := newRoundTripperTest(t)

			defer func() {
				mockedDefaultRT.AssertExpectations(t)
				mockedProxyRT.AssertExpectations(t)
			}()

			assert := assert.New(t)
			mockedDefaultRT.On("RoundTrip", mock.Anything).Return(nil, netError).Once()

			assert.False(rtt.wrt.proxyInUse.IsSet())
			assert.Equal(rtt.wrt.roundTripper(), mockedDefaultRT)
			_, err := rtt.wrt.RoundTrip(req)
			if assert.Error(err) {
				assert.True(rtt.wrt.proxyInUse.IsSet())
				assert.Equal(rtt.wrt.roundTripper(), mockedProxyRT)
			}
		})
	})

	t.Run("Switch back to default after defined time", func(t *testing.T) {
		assert := assert.New(t)
		rtt, mockedDefaultRT, mockedProxyRT := newRoundTripperTest(t)
		rtt.wrt.proxyDuration = 1 * time.Millisecond

		defer func() {
			mockedDefaultRT.AssertExpectations(t)
			mockedProxyRT.AssertExpectations(t)
		}()

		assert.Equal(rtt.wrt.roundTripper(), rtt.wrt.defaultRT)
		mockedDefaultRT.On("RoundTrip", mock.Anything).Return(nil, netError).Once()
		_, err = rtt.wrt.RoundTrip(req)
		require.Error(t, err)

		require.True(t, rtt.wrt.proxyInUse.IsSet())
		mockedProxyRT.On("RoundTrip", mock.Anything).Return(&http.Response{StatusCode: http.StatusOK}, nil).Once()
		_, err = rtt.wrt.RoundTrip(req)
		require.NoError(t, err)

		for i := 0; i < 5; i++ {
			err = nil
			if rtt.wrt.proxyInUse.IsSet() {
				err = errors.New("proxy is still being used")
			}
			if err == nil {
				break
			}
			time.Sleep(100 * time.Millisecond)
		}

		assert.Nil(err)
		assert.Equal(rtt.wrt.roundTripper(), rtt.wrt.defaultRT)
	})

}
