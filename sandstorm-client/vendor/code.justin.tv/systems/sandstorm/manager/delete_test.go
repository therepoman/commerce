package manager

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDeleteSecretInputValidate(t *testing.T) {

	t.Run("validate", func(t *testing.T) {
		assert := assert.New(t)
		t.Run("name is empty", func(t *testing.T) {
			input := &DeleteSecretInput{}
			err := input.validate()
			assert.IsType(&InputValidationError{}, err)
		})
		t.Run("valid input", func(t *testing.T) {
			input := &DeleteSecretInput{
				Name: "test",
			}
			err := input.validate()
			assert.Nil(err)

			input.ActionUser = "testUser"
			err = input.validate()
			assert.Nil(err)
		})
	})
}
