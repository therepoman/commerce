package manager

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCopySecretInputValidate(t *testing.T) {

	t.Run("validate", func(t *testing.T) {
		assert := assert.New(t)
		t.Run("destination is empty", func(t *testing.T) {
			input := &CopySecretInput{
				Source: "test",
			}
			err := input.validate()
			assert.IsType(&InputValidationError{}, err)
		})
		t.Run("source is empty", func(t *testing.T) {
			input := &CopySecretInput{
				Destination: "test",
			}
			err := input.validate()
			assert.IsType(&InputValidationError{}, err)
		})
		t.Run("valid input", func(t *testing.T) {
			input := &CopySecretInput{
				Source:      "test",
				Destination: "test2",
			}
			err := input.validate()
			assert.Nil(err)

			input.ActionUser = "testUser"
			err = input.validate()
			assert.Nil(err)
		})
	})
}
