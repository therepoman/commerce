package manager

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestClearSecretCallbacks(t *testing.T) {
	mt := newManagerTest(t)
	defer mt.teardown(t)

	mt.m.RegisterSecretUpdateCallback("my-secret-name", func(SecretChangeset) {})

	mt.m.ClearSecretCallbacks()

	assert.Empty(t, mt.m.secretCallbacks)
}

func TestRegisterSecretUpdateCallback(t *testing.T) {
	const testSecretName = "my-secret-name"

	cb := func(input SecretChangeset) {
	}

	t.Run("no existing callbacks", func(t *testing.T) {
		mt := newManagerTest(t)
		defer mt.teardown(t)

		mt.m.RegisterSecretUpdateCallback(testSecretName, cb)

		assert.Contains(t, mt.m.secretCallbacks, testSecretName)
		assert.Len(t, mt.m.secretCallbacks, 1)
		assert.Len(t, mt.m.secretCallbacks[testSecretName], 1)
	})

	t.Run("one existing callbacks", func(t *testing.T) {
		mt := newManagerTest(t)
		defer mt.teardown(t)

		mt.m.RegisterSecretUpdateCallback(testSecretName, cb)

		mt.m.RegisterSecretUpdateCallback(testSecretName, cb)

		assert.Contains(t, mt.m.secretCallbacks, testSecretName)
		assert.Len(t, mt.m.secretCallbacks, 1)
		assert.Len(t, mt.m.secretCallbacks[testSecretName], 2)
	})
}

func TestTriggerSecretUpdateCallbacks(t *testing.T) {
	const testSecretName = "my-secret-name"

	t.Run("no callbacks", func(t *testing.T) {
		mt := newManagerTest(t)
		defer mt.teardown(t)

		mt.m.triggerSecretUpdateCallbacks(testSecretName, 1234566)
	})

	t.Run("no callbacks match", func(t *testing.T) {
		mt := newManagerTest(t)
		defer mt.teardown(t)

		calls := []SecretChangeset{}
		cb := func(input SecretChangeset) {
			calls = append(calls, input)
		}

		mt.m.RegisterSecretUpdateCallback("something-else", cb)

		mt.m.triggerSecretUpdateCallbacks(testSecretName, 1234566)

		assert.Equal(t, []SecretChangeset{}, calls)
	})

	t.Run("one callback matches", func(t *testing.T) {
		mt := newManagerTest(t)
		defer mt.teardown(t)

		calls := []SecretChangeset{}
		cb := func(input SecretChangeset) {
			calls = append(calls, input)
		}

		mt.m.RegisterSecretUpdateCallback(testSecretName, cb)

		updatedAt := time.Now().Unix()
		mt.m.triggerSecretUpdateCallbacks(testSecretName, updatedAt)

		triggeredCall := calls[0]
		assert.True(t, triggeredCall.SecretsHaveChanged())
		assert.True(t, triggeredCall.SecretHasBeenUpdated(testSecretName))
		assert.Equal(t, triggeredCall.GetSecretUpdatedAt(testSecretName), updatedAt)

	})
}
