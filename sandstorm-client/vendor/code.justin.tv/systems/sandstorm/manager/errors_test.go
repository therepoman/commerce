package manager

import (
	"errors"
	"fmt"
	"testing"

	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/stretchr/testify/assert"
)

func TestParseGetError(t *testing.T) {
	t.Run("Should return custom error when 'AccessDeniedException' is encountered", func(t *testing.T) {
		myErr := awserr.New("AccessDeniedException", "random message", errors.New("my error"))
		secretName := "my/secret/thing/secret1"

		err := parseGetError(secretName, myErr)
		expectedErr := fmt.Errorf(
			"Failed to fetch secret %s. Error was: The role you specified cannot fetch %s",
			secretName,
			secretName)

		assert.Equal(t, expectedErr, err)
	})

	t.Run("Should return error when 'AccessDeniedException' is not encountered", func(t *testing.T) {
		myErr := awserr.New("AccessDenied", "random message", errors.New("my error"))
		secretName := "my/secret/thing/secret1"

		err := parseGetError(secretName, myErr)
		expectedErr := fmt.Errorf("Failed to fetch secret %s. Error was: %s", secretName, myErr.Error())

		assert.Equal(t, expectedErr, err)
	})
}
