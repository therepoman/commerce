// +build integration

package manager

import (
	"testing"
	"time"

	"code.justin.tv/systems/sandstorm/testutil"

	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestManagerPatchIntegration(t *testing.T) {
	m := integrationTestCfg.Manager(t)

	t.Run("patch secret in stages", func(t *testing.T) {
		const originalSecretValue = "myPlaintext"
		const patchedSecretValue = "myPatchedPlaintext"

		secretName := testutil.GetRandomSecretName(t)
		defer func() {
			err := m.Delete(secretName)
			assert.NoError(t, err)
		}()

		assertCurrentSecret := func(t *testing.T, s *Secret) {
			currentSecret, err := m.Get(secretName)
			assert.NoError(t, err)

			// zero out other fields for comparison
			currentSecret.key = nil
			currentSecret.ciphertext = nil
			assert.Equal(t, s, currentSecret)
		}

		err := m.Post(&Secret{
			Class:     ClassPublic,
			Name:      secretName,
			Plaintext: []byte(originalSecretValue),
		})
		assert.NoError(t, err)

		// update KeyARN and UpdatedAt for test
		currentSecret, err := m.Get(secretName)
		assert.NoError(t, err)
		keyARN := currentSecret.KeyARN
		updatedAt := currentSecret.UpdatedAt
		getOriginalSecret := func() *Secret {
			return &Secret{
				Class:     ClassPublic,
				Name:      secretName,
				Plaintext: []byte(originalSecretValue),
				KeyARN:    keyARN,
				UpdatedAt: updatedAt,
			}
		}

		t.Run("check first value", func(t *testing.T) {
			assertCurrentSecret(t, getOriginalSecret())
		})

		t.Run("patch noop", func(t *testing.T) {
			assert := assert.New(t)

			err := m.Patch(&PatchInput{
				Name: secretName,
			})
			assert.NoError(err)

			assertCurrentSecret(t, getOriginalSecret())
		})

		t.Run("update DoNotBroadcast", func(t *testing.T) {
			assert := assert.New(t)

			dnb := true
			err := m.Patch(&PatchInput{
				Name:           secretName,
				DoNotBroadcast: &dnb,
			})
			assert.NoError(err)

			expectedSecret := getOriginalSecret()
			expectedSecret.DoNotBroadcast = true
			assertCurrentSecret(t, expectedSecret)
		})

		t.Run("update Class", func(t *testing.T) {
			assert := assert.New(t)

			cls := ClassCustomer
			err := m.Patch(&PatchInput{
				Name:  secretName,
				Class: &cls,
			})
			assert.NoError(err)

			expectedSecret := getOriginalSecret()
			expectedSecret.DoNotBroadcast = true
			expectedSecret.Class = ClassCustomer
			assertCurrentSecret(t, expectedSecret)
		})

		t.Run("update CrossEnv", func(t *testing.T) {
			assert := assert.New(t)

			crossEnv := true
			err := m.Patch(&PatchInput{
				Name:     secretName,
				CrossEnv: &crossEnv,
			})
			assert.NoError(err)

			expectedSecret := getOriginalSecret()
			expectedSecret.DoNotBroadcast = true
			expectedSecret.Class = ClassCustomer
			expectedSecret.CrossEnv = true
			assertCurrentSecret(t, expectedSecret)
		})

		t.Run("update Plaintext", func(t *testing.T) {
			assert := assert.New(t)

			// versions have a 1 second delta
			time.Sleep(time.Second)
			err := m.Patch(&PatchInput{
				Name:      secretName,
				Plaintext: []byte(patchedSecretValue),
			})
			assert.NoError(err)

			currentSecret, err := m.Get(secretName)
			require.NoError(t, err)
			assert.True(currentSecret.UpdatedAt > updatedAt)

			expectedSecret := getOriginalSecret()
			expectedSecret.Plaintext = []byte(patchedSecretValue)
			expectedSecret.DoNotBroadcast = true
			expectedSecret.Class = ClassCustomer
			expectedSecret.CrossEnv = true
			expectedSecret.UpdatedAt = currentSecret.UpdatedAt
			assertCurrentSecret(t, expectedSecret)
		})
	})
}

func TestManagerPutAuditIntegration(t *testing.T) { // manager with real dynamo
	assert := assert.New(t)
	m := integrationTestCfg.Manager(t)

	secretValue := "mysecret"

	resetSecret := func(secretName string) {
		err := m.Delete(secretName)
		assert.Nil(err)
	}

	t.Run("Invalid Secret Name", func(t *testing.T) {
		t.Run("Does not create secret with invalid name", func(t *testing.T) {
			secretName := "this/is/an/invalid$ecret"
			secretValue := "some secret"
			err := m.Post(&Secret{
				Name:      secretName,
				Plaintext: []byte(secretValue),
			})
			assert.NotNil(err)
			assert.Equal(err, errInvalidSecretName)
		})
	})

	t.Run("Audit table", func(t *testing.T) {
		t.Run("Secret created with no action user", func(t *testing.T) {
			secretName := testutil.GetRandomSecretName(t)
			defer resetSecret(secretName)

			defer func() {
				assert.NoError(m.inventoryClient.Stop())
			}()

			err := m.Post(&Secret{
				Name:      secretName,
				Plaintext: []byte(secretValue),
			})
			if err != nil {
				t.Fatal(err)
			}

			secret, err := m.Get(secretName)
			assert.Nil(err)
			assert.NotNil(secret)

			ddbSecretAudit, err := getSecretAuditRecord(m, secret)
			assert.Nil(err)
			assert.NotNil(ddbSecretAudit)
			assert.NotEmpty(ddbSecretAudit.ActionUser)

			t.Run("Patch with no action user, uses aws caller identity", func(t *testing.T) {
				err := m.Patch(
					&PatchInput{
						Name:      secretName,
						Plaintext: []byte("updatedSecret"),
					},
				)
				secret, err := m.Get(secretName)
				assert.Nil(err)
				assert.NotNil(secret)

				ddbSecretAudit, err = getSecretAuditRecord(m, secret)
				assert.Nil(err)
				assert.NotNil(secret)
				assert.NotEmpty(ddbSecretAudit.ActionUser)
			})

			t.Run("Patch with action user updates the audit table", func(t *testing.T) {
				patchActionUser := "patchActionUser"
				m.Config.ActionUser = patchActionUser
				err := m.Patch(
					&PatchInput{
						Name:      secretName,
						Plaintext: []byte("updatedSecret"),
					},
				)
				secret, err := m.Get(secretName)
				assert.Nil(err)
				assert.NotNil(secret)

				ddbSecretAudit, err = getSecretAuditRecord(m, secret)
				assert.Nil(err)
				assert.NotNil(secret)
				assert.Equal(patchActionUser, ddbSecretAudit.ActionUser)
			})
		})

		t.Run("Secret created with action user", func(t *testing.T) {
			actionUser := "postActionUser"
			m.Config.ActionUser = actionUser
			secretName := testutil.GetRandomSecretName(t)
			defer resetSecret(secretName)

			defer func() {
				assert.NoError(m.inventoryClient.Stop())
			}()
			err := m.Post(&Secret{
				Name:      secretName,
				Plaintext: []byte(secretValue),
			})

			if err != nil {
				t.Fatal(err)
			}

			secret, err := m.Get(secretName)
			assert.Nil(err)
			assert.NotNil(secret)

			ddbSecretAudit, err := getSecretAuditRecord(m, secret)
			assert.Nil(err)
			assert.NotNil(secret)
			assert.Equal(actionUser, ddbSecretAudit.ActionUser)
		})

	})

}

func getSecretAuditRecord(m *Manager, secret *Secret) (ddbSecretAudit dynamoSecretAudit, err error) {

	inputForGetVersion := m.inputForGetVersion(secret.Name, secret.UpdatedAt)
	out, err := m.dynamoDB().Query(inputForGetVersion)
	if err != nil {
		return
	}
	for _, item := range out.Items {
		err = dynamodbattribute.UnmarshalMap(item, &ddbSecretAudit)
	}
	return
}
