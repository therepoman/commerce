package manager

import (
	"context"
	"strconv"
	"testing"

	invMocks "code.justin.tv/systems/sandstorm/inventory/heartbeat/mocks"
	"code.justin.tv/systems/sandstorm/logging"
	"code.justin.tv/systems/sandstorm/mocks"
	"code.justin.tv/systems/sandstorm/pkg/envelope/envelopemocks"
	"code.justin.tv/systems/sandstorm/pkg/stat"
	"code.justin.tv/systems/sandstorm/queue"
	queueMocks "code.justin.tv/systems/sandstorm/queue/mocks"
	"code.justin.tv/systems/sandstorm/resource"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/kms/kmsiface"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

const testSecretName = "testSecretName"

type DummyStack struct{}

func (s DummyStack) AdminRoleARN() string                   { return "dummy-admin-role-arn" }
func (s DummyStack) AccountID() string                      { return "dummy-account-id" }
func (s DummyStack) AgentTestingRoleARN() string            { return "dummy-testing-role-arn" }
func (s DummyStack) AWSRegion() string                      { return "dummy-aws-region" }
func (s DummyStack) AuditTableName() string                 { return "dummy-audit-table-name" }
func (s DummyStack) CloudwatchRoleARN() string              { return "dummy-cloudwatch-role-arn" }
func (s DummyStack) Environment() string                    { return "dummy-environment" }
func (s DummyStack) HeartbeatsTableName() string            { return "dummy-heartbeats-table-name" }
func (s DummyStack) InventoryAdminRoleARN() string          { return "dummy-inventory-admin-role-arn" }
func (s DummyStack) InventoryRoleARN() string               { return "dummy-inventory-role-arn" }
func (s DummyStack) InventoryStatusURL() string             { return "dummy-inventory-status-url" }
func (s DummyStack) NamespaceTableName() string             { return "dummy-namespace-table-name" }
func (s DummyStack) PutHeartbeatLambdaFunctionName() string { return "dummy-phlf-name" }
func (s DummyStack) ProxyURL() string                       { return "dummy-proxy-url" }
func (s DummyStack) SecretsQueueNamePrefix() string         { return "dummy-secrets-queue-name-prefix" }
func (s DummyStack) SecretsTableName() string               { return "dummy-secrets-table-name" }
func (s DummyStack) SecretsTopicArn() string                { return "dummy-secrets-topic-arn" }
func (s DummyStack) KMSPrimaryKey() resource.KMSKey {
	return resource.KMSKey{Region: "kr", KeyARN: "ka"}
}

type managerTest struct {
	m             *Manager
	mockDynamoDB  *mocks.DynamoDBAPI
	mockInventory *invMocks.API
	mockedQueue   *queueMocks.Queuer
	mockEnveloper *envelopemocks.Enveloper
	stack         resource.Stack
}

func (mt *managerTest) teardown(t *testing.T) {
	assert.True(t, mt.mockDynamoDB.AssertExpectations(t))
	assert.True(t, mt.mockEnveloper.AssertExpectations(t))
	assert.True(t, mt.mockInventory.AssertExpectations(t))
	assert.True(t, mt.mockedQueue.AssertExpectations(t))
}

func (mt *managerTest) MockCleanUpSecretRows(secretName string) {
	mt.mockDynamoDB.On("UpdateItemWithContext", mock.Anything, &dynamodb.UpdateItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"name": {S: aws.String(secretName)},
		},
		TableName:        aws.String(mt.stack.SecretsTableName()),
		UpdateExpression: aws.String("REMOVE encrypted_data_keys"),
	}).Return(nil, nil).Once()
}

func newManagerTest(t *testing.T) *managerTest {
	ddb := new(mocks.DynamoDBAPI)
	q := new(queueMocks.Queuer)
	mockedInventory := new(invMocks.API)
	mockedKMS := new(mocks.KMSAPI)
	mockEnveloper := new(envelopemocks.Enveloper)
	stack := DummyStack{}
	m := &Manager{
		statter:                 stat.NoopClient{},
		inventoryClient:         mockedInventory,
		cache:                   &cache{},
		queue:                   q,
		Logger:                  &logging.NoopLogger{},
		stopListeningForUpdates: make(chan struct{}),
		Config: Config{
			Stack:       stack,
			ServiceName: "unknown-service",
		},
		secretCallbacks: make(map[string][]func(input SecretChangeset)),
		clientLoader: &clientLoader{
			dynamoDBs: map[string]dynamodbiface.DynamoDBAPI{
				stack.AWSRegion(): ddb,
			},
			kmsClients: map[string]kmsiface.KMSAPI{
				stack.AWSRegion(): mockedKMS,
			},
		},
		enveloper: mockEnveloper,
	}
	return &managerTest{
		m:             m,
		mockDynamoDB:  ddb,
		mockInventory: mockedInventory,
		mockedQueue:   q,
		mockEnveloper: mockEnveloper,
		stack:         stack,
	}
}

func TestListenForUpdates(t *testing.T) {
	testSecret := newTestSecret()

	t.Run("Deletes the entry from cache and triggers callbacks if update was recieved.", func(t *testing.T) {
		mgr := newManagerTest(t)
		defer mgr.teardown(t)
		mgr.m.cache = &cache{
			secretMap: make(map[string]*Secret),
		}

		calls := []SecretChangeset{}
		cb := func(input SecretChangeset) {
			calls = append(calls, input)
		}

		mgr.m.RegisterSecretUpdateCallback(testSecret.Name, cb)

		mgr.m.cache.Set(testSecret.Name, testSecret)
		assert.NotNil(t, mgr.m.cache.Get(testSecret.Name))

		queueSecret := &queue.Secret{
			UpdatedAt: struct{ S string }{
				S: strconv.FormatInt(testSecret.UpdatedAt, 10),
			},
			Name: struct{ S string }{
				S: testSecret.Name,
			},
		}
		mgr.mockedQueue.On("PollForSecret", mock.Anything).Return(queueSecret, nil).Once()
		err := mgr.m.listenForSecretUpdates()
		require.NoError(t, err)
		assert.Nil(t, mgr.m.cache.Get(testSecret.Name))
		assert.Equal(t, 1, len(calls))
		assert.True(t, calls[0].SecretHasBeenUpdated(testSecret.Name), "callback was called once")
	})

	t.Run("Does not delete the entry from cache if update was recieved.", func(t *testing.T) {
		mgr := newManagerTest(t)
		defer mgr.teardown(t)
		mgr.m.cache = &cache{
			secretMap: make(map[string]*Secret),
		}

		mgr.m.cache.Set(testSecret.Name, testSecret)
		assert.NotNil(t, mgr.m.cache.Get(testSecret.Name))

		mgr.mockedQueue.On("PollForSecret", mock.Anything).Return(nil, nil).Once()
		err := mgr.m.listenForSecretUpdates()
		assert.Nil(t, err)
		assert.NotNil(t, mgr.m.cache.Get(testSecret.Name))
		mgr.m.cache.Delete(testSecret.Name)
	})

	t.Run("ListenForUpdatesInitializes the cache", func(t *testing.T) {
		mgr := newManagerTest(t)

		mgr.mockedQueue.On("PollForSecret", mock.Anything).Return(nil, nil)
		mgr.mockedQueue.On("Setup").Return(nil)
		defer mgr.teardown(t)

		err := mgr.m.ListenForUpdates()
		assert.NoError(t, err)
		assert.NotNil(t, mgr.m.cache.secretMap)

		mgr.mockedQueue.On("Delete").Return(nil).Once()
		err = mgr.m.StopListeningForUpdates()
		assert.NoError(t, err)
		assert.Nil(t, mgr.m.cache.secretMap)

		// there is a race condition where this might be called. we call this
		// manually so the call assertion doesn't fail. There is a .Maybe() but not
		// in this version of testify/mock.
		_, err = mgr.mockedQueue.PollForSecret(context.Background())
		assert.NoError(t, err)
	})
}

func TestConfig(t *testing.T) {

	defEnv := "production"
	expectedProdConfig := Config{
		Environment: defEnv,
		AWSConfig: &aws.Config{
			EndpointResolver:    resource.AWSEndpointResolver{},
			Region:              aws.String("us-west-2"),
			STSRegionalEndpoint: endpoints.RegionalSTSEndpoint,
		},
		Logger: &logging.NoopLogger{},
		Queue: queue.Config{
			QueueNamePrefix: defaultQueueNamePrefix,
		},
	}

	t.Run("returns prod configs for default", func(t *testing.T) {
		config := DefaultConfig()
		assert.Equal(t, expectedProdConfig, config)
	})

	t.Run("testing default config", func(t *testing.T) {
		testEnv := "testing"
		expectedTestingConfig := Config{
			Environment: testEnv,
			AWSConfig:   &aws.Config{Region: aws.String("us-west-2")},
			Logger:      &logging.NoopLogger{},
			Queue:       queue.Config{},
		}
		testingConfig := getDefaultConfigByEnvironment("testing")
		assert.NotEqual(t, expectedTestingConfig, testingConfig)
	})

	t.Run("instance id is set to provided uuid", func(t *testing.T) {
		iID, err := uuid.NewV4()
		require.NoError(t, err)
		config := Config{
			InstanceID: iID.String(),
		}

		require.NoError(t, config.fillDefaults(DefaultConfig()))
		assert.Equal(t, iID.String(), config.InstanceID)
	})

	t.Run("defaults are filled", func(t *testing.T) {
		config := Config{}
		require.NoError(t, config.fillDefaults(DefaultConfig()))
		assert.NotEmpty(t, config.InstanceID)
		assert.NotEmpty(t, config.AWSConfig)
		assert.Equal(t, &logging.NoopLogger{}, config.Logger)
		assert.NotEmpty(t, config.ActionUser)
		assert.NotEmpty(t, config.Queue)
	})
}
