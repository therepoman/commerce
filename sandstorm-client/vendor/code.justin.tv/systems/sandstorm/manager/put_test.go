package manager

import (
	"errors"
	"testing"

	"code.justin.tv/systems/sandstorm/pkg/envelope"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestPatchInput(t *testing.T) {
	t.Run("Validate", func(t *testing.T) {
		t.Run("should return error if both autogenerate and plaintext are sent", func(t *testing.T) {
			assert := assert.New(t)

			p := &PatchInput{
				Autogenerate: &FillPlaintextRequest{},
				Plaintext:    []byte("some plaintext"),
			}
			err := p.Validate()

			assert.Equal(ErrAutogenerateAndPlaintextBothSent, err)
		})

		t.Run("should validate autogenerate if sent", func(t *testing.T) {
			assert := assert.New(t)

			p := &PatchInput{
				Autogenerate: &FillPlaintextRequest{
					Length: -1,
				},
			}
			err := p.Validate()

			assert.Error(err)
		})

		t.Run("validate should only run once", func(t *testing.T) {
			assert := assert.New(t)

			p := &PatchInput{
				// bad case when both autogenerate and plaintext are sent
				Autogenerate: &FillPlaintextRequest{},
				Plaintext:    []byte("some plaintext"),
				validated:    true,
			}
			err := p.Validate()

			assert.NoError(err)
		})

		t.Run("class in range [1,4] should be fine", func(t *testing.T) {
			assert := assert.New(t)

			for class := SecretClass(1); class <= 4; class++ {
				p := &PatchInput{
					Class: &class,
				}
				err := p.Validate()
				assert.NoError(err)
			}
		})

		t.Run("class outside of the range [1, 4] should error", func(t *testing.T) {
			assert := assert.New(t)

			for _, class := range []SecretClass{0, 5} {
				p := &PatchInput{
					Class: &class,
				}
				err := p.Validate()
				assert.Equal(ErrInvalidClass, err)
			}
		})
	})

	t.Run("hasPlaintextUpdates", func(t *testing.T) {
		t.Run("should return true if Autogenerate is sent", func(t *testing.T) {
			p := &PatchInput{Autogenerate: &FillPlaintextRequest{}}
			assert.True(t, p.hasPlaintextUpdates())
		})

		t.Run("should return true if plaintext is sent", func(t *testing.T) {
			p := &PatchInput{Plaintext: []byte("something")}
			assert.True(t, p.hasPlaintextUpdates())
		})

		t.Run("should return false if neither are sent", func(t *testing.T) {
			p := &PatchInput{}
			assert.False(t, p.hasPlaintextUpdates())
		})
	})
}

func TestPatch(t *testing.T) {
	existingSecret := &Secret{
		Name:       "mySecretName",
		UpdatedAt:  147,
		key:        []byte("mykey"),
		ciphertext: []byte("myciphertext"),
	}

	t.Run("should raise error on class == 0", func(t *testing.T) {
		assert := assert.New(t)
		mt := newManagerTest(t)
		defer mt.teardown(t)

		class := SecretClass(0)

		// mock the initial Query call so it doesn't return ErrSecretDoesNotExist
		_, err := dynamodbattribute.MarshalMap(existingSecret.asDynamoSecret())
		if err != nil {
			t.Fatal(err)
		}
		err = mt.m.Patch(&PatchInput{
			Name:  "mySecretName",
			Class: &class,
		})
		assert.Equal(ErrInvalidClass, err)
	})

	t.Run("should preserve patchInput.Class if set", func(t *testing.T) {
		mt := newManagerTest(t)
		defer mt.teardown(t)

		// mock the initial Query call so it doesn't return ErrSecretDoesNotExist
		item, err := dynamodbattribute.MarshalMap(existingSecret.asDynamoSecret())
		require.NoError(t, err)

		mt.mockDynamoDB.On("Query", mock.Anything).Return(&dynamodb.QueryOutput{
			Count: aws.Int64(1),
			Items: []map[string]*dynamodb.AttributeValue{item},
		}, nil).Once()

		// secrets table call
		mt.mockDynamoDB.On("UpdateItem", &dynamodb.UpdateItemInput{
			Key: map[string]*dynamodb.AttributeValue{
				"name": {S: aws.String("mySecretName")},
			},
			ReturnConsumedCapacity: aws.String("INDEXES"),
			TableName:              aws.String(mt.stack.SecretsTableName()),
			ExpressionAttributeNames: map[string]*string{
				"#class": aws.String("class"),
			},
			ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
				":class": {N: aws.String("3")},
			},
			UpdateExpression: aws.String("SET #class = :class"),
		}).Return(nil, nil).Once()

		// audit table call
		mt.mockDynamoDB.On("UpdateItem", mock.Anything).Run(func(args mock.Arguments) {
			resultOut := args.Get(0).(*dynamodb.UpdateItemInput)

			expectedOut := &dynamodb.UpdateItemInput{
				Key: map[string]*dynamodb.AttributeValue{
					"name":       {S: aws.String("mySecretName")},
					"updated_at": {N: aws.String("147")},
				},
				ReturnConsumedCapacity: aws.String("INDEXES"),
				TableName:              aws.String(mt.stack.AuditTableName()),
				ExpressionAttributeNames: map[string]*string{
					"#action_user":  aws.String("action_user"),
					"#class":        aws.String("class"),
					"#service_name": aws.String("service_name"),
				},
				ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
					":action_user":  {S: aws.String("")},
					":class":        {N: aws.String("3")},
					":service_name": {S: aws.String("unknown-service")},
				},
			}

			assert.Equal(t, expectedOut.Key, resultOut.Key)
			assert.Equal(t, expectedOut.ReturnConsumedCapacity, resultOut.ReturnConsumedCapacity)
			assert.Equal(t, expectedOut.TableName, resultOut.TableName)
			assert.Equal(t, expectedOut.ExpressionAttributeNames, resultOut.ExpressionAttributeNames)
			assert.Equal(t, expectedOut.ExpressionAttributeValues, resultOut.ExpressionAttributeValues)
		}).Return(nil, nil).Once()

		class := ClassRestricted
		assert.NoError(t, mt.m.Patch(&PatchInput{
			Name:  "mySecretName",
			Class: &class,
		}))
	})
}

func TestSealPatchInputPlaintext(t *testing.T) {
	t.Run("should seal plaintext values", func(t *testing.T) {
		const (
			testsecretName = "mySecretName"
			testCipherText = "some ciphertext"
			testKey        = "some key"
			testKeyArn     = "some key arn"
			testPlaintext  = "some plaintext"
			testUpdatedAt  = 17483
		)

		assert := assert.New(t)

		mt := newManagerTest(t)
		defer mt.teardown(t)

		s := Secret{
			UpdatedAt:  testUpdatedAt,
			key:        []byte(testKey),
			ciphertext: []byte(testCipherText),
			KeyARN:     testKeyArn,
		}
		sealedSecret := s.asDynamoSecret()

		mt.mockEnveloper.On("Seal", mock.Anything, []byte(testPlaintext)).Return(
			envelope.EncryptedDataKey{
				KeyARN: testKeyArn,
				Value:  []byte(testKey),
			},
			[]byte(testCipherText),
			nil,
		).Once()

		p := &PatchInput{
			Name:      testSecretName,
			Plaintext: []byte(testPlaintext),
		}

		err := mt.m.sealPatchInputPlaintext(p)
		assert.NoError(err)

		assert.Equal(&PatchInput{
			Name:      testSecretName,
			Plaintext: make([]byte, len(testPlaintext)),
			Key:       &sealedSecret.Key,
			Value:     &sealedSecret.Value,
			KeyARN:    &sealedSecret.KeyARN,

			// only know this at runtime
			UpdatedAt: p.UpdatedAt,
		}, p)
	})

	t.Run("should autogenerate if sent in request seal plaintext values", func(t *testing.T) {
		const (
			testsecretName = "mySecretName"
			testCipherText = "some ciphertext"
			testKey        = "some key"
			testKeyArn     = "some key arn"
			testPlaintext  = "some plaintext"
			testUpdatedAt  = 17483
		)

		assert := assert.New(t)

		mt := newManagerTest(t)
		defer mt.teardown(t)

		s := Secret{
			UpdatedAt:  testUpdatedAt,
			key:        []byte(testKey),
			ciphertext: []byte(testCipherText),
			KeyARN:     testKeyArn,
		}
		sealedSecret := s.asDynamoSecret()

		mt.mockEnveloper.On(
			"Seal", mock.Anything, mock.Anything,
		).Run(func(args mock.Arguments) {
			plaintext := args.Get(1).([]byte)
			assert.Len(plaintext, 64)
		}).Return(
			envelope.EncryptedDataKey{
				KeyARN: testKeyArn,
				Value:  []byte(testKey),
			},
			[]byte(testCipherText),
			nil,
		).Once()

		p := &PatchInput{
			Name: testSecretName,
			Autogenerate: &FillPlaintextRequest{
				Length: 48,
			},
		}

		err := mt.m.sealPatchInputPlaintext(p)
		assert.NoError(err)

		assert.Equal(&PatchInput{
			Autogenerate: &FillPlaintextRequest{
				Length: 48,
			},
			Name:   testSecretName,
			Key:    &sealedSecret.Key,
			Value:  &sealedSecret.Value,
			KeyARN: &sealedSecret.KeyARN,

			// only know this at runtime
			UpdatedAt: p.UpdatedAt,
		}, p)
	})

	t.Run("should forward autogenerate errors", func(t *testing.T) {
		assert := assert.New(t)

		mt := newManagerTest(t)
		defer mt.teardown(t)

		err := mt.m.sealPatchInputPlaintext(&PatchInput{
			Name: "mySecretName",
			Autogenerate: &FillPlaintextRequest{
				Length: -1,
			},
		})
		assert.Error(err)
	})

	t.Run("should forward seal errors", func(t *testing.T) {
		const (
			testsecretName = "mySecretName"
			testCipherText = "some ciphertext"
			testKey        = "some key"
			testKeyArn     = "some key arn"
			testPlaintext  = "some plaintext"
			testUpdatedAt  = 17483
		)

		var (
			sealError = errors.New("mySealError")
		)

		assert := assert.New(t)

		mt := newManagerTest(t)
		defer mt.teardown(t)

		mt.mockEnveloper.On(
			"Seal", mock.Anything, mock.Anything,
		).Return(envelope.EncryptedDataKey{}, nil, sealError).Once()

		err := mt.m.sealPatchInputPlaintext(&PatchInput{
			Name: testSecretName,
			Autogenerate: &FillPlaintextRequest{
				Length: 48,
			},
		})
		assert.Equal(sealError, err)
	})
}

func TestPost(t *testing.T) {
	const secretName = "group/project/environment/key"

	t.Run("Post should populate a namespace field correctly", func(t *testing.T) {
		assert := assert.New(t)
		mt := newManagerTest(t)
		defer mt.teardown(t)

		mt.mockDynamoDB.On("Query", mock.Anything).Return(&dynamodb.QueryOutput{
			Count: aws.Int64(0),
		}, nil)
		mt.mockEnveloper.On(
			"Seal", mock.Anything, mock.Anything,
		).Return(
			envelope.EncryptedDataKey{
				Value:  []byte("testRandom"),
				KeyARN: "randomKeyARN",
			}, nil, nil).Once()

		first := true
		mt.mockDynamoDB.On("PutItem", mock.Anything).Run(func(args mock.Arguments) {
			if !first {
				return
			}

			putInput := args.Get(0).(*dynamodb.PutItemInput)

			namespace, ok := putInput.Item["namespace"]
			if !assert.True(ok, "namespace key not found") {
				return
			}
			assert.Equal("group", aws.StringValue(namespace.S))
			first = false
		}).Return(&dynamodb.PutItemOutput{}, nil)

		err := mt.m.Post(&Secret{
			Name:      secretName,
			Plaintext: []byte("my plaintext"),
		})

		assert.NoError(err)
		assert.False(first)
	})
}
