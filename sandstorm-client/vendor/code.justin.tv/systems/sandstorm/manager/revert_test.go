package manager

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRevertSecretInputValidate(t *testing.T) {

	t.Run("validate", func(t *testing.T) {
		assert := assert.New(t)
		t.Run("version is empty", func(t *testing.T) {
			input := &RevertSecretInput{
				Name: "test",
			}
			err := input.validate()
			assert.IsType(&InputValidationError{}, err)
		})
		t.Run("name is empty", func(t *testing.T) {
			input := &RevertSecretInput{
				Version: 12344,
			}
			err := input.validate()
			assert.IsType(&InputValidationError{}, err)
		})
		t.Run("valid input", func(t *testing.T) {
			input := &RevertSecretInput{
				Name:    "test",
				Version: 1234,
			}
			err := input.validate()
			assert.Nil(err)

			input.ActionUser = "testUser"
			err = input.validate()
			assert.Nil(err)
		})
	})
}
