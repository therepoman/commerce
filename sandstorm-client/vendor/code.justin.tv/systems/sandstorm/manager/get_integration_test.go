// +build integration

package manager

import (
	"testing"
	"time"

	"code.justin.tv/systems/sandstorm/inventory/consumedsecrets"
	"code.justin.tv/systems/sandstorm/testutil"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestManagerGetIntegration(t *testing.T) {
	assert := assert.New(t)

	m := integrationTestCfg.Manager(t)
	consumedSecrets := integrationTestCfg.ConsumedSecrets(t)

	secretValue := "mysecret"

	resetSecret := func(secretName string) {
		err := m.Delete(secretName)
		assert.Nil(err)
	}

	t.Run("manager.get integration tests", func(t *testing.T) {
		t.Run("GetVersionsEncrypted", func(t *testing.T) {
			secretName := testutil.GetRandomSecretName(t)
			defer resetSecret(secretName)
			t.Run("with 4 secret revisions should retrieve the results in order of latest UpdatedAt", func(t *testing.T) {
				secretValues := []string{
					"secret-0",
					"secret-1",
					"secret-2",
				}
				err := m.Post(&Secret{
					Name:      secretName,
					Plaintext: []byte(secretValue),
				})
				require.NoError(t, err)

				_, err = m.Get(secretName)
				require.NoError(t, err)

				for n := 0; n < 3; n++ {
					time.Sleep(time.Duration(1 * time.Second))
					err := m.Patch(&PatchInput{
						Name:      secretName,
						Plaintext: []byte(secretValues[n]),
					})
					require.NoError(t, err)
				}

				_, err = m.Get(secretName)
				require.NoError(t, err)

				t.Run("when retrieving as a single list", func(t *testing.T) {
					versions, err := m.GetVersionsEncrypted(secretName, 10, 0)
					require.NoError(t, err)
					assert.NotNil(versions)
					assert.Len(versions.Secrets, 4)
					for n := 0; n < 2; n++ {
						assert.True(versions.Secrets[n].UpdatedAt > versions.Secrets[n+1].UpdatedAt)
					}
				})

				t.Run("when retrieving paginated", func(t *testing.T) {
					t.Parallel()
					versions, err := m.GetVersionsEncrypted(secretName, 1, 0)

					latestSecret, err := m.GetVersion(secretName, versions.Secrets[0].UpdatedAt)
					require.NoError(t, err)
					if assert.NotNil(latestSecret) {
						assert.Equal(secretValues[2], string(latestSecret.Plaintext))
					}

					for n := 1; n < 2; n++ {
						versions, err = m.GetVersionsEncrypted(secretName, 1, versions.NextKey)
						assert.Nil(err)
						assert.True(versions.NextKey > 0)

						secret, err := m.GetVersion(secretName, versions.Secrets[0].UpdatedAt)
						assert.Nil(err)
						if assert.NotNil(secret) {
							assert.Equal(secretValues[2-n], string(secret.Plaintext))
							assert.True(secret.UpdatedAt < latestSecret.UpdatedAt)
						}
						latestSecret = secret
					}
				})
			})
		})

		t.Run("CrossEnvironmentSecretsSet", func(t *testing.T) {
			secretName := testutil.GetRandomSecretName(t)
			defer resetSecret(secretName)

			const secretValue = "mySecretValue"
			err := m.Post(&Secret{
				Name:      secretName,
				Plaintext: []byte(secretValue),
				CrossEnv:  false,
				Class:     5,
			})
			assert.Nil(err)

			t.Run("should return cross_env secrets", func(t *testing.T) {
				filtered, err := m.CrossEnvironmentSecretsSet([]string{
					secretName,
				})
				assert.NoError(err)
				assert.Equal(map[string]struct{}{}, filtered)
			})

			t.Run("should not return non cross_env secrets", func(t *testing.T) {
				err := m.Put(&Secret{
					Name:      secretName,
					Plaintext: []byte(secretValue),
					CrossEnv:  true,
					Class:     5,
				})
				assert.Nil(err)

				filtered, err := m.CrossEnvironmentSecretsSet([]string{
					secretName,
				})
				assert.NoError(err)
				assert.Equal(map[string]struct{}{
					secretName: {},
				}, filtered)
			})
		})

		t.Run("GetVersion", func(t *testing.T) {
			secretName := testutil.GetRandomSecretName(t)
			defer resetSecret(secretName)

			err := m.Post(&Secret{
				Name:      secretName,
				Plaintext: []byte(secretValue),
			})
			assert.Nil(err)

			versions, err := m.GetVersionsEncrypted(secretName, 10, 0)
			require.NoError(t, err)
			require.Len(t, versions.Secrets, 1)
			actualSecretValue, err := m.GetVersion(secretName, versions.Secrets[0].UpdatedAt)
			assert.Nil(err)
			if assert.NotNil(actualSecretValue) {
				assert.Equal(secretValue, string(actualSecretValue.Plaintext))
			}
		})
	})

	t.Run("ReportSecretStatus", func(t *testing.T) {
		secretName := testutil.GetRandomSecretName(t)
		defer resetSecret(secretName)

		defer func() {
			assert.NoError(m.inventoryClient.Stop())
		}()

		err := m.Post(&Secret{
			Name:      secretName,
			Plaintext: []byte("myPlaintext"),
		})
		if err != nil {
			t.Fatal(err)
		}

		t.Run("get success report status", func(t *testing.T) {
			secret, err := m.Get(secretName)
			assert.NoError(err)
			assert.NotNil(secret)

			getTime := time.Now().UTC()

			var found bool
			for nTry := 0; nTry < 10; nTry++ {
				time.Sleep(time.Second)

				cs := []*consumedsecrets.ConsumedSecret{}

				err := consumedSecrets.IterateBySecret(secretName, func(s *consumedsecrets.ConsumedSecret) (err error) {
					cs = append(cs, s)
					return
				})
				if err != nil {
					t.Fatal(err)
				}

				t.Logf("%d heartbeats were found", len(cs))

				hostName := m.Config.Host
				serviceName := m.Config.ServiceName

				// check all values in cs and make sure only one of them has the same combo of
				//
				// - host name
				// - secret name
				// - service name
				filteredCs := []*consumedsecrets.ConsumedSecret{}
				for _, c := range cs {
					if c.Host == hostName && c.Secret == secretName && c.Service == serviceName {
						filteredCs = append(filteredCs, c)
					}
				}

				if len(filteredCs) == 0 {
					t.Logf("no heartbeats were found")
					continue
				}

				assert.Len(filteredCs, 1)

				c := filteredCs[0]
				td := getTime.Unix() - c.UpdatedAt
				if td > 10 {
					t.Logf("heartbeat was found but was reported at %d seconds ago", td)
					continue
				}
				assert.Equal(secret.UpdatedAt, c.UpdatedAt)

				found = true
				break
			}

			assert.True(found, "heartbeat was not found within 10 seconds")
		})
	})
}
