package manager_test

import (
	"context"
	"log"
	"time"

	"code.justin.tv/systems/sandstorm/callback"
	"code.justin.tv/systems/sandstorm/manager"
	"github.com/aws/aws-lambda-go/lambda"
)

// Creating a manager with a sandstorm role
func ExampleNew() {
	m := manager.New(manager.Config{
		// <sandstorm role arn> is created via
		// https://dashboard.internal.justin.tv/sandstorm/
		AWSConfig: manager.AWSConfig(nil, "<sandstorm role arn>"),
	})

	secret, err := m.Get("my/projects/testing/secret")
	if err != nil {
		// handle error
	}
	if secret == nil {
		log.Print("secret does not exist")
	}
}

// Register a callback to be triggered on a secret update with a dwell duration of 10s.
//
// This will accumulate successive executions of the callback until 10s has elapsed
// between executions. A common use case for using dwelled callbacks are operations
// that have several secret dependencies that need to be updated at the same time,
// i.e. username & password.
func ExampleManager_RegisterSecretUpdateCallback_dwell() {
	dw := &callback.Dweller{
		Duration: 10 * time.Second,
	}
	m := manager.New(manager.Config{})

	secretCb := func(input manager.SecretChangeset) {
		if input.SecretsHaveChanged() {

			// some things on secret updates

			if input.SecretHasBeenUpdated("dbUserName") {
				// logic related to secret dbUserName
			}

			// other things

			if input.SecretHasBeenUpdated("dbPassword") {
				// things related to secret dbPassword
			}

			// other things
		}

		// rest of things.
	}

	dwellCb := dw.Dwell(secretCb)
	m.RegisterSecretUpdateCallback("dbUserName", dwellCb)

	m.RegisterSecretUpdateCallback("dbPassword", dwellCb)

	err := m.ListenForUpdates()
	if err != nil {
		log.Fatal(err.Error())
	}
	// Do rest of the things.
}

// callback will be executed after a random time (upto Duration)
//
// E.g. if you would like to fetch a secret on thousands of machine,
// this would spread the fetch over 30 seconds so that
// not all the machine try to fetch the secret at same time.
// Hence, avoid getting throttled by sandstorm.
func ExampleManager_RegisterSecretUpdateCallback_splay() {
	splayer := &callback.Splayer{
		Duration: 30 * time.Second,
	}

	m := manager.New(manager.Config{})

	secretCb := func(input manager.SecretChangeset) {
		if input.SecretHasBeenUpdated("someSecretName") {
			// do work
		}
		// other work
	}

	// Call the secretCb after a random time (from 0 to Duration), after
	// recieving the secret update notification.
	m.RegisterSecretUpdateCallback("someSecretName", splayer.Splay(secretCb))

	err := m.ListenForUpdates()
	if err != nil {
		log.Fatal(err.Error())
	}
	// .....
}

// Register a callback to be triggered on a secret update.
//
// This will accumulate successive executions of the callback until 10s has elapsed
// between executions, and then spread the execution of callback over
// 5 seconds.
// A common use case for dwelled and splayed callback are operations
// that have several secret dependencies, and needs to be updated at the same
// time, and because thses secrets are used by hundered of machines, splay them over
// several seconds to avoid sandstorm throttling.
//
// E.g. sandstorm agent uses dwell and splay configs time to mimic similar behavior
func ExampleManager_RegisterSecretUpdateCallback_splayedDweller() {
	splayer := &callback.Splayer{
		Duration: 5 * time.Second,
	}

	dw := &callback.Dweller{
		Duration: 10 * time.Second,
	}
	m := manager.New(manager.Config{})

	secretCb := func(input manager.SecretChangeset) {
		if input.SecretsHaveChanged() {

			// some things on secret updates

			if input.SecretHasBeenUpdated("dbUserName") {
				// work related to secret dbUserName
			}

			// other things

			if input.SecretHasBeenUpdated("dbPassword") {
				// work related to secret dbPassword
			}

			// other work
		}
		// rest of things.
	}

	dwellSplayedCb := dw.Dwell(splayer.Splay(secretCb))

	m.RegisterSecretUpdateCallback("dbUsername", dwellSplayedCb)

	m.RegisterSecretUpdateCallback("dbPassword", dwellSplayedCb)

	err := m.ListenForUpdates()
	if err != nil {
		log.Fatal(err.Error())
	}
	// Do rest of the things.
}

type LambdaHandler struct {
	Sandstorm *manager.Manager
}

func (lh *LambdaHandler) Handle(ctx context.Context) error {
	return nil
}

// The Sandstorm SDK works well in lambda too but make sure to cache secret
// values in memory to optimize call time.
func ExampleManager_RegisterSecretUpdateCallback_lambdaUsage() {
	// in the main function for the lambda.
	m := manager.New(manager.Config{
		// <sandstorm role arn> is created via
		// https://dashboard.internal.justin.tv/sandstorm/
		AWSConfig: manager.AWSConfig(nil, "<sandstorm role arn>"),
	})

	// ListenForUpdates will start a go routine to manage an in-memory cache.
	if err := m.ListenForUpdates(); err != nil {
		// handle error
	}

	// give the handler an initialized manager to re-use. it will reuse a value in
	// its in-memory cache.
	h := &LambdaHandler{
		Sandstorm: m,
	}

	// start the lambda. calls to m.Get will be cached after the initial fetch.
	lambda.Start(h.Handle)
}
