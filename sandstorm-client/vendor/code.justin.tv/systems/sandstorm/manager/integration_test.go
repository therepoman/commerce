// +build integration

package manager

import (
	"testing"
	"time"

	"code.justin.tv/systems/sandstorm/inventory/consumedsecrets"
	"code.justin.tv/systems/sandstorm/mocks"
	"code.justin.tv/systems/sandstorm/pkg/integration"
	"code.justin.tv/systems/sandstorm/pkg/log"
	"code.justin.tv/systems/sandstorm/resource"
	"github.com/stretchr/testify/require"
)

type integrationTestConfig struct{}

func (tc integrationTestConfig) AWSConfigurer(t *testing.T) resource.AWSConfigurer {
	return resource.AWSConfigurer{
		Region: integration.Stack(t).AWSRegion(),
	}
}

func (tc integrationTestConfig) ManagerConfig(t *testing.T) *Config {
	stack := integration.Stack(t)

	return &Config{
		AWSConfig: tc.AWSConfigurer(t).
			AWSConfig(nil, stack.AdminRoleARN()),
		Environment:       stack.Environment(),
		Host:              "someHostName",
		InventoryInterval: 1 * time.Second,
		ServiceName:       "manager-integration-test",
		Stack:             stack,
		Logger:            &log.TestLogger{T: t},
	}
}

func (tc integrationTestConfig) Manager(t *testing.T) *Manager {
	mgr, err := NewWithError(*tc.ManagerConfig(t))
	require.NoError(t, err)
	return mgr
}

func (tc integrationTestConfig) ConsumedSecrets(t *testing.T) consumedsecrets.API {
	stack := integration.Stack(t)

	return consumedsecrets.New(
		&consumedsecrets.Config{
			TableName: stack.HeartbeatsTableName(),
		},
		tc.AWSConfigurer(t).
			STSSession(nil, stack.InventoryAdminRoleARN()),
	)
}

func (tc integrationTestConfig) ProxyOnlyManager(t *testing.T) *Manager {
	cfg := tc.ManagerConfig(t)
	cfg.AWSConfig = nil

	stack := integration.Stack(t)

	mgr, err := NewWithError(*cfg.
		WithProxyFallback(stack.ProxyURL(), "my-pop", &NoopProxyStatter{}).
		WithRole(stack.AdminRoleARN()))
	require.NoError(t, err)

	wrt := mgr.Config.AWSConfig.HTTPClient.Transport.(*wrappedRoundTripper)
	require.NoError(t, wrt.init())
	wrt.defaultRT = new(mocks.RoundTripper)
	wrt.switchToProxy()

	return mgr
}

var integrationTestCfg = integrationTestConfig{}
