// +build integration

package manager

import (
	"testing"
	"time"

	"code.justin.tv/systems/sandstorm/testutil"
	"github.com/stretchr/testify/assert"
)

func TestE2EListenForUpdates(t *testing.T) {
	m := integrationTestCfg.Manager(t)

	secretName := testutil.GetRandomSecretName(t)
	defer func() {
		err := m.Delete(secretName)
		assert.NoError(t, err)

		err = m.StopListeningForUpdates()
		assert.NoError(t, err)
	}()

	const (
		originalSecretValue = "plaintextSecret"
		patchedSecretValue  = "patchedPlaintextSecret"
	)

	err := m.Post(&Secret{
		Class:     ClassPublic,
		Name:      secretName,
		Plaintext: []byte(originalSecretValue),
	})
	if err != nil {
		t.Fatal(err)
	}

	err = m.ListenForUpdates()
	if err != nil {
		t.Fatal(err)
	}

	_, err = m.Get(secretName)
	if err != nil {
		t.Fatal(err)
	}

	t.Run("retrieve secret, secret is cached", func(t *testing.T) {
		cachedSecret := m.cache.Get(secretName)
		assert.NotNil(t, cachedSecret)
		assert.Equal(t, originalSecretValue, string(cachedSecret.Plaintext))
	})

	t.Run("rotate secret", func(t *testing.T) {
		err := m.Patch(&PatchInput{
			Name:      secretName,
			Plaintext: []byte(patchedSecretValue),
		})
		if err != nil {
			t.Fatal(err)
		}

		timeout := time.NewTimer(5 * time.Second)

		for {
			select {
			case <-timeout.C:
				t.Fatal("did not receive sqs message updating secret " + secretName)
			default:
			}

			cachedSecret := m.cache.Get(secretName)
			if cachedSecret == nil {
				break
			}
		}
	})
}
