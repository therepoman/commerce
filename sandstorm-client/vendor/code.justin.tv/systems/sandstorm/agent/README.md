# sandstorm-agent
---
A server side daemon for automatically fetching and updating managed secrets from [Sandstorm](https://git-aws.internal.justin.tv/systems/sandstorm).

## Documentation
Please see our [wiki](https://wiki.twitch.com/display/SSE/Sandstorm%3A+Secret+Management+System)

## Problems?

Contact us on slack at [#sse](https://twitch.slack.com/messages/sse), or open an [issue on github](https://git-aws.internal.justin.tv/systems/sandstorm/issues)

## Production Commands

```
// restart
sudo monit restart sandstorm-agent
```

## Logging

`/var/log/jtv/sandstorm-agent.log`
`/var/log/syslog`

# Local Development

## Local/Vagrant Testing

Provide AWS Credentials via ENV variables or `~/.aws/credentials`:
```
AWS_ACCESS_KEY_ID=foo
AWS_SECRET_ACCESS_KEY=bar
```

```
go run main.go --help
go run main.go run path_to_config_folder
go run main.go watch path_to_config_folder
```

## Testing Manta Build Process

Install [manta](https://git-aws.internal.justin.tv/release/manta-style#installation), and
run:

```bash
./build_agent.sh
```

See our [test configuration folder](test) for a full list of options.

## Deploying A New Sandstorm Agent Release

1. Bump [version number](../cmd/sandstorm-agent/version).
2. Publish a new release in [clean-deploy](https://clean-deploy.internal.justin.tv/#/systems/sandstorm) by selecting the "aptly" job.
3. Run [puppet](https://git-aws.internal.justin.tv/systems/puppet#updating-puppet-nodes) to update
all dependent nodes with `twitch_sandstorm_agent`.

### Canary Flow

1. Bump [version number](../cmd/sandstorm-agent/version).
2. Publish a canary release using "aptly-canary" in clean-deploy.
3. Find a node using canary sandstorm-agent [with oncall](#listing-nodes-running-sandstorm-agent) tool.
4. SSH onto the node and run `sudo puppet agent --test tags=twitch_sandstorm_agent`
5. Rotate a key and see if something breaks.

### Listing Nodes Running Sandstorm Agent

You can use `lsconsul` tool:
https://wiki.twitch.com/display/SSE/Core+Systems+Onboarding+Information
```
lsconsul -x sandstorm-agent-canary
```

Also achieved with consul and the `oncall` tool:
```
git clone https://git-aws.internal.justin.tv/web/oncall
./oncall list -g twitch_sandstorm_agent -d us-west2 -t <production|canary>
```

To test beanstalk flow, we have two nodes inside `twitch-sandstorm-aws`:
http://canary-sse-sandstorm-beanstalk-demo-env.bvyhfzsxyp.us-west-2.elasticbeanstalk.com/ (10.201.222.60)
http://prod-sse-sandstorm-beanstalk-demo-env.bvyhfzsxyp.us-west-2.elasticbeanstalk.com/ (10.201.221.70)
They're created via TCS and can be modified in https://git-aws.internal.justin.tv/sse/sandstorm-beanstalk-demo.
