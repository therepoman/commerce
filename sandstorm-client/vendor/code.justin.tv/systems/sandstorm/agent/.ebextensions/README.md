# sandstorm-agent ebextension for Elastic Beanstalk

This is an ebextension that installs and sets up sandstorm-agent. This assumes you've gone through the normal steps for setting up sandstorm IAM role. Please see https://wiki.twitch.com/display/SSE/Creating+Sandstorm+Roles+and+Groups+via+Dashboard if you haven't done so yet. 

## What the extension does

The ebextension will automatically install sandstorm-agent and set up the default sandstorm-agent configs for you as well as put the `/etc/sandstorm-agent` directory into place for you. It will also place the `conf.d` and `templates.d` subdirectories into place.

This extension supports both preconfigured elastic beanstalk platforms and single container docker environments.
If your app uses docker, please make sure to read the docker specific sections.

## Example Implementations

### Gnosis (preconfigured beanstalk platform)
https://git-aws.internal.justin.tv/qa/gnosis/tree/master/.ebextensions

### Autohost (single container docker, sandstorm manages rotations)
https://git-aws.internal.justin.tv/discovery/autohost/tree/master/.ebextensions

### Playlist (single container docker, must redeploy on rotation)
https://git-aws.internal.justin.tv/vod/playlists/tree/master/deploy/.ebextensions

## What you have to do

Copy .ebextensions to your app repo.

## Roles and Permission
Make sure you give your elastic beanstalk ec2 iam role (instance profile) includes the following statements:

```
{
    "Action": "sts:AssumeRole",
    "Resource": [
        "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/test" //REPLACE WITH YOUR ROLE FROM DASHBOARD
    ],
    "Effect": "Allow"
}
```        

## Config
There are two ways to set up the ebextension:  
1. [Secret rotations  require a redeploy of your application](#redeploy-method)  
2. [Sandstorm-Agent to runs in the background and listens for updates](#monitor-method) (Thanks to Nils for this method)

In both cases, Sandstorm-Agent fetches secrets on app deploy and templates to file.

For either setups:  
`TWITCH_ENV` = environment namespace for your secret (ex `production` for `syseng/testerino/production/test`)
`TWITCH_ROLE` = role from dashboard that you creted (ex `arn:aws:iam::734326455073:role/sandstorm/production/templated/role/test`)




### Redeploy Method
Rename `01-sandstorm.config.redeploy` to `01-sandstorm.config` and delete `01-sandstorm.config.monit`

This config fetches `TWITCH_ENV` and `TWITCH_ROLE` from the beanstalk environment variables and replaces those variables inside the files in `templates.d` and `conf.d`. If you'd like to use different variable names, change the names as appropriate in `01-sandstorm.config` and your files in `templates.d` and `conf.d`. 

Go to [Template Definitions](#template-definitions]

### Monitor Method

Rename `01-sandstorm.config.monit` to `01-sandstorm.config` and delete `01-sandstorm.config.redeploy`

If you'd like to reference the environment variables to set your role in your config, the ebextension defaults to using `{TWITCH_ROLE}`. If you'd like to use a different variable for your environment, change `.ebextensions/scripts/sandstorm-agent/sandstorm-setup.sh` accordingly. You MUST set `TWITCH_ROLE` in your EB Environment Variables to use this.

Go to [Template Definitions](#template-definitions]

### Docker Setup
If you're using the single container docker environment, your `Dockerrun.aws.json` should include a section for mounted Volumes.

Ex:
```
{
  "AWSEBDockerrunVersion": "1",
  "Volumes": [
    {
      "ContainerDirectory": "/var/app",
      "HostDirectory": "/var/app"
    }
  ],
  "Logging": "/var/eb_log"
}
```

Secrets will be written to the host directory and mounted to the container.

## Template Definitions
Inside `.ebextensions/data/sandstorm-agent/conf.d`, create as many `.conf` template definitions as you need. An example is included, please delete the example when deploying.

Go to [Template Sources](#template-sources)

### Docker
If you are using the single docker container environment, you MUST set template destination to be somewhere in the mounted host directory declared above. Otherwise, your app won't be able to access it. If you'd like your app to be restarted on secret change, use `docker restart $(docker ps -q)` as your restart command. See `.ebextensions/data/sandstorm-agent/conf.d/10-test.conf` for an example. 


## Template Sources
Inside `.ebextensions/data/sandstorm-agent/template.d`, please place as many template sources as you need. An example source to go with the example template config is also provided. Please delete when deploying.

If you'd like to reference the environment in your template sources, the ebextension defaults to using `{TWITCH_ENV}`. If you'd like to use a different variable for your environment, change `.ebextensions/scripts/sandstorm-agent/sandstorm-setup.sh` accordingly. You MUST set `TWITCH_ENV` in your EB Environment Variables to use this.

If you would prefer to have Sandstorm-Agent just update secrets and NOT restart your service, set `command = "none"` or `command = ""`. You can then manually trigger a redeploy of your app to increment version numbers.

Please see `.ebextensions/data/sandstorm-agent/template.d/test` for an example.

Please see https://git-aws.internal.justin.tv/systems/sandstorm-agent/blob/master/docs/non_puppet_agent.md#sandstorm-agent-templates for more details on Sandstorm templates. 

## ebextension configs
Please note that 
```
Configuration files are processed in alphabetical order, so you can split your configuration activities into multiple stages.
```
Therefore, if your ebextension config relies on Sandstorm secrets, please ensure that it is lexographically after the Sandstorm config. The sandstorm config is named `01-sandstorm.config` for your convenience, assuming it needs to be run first. You'd name a second config something like `02-secondconfig.config`.

## Logging
Sandstorm-Agent will log messages to `/var/log/messages`
