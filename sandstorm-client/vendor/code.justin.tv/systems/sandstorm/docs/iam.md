# IAM policies for Sandstorm

Sandstorm needs the following rights

## Credential manager - API server or user

[apiserver_policy.json](apiserver_policy.json)

This grants GenerateDataKey and Decrypt on the KMS master key, along with
DescribeTable, Query, Scan, Delete/Put/Update item on DynamoDB. Also allows the
policy generator to manage IAM policies.

* `arn:aws:kms:us-west-2:ACCOUNT:key/KEY_ID`

* `arn:aws:dynamodb:us-west-2:ACCOUNT:table/TABLE_NAME`

* `arn:aws:iam::ACCOUNT:policy/sandstorm/ENVIRONMENT/{templated/policy,templated/role,aux_policy}`

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "kms:Decrypt",
                "kms:GenerateDataKey"
            ],
            "Effect": "Allow",
            "Resource": "arn:aws:kms:us-west-2:734326455073:key/60e0f97f-eeca-4834-b146-7ac9fb20fd74"
        },
        {
            "Action": [
                "iam:AttachRolePolicy",
                "iam:CreatePolicy",
                "iam:CreatePolicyVersion",
                "iam:CreateRole",
                "iam:DeletePolicy",
                "iam:DeletePolicyVersion",
                "iam:DeleteRole",
                "iam:DetachRolePolicy",
                "iam:GetPolicy",
                "iam:GetPolicyVersion",
                "iam:GetRole",
                "iam:ListAttachedRolePolicies",
                "iam:ListPolicies",
                "iam:ListPolicyVersions",
                "iam:ListRoles",
                "iam:SetDefaultPolicyVersion",
                "iam:UpdateAssumeRolePolicy"
            ],
            "Effect": "Allow",
            "Resource": [
                "arn:aws:iam::734326455073:policy/sandstorm/production/templated/policy/",
                "arn:aws:iam::734326455073:policy/sandstorm/production/templated/policy/*",
                "arn:aws:iam::734326455073:policy/sandstorm/production/templated/role/",
                "arn:aws:iam::734326455073:policy/sandstorm/production/templated/role/*",
                "arn:aws:iam::734326455073:policy/sandstorm/production/aux_policy/",
                "arn:aws:iam::734326455073:policy/sandstorm/production/aux_policy/*"
            ]
        },
        {
            "Action": [
                "dynamodb:DeleteItem",
                "dynamodb:PutItem",
                "dynamodb:Query",
                "dynamodb:Scan",
                "dynamodb:DescribeTable",
                "dynamodb:UpdateItem"
            ],
            "Effect": "Allow",
            "Resource": [
                "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-production",
                "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-production_audit",
                "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-production_audit/index/namespace_name",
                "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-production_namespaces",
                "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-production/index/namespace_name"
            ]
        }
    ]
}
```

### Policy Generator Auxiliary Policy

[aux_policy.json](aux_policy.json)

The aux policy contains ACLs for the agent to access SNS/SQS/KMS resources.
This is required by the policy generator in the sandstorm API. This policy
should be created under path `/sandstorm/ENVIRONMENT/aux_policy/`.

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "sqs:CreateQueue",
                "sqs:GetQueueAttributes",
                "sqs:DeleteMessage",
                "sqs:ReceiveMessage",
                "sqs:SetQueueAttributes"
            ],
            "Effect": "Allow",
            "Resource": "arn:aws:sqs:us-west-2:734326455073:sandstorm*"
        },
        {
            "Action": [
                "sns:Subscribe"
            ],
            "Effect": "Allow",
            "Resource": "arn:aws:sns:us-west-2:734326455073:sandstorm-production"
        },
        {
            "Action": [
                "kms:Decrypt"
            ],
            "Effect": "Allow",
            "Resource": "arn:aws:kms:us-west-2:734326455073:key/60e0f97f-eeca-4834-b146-7ac9fb20fd74"
        }
    ]
}
```

## Agent policy

[agent_policy.json](agent_policy.json)

This grants the following:

* Decrypt on the KMS master key
* Query on DynamoDB table and related indices for namespaces `namespace1` and `namespace2`
* CreateQueue, DeleteMessage, GetQueueAttributes, ReceiveMessage, SetQueueAttributes on all queues named `sandstorm*`
* The ability to subscribe the queue to the SNS topic

(Namespace is the name of the key up until the first `/`)

Target resources:
* `arn:aws:kms:us-west-2:ACCOUNT:key/KEY_ID`
* `arn:aws:dynamodb:us-west-2:ACCOUNT:table/TABLE_NAME`
* `arn:aws:dynamodb:us-west-2:ACCOUNT:table/TABLE_NAME/index/namespace_name`
* `arn:aws:dynamodb:us-west-2:ACCOUNT:table/TABLE_NAME/index/key_generation`
* `arn:aws:sns:us-west-2:ACCOUNT:TOPIC_NAME`
* `arn:aws:sqs:us-west-2:ACCOUNT:sandstorm*`

```json
{
    "Version": "2012-10-17",
        "Statement": [
        {
            "Sid": "SQS",
            "Effect": "Allow",
            "Action": [
                "sqs:CreateQueue",
            "sqs:DeleteMessage",
            "sqs:GetQueueAttributes",
            "sqs:ReceiveMessage",
            "sqs:SetQueueAttributes"
            ],
            "Resource": [
                "arn:aws:sqs:us-west-2:673385534282:sandstorm*"
            ]
        },
        {
            "Sid": "DynamoTableRO",
            "Effect": "Allow",
            "Action": [
                "dynamodb:GetItem",
            "dynamodb:Query"
            ],
            "Condition": {
                "ForAllValues:StringLike": {
                    "dynamodb:LeadingKeys":[
                        "namespace1/*",
                    "namespace2/*"
                    ]
                }
            },
            "Resource": [
                "arn:aws:dynamodb:us-west-2:673385534282:table/sandstorm-production",
            "arn:aws:dynamodb:us-west-2:673385534282:table/sandstorm-production/index/key_generation"
            ]
        },
        {
            "Sid": "DynamoNamespaceRO",
            "Effect": "Allow",
            "Action": [
                "dynamodb:Query"
            ],
            "Condition":{
                "ForAllValues:StringEquals":{
                    "dynamodb:LeadingKeys":[
                        "namespace1",
                    "namespace2"
                    ]
                }
            },
            "Resource": [
                "arn:aws:dynamodb:us-west-2:673385534282:table/sandstorm-production/index/namespace_name"
            ]
        },
        {
            "Sid": "snsSubscribe",
            "Effect": "Allow",
            "Action": [
                "sns:Subscribe"
            ],
            "Resource": [
                "arn:aws:sns:us-west-2:673385534282:sandstorm-production"
            ]
        },
        {
            "Sid": "kmsDecrypt",
            "Effect": "Allow",
            "Action": [
                "kms:Decrypt"
            ],
            "Resource": [
                "arn:aws:kms:us-west-2:673385534282:key/1805013b-e64b-4b9a-b7ee-e5f3697eece6"
            ]
        }
    ]
}
```
