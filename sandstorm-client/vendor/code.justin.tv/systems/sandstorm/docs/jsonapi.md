# Sandstorm JSON API

The JSONAPI server provides a JSON REST API that UIs can use. 

Configuration example is in config.hcl. It needs a [Guardian OAuth](https://git-aws.internal.justin.tv/systems/guardian)
client ID and secret to verify the user-provided OAuth tokens. These can be obtained [here](https://dashboard.internal.justin.tv/guardian)


## Example config
for OAuth integration with guardian, sandstorm uses [aasimov](https://git-aws.internal.justin.tv/systems/guardian/blob/master/middleware/asiimov.go)
```
# DynamoDB Table and KMS master key
sandstorm {
    tablename = "sandstorm-env"
    key_id = "alias/sandstorm-env"
    ddb_statsd_host_port = "127.0.0.1:8125"
    ddb_statsd_prefix = "sandstorm-api.dev"
    role_arn = "role_to_assume"
}

# Guardian OAuth
oauth {
      client_id = "foo"
      secret = "bar"
# admin groups are ldap groups allowed view/modify all secrets
      admin_groups = ["team-syseng"]
#values below are OAuth defaults for use with aasimov, guardian's middleware (linked above)
      checktoken_url = "https://guardian.prod.us-west2.twitch.tv/oauth2/check_token"
      auth_url = "https://guardian.prod.us-west2.twitch.tv/authorize"
      token_url = "https://guardian.prod.us-west2.twitch.tv/oauth2/token"
}


# HTTP listen
http {
    listen = "127.0.0.1:4711"
}

# CORS config
cors {
#depends on the environment
    allowed_origins = [
        "https://dashboard-staging.internal.justin.tv",
        "https://dashboard.internal.justin.tv"
    ]
    allowed_methods = ["GET", "POST", "PUT", "PATCH", "DELETE"]
    allowed_headers = ["*"]
}

syslog {
    enabled = true
    format = "json"
}

changelog {
    enabled = true
    endpoint = "<%= @changelog_endpoint %>"
}

policy_generator {
    environment = "<%= @twitch_environment %>"
    aux_policy_arn = "<%= @sandstorm_aux_policy_arn %>"
    rw_aux_policy_arn = "<%= @sandstorm_rw_aux_policy_arn %>"
}
```

## API
All routes except `/status` require a Guardian OAuth Token provided as
the `Authorization` header. Example:

`Authorization: bearer <hexstring>`.

### Healthcheck

* GET `/status`
Health check for ELB. Reports OK if the application can use specified
DynamoDB table and KMS master key

### Secrets

* GET `/secrets`
List secrets

* GET `/secrets/{secret}`
Returns details of the secret

* GET `/secrets/{secret}/{updated_at}`
Returns details of the specified secret version

* POST `/secrets`
Adds a new secret or new version of a secret

* PATCH `/secrets/{secret}`
Adds a new version of the specified secret

* DELETE `/secrets/{secret}`
Deletes all version of the specified secret

### Roles

When updating/deleting/retrieving a specific role, pass in the name of the
role.

### Wildcards

Wildcards are allowed in secret name field only. For example:

_Allowed:_

* `team/service/env/*`
* `team/service/env/thing*`
* `team/service/env/*blah`
* `team/service/env/pop/*`

_Not Allowed:_

* `team/*/env/blah`
* `team/*/env/*`

### Routes

* GET `/roles` List roles

Returns a list of role names

Response:

```json
[
    "role1",
    "role2"
]
```

* POST `/roles` Adds a new role

Request:

```json
{
    "allowed_arns": [
        "arn:aws:iam::734326455073:user/jchen"
    ],
    "name": "test-role-here",
    "secret_keys": [
        "syseng/testerino/testing/test_secret",
        "syseng/blah/testing/*",
        "syseng/testerino/testing/test_secret_2"
    ],
    "owners": [
        "team-syseng",
    ],
    "write_access": true,
}
```

Response:

```json
{
  "generated_role_arn": "arn:aws:iam::734326455073:role/sandstorm/testing/templated/role/bleep-bloop",
  "name": "bleep-bloop",
  "generated_policy": {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": [
          "dynamodb:Query"
        ],
        "Resource": [
          "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-testing/index/namespace_name"
        ],
        "Effect": "Allow",
        "Condition": {
          "ForAllValues:StringEquals": {
            "dynamodb:LeadingKeys": [
              "syseng"
            ]
          }
        },
        "Sid": "DynamoDBNamespaceIndex"
      },
      {
        "Action": [
          "dynamodb:Query",
          "dynamodb:DescribeTable",
          "dynamodb:GetItem"
        ],
        "Resource": [
          "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-testing",
          "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-testing/index/key_generation"
        ],
        "Effect": "Allow",
        "Condition": {
          "ForAllValues:StringLike": {
            "dynamodb:LeadingKeys": [
              "syseng/testerino/testing/test_secret",
              "syseng/blah/testing/*",
              "syseng/testerino/testing/test_secret_2"
            ]
          }
        },
        "Sid": "DynamoDBSecretsTable"
      }
    ]
  },
  "assume_role_policy": {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": "sts:AssumeRole",
        "Resource": [
          "arn:aws:iam::734326455073:role/sandstorm/testing/templated/role/bleep-bloop"
        ]
      }
    ]
  },
  "allowed_arns": [
    "arn:aws:iam::734326455073:user/jchen"
  ],
  "write_access": true,
  "aux_policy_arn": "arn:aws:iam::734326455073:policy/sandstorm/testing/aux_policy/sandstorm-agent-testing-rw-aux"
}
```

* GET `/roles/{role}` Returns details of specified role

```json
{
  "generated_role_arn": "arn:aws:iam::734326455073:role/sandstorm/testing/templated/role/bleep-bloop",
  "name": "bleep-bloop",
  "generated_policy": {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": [
          "dynamodb:Query"
        ],
        "Resource": [
          "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-testing/index/namespace_name"
        ],
        "Effect": "Allow",
        "Condition": {
          "ForAllValues:StringEquals": {
            "dynamodb:LeadingKeys": [
              "syseng"
            ]
          }
        },
        "Sid": "DynamoDBNamespaceIndex"
      },
      {
        "Action": [
          "dynamodb:Query",
          "dynamodb:DescribeTable",
          "dynamodb:GetItem"
        ],
        "Resource": [
          "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-testing",
          "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-testing/index/key_generation"
        ],
        "Effect": "Allow",
        "Condition": {
          "ForAllValues:StringLike": {
            "dynamodb:LeadingKeys": [
              "syseng/testerino/testing/test_secret_3",
              "syseng/testerino/testing/test_secret_4"
            ]
          }
        },
        "Sid": "DynamoDBSecretsTable"
      }
    ]
  },
  "assume_role_policy": {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": "sts:AssumeRole",
        "Resource": [
          "arn:aws:iam::734326455073:role/sandstorm/testing/templated/role/bleep-bloop"
        ]
      }
    ]
  },
  "allowed_arns": [
    "arn:aws:iam::734326455073:user/bach"
  ],
  "write_access": true,
  "aux_policy_arn": "arn:aws:iam::734326455073:policy/sandstorm/testing/aux_policy/sandstorm-agent-testing-rw-aux"
}
```

* POST `/roles/{role}` Adds a new version of specified role

Request:

```json
{
    "allowed_arns": ["arn:aws:iam::734326455073:user/bach"],
    "name": "bleep-bloop",
    "write_access": false,
    "secret_keys": ["syseng/testerino/testing/test_secret_3", "syseng/testerino/testing/test_secret_4"]
}
```

Response:

```json
{
  "name": "bleep-bloop",
  "generated_policy": {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": [
          "dynamodb:Query"
        ],
        "Resource": [
          "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-testing/index/namespace_name"
        ],
        "Effect": "Allow",
        "Condition": {
          "ForAllValues:StringEquals": {
            "dynamodb:LeadingKeys": [
              "syseng"
            ]
          }
        },
        "Sid": "DynamoDBNamespaceIndex"
      },
      {
        "Action": [
          "dynamodb:Query",
          "dynamodb:DescribeTable",
          "dynamodb:GetItem"
        ],
        "Resource": [
          "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-testing",
          "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-testing/index/key_generation"
        ],
        "Effect": "Allow",
        "Condition": {
          "ForAllValues:StringLike": {
            "dynamodb:LeadingKeys": [
              "syseng/testerino/testing/test_secret_3",
              "syseng/testerino/testing/test_secret_4"
            ]
          }
        },
        "Sid": "DynamoDBSecretsTable"
      }
    ]
  },
  "assume_role_policy": {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": "sts:AssumeRole",
        "Resource": [
          "arn:aws:iam::734326455073:role/sandstorm/testing/templated/role/bleep-bloop"
        ]
      }
    ]
  },
  "write_access": false,
  "allowed_arns": [
    "arn:aws:iam::734326455073:user/bach"
  ]
}
```

* DELETE `/roles/{role}` Deletes specified role
