# Assigning AWS EC2 Instance Roles for Sandstorm

## Cross account roles

Sandstorm's infrastructure lives within the twitch-systems-aws account, which means
that you need to set up cross account AWS access in order to integrate
Sandstorm.

#### Creating A Role In The Dashboard

https://wiki.twitch.com/display/SSE/Creating+Sandstorm+Roles+and+Groups+via+Dashboard

To create a new role in the dashboard, select the relevent secrets using the checkboxes in
the [manage secrets](https://dashboard.internal.justin.tv/sandstorm?view=managesecrets)
view in the dashboard and click the "ADD NEW ROLE" button.


You'll need to copy the generated role
policy that's displayed after you create a role in the dashboard and attach it
to a policy document that's attached to the role in your local account that's
associated with your instance.

#### Updating A Role In The Dashboard

Go to the [manage
roles](https://dashboard.internal.justin.tv/sandstorm?view=manageroles) view,
select your role, and update the Allowed ARNs and/or Selected Secrets fields,
and click "SAVE". If you want to rename your role, you need to
[delete the existing role](#removing-a-role-in-the-dashboard) and
[create a new one](#creating-a-role-in-the-dashboard).

#### Removing A Role In The Dashboard

Go to the [manage
roles](https://dashboard.internal.justin.tv/sandstorm?view=manageroles) view,
select your role and click "DELETE".

## Using An Existing IAM Role ##

Simply add `aws_iam_roles = "<YOUR ROLE NAME>"` to your `puppet_instance` module.
