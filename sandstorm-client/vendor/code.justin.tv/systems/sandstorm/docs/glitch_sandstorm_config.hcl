dwell= 25
role = "none"
topicArn = "arn:aws:sns:us-west-2:734326455073:sandstorm-production"
KeyID = "alias/sandstorm-production"
tableName = "sandstorm-production"
Region = "us-west-2"
splay = 1
roleArn = "arn:aws:iam::734326455073:role/sandstorm-agent-systems"
logging = true
port = 7331
template {
  source = "/etc/sandstorm-agent/templates.d/glitch"
  destination = "/etc/slackbot/config.json"
  command = "restart glitch"
}