# Using Sandstorm Without Puppet

## 1. Installation

Sandstorm-agent is available as a Debian package using apt-get [`sudo apt-get install sandstorm-agent`]
or more manually via an [S3 bucket](https://console.aws.amazon.com/s3/home?region=us-west-2#&bucket=devtools-deploy-artifacts&prefix=systems/sandstorm-agent/).

## 2. Setup

### Assign IAM Roles and Credentials

Sandstorm-Agent is reliant on a number AWS services and requires AWS IAM credentials
in order to run. If your service exists on baremetal, the necessary credentials should
already be in place. To be safe, verify they exist at `/root/.aws/credentials`.

If your service runs on AWS EC2 we've written a guide for integrating Sandstorm-Agent
[via Terraform](./terraform.md).


### Sandstorm-Agent Config

Sandstorm-Agent configuration files typically go in `/etc/sandstorm-agent/conf.d`
with filenames ending with `.conf`. The minimum configuration to get a working
agent is shown in [this sample file](./default_config.hcl). All of the options
can be customized to meet your service's needs. Below is a description of the
attributes that can be overridden in the configuration file:

Attribute   | Description
----------- | -------------
`dwell`       | Time in seconds to wait for additional updates before processing updates.
`splay`       | Ceiling of interval [0-N] seconds to randomly wait before a post-update service restart.
`topicArn`    | The topic that your queue should listen on.
`tableName`   | The DynamoDB table to fetch secrets from.
`keyID`       | A KMS KeyId, either a key alias prefixed by 'alias/', a key ARN, or a key UUID.
`roleArn`     | An IAM Role ARN if assuming a role in different AWS account.
`port`         | Port for health check to run on.
Place the config file into `/etc/sandstorm-agent/`


You should really only ever need to change `dwell` and `splay` unless you really know
what you're doing.

**If your instance is on ec2, you must override `roleArn` to be: `'arn:aws:iam::734326455073:role/<cross-account-role>'`**.
The cross-account role is the role from the `twitch-systems-aws` account that you created on dashboard.internal. It will always have the account number `734326455073` as part of the ARN.
If your host is baremetal, no further actions are necessary.

### Sandstorm-Agent Templates

A sandstorm-agent template is a the template for the config of your service.

To TRIM TRAILING NEWLINE, add `{{- /*suppress trailing new line*/ -}}` to the end of your file.

Sandstorm-agent understands the following templating format:

```
{{ key "SECRETNAME" }}
```

#### Sandstorm Template Examples

##### JSON Config
* `${module_name}/json_template`:

```
"rollbar_api_token": "{{ key "web_rollbar_api_token" }}",
"github_oauth_secret": "{{ key "github_oauth_secret" }}"
```

Corresponding Output:

```
"rollbar_api_token": "dfg034mt043jh439rn",
"github_oauth_secret": "asd8dj32oj2ho2h8329h32"
```

##### Files/Certs/SSH Keys

```
{{ key "your_ssh_cert_key" }}
```

Corresponding Output:

```
-----BEGIN ENCRYPTED PRIVATE KEY-----
MIIFDjBABgkqhkiG9w0BBQ0wMzAbBgkqhkiG9w0BBQwwDgQIS2qgprFqPxECAggA
MBQGCCqGSIb3DQMHBAgD1kGN4ZslJgSCBMi1xk9jhlPxP3FyaMIUq8QmckXCs3Sa
X0R+meOaudPTBxoSgCCM51poFgaqt4l6VlTN4FRpj+c/WZeoMM/BVXO+nayuIMyH
blK948UAda/bWVmZjXfY4Tztah0CuqlAldOQBzu8TwE7WDwo5S7lo5u0EXEoqCCq
H0ga/iLNvWYexG7FHLRiq5hTj0g9mUPEbeTXuPtOkTEb/0ckVE2iZH9l7g5edmUZ
GEs=
-----END ENCRYPTED PRIVATE KEY-----
```


### Adding Sandstorm-Agent templates to Sandstorm-Agent

Once you have created a template, it must also be registered with your Sandstorm-Agent
configuration using the following format:

Put the following in an individual file in `/etc/sandstorm-agent/conf.d`, one
for each template, with a filename ending in `.conf`; for example, `/etc/sandstorm-agent/conf.d/10-example.conf`.

```
template {
  source = "/PATH/TO/TEMPLATE"
  destination = "/PATH/TO/CONFIG/FOR/YOUR/SERVICE"
  command = "Command to be run"
  user = "root"
  group = "ssl-cert"
  mode = "0640"
}
```

If user/group/mode are not specified, they default to root/root/0644.

"" and "none" are valid commands.

**Only** templates declared in the configuration file will be processed. This is required
for each template file you write. Here is an example of a fully configured configuration
file in production for a Slack Bot: [Glitch sandstorm-agent config](./glitch_sandstorm_config.hcl).

## 3. Starting Sandstorm-Agent

We use monit to monitor and run sandstorm-agent. Verify that both monit and a Sandstorm-Agent
monit [configuration](./monit_config) (`/etc/monit/conf.d/sandstorm-agent`) are on service's hosts. As the root user,
then start Sandstorm-agent using the command `monit start sandstorm-agent`.
