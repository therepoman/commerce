# Using Sandstorm (Agent) via Puppet
---

## 1. Puppet: `twitch_sandstorm_agent::template`

You can register a new template by using the `twitch_sandstorm_agent::template`
resource:

```puppet
twitch_sandstorm_agent::template { "unique_template_name":
  destination     => $service_config_file,
  restart_command => $service_restart_command,
  contents        => file("${module_name}/sandstorm_template"),
}
```

You can also specify a single key to file output via:

```puppet
twitch_sandstorm_agent::template { "unique_template_name":
  destination     => $service_config_file,
  key             => 'SECRET_KEY_NAME'
}
```

Alternatively, you can register new templates in hiera via:
```sandstorm_keys:
    "unique_template_name":
        destination: $service_config_file
        key: 'SECRET_KEY_NAME'
        restart_command: $service_restart_command
```

and

```
sandstorm_keys:
    "unique_template_name":
        destination:  $service_config_file
        template: ${module_name}/template_name
        restart_command: $service_restart_command
```

You can register as many templates as necessary.

Please see [template.pp](https://git-aws.internal.justin.tv/systems/puppet/blob/master/modules/twitch_sandstorm_agent/manifests/template.pp) for a full list of template attributes.

To TRIM TRAILING NEWLINE set `trim_newline=true`.

Store templates in  `<module name>/<template name>` in puppet as defined by your template registration above.

For a working example, see Systems's [dashboard](https://dashboard.internal.justin.tv) puppet [configuration.](https://git-aws.internal.justin.tv/systems/puppet/blob/master/modules/twitch_dashboard/manifests/config.pp).

### Sandstorm Template Examples

#### JSON Config
* `${module_name}/json_template`:

```
"rollbar_api_token": "{{ key "web_rollbar_api_token" }}",
"github_oauth_secret": "{{ key "github_oauth_secret" }}"
```

Corresponding Output:

```
"rollbar_api_token": "dfg034mt043jh439rn",
"github_oauth_secret": "asd8dj32oj2ho2h8329h32"
```

#### Files/Certs/SSH Keys
* `${module_name}/ssl_cert_template`:

```
{{ key "your_ssh_cert_key" }}
```

Corresponding Output:

```
-----BEGIN ENCRYPTED PRIVATE KEY-----
MIIFDjBABgkqhkiG9w0BBQ0wMzAbBgkqhkiG9w0BBQwwDgQIS2qgprFqPxECAggA
MBQGCCqGSIb3DQMHBAgD1kGN4ZslJgSCBMi1xk9jhlPxP3FyaMIUq8QmckXCs3Sa
X0R+meOaudPTBxoSgCCM51poFgaqt4l6VlTN4FRpj+c/WZeoMM/BVXO+nayuIMyH
blK948UAda/bWVmZjXfY4Tztah0CuqlAldOQBzu8TwE7WDwo5S7lo5u0EXEoqCCq
H0ga/iLNvWYexG7FHLRiq5hTj0g9mUPEbeTXuPtOkTEb/0ckVE2iZH9l7g5edmUZ
GEs=
-----END ENCRYPTED PRIVATE KEY-----
```

## 2. Assign AWS EC2 Instance Roles

If you're using Terraform to provision EC2 backed instances for your service, use our
[EC2 instances with Terraform](../docs/terraform.md)
guide.

### Baremetal

If your service runs on baremetal, it should already have the credentials it needs
to to access sandstorm. Verify they exist at `/root/.aws/credentials`.

## 3. (Optional) Configuration Overrides in Hiera

### Fields

Below are a list of attributes that can be used to override updater daemon's default values.

Attribute   | Description
----------- | -------------
`$dwell`       | Minimum time in seconds to wait for additional updates before performing any actions.
`$splay`       | Ceiling of interval [0-N] seconds to randomly wait before a post-update service restart.
`$role`       | Not yet implemented, but will be used for grouping secrets together.
`$topic_arn`    | The topic that your queue should listen on.
`$table_name`   | The DynamoDB table to fetch secrets from.
`$key_id`       | A KMS KeyId, either a key alias prefixed by 'alias/', a key ARN, or a key UUID.
`$role_arn`     | An IAM Role ARN if assuming a role in different AWS account.
`$port`         | Port for health check to run on.
`$pid_path`     | File name for pid file (should not really change)
`$pop`          | Name of your pop (will take this from facter by default)
`$use_squid_proxy`    | Sets sandstorm to use squid proxy as fallback
`$proxy`        | Will default to squid proxy if use_proxy set
`$backoff_multiplier`     | Exponential backoff multiplier
`$max_wait_time`     | Max wait time when backing off


### Register The Sandstorm Agent Class

In your [hiera cluster definition](https://git-aws.internal.justin.tv/systems/puppet/tree/master/hiera/cluster)
include, if your service is in ec2, you *must* override `twitch_sandstorm_agent::role_arn` to `'arn:aws:iam::734326455073:role/sandstorm-agent-<TEAM_NAME>'`

```
// https://git-aws.internal.justin.tv/systems/puppet/tree/master/hiera/cluster/your_cluster.yaml

classes:
  - 'your_service'
  - ...

// Add Overrides
twitch_sandstorm_agent::dwell: 10
twitch_sandstorm_agent::topicArn: 'production-foo'
...
```

