job {
    name 'systems-sandstorm-build'
    using 'TEMPLATE-autobuild'

    /**
     * credentials required for running tests with manta
     */
    wrappers {
      credentialsBinding {
        string 'AWS_SECRET_ACCESS_KEY', 'sse_sandstorm_aws_secret_access'
        string 'AWS_ACCESS_KEY_ID', 'sse_sandstorm_aws_access_id'
        string 'dta_tools_deploy', 'dta_tools_deploy'
        file('COURIERD_PRIVATE_KEY', 'courierd')
        file('AWS_CONFIG_FILE', 'aws_config')
      }
      environmentVariables {
        env('ENVIRONMENT', 'development')
      }
    }


    scm {
        git {
            remote {
                github 'systems/sandstorm', 'ssh', 'git-aws.internal.justin.tv'
                credentials 'git-aws-read-key'
            }
            configure { node ->
                node / 'extensions' << 'hudson.plugins.git.extensions.impl.SubmoduleOption' {
                    recursiveSubmodules true
                    timeout 20
                }
            }
            githubPolling(false)
        }
    }

    triggers {
      githubPush {
        githubPolling(false)
      }
    }

    steps {
        shell 'bash ./jenkins/build.sh'
        // this is from https://git-aws.internal.justin.tv/release/jenkins-jobs/blob/master/1templates/base.groovy
        saveDeployArtifact 'systems/sandstorm', 'build'
    }
    publishers {
        archiveArtifacts {
            pattern('.manta/*.log')
            allowEmpty()
        }
    }
}
