# prism

[![Go Report Card](https://goreportcard.internal.justin.tv/badge/code.justin.tv/commerce/prism)](https://goreportcard.internal.justin.tv/report/code.justin.tv/commerce/prism)

🌈 Prism - the Twitch chat preprocessor service


## Endpoints

| Environment | Endpoint |
| --- | --- |
| Staging | https://prism-payday-staging.internal.justin.tv |
| Prod | https://prism-payday-prod.internal.justin.tv |

## Teleport Bastion Clusters

| Environment | Cluster |
| --- | --- |
| Staging | teleport-remote-twitch-prism-devo |
| Prod | teleport-remote-twitch-prism-prod |