package models

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestMessageTreatment_ComparePriority(t *testing.T) {
	Convey("Given a messageTreatment", t, func() {
		Convey("that's empty", func() {
			messageTreatment := MessageTreatment("")

			Convey("and a new message treatment with a defined priority", func() {
				newMessageTreatment := MessageTreatment("default")

				So(messageTreatment.ComparePriority(newMessageTreatment), ShouldEqual, newMessageTreatment)
			})

			Convey("and a new message treatment without a defined priority", func() {
				newMessageTreatment := MessageTreatment("")

				So(messageTreatment.ComparePriority(newMessageTreatment), ShouldEqual, messageTreatment)
			})
		})

		Convey("that's defined", func() {
			messageTreatment := SYSTEM_MESSAGE

			Convey("and a new message treatment with an undefined priority", func() {
				newMessageTreatment := MessageTreatment("")

				So(messageTreatment.ComparePriority(newMessageTreatment), ShouldEqual, messageTreatment)
			})

			Convey("and a new message treatment with a lower defined priority", func() {
				newMessageTreatment := MessageTreatment("default")

				So(messageTreatment.ComparePriority(newMessageTreatment), ShouldEqual, messageTreatment)
			})

			Convey("and a new message treatment with a higher defined priority", func() {
				newMessageTreatment := MessageTreatment("anoncheer")

				So(messageTreatment.ComparePriority(newMessageTreatment), ShouldEqual, newMessageTreatment)
			})
		})
	})
}
