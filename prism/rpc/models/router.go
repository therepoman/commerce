package models

const (
	ANON_CHEER_ROUTE     = Route("anonymous_cheer_chat_message") //TODO: replace with anonymous system message in the future
	TMI_ROUTE            = Route("regular_chat_message")
	ROOMS_ROUTE          = Route("rooms_message")
	SYSTEM_MESSAGE_ROUTE = Route("system_message")

	ANONYMOUS_CHEER = MessageTreatment("anoncheer")
	DEFAULT_MESSAGE = MessageTreatment("default")
	SYSTEM_MESSAGE  = MessageTreatment("system_message") //TODO: replace with specialized system messages for different features
)

var MessageTreatmentRouter = map[MessageTreatment]Route{
	ANONYMOUS_CHEER: ANON_CHEER_ROUTE, //TODO: generalize anon cheering into anon messages
	SYSTEM_MESSAGE:  SYSTEM_MESSAGE_ROUTE,
	DEFAULT_MESSAGE: TMI_ROUTE,
}

var MessageTreatmentPriorityLookup = map[MessageTreatment]Priority{
	ANONYMOUS_CHEER: Priority(2),
	SYSTEM_MESSAGE:  Priority(1),
	DEFAULT_MESSAGE: Priority(0),
}

type Route string

type MessageTreatment string

type Priority int

// ComparePriority compares the priority of a message treatment to this message treatment and returns the highest
// priority one
func (mt MessageTreatment) ComparePriority(newMessageTreatment MessageTreatment) MessageTreatment {
	// The highest priority message treatment should be most prioritized

	newPriority, ok := MessageTreatmentPriorityLookup[newMessageTreatment]
	if !ok {
		// TODO: Return an error here and track error metrics
		// Better to keep an existing (potentially empty) message treatment than allow an undefined one through
		return mt
	}

	// Since the new message treatment has a priority, replace an empty message treatment with it
	if mt == MessageTreatment("") {
		return newMessageTreatment
	}

	existingPriority, ok := MessageTreatmentPriorityLookup[mt]
	if !ok {
		// TODO: Return an error here and track error metrics
		// Better to allow a new treatment with a defined priority than to keep an existing treatment without one
		return newMessageTreatment
	}

	if existingPriority >= newPriority {
		return mt
	} else {
		return newMessageTreatment
	}
}
