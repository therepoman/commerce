// Code generated by protoc-gen-go. DO NOT EDIT.
// source: rpc/tenant/prism-tenant.proto

package prism_tenant

import (
	fmt "fmt"
	math "math"

	rpc "code.justin.tv/commerce/prism/rpc"
	proto "github.com/golang/protobuf/proto"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type HealthCheckReq struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *HealthCheckReq) Reset()         { *m = HealthCheckReq{} }
func (m *HealthCheckReq) String() string { return proto.CompactTextString(m) }
func (*HealthCheckReq) ProtoMessage()    {}
func (*HealthCheckReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_d6da0bc22d3806dd, []int{0}
}

func (m *HealthCheckReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_HealthCheckReq.Unmarshal(m, b)
}
func (m *HealthCheckReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_HealthCheckReq.Marshal(b, m, deterministic)
}
func (m *HealthCheckReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_HealthCheckReq.Merge(m, src)
}
func (m *HealthCheckReq) XXX_Size() int {
	return xxx_messageInfo_HealthCheckReq.Size(m)
}
func (m *HealthCheckReq) XXX_DiscardUnknown() {
	xxx_messageInfo_HealthCheckReq.DiscardUnknown(m)
}

var xxx_messageInfo_HealthCheckReq proto.InternalMessageInfo

type HealthCheckResp struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *HealthCheckResp) Reset()         { *m = HealthCheckResp{} }
func (m *HealthCheckResp) String() string { return proto.CompactTextString(m) }
func (*HealthCheckResp) ProtoMessage()    {}
func (*HealthCheckResp) Descriptor() ([]byte, []int) {
	return fileDescriptor_d6da0bc22d3806dd, []int{1}
}

func (m *HealthCheckResp) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_HealthCheckResp.Unmarshal(m, b)
}
func (m *HealthCheckResp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_HealthCheckResp.Marshal(b, m, deterministic)
}
func (m *HealthCheckResp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_HealthCheckResp.Merge(m, src)
}
func (m *HealthCheckResp) XXX_Size() int {
	return xxx_messageInfo_HealthCheckResp.Size(m)
}
func (m *HealthCheckResp) XXX_DiscardUnknown() {
	xxx_messageInfo_HealthCheckResp.DiscardUnknown(m)
}

var xxx_messageInfo_HealthCheckResp proto.InternalMessageInfo

type TokenizeAndValidateMessageReq struct {
	UserId               string           `protobuf:"bytes,1,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
	ChannelId            string           `protobuf:"bytes,2,opt,name=channel_id,json=channelId,proto3" json:"channel_id,omitempty"`
	RoomId               string           `protobuf:"bytes,3,opt,name=room_id,json=roomId,proto3" json:"room_id,omitempty"`
	MessageId            string           `protobuf:"bytes,4,opt,name=message_id,json=messageId,proto3" json:"message_id,omitempty"`
	Message              string           `protobuf:"bytes,5,opt,name=message,proto3" json:"message,omitempty"`
	ConsumableAmounts    map[string]int64 `protobuf:"bytes,6,rep,name=consumable_amounts,json=consumableAmounts,proto3" json:"consumable_amounts,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"varint,2,opt,name=value,proto3"`
	Anonymous            bool             `protobuf:"varint,7,opt,name=anonymous,proto3" json:"anonymous,omitempty"`
	XXX_NoUnkeyedLiteral struct{}         `json:"-"`
	XXX_unrecognized     []byte           `json:"-"`
	XXX_sizecache        int32            `json:"-"`
}

func (m *TokenizeAndValidateMessageReq) Reset()         { *m = TokenizeAndValidateMessageReq{} }
func (m *TokenizeAndValidateMessageReq) String() string { return proto.CompactTextString(m) }
func (*TokenizeAndValidateMessageReq) ProtoMessage()    {}
func (*TokenizeAndValidateMessageReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_d6da0bc22d3806dd, []int{2}
}

func (m *TokenizeAndValidateMessageReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TokenizeAndValidateMessageReq.Unmarshal(m, b)
}
func (m *TokenizeAndValidateMessageReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TokenizeAndValidateMessageReq.Marshal(b, m, deterministic)
}
func (m *TokenizeAndValidateMessageReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TokenizeAndValidateMessageReq.Merge(m, src)
}
func (m *TokenizeAndValidateMessageReq) XXX_Size() int {
	return xxx_messageInfo_TokenizeAndValidateMessageReq.Size(m)
}
func (m *TokenizeAndValidateMessageReq) XXX_DiscardUnknown() {
	xxx_messageInfo_TokenizeAndValidateMessageReq.DiscardUnknown(m)
}

var xxx_messageInfo_TokenizeAndValidateMessageReq proto.InternalMessageInfo

func (m *TokenizeAndValidateMessageReq) GetUserId() string {
	if m != nil {
		return m.UserId
	}
	return ""
}

func (m *TokenizeAndValidateMessageReq) GetChannelId() string {
	if m != nil {
		return m.ChannelId
	}
	return ""
}

func (m *TokenizeAndValidateMessageReq) GetRoomId() string {
	if m != nil {
		return m.RoomId
	}
	return ""
}

func (m *TokenizeAndValidateMessageReq) GetMessageId() string {
	if m != nil {
		return m.MessageId
	}
	return ""
}

func (m *TokenizeAndValidateMessageReq) GetMessage() string {
	if m != nil {
		return m.Message
	}
	return ""
}

func (m *TokenizeAndValidateMessageReq) GetConsumableAmounts() map[string]int64 {
	if m != nil {
		return m.ConsumableAmounts
	}
	return nil
}

func (m *TokenizeAndValidateMessageReq) GetAnonymous() bool {
	if m != nil {
		return m.Anonymous
	}
	return false
}

type TokenizeAndValidateMessageResp struct {
	ConsumableAmounts    map[string]int64 `protobuf:"bytes,1,rep,name=consumable_amounts,json=consumableAmounts,proto3" json:"consumable_amounts,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"varint,2,opt,name=value,proto3"`
	Tokens               []*rpc.Token     `protobuf:"bytes,2,rep,name=tokens,proto3" json:"tokens,omitempty"`
	XXX_NoUnkeyedLiteral struct{}         `json:"-"`
	XXX_unrecognized     []byte           `json:"-"`
	XXX_sizecache        int32            `json:"-"`
}

func (m *TokenizeAndValidateMessageResp) Reset()         { *m = TokenizeAndValidateMessageResp{} }
func (m *TokenizeAndValidateMessageResp) String() string { return proto.CompactTextString(m) }
func (*TokenizeAndValidateMessageResp) ProtoMessage()    {}
func (*TokenizeAndValidateMessageResp) Descriptor() ([]byte, []int) {
	return fileDescriptor_d6da0bc22d3806dd, []int{3}
}

func (m *TokenizeAndValidateMessageResp) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TokenizeAndValidateMessageResp.Unmarshal(m, b)
}
func (m *TokenizeAndValidateMessageResp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TokenizeAndValidateMessageResp.Marshal(b, m, deterministic)
}
func (m *TokenizeAndValidateMessageResp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TokenizeAndValidateMessageResp.Merge(m, src)
}
func (m *TokenizeAndValidateMessageResp) XXX_Size() int {
	return xxx_messageInfo_TokenizeAndValidateMessageResp.Size(m)
}
func (m *TokenizeAndValidateMessageResp) XXX_DiscardUnknown() {
	xxx_messageInfo_TokenizeAndValidateMessageResp.DiscardUnknown(m)
}

var xxx_messageInfo_TokenizeAndValidateMessageResp proto.InternalMessageInfo

func (m *TokenizeAndValidateMessageResp) GetConsumableAmounts() map[string]int64 {
	if m != nil {
		return m.ConsumableAmounts
	}
	return nil
}

func (m *TokenizeAndValidateMessageResp) GetTokens() []*rpc.Token {
	if m != nil {
		return m.Tokens
	}
	return nil
}

type ProcessMessageReq struct {
	UserId               string             `protobuf:"bytes,1,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
	ChannelId            string             `protobuf:"bytes,2,opt,name=channel_id,json=channelId,proto3" json:"channel_id,omitempty"`
	RoomId               string             `protobuf:"bytes,3,opt,name=room_id,json=roomId,proto3" json:"room_id,omitempty"`
	MessageId            string             `protobuf:"bytes,4,opt,name=message_id,json=messageId,proto3" json:"message_id,omitempty"`
	Message              string             `protobuf:"bytes,5,opt,name=message,proto3" json:"message,omitempty"`
	ConsumableAmounts    map[string]int64   `protobuf:"bytes,6,rep,name=consumable_amounts,json=consumableAmounts,proto3" json:"consumable_amounts,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"varint,2,opt,name=value,proto3"`
	Anonymous            bool               `protobuf:"varint,7,opt,name=anonymous,proto3" json:"anonymous,omitempty"`
	GeoIpMetadata        *rpc.GeoIPMetadata `protobuf:"bytes,8,opt,name=geo_ip_metadata,json=geoIpMetadata,proto3" json:"geo_ip_metadata,omitempty"`
	XXX_NoUnkeyedLiteral struct{}           `json:"-"`
	XXX_unrecognized     []byte             `json:"-"`
	XXX_sizecache        int32              `json:"-"`
}

func (m *ProcessMessageReq) Reset()         { *m = ProcessMessageReq{} }
func (m *ProcessMessageReq) String() string { return proto.CompactTextString(m) }
func (*ProcessMessageReq) ProtoMessage()    {}
func (*ProcessMessageReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_d6da0bc22d3806dd, []int{4}
}

func (m *ProcessMessageReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ProcessMessageReq.Unmarshal(m, b)
}
func (m *ProcessMessageReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ProcessMessageReq.Marshal(b, m, deterministic)
}
func (m *ProcessMessageReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ProcessMessageReq.Merge(m, src)
}
func (m *ProcessMessageReq) XXX_Size() int {
	return xxx_messageInfo_ProcessMessageReq.Size(m)
}
func (m *ProcessMessageReq) XXX_DiscardUnknown() {
	xxx_messageInfo_ProcessMessageReq.DiscardUnknown(m)
}

var xxx_messageInfo_ProcessMessageReq proto.InternalMessageInfo

func (m *ProcessMessageReq) GetUserId() string {
	if m != nil {
		return m.UserId
	}
	return ""
}

func (m *ProcessMessageReq) GetChannelId() string {
	if m != nil {
		return m.ChannelId
	}
	return ""
}

func (m *ProcessMessageReq) GetRoomId() string {
	if m != nil {
		return m.RoomId
	}
	return ""
}

func (m *ProcessMessageReq) GetMessageId() string {
	if m != nil {
		return m.MessageId
	}
	return ""
}

func (m *ProcessMessageReq) GetMessage() string {
	if m != nil {
		return m.Message
	}
	return ""
}

func (m *ProcessMessageReq) GetConsumableAmounts() map[string]int64 {
	if m != nil {
		return m.ConsumableAmounts
	}
	return nil
}

func (m *ProcessMessageReq) GetAnonymous() bool {
	if m != nil {
		return m.Anonymous
	}
	return false
}

func (m *ProcessMessageReq) GetGeoIpMetadata() *rpc.GeoIPMetadata {
	if m != nil {
		return m.GeoIpMetadata
	}
	return nil
}

type ProcessMessageResp struct {
	NewBalance                 map[string]int64      `protobuf:"bytes,1,rep,name=new_balance,json=newBalance,proto3" json:"new_balance,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"varint,2,opt,name=value,proto3"`
	ConsumableAmountsProcessed map[string]int64      `protobuf:"bytes,2,rep,name=consumable_amounts_processed,json=consumableAmountsProcessed,proto3" json:"consumable_amounts_processed,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"varint,2,opt,name=value,proto3"`
	Transformations            []*rpc.Transformation `protobuf:"bytes,3,rep,name=transformations,proto3" json:"transformations,omitempty"`
	MessageTreatment           string                `protobuf:"bytes,4,opt,name=message_treatment,json=messageTreatment,proto3" json:"message_treatment,omitempty"`
	XXX_NoUnkeyedLiteral       struct{}              `json:"-"`
	XXX_unrecognized           []byte                `json:"-"`
	XXX_sizecache              int32                 `json:"-"`
}

func (m *ProcessMessageResp) Reset()         { *m = ProcessMessageResp{} }
func (m *ProcessMessageResp) String() string { return proto.CompactTextString(m) }
func (*ProcessMessageResp) ProtoMessage()    {}
func (*ProcessMessageResp) Descriptor() ([]byte, []int) {
	return fileDescriptor_d6da0bc22d3806dd, []int{5}
}

func (m *ProcessMessageResp) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ProcessMessageResp.Unmarshal(m, b)
}
func (m *ProcessMessageResp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ProcessMessageResp.Marshal(b, m, deterministic)
}
func (m *ProcessMessageResp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ProcessMessageResp.Merge(m, src)
}
func (m *ProcessMessageResp) XXX_Size() int {
	return xxx_messageInfo_ProcessMessageResp.Size(m)
}
func (m *ProcessMessageResp) XXX_DiscardUnknown() {
	xxx_messageInfo_ProcessMessageResp.DiscardUnknown(m)
}

var xxx_messageInfo_ProcessMessageResp proto.InternalMessageInfo

func (m *ProcessMessageResp) GetNewBalance() map[string]int64 {
	if m != nil {
		return m.NewBalance
	}
	return nil
}

func (m *ProcessMessageResp) GetConsumableAmountsProcessed() map[string]int64 {
	if m != nil {
		return m.ConsumableAmountsProcessed
	}
	return nil
}

func (m *ProcessMessageResp) GetTransformations() []*rpc.Transformation {
	if m != nil {
		return m.Transformations
	}
	return nil
}

func (m *ProcessMessageResp) GetMessageTreatment() string {
	if m != nil {
		return m.MessageTreatment
	}
	return ""
}

type PostProcessMessageReq struct {
	UserId               string             `protobuf:"bytes,1,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
	ChannelId            string             `protobuf:"bytes,2,opt,name=channel_id,json=channelId,proto3" json:"channel_id,omitempty"`
	RoomId               string             `protobuf:"bytes,3,opt,name=room_id,json=roomId,proto3" json:"room_id,omitempty"`
	MessageId            string             `protobuf:"bytes,4,opt,name=message_id,json=messageId,proto3" json:"message_id,omitempty"`
	Message              string             `protobuf:"bytes,5,opt,name=message,proto3" json:"message,omitempty"`
	ConsumableAmounts    map[string]int64   `protobuf:"bytes,6,rep,name=consumable_amounts,json=consumableAmounts,proto3" json:"consumable_amounts,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"varint,2,opt,name=value,proto3"`
	Anonymous            bool               `protobuf:"varint,7,opt,name=anonymous,proto3" json:"anonymous,omitempty"`
	GeoIpMetadata        *rpc.GeoIPMetadata `protobuf:"bytes,8,opt,name=geo_ip_metadata,json=geoIpMetadata,proto3" json:"geo_ip_metadata,omitempty"`
	SentMessage          string             `protobuf:"bytes,9,opt,name=sent_message,json=sentMessage,proto3" json:"sent_message,omitempty"`
	XXX_NoUnkeyedLiteral struct{}           `json:"-"`
	XXX_unrecognized     []byte             `json:"-"`
	XXX_sizecache        int32              `json:"-"`
}

func (m *PostProcessMessageReq) Reset()         { *m = PostProcessMessageReq{} }
func (m *PostProcessMessageReq) String() string { return proto.CompactTextString(m) }
func (*PostProcessMessageReq) ProtoMessage()    {}
func (*PostProcessMessageReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_d6da0bc22d3806dd, []int{6}
}

func (m *PostProcessMessageReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PostProcessMessageReq.Unmarshal(m, b)
}
func (m *PostProcessMessageReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PostProcessMessageReq.Marshal(b, m, deterministic)
}
func (m *PostProcessMessageReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PostProcessMessageReq.Merge(m, src)
}
func (m *PostProcessMessageReq) XXX_Size() int {
	return xxx_messageInfo_PostProcessMessageReq.Size(m)
}
func (m *PostProcessMessageReq) XXX_DiscardUnknown() {
	xxx_messageInfo_PostProcessMessageReq.DiscardUnknown(m)
}

var xxx_messageInfo_PostProcessMessageReq proto.InternalMessageInfo

func (m *PostProcessMessageReq) GetUserId() string {
	if m != nil {
		return m.UserId
	}
	return ""
}

func (m *PostProcessMessageReq) GetChannelId() string {
	if m != nil {
		return m.ChannelId
	}
	return ""
}

func (m *PostProcessMessageReq) GetRoomId() string {
	if m != nil {
		return m.RoomId
	}
	return ""
}

func (m *PostProcessMessageReq) GetMessageId() string {
	if m != nil {
		return m.MessageId
	}
	return ""
}

func (m *PostProcessMessageReq) GetMessage() string {
	if m != nil {
		return m.Message
	}
	return ""
}

func (m *PostProcessMessageReq) GetConsumableAmounts() map[string]int64 {
	if m != nil {
		return m.ConsumableAmounts
	}
	return nil
}

func (m *PostProcessMessageReq) GetAnonymous() bool {
	if m != nil {
		return m.Anonymous
	}
	return false
}

func (m *PostProcessMessageReq) GetGeoIpMetadata() *rpc.GeoIPMetadata {
	if m != nil {
		return m.GeoIpMetadata
	}
	return nil
}

func (m *PostProcessMessageReq) GetSentMessage() string {
	if m != nil {
		return m.SentMessage
	}
	return ""
}

type PostProcessMessageResp struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *PostProcessMessageResp) Reset()         { *m = PostProcessMessageResp{} }
func (m *PostProcessMessageResp) String() string { return proto.CompactTextString(m) }
func (*PostProcessMessageResp) ProtoMessage()    {}
func (*PostProcessMessageResp) Descriptor() ([]byte, []int) {
	return fileDescriptor_d6da0bc22d3806dd, []int{7}
}

func (m *PostProcessMessageResp) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PostProcessMessageResp.Unmarshal(m, b)
}
func (m *PostProcessMessageResp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PostProcessMessageResp.Marshal(b, m, deterministic)
}
func (m *PostProcessMessageResp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PostProcessMessageResp.Merge(m, src)
}
func (m *PostProcessMessageResp) XXX_Size() int {
	return xxx_messageInfo_PostProcessMessageResp.Size(m)
}
func (m *PostProcessMessageResp) XXX_DiscardUnknown() {
	xxx_messageInfo_PostProcessMessageResp.DiscardUnknown(m)
}

var xxx_messageInfo_PostProcessMessageResp proto.InternalMessageInfo

func init() {
	proto.RegisterType((*HealthCheckReq)(nil), "code.justin.tv.commerce.prism.tenant.HealthCheckReq")
	proto.RegisterType((*HealthCheckResp)(nil), "code.justin.tv.commerce.prism.tenant.HealthCheckResp")
	proto.RegisterType((*TokenizeAndValidateMessageReq)(nil), "code.justin.tv.commerce.prism.tenant.TokenizeAndValidateMessageReq")
	proto.RegisterMapType((map[string]int64)(nil), "code.justin.tv.commerce.prism.tenant.TokenizeAndValidateMessageReq.ConsumableAmountsEntry")
	proto.RegisterType((*TokenizeAndValidateMessageResp)(nil), "code.justin.tv.commerce.prism.tenant.TokenizeAndValidateMessageResp")
	proto.RegisterMapType((map[string]int64)(nil), "code.justin.tv.commerce.prism.tenant.TokenizeAndValidateMessageResp.ConsumableAmountsEntry")
	proto.RegisterType((*ProcessMessageReq)(nil), "code.justin.tv.commerce.prism.tenant.ProcessMessageReq")
	proto.RegisterMapType((map[string]int64)(nil), "code.justin.tv.commerce.prism.tenant.ProcessMessageReq.ConsumableAmountsEntry")
	proto.RegisterType((*ProcessMessageResp)(nil), "code.justin.tv.commerce.prism.tenant.ProcessMessageResp")
	proto.RegisterMapType((map[string]int64)(nil), "code.justin.tv.commerce.prism.tenant.ProcessMessageResp.ConsumableAmountsProcessedEntry")
	proto.RegisterMapType((map[string]int64)(nil), "code.justin.tv.commerce.prism.tenant.ProcessMessageResp.NewBalanceEntry")
	proto.RegisterType((*PostProcessMessageReq)(nil), "code.justin.tv.commerce.prism.tenant.PostProcessMessageReq")
	proto.RegisterMapType((map[string]int64)(nil), "code.justin.tv.commerce.prism.tenant.PostProcessMessageReq.ConsumableAmountsEntry")
	proto.RegisterType((*PostProcessMessageResp)(nil), "code.justin.tv.commerce.prism.tenant.PostProcessMessageResp")
}

func init() { proto.RegisterFile("rpc/tenant/prism-tenant.proto", fileDescriptor_d6da0bc22d3806dd) }

var fileDescriptor_d6da0bc22d3806dd = []byte{
	// 748 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xcc, 0x56, 0x4d, 0x4f, 0x13, 0x4f,
	0x18, 0x67, 0xdb, 0x52, 0xe8, 0x53, 0xfe, 0x94, 0x4e, 0xfe, 0xe2, 0x66, 0x03, 0x5a, 0x1b, 0x0e,
	0x4d, 0x94, 0x6d, 0x82, 0x12, 0x89, 0xe2, 0x81, 0x17, 0x23, 0x3d, 0x40, 0x9a, 0x4d, 0xa3, 0x06,
	0x0f, 0xcd, 0xb0, 0xfb, 0x08, 0x2b, 0xdd, 0x99, 0x65, 0x67, 0x0a, 0xc1, 0xe8, 0x41, 0x13, 0x0f,
	0x7a, 0xf3, 0xa8, 0xdf, 0xc0, 0x9b, 0x7e, 0x16, 0x13, 0x3f, 0x8f, 0xd9, 0x37, 0xa4, 0x2f, 0xb4,
	0xa5, 0xf4, 0xc0, 0x6d, 0x9f, 0x97, 0xdf, 0x6f, 0x9e, 0xf9, 0xcd, 0xb3, 0x33, 0x0f, 0xcc, 0x7b,
	0xae, 0x59, 0x96, 0xc8, 0x28, 0x93, 0x65, 0xd7, 0xb3, 0x85, 0xb3, 0x18, 0x1a, 0xba, 0xeb, 0x71,
	0xc9, 0xc9, 0x82, 0xc9, 0x2d, 0xd4, 0xdf, 0x34, 0x85, 0xb4, 0x99, 0x2e, 0x8f, 0x75, 0x93, 0x3b,
	0x0e, 0x7a, 0x26, 0xea, 0x41, 0xae, 0x1e, 0xe6, 0x6a, 0xcb, 0xad, 0x59, 0xe5, 0x38, 0x2b, 0x64,
	0x2c, 0xfb, 0x4b, 0x84, 0xdc, 0x02, 0xbd, 0x63, 0x3b, 0x40, 0x73, 0xc9, 0x8b, 0x33, 0x30, 0xbd,
	0x85, 0xb4, 0x21, 0x0f, 0x36, 0x0e, 0xd0, 0x3c, 0x34, 0xf0, 0xa8, 0x98, 0x87, 0x5c, 0x8b, 0x47,
	0xb8, 0xc5, 0x6f, 0x49, 0x98, 0xaf, 0xf1, 0x43, 0x64, 0xf6, 0x5b, 0x5c, 0x63, 0xd6, 0x73, 0xda,
	0xb0, 0x2d, 0x2a, 0x71, 0x1b, 0x85, 0xa0, 0xfb, 0x68, 0xe0, 0x11, 0xb9, 0x09, 0x13, 0x4d, 0x81,
	0x5e, 0xdd, 0xb6, 0x54, 0xa5, 0xa0, 0x94, 0x32, 0x46, 0xda, 0x37, 0x2b, 0x16, 0x99, 0x07, 0x30,
	0x0f, 0x28, 0x63, 0xd8, 0xf0, 0x63, 0x89, 0x20, 0x96, 0x89, 0x3c, 0x15, 0xcb, 0xc7, 0x79, 0x9c,
	0x3b, 0x7e, 0x2c, 0x19, 0xe2, 0x7c, 0x33, 0xc4, 0x39, 0x21, 0xbd, 0x1f, 0x4b, 0x85, 0xb8, 0xc8,
	0x53, 0xb1, 0x88, 0x0a, 0x13, 0x91, 0xa1, 0x8e, 0x07, 0xb1, 0xd8, 0x24, 0x9f, 0x15, 0x20, 0x26,
	0x67, 0xa2, 0xe9, 0xd0, 0xbd, 0x06, 0xd6, 0xa9, 0xc3, 0x9b, 0x4c, 0x0a, 0x35, 0x5d, 0x48, 0x96,
	0xb2, 0x4b, 0xbb, 0xfa, 0x20, 0x5a, 0xea, 0x3d, 0xf7, 0xaa, 0x6f, 0x9c, 0xb1, 0xaf, 0x85, 0xe4,
	0x4f, 0x99, 0xf4, 0x4e, 0x8d, 0xbc, 0xd9, 0xee, 0x27, 0x73, 0x90, 0xa1, 0x8c, 0xb3, 0x53, 0x87,
	0x37, 0x85, 0x3a, 0x51, 0x50, 0x4a, 0x93, 0xc6, 0x3f, 0x87, 0xb6, 0x09, 0xb3, 0xdd, 0xa9, 0xc8,
	0x0c, 0x24, 0x0f, 0xf1, 0x34, 0x52, 0xd2, 0xff, 0x24, 0xff, 0xc3, 0xf8, 0x31, 0x6d, 0x34, 0x31,
	0x50, 0x30, 0x69, 0x84, 0xc6, 0xa3, 0xc4, 0x8a, 0x52, 0xfc, 0x95, 0x80, 0x5b, 0xbd, 0xea, 0x15,
	0x2e, 0xf9, 0xd2, 0x5d, 0x12, 0x25, 0x90, 0xe4, 0xd5, 0xd5, 0x25, 0x11, 0xee, 0x25, 0x34, 0x59,
	0x85, 0xb4, 0xf4, 0xb9, 0x84, 0x9a, 0x08, 0xd6, 0x5f, 0xe8, 0xb3, 0x7e, 0xb0, 0xb0, 0x11, 0x61,
	0x46, 0xa4, 0xd9, 0x9f, 0x24, 0xe4, 0xab, 0x1e, 0x37, 0x51, 0x88, 0x6b, 0xd9, 0xc3, 0xef, 0x7b,
	0xb4, 0xf0, 0xce, 0x60, 0xe7, 0xd5, 0xb1, 0xbd, 0x51, 0xb5, 0x2d, 0xa9, 0x41, 0x6e, 0x1f, 0x79,
	0xdd, 0x76, 0xeb, 0x0e, 0x4a, 0x6a, 0x51, 0x49, 0xd5, 0xc9, 0x82, 0x52, 0xca, 0x2e, 0xdd, 0xeb,
	0x53, 0xd9, 0x33, 0xe4, 0x95, 0xea, 0x76, 0x84, 0x31, 0xfe, 0xdb, 0x47, 0x5e, 0x71, 0x63, 0x73,
	0x44, 0x07, 0xfb, 0x33, 0x05, 0xa4, 0x7d, 0xe7, 0xc2, 0x25, 0x36, 0x64, 0x19, 0x9e, 0xd4, 0xf7,
	0x68, 0x83, 0x32, 0x13, 0xa3, 0xc6, 0xdf, 0x1a, 0x4e, 0x48, 0xe1, 0xea, 0x3b, 0x78, 0xb2, 0x1e,
	0x52, 0x85, 0x12, 0x02, 0x3b, 0x73, 0x90, 0xef, 0x0a, 0xcc, 0x75, 0x9e, 0x5d, 0xdd, 0x0d, 0x59,
	0xd0, 0x8a, 0xba, 0xfe, 0xe5, 0xd0, 0x8b, 0x77, 0xa8, 0x54, 0x8d, 0xa9, 0xc3, 0x62, 0x34, 0xf3,
	0xc2, 0x04, 0xf2, 0x02, 0x72, 0xd2, 0xa3, 0x4c, 0xbc, 0xe6, 0x9e, 0x43, 0xa5, 0xcd, 0x99, 0x50,
	0x93, 0x41, 0x39, 0x8b, 0xfd, 0x7e, 0xc2, 0x16, 0x94, 0xd1, 0xce, 0x42, 0xee, 0x42, 0x3e, 0xee,
	0x74, 0xe9, 0x21, 0x95, 0x0e, 0x32, 0x19, 0x35, 0xfc, 0x4c, 0x14, 0xa8, 0xc5, 0x7e, 0xed, 0x09,
	0xe4, 0xda, 0x14, 0xbc, 0xcc, 0x19, 0x6b, 0xdb, 0x70, 0xbb, 0x8f, 0x06, 0x97, 0x6a, 0x99, 0x8f,
	0x29, 0xb8, 0x51, 0xe5, 0x42, 0x5e, 0xe7, 0xfb, 0xe0, 0x43, 0xaf, 0x37, 0xcd, 0x18, 0xb0, 0x95,
	0xba, 0xed, 0xf1, 0x7a, 0x5f, 0x0a, 0xe4, 0x0e, 0x4c, 0x09, 0x64, 0xb2, 0x1e, 0xcb, 0x92, 0x09,
	0x64, 0xc9, 0xfa, 0xbe, 0x68, 0x0f, 0x23, 0xba, 0x37, 0x54, 0x98, 0xed, 0xa6, 0x8f, 0x70, 0x97,
	0x7e, 0xa7, 0x20, 0x5b, 0xf5, 0x2b, 0xad, 0x05, 0x32, 0x92, 0x77, 0x90, 0x3d, 0x37, 0x1d, 0x91,
	0x07, 0x83, 0x89, 0xdf, 0x3a, 0x62, 0x69, 0xcb, 0x43, 0xa0, 0x84, 0x5b, 0x1c, 0x23, 0x3f, 0x14,
	0xd0, 0x2e, 0x7e, 0x89, 0xc9, 0xc6, 0x08, 0xc6, 0x1b, 0x6d, 0x73, 0x14, 0x03, 0x41, 0x71, 0x8c,
	0x7c, 0x52, 0x60, 0xba, 0x55, 0x50, 0xf2, 0x70, 0xc8, 0xb7, 0x4b, 0x5b, 0x19, 0xf6, 0xba, 0x2c,
	0x8e, 0x91, 0xaf, 0x0a, 0x90, 0xce, 0xc3, 0x25, 0x8f, 0xaf, 0xf0, 0xdb, 0x68, 0xab, 0xc3, 0x83,
	0xfd, 0x9a, 0xd6, 0xa7, 0x77, 0xa7, 0xce, 0x27, 0xee, 0xa5, 0x83, 0x61, 0xfc, 0xfe, 0xdf, 0x00,
	0x00, 0x00, 0xff, 0xff, 0x70, 0x2b, 0xbe, 0xe5, 0x0a, 0x0c, 0x00, 0x00,
}
