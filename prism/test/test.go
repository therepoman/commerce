package test

import (
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func GetNumCalls(m *mock.Mock, method string) int {
	numCalls := 0
	for _, call := range m.Calls {
		if call.Method == method {
			numCalls++
		}
	}
	return numCalls
}

type Range struct {
	Min int
	Max int
}

func NewRange(min int, max int) Range {
	return Range{
		Min: min,
		Max: max,
	}
}

func AssertInRange(actualVal int, expectedRange Range) {
	So(actualVal, ShouldBeGreaterThanOrEqualTo, expectedRange.Min)
	So(actualVal, ShouldBeLessThanOrEqualTo, expectedRange.Max)
}
