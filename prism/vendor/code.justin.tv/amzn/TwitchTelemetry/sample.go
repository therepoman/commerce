package telemetry

import "time"

// Sample holds telemetry data for a single event
type Sample struct {
	// Metric holds information that uniquely identifies the Metric that this sample belongs to. Namely,
	// the Metric contains the (Name, DimensionSet) of this Sample
	MetricID MetricID

	// RollupDimensions is an optional list of rollup dimension key sets.
	// Example:
	// RollupDimensions = [][]string {
	//     []string {"Domain"},
	//     []string {"Domain", "Operation"},
	// }
	// Given a MetricID.Dimensions with values for ServiceName, Operation, and Domain,
	// the final dimensions to send for this RollupDimensions would be:
	// []DimensionSet {
	//     DimensionSet {
	//         "ServiceName": "MyService",
	//         "Operation": "DoStuff",
	//         "Domain": "Foo",
	//     },
	//     DimensionSet {
	//         "ServiceName": "MyService",
	//         "Operation": "DoStuff",
	//     },
	//     DimensionSet {
	//         "ServiceName": "MyService",
	//     },
	// }
	RollupDimensions [][]string

	// Timestamp is the time that this sample was obtained.
	Timestamp time.Time

	// Value for the sample.
	Value float64

	// Unit of the sample value.  Use CloudWatch unit values.
	Unit string
}
