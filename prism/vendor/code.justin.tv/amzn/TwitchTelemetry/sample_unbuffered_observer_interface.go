package telemetry

// SampleUnbufferedObserver represents an observer that accepts Samples but does not maintain any buffering or
// flushing logic on its own.
//
// A sample use case would be having a buffered aggregator that requires a sender it can control (tell the sender
// when to flush and send it to a sample Distribution without having the sender perform any buffering logic on its own)
type SampleUnbufferedObserver interface {
	FlushWithoutBuffering(distributions []*Distribution)
}
