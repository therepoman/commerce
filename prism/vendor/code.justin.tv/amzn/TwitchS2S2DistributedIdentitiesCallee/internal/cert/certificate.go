package cert

import (
	"crypto/ecdsa"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/service"
)

type certificate struct {
	Certificate *x509.Certificate
	PublicKey   *ecdsa.PublicKey
	KeyID       string
	Service     *service.Service
}

func (k *certificate) Validate() error {
	now := time.Now()

	if k.Certificate.NotBefore.After(now) {
		return fmt.Errorf("certificate for %s is only valid after %s", k.KeyID, k.Certificate.NotBefore)
	}

	if k.Certificate.NotAfter.Before(now) {
		return fmt.Errorf("certificate for %s is only valid before %s", k.KeyID, k.Certificate.NotAfter)
	}

	return nil
}

func (k *certificate) UnmarshalJSON(in []byte) error {
	var jsonWebKey struct {
		Algorithm            string   `json:"alg"`
		KeyID                string   `json:"kid"`
		KeyOperations        []string `json:"key_ops"`
		KeyType              string   `json:"kty"`
		Use                  string   `json:"use"`
		X509CertificateChain [][]byte `json:"x5c"`
	}
	if err := json.Unmarshal(in, &jsonWebKey); err != nil {
		return err
	}

	if len(jsonWebKey.X509CertificateChain) != 1 {
		return fmt.Errorf("wrong format on jwks document: '%s'", string(in))
	}

	certificate, err := x509.ParseCertificate(jsonWebKey.X509CertificateChain[0])
	if err != nil {
		return err
	}

	k.Certificate = certificate
	k.KeyID = jsonWebKey.KeyID

	publicKey, ok := k.Certificate.PublicKey.(*ecdsa.PublicKey)
	if !ok {
		return fmt.Errorf("wrong algorithm on jwks document, only ES384 is supported: '%s'", string(in))
	}
	k.PublicKey = publicKey

	svc, err := service.Parse(k.Certificate.Subject.CommonName)
	if err != nil {
		return err
	}
	k.Service = svc

	return nil
}
