package s2s2err

import "fmt"

const wikiBaseLink = "https://wiki.twitch.com/display/SSE/S2S2DI+Errors#S2S2DIErrors"

// Code are short codes that are linked from the wiki
type Code string

// values for Code
const (
	CodeServiceStageNotFound = "ServiceStageNotFound"
	CodeX5UNotInCache        = "X5UNotInCache"
)

// NewError returns a wrapped error that includes documentation on how to
// resolve.
func NewError(code Code, originalError error) error {
	return &wrappedError{code: code, originalError: originalError}
}

type wrappedError struct {
	originalError error
	code          Code
}

func (err wrappedError) Error() string {
	return fmt.Sprintf("%s. See %s", err.originalError.Error(), err.link())
}

func (err wrappedError) link() string {
	return fmt.Sprintf("%s-%s", wikiBaseLink, err.code)
}
