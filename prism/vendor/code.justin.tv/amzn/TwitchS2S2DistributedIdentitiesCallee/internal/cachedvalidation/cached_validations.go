package cachedvalidation

import (
	"bytes"
	"container/list"
	"context"
	"crypto/subtle"
	"sync"

	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/validation"
	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/validation/validationiface"
)

// New returns a new CachedValidations
func New(
	validations validationiface.ValidationsAPI,
	maxLen int,
) *CachedValidations {
	return &CachedValidations{
		validations: validations,
		cache:       make(map[string]*list.Element),
		evictList:   list.New(),
		maxLen:      maxLen,
	}
}

// CachedValidations is a cached implementation of validation.Validations
type CachedValidations struct {
	validations validationiface.ValidationsAPI

	processLock sync.Mutex

	cacheLock sync.RWMutex
	evictList *list.List
	cache     map[string]*list.Element
	maxLen    int
}

// Validate validates an authentication presented.
func (v *CachedValidations) Validate(ctx context.Context, token []byte) (*validation.Validation, error) {
	value := v.fetchFromCache(token)
	if value != nil {
		if err := value.Validation.ValidateTimeClaims(); err != nil {
			return nil, err
		}

		return value.Validation, nil
	}

	value, err := v.processValidate(ctx, token)
	if err != nil {
		return nil, err
	}

	return value.Validation, nil
}

func (v *CachedValidations) processValidate(
	ctx context.Context,
	token []byte,
) (*cacheValue, error) {
	v.processLock.Lock()
	defer v.processLock.Unlock()

	if currentValue := v.fetchFromCache(token); currentValue != nil {
		// we didn't get the lock first
		return currentValue, nil
	}

	validation, err := v.validations.Validate(ctx, token)
	if err != nil {
		return nil, err
	}

	value := &cacheValue{
		Token:      token,
		Validation: validation,
	}
	v.updateCache(token, value)

	return value, nil
}

func (v *CachedValidations) fetchFromCache(token []byte) *cacheValue {
	tokenKey := v.tokenKey(token)
	if tokenKey == "" {
		return nil
	}

	element := func() *list.Element {
		v.cacheLock.RLock()
		defer v.cacheLock.RUnlock()
		return v.cache[tokenKey]
	}()
	if element == nil {
		return nil
	}

	func() {
		v.cacheLock.Lock()
		defer v.cacheLock.Unlock()
		v.evictList.MoveToFront(element)
	}()

	value := element.Value.(*cacheValue)

	if subtle.ConstantTimeCompare(value.Token, token) == 1 {
		return value
	}
	return nil
}

func (v *CachedValidations) updateCache(token []byte, value *cacheValue) {
	tokenKey := v.tokenKey(token)

	v.cacheLock.Lock()
	defer v.cacheLock.Unlock()

	if v.evictList.Len() >= v.maxLen {
		back := v.evictList.Back()
		delete(v.cache, v.tokenKey(back.Value.(*cacheValue).Token))
		v.evictList.Remove(back)
	}

	v.cache[tokenKey] = v.evictList.PushFront(value)
}

func (v *CachedValidations) tokenKey(token []byte) string {
	if i := bytes.LastIndex(token, []byte(".")); i > 0 {
		return string(token[:i])
	}
	return ""
}
