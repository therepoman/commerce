package s2s2dicallee

import (
	"encoding/json"
	"net/http"

	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/cert"
	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/logutil/logutiliface"
)

// Handler are the handlers used by S2S
type Handler interface {
	http.Handler

	// Returns a Handler to use for onboarding services on s2s2.
	// This will records metrics on how many callers are sending
	// valid s2s2 headers for onboarding sevices to s2s2.
	// Requests will not be rejected if invalid or no s2s2 auth
	// header is presented.
	RecordMetricsOnly() http.Handler
}

// handler implements Handler.
type handler struct {
	Inner  http.Handler
	Callee *Callee
	Logger logutiliface.LoggerAPI
}

//ServeHTTP implements http.Handler
func (h *handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	requestSubject, err := h.Callee.ValidateAuthentication(r)
	if err != nil {
		h.renderError(w, h.logErrorsAndReturnStatusCode(r, err), err)
		return
	}
	h.Inner.ServeHTTP(w, r.WithContext(SetRequestSubject(r.Context(), requestSubject)))
	return
}

// RecordMetricsOnly catches any errors from ValidateAuthentication and only
// records metrics and logs the errors.
func (h *handler) RecordMetricsOnly() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		requestSubject, err := h.Callee.ValidateAuthentication(r)
		if err != nil {
			h.logErrorsAndReturnStatusCode(r, err)
			h.Inner.ServeHTTP(w, r.WithContext(r.Context()))
			return
		}

		h.Inner.ServeHTTP(w, r.WithContext(SetRequestSubject(r.Context(), requestSubject)))
		return
	})
}

func (h *handler) logErrorsAndReturnStatusCode(r *http.Request, err error) int {
	switch err {
	case ErrNoAuthenticationPresented:
		h.Logger.LogAnonymousRequest(r)
		return http.StatusUnauthorized
	default:
		switch err := err.(type) {
		case *cert.ErrX5UNotInCache:
			h.Logger.LogUncachedX5U(err.X5U)
			return http.StatusForbidden
		default:
			h.Logger.LogUnknownError(err)
		}
	}
	return http.StatusInternalServerError
}

// format that twirp uses
type jsonError struct {
	Code string `json:"code"`
	Msg  string `json:"msg"`
}

func (h *handler) renderError(w http.ResponseWriter, code int, reason error) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	if err := json.NewEncoder(w).Encode(jsonError{
		Code: errorCodeString(code),
		Msg:  reason.Error(),
	}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
