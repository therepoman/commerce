package s2s2dicallee

// Service represents a s2s2di service
type Service struct {
	Name  string
	Stage string
}
