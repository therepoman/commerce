package s2s2dicallee

import (
	"context"
	"net/http"
	"strings"
	"time"

	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/cert/certiface"
	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/logutil/logutiliface"
	"code.justin.tv/amzn/TwitchS2S2DistributedIdentitiesCallee/internal/validation/validationiface"
)

const (
	authorizationHeader           = "Authorization"
	authorizationHeaderTypeBearer = "Bearer"
)

// Callee is a service receiving s2s2 distributed identity requests
type Callee struct {
	validations        validationiface.ValidationsAPI
	certificates       certiface.CertificatesAPI
	refreshRateLimiter *time.Ticker
	logger             logutiliface.LoggerAPI
}

// Handler returns a handler that validates S2S2DI authentication
func (c *Callee) Handler(inner http.Handler) Handler {
	return &handler{
		Inner:  inner,
		Callee: c,
		Logger: c.logger,
	}
}

// ValidateAuthentication validates a http.Request was authenticated via S2S2
// Distributed Identities.
func (c *Callee) ValidateAuthentication(req *http.Request) (AuthenticatedSubject, error) {
	authorizationHeaderValue := req.Header.Get(authorizationHeader)
	if authorizationHeaderValue == "" {
		return nil, ErrNoAuthenticationPresented
	}

	parts := strings.SplitN(authorizationHeaderValue, " ", 2)
	if len(parts) != 2 {
		return nil, &ErrInvalidAuthorizationHeader{Value: authorizationHeaderValue}
	}

	authorizationType, token := parts[0], parts[1]
	if authorizationType != authorizationHeaderTypeBearer {
		return nil, &ErrInvalidAuthorizationHeader{Value: authorizationHeaderValue}
	}

	validation, err := c.validations.Validate(req.Context(), []byte(token))
	if err != nil {
		return nil, err
	}

	return &authenticatedSubject{service: validation.Subject}, nil
}

// HardRefreshCache will hard refresh all tokens in our cache.
func (c *Callee) HardRefreshCache(ctx context.Context) error {
	select {
	case <-ctx.Done():
		return ctx.Err()
	case <-c.refreshRateLimiter.C:
	}
	return c.certificates.Refresh(ctx)
}
