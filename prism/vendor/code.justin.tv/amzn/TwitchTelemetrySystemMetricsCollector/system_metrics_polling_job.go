package agent

import (
	logging "code.justin.tv/amzn/TwitchLogging"
	identifier "code.justin.tv/amzn/TwitchProcessIdentifier"
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	poller "code.justin.tv/amzn/TwitchTelemetryPollingCollector"
	"log"
	"time"
)

// SystemMetricsCollector represents the type for available system level metrics collectors
type SystemMetricsCollector int

const (
	// CPUCollector represents CPU metrics
	CPUCollector SystemMetricsCollector = iota
	// NetCollector represents CPU metrics
	NetCollector
	// DiskCollector represents disk space and utilization metrics
	DiskCollector
	// DiskIOCollector represents disk io metrics
	DiskIOCollector
	// MemCollector represents memory (RAM) metrics
	MemCollector
	// LoadCollector represents load averages metrics
	LoadCollector
)

// SystemMetricsCollectorConfig represent a config that is used to determine the type of collectors to construct
type SystemMetricsCollectorConfig struct {
	Collectors []SystemMetricsCollector
	PerCPU     bool
}

// SystemMetricsPollingJob collects system metrics
type SystemMetricsPollingJob struct {
	systemMetricPollingJobs map[string]poller.PollingJob
	logger                  logging.Logger
}

// NewSystemMetricsPollingCollector returns a new polling collector that contains jobs for every collector in the given config
func NewSystemMetricsPollingCollector(config SystemMetricsCollectorConfig, interval time.Duration, pid *identifier.ProcessIdentifier, obs telemetry.SampleObserver, l logging.Logger) *poller.PollingCollector {
	// Construct a polling job
	systemMetricsPollingJob := newSystemMetricsPollingJob(l)

	// Register all requested collectors
	for _, collector := range config.Collectors {
		switch collector {
		case CPUCollector:
			systemMetricsPollingJob.EnableCPU(config.PerCPU)
		case NetCollector:
			systemMetricsPollingJob.EnableNet()
		case DiskCollector:
			systemMetricsPollingJob.EnableDisk()
		case MemCollector:
			systemMetricsPollingJob.EnableMemory()
		case LoadCollector:
			systemMetricsPollingJob.EnableLoad()
		case DiskIOCollector:
			systemMetricsPollingJob.EnableDiskIO()
		}
	}

	// Construct a new sample builder so we have per-host metric names
	hostSampleBuilder := &telemetry.SampleBuilder{
		ProcessIdentifier:             *pid,
		EnableProcessAddressDimension: true,
		Dimensions:                    telemetry.DimensionSet{telemetry.DimensionSubstage: ""},
	}

	return poller.NewPollingCollector(systemMetricsPollingJob, interval, hostSampleBuilder, obs, l)
}

// EnableCPU turns on collection of CPU system metrics
func (smpj *SystemMetricsPollingJob) EnableCPU(perCPU bool) {
	cpuJob := NewCPUPollingJob(perCPU, smpj.logger)
	smpj.registerJob("cpu", cpuJob)
}

// EnableMemory turns on collection of Memory system metrics
func (smpj *SystemMetricsPollingJob) EnableMemory() {
	memJob := NewMemoryPollingJob(smpj.logger)
	smpj.registerJob("mem", memJob)
}

// EnableLoad turns on collection of LoadAverage system metrics
func (smpj *SystemMetricsPollingJob) EnableLoad() {
	loadJob := NewLoadPollingJob(smpj.logger)
	smpj.registerJob("load", loadJob)
}

// EnableDisk turns on collection of disk system metrics
func (smpj *SystemMetricsPollingJob) EnableDisk() {
	diskJob := NewDiskPollingJob(smpj.logger)
	smpj.registerJob("disk", diskJob)
}

// EnableDiskIO turns on collection of disk io system metrics
func (smpj *SystemMetricsPollingJob) EnableDiskIO() {
	diskIOJob := NewDiskIOPollingJob(smpj.logger)
	smpj.registerJob("diskIO", diskIOJob)
}

// EnableNet turns on collection of network system metrics
func (smpj *SystemMetricsPollingJob) EnableNet() {
	netJob := NewNetPollingJob(smpj.logger)
	smpj.registerJob("net", netJob)
}

// Fetch scrapes samples for every system collector into the accumulator,
// then returns all accumulated samples
func (smpj *SystemMetricsPollingJob) Fetch() ([]*telemetry.Sample, error) {
	totalSamples := make([]*telemetry.Sample, 0)
	for name, job := range smpj.systemMetricPollingJobs {
		returnedSamples, err := job.Fetch()
		if err != nil {
			if smpj.logger != nil {
				smpj.logger.Log("Failed scraping samples", "Component", name)
			} else {
				log.Printf("Failed scraping samples for Componet %v", name)
			}
			continue
		}
		totalSamples = append(totalSamples, returnedSamples...)
	}
	return totalSamples, nil
}

func newSystemMetricsPollingJob(l logging.Logger) *SystemMetricsPollingJob {
	return &SystemMetricsPollingJob{
		systemMetricPollingJobs: make(map[string]poller.PollingJob),
		logger:                  l,
	}
}

func (smpj *SystemMetricsPollingJob) registerJob(name string, job poller.PollingJob) {
	if !smpj.jobIsRegistered(name) {
		smpj.systemMetricPollingJobs[name] = job
	}
}

func (smpj *SystemMetricsPollingJob) jobIsRegistered(name string) bool {
	_, found := smpj.systemMetricPollingJobs[name]
	return found
}
