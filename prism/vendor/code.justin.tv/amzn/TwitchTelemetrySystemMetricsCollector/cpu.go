package agent

import (
	"github.com/shirou/gopsutil/cpu"

	logging "code.justin.tv/amzn/TwitchLogging"
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"math"
)

// Calculations in this collector heavily inspired by:
// https://github.com/influxdata/telegraf/blob/master/plugins/inputs/cpu/cpu.go

// CPUPollingJob scrapes data about CPU usage
type CPUPollingJob struct {
	perCPU         bool
	lastTimesStats map[string]cpu.TimesStat
	logger         logging.Logger
}

// NewCPUPollingJob creates and returns a new CPUPollingJob
func NewCPUPollingJob(perCPU bool, l logging.Logger) *CPUPollingJob {
	return &CPUPollingJob{
		perCPU:         perCPU,
		lastTimesStats: make(map[string]cpu.TimesStat),
		logger:         l,
	}
}

// Fetch collects data about CPU usage
func (cpj *CPUPollingJob) Fetch() ([]*telemetry.Sample, error) {
	currentTimesStat := []cpu.TimesStat{}
	samples := []*telemetry.Sample{}
	// total stats
	totalTimes, err := cpu.Times(false)
	if err != nil {
		return nil, err
	}
	currentTimesStat = append(currentTimesStat, totalTimes...)
	// Only have per CPU stats if requested
	if cpj.perCPU {
		times, err := cpu.Times(true)
		if err != nil {
			return nil, err
		}
		currentTimesStat = append(currentTimesStat, times...)
	}

	// For each TimesStat, diff from the last entry, make a sample
	for _, timesStat := range currentTimesStat {
		lastTimesStat, found := cpj.lastTimesStats[timesStat.CPU]
		// If there's no lastTimeStats, set it and don't report (this is the first run through)
		if !found {
			cpj.lastTimesStats[timesStat.CPU] = timesStat
			continue
		}
		// Otherwise, we can construct samples
		user := userDelta(timesStat, lastTimesStat)
		samples = append(samples, baseCPUSample("CpuUser", timesStat.CPU, user))

		system := systemDelta(timesStat, lastTimesStat)
		samples = append(samples, baseCPUSample("CpuSystem", timesStat.CPU, system))

		ioWait := ioWaitDelta(timesStat, lastTimesStat)
		samples = append(samples, baseCPUSample("CpuIoWait", timesStat.CPU, ioWait))

		// Finish by setting the previous to the current
		cpj.lastTimesStats[timesStat.CPU] = timesStat
	}

	return samples, nil
}

func baseCPUSample(metricName, cpuName string, value float64) *telemetry.Sample {
	metricID := telemetry.NewMetricID(metricName)
	metricID.AddDimension("CPU", cpuName)
	return &telemetry.Sample{
		MetricID: *metricID,
		// Ensure we only return positive numbers
		Value: math.Max(value, 0.0),
		Unit:  telemetry.UnitCount,
	}
}

func totalCPUTime(t cpu.TimesStat) float64 {
	total := t.User + t.System + t.Nice + t.Iowait + t.Irq + t.Softirq + t.Steal +
		t.Idle
	return total
}

func userDelta(cur, last cpu.TimesStat) float64 {
	total := totalCPUTime(cur) - totalCPUTime(last)
	return 100 * (cur.User - last.User - (cur.Guest - last.Guest)) / total
}

func systemDelta(cur, last cpu.TimesStat) float64 {
	total := totalCPUTime(cur) - totalCPUTime(last)
	return 100 * (cur.System - last.System) / total
}

func ioWaitDelta(cur, last cpu.TimesStat) float64 {
	total := totalCPUTime(cur) - totalCPUTime(last)
	return 100 * (cur.Iowait - last.Iowait) / total
}
