package agent

import (
	"github.com/shirou/gopsutil/mem"

	logging "code.justin.tv/amzn/TwitchLogging"
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"math"
)

// MemoryPollingJob collects memory system metrics
type MemoryPollingJob struct {
	logger logging.Logger
}

// NewMemoryPollingJob creates and returns a new MemoryPollingJob
func NewMemoryPollingJob(l logging.Logger) *MemoryPollingJob {
	return &MemoryPollingJob{
		logger: l,
	}
}

// Fetch returns telemetry samples measuring system memory
func (mpj *MemoryPollingJob) Fetch() ([]*telemetry.Sample, error) {
	virtMemStat, err := mem.VirtualMemory()
	if err != nil {
		return nil, err
	}
	samples := []*telemetry.Sample{}
	samples = append(samples, baseMemorySample("MemTotal", float64(virtMemStat.Total)))
	samples = append(samples, baseMemorySample("MemAvailable", float64(virtMemStat.Available)))
	samples = append(samples, baseMemorySample("MemUsed", float64(virtMemStat.Used)))
	return samples, nil
}

func baseMemorySample(metricName string, value float64) *telemetry.Sample {
	metricID := telemetry.NewMetricID(metricName)
	return &telemetry.Sample{
		MetricID: *metricID,
		// Ensure we only return positive numbers
		Value: math.Max(value, 0.0),
		Unit:  telemetry.UnitBytes,
	}
}
