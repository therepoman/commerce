package agent

import (
	logging "code.justin.tv/amzn/TwitchLogging"
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	"github.com/shirou/gopsutil/disk"
	"log"
	"math"
)

// DiskPollingJob collects system metrics for hardware disks
type DiskPollingJob struct {
	logger logging.Logger
}

// DiskIOPollingJob collects system metrics for hardware disk io
type DiskIOPollingJob struct {
	lastIOCounterStats map[string]disk.IOCountersStat
	logger             logging.Logger
}

// NewDiskPollingJob returns a new disk polling job
func NewDiskPollingJob(l logging.Logger) *DiskPollingJob {
	return &DiskPollingJob{
		logger: l,
	}
}

// NewDiskIOPollingJob returns a new disk polling job
func NewDiskIOPollingJob(l logging.Logger) *DiskIOPollingJob {
	return &DiskIOPollingJob{
		lastIOCounterStats: make(map[string]disk.IOCountersStat),
		logger:             l,
	}
}

// Fetch returns disk metrics for all discovered hardware disks
func (dpj *DiskPollingJob) Fetch() ([]*telemetry.Sample, error) {

	// collect all physical partitions
	partitions, err := disk.Partitions(false)
	if err != nil {
		return nil, err
	}

	samples := []*telemetry.Sample{}

	// for each partition found, collect usage stats
	for _, partition := range partitions {
		// usage stats
		usage, err := disk.Usage(partition.Mountpoint)
		if err != nil {
			if dpj.logger != nil {
				dpj.logger.Log("No information for device", "Device", partition.Device, "Error", err.Error())
			} else {
				log.Printf("No information for device %v, error %v", partition.Device, err)
			}
			continue
		}
		samples = append(samples, baseDiskUsageSample("DiskTotal", partition.Device, float64(usage.Total), telemetry.UnitBytes))
		samples = append(samples, baseDiskUsageSample("DiskFree", partition.Device, float64(usage.Free), telemetry.UnitBytes))
	}
	return samples, nil
}

// Fetch returns disk io metrics for all discovered hardware disks
func (dpj *DiskIOPollingJob) Fetch() ([]*telemetry.Sample, error) {
	samples := []*telemetry.Sample{}

	// global IO stat collection
	curIOCounterStats, err := disk.IOCounters([]string{}...)
	if err != nil {
		return nil, err
	}
	// loop over counter stats and generate disk IO samples
	for deviceName, curStat := range curIOCounterStats {
		prevStat, found := dpj.lastIOCounterStats[deviceName]
		if found {
			// generate stats from a diff, only if the prevStat was found
			samples = append(samples, diskIOSamplesFromDiff(curStat, prevStat)...)
		}
		// stash the current stats as the next collection's 'previous'
		dpj.lastIOCounterStats[deviceName] = curStat
	}

	return samples, nil
}

func baseDiskUsageSample(metricName, mountpoint string, value float64, unit string) *telemetry.Sample {
	metricID := telemetry.NewMetricID(metricName)
	metricID.AddDimension("Mountpoint", mountpoint)
	return &telemetry.Sample{
		MetricID: *metricID,
		// Ensure we only return positive numbers
		Value: math.Max(value, 0.0),
		Unit:  unit,
	}
}

func baseDiskIOSample(metricName, diskName string, value float64, unit string) *telemetry.Sample {
	metricID := telemetry.NewMetricID(metricName)
	metricID.AddDimension("Device", diskName)
	return &telemetry.Sample{
		MetricID: *metricID,
		// Ensure we only return positive numbers
		Value: math.Max(value, 0.0),
		Unit:  unit,
	}
}

// take a diff of the previous statistics from the current statistics, and return
// the result in a new IOCountersStat struct
func diskIOSamplesFromDiff(curStat, prevStat disk.IOCountersStat) []*telemetry.Sample {
	diffStat := disk.IOCountersStat{}

	device := curStat.Name
	// generate diff
	// NOTE: these are monotonically increasing uint64's but this is fine
	// because any overflow will still calculate correctly in golang.
	diffStat.ReadBytes = curStat.ReadBytes - prevStat.ReadBytes
	diffStat.WriteBytes = curStat.WriteBytes - prevStat.WriteBytes
	diffStat.ReadTime = curStat.ReadTime - prevStat.ReadTime
	diffStat.WriteTime = curStat.WriteTime - prevStat.WriteTime
	diffStat.IoTime = curStat.IoTime - prevStat.IoTime

	// generate samples from diff
	// all times are in ms, so divide by 1000
	samples := []*telemetry.Sample{}
	samples = append(samples, baseDiskIOSample("DiskIOReadBytes", device, float64(diffStat.ReadBytes), telemetry.UnitBytes))
	samples = append(samples, baseDiskIOSample("DiskIOWriteBytes", device, float64(diffStat.WriteBytes), telemetry.UnitBytes))
	samples = append(samples, baseDiskIOSample("DiskIOReadTime", device, float64(diffStat.ReadTime)/1000, telemetry.UnitSeconds))
	samples = append(samples, baseDiskIOSample("DiskIOWriteTime", device, float64(diffStat.WriteTime)/1000, telemetry.UnitSeconds))
	samples = append(samples, baseDiskIOSample("DiskIOIoTime", device, float64(diffStat.IoTime)/1000, telemetry.UnitSeconds))
	return samples

}
