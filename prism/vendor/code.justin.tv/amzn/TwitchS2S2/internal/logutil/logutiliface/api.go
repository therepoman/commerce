package logutiliface

// LoggerAPI is what logutil.Logger implements
type LoggerAPI interface {
	LogAnonymousRequest(_, _, _, _ string)
}
