package authorizationiface

import (
	"context"

	"code.justin.tv/amzn/TwitchS2S2/internal/authorization"
)

// AuthorizationsAPI is the interface of authorization.Authorizations.
type AuthorizationsAPI interface {
	Validate(ctx context.Context, authorizationType, authorizationToken string) (*authorization.Authorization, error)
}
