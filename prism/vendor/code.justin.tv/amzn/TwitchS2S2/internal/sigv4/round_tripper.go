package sigv4

import (
	"io"
	"net/http"
	"time"
)

// Signer is the v4.Signer interface.
type Signer interface {
	Sign(r *http.Request, body io.ReadSeeker, service, region string, signTime time.Time) (http.Header, error)
}

// RoundTripper signs requests via SigV4 before sending requests to a backend.
type RoundTripper struct {
	http.RoundTripper

	AWSRegion  string
	AWSService string
	Signer     Signer
}

// RoundTrip implements http.RoundTripper
func (rt *RoundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	if _, err := rt.Signer.Sign(req, nil, rt.AWSService, rt.AWSRegion, time.Now()); err != nil {
		return nil, err
	}
	return rt.RoundTripper.RoundTrip(req)
}
