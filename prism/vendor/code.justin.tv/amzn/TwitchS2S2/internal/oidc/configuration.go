package oidc

// Configuration is the response OIDC Discovery document.
// See https://auth0.com/docs/protocols/oidc/openid-connect-discovery
type Configuration struct {
	IssuerID              string `json:"issuer"`
	AuthorizationEndpoint string `json:"authorization_endpoint"`
	TokenEndpoint         string `json:"token_endpoint"`
	JwksURI               string `json:"jwks_uri"`
}
