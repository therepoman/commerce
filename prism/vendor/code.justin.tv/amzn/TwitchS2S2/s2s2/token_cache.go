package s2s2

import (
	"time"

	"code.justin.tv/amzn/TwitchS2S2/internal/token"
)

func defaultCacheKey(in *token.Options) string {
	return in.Host()
}

func defaultCacheValueStale(_ *token.Options, in *token.Token) bool {
	return time.Now().After(in.Issued.Add(15 * time.Minute))
}

func defaultCacheValueExpired(_ *token.Options, in *token.Token) bool {
	return time.Now().After(in.Issued.Add(in.ExpiresIn))
}

func accessTokenStale(o *token.Options, in *token.Token) bool {
	return defaultCacheValueStale(nil, in) || !in.Scope.Contains(o.Scope())
}

func accessTokenExpired(o *token.Options, in *token.Token) bool {
	return defaultCacheValueExpired(nil, in) || !in.Scope.Contains(o.Scope())
}
