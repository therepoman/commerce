package s2s2

import (
	"net/http"
)

// Handler are the handlers used by S2S
type Handler interface {
	http.Handler

	// DEPRECATED: use RecordMetricsOnly
	PassthroughIfAuthorizationNotPresented() http.Handler

	// Returns a Handler to use for onboarding services on s2s2.
	// This will records metrics on how many callers are sending
	// valid s2s2 headers for onboarding sevices to s2s2.
	// Requests will not be rejected if invalid or no s2s2 auth
	// header is presented.
	RecordMetricsOnly() http.Handler
}
