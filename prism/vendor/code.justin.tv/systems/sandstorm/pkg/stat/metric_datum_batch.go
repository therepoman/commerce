package stat

import "github.com/aws/aws-sdk-go/service/cloudwatch"

type metricDatumBatch struct {
	Items []*cloudwatch.MetricDatum
}

func (b *metricDatumBatch) HasMore() bool {
	return len(b.Items) > 0
}

func (b *metricDatumBatch) Pop(maxLength int) []*cloudwatch.MetricDatum {
	if len(b.Items) <= maxLength {
		items := b.Items
		b.Items = nil
		return items
	}

	items := b.Items[:maxLength]
	b.Items = b.Items[maxLength:]

	return items
}
