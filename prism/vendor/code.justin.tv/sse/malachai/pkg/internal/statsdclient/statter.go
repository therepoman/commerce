package statsdclient

import (
	"fmt"
	"time"

	"code.justin.tv/sse/malachai/pkg/config"
	"github.com/cactus/go-statsd-client/statsd"
)

// NewStatter instantiates a new statter
func NewStatter(environment, serviceName string, noop bool) (statter statsd.Statter, err error) {
	if environment == "testing" || noop {
		statter, err = statsd.NewNoopClient()
		return
	}

	resources := config.GetResourcesForProduction()
	prefix := fmt.Sprintf("%s.%s.%s.%s", "malachai", environment, resources.Region, serviceName)

	statter, err = statsd.NewBufferedClient("statsd.internal.justin.tv:8125", prefix, time.Second, 512)
	return
}
