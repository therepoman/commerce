package events

import (
	"time"

	"code.justin.tv/sse/malachai/pkg/internal/closer"
	"code.justin.tv/sse/malachai/pkg/internal/fhevent"
	"code.justin.tv/sse/malachai/pkg/log"
)

// NewEventLogger constructs an in memory event logger
func NewEventLogger(cfg Config, logger log.S2SLogger) (*Client, error) {
	if err := cfg.fillDefaults(); err != nil {
		return nil, err
	}

	if logger == nil {
		logger = &log.NoOpLogger{}
	}

	closer := closer.New()

	client := &Client{
		BatchShipper: &batchShipper{
			Closer: closer,
			EventWriter: &fhevent.Client{
				Logger: logger,
			},
			Logger:                     logger,
			MaxItemsInBuffer:           cfg.maxItemsInBuffer(),
			BatchFlushRate:             time.Minute,
			ItemFlushRate:              time.Minute,
			FirehoseDeliveryStreamName: cfg.firehoseDeliveryStreamName(),
			MaxFirehoseFlushTime:       time.Minute,
			FirehoseRetryRate:          100 * time.Millisecond,
		},
		Closer: closer,
		Logger: logger,
		cfg:    cfg,
	}

	return client, nil
}
