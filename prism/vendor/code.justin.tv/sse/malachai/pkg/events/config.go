package events

import (
	"time"

	"code.justin.tv/sse/malachai/pkg/config"
	"code.justin.tv/sse/malachai/pkg/internal/fhevent"
)

// Config holds configuration options for EventLogger
type Config struct {
	Environment string

	// optional. Number of bytes to limit memory to.
	// by default, we will limit to 1024 records (500KB per record).
	BufferSizeLimit int64

	// deprecated: this is not used.
	ReportInterval time.Duration

	resources *config.Resources
}

func (cfg *Config) fillDefaults() (err error) {
	if cfg.Environment == "" {
		cfg.Environment = "production"
	}

	res, err := config.GetResources(cfg.Environment)
	if err != nil {
		return
	}

	cfg.resources = res

	return
}

// maxItemsInBuffer returns the number of items were willing to buffer in memory
// before start seeing signature latency drop.
func (cfg *Config) maxItemsInBuffer() int {
	if cfg.BufferSizeLimit == 0 {
		return 1024
	}

	return int(cfg.BufferSizeLimit / fhevent.FirehoseMaxRecordSizeInBytes)
}

func (cfg *Config) firehoseDeliveryStreamName() string {
	return cfg.resources.FirehoseDeliveryStreamName
}

func (cfg *Config) firehoseRegion() string {
	return cfg.resources.Region
}
