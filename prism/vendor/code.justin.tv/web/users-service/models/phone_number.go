package models

import (
	"time"
)

type PhoneNumberProperties struct {
	PhoneNumber string
	Code        string
	SentAt      time.Time
}

type PhoneNumberCodeProperties struct {
	Code string `json:"code"`
}
