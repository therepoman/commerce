package telemetry

import "time"

// ReporterConfig represents configurable values for telemetry reporter
// Interval: the frequency at which metrics are flushed (flush period)
// Size: the size of the buffer
// Period: the time resolution needed
type ReporterConfig struct {
	Interval time.Duration
	Size     int
	Period   time.Duration
}
