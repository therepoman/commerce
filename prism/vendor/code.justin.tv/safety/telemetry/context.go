package telemetry

import (
	"context"
	"fmt"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"
)

type key int

const sampleReporterCtxKey key = iota

// WithSampleReporter returns a new context with the given sample reporter.
// The sample reporter can be later retrieved with FromContext function
func WithSampleReporter(ctx context.Context, sampleReporter *telemetry.SampleReporter) context.Context {
	return context.WithValue(ctx, sampleReporterCtxKey, sampleReporter)
}

// FromContext returns the sample reporter associated with the context.
// It must be set with the WithSampleReporter function or this function will panic if reporter is not already set in context
func FromContext(ctx context.Context) *telemetry.SampleReporter {
	reporter := ctx.Value(sampleReporterCtxKey)
	if reporter == nil {
		panic("SampleReporter is not set in context")
	}

	sr, ok := reporter.(*telemetry.SampleReporter)
	if !ok {
		panic(fmt.Sprintf("SampleReporter stored in context has invalid type %T", reporter))
	}

	return sr
}
