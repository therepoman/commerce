package session

import (
	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
)

func Corrected(original client.ConfigProvider) (client.ConfigProvider, error) {
	configCopyWithSTSRegionalEndpoint := original.ClientConfig(sts.ServiceName).Config.Copy().WithSTSRegionalEndpoint(endpoints.RegionalSTSEndpoint)
	return session.NewSession(configCopyWithSTSRegionalEndpoint)
}
