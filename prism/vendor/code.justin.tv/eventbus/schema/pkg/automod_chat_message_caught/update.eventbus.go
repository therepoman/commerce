// NOTE this is a generated file! do not edit!

package automod_chat_message_caught

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	UpdateEventType = "AutomodChatMessageCaughtUpdate"
)

type Update = AutomodChatMessageCaughtUpdate

type AutomodChatMessageCaughtUpdateHandler func(context.Context, *eventbus.Header, *AutomodChatMessageCaughtUpdate) error

func (h AutomodChatMessageCaughtUpdateHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &AutomodChatMessageCaughtUpdate{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterAutomodChatMessageCaughtUpdateHandler(mux *eventbus.Mux, f AutomodChatMessageCaughtUpdateHandler) {
	mux.RegisterHandler(UpdateEventType, f.Handler())
}

func RegisterUpdateHandler(mux *eventbus.Mux, f AutomodChatMessageCaughtUpdateHandler) {
	RegisterAutomodChatMessageCaughtUpdateHandler(mux, f)
}

func (*AutomodChatMessageCaughtUpdate) EventBusName() string {
	return UpdateEventType
}
