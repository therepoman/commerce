// NOTE this is a generated file! do not edit!

package automod_chat_message_caught

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	CreateEventType = "AutomodChatMessageCaughtCreate"
)

type Create = AutomodChatMessageCaughtCreate

type AutomodChatMessageCaughtCreateHandler func(context.Context, *eventbus.Header, *AutomodChatMessageCaughtCreate) error

func (h AutomodChatMessageCaughtCreateHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &AutomodChatMessageCaughtCreate{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterAutomodChatMessageCaughtCreateHandler(mux *eventbus.Mux, f AutomodChatMessageCaughtCreateHandler) {
	mux.RegisterHandler(CreateEventType, f.Handler())
}

func RegisterCreateHandler(mux *eventbus.Mux, f AutomodChatMessageCaughtCreateHandler) {
	RegisterAutomodChatMessageCaughtCreateHandler(mux, f)
}

func (*AutomodChatMessageCaughtCreate) EventBusName() string {
	return CreateEventType
}
