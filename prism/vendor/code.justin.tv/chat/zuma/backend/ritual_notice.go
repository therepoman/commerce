package backend

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"code.justin.tv/chat/golibs/errx"
	tmi "code.justin.tv/chat/tmi/client"
	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/chat/zuma/internal/models"
)

const (
	// Hard coded values for the new_chatter ritual
	ritualUserNoticeMessageID   = "ritual"
	ritualUserNoticeTypeID      = "ritual-name"
	ritualUserNoticeReplaceText = "{name}"
	ritualUserNoticeTemplate    = "@{name} is new here. Say hello!"
)

func (b *backenderImpl) SendRitualNotice(ctx context.Context, displayName, messageBody string, t models.RitualToken) error {
	userID, err := strconv.Atoi(t.UserID)
	if err != nil {
		return errx.Wrap(err, fmt.Sprintf("non-integer user ID: %v", t.UserID))
	}

	channelID, err := strconv.Atoi(t.ChannelID)
	if err != nil {
		return errx.Wrap(err, fmt.Sprintf("non-integer channel ID: %v", t.ChannelID))
	}

	ritualName := formatRitualName(t.RitualType)

	err = b.clients.Clue.SendUserNotice(ctx, tmi.SendUserNoticeParams{
		SenderUserID:      userID,
		TargetChannelID:   channelID,
		Body:              messageBody,
		DefaultSystemBody: strings.Replace(ritualUserNoticeTemplate, ritualUserNoticeReplaceText, displayName, -1),
		MsgID:             ritualUserNoticeMessageID,
		MsgParams: []tmi.UserNoticeMsgParam{
			{
				Key:   ritualUserNoticeTypeID,
				Value: ritualName,
			},
		},
	}, nil)

	if err != nil {
		return errx.Wrap(err, "sending ritual notice to Clue")
	}
	return nil
}

func formatRitualName(ritualType string) string {
	switch ritualType {
	case api.RitualTypeNewChatter:
		// mobile clients expect the string `new_chatter`
		return strings.ToLower(ritualType)
	default:
		return ritualType
	}
}
