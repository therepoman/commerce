package api

const (
	SNSAddChannelModeratorEventName    = "add_channel_moderator"
	SNSRemoveChannelModeratorEventName = "remove_channel_moderator"
	SNSAddChannelVIPEventName          = "add_channel_vip"
	SNSRemoveChannelVIPEventName       = "remove_channel_vip"
)

// AddChannelModeratorSNSEvent is published when a user is added as a channel moderator
type AddChannelModeratorSNSEvent struct {
	ChannelID        string `json:"channel_id"`
	TargetUserID     string `json:"target_user_id"`
	RequestingUserID string `json:"requesting_user_id"`
}

// RemoveChannelModeratorSNSEvent is published when a user is removed as a channel moderator
type RemoveChannelModeratorSNSEvent struct {
	ChannelID        string `json:"channel_id"`
	TargetUserID     string `json:"target_user_id"`
	RequestingUserID string `json:"requesting_user_id"`
}

// AddChannelVIPSNSEvent is published when a user is added as a channel VIP
type AddChannelVIPSNSEvent struct {
	ChannelID        string `json:"channel_id"`
	TargetUserID     string `json:"target_user_id"`
	RequestingUserID string `json:"requesting_user_id"`
}

// RemoveChannelVIPSNSEvent is published when a user is removed as a channel VIP
type RemoveChannelVIPSNSEvent struct {
	ChannelID        string `json:"channel_id"`
	TargetUserID     string `json:"target_user_id"`
	RequestingUserID string `json:"requesting_user_id"`
}
