package api

import (
	"net"
	"time"

	"code.justin.tv/feeds/feeds-common/entity"
)

// ExtractMessageRequest is the body for the POST /v1/maap/extraction/extract endpoint
type ExtractMessageRequest struct {
	// SenderID is the user sending the message.
	SenderID string `json:"sender_id"`
	// MessageText is the message body.
	MessageText string `json:"message_text"`
	// ContainerOwner is the entity that owns the container the message will be
	// published to, if any.
	// For example, chatrooms have owners, but whispers do not.
	ContainerOwner *entity.Entity `json:"container_owner_id"`

	// Optional arguments:
	Namespace string `json:"namespace"`
	// Max allowed character length in the message text before truncation.
	MaxCharacterLength *int `json:"max_character_length"`
	// Enforcement Rules to apply to this extraction
	EnforcementRules []string `json:"enforcement_rules"`
	// OmitDefaultEnforcements allows you to omit default enforcement rules
	OmitDefaultEnforcements bool `json:"override_all_enforcement_rules"`
	// AlwaysReturn says that even if a rule fails, we should still give back the full response
	AlwaysReturn bool `json:"always_return"`
	// ClientIP contains the IP address of the client making the request, to be used
	// by the Is IP blocked enforcement, if ommitted the enforcement will succeed
	ClientIP *net.IP `json:"client_ip"`
}

// ExtractMessageResponse is the response for the POST /v1/maap/extraction/extract endpoint
type ExtractMessageResponse struct {
	Container MessageContainer `json:"container"`
	Risk      MessageRisk      `json:"risk"`
	Content   MessageContent   `json:"content"`
	Sender    MessageSender    `json:"sender"`
}

// EnforceMessageRequest is the body for the POST /v2/maap/messages/enforce endpoint
type EnforceMessageRequest struct {
	// SenderID is the user sending the message.
	SenderID string `json:"sender_id"`
	// MessageText is the message body.
	MessageText string `json:"message_text"`
	// ContainerOwner is the entity that owns the container the message will be
	// published to, if any.
	// For example, chatrooms have owners, but whispers do not.
	ContainerOwner *entity.Entity `json:"container_owner_id"`
	// ClientIP is the IP address of the user sending the message, if available.
	// Used for checking if the IP address is blocked.
	ClientIP *net.IP `json:"client_ip"`

	// EnforcementRules defines which enforcement rules to run on this message.
	EnforcementRules EnforcementRuleset `json:"enforcement_rules"`
	// EnforcementConfig lets you tune various enforcement rules with additional parameters
	EnforcementConfig EnforcementConfiguration `json:"enforcement_configuration"`

	// MaxCharacterLength, if set, will truncate the message text.
	// Defaults to 1000 runes (not bytes).
	MaxCharacterLength *int `json:"max_character_length"`

	// SkipExtraction will not include the parsed message text in the response.
	// Set this to true if the client only plans to use the enforcement check.
	SkipExtraction bool `json:"skip_extraction"`

	// ParseCheermotes sets whether messages should be parsed for cheermotes. Should only be
	// set if the message has been approved by Payday.
	ParseCheermotes bool `json:"parse_cheermotes"`
}

// EnforceMessageResponse is the POST'd response for /v2/maap/extraction/enforce endpoint
type EnforceMessageResponse struct {
	ExtractProperties `json:"extract"`
	EnforceProperties `json:"enforce"`
}

// ListMessagesBySenderRequest is the body for the POST /v1/maap/messages/list_by_sender endpoint.
type ListMessagesBySenderRequest struct {
	SenderID       string        `json:"sender_id" validate:"nonzero"`
	ContainerOwner entity.Entity `json:"owner_id" validate:"nonzero"`
	Limit          int           `json:"limit" validate:"min=1,max=1000"`
	Cursor         string        `json:"cursor"`
	SortAscending  bool          `json:"sort_ascending"`
	Namespace      string        `json:"namespace"`
	// If set, return only messages where the IsPublished field matches this value.
	FilterIsPublished *bool `json:"filter_is_published"`
}

// ListMessagesBySenderResponse is the response for the POST /v1/maap/messages/list_by_sender endpoint.
type ListMessagesBySenderResponse struct {
	Results         []MessageResult `json:"results"`
	HasPreviousPage bool            `json:"has_previous_page"`
	HasNextPage     bool            `json:"has_next_page"`
}

// GetMessageRequest is the body for the POST /v1/maap/messages/get endpoint.
type GetMessageRequest struct {
	Namespace string `json:"namespace"`
	MessageID string `json:"message_id"`
	// If set, the History field will be populated in the response with a list of edits and deletes.
	// This should only be used for moderation purposes, and should not be exposed to regular users.
	IncludeHistory bool `json:"include_history"`
}

// GetMessageResponse is the response for the POST /v1/maap/messages/get endpoint.
type GetMessageResponse struct {
	Message Message `json:"message"`
}

// CreateMessageRequest is the body for the POST /v1/maap/messages/create endpoint.
type CreateMessageRequest struct {
	ContainerID     string         `json:"container_id"`
	Namespace       string         `json:"namespace"`
	ParentMessageID string         `json:"parent_message_id"` // ParentMessageID is optional. This can be left as blank
	Content         MessageContent `json:"content"`
	Sender          MessageSender  `json:"sender"`
	BitsAmount      int            `json:"bits_amount"`
	Databag         Databag        `json:"databag"`

	// OrderByKey is an optional string that will be used for sorting/indexing
	// in the backend datastore. If not set, OrderByKey will default to the
	// message creation time in Unix nanoseconds.
	//
	// NOTE: It is the caller's responsibility to ensure this string is natively
	// sortable. For example, numbers are NOT sortable unless they are left-padded
	// to the same length (example: "400" > "1000", but "0400" < "1000").
	OrderByKey *string `json:"order_by_key"`

	// SentAtTimestampOverride is an optional time that the message would be created at.
	// Used to backfill MaaP and should not be used without consulting communications team.
	// Default is time.Now()
	SentAtTimestampOverride *time.Time `json:"sent_at_timestamp_override"`

	// MessageID an optional message id to be used when the message is created as
	// a result of a backfill or sent from stream chat.
	MessageID *string `json:"message_id"`
}

// CreateMessageResponse is the response for the POST /v1/maap/messages/create endpoint.
type CreateMessageResponse GetMessageResponse

// BulkUpdateMessagesRequest is the body for the POST /v1/maap/messages/bulk_update endpoint.
type BulkUpdateMessagesRequest struct {
	UpdaterID   string   `json:"updater_id" validate:"nonzero"`
	MessageIDs  []string `json:"message_ids" validate:"min=1,max=1000"`
	IsPublished *bool    `json:"is_published"`
	IsDeleted   *bool    `json:"is_deleted"`
}

// BulkUpdateMessagesResponse is the response for the POST /v1/maap/messages/bulk_update endpoint.
type BulkUpdateMessagesResponse struct {
	UnprocessedMessageIDs []string `json:"unprocessed_message_ids"`
	Errors                []string `json:"errors"`
}

type containerResponse struct {
	Container Container `json:"container"`
}

// GetContainerByUniqueNameRequest is the body for the POST /v1/maap/containers/get_by_name endpoint.
type GetContainerByUniqueNameRequest struct {
	Namespace  string `json:"namespace"`
	UniqueName string `json:"unique_name"`
}

// GetContainerByUniqueNameResponse is the response for the POST /v1/maap/containers/get_by_name endpoint.
type GetContainerByUniqueNameResponse containerResponse

// CreateContainerRequest is the body for the POST /v1/maap/containers/create endpoint.
type CreateContainerRequest struct {
	Namespace      string         `json:"namespace"`
	UniqueName     string         `json:"unique_name"`
	ContainerOwner *entity.Entity `json:"owner_id"`
	RequireReview  string         `json:"require_review"`
	Databag        Databag        `json:"databag"`
}

// CreateContainerResponse is the response for the POST /v1/maap/containers/create endpoint.
type CreateContainerResponse containerResponse

// UpdateContainerRequest is the body for the POST /v1/maap/containers/update endpoint.
type UpdateContainerRequest struct {
	ID             string               `json:"id"`
	Namespace      string               `json:"namespace"`
	UniqueName     *string              `json:"unique_name"`
	ContainerOwner *entity.Entity       `json:"owner_id"`
	RequireReview  *string              `json:"require_review"`
	DatabagUpdates []DatabagFieldUpdate `json:"databag_updates"`
}

// UpdateContainerResponse is the response for the POST /v1/maap/containers/update endpoint.
type UpdateContainerResponse containerResponse
