package api

import (
	"time"
)

const (
	ErrCodeInvalidCursor = "invalid_cursor"
)

// GetModRequest is the body for the POST /v1/mods/get endpoint
type GetModRequest struct {
	ChannelID string `json:"channel_id"`
	UserID    string `json:"user_id"`
}

// GetModResponse is the response from the POST /v1/mods/get endpoint
type GetModResponse struct {
	IsMod     bool       `json:"is_mod"`
	CreatedAt *time.Time `json:"created_at"`
}

// ListModsRequest is the body for the POST /v1/mods/list endpoint
type ListModsRequest struct {
	ChannelID string `json:"channel_id"`
}

// ListModsResponse is the response from the POST /v1/mods/list endpoint
type ListModsResponse struct {
	Mods []string `json:"mods"`
}

// ListModsV2Request is the body for the POST /v2/mods/list endpoint
type ListModsV2Request struct {
	ChannelID string `json:"channel_id"`
	Cursor    string `json:"cursor"`
	Limit     int    `json:"limit"`
}

// ListModsV2Response is the response from the POST /v2/mods/list endpoint
type ListModsV2Response struct {
	ModResults []ModResult `json:"mod_results"`
}

// ModResult an element in a paginated list of moderators
type ModResult struct {
	Mod    Mod    `json:"mod"`
	Cursor string `json:"cursor"`
}

// Mod is a moderator in a list of mods
type Mod struct {
	UserID    string     `json:"user_id"`
	CreatedAt *time.Time `json:"created_at"`
}

// AddModRequest is the body for the POST /v1/mods/add endpoint.
type AddModRequest struct {
	ChannelID        string `json:"channel_id"`
	TargetUserID     string `json:"target_user_id"`
	RequestingUserID string `json:"requesting_user_id"`
}

// AddModResponse is the response for the POST /v1/mods/add endpoint.
type AddModResponse struct {
	// ErrorCode is an enum that describes the type of logical error that occurred.
	// Nil if the operation was successful.
	ErrorCode *string `json:"error_code,omitempty"`
}

// RemoveModRequest is the body for the POST /v1/mods/remove endpoint
type RemoveModRequest struct {
	ChannelID        string `json:"channel_id"`
	TargetUserID     string `json:"target_user_id"`
	RequestingUserID string `json:"requesting_user_id"`
}

// RemoveModResponse is the response for the POST /v1/mods/remove endpoint.
type RemoveModResponse AddModResponse

// ListUserBlocksParams is the body for the POST /v1/users/blocks/get endpoint
type ListUserBlocksParams struct {
	UserID string `json:"user_id"`
	Cursor string `json:"cursor"`
	Limit  int    `json:"limit"`
}

// ListUserBlocksResponse is the response for POST /v1/users/blocks/get endpoint
type ListUserBlocksResponse struct {
	BlockedUserIDs []string `json:"blocked_user_ids"`
	Cursor         string   `json:"cursor"`
}

// ListUserBlockersParams is the body for the POST /v1/users/blocks/blockers endpoint
type ListUserBlockersParams struct {
	UserID string `json:"user_id"`
}

// ListUserBlockersResponse is the response for POST /v1/users/blocks/blockers endpoint
type ListUserBlockersResponse struct {
	BlockerUserIDs []string `json:"blocker_user_ids"`
}

// AddUserBlockParams is the body for the POST /v1/users/blocks/add endpoint
type AddUserBlockParams struct {
	UserID       string `json:"user_id"`
	TargetUserID string `json:"target_user_id"`
	Reason       string `json:"reason"`
	// SourceContext identifies what user flow initiated the block (e.g. chat, whisper).
	SourceContext string `json:"source_context"`
}

// AddUserBlockResponse is the response for the POST /v1/users/blocks/add endpoint
type AddUserBlockResponse struct {
	Status  int    `json:"status"`
	Error   string `json:"error"`
	Message string `json:"message"`
}

// IsBlockedParams is the body for the POST /v1/users/blocks/is_blocked endpoint
type IsBlockedParams struct {
	UserID        string `json:"user_id"`
	BlockedUserID string `json:"blocked_user_id"`
}

// IsBlockedResponse is the response from POST /v1/user/blocks/is_blocked
type IsBlockedResponse struct {
	UserID        string `json:"user_id"`
	BlockedUserID string `json:"blocked_user_id"`
	IsBlocked     bool   `json:"is_blocked"`
}

// BulkIsBlockedParams is the body for the POST /v1/users/blocks/bulk endpoint
type BulkIsBlockedParams struct {
	UserID        string   `json:"user_id"`
	TargetUserIDs []string `json:"target_user_ids"`
	Bidirectional bool     `json:"bidirectional"` // if true, will also check if any users in TargetUserIDs have blocked user with UserID
}

// BulkIsBlockedResponse is the response for POST /v1/users/blocks/bulk_is_blocked endpoint.
// It tells whether a user is blocking any target users, or vice-versa
// If there's a lookup error, we log it but do not return it. Instead we assume user(s) that failed lookup are not blocked
type BulkIsBlockedResponse struct {
	IsBlockedByUser map[string]bool `json:"is_blocked_by_user"` // tells us which target users a user has blocked
	IsBlockingUser  map[string]bool `json:"is_blocking_user"`   // if request was bidirectional, this map will tell us which target users have blocked user
}

// UserRoleRequest is the body for the POST /v1/users/roles/get endpoint
type UserRoleRequest struct {
	ChannelID string `json:"channel_id"`
	UserID    string `json:"user_id"`
}

// UserRoleResponse is the response from POST /v1/users/roles/get
type UserRoleResponse struct {
	Role string `json:"role"`
}

// Various roles for UserRoleResponse.Role:
const (
	UserRoleBroadcaster = "broadcaster"
	UserRoleModerator   = "mod"
	UserRoleVIP         = "vip"
)

// RemoveUserBlockParams is the body for the POST /v1/users/blocks/remove endpoint
type RemoveUserBlockParams struct {
	UserID       string `json:"user_id"`
	TargetUserID string `json:"target_user_id"`
}

// AutoModCheckUsernameRequest is the body for the POST /v1/entities/username/check
type AutoModCheckUsernameRequest struct {
	Username                    string  `json:"username"`
	ChannelID                   string  `json:"channel_id"`
	OverrideAutoModLevel        *int    `json:"override_automod_level"`
	OverrideAutoModLanguageCode *string `json:"override_automod_language_code"`
	DeviceID                    string  `json:"device_id"`
}

// AutoModCheckUsernameResponse is the response from POST /v1/entities/username/check
type AutomodCheckUsernameResponse struct {
	IsOK bool `json:"is_ok"`
}

// GetVIPRequest is the body for the POST /v1/vips/get endpoint
type GetVIPRequest struct {
	ChannelID string `json:"channel_id"`
	UserID    string `json:"user_id"`
}

// GetVIPResponse is the response from the POST /v1/vips/get endpoint
type GetVIPResponse struct {
	IsVIP     bool       `json:"is_vip"`
	GrantedAt *time.Time `json:"granted_at"`
}

// ListVIPsRequest is the body for the POST /v1/vips/list endpoint
type ListVIPsRequest struct {
	ChannelID string `json:"channel_id"`
	Cursor    string `json:"cursor"`
	Limit     int    `json:"limit"`
}

// ListVIPsResponse is the response from the POST /v1/vips/list endpoint
type ListVIPsResponse struct {
	VIPs []VIPResult `json:"vips"`
}

// VIPResult an element in a paginated list of vips
type VIPResult struct {
	VIP    VIP    `json:"vip"`
	Cursor string `json:"cursor"`
}

// VIP element with UserID for list of vips.
type VIP struct {
	UserID    string     `json:"user_id"`
	GrantedAt *time.Time `json:"granted_at"`
}

// AddVIPRequest is the body for the POST /v1/vips/add endpoint.
type AddVIPRequest struct {
	ChannelID        string `json:"channel_id"`
	TargetUserID     string `json:"target_user_id"`
	RequestingUserID string `json:"requesting_user_id"`
}

// AddVIPResponse is the response for the POST /v1/vips/add endpoint.
type AddVIPResponse struct {
	// ErrorCode is an enum that describes the type of logical error that occurred.
	// Nil if the operation was successful.
	ErrorCode *string `json:"error_code,omitempty"`
}

// RemoveVIPRequest is the body for the POST /v1/vips/remove endpoint.
type RemoveVIPRequest struct {
	ChannelID        string `json:"channel_id"`
	TargetUserID     string `json:"target_user_id"`
	RequestingUserID string `json:"requesting_user_id"`
}

// RemoveVIPResponse is the response for the POST /v1/vips/remove endpoint.
type RemoveVIPResponse AddVIPResponse
