package zuma

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

const (
	defaultStatSampleRate = 0.1
	defaultTimingXactName = "zuma"
)

// Client is the interface for the Zuma go client
type Client interface {
	GetMod(ctx context.Context, channelID, userID string, reqOpts *twitchclient.ReqOpts) (api.GetModResponse, error)
	// ListMods is deprecated. ListModsV2 shoudld be used instead
	ListMods(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (api.ListModsResponse, error)
	ListModsV2(ctx context.Context, channelID, cursor string, limit int, reqOpts *twitchclient.ReqOpts) (api.ListModsV2Response, error)
	AddMod(ctx context.Context, channelID, targetUserID, requestingUserID string, reqOpts *twitchclient.ReqOpts) (api.AddModResponse, error)
	RemoveMod(ctx context.Context, channelID, targetUserID, requestingUserID string, reqOpts *twitchclient.ReqOpts) (api.RemoveModResponse, error)

	GetUserChatProperties(ctx context.Context, params api.UserChatPropertiesRequest, reqOpts *twitchclient.ReqOpts) (api.UserChatPropertiesResponse, error)
	ModifyUserChatProperties(ctx context.Context, params api.ModifyUserChatPropertiesRequest, reqOpts *twitchclient.ReqOpts) error

	GetVIP(ctx context.Context, channelID, userID string, reqOpts *twitchclient.ReqOpts) (api.GetVIPResponse, error)
	ListVIPs(ctx context.Context, channelID, cursor string, limit int, reqOpts *twitchclient.ReqOpts) (api.ListVIPsResponse, error)
	AddVIP(ctx context.Context, channelID, targetUserID, requestingUserID string, reqOpts *twitchclient.ReqOpts) (api.AddVIPResponse, error)
	RemoveVIP(ctx context.Context, channelID, targetUserID, requestingUserID string, reqOpts *twitchclient.ReqOpts) (api.RemoveVIPResponse, error)

	ListUserBlocks(ctx context.Context, params api.ListUserBlocksParams, reqOpts *twitchclient.ReqOpts) (api.ListUserBlocksResponse, error)
	AddUserBlock(ctx context.Context, params api.AddUserBlockParams, reqOpts *twitchclient.ReqOpts) (api.AddUserBlockResponse, error)
	RemoveUserBlock(ctx context.Context, params api.RemoveUserBlockParams, reqOpts *twitchclient.ReqOpts) error
	IsBlocked(ctx context.Context, params api.IsBlockedParams, opts *twitchclient.ReqOpts) (api.IsBlockedResponse, error)
	BulkIsBlocked(ctx context.Context, params api.BulkIsBlockedParams, reqOpts *twitchclient.ReqOpts) (api.BulkIsBlockedResponse, error)
	ListUserBlockers(ctx context.Context, params api.ListUserBlockersParams, opts *twitchclient.ReqOpts) (api.ListUserBlockersResponse, error)

	GetUserRoles(ctx context.Context, params api.UserRoleRequest, reqOpts *twitchclient.ReqOpts) (api.UserRoleResponse, error)

	// Messaging as a Platform (MaaP)

	// Deprecated: Replaced with EnforceMessage
	ExtractMessage(ctx context.Context, params api.ExtractMessageRequest, reqOpts *twitchclient.ReqOpts) (api.ExtractMessageResponse, error)
	// EnforceMessage provides the same functionality as ExtractMessage but with better default behavior, a simpler api and other improvements
	EnforceMessage(ctx context.Context, params api.EnforceMessageRequest, reqOpts *twitchclient.ReqOpts) (api.EnforceMessageResponse, error)
	ListMessagesBySender(ctx context.Context, params api.ListMessagesBySenderRequest, reqOpts *twitchclient.ReqOpts) (api.ListMessagesBySenderResponse, error)
	GetMessage(ctx context.Context, params api.GetMessageRequest, reqOpts *twitchclient.ReqOpts) (api.GetMessageResponse, error)
	CreateMessage(ctx context.Context, params api.CreateMessageRequest, reqOpts *twitchclient.ReqOpts) (api.CreateMessageResponse, error)
	BulkUpdateMessages(ctx context.Context, params api.BulkUpdateMessagesRequest, reqOpts *twitchclient.ReqOpts) (api.BulkUpdateMessagesResponse, error)

	GetContainerByUniqueName(ctx context.Context, params api.GetContainerByUniqueNameRequest, reqOpts *twitchclient.ReqOpts) (api.GetContainerByUniqueNameResponse, error)
	CreateContainer(ctx context.Context, params api.CreateContainerRequest, reqOpts *twitchclient.ReqOpts) (api.CreateContainerResponse, error)
	UpdateContainer(ctx context.Context, params api.UpdateContainerRequest, reqOpts *twitchclient.ReqOpts) (api.UpdateContainerResponse, error)

	// Rituals
	ListRitualTokensByChannel(ctx context.Context, params api.ListRitualTokensByChannelRequest, reqOpts *twitchclient.ReqOpts) (api.ListRitualTokensByChannelResponse, error)
	RequestEligibleRitualToken(ctx context.Context, params api.RequestEligibleRitualTokenRequest, reqOpts *twitchclient.ReqOpts) (api.RequestEligibleRitualTokenResponse, error)
	UpdateRitualToken(ctx context.Context, params api.UpdateRitualTokenRequest, reqOpts *twitchclient.ReqOpts) (api.UpdateRitualTokenResponse, error)

	// Entities
	AutoModCheckUsername(ctx context.Context, params api.AutoModCheckUsernameRequest, reqOpts *twitchclient.ReqOpts) (api.AutomodCheckUsernameResponse, error)
}

type client struct {
	twitchclient.Client
}

// NewClient creates a new Zuma go client
func NewClient(conf twitchclient.ClientConf) (Client, error) {
	if conf.TimingXactName == "" {
		conf.TimingXactName = defaultTimingXactName
	}
	twitchClient, err := twitchclient.NewClient(conf)
	return &client{twitchClient}, err
}

func (c *client) GetMod(ctx context.Context, channelID, userID string, reqOpts *twitchclient.ReqOpts) (api.GetModResponse, error) {
	bodyBytes, err := json.Marshal(api.GetModRequest{
		ChannelID: channelID,
		UserID:    userID,
	})
	if err != nil {
		return api.GetModResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/mods/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetModResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.mods.get",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetModResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.GetModResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetModResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetModResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) ListMods(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (api.ListModsResponse, error) {
	bodyBytes, err := json.Marshal(api.ListModsRequest{
		ChannelID: channelID,
	})
	if err != nil {
		return api.ListModsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/mods/list", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListModsResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.mods.list",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.ListModsResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.ListModsResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.ListModsResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.ListModsResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) ListModsV2(ctx context.Context, channelID, cursor string, limit int, reqOpts *twitchclient.ReqOpts) (api.ListModsV2Response, error) {
	bodyBytes, err := json.Marshal(api.ListModsV2Request{
		ChannelID: channelID,
		Cursor:    cursor,
		Limit:     limit,
	})
	if err != nil {
		return api.ListModsV2Response{}, err
	}

	req, err := c.NewRequest("POST", "/v2/mods/list", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListModsV2Response{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.mods.list_v2",
		StatSampleRate: defaultStatSampleRate,
	})

	decoded := api.ListModsV2Response{}
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.ListModsV2Response{}, err
	}

	return decoded, nil
}

func (c *client) AddMod(ctx context.Context, channelID, targetUserID, requestingUserID string, reqOpts *twitchclient.ReqOpts) (api.AddModResponse, error) {
	bodyBytes, err := json.Marshal(api.AddModRequest{
		ChannelID:        channelID,
		TargetUserID:     targetUserID,
		RequestingUserID: requestingUserID,
	})
	if err != nil {
		return api.AddModResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/mods/add", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.AddModResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.mods.add",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.AddModResponse{}, err
	}
	defer close(resp.Body)

	var decoded api.AddModResponse
	err = decodeErrorCodeResponse(ctx, resp, &decoded)
	if err != nil {
		return api.AddModResponse{}, err
	}

	return decoded, nil
}

func (c *client) RemoveMod(ctx context.Context, channelID, targetUserID, requestingUserID string, reqOpts *twitchclient.ReqOpts) (api.RemoveModResponse, error) {
	bodyBytes, err := json.Marshal(api.RemoveModRequest{
		ChannelID:        channelID,
		TargetUserID:     targetUserID,
		RequestingUserID: requestingUserID,
	})
	if err != nil {
		return api.RemoveModResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/mods/remove", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.RemoveModResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.mods.remove",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.RemoveModResponse{}, err
	}
	defer close(resp.Body)

	var decoded api.RemoveModResponse
	err = decodeErrorCodeResponse(ctx, resp, &decoded)
	if err != nil {
		return api.RemoveModResponse{}, err
	}

	return decoded, nil
}

func (c *client) GetVIP(ctx context.Context, channelID, userID string, reqOpts *twitchclient.ReqOpts) (api.GetVIPResponse, error) {
	bodyBytes, err := json.Marshal(api.GetModRequest{
		ChannelID: channelID,
		UserID:    userID,
	})
	if err != nil {
		return api.GetVIPResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/vips/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.GetVIPResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.vips.get",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.GetVIPResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.GetVIPResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.GetVIPResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.GetVIPResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) ListVIPs(ctx context.Context, channelID, cursor string, limit int, reqOpts *twitchclient.ReqOpts) (api.ListVIPsResponse, error) {
	bodyBytes, err := json.Marshal(api.ListVIPsRequest{
		ChannelID: channelID,
		Cursor:    cursor,
		Limit:     limit,
	})
	if err != nil {
		return api.ListVIPsResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/vips/list", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListVIPsResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.vips.list",
		StatSampleRate: defaultStatSampleRate,
	})

	decoded := api.ListVIPsResponse{}
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.ListVIPsResponse{}, err
	}

	return decoded, nil
}

func (c *client) AddVIP(ctx context.Context, channelID, targetUserID, requestingUserID string, reqOpts *twitchclient.ReqOpts) (api.AddVIPResponse, error) {
	bodyBytes, err := json.Marshal(api.AddModRequest{
		ChannelID:        channelID,
		TargetUserID:     targetUserID,
		RequestingUserID: requestingUserID,
	})
	if err != nil {
		return api.AddVIPResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/vips/add", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.AddVIPResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.vips.add",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.AddVIPResponse{}, err
	}
	defer close(resp.Body)

	var decoded api.AddVIPResponse
	err = decodeErrorCodeResponse(ctx, resp, &decoded)
	if err != nil {
		return api.AddVIPResponse{}, err
	}

	return decoded, nil
}

func (c *client) RemoveVIP(ctx context.Context, channelID, targetUserID, requestingUserID string, reqOpts *twitchclient.ReqOpts) (api.RemoveVIPResponse, error) {
	bodyBytes, err := json.Marshal(api.AddModRequest{
		ChannelID:        channelID,
		TargetUserID:     targetUserID,
		RequestingUserID: requestingUserID,
	})
	if err != nil {
		return api.RemoveVIPResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/vips/remove", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.RemoveVIPResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.vips.remove",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.RemoveVIPResponse{}, err
	}
	defer close(resp.Body)

	var decoded api.RemoveVIPResponse
	err = decodeErrorCodeResponse(ctx, resp, &decoded)
	if err != nil {
		return api.RemoveVIPResponse{}, err
	}

	return decoded, nil
}

func (c *client) ListUserBlocks(ctx context.Context, params api.ListUserBlocksParams, reqOpts *twitchclient.ReqOpts) (api.ListUserBlocksResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ListUserBlocksResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/users/blocks/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListUserBlocksResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.users.list_user_blocks",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.ListUserBlocksResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.ListUserBlocksResponse{}, err
	}
	return decoded, nil
}

func (c *client) ListUserBlockers(ctx context.Context, params api.ListUserBlockersParams, reqOpts *twitchclient.ReqOpts) (api.ListUserBlockersResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ListUserBlockersResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/users/blocks/blockers", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ListUserBlockersResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.users.list_blockers",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.ListUserBlockersResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.ListUserBlockersResponse{}, err
	}
	return decoded, nil
}

func (c *client) IsBlocked(ctx context.Context, params api.IsBlockedParams, reqOpts *twitchclient.ReqOpts) (api.IsBlockedResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.IsBlockedResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/users/blocks/is_blocked", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.IsBlockedResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.users.is_blocked",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.IsBlockedResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.IsBlockedResponse{}, err
	}
	return decoded, nil
}

func (c *client) BulkIsBlocked(ctx context.Context, params api.BulkIsBlockedParams, reqOpts *twitchclient.ReqOpts) (api.BulkIsBlockedResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.BulkIsBlockedResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/users/blocks/bulk_is_blocked", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.BulkIsBlockedResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.users.bulk_is_blocked",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.BulkIsBlockedResponse
	_, err = c.DoJSON(ctx, &decoded, req, combinedReqOpts)
	if err != nil {
		return api.BulkIsBlockedResponse{}, err
	}

	return decoded, nil
}

func (c *client) AddUserBlock(ctx context.Context, params api.AddUserBlockParams, reqOpts *twitchclient.ReqOpts) (api.AddUserBlockResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.AddUserBlockResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/users/blocks/add", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.AddUserBlockResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.users.add_block",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.AddUserBlockResponse{}, err
	}
	defer close(resp.Body)

	// 422 is a valid, expected response and should be handled
	if resp.StatusCode >= 400 && resp.StatusCode != http.StatusUnprocessableEntity {
		return api.AddUserBlockResponse{}, c.errorFromFailedRequest(resp)
	}

	data := api.AddUserBlockResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return api.AddUserBlockResponse{}, err
	}

	return data, nil
}

func (c *client) RemoveUserBlock(ctx context.Context, params api.RemoveUserBlockParams, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/users/blocks/remove", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.users.remove_block",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return c.errorFromFailedRequest(resp)
	}

	return nil
}

func (c *client) GetUserRoles(ctx context.Context, params api.UserRoleRequest, reqOpts *twitchclient.ReqOpts) (api.UserRoleResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.UserRoleResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/users/roles/get", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.UserRoleResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.users.get_roles",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.UserRoleResponse{}, err
	}
	defer close(resp.Body)

	if resp.StatusCode >= 400 {
		return api.UserRoleResponse{}, c.errorFromFailedRequest(resp)
	}

	var data api.UserRoleResponse

	if err = json.NewDecoder(resp.Body).Decode(&data); err != nil {
		return api.UserRoleResponse{}, err
	}

	return data, nil
}

// handleFailedRequest parses the detailed ErrorResponse from Zuma, if one is
// provided
func (c *client) errorFromFailedRequest(resp *http.Response) error {
	var errResp api.ErrorResponse
	err := json.NewDecoder(resp.Body).Decode(&errResp)
	if err != nil {
		// could not decode the Zuma response
		return &api.ErrorResponse{
			Err:    fmt.Sprintf("failed zuma request. StatusCode=%v Unable to read response body (%v)", resp.StatusCode, err),
			Status: resp.StatusCode,
		}
	}
	return &errResp
}

// decodeErrorCodeResponse unmarshals a JSON response into v. This function returns an error
// if the status code is >= 400, unless an error_code field is present in the response.
func decodeErrorCodeResponse(ctx context.Context, resp *http.Response, v interface{}) error {
	body, err := ioutil.ReadAll(resp.Body)
	close(resp.Body)
	if err != nil {
		return err
	}

	// Reassign the body since we can only read the ReadCloser once.
	resp.Body = ioutil.NopCloser(bytes.NewBuffer(body))
	defer close(resp.Body)

	err = json.Unmarshal(body, &v)
	if err != nil {
		return twitchclient.HandleFailedResponse(resp)
	}

	if resp.StatusCode >= 400 {
		var errorCodeResponse api.ErrorCodeResponse
		err = json.Unmarshal(body, &errorCodeResponse)
		if err != nil {
			return err
		}

		if errorCodeResponse.ErrorCode != nil {
			return nil
		}

		return twitchclient.HandleFailedResponse(resp)
	}

	return nil
}

func close(closer io.Closer) {
	if err := closer.Close(); err != nil {
		log.Printf("error closing response body: %v", err)
	}
}
