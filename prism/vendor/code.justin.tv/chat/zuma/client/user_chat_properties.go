package zuma

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) GetUserChatProperties(ctx context.Context, params api.UserChatPropertiesRequest, reqOpts *twitchclient.ReqOpts) (api.UserChatPropertiesResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.UserChatPropertiesResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/users/properties", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.UserChatPropertiesResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.user_chat_properties.get",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return api.UserChatPropertiesResponse{}, err
	}

	defer close(resp.Body)

	data := api.UserChatPropertiesResponse{}
	if err := json.NewDecoder(resp.Body).Decode(&data); err != nil {
		return api.UserChatPropertiesResponse{}, fmt.Errorf("can't read response from zuma: %s", err)
	}

	return data, nil
}

func (c *client) ModifyUserChatProperties(ctx context.Context, params api.ModifyUserChatPropertiesRequest, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/v1/users/set_properties", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.user_chat_properties.set",
		StatSampleRate: defaultStatSampleRate,
	})

	_, err = c.Do(ctx, req, combinedReqOpts)
	return err
}
