package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) AutoModCheckUsername(ctx context.Context, params api.AutoModCheckUsernameRequest, reqOpts *twitchclient.ReqOpts) (api.AutomodCheckUsernameResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.AutomodCheckUsernameResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/entities/username/check", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.AutomodCheckUsernameResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.entities.automod_check_username",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.AutomodCheckUsernameResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.AutomodCheckUsernameResponse{}, err
	}
	return decoded, nil
}
