package zuma

import (
	"bytes"
	"context"
	"encoding/json"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) ExtractMessage(ctx context.Context, params api.ExtractMessageRequest, reqOpts *twitchclient.ReqOpts) (api.ExtractMessageResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.ExtractMessageResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v1/maap/extraction/extract", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.ExtractMessageResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.maap_extraction.extract",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.ExtractMessageResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.ExtractMessageResponse{}, err
	}
	return decoded, nil
}

func (c *client) EnforceMessage(ctx context.Context, params api.EnforceMessageRequest, reqOpts *twitchclient.ReqOpts) (api.EnforceMessageResponse, error) {
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return api.EnforceMessageResponse{}, err
	}

	req, err := c.NewRequest("POST", "/v2/maap/messages/enforce", bytes.NewReader(bodyBytes))
	if err != nil {
		return api.EnforceMessageResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.zuma.maap.enforce_v2",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded api.EnforceMessageResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return api.EnforceMessageResponse{}, err
	}
	return decoded, nil
}
