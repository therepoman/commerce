package chatrooms

import (
	"context"
	"encoding/json"

	"github.com/twitchtv/twirp"
)

const (
	clientErrorMetaKey = "client_error"
	errorCodeCtxKey    = iota
)

// WithErrorCodePtr returns a context that can be used with SetErrorCode and ErrorCodeFromCtx
// to store and later retrieve the error code of the response.
func WithErrorCodePtr(ctx context.Context) context.Context {
	var errorCode string
	return context.WithValue(ctx, errorCodeCtxKey, &errorCode)
}

// SetErrorCode stores the code in the context previously initialized with WithErrorCodePtr.
func SetErrorCode(ctx context.Context, code string) {
	if errorCodePtr, ok := ctx.Value(errorCodeCtxKey).(*string); ok && errorCodePtr != nil {
		*errorCodePtr = code
	}
}

// ErrorCodeFromCtx returns the error code previously set by SetErrorCode. If an error code has not
// been set, returns false.
func ErrorCodeFromCtx(ctx context.Context) (string, bool) {
	if errorCodePtr, ok := ctx.Value(errorCodeCtxKey).(*string); ok && errorCodePtr != nil && *errorCodePtr != "" {
		return *errorCodePtr, true
	}

	return "", false
}

func NewClientError(ctx context.Context, twErr twirp.Error, v interface{}) twirp.Error {
	if code, ok := v.(string); ok {
		v = ClientError{
			ErrorCode: code,
		}
	}

	b, err := json.Marshal(v)
	if err != nil {
		return twErr
	}

	var clientErr ClientError
	json.Unmarshal(b, &clientErr)
	if clientErr.ErrorCode != "" {
		SetErrorCode(ctx, clientErr.ErrorCode)
	}

	return twErr.WithMeta(clientErrorMetaKey, string(b))
}

// ParseClientError takes an error, and if it is a ClientError unmarshals the error set
// by NewClientError to the value pointed to by v. Otherwise returns err.
func ParseClientError(err error, v interface{}) error {
	twErr, ok := err.(twirp.Error)
	if !ok {
		return err
	}

	clientErr := twErr.Meta(clientErrorMetaKey)
	if clientErr == "" {
		return err
	}

	jsonErr := json.Unmarshal([]byte(clientErr), v)
	if jsonErr != nil {
		return jsonErr
	}

	return nil
}
