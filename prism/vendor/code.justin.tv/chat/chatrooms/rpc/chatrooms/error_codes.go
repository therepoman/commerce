package chatrooms

// These error codes are meant to be returned within a chatrooms.ClientError type.

const (
	// Generic error codes
	ErrCodeInvalidPermissions      = "INVALID_PERMISSIONS"
	ErrCodeMissingRequiredArgument = "MISSING_REQUIRED_ARGUMENT"
	ErrCodeRoomNotFound            = "ROOM_NOT_FOUND"
	ErrCodeMessageNotFound         = "MESSAGE_NOT_FOUND"
	ErrCodeLimitExceedsMax         = "EXCEEDS_MAX_LIMIT"

	// JoinChannel
	ErrCodeUserBannedFromChannel = "USER_BANNED_FROM_CHANNEL"

	// CreateRoom / UpdateRoom
	ErrCodeIncompatibleRoomProperties    = "INCOMPATIBLE_ROOM_PROPERTIES"
	ErrCodeNameInvalidLength             = "INVALID_NAME_LENGTH"
	ErrCodeNameContainsInvalidCharacters = "NAME_CONTAINS_INVALID_CHARACTERS"
	ErrCodeNameInappropriate             = "INAPPROPRIATE_NAME"
	ErrCodeNameNotUnique                 = "ROOM_NAME_NOT_UNIQUE"
	ErrCodeTopicInvalidLength            = "INVALID_TOPIC_LENGTH"
	ErrCodeTopicInappropriate            = "INAPPROPRIATE_TOPIC"
	ErrCodeExceedsMaxAllowedRooms        = "EXCEEDS_MAX_ALLOWED_ROOMS"

	// SendMessage
	ErrCodeFailedAutoModEnforcement       = "FAILED_AUTOMOD_ENFORCEMENT"
	ErrCodeFailedBannedEnforcement        = "FAILED_BANNED_ENFORCEMENT"
	ErrCodeFailedTimedOutEnforcement      = "FAILED_TIMED_OUT_ENFORCEMENT"
	ErrCodeFailedEmoteOnlyModeEnforcement = "FAILED_EMOTEONLY_MODE"
	ErrCodeFailedR9KModeEnforcement       = "FAILED_R9K_MODE"
	ErrCodeFailedSlowModeEnforcement      = "FAILED_SLOW_MODE"
	ErrCodeFailedSpamEnforcement          = "FAILED_SPAM_ENFORCEMENT"
	ErrCodeFailedSuspendedEnforcement     = "FAILED_SUSPENDED_ENFORCEMENT"
	ErrCodeFailedZalgoEnforcement         = "FAILED_ZALGO_ENFORCEMENT"
	ErrCodeFailedRateLimit                = "FAILED_RATE_LIMIT"
	ErrCodeInvalidMessage                 = "INVALID_MESSAGE"

	// ListMessages
	ErrCodeInvalidCursor = "INVALID_CURSOR"

	// EditMessage
	ErrCodeMessageDeleted        = "MESSAGE_DELETED"
	ErrCodeCannotEditBitsMessage = "CANNOT_EDIT_BITS_MESSAGE"

	// UpdateRoomMode
	ErrCodeInvalidSlowModeDuration = "INVALID_SLOW_MODE_DURATION"
)

type ClientError struct {
	ErrorCode string `json:"error_code"`
}

// UpdateRoomModesError is the error for the UpdateRoomModes mutation.
type UpdateRoomModesError struct {
	ErrorCode               string `json:"error_code"`
	MinimumSlowModeDuration *int   `json:"minimum_slow_mode_duration"`
	MaximumSlowModeDuration *int   `json:"maximum_slow_mode_duration"`
}

type CreateRoomError struct {
	ErrorCode       string `json:"error_code"`
	MaxAllowedRooms *int   `json:"max_allowed_rooms"`
	MinLength       *int   `json:"min_length"`
	MaxLength       *int   `json:"max_length"`
}

type UpdateRoomError CreateRoomError

type ListMembersError struct {
	ErrorCode string `json:"error_code"`
	MaxLimit  *int   `json:"max_limit"`
}

type ListMessagesError struct {
	ErrorCode string `json:"error_code"`
	MaxLimit  *int   `json:"max_limit"`
}
type SendMessageError struct {
	ErrorCode                string `json:"error_code"`
	SlowModeDurationSeconds  *int   `json:"slow_mode_duration_seconds"`
	RemainingDurationSeconds *int   `json:"remaining_duration_seconds"`
}
