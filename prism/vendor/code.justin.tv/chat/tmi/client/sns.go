package tmi

import "time"

const (
	// ChatBanEventName is published to SNS when a user receives a permanent or
	// temporary ban from a channel.
	ChatBanEventName = "chat_ban"
	// ChatUnbanEventName is published to SNS when a user's permanent or
	// temporary ban from a channel is removed.
	ChatUnbanEventName = "chat_unban"
)

// ChatBanEventPayload stores data about a chat ban.
type ChatBanEventPayload struct {
	ChannelID   string     `json:"channel_id"`
	TargetID    string     `json:"target_id"`
	RequesterID string     `json:"requester_id"`
	BannedAt    time.Time  `json:"banned_at"`
	ExpiresAt   *time.Time `json:"expires_at,omitempty"`
}

// ChatUnbanEventPayload stores data about a chat unban.
type ChatUnbanEventPayload struct {
	ChannelID   string `json:"channel_id"`
	TargetID    string `json:"target_id"`
	RequesterID string `json:"requester_id"`
}
