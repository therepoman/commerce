package tmi

import (
	"time"

	badgesModels "code.justin.tv/chat/badges/app/models"
	"code.justin.tv/feeds/feeds-common/entity"
)

type SendMessageParams struct {
	UserID string `json:"user_id"`
	RoomID string `json:"room_id"`
	Body   string `json:"body"`
}

type SendMessageResponse struct {
	// Result indicates the result of the message
	//	- command_success
	//	- command_invalid
	//	- msg_rejected
	//		- r9k drop, etc
	//	- msg_delivered
	//	- user_ratelimited
	//		- ip or account rate limits
	//	- room_ratelimited
	//	- no_permission
	Result string `json:"result"`

	// MsgBody is the default (English) user-facing message text
	MsgBody string `json:"msg_body"`

	// MsgKey is the key for creating user-facing message text
	MsgKey string `json:"msg_key"`

	// MsgParams are the params for creating user-facing message text
	MsgParams map[string]interface{} `json:"msg_params"`

	// SentMsgID is the uuid for the sent message, if one was sent
	SentMsgID string `json:"sent_msg_id"`
}

type UserNoticeMsgParam struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

type SendUserNoticeParams struct {
	SenderUserID      int                  `json:"sender_user_id"`
	TargetChannelID   int                  `json:"target_channel_id"`
	Body              string               `json:"body"`
	MsgID             string               `json:"msg_id"`
	MsgParams         []UserNoticeMsgParam `json:"msg_params"`
	DefaultSystemBody string               `json:"default_system_body"`
}

type ApproveAutoModRejectedParams struct {
	MsgID           string `json:"msg_id"`
	RequesterUserID int    `json:"requester_user_id"`
}

type BitsMessageResponse struct {
	Sent        bool            `json:"sent"`
	MsgBody     string          `json:"msg_body"`
	MsgTags     BitsMessageTags `json:"msg_tags"`
	SentMsgBody string
	SentMsgTags UserRoomMsgTags
}

type BitsMessageTags struct {
	Badges      []badgesModels.Badge `json:"badges"`
	Color       string               `json:"color"`
	DisplayName string               `json:"display_name"`
	Emotes      []EmoticonMatch      `json:"emotes"`
	MsgID       string               `json:"msg_id"`
}

// TEMPORARY: Will Remove soon
type UserRoomMsgTags struct {
	Badges      []badgesModels.Badge
	Color       string
	DisplayName string
	Emotes      []EmoticonMatch
	Moderator   bool
	MsgID       string
	RoomID      int
	Subscriber  bool
	Turbo       bool
	UserID      int
	UserType    string

	// Optional tags
	Bits       *int
	Historical *bool
}

// EmoticonMatch stores the start and end indices where an emoticon was found in a string.
type EmoticonMatch struct {
	// ID is the unique row ID for the emoticon.
	ID int `json:"id"`
	// Start is the index in the original string where the emoticon starts (0-indexed, inclusive).
	Start int `json:"start"`
	// End is the index in the original string where the emoticon ends (0-indexed, inclusive).
	End int `json:"end"`
	// Set is the emoticon set that contains this emoticon.
	Set int `json:"set"`
}

type AddEmotesParams struct {
	RoomID string            `json:"room_id"`
	UserID string            `json:"user_id"`
	Emotes []AddEmoteTallies `json:"emotes"`
}

type AddEmoteTallies struct {
	Set     int    `json:"set"`
	ID      int    `json:"id"`
	Pattern string `json:"pattern"`
	Count   int    `json:"count"`
}

type Emote struct {
	Set     int    `json:"set"`
	ID      int    `json:"id"`
	Pattern string `json:"pattern"`
}

type UserFirstRoomChatResponse struct {
	// nil if the user hasn't chatted, otherwise the time of the user's first chat
	FirstChatTime *time.Time `json:"first_chat_time,omitempty"`
}

type ModifyUserPropertiesParams struct {
	ChatColor *string `json:"chat_color"`
}

type RoomsMessageContentFragment struct {
	Text          string `json:"text"`
	EmoticonID    string `json:"emoticon_id"`
	EmoticonSetID string `json:"emoticon_set_id"`
}

type RoomsMessage struct {
	ID                string                        `json:"id"`
	RoomID            string                        `json:"room_id"`
	RoomOwner         entity.Entity                 `json:"room_owner"`
	SentAt            time.Time                     `json:"send_at"`
	ContentText       string                        `json:"context_text"`
	ContentFragments  []RoomsMessageContentFragment `json:"content_fragments"`
	SenderID          string                        `json:"sender_id"`
	SenderLogin       string                        `json:"sender_login"`
	SenderDisplayName string                        `json:"sender_display_name"`
	SenderChatColor   string                        `json:"sender_chat_color"`
	SenderBadges      []badgesModels.Badge          `json:"sender_badges"`
	BitsAmount        int                           `json:"bits_amount"`
}

type PublishRoomsMessageParams struct {
	Message RoomsMessage `json:"message"`
}

const (
	ResultMsgRejected      = "msg_rejected"
	ResultMsgDelivered     = "msg_delivered"
	ResultCmdSuccess       = "cmd_success"
	ResultCmdFailed        = "cmd_failed"
	ResultCmdInvalid       = "cmd_invalid"
	ResultCmdUsage         = "cmd_usage"
	ResultNoPermission     = "no_permission"
	ResultUserRateLimited  = "user_rate_limited"
	ResultRoomRateLimited  = "room_rate_limited"
	ResultMsgBadCharacters = "invalid_message_characters"
)
