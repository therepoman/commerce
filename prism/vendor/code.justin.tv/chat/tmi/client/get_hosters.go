package tmi

import (
	"net/url"

	"context"

	"code.justin.tv/foundation/twitchclient"
)

type HostersResponse struct {
	Hosts []HostEntity `json:"hosts"`
}

type HostEntity struct {
	HostID            int    `json:"host_id"`
	HostLogin         string `json:"host_login"`
	HostDisplayName   string `json:"host_display_name"`
	TargetID          int    `json:"target_id"`
	TargetLogin       string `json:"target_login"`
	TargetDisplayName string `json:"target_display_name"`
	HostPartnered     bool   `json:"host_partnered"`
}

func (c *clientImpl) GetHosters(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*HostersResponse, error) {
	path := (&url.URL{
		Path: "/hosts",
		RawQuery: (&url.Values{
			"target":         []string{channelID},
			"include_logins": []string{"1"},
		}).Encode(),
	}).String()

	req, err := c.NewRequest("GET", path, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.clue.user_hosters",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded HostersResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return nil, err
	}
	return &decoded, nil
}
