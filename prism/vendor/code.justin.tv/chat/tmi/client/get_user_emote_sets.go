package tmi

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"context"

	"code.justin.tv/foundation/twitchclient"
)

type UserEmoteSetsResponse struct {
	SetIDs []int `json:"emote_set_ids"`
}

func (c *clientImpl) GetUserEmoteSets(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*UserEmoteSetsResponse, error) {
	path := fmt.Sprintf("/users/%s/emote_sets", url.QueryEscape(userID))
	req, err := c.NewRequest("GET", path, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.tmi.get_user_emote_sets",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer func() {
		if cerr := resp.Body.Close(); cerr != nil && err == nil {
			err = cerr
		}
	}()
	if resp.StatusCode != http.StatusOK {
		return nil, httpErrorImpl{
			statusCode: resp.StatusCode,
		}
	}

	decoded := &UserEmoteSetsResponse{}
	if err := json.NewDecoder(resp.Body).Decode(decoded); err != nil {
		return nil, err
	}
	return decoded, nil
}
