package tmi

const (
	NoPermission = "no_permission"

	// Timeout/bans
	BadTimeoutAnon        = "bad_timeout_anon"
	BadTimeoutBroadcaster = "bad_timeout_broadcaster"
	BadTimeoutStaff       = "bad_timeout_staff"
	BadTimeoutAdmin       = "bad_timeout_admin"
	BadTimeoutGlobalMod   = "bad_timeout_global_mod"
	BadTimeoutSelf        = "bad_timeout_self"
	BadTimeoutMod         = "bad_timeout_mod"
	BadTimeoutDuration    = "bad_timeout_duration"
	TimeoutSuccess        = "timeout_success"

	BadBanAnon        = "bad_ban_anon"
	BadBanBroadcaster = "bad_ban_broadcaster"
	BadBanStaff       = "bad_ban_staff"
	BadBanAdmin       = "bad_ban_admin"
	BadBanGlobalMod   = "bad_ban_global_mod"
	BadBanSelf        = "bad_ban_self"
	BadBanMod         = "bad_ban_mod"
	BadBanError       = "bad_ban_error"
	BadUnbanNoBan     = "bad_unban_no_ban"
	AlreadyBanned     = "already_banned"
	BanSuccess        = "ban_success"
	UnbanSuccess      = "unban_success"
	TimeoutNoTimeout  = "timeout_no_timeout"
	UntimeoutSuccess  = "untimeout_success"
	UntimeoutIsBanned = "untimeout_banned"

	AddChannelBlockedTermSuccess           = "add_channel_blocked_term_success"
	AddChannelBlockedTermNoPermissions     = "bad_add_channel_blocked_term_no_permissions"
	AddChannelBlockedTermInvalidDuration   = "bad_add_channel_blocked_term_invalid_duration"
	AddChannelBlockedTermError             = "bad_add_channel_blocked_term_error"
	AddChannelBlockedTermPermittedConflict = "bad_add_channel_blocked_term_permitted_conflict"

	DeleteChannelBlockedTermSuccess       = "delete_channel_blocked_term_success"
	DeleteChannelBlockedTermNoPermissions = "delete_channel_blocked_term_no_permissions"
	DeleteChannelBlockedTermError         = "delete_channel_blocked_term_error"

	AddChannelPermittedTermSuccess         = "add_channel_permitted_term_success"
	AddChannelPermittedTermNoPermissions   = "bad_add_channel_permitted_term_no_permissions"
	AddChannelPermittedTermInvalidDuration = "bad_add_channel_permitted_term_invalid_duration"
	AddChannelPermittedTermError           = "bad_add_channel_permitted_term_error"
	AddChannelPermittedTermBlockedConflict = "bad_add_channel_permitted_term_blocked_conflict"

	DeleteChannelPermittedTermSuccess       = "delete_channel_permitted_term_success"
	DeleteChannelPermittedTermNoPermissions = "delete_channel_permitted_term_no_permissions"
	DeleteChannelPermittedTermError         = "delete_channel_permitted_term_error"
)
