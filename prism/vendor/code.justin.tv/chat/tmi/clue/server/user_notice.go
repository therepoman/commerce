package server

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"

	"code.justin.tv/chat/tmi/clue/api"
	"code.justin.tv/chat/tmi/clue/logic"

	"context"
	"gopkg.in/validator.v2"
)

var (
	maxBodyBytes int64 = 1024 * 500
)

func (srv *ServeMux) UserNotice(ctx context.Context, response *Response, request *Request) error {
	var requestData api.UserNoticeMessage

	body, err := ioutil.ReadAll(&io.LimitedReader{
		R: request.Body,
		N: maxBodyBytes,
	})

	if err != nil {
		return err
	}

	err = json.Unmarshal(body, &requestData)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		return err
	}

	err = validator.Validate(&requestData)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		return err
	}

	msgParams := map[string]string{}
	if len(requestData.MsgParams) > 0 {
		for _, param := range requestData.MsgParams {
			err = validator.Validate(param)
			if err != nil {
				response.WriteHeader(http.StatusBadRequest)
				return err
			}
			msgParams[param.Key] = param.Value
		}
	}

	userNoticeParams := logic.UserNoticeParams{
		SenderUserID:      requestData.SenderUserID,
		TargetChannelID:   requestData.TargetChannelID,
		Body:              requestData.Body,
		MsgId:             requestData.MsgId,
		MsgParams:         msgParams,
		DefaultSystemBody: requestData.DefaultSystemBody,
	}

	if err := srv.Logic.UserNotice(ctx, userNoticeParams); err != nil {
		if status, ok := httpStatusFromError(err); ok {
			response.WriteHeader(status)
		}
		return err
	}

	response.WriteJSON(&struct{}{})
	return nil
}
