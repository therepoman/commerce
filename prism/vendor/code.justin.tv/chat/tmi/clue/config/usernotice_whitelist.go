package config

// UsernoticeWhitelist lists known usernotice types that are allowed to be
// sent through Twitch chat.
var UsernoticeWhitelist = map[string]struct{}{
	// Sent when a user subscribes to a channel.
	"sub": {},
	// Sent when a user renews a subscription to a channel.
	"resub": {},
	// Sent when a user sends a subscription gift to another user.
	"subgift": {},

	// Sent to the raided channel that it has been raided.
	"raid": {},
	// Sent to the raiding channel that a raid has been canceled.
	"unraid": {},

	// Sent when a user redeems a ritual token.
	"ritual": {},

	// ???. Called by commerce/janus.
	"purchase": {},

	// Sent when random users are gifted in chat.
	"rewardgift": {},
}
