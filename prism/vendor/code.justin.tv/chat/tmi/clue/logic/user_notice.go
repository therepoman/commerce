package logic

import (
	"context"
	"fmt"
	"strconv"
	"unicode"
	"unicode/utf8"

	"code.justin.tv/chat/golibs/errx"
	"code.justin.tv/chat/golibs/logx"
	tmi "code.justin.tv/chat/tmi/client"
	"code.justin.tv/chat/tmi/clue/config"
	"code.justin.tv/chat/tmi/clue/internal/clients"
	"code.justin.tv/chat/tmi/clue/util"
	"code.justin.tv/chat/tmi/message"
)

type UserNoticeParams struct {
	SenderUserID      int
	TargetChannelID   int
	Body              string
	MsgId             string
	MsgParams         map[string]string
	DefaultSystemBody string
}

func (l *ClueLogicImpl) UserNotice(ctx context.Context, userNoticeParams UserNoticeParams) error {
	if _, whitelisted := config.UsernoticeWhitelist[userNoticeParams.MsgId]; !whitelisted {
		return errx.New(fmt.Sprintf("Usernotice type \"%s\" is not whitelisted in \"usernotice_whitelist.go\".", userNoticeParams.MsgId))
	}

	action, err := l.generateUserNoticeAction(ctx, userNoticeParams)
	if err != nil {
		return err
	}

	if _, err := l.pubSend(ctx, action); err != nil {
		return err
	}

	// Track stats of usernotice message usage.
	l.Stats.Inc(fmt.Sprintf("event.usernotice_%s", userNoticeParams.MsgId), 1, 1)

	return nil
}

func (l *ClueLogicImpl) generateUserNoticeAction(ctx context.Context, userNoticeParams UserNoticeParams) (message.Action, error) {
	room, err := l.Repository.ChannelRoom(ctx, userNoticeParams.TargetChannelID)
	if err != nil {
		return nil, err
	}

	user, err := l.Repository.User(ctx, userNoticeParams.SenderUserID)
	if err != nil {
		return nil, err
	}
	userIsSuspended, err := user.IsDisabled(ctx)
	if err != nil {
		return nil, err
	}
	if userIsSuspended {
		return nil, errx.New("sender is suspended")
	}

	var shouldProcessUserBody bool
	if len(userNoticeParams.Body) == 0 || isAllSpaces(userNoticeParams.Body) {
		shouldProcessUserBody = false
	} else {
		shouldProcessUserBody, err = l.enforceCustomMessage(ctx, user, room)

		if err != nil {
			return nil, err
		}
	}

	var body string
	var emoticonMatches []tmi.EmoticonMatch

	if shouldProcessUserBody {
		bannedWords := l.allBannedWords(ctx, room)

		body = l.sanitizeBody(ctx, userNoticeParams.Body)
		body = l.censorBody(ctx, body, bannedWords)

		if utf8.RuneCountInString(body) == 0 {
			return nil, errx.New(fmt.Sprintf("sanitized body is empty - got body: %q", userNoticeParams.Body))
		}

		params := clients.EnforceMessageParams{
			SenderID:    strconv.Itoa(user.Id()),
			ChannelID:   strconv.Itoa(room.Id()),
			MessageText: body,
		}
		zumaResp, zumaErr := l.backend.EnforceMessage(ctx, params)
		if zumaErr != nil {
			// We log the error and continue; the notice will not have parsed emotes.
			logx.Error(ctx, zumaErr)
		}
		emoticonMatches = emoticonMatchesFromMessageFragments(ctx, zumaResp.ExtractProperties.Content.Fragments)
	}

	tags, err := l.userRoomMsgTags(ctx, user, room, emoticonMatches)
	if err != nil {
		return nil, err
	}
	oldTags := tags.toOldTags().
		SetMsgParams(userNoticeParams.MsgParams).
		SetSystemMsg(userNoticeParams.DefaultSystemBody).
		SetMsgId(userNoticeParams.MsgId).
		SetLogin(user.Login())

	ircChannel := util.IrcChannelFromName(room.Name())
	action := command(ircChannel, user.Login(), "USERNOTICE", body, oldTags)

	return action, nil
}

func (l *ClueLogicImpl) enforceCustomMessage(ctx context.Context, u User, r Room) (bool, error) {
	if roomBanned, err := r.IsBanned(ctx, u); err != nil {
		return false, err
	} else if roomBanned {
		return false, nil
	}

	if timeout, err := r.GetTimeout(ctx, u); err != nil {
		return false, err
	} else if timeout > 0 {
		return false, nil
	}

	if disabled, err := u.IsDisabled(ctx); err != nil {
		return false, err
	} else if disabled {
		return false, nil
	}

	if isAllowedToJoin, err := r.IsAllowedToJoin(ctx, u); err != nil {
		return false, err
	} else if !isAllowedToJoin {
		return false, nil
	}

	return true, nil
}

func isAllSpaces(s string) bool {
	for _, char := range s {
		if !unicode.IsSpace(char) {
			return false
		}
	}

	return true
}
