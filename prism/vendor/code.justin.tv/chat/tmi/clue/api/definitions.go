package api

// Used by other Twitch teams. Please check and alert other teams ahead of changes.
type BitsMessage struct {
	SenderUserID    int    `json:"sender_user_id,omitempty,string"`
	TargetChannelID int    `json:"target_channel_id,omitempty,string"`
	Body            string `json:"body,omitempty"`
	Numberbits      int    `json:"number_bits"`
}

type AutoModApproveMessage struct {
	MsgID           string `json:"msg_id"`
	RequesterUserID int    `json:"requester_user_id"`
	RoomID          int    `json:"room_id"`
}

type AutoModDenyMessage struct {
	MsgID           string `json:"msg_id"`
	RequesterUserID int    `json:"requester_user_id"`
	RoomID          int    `json:"room_id"`
}

// ChannelAutoModProperties represents what properties a user has chosen for their AutoMod rules.
// Each rule is an int64, which any topic above that value should trigger a rejection. 0 is considered "off"
type ChannelAutoModProperties struct {
	AggressiveChannelLevel *int64 `json:"aggressive_channel_level,omitempty"`
	SexualChannelLevel     *int64 `json:"sexual_channel_level,omitempty"`
	ProfanityChannelLevel  *int64 `json:"profanity_channel_level,omitempty"`
	IdentityChannelLevel   *int64 `json:"identity_channel_level,omitempty"`
}

type UserNoticeMessage struct {
	SenderUserID    int `json:"sender_user_id" validate:"min=1"`
	TargetChannelID int `json:"target_channel_id" validate:"min=1"`

	// user message
	Body string `json:"body" validate:"max=500"`

	// for internationalization. if a translation exists for this ID, then it is
	// used over the system body.
	MsgId string `json:"msg_id" validate:"nonzero"`
	// optional positional arguments. tags will contain
	//		`msg-param-<key1>=value1,msg-param-<key2>=value2,...`
	MsgParams []*UserNoticeMessageParam `json:"msg_params" validate:"max=20"`
	// default internationalization string
	DefaultSystemBody string `json:"default_system_body" validate:"nonzero"`
}

type UserNoticeMessageParam struct {
	Key   string `json:"key" validate:"nonzero,regexp=^[0-9a-zA-Z\\-]+$"`
	Value string `json:"value" validate:"nonzero"`
}

// AutoModRejectedMessage is the response for the AutoModRejectedMessage endpoint
type AutoModRejectedMessage struct {
	MsgID           string   `json:"msg_id"`
	SenderUserID    string   `json:"sender_user_id"`
	TargetChannelID string   `json:"target_channel_id"`
	MessageBody     string   `json:"message_body"`
	FailedFragments []string `json:"failed_fragments"`
	HadActionTaken  bool     `json:"had_action_taken"`
}

// AutoModCheckMessage is the params for the AutoModCheckMessage endpoint
type AutoModCheckMessage struct {
	MsgID               string `json:"msg_id"`
	SenderUserID        string `json:"sender_user_id"`
	TargetChannelID     string `json:"target_channel_id"`
	TargetUserID        string `json:"target_user_id"`
	MessageBody         string `json:"message_body"`
	DefaultAutoModLevel *int   `json:"default_automod_level"`
}

// AutoModCheckMessageResult is the response JSON for the AutoModCheckMessage endpoint
type AutoModCheckMessageResult struct {
	MsgID           string         `json:"msg_id"`
	Response        bool           `json:"response"`
	Topics          map[string]int `json:"topics,omitempty"`
	IsAutoAllowed   bool           `json:"is_chat_auto_allowed,omitempty"`
	IsAutoDenied    bool           `json:"is_chat_auto_denied,omitempty"`
	SenderUserID    string         `json:"sender_user_id,omitempty"`
	TargetChannelID string         `json:"target_channel_id,omitempty"`
	TargetUserID    string         `json:"target_user_id,omitempty"`
	CensoredMessage string         `json:"censored_message"`
}
