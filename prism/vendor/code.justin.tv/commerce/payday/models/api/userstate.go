package api

const (
	UserStateNew      = UserState("new")
	UserStateAcquired = UserState("acquired")
	UserStateCheered  = UserState("cheered")
	UserStateUnknown  = UserState("unknown")
)

type UserState string

func NewUserState(state string) UserState {
	switch UserState(state) {
	case UserStateNew:
		return UserStateNew
	case UserStateAcquired:
		return UserStateAcquired
	case UserStateCheered:
		return UserStateCheered
	default:
		return UserStateUnknown
	}
}

func (us UserState) ToString() string {
	return string(us)
}
