package pointers

import "time"

func IntP(i int) *int {
	return &i
}

func Int32P(i int32) *int32 {
	return &i
}

func Int64P(i int64) *int64 {
	return &i
}

func BoolP(b bool) *bool {
	return &b
}

func StringP(s string) *string {
	return &s
}

func DurationP(d time.Duration) *time.Duration {
	return &d
}

func TimeP(t time.Time) *time.Time {
	return &t
}

func Int32PEquals(a, b *int32) bool {
	if a == nil && b == nil {
		return true
	}
	if (a == nil && b != nil) || (a != nil && b == nil) {
		return false
	}
	return *a == *b
}

func Int64PEquals(a, b *int64) bool {
	if a == nil && b == nil {
		return true
	}
	if (a == nil && b != nil) || (a != nil && b == nil) {
		return false
	}
	return *a == *b
}

func Int64ArrayPEquals(a, b *[]int64) bool {
	if a == nil && b == nil {
		return true
	}
	if (a == nil && b != nil) || (a != nil && b == nil) {
		return false
	}
	if len(*a) != len(*b) {
		return false
	}
	for x := 0; x < len(*a); x++ {
		if (*a)[x] != (*b)[x] {
			return false
		}
	}
	return true
}

func BoolPEquals(a, b *bool) bool {
	if a == nil && b == nil {
		return true
	}
	if (a == nil && b != nil) || (a != nil && b == nil) {
		return false
	}
	return *a == *b
}

func StringPEquals(a, b *string) bool {
	if a == nil && b == nil {
		return true
	}
	if (a == nil && b != nil) || (a != nil && b == nil) {
		return false
	}
	return *a == *b
}

func TimePEquals(a, b *time.Time) bool {
	if a == nil && b == nil {
		return true
	}
	if (a == nil && b != nil) || (a != nil && b == nil) {
		return false
	}
	return (*a).Equal(*b)
}

func Int32PMerge(original, mod *int32) *int32 {
	if mod == nil {
		return original
	} else {
		return mod
	}
}

func Int64PMerge(original, mod *int64) *int64 {
	if mod == nil {
		return original
	} else {
		return mod
	}
}

func BoolPMerge(original, mod *bool) *bool {
	if mod == nil {
		return original
	} else {
		return mod
	}
}

func StringPMerge(original, mod *string) *string {
	if mod == nil {
		return original
	} else {
		return mod
	}
}

func Int32OrDefault(i *int32, defaultVal int32) int32 {
	if i == nil {
		return defaultVal
	} else {
		return *i
	}
}

func Int64OrDefault(i *int64, defaultVal int64) int64 {
	if i == nil {
		return defaultVal
	} else {
		return *i
	}
}

func Int64ArrayOrDefault(i *[]int64, defaultVal []int64) []int64 {
	if i == nil {
		return defaultVal
	} else {
		return *i
	}
}

func BoolOrDefault(b *bool, defaultVal bool) bool {
	if b == nil {
		return defaultVal
	} else {
		return *b
	}
}

func StringOrDefault(s *string, defaultVal string) string {
	if s == nil {
		return defaultVal
	} else {
		return *s
	}
}
