package usernotice

import (
	"context"
	"strconv"

	"code.justin.tv/commerce/payday/clients/tmi"
	"code.justin.tv/commerce/payday/models"
)

const (
	SystemMessageID = "contributechannelchallenge"
)

type UserNotice interface {
	SendChallengeUserNotice(ctx context.Context, channelID string, bits int, userID string, message string) error
}

type userNotice struct {
	TmiClient tmi.ITMIClient `inject:""`
}

func NewUserNotice() UserNotice {
	return &userNotice{}
}

func (un *userNotice) SendChallengeUserNotice(ctx context.Context, channelID string, bits int, userID string, title string) error {
	parsedChannelID, err := strconv.ParseInt(channelID, 10, 64)

	if err != nil {
		return err
	}

	parsedUserID, err := strconv.ParseInt(userID, 10, 64)

	if err != nil {
		return err
	}

	err = un.TmiClient.SendUserNotice(models.UserNoticeParams{
		SenderUserID:    parsedUserID,
		TargetChannelID: parsedChannelID,
		MsgId:           SystemMessageID,
		MsgParams: []models.UserNoticeMsgParam{
			{
				Key:   "userID",
				Value: userID,
			},
			{
				Key:   "bits",
				Value: strconv.FormatInt(int64(bits), 10),
			},
			{
				Key:   "title",
				Value: title,
			},
		},
		DefaultSystemBody: "contribute to challenge notification",
	})

	return err
}
