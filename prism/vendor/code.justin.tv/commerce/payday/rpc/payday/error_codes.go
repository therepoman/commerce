package paydayrpc

const (
	// General
	ErrCodeUserNotPermitted = "USER_NOT_PERMITTED"

	// UseBitsOnPoll
	ErrCodeInsufficientBitsBalance = "INSUFFICIENT_BITS_BALANCE"

	// UpdateCheermoteTier
	ErrCodeMissingRequiredAgrument = "MISSING_REQUIRED_ARGUMENT"
	ErrCodeInvalidImageUpload      = "INVALID_IMAGE_UPLOADED"
	ErrCodeImageAssetsNotFound     = "IMAGE_ASSETS_NOT_FOUND"
	ErrCodeTooManyImageAssets      = "TOO_MANY_IMAGE_ASSETS"
	ErrCodeMissingStaticAsset      = "MISSING_STATIC_ASSET"
	ErrCodeMissingAnimatedAsset    = "MISSING_ANIMATED_ASSET"

	// AssignEmoteToBitsTier
	ErrCodeEmotesNotAllowedInBitsTier  = "EMOTES_NOT_ALLOWED_IN_BITS_TIER"
	ErrCodeBitsTierAlreadyFullOfEmotes = "BITS_TIER_ALREADY_FULL_OF_EMOTES"
	ErrCodeInvalidPartnerType          = "INVALID_PARTNER_TYPE"
	ErrCodeEmoteDoesNotExist           = "EMOTE_DOES_NOT_EXIST"
	ErrCodeEmoteCodeNotUnique          = "EMOTE_CODE_NOT_UNIQUE"
	ErrCodeInvalidEmoteState           = "INVALID_EMOTE_STATE"
)

type ClientError struct {
	ErrorCode string `json:"error_code"`
}
