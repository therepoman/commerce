package audit

import (
	"context"
	"encoding/json"
	"net/http"
	"time"
)

/*
	Because request headers are not available in context by default,
	this wrapper adds them to context on ServeHTTP call.
	At the Twirp API Level, these fields can then be accessed to create Access Log Message.
*/

type MuxWrapper struct {
	handler http.Handler
}

var auditKey int

type auditEvent struct {
	Api        string      `json:"api"`
	Method     string      `json:"method"`
	Url        string      `json:"url"`
	RemoteAddr string      `json:"remote_addr"`
	Header     http.Header `json:"header"`
	Body       interface{} `json:"body"`
	Timestamp  time.Time   `json:"timestamp"`
}

func NewMuxWrapper(handler http.Handler) *MuxWrapper {
	return &MuxWrapper{
		handler: handler,
	}
}

func (a *MuxWrapper) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	ctx := req.Context()
	ctx = context.WithValue(ctx, &auditKey, auditEvent{
		Method:     req.Method,
		Url:        req.URL.String(),
		RemoteAddr: req.RemoteAddr,
		Header:     req.Header,
	})
	a.handler.ServeHTTP(resp, req.WithContext(ctx))
}

// creates an access log string with the fields in the following order:
// api, method, url, remoteAddr, header, body
func CreateAccessLogMsg(ctx context.Context, api string, body interface{}) (string, error) {
	raw := ctx.Value(&auditKey)
	var event auditEvent
	if raw != nil {
		event = raw.(auditEvent)
	}
	event.Api = api
	event.Body = body
	event.Timestamp = time.Now()

	var eventJson string
	eventBytes, err := json.Marshal(event)
	if err != nil {
		return "", err
	} else {
		eventJson = string(eventBytes)
	}
	return eventJson, nil
}
