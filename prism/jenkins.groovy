job {
    name 'commerce-prism'
    using 'TEMPLATE-autobuild'
    concurrentBuild true

    scm {
        git {
            remote {
                github 'commerce/prism', 'ssh', 'git.xarth.tv'
                credentials 'git-aws-read-key'
            }
            clean true
        }
    }

    wrappers {
        sshAgent 'git-aws-read-key'
        preBuildCleanup()
        timestamps()
        credentialsBinding {
            file('COURIERD_PRIVATE_KEY', 'courierd')
            file('AWS_CONFIG_FILE', 'aws_config')
        }
    }

    steps {
        shell './scripts/docker_build.sh'
        shell './scripts/docker_push.sh'
        saveDeployArtifact 'commerce/prism-deploy', 'deploy'
    }
}

freeStyleJob('commerce-prism-staging-deploy') {
    using 'TEMPLATE-skadi-deploy'

    wrappers {
        credentialsBinding {
            file('COURIERD_PRIVATE_KEY', 'courierd')
            file('AWS_CONFIG_FILE', 'aws_config')
            string 'AWS_ACCESS_KEY', 'prism-devo-tcs-access-key'
            string 'AWS_SECRET_KEY', 'prism-devo-tcs-secret-key'
        }
    }

    scm {
        git {
            branch('$GIT_COMMIT')
            remote {
                github 'commerce/prism', 'ssh', 'git.xarth.tv'
                credentials 'git-aws-read-key'
            }
        }
    }

    steps {
        // download .ebextensions into deploy/.ebextensions
        shell 'mkdir -p .ebextensions'
        downloadDeployArtifact 'commerce/prism-deploy'
        shell 'rm -f *.manifest*'
        // deploy server
        shell """set +x
                |export AWS_SECRET_ACCESS_KEY=\$AWS_SECRET_KEY
                |export AWS_ACCESS_KEY_ID=\$AWS_ACCESS_KEY
                |export PYTHONUNBUFFERED=1
                |set -x
                |export IMAGE=docker-registry.internal.justin.tv/commerce-prism:\$GIT_COMMIT
                |
                |cat <<EOF > Dockerrun.aws.json
                |{
                |  "AWSEBDockerrunVersion": "1",
                |  "Image": {
                |    "Name": "\$IMAGE",
                |    "Update": "false"
                |  },
                |  "Ports": [
                |    {
                |      "ContainerPort": "8000"
                |    }
                |  ],
                |  "Volumes": [
                |    {
                |      "ContainerDirectory": "/var/app",
                |      "HostDirectory": "/var/app"
                |    },
                |    {
                |      "ContainerDirectory": "/etc/ssl/certs/ca-bundle.crt",
                |      "HostDirectory": "/etc/ssl/certs/ca-bundle.crt"
                |    }
                |  ],
                |  "Logging": "/var/log/"
                |}
                |EOF
                |
                | zip -r artifact.zip Dockerrun.aws.json .ebextensions
                | eb labs cleanup-versions --num-to-leave 10 --older-than 5 --force -v --region us-west-2
                | eb deploy staging-commerce-prism-env --timeout 30 """.stripMargin()
        downstreamParameterized {
            trigger('commerce-prism-integration-tests') {
                parameters {
                    gitRevision()
                    predefinedProp('ENVIRONMENT', 'staging')
                }
            }
        }
    }
}

freeStyleJob('commerce-prism-prod-deploy') {
    using 'TEMPLATE-skadi-deploy'

    wrappers {
        credentialsBinding {
            file('COURIERD_PRIVATE_KEY', 'courierd')
            file('AWS_CONFIG_FILE', 'aws_config')
            string 'AWS_ACCESS_KEY', 'prism-prod-tcs-access-key'
            string 'AWS_SECRET_KEY', 'prism-prod-tcs-secret-key'
        }
    }

    scm {
        git {
            branch('$GIT_COMMIT')
            remote {
                github 'commerce/prism', 'ssh', 'git.xarth.tv'
                credentials 'git-aws-read-key'
            }
        }
    }

    steps {
        // download .ebextensions into deploy/.ebextensions
        shell 'mkdir -p .ebextensions'
        downloadDeployArtifact 'commerce/prism-deploy'
        shell 'rm -f *.manifest*'
        // deploy server
        shell """set +x
                |export AWS_SECRET_ACCESS_KEY=\$AWS_SECRET_KEY
                |export AWS_ACCESS_KEY_ID=\$AWS_ACCESS_KEY
                |export PYTHONUNBUFFERED=1
                |set -x
                |export IMAGE=docker-registry.internal.justin.tv/commerce-prism:\$GIT_COMMIT
                |
                |cat <<EOF > Dockerrun.aws.json
                |{
                |  "AWSEBDockerrunVersion": "1",
                |  "Image": {
                |    "Name": "\$IMAGE",
                |    "Update": "false"
                |  },
                |  "Ports": [
                |    {
                |      "ContainerPort": "8000"
                |    }
                |  ],
                |  "Volumes": [
                |    {
                |      "ContainerDirectory": "/var/app",
                |      "HostDirectory": "/var/app"
                |    },
                |    {
                |      "ContainerDirectory": "/etc/ssl/certs/ca-bundle.crt",
                |      "HostDirectory": "/etc/ssl/certs/ca-bundle.crt"
                |    }
                |  ],
                |  "Logging": "/var/log/"
                |}
                |EOF
                |
                | zip -r artifact.zip Dockerrun.aws.json .ebextensions
                | eb labs cleanup-versions --num-to-leave 10 --older-than 5 --force -v --region us-west-2
                | eb deploy prod-commerce-prism-env --timeout 60 """.stripMargin()
    }
}

job {
    name 'commerce-prism-integration-tests'

    parameters {
        stringParam 'GIT_COMMIT'
        stringParam 'ENVIRONMENT'
    }

    configure { project ->
        project / builders / 'com.cloudbees.jenkins.GitHubSetCommitStatusBuilder'
    }

    wrappers {
        credentialsBinding {
            string 'AWS_ACCESS_KEY', 'prism-devo-tcs-access-key'
            string 'AWS_SECRET_KEY', 'prism-devo-tcs-secret-key'
            string 'QA_BITS_TESTS_OAUTH_TOKEN', 'QA_BITS_TESTS_OAUTH_TOKEN'
        }
        preBuildCleanup()
        timestamps()
    }

    scm {
        git {
            branch('$GIT_COMMIT')
            remote {
                github 'commerce/prism', 'ssh', 'git.xarth.tv'
                credentials 'git-aws-read-key'
            }
        }
    }

    steps {
        shell 'sleep 10'
        shell 'manta -v -f .manta_integration.json -e ENVIRONMENT=$ENVIRONMENT -e AWS_ACCESS_KEY=$AWS_ACCESS_KEY -e AWS_SECRET_KEY=$AWS_SECRET_KEY -e QA_BITS_TESTS_OAUTH_TOKEN=$QA_BITS_TESTS_OAUTH_TOKEN'
    }

    publishers {
        githubCommitNotifier()
    }
}