package client

import (
	"context"
	"net/http"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend/middleware"
	"code.justin.tv/foundation/twitchclient"
	"github.com/twitchtv/twirp"
)

func WithTwitchClientIDHeader(ctx context.Context) context.Context {
	header := make(http.Header)
	header.Set(twitchclient.TwitchClientIDHeader, middleware.GetClientID(ctx))

	ctxWithHeaders, err := twirp.WithHTTPRequestHeaders(ctx, header)
	if err != nil {
		log.WithError(err).Error("error setting HTTP header with TwitchClientID")
		return ctx
	}

	return ctxWithHeaders
}
