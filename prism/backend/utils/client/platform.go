package client

const (
	IPhoneClientID           = "85lcqzxpb9bqu9z6ga1ol55du"
	IPadClientID             = "p9lhq6azjkdl72hs5xnt3amqu7vv8k2"
	AndroidClientID          = "kd1unb4b3q4t58fwlpcbzcbnm76a8fp"
	WebClientClientID        = "jzkbprff40iqj646a697cyrvl0zt2m6"
	TwilightInternalClientID = "ps1pu6on4i1x19vo6pgcjyzckr7kr1"
	TwilightClientID         = "kimne78kx3ncx6brgo4mv6wki5h1ko"
	DesktopAppClientID       = "jf3xu125ejjjt5cl4osdjci6oz6p93r"
	UnknownPlatform          = "unknown"
	WebClientPlatform        = "web"
	AndroidPlatform          = "Android"
	IPadPlatform             = "iPad"
	IPhonePlatform           = "iPhone"
	DesktopPlatform          = "desktop"
)

// GetClientPlatform gets the platform based on clientID
func GetClientPlatform(clientID string) string {
	switch clientID {
	case IPhoneClientID:
		return IPhonePlatform
	case IPadClientID:
		return IPadPlatform
	case AndroidClientID:
		return AndroidPlatform
	case WebClientClientID:
		return WebClientPlatform
	case TwilightInternalClientID:
		return WebClientPlatform
	case TwilightClientID:
		return WebClientPlatform
	case DesktopAppClientID:
		return DesktopPlatform
	default:
		return UnknownPlatform
	}
}
