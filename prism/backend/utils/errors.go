package utils

import (
	"errors"

	"github.com/twitchtv/twirp"
)

const (
	error500Code = 500
)

var AllTenantsErroredDuringProcessCall = errors.New("all tenants encountered errors during the process message call")

var DownstreamDependencyError = twirp.InternalError("downstream dependency error processing message")

// IsTwirpError returns true if an error implements the Twirp error interface
func IsTwirpError(err error) bool {
	_, ok := err.(twirp.Error)
	return ok
}

func IsA5XXError(err twirp.Error) bool {
	if err == nil {
		return false
	}

	return twirp.ServerHTTPStatusFromErrorCode(err.Code()) >= error500Code
}

func ToTwirpError(err error) twirp.Error {
	if err == nil {
		return nil
	}

	tErr, ok := err.(twirp.Error)
	if !ok {
		tErr = twirp.InternalErrorWith(err)
	}
	return tErr
}

func PrioritizeByLowestCode(err error, err2 error) twirp.Error {
	twErr := ToTwirpError(err)
	twErr2 := ToTwirpError(err2)

	if twirp.ServerHTTPStatusFromErrorCode(twErr2.Code()) < twirp.ServerHTTPStatusFromErrorCode(twErr.Code()) {
		return twErr2
	}

	return twErr
}
