package utils

import "regexp"

var testIDRegex, _ = regexp.Compile("(^7357[0-9]{14})|(^perf)") // 18-digit numbers that begin with 7357

// IsTestUserID returns true if a userID is a test userID
func IsTestUserID(userID string) bool {
	return testIDRegex.MatchString(userID)
}
