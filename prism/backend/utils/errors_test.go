package utils

import (
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestIsTwirpError(t *testing.T) {
	Convey("IsTwirpErrors", t, func() {
		Convey("for non-twirp errors", func() {
			So(IsTwirpError(errors.New("test error")), ShouldBeFalse)
		})

		Convey("for twirp errors", func() {
			So(IsTwirpError(twirp.NewError(twirp.InvalidArgument, "test invalid argument")), ShouldBeTrue)
		})
	})
}

func TestIsA5XXError(t *testing.T) {
	Convey("IsA5XXError", t, func() {
		Convey("for nil errors", func() {
			So(IsA5XXError(nil), ShouldBeFalse)
		})

		Convey("for 5XX errors", func() {
			So(IsA5XXError(twirp.NewError(twirp.Internal, "test 5XX error")), ShouldBeTrue)
		})

		Convey("for 4XX errors", func() {
			So(IsA5XXError(twirp.NewError(twirp.InvalidArgument, "test 4XX error")), ShouldBeFalse)
		})
	})
}

func TestToTwirpError(t *testing.T) {
	Convey("ToTwirpError", t, func() {
		Convey("for existing twirp errors", func() {
			twErr := twirp.NewError(twirp.InvalidArgument, "test 4XX error")

			So(ToTwirpError(twErr), ShouldEqual, twErr)
		})

		Convey("for existing non-twirp errors", func() {
			err := errors.New("test error")
			twErr := twirp.InternalErrorWith(err)

			So(ToTwirpError(err).Code(), ShouldEqual, twErr.Code())
			So(ToTwirpError(err).Error(), ShouldEqual, twErr.Error())
		})

		Convey("for a nil error", func() {
			var err error

			So(ToTwirpError(err), ShouldBeNil)
		})
	})
}
