package anonymous_message_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/commerce/prism/backend/anonymous_message"
	dynamo_transaction "code.justin.tv/commerce/prism/backend/dynamo/transaction"
	zuma_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/chat/zuma"
	prism "code.justin.tv/commerce/prism/rpc/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestAnonymousMessageErrorHandler_Handle(t *testing.T) {
	Convey("Given an anonymous message error handler", t, func() {
		userID := "127516203"
		channelID := "116076154"
		tx := &dynamo_transaction.Transaction{UserID: userID, ChannelID: channelID, ShouldSendAnyway: false}

		zumaClient := new(zuma_mock.Client)

		testErr := errors.New("test error")

		anonMessageErrorHandler := anonymous_message.NewTestAnonymousMessageErrorHandler(zumaClient)

		Convey("When the tenant error is NOT an invalid anonymous message error", func() {
			tenantErr := testErr
			isAnonErr, anonErr := anonMessageErrorHandler.Handle(context.Background(), tx, tenantErr)

			So(isAnonErr, ShouldBeFalse)
			So(anonErr, ShouldBeNil)
		})

		Convey("When the tenant error is an invalid anonymous message error, and the ShouldSendAnyway flag is true", func() {
			tenantErr := prism.NewInvalidAnonymousMessageError("test error", "anon1 anon2")
			tx.ShouldSendAnyway = true

			isAnonErr, anonErr := anonMessageErrorHandler.Handle(context.Background(), tx, tenantErr)

			So(isAnonErr, ShouldBeTrue)
			So(anonErr, ShouldBeNil)
		})

		Convey("When the tenant error is an invalid anonymous message error, and the ShouldSendAnyway flag is false", func() {
			tenantErr := prism.NewInvalidAnonymousMessageError("test error", "anon1 anon2")
			tx.ShouldSendAnyway = false

			zumaClient.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(api.EnforceMessageResponse{}, nil)

			isAnonErr, anonErr := anonMessageErrorHandler.Handle(context.Background(), tx, tenantErr)

			So(isAnonErr, ShouldBeTrue)
			So(anonErr, ShouldNotBeNil)
		})
	})
}
