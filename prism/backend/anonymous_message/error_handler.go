package anonymous_message

import (
	"context"

	zumaapi "code.justin.tv/chat/zuma/app/api"
	zuma_client "code.justin.tv/chat/zuma/client"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend/consumable"
	transaction_dynamo "code.justin.tv/commerce/prism/backend/dynamo/transaction"
	"code.justin.tv/commerce/prism/backend/models"
	apimodel "code.justin.tv/commerce/prism/backend/models/api"
	prism "code.justin.tv/commerce/prism/rpc/models"
	"code.justin.tv/feeds/feeds-common/entity"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/twitchtv/twirp"
)

// AnonymousMessageErrorHandler represents a handler for anonymous message errors
type AnonymousMessageErrorHandler interface {
	Handle(ctx context.Context, tx *transaction_dynamo.Transaction, tenantErr error) (isAnonMessageErr bool, anonErr error)
}

type anonymousMessageErrorHandler struct {
	ZumaClient zuma_client.Client `inject:""`
}

// NewAnonymousMessageErrorHandler creates an AnonymousMessageErrorHandler
func NewAnonymousMessageErrorHandler() AnonymousMessageErrorHandler {
	return &anonymousMessageErrorHandler{}
}

// NewTestAnonymousMessageErrorHandler creates a test AnonymousMessageErrorHandler
func NewTestAnonymousMessageErrorHandler(zumaClient zuma_client.Client) AnonymousMessageErrorHandler {
	return &anonymousMessageErrorHandler{
		ZumaClient: zumaClient,
	}
}

// Handle returns whether an error is an "invalid anonymous message" error and whether the caller should ignore the error based on whether the user has indicated
// "ShouldSendAnyway" (i.e. to send the anonymized version of the message that was previewed to them already)
func (a *anonymousMessageErrorHandler) Handle(ctx context.Context, tx *transaction_dynamo.Transaction, tenantErr error) (isAnonMessageErr bool, anonErr error) {
	var errData prism.InvalidAnonymousMessageErrorData
	parsingErr := prism.ParseClientError(tenantErr, &errData)
	if parsingErr != nil || errData.AnonymizedMessage == "" {
		// Not an "invalid anonymous message" error
		isAnonMessageErr = false
		anonErr = nil
	} else if tx.ShouldSendAnyway {
		// User has agreed to anonymize the message. Ignore the error.
		tx.RequestedMessage = errData.AnonymizedMessage
		// Make sure that only bits consumable are present in anonymous messages for now (unless we start supporting multiple consumable types for anon messages)
		tx.ConsumablesRequested = models.ConsumableMap{
			consumable.ConsumableType_Bits: tx.ConsumablesRequested[consumable.ConsumableType_Bits],
		}
		isAnonMessageErr = true
		anonErr = nil
	} else {
		// Return a preview anonymized message to the user
		// Call Zuma to tokenize message entirely
		containerOwner := entity.New(entity.NamespaceUser, tx.ChannelID)

		extractMessageReq := zumaapi.EnforceMessageRequest{
			SkipExtraction:   false,
			ParseCheermotes:  true,
			SenderID:         tx.UserID,
			MessageText:      errData.AnonymizedMessage,
			ContainerOwner:   &containerOwner,
			EnforcementRules: zumaapi.EnforcementRuleset{},
			EnforcementConfig: zumaapi.EnforcementConfiguration{
				CheckAllyClassification: aws.Bool(true),
			},
		}

		zumaResponse, err := a.ZumaClient.EnforceMessage(ctx, extractMessageReq, nil)
		if err != nil {
			msg := "error enforcing message with zuma for invalid anonymous message"
			if ctx.Err() == nil {
				// Only log if the call to zuma had time to complete
				log.WithError(err).Error(msg)
			}
			return true, twirp.InternalError(msg)
		}

		errorResponse := apimodel.CreateEventErrorResponse{
			Status:  apimodel.AnonymousMessageInvalid,
			Message: apimodel.AnonymousMessageInvalidError,
			Data: apimodel.CreateEventErrorData{
				EnforceMessageResponse: &zumaResponse,
			},
		}

		isAnonMessageErr = true
		anonErr = prism.NewClientError(twirp.NewError(twirp.InvalidArgument, apimodel.AnonymousMessageInvalidError), errorResponse)
	}

	return
}
