package transaction

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/config"
	prism "code.justin.tv/commerce/prism/rpc"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/guregu/dynamo"
)

type daoImpl struct {
	*dynamo.Table
	suffix string
}

// DAO represents a DynamoDB transaction DAO
type DAO interface {
	GetTransaction(transactionID string) (*Transaction, error)
	CreateNew(t *Transaction) error
	CreateOrUpdate(t *Transaction) error
}

// NewDAO creates a new transaction DAO
func NewDAO(sess *session.Session, suffix string) DAO {
	db := dynamo.New(sess, &aws.Config{
		Retryer: client.DefaultRetryer{
			NumMaxRetries: 3,
		},
		HTTPClient: &http.Client{
			Timeout: 100 * time.Millisecond,
			Transport: &http.Transport{
				MaxConnsPerHost:     1000,
				MaxIdleConnsPerHost: 1000,
				IdleConnTimeout:     90 * time.Second,
			},
		},
	})
	table := db.Table(fmt.Sprintf("transactions-%s", suffix))
	return &daoImpl{
		Table:  &table,
		suffix: suffix,
	}
}

type ProcessingStatus string

type ChatStatus string

type TenantStatus string

const (
	StatusInProgressStarted = ProcessingStatus("in_progress_started")

	StatusCompletedAllTenantsTokenizeAndValidateSuccessfully      = ProcessingStatus("completed_all_tenants_tokenize_and_validate_successfully")
	StatusCompletedPartialTenantTokenizeAndValidateError          = ProcessingStatus("completed_partial_tenant_tokenize_and_validate_error")
	StatusCompletedAllTenantsEncounteredTokenizeAndValidateErrors = ProcessingStatus("completed_all_tenants_encountered_tokenize_and_validate_errors")

	StatusFailedZumaValidation   = ProcessingStatus("failed_zuma_validation")
	StatusFailedTenantValidation = ProcessingStatus("failed_tenant_validation")

	StatusFailedAnonymousMessageValidation = ProcessingStatus("failed_anonymous_message_validation")

	StatusCompletedAllTenantsProcessedSuccessfully       = ProcessingStatus("completed_all_tenants_processed_successfully")
	StatusCompletedPartialTenantProcessingError          = ProcessingStatus("completed_partial_tenant_processing_error")
	StatusCompletedAllTenantsEncounteredProcessingErrors = ProcessingStatus("completed_all_tenants_encountered_processing_errors")

	StatusCompletedAllTenantsPostProcessedSuccessfully       = ProcessingStatus("completed_all_tenants_post_processed_successfully")
	StatusCompletedPartialTenantPostProcessingError          = ProcessingStatus("completed_partial_tenant_post_processing_error")
	StatusCompletedAllTenantsEncounteredPostProcessingErrors = ProcessingStatus("completed_all_tenants_encountered_post_processing_errors")

	StatusFailedToSendMessageToChat    = ChatStatus("failed_to_send_message_to_chat")
	StatusSuccesfullySentMessageToChat = ChatStatus("successfully_sent_message_to_chat")

	StatusTenantSuccessfullyProcessed = TenantStatus("tenant_successfully_processed")
	StatusTenantProcessingFailed      = TenantStatus("tenant_processing_failed")

	DaysToLiveBeforeDeletion = 30
)

// Transaction is a DynamoDB transaction
type Transaction struct {
	TransactionID           string                                     `dynamo:"transaction_id"`
	UserID                  string                                     `dynamo:"user_id"`
	ChannelID               string                                     `dynamo:"channel_id"`
	RoomID                  string                                     `dynamo:"room_id"`
	Anonymous               bool                                       `dynamo:"anonymous"`
	RequestedMessage        string                                     `dynamo:"requested_message"`
	ProcessedMessage        string                                     `dynamo:"processed_message"`
	SentMessage             string                                     `dynamo:"sent_message"`
	ConsumablesRequested    models.ConsumableMap                       `dynamo:"consumables_requested"`
	ConsumablesProcessed    map[config.TenantName]models.ConsumableMap `dynamo:"consumables_processed"`
	ProcessingStatus        ProcessingStatus                           `dynamo:"processing_status"`
	ChatStatus              ChatStatus                                 `dynamo:"chat_status"`
	TenantValidateErrors    map[config.TenantName]string               `dynamo:"tenant_validate_errors"`
	TenantProcessErrors     map[config.TenantName]string               `dynamo:"tenant_process_errors"`
	TenantPostProcessErrors map[config.TenantName]string               `dynamo:"tenant_post_process_errors"`
	TransformationErrors    map[config.TenantName][]string             `dynamo:"transformation_errors"`
	TenantStatuses          map[config.TenantName]TenantStatus         `dynamo:"tenant_statuses"`
	StartTime               time.Time                                  `dynamo:"start_time"`
	EndTime                 time.Time                                  `dynamo:"end_time"`
	MessageTreatment        prism_models.MessageTreatment              `dynamo:"message_treatment"`
	MessageRoute            prism_models.Route                         `dynamo:"message_route"`
	ShouldSendAnyway        bool                                       `dynamo:"should_send_anyway"`
	AutomodAcknowledged     bool                                       `dynamo:"automod_acknowledged"`
	Transformations         map[config.TenantName][]string             `dynamo:"transformations"`
	NewBalance              map[config.TenantName]models.ConsumableMap `dynamo:"new_balance"`
	ImageURL                string                                     `dynamo:"image_url"`

	// This row will set up an automatic deletion by DynamoDB based off of an epoch-based timestamp.
	// Rows that do not have a timestamp or a timestamp older than 5 years from creation of this column in dynamo
	// will not be processed.
	TTL int64 `dynamo:"ttl"`

	// Do NOT store GeoIP data in DynamoDB
	GeoIPMetadata prism.GeoIPMetadata `dynamo:"-"`
}

func (dao *daoImpl) GetTransaction(transactionID string) (*Transaction, error) {
	var t Transaction
	err := dao.Get("transaction_id", transactionID).One(&t)
	if err == dynamo.ErrNotFound {
		return nil, nil
	}
	return &t, err
}

func (dao *daoImpl) CreateNew(t *Transaction) error {
	if t == nil {
		return errors.New("cannot create nil transaction")
	}

	updateTTL(t)

	return dao.Put(*t).If("attribute_not_exists(transaction_id)").Run()
}

func (dao *daoImpl) CreateOrUpdate(t *Transaction) error {
	if t == nil {
		return errors.New("cannot create/update nil transaction")
	}

	updateTTL(t)

	return dao.Put(*t).Run()
}

func updateTTL(t *Transaction) {
	// Store transactions for 30 days for debugging then dump
	// If the transaction is being touched again, update it's TTL.
	t.TTL = time.Now().AddDate(0, 0, DaysToLiveBeforeDeletion).Unix()
}

// ConvertTenantErrorMap converts a tenant error map to a tenant error string map
func ConvertTenantErrorMap(m map[config.TenantName]error) map[config.TenantName]string {
	newMap := make(map[config.TenantName]string)
	for tenant, err := range m {
		newMap[tenant] = err.Error()
	}
	return newMap
}

// ConvertTenantErrorListMap converts a tenant error list map to a tenant error string list map
func ConvertTenantErrorListMap(m map[config.TenantName][]error) map[config.TenantName][]string {
	newMap := make(map[config.TenantName][]string)
	for tenant, errs := range m {
		newErrs := make([]string, 0)
		for _, err := range errs {
			newErrs = append(newErrs, err.Error())
		}
		newMap[tenant] = newErrs
	}
	return newMap
}

// ConvertTenantTransformationsListMap converts a tenant transformation list map to a tenant transformation JSON string map
func ConvertTenantTransformationsListMap(m map[config.TenantName][]*prism.Transformation) map[config.TenantName][]string {
	newMap := make(map[config.TenantName][]string)
	for tenant, transformations := range m {
		newTransformations := make([]string, 0)
		for _, transformation := range transformations {
			transformationJSON, err := json.Marshal(transformation)
			if err != nil {
				log.WithError(err).Error("Encountered error while marshalling transformation")
				continue
			}
			newTransformations = append(newTransformations, string(transformationJSON))
		}
		newMap[tenant] = newTransformations
	}
	return newMap
}

// IsConditionalCheckFailure checks if an error is a dynamo conditional check failed exception
func IsConditionalCheckFailure(err error) bool {
	aerr, ok := err.(awserr.Error)
	return ok && aerr.Code() == dynamodb.ErrCodeConditionalCheckFailedException
}
