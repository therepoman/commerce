package throttle

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/prism/backend/middleware"
	"code.justin.tv/commerce/prism/backend/models"
	redis_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/redis"
	"github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestThrottler_Throttle(t *testing.T) {
	Convey("Given a throttler", t, func() {
		redisClient := new(redis_mock.Client)

		t := throttler{
			RedisClient: redisClient,
		}

		Convey("Given an integration test context", func() {
			ctx := context.WithValue(context.Background(), middleware.ThrottleConfigContextKey, models.ThrottleConfigDisabled)

			isThrottled, err := t.Throttle(ctx, "test", time.Second)
			So(isThrottled, ShouldBeFalse)
			So(err, ShouldBeNil)
		})

		Convey("Given a regular context", func() {
			ctx := context.Background()

			Convey("If redis errors", func() {
				redisClient.On("Get", mock.Anything).Return("", errors.New("test error"))

				isThrottled, err := t.Throttle(ctx, "test", time.Second)
				So(isThrottled, ShouldBeFalse)
				So(err, ShouldNotBeNil)
			})

			Convey("If redis has the key stored already", func() {
				redisClient.On("Get", mock.Anything).Return("test", nil)

				isThrottled, err := t.Throttle(ctx, "test", time.Second)
				So(isThrottled, ShouldBeTrue)
				So(err, ShouldBeNil)
			})

			Convey("If redis does not have the key stored already", func() {
				redisClient.On("Get", mock.Anything).Return("", redis.Nil)

				Convey("If redis fails to set the new key value", func() {
					redisClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test error"))

					isThrottled, err := t.Throttle(ctx, "test", time.Second)
					So(isThrottled, ShouldBeFalse)
					So(err, ShouldNotBeNil)
				})

				Convey("If redis sets the new key value successfully", func() {
					redisClient.On("Set", mock.Anything, mock.Anything, mock.Anything).Return(nil)

					isThrottled, err := t.Throttle(ctx, "test", time.Second)
					So(isThrottled, ShouldBeFalse)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}
