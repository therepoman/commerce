package throttle

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	redis_client "code.justin.tv/commerce/prism/backend/clients/redis"
	"code.justin.tv/commerce/prism/backend/middleware"
	"code.justin.tv/commerce/prism/backend/models"
	"github.com/go-redis/redis"
)

// Throttler represents a throttler
type Throttler interface {
	Throttle(ctx context.Context, key string, expiration time.Duration) (bool, error)
}

type throttler struct {
	RedisClient redis_client.Client `inject:""`
}

// NewThrottler creates a new Throttler
func NewThrottler() Throttler {
	return &throttler{}
}

func (t *throttler) Throttle(ctx context.Context, key string, expiration time.Duration) (bool, error) {
	if isThrottleDisabledRequest(ctx) {
		return false, nil
	}

	val, err := t.RedisClient.Get(key)
	if err != nil && err != redis.Nil {
		log.WithError(err).WithField("key", key).Error("Error getting redis value")
		return false, err
	}
	if val != "" {
		return true, nil
	}

	err = t.RedisClient.Set(key, "true", expiration)
	if err != nil {
		log.WithError(err).WithField("key", key).Error("Error setting redis value")
		return false, err
	}

	return false, nil
}

func isThrottleDisabledRequest(ctx context.Context) bool {
	cfg := ctx.Value(middleware.ThrottleConfigContextKey)
	val, ok := cfg.(string)
	return ok && val == models.ThrottleConfigDisabled
}
