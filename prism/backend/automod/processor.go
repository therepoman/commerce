package automod

import (
	"context"
	"encoding/json"
	"errors"

	"code.justin.tv/amzn/TwitchSafetyAutoModTwirp"
	zuma_api "code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/chat/zuma/rpc/maap"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend/cache/automod"
	"code.justin.tv/commerce/prism/backend/clients/sns"
	"code.justin.tv/commerce/prism/backend/clients/zuma"
	"code.justin.tv/commerce/prism/backend/consumable"
	transaction_dynamo "code.justin.tv/commerce/prism/backend/dynamo/transaction"
	"code.justin.tv/commerce/prism/backend/tenant"
	"code.justin.tv/commerce/prism/backend/usersservice"
	"code.justin.tv/commerce/prism/config"
	prism "code.justin.tv/commerce/prism/rpc"
	"github.com/gofrs/uuid"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// Processor is an interface for an AutoMod Processor
type Processor interface {
	Process(ctx context.Context, tx *transaction_dynamo.Transaction, messageTokens map[config.TenantName][]*tenant.Token, autoModResp *zuma_api.AutoModResponse, msgFragments []*maap.MessageContentFragment) error
}

type processor struct {
	AutoModCache     automod.Cache                                `inject:""`
	AutoModClient    TwitchSafetyAutoModTwirp.TwitchSafetyAutoMod `inject:""`
	SNSClient        sns.IClient                                  `inject:""`
	Config           *config.Config                               `inject:""`
	UserFetcher      usersservice.Fetcher                         `inject:""`
	ConsumableMapper consumable.Mapper                            `inject:""`
}

var (
	errInvalidAutoModResponse = errors.New("invalid automod response object")
	errInvalidTransaction     = errors.New("invalid transaction")
	errInvalidUserProps       = errors.New("invalid user props given from user service")
)

// NewProcessor creates an AutoMod Processor
func NewProcessor() Processor {
	return &processor{}
}

// NewTestProcessor creates a test AutoMod Processor
func NewTestProcessor(
	config *config.Config,
	autoModCache automod.Cache,
	snsClient sns.IClient,
	userFetcher usersservice.Fetcher,
	automodClient TwitchSafetyAutoModTwirp.TwitchSafetyAutoMod) Processor {
	return &processor{
		Config:        config,
		AutoModCache:  autoModCache,
		AutoModClient: automodClient,
		SNSClient:     snsClient,
		UserFetcher:   userFetcher,
	}
}

// Process handles logic for starting the AutoMod process
func (ap *processor) Process(ctx context.Context, tx *transaction_dynamo.Transaction, messageTokens map[config.TenantName][]*tenant.Token, autoModResp *zuma_api.AutoModResponse, msgFragments []*maap.MessageContentFragment) error {
	if tx == nil {
		return errInvalidTransaction
	}

	if autoModResp == nil {
		return errInvalidAutoModResponse
	}

	uuid, err := uuid.NewV4()
	if err != nil {
		return err
	}
	msgID := uuid.String()

	// Cache the user's request in the AutoMod cache to block future requests from this user until their AutoModded request is resolved.
	// Also, queue the request in the AutoMod timeout queue in order to automatically resolve the user's request if it is not handled by a mod after some time.
	err = ap.cacheAndQueueUserRequest(ctx, msgID, tx, messageTokens)
	if err != nil {
		return err
	}

	userProps, err := ap.UserFetcher.Fetch(ctx, tx.UserID)
	if err != nil {
		log.WithError(err).WithField("userID", tx.UserID).Error("Failed to get user from Users Service")
		return err
	}

	if userProps.Login == nil || userProps.Displayname == nil {
		return errInvalidUserProps
	}

	category, highestLevel := getCategoryAndHighestLevel(*autoModResp)

	var bitsAmount int64
	if _, ok := tx.ConsumablesRequested[consumable.ConsumableType_Bits]; ok {
		bitsAmount = tx.ConsumablesRequested[consumable.ConsumableType_Bits][consumable.Consumable_Bits]
	}

	_, err = ap.AutoModClient.CreateCaughtMessage(ctx, &TwitchSafetyAutoModTwirp.CreateCaughtMessageRequest{
		Message: &maap.Message{
			Id:     msgID,
			SentAt: timestamppb.New(tx.StartTime),
			Content: &maap.MessageContent{
				Text:      tx.RequestedMessage,
				Fragments: msgFragments,
			},
			Sender: &maap.MessageSender{
				UserId:      tx.UserID,
				Login:       *userProps.Login,
				DisplayName: *userProps.Displayname,
			},
			BitsAmount: bitsAmount,
		},
		Params: &TwitchSafetyAutoModTwirp.CreateCaughtMessageParams{
			ChannelID: tx.ChannelID,
			ContentClassification: &TwitchSafetyAutoModTwirp.EnforcementContentClassification{
				Category: category,
				Level:    int64(highestLevel),
			},
			Tags:             &TwitchSafetyAutoModTwirp.PrivMsgTags{}, // can only be provided through the kinesis stream, so empty tags is fine
			IsFromMsgBroker:  false,
			DetectedLanguage: autoModResp.DetectedLanguage,
		},
	})

	return err
}

func getCategoryAndHighestLevel(autoModResp zuma_api.AutoModResponse) (string, int) {
	if autoModResp.AllyTopics != nil {
		highestLevel := 0
		category := zuma.UnknownCategory
		if autoModResp.AllyTopics.Ableism > highestLevel {
			highestLevel = autoModResp.AllyTopics.Ableism
			category = zuma.AbleismCategory
		}

		if autoModResp.AllyTopics.Aggression > highestLevel {
			highestLevel = autoModResp.AllyTopics.Aggression
			category = zuma.AggressionCategory
		}

		if autoModResp.AllyTopics.Homophobia > highestLevel {
			highestLevel = autoModResp.AllyTopics.Homophobia
			category = zuma.HomophobiaCategory
		}

		if autoModResp.AllyTopics.Misogyny > highestLevel {
			highestLevel = autoModResp.AllyTopics.Misogyny
			category = zuma.MisogynyCategory
		}

		if autoModResp.AllyTopics.NameCalling > highestLevel {
			highestLevel = autoModResp.AllyTopics.NameCalling
			category = zuma.NamecallingCategory
		}

		if autoModResp.AllyTopics.Racism > highestLevel {
			highestLevel = autoModResp.AllyTopics.Racism
			category = zuma.RacismCategory
		}

		if autoModResp.AllyTopics.SexWords > highestLevel {
			highestLevel = autoModResp.AllyTopics.SexWords
			category = zuma.SexWordsCategory
		}

		if autoModResp.AllyTopics.Swearing > highestLevel {
			highestLevel = autoModResp.AllyTopics.Swearing
			category = zuma.SwearingCategory
		}

		if highestLevel > 0 {
			return string(category), highestLevel
		}
	}
	// return sift category
	category, highestLevel := zuma.CategoryAndHighestLevelFromMostSignificantKnownTopic(autoModResp.Topics)
	return string(category), highestLevel
}

func (ap *processor) cacheAndQueueUserRequest(ctx context.Context, msgID string, tx *transaction_dynamo.Transaction, messageTokens map[config.TenantName][]*tenant.Token) error {
	processMessageReq := prism.ProcessMessageReq{
		UserId:            tx.UserID,
		ChannelId:         tx.ChannelID,
		RoomId:            tx.RoomID,
		MessageId:         msgID,
		Message:           tx.RequestedMessage,
		ConsumableAmounts: tx.ConsumablesRequested.ToRawConsumables(),
		Anonymous:         tx.Anonymous,
		ShouldSendAnyway:  tx.ShouldSendAnyway,
	}

	var tokenTextList []string
	for _, tokens := range messageTokens {
		for _, token := range tokens {
			tokenTextList = append(tokenTextList, token.Text)
		}
	}

	automodCacheValue := automod.CacheValue{
		ProcessMessageReq: processMessageReq,
		MessageTokens:     tokenTextList,
	}

	err := ap.AutoModCache.Set(tx.UserID, automodCacheValue)
	if err != nil {
		return err
	}

	snsMsgJSON, err := json.Marshal(processMessageReq)
	if err != nil {
		log.WithError(err).Error("Failed to marshal ProcessMessageReq into json for sending an SNS message")
		return err
	}

	err = ap.SNSClient.PostToTopic(ap.Config.AutoModSNSTopicARN, string(snsMsgJSON))
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"snsTopic":  ap.Config.AutoModSNSTopicARN,
			"channelID": tx.ChannelID,
			"UserID":    tx.UserID,
		}).Error("Failed to send AutoModded message to the SNS topic")
	}

	return err
}
