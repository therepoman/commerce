package automod_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/amzn/TwitchSafetyAutoModTwirp"
	"code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/commerce/prism/backend/automod"
	"code.justin.tv/commerce/prism/backend/clients/zuma"
	dynamo_transaction "code.justin.tv/commerce/prism/backend/dynamo/transaction"
	"code.justin.tv/commerce/prism/config"
	chmod_automod_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/amzn/chmod_automod"
	automod_cache_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/cache/automod"
	sns_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/sns"
	usersservice_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/usersservice"
	"code.justin.tv/web/users-service/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestAutoModProcessor_Process(t *testing.T) {
	Convey("Given an automod processor", t, func() {
		userID := "127516203"
		userLogin := "bigchungus"
		userDisplayName := "smallchungus"
		channelID := "116076154"
		tx := &dynamo_transaction.Transaction{UserID: userID, ChannelID: channelID}

		mockConfig, err := config.LoadConfig(config.UnitTest)
		So(err, ShouldBeNil)
		autoModCache := new(automod_cache_mock.Cache)
		snsClient := new(sns_mock.IClient)
		userFetcher := new(usersservice_mock.Fetcher)
		automodClient := new(chmod_automod_mock.TwitchSafetyAutoMod)

		testErr := errors.New("test error")

		autoModProcessor := automod.NewTestProcessor(mockConfig, autoModCache, snsClient, userFetcher, automodClient)

		Convey("When passing invalid parameters", func() {
			Convey("When passing an invalid transaction", func() {
				err := autoModProcessor.Process(context.Background(), nil, nil, nil, nil)
				So(err, ShouldNotBeNil)
			})

			Convey("When passing an invalid automod response object", func() {
				err := autoModProcessor.Process(context.Background(), tx, nil, nil, nil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("When setting the AutoMod cache fails", func() {
			autoModCache.On("Set", mock.Anything, mock.Anything).Return(testErr)

			err := autoModProcessor.Process(context.Background(), tx, nil, &api.AutoModResponse{}, nil)
			So(err, ShouldNotBeNil)
		})

		Convey("When setting the AutoMod cache succeeds", func() {
			autoModCache.On("Set", mock.Anything, mock.Anything).Return(nil)

			Convey("When posting to SNS fails", func() {
				snsClient.On("PostToTopic", mock.Anything, mock.Anything).Return(testErr)

				err := autoModProcessor.Process(context.Background(), tx, nil, &api.AutoModResponse{}, nil)
				So(err, ShouldNotBeNil)
			})

			Convey("When posting to SNS succeeds", func() {
				snsClient.On("PostToTopic", mock.Anything, mock.Anything).Return(nil)

				Convey("When users service client fails", func() {
					userFetcher.On("Fetch", mock.Anything, mock.Anything).Return(nil, testErr)

					err := autoModProcessor.Process(context.Background(), tx, nil, &api.AutoModResponse{}, nil)
					So(err, ShouldNotBeNil)
				})

				Convey("When users service client succeeds", func() {

					Convey("When users service misses vital parameters", func() {
						userFetcher.On("Fetch", mock.Anything, mock.Anything).Return(&models.Properties{Login: &userLogin, Displayname: nil}, nil)
						err := autoModProcessor.Process(context.Background(), tx, nil, &api.AutoModResponse{}, nil)
						So(err, ShouldNotBeNil)
					})

					Convey("When automod client fails", func() {
						userFetcher.On("Fetch", mock.Anything, mock.Anything).Return(&models.Properties{Login: &userLogin, Displayname: &userDisplayName}, nil)
						automodClient.On("CreateCaughtMessage", mock.Anything, mock.Anything).Return(nil, testErr)

						err := autoModProcessor.Process(context.Background(), tx, nil, &api.AutoModResponse{}, nil)
						So(err, ShouldNotBeNil)
					})

					Convey("When automod client succeeds", func() {
						userFetcher.On("Fetch", mock.Anything, mock.Anything).Return(&models.Properties{Login: &userLogin, Displayname: &userDisplayName}, nil)
						automodClient.On("CreateCaughtMessage", mock.Anything, mock.Anything).Return(nil, nil)

						err := autoModProcessor.Process(context.Background(), tx, nil, &api.AutoModResponse{}, nil)
						So(err, ShouldBeNil)
					})
				})
			})
		})

		Convey("Test ally categories", func() {
			autoModCache.On("Set", mock.Anything, mock.Anything).Return(nil)
			snsClient.On("PostToTopic", mock.Anything, mock.Anything).Return(nil)
			userFetcher.On("Fetch", mock.Anything, mock.Anything).Return(&models.Properties{Login: &userLogin, Displayname: &userDisplayName}, nil)
			automodClient.On("CreateCaughtMessage", mock.Anything, mock.Anything).Return(nil, nil)

			testCases := []struct {
				allyTopics           *api.AllyTopics
				expectedCategory     string
				expectedHighestLevel int
			}{
				{
					allyTopics: &api.AllyTopics{
						Ableism:    3,
						Homophobia: 2,
						Aggression: 1,
					},
					expectedCategory:     "ableism",
					expectedHighestLevel: 3,
				},
				{
					allyTopics: &api.AllyTopics{
						Ableism:    1,
						Aggression: 2,
					},
					expectedCategory:     "aggression",
					expectedHighestLevel: 2,
				},
				{
					allyTopics: &api.AllyTopics{
						Homophobia: 1,
					},
					expectedCategory:     "homophobia",
					expectedHighestLevel: 1,
				},
				{
					allyTopics: &api.AllyTopics{
						Misogyny: 1,
					},
					expectedCategory:     "misogyny",
					expectedHighestLevel: 1,
				},
				{
					allyTopics: &api.AllyTopics{
						NameCalling: 1,
					},
					expectedCategory:     "namecalling",
					expectedHighestLevel: 1,
				},
				{
					allyTopics: &api.AllyTopics{
						Racism: 1,
					},
					expectedCategory:     "racism",
					expectedHighestLevel: 1,
				},
				{
					allyTopics: &api.AllyTopics{
						SexWords: 1,
					},
					expectedCategory:     "sexwords",
					expectedHighestLevel: 1,
				},
				{
					allyTopics: &api.AllyTopics{
						Swearing: 1,
					},
					expectedCategory:     "swearing",
					expectedHighestLevel: 1,
				},
				{
					expectedCategory:     "unknown",
					expectedHighestLevel: 0,
				},
			}

			for idx, tc := range testCases {
				err := autoModProcessor.Process(context.Background(), tx, nil, &api.AutoModResponse{AllyTopics: tc.allyTopics}, nil)
				So(err, ShouldBeNil)
				createCaughtMessageRequest := (automodClient.Calls[idx].Arguments[1]).(*TwitchSafetyAutoModTwirp.CreateCaughtMessageRequest)
				So(createCaughtMessageRequest, ShouldNotBeNil)
				So(createCaughtMessageRequest.Params.ContentClassification.Category, ShouldEqual, tc.expectedCategory)
				So(createCaughtMessageRequest.Params.ContentClassification.Level, ShouldEqual, int64(tc.expectedHighestLevel))
			}
		})

		Convey("Test sift categories", func() {
			autoModCache.On("Set", mock.Anything, mock.Anything).Return(nil)
			snsClient.On("PostToTopic", mock.Anything, mock.Anything).Return(nil)
			userFetcher.On("Fetch", mock.Anything, mock.Anything).Return(&models.Properties{Login: &userLogin, Displayname: &userDisplayName}, nil)
			automodClient.On("CreateCaughtMessage", mock.Anything, mock.Anything).Return(nil, nil)

			testCases := []struct {
				topics               map[string]int
				expectedCategory     string
				expectedHighestLevel int
			}{
				{
					topics: map[string]int{
						"1":  3,
						"5":  5,
						"10": 7,
					},
					expectedCategory:     "aggressive",
					expectedHighestLevel: zuma.SiftRiskMapToShieldNumber[3],
				},
				{
					topics: map[string]int{
						"4": 3,
					},
					expectedCategory:     "sexual",
					expectedHighestLevel: zuma.SiftRiskMapToShieldNumber[3],
				},
				{
					topics: map[string]int{
						"5": 3,
					},
					expectedCategory:     "profanity",
					expectedHighestLevel: zuma.SiftRiskMapToShieldNumber[3],
				},
				{
					topics: map[string]int{
						"1":  7,
						"10": 3,
					},
					expectedCategory:     "identity",
					expectedHighestLevel: zuma.SiftRiskMapToShieldNumber[3],
				},
				{
					topics: map[string]int{
						"whatever": 3,
					},
					expectedCategory:     "unknown",
					expectedHighestLevel: 0,
				},
			}

			for idx, tc := range testCases {
				err := autoModProcessor.Process(context.Background(), tx, nil, &api.AutoModResponse{Topics: tc.topics}, nil)
				So(err, ShouldBeNil)
				createCaughtMessageRequest := (automodClient.Calls[idx].Arguments[1]).(*TwitchSafetyAutoModTwirp.CreateCaughtMessageRequest)
				So(createCaughtMessageRequest, ShouldNotBeNil)
				So(createCaughtMessageRequest.Params.ContentClassification.Category, ShouldEqual, tc.expectedCategory)
				So(createCaughtMessageRequest.Params.ContentClassification.Level, ShouldEqual, int64(tc.expectedHighestLevel))
			}
		})

		Convey("Should send detected language code to automod when there is detected language code from zuma response", func() {
			autoModCache.On("Set", mock.Anything, mock.Anything).Return(nil)
			snsClient.On("PostToTopic", mock.Anything, mock.Anything).Return(nil)
			userFetcher.On("Fetch", mock.Anything, mock.Anything).Return(&models.Properties{Login: &userLogin, Displayname: &userDisplayName}, nil)
			automodClient.On("CreateCaughtMessage", mock.Anything, mock.Anything).Return(nil, nil)

			err := autoModProcessor.Process(context.Background(), tx, nil, &api.AutoModResponse{DetectedLanguage: "en"}, nil)
			So(err, ShouldBeNil)
			createCaughtMessageRequest := (automodClient.Calls[0].Arguments[1]).(*TwitchSafetyAutoModTwirp.CreateCaughtMessageRequest)
			So(createCaughtMessageRequest, ShouldNotBeNil)
			So(createCaughtMessageRequest.Params.DetectedLanguage, ShouldEqual, "en")
		})

		Convey("Should not send detected language code to automod when there is no detected language code from zuma response", func() {
			autoModCache.On("Set", mock.Anything, mock.Anything).Return(nil)
			snsClient.On("PostToTopic", mock.Anything, mock.Anything).Return(nil)
			userFetcher.On("Fetch", mock.Anything, mock.Anything).Return(&models.Properties{Login: &userLogin, Displayname: &userDisplayName}, nil)
			automodClient.On("CreateCaughtMessage", mock.Anything, mock.Anything).Return(nil, nil)

			err := autoModProcessor.Process(context.Background(), tx, nil, &api.AutoModResponse{}, nil)
			So(err, ShouldBeNil)
			createCaughtMessageRequest := (automodClient.Calls[0].Arguments[1]).(*TwitchSafetyAutoModTwirp.CreateCaughtMessageRequest)
			So(createCaughtMessageRequest, ShouldNotBeNil)
			So(createCaughtMessageRequest.Params.DetectedLanguage, ShouldBeEmpty)
		})
	})
}
