package channel_whitelist

import (
	"context"

	base_filter "code.justin.tv/commerce/prism/backend/filter"
	"code.justin.tv/commerce/prism/backend/utils"
	prism "code.justin.tv/commerce/prism/rpc"
)

var defaultWhitelistedChannelIDs = []string{
	"145903336", // qa_bits_tests
	"116076154", // qa_bits_partner
	"108707191", // seph
	"196291456", // qa_bits_extensions
}

type filter struct {
	whitelistedChannelIDs map[string]interface{}
}

// NewDefaultChannelWhitelistFilter creates a new default channel whitelist filter
func NewDefaultChannelWhitelistFilter() base_filter.Filter {
	return NewChannelWhitelistFilter(defaultWhitelistedChannelIDs...)
}

// NewChannelWhitelistFilter creates a new channel whitelist filter
func NewChannelWhitelistFilter(channelIDs ...string) base_filter.Filter {
	whitelistedChannelIDs := make(map[string]interface{})
	for _, channelID := range channelIDs {
		whitelistedChannelIDs[channelID] = nil
	}

	return &filter{
		whitelistedChannelIDs: whitelistedChannelIDs,
	}
}

func (f *filter) Name() string {
	return "ChannelWhitelist"
}

func (f *filter) AllowMessage(ctx context.Context, req prism.ProcessMessageReq) (bool, error) {
	if utils.IsTestUserID(req.ChannelId) {
		return true, nil
	}

	_, ok := f.whitelistedChannelIDs[req.ChannelId]
	return ok, nil
}
