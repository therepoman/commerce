package channel_whitelist_test

import (
	"context"
	"testing"

	"code.justin.tv/commerce/prism/backend/filter"
	"code.justin.tv/commerce/prism/backend/filter/channel_whitelist"
	prism "code.justin.tv/commerce/prism/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func testFilter(filter filter.Filter, channelID string, expectedAllow bool, expectedErr error) {
	allow, err := filter.AllowMessage(context.Background(), prism.ProcessMessageReq{
		ChannelId: channelID,
		UserId:    "test-user",
		MessageId: "test-message",
		Message:   "test message",
	})
	So(allow, ShouldEqual, expectedAllow)
	So(err, ShouldEqual, expectedErr)
}

func TestChannelWhitelistFilter_EmptyFilter(t *testing.T) {
	Convey("Given an empty whitelist", t, func() {
		f := channel_whitelist.NewChannelWhitelistFilter()
		Convey("Everything is filtered", func() {
			testFilter(f, "", false, nil)
			testFilter(f, "   ", false, nil)
			testFilter(f, "A", false, nil)
			testFilter(f, "ABC", false, nil)
			testFilter(f, "TEST", false, nil)
			testFilter(f, "108707191", false, nil)

			Convey("Except test channels are always allowed", func() {
				testFilter(f, "735700000000000001", true, nil)
			})
		})
	})
}

func TestChannelWhitelistFilter_SingleItemFilter(t *testing.T) {
	Convey("Given a single channel whitelist", t, func() {
		f := channel_whitelist.NewChannelWhitelistFilter("chan1")
		Convey("Only the single channel is allowed", func() {
			testFilter(f, "", false, nil)
			testFilter(f, "   ", false, nil)
			testFilter(f, "A", false, nil)
			testFilter(f, "chan1", true, nil)
			testFilter(f, "chan2", false, nil)

			Convey("Except test channels are always allowed", func() {
				testFilter(f, "735700000000000001", true, nil)
			})
		})
	})
}

func TestChannelWhitelistFilter_MultiItemFilter(t *testing.T) {
	Convey("Given a multi channel whitelist", t, func() {
		f := channel_whitelist.NewChannelWhitelistFilter("chan1", "chan2", "chan3")
		Convey("Only the whitelisted channels are allowed", func() {
			testFilter(f, "", false, nil)
			testFilter(f, "   ", false, nil)
			testFilter(f, "A", false, nil)
			testFilter(f, "chan1", true, nil)
			testFilter(f, "chan2", true, nil)
			testFilter(f, "chan3", true, nil)
			testFilter(f, "chan4", false, nil)

			Convey("Except test channels are always allowed", func() {
				testFilter(f, "735700000000000001", true, nil)
			})
		})
	})
}
