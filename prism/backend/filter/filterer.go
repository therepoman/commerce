package filter

import (
	"context"
	"errors"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend/clients/stats"
	prism "code.justin.tv/commerce/prism/rpc"
)

var FilterError = errors.New("filter error")
var FiltererTimeoutError = errors.New("timed out processing filters")

type filterer struct {
	Stats   stats.Statter `inject:""`
	Filters []Filter      `inject:"filters"`
}

// Filterer represents a request filterer
type Filterer interface {
	ShouldFilter(ctx context.Context, req prism.ProcessMessageReq) (bool, error)
}

// NewFilterer creates a new filterer
func NewFilterer() Filterer {
	return &filterer{}
}

// NewTestFilterer creates a new test filterer
func NewTestFilterer(stats stats.Statter, filters []Filter) Filterer {
	return &filterer{
		Stats:   stats,
		Filters: filters,
	}
}

func latencyKey(tenant string) string {
	return fmt.Sprintf("%s_filter_latency", tenant)
}

func errorsKey(tenant string) string {
	return fmt.Sprintf("%s_filter_errors", tenant)
}

func errorCount(err error) int64 {
	if err != nil {
		return 1
	}
	return 0
}

func (fil *filterer) ShouldFilter(ctx context.Context, req prism.ProcessMessageReq) (bool, error) {
	startTime := time.Now()
	var err error
	defer func() {
		latency := time.Since(startTime)
		fil.Stats.TimingDuration(latencyKey("filter_manager"), latency, 1)

		fil.Stats.Inc(errorsKey("filter_manager"), errorCount(err), 1)
	}()

	if len(fil.Filters) < 1 {
		return false, nil
	}

	filterRespChan := make(chan AllowMessageResp)
	for _, f := range fil.Filters {
		go func(f Filter) {
			filterStartTime := time.Now()
			allowMsg, allowMsgErr := f.AllowMessage(ctx, req)
			filterLatency := time.Since(filterStartTime)

			fil.Stats.TimingDuration(latencyKey(f.Name()), filterLatency, 1)

			fil.Stats.Inc(errorsKey(f.Name()), errorCount(allowMsgErr), 1)

			filterRespChan <- AllowMessageResp{
				FilterName:   f.Name(),
				AllowMessage: allowMsg,
				Error:        allowMsgErr,
			}
		}(f)
	}

	filterResps := make(map[string]AllowMessageResp)
FilterWaitLoop:
	for {
		select {
		case filterResp := <-filterRespChan:
			if filterResp.Error != nil {
				log.WithError(filterResp.Error).WithField("filter", filterResp.FilterName).Error("error calling message filter")
				return true, FilterError
			}

			if !filterResp.AllowMessage {
				return true, nil
			}

			filterResps[filterResp.FilterName] = filterResp
			if len(filterResps) >= len(fil.Filters) {
				break FilterWaitLoop
			}
		case <-ctx.Done():
			log.Error("timed out calling all filters")
			return true, FiltererTimeoutError
		}
	}

	return false, nil
}
