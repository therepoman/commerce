package filter_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/prism/backend/filter"
	stats_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/stats"
	filter_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/filter"
	prism "code.justin.tv/commerce/prism/rpc"
	"code.justin.tv/commerce/prism/test"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

type slowFilter struct {
	WaitDuration    time.Duration
	FinishedWaiting bool
}

func newSlowFilter(dur time.Duration) *slowFilter {
	return &slowFilter{WaitDuration: dur}
}

func (sf *slowFilter) Name() string {
	return "Slow Filter"
}

func (sf *slowFilter) AllowMessage(ctx context.Context, req prism.ProcessMessageReq) (bool, error) {
	sf.FinishedWaiting = false
	time.Sleep(sf.WaitDuration)
	sf.FinishedWaiting = true
	return true, nil
}

func (sf *slowFilter) Finished() bool {
	return sf.FinishedWaiting
}

func testFilterer(c context.Context, filterer filter.Filterer, stats *stats_mock.Statter, expectedShouldFilter bool, expectedErr error, expectedMetrics test.Range) {
	shouldFilter, err := filterer.ShouldFilter(c, prism.ProcessMessageReq{})
	So(shouldFilter, ShouldEqual, expectedShouldFilter)
	So(err, ShouldEqual, expectedErr)
	test.AssertInRange(test.GetNumCalls(&stats.Mock, "TimingDuration"), expectedMetrics)
	test.AssertInRange(test.GetNumCalls(&stats.Mock, "Inc"), expectedMetrics)
}

func TestFilterer_NoFilters(t *testing.T) {
	Convey("Given a filterer without filters", t, func() {
		stats := new(stats_mock.Statter)
		filterer := filter.NewTestFilterer(stats, []filter.Filter{})
		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("Filter should never filter or error", func() {
			testFilterer(context.Background(), filterer, stats, false, nil, test.NewRange(1, 1))
		})
	})
}

func TestFilterer_OneFilter(t *testing.T) {
	Convey("Given a filterer with one filter", t, func() {
		stats := new(stats_mock.Statter)
		filter1 := new(filter_mock.Filter)
		filterer := filter.NewTestFilterer(stats, []filter.Filter{filter1})
		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		filter1.On("Name").Return("Filter1")

		Convey("When the filter errors", func() {
			filter1.On("AllowMessage", mock.Anything, mock.Anything).Return(false, errors.New("test error"))

			Convey("The filterer should error", func() {
				testFilterer(context.Background(), filterer, stats, true, filter.FilterError, test.NewRange(2, 2))
			})
		})

		Convey("When the filter allows the message", func() {
			filter1.On("AllowMessage", mock.Anything, mock.Anything).Return(true, nil)

			Convey("The filterer should not filter", func() {
				testFilterer(context.Background(), filterer, stats, false, nil, test.NewRange(2, 2))
			})
		})

		Convey("When the filter denies the message", func() {
			filter1.On("AllowMessage", mock.Anything, mock.Anything).Return(false, nil)

			Convey("The filterer should filter", func() {
				testFilterer(context.Background(), filterer, stats, true, nil, test.NewRange(2, 2))
			})
		})
	})
}

func TestFilterer_MultipleFilters(t *testing.T) {
	Convey("Given a filterer with multiple filters", t, func() {
		stats := new(stats_mock.Statter)
		filter1 := new(filter_mock.Filter)
		filter2 := new(filter_mock.Filter)
		filter3 := new(filter_mock.Filter)
		filterer := filter.NewTestFilterer(stats, []filter.Filter{filter1, filter2, filter3})
		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		filter1.On("Name").Return("Filter1")
		filter2.On("Name").Return("Filter2")
		filter3.On("Name").Return("Filter3")

		Convey("When one filter errors", func() {
			filter1.On("AllowMessage", mock.Anything, mock.Anything).Return(false, errors.New("test error"))
			filter2.On("AllowMessage", mock.Anything, mock.Anything).Return(true, nil)
			filter3.On("AllowMessage", mock.Anything, mock.Anything).Return(true, nil)

			Convey("The filterer should error", func() {
				testFilterer(context.Background(), filterer, stats, true, filter.FilterError, test.NewRange(2, 4))
			})
		})

		Convey("When only one filter denies the message", func() {
			filter1.On("AllowMessage", mock.Anything, mock.Anything).Return(true, nil)
			filter2.On("AllowMessage", mock.Anything, mock.Anything).Return(false, nil)
			filter3.On("AllowMessage", mock.Anything, mock.Anything).Return(true, nil)

			Convey("The filterer should filter the message", func() {
				testFilterer(context.Background(), filterer, stats, true, nil, test.NewRange(2, 4))
			})
		})

		Convey("When only one filter allows the message", func() {
			filter1.On("AllowMessage", mock.Anything, mock.Anything).Return(false, nil)
			filter2.On("AllowMessage", mock.Anything, mock.Anything).Return(true, nil)
			filter3.On("AllowMessage", mock.Anything, mock.Anything).Return(false, nil)

			Convey("The filterer should filter the message", func() {
				testFilterer(context.Background(), filterer, stats, true, nil, test.NewRange(2, 4))
			})
		})

		Convey("When all filters deny the message", func() {
			filter1.On("AllowMessage", mock.Anything, mock.Anything).Return(false, nil)
			filter2.On("AllowMessage", mock.Anything, mock.Anything).Return(false, nil)
			filter3.On("AllowMessage", mock.Anything, mock.Anything).Return(false, nil)

			Convey("The filterer should filter the message", func() {
				testFilterer(context.Background(), filterer, stats, true, nil, test.NewRange(2, 4))
			})
		})

		Convey("When all filters allow the message", func() {
			filter1.On("AllowMessage", mock.Anything, mock.Anything).Return(true, nil)
			filter2.On("AllowMessage", mock.Anything, mock.Anything).Return(true, nil)
			filter3.On("AllowMessage", mock.Anything, mock.Anything).Return(true, nil)

			Convey("The filterer should not filter the message", func() {
				testFilterer(context.Background(), filterer, stats, false, nil, test.NewRange(4, 4))
			})
		})
	})
}

func TestFilterer_SlowFilter(t *testing.T) {
	Convey("Given a filterer with multiple filters including a slow filter", t, func() {
		stats := new(stats_mock.Statter)
		filter1 := new(filter_mock.Filter)
		slowFilter := newSlowFilter(60 * time.Millisecond)
		filter3 := new(filter_mock.Filter)
		filterer := filter.NewTestFilterer(stats, []filter.Filter{filter1, slowFilter, filter3})
		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		filter1.On("Name").Return("Filter1")
		filter3.On("Name").Return("Filter3")

		Convey("When one other filter errors", func() {
			filter1.On("AllowMessage", mock.Anything, mock.Anything).Return(false, errors.New("test error"))
			filter3.On("AllowMessage", mock.Anything, mock.Anything).Return(true, nil)

			Convey("The filterer should error before the slow filter finishes", func() {
				testFilterer(context.Background(), filterer, stats, true, filter.FilterError, test.NewRange(2, 3))
				So(slowFilter.Finished(), ShouldBeFalse)
			})
		})

		Convey("When one other filter denies the message", func() {
			filter1.On("AllowMessage", mock.Anything, mock.Anything).Return(false, nil)
			filter3.On("AllowMessage", mock.Anything, mock.Anything).Return(true, nil)

			Convey("The filterer should filter before the slow filter finishes", func() {
				testFilterer(context.Background(), filterer, stats, true, nil, test.NewRange(2, 3))
				So(slowFilter.Finished(), ShouldBeFalse)
			})
		})

		Convey("When all other filters allow the message", func() {
			filter1.On("AllowMessage", mock.Anything, mock.Anything).Return(true, nil)
			filter3.On("AllowMessage", mock.Anything, mock.Anything).Return(true, nil)

			Convey("The filterer should wait for the slow filter to finish", func() {
				testFilterer(context.Background(), filterer, stats, false, nil, test.NewRange(4, 4))
				So(slowFilter.Finished(), ShouldBeTrue)
			})
		})

		Convey("When all other filters allow the message and the context has a timeout", func() {
			filter1.On("AllowMessage", mock.Anything, mock.Anything).Return(true, nil)
			filter3.On("AllowMessage", mock.Anything, mock.Anything).Return(true, nil)
			ctx, cancel := context.WithTimeout(context.Background(), 20*time.Millisecond)

			Convey("The filterer should return a timeout error", func() {
				testFilterer(ctx, filterer, stats, true, filter.FiltererTimeoutError, test.NewRange(3, 3))
				So(slowFilter.Finished(), ShouldBeFalse)
				cancel()
			})
		})
	})
}
