package filter

import (
	"context"

	prism "code.justin.tv/commerce/prism/rpc"
)

type AllowMessageResp struct {
	FilterName   string
	AllowMessage bool
	Error        error
}
type Filter interface {
	Name() string
	AllowMessage(ctx context.Context, req prism.ProcessMessageReq) (bool, error)
}
