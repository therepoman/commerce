package tenant

import (
	"code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/config"
	prism "code.justin.tv/commerce/prism/rpc"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
)

type HealthCheckReq struct {
}

type TokenizeAndValidateReq struct {
	UserID               string
	ChannelID            string
	RoomID               string
	Message              string
	MessageID            string
	ConsumablesRequested models.ConsumableMap
	Anonymous            bool
}

type TokenizeAndValidateResp struct {
	Tokens []*Token
	Error  error
}

type ProcessMessageReq struct {
	UserID               string
	ChannelID            string
	RoomID               string
	Message              string
	MessageID            string
	ConsumablesRequested models.ConsumableMap
	Anonymous            bool
	GeoIPMetadata        prism.GeoIPMetadata
}

type ProcessMessageResp struct {
	Transformations      []*prism.Transformation
	ConsumablesProcessed models.ConsumableMap
	Error                error
	MessageTreatment     prism_models.MessageTreatment
	NewBalance           map[string]int64
}

type PostProcessMessageReq struct {
	UserID               string
	ChannelID            string
	RoomID               string
	Message              string
	MessageID            string
	ConsumablesProcessed map[config.TenantName]models.ConsumableMap
	Anonymous            bool
	GeoIPMetadata        prism.GeoIPMetadata
	SentMessage          string
}

type PostProcessMessageResp struct {
	Error error
}

type Error struct {
	TenantName config.TenantName
	Error      error
}

type Token struct {
	Type   string
	Text   string
	Amount int64
}
