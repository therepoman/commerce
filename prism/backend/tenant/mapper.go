package tenant

import (
	"net/url"
	"sync"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend/clients/s2s/caller"
	"code.justin.tv/commerce/prism/config"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	"github.com/pkg/errors"
)

const (
	defaultTenantTimeout = 10 * time.Second
)

// Mapper represents a mapper from TenantName to PrismTenant
type Mapper interface {
	Get(tenantName config.TenantName) (prism_tenant.PrismTenant, error)
	GetAll() map[config.TenantName]prism_tenant.PrismTenant
}

type mapper struct {
	Cache  *sync.Map
	Config *config.Config `inject:""`
}

// NewMapper creates a new Mapper
func NewMapper() Mapper {
	m := &mapper{}
	m.Cache = new(sync.Map)
	return m
}

// NewTestMapper creates a new test Mapper
func NewTestMapper(cfg *config.Config) Mapper {
	m := &mapper{}
	m.Cache = new(sync.Map)
	m.Config = cfg
	return m
}

func (m *mapper) Get(tenantName config.TenantName) (prism_tenant.PrismTenant, error) {
	if client, ok := m.getFromCache(tenantName); ok {
		return client, nil
	}

	tenantConfig, ok := m.Config.Tenants[tenantName]
	if !ok {
		log.WithField("tenant_name", tenantName).Error("no config for tenant name")
		return nil, errors.New("unable to find tenant config")
	}

	switch tenantConfig.Type {
	case config.TenantType_External:
		return m.getExternal(tenantName, tenantConfig)
	default:
		log.WithField("tenant_type", tenantConfig.Type).Error("invalid tenant type")
		return nil, errors.New("invalid tenant type")
	}
}

func (m *mapper) getExternal(tenantName config.TenantName, tenantConfig config.TenantConfig) (prism_tenant.PrismTenant, error) {
	if strings.Blank(tenantConfig.URL) {
		m.storeInCache(tenantName, nil)
		return nil, nil
	}

	if _, err := url.ParseRequestURI(tenantConfig.URL); err != nil {
		log.WithField("tenant_name", tenantName).Error("tenant configured with invalid url")
		return nil, errors.Wrapf(err, "invalid tenant URL")
	}

	httpClient, err := caller.NewHttpClient(m.Config, tenantConfig.S2SEnabled)
	if err != nil {
		log.WithField("tenant_name", tenantName).WithError(err).Error("failed to create S2S http client")
		return nil, err
	}

	if tenantConfig.TimeoutMilliseconds > 0 {
		httpClient.Timeout = time.Duration(tenantConfig.TimeoutMilliseconds) * time.Millisecond
	} else {
		httpClient.Timeout = defaultTenantTimeout
	}
	client := prism_tenant.NewPrismTenantProtobufClient(tenantConfig.URL, httpClient)
	m.storeInCache(tenantName, client)

	return client, nil
}

func (m *mapper) GetAll() map[config.TenantName]prism_tenant.PrismTenant {
	tMap := make(map[config.TenantName]prism_tenant.PrismTenant)
	tenants := m.Config.Tenants

	for name := range tenants {
		tenant, err := m.Get(name)
		if err != nil {
			log.WithField("tenant_name", name).WithError(err).Error("error getting tenant during GetAll tenants")
			continue
		}
		tMap[name] = tenant
	}

	return tMap
}

func (m *mapper) getFromCache(tenantName config.TenantName) (prism_tenant.PrismTenant, bool) {
	clientInterface, ok := m.Cache.Load(tenantName)
	if !ok {
		return nil, false
	}

	client, ok := clientInterface.(prism_tenant.PrismTenant)
	if !ok {
		return nil, false
	}

	return client, true
}

func (m *mapper) storeInCache(tenantName config.TenantName, tenant prism_tenant.PrismTenant) {
	m.Cache.Store(tenantName, tenant)
}
