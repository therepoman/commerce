package local

import (
	"context"

	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
)

type localTenant struct {
}

// NewLocalTenant creates a new local PrismtTenant that does resides within Prism
func NewLocalTenant() prism_tenant.PrismTenant {
	return &localTenant{}
}

func (v *localTenant) HealthCheck(ctx context.Context, req *prism_tenant.HealthCheckReq) (*prism_tenant.HealthCheckResp, error) {
	return nil, nil
}

func (v *localTenant) TokenizeAndValidateMessage(ctx context.Context, req *prism_tenant.TokenizeAndValidateMessageReq) (*prism_tenant.TokenizeAndValidateMessageResp, error) {
	return nil, nil
}

func (v *localTenant) ProcessMessage(ctx context.Context, req *prism_tenant.ProcessMessageReq) (*prism_tenant.ProcessMessageResp, error) {
	return nil, nil
}

func (v *localTenant) PostProcessMessage(ctx context.Context, req *prism_tenant.PostProcessMessageReq) (*prism_tenant.PostProcessMessageResp, error) {
	return nil, nil
}
