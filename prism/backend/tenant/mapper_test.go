package tenant

import (
	"testing"

	"code.justin.tv/commerce/prism/config"
	. "github.com/smartystreets/goconvey/convey"
)

func TestTenantMap_Get(t *testing.T) {
	Convey("TestTenantMap_Get", t, func() {
		mockConfig, err := config.LoadConfig(config.UnitTest)
		So(err, ShouldBeNil)

		Convey("Given a tenant mapper and empty local tenant map", func() {
			mapper := NewTestMapper(mockConfig)

			Convey("Errors when there is no tenant configured", func() {
				tenant, err := mapper.Get("unconfigured-tenant")
				So(err, ShouldNotBeNil)
				So(tenant, ShouldBeNil)
			})

			Convey("Errors when there is no type configured", func() {
				tenant, err := mapper.Get("no-type-tenant")
				So(err, ShouldNotBeNil)
				So(tenant, ShouldBeNil)
			})

			Convey("Errors when the tenant is configured with a bad type", func() {
				tenant, err := mapper.Get("bad-type-tenant")
				So(err, ShouldNotBeNil)
				So(tenant, ShouldBeNil)
			})

			Convey("Errors when there is a tenant configured with a blank url", func() {
				tenant, err := mapper.Get("no-url-tenant")
				So(err, ShouldBeNil)
				So(tenant, ShouldBeNil)
			})

			Convey("Errors when there is a tenant configured with a bad url", func() {
				tenant, err := mapper.Get("bad-url-tenant")
				So(err, ShouldNotBeNil)
				So(tenant, ShouldBeNil)
			})

			Convey("Errors for a local tenant that is in the config but not the local tenant map", func() {
				tenant, err := mapper.Get("voldemort")
				So(err, ShouldNotBeNil)
				So(tenant, ShouldBeNil)
			})

			Convey("Succeeds when there is an external tenant configured", func() {
				tenant, err := mapper.Get("payday")
				So(err, ShouldBeNil)
				So(tenant, ShouldNotBeNil)
			})
		})
	})
}
