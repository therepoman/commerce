package approve_automod_message_test

import (
	"testing"

	"code.justin.tv/commerce/prism/backend/validation/approve_automod_message"
	prism "code.justin.tv/commerce/prism/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func validRequest() *prism.ApproveAutoModMessageReq {
	return &prism.ApproveAutoModMessageReq{
		UserId:       "test-user-id",
		TargetUserId: "test-target-user-id",
		MessageId:    "test-message-id",
	}
}

func TestValidator_Validate(t *testing.T) {
	Convey("Given a validator", t, func() {
		validator := approve_automod_message.NewValidator()

		Convey("Errors on a nil request", func() {
			So(validator.Validate(nil), ShouldNotBeNil)
		})

		Convey("Errors on blank user ID", func() {
			req := validRequest()
			req.UserId = " "
			So(validator.Validate(req), ShouldNotBeNil)
		})

		Convey("Errors on blank target user ID", func() {
			req := validRequest()
			req.TargetUserId = " "
			So(validator.Validate(req), ShouldNotBeNil)
		})

		Convey("Errors on blank message id", func() {
			req := validRequest()
			req.MessageId = " "
			So(validator.Validate(req), ShouldNotBeNil)
		})
	})
}
