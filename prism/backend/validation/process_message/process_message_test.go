package process_message_test

import (
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/prism/backend/validation/process_message"
	prism "code.justin.tv/commerce/prism/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func validRequest() *prism.ProcessMessageReq {
	return &prism.ProcessMessageReq{
		ChannelId: "test-channel",
		UserId:    "test-user",
		MessageId: "test-message-id",
		Message:   random.String(500),
	}
}

func TestValidator_Validate(t *testing.T) {
	Convey("Given a validator", t, func() {
		validator := process_message.NewValidator()

		Convey("Errors on a nil request", func() {
			So(validator.Validate(nil), ShouldNotBeNil)
		})

		Convey("Errors on blank channel", func() {
			req := validRequest()
			req.ChannelId = " "
			So(validator.Validate(req), ShouldNotBeNil)
		})

		Convey("Errors on blank user", func() {
			req := validRequest()
			req.UserId = " "
			So(validator.Validate(req), ShouldNotBeNil)
		})

		Convey("Errors on blank message id", func() {
			req := validRequest()
			req.MessageId = " "
			So(validator.Validate(req), ShouldNotBeNil)
		})

		Convey("Errors on blank message", func() {
			req := validRequest()
			req.Message = " "
			So(validator.Validate(req), ShouldNotBeNil)
		})

		Convey("Errors on messages with hidden characters", func() {
			req := validRequest()
			req.Message = "cheer1"
			So(validator.Validate(req), ShouldBeNil)
			req.Message = "cheer1 trihard9\n"
			So(validator.Validate(req), ShouldBeNil)
			req.Message = "\tcheer1"
			So(validator.Validate(req), ShouldBeNil)
			req.Message = "cheer1, TRihard9!"
			So(validator.Validate(req), ShouldBeNil)
			req.Message = "trihard​9" // hidden zero width space (U+200B) character between 'd' and '9'
			So(validator.Validate(req), ShouldNotBeNil)
			req.Message = "PogChamp1 trihard​9999999999​" // hidden zero width space (U+200B) character between 'd' and '9', and at the end of the string
			So(validator.Validate(req), ShouldNotBeNil)
			req.Message = "cheer1 trihard9​\n" // hidden zero width space (U+200B) character between '9' and '\n'
			So(validator.Validate(req), ShouldNotBeNil)
		})

		Convey("Errors on message that is too long", func() {
			req := validRequest()
			req.Message = random.String(501)
			So(validator.Validate(req), ShouldNotBeNil)
		})

		Convey("Succeeds on valid request", func() {
			So(validator.Validate(validRequest()), ShouldBeNil)
		})
	})
}
