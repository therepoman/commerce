package process_message

import (
	"unicode/utf8"

	"code.justin.tv/commerce/gogogadget/strings"
	prism "code.justin.tv/commerce/prism/rpc"
	"github.com/twitchtv/twirp"
)

const (
	maxMessageLength = 500
)

// Validator represents an ProcessMessage request validator
type Validator interface {
	Validate(req *prism.ProcessMessageReq) error
}

type validator struct {
}

// NewValidator creates a new Validator
func NewValidator() Validator {
	return &validator{}
}

func (v *validator) Validate(req *prism.ProcessMessageReq) error {
	if req == nil {
		return twirp.InvalidArgumentError("request", "cannot be nil")
	} else if strings.Blank(req.UserId) {
		return twirp.InvalidArgumentError("user_id", "cannot be blank")
	} else if strings.Blank(req.ChannelId) {
		return twirp.InvalidArgumentError("channel_id", "cannot be blank")
	} else if strings.Blank(req.MessageId) {
		return twirp.InvalidArgumentError("message_id", "cannot be blank")
	} else if strings.Blank(req.Message) {
		return twirp.InvalidArgumentError("message", "cannot be blank")
	} else if strings.ContainsHiddenCharacters(req.Message) {
		return twirp.InvalidArgumentError("message", "cannot contain hidden characters")
	} else if utf8.RuneCountInString(req.Message) > maxMessageLength {
		return twirp.InvalidArgumentError("message", "message exceeds maximum length")
	}

	return nil
}
