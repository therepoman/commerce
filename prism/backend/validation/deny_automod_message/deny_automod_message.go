package deny_automod_message

import (
	"code.justin.tv/commerce/gogogadget/strings"
	prism "code.justin.tv/commerce/prism/rpc"
	"github.com/twitchtv/twirp"
)

// Validator represents an DenyAutoModMessage request validator
type Validator interface {
	Validate(req *prism.DenyAutoModMessageReq) error
}

type validator struct {
}

// NewValidator creates a new Validator
func NewValidator() Validator {
	return &validator{}
}

func (v *validator) Validate(req *prism.DenyAutoModMessageReq) error {
	if req == nil {
		return twirp.InvalidArgumentError("request", "cannot be nil")
	} else if strings.Blank(req.UserId) {
		return twirp.RequiredArgumentError("UserId")
	} else if strings.Blank(req.TargetUserId) {
		return twirp.RequiredArgumentError("TargetUserId")
	} else if strings.Blank(req.MessageId) {
		return twirp.RequiredArgumentError("MessageId")
	}

	return nil
}
