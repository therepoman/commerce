package landlord

import (
	"context"
	"fmt"
	"sync"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/backend/tenant"
	"code.justin.tv/commerce/prism/backend/utils"
	"code.justin.tv/commerce/prism/backend/utils/client"
	"code.justin.tv/commerce/prism/config"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
)

func tokenizeAndValidateLatencyKey(tenant string) string {
	return fmt.Sprintf("%s_tokenize_and_validate_latency", tenant)
}

func tokenizeAndValidateErrorsKey(tenant string) string {
	return fmt.Sprintf("%s_tokenize_and_validate_errors", tenant)
}

func anyTokenizeAndValidateErr(respMap map[config.TenantName]tenant.TokenizeAndValidateResp) int64 {
	for _, resp := range respMap {
		if resp.Error != nil {
			return 1
		}
	}
	return 0
}

func tenantTokenizeAndValidateResponseCount(respMap *sync.Map) int {
	responded := 0
	respMap.Range(func(key, value interface{}) bool {
		tenantRespValue, ok := value.(tenant.TokenizeAndValidateResp)
		if !ok {
			log.Error("error converting tenant resp value key to TokenizeAndValidateResp")
			return true
		}

		if tenantRespValue.Error != TenantTimeoutError {
			responded++
		}
		return true
	})
	return responded
}

func (l *landlord) TokenizeAndValidate(ctx context.Context, req *tenant.TokenizeAndValidateReq) (respMap map[config.TenantName]tenant.TokenizeAndValidateResp) {
	startTime := time.Now()
	defer func() {
		latency := time.Since(startTime)

		l.Stats.TimingDuration(tokenizeAndValidateLatencyKey("landlord"), latency, 1)

		l.Stats.Inc(tokenizeAndValidateErrorsKey("landlord"), anyTokenizeAndValidateErr(respMap), 1)
	}()

	var tenantClients = make(map[config.TenantName]prism_tenant.PrismTenant)
	var tenantConsumables = make(map[config.TenantName]map[models.Consumable]int64)

	for consumableType, consumables := range req.ConsumablesRequested {
		tenantClient, tenantName, err := l.ConsumableMapper.GetTenant(consumableType)
		if err != nil {
			log.WithError(err).WithField("consumable_type", consumableType).Error("error getting tenant from consumable type")
			return nil
		}
		tenantClients[tenantName] = tenantClient
		tenantConsumables[tenantName] = consumables
	}

	if len(tenantClients) < 1 {
		return nil
	}

	tenantResps := new(sync.Map)
	tokenizeAndValidateRespChan := make(chan NamedTokenizeAndValidateResp)
	for name, t := range tenantClients {
		go func(t prism_tenant.PrismTenant, name config.TenantName) {
			tenantStartTime := time.Now()
			ctx := client.WithTwitchClientIDHeader(ctx)

			tenantRequest := &prism_tenant.TokenizeAndValidateMessageReq{
				Message:           req.Message,
				Anonymous:         req.Anonymous,
				ChannelId:         req.ChannelID,
				ConsumableAmounts: req.ConsumablesRequested.ToRawConsumables(),
				MessageId:         req.MessageID,
				UserId:            req.UserID,
				RoomId:            req.RoomID,
			}
			tenantResp, err := t.TokenizeAndValidateMessage(ctx, tenantRequest)
			tenantLatency := time.Since(tenantStartTime)

			l.Stats.TimingDuration(tokenizeAndValidateLatencyKey(string(name)), tenantLatency, 1)

			l.Stats.Inc(tokenizeAndValidateErrorsKey(string(name)), errorCount(err), 1)

			var tenantResponse tenant.TokenizeAndValidateResp
			if err != nil {
				tenantResponse = tenant.TokenizeAndValidateResp{
					Error: err,
				}
			} else {
				for _, token := range tenantResp.Tokens {
					tenantResponse.Tokens = append(tenantResponse.Tokens, &tenant.Token{
						Type:   string(name),
						Text:   token.Text,
						Amount: token.Amount,
					})
				}
			}

			tokenizeAndValidateRespChan <- NamedTokenizeAndValidateResp{
				TenantName: name,
				TenantResp: tenantResponse,
			}
		}(t, name)

		// Prepopulate the map in case of tenant time-outs
		tenantResps.Store(name, tenant.TokenizeAndValidateResp{
			Error: TenantTimeoutError,
		})
	}

TenantWaitLoop:
	for {
		select {
		case tokenizeAndValidateResp := <-tokenizeAndValidateRespChan:
			tenantResps.Store(tokenizeAndValidateResp.TenantName, tokenizeAndValidateResp.TenantResp)
			if tokenizeAndValidateResp.TenantResp.Error != nil {
				twErr := utils.ToTwirpError(tokenizeAndValidateResp.TenantResp.Error)

				if utils.IsA5XXError(twErr) {
					log.WithError(tokenizeAndValidateResp.TenantResp.Error).WithField("tenant", tokenizeAndValidateResp.TenantName).Error("error calling tenant tokenize and validate")
				}
			}

			if tenantTokenizeAndValidateResponseCount(tenantResps) >= len(tenantClients) {
				break TenantWaitLoop
			}
		case <-ctx.Done():
			log.Error("timed out calling tokenize and validate for all tenants")
			break TenantWaitLoop
		}
	}

	tenantResponseMap := make(map[config.TenantName]tenant.TokenizeAndValidateResp)
	tenantResps.Range(func(key, value interface{}) bool {
		tenantRespKey, ok := key.(config.TenantName)
		if !ok {
			log.WithField("tenantName", key).Error("error converting tenant resp key to TenantName")
			return true
		}

		tenantRespValue, ok := value.(tenant.TokenizeAndValidateResp)
		if !ok {
			log.Error("error converting tenant resp value key to TokenizeAndValidateResp")
			return true
		}

		tenantResponseMap[tenantRespKey] = tenantRespValue
		return true
	})

	return tenantResponseMap
}
