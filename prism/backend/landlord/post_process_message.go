package landlord

import (
	"context"
	"fmt"
	"sync"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/backend/tenant"
	"code.justin.tv/commerce/prism/backend/utils/client"
	"code.justin.tv/commerce/prism/config"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
)

func postProcessMessageLatencyKey(tenant string) string {
	return fmt.Sprintf("%s_post_process_message_latency", tenant)
}

func postProcessMessageErrorsKey(tenant string) string {
	return fmt.Sprintf("%s_post_process_message_errors", tenant)
}

func anyPostProcessMessageErr(respMap map[config.TenantName]tenant.PostProcessMessageResp) int64 {
	for _, resp := range respMap {
		if resp.Error != nil {
			return 1
		}
	}
	return 0
}

func tenantPostProcessMessageResponseCount(respMap *sync.Map) int {
	responded := 0
	respMap.Range(func(key, value interface{}) bool {
		tenantRespValue, ok := value.(tenant.PostProcessMessageResp)
		if !ok {
			log.Error("error converting tenant resp value key to PostProcessMessageResp")
			return true
		}

		if tenantRespValue.Error != TenantTimeoutError {
			responded++
		}
		return true
	})
	return responded
}

func (l *landlord) PostProcessMessage(ctx context.Context, req *tenant.PostProcessMessageReq) (respMap map[config.TenantName]tenant.PostProcessMessageResp) {
	startTime := time.Now()
	defer func() {
		latency := time.Since(startTime)

		l.Stats.TimingDuration(postProcessMessageLatencyKey("landlord"), latency, 1)

		l.Stats.Inc(postProcessMessageErrorsKey("landlord"), anyPostProcessMessageErr(respMap), 1)
	}()

	var tenantClients = make(map[config.TenantName]prism_tenant.PrismTenant)
	var tenantConsumables = make(map[config.TenantName]map[models.Consumable]int64)

	for tenantName, consumableMap := range req.ConsumablesProcessed {
		tenantClient, err := l.TenantMapper.Get(tenantName)
		if err != nil {
			log.WithError(err).WithField("tenant_name", tenantName).Error("error getting tenant from tenant name")
			return nil
		}

		tenantClients[tenantName] = tenantClient
		tenantConsumables[tenantName] = consumableMap.Flatten()
	}

	if len(tenantClients) < 1 {
		log.WithField("request", req).Error("no tenants processed during landlord processing")
		return nil
	}

	tenantResps := new(sync.Map)
	postProcessMessageRespChan := make(chan NamedPostProcessMessageResp)
	for name, t := range tenantClients {
		go func(t prism_tenant.PrismTenant, name config.TenantName) {
			tenantStartTime := time.Now()
			ctx := client.WithTwitchClientIDHeader(ctx)

			tenantRequest := &prism_tenant.PostProcessMessageReq{
				Message:           req.Message,
				Anonymous:         req.Anonymous,
				ChannelId:         req.ChannelID,
				ConsumableAmounts: models.ToRawConsumables(tenantConsumables[name]),
				MessageId:         req.MessageID,
				UserId:            req.UserID,
				RoomId:            req.RoomID,
				GeoIpMetadata:     &req.GeoIPMetadata,
				SentMessage:       req.SentMessage,
			}
			_, err := t.PostProcessMessage(ctx, tenantRequest)
			tenantLatency := time.Since(tenantStartTime)

			l.Stats.TimingDuration(postProcessMessageLatencyKey(string(name)), tenantLatency, 1)

			l.Stats.Inc(postProcessMessageErrorsKey(string(name)), errorCount(err), 1)

			tenantResponse := tenant.PostProcessMessageResp{
				Error: err,
			}

			postProcessMessageRespChan <- NamedPostProcessMessageResp{
				TenantName: name,
				TenantResp: tenantResponse,
			}
		}(t, name)

		// Prepopulate the map in case of tenant time-outs
		tenantResps.Store(name, tenant.PostProcessMessageResp{
			Error: TenantTimeoutError,
		})
	}

TenantWaitLoop:
	for {
		select {
		case postProcessMessageResp := <-postProcessMessageRespChan:
			tenantResps.Store(postProcessMessageResp.TenantName, postProcessMessageResp.TenantResp)
			if postProcessMessageResp.TenantResp.Error != nil {
				log.WithError(postProcessMessageResp.TenantResp.Error).WithField("tenant", postProcessMessageResp.TenantName).Error("error calling tenant post process message")
			}

			if tenantPostProcessMessageResponseCount(tenantResps) >= len(tenantClients) {
				break TenantWaitLoop
			}
		case <-ctx.Done():
			log.Error("timed out calling post process message for all tenants")
			break TenantWaitLoop
		}
	}

	tenantResponseMap := make(map[config.TenantName]tenant.PostProcessMessageResp)
	tenantResps.Range(func(key, value interface{}) bool {
		tenantRespKey, ok := key.(config.TenantName)
		if !ok {
			log.WithField("tenantName", key).Error("error converting tenant resp key to TenantName")
			return true
		}

		tenantRespValue, ok := value.(tenant.PostProcessMessageResp)
		if !ok {
			log.Error("error converting tenant resp value key to PostProcessMessageResp")
			return true
		}

		tenantResponseMap[tenantRespKey] = tenantRespValue
		return true
	})
	return tenantResponseMap
}
