package landlord_test

import (
	"context"
	"errors"
	"testing"
	"time"

	ll "code.justin.tv/commerce/prism/backend/landlord"
	"code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/backend/tenant"
	"code.justin.tv/commerce/prism/config"
	stats_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/stats"
	consumable_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/consumable"
	tenant_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/tenant"
	prism_tenant_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/rpc/tenant"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	"code.justin.tv/commerce/prism/test"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

var tokenizeAndValidateTestError1 = errors.New("test-error-1")
var tokenizeAndValidateTestError2 = errors.New("test-error-2")
var tokenizeAndValidateTestError3 = errors.New("test-error-3")

func testTokenizeAndValidate(c context.Context, landlord ll.Landlord, stats *stats_mock.Statter, expectedResps map[config.TenantName]tenant.TokenizeAndValidateResp, expectedMetrics test.Range, req *tenant.TokenizeAndValidateReq) {
	actualResp := landlord.TokenizeAndValidate(c, req)
	for expectedTenantName, expectedTokenizeAndValidateResp := range expectedResps {
		actualTokenizeAndValidateResp, ok := actualResp[expectedTenantName]

		So(ok, ShouldBeTrue)
		So(actualTokenizeAndValidateResp.Error, ShouldEqual, expectedTokenizeAndValidateResp.Error)
		So(len(actualTokenizeAndValidateResp.Tokens), ShouldEqual, len(expectedTokenizeAndValidateResp.Tokens))
	}
	test.AssertInRange(test.GetNumCalls(&stats.Mock, "TimingDuration"), expectedMetrics)
	test.AssertInRange(test.GetNumCalls(&stats.Mock, "Inc"), expectedMetrics)
}

func TestTokenizeAndValidate_NoTenants(t *testing.T) {
	Convey("Given a landlord without tenants", t, func() {
		stats := new(stats_mock.Statter)
		tenantMapper := new(tenant_mock.Mapper)
		consumableMapper := new(consumable_mock.Mapper)

		landlord := ll.NewTestLandlord(stats, tenantMapper, consumableMapper)
		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		tenantMapper.On("Get", mock.Anything).Return(nil, errors.New("test error"))

		Convey("Landlord should return an empty response", func() {
			req := &tenant.TokenizeAndValidateReq{
				UserID:    "1234",
				ChannelID: "5678",
				MessageID: "test-message-id",
				Message:   "test message",
			}

			testTokenizeAndValidate(context.Background(), landlord, stats, nil, test.NewRange(1, 1), req)
		})
	})
}

func TestTokenizeAndValidate_OneTenant(t *testing.T) {
	Convey("Given a landlord with a single tenant", t, func() {
		stats := new(stats_mock.Statter)
		tenant1 := new(prism_tenant_mock.PrismTenant)
		tenantMapper := new(tenant_mock.Mapper)
		consumableMapper := new(consumable_mock.Mapper)

		landlord := ll.NewTestLandlord(stats, tenantMapper, consumableMapper)
		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		req := &tenant.TokenizeAndValidateReq{
			UserID:    "1234",
			ChannelID: "5678",
			MessageID: "test-message-id",
			Message:   "test message",
			ConsumablesRequested: models.ConsumableMap{
				"test-type-1": {
					"test-consumable-1": 1,
				},
			},
		}
		consumableMapper.On("GetTenant", config.ConsumableType("test-type-1")).Return(tenant1, config.TenantName("test-tenant-1"), nil)

		Convey("When the tenant errors", func() {
			tenant1.On("TokenizeAndValidateMessage", mock.Anything, mock.Anything).Return(nil, tokenizeAndValidateTestError1)

			Convey("The landlord should return the correct response", func() {
				testTokenizeAndValidate(context.Background(), landlord, stats, map[config.TenantName]tenant.TokenizeAndValidateResp{
					config.TenantName("test-tenant-1"): {
						Error: tokenizeAndValidateTestError1,
					},
				}, test.NewRange(2, 2), req)
			})
		})

		Convey("When the tenant does not error", func() {
			tenant1.On("TokenizeAndValidateMessage", mock.Anything, mock.Anything).Return(&prism_tenant.TokenizeAndValidateMessageResp{
				ConsumableAmounts: map[string]int64{"test-consumable-1": 1},
			}, nil)

			Convey("The landlord should return the correct response", func() {
				testTokenizeAndValidate(context.Background(), landlord, stats, map[config.TenantName]tenant.TokenizeAndValidateResp{
					config.TenantName("test-tenant-1"): {
						Error:  nil,
						Tokens: []*tenant.Token{},
					},
				}, test.NewRange(2, 2), req)
			})
		})
	})
}

func TestTokenizeAndValidate_MultipleTenants(t *testing.T) {
	Convey("Given a landlord with a multiple tenants", t, func() {
		stats := new(stats_mock.Statter)
		tenant1 := new(prism_tenant_mock.PrismTenant)
		tenant2 := new(prism_tenant_mock.PrismTenant)
		tenant3 := new(prism_tenant_mock.PrismTenant)
		tenantMapper := new(tenant_mock.Mapper)
		consumableMapper := new(consumable_mock.Mapper)

		landlord := ll.NewTestLandlord(stats, tenantMapper, consumableMapper)
		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		req := &tenant.TokenizeAndValidateReq{
			UserID:    "1234",
			ChannelID: "5678",
			MessageID: "test-message-id",
			Message:   "test message",
			ConsumablesRequested: models.ConsumableMap{
				"test-type-1": {
					"test-consumable-1": 1,
				},
				"test-type-2": {
					"test-consumable-2": 4,
				},
				"test-type-3": {
					"test-tenant-3": 20,
				},
			},
		}

		consumableMapper.On("GetTenant", config.ConsumableType("test-type-1")).Return(tenant1, config.TenantName("test-tenant-1"), nil)
		consumableMapper.On("GetTenant", config.ConsumableType("test-type-2")).Return(tenant2, config.TenantName("test-tenant-2"), nil)
		consumableMapper.On("GetTenant", config.ConsumableType("test-type-3")).Return(tenant3, config.TenantName("test-tenant-3"), nil)

		Convey("When one tenant errors", func() {
			tenant1.On("TokenizeAndValidateMessage", mock.Anything, mock.Anything).Return(&prism_tenant.TokenizeAndValidateMessageResp{
				ConsumableAmounts: map[string]int64{"test-consumable-1": 1},
			}, nil)
			tenant2.On("TokenizeAndValidateMessage", mock.Anything, mock.Anything).Return(nil, tokenizeAndValidateTestError2)
			tenant3.On("TokenizeAndValidateMessage", mock.Anything, mock.Anything).Return(&prism_tenant.TokenizeAndValidateMessageResp{
				ConsumableAmounts: map[string]int64{"test-consumable-3": 20},
			}, nil)

			Convey("The landlord should return the correct response", func() {
				testTokenizeAndValidate(context.Background(), landlord, stats, map[config.TenantName]tenant.TokenizeAndValidateResp{
					config.TenantName("test-tenant-1"): {
						Error:  nil,
						Tokens: []*tenant.Token{},
					},
					config.TenantName("test-tenant-2"): {
						Error: tokenizeAndValidateTestError2,
					},
					config.TenantName("test-tenant-3"): {
						Error:  nil,
						Tokens: []*tenant.Token{},
					},
				}, test.NewRange(4, 4), req)
			})
		})

		Convey("When all tenants error", func() {
			tenant1.On("TokenizeAndValidateMessage", mock.Anything, mock.Anything).Return(nil, tokenizeAndValidateTestError1)
			tenant2.On("TokenizeAndValidateMessage", mock.Anything, mock.Anything).Return(nil, tokenizeAndValidateTestError2)
			tenant3.On("TokenizeAndValidateMessage", mock.Anything, mock.Anything).Return(nil, tokenizeAndValidateTestError3)

			Convey("The landlord should return the correct response", func() {
				testTokenizeAndValidate(context.Background(), landlord, stats, map[config.TenantName]tenant.TokenizeAndValidateResp{
					config.TenantName("test-tenant-1"): {
						Error: tokenizeAndValidateTestError1,
					},
					config.TenantName("test-tenant-2"): {
						Error: tokenizeAndValidateTestError2,
					},
					config.TenantName("test-tenant-3"): {
						Error: tokenizeAndValidateTestError3,
					},
				}, test.NewRange(4, 4), req)
			})
		})

		Convey("When no tenants error", func() {
			tenant1.On("TokenizeAndValidateMessage", mock.Anything, mock.Anything).Return(&prism_tenant.TokenizeAndValidateMessageResp{
				ConsumableAmounts: map[string]int64{"test-consumable-1": 1},
			}, nil)
			tenant2.On("TokenizeAndValidateMessage", mock.Anything, mock.Anything).Return(&prism_tenant.TokenizeAndValidateMessageResp{
				ConsumableAmounts: map[string]int64{"test-consumable-2": 4},
			}, nil)
			tenant3.On("TokenizeAndValidateMessage", mock.Anything, mock.Anything).Return(&prism_tenant.TokenizeAndValidateMessageResp{
				ConsumableAmounts: map[string]int64{"test-consumable-3": 20},
			}, nil)

			Convey("The landlord should return the correct response", func() {
				testTokenizeAndValidate(context.Background(), landlord, stats, map[config.TenantName]tenant.TokenizeAndValidateResp{
					config.TenantName("test-tenant-1"): {
						Error:  nil,
						Tokens: []*tenant.Token{},
					},
					config.TenantName("test-tenant-2"): {
						Error:  nil,
						Tokens: []*tenant.Token{},
					},
					config.TenantName("test-tenant-3"): {
						Error:  nil,
						Tokens: []*tenant.Token{},
					},
				}, test.NewRange(4, 4), req)
			})
		})
	})
}

func TestTokenizeAndValidate_LateTenant(t *testing.T) {
	Convey("Given a landlord with a late tenant", t, func() {
		stats := new(stats_mock.Statter)
		tenant1 := new(prism_tenant_mock.PrismTenant)
		lateTenant := new(prism_tenant_mock.PrismTenant)
		tenant3 := new(prism_tenant_mock.PrismTenant)
		tenantMapper := new(tenant_mock.Mapper)
		consumableMapper := new(consumable_mock.Mapper)

		landlord := ll.NewTestLandlord(stats, tenantMapper, consumableMapper)
		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		req := &tenant.TokenizeAndValidateReq{
			UserID:    "1234",
			ChannelID: "5678",
			MessageID: "test-message-id",
			Message:   "test message",
			ConsumablesRequested: models.ConsumableMap{
				"test-type-1": {
					"test-consumable-1": 1,
				},
				"test-type-2": {
					"test-consumable-2": 4,
				},
				"test-type-3": {
					"test-consumable-3": 20,
				},
			},
		}

		consumableMapper.On("GetTenant", config.ConsumableType("test-type-1")).Return(tenant1, config.TenantName("test-tenant-1"), nil)
		consumableMapper.On("GetTenant", config.ConsumableType("test-type-2")).Return(lateTenant, config.TenantName("late-tenant"), nil)
		consumableMapper.On("GetTenant", config.ConsumableType("test-type-3")).Return(tenant3, config.TenantName("test-tenant-3"), nil)

		Convey("When all other tenants error", func() {
			tenant1.On("TokenizeAndValidateMessage", mock.Anything, mock.Anything).Return(nil, tokenizeAndValidateTestError1)
			lateTenant.On("TokenizeAndValidateMessage", mock.Anything, mock.Anything).WaitUntil(nil).Return(&prism_tenant.TokenizeAndValidateMessageResp{
				ConsumableAmounts: map[string]int64{"test-consumable-2": 4},
			}, nil)
			tenant3.On("TokenizeAndValidateMessage", mock.Anything, mock.Anything).Return(nil, tokenizeAndValidateTestError3)

			Convey("The landlord should still wait on the late tenant", func() {
				testTokenizeAndValidate(context.Background(), landlord, stats, map[config.TenantName]tenant.TokenizeAndValidateResp{
					config.TenantName("test-tenant-1"): {
						Error: tokenizeAndValidateTestError1,
					},
					config.TenantName("late-tenant"): {
						Tokens: []*tenant.Token{},
						Error:  nil,
					},
					config.TenantName("test-tenant-3"): {
						Error: tokenizeAndValidateTestError3,
					},
				}, test.NewRange(4, 4), req)
			})
		})

		Convey("When all other tenants succeed", func() {
			tenant1.On("TokenizeAndValidateMessage", mock.Anything, mock.Anything).Return(&prism_tenant.TokenizeAndValidateMessageResp{
				ConsumableAmounts: map[string]int64{"test-consumable-1": 1},
			}, nil)
			lateTenant.On("TokenizeAndValidateMessage", mock.Anything, mock.Anything).WaitUntil(nil).Return(&prism_tenant.TokenizeAndValidateMessageResp{
				ConsumableAmounts: map[string]int64{"test-consumable-2": 4},
			}, nil)
			tenant3.On("TokenizeAndValidateMessage", mock.Anything, mock.Anything).Return(&prism_tenant.TokenizeAndValidateMessageResp{
				ConsumableAmounts: map[string]int64{"test-consumable-3": 20},
			}, nil)

			Convey("The landlord waits on the late tenant before succeeding", func() {
				testTokenizeAndValidate(context.Background(), landlord, stats, map[config.TenantName]tenant.TokenizeAndValidateResp{
					config.TenantName("test-tenant-1"): {
						Tokens: []*tenant.Token{},
						Error:  nil,
					},
					config.TenantName("late-tenant"): {
						Tokens: []*tenant.Token{},
						Error:  nil,
					},
					config.TenantName("test-tenant-3"): {
						Tokens: []*tenant.Token{},
						Error:  nil,
					},
				}, test.NewRange(4, 4), req)
			})
		})

		Convey("When all other tenants succeed and the context has a timeout", func() {
			tenant1.On("TokenizeAndValidateMessage", mock.Anything, mock.Anything).Return(&prism_tenant.TokenizeAndValidateMessageResp{
				ConsumableAmounts: map[string]int64{"test-consumable-1": 1},
			}, nil)
			lateTenant.On("TokenizeAndValidateMessage", mock.Anything, mock.Anything).WaitUntil(time.After(time.Second)).Return(&prism_tenant.TokenizeAndValidateMessageResp{
				ConsumableAmounts: map[string]int64{"test-consumable-2": 1},
			}, nil)
			tenant3.On("TokenizeAndValidateMessage", mock.Anything, mock.Anything).Return(&prism_tenant.TokenizeAndValidateMessageResp{
				ConsumableAmounts: map[string]int64{"test-consumable-3": 20},
			}, nil)
			ctx, cancel := context.WithTimeout(context.Background(), 20*time.Millisecond)

			Convey("The landlord should return a timeout error", func() {
				testTokenizeAndValidate(ctx, landlord, stats, map[config.TenantName]tenant.TokenizeAndValidateResp{
					config.TenantName("test-tenant-1"): {
						Tokens: []*tenant.Token{},
						Error:  nil,
					},
					config.TenantName("late-tenant"): {
						Error: ll.TenantTimeoutError,
					},
					config.TenantName("test-tenant-3"): {
						Tokens: []*tenant.Token{},
						Error:  nil,
					},
				}, test.NewRange(3, 4), req)
				cancel()
			})
		})
	})
}
