package landlord

import (
	"context"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/config"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
)

func healthCheckLatencyKey(tenant string) string {
	return fmt.Sprintf("%s_health_check_latency", tenant)
}

func healthCheckErrorsKey(tenant string) string {
	return fmt.Sprintf("%s_health_check_errors", tenant)
}

func (l *landlord) PerformTenantHealthChecks(ctx context.Context) (err error) {
	startTime := time.Now()
	defer func() {
		latency := time.Since(startTime)

		l.Stats.TimingDuration(healthCheckLatencyKey("landlord"), latency, 1)

		l.Stats.Inc(healthCheckErrorsKey("landlord"), errorCount(err), 1)
	}()

	tenants := l.TenantMapper.GetAll()

	if len(tenants) < 1 {
		return nil
	}

	healthCheckErrChan := make(chan NamedError)
	for name, t := range tenants {
		go func(pt prism_tenant.PrismTenant, name config.TenantName) {
			tenantStartTime := time.Now()

			tenantRequest := &prism_tenant.HealthCheckReq{}

			_, err := pt.HealthCheck(ctx, tenantRequest)
			tenantLatency := time.Since(tenantStartTime)

			l.Stats.TimingDuration(healthCheckLatencyKey(string(name)), tenantLatency, 1)

			l.Stats.Inc(healthCheckErrorsKey(string(name)), errorCount(err), 1)

			healthCheckErrChan <- NamedError{
				TenantName: name,
				Error:      err,
			}
		}(t, name)
	}

	tenantErrs := make(map[config.TenantName]error)
TenantWaitLoop:
	for {
		select {
		case tenantErr := <-healthCheckErrChan:
			if tenantErr.Error != nil {
				log.WithError(tenantErr.Error).WithField("tenant", tenantErr.TenantName).Error("error calling tenant health check")
				return TenantError
			}

			tenantErrs[tenantErr.TenantName] = tenantErr.Error
			if len(tenantErrs) >= len(tenants) {
				break TenantWaitLoop
			}
		case <-ctx.Done():
			log.Error("timed out calling health check for all tenants")
			return LandlordTimeoutError
		}
	}

	return nil
}
