package landlord

import (
	"context"
	"errors"

	"code.justin.tv/commerce/prism/backend/clients/stats"
	"code.justin.tv/commerce/prism/backend/consumable"
	"code.justin.tv/commerce/prism/backend/tenant"
	"code.justin.tv/commerce/prism/config"
)

var TenantError = errors.New("tenant error")
var TenantTimeoutError = errors.New("tenant timed out")
var LandlordTimeoutError = errors.New("timed out processing tenants")

type landlord struct {
	Stats            stats.Statter     `inject:""`
	TenantMapper     tenant.Mapper     `inject:""`
	ConsumableMapper consumable.Mapper `inject:""`
}

// Landlord represents a tenant landlord that manages how tenants call various operations simulataneously
type Landlord interface {
	PerformTenantHealthChecks(ctx context.Context) error
	TokenizeAndValidate(ctx context.Context, req *tenant.TokenizeAndValidateReq) map[config.TenantName]tenant.TokenizeAndValidateResp
	ProcessMessage(ctx context.Context, req *tenant.ProcessMessageReq) map[config.TenantName]tenant.ProcessMessageResp
	PostProcessMessage(ctx context.Context, req *tenant.PostProcessMessageReq) map[config.TenantName]tenant.PostProcessMessageResp
}

// NewLandlord creates a new Landlord
func NewLandlord() Landlord {
	return &landlord{}
}

// NewTestLandlord creates a new test Landlord
func NewTestLandlord(stats stats.Statter, tenantMapper tenant.Mapper, consumableMapper consumable.Mapper) Landlord {
	return &landlord{
		Stats:            stats,
		TenantMapper:     tenantMapper,
		ConsumableMapper: consumableMapper,
	}
}

type NamedTokenizeAndValidateResp struct {
	TenantName config.TenantName
	TenantResp tenant.TokenizeAndValidateResp
}

type NamedProcessMessageResp struct {
	TenantName config.TenantName
	TenantResp tenant.ProcessMessageResp
}

type NamedPostProcessMessageResp struct {
	TenantName config.TenantName
	TenantResp tenant.PostProcessMessageResp
}

type NamedError struct {
	TenantName config.TenantName
	Error      error
}

func errorCount(err error) int64 {
	if err != nil {
		return 1
	}
	return 0
}
