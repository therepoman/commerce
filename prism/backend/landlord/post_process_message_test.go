package landlord_test

import (
	"context"
	"errors"
	"testing"
	"time"

	ll "code.justin.tv/commerce/prism/backend/landlord"
	"code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/backend/tenant"
	"code.justin.tv/commerce/prism/config"
	stats_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/stats"
	consumable_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/consumable"
	tenant_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/tenant"
	prism_tenant_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/rpc/tenant"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	"code.justin.tv/commerce/prism/test"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

var postProcessMessageTestError1 = errors.New("test-error-1")
var postProcessMessageTestError2 = errors.New("test-error-2")
var postProcessMessageTestError3 = errors.New("test-error-3")

func testPostProcessMessage(c context.Context, landlord ll.Landlord, stats *stats_mock.Statter, expectedResps map[config.TenantName]tenant.PostProcessMessageResp, expectedMetrics test.Range, req *tenant.PostProcessMessageReq) {
	actualResp := landlord.PostProcessMessage(c, req)
	for expectedTenantName, expectedPostProcessMessageResp := range expectedResps {
		actualPostProcessMessageResp, ok := actualResp[expectedTenantName]
		So(ok, ShouldBeTrue)
		So(actualPostProcessMessageResp.Error, ShouldEqual, expectedPostProcessMessageResp.Error)
	}
	test.AssertInRange(test.GetNumCalls(&stats.Mock, "TimingDuration"), expectedMetrics)
	test.AssertInRange(test.GetNumCalls(&stats.Mock, "Inc"), expectedMetrics)
}

func TestPostProcessMessage_NoTenants(t *testing.T) {
	Convey("Given a landlord without tenants", t, func() {
		stats := new(stats_mock.Statter)
		tenantMapper := new(tenant_mock.Mapper)
		consumableMapper := new(consumable_mock.Mapper)

		landlord := ll.NewTestLandlord(stats, tenantMapper, consumableMapper)
		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		tenantMapper.On("Get", config.TenantName("test-tenant-1")).Return(nil, errors.New("test error"))

		Convey("Landlord should return an empty response", func() {
			req := &tenant.PostProcessMessageReq{
				UserID:    "1234",
				ChannelID: "5678",
				MessageID: "test-message-id",
				Message:   "test message",
				ConsumablesProcessed: map[config.TenantName]models.ConsumableMap{
					"test-tenant-1": {
						"test-type-1": {
							"test-consumable-1": 1,
						},
					},
				},
				SentMessage: "test ***",
			}

			testPostProcessMessage(context.Background(), landlord, stats, nil, test.NewRange(1, 1), req)
		})
	})
}

func TestLandlordPostProcessMessage_OneTenant(t *testing.T) {
	Convey("Given a landlord with a single tenant", t, func() {
		stats := new(stats_mock.Statter)
		tenant1 := new(prism_tenant_mock.PrismTenant)
		tenantMapper := new(tenant_mock.Mapper)
		consumableMapper := new(consumable_mock.Mapper)

		landlord := ll.NewTestLandlord(stats, tenantMapper, consumableMapper)
		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		tenantMapper.On("Get", config.TenantName("test-tenant-1")).Return(tenant1, nil)

		Convey("When the tenant errors", func() {
			tenant1.On("PostProcessMessage", mock.Anything, mock.Anything).Return(nil, postProcessMessageTestError1)

			Convey("The landlord should return the correct response", func() {
				req := &tenant.PostProcessMessageReq{
					UserID:    "1234",
					ChannelID: "5678",
					MessageID: "test-message-id",
					Message:   "test message",
					ConsumablesProcessed: map[config.TenantName]models.ConsumableMap{
						"test-tenant-1": {
							"test-type-1": {
								"test-consumable-1": 1,
							},
						},
					},
					SentMessage: "test ***",
				}

				testPostProcessMessage(context.Background(), landlord, stats, map[config.TenantName]tenant.PostProcessMessageResp{
					config.TenantName("test-tenant-1"): {
						Error: postProcessMessageTestError1,
					},
				}, test.NewRange(2, 2), req)
			})
		})

		Convey("When the tenant does not error", func() {
			tenant1.On("PostProcessMessage", mock.Anything, mock.Anything).Return(&prism_tenant.PostProcessMessageResp{}, nil)

			Convey("The landlord should return the correct response", func() {
				req := &tenant.PostProcessMessageReq{
					UserID:    "1234",
					ChannelID: "5678",
					MessageID: "test-message-id",
					Message:   "test message",
					ConsumablesProcessed: map[config.TenantName]models.ConsumableMap{
						"test-tenant-1": {
							"test-type-1": {
								"test-consumable-1": 1,
							},
						},
					},
					SentMessage: "test ***",
				}

				testPostProcessMessage(context.Background(), landlord, stats, map[config.TenantName]tenant.PostProcessMessageResp{
					config.TenantName("test-tenant-1"): {
						Error: nil,
					},
				}, test.NewRange(2, 2), req)
			})
		})
	})
}

func TestLandlordPostProcessMessage_MultipleTenants(t *testing.T) {
	Convey("Given a landlord with a multiple tenants", t, func() {
		stats := new(stats_mock.Statter)
		tenant1 := new(prism_tenant_mock.PrismTenant)
		tenant2 := new(prism_tenant_mock.PrismTenant)
		tenant3 := new(prism_tenant_mock.PrismTenant)
		tenantMapper := new(tenant_mock.Mapper)
		consumableMapper := new(consumable_mock.Mapper)

		landlord := ll.NewTestLandlord(stats, tenantMapper, consumableMapper)
		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		tenantMapper.On("Get", config.TenantName("test-tenant-1")).Return(tenant1, nil)
		tenantMapper.On("Get", config.TenantName("test-tenant-2")).Return(tenant2, nil)
		tenantMapper.On("Get", config.TenantName("test-tenant-3")).Return(tenant3, nil)

		req := &tenant.PostProcessMessageReq{
			UserID:    "1234",
			ChannelID: "5678",
			MessageID: "test-message-id",
			Message:   "test message",
			ConsumablesProcessed: map[config.TenantName]models.ConsumableMap{
				"test-tenant-1": {
					"test-type-1": {
						"test-consumable-1": 1,
					},
				},
				"test-tenant-2": {
					"test-type-2": {
						"test-consumable-2": 4,
					},
				},
				"test-tenant-3": {
					"test-type-3": {
						"test-consumable-3": 20,
					},
				},
			},
		}

		Convey("When one tenant errors", func() {
			tenant1.On("PostProcessMessage", mock.Anything, mock.Anything).Return(&prism_tenant.PostProcessMessageResp{}, nil)
			tenant2.On("PostProcessMessage", mock.Anything, mock.Anything).Return(nil, postProcessMessageTestError2)
			tenant3.On("PostProcessMessage", mock.Anything, mock.Anything).Return(&prism_tenant.PostProcessMessageResp{}, nil)

			Convey("The landlord should return the correct response", func() {
				testPostProcessMessage(context.Background(), landlord, stats, map[config.TenantName]tenant.PostProcessMessageResp{
					config.TenantName("test-tenant-1"): {
						Error: nil,
					},
					config.TenantName("test-tenant-2"): {
						Error: postProcessMessageTestError2,
					},
					config.TenantName("test-tenant-3"): {
						Error: nil,
					},
				}, test.NewRange(4, 4), req)
			})
		})

		Convey("When all tenants error", func() {
			tenant1.On("PostProcessMessage", mock.Anything, mock.Anything).Return(nil, postProcessMessageTestError1)
			tenant2.On("PostProcessMessage", mock.Anything, mock.Anything).Return(nil, postProcessMessageTestError2)
			tenant3.On("PostProcessMessage", mock.Anything, mock.Anything).Return(nil, postProcessMessageTestError3)

			Convey("The landlord should return the correct response", func() {
				testPostProcessMessage(context.Background(), landlord, stats, map[config.TenantName]tenant.PostProcessMessageResp{
					config.TenantName("test-tenant-1"): {
						Error: postProcessMessageTestError1,
					},
					config.TenantName("test-tenant-2"): {
						Error: postProcessMessageTestError2,
					},
					config.TenantName("test-tenant-3"): {
						Error: postProcessMessageTestError3,
					},
				}, test.NewRange(4, 4), req)
			})
		})

		Convey("When no tenants error", func() {
			tenant1.On("PostProcessMessage", mock.Anything, mock.Anything).Return(&prism_tenant.PostProcessMessageResp{}, nil)
			tenant2.On("PostProcessMessage", mock.Anything, mock.Anything).Return(&prism_tenant.PostProcessMessageResp{}, nil)
			tenant3.On("PostProcessMessage", mock.Anything, mock.Anything).Return(&prism_tenant.PostProcessMessageResp{}, nil)

			Convey("The landlord should return the correct response", func() {
				testPostProcessMessage(context.Background(), landlord, stats, map[config.TenantName]tenant.PostProcessMessageResp{
					config.TenantName("test-tenant-1"): {
						Error: nil,
					},
					config.TenantName("test-tenant-2"): {
						Error: nil,
					},
					config.TenantName("test-tenant-3"): {
						Error: nil,
					},
				}, test.NewRange(4, 4), req)
			})
		})
	})
}

func TestPostProcessMessage_LateTenant(t *testing.T) {
	Convey("Given a landlord with a late tenant", t, func() {
		stats := new(stats_mock.Statter)
		tenant1 := new(prism_tenant_mock.PrismTenant)
		tenant2 := new(prism_tenant_mock.PrismTenant)
		tenant3 := new(prism_tenant_mock.PrismTenant)
		tenantMapper := new(tenant_mock.Mapper)
		consumableMapper := new(consumable_mock.Mapper)

		landlord := ll.NewTestLandlord(stats, tenantMapper, consumableMapper)
		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		tenantMapper.On("Get", config.TenantName("test-tenant-1")).Return(tenant1, nil)
		tenantMapper.On("Get", config.TenantName("late-tenant")).Return(tenant2, nil)
		tenantMapper.On("Get", config.TenantName("test-tenant-3")).Return(tenant3, nil)

		req := &tenant.PostProcessMessageReq{
			UserID:    "1234",
			ChannelID: "5678",
			MessageID: "test-message-id",
			Message:   "test message",
			ConsumablesProcessed: map[config.TenantName]models.ConsumableMap{
				"test-tenant-1": {
					"test-type-1": {
						"test-consumable-1": 1,
					},
				},
				"late-tenant": {
					"test-type-2": {
						"test-consumable-2": 4,
					},
				},
				"test-tenant-3": {
					"test-type-3": {
						"test-consumable-3": 20,
					},
				},
			},
		}

		Convey("When all other tenants error", func() {
			tenant1.On("PostProcessMessage", mock.Anything, mock.Anything).Return(nil, postProcessMessageTestError1)
			tenant2.On("PostProcessMessage", mock.Anything, mock.Anything).WaitUntil(nil).Return(&prism_tenant.PostProcessMessageResp{}, nil)
			tenant3.On("PostProcessMessage", mock.Anything, mock.Anything).Return(nil, postProcessMessageTestError3)

			Convey("The landlord should still wait on the late tenant", func() {
				testPostProcessMessage(context.Background(), landlord, stats, map[config.TenantName]tenant.PostProcessMessageResp{
					config.TenantName("test-tenant-1"): {
						Error: postProcessMessageTestError1,
					},
					config.TenantName("late-tenant"): {
						Error: nil,
					},
					config.TenantName("test-tenant-3"): {
						Error: postProcessMessageTestError3,
					},
				}, test.NewRange(4, 4), req)
			})
		})

		Convey("When all other tenants succeed", func() {
			tenant1.On("PostProcessMessage", mock.Anything, mock.Anything).Return(&prism_tenant.PostProcessMessageResp{}, nil)
			tenant2.On("PostProcessMessage", mock.Anything, mock.Anything).WaitUntil(nil).Return(&prism_tenant.PostProcessMessageResp{}, nil)
			tenant3.On("PostProcessMessage", mock.Anything, mock.Anything).Return(&prism_tenant.PostProcessMessageResp{}, nil)

			Convey("The landlord waits on the late tenant before succeeding", func() {
				testPostProcessMessage(context.Background(), landlord, stats, map[config.TenantName]tenant.PostProcessMessageResp{
					config.TenantName("test-tenant-1"): {
						Error: nil,
					},
					config.TenantName("late-tenant"): {
						Error: nil,
					},
					config.TenantName("test-tenant-3"): {
						Error: nil,
					},
				}, test.NewRange(4, 4), req)
			})
		})

		Convey("When all other tenants succeed and the context has a timeout", func() {
			tenant1.On("PostProcessMessage", mock.Anything, mock.Anything).Return(&prism_tenant.PostProcessMessageResp{}, nil)
			tenant2.On("PostProcessMessage", mock.Anything, mock.Anything).WaitUntil(time.After(time.Hour))
			tenant3.On("PostProcessMessage", mock.Anything, mock.Anything).Return(&prism_tenant.PostProcessMessageResp{}, nil)
			ctx, cancel := context.WithTimeout(context.Background(), 20*time.Millisecond)

			Convey("The landlord should return a timeout error", func() {
				testPostProcessMessage(ctx, landlord, stats, map[config.TenantName]tenant.PostProcessMessageResp{
					config.TenantName("test-tenant-1"): {
						Error: nil,
					},
					config.TenantName("late-tenant"): {
						Error: ll.TenantTimeoutError,
					},
					config.TenantName("test-tenant-3"): {
						Error: nil,
					},
				}, test.NewRange(3, 4), req)
				cancel()
			})
		})
	})
}
