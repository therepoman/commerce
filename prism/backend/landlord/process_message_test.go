package landlord_test

import (
	"context"
	"errors"
	"testing"
	"time"

	ll "code.justin.tv/commerce/prism/backend/landlord"
	"code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/backend/tenant"
	"code.justin.tv/commerce/prism/config"
	stats_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/stats"
	consumable_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/consumable"
	tenant_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/tenant"
	prism_tenant_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/rpc/tenant"
	prism "code.justin.tv/commerce/prism/rpc"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	"code.justin.tv/commerce/prism/test"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

var processMessageTestError1 = errors.New("test-error-1")
var processMessageTestError2 = errors.New("test-error-2")
var processMessageTestError3 = errors.New("test-error-3")

func testProcessMessage(c context.Context, landlord ll.Landlord, stats *stats_mock.Statter, expectedResps map[config.TenantName]tenant.ProcessMessageResp, expectedMetrics test.Range, req *tenant.ProcessMessageReq) {
	actualResp := landlord.ProcessMessage(c, req)
	for expectedTenantName, expectedProcessMessageResp := range expectedResps {
		actualProcessMessageResp, ok := actualResp[expectedTenantName]
		So(ok, ShouldBeTrue)
		So(actualProcessMessageResp.Error, ShouldEqual, expectedProcessMessageResp.Error)
		So(len(actualProcessMessageResp.Transformations), ShouldEqual, len(expectedProcessMessageResp.Transformations))
	}
	test.AssertInRange(test.GetNumCalls(&stats.Mock, "TimingDuration"), expectedMetrics)
	test.AssertInRange(test.GetNumCalls(&stats.Mock, "Inc"), expectedMetrics)
}

func TestProcessMessage_NoTenants(t *testing.T) {
	Convey("Given a landlord without tenants", t, func() {
		stats := new(stats_mock.Statter)
		tenantMapper := new(tenant_mock.Mapper)
		consumableMapper := new(consumable_mock.Mapper)

		landlord := ll.NewTestLandlord(stats, tenantMapper, consumableMapper)
		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		consumableMapper.On("GetTenant", config.ConsumableType("test-type-1")).Return(nil, config.TenantName(""), errors.New("test error"))

		Convey("Landlord should return an empty response", func() {
			req := &tenant.ProcessMessageReq{
				UserID:    "1234",
				ChannelID: "5678",
				MessageID: "test-message-id",
				Message:   "test message",
				ConsumablesRequested: models.ConsumableMap{
					"test-type-1": {
						"test-consumable-1": 1,
					},
				},
			}

			testProcessMessage(context.Background(), landlord, stats, nil, test.NewRange(1, 1), req)
		})
	})
}

func TestLandlordProcessMessage_OneTenant(t *testing.T) {
	Convey("Given a landlord with a single tenant", t, func() {
		stats := new(stats_mock.Statter)
		tenant1 := new(prism_tenant_mock.PrismTenant)
		tenantMapper := new(tenant_mock.Mapper)
		consumableMapper := new(consumable_mock.Mapper)

		landlord := ll.NewTestLandlord(stats, tenantMapper, consumableMapper)
		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		consumableMapper.On("GetTenant", config.ConsumableType("test-type-1")).Return(tenant1, config.TenantName("test-tenant-1"), nil)

		Convey("When the tenant errors", func() {
			tenant1.On("ProcessMessage", mock.Anything, mock.Anything).Return(nil, processMessageTestError1)

			Convey("The landlord should return the correct response", func() {
				req := &tenant.ProcessMessageReq{
					UserID:    "1234",
					ChannelID: "5678",
					MessageID: "test-message-id",
					Message:   "test message",
					ConsumablesRequested: models.ConsumableMap{
						"test-type-1": {
							"test-consumable-1": 1,
						},
					},
				}

				testProcessMessage(context.Background(), landlord, stats, map[config.TenantName]tenant.ProcessMessageResp{
					config.TenantName("test-tenant-1"): {
						Error: processMessageTestError1,
					},
				}, test.NewRange(2, 2), req)
			})
		})

		Convey("When the tenant does not error", func() {
			Convey("When the tenant returns an empty consumable map", func() {
				tenant1.On("ProcessMessage", mock.Anything, mock.Anything).Return(&prism_tenant.ProcessMessageResp{
					Transformations:  []*prism.Transformation{{}, {}},
					MessageTreatment: string(prism_models.DEFAULT_MESSAGE),
				}, nil)

				req := &tenant.ProcessMessageReq{
					UserID:    "1234",
					ChannelID: "5678",
					MessageID: "test-message-id",
					Message:   "test message",
					ConsumablesRequested: models.ConsumableMap{
						"test-type-1": {
							"test-consumable-1": 1,
						},
					},
				}

				testProcessMessage(context.Background(), landlord, stats, map[config.TenantName]tenant.ProcessMessageResp{
					config.TenantName("test-tenant-1"): {
						Error: ll.EmptyConsumableMapError,
					},
				}, test.NewRange(2, 2), req)
			})

			Convey("When the tenant returns a consumable map with processed consumables", func() {
				tenant1.On("ProcessMessage", mock.Anything, mock.Anything).Return(&prism_tenant.ProcessMessageResp{
					Transformations:  []*prism.Transformation{{}, {}},
					MessageTreatment: string(prism_models.DEFAULT_MESSAGE),
					ConsumableAmountsProcessed: map[string]int64{
						"test-consumable-1": 1,
					},
				}, nil)

				Convey("The landlord should return the correct response", func() {
					req := &tenant.ProcessMessageReq{
						UserID:    "1234",
						ChannelID: "5678",
						MessageID: "test-message-id",
						Message:   "test message",
						ConsumablesRequested: models.ConsumableMap{
							"test-type-1": {
								"test-consumable-1": 1,
							},
						},
					}

					testProcessMessage(context.Background(), landlord, stats, map[config.TenantName]tenant.ProcessMessageResp{
						config.TenantName("test-tenant-1"): {
							Error:            nil,
							Transformations:  []*prism.Transformation{{}, {}},
							MessageTreatment: prism_models.DEFAULT_MESSAGE,
						},
					}, test.NewRange(2, 2), req)
				})
			})
		})
	})
}

func TestLandlordProcessMessage_MultipleTenants(t *testing.T) {
	Convey("Given a landlord with a multiple tenants", t, func() {
		stats := new(stats_mock.Statter)
		tenant1 := new(prism_tenant_mock.PrismTenant)
		tenant2 := new(prism_tenant_mock.PrismTenant)
		tenant3 := new(prism_tenant_mock.PrismTenant)
		tenantMapper := new(tenant_mock.Mapper)
		consumableMapper := new(consumable_mock.Mapper)

		landlord := ll.NewTestLandlord(stats, tenantMapper, consumableMapper)
		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		consumableMapper.On("GetTenant", config.ConsumableType("test-type-1")).Return(tenant1, config.TenantName("test-tenant-1"), nil)
		consumableMapper.On("GetTenant", config.ConsumableType("test-type-2")).Return(tenant2, config.TenantName("test-tenant-2"), nil)
		consumableMapper.On("GetTenant", config.ConsumableType("test-type-3")).Return(tenant3, config.TenantName("test-tenant-3"), nil)

		req := &tenant.ProcessMessageReq{
			UserID:    "1234",
			ChannelID: "5678",
			MessageID: "test-message-id",
			Message:   "test message",
			ConsumablesRequested: models.ConsumableMap{
				"test-type-1": {
					"test-consumable-1": 1,
				},
				"test-type-2": {
					"test-consumable-2": 4,
				},
				"test-type-3": {
					"test-consumable-3": 20,
				},
			},
		}

		Convey("When one tenant errors", func() {
			tenant1.On("ProcessMessage", mock.Anything, mock.Anything).Return(&prism_tenant.ProcessMessageResp{
				Transformations:  []*prism.Transformation{{}, {}},
				MessageTreatment: string(prism_models.DEFAULT_MESSAGE),
				ConsumableAmountsProcessed: map[string]int64{
					"test-consumable-1": 1,
				},
			}, nil)
			tenant2.On("ProcessMessage", mock.Anything, mock.Anything).Return(nil, processMessageTestError2)
			tenant3.On("ProcessMessage", mock.Anything, mock.Anything).Return(&prism_tenant.ProcessMessageResp{
				Transformations:  []*prism.Transformation{{}, {}, {}},
				MessageTreatment: string(prism_models.DEFAULT_MESSAGE),
				ConsumableAmountsProcessed: map[string]int64{
					"test-consumable-3": 20,
				},
			}, nil)

			Convey("The landlord should return the correct response", func() {
				testProcessMessage(context.Background(), landlord, stats, map[config.TenantName]tenant.ProcessMessageResp{
					config.TenantName("test-tenant-1"): {
						Error:            nil,
						Transformations:  []*prism.Transformation{{}, {}},
						MessageTreatment: prism_models.DEFAULT_MESSAGE,
					},
					config.TenantName("test-tenant-2"): {
						Error: processMessageTestError2,
					},
					config.TenantName("test-tenant-3"): {
						Error:            nil,
						Transformations:  []*prism.Transformation{{}, {}, {}},
						MessageTreatment: prism_models.DEFAULT_MESSAGE,
					},
				}, test.NewRange(4, 4), req)
			})
		})

		Convey("When all tenants error", func() {
			tenant1.On("ProcessMessage", mock.Anything, mock.Anything).Return(nil, processMessageTestError1)
			tenant2.On("ProcessMessage", mock.Anything, mock.Anything).Return(nil, processMessageTestError2)
			tenant3.On("ProcessMessage", mock.Anything, mock.Anything).Return(nil, processMessageTestError3)

			Convey("The landlord should return the correct response", func() {
				testProcessMessage(context.Background(), landlord, stats, map[config.TenantName]tenant.ProcessMessageResp{
					config.TenantName("test-tenant-1"): {
						Error: processMessageTestError1,
					},
					config.TenantName("test-tenant-2"): {
						Error: processMessageTestError2,
					},
					config.TenantName("test-tenant-3"): {
						Error: processMessageTestError3,
					},
				}, test.NewRange(4, 4), req)
			})
		})

		Convey("When no tenants error", func() {
			tenant1.On("ProcessMessage", mock.Anything, mock.Anything).Return(&prism_tenant.ProcessMessageResp{
				Transformations:  []*prism.Transformation{{}},
				MessageTreatment: string(prism_models.DEFAULT_MESSAGE),
				ConsumableAmountsProcessed: map[string]int64{
					"test-consumable-1": 1,
				},
			}, nil)
			tenant2.On("ProcessMessage", mock.Anything, mock.Anything).Return(&prism_tenant.ProcessMessageResp{
				Transformations:  []*prism.Transformation{{}, {}},
				MessageTreatment: string(prism_models.DEFAULT_MESSAGE),
				ConsumableAmountsProcessed: map[string]int64{
					"test-consumable-2": 4,
				},
			}, nil)
			tenant3.On("ProcessMessage", mock.Anything, mock.Anything).Return(&prism_tenant.ProcessMessageResp{
				Transformations:  []*prism.Transformation{{}, {}},
				MessageTreatment: string(prism_models.DEFAULT_MESSAGE),
				ConsumableAmountsProcessed: map[string]int64{
					"test-consumable-3": 20,
				},
			}, nil)

			Convey("The landlord should return the correct response", func() {
				testProcessMessage(context.Background(), landlord, stats, map[config.TenantName]tenant.ProcessMessageResp{
					config.TenantName("test-tenant-1"): {
						Error:            nil,
						Transformations:  []*prism.Transformation{{}},
						MessageTreatment: prism_models.DEFAULT_MESSAGE,
					},
					config.TenantName("test-tenant-2"): {
						Error:            nil,
						Transformations:  []*prism.Transformation{{}, {}},
						MessageTreatment: prism_models.DEFAULT_MESSAGE,
					},
					config.TenantName("test-tenant-3"): {
						Error:            nil,
						Transformations:  []*prism.Transformation{{}, {}},
						MessageTreatment: prism_models.DEFAULT_MESSAGE,
					},
				}, test.NewRange(4, 4), req)
			})
		})
	})
}

func TestProcessMessage_LateTenant(t *testing.T) {
	Convey("Given a landlord with a late tenant", t, func() {
		stats := new(stats_mock.Statter)
		tenant1 := new(prism_tenant_mock.PrismTenant)
		tenant2 := new(prism_tenant_mock.PrismTenant)
		tenant3 := new(prism_tenant_mock.PrismTenant)
		tenantMapper := new(tenant_mock.Mapper)
		consumableMapper := new(consumable_mock.Mapper)

		landlord := ll.NewTestLandlord(stats, tenantMapper, consumableMapper)
		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		consumableMapper.On("GetTenant", config.ConsumableType("test-type-1")).Return(tenant1, config.TenantName("test-tenant-1"), nil)
		consumableMapper.On("GetTenant", config.ConsumableType("test-type-2")).Return(tenant2, config.TenantName("late-tenant"), nil)
		consumableMapper.On("GetTenant", config.ConsumableType("test-type-3")).Return(tenant3, config.TenantName("test-tenant-3"), nil)

		req := &tenant.ProcessMessageReq{
			UserID:    "1234",
			ChannelID: "5678",
			MessageID: "test-message-id",
			Message:   "test message",
			ConsumablesRequested: models.ConsumableMap{
				"test-type-1": {
					"test-consumable-1": 1,
				},
				"test-type-2": {
					"test-consumable-2": 4,
				},
				"test-type-3": {
					"test-consumable-3": 20,
				},
			},
		}

		Convey("When all other tenants error", func() {
			tenant1.On("ProcessMessage", mock.Anything, mock.Anything).Return(nil, processMessageTestError1)
			tenant2.On("ProcessMessage", mock.Anything, mock.Anything).WaitUntil(nil).Return(&prism_tenant.ProcessMessageResp{
				Transformations:  []*prism.Transformation{{}, {}},
				MessageTreatment: string(prism_models.DEFAULT_MESSAGE),
				ConsumableAmountsProcessed: map[string]int64{
					"test-consumable-2": 4,
				},
			}, nil)
			tenant3.On("ProcessMessage", mock.Anything, mock.Anything).Return(nil, processMessageTestError3)

			Convey("The landlord should still wait on the late tenant", func() {
				testProcessMessage(context.Background(), landlord, stats, map[config.TenantName]tenant.ProcessMessageResp{
					config.TenantName("test-tenant-1"): {
						Error: processMessageTestError1,
					},
					config.TenantName("late-tenant"): {
						Transformations:  []*prism.Transformation{{}, {}},
						MessageTreatment: prism_models.DEFAULT_MESSAGE,
						Error:            nil,
					},
					config.TenantName("test-tenant-3"): {
						Error: processMessageTestError3,
					},
				}, test.NewRange(4, 4), req)
			})
		})

		Convey("When all other tenants succeed", func() {
			tenant1.On("ProcessMessage", mock.Anything, mock.Anything).Return(&prism_tenant.ProcessMessageResp{
				Transformations:  []*prism.Transformation{{}},
				MessageTreatment: string(prism_models.DEFAULT_MESSAGE),
				ConsumableAmountsProcessed: map[string]int64{
					"test-consumable-1": 1,
				},
			}, nil)
			tenant2.On("ProcessMessage", mock.Anything, mock.Anything).WaitUntil(nil).Return(&prism_tenant.ProcessMessageResp{
				Transformations:  []*prism.Transformation{{}, {}},
				MessageTreatment: string(prism_models.DEFAULT_MESSAGE),
				ConsumableAmountsProcessed: map[string]int64{
					"test-consumable-2": 4,
				},
			}, nil)
			tenant3.On("ProcessMessage", mock.Anything, mock.Anything).Return(&prism_tenant.ProcessMessageResp{
				Transformations:  []*prism.Transformation{{}, {}, {}},
				MessageTreatment: string(prism_models.DEFAULT_MESSAGE),
				ConsumableAmountsProcessed: map[string]int64{
					"test-consumable-3": 20,
				},
			}, nil)

			Convey("The landlord waits on the late tenant before succeeding", func() {
				testProcessMessage(context.Background(), landlord, stats, map[config.TenantName]tenant.ProcessMessageResp{
					config.TenantName("test-tenant-1"): {
						Transformations:  []*prism.Transformation{{}},
						MessageTreatment: prism_models.DEFAULT_MESSAGE,
						Error:            nil,
					},
					config.TenantName("late-tenant"): {
						Transformations:  []*prism.Transformation{{}, {}},
						MessageTreatment: prism_models.DEFAULT_MESSAGE,
						Error:            nil,
					},
					config.TenantName("test-tenant-3"): {
						Transformations:  []*prism.Transformation{{}, {}, {}},
						MessageTreatment: prism_models.DEFAULT_MESSAGE,
						Error:            nil,
					},
				}, test.NewRange(4, 4), req)
			})
		})

		Convey("When all other tenants succeed and the context has a timeout", func() {
			tenant1.On("ProcessMessage", mock.Anything, mock.Anything).Return(&prism_tenant.ProcessMessageResp{
				Transformations:  []*prism.Transformation{{}},
				MessageTreatment: string(prism_models.DEFAULT_MESSAGE),
				ConsumableAmountsProcessed: map[string]int64{
					"test-consumable-1": 1,
				},
			}, nil)
			tenant2.On("ProcessMessage", mock.Anything, mock.Anything).WaitUntil(time.After(time.Hour))
			tenant3.On("ProcessMessage", mock.Anything, mock.Anything).Return(&prism_tenant.ProcessMessageResp{
				Transformations:  []*prism.Transformation{{}, {}, {}},
				MessageTreatment: string(prism_models.DEFAULT_MESSAGE),
				ConsumableAmountsProcessed: map[string]int64{
					"test-consumable-3": 20,
				},
			}, nil)
			ctx, cancel := context.WithTimeout(context.Background(), 20*time.Millisecond)

			Convey("The landlord should return a timeout error", func() {
				testProcessMessage(ctx, landlord, stats, map[config.TenantName]tenant.ProcessMessageResp{
					config.TenantName("test-tenant-1"): {
						Transformations:  []*prism.Transformation{{}},
						MessageTreatment: prism_models.DEFAULT_MESSAGE,
						Error:            nil,
					},
					config.TenantName("late-tenant"): {
						Error: ll.TenantTimeoutError,
					},
					config.TenantName("test-tenant-3"): {
						Transformations:  []*prism.Transformation{{}, {}, {}},
						MessageTreatment: prism_models.DEFAULT_MESSAGE,
						Error:            nil,
					},
				}, test.NewRange(3, 4), req)
				cancel()
			})
		})
	})
}
