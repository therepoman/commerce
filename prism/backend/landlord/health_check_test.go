package landlord_test

import (
	"context"
	"errors"
	"testing"
	"time"

	ll "code.justin.tv/commerce/prism/backend/landlord"
	"code.justin.tv/commerce/prism/config"
	stats_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/stats"
	consumable_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/consumable"
	tenant_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/tenant"
	prism_tenant_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/rpc/tenant"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
	"code.justin.tv/commerce/prism/test"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func testHealthChecks(c context.Context, landlord ll.Landlord, stats *stats_mock.Statter, expectedErr error, expectedMetrics test.Range) {
	err := landlord.PerformTenantHealthChecks(c)
	So(err, ShouldEqual, expectedErr)
	test.AssertInRange(test.GetNumCalls(&stats.Mock, "TimingDuration"), expectedMetrics)
	test.AssertInRange(test.GetNumCalls(&stats.Mock, "Inc"), expectedMetrics)
}

func TestLandlordHealthChecks_NoTenants(t *testing.T) {
	Convey("Given a landlord without tenants", t, func() {
		stats := new(stats_mock.Statter)
		tenantMapper := new(tenant_mock.Mapper)
		consumableMapper := new(consumable_mock.Mapper)
		landlord := ll.NewTestLandlord(stats, tenantMapper, consumableMapper)
		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		tenantMapper.On("GetAll").Return(nil)

		Convey("Landlord should never error on health checks", func() {
			testHealthChecks(context.Background(), landlord, stats, nil, test.NewRange(1, 1))
		})
	})
}

func TestLandlordHealthChecks_OneTenant(t *testing.T) {
	Convey("Given a landlord with a single tenant", t, func() {
		stats := new(stats_mock.Statter)
		tenantMapper := new(tenant_mock.Mapper)
		consumableMapper := new(consumable_mock.Mapper)
		tenant1 := new(prism_tenant_mock.PrismTenant)
		landlord := ll.NewTestLandlord(stats, tenantMapper, consumableMapper)
		tMap := map[config.TenantName]prism_tenant.PrismTenant{
			config.TenantName("Tenant1"): tenant1,
		}

		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		tenantMapper.On("GetAll").Return(tMap)

		Convey("When the tenant errors", func() {
			tenant1.On("HealthCheck", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

			Convey("The landlord should error", func() {
				testHealthChecks(context.Background(), landlord, stats, ll.TenantError, test.NewRange(2, 2))
			})
		})

		Convey("When the tenant does not error", func() {
			tenant1.On("HealthCheck", mock.Anything, mock.Anything).Return(nil, nil)

			Convey("The landlord should not error", func() {
				testHealthChecks(context.Background(), landlord, stats, nil, test.NewRange(2, 2))
			})
		})
	})
}

func TestLandlordHealthChecks_MultipleTenants(t *testing.T) {
	Convey("Given a landlord with multiple tenants", t, func() {
		stats := new(stats_mock.Statter)
		tenant1 := new(prism_tenant_mock.PrismTenant)
		tenant2 := new(prism_tenant_mock.PrismTenant)
		tenant3 := new(prism_tenant_mock.PrismTenant)
		tenantMapper := new(tenant_mock.Mapper)
		consumableMapper := new(consumable_mock.Mapper)
		tMap := map[config.TenantName]prism_tenant.PrismTenant{
			config.TenantName("Tenant1"): tenant1,
			config.TenantName("Tenant2"): tenant2,
			config.TenantName("Tenant3"): tenant3,
		}
		landlord := ll.NewTestLandlord(stats, tenantMapper, consumableMapper)
		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		tenantMapper.On("GetAll").Return(tMap)

		Convey("When one tenant errors", func() {
			tenant1.On("HealthCheck", mock.Anything, mock.Anything).Return(nil, nil)
			tenant2.On("HealthCheck", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
			tenant3.On("HealthCheck", mock.Anything, mock.Anything).Return(nil, nil)

			Convey("The landlord should error", func() {
				testHealthChecks(context.Background(), landlord, stats, ll.TenantError, test.NewRange(2, 4))
			})
		})

		Convey("When all tenants error", func() {
			tenant1.On("HealthCheck", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
			tenant2.On("HealthCheck", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
			tenant3.On("HealthCheck", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

			Convey("The landlord should error", func() {
				testHealthChecks(context.Background(), landlord, stats, ll.TenantError, test.NewRange(2, 4))
			})
		})

		Convey("When no tenants error", func() {
			tenant1.On("HealthCheck", mock.Anything, mock.Anything).Return(nil, nil)
			tenant2.On("HealthCheck", mock.Anything, mock.Anything).Return(nil, nil)
			tenant3.On("HealthCheck", mock.Anything, mock.Anything).Return(nil, nil)

			Convey("The landlord should not error", func() {
				testHealthChecks(context.Background(), landlord, stats, nil, test.NewRange(2, 4))
			})
		})
	})
}

func TestLandlordHealthChecks_LateTenant(t *testing.T) {
	Convey("Given a landlord with a late tenant", t, func() {
		stats := new(stats_mock.Statter)
		tenant1 := new(prism_tenant_mock.PrismTenant)
		tenant2 := new(prism_tenant_mock.PrismTenant)
		tenant3 := new(prism_tenant_mock.PrismTenant)
		tenantMapper := new(tenant_mock.Mapper)
		consumableMapper := new(consumable_mock.Mapper)
		tMap := map[config.TenantName]prism_tenant.PrismTenant{
			config.TenantName("Tenant1"): tenant1,
			config.TenantName("Tenant2"): tenant2,
			config.TenantName("Tenant3"): tenant3,
		}

		landlord := ll.NewTestLandlord(stats, tenantMapper, consumableMapper)
		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		tenantMapper.On("GetAll").Return(tMap)

		Convey("When one other tenant errors", func() {
			tenant1.On("HealthCheck", mock.Anything, mock.Anything).Return(nil, nil)
			tenant2.On("HealthCheck", mock.Anything, mock.Anything).WaitUntil(time.After(time.Hour)).Return(nil, nil)
			tenant3.On("HealthCheck", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

			Convey("The landlord should error without waiting on the late tenant", func() {
				testHealthChecks(context.Background(), landlord, stats, ll.TenantError, test.NewRange(2, 3))
			})
		})

		Convey("When all other tenants succeed", func() {
			tenant1.On("HealthCheck", mock.Anything, mock.Anything).Return(nil, nil)
			tenant2.On("HealthCheck", mock.Anything, mock.Anything).WaitUntil(time.After(time.Second)).Return(nil, nil)
			tenant3.On("HealthCheck", mock.Anything, mock.Anything).Return(nil, nil)

			Convey("The landlord waits on the late tenant before succeeding", func() {
				testHealthChecks(context.Background(), landlord, stats, nil, test.NewRange(4, 4))
			})
		})

		Convey("When all other tenants succeed and the context has a timeout", func() {
			tenant1.On("HealthCheck", mock.Anything, mock.Anything).Return(nil, nil)
			tenant2.On("HealthCheck", mock.Anything, mock.Anything).WaitUntil(time.After(time.Hour)).Return(nil, nil)
			tenant3.On("HealthCheck", mock.Anything, mock.Anything).Return(nil, nil)
			ctx, cancel := context.WithTimeout(context.Background(), 20*time.Millisecond)

			Convey("The landlord should return a timeout error", func() {
				testHealthChecks(ctx, landlord, stats, ll.LandlordTimeoutError, test.NewRange(3, 3))
				cancel()
			})
		})
	})
}
