package landlord

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/backend/tenant"
	"code.justin.tv/commerce/prism/backend/utils"
	"code.justin.tv/commerce/prism/backend/utils/client"
	"code.justin.tv/commerce/prism/config"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
)

const (
	EmptyConsumableMapMsg = "tenant returned nil or empty consumable map"
)

var EmptyConsumableMapError = errors.New(EmptyConsumableMapMsg)

func processMessageLatencyKey(tenant string) string {
	return fmt.Sprintf("%s_process_message_latency", tenant)
}

func processMessageErrorsKey(tenant string) string {
	return fmt.Sprintf("%s_process_message_errors", tenant)
}

func anyProcessMessageErr(respMap map[config.TenantName]tenant.ProcessMessageResp) int64 {
	for _, resp := range respMap {
		if resp.Error != nil {
			return 1
		}
	}
	return 0
}

func tenantProcessMessageResponseCount(respMap *sync.Map) int {
	responded := 0
	respMap.Range(func(key, value interface{}) bool {
		tenantRespValue, ok := value.(tenant.ProcessMessageResp)
		if !ok {
			log.Error("error converting tenant resp value key to ProcessMessageResp")
			return true
		}

		if tenantRespValue.Error != TenantTimeoutError {
			responded++
		}
		return true
	})
	return responded
}

func (l *landlord) ProcessMessage(ctx context.Context, req *tenant.ProcessMessageReq) (respMap map[config.TenantName]tenant.ProcessMessageResp) {
	startTime := time.Now()
	defer func() {
		latency := time.Since(startTime)

		l.Stats.TimingDuration(processMessageLatencyKey("landlord"), latency, 1)

		l.Stats.Inc(processMessageErrorsKey("landlord"), anyProcessMessageErr(respMap), 1)
	}()

	var tenantClients = make(map[config.TenantName]prism_tenant.PrismTenant)
	var tenantConsumables = make(map[config.TenantName]map[models.Consumable]int64)

	for consumableType, consumables := range req.ConsumablesRequested {
		tenantClient, tenantName, err := l.ConsumableMapper.GetTenant(consumableType)
		if err != nil {
			log.WithError(err).WithField("consumable_type", consumableType).Error("error getting tenant from consumable type")
			return make(map[config.TenantName]tenant.ProcessMessageResp)
		}

		tenantClients[tenantName] = tenantClient
		tenantConsumables[tenantName] = consumables
	}

	if len(tenantClients) < 1 {
		log.WithField("request", req).Error("no tenants processed during landlord processing")
		return make(map[config.TenantName]tenant.ProcessMessageResp)
	}

	tenantResps := new(sync.Map)
	processMessageRespChan := make(chan NamedProcessMessageResp)
	for name, t := range tenantClients {
		go func(t prism_tenant.PrismTenant, name config.TenantName) {
			tenantStartTime := time.Now()
			ctx := client.WithTwitchClientIDHeader(ctx)

			tenantRequest := &prism_tenant.ProcessMessageReq{
				Message:           req.Message,
				Anonymous:         req.Anonymous,
				ChannelId:         req.ChannelID,
				ConsumableAmounts: models.ToRawConsumables(tenantConsumables[name]),
				MessageId:         req.MessageID,
				UserId:            req.UserID,
				RoomId:            req.RoomID,
				GeoIpMetadata:     &req.GeoIPMetadata,
			}
			tenantResp, err := t.ProcessMessage(ctx, tenantRequest)
			tenantLatency := time.Since(tenantStartTime)

			l.Stats.TimingDuration(processMessageLatencyKey(string(name)), tenantLatency, 1)

			l.Stats.Inc(processMessageErrorsKey(string(name)), errorCount(err), 1)

			var tenantResponse tenant.ProcessMessageResp
			if err != nil {
				tenantResponse = tenant.ProcessMessageResp{
					Error: err,
				}
			} else if tenantResp.ConsumableAmountsProcessed == nil || len(tenantResp.ConsumableAmountsProcessed) == 0 {
				log.WithField("tenantName", name).Error(EmptyConsumableMapMsg)
				tenantResponse = tenant.ProcessMessageResp{
					Error: EmptyConsumableMapError,
				}
			} else {
				tenantResponse = tenant.ProcessMessageResp{
					MessageTreatment:     prism_models.MessageTreatment(tenantResp.MessageTreatment),
					ConsumablesProcessed: models.ToConsumableMap(tenantResp.ConsumableAmountsProcessed),
					Transformations:      tenantResp.Transformations,
					NewBalance:           tenantResp.NewBalance,
				}
			}

			processMessageRespChan <- NamedProcessMessageResp{
				TenantName: name,
				TenantResp: tenantResponse,
			}
		}(t, name)

		// Prepopulate the map in case of tenant time-outs
		tenantResps.Store(name, tenant.ProcessMessageResp{
			Error: TenantTimeoutError,
		})
	}

TenantWaitLoop:
	for {
		select {
		case processMessageResp := <-processMessageRespChan:
			tenantResps.Store(processMessageResp.TenantName, processMessageResp.TenantResp)
			if processMessageResp.TenantResp.Error != nil {
				twErr := utils.ToTwirpError(processMessageResp.TenantResp.Error)

				if utils.IsA5XXError(twErr) {
					log.WithError(processMessageResp.TenantResp.Error).WithField("tenant", processMessageResp.TenantName).Error("5xx error calling tenant process message")
				} else {
					log.WithError(processMessageResp.TenantResp.Error).WithField("tenant", processMessageResp.TenantName).Warn("4xx error calling tenant process message")
				}
			}

			if tenantProcessMessageResponseCount(tenantResps) >= len(tenantClients) {
				break TenantWaitLoop
			}
		case <-ctx.Done():
			log.Error("timed out calling process message for all tenants")
			break TenantWaitLoop
		}
	}

	tenantResponseMap := make(map[config.TenantName]tenant.ProcessMessageResp)
	tenantResps.Range(func(key, value interface{}) bool {
		tenantRespKey, ok := key.(config.TenantName)
		if !ok {
			log.WithField("tenantName", key).Error("error converting tenant resp key to TenantName")
			return true
		}

		tenantRespValue, ok := value.(tenant.ProcessMessageResp)
		if !ok {
			log.Error("error converting tenant resp value key to ProcessMessageResp")
			return true
		}

		tenantResponseMap[tenantRespKey] = tenantRespValue
		return true
	})
	return tenantResponseMap
}
