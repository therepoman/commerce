package automod

import (
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	redis_client "code.justin.tv/commerce/prism/backend/clients/redis"
	prism "code.justin.tv/commerce/prism/rpc"
	"github.com/go-redis/redis"
)

const (
	automodKeyFormat = "automod-%s"
	automodCacheTTL  = 5 * time.Minute
)

// CacheValue is the value stored in the AutoMod cache
type CacheValue struct {
	ProcessMessageReq prism.ProcessMessageReq `json:"process_message_req"`
	MessageTokens     []string                `json:"message_tokens"`
}

// Cache represents an AutoMod cache
type Cache interface {
	Get(userID string) *CacheValue
	Set(userID string, val CacheValue) error
	Delete(userID string) error
}

type cache struct {
	RedisClient redis_client.Client `inject:""`
}

// NewCache creates a new AutoMod Cache
func NewCache() Cache {
	return &cache{}
}

// Get ...
func (ac *cache) Get(userID string) *CacheValue {
	automodCacheValueStr, err := ac.RedisClient.Get(fmt.Sprintf(automodKeyFormat, userID))
	if err != nil {
		if err != redis.Nil {
			// Actual redis failure, not a "key does not exist" error
			log.WithError(err).WithField("userID", userID).Error("Error getting AutoMod cache value")
		}
		return nil
	}

	if strings.Blank(automodCacheValueStr) {
		return nil
	}

	var automodCacheValue CacheValue
	err = json.Unmarshal([]byte(automodCacheValueStr), &automodCacheValue)
	if err != nil {
		delErr := ac.Delete(userID)
		if delErr != nil {
			log.WithError(delErr).WithField("userID", userID).Error("Error deleting AutoMod record")
		}
		log.WithError(err).WithField("userID", userID).Error("Error unmarshaling AutoMod cache value")
		return nil
	}

	return &automodCacheValue
}

// Set ...
func (ac *cache) Set(userID string, val CacheValue) error {
	automodCacheValueJSON, err := json.Marshal(val)
	if err != nil {
		log.WithError(err).WithField("userID", userID).Error("Error marshaling AutoMod cache value")
		return err
	}

	err = ac.RedisClient.Set(fmt.Sprintf(automodKeyFormat, userID), string(automodCacheValueJSON), automodCacheTTL)
	if err != nil {
		log.WithError(err).WithField("userID", userID).Error("Error setting AutoMod cache value in cache")
	}

	return err
}

// Delete ...
func (ac *cache) Delete(userID string) error {
	err := ac.RedisClient.Delete(fmt.Sprintf(automodKeyFormat, userID))
	if err != nil {
		log.WithError(err).WithField("userID", userID).Error("Error deleting AutoMod record")
	}

	return err
}
