package usersservice

import (
	"errors"
	"testing"

	redis_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/redis"
	"github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestCache_Get(t *testing.T) {
	Convey("Given an AutoModCache", t, func() {
		userID := "131495971"

		redisClient := new(redis_mock.Client)

		usersServiceCache := &cache{
			RedisClient: redisClient,
		}

		Convey("When the RedisClient fails", func() {
			redisClient.On("Get", mock.Anything).Return("", errors.New("test error"))

			res := usersServiceCache.Get(userID)
			So(res, ShouldBeNil)
		})

		Convey("When the key is not found in Redis", func() {
			redisClient.On("Get", mock.Anything).Return("", redis.Nil)

			res := usersServiceCache.Get(userID)
			So(res, ShouldBeNil)
		})

		Convey("When an empty string is returned from Redis", func() {
			redisClient.On("Get", mock.Anything).Return("", nil)

			res := usersServiceCache.Get(userID)
			So(res, ShouldBeNil)
		})

		Convey("When a non-empty string is returned from Redis", func() {
			redisClient.On("Get", mock.Anything).Return("{\"id\":\"131495971\"}", nil)

			res := usersServiceCache.Get(userID)
			So(res, ShouldNotBeNil)
		})
	})
}
