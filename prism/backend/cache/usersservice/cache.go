package usersservice

import (
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	redis_client "code.justin.tv/commerce/prism/backend/clients/redis"
	"code.justin.tv/web/users-service/models"
	"github.com/go-redis/redis"
)

const (
	usersServiceCacheKeyFormat = "user-service-user-%s"
	usersServiceCacheTTL       = time.Minute * 5
)

// Cache respresents a Users Service cache
type Cache interface {
	Get(userID string) *models.Properties
	Set(userID string, val *models.Properties) error
	Delete(userID string) error
}

// NewCache creates a new Users Service Cache
func NewCache() Cache {
	return &cache{}
}

type cache struct {
	RedisClient redis_client.Client `inject:""`
}

// Get ...
func (c *cache) Get(userID string) *models.Properties {
	cacheValueStr, err := c.RedisClient.Get(fmt.Sprintf(usersServiceCacheKeyFormat, userID))
	if err != nil {
		if err != redis.Nil {
			// Actual redis failure, not a "key does not exist" error
			log.WithError(err).WithField("userID", userID).Error("Error getting users service user out of cache")
		}
		return nil
	}

	if strings.Blank(cacheValueStr) {
		return nil
	}

	var userProps models.Properties
	err = json.Unmarshal([]byte(cacheValueStr), &userProps)
	if err != nil {
		delErr := c.Delete(userID)
		if delErr != nil {
			log.WithError(delErr).WithField("userID", userID).Error("Error deleting users service user from cache")
		}
		log.WithError(err).WithField("userID", userID).Error("Error unmarshaling users service user cache value")
		return nil
	}

	return &userProps
}

// Set ...
func (c *cache) Set(userID string, userProps *models.Properties) error {
	userPropsJSON, err := json.Marshal(userProps)
	if err != nil {
		log.WithError(err).WithField("userID", userID).Error("Error marshaling users service user cache value")
		return err
	}

	err = c.RedisClient.Set(fmt.Sprintf(usersServiceCacheKeyFormat, userID), string(userPropsJSON), usersServiceCacheTTL)
	if err != nil {
		log.WithError(err).WithField("userID", userID).Error("Error setting users service user in cache")
	}

	return err
}

// Delete ...
func (c *cache) Delete(userID string) error {
	err := c.RedisClient.Delete(fmt.Sprintf(usersServiceCacheKeyFormat, userID))
	if err != nil {
		log.WithError(err).WithField("userID", userID).Error("Error deleting users service user from cache")
	}

	return err
}
