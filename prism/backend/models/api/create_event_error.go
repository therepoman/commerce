package api

import (
	zumaapi "code.justin.tv/chat/zuma/app/api"
)

const (
	AnonymousMessageInvalidError = "Anonymous message is invalid"
	AutoModError                 = "Message has been flagged by automod"
	AutoModPendingMessageError   = "User has a pending automod message"
	BadMessage                   = "Message is not valid"
	ChannelBlockedTermsError     = "Message contains channel blocked terms"
	RequestThrottledError        = "User message request throttled"
	MessageFilteredError         = "Message was filtered"
	UserBannedInChannelError     = "User is banned or timed out in channel"
	UserSuspendedError           = "User is suspended"

	AnonymousMessageInvalid CreateEventErrorStatus = "anonymous_message_invalid"
	AutoModMessage          CreateEventErrorStatus = "automod_message"
	AutoModPending          CreateEventErrorStatus = "automod_pending_cheer"
	ChannelBlockedTerms     CreateEventErrorStatus = "channel_blocked_terms"
	UserBanned              CreateEventErrorStatus = "user_banned"
	UserSuspended           CreateEventErrorStatus = "user_suspended"
	ZalgoMessage            CreateEventErrorStatus = "zalgo_message"
)

type CreateEventErrorData struct {
	Automod                *zumaapi.AutoModResponse        `json:"automod,omitempty"`
	EnforceMessageResponse *zumaapi.EnforceMessageResponse `json:"zuma,omitempty"`
}

type CreateEventErrorStatus string

type CreateEventErrorResponse struct {
	Status  CreateEventErrorStatus `json:"status"`
	Message string                 `json:"message"`
	Data    CreateEventErrorData   `json:"data"`
}
