package models

import (
	"strings"

	"code.justin.tv/commerce/prism/config"
)

const (
	delimiter = "."
)

type Consumable string

func (c Consumable) String() string {
	return string(c)
}

type ConsumableMap map[config.ConsumableType]map[Consumable]int64

func (m ConsumableMap) Add(consumableType config.ConsumableType, consumable Consumable, amount int64) {
	if _, ok := m[consumableType]; !ok {
		m[consumableType] = make(map[Consumable]int64)
	}
	m[consumableType][consumable] += amount
}

func (m ConsumableMap) AddRaw(rawConsumable string, amount int64) {
	consumableType, consumable := DecodeRawConsumable(rawConsumable)
	m.Add(consumableType, consumable, amount)
}

func (m ConsumableMap) ToRawConsumables() map[string]int64 {
	rawConsumables := make(map[string]int64)
	for _, consumablesOfType := range m {
		for consumable, amount := range consumablesOfType {
			rawConsumables[consumable.String()] += amount
		}
	}
	return rawConsumables
}

func (m ConsumableMap) Flatten() map[Consumable]int64 {
	flattenedConsumables := make(map[Consumable]int64)
	for _, consumablesOfType := range m {
		for consumable, amount := range consumablesOfType {
			flattenedConsumables[consumable] += amount
		}
	}
	return flattenedConsumables
}

func ToRawConsumables(consumables map[Consumable]int64) map[string]int64 {
	rawConsumables := make(map[string]int64)
	for consumable, amount := range consumables {
		rawConsumables[consumable.String()] += amount
	}
	return rawConsumables
}

func DecodeRawConsumable(rawConsumable string) (config.ConsumableType, Consumable) {
	parts := strings.Split(rawConsumable, delimiter)
	return config.ConsumableType(parts[0]), Consumable(rawConsumable)
}

func ToConsumableMap(rawConsumables map[string]int64) ConsumableMap {
	consumableMap := make(ConsumableMap)
	for rawConsumable, amount := range rawConsumables {
		consumableType, consumable := DecodeRawConsumable(rawConsumable)
		consumableMap.Add(consumableType, consumable, amount)
	}
	return consumableMap
}
