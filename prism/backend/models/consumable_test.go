package models

import (
	"testing"

	"code.justin.tv/commerce/prism/config"
	. "github.com/smartystreets/goconvey/convey"
)

func TestDecodeRawConsumable(t *testing.T) {
	Convey("Test decode raw consumable", t, func() {

		Convey("Test consumable without a delimiter", func() {
			ct, c := DecodeRawConsumable("bits")
			So(ct, ShouldEqual, config.ConsumableType("bits"))
			So(c, ShouldEqual, Consumable("bits"))
		})

		Convey("Test consumable with a delimiter", func() {
			ct, c := DecodeRawConsumable("volds.123")
			So(ct, ShouldEqual, config.ConsumableType("volds"))
			So(c, ShouldEqual, Consumable("volds.123"))
		})
	})
}

func TestToRawConsumables(t *testing.T) {
	Convey("Test to raw consumables", t, func() {

		rawConsumables := ConsumableMap{
			config.ConsumableType("volds"): {
				Consumable("volds.123"): 5,
				Consumable("volds.456"): 10,
			},
			config.ConsumableType("bits"): {
				Consumable("bits"): 100,
			},
		}.ToRawConsumables()

		So(rawConsumables["volds.123"], ShouldEqual, 5)
		So(rawConsumables["volds.456"], ShouldEqual, 10)
		So(rawConsumables["bits"], ShouldEqual, 100)
	})
}
