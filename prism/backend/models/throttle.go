package models

import "time"

const (
	ProcessMessageRequestThrottle = 1 * time.Second
	ProcessMessageRequestFormat   = "%s.process_message"

	ThrottleConfigDisabled = "disabled"
)
