package models

const (
	BitsCheerZeroEventName                 = "bits_cheer_zero"
	BitsAutoModFlaggedMessageEventName     = "bits_automod_flagged_message"
	BitsAutoModMessageEnforcementEventName = "bits_automod_message_enforcement"
)

type AutoModOutcome string

const (
	AutoModApproved = AutoModOutcome("approved")
	AutoModDenied   = AutoModOutcome("denied")
	AutoModTimedout = AutoModOutcome("timedout")
)

type DataScienceCommonProperties struct {
	ServerTime int64  `json:"server_time"`
	ClientID   string `json:"client_id"`
	Platform   string `json:"platform"`
}

type BitsCheerZeroEvent struct {
	TransactionID string `json:"transaction_id"`
	UserID        string `json:"user_id"`
	ChannelID     string `json:"channel_id"`
	Message       string `json:"message"`
	DataScienceCommonProperties
}

type BitsAutoModFlaggedMessageEventProperties struct {
	TransactionID string `json:"transaction_id"`
	UserID        string `json:"user_id"`
	UserLogin     string `json:"user_login"`
	ChannelID     string `json:"channel_id"`
	ChannelLogin  string `json:"channel_login"`
	UsedTotal     int    `json:"used_total"`
	Message       string `json:"message"`
	AutoModReason string `json:"automod_reason"`
	DataScienceCommonProperties
}

//Based on https://git-aws.internal.justin.tv/chat/tmi/blob/4e2b02dfb86a76e1090358b2173a70e6d28963aa/clue/logic/privmsg_tracking.go#L144-L164
//and https://git-aws.internal.justin.tv/chat/tmi/blob/86169095f61b7041dbb84beb4dff6275ced8dde6/clue/logic/automod_approve.go#L179-L190
type BitsAutoModMessageEnforcementEventProperties struct {
	ActionType        AutoModOutcome `json:"action_type"`
	Body              string         `json:"body"`
	CanonicalClientID string         `json:"canonical_client_id"`
	Channel           string         `json:"channel"`
	ChannelID         string         `json:"channel_id"`
	EnforcementReason string         `json:"enforcement_reason"`
	MessageID         string         `json:"msg_id"`
	RoomID            string         `json:"room_id"`
	UserLogin         string         `json:"login"`
	UserID            string         `json:"user_id"`
	DataScienceCommonProperties
}
