package consumable

import (
	"errors"

	string_utils "code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/backend/tenant"
	"code.justin.tv/commerce/prism/config"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	prism_tenant "code.justin.tv/commerce/prism/rpc/tenant"
)

const (
	Tenant_Payday            = config.TenantName("payday")
	ConsumableType_Bits      = config.ConsumableType(prism_models.ConsumableType_Bits)
	Consumable_Bits          = models.Consumable(prism_models.Consumable_Bits)
	ConsumableType_Widemotes = config.ConsumableType(prism_models.ConsumableType_Widemotes)
)

// Mapper represents a consumable / consumable type to tenant mapper
type Mapper interface {
	GetTenant(consumableType config.ConsumableType) (prism_tenant.PrismTenant, config.TenantName, error)
	Validate(rawConsumables map[string]int64) (models.ConsumableMap, error)
}

type mapper struct {
	Config       *config.Config `inject:""`
	TenantMapper tenant.Mapper  `inject:""`
}

// NewMapper creates a new Mapper
func NewMapper() Mapper {
	return &mapper{}
}

// NewTestMapper creates a new test Mapper
func NewTestMapper(cfg *config.Config, tenantMapper tenant.Mapper) Mapper {
	return &mapper{
		Config:       cfg,
		TenantMapper: tenantMapper,
	}
}

func (m *mapper) GetTenant(consumableType config.ConsumableType) (prism_tenant.PrismTenant, config.TenantName, error) {
	ctCfg, ok := m.Config.ConsumableTypes[consumableType]
	if !ok {
		return nil, "", errors.New("no config for consumable type")
	}

	if string_utils.Blank(ctCfg.TenantName.String()) {
		return nil, "", errors.New("consumable type not configured with a tenant name")
	}

	t, err := m.TenantMapper.Get(ctCfg.TenantName)
	if err != nil {
		return nil, ctCfg.TenantName, err
	}

	return t, ctCfg.TenantName, nil
}

func (m *mapper) Validate(rawConsumables map[string]int64) (models.ConsumableMap, error) {
	if len(rawConsumables) == 0 {
		return nil, errors.New("no consumables requested")
	}

	consumables := make(models.ConsumableMap)
	for rawConsumable, amount := range rawConsumables {
		consumableType, consumable := models.DecodeRawConsumable(rawConsumable)

		consumableTypeConfig, ok := m.Config.ConsumableTypes[consumableType]
		if !ok {
			return nil, errors.New("requested consumable type is invalid")
		}

		switch consumableTypeConfig.BalanceScope {
		case config.BalanceScope_Global:
			if consumableType.String() != consumable.String() {
				return nil, errors.New("requested a global consumable with a non-global scope")
			}
		case config.BalanceScope_Channel:
			//TODO: Implement channel specific consumable validation

		case config.BalanceScope_Emote:
			//TODO: Implement emote-specific consumable validation

		default:
			log.WithField("consumable_type", consumableType).Error("consumable type configured with invalid balance")
			return nil, errors.New("consumable type configured with invalid balance")
		}

		consumables.Add(consumableType, consumable, amount)
	}

	return consumables, nil
}
