package consumable

import (
	"errors"
	"testing"

	"code.justin.tv/commerce/prism/config"
	tenant_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/tenant"
	. "github.com/smartystreets/goconvey/convey"
)

func TestConsumableMapper_Validate(t *testing.T) {
	Convey("Given a consumable mapper", t, func() {
		cfg, err := config.LoadConfig(config.UnitTest)
		So(err, ShouldBeNil)
		tenantMapper := new(tenant_mock.Mapper)
		mapper := NewTestMapper(cfg, tenantMapper)

		Convey("When requested consumables is empty", func() {
			_, err := mapper.Validate(map[string]int64{})
			So(err, ShouldNotBeNil)
		})

		Convey("When there is a consumable requested that isn't in the config", func() {
			_, err := mapper.Validate(map[string]int64{
				"unconfigured-consumable": 1,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("When there is a consumable that is configured as globally scoped", func() {
			Convey("and the request asks for a non-global scope", func() {
				_, err := mapper.Validate(map[string]int64{
					"bits.1234": 1,
				})
				So(err, ShouldNotBeNil)
			})

			Convey("and the request asks for a global scope", func() {
				resp, err := mapper.Validate(map[string]int64{
					"bits": 1,
				})
				So(err, ShouldBeNil)

				bitsAmount, hasBits := resp[ConsumableType_Bits][Consumable_Bits]
				So(hasBits, ShouldBeTrue)
				So(bitsAmount, ShouldEqual, 1)
			})
		})
	})
}

func TestConsumableMapper_GetTenant(t *testing.T) {
	Convey("Given a consumable mapper", t, func() {
		cfg, err := config.LoadConfig(config.UnitTest)
		So(err, ShouldBeNil)
		tenantMapper := new(tenant_mock.Mapper)
		mapper := NewTestMapper(cfg, tenantMapper)

		Convey("Errors for an unknown consumable type", func() {
			_, _, err := mapper.GetTenant(config.ConsumableType("unknown-type"))
			So(err, ShouldNotBeNil)
		})

		Convey("Errors for a consumable type without a tenant", func() {
			_, _, err := mapper.GetTenant(config.ConsumableType("no-tenant"))
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when tenant mapper errors", func() {
			tenantMapper.On("Get", config.TenantName("payday")).Return(nil, errors.New("test error"))
			_, _, err := mapper.GetTenant(config.ConsumableType("bits"))
			So(err, ShouldNotBeNil)
		})

		Convey("Succeeds when tenant mapper succeeds", func() {
			tenantMapper.On("Get", config.TenantName("payday")).Return(nil, nil)
			_, _, err := mapper.GetTenant(config.ConsumableType("bits"))
			So(err, ShouldBeNil)
		})
	})
}
