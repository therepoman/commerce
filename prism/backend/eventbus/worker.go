package eventbusworker

import (
	"context"

	"code.justin.tv/commerce/prism/backend/utils"
	prism "code.justin.tv/commerce/prism/rpc"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/schema/pkg/automod_chat_message_caught"
)

// EventBusWorker is worker that works on messages from channel mod actions SQS
type EventBusWorker struct {
	prism prism.Prism
}

// AddAutoModEventWorkerToMux creates a new eventbus worker
func AddAutoModEventWorkerToMux(prism prism.Prism, eventbusMux *eventbus.Mux) {
	worker := &EventBusWorker{
		prism: prism,
	}

	automod_chat_message_caught.RegisterUpdateHandler(eventbusMux, worker.handleUpdateCaughtMessage)
}

func (w *EventBusWorker) handleUpdateCaughtMessage(ctx context.Context, h *eventbus.Header, event *automod_chat_message_caught.Update) error {
	// only process events that involve bits
	if event == nil || event.BitsAmount <= 0 {
		return nil
	}
	var processingErr error
	switch event.Status {
	case automod_chat_message_caught.AutomodCaughtMessageStatus_AUTOMOD_CAUGHT_MESSAGE_STATUS_APPROVED:
		_, processingErr = w.prism.ApproveAutoModMessage(ctx, &prism.ApproveAutoModMessageReq{
			UserId:       event.UserId,
			TargetUserId: event.Sender.UserId,
			MessageId:    event.MessageId,
		})
	case automod_chat_message_caught.AutomodCaughtMessageStatus_AUTOMOD_CAUGHT_MESSAGE_STATUS_DENIED:
		_, processingErr = w.prism.DenyAutoModMessage(ctx, &prism.DenyAutoModMessageReq{
			UserId:       event.UserId,
			TargetUserId: event.Sender.UserId,
			MessageId:    event.MessageId,
		})
	default:
		return nil
	}
	// only error and fill in the dlq on 5xx errors
	if processingErr != nil && !utils.IsTwirpError(processingErr) {
		return processingErr
	}
	return nil
}
