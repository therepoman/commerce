package server

import (
	"context"

	"code.justin.tv/commerce/prism/backend/api/approve_automod_message"
	"code.justin.tv/commerce/prism/backend/api/deny_automod_message"
	"code.justin.tv/commerce/prism/backend/api/process_message"
	prism "code.justin.tv/commerce/prism/rpc"
)

type server struct {
	ProcessMessageAPI        *process_message.API         `inject:""`
	ApproveAutoModMessageAPI *approve_automod_message.API `inject:""`
	DenyAutoModMessageAPI    *deny_automod_message.API    `inject:""`
}

// NewServer creates a new Prism server
func NewServer() prism.Prism {
	return &server{}
}

func (s *server) HealthCheck(ctx context.Context, req *prism.HealthCheckReq) (*prism.HealthCheckResp, error) {
	return &prism.HealthCheckResp{}, nil
}

func (s *server) ProcessMessage(ctx context.Context, req *prism.ProcessMessageReq) (*prism.ProcessMessageResp, error) {
	return s.ProcessMessageAPI.ProcessMessage(ctx, req)
}

func (s *server) ApproveAutoModMessage(ctx context.Context, req *prism.ApproveAutoModMessageReq) (*prism.ApproveAutoModMessageResp, error) {
	return s.ApproveAutoModMessageAPI.ApproveAutoModMessage(ctx, req)
}

func (s *server) DenyAutoModMessage(ctx context.Context, req *prism.DenyAutoModMessageReq) (*prism.DenyAutoModMessageResp, error) {
	return s.DenyAutoModMessageAPI.DenyAutoModMessage(ctx, req)
}
