package middleware

import (
	"context"
	"net/http"
	"strings"

	"code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/config"
)

const (
	ThrottleConfigContextKey            = contextKey("throttle-config")
	IntegrationTestThrottleConfigHeader = "Integration-Test-Throttle-Config"
)

// NewIntegrationTestMiddleware ...
func NewIntegrationTestMiddleware(config *config.Config) func(h http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r != nil {
				if !config.IsProd() {
					throttleConfig := r.Header.Get(IntegrationTestThrottleConfigHeader)

					if strings.EqualFold(throttleConfig, models.ThrottleConfigDisabled) {
						r = r.WithContext(context.WithValue(r.Context(), ThrottleConfigContextKey, models.ThrottleConfigDisabled))
					}
				}
			}
			h.ServeHTTP(w, r)
		})
	}
}
