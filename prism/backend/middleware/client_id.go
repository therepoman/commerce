package middleware

import (
	"context"
	"net/http"

	"code.justin.tv/foundation/twitchclient"
)

const (
	clientIDContextKey = contextKey("client-id-context-key")
)

// NewClientIDMiddleware ...
func NewClientIDMiddleware() func(h http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r != nil {
				r = r.WithContext(context.WithValue(r.Context(), clientIDContextKey, r.Header.Get(twitchclient.TwitchClientIDHeader)))
			}
			h.ServeHTTP(w, r)
		})
	}
}

// GetClientID ...
func GetClientID(ctx context.Context) string {
	tmp := ctx.Value(clientIDContextKey)

	if tmp == nil {
		return ""
	}

	clientID, ok := tmp.(string)
	if !ok {
		return ""
	}

	return clientID
}
