package datascience

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/prism/backend/models"
	spade_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/common/spade-client-go/spade"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestDataScienceTracker_TrackEvent(t *testing.T) {
	Convey("Given a DataScienceTracker", t, func() {
		userID := "131495971"
		channelID := "116076154"
		eventName := "someevent"

		dataScienceEvent := &models.BitsAutoModFlaggedMessageEventProperties{}

		spadeClient := new(spade_mock.Client)

		dataScienceTracker := &tracker{
			SpadeClient: spadeClient,
		}

		Convey("When the user is a test user", func() {
			userID = "735700000000000001"

			err := dataScienceTracker.TrackEvent(context.Background(), eventName, dataScienceEvent, userID, channelID)
			So(err, ShouldBeNil)
			spadeClient.AssertNumberOfCalls(t, "TrackEvent", 0)
		})

		Convey("When the channel is a test user", func() {
			channelID = "735700000000000001"

			err := dataScienceTracker.TrackEvent(context.Background(), eventName, dataScienceEvent, userID, channelID)
			So(err, ShouldBeNil)
			spadeClient.AssertNumberOfCalls(t, "TrackEvent", 0)
		})

		Convey("When the spade client fails", func() {
			spadeClient.On("TrackEvent", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test error"))

			err := dataScienceTracker.TrackEvent(context.Background(), eventName, dataScienceEvent, userID, channelID)
			So(err, ShouldNotBeNil)
			spadeClient.AssertNumberOfCalls(t, "TrackEvent", 1)
		})

		Convey("When the spade client succeeds", func() {
			spadeClient.On("TrackEvent", mock.Anything, mock.Anything, mock.Anything).Return(nil)

			err := dataScienceTracker.TrackEvent(context.Background(), eventName, dataScienceEvent, userID, channelID)
			So(err, ShouldBeNil)
			spadeClient.AssertNumberOfCalls(t, "TrackEvent", 1)
		})
	})
}
