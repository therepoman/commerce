package datascience

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend/middleware"
	"code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/backend/usersservice"
	"code.justin.tv/commerce/prism/backend/utils"
	"code.justin.tv/commerce/prism/backend/utils/client"
	"code.justin.tv/common/spade-client-go/spade"
)

// Tracker represents a data science tracker
type Tracker interface {
	TrackEvent(ctx context.Context, eventName string, eventProperties interface{}, userID string, channelID string) error
	GetDataScienceCommonProperties(ctx context.Context) models.DataScienceCommonProperties
	GetUserLoginForDataScience(ctx context.Context, userID string) string
}

type tracker struct {
	SpadeClient spade.Client         `inject:""`
	UserFetcher usersservice.Fetcher `inject:""`
}

// NewTracker creates a new data science tracker
func NewTracker() Tracker {
	return &tracker{}
}

func (ds *tracker) TrackEvent(ctx context.Context, eventName string, eventProperties interface{}, userID string, channelID string) error {
	if !ds.isDataScienceEnabled(userID) || !ds.isDataScienceEnabled(channelID) {
		return nil
	}

	err := ds.SpadeClient.TrackEvent(ctx, eventName, eventProperties)
	return err
}

func (ds *tracker) isDataScienceEnabled(userID string) bool {
	return userID == "" || !utils.IsTestUserID(userID)
}

func (ds *tracker) GetDataScienceCommonProperties(ctx context.Context) models.DataScienceCommonProperties {
	clientID := middleware.GetClientID(ctx)
	platform := client.GetClientPlatform(clientID)
	return models.DataScienceCommonProperties{
		ServerTime: time.Now().Unix(),
		ClientID:   clientID,
		Platform:   platform,
	}
}

func (ds *tracker) GetUserLoginForDataScience(ctx context.Context, userID string) string {
	if userID == "" {
		return ""
	}

	userProps, err := ds.UserFetcher.Fetch(ctx, userID)
	var userLogin string
	if err != nil {
		log.WithError(err).WithField("userID", userID).Error("Error looking up user from user service for userID")
	} else if userProps == nil || userProps.Login == nil {
		log.WithField("userID", userID).Error("User service response user is nil for userID")
	} else {
		userLogin = *userProps.Login
	}
	return userLogin
}
