package worker

import (
	"context"
	"fmt"
)

type NamedTerminator interface {
	Terminator
	Name() string
}

type Terminator interface {
	Shutdown(ctx context.Context) error
}

func SQSWorkerLatencyKey(worker string) string {
	return fmt.Sprintf("sqs_worker_%s_latency", worker)
}
