package automod_timeout

import (
	"context"
	"encoding/json"
	"time"

	"code.justin.tv/amzn/TwitchSafetyAutoModTwirp"
	zuma_api "code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/commerce/gogogadget/aws/sqs/sns"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend/cache/automod"
	"code.justin.tv/commerce/prism/backend/clients/stats"
	"code.justin.tv/commerce/prism/backend/datascience"
	"code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/backend/transformation"
	"code.justin.tv/commerce/prism/backend/worker"
	prism "code.justin.tv/commerce/prism/rpc"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"github.com/twitchtv/twirp"
)

const (
	uuidNamespace_message                      = "454382b0-f2ac-4eaf-95d0-59a3b8c70848"
	moderationActionAutoModCheerMessageTimeout = "automod_cheer_message_timeout"
)

func ProcessMessageReqFromJSON(jsonStr string) (*prism.ProcessMessageReq, error) {
	var req prism.ProcessMessageReq
	err := json.Unmarshal([]byte(jsonStr), &req)
	if err != nil {
		return nil, err
	}
	return &req, nil
}

// Worker is an AutoMod timeout SQS worker
type Worker struct {
	AutoModCache       automod.Cache                                `inject:""`
	AutoModClient      TwitchSafetyAutoModTwirp.TwitchSafetyAutoMod `inject:""`
	PrismAPI           prism.Prism                                  `inject:""`
	Transformer        transformation.Transformer                   `inject:""`
	Stats              stats.Statter                                `inject:""`
	DataScienceTracker datascience.Tracker                          `inject:""`
}

// Handle handles an SQS message from the AutoMod timeout SQS queue
func (w *Worker) Handle(msg *sqs.Message) error {
	startTime := time.Now()
	defer func() {
		latency := time.Since(startTime)

		w.Stats.TimingDuration(worker.SQSWorkerLatencyKey("AutoModTimeout"), latency, 1)
	}()

	ctx := context.Background()
	var processMessageReq *prism.ProcessMessageReq

	snsMsg, err := sns.Extract(msg)
	if err != nil {
		return errors.Wrap(err, "Failed to unmarshal ProcessMessageReq from SNS JSON")
	}

	processMessageReq, err = ProcessMessageReqFromJSON(snsMsg.Message)
	if err != nil {
		return errors.Wrap(err, "Failed to unmarshal ProcessMessageReq from SNS JSON")
	}

	// Check Automod service to see if the message has been approved/denied yet
	resp, err := w.AutoModClient.BulkGetCaughtMessages(ctx, &TwitchSafetyAutoModTwirp.BulkGetCaughtMessagesRequest{
		MessageIDs: []string{processMessageReq.MessageId},
	})
	if err != nil {
		// All errors including not founds will fail
		return errors.Wrap(err, "Failed to check automod message status")
	}
	caughtMessage, ok := resp.Result[processMessageReq.MessageId]
	if !ok {
		return errors.Wrap(err, "Failed to find automod message in automod response")
	}
	// Only act on expired messages
	if caughtMessage.Status != TwitchSafetyAutoModTwirp.MessageStatus_EXPIRED {
		return nil
	}

	// Get message tokens stored in redis
	// Note: Cache is keyed by userID to prevent a user from stacking multiple pending AutoMod messages
	automodCacheValue := w.AutoModCache.Get(processMessageReq.UserId)
	if automodCacheValue == nil {
		return nil
	}

	if automodCacheValue.ProcessMessageReq.MessageId != caughtMessage.Message.Id {
		return nil
	}

	err = w.AutoModCache.Delete(processMessageReq.UserId)
	if err != nil {
		return errors.Wrap(err, "Failed to delete automodded message from automod cache")
	}

	newMessageNamespace, err := uuid.FromString(uuidNamespace_message)
	if err != nil {
		return errors.Wrap(err, "Failed to generate UUID")
	}
	newMessageUUID := uuid.NewV5(newMessageNamespace, snsMsg.MessageId)

	// Remove non-consumable tokens from original message
	messageTokensJSON, err := json.Marshal(automodCacheValue.MessageTokens)
	if err != nil {
		return errors.Wrap(err, "Failed to unmarshal message tokens")
	}
	sanitizedMessage, err := w.Transformer.Transform(processMessageReq.Message, &prism.Transformation{
		Type: prism.TransformationType_REMOVE_TOKENS_EXCEPT,
		Arguments: map[string]string{
			prism_models.TransformationArg_TokensToKeep: string(messageTokensJSON),
		},
	})
	if err != nil {
		return errors.Wrap(err, "Failed to strip text from message")
	}

	originalMessageID := processMessageReq.MessageId
	processMessageReq.MessageId = newMessageUUID.String()
	processMessageReq.Message = sanitizedMessage
	processMessageReq.AutomodAcknowledged = true // TODO: add check for this

	w.trackAutoModMessageEnforcementEvent(ctx, processMessageReq)

	_, err = w.PrismAPI.ProcessMessage(ctx, processMessageReq)

	if err != nil {
		return errors.Wrap(err, "Failed to process timed out message")
	}

	// Update message in Automod service from EXPIRED to DENIED since bits were processed
	_, err = w.AutoModClient.UpdateCaughtMessages(ctx, &TwitchSafetyAutoModTwirp.UpdateCaughtMessagesRequest{
		MessageIDs: []string{originalMessageID},
		Status:     TwitchSafetyAutoModTwirp.MessageStatus_DENIED,
		ReasonCode: moderationActionAutoModCheerMessageTimeout,
	})
	if err != nil {
		twerr, ok := err.(twirp.Error)
		if !ok {
			return errors.Wrap(err, "Failed to mark message in automod as DENIED")
		}
		// Ignore error if message was not found
		if ok && twerr.Code() != twirp.NotFound {
			return errors.Wrap(twerr, "Failed to mark message in automod as DENIED")
		}
	}

	return nil
}

func (w *Worker) trackAutoModMessageEnforcementEvent(ctx context.Context, req *prism.ProcessMessageReq) {
	dataScienceCommonProperties := w.DataScienceTracker.GetDataScienceCommonProperties(ctx)
	dataScienceEvent := &models.BitsAutoModMessageEnforcementEventProperties{
		ActionType:                  models.AutoModTimedout,
		UserID:                      req.UserId,
		ChannelID:                   req.ChannelId,
		Body:                        req.Message,
		RoomID:                      req.RoomId,
		EnforcementReason:           zuma_api.AutoModEnforcementName,
		MessageID:                   req.MessageId,
		CanonicalClientID:           dataScienceCommonProperties.Platform,
		DataScienceCommonProperties: dataScienceCommonProperties,
	}

	go func(e *models.BitsAutoModMessageEnforcementEventProperties) {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		dataScienceEvent.UserLogin = w.DataScienceTracker.GetUserLoginForDataScience(ctx, req.UserId)
		dataScienceEvent.Channel = w.DataScienceTracker.GetUserLoginForDataScience(ctx, req.ChannelId)

		err := w.DataScienceTracker.TrackEvent(ctx, models.BitsAutoModMessageEnforcementEventName, dataScienceEvent, req.UserId, req.ChannelId)
		if err != nil {
			log.WithError(err).WithField("AutoModOutcome", models.AutoModTimedout).Error("Error tracking Bits Automod Message Enforcement event with data science")
		}
	}(dataScienceEvent)
}
