package automod_timeout_test

import (
	"context"
	"encoding/json"
	"errors"
	"testing"

	"code.justin.tv/amzn/TwitchSafetyAutoModTwirp"
	"code.justin.tv/chat/zuma/rpc/maap"
	"code.justin.tv/commerce/gogogadget/aws/sqs/sns"
	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/prism/backend/cache/automod"
	backend_models "code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/backend/worker/automod_timeout"
	chmod_automod_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/amzn/chmod_automod"
	automod_cache_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/cache/automod"
	stats_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/stats"
	datascience_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/datascience"
	transformation_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/transformation"
	prism_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/rpc"
	prism "code.justin.tv/commerce/prism/rpc"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func SNSMessageToJSON(msg *sns.Message) string {
	msgBytes, err := json.Marshal(msg)
	So(err, ShouldBeNil)
	return string(msgBytes)
}

func ProcessMessageReqToJSON(req *prism.ProcessMessageReq) string {
	reqBytes, err := json.Marshal(req)
	So(err, ShouldBeNil)
	return string(reqBytes)
}

func TestWorker_Handle(t *testing.T) {
	Convey("Given an AutoMod timeout worker", t, func() {
		userID := "127516203"
		channelID := "116076154"
		messageID := "test-message-id"
		message := "cheer10 fuck yeah"

		processMessageReq := prism.ProcessMessageReq{
			UserId:    userID,
			ChannelId: channelID,
			MessageId: messageID,
			Message:   message,
		}

		testErr := errors.New("test error")

		autoModCache := new(automod_cache_mock.Cache)
		prismAPI := new(prism_mock.Prism)
		transformer := new(transformation_mock.Transformer)
		stats := new(stats_mock.Statter)
		dataScienceTracker := new(datascience_mock.Tracker)
		automodClient := new(chmod_automod_mock.TwitchSafetyAutoMod)

		worker := automod_timeout.Worker{
			AutoModCache:       autoModCache,
			AutoModClient:      automodClient,
			PrismAPI:           prismAPI,
			Transformer:        transformer,
			Stats:              stats,
			DataScienceTracker: dataScienceTracker,
		}

		dataScienceTracker.On("GetUserLoginForDataScience", mock.Anything, mock.Anything).Return("")
		dataScienceTracker.On("GetDataScienceCommonProperties", mock.Anything).Return(backend_models.DataScienceCommonProperties{})
		dataScienceTracker.On("TrackEvent", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

		automodClient.On("UpdateCaughtMessages", mock.Anything, mock.Anything).Return(func(ctx context.Context, req *TwitchSafetyAutoModTwirp.UpdateCaughtMessagesRequest) *TwitchSafetyAutoModTwirp.UpdateCaughtMessagesResponse {
			So(len(req.MessageIDs), ShouldEqual, 1)
			So(req.MessageIDs[0], ShouldEqual, messageID)
			So(req.Status, ShouldEqual, TwitchSafetyAutoModTwirp.MessageStatus_DENIED)
			return &TwitchSafetyAutoModTwirp.UpdateCaughtMessagesResponse{}
		}, nil)

		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("When the SQS message is nil", func() {
			err := worker.Handle(nil)
			So(err, ShouldNotBeNil)
		})

		Convey("When the SQS body is nil", func() {
			err := worker.Handle(&sqs.Message{
				Body: nil,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("When the SQS body is invalid json", func() {
			err := worker.Handle(&sqs.Message{
				Body: pointers.StringP("not valid json"),
			})
			So(err, ShouldNotBeNil)
		})

		Convey("When the SNS message body contains invalid json", func() {
			err := worker.Handle(&sqs.Message{
				Body: pointers.StringP(SNSMessageToJSON(&sns.Message{
					Message: "not valid json",
				})),
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Given a valid SQS message", func() {
			sqsMsg := &sqs.Message{
				Body: pointers.StringP(SNSMessageToJSON(&sns.Message{
					Message: ProcessMessageReqToJSON(&processMessageReq),
				})),
			}

			Convey("When Automod service returns entry with status not EXPIRED", func() {
				automodClient.On("BulkGetCaughtMessages", mock.Anything, mock.Anything).Return(&TwitchSafetyAutoModTwirp.BulkGetCaughtMessagesResponse{
					Result: map[string]*TwitchSafetyAutoModTwirp.CaughtMessage{
						messageID: {
							Status: TwitchSafetyAutoModTwirp.MessageStatus_ALLOWED,
						},
					},
				}, nil)

				err := worker.Handle(sqsMsg)
				So(err, ShouldBeNil)
			})

			Convey("When Automod service returns entry with status EXPIRED", func() {
				automodClient.On("BulkGetCaughtMessages", mock.Anything, mock.Anything).Return(&TwitchSafetyAutoModTwirp.BulkGetCaughtMessagesResponse{
					Result: map[string]*TwitchSafetyAutoModTwirp.CaughtMessage{
						messageID: {
							Status: TwitchSafetyAutoModTwirp.MessageStatus_EXPIRED,
							Message: &maap.Message{
								Id: messageID,
							},
						},
					},
				}, nil)

				Convey("When the AutoMod cache returns nil", func() {
					autoModCache.On("Get", mock.Anything).Return(nil)

					err := worker.Handle(sqsMsg)
					So(err, ShouldBeNil)
				})

				Convey("When the AutoMod cache returns a different request than the SQS message request", func() {
					autoModCache.On("Get", mock.Anything).Return(&automod.CacheValue{
						ProcessMessageReq: prism.ProcessMessageReq{MessageId: "some-other-message"},
					})

					err := worker.Handle(sqsMsg)
					So(err, ShouldBeNil)
				})

				Convey("When the AutoMod cache and Automod service returns a valid request", func() {
					autoModCache.On("Get", mock.Anything).Return(&automod.CacheValue{
						ProcessMessageReq: processMessageReq,
					})

					Convey("When the AutoMod cache fails to delete the existing request", func() {
						autoModCache.On("Delete", mock.Anything).Return(testErr)

						err := worker.Handle(sqsMsg)
						So(err, ShouldNotBeNil)
					})

					Convey("When the AutoMod cache successfully deletes the existing request", func() {
						autoModCache.On("Delete", mock.Anything).Return(nil)

						Convey("When the Transformer fails", func() {
							transformer.On("Transform", mock.Anything, mock.Anything).Return("", testErr)

							err := worker.Handle(sqsMsg)
							So(err, ShouldNotBeNil)
						})

						Convey("When the transformer succeeds", func() {
							transformer.On("Transform", mock.Anything, mock.Anything).Return("new message", nil)

							Convey("When the ProcessMessage call fails", func() {
								prismAPI.On("ProcessMessage", mock.Anything, mock.Anything).Return(nil, testErr)

								err := worker.Handle(sqsMsg)
								So(err, ShouldNotBeNil)
							})

							Convey("When the ProcessMessage call succeeds", func() {
								prismAPI.On("ProcessMessage", mock.Anything, mock.Anything).Return(nil, nil)

								err := worker.Handle(sqsMsg)
								So(err, ShouldBeNil)
							})
						})
					})
				})
			})
		})

	})

}
