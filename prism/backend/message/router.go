package message

import (
	"context"
	"errors"
	"fmt"

	"code.justin.tv/chat/chatrooms/rpc/chatrooms"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend/clients/rooms"
	"code.justin.tv/commerce/prism/backend/clients/tmi"
	"code.justin.tv/commerce/prism/backend/consumable"
	transaction_dynamo "code.justin.tv/commerce/prism/backend/dynamo/transaction"
	"code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/config"
	prism "code.justin.tv/commerce/prism/rpc/models"
)

// Router represents a message router
type Router interface {
	RouteMessage(ctx context.Context, tx transaction_dynamo.Transaction) (prism.Route, string, error)
}

type router struct {
	TMIClient   tmi.ITMIClient `inject:""` // TODO: Migrate off payday TMI client
	RoomsClient rooms.Client   `inject:""`
}

// NewRouter creates a new message Router
func NewRouter() Router {
	return &router{}
}

// NewTestRouter creates a new test message Router
func NewTestRouter(tmiClient tmi.ITMIClient, roomsClient rooms.Client) Router {
	return &router{
		TMIClient:   tmiClient,
		RoomsClient: roomsClient,
	}
}

func (r *router) RouteMessage(ctx context.Context, tx transaction_dynamo.Transaction) (prism.Route, string, error) {
	var chatErr error

	route, ok := prism.MessageTreatmentRouter[tx.MessageTreatment]
	if !ok {
		log.WithField("transaction", tx).Error("Could not find route for message treatment")
	}

	if !hasWidemotes(tx.ConsumablesProcessed) && !hasBits(tx.ConsumablesProcessed) {
		msg := "message must process supported consumables"
		log.WithField("consumablesProcessed", tx.ConsumablesProcessed).Error(msg)
		return route, "", errors.New(msg)
	}

	var sentMessage string
	switch {
	case route == prism.SYSTEM_MESSAGE_ROUTE:
		systemMessageParams := tmi.SystemMessageParams{
			Recipient: string(tx.ChannelID),
			Message:   tx.ProcessedMessage,
			ID:        string(tx.MessageTreatment),
		}
		chatErr = r.TMIClient.SendSystemMessage(systemMessageParams)
		if chatErr == nil {
			sentMessage = tx.ProcessedMessage
		}
	case tx.RoomID != "" && r.RoomsClient.IsRoomsEnabledChannel(tx.ChannelID):
		route = prism.ROOMS_ROUTE

		bitsAmount := tx.ConsumablesProcessed[consumable.Tenant_Payday][consumable.ConsumableType_Bits][consumable.Consumable_Bits]

		var resp *chatrooms.SendMessageResponse
		resp, chatErr = r.RoomsClient.SendMessageToRoom(ctx, &chatrooms.SendMessageRequest{
			RoomId:      tx.RoomID,
			BitsAmount:  bitsAmount,
			MessageText: tx.ProcessedMessage,
			Nonce:       tx.TransactionID,
			SenderId:    tx.UserID,
		})
		if resp != nil && resp.Message != nil && resp.Message.Content != nil {
			sentMessage = resp.Message.Content.Text
		}
	case route == prism.ANON_CHEER_ROUTE:
		route = prism.TMI_ROUTE

		bitsAmount := tx.ConsumablesProcessed[consumable.Tenant_Payday][consumable.ConsumableType_Bits][consumable.Consumable_Bits]

		var resp *tmi.BitsMessageResponse
		resp, chatErr = r.TMIClient.SendBitsMessage(ctx, tmi.SendMessageParams{
			Sender:    models.AnAnonymousCheererTUID,
			Recipient: tx.ChannelID,
			Message:   tx.ProcessedMessage,
			Amount:    int(bitsAmount),
		})
		if resp != nil {
			sentMessage = resp.MsgBody
		}
	case route == prism.TMI_ROUTE:
		bitsAmount := tx.ConsumablesProcessed[consumable.Tenant_Payday][consumable.ConsumableType_Bits][consumable.Consumable_Bits]

		var resp *tmi.BitsMessageResponse
		resp, chatErr = r.TMIClient.SendBitsMessage(ctx, tmi.SendMessageParams{
			Sender:    tx.UserID,
			Recipient: tx.ChannelID,
			Message:   tx.ProcessedMessage,
			Amount:    int(bitsAmount),
			ImageURL:  tx.ImageURL,
		})
		if resp != nil {
			sentMessage = resp.MsgBody
			// For a currently unknown reason, we occasionally get an errorless and messageless response from TMI.
			// If we don't treat this as an error the Payday tenant postprocessing steps will fail, resulting in the
			// user's bits being taken without a message posted.
			if sentMessage == "" {
				chatErr = fmt.Errorf("TMI response for transaction %s had no message body", tx.TransactionID)
				errorCode := ""
				if resp.ErrorCode != nil {
					errorCode = *resp.ErrorCode
				}

				log.WithFields(log.Fields{
					"transaction":  tx,
					"tmi_response": resp,
					"route":        route,
					"errorCode":    errorCode,
				}).Error("TMI returned non-error response with no message body")
			}
		}
	default:
		msg := "unknown or undefined route specified for message"
		log.WithField("transaction", tx).Error(msg)
		return route, "", errors.New(msg)
	}

	return route, sentMessage, chatErr
}

func hasWidemotes(consumablesProcessed map[config.TenantName]models.ConsumableMap) bool {
	payday, ok := consumablesProcessed[consumable.Tenant_Payday]
	if !ok {
		return false
	}

	widemotes, ok := payday[consumable.ConsumableType_Widemotes]
	if !ok {
		return false
	}

	for _, widemoteProcessed := range widemotes {
		if widemoteProcessed > 0 {
			return true
		}
	}
	return false
}

func hasBits(consumablesProcessed map[config.TenantName]models.ConsumableMap) bool {
	payday, ok := consumablesProcessed[consumable.Tenant_Payday]
	if !ok {
		return false
	}

	bits, ok := payday[consumable.ConsumableType_Bits]
	if !ok {
		return false
	}

	return bits[consumable.Consumable_Bits] > 0
}
