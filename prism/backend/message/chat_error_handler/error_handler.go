package chat_error_handler

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/aws/s3"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/prism/backend/clients/slack"
	"code.justin.tv/commerce/prism/backend/clients/sns"
	"code.justin.tv/commerce/prism/backend/consumable"
	"code.justin.tv/commerce/prism/backend/dynamo/transaction"
	"code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/backend/usersservice"
	"code.justin.tv/commerce/prism/config"
	"github.com/sirupsen/logrus"
)

const (
	productionEnvironment = "prod"
)

type ChatErrorHandler interface {
	HandleChatError(tx *transaction.Transaction, chatSendErr error)
}

type chatErrorHandler struct {
	Config      *config.Config       `inject:""`
	Slacker     slack.Slacker        `inject:""`
	UserFetcher usersservice.Fetcher `inject:""`
	S3Client    s3.Client            `inject:""`
	SNSClient   sns.IClient          `inject:""`
}

func NewChatErrorHandler() ChatErrorHandler {
	return &chatErrorHandler{}
}

type ChatError struct {
	TransactionID        string               `json:"transaction_id"`
	Timestamp            time.Time            `json:"timestamp"`
	UserID               string               `json:"user_id"`
	UserLogin            string               `json:"user_login"`
	ChannelID            string               `json:"channel_id"`
	ChannelLogin         string               `json:"channel_login"`
	ConsumablesRequested models.ConsumableMap `json:"consumables_requested"`
	RequestedMessage     string               `json:"requested_message"`
	ProcessedMessage     string               `json:"processed_message"`
	MessageRoute         string               `json:"message_route"`
	ChatError            string               `json:"chat_error"`
}

func (ceh *chatErrorHandler) HandleChatError(tx *transaction.Transaction, chatSendErr error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if tx == nil {
		logrus.Error("cannot handle nil transaction")
		return
	}

	chatSendErrStr := "nil"
	if chatSendErr != nil {
		chatSendErrStr = chatSendErr.Error()
	}

	userLogin := ceh.getUserLoginFromID(ctx, tx.UserID)
	channelLogin := ceh.getUserLoginFromID(ctx, tx.ChannelID)
	err := ceh.postToSlack(tx, userLogin, channelLogin, chatSendErrStr)
	if err != nil {
		logrus.WithError(err).Error("error posting chat error to slack")
	}

	ce := toChatError(tx, userLogin, channelLogin, chatSendErrStr)

	err = ceh.postToSNS(ctx, ce)
	if err != nil {
		logrus.WithError(err).Error("error posting chat error to sns")
	}

	err = ceh.postToS3(ctx, ce)
	if err != nil {
		logrus.WithError(err).Error("error posting chat error to s3")
	}
}

func (ceh *chatErrorHandler) postToSlack(tx *transaction.Transaction, userLogin string, channelLogin string, chatSendErrStr string) error {
	if ceh.Config == nil || strings.Blank(ceh.Config.ChatFailureSlackWebhookURL) {
		return errors.New("slack webhook not configured")
	}

	text := ":prism_mike: *Chat error in prism message* :prism_mike:\n"

	if ceh.Config.EnvironmentName != productionEnvironment {
		text += fmt.Sprintf("Don't worry it's *not prod*. Environment: %s\n", ceh.Config.EnvironmentName)
	}

	if tx.Anonymous {
		text += fmt.Sprintf(":bogashh: *ANONYMOUS CHEER* :bogashh:\n")
	}

	var bitsAmount int32
	if _, ok := tx.ConsumablesRequested[consumable.ConsumableType_Bits]; !ok {
		bitsAmount = int32(tx.ConsumablesRequested[consumable.ConsumableType_Bits][consumable.Consumable_Bits])
	}

	text += fmt.Sprintf("%s `%d`\n", getSlackBitsEmote(bitsAmount, tx.Anonymous), bitsAmount)
	text += "```"
	text += fmt.Sprintf("Transaction ID:    %s\n", tx.TransactionID)
	text += fmt.Sprintf("User:              %s (%s)\n", userLogin, tx.UserID)
	text += fmt.Sprintf("Channel:           %s (%s)\n", channelLogin, tx.ChannelID)
	text += fmt.Sprintf("Bits Amount:       %d\n", bitsAmount)
	text += fmt.Sprintf("Requested Message: %s\n", tx.RequestedMessage)
	text += fmt.Sprintf("Processed Message: %s\n", tx.ProcessedMessage)
	text += fmt.Sprintf("Message Route:     %s\n", tx.MessageRoute)
	text += fmt.Sprintf("Chat Error:        %s", chatSendErrStr)
	text += "```"

	return ceh.Slacker.PostMsg(ceh.Config.ChatFailureSlackWebhookURL, text)
}

func (ceh *chatErrorHandler) postToS3(ctx context.Context, ce ChatError) error {
	if strings.Blank(ceh.Config.ChatFailureS3Bucket) {
		return errors.New("s3 bucket not configured")
	}

	key := fmt.Sprintf("%s/%d/%s", ceh.Config.EnvironmentName, ce.Timestamp.UnixNano(), ce.TransactionID)

	ceJSON, err := json.Marshal(ce)
	if err != nil {
		return err
	}
	ceReader := bytes.NewReader(ceJSON)

	return ceh.S3Client.PutFile(ctx, ceh.Config.ChatFailureS3Bucket, key, ceReader)
}

func (ceh *chatErrorHandler) postToSNS(ctx context.Context, ce ChatError) error {
	if strings.Blank(ceh.Config.ChatFailureSNSTopicARN) {
		return errors.New("sns topic not configured")
	}

	ceJSON, err := json.Marshal(ce)
	if err != nil {
		return err
	}

	return ceh.SNSClient.PostToTopic(ceh.Config.ChatFailureSNSTopicARN, string(ceJSON))
}

func (ceh *chatErrorHandler) getUserLoginFromID(ctx context.Context, id string) string {
	userProps, err := ceh.UserFetcher.Fetch(ctx, id)
	if err != nil {
		logrus.WithField("id", id).WithError(err).Error("error getting login from id from users service")
		return "<user_service_error>"
	}
	if userProps == nil || userProps.Login == nil {
		logrus.WithField("id", id).Error("users service returned nil user")
		return "<user_not_found>"
	}
	return *userProps.Login
}

func getSlackBitsEmote(amount int32, isAnonymous bool) string {
	var emote string
	if isAnonymous {
		emote = "anon"
	} else {
		emote = "chuckCheer"
	}

	var tier int
	switch {
	case amount < 100:
		tier = 1
	case amount < 1000:
		tier = 100
	case amount < 5000:
		tier = 1000
	case amount < 10000:
		tier = 5000
	case amount < 100000:
		tier = 10000
	default:
		tier = 100000
	}

	return fmt.Sprintf(":%s%d:", emote, tier)
}

func toChatError(tx *transaction.Transaction, userLogin string, channelLogin string, chatSendErr string) ChatError {
	return ChatError{
		TransactionID:        tx.TransactionID,
		Timestamp:            tx.StartTime,
		UserID:               tx.UserID,
		UserLogin:            userLogin,
		ChannelID:            tx.ChannelID,
		ChannelLogin:         channelLogin,
		ConsumablesRequested: tx.ConsumablesRequested,
		RequestedMessage:     tx.RequestedMessage,
		ProcessedMessage:     tx.ProcessedMessage,
		MessageRoute:         string(tx.MessageRoute),
		ChatError:            chatSendErr,
	}
}
