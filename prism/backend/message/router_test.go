package message

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/chat/chatrooms/rpc/chatrooms"
	"code.justin.tv/commerce/prism/backend/clients/tmi"
	"code.justin.tv/commerce/prism/backend/consumable"
	transaction_dynamo "code.justin.tv/commerce/prism/backend/dynamo/transaction"
	"code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/config"
	rooms_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/rooms"
	tmi_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/tmi"
	prism "code.justin.tv/commerce/prism/rpc/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestProcessor_RouteMessage(t *testing.T) {
	Convey("Given a router", t, func() {
		tmiClient := new(tmi_mock.ITMIClient)
		roomsClient := new(rooms_mock.Client)

		router := NewTestRouter(tmiClient, roomsClient)

		roomsClient.On("IsRoomsEnabledChannel", mock.Anything).Return(true)

		Convey("For messages without a route", func() {
			Convey("we error out", func() {
				tx := transaction_dynamo.Transaction{
					ProcessedMessage: "test",
					ChannelID:        "116076154", //qa_bits_partner
					UserID:           "127380717", //graham
					TransactionID:    "abc123",
				}

				route, newMsg, err := router.RouteMessage(context.Background(), tx)

				So(route, ShouldBeEmpty)
				So(err, ShouldNotBeNil)
				So(newMsg, ShouldEqual, "")
			})
		})

		Convey("For messages sent to sendBitsMessage", func() {
			Convey("when tmi sendBitsMessage returns an error", func() {
				tmiClient.On("SendBitsMessage", mock.Anything, mock.Anything).Return(nil, errors.New("test TMI error"))

				tx := transaction_dynamo.Transaction{
					ProcessedMessage: "test",
					ChannelID:        "116076154", //qa_bits_partner
					UserID:           "127380717", //graham
					TransactionID:    "abc123",
					MessageTreatment: prism.DEFAULT_MESSAGE,
				}

				route, newMsg, err := router.RouteMessage(context.Background(), tx)

				So(newMsg, ShouldEqual, "")
				So(route, ShouldEqual, prism.TMI_ROUTE)
				So(err, ShouldNotBeNil)
			})

			Convey("a transaction by default goes to TMI", func() {
				tx := transaction_dynamo.Transaction{
					ProcessedMessage: "test cheer1 coregamers love value",
					ChannelID:        "116076154", //qa_bits_partner
					UserID:           "127380717", //graham
					TransactionID:    "abc123",
					ConsumablesProcessed: map[config.TenantName]models.ConsumableMap{
						consumable.Tenant_Payday: {
							consumable.ConsumableType_Bits: {
								consumable.Consumable_Bits: 1,
							},
						},
					},
					MessageTreatment: prism.DEFAULT_MESSAGE,
				}

				tmiClient.On("SendBitsMessage", mock.Anything, mock.Anything).Return(&tmi.BitsMessageResponse{
					MsgBody: "test cheer1 *** love value",
				}, nil)

				route, newMsg, err := router.RouteMessage(context.Background(), tx)

				So(newMsg, ShouldEqual, "test cheer1 *** love value")
				So(route, ShouldEqual, prism.TMI_ROUTE)
				So(err, ShouldBeNil)
			})
		})

		Convey("For messages sent to rooms", func() {
			Convey("when rooms returns an error", func() {
				roomsClient.On("SendMessageToRoom", mock.Anything, mock.Anything).Return(nil, errors.New("test rooms error"))

				tx := transaction_dynamo.Transaction{
					RoomID:           "1234",
					ProcessedMessage: "test cheer1",
					ChannelID:        "116076154", //qa_bits_partner
					UserID:           "127380717", //graham
					TransactionID:    "abc123",
					ConsumablesProcessed: map[config.TenantName]models.ConsumableMap{
						consumable.Tenant_Payday: {
							consumable.ConsumableType_Bits: {
								consumable.Consumable_Bits: 1,
							},
						},
					},
					MessageTreatment: prism.DEFAULT_MESSAGE,
				}

				route, newMsg, err := router.RouteMessage(context.Background(), tx)

				So(newMsg, ShouldEqual, "")
				So(route, ShouldEqual, prism.ROOMS_ROUTE)
				So(err, ShouldNotBeNil)
			})

			Convey("when rooms succeeds", func() {
				tx := transaction_dynamo.Transaction{
					RoomID:           "1234",
					ProcessedMessage: "test cheer1 coregamers love value",
					ChannelID:        "116076154", //qa_bits_partner
					UserID:           "127380717", //graham
					TransactionID:    "abc123",
					ConsumablesProcessed: map[config.TenantName]models.ConsumableMap{
						consumable.Tenant_Payday: {
							consumable.ConsumableType_Bits: {
								consumable.Consumable_Bits: 1,
							},
						},
					},
					MessageTreatment: prism.DEFAULT_MESSAGE,
				}

				roomsClient.On("SendMessageToRoom", mock.Anything, mock.Anything).Return(&chatrooms.SendMessageResponse{
					Message: &chatrooms.Message{
						Content: &chatrooms.MessageContent{
							Text: "test cheer1 *** love value",
						},
					},
					Success:      true,
					ResponseCode: "200",
				}, nil)

				route, newMsg, err := router.RouteMessage(context.Background(), tx)

				So(newMsg, ShouldEqual, "test cheer1 *** love value")
				So(route, ShouldEqual, prism.ROOMS_ROUTE)
				So(err, ShouldBeNil)
			})
		})

		Convey("For messages sent to sendSystemMessage", func() {
			Convey("when tmi sendSystemMessage returns an error", func() {
				tmiClient.On("SendSystemMessage", mock.Anything).Return(errors.New("test TMI error"))

				tx := transaction_dynamo.Transaction{
					ProcessedMessage: "test cheer1",
					ChannelID:        "116076154", //qa_bits_partner
					UserID:           "127380717", //graham
					TransactionID:    "abc123",
					ConsumablesProcessed: map[config.TenantName]models.ConsumableMap{
						consumable.Tenant_Payday: {
							consumable.ConsumableType_Bits: {
								consumable.Consumable_Bits: 1,
							},
						},
					},
					MessageTreatment: prism.SYSTEM_MESSAGE,
				}

				route, newMsg, err := router.RouteMessage(context.Background(), tx)

				So(newMsg, ShouldEqual, "")
				So(route, ShouldEqual, prism.SYSTEM_MESSAGE_ROUTE)
				So(err, ShouldNotBeNil)
			})

			Convey("a transaction with a special treatment can target system message", func() {
				tx := transaction_dynamo.Transaction{
					ProcessedMessage: "test cheer1 coregamers love value",
					ChannelID:        "116076154", //qa_bits_partner
					UserID:           "127380717", //graham
					TransactionID:    "abc123",
					ConsumablesProcessed: map[config.TenantName]models.ConsumableMap{
						consumable.Tenant_Payday: {
							consumable.ConsumableType_Bits: {
								consumable.Consumable_Bits: 1,
							},
						},
					},
					MessageTreatment: prism.SYSTEM_MESSAGE,
				}
				tmiClient.On("SendSystemMessage", mock.Anything).Return(nil)

				route, newMsg, err := router.RouteMessage(context.Background(), tx)

				So(newMsg, ShouldEqual, tx.ProcessedMessage)
				So(route, ShouldEqual, prism.SYSTEM_MESSAGE_ROUTE)
				So(err, ShouldBeNil)
			})
		})
	})
}
