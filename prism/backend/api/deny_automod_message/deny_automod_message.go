package deny_automod_message

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/amzn/TwitchSafetyAutoModTwirp"
	zuma_api "code.justin.tv/chat/zuma/app/api"
	zuma_client "code.justin.tv/chat/zuma/client"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend/cache/automod"
	"code.justin.tv/commerce/prism/backend/datascience"
	"code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/backend/transformation"
	"code.justin.tv/commerce/prism/backend/utils"
	deny_automod_message_validator "code.justin.tv/commerce/prism/backend/validation/deny_automod_message"
	prism "code.justin.tv/commerce/prism/rpc"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	"github.com/twitchtv/twirp"
)

const (
	timeout = 5 * time.Second
)

// NewAPI creates a new API
func NewAPI() *API {
	return &API{}
}

// API is the DenyAutoModMessage API struct
type API struct {
	DenyAutoModMessageValidator deny_automod_message_validator.Validator     `inject:""`
	AutoModClient               TwitchSafetyAutoModTwirp.TwitchSafetyAutoMod `inject:""`
	AutoModCache                automod.Cache                                `inject:""`
	ZumaClient                  zuma_client.Client                           `inject:""`
	PrismAPI                    prism.Prism                                  `inject:""`
	Transformer                 transformation.Transformer                   `inject:""`
	DataScienceTracker          datascience.Tracker                          `inject:""`
}

// DenyAutoModMessage handles the logic for deny an automodded message
func (api *API) DenyAutoModMessage(ctx context.Context, req *prism.DenyAutoModMessageReq) (*prism.DenyAutoModMessageResp, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	err := api.DenyAutoModMessageValidator.Validate(req)
	if err != nil {
		return nil, err
	}

	// Check that the TargetUserId is currently moderated
	automodCacheValue := api.AutoModCache.Get(req.TargetUserId)
	if automodCacheValue == nil {
		return nil, twirp.NotFoundError(fmt.Sprintf("TargetUserId \"%s\" is not currently moderated", req.TargetUserId))
	}
	originalAutoModdedRequest := &automodCacheValue.ProcessMessageReq

	// Check that the requester is a mod in the channel
	zumaResponse, err := api.ZumaClient.GetMod(ctx, originalAutoModdedRequest.ChannelId, req.UserId, nil)
	if err != nil {
		msg := "DenyAutoModMessage encountered error while getting mod from Zuma"
		log.WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	if !zumaResponse.IsMod && req.UserId != originalAutoModdedRequest.ChannelId {
		return nil, twirp.InvalidArgumentError("UserId", fmt.Sprintf("\"%s\" is not a mod in channel \"%s\"", req.UserId, originalAutoModdedRequest.ChannelId))
	}

	// Delete the target user from the automod cache, as they are now officially unmodded
	err = api.AutoModCache.Delete(req.TargetUserId)
	if err != nil {
		msg := "DenyAutoModMessage encountered error while deleting from AutoMod cache"
		log.WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	// Remove non-consumable tokens from original message
	messageTokensJSON, err := json.Marshal(automodCacheValue.MessageTokens)
	if err != nil {
		msg := "DenyAutoModMessage encountered error while marshalling message tokens"
		log.WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}
	sanitizedMessage, err := api.Transformer.Transform(originalAutoModdedRequest.Message, &prism.Transformation{
		Type: prism.TransformationType_REMOVE_TOKENS_EXCEPT,
		Arguments: map[string]string{
			prism_models.TransformationArg_TokensToKeep: string(messageTokensJSON),
		},
	})
	if err != nil {
		msg := "DenyAutoModMessage encountered error stripping text from message"
		log.WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	// Re-process the message request
	originalAutoModdedRequest.MessageId = req.MessageId
	originalAutoModdedRequest.Message = sanitizedMessage
	originalAutoModdedRequest.AutomodAcknowledged = true

	api.trackAutoModMessageEnforcementEvent(ctx, originalAutoModdedRequest)

	_, err = api.PrismAPI.ProcessMessage(ctx, originalAutoModdedRequest)

	if err != nil {
		msg := "DenyAutoModMessage encountered error while re-processing message"
		log.WithError(err).Error(msg)

		if utils.IsTwirpError(err) {
			return nil, err
		}
		return nil, twirp.InternalError(msg)
	}

	return &prism.DenyAutoModMessageResp{}, nil
}

func (api *API) trackAutoModMessageEnforcementEvent(ctx context.Context, req *prism.ProcessMessageReq) {
	dataScienceCommonProperties := api.DataScienceTracker.GetDataScienceCommonProperties(ctx)
	dataScienceEvent := &models.BitsAutoModMessageEnforcementEventProperties{
		ActionType:                  models.AutoModDenied,
		UserID:                      req.UserId,
		ChannelID:                   req.ChannelId,
		Body:                        req.Message,
		RoomID:                      req.RoomId,
		EnforcementReason:           zuma_api.AutoModEnforcementName,
		MessageID:                   req.MessageId,
		CanonicalClientID:           dataScienceCommonProperties.Platform,
		DataScienceCommonProperties: dataScienceCommonProperties,
	}

	go func(e *models.BitsAutoModMessageEnforcementEventProperties) {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		dataScienceEvent.UserLogin = api.DataScienceTracker.GetUserLoginForDataScience(ctx, req.UserId)
		dataScienceEvent.Channel = api.DataScienceTracker.GetUserLoginForDataScience(ctx, req.ChannelId)

		err := api.DataScienceTracker.TrackEvent(ctx, models.BitsAutoModMessageEnforcementEventName, dataScienceEvent, req.UserId, req.ChannelId)
		if err != nil {
			log.WithError(err).WithField("AutoModOutcome", models.AutoModDenied).Error("Error tracking Bits Automod Message Enforcement event with data science")
		}
	}(dataScienceEvent)
}
