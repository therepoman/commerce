package approve_automod_message_test

import (
	"context"
	"errors"
	"testing"

	zuma_api "code.justin.tv/chat/zuma/app/api"
	"code.justin.tv/commerce/prism/backend/api/approve_automod_message"
	"code.justin.tv/commerce/prism/backend/cache/automod"
	backend_models "code.justin.tv/commerce/prism/backend/models"
	zuma_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/chat/zuma"
	automod_cache_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/cache/automod"
	datascience_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/datascience"
	approve_automod_message_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/validation/approve_automod_message"
	prism_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/rpc"
	prism "code.justin.tv/commerce/prism/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestAPI_ApproveAutoModMessage(t *testing.T) {
	Convey("Given an API", t, func() {
		userID := "127516203"
		channelID := "116076154"
		messageID := "test-message-id"
		message := "cheer10 fuck yeah"

		processMessageReq := prism.ProcessMessageReq{
			UserId:    userID,
			ChannelId: channelID,
			MessageId: messageID,
			Message:   message,
		}

		req := &prism.ApproveAutoModMessageReq{
			UserId:       channelID,
			TargetUserId: userID,
			MessageId:    messageID,
		}

		ctx := context.Background()

		testErr := errors.New("test error")

		autoModCache := new(automod_cache_mock.Cache)
		prismAPI := new(prism_mock.Prism)
		validator := new(approve_automod_message_mock.Validator)
		zumaClient := new(zuma_mock.Client)
		dataScienceTracker := new(datascience_mock.Tracker)

		api := approve_automod_message.API{
			AutoModCache:                   autoModCache,
			PrismAPI:                       prismAPI,
			ApproveAutoModMessageValidator: validator,
			ZumaClient:                     zumaClient,
			DataScienceTracker:             dataScienceTracker,
		}

		dataScienceTracker.On("GetUserLoginForDataScience", mock.Anything, mock.Anything).Return("")
		dataScienceTracker.On("GetDataScienceCommonProperties", mock.Anything).Return(backend_models.DataScienceCommonProperties{})
		dataScienceTracker.On("TrackEvent", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("When the validator errors", func() {
			validator.On("Validate", mock.Anything).Return(errors.New("test error"))

			res, err := api.ApproveAutoModMessage(ctx, req)
			So(err, ShouldNotBeNil)
			So(res, ShouldBeNil)
		})

		Convey("When the validator succeeds", func() {
			validator.On("Validate", mock.Anything).Return(nil)

			Convey("When the AutoMod cache returns nil", func() {
				autoModCache.On("Get", mock.Anything).Return(nil)

				res, err := api.ApproveAutoModMessage(ctx, req)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeNil)
			})

			Convey("When the AutoMod cache returns a valid request", func() {
				autoModCache.On("Get", mock.Anything).Return(&automod.CacheValue{
					ProcessMessageReq: processMessageReq,
				})

				Convey("When the zuma client fails", func() {
					zumaClient.On("GetMod", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(zuma_api.GetModResponse{}, errors.New("test error"))

					res, err := api.ApproveAutoModMessage(ctx, req)
					So(err, ShouldNotBeNil)
					So(res, ShouldBeNil)
				})

				Convey("When the zuma client succeeds", func() {
					Convey("When the requester is not a channel mod or broadcaster", func() {
						zumaClient.On("GetMod", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(zuma_api.GetModResponse{IsMod: false}, nil)
						req.UserId = "1234"

						res, err := api.ApproveAutoModMessage(ctx, req)
						So(err, ShouldNotBeNil)
						So(res, ShouldBeNil)
					})

					Convey("When the requester is a channel mod or broadcaster", func() {
						zumaClient.On("GetMod", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(zuma_api.GetModResponse{IsMod: false}, nil)
						req.UserId = processMessageReq.ChannelId

						Convey("When the AutoMod cache fails to delete the existing request", func() {
							autoModCache.On("Delete", mock.Anything).Return(testErr)

							res, err := api.ApproveAutoModMessage(ctx, req)
							So(err, ShouldNotBeNil)
							So(res, ShouldBeNil)
						})

						Convey("When the AutoMod cache successfully deletes the existing request", func() {
							autoModCache.On("Delete", mock.Anything).Return(nil)

							Convey("When the ProcessMessage call fails", func() {
								prismAPI.On("ProcessMessage", mock.Anything, mock.Anything).Return(nil, testErr)

								res, err := api.ApproveAutoModMessage(ctx, req)
								So(err, ShouldNotBeNil)
								So(res, ShouldBeNil)
							})

							Convey("When the ProcessMessage call succeeds", func() {
								prismAPI.On("ProcessMessage", mock.Anything, mock.Anything).Return(nil, nil)
								res, err := api.ApproveAutoModMessage(ctx, req)
								So(err, ShouldBeNil)
								So(res, ShouldNotBeNil)
							})
						})
					})
				})
			})
		})
	})
}
