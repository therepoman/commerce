package approve_automod_message

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/amzn/TwitchSafetyAutoModTwirp"
	zuma_api "code.justin.tv/chat/zuma/app/api"
	zuma_client "code.justin.tv/chat/zuma/client"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend/cache/automod"
	"code.justin.tv/commerce/prism/backend/datascience"
	"code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/backend/utils"
	approve_automod_message_validator "code.justin.tv/commerce/prism/backend/validation/approve_automod_message"
	prism "code.justin.tv/commerce/prism/rpc"
	"github.com/twitchtv/twirp"
)

const (
	timeout = 5 * time.Second
)

// NewAPI creates a new API
func NewAPI() *API {
	return &API{}
}

// API is the ApproveAutoModMessage API struct
type API struct {
	ApproveAutoModMessageValidator approve_automod_message_validator.Validator  `inject:""`
	AutoModClient                  TwitchSafetyAutoModTwirp.TwitchSafetyAutoMod `inject:""`
	AutoModCache                   automod.Cache                                `inject:""`
	ZumaClient                     zuma_client.Client                           `inject:""`
	PrismAPI                       prism.Prism                                  `inject:""`
	DataScienceTracker             datascience.Tracker                          `inject:""`
}

// ApproveAutoModMessage handles the logic for approving an automodded message
func (api *API) ApproveAutoModMessage(ctx context.Context, req *prism.ApproveAutoModMessageReq) (*prism.ApproveAutoModMessageResp, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	err := api.ApproveAutoModMessageValidator.Validate(req)
	if err != nil {
		return nil, err
	}

	// Check that the TargetUserId is currently moderated
	automodCacheValue := api.AutoModCache.Get(req.TargetUserId)
	if automodCacheValue == nil {
		return nil, twirp.NotFoundError(fmt.Sprintf("TargetUserId \"%s\" is not currently moderated", req.TargetUserId))
	}
	originalAutoModdedRequest := &automodCacheValue.ProcessMessageReq

	// Check that the requester is a mod in the channel
	zumaResponse, err := api.ZumaClient.GetMod(ctx, originalAutoModdedRequest.ChannelId, req.UserId, nil)
	if err != nil {
		msg := "ApproveAutoModMessage encountered error while getting mod from Zuma"
		log.WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	if !zumaResponse.IsMod && req.UserId != originalAutoModdedRequest.ChannelId {
		return nil, twirp.InvalidArgumentError("UserId", fmt.Sprintf("\"%s\" is not a mod in channel \"%s\"", req.UserId, originalAutoModdedRequest.ChannelId))
	}

	// Delete the target user from the automod cache, as they are now officially unmodded
	err = api.AutoModCache.Delete(req.TargetUserId)
	if err != nil {
		msg := "ApproveAutoModMessage encountered error while deleting from AutoMod cache"
		log.WithError(err).Error(msg)
		return nil, twirp.InternalError(msg)
	}

	// Re-process the message request
	originalAutoModdedRequest.MessageId = req.MessageId
	originalAutoModdedRequest.AutomodAcknowledged = true

	api.trackAutoModMessageEnforcementEvent(ctx, originalAutoModdedRequest)

	_, err = api.PrismAPI.ProcessMessage(ctx, originalAutoModdedRequest)
	if err != nil {
		msg := "ApproveAutoModMessage encountered error while re-processing message"
		log.WithError(err).Error(msg)

		if utils.IsTwirpError(err) {
			return nil, err
		}
		return nil, twirp.InternalError(msg)
	}

	return &prism.ApproveAutoModMessageResp{}, nil
}

func (api *API) trackAutoModMessageEnforcementEvent(ctx context.Context, req *prism.ProcessMessageReq) {
	dataScienceCommonProperties := api.DataScienceTracker.GetDataScienceCommonProperties(ctx)
	dataScienceEvent := &models.BitsAutoModMessageEnforcementEventProperties{
		ActionType:                  models.AutoModApproved,
		UserID:                      req.UserId,
		ChannelID:                   req.ChannelId,
		Body:                        req.Message,
		RoomID:                      req.RoomId,
		EnforcementReason:           zuma_api.AutoModEnforcementName,
		MessageID:                   req.MessageId,
		CanonicalClientID:           dataScienceCommonProperties.Platform,
		DataScienceCommonProperties: dataScienceCommonProperties,
	}

	go func(e *models.BitsAutoModMessageEnforcementEventProperties) {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		dataScienceEvent.UserLogin = api.DataScienceTracker.GetUserLoginForDataScience(ctx, req.UserId)
		dataScienceEvent.Channel = api.DataScienceTracker.GetUserLoginForDataScience(ctx, req.ChannelId)

		err := api.DataScienceTracker.TrackEvent(ctx, models.BitsAutoModMessageEnforcementEventName, dataScienceEvent, req.UserId, req.ChannelId)
		if err != nil {
			log.WithError(err).WithField("AutoModOutcome", models.AutoModApproved).Error("Error tracking Bits Automod Message Enforcement event with data science")
		}
	}(dataScienceEvent)
}
