package process_message

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/audit"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend/clients/cloudwatchlogger"
	"code.justin.tv/commerce/prism/backend/clients/stats"
	dynamo_transaction "code.justin.tv/commerce/prism/backend/dynamo/transaction"
	"code.justin.tv/commerce/prism/backend/message/chat_error_handler"
	"code.justin.tv/commerce/prism/backend/transaction"
	"code.justin.tv/commerce/prism/backend/transaction/process"
	"code.justin.tv/commerce/prism/backend/utils"
	process_message_validator "code.justin.tv/commerce/prism/backend/validation/process_message"
	prism "code.justin.tv/commerce/prism/rpc"
	"github.com/twitchtv/twirp"
)

const (
	timeout = 30 * time.Second
)

// NewAPI creates a new API
func NewAPI() *API {
	return &API{}
}

// API is the ProcessMessage API struct
type API struct {
	ProcessMessageValidator process_message_validator.Validator `inject:""`
	TransactionProcessor    transaction.Processor               `inject:""`
	ChatErrorHandler        chat_error_handler.ChatErrorHandler `inject:""`
	Stats                   stats.Statter                       `inject:""`
	AuditLogger             cloudwatchlogger.CloudWatchLogger   `inject:""`
}

// ProcessMessage processes messages containing consumables
func (api *API) ProcessMessage(ctx context.Context, req *prism.ProcessMessageReq) (*prism.ProcessMessageResp, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	shouldProcess, err := api.TransactionProcessor.ShouldProcess(ctx, req)
	if err != nil {
		// Twirp errors are only returned from ShouldProcess for expected error scenarios (not 5XXs)
		// In these cases, the message is not sent, and we should return the twirp error
		if utils.IsTwirpError(err) {
			return &prism.ProcessMessageResp{ChatMessageSent: false}, err
		}

		log.WithField("request", req).WithError(err).Error("error while determining whether or not to process transaction")
		return nil, utils.DownstreamDependencyError
	}

	if !shouldProcess {
		log.WithField("request", req).WithField("channel_id", req.ChannelId).Error("decided not to process a transaction")
		return &prism.ProcessMessageResp{ChatMessageSent: false}, nil
	}

	// For SOX Compliance, when this block is reached we need to audit log the request
	auditLoggerTimer := time.Now()
	apiName := "ProcessMessage"
	msg, err := audit.CreateAccessLogMsg(ctx, apiName, req)
	if err != nil {
		log.WithError(err).Error(fmt.Sprintf("failed to generate access log msg in %s", apiName))
	} else {
		err = api.AuditLogger.Send(ctx, msg)
		if err != nil {
			log.WithError(err).Warn(fmt.Sprintf("error while sending audit log in %s", apiName))
		}
	}
	api.Stats.TimingDuration("ProcessMessage_AuditLogger", time.Since(auditLoggerTimer), 1)

	tx, err := api.TransactionProcessor.Start(ctx, req)
	if err != nil {
		if dynamo_transaction.IsConditionalCheckFailure(err) {
			log.WithField("request", req).WithError(err).Warn("conditional check failure while starting transaction")
			return nil, twirp.InvalidArgumentError("message_id", "message id is not unique")
		} else {
			log.WithField("request", req).WithError(err).Error("error starting transaction")
		}
		return nil, utils.DownstreamDependencyError
	}

	validationResp := api.TransactionProcessor.Validate(ctx, tx)
	if validationResp.DynamoUpdateErr != nil {
		log.WithField("transaction", tx).WithError(validationResp.DynamoUpdateErr).Error("error updating transaction in dynamo")
	}

	if validationResp.AutoModProcessed && validationResp.ValidationErr == nil {
		// User has consented to being automodded. Their request will be processed separately.
		return &prism.ProcessMessageResp{ChatMessageSent: false}, nil
	}

	if validationResp.ValidationErr != nil {
		// If the validation error is not a twirp error, make sure to return a twirp error
		twErr, ok := validationResp.ValidationErr.(twirp.Error)
		if !ok {
			log.WithField("transaction", tx).WithError(validationResp.ValidationErr).Error("non-twirp transaction validation error")
			return nil, utils.DownstreamDependencyError
		}

		// If the error is already a 4XX twirp error, return it
		return nil, twErr
	}

	processResp := api.TransactionProcessor.Process(ctx, tx)
	if processResp.DynamoUpdateErr != nil {
		log.WithField("transaction", tx).WithError(processResp.DynamoUpdateErr).Error("error updating transaction in dynamo")
	}

	if processResp.ProcessErr != nil {
		all4xxErrors := logProcessError(tx, processResp)
		if all4xxErrors {
			return nil, twirp.NewError(twirp.FailedPrecondition, "user error on process message call")
		} else {
			return nil, utils.DownstreamDependencyError
		}
	}

	sendToChatResp := api.TransactionProcessor.SendToChat(ctx, tx, processResp.Transformations)
	if sendToChatResp.DynamoUpdateErr != nil {
		log.WithField("transaction", tx).WithError(sendToChatResp.DynamoUpdateErr).Error("error updating transaction in dynamo")
	}

	if sendToChatResp.OverallErr != nil {
		log.WithField("transaction", tx).WithError(sendToChatResp.OverallErr).Error("error sending message to chat")
		return nil, utils.DownstreamDependencyError
	}

	if sendToChatResp.ChatSendErr != nil {
		log.WithField("transaction", tx).WithError(sendToChatResp.ChatSendErr).Error("error routing message")
		go api.ChatErrorHandler.HandleChatError(tx, sendToChatResp.ChatSendErr)
		return nil, twirp.InternalError("error sending message to chat")
	}

	postProcessResp := api.TransactionProcessor.PostProcess(ctx, tx)
	if postProcessResp.PostProcessErr != nil {
		log.WithField("transaction", tx).WithError(postProcessResp.PostProcessErr).Error("error post processing transaction")
		return nil, twirp.InternalError("error post processing message")
	}

	return &prism.ProcessMessageResp{ChatMessageSent: true, NewBalance: processResp.NewBalance}, nil
}

func logProcessError(tx *dynamo_transaction.Transaction, resp process.ProcessResp) bool {
	if resp.ProcessErr == utils.AllTenantsErroredDuringProcessCall {
		all4xxErrors := true
		for _, tenantErr := range resp.TenantProcessErrs {
			if utils.IsA5XXError(utils.ToTwirpError(tenantErr)) {
				all4xxErrors = false
				break
			}
		}

		if all4xxErrors {
			log.WithField("transaction", tx).WithError(resp.ProcessErr).Warn("error processing transaction - all tenants errored")
		} else {
			log.WithField("transaction", tx).WithError(resp.ProcessErr).Error("error processing transaction - all tenants errored")
		}
		return all4xxErrors
	} else {
		log.WithField("transaction", tx).WithError(resp.ProcessErr).Error("error processing transaction")
		return false
	}
}
