package process_message_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/prism/backend/api/process_message"
	"code.justin.tv/commerce/prism/backend/clients/cloudwatchlogger"
	"code.justin.tv/commerce/prism/backend/consumable"
	dyanmo_transaction "code.justin.tv/commerce/prism/backend/dynamo/transaction"
	"code.justin.tv/commerce/prism/backend/transaction/post_process"
	"code.justin.tv/commerce/prism/backend/transaction/process"
	"code.justin.tv/commerce/prism/backend/transaction/send_to_chat"
	"code.justin.tv/commerce/prism/backend/transaction/validate"
	"code.justin.tv/commerce/prism/backend/utils"
	"code.justin.tv/commerce/prism/config"
	stats_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/stats"
	chat_error_handler_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/message/chat_error_handler"
	transaction_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/transaction"
	prism "code.justin.tv/commerce/prism/rpc"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestAPI_ProcessMessage(t *testing.T) {
	Convey("Given an API", t, func() {
		processor := new(transaction_mock.Processor)
		chatErrorHandler := new(chat_error_handler_mock.ChatErrorHandler)
		stats := new(stats_mock.Statter)
		auditLogger := cloudwatchlogger.NewCloudWatchLogNoopClient()

		api := &process_message.API{
			TransactionProcessor: processor,
			ChatErrorHandler:     chatErrorHandler,
			Stats:                stats,
			AuditLogger:          auditLogger,
		}

		Convey("Errors when transactionProcessor errors on should process", func() {
			Convey("If it's a twirp error from throttling", func() {
				processor.On("ShouldProcess", mock.Anything, mock.Anything).Return(false, twirp.NewError(twirp.FailedPrecondition, "test error"))
				resp, err := api.ProcessMessage(context.Background(), &prism.ProcessMessageReq{})
				So(resp.ChatMessageSent, ShouldBeFalse)
				So(err, ShouldNotBeNil)
			})

			Convey("If it's any other kind of error", func() {
				processor.On("ShouldProcess", mock.Anything, mock.Anything).Return(false, errors.New("test error"))
				_, err := api.ProcessMessage(context.Background(), &prism.ProcessMessageReq{})
				So(err, ShouldNotBeNil)
			})
		})

		Convey("Returns early when transactionProcessor indicates should not process", func() {
			processor.On("ShouldProcess", mock.Anything, mock.Anything).Return(false, nil)
			resp, err := api.ProcessMessage(context.Background(), &prism.ProcessMessageReq{})
			So(err, ShouldBeNil)
			So(resp.ChatMessageSent, ShouldBeFalse)
		})

		Convey("When transactionProcessor indicates should process", func() {
			processor.On("ShouldProcess", mock.Anything, mock.Anything).Return(true, nil)
			stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return()

			Convey("Errors when transactionProcessor transaction start errors with a dynamo conditional error", func() {
				processor.On("Start", mock.Anything, mock.Anything).Return(nil, awserr.New(dynamodb.ErrCodeConditionalCheckFailedException, "conditional check failure", nil))
				_, err := api.ProcessMessage(context.Background(), &prism.ProcessMessageReq{})
				So(err, ShouldNotBeNil)
				So(utils.IsA5XXError(utils.ToTwirpError(err)), ShouldBeFalse)
			})

			Convey("Errors when transactionProcessor transaction start errors with a different error", func() {
				processor.On("Start", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))
				_, err := api.ProcessMessage(context.Background(), &prism.ProcessMessageReq{})
				So(err, ShouldNotBeNil)
				So(utils.IsA5XXError(utils.ToTwirpError(err)), ShouldBeTrue)
			})

			Convey("When transactionProcessor transaction start succeeds", func() {
				tx := &dyanmo_transaction.Transaction{TransactionID: "test-tx-id"}
				processor.On("Start", mock.Anything, mock.Anything).Return(tx, nil)

				Convey("Errors when transactionProcessor validate errors", func() {
					processor.On("Validate", mock.Anything, tx).Return(validate.ValidationResp{ValidationErr: errors.New("test error")})
					_, err := api.ProcessMessage(context.Background(), &prism.ProcessMessageReq{})
					So(err, ShouldNotBeNil)
				})

				Convey("When transactionProcessor validate succeeds", func() {
					processor.On("Validate", mock.Anything, tx).Return(validate.ValidationResp{})

					Convey("Errors when processor process errors", func() {
						processor.On("Process", mock.Anything, tx).Return(process.ProcessResp{ProcessErr: errors.New("test error")})
						_, err := api.ProcessMessage(context.Background(), &prism.ProcessMessageReq{})
						So(err, ShouldNotBeNil)
						twripErr := err.(twirp.Error)
						So(twripErr.Code(), ShouldEqual, twirp.Internal)
					})

					Convey("Errors when processor process returns all tenants errored during processing and they were 5xx", func() {
						processor.On("Process", mock.Anything, tx).Return(process.ProcessResp{
							ProcessErr: utils.AllTenantsErroredDuringProcessCall,
							TenantProcessErrs: map[config.TenantName]error{
								consumable.Tenant_Payday: errors.New("WALRUS STRIKE"),
							},
						})
						_, err := api.ProcessMessage(context.Background(), &prism.ProcessMessageReq{})
						So(err, ShouldNotBeNil)
						twripErr := err.(twirp.Error)
						So(twripErr.Code(), ShouldEqual, twirp.Internal)
					})

					Convey("Errors when processor process errors all tenants errored during processing but they were 4xx", func() {
						processor.On("Process", mock.Anything, tx).Return(process.ProcessResp{
							ProcessErr: utils.AllTenantsErroredDuringProcessCall,
							TenantProcessErrs: map[config.TenantName]error{
								consumable.Tenant_Payday: twirp.InvalidArgumentError("walrus", "was not chill enough"),
							},
						})
						_, err := api.ProcessMessage(context.Background(), &prism.ProcessMessageReq{})
						So(err, ShouldNotBeNil)
						twripErr := err.(twirp.Error)
						So(twripErr.Code(), ShouldEqual, twirp.FailedPrecondition)
					})

					Convey("When transactionProcessor process succeeds", func() {
						processor.On("Process", mock.Anything, tx).Return(process.ProcessResp{})

						Convey("Errors when transactionProcessor send to chat errors due to tenants", func() {
							processor.On("SendToChat", mock.Anything, tx, mock.Anything).Return(send_to_chat.SendToChatResp{OverallErr: errors.New("test error")})
							_, err := api.ProcessMessage(context.Background(), &prism.ProcessMessageReq{})
							So(err, ShouldNotBeNil)
						})

						Convey("Errors when transactionProcessor send to chat errors due to chat", func() {
							processor.On("SendToChat", mock.Anything, tx, mock.Anything).Return(send_to_chat.SendToChatResp{ChatSendErr: errors.New("test error")})
							chatErrorHandler.On("HandleChatError", mock.Anything, mock.Anything).Return()
							_, err := api.ProcessMessage(context.Background(), &prism.ProcessMessageReq{})
							So(err, ShouldNotBeNil)
						})

						Convey("When transactionProcessor send to chat succeeds", func() {
							processor.On("SendToChat", mock.Anything, tx, mock.Anything).Return(send_to_chat.SendToChatResp{})

							Convey("Errors when post process errors", func() {
								processor.On("PostProcess", mock.Anything, tx, mock.Anything).Return(post_process.PostProcessResp{
									PostProcessErr: errors.New("test error"),
								})
								_, err := api.ProcessMessage(context.Background(), &prism.ProcessMessageReq{})
								So(err, ShouldNotBeNil)
							})

							Convey("Succeeds when post process succeeds", func() {
								processor.On("PostProcess", mock.Anything, tx, mock.Anything).Return(post_process.PostProcessResp{
									PostProcessErr: nil,
								})

								resp, err := api.ProcessMessage(context.Background(), &prism.ProcessMessageReq{})
								So(err, ShouldBeNil)
								So(resp.ChatMessageSent, ShouldBeTrue)
							})
						})
					})
				})
			})
		})
	})
}
