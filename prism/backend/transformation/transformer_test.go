package transformation_test

import (
	"testing"

	"code.justin.tv/commerce/prism/backend/transformation"
	prism "code.justin.tv/commerce/prism/rpc"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestTransform(t *testing.T) {
	Convey("Given a transformer", t, func() {
		transformer := transformation.NewTransformer()

		Convey("Errors on a nil transformation", func() {
			_, err := transformer.Transform("test msg", nil)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors on an invalid transformation type", func() {
			_, err := transformer.Transform("test msg", &prism.Transformation{
				Type: prism.TransformationType(-1),
			})
			So(err, ShouldNotBeNil)
		})

		Convey("Succeeds on valid APPEND_TOKEN_TO_END_IF_NOT_ALREADY_PRESENT transformation", func() {
			newMsg, err := transformer.Transform("cheer100", prism_models.AppendTokenToEndIfNotAlreadyPresentTransformation("#charity"))
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "cheer100 #charity")
		})

		Convey("Succeeds on valid APPEND_TOKEN_AFTER_LAST_OCCURRENCE_OF_CHEERMOTE transformation", func() {
			newMsg, err := transformer.Transform("Doritos50 CRUNCH TIME Doritos50", prism_models.AppendTokenAfterLastOccurrenceOfCheermoteTransformation("Bonus10", "Doritos"))
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "Doritos50 CRUNCH TIME Doritos50 Bonus10")
		})

		Convey("Succeeds on valid FIND_AND_REPLACE_TOKEN transformation", func() {
			newMsg, err := transformer.Transform("This is a sticker: MySticker", prism_models.FindAndReplaceTokenTransformation("MySticker", "MyStickerCode"))
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "This is a sticker: MyStickerCode")
		})
	})
}
