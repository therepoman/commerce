package append_token_after_last_occurrence_of_cheermote_test

import (
	"testing"

	"code.justin.tv/commerce/prism/backend/transformation/append_token_after_last_occurrence_of_cheermote"
	prism "code.justin.tv/commerce/prism/rpc/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestTransform(t *testing.T) {
	Convey("Test various cases", t, func() {
		Convey("Errors when missing TokenToAppend arg", func() {
			_, err := append_token_after_last_occurrence_of_cheermote.Transform("", map[string]string{})
			So(err, ShouldNotBeNil)
		})
		Convey("Errors when TokenToAppend arg is blank", func() {
			_, err := append_token_after_last_occurrence_of_cheermote.Transform("", map[string]string{
				prism.TransformationArg_TokenToAppend: " ",
			})
			So(err, ShouldNotBeNil)
		})
		Convey("Errors when CheermotePrefix arg is blank", func() {
			_, err := append_token_after_last_occurrence_of_cheermote.Transform("", map[string]string{
				prism.TransformationArg_TokenToAppend:   "Bonus100",
				prism.TransformationArg_CheermotePrefix: "",
			})
			So(err, ShouldNotBeNil)
		})
		Convey("Inserts correctly when only one cheermote and nothing else", func() {
			newMsg, err := append_token_after_last_occurrence_of_cheermote.Transform("DoRiTos100", map[string]string{
				prism.TransformationArg_TokenToAppend:   "Bonus100",
				prism.TransformationArg_CheermotePrefix: "Doritos",
			})
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "DoRiTos100 Bonus100")
		})

		Convey("Inserts correctly when only one cheermote at the start", func() {
			newMsg, err := append_token_after_last_occurrence_of_cheermote.Transform("DORITOS100 some msg", map[string]string{
				prism.TransformationArg_TokenToAppend:   "Bonus100",
				prism.TransformationArg_CheermotePrefix: "Doritos",
			})
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "DORITOS100 Bonus100 some msg")
		})

		Convey("Inserts correctly when only one cheermote in the middle", func() {
			newMsg, err := append_token_after_last_occurrence_of_cheermote.Transform("some doritos100 msg", map[string]string{
				prism.TransformationArg_TokenToAppend:   "Bonus100",
				prism.TransformationArg_CheermotePrefix: "Doritos",
			})
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "some doritos100 Bonus100 msg")
		})

		Convey("Inserts correctly when only one cheermote at the end", func() {
			newMsg, err := append_token_after_last_occurrence_of_cheermote.Transform("some msg doritos100", map[string]string{
				prism.TransformationArg_TokenToAppend:   "Bonus100",
				prism.TransformationArg_CheermotePrefix: "Doritos",
			})
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "some msg doritos100 Bonus100")
		})

		Convey("Inserts correctly when there are multiple cheermotes at the start", func() {
			newMsg, err := append_token_after_last_occurrence_of_cheermote.Transform("   doritos100    doritos1000  doRITos99   some msg", map[string]string{
				prism.TransformationArg_TokenToAppend:   "Bonus100",
				prism.TransformationArg_CheermotePrefix: "Doritos",
			})
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "   doritos100    doritos1000  doRITos99 Bonus100   some msg")
		})

		Convey("Inserts correctly when there are multiple cheermotes in the middle", func() {
			newMsg, err := append_token_after_last_occurrence_of_cheermote.Transform("some  doriTOS9999    dorItos1  DORitos420  msg", map[string]string{
				prism.TransformationArg_TokenToAppend:   "Bonus100",
				prism.TransformationArg_CheermotePrefix: "Doritos",
			})
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "some  doriTOS9999    dorItos1  DORitos420 Bonus100  msg")
		})

		Convey("Inserts correctly when there are multiple cheermotes at the end", func() {
			newMsg, err := append_token_after_last_occurrence_of_cheermote.Transform("some  msg doriTOS9999    dorItos1  DORitos420 ", map[string]string{
				prism.TransformationArg_TokenToAppend:   "Bonus100",
				prism.TransformationArg_CheermotePrefix: "Doritos",
			})
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "some  msg doriTOS9999    dorItos1  DORitos420 Bonus100 ")
		})

		Convey("Inserts correctly when there are multiple cheermotes spread across the message", func() {
			newMsg, err := append_token_after_last_occurrence_of_cheermote.Transform("Doritos1 some Doritos2 msg Doritos3 FakeDoritos100", map[string]string{
				prism.TransformationArg_TokenToAppend:   "Bonus100",
				prism.TransformationArg_CheermotePrefix: "Doritos",
			})
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "Doritos1 some Doritos2 msg Doritos3 Bonus100 FakeDoritos100")
		})

		Convey("Inserts correctly when there is one cheermote", func() {
			newMsg, err := append_token_after_last_occurrence_of_cheermote.Transform("Gamefuel1", map[string]string{
				prism.TransformationArg_TokenToAppend:   "Bonus1",
				prism.TransformationArg_CheermotePrefix: "Gamefuel",
			})
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "Gamefuel1 Bonus1")
		})

		Convey("Inserts correctly when there are two cheermotes ", func() {
			newMsg, err := append_token_after_last_occurrence_of_cheermote.Transform("Gamefuel1 Gamefuel1", map[string]string{
				prism.TransformationArg_TokenToAppend:   "Bonus2",
				prism.TransformationArg_CheermotePrefix: "Gamefuel",
			})
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "Gamefuel1 Gamefuel1 Bonus2")
		})

		Convey("Inserts correctly when there are three cheermotes ", func() {
			newMsg, err := append_token_after_last_occurrence_of_cheermote.Transform("Gamefuel1 Gamefuel1 Gamefuel1", map[string]string{
				prism.TransformationArg_TokenToAppend:   "Bonus3",
				prism.TransformationArg_CheermotePrefix: "Gamefuel",
			})
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "Gamefuel1 Gamefuel1 Gamefuel1 Bonus3")
		})

		Convey("Inserts correctly when there are four cheermotes ", func() {
			newMsg, err := append_token_after_last_occurrence_of_cheermote.Transform("Gamefuel1 Gamefuel1 Gamefuel1 Gamefuel1", map[string]string{
				prism.TransformationArg_TokenToAppend:   "Bonus4",
				prism.TransformationArg_CheermotePrefix: "Gamefuel",
			})
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "Gamefuel1 Gamefuel1 Gamefuel1 Gamefuel1 Bonus4")
		})

		Convey("Inserts correctly when there is a cheermote prefix leading into something else", func() {
			newMsg, err := append_token_after_last_occurrence_of_cheermote.Transform("Gamefuel1 Gamefuel1 Gamefuel1Nopejustkidding", map[string]string{
				prism.TransformationArg_TokenToAppend:   "Bonus2",
				prism.TransformationArg_CheermotePrefix: "Gamefuel",
			})
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "Gamefuel1 Gamefuel1 Bonus2 Gamefuel1Nopejustkidding")
		})
	})
}
