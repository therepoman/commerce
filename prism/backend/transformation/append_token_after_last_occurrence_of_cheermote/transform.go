package append_token_after_last_occurrence_of_cheermote

import (
	"errors"
	"fmt"
	"regexp"

	string_utils "code.justin.tv/commerce/gogogadget/strings"
	prism "code.justin.tv/commerce/prism/rpc/models"
)

const (
	cheermoteRegexTemplate = `(?i)(^|\s)%s\d+\b`
)

// Transform appends a token after the last occurrence of a specific cheermote
func Transform(originalMsg string, args map[string]string) (newMsg string, err error) {
	tokenToAppend, ok := args[prism.TransformationArg_TokenToAppend]
	if !ok || string_utils.Blank(tokenToAppend) {
		return "", errors.New("missing required argument: tokenToAppend")
	}

	cheermotePrefix, ok := args[prism.TransformationArg_CheermotePrefix]
	if !ok || string_utils.Blank(cheermotePrefix) {
		return "", errors.New("missing required argument: cheermotePrefix")
	}

	regex, err := regexp.Compile(fmt.Sprintf(cheermoteRegexTemplate, cheermotePrefix))
	if err != nil {
		return "", errors.New("cheermote regex did not compile")
	}

	originalMsgBytes := []byte(originalMsg)
	matches := regex.FindAllIndex(originalMsgBytes, -1)
	if len(matches) < 1 {
		return "", errors.New("no matches found")
	}

	lastMatchIndex := matches[len(matches)-1]
	if len(lastMatchIndex) != 2 {
		return "", errors.New("match index not in expected format")
	}
	lastMatchEndIndex := lastMatchIndex[1]

	// If the regex matched a final space character, we ignore it
	endCharacter := originalMsg[lastMatchEndIndex-1 : lastMatchEndIndex]
	if string_utils.Blank(endCharacter) {
		lastMatchEndIndex--
	}

	toInsert := []byte(fmt.Sprintf(" %s", tokenToAppend))
	before := originalMsgBytes[0:lastMatchEndIndex]
	after := originalMsgBytes[lastMatchEndIndex:]

	newMsgBytes := make([]byte, 0)
	newMsgBytes = append(newMsgBytes, before...)
	newMsgBytes = append(newMsgBytes, toInsert...)
	newMsgBytes = append(newMsgBytes, after...)
	return string(newMsgBytes), nil
}
