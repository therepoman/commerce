package find_and_replace_token_test

import (
	"testing"

	"code.justin.tv/commerce/prism/backend/transformation/find_and_replace_token"
	prism "code.justin.tv/commerce/prism/rpc/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestTransform(t *testing.T) {
	Convey("Test various cases", t, func() {
		Convey("Errors when missing TokenToFind arg", func() {
			_, err := find_and_replace_token.Transform("", map[string]string{})
			So(err, ShouldNotBeNil)
		})
		Convey("Errors when TokenToFind arg is blank", func() {
			_, err := find_and_replace_token.Transform("", map[string]string{
				prism.TransformationArg_TokenToFind: " ",
			})
			So(err, ShouldNotBeNil)
		})
		Convey("Errors when missing TokenToReplace arg", func() {
			_, err := find_and_replace_token.Transform("", map[string]string{
				prism.TransformationArg_TokenToFind: "a",
			})
			So(err, ShouldNotBeNil)
		})
		Convey("Errors when TokenToReplace arg is blank", func() {
			_, err := find_and_replace_token.Transform("", map[string]string{
				prism.TransformationArg_TokenToFind:    "a",
				prism.TransformationArg_TokenToReplace: " ",
			})
			So(err, ShouldNotBeNil)
		})
		Convey("Doesn't replace when the doesn't match", func() {
			newMsg, err := find_and_replace_token.Transform("somesticker but not in the right case", map[string]string{
				prism.TransformationArg_TokenToFind:    "SomeSticker",
				prism.TransformationArg_TokenToReplace: "SomePermStickerCode",
			})
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "somesticker but not in the right case")
		})
		Convey("Extra spaces get removed", func() {
			newMsg, err := find_and_replace_token.Transform(" a    b  SomeSticker     c  d   SomeSticker   e    f    ", map[string]string{
				prism.TransformationArg_TokenToFind:    "SomeSticker",
				prism.TransformationArg_TokenToReplace: "SomePermStickerCode",
			})
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "a b SomePermStickerCode c d SomePermStickerCode e f")
		})
		Convey("Happy case", func() {
			newMsg, err := find_and_replace_token.Transform("Look at me about to use a sticker! SomeSticker <- That's the sticker!", map[string]string{
				prism.TransformationArg_TokenToFind:    "SomeSticker",
				prism.TransformationArg_TokenToReplace: "SomePermStickerCode",
			})
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "Look at me about to use a sticker! SomePermStickerCode <- That's the sticker!")
		})
	})
}
