package find_and_replace_token

import (
	"errors"
	"strings"

	string_utils "code.justin.tv/commerce/gogogadget/strings"
	prism "code.justin.tv/commerce/prism/rpc/models"
)

// Transform finds and replaces a token in a message
func Transform(originalMsg string, args map[string]string) (newMsg string, err error) {
	tokenToFind, ok := args[prism.TransformationArg_TokenToFind]
	if !ok || string_utils.Blank(tokenToFind) {
		return "", errors.New("missing required argument: tokenToFind")
	}

	tokenToReplace, ok := args[prism.TransformationArg_TokenToReplace]
	if !ok || string_utils.Blank(tokenToReplace) {
		return "", errors.New("missing required argument: tokenToReplace")
	}

	for _, token := range strings.Fields(originalMsg) {
		if token == tokenToFind {
			token = tokenToReplace
		}
		if len(newMsg) > 0 {
			newMsg += " "
		}
		newMsg += token
	}
	return newMsg, nil
}
