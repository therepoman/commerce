package remove_tokens_except_test

import (
	"testing"

	"code.justin.tv/commerce/prism/backend/transformation/remove_tokens_except"
	prism "code.justin.tv/commerce/prism/rpc/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestTransform(t *testing.T) {
	Convey("Test various cases", t, func() {
		Convey("Errors when missing TokensToKeep arg", func() {
			_, err := remove_tokens_except.Transform("", map[string]string{})
			So(err, ShouldNotBeNil)
		})
		Convey("Errors when TokensToKeep arg is blank", func() {
			_, err := remove_tokens_except.Transform("", map[string]string{
				prism.TransformationArg_TokensToKeep: " ",
			})
			So(err, ShouldNotBeNil)
		})
		Convey("Errors when TokensToKeep is invalid JSON", func() {
			_, err := remove_tokens_except.Transform("", map[string]string{
				prism.TransformationArg_TokensToKeep: "[\"cheer100\"",
			})
			So(err, ShouldNotBeNil)
		})
		Convey("Does not remove anything when all tokens should be kept", func() {
			newMsg, err := remove_tokens_except.Transform("cheer100 cheer200", map[string]string{
				prism.TransformationArg_TokensToKeep: "[\"cheer100\",\"cheer200\"]",
			})
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "cheer100 cheer200")
		})
		Convey("Extra spaces get removed", func() {
			newMsg, err := remove_tokens_except.Transform(" cheer100   i      am      voldemort         cheer200   ", map[string]string{
				prism.TransformationArg_TokensToKeep: "[\"cheer100\",\"cheer200\"]",
			})
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "cheer100 cheer200")
		})
		Convey("Happy case", func() {
			newMsg, err := remove_tokens_except.Transform("cheer100 sephBigVoldemort fuck em up dark lord sephBigVoldemort", map[string]string{
				prism.TransformationArg_TokensToKeep: "[\"cheer100\",\"sephBigVoldemort\"]",
			})
			So(err, ShouldBeNil)
			So(newMsg, ShouldEqual, "cheer100 sephBigVoldemort sephBigVoldemort")
		})
	})
}
