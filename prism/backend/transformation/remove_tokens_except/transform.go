package remove_tokens_except

import (
	"encoding/json"
	"errors"
	"strings"

	string_utils "code.justin.tv/commerce/gogogadget/strings"
	prism "code.justin.tv/commerce/prism/rpc/models"
)

// Transform removes all tokens excepts a list of specified tokens
func Transform(originalMsg string, args map[string]string) (newMsg string, err error) {
	tokensToKeepJSON, ok := args[prism.TransformationArg_TokensToKeep]
	if !ok || string_utils.Blank(tokensToKeepJSON) {
		return "", errors.New("missing required argument: tokensToKeep")
	}

	var tokensToKeep []string
	err = json.Unmarshal([]byte(tokensToKeepJSON), &tokensToKeep)
	if err != nil {
		return "", errors.New("failed to unmarshal tokens to keep")
	}

	tokensToKeepMap := make(map[string]interface{})
	for _, token := range tokensToKeep {
		tokensToKeepMap[strings.ToLower(token)] = nil
	}

	for _, token := range strings.Fields(originalMsg) {
		if _, ok := tokensToKeepMap[strings.ToLower(token)]; ok {
			if len(newMsg) > 0 {
				newMsg += " "
			}
			newMsg += token
		}
	}

	return newMsg, nil
}
