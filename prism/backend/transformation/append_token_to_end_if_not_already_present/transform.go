package append_token_to_end_if_not_already_present

import (
	"errors"
	"fmt"
	"strings"

	string_utils "code.justin.tv/commerce/gogogadget/strings"
	prism "code.justin.tv/commerce/prism/rpc/models"
)

// Transform appends a token to the end of a message if it is not already present
func Transform(originalMsg string, args map[string]string) (newMsg string, err error) {
	tokenToAppend, ok := args[prism.TransformationArg_TokenToAppend]
	if !ok || string_utils.Blank(tokenToAppend) {
		return "", errors.New("missing required argument: tokenToAppend")
	}

	if strings.Contains(originalMsg, tokenToAppend) {
		return originalMsg, nil
	}

	return fmt.Sprintf("%s %s", strings.TrimSpace(originalMsg), tokenToAppend), nil
}
