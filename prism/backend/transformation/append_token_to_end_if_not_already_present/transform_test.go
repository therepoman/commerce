package append_token_to_end_if_not_already_present_test

import (
	"testing"

	"code.justin.tv/commerce/prism/backend/transformation/append_token_to_end_if_not_already_present"
	prism "code.justin.tv/commerce/prism/rpc/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestTransform(t *testing.T) {
	Convey("Test various cases", t, func() {
		Convey("Errors when missing TokenToAppend arg", func() {
			_, err := append_token_to_end_if_not_already_present.Transform("", map[string]string{})
			So(err, ShouldNotBeNil)
		})
		Convey("Errors when TokenToAppend arg is blank", func() {
			_, err := append_token_to_end_if_not_already_present.Transform("", map[string]string{
				prism.TransformationArg_TokenToAppend: " ",
			})
			So(err, ShouldNotBeNil)
		})
		Convey("Does not change msg when token is already present at the start", func() {
			msg := "#charity some msg"
			newMsg, err := append_token_to_end_if_not_already_present.Transform(msg, map[string]string{
				prism.TransformationArg_TokenToAppend: "#charity",
			})
			So(newMsg, ShouldEqual, msg)
			So(err, ShouldBeNil)
		})
		Convey("Does not change msg when token is already present in the middle", func() {
			msg := "some #charity msg"
			newMsg, err := append_token_to_end_if_not_already_present.Transform(msg, map[string]string{
				prism.TransformationArg_TokenToAppend: "#charity",
			})
			So(newMsg, ShouldEqual, msg)
			So(err, ShouldBeNil)
		})
		Convey("Does not change msg when token is already present at the end", func() {
			msg := "some msg #charity"
			newMsg, err := append_token_to_end_if_not_already_present.Transform(msg, map[string]string{
				prism.TransformationArg_TokenToAppend: "#charity",
			})
			So(newMsg, ShouldEqual, msg)
			So(err, ShouldBeNil)
		})
		Convey("Appends token when missing and ends in a space", func() {
			msg := "some msg "
			newMsg, err := append_token_to_end_if_not_already_present.Transform(msg, map[string]string{
				prism.TransformationArg_TokenToAppend: "#charity",
			})
			So(newMsg, ShouldEqual, "some msg #charity")
			So(err, ShouldBeNil)
		})
		Convey("Appends token when missing and ends without a space", func() {
			msg := "some msg"
			newMsg, err := append_token_to_end_if_not_already_present.Transform(msg, map[string]string{
				prism.TransformationArg_TokenToAppend: "#charity",
			})
			So(newMsg, ShouldEqual, "some msg #charity")
			So(err, ShouldBeNil)
		})
		Convey("Trims extra spaces", func() {
			msg := "   some msg   "
			newMsg, err := append_token_to_end_if_not_already_present.Transform(msg, map[string]string{
				prism.TransformationArg_TokenToAppend: "#charity",
			})
			So(newMsg, ShouldEqual, "some msg #charity")
			So(err, ShouldBeNil)
		})
	})
}
