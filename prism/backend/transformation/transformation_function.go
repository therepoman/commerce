package transformation

type TransformationFunction func(originalMsg string, args map[string]string) (newMsg string, err error)
