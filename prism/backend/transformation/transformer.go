package transformation

import (
	"errors"

	"code.justin.tv/commerce/prism/backend/transformation/append_token_after_last_occurrence_of_cheermote"
	"code.justin.tv/commerce/prism/backend/transformation/append_token_to_end_if_not_already_present"
	"code.justin.tv/commerce/prism/backend/transformation/find_and_replace_token"
	"code.justin.tv/commerce/prism/backend/transformation/remove_tokens_except"
	prism "code.justin.tv/commerce/prism/rpc"
)

// Transformer represents a chat message transformer
type Transformer interface {
	Transform(originalMsg string, transformation *prism.Transformation) (newMsg string, err error)
}

type transformer struct {
}

// NewTransformer creates a new chat message Transformer
func NewTransformer() Transformer {
	return &transformer{}
}

func (t *transformer) Transform(originalMsg string, transformation *prism.Transformation) (newMsg string, err error) {
	if transformation == nil {
		return "", errors.New("cannot have a nil transformation")
	}

	_, ok := prism.TransformationType_name[int32(transformation.Type)]
	if !ok {
		return "", errors.New("invalid transformation type")
	}

	var transformationFunction TransformationFunction
	switch transformation.Type {
	case prism.TransformationType_APPEND_TOKEN_TO_END_IF_NOT_ALREADY_PRESENT:
		transformationFunction = append_token_to_end_if_not_already_present.Transform
	case prism.TransformationType_APPEND_TOKEN_AFTER_LAST_OCCURRENCE_OF_CHEERMOTE:
		transformationFunction = append_token_after_last_occurrence_of_cheermote.Transform
	case prism.TransformationType_FIND_AND_REPLACE_TOKEN:
		transformationFunction = find_and_replace_token.Transform
	case prism.TransformationType_REMOVE_TOKENS_EXCEPT:
		transformationFunction = remove_tokens_except.Transform
	default:
		return "", errors.New("unsupported transformation type")
	}
	return transformationFunction(originalMsg, transformation.Arguments)
}
