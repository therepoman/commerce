package backend

import (
	"context"
	"io"
	"net/http"
	"time"

	"code.justin.tv/amzn/TwitchSafetyAutoModTwirp"
	"code.justin.tv/chat/chatrooms/rpc/chatrooms"
	zuma "code.justin.tv/chat/zuma/client"
	"code.justin.tv/commerce/gogogadget/aws/s3"
	sqs_worker "code.justin.tv/commerce/gogogadget/aws/sqs/worker"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/utils/strings"
	"code.justin.tv/commerce/prism/backend/anonymous_message"
	"code.justin.tv/commerce/prism/backend/api/approve_automod_message"
	"code.justin.tv/commerce/prism/backend/api/deny_automod_message"
	"code.justin.tv/commerce/prism/backend/api/process_message"
	"code.justin.tv/commerce/prism/backend/automod"
	automod_cache "code.justin.tv/commerce/prism/backend/cache/automod"
	usersservice_cache "code.justin.tv/commerce/prism/backend/cache/usersservice"
	"code.justin.tv/commerce/prism/backend/clients/cloudwatchlogger"
	"code.justin.tv/commerce/prism/backend/clients/payday"
	"code.justin.tv/commerce/prism/backend/clients/redis"
	"code.justin.tv/commerce/prism/backend/clients/rooms"
	"code.justin.tv/commerce/prism/backend/clients/sandstorm"
	"code.justin.tv/commerce/prism/backend/clients/slack"
	"code.justin.tv/commerce/prism/backend/clients/sns"
	"code.justin.tv/commerce/prism/backend/clients/spade"
	"code.justin.tv/commerce/prism/backend/clients/stats"
	"code.justin.tv/commerce/prism/backend/clients/tmi"
	"code.justin.tv/commerce/prism/backend/clients/wrapper"
	"code.justin.tv/commerce/prism/backend/consumable"
	"code.justin.tv/commerce/prism/backend/datascience"
	transaction_dynamo "code.justin.tv/commerce/prism/backend/dynamo/transaction"
	eventbusworker "code.justin.tv/commerce/prism/backend/eventbus"
	"code.justin.tv/commerce/prism/backend/filter"
	"code.justin.tv/commerce/prism/backend/landlord"
	"code.justin.tv/commerce/prism/backend/message"
	"code.justin.tv/commerce/prism/backend/message/chat_error_handler"
	"code.justin.tv/commerce/prism/backend/server"
	"code.justin.tv/commerce/prism/backend/tenant"
	"code.justin.tv/commerce/prism/backend/throttle"
	"code.justin.tv/commerce/prism/backend/transaction"
	"code.justin.tv/commerce/prism/backend/transaction/post_process"
	"code.justin.tv/commerce/prism/backend/transaction/process"
	"code.justin.tv/commerce/prism/backend/transaction/send_to_chat"
	"code.justin.tv/commerce/prism/backend/transaction/should_process"
	"code.justin.tv/commerce/prism/backend/transaction/start"
	"code.justin.tv/commerce/prism/backend/transaction/validate"
	"code.justin.tv/commerce/prism/backend/transaction/validation"
	"code.justin.tv/commerce/prism/backend/transformation"
	"code.justin.tv/commerce/prism/backend/usersservice"
	approve_automod_message_validator "code.justin.tv/commerce/prism/backend/validation/approve_automod_message"
	deny_automod_message_validator "code.justin.tv/commerce/prism/backend/validation/deny_automod_message"
	process_message_validator "code.justin.tv/commerce/prism/backend/validation/process_message"
	"code.justin.tv/commerce/prism/backend/worker"
	"code.justin.tv/commerce/prism/backend/worker/automod_timeout"
	"code.justin.tv/commerce/prism/config"
	prism "code.justin.tv/commerce/prism/rpc"
	"code.justin.tv/commerce/splatter"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/client/subscriber/sqsclient"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/web/users-service/client/usersclient_internal"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/facebookgo/inject"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
)

const (
	localhostServerEndpoint    = "http://localhost:8000"
	DefaultNumberOfConnections = 200
)

// Backend represents the server backend
type Backend interface {
	Ping(w http.ResponseWriter, r *http.Request)
	TwirpServer() prism.Prism
	Statter() statsd.Statter
	Shutdown(ctx context.Context) *errgroup.Group
}

type backend struct {
	Server prism.Prism `inject:""`

	// Config
	Config *config.Config `inject:""`

	// Stats
	Stats statsd.Statter `inject:""`

	// SQS Client
	SQS sqsiface.SQSAPI `inject:""`

	// SQS Workers
	AutoModTimeoutWorker *automod_timeout.Worker `inject:""`

	EventBusSQSClient *sqsclient.SQSClient

	// Audit Logger
	AuditLogger cloudwatchlogger.CloudWatchLogger `inject:""`

	// Worker Terminators
	terminators []worker.NamedTerminator
}

// NewBackend creates a new server backend
func NewBackend(cfg *config.Config) (Backend, error) {
	b := &backend{}

	if cfg == nil {
		return nil, errors.New("received nil config")
	}

	statters := make([]statsd.Statter, 0)

	if cfg.CloudwatchMetricsConfig != nil {
		cloudwatchStatter := splatter.NewBufferedTelemetryCloudWatchStatter(&splatter.BufferedTelemetryConfig{
			AWSRegion:         cfg.AWSRegion,
			ServiceName:       "prism",
			Stage:             cfg.EnvironmentName,
			FlushPeriod:       30 * time.Second,
			BufferSize:        cfg.CloudwatchMetricsConfig.BufferSize,
			AggregationPeriod: time.Minute,
			Substage:          "primary",
		}, map[string]bool{})

		statters = append(statters, cloudwatchStatter)
	} else {
		log.Warn("Config file does not contain cloudwatch metric configs")
	}

	statsdStatter, err := splatter.NewCompositeStatter(statters)
	if err != nil {
		return nil, err
	}

	statter := stats.NewStatter()

	awsConfig := &aws.Config{
		Region: aws.String(cfg.AWSRegion),
	}
	sess, err := session.NewSession(awsConfig)
	if err != nil {
		return nil, err
	}

	sandstormClient, err := sandstorm.New(awsConfig, sess, cfg)
	if err != nil {
		return nil, errors.Wrap(err, "failed to initiate sandstorm client")
	}

	chatFailureSlackWebhookURL, err := sandstormClient.GetSecret(cfg.ChatFailureSlackWebHookSecretName)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get secret from sandstorm client")
	}
	if chatFailureSlackWebhookURL == "" {
		return nil, errors.Wrap(err, "got empty string as secret from sandstorm client")
	}
	cfg.ChatFailureSlackWebhookURL = chatFailureSlackWebhookURL

	clientHTTPTransport := twitchclient.TransportConf{
		MaxIdleConnsPerHost: DefaultNumberOfConnections,
	}

	zumaClient, err := zuma.NewClient(twitchclient.ClientConf{
		Host:      cfg.Zuma.Endpoint,
		Transport: clientHTTPTransport,
		Stats:     statsdStatter,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewHystrixRoundTripWrapper("zuma", cfg.Zuma.TimeoutMilliseconds, statter),
		},
	})
	if err != nil {
		return nil, err
	}

	chatroomsClient := chatrooms.NewChatroomsProtobufClient(cfg.Rooms.Endpoint, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Host:  cfg.Rooms.Endpoint,
		Stats: statsdStatter,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewHystrixRoundTripWrapper("rooms", cfg.Rooms.TimeoutMilliseconds, statter),
		},
	}))

	usersServiceClient, err := usersclient_internal.NewClient(twitchclient.ClientConf{
		Host:  cfg.UsersService.Endpoint,
		Stats: statsdStatter,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewHystrixRoundTripWrapper("users", cfg.UsersService.TimeoutMilliseconds, statter),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to init users client")
	}

	automodClientConf := twitchclient.ClientConf{
		Host:  cfg.AutoMod.Endpoint,
		Stats: statsdStatter,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewHystrixRoundTripWrapper("automod", cfg.AutoMod.TimeoutMilliseconds, statter),
		},
	}
	automodClient := TwitchSafetyAutoModTwirp.NewTwitchSafetyAutoModProtobufClient(
		cfg.AutoMod.Endpoint,
		twitchclient.NewHTTPClient(automodClientConf),
	)

	spadeClient, err := spade.NewClient(cfg, statsdStatter)
	if err != nil {
		return nil, err
	}

	// SQS
	sqsClient := sqs.New(sess, aws.NewConfig().WithRegion(cfg.AWSRegion))

	// Filters
	// Our whitelist for all of Prism is currently in the GQL layer. Uncomment below for fine-grained filtering
	// whitelistedChannelIDFilter := channel_whitelist.NewDefaultChannelWhitelistFilter()

	// Audit Logging for SOX Compliance
	cwloggerClient := cloudwatchlogger.NewCloudWatchLogNoopClient()
	if strings.NotBlank(cfg.AuditLogGroup) {
		cwloggerClient, err = cloudwatchlogger.NewCloudWatchLogClient(cfg.AWSRegion, cfg.AuditLogGroup)
		if err != nil {
			return nil, err
		}
	}

	var graph inject.Graph
	err = graph.Provide(
		// Backend (graph root)
		&inject.Object{Value: b},

		// Config
		&inject.Object{Value: cfg},

		// RPC Server
		&inject.Object{Value: server.NewServer()},

		// APIs
		&inject.Object{Value: process_message.NewAPI()},
		&inject.Object{Value: approve_automod_message.NewAPI()},
		&inject.Object{Value: deny_automod_message.NewAPI()},

		// Metrics
		&inject.Object{Value: statsdStatter},

		// Clients
		&inject.Object{Value: tmi.NewHttpTMIClient(cfg.TMI.Endpoint)}, // TODO: Migrate off this deprecated TMI client that was copied from payday
		&inject.Object{Value: automodClient},
		&inject.Object{Value: chatroomsClient},
		&inject.Object{Value: rooms.NewRoomsClient(chatroomsClient)},
		&inject.Object{Value: redis.NewClient(cfg)},
		&inject.Object{Value: sns.NewClient(sess, cfg)},
		&inject.Object{Value: sqsClient},
		&inject.Object{Value: zumaClient},
		&inject.Object{Value: usersServiceClient},
		&inject.Object{Value: spadeClient},
		&inject.Object{Value: statter},
		&inject.Object{Value: s3.NewDefaultClient()},
		&inject.Object{Value: cwloggerClient},
		&inject.Object{Value: payday.NewPaydayClient(cfg.Payday.Endpoint)},

		// DAOs
		&inject.Object{Value: transaction_dynamo.NewDAO(sess, cfg.DynamoSuffix)},

		// Request Validation
		&inject.Object{Value: process_message_validator.NewValidator()},
		&inject.Object{Value: approve_automod_message_validator.NewValidator()},
		&inject.Object{Value: deny_automod_message_validator.NewValidator()},

		// Transaction Processor
		&inject.Object{Value: transaction.NewProcessor()},
		&inject.Object{Name: "shoulder", Value: should_process.NewShoulder()},
		&inject.Object{Name: "starter", Value: start.NewStarter()},
		&inject.Object{Name: "validator", Value: validate.NewValidator()},
		&inject.Object{Name: "processor", Value: process.NewProcessor()},
		&inject.Object{Name: "sender", Value: send_to_chat.NewSender()},
		&inject.Object{Name: "post_processor", Value: post_process.NewPostProcessor()},

		// Transformer
		&inject.Object{Value: transformation.NewTransformer()},

		// Tenants
		&inject.Object{Value: landlord.NewLandlord()},
		&inject.Object{Value: tenant.NewMapper()},

		// Consumables
		&inject.Object{Value: consumable.NewMapper()},

		// Message Validation
		&inject.Object{Value: validation.NewZumaValidator()},
		&inject.Object{Value: automod.NewProcessor()},
		&inject.Object{Value: anonymous_message.NewAnonymousMessageErrorHandler()},

		// Filters
		&inject.Object{Value: filter.NewFilterer()},
		&inject.Object{
			Name:  "filters",
			Value: []filter.Filter{},
		},

		// Throttlers
		&inject.Object{Value: throttle.NewThrottler()},

		// SQS workers
		&inject.Object{Value: &automod_timeout.Worker{}},

		// Router
		&inject.Object{Value: message.NewRouter()},

		// Caches
		&inject.Object{Value: automod_cache.NewCache()},
		&inject.Object{Value: usersservice_cache.NewCache()},

		// Users Service Fetcher
		&inject.Object{Value: usersservice.NewFetcher()},

		// Data Science
		&inject.Object{Value: datascience.NewTracker()},

		// Slack
		&inject.Object{Value: slack.NewSlacker()},

		// Chat Error Handler
		&inject.Object{Value: chat_error_handler.NewChatErrorHandler()},
	)

	if err != nil {
		return nil, err
	}

	err = graph.Populate()
	if err != nil {
		return nil, err
	}

	awsSession, err := session.NewSession(aws.NewConfig().WithRegion(cfg.AWSRegion))
	if err != nil {
		return nil, errors.Wrap(err, "creating event bus worker, aws session")
	}

	ebMux := eventbus.NewMux()

	eventbusworker.AddAutoModEventWorkerToMux(b.Server, ebMux)

	ebSQSClient, err := sqsclient.New(sqsclient.Config{
		Session:            awsSession,
		Dispatcher:         ebMux.Dispatcher(),
		DeliverConcurrency: b.Config.NumberOfEventBusWorkers,
		QueueURL:           b.Config.EventBusSQSQueueURL,
		VisibilityTimeout:  30 * time.Second,
		HookCallback: func(event sqsclient.HookEvent) {
			if err := event.Error(); err != nil {
				log.WithError(err).Error("eventbus worker")
			}
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "creating event bus worker, eventbus client")
	}

	b.EventBusSQSClient = ebSQSClient

	b.setupWorkers()

	return b, nil
}

func (b *backend) setupWorkers() {
	b.terminators = b.setupSQSWorker(b.terminators, b.Config.AutoModTimeoutQueueConfig, b.AutoModTimeoutWorker)
}

func (b *backend) setupSQSWorker(terminators []worker.NamedTerminator, queueConfig *config.QueueConfig, worker sqs_worker.Worker) []worker.NamedTerminator {
	if queueConfig != nil && queueConfig.Name != "" {
		terminators = append(terminators, sqs_worker.NewSQSWorkerManager(queueConfig.Name, queueConfig.NumWorkers, 1, b.SQS, worker, b.Stats))
	}
	return terminators
}

func (b *backend) Ping(w http.ResponseWriter, r *http.Request) {
	prismClient := prism.NewPrismProtobufClient(localhostServerEndpoint, http.DefaultClient)
	_, err := prismClient.HealthCheck(r.Context(), &prism.HealthCheckReq{})
	if err != nil {
		log.WithError(err).Error("Could not ping prism server")
		w.WriteHeader(http.StatusInternalServerError)
		_, innerErr := io.WriteString(w, err.Error())
		if innerErr != nil {
			log.WithError(innerErr).Error("Error writing http response")
		}
		return
	}

	w.WriteHeader(http.StatusOK)
	_, err = io.WriteString(w, "pong")
	if err != nil {
		log.WithError(err).Error("Error writing http response")
	}
}

func (b *backend) TwirpServer() prism.Prism {
	return b.Server
}

func (b *backend) Statter() statsd.Statter {
	return b.Stats
}

func (b *backend) Shutdown(ctx context.Context) *errgroup.Group {
	errg, _ := errgroup.WithContext(ctx)

	errg.Go(func() error {
		return b.EventBusSQSClient.Shutdown()
	})

	errg.Go(func() error {
		b.AuditLogger.Shutdown()
		return nil
	})

	for _, terminator := range b.terminators {
		t := terminator
		errg.Go(func() error {
			log.Infof("Shutting down %s", t.Name())
			err := t.Shutdown(ctx)
			if err != nil {
				log.WithError(err).Errorf("Could not shutdown %s", t.Name())
			}
			return err
		})
	}
	return errg
}
