package transaction

import (
	"context"

	transaction_dynamo "code.justin.tv/commerce/prism/backend/dynamo/transaction"
	"code.justin.tv/commerce/prism/backend/transaction/post_process"
	"code.justin.tv/commerce/prism/backend/transaction/process"
	"code.justin.tv/commerce/prism/backend/transaction/send_to_chat"
	"code.justin.tv/commerce/prism/backend/transaction/should_process"
	"code.justin.tv/commerce/prism/backend/transaction/start"
	"code.justin.tv/commerce/prism/backend/transaction/validate"
	"code.justin.tv/commerce/prism/config"
	prism "code.justin.tv/commerce/prism/rpc"
)

// Processor represents a transaction processor (processing of all transaction steps)
type Processor interface {
	should_process.Shoulder
	validate.Validator
	start.Starter
	process.Processor
	send_to_chat.Sender
	post_process.PostProcessor
}

type processor struct {
	Shoulder      should_process.Shoulder    `inject:"shoulder"`
	Starter       start.Starter              `inject:"starter"`
	Validator     validate.Validator         `inject:"validator"`
	Processor     process.Processor          `inject:"processor"`
	Sender        send_to_chat.Sender        `inject:"sender"`
	PostProcessor post_process.PostProcessor `inject:"post_processor"`
}

// NewProcessor creates a new Processor
func NewProcessor() Processor {
	return &processor{}
}

func (p *processor) ShouldProcess(ctx context.Context, req *prism.ProcessMessageReq) (bool, error) {
	return p.Shoulder.ShouldProcess(ctx, req)
}

func (p *processor) Start(ctx context.Context, req *prism.ProcessMessageReq) (*transaction_dynamo.Transaction, error) {
	return p.Starter.Start(ctx, req)
}

func (p *processor) Validate(ctx context.Context, tx *transaction_dynamo.Transaction) validate.ValidationResp {
	return p.Validator.Validate(ctx, tx)
}

func (p *processor) Process(ctx context.Context, tx *transaction_dynamo.Transaction) process.ProcessResp {
	return p.Processor.Process(ctx, tx)
}

func (p *processor) SendToChat(ctx context.Context, tx *transaction_dynamo.Transaction, transformations map[config.TenantName][]*prism.Transformation) send_to_chat.SendToChatResp {
	return p.Sender.SendToChat(ctx, tx, transformations)
}

func (p *processor) PostProcess(ctx context.Context, tx *transaction_dynamo.Transaction) post_process.PostProcessResp {
	return p.PostProcessor.PostProcess(ctx, tx)
}
