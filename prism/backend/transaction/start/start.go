package start

import (
	"context"
	"time"

	"code.justin.tv/commerce/prism/backend/clients/payday"
	"code.justin.tv/commerce/prism/backend/clients/stats"
	"code.justin.tv/commerce/prism/backend/consumable"
	transaction_dynamo "code.justin.tv/commerce/prism/backend/dynamo/transaction"
	"code.justin.tv/commerce/prism/backend/transaction/common"
	"code.justin.tv/commerce/prism/backend/utils"
	"code.justin.tv/commerce/prism/backend/validation/process_message"
	prism "code.justin.tv/commerce/prism/rpc"
	"code.justin.tv/commerce/prism/rpc/models"
)

// Starter represents a transaction starter
type Starter interface {
	Start(ctx context.Context, req *prism.ProcessMessageReq) (*transaction_dynamo.Transaction, error)
}

type starter struct {
	RequestValidator process_message.Validator `inject:""`
	ConsumableMapper consumable.Mapper         `inject:""`
	TransactionDAO   transaction_dynamo.DAO    `inject:""`
	Stats            stats.Statter             `inject:""`
	Payday           payday.Client             `inject:""`
}

// NewStarter creates a new Starter
func NewStarter() Starter {
	return &starter{}
}

func (s *starter) Start(ctx context.Context, req *prism.ProcessMessageReq) (tx *transaction_dynamo.Transaction, err error) {
	startTime := time.Now()
	defer func() {
		latency := time.Since(startTime)

		s.Stats.TimingDuration("transaction_processing_step__start_latency", latency, 1)

		s.Stats.Inc("transaction_processing_step__start_errors", common.ErrCount(err), 1)

		twErr := utils.ToTwirpError(err)
		if utils.IsA5XXError(twErr) {
			s.Stats.Inc("transaction_processing_step__5XX_start_errors", common.ErrCount(err), 1)
		} else {
			s.Stats.Inc("transaction_processing_step__4XX_start_errors", common.ErrCount(err), 1)
		}
	}()

	err = s.RequestValidator.Validate(req)
	if err != nil {
		return nil, err
	}

	consumables, err := s.ConsumableMapper.Validate(req.ConsumableAmounts)
	if err != nil {
		return nil, err
	}

	tx = &transaction_dynamo.Transaction{
		TransactionID:        req.MessageId,
		UserID:               req.UserId,
		ChannelID:            req.ChannelId,
		RoomID:               req.RoomId,
		Anonymous:            req.Anonymous,
		RequestedMessage:     req.Message,
		ConsumablesRequested: consumables,
		ProcessingStatus:     transaction_dynamo.StatusInProgressStarted,
		StartTime:            time.Now(),
		ShouldSendAnyway:     req.ShouldSendAnyway,
		AutomodAcknowledged:  req.AutomodAcknowledged,
	}

	if req.GeoIpMetadata != nil {
		tx.GeoIPMetadata = *req.GeoIpMetadata
	}

	if req.ExperimentMetadata != nil {
		if imageID, ok := req.ExperimentMetadata.Arguments[models.Experiments_Image_ID]; ok {
			imageURL, err := s.Payday.GetImageUrl(ctx, imageID)
			if err != nil {
				return nil, err
			}
			tx.ImageURL = imageURL
		}
	}

	err = s.TransactionDAO.CreateNew(tx)
	return tx, err
}
