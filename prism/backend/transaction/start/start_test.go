package start

import (
	"context"
	"errors"
	"testing"

	payday_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/payday"

	dynamo_transaction "code.justin.tv/commerce/prism/backend/dynamo/transaction"
	"code.justin.tv/commerce/prism/backend/models"
	stats_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/stats"
	consumable_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/consumable"
	transaction_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/dynamo/transaction"
	process_message_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/validation/process_message"
	prism "code.justin.tv/commerce/prism/rpc"
	experiments "code.justin.tv/commerce/prism/rpc/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestStarter_Start(t *testing.T) {
	Convey("Given a processor", t, func() {
		dao := new(transaction_mock.DAO)
		validator := new(process_message_mock.Validator)
		consumableMapper := new(consumable_mock.Mapper)
		stats := new(stats_mock.Statter)
		payday := new(payday_mock.Client)

		starter := starter{
			TransactionDAO:   dao,
			ConsumableMapper: consumableMapper,
			RequestValidator: validator,
			Stats:            stats,
			Payday:           payday,
		}

		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return()
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return()

		Convey("Error when the validator returns an error", func() {
			validator.On("Validate", mock.Anything).Return(errors.New("test error"))
			_, err := starter.Start(context.Background(), &prism.ProcessMessageReq{})
			So(err, ShouldNotBeNil)
		})

		Convey("When validator succeeds", func() {
			validator.On("Validate", mock.Anything).Return(nil)

			Convey("Errors when consumable mapper validate errors", func() {
				consumableMapper.On("Validate", mock.Anything).Return(nil, errors.New("test error"))
			})

			Convey("When consumable mapper validate succeeds", func() {
				consumableMapper.On("Validate", mock.Anything).Return(models.ConsumableMap{}, nil)

				Convey("When ImageID is defined", func() {
					Convey("When payday returns an error", func() {
						animeBearError := errors.New("ANIME BEAR ERROR")
						payday.On("GetImageUrl", mock.Anything, mock.Anything).Return("", animeBearError)

						_, err := starter.Start(context.Background(), &prism.ProcessMessageReq{
							ExperimentMetadata: &prism.ExperimentMetadata{
								Arguments: map[string]string{
									experiments.Experiments_Image_ID: "an-image-id",
								},
							},
						})
						So(err, ShouldEqual, animeBearError)
					})

					Convey("When payday returns data", func() {
						payday.On("GetImageUrl", mock.Anything, mock.Anything).Return("https://twitch.tv/screenshot.jpg", nil)
						dao.On("CreateNew", mock.Anything).Return(nil)

						tx, err := starter.Start(context.Background(), &prism.ProcessMessageReq{
							MessageId: "test-message-id",
							ExperimentMetadata: &prism.ExperimentMetadata{
								Arguments: map[string]string{
									experiments.Experiments_Image_ID: "an-image-id",
								},
							},
						})
						So(err, ShouldBeNil)
						So(tx.TransactionID, ShouldEqual, "test-message-id")
						So(tx.ImageURL, ShouldEqual, "https://twitch.tv/screenshot.jpg")
					})
				})

				Convey("When ImageID is not defined", func() {
					Convey("Errors when dao errors", func() {
						dao.On("CreateNew", mock.Anything).Return(errors.New("test error"))
						_, err := starter.Start(context.Background(), &prism.ProcessMessageReq{})
						So(err, ShouldNotBeNil)
					})

					Convey("Succeeds when dao succeeds", func() {
						dao.On("CreateNew", mock.Anything).Return(nil)
						tx, err := starter.Start(context.Background(), &prism.ProcessMessageReq{
							MessageId: "test-message-id",
							ChannelId: "test-channel-id",
							UserId:    "test-user-id",
							Message:   "test-message",
						})
						So(err, ShouldBeNil)
						So(tx.TransactionID, ShouldEqual, "test-message-id")
						So(tx.ChannelID, ShouldEqual, "test-channel-id")
						So(tx.UserID, ShouldEqual, "test-user-id")
						So(tx.RequestedMessage, ShouldEqual, "test-message")
						So(tx.ProcessingStatus, ShouldEqual, dynamo_transaction.StatusInProgressStarted)
					})
				})
			})
		})
	})
}
