package process

import (
	"context"
	"errors"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend/clients/stats"
	transaction_dynamo "code.justin.tv/commerce/prism/backend/dynamo/transaction"
	"code.justin.tv/commerce/prism/backend/landlord"
	"code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/backend/tenant"
	"code.justin.tv/commerce/prism/backend/transaction/common"
	"code.justin.tv/commerce/prism/backend/utils"
	"code.justin.tv/commerce/prism/config"
	prism "code.justin.tv/commerce/prism/rpc"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
)

type processor struct {
	Landlord       landlord.Landlord      `inject:""`
	TransactionDAO transaction_dynamo.DAO `inject:""`
	Stats          stats.Statter          `inject:""`
}

// Processor represents a transaction processor (process step of the transaction)
type Processor interface {
	Process(ctx context.Context, tx *transaction_dynamo.Transaction) ProcessResp
}

// ProcessResp is the response of the Processor's Process function
type ProcessResp struct {
	ProcessErr        error
	DynamoUpdateErr   error
	TenantProcessErrs map[config.TenantName]error
	Transformations   map[config.TenantName][]*prism.Transformation
	NewBalance        map[string]int64
}

// NewProcessor creates a new transaction Processor
func NewProcessor() Processor {
	return &processor{}
}

func (p *processor) Process(ctx context.Context, tx *transaction_dynamo.Transaction) (resp ProcessResp) {
	startTime := time.Now()
	defer func() {
		latency := time.Since(startTime)

		p.Stats.TimingDuration("transaction_processing_step__process_latency", latency, 1)

		p.Stats.Inc("transaction_processing_step__process_errors", common.ErrCount(resp.ProcessErr), 1)

		twErr := utils.ToTwirpError(resp.ProcessErr)
		if utils.IsA5XXError(twErr) {
			p.Stats.Inc("transaction_processing_step__5XX_process_errors", common.ErrCount(resp.ProcessErr), 1)
		} else {
			p.Stats.Inc("transaction_processing_step__4XX_process_errors", common.ErrCount(resp.ProcessErr), 1)
		}
	}()

	if tx == nil {
		return ProcessResp{ProcessErr: errors.New("transaction cannot be nil")}
	}

	if tx.ProcessingStatus != transaction_dynamo.StatusCompletedAllTenantsTokenizeAndValidateSuccessfully {
		return ProcessResp{ProcessErr: errors.New("transaction is not in the correct status")}
	}

	transformations := make(map[config.TenantName][]*prism.Transformation)
	tenantErrors := make(map[config.TenantName]error)
	tenantResps := p.Landlord.ProcessMessage(ctx, &tenant.ProcessMessageReq{
		UserID:               tx.UserID,
		ChannelID:            tx.ChannelID,
		RoomID:               tx.RoomID,
		MessageID:            tx.TransactionID,
		Message:              tx.RequestedMessage,
		ConsumablesRequested: tx.ConsumablesRequested,
		Anonymous:            tx.Anonymous,
		GeoIPMetadata:        tx.GeoIPMetadata,
	})

	consumablesProcessed := make(map[config.TenantName]models.ConsumableMap)
	newBalance := make(map[config.TenantName]models.ConsumableMap)
	combinedNewBalance := make(map[string]int64)

	for tenantName, resp := range tenantResps {
		if resp.Error != nil {
			tenantErrors[tenantName] = resp.Error
		}

		transformations[tenantName] = resp.Transformations
		consumablesProcessed[tenantName] = resp.ConsumablesProcessed
		newBalance[tenantName] = models.ToConsumableMap(resp.NewBalance)

		for consumable, balance := range resp.NewBalance {
			combinedNewBalance[consumable] += balance
		}

		tx.MessageTreatment = tx.MessageTreatment.ComparePriority(resp.MessageTreatment)
	}

	tx.Transformations = transaction_dynamo.ConvertTenantTransformationsListMap(transformations)

	if tx.Anonymous {
		tx.MessageTreatment = tx.MessageTreatment.ComparePriority(prism_models.ANONYMOUS_CHEER)
	}

	tx.ConsumablesProcessed = consumablesProcessed
	tx.NewBalance = newBalance

	var processingError error
	if len(tenantErrors) < 1 {
		tx.ProcessingStatus = transaction_dynamo.StatusCompletedAllTenantsProcessedSuccessfully
	} else if len(tenantErrors) > 0 && len(tenantErrors) < len(tenantResps) {
		tx.ProcessingStatus = transaction_dynamo.StatusCompletedPartialTenantProcessingError
		tx.TenantProcessErrors = transaction_dynamo.ConvertTenantErrorMap(tenantErrors)
	} else {
		processingError = utils.AllTenantsErroredDuringProcessCall
		tx.ProcessingStatus = transaction_dynamo.StatusCompletedAllTenantsEncounteredProcessingErrors
		tx.TenantProcessErrors = transaction_dynamo.ConvertTenantErrorMap(tenantErrors)
	}

	// Need to make sure there's somewhere for this message to go, even if it's just the default
	if tx.MessageTreatment == "" {
		if processingError != utils.AllTenantsErroredDuringProcessCall {
			log.WithField("transaction", tx).Error("No tenant provided any valid message treatment")
		}
		tx.MessageTreatment = prism_models.MessageTreatment("default")
	}

	return ProcessResp{
		ProcessErr:        processingError,
		DynamoUpdateErr:   p.TransactionDAO.CreateOrUpdate(tx),
		Transformations:   transformations,
		TenantProcessErrs: tenantErrors,
		NewBalance:        combinedNewBalance,
	}
}
