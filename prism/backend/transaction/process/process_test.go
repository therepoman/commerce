package process

import (
	"context"
	"errors"
	"testing"

	dynamo_transaction "code.justin.tv/commerce/prism/backend/dynamo/transaction"
	"code.justin.tv/commerce/prism/backend/tenant"
	"code.justin.tv/commerce/prism/config"
	stats_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/stats"
	transaction_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/dynamo/transaction"
	landlord_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/landlord"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestProcessor_Process(t *testing.T) {
	Convey("Given a processor", t, func() {
		dao := new(transaction_mock.DAO)
		landlord := new(landlord_mock.Landlord)
		stats := new(stats_mock.Statter)

		processor := processor{
			TransactionDAO: dao,
			Landlord:       landlord,
			Stats:          stats,
		}

		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return()
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return()

		Convey("Error for a nil transaction", func() {
			resp := processor.Process(context.Background(), nil)
			So(resp.ProcessErr, ShouldNotBeNil)
		})

		Convey("Error for a transaction in the wrong status", func() {
			resp := processor.Process(context.Background(), &dynamo_transaction.Transaction{
				ProcessingStatus: dynamo_transaction.StatusInProgressStarted,
			})
			So(resp.ProcessErr, ShouldNotBeNil)
		})

		Convey("When all tenants succeed", func() {
			landlord.On("ProcessMessage", mock.Anything, mock.Anything).Return(map[config.TenantName]tenant.ProcessMessageResp{
				config.TenantName("test-tenant-1"): {
					Error: nil,
				},
				config.TenantName("test-tenant-2"): {
					Error: nil,
				},
				config.TenantName("test-tenant-3"): {
					Error: nil,
				},
			})

			Convey("When DAO update errors", func() {
				tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusCompletedAllTenantsTokenizeAndValidateSuccessfully}
				dao.On("CreateOrUpdate", mock.Anything).Return(errors.New("test error"))
				resp := processor.Process(context.Background(), tx)
				So(resp.ProcessErr, ShouldBeNil)
				So(resp.DynamoUpdateErr, ShouldNotBeNil)
				So(tx.ProcessingStatus, ShouldEqual, dynamo_transaction.StatusCompletedAllTenantsProcessedSuccessfully)
				So(len(resp.TenantProcessErrs), ShouldEqual, 0)
			})

			Convey("When DAO update succeeds", func() {
				tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusCompletedAllTenantsTokenizeAndValidateSuccessfully}
				dao.On("CreateOrUpdate", mock.Anything).Return(nil)
				resp := processor.Process(context.Background(), tx)
				So(resp.ProcessErr, ShouldBeNil)
				So(resp.DynamoUpdateErr, ShouldBeNil)
				So(tx.ProcessingStatus, ShouldEqual, dynamo_transaction.StatusCompletedAllTenantsProcessedSuccessfully)
				So(len(resp.TenantProcessErrs), ShouldEqual, 0)
			})
		})

		Convey("When some tenants error", func() {
			landlord.On("ProcessMessage", mock.Anything, mock.Anything).Return(map[config.TenantName]tenant.ProcessMessageResp{
				config.TenantName("test-tenant-1"): {
					Error: nil,
				},
				config.TenantName("test-tenant-2"): {
					Error: errors.New("test error"),
				},
				config.TenantName("test-tenant-3"): {
					Error: nil,
				},
			})

			Convey("When DAO update errors", func() {
				tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusCompletedAllTenantsTokenizeAndValidateSuccessfully}
				dao.On("CreateOrUpdate", mock.Anything).Return(errors.New("test error"))
				resp := processor.Process(context.Background(), tx)
				So(resp.ProcessErr, ShouldBeNil)
				So(resp.DynamoUpdateErr, ShouldNotBeNil)
				So(tx.ProcessingStatus, ShouldEqual, dynamo_transaction.StatusCompletedPartialTenantProcessingError)
				So(len(resp.TenantProcessErrs), ShouldEqual, 1)
			})

			Convey("When DAO update succeeds", func() {
				tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusCompletedAllTenantsTokenizeAndValidateSuccessfully}
				dao.On("CreateOrUpdate", mock.Anything).Return(nil)
				resp := processor.Process(context.Background(), tx)
				So(resp.ProcessErr, ShouldBeNil)
				So(resp.DynamoUpdateErr, ShouldBeNil)
				So(tx.ProcessingStatus, ShouldEqual, dynamo_transaction.StatusCompletedPartialTenantProcessingError)
				So(len(resp.TenantProcessErrs), ShouldEqual, 1)
			})
		})

		Convey("When all tenants error", func() {
			landlord.On("ProcessMessage", mock.Anything, mock.Anything).Return(map[config.TenantName]tenant.ProcessMessageResp{
				config.TenantName("test-tenant-1"): {
					Error: errors.New("test error"),
				},
				config.TenantName("test-tenant-2"): {
					Error: errors.New("test error"),
				},
				config.TenantName("test-tenant-3"): {
					Error: errors.New("test error"),
				},
			})

			Convey("When DAO update errors", func() {
				tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusCompletedAllTenantsTokenizeAndValidateSuccessfully}
				dao.On("CreateOrUpdate", mock.Anything).Return(errors.New("test error"))
				resp := processor.Process(context.Background(), tx)
				So(resp.ProcessErr, ShouldNotBeNil)
				So(resp.DynamoUpdateErr, ShouldNotBeNil)
				So(tx.ProcessingStatus, ShouldEqual, dynamo_transaction.StatusCompletedAllTenantsEncounteredProcessingErrors)
				So(len(resp.TenantProcessErrs), ShouldEqual, 3)
			})

			Convey("When DAO update succeeds", func() {
				tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusCompletedAllTenantsTokenizeAndValidateSuccessfully}
				dao.On("CreateOrUpdate", mock.Anything).Return(nil)
				resp := processor.Process(context.Background(), tx)
				So(resp.ProcessErr, ShouldNotBeNil)
				So(resp.DynamoUpdateErr, ShouldBeNil)
				So(tx.ProcessingStatus, ShouldEqual, dynamo_transaction.StatusCompletedAllTenantsEncounteredProcessingErrors)
				So(len(resp.TenantProcessErrs), ShouldEqual, 3)
			})
		})
	})
}
