package post_process

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/prism/backend/clients/stats"
	transaction_dynamo "code.justin.tv/commerce/prism/backend/dynamo/transaction"
	"code.justin.tv/commerce/prism/backend/landlord"
	"code.justin.tv/commerce/prism/backend/tenant"
	"code.justin.tv/commerce/prism/backend/transaction/common"
	"code.justin.tv/commerce/prism/backend/utils"
	"code.justin.tv/commerce/prism/config"
)

type postProcessor struct {
	Landlord       landlord.Landlord      `inject:""`
	TransactionDAO transaction_dynamo.DAO `inject:""`
	Stats          stats.Statter          `inject:""`
}

type PostProcessor interface {
	PostProcess(ctx context.Context, tx *transaction_dynamo.Transaction) PostProcessResp
}

type PostProcessResp struct {
	PostProcessErr        error
	DynamoUpdateErr       error
	TenantPostProcessErrs map[config.TenantName]error
}

func NewPostProcessor() PostProcessor {
	return &postProcessor{}
}

func (pp *postProcessor) PostProcess(ctx context.Context, tx *transaction_dynamo.Transaction) (resp PostProcessResp) {
	startTime := time.Now()
	defer func() {
		latency := time.Since(startTime)

		pp.Stats.TimingDuration("transaction_processing_step__post_process_latency", latency, 1)

		pp.Stats.Inc("transaction_processing_step__post_process_errors", common.ErrCount(resp.PostProcessErr), 1)

		twErr := utils.ToTwirpError(resp.PostProcessErr)
		if utils.IsA5XXError(twErr) {
			pp.Stats.Inc("transaction_processing_step__5XX_post_process_errors", common.ErrCount(resp.PostProcessErr), 1)
		} else {
			pp.Stats.Inc("transaction_processing_step__4XX_post_process_errors", common.ErrCount(resp.PostProcessErr), 1)
		}
	}()

	if tx == nil {
		return PostProcessResp{PostProcessErr: errors.New("transaction cannot be nil")}
	}

	_, isAcceptedStatus := map[transaction_dynamo.ProcessingStatus]interface{}{
		transaction_dynamo.StatusCompletedAllTenantsProcessedSuccessfully: nil,
		transaction_dynamo.StatusCompletedPartialTenantProcessingError:    nil,
	}[tx.ProcessingStatus]
	if !isAcceptedStatus {
		return PostProcessResp{PostProcessErr: errors.New("transaction is not in the correct status")}
	}

	if tx.ChatStatus != transaction_dynamo.StatusSuccesfullySentMessageToChat {
		return PostProcessResp{PostProcessErr: errors.New("transaction chat status is not in the correct status")}
	}

	tenantErrors := make(map[config.TenantName]error)
	tenantResps := pp.Landlord.PostProcessMessage(ctx, &tenant.PostProcessMessageReq{
		UserID:               tx.UserID,
		ChannelID:            tx.ChannelID,
		RoomID:               tx.RoomID,
		MessageID:            tx.TransactionID,
		Message:              tx.RequestedMessage,
		ConsumablesProcessed: tx.ConsumablesProcessed,
		Anonymous:            tx.Anonymous,
		GeoIPMetadata:        tx.GeoIPMetadata,
		SentMessage:          tx.SentMessage,
	})

	for tenantName, tenantResp := range tenantResps {
		if tenantResp.Error != nil {
			tenantErrors[tenantName] = tenantResp.Error
		}
	}

	var postProcessingError error
	if len(tenantErrors) < 1 {
		tx.ProcessingStatus = transaction_dynamo.StatusCompletedAllTenantsPostProcessedSuccessfully
	} else if len(tenantErrors) > 0 && len(tenantErrors) < len(tenantResps) {
		tx.ProcessingStatus = transaction_dynamo.StatusCompletedPartialTenantPostProcessingError
		tx.TenantPostProcessErrors = transaction_dynamo.ConvertTenantErrorMap(tenantErrors)
	} else {
		postProcessingError = errors.New("all tenants encountered post processing errors")
		tx.ProcessingStatus = transaction_dynamo.StatusCompletedAllTenantsEncounteredPostProcessingErrors
		tx.TenantPostProcessErrors = transaction_dynamo.ConvertTenantErrorMap(tenantErrors)
	}

	return PostProcessResp{
		PostProcessErr:        postProcessingError,
		DynamoUpdateErr:       pp.TransactionDAO.CreateOrUpdate(tx),
		TenantPostProcessErrs: tenantErrors,
	}
}
