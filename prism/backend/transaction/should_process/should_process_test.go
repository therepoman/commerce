package should_process

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/prism/backend/cache/automod"
	automod_cache_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/cache/automod"
	stats_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/stats"
	filter_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/filter"
	throttler_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/throttle"
	process_message_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/validation/process_message"
	prism "code.justin.tv/commerce/prism/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestProcessor_ShouldProcess(t *testing.T) {
	Convey("Given a processor", t, func() {
		filterer := new(filter_mock.Filterer)
		validator := new(process_message_mock.Validator)
		autoModCache := new(automod_cache_mock.Cache)
		throttler := new(throttler_mock.Throttler)
		stats := new(stats_mock.Statter)

		shoulder := shoulder{
			RequestValidator: validator,
			Filterer:         filterer,
			AutoModCache:     autoModCache,
			Throttler:        throttler,
			Stats:            stats,
		}

		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return()
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return()

		Convey("Error when the validator returns an error", func() {
			validator.On("Validate", mock.Anything).Return(errors.New("test error"))
			shouldProcess, err := shoulder.ShouldProcess(context.Background(), &prism.ProcessMessageReq{})
			So(err, ShouldNotBeNil)
			So(shouldProcess, ShouldBeFalse)
		})

		Convey("When validator succeeds", func() {
			validator.On("Validate", mock.Anything).Return(nil)

			Convey("Errors when filterer errors", func() {
				filterer.On("ShouldFilter", mock.Anything, mock.Anything).Return(false, errors.New("test error"))
				shouldProcess, err := shoulder.ShouldProcess(context.Background(), &prism.ProcessMessageReq{})
				So(err, ShouldNotBeNil)
				So(shouldProcess, ShouldBeFalse)
			})

			Convey("Should not process when filterer filters", func() {
				filterer.On("ShouldFilter", mock.Anything, mock.Anything).Return(true, errors.New("test twirp error"))
				shouldProcess, err := shoulder.ShouldProcess(context.Background(), &prism.ProcessMessageReq{})
				So(err, ShouldNotBeNil)
				So(shouldProcess, ShouldBeFalse)
			})

			Convey("When filterer does not filter", func() {
				filterer.On("ShouldFilter", mock.Anything, mock.Anything).Return(false, nil)

				Convey("Should not process when user has a pending automod request", func() {
					autoModCache.On("Get", mock.Anything).Return(&automod.CacheValue{})

					shouldProcess, err := shoulder.ShouldProcess(context.Background(), &prism.ProcessMessageReq{})
					So(err, ShouldNotBeNil)
					So(shouldProcess, ShouldBeFalse)
				})

				Convey("When user does NOT have a pending automod request", func() {
					autoModCache.On("Get", mock.Anything).Return(nil)

					Convey("When the throttler errors connecting to redis", func() {
						throttler.On("Throttle", mock.Anything, mock.Anything, mock.Anything).Return(false, errors.New("test error"))

						shouldProcess, err := shoulder.ShouldProcess(context.Background(), &prism.ProcessMessageReq{})
						So(err, ShouldBeNil)
						So(shouldProcess, ShouldBeTrue)
					})

					Convey("When the throttler returns that the user is throttled", func() {
						throttler.On("Throttle", mock.Anything, mock.Anything, mock.Anything).Return(true, nil)

						shouldProcess, err := shoulder.ShouldProcess(context.Background(), &prism.ProcessMessageReq{})
						So(err, ShouldNotBeNil)
						So(shouldProcess, ShouldBeFalse)
					})

					Convey("When the throttler returns that the user is NOT throttled", func() {
						throttler.On("Throttle", mock.Anything, mock.Anything, mock.Anything).Return(false, nil)

						shouldProcess, err := shoulder.ShouldProcess(context.Background(), &prism.ProcessMessageReq{})
						So(err, ShouldBeNil)
						So(shouldProcess, ShouldBeTrue)
					})
				})
			})
		})
	})
}
