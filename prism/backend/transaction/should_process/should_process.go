package should_process

import (
	"context"
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	automod_cache "code.justin.tv/commerce/prism/backend/cache/automod"
	"code.justin.tv/commerce/prism/backend/clients/stats"
	"code.justin.tv/commerce/prism/backend/filter"
	"code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/backend/models/api"
	"code.justin.tv/commerce/prism/backend/throttle"
	"code.justin.tv/commerce/prism/backend/transaction/common"
	"code.justin.tv/commerce/prism/backend/utils"
	"code.justin.tv/commerce/prism/backend/validation/process_message"
	prism "code.justin.tv/commerce/prism/rpc"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	"github.com/twitchtv/twirp"
)

// Shoulder represents a transaction's "ShouldProcess" checker
type Shoulder interface {
	ShouldProcess(ctx context.Context, req *prism.ProcessMessageReq) (bool, error)
}

type shoulder struct {
	RequestValidator process_message.Validator `inject:""`
	Filterer         filter.Filterer           `inject:""`
	AutoModCache     automod_cache.Cache       `inject:""`
	Throttler        throttle.Throttler        `inject:""`
	Stats            stats.Statter             `inject:""`
}

// NewShoulder creates a new Shoulder
func NewShoulder() Shoulder {
	return &shoulder{}
}

func (s *shoulder) ShouldProcess(ctx context.Context, req *prism.ProcessMessageReq) (should bool, err error) {
	startTime := time.Now()
	defer func() {
		latency := time.Since(startTime)

		s.Stats.TimingDuration("transaction_processing_step__should_process_latency", latency, 1)

		s.Stats.Inc("transaction_processing_step__should_process_errors", common.ErrCount(err), 1)

		twErr := utils.ToTwirpError(err)
		if utils.IsA5XXError(twErr) {
			s.Stats.Inc("transaction_processing_step__5XX_should_process_errors", common.ErrCount(err), 1)
		} else {
			s.Stats.Inc("transaction_processing_step__4XX_should_process_errors", common.ErrCount(err), 1)
		}
	}()

	err = s.RequestValidator.Validate(req)
	if err != nil {
		return false, err
	}

	shouldFilter, err := s.Filterer.ShouldFilter(ctx, *req)
	if err != nil {
		return false, err
	}
	if shouldFilter {
		return false, twirp.NewError(twirp.FailedPrecondition, api.MessageFilteredError)
	}

	pendingAutoModRequest := s.AutoModCache.Get(req.UserId)
	if pendingAutoModRequest != nil {
		errorResponse := api.CreateEventErrorResponse{
			Status:  api.AutoModPending,
			Message: api.AutoModPendingMessageError,
		}
		return false, prism_models.NewClientError(twirp.NewError(twirp.FailedPrecondition, api.AutoModPendingMessageError), errorResponse)
	}

	// If redis is down, we still want to process messages under the assumption that we are not getting
	// spammed with consumables messages during the outage. This is why we only warn on the error returned here.
	throttled, err := s.Throttler.Throttle(ctx, fmt.Sprintf(models.ProcessMessageRequestFormat, req.UserId), models.ProcessMessageRequestThrottle)
	if err != nil {
		log.WithField("key", fmt.Sprintf(models.ProcessMessageRequestFormat, req.UserId)).Warn("could not throttle message")
	}

	if throttled {
		log.WithField("request", req).Warn("user message request throttled by throttler")
		return false, twirp.NewError(twirp.FailedPrecondition, api.RequestThrottledError)
	}

	return true, nil
}
