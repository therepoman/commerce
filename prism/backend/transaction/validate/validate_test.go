package validate

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/chat/zuma/app/api"
	dynamo_transaction "code.justin.tv/commerce/prism/backend/dynamo/transaction"
	"code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/backend/tenant"
	"code.justin.tv/commerce/prism/backend/transaction/validation"
	"code.justin.tv/commerce/prism/backend/utils"
	"code.justin.tv/commerce/prism/config"
	zuma_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/chat/zuma"
	anonymous_mesage_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/anonymous_message"
	automod_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/automod"
	stats_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/stats"
	datascience_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/datascience"
	transaction_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/dynamo/transaction"
	landlord_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/landlord"
	prism "code.justin.tv/commerce/prism/rpc/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestProcessor_Validate(t *testing.T) {
	Convey("Given a processor", t, func() {
		dao := new(transaction_mock.DAO)
		landlord := new(landlord_mock.Landlord)
		autoModProcessor := new(automod_mock.Processor)
		anonMessageErrorHandler := new(anonymous_mesage_mock.AnonymousMessageErrorHandler)
		dataScienceTracker := new(datascience_mock.Tracker)
		stats := new(stats_mock.Statter)

		zumaClient := new(zuma_mock.Client)
		zumaValidator := validation.NewTestZumaValidator(zumaClient)

		validator := validator{
			Landlord:                     landlord,
			TransactionDAO:               dao,
			AnonymousMessageErrorHandler: anonMessageErrorHandler,
			ZumaValidator:                zumaValidator,
			AutoModProcessor:             autoModProcessor,
			DataScienceTracker:           dataScienceTracker,
			Stats:                        stats,
		}

		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return()
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return()

		dataScienceTracker.On("GetUserLoginForDataScience", mock.Anything, mock.Anything).Return("")
		dataScienceTracker.On("GetDataScienceCommonProperties", mock.Anything).Return(models.DataScienceCommonProperties{})
		dataScienceTracker.On("TrackEvent", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("Error for a nil transaction", func() {
			resp := validator.Validate(context.Background(), nil)
			So(resp.ValidationErr, ShouldNotBeNil)
		})

		Convey("Error for a transaction in the wrong status", func() {
			resp := validator.Validate(context.Background(), &dynamo_transaction.Transaction{
				ProcessingStatus: dynamo_transaction.StatusCompletedAllTenantsTokenizeAndValidateSuccessfully,
			})
			So(resp.ValidationErr, ShouldNotBeNil)
		})

		Convey("InvalidArgument error for a transaction without any consumables requested", func() {
			resp := validator.Validate(context.Background(), &dynamo_transaction.Transaction{
				ProcessingStatus: dynamo_transaction.StatusInProgressStarted,
				ConsumablesRequested: models.ConsumableMap{
					prism.ConsumableType_Bits: {
						prism.Consumable_Bits: 0,
					},
				},
			})
			So(resp.ValidationErr, ShouldNotBeNil)
			So(utils.ToTwirpError(resp.ValidationErr).Code(), ShouldEqual, twirp.InvalidArgument)
			dataScienceTracker.AssertCalled(t, "TrackEvent", mock.Anything, models.BitsCheerZeroEventName, mock.Anything, mock.Anything, mock.Anything)
		})

		Convey("When tokenize and validate fails for all tenants", func() {
			zumaClient.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(api.EnforceMessageResponse{
				EnforceProperties: api.EnforceProperties{
					AllPassed: true,
				},
			}, nil)

			landlord.On("TokenizeAndValidate", mock.Anything, mock.Anything).Return(map[config.TenantName]tenant.TokenizeAndValidateResp{
				config.TenantName("test-tenant-1"): {
					Error: errors.New("test error"),
				},
				config.TenantName("test-tenant-2"): {
					Error: errors.New("test error"),
				},
			})
			anonMessageErrorHandler.On("Handle", mock.Anything, mock.Anything, mock.Anything).Return(false, nil)

			Convey("When DAO update errors", func() {
				tx := &dynamo_transaction.Transaction{
					ProcessingStatus: dynamo_transaction.StatusInProgressStarted,
					ConsumablesRequested: models.ConsumableMap{
						prism.ConsumableType_Bits: {
							prism.Consumable_Bits: 1,
						},
					},
				}
				dao.On("CreateOrUpdate", mock.Anything).Return(errors.New("test error"))
				resp := validator.Validate(context.Background(), tx)
				So(resp.ValidationErr, ShouldNotBeNil)
				So(resp.DynamoUpdateErr, ShouldNotBeNil)
				So(tx.ProcessingStatus, ShouldEqual, dynamo_transaction.StatusCompletedAllTenantsEncounteredTokenizeAndValidateErrors)
				So(len(resp.TenantTokenizeAndValidateErrs), ShouldEqual, 2)
			})

			Convey("When DAO update succeeds", func() {
				tx := &dynamo_transaction.Transaction{
					ProcessingStatus: dynamo_transaction.StatusInProgressStarted,
					ConsumablesRequested: models.ConsumableMap{
						prism.ConsumableType_Bits: {
							prism.Consumable_Bits: 1,
						},
					},
				}
				dao.On("CreateOrUpdate", mock.Anything).Return(nil)
				resp := validator.Validate(context.Background(), tx)
				So(resp.ValidationErr, ShouldNotBeNil)
				So(resp.DynamoUpdateErr, ShouldBeNil)
				So(tx.ProcessingStatus, ShouldEqual, dynamo_transaction.StatusCompletedAllTenantsEncounteredTokenizeAndValidateErrors)
				So(len(resp.TenantTokenizeAndValidateErrs), ShouldEqual, 2)
			})
		})

		Convey("When tokenize and validate fails for some tenants", func() {
			zumaClient.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(api.EnforceMessageResponse{
				EnforceProperties: api.EnforceProperties{
					AllPassed: true,
				},
			}, nil)

			landlord.On("TokenizeAndValidate", mock.Anything, mock.Anything).Return(map[config.TenantName]tenant.TokenizeAndValidateResp{
				config.TenantName("test-tenant-1"): {
					Error: nil,
				},
				config.TenantName("test-tenant-2"): {
					Error: errors.New("test error"),
				},
			})
			anonMessageErrorHandler.On("Handle", mock.Anything, mock.Anything, mock.Anything).Return(false, nil)

			Convey("When DAO update errors", func() {
				tx := &dynamo_transaction.Transaction{
					ProcessingStatus: dynamo_transaction.StatusInProgressStarted,
					ConsumablesRequested: models.ConsumableMap{
						prism.ConsumableType_Bits: {
							prism.Consumable_Bits: 1,
						},
					},
				}
				dao.On("CreateOrUpdate", mock.Anything).Return(errors.New("test error"))
				resp := validator.Validate(context.Background(), tx)
				So(resp.ValidationErr, ShouldNotBeNil)
				So(resp.DynamoUpdateErr, ShouldNotBeNil)
				So(tx.ProcessingStatus, ShouldEqual, dynamo_transaction.StatusCompletedPartialTenantTokenizeAndValidateError)
				So(len(resp.TenantTokenizeAndValidateErrs), ShouldEqual, 1)
			})

			Convey("When DAO update succeeds", func() {
				tx := &dynamo_transaction.Transaction{
					ProcessingStatus: dynamo_transaction.StatusInProgressStarted,
					ConsumablesRequested: models.ConsumableMap{
						prism.ConsumableType_Bits: {
							prism.Consumable_Bits: 1,
						},
					},
				}
				dao.On("CreateOrUpdate", mock.Anything).Return(nil)
				resp := validator.Validate(context.Background(), tx)
				So(resp.ValidationErr, ShouldNotBeNil)
				So(resp.DynamoUpdateErr, ShouldBeNil)
				So(tx.ProcessingStatus, ShouldEqual, dynamo_transaction.StatusCompletedPartialTenantTokenizeAndValidateError)
				So(len(resp.TenantTokenizeAndValidateErrs), ShouldEqual, 1)
			})
		})

		Convey("When a tenant returns an invalid anon message error", func() {
			zumaClient.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(api.EnforceMessageResponse{
				EnforceProperties: api.EnforceProperties{
					AllPassed: true,
				},
			}, nil)

			landlord.On("TokenizeAndValidate", mock.Anything, mock.Anything).Return(map[config.TenantName]tenant.TokenizeAndValidateResp{
				config.TenantName("test-tenant-1"): {
					Error: prism.NewInvalidAnonymousMessageError("test error", "anon1 anon2"),
				},
				config.TenantName("test-tenant-2"): {
					Error: nil,
				},
			})

			Convey("When the error handler returns a nil error", func() {
				tx := &dynamo_transaction.Transaction{
					ProcessingStatus: dynamo_transaction.StatusInProgressStarted,
					ConsumablesRequested: models.ConsumableMap{
						prism.ConsumableType_Bits: {
							prism.Consumable_Bits: 1,
						},
					},
				}
				dao.On("CreateOrUpdate", mock.Anything).Return(nil)
				anonMessageErrorHandler.On("Handle", mock.Anything, mock.Anything, mock.Anything).Return(true, nil)
				zumaClient.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(api.EnforceMessageResponse{
					EnforceProperties: api.EnforceProperties{
						AllPassed: true,
					},
				}, nil)
				resp := validator.Validate(context.Background(), tx)
				So(resp.ValidationErr, ShouldBeNil)
				So(resp.DynamoUpdateErr, ShouldBeNil)
				So(tx.ProcessingStatus, ShouldEqual, dynamo_transaction.StatusCompletedAllTenantsTokenizeAndValidateSuccessfully)
				So(len(resp.TenantTokenizeAndValidateErrs), ShouldEqual, 0)
			})

			Convey("When the error handler returns a non-nil error", func() {
				tx := &dynamo_transaction.Transaction{
					ProcessingStatus: dynamo_transaction.StatusInProgressStarted,
					ConsumablesRequested: models.ConsumableMap{
						prism.ConsumableType_Bits: {
							prism.Consumable_Bits: 1,
						},
					},
				}
				dao.On("CreateOrUpdate", mock.Anything).Return(nil)
				anonMessageErrorHandler.On("Handle", mock.Anything, mock.Anything, mock.Anything).Return(true, errors.New("test error"))
				resp := validator.Validate(context.Background(), tx)
				So(resp.ValidationErr, ShouldNotBeNil)
				So(resp.DynamoUpdateErr, ShouldBeNil)
				So(tx.ProcessingStatus, ShouldEqual, dynamo_transaction.StatusFailedAnonymousMessageValidation)
				So(len(resp.TenantTokenizeAndValidateErrs), ShouldEqual, 0)
			})
		})

		Convey("When tokenize and validate succeeds for all tenants", func() {
			landlord.On("TokenizeAndValidate", mock.Anything, mock.Anything).Return(map[config.TenantName]tenant.TokenizeAndValidateResp{
				config.TenantName("test-tenant-1"): {
					Error: nil,
				},
				config.TenantName("test-tenant-2"): {
					Error: nil,
				},
			})

			Convey("When zuma validation fails", func() {
				zumaClient.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(api.EnforceMessageResponse{
					EnforceProperties: api.EnforceProperties{
						AllPassed: false,
						Banned: &api.BannedResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: false,
							},
						},
						Suspended: &api.GenericEnforcementResponse{
							Passed: true,
						},
						Zalgo: &api.GenericEnforcementResponse{
							Passed: true,
						},
						ChannelBlockedTerms: &api.ChannelBlockedTermsResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						AutoMod: &api.AutoModResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						Blocked: &api.GenericEnforcementResponse{
							Passed: true,
						},
						DisposableEmail: &api.GenericEnforcementResponse{
							Passed: true,
						},
						EmoteOnly: &api.GenericEnforcementResponse{
							Passed: true,
						},
						Entropy: &api.GenericEnforcementResponse{
							Passed: true,
						},
						FollowersOnly: &api.FollowersOnlyResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						Spam: &api.SpamResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						SubscribersOnly: &api.GenericEnforcementResponse{
							Passed: true,
						},
						VerifiedOnly: &api.GenericEnforcementResponse{
							Passed: true,
						},
					},
				}, nil)

				tx := &dynamo_transaction.Transaction{
					ProcessingStatus: dynamo_transaction.StatusInProgressStarted,
					ConsumablesRequested: models.ConsumableMap{
						prism.ConsumableType_Bits: {
							prism.Consumable_Bits: 1,
						},
					},
				}
				dao.On("CreateOrUpdate", mock.Anything).Return(nil)
				resp := validator.Validate(context.Background(), tx)
				So(resp.ValidationErr, ShouldNotBeNil)
				So(resp.DynamoUpdateErr, ShouldBeNil)
				So(tx.ProcessingStatus, ShouldEqual, dynamo_transaction.StatusFailedZumaValidation)
			})

			Convey("When zuma validation fails because of AutoMod", func() {
				zumaClient.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(api.EnforceMessageResponse{
					EnforceProperties: api.EnforceProperties{
						AllPassed: false,
						Banned: &api.BannedResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						Suspended: &api.GenericEnforcementResponse{
							Passed: true,
						},
						Zalgo: &api.GenericEnforcementResponse{
							Passed: true,
						},
						ChannelBlockedTerms: &api.ChannelBlockedTermsResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						AutoMod: &api.AutoModResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: false,
							},
						},
						Blocked: &api.GenericEnforcementResponse{
							Passed: true,
						},
						DisposableEmail: &api.GenericEnforcementResponse{
							Passed: true,
						},
						EmoteOnly: &api.GenericEnforcementResponse{
							Passed: true,
						},
						Entropy: &api.GenericEnforcementResponse{
							Passed: true,
						},
						FollowersOnly: &api.FollowersOnlyResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						Spam: &api.SpamResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						SubscribersOnly: &api.GenericEnforcementResponse{
							Passed: true,
						},
						VerifiedOnly: &api.GenericEnforcementResponse{
							Passed: true,
						},
					},
				}, nil)

				Convey("When ShouldSendAnyway is false", func() {
					tx := &dynamo_transaction.Transaction{
						ProcessingStatus: dynamo_transaction.StatusInProgressStarted,
						ShouldSendAnyway: false,
						ConsumablesRequested: models.ConsumableMap{
							prism.ConsumableType_Bits: {
								prism.Consumable_Bits: 1,
							},
						},
					}
					dao.On("CreateOrUpdate", mock.Anything).Return(nil)
					resp := validator.Validate(context.Background(), tx)
					So(resp.ValidationErr, ShouldNotBeNil)
					So(resp.DynamoUpdateErr, ShouldBeNil)
					So(tx.ProcessingStatus, ShouldEqual, dynamo_transaction.StatusFailedZumaValidation)
					autoModProcessor.AssertNumberOfCalls(t, "Process", 0)
				})

				Convey("When ShouldSendAnyway is true", func() {
					Convey("When AutoMod Processor fails", func() {
						autoModProcessor.On("Process", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test error"))
						tx := &dynamo_transaction.Transaction{
							ProcessingStatus: dynamo_transaction.StatusInProgressStarted,
							ShouldSendAnyway: true,
							ConsumablesRequested: models.ConsumableMap{
								prism.ConsumableType_Bits: {
									prism.Consumable_Bits: 1,
								},
							},
						}
						dao.On("CreateOrUpdate", mock.Anything).Return(nil)
						resp := validator.Validate(context.Background(), tx)
						So(resp.AutoModProcessed, ShouldBeFalse)
						So(resp.ValidationErr, ShouldNotBeNil)
						So(resp.DynamoUpdateErr, ShouldBeNil)
						So(tx.ProcessingStatus, ShouldEqual, dynamo_transaction.StatusFailedZumaValidation)
						autoModProcessor.AssertNumberOfCalls(t, "Process", 1)
					})

					Convey("When AutoMod Processor succeeds", func() {
						autoModProcessor.On("Process", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
						tx := &dynamo_transaction.Transaction{
							ProcessingStatus: dynamo_transaction.StatusInProgressStarted,
							ShouldSendAnyway: true,
							ConsumablesRequested: models.ConsumableMap{
								prism.ConsumableType_Bits: {
									prism.Consumable_Bits: 1,
								},
							},
						}
						dao.On("CreateOrUpdate", mock.Anything).Return(nil)
						resp := validator.Validate(context.Background(), tx)
						So(resp.AutoModProcessed, ShouldBeTrue)
						So(resp.ValidationErr, ShouldBeNil)
						So(resp.DynamoUpdateErr, ShouldBeNil)
						So(tx.ProcessingStatus, ShouldEqual, dynamo_transaction.StatusFailedZumaValidation)
						autoModProcessor.AssertNumberOfCalls(t, "Process", 1)
					})
				})
			})

			Convey("When zuma validation passes", func() {
				zumaClient.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(api.EnforceMessageResponse{
					EnforceProperties: api.EnforceProperties{
						AllPassed: true,
					},
				}, nil)

				Convey("When DAO update errors", func() {
					tx := &dynamo_transaction.Transaction{
						ProcessingStatus: dynamo_transaction.StatusInProgressStarted,
						ConsumablesRequested: models.ConsumableMap{
							prism.ConsumableType_Bits: {
								prism.Consumable_Bits: 1,
							},
						},
					}
					dao.On("CreateOrUpdate", mock.Anything).Return(errors.New("test error"))
					resp := validator.Validate(context.Background(), tx)
					So(resp.ValidationErr, ShouldBeNil)
					So(resp.DynamoUpdateErr, ShouldNotBeNil)
					So(tx.ProcessingStatus, ShouldEqual, dynamo_transaction.StatusCompletedAllTenantsTokenizeAndValidateSuccessfully)
					So(resp.TenantTokenizeAndValidateErrs, ShouldBeEmpty)
				})

				Convey("When DAO update succeeds", func() {
					tx := &dynamo_transaction.Transaction{
						ProcessingStatus: dynamo_transaction.StatusInProgressStarted,
						ConsumablesRequested: models.ConsumableMap{
							prism.ConsumableType_Bits: {
								prism.Consumable_Bits: 1,
							},
						},
					}
					dao.On("CreateOrUpdate", mock.Anything).Return(nil)
					resp := validator.Validate(context.Background(), tx)
					So(resp.ValidationErr, ShouldBeNil)
					So(resp.DynamoUpdateErr, ShouldBeNil)
					So(tx.ProcessingStatus, ShouldEqual, dynamo_transaction.StatusCompletedAllTenantsTokenizeAndValidateSuccessfully)
					So(resp.TenantTokenizeAndValidateErrs, ShouldBeEmpty)
				})
			})
		})
	})
}
