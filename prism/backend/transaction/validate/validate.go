package validate

import (
	"context"
	"errors"
	"time"

	zuma_api "code.justin.tv/chat/zuma/app/api"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend/anonymous_message"
	"code.justin.tv/commerce/prism/backend/automod"
	"code.justin.tv/commerce/prism/backend/clients/stats"
	"code.justin.tv/commerce/prism/backend/consumable"
	"code.justin.tv/commerce/prism/backend/datascience"
	transaction_dynamo "code.justin.tv/commerce/prism/backend/dynamo/transaction"
	"code.justin.tv/commerce/prism/backend/landlord"
	"code.justin.tv/commerce/prism/backend/models"
	apimodel "code.justin.tv/commerce/prism/backend/models/api"
	"code.justin.tv/commerce/prism/backend/tenant"
	"code.justin.tv/commerce/prism/backend/transaction/common"
	"code.justin.tv/commerce/prism/backend/transaction/validation"
	"code.justin.tv/commerce/prism/backend/utils"
	"code.justin.tv/commerce/prism/config"
	prism "code.justin.tv/commerce/prism/rpc/models"
	"github.com/twitchtv/twirp"
)

// ValidationResp is the result of the Validator's Validate function
type ValidationResp struct {
	ValidationErr                 error
	DynamoUpdateErr               error
	TenantTokenizeAndValidateErrs map[config.TenantName]error
	Tokens                        map[config.TenantName][]*tenant.Token
	AutoModProcessed              bool
}

// Validator represents a transaction validator
type Validator interface {
	Validate(ctx context.Context, tx *transaction_dynamo.Transaction) ValidationResp
}

type validator struct {
	Landlord                     landlord.Landlord                              `inject:""`
	TransactionDAO               transaction_dynamo.DAO                         `inject:""`
	AnonymousMessageErrorHandler anonymous_message.AnonymousMessageErrorHandler `inject:""`
	ZumaValidator                validation.ZumaValidator                       `inject:""`
	AutoModProcessor             automod.Processor                              `inject:""`
	DataScienceTracker           datascience.Tracker                            `inject:""`
	Stats                        stats.Statter                                  `inject:""`
}

// NewValidator creates a new transaction Validator
func NewValidator() Validator {
	return &validator{}
}

func (v *validator) Validate(ctx context.Context, tx *transaction_dynamo.Transaction) (resp ValidationResp) {
	startTime := time.Now()
	defer func() {
		latency := time.Since(startTime)

		v.Stats.TimingDuration("transaction_processing_step__validate_latency", latency, 1)

		v.Stats.Inc("transaction_processing_step__validate_errors", common.ErrCount(resp.ValidationErr), 1)

		twErr := utils.ToTwirpError(resp.ValidationErr)
		if utils.IsA5XXError(twErr) {
			v.Stats.Inc("transaction_processing_step__5XX_validate_errors", common.ErrCount(resp.ValidationErr), 1)
		} else {
			v.Stats.Inc("transaction_processing_step__4XX_validate_errors", common.ErrCount(resp.ValidationErr), 1)
		}
	}()

	if tx == nil {
		return ValidationResp{ValidationErr: errors.New("transaction cannot be nil")}
	}

	if tx.ProcessingStatus != transaction_dynamo.StatusInProgressStarted {
		return ValidationResp{ValidationErr: errors.New("transaction is not in the correct status")}
	}

	if hasEmptyConsumable(tx.ConsumablesRequested) {
		log.WithFields(log.Fields{
			"userID":        tx.UserID,
			"channelID":     tx.ChannelID,
			"transactionID": tx.TransactionID,
		}).Warn("requested an empty consumable")

		v.trackCheerZeroMessageEvent(ctx, tx)
		return ValidationResp{ValidationErr: twirp.NewError(twirp.InvalidArgument, "transaction must have at least one consumable requested")}
	}

	zumaErrChan := make(chan error)
	go func() {
		zumaErrChan <- v.ZumaValidator.PerformZumaValidation(ctx, *tx)
	}()

	// Tokenize and validate the request
	tenantResps := v.Landlord.TokenizeAndValidate(ctx, &tenant.TokenizeAndValidateReq{
		UserID:               tx.UserID,
		ChannelID:            tx.ChannelID,
		RoomID:               tx.RoomID,
		MessageID:            tx.TransactionID,
		Message:              tx.RequestedMessage,
		ConsumablesRequested: tx.ConsumablesRequested,
		Anonymous:            tx.Anonymous,
	})

	tokens := make(map[config.TenantName][]*tenant.Token)
	tenantErrors := make(map[config.TenantName]error)
	originalRequestedMessage := tx.RequestedMessage
	isMessageAnonymized := false
	var validationErr error
	for tenantName, resp := range tenantResps {
		if resp.Error != nil {
			// Check if the error is related to anonymous messages, and if so, handle it appropriately
			if isAnonMessageErr, anonErr := v.AnonymousMessageErrorHandler.Handle(ctx, tx, resp.Error); isAnonMessageErr {
				// A nil anon error in this case indicates that validation should ignore the error
				if anonErr != nil {
					tx.ProcessingStatus = transaction_dynamo.StatusFailedAnonymousMessageValidation
					return ValidationResp{
						ValidationErr:   anonErr,
						DynamoUpdateErr: v.TransactionDAO.CreateOrUpdate(tx),
					}
				}
				isMessageAnonymized = originalRequestedMessage != tx.RequestedMessage
			} else {
				tenantErrors[tenantName] = resp.Error

				if validationErr == nil {
					validationErr = resp.Error
				} else {
					//We want to return 400 errors to clients before 500s
					validationErr = utils.PrioritizeByLowestCode(validationErr, resp.Error)
				}
			}
		}
		tokens[tenantName] = resp.Tokens
	}

	if len(tenantErrors) < 1 {
		tx.ProcessingStatus = transaction_dynamo.StatusCompletedAllTenantsTokenizeAndValidateSuccessfully
	} else if len(tenantErrors) > 0 && len(tenantErrors) < len(tenantResps) {
		log.Warn("some tenants encountered tokenize and validate errors")
		tx.ProcessingStatus = transaction_dynamo.StatusCompletedPartialTenantTokenizeAndValidateError
		tx.TenantValidateErrors = transaction_dynamo.ConvertTenantErrorMap(tenantErrors)
	} else {
		log.Warn("all tenants encountered tokenize and validate errors")
		tx.ProcessingStatus = transaction_dynamo.StatusCompletedAllTenantsEncounteredTokenizeAndValidateErrors
		tx.TenantValidateErrors = transaction_dynamo.ConvertTenantErrorMap(tenantErrors)
	}

	if validationErr != nil {
		return ValidationResp{
			ValidationErr:                 validationErr,
			DynamoUpdateErr:               v.TransactionDAO.CreateOrUpdate(tx),
			Tokens:                        tokens,
			TenantTokenizeAndValidateErrs: tenantErrors,
		}
	}

	// Wait for zuma to respond
	select {
	case zumaErr := <-zumaErrChan:
		validationErr = zumaErr
	case <-ctx.Done():
		log.Error("timed out waiting on zuma response")
		return ValidationResp{
			ValidationErr:                 errors.New("timed out waiting on zuma response"),
			DynamoUpdateErr:               v.TransactionDAO.CreateOrUpdate(tx),
			Tokens:                        tokens,
			TenantTokenizeAndValidateErrs: tenantErrors,
		}
	}

	// Peform AutoMod validation
	autoModProcessed := false
	if validationErr != nil {
		currentProcessingStatus := tx.ProcessingStatus
		tx.ProcessingStatus = transaction_dynamo.StatusFailedZumaValidation

		// Handle AutoMod
		var zumaResp apimodel.CreateEventErrorResponse
		parsingErr := prism.ParseClientError(validationErr, &zumaResp)
		if parsingErr == nil && zumaResp.Status == apimodel.AutoModMessage {
			if isMessageAnonymized {
				// If the message was anonymized, ignore AutoMod as text will have been stripped out at this stage
				validationErr = nil
				// Go back to the previous processing status
				tx.ProcessingStatus = currentProcessingStatus
			} else if tx.ShouldSendAnyway {
				fragments, err := validation.ExportMessageFragments(zumaResp.Data.EnforceMessageResponse.Content)
				if err != nil {
					msg := "Failed to extract message fragments from zuma response"
					log.WithError(err).Error(msg)
					validationErr = errors.New(msg)
				} else {
					err = v.AutoModProcessor.Process(ctx, tx, tokens, zumaResp.Data.Automod, fragments)
					if err != nil {
						msg := "Failed to start AutoMod process"
						log.WithError(err).Error(msg)
						validationErr = errors.New(msg)
					} else {
						autoModProcessed = true
						validationErr = nil
					}
				}
			} else {
				v.trackAutoModFlaggedMessageEvent(ctx, tx, zumaResp.Data.Automod)
			}
		}
	}

	return ValidationResp{
		ValidationErr:                 validationErr,
		DynamoUpdateErr:               v.TransactionDAO.CreateOrUpdate(tx),
		Tokens:                        tokens,
		TenantTokenizeAndValidateErrs: tenantErrors,
		AutoModProcessed:              autoModProcessed,
	}
}

func (v *validator) trackAutoModFlaggedMessageEvent(ctx context.Context, tx *transaction_dynamo.Transaction, autoModResp *zuma_api.AutoModResponse) {
	// This data science event is so we can track people trying to game the automod system
	dataScienceEvent := &models.BitsAutoModFlaggedMessageEventProperties{
		ChannelID:                   tx.ChannelID,
		UserID:                      tx.UserID,
		AutoModReason:               autoModResp.Reason,
		Message:                     tx.RequestedMessage,
		TransactionID:               tx.TransactionID,
		DataScienceCommonProperties: v.DataScienceTracker.GetDataScienceCommonProperties(ctx),
	}

	// TODO: Support non-bits consumable types
	if _, ok := tx.ConsumablesRequested[consumable.ConsumableType_Bits]; ok {
		dataScienceEvent.UsedTotal = int(tx.ConsumablesRequested[consumable.ConsumableType_Bits][consumable.Consumable_Bits])
	}

	go func(e *models.BitsAutoModFlaggedMessageEventProperties) {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		dataScienceEvent.UserLogin = v.DataScienceTracker.GetUserLoginForDataScience(ctx, tx.UserID)
		dataScienceEvent.ChannelLogin = v.DataScienceTracker.GetUserLoginForDataScience(ctx, tx.ChannelID)

		err := v.DataScienceTracker.TrackEvent(ctx, models.BitsAutoModFlaggedMessageEventName, dataScienceEvent, tx.UserID, tx.ChannelID)
		if err != nil {
			log.WithError(err).Error("Error tracking automod flagging event with data science")
		}
	}(dataScienceEvent)
}

func (v *validator) trackCheerZeroMessageEvent(ctx context.Context, tx *transaction_dynamo.Transaction) {
	// This data science event is so we can track people trying to send cheer0s
	dataScienceEvent := &models.BitsCheerZeroEvent{
		ChannelID:                   tx.ChannelID,
		UserID:                      tx.UserID,
		Message:                     tx.RequestedMessage,
		TransactionID:               tx.TransactionID,
		DataScienceCommonProperties: v.DataScienceTracker.GetDataScienceCommonProperties(ctx),
	}

	// Only track cheer0s, not other consumable0 messages
	consumableMap, ok := tx.ConsumablesRequested[prism.ConsumableType_Bits]
	if !ok {
		return
	}

	amount, ok := consumableMap[prism.Consumable_Bits]
	if !ok || amount > 0 {
		return
	}

	go func(e *models.BitsCheerZeroEvent) {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		err := v.DataScienceTracker.TrackEvent(ctx, models.BitsCheerZeroEventName, dataScienceEvent, tx.UserID, tx.ChannelID)
		if err != nil {
			log.WithError(err).Error("Error tracking cheer0 event with data science")
		}

		v.Stats.Inc("cheer_zero_count", 1, 1)
	}(dataScienceEvent)
}

func hasEmptyConsumable(consumablesRequested models.ConsumableMap) bool {
	for _, consumableMap := range consumablesRequested {
		for _, requestedAmount := range consumableMap {
			if requestedAmount == 0 {
				return true
			}
		}
	}
	return false
}
