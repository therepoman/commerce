package common

// ErrCount returns 1 if an error is not nil, 0 otherwise
func ErrCount(err error) int64 {
	if err != nil {
		return 1
	}
	return 0
}
