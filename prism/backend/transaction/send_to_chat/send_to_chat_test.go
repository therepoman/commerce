package send_to_chat

import (
	"context"
	"errors"
	"testing"

	dynamo_transaction "code.justin.tv/commerce/prism/backend/dynamo/transaction"
	"code.justin.tv/commerce/prism/config"
	stats_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/stats"
	transaction_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/dynamo/transaction"
	message_router_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/message"
	transformation_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/transformation"
	prism "code.justin.tv/commerce/prism/rpc"
	prism_models "code.justin.tv/commerce/prism/rpc/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestSender_SendToChat(t *testing.T) {
	Convey("Given a sender", t, func() {
		dao := new(transaction_mock.DAO)
		router := new(message_router_mock.Router)
		transformer := new(transformation_mock.Transformer)
		stats := new(stats_mock.Statter)

		sender := sender{
			MessageRouter:  router,
			Transformer:    transformer,
			TransactionDAO: dao,
			Stats:          stats,
		}

		stats.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything).Return()
		stats.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return()

		Convey("Error for a nil transaction", func() {
			resp := sender.SendToChat(context.Background(), nil, map[config.TenantName][]*prism.Transformation{})
			So(resp.OverallErr, ShouldNotBeNil)
		})

		Convey("Error for a transaction in the wrong status", func() {
			resp := sender.SendToChat(context.Background(), &dynamo_transaction.Transaction{
				ProcessingStatus: dynamo_transaction.StatusInProgressStarted,
			}, map[config.TenantName][]*prism.Transformation{})
			So(resp.OverallErr, ShouldNotBeNil)
		})

		Convey("When there are no transformations", func() {
			transformations := make(map[config.TenantName][]*prism.Transformation)

			Convey("When TMI fails", func() {
				router.On("RouteMessage", mock.Anything, mock.Anything).Return(prism_models.Route(""), "", errors.New("test error"))

				Convey("When DAO update errors", func() {
					tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusCompletedAllTenantsProcessedSuccessfully}
					dao.On("CreateOrUpdate", mock.Anything).Return(errors.New("test error"))
					resp := sender.SendToChat(context.Background(), tx, transformations)
					So(resp.OverallErr, ShouldBeNil)
					So(resp.DynamoUpdateErr, ShouldNotBeNil)
					So(resp.ChatSendErr, ShouldNotBeNil)
					So(tx.ChatStatus, ShouldEqual, dynamo_transaction.StatusFailedToSendMessageToChat)
					So(len(resp.TransformationErrs), ShouldEqual, 0)
					So(len(tx.TransformationErrors), ShouldEqual, 0)
				})

				Convey("When DAO update succeeds", func() {
					tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusCompletedAllTenantsProcessedSuccessfully}
					dao.On("CreateOrUpdate", mock.Anything).Return(nil)
					resp := sender.SendToChat(context.Background(), tx, transformations)
					So(resp.OverallErr, ShouldBeNil)
					So(resp.DynamoUpdateErr, ShouldBeNil)
					So(resp.ChatSendErr, ShouldNotBeNil)
					So(tx.ChatStatus, ShouldEqual, dynamo_transaction.StatusFailedToSendMessageToChat)
					So(len(resp.TransformationErrs), ShouldEqual, 0)
					So(len(tx.TransformationErrors), ShouldEqual, 0)
				})
			})

			Convey("When TMI succeeds", func() {
				router.On("RouteMessage", mock.Anything, mock.Anything).Return(prism_models.TMI_ROUTE, "msg", nil)

				Convey("When DAO update errors", func() {
					tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusCompletedAllTenantsProcessedSuccessfully}
					dao.On("CreateOrUpdate", mock.Anything).Return(errors.New("test error"))
					resp := sender.SendToChat(context.Background(), tx, transformations)
					So(resp.OverallErr, ShouldBeNil)
					So(resp.DynamoUpdateErr, ShouldNotBeNil)
					So(tx.ChatStatus, ShouldEqual, dynamo_transaction.StatusSuccesfullySentMessageToChat)
					So(len(resp.TransformationErrs), ShouldEqual, 0)
					So(len(tx.TransformationErrors), ShouldEqual, 0)
				})

				Convey("When DAO update succeeds", func() {
					tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusCompletedAllTenantsProcessedSuccessfully}
					dao.On("CreateOrUpdate", mock.Anything).Return(nil)
					resp := sender.SendToChat(context.Background(), tx, transformations)
					So(resp.OverallErr, ShouldBeNil)
					So(resp.DynamoUpdateErr, ShouldBeNil)
					So(tx.ChatStatus, ShouldEqual, dynamo_transaction.StatusSuccesfullySentMessageToChat)
					So(len(resp.TransformationErrs), ShouldEqual, 0)
					So(len(tx.TransformationErrors), ShouldEqual, 0)
				})
			})
		})

		Convey("When the transformations error", func() {
			transformations := map[config.TenantName][]*prism.Transformation{
				config.TenantName("test-tenant-1"): {
					&prism.Transformation{},
					&prism.Transformation{},
				},
				config.TenantName("test-tenant-2"): {
					&prism.Transformation{},
				},
			}
			transformer.On("Transform", mock.Anything, mock.Anything).Return("", errors.New("test error"))

			Convey("When TMI fails", func() {
				router.On("RouteMessage", mock.Anything, mock.Anything).Return(prism_models.Route(""), "", errors.New("test error"))

				Convey("When DAO update errors", func() {
					tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusCompletedAllTenantsProcessedSuccessfully}
					dao.On("CreateOrUpdate", mock.Anything).Return(errors.New("test error"))
					resp := sender.SendToChat(context.Background(), tx, transformations)
					So(resp.OverallErr, ShouldBeNil)
					So(resp.DynamoUpdateErr, ShouldNotBeNil)
					So(resp.ChatSendErr, ShouldNotBeNil)
					So(tx.ChatStatus, ShouldEqual, dynamo_transaction.StatusFailedToSendMessageToChat)
					So(len(resp.TransformationErrs), ShouldEqual, 2)
					So(len(tx.TransformationErrors), ShouldEqual, 2)
				})

				Convey("When DAO update succeeds", func() {
					tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusCompletedAllTenantsProcessedSuccessfully}
					dao.On("CreateOrUpdate", mock.Anything).Return(nil)
					resp := sender.SendToChat(context.Background(), tx, transformations)
					So(resp.OverallErr, ShouldBeNil)
					So(resp.DynamoUpdateErr, ShouldBeNil)
					So(resp.ChatSendErr, ShouldNotBeNil)
					So(tx.ChatStatus, ShouldEqual, dynamo_transaction.StatusFailedToSendMessageToChat)
					So(len(resp.TransformationErrs), ShouldEqual, 2)
					So(len(tx.TransformationErrors), ShouldEqual, 2)
				})
			})

			Convey("When TMI succeeds", func() {
				router.On("RouteMessage", mock.Anything, mock.Anything).Return(prism_models.TMI_ROUTE, "msg", nil)

				Convey("When DAO update errors", func() {
					tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusCompletedAllTenantsProcessedSuccessfully}
					dao.On("CreateOrUpdate", mock.Anything).Return(errors.New("test error"))
					resp := sender.SendToChat(context.Background(), tx, transformations)
					So(resp.OverallErr, ShouldBeNil)
					So(resp.DynamoUpdateErr, ShouldNotBeNil)
					So(tx.ChatStatus, ShouldEqual, dynamo_transaction.StatusSuccesfullySentMessageToChat)
					So(len(resp.TransformationErrs), ShouldEqual, 2)
					So(len(tx.TransformationErrors), ShouldEqual, 2)
				})

				Convey("When DAO update succeeds", func() {
					tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusCompletedAllTenantsProcessedSuccessfully}
					dao.On("CreateOrUpdate", mock.Anything).Return(nil)
					resp := sender.SendToChat(context.Background(), tx, transformations)
					So(resp.OverallErr, ShouldBeNil)
					So(resp.DynamoUpdateErr, ShouldBeNil)
					So(tx.ChatStatus, ShouldEqual, dynamo_transaction.StatusSuccesfullySentMessageToChat)
					So(len(resp.TransformationErrs), ShouldEqual, 2)
					So(len(tx.TransformationErrors), ShouldEqual, 2)
				})
			})
		})

		Convey("When the transformations succeed", func() {
			transformations := map[config.TenantName][]*prism.Transformation{
				config.TenantName("test-tenant-1"): {
					&prism.Transformation{},
					&prism.Transformation{},
				},
				config.TenantName("test-tenant-2"): {
					&prism.Transformation{},
				},
			}
			transformer.On("Transform", mock.Anything, mock.Anything).Return("", nil)

			Convey("When TMI fails", func() {
				router.On("RouteMessage", mock.Anything, mock.Anything).Return(prism_models.Route(""), "", errors.New("test error"))

				Convey("When DAO update errors", func() {
					tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusCompletedAllTenantsProcessedSuccessfully}
					dao.On("CreateOrUpdate", mock.Anything).Return(errors.New("test error"))
					resp := sender.SendToChat(context.Background(), tx, transformations)
					So(resp.OverallErr, ShouldBeNil)
					So(resp.DynamoUpdateErr, ShouldNotBeNil)
					So(resp.ChatSendErr, ShouldNotBeNil)
					So(tx.ChatStatus, ShouldEqual, dynamo_transaction.StatusFailedToSendMessageToChat)
					So(len(resp.TransformationErrs), ShouldEqual, 0)
					So(len(tx.TransformationErrors), ShouldEqual, 0)
				})

				Convey("When DAO update succeeds", func() {
					tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusCompletedAllTenantsProcessedSuccessfully}
					dao.On("CreateOrUpdate", mock.Anything).Return(nil)
					resp := sender.SendToChat(context.Background(), tx, transformations)
					So(resp.OverallErr, ShouldBeNil)
					So(resp.DynamoUpdateErr, ShouldBeNil)
					So(resp.ChatSendErr, ShouldNotBeNil)
					So(tx.ChatStatus, ShouldEqual, dynamo_transaction.StatusFailedToSendMessageToChat)
					So(len(resp.TransformationErrs), ShouldEqual, 0)
					So(len(tx.TransformationErrors), ShouldEqual, 0)
				})
			})

			Convey("When TMI succeeds", func() {
				router.On("RouteMessage", mock.Anything, mock.Anything).Return(prism_models.TMI_ROUTE, "", nil)

				Convey("When DAO update errors", func() {
					tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusCompletedAllTenantsProcessedSuccessfully}
					dao.On("CreateOrUpdate", mock.Anything).Return(errors.New("test error"))
					resp := sender.SendToChat(context.Background(), tx, transformations)
					So(resp.OverallErr, ShouldBeNil)
					So(resp.DynamoUpdateErr, ShouldNotBeNil)
					So(tx.ChatStatus, ShouldEqual, dynamo_transaction.StatusSuccesfullySentMessageToChat)
					So(len(resp.TransformationErrs), ShouldEqual, 0)
					So(len(tx.TransformationErrors), ShouldEqual, 0)
				})

				Convey("When DAO update succeeds", func() {
					tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusCompletedAllTenantsProcessedSuccessfully}
					dao.On("CreateOrUpdate", mock.Anything).Return(nil)
					resp := sender.SendToChat(context.Background(), tx, transformations)
					So(resp.OverallErr, ShouldBeNil)
					So(resp.DynamoUpdateErr, ShouldBeNil)
					So(tx.ChatStatus, ShouldEqual, dynamo_transaction.StatusSuccesfullySentMessageToChat)
					So(len(resp.TransformationErrs), ShouldEqual, 0)
					So(len(tx.TransformationErrors), ShouldEqual, 0)
				})
			})
		})
	})
}
