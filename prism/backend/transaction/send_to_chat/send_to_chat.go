package send_to_chat

import (
	"context"
	"errors"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend/clients/stats"
	transaction_dynamo "code.justin.tv/commerce/prism/backend/dynamo/transaction"
	"code.justin.tv/commerce/prism/backend/message"
	"code.justin.tv/commerce/prism/backend/transaction/common"
	"code.justin.tv/commerce/prism/backend/transformation"
	"code.justin.tv/commerce/prism/backend/utils"
	"code.justin.tv/commerce/prism/config"
	prism "code.justin.tv/commerce/prism/rpc"
)

// Sender represents a transaction chat message sender
type Sender interface {
	SendToChat(ctx context.Context, tx *transaction_dynamo.Transaction, transformations map[config.TenantName][]*prism.Transformation) SendToChatResp
}

type sender struct {
	MessageRouter  message.Router             `inject:""`
	Transformer    transformation.Transformer `inject:""`
	TransactionDAO transaction_dynamo.DAO     `inject:""`
	Stats          stats.Statter              `inject:""`
}

// NewSender creates a new transaction chat message sender
func NewSender() Sender {
	return &sender{}
}

// SendToChatResp is the response of the Sender's SentToChat function
type SendToChatResp struct {
	OverallErr         error
	TransformationErrs map[config.TenantName][]error
	ChatSendErr        error
	DynamoUpdateErr    error
}

func (s *sender) SendToChat(ctx context.Context, tx *transaction_dynamo.Transaction, transformations map[config.TenantName][]*prism.Transformation) (resp SendToChatResp) {
	startTime := time.Now()
	defer func() {
		latency := time.Since(startTime)

		s.Stats.TimingDuration("transaction_processing_step__send_to_chat_latency", latency, 1)

		s.Stats.Inc("transaction_processing_step__send_to_chat_errors", common.ErrCount(resp.OverallErr), 1)

		twErr := utils.ToTwirpError(resp.OverallErr)
		if utils.IsA5XXError(twErr) {
			s.Stats.Inc("transaction_processing_step__5XX_send_to_chat_errors", common.ErrCount(resp.OverallErr), 1)
		} else {
			s.Stats.Inc("transaction_processing_step__4XX_send_to_chat_errors", common.ErrCount(resp.OverallErr), 1)
		}
	}()

	if tx == nil {
		return SendToChatResp{OverallErr: errors.New("transaction cannot be nil")}
	}

	_, isAcceptedStatus := map[transaction_dynamo.ProcessingStatus]interface{}{
		transaction_dynamo.StatusCompletedAllTenantsProcessedSuccessfully: nil,
		transaction_dynamo.StatusCompletedPartialTenantProcessingError:    nil,
	}[tx.ProcessingStatus]
	if !isAcceptedStatus {
		return SendToChatResp{OverallErr: errors.New("transaction is not in the correct status")}
	}

	transformationErrors := make(map[config.TenantName][]error)
	msg := tx.RequestedMessage
	for tenantName, tenantTransformations := range transformations {
		tenantTransformationErrs := make([]error, 0)
		for _, t := range tenantTransformations {
			var err error
			msg, err = s.Transformer.Transform(msg, t)
			if err != nil {
				log.WithField("tenant_name", tenantName.String()).WithError(err).Error("error applying transformation")
				tenantTransformationErrs = append(tenantTransformationErrs, err)
			}
			if len(tenantTransformationErrs) > 0 {
				transformationErrors[tenantName] = tenantTransformationErrs
			}
		}
	}
	tx.ProcessedMessage = msg
	tx.TransformationErrors = transaction_dynamo.ConvertTenantErrorListMap(transformationErrors)

	route, sentMsg, err := s.MessageRouter.RouteMessage(ctx, *tx)
	if err != nil {
		tx.ChatStatus = transaction_dynamo.StatusFailedToSendMessageToChat
	} else {
		tx.SentMessage = sentMsg
		tx.ChatStatus = transaction_dynamo.StatusSuccesfullySentMessageToChat
	}

	tx.MessageRoute = route
	tx.EndTime = time.Now()

	return SendToChatResp{
		ChatSendErr:        err,
		TransformationErrs: transformationErrors,
		DynamoUpdateErr:    s.TransactionDAO.CreateOrUpdate(tx),
	}
}
