package validation

import (
	"context"
	"encoding/json"
	"strings"

	zumaapi "code.justin.tv/chat/zuma/app/api"
	zuma "code.justin.tv/chat/zuma/client"
	"code.justin.tv/chat/zuma/rpc/maap"
	log "code.justin.tv/commerce/logrus"
	transaction_dynamo "code.justin.tv/commerce/prism/backend/dynamo/transaction"
	apimodel "code.justin.tv/commerce/prism/backend/models/api"
	prism "code.justin.tv/commerce/prism/rpc/models"
	"code.justin.tv/feeds/feeds-common/entity"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/twitchtv/twirp"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var enforcementRules = zumaapi.EnforcementRuleset{
	AutoMod:             true,
	Banned:              true,
	Blocked:             true,
	ChannelBlockedTerms: true,
	DisposableEmail:     true,
	EmotesOnly:          true,
	Entropy:             true,
	FollowersOnly:       true,
	Spam:                true,
	SubscribersOnly:     true,
	Suspended:           true,
	VerifiedOnly:        true,
	Zalgo:               true,
}

// ZumaValidator represents a Zuma validator
type ZumaValidator interface {
	PerformZumaValidation(ctx context.Context, tx transaction_dynamo.Transaction) error
}

type zumaValidator struct {
	ZumaClient zuma.Client `inject:""`
}

// NewZumaValidator creates a new ZumaValidator
func NewZumaValidator() ZumaValidator {
	return &zumaValidator{}
}

// NewTestZumaValidator creates a new test ZumaValidator
func NewTestZumaValidator(zumaClient zuma.Client) ZumaValidator {
	return &zumaValidator{
		ZumaClient: zumaClient,
	}
}

func (zv *zumaValidator) PerformZumaValidation(ctx context.Context, tx transaction_dynamo.Transaction) error {
	containerOwner := entity.New(entity.NamespaceUser, tx.ChannelID)

	extractMessageReq := zumaapi.EnforceMessageRequest{
		// ShouldSendAnyway in most cases is false. Only when the message is anounymous cheering/caught by AutoMod and with bits it can be true.
		SkipExtraction:   !tx.ShouldSendAnyway,
		SenderID:         tx.UserID,
		MessageText:      tx.RequestedMessage,
		ContainerOwner:   &containerOwner,
		EnforcementRules: enforcementRules,
		ParseCheermotes:  tx.ShouldSendAnyway, // when the message is caught by automod, get the cheermote fragment from Zuma and pass it to AutoMod service.
		EnforcementConfig: zumaapi.EnforcementConfiguration{
			CheckAllyClassification: aws.Bool(true),
		},
	}

	zumaResponse, err := zv.ZumaClient.EnforceMessage(ctx, extractMessageReq, nil)

	if err != nil {
		msg := "error getting zuma eligibility from zuma service"

		if ctx.Err() == nil {
			//Only log if the call to zuma had time to complete
			log.WithError(err).Error(msg)
		}

		return twirp.InternalError(msg)
	}

	if !zumaResponse.AllPassed {
		reasons := []string{""}
		var err error

		if zumaResponse.Banned != nil && !zumaResponse.Banned.Passed {
			reasons = append(reasons, "zuma_banned")

			errorResponse := apimodel.CreateEventErrorResponse{
				Status:  apimodel.UserBanned,
				Message: apimodel.UserBannedInChannelError,
			}

			err = prism.NewClientError(twirp.NewError(twirp.InvalidArgument, apimodel.UserBannedInChannelError), errorResponse)
		}

		if zumaResponse.Suspended != nil && !zumaResponse.Suspended.Passed {
			reasons = append(reasons, "zuma_suspended")

			errorResponse := apimodel.CreateEventErrorResponse{
				Status:  apimodel.UserSuspended,
				Message: apimodel.UserSuspendedError,
			}

			err = prism.NewClientError(twirp.NewError(twirp.InvalidArgument, apimodel.UserSuspendedError), errorResponse)
		}

		if zumaResponse.ChannelBlockedTerms != nil && !zumaResponse.ChannelBlockedTerms.Passed {
			reasons = append(reasons, "zuma_channel_blocked_terms")

			errorResponse := apimodel.CreateEventErrorResponse{
				Status:  apimodel.ChannelBlockedTerms,
				Message: apimodel.ChannelBlockedTermsError,
			}

			err = prism.NewClientError(twirp.NewError(twirp.InvalidArgument, apimodel.ChannelBlockedTermsError), errorResponse)
		}

		if zumaResponse.AutoMod != nil && !zumaResponse.AutoMod.Passed {
			reasons = append(reasons, "zuma_automod")

			if !tx.AutomodAcknowledged {
				errorResponse := apimodel.CreateEventErrorResponse{
					Status:  apimodel.AutoModMessage,
					Message: apimodel.AutoModError,
					Data: apimodel.CreateEventErrorData{
						Automod:                zumaResponse.AutoMod,
						EnforceMessageResponse: &zumaResponse,
					},
				}

				err = prism.NewClientError(twirp.NewError(twirp.InvalidArgument, apimodel.AutoModError), errorResponse)
			}
		}

		if zumaResponse.Blocked != nil && !zumaResponse.Blocked.Passed {
			reasons = append(reasons, "zuma_blocked")
		}

		if zumaResponse.DisposableEmail != nil && !zumaResponse.DisposableEmail.Passed {
			reasons = append(reasons, "zuma_email")
		}

		if zumaResponse.EmoteOnly != nil && !zumaResponse.EmoteOnly.Passed {
			reasons = append(reasons, "zuma_emote")
		}

		if zumaResponse.Entropy != nil && !zumaResponse.Entropy.Passed {
			reasons = append(reasons, "zuma_entropy")
		}

		if zumaResponse.FollowersOnly != nil && !zumaResponse.FollowersOnly.Passed {
			reasons = append(reasons, "zuma_followers")
		}

		if zumaResponse.Spam != nil && !zumaResponse.Spam.Passed {
			reasons = append(reasons, "zuma_spam")
		}

		if zumaResponse.SubscribersOnly != nil && !zumaResponse.SubscribersOnly.Passed {
			reasons = append(reasons, "zuma_subs")
		}

		if zumaResponse.VerifiedOnly != nil && !zumaResponse.VerifiedOnly.Passed {
			reasons = append(reasons, "zuma_verified")
		}

		fields := log.Fields{
			"reasons": strings.Join(reasons, " "),
		}
		// EnforceMessageResponse has many pointers in its response types, which are not useful
		// to log without dereferencing. It is easiest to attempt to marshal it into JSON.
		encodedResponse, marshalError := json.Marshal(zumaResponse)
		if marshalError != nil {
			// Fall back to logging the unaltered zuma response. Additionally log a warning that we
			// couldn't marshal the response.
			fields["zumaResponse"] = zumaResponse
			log.WithField("zumaResponse", zumaResponse).Warn("could not marshal zuma response into JSON")
		} else {
			fields["cleanedZumaResponse"] = string(encodedResponse)
		}
		log.WithFields(fields).Info("zuma_rejected")

		return err
	}

	return nil
}

// ExportMessageFragments is a conversion function to achieve the maap version of messageContent
func ExportMessageFragments(content zumaapi.MessageContentV2) ([]*maap.MessageContentFragment, error) {
	fragments := []*maap.MessageContentFragment{}
	for _, f := range content.Fragments {
		var emoticonFragment *maap.MessageContentFragmentEmoticon
		if f.Emoticon != nil {
			emoticonFragment = &maap.MessageContentFragmentEmoticon{
				EmoticonID:    f.Emoticon.EmoticonID,
				EmoticonSetID: f.Emoticon.EmoticonSetID,
			}
		}

		var cheermoteFragment *maap.MessageContentFragmentCheermote
		if f.Cheermote != nil {
			cheermoteFragment = &maap.MessageContentFragmentCheermote{
				Prefix:     f.Cheermote.Prefix,
				BitsAmount: int64(f.Cheermote.BitsAmount),
				Tier:       int64(f.Cheermote.Tier),
			}
		}

		var linkFragment *maap.MessageContentFragmentLink
		if f.Link != nil {
			em, err := exportEmbed(f.Link.EmbedMetadata)
			if err != nil {
				return nil, err
			}
			linkFragment = &maap.MessageContentFragmentLink{
				Host:          f.Link.Host,
				EmbedMetadata: em,
			}
		}

		var mentionFragment *maap.MessageContentFragmentMention
		if f.Mention != nil {
			mentionFragment = &maap.MessageContentFragmentMention{
				UserID:      f.Mention.UserID,
				Login:       f.Mention.Login,
				DisplayName: f.Mention.DisplayName,
			}
		}

		var autoModFragment *maap.MessageContentFragmentAutoMod
		if f.AutoMod != nil {
			topics := make(map[string]int64, len(f.AutoMod.Topics))
			for topic, risk := range f.AutoMod.Topics {
				topics[topic] = int64(risk)
			}

			autoModFragment = &maap.MessageContentFragmentAutoMod{
				Topics: topics,
			}
		}

		fragments = append(fragments, &maap.MessageContentFragment{
			Text:        f.Text,
			Emoticon:    emoticonFragment,
			Cheermote:   cheermoteFragment,
			Link:        linkFragment,
			UserMention: mentionFragment,
			Automod:     autoModFragment,
		})
	}

	return fragments, nil
}

func exportEmbed(e *zumaapi.MessageContentFragmentLinkEmbed) (*maap.MessageContentFragmentLinkEmbed, error) {
	if e == nil {
		return nil, nil
	}

	embed := &maap.MessageContentFragmentLinkEmbed{
		ErrorCode:    e.ErrorCode,
		RequestUrl:   e.RequestURL,
		Type:         e.Type,
		Title:        e.Title,
		Description:  e.Description,
		AuthorName:   e.AuthorName,
		AuthorUrl:    e.AuthorURL,
		ThumbnailUrl: e.ThumbnailURL,
		ProviderName: e.ProviderName,
	}

	if md := e.TwitchMetadata; md != nil {
		if ttValue, ok := maap.TwitchType_value[strings.ToUpper(md.TwitchType)]; ok {
			var err error
			twitchMetadata := &maap.TwitchEmbedMetadata{TwitchType: maap.TwitchType(ttValue)}
			switch twitchMetadata.TwitchType {
			case maap.TwitchType_CLIP:
				twitchMetadata.Clip, err = exportTwitchClipMetadata(md.Clip)
			case maap.TwitchType_VOD:
				twitchMetadata.Vod, err = exportTwitchVODMetadata(md.VOD)
			case maap.TwitchType_STREAM:
				twitchMetadata.Stream, err = exportTwitchStreamMetadata(md.Stream)
			case maap.TwitchType_EVENT:
				twitchMetadata.Event, err = exportTwitchEventMetadata(md.Event)
			}
			if err != nil {
				return nil, err
			}
			embed.TwitchMetadata = twitchMetadata
		}
	}

	return embed, nil
}

func exportTwitchClipMetadata(clip *zumaapi.TwitchClipMetadata) (*maap.TwitchClipMetadata, error) {
	if clip == nil {
		return nil, nil
	}

	return &maap.TwitchClipMetadata{
		BroadcasterId:      clip.BroadcasterID,
		BroadcasterLogin:   clip.BroadcasterLogin,
		CreatedAt:          timestamppb.New(*clip.CreatedAt),
		Game:               clip.Game,
		VideoLengthSeconds: int32(clip.VideoLengthSeconds),
		ViewCount:          int32(clip.ViewCount),
	}, nil
}

func exportTwitchVODMetadata(vod *zumaapi.TwitchVODMetadata) (*maap.TwitchVODMetadata, error) {
	if vod == nil {
		return nil, nil
	}

	return &maap.TwitchVODMetadata{
		BroadcasterId:      vod.BroadcasterID,
		BroadcasterLogin:   vod.BroadcasterLogin,
		CreatedAt:          timestamppb.New(*vod.CreatedAt),
		Game:               vod.Game,
		VideoLengthSeconds: int32(vod.VideoLengthSeconds),
		ViewCount:          int32(vod.ViewCount),
	}, nil
}

func exportTwitchStreamMetadata(stream *zumaapi.TwitchStreamMetadata) (*maap.TwitchStreamMetadata, error) {
	if stream == nil {
		return nil, nil
	}

	return &maap.TwitchStreamMetadata{
		BroadcasterId:    stream.BroadcasterID,
		BroadcasterLogin: stream.BroadcasterLogin,
		CreatedAt:        timestamppb.New(*stream.CreatedAt),
		Game:             stream.Game,
		StreamType:       stream.StreamType,
		ViewCount:        int32(stream.ViewCount),
	}, nil
}

func exportTwitchEventMetadata(event *zumaapi.TwitchEventMetadata) (*maap.TwitchEventMetadata, error) {
	if event == nil {
		return nil, nil
	}

	return &maap.TwitchEventMetadata{
		AuthorId:    event.AuthorID,
		AuthorLogin: event.AuthorLogin,
		Game:        event.Game,
		StartTime:   timestamppb.New(*event.StartTime),
		EndTime:     timestamppb.New(*event.EndTime),
	}, nil
}
