package validation

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/chat/zuma/app/api"
	dynamo_transaction "code.justin.tv/commerce/prism/backend/dynamo/transaction"
	zuma_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/chat/zuma"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestZumaValidator_PerformZumaValidation(t *testing.T) {
	Convey("Given a ZumaValidator", t, func() {
		zumaClient := new(zuma_mock.Client)

		zumaValidator := NewTestZumaValidator(zumaClient)

		Convey("When zuma validation does not pass", func() {
			Convey("Due to zuma service outage", func() {
				zumaClient.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(api.EnforceMessageResponse{}, errors.New("test error"))

				tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusInProgressStarted}
				err := zumaValidator.PerformZumaValidation(context.Background(), *tx)
				So(err, ShouldNotBeNil)
			})

			Convey("Due to a message from a banned user", func() {
				zumaClient.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(api.EnforceMessageResponse{
					EnforceProperties: api.EnforceProperties{
						AllPassed: false,
						Banned: &api.BannedResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: false,
							},
						},
						Suspended: &api.GenericEnforcementResponse{
							Passed: true,
						},
						Zalgo: &api.GenericEnforcementResponse{
							Passed: true,
						},
						ChannelBlockedTerms: &api.ChannelBlockedTermsResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						AutoMod: &api.AutoModResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						Blocked: &api.GenericEnforcementResponse{
							Passed: true,
						},
						DisposableEmail: &api.GenericEnforcementResponse{
							Passed: true,
						},
						EmoteOnly: &api.GenericEnforcementResponse{
							Passed: true,
						},
						Entropy: &api.GenericEnforcementResponse{
							Passed: true,
						},
						FollowersOnly: &api.FollowersOnlyResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						Spam: &api.SpamResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						SubscribersOnly: &api.GenericEnforcementResponse{
							Passed: true,
						},
						VerifiedOnly: &api.GenericEnforcementResponse{
							Passed: true,
						},
					},
				}, nil)

				tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusInProgressStarted}
				err := zumaValidator.PerformZumaValidation(context.Background(), *tx)
				So(err, ShouldNotBeNil)
			})

			Convey("Due to a message from a suspended user", func() {
				zumaClient.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(api.EnforceMessageResponse{
					EnforceProperties: api.EnforceProperties{
						AllPassed: false,
						Banned: &api.BannedResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						Suspended: &api.GenericEnforcementResponse{
							Passed: false,
						},
						Zalgo: &api.GenericEnforcementResponse{
							Passed: true,
						},
						ChannelBlockedTerms: &api.ChannelBlockedTermsResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						AutoMod: &api.AutoModResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						Blocked: &api.GenericEnforcementResponse{
							Passed: true,
						},
						DisposableEmail: &api.GenericEnforcementResponse{
							Passed: true,
						},
						EmoteOnly: &api.GenericEnforcementResponse{
							Passed: true,
						},
						Entropy: &api.GenericEnforcementResponse{
							Passed: true,
						},
						FollowersOnly: &api.FollowersOnlyResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						Spam: &api.SpamResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						SubscribersOnly: &api.GenericEnforcementResponse{
							Passed: true,
						},
						VerifiedOnly: &api.GenericEnforcementResponse{
							Passed: true,
						},
					},
				}, nil)

				tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusInProgressStarted}
				err := zumaValidator.PerformZumaValidation(context.Background(), *tx)
				So(err, ShouldNotBeNil)
			})

			Convey("Due to a message containing blocked terms", func() {
				zumaClient.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(api.EnforceMessageResponse{
					EnforceProperties: api.EnforceProperties{
						AllPassed: false,
						Banned: &api.BannedResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						Suspended: &api.GenericEnforcementResponse{
							Passed: true,
						},
						Zalgo: &api.GenericEnforcementResponse{
							Passed: true,
						},
						ChannelBlockedTerms: &api.ChannelBlockedTermsResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: false,
							},
						},
						AutoMod: &api.AutoModResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						Blocked: &api.GenericEnforcementResponse{
							Passed: true,
						},
						DisposableEmail: &api.GenericEnforcementResponse{
							Passed: true,
						},
						EmoteOnly: &api.GenericEnforcementResponse{
							Passed: true,
						},
						Entropy: &api.GenericEnforcementResponse{
							Passed: true,
						},
						FollowersOnly: &api.FollowersOnlyResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						Spam: &api.SpamResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						SubscribersOnly: &api.GenericEnforcementResponse{
							Passed: true,
						},
						VerifiedOnly: &api.GenericEnforcementResponse{
							Passed: true,
						},
					},
				}, nil)

				tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusInProgressStarted}
				err := zumaValidator.PerformZumaValidation(context.Background(), *tx)
				So(err, ShouldNotBeNil)
			})

			Convey("Due to a message that was flagged by automod", func() {
				zumaClient.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(api.EnforceMessageResponse{
					EnforceProperties: api.EnforceProperties{
						AllPassed: false,
						Banned: &api.BannedResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						Suspended: &api.GenericEnforcementResponse{
							Passed: true,
						},
						Zalgo: &api.GenericEnforcementResponse{
							Passed: true,
						},
						ChannelBlockedTerms: &api.ChannelBlockedTermsResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						AutoMod: &api.AutoModResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: false,
							},
						},
						Blocked: &api.GenericEnforcementResponse{
							Passed: true,
						},
						DisposableEmail: &api.GenericEnforcementResponse{
							Passed: true,
						},
						EmoteOnly: &api.GenericEnforcementResponse{
							Passed: true,
						},
						Entropy: &api.GenericEnforcementResponse{
							Passed: true,
						},
						FollowersOnly: &api.FollowersOnlyResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						Spam: &api.SpamResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						SubscribersOnly: &api.GenericEnforcementResponse{
							Passed: true,
						},
						VerifiedOnly: &api.GenericEnforcementResponse{
							Passed: true,
						},
					},
				}, nil)

				Convey("When automod has already been acknowledged", func() {
					tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusInProgressStarted, AutomodAcknowledged: true}
					err := zumaValidator.PerformZumaValidation(context.Background(), *tx)
					So(err, ShouldBeNil)
				})

				Convey("When automod has NOT already been acknowledged", func() {
					tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusInProgressStarted, AutomodAcknowledged: false}
					err := zumaValidator.PerformZumaValidation(context.Background(), *tx)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("Due to a any other reason", func() {
				zumaClient.On("EnforceMessage", mock.Anything, mock.Anything, mock.Anything).Return(api.EnforceMessageResponse{
					EnforceProperties: api.EnforceProperties{
						AllPassed: false,
						Banned: &api.BannedResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						Suspended: &api.GenericEnforcementResponse{
							Passed: true,
						},
						Zalgo: &api.GenericEnforcementResponse{
							Passed: true,
						},
						ChannelBlockedTerms: &api.ChannelBlockedTermsResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						AutoMod: &api.AutoModResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: true,
							},
						},
						Blocked: &api.GenericEnforcementResponse{
							Passed: false,
						},
						DisposableEmail: &api.GenericEnforcementResponse{
							Passed: false,
						},
						EmoteOnly: &api.GenericEnforcementResponse{
							Passed: false,
						},
						Entropy: &api.GenericEnforcementResponse{
							Passed: false,
						},
						FollowersOnly: &api.FollowersOnlyResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: false,
							},
						},
						Spam: &api.SpamResponse{
							GenericEnforcementResponse: api.GenericEnforcementResponse{
								Passed: false,
							},
						},
						SubscribersOnly: &api.GenericEnforcementResponse{
							Passed: false,
						},
						VerifiedOnly: &api.GenericEnforcementResponse{
							Passed: false,
						},
					},
				}, nil)

				tx := &dynamo_transaction.Transaction{ProcessingStatus: dynamo_transaction.StatusInProgressStarted}
				err := zumaValidator.PerformZumaValidation(context.Background(), *tx)
				So(err, ShouldBeNil)
			})
		})
	})
}
