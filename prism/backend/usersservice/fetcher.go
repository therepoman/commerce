package usersservice

import (
	"context"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend/cache/usersservice"
	"code.justin.tv/web/users-service/client/usersclient_internal"
	"code.justin.tv/web/users-service/models"
)

// Fetcher represents a Users Service user properties fetcher
type Fetcher interface {
	Fetch(ctx context.Context, userID string) (*models.Properties, error)
}

// NewFetcher creates a new Users Service user properties fetcher
func NewFetcher() Fetcher {
	return &fetcher{}
}

type fetcher struct {
	Cache              usersservice.Cache                  `inject:""`
	UsersServiceClient usersclient_internal.InternalClient `inject:""`
}

func (f *fetcher) Fetch(ctx context.Context, userID string) (*models.Properties, error) {
	cachedUser := f.Cache.Get(userID)
	if cachedUser != nil {
		return cachedUser, nil
	}

	userProps, err := f.UsersServiceClient.GetUserByID(ctx, userID, nil)
	if err != nil || userProps == nil {
		return nil, err
	}

	err = f.Cache.Set(userID, userProps)
	if err != nil {
		log.WithError(err).WithField("userID", userID).Error("Failed to set user in users service cache")
	}

	return userProps, nil
}
