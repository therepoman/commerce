package usersservice

import (
	"context"
	"errors"
	"testing"

	usersservice_cache_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/cache/usersservice"
	usersclient_internal_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/web/users-service/client/usersclient_internal"
	"code.justin.tv/web/users-service/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestFetcher_Fetch(t *testing.T) {
	Convey("Given a UsersService Fetcher", t, func() {
		userID := "131495971"

		usersServiceCache := new(usersservice_cache_mock.Cache)
		usersServiceClient := new(usersclient_internal_mock.InternalClient)

		userFetcher := &fetcher{
			Cache:              usersServiceCache,
			UsersServiceClient: usersServiceClient,
		}

		Convey("When the cache returns a non-nil user", func() {
			usersServiceCache.On("Get", mock.Anything).Return(&models.Properties{})

			res, err := userFetcher.Fetch(context.Background(), userID)
			So(res, ShouldNotBeNil)
			So(err, ShouldBeNil)
			usersServiceClient.AssertNumberOfCalls(t, "GetUserByID", 0)
		})

		Convey("When the cache returns a nil user", func() {
			usersServiceCache.On("Get", mock.Anything).Return(nil)

			Convey("When the users service client returns an error", func() {
				usersServiceClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

				res, err := userFetcher.Fetch(context.Background(), userID)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
				usersServiceCache.AssertNumberOfCalls(t, "Set", 0)
			})

			Convey("When the users service client returns a nil user", func() {
				usersServiceClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

				res, err := userFetcher.Fetch(context.Background(), userID)
				So(res, ShouldBeNil)
				So(err, ShouldBeNil)
				usersServiceCache.AssertNumberOfCalls(t, "Set", 0)
			})

			Convey("When the users service client returns a non-nil user", func() {
				usersServiceClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).Return(&models.Properties{}, nil)
				usersServiceCache.On("Set", mock.Anything, mock.Anything).Return(nil)

				res, err := userFetcher.Fetch(context.Background(), userID)
				So(res, ShouldNotBeNil)
				So(err, ShouldBeNil)
				usersServiceCache.AssertNumberOfCalls(t, "Set", 1)
			})
		})
	})
}
