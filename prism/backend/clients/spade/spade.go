package spade

import (
	"fmt"
	"net/http"
	"net/url"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/config"
	"code.justin.tv/common/spade-client-go/spade"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
	"golang.org/x/net/context"
)

// NewClient creates a new Spade client
func NewClient(cfg *config.Config, stats statsd.Statter) (spade.Client, error) {
	if !shouldEnableSpade(cfg) {
		return new(NoopClient), nil
	}

	spadeURL, err := url.Parse(cfg.Spade.Endpoint)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse cfg.SpadeEndpoint when creating Spade client")
	}

	opts := []spade.InitFunc{
		spade.InitBaseURL(*spadeURL),
		spade.InitStatHook(func(name string, httpStatusCode int, d time.Duration) {
			err := stats.TimingDuration(fmt.Sprintf("service.spade.%s.%d", name, httpStatusCode), d, 1)
			if err != nil {
				log.WithError(err).Error("error logging spade timing metric")
			}
		}),
		spade.InitHTTPClient(&http.Client{
			Timeout: time.Duration(cfg.Spade.TimeoutMilliseconds) * time.Millisecond,
		}),
	}

	return spade.NewClient(opts...)
}

func shouldEnableSpade(cfg *config.Config) bool {
	return cfg.Spade.Endpoint != ""
}

// NoopClient implements the (code.justin.tv/common/spade-client-go).Client interface with no-op methods.
// It's useful as a dummy client when executing in an environment that should not generate Spade data.
type NoopClient struct{}

// TrackEvent implements the Spade TrackEvent method, but does nothing
func (*NoopClient) TrackEvent(ctx context.Context, event string, properties interface{}) error {
	return nil
}

// TrackEvents implements the Spade TrackEvents method, but does nothing
func (*NoopClient) TrackEvents(ctx context.Context, events ...spade.Event) error {
	return nil
}
