package zuma

type RiskCategory string

const (
	aggressiveCategory = RiskCategory("aggressive")
	sexualCategory     = RiskCategory("sexual")
	profanityCategory  = RiskCategory("profanity")
	identityCategory   = RiskCategory("identity")
	// the following 8 categories are ally categories
	AbleismCategory     = RiskCategory("ableism")
	AggressionCategory  = RiskCategory("aggression")
	HomophobiaCategory  = RiskCategory("homophobia")
	MisogynyCategory    = RiskCategory("misogyny")
	NamecallingCategory = RiskCategory("namecalling")
	RacismCategory      = RiskCategory("racism")
	SexWordsCategory    = RiskCategory("sexwords")
	SwearingCategory    = RiskCategory("swearing")
	// unknown category
	UnknownCategory = RiskCategory("unknown")
)

// Mapping between AutoMod topic ID and Risk Category
// Obtained from https://git-aws.internal.justin.tv/chat/zuma/blob/master/internal/clients/sift/models.go
var topicToCategoryMapping = map[string]RiskCategory{
	"1":  aggressiveCategory,
	"4":  sexualCategory,
	"5":  profanityCategory,
	"10": identityCategory,
}

// sift levels to shield levels.
// For sift levels, 3 is the highest level.
// For shield levels, 4 is the highest level.
var SiftRiskMapToShieldNumber map[int]int = map[int]int{7: 1, 6: 2, 5: 3, 3: 4}

func CategoryAndHighestLevelFromMostSignificantKnownTopic(topics map[string]int) (RiskCategory, int) {
	largestCategory := UnknownCategory
	largestCategoryValue := 0

	for topic, topicValue := range topics {
		topicShieldLevel := SiftRiskMapToShieldNumber[topicValue]
		if category, exists := topicToCategoryMapping[topic]; exists && topicShieldLevel > largestCategoryValue {
			largestCategory = category
			largestCategoryValue = topicShieldLevel
		}
	}

	return largestCategory, largestCategoryValue
}
