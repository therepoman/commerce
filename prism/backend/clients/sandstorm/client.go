package sandstorm

import (
	"errors"
	"sync"
	"time"

	"code.justin.tv/commerce/prism/config"
	"code.justin.tv/systems/sandstorm/manager"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
)

type Client interface {
	GetSecret(secretName string) (string, error)
}

type client struct {
	sandstormManager *manager.Manager
	secretCache      *sync.Map
}

type fakeclient struct {
}

func (fc *fakeclient) GetSecret(secretName string) (string, error) {
	return "randomSecret", nil
}

func New(awsConfig *aws.Config, sess *session.Session, cfg *config.Config) (Client, error) {
	// if it is not prod or staging env we want to make a fake client
	if !(cfg.IsProd() || cfg.IsStaging()) {
		return &fakeclient{}, nil
	}

	stsClient := sts.New(sess)
	arp := &stscreds.AssumeRoleProvider{
		Duration:     900 * time.Second,
		ExpiryWindow: 10 * time.Second,
		RoleARN:      cfg.SandstormRole,
		Client:       stsClient,
	}

	creds := credentials.NewCredentials(arp)
	awsConfig.WithCredentials(creds)

	managerCfg := manager.Config{
		AWSConfig: awsConfig,
		TableName: "sandstorm-production",
		KeyID:     "alias/sandstorm-production",
	}

	sandstormManager := manager.New(managerCfg)

	return &client{
		sandstormManager: sandstormManager,
		secretCache:      new(sync.Map),
	}, nil
}

func (c *client) GetSecret(secretName string) (string, error) {
	cachedSecret, isCached := c.secretCache.Load(secretName)
	if isCached {
		secret, isCorrectType := cachedSecret.(string)
		if !isCorrectType {
			return "", errors.New("incorrect secret type cached")
		}
		return secret, nil
	}

	secret, err := c.sandstormManager.Get(secretName)
	if err != nil {
		return "", err
	}

	secretStr := string(secret.Plaintext)
	c.secretCache.Store(secretName, secretStr)

	return secretStr, nil
}
