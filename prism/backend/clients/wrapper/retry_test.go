package wrapper_test

import (
	"bytes"
	"io/ioutil"
	"testing"

	"github.com/afex/hystrix-go/hystrix"

	"code.justin.tv/commerce/prism/backend/clients/wrapper"
	"code.justin.tv/commerce/prism/mocks"
	stats_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/stats"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	statusCode400 = 400
	statusCode503 = 503
)

func TestRetryRoundTripper(t *testing.T) {
	Convey("Test RoundTrip", t, func() {
		defaultReqBodyBytes := []byte("this is the request body in byte array format")
		defaultRespBodyBytes := []byte("this is the response body in byte array format. http 200 ok lul")
		defaultHTTPRequest := generateHTTPRequest(defaultReqBodyBytes)

		mockDownstreamResponse := new(mocks.RoundTripper)
		mockStatter := new(stats_mock.Statter)
		mockStatter.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		name := "test"
		wrapper := wrapper.RetryRoundTripWrapper{
			Next:  mockDownstreamResponse,
			Name:  name,
			Stats: mockStatter,
		}

		Convey("Downstream returns 200", func() {
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(generateHTTPResponse(statusCode200, defaultRespBodyBytes), nil)

			resp, err := wrapper.RoundTrip(defaultHTTPRequest)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)

			respBodyBytes, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.StatusCode, ShouldEqual, statusCode200)
			So(bytes.Compare(respBodyBytes, defaultRespBodyBytes), ShouldEqual, 0)
			mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
		})

		Convey("Downstream returns 400", func() {
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(generateHTTPResponse(statusCode400, defaultRespBodyBytes), nil)

			resp, err := wrapper.RoundTrip(defaultHTTPRequest)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)

			respBodyBytes, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.StatusCode, ShouldEqual, statusCode400)
			So(bytes.Compare(respBodyBytes, defaultRespBodyBytes), ShouldEqual, 0)
			mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
		})

		Convey("Downstream returns 500", func() {
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(generateHTTPResponse(statusCode500, defaultRespBodyBytes), nil)

			resp, err := wrapper.RoundTrip(defaultHTTPRequest)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)

			respBodyBytes, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.StatusCode, ShouldEqual, statusCode500)
			So(bytes.Compare(respBodyBytes, defaultRespBodyBytes), ShouldEqual, 0)
			mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 2)
		})

		Convey("Downstream returns 503", func() {
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(generateHTTPResponse(statusCode503, defaultRespBodyBytes), nil)

			resp, err := wrapper.RoundTrip(defaultHTTPRequest)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)

			respBodyBytes, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.StatusCode, ShouldEqual, statusCode503)
			So(bytes.Compare(respBodyBytes, defaultRespBodyBytes), ShouldEqual, 0)
			mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 2)
		})

		Convey("error in hystrix - max concurrency", func() {
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(nil, hystrix.ErrMaxConcurrency)

			resp, err := wrapper.RoundTrip(defaultHTTPRequest)
			So(err, ShouldNotBeNil)
			So(resp, ShouldBeNil)

			mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 2)
		})

		Convey("error in hystrix - circuit open", func() {
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(nil, hystrix.ErrCircuitOpen)

			resp, err := wrapper.RoundTrip(defaultHTTPRequest)
			So(err, ShouldNotBeNil)
			So(resp, ShouldBeNil)

			mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 2)
		})

		Convey("error in hystrix - timeout", func() {
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(nil, hystrix.ErrTimeout)

			resp, err := wrapper.RoundTrip(defaultHTTPRequest)
			So(err, ShouldNotBeNil)
			So(resp, ShouldBeNil)

			mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 2)
		})

		Convey("both resp and err are nil", func() {
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(nil, nil)

			resp, err := wrapper.RoundTrip(defaultHTTPRequest)
			So(err, ShouldBeNil)
			So(resp, ShouldBeNil)

			mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
		})
	})
}
