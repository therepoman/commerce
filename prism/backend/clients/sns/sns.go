package sns

import (
	"context"

	"code.justin.tv/commerce/prism/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
)

// MessageAttributesV1 is a map containing the V1 message attributes
var MessageAttributesV1 = map[string]*sns.MessageAttributeValue{
	"Version": {
		DataType:    aws.String("Number"),
		StringValue: aws.String("1"),
	},
}

// Client is an SNS client
type Client struct {
	sns *sns.SNS
}

// IClient represents an SNS client
type IClient interface {
	PostToTopic(topic string, msg string) error
	PostToTopicWithMessageAttributes(topic string, msg string, msgAttributes map[string]*sns.MessageAttributeValue) error
	PostToTopicWithContext(ctx context.Context, topic string, msg string) error
}

// NewClient creates a new SNS client
func NewClient(sess *session.Session, cfg *config.Config) IClient {
	return &Client{
		sns: sns.New(sess, aws.NewConfig().WithRegion(cfg.AWSRegion)),
	}
}

// PostToTopic posts a message to an SNS topic
func (s *Client) PostToTopic(topic string, msg string) error {
	pi := &sns.PublishInput{
		Message:  aws.String(msg),
		TopicArn: aws.String(topic),
	}
	_, err := s.sns.Publish(pi)
	return err
}

// PostToTopicWithMessageAttributes posts a message to an SNS topic with message attributes
func (s *Client) PostToTopicWithMessageAttributes(topic string, msg string, msgAttributes map[string]*sns.MessageAttributeValue) error {
	pi := &sns.PublishInput{
		Message:           aws.String(msg),
		TopicArn:          aws.String(topic),
		MessageAttributes: msgAttributes,
	}
	_, err := s.sns.Publish(pi)
	return err
}

// PostToTopicWithContext posts a message to an SNS topic with a context
func (s *Client) PostToTopicWithContext(ctx context.Context, topic string, msg string) error {
	pi := &sns.PublishInput{
		Message:  aws.String(msg),
		TopicArn: aws.String(topic),
	}
	_, err := s.sns.PublishWithContext(ctx, pi)
	return err
}
