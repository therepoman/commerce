package payday

import (
	"context"
	"net/http"

	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
)

type Client interface {
	GetImageUrl(ctx context.Context, imageID string) (string, error)
}

type clientImpl struct {
	client paydayrpc.Payday
}

func NewPaydayClient(paydayHost string) Client {
	return &clientImpl{
		client: paydayrpc.NewPaydayProtobufClient(paydayHost, http.DefaultClient),
	}
}

func (c *clientImpl) GetImageUrl(ctx context.Context, imageID string) (string, error) {
	resp, err := c.client.GetCheerScreenshotFromId(ctx, &paydayrpc.GetCheerScreenshotFromIdReq{
		ImageId: imageID,
	})
	if err != nil {
		return "", err
	}

	return resp.GetImageUrl(), nil
}
