package rooms

import (
	"context"
	"testing"

	"errors"

	"code.justin.tv/chat/chatrooms/rpc/chatrooms"
	chatrooms_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/chat/chatrooms/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestRoomsClient(t *testing.T) {
	Convey("Given a Rooms client wrapper", t, func() {
		chatroomsClient := new(chatrooms_mock.Chatrooms)
		roomsClient := NewRoomsClient(chatroomsClient)

		Convey("Sending a rooms message", func() {
			Convey("that errors", func() {
				request := &chatrooms.SendMessageRequest{
					RoomId:      "1234",
					BitsAmount:  1,
					SenderId:    "127380717",
					MessageText: "cheer1 test",
					Nonce:       "abc123",
				}

				chatroomsClient.On("SendMessage", mock.Anything, mock.Anything).Return(nil, errors.New("test error"))

				response, err := roomsClient.SendMessageToRoom(context.Background(), request)
				So(err, ShouldNotBeNil)
				So(response, ShouldBeNil)
			})

			Convey("that is successful", func() {
				request := &chatrooms.SendMessageRequest{
					RoomId:      "1234",
					BitsAmount:  1,
					SenderId:    "127380717",
					MessageText: "cheer1 test",
					Nonce:       "abc123",
				}

				chatroomsClient.On("SendMessage", mock.Anything, mock.Anything).Return(&chatrooms.SendMessageResponse{
					Success: true,
					Message: &chatrooms.Message{},
				}, nil)

				response, err := roomsClient.SendMessageToRoom(context.Background(), request)
				So(err, ShouldBeNil)
				So(response, ShouldNotBeNil)
			})
		})

		Convey("Checking the rooms enabled channel whitelist", func() {
			Convey("when the channel is on the list", func() {
				So(roomsClient.IsRoomsEnabledChannel("108707191"), ShouldBeTrue)
			})

			Convey("when the channel is not on the list", func() {
				So(roomsClient.IsRoomsEnabledChannel("abc123"), ShouldBeFalse)
			})
		})
	})
}
