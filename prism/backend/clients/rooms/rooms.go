package rooms

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/chat/chatrooms/rpc/chatrooms"
	"code.justin.tv/commerce/payday/httputils"
	"github.com/afex/hystrix-go/hystrix"
)

const (
	// Attempts the call to rooms 5 times with exponential backoff
	// Delays work out to: 100ms 200ms 400ms 800ms
	attempts             = 5
	initialRetryDelay    = 100 * time.Millisecond
	retryDelayMultiplier = 2
	maxRetryDelay        = 1 * time.Second
	roomsSendToRoom      = "rooms:send_to_room"
)

var retryDelayFunc = httputils.NewExponentialBackoffRetryFunction(initialRetryDelay, retryDelayMultiplier, maxRetryDelay)

var bitsInRoomsEnabledChannelIDs = map[string]interface{}{
	"108707191": nil, // twitch.tv/seph
	"27697171":  nil, // twitch.tv/richard

	"137512364": nil, // twitch.tv/overwatchleague
	"209684100": nil, // twitch.tv/overwatchleague_allaccess
	"188864445": nil, // twitch.tv/overwatchleague_kr
	"188863650": nil, // twitch.tv/overwatchleague_fr
	"190160460": nil, // twitch.tv/overwatchleague_zh
	"192718746": nil, // twitch.tv/overwatchleague_zhtw
}

// Client represents a Chat Rooms client
type Client interface {
	SendMessageToRoom(ctx context.Context, req *chatrooms.SendMessageRequest) (*chatrooms.SendMessageResponse, error)
	IsRoomsEnabledChannel(channelID string) bool
}

// ClientImpl implements Client
type ClientImpl struct {
	RoomsClient chatrooms.Chatrooms `inject:""`
}

// NewRoomsClient creates a new Chat Rooms client
func NewRoomsClient(roomsClient chatrooms.Chatrooms) Client {
	return &ClientImpl{
		RoomsClient: roomsClient,
	}
}

// SendMessageToRoom sends a message to a chat room
func (c *ClientImpl) SendMessageToRoom(ctx context.Context, req *chatrooms.SendMessageRequest) (*chatrooms.SendMessageResponse, error) {
	var err error
	var resp *chatrooms.SendMessageResponse
	for attempt := 1; attempt <= attempts; attempt++ {
		resp, err = c.sendMessageToRoom(ctx, req)
		if err == nil || attempt == attempts {
			break
		}

		timer := time.After(retryDelayFunc(attempt, err))
		select {
		case <-timer:
			continue
		case <-ctx.Done():
			return nil, errors.New("timed out waiting on SendMessageToRooms request to succeed")
		}
	}
	return resp, err
}

func (c *ClientImpl) sendMessageToRoom(ctx context.Context, req *chatrooms.SendMessageRequest) (*chatrooms.SendMessageResponse, error) {
	var resp *chatrooms.SendMessageResponse
	err := hystrix.Do(roomsSendToRoom, func() error {
		innerResp, innerErr := c.RoomsClient.SendMessage(ctx, req)
		if innerErr != nil {
			return innerErr
		}
		resp = innerResp
		return nil
	}, nil)

	if err != nil {
		return nil, err
	}
	return resp, nil
}

// IsRoomsEnabledChannel returns true if a channelID has rooms enabled
func (c *ClientImpl) IsRoomsEnabledChannel(channelID string) bool {
	_, ok := bitsInRoomsEnabledChannelIDs[channelID]

	return ok
}
