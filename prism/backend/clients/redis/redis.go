package redis

import (
	"time"

	"code.justin.tv/commerce/prism/config"
	"github.com/go-redis/redis"
)

// Client represents a Redis client
type Client interface {
	Ping() error
	Get(key string) (string, error)
	Set(key string, value interface{}, expiration time.Duration) error
	Delete(key string) error
}

type client struct {
	redisClient redis.Cmdable
}

// NewClient creates a new Redis Client
func NewClient(cfg *config.Config) Client {
	if cfg.UseRedisClusterMode {
		redisClient := redis.NewClusterClient(&redis.ClusterOptions{
			Addrs: []string{cfg.RedisEndpoint},
		})

		return &client{
			redis.Cmdable(redisClient),
		}
	}

	redisClient := redis.NewClient(&redis.Options{
		Addr: cfg.RedisEndpoint,
	})

	return &client{
		redis.Cmdable(redisClient),
	}
}

// Ping ...
func (c *client) Ping() error {
	_, err := c.redisClient.Ping().Result()
	return err
}

// Get ...
func (c *client) Get(key string) (string, error) {
	value, err := c.redisClient.Get(key).Result()
	if err != nil {
		return "", err
	}
	return value, nil
}

// Set ...
func (c *client) Set(key string, value interface{}, expiration time.Duration) error {
	return c.redisClient.Set(key, value, expiration).Err()
}

// Delete ...
func (c *client) Delete(key string) error {
	return c.redisClient.Del(key).Err()
}
