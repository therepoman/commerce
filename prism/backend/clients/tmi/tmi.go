package tmi

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	badgesModels "code.justin.tv/chat/badges/app/models"
	httputils "code.justin.tv/commerce/gogogadget/http"
	log "code.justin.tv/commerce/logrus"
	"github.com/pkg/errors"
)

// TODO: Migrate off this deprecated TMI client that was copied from payday

const BITS_MESSAGE_ROUTE = "/internal/bits_message"
const USER_NOTICE_ROUTE = "/internal/usernotice"
const OFFICIAL_TWITCH_USER_ID = 12826 //twitch username

const (
	// Retries the call to TMI 8 times with exponential backoff
	// Delays works out to: 50ms 100ms 200ms 400ms 800ms 1600ms 2000ms
	retryAttempts        = 8
	initialRetryDelay    = 50 * time.Millisecond
	retryDelayMultiplier = 2
	maxRetryDelay        = 2 * time.Second
)

// ITMIClient represents a TMI client
type ITMIClient interface {
	SendBitsMessage(ctx context.Context, params SendMessageParams) (*BitsMessageResponse, error)
	SendSystemMessage(params SystemMessageParams) error
}

// TMIClient is a TMI client
type TMIClient struct {
	client httputils.HttpUtilClient
	host   string
}

// TMIMessage is a TMI message
type TMIMessage struct {
	SenderUserID    string `json:"sender_user_id,omitempty"`
	TargetChannelID string `json:"target_channel_id,omitempty"`
	Body            string `json:"body,omitempty"`
	NumberBits      int    `json:"number_bits"`
	ImageURL        string `json:"image_url,omitempty"`
	IsNoop          bool   `json:"is_noop"`
}

// UserNoticeMsgParam ...
type UserNoticeMsgParam struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

// UserNoticeParams ...
type UserNoticeParams struct {
	SenderUserID      int64                `json:"sender_user_id"`
	TargetChannelID   int64                `json:"target_channel_id"`
	Body              string               `json:"body"`
	MsgID             string               `json:"msg_id"`
	MsgParams         []UserNoticeMsgParam `json:"msg_params"`
	DefaultSystemBody string               `json:"default_system_body"`
}

// BitsMessageResponse ...
type BitsMessageResponse struct {
	// Sent indicates whether the message was sent (if IsNoop = false) or have
	// been sent (if IsNoop = true).
	Sent bool `json:"sent"`
	// IsNoop indicates whether the request was made as as noop request and the
	// message would not be published even if it passed enforcements.
	IsNoop      bool            `json:"is_noop"`
	MsgBody     string          `json:"msg_body"`
	MsgTags     BitsMessageTags `json:"msg_tags"`
	SentMsgBody string
	SentMsgTags UserRoomMsgTags
	ErrorCode   *string `json:"error_code,omitempty"`
}

// BitsMessageTags ...
type BitsMessageTags struct {
	Badges      []badgesModels.Badge `json:"badges"`
	Color       string               `json:"color"`
	DisplayName string               `json:"display_name"`
	Emotes      []EmoticonMatch      `json:"emotes"`
	MsgID       string               `json:"msg_id"`
}

// UserRoomMsgTags ...
type UserRoomMsgTags struct {
	Badges      []badgesModels.Badge
	Color       string
	DisplayName string
	Emotes      []EmoticonMatch
	Moderator   bool
	MsgID       string
	RoomID      int
	Subscriber  bool
	Turbo       bool
	UserID      int
	UserType    string

	// Optional tags
	Bits       *int
	Historical *bool
}

// EmoticonMatch ...
type EmoticonMatch struct {
	// ID is the unique row ID for the emoticon.
	ID int `json:"id"`
	// Start is the index in the original string where the emoticon starts (0-indexed, inclusive).
	Start int `json:"start"`
	// End is the index in the original string where the emoticon ends (0-indexed, inclusive).
	End int `json:"end"`
	// Set is the emoticon set that contains this emoticon.
	Set int `json:"set"`
}

// SendMessageParams ...
type SendMessageParams struct {
	Sender    string
	Recipient string
	Amount    int
	Message   string
	ImageURL  string
}

// SystemMessageParams ...
type SystemMessageParams struct {
	Recipient string
	Message   string
	ID        string
	Params    []UserNoticeMsgParam
}

// NewTMIClient creates a new TMI client
func NewTMIClient(client httputils.HttpUtilClient, host string) ITMIClient {
	return &TMIClient{
		client: client,
		host:   host,
	}
}

// NewHttpTMIClient creates a new HTTP TMI Client
func NewHttpTMIClient(host string) ITMIClient {
	httpClient := httputils.NewHttpUtilClientApiWithCustomRetryDelayFunc(
		retryAttempts,
		httputils.NewExponentialBackoffRetryFunction(
			initialRetryDelay,
			retryDelayMultiplier,
			maxRetryDelay))

	return NewTMIClient(httpClient, host)
}

// SendBitsMessage sends a bits message to chat
func (t *TMIClient) SendBitsMessage(ctx context.Context, params SendMessageParams) (*BitsMessageResponse, error) {
	body, err := json.Marshal(TMIMessage{
		SenderUserID:    params.Sender,
		TargetChannelID: params.Recipient,
		Body:            params.Message,
		NumberBits:      params.Amount,
		ImageURL:        params.ImageURL,
		IsNoop:          false,
	})

	if err != nil {
		return nil, fmt.Errorf("TMIClient: Error while marshaling TMIMessage %v", err)
	}

	url := t.host + BITS_MESSAGE_ROUTE
	responseBytes, statusCode, err := t.client.HttpPost(ctx, url, bytes.NewReader(body))

	if err != nil {
		msg := "TMIClient: Error while sending request to TMI"
		log.WithFields(log.Fields{
			"sender":    params.Sender,
			"recipient": params.Recipient,
			"amount":    params.Amount,
		},
		).WithError(err).Error(msg)
		return nil, fmt.Errorf("%v %s", err, msg)
	}

	if statusCode != http.StatusOK {
		msg := "TMIClient: Tmi request failed while sending payload"
		log.WithFields(log.Fields{
			"statusCode": statusCode,
			"sender":     params.Sender,
			"recipient":  params.Recipient,
			"amount":     params.Amount,
		},
		).Error(msg)
		return nil, errors.New(msg)
	}

	response := BitsMessageResponse{}
	err = json.Unmarshal(responseBytes, &response)

	if err != nil {
		return nil, fmt.Errorf("%v %s %s", err, "Could not marshal chat message", string(responseBytes))
	}

	return &response, nil
}

func (t *TMIClient) sendUserNotice(params UserNoticeParams) error {
	body, err := json.Marshal(params)

	if err != nil {
		msg := "TMIClient: Error while marshaling UserNoticeParams"
		log.WithError(err).Errorf(msg)
		return errors.New(msg)
	}

	url := t.host + USER_NOTICE_ROUTE
	_, statusCode, err := t.client.HttpPost(context.Background(), url, bytes.NewReader(body))

	if err != nil {
		msg := "TMIClient: Error while creating new request for SendUserNotice"
		log.WithError(err).Errorf(msg)
		return errors.Wrap(err, msg)
	}

	if statusCode != http.StatusOK {
		msg := "TMIClient: Tmi UserNotice request failed while sending payload"
		log.WithField("statusCode", statusCode).Error(msg)
		return errors.New(msg)
	}

	return nil
}

// SendSystemMessage sends a system message to chat
func (t *TMIClient) SendSystemMessage(params SystemMessageParams) error {
	recipientID, err := strconv.ParseInt(params.Recipient, 10, 64)
	if err != nil {
		msg := "TMIClient: Error non-numeric recipientId used in SendSystemMessage"
		log.WithError(err).Error(msg)
		return errors.New(msg)
	}

	return t.sendUserNotice(UserNoticeParams{
		SenderUserID:      OFFICIAL_TWITCH_USER_ID,
		TargetChannelID:   recipientID,
		Body:              "",
		MsgID:             params.ID,
		MsgParams:         params.Params,
		DefaultSystemBody: params.Message,
	})
}
