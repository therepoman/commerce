package tmi

import (
	"context"
	"encoding/json"
	"testing"

	"code.justin.tv/commerce/prism/config"
	http_mock "code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/gogogadget"
	"code.justin.tv/edge/visage/__vendor/github.com/stretchr/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
)

const env = "staging"

func TestTMIClient(t *testing.T) {
	cfg, err := config.LoadConfig(env)
	if err != nil {
		panic(err)
	}

	Convey("Given a TMI client wrapper", t, func() {
		httpUtilClient := new(http_mock.HttpUtilClient)
		tmiClient := NewTMIClient(httpUtilClient, cfg.TMI.Endpoint)

		Convey("Sending a Bits message is successful", func() {

			response := BitsMessageResponse{
				Sent:        true,
				IsNoop:      false,
				MsgBody:     "cheer1",
				SentMsgBody: "cheer1",
			}

			respBytes, err := json.Marshal(response)
			So(err, ShouldBeNil)

			httpUtilClient.On("HttpPost", mock.Anything, mock.Anything, mock.Anything).Return(respBytes, 200, nil)

			messageParams := SendMessageParams{
				Amount:    1,
				Message:   "cheer1",
				Recipient: "116076154", //qa_bits_partner
				Sender:    "127380717", //graham
			}

			resp, err := tmiClient.SendBitsMessage(context.Background(), messageParams)
			So(err, ShouldBeNil)
			So(resp.IsNoop, ShouldBeFalse)
			So(resp.Sent, ShouldBeTrue)
		})
	})
}
