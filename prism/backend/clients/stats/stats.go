package stats

import (
	"time"

	log "code.justin.tv/commerce/logrus"
	"github.com/cactus/go-statsd-client/statsd"
)

type Statter interface {
	Inc(string, int64, float32)
	TimingDuration(string, time.Duration, float32)
}

type statter struct {
	Stats statsd.Statter `inject:""`
}

func NewStatter() Statter {
	return &statter{}
}

func NewTestStatter(statsdStatter statsd.Statter) Statter {
	return &statter{
		Stats: statsdStatter,
	}
}

func (s *statter) Inc(stat string, value int64, rate float32) {
	err := s.Stats.Inc(stat, value, rate)

	if err != nil {
		log.WithError(err).Error("error logging metric")
	}
}

func (s *statter) TimingDuration(stat string, delta time.Duration, rate float32) {
	err := s.Stats.TimingDuration(stat, delta, rate)

	if err != nil {
		log.WithError(err).Error("error logging metric")
	}
}
