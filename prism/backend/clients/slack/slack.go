package slack

import (
	"bytes"
	"encoding/json"
	"net/http"
	"time"

	log "code.justin.tv/commerce/logrus"
)

type Slacker interface {
	Post(webHookUrl string, msg Msg) error
	PostMsg(webHookUrl string, msgBody string) error
}

type slacker struct {
	HttpClient *http.Client
	WebHookUrl string
}

func NewSlacker() Slacker {
	return &slacker{
		HttpClient: &http.Client{
			Timeout: 2 * time.Second,
		},
	}
}

type Msg struct {
	Text        string       `json:"text"`
	UnfurlLinks bool         `json:"unfurl_links"`
	Parse       string       `json:"parse"`
	Attachments []Attachment `json:"attachments"`
}

type Attachment struct {
	Fallback string `json:"fallback"`
	Pretext  string `json:"pretext"`
	ImageURL string `json:"image_url"`
}

func (s *slacker) Post(webHookUrl string, msg Msg) error {
	url := webHookUrl
	requestJson, err := json.Marshal(&msg)
	if err != nil {
		log.WithError(err).Error("Could not marshal json for slack message")
		return err
	}

	requestJsonReader := bytes.NewReader(requestJson)

	_, err = s.HttpClient.Post(url, "application/json", requestJsonReader)
	if err != nil {
		log.WithField("url", url).WithError(err).Error("could not send message to slack webhook")
		return err
	}
	return nil
}

func (s *slacker) PostMsg(webHookUrl string, msgBody string) error {
	msg := Msg{
		Text:        msgBody,
		UnfurlLinks: true,
		Parse:       "full",
	}
	return s.Post(webHookUrl, msg)
}
