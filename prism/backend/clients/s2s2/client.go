package s2s2

import (
	"os"

	twitchlogging "code.justin.tv/amzn/TwitchLogging"
	identifier "code.justin.tv/amzn/TwitchProcessIdentifier"
	"code.justin.tv/amzn/TwitchS2S2/c7s"
	"code.justin.tv/amzn/TwitchS2S2/s2s2"
	twitchtelemetry "code.justin.tv/amzn/TwitchTelemetry"
	twitchtelemetrymiddleware "code.justin.tv/amzn/TwitchTelemetryMetricsMiddleware"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/config"
	"code.justin.tv/safety/telemetry"
	"code.justin.tv/video/metrics-middleware/v2/operation"
	"github.com/pborman/uuid"
)

func NewS2S2Client(cfg *config.Config, logger twitchlogging.Logger) *s2s2.S2S2 {
	s2s2Client, err := s2s2.New(&s2s2.Options{
		Config: &c7s.Config{
			ClientServiceName:   cfg.S2SServiceName,
			ServiceOrigins:      cfg.S2SServiceOrigin,
			EnableAccessLogging: true,
		},
		OperationStarter: &operation.Starter{
			OpMonitors: []operation.OpMonitor{
				&twitchtelemetrymiddleware.OperationMonitor{
					SampleReporter: getSampleReporter(cfg),
				},
			},
		},
		Logger: logger,
	})
	if err != nil {
		log.WithError(err).Fatal("Unable to create s2s2 client")
	}

	return s2s2Client
}

func getSampleReporter(cfg *config.Config) twitchtelemetry.SampleReporter {
	hostname := "unknown"
	if envHostname, err := os.Hostname(); err == nil {
		hostname = envHostname
	}

	telemetryReporter := telemetry.ConfigureTelemetry(&identifier.ProcessIdentifier{
		Service:  "prism",
		Region:   cfg.AWSRegion,
		Stage:    cfg.EnvironmentName,
		LaunchID: uuid.New(),
		Machine:  hostname,
	}, nil)

	return telemetryReporter
}
