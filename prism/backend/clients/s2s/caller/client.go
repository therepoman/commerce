package caller

import (
	"net/http"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/config"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	"github.com/pkg/errors"
)

// NewHttpClient creates a new S2S caller HTTP client
func NewHttpClient(cfg *config.Config, useS2S bool) (*http.Client, error) {
	if cfg == nil {
		return nil, errors.New("config cannot be nil")

	}

	httpClient := &http.Client{}
	if useS2S {
		s2sConfig := caller.Config{
			DisableStatsClient: true,
		}
		rt, err := caller.NewRoundTripper(cfg.S2SServiceName, &s2sConfig, log.New())
		if err != nil {
			return nil, errors.Wrap(err, "failed to create s2s round tripper")
		}
		httpClient.Transport = rt
	}

	return httpClient, nil
}
