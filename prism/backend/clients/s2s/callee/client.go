package callee

import (
	"errors"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/config"
	"code.justin.tv/sse/malachai/pkg/events"
	"code.justin.tv/sse/malachai/pkg/s2s/callee"
)

// NewClient creates a new S2S callee client
func NewClient(cfg *config.Config) (callee.ClientAPI, error) {
	if cfg == nil {
		return nil, errors.New("config cannot be nil")
	}

	eventsWriterClient, err := events.NewEventLogger(events.Config{}, log.New())

	if err != nil {
		log.Errorf("unable to create malachai events writer client: %v", err)
		return nil, err
	}

	s2sCalleeClient := &callee.Client{
		ServiceName:        cfg.S2SServiceName,
		EventsWriterClient: eventsWriterClient,
		Config: &callee.Config{
			SupportWildCard:    true,
			DisableStatsClient: true,
			PassthroughMode:    cfg.S2SPassthrough,
		},
	}

	err = s2sCalleeClient.Start()
	if err != nil {
		log.Errorf("unable to create malachai S2S callee client: %v", err)
		return nil, err
	}

	return s2sCalleeClient, nil
}
