package main

import (
	"context"
	"math/rand"
	"os"
	"time"

	"code.justin.tv/amzn/TwitchS2S2/s2s2"
	"code.justin.tv/commerce/gogogadget/audit"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend"
	"code.justin.tv/commerce/prism/backend/clients/cloudwatchlogger"
	s2s2client "code.justin.tv/commerce/prism/backend/clients/s2s2"
	"code.justin.tv/commerce/prism/backend/middleware"
	"code.justin.tv/commerce/prism/config"
	prism "code.justin.tv/commerce/prism/rpc"
	"code.justin.tv/commerce/splatter"
	"github.com/twitchtv/twirp"
	goji_graceful "github.com/zenazn/goji/graceful"
	"goji.io"
	"goji.io/pat"
)

const (
	workerShutdownTimeout = 1 * time.Minute
)

func getEnv() config.Environment {
	env := os.Getenv(config.EnvironmentEnvironmentVariable)
	if env != "" {
		log.Infof("Found ENVIRONMENT environment variable: %s", env)
	}

	args := os.Args
	if len(args) > 2 {
		log.Panic("Received too many CLI args")
	} else if len(args) == 2 {
		env = args[1]
		log.Infof("Using environment from CLI arg: %s", env)
	}

	if !config.IsValidEnvironment(config.Environment(env)) {
		log.Errorf("Invalid environment: %s", env)
		log.Infof("Falling back to default environment: %s", string(config.Default))
		return config.Default
	}

	return config.Environment(env)
}

func main() {
	env := getEnv()

	rand.Seed(time.Now().UnixNano())

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		log.WithError(err).Panic("Error loading config")
	}

	log.Infof("Loaded config: %s", cfg.EnvironmentName)

	be, err := backend.NewBackend(cfg)
	if err != nil {
		log.WithError(err).Panic("Error initializing backend")
	}

	twirpStatsHook := twirp.ChainHooks(splatter.NewStatsdServerHooks(be.Statter()))
	twirpHandler := prism.NewPrismServer(be.TwirpServer(), twirpStatsHook)

	mux := goji.NewMux()
	mux.HandleFunc(pat.Get("/ping"), be.Ping)

	var s2s2logger cloudwatchlogger.CloudWatchLogger
	var s2s2Client *s2s2.S2S2
	if cfg.S2SEnabled {
		s2s2logger, err = cloudwatchlogger.NewCloudWatchLogClient(cfg.AWSRegion, cfg.S2SAuthLogGroup)
		if err != nil {
			log.WithError(err).Panic("Error initializing s2s2 logger")
		}
		s2s2Client = s2s2client.NewS2S2Client(cfg, cloudwatchlogger.AdaptToTwitchLoggingLogger(s2s2logger, cfg.S2SAuthLogGroup))
		handler := s2s2Client.RequireAuthentication(twirpHandler)
		if cfg.S2SPassthrough {
			mux.Handle(pat.Post(prism.PrismPathPrefix+"*"), audit.NewMuxWrapper(handler.RecordMetricsOnly()))
		} else {
			mux.Handle(pat.Post(prism.PrismPathPrefix+"*"), audit.NewMuxWrapper(handler))
		}
	} else {
		s2s2logger = cloudwatchlogger.NewCloudWatchLogNoopClient()
		mux.Handle(pat.Post(prism.PrismPathPrefix+"*"), audit.NewMuxWrapper(twirpHandler))
	}

	mux.Use(middleware.NewIntegrationTestMiddleware(cfg))
	mux.Use(middleware.NewClientIDMiddleware())

	goji_graceful.HandleSignals()
	err = goji_graceful.ListenAndServe(":8000", mux)
	if err != nil {
		log.WithError(err).Error("Mux listen and serve error")
	}

	log.Info("Initiated shutdown process")

	shutdownStart := time.Now()
	shutdownContext, shutdownContextCancel := context.WithTimeout(context.Background(), workerShutdownTimeout)
	defer shutdownContextCancel()

	shutdownErrGroup := be.Shutdown(shutdownContext)
	shutdownErrGroup.Go(func() error {
		s2s2logger.Shutdown()
		return nil
	})

	// Wait on all async processors to finish shutting down
	if err = shutdownErrGroup.Wait(); err != nil {
		log.WithError(err).Error("Error attempting to shutdown")
	}

	goji_graceful.Wait()
	log.Infof("Finished shutting down in: %v", time.Since(shutdownStart))
}
