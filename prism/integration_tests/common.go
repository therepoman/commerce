package integration_tests

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	zuma "code.justin.tv/chat/zuma/client"
	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/logrus"
	payday "code.justin.tv/commerce/payday/client"
	payday_model "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/prism/backend/clients/stats"
	"code.justin.tv/commerce/prism/backend/clients/wrapper"
	"code.justin.tv/commerce/prism/backend/middleware"
	"code.justin.tv/commerce/prism/backend/models"
	"code.justin.tv/commerce/prism/config"
	"code.justin.tv/commerce/prism/integration_tests/clients"
	prism "code.justin.tv/commerce/prism/rpc"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	"code.justin.tv/web/users-service/client/usersclient_internal"
	user_models "code.justin.tv/web/users-service/models"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/gofrs/uuid"
	log "github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
	irc "github.com/thoj/go-ircevent"
	"github.com/twitchtv/twirp"
)

const (
	TUID_bits_test_1     = "138165611"
	TUID_bits_test_2     = "138165652"
	TUID_bits_test_3     = "138165685"
	TUID_qa_bits_partner = "116076154"
	TUID_qa_bits_tests   = "145903336"
	LOGIN_qa_bits_tests  = "qa_bits_tests"
	TUID_seph            = "108707191"

	test_user_prefix = "prism_tests_"
)

const (
	testUserIdPrefixString = "7357"
	testUserIdSuffixDigits = 14
)

const (
	ircEndpoint = "irc.chat.twitch.tv:6667"
)

func GetConfig() config.Config {
	environment := os.Getenv("ENVIRONMENT")
	if environment == "" {
		log.Panic("ENVIRONMENT env variable not set")
	}

	if environment == "production" || environment == "prod" {
		log.Panic("Please don't run these integration tests against prod!")
	}

	log.Infof("Loading config file for environment: %s", environment)
	cfg, err := config.LoadConfig(config.Environment(environment))
	if err != nil {
		log.WithError(err).Panicf("Could not load config file for environment: %s", environment)
	}

	if cfg == nil {
		log.WithError(err).Panicf("Could not find a config file for environment: %s", environment)
	}

	return *cfg
}

func GetPrismClient() prism.Prism {
	return prism.NewPrismProtobufClient(GetConfig().Prism.Endpoint, http.DefaultClient)
}

func GetIntegrationTestContext(disableThrottling bool) context.Context {
	throttleHeader := ""
	if disableThrottling {
		throttleHeader = models.ThrottleConfigDisabled
	}

	ctx, err := twirp.WithHTTPRequestHeaders(context.Background(), http.Header{
		middleware.IntegrationTestThrottleConfigHeader: []string{throttleHeader},
	})
	So(err, ShouldBeNil)
	return ctx
}

func GetPaydayClient() payday.Client {
	cfg := GetConfig()
	paydayConfig := twitchclient.ClientConf{
		Host: cfg.Payday.Endpoint,
	}

	s2sConfig := caller.Config{}
	rt, err := caller.NewRoundTripper(cfg.S2SServiceName, &s2sConfig, logrus.New())
	if err != nil {
		log.Panicf("Failed to create s2s round tripper: %v", err)
	}

	paydayConfig.RoundTripperWrappers = append(paydayConfig.RoundTripperWrappers,
		func(inner http.RoundTripper) http.RoundTripper {
			rt.SetInnerRoundTripper(inner)
			return rt
		},
	)

	client, err := payday.NewClient(paydayConfig)
	if err != nil {
		log.WithError(err).Panicf("Error getting payday client for environment")
	}

	return client

}

func GetZumaClient() zuma.Client {
	cfg := GetConfig()

	clientHTTPTransport := twitchclient.TransportConf{
		MaxIdleConnsPerHost: 100,
	}

	noOpStatter, err := statsd.NewNoopClient()
	if err != nil {
		log.WithError(err).Panic("Error creating noop statter")
	}

	statter := stats.NewTestStatter(noOpStatter)

	zumaClient, err := zuma.NewClient(twitchclient.ClientConf{
		Host:      cfg.Zuma.Endpoint,
		Transport: clientHTTPTransport,
		Stats:     noOpStatter,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewHystrixRoundTripWrapper("zuma", cfg.Zuma.TimeoutMilliseconds, statter),
		},
	})
	if err != nil {
		log.WithError(err).Panic("Error creating zuma client")
	}

	return zumaClient
}

func GetClueClient() clients.Clue {
	cfg := GetConfig()
	tmiClient, err := clients.NewTMIClient(cfg.Clue.Endpoint)
	if err != nil {
		log.WithError(err).Panic("Error creating TMI client")
	}

	return tmiClient
}

func GetUserServiceClient() usersclient_internal.InternalClient {
	cfg := GetConfig()

	noOpStatter, err := statsd.NewNoopClient()
	if err != nil {
		log.WithError(err).Panic("Error creating noop statter")
	}

	usersServiceClient, err := usersclient_internal.NewClient(twitchclient.ClientConf{
		Host:  cfg.UsersService.Endpoint,
		Stats: noOpStatter,
	})
	if err != nil {
		log.WithError(err).Panic("Error creating userservice client")
	}

	return usersServiceClient
}

func CreateTestUser(ctx context.Context, userServiceClient usersclient_internal.InternalClient) string {
	suffix := random.NumberString(10)
	resp, err := userServiceClient.CreateUser(ctx, &user_models.CreateUserProperties{
		Login:    test_user_prefix + suffix,
		IP:       "123.123.123.123",
		Location: "Test Land",
		Birthday: user_models.Birthday{
			Month: 4,
			Day:   20,
			Year:  1969,
		},
		Email: "bits-integration-tests@justin.tv",
	}, nil)
	So(err, ShouldBeNil)
	So(resp, ShouldNotBeNil)
	So(resp.ID, ShouldNotBeBlank)
	return resp.ID
}

func CleanupTestUser(ctx context.Context, userServiceClient usersclient_internal.InternalClient, userID string) {
	resp, err := userServiceClient.GetUserByID(ctx, userID, nil)
	So(err, ShouldBeNil)
	So(resp.Login, ShouldNotBeNil)
	So(*resp.Login, ShouldStartWith, test_user_prefix)

	err = userServiceClient.SoftDeleteUser(ctx, userID, "seph", nil)
	So(err, ShouldBeNil)

	err = userServiceClient.HardDeleteUser(ctx, userID, "seph", false, nil)
	So(err, ShouldBeNil)
}

func AddBits(paydayClient payday.Client, userID string, numBits int32) {
	eventID := RandomUUID()
	err := paydayClient.AddBits(context.Background(), &payday_model.AddBitsRequest{
		TwitchUserId:      userID,
		AmountOfBitsToAdd: numBits,
		EventId:           eventID,
		TypeOfBitsToAdd:   "CB_0",
		AdminReason:       "prism_integration_tests",
		AdminUserId:       TUID_qa_bits_partner,
	}, nil)
	So(err, ShouldBeNil)
}

func GetRandomTesUser() string {
	return testUserIdPrefixString + random.NumberString(testUserIdSuffixDigits)
}

func RandomUUID() string {
	id, err := uuid.NewV4()
	So(err, ShouldBeNil)
	return id.String()
}

func OnboardWithBits(paydayClient payday.Client, channelID string) {
	onboarded := true
	err := paydayClient.UpdateChannelSettings(context.Background(), channelID, payday_model.UpdateSettingsRequest{
		Onboarded: &onboarded,
	}, nil)
	So(err, ShouldBeNil)
}

var FailedPreconditionOnProcessError = twirp.NewError(twirp.FailedPrecondition, "user error on process message call")
var DownstreamDependencyError = twirp.InternalError("downstream dependency error processing message")
var FlaggedByAutomodError = twirp.NewError(twirp.InvalidArgument, "Message has been flagged by automod")

func PerformPrismBitsTransaction(ctx context.Context, prismClient prism.Prism, txID string, userID string, channelID string,
	amount int64, msg string, sendAnyway bool, expectError error, expectChatSent bool) string {

	if txID == "" {
		txID = RandomUUID()
	}
	fullMsg := txID + " " + msg

	ircErrChan := make(chan error, 1)
	validateIRC := expectChatSent && channelID == TUID_qa_bits_tests
	if validateIRC {
		go func(fullMsg1 string) {
			ircErrChan <- WaitForIRCMessageInQaBitsTests(fullMsg1)
		}(fullMsg)
	}

	log.Infof("New Prism Transaction (TransactionID: %s, UserID: %s, ChannelID: %s, Bits: %d)", txID, userID, channelID, amount)

	resp, err := prismClient.ProcessMessage(ctx, &prism.ProcessMessageReq{
		UserId:    userID,
		ChannelId: channelID,
		MessageId: txID,
		Message:   fullMsg,
		ConsumableAmounts: map[string]int64{
			"bits": amount,
		},
		ShouldSendAnyway: sendAnyway,
	})

	if expectError != nil {
		So(err, ShouldNotBeNil)
		So(err.Error(), ShouldResemble, expectError.Error())
	} else {
		So(err, ShouldBeNil)
		So(resp.ChatMessageSent, ShouldEqual, expectChatSent)

		if validateIRC {
			ircWaitCtx, cancel := context.WithTimeout(ctx, 10*time.Second)
			defer cancel()

			select {
			case <-ircWaitCtx.Done():
				So(errors.New("timed out waiting for irc response"), ShouldBeNil)
			case ircErr := <-ircErrChan:
				So(ircErr, ShouldBeNil)
			}
		}
	}

	return txID
}

func MakeUserMod(ctx context.Context, zumaClient zuma.Client, channelID string, modUserID string) {
	_, err := zumaClient.AddMod(ctx, channelID, modUserID, channelID, nil)
	So(err, ShouldBeNil)

}

func WaitForIRCMessageInQaBitsTests(msg string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	oauthToken, err := GetOAuthToken()
	if err != nil {
		return err
	}

	conn := irc.IRC(LOGIN_qa_bits_tests, LOGIN_qa_bits_tests)
	conn.UseTLS = false
	conn.Password = fmt.Sprintf("oauth:%s", oauthToken)

	err = conn.Connect(ircEndpoint)
	if err != nil {
		return err
	}

	msgChannel := make(chan string, 1)
	loginFailed := make(chan bool, 1)

	conn.AddCallback("PRIVMSG", func(event *irc.Event) {
		if msg == event.Message() {
			msgChannel <- event.Message()
		}
	})

	conn.AddCallback("NOTICE", func(event *irc.Event) {
		if strings.Contains(event.Message(), "Login authentication failed") {
			loginFailed <- true
		}
	})

	conn.Join("#" + LOGIN_qa_bits_tests)

	select {
	case <-ctx.Done():
		return errors.New("timed out waiting on irc message")
	case <-msgChannel:
		return nil
	case <-loginFailed:
		return errors.New("irc login failed")
	}
}

func GetOAuthToken() (string, error) {
	token := os.Getenv("QA_BITS_TESTS_OAUTH_TOKEN")
	if token == "" {
		return "", errors.New("qa_bits_test oauth token is not set")
	}
	return token, nil
}
