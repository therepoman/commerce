// +build integration

package integration_tests

import (
	"context"
	"testing"

	prism "code.justin.tv/commerce/prism/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestHealthCheck(t *testing.T) {
	Convey("Test the health check endpoint", t, func() {
		prismClient := GetPrismClient()
		_, err := prismClient.HealthCheck(context.Background(), &prism.HealthCheckReq{})
		So(err, ShouldBeNil)
	})
}
