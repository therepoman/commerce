// +build integration

package integration_tests

import (
	"context"
	"errors"
	"testing"
	"time"

	prism "code.justin.tv/commerce/prism/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestUseBits_GetsAutomodded_ThenDenied(t *testing.T) {
	Convey("Given the necessary clients", t, func() {
		ctx := GetIntegrationTestContext(true)

		paydayClient := GetPaydayClient()
		prismClient := GetPrismClient()
		zumaClient := GetZumaClient()
		clueClient := GetClueClient()

		Convey("Given an existing test channel with strict automod settings", func() {
			channelID := TUID_qa_bits_tests // change to qa_bits_tests once automod settings have propogated to staging
			clueClient.SetAutmodPropertiesToStrict(ctx, channelID)

			Convey("Given a new test user with sufficient bits in their balance", func() {
				userID := TUID_bits_test_1
				AddBits(paydayClient, userID, 100)

				Convey("Given a new test user that is a mod in the channel", func() {
					modID := TUID_bits_test_2
					MakeUserMod(ctx, zumaClient, channelID, modID)

					Convey("When prism process message is called with a vulgar message, it is flagged by automod", func() {
						PerformPrismBitsTransaction(ctx, prismClient, "", userID, channelID, 100,
							"cheer100 shit", false, FlaggedByAutomodError, false)

						Convey("Succeeds when the message is sent anyway", func() {
							PerformPrismBitsTransaction(ctx, prismClient, "", userID, channelID, 100,
								"cheer100 shit", true, nil, false)

							Convey("Succeeds when prism is called with automod deny", func() {
								_, err := prismClient.DenyAutoModMessage(ctx, &prism.DenyAutoModMessageReq{
									UserId:       modID,
									TargetUserId: userID,
									MessageId:    RandomUUID(),
								})
								So(err, ShouldBeNil)
							})
						})
					})
				})
			})
		})
	})
}

func TestUseBits_GetsAutomodded_ThenApproved(t *testing.T) {
	Convey("Given the necessary clients", t, func() {
		ctx := GetIntegrationTestContext(true)

		paydayClient := GetPaydayClient()
		prismClient := GetPrismClient()
		zumaClient := GetZumaClient()
		clueClient := GetClueClient()

		vulgarCheer := "cheer100 shit"

		Convey("Given an existing test channel", func() {
			channelID := TUID_qa_bits_tests // change to qa_bits_tests once automod settings have propogated to staging
			clueClient.SetAutmodPropertiesToStrict(ctx, channelID)

			Convey("Given an existing test user with sufficient bits in their balance", func() {
				userID := TUID_bits_test_1
				AddBits(paydayClient, userID, 100)

				modID := TUID_bits_test_2
				MakeUserMod(ctx, zumaClient, channelID, modID)

				Convey("When prism process message is called with a vulgar message, it is flagged by automod", func() {
					PerformPrismBitsTransaction(ctx, prismClient, "", userID, channelID, 100,
						"cheer100 shit", false, FlaggedByAutomodError, false)

					Convey("Succeeds when the message is sent anyway", func() {
						msgID := PerformPrismBitsTransaction(ctx, prismClient, "", userID, channelID, 100,
							vulgarCheer, true, nil, false)

						Convey("Succeeds when prism is called with automod approve and the message shows up in irc", func() {
							ircErrChan := make(chan error, 1)
							go func(fullMsg string) {
								ircErrChan <- WaitForIRCMessageInQaBitsTests(fullMsg)

							}(msgID + " " + vulgarCheer)
							_, err := prismClient.ApproveAutoModMessage(ctx, &prism.ApproveAutoModMessageReq{
								UserId:       modID,
								TargetUserId: userID,
								MessageId:    RandomUUID(),
							})
							So(err, ShouldBeNil)

							ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
							defer cancel()

							select {
							case <-ctx.Done():
								So(errors.New("timed out waiting on irc message"), ShouldBeNil)
							case ircErr := <-ircErrChan:
								So(ircErr, ShouldBeNil)
							}
						})
					})
				})
			})
		})
	})
}
