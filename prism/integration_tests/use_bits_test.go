// +build integration

package integration_tests

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestUseBits_SingleUserHappyPath(t *testing.T) {
	Convey("Given the necessary clients", t, func() {
		ctx := GetIntegrationTestContext(true)

		paydayClient := GetPaydayClient()
		prismClient := GetPrismClient()

		Convey("Given an existing test channel", func() {
			channelID := TUID_qa_bits_tests

			Convey("Given an existing test user with sufficient bits in their balance", func() {
				userID := TUID_bits_test_1
				AddBits(paydayClient, userID, 100)

				Convey("When prism process message is called the chat message is sent without error", func() {
					PerformPrismBitsTransaction(ctx, prismClient, "", userID, channelID, 100, "cheer100",
						false, nil, true)
				})
			})
		})
	})
}

func TestUseBits_InsufficientBalance(t *testing.T) {
	Convey("Given the necessary clients", t, func() {
		ctx := GetIntegrationTestContext(true)

		prismClient := GetPrismClient()
		userServiceClient := GetUserServiceClient()

		Convey("Given an existing test channel", func() {
			channelID := TUID_qa_bits_tests

			Convey("Given a new test user with insufficient bits in their balance", func() {
				userID := CreateTestUser(ctx, userServiceClient)

				Convey("When prism process message is called no chat message is sent", func() {
					PerformPrismBitsTransaction(ctx, prismClient, "", userID, channelID, 100, "cheer100",
						false, FailedPreconditionOnProcessError, false)

					Convey("Cleanup the test user", func() {
						CleanupTestUser(ctx, userServiceClient, userID)
					})
				})
			})
		})
	})
}
