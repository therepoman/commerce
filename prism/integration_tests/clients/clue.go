package clients

import (
	"context"

	tmi_client "code.justin.tv/chat/tmi/client"
	clue_api "code.justin.tv/chat/tmi/clue/api"
	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/foundation/twitchclient"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	strictModeration = 3
)

type clue struct {
	Client tmi_client.Client
}

type Clue interface {
	SetAutmodPropertiesToStrict(ctx context.Context, channelID string)
}

func NewTMIClient(host string) (Clue, error) {
	tmiClient, err := tmi_client.NewClient(twitchclient.ClientConf{
		Host: host,
	})
	if err != nil {
		return nil, err
	}

	return &clue{
		Client: tmiClient,
	}, nil
}

func (c *clue) SetAutmodPropertiesToStrict(ctx context.Context, channelID string) {
	err := c.Client.UpdateInternalRoomProperties(ctx, channelID, tmi_client.UpdateInternalRoomPropertiesRequest{
		AutoModProperties: &clue_api.ChannelAutoModProperties{
			AggressiveChannelLevel: pointers.Int64P(strictModeration),
			IdentityChannelLevel:   pointers.Int64P(strictModeration),
			ProfanityChannelLevel:  pointers.Int64P(strictModeration),
			SexualChannelLevel:     pointers.Int64P(strictModeration),
		},
		BroadcasterLanguageMode:    pointers.BoolP(false),
		ChatDelayDuration:          pointers.IntP(0),
		ChatFastsubs:               pointers.BoolP(false),
		ChatRequireVerifiedAccount: pointers.BoolP(false),
		ChatRules:                  []string{},
		HideChatLinks:              pointers.BoolP(false),
		RitualsEnabled:             pointers.BoolP(false),
		R9kOnlyChat:                pointers.BoolP(false),
		SubscribersOnlyChat:        pointers.BoolP(false),
	}, nil)
	So(err, ShouldBeNil)
}
