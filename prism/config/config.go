package config

import (
	"fmt"
	"io/ioutil"
	"os"

	log "code.justin.tv/commerce/logrus"
	"github.com/go-yaml/yaml"
)

const (
	LocalConfigFilePath            = "/src/code.justin.tv/commerce/prism/config/data/%s.yaml"
	GlobalConfigFilePath           = "/etc/prism/config/%s.yaml"
	EnvironmentEnvironmentVariable = "ENVIRONMENT"

	UnitTest  = Environment("unit-test")
	SmokeTest = Environment("smoke-test")
	Local     = Environment("local")
	Staging   = Environment("staging")
	Prod      = Environment("prod")
	Default   = Local
)

type Config struct {
	EnvironmentName                   string                                  `yaml:"environment-name"`
	StatsPrefix                       string                                  `yaml:"stats-prefix"`
	StatsURL                          string                                  `yaml:"stats-url"`
	AWSRegion                         string                                  `yaml:"aws-region"`
	EventBusSQSQueueURL               string                                  `yaml:"eventbus-sqs-url"`
	NumberOfEventBusWorkers           int                                     `yaml:"num-of-eventbus-workers"`
	CloudwatchMetricsConfig           *CloudwatchMetricsConfig                `yaml:"cloudwatch-metrics-config"`
	DynamoSuffix                      string                                  `yaml:"dynamo-suffix"`
	SandstormRoleARN                  string                                  `yaml:"sandstorm-role-arn"`
	SandstormECCPublicKey             string                                  `yaml:"sandstorm-ecc-public-key"`
	S2SServiceName                    string                                  `yaml:"s2s-service-name"`
	S2SServiceOrigin                  string                                  `yaml:"s2s-service-origin"`
	S2SEnabled                        bool                                    `yaml:"s2s-enabled"`
	S2SPassthrough                    bool                                    `yaml:"s2s-passthrough"`
	S2SAuthLogGroup                   string                                  `yaml:"s2s-auth-log-group"`
	UseRedisClusterMode               bool                                    `yaml:"use-redis-cluster-mode"`
	RedisEndpoint                     string                                  `yaml:"redis-endpoint"`
	Pachinko                          ClientConfig                            `yaml:"pachinko"`
	Payday                            ClientConfig                            `yaml:"payday"`
	Prism                             ClientConfig                            `yaml:"prism"`
	Zuma                              ClientConfig                            `yaml:"zuma"`
	TMI                               ClientConfig                            `yaml:"tmi"`
	Clue                              ClientConfig                            `yaml:"clue"`
	AutoMod                           ClientConfig                            `yaml:"automod"`
	UsersService                      ClientConfig                            `yaml:"users-service"`
	Rooms                             ClientConfig                            `yaml:"rooms"`
	Spade                             ClientConfig                            `yaml:"spade"`
	AutoModSNSTopicARN                string                                  `yaml:"automod-sns-topic-arn"`
	AutoModTimeoutQueueConfig         *QueueConfig                            `yaml:"automod-timeout-queue"`
	Tenants                           map[TenantName]TenantConfig             `yaml:"tenants"`
	ConsumableTypes                   map[ConsumableType]ConsumableTypeConfig `yaml:"consumable-types"`
	ChatFailureSlackWebhookURL        string                                  `yaml:"chat-failure-slack-webhook-url"`
	ChatFailureS3Bucket               string                                  `yaml:"chat-failure-s3-bucket"`
	ChatFailureSNSTopicARN            string                                  `yaml:"chat-failure-sns-topic-arn"`
	SandstormRole                     string                                  `yaml:"sandstorm-role"`
	ChatFailureSlackWebHookSecretName string                                  `yaml:"sandstorm-chat-failure-slack-web-hook-secret-name"`
	AuditLogGroup                     string                                  `yaml:"audit-log-group"`
}

type CloudwatchMetricsConfig struct {
	BufferSize               int     `yaml:"buffer-size"`
	BatchSize                int     `yaml:"batch-size"`
	FlushIntervalMinutes     int     `yaml:"flush-interval-minutes"`
	FlushCheckDelayMS        int     `yaml:"flush-check-delay-ms"`
	EmergencyFlushPercentage float64 `yaml:"emergency-flush-percentage"`
}

type QueueConfig struct {
	Name       string `yaml:"name"`
	URL        string `yaml:"url"`
	NumWorkers int    `yaml:"num-workers"`
}

type ClientConfig struct {
	Endpoint            string `yaml:"endpoint"`
	TimeoutMilliseconds int    `yaml:"timeout-milliseconds"`
}

type Environment string

var Environments = map[Environment]interface{}{UnitTest: nil, SmokeTest: nil, Local: nil, Staging: nil, Prod: nil}

func IsValidEnvironment(env Environment) bool {
	_, ok := Environments[env]
	return ok
}

func LoadConfig(env Environment) (*Config, error) {
	if !IsValidEnvironment(env) {
		log.Errorf("Invalid environment: %s. Falling back to local", env)
		env = Local
	}

	baseFileName := string(env)
	filePath, err := getConfigFilePath(baseFileName)
	if err != nil {
		return nil, err
	}
	return loadConfig(filePath)
}

func loadConfig(path string) (*Config, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer func() {
		err := file.Close()
		if err != nil {
			log.WithError(err).Error("Error closing config file")
		}
	}()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var cfg Config
	err = yaml.Unmarshal(fileBytes, &cfg)
	if err != nil {
		return nil, err
	}
	return &cfg, nil
}

func getConfigFilePath(baseFilename string) (string, error) {
	localFname := os.Getenv("GOPATH") + fmt.Sprintf(LocalConfigFilePath, baseFilename)
	if _, err := os.Stat(localFname); !os.IsNotExist(err) {
		return localFname, nil
	}
	globalFname := fmt.Sprintf(GlobalConfigFilePath, baseFilename)
	if _, err := os.Stat(globalFname); os.IsNotExist(err) {
		return "", err
	}
	return globalFname, nil
}

func (c *Config) IsProd() bool {
	return Environment(c.EnvironmentName) == Prod
}

func (c *Config) IsStaging() bool {
	return Environment(c.EnvironmentName) == Staging
}
