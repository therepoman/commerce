package config

// TenantName is service name like payday, pachinko, etc
type TenantName string

func (tn TenantName) String() string {
	return string(tn)
}

// TenantType is whether the tenant is externally hosted (ex: payday), or locally hosted within prism (ex: local)
type TenantType string

func (tt TenantType) String() string {
	return string(tt)
}

const (
	TenantType_External = TenantType("external")
	TenantType_Local    = TenantType("local")
)

var TenantTypes = map[TenantType]interface{}{
	TenantType_External: nil,
	TenantType_Local:    nil,
}

// TenantConfig is config options for a given tenant
type TenantConfig struct {
	Type                TenantType `yaml:"type"`
	URL                 string     `yaml:"url"`
	S2SEnabled          bool       `yaml:"s2s-enabled"`
	TimeoutMilliseconds int        `yaml:"timeout-milliseconds"`
}

// ConsumableType is a unit that can be consumed (ex: bits, volds)
type ConsumableType string

func (cn ConsumableType) String() string {
	return string(cn)
}

// BalanceScope is whether consumable balances are global, channel-specific, emote-specific, etc
type BalanceScope string

func (bs BalanceScope) String() string {
	return string(bs)
}

const (
	BalanceScope_Global  = BalanceScope("global")
	BalanceScope_Channel = BalanceScope("channel")
	BalanceScope_Emote   = BalanceScope("emote")
)

var BalanceScopes = map[BalanceScope]interface{}{
	BalanceScope_Global:  nil,
	BalanceScope_Channel: nil,
	BalanceScope_Emote:   nil,
}

// ConsumableTypeConfig is config options for a given consumable-type, maps the consumable-type to a particular tenant
type ConsumableTypeConfig struct {
	TenantName   TenantName   `yaml:"tenant-name"`
	BalanceScope BalanceScope `yaml:"balance-scope"`
}
