package main

import (
	"context"
	"errors"
	"log"
	"net/http"
	"time"

	prism "code.justin.tv/commerce/prism/rpc"
)

const (
	smokeTestTimeout        = 10 * time.Second
	healthCheckAttemptDelay = 500 * time.Millisecond
	localhostTwirpEndpoint  = "http://localhost:8000"
)

type SmokeTest struct {
	PrismClient prism.Prism
}

func main() {
	log.Println("Starting smoke test")
	prismClient := prism.NewPrismProtobufClient(localhostTwirpEndpoint, http.DefaultClient)
	smokeTest := &SmokeTest{
		PrismClient: prismClient,
	}

	start := time.Now()
	for time.Since(start) < smokeTestTimeout {
		err := smokeTest.performHealthCheck()
		if err == nil {
			log.Println("Smoke test pinged health check successfully")
			return
		}

		log.Println(err)
		time.Sleep(healthCheckAttemptDelay)
	}
	log.Panicln("Smoke test failed all attempts to ping health check")
}

func (st *SmokeTest) performHealthCheck() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	log.Println("Pinging healthcheck API...")

	resp, err := st.PrismClient.HealthCheck(ctx, &prism.HealthCheckReq{})
	if err != nil {
		return err
	}

	if resp == nil {
		return errors.New("receive nil response from health check")
	}

	return nil
}
