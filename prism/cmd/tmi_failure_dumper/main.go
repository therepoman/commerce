package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"code.justin.tv/commerce/gogogadget/aws/sqs/sns"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/prism/backend/message/chat_error_handler"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	awssqs "github.com/aws/aws-sdk-go/service/sqs"
)

const (
	sqsRegion        = "us-west-2"
	tmiFailuresQueue = "https://sqs.us-west-2.amazonaws.com/033842175527/prod-chat-errors-sqs"
	outputDir        = "/Users/seph/sandbox/tmi-failures/"
)

func main() {
	sess, err := session.NewSession()
	if err != nil {
		panic(err)
	}
	sqsClient := awssqs.New(sess, &aws.Config{Region: aws.String(sqsRegion)})

	// Poll SQS messages
	messages, err := pollMessages(sqsClient, tmiFailuresQueue)
	if err != nil {
		panic(err)
	}

	amountByUser := make(map[string]int64)
	amountByChannel := make(map[string]int64)
	var grandTotal int64

	// Individual Transactions
	transactionsFile, err := os.Create(outputDir + "transactions.csv")
	if err != nil {
		panic(err)
	}
	defer func() {
		innerErr := transactionsFile.Close()
		if innerErr != nil {
			fmt.Println(innerErr)
		}
	}()

	transactionsWriter := csv.NewWriter(transactionsFile)
	defer transactionsWriter.Flush()

	err = transactionsWriter.Write([]string{
		"Timestamp",
		"BitsAmount",
		"UserID",
		"UserLogin",
		"ChannelID",
		"ChannelLogin",
		"Message",
	})
	if err != nil {
		panic(err)
	}

	for _, message := range messages {
		var snsMessage sns.Message
		err = json.Unmarshal([]byte(*message.Body), &snsMessage)
		if err != nil {
			panic(err)
		}

		var chatError chat_error_handler.ChatError
		err = json.Unmarshal([]byte(snsMessage.Message), &chatError)
		if err != nil {
			panic(err)
		}

		sanitizedMessage := strings.Replace(chatError.RequestedMessage, "\n", " ", -1)
		bitsAmount := chatError.ConsumablesRequested["bits"]["bits"]

		err = transactionsWriter.Write([]string{
			chatError.Timestamp.String(),
			fmt.Sprintf("%d", bitsAmount),
			chatError.UserID,
			chatError.UserLogin,
			chatError.ChannelID,
			chatError.ChannelLogin,
			sanitizedMessage,
		})
		if err != nil {
			panic(err)
		}

		amountByUser[chatError.UserLogin] += bitsAmount
		amountByChannel[chatError.ChannelLogin] += bitsAmount
		grandTotal += bitsAmount
	}

	// Bits Amount By User
	usersFile, err := os.Create(outputDir + "users.csv")
	if err != nil {
		panic(err)
	}
	defer func() {
		innerErr := usersFile.Close()
		if innerErr != nil {
			fmt.Println(innerErr)
		}
	}()

	usersWriter := csv.NewWriter(usersFile)
	defer usersWriter.Flush()

	for user, amount := range amountByUser {
		err = usersWriter.Write([]string{
			user,
			fmt.Sprintf("%d", amount),
		})
		if err != nil {
			panic(err)
		}
	}

	// Bits Amount By Channel
	channelsFile, err := os.Create(outputDir + "channels.csv")
	if err != nil {
		panic(err)
	}
	defer func() {
		innerErr := channelsFile.Close()
		if innerErr != nil {
			fmt.Println(innerErr)
		}
	}()

	channelsWriter := csv.NewWriter(channelsFile)
	defer channelsWriter.Flush()

	for channel, amount := range amountByChannel {
		err = channelsWriter.Write([]string{
			channel,
			fmt.Sprintf("%d", amount),
		})
		if err != nil {
			panic(err)
		}
	}

	fmt.Printf("Grand Bits Total: %d \n", grandTotal)

}

func pollMessages(sqsClient *awssqs.SQS, queueUrl string) ([]*awssqs.Message, error) {
	messages := make([]*awssqs.Message, 0)

	for {
		params := &awssqs.ReceiveMessageInput{
			QueueUrl: aws.String(queueUrl),
			AttributeNames: []*string{
				aws.String("All"),
			},
			MaxNumberOfMessages: aws.Int64(10),
			MessageAttributeNames: []*string{
				aws.String("All"),
			},
			VisibilityTimeout: aws.Int64(10),
			WaitTimeSeconds:   aws.Int64(1),
		}

		resp, err := sqsClient.ReceiveMessage(params)
		if err != nil {
			log.WithError(err).Error("Unable to receive message from SQS queue")
			return nil, err
		}

		if len(resp.Messages) > 0 {
			messages = append(messages, resp.Messages...)
		} else {
			return messages, nil
		}
	}
}
