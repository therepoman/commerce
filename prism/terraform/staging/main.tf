module "prism" {
  source                                = "../modules/prism"
  owner                                 = "twitch-prism-aws-devo@amazon.com"
  aws_profile                           = "prism-devo"
  aws_account_id                        = "413064813793"
  env                                   = "staging"
  env_full                              = "staging"
  vpc_id                                = "vpc-083f2f60d58100ca8"
  private_subnets                       = "subnet-00f7ce51dbdcebc73,subnet-08eab58e003d03c11,subnet-046bd9286363dc805"
  private_cidr_blocks                   = ["10.204.244.0/24", "10.204.245.0/24", "10.204.246.0/24"]
  sg_id                                 = "sg-0c9f4dba41ab4580c"
  instance_type                         = "t2.micro"
  cname_prefix                          = "prism-staging"
  min_host_count                        = "2"
  max_host_count                        = "4"
  deployment_max_batch_size             = "1"
  dynamo_min_read_capacity              = 5
  dynamo_min_write_capacity             = 5
  autoscaling_role                      = "arn:aws:iam::413064813793:role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable"
  elasticache_replicas_per_node_group   = "1"
  elasticache_num_node_groups           = "2"
  elasticache_instance_type             = "cache.m3.medium"
  sns_trusted_role_arns                 = ["arn:aws:iam::413064813793:role/staging-commerce-prism"]
  application_logs_cloudwatch_log_group = "/aws/elasticbeanstalk/staging-commerce-prism-env/var/log/eb-docker/containers/eb-current-app/stdouterr.log"
}

terraform {
  backend "s3" {
    bucket  = "prism-terraform-devo"
    key     = "tfstate/commerce/prism/terraform/staging"
    region  = "us-west-2"
    profile = "prism-devo"
  }
}

provider "aws" {
  region  = "us-west-2"
  profile = "prism-devo"
}
