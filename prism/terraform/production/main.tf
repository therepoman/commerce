module "prism" {
  source                                = "../modules/prism"
  owner                                 = "twitch-prism-aws-prod@amazon.com"
  aws_profile                           = "prism-prod"
  aws_account_id                        = "033842175527"
  env                                   = "prod"
  env_full                              = "production"
  vpc_id                                = "vpc-0d0d64db5414beaa5"
  private_subnets                       = "subnet-092630d4cbd862190,subnet-0368f56cf24bb8850,subnet-0b0c1cd317c3e38b7"
  private_cidr_blocks                   = ["10.204.252.0/24", "10.204.253.0/24", "10.204.254.0/24"]
  sg_id                                 = "sg-0c288c05f07a42d1e"
  instance_type                         = "c3.xlarge"
  cname_prefix                          = "prism-prod"
  min_host_count                        = "8"
  max_host_count                        = "64"
  deployment_max_batch_size             = "3"
  dynamo_min_read_capacity              = 100
  dynamo_min_write_capacity             = 100
  autoscaling_role                      = "arn:aws:iam::033842175527:role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable"
  elasticache_replicas_per_node_group   = "2"
  elasticache_num_node_groups           = "2"
  elasticache_instance_type             = "cache.r4.xlarge"
  sns_trusted_role_arns                 = ["arn:aws:iam::033842175527:role/prod-commerce-prism"]
  application_logs_cloudwatch_log_group = "/aws/elasticbeanstalk/prod-commerce-prism-env/var/log/eb-docker/containers/eb-current-app/stdouterr.log"

  // Legal recommends 30 days TTL for sensitive log groups
  audit_logs_retention_in_days    = 30
  s2s_auth_logs_retention_in_days = 30
}

terraform {
  backend "s3" {
    bucket  = "prism-terraform-prod"
    key     = "tfstate/commerce/prism/terraform/prod"
    region  = "us-west-2"
    profile = "prism-prod"
  }
}

provider "aws" {
  region  = "us-west-2"
  profile = "prism-prod"
}
