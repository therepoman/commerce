module "prism_alarms" {
  count                                 = "${var.env == "prod" ? 1 : 0}"
  env                                   = "${var.env}"
  source                                = "prism_alarms"
  application_logs_cloudwatch_log_group = "${var.application_logs_cloudwatch_log_group}"
}