# This sets up a Twitch Security Log funnel that can subscribe to different CW log groups.
# We only need 1 funnel per account.

# What logs to send: https://wiki.xarth.tv/display/CTI/%5BRM-03%5D+What+to+log
# instruction: https://wiki.xarth.tv/pages/viewpage.action?spaceKey=SEC&title=Sending+Logs+to+Twitch+Security#SendingLogstoTwitchSecurity-ECS(includingFargate)

resource "aws_cloudformation_stack" "twitch_security_log_funnel" {
  count        = "${var.env == "prod" ? 1 : 0}"
  name         = "twitch-security-log-funnel"
  template_url = "https://s3-us-west-2.amazonaws.com/twitch-tails-cloudformation-production-us-west-2/funnel.yaml"
  capabilities = ["CAPABILITY_IAM"]
}
