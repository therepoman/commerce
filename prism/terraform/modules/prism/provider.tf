provider "aws" {
  region  = "us-west-2"
  profile = "${var.aws_profile}"
  alias   = "${var.aws_profile}"
  version = "2.57.0"             # Pinning to specific version as per https://jira.twitch.com/browse/MOMENTS-1268
}
