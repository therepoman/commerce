module "automod_timeout_sqs" {
  source            = "../simple_sqs"
  environment       = "${var.env}"
  base_queue_name   = "automod-timeout"
  permitted_sns_arn = "${module.automod_sns.topic_arn}"
  delivery_delay    = "60"
}

resource "aws_sns_topic_subscription" "automod_timeout_sqs_subscription" {
  topic_arn = "${module.automod_sns.topic_arn}"
  protocol  = "sqs"
  endpoint  = "${module.automod_timeout_sqs.simple_queue_arn}"
}

module "chat_errors_sqs" {
  source            = "../simple_sqs"
  environment       = "${var.env}"
  base_queue_name   = "chat-errors"
  permitted_sns_arn = "${module.chat_errors_sns.topic_arn}"
  delivery_delay    = "60"
}

resource "aws_sns_topic_subscription" "chat_errors_sqs_subscription" {
  topic_arn = "${module.chat_errors_sns.topic_arn}"
  protocol  = "sqs"
  endpoint  = "${module.chat_errors_sqs.simple_queue_arn}"
}
