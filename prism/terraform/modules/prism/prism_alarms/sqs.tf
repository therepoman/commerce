// AutoMod timeout SQS
resource "aws_cloudwatch_metric_alarm" "automod_timeout_handler_latency" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-sqs-worker-AutoModTimeout-latency"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "sqs_worker_AutoModTimeout_latency"
  namespace                 = "${var.prism_namespace}"
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "5"
  unit                      = "Seconds"
  alarm_description         = "AutoMod timeout SQS worker latency"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  dimensions                = {
    Region   = "us-west-2"
    Service  = "prism"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "automod_timeout_dlq_count" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-sqs-AutoModTimeout-dlq-count"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "AutoMod timeout SQS DLQ count is high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  dimensions                = {
    QueueName = "${var.env}-automod-timeout-dlq"
  }
}

resource "aws_cloudwatch_metric_alarm" "eventbus_automod_updated_caught_messages_dlq_count" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-sqs-eventbus-AutoModUpdatedCaughtMessages-dlq-count"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "AutoMod Updated Caught Messages SQS DLQ count is high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  dimensions                = {
    QueueName = "eventbus_automod_updated_messages_queue_deadletter"
  }
}