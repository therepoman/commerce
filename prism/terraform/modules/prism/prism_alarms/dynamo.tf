// Transactions DynamoDB table throttling
resource "aws_cloudwatch_metric_alarm" "transactions_throttled_read_events" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-transactions-read-throttles"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ReadThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "Transactions DynamoDB table read throttle events"
  insufficient_data_actions = []
  dimensions                = {
    TableName = "transactions-${var.env}"
  }
}

resource "aws_cloudwatch_metric_alarm" "transactions_throttled_write_events" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-transactions-write-throttles"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "WriteThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "Transactions DynamoDB table write throttle events"
  treat_missing_data        = "notBreaching"
  insufficient_data_actions = []
  dimensions                = {
    TableName = "transactions-${var.env}"
  }
}