// ProcessMessage
resource "aws_cloudwatch_metric_alarm" "process_message_latency_p90" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-ProcessMessage-latency-p90"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.ProcessMessage.response"
  namespace                 = "${var.prism_namespace}"
  period                    = "60"
  extended_statistic        = "p90"
  threshold                 = "1"
  unit                      = "Seconds"
  alarm_description         = "ProcessMessage API latency p90"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  dimensions                = {
    Region   = "us-west-2"
    Service  = "prism"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "process_message_latency_p99" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-ProcessMessage-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.ProcessMessage.response"
  namespace                 = "${var.prism_namespace}"
  period                    = "60"
  extended_statistic        = "p99"
  threshold                 = "2"
  unit                      = "Seconds"
  alarm_description         = "ProcessMessage API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  dimensions                = {
    Region   = "us-west-2"
    Service  = "prism"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

// ApproveAutoModMessage
resource "aws_cloudwatch_metric_alarm" "approve_automod_message_errors" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-ApproveAutoModMessage-500-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.ApproveAutoModMessage.500"
  namespace                 = "${var.prism_namespace}"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  unit                      = "Count"
  alarm_description         = "ApproveAutoModMessage API 500 errors"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  dimensions                = {
    Region   = "us-west-2"
    Service  = "prism"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "approve_automod_message_latency_p50" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-ApproveAutoModMessage-latency-p50"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.ApproveAutoModMessage.response"
  namespace                 = "${var.prism_namespace}"
  period                    = "300"
  extended_statistic        = "p50"
  threshold                 = "2"
  unit                      = "Seconds"
  alarm_description         = "ApproveAutoModMessage API latency p50"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  dimensions                = {
    Region   = "us-west-2"
    Service  = "prism"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "approve_automod_message_latency_p90" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-ApproveAutoModMessage-latency-p90"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.ApproveAutoModMessage.response"
  namespace                 = "${var.prism_namespace}"
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "3"
  unit                      = "Seconds"
  alarm_description         = "ApproveAutoModMessage API latency p90"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  dimensions                = {
    Region   = "us-west-2"
    Service  = "prism"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "approve_automod_message_latency_p99" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-ApproveAutoModMessage-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.ApproveAutoModMessage.response"
  namespace                 = "${var.prism_namespace}"
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "5"
  unit                      = "Seconds"
  alarm_description         = "ApproveAutoModMessage API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  dimensions                = {
    Region   = "us-west-2"
    Service  = "prism"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

// DenyAutoModMessage
resource "aws_cloudwatch_metric_alarm" "deny_automod_message_errors" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-DenyAutoModMessage-500-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.DenyAutoModMessage.500"
  namespace                 = "${var.prism_namespace}"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  unit                      = "Count"
  alarm_description         = "DenyAutoModMessage API 500 errors"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  dimensions                = {
    Region   = "us-west-2"
    Service  = "prism"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "deny_automod_message_latency_p50" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-DenyAutoModMessage-latency-p50"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.DenyAutoModMessage.response"
  namespace                 = "${var.prism_namespace}"
  period                    = "300"
  extended_statistic        = "p50"
  threshold                 = "2"
  unit                      = "Seconds"
  alarm_description         = "DenyAutoModMessage API latency p50"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  dimensions                = {
    Region   = "us-west-2"
    Service  = "prism"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "deny_automod_message_latency_p90" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-DenyAutoModMessage-latency-p90"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.DenyAutoModMessage.response"
  namespace                 = "${var.prism_namespace}"
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "3"
  unit                      = "Seconds"
  alarm_description         = "DenyAutoModMessage API latency p90"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  dimensions                = {
    Region   = "us-west-2"
    Service  = "prism"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "deny_automod_message_latency_p99" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-DenyAutoModMessage-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.DenyAutoModMessage.response"
  namespace                 = "${var.prism_namespace}"
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "5"
  unit                      = "Seconds"
  alarm_description         = "DenyAutoModMessage API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  dimensions                = {
    Region   = "us-west-2"
    Service  = "prism"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

/* Availability custom metric */
module "ProcessMessage_availability" {
  source                     = "availability"
  api_name                   = "ProcessMessage"
  env                        = "${var.env}"
  low_watermark              = "0.995"
  requests_metric_name_5xx   = "counter.twirp.status_codes.ProcessMessage.500"
  requests_metric_name_total = "counter.twirp.ProcessMessage.requests"
}