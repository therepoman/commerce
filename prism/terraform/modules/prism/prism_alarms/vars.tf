variable "env" {
  description = "Whether it's prod, dev, etc"
}

variable "prism_namespace" {
  default = "prism"
  type = "string"
}

variable "logscan_namespace" {
  default = "prism-logs"
  type = "string"
}

variable "application_logs_cloudwatch_log_group" {
  description = "Cloudwatch log group in which application logs are written"
  type = "string"
}

variable "count" {
  description = "The number of alarms to create. Mainly used to disable alarms."
}

