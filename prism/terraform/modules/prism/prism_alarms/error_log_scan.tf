resource "aws_cloudwatch_log_metric_filter" "error_scan" {
  count          = "${var.count}"
  name           = "prism-error-scan"
  pattern        = "level=error"
  log_group_name = "${var.application_logs_cloudwatch_log_group}"

  metric_transformation {
    name          = "prism-errors"
    namespace     = "${var.logscan_namespace}"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_metric_alarm" "prism_error_warning" {
  count                     = "${var.count}"
  alarm_name                = "prism-${var.env}-logscan-error-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "7"
  metric_name               = "prism-errors"
  namespace                 = "${var.logscan_namespace}"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "15"
  unit                      = "Count"
  alarm_description         = "Low volume of error logs"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "prism_error_alert" {
  count                     = "${var.count}"
  alarm_name                = "prism-${var.env}-logscan-error-alert"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "7"
  metric_name               = "prism-errors"
  namespace                 = "${var.logscan_namespace}"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "30"
  unit                      = "Count"
  alarm_description         = "High volume of error logs"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_log_metric_filter" "panic_scan" {
  count          = "${var.count}"
  name           = "prism-panic-scan"
  pattern        = "\"panic:\""
  log_group_name = "${var.application_logs_cloudwatch_log_group}"

  metric_transformation {
    name          = "prism-panics"
    namespace     = "${var.logscan_namespace}"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_metric_alarm" "prism_panic_warning" {
  count                     = "${var.count}"
  alarm_name                = "prism-${var.env}-logscan-panic-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  metric_name               = "prism-panics"
  namespace                 = "${var.logscan_namespace}"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "Low volume of panic logs"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "prism_panic_alert" {
  count                     = "${var.count}"
  alarm_name                = "prism-${var.env}-logscan-panic-alert"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  metric_name               = "prism-panics"
  namespace                 = "${var.logscan_namespace}"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "25"
  unit                      = "Count"
  alarm_description         = "High volume of panic logs"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}