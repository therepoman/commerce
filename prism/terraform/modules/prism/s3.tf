resource "aws_s3_bucket" "bucket" {
  bucket = "prism-${var.env_full}-chat-errors"
  acl    = "private"
}