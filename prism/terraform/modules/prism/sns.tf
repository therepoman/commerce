module "automod_sns" {
  source = "../simple_sns"
  environment = "${var.env}"
  base_topic_name = "automod"
  trusted_publisher_role_arns = ["${var.sns_trusted_role_arns}"]
}

module "chat_errors_sns" {
  source = "../simple_sns"
  environment = "${var.env}"
  base_topic_name = "chat-errors"
  trusted_publisher_role_arns = ["${var.sns_trusted_role_arns}"]
}