module "prism_dynamo_tables" {
  source                   = "prism_dynamo_tables"
  suffix                   = "${var.env}"
  autoscaling_role         = "${var.autoscaling_role}"
  min_read_capacity        = "${var.dynamo_min_read_capacity}"
  min_write_capacity       = "${var.dynamo_min_write_capacity}"
  dynamo_backup_lambda_arn = "${module.dynamo_backup_lambda.lambda_arn}"
}