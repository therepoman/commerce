output "beanstalk_environment_name" {
  value = "${module.beanstalk.beanstalk_environment_name}"
}

output "beanstalk_environment_cname" {
  value = "${module.beanstalk.beanstalk_environment_cname}"
}

output "beanstalk_autoscaling_groups" {
  value = "${module.beanstalk.autoscaling_groups}"
}

output "beanstalk_instances" {
  value = "${module.beanstalk.instances}"
}

output "beanstalk_launch_configurations" {
  value = "${module.beanstalk.launch_configurations}"
}

output "beanstalk_load_balancers" {
  value = "${module.beanstalk.load_balancers}"
}

output "beanstalk_iam_role_id" {
  value = "${module.beanstalk.iam_role_id}"
}

output "beanstalk_iam_role_arn" {
  value = "${module.beanstalk.iam_role_arn}"
}
