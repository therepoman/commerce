resource "aws_dynamodb_table" "transactions_table" {
  name           = "transactions-${var.suffix}"
  billing_mode   = "PAY_PER_REQUEST"
  hash_key       = "transaction_id"
  stream_enabled           = "true"
  stream_view_type         = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = "true"
  }

  attribute {
    name = "transaction_id"
    type = "S"
  }

  ttl {
    attribute_name = "ttl"
    enabled        = true
  }
}

module "transactions_scheduled_backups" {
  source                   = "../../scheduled_dynamo_backup_event"
  table_name               = "transactions-${var.suffix}"
  dynamo_backup_lambda_arn = "${var.dynamo_backup_lambda_arn}"
}
