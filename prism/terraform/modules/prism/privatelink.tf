module "privatelink-zone" {
  source      = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-zone"
  name        = "prism"
  environment = "${var.env_full}"
}

module "privatelink-cert" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-cert"

  name        = "prism"
  environment = "${var.env_full}"
  zone_id     = "${module.privatelink-zone.zone_id}"
  alb_dns     = "${module.beanstalk.beanstalk_environment_cname}"
}

// TODO: import endpoint service into terraform

// privatelinks with downstreams

module "privatelink-ldap" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint"
  name            = "ldap-a2z"
  endpoint        = "com.amazonaws.vpce.us-west-2.vpce-svc-0437151f68c61b808"
  security_groups = ["${var.sg_id}"]
  vpc_id          = "${var.vpc_id}"
  subnets         = "${split(",", var.private_subnets)}"
  dns             = "ldap.twitch.a2z.com"
}

module "privatelink-automod-prod" {
  source   = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint"
  name     = "automod"
  endpoint = "com.amazonaws.vpce.us-west-2.vpce-svc-0b4773c8348e9917b"
  dns      = "us-west-2.prod.twitchsafetyautomod.s.twitch.a2z.com"
  security_groups = ["${var.sg_id}"]
  vpc_id          = "${var.vpc_id}"
  subnets         = "${split(",", var.private_subnets)}"
}

module "privatelink-automod-staging" {
  source   = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint"
  name     = "automod"
  endpoint = "com.amazonaws.vpce.us-west-2.vpce-svc-0c165e608e2e3772f"
  dns      = "us-west-2.beta.twitchsafetyautomod.s.twitch.a2z.com"
  security_groups = ["${var.sg_id}"]
  vpc_id          = "${var.vpc_id}"
  subnets         = "${split(",", var.private_subnets)}"
}