variable "aws_profile" {
  description = "The AWS Named Profile to use when running terraform and creating AWS resources"
}

variable "aws_account_id" {
  description = "AWS Account ID for the service"
}

variable "aws_region" {
  description = "AWS region to operate in."
  default     = "us-west-2"
}

variable "vpc_id" {
  description = "Id of the systems-created VPC"
}

variable "private_subnets" {
  description = "Comma-separated list of private subnets"
}

variable "private_cidr_blocks" {
  description = "The private subnet cidr blocks for this stage"
  type        = "list"
}

variable "sg_id" {
  description = "Id of the security group"
}

variable "instance_type" {
  description = "EC2 instance type"
}

variable "owner" {
  description = "Owner of the account"
}

variable "service" {
  description = "Name of the service being built. Should probably leave this as the default"
  default     = "commerce/prism"
}

variable "service_short_name" {
  description = "Short name of the service being built. Should probably leave this as the default"
  default     = "prism"
}

variable "env" {
  description = "Whether it's prod, dev, etc"
}

variable "env_full" {
  description = "Whether it's production, development, etc"
}

variable "cname_prefix" {
  description = "Prefix of the cname"
}

variable "eb_environment_service_role" {
  description = "The name of an IAM role that Elastic Beanstalk uses to manage resources for the environment."
  default     = "aws-elasticbeanstalk-service-role"
}

variable "redis_port" {
  description = "The port used by the redis server"
  default     = 6379
}

variable "min_host_count" {
  description = "Minimum number of hosts in the beanstalk ASG"
  default     = 1
}

variable "max_host_count" {
  description = "Maximum number of hosts in the beanstalk ASG"
  default     = 4
}

variable "asgtrigger_measure_name" {
  description = "Metric used for your Auto Scaling trigger."
  default     = "NetworkOut"
}

variable "asgtrigger_unit" {
  description = "Unit for the trigger measurement, such as Bytes."
  default     = "Percent"
}

variable "asgtrigger_lower_threshold" {
  description = "If the measurement falls below this number for the breach duration, a trigger is fired."
  default     = "5"
}

variable "asgtrigger_upper_threshold" {
  description = "If the measurement is higher than this number for the breach duration, a trigger is fired."
  default     = "60"
}

variable "deployment_max_batch_size" {
  description = "the maximum of hosts to be deployed to at a time"
}

variable "autoscaling_role" {
  description = "Role that will be used to perform dynamo autoscaling actions"
}

variable "dynamo_min_read_capacity" {
  description = "Min read capacity for dynamodb tables"
}

variable "dynamo_min_write_capacity" {
  description = "Min write capacity for dynamodb tables"
}

variable "elasticache_auto_failover" {
  description = "Describes the type of failover behavior for the elasticache groups"
  default     = true
}

variable "elasticache_instance_type" {
  description = "Describes the type of instance to be used in our elasticache cluster"
  default     = "cache.m4.large"
}

variable "elasticache_replicas_per_node_group" {
  description = "Number of read replicas per elasticache node group"
}

variable "elasticache_num_node_groups" {
  description = "Number of elasticache node groups"
}

variable "sns_trusted_role_arns" {
  description = "List of trusted role arns that can publish to SNS"
  type        = "list"
}

variable "application_logs_cloudwatch_log_group" {
  description = "Cloudwatch log group in which application logs are written"
  type        = "string"
}

variable "audit_logs_retention_in_days" {
  default     = 7
  description = "Audit Log Retention Period, defaults to 7 days"
}

variable "s2s_auth_logs_retention_in_days" {
  default     = 7
  description = "S2S Auth Log Retention Period, defaults to 7 days"
}
