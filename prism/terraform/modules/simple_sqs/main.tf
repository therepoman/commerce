resource "aws_sqs_queue_policy" "queue_policy" {
  queue_url = "${aws_sqs_queue.simple_sqs_queue.id}"
  policy = "${data.aws_iam_policy_document.default_queue_policy_doc.json}"
}

data "aws_iam_policy_document" "default_queue_policy_doc" {
  statement {
    sid = "PrismSimpleSQSSid"

    actions = [
      "SQS:SendMessage",
    ]

    principals {
      identifiers = [
        "*",
      ]
      type = "AWS"
    }

    resources = [
      "${aws_sqs_queue.simple_sqs_queue.arn}",
    ]

    condition {
      test = "ArnEquals"
      variable = "aws:SourceArn"

      values = [
        "${var.permitted_sns_arn}",
      ]
    }
  }
}

resource "aws_sqs_queue" "simple_sqs_queue_dlq" {
  message_retention_seconds = "${var.message_retention_seconds}"
  name = "${var.environment}-${var.base_queue_name}-dlq"
}

resource "aws_sqs_queue" "simple_sqs_queue" {
  message_retention_seconds = "${var.message_retention_seconds}"
  name = "${var.environment}-${var.base_queue_name}-sqs"
  receive_wait_time_seconds = "${var.receive_wait_time}"
  delay_seconds = "${var.delivery_delay}"
  redrive_policy = <<EOF
{
  "deadLetterTargetArn": "${aws_sqs_queue.simple_sqs_queue_dlq.arn}",
  "maxReceiveCount": ${var.max_receive_count}
}
EOF
}
