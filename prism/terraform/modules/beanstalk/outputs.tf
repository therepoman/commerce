output "beanstalk_environment_name" {
  value = "${aws_elastic_beanstalk_environment.bs.name}"
}

output "beanstalk_environment_cname" {
  value = "${aws_elastic_beanstalk_environment.bs.cname}"
}

output "autoscaling_groups" {
  value = "${aws_elastic_beanstalk_environment.bs.autoscaling_groups}"
}

output "instances" {
  value = "${aws_elastic_beanstalk_environment.bs.instances}"
}

output "launch_configurations" {
  value = "${aws_elastic_beanstalk_environment.bs.launch_configurations}"
}

output "load_balancers" {
  value = "${aws_elastic_beanstalk_environment.bs.load_balancers}"
}

output "iam_role_id" {
  value = "${aws_iam_role.bs.id}"
}

output "iam_role_arn" {
  value = "${aws_iam_role.bs.arn}"
}

output "iam_role_name" {
  value = "${aws_iam_role.bs.name}"
}

output "cname" {
  value = "${aws_elastic_beanstalk_environment.bs.cname}"
}