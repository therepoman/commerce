data "http" "eventbus_cloudformation" {
  url = "https://eventbus-setup.s3-us-west-2.amazonaws.com/cloudformation.yaml"
}

resource "aws_cloudformation_stack" "eventbus" {
  name          = "EventBus"
  capabilities  = ["CAPABILITY_NAMED_IAM"]
  template_body = "${data.http.eventbus_cloudformation.body}"
}

resource "aws_iam_role_policy_attachment" "eventbus_access_attach" {
  role       = "${aws_iam_role.bs.name}"
  policy_arn = "${aws_cloudformation_stack.eventbus.outputs["EventBusAccessPolicyARN"]}"
}

resource "aws_sqs_queue" "eventbus_automod_updated_messages_queue" {
  name              = "eventbus_automod_updated_messages_queue"
  policy            = "${aws_cloudformation_stack.eventbus.outputs["EventBusSQSPolicyDocument"]}"
  kms_master_key_id = "${aws_cloudformation_stack.eventbus.outputs["EventBusKMSMasterKeyARN"]}"
  redrive_policy    = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.eventbus_automod_updated_messages_queue_deadletter.arn}\",\"maxReceiveCount\":5}"
  message_retention_seconds = "7200" // 2 hours
}

resource "aws_sqs_queue" "eventbus_automod_updated_messages_queue_deadletter" {
  name              = "eventbus_automod_updated_messages_queue_deadletter"
  kms_master_key_id = "${aws_cloudformation_stack.eventbus.outputs["EventBusKMSMasterKeyARN"]}"
  message_retention_seconds = "1209600" // 14 days
}