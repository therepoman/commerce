variable "base_topic_name" {
  type = "string"
}

variable "trusted_publisher_role_arns" {
  type = "list"
}

variable "environment" {
  type = "string"
}
