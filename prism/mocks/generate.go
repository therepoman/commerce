package mocks

// Use command: make generate_mocks
// To regenerate the mocks defined in this file

// To add additional mocks follow this template:
///go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery
//  -name={Interface Name}
//  -dir=$GOPATH/src/code.justin.tv/commerce/prism/{Path to Interface}
//  -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/{Path to Interface}
//  -outpkg={Interface Package}_mock

// Chat Error Handler
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=ChatErrorHandler -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/message/chat_error_handler -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/message/chat_error_handler -outpkg=chat_error_handler_mock

// Throttler
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Throttler -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/throttle -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/throttle -outpkg=throttler_mock

// Rooms Client
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Chatrooms -dir=$GOPATH/src/code.justin.tv/commerce/prism/vendor/code.justin.tv/chat/chatrooms/rpc/chatrooms -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/chat/chatrooms/rpc -outpkg=chatrooms_mock

// Rooms Client Wrapper
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/clients/rooms -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/rooms -outpkg=rooms_mock

// Prism
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Prism -dir=$GOPATH/src/code.justin.tv/commerce/prism/rpc -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/rpc -outpkg=prism_mock

// Tenant Mapper
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Mapper -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/tenant -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/tenant -outpkg=tenant_mock

// Prism Tenant Client
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=PrismTenant -dir=$GOPATH/src/code.justin.tv/commerce/prism/rpc/tenant -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/rpc/tenant -outpkg=prism_tenant_mock

// Message Router
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Router -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/message -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/message -outpkg=message_router_mock

// Http Utils Client
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=HttpUtilClient -dir=$GOPATH/src/code.justin.tv/commerce/prism/vendor/code.justin.tv/commerce/gogogadget/http -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/gogogadget -outpkg=http_mock

// Zuma Client
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/prism/vendor/code.justin.tv/chat/zuma/client -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/chat/zuma -outpkg=zuma_mock

// AutoMod Client
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=TwitchSafetyAutoMod -dir=$GOPATH/src/code.justin.tv/commerce/prism/vendor/code.justin.tv/amzn/TwitchSafetyAutoModTwirp -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/amzn/chmod_automod -outpkg=chmod_automod_mock

// Stats Client
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Statter -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/clients/stats -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/stats  -outpkg=stats_mock

// Base Filter
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Filter -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/filter -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/filter -outpkg=filter_mock

// Transaction DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/dynamo/transaction -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/dynamo/transaction -outpkg=transaction_mock

// Process Message Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/validation/process_message -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/validation/process_message -outpkg=process_message_mock

// Approve AutoMod Message Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/validation/approve_automod_message -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/validation/approve_automod_message -outpkg=approve_automod_message_mock

// Deny AutoMod Message Validator
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Validator -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/validation/deny_automod_message -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/validation/deny_automod_message -outpkg=deny_automod_message_mock

// Landlord
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Landlord -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/landlord -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/landlord -outpkg=landlord_mock

// TMI Client
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=ITMIClient -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/clients/tmi -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/tmi -outpkg=tmi_mock

// Redis Client
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/clients/redis -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/redis -outpkg=redis_mock

// Transaction Processor
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Processor -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/transaction -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/transaction -outpkg=transaction_mock

// Transformer
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Transformer -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/transformation -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/transformation -outpkg=transformation_mock

// AutoMod Cache
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Cache -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/cache/automod -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/cache/automod -outpkg=automod_mock

// Users Service User Cache
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Cache -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/cache/usersservice -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/cache/usersservice -outpkg=usersservice_mock

// Users Service User Fetcher
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Fetcher -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/usersservice -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/usersservice -outpkg=usersservice_mock

// SNS Client
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=IClient -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/clients/sns -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/sns -outpkg=sns_mock

// Users Service Internal Client
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=InternalClient -dir=$GOPATH/src/code.justin.tv/commerce/prism/vendor/code.justin.tv/web/users-service/client/usersclient_internal -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/web/users-service/client/usersclient_internal -outpkg=usersclient_internal_mock

// AutoMod Processor
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Processor -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/automod -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/automod -outpkg=automod_mock

// Consumable Mapper
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Mapper -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/consumable -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/consumable -outpkg=consumable_mock

// Anonymous Message Error Handler
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=AnonymousMessageErrorHandler -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/anonymous_message -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/anonymous_message -outpkg=anonymous_message_mock

// DataScience Tracker
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Tracker -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/datascience -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/datascience -outpkg=datascience_mock

// Spade Client
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/prism/vendor/code.justin.tv/common/spade-client-go/spade -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/common/spade-client-go/spade -outpkg=spade_mock

// Payday Client
//go:generate $GOPATH/src/code.justin.tv/commerce/prism/_tools/bin/retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/prism/backend/clients/payday/ -output=$GOPATH/src/code.justin.tv/commerce/prism/mocks/code.justin.tv/commerce/prism/backend/clients/payday -outpkg=payday_mock
