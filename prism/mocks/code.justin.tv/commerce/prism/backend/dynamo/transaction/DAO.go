// Code generated by mockery v1.0.0
package transaction_mock

import mock "github.com/stretchr/testify/mock"
import transaction "code.justin.tv/commerce/prism/backend/dynamo/transaction"

// DAO is an autogenerated mock type for the DAO type
type DAO struct {
	mock.Mock
}

// CreateNew provides a mock function with given fields: t
func (_m *DAO) CreateNew(t *transaction.Transaction) error {
	ret := _m.Called(t)

	var r0 error
	if rf, ok := ret.Get(0).(func(*transaction.Transaction) error); ok {
		r0 = rf(t)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// CreateOrUpdate provides a mock function with given fields: t
func (_m *DAO) CreateOrUpdate(t *transaction.Transaction) error {
	ret := _m.Called(t)

	var r0 error
	if rf, ok := ret.Get(0).(func(*transaction.Transaction) error); ok {
		r0 = rf(t)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// GetTransaction provides a mock function with given fields: transactionID
func (_m *DAO) GetTransaction(transactionID string) (*transaction.Transaction, error) {
	ret := _m.Called(transactionID)

	var r0 *transaction.Transaction
	if rf, ok := ret.Get(0).(func(string) *transaction.Transaction); ok {
		r0 = rf(transactionID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*transaction.Transaction)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(transactionID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
