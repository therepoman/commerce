package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"path"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/logrus"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/golang/protobuf/jsonpb"
	vegeta "github.com/tsenart/vegeta/lib"
)

const (
	paydayEndpoint = "https://internal-payday-staging-41489265.us-west-2.elb.amazonaws.com/twirp/code.justin.tv.commerce.paydayrpc.Payday"
	dataDir        = "data"
	outDir         = "loadtest-results"
)

var (
	getBitsBadgeTierEmotes = flag.Bool("getbitsbadgetieremotes", false, "run GetBitsBadgeTierEmotes load test")
)

/**
If you need to call above 100TPS, copy the following file into your ec2 box's /etc/security/limits.conf:

# /etc/security/limits.conf
#
#This file sets the resource limits for the users logged in via PAM.
#It does not affect resource limits of the system services.
#
#Also note that configuration files in /etc/security/limits.d directory,
#which are read in alphabetical order, override the settings in this
#file in case the domain is the same or more specific.
#That means for example that setting a limit for wildcard domain here
#can be overriden with a wildcard setting in a config file in the
#subdirectory, but a user specific setting here can be overriden only
#with a user specific setting in the subdirectory.
#
#Each line describes a limit for a user in the form:
#
#<domain>        <type>  <item>  <value>
#
#Where:
#<domain> can be:
#        - a user name
#        - a group name, with @group syntax
#        - the wildcard *, for default entry
#        - the wildcard %, can be also used with %group syntax,
#                 for maxlogin limit
#
#<type> can have the two values:
#        - "soft" for enforcing the soft limits
#        - "hard" for enforcing hard limits
#
#<item> can be one of the following:
#        - core - limits the core file size (KB)
#        - data - max data size (KB)
#        - fsize - maximum filesize (KB)
#        - memlock - max locked-in-memory address space (KB)
#        - nofile - max number of open file descriptors
#        - rss - max resident set size (KB)
#        - stack - max stack size (KB)
#        - cpu - max CPU time (MIN)
#        - nproc - max number of processes
#        - as - address space limit (KB)
#        - maxlogins - max number of logins for this user
#        - maxsyslogins - max number of logins on the system
#        - priority - the priority to run user process with
#        - locks - max number of file locks the user can hold
#        - sigpending - max number of pending signals
#        - msgqueue - max memory used by POSIX message queues (bytes)
#        - nice - max nice priority allowed to raise to values: [-20, 19]
#        - rtprio - max realtime priority
#
#<domain>      <type>  <item>         <value>
#
#*               soft    core            0
#*               hard    rss             10000
#@student        hard    nproc           20
#@faculty        soft    nproc           20
#@faculty        hard    nproc           50
#ftp             hard    nproc           0
#@student        -       maxlogins       4
*         hard    nofile      1000000
*         soft    nofile      1000000
root      hard    nofile      1000000
root      soft    nofile      1000000
# End of file

*/

func init() {
	flag.Parse()
	rand.Seed(time.Now().UTC().UnixNano())
}

func main() {
	if *getBitsBadgeTierEmotes {
		var testUsers []string
		readJSON("UserIDs", &testUsers)

		var testChannels []string
		readJSON("ChannelIDs", &testChannels)

		testCase_GetBitsBadgeTierEmoteRewards(&testUsers, &testChannels)
	}
}

func getRandomTestItem(itemsP *[]string) string {
	if itemsP == nil {
		panic(errors.New("received nil items in getRandomTestItem"))
	}

	items := *itemsP

	randomIndex := random.Int(0, len(items)-1)
	return items[randomIndex]
}

/**
GetBitsBadgeTierEmoteRewards
*/
func createGetBitsBadgeTierEmotesTargeter(users *[]string, channels *[]string) vegeta.Targeter {
	headers := make(http.Header)
	headers.Add("Content-Type", "application/json")
	count := 0
	url := paydayEndpoint + "/GetBitsBadgeTierEmotes"

	return func(t *vegeta.Target) (err error) {
		userID := getRandomTestItem(users)
		channelID := getRandomTestItem(channels)

		req := &paydayrpc.GetBitsBadgeTierEmotesReq{
			UserId:    userID,
			ChannelId: channelID,
		}

		reqBody := bytes.NewBuffer(nil)
		marshaler := &jsonpb.Marshaler{OrigName: true}
		if err = marshaler.Marshal(reqBody, req); err != nil {
			return err
		}

		t.Method = "POST"
		t.URL = url
		t.Header = headers
		t.Body = reqBody.Bytes()
		count++
		return err
	}
}

func testCase_GetBitsBadgeTierEmoteRewards(users *[]string, channels *[]string) {
	//we currently have 20 EC2 instances in staging, so multiply the mean and amp by 20 each
	testDuration := time.Minute * 30
	pacer := vegeta.SinePacer{
		Period:  testDuration,
		Mean:    vegeta.Rate{501, time.Second},
		Amp:     vegeta.Rate{499, time.Second},
		StartAt: vegeta.Trough,
	}

	attack(createGetBitsBadgeTierEmotesTargeter(users, channels), pacer, testDuration, "GetBitsBadgeTierEmotes")
}

/**
Utils
*/

func readJSON(sourceFileName string, out interface{}) {
	dir, err := os.Getwd()

	if err != nil {
		panic(err)
	}

	source := path.Join(dir, dataDir, fmt.Sprintf("%s.json", sourceFileName))

	bytes, err := ioutil.ReadFile(source)

	err = json.Unmarshal(bytes, out)

	if err != nil {
		panic(err)
	}
}

func printMetrics(title string, metrics vegeta.Metrics) {
	dir, err := os.Getwd()

	if err != nil {
		panic(err)
	}

	dest := path.Join(dir, outDir, fmt.Sprintf("%d_%s_results.txt", time.Now().Unix(), title))

	writers := make([]io.Writer, 0)
	writers = append(writers, os.Stdout)

	os.MkdirAll(path.Join(dir, outDir), os.ModePerm)
	file, err := os.Create(dest)
	if err != nil {
		logrus.WithError(err).WithField("dest", dest).Info("Error creating results file")
	} else {
		writers = append(writers, file)
	}

	w := io.MultiWriter(writers...)

	_, _ = fmt.Fprintf(w, "\n====== %s Results ======\n", title)

	_, _ = fmt.Fprintf(w, "Start Time: %s \n", metrics.Earliest.String())
	_, _ = fmt.Fprintf(w, "End Time: %s \n", metrics.Latest.String())
	_, _ = fmt.Fprintf(w, "Duration: %s \n", metrics.Duration.String())

	_, _ = fmt.Fprintf(w, "Requests: %d \n", metrics.Requests)
	_, _ = fmt.Fprintf(w, "Success Percentage: %f \n", metrics.Success)
	_, _ = fmt.Fprintf(w, "Request Rate: %f \n", metrics.Rate)
	_, _ = fmt.Fprintf(w, "Succesful Request Throughput: %f \n\n", metrics.Throughput)

	_, _ = fmt.Fprintf(w, "Latency Average: %s \n", metrics.Latencies.Mean)
	_, _ = fmt.Fprintf(w, "Latency p99: %s \n", metrics.Latencies.P99)
	_, _ = fmt.Fprintf(w, "Latency Max: %s \n\n", metrics.Latencies.Max)

	_, _ = fmt.Fprintf(w, "Status Codes: %v \n", metrics.StatusCodes)
	_, _ = fmt.Fprintf(w, "Errors: %v\n", metrics.Errors)
}

func attack(targeter vegeta.Targeter, pacer vegeta.Pacer, dur time.Duration, name string) {
	logrus.Info("Starting attack on " + name)
	attacker := vegeta.NewAttacker()
	var metrics vegeta.Metrics
	for res := range attacker.Attack(targeter, pacer, dur, name+"-Attack") {
		metrics.Add(res)
	}
	metrics.Close()

	printMetrics(name, metrics)
}
