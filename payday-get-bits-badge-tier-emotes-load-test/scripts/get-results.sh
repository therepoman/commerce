#!/bin/bash

. $GOPATH/src/code.justin.tv/commerce/payday-get-bits-badge-tier-emotes-load-test/variables.sh

outputDir=$OUTPUT_DIR
loadTestIPs=($(aws ec2 describe-instances --filters "Name=tag:Name,Values=load-testing" --profile ${AWS_PROFILE} | jq -cr .Reservations[].Instances[].PrivateIpAddress))

mkdir -p $outputDir

for i in "${loadTestIPs[@]}"
do
    echo "Getting results from to ${i}..."
    scp "${USERNAME}@${i}:nohup.out" "${outputDir}/nohup-${i}.txt"
    scp "${USERNAME}@${i}:test.out" "${outputDir}/${i}.txt"
done