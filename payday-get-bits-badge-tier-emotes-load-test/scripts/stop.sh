#!/bin/bash

. $GOPATH/src/code.justin.tv/commerce/payday-get-bits-badge-tier-emotes-load-test/variables.sh

loadTestIPs=($(aws ec2 describe-instances --filters "Name=tag:Name,Values=load-testing" --profile ${AWS_PROFILE} | jq -cr .Reservations[].Instances[].PrivateIpAddress))

for i in "${loadTestIPs[@]}"
do
    echo "SCPing to ${i}..."
    scp $GOPATH/src/code.justin.tv/commerce/payday-get-bits-badge-tier-emotes-load-test/scripts/stop-load-test.sh "${USERNAME}@${i}:."
    scp variables.sh "${USERNAME}@${i}:."
    ssh -t "${USERNAME}@${i}" ./stop-load-test.sh
done