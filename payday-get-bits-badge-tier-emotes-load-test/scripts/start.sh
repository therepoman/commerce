#!/bin/bash

. $GOPATH/src/code.justin.tv/commerce/payday-get-bits-badge-tier-emotes-load-test/variables.sh

loadTestIPs=($(aws ec2 describe-instances --filters "Name=tag:Name,Values=load-testing" --profile ${AWS_PROFILE} | jq -cr .Reservations[].Instances[].PrivateIpAddress))

echo "compiling main.go"
GOOS=linux go build -o main

for i in "${loadTestIPs[@]}"
do
    echo "SCPing to ${i}..."
    scp main "${USERNAME}@${i}:."
    scp -r data "${USERNAME}@${i}:."
    scp $GOPATH/src/code.justin.tv/commerce/payday-get-bits-badge-tier-emotes-load-test/scripts/run-load-test.sh "${USERNAME}@${i}:."
    scp variables.sh "${USERNAME}@${i}:."
    ssh -t "${USERNAME}@${i}" ulimit -Sn 4096
    ssh -t "${USERNAME}@${i}" nohup ./run-load-test.sh
done