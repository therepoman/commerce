// Seed plan:
//   * 1,000 users, each having
//   * A random number of Bits used in each of
//   * 1000 channels, with their cumulative number
//   * Evenly distributed between 0 and 100K (where zero cheers are omitted from Pantheon ingestion)
//   * With each channel having 75% of its unlocked bits Emotes uploaded
//   * At randomly set tiers

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"path"
	"strings"
	"time"

	"code.justin.tv/commerce/pantheon/backend/leaderboard/domain"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/random"
	mako_client "code.justin.tv/commerce/mako/twirp"
	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/payday/badgetiers"
	payday "code.justin.tv/commerce/payday/client"
	api2 "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/commerce/payday/utils/math"
	"code.justin.tv/foundation/twitchclient"
	"github.com/gofrs/uuid"
	"golang.org/x/time/rate"
)

const (
	maxCumulativeBitsValue = 100000
	usersToCreate          = 1000
	channelsToCreate       = 1000
	pantheonHost           = "https://main.us-west-2.beta.pantheon.twitch.a2z.com"     //"https://main.us-west-2.prod.pantheon.twitch.a2z.com"
	paydayHost             = "https://main.us-west-2.beta.payday.twitch.a2z.com"     //"https://prod.payday.twitch.a2z.com"
	makoHost               = "https://main.us-west-2.beta.mako.twitch.a2z.com" //"https://main.us-west-2.prod.mako.twitch.a2z.com"
	outDir                 = "data"

	emoteImage28ID  = "127380717_28_b3531eb6-1788-4cee-8048-62a637fbb169"
	emoteImage56ID  = "127380717_56_fe005d66-d7cf-4cf2-aea8-8781c3547309"
	emoteImage112ID = "127380717_112_5deeb472-e1a0-4df8-9981-da229d39b1f3"
	userIDPrefix    = "bits_get_emotes_load_test_user"

	maxPantheonIngestTPS = 300
	maxPaydayIngestTPS   = 300
	maxMakoIngestTPS     = 300
)

var createPantheonLimiter = rate.NewLimiter(maxPantheonIngestTPS, 1)
var createPaydayLimiter = rate.NewLimiter(maxPaydayIngestTPS, 1)
var createMakoLimiter = rate.NewLimiter(maxMakoIngestTPS, 1)

type ChannelIDToMax struct {
	ChannelID string
	Max       int64
}

type Progress struct {
	ChannelIdsToMax         []ChannelIDToMax
	BadgeTierConfigurations []api2.GetBadgesResponse
	Emotes                  []mako_client.CreateEmoticonReponse
	UserIDs                 []string
}

func makePantheonClient() pantheonrpc.Pantheon {
	return pantheonrpc.NewPantheonProtobufClient(pantheonHost, http.DefaultClient)
}

func makePaydayClient() payday.Client {
	paydayClient, err := payday.NewClient(twitchclient.ClientConf{
		Host: paydayHost,
	})

	if err != nil {
		panic(err)
	}

	return paydayClient
}

func makeMakoClient() mako_client.Mako {
	return mako_client.NewMakoProtobufClient(makoHost, http.DefaultClient)
}

func ingestTestDataIntoPantheon(pantheon pantheonrpc.Pantheon, progress Progress) []ChannelIDToMax {
	// we don't need anymore for this step
	if len(progress.ChannelIdsToMax) >= channelsToCreate {
		return progress.ChannelIdsToMax
	}

	ctx := context.Background()

	var channelIDs = make([]string, 0, channelsToCreate-len(progress.ChannelIdsToMax))
	for i := 0; i < len(channelIDs); i++ {
		channelID := createUniqueTestID()
		channelIDs = append(channelIDs, channelID)
	}

	var userIDs = make([]string, 0, usersToCreate)
	for i := 0; i < usersToCreate; i++ {
		userID := createUniqueTestID()
		userIDs = append(userIDs, userID)
	}

	for _, channelID := range channelIDs {
		max := int64(0)
		for j, userID := range userIDs {
			err := createPantheonLimiter.Wait(ctx)
			if err != nil {
				panic(err)
			}

			eventID, err := uuid.NewV4()
			if err != nil {
				panic(err)
			}

			randomCumulativeBitsValue := getRandomCumulativeBitsValue()

			max = math.MaxInt64(max, randomCumulativeBitsValue)

			if randomCumulativeBitsValue > 0 {
				event := pantheonrpc.PublishEventReq{
					Domain:      "bits-usage-by-channel-v1",
					EventId:     eventID.String(),
					TimeOfEvent: uint64(time.Now().UnixNano()),
					GroupingKey: channelID,
					EntryKey:    userID,
					EventValue:  randomCumulativeBitsValue,
				}

				_, err = pantheon.PublishEvent(ctx, &event)
				if err != nil {
					panic(err)
				}
			}
			reportProgress("FillPantheonLeaderboardsChannel", len(progress.ChannelIdsToMax), channelsToCreate)
			reportProgress("FillPantheonLeaderboardsUser", j, usersToCreate)
		}

		channelIDToMax := ChannelIDToMax{
			ChannelID: channelID,
			Max:       max,
		}

		progress.ChannelIdsToMax = append(progress.ChannelIdsToMax, channelIDToMax)
		writeResults("in-progress-pantheon", progress) // Cache intermediate output
	}

	return progress.ChannelIdsToMax
}

func getRandomCumulativeBitsValue() int64 {
	return random.Int64(0, maxCumulativeBitsValue)
}

func createUniqueTestID() string {
	ownerUUID, err := uuid.NewV4()

	if err != nil {
		panic(err)
	}

	return fmt.Sprintf("%s-%s", userIDPrefix, ownerUUID.String())
}

func ingestTestEmoteGroupsIntoPayday(payday payday.Client, progress Progress) []api2.GetBadgesResponse {
	ctx := context.Background()

	for len(progress.BadgeTierConfigurations) < len(progress.ChannelIdsToMax) {
		// start where we left off!
		channelIDToMax := progress.ChannelIdsToMax[len(progress.BadgeTierConfigurations)]

		err := createPaydayLimiter.Wait(ctx)
		if err != nil {
			panic(err)
		}

		setBadgesRequest := createSetBadgesRequest(channelIDToMax.Max)

		resp, err := payday.SetBadgeTiers(ctx, channelIDToMax.ChannelID, setBadgesRequest, nil)
		if err != nil {
			panic(err)
		}

		if resp != nil {
			progress.BadgeTierConfigurations = append(progress.BadgeTierConfigurations, *resp)
			reportProgress("FillPaydayBadgesConfig", len(progress.BadgeTierConfigurations), len(progress.ChannelIdsToMax))
			writeResults("in-progress-payday", progress) // Cache intermediate output
		}
	}

	return progress.BadgeTierConfigurations
}

func createSetBadgesRequest(max int64) api2.SetBadgesRequest {
	tiers := make([]*api2.BadgeTierSetting, 0)
	for threshold := range badgetiers.BitsChatBadgeTiers {
		if threshold == 1 || threshold == 100 {
			continue
		}

		if threshold > max {
			break
		}

		// 66% chance of an unlocked slot being filled
		shouldBeFilled := random.Int(0, 2) > 0

		if shouldBeFilled {
			code, codeSuffix := getCode()
			tier := api2.BadgeTierSetting{
				Threshold: threshold,
				Enabled:   pointers.BoolP(true),
				// These are not strictly speaking used, they are required by validation rules, but we short circuit
				// before we get to calling Mako (so they're basically discarded by Payday).
				EmoticonSettings: &[]api2.EmoticonSetting{
					{
						Code:       code,
						CodeSuffix: pointers.StringP(codeSuffix),
					},
				},
			}

			tiers = append(tiers, &tier)
		}
	}

	setBadgesRequest := api2.SetBadgesRequest{
		Tiers: tiers,
	}

	return setBadgesRequest
}

func getCode() (string, string) {
	suffixUUID, err := uuid.NewV4()
	if err != nil {
		panic(err)
	}

	suffix := strings.ToUpper(strings.ReplaceAll(suffixUUID.String(), "-", ""))

	prefix := "bterloadtest"

	code := prefix + suffix
	codeSuffix := suffix

	return code, codeSuffix
}

func ingestEmotesIntoMako(makoClient mako_client.Mako, progress Progress) []mako_client.CreateEmoticonReponse {
	ctx := context.Background()

	fmt.Printf("Starting Mako loop with %d of %d\n", len(progress.Emotes), len(progress.BadgeTierConfigurations))
	for len(progress.Emotes) < len(progress.BadgeTierConfigurations) {
		badge := progress.BadgeTierConfigurations[len(progress.Emotes)]

		fmt.Printf(fmt.Sprintf("%d\n", len(progress.Emotes)))

		if len(badge.Tiers) == 0 {
			progress.Emotes = append(progress.Emotes, mako_client.CreateEmoticonReponse{})
			reportProgress("Mako emote config", len(progress.Emotes), len(progress.BadgeTierConfigurations))
			writeResults("in-progress-mako", progress) // Cache intermediate output
		}

		for _, tier := range badge.Tiers {
			if tier.EmoticonUploadConfigurations == nil || len(*tier.EmoticonUploadConfigurations) == 0 {
				fmt.Printf("Skipping tier on channel %s\n", tier.ChannelId)
				continue
			}

			err := createMakoLimiter.Wait(ctx)
			if err != nil {
				panic(err)
			}

			emoticonUploadConfigs := *tier.EmoticonUploadConfigurations
			emoticonUploadConfig := emoticonUploadConfigs[0]

			code, codeSuffix := getCode()
			createEmoticonRequest := mako_client.CreateEmoticonRequest{
				UserId:                    tier.ChannelId,
				Code:                      code,
				CodeSuffix:                codeSuffix,
				State:                     mako_client.EmoteState_active.String(),
				GroupId:                   emoticonUploadConfig.GroupID,
				Image28Id:                 emoteImage28ID,
				Image56Id:                 emoteImage56ID,
				Image112Id:                emoteImage112ID,
				Domain:                    mako_client.Domain_emotes.String(),
				EmoteGroup:                mako_client.EmoteGroup_BitsBadgeTierEmotes,
				EnforceModerationWorkflow: false,
			}
			resp, err := makoClient.CreateEmoticon(ctx, &createEmoticonRequest)
			if err != nil {
				panic(err)
			}

			if resp != nil {
				progress.Emotes = append(progress.Emotes, *resp)
				reportProgress("Mako emote config", len(progress.Emotes), len(progress.BadgeTierConfigurations))
				writeResults("in-progress-mako", progress) // Cache intermediate output
			}
		}
	}

	return progress.Emotes
}

func getUserIDsFromPantheon(pantheonClient pantheonrpc.Pantheon, progress Progress) []string {
	ctx := context.Background()

	if len(progress.UserIDs) >= len(progress.ChannelIdsToMax) {
		return progress.UserIDs
	}

	var userIDs []string
	for i, channelIDToMax := range progress.ChannelIdsToMax {

		err := createPantheonLimiter.Wait(ctx)
		if err != nil {
			panic(err)
		}

		req := &pantheonrpc.GetLeaderboardReq{
			Domain:      string(domain.BitsUsageByChannelDomain),
			TimeUnit:    pantheonrpc.TimeUnit_ALLTIME,
			GroupingKey: channelIDToMax.ChannelID,
			TopN:        1000,
		}

		resp, err := pantheonClient.GetLeaderboard(ctx, req)
		if err != nil {
			panic(err)
		}

		if resp != nil {
			for _, entry := range resp.Top {
				userIDs = append(userIDs, entry.EntryKey)
			}

			reportProgress("GetPantheonUserIDs", i, len(progress.ChannelIdsToMax))
		}
	}

	return dedupeUserIDs(userIDs)
}

func dedupeUserIDs(userIDs []string) []string {
	var distinctUserIDsMap = make(map[string]bool, usersToCreate)

	for _, user := range userIDs {
		distinctUserIDsMap[user] = true
	}

	var distinctUserIDs = make([]string, 0, len(distinctUserIDsMap))
	for userID := range distinctUserIDsMap {
		distinctUserIDs = append(distinctUserIDs, userID)
	}

	return distinctUserIDs
}

func getChannelIDs(channelIDToMaxes []ChannelIDToMax) []string {
	var channelIDs = make([]string, 0, len(channelIDToMaxes))
	for _, channelIDToMax := range channelIDToMaxes {
		channelIDs = append(channelIDs, channelIDToMax.ChannelID)
	}
	return channelIDs
}

func main() {
	pantheonClient := makePantheonClient()
	paydayClient := makePaydayClient()
	makoClient := makeMakoClient()

	progress := readResults("seed-resumable")

	// Create test leaderboard data in Pantheon
	progress.ChannelIdsToMax = ingestTestDataIntoPantheon(pantheonClient, progress)
	writeResults("seed stage 1", progress) // Cache intermediate output

	progress.BadgeTierConfigurations = ingestTestEmoteGroupsIntoPayday(paydayClient, progress)
	writeResults("seed stage 2", progress)

	progress.Emotes = ingestEmotesIntoMako(makoClient, progress)
	writeResults("seed stage 3", progress)

	progress.UserIDs = getUserIDsFromPantheon(pantheonClient, progress)
	writeResults("seed stage 4 (final)", progress)
	writeSeedData("UserIDs", progress.UserIDs)

	channelIDs := getChannelIDs(progress.ChannelIdsToMax)
	writeSeedData("ChannelIDs", channelIDs)
}

// Other util functions
func reportProgress(name string, progress, total int) {
	if rand.Intn(100) == 1 && progress > 0 {
		fmt.Printf("%s: %d of %d\n", name, progress, total)
	}
}

func writeResults(destFileName string, payload Progress) {
	payloadJSON, err := json.Marshal(&payload)
	if err != nil {
		panic(err)
	}

	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	dest := path.Join(dir, outDir, fmt.Sprintf("%s.json", destFileName))
	err = ioutil.WriteFile(dest, payloadJSON, 0666)
	if err != nil {
		panic(err)
	}

	dest = path.Join(dir, outDir, fmt.Sprintf("%s.json", "seed-resumable"))
	err = ioutil.WriteFile(dest, payloadJSON, 0666)
	if err != nil {
		panic(err)
	}
}

func writeSeedData(destFileName string, payload []string) {
	payloadJSON, err := json.Marshal(&payload)
	if err != nil {
		panic(err)
	}

	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	//place in the primary data folder, not /seed/data
	dest := path.Join(dir, "../", outDir, fmt.Sprintf("%s.json", destFileName))
	err = ioutil.WriteFile(dest, payloadJSON, 0666)
	if err != nil {
		panic(err)
	}
}

func readResults(destFileName string) Progress {
	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	dest := path.Join(dir, outDir, fmt.Sprintf("%s.json", destFileName))
	file, err := ioutil.ReadFile(dest)
	if err != nil {
		return Progress{}
	}

	progress := &Progress{}
	err = json.Unmarshal(file, progress)
	if err != nil {
		return Progress{}
	}

	fmt.Printf("loaded in progress state as %s\n", progress)
	return *progress
}

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}
