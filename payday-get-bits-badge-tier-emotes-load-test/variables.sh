#!/bin/bash

# This file exists to define some env vars for running your load test

# The AWS profile you're operating in
AWS_PROFILE=default

# Your LDAP username you use to login to EC2 hosts
USERNAME=grahaall

RUN_FLAG=getbitsbadgetieremotes

OUTPUT_DIR=loadtest-results