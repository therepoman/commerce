package api

import (
	"encoding/json"
	"fmt"
)

// data structures for modeling bits channel APIs

const (
	CustomCheermoteStatusEnabled   = "ENABLED"
	CustomCheermoteStatusDisabled  = "DISABLED"
	CustomCheermoteStatusModerated = "MODERATED"
)

const (
	DefaultPinTopCheers              = true
	DefaultPinRecentCheers           = true
	DefaultMinBits                   = int64(1)
	DefaultRecentCheerMin            = int64(1)
	DefaultRecentCheerTimeoutSeconds = int64(10)
	DefaultCustomCheermoteStatus     = CustomCheermoteStatusEnabled
	DefaultLeaderboardEnabled        = true
	DefaultCheerBombEventOptOut      = false
	DefaultOptedOutOfVoldemort       = false
)

type GetOptOutResponse struct {
	User        string `json:"user"`
	OptedOut    bool   `json:"optedOut"`
	Annotation  string `json:"annotation"`
	LastUpdated int64  `json:"lastUpdated"`
}

type GetOnboardedResponse struct {
	Onboarded   bool  `json:"onboarded"`
	LastUpdated int64 `json:"last_updated"`
}

type SetOptOutRequest struct {
	OptedOut   bool   `json:"optedOut"`
	Annotation string `json:"annotation"`
}

type GetMinBitsResponse struct {
	User        string `json:"user"`
	MinBits     int64  `json:"min_bits"`
	LastUpdated int64  `json:"last_updated"`
}

type SetMinBitsRequest struct {
	MinBits int64 `json:"min_bits"`
}

type ChannelEligibleResponse struct {
	Eligible              bool       `json:"eligible"`
	MinimumBits           int64      `json:"min_bits"`
	MinimumBitsEmote      int64      `json:"min_bits_emote"`
	EnabledBitsBadgeTiers []int64    `json:"enabled_bits_badge_tiers"`
	Hashtags              []*Hashtag `json:"hashtags"`
	PinTopCheers          bool       `json:"pin_top_cheers"`
	PinRecentCheers       bool       `json:"pin_recent_cheers"`
	RecentCheerMin        int64      `json:"recent_cheer_min"`
	RecentCheerTimeoutMs  int64      `json:"recent_cheer_timeout_ms"`
	Event                 string     `json:"event,omitempty"`
	HGCEnabled            bool       `json:"hgc_enabled,omitempty"`
	LeaderboardEnabled    bool       `json:"leaderboard_enabled"`
	LeaderboardTimePeriod string     `json:"leaderboard_time_period"`
	CheerBombEventOptOut  bool       `json:"cheer_bomb_event_opt_out"`
	OptedOutOfVoldemort   bool       `json:"opted_out_of_voldemort"`
	PollsEnabled          bool       `json:"polls_enabled"`
}

type UpdateSettingsRequest struct {
	MinBits                *int64  `json:"min_bits"`
	MinBitsEmote           *int64  `json:"min_bits_emote"`
	PinTopCheers           *bool   `json:"pin_top_cheers"`
	PinRecentCheers        *bool   `json:"pin_recent_cheers"`
	RecentCheerMin         *int64  `json:"recent_cheer_min"`
	RecentCheerTimeout     *int64  `json:"recent_cheer_timeout"`
	CustomCheermoteEnabled *bool   `json:"custom_cheermote_enabled"`
	LeaderboardEnabled     *bool   `json:"leaderboard_enabled"`
	LeaderboardTimePeriod  *string `json:"leaderboard_time_period"`
	CheerBombEventOptOut   *bool   `json:"cheer_bomb_event_opt_out"`
	Onboarded              *bool   `json:"onboarded"`
	OptedOutOfVoldemort    *bool   `json:"opted_out_of_voldemort"`
}

type GetSettingsResponse struct {
	MinBits                   int64  `json:"min_bits"`
	MinBitsEmote              int64  `json:"min_bits_emote"`
	EligibleForBitsProgram    bool   `json:"eligible_for_bits_program"`
	HasServicesRecord         bool   `json:"has_services_record"`
	Onboarded                 bool   `json:"onboarded"`
	AmendmentSigned           bool   `json:"amendment_signed"`
	PinTopCheers              bool   `json:"pin_top_cheers"`
	PinRecentCheers           bool   `json:"pin_recent_cheers"`
	RecentCheerMin            int64  `json:"recent_cheer_min"`
	RecentCheerTimeout        int64  `json:"recent_cheer_timeout"`
	CustomCheermoteEnabled    bool   `json:"custom_cheermote_enabled"`
	Prefix                    string `json:"prefix"`
	CanEditBadgeImages        bool   `json:"can_edit_badge_images"`
	ShouldSeeBitsNotification bool   `json:"should_see_bits_notification"`
	LeaderboardEnabled        bool   `json:"leaderboard_enabled"`
	LeaderboardTimePeriod     string `json:"leaderboard_time_period"`
	CheerBombEventOptOut      bool   `json:"cheer_bomb_event_opt_out"`
	OptedOutOfVoldemort       bool   `json:"opted_out_of_voldemort"`
}

func ConvertJsonToGetOptOutResponse(jsonResponse []byte) (*GetOptOutResponse, error) {
	var response GetOptOutResponse
	err := json.Unmarshal(jsonResponse, &response)
	if err != nil {
		return nil, fmt.Errorf("Could not convert json to GetOptOutResponse, %v", err)
	}
	return &response, nil
}

func ConvertJsonToGetMinBitsResponse(jsonResponse []byte) (*GetMinBitsResponse, error) {
	var response GetMinBitsResponse
	err := json.Unmarshal(jsonResponse, &response)
	if err != nil {
		return nil, fmt.Errorf("Could not convert json to GetMinBitsResponse, %v", err)
	}
	return &response, nil
}

func ConvertJsonToChannelEligibleResponse(jsonResponse []byte) (*ChannelEligibleResponse, error) {
	var response ChannelEligibleResponse
	err := json.Unmarshal(jsonResponse, &response)
	if err != nil {
		return nil, fmt.Errorf("Could not convert json to ChannelEligibleResponse, %v", err)
	}
	return &response, nil
}

func ConvertJsonToGetSettingsResponse(jsonResponse []byte) (*GetSettingsResponse, error) {
	var response GetSettingsResponse
	err := json.Unmarshal(jsonResponse, &response)
	if err != nil {
		return nil, fmt.Errorf("Could not convert json to GetSettingsResponse, %v", err)
	}
	return &response, nil
}
