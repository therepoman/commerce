package api

type Hashtag struct {
	Tag      string `json:"hashtag"`
	Selected bool   `json:"selected"`
	Total    int64  `json:"total"`
}

type GetHashtagsResponse struct {
	Hashtags []*Hashtag `json:"hashtags"`
}

type SetHashtagRequest struct {
	Hashtags []*Hashtag `json:"hashtags"`
}
