package api

import (
	"time"
)

// data structures for modeling bits chat badges APIs

type BadgeTier struct {
	ChannelId                    string                         `json:"channelId"`
	Threshold                    int64                          `json:"threshold"`
	Enabled                      bool                           `json:"enabled"`
	Title                        string                         `json:"title"`
	ImageURL1x                   string                         `json:"image_url_1x"`
	ImageURL2x                   string                         `json:"image_url_2x"`
	ImageURL4x                   string                         `json:"image_url_4x"`
	UnlockedUsersCount           int64                          `json:"unlocked_users_count"`
	Emoticons                    *[]Emoticon                    `json:"emoticons,omitempty"`
	EmoticonUploadConfigurations *[]EmoticonUploadConfiguration `json:"emoticon_data,omitempty"`
	LastUpdated                  *time.Time                     `json:"last_updated"`
	CanUploadEmotesForTier       bool                           `json:"can_upload_emotes_for_tier"`
}

type Emoticon struct {
	Code      string `json:"code"`
	Suffix    string `json:"suffix"`
	EmoteID   string `json:"emote_id"`
	GroupID   string `json:"group_id"`
	OwnerID   string `json:"owner_id"`
	State     string `json:"state"`
	StateEnum int32  `json:"state_enum"`
}

type EmoticonUploadConfiguration struct {
	Threshold           int64             `json:"threshold"`
	Code                string            `json:"code"`
	CodeSuffix          string            `json:"code_suffix"`
	ImageUploadConfig1X ImageUploadConfig `json:"image_upload_config_1x"`
	ImageUploadConfig2X ImageUploadConfig `json:"image_upload_config_2x"`
	ImageUploadConfig4X ImageUploadConfig `json:"image_upload_config_4x"`
	GroupID             string            `json:"group_id"`
	OwnerID             string            `json:"owner_id"`
}

// IDs are obtained from upload service during the image upload process, before this
type ImageUploadConfig struct {
	UploadID         string `json:"upload_id"`
	EmoteSignedS3URL string `json:"emote_signed_s3_url"`
	ImageID          string `json:"image_id"`
	EmoteImageURL    string `json:"emote_image_url"`
}

type BadgeTierSetting struct {
	Threshold        int64              `json:"threshold"`
	DeleteImage      *bool              `json:"delete_image"`
	DeleteTitle      *bool              `json:"delete_title"`
	ImageData1x      *string            `json:"image_data_1x"`
	ImageData2x      *string            `json:"image_data_2x"`
	ImageData4x      *string            `json:"image_data_4x"`
	Enabled          *bool              `json:"enabled"`
	Title            *string            `json:"title"`
	EmoticonSettings *[]EmoticonSetting `json:"emoticons"`
}

type EmoticonSetting struct {
	Code           string  `json:"code"`
	CodeSuffix     *string `json:"code_suffix"`
	DeleteEmoticon *bool   `json:"delete_emoticon"`
	EmoteID        *string `json:"emote_id"`
}

type EmoticonBackfillSNSMessage struct {
	Threshold int64     `json:"threshold"`
	GroupID   string    `json:"group_id"`
	ChannelID string    `json:"channel_id"`
	UserIDs   *[]string `json:"user_ids"`
}

type GetBadgesResponse struct {
	Tiers                    []*BadgeTier `json:"tiers"`
	CanUploadBadgeTierEmotes *bool        `json:"bool,omitempty"`
}

type SetBadgesRequest struct {
	Tiers []*BadgeTierSetting `json:"tiers"`
}

type SetBadgeTiersAdminRequest struct {
	Tiers []*BadgeTierSetting `json:"tiers"`
}
