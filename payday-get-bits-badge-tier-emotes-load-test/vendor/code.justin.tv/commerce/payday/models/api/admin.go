package api

type AddBitsRequest struct {
	AmountOfBitsToAdd   int32
	TypeOfBitsToAdd     string
	EventId             string
	TwitchUserId        string
	AdminUserId         string
	AdminReason         string
	EventReasonCode     string
	NotifyUser          bool
	Origin              string
	BulkGrantCampaignID string
}

type RemoveBitsRequest struct {
	AmountOfBitsToRemove int32
	EventId              string
	TwitchUserId         string
	AdminUserId          string
	AdminReason          string
	EventReasonCode      string
}

type OnboardUserRequest struct {
	PayoutId  string `json:"payout_id"`
	Onboarded bool   `json:"onboarded"`
}
