package api

import (
	"encoding/json"
	"fmt"
)

// data structures for modeling bits balance APIs

type GetBalanceResponse struct {
	Balance                       int32           `json:"balance"`
	Bundles                       map[string]bool `json:"bundles"`
	ChannelTotal                  *int32          `json:"channel_total,omitempty"`
	InventoryLimit                int32           `json:"inventory_limit"`
	HighestEntitledBadge          int64           `json:"highest_entitled_badge"`
	MostRecentPurchaseWebPlatform string          `json:"most_recent_purchase_web_platform"`
}

func ConvertJsonToGetBalanceResponse(jsonResponse []byte) (*GetBalanceResponse, error) {
	var response GetBalanceResponse
	err := json.Unmarshal(jsonResponse, &response)
	if err != nil {
		return nil, fmt.Errorf("Could not convert json to GetBalanceResponse, %v", err)
	}
	return &response, nil
}
