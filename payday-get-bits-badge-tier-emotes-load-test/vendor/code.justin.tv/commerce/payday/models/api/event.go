package api

import (
	"encoding/json"
	"fmt"
	"time"

	zumaapi "code.justin.tv/chat/zuma/app/api"
)

type CreateEventRequest struct {
	UserId            string `json:"user_id"`
	ChannelId         string `json:"channel_id"`
	Amount            int32  `json:"amount"`
	Message           string `json:"message"`
	EventId           string `json:"event_id"`
	RoomID            string `json:"room_id"`
	ShouldCheerAnyway bool   `json:"should_cheer_anyway"`
	IsAutoModEnabled  bool   `json:"is_automod_enabled"`
	IPAddress         string `json:"ip_addresss"`
	IsAnonymous       bool   `json:"is_anonymous"`
	IsLegacy          bool
	ClientID          string `json:"client_id"`
	CountryCode       string `json:"country_code"`
	City              string `json:"city"`
	PostalCode        string `json:"postal_code"`
}

type CreateEventResponse struct {
	Balance int32  `json:"balance"`
	EventId string `json:"event_id"`
}

type CreateEventErrorResponse struct {
	Status  CreateEventErrorStatus `json:"status"`
	Message string                 `json:"message"`
	Data    CreateEventErrorData   `json:"data"`
}

type CreateEventErrorStatus string

const (
	InvalidEventId               CreateEventErrorStatus = "invalid_event_id"
	TooLargeBitsEmote            CreateEventErrorStatus = "bits_emote_too_large"
	TooLargeCheer                CreateEventErrorStatus = "bits_cheer_too_large"
	EmoteAmountBelowMinBits      CreateEventErrorStatus = "below_min_bits_emote"
	InvalidBitsMessage           CreateEventErrorStatus = "invalid_bits_message"
	MessageLengthExceeded        CreateEventErrorStatus = "message_length_exceeded"
	InvalidBitsAmount            CreateEventErrorStatus = "invalid_bits_amount"
	AmountBelowMinBits           CreateEventErrorStatus = "below_min_bits"
	RequestThrottled             CreateEventErrorStatus = "request_throttled"
	InsufficientBalance          CreateEventErrorStatus = "insufficient_balance"
	ZalgoMessage                 CreateEventErrorStatus = "zalgo_message"
	AutoModMessage               CreateEventErrorStatus = "automod_message"
	ChannelIneligible            CreateEventErrorStatus = "channel_ineligible"
	UserIneligible               CreateEventErrorStatus = "user_ineligible"
	UserBanned                   CreateEventErrorStatus = "user_banned"
	UserSuspended                CreateEventErrorStatus = "user_suspended"
	AutoModPending               CreateEventErrorStatus = "automod_pending_cheer"
	ChannelBlockedTerms          CreateEventErrorStatus = "channel_blocked_terms"
	AnonymousCheermoteNotAllowed CreateEventErrorStatus = "anonymous_cheermote_not_allowed"
)

type CreateEventErrorData struct {
	Automod                *zumaapi.AutoModResponse        `json:"automod,omitempty"`
	EnforceMessageResponse *zumaapi.EnforceMessageResponse `json:"zuma,omitempty"`
}

type Event struct {
	Timestamp time.Time `json:"time"`
	UserId    string    `json:"user_id"`
	Total     int64     `json:"total"`
	ChannelId string    `json:"channel_id"`
	EventId   string    `json:"event_id"`
}

type UserEventsResponse struct {
	Events []Event `json:"events"`
	Cursor string  `json:"cursor"`
}

type AdminEvent struct {
	Timestamp    time.Time `json:"time"`
	UserId       string    `json:"user_id"`
	Total        int64     `json:"total"`
	ChannelId    string    `json:"channel_id"`
	EventId      string    `json:"event_id"`
	AdminId      string    `json:"admin_id"`
	AdminReason  string    `json:"admin_reason"`
	ExtensionID  string    `json:"extension_id"`
	ExtensionSKU string    `json:"extension_sku"`
}

type AdminUserEventsResponse struct {
	Events []AdminEvent `json:"events"`
	Cursor string       `json:"cursor"`
}

func ConvertJsonToCreateEventResponse(jsonResponse []byte) (*CreateEventResponse, error) {
	var response CreateEventResponse
	err := json.Unmarshal(jsonResponse, &response)
	if err != nil {
		return nil, fmt.Errorf("Could not convert json to CreateEventResponse, %v", err)
	}
	return &response, nil
}

func ConvertJsonToCreateEventErrorResponse(jsonResponse []byte) (*CreateEventErrorResponse, error) {
	var response CreateEventErrorResponse
	err := json.Unmarshal(jsonResponse, &response)
	if err != nil {
		return nil, fmt.Errorf("Could not convert json to CreateEventErrorResponse, %v", err)
	}
	return &response, nil
}

func ConvertJsonToUserEventsResponse(jsonResponse []byte) (*UserEventsResponse, error) {
	var response UserEventsResponse
	err := json.Unmarshal(jsonResponse, &response)
	if err != nil {
		return nil, fmt.Errorf("Could not convert json to UserEventsResponse, %v", err)
	}
	return &response, nil
}

func ConvertJsonToAdminUserEventsResponse(jsonResponse []byte) (*AdminUserEventsResponse, error) {
	var response AdminUserEventsResponse
	err := json.Unmarshal(jsonResponse, &response)
	if err != nil {
		return nil, fmt.Errorf("Could not convert json to AdminUserEventsResponse, %v", err)
	}
	return &response, nil
}
