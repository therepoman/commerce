package api

import (
	"errors"
	"fmt"
	"strings"
	"time"
)

const (
	maxBrowseLimit  = 100
	maxBrowseOffset = 10000

	ErrCodeInvalidCursor = "invalid_cursor"
)

// GetModRequest is the body for the POST /v1/mods/get endpoint
type GetModRequest struct {
	ChannelID string `json:"channel_id"`
	UserID    string `json:"user_id"`
}

// GetModResponse is the response from the POST /v1/mods/get endpoint
type GetModResponse struct {
	IsMod     bool       `json:"is_mod"`
	CreatedAt *time.Time `json:"created_at"`
}

// ListModsRequest is the body for the POST /v1/mods/list endpoint
type ListModsRequest struct {
	ChannelID string `json:"channel_id"`
}

// ListModsResponse is the response from the POST /v1/mods/list endpoint
type ListModsResponse struct {
	Mods []string `json:"mods"`
}

// ListModsV2Request is the body for the POST /v2/mods/list endpoint
type ListModsV2Request struct {
	ChannelID string `json:"channel_id"`
	Cursor    string `json:"cursor"`
	Limit     int    `json:"limit"`
}

// ListModsV2Response is the response from the POST /v2/mods/list endpoint
type ListModsV2Response struct {
	ModResults []ModResult `json:"mod_results"`
}

// ModResult an element in a paginated list of moderators
type ModResult struct {
	Mod    Mod    `json:"mod"`
	Cursor string `json:"cursor"`
}

// Mod is a moderator in a list of mods
type Mod struct {
	UserID    string     `json:"user_id"`
	CreatedAt *time.Time `json:"created_at"`
}

// AddModRequest is the body for the POST /v1/mods/add endpoint.
type AddModRequest struct {
	ChannelID        string `json:"channel_id"`
	TargetUserID     string `json:"target_user_id"`
	RequestingUserID string `json:"requesting_user_id"`
}

// AddModResponse is the response for the POST /v1/mods/add endpoint.
type AddModResponse struct {
	// ErrorCode is an enum that describes the type of logical error that occurred.
	// Nil if the operation was successful.
	ErrorCode *string `json:"error_code,omitempty"`
}

// RemoveModRequest is the body for the POST /v1/mods/remove endpoint
type RemoveModRequest struct {
	ChannelID        string `json:"channel_id"`
	TargetUserID     string `json:"target_user_id"`
	RequestingUserID string `json:"requesting_user_id"`
}

// RemoveModResponse is the response for the POST /v1/mods/remove endpoint.
type RemoveModResponse AddModResponse

// ListGameFollowsRequest is the body for the POST /v1/game_follows/list and POST /v1/game_follows/list_live endpoints
type ListGameFollowsRequest struct {
	UserID       string `json:"user_id"`
	Offset       int    `json:"offset"`
	Limit        int    `json:"limit"`
	Cursor       string `json:"cursor"`
	ShowOnlyLive bool   `json:"show_only_live"`

	parsedCursor *int
}

func (req ListGameFollowsRequest) SetParsedCursor(parsedCursor int) {
	req.parsedCursor = &parsedCursor
}

func (req ListGameFollowsRequest) GetParsedCursor() int {
	return *req.parsedCursor
}

// ListGameFollowsResponse is the response from the POST /v1/game_follows/list endpoint
type ListGameFollowsResponse struct {
	Games  []GameFollow `json:"games"`
	Cursor string       `json:"cursor"`
}

// CountGameFollowsRequest is the body for the POST /v1/game_follows/count endpoint
type CountGameFollowsRequest struct {
	UserID       string `json:"user_id"`
	ShowOnlyLive bool   `json:"show_only_live"`
}

// CountGameFollowsResponse is the response from the POST /v1/game_follows/count endpoint
type CountGameFollowsResponse struct {
	Total int `json:"total"`
}

// GameFollowRequest is the body for the POST /v1/game_follows/get, /v1/game_follows/create, and /v1/game_follows/delete endpoints
type GameFollowRequest struct {
	UserID string `json:"user_id"`
	GameID string `json:"game_id"`
}

// GameFollowResponse is the response from the POST /v1/game_follows/get, /v1/game_follows/create, and /v1/game_follows/delete endpoints
type GameFollowResponse struct {
	Game *GameFollow `json:"game"`
}

// GameFollow is the inner struct of the POST /v1/game_follows/list endpoint
type GameFollow struct {
	UserID    string    `json:"user_id"`
	GameID    string    `json:"game_id"`
	CreatedAt time.Time `json:"created_at"`
}

// CountGameFollowersRequest is the body for the POST /v1/game_followers/count endpoint
type CountGameFollowersRequest struct {
	GameID string `json:"game_id"`
}

// CountGameFollowersResponse is the response from the POST /v1/game_followers/count endpoint
type CountGameFollowersResponse struct {
	Total int `json:"total"`
}

// ListGameFollowsLiveRequest is the body for the POST /v1/game_follows/live endpoint
type ListGameFollowsLiveRequest struct {
	UserID string `json:"user_id"`
	Limit  int    `json:"limit"`
	Offset int    `json:"offset"`
}

// ListGameFollowsLiveResponse is the response from the POST /v1/game_follows/live endpoint
type ListGameFollowsLiveResponse struct {
	Games []GameFollowLive `json:"games"`
	Total int              `json:"total"`
}

// GameFollowLive is the inner struct of the POST /v1/game_follows/live endpoint
type GameFollowLive struct {
	GameID    string    `json:"game_id"`
	CreatedAt time.Time `json:"created_at"`
	Channels  int       `json:"channels"`
	Viewers   int       `json:"viewers"`
}

const (
	// Official community types
	CommunityTypeGame     = "game"
	CommunityTypeCreative = "creative"
	// Unofficial (user-created) community types
	CommunityTypeOther = "other"

	// Character used to separate the values for the Filter field in BrowseTop
	BrowseTopFilterSeparator = ":"
	// Values allowed for the Filter list
	BrowseTopFilterAll         = "all"
	BrowseTopFilterGames       = "games"
	BrowseTopFilterCommunities = "communities"
	BrowseTopFilterCreative    = "creative"
)

// ListUserBlocksParams is the body for the POST /v1/users/blocks/get endpoint
type ListUserBlocksParams struct {
	UserID string `json:"user_id"`
}

// ListUserBlocksResponse is the response for POST /v1/users/blocks/get endpoint
type ListUserBlocksResponse struct {
	BlockedUserIDs []string `json:"blocked_user_ids"`
}

// ListUserBlockersParams is the body for the POST /v1/users/blocks/blockers endpoint
type ListUserBlockersParams struct {
	UserID string `json:"user_id"`
}

// ListUserBlockersResponse is the response for POST /v1/users/blocks/blockers endpoint
type ListUserBlockersResponse struct {
	BlockerUserIDs []string `json:"blocker_user_ids"`
}

// AddUserBlockParams is the body for the POST /v1/users/blocks/add endpoint
type AddUserBlockParams struct {
	UserID       string `json:"user_id"`
	TargetUserID string `json:"target_user_id"`
	Reason       string `json:"reason"`
	// SourceContext identifies what user flow initiated the block (e.g. chat, whisper).
	SourceContext string `json:"source_context"`
}

// AddUserBlockResponse is the response for the POST /v1/users/blocks/add endpoint
type AddUserBlockResponse struct {
	Status  int    `json:"status"`
	Error   string `json:"error"`
	Message string `json:"message"`
}

// IsBlockedParams is the body for the POST /v1/users/blocks/is_blocked endpoint
type IsBlockedParams struct {
	UserID        string `json:"user_id"`
	BlockedUserID string `json:"blocked_user_id"`
}

// IsBlockedResponse is the response from POST /v1/user/blocks/is_blocked
type IsBlockedResponse struct {
	UserID        string `json:"user_id"`
	BlockedUserID string `json:"blocked_user_id"`
	IsBlocked     bool   `json:"is_blocked"`
}

// UserRoleRequest is the body for the POST /v1/users/roles/get endpoint
type UserRoleRequest struct {
	ChannelID string `json:"channel_id"`
	UserID    string `json:"user_id"`
}

// UserRoleResponse is the response from POST /v1/users/roles/get
type UserRoleResponse struct {
	Role string `json:"role"`
}

// Various roles for UserRoleResponse.Role:
const (
	UserRoleBroadcaster = "broadcaster"
	UserRoleModerator   = "mod"
	UserRoleVIP         = "vip"
)

// RemoveUserBlockParams is the body for the POST /v1/users/blocks/remove endpoint
type RemoveUserBlockParams struct {
	UserID       string `json:"user_id"`
	TargetUserID string `json:"target_user_id"`
}

// ParseMessageRequest is the body for the POST /v1/messages/parse endpoint
type ParseMessageRequest struct {
	ChannelID string `json:"channel_id"`
	UserID    string `json:"user_id"`
	Body      string `json:"body"`
}

// ParseMessageResponse is the response of the POST /v1/messages/parse endpoint
type ParseMessageResponse struct {
	Status      ParseMessageStatus  `json:"status"`
	Message     *ParseMessageResult `json:"message,omitempty"`
	BannedUntil *time.Time          `json:"banned_until,omitempty"`
}

// ParseMessageResult is the inner JSON for the parse message endpoint
type ParseMessageResult struct {
	Body            string                      `json:"body"`
	Emoticons       []ParseMessageEmoticonRange `json:"emoticons"`
	UserBadges      []ParseMessageUserBadge     `json:"user_badges"`
	UserColor       string                      `json:"user_color"`
	UserLogin       string                      `json:"user_login"`
	UserDisplayName string                      `json:"user_display_name"`
}

// ParseMessageEmoticonRange defines the JSON structure for an emoticon range
type ParseMessageEmoticonRange struct {
	ParseMessageBodyRange
	ID string `json:"id"`
}

// ParseMessageBodyRange defines the JSON structure for an arbitrary range
type ParseMessageBodyRange struct {
	Begin int `json:"begin"`
	End   int `json:"end"`
}

type ParseMessageUserBadge struct {
	ID      string `json:"id"`
	Version string `json:"version"`
}

type ParseMessageStatus string

const (
	ParseMessageStatusUserBanned      = "user_banned"
	ParseMessageStatusUserNotVerified = "user_not_verified"
	ParseMessageStatusReviewRequired  = "review_required"
	ParseMessageStatusOK              = "ok"
)

// CheckEmoticonsAccessRequest is the body for the POST /v1/users/emoticons/check_access endpoint
type CheckEmoticonsAccessRequest struct {
	UserID      string   `json:"user_id"`
	EmoticonIDs []string `json:"emoticon_ids"`
}

// CheckEmoticonsAccessResponse is the response for the POST /v1/users/emoticons/check_access endpoint
type CheckEmoticonsAccessResponse struct {
	UserEmoticonSetIDs []string `json:"user_emoticon_set_ids"`
	NoAccessIDs        []string `json:"no_access_ids"`
}

// GetEmoticonIDsRequest is the body for the POST /v1/users/emoticons/get_ids endpoint
type GetEmoticonIDsRequest struct{}

// GetEmoticonIDsResponse is the response for the POST /v1/users/emoticons/get_ids endpoint
type GetEmoticonIDsResponse struct {
	EmoticonIDs []string `json:"emoticon_ids"`
}

type BrowseSortType string

const (
	BrowseSortTypeViewers   BrowseSortType = "viewers"
	BrowseSortTypeRelevancy BrowseSortType = "relevancy"
)

// GetTopBrowseParams is the body for the GET /v1/browse/top endpoint
type GetTopBrowseParams struct {
	SortBy BrowseSortType `json:"sort_by"`
	Filter string         `json:"filter"`
	Cursor string         `json:"cursor"`
	Limit  int            `json:"limit"`

	parsedCursor *int
}

func (p *GetTopBrowseParams) Validate() error {
	if p.SortBy != "" &&
		p.SortBy != BrowseSortTypeViewers &&
		p.SortBy != BrowseSortTypeRelevancy {
		return errors.New("invalid sort_by value")
	}

	if p.Filter != "" && p.Filter != "all" {
		for _, filter := range strings.Split(p.Filter, BrowseTopFilterSeparator) {
			switch filter {
			case BrowseTopFilterGames, BrowseTopFilterCommunities, BrowseTopFilterCreative:
				continue
			default:
				return errors.New("invalid filter value (expected 'games', 'communities' or 'creative', separated by ':')")
			}
		}
	}

	if p.Limit < 0 || p.Limit > maxBrowseLimit {
		return fmt.Errorf("limit must be between 0 and %d", maxBrowseLimit)
	}

	offset, err := p.GetCursor()
	if err != nil {
		return err
	}
	if offset < 0 || offset > maxBrowseOffset {
		return errors.New("invalid cursor")
	}

	return nil
}

func (p *GetTopBrowseParams) GetCursor() (int, error) {
	if p.parsedCursor != nil {
		return *p.parsedCursor, nil
	}
	offset, err := CursorToInt(p.Cursor)
	if err != nil {
		return 0, err
	}
	p.parsedCursor = &offset
	return *p.parsedCursor, err
}

// GetTopBrowseResponse is the response for the GET /v1/browse/top endpoint
type GetTopBrowseResponse struct {
	Total  int           `json:"_total"`
	Cursor string        `json:"_cursor"`
	Top    []BrowseEntry `json:"top"`
}

// BrowseEntry is the inner struct for GetTopBrowseResponse, it's a unified
// type with all the info that the frontend will need to display this category
// on a browse page. Used for top games.
type BrowseEntry struct {
	ID       string `json:"_id"`
	Type     string `json:"type"`
	Channels int    `json:"channels"`
	Viewers  int    `json:"viewers"`
}

// AutoModCheckUsernameRequest is the body for the POST /v1/entities/username/check
type AutoModCheckUsernameRequest struct {
	Username                    string  `json:"username"`
	ChannelID                   string  `json:"channel_id"`
	OverrideAutoModLevel        *int    `json:"override_automod_level"`
	OverrideAutoModLanguageCode *string `json:"override_automod_language_code"`
	DeviceID                    string  `json:"device_id"`
}

// AutoModCheckUsernameResponse is the response from POST /v1/entities/username/check
type AutomodCheckUsernameResponse struct {
	IsOK bool `json:"is_ok"`
}

// GetVIPRequest is the body for the POST /v1/vips/get endpoint
type GetVIPRequest struct {
	ChannelID string `json:"channel_id"`
	UserID    string `json:"user_id"`
}

// GetVIPResponse is the response from the POST /v1/vips/get endpoint
type GetVIPResponse struct {
	IsVIP     bool       `json:"is_vip"`
	GrantedAt *time.Time `json:"granted_at"`
}

// ListVIPsRequest is the body for the POST /v1/vips/list endpoint
type ListVIPsRequest struct {
	ChannelID string `json:"channel_id"`
	Cursor    string `json:"cursor"`
	Limit     int    `json:"limit"`
}

// ListVIPsResponse is the response from the POST /v1/vips/list endpoint
type ListVIPsResponse struct {
	VIPs []VIPResult `json:"vips"`
}

// VIPResult an element in a paginated list of vips
type VIPResult struct {
	VIP    VIP    `json:"vip"`
	Cursor string `json:"cursor"`
}

// VIP element with UserID for list of vips.
type VIP struct {
	UserID    string     `json:"user_id"`
	GrantedAt *time.Time `json:"granted_at"`
}

// AddVIPRequest is the body for the POST /v1/vips/add endpoint.
type AddVIPRequest struct {
	ChannelID        string `json:"channel_id"`
	TargetUserID     string `json:"target_user_id"`
	RequestingUserID string `json:"requesting_user_id"`
}

// AddVIPResponse is the response for the POST /v1/vips/add endpoint.
type AddVIPResponse struct {
	// ErrorCode is an enum that describes the type of logical error that occurred.
	// Nil if the operation was successful.
	ErrorCode *string `json:"error_code,omitempty"`
}

// RemoveVIPRequest is the body for the POST /v1/vips/remove endpoint.
type RemoveVIPRequest struct {
	ChannelID        string `json:"channel_id"`
	TargetUserID     string `json:"target_user_id"`
	RequestingUserID string `json:"requesting_user_id"`
}

// RemoveVIPResponse is the response for the POST /v1/vips/remove endpoint.
type RemoveVIPResponse AddVIPResponse
