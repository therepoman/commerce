package api

import "time"

// ChatRoomPropertiesRequest is the request sent to /v1/channels/get
type ChatRoomPropertiesRequest struct {
	ChannelID string `json:"channel_id" validate:"nonzero"`
}

// ChatRoomPropertiesResponse is the response /v1/channels/get
type ChatRoomPropertiesResponse struct {
	ChannelID string `json:"channel_id"`

	BroadcasterLanguageMode    bool                     `json:"match_broadcaster_language_required"`
	ChatFastsubs               bool                     `json:"fastsubs_enabled"`
	ChatRequireVerifiedAccount bool                     `json:"verified_only"`
	ChatRules                  []string                 `json:"chat_rules"`
	FacebookConnectModerated   bool                     `json:"facebook_connected_only"`
	GlobalBannedWordsOptout    bool                     `json:"globally_banned_words_opt_out"`
	HideChatLinks              bool                     `json:"hide_links"`
	R9kOnlyChat                bool                     `json:"r9k_enabled"`
	SubscribersOnlyChat        bool                     `json:"subs_only"`
	FollowersOnlyEnabled       bool                     `json:"followers_only_enabled"`
	FollowersOnlyDuration      time.Duration            `json:"followers_only_duration"`
	EmotesOnly                 bool                     `json:"emotes_only"`
	RitualsEnabled             bool                     `json:"rituals_enabled"`
	RitualsWhitelisted         bool                     `json:"rituals_whitelisted"`
	AutoModProperties          ChannelAutoModProperties `json:"automod_properties"`
}

// ChannelAutoModProperties stores the channel's AutoMod settings.
type ChannelAutoModProperties struct {
	AggressiveChannelLevel *int64 `json:"aggressive_channel_level,omitempty"`
	SexualChannelLevel     *int64 `json:"sexual_channel_level,omitempty"`
	ProfanityChannelLevel  *int64 `json:"profanity_channel_level,omitempty"`
	IdentityChannelLevel   *int64 `json:"identity_channel_level,omitempty"`
}
