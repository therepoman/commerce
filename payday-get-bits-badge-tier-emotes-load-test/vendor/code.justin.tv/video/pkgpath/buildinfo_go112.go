// +build go1.12

package pkgpath

import "runtime/debug"

func buildInfoMainPath() string {
	bi, ok := debug.ReadBuildInfo()
	// With go build path/*.go, Path is set to "command-line-arguments"
	if ok && bi.Path != "command-line-arguments" {
		return bi.Path
	}
	return ""
}
