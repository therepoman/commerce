package scopes

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

type scopeList struct {
	Scopes []scope `json:"scopes" validate:"nonzero"`
}

type scope struct {
	Name         string   `json:"name" validate:"nonzero"`
	Capabilities []string `json:"capabilities" validate:"nonzero"`
}

type capabilityList struct {
	Capabilities []Capability `json:"capabilities"`
}

// Capability defines the struct to represent an individual capability
// listed in a configuration file: "/{service}/capabilities.json"
type Capability struct {
	Name                   string                 `json:"name" validate:"nonzero"`
	AuthenticationMethods  []AuthenticationMethod `json:"authenticationMethods"`
	AuthorizationParams    []AuthorizationParam   `json:"authorizationParams"`
	AuthorizationFunctions []string               `json:"authorizationFunctions"`
	CapabilityParams       []CapabilityParam      `json:"capabilityParams"`
	Expiration             int                    `json:"expiration" validate:"nonzero,max=6000"`
	Audience               string                 `json:"audience" validate:"nonzero,regexp=^code.justin.tv/[A-Za-z0-9-_]+/[A-Za-z0-9-_]+$"`
}

// AuthenticationMethod defines the struct to represent an individual
// authentication method along with any related scopes
type AuthenticationMethod struct {
	Method string   `json:"method" validate:"nonzero,regexp=^(oauth|rails)$"`
	Scopes []string `json:"scopes,omitempty"`
}

// AuthorizationParam defines the struct to represent an individual
// parameter needed for an authorization function
type AuthorizationParam struct {
	Name string `json:"name"`
	Type string `json:"type"`
}

// CapabilityParam defines the struct to represent an expected
// parameter in the returned authorization token
type CapabilityParam struct {
	Name      string `json:"name"`
	Type      string `json:"type"`
	MaxLength int    `json:"maxLength"`
}

// CapabilityMap defines the struct for mapping capability name to its
// respective details
type CapabilityMap map[string]Capability

// ScopeMap defines the struct for mapping scope name to a mapping of
// capability name to the respective capability's details
//   scopeName -> {
//     capName -> capDetails,
//     otherCap -> otherCapDetails,
//     ...
//   }
// A ScopeMap is used to determine which capabilities are required to
// perform an action defined by a scope
type ScopeMap map[string]*CapabilityMap

// Error message for an undefined scope
var ErrUndefinedScope = errors.New("Undefined request scope")

// Error message for an undefined capability
var ErrUndefinedCapability = errors.New("Undefined scope capability")

// Delimiter used to separate a service from a capability name owned by the
// respective service
var SvcCapDelimiter = "::"

// ParseScopes looks at the target path for service name directories and reads
// all configuration files (scopes and capabilities) in the each directory to
// create a ScopeMap of all found scopes and capabilities
func ParseScopes(repoPath string) *ScopeMap {
	capDir := fmt.Sprint(repoPath, "/capabilities/")
	scopeMapping := make(ScopeMap)
	allCapabilities := make(CapabilityMap)

	configuredSvcs, err := ioutil.ReadDir(capDir)
	if err != nil {
		panic(err)
	}

	for _, capabilityFile := range configuredSvcs {
		svcName := strings.Split(capabilityFile.Name(), ".")[0]
		rawCapabilities, err := ioutil.ReadFile(fmt.Sprint(capDir, svcName, ".json"))
		if err != nil {
			panic(err)
		}

		var svcCapabilities capabilityList
		json.Unmarshal(rawCapabilities, &svcCapabilities)

		for _, capability := range svcCapabilities.Capabilities {
			capPath := fmt.Sprint(svcName, SvcCapDelimiter, capability.Name)
			allCapabilities[capPath] = capability
		}
	}

	rawScopes, err := ioutil.ReadFile(fmt.Sprint(repoPath, "/scopes.json"))
	if err != nil {
		panic(err)
	}

	var svcScopes scopeList
	json.Unmarshal(rawScopes, &svcScopes)

	for _, scope := range svcScopes.Scopes {
		capabilityMap := make(CapabilityMap)
		for _, capability := range scope.Capabilities {
			capabilityMap[capability] = allCapabilities[capability]
		}
		scopeMapping[scope.Name] = &capabilityMap
	}
	return &scopeMapping
}

var errUnmatchedCapability = errors.New("Scope specifies unknown capability")

// ParseAndValidate performs the same tasks as ParseScopes except it returns
// errors for any failed content validations
func ParseAndValidate(repoPath string) (*ScopeMap, error) {
	scopeMapping := make(ScopeMap)
	allCapabilities := make(CapabilityMap)

	for serviceName, serviceCapabilityFile := range findCapabilityConfigs(repoPath) {
		var serviceCapabilityList capabilityList
		err := ReadAndValidateConfig(&serviceCapabilityList, serviceCapabilityFile)
		if err != nil {
			log.Fatal(err)
		}

		for _, capability := range serviceCapabilityList.Capabilities {
			err = configValidator.Validate(capability)
			if err != nil {
				return &scopeMapping, capabilityValidateError(capability, err)
			}

			capabilityRef := fmt.Sprint(serviceName, SvcCapDelimiter, capability.Name)
			allCapabilities[capabilityRef] = capability
		}
	}

	var svcScopes scopeList
	err := ReadAndValidateConfig(&svcScopes, findScopeConfig(repoPath))
	if err != nil {
		log.Fatal(err)
	}

	for _, scope := range svcScopes.Scopes {
		err = configValidator.Validate(scope)
		if err != nil {
			return &scopeMapping, scopeValidateError(scope, err)
		}

		capabilityMap := make(CapabilityMap)
		for _, capabilityRef := range scope.Capabilities {
			matchedCapability := allCapabilities[capabilityRef]
			if matchedCapability.Name == "" {
				return &scopeMapping, scopeValidateError(scope, errUnmatchedCapability)
			}
			capabilityMap[capabilityRef] = allCapabilities[capabilityRef]
		}
		scopeMapping[scope.Name] = &capabilityMap
	}

	return &scopeMapping, nil
}

func scopeValidateError(scope scope, validateError error) error {
	return fmt.Errorf("Scope: \"%s\": \"%s\"", scope.Name, validateError)
}

func capabilityValidateError(capability Capability, validateError error) error {
	return fmt.Errorf("Capability: \"%s\", %s", capability.Name, validateError)
}

func findCapabilityConfigs(rootPath string) map[string]string {
	capabilityDir := fmt.Sprint(rootPath, "/capabilities/")
	availableServices, err := ioutil.ReadDir(capabilityDir)
	if err != nil {
		panic(err)
	}

	capabilityConfigs := make(map[string]string, len(availableServices))
	for _, serviceConfig := range availableServices {
		serviceName := strings.Split(serviceConfig.Name(), ".")[0]
		capabilityConfigs[serviceName] = fmt.Sprint(capabilityDir, serviceName, ".json")
	}

	return capabilityConfigs
}

func findScopeConfig(rootPath string) string {
	return fmt.Sprint(rootPath, "/scopes.json")
}

// GetCapabilitiesFromScope retrieve a map of capabilities from a ScopeMap given
// the name of a scope
func (s *ScopeMap) GetCapabilitiesFromScope(scopeName string) (*CapabilityMap, error) {
	capMap := (*s)[scopeName]
	if capMap == nil {
		return nil, ErrUndefinedScope
	}

	return capMap, nil
}

// GetCapabilityFromScope retrieve capability details from a ScopeMap given the
// name of the scope and the name of the capability
func (s *ScopeMap) GetCapabilityFromScope(scopeName string, capName string) (Capability, error) {
	capMap := (*s)[scopeName]
	if capMap == nil {
		return Capability{}, ErrUndefinedScope
	}

	capability := (*capMap)[capName]
	if capability.Name == "" {
		return Capability{}, ErrUndefinedCapability
	}

	return capability, nil
}
