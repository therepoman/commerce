package scopes

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"reflect"
	"strings"

	"gopkg.in/validator.v2"
)

var configValidator = validator.NewValidator()

// Validate inspects route, scope, and capability definitions in the given path
// and logs any failed validations defined by the `validate` tags set on the
// struct definitions
func Validate(repoPath string, debug bool) {
	err := validateRoutes(fmt.Sprint(repoPath, "routes.json"))
	if err != nil {
		log.Fatalln(err)
	}

	parsed, err := ParseAndValidate(repoPath)
	if err != nil {
		log.Fatalln(err)
	}
	if debug {
		for scopeName, scopeCaps := range *parsed {
			log.Println(fmt.Sprintf("%s: %v", scopeName, scopeCaps))
		}
	}
	return
}

// ReadAndValidateConfig opens the specified file to parse a configuration
// (route, scope, or capability) and validates its values based on previously
// specified requirements
func ReadAndValidateConfig(conf interface{}, filename string) error {
	f, err := os.Open(filename)
	if err != nil {
		return err
	}

	d := json.NewDecoder(f)
	err = d.Decode(conf)
	if err != nil {
		return err
	}

	return configValidator.Validate(conf)
}

// Routes validation

type routesConfig struct {
	Routes map[string]route `json:"routes" validate:"nonzero"`
}
type route struct {
	Backend         string                  `json:"backend" validate:"nonzero"`
	TargetPath      string                  `json:"targetPath" validate:"nonzero"`
	Parameters      map[string]string       `json:"parameters"`
	PassCredentials []string                `json:"passCredentials"`
	Methods         map[string]*routeMethod `json:"methods" validate:"nonzero"`
}
type routeMethod struct {
	Scopes []string `json:"scopes" validate:"min=1"`
}

func validateRoutes(routesFile string) error {
	var conf routesConfig
	err := ReadAndValidateConfig(&conf, routesFile)
	if err != nil {
		return err
	}

	// validator does not validate json structs with dynamic keys
	// configValidator.SetValidationFunc("validHTTPMethods", validateHTTPMethods)
	for routePath, route := range conf.Routes {
		err = configValidator.Validate(route)
		if err != nil {
			return routeValidateError(routePath, err)
		}
	}

	return nil
}

func routeValidateError(routePath string, validateError error) error {
	return fmt.Errorf("Path: \"%s\", %s", routePath, validateError)
}

// Scopes and capabilities validation

// Custom validation methods

var validHTTPMethods = []string{"GET", "POST", "PUT", "DELETE"}

func validateHTTPMethods(v interface{}, param string) error {
	st := reflect.ValueOf(v)
	if st.Kind() != reflect.String {
		return validator.ErrUnsupported
	}
	for _, method := range strings.Split(st.String(), ",") {
		if !validHTTPMethod(method) {
			return fmt.Errorf("Unsupported route method: '%s'", method)
		}
	}

	return nil
}
func validHTTPMethod(method string) bool {
	for _, allowed := range validHTTPMethods {
		if method == allowed {
			return true
		}
	}
	return false
}
