# Functions

## Supported Functions
With respective owners

### Cartman

- `HasElevatedPrivileges`: Is the authenticated user a staff or admin?

### Spectre

- `CanEditChannel`: Has the authenticated user been granted `"editor"` privileges on the given channel?

### Mock

- `MockFail`: Always returns false
- `MockSuccess`: Always returns true
