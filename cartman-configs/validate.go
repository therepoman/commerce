package main

import (
	"os"

	"code.justin.tv/web/cartman-configs/scopes"
)

func main() {
	cmdArg := os.Args[1:]

	var debug bool
	if len(cmdArg) > 0 {
		if cmdArg[0] == "debug" {
			debug = true
		}
	}

	scopes.Validate("./", debug)
}
