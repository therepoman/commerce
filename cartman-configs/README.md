# cartman-configs

[Cartman](https://git-aws.internal.justin.tv/web/cartman) and [Cartman-shim](https://git-aws.internal.justin.tv/web/cartman-shim) read from this repository to determine which capabilities must be looked up given a request scope in order to return the appropriate [JWT](http://jwt.io) authorization token.

This repository is deployed to all places where Cartman and Cartman-shim are also deployed

## scopes.go

Included in this repository is the `scopes` package, accessible by the following import statement:

```
import "code.justin.tv/web/cartman-configs/scopes"
```

`ParseScopes(repoPath string)` will inspect `scopes.json` and all capabilities in the `capabilities/` subdirectory and return a `*ScopeMap` struct pointer. The `ScopeMap` struct defines the mapping of scope names to their respective mappings of capability name to the respective capability details


## Global Cartman and Cartman-shim Configurations

A `routes.json` file fines the paths which Cartman-shim will treat as requests requiring authorization tokens from Cartman. Each route specified by a URL path pattern must include:

- `backend`: The target backend which should receive the transformed request with a newly acquired authorization token
- `targetPath`: URL path pattern for the target service
  - Similar to the URL path pattern specified in the matched route, variables are interpolated by taking values from either the input URL or arguments in the request
- `parameters`: A mapping of input parameter to output parameter used to send the proper arguments to the target service
  - Legacy APIs may have required parameters that no longer apply to new endpoints so this mapping provides support for legacy APIs while allowing new APIs to be developed independently
  - A `"old_param":"new_param"` entry would take `old_param` in the incoming HTTP request and pass its value `new_param` in the HTTP request to the target service
- `passCredentials`: An array of credentials which should be taken from the incoming HTTP request and copied to the outgoing HTTP request
- `methods`: A map of HTTP method to its required list of scopes. Any unmatched HTTP methods will be transformed using the given `backend`, `targetPath`, and `parameters` fields and forwarded without a Cartman authorization token
  - `scopes`: An array of scopes required to generate a suitable authorization token for the request to the target service

A `backends.json` file defines the routes for all backends which manage Cartman tokens. Each backend is specified by a name and includes a `targetURL` object. Backends are specified in route definitions to determine what target a Cartman request should forward requests to.

A `scopes.json` file defines a list of _scopes_ maintained by Cartman. Each scope has a corresponding set of namespaced _capabilities_ which identity the means which Cartman must use to retrieve data. The namespace of a capability determine in which subdirectory the capability is defined.

## Service-specific Configurations

A `capabilities.json` file defines a list of _capabilities_ and a corresponding set of information dictating the required authentication, the expected expiration and target audience, and the exact authorization function to execute. Each capability must include:

- `name`: The name of the capability which will be namespaced when referenced in a scope definitions
  - `"name": "sample_capability"` in `capabilities/sample.json` will be referenced as `sample::sample_capability`
- `authenticationMethods`: An array of acceptable authentication protocols (`"oauth", "rails"`) which Cartman will verify
  - A capability can have multiple acceptable authentication protocols
  - As long as one of the protocols is validated the authorization function will be executed
- `authorizationFunctions`: An array of function names called to compute the authorization granted to the requesting user for the specified capability
  - Functions are defined in `web/cartman`, see [`RegisterFuncs`](https://git-aws.internal.justin.tv/web/cartman/blob/master/backend/functions.go)
- `authorizationParams`: An array of required parameters and respective types (`"string", "int", "authn_user"`) to be extracted from a Cartman HTTP request and passed to authorization functions
  - `authn_user` is a special type which indicates the value of the argument should be extracted from the specified authentication protocol, e.g. `OwnerID` of an OAuth token or `UserID` of a Rails session
- `capabilityParams`: An array of parameters and respective types (`"string", "int"`) to be included in the returned authorization token
- `expiration`: Expected expiration time (in seconds) of the specified capability
  - For scopes which require multiple capabilities, the minimum expiration time will be selected for the resulting token
- `audience`: Expected target service to receive the resulting authorization token

New capabilities should follow the convention of camelCase names for left-side assignments and underscore_delimited values, e.g. `"authorizationFunctions": ["check_for_authorization"]`

# Helpful Tips

## Debugging

`validate.go` is used to validate scopes during the build process but it can also be used to print out the list of parsed scopes and capabilities:

```
go run validate.go debug
```
