
job {
  name 'web-cartman-configs'
  using 'TEMPLATE-autobuild'
  scm {
    git {
        remote {
            github 'web/cartman-configs', 'ssh', 'git-aws.internal.justin.tv'
            credentials 'git-aws-read-key'
        }
    }
  }
  steps {
    shell 'rm -rf .manta/'
    shell 'manta -proxy'
    saveDeployArtifact 'web/cartman-configs', '.manta'
  }
}

// Use TEMPLATE-deploy or TEMPLATE-deploy-aws depending on whether your hosts
// are in bare metal or aws.
job {
  name 'web-cartman-configs-deploy'
  using 'TEMPLATE-deploy'
  steps {
    shell 'courier deploy --repo web/cartman-configs --dir /opt/twitch/cartman-configs'
  }
}