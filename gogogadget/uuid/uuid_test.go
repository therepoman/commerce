package uuid

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIsValidUUIDFalse(t *testing.T) {
	result := IsValidUUID("definitelynotauuid")
	assert.False(t, result)
}

func TestIsValidUUIDTrue(t *testing.T) {
	result := IsValidUUID("0676b1d4-b958-4671-a900-b24f0260bfc3")
	assert.True(t, result)
}
