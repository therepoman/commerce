package lrucache

import (
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

func TestLRUCache(t *testing.T) {
	Convey("Tests creation of a new cache", t, func() {
		cache, _ := NewLRUCache(NewLRUCacheParams{Size: 2})
		So(cache, ShouldNotBeNil)
	})

	Convey("Adds 3 items to cache of size 2 drops the first", t, func() {
		cache, _ := NewLRUCache(NewLRUCacheParams{Size: 2})
		cache.AddItem("1", []string{"foo"}, 2*time.Second)
		cache.AddItem("2", []string{"bar"}, 2*time.Second)
		cache.AddItem("3", []string{"baz"}, 2*time.Second)
		item1, ok := cache.GetItem("1")
		So(item1.Value(), ShouldBeNil)
		So(ok, ShouldBeFalse)

		item2, ok := cache.GetItem("2")
		So(item2.Value(), ShouldResemble, []string{"bar"})
		So(ok, ShouldBeTrue)

		time.Sleep(3 * time.Second)

		item2, ok = cache.GetItem("2")
		So(item2.Value(), ShouldBeNil)
		So(ok, ShouldBeFalse)
	})

	Convey("Removed items are not in the cache", t, func() {
		cache, _ := NewLRUCache(NewLRUCacheParams{Size: 2})
		cache.AddItem("1", []string{"foo"}, 2*time.Second)
		cache.RemoveItem("1")
		item, ok := cache.GetItem("1")
		So(item.Value(), ShouldBeNil)
		So(ok, ShouldBeFalse)
	})
}
