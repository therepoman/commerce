package lrucache

import (
	"time"

	lru "github.com/hashicorp/golang-lru"
	"github.com/pkg/errors"
)

type ILRUCache interface {
	AddItem(key string, item interface{}, ttl time.Duration)
	GetItem(key string) (ICacheItem, bool)
	RemoveItem(key string)
}

type LRUCache struct {
	cache *lru.Cache
}

type NewLRUCacheParams struct {
	Size int
}

func NewLRUCache(params NewLRUCacheParams) (*LRUCache, error) {
	cache, err := lru.New(params.Size)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create lru cache")
	}

	return &LRUCache{
		cache: cache,
	}, nil
}

func (c *LRUCache) AddItem(key string, item interface{}, ttl time.Duration) {
	c.cache.Add(key, newCacheItem(item, ttl))
}

func (c *LRUCache) GetItem(key string) (ICacheItem, bool) {
	item, ok := c.cache.Get(key)
	if !ok {
		return CacheItem{}, false
	}

	cacheItem, ok := item.(CacheItem)
	if !ok {
		return CacheItem{}, false
	}

	if cacheItem.expired() {
		return CacheItem{}, false
	}

	return cacheItem, true
}

func (c *LRUCache) RemoveItem(key string) {
	c.cache.Remove(key)
}

type ICacheItem interface {
	Value() interface{}
}

type CacheItem struct {
	value      interface{}
	expiration time.Time
}

func newCacheItem(value interface{}, ttl time.Duration) CacheItem {
	return CacheItem{
		value:      value,
		expiration: time.Now().Add(ttl),
	}
}

func (i CacheItem) Value() interface{} {
	return i.value
}

func (i CacheItem) expired() bool {
	return i.expiration.Before(time.Now())
}
