package ctx

import (
	"context"
	"net/url"
	"testing"

	"github.com/pkg/errors"

	. "github.com/smartystreets/goconvey/convey"
)

func TestIsCancelled(t *testing.T) {
	Convey("Tests context.Cancelled errors are identified properly", t, func() {
		Convey("no match", func() {
			someNovelErr := errors.New("hi")

			Convey("one level", func() {
				isCancelled := IsCancelled(someNovelErr)
				So(isCancelled, ShouldBeFalse)
			})

			Convey("two levels", func() {
				originalErr := someNovelErr
				secondLevel := errors.Wrap(originalErr, "second level")

				isCancelled := IsCancelled(secondLevel)
				So(isCancelled, ShouldBeFalse)
			})

			Convey("three levels", func() {
				originalErr := someNovelErr
				secondLevel := errors.Wrap(originalErr, "second level")
				thirdLevel := errors.Wrap(secondLevel, "third level")

				isCancelled := IsCancelled(thirdLevel)
				So(isCancelled, ShouldBeFalse)
			})
		})

		Convey("match", func() {
			Convey("one level", func() {
				isCancelled := IsCancelled(context.Canceled)
				So(isCancelled, ShouldBeTrue)
			})

			Convey("two levels", func() {
				originalErr := context.Canceled
				secondLevel := errors.Wrap(originalErr, "second level")

				isCancelled := IsCancelled(secondLevel)
				So(isCancelled, ShouldBeTrue)
			})

			Convey("three levels", func() {
				originalErr := context.Canceled
				secondLevel := errors.Wrap(originalErr, "second level")
				thirdLevel := errors.Wrap(secondLevel, "third level")

				isCancelled := IsCancelled(thirdLevel)
				So(isCancelled, ShouldBeTrue)
			})
		})

		Convey("url match", func() {
			urlErr := &url.Error{Err: context.Canceled}

			Convey("one level", func() {
				isCancelled := IsCancelled(urlErr)
				So(isCancelled, ShouldBeTrue)
			})

			Convey("two levels", func() {
				secondLevel := errors.Wrap(urlErr, "second level")

				isCancelled := IsCancelled(secondLevel)
				So(isCancelled, ShouldBeTrue)
			})

			Convey("three levels", func() {
				secondLevel := errors.Wrap(urlErr, "second level")
				thirdLevel := errors.Wrap(secondLevel, "third level")

				isCancelled := IsCancelled(thirdLevel)
				So(isCancelled, ShouldBeTrue)
			})
		})
	})
}

func TestIsDeadlineExceeded(t *testing.T) {
	Convey("Tests context.DeadlineExceeded errors are identified properly", t, func() {
		Convey("no match", func() {
			someNovelErr := errors.New("hi")

			Convey("one level", func() {
				isCancelled := IsDeadlineExceeded(someNovelErr)
				So(isCancelled, ShouldBeFalse)
			})

			Convey("two levels", func() {
				originalErr := someNovelErr
				secondLevel := errors.Wrap(originalErr, "second level")

				isCancelled := IsDeadlineExceeded(secondLevel)
				So(isCancelled, ShouldBeFalse)
			})

			Convey("three levels", func() {
				originalErr := someNovelErr
				secondLevel := errors.Wrap(originalErr, "second level")
				thirdLevel := errors.Wrap(secondLevel, "third level")

				isCancelled := IsDeadlineExceeded(thirdLevel)
				So(isCancelled, ShouldBeFalse)
			})
		})

		Convey("match", func() {
			Convey("one level", func() {
				isCancelled := IsDeadlineExceeded(context.DeadlineExceeded)
				So(isCancelled, ShouldBeTrue)
			})

			Convey("two levels", func() {
				originalErr := context.DeadlineExceeded
				secondLevel := errors.Wrap(originalErr, "second level")

				isCancelled := IsDeadlineExceeded(secondLevel)
				So(isCancelled, ShouldBeTrue)
			})

			Convey("three levels", func() {
				originalErr := context.DeadlineExceeded
				secondLevel := errors.Wrap(originalErr, "second level")
				thirdLevel := errors.Wrap(secondLevel, "third level")

				isCancelled := IsDeadlineExceeded(thirdLevel)
				So(isCancelled, ShouldBeTrue)
			})
		})

		Convey("url match", func() {
			urlErr := &url.Error{Err: context.DeadlineExceeded}

			Convey("one level", func() {
				isCancelled := IsDeadlineExceeded(urlErr)
				So(isCancelled, ShouldBeTrue)
			})

			Convey("two levels", func() {
				secondLevel := errors.Wrap(urlErr, "second level")

				isCancelled := IsDeadlineExceeded(secondLevel)
				So(isCancelled, ShouldBeTrue)
			})

			Convey("three levels", func() {
				secondLevel := errors.Wrap(urlErr, "second level")
				thirdLevel := errors.Wrap(secondLevel, "third level")

				isCancelled := IsDeadlineExceeded(thirdLevel)
				So(isCancelled, ShouldBeTrue)
			})
		})
	})
}
