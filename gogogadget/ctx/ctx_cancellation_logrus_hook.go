package ctx

import (
	"io/ioutil"

	"code.justin.tv/commerce/logrus"
)

type cancellationLogrusHook struct{}

func (h *cancellationLogrusHook) Fire(entry *logrus.Entry) error {
	if entry == nil {
		return nil
	}

	entryErrRaw, ok := entry.Data[logrus.ErrorKey]
	if !ok {
		return nil
	}

	entryErr, ok := entryErrRaw.(error)
	if !ok {
		return nil
	}

	// Do not log context cancellation errors
	if IsCancelled(entryErr) {
		*entry = logrus.Entry{
			Logger: &logrus.Logger{
				Out:       ioutil.Discard,
				Formatter: &logrus.JSONFormatter{},
			},
		}
	}

	return nil
}

func (h *cancellationLogrusHook) Levels() []logrus.Level {
	return logrus.AllLevels
}

// NewCtxCancellationLogrusHook creates a logrus hook that prevents all logs that were caused by context cancellation.
func NewCtxCancellationLogrusHook() logrus.Hook {
	return &cancellationLogrusHook{}
}
