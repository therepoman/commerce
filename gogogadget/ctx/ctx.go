package ctx

import (
	"context"
	"net/url"

	"code.justin.tv/commerce/logrus"
	errorutils "github.com/pkg/errors"
)

// cause unwraps errors wrapped with errors.Wrap, it works just like
// errors.Cause, but also unwraps url.Errors (returned by HTTP clients).
// credit: https://git-aws.internal.justin.tv/edge/helix/blob/master/internal/errorutils/errorutils.go#L69-L78
func cause(err error) error {
	c := errorutils.Cause(err)
	if uErr, ok := c.(*url.Error); ok {
		c = uErr.Err
	}
	return c
}

func IsCancelled(err error) bool {
	return cause(err) == context.Canceled
}

func IsDeadlineExceeded(err error) bool {
	return cause(err) == context.DeadlineExceeded
}

func IsCtxErr(err error) bool {
	return IsCancelled(err) || IsDeadlineExceeded(err)
}

func LogErrUnlessCtxCancelled(err error, message string, fields logrus.Fields) {
	if err != nil && !IsCancelled(err) {
		logrus.WithError(err).WithFields(fields).Error(message)
	}
}
