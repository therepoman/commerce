package http

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	log "code.justin.tv/commerce/logrus"
	"github.com/pkg/errors"
)

type HttpUtilClient interface {
	HttpPerformRequest(ctx context.Context, req *http.Request) ([]byte, int, error)
	HttpGet(ctx context.Context, url string) ([]byte, int, error)
	HttpGetWithHeaders(ctx context.Context, url string, headers map[string]string) ([]byte, int, error)
	HttpPatch(ctx context.Context, url string, body io.Reader) ([]byte, int, error)
	HttpPut(ctx context.Context, url string, body io.Reader) ([]byte, int, error)
	HttpPost(ctx context.Context, url string, body io.Reader) ([]byte, int, error)
	HttpPostWithHeaders(ctx context.Context, url string, body io.Reader, headers map[string]string) ([]byte, int, error)
	HttpDelete(ctx context.Context, url string, body io.Reader) ([]byte, int, error)
}

const defaultTimeout = time.Second * 5
const DefaultNumAttempts = 3
const defaultRetryDelay = time.Millisecond * 100

type GetRetryDelayFunction func(int, error) time.Duration

type HttpUtilClientApi struct {
	Timeout        time.Duration
	NumAttempts    int
	RetryDelayFunc GetRetryDelayFunction
}

func NewHttpUtilClientApi(timeout time.Duration, numAttempts int, retryDelayFunc GetRetryDelayFunction) HttpUtilClient {
	return &HttpUtilClientApi{
		Timeout:        timeout,
		NumAttempts:    numAttempts,
		RetryDelayFunc: retryDelayFunc,
	}
}

func NewDefaultHttpUtilClientApi() HttpUtilClient {
	return NewHttpUtilClientApi(defaultTimeout, DefaultNumAttempts, NewFixedDelayRetryFunction(defaultRetryDelay))
}

func NewHttpUtilClientApiWithCustomTimeout(timeout time.Duration) HttpUtilClient {
	return NewHttpUtilClientApi(timeout, DefaultNumAttempts, NewFixedDelayRetryFunction(defaultRetryDelay))
}

func NewHttpUtilClientApiWithCustomRetryDelayFunc(numAttempts int, retryDelayFunc GetRetryDelayFunction) HttpUtilClient {
	return NewHttpUtilClientApi(defaultTimeout, numAttempts, retryDelayFunc)
}

func NewFixedDelayRetryFunction(retryDelay time.Duration) GetRetryDelayFunction {
	return func(attempt int, err error) time.Duration {
		return retryDelay
	}
}

func NewExponentialBackoffRetryFunction(initialDelay time.Duration, multiplier int, maxDelay time.Duration) GetRetryDelayFunction {
	return func(attempt int, err error) time.Duration {
		delay := initialDelay
		for i := 2; i <= attempt; i++ {
			delay *= time.Duration(multiplier)
			if delay >= maxDelay {
				return maxDelay
			}
		}
		return delay
	}
}

// Allows issuing and cancel of an HTTP request if the ctx.Done
// is closed while the request of response is being processed.
func (this *HttpUtilClientApi) httpDo(ctx context.Context, req *http.Request, result func(*http.Response, error) error) error {
	req.WithContext(ctx)
	tr := &http.Transport{}
	client := &http.Client{Transport: tr}
	return result(client.Do(req))
}

func isSuccessfulHttpStatusCode(statusCode int) bool {
	return statusCode >= 200 && statusCode < 300
}

func (this *HttpUtilClientApi) HttpPerformRequest(parent context.Context, req *http.Request) ([]byte, int, error) {
	var bytesResult []byte
	var responseStatusCode int

	var bodyBuffer []byte
	var err error

	if req.Body != nil {
		bodyBuffer, err = ioutil.ReadAll(req.Body)
		if err != nil {
			msg := "Error saving request body to buffer"
			log.WithError(err).Error(msg)
			return bytesResult, responseStatusCode, errors.Wrap(err, msg)
		}
	}

	for attempt := 1; attempt <= this.NumAttempts; attempt++ {
		select {
		case <-parent.Done():
			return nil, http.StatusInternalServerError, errors.New("Timed out waiting on request to succeed")
		default:
			ctx, cancel := context.WithTimeout(parent, this.Timeout)
			defer cancel()

			if req.Body != nil {
				req.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBuffer))
			}

			err = this.httpDo(ctx, req, func(resp *http.Response, err error) error {
				if resp != nil && resp.Body != nil {
					defer close(resp.Body)
				}
				if err != nil {
					msg := fmt.Sprintf("Error performing request, %v", err)
					log.Warnf(msg)
					return errors.Wrap(err, msg)
				}
				responseStatusCode = resp.StatusCode
				if req.Method == "GET" && resp.StatusCode == 404 {
					return nil
				}
				if !isSuccessfulHttpStatusCode(resp.StatusCode) && resp.StatusCode != http.StatusUnprocessableEntity {
					log.WithField("statusCode", resp.StatusCode).Warn("Received non-successful HTTP response code")
					return errors.New(fmt.Sprintf("Received non-successful HTTP response code: %d", resp.StatusCode))
				}
				bytesResult, err = ioutil.ReadAll(resp.Body)
				if err != nil {
					msg := "Error reading contents from HTTP result body"
					log.Warnf(msg)
					return errors.Wrap(err, msg)
				}
				return nil
			})

			if err == nil {
				return bytesResult, responseStatusCode, nil
			} else if attempt < this.NumAttempts {
				timer := time.After(this.RetryDelayFunc(attempt, err))

				select {
				case <-timer:
					continue
				case <-parent.Done():
					return nil, http.StatusInternalServerError, errors.New("Timed out waiting on request to succeed")
				}
			}
		}
	}

	if err != nil {
		msg := "HTTP request failed after multiple attempts."
		log.WithField("numAttempts", this.NumAttempts).WithError(err).Error(msg)
		return nil, responseStatusCode, errors.Wrap(err, msg)
	}

	return bytesResult, responseStatusCode, nil
}

func (this *HttpUtilClientApi) HttpGet(ctx context.Context, url string) ([]byte, int, error) {
	req, err := GetRequest(url)
	if err != nil {
		return nil, 0, err
	}
	return this.HttpPerformRequest(ctx, req)
}

func (this *HttpUtilClientApi) HttpGetWithHeaders(ctx context.Context, url string, headers map[string]string) ([]byte, int, error) {
	req, err := GetRequest(url)
	applyHeaders(req, headers)
	if err != nil {
		return nil, 0, err
	}
	return this.HttpPerformRequest(ctx, req)
}

func (this *HttpUtilClientApi) HttpPatch(ctx context.Context, url string, body io.Reader) ([]byte, int, error) {
	req, err := PatchRequest(url, body)
	if err != nil {
		return nil, 0, err
	}
	return this.HttpPerformRequest(ctx, req)
}

func (this *HttpUtilClientApi) HttpPut(ctx context.Context, url string, body io.Reader) ([]byte, int, error) {
	req, err := PutRequest(url, body)
	if err != nil {
		return nil, 0, err
	}
	return this.HttpPerformRequest(ctx, req)
}

func (this *HttpUtilClientApi) HttpPost(ctx context.Context, url string, body io.Reader) ([]byte, int, error) {
	req, err := PostRequest(url, body)
	if err != nil {
		return nil, 0, err
	}
	return this.HttpPerformRequest(ctx, req)
}

func (this *HttpUtilClientApi) HttpPostWithHeaders(ctx context.Context, url string, body io.Reader, headers map[string]string) ([]byte, int, error) {
	req, err := PostRequest(url, body)
	applyHeaders(req, headers)
	if err != nil {
		return nil, 0, err
	}
	return this.HttpPerformRequest(ctx, req)
}

func (this *HttpUtilClientApi) HttpDelete(ctx context.Context, url string, body io.Reader) ([]byte, int, error) {
	req, err := DeleteRequest(url, body)
	if err != nil {
		return nil, 0, err
	}
	return this.HttpPerformRequest(ctx, req)
}

func bodyRequest(httpVerb string, url string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest(httpVerb, url, body)
	if err != nil {
		return nil, err
	}
	if strings.TrimSpace(req.Header.Get("Content-Type")) == "" {
		req.Header.Set("Content-Type", "application/json")
	}
	return req, nil
}

func applyHeaders(req *http.Request, headers map[string]string) {
	for headerKey, headerValue := range headers {
		req.Header.Set(headerKey, headerValue)
	}
}

func GetRequest(url string) (*http.Request, error) {
	return http.NewRequest("GET", url, nil)
}

func PatchRequest(url string, body io.Reader) (*http.Request, error) {
	return bodyRequest("PATCH", url, body)
}

func PutRequest(url string, body io.Reader) (*http.Request, error) {
	return bodyRequest("PUT", url, body)
}

func PostRequest(url string, body io.Reader) (*http.Request, error) {
	return bodyRequest("POST", url, body)
}

func DeleteRequest(url string, body io.Reader) (*http.Request, error) {
	return bodyRequest("DELETE", url, body)
}

func CreateURL(base *url.URL, values url.Values) *url.URL {
	return &url.URL{
		Scheme:   base.Scheme,
		Opaque:   base.Opaque,
		User:     base.User,
		Host:     base.Host,
		Path:     base.Path,
		RawQuery: values.Encode(),
		Fragment: base.Fragment,
	}
}

func close(body io.ReadCloser) {
	err := body.Close()
	if err != nil {
		log.WithError(err).Error("Failed to close")
	}
}
