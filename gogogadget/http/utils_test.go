package http

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var requestNum int

func setup(codes ...int) *httptest.Server {
	requestNum = 0
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ioutil.ReadAll(r.Body)
		w.WriteHeader(codes[requestNum])

		w.Header().Set("Content-Type", "application/json")
		fmt.Fprintln(w, "testBody")
		requestNum += 1
	}))
	return server
}

func TestCreateURL(t *testing.T) {
	base, _ := url.Parse("http://test_host/resource")
	v := url.Values{}
	v.Add("data", "foo")

	expected, _ := url.Parse("http://test_host/resource?data=foo")

	actual := CreateURL(base, v)
	assert.Equal(t, expected, actual)
}

func TestHttpGetSucceedsOnFirst(t *testing.T) {
	server := setup(200)
	client := NewDefaultHttpUtilClientApi()

	body, code, err := client.HttpGet(context.Background(), server.URL)
	assert.Equal(t, "testBody\n", string(body))
	assert.Equal(t, 200, code)
	assert.NoError(t, err)
}

func TestHttpPostSucceedsOnFirst(t *testing.T) {
	server := setup(200)
	client := NewDefaultHttpUtilClientApi()

	body, code, err := client.HttpPost(context.Background(), server.URL, bytes.NewReader([]byte{byte('x')}))
	assert.Equal(t, "testBody\n", string(body))
	assert.Equal(t, 200, code)
	assert.NoError(t, err)
}

func TestHttpGetSucceedsOnSecond(t *testing.T) {
	server := setup(500, 200)
	client := NewDefaultHttpUtilClientApi()

	body, code, err := client.HttpGet(context.Background(), server.URL)
	assert.Equal(t, "testBody\n", string(body))
	assert.Equal(t, 200, code)
	assert.NoError(t, err)
}

func TestHttpPostSucceedsOnSecond(t *testing.T) {
	server := setup(500, 200)
	client := NewDefaultHttpUtilClientApi()

	body, code, err := client.HttpPost(context.Background(), server.URL, bytes.NewReader([]byte{byte('x')}))
	assert.Equal(t, "testBody\n", string(body))
	assert.Equal(t, 200, code)
	assert.NoError(t, err)
}

func TestHttpGetSucceedsOnThird(t *testing.T) {
	server := setup(500, 500, 200)
	client := NewDefaultHttpUtilClientApi()

	body, code, err := client.HttpGet(context.Background(), server.URL)
	assert.Equal(t, "testBody\n", string(body))
	assert.Equal(t, 200, code)
	assert.NoError(t, err)
}

func TestHttpPostSucceedsOnThird(t *testing.T) {
	server := setup(500, 500, 200)
	client := NewDefaultHttpUtilClientApi()

	body, code, err := client.HttpPost(context.Background(), server.URL, bytes.NewReader([]byte{byte('x')}))
	assert.Equal(t, "testBody\n", string(body))
	assert.Equal(t, 200, code)
	assert.NoError(t, err)
}

func TestHttpGetFailsAfterThreeAttempts(t *testing.T) {
	server := setup(500, 500, 500)
	client := NewDefaultHttpUtilClientApi()

	_, code, err := client.HttpGet(context.Background(), server.URL)
	assert.Equal(t, 500, code)
	assert.Error(t, err)
}

func TestHttpPostFailsAfterThreeAttempts(t *testing.T) {
	server := setup(500, 500, 500)
	client := NewDefaultHttpUtilClientApi()

	_, code, err := client.HttpPost(context.Background(), server.URL, bytes.NewReader([]byte{byte('x')}))
	assert.Equal(t, 500, code)
	assert.Error(t, err)
}

func TestHttpGetDoesNotRetryOnNoContent(t *testing.T) {
	server := setup(204)
	client := NewDefaultHttpUtilClientApi()

	_, code, err := client.HttpGet(context.Background(), server.URL)
	assert.Equal(t, 204, code)
	assert.NoError(t, err)
}

func TestHttpPostDoesNotRetryOnNoContent(t *testing.T) {
	server := setup(204)
	client := NewDefaultHttpUtilClientApi()

	_, code, err := client.HttpPost(context.Background(), server.URL, bytes.NewReader([]byte{byte('x')}))
	assert.Equal(t, 204, code)
	assert.NoError(t, err)
}

func TestNewExponentialBackoffRetryFunction(t *testing.T) {
	retryDelayFunc := NewExponentialBackoffRetryFunction(50*time.Millisecond, 2, time.Second*2)
	assert.Equal(t, 50*time.Millisecond, retryDelayFunc(1, nil))
	assert.Equal(t, 100*time.Millisecond, retryDelayFunc(2, nil))
	assert.Equal(t, 200*time.Millisecond, retryDelayFunc(3, nil))
	assert.Equal(t, 400*time.Millisecond, retryDelayFunc(4, nil))
	assert.Equal(t, 800*time.Millisecond, retryDelayFunc(5, nil))
	assert.Equal(t, 1600*time.Millisecond, retryDelayFunc(6, nil))
	assert.Equal(t, 2000*time.Millisecond, retryDelayFunc(7, nil))
	assert.Equal(t, 2000*time.Millisecond, retryDelayFunc(8, nil))
	assert.Equal(t, 2000*time.Millisecond, retryDelayFunc(9, nil))
}
