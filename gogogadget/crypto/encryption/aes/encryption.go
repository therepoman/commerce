package aes

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"io"

	"github.com/pkg/errors"
)

const nonceLength = 12

func Encrypt(key []byte, data []byte) ([]byte, error) {

	aesCipherBlock, err := aes.NewCipher(key)
	if err != nil {
		return nil, errors.Wrap(err, "error creating AES cipher")
	}

	nonce := make([]byte, nonceLength)
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return nil, errors.Wrap(err, "error creating nonce")
	}

	aesgcm, err := cipher.NewGCM(aesCipherBlock)
	if err != nil {
		return nil, errors.Wrap(err, "error creating GCM")
	}

	encrypted := aesgcm.Seal(nil, nonce, data, nil)
	return append(nonce, encrypted...), nil
}

func Decrypt(key []byte, data []byte) ([]byte, error) {
	nonce := data[:nonceLength]
	encrypted := data[nonceLength:]

	aesCipherBlock, err := aes.NewCipher(key)
	if err != nil {
		return []byte{}, errors.Wrap(err, "error creating AES cipher")
	}

	aesgcm, err := cipher.NewGCM(aesCipherBlock)
	if err != nil {
		return []byte{}, errors.Wrap(err, "error creating GCM")
	}

	decrypted, err := aesgcm.Open(nil, nonce, encrypted, nil)
	if err != nil {
		return []byte{}, errors.Wrap(err, "error opening encrypted data")
	}

	return decrypted, nil
}
