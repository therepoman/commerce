package random

import (
	"crypto/rand"
	"errors"
	"math/big"
)

const DefaultAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

func RandomString(alphabet string, length int) (string, error) {
	if len(alphabet) == 0 {
		return "", errors.New("alphabet cannot be empty")
	}

	if length < 0 {
		return "", errors.New("length cannot be negative")
	}

	bytes := make([]byte, length)
	alphabetLength := big.NewInt(int64(len(alphabet)))
	if alphabetLength == nil {
		return "", errors.New("invalid alphabet length")
	}

	for i := 0; i < length; i++ {
		alphabetIndex, err := rand.Int(rand.Reader, alphabetLength)
		if err != nil {
			return "", err
		}

		if alphabetIndex == nil {
			return "", errors.New("invalid alphabet index generated")
		}

		bytes[i] = alphabet[alphabetIndex.Int64()]
	}
	return string(bytes), nil
}
