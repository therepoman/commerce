package random

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestRandomString(t *testing.T) {
	Convey("Test various inputs", t, func() {
		Convey("Errors when alphabet is empty", func() {
			_, err := RandomString("", 1)
			So(err, ShouldNotBeNil)
		})
		Convey("Errors when length is negative", func() {
			_, err := RandomString("A", -1)
			So(err, ShouldNotBeNil)
		})
		Convey("Generates empty string when length is zero", func() {
			out, err := RandomString("A", 0)
			So(out, ShouldEqual, "")
			So(err, ShouldBeNil)
		})
		Convey("Generates repeat string when alphabet consists of one char", func() {
			out, err := RandomString("A", 10)
			So(out, ShouldEqual, "AAAAAAAAAA")
			So(err, ShouldBeNil)
		})
		Convey("Happy case", func() {
			out, err := RandomString("ABC", 30)
			So(len(out), ShouldEqual, 30)
			So(err, ShouldBeNil)
		})
	})
}
