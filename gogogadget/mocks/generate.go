package mocks

// Use command: make generate_mocks
// To regenerate the mocks defined in this file

// AWS SDK DynamoDB Interface
//go:generate $GOPATH/src/code.justin.tv/commerce/gogogadget/_tools/bin/retool do mockery -name=DynamoDBAPI -dir=$GOPATH/src/code.justin.tv/commerce/gogogadget/vendor/github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface -output=$GOPATH/src/code.justin.tv/commerce/gogogadget/mocks/github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface -outpkg=dynamodbiface_mock

// AWS SDK ElastiCache Interface
//go:generate $GOPATH/src/code.justin.tv/commerce/gogogadget/_tools/bin/retool do mockery -name=ElastiCacheAPI -dir=$GOPATH/src/code.justin.tv/commerce/gogogadget/vendor/github.com/aws/aws-sdk-go/service/elasticache/elasticacheiface -output=$GOPATH/src/code.justin.tv/commerce/gogogadget/mocks/github.com/aws/aws-sdk-go/service/elasticache/elasticacheiface -outpkg=elasticacheiface_mock

// S3 client
//go:generate $GOPATH/src/code.justin.tv/commerce/gogogadget/_tools/bin/retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/gogogadget/aws/s3 -output=$GOPATH/src/code.justin.tv/commerce/gogogadget/mocks/code.justin.tv/commerce/gogogadget/aws/s3 -outpkg=s3_mock

// LRUCache
//go:generate $GOPATH/src/code.justin.tv/commerce/gogogadget/_tools/bin/retool do mockery -name=ILRUCache -dir=$GOPATH/src/code.justin.tv/commerce/gogogadget/lrucache -output=$GOPATH/src/code.justin.tv/commerce/gogogadget/mocks/code.justin.tv/commerce/gogogadget/lrucache -outpkg=lrucache_mock

// CacheItem
//go:generate $GOPATH/src/code.justin.tv/commerce/gogogadget/_tools/bin/retool do mockery -name=ICacheItem -dir=$GOPATH/src/code.justin.tv/commerce/gogogadget/lrucache -output=$GOPATH/src/code.justin.tv/commerce/gogogadget/mocks/code.justin.tv/commerce/gogogadget/lrucacheitem -outpkg=lrucacheitem_mock
