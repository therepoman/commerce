package golang

import (
	"runtime"
	"strings"
)

type GoVersion string

const (
	go15Internal  = "go1.5"
	go16Internal  = "go1.6"
	go17Internal  = "go1.7"
	go18Internal  = "go1.8"
	go19Internal  = "go1.9"
	go110Internal = "go1.10"
)

const (
	Go15      = GoVersion("1.5")
	Go16      = GoVersion("1.6")
	Go17      = GoVersion("1.7")
	Go18      = GoVersion("1.8")
	Go19      = GoVersion("1.9")
	Go110     = GoVersion("1.10")
	GoUnknown = GoVersion("unknown")
)

func internalGoVersion() string {
	return runtime.Version()
}

func GetGoVersion() GoVersion {
	goVersion := internalGoVersion()
	if strings.Contains(goVersion, go110Internal) {
		return Go110
	} else if strings.Contains(goVersion, go19Internal) {
		return Go19
	} else if strings.Contains(goVersion, go18Internal) {
		return Go18
	} else if strings.Contains(goVersion, go17Internal) {
		return Go17
	} else if strings.Contains(goVersion, go16Internal) {
		return Go16
	} else if strings.Contains(goVersion, go15Internal) {
		return Go15
	}
	return GoUnknown
}

func IsGo19OrNewer() bool {
	currentVer := GetGoVersion()
	return currentVer == Go19 || currentVer == Go110 || currentVer == GoUnknown
}
