package compression

import (
	"compress/flate"
	"fmt"

	. "github.com/smartystreets/goconvey/convey"
)

func (s *CompressionTestSuite) TestFlate() {
	Convey("given a flate compressor", s.T(), func() {
		var level int
		Convey("given no compression level", func() {
			level = flate.NoCompression
		})

		Convey("given default compression level", func() {
			level = flate.DefaultCompression
		})

		Convey("given huffman only level", func() {
			level = flate.HuffmanOnly
		})

		Convey("given best speed level", func() {
			level = flate.BestSpeed
		})

		Convey("given best compression level", func() {
			level = flate.BestCompression
		})

		fc, err := NewFlateCompressor(level)
		So(err, ShouldBeNil)

		c, err := fc.CompressString(s.value)
		So(err, ShouldBeNil)

		if s.outputStats {
			fmt.Printf("level: %s\noriginal len: %d\ncompressed len: %d\n\n", LevelString(level), len(s.value), len(c))
		}

		uc, err := fc.DecompressString(c)
		So(err, ShouldBeNil)
		So(uc, ShouldEqual, s.value)
	})
}
