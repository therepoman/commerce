package compression

import (
	"compress/lzw"
	"fmt"
)

func (s *CompressionTestSuite) TestLzw() {
	lc, err := NewLzwCompressor(lzw.LSB, 8)
	s.Require().NoError(err)

	c, err := lc.CompressString(s.value)
	s.Require().NoError(err)

	if s.outputStats {
		fmt.Printf("original len: %d\ncompressed len: %d\n", len(s.value), len(c))
	}

	uc, err := lc.DecompressString(c)
	s.Require().NoError(err)
	s.Require().Equal(s.value, uc)
}
