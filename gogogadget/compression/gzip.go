package compression

import (
	"compress/gzip"
	"io"
	"strings"
	"sync"

	"github.com/pkg/errors"
)

type GzipCompressor struct {
	compressionLevel int
	readers          sync.Pool
	writers          sync.Pool
}

func NewGzipCompressor(compressionLevel int) (*GzipCompressor, error) {
	if compressionLevel < gzip.HuffmanOnly || compressionLevel > gzip.BestCompression {
		return nil, errors.New("invalid compression level provided")
	}

	return &GzipCompressor{
		writers: sync.Pool{
			New: func() interface{} {
				w, _ := gzip.NewWriterLevel(nil, compressionLevel)
				return w
			},
		},
	}, nil
}

func (c *GzipCompressor) CompressString(val string) (string, error) {
	var buff strings.Builder
	gw, ok := c.writers.Get().(*gzip.Writer)
	if !ok || gw == nil {
		return "", errors.New("could not get gzip writer")
	}
	defer c.writers.Put(gw)
	gw.Reset(&buff)

	if _, err := strings.NewReader(val).WriteTo(gw); err != nil {
		return "", errors.Wrap(err, "could not write string to gzip buffer")
	}

	if err := gw.Close(); err != nil {
		return "", errors.Wrap(err, "could not close gzip writer")
	}

	return buff.String(), nil
}

func (c *GzipCompressor) DecompressString(val string) (string, error) {
	sr := strings.NewReader(val)
	gr, ok := c.readers.Get().(*gzip.Reader)
	if !ok || gr == nil {
		var err error
		gr, err = gzip.NewReader(sr)
		if err != nil {
			return "", errors.Wrap(err, "could not create new gzip reader")
		}
	} else {
		gr.Reset(sr)
	}
	defer c.readers.Put(gr)

	buff := strings.Builder{}
	_, err := io.Copy(&buff, gr)
	if err != nil {
		return "", errors.Wrap(err, "could not read from gzip reader")
	}

	return buff.String(), nil
}
