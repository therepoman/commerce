package compression

import (
	"bytes"
	"compress/lzw"
	"io"
	"strings"

	"github.com/pkg/errors"
)

type LzwCompressor struct {
	order    lzw.Order
	litWidth int
}

func NewLzwCompressor(order lzw.Order, litWidth int) (*LzwCompressor, error) {
	if litWidth < 2 || litWidth > 8 {
		return nil, errors.New("invalid lit width: must be between 2 and 8 inclusively")
	}

	return &LzwCompressor{
		order:    order,
		litWidth: litWidth,
	}, nil
}

func (c *LzwCompressor) CompressString(val string) (string, error) {
	var buff bytes.Buffer
	lw := lzw.NewWriter(&buff, c.order, c.litWidth)

	if _, err := strings.NewReader(val).WriteTo(lw); err != nil {
		return "", errors.Wrap(err, "could not write string to lzw buffer")
	}

	if err := lw.Close(); err != nil {
		return "", errors.Wrap(err, "could not close lzw writer")
	}

	return buff.String(), nil
}

func (c *LzwCompressor) DecompressString(val string) (string, error) {
	lr := lzw.NewReader(strings.NewReader(val), c.order, c.litWidth)

	buff := strings.Builder{}
	_, err := io.Copy(&buff, lr)
	if err != nil {
		return "", errors.Wrap(err, "could not read from lzw reader")
	}

	return buff.String(), nil
}
