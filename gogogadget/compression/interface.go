package compression

type Compressor interface {
	CompressString(val string) (string, error)
	DecompressString(val string) (string, error)
}
