package compression

import (
	"compress/zlib"
	"io"
	"strings"
	"sync"

	"github.com/pkg/errors"
)

type ZlibCompressor struct {
	compressionLevel int
	writers          sync.Pool
	readers          sync.Pool
}

func NewZlibCompressor(compressionLevel int) (*ZlibCompressor, error) {
	if compressionLevel < zlib.HuffmanOnly || compressionLevel > zlib.BestCompression {
		return nil, errors.New("invalid compression level provided")
	}

	return &ZlibCompressor{
		writers: sync.Pool{
			New: func() interface{} {
				w, _ := zlib.NewWriterLevel(nil, compressionLevel)
				return w
			},
		},
	}, nil
}

func (c *ZlibCompressor) CompressString(val string) (string, error) {
	var buff strings.Builder
	zw, ok := c.writers.Get().(*zlib.Writer)
	if !ok || zw == nil {
		return "", errors.New("could not create zlib writer")
	}
	defer c.writers.Put(zw)
	zw.Reset(&buff)

	if _, err := strings.NewReader(val).WriteTo(zw); err != nil {
		return "", errors.Wrap(err, "could not write string to zlib buffer")
	}

	if err := zw.Close(); err != nil {
		return "", errors.Wrap(err, "could not close zlib writer")
	}

	return buff.String(), nil
}

func (c *ZlibCompressor) DecompressString(val string) (string, error) {
	sr := strings.NewReader(val)
	zr, ok := c.readers.Get().(io.ReadCloser)
	if !ok || zr == nil {
		var err error
		zr, err = zlib.NewReader(sr)
		if err != nil {
			return "", errors.Wrap(err, "could not create zlib reader")
		}
	} else {
		zr.(zlib.Resetter).Reset(sr, nil)
	}

	buff := strings.Builder{}
	_, err := io.Copy(&buff, zr)
	if err != nil {
		return "", errors.Wrap(err, "could not read from zlib reader")
	}

	return buff.String(), nil
}
