package compression

import (
	"compress/gzip"
	"fmt"

	. "github.com/smartystreets/goconvey/convey"
)

func (s *CompressionTestSuite) TestGzip() {
	Convey("given a gzip compressor", s.T(), func() {
		var level int
		Convey("given no compression level", func() {
			level = gzip.NoCompression
		})

		Convey("given default compression level", func() {
			level = gzip.DefaultCompression
		})

		Convey("given huffman only level", func() {
			level = gzip.HuffmanOnly
		})

		Convey("given best speed level", func() {
			level = gzip.BestSpeed
		})

		Convey("given best compression level", func() {
			level = gzip.BestCompression
		})

		gc, err := NewGzipCompressor(level)
		So(err, ShouldBeNil)

		c, err := gc.CompressString(s.value)
		So(err, ShouldBeNil)

		if s.outputStats {
			fmt.Printf("level: %s\noriginal len: %d\ncompressed len: %d\n\n", LevelString(level), len(s.value), len(c))
		}

		uc, err := gc.DecompressString(c)
		So(err, ShouldBeNil)
		So(uc, ShouldEqual, s.value)
	})
}
