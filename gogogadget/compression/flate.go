package compression

import (
	"compress/flate"
	"io"
	"strings"
	"sync"

	"github.com/pkg/errors"
)

type FlateCompressor struct {
	writers sync.Pool
	readers sync.Pool
}

func NewFlateCompressor(compressionLevel int) (*FlateCompressor, error) {
	if compressionLevel < flate.HuffmanOnly || compressionLevel > flate.BestCompression {
		return nil, errors.New("invalid compression level provided")
	}

	return &FlateCompressor{
		writers: sync.Pool{
			New: func() interface{} {
				w, _ := flate.NewWriter(nil, compressionLevel)
				return w
			},
		},
	}, nil
}

func (c *FlateCompressor) CompressString(val string) (string, error) {
	var buff strings.Builder
	fw, ok := c.writers.Get().(*flate.Writer)
	if !ok || fw == nil {
		return "", errors.New("could not create flate writer")
	}
	defer c.writers.Put(fw)
	fw.Reset(&buff)

	if _, err := strings.NewReader(val).WriteTo(fw); err != nil {
		return "", errors.Wrap(err, "could not write string to flate buffer")
	}

	if err := fw.Close(); err != nil {
		return "", errors.Wrap(err, "could not close flate writer")
	}

	return buff.String(), nil
}

func (c *FlateCompressor) DecompressString(val string) (string, error) {
	sr := strings.NewReader(val)
	fr, ok := c.readers.Get().(io.ReadCloser)
	if !ok || fr == nil {
		fr = flate.NewReader(sr)
	} else {
		fr.(flate.Resetter).Reset(sr, nil)
	}
	defer c.readers.Put(fr)

	buff := strings.Builder{}
	_, err := io.Copy(&buff, fr)
	if err != nil {
		return "", errors.Wrap(err, "could not read from flate reader")
	}

	return buff.String(), nil
}
