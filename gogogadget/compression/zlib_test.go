package compression

import (
	"compress/zlib"
	"fmt"

	. "github.com/smartystreets/goconvey/convey"
)

func (s *CompressionTestSuite) TestZlib() {
	Convey("given a zlib compressor", s.T(), func() {
		var level int
		Convey("given no compression level", func() {
			level = zlib.NoCompression
		})

		Convey("given default compression level", func() {
			level = zlib.DefaultCompression
		})

		Convey("given huffman only level", func() {
			level = zlib.HuffmanOnly
		})

		Convey("given best speed level", func() {
			level = zlib.BestSpeed
		})

		Convey("given best compression level", func() {
			level = zlib.BestCompression
		})

		zc, err := NewZlibCompressor(level)
		So(err, ShouldBeNil)

		c, err := zc.CompressString(s.value)
		So(err, ShouldBeNil)

		if s.outputStats {
			fmt.Printf("level: %s\noriginal len: %d\ncompressed len: %d\n\n", LevelString(level), len(s.value), len(c))
		}

		uc, err := zc.DecompressString(c)
		So(err, ShouldBeNil)
		So(uc, ShouldEqual, s.value)
	})
}
