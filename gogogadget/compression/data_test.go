package compression

import (
	"compress/flate"
	"fmt"
	"testing"

	"github.com/stretchr/testify/suite"
)

func TestCompression(t *testing.T) {
	suite.Run(t, new(CompressionTestSuite))
}

type CompressionTestSuite struct {
	suite.Suite

	value       string
	outputStats bool
}

func (s *CompressionTestSuite) SetupSuite() {
	jsonString := `{"TotalBits":322,"CanPurchase":{"B017L2UX4C":true,"B018WMZKR0":true,"B018WMZN5E":true,"B018WMZSPY":true,"B018WMZYPI":true,"B01G4BISOS":true,"B073RTRHTZ":true,"B075ZD2VZQ":false,"B077H4S2KH":false,"B079Y8SGFK":false,"B07KFRRT2Y":false,"SKU-00000026":false},"TotalBitsWithBroadcaster":322,"MaximumBitsInventoryLimit":1000000,"ProductInformation":{"B017L2UX4C":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"B018WMZKR0":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"B018WMZN5E":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"B018WMZSPY":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"B018WMZYPI":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"B01G4BISOS":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"B073RTRHTZ":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"B075ZD2VZQ":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"B077H4S2KH":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"B079Y8SGFK":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"B07KFRRT2Y":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"SKU-00000026":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null}},"MostRecentPurchaseWebPlatform":"AMAZON"}`

	s.value = jsonString
	s.outputStats = true
}

func LevelString(level int) string {
	switch level {
	case flate.HuffmanOnly:
		return "huffman only"
	case flate.NoCompression:
		return "no compression"
	case flate.DefaultCompression:
		return "default compression"
	case flate.BestSpeed:
		return "best speed"
	case flate.BestCompression:
		return "best compression"
	default:
		return fmt.Sprintf("unknown level: %d", level)
	}
}
