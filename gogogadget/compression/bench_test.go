package compression

import (
	"compress/flate"
	"compress/lzw"
	"fmt"
	"testing"
)

const (
	jsonStringFormat = `{"TotalBits":%d,"CanPurchase":{"B017L2UX4C":true,"B018WMZKR0":true,"B018WMZN5E":true,"B018WMZSPY":true,"B018WMZYPI":true,"B01G4BISOS":true,"B073RTRHTZ":true,"B075ZD2VZQ":false,"B077H4S2KH":false,"B079Y8SGFK":false,"B07KFRRT2Y":false,"SKU-00000026":false},"TotalBitsWithBroadcaster":322,"MaximumBitsInventoryLimit":1000000,"ProductInformation":{"B017L2UX4C":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"B018WMZKR0":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"B018WMZN5E":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"B018WMZSPY":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"B018WMZYPI":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"B01G4BISOS":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"B073RTRHTZ":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"B075ZD2VZQ":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"B077H4S2KH":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"B079Y8SGFK":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"B07KFRRT2Y":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null},"SKU-00000026":{"Platforms":["AMAZON","PAYPAL","XSOLLA"],"NumberOfBits":1000,"Promotional":false,"PromoType":null,"PromoId":null,"CantPurcaseReason":null,"PurchasableQuantity":null}},"MostRecentPurchaseWebPlatform":"AMAZON"}`
)

func BenchmarkFlateCompressor(b *testing.B) {
	c, _ := NewFlateCompressor(flate.BestCompression)
	for n := 0; n < b.N; n++ {
		val := fmt.Sprintf(jsonStringFormat, n)

		cVal, _ := c.CompressString(val)

		ucVal, _ := c.DecompressString(cVal)

		if val != ucVal {
			fmt.Println("value after compress / uncompress not equal")
		}
	}
}

func BenchmarkZlibCompressor(b *testing.B) {
	c, _ := NewZlibCompressor(flate.BestCompression)
	for n := 0; n < b.N; n++ {
		val := fmt.Sprintf(jsonStringFormat, n)

		cVal, _ := c.CompressString(val)

		ucVal, _ := c.DecompressString(cVal)

		if val != ucVal {
			fmt.Println("value after compress / uncompress not equal")
		}
	}
}

func BenchmarkGzipCompressor(b *testing.B) {
	c, _ := NewGzipCompressor(flate.BestCompression)
	for n := 0; n < b.N; n++ {
		val := fmt.Sprintf(jsonStringFormat, n)

		cVal, _ := c.CompressString(val)

		ucVal, _ := c.DecompressString(cVal)

		if val != ucVal {
			fmt.Println("value after compress / uncompress not equal")
		}
	}
}

func BenchmarkLzwCompressor(b *testing.B) {
	c, _ := NewLzwCompressor(lzw.LSB, 8)
	for n := 0; n < b.N; n++ {
		val := fmt.Sprintf(jsonStringFormat, n)

		cVal, _ := c.CompressString(val)

		ucVal, _ := c.DecompressString(cVal)

		if val != ucVal {
			fmt.Println("value after compress / uncompress not equal")
		}
	}
}
