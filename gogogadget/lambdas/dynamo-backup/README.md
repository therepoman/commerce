# Dynamo Backup Lambda

## Lambda Behavior
This lambda accepts a JSON request in the form of:
```json
{
  "table-name" : "some-dynamo-table"
}
```
Triggering the lambda with this input will create a dynamo backup for the corresponding dynamodb table.

## Building the Deploy Target
Simply run the build-deploy-target.sh shell script.

```shell
./build-deploy-target.sh
```

This creates a deployment.zip file which can be uploaded to your lambda.

## Automatic Scheduled Backups
To automatically backup your dynamo tables, setup scheduled cloudwatch events to trigger this lambda at a set interval.
You will need to configure one cloudwatch even per dynamodb table with a different JSON payload.
