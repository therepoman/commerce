package main

import (
	"errors"
	"fmt"
	"sort"
	"strings"
	"time"

	string_utils "code.justin.tv/commerce/gogogadget/strings"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

const (
	defaultRegion = "us-west-2"
)

type TimeUnit string

const (
	Hour  = TimeUnit("hour")
	Day   = TimeUnit("day")
	Week  = TimeUnit("week")
	Month = TimeUnit("month")
)

var timeUnits = map[TimeUnit]interface{}{
	Hour:  nil,
	Day:   nil,
	Week:  nil,
	Month: nil,
}

func (t TimeUnit) IsValid() bool {
	_, ok := timeUnits[t]
	return ok
}

type Request struct {
	TableName    string   `json:"table_name"`
	TimeUnit     TimeUnit `json:"time_unit"`
	NumberToKeep int      `json:"number_to_keep"`
	Region       string   `json:"region"`
}

func ProcessBackupRequest(request Request, dynamoClient dynamodbiface.DynamoDBAPI) error {
	if string_utils.Blank(request.TableName) {
		return errors.New("table name cannot be blank")
	}

	if !request.TimeUnit.IsValid() {
		return errors.New(fmt.Sprintf("invalid timeunit: %v", request.TimeUnit))
	}

	if request.NumberToKeep <= 0 {
		return errors.New("number to keep must be a positive integer")
	}

	backupName := fmt.Sprintf("%s_%s_backup_%s", request.TableName, request.TimeUnit, time.Now().Format(time.RFC3339))
	backupName = strings.Replace(backupName, ":", "-", -1)
	cbi := &dynamodb.CreateBackupInput{
		BackupName: aws.String(backupName),
		TableName:  aws.String(request.TableName),
	}
	_, err := dynamoClient.CreateBackup(cbi)
	if err != nil {
		return err
	}

	lbi := &dynamodb.ListBackupsInput{
		TableName: aws.String(request.TableName),
	}
	lbo, err := dynamoClient.ListBackups(lbi)
	if err != nil {
		return err
	}
	if lbo == nil {
		return errors.New("dynamo returned nil response")
	}

	backupsForTimeUnit := make([]dynamodb.BackupSummary, 0)
	for _, backup := range lbo.BackupSummaries {
		if backup != nil && backup.BackupName != nil && backup.BackupCreationDateTime != nil {
			backupName := *backup.BackupName
			backupName = strings.TrimPrefix(backupName, request.TableName+"_")
			backupNameParts := strings.Split(backupName, "_")
			if len(backupNameParts) > 0 {
				backupTimeUnit := backupNameParts[0]
				if backupTimeUnit == string(request.TimeUnit) {
					backupsForTimeUnit = append(backupsForTimeUnit, *backup)
				}
			}
		}
	}

	sort.Slice(backupsForTimeUnit, func(i, j int) bool {
		return backupsForTimeUnit[i].BackupCreationDateTime.After(*backupsForTimeUnit[j].BackupCreationDateTime)
	})

	for i, backupToDelete := range backupsForTimeUnit {
		if i >= request.NumberToKeep {
			dbi := &dynamodb.DeleteBackupInput{
				BackupArn: backupToDelete.BackupArn,
			}
			_, err := dynamoClient.DeleteBackup(dbi)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func Handler(request Request) error {
	region := defaultRegion
	if string_utils.NotBlank(request.Region) {
		region = request.Region
	}

	awsConfig := &aws.Config{
		Region:              aws.String(region),
		STSRegionalEndpoint: endpoints.RegionalSTSEndpoint,
	}

	sess, err := session.NewSession(awsConfig)
	if err != nil {
		return err
	}

	dynamoClient := dynamodb.New(sess, awsConfig)
	return ProcessBackupRequest(request, dynamoClient)
}

func main() {
	lambda.Start(Handler)
}
