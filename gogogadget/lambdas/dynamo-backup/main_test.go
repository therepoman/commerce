package main

import (
	"errors"
	"testing"
	"time"

	dynamodbiface_mock "code.justin.tv/commerce/gogogadget/mocks/github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestProcessBackupRequest(t *testing.T) {
	Convey("Given a dynamodb mock", t, func() {
		dynamoClient := new(dynamodbiface_mock.DynamoDBAPI)

		Convey("Errors when table name is blank", func() {
			err := ProcessBackupRequest(Request{
				TableName:    "",
				TimeUnit:     "day",
				NumberToKeep: 1,
			}, dynamoClient)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when the time unit is blank", func() {
			err := ProcessBackupRequest(Request{
				TableName:    "test-table",
				TimeUnit:     "",
				NumberToKeep: 1,
			}, dynamoClient)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when the time unit is invalid", func() {
			err := ProcessBackupRequest(Request{
				TableName:    "test-table",
				TimeUnit:     "not-valid",
				NumberToKeep: 1,
			}, dynamoClient)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when number to keep is negative", func() {
			err := ProcessBackupRequest(Request{
				TableName:    "test-table",
				TimeUnit:     "not-valid",
				NumberToKeep: -1,
			}, dynamoClient)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when number to keep is zero", func() {
			err := ProcessBackupRequest(Request{
				TableName:    "test-table",
				TimeUnit:     "not-valid",
				NumberToKeep: 0,
			}, dynamoClient)
			So(err, ShouldNotBeNil)
		})

		Convey("Given a valid request", func() {
			tableName := "test-table"
			timeUnit := "hour"
			numberToKeep := 1
			req := Request{
				TableName:    tableName,
				TimeUnit:     TimeUnit(timeUnit),
				NumberToKeep: numberToKeep,
			}

			Convey("Errors when create backup errors", func() {
				dynamoClient.On("CreateBackup", mock.Anything).Return(nil, errors.New("test error"))
				err := ProcessBackupRequest(req, dynamoClient)
				So(err, ShouldNotBeNil)
			})

			Convey("When create backup succeeds", func() {
				dynamoClient.On("CreateBackup", mock.Anything).Return(nil, nil)

				Convey("Errors when list backups errors", func() {
					dynamoClient.On("ListBackups", mock.Anything).Return(nil, errors.New("test error"))
					err := ProcessBackupRequest(req, dynamoClient)
					So(err, ShouldNotBeNil)
				})

				Convey("Errors when list backups returns nil", func() {
					dynamoClient.On("ListBackups", mock.Anything).Return(nil, nil)
					err := ProcessBackupRequest(req, dynamoClient)
					So(err, ShouldNotBeNil)
				})

				Convey("Succeeds without deleting when list backups returns empty array", func() {
					dynamoClient.On("ListBackups", mock.Anything).Return(&dynamodb.ListBackupsOutput{
						BackupSummaries: []*dynamodb.BackupSummary{},
					}, nil)
					err := ProcessBackupRequest(req, dynamoClient)
					So(err, ShouldBeNil)
					dynamoClient.AssertNotCalled(t, "DeleteBackup", mock.Anything)
				})

				Convey("Errors when delete backup errors", func() {
					dynamoClient.On("ListBackups", mock.Anything).Return(&dynamodb.ListBackupsOutput{
						BackupSummaries: []*dynamodb.BackupSummary{
							{BackupName: aws.String(tableName + "_hour_backup_1"), BackupCreationDateTime: aws.Time(time.Now())},
							{BackupName: aws.String(tableName + "_hour_backup_2"), BackupCreationDateTime: aws.Time(time.Now())},
						},
					}, nil)
					dynamoClient.On("DeleteBackup", mock.Anything).Return(nil, errors.New("test error"))
					err := ProcessBackupRequest(req, dynamoClient)
					So(err, ShouldNotBeNil)
				})

				Convey("Only deletes backups with the same time unit", func() {
					dynamoClient.On("ListBackups", mock.Anything).Return(&dynamodb.ListBackupsOutput{
						BackupSummaries: []*dynamodb.BackupSummary{
							{BackupName: aws.String(tableName + "_hour_backup_1"), BackupCreationDateTime: aws.Time(time.Now())},
							{BackupName: aws.String(tableName + "_hour_backup_2"), BackupCreationDateTime: aws.Time(time.Now())},
							{BackupName: aws.String(tableName + "_day_backup_1"), BackupCreationDateTime: aws.Time(time.Now())},
							{BackupName: aws.String(tableName + "_day_backup_2"), BackupCreationDateTime: aws.Time(time.Now())},
							{BackupName: aws.String(tableName + "_hour_backup_3"), BackupCreationDateTime: aws.Time(time.Now())},
							{BackupName: aws.String(tableName + "_hour_backup_4"), BackupCreationDateTime: aws.Time(time.Now())},
						},
					}, nil)
					dynamoClient.On("DeleteBackup", mock.Anything).Return(nil, nil)
					err := ProcessBackupRequest(req, dynamoClient)
					So(err, ShouldBeNil)
					dynamoClient.AssertNumberOfCalls(t, "DeleteBackup", 3)
				})

				Convey("Keeps the proper number of backups around", func() {
					dynamoClient.On("ListBackups", mock.Anything).Return(&dynamodb.ListBackupsOutput{
						BackupSummaries: []*dynamodb.BackupSummary{
							{BackupName: aws.String(tableName + "_hour_backup_1"), BackupCreationDateTime: aws.Time(time.Now())},
							{BackupName: aws.String(tableName + "_hour_backup_2"), BackupCreationDateTime: aws.Time(time.Now())},
							{BackupName: aws.String(tableName + "_hour_backup_3"), BackupCreationDateTime: aws.Time(time.Now())},
							{BackupName: aws.String(tableName + "_hour_backup_4"), BackupCreationDateTime: aws.Time(time.Now())},
						},
					}, nil)
					dynamoClient.On("DeleteBackup", mock.Anything).Return(nil, nil)
					req.NumberToKeep = 3
					err := ProcessBackupRequest(req, dynamoClient)
					So(err, ShouldBeNil)
					dynamoClient.AssertNumberOfCalls(t, "DeleteBackup", 1)
				})

				Convey("Deletes the oldest backups", func() {
					dynamoClient.On("ListBackups", mock.Anything).Return(&dynamodb.ListBackupsOutput{
						BackupSummaries: []*dynamodb.BackupSummary{
							{
								BackupName:             aws.String(tableName + "_hour_backup_1"),
								BackupCreationDateTime: aws.Time(time.Now().Add(-3 * time.Hour)),
								BackupArn:              aws.String("delete"),
							},
							{
								BackupName:             aws.String(tableName + "_hour_backup_2"),
								BackupCreationDateTime: aws.Time(time.Now()),
								BackupArn:              aws.String("keep"),
							},
							{
								BackupName:             aws.String(tableName + "_hour_backup_3"),
								BackupCreationDateTime: aws.Time(time.Now().Add(-2 * time.Hour)),
								BackupArn:              aws.String("delete"),
							},
							{
								BackupName:             aws.String(tableName + "_hour_backup_4"),
								BackupCreationDateTime: aws.Time(time.Now().Add(-1 * time.Hour)),
								BackupArn:              aws.String("keep"),
							},
						},
					}, nil)
					dynamoClient.On("DeleteBackup", mock.Anything).Return(nil, nil)
					req.NumberToKeep = 2
					err := ProcessBackupRequest(req, dynamoClient)
					So(err, ShouldBeNil)
					dynamoClient.AssertNumberOfCalls(t, "DeleteBackup", 2)

					firstDeleteInput, ok := dynamoClient.Calls[2].Arguments.Get(0).(*dynamodb.DeleteBackupInput)
					So(ok, ShouldBeTrue)
					So(*firstDeleteInput.BackupArn, ShouldEqual, "delete")

					secondDeleteInput, ok := dynamoClient.Calls[3].Arguments.Get(0).(*dynamodb.DeleteBackupInput)
					So(ok, ShouldBeTrue)
					So(*secondDeleteInput.BackupArn, ShouldEqual, "delete")
				})
			})
		})

	})
}
