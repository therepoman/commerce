package main

import (
	"bytes"
	"context"
	"errors"
	"io/ioutil"
	"net/http"

	"code.justin.tv/commerce/gogogadget/aws/s3"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
)

var awsConfig = &aws.Config{
	Region:              aws.String("us-west-2"),
	STSRegionalEndpoint: endpoints.RegionalSTSEndpoint,
}

type Request struct {
	SourceFileURL     string `json:"source_file_url"`
	DestinationBucket string `json:"destination_bucket"`
	DestinationKey    string `json:"destination_key"`
}

func DownloadFileToS3(s3Client s3.Client, req Request) error {
	ctx := context.Background()

	sourceFileURL := req.SourceFileURL
	resp, err := http.Get(sourceFileURL)
	if err != nil {
		return err
	}

	if resp == nil {
		return errors.New("received a nil response")
	}

	file, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	fileReader := bytes.NewReader(file)
	err = s3Client.PutFile(ctx, req.DestinationBucket, req.DestinationKey, fileReader)
	if err != nil {
		return err
	}

	return nil
}

func Handler(req Request) error {
	sess, err := session.NewSession(awsConfig)
	if err != nil {
		return err
	}

	s3Client := s3.NewClientFromSession(sess, awsConfig)
	return DownloadFileToS3(s3Client, req)
}

func main() {
	lambda.Start(Handler)
}
