# Redis Backup Lambda

## Redis Clusters and Nodes
Redis uses the term "cluster" regardless of whether or not cluster-mode is enabled.
Basically, cluster mode refers to redis environments with more than one shard.
Redis clusters that are not in cluster mode consist of only one single shard.
All redis shards can have multiple nodes (one primary and n readonly replicas) regardless of their cluster mode status.

This naming paradigm is particularly confusing, so see the [AWS Documentation](https://docs.aws.amazon.com/AmazonElastiCache/latest/red-ug/Replication.Redis-RedisCluster.html) for clarification.

**Important:**
This lambda is designed to backup the primary node of a redis cluster that has redis cluster mode DISABLED.

## Lambda Behavior
This lambda accepts a JSON request in the form of:
```json
{
  "cluster_name" : "some-redis-cluster",
  "time_unit" : "hour",
  "number_to_keep" : 5
}
```
Triggering the lambda with this input will create a redis backup for the primary node of the redis cluster.

## Building the Deploy Target
Simply run the build-deploy-target.sh shell script.

```shell
./build-deploy-target.sh
```

This creates a deployment.zip file which can be uploaded to your lambda.

## Automatic Scheduled Backups
To automatically backup your redis clusters, setup scheduled cloudwatch events to trigger this lambda at a set interval.
You will need to configure one cloudwatch even per redis cluster with a different JSON payload.
