package main

import (
	"errors"
	"fmt"
	"math/rand"
	"sort"
	"strings"
	"time"

	string_utils "code.justin.tv/commerce/gogogadget/strings"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/elasticache"
	"github.com/aws/aws-sdk-go/service/elasticache/elasticacheiface"
)

type TimeUnit string

const (
	Hour  = TimeUnit("hour")
	Day   = TimeUnit("day")
	Week  = TimeUnit("week")
	Month = TimeUnit("month")
)

var timeUnits = map[TimeUnit]interface{}{
	Hour:  nil,
	Day:   nil,
	Week:  nil,
	Month: nil,
}

func (t TimeUnit) IsValid() bool {
	_, ok := timeUnits[t]
	return ok
}

type Request struct {
	ClusterName  string   `json:"cluster_name"`
	TimeUnit     TimeUnit `json:"time_unit"`
	NumberToKeep int      `json:"number_to_keep"`
}

var awsConfig = &aws.Config{
	Region:              aws.String("us-west-2"),
	STSRegionalEndpoint: endpoints.RegionalSTSEndpoint,
}

func getNodes(request Request, ecClient elasticacheiface.ElastiCacheAPI) (nodeToBackup string, allNodes []string, err error) {
	drgo, err := ecClient.DescribeReplicationGroups(&elasticache.DescribeReplicationGroupsInput{
		ReplicationGroupId: aws.String(request.ClusterName),
	})
	if err != nil {
		return "", []string{}, err
	}

	if drgo == nil {
		return "", []string{}, errors.New("elasticache returned nil response")
	}

	if len(drgo.ReplicationGroups) != 1 {
		return "", []string{}, errors.New("elasticache did not return exactly one replication group")
	}

	rg := drgo.ReplicationGroups[0]
	if rg == nil {
		return "", []string{}, errors.New("elasticache returned nil replication group")
	}

	if len(rg.NodeGroups) != 1 {
		return "", []string{}, errors.New("elasticache did not return exactly one node group")
	}

	ng := rg.NodeGroups[0]
	if ng == nil {
		return "", []string{}, errors.New("elasticache returned nil node group")
	}

	allNodes = make([]string, 0)
	replicaNodes := make([]string, 0)
	for _, ngm := range ng.NodeGroupMembers {
		if ngm == nil || ngm.CacheClusterId == nil || ngm.CurrentRole == nil {
			continue
		}

		if *ngm.CurrentRole == "replica" {
			replicaNodes = append(replicaNodes, *ngm.CacheClusterId)
		}
		allNodes = append(allNodes, *ngm.CacheClusterId)
	}

	if len(replicaNodes) <= 0 {
		return "", []string{}, errors.New("elasticache node group did not contain a replica node")
	}

	// We backup a randomly chosen replica node
	nodeToBackup = replicaNodes[rand.Int31n(int32(len(replicaNodes)))]
	return nodeToBackup, allNodes, nil
}

func ProcessBackupRequest(request Request, ecClient elasticacheiface.ElastiCacheAPI) error {
	rand.Seed(time.Now().UnixNano())

	if string_utils.Blank(request.ClusterName) {
		return errors.New("cluster name cannot be blank")
	}

	if !request.TimeUnit.IsValid() {
		return errors.New(fmt.Sprintf("invalid timeunit: %v", request.TimeUnit))
	}

	if request.NumberToKeep <= 0 {
		return errors.New("number to keep must be a positive integer")
	}

	backupName := fmt.Sprintf("%s-%s-backup-%s", request.ClusterName, request.TimeUnit, time.Now().Format(time.RFC3339))
	backupName = strings.Replace(backupName, ":", "-", -1)

	// Returns both a replica node to backup and a list of all nodes in the cluster
	nodeToBackup, allNodes, err := getNodes(request, ecClient)
	if err != nil {
		return err
	}

	// Create a new backup
	_, err = ecClient.CreateSnapshot(&elasticache.CreateSnapshotInput{
		CacheClusterId: aws.String(nodeToBackup),
		SnapshotName:   aws.String(backupName),
	})
	if err != nil {
		return err
	}

	// Get all backups across all nodes in the cluster (primary and replica nodes can swap behind the scenes)
	allNodeSnapshots := make([]*elasticache.Snapshot, 0)
	for _, node := range allNodes {
		dso, err := ecClient.DescribeSnapshots(&elasticache.DescribeSnapshotsInput{
			CacheClusterId: aws.String(node),
		})
		if err != nil {
			return err
		}
		if dso == nil {
			return errors.New("elasticache returned nil describe snapshots response")
		}
		allNodeSnapshots = append(allNodeSnapshots, dso.Snapshots...)
	}

	// Filter the backups down to only those of this time unit
	snapshotsForTimeUnit := make([]elasticache.Snapshot, 0)
	for _, snapshot := range allNodeSnapshots {
		if snapshot != nil && snapshot.SnapshotName != nil && len(snapshot.NodeSnapshots) == 1 {
			nodeSnapshot := snapshot.NodeSnapshots[0]
			if nodeSnapshot == nil || nodeSnapshot.SnapshotCreateTime == nil {
				continue
			}

			backupName := *snapshot.SnapshotName
			backupName = strings.TrimPrefix(backupName, request.ClusterName+"-")
			firstDashIndex := strings.Index(backupName, "-")
			if firstDashIndex == -1 {
				continue
			}
			backupTimeUnit := backupName[:firstDashIndex]
			if backupTimeUnit == string(request.TimeUnit) {
				snapshotsForTimeUnit = append(snapshotsForTimeUnit, *snapshot)
			}
		}
	}

	// Sort the backups by creation time
	sort.Slice(snapshotsForTimeUnit, func(i, j int) bool {
		return snapshotsForTimeUnit[i].NodeSnapshots[0].SnapshotCreateTime.After(*snapshotsForTimeUnit[j].NodeSnapshots[0].SnapshotCreateTime)
	})

	// Delete the oldest backups
	for i, backupToDelete := range snapshotsForTimeUnit {
		if i >= request.NumberToKeep {
			_, err = ecClient.DeleteSnapshot(&elasticache.DeleteSnapshotInput{
				SnapshotName: aws.String(*backupToDelete.SnapshotName),
			})
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func Handler(request Request) error {
	sess, err := session.NewSession(awsConfig)
	if err != nil {
		return err
	}

	ec := elasticache.New(sess, awsConfig)
	return ProcessBackupRequest(request, ec)
}

func main() {
	lambda.Start(Handler)
}
