package main

import (
	"errors"
	"testing"
	"time"

	elasticacheiface_mock "code.justin.tv/commerce/gogogadget/mocks/github.com/aws/aws-sdk-go/service/elasticache/elasticacheiface"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/elasticache"
	"github.com/brildum/testify/mock"
	. "github.com/smartystreets/goconvey/convey"
)

func TestProcessBackupRequest(t *testing.T) {
	Convey("Given an elasticache mock", t, func() {
		ecClient := new(elasticacheiface_mock.ElastiCacheAPI)

		Convey("Errors when cluster name is blank", func() {
			err := ProcessBackupRequest(Request{
				ClusterName:  "",
				TimeUnit:     "day",
				NumberToKeep: 1,
			}, ecClient)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when time unit is blank", func() {
			err := ProcessBackupRequest(Request{
				ClusterName:  "test-cluster",
				TimeUnit:     "",
				NumberToKeep: 1,
			}, ecClient)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when time unit is invalid", func() {
			err := ProcessBackupRequest(Request{
				ClusterName:  "test-cluster",
				TimeUnit:     "not-valid",
				NumberToKeep: 1,
			}, ecClient)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when number to keep is negative", func() {
			err := ProcessBackupRequest(Request{
				ClusterName:  "test-cluster",
				TimeUnit:     "day",
				NumberToKeep: -1,
			}, ecClient)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when number to keep is zero", func() {
			err := ProcessBackupRequest(Request{
				ClusterName:  "test-cluster",
				TimeUnit:     "day",
				NumberToKeep: 0,
			}, ecClient)
			So(err, ShouldNotBeNil)
		})

		Convey("Given a valid request", func() {
			req := Request{
				ClusterName:  "test-cluster",
				TimeUnit:     "day",
				NumberToKeep: 1,
			}

			Convey("Errors when DescribeReplicationGroups call errors", func() {
				ecClient.On("DescribeReplicationGroups", mock.Anything).Return(nil, errors.New("test error"))
				err := ProcessBackupRequest(req, ecClient)
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when DescribeReplicationGroups call returns nil", func() {
				ecClient.On("DescribeReplicationGroups", mock.Anything).Return(nil, nil)
				err := ProcessBackupRequest(req, ecClient)
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when DescribeReplicationGroups call returns zero replication groups", func() {
				ecClient.On("DescribeReplicationGroups", mock.Anything).Return(&elasticache.DescribeReplicationGroupsOutput{
					ReplicationGroups: []*elasticache.ReplicationGroup{},
				}, nil)
				err := ProcessBackupRequest(req, ecClient)
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when DescribeReplicationGroups call returns more than one replication groups", func() {
				ecClient.On("DescribeReplicationGroups", mock.Anything).Return(&elasticache.DescribeReplicationGroupsOutput{
					ReplicationGroups: []*elasticache.ReplicationGroup{
						{},
						{},
					},
				}, nil)
				err := ProcessBackupRequest(req, ecClient)
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when DescribeReplicationGroups call returns one nil replication groups", func() {
				ecClient.On("DescribeReplicationGroups", mock.Anything).Return(&elasticache.DescribeReplicationGroupsOutput{
					ReplicationGroups: []*elasticache.ReplicationGroup{
						nil,
					},
				}, nil)
				err := ProcessBackupRequest(req, ecClient)
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when replication group has zero node groups", func() {
				ecClient.On("DescribeReplicationGroups", mock.Anything).Return(&elasticache.DescribeReplicationGroupsOutput{
					ReplicationGroups: []*elasticache.ReplicationGroup{
						{
							NodeGroups: []*elasticache.NodeGroup{},
						},
					},
				}, nil)
				err := ProcessBackupRequest(req, ecClient)
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when replication group has more than one node group", func() {
				ecClient.On("DescribeReplicationGroups", mock.Anything).Return(&elasticache.DescribeReplicationGroupsOutput{
					ReplicationGroups: []*elasticache.ReplicationGroup{
						{
							NodeGroups: []*elasticache.NodeGroup{
								{},
								{},
							},
						},
					},
				}, nil)
				err := ProcessBackupRequest(req, ecClient)
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when replication group has one nil node group", func() {
				ecClient.On("DescribeReplicationGroups", mock.Anything).Return(&elasticache.DescribeReplicationGroupsOutput{
					ReplicationGroups: []*elasticache.ReplicationGroup{
						{
							NodeGroups: []*elasticache.NodeGroup{
								nil,
							},
						},
					},
				}, nil)
				err := ProcessBackupRequest(req, ecClient)
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when node group has empty members array", func() {
				ecClient.On("DescribeReplicationGroups", mock.Anything).Return(&elasticache.DescribeReplicationGroupsOutput{
					ReplicationGroups: []*elasticache.ReplicationGroup{
						{
							NodeGroups: []*elasticache.NodeGroup{
								{
									NodeGroupMembers: []*elasticache.NodeGroupMember{},
								},
							},
						},
					},
				}, nil)
				err := ProcessBackupRequest(req, ecClient)
				So(err, ShouldNotBeNil)
			})

			Convey("Errors when node group has no replica node", func() {
				ecClient.On("DescribeReplicationGroups", mock.Anything).Return(&elasticache.DescribeReplicationGroupsOutput{
					ReplicationGroups: []*elasticache.ReplicationGroup{
						{
							NodeGroups: []*elasticache.NodeGroup{
								{
									NodeGroupMembers: []*elasticache.NodeGroupMember{
										{
											CacheClusterId: aws.String("test-cluster-id"),
											CurrentRole:    aws.String("primary"),
										},
									},
								},
							},
						},
					},
				}, nil)
				err := ProcessBackupRequest(req, ecClient)
				So(err, ShouldNotBeNil)
			})

			Convey("When DescribeReplicationGroups returns one replica node", func() {
				clusterID := "test-cluster-id"
				ecClient.On("DescribeReplicationGroups", mock.Anything).Return(&elasticache.DescribeReplicationGroupsOutput{
					ReplicationGroups: []*elasticache.ReplicationGroup{
						{
							NodeGroups: []*elasticache.NodeGroup{
								{
									NodeGroupMembers: []*elasticache.NodeGroupMember{
										{
											CacheClusterId: aws.String(clusterID),
											CurrentRole:    aws.String("replica"),
										},
									},
								},
							},
						},
					},
				}, nil)

				Convey("Errors when CreateSnapshot errors", func() {
					ecClient.On("CreateSnapshot", mock.Anything).Return(nil, errors.New("test error"))
					err := ProcessBackupRequest(req, ecClient)
					So(err, ShouldNotBeNil)
				})

				Convey("When CreateSnapshot succeeds", func() {
					ecClient.On("CreateSnapshot", mock.Anything).Return(nil, nil)

					Convey("Errors when DescribeSnapshots errors", func() {
						ecClient.On("DescribeSnapshots", mock.Anything).Return(nil, errors.New("test error"))
						err := ProcessBackupRequest(req, ecClient)
						So(err, ShouldNotBeNil)
					})

					Convey("Errors when DescribeSnapshots returns nil response", func() {
						ecClient.On("DescribeSnapshots", mock.Anything).Return(nil, nil)
						err := ProcessBackupRequest(req, ecClient)
						So(err, ShouldNotBeNil)
					})

					Convey("Succeeds without deleting when DescribeSnapshots returns no snapshots", func() {
						ecClient.On("DescribeSnapshots", mock.Anything).Return(&elasticache.DescribeSnapshotsOutput{
							Snapshots: []*elasticache.Snapshot{},
						}, nil)
						err := ProcessBackupRequest(req, ecClient)
						So(err, ShouldBeNil)
						ecClient.AssertNotCalled(t, "DeleteSnapshot", mock.Anything)

						csi, ok := ecClient.Calls[1].Arguments.Get(0).(*elasticache.CreateSnapshotInput)
						So(ok, ShouldBeTrue)
						So(*csi.CacheClusterId, ShouldEqual, clusterID)
					})

					Convey("Only deletes backups with the same time unit", func() {
						ecClient.On("DescribeSnapshots", mock.Anything).Return(&elasticache.DescribeSnapshotsOutput{
							Snapshots: []*elasticache.Snapshot{
								{SnapshotName: aws.String(clusterID + "-hour-backup-1"), NodeSnapshots: []*elasticache.NodeSnapshot{{SnapshotCreateTime: aws.Time(time.Now())}}},
								{SnapshotName: aws.String(clusterID + "-hour-backup-2"), NodeSnapshots: []*elasticache.NodeSnapshot{{SnapshotCreateTime: aws.Time(time.Now())}}},
								{SnapshotName: aws.String(clusterID + "-day-backup-1"), NodeSnapshots: []*elasticache.NodeSnapshot{{SnapshotCreateTime: aws.Time(time.Now())}}},
								{SnapshotName: aws.String(clusterID + "-day-backup-2"), NodeSnapshots: []*elasticache.NodeSnapshot{{SnapshotCreateTime: aws.Time(time.Now())}}},
								{SnapshotName: aws.String(clusterID + "-hour-backup-3"), NodeSnapshots: []*elasticache.NodeSnapshot{{SnapshotCreateTime: aws.Time(time.Now())}}},
								{SnapshotName: aws.String(clusterID + "-hour-backup-4"), NodeSnapshots: []*elasticache.NodeSnapshot{{SnapshotCreateTime: aws.Time(time.Now())}}},
							},
						}, nil)

						req.TimeUnit = "hour"
						req.NumberToKeep = 1
						req.ClusterName = clusterID

						ecClient.On("DeleteSnapshot", mock.Anything).Return(nil, nil)
						err := ProcessBackupRequest(req, ecClient)
						So(err, ShouldBeNil)
						ecClient.AssertNumberOfCalls(t, "DeleteSnapshot", 3)
					})

					Convey("Keeps the proper number of backups around", func() {
						ecClient.On("DescribeSnapshots", mock.Anything).Return(&elasticache.DescribeSnapshotsOutput{
							Snapshots: []*elasticache.Snapshot{
								{SnapshotName: aws.String(clusterID + "-hour-backup-1"), NodeSnapshots: []*elasticache.NodeSnapshot{{SnapshotCreateTime: aws.Time(time.Now())}}},
								{SnapshotName: aws.String(clusterID + "-hour-backup-2"), NodeSnapshots: []*elasticache.NodeSnapshot{{SnapshotCreateTime: aws.Time(time.Now())}}},
								{SnapshotName: aws.String(clusterID + "-hour-backup-3"), NodeSnapshots: []*elasticache.NodeSnapshot{{SnapshotCreateTime: aws.Time(time.Now())}}},
								{SnapshotName: aws.String(clusterID + "-hour-backup-4"), NodeSnapshots: []*elasticache.NodeSnapshot{{SnapshotCreateTime: aws.Time(time.Now())}}},
							},
						}, nil)

						req.TimeUnit = "hour"
						req.NumberToKeep = 1
						req.ClusterName = clusterID

						ecClient.On("DeleteSnapshot", mock.Anything).Return(nil, nil)
						err := ProcessBackupRequest(req, ecClient)
						So(err, ShouldBeNil)
						ecClient.AssertNumberOfCalls(t, "DeleteSnapshot", 3)
					})

					Convey("Deletes the oldest backups", func() {
						ecClient.On("DescribeSnapshots", mock.Anything).Return(&elasticache.DescribeSnapshotsOutput{
							Snapshots: []*elasticache.Snapshot{
								{SnapshotName: aws.String(clusterID + "-hour-backup-1-delete"), NodeSnapshots: []*elasticache.NodeSnapshot{{SnapshotCreateTime: aws.Time(time.Now().Add(-3 * time.Hour))}}},
								{SnapshotName: aws.String(clusterID + "-hour-backup-2-keep"), NodeSnapshots: []*elasticache.NodeSnapshot{{SnapshotCreateTime: aws.Time(time.Now())}}},
								{SnapshotName: aws.String(clusterID + "-hour-backup-3-delete"), NodeSnapshots: []*elasticache.NodeSnapshot{{SnapshotCreateTime: aws.Time(time.Now().Add(-2 * time.Hour))}}},
								{SnapshotName: aws.String(clusterID + "-hour-backup-4-keep"), NodeSnapshots: []*elasticache.NodeSnapshot{{SnapshotCreateTime: aws.Time(time.Now().Add(-1 * time.Hour))}}},
							},
						}, nil)

						req.TimeUnit = "hour"
						req.NumberToKeep = 2
						req.ClusterName = clusterID

						ecClient.On("DeleteSnapshot", mock.Anything).Return(nil, nil)

						err := ProcessBackupRequest(req, ecClient)
						So(err, ShouldBeNil)
						ecClient.AssertNumberOfCalls(t, "DeleteSnapshot", 2)

						firstDeleteInput, ok := ecClient.Calls[3].Arguments.Get(0).(*elasticache.DeleteSnapshotInput)
						So(ok, ShouldBeTrue)
						So(*firstDeleteInput.SnapshotName, ShouldEndWith, "delete")

						secondDeleteInput, ok := ecClient.Calls[4].Arguments.Get(0).(*elasticache.DeleteSnapshotInput)
						So(ok, ShouldBeTrue)
						So(*secondDeleteInput.SnapshotName, ShouldEndWith, "delete")
					})
				})
			})

			Convey("Creates the correct snapshot when DescribeReplicationGroups returns multiple nodes", func() {
				primaryClusterID := "primary-cluster-id"
				replicaClusterID := "replica-cluster-id"
				ecClient.On("DescribeReplicationGroups", mock.Anything).Return(&elasticache.DescribeReplicationGroupsOutput{
					ReplicationGroups: []*elasticache.ReplicationGroup{
						{
							NodeGroups: []*elasticache.NodeGroup{
								{
									NodeGroupMembers: []*elasticache.NodeGroupMember{
										{
											CacheClusterId: aws.String(primaryClusterID),
											CurrentRole:    aws.String("primary"),
										},
										{
											CacheClusterId: aws.String(replicaClusterID),
											CurrentRole:    aws.String("replica"),
										},
									},
								},
							},
						},
					},
				}, nil)

				ecClient.On("CreateSnapshot", mock.Anything).Return(nil, nil)
				ecClient.On("DescribeSnapshots", mock.Anything).Return(&elasticache.DescribeSnapshotsOutput{
					Snapshots: []*elasticache.Snapshot{},
				}, nil)

				err := ProcessBackupRequest(req, ecClient)
				So(err, ShouldBeNil)

				csi, ok := ecClient.Calls[1].Arguments.Get(0).(*elasticache.CreateSnapshotInput)
				So(ok, ShouldBeTrue)
				So(*csi.CacheClusterId, ShouldEqual, replicaClusterID)

				ecClient.AssertNumberOfCalls(t, "DescribeSnapshots", 2)
				dis1, ok := ecClient.Calls[2].Arguments.Get(0).(*elasticache.DescribeSnapshotsInput)
				So(ok, ShouldBeTrue)
				So(*dis1.CacheClusterId, ShouldEqual, primaryClusterID)

				dis2, ok := ecClient.Calls[3].Arguments.Get(0).(*elasticache.DescribeSnapshotsInput)
				So(ok, ShouldBeTrue)
				So(*dis2.CacheClusterId, ShouldEqual, replicaClusterID)
			})
		})
	})
}
