package strings

import (
	"testing"

	"code.justin.tv/commerce/gogogadget/pointers"
	"github.com/stretchr/testify/assert"
)

func TestReverse(t *testing.T) {
	result := Reverse("abcd")
	assert.Equal(t, "dcba", result)

	result = Reverse("racecar")
	assert.Equal(t, "racecar", result)
}

func TestBlank(t *testing.T) {
	assert.True(t, BlankP(nil))
	assert.True(t, BlankP(pointers.StringP("")))
	assert.True(t, BlankP(pointers.StringP("    ")))
	assert.True(t, BlankP(pointers.StringP("    \t \t \t \n \n \t  ")))

	assert.False(t, BlankP(pointers.StringP("A")))
	assert.False(t, BlankP(pointers.StringP("        A        ")))
}

func TestNotBlank(t *testing.T) {
	assert.False(t, NotBlankP(nil))
	assert.False(t, NotBlankP(pointers.StringP("")))
	assert.False(t, NotBlankP(pointers.StringP("    ")))
	assert.False(t, NotBlankP(pointers.StringP("    \t \t \t \n \n \t  ")))

	assert.True(t, NotBlankP(pointers.StringP("A")))
	assert.True(t, NotBlankP(pointers.StringP("        A        ")))
}

func TestSet(t *testing.T) {
	l := NewSet()
	assert.Equal(t, 0, len(l.GetList()))
	l.Add("A")
	assert.Equal(t, 1, len(l.GetList()))
	l.Add("A")
	assert.Equal(t, 1, len(l.GetList()))
	l.Add("B")
	assert.Equal(t, 2, len(l.GetList()))
	l.Add("B")
	assert.Equal(t, 2, len(l.GetList()))
	assert.True(t, l.Contains("A"))
	assert.True(t, l.Contains("B"))
}

func TestTruncate(t *testing.T) {
	assert.Equal(t, "", Trucate("", 0))
	assert.Equal(t, "", Trucate("", 1))
	assert.Equal(t, "", Trucate("", 2))

	assert.Equal(t, "", Trucate("a", 0))
	assert.Equal(t, "a", Trucate("a", 1))
	assert.Equal(t, "a", Trucate("a", 2))

	assert.Equal(t, "", Trucate("ab", 0))
	assert.Equal(t, "a", Trucate("ab", 1))
	assert.Equal(t, "ab", Trucate("ab", 2))
}

func TestCommatizeInt(t *testing.T) {
	result := CommatizeInt(1000)
	assert.Equal(t, "1,000", result)

	result = CommatizeInt(10000)
	assert.Equal(t, "10,000", result)

	result = CommatizeInt(100000)
	assert.Equal(t, "100,000", result)

	result = CommatizeInt(1000000)
	assert.Equal(t, "1,000,000", result)
}

func TestSetFromArray(t *testing.T) {
	a := []string{"A", "B", "C"}
	l := NewSetFromArray(a)
	assert.Equal(t, 3, len(l.GetList()))
	assert.True(t, l.Contains("A"))
}

func TestIsInt32Parsable(t *testing.T) {
	result := IsInt32Parsable("not an int")
	assert.False(t, result)

	result = IsInt32Parsable("10")
	assert.True(t, result)
}

func TestIsInt64Parsable(t *testing.T) {
	result := IsInt64Parsable("not an int")
	assert.False(t, result)

	result = IsInt64Parsable("100000")
	assert.True(t, result)
}

func TestIsFloatParsable(t *testing.T) {
	result := IsFloatParsable("not a float")
	assert.False(t, result)

	result = IsFloatParsable("1.2312321")
	assert.True(t, result)
}

func TestDedupe(t *testing.T) {
	expected := []string{"tuna", "salmon", "egg"}
	result := Dedupe([]string{"tuna", "salmon", "tuna", "egg", "salmon", "egg"})
	assert.Equal(t, expected, result)
}

func TestRemoveEmptyStrings(t *testing.T) {
	expected := []string{"cub", "den", "wolf"}
	result := RemoveEmptyStrings([]string{"cub", "den", "", " ", "wolf", ""})
	assert.Equal(t, expected, result)
}

func TestUSDPriceStringToCents(t *testing.T) {
	var err error
	var cents int64

	// Invalid input cases
	_, err = USDPriceStringToCents("")
	assert.Error(t, err)

	_, err = USDPriceStringToCents("abc")
	assert.Error(t, err)

	_, err = USDPriceStringToCents("ab.12")
	assert.Error(t, err)

	_, err = USDPriceStringToCents("$123.45")
	assert.Error(t, err)

	_, err = USDPriceStringToCents("123,456.78")
	assert.Error(t, err)

	_, err = USDPriceStringToCents("12.345")
	assert.Error(t, err)

	// Valid input cases
	cents, err = USDPriceStringToCents("1.23")
	assert.NoError(t, err)
	assert.Equal(t, int64(123), cents)

	cents, err = USDPriceStringToCents("  2.00  ")
	assert.NoError(t, err)
	assert.Equal(t, int64(200), cents)

	cents, err = USDPriceStringToCents("123456.78")
	assert.NoError(t, err)
	assert.Equal(t, int64(12345678), cents)

	cents, err = USDPriceStringToCents("7.0")
	assert.NoError(t, err)
	assert.Equal(t, int64(700), cents)

	cents, err = USDPriceStringToCents("    7.0   ")
	assert.NoError(t, err)
	assert.Equal(t, int64(700), cents)

	cents, err = USDPriceStringToCents("14.0")
	assert.NoError(t, err)
	assert.Equal(t, int64(1400), cents)

	cents, err = USDPriceStringToCents("3.1")
	assert.NoError(t, err)
	assert.Equal(t, int64(310), cents)

	cents, err = USDPriceStringToCents("3.10")
	assert.NoError(t, err)
	assert.Equal(t, int64(310), cents)
}

func TestContainsHiddenCharacters(t *testing.T) {
	assert.Equal(t, false, ContainsHiddenCharacters("cheer1"))
	assert.Equal(t, false, ContainsHiddenCharacters("cheer1 trihard9\n"))
	assert.Equal(t, false, ContainsHiddenCharacters("\tcheer1"))
	assert.Equal(t, false, ContainsHiddenCharacters("cheer1, TRihard9!"))
	assert.Equal(t, true, ContainsHiddenCharacters("trihard​9"))                     // hidden zero width space (U+200B) character between 'd' and '9'
	assert.Equal(t, true, ContainsHiddenCharacters("PogChamp1 trihard​9999999999​")) // hidden zero width space (U+200B) character between 'd' and '9', and at the end of the string
	assert.Equal(t, true, ContainsHiddenCharacters("cheer1 trihard9​\n"))            // hidden zero width space (U+200B) character between '9' and '\n'
}
