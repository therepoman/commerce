package random

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestIntFromStringSeed(t *testing.T) {
	Convey("Tests that various strings deterministically produce the same int", t, func() {
		val, _ := IntFromStringSeed("twitch.tv/michael", 10)
		So(val, ShouldEqual, 1)

		val, _ = IntFromStringSeed("twitch.tv/michael", 3)
		So(val, ShouldEqual, 1)

		val, _ = IntFromStringSeed("aloha", 1000)
		So(val, ShouldEqual, 636)

		val, _ = IntFromStringSeed("aloha aloha", 100)
		So(val, ShouldEqual, 67)

		val, _ = IntFromStringSeed("xxmoments4lyfexx", 8)
		So(val, ShouldEqual, 5)
	})
}
