package ttlcache

import (
	"errors"
	"math/rand"
	"reflect"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	numItems = 50
)

type data struct {
	garbage map[string]string
}

func makeGarbageData() data {
	garbage := make(map[string]string)
	for i := 0; i < 1000; i++ {
		garbage[random.String(32)] = random.String(32)
	}
	return data{
		garbage: garbage,
	}
}

func TestCacheMultiItemExpiration(t *testing.T) {
	Convey("Test multi item cache expiration", t, func() {
		rand.Seed(time.Now().UnixNano())

		c := NewCache(50 * time.Millisecond)

		for i := 0; i < numItems; i++ {
			c.Set(random.String(32), makeGarbageData(), random.Duration(100*time.Millisecond, 500*time.Millisecond))
		}

		cacheLen := c.Len()
		So(cacheLen, ShouldEqual, numItems)

		startTime := time.Now()
		for {
			newCacheLen := c.Len()
			So(newCacheLen, ShouldBeLessThanOrEqualTo, cacheLen)
			cacheLen = newCacheLen

			if cacheLen == 0 {
				break
			}

			if time.Since(startTime) > 600*time.Millisecond {
				So(errors.New("timed out waiting for cache elements to expire"), ShouldBeNil)
				break
			}

			time.Sleep(10 * time.Millisecond)
		}

		So(c.Len(), ShouldEqual, 0)
	})
}

func TestCacheExpiration(t *testing.T) {
	Convey("Test single item cache expiration", t, func() {
		rand.Seed(time.Now().UnixNano())

		startTime := time.Now()
		c := NewCache(50 * time.Millisecond)

		k := random.String(32)
		v := makeGarbageData()
		c.Set(k, v, 200*time.Millisecond)

		So(c.Len(), ShouldEqual, 1)
		So(reflect.DeepEqual(c.Get(k), v), ShouldBeTrue)

		for {
			actualV := c.Get(k)
			if c.Len() == 0 {
				So(c.Get(k), ShouldBeNil)
				So(time.Since(startTime), ShouldBeBetween, 150*time.Millisecond, 300*time.Millisecond)
				break
			}
			So(reflect.DeepEqual(actualV, v), ShouldBeTrue)

			if time.Since(startTime) > 6*time.Second {
				So(errors.New("timed out waiting for cache element to expire"), ShouldBeNil)
				break
			}

			time.Sleep(10 * time.Millisecond)
		}

		So(c.Len(), ShouldEqual, 0)
	})
}

func TestCacheGetAll(t *testing.T) {
	Convey("Test cache get all", t, func() {
		rand.Seed(time.Now().UnixNano())

		c := NewCache(50 * time.Millisecond)
		length := 5

		expectedData := make(map[string]data)
		for i := 0; i < length; i++ {
			k := random.String(32)
			v := makeGarbageData()
			c.Set(k, v, 200*time.Millisecond)
			expectedData[k] = v
		}

		cachedAll := c.GetAll()
		actualData := make(map[string]data)
		for actualK, actualVInterface := range cachedAll {
			actualV, ok := actualVInterface.(data)
			So(ok, ShouldBeTrue)
			actualData[actualK] = actualV
		}

		So(reflect.DeepEqual(expectedData, actualData), ShouldBeTrue)
	})
}
