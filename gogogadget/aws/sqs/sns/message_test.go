package sns

import (
	"testing"

	"code.justin.tv/commerce/gogogadget/pointers"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
)

func TestExtract(t *testing.T) {
	Convey("Given an SQS message", t, func() {

		var sqsMsg *sqs.Message

		Convey("Errors when the message is nil", func() {
			sqsMsg = nil
			_, err := Extract(sqsMsg)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when the body is nil", func() {
			sqsMsg = &sqs.Message{
				Body: nil,
			}
			_, err := Extract(sqsMsg)
			So(err, ShouldNotBeNil)
		})

		Convey("Errors when the body is invalid json", func() {
			sqsMsg = &sqs.Message{
				Body: pointers.StringP("this is not valid json"),
			}
			_, err := Extract(sqsMsg)
			So(err, ShouldNotBeNil)
		})

		Convey("Succeeds when the body is valid json", func() {
			sqsMsg = &sqs.Message{
				Body: pointers.StringP("{\"Message\":\"InnerMsg\"}"),
			}
			msg, err := Extract(sqsMsg)
			So(err, ShouldBeNil)
			So(msg.Message, ShouldEqual, "InnerMsg")
		})
	})
}
