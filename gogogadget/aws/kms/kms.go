package kms

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kms"
	"github.com/aws/aws-sdk-go/service/kms/kmsiface"
)

const (
	defaultRegion = "us-west-2"
	KeySpecAES128 = KeySpec("AES_128")
	KeySpecAES256 = KeySpec("AES_256")
)

type KeySpec string

type Client interface {
	GenerateDataKey(masterKeyID string, keySpec KeySpec, encryptionContext map[string]string) (*kms.GenerateDataKeyOutput, error)
	Decrypt(cipherText []byte, encryptionContext map[string]string) (*kms.DecryptOutput, error)
}

type SDKClientWrapper struct {
	SDKClient kmsiface.KMSAPI
}

func NewDefaultKMSClient() Client {
	return &SDKClientWrapper{
		SDKClient: kms.New(session.New(), NewDefaultConfig()),
	}
}

func NewDefaultConfig() *aws.Config {
	return &aws.Config{
		Region:              aws.String(defaultRegion),
		STSRegionalEndpoint: endpoints.RegionalSTSEndpoint,
	}
}

func (c *SDKClientWrapper) GenerateDataKey(masterKeyID string, keySpec KeySpec, encryptionContext map[string]string) (*kms.GenerateDataKeyOutput, error) {
	input := &kms.GenerateDataKeyInput{
		KeyId:             aws.String(masterKeyID),
		KeySpec:           aws.String(string(keySpec)),
		EncryptionContext: aws.StringMap(encryptionContext),
	}
	out, err := c.SDKClient.GenerateDataKey(input)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *SDKClientWrapper) Decrypt(cipherText []byte, encryptionContext map[string]string) (*kms.DecryptOutput, error) {
	input := &kms.DecryptInput{
		CiphertextBlob:    cipherText,
		EncryptionContext: aws.StringMap(encryptionContext),
	}
	out, err := c.SDKClient.Decrypt(input)
	if err != nil {
		return nil, err
	}
	return out, nil
}
