package cgre

import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.TimeCharacteristic
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import config.Config


object StreamingJob {
  val mapper = JsonMapper.builder()
    .addModule(DefaultScalaModule)
    .build()

  def main(args: Array[String]): Unit = {
    // 1. Define stream environment with settings
    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    env.enableCheckpointing(60000) // 1 min
    env.getConfig.setLatencyTrackingInterval(500)

    // 2. Define a source
    val sourceStream = Kinesis.makeSource(Config.settings.inputStream, env)

    // 3. Process
    val events: DataStream[MWEvent] = sourceStream.map(x => {
      x.fields
    })

    val output: DataStream[OnlineStatus] = events.filter(x => x.filter)
      .keyBy(e => (e.userID, e.channelID))
      .process(new UserChannelOnlineStatusFunction)

    val sink = Kinesis.makeSink(Config.settings.outputStream)

    output.map(x => toJson(x)).addSink(sink).name(Config.settings.outputStream).uid("OnlineStatus")

    // 5. Execute
    env.execute("cgre")
  }

  def toJson(o: OnlineStatus): String = {
    mapper.writeValueAsString(o)
  }
}
