package cgre

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.format.DateTimeFormatter
import java.time.{ZoneId, ZonedDateTime}

object SpadeTime {
  val ZONE_ID: ZoneId = ZoneId.of("UTC")
  val DTF: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZONE_ID)
}

case class MWEvent(
                    @JsonProperty("channel_id")
                    channelID: String,
                    @JsonProperty("user_id")
                    userID: String,
                    @JsonProperty("time_utc")
                    timeUTC: String,
                  ) {
  def filter: Boolean = this.channelID != null &&
    this.timeUTC != null &&
    this.timeUTC != "" &&
    this.channelID.!=("") &&
    this.userID != null &&
    this.userID.!=("")

  def unixTime : Long = {
    ZonedDateTime.parse(timeUTC, SpadeTime.DTF).toInstant.toEpochMilli
  }
}
