package cgre

import org.slf4j.LoggerFactory

object Logger {
  val LOG = LoggerFactory.getLogger(classOf[Nothing])
}
