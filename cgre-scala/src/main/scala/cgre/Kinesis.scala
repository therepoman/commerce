package cgre

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.{DefaultScalaModule, ScalaObjectMapper}
import org.apache.flink.streaming.connectors.kinesis.{FlinkKinesisConsumer, FlinkKinesisProducer, KinesisPartitioner}
import org.apache.flink.streaming.api.scala._

import java.util.{Base64, Properties}
import org.apache.flink.streaming.connectors.kinesis.config.{AWSConfigConstants, ConsumerConfigConstants}
import config.Config
import org.apache.commons.io.IOUtils
import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.slf4j.LoggerFactory

import java.io.ByteArrayInputStream
import java.nio.charset.StandardCharsets
import java.util.zip.{Inflater, InflaterInputStream}

case class OnlineStatus(channelID: String, userID: String, isOnline: Boolean) {}

object Kinesis {
  val mapper = new ObjectMapper() with ScalaObjectMapper
  mapper.registerModule(DefaultScalaModule)
  mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

  def makeSource(streamName: String, env: StreamExecutionEnvironment): DataStream[EventRecord] = {
    val consumerConfig: Properties = new Properties()
    consumerConfig.put(AWSConfigConstants.AWS_REGION, "us-west-2")

    consumerConfig.put(ConsumerConfigConstants.STREAM_INITIAL_POSITION, "LATEST")
    consumerConfig.put(ConsumerConfigConstants.SHARD_USE_ADAPTIVE_READS, "true")

    if (Config.settings.localMode) {
      consumerConfig.put(AWSConfigConstants.AWS_PROFILE_NAME, "twitch-cgre-dev")
      consumerConfig.put(AWSConfigConstants.AWS_CREDENTIALS_PROVIDER, "PROFILE")
    }

    val consumer = new FlinkKinesisConsumer[String](streamName,
      new SimpleStringSchema(),
      consumerConfig)

    val consumerSource = env.addSource(consumer)
      .name(streamName).uid(streamName).setParallelism(Config.settings.readParallelism)

    flattenCompressedStream(consumerSource)
  }

  def makeSink(streamName: String): FlinkKinesisProducer[String] = {
    // https://github.com/awslabs/amazon-kinesis-producer/blob/master/java/amazon-kinesis-producer-sample/default_config.properties
    val kinesisProducerConfig = new Properties()
    kinesisProducerConfig.put(AWSConfigConstants.AWS_REGION, "us-west-2")
    kinesisProducerConfig.put("MetricsLevel", "summary")
    kinesisProducerConfig.put("LogLevel", "warning")
    if (Config.settings.localMode) {
      kinesisProducerConfig.put(AWSConfigConstants.AWS_PROFILE_NAME, "twitch-cgre-dev")
      kinesisProducerConfig.put(AWSConfigConstants.AWS_CREDENTIALS_PROVIDER, "PROFILE")
    }

    val kinesisProducer = new FlinkKinesisProducer[String](new SimpleStringSchema, kinesisProducerConfig)
    kinesisProducer.setFailOnError(true)
    kinesisProducer.setDefaultStream(streamName)
    kinesisProducer.setCustomPartitioner(new RandomKinesisPartitioner())
    kinesisProducer
  }

  def flattenCompressedStream[T](consumer: DataStream[String]): DataStream[EventRecord] = {
    consumer.flatMap(input => {
      try {
        val decompressed = decompress(mapper.readValue(input, classOf[KinesisRecord]).data)

        mapper.readValue[Seq[EventRecord]](decompressed)
      } catch {
        case e : Exception =>
          Logger.LOG.error("flattenCompressedStream exception {}", e)
          Seq()
      }
    })
      .name("decompress-flatten")
      .uid("decompress-flatten")
  }

  def decompress(compressed: String): String = {
    val decoder = Base64.getDecoder
    val decoded = decoder.decode(compressed.getBytes)
    val inputStream = new ByteArrayInputStream(decoded)
    inputStream.skip(1)
    val stream = new InflaterInputStream(inputStream, new Inflater(true))
    IOUtils.toString(stream, StandardCharsets.UTF_8)
  }
}

  // This is built in for 1.12, but KDA only supports 1.11 right now
  class RandomKinesisPartitioner extends KinesisPartitioner[String] {
    override def getPartitionId(element: String): String = {
      java.util.UUID.randomUUID.toString
    }
  }

  case class EventRecord(
                          @JsonProperty("Name")
                          name: String,
                          @JsonProperty("Fields")
                          fields: MWEvent
                        )

  case class KinesisRecord(
                            @JsonProperty("Data")
                            data: String
                          )

