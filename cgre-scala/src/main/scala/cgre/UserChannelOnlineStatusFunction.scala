package cgre

import cgre.CountTypes.{OutputType, ValueType}
import org.apache.flink.api.common.state.{ValueState, ValueStateDescriptor}
import org.apache.flink.streaming.api.functions.KeyedProcessFunction
import org.apache.flink.util.Collector

import java.time.ZonedDateTime
import org.joda.time.{DateTime, DateTimeZone}

object CountTypes {
  type KeyType = (String, String)
  type ValueType = MWEvent
  type OutputType = OnlineStatus
}

case class UserChannelTimestamp(key: (String, String), lastModified: Long)

class UserChannelOnlineStatusFunction extends KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType] {
  lazy val state: ValueState[UserChannelTimestamp] = getRuntimeContext.getState(new ValueStateDescriptor[UserChannelTimestamp]("UCState", classOf[UserChannelTimestamp]))
  val timeWindow = 900000

  override def processElement(value: CountTypes.ValueType, ctx: KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType]#Context, out: Collector[CountTypes.OutputType]): Unit = {
    // 1. If the timestamp is greater than 15 minutes old, do nothing.
    if (!withinAcceptableTime(value.timeUTC)) {
      // TODO: emit some metric?
      return
    }
    // 2. If key (user, channel) has no state, they are seen for the first time. Emit an event to indicate they are online.
    val stored = state.value()
    if(stored == null) {
      Logger.LOG.info("{} is being marked online", value)
      out.collect(OnlineStatus(value.channelID, value.userID, isOnline = true))
    }

    // 3. Get the latest event timestamp by comparing the current event with any stored events
    var eventTimestamp = ZonedDateTime.parse(value.timeUTC, SpadeTime.DTF).toInstant.toEpochMilli
    if (stored != null) {
      eventTimestamp = math.max(eventTimestamp, stored.lastModified)
    }

    // 4. Store the event with the latest timestamp
    val current = UserChannelTimestamp((value.channelID, value.userID), eventTimestamp)
    state.update(current)

    // 5. Register a timer to process event expiration.
    ctx.timerService.registerProcessingTimeTimer(current.lastModified + timeWindow)
  }

  override def onTimer(
                        timestamp: Long,
                        ctx: KeyedProcessFunction[CountTypes.KeyType, CountTypes.ValueType, CountTypes.OutputType]#OnTimerContext,
                        out: Collector[CountTypes.OutputType]): Unit = {
    state.value match {
      case UserChannelTimestamp(key, lastModified) if (timestamp == lastModified + timeWindow) =>
        Logger.LOG.info("{}} is being marked offline", key)
        out.collect(new OnlineStatus(key._1, key._2, isOnline = false))
        state.clear()
      case _ => Logger.LOG.info("onTimer throwaway at time {}", ctx.timestamp())
    }
  }

  def withinAcceptableTime(timeUTC: String): Boolean = {
    //2021-09-05 18:16:43
    val eventTime = ZonedDateTime.parse(timeUTC, SpadeTime.DTF).toInstant.toEpochMilli
    val currentTime = DateTime.now(DateTimeZone.UTC).getMillis

    if (currentTime - eventTime > timeWindow) {
      return false
    }

    true
  }
}
