package config

import com.amazonaws.services.kinesisanalytics.runtime.KinesisAnalyticsRuntime

object Config {
  case class Settings(
                       envName: String,
                       localMode: Boolean,
                       inputStream: String,
                       outputStream: String,
                       readParallelism: Int,
                     )

  private val localSetting: Settings = Settings(
    envName = "local",
    localMode = true,
    inputStream = "spade-downstream-staging-cgre-mw",
    outputStream = "beta-sink-mw-aggregation",
    readParallelism = 10,
  )

  private val stagingSetting: Settings = localSetting.copy(
    envName="staging",
    localMode = false,
    inputStream = "spade-downstream-staging-cgre-mw",
    outputStream = "beta-sink-mw-aggregation",
    readParallelism = 32,
  )

  private val prodSettings: Settings = Settings(
    envName="prod",
    localMode = false,
    inputStream = "spade-downstream-prod-cgre-mw",
    outputStream = "prod-sink-mw-aggregation",
    readParallelism = 128,
  )

  var settings: Settings = localSetting

  private val props = KinesisAnalyticsRuntime.getApplicationProperties
  if (props.size() != 0) {
    // running inside KDA
    if (props.get("config").get("env") == "prod") {
      settings = prodSettings
    } else {
      settings = stagingSetting
    }
  } else {
    val env: String = System.getenv("ENV")
    if (env == "staging") {
      settings = stagingSetting
    }
  }
}
