package cgre

import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import org.joda.time.{DateTime, DateTimeZone}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TestUserChannelOnlineStatusFunction extends AnyFlatSpec with Matchers{
  val fn = new UserChannelOnlineStatusFunction

  "withinAcceptableTime" should "return true if the event was received within 15 minutes" in {
    val currentTime = new DateTime(DateTimeZone.UTC).getMillis
    val format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
    val eventTime = format.withZone(DateTimeZone.UTC).print(currentTime)

    fn.withinAcceptableTime(eventTime) should equal(true)

    val eventTime2 = format.withZone(DateTimeZone.UTC).print(currentTime + 10000)
    fn.withinAcceptableTime(eventTime2) should equal(true)

    val eventTime3 = format.withZone(DateTimeZone.UTC).print(currentTime - 100000)
    fn.withinAcceptableTime(eventTime3) should equal(true)
  }

  "withinAcceptableTime" should "return false if the event was received after 15 minutes" in {
    val currentTime = new DateTime(DateTimeZone.UTC).getMillis
    val format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
    val eventTime = format.withZone(DateTimeZone.UTC).print(currentTime-fn.timeWindow)

    fn.withinAcceptableTime(eventTime) should equal(false)
  }
}
