package cgre

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TestSpade extends AnyFlatSpec with Matchers {
    val mapper: ObjectMapper = JsonMapper.builder()
      .addModule(DefaultScalaModule)
      .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
      .build()

  "MWEvent filter" should "filter out empty channelID" in {
    val event = MWEvent(
      channelID = "",
      userID = "123",
      timeUTC = "2006-01-02 15:04:05",
    )
    val isValid = event.filter
    assert(!isValid)
  }

  "MWEvent filter" should "filter out empty userID" in {
    val event = MWEvent(
      channelID = "123",
      userID = "",
      timeUTC = "2006-01-02 15:04:05",
    )
    val isValid = event.filter
    assert(!isValid)
  }

  "MWEvent filter" should "filter out empty timeUTC" in {
    val event = MWEvent(
      channelID = "123",
      userID = "123",
      timeUTC = "",
    )
    val isValid = event.filter
    assert(!isValid)
  }

  "MWEvent filter" should "not filter a valid event" in {
    val event = MWEvent(
      channelID = "123",
      userID = "123",
      timeUTC = "2006-01-02 15:04:05",
    )
    val isValid = event.filter
    assert(isValid)
  }
}
