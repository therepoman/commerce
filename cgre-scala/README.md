#CGRE Flink

### Development 

### Compiling
Running `sbt assembly` will run tests and if successful, output a `.jar` file to `./target/scala-2.12/minute-watched-assembly-0.1-SNAPSHOT.jar`

### Deploying
Upload the `.jar` file to s3 bucket. Configure KDA to use the new file.
See link