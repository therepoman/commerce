package config

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"

	log "code.justin.tv/commerce/logrus"
	"github.com/go-yaml/yaml"
)

const (
	LocalConfigFilePath  = "/src/code.justin.tv/commerce/pagle/config/data/%s.yaml"
	GlobalConfigFilePath = "/etc/pagle/config/%s.yaml"
	LambdaConfigFilePath = "/config/data/%s.yaml"

	LambdaRootPathEnvironmentVariable = "LAMBDA_TASK_ROOT"
	EnvironmentEnvironmentVariable    = "ENVIRONMENT"

	UnitTest  = Environment("unit-test")
	SmokeTest = Environment("smoke-test")
	Local     = Environment("local")
	Staging   = Environment("staging")
	Prod      = Environment("prod")
	Default   = Local
)

type PDMS struct {
	CallerRole     string `yaml:"caller-role"`
	LambdaARN      string `yaml:"lambda-arn"`
	DryRun         bool   `yaml:"dry-run"`
	ReportDeletion bool   `yaml:"report-deletion"`
}

type SNS struct {
	ConditionParticipantProcessingARN string `yaml:"condition-participant-processing-arn"`
	ConditionParticipantCompletionARN string `yaml:"condition-participant-completion-arn"`
	ConditionParticipantCleanupARN    string `yaml:"condition-participant-cleanup-arn"`
	ConditionRunProcessingARN         string `yaml:"condition-run-processing-arn"`
	ConditionAggregatesRetryARN       string `yaml:"condition-aggregates-retry-arn"`
	ConditionCompletionARN            string `yaml:"condition-completion-arn"`
	PublishThrottlingLimitTPS         int    `yaml:"publish-throttling-limit-tps"`
	GringottsRevenueEventARN          string `yaml:"gringotts-revenue-event-arn"`
}

type SFN struct {
	TimeoutConditionARN            string `yaml:"timeout-condition-arn"`
	TimeoutConditionParticipantARN string `yaml:"timeout-condition-participant-arn"`
	ConditionRunProcessingARN      string `yaml:"condition-run-processing-arn"`
	PDMSUserDeletionARN            string `yaml:"pdms-user-deletion-arn"`
}

type S3 struct {
	FinalizedConditionParticipantsBucket string `yaml:"finalized-condition-participants-bucket"`
	EffectStaticConfigBucket             string `yaml:"effect-static-config-bucket"`
}

type CacheConfig struct {
	SetEnabled bool `yaml:"set-enabled"`
}

type Cache struct {
	GetConditions            *CacheConfig `yaml:"get-conditions"`
	GetConditionParticipants *CacheConfig `yaml:"get-condition-participants"`
}

type Config struct {
	EnvironmentName          string `yaml:"environment-name"`
	PagleLambdaEndpoint      string `yaml:"pagle-lambda-endpoint"`
	PagleIntegrationEndpoint string `yaml:"pagle-integration-endpoint"`
	DynamoSuffix             string `yaml:"dynamo-suffix"`
	AWSRegion                string `yaml:"aws-region"`
	PaydayEndpoint           string `yaml:"payday-endpoint"`
	EMSEndpoint              string `yaml:"ems-endpoint"`
	S2SServiceName           string `yaml:"s2s-service-name"`
	CloudWatchMetricsEnabled bool   `yaml:"cloudwatch-metrics-enabled"`
	SNS                      *SNS   `yaml:"sns"`
	SFN                      *SFN   `yaml:"sfn"`
	S3                       *S3    `yaml:"s3"`
	UseRedisClusterMode      bool   `yaml:"use-redis-cluster-mode"`
	RedisEndpoint            string `yaml:"redis-endpoint"`
	Cache                    *Cache `yaml:"cache"`
	PagleECSCluster          string `yaml:"pagle-ecs-cluster"`
	PubsubEndpoint           string `yaml:"pubsub-endpoint"`
	OwlEndpoint              string `yaml:"owl-endpoint"`
	RipleyEndpoint           string `yaml:"ripley-endpoint"`
	ServiceCatalogID         string `yaml:"service-catalog-id"`
	PDMS                     *PDMS  `yaml:"pdms"`
}

type Environment string

var Environments = map[Environment]interface{}{UnitTest: nil, SmokeTest: nil, Local: nil, Staging: nil, Prod: nil}

func IsValidEnvironment(env Environment) bool {
	_, ok := Environments[env]
	return ok
}

func LoadConfig(env Environment) (*Config, error) {
	if !IsValidEnvironment(env) {
		log.Errorf("Invalid environment: %s. Falling back to local", env)
		env = Local
	}

	baseFileName := string(env)
	filePath, err := getConfigFilePath(baseFileName)
	if err != nil {
		return nil, err
	}
	return loadConfig(filePath)
}

func loadConfig(path string) (*Config, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer func() {
		err := file.Close()
		if err != nil {
			log.WithError(err).Error("Error closing config file")
		}
	}()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var cfg Config
	err = yaml.Unmarshal(fileBytes, &cfg)
	if err != nil {
		return nil, err
	}
	return &cfg, nil
}

func getConfigFilePath(baseFilename string) (string, error) {
	if lambdaRootPath := os.Getenv(LambdaRootPathEnvironmentVariable); lambdaRootPath != "" {
		lambdaFName := path.Join(lambdaRootPath, fmt.Sprintf(LambdaConfigFilePath, baseFilename))
		_, err := os.Stat(lambdaFName)
		return lambdaFName, err
	}

	localFname := os.Getenv("GOPATH") + fmt.Sprintf(LocalConfigFilePath, baseFilename)
	if _, err := os.Stat(localFname); !os.IsNotExist(err) {
		return localFname, nil
	}

	globalFname := fmt.Sprintf(GlobalConfigFilePath, baseFilename)
	if _, err := os.Stat(globalFname); os.IsNotExist(err) {
		return "", err
	}
	return globalFname, nil
}

func GetEnv() Environment {
	env := os.Getenv(EnvironmentEnvironmentVariable)
	if env != "" {
		log.Infof("Found ENVIRONMENT environment variable: %s", env)
	}

	args := os.Args
	if len(args) > 2 {
		log.Panic("Received too many CLI args")
	} else if len(args) == 2 {
		env = args[1]
		log.Infof("Using environment from CLI arg: %s", env)
	}

	if !IsValidEnvironment(Environment(env)) {
		log.Errorf("Invalid environment: %s", env)
		log.Infof("Falling back to default environment: %s", string(Default))
		return Default
	}

	return Environment(env)
}
