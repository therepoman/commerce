// +build integration

package batchgetcondition

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pagle/integration_tests/common"
	pagle "code.justin.tv/commerce/pagle/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestBatchGetCondition(t *testing.T) {
	Convey("given a BatchGetCondition API", t, func() {
		Convey("when the condition exists", func() {
			conditionOwnerID, err := common.GetUniqueConditionOwnerID()

			if err != nil {
				t.Error("Failed to create test owner UUID")
			}

			pagleClient := common.GetPagleClient()

			createConditionResp, err := pagleClient.CreateCondition(context.Background(), &pagle.CreateConditionReq{
				OwnerId:                           conditionOwnerID,
				Name:                              "An integ condition",
				SupportedEffects:                  []pagle.EffectType{pagle.EffectType_GIVE_BITS},
				DefineEffectSettingsWhenSatisfied: true,
				Domain:                            common.IntegDomain,
			})

			So(err, ShouldBeNil)

			Convey("we should return the condition from BatchGetCondition", func() {
				batchGetConditionResp, err := pagleClient.BatchGetCondition(context.Background(), &pagle.BatchGetConditionReq{
					Requests: []*pagle.GetConditionReq{
						{
							ConditionId: createConditionResp.Condition.ConditionId,
							OwnerId:     conditionOwnerID,
							Domain:      common.IntegDomain,
						},
					},
				})

				So(err, ShouldBeNil)
				So(batchGetConditionResp, ShouldNotBeNil)
				So(batchGetConditionResp.Conditions, ShouldHaveLength, 1)
				So(batchGetConditionResp.Conditions[0].ConditionId, ShouldEqual, createConditionResp.Condition.ConditionId)
				So(batchGetConditionResp.Conditions[0].OwnerId, ShouldEqual, conditionOwnerID)
				So(batchGetConditionResp.Conditions[0].Domain, ShouldEqual, common.IntegDomain)
				So(batchGetConditionResp.Conditions[0].DefineEffectSettingsWhenSatisfied, ShouldBeTrue)
				So(batchGetConditionResp.Conditions[0].DisableWhenSatisfied, ShouldBeFalse)
			})
		})
	})
}
