package util

import (
	"code.justin.tv/commerce/gogogadget/random"
)

const (
	IntegBitsTestUserIdPrefixString = "7357"
	IntegBitsTestUserIdSuffixDigits = 14
)

// Payday identifies userIDs matching this pattern as test users.
// It would be better to import this function from Payday directly, but doing so requires 700+ vendor files.
func GetRandomBitsTestUser() string {
	return IntegBitsTestUserIdPrefixString + random.NumberString(IntegBitsTestUserIdSuffixDigits)
}
