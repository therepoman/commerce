// +build integration

package createcondition

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pagle/integration_tests/common"
	pagle "code.justin.tv/commerce/pagle/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCreateCondition(t *testing.T) {
	Convey("given a CreateCondition API", t, func() {
		Convey("when a new condition is created", func() {
			pagleClient := common.GetPagleClient()
			createConditionResp, err := pagleClient.CreateCondition(context.Background(), &pagle.CreateConditionReq{
				OwnerId:                           common.IntegConditionOwnerID,
				Name:                              "An integ condition",
				SupportedEffects:                  []pagle.EffectType{pagle.EffectType_GIVE_BITS},
				DefineEffectSettingsWhenSatisfied: true,
				Domain:                            common.IntegDomain,
			})

			Convey("we should return the new condition ID", func() {
				So(err, ShouldBeNil)
				So(createConditionResp, ShouldNotBeNil)
				So(createConditionResp.Condition.ConditionId, ShouldNotBeEmpty)
			})
		})

		Convey("when a new condition is created with invalid effect settings (recipient is not eligible to receive bits)", func() {
			pagleClient := common.GetPagleClient()
			createConditionResp, err := pagleClient.CreateCondition(context.Background(), &pagle.CreateConditionReq{
				OwnerId:                           common.IntegConditionOwnerID,
				Name:                              "An integ condition",
				SupportedEffects:                  []pagle.EffectType{pagle.EffectType_GIVE_BITS},
				DefineEffectSettingsWhenSatisfied: false,
				EffectSettings: []*pagle.EffectSettings{
					{
						EffectType: pagle.EffectType_GIVE_BITS,
						Settings: &pagle.EffectSettings_GiveBitsSettings{
							GiveBitsSettings: &pagle.GiveBitsSettings{
								PoolRecipients: []*pagle.PoolRecipientWeightedShare{
									{
										ToUserId:    common.IntegBitsRealNonPartner,
										ShareWeight: 1,
									},
								},
							},
						},
					},
				},
				Domain: common.IntegDomain,
			})

			Convey("we should error", func() {
				So(err, ShouldNotBeNil)
				So(createConditionResp, ShouldBeNil)
			})
		})

		Convey("when a new condition is created with valid effect settings", func() {
			pagleClient := common.GetPagleClient()
			req := &pagle.CreateConditionReq{
				OwnerId:                           common.IntegConditionOwnerID,
				Name:                              "An integ condition",
				SupportedEffects:                  []pagle.EffectType{pagle.EffectType_GIVE_BITS},
				DefineEffectSettingsWhenSatisfied: false,
				EffectSettings: []*pagle.EffectSettings{
					{
						EffectType: pagle.EffectType_GIVE_BITS,
						Settings: &pagle.EffectSettings_GiveBitsSettings{
							GiveBitsSettings: &pagle.GiveBitsSettings{
								PoolRecipients: []*pagle.PoolRecipientWeightedShare{
									{
										ToUserId:    common.IntegBitsRealPartnerOptedIn,
										ShareWeight: 1,
									},
								},
							},
						},
					},
				},
				Domain: common.IntegDomain,
			}

			createConditionResp, err := pagleClient.CreateCondition(context.Background(), req)

			Convey("we should return the created condition with the provided effect settings", func() {
				So(err, ShouldBeNil)
				So(createConditionResp.Condition, ShouldNotBeNil)
				So(createConditionResp.Condition.ConditionId, ShouldNotBeEmpty)
				So(createConditionResp.Condition.DefineEffectSettingsWhenSatisfied, ShouldBeFalse)
				So(createConditionResp.Condition.EffectSettings, ShouldNotBeEmpty)
				So(createConditionResp.Condition.EffectSettings[0].EffectType, ShouldEqual, pagle.EffectType_GIVE_BITS)
				So(createConditionResp.Condition.EffectSettings[0].Settings, ShouldResemble, &pagle.EffectSettings_GiveBitsSettings{
					GiveBitsSettings: &pagle.GiveBitsSettings{
						PoolRecipients: []*pagle.PoolRecipientWeightedShare{
							{
								ToUserId:    common.IntegBitsRealPartnerOptedIn,
								ShareWeight: 1,
							},
						},
					},
				})
			})
		})

		// TODO: Should probably test condition timeouts, but we need to do it in a way that is not flaky.
		//   Blindly waiting for the step function to invoke is probably not ideal.
	})
}
