// +build integration

package getconditionparticipantsbyownerid

import (
	"context"
	"sort"
	"testing"

	"code.justin.tv/commerce/pagle/backend/models"

	"code.justin.tv/commerce/pagle/integration_tests/common"
	pagle "code.justin.tv/commerce/pagle/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetConditionParticipantsByOwnerID(t *testing.T) {
	Convey("given a GetConditionParticipantsByOwnerID API", t, func() {
		Convey("when conditions exists for different owners, and a user has contributed to them", func() {
			// ConditionOwnerIDs are used to sort the ConditionParticipants array in the response.
			conditionOwnerID1 := "testID-1"
			conditionOwnerID2 := "testID-2"
			conditionOwnerID3 := "testID-3"

			pagleClient := common.GetPagleClient()

			createConditionResp1, err := pagleClient.CreateCondition(context.Background(), &pagle.CreateConditionReq{
				OwnerId:                           conditionOwnerID1,
				Name:                              "1-An integ condition",
				SupportedEffects:                  []pagle.EffectType{pagle.EffectType_GIVE_BITS},
				DefineEffectSettingsWhenSatisfied: true,
				Domain:                            common.IntegDomain,
			})

			So(err, ShouldBeNil)

			createConditionResp2, err := pagleClient.CreateCondition(context.Background(), &pagle.CreateConditionReq{
				OwnerId:                           conditionOwnerID2,
				Name:                              "2-An integ condition-2",
				SupportedEffects:                  []pagle.EffectType{pagle.EffectType_GIVE_BITS},
				DefineEffectSettingsWhenSatisfied: true,
				Domain:                            common.IntegDomain,
			})

			So(err, ShouldBeNil)

			createConditionResp3, err := pagleClient.CreateCondition(context.Background(), &pagle.CreateConditionReq{
				OwnerId:                           conditionOwnerID3,
				Name:                              "3-An integ condition-3",
				SupportedEffects:                  []pagle.EffectType{pagle.EffectType_GIVE_BITS},
				DefineEffectSettingsWhenSatisfied: true,
				Domain:                            common.IntegDomain,
			})

			So(err, ShouldBeNil)

			testUserID, err := common.AddBitsForTestUser(3)

			So(err, ShouldBeNil)

			createConditionParticipantResp1, err := pagleClient.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
				ConditionId:                 createConditionResp1.Condition.ConditionId,
				ConditionOwnerId:            conditionOwnerID1,
				ConditionParticipantOwnerId: testUserID,
				TtlSeconds:                  models.ConditionParticipantMaxTTL,
				Effect: &pagle.Effect{
					EffectType: pagle.EffectType_GIVE_BITS,
					Params: &pagle.Effect_GiveBitsParams{
						GiveBitsParams: &pagle.GiveBitsParams{
							Amount:     1,
							FromUserId: testUserID,
						},
					},
				},
				Domain: common.IntegDomain,
			})

			So(err, ShouldBeNil)

			createConditionParticipantResp2, err := pagleClient.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
				ConditionId:                 createConditionResp2.Condition.ConditionId,
				ConditionOwnerId:            conditionOwnerID2,
				ConditionParticipantOwnerId: testUserID,
				TtlSeconds:                  models.ConditionParticipantMaxTTL,
				Effect: &pagle.Effect{
					EffectType: pagle.EffectType_GIVE_BITS,
					Params: &pagle.Effect_GiveBitsParams{
						GiveBitsParams: &pagle.GiveBitsParams{
							Amount:     1,
							FromUserId: testUserID,
						},
					},
				},
				Domain: common.IntegDomain,
			})

			So(err, ShouldBeNil)

			createConditionParticipantResp3, err := pagleClient.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
				ConditionId:                 createConditionResp3.Condition.ConditionId,
				ConditionOwnerId:            conditionOwnerID3,
				ConditionParticipantOwnerId: testUserID,
				TtlSeconds:                  models.ConditionParticipantMaxTTL,
				Effect: &pagle.Effect{
					EffectType: pagle.EffectType_GIVE_BITS,
					Params: &pagle.Effect_GiveBitsParams{
						GiveBitsParams: &pagle.GiveBitsParams{
							Amount:     1,
							FromUserId: testUserID,
						},
					},
				},
				Domain: common.IntegDomain,
			})

			So(err, ShouldBeNil)

			Convey("we should return all the conditionParticipants belonging to the user when calling GetConditionParticipantsByOwnerID", func() {
				getConditionParticipantsByOwnerIDResp, err := pagleClient.GetConditionParticipantsByOwnerID(context.Background(), &pagle.GetConditionParticipantsByOwnerIDReq{
					ConditionParticipantOwnerId:  testUserID,
					Domain:                       common.IntegDomain,
				})

				So(err, ShouldBeNil)
				So(getConditionParticipantsByOwnerIDResp, ShouldNotBeNil)
				So(getConditionParticipantsByOwnerIDResp.ConditionParticipants, ShouldHaveLength, 3)
				sort.Slice(getConditionParticipantsByOwnerIDResp.ConditionParticipants, func(i, j int) bool {
					return getConditionParticipantsByOwnerIDResp.ConditionParticipants[i].ConditionOwnerId < getConditionParticipantsByOwnerIDResp.ConditionParticipants[j].ConditionOwnerId
				})
				So(getConditionParticipantsByOwnerIDResp.ConditionParticipants[0].ConditionParticipantId, ShouldEqual, createConditionParticipantResp1.ConditionParticipant.ConditionParticipantId)
				So(getConditionParticipantsByOwnerIDResp.ConditionParticipants[1].ConditionParticipantId, ShouldEqual, createConditionParticipantResp2.ConditionParticipant.ConditionParticipantId)
				So(getConditionParticipantsByOwnerIDResp.ConditionParticipants[2].ConditionParticipantId, ShouldEqual, createConditionParticipantResp3.ConditionParticipant.ConditionParticipantId)

			})
		})
	})
}
