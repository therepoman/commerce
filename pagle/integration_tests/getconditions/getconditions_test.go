// +build integration

package getconditions

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pagle/integration_tests/common"
	pagle "code.justin.tv/commerce/pagle/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetConditions(t *testing.T) {
	Convey("given a GetConditions API", t, func() {
		Convey("when a condition exists for a user", func() {
			conditionOwnerID, err := common.GetUniqueConditionOwnerID()

			if err != nil {
				t.Error("Failed to create test owner UUID")
			}

			pagleClient := common.GetPagleClient()

			createConditionResp, err := pagleClient.CreateCondition(context.Background(), &pagle.CreateConditionReq{
				OwnerId:                           conditionOwnerID,
				Name:                              "An integ condition",
				SupportedEffects:                  []pagle.EffectType{pagle.EffectType_GIVE_BITS},
				DefineEffectSettingsWhenSatisfied: true,
				Domain:                            common.IntegDomain,
			})

			So(err, ShouldBeNil)

			Convey("we should return the condition from GetConditions", func() {
				getConditionsResp, err := pagleClient.GetConditions(context.Background(), &pagle.GetConditionsReq{
					OwnerId:        conditionOwnerID,
					ConditionState: pagle.ConditionState_ACTIVE,
					Domain:         common.IntegDomain,
				})

				So(err, ShouldBeNil)
				So(getConditionsResp, ShouldNotBeNil)
				So(getConditionsResp.Conditions, ShouldHaveLength, 1)
				So(getConditionsResp.Conditions[0].ConditionId, ShouldEqual, createConditionResp.Condition.ConditionId)
				So(getConditionsResp.Conditions[0].ConditionState, ShouldEqual, pagle.ConditionState_ACTIVE)
				So(getConditionsResp.Conditions[0].DefineEffectSettingsWhenSatisfied, ShouldBeTrue)
				So(getConditionsResp.Conditions[0].DisableWhenSatisfied, ShouldBeFalse)
			})

			Convey("we should respect the condition state filters", func() {
				createConditionResp2, err := pagleClient.CreateCondition(context.Background(), &pagle.CreateConditionReq{
					OwnerId:                           conditionOwnerID,
					Name:                              "An integ condition",
					SupportedEffects:                  []pagle.EffectType{pagle.EffectType_GIVE_BITS},
					DefineEffectSettingsWhenSatisfied: true,
					Domain:                            common.IntegDomain,
				})

				So(err, ShouldBeNil)

				getConditionsResp, err := pagleClient.GetConditions(context.Background(), &pagle.GetConditionsReq{
					OwnerId:        conditionOwnerID,
					ConditionState: pagle.ConditionState_ACTIVE,
					Domain:         common.IntegDomain,
				})

				So(err, ShouldBeNil)
				So(getConditionsResp, ShouldNotBeNil)
				So(getConditionsResp.Conditions, ShouldHaveLength, 2)

				_, err = pagleClient.CancelCondition(context.Background(), &pagle.CancelConditionReq{
					ConditionId: createConditionResp2.Condition.ConditionId,
					OwnerId:     conditionOwnerID,
					Domain:      common.IntegDomain,
				})

				So(err, ShouldBeNil)

				getConditionsResp2, err := pagleClient.GetConditions(context.Background(), &pagle.GetConditionsReq{
					OwnerId:        conditionOwnerID,
					ConditionState: pagle.ConditionState_ACTIVE,
					Domain:         common.IntegDomain,
				})

				So(err, ShouldBeNil)
				So(getConditionsResp2, ShouldNotBeNil)
				So(getConditionsResp2.Conditions, ShouldHaveLength, 1)
			})
		})
	})
}
