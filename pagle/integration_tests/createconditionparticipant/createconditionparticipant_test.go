// +build integration

package createconditionparticipant

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/integration_tests/common"
	pagle "code.justin.tv/commerce/pagle/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCreateConditionParticipant(t *testing.T) {
	Convey("given a CreateConditionParticipant API", t, func() {
		Convey("given an active condition exists", func() {
			pagleClient := common.GetPagleClient()
			createConditionResp, err := pagleClient.CreateCondition(context.Background(), &pagle.CreateConditionReq{
				OwnerId:                           common.IntegConditionOwnerID,
				Name:                              "An integ condition",
				SupportedEffects:                  []pagle.EffectType{pagle.EffectType_GIVE_BITS},
				DefineEffectSettingsWhenSatisfied: true,
				Domain:                            common.IntegDomain,
			})

			So(err, ShouldBeNil)

			testUserID, err := common.AddBitsForTestUser(1)

			So(err, ShouldBeNil)

			Convey("when attempting to create a conditionParticipant with the incorrect condition owner", func() {
				_, err = pagleClient.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
					ConditionId:                 createConditionResp.Condition.ConditionId,
					ConditionOwnerId:            "wrong-owner",
					ConditionParticipantOwnerId: testUserID,
					TtlSeconds:                  models.ConditionParticipantMaxTTL,
					Effect: &pagle.Effect{
						EffectType: pagle.EffectType_GIVE_BITS,
						Params: &pagle.Effect_GiveBitsParams{
							GiveBitsParams: &pagle.GiveBitsParams{
								Amount:     1,
								FromUserId: testUserID,
							},
						},
					},
					Domain: common.IntegDomain,
				})

				Convey("we should error", func() {
					So(err, ShouldNotBeNil)
				})
			})

			testUserID, err = common.AddBitsForTestUser(1)

			So(err, ShouldBeNil)

			Convey("when attempting to create a conditionParticipant with an out of bounds ttl", func() {
				_, err = pagleClient.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
					ConditionId:                 createConditionResp.Condition.ConditionId,
					ConditionOwnerId:            common.IntegConditionOwnerID,
					ConditionParticipantOwnerId: testUserID,
					TtlSeconds:                  models.ConditionParticipantMaxTTL + 1,
					Effect: &pagle.Effect{
						EffectType: pagle.EffectType_GIVE_BITS,
						Params: &pagle.Effect_GiveBitsParams{
							GiveBitsParams: &pagle.GiveBitsParams{
								Amount:     1,
								FromUserId: testUserID,
							},
						},
					},
					Domain: common.IntegDomain,
				})

				Convey("we should error", func() {
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when attempting to create a conditionParticipant with a bits amount <= 0, we should error", func() {
				_, err = pagleClient.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
					ConditionId:                 createConditionResp.Condition.ConditionId,
					ConditionOwnerId:            common.IntegConditionOwnerID,
					ConditionParticipantOwnerId: testUserID,
					TtlSeconds:                  models.ConditionParticipantMaxTTL,
					Effect: &pagle.Effect{
						EffectType: pagle.EffectType_GIVE_BITS,
						Params: &pagle.Effect_GiveBitsParams{
							GiveBitsParams: &pagle.GiveBitsParams{
								Amount:     0,
								FromUserId: testUserID,
							},
						},
					},
					Domain: common.IntegDomain,
				})

				So(err, ShouldNotBeNil)
			})

			Convey("when attempting to create a conditionParticipant with the correct owner", func() {
				testUserID, err := common.AddBitsForTestUser(1)

				So(err, ShouldBeNil)

				createConditionParticipantResp, err := pagleClient.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
					ConditionId:                 createConditionResp.Condition.ConditionId,
					ConditionOwnerId:            common.IntegConditionOwnerID,
					ConditionParticipantOwnerId: testUserID,
					TtlSeconds:                  models.ConditionParticipantMaxTTL,
					Effect: &pagle.Effect{
						EffectType: pagle.EffectType_GIVE_BITS,
						Params: &pagle.Effect_GiveBitsParams{
							GiveBitsParams: &pagle.GiveBitsParams{
								Amount:     1,
								FromUserId: testUserID,
							},
						},
					},
					Domain: common.IntegDomain,
				})

				So(err, ShouldBeNil)
				So(createConditionParticipantResp, ShouldNotBeNil)

				Convey("we should return the conditionParticipant ID", func() {
					So(createConditionParticipantResp.ConditionParticipant.ConditionParticipantId, ShouldNotBeEmpty)
				})

				Convey("we should return the provided effect as part of the conditionParticipant", func() {
					So(createConditionParticipantResp.ConditionParticipant.Effect.EffectType, ShouldEqual, pagle.EffectType_GIVE_BITS)
					So(createConditionParticipantResp.ConditionParticipant.Effect.Params, ShouldResemble, &pagle.Effect_GiveBitsParams{
						GiveBitsParams: &pagle.GiveBitsParams{
							Amount:     1,
							FromUserId: testUserID,
						},
					})
				})

				Convey("we should see the conditionParticipant in pending state", func() {
					conditionParticipantResp, err := pagleClient.GetConditionParticipant(context.Background(), &pagle.GetConditionParticipantReq{
						ConditionParticipantId:      createConditionParticipantResp.ConditionParticipant.ConditionParticipantId,
						Domain:                      common.IntegDomain,
						ConditionParticipantOwnerId: createConditionParticipantResp.ConditionParticipant.OwnerId,
					})

					So(err, ShouldBeNil)
					So(conditionParticipantResp, ShouldNotBeNil)
					So(conditionParticipantResp.ConditionParticipant, ShouldNotBeNil)
					So(conditionParticipantResp.ConditionParticipant.ConditionParticipantProcessingState, ShouldEqual, pagle.ConditionParticipantProcessingState_PENDING)
				})
			})
		})
	})

	// TODO: as soon as we have more than one effect type, add a test that validates that we error
	//  when attempting to create a conditionParticipant with a non-whitelisted effect type.
}
