// +build integration

package healthcheck

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pagle/integration_tests/common"
	"code.justin.tv/commerce/pagle/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestHealthCheck(t *testing.T) {
	Convey("given a HealthCheck API", t, func() {
		Convey("when calling the health check, we should not error", func() {
			pagleClient := common.GetPagleClient()
			_, err := pagleClient.HealthCheck(context.Background(), &pagle.HealthCheckReq{})
			So(err, ShouldBeNil)
		})
	})
}
