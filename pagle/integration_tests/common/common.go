package common

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"
	"time"

	"code.justin.tv/commerce/pagle/integration_tests/util"

	"code.justin.tv/commerce/pagle/config"
	pagle "code.justin.tv/commerce/pagle/rpc"
	payday "code.justin.tv/commerce/payday/client"
	payday_model "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	"github.com/gofrs/uuid"
	log "github.com/sirupsen/logrus"
	"k8s.io/apimachinery/pkg/util/wait"
)

const (
	IntegDomain           = "__integration__"
	IntegConditionOwnerID = "__integration-condition-owner__"

	IntegAdminGiveBitsUser   = "116076154" // qa-bits-partner
	IntegAdminGiveBitsType   = "CB_0"
	IntegAdminGiveBitsReason = "pagle_integration_tests"

	// Obtained from Payday integration tests
	IntegBitsRealPartnerOptedOut = "67652383"
	IntegBitsRealPartnerOptedIn  = "54361543" // jtmoney357
	IntegBitsRealNonPartner      = "67656031"
)

var (
	ConditionParticipantCompletionPollTimeout  = 15 * time.Second
	ConditionParticipantCompletionPollInterval = 2 * time.Second
)

var paydayClient payday.Client
var pagleClient pagle.Pagle

func init() {
	// Only init once - we don't want to incur the overhead of multiple s2s roundtrippers
	paydayClient = makePaydayClient()
	pagleClient = makePagleClient()
}

func loadConfig() *config.Config {
	environment := os.Getenv("ENVIRONMENT")
	if environment == "" {
		log.Panic("ENVIRONMENT env variable not set")
	}

	if environment == "production" || environment == "prod" {
		log.Panic("Please don't run these integration tests against prod!")
	}

	log.Infof("Loading config file for environment: %s", environment)
	cfg, err := config.LoadConfig(config.Environment(environment))
	if err != nil {
		log.WithError(err).Panicf("Could not load config file for environment: %s", environment)
	}

	if cfg == nil {
		log.WithError(err).Panicf("Could not find a config file for environment: %s", environment)
	}

	return cfg
}

func GetPagleClient() pagle.Pagle {
	return pagleClient
}

func makePagleClient() pagle.Pagle {
	cfg := loadConfig()

	return pagle.NewPagleProtobufClient(cfg.PagleIntegrationEndpoint, http.DefaultClient)
}

func makePaydayClient() payday.Client {
	cfg := loadConfig()

	paydayConfig := twitchclient.ClientConf{
		Host: cfg.PaydayEndpoint,
	}

	s2sConfig := caller.Config{}
	rt, err := caller.NewRoundTripper(cfg.S2SServiceName, &s2sConfig, log.New())
	if err != nil {
		log.Panicf("Failed to create s2s round tripper: %v", err)
	}

	paydayConfig.RoundTripperWrappers = append(paydayConfig.RoundTripperWrappers,
		func(inner http.RoundTripper) http.RoundTripper {
			rt.SetInnerRoundTripper(inner)
			return rt
		},
	)

	client, err := payday.NewClient(paydayConfig)
	if err != nil {
		log.WithError(err).Panicf("Error getting payday client for environment")
	}

	return client

}

func GetUniqueConditionOwnerID() (string, error) {
	ownerUUID, err := uuid.NewV4()

	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%s-%s", IntegConditionOwnerID, ownerUUID.String()), nil
}

func PollConditionParticipantCompletion(conditionParticipantID, conditionParticipantOwnerID string) (pagle.ConditionParticipant, error) {
	pagleClient := GetPagleClient()

	var conditionParticipant pagle.ConditionParticipant
	attempts := 0

	err := wait.Poll(ConditionParticipantCompletionPollInterval, ConditionParticipantCompletionPollTimeout, func() (bool, error) {
		attempts++

		getConditionParticipantResp, requestErr := pagleClient.GetConditionParticipant(context.Background(), &pagle.GetConditionParticipantReq{
			ConditionParticipantId:      conditionParticipantID,
			Domain:                      IntegDomain,
			ConditionParticipantOwnerId: conditionParticipantOwnerID,
		})

		if requestErr != nil {
			return true, requestErr
		}

		if getConditionParticipantResp == nil {
			return true, errors.New("received nil response calling GetConditionParticipant")
		}

		if getConditionParticipantResp.ConditionParticipant == nil {
			return true, errors.New("GetConditionParticipant returned a nil conditionParticipant")
		}

		// Condition participant processing is behind sqs, which means errors can be retried. Only consider a participant to
		// be erred if we see the error state after multiple attempts (giving sqs an opportunity to retry).
		if attempts < 5 && getConditionParticipantResp.ConditionParticipant.ConditionParticipantProcessingState == pagle.ConditionParticipantProcessingState_ERROR {
			return false, nil
		}

		// Any other state indicates that the conditionParticipant has finished processing
		if getConditionParticipantResp.ConditionParticipant.ConditionParticipantProcessingState != pagle.ConditionParticipantProcessingState_PENDING {
			conditionParticipant = *getConditionParticipantResp.ConditionParticipant
			return true, nil
		}

		return false, nil
	})

	return conditionParticipant, err
}

// Returns the test userID that the bits were added to
func AddBitsForTestUser(numBits int32) (string, error) {
	testUserID := util.GetRandomBitsTestUser()

	err := AddBitsForUser(testUserID, numBits)

	return testUserID, err
}

func AddBitsForUser(userID string, numBits int32) error {
	eventID, err := uuid.NewV4()

	if err != nil {
		return err
	}

	err = paydayClient.AddBits(context.Background(), &payday_model.AddBitsRequest{
		TwitchUserId:      userID,
		AmountOfBitsToAdd: numBits,
		EventId:           eventID.String(),
		TypeOfBitsToAdd:   IntegAdminGiveBitsType,
		AdminReason:       IntegAdminGiveBitsReason,
		AdminUserId:       IntegAdminGiveBitsUser,
	}, nil)

	return err
}
