// +build integration

package cancelcondition

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/integration_tests/common"
	pagle "code.justin.tv/commerce/pagle/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCancelCondition(t *testing.T) {
	Convey("given a CancelCondition API", t, func() {
		Convey("given an active condition exists", func() {
			pagleClient := common.GetPagleClient()
			createConditionResp, err := pagleClient.CreateCondition(context.Background(), &pagle.CreateConditionReq{
				OwnerId:                           common.IntegConditionOwnerID,
				Name:                              "An integ condition",
				SupportedEffects:                  []pagle.EffectType{pagle.EffectType_GIVE_BITS},
				DefineEffectSettingsWhenSatisfied: true,
				Domain:                            common.IntegDomain,
			})

			So(err, ShouldBeNil)

			testUserID, err := common.AddBitsForTestUser(1)

			So(err, ShouldBeNil)

			createConditionParticipantResp, err := pagleClient.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
				ConditionId:                 createConditionResp.Condition.ConditionId,
				ConditionOwnerId:            common.IntegConditionOwnerID,
				ConditionParticipantOwnerId: testUserID,
				TtlSeconds:                  models.ConditionParticipantMaxTTL,
				Effect: &pagle.Effect{
					EffectType: pagle.EffectType_GIVE_BITS,
					Params: &pagle.Effect_GiveBitsParams{
						GiveBitsParams: &pagle.GiveBitsParams{
							Amount:     1,
							FromUserId: testUserID,
						},
					},
				},
				Domain: common.IntegDomain,
			})

			So(err, ShouldBeNil)

			Convey("when attempting to cancel a condition with the incorrect owner", func() {
				_, err = pagleClient.CancelCondition(context.Background(), &pagle.CancelConditionReq{
					ConditionId: createConditionResp.Condition.ConditionId,
					OwnerId:     "wrong-owner",
					Domain:      common.IntegDomain,
				})

				Convey("we should error", func() {
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the owner is authorized to cancel the condition", func() {
				_, err = pagleClient.CancelCondition(context.Background(), &pagle.CancelConditionReq{
					ConditionId: createConditionResp.Condition.ConditionId,
					OwnerId:     common.IntegConditionOwnerID,
					Domain:      common.IntegDomain,
				})

				Convey("we should not error", func() {
					So(err, ShouldBeNil)
				})

				Convey("we should set the condition to Canceled", func() {
					conditionResp, err := pagleClient.GetCondition(context.Background(), &pagle.GetConditionReq{
						OwnerId:     common.IntegConditionOwnerID,
						ConditionId: createConditionResp.Condition.ConditionId,
						Domain:      common.IntegDomain,
					})

					So(err, ShouldBeNil)
					So(conditionResp, ShouldNotBeNil)
					So(conditionResp.Condition, ShouldNotBeNil)
					So(conditionResp.Condition.ConditionState, ShouldEqual, pagle.ConditionState_CANCELED)
				})

				Convey("we should release all pending conditionParticipants", func() {
					conditionParticipant, err := common.PollConditionParticipantCompletion(
						createConditionParticipantResp.ConditionParticipant.ConditionParticipantId,
						createConditionParticipantResp.ConditionParticipant.OwnerId,
					)

					So(err, ShouldBeNil)
					So(conditionParticipant.ConditionParticipantEndState, ShouldEqual, pagle.ConditionParticipantEndState_CONDITION_CANCELED)
					So(conditionParticipant.ConditionParticipantProcessingState, ShouldEqual, pagle.ConditionParticipantProcessingState_SUCCESS)
				})

				Convey("we should error when attempting to create condition participants against the canceled condition", func() {
					testUserID, err := common.AddBitsForTestUser(1)

					So(err, ShouldBeNil)

					_, err = pagleClient.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
						ConditionId:                 createConditionResp.Condition.ConditionId,
						ConditionOwnerId:            common.IntegConditionOwnerID,
						ConditionParticipantOwnerId: testUserID,
						TtlSeconds:                  models.ConditionParticipantMaxTTL,
						Effect: &pagle.Effect{
							EffectType: pagle.EffectType_GIVE_BITS,
							Params: &pagle.Effect_GiveBitsParams{
								GiveBitsParams: &pagle.GiveBitsParams{
									Amount:     1,
									FromUserId: testUserID,
								},
							},
						},
						Domain: common.IntegDomain,
					})

					So(err, ShouldNotBeNil)
				})
			})
		})
	})
}
