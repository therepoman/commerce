// +build integration

package satisfycondition

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/integration_tests/common"
	pagle "code.justin.tv/commerce/pagle/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSatisfyCondition(t *testing.T) {
	Convey("given a SatisfyCondition API", t, func() {
		Convey("given an active condition exists, with effect settings undefined", func() {
			pagleClient := common.GetPagleClient()

			createConditionResp, err := pagleClient.CreateCondition(context.Background(), &pagle.CreateConditionReq{
				OwnerId:                           common.IntegConditionOwnerID,
				Name:                              "An integ condition",
				SupportedEffects:                  []pagle.EffectType{pagle.EffectType_GIVE_BITS},
				DefineEffectSettingsWhenSatisfied: true,
				Domain:                            common.IntegDomain,
			})

			So(err, ShouldBeNil)

			testUserID, err := common.AddBitsForTestUser(1)

			So(err, ShouldBeNil)

			createConditionParticipantResp, err := pagleClient.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
				ConditionId:                 createConditionResp.Condition.ConditionId,
				ConditionOwnerId:            common.IntegConditionOwnerID,
				ConditionParticipantOwnerId: testUserID,
				TtlSeconds:                  models.ConditionParticipantMaxTTL,
				Effect: &pagle.Effect{
					EffectType: pagle.EffectType_GIVE_BITS,
					Params: &pagle.Effect_GiveBitsParams{
						GiveBitsParams: &pagle.GiveBitsParams{
							Amount:     1,
							FromUserId: testUserID,
						},
					},
				},
				Domain: common.IntegDomain,
			})

			So(err, ShouldBeNil)

			Convey("when attempting to satisfy a condition without effect settings for supported effects", func() {
				_, err = pagleClient.SatisfyCondition(context.Background(), &pagle.SatisfyConditionReq{
					ConditionId:    createConditionResp.Condition.ConditionId,
					OwnerId:        common.IntegConditionOwnerID,
					EffectSettings: []*pagle.EffectSettings{},
					Domain:         common.IntegDomain,
				})

				Convey("we should error", func() {
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when attempting to satisfy a condition with the incorrect owner", func() {
				_, err = pagleClient.SatisfyCondition(context.Background(), &pagle.SatisfyConditionReq{
					ConditionId: createConditionResp.Condition.ConditionId,
					OwnerId:     "wrong-owner",
					EffectSettings: []*pagle.EffectSettings{
						{
							EffectType: pagle.EffectType_GIVE_BITS,
							Settings: &pagle.EffectSettings_GiveBitsSettings{
								GiveBitsSettings: &pagle.GiveBitsSettings{
									PoolRecipients: []*pagle.PoolRecipientWeightedShare{
										{
											ToUserId:    common.IntegBitsRealPartnerOptedIn,
											ShareWeight: 1,
										},
									},
								},
							},
						},
					},
					Domain: common.IntegDomain,
				})

				Convey("we should error", func() {
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the owner is authorized to satisfy the condition", func() {
				_, err = pagleClient.SatisfyCondition(context.Background(), &pagle.SatisfyConditionReq{
					ConditionId: createConditionResp.Condition.ConditionId,
					OwnerId:     common.IntegConditionOwnerID,
					EffectSettings: []*pagle.EffectSettings{
						{
							EffectType: pagle.EffectType_GIVE_BITS,
							Settings: &pagle.EffectSettings_GiveBitsSettings{
								GiveBitsSettings: &pagle.GiveBitsSettings{
									PoolRecipients: []*pagle.PoolRecipientWeightedShare{
										{
											ToUserId:    common.IntegBitsRealPartnerOptedIn,
											ShareWeight: 1,
										},
									},
								},
							},
						},
					},
					Domain: common.IntegDomain,
				})

				Convey("we should not error", func() {
					So(err, ShouldBeNil)
				})

				Convey("we should finalize all conditionParticipants", func() {
					conditionParticipant, err := common.PollConditionParticipantCompletion(
						createConditionParticipantResp.ConditionParticipant.ConditionParticipantId,
						createConditionParticipantResp.ConditionParticipant.OwnerId,
					)

					So(err, ShouldBeNil)
					So(conditionParticipant.ConditionParticipantEndState, ShouldEqual, pagle.ConditionParticipantEndState_SATISFIED)
					So(conditionParticipant.ConditionParticipantProcessingState, ShouldEqual, pagle.ConditionParticipantProcessingState_SUCCESS)
				})
			})

			Convey("when a effect settings are invalid (recipient is not eligible to receive bits)", func() {
				_, err = pagleClient.SatisfyCondition(context.Background(), &pagle.SatisfyConditionReq{
					ConditionId: createConditionResp.Condition.ConditionId,
					OwnerId:     common.IntegConditionOwnerID,
					EffectSettings: []*pagle.EffectSettings{
						{
							EffectType: pagle.EffectType_GIVE_BITS,
							Settings: &pagle.EffectSettings_GiveBitsSettings{
								GiveBitsSettings: &pagle.GiveBitsSettings{
									PoolRecipients: []*pagle.PoolRecipientWeightedShare{
										{
											// Recipient is not a partner, so they cannot receive bits
											ToUserId:    common.IntegBitsRealNonPartner,
											ShareWeight: 1,
										},
									},
								},
							},
						},
					},
					Domain: common.IntegDomain,
				})

				Convey("we should error", func() {
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the effect is invalid in at least one condition participant (user tries to give bits to themselves)", func() {
				// We don't need to give bits to this user since they will never spend bits in this test. However, for the test to run as intended,
				// they should always have a non-zero bits balance
				err := common.AddBitsForUser(common.IntegBitsRealPartnerOptedIn, 1)
				So(err, ShouldBeNil)

				createConditionParticipantResp2, err := pagleClient.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
					ConditionId:                 createConditionResp.Condition.ConditionId,
					ConditionOwnerId:            common.IntegConditionOwnerID,
					ConditionParticipantOwnerId: common.IntegBitsRealPartnerOptedIn,
					TtlSeconds:                  models.ConditionParticipantMaxTTL,
					Effect: &pagle.Effect{
						EffectType: pagle.EffectType_GIVE_BITS,
						Params: &pagle.Effect_GiveBitsParams{
							GiveBitsParams: &pagle.GiveBitsParams{
								Amount:     1,
								FromUserId: common.IntegBitsRealPartnerOptedIn,
							},
						},
					},
					Domain: common.IntegDomain,
				})

				So(err, ShouldBeNil)

				_, err = pagleClient.SatisfyCondition(context.Background(), &pagle.SatisfyConditionReq{
					ConditionId: createConditionResp.Condition.ConditionId,
					OwnerId:     common.IntegConditionOwnerID,
					EffectSettings: []*pagle.EffectSettings{
						{
							EffectType: pagle.EffectType_GIVE_BITS,
							Settings: &pagle.EffectSettings_GiveBitsSettings{
								GiveBitsSettings: &pagle.GiveBitsSettings{
									PoolRecipients: []*pagle.PoolRecipientWeightedShare{
										{
											ToUserId:    common.IntegBitsRealPartnerOptedIn,
											ShareWeight: 1,
										},
									},
								},
							},
						},
					},
					Domain: common.IntegDomain,
				})

				Convey("we should not error", func() {
					So(err, ShouldBeNil)
				})

				Convey("we should cancel the invalid conditionParticipants and not satisfy them, but we should finalize the valid ones", func() {
					validConditionParticipant, err := common.PollConditionParticipantCompletion(
						createConditionParticipantResp.ConditionParticipant.ConditionParticipantId,
						createConditionParticipantResp.ConditionParticipant.OwnerId,
					)

					// IntegBitsRealPartnerOptedIn giving bits to themselves, which is invalid
					invalidConditionParticipant, err := common.PollConditionParticipantCompletion(
						createConditionParticipantResp2.ConditionParticipant.ConditionParticipantId,
						createConditionParticipantResp2.ConditionParticipant.OwnerId,
					)

					So(err, ShouldBeNil)
					So(invalidConditionParticipant.ConditionParticipantEndState, ShouldEqual, pagle.ConditionParticipantEndState_CONDITION_PARTICIPANT_FAILED_VALIDATION)
					So(invalidConditionParticipant.ConditionParticipantProcessingState, ShouldEqual, pagle.ConditionParticipantProcessingState_SUCCESS)

					So(validConditionParticipant.ConditionParticipantEndState, ShouldEqual, pagle.ConditionParticipantEndState_SATISFIED)
					So(validConditionParticipant.ConditionParticipantProcessingState, ShouldEqual, pagle.ConditionParticipantProcessingState_SUCCESS)
				})
			})
		})

		Convey("given an active condition exists, with effect settings defined", func() {
			pagleClient := common.GetPagleClient()

			createConditionResp, err := pagleClient.CreateCondition(context.Background(), &pagle.CreateConditionReq{
				OwnerId:                           common.IntegConditionOwnerID,
				Name:                              "An integ condition",
				SupportedEffects:                  []pagle.EffectType{pagle.EffectType_GIVE_BITS},
				DefineEffectSettingsWhenSatisfied: false,
				EffectSettings: []*pagle.EffectSettings{
					{
						EffectType: pagle.EffectType_GIVE_BITS,
						Settings: &pagle.EffectSettings_GiveBitsSettings{
							GiveBitsSettings: &pagle.GiveBitsSettings{
								PoolRecipients: []*pagle.PoolRecipientWeightedShare{
									{
										ToUserId:    common.IntegBitsRealPartnerOptedIn,
										ShareWeight: 1,
									},
								},
							},
						},
					},
				},
				Domain: common.IntegDomain,
			})

			So(err, ShouldBeNil)

			Convey("we should not error when effect settings are omitted in Satisfy call", func() {
				_, err = pagleClient.SatisfyCondition(context.Background(), &pagle.SatisfyConditionReq{
					ConditionId: createConditionResp.Condition.ConditionId,
					OwnerId:     common.IntegConditionOwnerID,
					Domain:      common.IntegDomain,
				})

				So(err, ShouldBeNil)
			})
		})
	})
}
