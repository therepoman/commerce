// +build integration

package getconditionparticipants

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pagle/backend/models"

	"code.justin.tv/commerce/pagle/integration_tests/common"
	pagle "code.justin.tv/commerce/pagle/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetConditionParticipants(t *testing.T) {
	Convey("given a GetConditionParticipants API", t, func() {
		Convey("when a condition exists for a user", func() {
			conditionOwnerID, err := common.GetUniqueConditionOwnerID()

			if err != nil {
				t.Error("Failed to create test owner UUID")
			}

			pagleClient := common.GetPagleClient()

			createConditionResp, err := pagleClient.CreateCondition(context.Background(), &pagle.CreateConditionReq{
				OwnerId:                           conditionOwnerID,
				Name:                              "An integ condition",
				SupportedEffects:                  []pagle.EffectType{pagle.EffectType_GIVE_BITS},
				DefineEffectSettingsWhenSatisfied: true,
				Domain:                            common.IntegDomain,
			})

			So(err, ShouldBeNil)

			testUserID, err := common.AddBitsForTestUser(1)

			So(err, ShouldBeNil)

			createConditionParticipantResp, err := pagleClient.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
				ConditionId:                 createConditionResp.Condition.ConditionId,
				ConditionOwnerId:            conditionOwnerID,
				ConditionParticipantOwnerId: testUserID,
				TtlSeconds:                  models.ConditionParticipantMaxTTL,
				Effect: &pagle.Effect{
					EffectType: pagle.EffectType_GIVE_BITS,
					Params: &pagle.Effect_GiveBitsParams{
						GiveBitsParams: &pagle.GiveBitsParams{
							Amount:     1,
							FromUserId: testUserID,
						},
					},
				},
				Domain: common.IntegDomain,
			})

			So(err, ShouldBeNil)

			Convey("we should return conditionParticipants when calling GetConditionParticipants", func() {
				getConditionsResp, err := pagleClient.GetConditionParticipants(context.Background(), &pagle.GetConditionParticipantsReq{
					ConditionParticipantOwnerId:  testUserID,
					ConditionParticipantEndState: pagle.ConditionParticipantEndState_PENDING_COMPLETION,
					ConditionOwnerId:             conditionOwnerID,
					Domain:                       common.IntegDomain,
				})

				So(err, ShouldBeNil)
				So(getConditionsResp, ShouldNotBeNil)
				So(getConditionsResp.ConditionParticipants, ShouldHaveLength, 1)
				So(getConditionsResp.ConditionParticipants[0].ConditionParticipantId, ShouldEqual, createConditionParticipantResp.ConditionParticipant.ConditionParticipantId)
				So(getConditionsResp.ConditionParticipants[0].ConditionParticipantEndState, ShouldEqual, pagle.ConditionParticipantEndState_PENDING_COMPLETION)
				So(getConditionsResp.ConditionParticipants[0].ConditionId, ShouldEqual, createConditionResp.Condition.ConditionId)
				So(getConditionsResp.ConditionParticipants[0].ConditionOwnerId, ShouldEqual, conditionOwnerID)
				So(getConditionsResp.ConditionParticipants[0].Domain, ShouldEqual, common.IntegDomain)
			})
		})
	})
}
