// Code generated by mockery v1.0.0
package ems_mock

import context "context"
import documents "code.justin.tv/gds/gds/extensions/ems/documents"

import mock "github.com/stretchr/testify/mock"

// EMSClient is an autogenerated mock type for the EMSClient type
type EMSClient struct {
	mock.Mock
}

// GetChannelExtension provides a mock function with given fields: ctx, extensionID, installationChannelID
func (_m *EMSClient) GetChannelExtension(ctx context.Context, extensionID string, installationChannelID string) (*documents.InstalledExtensionDocument, error) {
	ret := _m.Called(ctx, extensionID, installationChannelID)

	var r0 *documents.InstalledExtensionDocument
	if rf, ok := ret.Get(0).(func(context.Context, string, string) *documents.InstalledExtensionDocument); ok {
		r0 = rf(ctx, extensionID, installationChannelID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*documents.InstalledExtensionDocument)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string) error); ok {
		r1 = rf(ctx, extensionID, installationChannelID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// IsExtensionBitsEnabled provides a mock function with given fields: ctx, extensionID, installationChannelID
func (_m *EMSClient) IsExtensionBitsEnabled(ctx context.Context, extensionID string, installationChannelID string) (bool, error) {
	ret := _m.Called(ctx, extensionID, installationChannelID)

	var r0 bool
	if rf, ok := ret.Get(0).(func(context.Context, string, string) bool); ok {
		r0 = rf(ctx, extensionID, installationChannelID)
	} else {
		r0 = ret.Get(0).(bool)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string) error); ok {
		r1 = rf(ctx, extensionID, installationChannelID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
