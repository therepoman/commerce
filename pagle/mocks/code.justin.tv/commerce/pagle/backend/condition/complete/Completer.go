// Code generated by mockery v0.0.0-dev. DO NOT EDIT.

package conditioncompleter_mock

import (
	context "context"

	models "code.justin.tv/commerce/pagle/backend/models"
	mock "github.com/stretchr/testify/mock"
)

// Completer is an autogenerated mock type for the Completer type
type Completer struct {
	mock.Mock
}

// SatisfyCondition provides a mock function with given fields: ctx, domain, conditionID, ownerID, effectSettings
func (_m *Completer) SatisfyCondition(ctx context.Context, domain string, conditionID string, ownerID string, effectSettings []models.EffectSettings) error {
	ret := _m.Called(ctx, domain, conditionID, ownerID, effectSettings)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string, []models.EffectSettings) error); ok {
		r0 = rf(ctx, domain, conditionID, ownerID, effectSettings)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// TerminateCondition provides a mock function with given fields: ctx, domain, conditionID, ownerID, terminationState
func (_m *Completer) TerminateCondition(ctx context.Context, domain string, conditionID string, ownerID string, terminationState models.ConditionState) error {
	ret := _m.Called(ctx, domain, conditionID, ownerID, terminationState)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string, models.ConditionState) error); ok {
		r0 = rf(ctx, domain, conditionID, ownerID, terminationState)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
