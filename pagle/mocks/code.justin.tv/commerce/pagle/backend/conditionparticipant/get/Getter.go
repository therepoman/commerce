// Code generated by mockery v0.0.0-dev. DO NOT EDIT.

package conditionparticipantgetter_mock

import (
	context "context"

	mock "github.com/stretchr/testify/mock"

	models "code.justin.tv/commerce/pagle/backend/models"
)

// Getter is an autogenerated mock type for the Getter type
type Getter struct {
	mock.Mock
}

// GetConditionParticipant provides a mock function with given fields: ctx, conditionRunID, conditionParticipantID
func (_m *Getter) GetConditionParticipant(ctx context.Context, conditionRunID string, conditionParticipantID string) (models.ConditionParticipant, error) {
	ret := _m.Called(ctx, conditionRunID, conditionParticipantID)

	var r0 models.ConditionParticipant
	if rf, ok := ret.Get(0).(func(context.Context, string, string) models.ConditionParticipant); ok {
		r0 = rf(ctx, conditionRunID, conditionParticipantID)
	} else {
		r0 = ret.Get(0).(models.ConditionParticipant)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string) error); ok {
		r1 = rf(ctx, conditionRunID, conditionParticipantID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetConditionParticipantByDomainAndOwnerAndID provides a mock function with given fields: ctx, domain, conditionParticipantID, conditionParticipantOwnerID
func (_m *Getter) GetConditionParticipantByDomainAndOwnerAndID(ctx context.Context, domain string, conditionParticipantID string, conditionParticipantOwnerID string) (models.ConditionParticipant, error) {
	ret := _m.Called(ctx, domain, conditionParticipantID, conditionParticipantOwnerID)

	var r0 models.ConditionParticipant
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string) models.ConditionParticipant); ok {
		r0 = rf(ctx, domain, conditionParticipantID, conditionParticipantOwnerID)
	} else {
		r0 = ret.Get(0).(models.ConditionParticipant)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string, string) error); ok {
		r1 = rf(ctx, domain, conditionParticipantID, conditionParticipantOwnerID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetConditionParticipantsByDomainAndOwnerID provides a mock function with given fields: ctx, domain, conditionParticipantOwnerID, cursor
func (_m *Getter) GetConditionParticipantsByDomainAndOwnerID(ctx context.Context, domain string, conditionParticipantOwnerID string, cursor string) ([]*models.ConditionParticipant, string, error) {
	ret := _m.Called(ctx, domain, conditionParticipantOwnerID, cursor)

	var r0 []*models.ConditionParticipant
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string) []*models.ConditionParticipant); ok {
		r0 = rf(ctx, domain, conditionParticipantOwnerID, cursor)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*models.ConditionParticipant)
		}
	}

	var r1 string
	if rf, ok := ret.Get(1).(func(context.Context, string, string, string) string); ok {
		r1 = rf(ctx, domain, conditionParticipantOwnerID, cursor)
	} else {
		r1 = ret.Get(1).(string)
	}

	var r2 error
	if rf, ok := ret.Get(2).(func(context.Context, string, string, string) error); ok {
		r2 = rf(ctx, domain, conditionParticipantOwnerID, cursor)
	} else {
		r2 = ret.Error(2)
	}

	return r0, r1, r2
}

// GetConditionParticipantsByOwnerIDAndConditionOwnerID provides a mock function with given fields: ctx, domain, conditionParticipantOwnerID, conditionOwnerID, cursor, conditionParticipantEndState
func (_m *Getter) GetConditionParticipantsByOwnerIDAndConditionOwnerID(ctx context.Context, domain string, conditionParticipantOwnerID string, conditionOwnerID string, cursor string, conditionParticipantEndState models.ConditionParticipantEndState) ([]*models.ConditionParticipant, string, error) {
	ret := _m.Called(ctx, domain, conditionParticipantOwnerID, conditionOwnerID, cursor, conditionParticipantEndState)

	var r0 []*models.ConditionParticipant
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string, string, models.ConditionParticipantEndState) []*models.ConditionParticipant); ok {
		r0 = rf(ctx, domain, conditionParticipantOwnerID, conditionOwnerID, cursor, conditionParticipantEndState)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*models.ConditionParticipant)
		}
	}

	var r1 string
	if rf, ok := ret.Get(1).(func(context.Context, string, string, string, string, models.ConditionParticipantEndState) string); ok {
		r1 = rf(ctx, domain, conditionParticipantOwnerID, conditionOwnerID, cursor, conditionParticipantEndState)
	} else {
		r1 = ret.Get(1).(string)
	}

	var r2 error
	if rf, ok := ret.Get(2).(func(context.Context, string, string, string, string, models.ConditionParticipantEndState) error); ok {
		r2 = rf(ctx, domain, conditionParticipantOwnerID, conditionOwnerID, cursor, conditionParticipantEndState)
	} else {
		r2 = ret.Error(2)
	}

	return r0, r1, r2
}

// GetPendingConditionParticipantsByConditionRunID provides a mock function with given fields: ctx, domain, conditionOwnerID, conditionRunID
func (_m *Getter) GetPendingConditionParticipantsByConditionRunID(ctx context.Context, domain string, conditionOwnerID string, conditionRunID string) (*[]*models.ConditionParticipant, error) {
	ret := _m.Called(ctx, domain, conditionOwnerID, conditionRunID)

	var r0 *[]*models.ConditionParticipant
	if rf, ok := ret.Get(0).(func(context.Context, string, string, string) *[]*models.ConditionParticipant); ok {
		r0 = rf(ctx, domain, conditionOwnerID, conditionRunID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*[]*models.ConditionParticipant)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, string, string) error); ok {
		r1 = rf(ctx, domain, conditionOwnerID, conditionRunID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
