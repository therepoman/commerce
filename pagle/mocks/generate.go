package mocks

// Condition DAO
//go:generate retool do mockery --name=DAO --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/dynamo/condition --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/dynamo/condition --outpkg=conditiondao_mock

// ConditionParticipant DAO
//go:generate retool do mockery --name=DAO --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/dynamo/conditionparticipant --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/dynamo/conditionparticipant --outpkg=conditionparticipantdao_mock

// ConditionAggregates DAO
//go:generate retool do mockery --name=DAO --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/dynamo/conditionaggregates --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/dynamo/conditionaggregates --outpkg=conditionaggregatesdao_mock

// Transaction DAO
//go:generate retool do mockery --name=DAO --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/dynamo/transaction --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/dynamo/transaction --outpkg=transactiondao_mock

// ConditionParticipant Completer
//go:generate retool do mockery --name=Completer --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/conditionparticipant/complete --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/conditionparticipant/complete --outpkg=conditionparticipantcompleter_mock

// ConditionParticipant Creator
//go:generate retool do mockery --name=Creator --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/conditionparticipant/create --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/conditionparticipant/create --outpkg=conditionparticipantcreator_mock

// ConditionParticipant Getter
//go:generate retool do mockery --name=Getter --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/conditionparticipant/get --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/conditionparticipant/get --outpkg=conditionparticipantgetter_mock

// Condition Completer
//go:generate retool do mockery --name=Completer --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/condition/complete --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/condition/complete --outpkg=conditioncompleter_mock

// Condition Creator
//go:generate retool do mockery --name=Creator --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/condition/create --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/condition/create --outpkg=conditioncreator_mock

// Condition EffectTotals
//go:generate retool do mockery --name=EffectTotals --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/condition/effecttotals --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/condition/effecttotals --outpkg=conditioneffecttotals_mock

// Condition Getter
//go:generate retool do mockery --name=Getter --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/condition/get --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/condition/get --outpkg=conditiongetter_mock

// ConditionRun Completer
//go:generate retool do mockery --name=Completer --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/conditionrun/complete --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/conditionrun/complete --outpkg=conditionruncompleter_mock

// ConditionRun Creator
//go:generate retool do mockery --name=Creator --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/conditionrun/create --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/conditionrun/create --outpkg=conditionruncreator_mock

// ConditionRun Getter
//go:generate retool do mockery --name=Getter --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/conditionrun/get --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/conditionrun/get --outpkg=conditionrungetter_mock

// Effect Controller
//go:generate retool do mockery --name=EffectController --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/effect --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/effect --outpkg=effectcontroller_mock

// GiveBits Tenant
//go:generate retool do mockery --name=GiveBitsTenant --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/effect/tenant/givebits --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/effect/tenant/givebits --outpkg=givebitstenant_mock

// SFN Client
//go:generate retool do mockery --name=SFNClient --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/sfn --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/sfn --outpkg=sfnclient_mock

// SNS Client
//go:generate retool do mockery --name=SNSClient --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/sns --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/sns --outpkg=snsclient_mock

// S3 Client
//go:generate retool do mockery --name=S3Client --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/s3 --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/s3 --outpkg=s3client_mock

// Redis Client
//go:generate retool do mockery --name=Client --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/redis --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/redis --outpkg=redis_mock

// GetConditions Cache
//go:generate retool do mockery --name=Cache --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/cache/getconditions --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/cache/getconditions --outpkg=getconditions_mock

// GetConditionParticipants Cache
//go:generate retool do mockery --name=Cache --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/cache/getconditionparticipants --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/cache/getconditionparticipants --outpkg=getconditionparticipants_mock

// Statter
//go:generate retool do mockery --name=Statter --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/stats --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/stats --outpkg=statter_mock

// Payday
//go:generate retool do mockery --name=Client --dir=$GOPATH/src/code.justin.tv/commerce/pagle/vendor/code.justin.tv/commerce/payday/client --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/payday/client --outpkg=payday_mock

// Payday RPC
//go:generate retool do mockery --name=Payday --dir=$GOPATH/src/code.justin.tv/commerce/pagle/vendor/code.justin.tv/commerce/payday/rpc/payday --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/payday/rpc/payday --outpkg=payday_mock

// Effect Static Config Poller
//go:generate retool do mockery --name=Poller --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/effectstaticconfig --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/effectstaticconfig --outpkg=effectstaticconfig_mock

// Pubsub Client
//go:generate retool do mockery --name=PubsubClient --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/pubsub --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/pubsub --outpkg=pubsub_mock

// Owl Client
//go:generate retool do mockery --name=OwlClient --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/owl --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/owl --outpkg=owlclient_mock

// Ripley Client
//go:generate retool do mockery --name=Client --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/ripley --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/ripley --outpkg=ripleyclient_mock

// EMS Client
//go:generate retool do mockery --name=EMSClient --dir=$GOPATH/src/code.justin.tv/commerce/pagle/backend/ems --output=$GOPATH/src/code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/ems --outpkg=ems_mock
