FROM golang:1.12-alpine as builder

ADD . /go/src/code.justin.tv/commerce/pagle
WORKDIR /go/src/code.justin.tv/commerce/pagle

RUN apk update && apk upgrade && \
    apk add -U --no-cache ca-certificates && \
    apk add --no-cache git && \
    apk add build-base

RUN GIT_COMMIT=$(git rev-list -1 HEAD) && go build -ldflags "-X main.Version=$GIT_COMMIT" -o /pagle main.go

# Runner Container

FROM alpine as runner

COPY --from=builder /pagle /pagle
COPY --from=builder /go/src/code.justin.tv/commerce/pagle/config/data /etc/pagle/config
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

ENTRYPOINT ["/pagle"]