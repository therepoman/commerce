region = "us-west-2"

environment = "production"

env = "prod"

aws_profile = "pagle-prod"

vpc_id = "vpc-08745210cb2589b1d"

security_group = "sg-060bad2a26ec73328"

subnets = "subnet-0696cc27709df6788,subnet-07f6ac70bdb20e1e2,subnet-09554410528775a87"

cluster_name = "nat-pagle-cluster"

image = "196902209557.dkr.ecr.us-west-2.amazonaws.com/commerce/pagle"

min_instances = 0

max_instances = 0

private_cidr_blocks = ["10.205.198.0/24", "10.205.197.0/24", "10.205.196.0/24"]

elasticache_replicas_per_node_group = "2"

elasticache_num_node_groups = "5"

elasticache_instance_type = "cache.r5.large"

cpu = 2048

// This ends up being 4096. Terracode doubles this value, for some reason.
memory = 2048

whitelisted_arns_for_privatelink = [
  "arn:aws:iam::021561903526:root", // payday
  "arn:aws:iam::787149559823:root", // graphql-prod
  "arn:aws:iam::028439334451:root", // helix
]

whitelisted_arns_for_sns = [
  "arn:aws:iam::021561903526:root", // payday
]

ripley_dns =  "prod.ripley.twitch.a2z.com"
ripley_private_zone = "ripley.twitch.a2z.com"
ripley_vpc_endpoint_service_name = "com.amazonaws.vpce.us-west-2.vpce-svc-0e9e8a2896b1d5e89"
