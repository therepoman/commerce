variable "table_name" {
  type = "string"
}

variable "index_name" {
  type = "string"
}

variable "autoscaling_role" {
  type = "string"
}

variable "min_read_capacity" {
  type    = "string"
  default = "5"
}

variable "max_read_capacity" {
  type    = "string"
  default = "10000"
}

variable "min_write_capacity" {
  type    = "string"
  default = "5"
}

variable "max_write_capacity" {
  type    = "string"
  default = "10000"
}

variable "read_utilization" {
  type    = "string"
  default = "50"
}

variable "write_utilization" {
  type    = "string"
  default = "50"
}

resource "aws_appautoscaling_target" "dynamodb_read_capacity_autoscaling" {
  max_capacity       = "${var.max_read_capacity}"
  min_capacity       = "${var.min_read_capacity}"
  resource_id        = "table/${var.table_name}/index/${var.index_name}"
  role_arn           = "${var.autoscaling_role}"
  scalable_dimension = "dynamodb:index:ReadCapacityUnits"
  service_namespace  = "dynamodb"
}

resource "aws_appautoscaling_policy" "dynamodb_read_capacity_autoscaling_policy" {
  name               = "${var.table_name}_${var.index_name}_read_scaling"
  resource_id        = "table/${var.table_name}/index/${var.index_name}"
  scalable_dimension = "dynamodb:index:ReadCapacityUnits"
  service_namespace  = "dynamodb"
  policy_type        = "TargetTrackingScaling"

  target_tracking_scaling_policy_configuration {
    target_value       = "${var.read_utilization}"
    scale_in_cooldown  = 0
    scale_out_cooldown = 0

    predefined_metric_specification {
      predefined_metric_type = "DynamoDBReadCapacityUtilization"
    }
  }

  depends_on = ["aws_appautoscaling_target.dynamodb_read_capacity_autoscaling"]
}

resource "aws_cloudwatch_metric_alarm" "throttled_read_events_alarm" {
  alarm_name          = "${var.table_name}_${var.index_name}_throttled_read_events_alarm"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "4"
  metric_name         = "ReadThrottleEvents"
  namespace           = "AWS/DynamoDB"
  period              = "300"
  statistic           = "Sum"
  threshold           = "0"
  alarm_description   = "This metric checks that there are no throttled read events for dynamo table ${var.table_name} index ${var.index_name}"
  treat_missing_data  = "notBreaching"

  dimensions = {
    IndexName = "${var.table_name}_${var.index_name}"
  }
}

resource "aws_appautoscaling_target" "dynamodb_write_capacity_autoscaling" {
  max_capacity       = "${var.max_write_capacity}"
  min_capacity       = "${var.min_write_capacity}"
  resource_id        = "table/${var.table_name}/index/${var.index_name}"
  role_arn           = "${var.autoscaling_role}"
  scalable_dimension = "dynamodb:index:WriteCapacityUnits"
  service_namespace  = "dynamodb"
}

resource "aws_appautoscaling_policy" "dynamodb_write_capacity_autoscaling_policy" {
  name               = "${var.table_name}_${var.index_name}_write_scaling"
  resource_id        = "table/${var.table_name}/index/${var.index_name}"
  scalable_dimension = "dynamodb:index:WriteCapacityUnits"
  service_namespace  = "dynamodb"
  policy_type        = "TargetTrackingScaling"

  target_tracking_scaling_policy_configuration {
    target_value       = "${var.write_utilization}"
    scale_in_cooldown  = 0
    scale_out_cooldown = 0

    predefined_metric_specification {
      predefined_metric_type = "DynamoDBWriteCapacityUtilization"
    }
  }

  depends_on = ["aws_appautoscaling_target.dynamodb_write_capacity_autoscaling"]
}

resource "aws_cloudwatch_metric_alarm" "throttled_write_events_alarm" {
  alarm_name          = "${var.table_name}_${var.index_name}_throttled_write_events_alarm"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "4"
  metric_name         = "WriteThrottleEvents"
  namespace           = "AWS/DynamoDB"
  period              = "300"
  statistic           = "Sum"
  threshold           = "0"
  alarm_description   = "This metric checks that there are no throttled write events for dynamo table ${var.table_name} index ${var.index_name}"
  treat_missing_data  = "notBreaching"

  dimensions = {
    IndexName = "${var.table_name}_${var.index_name}"
  }
}
