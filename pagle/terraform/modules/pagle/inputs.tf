variable "image" {
  type        = "string"
  description = "The docker image and tag to run (e.g., 'nginx')"
}

variable "tag" {
  type    = "string"
  default = ""
}

variable "logging" {
  default     = "awslogs"
  description = "The logging driver to use (e.g., json, awslogs)"
}

variable "name" {
  type        = "string"
  description = "The name of the service"
}

variable "port" {
  default     = 3000
  description = "The port the container/application is serving traffic from."
}

variable "alb_port" {
  default     = 80
  description = "The port the load balancer is accepting traffic from."
}

variable "cluster" {
  description = "The ECS cluster name"
}

variable "security_group" {
  type = "string"
}

variable "deployment" {
  type = "map"

  default = {
    minimum_percent = 100
    maximum_percent = 200
  }
}

variable "desired_count" {
  description = "Number of containers that should be running (autoscaling will override this)"
  default     = 3
}

variable "subnets" {
  default = ""
}

variable "env_vars" {
  type    = "list"
  default = []
}

variable "command" {
  type    = "list"
  default = []
}

variable "environment" {
  type        = "string"
  description = "The environment to run the service in (e.g, staging/production)"
}

variable "env" {
  description = "The AWS environment. Must begin with one of the following:  dev (development),  stage (staging), or  prod (production)."
}

variable "vpc_id" {
  type = "string"
}

variable "region" {
  default     = "us-west-2"
  description = "The AWS Region for the resources to live in"
}

variable "cpu" {
  default     = 256
  description = "How many CPU units to use"
}

variable "memory" {
  default = 256
}

variable "canary_cpu" {
  default     = 256
  description = "How many CPU units to use for canary"
}

variable "canary_memory" {
  default = 256
}

variable "min_instances" {
  default = 3
}

variable "max_instances" {
  default = 3
}

variable "healthcheck" {
  type    = "string"
  default = "/ping"
}

# AWS ElastiCache
variable "elasticache_instance_type" {
  description = "Describes the type of instance to be used in our elasticache cluster"
  default     = "cache.m4.large"
}

variable "elasticache_auto_failover" {
  description = "Describes the type of failover behavior for the elasticache groups"
  default     = true
}

variable "redis_port" {
  description = "The port used by the redis server"
  default     = 6379
}

variable "elasticache_replicas_per_node_group" {
  description = "Number of read replicas per elasticache node group"
}

variable "elasticache_num_node_groups" {
  description = "Number of elasticache node groups"
}

variable "private_cidr_blocks" {
  type    = "list"
  default = []
}

variable "whitelisted_arns_for_privatelink" {
  type    = "list"
  default = []
}

variable "ripley_vpc_endpoint_service_name" {
  type        = "string"
  description = "The name of the Ripley VPC endpoint service"
}

variable "ripley_dns" {
  type        = "string"
  description = "DNS for Payday"
}

variable "ripley_private_zone" {
  type        = "string"
  description = "Zone for Payday"
}

variable "whitelisted_arns_for_sns" {
  type    = "list"
  default = []
}
