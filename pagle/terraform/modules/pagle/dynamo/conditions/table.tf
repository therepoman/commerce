locals {
  table_name                                 = "conditions-${var.env}"
  domain_owner_id_key                        = "domain-owner_id"
  condition_id_key                           = "condition_id"
  condition_state_key                        = "condition_state"
  condition_state_gsi_name                   = "${local.domain_owner_id_key}-${local.condition_state_key}-index"
  owner_id_key                               = "owner_id"
  owner_id_gsi_name                          = "${local.owner_id_key}-${local.condition_id_key}-index"
  extension_installation_channel_id_key      = "extension_installation_channel_id"
  extension_installation_channel_id_gsi_name = "${local.extension_installation_channel_id_key}-${local.condition_id_key}-index"
}

resource "aws_dynamodb_table" "table" {
  name             = "${local.table_name}"
  hash_key         = "${local.domain_owner_id_key}"
  range_key        = "${local.condition_id_key}"
  stream_enabled   = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"
  billing_mode     = "PAY_PER_REQUEST"

  global_secondary_index {
    hash_key        = "${local.domain_owner_id_key}"
    range_key       = "${local.condition_state_key}"
    name            = "${local.condition_state_gsi_name}"
    projection_type = "ALL"
  }

  global_secondary_index {
    hash_key        = "${local.owner_id_key}"
    range_key       = "${local.condition_id_key}"
    name            = "${local.owner_id_gsi_name}"
    projection_type = "ALL"
  }

  global_secondary_index {
    hash_key        = "${local.extension_installation_channel_id_key}"
    range_key       = "${local.condition_id_key}"
    name            = "${local.extension_installation_channel_id_gsi_name}"
    projection_type = "ALL"
  }

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "${local.domain_owner_id_key}"
    type = "S"
  }

  attribute {
    name = "${local.condition_id_key}"
    type = "S"
  }

  attribute {
    name = "${local.condition_state_key}"
    type = "S"
  }

  attribute {
    name = "${local.owner_id_key}"
    type = "S"
  }

  attribute {
    name = "${local.extension_installation_channel_id_key}"
    type = "S"
  }
}

module "scheduled_backups" {
  source                   = "../../../dynamo_scheduled_backup_event"
  table_name               = "${local.table_name}"
  dynamo_backup_lambda_arn = "${var.dynamo_backup_lambda_arn}"
}
