locals {
  name                                                         = "condition_participants"
  table_name                                                   = "condition-participants-${var.env}"
  condition_participant_id_key                                 = "condition_participant_id"
  domain_owner_id_key                                          = "domain-owner_id"
  condition_run_id_key                                         = "condition_run_id"
  condition_participant_end_state_key                          = "condition_participant_end_state"
  domain_condition_owner_id_condition_participant_owner_id_key = "domain-condition_owner_id-condition_participant_owner_id"

  owner_gsi_name                             = "${local.domain_owner_id_key}-${local.condition_participant_id_key}-index"
  condition_owner_participant_owner_gsi_name = "${local.domain_condition_owner_id_condition_participant_owner_id_key}-${local.condition_participant_end_state_key}-index"

  owner_id_key      = "owner_id"
  owner_id_gsi_name = "${local.owner_id_key}-${local.condition_participant_id_key}-index"

  condition_owner_id_key      = "condition_owner_id"
  condition_owner_id_gsi_name = "${local.condition_owner_id_key}-${local.condition_participant_id_key}-index"
}

resource "aws_dynamodb_table" "table" {
  name             = "${local.table_name}"
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"
  billing_mode     = "PAY_PER_REQUEST"
  hash_key         = "${local.condition_run_id_key}"
  range_key        = "${local.condition_participant_id_key}"

  // TODO: we can probably figure out a way to get rid of this GSI
  global_secondary_index {
    hash_key        = "${local.domain_owner_id_key}"
    range_key       = "${local.condition_participant_id_key}"
    name            = "${local.owner_gsi_name}"
    projection_type = "ALL"
  }

  global_secondary_index {
    hash_key        = "${local.domain_condition_owner_id_condition_participant_owner_id_key}"
    range_key       = "${local.condition_participant_end_state_key}"
    name            = "${local.condition_owner_participant_owner_gsi_name}"
    projection_type = "ALL"
  }

  global_secondary_index {
    hash_key        = "${local.owner_id_key}"
    range_key       = "${local.condition_participant_id_key}"
    name            = "${local.owner_id_gsi_name}"
    projection_type = "ALL"
  }

  global_secondary_index {
    hash_key        = "${local.condition_owner_id_key}"
    range_key       = "${local.condition_participant_id_key}"
    name            = "${local.condition_owner_id_gsi_name}"
    projection_type = "ALL"
  }

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "${local.condition_participant_id_key}"
    type = "S"
  }

  attribute {
    name = "${local.domain_owner_id_key}"
    type = "S"
  }

  attribute {
    name = "${local.condition_run_id_key}"
    type = "S"
  }

  attribute {
    name = "${local.condition_participant_end_state_key}"
    type = "S"
  }

  attribute {
    name = "${local.domain_condition_owner_id_condition_participant_owner_id_key}"
    type = "S"
  }

  attribute {
    name = "${local.owner_id_key}"
    type = "S"
  }

  attribute {
    name = "${local.condition_owner_id_key}"
    type = "S"
  }
}

module "scheduled_backups" {
  source                   = "../../../dynamo_scheduled_backup_event"
  table_name               = "${local.table_name}"
  dynamo_backup_lambda_arn = "${var.dynamo_backup_lambda_arn}"
}

output "stream_arn" {
  value = "${aws_dynamodb_table.table.stream_arn}"
}
