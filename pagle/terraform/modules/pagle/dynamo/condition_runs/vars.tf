variable "env" {
  description = "Whether it's prod, dev, etc"
}

variable "dynamo_backup_lambda_arn" {
  description = "ARN for lambda function that performs dynamo backups"
}
