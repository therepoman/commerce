locals {
  table_name = "condition-runs-${var.env}"
  hash_key   = "condition_run_id"
}

resource "aws_dynamodb_table" "table" {
  name             = "${local.table_name}"
  hash_key         = "${local.hash_key}"
  stream_enabled   = "true"
  stream_view_type = "NEW_AND_OLD_IMAGES"
  billing_mode     = "PAY_PER_REQUEST"

  point_in_time_recovery {
    enabled = true
  }

  attribute {
    name = "${local.hash_key}"
    type = "S"
  }

  ttl {
    attribute_name = "ttl"
    enabled        = true
  }
}

module "scheduled_backups" {
  source                   = "../../../dynamo_scheduled_backup_event"
  table_name               = "${local.table_name}"
  dynamo_backup_lambda_arn = "${var.dynamo_backup_lambda_arn}"
}
