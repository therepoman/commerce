resource "aws_s3_bucket" "finalized-condition-participants" {
  bucket = "pagle-${var.env}-finalized-condition-participants"
  acl    = "private"

  lifecycle_rule {
    enabled = true

    expiration {
      days = 14
    }
  }
}

resource "aws_s3_bucket" "effect-static-config" {
  bucket = "pagle-${var.env}-effect-static-config"
  acl    = "private"
}

resource "aws_s3_bucket" "lambdas" {
  bucket = "pagle-lambdas-${var.env}"
  acl    = "private"
}
