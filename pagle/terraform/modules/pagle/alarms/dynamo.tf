// Conditions DynamoDB table throttling
resource "aws_cloudwatch_metric_alarm" "conditions_throttled_read_events" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-conditions-read-throttles"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ReadThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "Conditions DynamoDB table reads are being throttled"
  insufficient_data_actions = []

  dimensions = {
    TableName = "conditions-${var.env}"
  }
}

resource "aws_cloudwatch_metric_alarm" "conditions_throttled_write_events" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-conditions-write-throttles"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "WriteThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "Conditions DynamoDB table writes are being throttled"
  insufficient_data_actions = []

  dimensions = {
    TableName = "conditions-${var.env}"
  }
}

// Condition runs DynamoDB table throttling
resource "aws_cloudwatch_metric_alarm" "condition_runs_throttled_read_events" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-condition-runs-read-throttles"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ReadThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "Conditions runs DynamoDB table reads are being throttled"
  insufficient_data_actions = []

  dimensions = {
    TableName = "condition-runs-${var.env}"
  }
}

resource "aws_cloudwatch_metric_alarm" "condition_runs_throttled_write_events" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-condition-runs-write-throttles"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "WriteThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "Condition runs DynamoDB table writes are being throttled"
  insufficient_data_actions = []

  dimensions = {
    TableName = "condition-runs-${var.env}"
  }
}

// Condition participants DynamoDB table throttling
resource "aws_cloudwatch_metric_alarm" "condition_participants_throttled_read_events" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-condition-participants-read-throttles"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ReadThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "Conditions participants DynamoDB table reads are being throttled"
  insufficient_data_actions = []

  dimensions = {
    TableName = "condition-participants-${var.env}"
  }
}

resource "aws_cloudwatch_metric_alarm" "condition_participants_throttled_write_events" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-condition-participants-write-throttles"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "WriteThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "Condition participants DynamoDB table writes are being throttled"
  insufficient_data_actions = []

  dimensions = {
    TableName = "condition-participants-${var.env}"
  }
}

// Condition aggregates DynamoDB table throttling
resource "aws_cloudwatch_metric_alarm" "condition_aggregates_throttled_read_events" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-condition-aggregates-read-throttles"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ReadThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "Conditions aggregates DynamoDB table reads are being throttled"
  insufficient_data_actions = []

  dimensions = {
    TableName = "condition-aggregates-${var.env}"
  }
}

resource "aws_cloudwatch_metric_alarm" "condition_aggregates_throttled_write_events" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-condition-aggregates-write-throttles"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "WriteThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "Condition aggregates DynamoDB table writes are being throttled"
  insufficient_data_actions = []

  dimensions = {
    TableName = "condition-aggregates-${var.env}"
  }
}
