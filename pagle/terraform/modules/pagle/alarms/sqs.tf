resource "aws_cloudwatch_metric_alarm" "condition_run_processing_dlq_count" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-sqs-condition-run-processing-dlq-count"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "Condition run processing SQS DLQ count is high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    QueueName = "${var.env}-condition-run-processing-queue_deadletter"
  }
}

resource "aws_cloudwatch_metric_alarm" "condition_participant_processing_dlq_count" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-sqs-condition-participant-processing-dlq-count"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "Condition participant processing SQS DLQ count is high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    QueueName = "${var.env}-condition-participant-processing-queue_deadletter"
  }
}

resource "aws_cloudwatch_metric_alarm" "condition_participant_cleanup_dlq_count" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-sqs-condition-participant-cleanup-dlq-count"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "Condition participant cleanup SQS DLQ count is high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    QueueName = "${var.env}-condition-participant-cleanup-queue_deadletter"
  }
}

resource "aws_cloudwatch_metric_alarm" "condition_aggregates_retry_dlq_count" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-sqs-condition-aggregates-retry-dlq-count"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "Condition aggregates retry SQS DLQ count is high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    QueueName = "${var.env}-condition-aggregates-retry-queue_deadletter"
  }
}
