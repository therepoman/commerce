// Overall
resource "aws_cloudwatch_metric_alarm" "availability_alert" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-API-availability-alert"
  comparison_operator       = "LessThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  threshold                 = "0.99"
  alarm_description         = "API availability dropped below 99% for 3 periods"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  metric_query {
    id          = "e1"
    expression  = "(FILL(m1,1)-m2)/FILL(m1,1)"
    label       = "Availability"
    return_data = "true"
  }

  metric_query {
    id = "m1"

    metric {
      metric_name = "counter.twirp.total.requests"
      namespace   = "${var.pagle_namespace}"
      period      = "300"
      stat        = "Sum"
      unit        = "Count"

      dimensions = {
        Region  = "us-west-2"
        Service = "pagle"
        Stage   = "${var.env}"
      }
    }
  }

  metric_query {
    id = "m2"

    metric {
      metric_name = "counter.twirp.status_codes.total.500"
      namespace   = "${var.pagle_namespace}"
      period      = "300"
      stat        = "Sum"
      unit        = "Count"

      dimensions = {
        Region  = "us-west-2"
        Service = "pagle"
        Stage   = "${var.env}"
      }
    }
  }
}

// CreateCondition
resource "aws_cloudwatch_metric_alarm" "create_condition_errors" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-CreateCondition-500-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.CreateCondition.500"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  unit                      = "Count"
  alarm_description         = "CreateCondition API 500 errors"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "create_condition_latency_p50" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-CreateCondition-latency-p50"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.CreateCondition.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p50"
  threshold                 = "0.4"
  unit                      = "Seconds"
  alarm_description         = "CreateCondition API latency p50"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "create_condition_latency_p90" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-CreateCondition-latency-p90"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.CreateCondition.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.6"
  unit                      = "Seconds"
  alarm_description         = "CreateCondition API latency p90"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "create_condition_latency_p99" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-CreateCondition-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.CreateCondition.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.8"
  unit                      = "Seconds"
  alarm_description         = "CreateCondition API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

// CreateConditionParticipant
resource "aws_cloudwatch_metric_alarm" "create_condition_participant_errors" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-CreateConditionParticipant-500-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.CreateConditionParticipant.500"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  unit                      = "Count"
  alarm_description         = "CreateConditionParticipant API 500 errors"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "create_condition_participant_latency_p50" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-CreateConditionParticipant-latency-p50"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.CreateConditionParticipant.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p50"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "CreateConditionParticipant API latency p50"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "create_condition_participant_latency_p90" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-CreateConditionParticipant-latency-p90"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.CreateConditionParticipant.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "1"
  unit                      = "Seconds"
  alarm_description         = "CreateConditionParticipant API latency p90"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "create_condition_participant_latency_p99" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-CreateConditionParticipant-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.CreateConditionParticipant.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "1.5"
  unit                      = "Seconds"
  alarm_description         = "CreateConditionParticipant API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

// GetCondition
resource "aws_cloudwatch_metric_alarm" "get_condition_errors" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-GetCondition-500-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.GetCondition.500"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  unit                      = "Count"
  alarm_description         = "GetCondition API 500 errors"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_condition_latency_p50" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-GetCondition-latency-p50"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetCondition.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p50"
  threshold                 = "0.3"
  unit                      = "Seconds"
  alarm_description         = "GetCondition API latency p50"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_condition_latency_p90" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-GetCondition-latency-p90"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetCondition.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "GetCondition API latency p90"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_condition_latency_p99" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-GetCondition-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetCondition.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.7"
  unit                      = "Seconds"
  alarm_description         = "GetCondition API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

// GetConditions
resource "aws_cloudwatch_metric_alarm" "get_conditions_errors" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-GetConditions-500-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.GetConditions.500"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  unit                      = "Count"
  alarm_description         = "GetConditions API 500 errors"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_conditions_latency_p50" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-GetConditions-latency-p50"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetConditions.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p50"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "GetConditions API latency p50"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_conditions_latency_p90" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-GetConditions-latency-p90"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetConditions.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.8"
  unit                      = "Seconds"
  alarm_description         = "GetConditions API latency p90"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_conditions_latency_p99" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-GetConditions-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetConditions.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "1"
  unit                      = "Seconds"
  alarm_description         = "GetConditions API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

// GetConditionParticipants
resource "aws_cloudwatch_metric_alarm" "get_condition_participants_errors" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-GetConditionParticipants-500-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.GetConditionParticipants.500"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  unit                      = "Count"
  alarm_description         = "GetConditionParticipants API 500 errors"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_condition_participants_latency_p50" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-GetConditionParticipants-latency-p50"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetConditionParticipants.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p50"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "GetConditionParticipants API latency p50"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_condition_participants_latency_p90" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-GetConditionParticipants-latency-p90"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetConditionParticipants.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.8"
  unit                      = "Seconds"
  alarm_description         = "GetConditionParticipants API latency p90"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_condition_participants_latency_p99" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-GetConditionParticipants-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetConditionParticipants.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "1"
  unit                      = "Seconds"
  alarm_description         = "GetConditionParticipants API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

// BatchGetConditionSummary
resource "aws_cloudwatch_metric_alarm" "batch_get_condition_summary_errors" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-BatchGetConditionSummary-500-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.BatchGetConditionSummary.500"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  unit                      = "Count"
  alarm_description         = "BatchGetConditionSummary API 500 errors"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "batch_get_condition_summary_latency_p50" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-BatchGetConditionSummary-latency-p50"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.BatchGetConditionSummary.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p50"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "BatchGetConditionSummary API latency p50"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "batch_get_condition_summary_latency_p90" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-BatchGetConditionSummary-latency-p90"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.BatchGetConditionSummary.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.8"
  unit                      = "Seconds"
  alarm_description         = "BatchGetConditionSummary API latency p90"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "batch_get_condition_summary_latency_p99" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-BatchGetConditionSummary-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.BatchGetConditionSummary.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "1"
  unit                      = "Seconds"
  alarm_description         = "BatchGetConditionSummary API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

// GetConditionSummary
resource "aws_cloudwatch_metric_alarm" "get_condition_summary_errors" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-GetConditionSummary-500-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.GetConditionSummary.500"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  unit                      = "Count"
  alarm_description         = "GetConditionSummary API 500 errors"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_condition_summary_latency_p50" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-GetConditionSummary-latency-p50"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetConditionSummary.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p50"
  threshold                 = "0.3"
  unit                      = "Seconds"
  alarm_description         = "GetConditionSummary API latency p50"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_condition_summary_latency_p90" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-GetConditionSummary-latency-p90"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetConditionSummary.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "GetConditionSummary API latency p90"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_condition_summary_latency_p99" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-GetConditionSummary-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetConditionSummary.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.7"
  unit                      = "Seconds"
  alarm_description         = "GetConditionSummary API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

// SatisfyCondition
resource "aws_cloudwatch_metric_alarm" "satisfy_condition_errors" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-SatisfyCondition-500-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.SatisfyCondition.500"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  unit                      = "Count"
  alarm_description         = "SatisfyCondition API 500 errors"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "satisfy_condition_latency_p50" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-SatisfyCondition-latency-p50"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.SatisfyCondition.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p50"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "SatisfyCondition API latency p50"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "satisfy_condition_latency_p90" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-SatisfyCondition-latency-p90"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.SatisfyCondition.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "1"
  unit                      = "Seconds"
  alarm_description         = "SatisfyCondition API latency p90"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "satisfy_condition_latency_p99" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-SatisfyCondition-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.SatisfyCondition.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "1.5"
  unit                      = "Seconds"
  alarm_description         = "SatisfyCondition API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

// CancelCondition
resource "aws_cloudwatch_metric_alarm" "cancel_condition_errors" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-CancelCondition-500-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.CancelCondition.500"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  unit                      = "Count"
  alarm_description         = "CancelCondition API 500 errors"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "cancel_condition_latency_p50" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-CancelCondition-latency-p50"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.CancelCondition.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p50"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "CancelCondition API latency p50"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "cancel_condition_latency_p90" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-CancelCondition-latency-p90"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.CancelCondition.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "1"
  unit                      = "Seconds"
  alarm_description         = "CancelCondition API latency p90"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "cancel_condition_latency_p99" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-CancelCondition-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.CancelCondition.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "1.5"
  unit                      = "Seconds"
  alarm_description         = "CancelCondition API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

// BatchGetCondition
resource "aws_cloudwatch_metric_alarm" "batch_get_condition_errors" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-BatchGetCondition-500-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.BatchGetCondition.500"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  unit                      = "Count"
  alarm_description         = "BatchGetCondition API 500 errors"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "batch_get_condition_latency_p50" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-BatchGetCondition-latency-p50"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.BatchGetCondition.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p50"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "BatchGetCondition API latency p50"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "batch_get_condition_latency_p90" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-BatchGetCondition-latency-p90"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.BatchGetCondition.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.8"
  unit                      = "Seconds"
  alarm_description         = "BatchGetCondition API latency p90"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "batch_get_condition_latency_p99" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-BatchGetCondition-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.BatchGetCondition.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "1"
  unit                      = "Seconds"
  alarm_description         = "BatchGetCondition API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

// GetConditionParticipant
resource "aws_cloudwatch_metric_alarm" "get_condition_participant_errors" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-GetConditionParticipant-500-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.GetConditionParticipant.500"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  unit                      = "Count"
  alarm_description         = "GetConditionParticipant API 500 errors"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_condition_participant_latency_p50" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-GetConditionParticipant-latency-p50"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetConditionParticipant.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p50"
  threshold                 = "0.3"
  unit                      = "Seconds"
  alarm_description         = "GetConditionParticipant API latency p50"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_condition_participant_latency_p90" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-GetConditionParticipant-latency-p90"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetConditionParticipant.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "GetConditionParticipant API latency p90"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_condition_participant_latency_p99" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-GetConditionParticipant-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetConditionParticipant.response"
  namespace                 = "${var.pagle_namespace}"
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.7"
  unit                      = "Seconds"
  alarm_description         = "GetConditionParticipant API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = "us-west-2"
    Service  = "pagle"
    Stage    = "${var.env}"
    Substage = "primary"
  }
}
