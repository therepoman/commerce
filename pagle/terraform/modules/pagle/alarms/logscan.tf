// Errors
resource "aws_cloudwatch_metric_alarm" "pagle_error_warning" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-pagle-logscan-error-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "7"
  metric_name               = "pagle-errors"
  namespace                 = "${var.pagle_logscan_namespace}"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "15"
  unit                      = "Count"
  alarm_description         = "Low volume of error logs"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "pagle_error_alert" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-pagle-logscan-error-alert"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "7"
  metric_name               = "pagle-errors"
  namespace                 = "${var.pagle_logscan_namespace}"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "30"
  unit                      = "Count"
  alarm_description         = "High volume of error logs"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}

// Panics
resource "aws_cloudwatch_metric_alarm" "pagle_panic_alert" {
  count                     = "${var.count}"
  alarm_name                = "${var.env}-pagle-logscan-panic-alert"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  metric_name               = "pagle-panics"
  namespace                 = "${var.pagle_logscan_namespace}"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "High volume of panic logs"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
}
