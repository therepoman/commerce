variable "env" {
  description = "The AWS environment. Must begin with one of the following:  dev (development),  stage (staging), or  prod (production)."
}

variable "pagle_namespace" {
  default = "pagle"
  type    = "string"
}

variable "pagle_logscan_namespace" {
  default = "PagleLogs"
  type    = "string"
}

variable "count" {
  description = "The number of alarms to create. Mainly used to disable alarms."
}
