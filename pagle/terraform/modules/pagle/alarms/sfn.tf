resource "aws_cloudwatch_metric_alarm" "condition_run_processing_stepfn_executions_failed" {
  count                     = "${var.count}"
  alarm_name                = "condition-run-processing-stepfn-executions-failed"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ExecutionsFailed"
  namespace                 = "AWS/States"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "Condition run processing step function execution failures are high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    StateMachineArn = "arn:aws:states:us-west-2:196902209557:stateMachine:condition_run_processing_state_machine"
  }
}

resource "aws_cloudwatch_metric_alarm" "condition_participant_timeout_stepfn_executions_failed" {
  count                     = "${var.count}"
  alarm_name                = "condition-participant-timeout-stepfn-executions-failed"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ExecutionsFailed"
  namespace                 = "AWS/States"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "Condition participant timeout step function execution failures are high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    StateMachineArn = "arn:aws:states:us-west-2:196902209557:stateMachine:condition_participant_timeout_state_machine"
  }
}

resource "aws_cloudwatch_metric_alarm" "condition_timeout_stepfn_executions_failed" {
  count                     = "${var.count}"
  alarm_name                = "condition-timeout-stepfn-executions-failed"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ExecutionsFailed"
  namespace                 = "AWS/States"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "Condition timeout step function execution failures are high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    StateMachineArn = "arn:aws:states:us-west-2:196902209557:stateMachine:condition_timeout_state_machine"
  }
}
