// condition participant processing
module "condition_participant_processing_queue" {
  source             = "../sqs_queue"
  queue_name         = "${var.env}-condition-participant-processing-queue"
  subscribe_to_topic = "${aws_sns_topic.condition_participant_processing_topic.arn}"
}

resource "aws_sns_topic_subscription" "condition_participant_processing_topic_queue_subscription" {
  topic_arn = "${aws_sns_topic.condition_participant_processing_topic.arn}"
  protocol  = "sqs"
  endpoint  = "${module.condition_participant_processing_queue.queue_arn}"
}

// condition participant cleanup
module "condition_participant_cleanup_queue" {
  source             = "../sqs_queue"
  queue_name         = "${var.env}-condition-participant-cleanup-queue"
  subscribe_to_topic = "${aws_sns_topic.condition_participant_cleanup_topic.arn}"
}

resource "aws_sns_topic_subscription" "condition_participant_cleanup_topic_queue_subscription" {
  topic_arn = "${aws_sns_topic.condition_participant_cleanup_topic.arn}"
  protocol  = "sqs"
  endpoint  = "${module.condition_participant_cleanup_queue.queue_arn}"
}

// condition run processing
module "condition_run_processing_queue" {
  source             = "../sqs_queue"
  queue_name         = "${var.env}-condition-run-processing-queue"
  subscribe_to_topic = "${aws_sns_topic.condition_run_processing_topic.arn}"
}

resource "aws_sns_topic_subscription" "condition_processing_topic_queue_subscription" {
  topic_arn = "${aws_sns_topic.condition_run_processing_topic.arn}"
  protocol  = "sqs"
  endpoint  = "${module.condition_run_processing_queue.queue_arn}"
}

// condition aggregates retry
module "condition_aggregates_retry_queue" {
  source             = "../sqs_queue"
  queue_name         = "${var.env}-condition-aggregates-retry-queue"
  subscribe_to_topic = "${aws_sns_topic.condition_aggregates_retry_topic.arn}"
}

resource "aws_sns_topic_subscription" "condition_aggregates_retry_topic_queue_subscription" {
  topic_arn = "${aws_sns_topic.condition_aggregates_retry_topic.arn}"
  protocol  = "sqs"
  endpoint  = "${module.condition_aggregates_retry_queue.queue_arn}"
}
