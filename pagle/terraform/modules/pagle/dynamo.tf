module "condition_runs_table" {
  source                   = "./dynamo/condition_runs"
  env                      = "${var.env}"
  dynamo_backup_lambda_arn = "${module.dynamo_backup_lambda.lambda_arn}"
}

module "condition_participants_table" {
  source                   = "./dynamo/condition_participants"
  env                      = "${var.env}"
  dynamo_backup_lambda_arn = "${module.dynamo_backup_lambda.lambda_arn}"
}

module "conditions_table" {
  source                   = "./dynamo/conditions"
  env                      = "${var.env}"
  dynamo_backup_lambda_arn = "${module.dynamo_backup_lambda.lambda_arn}"
}

module "condition_aggregates_table" {
  source                   = "./dynamo/condition_aggregates"
  env                      = "${var.env}"
  dynamo_backup_lambda_arn = "${module.dynamo_backup_lambda.lambda_arn}"
}
