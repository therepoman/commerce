// NLB -> ALB Proxy
// Really only useful so that we can private link Pagle -> Payday, allowing us to call Pagle Twirp
// from Jenkins.
resource "aws_lb" "nlb_proxy" {
  name               = "pagle-proxy"
  internal           = true
  load_balancer_type = "network"
  subnets            = "${split(",", var.subnets)}"

  enable_deletion_protection = true
}

resource "aws_lb_target_group" "nlb_proxy" {
  name        = "pagle-proxy"
  port        = 443
  protocol    = "TLS"
  vpc_id      = "${var.vpc_id}"
  target_type = "ip"
}

resource "aws_lb_listener" "nlb_proxy" {
  load_balancer_arn = "${aws_lb.nlb_proxy.arn}"
  port              = "443"
  protocol          = "TLS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "${module.service.cert}"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.nlb_proxy.arn}"
  }
}

resource "aws_s3_bucket" "nlb_proxy" {
  bucket = "pagle-${var.environment}-proxy-ips"
}
