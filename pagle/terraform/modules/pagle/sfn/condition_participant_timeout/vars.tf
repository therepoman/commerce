variable "region" {
  description = "The region where AWS resources live"
}

variable "condition_participant_timeout_lambda_arn" {
  description = "The ARN of the condition_participant_timeout lambda"
}
