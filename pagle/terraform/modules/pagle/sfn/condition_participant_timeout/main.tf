resource "aws_iam_role" "condition_participant_timeout_sfn" {
  name = "condition_participant_timeout_sfn"

  assume_role_policy = "${data.aws_iam_policy_document.sfn_assume_role_policy_document.json}"
}

data "aws_iam_policy_document" "sfn_assume_role_policy_document" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"

      identifiers = [
        "states.${var.region}.amazonaws.com",
      ]
    }
  }
}

data "aws_iam_policy_document" "lambda_execution_policy_document" {
  version = "2012-10-17"

  statement {
    actions = [
      "lambda:InvokeFunction",
      "states:StartExecution",
    ]

    effect = "Allow"

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_role_policy" "lambda_execution" {
  name   = "condition_participant_timeout_sfn_lambda_execution"
  role   = "${aws_iam_role.condition_participant_timeout_sfn.id}"
  policy = "${data.aws_iam_policy_document.lambda_execution_policy_document.json}"
}

resource "aws_sfn_state_machine" "condition_participant_timeout_state_machine" {
  name     = "condition_participant_timeout_state_machine"
  role_arn = "${aws_iam_role.condition_participant_timeout_sfn.arn}"

  definition = <<EOF
{
  "Comment": "A state machine that times out a condition participant after a set period of time.",
  "StartAt": "AwaitTimeout",
  "States": {
    "AwaitTimeout": {
      "Type" : "Wait",
      "SecondsPath": "$.ttl_seconds",
      "Next": "TimeoutConditionParticipant"
    },
    "TimeoutConditionParticipant": {
      "Type" : "Task",
      "Resource": "${var.condition_participant_timeout_lambda_arn}",
      "Next": "SuccessState",
      "Catch": [
          {
            "ErrorEquals": ["States.ALL"],
            "Next": "FailState"
          }
      ],
      "Retry": [{
        "ErrorEquals": [
          "States.ALL"
        ],
        "IntervalSeconds": 5,
        "MaxAttempts": 5,
        "BackoffRate": 3.0
      }]
    },
    "FailState": {
      "Type": "Fail"
    },
    "SuccessState": {
      "Type": "Succeed"
    }
  }
}
EOF
}
