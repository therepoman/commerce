variable "region" {
  description = "The region where AWS resources live"
}

variable "condition_timeout_lambda_arn" {
  description = "The ARN of the condition_timeout lambda"
}
