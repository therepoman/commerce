variable "region" {
  description = "The region where AWS resources live"
}

variable "finalize_condition_run_lambda_arn" {
  description = "The ARN of the finalize_condition_run lambda"
}

variable "process_effect_rollup_lambda_arn" {
  description = "The ARN of the process_effect_rollup lambda"
}

variable "enqueue_condition_participants_lambda_arn" {
  description = "The ARN of the enqueue_condition_participants lambda"
}
