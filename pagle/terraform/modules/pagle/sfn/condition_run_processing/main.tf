resource "aws_iam_role" "condition_run_processing_sfn" {
  name = "condition_run_processing_sfn"

  assume_role_policy = "${data.aws_iam_policy_document.sfn_assume_role_policy_document.json}"
}

data "aws_iam_policy_document" "sfn_assume_role_policy_document" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"

      identifiers = [
        "states.${var.region}.amazonaws.com",
      ]
    }
  }
}

data "aws_iam_policy_document" "lambda_execution_policy_document" {
  version = "2012-10-17"

  statement {
    actions = [
      "lambda:InvokeFunction",
      "states:StartExecution",
    ]

    effect = "Allow"

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_role_policy" "lambda_execution" {
  name   = "condition_run_processing_sfn_lambda_execution"
  role   = "${aws_iam_role.condition_run_processing_sfn.id}"
  policy = "${data.aws_iam_policy_document.lambda_execution_policy_document.json}"
}

resource "aws_sfn_state_machine" "condition_run_processing_state_machine" {
  name     = "condition_run_processing_state_machine"
  role_arn = "${aws_iam_role.condition_run_processing_sfn.arn}"

  definition = <<EOF
{
  "Comment": "A state machine that finalizes a condition run.",
  "StartAt": "FinalizeConditionRun",
  "States": {
    "FinalizeConditionRun": {
      "Type" : "Task",
      "Resource": "${var.finalize_condition_run_lambda_arn}",
      "Next": "ProcessEffectRollup",
      "Catch": [
          {
            "ErrorEquals": ["States.ALL"],
            "Next": "FailState"
          }
      ],
      "Retry": [{
        "ErrorEquals": [
          "States.ALL"
        ],
        "IntervalSeconds": 5,
        "MaxAttempts": 5,
        "BackoffRate": 3.0
      }]
    },
    "ProcessEffectRollup": {
      "Type" : "Task",
      "ResultPath": "$.process_effect_rollup_output",
      "Resource": "${var.process_effect_rollup_lambda_arn}",
      "Next": "HasParticipantsToProcess",
      "Catch": [
          {
            "ErrorEquals": ["States.ALL"],
            "Next": "FailState"
          }
      ],
      "Retry": [{
        "ErrorEquals": [
          "States.ALL"
        ],
        "IntervalSeconds": 5,
        "MaxAttempts": 5,
        "BackoffRate": 3.0
      }]
    },
    "HasParticipantsToProcess": {
      "Type": "Choice",
      "Comment": "Checks whether condition run has any participants to process",
      "Choices": [
        {
          "Variable": "$.has_participants_to_process",
          "BooleanEquals": false,
          "Next": "SuccessState"
        },
        {
          "Variable": "$.has_participants_to_process",
          "BooleanEquals": true,
          "Next": "EnqueueConditionParticipants"
        }
      ],
      "Default": "FailState"
    },
    "EnqueueConditionParticipants": {
      "Type" : "Task",
      "Resource": "${var.enqueue_condition_participants_lambda_arn}",
      "Next": "SuccessState",
      "Catch": [
          {
            "ErrorEquals": ["States.ALL"],
            "Next": "FailState"
          }
      ],
      "Retry": [{
        "ErrorEquals": [
          "States.ALL"
        ],
        "IntervalSeconds": 5,
        "MaxAttempts": 5,
        "BackoffRate": 3.0
      }]
    },
    "FailState": {
      "Type": "Fail"
    },
    "SuccessState": {
      "Type": "Succeed"
    }
  }
}
EOF
}
