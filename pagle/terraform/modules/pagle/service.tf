module "service" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//twitch-service?ref=7ea2d6c1c82592751c88f328c36d82bbe1087cda"

  name        = "${var.name}"
  image       = "${var.image}"
  memory      = "${var.memory}"
  logging     = "${var.logging}"
  cpu         = "${var.cpu}"
  alb_port    = "${var.alb_port}"
  env_vars    = "${var.env_vars}"
  command     = "${var.command}"
  launch_type = "FARGATE"

  counts = {
    min = "${var.min_instances}"
    max = "${var.max_instances}"
  }

  port = 8000
  dns  = "*.${var.name}-${var.environment}.internal.justin.tv"

  healthcheck = "${var.healthcheck}"

  cluster        = "${var.cluster}"
  security_group = "${var.security_group}"
  region         = "${var.region}"
  vpc_id         = "${var.vpc_id}"
  environment    = "${var.environment}"
  subnets        = "${var.subnets}"
}
