module "condition_timeout_sfn" {
  source                       = "./sfn/condition_timeout"
  region                       = "${var.region}"
  condition_timeout_lambda_arn = "${module.condition_timeout_lambda.lambda_arn}"
}

module "condition_participant_timeout_sfn" {
  source                                   = "./sfn/condition_participant_timeout"
  region                                   = "${var.region}"
  condition_participant_timeout_lambda_arn = "${module.condition_participant_timeout_lambda.lambda_arn}"
}

module "condition_run_processing_sfn" {
  source                                    = "./sfn/condition_run_processing"
  region                                    = "${var.region}"
  finalize_condition_run_lambda_arn         = "${module.finalize_condition_run_lambda.lambda_arn}"
  enqueue_condition_participants_lambda_arn = "${module.enqueue_condition_participants_lambda.lambda_arn}"
  process_effect_rollup_lambda_arn          = "${module.process_effect_rollup_lambda.lambda_arn}"
}
