module "pagle_alarms" {
  count                   = "${var.env == "prod" ? 1 : 0}"
  env                     = "${var.env}"
  source                  = "alarms"
  pagle_logscan_namespace = "${var.pagle_logscan_namespace}"
}
