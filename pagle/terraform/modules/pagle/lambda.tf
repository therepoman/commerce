module "dynamo_backup_lambda" {
  source = "../dynamo_backup_lambda"
}

module "condition_timeout_lambda" {
  source                  = "../lambda"
  env                     = "${var.env}"
  pagle_logscan_namespace = "${var.pagle_logscan_namespace}"
  security_groups         = ["${var.security_group}"]
  subnets                 = "${var.subnets}"
  lambda_name             = "condition-timeout"
  lambda_bucket           = "${aws_s3_bucket.lambdas.bucket}"
}

module "condition_participant_timeout_lambda" {
  source                  = "../lambda"
  env                     = "${var.env}"
  pagle_logscan_namespace = "${var.pagle_logscan_namespace}"
  security_groups         = ["${var.security_group}"]
  subnets                 = "${var.subnets}"
  lambda_name             = "condition-participant-timeout"
  lambda_bucket           = "${aws_s3_bucket.lambdas.bucket}"
}

module "condition_aggregates_retry_lambda" {
  source                  = "../lambda"
  env                     = "${var.env}"
  pagle_logscan_namespace = "${var.pagle_logscan_namespace}"
  security_groups         = ["${var.security_group}"]
  subnets                 = "${var.subnets}"
  lambda_name             = "condition-aggregates-retry"
  lambda_bucket           = "${aws_s3_bucket.lambdas.bucket}"
}

resource "aws_lambda_event_source_mapping" "sqs_to_condition_aggregates_retry_lambda" {
  event_source_arn = "${module.condition_aggregates_retry_queue.queue_arn}"
  function_name    = "${module.condition_aggregates_retry_lambda.lambda_arn}"

  // In order to facilitate simple retries/DLQ, the lambda can only handle a single message at a time.
  batch_size = 1
}

module "condition_run_processing_lambda" {
  source                  = "../lambda"
  env                     = "${var.env}"
  pagle_logscan_namespace = "${var.pagle_logscan_namespace}"
  security_groups         = ["${var.security_group}"]
  subnets                 = "${var.subnets}"
  lambda_name             = "condition-run-processing"
  lambda_bucket           = "${aws_s3_bucket.lambdas.bucket}"
}

resource "aws_lambda_event_source_mapping" "sqs_to_condition_processing_lambda" {
  event_source_arn = "${module.condition_run_processing_queue.queue_arn}"
  function_name    = "${module.condition_run_processing_lambda.lambda_arn}"

  // In order to facilitate simple retries/DLQ, the lambda can only handle a single message at a time.
  batch_size = 1
}

module "finalize_condition_run_lambda" {
  source                  = "../lambda"
  env                     = "${var.env}"
  pagle_logscan_namespace = "${var.pagle_logscan_namespace}"
  security_groups         = ["${var.security_group}"]
  subnets                 = "${var.subnets}"
  lambda_name             = "finalize-condition-run"
  memory_size             = 3008
  timeout                 = 300
  lambda_bucket           = "${aws_s3_bucket.lambdas.bucket}"
}

module "process_effect_rollup_lambda" {
  source                  = "../lambda"
  env                     = "${var.env}"
  pagle_logscan_namespace = "${var.pagle_logscan_namespace}"
  security_groups         = ["${var.security_group}"]
  subnets                 = "${var.subnets}"
  lambda_name             = "process-effect-rollup"
  memory_size             = 3008
  timeout                 = 300
  lambda_bucket           = "${aws_s3_bucket.lambdas.bucket}"
}

module "enqueue_condition_participants_lambda" {
  source                  = "../lambda"
  env                     = "${var.env}"
  pagle_logscan_namespace = "${var.pagle_logscan_namespace}"
  security_groups         = ["${var.security_group}"]
  subnets                 = "${var.subnets}"
  lambda_name             = "enqueue-condition-participants"
  memory_size             = 3008
  timeout                 = 300
  lambda_bucket           = "${aws_s3_bucket.lambdas.bucket}"
}

module "condition_participant_cleanup_lambda" {
  source                  = "../lambda"
  env                     = "${var.env}"
  pagle_logscan_namespace = "${var.pagle_logscan_namespace}"
  security_groups         = ["${var.security_group}"]
  subnets                 = "${var.subnets}"
  lambda_name             = "condition-participant-cleanup"
  lambda_bucket           = "${aws_s3_bucket.lambdas.bucket}"
}

resource "aws_lambda_event_source_mapping" "sqs_to_condition_participant_cleanup_lambda" {
  event_source_arn = "${module.condition_participant_cleanup_queue.queue_arn}"
  function_name    = "${module.condition_participant_cleanup_lambda.lambda_arn}"

  // In order to facilitate simple retries/DLQ, the lambda can only handle a single message at a time.
  batch_size = 1
}

module "condition_participant_processing_lambda" {
  source                  = "../lambda"
  env                     = "${var.env}"
  pagle_logscan_namespace = "${var.pagle_logscan_namespace}"
  security_groups         = ["${var.security_group}"]
  subnets                 = "${var.subnets}"
  lambda_name             = "condition-participant-processing"
  lambda_bucket           = "${aws_s3_bucket.lambdas.bucket}"
}

resource "aws_lambda_event_source_mapping" "sqs_to_condition_participant_processing_lambda" {
  event_source_arn = "${module.condition_participant_processing_queue.queue_arn}"
  function_name    = "${module.condition_participant_processing_lambda.lambda_arn}"

  // In order to facilitate simple retries/DLQ, the lambda can only handle a single message at a time.
  batch_size = 1
}

module "pdms_condition_aggregates" {
  source                  = "../lambda"
  env                     = "${var.env}"
  pagle_logscan_namespace = "${var.pagle_logscan_namespace}"
  security_groups         = ["${var.security_group}"]
  subnets                 = "${var.subnets}"
  lambda_name             = "pdms-condition-aggregates"
  lambda_bucket           = "${aws_s3_bucket.lambdas.bucket}"

  memory_size = 3008
  timeout     = 900
}

module "pdms_condition_participants" {
  source                  = "../lambda"
  env                     = "${var.env}"
  pagle_logscan_namespace = "${var.pagle_logscan_namespace}"
  security_groups         = ["${var.security_group}"]
  subnets                 = "${var.subnets}"
  lambda_name             = "pdms-condition-participants"
  lambda_bucket           = "${aws_s3_bucket.lambdas.bucket}"

  memory_size = 3008
  timeout     = 900
}

module "pdms_conditions" {
  source                  = "../lambda"
  env                     = "${var.env}"
  pagle_logscan_namespace = "${var.pagle_logscan_namespace}"
  security_groups         = ["${var.security_group}"]
  subnets                 = "${var.subnets}"
  lambda_name             = "pdms-conditions"
  lambda_bucket           = "${aws_s3_bucket.lambdas.bucket}"

  memory_size = 3008
  timeout     = 900
}

module "pdms_report_deletion" {
  source                  = "../lambda"
  env                     = "${var.env}"
  pagle_logscan_namespace = "${var.pagle_logscan_namespace}"
  security_groups         = ["${var.security_group}"]
  subnets                 = "${var.subnets}"
  lambda_name             = "pdms-report-deletion"
  lambda_bucket           = "${aws_s3_bucket.lambdas.bucket}"

  memory_size = 3008
  timeout     = 900
}

module "pdms_start_user_deletion" {
  source                  = "../lambda"
  env                     = "${var.env}"
  pagle_logscan_namespace = "${var.pagle_logscan_namespace}"
  security_groups         = ["${var.security_group}"]
  subnets                 = "${var.subnets}"
  lambda_name             = "pdms-start-user-deletion"
  lambda_bucket           = "${aws_s3_bucket.lambdas.bucket}"

  memory_size = 3008
  timeout     = 900
}
