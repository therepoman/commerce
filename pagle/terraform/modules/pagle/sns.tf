resource "aws_sns_topic" "condition_participant_processing_topic" {
  name = "${var.env}-condition-participant-processing"
}

resource "aws_sns_topic" "condition_participant_completion_topic" {
  name = "${var.env}-condition-participant-completion"
}

resource "aws_sns_topic" "condition_participant_cleanup_topic" {
  name = "${var.env}-condition-participant-cleanup"
}

resource "aws_sns_topic" "condition_run_processing_topic" {
  name = "${var.env}-condition-run-processing"
}

resource "aws_sns_topic" "condition_aggregates_retry_topic" {
  name = "${var.env}-condition-aggregates-retry"
}

resource "aws_sns_topic" "condition_completion_topic" {
  name = "${var.env}-condition-completion"
}

resource "aws_sns_topic_policy" "condition_completion_policy_attachment" {
  arn = "${aws_sns_topic.condition_completion_topic.arn}"

  policy = "${data.aws_iam_policy_document.sns-topic-policy.json}"
}

data "aws_iam_policy_document" "sns-topic-policy" {
  statement {
    actions = [
      "SNS:Subscribe",
      "SNS:SetTopicAttributes",
      "SNS:Receive",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
    ]

    effect = "Allow"

    principals {
      type = "AWS"

      identifiers = "${var.whitelisted_arns_for_sns}"
    }

    resources = [
      "${aws_sns_topic.condition_completion_topic.arn}",
    ]
  }
}
