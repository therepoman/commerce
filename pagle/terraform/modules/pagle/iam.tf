// Grant the Instance Profile full dynamo access
resource "aws_iam_role_policy_attachment" "dynamo_policy_attachment" {
  role       = "${module.service.service_role}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

resource "aws_iam_role_policy_attachment" "dynamo_policy_attachment_canary" {
  role       = "${module.service.canary_role}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

// Grant the Instance Profile full SQS access
resource "aws_iam_role_policy_attachment" "sqs_policy_attachment" {
  role       = "${module.service.service_role}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
}

resource "aws_iam_role_policy_attachment" "sqs_policy_attachment_canary" {
  role       = "${module.service.canary_role}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
}

// Grant the Instance Profile full SNS access
resource "aws_iam_role_policy_attachment" "sns_policy_attachment" {
  role       = "${module.service.service_role}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonSNSFullAccess"
}

resource "aws_iam_role_policy_attachment" "sns_policy_attachment_canary" {
  role       = "${module.service.canary_role}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonSNSFullAccess"
}

// Grant the Instance Profile full S3 access
resource "aws_iam_role_policy_attachment" "s3_policy_attachment" {
  role       = "${module.service.service_role}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_role_policy_attachment" "s3_policy_attachment_canary" {
  role       = "${module.service.canary_role}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

// Grant the Instance Profile full SFN access
resource "aws_iam_role_policy_attachment" "sfn_policy_attachment" {
  role       = "${module.service.service_role}"
  policy_arn = "arn:aws:iam::aws:policy/AWSStepFunctionsFullAccess"
}

resource "aws_iam_role_policy_attachment" "sfn_policy_attachment_canary" {
  role       = "${module.service.canary_role}"
  policy_arn = "arn:aws:iam::aws:policy/AWSStepFunctionsFullAccess"
}

// Grant the Instance Profile full ECS access
resource "aws_iam_role_policy_attachment" "ecs_policy_attachment" {
  role       = "${module.service.service_role}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonECS_FullAccess"
}

resource "aws_iam_role_policy_attachment" "ecs_policy_attachment_canary" {
  role       = "${module.service.canary_role}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonECS_FullAccess"
}
