variable "pagle_logscan_namespace" {
  default = "PagleLogs"
  type    = "string"
}

variable "pagle_log_group" {
  default = "pagle"
  type    = "string"
}

resource "aws_cloudwatch_log_metric_filter" "error_scan" {
  name           = "pagle-error-scan"
  pattern        = "level=error"
  log_group_name = "${var.pagle_log_group}"

  metric_transformation {
    name          = "pagle-errors"
    namespace     = "${var.pagle_logscan_namespace}"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "fatal_scan" {
  name           = "pagle-fatal-scan"
  pattern        = "level=fatal"
  log_group_name = "${var.pagle_log_group}"

  metric_transformation {
    name          = "pagle-fatals"
    namespace     = "${var.pagle_logscan_namespace}"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "panic_scan" {
  name           = "pagle-panic-scan"
  pattern        = "\"panic:\""
  log_group_name = "${var.pagle_log_group}"

  metric_transformation {
    name          = "pagle-panics"
    namespace     = "${var.pagle_logscan_namespace}"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "warning_scan" {
  name           = "pagle-warning-scan"
  pattern        = "level=warn"
  log_group_name = "${var.pagle_log_group}"

  metric_transformation {
    name          = "pagle-warns"
    namespace     = "${var.pagle_logscan_namespace}"
    value         = "1"
    default_value = "0"
  }
}
