module "privatelink" {
  source      = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-zone"
  name        = "pagle"
  environment = "${var.environment}"
}

module "privatelink-cert" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-cert"

  name        = "pagle"
  environment = "${var.environment}"
  zone_id     = "${module.privatelink.zone_id}"
  alb_dns     = "${module.service.dns}"
}

module "privatelink_a2z" {
  source           = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink"
  name             = "pagle"
  region           = "${var.region}"
  environment      = "${var.environment}"
  alb_dns          = "${module.service.dns}"
  cert_arn         = "${module.privatelink-cert.arn}"
  vpc_id           = "${var.vpc_id}"
  subnets          = "${split(",", var.subnets)}"
  whitelisted_arns = "${var.whitelisted_arns_for_privatelink}"
}

module "privatelink-ripley" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-service"

  name            = "ripley"
  zone            = "${var.ripley_private_zone}"
  dns             = "${var.ripley_dns}"
  endpoint        = "${var.ripley_vpc_endpoint_service_name}"
  environment     = "${var.environment}"
  security_groups = ["${var.security_group}"]
  subnets         = "${split(",", var.subnets)}"
  vpc_id          = "${var.vpc_id}"
}

// privatelinks with downstreams

module "privatelink-ldap" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint"
  name            = "ldap-a2z"
  endpoint        = "com.amazonaws.vpce.us-west-2.vpce-svc-0437151f68c61b808"
  security_groups = ["${var.security_group}"]
  vpc_id          = "${var.vpc_id}"
  subnets         = "${split(",", var.subnets)}"
  dns             = "ldap.twitch.a2z.com"
}
