resource "aws_iam_role" "lambda_iam_role" {
  name = "${var.lambda_name}_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

data "aws_iam_policy_document" "lambda_policy" {
  statement {
    effect = "Allow"

    actions = [
      "cloudwatch:PutMetricData",
    ]

    resources = ["*"]
  }
}

// TODO: policy attachment should be done per-lambda
resource "aws_iam_role_policy_attachment" "vpc_execution_attachment" {
  role       = "${aws_iam_role.lambda_iam_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}

resource "aws_iam_role_policy_attachment" "dynamo_attachment" {
  role       = "${aws_iam_role.lambda_iam_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

resource "aws_iam_role_policy_attachment" "sqs_attachment" {
  role       = "${aws_iam_role.lambda_iam_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
}

resource "aws_iam_role_policy_attachment" "sns_attachment" {
  role       = "${aws_iam_role.lambda_iam_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonSNSFullAccess"
}

resource "aws_iam_role_policy_attachment" "s3_attachment" {
  role       = "${aws_iam_role.lambda_iam_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_role_policy_attachment" "sfn_policy_attachment" {
  role       = "${aws_iam_role.lambda_iam_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AWSStepFunctionsFullAccess"
}

resource "aws_iam_role_policy" "lambda_policy" {
  role   = "${aws_iam_role.lambda_iam_role.name}"
  policy = "${data.aws_iam_policy_document.lambda_policy.json}"
}

resource "aws_lambda_function" "lambda" {
  function_name = "${var.lambda_name}"
  role          = "${aws_iam_role.lambda_iam_role.arn}"
  handler       = "main"
  runtime       = "go1.x"
  timeout       = "${var.timeout}"
  memory_size   = "${var.memory_size}"

  s3_bucket = "${var.lambda_bucket}"
  s3_key    = "${var.lambda_name}/latest/${var.lambda_name}.zip"

  environment {
    variables = {
      ENVIRONMENT = "${var.env}"
    }
  }

  vpc_config {
    security_group_ids = "${var.security_groups}"
    subnet_ids         = ["${split(",", var.subnets)}"]
  }

  lifecycle {
    ignore_changes = ["s3_key", "s3_bucket", "publish", "last_modified"]
  }
}

output "lambda_arn" {
  value = "${aws_lambda_function.lambda.arn}"
}

resource aws_cloudwatch_log_group "logs" {
  name              = "/aws/lambda/${aws_lambda_function.lambda.function_name}"
  retention_in_days = 30
}

resource aws_cloudwatch_log_metric_filter "logscan_errors" {
  log_group_name = "${aws_cloudwatch_log_group.logs.name}"

  metric_transformation {
    name          = "${aws_lambda_function.lambda.function_name}-logscan-errors-${var.env}"
    namespace     = "${var.pagle_logscan_namespace}"
    value         = "1"
    default_value = "0"
  }

  name    = "${aws_lambda_function.lambda.function_name}-logscan-errors-${var.env}"
  pattern = "level=error"
}

resource aws_cloudwatch_log_metric_filter "logscan_panics" {
  log_group_name = "${aws_cloudwatch_log_group.logs.name}"

  metric_transformation {
    name          = "${aws_lambda_function.lambda.function_name}-logscan-panics-${var.env}"
    namespace     = "${var.pagle_logscan_namespace}"
    value         = "1"
    default_value = "0"
  }

  name    = "${aws_lambda_function.lambda.function_name}-logscan-panics-${var.env}"
  pattern = "panic"
}
