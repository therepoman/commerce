variable "env" {}

variable "pagle_logscan_namespace" {}

variable "security_groups" {
  type = "list"
}

variable "subnets" {}

variable "lambda_name" {}

variable "timeout" {
  default = 60
}

variable "memory_size" {
  default = 1024
}

variable "lambda_bucket" {}
