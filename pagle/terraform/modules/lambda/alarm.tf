resource "aws_cloudwatch_metric_alarm" "lambda_errors" {
  count                     = "${var.env == "prod" ? 1 : 0}"
  alarm_name                = "${var.env}-${var.lambda_name}-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "Errors"
  namespace                 = "AWS/Lambda"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "${var.lambda_name} lambda has errors"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    FunctionName = "${aws_lambda_function.lambda.function_name}"
  }
}

resource "aws_cloudwatch_metric_alarm" "lambda_latency" {
  count                     = "${var.env == "prod" ? 1 : 0}"
  alarm_name                = "${var.env}-${var.lambda_name}-latency"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "Duration"
  namespace                 = "AWS/Lambda"
  period                    = "300"
  statistic                 = "Average"
  threshold                 = "${var.timeout * 0.8}"
  unit                      = "Milliseconds"
  alarm_description         = "${var.lambda_name} lambda has high latency"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    FunctionName = "${aws_lambda_function.lambda.function_name}"
  }
}

resource "aws_cloudwatch_metric_alarm" "lambda_throttles" {
  count                     = "${var.env == "prod" ? 1 : 0}"
  alarm_name                = "${var.env}-${var.lambda_name}-throttles"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "Throttles"
  namespace                 = "AWS/Lambda"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "${var.lambda_name} lambda is getting throttled"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    FunctionName = "${aws_lambda_function.lambda.function_name}"
  }
}
