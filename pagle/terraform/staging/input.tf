variable "aws_profile" {}

variable "vpc_id" {}

variable "region" {}

variable "security_group" {}

variable "env" {
  description = "The AWS environment. Must begin with one of the following:  dev (development),  stage (staging), or  prod (production)."
}

variable "environment" {}

variable "subnets" {}

variable "healthcheck" {
  default = "/ping"
}

variable "cluster_name" {}

variable "image" {}

variable "min_instances" {}

variable "max_instances" {}

provider "aws" {
  region  = "${var.region}"
  profile = "${var.aws_profile}"
}

variable "private_cidr_blocks" {
  type    = "list"
  default = []
}

variable "elasticache_replicas_per_node_group" {}

variable "elasticache_num_node_groups" {}

variable "cpu" {}

variable "memory" {}

variable "whitelisted_arns_for_privatelink" {
  type    = "list"
  default = []
}

variable "ripley_vpc_endpoint_service_name" {
  type        = "string"
  description = "The name of the Ripley VPC endpoint service"
}

variable "ripley_dns" {
  type        = "string"
  description = "DNS for Payday"
}

variable "ripley_private_zone" {
  type        = "string"
  description = "Zone for Payday"
}

variable "whitelisted_arns_for_sns" {
  type    = "list"
  default = []
}
