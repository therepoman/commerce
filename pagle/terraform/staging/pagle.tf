module "pagle" {
  source = "../modules/pagle"

  name                                = "pagle"
  image                               = "${var.image}"
  cluster                             = "${var.cluster_name}"
  security_group                      = "${var.security_group}"
  region                              = "${var.region}"
  vpc_id                              = "${var.vpc_id}"
  environment                         = "${var.environment}"
  env                                 = "${var.env}"
  subnets                             = "${var.subnets}"
  min_instances                       = "${var.min_instances}"
  max_instances                       = "${var.max_instances}"
  private_cidr_blocks                 = "${var.private_cidr_blocks}"
  elasticache_num_node_groups         = "${var.elasticache_num_node_groups}"
  elasticache_replicas_per_node_group = "${var.elasticache_replicas_per_node_group}"
  cpu                                 = "${var.cpu}"
  memory                              = "${var.memory}"
  whitelisted_arns_for_privatelink    = "${var.whitelisted_arns_for_privatelink}"
  whitelisted_arns_for_sns            = "${var.whitelisted_arns_for_sns}"
  ripley_dns                          = "${var.ripley_dns}"
  ripley_private_zone                 = "${var.ripley_private_zone}"
  ripley_vpc_endpoint_service_name    = "${var.ripley_vpc_endpoint_service_name}"

  env_vars = [
    {
      name  = "ENVIRONMENT"
      value = "${var.env}"
    },
  ]
}

terraform {
  backend "s3" {
    bucket  = "pagle-terraform-staging"
    key     = "tfstate/commerce/pagle/terraform/staging"
    region  = "us-west-2"
    profile = "pagle-devo"
  }
}
