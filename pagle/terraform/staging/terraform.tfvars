region = "us-west-2"

environment = "staging"

env = "staging"

aws_profile = "pagle-devo"

vpc_id = "vpc-087dcec76b4745a26"

security_group = "sg-0a6b552a50dce5007"

subnets = "subnet-056a776cb06e26bb6,subnet-07c30f99edb1abe9f,subnet-0be4f18eb9a6a33e6"

cluster_name = "nat-pagle-cluster"

image = "574977834358.dkr.ecr.us-west-2.amazonaws.com/commerce/pagle"

min_instances = 3

max_instances = 10

private_cidr_blocks = ["10.205.68.0/24", "10.205.69.0/24", "10.205.70.0/24"]

elasticache_replicas_per_node_group = "1"

elasticache_num_node_groups = "2"

cpu = 1024

// This ends up being 2048. Terracode doubles this value, for some reason.
memory = 1024

whitelisted_arns_for_privatelink = [
  "arn:aws:iam::021561903526:root", // payday
  "arn:aws:iam::645130450452:root", // graphql-dev
  "arn:aws:iam::327140220177:root", // helix
]

whitelisted_arns_for_sns = [
  "arn:aws:iam::021561903526:root", // payday
]

ripley_dns =  "beta.ripley.twitch.a2z.com"
ripley_private_zone = "ripley.twitch.a2z.com"
ripley_vpc_endpoint_service_name = "com.amazonaws.vpce.us-west-2.vpce-svc-0302ab0bc9133e1a4"
