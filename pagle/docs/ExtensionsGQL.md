# Extensions-related GraphQL Queries

There are currently a few GraphQL queries that interact with Pagle via the Extensions query, specifically the following:

```
query {
  extension(id: "extension-id", version: "extension-version") {
    challengeConditions { ... }
    challengeConditionParticipants { ... }
  }
}
```

Here are some things to note if you're doing testing related to these queries:

* Due to limitations on the Extensions side, we are unable to test these queries in staging. You have to do testing in production. Use a GraphQL sandbox environment or point your local GraphQL environment to Pagle production.
* You will need to use a real test Extension ID to test things out. In addition, your Twitch User ID will need to be whitelisted on the Extensions side in order to be allowed to use that Extension ID.
    * Example: `d1dp7kdgsq3yg4ddyyel9mz5jebvoo` is a test Extension ID that is owned by David Peppel. If you want to use this ID for testing, ask him to whitelist your TUID.
* You will need to specify an Extension version as well. Contact Extensions if you have no idea what this should be.
    * Example: For the `d1dp7kdgsq3yg4ddyyel9mz5jebvoo` Extension, use version `0.0.1`.