# Twirp Call Pattern

## Create a Condition

Creates a condition, bound to a single owner.

```go
conditionResp, err := pagleClient.CreateCondition(ctx, &pagle.CreateConditionReq{
    OwnerId: "123456",
    Name: "123456 wins a game of Fortnite",
    SupportedEffects: []pagle.EffectType{pagle.EffectType_GIVE_BITS}, // only effects of this type can be created against this condition
    TimeoutAt: "1970-01-01T00:00:00Z", // optional future timestamp - when this date is hit, the condition is terminated, and all pending effects are released
    DisableWhenConditionSatisfied: false, // true if a condition should become inactive after first SatisfyCondition call
    DefineEffectSettingsWhenSatisfied: true // true if EffectSettings are to be set when condition is satisfied, false if set when condition is created
})
```

`EffectSettings` can be passed at time of condition creation if `DefineEffectSettingsWhenSatisfied` is set to `false`.

```go
conditionResp, err := pagleClient.CreateCondition(ctx, &pagle.CreateConditionReq{
    OwnerId: "123456",
    Name: "123456 wins a game of Fortnite",
    SupportedEffects: []pagle.EffectType{pagle.EffectType_GIVE_BITS},
    EffectSettings: []*pagle.EffectSettings{
        {
            EffectType: pagle.EffectType_GIVE_BITS,
            Settings: &pagle.EffectSettings_GiveBitsSettings{ // this structure is specific to the EffectType
                GiveBitsSettings: &pagle.GiveBitsSettings{
                    PoolRecipients: []*pagle.PoolRecipientWeightedShare{ // How the pool of Bits against the condition should be split
                        {
                            ToUserId: "123456",
                            ShareWeight: 3, // 75% (3/4) of Bits go to "123456"
                        },
                        {
                            ToUserId: "789012",
                            ShareWeight: 1, // 25% (1/4) of Bits go to "789012"
                        },
                    },
                },
            },
        },
    },
})
``` 

## Create a ConditionParticipant

Creates a condition participant with a single effect, against an active condition

```go
conditionParticipantResp, err := pagleClient.CreateConditionParticipant(ctx, &pagle.CreateConditionParticipantReq{
    ConditionId: conditionResp.ConditionId,
    ConditionOwnerId: "123456", // will error if ConditionOwnerId doesn't match with owner passed when condition was created
    ConditionParticipantOwnerId: "67890",
    EffectType: pagle.EffectType_GIVE_BITS,
    Effect: &pagle.CreateConditionParticipantReq_GiveBitsParams{ // this structure is specific to the EffectType
        GiveBitsParams: &pagle.GiveBitsParams{
            Amount: 100, // if associated condition is satisfied, give 100 Bits from balance (held in escrow at time of condition participant creation) 
            FromUserId: "67890",
        },
    },
    TtlSeconds: 60,
})
```

## Satisfy a Condition

Satisfies an active condition. All pending condition participants are processed, and their effects finalized

```go
_, err = pagleClient.SatisfyCondition(ctx, &pagle.SatisfyConditionReq{
    ConditionId: conditionResp.ConditionId,
    OwnerId: "123456", // will error if OwnerId doesn't match with owner passed when condition was created
    EffectSettings: []*pagle.EffectSettings{ // Same structure as seen in CreateCondition call. Define this structure if `Condition.DefineEffectSettingsWhenSatisfied` is `true`
        {
            EffectType: pagle.EffectType_GIVE_BITS,
            Settings: &pagle.EffectSettings_GiveBitsSettings{
                GiveBitsSettings: &pagle.GiveBitsSettings{
                    PoolRecipients: []*pagle.PoolRecipientWeightedShare{
                        {
                            ToUserId: "123456",
                            ShareWeight: 3,
                        },
                        {
                            ToUserId: "789012",
                            ShareWeight: 1,
                        },
                    },
                },
            },
        },
    },
})
```

## Cancel a Condition

Cancels an active condition. All pending condition participants are processed, and their effects released

```go

_, err = pagleClient.CancelCondition(ctx, &pagle.CancelConditionReq{
    ConditionId: conditionResp.ConditionId,
    OwnerId: "123456", // will error if OwnerId doesn't match with owner passed when condition was created
})
```