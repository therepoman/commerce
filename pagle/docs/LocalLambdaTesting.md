# Testing Lambdas Locally

## Prerequisites

Docker for OSX

```bash
brew cask install docker
```

## Start localstack

```bash
make start_localstack
```

## Build Lambda Changes

```bash
make build_lambdas
```

## Call Lambda
```bash
# name: Name of lambda function, corresponds to folder name within terraform/modules/pagle/lambda
# payload: Data to pass to lambda when invoked
#
# Note: this will return an error, just for illustrative purposes
make lambda_local name=condition-processing payload='{\"Records\":[{\"Body\":null}\,{\"Body\":null}]}'
```