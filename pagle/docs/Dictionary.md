# Dictionary

Pagle has a lot of abstract concepts, some with more clear names than others.

## Condition

The x in "if x happens I will give y". `Conditions` either happen or they don't - if anything more than this binary state is required, a `Condition` will need to be created for each possible state. 

### EffectSettings

Defined either when a `Condition` is created, or each time it is `Satisfied`. Contains information about how contributions of a particular `EffectType` should be handled.

As an example, for the Bits type, this is where we define where the pooled Bits should be distributed (e.g. 50% to channelA and 50% to channelB).

### ConditionAggregates

Each tenant has the ability to define atomic counting logic for their particular effect. This hook is invoked each time a `ConditionParticipant` goes through a state change. This allows us to keep tally of how many of each effect are `PENDING`, `EXPIRED`, `SATISFIED`, etc. 

This data is then exposed via the `GetConditionSummary` API

## ConditionRun

When `disable_when_satisfied` is set to `true` on a `Condition`, it is closed to further contribution after the first call to `SatisfyCondition`. When `false`, we allow users to continue contributing after the `SatisfyCondition` call. These effects will be in a pending state until the next `SatisfyCondition` call.

The abstraction that allows us to partition `ConditionParticipants` in this way is called a `ConditionRun`. In the `disable_when_satisfied: true` case, `ConditionRun` maps 1:1 with `Condition`. In the inverse case, we create a new `ConditionRun` every time `SatisfyCondition` is called.

This construct is opaque to twirp callers, but is useful to know about for tenant maintainers. 

### PreProcessedConditionParticipant

When a `ConditionRun` is completed, we pass all of the pending `ConditionParticipants` to their respective `Tenant#PreProcessConditionParticipants` hooks. One output from this is the `PreProcessedConditionParticipant` structure, which contains the original `ConditionParticipant`, as well as the derived `EffectOutput` (read more on this below).

### EffectRollup

The other generated artifact from `Tenant#PreProcessConditionParticipants` is the `EffectRollup`. This allows tenants to bubble up facts/totals about the entire pool of effects associated with the `ConditionRun` for later processing. As an example, for the Bits type, we set the total number of Bits so that we know how much to payout the creator(s).

This may sound similar to `ConditionAggregates`, but is different in the following ways:

* `EffectRollup` is an internal structure, used only during processing when a `Condition` is satisfied
* `ConditionAggregates` represents effect totals across all `ConditionRuns`

## ConditionParticipant

This would have been better named `ParticipationEvent` or `ContributionEvent`. Represents a single contribution to a condition (i.e. if a user contributes twice to a condition, two `ConditionParticipants` will be created).

Each `ConditionParticipant` has a single `Effect` (i.e. if a user wants to contribute Bits and Subs to the same condition, a `ConditionParticipant` would need to be created for each).

## Effect

The atom being contributed to a `Condition`. Each effect has a corresponding tenant that handles its specific processing. Creating an `Effect` is a promise, thus we must be able to escrow (hold) each `EffectType` that we introduce.

### EffectType

The type of effect being contributed (e.g. Bits, Sub, etc). This controls tenant routing.

### EffectParams

User defined input, specific to the `EffectType`. As an example, for a Bits effect, we define how many Bits the user is contributing.

### EffectOutput

Defined on each effect in the `Tenant#PreProcessConditionParticipants` hook. Optionally allows us to store data about how we should process an individual `ConditionParticipant's` effect. As an example, we might store that userA's contribution should be routed to channelA. 

### EffectStaticConfig

Pagle has the ability to store static config about a particular effect type in s3, making the values available to various tenant hooks. As an example, during development we configured an `allowlist` to limit access to creation of Bits effects.