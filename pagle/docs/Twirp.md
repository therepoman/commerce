# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [rpc/pagle-service.proto](#rpc/pagle-service.proto)
    - [BatchGetConditionReq](#code.justin.tv.commerce.pagle.BatchGetConditionReq)
    - [BatchGetConditionResp](#code.justin.tv.commerce.pagle.BatchGetConditionResp)
    - [BatchGetConditionSummaryReq](#code.justin.tv.commerce.pagle.BatchGetConditionSummaryReq)
    - [BatchGetConditionSummaryResp](#code.justin.tv.commerce.pagle.BatchGetConditionSummaryResp)
    - [CancelConditionReq](#code.justin.tv.commerce.pagle.CancelConditionReq)
    - [CancelConditionResp](#code.justin.tv.commerce.pagle.CancelConditionResp)
    - [Condition](#code.justin.tv.commerce.pagle.Condition)
    - [ConditionParticipant](#code.justin.tv.commerce.pagle.ConditionParticipant)
    - [ConditionParticipantEffectSummaryForState](#code.justin.tv.commerce.pagle.ConditionParticipantEffectSummaryForState)
    - [ConditionParticipantSummary](#code.justin.tv.commerce.pagle.ConditionParticipantSummary)
    - [ConditionParticipantSummaryForState](#code.justin.tv.commerce.pagle.ConditionParticipantSummaryForState)
    - [ConditionSummary](#code.justin.tv.commerce.pagle.ConditionSummary)
    - [CreateConditionParticipantReq](#code.justin.tv.commerce.pagle.CreateConditionParticipantReq)
    - [CreateConditionParticipantResp](#code.justin.tv.commerce.pagle.CreateConditionParticipantResp)
    - [CreateConditionReq](#code.justin.tv.commerce.pagle.CreateConditionReq)
    - [CreateConditionResp](#code.justin.tv.commerce.pagle.CreateConditionResp)
    - [Effect](#code.justin.tv.commerce.pagle.Effect)
    - [EffectSettings](#code.justin.tv.commerce.pagle.EffectSettings)
    - [Extension](#code.justin.tv.commerce.pagle.Extension)
    - [GetConditionParticipantReq](#code.justin.tv.commerce.pagle.GetConditionParticipantReq)
    - [GetConditionParticipantResp](#code.justin.tv.commerce.pagle.GetConditionParticipantResp)
    - [GetConditionParticipantsByOwnerIDReq](#code.justin.tv.commerce.pagle.GetConditionParticipantsByOwnerIDReq)
    - [GetConditionParticipantsByOwnerIDResp](#code.justin.tv.commerce.pagle.GetConditionParticipantsByOwnerIDResp)
    - [GetConditionParticipantsReq](#code.justin.tv.commerce.pagle.GetConditionParticipantsReq)
    - [GetConditionParticipantsResp](#code.justin.tv.commerce.pagle.GetConditionParticipantsResp)
    - [GetConditionReq](#code.justin.tv.commerce.pagle.GetConditionReq)
    - [GetConditionResp](#code.justin.tv.commerce.pagle.GetConditionResp)
    - [GetConditionSummaryReq](#code.justin.tv.commerce.pagle.GetConditionSummaryReq)
    - [GetConditionSummaryResp](#code.justin.tv.commerce.pagle.GetConditionSummaryResp)
    - [GetConditionsReq](#code.justin.tv.commerce.pagle.GetConditionsReq)
    - [GetConditionsResp](#code.justin.tv.commerce.pagle.GetConditionsResp)
    - [GiveBitsEffectSummaryForState](#code.justin.tv.commerce.pagle.GiveBitsEffectSummaryForState)
    - [GiveBitsParams](#code.justin.tv.commerce.pagle.GiveBitsParams)
    - [GiveBitsSettings](#code.justin.tv.commerce.pagle.GiveBitsSettings)
    - [HealthCheckReq](#code.justin.tv.commerce.pagle.HealthCheckReq)
    - [HealthCheckResp](#code.justin.tv.commerce.pagle.HealthCheckResp)
    - [PoolRecipientWeightedShare](#code.justin.tv.commerce.pagle.PoolRecipientWeightedShare)
    - [SatisfyConditionReq](#code.justin.tv.commerce.pagle.SatisfyConditionReq)
    - [SatisfyConditionResp](#code.justin.tv.commerce.pagle.SatisfyConditionResp)
  
    - [ConditionErrorCode](#code.justin.tv.commerce.pagle.ConditionErrorCode)
    - [ConditionParticipantEndState](#code.justin.tv.commerce.pagle.ConditionParticipantEndState)
    - [ConditionParticipantErrorCode](#code.justin.tv.commerce.pagle.ConditionParticipantErrorCode)
    - [ConditionParticipantProcessingState](#code.justin.tv.commerce.pagle.ConditionParticipantProcessingState)
    - [ConditionState](#code.justin.tv.commerce.pagle.ConditionState)
    - [CreateConditionErrorCode](#code.justin.tv.commerce.pagle.CreateConditionErrorCode)
    - [CreateConditionParticipantErrorCode](#code.justin.tv.commerce.pagle.CreateConditionParticipantErrorCode)
    - [EffectType](#code.justin.tv.commerce.pagle.EffectType)
  
    - [Pagle](#code.justin.tv.commerce.pagle.Pagle)
  
- [Scalar Value Types](#scalar-value-types)



<a name="rpc/pagle-service.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## rpc/pagle-service.proto



<a name="code.justin.tv.commerce.pagle.BatchGetConditionReq"></a>

### BatchGetConditionReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| requests | [GetConditionReq](#code.justin.tv.commerce.pagle.GetConditionReq) | repeated |  |






<a name="code.justin.tv.commerce.pagle.BatchGetConditionResp"></a>

### BatchGetConditionResp



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| conditions | [Condition](#code.justin.tv.commerce.pagle.Condition) | repeated |  |






<a name="code.justin.tv.commerce.pagle.BatchGetConditionSummaryReq"></a>

### BatchGetConditionSummaryReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| requests | [GetConditionSummaryReq](#code.justin.tv.commerce.pagle.GetConditionSummaryReq) | repeated |  |






<a name="code.justin.tv.commerce.pagle.BatchGetConditionSummaryResp"></a>

### BatchGetConditionSummaryResp



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| summaries | [ConditionSummary](#code.justin.tv.commerce.pagle.ConditionSummary) | repeated |  |






<a name="code.justin.tv.commerce.pagle.CancelConditionReq"></a>

### CancelConditionReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| condition_id | [string](#string) |  |  |
| owner_id | [string](#string) |  |  |
| domain | [string](#string) |  |  |






<a name="code.justin.tv.commerce.pagle.CancelConditionResp"></a>

### CancelConditionResp







<a name="code.justin.tv.commerce.pagle.Condition"></a>

### Condition
Represents a condition.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| condition_id | [string](#string) |  |  |
| owner_id | [string](#string) |  |  |
| condition_run_id | [string](#string) |  |  |
| name | [string](#string) |  |  |
| timeout_at | [google.protobuf.Timestamp](#google.protobuf.Timestamp) |  |  |
| created_at | [google.protobuf.Timestamp](#google.protobuf.Timestamp) |  |  |
| updated_at | [google.protobuf.Timestamp](#google.protobuf.Timestamp) |  |  |
| condition_state | [ConditionState](#code.justin.tv.commerce.pagle.ConditionState) |  |  |
| supported_effects | [EffectType](#code.justin.tv.commerce.pagle.EffectType) | repeated |  |
| effect_settings | [EffectSettings](#code.justin.tv.commerce.pagle.EffectSettings) | repeated |  |
| domain | [string](#string) |  |  |
| disable_when_satisfied | [bool](#bool) |  |  |
| define_effect_settings_when_satisfied | [bool](#bool) |  |  |
| extension_id | [string](#string) |  | [Optional] If defined, implies that the condition was created in an extension. Example side effect: the owner of the extension will receive a set percentage of *all pooled Bits* each time the condition is satisfied. |
| extension | [Extension](#code.justin.tv.commerce.pagle.Extension) |  |  |






<a name="code.justin.tv.commerce.pagle.ConditionParticipant"></a>

### ConditionParticipant
Represents a condition participant.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| condition_participant_id | [string](#string) |  |  |
| owner_id | [string](#string) |  |  |
| condition_participant_processing_state | [ConditionParticipantProcessingState](#code.justin.tv.commerce.pagle.ConditionParticipantProcessingState) |  |  |
| condition_participant_end_state | [ConditionParticipantEndState](#code.justin.tv.commerce.pagle.ConditionParticipantEndState) |  |  |
| effect | [Effect](#code.justin.tv.commerce.pagle.Effect) |  |  |
| condition_id | [string](#string) |  |  |
| condition_owner_id | [string](#string) |  |  |
| domain | [string](#string) |  |  |






<a name="code.justin.tv.commerce.pagle.ConditionParticipantEffectSummaryForState"></a>

### ConditionParticipantEffectSummaryForState
Summary of condition participants effects that are in a particular state


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| give_bits | [GiveBitsEffectSummaryForState](#code.justin.tv.commerce.pagle.GiveBitsEffectSummaryForState) |  |  |






<a name="code.justin.tv.commerce.pagle.ConditionParticipantSummary"></a>

### ConditionParticipantSummary
Summary of condition participants attached to a particular condition.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total | [int64](#int64) |  |  |
| pending | [ConditionParticipantSummaryForState](#code.justin.tv.commerce.pagle.ConditionParticipantSummaryForState) |  |  |
| satisfied | [ConditionParticipantSummaryForState](#code.justin.tv.commerce.pagle.ConditionParticipantSummaryForState) |  |  |
| condition_canceled | [ConditionParticipantSummaryForState](#code.justin.tv.commerce.pagle.ConditionParticipantSummaryForState) |  |  |
| condition_timeout | [ConditionParticipantSummaryForState](#code.justin.tv.commerce.pagle.ConditionParticipantSummaryForState) |  |  |
| canceled | [ConditionParticipantSummaryForState](#code.justin.tv.commerce.pagle.ConditionParticipantSummaryForState) |  |  |
| timeout | [ConditionParticipantSummaryForState](#code.justin.tv.commerce.pagle.ConditionParticipantSummaryForState) |  |  |
| failed_validation | [ConditionParticipantSummaryForState](#code.justin.tv.commerce.pagle.ConditionParticipantSummaryForState) |  |  |






<a name="code.justin.tv.commerce.pagle.ConditionParticipantSummaryForState"></a>

### ConditionParticipantSummaryForState
Summary of condition participants that are in a particular state


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total | [int64](#int64) |  |  |
| effects | [ConditionParticipantEffectSummaryForState](#code.justin.tv.commerce.pagle.ConditionParticipantEffectSummaryForState) |  |  |






<a name="code.justin.tv.commerce.pagle.ConditionSummary"></a>

### ConditionSummary



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| condition_id | [string](#string) |  |  |
| owner_id | [string](#string) |  |  |
| domain | [string](#string) |  |  |
| participants | [ConditionParticipantSummary](#code.justin.tv.commerce.pagle.ConditionParticipantSummary) |  |  |






<a name="code.justin.tv.commerce.pagle.CreateConditionParticipantReq"></a>

### CreateConditionParticipantReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| condition_id | [string](#string) |  |  |
| condition_participant_owner_id | [string](#string) |  |  |
| condition_owner_id | [string](#string) |  |  |
| ttl_seconds | [int32](#int32) |  | Max 28 days |
| effect | [Effect](#code.justin.tv.commerce.pagle.Effect) |  |  |
| domain | [string](#string) |  |  |






<a name="code.justin.tv.commerce.pagle.CreateConditionParticipantResp"></a>

### CreateConditionParticipantResp



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| condition_participant | [ConditionParticipant](#code.justin.tv.commerce.pagle.ConditionParticipant) |  |  |






<a name="code.justin.tv.commerce.pagle.CreateConditionReq"></a>

### CreateConditionReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| owner_id | [string](#string) |  |  |
| name | [string](#string) |  |  |
| timeout_at | [google.protobuf.Timestamp](#google.protobuf.Timestamp) |  |  |
| supported_effects | [EffectType](#code.justin.tv.commerce.pagle.EffectType) | repeated |  |
| effect_settings | [EffectSettings](#code.justin.tv.commerce.pagle.EffectSettings) | repeated | If settings are defined for an EffectType here, they must not be defined when ConditionSatisfied is called. |
| description | [string](#string) |  |  |
| disable_when_condition_satisfied | [bool](#bool) |  |  |
| define_effect_settings_when_satisfied | [bool](#bool) |  |  |
| domain | [string](#string) |  |  |
| extension_id | [string](#string) |  | [Optional] If defined, implies that the condition was created in an extension. Example side effect: the owner of the extension will receive a set percentage of *all pooled Bits* each time the condition is satisfied. |
| extension | [Extension](#code.justin.tv.commerce.pagle.Extension) |  |  |






<a name="code.justin.tv.commerce.pagle.CreateConditionResp"></a>

### CreateConditionResp



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| condition | [Condition](#code.justin.tv.commerce.pagle.Condition) |  |  |






<a name="code.justin.tv.commerce.pagle.Effect"></a>

### Effect
Represents an effect with its type and parameters.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| effect_type | [EffectType](#code.justin.tv.commerce.pagle.EffectType) |  |  |
| give_bits_params | [GiveBitsParams](#code.justin.tv.commerce.pagle.GiveBitsParams) |  |  |






<a name="code.justin.tv.commerce.pagle.EffectSettings"></a>

### EffectSettings
Represents settings for an effect type.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| effect_type | [EffectType](#code.justin.tv.commerce.pagle.EffectType) |  |  |
| give_bits_settings | [GiveBitsSettings](#code.justin.tv.commerce.pagle.GiveBitsSettings) |  |  |






<a name="code.justin.tv.commerce.pagle.Extension"></a>

### Extension



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| installation_channel_id | [string](#string) |  |  |






<a name="code.justin.tv.commerce.pagle.GetConditionParticipantReq"></a>

### GetConditionParticipantReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| condition_participant_id | [string](#string) |  |  |
| condition_participant_owner_id | [string](#string) |  |  |
| domain | [string](#string) |  |  |






<a name="code.justin.tv.commerce.pagle.GetConditionParticipantResp"></a>

### GetConditionParticipantResp



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| condition_participant | [ConditionParticipant](#code.justin.tv.commerce.pagle.ConditionParticipant) |  |  |






<a name="code.justin.tv.commerce.pagle.GetConditionParticipantsByOwnerIDReq"></a>

### GetConditionParticipantsByOwnerIDReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| condition_participant_owner_id | [string](#string) |  |  |
| cursor | [string](#string) |  |  |
| domain | [string](#string) |  |  |






<a name="code.justin.tv.commerce.pagle.GetConditionParticipantsByOwnerIDResp"></a>

### GetConditionParticipantsByOwnerIDResp



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| condition_participants | [ConditionParticipant](#code.justin.tv.commerce.pagle.ConditionParticipant) | repeated |  |
| cursor | [string](#string) |  |  |






<a name="code.justin.tv.commerce.pagle.GetConditionParticipantsReq"></a>

### GetConditionParticipantsReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| condition_participant_owner_id | [string](#string) |  |  |
| condition_owner_id | [string](#string) |  |  |
| condition_participant_end_state | [ConditionParticipantEndState](#code.justin.tv.commerce.pagle.ConditionParticipantEndState) |  |  |
| cursor | [string](#string) |  |  |
| domain | [string](#string) |  |  |






<a name="code.justin.tv.commerce.pagle.GetConditionParticipantsResp"></a>

### GetConditionParticipantsResp



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| condition_participants | [ConditionParticipant](#code.justin.tv.commerce.pagle.ConditionParticipant) | repeated |  |
| cursor | [string](#string) |  |  |






<a name="code.justin.tv.commerce.pagle.GetConditionReq"></a>

### GetConditionReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| owner_id | [string](#string) |  |  |
| condition_id | [string](#string) |  |  |
| domain | [string](#string) |  |  |






<a name="code.justin.tv.commerce.pagle.GetConditionResp"></a>

### GetConditionResp



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| condition | [Condition](#code.justin.tv.commerce.pagle.Condition) |  |  |






<a name="code.justin.tv.commerce.pagle.GetConditionSummaryReq"></a>

### GetConditionSummaryReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| condition_id | [string](#string) |  |  |
| owner_id | [string](#string) |  |  |
| domain | [string](#string) |  |  |






<a name="code.justin.tv.commerce.pagle.GetConditionSummaryResp"></a>

### GetConditionSummaryResp



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| summary | [ConditionSummary](#code.justin.tv.commerce.pagle.ConditionSummary) |  |  |






<a name="code.justin.tv.commerce.pagle.GetConditionsReq"></a>

### GetConditionsReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| owner_id | [string](#string) |  |  |
| condition_state | [ConditionState](#code.justin.tv.commerce.pagle.ConditionState) |  |  |
| cursor | [string](#string) |  |  |
| domain | [string](#string) |  |  |






<a name="code.justin.tv.commerce.pagle.GetConditionsResp"></a>

### GetConditionsResp



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| conditions | [Condition](#code.justin.tv.commerce.pagle.Condition) | repeated |  |
| cursor | [string](#string) |  |  |






<a name="code.justin.tv.commerce.pagle.GiveBitsEffectSummaryForState"></a>

### GiveBitsEffectSummaryForState
Summary of give bits effects that are in a particular state


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_effects | [int64](#int64) |  |  |
| total_bits_amount | [int64](#int64) |  |  |






<a name="code.justin.tv.commerce.pagle.GiveBitsParams"></a>

### GiveBitsParams
Parameters describing a specific GiveBits effect.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| amount | [int32](#int32) |  |  |
| from_user_id | [string](#string) |  |  |






<a name="code.justin.tv.commerce.pagle.GiveBitsSettings"></a>

### GiveBitsSettings
Represents condition level settings specific to the GiveBits effect type.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| pool_recipients | [PoolRecipientWeightedShare](#code.justin.tv.commerce.pagle.PoolRecipientWeightedShare) | repeated |  |






<a name="code.justin.tv.commerce.pagle.HealthCheckReq"></a>

### HealthCheckReq







<a name="code.justin.tv.commerce.pagle.HealthCheckResp"></a>

### HealthCheckResp



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| response | [string](#string) |  |  |






<a name="code.justin.tv.commerce.pagle.PoolRecipientWeightedShare"></a>

### PoolRecipientWeightedShare
Represents the weighted share a particular user will receive out of a pool of effects. Not all
effects can be pooled, therefore not all EffectSettings will implement this structure.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| to_user_id | [string](#string) |  |  |
| share_weight | [int64](#int64) |  | The weight of this user&#39;s share of the bits pool. Examples: - If there is one user in the pool and they are getting 100% of the bits pool, then their share_weight can be any non-zero positive integer - If there are 2 users in the pool, and this user is supposed to receive 25% of the bits pool, then some possible share_weight values: 1 for this user, 3 for the other (1/4: 25%, 3/4: 75%) / 2 for this user, 6 for the other (2/8: 25%, 6/8: 75%)/ etc. - If there are 3 users in the pool, and this user is supposed to receive 33% of the bits pool, then some possible share_weight values: 1 for each user (1/3: 33%), 2 for each user (2/6: 33%), ..., n for each user (n/3n: 33%) |






<a name="code.justin.tv.commerce.pagle.SatisfyConditionReq"></a>

### SatisfyConditionReq



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| condition_id | [string](#string) |  |  |
| owner_id | [string](#string) |  |  |
| effect_settings | [EffectSettings](#code.justin.tv.commerce.pagle.EffectSettings) | repeated | If settings for an EffectType were not defined when the condition was created, they must be defined here. If settings for an EffectType were defined when the condition was created, they must not be defined here. |
| domain | [string](#string) |  |  |






<a name="code.justin.tv.commerce.pagle.SatisfyConditionResp"></a>

### SatisfyConditionResp






 


<a name="code.justin.tv.commerce.pagle.ConditionErrorCode"></a>

### ConditionErrorCode


| Name | Number | Description |
| ---- | ------ | ----------- |
| CONDITION_NOT_FOUND | 0 | Condition does not exist. |
| CONDITION_NOT_ACTIVE | 1 | Condition exists, but is not active. |



<a name="code.justin.tv.commerce.pagle.ConditionParticipantEndState"></a>

### ConditionParticipantEndState
Represents the final state of the condition participant.

| Name | Number | Description |
| ---- | ------ | ----------- |
| CONDITION_CANCELED | 0 | Associated condition canceled |
| CONDITION_EXPIRED | 1 | Associated condition timed out |
| SATISFIED | 2 | Associated condition satisfied |
| PENDING_COMPLETION | 3 | Associated with a pending condition run |
| CONDITION_PARTICIPANT_EXPIRED | 4 | Condition Participant expired via TTL |
| CONDITION_PARTICIPANT_CANCELED | 5 | Condition Participant was canceled |
| CONDITION_PARTICIPANT_FAILED_VALIDATION | 6 | Condition Participant failed validation prior to processing |



<a name="code.justin.tv.commerce.pagle.ConditionParticipantErrorCode"></a>

### ConditionParticipantErrorCode


| Name | Number | Description |
| ---- | ------ | ----------- |
| CONDITION_PARTICIPANT_NOT_FOUND | 0 | Condition participant does not exist. |
| CONDITION_PARTICIPANT_NOT_PENDING | 1 | Condition participant exists, but is not pending. |



<a name="code.justin.tv.commerce.pagle.ConditionParticipantProcessingState"></a>

### ConditionParticipantProcessingState
Represents the workflow state of a condition participant.

| Name | Number | Description |
| ---- | ------ | ----------- |
| PENDING | 0 | Associated with a pending condition run |
| SUCCESS | 1 | Completed without errors, either satisfied or terminated |
| ERROR | 2 | Errored at some point in workflow |
| CREATING | 3 | Condition Participant is being created |
| FAILED_TO_CREATE | 4 | Condition Participant failed to be created |



<a name="code.justin.tv.commerce.pagle.ConditionState"></a>

### ConditionState
Represents the state of a condition.

| Name | Number | Description |
| ---- | ------ | ----------- |
| ACTIVE | 0 | New condition participants can be associated |
| CANCELED | 1 | Manually terminated, new condition participants cannot be associated |
| INACTIVE | 2 | New condition participants cannot be associated |
| EXPIRED | 3 | Automatically terminated, new condition participants cannot be associated |



<a name="code.justin.tv.commerce.pagle.CreateConditionErrorCode"></a>

### CreateConditionErrorCode
Errors that can occur when creating a condition via CreateCondition.

| Name | Number | Description |
| ---- | ------ | ----------- |
| BITS_RECIPIENT_INELIGIBLE | 0 | User tried to create a condition with a bits recipient that is ineligible to receive bits. |



<a name="code.justin.tv.commerce.pagle.CreateConditionParticipantErrorCode"></a>

### CreateConditionParticipantErrorCode
Errors that can occur when creating a condition participant via CreateConditionParticipant.

| Name | Number | Description |
| ---- | ------ | ----------- |
| BITS_BENEFACTOR_INELIGIBLE | 0 | User tried to create a condition participant with a GIVE_BITS effect type, but they are ineligible to give bits. |
| EFFECT_TYPE_UNSUPPORTED_BY_CONDITION | 1 | User tried to create a condition participant with an effect type that is not supported by the associated condition. |
| INSUFFICIENT_BITS_BALANCE | 3 | User tried to create a condition participant with a GIVE_BITS effect type, but they have insufficient balance. |



<a name="code.justin.tv.commerce.pagle.EffectType"></a>

### EffectType
Represents the type of effect that is associate with a condition participant.

| Name | Number | Description |
| ---- | ------ | ----------- |
| GIVE_BITS | 0 | Give bits |


 

 


<a name="code.justin.tv.commerce.pagle.Pagle"></a>

### Pagle


| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| HealthCheck | [HealthCheckReq](#code.justin.tv.commerce.pagle.HealthCheckReq) | [HealthCheckResp](#code.justin.tv.commerce.pagle.HealthCheckResp) | Ping |
| CreateCondition | [CreateConditionReq](#code.justin.tv.commerce.pagle.CreateConditionReq) | [CreateConditionResp](#code.justin.tv.commerce.pagle.CreateConditionResp) | Creates a condition |
| CancelCondition | [CancelConditionReq](#code.justin.tv.commerce.pagle.CancelConditionReq) | [CancelConditionResp](#code.justin.tv.commerce.pagle.CancelConditionResp) | Cancels a condition, releases all pending effects |
| SatisfyCondition | [SatisfyConditionReq](#code.justin.tv.commerce.pagle.SatisfyConditionReq) | [SatisfyConditionResp](#code.justin.tv.commerce.pagle.SatisfyConditionResp) | Satisfy a condition, finalizes all pending effects |
| GetConditionSummary | [GetConditionSummaryReq](#code.justin.tv.commerce.pagle.GetConditionSummaryReq) | [GetConditionSummaryResp](#code.justin.tv.commerce.pagle.GetConditionSummaryResp) | Summarizes the state of a condition, including how many condition participants are in the various states of completion |
| BatchGetConditionSummary | [BatchGetConditionSummaryReq](#code.justin.tv.commerce.pagle.BatchGetConditionSummaryReq) | [BatchGetConditionSummaryResp](#code.justin.tv.commerce.pagle.BatchGetConditionSummaryResp) | Batched GetConditionSummary |
| GetConditions | [GetConditionsReq](#code.justin.tv.commerce.pagle.GetConditionsReq) | [GetConditionsResp](#code.justin.tv.commerce.pagle.GetConditionsResp) | Gets conditions for an owner |
| GetCondition | [GetConditionReq](#code.justin.tv.commerce.pagle.GetConditionReq) | [GetConditionResp](#code.justin.tv.commerce.pagle.GetConditionResp) | Gets a condition |
| BatchGetCondition | [BatchGetConditionReq](#code.justin.tv.commerce.pagle.BatchGetConditionReq) | [BatchGetConditionResp](#code.justin.tv.commerce.pagle.BatchGetConditionResp) | Batched GetCondition |
| CreateConditionParticipant | [CreateConditionParticipantReq](#code.justin.tv.commerce.pagle.CreateConditionParticipantReq) | [CreateConditionParticipantResp](#code.justin.tv.commerce.pagle.CreateConditionParticipantResp) | Creates a condition participant for a condition |
| GetConditionParticipant | [GetConditionParticipantReq](#code.justin.tv.commerce.pagle.GetConditionParticipantReq) | [GetConditionParticipantResp](#code.justin.tv.commerce.pagle.GetConditionParticipantResp) | Gets a condition participant |
| GetConditionParticipants | [GetConditionParticipantsReq](#code.justin.tv.commerce.pagle.GetConditionParticipantsReq) | [GetConditionParticipantsResp](#code.justin.tv.commerce.pagle.GetConditionParticipantsResp) | Gets condition participants for an owner |
| GetConditionParticipantsByOwnerID | [GetConditionParticipantsByOwnerIDReq](#code.justin.tv.commerce.pagle.GetConditionParticipantsByOwnerIDReq) | [GetConditionParticipantsByOwnerIDResp](#code.justin.tv.commerce.pagle.GetConditionParticipantsByOwnerIDResp) | Gets condition participants for a single participant |

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

