# Creating a New Effect Type

## Overview

A condition participant must specify an output effect, which defines what happens when the condition is satisfied. A corresponding effect tenant must exist that implements methods that:

* [CreateTransaction] places a hold (escrow)
* [ReleaseTransaction] releases a hold
* [FinalizeTransaction] finalizes a hold
* [PreProcessConditionParticipants] does some optional pre-processing on a set of condition participants, given effect settings defined by the condition
* [ValidateAndFilterConditionParticipants] validates that a set of condition participants can be satisfied, given effect settings defined by the condition
* [ValidateEffectSettings] validates that the effect settings can be fulfilled by the tenant, given effect settings defined by the condition
* [ValidateEffect] validates that the effect can be fulfilled by the tenant, given the effect defined by the condition participant
* [MakeEffectAggregateGetter] provides details about how to increment/decrement condition aggregates for the effect type (exposed in GetConditionSummary)

## Steps required

Assume you wanted to enable challenges for a new atom called `widget`.

### 1. Update proto file

[pagle-service.proto](https://git-aws.internal.justin.tv/commerce/pagle/blob/master/rpc/pagle-service.proto)

Example:

```proto
// Add a new effect param set - these parameters describe the input for a specific EffectType
message GiveWidgetsParams {
    int32 amount = 1;
    string from_user_id = 2;
}

// Add new param set to oneof rule
message Effect {
    oneof params {
        GiveBitsParams give_bits_params = 2;
        GiveWidgetsParams give_widgets_params = 3;
    }
}

// Add new effect type
enum EffectType {
    GIVE_BITS = 0;
    GIVE_WIDGETS = 1;
}

// Add a structure that defines the effect settings. Defined when a condition is created,
// or each time a condition is satisfied. This data will be passed to your tenant's PreProcessConditionParticipants method.
// As an example, this structure can be used to define how effects attached to a condition will be distributed
// (e.g. 50% of Bits to channelA and 50% to channelB) 
message GiveWidgetSettings {
    bool some_truth_about_all_widget_effects_for_this_condition = 1;
}

// Add new settings to oneof rule
message EffectSettings {
    EffectType effect_type = 1;

    oneof settings {
        GiveBitsSettings give_bits_settings = 2;
        GiveWidgetSettings give_widget_settings = 3;
    }
}

// Add message that represents the effect-specific aggregates that will be exposed in GetConditionSummary
message GiveWidgetEffectSummaryForState {
    int64 total_effects = 1;
    int64 total_widget_value = 2;
}

// Add new message to effect summary structure
message ConditionParticipantEffectSummaryForState {
    int64 total = 1;
    GiveBitsEffectSummaryForState give_bits = 2;
    GiveWidgetEffectSummaryForState give_widgets = 3;
}

```

### 2. Update internal models

In Pagle we terminate the protobuf representation at the API layer, and transform into an internal representation. 

[model.go](https://git-aws.internal.justin.tv/commerce/pagle/blob/master/backend/models/model.go)

Example:

```go
// Add effect internal representation
type EffectParamsGiveWidgets struct {
	Amount     int32  `dynamodbav:"amount"`
	FromUserID string `dynamodbav:"from_user_id"`
}

// Add new effect struct to parent EffectParams
type EffectParams struct {
	GiveBits    *EffectParamsGiveBits    `dynamodbav:"give_bits"`
	GiveWidgets *EffectParamsGiveWidgets `dynamodbav:"give_widgets"`
}

// Add a new EffectType
const (
	GiveBits    = EffectType("GIVE_BITS")
	GiveWidgets = EffectType("GIVE_WIDGETS")
)

// Add effect settings structure
type GiveWidgetSettings struct {
	SomeTruthAboutAllWidgetEffectsForThisCondition bool 
}

// Add new effect settings struct to parent EffectTypeSettings
type EffectTypeSettings struct {
	GiveBits    *GiveBitsSettings   `dynamodbav:"give_bits"`
	GiveWidgets *GiveWidgetSettings `dynamodbav:"give_widgets"`
}

// Add structure that represents the effect-specific aggregates
type GiveWidgetsEffectSummary struct {
	TotalEffects    int64 `dynamodbav:"total_effects"`
	TotalBitsAmount int64 `dynamodbav:"total_widget_value"`
}

// Add new structure to effect summary
type EffectSummary struct {
	GiveBits GiveBitsEffectSummary `dynamodbav:"give_bits"`
	GiveWidgets GiveWidgetsEffectSummary `dynamodbav:"give_widgets"`
}
```

### 3. Add a new tenant that implements Tenant

[interface](https://git-aws.internal.justin.tv/commerce/pagle/blob/master/backend/effect/tenant/tenant.go)

[Example tenant](https://git-aws.internal.justin.tv/commerce/pagle/blob/master/backend/effect/tenant/givebits/givebits.go)

### 4. Add tenant to dependency tree 

[tree](https://git-aws.internal.justin.tv/commerce/pagle/blob/master/backend/backend.go)

Example:

```go
&inject.Object{Value: givebits.NewGiveBitsTenant(), Name: "givebitstenant"},
&inject.Object{Value: givewidgets.NewGiveWidgetsTenant(), Name: "givewidgetstenant"},
```

### 5. Inject new tenant

[injection](https://git-aws.internal.justin.tv/commerce/pagle/blob/master/backend/effect/effect.go)

Example:

```go
type controllerImpl struct {
	GiveBitsTenant    givebits.GiveBitsTenant       `inject:"givebitstenant"`
	GiveWidgetsTenant givewidgets.GiveWidgetsTenant `inject:"givewidgetstenant"`
}
```

### 6. Add case for routing to new tenant

[CreateTenant](https://git-aws.internal.justin.tv/commerce/pagle/blob/master/backend/effect/effect.go)

Example:

```go
    switch effectType {
	case models.GiveBits:
		return c.GiveBitsTenant, n
    }
	case models.GiveWidgets:
		return c.GiveWidgetsTenant, n
    }
    }
```

### 7. Add mappings between Twirp structures and internal representation

[proto.go](https://git-aws.internal.justin.tv/commerce/pagle/blob/master/backend/utils/proto/proto.go)

* GetEffectFromCreateConditionParticipantRequest - Add case for extracting effect from CreateConditionParticipant Twirp request, and converting to internal representation
* ProtoEffectSettingToEffectSetting - Add case for converting Twirp effect settings to internal representation
* ProtoEffectTypeToEffectType - Add case for converting Twirp effect type to internal representation
* EffectSettingToProtoEffectSetting - Add case for converting internal effect settings to Twirp representation
* EffectTypeToProtoEffectType - Add case for converting internal effect type to Twirp representation
* EffectToProtoEffect - Add case for converting internal effect to Twirp representation
* ConditionParticipantAggregatesForStateToProtoConditionParticipantSummaryForState - Add mapping from internal aggregate representation to Twirp representation