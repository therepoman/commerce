# pagle
🎣 Pagle - the Twitch IFTTT service for consumables.

![Nat](https://s3-us-west-2.amazonaws.com/bits-assets/test/pagle/nat.jpeg)

## Endpoints

| Environment | Endpoint | AWS Account ID | VPCE |
| --- | --- | --- | --- |
| Staging | https://main.us-west-2.beta.pagle.twitch.a2z.com | 574977834358  | com.amazonaws.vpce.us-west-2.vpce-svc-02e9ac77309ea32ce | 
| Prod | https://main.us-west-2.prod.pagle.twitch.a2z.com | 196902209557 | com.amazonaws.vpce.us-west-2.vpce-svc-063dc08efb792ed18 |

## Docs

[Pagle Vocabulary](docs/Dictionary.md)

[Twirp](docs/Twirp.md)

[Call pattern examples](docs/TwirpCallPattern.md)