package main

import (
	"context"
	"math/rand"
	"time"

	"code.justin.tv/commerce/pagle/backend/auth"

	"code.justin.tv/chat/golibs/graceful"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend"
	"code.justin.tv/commerce/pagle/config"
	pagle "code.justin.tv/commerce/pagle/rpc"
	"code.justin.tv/commerce/splatter"
	"github.com/twitchtv/twirp"
	goji_graceful "github.com/zenazn/goji/graceful"
	"goji.io"
	"goji.io/pat"
)

const (
	workerShutdownTimeout = 1 * time.Minute
)

func main() {
	env := config.GetEnv()

	rand.Seed(time.Now().UnixNano())

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		log.WithError(err).Panic("Error loading config")
	}

	log.Infof("Loaded config: %s", cfg.EnvironmentName)

	be, err := backend.NewBackend(cfg)
	if err != nil {
		log.WithError(err).Panic("Error initializing backend")
	}

	err = be.StartAsyncWork()

	if err != nil {
		log.WithError(err).Panic("Error starting async work")
	}

	twirpStatsHook := twirp.ChainHooks(splatter.NewStatsdServerHooks(be.GetStatsDStatter()))
	twirpHandler := pagle.NewPagleServer(be.TwirpServer(), twirpStatsHook)

	mux := goji.NewMux()
	mux.Use(auth.WithCartmanToken)

	mux.HandleFunc(pat.Get("/ping"), be.Ping)
	mux.Handle(pat.Post(pagle.PaglePathPrefix+"*"), twirpHandler)

	goji_graceful.HandleSignals()
	err = goji_graceful.ListenAndServe(":8000", mux)
	if err != nil {
		log.WithError(err).Error("Mux listen and serve error")
	}

	log.Info("Initiated shutdown process")

	shutdownStart := time.Now()
	shutdownContext, shutdownContextCancel := context.WithTimeout(context.Background(), workerShutdownTimeout)
	defer shutdownContextCancel()

	shutdownErrGroup := be.Shutdown(shutdownContext)

	// Wait on all graceful goroutines to finish shutting down
	if err = graceful.Wait(); err != nil {
		log.WithError(err).Error("Error attempting to shutdown while waiting for graceful goroutines")
	}

	// Wait on all async processors to finish shutting down
	if err = shutdownErrGroup.Wait(); err != nil {
		log.WithError(err).Error("Error attempting to shutdown while waiting for processors")
	}

	goji_graceful.Wait()
	log.Infof("Finished shutting down in: %v", time.Since(shutdownStart))
}
