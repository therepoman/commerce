# `TwitchTelemetryStatsdMetricsSender`

This is a package that can be used for sending metrics to the given statsd endpoint.

This package is meant to be used with samples sent from [TwitchTelemetry](https://code.amazon.com/packages/TwitchTelemetry/trees/mainline).

When taking in new samples, this package handles rollups and missing dimensions by replacing them with `all` and `none`
respectively.

This client re-resolves DNS by constructing fresh statters for every `flush`.

Dimensions/ values from TwitchTelemetry are used to construct a single metric name (since statsd does not support
dimensions )as follows:
`<service>.<stage>.<substage>.<region>.<everything_else_sorted_on_dimension_name>`

Each dimension name and value from a TwitchTelemetry is mapped directly to a CloudWatch dimension (no naming/ mapping conversions are made).

We recommend reading the [Metric Fulton Doc](https://docs.fulton.twitch.a2z.com/docs/metrics.html) for more information.

#### Referring to this package
To refer to this dependency, refer to its alias of `code.justin.tv/amzn/TwitchTelemetryStatsdMetricsSender`

#### Help
For help using this package, please ping the [#fulton](https://twitch.slack.com/messages/C9BUPDUC8) Slack channel