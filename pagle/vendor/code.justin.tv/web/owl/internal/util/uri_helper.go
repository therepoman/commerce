package util

import (
	"strings"
)

const URISeparator = ","

//formatURIs transforms a slice of uri's into one comma seperated string
func FormatURIs(uris []string) string {
	//todo should we base64 encode the uri's ?
	return strings.Join(uris, URISeparator)
}

//SeparateURI's assumes it is receiving a comma separated string and splits into a []string
func SeparateURIs(uris string) []string {
	return strings.Split(uris, URISeparator)
}
