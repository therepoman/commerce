package util

import (
	"math/rand"
	"time"
)

// Duration returns the duration jittered based on a percentage of the duration's size.
// The jittering is done by doing a +/- of some fraction of the principle duration.
// threshold is the percentage band of the principle to use for the jittering.
// threshold must be a float64 between 0 and 1.
// A threshold of 0 will result in no jittering. A threshold not between 0 and 1
// will return the principle.
// This uses a jitter at the resolution of seconds.
func JitterDuration(principle time.Duration, threshold float64) time.Duration {
	if threshold == 0 || !validPercentage(threshold) {
		// Don't jitter if there is no threshold or if the percentage is out of bounds.
		// Sort of a weird implicit behavior.
		return principle
	}

	// We can pick any time scale we want for the jitter as long as it's an integer,
	// but know that the time conversion here locks us into that resolution.
	// Seconds seems to be like a good resolution, so we're sticking with that.
	band := int(principle.Seconds() * threshold)

	// Produces a duration that is +/- a random integer within the size of band
	jitter := time.Duration(rand.Intn(2*band)-band) * time.Second

	return principle + jitter
}

func validPercentage(num float64) bool {
	return num > 0 && num <= 1
}
