package util

import (
	"crypto/rand"
	"math/big"
)

const alphabet = "abcdefghijklmnopqrstuvwxyz0123456789"

var alphabetLength = big.NewInt(int64(len(alphabet)))

func RandomToken(length int) (string, error) {
	var bytes = make([]byte, length)

	for i := range bytes {
		offset, err := rand.Int(rand.Reader, alphabetLength)
		if err != nil {
			return "", err
		}
		bytes[i] = alphabet[offset.Uint64()]
	}

	return string(bytes), nil
}
