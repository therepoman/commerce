package owl

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"code.justin.tv/foundation/twitchclient"
)

// CreateReviewedScopesRequest specifies the parameters for a request to create
// reviewed scopes for a client
type CreateReviewedScopesRequest struct {
	ReviewedScopes []string `json:"reviewed_scopes"`
}

func (c *client) CreateReviewedScopes(ctx context.Context, clientID string, reviewedScopes []string, reqOpts *twitchclient.ReqOpts) error {
	if len(clientID) == 0 {
		return &twitchclient.Error{
			StatusCode: 400,
			Message:    "Missing param: client_id",
		}
	}

	if reviewedScopes == nil || len(reviewedScopes) == 0 {
		return &twitchclient.Error{
			StatusCode: 400,
			Message:    "Missing param: reviewed_scopes",
		}
	}

	createReviewedScopesReq := &CreateReviewedScopesRequest{
		ReviewedScopes: reviewedScopes,
	}
	body, err := json.Marshal(createReviewedScopesReq)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", fmt.Sprintf("clients/%s/reviewed_scopes", url.PathEscape(clientID)), bytes.NewBuffer(body))
	if err != nil {
		return err
	}
	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.create_reviewed_scopes",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if resp.StatusCode != http.StatusOK {
		return twitchclient.HandleFailedResponse(resp)
	}

	return err
}

func (c *client) DeleteReviewedScope(ctx context.Context, clientID string, reviewedScope string, reqOpts *twitchclient.ReqOpts) error {
	if len(clientID) == 0 {
		return &twitchclient.Error{
			StatusCode: 400,
			Message:    "Missing param: client_id",
		}
	}

	if len(reviewedScope) == 0 {
		return &twitchclient.Error{
			StatusCode: 400,
			Message:    "Missing param: reviewed_scope",
		}
	}

	req, err := c.NewRequest("DELETE", fmt.Sprintf("/clients/%s/reviewed_scopes/%s", url.PathEscape(clientID), url.PathEscape(reviewedScope)), nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.delete_reviewed_scope",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if resp.StatusCode != http.StatusNoContent {
		return twitchclient.HandleFailedResponse(resp)
	}

	return err
}

func (c *client) DeleteReviewedScopes(ctx context.Context, clientID string, reqOpts *twitchclient.ReqOpts) error {
	if len(clientID) == 0 {
		return &twitchclient.Error{
			StatusCode: 400,
			Message:    "Missing param: client_id",
		}
	}

	req, err := c.NewRequest("DELETE", fmt.Sprintf("/clients/%s/reviewed_scopes", url.PathEscape(clientID)), nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.delete_reviewed_scopes",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if resp.StatusCode != http.StatusNoContent {
		return twitchclient.HandleFailedResponse(resp)
	}

	return err
}
