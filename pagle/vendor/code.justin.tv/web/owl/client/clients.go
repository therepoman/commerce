package owl

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/web/owl/oauth2"
)

// SortKey is a sort key used to sort a list of clients
type SortKey string

// SortOrder is a sort order used to sort a list of clients
type SortOrder string

// FilterableColumn is a column which can be used to filter clients
type FilterableColumn string

const (
	// SortKeyClientName is the sort key used to sort on the client's name
	SortKeyClientName SortKey = "name"
	// SortKeyClientID is the sort key used to sort on a client's client_id (i.e. the API key)
	SortKeyClientID SortKey = "client_id"
	// SortAsc specifies that the sort is ordered from lowest to highest
	SortAsc SortOrder = "ASC"
	// SortDesc specifies that the sort is ordered from highest to lowest
	SortDesc SortOrder = "DESC"
	// FilterColName is the key used to filter queries by the "name" column
	FilterColName FilterableColumn = "name"
	// FilterColOwnerID is the key used to filter queries by the "oauth2_client_owner_id" column
	FilterColOwnerID FilterableColumn = "owner_id"
	// FilterColGroupID is the key used to filter queries by the "group_id" column
	FilterColGroupID FilterableColumn = "group_id"

	// CursorParam is the query param used for a query cursor
	CursorParam = "cursor"
	// ShowHiddenParam is the query param used for the show_hidden flag
	ShowHiddenParam = "show_hidden"
	// isExtensionParam is the query param used for the is_extension flag
	isExtensionParam = "is_extension"
	// SortKeyParam is the query param used for a sort key
	SortKeyParam = "sort_key"
	// SortOrderParam is the query param used for a sort order
	SortOrderParam = "sort_order"
)

func (c *client) GetClient(ctx context.Context, clientID string, reqOpts *twitchclient.ReqOpts) (*oauth2.Client, error) {
	if clientID == "" {
		return nil, &twitchclient.Error{
			StatusCode: 400,
			Message:    "No client ID specified",
		}
	}

	req, err := c.NewRequest("GET", fmt.Sprintf("/clients/%s", clientID), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.get_client",
		StatSampleRate: defaultStatSampleRate,
	})

	client := &oauth2.Client{}
	_, err = c.DoJSON(ctx, &client, req, combinedReqOpts)
	if twitchclientErr, ok := err.(*twitchclient.Error); ok {
		if twitchclientErr.StatusCode == http.StatusNotFound {
			return nil, ErrInvalidClientID
		}
	}

	return client, err
}

func (c *client) GetClientByRowID(ctx context.Context, clientRowID string, reqOpts *twitchclient.ReqOpts) (*oauth2.Client, error) {
	if clientRowID == "" {
		return nil, &twitchclient.Error{
			StatusCode: 400,
			Message:    "No client row ID specified",
		}
	}

	req, err := c.NewRequest("GET", fmt.Sprintf("/clients_legacy/%s", clientRowID), nil)
	if err != nil {
		return nil, err
	}
	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.get_client_by_row_id",
		StatSampleRate: defaultStatSampleRate,
	})

	client := &oauth2.Client{}
	_, err = c.DoJSON(ctx, &client, req, combinedReqOpts)
	if twitchclientErr, ok := err.(*twitchclient.Error); ok {
		if twitchclientErr.StatusCode == http.StatusNotFound {
			return nil, ErrInvalidClientRowID
		}
	}

	return client, err
}

// GetClientsResponse specifies the fields returned for a request to get all
// clients given a set of filters
type GetClientsResponse struct {
	Clients    []*oauth2.Client `json:"clients"`
	NextCursor string           `json:"next_cursor"`
}

// GetClients returns a slice of clients that passes the filters provided and a cursor
// to continue enumerating the rest of the matching result set if it is larger than the
// slice returned.
func (c *client) GetClients(ctx context.Context, cursor string, showHidden bool,
	isExtension bool, filters map[FilterableColumn]string, sKey SortKey, sOrder SortOrder,
	reqOpts *twitchclient.ReqOpts) ([]*oauth2.Client, string, error) {

	params := url.Values{
		CursorParam:      {cursor},
		ShowHiddenParam:  {strconv.FormatBool(showHidden)},
		isExtensionParam: {strconv.FormatBool(isExtension)},
		SortKeyParam:     {string(sKey)},
		SortOrderParam:   {string(sOrder)},
	}
	for col, val := range filters {
		params.Set(string(col), val)
	}

	req, err := c.NewRequest("GET", fmt.Sprintf("/clients?%s", params.Encode()), nil)
	if err != nil {
		return nil, "", err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.get_all_clients",
		StatSampleRate: defaultStatSampleRate,
	})
	res := &GetClientsResponse{}
	if _, err := c.DoJSON(ctx, &res, req, combinedReqOpts); err != nil {
		return nil, "", err
	}
	return res.Clients, res.NextCursor, nil
}

// ValidateSecretRequest specifies the parameters for a request to validate a
// client secret
type ValidateSecretRequest struct {
	ClientSecret string `json:"client_secret"`
}

func (c *client) ValidateSecret(ctx context.Context, clientID, clientSecret string, reqOpts *twitchclient.ReqOpts) (bool, error) {
	if clientID == "" {
		return false, ErrInvalidClientID
	}
	if clientSecret == "" {
		return false, ErrInvalidClientSecret
	}

	payload := &ValidateSecretRequest{ClientSecret: clientSecret}
	body, err := json.Marshal(payload)
	if err != nil {
		return false, err
	}
	req, err := c.NewRequest("POST", fmt.Sprintf("/clients/%s/validate_secret", clientID), bytes.NewBuffer(body))
	if err != nil {
		return false, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.validate_secret",
		StatSampleRate: defaultStatSampleRate,
	})

	res, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return false, err
	}

	return res.StatusCode == http.StatusNoContent, nil
}

// CreateClientRequest is a struct that includes all the information needed to create a new OAuth2 application.
type CreateClientRequest struct {
	OwnerID         int      `json:"owner_id"`
	Name            string   `json:"name"`
	RedirectURI     string   `json:"redirect_uri"`
	RedirectURIs    []string `json:"redirect_uris"`
	Category        string   `json:"category"`
	OauthCategoryID int      `json:"oauth_category_id"`
	IsExtension     bool     `json:"is_extension"`
	Mode            string   `json:"client_mode"`
	GroupID         *string  `json:"group_id"`
}

// Validate checks that the CreateClientRequest contains all required params.
func (r *CreateClientRequest) Validate() error {
	missingParams := []string{}
	if r.OwnerID == 0 {
		missingParams = append(missingParams, "owner_id")
	}
	if r.Name == "" {
		missingParams = append(missingParams, "name")
	}
	if r.RedirectURI == "" && len(r.RedirectURIs) == 0 {
		missingParams = append(missingParams, "redirect_uri or redirect_uris")
	}
	if r.OauthCategoryID == 0 {
		missingParams = append(missingParams, "oauth_category_id")
	}
	if len(missingParams) > 0 {
		return fmt.Errorf("Missing required parameters: %s", strings.Join(missingParams, ","))
	}

	if r.RedirectURI != "" && len(r.RedirectURIs) != 0 {
		return ErrRedirectURIPayloadError
	}
	return nil
}

func (c *client) CreateClient(ctx context.Context, createReq CreateClientRequest, values url.Values, reqOpts *twitchclient.ReqOpts) (*oauth2.Client, error) {
	if err := createReq.Validate(); err != nil {
		return nil, err
	}

	if userID := values.Get(UserIDParam); userID == "" {
		return nil, ErrInvalidUserID
	}

	body, err := json.Marshal(createReq)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("POST", fmt.Sprintf("/clients?%s", values.Encode()), bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.create_client",
		StatSampleRate: defaultStatSampleRate,
	})

	client := &oauth2.Client{}
	_, err = c.DoJSON(ctx, client, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	return client, nil
}

// UpdateClientRequest is a struct that indicates the OAuth2 client properties that need to be modified.
type UpdateClientRequest struct {
	ClientName           *string  `json:"name"`
	OwnerID              *string  `json:"owner_id"`
	RedirectURI          *string  `json:"redirect_uri"`
	RedirectURIs         []string `json:"redirect_uris"`
	Category             *string  `json:"category"`
	Mode                 *string  `json:"client_mode"`
	GroupID              *string  `json:"group_id"`
	OauthCategoryID      *int     `json:"oauth_category_id"`
	IsHidden             *bool    `json:"hidden"`
	IsFirstParty         *bool    `json:"is_first_party"`
	IsFuelClient         *bool    `json:"is_fuel_client"`
	PasswordGrantAllowed *bool    `json:"password_grant_allowed"`
	ShouldExpireTokens   *bool    `json:"should_expire_tokens"`
	AllowEmbed           *bool    `json:"allow_embed"`
	IsExtension          *bool    `json:"is_extension"`
	Disabled             *bool    `json:"disabled"`
}

func (c *client) UpdateClient(ctx context.Context, clientID string, updateReq UpdateClientRequest, values url.Values, reqOpts *twitchclient.ReqOpts) (*oauth2.Client, error) {
	if clientID == "" {
		return nil, ErrInvalidClientID
	}

	if userID := values.Get(UserIDParam); userID == "" {
		return nil, ErrInvalidUserID
	}

	if len(updateReq.RedirectURIs) != 0 && updateReq.RedirectURI != nil {
		return nil, ErrRedirectURIPayloadError
	}

	body, err := json.Marshal(updateReq)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("PUT", fmt.Sprintf("/clients/%s?%s", url.PathEscape(clientID), values.Encode()), bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.update_client",
		StatSampleRate: defaultStatSampleRate,
	})

	client := &oauth2.Client{}
	_, err = c.DoJSON(ctx, client, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	return client, nil
}

func (c *client) DeleteClient(ctx context.Context, clientID string, values url.Values, reqOpts *twitchclient.ReqOpts) error {
	if clientID == "" {
		return ErrInvalidClientID
	}

	if userID := values.Get(UserIDParam); userID == "" {
		return ErrInvalidUserID
	}

	req, err := c.NewRequest("DELETE", fmt.Sprintf("/clients/%s?%s", url.PathEscape(clientID), values.Encode()), nil)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.delete_client",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if resp.StatusCode != http.StatusNoContent {
		return twitchclient.HandleFailedResponse(resp)
	}
	return err
}

func (c *client) ResetClientSecret(ctx context.Context, clientID string, values url.Values, reqOpts *twitchclient.ReqOpts) (*oauth2.Client, error) {
	if clientID == "" {
		return nil, ErrInvalidClientID
	}

	if userID := values.Get(UserIDParam); userID == "" {
		return nil, ErrInvalidUserID
	}

	req, err := c.NewRequest("POST", fmt.Sprintf("/clients/%s/reset_secret?%s", url.PathEscape(clientID), values.Encode()), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.reset_secret",
		StatSampleRate: defaultStatSampleRate,
	})

	client := &oauth2.Client{}
	_, err = c.DoJSON(ctx, client, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	return client, nil
}

// ClientRowIDResponse specifies the field returned for a client row ID
type ClientRowIDResponse struct {
	RowID string `json:"client_row_id"`
}

// ClientRowID returns the client row ID given a canonical client ID. This API
// is deprecated and should be removed eventually
func (c *client) ClientRowID(ctx context.Context, clientID string, reqOpts *twitchclient.ReqOpts) (string, error) {
	if clientID == "" {
		return "", &twitchclient.Error{
			StatusCode: 400,
			Message:    "No client ID specified",
		}
	}

	query := url.Values{
		ClientIDParam: {clientID},
	}
	req, err := c.NewRequest("GET", fmt.Sprintf("/client_info?%s", query.Encode()), nil)
	if err != nil {
		return "", err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.owl.client_info",
		StatSampleRate: defaultStatSampleRate,
	})

	var res *ClientRowIDResponse
	_, err = c.DoJSON(ctx, &res, req, combinedReqOpts)
	if twitchclientErr, ok := err.(*twitchclient.Error); ok {
		if twitchclientErr.StatusCode == http.StatusNotFound {
			return "", ErrInvalidClientID
		}
	}

	return res.RowID, err
}
