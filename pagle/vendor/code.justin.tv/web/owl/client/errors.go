package owl

import (
	"errors"
	"fmt"
	"net/http"
)

var (
	ErrInvalidAuthorizationID   = errors.New("Invalid authorization id")
	ErrInvalidAuthorizationCode = errors.New("Invalid authorization code")
	ErrInvalidClientCredentials = errors.New("Invalid client credentials")
	ErrInvalidClientID          = errors.New("Invalid client id")
	ErrInvalidClientRowID       = errors.New("Invalid client row id")
	ErrInvalidClientSecret      = errors.New("Invalid client secret")
	ErrNoAuthorization          = errors.New("No authorization found for the given user and client")
	ErrRedirectMismatch         = errors.New("Parameter redirect_uri does not match registered URI")
	ErrInvalidUserID            = errors.New("Invalid user id")
	ErrForbiddenClientID        = errors.New("Invalid client id")
	ErrRedirectURIPayloadError  = errors.New("only one of redirect_uri or redirect_uris can be set")
)

// ErrorResponse is the format for errors returned by Owl
type ErrorResponse struct {
	Status  int    `json:"status,omitempty"`
	Message string `json:"message,omitempty"`
	Error   string `json:"error,omitempty"`
}

// NewErrorResponse constructs an error response for errors returned by Owl
func NewErrorResponse(status int, message string) *ErrorResponse {
	return &ErrorResponse{
		Status:  status,
		Message: message,
		Error:   http.StatusText(status),
	}
}

// Error implements the error interface and specifies the error name and error
// message for an authorize request
type Error struct {
	Code    string
	Message string
}

func (e *Error) Error() string {
	return fmt.Sprintf("Invalid request, error code: %s, error description: %s", e.Code, e.Message)
}
