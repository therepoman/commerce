package oauth2

import (
	"crypto/sha1"
	"encoding/hex"
)

// Hashify converts a token to a hex sha1 digest
//
// Tokens are stored hashed for security
func Hashify(token string) (digest string) {
	hasher := sha1.New()
	hasher.Write([]byte(token))
	digest = hex.EncodeToString(hasher.Sum(nil))
	return
}
