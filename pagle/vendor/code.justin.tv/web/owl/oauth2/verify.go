package oauth2

// WhitelistClients is a list of client ids whose tokens we will automatically upgrade.
// We need to be able to grant additional scopes to internal clients, such as iOS, so they can use
// APIs with scopes created after authorizing. If one of these clients make a request missing
// whitelisted scopes, we'll automatically add them

var clientScopeAugmentMapping = map[string][]string{
	// iphone
	"5880": {
		"channel_commercial",
		"channel_editor",
		"channel_feed_edit",
		"channel_feed_read",
		"channel_feed_report",
		"channel_read",
		"collections_edit",
		"communities_edit",
		"communities_moderate",
		"user_edit",
		"user_follows_edit",
		"user_friends_edit",
		"user_friends_read",
		"user_presence_edit",
		"user_presence_friends_read",
		"user_subscriptions",
		"user_subscriptions_edit",
	},
	// ipad
	"11769": {
		"channel_commercial",
		"channel_editor",
		"channel_feed_edit",
		"channel_feed_read",
		"channel_feed_report",
		"channel_read",
		"collections_edit",
		"communities_edit",
		"communities_moderate",
		"user_edit",
		"user_follows_edit",
		"user_friends_edit",
		"user_friends_read",
		"user_presence_edit",
		"user_presence_friends_read",
		"user_subscriptions",
		"user_subscriptions_edit",
	},
	// android
	"17693": {
		"channel_commercial",
		"channel_editor",
		"channel_feed_edit",
		"channel_feed_read",
		"channel_feed_report",
		"channel_read",
		"collections_edit",
		"communities_edit",
		"communities_moderate",
		"user_edit",
		"user_follows_edit",
		"user_friends_edit",
		"user_friends_read",
		"user_presence_edit",
		"user_presence_friends_read",
		"user_subscriptions",
		"user_subscriptions_edit",
		"whispers_edit",
	},
	// site / web-client
	"20094": {
		"channel_commercial",
		"channel_editor",
		"channel_feed_edit",
		"channel_feed_read",
		"channel_feed_report",
		"channel_read",
		"collections_edit",
		"communities_edit",
		"communities_moderate",
		"user_edit",
		"user_entitlements_read",
		"user_follows_edit",
		"user_friends_edit",
		"user_friends_read",
		"user_presence_edit",
		"user_presence_friends_read",
		"user_subscriptions",
		"user_subscriptions_edit",
	},
	// Curse Production
	"71962": {
		"user_edit",
		"user_entitlements_read",
		"channel_commercial",
		"channel_editor",
		"channel_feed_edit",
		"channel_feed_read",
		"channel_feed_report",
		"channel_read",
		"channel_subscriptions",
		"channel_check_subscription",
		"collections_edit",
		"communities_edit",
		"communities_moderate",
		"user_follows_edit",
		"user_friends_edit",
		"user_friends_read",
		"user_presence_edit",
		"user_presence_friends_read",
		"user_subscriptions",
		"user_subscriptions_edit",
	},
	// Curse Desktop (legacy)
	"73427": {
		"user_edit",
		"user_entitlements_read",
		"channel_commercial",
		"channel_editor",
		"channel_feed_edit",
		"channel_feed_read",
		"channel_feed_report",
		"channel_read",
		"channel_subscriptions",
		"channel_check_subscription",
		"collections_edit",
		"communities_edit",
		"communities_moderate",
		"user_follows_edit",
		"user_friends_edit",
		"user_friends_read",
		"user_presence_edit",
		"user_presence_friends_read",
		"user_subscriptions",
		"user_subscriptions_edit",
	},
	//Curse Dev
	"104058": {
		"user_edit",
		"user_entitlements_read",
		"channel_commercial",
		"channel_editor",
		"channel_feed_edit",
		"channel_feed_read",
		"channel_feed_report",
		"channel_subscriptions",
		"channel_check_subscription",
		"channel_read",
		"collections_edit",
		"communities_edit",
		"communities_moderate",
		"user_follows_edit",
		"user_friends_edit",
		"user_friends_read",
		"user_presence_edit",
		"user_presence_friends_read",
		"user_subscriptions",
		"user_subscriptions_edit",
	},
	//Curse Stage
	"107242": {
		"user_edit",
		"user_entitlements_read",
		"channel_commercial",
		"channel_editor",
		"channel_feed_edit",
		"channel_feed_read",
		"channel_feed_report",
		"channel_read",
		"channel_subscriptions",
		"channel_check_subscription",
		"collections_edit",
		"communities_edit",
		"communities_moderate",
		"user_follows_edit",
		"user_friends_edit",
		"user_friends_read",
		"user_presence_edit",
		"user_presence_friends_read",
		"user_subscriptions",
		"user_subscriptions_edit",
	},
	// Drew McLean test app
	"8187": {
		"channel_commercial",
		"channel_editor",
		"channel_feed_edit",
		"channel_feed_read",
		"channel_feed_report",
		"channel_read",
		"user_edit",
		"user_follows_edit",
		"user_friends_edit",
		"user_friends_read",
		"user_presence_edit",
		"user_presence_friends_read",
		"user_subscriptions",
		"user_subscriptions_edit",
	},
	// Twilight Production (twitch.tv)
	"121931": {
		"channel_commercial",
		"channel_editor",
		"channel_feed_edit",
		"channel_feed_read",
		"channel_feed_report",
		"channel_read",
		"collections_edit",
		"communities_edit",
		"communities_moderate",
		"user_edit",
		"user_entitlements_read",
		"user_follows_edit",
		"user_friends_edit",
		"user_friends_read",
		"user_presence_edit",
		"user_presence_friends_read",
		"user_subscriptions",
		"user_subscriptions_edit",
	},
	// Twilight Staging (twitch.tech)
	"122163": {
		"channel_commercial",
		"channel_editor",
		"channel_feed_edit",
		"channel_feed_read",
		"channel_feed_report",
		"channel_read",
		"collections_edit",
		"communities_edit",
		"communities_moderate",
		"user_edit",
		"user_entitlements_read",
		"user_follows_edit",
		"user_friends_edit",
		"user_friends_read",
		"user_presence_edit",
		"user_presence_friends_read",
		"user_subscriptions",
		"user_subscriptions_edit",
	},
	// Twilight Local Dev
	"181814": {
		"channel_commercial",
		"channel_editor",
		"channel_feed_edit",
		"channel_feed_read",
		"channel_feed_report",
		"channel_read",
		"collections_edit",
		"communities_edit",
		"communities_moderate",
		"user_edit",
		"user_entitlements_read",
		"user_follows_edit",
		"user_friends_edit",
		"user_friends_read",
		"user_presence_edit",
		"user_presence_friends_read",
		"user_subscriptions",
		"user_subscriptions_edit",
	},
}

// AugmentScope adds Whitelisted scopes into the Authorization if the grant is for a Whitelisted Client
func AugmentScope(auth *Authorization) *Authorization {
	if auth == nil || auth == InvalidAuth {
		return InvalidAuth
	}

	augmentList, ok := clientScopeAugmentMapping[auth.ClientRowID]
	if !ok {
		return auth
	}

	// Whitelisted client, implicitly grant whitelistedscopes.
	for _, scope := range augmentList {
		if !contains(auth.Scope, []string{scope}) {
			auth.Scope = append(auth.Scope, scope)
		}
	}

	return auth
}

// VerifyScope returns the authorization if it has sufficient scopes. Otherwise returns InvalidAuth
func VerifyScope(auth *Authorization, scopes []string) *Authorization {
	if auth == nil || auth == InvalidAuth || !contains(auth.Scope, scopes) {
		return InvalidAuth
	}

	return auth
}
