package oauth2

import (
	"fmt"
	"strings"
	"time"

	"code.justin.tv/web/owl/internal/util"
)

type ResponseType int

const (
	TokenResponse ResponseType = iota + 1
	CodeResponse
)

const (
	accessTokenLength  = 30
	authCodeLength     = 30
	refreshTokenLength = 50
	clientIDLength     = 30
	clientSecretLength = 30
)

const (
	UserTokenLifetime     = 4 * time.Hour
	AppTokenLifetime      = 60 * 24 * time.Hour
	ImplicitTokenLifetime = 60 * 24 * time.Hour

	BearerTokenType = "bearer"
)

var (
	ErrNotFound            = fmt.Errorf("oauth2: OAuth2 object not found")
	ErrExpiredAuth         = fmt.Errorf("oauth2: authorization expired")
	ErrInvalidRefreshToken = fmt.Errorf("oauth2: invalid refresh token")
	// InvalidAuth represents that no authorization was found for a token
	InvalidAuth = &Authorization{Valid: false}
)

// NonExpiringTime is the UTC representation of the timestamp given to a token
// with no expiry. This is the same as a zero-valued time.Time in UTC
var NonExpiringTime time.Time = time.Time{}.In(time.UTC)

// ParseScopes takes a space separated string of scopes and returns an array of scopes,
// used for parsing scopes from HTTP params or the DB.
func ParseScope(scope string) []string {
	if scope == "" {
		return []string{}
	}
	return util.DedupeSlice(strings.Split(scope, " "))
}

// BuildScope takes an arary of scopes and returns a space separated string of scopes so
// they can be used in an HTTP response or stored in the DB.
func BuildScope(scope []string) string {
	return strings.Join(util.DedupeSlice(scope), " ")
}

// contains checks whether requestedScopes is a subset of authScopes
func contains(authScopes, requestedScopes []string) bool {
	scopeMap := make(map[string]struct{})
	for _, scope := range authScopes {
		scopeMap[scope] = struct{}{}
	}

	for _, scope := range requestedScopes {
		if _, ok := scopeMap[scope]; !ok && scope != "" {
			return false
		}
	}
	return true
}

// ExpiresIn converts a an expiration timestamp into a TTL, measured in seconds.
func ExpiresIn(expiresAt time.Time) int {
	if expiresAt.IsZero() {
		return 0
	}
	expiresIn := int(expiresAt.Sub(time.Now()) / time.Second)
	if expiresIn > 0 {
		return expiresIn
	}
	return 0
}
