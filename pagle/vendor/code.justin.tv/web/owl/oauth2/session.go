package oauth2

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

// Session represents an authorization of a client
type Session struct {
	AuthorizationID string     `json:"authorization_id"`
	ClientID        string     `json:"client_id"`
	ClientRowID     string     `json:"-"`
	CreatedAt       *time.Time `json:"created_at"`
	IDTokenClaims   []string   `json:"id_token_claims"`
	Scopes          []string   `json:"scopes"`
	SessionID       string     `json:"session_id"`
	UpdatedAt       *time.Time `json:"updated_at"`
	UserID          string     `json:"user_id"`
	UserinfoClaims  []string   `json:"userinfo_claims"`
}

const sessionTokenLength = 30

func NewSession(clientID, clientRowID, userID, authorizationID string, scopes, idTokenClaims, userinfoClaims []string) (*Session, error) {
	return &Session{
		AuthorizationID: authorizationID,
		ClientID:        clientID,
		ClientRowID:     clientRowID,
		IDTokenClaims:   idTokenClaims,
		Scopes:          scopes,
		SessionID:       uuid.NewV4().String(),
		UserID:          userID,
		UserinfoClaims:  userinfoClaims,
	}, nil
}
