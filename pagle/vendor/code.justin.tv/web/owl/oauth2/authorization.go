package oauth2

import (
	"time"

	"github.com/satori/go.uuid"
)

// Authorization represents an OAuth2 Authorization
// While owner ids are integers at present, we should support
// more advanced "GUID-style" identifiers that may indicate information
// like sharding references or site association
type Authorization struct {
	Valid             bool      `json:"valid"`
	ID                int       `json:"_id"`
	Scope             []string  `json:"scope"`
	OwnerID           string    `json:"owner_id"`
	ClientRowID       string    `json:"client_id"`
	ClientIDCanonical string    `json:"client_id_canonical"`
	CreatedAt         time.Time `json:"created_at"`
	UpdatedAt         time.Time `json:"updated_at"`
	ExpiresIn         int       `json:"expires_in"`
	UUID              string    `json:",omitempty"`
	IDTokenClaims     string    `json:"id_token_claims,omitempty"`
	UserinfoClaims    string    `json:"userinfo_claims,omitempty"`
}

func NewAuthorization(ownerID string, client *Client, scope []string) *Authorization {
	// Always initialize scope to empty string slice if it's sent in as nil
	if scope == nil {
		scope = []string{}
	}
	return &Authorization{
		OwnerID:           ownerID,
		ClientRowID:       client.RowID,
		ClientIDCanonical: client.ClientID,
		Scope:             scope,
		UUID:              uuid.NewV4().String(),
	}
}

func (a *Authorization) IsAppAccessToken() bool {
	return a.OwnerID == a.ClientIDCanonical
}
