package wire

import (
	"time"

	"github.com/gofrs/uuid"
	"github.com/golang/protobuf/ptypes"
	"github.com/pkg/errors"
)

func ToPublicHeader(input *HeaderV1) (*PublicHeader, error) {
	created, err := ptypes.Timestamp(input.CreatedAt)
	if err != nil {
		return nil, errors.Wrap(err, "Could not decode timestamp portion of header")
	}
	msgID, err := uuid.FromBytes(input.MessageId)
	if err != nil {
		return nil, errors.Wrap(err, "Could not decode UUID from header")
	}
	h := &PublicHeader{
		MessageID:   msgID,
		EventType:   input.EventType,
		CreatedAt:   created,
		Environment: input.Environment,
	}
	return h, nil
}

// this is converted to public types
type PublicHeader struct {
	MessageID   uuid.UUID
	EventType   string
	CreatedAt   time.Time
	Environment string
}
