package eventbus

import (
	"context"

	"github.com/golang/protobuf/proto"
)

// Message is implemented by auto-generated code for message types.
type Message interface {
	proto.Message
	EventBusName() string
}

// Publisher is used to send messages into the event bus.
//
// It is implemented by the canonical event bus SNS publisher and by
// several mock implementations available in the 'mock' package.
type Publisher interface {
	// Publish a message to the event bus.
	// This method will block until the message is queued into the bus or errors.
	Publish(ctx context.Context, message Message) error
}
