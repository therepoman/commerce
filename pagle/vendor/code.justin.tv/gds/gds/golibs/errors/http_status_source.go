package errors

// HTTPStatusSource returns an HTTPStatus when queried
type HTTPStatusSource interface {
	HTTPStatus() int
}

// HTTPStatusError is an error with an HTTPStatus
type HTTPStatusError interface {
	HTTPStatusSource
	error
}

// HTTPStatusAdaptable can return a copy as an HTTPStatusError
type HTTPStatusAdaptable interface {
	WithHTTPStatus(httpStatus int) HTTPStatusError
}

// WithHTTPStatus attaches or replaces the http status on an existing error
func WithHTTPStatus(e error, httpStatus int) HTTPStatusError {
	if cast, ok := e.(HTTPStatusAdaptable); ok {
		return cast.WithHTTPStatus(httpStatus)
	}
	return extract(e).WithHTTPStatus(httpStatus)
}

// GetHTTPStatusOrDefault extracts the HTTP status from an error or falls back
// to the specified default
func GetHTTPStatusOrDefault(e error, defaultHTTPStatus int) int {
	if cast, ok := e.(HTTPStatusSource); ok {
		return cast.HTTPStatus()
	}
	return defaultHTTPStatus
}
