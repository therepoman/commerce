package errors

import (
	"encoding/json"
)

// MarshalErrorForHTTP converts an arbitrary error to a json body string for
// transmission on the wire
func MarshalErrorForHTTP(err error) []byte {
	converted := extract(err)
	body, err := json.MarshalIndent(converted, "", "  ")
	if err == nil {
		return body
	}

	// we failed to marshal details, fall back to marshalling without them
	// which will not fail (just an int + 2 string combo at most)
	converted.Details = nil
	body, _ = json.MarshalIndent(converted, "", "  ")
	return body
}

// UnmarshalErrorForHTTP converts an incoming json blob and http status code
// into an error for use in a client. The second error returned is not nil if
// the value could not be unmarshaled.  This unmarshal is compatible with all
// errors sent by twitchhttp.
func UnmarshalErrorForHTTP(msg []byte) (result, failure error) {
	var data errorImpl
	failure = json.Unmarshal(msg, &data)
	if failure != nil {
		result = nil
		return
	}
	result = data.wrap()
	return
}
