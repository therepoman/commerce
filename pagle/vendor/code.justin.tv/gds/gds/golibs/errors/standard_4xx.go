package errors

import (
	"net/http"
)

// comment source: https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
var (
	// ErrBadRequest - The server cannot or will not process the request due to an apparent client error (e.g., malformed request syntax, size too large, invalid request message framing, or deceptive request routing)
	ErrBadRequest = makeHttpError(http.StatusBadRequest)
	// ErrUnauthorized - Similar to 403 Forbidden, but specifically for use when authentication is required and has failed or has not yet been provided. The response must include a WWW-Authenticate header field containing a challenge applicable to the requested resource.
	ErrUnauthorized = makeHttpError(http.StatusUnauthorized)
	// ErrPaymentRequired - Reserved for future use. The original intention was that this code might be used as part of some form of digital cash or micropayment scheme
	ErrPaymentRequired = makeHttpError(http.StatusPaymentRequired)
	// ErrForbidden - The request was valid, but the server is refusing action. The user might not have the necessary permissions for a resource, or may need an account of some sort
	ErrForbidden = makeHttpError(http.StatusForbidden)
	// ErrNotFound - The requested resource could not be found but may be available in the future. Subsequent requests by the client are permissible
	ErrNotFound = makeHttpError(http.StatusNotFound)
	// ErrMethodNotAllowed - A request method is not supported for the requested resource; for example, a GET request on a form that requires data to be presented via POST, or a PUT request on a read-only resource
	ErrMethodNotAllowed = makeHttpError(http.StatusMethodNotAllowed)
	// ErrNotAcceptable - The requested resource is capable of generating only content not acceptable according to the Accept headers sent in the request. See Content negotiation.
	ErrNotAcceptable = makeHttpError(http.StatusNotAcceptable)
	// ErrProxyAuthRequired - The client must first authenticate itself with the proxy
	ErrProxyAuthRequired = makeHttpError(http.StatusProxyAuthRequired)
	// ErrRequestTimeout - The server timed out waiting for the request. According to HTTP specifications: "The client did not produce a request within the time that the server was prepared to wait
	ErrRequestTimeout = makeHttpError(http.StatusRequestTimeout)
	// ErrConflict - Indicates that the request could not be processed because of conflict in the current state of the resource, such as an edit conflict between multiple simultaneous updates
	ErrConflict = makeHttpError(http.StatusConflict)
	// ErrGone - Indicates that the resource requested is no longer available and will not be available again. This should be used when a resource has been intentionally removed and the resource should be purged instead
	ErrGone = makeHttpError(http.StatusGone)
	// ErrLengthRequired - The request did not specify the length of its content, which is required by the requested resource
	ErrLengthRequired = makeHttpError(http.StatusLengthRequired)
	// ErrPreconditionFailed - The server does not meet one of the preconditions that the requester put on the request
	ErrPreconditionFailed = makeHttpError(http.StatusPreconditionFailed)
	// ErrRequestPayloadTooLarge - The request is larger than the server is willing or able to process
	ErrRequestPayloadTooLarge = makeHttpError(http.StatusRequestEntityTooLarge)
	// ErrRequestURITooLong - The URI provided was too long for the server to process
	ErrRequestURITooLong = makeHttpError(http.StatusRequestURITooLong)
	// ErrUnsupportedMediaType - The request entity has a media type which the server or resource does not support
	ErrUnsupportedMediaType = makeHttpError(http.StatusUnsupportedMediaType)
	// ErrRangeNotSatisfiable - The client has asked for a portion of the file (byte serving), but the server cannot supply that portion
	ErrRangeNotSatisfiable = makeHttpError(http.StatusRequestedRangeNotSatisfiable)
	// ErrExpectationFailed - The server cannot meet the requirements of the Expect request-header field
	ErrExpectationFailed = makeHttpError(http.StatusExpectationFailed)
	// ErrTeapot - This code was defined in 1998 as one of the traditional IETF April Fools' jokes
	ErrTeapot = makeHttpError(http.StatusTeapot) // RFC 2324
	// ErrUnprocessableEntity - The request was well-formed but was unable to be followed due to semantic errors
	ErrUnprocessableEntity = makeHttpError(http.StatusUnprocessableEntity)
	// ErrLocked - The resource that is being accessed is locked
	ErrLocked = makeHttpError(http.StatusLocked)
	// ErrFailedDependency - The request failed because it depended on another request and that request failed (e.g., a PROPPATCH)
	ErrFailedDependency = makeHttpError(http.StatusFailedDependency)
	// ErrUpgradeRequired - The client should switch to a different protocol such as TLS/1.0, given in the Upgrade header field
	ErrUpgradeRequired = makeHttpError(http.StatusUpgradeRequired)
	// ErrPreconditionRequired - The origin server requires the request to be conditional. Intended to prevent the 'lost update' problem, where a client GETs a resource's state, modifies it, and PUTs it back to the server, when meanwhile a third party has modified the state on the server, leading to a conflict.
	ErrPreconditionRequired = makeHttpError(http.StatusPreconditionRequired)
	// ErrTooManyRequests - The user has sent too many requests in a given amount of time. Intended for use with rate-limiting schemes
	ErrTooManyRequests = makeHttpError(http.StatusTooManyRequests)
	// ErrRequestHeaderFieldsTooLarge - The server is unwilling to process the request because either an individual header field, or all the header fields collectively, are too large
	ErrRequestHeaderFieldsTooLarge = makeHttpError(http.StatusRequestHeaderFieldsTooLarge)
	// ErrUnavailableForLegalReasons - A server operator has received a legal demand to deny access to a resource or to a set of resources that includes the requested resource
	ErrUnavailableForLegalReasons = makeHttpError(http.StatusUnavailableForLegalReasons)
)
