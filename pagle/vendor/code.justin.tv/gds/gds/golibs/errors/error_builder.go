package errors

// ErrorBuilder is an interface that allows crafting one-off StructuredError
// values
type ErrorBuilder interface {
	WithHTTPStatus(httpStatus int) ErrorBuilder
	WithDetails(details Details) ErrorBuilder
	WithErrorCode(errorCode string) ErrorBuilder
	Build() StructuredError
}

// NewBuilder returns an interface that can build an error with optional
// errorCode, status, and details. Structured errors that want to build strings
// intelligently from member fields should implement the ErrorCodeError/
// HTTPStatusError/DetailsError directly instead so that they can continue to be
// queried for their structured data through an intelligent interface.
func NewBuilder(message string) ErrorBuilder {
	return &errorBuilder{&errorImpl{Message: message}}
}

type errorBuilder struct {
	err *errorImpl
}

func (b *errorBuilder) WithHTTPStatus(httpStatus int) ErrorBuilder {
	b.err.HTTPStatus = &httpStatus
	return b
}

func (b *errorBuilder) WithDetails(details Details) ErrorBuilder {
	b.err.Details = &details
	return b
}

func (b *errorBuilder) WithErrorCode(errorCode string) ErrorBuilder {
	b.err.ErrorCode = &errorCode
	return b
}

func (b *errorBuilder) Build() StructuredError {
	return b.err.wrap()
}
