# Errors
This package provides a common, extensible format for describing structured errors between programs with support for error codes, automatic http status extraction, and arbitrary details that can be injected into client-side localized messages.

The format is backward compatible with `twitchclient.Error` so that all existing clients will continue to operate as expected when moving error implementations to this package.

## Structure
This package declares a set of implicit interfaces that error types can include to support more intelligent client responses: `ErrorCodeSource`, `HTTPStatusSource`, and `DetailsSource`.

An `ErrorCodeSource` contains an ErrorCode() string that can be used to select the appropriate localized client string or register typed error handling.

An `HTTPStatusSource` allows a service to report a correct status code without consulting an external lookup table.

A `DetailsSource` provides error-specific parameters that may be used in localized strings or recovery logic.

## Declaration
For one-off or simple error cases, an `ErrorBuilder` has been provided that allows the composition of error declarations:

`var err = errors.NewBuilder("Unauthorized").WithHTTPStatus(http.StatusUnauthorized).WithErrorCode("unauthorized").Build()`

In cases where more information is appropriate, a concrete struct can be used that implements the optional interfaces; this structure can then be automatically converted to the standard format on the wire:

```
type ErrExtensionNotFound struct{ id string }

func NewErrExtensionNotFound(id string) error     { return &ErrExtensionNotFound{id} }
func (e *ErrExtensionNotFound) Error() string     { return "Extension \"" + e.id + "\" could not be found" }
func (e *ErrExtensionNotFound) ErrorCode() string { return "extension_not_found" }
func (e *ErrExtensionNotFound) HTTPStatus() int   { return 404 }
func (e *ErrExtensionNotFound) Details() Details  { return Details{"id": e.id} }

var missing = NewErrExtensionNotFound("myextensionid")
```

## Promotion and Conversion
Any golang error can be promoted to add data for the optional interfaces using the following setters: `errors.WithHTTPStatus`, `errors.WithErrorCode`, and `errors.WithDetails`.  After promotion, the error will have the appropriate annotations on the wire when it is run through the custom marshaler.  

In addition, many legacy error formats in use by extensions code have their own way of storing information such as status codes. The `golibs/errors/errconv` package provides a notion of format converters that allow servers to standardize their output. The converters are in a subpackage so that they and their dependencies don't need to be vendored by clients.

The `golibs/http/NewAPIHandler` call has already integrated intelligent defaults and format conversions for you.  The standard error conversion and default logic provided there can be overridden by using `errconv.StoreConverter` to inject an error handler into a service's standard `context.Context`.

## Marshaling and Wire Format
When the sample above is run through `MarshalErrorForHTTP` it converts to the following JSON:
```
{
  "message": "Extension \"myextensionid\" could not be found",
  "error_code": "extension_not_found",
  "details": {
    "id": "myextensionid"
  },
  "status": 404
}
```
This format is 100% compatible with the serialization of `twitchclient.Error` for interoperability with standard twitch go http clients.

## Unmarshaling
Calling `errors.UnmarshalErrorForHTTP` will automatically construct an error type that correctly implements the subset of `errors.DetailsError`, `errors.ErrorCodeError`, or `errors.HTTPStatusError` that was specified on the wire. This means that all `twitchclient.Error` instances will correctly report their status code when interoperating with a legacy service. 

Calling `errors.ExtractFromHTTPResponse` will automatically extract a safe to report error from an http response if one is present, and should be used to build clients.

## Query
Since most golang code returns the `error` type, there are also convenience functions to extract the optional information from an arbitrary error: `errors.GetHTTPStatusOrDefault`, `errors.GetErrorCode`, and `errors.GetDetails`.
