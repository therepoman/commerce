package documents

import (
	"fmt"
	"strings"

	"code.justin.tv/gds/gds/extensions/ems/protocol"
	"code.justin.tv/gds/gds/extensions/models"
)

// ActivationRequirement is an enum that represents tasks that must be
// completed before an install can be activated
type ActivationRequirement string

const (
	// Anchor : every install must declare its attachment point
	Anchor ActivationRequirement = "anchor"
	// Configure : every install must be properly configured
	Configure ActivationRequirement = "configure"
	// Configure : every install must be properly configured
	HostedConfig ActivationRequirement = "hostedConfig"
	// Release : this was a public install but the extension was unpublished
	Release ActivationRequirement = "release"
	// Unknown : unable to interpret this value, code is likely out of date
	Unknown ActivationRequirement = "unknown"

	hostedConfigPrefix    = "hosted"
	hostedConfigSeparator = ":"
)

// ExtensionViews contains all of the view-specific information
type ExtensionViews struct {
	Mobile       *MobileExtensionView       `json:"mobile,omitempty"`
	Panel        *PanelExtensionView        `json:"panel,omitempty"`
	VideoOverlay *VideoOverlayExtensionView `json:"video_overlay,omitempty"`
	Component    *ComponentExtensionView    `json:"component,omitempty"`
	Hidden       *HiddenExtensionView       `json:"hidden,omitempty"`
	Config       *ConfigExtensionView       `json:"config,omitempty"`
	LiveConfig   *LiveConfigExtensionView   `json:"live_config,omitempty"`
}

type MobileExtensionView struct {
	CanLinkExternalContent bool   `json:"can_link_external_content"`
	ViewerPath             string `json:"viewer_path"`
	ViewerURL              string `json:"viewer_url"`
}

type PanelExtensionView struct {
	CanLinkExternalContent bool   `json:"can_link_external_content"`
	Height                 int    `json:"height"`
	ViewerPath             string `json:"viewer_path"`
	ViewerURL              string `json:"viewer_url"`
}

type VideoOverlayExtensionView struct {
	CanLinkExternalContent bool   `json:"can_link_external_content"`
	ViewerPath             string `json:"viewer_path"`
	ViewerURL              string `json:"viewer_url"`
}

type ComponentExtensionView struct {
	AspectWidth            int     `json:"aspect_width"`
	AspectHeight           int     `json:"aspect_height"`
	AspectRatioX           int     `json:"aspect_ratio_x"`
	AspectRatioY           int     `json:"aspect_ratio_y"`
	Autoscale              bool    `json:"autoscale"`
	ScalePixels            int     `json:"scale_pixels"`
	CanLinkExternalContent bool    `json:"can_link_external_content"`
	Size                   float64 `json:"size"`
	TargetHeight           int     `json:"target_height"`
	ViewerPath             string  `json:"viewer_path"`
	ViewerURL              string  `json:"viewer_url"`
	Zoom                   bool    `json:"zoom"`
	ZoomPixels             int     `json:"zoom_pixels"`
}

type HiddenExtensionView struct {
	CanLinkExternalContent bool   `json:"can_link_external_content"`
	ViewerPath             string `json:"viewer_path"`
	ViewerURL              string `json:"viewer_url"`
}

type ConfigExtensionView struct {
	CanLinkExternalContent bool   `json:"can_link_external_content"`
	ViewerPath             string `json:"viewer_path"`
	ViewerURL              string `json:"viewer_url"`
}

type LiveConfigExtensionView struct {
	CanLinkExternalContent bool   `json:"can_link_external_content"`
	ViewerPath             string `json:"viewer_path"`
	ViewerURL              string `json:"viewer_url"`
}

// Deprecated but still used by older mobile clients
type ViewerURLs struct {
	Mobile       string `json:"mobile,omitempty"`
	Panel        string `json:"panel,omitempty"`
	VideoOverlay string `json:"video_overlay,omitempty"`
	Component    string `json:"component,omitempty"`
	Hidden       string `json:"hidden,omitempty"`
}

// ExtensionDocument is the data we send back on the wire about an Extension.
type ExtensionDocument struct {
	Anchor                       string                             `json:"anchor"`
	AssetURLs                    []string                           `json:"asset_urls"`
	AuthorName                   string                             `json:"author_name"`
	BaseURI                      string                             `json:"base_uri"`
	BitsEnabled                  bool                               `json:"bits_enabled"` // deprecated, use BitsSupportLevel
	BitsSupportLevel             protocol.BitsSupportLevel          `json:"bits_support_level"`
	Categories                   []string                           `json:"categories"`
	CanInstall                   bool                               `json:"can_install"`
	ConfigURL                    string                             `json:"config_url"` // deprecated, need to remove usage from GQL
	ConfigurationLocation        string                             `json:"configuration_location"`
	Description                  string                             `json:"description"`
	EULATOSURL                   string                             `json:"eula_tos_url"`
	Games                        []int                              `json:"games"`
	ContentMatchedGames          []int                              `json:"content_matched_games"`
	HasChatSupport               bool                               `json:"has_chat_support"`
	IconURL                      string                             `json:"icon_url"`
	IconURLs                     map[protocol.IconSize]string       `json:"icon_urls"`
	ID                           string                             `json:"id"`
	InstallationCount            int                                `json:"installation_count"` // deprecated, but removing it is a PITA
	LiveConfigURL                string                             `json:"live_config_url"`    // deprecated, need to remove usage from GQL
	Name                         string                             `json:"name"`
	PanelHeight                  int                                `json:"panel_height"` // deprecated, need to remove usage from GQL
	PrivacyPolicyURL             string                             `json:"privacy_policy_url"`
	RequestIdentityLink          bool                               `json:"request_identity_link"`
	RequiredBroadcasterAbilities []string                           `json:"required_broadcaster_abilities"`
	RequiredConfiguration        string                             `json:"required_configuration"`
	ScreenshotURLs               []string                           `json:"screenshot_urls"`
	SKU                          string                             `json:"sku"`
	State                        string                             `json:"state"`
	SubscriptionsSupportLevel    protocol.SubscriptionsSupportLevel `json:"subscriptions_support_level"`
	Summary                      string                             `json:"summary"`
	SupportEmail                 string                             `json:"support_email"`
	VendorCode                   string                             `json:"vendor_code"`
	Version                      string                             `json:"version"`
	ViewerSummary                string                             `json:"viewer_summary"`
	ViewerURL                    string                             `json:"viewer_url"` // deprecated, need to remove usage from GQL
	ViewerURLs                   ViewerURLs                         `json:"viewer_urls"`
	Views                        ExtensionViews                     `json:"views"`
	WhitelistedConfigURLs        []string                           `json:"whitelisted_config_urls"`
	WhitelistedPanelURLs         []string                           `json:"whitelisted_panel_urls"`
}

// CompositeID returns a unique ID for the extension which is a combination of the
// extension ID and the version
func (e *ExtensionDocument) CompositeID() string {
	return e.ID + ":" + e.Version
}

// ExtensionsDocument is the data we send back on the wire about multiple extensions,
// along with the TOTAL number of extensions in the full query, for pagination purposes
type ExtensionsDocument struct {
	Count      int64                `json:"count"`
	Extensions []*ExtensionDocument `json:"extensions"`
}

// UploaderResponse is the data we send back for uploading asset requests
type UploaderResponse struct {
	// Url is a pre-signed upload URL, which comes from AWS's API
	Url string `protobuf:"bytes,1,opt,name=url" json:"url,omitempty"`
	// UploadID is a uuid
	UploadId string `protobuf:"bytes,2,opt,name=upload_id,json=uploadId" json:"upload_id,omitempty"`
}

// UploaderResponses is the map of UploaderResponses we send back for uploader
// Key of map represents uploading asset's type
type UploaderResponses struct {
	UploaderResponses map[string][]*UploaderResponse `json:"uploader_responses"`
}

// AdminExtensionsDocument is a raw database dump, used for internal-only endpoints
// such as Curse Mods who want to suck the entire list of extensions into their thing
type AdminExtensionsDocument struct {
	Count      int64               `json:"count"`
	Extensions []*models.Extension `json:"extensions"`
}

// CanAddExtensionsResponse contains a flag that determines if it is possible to add
// extensions
type CanAddExtensionsResponse struct {
	CanAddExtensions bool `json:"can_add_extensions"`
}

// ChannelDocument is the data we send back on the wire about a Channel; it has
// some installation information but not the things that are specific to a user
// or an extension (e.g., can you activate it)
type ChannelDocument struct {
	ID               string                           `json:"id"` // redundant with channel_id, will remove when visage upgrade lands
	ChannelID        string                           `json:"channel_id"`
	ExtensionID      string                           `json:"extension_id"`
	Version          string                           `json:"version"`
	ActivationConfig *ActivationConfigurationDocument `json:"activation_configuration"`
	ActivationState  string                           `json:"activation_state"`
}

// ChannelsDocument is the data we send back on the wire about multiple channels
type ChannelsDocument struct {
	Channels []*ChannelDocument `json:"channels"`
}

// ActivationConfigurationDocument represents the per-channel activation details
// of an extension (e.g., its activation slot)
type ActivationConfigurationDocument struct {
	Slot   string `json:"slot"`
	Anchor string `json:"anchor"`
}

// InstallationAbilitiesDocument represents the abilities an installation has on a specific
// channel for the requesting credentials
type InstallationAbilitiesDocument struct {
	IsChatEnabled                 bool `json:"is_chat_enabled"`
	IsBitsEnabled                 bool `json:"is_bits_enabled"`
	IsSubscriptionStatusAvailable bool `json:"is_subscription_status_available"`
}

// DynamicManagementDocument represents whether or not the extension's activation state will
// be dynamically managed
type DynamicManagementDocument struct {
	GameID    int  `json:"game_id"`
	IsManaged bool `json:"is_managed"`
}

// FeatureFlagsDocument represents the features a broadcaster can choose to enable/disable
// on a per-installation basis
type FeatureFlagsDocument struct {
	CanSendChat                   bool                        `json:"can_send_chat"`
	CanUseBits                    bool                        `json:"can_use_bits"`
	CanRetrieveSubscriptionStatus bool                        `json:"can_retrieve_subscription_status"`
	DynamicManagement             []DynamicManagementDocument `json:"dynamic_management"`
}

// ExtensionInstallationOAuthResponse represents the response to the call to get the oauth
// token stored on the installation of an extension
type ExtensionInstallationOAuthResponse struct {
	OAuthToken string `json:"oauth_token"`
}

// InstallationStatusDocument is the data we send back on the wire about the nature
// of the installation of a given extension on a channel.
type InstallationStatusDocument struct {
	Abilities         *InstallationAbilitiesDocument `json:"abilities"`
	ActivationState   string                         `json:"activation_state"`
	Anchor            *AnyAnchorParam                `json:"activation_configuration"`
	CanActivate       bool                           `json:"can_activate"`
	ID                string                         `json:"id"`
	PermittedFeatures *FeatureFlagsDocument          `json:"permitted_abilities"`
}

// InstalledExtensionDocument is a pair of extension, installation status structures
// that combines global information about an extension with channel-specific
// information about the nature of its installation.
type InstalledExtensionDocument struct {
	Extension          *ExtensionDocument          `json:"extension"`
	InstallationStatus *InstallationStatusDocument `json:"installation_status_document"`
	RequiredActions    []ActivationRequirement     `json:"required_action"`
}

// CompositeID returns a unique ID for the extension which is a combination of the
// extension ID, the version, and the provided channelID
func (e *InstalledExtensionDocument) CompositeID(channelID string) string {
	return e.Extension.ID + ":" + e.Extension.Version + ":" + channelID
}

// InstalledExtensionsDocument is a list of installed extensions documents.
type InstalledExtensionsDocument struct {
	InstalledExtensions []*InstalledExtensionDocument `json:"installed_extensions"`
}

// TesterJavascriptURLDocument is the data we return when a user requests
// the URL to the front end portion of extensions
// This is temporary until launch, since we need to hide our JS URL in this
// way to prevent people from just flipping on the extension experiment
// and seeing things.
type TesterJavascriptURLDocument struct {
	URL string `json:"url"`
}

// SearchSortFieldsDocument contains the list of fields that the database
// can be searched by / sorted on.
type SearchSortFieldsDocument struct {
	SearchFields []string `json:"search_fields"`
	SortFields   []string `json:"sort_fields"`
}

// UserPermissionsDocument contains the list of public global permissions a
// user has
type UserPermissionsDocument struct {
	Permissions []string `json:"permissions"`
}

// UserExtensionPermissionsDocument contains the list of public permissions a
// user has for a given extension
type UserExtensionPermissionsDocument struct {
	ExtensionID string   `json:"extension_id"`
	Permissions []string `json:"permissions"`
}

const (
	// RequiredActivationActionNone means that the activation request was processed
	RequiredActivationActionNone = "none"
	// RequiredActivationActionPromptForPermissions means that the front end should ask the broadcaster
	// for permissions and begin polling for receipt of said permissions
	RequiredActivationActionPromptForPermissions = "prompt_for_permissions"
)

// ActivationsDocument lets the caller know if any further action is
// required after an multi-activation request is submitted.
type ActivationsDocument struct {
	InstalledExtensions []*InstalledExtensionDocument `json:"installed_extensions"`
}

// ActivationDocument lets the caller know if any further action is
// required after an activation request is submitted.
type ActivationDocument struct {
	InstalledExtensions []*InstalledExtensionDocument `json:"installed_extensions"`
	RequiredAction      string                        `json:"required_action"`
}

// PostActivationConfigDocument is the legacy response document which only represented the
// config of the acted-upon extension and it's required configuration.
type PostActivationConfigDocument struct {
	InstallationStatus *InstallationStatusDocument `json:"installation_status"`
	RequiredAction     string                      `json:"required_action"`
}

type ExtensionAuthorEmailDocument struct {
	AuthorEmail string `json:"author_email"`
}

type HardDeleteUserDocument struct {
	BroadcasterWhitelists []string `json:"broadcaster_whitelists"`
	TesterWhitelists      []string `json:"tester_whitelists"`
	GlobalWhitelists      []string `json:"global_whitelists"`
	Extensions            []string `json:"extensions"`
	OrphanedExtensions    []string `json:"orphaned_extensions"`
	InstalledExtensions   []string `json:"installed_extensions"`
	ErrorMessages         []string `json:"error_messages"`
}

type AuditUserDocument struct {
	BroadcasterWhitelists []string `json:"broadcaster_whitelists"`
	TesterWhitelists      []string `json:"tester_whitelists"`
	GlobalWhitelists      []string `json:"global_whitelists"`
	InstalledExtensions   []string `json:"installed_extensions"`
}

// NewExtensionDocument converts a datastore Extension object
// into a structure suitable for serialization.
func NewExtensionDocument(extension *models.Extension, installationCount int, userID string, canSeeAllExtensions bool) *ExtensionDocument {
	var screenshotURLs, assetURLs []string
	var iconURL string

	uriBase := extension.TestingBaseURI
	if extension.State != models.InTest && extension.State != models.Deleted {
		uriBase = extension.ProductionBaseURI
	}

	if len(extension.ScreenshotURLs) > 0 {
		screenshotURLs = extension.ScreenshotURLs
	} else {
		for _, screenshotPath := range extension.ScreenshotPaths {
			screenshotURL := fmt.Sprintf("%s%s", uriBase, screenshotPath)
			screenshotURLs = append(screenshotURLs, screenshotURL)
		}
	}

	for _, assetPath := range extension.AssetPaths {
		assetURL := fmt.Sprintf("%s%s", uriBase, assetPath)
		assetURLs = append(assetURLs, assetURL)
	}

	if len(extension.IconURL) > 0 {
		iconURL = extension.IconURL
	} else {
		iconURL = fmt.Sprintf("%s%s", uriBase, extension.IconPath)
	}

	iconURLs := make(map[protocol.IconSize]string)
	if len(extension.IconURLs) > 0 {
		for iconSize, iconUrl := range extension.IconURLs {
			iconURLs[protocol.IconSize(iconSize)] = iconUrl
		}
	} else {
		for iconSize, iconPath := range extension.IconPaths {
			iconURLs[protocol.IconSize(iconSize)] = fmt.Sprintf("%s%s", uriBase, iconPath)
		}
	}

	whitelistedPanelURLs := []string{}
	whitelistedConfigURLs := []string{}
	for _, url := range extension.WhitelistedPanelURLs {
		whitelistedPanelURLs = append(whitelistedPanelURLs, url)
	}
	for _, url := range extension.WhitelistedConfigURLs {
		whitelistedConfigURLs = append(whitelistedConfigURLs, url)
	}

	requiredBroadcasterAbilities := []string{}
	for _, ability := range extension.RequiredBroadcasterAbilities {
		requiredBroadcasterAbilities = append(requiredBroadcasterAbilities, ability)
	}

	doc := ExtensionDocument{
		AssetURLs:                    assetURLs,
		AuthorName:                   extension.AuthorName,
		BaseURI:                      uriBase,
		BitsEnabled:                  extension.BitsEnabled,
		BitsSupportLevel:             extension.BitsSupportLevel,
		CanInstall:                   canSeeAllExtensions || models.CanInstall(extension, userID, canSeeAllExtensions),
		Categories:                   make([]string, len(extension.Categories)),
		ConfigurationLocation:        extension.ConfigurationLocation,
		Description:                  extension.Description,
		EULATOSURL:                   extension.EULATOSURL,
		Games:                        make([]int, len(extension.Games)),
		HasChatSupport:               extension.HasChatSupport,
		IconURL:                      iconURL,
		IconURLs:                     iconURLs,
		ID:                           extension.ID,
		InstallationCount:            -42,
		Name:                         extension.Name,
		PrivacyPolicyURL:             extension.PrivacyPolicyURL,
		RequestIdentityLink:          extension.RequestIdentityLink,
		RequiredBroadcasterAbilities: requiredBroadcasterAbilities,
		RequiredConfiguration:        extension.RequiredConfigurationString,
		ScreenshotURLs:               screenshotURLs,
		SKU:                          extension.SKU,
		State:                        models.StateString(extension.State),
		SubscriptionsSupportLevel:    extension.SubscriptionsSupportLevel,
		Summary:                      extension.Summary,
		SupportEmail:                 extension.SupportEmail,
		VendorCode:                   extension.VendorCode,
		Version:                      extension.Version,
		ViewerSummary:                extension.ViewerSummary,
		ViewerURLs:                   ViewerURLs{},
		Views:                        ExtensionViews{},
		WhitelistedConfigURLs:        whitelistedConfigURLs,
		WhitelistedPanelURLs:         whitelistedPanelURLs,
	}

	for i := range extension.Games {
		doc.Games[i] = extension.Games[i]
	}

	for i := range extension.Categories {
		// TODO: Add curated categories
		doc.Categories[i] = extension.Categories[i]
	}

	// non anchors
	if extension.Views.Config.ViewerPath != "" {
		doc.Views.Config = &ConfigExtensionView{
			ViewerPath: extension.Views.Config.ViewerPath,
			ViewerURL:  uriBase + extension.Views.Config.ViewerPath,
		}

		doc.ConfigURL = doc.Views.Config.ViewerURL
	}

	if extension.Views.LiveConfig.ViewerPath != "" {
		doc.Views.LiveConfig = &LiveConfigExtensionView{
			ViewerPath: extension.Views.LiveConfig.ViewerPath,
			ViewerURL:  uriBase + extension.Views.LiveConfig.ViewerPath,
		}

		doc.LiveConfigURL = doc.Views.LiveConfig.ViewerURL
	}

	if extension.Views.Mobile.ViewerPath != "" {
		doc.Views.Mobile = &MobileExtensionView{
			ViewerPath: extension.Views.Mobile.ViewerPath,
			ViewerURL:  uriBase + extension.Views.Mobile.ViewerPath,
		}

		doc.ViewerURLs.Mobile = doc.Views.Mobile.ViewerURL
	}

	// anchors
	if extension.Views.Component.ViewerPath != "" {
		doc.Views.Component = &ComponentExtensionView{
			AspectHeight: extension.Views.Component.AspectHeight,
			AspectWidth:  extension.Views.Component.AspectWidth,
			AspectRatioX: extension.Views.Component.AspectRatioX,
			AspectRatioY: extension.Views.Component.AspectRatioY,
			Autoscale:    extension.Views.Component.Autoscale,
			ScalePixels:  extension.Views.Component.ScalePixels,
			Size:         extension.Views.Component.Size,
			TargetHeight: extension.Views.Component.TargetHeight,
			Zoom:         extension.Views.Component.Zoom,
			ZoomPixels:   extension.Views.Component.ZoomPixels,
			ViewerPath:   extension.Views.Component.ViewerPath,
			ViewerURL:    uriBase + extension.Views.Component.ViewerPath,
		}

		doc.ViewerURLs.Component = doc.Views.Component.ViewerURL
	}

	if extension.Views.Hidden.ViewerPath != "" {
		doc.Views.Hidden = &HiddenExtensionView{
			ViewerPath: extension.Views.Hidden.ViewerPath,
			ViewerURL:  uriBase + extension.Views.Hidden.ViewerPath,
		}
	}

	if extension.Views.Panel.ViewerPath != "" {
		doc.Views.Panel = &PanelExtensionView{
			Height:     extension.Views.Panel.Height,
			ViewerPath: extension.Views.Panel.ViewerPath,
			ViewerURL:  uriBase + extension.Views.Panel.ViewerPath,
		}

		doc.ViewerURLs.Panel = doc.Views.Panel.ViewerURL
		doc.PanelHeight = doc.Views.Panel.Height
		doc.ViewerURL = doc.Views.Panel.ViewerURL
	}

	if extension.Views.VideoOverlay.ViewerPath != "" {
		doc.Views.VideoOverlay = &VideoOverlayExtensionView{
			ViewerPath: extension.Views.VideoOverlay.ViewerPath,
			ViewerURL:  uriBase + extension.Views.VideoOverlay.ViewerPath,
		}

		doc.ViewerURLs.VideoOverlay = doc.Views.VideoOverlay.ViewerURL
	}

	return &doc
}

// NewExtensionDocuments is a convenience function for converting
// many Extensions into their serialized documents
func NewExtensionDocuments(extensions []*models.Extension, totalCount int64, installationCounts map[string]int, userID string, canSeeAllExtensions bool) *ExtensionsDocument {
	ret := ExtensionsDocument{
		Count: totalCount,
	}

	for _, extension := range extensions {
		ret.Extensions = append(ret.Extensions, NewExtensionDocument(extension, installationCounts[extension.ID], userID, canSeeAllExtensions))
	}
	return &ret
}

// NewAdminExtensionsDocument is a convenience function for converting
// many Extensions into the admin document for Curse to use
func NewAdminExtensionsDocument(extensions []*models.Extension, totalCount int64) *AdminExtensionsDocument {
	return &AdminExtensionsDocument{
		Count:      totalCount,
		Extensions: extensions,
	}
}

// NewChannelDocument converts a datastore Channel object
// into a structure suitable for serialization.
func NewChannelDocument(installation *models.InstalledExtension) *ChannelDocument {
	return &ChannelDocument{
		ID:               installation.ChannelID, // redundant for backwards-compatible non-breakage until we land visage upgrade
		ChannelID:        installation.ChannelID,
		ExtensionID:      installation.ExtensionID,
		Version:          installation.Version,
		ActivationState:  installation.ActivationState,
		ActivationConfig: NewActivationConfigurationDocument(&installation.ActivationConfig),
	}
}

// NewChannelDocuments is a convenience function for converting
// many Channels into their serialized documents
func NewChannelDocuments(installations []*models.InstalledExtension) *ChannelsDocument {
	var ret ChannelsDocument

	for _, installation := range installations {
		ret.Channels = append(ret.Channels, NewChannelDocument(installation))
	}
	return &ret
}

// NewActivationConfigurationDocument converts the per-channel activation configuration
// into a format suitable for transmission
func NewActivationConfigurationDocument(config *models.ActivationConfiguration) *ActivationConfigurationDocument {
	return &ActivationConfigurationDocument{
		Slot:   config.Slot,
		Anchor: config.Anchor,
	}
}

// NewSearchSortFieldsDocument constructs a search/sort fields document
func NewSearchSortFieldsDocument(searchFields, sortFields []string) *SearchSortFieldsDocument {
	return &SearchSortFieldsDocument{
		SearchFields: searchFields,
		SortFields:   sortFields,
	}
}

const (
	CanCreateExtension    = "create_extension"
	CanManageExtension    = "manage_extension"
	CanMonetizeExtensions = "monetize_extensions"
)

// NewUserPermissionsDocument constructs a user permissions document
func NewUserPermissionsDocument(permissions []string) *UserPermissionsDocument {
	return &UserPermissionsDocument{
		Permissions: permissions,
	}
}

// NewUserExtensionPermissionsDocument constructs a user permissions document
func NewUserExtensionPermissionsDocument(extensionID string, permissions []string) *UserExtensionPermissionsDocument {
	return &UserExtensionPermissionsDocument{
		ExtensionID: extensionID,
		Permissions: permissions,
	}
}

func (a ActivationRequirement) InSlice(slice []ActivationRequirement) bool {
	for _, e := range slice {
		if a == e {
			return true
		}
	}
	return false
}
func (a ActivationRequirement) RemoveFromSlice(slice []ActivationRequirement) []ActivationRequirement {
	j := 0
	for _, n := range slice {
		if n != a {
			slice[j] = n
			j++
		}
	}
	return slice[:j]
}

func (a ActivationRequirement) Replace(slice []ActivationRequirement, b ActivationRequirement) []ActivationRequirement {
	for i, n := range slice {
		if n == a {
			slice[i] = b
		}
	}
	return slice
}

func RequiredHostedVersion(devVersion, broadcasterVersion string) string {
	return strings.Join([]string{hostedConfigPrefix, devVersion, broadcasterVersion}, hostedConfigSeparator)
}

func ExtractRequiredHostedVersions(requirements string) (string, string) {
	var dev, broadcaster string
	parts := strings.Split(requirements, hostedConfigSeparator)
	if parts[0] == hostedConfigPrefix {
		if len(parts) > 1 {
			dev = parts[1]
		}
		if len(parts) > 2 {
			broadcaster = parts[2]
		}
	}
	return dev, broadcaster
}

func HandleHostedConfig(doc InstalledExtensionDocument, devVersion string, broadcasterVersion string) InstalledExtensionDocument {
	if HostedConfig.InSlice(doc.RequiredActions) {
		dev, broad := ExtractRequiredHostedVersions(doc.Extension.RequiredConfiguration)
		if (dev != "" && dev != devVersion) || (broad != "" && broad != broadcasterVersion) {
			HostedConfig.Replace(doc.RequiredActions, Configure)
		} else {
			//remove HostedConfig
			doc.RequiredActions = HostedConfig.RemoveFromSlice(doc.RequiredActions)
			if len(doc.RequiredActions) == 0 {
				doc.InstallationStatus.ActivationState = models.ActivationStateActive
				doc.InstallationStatus.CanActivate = true
			} else if len(doc.RequiredActions) == 1 && doc.RequiredActions[0] == Anchor {
				doc.InstallationStatus.CanActivate = true
			}
		}
	}
	return doc
}

// NewExtensionViewsFromManifest maps ManifestViews structure to the ExtensionViews
// structure for use in GraphQL resolvers.
func NewExtensionViewsFromManifest(baseURI string, manifestViews *protocol.ManifestViews) *ExtensionViews {
	views := ExtensionViews{}

	if manifestViews.Panel != nil {
		views.Panel = &PanelExtensionView{
			Height:     manifestViews.Panel.Height,
			ViewerPath: manifestViews.Panel.ViewerPath,
			ViewerURL:  baseURI + manifestViews.Panel.ViewerPath,
		}
	}

	if manifestViews.Component != nil {
		views.Component = &ComponentExtensionView{
			CanLinkExternalContent: false,
			AspectHeight:           manifestViews.Component.AspectHeight,
			AspectWidth:            manifestViews.Component.AspectWidth,
			AspectRatioX:           manifestViews.Component.AspectRatioX,
			AspectRatioY:           manifestViews.Component.AspectRatioY,
			Autoscale:              manifestViews.Component.Autoscale,
			ScalePixels:            manifestViews.Component.ScalePixels,
			Size:                   float64(manifestViews.Component.Size) * 10000,
			TargetHeight:           manifestViews.Component.TargetHeight,
			ViewerPath:             manifestViews.Component.ViewerPath,
			ViewerURL:              baseURI + manifestViews.Component.ViewerPath,
			Zoom:                   manifestViews.Component.Zoom,
			ZoomPixels:             manifestViews.Component.ZoomPixels,
		}
	}

	if manifestViews.VideoOverlay != nil {
		views.VideoOverlay = &VideoOverlayExtensionView{
			ViewerPath: manifestViews.VideoOverlay.ViewerPath,
			ViewerURL:  baseURI + manifestViews.VideoOverlay.ViewerPath,
		}
	}

	if manifestViews.Config != nil {
		views.Config = &ConfigExtensionView{
			ViewerPath: manifestViews.Config.ViewerPath,
			ViewerURL:  baseURI + manifestViews.Config.ViewerPath,
		}
	}

	if manifestViews.LiveConfig != nil {
		views.LiveConfig = &LiveConfigExtensionView{
			ViewerPath: manifestViews.LiveConfig.ViewerPath,
			ViewerURL:  baseURI + manifestViews.LiveConfig.ViewerPath,
		}
	}

	if manifestViews.Mobile != nil {
		views.Mobile = &MobileExtensionView{
			ViewerPath: manifestViews.Mobile.ViewerPath,
			ViewerURL:  baseURI + manifestViews.Mobile.ViewerPath,
		}
	}

	if manifestViews.Hidden != nil {
		views.Hidden = &HiddenExtensionView{
			ViewerPath: manifestViews.Hidden.ViewerPath,
			ViewerURL:  baseURI + manifestViews.Hidden.ViewerPath,
		}
	}

	return &views
}
