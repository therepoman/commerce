package ems

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"

	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/gds/gds/extensions/ems/documents"
	"code.justin.tv/gds/gds/extensions/ems/protocol"
	gdserrors "code.justin.tv/gds/gds/golibs/errors"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
)

const (
	defaultStatSampleRate = 0.1
	defaultTimingXactName = "extensions"
	sendExtensionChatCap  = "send_extension_chat_message"
)

//go:generate go run github.com/maxbrunsfeld/counterfeiter -generate

//counterfeiter:generate . Client
// Client is the client library to the gds extension manager service
type Client interface {
	DiscomanClient

	// CanSendExtensionChatMessage checks a provided Twitch-Authorization token for the correct Cartman
	// capability to send a chat message and performs the logic to allow an extension to send a chat message
	// to the target channel
	CanSendExtensionChatMessage(ctx context.Context, twitchAuth, extensionID, version, channelID string, reqOpts *twitchclient.ReqOpts) (bool, *documents.InstalledExtensionDocument, error)

	// GetExtensionSummary returns the protocol.ExtensionSummary for a given Extension ID.
	GetExtensionSummary(ctx context.Context, extensionID string, reqOpts *twitchclient.ReqOpts) (*protocol.ExtensionSummary, error)

	// GetExtensions returns the complete list of extensions available for installation.
	// It supports search, sorting, and pagination.
	GetExtensions(ctx context.Context, params documents.GetExtensionsParams, reqOpts *twitchclient.ReqOpts) (*documents.ExtensionsDocument, error)

	// GetExtensionsByKeys returns a list of extensions by protocol.ExtensionKey in the same order
	// they are requested. Assuming all keys provided are in the valid protocol.ExtensionKey
	// format, either an extension record or nil will be returned in the same order as the
	// keys are provided.
	GetExtensionsByKeys(ctx context.Context, keys []string, reqOpts *twitchclient.ReqOpts) ([]*documents.ExtensionDocument, error)

	// GetUserPermissions returns the public global permissions for this user
	GetUserPermissions(ctx context.Context, reqOpts *twitchclient.ReqOpts) (*documents.UserPermissionsDocument, error)

	// GetUserExtensionPermissions returns the public permissions this user has for this extension
	// extensionID - the extension ID to check permissions of
	GetUserExtensionPermissions(ctx context.Context, extensionID string, reqOpts *twitchclient.ReqOpts) (*documents.UserExtensionPermissionsDocument, error)

	// GetExtensionByID returns information for the given extension
	// extensionID - the client ID for the desired extension
	// version - the specific version of that extension to query
	GetExtensionByID(ctx context.Context, extensionID, version string, reqOpts *twitchclient.ReqOpts) (*documents.ExtensionDocument, error)

	// GetExtensionInstallationOAuth returns the OAuth stored for a given installation
	// channelID - the channel to retrieve the installation for
	// extensionID - the client ID for the desired installation
	GetExtensionInstallationOAuth(ctx context.Context, channelID string, extensionID string, reqOpts *twitchclient.ReqOpts) (*documents.ExtensionInstallationOAuthResponse, error)

	// GetReleasedExtensionByID returns information for the released version of a given extension
	// extensionID - the client ID for the desired extension
	GetReleasedExtensionByID(ctx context.Context, extensionID string, reqOpts *twitchclient.ReqOpts) (*documents.ExtensionDocument, error)

	// GetExtensionsByChannelID returns the list of extensions that are installed on the given channel, along with information
	// about the actual installations
	GetExtensionsByChannelID(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*documents.InstalledExtensionsDocument, error)

	// GetMobileExtensionsByChannelID returns the list of mobile extensions that are installed on the given channel, along with information
	// about the actual installations
	GetMobileExtensionsByChannelID(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*documents.InstalledExtensionsDocument, error)

	// InstallExtension installs the given extension on the given channel
	// extensionID - the client ID for the desired extension
	// version - the specific version of that extension to query
	// channelID - the ID of the channel onto which the extension should be installed
	// deprecated: use InstallExtensionV2 instead
	InstallExtension(ctx context.Context, extensionID, version, channelID string, reqOpts *twitchclient.ReqOpts) (*documents.InstallationStatusDocument, error)

	// InstallExtensionV2 installs the given extension on the given channel
	// extensionID - the client ID for the desired extension
	// version - the specific version of that extension to query
	// channelID - the ID of the channel onto which the extension should be installed
	InstallExtensionV2(ctx context.Context, extensionID, version, channelID string, reqOpts *twitchclient.ReqOpts) (*documents.InstalledExtensionDocument, error)

	// ActivateExtensions applies a set of activation changes to extensions on the given channel. The extensions
	// must already be installed on the channel.
	// channelID - the ID of the channel for which the installed extensions should be activated/deactivated
	// config - structure containing information about the set of extension activation changes to apply
	ActivateExtensions(ctx context.Context, channelID string, config documents.ActivationConfigurationParams, reqOpts *twitchclient.ReqOpts) (*documents.ActivationsDocument, error)

	// SetInstallationConfiguration sets the given extension's installation configuration on the given channel.  In order
	// to activate an extension, this configuration string must match the one (if any) present in the extension manifest.
	// extensionID - the client ID for the desired extension
	// version - the specific version of that extension to query
	// channelID - the ID of the channel for which the installed extension should be activated/deactivated
	// requiredConfigurationString - string that must match the one in the extension manifest in order to activate
	SetInstallationConfiguration(ctx context.Context, extensionID, version, channelID string, requiredConfigurationString string, reqOpts *twitchclient.ReqOpts) error

	// SetFeatureFlags sets the the broadcaster's choices for feature flags on an extension installation.
	// extensionID - the client ID for the desired extension
	// version - the specific version of that extension to set
	// channelID - the ID of the channel for which the installed extension should be configured
	// flags - the subset of feature flags the broadcaster wishes to set
	SetFeatureFlags(ctx context.Context, extensionID, version, channelID string, flags documents.SetFeatureFlagsDocument, reqOpts *twitchclient.ReqOpts) (*documents.InstalledExtensionDocument, error)

	// SetBroadcasterOAuth sets the the broadcaster's oauth for an installed extension.
	// extensionID - the client ID for the desired extension
	// version - the specific version of that extension to set
	// channelID - the ID of the channel for which the installed extension should be configured
	// oauthToken - the oauthToken to store on the installed extension record
	SetBroadcasterOAuth(ctx context.Context, extensionID, version, channelID string, oauthToken string, reqOpts *twitchclient.ReqOpts) (*documents.InstalledExtensionDocument, error)

	// UninstallExtension uninstalls the given extension from the given channel
	// extensionID - the client ID for the desired extension
	// version - the specific version of that extension to query
	// channelID - the ID of the channel from which the extension should be uninstalled
	UninstallExtension(ctx context.Context, extensionID, version, channelID string, reqOpts *twitchclient.ReqOpts) error

	// AddExtension creates a new extension (or updates an existing extension in test)
	// manifest - A structure containing all the information related to the extension to add
	// deprecated: use AddExtensionV2 instead
	AddExtension(ctx context.Context, manifest documents.Manifest, reqOpts *twitchclient.ReqOpts) error

	// AddExtensionV2 creates a new extension (or updates an existing extension in test)
	// manifest - A structure containing all the information related to the extension to add
	// deprecated: use AddExtensionV3 instead
	AddExtensionV2(ctx context.Context, manifest protocol.ExtensionManifest, reqOpts *twitchclient.ReqOpts) (*protocol.ExtensionManifest, error)

	// AddExtensionV3 creates a new extension (or updates an existing extension in test).  It does not set any values
	// on the manifest's DiscoveryData field - that needs to be set using AddExtensionDiscoveryData after the extension/version is created.
	// manifest - A structure containing all the information related to the extension to add
	AddExtensionV3(ctx context.Context, manifest protocol.ExtensionManifest, reqOpts *twitchclient.ReqOpts) (*protocol.ExtensionManifest, error)

	// GetExtensionManifest gets an extension manifest, the developer facing version of an extension
	// extensionID - the client ID for the desired extension
	// version - the specific version of that extension to query
	GetExtensionManifest(ctx context.Context, extensionID string, version string, reqOpts *twitchclient.ReqOpts) (*protocol.ExtensionManifest, error)

	// GetExtensionManifests gets one page extension manifest, the developer facing version of an extension
	// extensionID - the client ID for the desired extension
	// token - optional paging token. If not provided the first page is returned. Token for next page is provided if another page exists
	GetExtensionManifests(ctx context.Context, extensionID string, token *string, reqOpts *twitchclient.ReqOpts) (map[string]*protocol.ExtensionManifest, bool, *string, error)

	// UpdateVersionAssetInfo updates an existing extension's asset info
	UpdateVersionAssetInfo(ctx context.Context, extensionID string, version string, body protocol.UpdateAssetInfoRequest, reqOpts *twitchclient.ReqOpts) (*protocol.ExtensionManifest, error)

	// DeleteExtensionVersion deletes the given extension version.  This is a soft delete; authorized
	// users can undelete the version by transitioning it back to "testing"
	DeleteExtensionVersion(ctx context.Context, extensionID, version string, reqOpts *twitchclient.ReqOpts) error

	// DeleteExtension deletes all versions of the given extension.  This is a soft delete; authorized
	// users can undelete any version of the extension by transitioning it back to "testing"
	DeleteExtension(ctx context.Context, extensionID string, reqOpts *twitchclient.ReqOpts) error

	// TransitionExtensionState moves an extension through the various phases of its lifecycle
	// (e.g., test --> in review, in review --> approved, etc)
	// extensionId - the client ID for the desired extension
	// version - the specific version of that extension that should change state
	// state - the desired target state; must be one of :
	// 	  ("test", "review", "approved", "rejected", "live", "deprecated")
	// deprecated: use TransitionExtensionStateV2 instead
	TransitionExtensionState(ctx context.Context, extensionID, version, state string, reqOpts *twitchclient.ReqOpts) error

	// IsExtension returns nil if the given ID is a known extension, and an error otherwise.
	IsExtension(ctx context.Context, extensionID string, reqOpts *twitchclient.ReqOpts) (bool, error)

	// AddExtensionImageAssets creates a upload response with url and uploadId for image assets
	// extensionId - the client ID for the desired extension
	// version - the specific version of that extension that should upload image
	// params - the number of new images for each type; must includes (if no new images of the type, pass 0) :
	// 	  ("logo", "taskbar", "discovery", "screenshot")
	AddExtensionImageAssets(ctx context.Context, extensionID, version string, params documents.AddExtensionImageAssetsFlags, reqOpts *twitchclient.ReqOpts) (*documents.UploaderResponses, error)

	// DeleteExtensionImageAssets deletes image assets with urls
	// extensionId - the client ID for the desired extension
	// version - the specific version of that extension that should delete image
	// params - the urls of image to be deleted; urls follow this format: https://s3-us-west-2.amazonaws.com/:bucketname/:id/:version/:type:upload_id
	DeleteExtensionImageAssets(ctx context.Context, extensionID, version string, params documents.DeleteExtensionAssetsParams, reqOpts *twitchclient.ReqOpts) (*protocol.ExtensionManifest, error)

	// AddExtensionZipAsset creates a upload response with url and uploadId for a zip asset
	AddExtensionZipAsset(ctx context.Context, extensionID, version, filename string, reqOpts *twitchclient.ReqOpts) (*documents.UploaderResponse, error)

	// UpdateVersionImagePath updates an extensions image path
	UpdateVersionImagePath(ctx context.Context, extensionID string, version string, params protocol.UpdateImagePathRequest, reqOpts *twitchclient.ReqOpts) (*protocol.ExtensionManifest, error)

	// TransitionExtensionStateV2 moves an extension through the various phases of its lifecycle and returns the resulting manifest
	// (e.g., test --> in review, in review --> approved, etc)
	// extensionId - the client ID for the desired extension
	// version - the specific version of that extension that should change state
	// state - the desired target state; must be one of :
	// 	  ("test", "review", "approved", "rejected", "live", "deprecated")
	TransitionExtensionStateV2(ctx context.Context, extensionID, version, state string, reqOpts *twitchclient.ReqOpts) (*protocol.ExtensionManifest, error)

	// CloneVersion Creates a copy of the old version with new version ID. Does not copy Assets.
	CloneVersion(ctx context.Context, extensionID string, version string, newVersion string, reqOpts *twitchclient.ReqOpts) (*protocol.ExtensionManifest, error)

	// AdminSetExtensionDescription updates the description of the given extension version.  Intended to be used by the
	// DXR team to update descriptions on extensions in the InReview state.
	AdminSetExtensionDescription(ctx context.Context, extensionID, version, description string, reqOpts *twitchclient.ReqOpts) error

	// HardDeleteUser is used by the Privacy subsystem to delete all information associated with the given user
	HardDeleteUser(ctx context.Context, userID string, extensionIDs []string, reqOpts *twitchclient.ReqOpts) (*documents.HardDeleteUserDocument, error)

	// AuditUser is used by the Privacy subsystem to gather all information associated with the given user
	AuditUser(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*documents.AuditUserDocument, error)

	// GDPRDebug is a temporary route for sanity testing S2S stuff
	GDPRDebug(ctx context.Context, reqOpts *twitchclient.ReqOpts) error
}

type clientImpl struct {
	twitchclient.Client

	cacheMutex             *sync.Mutex
	extensionApprovers     map[string]bool
	lastApproversUpdate    time.Time
	approversCacheDuration time.Duration
}

// NewClient creates an http client to call the EMS endpoints
func NewClient(conf twitchclient.ClientConf) (Client, error) {
	return internalNewClient(conf, 15*time.Second)
}

// NewS2SClient creates an http client to call the EMS endpoints
// that uses s2s to sign the requests
func NewS2SClient(name string, clientConfig twitchclient.ClientConf, callerConfig caller.Config) (Client, error) {
	rt, err := caller.NewRoundTripper(name, &callerConfig, nil)
	if err != nil {
		return nil, err
	}

	clientConfig.RoundTripperWrappers = append(clientConfig.RoundTripperWrappers,
		func(inner http.RoundTripper) http.RoundTripper {
			rt.SetInnerRoundTripper(inner)
			return rt
		},
	)

	return internalNewClient(clientConfig, 15*time.Second)
}

func internalNewClient(conf twitchclient.ClientConf, approversCacheDuration time.Duration) (Client, error) {
	if conf.TimingXactName == "" {
		conf.TimingXactName = defaultTimingXactName
	}

	twitchClient, err := twitchclient.NewClient(conf)
	return &clientImpl{
		twitchClient,
		&sync.Mutex{},
		map[string]bool{},
		time.Unix(0, 0),
		approversCacheDuration,
	}, err
}

func (c *clientImpl) CanSendExtensionChatMessage(ctx context.Context, twitchAuth, extensionID, version, channelID string, reqOpts *twitchclient.ReqOpts) (bool, *documents.InstalledExtensionDocument, error) {
	parts := strings.Split(twitchAuth, ".")
	if len(parts) != 3 {
		return false, nil, errors.New("Invalid token")
	}

	decoded, err := base64.RawStdEncoding.DecodeString(parts[1])
	if err != nil {
		return false, nil, err
	}

	isAuthenticated := strings.Contains(string(decoded), "\""+sendExtensionChatCap+"\"")
	if !isAuthenticated {
		return false, nil, nil
	}

	installs, err := c.GetExtensionsByChannelID(ctx, channelID, reqOpts)
	if err != nil {
		return false, nil, err
	}

	for _, install := range installs.InstalledExtensions {
		if install.Extension.ID == extensionID && install.Extension.Version == version {
			return install.InstallationStatus.Abilities.IsChatEnabled, install, nil
		}
	}

	return false, nil, nil
}

func (c *clientImpl) GetExtensionSummary(ctx context.Context, extensionID string, reqOpts *twitchclient.ReqOpts) (*protocol.ExtensionSummary, error) {
	url := url.URL{Path: fmt.Sprintf("/developer/extensions/%s/summary", extensionID)}
	req, err := c.NewRequest("GET", url.String(), nil)
	if err != nil {
		return nil, err
	}

	var extensionSummary protocol.ExtensionSummary
	err = c.execute(ctx, req, "get_extension_summary", &extensionSummary, reqOpts)
	if err != nil {
		return nil, err
	}

	return &extensionSummary, nil
}

func (c *clientImpl) GetExtensions(ctx context.Context, params documents.GetExtensionsParams, reqOpts *twitchclient.ReqOpts) (*documents.ExtensionsDocument, error) {
	url := url.URL{Path: "/extensions/search"}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("POST", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var document documents.ExtensionsDocument
	err = c.execute(ctx, req, "get_extensions", &document, reqOpts)
	if err != nil {
		return nil, err
	}
	return &document, nil
}

func (c *clientImpl) GetExtensionsByKeys(ctx context.Context, keys []string, reqOpts *twitchclient.ReqOpts) ([]*documents.ExtensionDocument, error) {
	url := url.URL{
		Path:     "/extensions",
		RawQuery: fmt.Sprintf("keys=%s", strings.Join(keys, ",")),
	}

	req, err := c.NewRequest("GET", url.String(), nil)
	if err != nil {
		return nil, err
	}

	var document []*documents.ExtensionDocument
	err = c.execute(ctx, req, "get_bulk_extensions_by_key", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return document, nil
}

func (c *clientImpl) GetUserPermissions(ctx context.Context, reqOpts *twitchclient.ReqOpts) (*documents.UserPermissionsDocument, error) {
	url := url.URL{Path: "/extensions/permissions"}

	req, err := c.NewRequest("GET", url.String(), nil)
	if err != nil {
		return nil, err
	}

	var document documents.UserPermissionsDocument
	err = c.execute(ctx, req, "get_user_permissions", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) GetUserExtensionPermissions(ctx context.Context, extensionID string, reqOpts *twitchclient.ReqOpts) (*documents.UserExtensionPermissionsDocument, error) {
	url := url.URL{Path: fmt.Sprintf("/extensions/%s/permissions", extensionID)}

	req, err := c.NewRequest("GET", url.String(), nil)
	if err != nil {
		return nil, err
	}

	var document documents.UserExtensionPermissionsDocument
	err = c.execute(ctx, req, "get_user_extension_permissions", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) GetExtensionByID(ctx context.Context, extensionID, version string, reqOpts *twitchclient.ReqOpts) (*documents.ExtensionDocument, error) {
	url := url.URL{Path: fmt.Sprintf("/extensions/%s/%s", extensionID, version)}
	req, err := c.NewRequest("GET", url.String(), nil)
	if err != nil {
		return nil, err
	}

	var document documents.ExtensionDocument

	err = c.execute(ctx, req, "get_extension", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) GetExtensionInstallationOAuth(ctx context.Context, channelID string, extensionID string, reqOpts *twitchclient.ReqOpts) (*documents.ExtensionInstallationOAuthResponse, error) {
	req, err := c.NewRequest("GET", fmt.Sprintf("/channels/%s/extensions/%s/oauth", channelID, extensionID), nil)
	if err != nil {
		return nil, err
	}

	var out documents.ExtensionInstallationOAuthResponse
	err = c.execute(ctx, req, "get_extension_installation_oauth", &out, reqOpts)
	if err != nil {
		return nil, err
	}

	return &out, nil
}

func (c *clientImpl) GetReleasedExtensionByID(ctx context.Context, extensionID string, reqOpts *twitchclient.ReqOpts) (*documents.ExtensionDocument, error) {
	url := url.URL{Path: fmt.Sprintf("/extensions/%s", extensionID)}
	req, err := c.NewRequest("GET", url.String(), nil)
	if err != nil {
		return nil, err
	}

	var document documents.ExtensionDocument
	err = c.execute(ctx, req, "get_released_extension", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) GetExtensionsByChannelID(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*documents.InstalledExtensionsDocument, error) {
	url := url.URL{Path: fmt.Sprintf("/channels/%s/extensions/v2", channelID)}
	req, err := c.NewRequest("GET", url.String(), nil)
	if err != nil {
		return nil, err
	}

	var document documents.InstalledExtensionsDocument
	err = c.execute(ctx, req, "get_extensions_by_channel_id", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) GetMobileExtensionsByChannelID(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*documents.InstalledExtensionsDocument, error) {
	url := url.URL{Path: fmt.Sprintf("/mobile/channels/%s/extensions", channelID)}
	req, err := c.NewRequest("GET", url.String(), nil)
	if err != nil {
		return nil, err
	}

	var document documents.InstalledExtensionsDocument
	err = c.execute(ctx, req, "get_mobile_extensions_by_channel_id", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) DeleteExtensionVersion(ctx context.Context, extensionID, version string, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/extensions/%s/%s", extensionID, version)}

	req, err := c.NewRequest("DELETE", url.String(), nil)
	if err != nil {
		return err
	}

	return c.execute(ctx, req, "delete_extension_version", nil, reqOpts)
}

func (c *clientImpl) DeleteExtension(ctx context.Context, extensionID string, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/extensions/%s", extensionID)}

	req, err := c.NewRequest("DELETE", url.String(), nil)
	if err != nil {
		return err
	}

	return c.execute(ctx, req, "delete_extension", nil, reqOpts)
}

func (c *clientImpl) InstallExtension(ctx context.Context, extensionID, version, channelID string, reqOpts *twitchclient.ReqOpts) (*documents.InstallationStatusDocument, error) {
	url := url.URL{Path: fmt.Sprintf("/channels/%s/extensions/%s/%s", channelID, extensionID, version)}

	req, err := c.NewRequest("POST", url.String(), nil)
	if err != nil {
		return nil, err
	}

	var document documents.InstallationStatusDocument
	err = c.execute(ctx, req, "install_extension", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) InstallExtensionV2(ctx context.Context, extensionID, version, channelID string, reqOpts *twitchclient.ReqOpts) (*documents.InstalledExtensionDocument, error) {
	req, err := c.NewRequest("POST", fmt.Sprintf("/channels/%s/extensions/%s/%s/v2", channelID, extensionID, version), nil)
	if err != nil {
		return nil, err
	}

	var out documents.InstalledExtensionDocument
	err = c.execute(ctx, req, "install_extension_v2", &out, reqOpts)
	if err != nil {
		return nil, err
	}

	return &out, nil
}

func (c *clientImpl) UninstallExtension(ctx context.Context, extensionID, version, channelID string, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/channels/%s/extensions/%s/%s", channelID, extensionID, version)}

	req, err := c.NewRequest("DELETE", url.String(), nil)
	if err != nil {
		return err
	}

	return c.execute(ctx, req, "uninstall_extension", nil, reqOpts)
}

func (c *clientImpl) AddExtension(ctx context.Context, manifest documents.Manifest, reqOpts *twitchclient.ReqOpts) error {
	bodyBytes, err := json.Marshal(manifest)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("POST", "/extensions", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	return c.execute(ctx, req, "add_extension", nil, reqOpts)
}

func (c *clientImpl) AddExtensionV2(ctx context.Context, manifest protocol.ExtensionManifest, reqOpts *twitchclient.ReqOpts) (*protocol.ExtensionManifest, error) {
	bodyBytes, err := json.Marshal(manifest)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("POST", "/developer/extensions", bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var out protocol.ExtensionManifest
	err = c.execute(ctx, req, "add_extension_v2", &out, reqOpts)
	if err != nil {
		return nil, err
	}

	return &out, nil
}

func (c *clientImpl) AddExtensionV3(ctx context.Context, manifest protocol.ExtensionManifest, reqOpts *twitchclient.ReqOpts) (*protocol.ExtensionManifest, error) {
	bodyBytes, err := json.Marshal(manifest)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("POST", "/developer/extensions/v3", bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var out protocol.ExtensionManifest
	err = c.execute(ctx, req, "add_extension_v3", &out, reqOpts)
	if err != nil {
		return nil, err
	}

	return &out, nil
}

func (c *clientImpl) UpdateVersionAssetInfo(ctx context.Context, extensionID string, version string, body protocol.UpdateAssetInfoRequest, reqOpts *twitchclient.ReqOpts) (*protocol.ExtensionManifest, error) {
	url := url.URL{Path: fmt.Sprintf("/developer/extensions/assets/%s/%s", extensionID, version)}

	bodyBytes, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("PUT", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var outManifest protocol.ExtensionManifest
	err = c.execute(ctx, req, "update_asset_info", &outManifest, reqOpts)
	if err != nil {
		return nil, err
	}

	return &outManifest, nil
}

func (c *clientImpl) GetExtensionManifest(ctx context.Context, extensionID string, version string, reqOpts *twitchclient.ReqOpts) (*protocol.ExtensionManifest, error) {
	url := url.URL{Path: fmt.Sprintf("/developer/extensions/%s/%s", extensionID, version)}

	req, err := c.NewRequest("GET", url.String(), nil)
	if err != nil {
		return nil, err
	}

	var manifest protocol.ExtensionManifest
	err = c.execute(ctx, req, "get_extension_manifest", &manifest, reqOpts)
	if err != nil {
		return nil, err
	}

	return &manifest, nil
}

func (c *clientImpl) GetExtensionManifests(ctx context.Context, extensionID string, token *string, reqOpts *twitchclient.ReqOpts) (map[string]*protocol.ExtensionManifest, bool, *string, error) {
	values := url.Values{}
	if token != nil && *token != "" {
		values.Add("token", *token)
	}
	url := url.URL{Path: fmt.Sprintf("/developer/extensions/%s", extensionID), RawQuery: values.Encode()}

	req, err := c.NewRequest("GET", url.String(), nil)
	if err != nil {
		return nil, false, nil, err
	}

	var doc protocol.ExtensionManifestResponse
	err = c.execute(ctx, req, "get_extension_manifests", &doc, reqOpts)
	if err != nil {
		return nil, false, nil, err
	}

	return doc.Manifests, doc.Deleted, doc.PagingToken, nil
}

func (c *clientImpl) TransitionExtensionState(ctx context.Context, extensionID, version, state string, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/extensions/%s/%s", extensionID, version)}

	bodyBytes, err := json.Marshal(documents.StateTransition{
		State: state,
	})
	if err != nil {
		return err
	}

	req, err := c.NewRequest("PUT", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	return c.execute(ctx, req, "transition_extension_state", nil, reqOpts)
}

func (c *clientImpl) TransitionExtensionStateV2(ctx context.Context, extensionID, version, state string, reqOpts *twitchclient.ReqOpts) (*protocol.ExtensionManifest, error) {
	url := url.URL{Path: fmt.Sprintf("/developer/extensions/transition/%s/%s", extensionID, version)}

	bodyBytes, err := json.Marshal(documents.StateTransition{
		State: state,
	})
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("PUT", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var out protocol.ExtensionManifest
	err = c.execute(ctx, req, "transition_extension_state_v2", &out, reqOpts)
	if err != nil {
		return nil, err
	}

	return &out, nil
}

func (c *clientImpl) ActivateExtensions(ctx context.Context, channelID string, config documents.ActivationConfigurationParams, reqOpts *twitchclient.ReqOpts) (*documents.ActivationsDocument, error) {
	url := url.URL{Path: fmt.Sprintf("/channels/%s/extensions/activations", channelID)}

	bodyBytes, err := json.Marshal(config)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("PUT", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var document documents.ActivationsDocument
	err = c.execute(ctx, req, "extension_activations", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, err
}

func (c *clientImpl) SetInstallationConfiguration(ctx context.Context, extensionID, version, channelID string, requiredConfigurationString string, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/channels/%s/extensions/%s/%s/required_configuration", channelID, extensionID, version)}

	bodyBytes, err := json.Marshal(documents.RequiredInstallationConfiguration{
		RequiredConfigurationString: requiredConfigurationString,
	})
	if err != nil {
		return err
	}

	req, err := c.NewRequest("PUT", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	return c.execute(ctx, req, "installation_configuration", nil, reqOpts)
}

func (c *clientImpl) SetFeatureFlags(ctx context.Context, extensionID, version, channelID string, flags documents.SetFeatureFlagsDocument, reqOpts *twitchclient.ReqOpts) (*documents.InstalledExtensionDocument, error) {
	url := url.URL{Path: fmt.Sprintf("/channels/%s/extensions/%s/%s/features", channelID, extensionID, version)}

	bodyBytes, err := json.Marshal(flags)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("PUT", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var document documents.InstalledExtensionDocument
	err = c.execute(ctx, req, "set_feature_flags", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, err
}

func (c *clientImpl) SetBroadcasterOAuth(ctx context.Context, extensionID, version, channelID, oauthToken string, reqOpts *twitchclient.ReqOpts) (*documents.InstalledExtensionDocument, error) {
	bodyBytes, err := json.Marshal(documents.SetBroadcasterOAuthDocument{OAuthToken: oauthToken})
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("PUT", fmt.Sprintf("/channels/%s/extensions/%s/%s/broadcaster_oauth", channelID, extensionID, version), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var out documents.InstalledExtensionDocument
	err = c.execute(ctx, req, "set_broadcaster_oauth", &out, reqOpts)
	if err != nil {
		return nil, err
	}

	return &out, nil
}

func (c *clientImpl) WhitelistUser(ctx context.Context, userID, action string, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/users/%s/whitelist/%s", userID, action)}

	req, err := c.NewRequest("PUT", url.String(), nil)
	if err != nil {
		return err
	}

	return c.execute(ctx, req, "whitelist_user", nil, reqOpts)
}

func (c *clientImpl) DeleteWhitelistUser(ctx context.Context, userID, action string, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/users/%s/whitelist/%s", userID, action)}
	req, err := c.NewRequest("DELETE", url.String(), nil)
	if err != nil {
		return err
	}

	return c.execute(ctx, req, "delete_whitelist_user", nil, reqOpts)
}

func (c *clientImpl) IsExtension(ctx context.Context, extensionID string, reqOpts *twitchclient.ReqOpts) (bool, error) {
	url := url.URL{Path: fmt.Sprintf("/extensions/%s/is_extension", extensionID)}
	req, err := c.NewRequest("GET", url.String(), nil)
	if err != nil {
		return false, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "is_extension",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return false, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusNotFound {
		return false, nil
	}
	if resp.StatusCode == http.StatusNoContent {
		return true, nil
	}

	return false, twitchclient.HandleFailedResponse(resp)
}

func (c *clientImpl) AddExtensionImageAssets(ctx context.Context, extensionID, version string, flags documents.AddExtensionImageAssetsFlags, reqOpts *twitchclient.ReqOpts) (*documents.UploaderResponses, error) {
	url := url.URL{Path: fmt.Sprintf("/extensions/%s/%s/image_assets", extensionID, version)}

	bodyBytes, err := json.Marshal(flags)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("POST", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var out documents.UploaderResponses
	err = c.execute(ctx, req, "add_extension_image_assets", &out, reqOpts)
	if err != nil {
		return nil, err
	}

	return &out, nil
}

func (c *clientImpl) DeleteExtensionImageAssets(ctx context.Context, extensionID, version string, params documents.DeleteExtensionAssetsParams, reqOpts *twitchclient.ReqOpts) (*protocol.ExtensionManifest, error) {
	url := url.URL{Path: fmt.Sprintf("/extensions/%s/%s/image_assets", extensionID, version)}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("DELETE", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var out protocol.ExtensionManifest
	err = c.execute(ctx, req, "delete_extension_image_assets", &out, reqOpts)
	if err != nil {
		return nil, err
	}

	return &out, nil
}

func (c *clientImpl) AddExtensionZipAsset(ctx context.Context, extensionID, version, filename string, reqOpts *twitchclient.ReqOpts) (*documents.UploaderResponse, error) {
	url := url.URL{Path: fmt.Sprintf("/extensions/%s/%s/zip_asset", extensionID, version)}

	bodyBytes, err := json.Marshal(documents.AddExtensionZipAssetRequest{Filename: filename})
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("POST", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var out documents.UploaderResponse
	err = c.execute(ctx, req, "add_extension_zip_asset", &out, reqOpts)
	if err != nil {
		return nil, err
	}

	return &out, nil
}

func (c *clientImpl) UpdateVersionImagePath(ctx context.Context, extensionID string, version string, params protocol.UpdateImagePathRequest, reqOpts *twitchclient.ReqOpts) (*protocol.ExtensionManifest, error) {
	url := url.URL{Path: fmt.Sprintf("/extensions/%s/%s/update_image_path", extensionID, version)}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("POST", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var out protocol.ExtensionManifest
	err = c.execute(ctx, req, "update_version_image_path", &out, reqOpts)
	if err != nil {
		return nil, err
	}

	return &out, nil
}

func (c *clientImpl) CloneVersion(ctx context.Context, extensionID string, oldVersion string, newVersion string, reqOpts *twitchclient.ReqOpts) (*protocol.ExtensionManifest, error) {
	values := url.Values{}
	values.Add("new_version", newVersion)

	url := url.URL{Path: fmt.Sprintf("/developer/extensions/%s/%s/clone", extensionID, oldVersion), RawQuery: values.Encode()}

	req, err := c.NewRequest("POST", url.String(), nil)
	if err != nil {
		return nil, err
	}

	var out protocol.ExtensionManifest
	err = c.execute(ctx, req, "clone_version", &out, reqOpts)
	if err != nil {
		return nil, err
	}

	return &out, nil
}

func (c *clientImpl) AdminSetExtensionDescription(ctx context.Context, extensionID, version, description string, reqOpts *twitchclient.ReqOpts) error {
	url := url.URL{Path: fmt.Sprintf("/admin/extensions/%s/%s/description", extensionID, version)}

	bodyBytes, err := json.Marshal(map[string]string{
		"description": description,
	})
	if err != nil {
		return err
	}

	req, err := c.NewRequest("PUT", url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	return c.execute(ctx, req, "admin_set_extension_description", nil, reqOpts)
}

func (c *clientImpl) HardDeleteUser(ctx context.Context, userID string, extensionIDs []string, reqOpts *twitchclient.ReqOpts) (*documents.HardDeleteUserDocument, error) {
	bodyBytes, err := json.Marshal(documents.HardDeleteUserParam{
		ExtensionIDs: extensionIDs,
	})
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("DELETE", fmt.Sprintf("/privacy/user/%s", userID), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var out documents.HardDeleteUserDocument
	err = c.execute(ctx, req, "delete_user", &out, reqOpts)
	if err != nil {
		return nil, err
	}

	return &out, nil
}

func (c *clientImpl) AuditUser(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*documents.AuditUserDocument, error) {
	req, err := c.NewRequest("GET", fmt.Sprintf("/privacy/user/%s", userID), nil)
	if err != nil {
		return nil, err
	}

	var out documents.AuditUserDocument
	err = c.execute(ctx, req, "audit_user", &out, reqOpts)
	if err != nil {
		return nil, err
	}

	return &out, nil
}

func (c *clientImpl) GDPRDebug(ctx context.Context, reqOpts *twitchclient.ReqOpts) error {
	req, err := c.NewRequest("GET", "/privacy/gdpr/debug", nil)
	if err != nil {
		return err
	}
	return c.execute(ctx, req, "gdpr_debug", nil, reqOpts)
}

func (c *clientImpl) handleErrorResponse(resp *http.Response) error {
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	clientErr, err := gdserrors.UnmarshalErrorForHTTP(body)
	if err != nil {
		return err
	}

	if code := gdserrors.GetHTTPStatusOrDefault(clientErr, 500); code >= 500 {
		return gdserrors.NewBuilder(resp.Status + ": server error").WithHTTPStatus(code).Build()
	}

	return clientErr
}

func (c *clientImpl) execute(ctx context.Context, req *http.Request, statName string, out interface{}, reqOpts *twitchclient.ReqOpts) error {
	opts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.extensions." + statName,
		StatSampleRate: defaultStatSampleRate,
	})

	defer close(req.Body)

	resp, err := c.Do(ctx, req, opts)
	if err != nil {
		return err
	}

	defer close(resp.Body)

	if err, found := gdserrors.ExtractFromHTTPResponse(resp); found {
		return err
	}

	if resp.StatusCode == http.StatusNoContent {
		out = nil
		return nil
	}

	if err = json.NewDecoder(resp.Body).Decode(out); err != nil {
		return fmt.Errorf("Unable to read response body: %s", err)
	}
	return nil
}

func close(c io.Closer) {
	if c != nil {
		_ = c.Close()
	}
}
