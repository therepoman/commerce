package ems

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/gds/gds/extensions/ems/documents"
	"code.justin.tv/gds/gds/extensions/ems/protocol"
)

//counterfeiter:generate . DiscomanClient
// DiscomanClient defines the (currently extended of EMS Client) Client interface
type DiscomanClient interface {
	// AddCategory creates a new category.  The requesting user must be whitelisted to edit categories.
	AddCategory(ctx context.Context, params documents.AddCategoryRequest, reqOpts *twitchclient.ReqOpts) (*documents.CategoryDocument, error)

	// AddExtensionToCategory adds an extension to the given category
	AddExtensionToCategory(ctx context.Context, eid, cid string, params documents.AddExtensionToCategoryRequest, reqOpts *twitchclient.ReqOpts) (*documents.ExtensionCategoryMembershipDocument, error)

	// AddDeveloperCategoryToExtension sets the category on the provided extension version.
	AddDeveloperCategoryToExtension(ctx context.Context, eid, version, cid string, reqOpts *twitchclient.ReqOpts) (*documents.ExtensionCategoryMembershipDocument, error)

	// AddGameToExtension sets the game on the provided extension version.
	AddGameToExtension(ctx context.Context, eid, version string, gid int, reqOpts *twitchclient.ReqOpts) (*documents.ExtensionGameMembershipDocument, error)

	// GetExtensionRecommendations gets extension id lists given a game id
	GetExtensionRecommendations(ctx context.Context, gid int, reqOpts *twitchclient.ReqOpts) (*documents.ExtensionRecommendationsDocument, error)

	// EditCategoryTranslation edits or creates a new localized name and description for a category
	EditCategoryTranslation(ctx context.Context, cid, language string, params documents.EditCategoryTranslationRequest, reqOpts *twitchclient.ReqOpts) (*documents.CategoryDocument, error)

	// GetCategories gets a list of categories.  It supports pagination.
	GetCategories(ctx context.Context, params documents.GetCategoriesRequest, reqOpts *twitchclient.ReqOpts) (*documents.CategoriesDocument, error)

	// GetCategory gets a category given a category id and language
	GetCategory(ctx context.Context, cid, language string, reqOpts *twitchclient.ReqOpts) (*documents.CategoryDocument, error)

	// GetCategoryExtensions gets a paginated list of extensions in the provided category ID
	GetCategoryExtensions(ctx context.Context, cid string, params documents.GetCategoryExtensionsRequest, reqOpts *twitchclient.ReqOpts) (*documents.CategoryExtensionsDocument, error)

	// DeleteCategory deletes a category.  It is an error to delete a developer category that is nonempty.
	DeleteCategory(ctx context.Context, cid string, reqOpts *twitchclient.ReqOpts) (*documents.CategoryDocument, error)

	// DeleteGameToExtension sets the game on the provided extension version.
	DeleteGameFromExtension(ctx context.Context, eid, version string, gid int, reqOpts *twitchclient.ReqOpts) (*documents.ExtensionGameMembershipDocument, error)

	// OrderCategories gives a bulk reordering endpoint for categories
	OrderCategories(ctx context.Context, params documents.OrderCategoriesRequest, reqOpts *twitchclient.ReqOpts) (*documents.CategoryOrderDocument, error)

	// OrderCategory gives a bulk reordering endpoint for the entries in a single category
	OrderCategory(ctx context.Context, cid string, params documents.OrderCategoryRequest, reqOpts *twitchclient.ReqOpts) (*documents.CategoryDocument, error)

	// RemoveExtensionFromCategory removes an extension from the given category
	RemoveExtensionFromCategory(ctx context.Context, eid, cid string, reqOpts *twitchclient.ReqOpts) (*documents.ExtensionCategoryMembershipDocument, error)

	// UpdateCategory updates an existing category.  The requesting user must be whitelisted to edit categories.
	UpdateCategory(ctx context.Context, cid string, params documents.AddCategoryRequest, reqOpts *twitchclient.ReqOpts) (*documents.CategoryDocument, error)

	// CreateFeaturedSchedule adds a new featured carousel schedule.  The requesting user must be whitelisted to edit categories.
	CreateFeaturedSchedule(ctx context.Context, doc documents.AddFeaturedScheduleRequest, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedScheduleDocument, error)

	// GetFeaturedSchedules returns a paginated list of featured carousel schedules.
	GetFeaturedSchedules(ctx context.Context, offset, limit int, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedSchedulesDocument, error)

	// GetFeaturedSchedule returns a single specified featured carousel schedule.
	GetFeaturedSchedule(ctx context.Context, fsid string, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedScheduleDocument, error)

	// DeleteFeaturedSchedule deletes the specified featured carousel schedule.  The requesting user must be whitelisted to edit categories.
	DeleteFeaturedSchedule(ctx context.Context, fsid string, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedScheduleDocument, error)

	// UpdateFeaturedSchedule updates the specified featured carousel schedule.  The requesting user must be whitelisted to edit categories.
	UpdateFeaturedSchedule(ctx context.Context, fsid string, doc documents.AddFeaturedScheduleRequest, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedScheduleDocument, error)

	// CreateFeaturedCarousel adds a new featured carousel schedule.  The requesting user must be whitelisted to edit categories.
	CreateFeaturedCarousel(ctx context.Context, doc documents.AddFeaturedCarouselRequest, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselDocument, error)

	// GetFeaturedCarousels returns a paginated list of featured carousels. The requesting user must be whitelisted to edit categories.
	GetFeaturedCarousels(ctx context.Context, offset, limit int, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselsDocument, error)

	// GetFeaturedCarousel returns a single specified featured carousel schedule.
	GetFeaturedCarousel(ctx context.Context, fcid string, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselDocument, error)

	// DeleteFeaturedCarousel deletes the specified featured carousel.  The requesting user must be whitelisted to edit categories.
	DeleteFeaturedCarousel(ctx context.Context, fcid string, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselDocument, error)

	// UpdateFeaturedCarousel updates the specified featured carousel.  The requesting user must be whitelisted to edit categories.
	UpdateFeaturedCarousel(ctx context.Context, fcid string, doc documents.AddFeaturedCarouselRequest, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselDocument, error)

	// OrderFeaturedCarousel updates the ordering of the entries in the specified featured carousel.  The requesting user must be whitelisted to edit categories.
	OrderFeaturedCarousel(ctx context.Context, fcid string, doc documents.OrderFeaturedCarouselRequest, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselDocument, error)

	// CreateFeaturedCarouselEntry adds a new featured carousel entry.  The requesting user must be whitelisted to edit categories.
	CreateFeaturedCarouselEntry(ctx context.Context, doc documents.AddFeaturedCarouselEntryRequest, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselEntryDocument, error)

	// GetFeaturedCarouselEntries returns a paginated list of featured carousel entries. The requesting user must be whitelisted to edit categories.
	GetFeaturedCarouselEntries(ctx context.Context, offset, limit int, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselEntriesDocument, error)

	// GetFeaturedCarouselEntry returns a single specified featured carousel entry.
	GetFeaturedCarouselEntry(ctx context.Context, fceid string, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselEntryDocument, error)

	// DeleteFeaturedCarouselEntry deletes the specified featured carousel entry.  The requesting user must be whitelisted to edit categories.
	DeleteFeaturedCarouselEntry(ctx context.Context, fceid string, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselEntryDocument, error)

	// UpdateFeaturedCarouselEntry updates the specified featured carousel entry.  The requesting user must be whitelisted to edit categories.
	UpdateFeaturedCarouselEntry(ctx context.Context, fceid string, doc documents.AddFeaturedCarouselEntryRequest, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselEntryDocument, error)

	// AddExtensionDiscoveryData creates or updates discovery data for a given extension version.
	AddExtensionDiscoveryData(ctx context.Context, extensionID string, version string, discoData *protocol.ExtensionManifest, reqOpts *twitchclient.ReqOpts) (*protocol.DiscoveryMetadata, error)

	// GetExtensionDiscoveryData returns the discovery data for a given extension version.
	GetExtensionDiscoveryData(ctx context.Context, extensionID string, version string, reqOpts *twitchclient.ReqOpts) (*protocol.DiscoveryMetadata, error)

	// CloneDiscoveryDataVersion copes the discovery data from a particular extension version to another version.
	CloneDiscoveryDataVersion(ctx context.Context, extensionID string, sourceVersion string, destVersion string, reqOpts *twitchclient.ReqOpts) (*protocol.DiscoveryMetadata, error)
}

func (c *clientImpl) AddCategory(ctx context.Context, params documents.AddCategoryRequest, reqOpts *twitchclient.ReqOpts) (*documents.CategoryDocument, error) {
	u := url.URL{Path: "/categories"}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest(http.MethodPost, u.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var document documents.CategoryDocument
	err = c.execute(ctx, req, "add_category", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) AddExtensionToCategory(ctx context.Context, eid, cid string, params documents.AddExtensionToCategoryRequest, reqOpts *twitchclient.ReqOpts) (*documents.ExtensionCategoryMembershipDocument, error) {
	u := url.URL{Path: fmt.Sprintf("/categories/%s/extensions/%s", cid, eid)}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest(http.MethodPost, u.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var document documents.ExtensionCategoryMembershipDocument
	err = c.execute(ctx, req, "add_extension_to_category", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) AddDeveloperCategoryToExtension(ctx context.Context, eid, version, cid string, reqOpts *twitchclient.ReqOpts) (*documents.ExtensionCategoryMembershipDocument, error) {
	u := url.URL{Path: fmt.Sprintf("/extensions/%s/%s/categories/%s", eid, version, cid)}

	req, err := c.NewRequest(http.MethodPost, u.String(), nil)

	if err != nil {
		return nil, err
	}

	var document documents.ExtensionCategoryMembershipDocument
	err = c.execute(ctx, req, "add_dev_category_to_extension", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) AddGameToExtension(ctx context.Context, eid, version string, gid int, reqOpts *twitchclient.ReqOpts) (*documents.ExtensionGameMembershipDocument, error) {
	u := url.URL{Path: fmt.Sprintf("/extensions/%s/%s/games/%d", eid, version, gid)}

	req, err := c.NewRequest(http.MethodPost, u.String(), nil)

	if err != nil {
		return nil, err
	}

	var document documents.ExtensionGameMembershipDocument
	err = c.execute(ctx, req, "add_game_to_extension", &document, reqOpts)
	if err != nil {
		return nil, err
	}
	return &document, nil
}

func (c *clientImpl) DeleteGameFromExtension(ctx context.Context, eid, version string, gid int, reqOpts *twitchclient.ReqOpts) (*documents.ExtensionGameMembershipDocument, error) {
	u := url.URL{Path: fmt.Sprintf("/extensions/%s/%s/games/%d", eid, version, gid)}

	req, err := c.NewRequest(http.MethodDelete, u.String(), nil)

	if err != nil {
		return nil, err
	}

	var document documents.ExtensionGameMembershipDocument
	err = c.execute(ctx, req, "delete_game_from_extension", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) GetExtensionRecommendations(ctx context.Context, gid int, reqOpts *twitchclient.ReqOpts) (*documents.ExtensionRecommendationsDocument, error) {
	u := url.URL{Path: fmt.Sprintf("/extension_recommendations/%d", gid)}

	req, err := c.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, err
	}

	var doc documents.ExtensionRecommendationsDocument
	err = c.execute(ctx, req, "get_extension_recommendations", &doc, reqOpts)
	if err != nil {
		return nil, err
	}

	return &doc, nil
}

func (c *clientImpl) DeleteCategory(ctx context.Context, cid string, reqOpts *twitchclient.ReqOpts) (*documents.CategoryDocument, error) {
	u := url.URL{Path: fmt.Sprintf("/categories/%s", cid)}

	req, err := c.NewRequest(http.MethodDelete, u.String(), nil)
	if err != nil {
		return nil, err
	}

	var document documents.CategoryDocument
	err = c.execute(ctx, req, "delete_category", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) EditCategoryTranslation(ctx context.Context, cid string, lang string, params documents.EditCategoryTranslationRequest, reqOpts *twitchclient.ReqOpts) (*documents.CategoryDocument, error) {
	u := url.URL{Path: fmt.Sprintf("/categories/%s/translations/%s", cid, lang)}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest(http.MethodPut, u.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var document documents.CategoryDocument
	err = c.execute(ctx, req, "edit_category_translation", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) GetCategories(ctx context.Context, params documents.GetCategoriesRequest, reqOpts *twitchclient.ReqOpts) (*documents.CategoriesDocument, error) {
	u := url.URL{Path: "/categories"}
	v := url.Values{}
	v.Set("limit", fmt.Sprintf("%d", params.Limit))
	v.Set("offset", fmt.Sprintf("%d", params.Offset))
	if params.Type != "" {
		v.Set("type", params.Type)
	}
	if params.IncludeHidden {
		v.Set("include_hidden", "1")
	}
	if params.IncludeDeleted {
		v.Set("include_deleted", "1")
	}
	if params.Language != "" {
		v.Set("lang", params.Language)
	}
	u.RawQuery = v.Encode()

	req, err := c.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, err
	}

	var document documents.CategoriesDocument
	err = c.execute(ctx, req, "get_categories", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) GetCategory(ctx context.Context, cid, language string, reqOpts *twitchclient.ReqOpts) (*documents.CategoryDocument, error) {
	u := url.URL{Path: fmt.Sprintf("/categories/%s", cid)}
	v := url.Values{}
	if language != "" {
		v.Set("lang", language)
	}
	u.RawQuery = v.Encode()

	req, err := c.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, err
	}

	var document documents.CategoryDocument
	err = c.execute(ctx, req, "get_category", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) GetCategoryExtensions(ctx context.Context, cid string, params documents.GetCategoryExtensionsRequest, reqOpts *twitchclient.ReqOpts) (*documents.CategoryExtensionsDocument, error) {
	u := url.URL{Path: fmt.Sprintf("/categories/%s/extensions", cid)}
	values := url.Values{}
	values.Set("limit", fmt.Sprintf("%d", params.Limit))
	values.Set("offset", fmt.Sprintf("%d", params.Offset))
	u.RawQuery = values.Encode()

	req, err := c.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, err
	}

	var document documents.CategoryExtensionsDocument
	err = c.execute(ctx, req, "get_category_extensions", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) OrderCategories(ctx context.Context, params documents.OrderCategoriesRequest, reqOpts *twitchclient.ReqOpts) (*documents.CategoryOrderDocument, error) {
	url := url.URL{Path: "/categories/order"}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest(http.MethodPost, url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var document documents.CategoryOrderDocument
	err = c.execute(ctx, req, "order_categories", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) OrderCategory(ctx context.Context, cid string, params documents.OrderCategoryRequest, reqOpts *twitchclient.ReqOpts) (*documents.CategoryDocument, error) {
	url := url.URL{Path: fmt.Sprintf("/categories/%s/order", cid)}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest(http.MethodPost, url.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var document documents.CategoryDocument
	err = c.execute(ctx, req, "order_category", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) RemoveExtensionFromCategory(ctx context.Context, eid, cid string, reqOpts *twitchclient.ReqOpts) (*documents.ExtensionCategoryMembershipDocument, error) {
	u := url.URL{Path: fmt.Sprintf("/categories/%s/extensions/%s", cid, eid)}

	req, err := c.NewRequest(http.MethodDelete, u.String(), nil)

	if err != nil {
		return nil, err
	}

	var document documents.ExtensionCategoryMembershipDocument
	err = c.execute(ctx, req, "remove_extension_from_category", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) UpdateCategory(ctx context.Context, cid string, params documents.AddCategoryRequest, reqOpts *twitchclient.ReqOpts) (*documents.CategoryDocument, error) {
	u := url.URL{Path: fmt.Sprintf("/categories/%s", cid)}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest(http.MethodPut, u.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var document documents.CategoryDocument
	err = c.execute(ctx, req, "update_category", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) CreateFeaturedSchedule(ctx context.Context, params documents.AddFeaturedScheduleRequest, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedScheduleDocument, error) {
	u := url.URL{Path: "/featured_schedules"}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest(http.MethodPost, u.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var document documents.FeaturedScheduleDocument
	err = c.execute(ctx, req, "create_featured_schedule", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) GetFeaturedSchedules(ctx context.Context, offset, limit int, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedSchedulesDocument, error) {
	u := url.URL{Path: "/featured_schedules"}
	v := url.Values{}
	v.Set("limit", fmt.Sprintf("%d", limit))
	v.Set("offset", fmt.Sprintf("%d", offset))
	u.RawQuery = v.Encode()

	req, err := c.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, err
	}

	var document documents.FeaturedSchedulesDocument
	err = c.execute(ctx, req, "get_featured_schedules", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) GetFeaturedSchedule(ctx context.Context, fsid string, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedScheduleDocument, error) {
	u := url.URL{Path: fmt.Sprintf("/featured_schedules/%s", fsid)}

	req, err := c.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, err
	}

	var document documents.FeaturedScheduleDocument
	err = c.execute(ctx, req, "get_featured_schedule", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) DeleteFeaturedSchedule(ctx context.Context, fsid string, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedScheduleDocument, error) {
	u := url.URL{Path: fmt.Sprintf("/featured_schedules/%s", fsid)}

	req, err := c.NewRequest(http.MethodDelete, u.String(), nil)
	if err != nil {
		return nil, err
	}

	var document documents.FeaturedScheduleDocument
	err = c.execute(ctx, req, "delete_featured_schedule", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) UpdateFeaturedSchedule(ctx context.Context, fsid string, params documents.AddFeaturedScheduleRequest, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedScheduleDocument, error) {
	u := url.URL{Path: fmt.Sprintf("/featured_schedules/%s", fsid)}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest(http.MethodPut, u.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var document documents.FeaturedScheduleDocument
	err = c.execute(ctx, req, "update_featured_schedule", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) CreateFeaturedCarousel(ctx context.Context, params documents.AddFeaturedCarouselRequest, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselDocument, error) {
	u := url.URL{Path: "/featured_carousels"}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest(http.MethodPost, u.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var document documents.FeaturedCarouselDocument
	err = c.execute(ctx, req, "create_featured_carousel", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) GetFeaturedCarousels(ctx context.Context, offset, limit int, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselsDocument, error) {
	u := url.URL{Path: "/featured_carousels"}
	v := url.Values{}
	v.Set("limit", fmt.Sprintf("%d", limit))
	v.Set("offset", fmt.Sprintf("%d", offset))
	u.RawQuery = v.Encode()

	req, err := c.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, err
	}

	var document documents.FeaturedCarouselsDocument
	err = c.execute(ctx, req, "get_featured_carousels", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) GetFeaturedCarousel(ctx context.Context, fcid string, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselDocument, error) {
	u := url.URL{Path: fmt.Sprintf("/featured_carousels/%s", fcid)}

	req, err := c.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, err
	}

	var document documents.FeaturedCarouselDocument
	err = c.execute(ctx, req, "get_featured_carousel", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) DeleteFeaturedCarousel(ctx context.Context, fcid string, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselDocument, error) {
	u := url.URL{Path: fmt.Sprintf("/featured_carousels/%s", fcid)}

	req, err := c.NewRequest(http.MethodDelete, u.String(), nil)
	if err != nil {
		return nil, err
	}

	var document documents.FeaturedCarouselDocument
	err = c.execute(ctx, req, "delete_featured_carousel", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) UpdateFeaturedCarousel(ctx context.Context, fcid string, params documents.AddFeaturedCarouselRequest, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselDocument, error) {
	u := url.URL{Path: fmt.Sprintf("/featured_carousels/%s", fcid)}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest(http.MethodPut, u.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var document documents.FeaturedCarouselDocument
	err = c.execute(ctx, req, "update_featured_carousel", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) OrderFeaturedCarousel(ctx context.Context, fcid string, params documents.OrderFeaturedCarouselRequest, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselDocument, error) {
	u := url.URL{Path: fmt.Sprintf("/featured_carousels/%s/order", fcid)}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest(http.MethodPost, u.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var document documents.FeaturedCarouselDocument
	err = c.execute(ctx, req, "order_featured_carousel", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) CreateFeaturedCarouselEntry(ctx context.Context, params documents.AddFeaturedCarouselEntryRequest, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselEntryDocument, error) {
	u := url.URL{Path: "/featured_carousel_entries"}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest(http.MethodPost, u.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var document documents.FeaturedCarouselEntryDocument
	err = c.execute(ctx, req, "create_featured_carousel_entry", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) GetFeaturedCarouselEntries(ctx context.Context, offset, limit int, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselEntriesDocument, error) {
	u := url.URL{Path: "/featured_carousel_entries"}
	v := url.Values{}
	v.Set("limit", fmt.Sprintf("%d", limit))
	v.Set("offset", fmt.Sprintf("%d", offset))
	u.RawQuery = v.Encode()

	req, err := c.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, err
	}

	var document documents.FeaturedCarouselEntriesDocument
	err = c.execute(ctx, req, "get_featured_carousel_entries", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) GetFeaturedCarouselEntry(ctx context.Context, fceid string, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselEntryDocument, error) {
	u := url.URL{Path: fmt.Sprintf("/featured_carousel_entries/%s", fceid)}

	req, err := c.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, err
	}

	var document documents.FeaturedCarouselEntryDocument
	err = c.execute(ctx, req, "get_featured_carousel_entry", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) DeleteFeaturedCarouselEntry(ctx context.Context, fceid string, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselEntryDocument, error) {
	u := url.URL{Path: fmt.Sprintf("/featured_carousel_entries/%s", fceid)}

	req, err := c.NewRequest(http.MethodDelete, u.String(), nil)
	if err != nil {
		return nil, err
	}

	var document documents.FeaturedCarouselEntryDocument
	err = c.execute(ctx, req, "delete_featured_carousel_entry", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) UpdateFeaturedCarouselEntry(ctx context.Context, fceid string, params documents.AddFeaturedCarouselEntryRequest, reqOpts *twitchclient.ReqOpts) (*documents.FeaturedCarouselEntryDocument, error) {
	u := url.URL{Path: fmt.Sprintf("/featured_carousel_entries/%s", fceid)}

	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest(http.MethodPut, u.String(), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var document documents.FeaturedCarouselEntryDocument
	err = c.execute(ctx, req, "update_featured_carousel_entry", &document, reqOpts)
	if err != nil {
		return nil, err
	}

	return &document, nil
}

func (c *clientImpl) AddExtensionDiscoveryData(ctx context.Context, extensionID string, version string, discoManifest *protocol.ExtensionManifest, reqOpts *twitchclient.ReqOpts) (*protocol.DiscoveryMetadata, error) {
	if discoManifest == nil {
		return nil, protocol.ErrMissingParameter
	}
	bodyBytes, err := json.Marshal(*discoManifest)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("PUT", fmt.Sprintf("/developer/extensions/%s/%s/discovery", extensionID, version), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var out protocol.DiscoveryMetadata
	err = c.execute(ctx, req, "add_discovery_data", &out, reqOpts)
	if err != nil {
		return nil, err
	}

	return &out, nil
}

func (c *clientImpl) setDiscoveryImages(ctx context.Context, extensionID string, version string, discoManifest *protocol.ExtensionManifest, reqOpts *twitchclient.ReqOpts) (*protocol.DiscoveryMetadata, error) {
	if discoManifest == nil {
		return nil, protocol.ErrMissingParameter
	}
	bodyBytes, err := json.Marshal(*discoManifest)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("PUT", fmt.Sprintf("/admin/extensions/%s/%s/discoveryimages", extensionID, version), bytes.NewReader(bodyBytes))
	if err != nil {
		return nil, err
	}

	var out protocol.DiscoveryMetadata
	err = c.execute(ctx, req, "set_discovery_images", &out, reqOpts)
	if err != nil {
		return nil, err
	}

	return &out, nil
}

func (c *clientImpl) GetExtensionDiscoveryData(ctx context.Context, extensionID string, version string, reqOpts *twitchclient.ReqOpts) (*protocol.DiscoveryMetadata, error) {
	url := url.URL{Path: fmt.Sprintf("/developer/extensions/%s/%s/discovery", extensionID, version)}
	req, err := c.NewRequest("GET", url.String(), nil)
	if err != nil {
		return nil, err
	}

	var out protocol.DiscoveryMetadata
	err = c.execute(ctx, req, "get_discovery_data", &out, reqOpts)
	if err != nil {
		return nil, err
	}

	return &out, nil
}

func (c *clientImpl) CloneDiscoveryDataVersion(ctx context.Context, extensionID string, sourceVersion string, destVersion string, reqOpts *twitchclient.ReqOpts) (*protocol.DiscoveryMetadata, error) {
	values := url.Values{}
	values.Add("new_version", destVersion)

	// Conceptually this should have destVersion in the path since that's the resource getting created, but leaving it
	// this way to match the clone version style.
	url := url.URL{Path: fmt.Sprintf("/developer/extensions/%s/%s/discovery/clone", extensionID, sourceVersion), RawQuery: values.Encode()}

	req, err := c.NewRequest("POST", url.String(), nil)
	if err != nil {
		return nil, err
	}

	var out protocol.DiscoveryMetadata
	err = c.execute(ctx, req, "clone_discovery_data", &out, reqOpts)
	if err != nil {
		return nil, err
	}

	return &out, nil
}
