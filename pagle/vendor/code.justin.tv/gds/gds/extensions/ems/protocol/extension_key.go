package protocol

import (
	"encoding/base64"
	"net/http"
	"regexp"
	"strings"

	"code.justin.tv/gds/gds/golibs/errors"
)

var (
	// ErrInvalidExtensionKey is returned when any extension keys in the bulk get endpoint are invalid
	ErrInvalidExtensionKey = errors.NewBuilder("Invalid extension key").WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	// ErrMissingExtensionKeys is returned when there are no extension keys provided to the bulk get endpoint
	ErrMissingExtensionKeys = errors.NewBuilder("No extension keys provided").WithHTTPStatus(http.StatusUnprocessableEntity).Build()

	validExtensionKey = regexp.MustCompile(`[^!@]+(\!$|@\d+(\.\d+)?(\.\d+)?$)`)
)

// ExtensionKey is a string encoded with id/version information about an extension
type ExtensionKey string

// ID parses the id out of an ExtensionKey
func (e ExtensionKey) ID() string {
	raw := string(e)
	index := strings.IndexAny(raw, "@!")
	switch index {
	case 0:
		return ""
	case -1:
		return raw
	default:
		return raw[:index]
	}
}

// Version parses the id out of an ExtensionKey
func (e ExtensionKey) Version() *string {
	vals := strings.Split(string(e), "@")
	if len(vals) < 2 {
		return nil
	}
	return &vals[1]
}

// NewExtensionKey creates a base64 encoded representation of an ExtensionKey
func NewExtensionKey(id string, version *string) string {
	return base64.RawURLEncoding.EncodeToString([]byte(GetKey(id, version)))
}

// ParseExtensionKey parses a base64 encoded ExtensionKey into an ExtensionKey.
// If parsing fails or the parsing result is not a valid ExtensionKey, ErrInvalidExtensionKey
// is returned.
func ParseExtensionKey(encoded string) (ExtensionKey, error) {
	key, err := base64.RawURLEncoding.DecodeString(encoded)
	if err != nil {
		return "", ErrInvalidExtensionKey
	}

	keyStr := string(key)
	if !validExtensionKey.MatchString(keyStr) {
		return "", ErrInvalidExtensionKey
	}

	return ExtensionKey(keyStr), nil
}

// GetKey turns an id/*version combination into the proper key (Released vs Versioned)
func GetKey(id string, version *string) ExtensionKey {
	if version == nil || *version == "" {
		return GetReleaseKey(id)
	}
	return GetVersionedKey(id, *version)
}

// GetReleaseKey appends the released marker (!) to the end of the provided ID
func GetReleaseKey(id string) ExtensionKey {
	return ExtensionKey(id + "!")
}

// GetVersionedKey joins the provided id and version on the version separator (@).
// If the provided version is an empty string, a release key is returned instead
func GetVersionedKey(id, version string) ExtensionKey {
	if version == "" {
		return GetReleaseKey(id)
	}
	return ExtensionKey(id + "@" + version)
}
