package models

import "fmt"

func In(val string, array []string) bool {
	for _, e := range array {
		if e == val {
			return true
		}
	}
	return false
}

// StateString provides a human-readable representation of an
// extension state.
func StateString(state ExtensionState) string {
	humanName, ok := HumanNameByState[state]
	if ok {
		return humanName
	}
	return fmt.Sprintf("Unknown extension state: %v", state)

}

// CanInstall determines whether the given user can install
// an extension (based on the broadcaster whitelist)
func CanInstall(extension *Extension, userID string, canSeeAllExtensions bool) bool {
	if canSeeAllExtensions || len(extension.BroadcasterWhitelist) == 0 {
		return true
	}
	return isUserWhitelisted(extension, userID)
}

// IsExtensionActionable determines whether the given user can perform actions
// such as install and activate for the given extension
func IsExtensionActionable(ext *Extension, userID string) bool {
	if ext.State == Deleted || ext.State == Deprecated || ext.State == Rejected {
		return false
	}
	if ext.State == PendingAction {
		return isUserWhitelisted(ext, userID)
	}
	return true
}

func isUserWhitelisted(ext *Extension, userID string) bool {
	return contains(ext.BroadcasterWhitelist, userID) || contains(ext.TestingAccounts, userID)
}

func contains(arr []string, s string) bool {
	for _, a := range arr {
		if s == a {
			return true
		}
	}
	return false
}
