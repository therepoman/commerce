package extensions

import "regexp"

// TODO : move to ems/model during cleanup

const (
	MaxBroadcasterAccounts          = 100
	MaxComponentExtensions          = 2
	MaxDescriptionLength            = 1024
	MaxInstalledExtensions          = 999 // TODO: Lower this once front end is handling error
	MaxNameLength                   = 40
	MaxPanelExtensions              = 3
	MaxSummaryLength                = 140
	MaxViewerSummaryLength          = 140
	MaxTestingAccounts              = 100
	MaxTestingAccountsWithWhitelist = 2000
	MaxWhitelistedURLs              = 25
	MaxGames                        = 15
	MaxAuthorNameLength             = 40
	// CurseForge sends percentages (1-50 width, 1-100 height) multiplied by 100 for these fields
	//
	// For our purposes, they are only used to get a final aspect ratio, so only the ratio between width and height
	// matters, so the larger values are purely for backwards compatibility.
	MinComponentAspectWidth  = 100
	MaxComponentAspectWidth  = 5000
	MinComponentAspectHeight = 100
	MaxComponentAspectHeight = 10000
	MinComponentTargetHeight = 100
	MaxComponentTargetHeight = 10000
	MinPanelHeight           = 100
	MaxPanelHeight           = 500
	MaxVersionLength         = 40

	// Image Assets
	MaxScreenshots     = 6
	MaxLogos           = 1
	MaxTaskbarIcons    = 1
	MaxDiscoveryImages = 1
)

var (
	ValidVersion = regexp.MustCompile(`^\d+(\.\d+)?(\.\d+)?$`)
)
