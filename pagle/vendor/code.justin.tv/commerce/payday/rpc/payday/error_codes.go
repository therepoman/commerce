package paydayrpc

const (
	// UseBitsOnPoll
	ErrCodeInsufficientBitsBalance = "INSUFFICIENT_BITS_BALANCE"
)

type ClientError struct {
	ErrorCode string `json:"error_code"`
}
