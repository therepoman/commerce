package signatureiface

import "net/http"

// SignatureAPI describes the signature api
type SignatureAPI interface {
	SignRequest(*http.Request) error
	SignRequestWithHashedBody(r *http.Request, hashedBody []byte) error
	GenerateSignature(r *http.Request, hashedBody []byte) (key string, value string, err error)
}
