package stats

// Dimension is a metric dimension
type Dimension struct {
	Name  string
	Value string
}

// IncrementableMetric are used to serialize a simple incrementable statistic to be
// flushed periodically.
type IncrementableMetric struct {
	MetricName string
	Dimensions []*Dimension
}

// IncrementableMetricCount is a count of a metric that has been incremented
type IncrementableMetricCount struct {
	Metric *IncrementableMetric
	Count  int
}
