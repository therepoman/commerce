#!/bin/bash

LOCAL_STACK_PORT="4574"
NAME=""
PAYLOAD=""
EXECUTION_TIME=$(date "+%s")

for i in "$@"
do
case $i in
    -n=*|--name=*)
    NAME="${i#*=}"
    shift
    ;;
    -p=*|--payload=*)
    PAYLOAD="${i#*=}"
    shift
    ;;
    *)
    ;;
esac
done

test "${NAME}" || (echo "name required" && exit 1)
test "${PAYLOAD}" || (echo "payload required" && exit 1)

echo "invoking: ${NAME}"
echo "payload: ${PAYLOAD}"

# Expects to be run from project root
path="${PWD}/builds/${NAME}.zip"
zipfile="fileb://${path}"
logfile="/tmp/local_lambda_execution-${NAME}-${EXECUTION_TIME}.log"

# Create log file
touch ${logfile}

# Create function
aws --endpoint-url="http://localhost:${LOCAL_STACK_PORT}" lambda create-function --function-name=${NAME} --runtime=go1.x --role=r1 --handler=main --zip-file=${zipfile} --environment Variables="{ENVIRONMENT=staging}"

# Invoke function
aws lambda --endpoint-url="http://localhost:${LOCAL_STACK_PORT}" invoke --function-name=${NAME} --payload=${PAYLOAD} ${logfile}

# Clean up logfile
rm ${logfile}

# Clean up function
aws --endpoint-url="http://localhost:${LOCAL_STACK_PORT}" lambda delete-function --function-name=${NAME}