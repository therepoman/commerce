#!/bin/bash -e

manta -v -f build.json

docker build -t docker-registry.internal.justin.tv/commerce-pagle:$GIT_COMMIT .
