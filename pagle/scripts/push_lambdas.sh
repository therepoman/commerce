#!/bin/bash -e

# Flags
s3bucket=$1
version=$2

# User Config
builddir=./builds
lambdadir=./lambdas

push() {
    name=$1
    s3path=$name/$version

    echo "-> [$name] pushing to s3 for version '$version'...."

    aws s3 cp $builddir/$name.zip s3://$s3bucket/$s3path/$name.zip
    aws s3 cp --content-type text/plain $builddir/$name.hash s3://$s3bucket/$s3path/hash
}

pushall() {
    for path in $lambdadir/*; do
        name=$(basename $path)
        echo "-> [$name] building lambda..."

        push $name
    done
}

pushall