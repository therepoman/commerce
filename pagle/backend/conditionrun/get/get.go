package get

import (
	"context"

	"code.justin.tv/commerce/pagle/backend/dynamo/conditionrun"
	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/models"
)

type Getter interface {
	GetConditionRun(ctx context.Context, conditionRunID string) (models.ConditionRun, error)
}

type getterImpl struct {
	ConditionRunDAO conditionrun.DAO `inject:""`
}

func NewGetter() Getter {
	return &getterImpl{}
}

func (c *getterImpl) GetConditionRun(ctx context.Context, conditionRunID string) (models.ConditionRun, error) {
	run, err := c.ConditionRunDAO.GetConditionRun(ctx, conditionRunID)

	if err != nil {
		return models.ConditionRun{}, err
	}

	if run == nil {
		return models.ConditionRun{}, errors.ConditionRunNotFound.New("condition run does not exist")
	}

	return *run, nil
}
