package complete

import (
	"context"
	"errors"
	"testing"
	"time"

	conditiongetter_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/condition/get"
	conditionparticipantcompleter_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/conditionparticipant/complete"
	conditionparticipantgetter_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/conditionparticipant/get"
	conditionrungetter_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/conditionrun/get"

	"code.justin.tv/commerce/pagle/backend/models"
	effectcontroller_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/effect"
	s3client_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/s3"
	sfnclient_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/sfn"
	statter_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/stats"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestConditionController_ProcessConditionRun(t *testing.T) {
	Convey("given a condition controller", t, func() {
		sfn := new(sfnclient_mock.SFNClient)
		statter := new(statter_mock.Statter)

		controller := completerImpl{
			SFNClient: sfn,
			Statter:   statter,
		}

		statter.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything)
		statter.On("Inc", mock.Anything, mock.Anything, mock.Anything)

		mockConditionID := "some-condition"
		mockOwnerID := "some-owner"
		mockConditionRunID := "some-condition-run"
		mockDomain := "test-domain"

		Convey("when executing the step function fails, we should return an error", func() {
			sfn.On("ExecuteConditionRunProcessingStateMachine", mock.Anything).Return(errors.New("ERROR"))

			err := controller.ProcessConditionRun(context.Background(), mockDomain, mockConditionID, mockConditionRunID, mockOwnerID)
			So(err, ShouldNotBeNil)
		})

		Convey("when executing the step function does not fail, we should return nil", func() {
			expectedSFNInput := models.ConditionRunProcessingSFNInput{
				ConditionID:    mockConditionID,
				Domain:         mockDomain,
				OwnerID:        mockOwnerID,
				ConditionRunID: mockConditionRunID,
			}
			sfn.On("ExecuteConditionRunProcessingStateMachine", expectedSFNInput).Return(nil)

			err := controller.ProcessConditionRun(context.Background(), mockDomain, mockConditionID, mockConditionRunID, mockOwnerID)
			So(err, ShouldBeNil)
		})
	})
}

func TestConditionController_FinalizeConditionRun(t *testing.T) {
	Convey("given a condition controller", t, func() {
		conditionRunGetter := new(conditionrungetter_mock.Getter)
		conditionParticipantGetter := new(conditionparticipantgetter_mock.Getter)
		conditionGetter := new(conditiongetter_mock.Getter)
		s3Client := new(s3client_mock.S3Client)
		effectController := new(effectcontroller_mock.EffectController)
		statter := new(statter_mock.Statter)

		controller := completerImpl{
			ConditionRunGetter:         conditionRunGetter,
			ConditionParticipantGetter: conditionParticipantGetter,
			ConditionGetter:            conditionGetter,
			EffectController:           effectController,
			S3Client:                   s3Client,
			Statter:                    statter,
		}

		statter.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything)
		statter.On("Inc", mock.Anything, mock.Anything, mock.Anything)

		mockConditionID := "some-condition"
		mockOwnerID := "some-owner"
		mockConditionRunID := "some-condition-run"
		mockDomain := "test-domain"
		mockSettings := []models.EffectSettings{
			{
				EffectType: models.GiveBits,
				Settings:   &models.EffectTypeSettings{},
			},
		}

		s3Client.On("StoreConditionParticipantEffectOutputs", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("when getting the condition run errors, we should error", func() {
			conditionRunGetter.On("GetConditionRun", mock.Anything, mockConditionRunID).Return(models.ConditionRun{}, errors.New("ERROR"))

			hasParticipantsToProcess, err := controller.FinalizeConditionRun(context.Background(), mockDomain, mockConditionID, mockConditionRunID, mockOwnerID)

			So(err, ShouldNotBeNil)
			So(hasParticipantsToProcess, ShouldBeFalse)
		})

		Convey("when GetConditionRun does not error", func() {
			Convey("when condition run was satisfied, we should pre-process conditionParticipants", func() {
				mockConditionRun := models.ConditionRun{
					ConditionRunEndState: models.ConditionRunEndStateSatisfied,
					EffectSettings:       mockSettings,
					CompletedAt:          time.Now(),
				}

				conditionRunGetter.On("GetConditionRun", mock.Anything, mockConditionRunID).Return(mockConditionRun, nil)

				Convey("when ConditionParticipantController.GetPendingConditionParticipantsByConditionRunID errors", func() {
					conditionParticipantGetter.On("GetPendingConditionParticipantsByConditionRunID", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("ERROR"))

					Convey("we should error", func() {
						hasParticipantsToProcess, err := controller.FinalizeConditionRun(context.Background(), mockDomain, mockConditionID, mockConditionRunID, mockOwnerID)

						So(err, ShouldNotBeNil)
						So(hasParticipantsToProcess, ShouldBeFalse)
					})
				})

				Convey("when ConditionParticipantController.GetPendingConditionParticipantsByConditionRunID returns nil, we should return nil", func() {
					conditionParticipantGetter.On("GetPendingConditionParticipantsByConditionRunID", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

					Convey("we should error", func() {
						hasParticipantsToProcess, err := controller.FinalizeConditionRun(context.Background(), mockDomain, mockConditionID, mockConditionRunID, mockOwnerID)

						So(err, ShouldBeNil)
						So(hasParticipantsToProcess, ShouldBeFalse)
					})
				})

				Convey("when ConditionParticipantController.GetPendingConditionParticipantsByConditionRunID returns pending participants", func() {
					mockInvalidConditionParticipant := models.ConditionParticipant{
						ConditionParticipantID: "some-conditionParticipant-id",
						CreatedAt:              mockConditionRun.CompletedAt.Add(-time.Minute),
						TTLSeconds:             60 * 5, // Expires 4 minutes after condition run completion - adding a substantial buffer so that the test is not flaky
					}

					mockActiveConditionParticipant := models.ConditionParticipant{
						ConditionParticipantID: "some-conditionParticipant-id",
						CreatedAt:              mockConditionRun.CompletedAt.Add(-time.Minute),
						TTLSeconds:             60 * 5, // Expires 4 minutes after condition run completion - adding a substantial buffer so that the test is not flaky
					}

					mockExpiredConditionParticipant := models.ConditionParticipant{
						ConditionParticipantID: "some-other-conditionParticipant-id",
						CreatedAt:              mockConditionRun.CompletedAt.Add(-time.Hour),
						TTLSeconds:             1, // Expires just under an hour before condition run completion
					}

					mockConditionParticipants := []*models.ConditionParticipant{
						&mockActiveConditionParticipant,
						&mockExpiredConditionParticipant,
					}

					conditionParticipantGetter.On("GetPendingConditionParticipantsByConditionRunID", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&mockConditionParticipants, nil)

					Convey("when GetCondition returns an error, we should error", func() {
						conditionGetter.On("GetCondition", mock.Anything, mockDomain, mockConditionID, mockOwnerID).Return(models.Condition{}, errors.New("ERROR"))

						hasParticipantsToProcess, err := controller.FinalizeConditionRun(context.Background(), mockDomain, mockConditionID, mockConditionRunID, mockOwnerID)

						So(err, ShouldNotBeNil)
						So(hasParticipantsToProcess, ShouldBeFalse)
					})

					Convey("when GetCondition returns a condition", func() {
						mockCondition := models.Condition{}
						conditionGetter.On("GetCondition", mock.Anything, mockDomain, mockConditionID, mockOwnerID).Return(mockCondition, nil)

						Convey("when EffectController.ValidateAndFilterConditionParticipants errors", func() {
							effectController.On("ValidateAndFilterConditionParticipants", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil, errors.New("ERROR"))

							Convey("we should error", func() {
								hasParticipantsToProcess, err := controller.FinalizeConditionRun(context.Background(), mockDomain, mockConditionID, mockConditionRunID, mockOwnerID)

								So(err, ShouldNotBeNil)
								So(hasParticipantsToProcess, ShouldBeFalse)
							})
						})

						Convey("when EffectController.ValidateAndFilterConditionParticipants does not error", func() {
							mockValidConditionParticipants := []models.ConditionParticipant{mockActiveConditionParticipant}
							mockInvalidConditionParticipants := []models.ConditionParticipant{mockInvalidConditionParticipant}
							effectController.On("ValidateAndFilterConditionParticipants", mock.Anything, mock.Anything, mock.Anything).Return(mockValidConditionParticipants, mockInvalidConditionParticipants, nil)

							Convey("when PreProcessConditionParticipants errors, we should return an error", func() {
								effectController.On("PreProcessConditionParticipants", mock.Anything, mockValidConditionParticipants, mockSettings, mockCondition).Return(nil, errors.New("ERROR"))

								hasParticipantsToProcess, err := controller.FinalizeConditionRun(context.Background(), mockDomain, mockConditionID, mockConditionRunID, mockOwnerID)

								So(err, ShouldNotBeNil)
								So(hasParticipantsToProcess, ShouldBeFalse)
							})

							Convey("when PreProcessConditionParticipants does not error", func() {
								mockEffectOuput := models.EffectOutput{}

								groupedProcessedConditionParticipants := []models.ConditionRunArtifactsByEffect{
									{
										ConditionParticipants: []models.PreProcessedConditionParticipant{
											{
												ConditionParticipant: mockActiveConditionParticipant,
												EffectOutput:         mockEffectOuput,
											},
										},
									},
								}

								effectController.On("PreProcessConditionParticipants", mock.Anything, mockValidConditionParticipants, mockSettings, mockCondition).Return(groupedProcessedConditionParticipants, nil)

								Convey("when the s3 client errors, we should return an error", func() {
									s3Client.On("StoreFinalizedConditionRun", mock.Anything, mockConditionID, mockConditionRunID, mock.Anything).Return(errors.New("ERROR"))

									hasParticipantsToProcess, err := controller.FinalizeConditionRun(context.Background(), mockDomain, mockConditionID, mockConditionRunID, mockOwnerID)

									So(err, ShouldNotBeNil)
									So(hasParticipantsToProcess, ShouldBeFalse)
								})

								Convey("when the s3 client does not error, we should return nil", func() {
									expectedFinalizedRun := models.FinalizedConditionRunS3Input{
										ConditionParticipants: []models.FinalizedConditionParticipantS3Input{
											{
												ConditionParticipantID:       mockExpiredConditionParticipant.ConditionParticipantID,
												ConditionParticipantEndState: models.ConditionParticipantEndStateExpired,
											},
											{
												ConditionParticipantID:       mockInvalidConditionParticipant.ConditionParticipantID,
												ConditionParticipantEndState: models.ConditionParticipantEndStateFailedValidation,
											},
											{
												ConditionParticipantID:           mockActiveConditionParticipant.ConditionParticipantID,
												ConditionParticipantEndState:     models.ConditionParticipantEndStateSatisfied,
												ConditionParticipantEffectOutput: &mockEffectOuput,
											},
										},
										EffectRollups: []models.FinalizedEffectRollupS3Input{
											{},
										},
									}

									s3Client.On("StoreFinalizedConditionRun", mock.Anything, mockConditionID, mockConditionRunID, expectedFinalizedRun).Return(nil)

									hasParticipantsToProcess, err := controller.FinalizeConditionRun(context.Background(), mockDomain, mockConditionID, mockConditionRunID, mockOwnerID)

									So(err, ShouldBeNil)
									So(hasParticipantsToProcess, ShouldBeTrue)
									s3Client.AssertNumberOfCalls(t, "StoreFinalizedConditionRun", 1)
								})
							})
						})
					})
				})
			})
		})
	})
}

func TestConditionController_EnqueueConditionParticipants(t *testing.T) {
	Convey("given a condition controller", t, func() {
		conditionParticipantCompleter := new(conditionparticipantcompleter_mock.Completer)
		s3Client := new(s3client_mock.S3Client)
		statter := new(statter_mock.Statter)

		controller := completerImpl{
			ConditionParticipantCompleter: conditionParticipantCompleter,
			S3Client:                      s3Client,
			Statter:                       statter,
		}

		statter.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything)
		statter.On("Inc", mock.Anything, mock.Anything, mock.Anything)

		mockConditionID := "some-condition"
		mockOwnerID := "some-owner"
		mockConditionRunID := "some-condition-run"
		mockDomain := "test-domain"

		Convey("when s3.GetFinalizedConditionRun returns an error, we should return an error", func() {
			s3Client.On("GetFinalizedConditionRun", mock.Anything, mockConditionID, mockConditionRunID).Return(models.FinalizedConditionRunS3Input{}, errors.New("ERROR"))

			err := controller.EnqueueConditionParticipants(context.Background(), mockDomain, mockConditionID, mockConditionRunID, mockOwnerID)
			So(err, ShouldNotBeNil)
		})

		Convey("when s3.GetFinalizedConditionRun returns an empty slice, we should return nil", func() {
			s3Client.On("GetFinalizedConditionRun", mock.Anything, mockConditionID, mockConditionRunID).Return(models.FinalizedConditionRunS3Input{}, nil)

			err := controller.EnqueueConditionParticipants(context.Background(), mockDomain, mockConditionID, mockConditionRunID, mockOwnerID)
			So(err, ShouldBeNil)
		})

		Convey("when s3.GetFinalizedConditionRun returns a list of participants to enqueue", func() {
			mockEffectOutput := models.EffectOutput{}
			mockFinalizedConditionParticipant := models.FinalizedConditionParticipantS3Input{
				ConditionParticipantID:           "some-participant",
				ConditionParticipantEndState:     models.ConditionParticipantEndStateSatisfied,
				ConditionParticipantEffectOutput: &mockEffectOutput,
			}
			mockFinalizedConditionRun := models.FinalizedConditionRunS3Input{
				ConditionParticipants: []models.FinalizedConditionParticipantS3Input{
					mockFinalizedConditionParticipant,
				},
				EffectRollups: []models.FinalizedEffectRollupS3Input{
					{},
				},
			}
			s3Client.On("GetFinalizedConditionRun", mock.Anything, mockConditionID, mockConditionRunID).Return(mockFinalizedConditionRun, nil)

			Convey("when a participant fails to enqueue, we should error", func() {
				conditionParticipantCompleter.On(
					"EnqueueForProcessing",
					mock.Anything,
					mockConditionRunID,
					mockFinalizedConditionParticipant.ConditionParticipantID,
					mockFinalizedConditionParticipant.ConditionParticipantEndState,
					mockFinalizedConditionParticipant.ConditionParticipantEffectOutput,
					true).Return(errors.New("ERROR")).Once()

				err := controller.EnqueueConditionParticipants(context.Background(), mockDomain, mockConditionID, mockConditionRunID, mockOwnerID)
				So(err, ShouldNotBeNil)
			})

			Convey("when all participants are enqueued, we should return nil", func() {
				conditionParticipantCompleter.On(
					"EnqueueForProcessing",
					mock.Anything,
					mockConditionRunID,
					mockFinalizedConditionParticipant.ConditionParticipantID,
					mockFinalizedConditionParticipant.ConditionParticipantEndState,
					mockFinalizedConditionParticipant.ConditionParticipantEffectOutput,
					true).Return(nil).Once()

				err := controller.EnqueueConditionParticipants(context.Background(), mockDomain, mockConditionID, mockConditionRunID, mockOwnerID)
				So(err, ShouldBeNil)
			})
		})
	})
}

func TestConditionRunCompleter_ProcessConditionRunEffectRollups(t *testing.T) {
	Convey("given a condition run completer", t, func() {
		effectController := new(effectcontroller_mock.EffectController)
		s3Client := new(s3client_mock.S3Client)
		statter := new(statter_mock.Statter)
		conditionGetter := new(conditiongetter_mock.Getter)
		conditionRunGetter := new(conditionrungetter_mock.Getter)

		controller := completerImpl{
			EffectController:   effectController,
			S3Client:           s3Client,
			Statter:            statter,
			ConditionRunGetter: conditionRunGetter,
			ConditionGetter:    conditionGetter,
		}

		statter.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything)
		statter.On("Inc", mock.Anything, mock.Anything, mock.Anything)

		mockConditionID := "some-condition"
		mockOwnerID := "some-owner"
		mockConditionRunID := "some-condition-run"
		mockDomain := "test-domain"

		Convey("when s3.GetFinalizedConditionRun returns an error, we should return an error", func() {
			s3Client.On("GetFinalizedConditionRun", mock.Anything, mockConditionID, mockConditionRunID).Return(models.FinalizedConditionRunS3Input{}, errors.New("ERROR"))

			err := controller.ProcessConditionRunEffectRollups(context.Background(), mockDomain, mockConditionID, mockConditionRunID, mockOwnerID)
			So(err, ShouldNotBeNil)
		})

		Convey("when s3.GetFinalizedConditionRun returns an empty struct, we should return nil", func() {
			s3Client.On("GetFinalizedConditionRun", mock.Anything, mockConditionID, mockConditionRunID).Return(models.FinalizedConditionRunS3Input{}, nil)

			err := controller.ProcessConditionRunEffectRollups(context.Background(), mockDomain, mockConditionID, mockConditionRunID, mockOwnerID)
			So(err, ShouldBeNil)
		})

		Convey("when s3.GetFinalizedConditionRun returns no effect rollups to process, we should return nil", func() {
			mockEffectOutput := models.EffectOutput{}
			mockFinalizedConditionParticipant := models.FinalizedConditionParticipantS3Input{
				ConditionParticipantID:           "some-participant",
				ConditionParticipantEndState:     models.ConditionParticipantEndStateSatisfied,
				ConditionParticipantEffectOutput: &mockEffectOutput,
			}
			mockFinalizedConditionRun := models.FinalizedConditionRunS3Input{
				ConditionParticipants: []models.FinalizedConditionParticipantS3Input{
					mockFinalizedConditionParticipant,
				},
				EffectRollups: []models.FinalizedEffectRollupS3Input{},
			}
			s3Client.On("GetFinalizedConditionRun", mock.Anything, mockConditionID, mockConditionRunID).Return(mockFinalizedConditionRun, nil)

			err := controller.ProcessConditionRunEffectRollups(context.Background(), mockDomain, mockConditionID, mockConditionRunID, mockOwnerID)
			So(err, ShouldBeNil)
		})

		Convey("when s3.GetFinalizedConditionRun returns a list of effect rollups to process", func() {
			mockEffectOutput := models.EffectOutput{}
			mockFinalizedConditionParticipant := models.FinalizedConditionParticipantS3Input{
				ConditionParticipantID:           "some-participant",
				ConditionParticipantEndState:     models.ConditionParticipantEndStateSatisfied,
				ConditionParticipantEffectOutput: &mockEffectOutput,
			}
			mockEffectRollups := []models.FinalizedEffectRollupS3Input{
				{
					EffectType: models.GiveBits,
					EffectRollup: &models.EffectRollupOutput{
						GiveBits: &models.EffectRollupOutputGiveBits{
							Recipients: []models.BitsPoolRecipientFixedShare{
								{
									Amount:   10,
									ToUserID: "foobar-channel",
								},
							},
						},
					},
				},
			}
			mockFinalizedConditionRun := models.FinalizedConditionRunS3Input{
				ConditionParticipants: []models.FinalizedConditionParticipantS3Input{
					mockFinalizedConditionParticipant,
				},
				EffectRollups: mockEffectRollups,
			}
			s3Client.On("GetFinalizedConditionRun", mock.Anything, mockConditionID, mockConditionRunID).Return(mockFinalizedConditionRun, nil)

			Convey("when ConditionGetter.GetCondition returns an error, we should return an error", func() {
				conditionGetter.On("GetCondition", mock.Anything, mockDomain, mockConditionID, mockOwnerID).Return(models.Condition{}, errors.New("ERROR"))

				err := controller.ProcessConditionRunEffectRollups(context.Background(), mockDomain, mockConditionID, mockConditionRunID, mockOwnerID)
				So(err, ShouldNotBeNil)
			})

			Convey("when GetCondition does not error", func() {
				conditionGetter.On("GetCondition", mock.Anything, mockDomain, mockConditionID, mockOwnerID).Return(models.Condition{}, nil)

				Convey("when ConditionRunGetter.GetConditionRun returns an error, we should return an error", func() {
					conditionRunGetter.On("GetConditionRun", mock.Anything, mockConditionRunID).Return(models.ConditionRun{}, errors.New("ERROR"))

					err := controller.ProcessConditionRunEffectRollups(context.Background(), mockDomain, mockConditionID, mockConditionRunID, mockOwnerID)
					So(err, ShouldNotBeNil)
				})

				Convey("when GetConditionRun does not error", func() {
					conditionRunGetter.On("GetConditionRun", mock.Anything, mockConditionRunID).Return(models.ConditionRun{}, nil)

					Convey("when effect rollups fail to process, we should error", func() {
						effectController.On(
							"ProcessEffectRollups",
							mock.Anything,
							mock.Anything,
							mock.Anything,
							mock.Anything).Return(errors.New("ERROR")).Once()

						err := controller.ProcessConditionRunEffectRollups(context.Background(), mockDomain, mockConditionID, mockConditionRunID, mockOwnerID)
						So(err, ShouldNotBeNil)
					})

					Convey("when all participants are enqueued, we should return nil", func() {
						effectController.On(
							"ProcessEffectRollups",
							mock.Anything,
							mock.Anything,
							mock.Anything,
							mockEffectRollups).Return(nil).Once()

						err := controller.ProcessConditionRunEffectRollups(context.Background(), mockDomain, mockConditionID, mockConditionRunID, mockOwnerID)
						So(err, ShouldBeNil)
					})
				})
			})
		})
	})
}
