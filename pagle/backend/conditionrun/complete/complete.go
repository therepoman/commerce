package complete

import (
	"context"
	"sync"
	"time"

	"code.justin.tv/commerce/gogogadget/math"
	"code.justin.tv/commerce/logrus"
	conditioncreator "code.justin.tv/commerce/pagle/backend/condition/create"
	conditiongetter "code.justin.tv/commerce/pagle/backend/condition/get"
	conditionparticipantcompleter "code.justin.tv/commerce/pagle/backend/conditionparticipant/complete"
	conditionparticipantgetter "code.justin.tv/commerce/pagle/backend/conditionparticipant/get"
	conditionrungetter "code.justin.tv/commerce/pagle/backend/conditionrun/get"
	"code.justin.tv/commerce/pagle/backend/dynamo/conditionrun"
	"code.justin.tv/commerce/pagle/backend/effect"
	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/backend/s3"
	"code.justin.tv/commerce/pagle/backend/sfn"
	"code.justin.tv/commerce/pagle/backend/sns"
	"code.justin.tv/commerce/pagle/backend/stats"
	conditionparticipantutils "code.justin.tv/commerce/pagle/backend/utils/conditionparticipant"
)

const (
	conditionRunProcessingLatencyStatName          = "condition_run_processing_latency"
	conditionRunProcessingErrorStatName            = "condition_run_processing_error"
	enqueueConditionParticipantsLatencyStatName    = "enqueue_condition_participants_latency"
	processConditionRunEffectRollupLatencyStatName = "process_condition_run_effect_rollup_latency"
	processConditionRunEffectRollupErrorStatName   = "process_condition_run_effect_rollup_error"
	enqueueConditionParticipantsErrorStatName      = "enqueue_condition_participants_error"
	finalizeConditionRunLatencyStatName            = "finalize_condition_run_latency"
	finalizeConditionRunErrorStatName              = "finalize_condition_run_error"

	// Runs on lambda where we have modest file descriptor limits
	maxEnqueueConditionParticipantConcurrency = 100
)

type EnqueueConditionParticipantJob struct {
	ConditionParticipantID       string
	ConditionRunID               string
	ConditionParticipantEndState models.ConditionParticipantEndState
	EffectOutput                 *models.EffectOutput
}

type Completer interface {
	// Marks a condition run as complete and enqueues for processing
	CompleteConditionRun(ctx context.Context, domain, ownerID, conditionID, oldConditionRunID string, conditionRunEndState models.ConditionRunEndState, shouldCreateNewRun bool, effectSettings []models.EffectSettings) error

	/**
	This suite of APIs is invoked when a condition is satisfied, canceled, or expires. The respective Twirp APIs for those actions publish an
	SNS message (condition-run-processing), which gets picked up by an SQS queue (condition-run-processing). The handler for
	this queue is a Lambda (condition-run-processing).
	*/

	// [Called by condition-run-processing Lambda] First step in condition run processing - creates a step function execution
	// that begins with a Lambda task (finalize-condition-run)
	ProcessConditionRun(ctx context.Context, domain, conditionID, conditionRunID, ownerID string) error
	// [Called by finalize-condition-run Lambda] Second step in condition run processing - queries Dynamo for all pending
	// condition participants in the condition run. Calculates effect outputs if the condition was satisfied. Saves all data necessary
	// to enqueue condition participants for processing to S3.
	FinalizeConditionRun(ctx context.Context, domain, conditionID, conditionRunID, ownerID string) (hasParticipantsToProcess bool, err error)
	// [Called by enqueue-condition-participants Lambda] Final step in condition run processing - pulls down S3 file from previous step,
	// looping over condition participants, sending an SNS message (condition-participant-processing) for each. Message gets picked
	// up by an SQS queue (condition-participant-processing).
	EnqueueConditionParticipants(ctx context.Context, domain, conditionID, conditionRunID, ownerID string) error
	// [Called by process-effect-rollup Lambda] Processing step in condition run processing. Gives tenants the ability to
	// do arbitrary processing on the "rollups" accumulated in the FinalizeConditionRun step. e.g. for the GiveBits tenant
	// we use this step to write broadcaster payout to Gringotts (it is only at this point that we know the totality of
	// Bits to be paid out)
	ProcessConditionRunEffectRollups(ctx context.Context, domain, conditionID, conditionRunID, ownerID string) error
}

type completerImpl struct {
	ConditionRunDAO               conditionrun.DAO                        `inject:""`
	S3Client                      s3.S3Client                             `inject:""`
	Statter                       stats.Statter                           `inject:""`
	ConditionRunGetter            conditionrungetter.Getter               `inject:"conditionrungetter"`
	ConditionParticipantGetter    conditionparticipantgetter.Getter       `inject:"conditionparticipantgetter"`
	ConditionParticipantCompleter conditionparticipantcompleter.Completer `inject:"conditionparticipantcompleter"`
	ConditionGetter               conditiongetter.Getter                  `inject:"conditiongetter"`
	ConditionCreator              conditioncreator.Creator                `inject:"conditioncreator"`
	EffectController              effect.EffectController                 `inject:"effectcontroller"`
	SFNClient                     sfn.SFNClient                           `inject:""`
	SNSClient                     sns.SNSClient                           `inject:""`
}

func NewCompleter() Completer {
	return &completerImpl{}
}

func (c *completerImpl) CompleteConditionRun(ctx context.Context, domain, ownerID, conditionID, oldConditionRunID string, conditionRunEndState models.ConditionRunEndState, shouldCreateNewRun bool, effectSettings []models.EffectSettings) error {
	// Associating new condition run first to ensure that an active run exists at all times (else we could run
	// into situations where a conditionParticipant is created in between completing the old one and creating the new one).
	if shouldCreateNewRun {
		_, _, err := c.ConditionCreator.CreateAndAssociateNewConditionRun(ctx, domain, ownerID, conditionID)

		if err != nil {
			return err
		}
	}

	err := c.ConditionRunDAO.CompleteConditionRun(ctx, oldConditionRunID, conditionRunEndState, effectSettings)

	if err != nil {
		return errors.InternalError.New("failed to complete the condition run")
	}

	err = c.SNSClient.PublishConditionRunProcessing(ctx, models.ConditionRunProcessingSNSMessage{
		ConditionID:    conditionID,
		OwnerID:        ownerID,
		ConditionRunID: oldConditionRunID,
		Domain:         domain,
	})

	if err != nil {
		return errors.InternalError.New("failed to enqueue the condition run for completion")
	}

	return nil
}

func (c *completerImpl) EnqueueConditionParticipants(ctx context.Context, domain, conditionID, conditionRunID, ownerID string) error {
	startTime := time.Now()
	defer func() {
		c.Statter.TimingDuration(enqueueConditionParticipantsLatencyStatName, time.Since(startTime), 1)
	}()

	logFields := logrus.Fields{
		"domain":           domain,
		"condition_id":     conditionID,
		"condition_run_id": conditionRunID,
		"owner_id":         ownerID,
	}

	finalizedConditionParticipants, err := c.S3Client.GetFinalizedConditionRun(ctx, conditionID, conditionRunID)

	if err != nil {
		c.logAndStatEnqueueConditionParticipantsError(logFields, err)
		return err
	}

	itemsToEnqueueCount := len(finalizedConditionParticipants.ConditionParticipants)

	if itemsToEnqueueCount == 0 {
		return nil
	}

	jobsChan := make(chan EnqueueConditionParticipantJob, itemsToEnqueueCount)
	doneChan := make(chan bool, 1)
	errChan := make(chan error, 1)

	c.createEnqueueConditionParticipantWorkerPool(ctx, itemsToEnqueueCount, jobsChan, doneChan, errChan)

	for _, finalizedConditionParticipant := range finalizedConditionParticipants.ConditionParticipants {
		jobsChan <- EnqueueConditionParticipantJob{
			ConditionParticipantID:       finalizedConditionParticipant.ConditionParticipantID,
			ConditionParticipantEndState: finalizedConditionParticipant.ConditionParticipantEndState,
			EffectOutput:                 finalizedConditionParticipant.ConditionParticipantEffectOutput,
			ConditionRunID:               conditionRunID,
		}
	}

	select {
	case <-doneChan:
	case err := <-errChan:
		if err != nil {
			c.logAndStatEnqueueConditionParticipantsError(logFields, err)
			return err
		}
	}

	return nil
}

func (c *completerImpl) FinalizeConditionRun(ctx context.Context, domain, conditionID, conditionRunID, ownerID string) (hasParticipantsToProcess bool, err error) {
	startTime := time.Now()
	defer func() {
		c.Statter.TimingDuration(finalizeConditionRunLatencyStatName, time.Since(startTime), 1)
	}()

	var finalizedConditionParticipants []models.FinalizedConditionParticipantS3Input
	var effectRollups []models.FinalizedEffectRollupS3Input

	logFields := logrus.Fields{
		"domain":           domain,
		"condition_id":     conditionID,
		"condition_run_id": conditionRunID,
		"owner_id":         ownerID,
	}

	conditionRun, err := c.ConditionRunGetter.GetConditionRun(ctx, conditionRunID)

	if err != nil {
		c.logAndStatFinalizeConditionRunError(logFields, err)
		return false, err
	}

	// Get all conditionParticipants for the current run
	conditionParticipants, err := c.ConditionParticipantGetter.GetPendingConditionParticipantsByConditionRunID(ctx, domain, ownerID, conditionRunID)

	if err != nil {
		c.logAndStatFinalizeConditionRunError(logFields, err)
		return false, err
	}

	if conditionParticipants == nil {
		logrus.WithFields(logFields).Info("Attempting to satisfy a condition with no pending conditionParticipants")
		return false, nil
	}

	condition, err := c.ConditionGetter.GetCondition(ctx, domain, conditionID, ownerID)

	if err != nil {
		c.logAndStatFinalizeConditionRunError(logFields, err)
		return false, err
	}

	// Ensure that none of the conditionParticipants have expired. A StepFunction normally handles conditionParticipant expiration,
	// so the check here is just to guard against that process being delayed.
	partitionedConditionParticipants := conditionparticipantutils.PartitionExpiredConditionParticipants(*conditionParticipants, conditionRun)

	if len(partitionedConditionParticipants.Expired) > 0 {
		finalizedExpiredConditionParticipants := c.finalizeTerminatedConditionParticipantList(partitionedConditionParticipants.Expired, models.ConditionParticipantEndStateExpired)
		finalizedConditionParticipants = append(finalizedConditionParticipants, finalizedExpiredConditionParticipants...)
	}

	if len(partitionedConditionParticipants.Active) > 0 {
		var finalizationErr error

		switch conditionRun.ConditionRunEndState {
		case models.ConditionRunEndStateTimeout:
			finalizedConditionTimeoutConditionParticipants := c.finalizeTerminatedConditionParticipantList(partitionedConditionParticipants.Active, models.ConditionParticipantEndStateConditionExpired)
			finalizedConditionParticipants = append(finalizedConditionParticipants, finalizedConditionTimeoutConditionParticipants...)
		case models.ConditionRunEndStateCanceled:
			finalizedConditionCanceledConditionParticipants := c.finalizeTerminatedConditionParticipantList(partitionedConditionParticipants.Active, models.ConditionParticipantEndStateConditionCanceled)
			finalizedConditionParticipants = append(finalizedConditionParticipants, finalizedConditionCanceledConditionParticipants...)
		case models.ConditionRunEndStateSatisfied:
			var finalizedSatisfiedConditionParticipants []models.FinalizedConditionParticipantS3Input
			finalizedSatisfiedConditionParticipants, effectRollups, finalizationErr = c.finalizeSatisfiedConditionParticipantList(ctx, partitionedConditionParticipants.Active, conditionRun.EffectSettings, condition)
			finalizedConditionParticipants = append(finalizedConditionParticipants, finalizedSatisfiedConditionParticipants...)
		default:
			finalizationErr = errors.IllegalArgument.New("unexpected condition run end state")
		}

		if finalizationErr != nil {
			c.logAndStatFinalizeConditionRunError(logFields, finalizationErr)
			return false, finalizationErr
		}
	}

	err = c.S3Client.StoreFinalizedConditionRun(ctx, conditionID, conditionRunID, models.FinalizedConditionRunS3Input{
		ConditionParticipants: finalizedConditionParticipants,
		EffectRollups:         effectRollups,
	})

	if err != nil {
		logrus.WithFields(logrus.Fields{
			"conditionID":    conditionID,
			"conditionRunID": conditionRunID,
		}).WithError(err).Error("failed to store finalized condition participants in S3")

		return false, err
	}

	return true, nil
}

func (c *completerImpl) ProcessConditionRun(ctx context.Context, domain, conditionID, conditionRunID, ownerID string) error {
	startTime := time.Now()
	defer func() {
		c.Statter.TimingDuration(conditionRunProcessingLatencyStatName, time.Since(startTime), 1)
	}()

	logFields := logrus.Fields{
		"domain":           domain,
		"condition_id":     conditionID,
		"condition_run_id": conditionRunID,
		"owner_id":         ownerID,
	}

	err := c.SFNClient.ExecuteConditionRunProcessingStateMachine(models.ConditionRunProcessingSFNInput{
		ConditionID:    conditionID,
		Domain:         domain,
		OwnerID:        ownerID,
		ConditionRunID: conditionRunID,
	})

	if err != nil {
		c.logAndStatProcessConditionRunError(logFields, err)
		return err
	}

	return nil
}

func (c *completerImpl) ProcessConditionRunEffectRollups(ctx context.Context, domain, conditionID, conditionRunID, ownerID string) error {
	startTime := time.Now()
	defer func() {
		c.Statter.TimingDuration(processConditionRunEffectRollupLatencyStatName, time.Since(startTime), 1)
	}()

	logFields := logrus.Fields{
		"domain":           domain,
		"condition_id":     conditionID,
		"condition_run_id": conditionRunID,
		"owner_id":         ownerID,
	}

	finalizedConditionRun, err := c.S3Client.GetFinalizedConditionRun(ctx, conditionID, conditionRunID)

	if err != nil {
		c.logAndStatEffeectRollupError(logFields, err)
		return err
	}

	if len(finalizedConditionRun.EffectRollups) == 0 {
		return nil
	}

	condition, err := c.ConditionGetter.GetCondition(ctx, domain, conditionID, ownerID)
	if err != nil {
		c.logAndStatEffeectRollupError(logFields, err)
		return err
	}

	conditionRun, err := c.ConditionRunGetter.GetConditionRun(ctx, conditionRunID)
	if err != nil {
		c.logAndStatEffeectRollupError(logFields, err)
		return err
	}

	return c.EffectController.ProcessEffectRollups(ctx, condition, conditionRun, finalizedConditionRun.EffectRollups)
}

func (c *completerImpl) finalizeTerminatedConditionParticipantList(conditionParticipants []models.ConditionParticipant, conditionParticipantEndState models.ConditionParticipantEndState) []models.FinalizedConditionParticipantS3Input {
	var finalizedConditionParticipants []models.FinalizedConditionParticipantS3Input

	for _, terminatedConditionParticipant := range conditionParticipants {
		finalizedConditionParticipants = append(finalizedConditionParticipants, models.FinalizedConditionParticipantS3Input{
			ConditionParticipantEndState: conditionParticipantEndState,
			ConditionParticipantID:       terminatedConditionParticipant.ConditionParticipantID,
		})
	}

	return finalizedConditionParticipants
}

func (c *completerImpl) finalizeSatisfiedConditionParticipantList(
	ctx context.Context,
	conditionParticipants []models.ConditionParticipant,
	settings []models.EffectSettings,
	condition models.Condition) ([]models.FinalizedConditionParticipantS3Input, []models.FinalizedEffectRollupS3Input, error) {

	var finalizedConditionParticipants []models.FinalizedConditionParticipantS3Input
	var finalizedEffectRollups []models.FinalizedEffectRollupS3Input

	validConditionParticipants, invalidConditionParticipants, err := c.EffectController.ValidateAndFilterConditionParticipants(ctx, conditionParticipants, settings)

	if err != nil {
		return nil, nil, err
	}

	if len(invalidConditionParticipants) > 0 {
		finalizedInvalidConditionParticipants := c.finalizeTerminatedConditionParticipantList(invalidConditionParticipants, models.ConditionParticipantEndStateFailedValidation)
		finalizedConditionParticipants = append(finalizedConditionParticipants, finalizedInvalidConditionParticipants...)
	}

	if len(validConditionParticipants) > 0 {
		var conditionRunArtifacts []models.ConditionRunArtifactsByEffect

		conditionRunArtifacts, err = c.EffectController.PreProcessConditionParticipants(ctx, validConditionParticipants, settings, condition)
		if err != nil {
			return nil, nil, err
		}

		for _, artifact := range conditionRunArtifacts {
			for _, participant := range artifact.ConditionParticipants {
				finalizedConditionParticipants = append(finalizedConditionParticipants, models.FinalizedConditionParticipantS3Input{
					ConditionParticipantID:           participant.ConditionParticipant.ConditionParticipantID,
					ConditionParticipantEndState:     models.ConditionParticipantEndStateSatisfied,
					ConditionParticipantEffectOutput: &participant.EffectOutput,
				})
			}

			finalizedEffectRollups = append(finalizedEffectRollups, models.FinalizedEffectRollupS3Input{
				EffectType:   artifact.EffectType,
				EffectRollup: artifact.EffectRollupOutput,
			})
		}
	}

	return finalizedConditionParticipants, finalizedEffectRollups, nil
}

func (c *completerImpl) createEnqueueConditionParticipantWorkerPool(ctx context.Context, jobCount int, jobsChan <-chan EnqueueConditionParticipantJob, doneChan chan<- bool, errChan chan<- error) {
	wg := sync.WaitGroup{}
	wg.Add(jobCount)
	workerCount := math.MinInt(maxEnqueueConditionParticipantConcurrency, jobCount)

	// Called in goroutine to give callers an opportunity to set up done/error listeners
	go func() {
		defer func() {
			wg.Wait()
			close(doneChan)
		}()

		for w := 1; w <= workerCount; w++ {
			go func(jobs <-chan EnqueueConditionParticipantJob, errChan chan<- error) {
				for job := range jobs {
					err := c.ConditionParticipantCompleter.EnqueueForProcessing(
						ctx,
						job.ConditionRunID,
						job.ConditionParticipantID,
						job.ConditionParticipantEndState,
						job.EffectOutput,
						true)

					if err != nil {
						errChan <- err
					}

					wg.Done()
				}
			}(jobsChan, errChan)
		}
	}()
}

func (c *completerImpl) logAndStatProcessConditionRunError(logFields logrus.Fields, err error) {
	logrus.WithFields(logFields).WithError(err).Error("error processing condition run")
	c.Statter.Inc(conditionRunProcessingErrorStatName, 1, 1)
}

func (c *completerImpl) logAndStatEffeectRollupError(logFields logrus.Fields, err error) {
	logrus.WithFields(logFields).WithError(err).Error("error processing effect rollups")
	c.Statter.Inc(processConditionRunEffectRollupErrorStatName, 1, 1)
}

func (c *completerImpl) logAndStatEnqueueConditionParticipantsError(logFields logrus.Fields, err error) {
	logrus.WithFields(logFields).WithError(err).Error("error enqueueing condition participants")
	c.Statter.Inc(enqueueConditionParticipantsErrorStatName, 1, 1)
}

func (c *completerImpl) logAndStatFinalizeConditionRunError(logFields logrus.Fields, err error) {
	logrus.WithFields(logFields).WithError(err).Error("error finalizing condition run")
	c.Statter.Inc(finalizeConditionRunErrorStatName, 1, 1)
}
