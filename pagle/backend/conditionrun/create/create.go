package create

import (
	"context"

	"code.justin.tv/commerce/pagle/backend/dynamo/conditionrun"
	"code.justin.tv/commerce/pagle/backend/models"
)

type Creator interface {
	CreateConditionRun(ctx context.Context, conditionID string) (*models.ConditionRun, error)
}

type creatorImpl struct {
	ConditionRunDAO conditionrun.DAO `inject:""`
}

func NewCreator() Creator {
	return &creatorImpl{}
}

func (c *creatorImpl) CreateConditionRun(ctx context.Context, conditionID string) (*models.ConditionRun, error) {
	return c.ConditionRunDAO.CreateConditionRun(ctx, conditionID)
}
