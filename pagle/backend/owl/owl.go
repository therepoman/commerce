package owl

import (
	"context"
	"strconv"

	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/hystrix/commands"
	owl "code.justin.tv/web/owl/client"
	"code.justin.tv/web/owl/oauth2"
	"github.com/afex/hystrix-go/hystrix"
	log "github.com/sirupsen/logrus"
)

type OwlClient interface {
	GetClient(ctx context.Context, clientID string) (*oauth2.Client, error)
	GetOwner(ctx context.Context, clientID string) (string, error)
}

type clientImpl struct {
	Client owl.Client `inject:""`
}

func NewClient() OwlClient {
	return &clientImpl{}
}

func (c *clientImpl) GetClient(ctx context.Context, clientID string) (*oauth2.Client, error) {
	var client *oauth2.Client

	err := hystrix.Do(commands.OwlGetClientCommand, func() error {
		var innerErr error

		client, innerErr = c.Client.GetClient(ctx, clientID, nil)

		return innerErr
	}, nil)

	if err != nil {
		return nil, err
	}

	return client, nil
}

func (c *clientImpl) GetOwner(ctx context.Context, clientID string) (string, error) {
	logFields := log.Fields{
		"client_id": clientID,
	}

	client, err := c.GetClient(ctx, clientID)

	if err != nil {
		msg := "Error getting client"
		log.WithFields(logFields).WithError(err).Error(msg)
		return "", errors.InternalError.New(msg)
	}

	if client == nil || client.OwnerID == nil {
		msg := "No owner found for client"
		log.WithFields(logFields).WithError(err).Error(msg)
		return "", errors.InternalError.New(msg)
	}

	ownerID := strconv.Itoa(*client.OwnerID)

	return ownerID, nil
}
