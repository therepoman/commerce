package effect

import (
	"context"
	"fmt"
	"sync"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend/effect/tenant"
	"code.justin.tv/commerce/pagle/backend/effect/tenant/givebits"
	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/backend/stats"
	"code.justin.tv/commerce/pagle/backend/utils/effect/settings"
)

const (
	tenantLatencyStatNamePattern = "%s_%s_tenant_latency"
)

type EffectController interface {
	CreateTenant(ctx context.Context, effectType models.EffectType) (tenant.Tenant, error)
	PreProcessConditionParticipants(ctx context.Context, conditionParticipants []models.ConditionParticipant, effectSettings []models.EffectSettings, condition models.Condition) ([]models.ConditionRunArtifactsByEffect, error)
	FinalizeEffect(ctx context.Context, effect models.Effect, effectSettings []models.EffectSettings, condition models.Condition) error
	ReleaseEffect(ctx context.Context, effect models.Effect) error
	CreateEffectHold(ctx context.Context, effect models.Effect, ttlSeconds int32, condition models.Condition) (updatedEffect models.Effect, validationErr, err error)
	ValidateSupportedEffects(ctx context.Context, supportedEffects []models.EffectType, extension *models.Extension) error
	ValidateAndFilterConditionParticipants(ctx context.Context, conditionParticipants []models.ConditionParticipant, effectSettings []models.EffectSettings) ([]models.ConditionParticipant, []models.ConditionParticipant, error)
	ValidateEffectSettings(ctx context.Context, supportedEffects []models.EffectType, effectSettings []models.EffectSettings, extension *models.Extension) ([]models.EffectSettings, error)
	ValidateEffect(ctx context.Context, effect models.Effect) error
	MakeEffectAggregateGetter(ctx context.Context, effect models.Effect) (models.EffectAggregateGetter, error)
	ProcessEffectRollups(ctx context.Context, condition models.Condition, conditionRun models.ConditionRun, effectRollups []models.FinalizedEffectRollupS3Input) error
}

type controllerImpl struct {
	GiveBitsTenant givebits.GiveBitsTenant `inject:"givebitstenant"`
	Statter        stats.Statter           `inject:""`
}

func NewController() EffectController {
	return &controllerImpl{}
}

type ValidatedAndFilteredConditionParticipantsMessage struct {
	ValidConditionParticipants   []models.ConditionParticipant
	InvalidConditionParticipants []models.ConditionParticipant
	Error                        error
}

func (c *controllerImpl) CreateTenant(ctx context.Context, effectType models.EffectType) (tenant.Tenant, error) {
	switch effectType {
	case models.GiveBits:
		return c.GiveBitsTenant, nil
	default:
		message := "unhandled effect type while getting effect tenant"
		log.WithField("effectType", effectType).Warn(message)
		return nil, errors.IllegalArgument.New(message)
	}
}

func (c *controllerImpl) statTenantLatency(effectType models.EffectType, operationName string, duration time.Duration) {
	c.Statter.TimingDuration(makeTenantLatencyKey(effectType, operationName), duration, 1)
}

func (c *controllerImpl) FinalizeEffect(ctx context.Context, effect models.Effect, effectSettings []models.EffectSettings, condition models.Condition) error {
	startTime := time.Now()

	defer func() {
		c.statTenantLatency(effect.EffectType, "FinalizeTransaction", time.Since(startTime))
	}()

	tenant, err := c.CreateTenant(ctx, effect.EffectType)

	if err != nil {
		return err
	}

	effectSettingsForType, err := settings.GetEffectSettingsForEffectType(effect.EffectType, effectSettings)

	if err != nil {
		return err
	}

	return tenant.FinalizeTransaction(ctx, effect, effectSettingsForType, condition)
}

func (c *controllerImpl) ReleaseEffect(ctx context.Context, effect models.Effect) error {
	startTime := time.Now()

	defer func() {
		c.statTenantLatency(effect.EffectType, "ReleaseTransaction", time.Since(startTime))
	}()

	tenant, err := c.CreateTenant(ctx, effect.EffectType)

	if err != nil {
		return err
	}

	return tenant.ReleaseTransaction(ctx, effect)
}

func (c *controllerImpl) CreateEffectHold(ctx context.Context, effect models.Effect, ttlSeconds int32, condition models.Condition) (updatedEffect models.Effect, validationErr, err error) {
	startTime := time.Now()

	defer func() {
		c.statTenantLatency(effect.EffectType, "CreateTransaction", time.Since(startTime))
	}()

	tenant, err := c.CreateTenant(ctx, effect.EffectType)

	if err != nil {
		return models.Effect{}, nil, err
	}

	transactionID, validationErr, err := tenant.CreateTransaction(ctx, effect, ttlSeconds, condition)

	if validationErr != nil {
		return models.Effect{}, validationErr, nil
	}

	if err != nil {
		return models.Effect{}, nil, err
	}

	effect.TransactionID = transactionID

	return effect, nil, nil
}

func (c *controllerImpl) PreProcessConditionParticipants(
	ctx context.Context,
	conditionParticipants []models.ConditionParticipant,
	effectSettings []models.EffectSettings,
	condition models.Condition) ([]models.ConditionRunArtifactsByEffect, error) {

	var wg sync.WaitGroup
	var groupedPreProcessedConditionParticipants []models.ConditionRunArtifactsByEffect

	groupedConditionParticipants := groupConditionParticipantsByEffect(conditionParticipants)

	workers := len(groupedConditionParticipants)

	wg.Add(workers)
	resultsChan := make(chan models.ConditionRunArtifactsByEffect, workers)

	for effectType, conditionParticipants := range groupedConditionParticipants {
		tenant, err := c.CreateTenant(ctx, effectType)

		if err != nil {
			return nil, err
		}

		effectSettingsForType, err := settings.GetEffectSettingsForEffectType(effectType, effectSettings)

		if err != nil {
			return nil, err
		}

		go func(conditionParticipants []models.ConditionParticipant, effectType models.EffectType) {
			startTime := time.Now()

			defer func() {
				c.statTenantLatency(effectType, "PreProcessConditionParticipants", time.Since(startTime))
			}()

			conditionParticipantsForEffectType, effectRollupOutput, err := tenant.PreProcessConditionParticipants(ctx, conditionParticipants, effectSettingsForType, condition)

			resultsChan <- models.ConditionRunArtifactsByEffect{
				EffectType:            effectType,
				ConditionParticipants: conditionParticipantsForEffectType,
				EffectRollupOutput:    effectRollupOutput,
				Error:                 err,
			}

			wg.Done()
		}(conditionParticipants, effectType)
	}

	go func() {
		defer close(resultsChan)
		wg.Wait()
	}()

	for result := range resultsChan {
		if result.Error != nil {
			return nil, result.Error
		}

		groupedPreProcessedConditionParticipants = append(groupedPreProcessedConditionParticipants, result)
	}

	return groupedPreProcessedConditionParticipants, nil
}

func (c *controllerImpl) ValidateAndFilterConditionParticipants(ctx context.Context, conditionParticipants []models.ConditionParticipant, effectSettings []models.EffectSettings) ([]models.ConditionParticipant, []models.ConditionParticipant, error) {
	var wg sync.WaitGroup
	var validConditionParticipants, invalidConditionParticipants []models.ConditionParticipant

	groupedConditionParticipants := groupConditionParticipantsByEffect(conditionParticipants)

	workers := len(groupedConditionParticipants)

	wg.Add(workers)
	resultsChan := make(chan ValidatedAndFilteredConditionParticipantsMessage, workers)

	for effectType, conditionParticipants := range groupedConditionParticipants {
		tenant, err := c.CreateTenant(ctx, effectType)
		if err != nil {
			return validConditionParticipants, invalidConditionParticipants, err
		}

		effectSettingsForType, err := settings.GetEffectSettingsForEffectType(effectType, effectSettings)
		if err != nil {
			return validConditionParticipants, invalidConditionParticipants, err
		}

		go func(conditionParticipants []models.ConditionParticipant, effectType models.EffectType) {
			startTime := time.Now()

			defer func() {
				c.statTenantLatency(effectType, "ValidateAndFilterConditionParticipants", time.Since(startTime))
			}()

			validConditionParticipantsForEffectType, invalidConditionParticipantsForEffectType, err := tenant.ValidateAndFilterConditionParticipants(conditionParticipants, effectSettingsForType)

			resultsChan <- ValidatedAndFilteredConditionParticipantsMessage{
				ValidConditionParticipants:   validConditionParticipantsForEffectType,
				InvalidConditionParticipants: invalidConditionParticipantsForEffectType,
				Error:                        err,
			}

			wg.Done()
		}(conditionParticipants, effectType)
	}

	go func() {
		defer close(resultsChan)
		wg.Wait()
	}()

	for result := range resultsChan {
		if result.Error != nil {
			return nil, nil, result.Error
		}

		validConditionParticipants = append(validConditionParticipants, result.ValidConditionParticipants...)
		invalidConditionParticipants = append(invalidConditionParticipants, result.InvalidConditionParticipants...)
	}

	return validConditionParticipants, invalidConditionParticipants, nil
}

func (c *controllerImpl) ValidateSupportedEffects(ctx context.Context, supportedEffects []models.EffectType, extension *models.Extension) error {
	for _, effectType := range supportedEffects {
		t, err := c.CreateTenant(ctx, effectType)
		if err != nil {
			return err
		}

		err = func(t tenant.Tenant) error {
			startTime := time.Now()

			defer func() {
				c.statTenantLatency(effectType, "ValidateSupportedEffect", time.Since(startTime))
			}()

			return t.ValidateSupportedEffect(ctx, extension)
		}(t)

		if err != nil {
			return err
		}
	}

	return nil
}

func (c *controllerImpl) ValidateEffectSettings(ctx context.Context, supportedEffects []models.EffectType, effectSettings []models.EffectSettings, extension *models.Extension) ([]models.EffectSettings, error) {
	// Validate that effect settings are set up correctly and correspond to supported effects
	validatedEffectSettings, err := settings.ValidateEffectSettings(supportedEffects, effectSettings)
	if err != nil {
		return nil, err
	}

	var wg sync.WaitGroup

	workers := len(validatedEffectSettings)

	wg.Add(workers)
	errChan := make(chan error, workers)

	for _, effectSettings := range validatedEffectSettings {
		tenant, err := c.CreateTenant(ctx, effectSettings.EffectType)
		if err != nil {
			return validatedEffectSettings, err
		}

		go func(effectSettings models.EffectSettings) {
			startTime := time.Now()

			defer func() {
				c.statTenantLatency(effectSettings.EffectType, "ValidateEffectSettings", time.Since(startTime))
			}()

			err := tenant.ValidateEffectSettings(ctx, effectSettings, extension)

			errChan <- err

			wg.Done()
		}(effectSettings)
	}

	go func() {
		defer close(errChan)
		wg.Wait()
	}()

	for err := range errChan {
		if err != nil {
			return nil, err
		}
	}

	return validatedEffectSettings, nil
}

func (c *controllerImpl) ValidateEffect(ctx context.Context, effect models.Effect) error {
	startTime := time.Now()

	defer func() {
		c.statTenantLatency(effect.EffectType, "ValidateEffect", time.Since(startTime))
	}()

	tenant, err := c.CreateTenant(ctx, effect.EffectType)
	if err != nil {
		return err
	}

	err = tenant.ValidateEffect(ctx, effect)

	return err
}

func (c *controllerImpl) MakeEffectAggregateGetter(ctx context.Context, effect models.Effect) (models.EffectAggregateGetter, error) {
	startTime := time.Now()

	defer func() {
		c.statTenantLatency(effect.EffectType, "MakeEffectAggregateGetter", time.Since(startTime))
	}()

	tenant, err := c.CreateTenant(ctx, effect.EffectType)

	if err != nil {
		return nil, err
	}

	getter := tenant.MakeEffectAggregateGetter(effect)

	return getter, nil
}

func (c *controllerImpl) ProcessEffectRollups(ctx context.Context, condition models.Condition, conditionRun models.ConditionRun, effectRollups []models.FinalizedEffectRollupS3Input) error {
	var wg sync.WaitGroup

	workers := len(effectRollups)

	wg.Add(workers)
	errChan := make(chan error, workers)

	for _, effectRollup := range effectRollups {
		tenant, err := c.CreateTenant(ctx, effectRollup.EffectType)

		if err != nil {
			return err
		}

		go func(effectRollup models.FinalizedEffectRollupS3Input) {
			startTime := time.Now()

			defer func() {
				c.statTenantLatency(effectRollup.EffectType, "ProcessEffectRollups", time.Since(startTime))
			}()

			err = tenant.ProcessEffectRollup(ctx, condition, conditionRun, effectRollup)

			errChan <- err

			wg.Done()
		}(effectRollup)
	}

	go func() {
		defer close(errChan)
		wg.Wait()
	}()

	for err := range errChan {
		if err != nil {
			return err
		}
	}

	return nil
}

func makeTenantLatencyKey(effectType models.EffectType, operationName string) string {
	return fmt.Sprintf(tenantLatencyStatNamePattern, effectType, operationName)
}

func groupConditionParticipantsByEffect(conditionParticipants []models.ConditionParticipant) map[models.EffectType][]models.ConditionParticipant {
	groupedConditionParticipants := make(map[models.EffectType][]models.ConditionParticipant)

	for _, conditionParticipant := range conditionParticipants {
		groupedConditionParticipants[conditionParticipant.Effect.EffectType] = append(
			groupedConditionParticipants[conditionParticipant.Effect.EffectType],
			conditionParticipant,
		)
	}

	return groupedConditionParticipants
}
