package effect

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pagle/backend/models"
	givebitstenant_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/effect/tenant/givebits"
	statter_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/stats"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestEffectController_PreProcessConditionParticipantsForBitsTenant(t *testing.T) {
	Convey("given an effect controller", t, func() {
		giveBitsTenant := new(givebitstenant_mock.GiveBitsTenant)
		statter := new(statter_mock.Statter)

		controller := controllerImpl{
			GiveBitsTenant: giveBitsTenant,
			Statter:        statter,
		}

		mockEffect := models.Effect{
			EffectType: models.GiveBits,
			Params: models.EffectParams{
				GiveBits: &models.EffectParamsGiveBits{
					PoolableInput: models.EffectInputPoolable{
						Amount:     50,
						FromUserID: "user-1",
					},
				},
			},
		}

		mockCondition := models.Condition{}

		mockConditionParticipants := []models.ConditionParticipant{
			{
				Effect: mockEffect,
			},
		}

		mockPoolRecipientsPercentage := []models.PoolRecipientWeightedShare{
			{
				ShareWeight: 1,
				ToUserID:    "channel-1",
			},
		}

		mockEffectSettings := []models.EffectSettings{
			{
				EffectType: models.GiveBits,
				Settings: &models.EffectTypeSettings{
					GiveBits: &models.GiveBitsSettings{
						PoolRecipients: mockPoolRecipientsPercentage,
					},
				},
			},
		}

		statter.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything)

		Convey("when give bits splitter does not return an error", func() {
			mockProcessedConditionParticipants := []models.PreProcessedConditionParticipant{
				{
					ConditionParticipant: models.ConditionParticipant{
						Effect: models.Effect{
							Params: models.EffectParams{
								GiveBits: &models.EffectParamsGiveBits{
									PoolableInput: models.EffectInputPoolable{
										Amount:     50,
										FromUserID: "user-1",
									},
								},
							},
						},
					},
					EffectOutput: models.EffectOutput{
						GiveBits: nil,
					},
				},
			}

			mockGroupedProcessedConditionParticipants := []models.ConditionRunArtifactsByEffect{
				{
					EffectType:            models.GiveBits,
					ConditionParticipants: mockProcessedConditionParticipants,
				},
			}

			giveBitsTenant.On("PreProcessConditionParticipants", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockProcessedConditionParticipants, nil, nil)

			Convey("we should return a single list of conditionParticipants, modified in place with distribution data", func() {
				conditionRunArtifacts, err := controller.PreProcessConditionParticipants(context.Background(), mockConditionParticipants, mockEffectSettings, mockCondition)
				So(err, ShouldBeNil)
				So(conditionRunArtifacts, ShouldResemble, mockGroupedProcessedConditionParticipants)
			})
		})

		Convey("when give bits splitter returns an error", func() {
			giveBitsTenant.On("PreProcessConditionParticipants", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, nil, errors.New("ERROR"))

			Convey("we should error", func() {
				preProcessedConditionParticipants, err := controller.PreProcessConditionParticipants(context.Background(), mockConditionParticipants, mockEffectSettings, mockCondition)
				So(err, ShouldNotBeNil)
				So(preProcessedConditionParticipants, ShouldBeNil)
			})
		})
	})
}

func TestEffectController_ValidateAndFilterConditionParticipants(t *testing.T) {
	Convey("given an effect controller", t, func() {
		giveBitsTenant := new(givebitstenant_mock.GiveBitsTenant)
		statter := new(statter_mock.Statter)

		controller := controllerImpl{
			GiveBitsTenant: giveBitsTenant,
			Statter:        statter,
		}

		mockEffect := models.Effect{
			EffectType: models.GiveBits,
			Params: models.EffectParams{
				GiveBits: &models.EffectParamsGiveBits{
					PoolableInput: models.EffectInputPoolable{
						Amount:     50,
						FromUserID: "user-1",
					},
				},
			},
		}

		mockConditionParticipants := []models.ConditionParticipant{
			{
				Effect: mockEffect,
			},
			{
				Effect: mockEffect,
			},
		}

		mockPoolRecipientsPercentage := []models.PoolRecipientWeightedShare{
			{
				ShareWeight: 1,
				ToUserID:    "channel-1",
			},
		}

		mockEffectSettings := []models.EffectSettings{
			{
				EffectType: models.GiveBits,
				Settings: &models.EffectTypeSettings{
					GiveBits: &models.GiveBitsSettings{
						PoolRecipients: mockPoolRecipientsPercentage,
					},
				},
			},
		}

		statter.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything)

		Convey("when give bits validation and filtering does not return an error", func() {
			mockValidConditionParticipants := []models.ConditionParticipant{
				{
					Effect: mockEffect,
				},
			}

			mockInvalidConditionParticipants := []models.ConditionParticipant{
				{
					Effect: mockEffect,
				},
			}

			giveBitsTenant.On("ValidateAndFilterConditionParticipants", mock.Anything, mock.Anything).Return(mockValidConditionParticipants, mockInvalidConditionParticipants, nil)

			Convey("we should return the valid and invalid conditionParticipants lists", func() {
				validConditionParticipants, invalidConditionParticipants, err := controller.ValidateAndFilterConditionParticipants(context.Background(), mockConditionParticipants, mockEffectSettings)
				So(err, ShouldBeNil)
				So(validConditionParticipants, ShouldResemble, mockValidConditionParticipants)
				So(invalidConditionParticipants, ShouldResemble, mockInvalidConditionParticipants)
			})
		})

		Convey("when give bits splitter returns an error", func() {
			giveBitsTenant.On("ValidateAndFilterConditionParticipants", mock.Anything, mock.Anything).Return(nil, nil, errors.New("ERROR"))

			Convey("we should error", func() {
				validConditionParticipants, invalidConditionParticipants, err := controller.ValidateAndFilterConditionParticipants(context.Background(), mockConditionParticipants, mockEffectSettings)
				So(err, ShouldNotBeNil)
				So(validConditionParticipants, ShouldBeNil)
				So(invalidConditionParticipants, ShouldBeNil)
			})
		})
	})
}

func TestEffectController_ValidateEffectSettings(t *testing.T) {
	Convey("given an effect controller", t, func() {
		giveBitsTenant := new(givebitstenant_mock.GiveBitsTenant)
		statter := new(statter_mock.Statter)

		var mockExtension *models.Extension

		controller := controllerImpl{
			GiveBitsTenant: giveBitsTenant,
			Statter:        statter,
		}

		mockPoolRecipientsPercentage := []models.PoolRecipientWeightedShare{
			{
				ShareWeight: 1,
				ToUserID:    "channel-1",
			},
		}

		mockEffectSettings := []models.EffectSettings{
			{
				EffectType: models.GiveBits,
				Settings: &models.EffectTypeSettings{
					GiveBits: &models.GiveBitsSettings{
						PoolRecipients: mockPoolRecipientsPercentage,
					},
				},
			},
		}

		mockSupportedEffects := []models.EffectType{models.GiveBits}

		statter.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything)

		Convey("when global effect settings are not valid, we should error", func() {
			var invalidEffectSettings []models.EffectSettings
			validatedEffectSettings, err := controller.ValidateEffectSettings(context.Background(), mockSupportedEffects, invalidEffectSettings, mockExtension)
			So(err, ShouldNotBeNil)
			So(validatedEffectSettings, ShouldBeNil)
		})

		Convey("when give bits effect settings validation does not return an error", func() {
			giveBitsTenant.On("ValidateEffectSettings", mock.Anything, mock.Anything, mockExtension).Return(nil)

			Convey("we should return the validated effect settings", func() {
				validatedEffectSettings, err := controller.ValidateEffectSettings(context.Background(), mockSupportedEffects, mockEffectSettings, mockExtension)
				So(err, ShouldBeNil)
				So(validatedEffectSettings, ShouldResemble, mockEffectSettings)
			})
		})

		Convey("when give bits effect settings validation returns an error", func() {
			giveBitsTenant.On("ValidateEffectSettings", mock.Anything, mock.Anything, mockExtension).Return(errors.New("ERROR"))

			Convey("we should error", func() {
				validatedEffectSettings, err := controller.ValidateEffectSettings(context.Background(), mockSupportedEffects, mockEffectSettings, mockExtension)
				So(err, ShouldNotBeNil)
				So(validatedEffectSettings, ShouldBeNil)
			})
		})
	})
}

func TestEffectController_ProcessEffectRollups(t *testing.T) {
	Convey("given an effect controller", t, func() {
		giveBitsTenant := new(givebitstenant_mock.GiveBitsTenant)
		statter := new(statter_mock.Statter)

		controller := controllerImpl{
			GiveBitsTenant: giveBitsTenant,
			Statter:        statter,
		}

		mockEffectRollups := []models.FinalizedEffectRollupS3Input{
			{
				EffectType: models.GiveBits,
				EffectRollup: &models.EffectRollupOutput{
					GiveBits: &models.EffectRollupOutputGiveBits{
						Recipients: []models.BitsPoolRecipientFixedShare{
							{
								Amount:   10,
								ToUserID: "foo-channel",
							},
						},
					},
				},
			},
		}

		mockCondition := models.Condition{}
		mockConditionRun := models.ConditionRun{}

		statter.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything)

		Convey("when give bits does not error", func() {
			giveBitsTenant.On("ProcessEffectRollup", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

			Convey("we should return nil", func() {
				err := controller.ProcessEffectRollups(context.Background(), mockCondition, mockConditionRun, mockEffectRollups)
				So(err, ShouldBeNil)
			})
		})

		Convey("when the give bits tenant eturns an error", func() {
			giveBitsTenant.On("ProcessEffectRollup", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("ERROR"))

			Convey("we should error", func() {
				err := controller.ProcessEffectRollups(context.Background(), mockCondition, mockConditionRun, mockEffectRollups)
				So(err, ShouldNotBeNil)
			})
		})
	})
}

func TestEffectController_ValidateEffect(t *testing.T) {
	Convey("given an effect controller", t, func() {
		giveBitsTenant := new(givebitstenant_mock.GiveBitsTenant)
		statter := new(statter_mock.Statter)

		controller := controllerImpl{
			GiveBitsTenant: giveBitsTenant,
			Statter:        statter,
		}

		mockEffect := models.Effect{
			EffectType: models.GiveBits,
			Params: models.EffectParams{
				GiveBits: &models.EffectParamsGiveBits{
					PoolableInput: models.EffectInputPoolable{
						Amount:     1,
						FromUserID: "user-1",
					},
				},
			},
		}

		statter.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything)

		Convey("when give bits effect validation does not return an error", func() {
			giveBitsTenant.On("ValidateEffect", mock.Anything, mock.Anything).Return(nil)

			Convey("we should NOT return an error", func() {
				err := controller.ValidateEffect(context.Background(), mockEffect)
				So(err, ShouldBeNil)
			})
		})

		Convey("when give bits effect validation returns an error", func() {
			giveBitsTenant.On("ValidateEffect", mock.Anything, mock.Anything).Return(errors.New("ERROR"))

			Convey("we should error", func() {
				err := controller.ValidateEffect(context.Background(), mockEffect)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
