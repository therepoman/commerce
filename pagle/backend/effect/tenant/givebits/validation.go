package givebits

import (
	"context"
	"sync"

	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/hystrix/commands"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/backend/utils/hystrix"
	"code.justin.tv/commerce/payday/models/api"
	"github.com/sirupsen/logrus"
)

type ChannelEligibilityMessage struct {
	Eligible bool
	Error    error
	UserID   string
}

func (t *giveBitsTenantImpl) ValidateSupportedEffect(ctx context.Context, extension *models.Extension) error {
	// Validate that extension installation on channel is Bits-enabled. This will implicitly ensure that the extension
	// developer can be paid out (they will be added as a pool recipient when the owning condition is satisfied)
	if extension != nil {
		isBitsEnabled, err := t.EMSClient.IsExtensionBitsEnabled(ctx, extension.ID, extension.InstallationChannelID)

		if err != nil {
			return err
		}

		if !isBitsEnabled {
			return errors.ChannelExtensionInstallationNotBitsEnabled.New("channel/extension not eligible for Bits effects")
		}
	}

	return nil
}

func (t *giveBitsTenantImpl) ValidateEffectSettings(ctx context.Context, effectSettings models.EffectSettings, extension *models.Extension) error {
	l := logrus.WithFields(logrus.Fields{
		"effect_settings": effectSettings,
		"extension":       extension,
	})

	poolRecipients, err := getPoolRecipientsFromSettings(effectSettings)
	if err != nil {
		return err
	}

	poolRecipientOwners := makeOwnerIDMapFromPoolRecipients(poolRecipients)

	arePoolRecipientsWhitelisted, err := t.arePoolRecipientsWhitelisted(poolRecipientOwners)

	if err != nil {
		return err
	}

	if !arePoolRecipientsWhitelisted {
		return errors.BitsRecipientIneligible.New("recipient is not whitelisted to receive bits")
	}

	var wg sync.WaitGroup

	workers := len(poolRecipientOwners)

	wg.Add(workers)
	resultsChan := make(chan ChannelEligibilityMessage, workers)

	for userID := range poolRecipientOwners {
		go func(userID string) {
			var resp *api.ChannelEligibleResponse

			err := hystrix.CallTwitchClientWithHystrix(commands.PaydayGetChannelInfoCommand, hystrix.HystrixDo, func() error {
				var err error

				resp, err = t.PaydayClient.GetChannelInfo(ctx, userID, nil)

				return err
			})

			resultsChan <- ChannelEligibilityMessage{
				Eligible: resp != nil && resp.Eligible,
				Error:    err,
				UserID:   userID,
			}

			wg.Done()
		}(userID)
	}

	go func() {
		defer close(resultsChan)
		wg.Wait()
	}()

	for result := range resultsChan {
		ll := l.WithField("eligibility_result", result)

		if result.Error != nil {
			message := "error checking bits eligibility"
			ll.WithError(result.Error).Error(message)
			return errors.InternalError.Wrap(result.Error, message)
		} else if !result.Eligible {
			message := "recipient is not eligible to receive bits"
			ll.Warn(message)
			return errors.BitsRecipientIneligible.New(message)
		}
	}

	return nil
}

func (t *giveBitsTenantImpl) ValidateEffect(ctx context.Context, effect models.Effect) error {
	params, err := getPoolableEffectParams(effect)
	if err != nil {
		return err
	}

	if params.Amount <= 0 {
		return errors.BitsAmountOutOfRange.New("must provide a bits amount > 0")
	}

	isBitsBenefactorWhitelisted, err := t.isBitsBenefactorWhitelisted(params.FromUserID)

	if err != nil {
		return err
	}

	if !isBitsBenefactorWhitelisted {
		return errors.BitsBenefactorIneligible.New("benefactor is not whitelisted to give bits")
	}

	return nil
}

func (t *giveBitsTenantImpl) ValidateAndFilterConditionParticipants(conditionParticipants []models.ConditionParticipant, effectSettings models.EffectSettings) ([]models.ConditionParticipant, []models.ConditionParticipant, error) {
	poolRecipients, err := getPoolRecipientsFromSettings(effectSettings)
	if err != nil {
		return nil, nil, err
	}

	poolRecipientOwners := makeOwnerIDMapFromPoolRecipients(poolRecipients)

	var validConditionParticipants, invalidConditionParticipants []models.ConditionParticipant

	for _, conditionParticipant := range conditionParticipants {
		params, err := getPoolableEffectParams(conditionParticipant.Effect)
		if err != nil {
			return nil, nil, err
		}

		if _, ok := poolRecipientOwners[params.FromUserID]; ok {
			// User cannot give bits to themselves. Mark condition participant as invalid.
			invalidConditionParticipants = append(invalidConditionParticipants, conditionParticipant)
		} else {
			// Condition participant is valid
			validConditionParticipants = append(validConditionParticipants, conditionParticipant)
		}
	}

	return validConditionParticipants, invalidConditionParticipants, nil
}
