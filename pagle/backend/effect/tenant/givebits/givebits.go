package givebits

import (
	"code.justin.tv/commerce/pagle/backend/effect/tenant"
	"code.justin.tv/commerce/pagle/backend/effectstaticconfig"
	"code.justin.tv/commerce/pagle/backend/ems"
	"code.justin.tv/commerce/pagle/backend/owl"
	"code.justin.tv/commerce/pagle/backend/ripley"
	"code.justin.tv/commerce/pagle/backend/sns"
	payday "code.justin.tv/commerce/payday/client"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
)

type GiveBitsTenant interface {
	tenant.Tenant
}

type giveBitsTenantImpl struct {
	PaydayTwirpClient        paydayrpc.Payday          `inject:""`
	PaydayClient             payday.Client             `inject:""`
	EffectStaticConfigPoller effectstaticconfig.Poller `inject:""`
	OwlClient                owl.OwlClient             `inject:""`
	SNSClient                sns.SNSClient             `inject:""`
	RipleyClient             ripley.Client             `inject:""`
	EMSClient                ems.EMSClient             `inject:""`
}

func NewGiveBitsTenant() GiveBitsTenant {
	return &giveBitsTenantImpl{}
}
