package givebits

import (
	"fmt"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/pagle/backend/models"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

func (t *giveBitsTenantImpl) MakeEffectAggregateGetter(effect models.Effect) models.EffectAggregateGetter {
	return func(incrementField, decrementField *string) ([]models.ConditionParticipantEffectAggregateUpdates, error) {
		var updates []models.ConditionParticipantEffectAggregateUpdates
		giveBitsParams, err := getPoolableEffectParams(effect)

		if err != nil {
			return nil, err
		}

		if !strings.BlankP(incrementField) {
			amountKey := fmt.Sprintf(models.GiveBitsTotalBitsAmountAggregateDocumentPattern, *incrementField)
			totalKey := fmt.Sprintf(models.GiveBitsTotalEffectsAggregateDocumentPattern, *incrementField)

			updates = append(updates, []models.ConditionParticipantEffectAggregateUpdates{
				{
					NameBuilder:    expression.Name(amountKey),
					OperandBuilder: expression.Name(amountKey).Plus(expression.Value(giveBitsParams.Amount)),
				},
				{
					NameBuilder:    expression.Name(totalKey),
					OperandBuilder: expression.Name(totalKey).Plus(expression.Value(1)),
				},
			}...)
		}

		if !strings.BlankP(decrementField) {
			amountKey := fmt.Sprintf(models.GiveBitsTotalBitsAmountAggregateDocumentPattern, *decrementField)
			totalKey := fmt.Sprintf(models.GiveBitsTotalEffectsAggregateDocumentPattern, *decrementField)

			updates = append(updates, []models.ConditionParticipantEffectAggregateUpdates{
				{
					NameBuilder:    expression.Name(amountKey),
					OperandBuilder: expression.Name(amountKey).Plus(expression.Value(-giveBitsParams.Amount)),
				},
				{
					NameBuilder:    expression.Name(totalKey),
					OperandBuilder: expression.Name(totalKey).Plus(expression.Value(-1)),
				},
			}...)
		}

		return updates, nil
	}
}
