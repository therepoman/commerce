package testhelpers

import (
	"code.justin.tv/commerce/pagle/backend/models"
)

const (
	MockExtensionOwnerID = "some-extension-owner"
)

type RevShareTestCase struct {
	Description                 string
	ConditionParticipants       []models.ConditionParticipant
	PoolRecipients              []models.PoolRecipientWeightedShare
	ExtensionRevSharePercentage float64
	ExpectedPoolDistribution    []models.BitsPoolRecipientFixedShare
}

func MakeExtensionRevShareTestCases() []RevShareTestCase {
	conditionParticipant5BitsUser1 := models.ConditionParticipant{
		Effect: models.Effect{
			EffectType: models.GiveBits,
			Params: models.EffectParams{
				GiveBits: &models.EffectParamsGiveBits{
					PoolableInput: models.EffectInputPoolable{
						Amount:     5,
						FromUserID: "user-1",
					},
				},
			},
		},
	}

	conditionParticipant5BitsUser2 := models.ConditionParticipant{
		Effect: models.Effect{
			EffectType: models.GiveBits,
			Params: models.EffectParams{
				GiveBits: &models.EffectParamsGiveBits{
					PoolableInput: models.EffectInputPoolable{
						Amount:     5,
						FromUserID: "user-2",
					},
				},
			},
		},
	}

	conditionParticipant10BitsUser3 := models.ConditionParticipant{
		Effect: models.Effect{
			EffectType: models.GiveBits,
			Params: models.EffectParams{
				GiveBits: &models.EffectParamsGiveBits{
					PoolableInput: models.EffectInputPoolable{
						Amount:     10,
						FromUserID: "user-3",
					},
				},
			},
		},
	}

	pool100 := []models.PoolRecipientWeightedShare{
		{
			ShareWeight: 1,
			ToUserID:    "channel-1",
		},
	}

	pool50_50 := []models.PoolRecipientWeightedShare{
		{
			ShareWeight: 1,
			ToUserID:    "channel-1",
		},
		{
			ShareWeight: 1,
			ToUserID:    "channel-2",
		},
	}

	pool_75_25 := []models.PoolRecipientWeightedShare{
		{
			ShareWeight: 75,
			ToUserID:    "channel-1",
		},
		{
			ShareWeight: 25,
			ToUserID:    "channel-2",
		},
	}

	pool_33_33_33 := []models.PoolRecipientWeightedShare{
		{
			ShareWeight: 1,
			ToUserID:    "channel-1",
		},
		{
			ShareWeight: 1,
			ToUserID:    "channel-2",
		},
		{
			ShareWeight: 1,
			ToUserID:    "channel-3",
		},
	}

	return []RevShareTestCase{
		{
			Description: "1 participant gives 5 bits, 1 channel in pool, dev 20% share",
			ConditionParticipants: []models.ConditionParticipant{
				conditionParticipant5BitsUser1,
			},
			PoolRecipients:              pool100,
			ExtensionRevSharePercentage: 0.2,
			ExpectedPoolDistribution: []models.BitsPoolRecipientFixedShare{
				{
					ToUserID: "channel-1",
					Amount:   4,
				},
				{
					ToUserID:                MockExtensionOwnerID,
					Amount:                  1,
					ForExtensionDevRevShare: true,
				},
			},
		},
		{
			Description: "2 participant give 5 bits each, 1 channel in pool, dev 20% share",
			ConditionParticipants: []models.ConditionParticipant{
				conditionParticipant5BitsUser1,
				conditionParticipant5BitsUser2,
			},
			PoolRecipients:              pool100,
			ExtensionRevSharePercentage: 0.2,
			ExpectedPoolDistribution: []models.BitsPoolRecipientFixedShare{
				{
					ToUserID: "channel-1",
					Amount:   8,
				},
				{
					ToUserID:                MockExtensionOwnerID,
					Amount:                  2,
					ForExtensionDevRevShare: true,
				},
			},
		},
		{
			Description: "2 participant give 5 bits each, 1 channel in pool, dev 50% share",
			ConditionParticipants: []models.ConditionParticipant{
				conditionParticipant5BitsUser1,
				conditionParticipant5BitsUser2,
			},
			PoolRecipients:              pool100,
			ExtensionRevSharePercentage: 0.5,
			ExpectedPoolDistribution: []models.BitsPoolRecipientFixedShare{
				{
					ToUserID: "channel-1",
					Amount:   5,
				},
				{
					ToUserID:                MockExtensionOwnerID,
					Amount:                  5,
					ForExtensionDevRevShare: true,
				},
			},
		},
		{
			Description: "2 participant give 5 bits each, 2 channels in pool 50/50, dev 20% share",
			ConditionParticipants: []models.ConditionParticipant{
				conditionParticipant5BitsUser1,
				conditionParticipant5BitsUser2,
			},
			PoolRecipients:              pool50_50,
			ExtensionRevSharePercentage: 0.2,
			ExpectedPoolDistribution: []models.BitsPoolRecipientFixedShare{
				{
					ToUserID: "channel-1",
					Amount:   4,
				},
				{
					ToUserID: "channel-2",
					Amount:   4,
				},
				{
					ToUserID:                MockExtensionOwnerID,
					Amount:                  2,
					ForExtensionDevRevShare: true,
				},
			},
		},
		{
			Description: "2 participant give 5 bits each, 1 participant gives 10 bits, 2 channels in pool 80/20, dev 20% share",
			ConditionParticipants: []models.ConditionParticipant{
				conditionParticipant5BitsUser1,
				conditionParticipant5BitsUser2,
				conditionParticipant10BitsUser3,
			},
			PoolRecipients:              pool_75_25,
			ExtensionRevSharePercentage: 0.2,
			ExpectedPoolDistribution: []models.BitsPoolRecipientFixedShare{
				{
					ToUserID: "channel-1",
					Amount:   12,
				},
				{
					ToUserID: "channel-2",
					Amount:   4,
				},
				{
					ToUserID:                MockExtensionOwnerID,
					Amount:                  4,
					ForExtensionDevRevShare: true,
				},
			},
		},
		{
			Description: "2 participant give 5 bits each, 3 channels in pool 33/33/33, dev 40% share",
			ConditionParticipants: []models.ConditionParticipant{
				conditionParticipant5BitsUser1,
				conditionParticipant5BitsUser2,
			},
			PoolRecipients:              pool_33_33_33,
			ExtensionRevSharePercentage: 0.4,
			ExpectedPoolDistribution: []models.BitsPoolRecipientFixedShare{
				{
					ToUserID: "channel-1",
					Amount:   2,
				},
				{
					ToUserID: "channel-2",
					Amount:   2,
				},
				{
					ToUserID: "channel-3",
					Amount:   2,
				},
				{
					ToUserID:                MockExtensionOwnerID,
					Amount:                  4,
					ForExtensionDevRevShare: true,
				},
			},
		},
	}
}
