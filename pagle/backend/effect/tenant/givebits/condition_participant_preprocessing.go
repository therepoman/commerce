package givebits

import (
	"context"
	"fmt"

	gringotts "code.justin.tv/commerce/gringotts/sns"
	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/backend/ripley"
	"github.com/sirupsen/logrus"
)

const (
	// Values copied from payday https://git-aws.internal.justin.tv/commerce/payday/blob/master/sqs/handlers/revenue/handler.go#L21-L27
	bitsUsedTransactionType             = "bits-used"
	bitsOnExtensionsRevenueSourcePrefix = "bits-on-extension"
	partnerParticipantRole              = "partner"
	affiliateParticipantRole            = "affiliate"
	extensionDeveloperParticipantRole   = "extension-dev"

	bitsChallengeSku = "bits-challenge"
)

func (t *giveBitsTenantImpl) PreProcessConditionParticipants(
	ctx context.Context,
	conditionParticipants []models.ConditionParticipant,
	effectSettings models.EffectSettings,
	condition models.Condition) ([]models.PreProcessedConditionParticipant, *models.EffectRollupOutput, error) {

	poolRecipients, err := getPoolRecipientsFromSettings(effectSettings)

	if err != nil {
		return nil, nil, err
	}

	bitsPoolRecipients := poolRecipientsToBitsPoolRecipients(poolRecipients)

	if condition.Extension != nil {
		shouldRevShareWithExtension, extensionCreatorID, err := t.getExtensionCreatorIDForRevShare(ctx, condition.Extension.ID)
		if err != nil {
			return nil, nil, err
		}

		if shouldRevShareWithExtension {
			bitsPoolRecipients, err = t.addExtensionDeveloperAsPoolRecipient(bitsPoolRecipients, extensionCreatorID)

			if err != nil {
				return nil, nil, err
			}
		}
	}

	var preProcessedConditionParticipants []models.PreProcessedConditionParticipant
	for _, participant := range conditionParticipants {
		preProcessedConditionParticipants = append(preProcessedConditionParticipants, models.PreProcessedConditionParticipant{
			ConditionParticipant: participant,
		})
	}

	// Determine how many bits are in pool
	amountToSplit, err := sumBitsForConditionParticipants(conditionParticipants)

	if err != nil {
		return nil, nil, err
	}

	// Determine how many bits each owner is allocated from total
	fixedRecipients, err := weightedToFixedRecipients(amountToSplit, bitsPoolRecipients)

	if err != nil {
		return nil, nil, err
	}

	return preProcessedConditionParticipants, &models.EffectRollupOutput{
		GiveBits: &models.EffectRollupOutputGiveBits{
			Recipients: fixedRecipients,
		},
	}, nil
}

func (t *giveBitsTenantImpl) ProcessEffectRollup(ctx context.Context, condition models.Condition, conditionRun models.ConditionRun, effectRollup models.FinalizedEffectRollupS3Input) error {
	log := logrus.WithFields(logrus.Fields{
		"condition_id":     condition.ConditionID,
		"domain":           condition.Domain,
		"owner_id":         condition.OwnerID,
		"condition_run_id": conditionRun.ConditionRunID,
	})

	if condition.Extension == nil {
		log.Info("payout not yet implemented for bits usage outside extensions, skipping")
		return nil
	}

	if effectRollup.EffectRollup == nil || effectRollup.EffectRollup.GiveBits == nil {
		log.Info("found no GiveBits effect rollups to process, skipping")
		return nil
	}

	for _, recipient := range effectRollup.EffectRollup.GiveBits.Recipients {
		revenueSource, participantRole, err := t.getRevenueSourceAndParticipantRole(ctx, condition, recipient)
		if err != nil {
			log.WithError(err).Error(err)
			return err
		}

		err = t.SNSClient.PublishGringottsRevenueEvent(ctx, gringotts.RevenueEventMessage{
			ChannelId:       recipient.ToUserID,
			TimeOfEvent:     conditionRun.CompletedAt,
			TransactionId:   fmt.Sprintf("%s.%s", conditionRun.ConditionRunID, recipient.ToUserID), // Each user can only be paid out once per conditionRun
			TransactionType: bitsUsedTransactionType,
			RevenueSource:   revenueSource,
			Revenue:         float64(recipient.Amount),
			ParticipantRole: participantRole,
			SKU:             bitsChallengeSku,
		})
		if err != nil {
			log.WithError(err).Error(err)
			return err
		}
	}

	return nil
}

func (t *giveBitsTenantImpl) getRevenueSourceAndParticipantRole(ctx context.Context, condition models.Condition, recipient models.BitsPoolRecipientFixedShare) (revenueSource, participantRole string, err error) {
	if condition.Extension != nil {
		revenueSourceID := ""

		// For extension developer payout, we attribute revenue to the channel where the extension is installed
		if recipient.ForExtensionDevRevShare {
			revenueSourceID = condition.Extension.InstallationChannelID
			participantRole = extensionDeveloperParticipantRole
			// For broadcaster payout, we attribute revenue to the extension that generated the revenue
		} else {
			revenueSourceID = condition.Extension.ID
			partnerType, err := t.RipleyClient.GetPartnerType(ctx, recipient.ToUserID)
			if err != nil {
				return "", "", err
			}

			switch partnerType {
			case ripley.AffiliatePartnerType:
				participantRole = affiliateParticipantRole
			case ripley.TraditionalPartnerType:
				participantRole = partnerParticipantRole
			default:
				return "", "", errors.InternalError.New("can only create a revenue event for partners and affiliates")
			}
		}

		return fmt.Sprintf("%s:%s", bitsOnExtensionsRevenueSourcePrefix, revenueSourceID), participantRole, nil
	} else {
		return "", "", errors.IllegalState.New("payout not yet implemented for bits usage outside extensions")
	}
}
