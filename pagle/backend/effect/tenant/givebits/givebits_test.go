package givebits

import (
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	gringotts "code.justin.tv/commerce/gringotts/sns"
	"code.justin.tv/commerce/pagle/backend/effect/tenant/givebits/testhelpers"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/backend/ripley"
	effectstaticconfig_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/effectstaticconfig"
	ems_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/ems"
	owlclient_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/owl"
	ripleyclient_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/ripley"
	snsclient_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/sns"
	payday_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/payday/client"
	paydayrpc_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/payday/models/api"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/shopspring/decimal"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestGiveBitsTenant_CreateTransaction(t *testing.T) {
	Convey("given a givebits tenant", t, func() {
		paydayClient := new(paydayrpc_mock.Payday)

		tenant := giveBitsTenantImpl{
			PaydayTwirpClient: paydayClient,
		}

		mockEffect := models.Effect{
			EffectType: models.GiveBits,
			Params: models.EffectParams{
				GiveBits: &models.EffectParamsGiveBits{
					PoolableInput: models.EffectInputPoolable{
						Amount:     50,
						FromUserID: "user-1",
					},
				},
			},
		}

		mockCondition := models.Condition{
			Name: "some-condition",
		}

		mockTTL := int32(60)

		Convey("when the effect params are invalid", func() {
			mockEffect = models.Effect{
				EffectType: models.GiveBits,
				Params: models.EffectParams{
					GiveBits: nil,
				},
			}

			Convey("we should error", func() {
				res, validationErr, err := tenant.CreateTransaction(context.Background(), mockEffect, mockTTL, mockCondition)
				So(validationErr, ShouldBeNil)
				So(err, ShouldNotBeNil)
				So(res, ShouldBeEmpty)
			})
		})

		Convey("when the user has insufficient balance", func() {
			insufficientBalanceErr := paydayrpc.NewClientError(twirp.NewError(twirp.FailedPrecondition, "insufficient bits balance"), paydayrpc.ClientError{
				ErrorCode: paydayrpc.ErrCodeInsufficientBitsBalance,
			})

			paydayClient.On("CreateHold", mock.Anything, mock.Anything).Return(nil, insufficientBalanceErr)

			Convey("we should return a validation error", func() {
				res, validationErr, err := tenant.CreateTransaction(context.Background(), mockEffect, mockTTL, mockCondition)
				So(validationErr, ShouldNotBeNil)
				So(err, ShouldBeNil)
				So(res, ShouldBeEmpty)
			})
		})

		Convey("when the effect params are valid", func() {
			Convey("when CreateHold fails", func() {
				paydayClient.On("CreateHold", mock.Anything, mock.Anything).Return(nil, errors.New("ERROR"))

				Convey("we should error", func() {
					res, validationErr, err := tenant.CreateTransaction(context.Background(), mockEffect, mockTTL, mockCondition)
					So(validationErr, ShouldBeNil)
					So(err, ShouldNotBeNil)
					So(res, ShouldBeEmpty)
				})
			})

			Convey("when CreateHold returns a nil response", func() {
				paydayClient.On("CreateHold", mock.Anything, mock.Anything).Return(nil, nil)

				Convey("we should error", func() {
					res, validationErr, err := tenant.CreateTransaction(context.Background(), mockEffect, mockTTL, mockCondition)
					So(validationErr, ShouldBeNil)
					So(err, ShouldNotBeNil)
					So(res, ShouldBeEmpty)
				})
			})

			Convey("when CreateHold succeeds", func() {
				paydayClient.On("CreateHold", mock.Anything, mock.Anything).Return(&paydayrpc.CreateHoldResp{
					Hold: &paydayrpc.Hold{
						HoldId: "hold-id",
					},
				}, nil)

				Convey("we should return the Hold ID", func() {
					res, validationErr, err := tenant.CreateTransaction(context.Background(), mockEffect, mockTTL, mockCondition)
					So(validationErr, ShouldBeNil)
					So(err, ShouldBeNil)
					So(res, ShouldEqual, "hold-id")
				})
			})
		})
	})
}

func TestGiveBitsTenant_FinalizeTransaction(t *testing.T) {
	Convey("given a givebits tenant", t, func() {
		paydayClient := new(paydayrpc_mock.Payday)

		tenant := giveBitsTenantImpl{
			PaydayTwirpClient: paydayClient,
		}

		mockTransactionID := "transaction-123"

		mockEffect := models.Effect{
			EffectType:    models.GiveBits,
			TransactionID: mockTransactionID,
			Params: models.EffectParams{
				GiveBits: &models.EffectParamsGiveBits{
					PoolableInput: models.EffectInputPoolable{
						Amount:     50,
						FromUserID: "user-1",
					},
				},
			},
			Output: models.EffectOutput{},
		}

		mockEffectSettings := models.EffectSettings{
			EffectType: models.GiveBits,
			Settings: &models.EffectTypeSettings{
				GiveBits: &models.GiveBitsSettings{
					PoolRecipients: []models.PoolRecipientWeightedShare{},
				},
			},
		}

		mockCondition := models.Condition{
			OwnerID: "kappa123",
		}

		Convey("when the effect params are invalid", func() {
			mockEffect = models.Effect{
				EffectType: models.GiveBits,
				Params: models.EffectParams{
					GiveBits: nil,
				},
			}

			Convey("we should error", func() {
				err := tenant.FinalizeTransaction(context.Background(), mockEffect, mockEffectSettings, mockCondition)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the effect params are valid", func() {
			Convey("when the effect output is valid", func() {
				Convey("when effect settings contains an extensionID, we should use the UseBitsInExtension transaction type", func() {
					mockExtension := &models.Extension{
						ID:                    "some-extension",
						InstallationChannelID: "abc",
					}
					mockEffectSettings := models.EffectSettings{
						EffectType: models.GiveBits,
						Settings: &models.EffectTypeSettings{
							GiveBits: &models.GiveBitsSettings{
								PoolRecipients: []models.PoolRecipientWeightedShare{},
							},
						},
					}

					mockCondition := models.Condition{Extension: mockExtension}

					expectedRequest := &paydayrpc.FinalizeHoldReq{
						HoldId:          mockEffect.TransactionID,
						UserId:          mockEffect.Params.GiveBits.PoolableInput.FromUserID,
						TransactionType: paydayrpc.TransactionType_UseBitsInExtensionChallenge,
						IgnorePayout:    true,
						BitsUsageAttributions: map[string]*paydayrpc.BitsUsageAttribution{
							mockExtension.InstallationChannelID: {
								BitsRevenueAttribution: 0,
								RewardAttribution:      int64(mockEffect.Params.GiveBits.PoolableInput.Amount),
							},
						},
					}

					paydayClient.On("FinalizeHold", mock.Anything, expectedRequest).Return(&paydayrpc.FinalizeHoldResp{
						Hold: &paydayrpc.Hold{
							HoldId: mockEffect.TransactionID,
						},
					}, nil)

					err := tenant.FinalizeTransaction(context.Background(), mockEffect, mockEffectSettings, mockCondition)
					So(err, ShouldBeNil)
				})

				Convey("when FinalizeHold fails, we should error", func() {
					paydayClient.On("FinalizeHold", mock.Anything, mock.Anything).Return(nil, errors.New("ERROR"))

					err := tenant.FinalizeTransaction(context.Background(), mockEffect, mockEffectSettings, mockCondition)
					So(err, ShouldNotBeNil)
				})

				Convey("when FinalizeHold succeeds, we should not error", func() {
					expectedRequest := &paydayrpc.FinalizeHoldReq{
						HoldId:          mockEffect.TransactionID,
						UserId:          mockEffect.Params.GiveBits.PoolableInput.FromUserID,
						TransactionType: paydayrpc.TransactionType_GiveBitsToBroadcasterChallenge,
						IgnorePayout:    true,
						BitsUsageAttributions: map[string]*paydayrpc.BitsUsageAttribution{
							mockCondition.OwnerID: {
								BitsRevenueAttribution: 0,
								RewardAttribution:      int64(mockEffect.Params.GiveBits.PoolableInput.Amount),
							},
						},
					}

					paydayClient.On("FinalizeHold", mock.Anything, expectedRequest).Return(&paydayrpc.FinalizeHoldResp{
						Hold: &paydayrpc.Hold{
							HoldId: mockEffect.TransactionID,
						},
					}, nil)

					err := tenant.FinalizeTransaction(context.Background(), mockEffect, mockEffectSettings, mockCondition)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}

func TestGiveBitsTenant_ReleaseTransaction(t *testing.T) {
	Convey("given a givebits tenant", t, func() {
		paydayClient := new(paydayrpc_mock.Payday)

		tenant := giveBitsTenantImpl{
			PaydayTwirpClient: paydayClient,
		}

		mockEffect := models.Effect{
			EffectType: models.GiveBits,
			Params: models.EffectParams{
				GiveBits: &models.EffectParamsGiveBits{
					PoolableInput: models.EffectInputPoolable{
						Amount:     50,
						FromUserID: "user-1",
					},
				},
			},
		}

		Convey("when the effect params are invalid", func() {
			mockEffect = models.Effect{
				EffectType: models.GiveBits,
				Params: models.EffectParams{
					GiveBits: nil,
				},
			}

			Convey("we should error", func() {
				err := tenant.ReleaseTransaction(context.Background(), mockEffect)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the effect params are valid", func() {
			Convey("when ReleaseHold fails", func() {
				paydayClient.On("ReleaseHold", mock.Anything, mock.Anything).Return(nil, errors.New("ERROR"))

				Convey("we should error", func() {
					err := tenant.ReleaseTransaction(context.Background(), mockEffect)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when ReleaseHold succeeds", func() {
				paydayClient.On("ReleaseHold", mock.Anything, mock.Anything).Return(&paydayrpc.ReleaseHoldResp{
					Hold: &paydayrpc.Hold{
						HoldId: "hold-id",
					},
				}, nil)

				Convey("we should NOT error", func() {
					err := tenant.ReleaseTransaction(context.Background(), mockEffect)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}

func TestGiveBitsTenant_PreProcessConditionParticipants(t *testing.T) {
	Convey("given a givebits tenant", t, func() {
		effectStaticConfigPoller := new(effectstaticconfig_mock.Poller)
		owlClient := new(owlclient_mock.OwlClient)

		tenant := giveBitsTenantImpl{
			EffectStaticConfigPoller: effectStaticConfigPoller,
			OwlClient:                owlClient,
		}

		mockEffect := models.Effect{
			EffectType: models.GiveBits,
			Params: models.EffectParams{
				GiveBits: &models.EffectParamsGiveBits{
					PoolableInput: models.EffectInputPoolable{
						Amount:     50,
						FromUserID: "user-1",
					},
				},
			},
		}

		mockCondition := models.Condition{}

		mockConditionParticipant := models.ConditionParticipant{
			Effect: mockEffect,
		}

		mockConditionParticipants := []models.ConditionParticipant{
			mockConditionParticipant,
		}

		mockPoolRecipientsPercentage := []models.PoolRecipientWeightedShare{
			{
				ShareWeight: 1,
				ToUserID:    "channel-1",
			},
		}

		mockEffectSettings := models.EffectSettings{
			EffectType: models.GiveBits,
			Settings: &models.EffectTypeSettings{
				GiveBits: &models.GiveBitsSettings{
					PoolRecipients: mockPoolRecipientsPercentage,
				},
			},
		}

		Convey("when the effect settings are missing pool recipients, we should error", func() {
			mockEffectSettings := models.EffectSettings{
				EffectType: models.GiveBits,
				Settings: &models.EffectTypeSettings{
					GiveBits: nil,
				},
			}

			preProcessedParticipants, _, err := tenant.PreProcessConditionParticipants(context.Background(), mockConditionParticipants, mockEffectSettings, mockCondition)
			So(err, ShouldNotBeNil)
			So(preProcessedParticipants, ShouldBeEmpty)
		})

		Convey("when the effect settings contain pool recipients", func() {
			Convey("when the Bits effects are bound to an extension", func() {
				mockExtensionID := "some-extension"
				mockExtension := &models.Extension{
					ID:                    mockExtensionID,
					InstallationChannelID: "abc",
				}

				mockExtensionOwnerID := "some-extension-owner"
				mockEffectSettings := models.EffectSettings{
					EffectType: models.GiveBits,
					Settings: &models.EffectTypeSettings{
						GiveBits: &models.GiveBitsSettings{
							PoolRecipients: mockPoolRecipientsPercentage,
						},
					},
				}

				mockCondition := models.Condition{Extension: mockExtension}

				Convey("when we error getting bits static config, we should error", func() {
					effectStaticConfigPoller.On("GetEffectStaticConfig").Return(nil, errors.New("ERROR"))

					_, _, err := tenant.PreProcessConditionParticipants(context.Background(), mockConditionParticipants, mockEffectSettings, mockCondition)

					So(err, ShouldNotBeNil)
				})

				Convey("when we error getting owner from owl client, we should error", func() {
					effectStaticConfigPoller.On("GetEffectStaticConfig").Return(&models.EffectStaticConfig{
						GiveBits: &models.GiveBitsStaticConfig{
							ExtensionRevShareBlacklist:  []string{},
							ExtensionRevSharePercentage: 0.2,
						},
					}, nil)

					owlClient.On("GetOwner", mock.Anything, mockExtensionID).Return("", errors.New("ERROR"))

					_, _, err := tenant.PreProcessConditionParticipants(context.Background(), mockConditionParticipants, mockEffectSettings, mockCondition)

					So(err, ShouldNotBeNil)
				})

				Convey("when the extension is blacklisted for payout, we should not add the extension owner as a pool recipient", func() {
					effectStaticConfigPoller.On("GetEffectStaticConfig").Return(&models.EffectStaticConfig{
						GiveBits: &models.GiveBitsStaticConfig{
							ExtensionRevShareBlacklist:  []string{mockExtensionID},
							ExtensionRevSharePercentage: 0.2,
						},
					}, nil)

					owlClient.On("GetOwner", mock.Anything, mockExtensionID).Return(mockExtensionOwnerID, nil)

					_, effectRollup, err := tenant.PreProcessConditionParticipants(context.Background(), mockConditionParticipants, mockEffectSettings, mockCondition)

					So(err, ShouldBeNil)
					So(effectRollup, ShouldNotBeNil)
					So(effectRollup.GiveBits, ShouldNotBeNil)

					So(effectRollup.GiveBits.Recipients, ShouldResemble, []models.BitsPoolRecipientFixedShare{
						{
							Amount:   50,
							ToUserID: "channel-1",
						},
					})
				})

				Convey("when the extension rev share percentage is out of range, we should error", func() {
					effectStaticConfigPoller.On("GetEffectStaticConfig").Return(&models.EffectStaticConfig{
						GiveBits: &models.GiveBitsStaticConfig{
							ExtensionRevShareBlacklist:  []string{},
							ExtensionRevSharePercentage: 1.1,
						},
					}, nil)

					owlClient.On("GetOwner", mock.Anything, mockExtensionID).Return(mockExtensionOwnerID, nil)

					_, _, err := tenant.PreProcessConditionParticipants(context.Background(), mockConditionParticipants, mockEffectSettings, mockCondition)

					So(err, ShouldNotBeNil)
				})

				Convey("when the rev share percentage is valid", func() {
					for _, testCase := range testhelpers.MakeExtensionRevShareTestCases() {
						func(testCase testhelpers.RevShareTestCase) {
							Convey(fmt.Sprintf("[%s] we should add the extension owner as a pool recipient with the correct weight", testCase.Description), func() {
								effectStaticConfigPoller.On("GetEffectStaticConfig").Return(&models.EffectStaticConfig{
									GiveBits: &models.GiveBitsStaticConfig{
										ExtensionRevShareBlacklist:  []string{},
										ExtensionRevSharePercentage: testCase.ExtensionRevSharePercentage,
									},
								}, nil)

								owlClient.On("GetOwner", mock.Anything, mockExtensionID).Return(testhelpers.MockExtensionOwnerID, nil)

								_, effectRollup, err := tenant.PreProcessConditionParticipants(context.Background(), testCase.ConditionParticipants, models.EffectSettings{
									EffectType: models.GiveBits,
									Settings: &models.EffectTypeSettings{
										GiveBits: &models.GiveBitsSettings{
											PoolRecipients: testCase.PoolRecipients,
										},
									},
								}, mockCondition)
								So(err, ShouldBeNil)
								So(effectRollup, ShouldNotBeNil)
								So(effectRollup.GiveBits, ShouldNotBeNil)

								So(effectRollup.GiveBits.Recipients, ShouldResemble, testCase.ExpectedPoolDistribution)
							})
						}(testCase)
					}
				})
			})

			Convey("when the Bits effects are not bound to an extension", func() {
				Convey("we should return the pre-processed condition participants", func() {
					preProcessedParticipants, effectRollup, err := tenant.PreProcessConditionParticipants(context.Background(), mockConditionParticipants, mockEffectSettings, mockCondition)
					So(err, ShouldBeNil)
					So(effectRollup, ShouldResemble, &models.EffectRollupOutput{
						GiveBits: &models.EffectRollupOutputGiveBits{
							Recipients: []models.BitsPoolRecipientFixedShare{
								{
									Amount:   50,
									ToUserID: "channel-1",
								},
							},
						}})
					So(preProcessedParticipants, ShouldResemble, []models.PreProcessedConditionParticipant{
						{
							ConditionParticipant: mockConditionParticipant,
						},
					})
				})
			})
		})
	})
}

func TestGiveBitsTenant_ValidateAndFilterConditionParticipants(t *testing.T) {
	Convey("given a givebits tenant", t, func() {
		tenant := giveBitsTenantImpl{}

		mockEffect := models.Effect{
			EffectType: models.GiveBits,
			Params: models.EffectParams{
				GiveBits: &models.EffectParamsGiveBits{
					PoolableInput: models.EffectInputPoolable{
						Amount:     50,
						FromUserID: "user-1",
					},
				},
			},
		}

		mockConditionParticipants := []models.ConditionParticipant{
			{
				Effect: mockEffect,
			},
		}

		mockPoolRecipientsPercentage := []models.PoolRecipientWeightedShare{
			{
				ShareWeight: 1,
				ToUserID:    "channel-1",
			},
		}

		mockEffectSettings := models.EffectSettings{
			EffectType: models.GiveBits,
			Settings: &models.EffectTypeSettings{
				GiveBits: &models.GiveBitsSettings{
					PoolRecipients: mockPoolRecipientsPercentage,
				},
			},
		}

		Convey("when the effect settings are invalid", func() {
			mockEffectSettings := models.EffectSettings{
				EffectType: models.GiveBits,
				Settings: &models.EffectTypeSettings{
					GiveBits: nil,
				},
			}

			Convey("we should error", func() {
				validConditionParticipants, invalidConditionParticipants, err := tenant.ValidateAndFilterConditionParticipants(mockConditionParticipants, mockEffectSettings)
				So(err, ShouldNotBeNil)
				So(validConditionParticipants, ShouldBeEmpty)
				So(invalidConditionParticipants, ShouldBeEmpty)
			})
		})

		Convey("when the effect settings are valid", func() {
			Convey("when a participant has an effect with invalid params", func() {
				mockEffect := models.Effect{
					EffectType: models.GiveBits,
					Params: models.EffectParams{
						GiveBits: nil,
					},
				}

				mockConditionParticipants := []models.ConditionParticipant{
					{
						Effect: mockEffect,
					},
				}

				Convey("we should error", func() {
					validConditionParticipants, invalidConditionParticipants, err := tenant.ValidateAndFilterConditionParticipants(mockConditionParticipants, mockEffectSettings)
					So(err, ShouldNotBeNil)
					So(validConditionParticipants, ShouldBeEmpty)
					So(invalidConditionParticipants, ShouldBeEmpty)
				})
			})

			Convey("when a participant has an effect that results in a user giving bits to themselves", func() {
				mockEffect := models.Effect{
					EffectType: models.GiveBits,
					Params: models.EffectParams{
						GiveBits: &models.EffectParamsGiveBits{
							PoolableInput: models.EffectInputPoolable{
								Amount:     50,
								FromUserID: "channel-1",
							},
						},
					},
				}

				mockConditionParticipants := []models.ConditionParticipant{
					{
						Effect: mockEffect,
					},
				}

				Convey("we should return that participant with the invalid participants", func() {
					validConditionParticipants, invalidConditionParticipants, err := tenant.ValidateAndFilterConditionParticipants(mockConditionParticipants, mockEffectSettings)
					So(err, ShouldBeNil)
					So(validConditionParticipants, ShouldBeEmpty)
					So(invalidConditionParticipants, ShouldResemble, mockConditionParticipants)
				})
			})

			Convey("when all participants have valid effects", func() {
				Convey("we should return the participants with the valid participants", func() {
					validConditionParticipants, invalidConditionParticipants, err := tenant.ValidateAndFilterConditionParticipants(mockConditionParticipants, mockEffectSettings)
					So(err, ShouldBeNil)
					So(validConditionParticipants, ShouldResemble, mockConditionParticipants)
					So(invalidConditionParticipants, ShouldBeEmpty)
				})
			})
		})
	})
}

func TestGiveBitsTenant_ValidateSupportedEffect(t *testing.T) {
	Convey("given a givebits tenant", t, func() {
		paydayClient := new(payday_mock.Client)
		effectStaticConfigPoller := new(effectstaticconfig_mock.Poller)
		owlClient := new(owlclient_mock.OwlClient)
		emsClient := new(ems_mock.EMSClient)

		tenant := giveBitsTenantImpl{
			PaydayClient:             paydayClient,
			EffectStaticConfigPoller: effectStaticConfigPoller,
			OwlClient:                owlClient,
			EMSClient:                emsClient,
		}

		Convey("when extension is nil, we should return nil", func() {
			err := tenant.ValidateSupportedEffect(context.Background(), nil)
			So(err, ShouldBeNil)
		})

		Convey("when extension is not nil", func() {
			mockExtension := &models.Extension{
				ID:                    "some-extension",
				InstallationChannelID: "abc",
			}

			Convey("when the call to IsExtensionBitsEnabled errors, we should error", func() {
				emsClient.On("IsExtensionBitsEnabled", mock.Anything, mockExtension.ID, mockExtension.InstallationChannelID).Return(false, errors.New("ERROR"))

				err := tenant.ValidateSupportedEffect(context.Background(), mockExtension)
				So(err, ShouldNotBeNil)
			})

			Convey("when the channel/extension is not eligible for payout, we should error", func() {
				emsClient.On("IsExtensionBitsEnabled", mock.Anything, mockExtension.ID, mockExtension.InstallationChannelID).Return(false, nil)

				err := tenant.ValidateSupportedEffect(context.Background(), mockExtension)
				So(err, ShouldNotBeNil)
			})
		})
	})
}

func TestGiveBitsTenant_ValidateEffectSettings(t *testing.T) {
	Convey("given a givebits tenant", t, func() {
		paydayClient := new(payday_mock.Client)
		effectStaticConfigPoller := new(effectstaticconfig_mock.Poller)
		owlClient := new(owlclient_mock.OwlClient)
		emsClient := new(ems_mock.EMSClient)

		tenant := giveBitsTenantImpl{
			PaydayClient:             paydayClient,
			EffectStaticConfigPoller: effectStaticConfigPoller,
			OwlClient:                owlClient,
			EMSClient:                emsClient,
		}

		mockPoolRecipientsPercentage := []models.PoolRecipientWeightedShare{
			{
				ShareWeight: 1,
				ToUserID:    "channel-1",
			},
		}

		mockEffectSettings := models.EffectSettings{
			EffectType: models.GiveBits,
			Settings: &models.EffectTypeSettings{
				GiveBits: &models.GiveBitsSettings{
					PoolRecipients: mockPoolRecipientsPercentage,
				},
			},
		}

		Convey("when the effect settings are invalid", func() {
			mockEffectSettings := models.EffectSettings{
				EffectType: models.GiveBits,
				Settings: &models.EffectTypeSettings{
					GiveBits: nil,
				},
			}

			Convey("we should error", func() {
				err := tenant.ValidateEffectSettings(context.Background(), mockEffectSettings, nil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the effect settings are valid", func() {
			Convey("when a recipient is not whitelisted", func() {
				effectStaticConfigPoller.On("GetEffectStaticConfig").Return(&models.EffectStaticConfig{
					GiveBits: &models.GiveBitsStaticConfig{
						ShouldEnforceWhitelist: true,
						RecipientUserWhitelist: []string{"channel-5"},
					},
				}, nil)

				Convey("we should error", func() {
					err := tenant.ValidateEffectSettings(context.Background(), mockEffectSettings, nil)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the recipients are whitelisted", func() {
				effectStaticConfigPoller.On("GetEffectStaticConfig").Return(&models.EffectStaticConfig{
					GiveBits: &models.GiveBitsStaticConfig{
						ShouldEnforceWhitelist: true,
						RecipientUserWhitelist: []string{"channel-1"},
					},
				}, nil)

				Convey("when GetChannelInfo fails", func() {
					paydayClient.On("GetChannelInfo", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("ERROR"))

					Convey("we should error", func() {
						err := tenant.ValidateEffectSettings(context.Background(), mockEffectSettings, nil)
						So(err, ShouldNotBeNil)
					})
				})

				Convey("when GetChannelInfo returns that a recipient is NOT eligible to receive bits", func() {
					paydayClient.On("GetChannelInfo", mock.Anything, mock.Anything, mock.Anything).Return(&api.ChannelEligibleResponse{
						Eligible: false,
					}, nil)

					Convey("we should error", func() {
						err := tenant.ValidateEffectSettings(context.Background(), mockEffectSettings, nil)
						So(err, ShouldNotBeNil)
					})
				})

				Convey("when GetChannelInfo returns that all recipients are eligible to receive bits", func() {
					paydayClient.On("GetChannelInfo", mock.Anything, mock.Anything, mock.Anything).Return(&api.ChannelEligibleResponse{
						Eligible: true,
					}, nil)

					Convey("we should NOT error", func() {
						err := tenant.ValidateEffectSettings(context.Background(), mockEffectSettings, nil)
						So(err, ShouldBeNil)
					})
				})
			})
		})
	})
}

func TestGiveBitsTenant_ValidateEffect(t *testing.T) {
	Convey("given a givebits tenant", t, func() {
		effectStaticConfigPoller := new(effectstaticconfig_mock.Poller)

		tenant := giveBitsTenantImpl{
			EffectStaticConfigPoller: effectStaticConfigPoller,
		}

		mockEffect := models.Effect{
			EffectType: models.GiveBits,
			Params: models.EffectParams{
				GiveBits: &models.EffectParamsGiveBits{
					PoolableInput: models.EffectInputPoolable{
						Amount:     1,
						FromUserID: "user-1",
					},
				},
			},
		}

		Convey("when the effect params are invalid", func() {
			mockEffect = models.Effect{
				EffectType: models.GiveBits,
				Params: models.EffectParams{
					GiveBits: nil,
				},
			}

			Convey("we should error", func() {
				err := tenant.ValidateEffect(context.Background(), mockEffect)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the user attempts to use less than 1 bit, we should error", func() {
			mockEffect := models.Effect{
				EffectType: models.GiveBits,
				Params: models.EffectParams{
					GiveBits: &models.EffectParamsGiveBits{
						PoolableInput: models.EffectInputPoolable{
							Amount:     0,
							FromUserID: "user-1",
						},
					},
				},
			}

			err := tenant.ValidateEffect(context.Background(), mockEffect)
			So(err, ShouldNotBeNil)
		})

		Convey("when the effect params are valid", func() {
			Convey("when the benefactor is not whitelisted", func() {
				effectStaticConfigPoller.On("GetEffectStaticConfig").Return(&models.EffectStaticConfig{
					GiveBits: &models.GiveBitsStaticConfig{
						ShouldEnforceWhitelist:  true,
						BenefactorUserWhitelist: []string{"user-5"},
					},
				}, nil)

				Convey("we should error", func() {
					err := tenant.ValidateEffect(context.Background(), mockEffect)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when the benefactor is whitelisted", func() {
				effectStaticConfigPoller.On("GetEffectStaticConfig").Return(&models.EffectStaticConfig{
					GiveBits: &models.GiveBitsStaticConfig{
						ShouldEnforceWhitelist:  true,
						BenefactorUserWhitelist: []string{"user-1"},
					},
				}, nil)

				Convey("we should NOT error", func() {
					err := tenant.ValidateEffect(context.Background(), mockEffect)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}

func TestPoolableEffectUtils_sumAmountsForConditionParticipants(t *testing.T) {
	Convey("given a sumBitsForConditionParticipants util", t, func() {
		Convey("it sums the effect input units", func() {
			sum, err := sumBitsForConditionParticipants([]models.ConditionParticipant{
				{
					Effect: models.Effect{
						EffectType: models.GiveBits,
						Params: models.EffectParams{
							GiveBits: &models.EffectParamsGiveBits{
								PoolableInput: models.EffectInputPoolable{
									Amount:     1,
									FromUserID: "user-1",
								},
							},
						},
					},
				},
				{
					Effect: models.Effect{
						EffectType: models.GiveBits,
						Params: models.EffectParams{
							GiveBits: &models.EffectParamsGiveBits{
								PoolableInput: models.EffectInputPoolable{
									Amount:     3,
									FromUserID: "user-1",
								},
							},
						},
					},
				},
				{
					Effect: models.Effect{
						EffectType: models.GiveBits,
						Params: models.EffectParams{
							GiveBits: &models.EffectParamsGiveBits{
								PoolableInput: models.EffectInputPoolable{
									Amount:     6,
									FromUserID: "user-1",
								},
							},
						},
					},
				},
			})

			So(err, ShouldBeNil)
			So(sum, ShouldEqual, 10)
		})
	})
}

func TestPoolableEffectUtils_poolRecipientWeightedSharesToWeightedFloatShares(t *testing.T) {
	Convey("given a poolRecipientsToBitsPoolRecipients util", t, func() {
		Convey("it should convert pool recipients to structure where weight is represented as a float", func() {
			total := poolRecipientsToBitsPoolRecipients([]models.PoolRecipientWeightedShare{
				{
					ShareWeight: 1,
				},
				{
					ShareWeight: 3,
				},
				{
					ShareWeight: 5,
				},
				{
					ShareWeight: 1,
				},
			})

			So(total, ShouldResemble, []models.BitsPoolRecipientWeightedShare{
				{
					ShareWeight: decimal.NewFromFloat(1),
				},
				{
					ShareWeight: decimal.NewFromFloat(3),
				},
				{
					ShareWeight: decimal.NewFromFloat(5),
				},
				{
					ShareWeight: decimal.NewFromFloat(1),
				},
			})
		})
	})
}

func TestPoolableEffectUtils_sumRecipientWeightValues(t *testing.T) {
	Convey("given a sumRecipientWeightValues util", t, func() {
		Convey("it sums the weight values given a list of pool recipients", func() {
			total := sumRecipientWeightValues([]models.BitsPoolRecipientWeightedShare{
				{
					ShareWeight: decimal.NewFromFloat(1),
				},
				{
					ShareWeight: decimal.NewFromFloat(3),
				},
				{
					ShareWeight: decimal.NewFromFloat(5),
				},
				{
					ShareWeight: decimal.NewFromFloat(1),
				},
			})

			So(total, ShouldEqual, decimal.New(10, 0))
		})
	})
}

func TestPoolableEffectUtils_ratioToFixedRecipients(t *testing.T) {
	Convey("given a weightedToFixedRecipients util", t, func() {
		Convey("it converts ratio pool recipients to fixed units", func() {
			mockPoolRecipientsPercentage := []models.BitsPoolRecipientWeightedShare{
				{
					ShareWeight: decimal.NewFromFloat(1),
					ToUserID:    "channel-1",
				},
				{
					ShareWeight: decimal.NewFromFloat(1),
					ToUserID:    "channel-2",
				},
			}

			recipients, err := weightedToFixedRecipients(100, mockPoolRecipientsPercentage)

			So(err, ShouldBeNil)
			So(recipients, ShouldResemble, []models.BitsPoolRecipientFixedShare{
				{
					ToUserID: "channel-1",
					Amount:   50,
				},
				{
					ToUserID: "channel-2",
					Amount:   50,
				},
			})
		})

		Convey("it randomly allocates bits if share is < 1", func() {
			mockPoolRecipientsPercentage := []models.BitsPoolRecipientWeightedShare{
				{
					ShareWeight: decimal.NewFromFloat(1),
					ToUserID:    "channel-1",
				},
				{
					ShareWeight: decimal.NewFromFloat(1),
					ToUserID:    "channel-2",
				},
			}

			recipients, err := weightedToFixedRecipients(100, mockPoolRecipientsPercentage)

			So(err, ShouldBeNil)

			var sum int32 = 0

			for _, recipient := range recipients {
				sum += recipient.Amount
			}

			So(sum, ShouldEqual, 100)
		})
	})
}

func TestGiveBitsTenant_ProcessEffectRollup(t *testing.T) {
	Convey("given a givebits tenant", t, func() {
		ripleyClient := new(ripleyclient_mock.Client)
		snsClient := new(snsclient_mock.SNSClient)

		tenant := giveBitsTenantImpl{
			RipleyClient: ripleyClient,
			SNSClient:    snsClient,
		}

		Convey("when processing a condition not owned by an extension, we should error", func() {
			mockCondition := models.Condition{
				Extension: nil,
			}

			err := tenant.ProcessEffectRollup(context.Background(), mockCondition, models.ConditionRun{}, models.FinalizedEffectRollupS3Input{})
			So(err, ShouldBeNil)
		})

		Convey("when EffectRollup is nil, we should return nil", func() {
			mockEffectRollup := models.FinalizedEffectRollupS3Input{
				EffectRollup: nil,
			}

			mockCondition := models.Condition{
				Extension: &models.Extension{
					ID:                    "extension-1",
					InstallationChannelID: "channel-a",
				},
			}

			err := tenant.ProcessEffectRollup(context.Background(), mockCondition, models.ConditionRun{}, mockEffectRollup)
			So(err, ShouldBeNil)
		})

		Convey("when EffectRollup.GiveBits is nil, we should return nil", func() {
			mockEffectRollup := models.FinalizedEffectRollupS3Input{
				EffectRollup: &models.EffectRollupOutput{
					GiveBits: nil,
				},
			}

			mockCondition := models.Condition{
				Extension: &models.Extension{
					ID:                    "extension-1",
					InstallationChannelID: "channel-a",
				},
			}

			err := tenant.ProcessEffectRollup(context.Background(), mockCondition, models.ConditionRun{}, mockEffectRollup)
			So(err, ShouldBeNil)
		})

		Convey("when ripley errors, we should error", func() {
			ripleyClient.On("GetPartnerType", mock.Anything, "a").Return(ripley.UnknownPartnerType, errors.New("ERROR"))
			snsClient.On("PublishGringottsRevenueEvent", mock.Anything, mock.Anything).Return(errors.New("ERROR"))

			mockEffectRollup := models.FinalizedEffectRollupS3Input{
				EffectRollup: &models.EffectRollupOutput{
					GiveBits: &models.EffectRollupOutputGiveBits{
						Recipients: []models.BitsPoolRecipientFixedShare{
							{
								Amount:   10,
								ToUserID: "a",
							},
						},
					},
				},
			}

			mockCondition := models.Condition{
				Extension: &models.Extension{
					ID:                    "extension-1",
					InstallationChannelID: "channel-a",
				},
			}

			err := tenant.ProcessEffectRollup(context.Background(), mockCondition, models.ConditionRun{}, mockEffectRollup)
			So(err, ShouldNotBeNil)
		})

		Convey("when ripley returns a non partner/affiliate, we should error", func() {
			ripleyClient.On("GetPartnerType", mock.Anything, "a").Return(ripley.UnknownPartnerType, nil)
			snsClient.On("PublishGringottsRevenueEvent", mock.Anything, mock.Anything).Return(errors.New("ERROR"))

			mockEffectRollup := models.FinalizedEffectRollupS3Input{
				EffectRollup: &models.EffectRollupOutput{
					GiveBits: &models.EffectRollupOutputGiveBits{
						Recipients: []models.BitsPoolRecipientFixedShare{
							{
								Amount:   10,
								ToUserID: "a",
							},
						},
					},
				},
			}

			mockCondition := models.Condition{
				Extension: &models.Extension{
					ID:                    "extension-1",
					InstallationChannelID: "channel-a",
				},
			}

			err := tenant.ProcessEffectRollup(context.Background(), mockCondition, models.ConditionRun{}, mockEffectRollup)
			So(err, ShouldNotBeNil)
		})

		Convey("when SNS errors, we should error", func() {
			ripleyClient.On("GetPartnerType", mock.Anything, "a").Return(ripley.AffiliatePartnerType, nil)
			snsClient.On("PublishGringottsRevenueEvent", mock.Anything, mock.Anything).Return(errors.New("ERROR"))

			mockEffectRollup := models.FinalizedEffectRollupS3Input{
				EffectRollup: &models.EffectRollupOutput{
					GiveBits: &models.EffectRollupOutputGiveBits{
						Recipients: []models.BitsPoolRecipientFixedShare{
							{
								Amount:   10,
								ToUserID: "a",
							},
						},
					},
				},
			}

			mockCondition := models.Condition{
				Extension: &models.Extension{
					ID:                    "extension-1",
					InstallationChannelID: "channel-a",
				},
			}

			err := tenant.ProcessEffectRollup(context.Background(), mockCondition, models.ConditionRun{}, mockEffectRollup)
			So(err, ShouldNotBeNil)
		})

		Convey("when processing a condition owned by an extension", func() {
			Convey("we should send an SNS message for the developer and broadcaster", func() {
				snsClient.On("PublishGringottsRevenueEvent", mock.Anything, mock.Anything).Return(nil)
				ripleyClient.On("GetPartnerType", mock.Anything, "a").Return(ripley.AffiliatePartnerType, nil)

				mockEffectRollup := models.FinalizedEffectRollupS3Input{
					EffectRollup: &models.EffectRollupOutput{
						GiveBits: &models.EffectRollupOutputGiveBits{
							Recipients: []models.BitsPoolRecipientFixedShare{
								{
									Amount:   10,
									ToUserID: "a",
								},
								{
									Amount:                  2,
									ToUserID:                "some-developer",
									ForExtensionDevRevShare: true,
								},
							},
						},
					},
				}

				mockCondition := models.Condition{
					Extension: &models.Extension{
						ID:                    "extension-1",
						InstallationChannelID: "channel-a",
					},
				}

				mockConditionRun := models.ConditionRun{
					ConditionRunID: "kappa123",
					CompletedAt:    time.Now(),
				}

				err := tenant.ProcessEffectRollup(context.Background(), mockCondition, mockConditionRun, mockEffectRollup)
				So(err, ShouldBeNil)
				So(snsClient.Calls[0].Arguments.Get(1), ShouldResemble, gringotts.RevenueEventMessage{
					ChannelId:       "a",
					TimeOfEvent:     mockConditionRun.CompletedAt,
					TransactionId:   fmt.Sprintf("%s.%s", mockConditionRun.ConditionRunID, "a"),
					TransactionType: bitsUsedTransactionType,
					RevenueSource:   fmt.Sprintf("%s:%s", bitsOnExtensionsRevenueSourcePrefix, mockCondition.Extension.ID),
					Revenue:         10,
					ParticipantRole: affiliateParticipantRole,
					SKU:             bitsChallengeSku,
				})

				So(snsClient.Calls[1].Arguments.Get(1), ShouldResemble, gringotts.RevenueEventMessage{
					ChannelId:       "some-developer",
					TimeOfEvent:     mockConditionRun.CompletedAt,
					TransactionId:   fmt.Sprintf("%s.%s", mockConditionRun.ConditionRunID, "some-developer"),
					TransactionType: bitsUsedTransactionType,
					RevenueSource:   fmt.Sprintf("%s:%s", bitsOnExtensionsRevenueSourcePrefix, mockCondition.Extension.InstallationChannelID),
					Revenue:         2,
					ParticipantRole: extensionDeveloperParticipantRole,
					SKU:             bitsChallengeSku,
				})
			})
		})
	})
}
