package givebits

import (
	"context"
	"time"

	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/hystrix/commands"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/backend/utils/hystrix"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/gofrs/uuid"
	"github.com/golang/protobuf/ptypes"
	"github.com/joomcode/errorx"
	errorpkg "github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

var (
	// Pachinko (which tracks Bits balances) requires a expiration date for holds. We provide one, which acts as a backup for our ttl process.
	// If something goes wrong with Pagle (e.g. items that should be satisfied end up in the DLQ), our on-call
	// would have three days to rectify before the hold is released by Pachinko.
	bitsExpirationBuffer = time.Hour * 72 // 3 days
)

func (t *giveBitsTenantImpl) CreateTransaction(ctx context.Context, effect models.Effect, ttlSeconds int32, condition models.Condition) (holdID string, validationErr error, err error) {
	params, err := getGiveBitsParams(effect)

	if err != nil {
		return "", nil, err
	}

	holdUUID, err := uuid.NewV4()

	if err != nil {
		return "", nil, errorx.Decorate(err, "failed to create holdID when creating payday hold")
	}

	holdIDString := holdUUID.String()
	expiresAt := time.Now().Add(bitsExpirationBuffer + (time.Second * time.Duration(ttlSeconds)))
	pbExpiresAt, err := ptypes.TimestampProto(expiresAt)

	if err != nil {
		return "", nil, errorx.Decorate(err, "failed to create protobuf timestamp when creating Payday hold")
	}

	var resp *paydayrpc.CreateHoldResp

	err = hystrix.CallTwirpWithHystrix(commands.PaydayRPCCreateHoldCommand, hystrix.HystrixDo, func() error {
		var err error

		resp, err = t.PaydayTwirpClient.CreateHold(ctx, &paydayrpc.CreateHoldReq{
			UserId:    params.PoolableInput.FromUserID,
			Amount:    int64(params.PoolableInput.Amount),
			HoldId:    holdIDString,
			ExpiresAt: pbExpiresAt,
		})

		return err
	})

	if err != nil {
		var paydayErr paydayrpc.ClientError
		parseErr := paydayrpc.ParseClientError(errorpkg.Cause(err), &paydayErr)
		if parseErr != nil {
			return "", nil, parseErr
		}

		if paydayErr.ErrorCode == paydayrpc.ErrCodeInsufficientBitsBalance {
			return "", errors.InsufficientBitsBalance.New("PaydayClient.CreateHold returned insufficient balance error"), nil
		}

		logrus.WithFields(logrus.Fields{
			"holdId": holdIDString,
			"userId": params.PoolableInput.FromUserID,
			"amount": params.PoolableInput.Amount,
		}).WithError(err).Info("failed to create a hold in Payday")
		return "", nil, err
	} else if resp == nil {
		return "", nil, errors.InternalError.New("PaydayClient.CreateHold returned nil response")
	}

	return resp.Hold.HoldId, nil, nil
}

func (t *giveBitsTenantImpl) FinalizeTransaction(ctx context.Context, effect models.Effect, effectSettings models.EffectSettings, condition models.Condition) error {
	params, err := getGiveBitsParams(effect)

	if err != nil {
		return err
	}

	transactionType := getPaydayTransactionType(condition)
	bitsUsageAttribution := makeBitsUsageAttribution(params, condition)

	err = hystrix.CallTwirpWithHystrix(commands.PaydayRPCFinalizeHoldCommand, hystrix.HystrixDo, func() error {
		_, reqError := t.PaydayTwirpClient.FinalizeHold(ctx, &paydayrpc.FinalizeHoldReq{
			UserId:                params.PoolableInput.FromUserID,
			HoldId:                effect.TransactionID,
			TransactionType:       transactionType,
			IgnorePayout:          true,
			BitsUsageAttributions: bitsUsageAttribution,
		})

		return reqError
	})

	if err != nil {
		logrus.WithFields(logrus.Fields{
			"holdId": effect.TransactionID,
			"userId": params.PoolableInput.FromUserID,
			"amount": params.PoolableInput.Amount,
		}).WithError(err).Error("failed to finalize hold in Payday")
		return err
	}

	return nil
}

func (t *giveBitsTenantImpl) ReleaseTransaction(ctx context.Context, effect models.Effect) error {
	params, err := getGiveBitsParams(effect)

	if err != nil {
		return err
	}

	err = hystrix.CallTwirpWithHystrix(commands.PaydayRPCReleaseHoldCommand, hystrix.HystrixDo, func() error {
		_, reqError := t.PaydayTwirpClient.ReleaseHold(ctx, &paydayrpc.ReleaseHoldReq{
			UserId: params.PoolableInput.FromUserID,
			HoldId: effect.TransactionID,
		})

		return reqError
	})

	if err != nil {
		logrus.WithFields(logrus.Fields{
			"holdId": effect.TransactionID,
			"userId": params.PoolableInput.FromUserID,
			"amount": params.PoolableInput.Amount,
		}).WithError(err).Error("failed to release hold in Payday")
		return err
	}

	return nil
}

func makeBitsUsageAttribution(params models.EffectParamsGiveBits, condition models.Condition) map[string]*paydayrpc.BitsUsageAttribution {
	// Attribute 100% of bits usage to channel where extension is installed
	if condition.Extension != nil {
		return map[string]*paydayrpc.BitsUsageAttribution{
			condition.Extension.InstallationChannelID: {
				BitsRevenueAttribution: 0, // pagle handles payout for bits effects
				RewardAttribution:      int64(params.PoolableInput.Amount),
			},
		}
	}

	// TODO: For now attributing all bits usage to the owner of the condition. This might not always be correct, so we should
	// circle back to this logic if it becomes insufficient.
	return map[string]*paydayrpc.BitsUsageAttribution{
		condition.OwnerID: {
			BitsRevenueAttribution: 0, // pagle handles payout for bits effects
			RewardAttribution:      int64(params.PoolableInput.Amount),
		},
	}
}
