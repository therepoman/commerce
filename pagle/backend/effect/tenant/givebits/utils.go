package givebits

import (
	"context"
	"math/rand"
	"time"

	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/models"
	"github.com/shopspring/decimal"
)

func (t *giveBitsTenantImpl) getExtensionCreatorIDForRevShare(ctx context.Context, extensionID string) (shouldRevShareWithExtension bool, extensionOwnerID string, err error) {
	if strings.Blank(extensionID) {
		return false, "", nil
	}

	bitsStaticConfig, err := t.getGiveBitsStaticConfig()

	if err != nil {
		return false, "", err
	}

	for _, blacklistedExtension := range bitsStaticConfig.ExtensionRevShareBlacklist {
		if blacklistedExtension == extensionID {
			return false, "", nil
		}
	}

	extensionOwnerID, err = t.OwlClient.GetOwner(ctx, extensionID)

	if err != nil {
		return false, "", err
	}

	return true, extensionOwnerID, nil
}

func (t *giveBitsTenantImpl) addExtensionDeveloperAsPoolRecipient(poolRecipients []models.BitsPoolRecipientWeightedShare, extensionOwnerID string) ([]models.BitsPoolRecipientWeightedShare, error) {
	bitsStaticConfig, err := t.getGiveBitsStaticConfig()

	if err != nil {
		return nil, err
	}

	if bitsStaticConfig.ExtensionRevSharePercentage <= 0 || bitsStaticConfig.ExtensionRevSharePercentage > 1 {
		return nil, errors.IllegalArgument.New("extension_rev_share_percentage out of range")
	}

	// Sum of pool weights prior to adding the developer share
	totalWeight := sumRecipientWeightValues(poolRecipients)
	extensionRevSharePercentageF := decimal.NewFromFloat(bitsStaticConfig.ExtensionRevSharePercentage)
	percentageShareAfterDeveloperCutF := decimal.New(1, 0).Sub(extensionRevSharePercentageF)

	// Calculates a weight that will give the developer a portion of the pool equal to ExtensionRevSharePercentage
	// Calculation w/o decimal.Decimal obscuring the logic would look like:
	//   (totalWeight - (totalWeight * percentageShareAfterDeveloperCut)) / percentageShareAfterDeveloperCut
	developerShareWeightF := totalWeight.Sub(totalWeight.Mul(percentageShareAfterDeveloperCutF)).Div(percentageShareAfterDeveloperCutF)

	poolRecipients = append(poolRecipients, models.BitsPoolRecipientWeightedShare{
		ShareWeight:             developerShareWeightF,
		ToUserID:                extensionOwnerID,
		ForExtensionDevRevShare: true,
	})

	return poolRecipients, nil
}

func (t *giveBitsTenantImpl) arePoolRecipientsWhitelisted(poolRecipientOwners map[string]bool) (bool, error) {
	giveBitsConfig, err := t.getGiveBitsStaticConfig()

	if err != nil {
		return false, err
	}

	if !giveBitsConfig.ShouldEnforceWhitelist || len(giveBitsConfig.RecipientUserWhitelist) == 0 {
		return true, nil
	}

	for userID := range poolRecipientOwners {
		var isRecipientWhitelisted bool

		for _, whitelistedUserID := range giveBitsConfig.RecipientUserWhitelist {
			if userID == whitelistedUserID {
				isRecipientWhitelisted = true
				break
			}
		}

		if !isRecipientWhitelisted {
			// At least one recipient is not whitelisted
			return false, nil
		}
	}

	return true, nil
}

func (t *giveBitsTenantImpl) isBitsBenefactorWhitelisted(benefactorUserID string) (bool, error) {
	giveBitsConfig, err := t.getGiveBitsStaticConfig()

	if err != nil {
		return false, err
	}

	if !giveBitsConfig.ShouldEnforceWhitelist || len(giveBitsConfig.BenefactorUserWhitelist) == 0 {
		return true, nil
	}

	for _, whitelistedUserID := range giveBitsConfig.BenefactorUserWhitelist {
		if benefactorUserID == whitelistedUserID {
			return true, nil
		}
	}

	return false, nil
}

func (t *giveBitsTenantImpl) getGiveBitsStaticConfig() (models.GiveBitsStaticConfig, error) {
	effectStaticConfig, err := t.EffectStaticConfigPoller.GetEffectStaticConfig()
	if err != nil {
		return models.GiveBitsStaticConfig{}, err
	}

	if effectStaticConfig == nil || effectStaticConfig.GiveBits == nil {
		return models.GiveBitsStaticConfig{}, errors.IllegalState.New("unable to get GiveBits static config")
	}

	return *effectStaticConfig.GiveBits, nil
}

func getPaydayTransactionType(condition models.Condition) paydayrpc.TransactionType {
	transactionType := paydayrpc.TransactionType_GiveBitsToBroadcasterChallenge

	if condition.Extension != nil {
		transactionType = paydayrpc.TransactionType_UseBitsInExtensionChallenge
	}

	return transactionType
}

func getPoolRecipientsFromSettings(effectSettings models.EffectSettings) ([]models.PoolRecipientWeightedShare, error) {
	giveBitsSettings, err := getGiveBitsSettings(effectSettings)

	if err != nil {
		return []models.PoolRecipientWeightedShare{}, err
	}

	return giveBitsSettings.PoolRecipients, nil
}

func makeOwnerIDMapFromPoolRecipients(poolRecipients []models.PoolRecipientWeightedShare) map[string]bool {
	poolRecipientIDs := map[string]bool{}
	for _, poolRecipient := range poolRecipients {
		poolRecipientIDs[poolRecipient.ToUserID] = true
	}

	return poolRecipientIDs
}

func getGiveBitsSettings(effectSettings models.EffectSettings) (models.GiveBitsSettings, error) {
	if effectSettings.Settings == nil || effectSettings.Settings.GiveBits == nil {
		return models.GiveBitsSettings{}, errors.IllegalArgument.New("unable to parse GiveBits settings")
	}

	return *effectSettings.Settings.GiveBits, nil
}

func getGiveBitsParams(effect models.Effect) (models.EffectParamsGiveBits, error) {
	if effect.Params.GiveBits == nil {
		return models.EffectParamsGiveBits{}, errors.IllegalArgument.New("unable to get GiveBits params from effect")
	}

	return *effect.Params.GiveBits, nil
}

func getPoolableEffectParams(effect models.Effect) (models.EffectInputPoolable, error) {
	if effect.Params.GiveBits == nil {
		return models.EffectInputPoolable{}, errors.IllegalArgument.New("unable to get GiveBits params from effect")
	}

	return effect.Params.GiveBits.PoolableInput, nil
}

// Sums total units in pool
func sumBitsForConditionParticipants(conditionParticipants []models.ConditionParticipant) (int32, error) {
	var sum int32 = 0

	for _, conditionParticipant := range conditionParticipants {
		params, err := getPoolableEffectParams(conditionParticipant.Effect)

		if err != nil {
			return 0, err
		}

		sum += params.Amount
	}

	return sum, nil
}

func poolRecipientsToBitsPoolRecipients(poolRecipients []models.PoolRecipientWeightedShare) []models.BitsPoolRecipientWeightedShare {
	var floatShares []models.BitsPoolRecipientWeightedShare

	for _, recipient := range poolRecipients {
		floatShares = append(floatShares, models.BitsPoolRecipientWeightedShare{
			ShareWeight: decimal.New(recipient.ShareWeight, 0),
			ToUserID:    recipient.ToUserID,
		})
	}

	return floatShares
}

// Sums the total share weight of all pool recipients
func sumRecipientWeightValues(poolRecipients []models.BitsPoolRecipientWeightedShare) decimal.Decimal {
	totalShareWeight := decimal.New(0, 0)

	for _, recipient := range poolRecipients {
		totalShareWeight = totalShareWeight.Add(recipient.ShareWeight)
	}

	return totalShareWeight
}

// Converts effect pool recipients from ratio (e.g. channel-1: 1, channel-2: 1) to Bits
// (e.g channel-1: 50 Bits, channel-2: 50 Bits)
func weightedToFixedRecipients(totalAmountToDistribute int32, bitsPoolRecipients []models.BitsPoolRecipientWeightedShare) ([]models.BitsPoolRecipientFixedShare, error) {
	var fixedRecipients []models.BitsPoolRecipientFixedShare
	var amountDistributed int32

	totalShareWeight := sumRecipientWeightValues(bitsPoolRecipients)
	totalAmountToDistributeF := decimal.New(int64(totalAmountToDistribute), 0)

	for _, recipient := range bitsPoolRecipients {
		percentShare := recipient.ShareWeight.Div(totalShareWeight)
		fixedShare := totalAmountToDistributeF.Mul(percentShare)
		// What should this be? rounding is not correct. Maybe need to set rounding mode too?
		fixedShareRounded := int32(fixedShare.IntPart())
		amountDistributed += fixedShareRounded

		fixedRecipients = append(fixedRecipients, models.BitsPoolRecipientFixedShare{
			ToUserID:                recipient.ToUserID,
			ForExtensionDevRevShare: recipient.ForExtensionDevRevShare,
			Amount:                  fixedShareRounded,
		})
	}

	if len(fixedRecipients) == 0 {
		return nil, errors.InternalError.New("error converting share weights to fixed amounts")
	}

	if amountDistributed > totalAmountToDistribute {
		return nil, errors.InternalError.New("calculated fixed recipient exceeds total units to distribute")
	} else if amountDistributed < totalAmountToDistribute {
		difference := totalAmountToDistribute - amountDistributed
		rand.Seed(time.Now().UnixNano())
		randomAssignee := rand.Intn(len(fixedRecipients))
		fixedRecipients[randomAssignee].Amount += difference
	}

	return fixedRecipients, nil
}
