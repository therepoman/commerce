package tenant

import (
	"context"

	"code.justin.tv/commerce/pagle/backend/models"
)

type Tenant interface {
	// Creates a hold on an effect
	CreateTransaction(ctx context.Context, effect models.Effect, ttlSeconds int32, condition models.Condition) (holdID string, validationErr, err error)
	// Finalizes a hold on an effect
	FinalizeTransaction(ctx context.Context, effect models.Effect, effectSettings models.EffectSettings, condition models.Condition) error
	// Releases a hold on an effect
	ReleaseTransaction(ctx context.Context, effect models.Effect) error
	// Applies (optional) processing to a list of condition participants, given the condition run's effect settings
	PreProcessConditionParticipants(ctx context.Context, conditionParticipants []models.ConditionParticipant, effectSettings models.EffectSettings, condition models.Condition) ([]models.PreProcessedConditionParticipant, *models.EffectRollupOutput, error)
	// Whether the condition can support the effect type.
	ValidateSupportedEffect(ctx context.Context, extension *models.Extension) error
	// Applies (optional) validation on a list of condition participants, given the condition run's effect settings. Condition participants
	// filtered out should have their effects released upstream
	ValidateAndFilterConditionParticipants(conditionParticipants []models.ConditionParticipant, effectSettings models.EffectSettings) ([]models.ConditionParticipant, []models.ConditionParticipant, error)
	// Returns an error if effect settings are invalid
	ValidateEffectSettings(ctx context.Context, effectSettings models.EffectSettings, extension *models.Extension) error
	// Returns an error if effect is invalid
	ValidateEffect(ctx context.Context, effect models.Effect) error
	// Higher order function. Returns a function that is responsible for providing details about how to increment/decrement
	// condition aggregates for a particular effect.
	MakeEffectAggregateGetter(effect models.Effect) models.EffectAggregateGetter
	// When condition participants are processed, we can optionally store some aggregate facts about the associated effects.
	// This method is provided those facts, and does optional, arbitrary processing on them. As an example, in the Bits case,
	// we use this step to post creator payout, as it is the only point time were we know the totality of the Bits
	// contributed.
	ProcessEffectRollup(ctx context.Context, condition models.Condition, conditionRun models.ConditionRun, effectRollup models.FinalizedEffectRollupS3Input) error
}
