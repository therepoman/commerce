package pubsub

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	pubclient "code.justin.tv/chat/pubsub-go-pubclient/client"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/backend/stats"
)

const (
	pubsubTimeout = 5 * time.Second

	publishLatencyStatName = "pubsub_publish_latency"
	publishErrorStatName   = "pubsub_publish_error"
)

type PubsubClient interface {
	PublishConditionCreate(condition models.Condition) error
	PublishConditionStateUpdate(updatedCondition models.Condition) error
	PublishConditionSatisfied(condition models.Condition) error
	PublishConditionParticipantCreate(conditionParticipant models.ConditionParticipant) error
	PublishConditionParticipantEndStateUpdate(updatedConditionParticipant models.ConditionParticipant) error
}

func NewPubsubClient() PubsubClient {
	return &pubsubClientImpl{}
}

type pubsubClientImpl struct {
	Client  pubclient.PubClient `inject:""`
	Statter stats.Statter       `inject:""`
}

func (p *pubsubClientImpl) PublishConditionCreate(condition models.Condition) error {
	topics := getConditionPubsubTopics(condition)
	data := models.ConditionToPubsubEventDataCondition(condition)

	return p.publish(topics, models.PubsubEventTypeConditionCreate, data)
}

func (p *pubsubClientImpl) PublishConditionStateUpdate(updatedCondition models.Condition) error {
	var pubsubEventType models.PubsubEventType

	switch updatedCondition.ConditionState {
	case models.ConditionActive:
		pubsubEventType = models.PubsubEventTypeConditionActive
	case models.ConditionCanceled:
		pubsubEventType = models.PubsubEventTypeConditionCanceled
	case models.ConditionExpired:
		pubsubEventType = models.PubsubEventTypeConditionExpired
	case models.ConditionInactive:
		pubsubEventType = models.PubsubEventTypeConditionInactive
	default:
		// Condition state does not have a corresponding pubsub event type
		return nil
	}

	topics := getConditionPubsubTopics(updatedCondition)
	data := models.ConditionToPubsubEventDataCondition(updatedCondition)

	return p.publish(topics, pubsubEventType, data)
}

func (p *pubsubClientImpl) PublishConditionSatisfied(condition models.Condition) error {
	topics := getConditionPubsubTopics(condition)
	data := models.ConditionToPubsubEventDataCondition(condition)

	return p.publish(topics, models.PubsubEventTypeConditionSatisfied, data)
}

func getConditionPubsubTopics(condition models.Condition) []string {
	return []string{
		// Pubsub topic for any public subscriber
		// Format: conditions.${domain}.${condition-owner-id}
		fmt.Sprintf("%s.%s.%s", models.PubsubTopicConditions, condition.Domain, condition.OwnerID),
	}
}

func (p *pubsubClientImpl) PublishConditionParticipantCreate(conditionParticipant models.ConditionParticipant) error {
	topics := getConditionParticipantPubsubTopics(conditionParticipant)
	data := models.ConditionParticipantToPubsubEventDataConditionParticipant(conditionParticipant)

	return p.publish(topics, models.PubsubEventTypeConditionParticipantCreate, data)
}

func (p *pubsubClientImpl) PublishConditionParticipantEndStateUpdate(updatedConditionParticipant models.ConditionParticipant) error {
	var pubsubEventType models.PubsubEventType

	switch updatedConditionParticipant.ConditionParticipantEndState {
	case models.ConditionParticipantEndStatePendingCompletion:
		pubsubEventType = models.PubsubEventTypeConditionParticipantPendingCompletion
	case models.ConditionParticipantEndStateConditionCanceled:
		pubsubEventType = models.PubsubEventTypeConditionParticipantConditionCanceled
	case models.ConditionParticipantEndStateConditionExpired:
		pubsubEventType = models.PubsubEventTypeConditionParticipantConditionExpired
	case models.ConditionParticipantEndStateSatisfied:
		pubsubEventType = models.PubsubEventTypeConditionParticipantSatisfied
	case models.ConditionParticipantEndStateExpired:
		pubsubEventType = models.PubsubEventTypeConditionParticipantExpired
	case models.ConditionParticipantEndStateCanceled:
		pubsubEventType = models.PubsubEventTypeConditionParticipantCanceled
	default:
		// Condition participant end state does not have a corresponding pubsub event type
		return nil
	}

	topics := getConditionParticipantPubsubTopics(updatedConditionParticipant)
	data := models.ConditionParticipantToPubsubEventDataConditionParticipant(updatedConditionParticipant)

	return p.publish(topics, pubsubEventType, data)
}

func getConditionParticipantPubsubTopics(conditionParticipant models.ConditionParticipant) []string {
	return []string{
		// Pubsub topic for condition participant owner
		// Format: condition-participants.${participant-owner-id}.${domain}.${condition-owner-id}
		// Note that ${participant-owner-id} is the auth'd subscriber's tuid (the intended recipient)
		fmt.Sprintf("%s.%s.%s.%s", models.PubsubTopicConditionParticipants, conditionParticipant.OwnerID, conditionParticipant.Domain, conditionParticipant.ConditionOwnerID),
		// Pubsub topic for condition participant condition owner
		// Format: condition-participants.${condition-owner-id}.${domain}.${condition-owner-id}
		// Note that ${condition-owner-id} is the auth'd condition owner's tuid (the intended recipient)
		fmt.Sprintf("%s.%s.%s.%s", models.PubsubTopicConditionParticipants, conditionParticipant.ConditionOwnerID, conditionParticipant.Domain, conditionParticipant.ConditionOwnerID),
	}
}

func (p *pubsubClientImpl) statPublishError() {
	p.Statter.Inc(publishErrorStatName, 1, 1)
}

func (p *pubsubClientImpl) publish(topics []string, pubsubEventType models.PubsubEventType, pubsubEventData interface{}) error {
	startTime := time.Now()

	defer func() {
		p.Statter.TimingDuration(publishLatencyStatName, time.Since(startTime), 1)
	}()

	ctx, cancel := context.WithTimeout(context.Background(), pubsubTimeout)
	defer cancel()

	logFields := log.Fields{
		"pubsubEventType": string(pubsubEventType),
		"pubsubEventData": pubsubEventData,
	}

	pubsubEventBytes, err := json.Marshal(models.PubsubEvent{
		Type: pubsubEventType,
		Data: pubsubEventData,
	})
	if err != nil {
		p.statPublishError()
		log.WithFields(logFields).WithError(err).Error("failed to marshal pubsub event for publishing")
		return err
	}

	err = p.Client.Publish(ctx, topics, string(pubsubEventBytes), nil)
	if err != nil {
		p.statPublishError()
		log.WithFields(logFields).WithError(err).Error("failed to publish pubsub event")
		return err
	}

	return nil
}
