package s3

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/aws/s3"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/backend/stats"
	"code.justin.tv/commerce/pagle/config"
)

const (
	storeFinalizedConditionParticipantsLatencyStatName = "s3_store_finalized_condition_participants_latency"
	storeFinalizedConditionParticipantsErrorStatName   = "s3_store_finalized_condition_participants_error"
	getFinalizedConditionParticipantsLatencyStatName   = "s3_get_finalized_condition_participants_latency"
	getFinalizedConditionParticipantsErrorStatName     = "s3_store_finalized_condition_participants_error"
	getEffectStaticConfigLatencyStatName               = "s3_get_effect_static_config_latency"
	getEffectStaticConfigErrorStatName                 = "s3_get_effect_static_config_error"
)

type S3Client interface {
	StoreFinalizedConditionRun(ctx context.Context, conditionID, conditionRunID string, finalizedConditionRun models.FinalizedConditionRunS3Input) error
	GetFinalizedConditionRun(ctx context.Context, conditionID, conditionRunID string) (models.FinalizedConditionRunS3Input, error)
	GetEffectStaticConfig(ctx context.Context) (*models.EffectStaticConfig, error)
}

func NewS3Client() S3Client {
	return &s3ClientImpl{}
}

type s3ClientImpl struct {
	S3      s3.Client      `inject:""`
	Config  *config.Config `inject:""`
	Statter stats.Statter  `inject:""`
}

func (s *s3ClientImpl) statStoreFinalizedConditionParticipantsError() {
	s.Statter.Inc(storeFinalizedConditionParticipantsErrorStatName, 1, 1)
}

func (s *s3ClientImpl) StoreFinalizedConditionRun(ctx context.Context, conditionID, conditionRunID string, finalizedConditionRun models.FinalizedConditionRunS3Input) error {
	startTime := time.Now()

	defer func() {
		s.Statter.TimingDuration(storeFinalizedConditionParticipantsLatencyStatName, time.Since(startTime), 1)
	}()

	s3Key := conditionParticipantEffectsS3Key(conditionID, conditionRunID)

	data, err := json.Marshal(finalizedConditionRun)
	if err != nil {
		s.statStoreFinalizedConditionParticipantsError()
		return err
	}

	err = s.S3.PutFile(ctx, s.Config.S3.FinalizedConditionParticipantsBucket, s3Key, bytes.NewReader(data))
	if err != nil {
		s.statStoreFinalizedConditionParticipantsError()
		return err
	}

	return nil
}

func (s *s3ClientImpl) statGetFinalizedConditionRunError() {
	s.Statter.Inc(getFinalizedConditionParticipantsErrorStatName, 1, 1)
}

func (s *s3ClientImpl) GetFinalizedConditionRun(ctx context.Context, conditionID, conditionRunID string) (models.FinalizedConditionRunS3Input, error) {
	startTime := time.Now()

	defer func() {
		s.Statter.TimingDuration(getFinalizedConditionParticipantsLatencyStatName, time.Since(startTime), 1)
	}()

	s3Key := conditionParticipantEffectsS3Key(conditionID, conditionRunID)

	res, err := s.S3.GetFile(ctx, s.Config.S3.FinalizedConditionParticipantsBucket, s3Key)
	if err != nil || !res.Exists {
		s.statGetFinalizedConditionRunError()
		return models.FinalizedConditionRunS3Input{}, err
	}

	var finalizedConditionParticipants models.FinalizedConditionRunS3Input
	err = json.Unmarshal(res.File, &finalizedConditionParticipants)
	if err != nil {
		s.statGetFinalizedConditionRunError()
		return models.FinalizedConditionRunS3Input{}, err
	}

	return finalizedConditionParticipants, nil
}

func conditionParticipantEffectsS3Key(conditionID string, conditionRunID string) string {
	return fmt.Sprintf("%s/%s", conditionID, conditionRunID)
}

func (s *s3ClientImpl) statGetEffectStaticConfigError() {
	s.Statter.Inc(getEffectStaticConfigErrorStatName, 1, 1)
}

func (s *s3ClientImpl) GetEffectStaticConfig(ctx context.Context) (*models.EffectStaticConfig, error) {
	startTime := time.Now()

	defer func() {
		s.Statter.TimingDuration(getEffectStaticConfigLatencyStatName, time.Since(startTime), 1)
	}()

	res, err := s.S3.GetFile(ctx, s.Config.S3.EffectStaticConfigBucket, "effect-static-config.json")
	if err != nil || !res.Exists {
		s.statGetEffectStaticConfigError()
		return nil, err
	}

	var config *models.EffectStaticConfig
	err = json.Unmarshal(res.File, &config)
	if err != nil {
		s.statGetEffectStaticConfigError()
		return nil, err
	}

	return config, nil
}
