package backend

import (
	"context"
	"io"
	"net/http"
	"time"

	"code.justin.tv/commerce/pagle/backend/ripley"

	conditioncompleter "code.justin.tv/commerce/pagle/backend/condition/complete"
	conditioncreator "code.justin.tv/commerce/pagle/backend/condition/create"
	conditiongetter "code.justin.tv/commerce/pagle/backend/condition/get"
	conditionparticipantcompleter "code.justin.tv/commerce/pagle/backend/conditionparticipant/complete"
	conditionparticipantcreator "code.justin.tv/commerce/pagle/backend/conditionparticipant/create"
	conditionparticipantgetter "code.justin.tv/commerce/pagle/backend/conditionparticipant/get"
	conditionruncompleter "code.justin.tv/commerce/pagle/backend/conditionrun/complete"
	conditionruncreator "code.justin.tv/commerce/pagle/backend/conditionrun/create"
	conditionrungetter "code.justin.tv/commerce/pagle/backend/conditionrun/get"
	emsPagle "code.justin.tv/commerce/pagle/backend/ems"
	ems "code.justin.tv/gds/gds/extensions/ems/client"
	riptwirp "code.justin.tv/revenue/ripley/rpc"

	"code.justin.tv/chat/golibs/graceful"
	pubclient "code.justin.tv/chat/pubsub-go-pubclient/client"
	"code.justin.tv/commerce/gogogadget/aws/s3"
	"code.justin.tv/commerce/gogogadget/aws/sns"
	log "code.justin.tv/commerce/logrus"
	conditionapi "code.justin.tv/commerce/pagle/backend/api/condition"
	conditionparticipantapi "code.justin.tv/commerce/pagle/backend/api/conditionparticipant"
	"code.justin.tv/commerce/pagle/backend/cache/getconditionparticipants"
	"code.justin.tv/commerce/pagle/backend/cache/getconditions"
	"code.justin.tv/commerce/pagle/backend/condition/effecttotals"
	conditiondynamo "code.justin.tv/commerce/pagle/backend/dynamo/condition"
	conditionaggregatesdynamo "code.justin.tv/commerce/pagle/backend/dynamo/conditionaggregates"
	conditionparticipantdynamo "code.justin.tv/commerce/pagle/backend/dynamo/conditionparticipant"
	conditionrundynamo "code.justin.tv/commerce/pagle/backend/dynamo/conditionrun"
	transactiondynamo "code.justin.tv/commerce/pagle/backend/dynamo/transaction"
	"code.justin.tv/commerce/pagle/backend/ecs"
	ecspoller "code.justin.tv/commerce/pagle/backend/ecs/poller"
	"code.justin.tv/commerce/pagle/backend/effect"
	"code.justin.tv/commerce/pagle/backend/effect/tenant/givebits"
	"code.justin.tv/commerce/pagle/backend/effectstaticconfig"
	hystrixStats "code.justin.tv/commerce/pagle/backend/hystrix/stats"
	owlPagle "code.justin.tv/commerce/pagle/backend/owl"
	"code.justin.tv/commerce/pagle/backend/pubsub"
	"code.justin.tv/commerce/pagle/backend/redis"
	s3Pagle "code.justin.tv/commerce/pagle/backend/s3"
	"code.justin.tv/commerce/pagle/backend/server"
	sfnPagle "code.justin.tv/commerce/pagle/backend/sfn"
	snsPagle "code.justin.tv/commerce/pagle/backend/sns"
	"code.justin.tv/commerce/pagle/backend/stats"
	"code.justin.tv/commerce/pagle/config"
	pagle "code.justin.tv/commerce/pagle/rpc"
	payday "code.justin.tv/commerce/payday/client"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/splatter"
	"code.justin.tv/foundation/twitchclient"
	owl "code.justin.tv/web/owl/client"
	metricCollector "github.com/afex/hystrix-go/hystrix/metric_collector"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/facebookgo/inject"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
)

const (
	localhostServerEndpoint = "http://localhost:8000"
	cloudwatchSubstage      = "primary"
	serviceName             = "pagle"
)

type Backend interface {
	Ping(w http.ResponseWriter, r *http.Request)
	TwirpServer() pagle.Pagle
	Shutdown(ctx context.Context) *errgroup.Group
	GetStatsDStatter() statsd.Statter
	StartAsyncWork() error
	// Accessors for lambda
	GetConditionAggregatesController() effecttotals.EffectTotals
	GetConditionCompleter() conditioncompleter.Completer
	GetConditionParticipantCompleter() conditionparticipantcompleter.Completer
	GetConditionRunCompleter() conditionruncompleter.Completer
	GetSFNClient() sfnPagle.SFNClient
	GetConditionDAO() conditiondynamo.DAO
	GetConditionParticipantDAO() conditionparticipantdynamo.DAO
	GetConditionAggregatesDAO() conditionaggregatesdynamo.DAO
}

type backend struct {
	Server                        pagle.Pagle                             `inject:""`
	Config                        *config.Config                          `inject:""`
	Statsd                        statsd.Statter                          `inject:""`
	ConditionAggregatesController effecttotals.EffectTotals               `inject:"conditionaggregatescontroller"`
	ConditionCompleter            conditioncompleter.Completer            `inject:"conditioncompleter"`
	ConditionParticipantCompleter conditionparticipantcompleter.Completer `inject:"conditionparticipantcompleter"`
	ConditionRunCompleter         conditionruncompleter.Completer         `inject:"conditionruncompleter"`
	SNSClient                     snsPagle.SNSClient                      `inject:""`
	SFNClient                     sfnPagle.SFNClient                      `inject:""`

	ConditionDAO            conditiondynamo.DAO            `inject:""`
	ConditionParticipantDAO conditionparticipantdynamo.DAO `inject:""`
	ConditionAggregatesDAO  conditionaggregatesdynamo.DAO  `inject:""`

	// Pollers
	ECSPoller                ecspoller.Poller          `inject:""`
	EffectStaticConfigPoller effectstaticconfig.Poller `inject:""`
}

func NewBackend(cfg *config.Config) (Backend, error) {
	b := &backend{}

	if cfg == nil {
		return nil, errors.New("received nil config")
	}

	statters := make([]statsd.Statter, 0)

	if cfg.CloudWatchMetricsEnabled {
		cloudwatchStatter := splatter.NewBufferedTelemetryCloudWatchStatter(&splatter.BufferedTelemetryConfig{
			AWSRegion:         cfg.AWSRegion,
			ServiceName:       serviceName,
			Stage:             cfg.EnvironmentName,
			Substage:          cloudwatchSubstage,
			BufferSize:        100000,
			AggregationPeriod: time.Minute,
			FlushPeriod:       time.Second * 30,
		}, map[string]bool{})

		statters = append(statters, cloudwatchStatter)
	}

	statsdStatter, err := splatter.NewCompositeStatter(statters)
	if err != nil {
		return nil, err
	}

	awsConfig := aws.NewConfig().WithRegion(cfg.AWSRegion)
	sess, err := session.NewSession(awsConfig)
	if err != nil {
		return nil, err
	}

	paydayClient, err := payday.NewClient(twitchclient.ClientConf{
		Host:  cfg.PaydayEndpoint,
		Stats: statsdStatter,
	})
	if err != nil {
		return nil, err
	}

	emsClient, err := ems.NewClient(twitchclient.ClientConf{
		Host:  cfg.EMSEndpoint,
		Stats: statsdStatter,
	})
	if err != nil {
		return nil, err
	}

	paydayRPC := paydayrpc.NewPaydayProtobufClient(cfg.PaydayEndpoint, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Host:  cfg.PaydayEndpoint,
		Stats: statsdStatter,
	}))

	pubsubClient, err := pubclient.NewPubClient(twitchclient.ClientConf{
		Host:  cfg.PubsubEndpoint,
		Stats: statsdStatter,
	})
	if err != nil {
		return nil, err
	}

	owlClient, err := owl.NewClient(twitchclient.ClientConf{
		Host:  cfg.OwlEndpoint,
		Stats: statsdStatter,
	})
	if err != nil {
		return nil, err
	}

	ripleyClient := riptwirp.NewRipleyProtobufClient(cfg.RipleyEndpoint, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Host:  cfg.RipleyEndpoint,
		Stats: statsdStatter,
	}))

	hystrixCollector := hystrixStats.NewHystrixMetricsCollector()

	var graph inject.Graph
	err = graph.Provide(
		// Backend (graph root)
		&inject.Object{Value: b},

		// Config
		&inject.Object{Value: cfg},

		// RPC Server
		&inject.Object{Value: server.NewServer()},

		// Stats
		&inject.Object{Value: statsdStatter},
		&inject.Object{Value: stats.NewStatter()},
		&inject.Object{Value: hystrixCollector},

		// API
		&inject.Object{Value: conditionapi.NewConditionAPI(), Name: "conditionapi"},
		&inject.Object{Value: conditionparticipantapi.NewConditionParticipantAPI(), Name: "conditionparticipantapi"},

		// Controllers
		&inject.Object{Value: effect.NewController(), Name: "effectcontroller"},
		&inject.Object{Value: effecttotals.NewEffectTotals(), Name: "conditionaggregatescontroller"},

		// Condition
		&inject.Object{Value: conditiongetter.NewGetter(), Name: "conditiongetter"},
		&inject.Object{Value: conditioncompleter.NewCompleter(), Name: "conditioncompleter"},
		&inject.Object{Value: conditioncreator.NewCreator(), Name: "conditioncreator"},

		// ConditionRun
		&inject.Object{Value: conditionrungetter.NewGetter(), Name: "conditionrungetter"},
		&inject.Object{Value: conditionruncreator.NewCreator(), Name: "conditionruncreator"},
		&inject.Object{Value: conditionruncompleter.NewCompleter(), Name: "conditionruncompleter"},

		// ConditionParticipant
		&inject.Object{Value: conditionparticipantgetter.NewGetter(), Name: "conditionparticipantgetter"},
		&inject.Object{Value: conditionparticipantcompleter.NewCompleter(), Name: "conditionparticipantcompleter"},
		&inject.Object{Value: conditionparticipantcreator.NewCreator(), Name: "conditionparticipantcreator"},

		// Effect Tenants
		&inject.Object{Value: givebits.NewGiveBitsTenant(), Name: "givebitstenant"},

		// DAOs
		&inject.Object{Value: conditiondynamo.NewDAO(sess, cfg.DynamoSuffix)},
		&inject.Object{Value: conditionparticipantdynamo.NewDAO(sess, cfg.DynamoSuffix)},
		&inject.Object{Value: conditionrundynamo.NewDAO(sess, cfg.DynamoSuffix)},
		&inject.Object{Value: conditionaggregatesdynamo.NewDAO(sess, cfg.DynamoSuffix)},
		&inject.Object{Value: transactiondynamo.NewDAO(sess)},

		// Payday
		&inject.Object{Value: paydayRPC},    // Payday Twirp
		&inject.Object{Value: paydayClient}, // Payday non-Twirp

		// EMS
		&inject.Object{Value: emsClient},
		&inject.Object{Value: emsPagle.NewEMSClient()},

		// Ripley
		&inject.Object{Value: ripleyClient},
		&inject.Object{Value: ripley.NewClient()},

		// OWL
		&inject.Object{Value: owlClient},
		&inject.Object{Value: owlPagle.NewClient()},

		// SNS
		&inject.Object{Value: sns.NewDefaultClient()},
		&inject.Object{Value: snsPagle.NewSNSClient()},

		// SFN
		&inject.Object{Value: sfn.New(sess, awsConfig)},
		&inject.Object{Value: sfnPagle.NewSFNClient()},

		// S3
		&inject.Object{Value: s3.NewDefaultClient()},
		&inject.Object{Value: s3Pagle.NewS3Client()},

		// ECS
		&inject.Object{Value: ecs.NewClient(sess, awsConfig)},

		// Redis
		&inject.Object{Value: redis.NewClient(cfg)},

		// Caches
		&inject.Object{Value: getconditions.NewCache()},
		&inject.Object{Value: getconditionparticipants.NewCache()},

		// Pollers
		&inject.Object{Value: ecspoller.NewPoller()},
		&inject.Object{Value: effectstaticconfig.NewPoller()},

		// Pubsub
		&inject.Object{Value: pubsubClient},
		&inject.Object{Value: pubsub.NewPubsubClient()},
	)

	if err != nil {
		return nil, err
	}

	err = graph.Populate()
	if err != nil {
		return nil, err
	}

	metricCollector.Registry.Register(func(name string) metricCollector.MetricCollector {
		return hystrixCollector.NewHystrixCommandMetricsCollector(name)
	})

	return b, nil
}

func (b *backend) GetConditionAggregatesController() effecttotals.EffectTotals {
	return b.ConditionAggregatesController
}

func (b *backend) GetConditionCompleter() conditioncompleter.Completer {
	return b.ConditionCompleter
}

func (b *backend) GetConditionParticipantCompleter() conditionparticipantcompleter.Completer {
	return b.ConditionParticipantCompleter
}

func (b *backend) GetConditionRunCompleter() conditionruncompleter.Completer {
	return b.ConditionRunCompleter
}

func (b *backend) GetSFNClient() sfnPagle.SFNClient {
	return b.SFNClient
}

func (b *backend) GetConditionDAO() conditiondynamo.DAO {
	return b.ConditionDAO
}

func (b *backend) GetConditionParticipantDAO() conditionparticipantdynamo.DAO {
	return b.ConditionParticipantDAO
}

func (b *backend) GetConditionAggregatesDAO() conditionaggregatesdynamo.DAO {
	return b.ConditionAggregatesDAO
}

// Must only be run from an ECS cluster (i.e. not from a Lambda)
func (b *backend) StartAsyncWork() error {
	if config.GetEnv() != config.Local {
		err := b.ECSPoller.Init()
		if err != nil {
			return err
		}

		graceful.Go(b.ECSPoller.RefreshAtInterval)
	}

	err := b.EffectStaticConfigPoller.Init()
	if err != nil {
		return err
	}

	graceful.Go(b.EffectStaticConfigPoller.RefreshAtInterval)

	return nil
}

func (b *backend) Ping(w http.ResponseWriter, r *http.Request) {
	pagleClient := pagle.NewPagleProtobufClient(localhostServerEndpoint, http.DefaultClient)
	_, err := pagleClient.HealthCheck(r.Context(), &pagle.HealthCheckReq{})
	if err != nil {
		log.WithError(err).Error("Could not ping pagle server")
		w.WriteHeader(http.StatusInternalServerError)
		_, innerErr := io.WriteString(w, err.Error())
		if innerErr != nil {
			log.WithError(innerErr).Error("Error writing http response")
		}
		return
	}

	w.WriteHeader(http.StatusOK)
	_, err = io.WriteString(w, "pong")
	if err != nil {
		log.WithError(err).Error("Error writing http response")
	}
}

func (b *backend) TwirpServer() pagle.Pagle {
	return b.Server
}

func (b *backend) Shutdown(ctx context.Context) *errgroup.Group {
	// Shutdown graceful goroutines (pollers)
	graceful.Shutdown()

	errg, _ := errgroup.WithContext(ctx)

	// Shutdown statter
	errg.Go(func() error {
		err := b.GetStatsDStatter().Close()
		if err != nil {
			log.WithError(err).Error("could not close statter")
			return err
		}

		return nil
	})

	return errg
}

func (b *backend) GetStatsDStatter() statsd.Statter {
	return b.Statsd
}
