package ripley

import (
	"context"

	"code.justin.tv/commerce/pagle/backend/hystrix/commands"
	hystrixutils "code.justin.tv/commerce/pagle/backend/utils/hystrix"
	ripley "code.justin.tv/revenue/ripley/rpc"
)

type PartnerType string

const (
	AffiliatePartnerType   = PartnerType("affiliate")   // Affiliate partners
	TraditionalPartnerType = PartnerType("traditional") // Traditional partners
	NonPartnerPartnerType  = PartnerType("non-partner") // Neither an affiliate partner nor a traditional partner
	UnknownPartnerType     = PartnerType("")            // We don't know their type due to downstream service call failures.
)

type Client interface {
	GetPartnerType(ctx context.Context, channelID string) (PartnerType, error)
}

type clientImpl struct {
	RipleyClient ripley.Ripley `inject:""`
}

func NewClient() Client {
	return &clientImpl{}
}

func (c *clientImpl) GetPartnerType(ctx context.Context, channelID string) (PartnerType, error) {
	var resp *ripley.GetPayoutTypeResponse
	err := hystrixutils.CallTwitchClientWithHystrix(commands.RipleyGetPayoutTypeCommand, hystrixutils.HystrixDo, func() error {
		var innerErr error
		resp, innerErr = c.RipleyClient.GetPayoutType(ctx, &ripley.GetPayoutTypeRequest{
			ChannelId: channelID,
		})

		return innerErr
	})

	if err != nil {
		return UnknownPartnerType, err
	}

	if resp.PayoutType.IsPartner {
		return TraditionalPartnerType, nil
	} else if resp.PayoutType.IsAffiliate {
		return AffiliatePartnerType, nil
	}

	return NonPartnerPartnerType, nil
}
