package server

import (
	"context"

	conditionapi "code.justin.tv/commerce/pagle/backend/api/condition"
	conditionParticipantapi "code.justin.tv/commerce/pagle/backend/api/conditionparticipant"
	pagle "code.justin.tv/commerce/pagle/rpc"
)

type server struct {
	ConditionAPI            conditionapi.ConditionAPI                       `inject:"conditionapi"`
	ConditionParticipantAPI conditionParticipantapi.ConditionParticipantAPI `inject:"conditionparticipantapi"`
}

func NewServer() pagle.Pagle {
	return &server{}
}

func (s *server) HealthCheck(ctx context.Context, req *pagle.HealthCheckReq) (*pagle.HealthCheckResp, error) {
	return &pagle.HealthCheckResp{
		Response: "OK",
	}, nil
}

func (s *server) CreateCondition(ctx context.Context, req *pagle.CreateConditionReq) (*pagle.CreateConditionResp, error) {
	return s.ConditionAPI.CreateCondition(ctx, req)
}

func (s *server) CancelCondition(ctx context.Context, req *pagle.CancelConditionReq) (*pagle.CancelConditionResp, error) {
	return s.ConditionAPI.CancelCondition(ctx, req)
}

func (s *server) SatisfyCondition(ctx context.Context, req *pagle.SatisfyConditionReq) (*pagle.SatisfyConditionResp, error) {
	return s.ConditionAPI.SatisfyCondition(ctx, req)
}

func (s *server) GetConditionSummary(ctx context.Context, req *pagle.GetConditionSummaryReq) (*pagle.GetConditionSummaryResp, error) {
	return s.ConditionAPI.GetConditionSummary(ctx, req)
}

func (s *server) BatchGetConditionSummary(ctx context.Context, req *pagle.BatchGetConditionSummaryReq) (*pagle.BatchGetConditionSummaryResp, error) {
	return s.ConditionAPI.BatchGetConditionSummary(ctx, req)
}

func (s *server) GetConditions(ctx context.Context, req *pagle.GetConditionsReq) (*pagle.GetConditionsResp, error) {
	return s.ConditionAPI.GetConditions(ctx, req)
}

func (s *server) CreateConditionParticipant(ctx context.Context, req *pagle.CreateConditionParticipantReq) (*pagle.CreateConditionParticipantResp, error) {
	return s.ConditionParticipantAPI.CreateConditionParticipant(ctx, req)
}

func (s *server) GetCondition(ctx context.Context, req *pagle.GetConditionReq) (*pagle.GetConditionResp, error) {
	return s.ConditionAPI.GetCondition(ctx, req)
}

func (s *server) BatchGetCondition(ctx context.Context, req *pagle.BatchGetConditionReq) (*pagle.BatchGetConditionResp, error) {
	return s.ConditionAPI.BatchGetCondition(ctx, req)
}

func (s *server) GetConditionParticipant(ctx context.Context, req *pagle.GetConditionParticipantReq) (*pagle.GetConditionParticipantResp, error) {
	return s.ConditionParticipantAPI.GetConditionParticipant(ctx, req)
}

func (s *server) GetConditionParticipants(ctx context.Context, req *pagle.GetConditionParticipantsReq) (*pagle.GetConditionParticipantsResp, error) {
	return s.ConditionParticipantAPI.GetConditionParticipants(ctx, req)
}

func (s *server) GetConditionParticipantsByOwnerID(ctx context.Context, req *pagle.GetConditionParticipantsByOwnerIDReq) (*pagle.GetConditionParticipantsByOwnerIDResp, error) {
	return s.ConditionParticipantAPI.GetConditionParticipantsByOwnerID(ctx, req)
}
