package sns

import (
	"context"
	"encoding/json"
	"time"

	"code.justin.tv/commerce/gogogadget/aws/sns"
	gringotts "code.justin.tv/commerce/gringotts/sns"
	ecspoller "code.justin.tv/commerce/pagle/backend/ecs/poller"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/backend/stats"
	"code.justin.tv/commerce/pagle/config"
	"golang.org/x/time/rate"
)

const (
	snsLimiterBurstLimit   = 20
	publishLatencyStatName = "sns_publish_latency"
	publishErrorStatName   = "sns_publish_error"
)

type SNSClient interface {
	PublishConditionParticipantProcessing(ctx context.Context, message models.ConditionParticipantProcessingSNSMessage) error
	PublishConditionParticipantCompletion(ctx context.Context, message models.ConditionParticipantCompletionSNSMessage) error
	PublishConditionParticipantCleanup(ctx context.Context, message models.ConditionParticipantCleanupSNSMessage) error
	PublishConditionRunProcessing(ctx context.Context, message models.ConditionRunProcessingSNSMessage) error
	PublishConditionAggregatesRetry(ctx context.Context, message models.ConditionAggregatesRetrySNSMessage) error
	PublishGringottsRevenueEvent(ctx context.Context, message gringotts.RevenueEventMessage) error
	PublishConditionCompletion(ctx context.Context, message models.ConditionCompletionSNSMessage) error
}

func NewSNSClient() SNSClient {
	return &snsClientImpl{}
}

type snsClientImpl struct {
	SNS       sns.Client       `inject:""`
	Config    *config.Config   `inject:""`
	ECSPoller ecspoller.Poller `inject:""`
	Statter   stats.Statter    `inject:""`
	limiter   *rate.Limiter
}

func (p *snsClientImpl) PublishConditionParticipantProcessing(ctx context.Context, message models.ConditionParticipantProcessingSNSMessage) error {
	return p.publish(ctx, p.Config.SNS.ConditionParticipantProcessingARN, message)
}

func (p *snsClientImpl) PublishConditionParticipantCompletion(ctx context.Context, message models.ConditionParticipantCompletionSNSMessage) error {
	return p.publish(ctx, p.Config.SNS.ConditionParticipantCompletionARN, message)
}

func (p *snsClientImpl) PublishConditionParticipantCleanup(ctx context.Context, message models.ConditionParticipantCleanupSNSMessage) error {
	return p.publish(ctx, p.Config.SNS.ConditionParticipantCleanupARN, message)
}

func (p *snsClientImpl) PublishConditionRunProcessing(ctx context.Context, message models.ConditionRunProcessingSNSMessage) error {
	return p.publish(ctx, p.Config.SNS.ConditionRunProcessingARN, message)
}

func (p *snsClientImpl) PublishConditionAggregatesRetry(ctx context.Context, message models.ConditionAggregatesRetrySNSMessage) error {
	return p.publish(ctx, p.Config.SNS.ConditionAggregatesRetryARN, message)
}

func (p *snsClientImpl) PublishGringottsRevenueEvent(ctx context.Context, message gringotts.RevenueEventMessage) error {
	return p.publish(ctx, p.Config.SNS.GringottsRevenueEventARN, message)
}

func (p *snsClientImpl) PublishConditionCompletion(ctx context.Context, message models.ConditionCompletionSNSMessage) error {
	return p.publish(ctx, p.Config.SNS.ConditionCompletionARN, message)
}

func (p *snsClientImpl) statPublishError() {
	p.Statter.Inc(publishErrorStatName, 1, 1)
}

func (p *snsClientImpl) publish(ctx context.Context, topicARN string, message interface{}) error {
	startTime := time.Now()

	defer func() {
		p.Statter.TimingDuration(publishLatencyStatName, time.Since(startTime), 1)
	}()

	mJSON, err := json.Marshal(message)

	if err != nil {
		p.statPublishError()
		return err
	}

	err = p.rateLimit(ctx)
	if err != nil {
		p.statPublishError()
		return err
	}

	err = p.SNS.Publish(ctx, topicARN, string(mJSON))

	if err != nil {
		p.statPublishError()
		return err
	}

	return nil
}

func (p *snsClientImpl) rateLimit(ctx context.Context) error {
	p.updateRateLimiter()

	err := p.limiter.Wait(ctx)
	return err
}

func (p *snsClientImpl) updateRateLimiter() {
	throttlingLimit := p.Config.SNS.PublishThrottlingLimitTPS
	ecsInstanceCount := p.ECSPoller.GetInstanceCount()
	targetRateLimit := rate.Limit(float64(throttlingLimit) / float64(ecsInstanceCount))

	if p.limiter == nil {
		// If the limiter has not been instantiated yet,
		p.limiter = rate.NewLimiter(targetRateLimit, snsLimiterBurstLimit)
	} else if p.limiter.Limit() != targetRateLimit {
		// If the ECS poller has a new instance count, update the rate limit
		p.limiter.SetLimit(targetRateLimit)
	}
}
