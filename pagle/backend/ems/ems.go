package ems

import (
	"context"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/foundation/twitchclient"

	"code.justin.tv/commerce/pagle/backend/auth"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/hystrix/commands"
	"code.justin.tv/commerce/pagle/backend/stats"
	hystrixutils "code.justin.tv/commerce/pagle/backend/utils/hystrix"
	"code.justin.tv/commerce/pagle/config"
	ems "code.justin.tv/gds/gds/extensions/ems/client"
	"code.justin.tv/gds/gds/extensions/ems/documents"
)

type EMSClient interface {
	GetChannelExtension(ctx context.Context, extensionID, installationChannelID string) (emsExtension *documents.InstalledExtensionDocument, err error)
	IsExtensionBitsEnabled(ctx context.Context, extensionID, installationChannelID string) (bool, error)
}

func NewEMSClient() EMSClient {
	return &emsClientImpl{}
}

type emsClientImpl struct {
	EMS     ems.Client     `inject:""`
	Config  *config.Config `inject:""`
	Statter stats.Statter  `inject:""`
}

func (e *emsClientImpl) GetChannelExtension(ctx context.Context, extensionID, installationChannelID string) (emsExtension *documents.InstalledExtensionDocument, err error) {
	if strings.Blank(extensionID) || strings.Blank(installationChannelID) {
		return nil, errors.IllegalArgument.New("GetChannelExtension requires both extension_id and installation_channel_id")
	}

	var extensions *documents.InstalledExtensionsDocument
	err = hystrixutils.CallTwitchClientWithHystrix(commands.EMSGetExtensionsByChannelIDCommand, hystrixutils.HystrixDo, func() error {
		jwt, ok := ctx.Value(auth.TwitchAuthHeader).(string)
		if !ok {
			return errors.InternalError.New("unexpected type when converting Twitch-Authorization header to string")
		}

		if jwt == "" {
			logrus.WithFields(logrus.Fields{
				"extension_id":            extensionID,
				"installation_channel_id": installationChannelID,
			}).Error("requesting extensions from EMS without an auth token")
		}

		extensions, err = e.EMS.GetExtensionsByChannelID(ctx, installationChannelID, &twitchclient.ReqOpts{
			AuthorizationToken: jwt,
		})
		return err
	})

	if err != nil {
		return nil, err
	}

	for _, extension := range extensions.InstalledExtensions {
		if extension != nil && extension.Extension != nil && extension.Extension.ID == extensionID {
			return extension, nil
		}
	}

	return nil, nil
}

func (e *emsClientImpl) IsExtensionBitsEnabled(ctx context.Context, extensionID, installationChannelID string) (bool, error) {
	emsExtension, err := e.GetChannelExtension(ctx, extensionID, installationChannelID)

	if err != nil {
		return false, err
	}

	if emsExtension != nil && emsExtension.InstallationStatus != nil && emsExtension.InstallationStatus.Abilities != nil {
		//Checking for ability on extension since an extension can be monetized but the broadcaster opted out of that functionality
		return emsExtension.InstallationStatus.Abilities.IsBitsEnabled, nil
	}

	return false, nil
}
