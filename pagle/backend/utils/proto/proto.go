package proto

import (
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/backend/utils/effect/settings/poolable"
	pagle "code.justin.tv/commerce/pagle/rpc"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
)

func EffectSettingToProtoEffectSetting(protoEffectType pagle.EffectType, setting models.EffectSettings) (*pagle.EffectSettings, error) {
	l := log.WithField("effectSetting", setting)

	switch protoEffectType {
	case pagle.EffectType_GIVE_BITS:
		{
			if setting.Settings == nil || setting.Settings.GiveBits == nil {
				message := "encountered nil GiveBits effect_settings in EffectSettingToProtoEffectSetting"
				l.Warn(message)

				return nil, errors.IllegalState.New(
					message,
				)
			}

			giveBitsSettings := setting.Settings.GiveBits
			poolRecipients := PoolRecipientsToProtoPoolRecipients(giveBitsSettings.PoolRecipients)

			return &pagle.EffectSettings{
				EffectType: pagle.EffectType_GIVE_BITS,
				Settings: &pagle.EffectSettings_GiveBitsSettings{
					GiveBitsSettings: &pagle.GiveBitsSettings{
						PoolRecipients: poolRecipients,
					},
				},
			}, nil
		}
	default:
		message := "encountered unknown effect type while converting effect setting from internal to proto representation"
		l.Warn(message)
		return nil, errors.IllegalState.New(message)
	}

}

func ProtoEffectSettingToEffectSetting(effectType models.EffectType, protoSetting pagle.EffectSettings) (models.EffectSettings, error) {
	l := log.WithField("effectSetting", protoSetting)

	switch effectType {
	case models.GiveBits:
		{
			giveBitsSettings := protoSetting.GetGiveBitsSettings()

			if giveBitsSettings == nil {
				message := "encountered nil GiveBits effect_settings in ProtoEffectSettingToEffectSetting"
				l.Warn(message)

				return models.EffectSettings{}, errors.IllegalArgument.New(message)
			}

			poolRecipients := ProtoPoolRecipientsToPoolRecipients(giveBitsSettings.PoolRecipients)

			err := poolable.ValidatePoolRecipients(poolRecipients)

			if err != nil {
				return models.EffectSettings{}, err
			}

			return models.EffectSettings{
				EffectType: models.GiveBits,
				Settings: &models.EffectTypeSettings{
					GiveBits: &models.GiveBitsSettings{
						PoolRecipients: poolRecipients,
					},
				},
			}, nil
		}
	default:
		message := "encountered unknown effect type while converting effect setting from proto to internal representation"
		l.Warn(message)
		return models.EffectSettings{}, errors.IllegalArgument.New(message)
	}

}

func EffectTypesToProtoEffectTypes(effectTypes []models.EffectType) ([]pagle.EffectType, error) {
	var protoEffectTypes []pagle.EffectType

	for _, effectType := range effectTypes {
		protoEffectType, err := EffectTypeToProtoEffectType(effectType)

		if err != nil {
			return protoEffectTypes, err
		}

		protoEffectTypes = append(protoEffectTypes, protoEffectType)
	}

	return protoEffectTypes, nil
}

func ProtoEffectTypesToEffectTypes(protoEffectTypes []pagle.EffectType) ([]models.EffectType, error) {
	var effectTypes []models.EffectType

	for _, protoEffectType := range protoEffectTypes {
		effectType, err := ProtoEffectTypeToEffectType(protoEffectType)

		if err != nil {
			return effectTypes, err
		}

		effectTypes = append(effectTypes, effectType)
	}

	return effectTypes, nil
}

func EffectSettingsToProtoEffectSettings(settings []models.EffectSettings) ([]*pagle.EffectSettings, error) {
	var protoSettings []*pagle.EffectSettings

	for _, setting := range settings {
		protoEffectType, err := EffectTypeToProtoEffectType(setting.EffectType)

		if err != nil {
			return protoSettings, err
		}

		protoSetting, err := EffectSettingToProtoEffectSetting(protoEffectType, setting)

		if err != nil {
			return protoSettings, err
		}

		protoSettings = append(protoSettings, protoSetting)
	}

	return protoSettings, nil
}

func ProtoEffectSettingsToEffectSettings(protoSettings []*pagle.EffectSettings) ([]models.EffectSettings, error) {
	var settings []models.EffectSettings

	for _, protoSetting := range protoSettings {
		if protoSetting == nil {
			continue
		}

		effectType, err := ProtoEffectTypeToEffectType(protoSetting.EffectType)

		if err != nil {
			return settings, err
		}

		setting, err := ProtoEffectSettingToEffectSetting(effectType, *protoSetting)

		if err != nil {
			return settings, err
		}

		settings = append(settings, setting)
	}

	return settings, nil
}

func EffectTypeToProtoEffectType(effectType models.EffectType) (pagle.EffectType, error) {
	switch effectType {
	case models.GiveBits:
		return pagle.EffectType_GIVE_BITS, nil
	default:
		message := "encountered unknown effect type while converting effect type from internal to proto representation"
		log.WithField("effectType", effectType).Error(message)
		return pagle.EffectType(-1), errors.IllegalArgument.New(message)
	}
}

func ProtoEffectTypeToEffectType(effectType pagle.EffectType) (models.EffectType, error) {
	switch effectType {
	case pagle.EffectType_GIVE_BITS:
		return models.GiveBits, nil
	default:
		message := "encountered unknown effect type while converting effect type from proto to internal representation"
		log.WithField("effectType", effectType).Error(message)
		return models.EffectType(""), errors.IllegalArgument.New(message)
	}
}

func EffectToProtoEffect(effect models.Effect) (*pagle.Effect, error) {
	protoEffectType, err := EffectTypeToProtoEffectType(effect.EffectType)

	if err != nil {
		return nil, err
	}

	switch protoEffectType {
	case pagle.EffectType_GIVE_BITS:
		{
			if effect.Params.GiveBits == nil {
				return nil, errors.IllegalArgument.New(
					"give bits params cannot be nil",
				)
			}

			giveBitsParams := effect.Params.GiveBits

			return &pagle.Effect{
				EffectType: pagle.EffectType_GIVE_BITS,
				Params: &pagle.Effect_GiveBitsParams{
					GiveBitsParams: &pagle.GiveBitsParams{
						Amount:     giveBitsParams.PoolableInput.Amount,
						FromUserId: giveBitsParams.PoolableInput.FromUserID,
					},
				},
			}, nil
		}
	default:
		message := "encountered unknown effect type while converting effect from internal to proto representation"
		log.WithField("effect", effect).Warn(message)
		return nil, errors.IllegalArgument.New(message)
	}
}

func ProtoEffectToEffect(protoEffect pagle.Effect) (models.Effect, error) {
	effectType, err := ProtoEffectTypeToEffectType(protoEffect.EffectType)

	if err != nil {
		return models.Effect{}, err
	}

	switch effectType {
	case models.GiveBits:
		{
			giveBitsParams := protoEffect.GetGiveBitsParams()

			if giveBitsParams == nil {
				return models.Effect{}, errors.IllegalArgument.New("give bits params from effect cannot be nil")
			}

			return models.Effect{
				EffectType: models.GiveBits,
				Params: models.EffectParams{
					GiveBits: &models.EffectParamsGiveBits{
						PoolableInput: models.EffectInputPoolable{
							Amount:     giveBitsParams.Amount,
							FromUserID: giveBitsParams.FromUserId,
						},
					},
				},
			}, nil
		}
	default:
		message := "encountered unknown effect type while converting effect from proto to internal representation"
		log.WithField("effect", protoEffect).Warn(message)
		return models.Effect{}, errors.IllegalArgument.New(message)
	}
}

func PoolRecipientsToProtoPoolRecipients(recipients []models.PoolRecipientWeightedShare) []*pagle.PoolRecipientWeightedShare {
	var protoRecipients []*pagle.PoolRecipientWeightedShare

	for _, recipient := range recipients {
		protoRecipients = append(protoRecipients, &pagle.PoolRecipientWeightedShare{
			ShareWeight: recipient.ShareWeight,
			ToUserId:    recipient.ToUserID,
		})
	}

	return protoRecipients
}

func ProtoPoolRecipientsToPoolRecipients(protoRecipients []*pagle.PoolRecipientWeightedShare) []models.PoolRecipientWeightedShare {
	var recipients []models.PoolRecipientWeightedShare

	for _, protoRecipient := range protoRecipients {
		if protoRecipient == nil {
			continue
		}

		recipients = append(recipients, models.PoolRecipientWeightedShare{
			ShareWeight: protoRecipient.ShareWeight,
			ToUserID:    protoRecipient.ToUserId,
		})
	}

	return recipients
}

func ConditionStateToProtoConditionState(conditionState models.ConditionState) (pagle.ConditionState, error) {
	switch conditionState {
	case models.ConditionActive:
		return pagle.ConditionState_ACTIVE, nil
	case models.ConditionInactive:
		return pagle.ConditionState_INACTIVE, nil
	case models.ConditionExpired:
		return pagle.ConditionState_EXPIRED, nil
	case models.ConditionCanceled:
		return pagle.ConditionState_CANCELED, nil
	default:
		message := "encountered unknown condition state while converting to proto representation"
		log.WithField("conditionState", conditionState).Error(message)
		return pagle.ConditionState(-1), errors.IllegalArgument.New(message)

	}
}

func ProtoConditionStateToConditionState(protoConditionState pagle.ConditionState) (models.ConditionState, error) {
	switch protoConditionState {
	case pagle.ConditionState_ACTIVE:
		return models.ConditionActive, nil
	case pagle.ConditionState_INACTIVE:
		return models.ConditionInactive, nil
	case pagle.ConditionState_EXPIRED:
		return models.ConditionExpired, nil
	case pagle.ConditionState_CANCELED:
		return models.ConditionCanceled, nil
	default:
		message := "encountered unknown condition state while converting to internal representation"
		log.WithField("protoConditionState", protoConditionState).Error(message)
		return models.ConditionState(""), errors.IllegalArgument.New(message)

	}
}

func ProtoConditionParticipantEndStateToConditionParticipantEndState(protoConditionParticipantEndState pagle.ConditionParticipantEndState) (models.ConditionParticipantEndState, error) {
	switch protoConditionParticipantEndState {
	case pagle.ConditionParticipantEndState_CONDITION_CANCELED:
		return models.ConditionParticipantEndStateConditionCanceled, nil
	case pagle.ConditionParticipantEndState_CONDITION_EXPIRED:
		return models.ConditionParticipantEndStateConditionExpired, nil
	case pagle.ConditionParticipantEndState_SATISFIED:
		return models.ConditionParticipantEndStateSatisfied, nil
	case pagle.ConditionParticipantEndState_PENDING_COMPLETION:
		return models.ConditionParticipantEndStatePendingCompletion, nil
	case pagle.ConditionParticipantEndState_CONDITION_PARTICIPANT_EXPIRED:
		return models.ConditionParticipantEndStateExpired, nil
	case pagle.ConditionParticipantEndState_CONDITION_PARTICIPANT_CANCELED:
		return models.ConditionParticipantEndStateCanceled, nil
	case pagle.ConditionParticipantEndState_CONDITION_PARTICIPANT_FAILED_VALIDATION:
		return models.ConditionParticipantEndStateFailedValidation, nil
	default:
		message := "encountered unknown condition participant end state while converting to internal representation"
		log.WithField("protoConditionParticipantEndState", protoConditionParticipantEndState).Error(message)
		return models.ConditionParticipantEndState(""), errors.IllegalArgument.New(message)

	}
}

func ConditionsToProtoConditions(conditions []*models.Condition) ([]*pagle.Condition, error) {
	var protoConditions []*pagle.Condition

	for _, condition := range conditions {
		if condition == nil {
			log.Warn("encountered a nil condition while converting to proto representation")
			continue
		}

		protoCondition, err := ConditionToProtoCondition(*condition)

		if err != nil {
			return protoConditions, err
		}

		protoConditions = append(protoConditions, &protoCondition)
	}

	return protoConditions, nil
}

func ExtensionToProtoExtension(extension models.Extension) (*pagle.Extension, error) {
	return &pagle.Extension{
		Id:                    extension.ID,
		InstallationChannelId: extension.InstallationChannelID,
	}, nil
}

func ProtoExtensionToExtension(extension pagle.Extension) (*models.Extension, error) {
	// TODO: uncomment once FE starts sending InstallationChannelId
	//if extension.InstallationChannelId == "" || extension.Id == "" {
	//	return nil, errors.IllegalArgument.New("must provided both an extension id and installation_channel_id")
	//}

	return &models.Extension{
		ID:                    extension.Id,
		InstallationChannelID: extension.InstallationChannelId,
	}, nil
}

func ConditionToProtoCondition(condition models.Condition) (pagle.Condition, error) {
	var timeoutAt *timestamp.Timestamp
	var extension *pagle.Extension
	var err error

	if condition.TimeoutAt != nil {
		timeoutAt, err = ptypes.TimestampProto(*condition.TimeoutAt)

		if err != nil {
			return pagle.Condition{}, err
		}
	}

	createdAt, err := ptypes.TimestampProto(condition.CreatedAt)

	if err != nil {
		return pagle.Condition{}, err
	}

	updatedAt, err := ptypes.TimestampProto(condition.UpdatedAt)

	if err != nil {
		return pagle.Condition{}, err
	}

	conditionState, err := ConditionStateToProtoConditionState(condition.ConditionState)

	if err != nil {
		return pagle.Condition{}, err
	}

	supportedEffects, err := EffectTypesToProtoEffectTypes(condition.SupportedEffects)

	if err != nil {
		return pagle.Condition{}, err
	}

	effectSettings, err := EffectSettingsToProtoEffectSettings(condition.EffectSettings)

	if err != nil {
		return pagle.Condition{}, err
	}

	if condition.Extension != nil {
		extension, err = ExtensionToProtoExtension(*condition.Extension)

		if err != nil {
			return pagle.Condition{}, err
		}
	}

	return pagle.Condition{
		ConditionId:                       condition.ConditionID,
		OwnerId:                           condition.OwnerID,
		ConditionRunId:                    condition.ConditionRunID,
		Name:                              condition.Name,
		TimeoutAt:                         timeoutAt,
		CreatedAt:                         createdAt,
		UpdatedAt:                         updatedAt,
		ConditionState:                    conditionState,
		SupportedEffects:                  supportedEffects,
		EffectSettings:                    effectSettings,
		Domain:                            condition.Domain,
		DisableWhenSatisfied:              condition.DisableWhenSatisfied,
		DefineEffectSettingsWhenSatisfied: condition.DefineEffectSettingsWhenSatisfied,
		Extension:                         extension,
	}, nil
}

func ConditionParticipantProcessingStateToProtoConditionParticipantProcessingState(conditionParticipantProcessingState models.ConditionParticipantProcessingState) (pagle.ConditionParticipantProcessingState, error) {
	switch conditionParticipantProcessingState {
	case models.ConditionParticipantProcessingStateSuccess:
		return pagle.ConditionParticipantProcessingState_SUCCESS, nil
	case models.ConditionParticipantProcessingStateProcessingError:
		return pagle.ConditionParticipantProcessingState_ERROR, nil
	case models.ConditionParticipantProcessingStatePending:
		return pagle.ConditionParticipantProcessingState_PENDING, nil
	case models.ConditionParticipantProcessingStateCreating:
		return pagle.ConditionParticipantProcessingState_CREATING, nil
	case models.ConditionParticipantProcessingStateFailedToCreate:
		return pagle.ConditionParticipantProcessingState_FAILED_TO_CREATE, nil
	default:
		message := "encountered unknown condition participant processing state while converting to proto representation"
		log.WithField("conditionParticipantProcessingState", conditionParticipantProcessingState).Error(message)
		return pagle.ConditionParticipantProcessingState(-1), errors.IllegalArgument.New(message)
	}
}

func ConditionParticipantEndStateToProtoConditionParticipantEndState(conditionParticipantEndState models.ConditionParticipantEndState) (pagle.ConditionParticipantEndState, error) {
	switch conditionParticipantEndState {
	case models.ConditionParticipantEndStatePendingCompletion:
		return pagle.ConditionParticipantEndState_PENDING_COMPLETION, nil
	case models.ConditionParticipantEndStateSatisfied:
		return pagle.ConditionParticipantEndState_SATISFIED, nil
	case models.ConditionParticipantEndStateConditionExpired:
		return pagle.ConditionParticipantEndState_CONDITION_EXPIRED, nil
	case models.ConditionParticipantEndStateConditionCanceled:
		return pagle.ConditionParticipantEndState_CONDITION_CANCELED, nil
	case models.ConditionParticipantEndStateExpired:
		return pagle.ConditionParticipantEndState_CONDITION_PARTICIPANT_EXPIRED, nil
	case models.ConditionParticipantEndStateCanceled:
		return pagle.ConditionParticipantEndState_CONDITION_PARTICIPANT_CANCELED, nil
	case models.ConditionParticipantEndStateFailedValidation:
		return pagle.ConditionParticipantEndState_CONDITION_PARTICIPANT_FAILED_VALIDATION, nil
	default:
		message := "encountered unknown condition participant end state while converting to proto representation"
		log.WithField("conditionParticipantEndState", conditionParticipantEndState).Error(message)
		return pagle.ConditionParticipantEndState(-1), errors.IllegalArgument.New(message)
	}
}

func ConditionParticipantsToProtoConditionParticipants(conditionParticipants []*models.ConditionParticipant) ([]*pagle.ConditionParticipant, error) {
	var protoConditionParticipants []*pagle.ConditionParticipant

	for _, conditionParticipant := range conditionParticipants {
		if conditionParticipant == nil {
			log.Warn("encountered a nil condition while converting to proto representation")
			continue
		}

		protoConditionParticipant, err := ConditionParticipantToProtoConditionParticipant(*conditionParticipant)

		if err != nil {
			return protoConditionParticipants, err
		}

		protoConditionParticipants = append(protoConditionParticipants, &protoConditionParticipant)
	}

	return protoConditionParticipants, nil
}

func ConditionParticipantToProtoConditionParticipant(conditionParticipant models.ConditionParticipant) (pagle.ConditionParticipant, error) {
	conditionParticipantProcessingState, err := ConditionParticipantProcessingStateToProtoConditionParticipantProcessingState(conditionParticipant.ConditionParticipantProcessingState)

	if err != nil {
		return pagle.ConditionParticipant{}, err
	}

	conditionParticipantEndState, err := ConditionParticipantEndStateToProtoConditionParticipantEndState(conditionParticipant.ConditionParticipantEndState)

	if err != nil {
		return pagle.ConditionParticipant{}, err
	}

	effect, err := EffectToProtoEffect(conditionParticipant.Effect)

	if err != nil {
		return pagle.ConditionParticipant{}, err
	}

	createdAt, err := ptypes.TimestampProto(conditionParticipant.CreatedAt)

	if err != nil {
		return pagle.ConditionParticipant{}, err
	}

	return pagle.ConditionParticipant{
		ConditionParticipantId:              conditionParticipant.ConditionParticipantID,
		OwnerId:                             conditionParticipant.OwnerID,
		ConditionParticipantProcessingState: conditionParticipantProcessingState,
		ConditionParticipantEndState:        conditionParticipantEndState,
		Effect:                              effect,
		ConditionId:                         conditionParticipant.ConditionID,
		ConditionOwnerId:                    conditionParticipant.ConditionOwnerID,
		Domain:                              conditionParticipant.Domain,
		TtlSeconds:                          conditionParticipant.TTLSeconds,
		CreatedAt:                           createdAt,
	}, nil
}

func ConditionParticipantAggregatesForStateToProtoConditionParticipantSummaryForState(aggregates models.ConditionParticipantAggregatesForState) *pagle.ConditionParticipantSummaryForState {
	return &pagle.ConditionParticipantSummaryForState{
		Total: aggregates.Total,
		Effects: &pagle.ConditionParticipantEffectSummaryForState{
			GiveBits: &pagle.GiveBitsEffectSummaryForState{
				TotalBitsAmount: aggregates.Effects.GiveBits.TotalBitsAmount,
				TotalEffects:    aggregates.Effects.GiveBits.TotalEffects,
			},
		},
	}
}

func ConditionParticipantAggregatesToProtoConditionParticipantSummary(participantAggregates models.ConditionParticipantAggregates) pagle.ConditionParticipantSummary {
	return pagle.ConditionParticipantSummary{
		Total:             participantAggregates.Total,
		Pending:           ConditionParticipantAggregatesForStateToProtoConditionParticipantSummaryForState(participantAggregates.Pending),
		Satisfied:         ConditionParticipantAggregatesForStateToProtoConditionParticipantSummaryForState(participantAggregates.Satisfied),
		ConditionCanceled: ConditionParticipantAggregatesForStateToProtoConditionParticipantSummaryForState(participantAggregates.ConditionCanceled),
		ConditionTimeout:  ConditionParticipantAggregatesForStateToProtoConditionParticipantSummaryForState(participantAggregates.ConditionTimeout),
		Canceled:          ConditionParticipantAggregatesForStateToProtoConditionParticipantSummaryForState(participantAggregates.Canceled),
		Timeout:           ConditionParticipantAggregatesForStateToProtoConditionParticipantSummaryForState(participantAggregates.Timeout),
		FailedValidation:  ConditionParticipantAggregatesForStateToProtoConditionParticipantSummaryForState(participantAggregates.FailedValidation),
	}
}

func ConditionAggregatesToProtoConditionSummary(aggregates models.ConditionAggregates) pagle.ConditionSummary {
	protoParticipants := ConditionParticipantAggregatesToProtoConditionParticipantSummary(aggregates.ConditionParticipants)

	return pagle.ConditionSummary{
		ConditionId:  aggregates.ConditionID,
		OwnerId:      aggregates.OwnerID,
		Domain:       aggregates.Domain,
		Participants: &protoParticipants,
	}
}

func BatchConditionAggregatesToProtoBatchConditionSummary(batchAggregates []models.ConditionAggregates) ([]*pagle.ConditionSummary, error) {
	var batchProto []*pagle.ConditionSummary

	for _, aggregates := range batchAggregates {
		protoConditionSummary := ConditionAggregatesToProtoConditionSummary(aggregates)

		batchProto = append(batchProto, &protoConditionSummary)
	}

	return batchProto, nil
}
