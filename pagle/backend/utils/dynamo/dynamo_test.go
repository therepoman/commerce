package dynamo

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/pointers"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	. "github.com/smartystreets/goconvey/convey"
)

type mockResponse struct {
	Output dynamodb.QueryOutput
	Error  error
}

// Returns the next response in the list each time it is called
func makeMockQuerier(orderedResponses []mockResponse) func(ctx aws.Context, input *dynamodb.QueryInput, opts ...request.Option) (*dynamodb.QueryOutput, error) {
	calls := 0

	return func(ctx context.Context, input *dynamodb.QueryInput, opts ...request.Option) (*dynamodb.QueryOutput, error) {
		if calls >= len(orderedResponses) {
			return nil, errors.New("out of bounds exception: call count exceeded mock responses length")
		}

		resp := orderedResponses[calls]

		calls += 1

		return &resp.Output, resp.Error
	}
}

func TestDynamoUtils_CountAll(t *testing.T) {
	Convey("given the CountAll util", t, func() {
		Convey("when one of the request errors", func() {
			Convey("we should return the error", func() {
				mockInput := dynamodb.QueryInput{}

				_, err := CountAll(context.Background(), makeMockQuerier([]mockResponse{
					{
						Output: dynamodb.QueryOutput{
							Count: pointers.Int64P(2),
							LastEvaluatedKey: map[string]*dynamodb.AttributeValue{
								"some_id": {
									S: pointers.StringP("some-hash"),
								},
							},
						},
						Error: nil,
					},
					{
						Output: dynamodb.QueryOutput{},
						Error:  errors.New("ERROR"),
					},
				}), mockInput)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when no requests error", func() {
			Convey("we should return the total count of results", func() {
				mockInput := dynamodb.QueryInput{}

				count, err := CountAll(context.Background(), makeMockQuerier([]mockResponse{
					{
						Output: dynamodb.QueryOutput{
							Count: pointers.Int64P(2),
							LastEvaluatedKey: map[string]*dynamodb.AttributeValue{
								"some_id": {
									S: pointers.StringP("some-hash"),
								},
							},
						},
						Error: nil,
					},
					{
						Output: dynamodb.QueryOutput{
							Count:            pointers.Int64P(8),
							LastEvaluatedKey: nil,
						},
						Error: nil,
					},
				}), mockInput)

				So(count, ShouldEqual, 10)
				So(err, ShouldBeNil)
			})
		})
	})
}

func TestDynamoUtils_QueryAll(t *testing.T) {
	Convey("given the QueryAll util", t, func() {
		Convey("when one of the request errors", func() {
			Convey("we should return the error", func() {
				mockInput := dynamodb.QueryInput{}

				_, err := QueryAll(context.Background(), "foo", makeMockQuerier([]mockResponse{
					{
						Output: dynamodb.QueryOutput{
							Items: []map[string]*dynamodb.AttributeValue{
								{
									"a": {},
									"b": {},
								},
							},
							LastEvaluatedKey: map[string]*dynamodb.AttributeValue{
								"some_id": {
									S: pointers.StringP("some-hash"),
								},
							},
						},
						Error: nil,
					},
					{
						Output: dynamodb.QueryOutput{},
						Error:  errors.New("ERROR"),
					},
				}), mockInput)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when no requests error", func() {
			Convey("we should return the the accumulated results", func() {
				mockInput := dynamodb.QueryInput{}

				items, err := QueryAll(context.Background(), "foo", makeMockQuerier([]mockResponse{
					{
						Output: dynamodb.QueryOutput{
							Items: []map[string]*dynamodb.AttributeValue{
								{
									"a": {},
								},
								{
									"b": {},
								},
							},
							LastEvaluatedKey: map[string]*dynamodb.AttributeValue{
								"some_id": {
									S: pointers.StringP("some-hash"),
								},
							},
						},
						Error: nil,
					},
					{
						Output: dynamodb.QueryOutput{
							Items: []map[string]*dynamodb.AttributeValue{
								{
									"c": {},
								},
								{
									"d": {},
								},
							},
							LastEvaluatedKey: nil,
						},
						Error: nil,
					},
				}), mockInput)

				So(items, ShouldHaveLength, 4)
				So(err, ShouldBeNil)
			})
		})
	})
}
