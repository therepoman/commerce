/**
	guregu/dynamo does a lot of auto-pagination behind the curtains. these utilities aim to give similar functionality,
	while still being able to use the aws sdk.
 	TODO: move to gogogadget
*/
package dynamo

import (
	"context"
	"encoding/base64"
	"encoding/json"

	"github.com/afex/hystrix-go/hystrix"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/guregu/dynamo"
	"github.com/joomcode/errorx"
)

const (
	MaxItemsPerTransaction = 25
)

type querierWithContext func(ctx aws.Context, input *dynamodb.QueryInput, opts ...request.Option) (*dynamodb.QueryOutput, error)

// Queries while LastEvaluatedKey is not nil, counting returned items
func CountAll(ctx context.Context, query querierWithContext, initialInput dynamodb.QueryInput) (int64, error) {
	var count int64 = 0

	for {
		result, err := query(ctx, &initialInput)

		if err != nil {
			return 0, errorx.Decorate(err, "query failed within CountAll")
		}

		if result == nil || result.Count == nil {
			return count, nil
		}

		count += *result.Count

		if result.LastEvaluatedKey == nil {
			break
		}

		initialInput.ExclusiveStartKey = result.LastEvaluatedKey
	}

	return count, nil
}

// Queries while LastEvaluatedKey is not nil, accumulating returned items
func QueryAll(ctx context.Context, circuitName string, query querierWithContext, initialInput dynamodb.QueryInput) ([]map[string]*dynamodb.AttributeValue, error) {
	var out []map[string]*dynamodb.AttributeValue

	for {
		var result *dynamodb.QueryOutput

		err := hystrix.Do(circuitName, func() error {
			var innerErr error
			result, innerErr = query(ctx, &initialInput)

			return innerErr
		}, nil)

		if err != nil {
			return nil, errorx.Decorate(err, "query failed within QueryAll")
		}

		if result == nil || len(result.Items) == 0 {
			return out, nil
		}

		out = append(out, result.Items...)

		if result.LastEvaluatedKey == nil {
			break
		}

		initialInput.ExclusiveStartKey = result.LastEvaluatedKey
	}

	return out, nil
}

func DecodePagingKey(cursor string) (dynamo.PagingKey, error) {
	if cursor == "" {
		return nil, nil
	}

	decoded, err := base64.StdEncoding.DecodeString(cursor)

	if err != nil {
		return nil, err
	}

	var pagingKey dynamo.PagingKey

	err = json.Unmarshal(decoded, &pagingKey)

	return pagingKey, err
}

func EncodePagingKey(key dynamo.PagingKey) (string, error) {
	if key == nil {
		return "", nil
	}

	keyJSON, err := json.Marshal(key)

	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(keyJSON), nil
}
