package privacy

import (
	"errors"

	"code.justin.tv/commerce/pagle/backend/models"
)

func IsValidUserDeletionRequest(input models.UserDeletionRequest) error {
	req := input.GetUserDeletionRequestData()

	if req == nil {
		return errors.New("nil request")
	}

	if len(req.UserIDs) < 1 {
		return errors.New("no users provided")
	}

	for _, userID := range req.UserIDs {
		if userID == "" {
			return errors.New("user IDs cannot be blank")
		}
	}

	return nil
}
