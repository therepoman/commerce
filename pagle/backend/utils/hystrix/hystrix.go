package hystrix

import (
	"code.justin.tv/foundation/twitchclient"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/twitchtv/twirp"
)

// Hystrix doesn't export these types, so we have to wrap hystrix.Do to enable
// dependency injection for tests.
type HystrixDoer func(name string, run func() error, fallback func(error) error) error

func HystrixDo(name string, run func() error, fallback func(error) error) error {
	return hystrix.Do(name, run, fallback)
}

// Wraps a twirp request with hystrix - circuit is opened for unknown and 5xx errors
// Returns
//  hystrix error if circuit is open else
//  non-5xx error returned from twirp call if non-nil else
//  nil
func CallTwirpWithHystrix(
	command string,
	hystrixDo HystrixDoer,
	twirpCall func() error) error {
	var innerErr error

	err := hystrixDo(command, func() error {
		err := twirpCall()

		if err == nil {
			return nil
		}

		twirpErr, ok := err.(twirp.Error)

		// Open the circuit for unknown (non-twirp) errors
		if !ok {
			return err
		}

		// Do not open the circuit for non-server errors
		if twirp.ServerHTTPStatusFromErrorCode(twirpErr.Code()) < 500 {
			innerErr = err
			return nil
		}

		// Open the circuit for server errors
		return err
	}, nil)

	if err == nil && innerErr != nil {
		err = innerErr
	}

	return err
}

// Wraps a twitch client request with hystrix - circuit is opened for unknown and 5xx errors
// Returns
//  hystrix error if circuit is open else
//  non-5xx error returned from twitch client call if non-nil else
//  nil
func CallTwitchClientWithHystrix(command string, hystrixDo HystrixDoer, twitchClientCall func() error) error {
	var innerErr error

	err := hystrixDo(command, func() error {
		err := twitchClientCall()

		if err == nil {
			return nil
		}

		statusCode := twitchclient.StatusCode(err)

		// Open the circuit for unknown (non-twitchclient) errors
		if statusCode == 0 {
			return err
		}

		// Do not open the circuit for non-server errors
		if statusCode < 500 {
			innerErr = err
			return nil
		}

		// Open the circuit for server errors
		return err
	}, nil)

	if err == nil && innerErr != nil {
		err = innerErr
	}

	return err
}
