package settings

import (
	"testing"

	"code.justin.tv/commerce/pagle/backend/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestConditionUtils_ValidateEffectSettings(t *testing.T) {
	Convey("given a ValidateEffectSettings util", t, func() {
		Convey("we should error if effect settings keys are duplicated", func() {
			mockSettings := []models.EffectSettings{
				{
					EffectType: models.GiveBits,
					Settings: &models.EffectTypeSettings{
						GiveBits: &models.GiveBitsSettings{},
					},
				},
				{
					EffectType: models.GiveBits,
					Settings: &models.EffectTypeSettings{
						GiveBits: &models.GiveBitsSettings{},
					},
				},
			}

			supportedEffects := []models.EffectType{models.GiveBits}

			validatedSettings, err := ValidateEffectSettings(supportedEffects, mockSettings)
			So(validatedSettings, ShouldHaveLength, 0)
			So(err, ShouldNotBeNil)
		})

		Convey("we should error if settings are not provided for a supported effect type", func() {
			supportedEffects := []models.EffectType{models.GiveBits}

			validatedSettings, err := ValidateEffectSettings(supportedEffects, []models.EffectSettings{})
			So(validatedSettings, ShouldHaveLength, 0)
			So(err, ShouldNotBeNil)
		})

		Convey("when validation passes", func() {
			Convey("we should return the validated settings", func() {
				mockSettings := []models.EffectSettings{
					{
						EffectType: models.GiveBits,
						Settings: &models.EffectTypeSettings{
							GiveBits: &models.GiveBitsSettings{},
						},
					},
				}

				supportedEffects := []models.EffectType{models.GiveBits}

				validatedSettings, err := ValidateEffectSettings(supportedEffects, mockSettings)
				So(validatedSettings, ShouldResemble, mockSettings)
				So(err, ShouldBeNil)
			})
		})
	})
}

func TestConditionUtils_IsEffectTypeSupportedByCondition(t *testing.T) {
	Convey("given a IsEffectTypeSupportedByCondition util", t, func() {
		mockCondition := models.Condition{
			SupportedEffects: []models.EffectType{
				models.GiveBits,
			},
		}

		Convey("we should return false if effect type is not supported", func() {
			isSupported := IsEffectTypeSupportedByCondition(mockCondition, models.EffectType("FAKE"))
			So(isSupported, ShouldBeFalse)
		})

		Convey("we should return true if effect type is supported", func() {
			isSupported := IsEffectTypeSupportedByCondition(mockCondition, models.GiveBits)
			So(isSupported, ShouldBeTrue)
		})
	})
}

func TestConditionUtils_GetEffectSettingsForEffectType(t *testing.T) {
	Convey("given a GetEffectSettingsForEffectType util", t, func() {
		mockBitsSettings := models.EffectSettings{
			EffectType: models.GiveBits,
			Settings:   &models.EffectTypeSettings{},
		}

		mockSettings := []models.EffectSettings{
			mockBitsSettings,
		}

		Convey("we should error if effect type is not found within settings", func() {
			settings, err := GetEffectSettingsForEffectType(models.EffectType("FAKE"), mockSettings)
			So(err, ShouldNotBeNil)
			So(settings, ShouldResemble, models.EffectSettings{})
		})

		Convey("we should return the settings if found", func() {
			settings, err := GetEffectSettingsForEffectType(models.GiveBits, mockSettings)
			So(err, ShouldBeNil)
			So(settings, ShouldResemble, mockBitsSettings)
		})
	})
}
