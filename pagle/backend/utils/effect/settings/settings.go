package settings

import (
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/models"
)

func ValidateEffectSettings(supportedEffects []models.EffectType, effectSettings []models.EffectSettings) ([]models.EffectSettings, error) {
	effectSettingsSet := map[models.EffectType]models.EffectSettings{}
	var dedupedEffectSettings []models.EffectSettings

	// Populate map, ensuring no duplicates
	for _, effectSetting := range effectSettings {
		if _, ok := effectSettingsSet[effectSetting.EffectType]; ok {
			return dedupedEffectSettings, errors.IllegalArgument.New("received multiple effect settings for the same EffectType")
		}

		effectSettingsSet[effectSetting.EffectType] = effectSetting
	}

	// Ensure settings are defined for each supported EffectType
	for _, supportedEffect := range supportedEffects {
		if _, ok := effectSettingsSet[supportedEffect]; !ok {
			return dedupedEffectSettings, errors.IllegalArgument.New("must provide effect settings for each supported EffectType")
		}
	}

	for _, effectSetting := range effectSettingsSet {
		dedupedEffectSettings = append(dedupedEffectSettings, effectSetting)
	}

	return dedupedEffectSettings, nil
}

func IsEffectTypeSupportedByCondition(condition models.Condition, effectType models.EffectType) bool {
	for _, supportedEffectType := range condition.SupportedEffects {
		if supportedEffectType == effectType {
			return true
		}
	}

	return false
}

func GetEffectSettingsForEffectType(effectType models.EffectType, effectSettings []models.EffectSettings) (models.EffectSettings, error) {
	for _, setting := range effectSettings {
		if setting.EffectType == effectType {
			return setting, nil
		}
	}

	message := "could not find effect settings for effectType"
	logrus.WithField("effectType", effectType).Error(message)

	return models.EffectSettings{}, errors.IllegalArgument.New(message)
}
