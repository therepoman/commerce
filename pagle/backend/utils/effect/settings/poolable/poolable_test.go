package poolable

import (
	"testing"

	"code.justin.tv/commerce/pagle/backend/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPoolableEffectUtils_ValidatePoolRecipients(t *testing.T) {
	Convey("given a validate pool recipients util", t, func() {
		Convey("it should error when ToUserID is blank at least once", func() {
			mockPoolRecipientsPercentage := []models.PoolRecipientWeightedShare{
				{
					ShareWeight: 1,
					ToUserID:    "channel-1",
				},
				{
					ShareWeight: 1,
					ToUserID:    "",
				},
			}

			err := ValidatePoolRecipients(mockPoolRecipientsPercentage)
			So(err, ShouldNotBeNil)
		})

		Convey("it should error when sum of share weights is less than or equal to zero", func() {
			mockPoolRecipientsPercentage := []models.PoolRecipientWeightedShare{
				{
					ShareWeight: 0,
					ToUserID:    "channel-1",
				},
				{
					ShareWeight: 0,
					ToUserID:    "channel-2",
				},
			}

			err := ValidatePoolRecipients(mockPoolRecipientsPercentage)
			So(err, ShouldNotBeNil)

			mockPoolRecipientsPercentage = []models.PoolRecipientWeightedShare{
				{
					ShareWeight: 0,
					ToUserID:    "channel-1",
				},
				{
					ShareWeight: -1,
					ToUserID:    "channel-2",
				},
			}

			err = ValidatePoolRecipients(mockPoolRecipientsPercentage)
			So(err, ShouldNotBeNil)
		})
	})
}
