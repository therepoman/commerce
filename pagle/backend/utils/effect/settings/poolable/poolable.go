package poolable

import (
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/models"
)

func ValidatePoolRecipients(poolRecipients []models.PoolRecipientWeightedShare) error {
	var totalShareWeight int64

	for _, recipient := range poolRecipients {

		if strings.Blank(recipient.ToUserID) {
			return errors.IllegalArgument.New("must provide an owner_id for each recipient")
		}

		totalShareWeight += recipient.ShareWeight
	}

	if totalShareWeight <= 0 {
		return errors.IllegalArgument.New("sum of recipient share weights must be greater than zero")
	}

	return nil
}
