package condition

import "code.justin.tv/commerce/pagle/backend/models"

func IsConditionActive(condition models.Condition) bool {
	return condition.ConditionState == models.ConditionActive
}
