package condition

import (
	"testing"

	"code.justin.tv/commerce/pagle/backend/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestConditionUtils_IsConditionActive(t *testing.T) {
	Convey("given a IsConditionActive util", t, func() {
		Convey("we should return true when condition state is active", func() {
			active := IsConditionActive(models.Condition{ConditionState: models.ConditionActive})
			So(active, ShouldBeTrue)
		})

		Convey("we should return false when condition state is anything else", func() {
			active := IsConditionActive(models.Condition{ConditionState: models.ConditionInactive})
			So(active, ShouldBeFalse)

			active = IsConditionActive(models.Condition{ConditionState: models.ConditionCanceled})
			So(active, ShouldBeFalse)

			active = IsConditionActive(models.Condition{ConditionState: models.ConditionExpired})
			So(active, ShouldBeFalse)
		})
	})
}
