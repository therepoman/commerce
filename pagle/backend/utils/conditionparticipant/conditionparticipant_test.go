package conditionparticipant

import (
	"testing"
	"time"

	"code.justin.tv/commerce/pagle/backend/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestConditionParticipantUtils_HasConditionParticipantExpired(t *testing.T) {
	Convey("given a HasConditionParticipantExpired util", t, func() {
		Convey("we should return true when conditionParticipant has expired", func() {
			conditionRun := models.ConditionRun{
				CompletedAt: time.Now(),
			}
			conditionParticipant := models.ConditionParticipant{
				CreatedAt:  conditionRun.CompletedAt.Add(-time.Hour),
				TTLSeconds: 1, // Expires just under an hour before condition run completion
			}
			expired := DidConditionParticipantExpireBeforeConditionRunCompletion(conditionParticipant, conditionRun)
			So(expired, ShouldBeTrue)
		})

		Convey("we should return false when conditionParticipant has not expired", func() {
			conditionRun := models.ConditionRun{
				CompletedAt: time.Now(),
			}
			conditionParticipant := models.ConditionParticipant{
				CreatedAt:  conditionRun.CompletedAt.Add(-time.Minute),
				TTLSeconds: 60 * 5, // Expires 4 minutes after condition run completion
			}
			expired := DidConditionParticipantExpireBeforeConditionRunCompletion(conditionParticipant, conditionRun)
			So(expired, ShouldBeFalse)
		})
	})
}

func TestConditionParticipantUtils_PartitionExpiredConditionParticipants(t *testing.T) {
	Convey("given a PartitionExpiredConditionParticipants util", t, func() {
		Convey("we should partition conditionParticipants based on whether or not they have expired", func() {
			mockConditionRun := models.ConditionRun{
				CompletedAt: time.Now(),
			}

			mockExpiredConditionParticipant := models.ConditionParticipant{
				CreatedAt:  mockConditionRun.CompletedAt.Add(-time.Hour),
				TTLSeconds: 1, // Expires just under an hour before condition run completion
			}

			mockActiveConditionParticipant := models.ConditionParticipant{
				CreatedAt:  mockConditionRun.CompletedAt.Add(-time.Minute),
				TTLSeconds: 60 * 5, // Expires 5 minutes after condition run completion
			}

			partitionedConditionParticipants := PartitionExpiredConditionParticipants([]*models.ConditionParticipant{
				&mockExpiredConditionParticipant,
				&mockActiveConditionParticipant,
			}, mockConditionRun)

			So(partitionedConditionParticipants, ShouldResemble, TTLPartitionedConditionParticipants{
				Active:  []models.ConditionParticipant{mockActiveConditionParticipant},
				Expired: []models.ConditionParticipant{mockExpiredConditionParticipant},
			})
		})
	})
}
