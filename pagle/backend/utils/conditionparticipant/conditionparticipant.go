package conditionparticipant

import (
	"time"

	"code.justin.tv/commerce/pagle/backend/models"
)

// DidConditionParticipantExpireBeforeConditionRunCompletion checks if a condition participant has expired, based on whether its expiry time occurs
// before the associated condition run completion time.
func DidConditionParticipantExpireBeforeConditionRunCompletion(conditionParticipant models.ConditionParticipant, conditionRun models.ConditionRun) bool {
	expiresAt := conditionParticipant.CreatedAt.Add(time.Second * time.Duration(conditionParticipant.TTLSeconds))
	return expiresAt.Before(conditionRun.CompletedAt)
}

type TTLPartitionedConditionParticipants struct {
	Active  []models.ConditionParticipant
	Expired []models.ConditionParticipant
}

func PartitionExpiredConditionParticipants(conditionParticipants []*models.ConditionParticipant, conditionRun models.ConditionRun) TTLPartitionedConditionParticipants {
	var activeConditionParticipants []models.ConditionParticipant
	var expiredConditionParticipants []models.ConditionParticipant

	for _, conditionParticipant := range conditionParticipants {
		if conditionParticipant == nil {
			continue
		}

		if DidConditionParticipantExpireBeforeConditionRunCompletion(*conditionParticipant, conditionRun) {
			expiredConditionParticipants = append(expiredConditionParticipants, *conditionParticipant)
			continue
		}
		activeConditionParticipants = append(activeConditionParticipants, *conditionParticipant)
	}

	return TTLPartitionedConditionParticipants{
		Active:  activeConditionParticipants,
		Expired: expiredConditionParticipants,
	}
}
