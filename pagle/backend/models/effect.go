package models

import "github.com/shopspring/decimal"

/*
	Effect
*/

type EffectType string

const (
	GiveBits = EffectType("GIVE_BITS")
)

// This solution contrasts the proto approach, which uses interfaces to model "oneof" type structures.
// The interface solution does not work with our Dynamo ORM, as it cannot serialize an interface type.
type EffectParams struct {
	GiveBits *EffectParamsGiveBits `dynamodbav:"give_bits" json:"give_bits"`
}

type EffectParamsGiveBits struct {
	PoolableInput EffectInputPoolable `dynamodbav:"poolable_input"  json:"poolable_input"`
}

type EffectOutput struct {
	GiveBits *EffectOutputGiveBits `dynamodbav:"give_bits" json:"give_bits"`
}

type EffectOutputGiveBits struct{}

type EffectRollupOutput struct {
	GiveBits *EffectRollupOutputGiveBits `dynamodbav:"give_bits" json:"give_bits"`
}

type EffectRollupOutputGiveBits struct {
	Recipients []BitsPoolRecipientFixedShare `dynamodbav:"recipients" json:"recipients"`
}

type Effect struct {
	EffectType    EffectType   `dynamodbav:"effect_type"    json:"effect_type"`
	Params        EffectParams `dynamodbav:"params"         json:"params"`
	Output        EffectOutput `dynamodbav:"output"         json:"output"`
	TransactionID string       `dynamodbav:"transaction_id" json:"transaction_id"` // Created by the effect tenant
}

type EffectInputPoolable struct {
	Amount     int32  `dynamodbav:"amount"       json:"amount"`
	FromUserID string `dynamodbav:"from_user_id" json:"from_user_id"`
}

/*
	Recipient Share
*/

type PoolRecipientWeightedShare struct {
	ShareWeight int64
	ToUserID    string
}

// Intermediate type, only used for calculating fixed pool shares. Shares should be stored and passed around
// as int64 all the way until they are passed to PreProcessConditionParticipants.
type BitsPoolRecipientWeightedShare struct {
	ShareWeight             decimal.Decimal
	ToUserID                string
	ForExtensionDevRevShare bool
}

type BitsPoolRecipientFixedShare struct {
	Amount                  int32  `dynamodbav:"amount"     json:"amount"`
	ToUserID                string `dynamodbav:"to_user_id" json:"to_user_id"`
	ForExtensionDevRevShare bool   `dynamodbav:"for_extension_dev_rev_share" json:"for_extension_dev_rev_share"`
}

/*
	Effect Settings
	Defined either
		a) At time of condition creation or
		b) At time of condition satisfaction

	Defining it at the time of condition creation makes subsequent definition unnecessary and invalid
*/

type EffectTypeSettings struct {
	GiveBits *GiveBitsSettings `dynamodbav:"give_bits"`
}

type EffectSettings struct {
	EffectType EffectType          `dynamodbav:"effect_type"`
	Settings   *EffectTypeSettings `dynamodbav:"settings"`
}

type GiveBitsSettings struct {
	PoolRecipients []PoolRecipientWeightedShare
}
