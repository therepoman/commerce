package models

import (
	"time"

	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

/*
	Condition Aggregates
*/

const (
	AggregateTotalDocumentPattern      = "condition_participants.total"
	StateAggregateTotalDocumentPattern = "condition_participants.%s.total"

	GiveBitsTotalBitsAmountAggregateDocumentPattern = "condition_participants.%s.effects.give_bits.total_bits_amount"
	GiveBitsTotalEffectsAggregateDocumentPattern    = "condition_participants.%s.effects.give_bits.total_effects"
)

type BatchGetConditionAggregateParams struct {
	Domain      string
	OwnerID     string
	ConditionID string
}

type ConditionParticipantEffectAggregateUpdates struct {
	NameBuilder    expression.NameBuilder
	OperandBuilder expression.OperandBuilder
}

// The tenant should return fields to increment if `incrementField` is non-nil, fields to decrement if
// `decrementField` is non-nil, and both if both fields are non-nil.
type EffectAggregateGetter func(incrementField, decrementField *string) ([]ConditionParticipantEffectAggregateUpdates, error)

type GiveBitsEffectSummary struct {
	TotalEffects    int64 `dynamodbav:"total_effects"`
	TotalBitsAmount int64 `dynamodbav:"total_bits_amount"`
}

type EffectSummary struct {
	GiveBits GiveBitsEffectSummary `dynamodbav:"give_bits"`
}

type ConditionParticipantAggregatesForState struct {
	Total   int64         `dynamodbav:"total"`
	Effects EffectSummary `dynamodbav:"effects"`
}

type ConditionParticipantAggregates struct {
	Total             int64                                  `dynamodbav:"total"`
	Pending           ConditionParticipantAggregatesForState `dynamodbav:"pending"`
	Satisfied         ConditionParticipantAggregatesForState `dynamodbav:"satisfied"`
	ConditionCanceled ConditionParticipantAggregatesForState `dynamodbav:"condition_canceled"`
	ConditionTimeout  ConditionParticipantAggregatesForState `dynamodbav:"condition_timeout"`
	FailedValidation  ConditionParticipantAggregatesForState `dynamodbav:"failed_validation"`
	Canceled          ConditionParticipantAggregatesForState `dynamodbav:"canceled"`
	Timeout           ConditionParticipantAggregatesForState `dynamodbav:"timeout"`
}

type ConditionAggregates struct {
	ConditionID           string                         `dynamodbav:"condition_id"`
	Domain                string                         `dynamodbav:"domain"`
	OwnerID               string                         `dynamodbav:"owner_id"`
	DomainOwnerID         string                         `dynamodbav:"domain-owner_id"`
	UpdatedAt             time.Time                      `dynamodbav:"updated_at"`
	ConditionParticipants ConditionParticipantAggregates `dynamodbav:"condition_participants"`
}
