package models

import "time"

/*
	Condition Run
*/
type ConditionRunEndState string

const (
	ConditionRunEndStatePending   = ConditionRunEndState("PENDING")
	ConditionRunEndStateSatisfied = ConditionRunEndState("SATISFIED")
	ConditionRunEndStateCanceled  = ConditionRunEndState("CANCELED")
	ConditionRunEndStateTimeout   = ConditionRunEndState("TIMEOUT")
)

type ConditionRun struct {
	ConditionRunID       string               `dynamodbav:"condition_run_id"`
	ConditionID          string               `dynamodbav:"condition_id"`
	CreatedAt            time.Time            `dynamodbav:"created_at"`
	UpdatedAt            time.Time            `dynamodbav:"updated_at"`
	CompletedAt          time.Time            `dynamodbav:"completed_at"`
	EffectSettings       []EffectSettings     `dynamodbav:"effect_settings"`
	ConditionRunEndState ConditionRunEndState `dynamodbav:"condition_run_end_state"`
	TTL                  time.Time            `dynamodbav:"ttl"`
}
