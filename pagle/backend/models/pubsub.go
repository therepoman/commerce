package models

type PubsubEventType string

type PubsubEvent struct {
	Type PubsubEventType `json:"type"`
	Data interface{}     `json:"data"`
}

const (
	// Topic for subscribing to condition updates. This is intended for any public subscriber.
	PubsubTopicConditions = "challenge-conditions"

	// Topic for subscribing to condition participant updates. This is intended for specific subscribers ONLY
	// and will require authenticated subscribers.
	PubsubTopicConditionParticipants = "challenge-condition-participants"

	// Event types
	PubsubEventTypeConditionCreate    = PubsubEventType("CONDITION_CREATE")
	PubsubEventTypeConditionActive    = PubsubEventType("CONDITION_ACTIVE")
	PubsubEventTypeConditionCanceled  = PubsubEventType("CONDITION_CANCELED")
	PubsubEventTypeConditionExpired   = PubsubEventType("CONDITION_EXPIRED")
	PubsubEventTypeConditionInactive  = PubsubEventType("CONDITION_INACTIVE")
	PubsubEventTypeConditionSatisfied = PubsubEventType("CONDITION_SATISFIED")

	PubsubEventTypeConditionParticipantCreate            = PubsubEventType("CONDITION_PARTICIPANT_CREATE")
	PubsubEventTypeConditionParticipantPendingCompletion = PubsubEventType("CONDITION_PARTICIPANT_PENDING_COMPLETION")
	PubsubEventTypeConditionParticipantConditionCanceled = PubsubEventType("CONDITION_PARTICIPANT_CONDITION_CANCELED")
	PubsubEventTypeConditionParticipantConditionExpired  = PubsubEventType("CONDITION_PARTICIPANT_CONDITION_EXPIRED")
	PubsubEventTypeConditionParticipantSatisfied         = PubsubEventType("CONDITION_PARTICIPANT_SATISFIED")
	PubsubEventTypeConditionParticipantExpired           = PubsubEventType("CONDITION_PARTICIPANT_EXPIRED")
	PubsubEventTypeConditionParticipantCanceled          = PubsubEventType("CONDITION_PARTICIPANT_CANCELED")
)

type PubsubEventDataCondition struct {
	ConditionID    string `json:"conditionID"`
	OwnerID        string `json:"ownerID"`
	Domain         string `json:"domain"`
	ConditionState string `json:"conditionState"`
}

func ConditionToPubsubEventDataCondition(condition Condition) PubsubEventDataCondition {
	return PubsubEventDataCondition{
		ConditionID:    condition.ConditionID,
		OwnerID:        condition.OwnerID,
		Domain:         condition.Domain,
		ConditionState: string(condition.ConditionState),
	}
}

type PubsubEventDataConditionParticipant struct {
	ConditionParticipantID       string `json:"conditionParticipantID"`
	OwnerID                      string `json:"ownerID"`
	Domain                       string `json:"domain"`
	ConditionID                  string `json:"conditionID"`
	ConditionOwnerID             string `json:"conditionOwnerID"`
	ConditionParticipantEndState string `json:"conditionParticipantEndState"`
}

func ConditionParticipantToPubsubEventDataConditionParticipant(conditionParticipant ConditionParticipant) PubsubEventDataConditionParticipant {
	return PubsubEventDataConditionParticipant{
		ConditionParticipantID:       conditionParticipant.ConditionParticipantID,
		OwnerID:                      conditionParticipant.OwnerID,
		Domain:                       conditionParticipant.Domain,
		ConditionID:                  conditionParticipant.ConditionID,
		ConditionOwnerID:             conditionParticipant.ConditionOwnerID,
		ConditionParticipantEndState: string(conditionParticipant.ConditionParticipantEndState),
	}
}
