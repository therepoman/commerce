package models

/*
	S3 Input
*/
type FinalizedConditionParticipantS3Input struct {
	ConditionParticipantID           string                       `json:"condition_participant_id"`
	ConditionParticipantEndState     ConditionParticipantEndState `json:"condition_participant_end_state"`
	ConditionParticipantEffectOutput *EffectOutput                `json:"condition_participant_effect_outputs"`
}

type FinalizedEffectRollupS3Input struct {
	EffectType   EffectType          `json:"effect_type"`
	EffectRollup *EffectRollupOutput `json:"effect_rollup"`
}

type FinalizedConditionRunS3Input struct {
	ConditionParticipants []FinalizedConditionParticipantS3Input `json:"condition_participants"`
	EffectRollups         []FinalizedEffectRollupS3Input         `json:"effect_rollups"`
}

/*
	S3 Effect Static Config
*/
type EffectStaticConfig struct {
	GiveBits *GiveBitsStaticConfig `json:"give_bits"`
}

type GiveBitsStaticConfig struct {
	ShouldEnforceWhitelist      bool     `json:"should_enforce_whitelist"` // TODO: Rename this, now ambiguous w/ addition of rev share blacklist
	BenefactorUserWhitelist     []string `json:"benefactor_user_whitelist"`
	RecipientUserWhitelist      []string `json:"recipient_user_whitelist"`
	ExtensionRevShareBlacklist  []string `json:"extension_rev_share_blacklist"`
	ExtensionRevSharePercentage float64  `json:"extension_rev_share_percentage"`
}
