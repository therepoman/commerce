package models

/*
	SNS Messages
*/
type ConditionParticipantProcessingSNSMessage struct {
	ConditionParticipantID            string                       `json:"condition_participant_id"`
	ConditionRunID                    string                       `json:"condition_run_id"`
	ConditionParticipantEndState      ConditionParticipantEndState `json:"condition_participant_end_state"`
	EffectOutput                      *EffectOutput                `json:"effect_output"`
	TriggeredByConditionRunCompletion bool                         `json:"triggered_by_condition_run_completion"`
}

type ConditionParticipantCleanupSNSMessage struct {
	ConditionParticipantID string `json:"condition_participant_id"`
	ConditionRunID         string `json:"condition_run_id"`
}

type ConditionRunProcessingSNSMessage struct {
	ConditionID    string `json:"condition_id"`
	ConditionRunID string `json:"condition_run_id"`
	OwnerID        string `json:"owner_id"`
	Domain         string `json:"domain"`
}

type ConditionParticipantCompletionSNSMessage struct {
	ConditionParticipantID              string                              `json:"condition_participant_id"`
	ConditionParticipantProcessingState ConditionParticipantProcessingState `json:"condition_participant_processing_state"`
	ConditionID                         string                              `json:"condition_id"`
	ConditionParticipantEndState        ConditionParticipantEndState        `json:"condition_participant_end_state"`
}

type ConditionAggregatesRetrySNSMessage struct {
	ConditionParticipant ConditionParticipant `json:"condition_participant"`
}

type ConditionCompletionSNSMessage struct {
	ConditionID    string         `json:"condition_id"`
	OwnerID        string         `json:"owner_id"`
	Domain         string         `json:"domain"`
	ConditionState ConditionState `json:"condition_state"`
}
