package models

import "time"

/*
	SFN Input
*/
type TimeoutConditionParticipantSFNInput struct {
	ConditionParticipantID string `json:"condition_participant_id"`
	ConditionRunID         string `json:"condition_run_id"`
	TTLSeconds             int32  `json:"ttl_seconds"`
}

type TimeoutConditionSFNInput struct {
	Domain      string    `json:"domain"`
	ConditionID string    `json:"condition_id"`
	OwnerID     string    `json:"owner_id"`
	TimeoutDate time.Time `json:"timeout_date"`
}

type ConditionRunProcessingSFNInput struct {
	Domain         string `json:"domain"`
	ConditionID    string `json:"condition_id"`
	OwnerID        string `json:"owner_id"`
	ConditionRunID string `json:"condition_run_id"`
}

type FinalizeConditionRunSFNOutput struct {
	HasParticipantsToProcess bool   `json:"has_participants_to_process"`
	Domain                   string `json:"domain"`
	ConditionID              string `json:"condition_id"`
	OwnerID                  string `json:"owner_id"`
	ConditionRunID           string `json:"condition_run_id"`
}

type UserDeletionRequest interface {
	GetUserDeletionRequestData() *UserDeletionRequestData
}

type UserDeletionRequestData struct {
	UserIDs        []string `json:"user_ids"`
	IsDryRun       bool     `json:"is_dry_run"`
	ReportDeletion bool     `json:"report_deletion"`
}

type UserDeletionResponseData struct {
	UserIDs        []string `json:"user_ids"`
	IsDryRun       bool     `json:"is_dry_run"`
	ReportDeletion bool     `json:"report_deletion"`
}

type StartUserDeletionRequest struct {
	UserDeletionRequestData
}

func (r *StartUserDeletionRequest) GetUserDeletionRequestData() *UserDeletionRequestData {
	if r == nil {
		return nil
	}

	return &r.UserDeletionRequestData
}

type ReportDeletionRequest struct {
	UserDeletionRequestData
}

func (r *ReportDeletionRequest) GetUserDeletionRequestData() *UserDeletionRequestData {
	if r == nil {
		return nil
	}

	return &r.UserDeletionRequestData
}

type ReportDeletionResponse struct {
	UserDeletionResponseData
	Timestamp time.Time `json:"timestamp"`
}

type CleanConditionsRequest struct {
	UserDeletionRequestData
}

func (r *CleanConditionsRequest) GetUserDeletionRequestData() *UserDeletionRequestData {
	if r == nil {
		return nil
	}

	return &r.UserDeletionRequestData
}

type CleanConditionsResponse struct {
	UserDeletionResponseData
}

type CleanConditionAggregatesRequest struct {
	UserDeletionRequestData
}

func (r *CleanConditionAggregatesRequest) GetUserDeletionRequestData() *UserDeletionRequestData {
	if r == nil {
		return nil
	}

	return &r.UserDeletionRequestData
}

type CleanConditionsAggregatesResponse struct {
	UserDeletionResponseData
}

type CleanConditionParticipantsRequest struct {
	UserDeletionRequestData
}

func (r *CleanConditionParticipantsRequest) GetUserDeletionRequestData() *UserDeletionRequestData {
	if r == nil {
		return nil
	}

	return &r.UserDeletionRequestData
}

type CleanConditionParticipantsResponse struct {
	UserDeletionResponseData
}
