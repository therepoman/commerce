package commands

import "github.com/afex/hystrix-go/hystrix"

const (
	// redis
	RedisGetCommand = "redis_get"
	RedisSetCommand = "redis_set"
	RedisDelCommand = "redis_del"

	// PaydayRPC
	PaydayRPCCreateHoldCommand   = "payday_rpc_create_hold"
	PaydayRPCFinalizeHoldCommand = "payday_rpc_finalize_hold"
	PaydayRPCReleaseHoldCommand  = "payday_rpc_release_hold"

	// Payday
	PaydayGetChannelInfoCommand = "payday_get_channel_info"

	// EMS
	EMSGetExtensionsByChannelIDCommand = "ems_get_extensions_by_channel_id"

	// Owl
	OwlGetClientCommand = "owl_get_client"

	// Ripley
	RipleyGetPayoutTypeCommand = "get_payout_type"

	// Dynamo
	DynamoTransactionCommand                       = "dynamo_transaction"
	DynamoGetConditionRunCommand                   = "dynamo_condition_run_get"
	DynamoCreateConditionRunCommand                = "dynamo_condition_run_create"
	DynamoCompleteConditionRunCommand              = "dynamo_condition_run_complete"
	DynamoGetConditionCommand                      = "dynamo_condition_get"
	DynamoBatchGetConditionCommand                 = "dynamo_condition_batch_get"
	DynamoSetConditionRunIDCommand                 = "dynamo_condition_set_condition_run_id"
	DynamoSetConditionStateCommand                 = "dynamo_condition_set_condition_state"
	DynamoGetConditionsByOwnerCommand              = "dynamo_condition_get_conditions_by_owner"
	DynamoQueryConditionsByExtensionOwnerIdCommand = "dynamo_condition_query_conditions_by_extension_owner_id"
	DynamoQueryConditionsByOwnerIdCommand          = "dynamo_condition_query_conditions_by_owner_id"

	DynamoGetConditionParticipantCommand                          = "dynamo_condition_participant_get"
	DynamoGetConditionParticipantByDomainAndOwnerAndIDCommand     = "dynamo_condition_participant_get_by_domain_and_owner_and_id"
	DynamoGetConditionParticipantsByDomainAndOwnerIDCommand       = "dynamo_condition_participants_get_by_domain_and_owner"
	DynamoGetConditionParticipantsByOwnerIDCommand                = "dynamo_condition_participants_get_by_owner"
	DynamoGetConditionParticipantsByConditionOwnerIDCommand       = "dynamo_condition_participants_get_by_conditon_owner"
	DynamoGetPendingConditionParticipantsByRunCommand             = "dynamo_condition_participant_get_pending_by_run"
	DynamoCreateConditionParticipantCommand                       = "dynamo_condition_participant_create"
	DynamoUpdateConditionParticipantCommand                       = "dynamo_condition_participant_update"
	DynamoGetConditionParticipantsByOwnerAndConditionOwnerCommand = "dynamo_condition_participant_get_by_owner_and_condition_owner"
	DynamoGetConditionAggregatesCommand                           = "dynamo_condition_aggregates_get"
	DynamoGetConditionAggregatesByOwnerIDCommand                  = "dynamo_condition_aggregates_get_by_owner_id"
	DynamoBatchGetConditionAggregatesCommand                      = "dynamo_condition_aggregates_batch_get"
	DynamoDeleteConditionCommand                                  = "dynamo_condition_delete"
	DynamoDeleteConditionParticipantCommand                       = "dynamo_condition_participant_delete"
	DynamoDeleteConditionAggregateCommand                         = "dynamo_condition_aggregate_delete"

	// Max concurrency per command
	DefaultMaxConcurrent = 1000
)

// General philosophy
//   * Commands called in async flows should have looser timeouts
//   * Commands getting/setting multiple items (e.g. Dynamo BatchGet and TransactWriteItems) should have looser timeouts
//   * Timeouts should be substantially higher than p99 (at least until we have more perf data)
func init() {
	// redis
	configureTimeout(RedisGetCommand, 300)
	configureTimeout(RedisSetCommand, 300)
	configureTimeout(RedisDelCommand, 300)

	// PaydayRPC
	configureTimeout(PaydayRPCCreateHoldCommand, 2500)   // p99-500ms, payday-> pachinko max-2000ms (as of 8/24/19)
	configureTimeout(PaydayRPCFinalizeHoldCommand, 2500) // p99-300ms, payday-> pachinko max-2000ms (as of 8/24/19)
	configureTimeout(PaydayRPCReleaseHoldCommand, 2500)  // p99-300ms, payday-> pachinko max-2000ms (as of 8/24/19)

	// Payday
	configureTimeout(PaydayGetChannelInfoCommand, 500) // p99-10ms, 4 week max-480ms (as of 8/24/19)

	// Owl
	configureTimeout(OwlGetClientCommand, 2500)

	// EMS
	configureTimeout(EMSGetExtensionsByChannelIDCommand, 5000)

	// Ripley
	configureTimeout(RipleyGetPayoutTypeCommand, 5000)

	// Dynamo
	configureTimeout(DynamoTransactionCommand, 5000)                                      // p99-250ms (as of 8/26/19), but we expect much larger transactions from streams w/ prod traffic
	configureTimeout(DynamoGetConditionRunCommand, 500)                                   // p99-5ms (as of 8/26/19)
	configureTimeout(DynamoCreateConditionRunCommand, 500)                                // p99-10ms (as of 8/26/19)
	configureTimeout(DynamoCompleteConditionRunCommand, 500)                              // p99-15ms (as of 8/26/19)
	configureTimeout(DynamoGetConditionCommand, 500)                                      // p99-20ms (as of 8/26/19)
	configureTimeout(DynamoBatchGetConditionCommand, 1500)                                // TODO: Determine p99
	configureTimeout(DynamoSetConditionRunIDCommand, 500)                                 // p99-10ms (as of 8/26/19)
	configureTimeout(DynamoSetConditionStateCommand, 500)                                 // p99-10ms (as of 8/26/19)
	configureTimeout(DynamoGetConditionsByOwnerCommand, 2000)                             // p99-120ms (as of 8/26/19)
	configureTimeout(DynamoGetConditionParticipantCommand, 500)                           // p99-15ms (as of 8/26/19)
	configureTimeout(DynamoGetConditionParticipantByDomainAndOwnerAndIDCommand, 500)      // p99-15ms (as of 8/26/19)
	configureTimeout(DynamoGetConditionParticipantsByDomainAndOwnerIDCommand, 500)        // p99-15ms (as of 7/15/20)
	configureTimeout(DynamoGetPendingConditionParticipantsByRunCommand, 2500)             // p99-20ms (as of 8/26/19), but we expect this to drastically increase w/ large conditions
	configureTimeout(DynamoCreateConditionParticipantCommand, 500)                        // p99-10ms (as of 8/26/19)
	configureTimeout(DynamoUpdateConditionParticipantCommand, 500)                        // p99-10ms (as of 8/26/19)
	configureTimeout(DynamoGetConditionParticipantsByOwnerAndConditionOwnerCommand, 2000) // p99-20ms (as of 8/26/19)
	configureTimeout(DynamoGetConditionAggregatesCommand, 500)                            // p99-10ms (as of 8/26/19)
	configureTimeout(DynamoBatchGetConditionAggregatesCommand, 1500)                      // p99-15ms (as of 8/26/19), but we expect this to increase by as much as 100x once GQL loader is fully utilized
}

func configureTimeout(cmd string, timeout int) {
	configure(cmd, timeout, DefaultMaxConcurrent)
}

func configure(cmd string, timeout int, maxConcurrent int) {
	hystrix.ConfigureCommand(cmd, hystrix.CommandConfig{
		Timeout:               timeout,
		MaxConcurrentRequests: maxConcurrent,
		ErrorPercentThreshold: hystrix.DefaultErrorPercentThreshold,
	})
}
