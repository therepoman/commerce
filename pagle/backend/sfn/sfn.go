package sfn

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/backend/stats"
	"code.justin.tv/commerce/pagle/config"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/aws/aws-sdk-go/service/sfn/sfniface"
)

const (
	startExecutionLatencyStatName = "sfn_start_execution_latency"
	startExecutionErrorStatName   = "sfn_start_execution_error"
)

type SFNClient interface {
	ExecuteTimeoutConditionStateMachine(executionInput models.TimeoutConditionSFNInput) error
	ExecuteTimeoutConditionParticipantStateMachine(executionInput models.TimeoutConditionParticipantSFNInput) error
	ExecuteConditionRunProcessingStateMachine(executionInput models.ConditionRunProcessingSFNInput) error
	ExecutePDMSUserDeletion(executionName string, executionInput models.StartUserDeletionRequest) error
}

func NewSFNClient() SFNClient {
	return &sfnClientImpl{}
}

type sfnClientImpl struct {
	SFN     sfniface.SFNAPI `inject:""`
	Config  *config.Config  `inject:""`
	Statter stats.Statter   `inject:""`
}

func (s *sfnClientImpl) ExecuteTimeoutConditionStateMachine(executionInput models.TimeoutConditionSFNInput) error {
	executionName := makeExecutionNameForTimeoutMachine("ttlcond", executionInput.ConditionID)
	return s.execute(s.Config.SFN.TimeoutConditionARN, executionName, executionInput)
}

func (s *sfnClientImpl) ExecuteTimeoutConditionParticipantStateMachine(executionInput models.TimeoutConditionParticipantSFNInput) error {
	executionName := makeExecutionNameForTimeoutMachine("ttlcondparticipant", executionInput.ConditionParticipantID)
	return s.execute(s.Config.SFN.TimeoutConditionParticipantARN, executionName, executionInput)
}

func (s *sfnClientImpl) ExecuteConditionRunProcessingStateMachine(executionInput models.ConditionRunProcessingSFNInput) error {
	executionName := fmt.Sprintf("condrunprocessing-%s", executionInput.ConditionRunID)
	return s.execute(s.Config.SFN.ConditionRunProcessingARN, executionName, executionInput)
}

func (s *sfnClientImpl) ExecutePDMSUserDeletion(executionName string, executionInput models.StartUserDeletionRequest) error {
	return s.execute(s.Config.SFN.PDMSUserDeletionARN, executionName, executionInput)
}

func (s *sfnClientImpl) statExecuteError() {
	s.Statter.Inc(startExecutionErrorStatName, 1, 1)
}

func (s *sfnClientImpl) execute(stateMachineARN string, executionName string, executionInput interface{}) error {
	startTime := time.Now()

	defer func() {
		s.Statter.TimingDuration(startExecutionLatencyStatName, time.Since(startTime), 1)
	}()

	inputJSON, err := json.Marshal(executionInput)

	if err != nil {
		s.statExecuteError()
		return err
	}

	startExecInput := &sfn.StartExecutionInput{
		Input:           pointers.StringP(string(inputJSON)),
		Name:            pointers.StringP(executionName),
		StateMachineArn: pointers.StringP(stateMachineARN),
	}

	_, err = s.SFN.StartExecution(startExecInput)

	if err != nil {
		s.statExecuteError()
		return err
	}

	return nil
}

func makeExecutionNameForTimeoutMachine(namePrefix, uuid string) string {
	// Name field of step function can only be 80 characters max, sampling last four characters of the unix timestamp
	// should provide sufficient uniqueness, even if we allow timeouts to be re-scheduled in the future (as of
	// 3/25/19, this will only happen once at creation.
	unixTimestamp := strconv.FormatInt(time.Now().Unix(), 10)
	unixTimestampLastFourChars := unixTimestamp[len(unixTimestamp)-4:]

	return fmt.Sprintf("%s__%s__%s", namePrefix, uuid, unixTimestampLastFourChars)
}
