package create

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/pagle/backend/models"
	effecttotals_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/condition/effecttotals"
	conditionruncreator_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/conditionrun/create"
	conditiondao_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/dynamo/condition"
	transactiondao_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/dynamo/transaction"
	effectcontroller_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/effect"
	ems_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/ems"
	pubsub_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/pubsub"
	sfnclient_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/sfn"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestCreator_CreateCondition(t *testing.T) {
	Convey("given a condition controller", t, func() {
		conditionDAO := new(conditiondao_mock.DAO)
		transactionDAO := new(transactiondao_mock.DAO)
		conditionRunCreator := new(conditionruncreator_mock.Creator)
		conditionAggregates := new(effecttotals_mock.EffectTotals)
		effectController := new(effectcontroller_mock.EffectController)
		sfnClient := new(sfnclient_mock.SFNClient)
		pubsubClient := new(pubsub_mock.PubsubClient)
		emsClient := new(ems_mock.EMSClient)

		controller := creatorImpl{
			ConditionDAO:                  conditionDAO,
			TransactionDAO:                transactionDAO,
			ConditionAggregatesController: conditionAggregates,
			ConditionRunCreator:           conditionRunCreator,
			EffectController:              effectController,
			SFNClient:                     sfnClient,
			PubsubClient:                  pubsubClient,
			EMSClient:                     emsClient,
		}

		ownerID := "foo-owner-id"
		name := "foo-name"
		description := "foo-description"
		mockDomain := "test-domain"
		shouldDisableWhenSatisfied := false
		defineEffectSettingsWhenSatisfied := false
		timeoutTime := time.Now()
		effectSettings := []models.EffectSettings{
			{
				EffectType: models.GiveBits,
				Settings: &models.EffectTypeSettings{
					GiveBits: &models.GiveBitsSettings{
						PoolRecipients: []models.PoolRecipientWeightedShare{
							{
								ShareWeight: 1,
								ToUserID:    ownerID,
							},
						},
					},
				},
			},
		}
		supportedEffects := []models.EffectType{models.GiveBits}
		var extension *models.Extension

		pubsubClient.On("PublishConditionCreate", mock.Anything).Return(nil)

		Convey("when extension is non nil", func() {
			extension := &models.Extension{}

			Convey("when EMS returns an error, we should error", func() {
				emsClient.On("GetChannelExtension", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("ERROR"))
				_, err := controller.CreateCondition(context.Background(),
					mockDomain,
					ownerID,
					name,
					description,
					shouldDisableWhenSatisfied,
					false,
					&timeoutTime,
					effectSettings,
					supportedEffects,
					extension)
				So(err, ShouldNotBeNil)
			})

			Convey("when EMS returns a nil extension, we should error", func() {
				emsClient.On("GetChannelExtension", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
				_, err := controller.CreateCondition(context.Background(),
					mockDomain,
					ownerID,
					name,
					description,
					shouldDisableWhenSatisfied,
					false,
					&timeoutTime,
					effectSettings,
					supportedEffects,
					extension)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when EffectController.ValidateSupportedEffects returns an error, we should return an error", func() {
			effectController.On("ValidateSupportedEffects", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("ERROR"))
			_, err := controller.CreateCondition(context.Background(),
				mockDomain,
				ownerID,
				name,
				description,
				shouldDisableWhenSatisfied,
				false,
				&timeoutTime,
				effectSettings,
				supportedEffects,
				extension)
			So(err, ShouldNotBeNil)
		})

		Convey("when EffectController.ValidateSupportedEffects does not return an error", func() {
			effectController.On("ValidateSupportedEffects", mock.Anything, mock.Anything, mock.Anything).Return(nil)

			Convey("when defineEffectSettingsWhenSatisfied is false, but effect settings validation errors", func() {
				effectController.On("ValidateEffectSettings", mock.Anything, mock.Anything, mock.Anything, extension).Return(nil, errors.New("ERROR"))

				Convey("we should error", func() {
					_, err := controller.CreateCondition(context.Background(),
						mockDomain,
						ownerID,
						name,
						description,
						shouldDisableWhenSatisfied,
						false,
						&timeoutTime,
						effectSettings,
						supportedEffects,
						extension)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when defineEffectSettingsWhenSatisfied is false, and effect settings validation succeeds", func() {
				effectController.On("ValidateEffectSettings", mock.Anything, mock.Anything, mock.Anything, extension).Return(effectSettings, nil)

				Convey("when CreateConditionPutTransaction errors", func() {
					conditionDAO.On("CreateConditionPutTransaction",
						mockDomain,
						ownerID,
						name,
						description,
						shouldDisableWhenSatisfied,
						defineEffectSettingsWhenSatisfied,
						&timeoutTime,
						effectSettings,
						supportedEffects,
						extension).Return(models.Condition{}, nil, errors.New("ERROR"))

					Convey("we should error", func() {
						_, err := controller.CreateCondition(context.Background(),
							mockDomain,
							ownerID,
							name,
							description,
							shouldDisableWhenSatisfied,
							false,
							&timeoutTime,
							effectSettings,
							supportedEffects,
							extension)
						So(err, ShouldNotBeNil)
					})
				})

				Convey("when CreateConditionPutTransaction returns a condition", func() {
					conditionID := "foo-condition-id"

					condition := models.Condition{
						ConditionID:                       conditionID,
						OwnerID:                           ownerID,
						Name:                              name,
						TimeoutAt:                         &timeoutTime,
						EffectSettings:                    effectSettings,
						SupportedEffects:                  supportedEffects,
						DisableWhenSatisfied:              shouldDisableWhenSatisfied,
						DefineEffectSettingsWhenSatisfied: defineEffectSettingsWhenSatisfied,
					}

					mockConditionPutTransaction := &dynamodb.TransactWriteItem{}

					conditionDAO.On("CreateConditionPutTransaction",
						mockDomain,
						ownerID,
						name,
						description,
						shouldDisableWhenSatisfied,
						defineEffectSettingsWhenSatisfied,
						&timeoutTime,
						effectSettings,
						supportedEffects,
						extension).Return(condition, mockConditionPutTransaction, nil)

					Convey("when CreateConditionAggregatesPutTransaction errors", func() {
						conditionAggregates.On("CreateConditionAggregatesPutTransaction", mockDomain, ownerID, conditionID).Return(nil, errors.New("ERROR"))

						Convey("we should error", func() {
							_, err := controller.CreateCondition(context.Background(),
								mockDomain,
								ownerID,
								name,
								description,
								shouldDisableWhenSatisfied,
								false,
								&timeoutTime,
								effectSettings,
								supportedEffects,
								extension)
							So(err, ShouldNotBeNil)
						})
					})

					Convey("when CreateConditionAggregatesPutTransaction does not error", func() {
						mockConditionAggregatesPutTransaction := &dynamodb.TransactWriteItem{}

						conditionAggregates.On("CreateConditionAggregatesPutTransaction", mockDomain, ownerID, conditionID).Return(mockConditionAggregatesPutTransaction, nil)

						Convey("when Transact errors", func() {
							transactionDAO.On("Transact", mock.Anything, []*dynamodb.TransactWriteItem{mockConditionPutTransaction, mockConditionAggregatesPutTransaction}).Return(errors.New("ERROR"))

							Convey("we should error", func() {
								_, err := controller.CreateCondition(context.Background(),
									mockDomain,
									ownerID,
									name,
									description,
									shouldDisableWhenSatisfied,
									false,
									&timeoutTime,
									effectSettings,
									supportedEffects,
									extension)
								So(err, ShouldNotBeNil)
							})
						})

						Convey("when Transact does not error", func() {
							transactionDAO.On("Transact", mock.Anything, []*dynamodb.TransactWriteItem{mockConditionPutTransaction, mockConditionAggregatesPutTransaction}).Return(nil)

							Convey("when CreateConditionRun errors", func() {
								conditionRunCreator.On("CreateConditionRun", mock.Anything, conditionID).Return(nil, errors.New("ERROR"))

								Convey("we should error", func() {
									_, err := controller.CreateCondition(context.Background(),
										mockDomain,
										ownerID,
										name,
										description,
										shouldDisableWhenSatisfied,
										false,
										&timeoutTime,
										effectSettings,
										supportedEffects,
										extension)
									So(err, ShouldNotBeNil)
								})
							})

							Convey("when CreateConditionRun returns a condition", func() {
								conditionRunID := "foo-condition-run-id"

								conditionRun := models.ConditionRun{
									ConditionRunID: conditionRunID,
									ConditionID:    conditionID,
								}

								conditionRunCreator.On("CreateConditionRun", mock.Anything, conditionID).Return(&conditionRun, nil)

								Convey("when SetConditionRunID errors", func() {
									conditionDAO.On("SetConditionRunID", mock.Anything, mockDomain, ownerID, conditionID, conditionRunID).Return(nil, errors.New("ERROR"))

									Convey("we should error", func() {
										_, err := controller.CreateCondition(context.Background(),
											mockDomain,
											ownerID,
											name,
											description,
											shouldDisableWhenSatisfied,
											false,
											&timeoutTime,
											effectSettings,
											supportedEffects,
											extension)
										So(err, ShouldNotBeNil)
									})
								})

								Convey("when SetConditionRunID updates the condition", func() {
									expectedCondition := condition
									expectedCondition.ConditionRunID = conditionRunID

									conditionDAO.On("SetConditionRunID", mock.Anything, mockDomain, ownerID, conditionID, conditionRunID).Return(&expectedCondition, nil)

									Convey("when a timeout is provided", func() {
										expectedSFNInput := models.TimeoutConditionSFNInput{
											ConditionID: conditionID,
											OwnerID:     ownerID,
											TimeoutDate: timeoutTime,
										}

										Convey("when SFNClient.ExecuteTimeoutConditionStateMachine errors", func() {
											sfnClient.On("ExecuteTimeoutConditionStateMachine", expectedSFNInput).Return(errors.New("ERROR"))

											Convey("we should error", func() {
												_, err := controller.CreateCondition(context.Background(),
													mockDomain,
													ownerID,
													name,
													description,
													shouldDisableWhenSatisfied,
													false,
													&timeoutTime,
													effectSettings,
													supportedEffects,
													extension)
												So(err, ShouldNotBeNil)
											})
										})

										Convey("when SFNClient.ExecuteTimeoutConditionStateMachine does not error", func() {
											sfnClient.On("ExecuteTimeoutConditionStateMachine", expectedSFNInput).Return(nil)

											Convey("we should return a newly created condition", func() {
												actualCondition, err := controller.CreateCondition(context.Background(),
													mockDomain,
													ownerID,
													name,
													description,
													shouldDisableWhenSatisfied,
													defineEffectSettingsWhenSatisfied,
													&timeoutTime,
													effectSettings,
													supportedEffects,
													extension)
												So(actualCondition, ShouldResemble, expectedCondition)
												So(err, ShouldBeNil)

												Convey("we should send a pubsub message", func() {
													time.Sleep(time.Second) // Wait for goroutine to run
													pubsubClient.AssertNumberOfCalls(t, "PublishConditionCreate", 1)
												})
											})
										})
									})
								})
							})
						})
					})
				})
			})
		})
	})
}
