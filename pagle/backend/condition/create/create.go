package create

import (
	"context"
	"time"

	"code.justin.tv/commerce/logrus"

	"code.justin.tv/commerce/pagle/backend/condition/effecttotals"
	conditionruncreator "code.justin.tv/commerce/pagle/backend/conditionrun/create"
	conditiondao "code.justin.tv/commerce/pagle/backend/dynamo/condition"
	"code.justin.tv/commerce/pagle/backend/dynamo/transaction"
	"code.justin.tv/commerce/pagle/backend/effect"
	"code.justin.tv/commerce/pagle/backend/ems"
	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/backend/pubsub"
	"code.justin.tv/commerce/pagle/backend/sfn"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type Creator interface {
	CreateAndAssociateNewConditionRun(ctx context.Context, domain, ownerID, conditionID string) (models.ConditionRun, models.Condition, error)
	CreateCondition(
		ctx context.Context,
		domain, ownerID, name, description string,
		disableWhenSatisfied, defineEffectSettingsWhenSatisfied bool,
		timeoutAt *time.Time,
		effectSettings []models.EffectSettings,
		supportedEffects []models.EffectType,
		extension *models.Extension) (models.Condition, error)
}

type creatorImpl struct {
	ConditionAggregatesController effecttotals.EffectTotals   `inject:"conditionaggregatescontroller"`
	ConditionDAO                  conditiondao.DAO            `inject:""`
	SFNClient                     sfn.SFNClient               `inject:""`
	TransactionDAO                transaction.DAO             `inject:""`
	PubsubClient                  pubsub.PubsubClient         `inject:""`
	EffectController              effect.EffectController     `inject:"effectcontroller"`
	ConditionRunCreator           conditionruncreator.Creator `inject:"conditionruncreator"`
	EMSClient                     ems.EMSClient               `inject:""`
}

func NewCreator() Creator {
	return &creatorImpl{}
}

func (c *creatorImpl) CreateCondition(
	ctx context.Context,
	domain, ownerID, name, description string,
	disableWhenSatisfied, defineEffectSettingsWhenSatisfied bool,
	timeoutAt *time.Time,
	effectSettings []models.EffectSettings,
	supportedEffects []models.EffectType,
	extension *models.Extension) (models.Condition, error) {

	var validatedEffectSettings []models.EffectSettings
	var err error

	log := logrus.WithFields(logrus.Fields{
		"domain":   domain,
		"owner_id": ownerID,
		"name":     name,
	})

	if extension != nil {
		log := log.WithFields(logrus.Fields{
			"extension": extension,
		})

		emsExtension, err := c.EMSClient.GetChannelExtension(ctx, extension.ID, extension.InstallationChannelID)

		if err != nil {
			log.WithError(err).Error("error calling EMSClient#GetChannelExtension")
			return models.Condition{}, err
		}

		if emsExtension == nil {
			message := "attempting to create a condition on behalf of an extension for a channel where it is not installed"
			log.Info(message)
			return models.Condition{}, errors.ChannelExtensionNotInstalled.New(message)
		}
	}

	err = c.EffectController.ValidateSupportedEffects(ctx, supportedEffects, extension)
	if err != nil {
		log.WithError(err).Warn("error calling EffectController#ValidateSupportedEffects")
		return models.Condition{}, err
	}

	if !defineEffectSettingsWhenSatisfied {
		validatedEffectSettings, err = c.EffectController.ValidateEffectSettings(ctx, supportedEffects, effectSettings, extension)

		if err != nil {
			log.WithError(err).Warn("error calling EffectController.ValidateEffectSettings from CreateCondition")
			return models.Condition{}, err
		}
	}

	cond, conditionTransaction, err := c.ConditionDAO.CreateConditionPutTransaction(
		domain,
		ownerID,
		name,
		description,
		disableWhenSatisfied,
		defineEffectSettingsWhenSatisfied,
		timeoutAt,
		validatedEffectSettings,
		supportedEffects,
		extension,
	)

	if err != nil {
		log.WithError(err).Error("error calling ConditionDAO.CreateConditionPutTransaction from CreateCondition")
		return models.Condition{}, err
	}

	// Create initial record in aggregates table. Values in this table are updated asynchronously
	// when changes are made to the condition participants table (via dynamo streams).
	conditionAggregatesTransaction, err := c.ConditionAggregatesController.CreateConditionAggregatesPutTransaction(domain, ownerID, cond.ConditionID)

	if err != nil {
		log.WithError(err).Error("error calling ConditionAggregatesController.CreateConditionAggregatesPutTransaction from CreateCondition")
		return models.Condition{}, err
	}

	err = c.TransactionDAO.Transact(ctx, []*dynamodb.TransactWriteItem{
		conditionTransaction,
		conditionAggregatesTransaction,
	})

	if err != nil {
		message := "error writing condition/conditionAggregates transaction"
		log.WithError(err).Error(message)
		return models.Condition{}, errors.InternalError.Wrap(err, message)
	}

	// Create initial condition run
	_, cond, err = c.CreateAndAssociateNewConditionRun(ctx, domain, ownerID, cond.ConditionID)
	if err != nil {
		log.WithError(err).Error("error calling CreateAndAssociateNewConditionRun")
		return models.Condition{}, err
	}

	// Create timeout step function if a timeout date was provided (on timeout date, TimeoutCondition will
	// be called via lambda)
	if timeoutAt != nil {
		err = c.SFNClient.ExecuteTimeoutConditionStateMachine(models.TimeoutConditionSFNInput{
			ConditionID: cond.ConditionID,
			OwnerID:     cond.OwnerID,
			TimeoutDate: *timeoutAt,
			Domain:      cond.Domain,
		})

		if err != nil {
			log.WithError(err).Error("error calling SFNClient.ExecuteTimeoutConditionStateMachine")
			return models.Condition{}, err
		}
	}

	// Send pubsub event
	go func() {
		_ = c.PubsubClient.PublishConditionCreate(cond)
	}()

	return cond, nil
}

func (c *creatorImpl) CreateAndAssociateNewConditionRun(ctx context.Context, domain, ownerID, conditionID string) (models.ConditionRun, models.Condition, error) {
	conditionRun, err := c.ConditionRunCreator.CreateConditionRun(ctx, conditionID)

	if err != nil {
		return models.ConditionRun{}, models.Condition{}, err
	}

	if conditionRun == nil {
		return models.ConditionRun{}, models.Condition{}, errors.InternalError.New("nil condition run returned from CreateConditionRun")
	}

	updatedCondition, err := c.ConditionDAO.SetConditionRunID(ctx, domain, ownerID, conditionID, conditionRun.ConditionRunID)

	if err != nil {
		return models.ConditionRun{}, models.Condition{}, err
	}

	if updatedCondition == nil {
		return models.ConditionRun{}, models.Condition{}, errors.InternalError.New("nil condition returned from SetConditionRunID")
	}

	return *conditionRun, *updatedCondition, nil
}
