package get

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pagle/backend/models"
	conditiondao_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/dynamo/condition"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestGetter_GetCondition(t *testing.T) {
	Convey("given a condition getter", t, func() {
		conditionDAO := new(conditiondao_mock.DAO)

		controller := getterImpl{
			ConditionDAO: conditionDAO,
		}

		mockCondition := models.Condition{
			ConditionID: "foo",
			OwnerID:     "owner-1",
			Domain:      "test-domain",
		}

		Convey("when ConditionDAO.GetCondition returns an error", func() {
			conditionDAO.On("GetCondition", mock.Anything, mockCondition.Domain, mockCondition.OwnerID, mockCondition.ConditionID).Return(nil, errors.New("ERROR"))

			Convey("we should return the error", func() {
				val, err := controller.GetCondition(context.Background(), mockCondition.Domain, mockCondition.ConditionID, mockCondition.OwnerID)
				So(err, ShouldNotBeNil)
				So(val, ShouldResemble, models.Condition{})
			})
		})

		Convey("when ConditionDAO.GetCondition returns a nil condition", func() {
			conditionDAO.On("GetCondition", mock.Anything, mockCondition.Domain, mockCondition.OwnerID, mockCondition.ConditionID).Return(nil, nil)

			Convey("we should return an error", func() {
				val, err := controller.GetCondition(context.Background(), mockCondition.Domain, mockCondition.ConditionID, mockCondition.OwnerID)
				So(err, ShouldNotBeNil)
				So(val, ShouldResemble, models.Condition{})
			})
		})

		Convey("when ConditionDAO.GetCondition does not return an error", func() {
			conditionDAO.On("GetCondition", mock.Anything, mockCondition.Domain, mockCondition.OwnerID, mockCondition.ConditionID).Return(&mockCondition, nil)

			Convey("we should return the condition", func() {
				val, err := controller.GetCondition(context.Background(), mockCondition.Domain, mockCondition.ConditionID, mockCondition.OwnerID)
				So(err, ShouldBeNil)
				So(val, ShouldResemble, mockCondition)
			})
		})
	})
}

func TestGetter_GetConditions(t *testing.T) {
	Convey("given a condition getter", t, func() {
		conditionDAO := new(conditiondao_mock.DAO)

		controller := getterImpl{
			ConditionDAO: conditionDAO,
		}

		mockOwnerID := "owner-1"
		mockCursor := "some-cursor"
		mockDomain := "test-domain"
		mockState := models.ConditionActive

		mockConditions := []*models.Condition{
			{
				OwnerID: mockOwnerID,
			},
		}

		Convey("when ConditionDAO.GetConditionsByOwnerID returns an error", func() {
			conditionDAO.On("GetConditionsByOwnerID", mock.Anything, mockDomain, mockOwnerID, mockCursor, mockState).Return(nil, "", errors.New("ERROR"))

			Convey("we should return the error", func() {
				var expectedConditions []*models.Condition

				val, newCursor, err := controller.GetConditionsByOwnerID(context.Background(), mockDomain, mockOwnerID, mockCursor, mockState)
				So(err, ShouldNotBeNil)
				So(newCursor, ShouldBeEmpty)
				So(val, ShouldResemble, expectedConditions)
			})
		})

		Convey("when ConditionDAO.GetConditionsByOwnerID does not return an error", func() {
			mockNewCursor := "some-new-cursor"
			conditionDAO.On("GetConditionsByOwnerID", mock.Anything, mockDomain, mockOwnerID, mockCursor, mockState).Return(mockConditions, mockNewCursor, nil)

			Convey("we should return the condition returned from ConditionDAO.GetConditionsByOwnerID", func() {
				val, newCursor, err := controller.GetConditionsByOwnerID(context.Background(), mockDomain, mockOwnerID, mockCursor, mockState)
				So(err, ShouldBeNil)
				So(newCursor, ShouldEqual, mockNewCursor)
				So(val, ShouldResemble, mockConditions)
			})
		})
	})
}
