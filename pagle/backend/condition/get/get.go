package get

import (
	"context"

	"code.justin.tv/commerce/logrus"
	conditiondao "code.justin.tv/commerce/pagle/backend/dynamo/condition"
	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/models"
	conditionutils "code.justin.tv/commerce/pagle/backend/utils/condition"
)

type Getter interface {
	GetCondition(ctx context.Context, domain, conditionID, ownerID string) (models.Condition, error)
	BatchGetCondition(ctx context.Context, batchGets []models.BatchGetConditionParams) ([]*models.Condition, error)
	GetActiveCondition(ctx context.Context, domain, conditionID, ownerID string) (models.Condition, error)
	GetConditionsByOwnerID(ctx context.Context, domain, ownerID, cursor string, conditionState models.ConditionState) ([]*models.Condition, string, error)
}

type getterImpl struct {
	ConditionDAO conditiondao.DAO `inject:""`
}

func NewGetter() Getter {
	return &getterImpl{}
}

func (c *getterImpl) GetCondition(ctx context.Context, domain, conditionID, ownerID string) (models.Condition, error) {
	cond, err := c.ConditionDAO.GetCondition(ctx, domain, ownerID, conditionID)

	if err != nil {
		return models.Condition{}, err
	}

	if cond == nil {
		message := "condition does not exist"

		logrus.WithFields(logrus.Fields{
			"conditionId": conditionID,
			"domain":      domain,
			"ownerID":     ownerID,
		}).Warn(message)

		return models.Condition{}, errors.ConditionNotFound.New(message)
	}

	return *cond, nil
}

func (c *getterImpl) BatchGetCondition(ctx context.Context, batchGets []models.BatchGetConditionParams) ([]*models.Condition, error) {
	return c.ConditionDAO.BatchGetCondition(ctx, batchGets)
}

func (c *getterImpl) GetActiveCondition(ctx context.Context, domain, conditionID, ownerID string) (models.Condition, error) {
	cond, err := c.GetCondition(ctx, domain, conditionID, ownerID)

	if err != nil {
		return models.Condition{}, err
	}

	active := conditionutils.IsConditionActive(cond)

	if !active {
		message := "condition is not active"

		logrus.WithFields(logrus.Fields{
			"conditionId": cond.ConditionID,
		}).Warn(message)

		return models.Condition{}, errors.ConditionNotActive.New(message)
	}

	return cond, nil
}

func (c *getterImpl) GetConditionsByOwnerID(ctx context.Context, domain, ownerID, cursor string, conditionState models.ConditionState) ([]*models.Condition, string, error) {
	return c.ConditionDAO.GetConditionsByOwnerID(ctx, domain, ownerID, cursor, conditionState)
}
