package complete

import (
	"context"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend/sns"
	"code.justin.tv/commerce/pagle/backend/stats"

	"code.justin.tv/commerce/pagle/backend/condition/create"
	conditionruncompleter "code.justin.tv/commerce/pagle/backend/conditionrun/complete"

	"code.justin.tv/commerce/pagle/backend/condition/get"
	conditiondao "code.justin.tv/commerce/pagle/backend/dynamo/condition"
	"code.justin.tv/commerce/pagle/backend/effect"
	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/backend/pubsub"
)

const (
	conditionCompletionSnsError = "condition_completion_sns_error"
)

type Completer interface {
	TerminateCondition(ctx context.Context, domain, conditionID, ownerID string, terminationState models.ConditionState) error
	SatisfyCondition(ctx context.Context, domain, conditionID, ownerID string, effectSettings []models.EffectSettings) error
}

type completerImpl struct {
	ConditionDAO          conditiondao.DAO                `inject:""`
	ConditionGetter       get.Getter                      `inject:"conditiongetter"`
	PubsubClient          pubsub.PubsubClient             `inject:""`
	SNSClient             sns.SNSClient                   `inject:""`
	EffectController      effect.EffectController         `inject:"effectcontroller"`
	ConditionRunCompleter conditionruncompleter.Completer `inject:"conditionruncompleter"`
	ConditionCreator      create.Creator                  `inject:"conditioncreator"`
	Statter               stats.Statter                   `inject:""`
}

func NewCompleter() Completer {
	return &completerImpl{}
}

func (c *completerImpl) TerminateCondition(ctx context.Context, domain, conditionID, ownerID string, terminationState models.ConditionState) error {
	logFields := logrus.Fields{
		"condition_id": conditionID,
		"owner_id":     ownerID,
		"domain":       domain,
	}

	conditionRunEndState, err := terminationStateToConditionRunEndState(terminationState)

	if err != nil {
		return err
	}

	_, err = c.ConditionGetter.GetActiveCondition(ctx, domain, conditionID, ownerID)

	if err != nil {
		return err
	}

	condition, err := c.ConditionDAO.SetConditionState(ctx, domain, ownerID, conditionID, terminationState)

	if err != nil {
		return err
	}

	err = c.ConditionRunCompleter.CompleteConditionRun(ctx, condition.Domain, condition.OwnerID, condition.ConditionID, condition.ConditionRunID, conditionRunEndState, false, nil)

	if err != nil {
		return err
	}

	go func() {
		_ = c.PubsubClient.PublishConditionStateUpdate(*condition)
	}()

	go func() {
		snsCtx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
		defer cancel()

		err = c.SNSClient.PublishConditionCompletion(snsCtx, models.ConditionCompletionSNSMessage{
			ConditionID:    condition.ConditionID,
			OwnerID:        condition.OwnerID,
			Domain:         condition.Domain,
			ConditionState: condition.ConditionState,
		})

		if err != nil {
			c.logAndStatCompleteConditionSNSError(logFields, err)
		}
	}()

	return nil
}

func (c *completerImpl) SatisfyCondition(ctx context.Context, domain, conditionID, ownerID string, effectSettings []models.EffectSettings) error {
	logFields := logrus.Fields{
		"condition_id": conditionID,
		"owner_id":     ownerID,
		"domain":       domain,
	}

	cond, err := c.ConditionGetter.GetActiveCondition(ctx, domain, conditionID, ownerID)

	if err != nil {
		return err
	}

	var validatedEffectSettings []models.EffectSettings

	if cond.DefineEffectSettingsWhenSatisfied {
		validatedEffectSettings, err = c.EffectController.ValidateEffectSettings(ctx, cond.SupportedEffects, effectSettings, cond.Extension)

		if err != nil {
			return err
		}
	} else {
		validatedEffectSettings = cond.EffectSettings
	}

	shouldDisableCondition := cond.DisableWhenSatisfied
	shouldCreateNewRun := !shouldDisableCondition
	oldConditionRunID := cond.ConditionRunID // Caching as condition could be associated with a new condition run below

	if shouldDisableCondition {
		updatedCondition, err := c.ConditionDAO.SetConditionState(ctx, domain, ownerID, conditionID, models.ConditionInactive)

		if err != nil {
			return err
		}

		cond = *updatedCondition

		go func() {
			_ = c.PubsubClient.PublishConditionStateUpdate(cond)
		}()

		go func() {
			snsCtx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
			defer cancel()

			err = c.SNSClient.PublishConditionCompletion(snsCtx, models.ConditionCompletionSNSMessage{
				ConditionID:    cond.ConditionID,
				OwnerID:        cond.OwnerID,
				Domain:         cond.Domain,
				ConditionState: cond.ConditionState,
			})

			if err != nil {
				c.logAndStatCompleteConditionSNSError(logFields, err)
			}
		}()
	}

	err = c.ConditionRunCompleter.CompleteConditionRun(ctx, domain, ownerID, cond.ConditionID, oldConditionRunID, models.ConditionRunEndStateSatisfied, shouldCreateNewRun, validatedEffectSettings)

	if err != nil {
		return err
	}

	go func() {
		_ = c.PubsubClient.PublishConditionSatisfied(cond)
	}()

	return nil
}

func terminationStateToConditionRunEndState(terminationState models.ConditionState) (models.ConditionRunEndState, error) {
	switch terminationState {
	case models.ConditionExpired:
		return models.ConditionRunEndStateTimeout, nil
	case models.ConditionCanceled:
		return models.ConditionRunEndStateCanceled, nil
	default:
		return "", errors.IllegalArgument.New("unexpected termination state when converting to condition run end state")
	}
}

func (c *completerImpl) logAndStatCompleteConditionSNSError(logFields logrus.Fields, err error) {
	logrus.WithFields(logFields).WithError(err).Error("error sending SNS message after condition completion")
	c.Statter.Inc(conditionCompletionSnsError, 1, 1)
}
