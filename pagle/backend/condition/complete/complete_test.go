package complete

import (
	"context"
	"errors"
	"testing"
	"time"

	snsclient_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/sns"
	statter_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/stats"

	"code.justin.tv/commerce/pagle/backend/models"
	conditiongetter_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/condition/get"
	conditionruncompleter_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/conditionrun/complete"
	conditiondao_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/dynamo/condition"
	effectcontroller_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/effect"
	pubsub_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/pubsub"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestConditionCompleter_TerminateCondition(t *testing.T) {
	Convey("given a condition controller", t, func() {
		conditionDAO := new(conditiondao_mock.DAO)
		conditionRunCompleter := new(conditionruncompleter_mock.Completer)
		conditionGetter := new(conditiongetter_mock.Getter)
		pubsubClient := new(pubsub_mock.PubsubClient)
		snsClient := new(snsclient_mock.SNSClient)
		statterMock := new(statter_mock.Statter)

		controller := completerImpl{
			ConditionDAO:          conditionDAO,
			ConditionRunCompleter: conditionRunCompleter,
			PubsubClient:          pubsubClient,
			ConditionGetter:       conditionGetter,
			SNSClient:             snsClient,
			Statter:               statterMock,
		}

		mockConditionID := "123"
		mockOwnerID := "owner-1"
		mockDomain := "test-domain"
		mockConditionRunID := "run-123"

		pubsubClient.On("PublishConditionStateUpdate", mock.Anything).Return(nil)
		statterMock.On("Inc", "condition_completion_error", int64(1), float32(1))

		Convey("when terminationState is invalid", func() {
			Convey("we should return an error", func() {
				err := controller.TerminateCondition(context.Background(), mockDomain, mockConditionID, mockOwnerID, models.ConditionActive)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when termination state is valid", func() {
			Convey("when ConditionController.GetCondition returns an error", func() {
				Convey("we should return an error", func() {
					conditionGetter.On("GetActiveCondition", mock.Anything, mockDomain, mockConditionID, mockOwnerID).Return(models.Condition{}, errors.New("ERROR"))
					err := controller.TerminateCondition(context.Background(), mockDomain, mockConditionID, mockOwnerID, models.ConditionExpired)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when ConditionController.GetCondition returns an active condition", func() {
				mockCondition := models.Condition{
					ConditionID:    mockConditionID,
					ConditionState: models.ConditionActive,
					OwnerID:        mockOwnerID,
					ConditionRunID: mockConditionRunID,
					Domain:         mockDomain,
				}
				conditionGetter.On("GetActiveCondition", mock.Anything, mockDomain, mockConditionID, mockOwnerID).Return(mockCondition, nil)

				Convey("when ConditionDAO.SetConditionState returns an error", func() {
					conditionDAO.On("SetConditionState", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("ERROR"))
					Convey("we should return the error", func() {
						err := controller.TerminateCondition(context.Background(), mockDomain, mockConditionID, mockOwnerID, models.ConditionExpired)
						So(err, ShouldNotBeNil)
					})
				})

				Convey("when ConditionDAO.SetConditionState does not return an error", func() {
					newMockCondition := mockCondition
					newMockCondition.ConditionState = models.ConditionExpired
					conditionDAO.On("SetConditionState", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&newMockCondition, nil)

					Convey("when completing the condition run errors", func() {
						conditionRunCompleter.On("CompleteConditionRun", mock.Anything, mockDomain, mockOwnerID, mockConditionID, mockConditionRunID, models.ConditionRunEndStateTimeout, false, []models.EffectSettings(nil)).Return(errors.New("ERROR"))

						Convey("we should error", func() {
							err := controller.TerminateCondition(context.Background(), mockDomain, mockConditionID, mockOwnerID, models.ConditionExpired)

							So(err, ShouldNotBeNil)
						})
					})

					Convey("when completing the condition run does not error", func() {
						conditionRunCompleter.On("CompleteConditionRun", mock.Anything, mockDomain, mockOwnerID, mockConditionID, mockConditionRunID, models.ConditionRunEndStateTimeout, false, []models.EffectSettings(nil)).Return(nil)

						Convey("when SNSClient.PublishConditionCompletion does not error", func() {
							snsClient.On("PublishConditionCompletion", mock.Anything, mock.Anything).Return(nil)

							Convey("we should return nil", func() {
								err := controller.TerminateCondition(context.Background(), mockDomain, mockConditionID, mockOwnerID, models.ConditionExpired)

								So(err, ShouldBeNil)

								Convey("we should send a pubsub message", func() {
									time.Sleep(time.Second) // Wait for goroutine to run
									pubsubClient.AssertNumberOfCalls(t, "PublishConditionStateUpdate", 1)

									Convey("we should send a sns message", func() {
										time.Sleep(time.Second) // Wait for goroutine to run
										snsClient.AssertNumberOfCalls(t, "PublishConditionCompletion", 1)
									})
								})
							})
						})
					})
				})
			})
		})
	})
}

func TestConditionCompleter_SatisfyCondition(t *testing.T) {
	Convey("given a condition controller", t, func() {
		conditionDAO := new(conditiondao_mock.DAO)
		conditionRunCompleter := new(conditionruncompleter_mock.Completer)
		effectController := new(effectcontroller_mock.EffectController)
		pubsubClient := new(pubsub_mock.PubsubClient)
		conditionGetter := new(conditiongetter_mock.Getter)
		snsClient := new(snsclient_mock.SNSClient)
		statterMock := new(statter_mock.Statter)

		controller := completerImpl{
			ConditionDAO:          conditionDAO,
			ConditionRunCompleter: conditionRunCompleter,
			EffectController:      effectController,
			PubsubClient:          pubsubClient,
			ConditionGetter:       conditionGetter,
			SNSClient:             snsClient,
			Statter:               statterMock,
		}

		mockConditionID := "some-condition"
		mockOwnerID := "some-owner"
		mockDomain := "test-domain"
		mockConditionRunID := "run-123"
		mockEffectSettings := []models.EffectSettings{}
		mockSettings := []models.EffectSettings{
			{
				EffectType: models.GiveBits,
				Settings:   &models.EffectTypeSettings{},
			},
		}

		var extension *models.Extension

		pubsubClient.On("PublishConditionStateUpdate", mock.Anything).Return(nil)
		pubsubClient.On("PublishConditionSatisfied", mock.Anything).Return(nil)
		statterMock.On("Inc", "condition_completion_error", int64(1), float32(1))

		Convey("when GetActiveCondition returns an error, we should error", func() {
			conditionGetter.On("GetActiveCondition", mock.Anything, mockDomain, mockConditionID, mockOwnerID).Return(models.Condition{}, errors.New("ERROR"))

			err := controller.SatisfyCondition(context.Background(), mockDomain, mockConditionID, mockOwnerID, mockSettings)

			So(err, ShouldNotBeNil)
		})

		Convey("when the condition is active", func() {
			Convey("when condition dictates that effect settings should be defined when satisfied, but effect settings validation errors, we should error", func() {
				mockCondition := models.Condition{
					ConditionID:                       mockConditionID,
					ConditionState:                    models.ConditionActive,
					OwnerID:                           mockOwnerID,
					EffectSettings:                    mockEffectSettings,
					DisableWhenSatisfied:              true,
					DefineEffectSettingsWhenSatisfied: true,
					SupportedEffects:                  []models.EffectType{models.GiveBits},
					ConditionRunID:                    mockConditionRunID,
					Domain:                            mockDomain,
				}
				conditionGetter.On("GetActiveCondition", mock.Anything, mockDomain, mockConditionID, mockOwnerID).Return(mockCondition, nil)
				effectController.On("ValidateEffectSettings", mock.Anything, mock.Anything, mock.Anything, extension).Return(nil, errors.New("ERROR"))

				err := controller.SatisfyCondition(context.Background(), mockDomain, mockConditionID, mockOwnerID, []models.EffectSettings{})
				So(err, ShouldNotBeNil)
			})

			Convey("when condition should be disabled", func() {
				mockCondition := models.Condition{
					ConditionID:                       mockConditionID,
					ConditionState:                    models.ConditionActive,
					OwnerID:                           mockOwnerID,
					EffectSettings:                    mockEffectSettings,
					DisableWhenSatisfied:              true,
					DefineEffectSettingsWhenSatisfied: false,
					ConditionRunID:                    mockConditionRunID,
					Domain:                            mockDomain,
				}
				conditionGetter.On("GetActiveCondition", mock.Anything, mockDomain, mockConditionID, mockOwnerID).Return(mockCondition, nil)

				Convey("when setting condition state errors", func() {
					conditionDAO.On("SetConditionState", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("ERROR"))

					Convey("we should error", func() {
						err := controller.SatisfyCondition(context.Background(), mockDomain, mockConditionID, mockOwnerID, mockSettings)

						So(err, ShouldNotBeNil)
					})
				})

				Convey("when setting condition state does not error", func() {
					conditionDAO.On("SetConditionState", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&mockCondition, nil)
					conditionRunCompleter.On("CompleteConditionRun", mock.Anything, mockDomain, mockOwnerID, mockConditionID, mockConditionRunID, models.ConditionRunEndStateSatisfied, false, mockEffectSettings).Return(errors.New("ERROR"))

					Convey("when SNSClient.PublishConditionCompletion does not error", func() {
						snsClient.On("PublishConditionCompletion", mock.Anything, mock.Anything).Return(nil)

						Convey("we should send a pubsub message", func() {
							_ = controller.SatisfyCondition(context.Background(), mockDomain, mockConditionID, mockOwnerID, mockSettings)

							time.Sleep(time.Second) // Wait for goroutine to run
							pubsubClient.AssertNumberOfCalls(t, "PublishConditionStateUpdate", 1)

							Convey("we should send a sns message", func() {
								time.Sleep(time.Second) // Wait for goroutine to run
								snsClient.AssertNumberOfCalls(t, "PublishConditionCompletion", 1)
							})
						})
					})
				})
			})

			Convey("when condition should not be disabled", func() {
				mockCondition := models.Condition{
					ConditionID:          mockConditionID,
					ConditionState:       models.ConditionActive,
					OwnerID:              mockOwnerID,
					EffectSettings:       mockEffectSettings,
					DisableWhenSatisfied: false,
					ConditionRunID:       mockConditionRunID,
					Domain:               mockDomain,
				}
				conditionGetter.On("GetActiveCondition", mock.Anything, mockDomain, mockConditionID, mockOwnerID).Return(mockCondition, nil)

				Convey("when completing the condition run errors", func() {
					conditionRunCompleter.On("CompleteConditionRun", mock.Anything, mockDomain, mockOwnerID, mockConditionID, mockConditionRunID, models.ConditionRunEndStateSatisfied, true, mockEffectSettings).Return(errors.New("ERROR"))

					Convey("we should error", func() {
						err := controller.SatisfyCondition(context.Background(), mockDomain, mockConditionID, mockOwnerID, mockSettings)

						So(err, ShouldNotBeNil)
					})
				})

				Convey("when completing the condition run does not error", func() {
					conditionRunCompleter.On("CompleteConditionRun", mock.Anything, mockDomain, mockOwnerID, mockConditionID, mockConditionRunID, models.ConditionRunEndStateSatisfied, true, mockEffectSettings).Return(nil)

					Convey("we should return nil", func() {
						err := controller.SatisfyCondition(context.Background(), mockDomain, mockConditionID, mockOwnerID, mockSettings)

						So(err, ShouldBeNil)

						Convey("we should send a pubsub message", func() {
							time.Sleep(time.Second) // Wait for goroutine to run
							pubsubClient.AssertNumberOfCalls(t, "PublishConditionSatisfied", 1)
						})
					})
				})
			})
		})
	})
}
