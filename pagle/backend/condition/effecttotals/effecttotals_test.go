package effecttotals

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pagle/backend/models"
	conditionaggregatesdao_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/dynamo/conditionaggregates"
	effectcontroller_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/effect"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestConditionAggregatesController_CalculateAndStore(t *testing.T) {
	Convey("given a conditions aggregates controller", t, func() {
		conditionAggregatesDAO := new(conditionaggregatesdao_mock.DAO)
		effectController := new(effectcontroller_mock.EffectController)

		controller := effectTotalsImpl{
			ConditionAggregatesDAO: conditionAggregatesDAO,
			EffectController:       effectController,
		}

		mockDomain := "test-domain"
		mockOwnerID := "test-owner"
		mockConditionID := "123"

		mockConditionParticipant := models.ConditionParticipant{
			ConditionParticipantEndState: models.ConditionParticipantEndStatePendingCompletion,
			Domain:                       mockDomain,
			ConditionOwnerID:             mockOwnerID,
			ConditionID:                  mockConditionID,
		}

		mockMakeEffectAggregateGetter := func() models.EffectAggregateGetter {
			return func(inc, dec *string) ([]models.ConditionParticipantEffectAggregateUpdates, error) {
				return []models.ConditionParticipantEffectAggregateUpdates{}, nil
			}
		}

		Convey("when we don't recognize the new state, we should error", func() {
			conditionAggregatesDAO.On("UpdateIncrementPending", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
			mockConditionParticipant := models.ConditionParticipant{
				ConditionParticipantEndState: models.ConditionParticipantEndState(""),
			}

			err := controller.CalculateAndStore(context.Background(), mockConditionParticipant)

			So(err, ShouldNotBeNil)
		})

		Convey("when a handler fails, we should return an error", func() {
			conditionAggregatesDAO.On("UpdateIncrementPending", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("ERROR"))
			effectController.On("MakeEffectAggregateGetter", mock.Anything, mock.Anything).Return(mockMakeEffectAggregateGetter(), nil)

			err := controller.CalculateAndStore(context.Background(), mockConditionParticipant)

			So(err, ShouldNotBeNil)
		})

		Convey("when MakeEffectAggregateGetter fails, we should return an error", func() {
			effectController.On("MakeEffectAggregateGetter", mock.Anything, mock.Anything).Return(nil, errors.New("ERROR"))

			err := controller.CalculateAndStore(context.Background(), mockConditionParticipant)

			So(err, ShouldNotBeNil)
		})

		Convey("when passed a participant that changed state, we should call the correct handler", func() {
			mockPending := models.ConditionParticipant{
				ConditionParticipantEndState: models.ConditionParticipantEndStatePendingCompletion,
				Domain:                       "pending-domain",
				ConditionOwnerID:             "pending-owner",
				ConditionID:                  "pending-condition",
			}

			mockSatisfied := models.ConditionParticipant{
				ConditionParticipantEndState: models.ConditionParticipantEndStateSatisfied,
				Domain:                       "satisfied-domain",
				ConditionOwnerID:             "satisfied-owner",
				ConditionID:                  "satisfied-condition",
			}

			mockConditionCanceled := models.ConditionParticipant{
				ConditionParticipantEndState: models.ConditionParticipantEndStateConditionCanceled,
				Domain:                       "condition_canceled-domain",
				ConditionOwnerID:             "condition_canceled-owner",
				ConditionID:                  "condition_canceled-condition",
			}

			mockConditionTimeout := models.ConditionParticipant{
				ConditionParticipantEndState: models.ConditionParticipantEndStateConditionExpired,
				Domain:                       "condition_timeout-domain",
				ConditionOwnerID:             "condition_timeout-owner",
				ConditionID:                  "condition_timeout-condition",
			}

			mockCanceled := models.ConditionParticipant{
				ConditionParticipantEndState: models.ConditionParticipantEndStateCanceled,
				Domain:                       "canceled-domain",
				ConditionOwnerID:             "canceled-owner",
				ConditionID:                  "canceled-condition",
			}

			mockTimeout := models.ConditionParticipant{
				ConditionParticipantEndState: models.ConditionParticipantEndStateExpired,
				Domain:                       "timeout-domain",
				ConditionOwnerID:             "timeout-owner",
				ConditionID:                  "timeout-condition",
			}

			mockFailedValidation := models.ConditionParticipant{
				ConditionParticipantEndState: models.ConditionParticipantEndStateFailedValidation,
				Domain:                       "failed_validation-domain",
				ConditionOwnerID:             "failed_validation-owner",
				ConditionID:                  "failed_validation-condition",
			}

			effectController.On("MakeEffectAggregateGetter", mock.Anything, mock.Anything).Return(mockMakeEffectAggregateGetter(), nil)
			conditionAggregatesDAO.On("UpdateIncrementPending", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()
			conditionAggregatesDAO.On("UpdateIncrementSatisfied", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()
			conditionAggregatesDAO.On("UpdateIncrementConditionCanceled", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()
			conditionAggregatesDAO.On("UpdateIncrementConditionTimeout", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()
			conditionAggregatesDAO.On("UpdateIncrementCanceled", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()
			conditionAggregatesDAO.On("UpdateIncrementTimeout", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()
			conditionAggregatesDAO.On("UpdateIncrementFailedValidation", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()

			err := controller.CalculateAndStore(context.Background(), mockPending)
			So(err, ShouldBeNil)

			err = controller.CalculateAndStore(context.Background(), mockSatisfied)
			So(err, ShouldBeNil)

			err = controller.CalculateAndStore(context.Background(), mockConditionCanceled)
			So(err, ShouldBeNil)

			err = controller.CalculateAndStore(context.Background(), mockConditionTimeout)
			So(err, ShouldBeNil)

			err = controller.CalculateAndStore(context.Background(), mockCanceled)
			So(err, ShouldBeNil)

			err = controller.CalculateAndStore(context.Background(), mockTimeout)
			So(err, ShouldBeNil)

			err = controller.CalculateAndStore(context.Background(), mockFailedValidation)
			So(err, ShouldBeNil)

			// Would prefer to assert that effect aggregate getter was passed as expected positionally, but mockery doesn't
			// seem to handle function arguments well.
			conditionAggregatesDAO.AssertCalled(t, "UpdateIncrementPending", mock.Anything, "pending-domain", "pending-owner", "pending-condition", mock.Anything)
			conditionAggregatesDAO.AssertCalled(t, "UpdateIncrementConditionCanceled", mock.Anything, "condition_canceled-domain", "condition_canceled-owner", "condition_canceled-condition", mock.Anything)
			conditionAggregatesDAO.AssertCalled(t, "UpdateIncrementConditionTimeout", mock.Anything, "condition_timeout-domain", "condition_timeout-owner", "condition_timeout-condition", mock.Anything)
			conditionAggregatesDAO.AssertCalled(t, "UpdateIncrementSatisfied", mock.Anything, "satisfied-domain", "satisfied-owner", "satisfied-condition", mock.Anything)
			conditionAggregatesDAO.AssertCalled(t, "UpdateIncrementTimeout", mock.Anything, "timeout-domain", "timeout-owner", "timeout-condition", mock.Anything)
			conditionAggregatesDAO.AssertCalled(t, "UpdateIncrementCanceled", mock.Anything, "canceled-domain", "canceled-owner", "canceled-condition", mock.Anything)
			conditionAggregatesDAO.AssertCalled(t, "UpdateIncrementFailedValidation", mock.Anything, "failed_validation-domain", "failed_validation-owner", "failed_validation-condition", mock.Anything)
		})
	})
}
