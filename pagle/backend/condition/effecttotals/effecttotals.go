package effecttotals

import (
	"context"
	"errors"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend/dynamo/conditionaggregates"
	"code.justin.tv/commerce/pagle/backend/effect"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/backend/sns"
	"code.justin.tv/commerce/pagle/backend/stats"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	conditionAggregatesSNSPublishErrorStatName = "condition_aggregate_retry_ingest_error"
)

type Updater func(ctx context.Context, domain, ownerID, conditionID string, effectAggregateGetter models.EffectAggregateGetter) error

type EffectTotals interface {
	Get(ctx context.Context, domain, ownerID, conditionID string) (models.ConditionAggregates, error)
	BatchGet(ctx context.Context, batchGets []models.BatchGetConditionAggregateParams) ([]models.ConditionAggregates, error)
	CalculateAndStore(ctx context.Context, conditionParticipant models.ConditionParticipant) error
	TryCalculateAndStore(ctx context.Context, conditionParticipant models.ConditionParticipant) error
	CreateConditionAggregatesPutTransaction(domain, ownerID, conditionID string) (*dynamodb.TransactWriteItem, error)
}

type effectTotalsImpl struct {
	ConditionAggregatesDAO conditionaggregates.DAO `inject:""`
	EffectController       effect.EffectController `inject:"effectcontroller"`
	SNSClient              sns.SNSClient           `inject:""`
	Statter                stats.Statter           `inject:""`
}

func NewEffectTotals() EffectTotals {
	return &effectTotalsImpl{}
}

func (c *effectTotalsImpl) Get(ctx context.Context, domain, ownerID, conditionID string) (models.ConditionAggregates, error) {
	return c.ConditionAggregatesDAO.GetConditionAggregates(ctx, domain, ownerID, conditionID)
}

func (c *effectTotalsImpl) BatchGet(ctx context.Context, batchGets []models.BatchGetConditionAggregateParams) ([]models.ConditionAggregates, error) {
	return c.ConditionAggregatesDAO.BatchGetConditionAggregates(ctx, batchGets)
}

/**
Attempt to update condition aggregates synchronously.
If it fails, enqueue the update for retry.
If both fail, manual intervention will be required.
*/
func (c *effectTotalsImpl) TryCalculateAndStore(ctx context.Context, conditionParticipant models.ConditionParticipant) error {
	logFields := log.Fields{
		"condition_participant": conditionParticipant,
	}
	err := c.CalculateAndStore(ctx, conditionParticipant)

	if err != nil {
		log.WithFields(logFields).WithError(err).Error("error calling CalculateAndStore within TryCalculateAndStore")

		snsErr := c.SNSClient.PublishConditionAggregatesRetry(ctx, models.ConditionAggregatesRetrySNSMessage{
			ConditionParticipant: conditionParticipant,
		})

		if snsErr != nil {
			c.Statter.Inc(conditionAggregatesSNSPublishErrorStatName, 1, 1)
			log.WithFields(logFields).WithError(snsErr).Error("error calling PublishConditionAggregatesRetry within TryCalculateAndStore")

			return snsErr
		}
	}

	return nil
}

func (c *effectTotalsImpl) CalculateAndStore(ctx context.Context, conditionParticipant models.ConditionParticipant) error {
	var updater Updater

	switch conditionParticipant.ConditionParticipantEndState {
	case models.ConditionParticipantEndStatePendingCompletion:
		updater = c.ConditionAggregatesDAO.UpdateIncrementPending
	case models.ConditionParticipantEndStateSatisfied:
		updater = c.ConditionAggregatesDAO.UpdateIncrementSatisfied
	case models.ConditionParticipantEndStateFailedValidation:
		updater = c.ConditionAggregatesDAO.UpdateIncrementFailedValidation
	case models.ConditionParticipantEndStateCanceled:
		updater = c.ConditionAggregatesDAO.UpdateIncrementCanceled
	case models.ConditionParticipantEndStateExpired:
		updater = c.ConditionAggregatesDAO.UpdateIncrementTimeout
	case models.ConditionParticipantEndStateConditionCanceled:
		updater = c.ConditionAggregatesDAO.UpdateIncrementConditionCanceled
	case models.ConditionParticipantEndStateConditionExpired:
		updater = c.ConditionAggregatesDAO.UpdateIncrementConditionTimeout
	default:
		msg := "unhandled state transition in CalculateAndStore"
		log.WithFields(log.Fields{
			"new_state": conditionParticipant.ConditionParticipantEndState,
		}).Error(msg)
		return errors.New(msg)
	}

	// Get function that returns instructions for how effect-specific fields should be decremented
	effectAggregateGetter, err := c.EffectController.MakeEffectAggregateGetter(ctx, conditionParticipant.Effect)

	if err != nil {
		log.WithError(err).Error("error making effect aggregate getter")
		return err
	}

	err = updater(
		ctx,
		conditionParticipant.Domain,
		conditionParticipant.ConditionOwnerID,
		conditionParticipant.ConditionID,
		effectAggregateGetter,
	)

	if err != nil {
		log.WithError(err).Error("error calling Updater within CalculateAndStore")
		return err
	}

	return nil
}

func (c *effectTotalsImpl) CreateConditionAggregatesPutTransaction(domain, ownerID, conditionID string) (*dynamodb.TransactWriteItem, error) {
	return c.ConditionAggregatesDAO.CreateConditionAggregatesPutTransaction(domain, ownerID, conditionID)
}
