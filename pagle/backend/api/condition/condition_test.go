package condition

import (
	"context"
	"errors"
	"testing"
	"time"

	conditioncompleter_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/condition/complete"
	conditioncreator_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/condition/create"
	conditiongetter_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/condition/get"

	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/config"
	getconditions_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/cache/getconditions"
	conditionaggregatescontroller_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/condition/effecttotals"
	pagle "code.justin.tv/commerce/pagle/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestConditionAPI_CreateCondition(t *testing.T) {
	Convey("given a condition api", t, func() {
		conditionCreator := new(conditioncreator_mock.Creator)

		api := conditionAPIImpl{
			ConditionCreator: conditionCreator,
		}

		ownerID := "foo-owner-id"
		name := "foo-name"
		domain := "test-domain"
		var extension *models.Extension

		Convey("when request is nil", func() {
			Convey("we should return an error", func() {
				resp, err := api.CreateCondition(context.Background(), nil)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the ownerID is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.CreateCondition(context.Background(), &pagle.CreateConditionReq{
					OwnerId:          "",
					Name:             name,
					SupportedEffects: []pagle.EffectType{pagle.EffectType_GIVE_BITS},
					Domain:           domain,
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the name is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.CreateCondition(context.Background(), &pagle.CreateConditionReq{
					OwnerId:          ownerID,
					Name:             "",
					SupportedEffects: []pagle.EffectType{pagle.EffectType_GIVE_BITS},
					Domain:           domain,
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when supported effects are not passed", func() {
			Convey("we should return an error", func() {
				resp, err := api.CreateCondition(context.Background(), &pagle.CreateConditionReq{
					OwnerId:          ownerID,
					Name:             "",
					SupportedEffects: []pagle.EffectType{},
					Domain:           domain,
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the domain is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.CreateCondition(context.Background(), &pagle.CreateConditionReq{
					OwnerId:          ownerID,
					Name:             name,
					SupportedEffects: []pagle.EffectType{pagle.EffectType_GIVE_BITS},
					Domain:           "",
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the requested condition passes validation", func() {
			expectedSupportedEffects := []models.EffectType{models.GiveBits}
			var expectedEffectSettings []models.EffectSettings

			var timeoutAt *time.Time
			req := &pagle.CreateConditionReq{
				OwnerId:          ownerID,
				Name:             name,
				SupportedEffects: []pagle.EffectType{pagle.EffectType_GIVE_BITS},
				Domain:           domain,
			}

			Convey("when CreateCondition returns an error", func() {
				conditionCreator.On(
					"CreateCondition",
					mock.Anything,
					req.Domain,
					req.OwnerId,
					req.Name,
					req.Description,
					req.DisableWhenConditionSatisfied,
					req.DefineEffectSettingsWhenSatisfied,
					timeoutAt,
					expectedEffectSettings,
					expectedSupportedEffects,
					extension).Return(models.Condition{}, errors.New("ERROR"))

				Convey("we should return an error", func() {
					resp, err := api.CreateCondition(context.Background(), req)
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})
			})

			Convey("when CreateCondition does not return an error", func() {
				conditionID := "foo-condition-id"
				mockCondition := models.Condition{
					ConditionID:    conditionID,
					ConditionState: models.ConditionActive,
				}

				conditionCreator.On("CreateCondition",
					mock.Anything,
					req.Domain,
					req.OwnerId,
					req.Name,
					req.Description,
					req.DisableWhenConditionSatisfied,
					req.DefineEffectSettingsWhenSatisfied,
					timeoutAt,
					expectedEffectSettings,
					expectedSupportedEffects,
					extension).Return(mockCondition, nil)

				Convey("we should return a response", func() {
					resp, err := api.CreateCondition(context.Background(), req)
					So(err, ShouldBeNil)
					So(resp.Condition.ConditionId, ShouldEqual, conditionID)
				})
			})
		})
	})
}

func TestConditionAPI_CancelCondition(t *testing.T) {
	Convey("given a condition api", t, func() {
		conditioncompleter := new(conditioncompleter_mock.Completer)

		api := conditionAPIImpl{
			ConditionCompleter: conditioncompleter,
		}

		mockConditionID := "foo"
		mockOwnerID := "owner-1"
		mockDomain := "test-domain"

		Convey("when request is nil", func() {
			Convey("we should return an error", func() {
				resp, err := api.CancelCondition(context.Background(), nil)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when conditionID is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.CancelCondition(context.Background(), &pagle.CancelConditionReq{
					ConditionId: "",
					OwnerId:     mockOwnerID,
					Domain:      mockDomain,
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when ownerID is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.CancelCondition(context.Background(), &pagle.CancelConditionReq{
					ConditionId: mockConditionID,
					OwnerId:     "",
					Domain:      mockDomain,
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when domain is blank", func() {
			Convey("we should return an error", func() {
				resp, err := api.CancelCondition(context.Background(), &pagle.CancelConditionReq{
					ConditionId: mockConditionID,
					OwnerId:     mockOwnerID,
					Domain:      "",
				})
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})
		})

		Convey("when the requested condition passes validation", func() {
			Convey("when ConditionController.TerminateCondition returns an error", func() {
				Convey("we should return the error", func() {
					conditioncompleter.On("TerminateCondition", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("ERROR"))

					resp, err := api.CancelCondition(context.Background(), &pagle.CancelConditionReq{
						ConditionId: mockConditionID,
						OwnerId:     mockOwnerID,
						Domain:      mockDomain,
					})
					So(err, ShouldNotBeNil)
					So(resp, ShouldBeNil)
				})
			})

			Convey("when ConditionController.TerminateCondition does not return an error", func() {
				Convey("we should return an empty CancelConditionResp", func() {
					conditioncompleter.On("TerminateCondition", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

					resp, err := api.CancelCondition(context.Background(), &pagle.CancelConditionReq{
						ConditionId: mockConditionID,
						OwnerId:     mockOwnerID,
						Domain:      mockDomain,
					})
					So(err, ShouldBeNil)
					So(resp, ShouldResemble, &pagle.CancelConditionResp{})
				})
			})
		})
	})
}

func TestConditionAPI_GetConditionSummary(t *testing.T) {
	Convey("given a condition api", t, func() {
		conditionAggregatesController := new(conditionaggregatescontroller_mock.EffectTotals)

		api := conditionAPIImpl{
			ConditionAggregatesController: conditionAggregatesController,
		}

		mockConditionID := "foo"
		mockOwnerID := "owner-1"
		mockDomain := "test-domain"

		Convey("when request is nil", func() {
			Convey("we should return an error", func() {
				val, err := api.GetConditionSummary(context.Background(), nil)
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when conditionID is blank", func() {
			Convey("we should return an error", func() {
				val, err := api.GetConditionSummary(context.Background(), &pagle.GetConditionSummaryReq{
					ConditionId: "",
					OwnerId:     mockOwnerID,
					Domain:      mockDomain,
				})
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when ownerID is blank", func() {
			Convey("we should return an error", func() {
				val, err := api.GetConditionSummary(context.Background(), &pagle.GetConditionSummaryReq{
					ConditionId: mockConditionID,
					OwnerId:     "",
					Domain:      mockDomain,
				})
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when domain is blank", func() {
			Convey("we should return an error", func() {
				val, err := api.GetConditionSummary(context.Background(), &pagle.GetConditionSummaryReq{
					ConditionId: mockConditionID,
					OwnerId:     mockOwnerID,
					Domain:      "",
				})
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when the requested condition passes validation", func() {
			Convey("when ConditionController.GetConditionAggregates returns an error", func() {
				conditionAggregatesController.On("Get", mock.Anything, mockDomain, mockOwnerID, mockConditionID).Return(models.ConditionAggregates{}, errors.New("ERROR"))

				Convey("we should return the error", func() {
					val, err := api.GetConditionSummary(context.Background(), &pagle.GetConditionSummaryReq{
						ConditionId: mockConditionID,
						OwnerId:     mockOwnerID,
						Domain:      mockDomain,
					})
					So(err, ShouldNotBeNil)
					So(val, ShouldBeNil)
				})
			})

			Convey("when ConditionController.GetConditionAggregates does not return an error", func() {
				mockConditionSummary := models.ConditionAggregates{
					ConditionID: mockConditionID,
					OwnerID:     mockOwnerID,
					Domain:      mockDomain,
					ConditionParticipants: models.ConditionParticipantAggregates{
						Total: 1,
						Pending: models.ConditionParticipantAggregatesForState{
							Total: 1,
							Effects: models.EffectSummary{
								GiveBits: models.GiveBitsEffectSummary{
									TotalEffects:    1,
									TotalBitsAmount: 10,
								},
							},
						},
					},
				}

				conditionAggregatesController.On("Get", mock.Anything, mockDomain, mockOwnerID, mockConditionID).Return(mockConditionSummary, nil)

				Convey("we should return an empty GetConditionSummaryResp", func() {
					val, err := api.GetConditionSummary(context.Background(), &pagle.GetConditionSummaryReq{
						ConditionId: mockConditionID,
						OwnerId:     mockOwnerID,
						Domain:      mockDomain,
					})
					So(err, ShouldBeNil)
					So(val, ShouldNotBeNil)
					So(val.Summary.OwnerId, ShouldEqual, mockOwnerID)
					So(val.Summary.ConditionId, ShouldEqual, mockConditionID)
					So(val.Summary.Domain, ShouldEqual, mockDomain)
					So(val.Summary.Participants.Total, ShouldEqual, 1)
					So(val.Summary.Participants.Pending.Total, ShouldEqual, 1)
					So(val.Summary.Participants.Pending.Effects.GiveBits.TotalBitsAmount, ShouldEqual, 10)
					So(val.Summary.Participants.Pending.Effects.GiveBits.TotalEffects, ShouldEqual, 1)
				})
			})
		})
	})
}

func TestConditionAPI_BatchGetConditionSummary(t *testing.T) {
	Convey("given a condition api", t, func() {
		conditionAggregatesController := new(conditionaggregatescontroller_mock.EffectTotals)

		api := conditionAPIImpl{
			ConditionAggregatesController: conditionAggregatesController,
		}

		mockConditionID := "foo"
		mockOwnerID := "owner-1"
		mockDomain := "test-domain"

		Convey("when request is nil", func() {
			Convey("we should return an error", func() {
				val, err := api.BatchGetConditionSummary(context.Background(), nil)
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when a request includes a blank conditionID", func() {
			Convey("we should return an error", func() {
				val, err := api.BatchGetConditionSummary(context.Background(), &pagle.BatchGetConditionSummaryReq{
					Requests: []*pagle.GetConditionSummaryReq{
						{
							ConditionId: "",
							OwnerId:     mockOwnerID,
							Domain:      mockDomain,
						},
					},
				})
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when a request includes a blank ownerID", func() {
			Convey("we should return an error", func() {
				val, err := api.BatchGetConditionSummary(context.Background(), &pagle.BatchGetConditionSummaryReq{
					Requests: []*pagle.GetConditionSummaryReq{
						{
							ConditionId: mockConditionID,
							OwnerId:     "",
							Domain:      mockDomain,
						},
					},
				})
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when a request includes a blank domain", func() {
			Convey("we should return an error", func() {
				val, err := api.BatchGetConditionSummary(context.Background(), &pagle.BatchGetConditionSummaryReq{
					Requests: []*pagle.GetConditionSummaryReq{
						{
							ConditionId: mockConditionID,
							OwnerId:     mockOwnerID,
							Domain:      "",
						},
					},
				})
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when the requests pass validation", func() {
			Convey("when ConditionController.GetConditionAggregates returns an error", func() {
				conditionAggregatesController.On("BatchGet", mock.Anything, mock.Anything).Return([]models.ConditionAggregates{}, errors.New("ERROR"))

				Convey("we should return the error", func() {
					val, err := api.BatchGetConditionSummary(context.Background(), &pagle.BatchGetConditionSummaryReq{
						Requests: []*pagle.GetConditionSummaryReq{
							{
								ConditionId: mockConditionID,
								OwnerId:     mockOwnerID,
								Domain:      mockDomain,
							},
						},
					})
					So(err, ShouldNotBeNil)
					So(val, ShouldBeNil)
				})
			})

			Convey("when ConditionController.GetConditionAggregates does not return an error", func() {
				mockSummaries := []models.ConditionAggregates{
					{
						ConditionID: mockConditionID,
						OwnerID:     mockOwnerID,
						Domain:      mockDomain,
						ConditionParticipants: models.ConditionParticipantAggregates{
							Total: 1,
							Pending: models.ConditionParticipantAggregatesForState{
								Total: 1,
								Effects: models.EffectSummary{
									GiveBits: models.GiveBitsEffectSummary{
										TotalEffects:    1,
										TotalBitsAmount: 10,
									},
								},
							},
						},
					},
				}

				conditionAggregatesController.On("BatchGet", mock.Anything, mock.Anything).Return(mockSummaries, nil)

				Convey("we should return an non-empty GetConditionSummaryResp", func() {
					val, err := api.BatchGetConditionSummary(context.Background(), &pagle.BatchGetConditionSummaryReq{
						Requests: []*pagle.GetConditionSummaryReq{
							{
								ConditionId: mockConditionID,
								OwnerId:     mockOwnerID,
								Domain:      mockDomain,
							},
						},
					})
					So(err, ShouldBeNil)
					So(val, ShouldNotBeNil)
					So(val.Summaries, ShouldHaveLength, 1)
					So(val.Summaries[0].OwnerId, ShouldEqual, mockOwnerID)
					So(val.Summaries[0].ConditionId, ShouldEqual, mockConditionID)
					So(val.Summaries[0].Domain, ShouldEqual, mockDomain)
					So(val.Summaries[0].Participants.Total, ShouldEqual, 1)
					So(val.Summaries[0].Participants.Pending.Total, ShouldEqual, 1)
					So(val.Summaries[0].Participants.Pending.Effects.GiveBits.TotalBitsAmount, ShouldEqual, 10)
					So(val.Summaries[0].Participants.Pending.Effects.GiveBits.TotalEffects, ShouldEqual, 1)
				})
			})
		})
	})
}

func TestConditionAPI_GetConditions(t *testing.T) {
	Convey("given a condition api", t, func() {
		conditionGetter := new(conditiongetter_mock.Getter)
		getConditionsCache := new(getconditions_mock.Cache)

		mockConfig, err := config.LoadConfig(config.UnitTest)
		So(err, ShouldBeNil)

		api := conditionAPIImpl{
			ConditionGetter:    conditionGetter,
			GetConditionsCache: getConditionsCache,
			Config:             mockConfig,
		}

		mockOwnerID := "foo"
		mockDomain := "test-domain"

		Convey("when request is nil", func() {
			Convey("we should return an error", func() {
				val, err := api.GetConditions(context.Background(), nil)
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when ownerID is blank", func() {
			Convey("we should return an error", func() {
				val, err := api.GetConditions(context.Background(), &pagle.GetConditionsReq{
					OwnerId: "",
					Domain:  mockDomain,
				})
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when domain is blank", func() {
			Convey("we should return an error", func() {
				val, err := api.GetConditions(context.Background(), &pagle.GetConditionsReq{
					OwnerId: mockOwnerID,
					Domain:  "",
				})
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when request is valid", func() {
			Convey("when the response is cached", func() {
				getConditionsCache.On("Get", mock.Anything).Return(&pagle.GetConditionsResp{})

				Convey("we should return immediately and not call the controller", func() {
					val, err := api.GetConditions(context.Background(), &pagle.GetConditionsReq{
						OwnerId: mockOwnerID,
						Domain:  mockDomain,
					})
					So(err, ShouldBeNil)
					So(val, ShouldNotBeNil)
					conditionGetter.AssertNumberOfCalls(t, "GetConditionsByOwnerID", 0)
				})
			})

			Convey("when the response is NOT cached", func() {
				getConditionsCache.On("Get", mock.Anything).Return(nil)

				Convey("when ConditionController.GetConditions returns an error", func() {
					conditionGetter.On("GetConditionsByOwnerID", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]*models.Condition{}, "", errors.New("ERROR"))

					Convey("we should return the error", func() {
						val, err := api.GetConditions(context.Background(), &pagle.GetConditionsReq{
							OwnerId: mockOwnerID,
							Domain:  mockDomain,
						})
						So(err, ShouldNotBeNil)
						So(val, ShouldBeNil)
					})
				})

				Convey("when ConditionController.GetConditions does not return an error", func() {
					mockConditions := []*models.Condition{
						{
							ConditionID:    "1",
							ConditionState: models.ConditionActive,
						},
						{
							ConditionID:    "2",
							ConditionState: models.ConditionExpired,
						},
					}

					mockNewCursor := "some-cursor"

					conditionGetter.On("GetConditionsByOwnerID", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockConditions, mockNewCursor, nil)
					getConditionsCache.On("Set", mock.Anything, mock.Anything).Return(nil)

					Convey("we should return the conditions and a cursor and cache the response", func() {
						val, err := api.GetConditions(context.Background(), &pagle.GetConditionsReq{
							OwnerId: mockOwnerID,
							Domain:  mockDomain,
						})
						So(err, ShouldBeNil)
						So(val.Conditions, ShouldHaveLength, 2)
						So(val.Cursor, ShouldEqual, mockNewCursor)
						getConditionsCache.AssertNumberOfCalls(t, "Set", 1)
					})
				})
			})
		})
	})
}

func TestConditionAPI_GetCondition(t *testing.T) {
	Convey("given a condition api", t, func() {
		conditionGetter := new(conditiongetter_mock.Getter)

		api := conditionAPIImpl{
			ConditionGetter: conditionGetter,
		}

		mockOwnerID := "foo"
		mockConditionID := "123"
		mockDomain := "test-domain"

		Convey("when request is nil", func() {
			Convey("we should return an error", func() {
				val, err := api.GetCondition(context.Background(), nil)
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when ownerID is blank", func() {
			Convey("we should return an error", func() {
				val, err := api.GetCondition(context.Background(), &pagle.GetConditionReq{
					OwnerId:     "",
					ConditionId: mockConditionID,
					Domain:      mockDomain,
				})
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when conditionID is blank", func() {
			Convey("we should return an error", func() {
				val, err := api.GetCondition(context.Background(), &pagle.GetConditionReq{
					OwnerId:     mockOwnerID,
					ConditionId: "",
					Domain:      mockDomain,
				})
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when domain is blank", func() {
			Convey("we should return an error", func() {
				val, err := api.GetCondition(context.Background(), &pagle.GetConditionReq{
					OwnerId:     mockOwnerID,
					ConditionId: mockConditionID,
					Domain:      "",
				})
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when request is valid", func() {
			Convey("when ConditionController.GetCondition returns an error", func() {
				conditionGetter.On("GetCondition", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(models.Condition{}, errors.New("ERROR"))

				Convey("we should return the error", func() {
					val, err := api.GetCondition(context.Background(), &pagle.GetConditionReq{
						OwnerId:     mockOwnerID,
						ConditionId: mockConditionID,
						Domain:      mockDomain,
					})
					So(err, ShouldNotBeNil)
					So(val, ShouldBeNil)
				})
			})

			Convey("when ConditionController.GetCondition does not return an error", func() {
				mockCondition := models.Condition{
					ConditionID:    mockConditionID,
					ConditionState: models.ConditionActive,
				}

				conditionGetter.On("GetCondition", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockCondition, nil)

				Convey("we should return the condition", func() {
					val, err := api.GetCondition(context.Background(), &pagle.GetConditionReq{
						OwnerId:     mockOwnerID,
						ConditionId: mockConditionID,
						Domain:      mockDomain,
					})
					So(err, ShouldBeNil)
					So(val.Condition, ShouldNotBeNil)
					So(val.Condition.ConditionId, ShouldEqual, mockConditionID)
				})
			})
		})
	})
}

func TestConditionAPI_BatchGetCondition(t *testing.T) {
	Convey("given a condition api", t, func() {
		conditionGetter := new(conditiongetter_mock.Getter)

		api := conditionAPIImpl{
			ConditionGetter: conditionGetter,
		}

		mockConditionID := "foo"
		mockOwnerID := "owner-1"
		mockDomain := "test-domain"

		Convey("when request is nil", func() {
			Convey("we should return an error", func() {
				val, err := api.BatchGetCondition(context.Background(), nil)
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when request Requests is nil", func() {
			Convey("we should return an error", func() {
				val, err := api.BatchGetCondition(context.Background(), &pagle.BatchGetConditionReq{
					Requests: nil,
				})
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when a request includes a blank conditionID", func() {
			Convey("we should return an error", func() {
				val, err := api.BatchGetCondition(context.Background(), &pagle.BatchGetConditionReq{
					Requests: []*pagle.GetConditionReq{
						{
							ConditionId: "",
							OwnerId:     mockOwnerID,
							Domain:      mockDomain,
						},
					},
				})
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when a request includes a blank ownerID", func() {
			Convey("we should return an error", func() {
				val, err := api.BatchGetCondition(context.Background(), &pagle.BatchGetConditionReq{
					Requests: []*pagle.GetConditionReq{
						{
							ConditionId: mockConditionID,
							OwnerId:     "",
							Domain:      mockDomain,
						},
					},
				})
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when a request includes a blank domain", func() {
			Convey("we should return an error", func() {
				val, err := api.BatchGetCondition(context.Background(), &pagle.BatchGetConditionReq{
					Requests: []*pagle.GetConditionReq{
						{
							ConditionId: mockConditionID,
							OwnerId:     mockOwnerID,
							Domain:      "",
						},
					},
				})
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when the requests pass validation", func() {
			Convey("when ConditionController.BatchGetCondition returns an error", func() {
				conditionGetter.On("BatchGetCondition", mock.Anything, mock.Anything).Return([]*models.Condition{}, errors.New("ERROR"))

				Convey("we should return the error", func() {
					val, err := api.BatchGetCondition(context.Background(), &pagle.BatchGetConditionReq{
						Requests: []*pagle.GetConditionReq{
							{
								ConditionId: mockConditionID,
								OwnerId:     mockOwnerID,
								Domain:      mockDomain,
							},
						},
					})
					So(err, ShouldNotBeNil)
					So(val, ShouldBeNil)
				})
			})

			Convey("when ConditionController.BatchGetCondition does not return an error", func() {
				mockConditions := []*models.Condition{
					{
						ConditionID:    mockConditionID,
						OwnerID:        mockOwnerID,
						Domain:         mockDomain,
						ConditionState: models.ConditionActive,
					},
				}

				conditionGetter.On("BatchGetCondition", mock.Anything, mock.Anything).Return(mockConditions, nil)

				Convey("we should return an non-empty BatchGetConditionResp", func() {
					val, err := api.BatchGetCondition(context.Background(), &pagle.BatchGetConditionReq{
						Requests: []*pagle.GetConditionReq{
							{
								ConditionId: mockConditionID,
								OwnerId:     mockOwnerID,
								Domain:      mockDomain,
							},
						},
					})
					So(err, ShouldBeNil)
					So(val, ShouldNotBeNil)
					So(val.Conditions, ShouldHaveLength, 1)
					So(val.Conditions[0].OwnerId, ShouldEqual, mockOwnerID)
					So(val.Conditions[0].ConditionId, ShouldEqual, mockConditionID)
					So(val.Conditions[0].Domain, ShouldEqual, mockDomain)
				})
			})
		})
	})
}
