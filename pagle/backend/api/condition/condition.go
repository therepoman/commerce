package condition

import (
	"context"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend/cache/getconditions"
	conditioncompletor "code.justin.tv/commerce/pagle/backend/condition/complete"
	conditioncreator "code.justin.tv/commerce/pagle/backend/condition/create"
	"code.justin.tv/commerce/pagle/backend/condition/effecttotals"
	conditiongetter "code.justin.tv/commerce/pagle/backend/condition/get"
	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/backend/utils/proto"
	"code.justin.tv/commerce/pagle/config"
	pagle "code.justin.tv/commerce/pagle/rpc"
	"github.com/golang/protobuf/ptypes"
	"github.com/twitchtv/twirp"
)

type ConditionAPI interface {
	CreateCondition(ctx context.Context, req *pagle.CreateConditionReq) (*pagle.CreateConditionResp, error)
	CancelCondition(ctx context.Context, req *pagle.CancelConditionReq) (*pagle.CancelConditionResp, error)
	SatisfyCondition(ctx context.Context, req *pagle.SatisfyConditionReq) (*pagle.SatisfyConditionResp, error)
	GetConditionSummary(ctx context.Context, req *pagle.GetConditionSummaryReq) (*pagle.GetConditionSummaryResp, error)
	GetConditions(ctx context.Context, req *pagle.GetConditionsReq) (*pagle.GetConditionsResp, error)
	GetCondition(ctx context.Context, req *pagle.GetConditionReq) (*pagle.GetConditionResp, error)
	BatchGetConditionSummary(ctx context.Context, req *pagle.BatchGetConditionSummaryReq) (*pagle.BatchGetConditionSummaryResp, error)
	BatchGetCondition(ctx context.Context, req *pagle.BatchGetConditionReq) (*pagle.BatchGetConditionResp, error)
}

type conditionAPIImpl struct {
	ConditionGetter               conditiongetter.Getter       `inject:"conditiongetter"`
	ConditionCompleter            conditioncompletor.Completer `inject:"conditioncompleter"`
	ConditionCreator              conditioncreator.Creator     `inject:"conditioncreator"`
	GetConditionsCache            getconditions.Cache          `inject:""`
	ConditionAggregatesController effecttotals.EffectTotals    `inject:"conditionaggregatescontroller"`
	Config                        *config.Config               `inject:""`
}

func NewConditionAPI() ConditionAPI {
	return &conditionAPIImpl{}
}

func (api conditionAPIImpl) CreateCondition(ctx context.Context, req *pagle.CreateConditionReq) (*pagle.CreateConditionResp, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if strings.Blank(req.OwnerId) {
		return nil, twirp.RequiredArgumentError("ownerId")
	}

	if strings.Blank(req.Name) {
		return nil, twirp.RequiredArgumentError("name")
	}

	if len(req.SupportedEffects) == 0 {
		return nil, twirp.RequiredArgumentError("supportedEffects")
	}

	if strings.Blank(req.Domain) {
		return nil, twirp.RequiredArgumentError("domain")
	}

	var timeoutAt *time.Time
	var err error
	if req.TimeoutAt != nil {
		timestamp, err := ptypes.Timestamp(req.TimeoutAt)
		if err != nil {
			return nil, twirp.InvalidArgumentError("timeoutAt", "is invalid")
		}

		if timestamp.Before(time.Now()) {
			return nil, twirp.InvalidArgumentError("timeoutAt", "must be after the current time")
		}

		timeoutAt = &timestamp
	}

	supportedEffects, err := proto.ProtoEffectTypesToEffectTypes(req.SupportedEffects)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	effectSettings, err := proto.ProtoEffectSettingsToEffectSettings(req.EffectSettings)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	var extension *models.Extension
	if req.Extension != nil {
		extension, err = proto.ProtoExtensionToExtension(*req.Extension)

		if err != nil {
			return nil, errors.InternalPagleErrorToTwirpError(err)
		}
	}

	c, err := api.ConditionCreator.CreateCondition(
		ctx,
		req.Domain,
		req.OwnerId,
		req.Name,
		req.Description,
		req.DisableWhenConditionSatisfied,
		req.DefineEffectSettingsWhenSatisfied,
		timeoutAt,
		effectSettings,
		supportedEffects,
		extension,
	)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	protoCondition, err := proto.ConditionToProtoCondition(c)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	return &pagle.CreateConditionResp{
		Condition: &protoCondition,
	}, nil
}

func (api conditionAPIImpl) CancelCondition(ctx context.Context, req *pagle.CancelConditionReq) (*pagle.CancelConditionResp, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if strings.Blank(req.ConditionId) {
		return nil, twirp.RequiredArgumentError("conditionId")
	}

	if strings.Blank(req.OwnerId) {
		return nil, twirp.RequiredArgumentError("ownerId")
	}

	if strings.Blank(req.Domain) {
		return nil, twirp.RequiredArgumentError("domain")
	}

	err := api.ConditionCompleter.TerminateCondition(ctx, req.Domain, req.ConditionId, req.OwnerId, models.ConditionCanceled)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	return &pagle.CancelConditionResp{}, err
}

func (api conditionAPIImpl) SatisfyCondition(ctx context.Context, req *pagle.SatisfyConditionReq) (*pagle.SatisfyConditionResp, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if strings.Blank(req.ConditionId) {
		return nil, twirp.RequiredArgumentError("conditionId")
	}

	if strings.Blank(req.OwnerId) {
		return nil, twirp.RequiredArgumentError("ownerId")
	}

	if strings.Blank(req.Domain) {
		return nil, twirp.RequiredArgumentError("domain")
	}

	effectSettings, err := proto.ProtoEffectSettingsToEffectSettings(req.EffectSettings)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	err = api.ConditionCompleter.SatisfyCondition(ctx, req.Domain, req.ConditionId, req.OwnerId, effectSettings)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	return &pagle.SatisfyConditionResp{}, nil
}

func (api conditionAPIImpl) GetConditionSummary(ctx context.Context, req *pagle.GetConditionSummaryReq) (*pagle.GetConditionSummaryResp, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if strings.Blank(req.ConditionId) {
		return nil, twirp.RequiredArgumentError("conditionId")
	}

	if strings.Blank(req.OwnerId) {
		return nil, twirp.RequiredArgumentError("ownerId")
	}

	if strings.Blank(req.Domain) {
		return nil, twirp.RequiredArgumentError("domain")
	}

	aggregates, err := api.ConditionAggregatesController.Get(ctx, req.Domain, req.OwnerId, req.ConditionId)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	protoSummary := proto.ConditionAggregatesToProtoConditionSummary(aggregates)

	return &pagle.GetConditionSummaryResp{
		Summary: &protoSummary,
	}, nil
}

func (api conditionAPIImpl) BatchGetConditionSummary(ctx context.Context, req *pagle.BatchGetConditionSummaryReq) (*pagle.BatchGetConditionSummaryResp, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if req.Requests == nil || len(req.Requests) == 0 {
		return nil, twirp.RequiredArgumentError("must provide a list of requests")
	}

	var batchGets []models.BatchGetConditionAggregateParams

	for _, item := range req.Requests {
		if strings.Blank(item.ConditionId) {
			return nil, twirp.RequiredArgumentError("conditionId")
		}

		if strings.Blank(item.OwnerId) {
			return nil, twirp.RequiredArgumentError("ownerId")
		}

		if strings.Blank(item.Domain) {
			return nil, twirp.RequiredArgumentError("domain")
		}

		batchGets = append(batchGets, models.BatchGetConditionAggregateParams{
			Domain:      item.Domain,
			OwnerID:     item.OwnerId,
			ConditionID: item.ConditionId,
		})
	}

	batchAggregates, err := api.ConditionAggregatesController.BatchGet(ctx, batchGets)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	batchSummaries, err := proto.BatchConditionAggregatesToProtoBatchConditionSummary(batchAggregates)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	return &pagle.BatchGetConditionSummaryResp{
		Summaries: batchSummaries,
	}, nil
}

func (api conditionAPIImpl) GetConditions(ctx context.Context, req *pagle.GetConditionsReq) (*pagle.GetConditionsResp, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if strings.Blank(req.OwnerId) {
		return nil, twirp.RequiredArgumentError("ownerId")
	}

	if strings.Blank(req.Domain) {
		return nil, twirp.RequiredArgumentError("domain")
	}

	// Check if the GetConditions response has a cached version first
	cachedConditionsRes := api.GetConditionsCache.Get(req)
	if cachedConditionsRes != nil {
		return cachedConditionsRes, nil
	}

	conditionState, err := proto.ProtoConditionStateToConditionState(req.ConditionState)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	conditions, cursor, err := api.ConditionGetter.GetConditionsByOwnerID(ctx, req.Domain, req.OwnerId, req.Cursor, conditionState)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	protoConditions, err := proto.ConditionsToProtoConditions(conditions)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	res := &pagle.GetConditionsResp{
		Conditions: protoConditions,
		Cursor:     cursor,
	}

	if api.Config.Cache.GetConditions.SetEnabled {
		// Cache the GetConditions response
		err = api.GetConditionsCache.Set(req, res)
		if err != nil {
			log.WithError(err).WithFields(log.Fields{
				"ownerID": req.OwnerId,
				"state":   req.ConditionState,
				"cursor":  req.Cursor,
			}).Error("Failed to cache GetConditions response")
		}
	}

	return res, nil
}

func (api conditionAPIImpl) GetCondition(ctx context.Context, req *pagle.GetConditionReq) (*pagle.GetConditionResp, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if strings.Blank(req.OwnerId) {
		return nil, twirp.RequiredArgumentError("ownerId")
	}

	if strings.Blank(req.ConditionId) {
		return nil, twirp.RequiredArgumentError("conditionId")
	}

	if strings.Blank(req.Domain) {
		return nil, twirp.RequiredArgumentError("domain")
	}

	condition, err := api.ConditionGetter.GetCondition(ctx, req.Domain, req.ConditionId, req.OwnerId)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	protoCondition, err := proto.ConditionToProtoCondition(condition)

	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	return &pagle.GetConditionResp{
		Condition: &protoCondition,
	}, nil
}

func (api conditionAPIImpl) BatchGetCondition(ctx context.Context, req *pagle.BatchGetConditionReq) (*pagle.BatchGetConditionResp, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if req.Requests == nil || len(req.Requests) == 0 {
		return nil, twirp.RequiredArgumentError("must provide a list of requests")
	}

	var batchGets []models.BatchGetConditionParams

	for _, item := range req.Requests {
		if strings.Blank(item.ConditionId) {
			return nil, twirp.RequiredArgumentError("conditionId")
		}

		if strings.Blank(item.OwnerId) {
			return nil, twirp.RequiredArgumentError("ownerId")
		}

		if strings.Blank(item.Domain) {
			return nil, twirp.RequiredArgumentError("domain")
		}

		batchGets = append(batchGets, models.BatchGetConditionParams{
			Domain:      item.Domain,
			OwnerID:     item.OwnerId,
			ConditionID: item.ConditionId,
		})
	}

	conditions, err := api.ConditionGetter.BatchGetCondition(ctx, batchGets)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	protoConditions, err := proto.ConditionsToProtoConditions(conditions)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	return &pagle.BatchGetConditionResp{
		Conditions: protoConditions,
	}, nil
}
