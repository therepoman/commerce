package conditionparticipant

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/config"
	getconditionparticipants_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/cache/getconditionparticipants"
	conditionparticipantcreator_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/conditionparticipant/create"
	conditionparticipantgetter_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/conditionparticipant/get"
	pagle "code.justin.tv/commerce/pagle/rpc"
	"github.com/golang/protobuf/ptypes"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestConditionParticipantAPI_CreateConditionParticipant(t *testing.T) {
	Convey("given a create conditionParticipant api", t, func() {
		conditionParticipantCreator := new(conditionparticipantcreator_mock.Creator)

		api := conditionParticipantAPIImpl{
			ConditionParticipantCreator: conditionParticipantCreator,
		}

		mockConditionID := "some-condition"
		mockConditionParticipantOwnerID := "some-conditionParticipant-owner"
		mockConditionOwnerID := "some-condition-owner"
		mockDomain := "test-domain"
		mockEffectParams := &pagle.Effect_GiveBitsParams{
			GiveBitsParams: &pagle.GiveBitsParams{
				Amount:     1,
				FromUserId: mockConditionParticipantOwnerID,
			},
		}
		mockTTL := models.ConditionParticipantMaxTTL - 1
		mockConditionParticipantID := "some-conditionParticipant-id"

		Convey("when request is nil, we should error", func() {
			resp, err := api.CreateConditionParticipant(context.Background(), nil)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when condition id is blank, we should error", func() {
			resp, err := api.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
				ConditionId:                 "",
				ConditionParticipantOwnerId: mockConditionParticipantOwnerID,
				ConditionOwnerId:            mockConditionID,
				TtlSeconds:                  mockTTL,
				Effect: &pagle.Effect{
					EffectType: pagle.EffectType_GIVE_BITS,
					Params:     mockEffectParams,
				},
				Domain: mockDomain,
			})
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when conditionParticipant owner id is blank, we should error", func() {
			resp, err := api.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
				ConditionId:                 mockConditionID,
				ConditionParticipantOwnerId: "",
				ConditionOwnerId:            mockConditionID,
				TtlSeconds:                  mockTTL,
				Effect: &pagle.Effect{
					EffectType: pagle.EffectType_GIVE_BITS,
					Params:     mockEffectParams,
				},
				Domain: mockDomain,
			})
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when condition owner id is blank, we should error", func() {
			resp, err := api.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
				ConditionId:                 mockConditionID,
				ConditionParticipantOwnerId: mockConditionParticipantOwnerID,
				ConditionOwnerId:            "",
				TtlSeconds:                  mockTTL,
				Effect: &pagle.Effect{
					EffectType: pagle.EffectType_GIVE_BITS,
					Params:     mockEffectParams,
				},
				Domain: mockDomain,
			})
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when effect is nil, we should error", func() {
			resp, err := api.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
				ConditionId:                 mockConditionID,
				ConditionParticipantOwnerId: mockConditionParticipantOwnerID,
				ConditionOwnerId:            mockConditionOwnerID,
				TtlSeconds:                  mockTTL,
				Effect:                      nil,
				Domain:                      mockDomain,
			})
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when effect params are nil, we should error", func() {
			mockEffectParams.GiveBitsParams = nil

			resp, err := api.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
				ConditionId:                 mockConditionID,
				ConditionParticipantOwnerId: mockConditionParticipantOwnerID,
				ConditionOwnerId:            mockConditionOwnerID,
				TtlSeconds:                  mockTTL,
				Effect: &pagle.Effect{
					EffectType: pagle.EffectType_GIVE_BITS,
					Params:     mockEffectParams,
				},
				Domain: mockDomain,
			})
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when effect is invalid, we should error", func() {
			resp, err := api.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
				ConditionId:                 mockConditionID,
				ConditionParticipantOwnerId: mockConditionParticipantOwnerID,
				ConditionOwnerId:            mockConditionOwnerID,
				TtlSeconds:                  mockTTL,
				Effect: &pagle.Effect{
					EffectType: pagle.EffectType_GIVE_BITS,
					Params:     nil,
				},
				Domain: mockDomain,
			})
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when ttl is 0, we should error", func() {
			resp, err := api.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
				ConditionId:                 mockConditionID,
				ConditionParticipantOwnerId: mockConditionParticipantOwnerID,
				ConditionOwnerId:            mockConditionOwnerID,
				TtlSeconds:                  0,
				Effect: &pagle.Effect{
					EffectType: pagle.EffectType_GIVE_BITS,
					Params:     mockEffectParams,
				},
				Domain: mockDomain,
			})
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when domain is empty, we should error", func() {
			resp, err := api.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
				ConditionId:                 mockConditionID,
				ConditionParticipantOwnerId: mockConditionParticipantOwnerID,
				ConditionOwnerId:            mockConditionOwnerID,
				TtlSeconds:                  0,
				Effect: &pagle.Effect{
					EffectType: pagle.EffectType_GIVE_BITS,
					Params:     mockEffectParams,
				},
				Domain: "",
			})
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when the request passes validation", func() {
			Convey("when the call to ConditionParticipantController.CreateConditionParticipant errors, we should error", func() {
				conditionParticipantCreator.On("CreateConditionParticipant", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(models.ConditionParticipant{}, errors.New("ERROR"))

				resp, err := api.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
					ConditionId:                 mockConditionID,
					ConditionParticipantOwnerId: mockConditionParticipantOwnerID,
					ConditionOwnerId:            mockConditionOwnerID,
					TtlSeconds:                  mockTTL,
					Effect: &pagle.Effect{
						EffectType: pagle.EffectType_GIVE_BITS,
						Params:     mockEffectParams,
					},
					Domain: mockDomain,
				})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("when the call to ConditionParticipantController.CreateConditionParticipant does not error, we should return the new conditionParticipant id", func() {
				mockConditionParticipant := models.ConditionParticipant{
					ConditionParticipantID:              mockConditionParticipantID,
					ConditionParticipantEndState:        models.ConditionParticipantEndStatePendingCompletion,
					ConditionParticipantProcessingState: models.ConditionParticipantProcessingStatePending,
					Effect: models.Effect{
						EffectType: models.GiveBits,
						Params: models.EffectParams{
							GiveBits: &models.EffectParamsGiveBits{
								PoolableInput: models.EffectInputPoolable{
									FromUserID: mockConditionParticipantOwnerID,
									Amount:     1,
								},
							},
						},
					},
				}
				conditionParticipantCreator.On("CreateConditionParticipant", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockConditionParticipant, nil)

				resp, err := api.CreateConditionParticipant(context.Background(), &pagle.CreateConditionParticipantReq{
					ConditionId:                 mockConditionID,
					ConditionParticipantOwnerId: mockConditionParticipantOwnerID,
					ConditionOwnerId:            mockConditionOwnerID,
					TtlSeconds:                  mockTTL,
					Effect: &pagle.Effect{
						EffectType: pagle.EffectType_GIVE_BITS,
						Params:     mockEffectParams,
					},
					Domain: mockDomain,
				})

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.ConditionParticipant.ConditionParticipantId, ShouldEqual, mockConditionParticipant.ConditionParticipantID)
			})
		})
	})
}

func TestConditionParticipantAPI_GetConditionParticipant(t *testing.T) {
	Convey("given a get conditionParticipant api", t, func() {
		conditionParticipantGetter := new(conditionparticipantgetter_mock.Getter)

		api := conditionParticipantAPIImpl{
			ConditionParticipantGetter: conditionParticipantGetter,
		}

		mockConditionParticipantID := "some-condition-participant-id"
		mockConditionParticipantOwnerID := "some-condition-participant-owner-id"
		mockDomain := "test-domain"

		Convey("when request is nil, we should error", func() {
			resp, err := api.GetConditionParticipant(context.Background(), nil)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when conditionParticipant id is blank, we should error", func() {
			resp, err := api.GetConditionParticipant(context.Background(), &pagle.GetConditionParticipantReq{
				ConditionParticipantId:      "",
				ConditionParticipantOwnerId: mockConditionParticipantOwnerID,
				Domain:                      mockDomain,
			})
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when conditionParticipantOwnerId id is blank, we should error", func() {
			resp, err := api.GetConditionParticipant(context.Background(), &pagle.GetConditionParticipantReq{
				ConditionParticipantId:      mockConditionParticipantID,
				ConditionParticipantOwnerId: "",
				Domain:                      mockDomain,
			})
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when domain id is blank, we should error", func() {
			resp, err := api.GetConditionParticipant(context.Background(), &pagle.GetConditionParticipantReq{
				ConditionParticipantId:      mockConditionParticipantID,
				ConditionParticipantOwnerId: mockConditionParticipantOwnerID,
				Domain:                      "",
			})
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when the request passes validation", func() {
			Convey("when the call to ConditionParticipantController.GetConditionParticipant errors, we should error", func() {
				conditionParticipantGetter.On("GetConditionParticipantByDomainAndOwnerAndID", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(models.ConditionParticipant{}, errors.New("ERROR"))

				resp, err := api.GetConditionParticipant(context.Background(), &pagle.GetConditionParticipantReq{
					ConditionParticipantId:      mockConditionParticipantID,
					ConditionParticipantOwnerId: mockConditionParticipantOwnerID,
					Domain:                      mockDomain,
				})

				So(resp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("when the call to ConditionParticipantController.GetConditionParticipant does not error, we should return the new conditionParticipant id", func() {
				mockCreatedAt := time.Now()
				createdAtProto, err := ptypes.TimestampProto(mockCreatedAt)
				if err != nil {
					t.Fail()
				}

				mockConditionParticipant := models.ConditionParticipant{
					ConditionParticipantID:              mockConditionParticipantID,
					ConditionParticipantEndState:        models.ConditionParticipantEndStatePendingCompletion,
					ConditionParticipantProcessingState: models.ConditionParticipantProcessingStatePending,
					CreatedAt:                           mockCreatedAt,
					Effect: models.Effect{
						EffectType: models.GiveBits,
						Params: models.EffectParams{
							GiveBits: &models.EffectParamsGiveBits{
								PoolableInput: models.EffectInputPoolable{
									FromUserID: mockConditionParticipantOwnerID,
									Amount:     1,
								},
							},
						},
					},
				}
				conditionParticipantGetter.On("GetConditionParticipantByDomainAndOwnerAndID", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockConditionParticipant, nil)

				resp, err := api.GetConditionParticipant(context.Background(), &pagle.GetConditionParticipantReq{
					ConditionParticipantId:      mockConditionParticipantID,
					ConditionParticipantOwnerId: mockConditionParticipantOwnerID,
					Domain:                      mockDomain,
				})

				So(resp, ShouldNotBeNil)
				So(*resp, ShouldResemble, pagle.GetConditionParticipantResp{
					ConditionParticipant: &pagle.ConditionParticipant{
						ConditionParticipantId:              mockConditionParticipantID,
						ConditionParticipantProcessingState: pagle.ConditionParticipantProcessingState_PENDING,
						ConditionParticipantEndState:        pagle.ConditionParticipantEndState_PENDING_COMPLETION,
						CreatedAt:                           createdAtProto,
						Effect: &pagle.Effect{
							EffectType: pagle.EffectType_GIVE_BITS,
							Params: &pagle.Effect_GiveBitsParams{
								GiveBitsParams: &pagle.GiveBitsParams{
									FromUserId: mockConditionParticipantOwnerID,
									Amount:     1,
								},
							},
						},
					},
				})
				So(err, ShouldBeNil)
			})
		})
	})
}

func TestConditionAPI_GetConditionParticipants(t *testing.T) {
	Convey("given a conditionParticipant api", t, func() {
		conditionParticipantGetter := new(conditionparticipantgetter_mock.Getter)
		getConditionParticipantsCache := new(getconditionparticipants_mock.Cache)

		mockConfig, err := config.LoadConfig(config.UnitTest)
		So(err, ShouldBeNil)

		api := conditionParticipantAPIImpl{
			ConditionParticipantGetter:    conditionParticipantGetter,
			GetConditionParticipantsCache: getConditionParticipantsCache,
			Config:                        mockConfig,
		}

		mockOwnerID := "foo"
		mockConditionOwnerID := "bar"
		mockDomain := "test-domain"

		Convey("when request is nil", func() {
			Convey("we should return an error", func() {
				val, err := api.GetConditionParticipants(context.Background(), nil)
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when conditionParticipantOwnerID is blank", func() {
			Convey("we should return an error", func() {
				val, err := api.GetConditionParticipants(context.Background(), &pagle.GetConditionParticipantsReq{
					ConditionParticipantOwnerId: "",
					ConditionOwnerId:            mockConditionOwnerID,
					Domain:                      mockDomain,
				})
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when conditionOwnerID is blank", func() {
			Convey("we should return an error", func() {
				val, err := api.GetConditionParticipants(context.Background(), &pagle.GetConditionParticipantsReq{
					ConditionParticipantOwnerId: mockOwnerID,
					ConditionOwnerId:            "",
					Domain:                      mockDomain,
				})
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when domain is blank", func() {
			Convey("we should return an error", func() {
				val, err := api.GetConditionParticipants(context.Background(), &pagle.GetConditionParticipantsReq{
					ConditionParticipantOwnerId: mockOwnerID,
					ConditionOwnerId:            mockConditionOwnerID,
					Domain:                      "",
				})
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when request is valid", func() {
			Convey("when the response is cached", func() {
				getConditionParticipantsCache.On("Get", mock.Anything).Return(&pagle.GetConditionParticipantsResp{})

				Convey("we should return immediately and not call the controller", func() {
					val, err := api.GetConditionParticipants(context.Background(), &pagle.GetConditionParticipantsReq{
						ConditionParticipantOwnerId: mockOwnerID,
						ConditionOwnerId:            mockConditionOwnerID,
						Domain:                      mockDomain,
					})
					So(err, ShouldBeNil)
					So(val, ShouldNotBeNil)
					conditionParticipantGetter.AssertNumberOfCalls(t, "GetConditionParticipantsByOwnerIDAndConditionOwnerID", 0)
				})
			})

			Convey("when the response is NOT cached", func() {
				getConditionParticipantsCache.On("Get", mock.Anything).Return(nil)

				Convey("when ConditionController.GetConditionParticipantsByOwnerIDAndConditionOwnerID returns an error", func() {
					conditionParticipantGetter.On("GetConditionParticipantsByOwnerIDAndConditionOwnerID", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]*models.ConditionParticipant{}, "", errors.New("ERROR"))

					Convey("we should return the error", func() {
						val, err := api.GetConditionParticipants(context.Background(), &pagle.GetConditionParticipantsReq{
							ConditionParticipantOwnerId: mockOwnerID,
						})
						So(err, ShouldNotBeNil)
						So(val, ShouldBeNil)
					})
				})

				Convey("when ConditionController.GetConditionParticipantsByOwnerIDAndConditionOwnerID does not return an error", func() {
					mockEffect := models.Effect{
						EffectType: models.GiveBits,
						Params: models.EffectParams{
							GiveBits: &models.EffectParamsGiveBits{
								PoolableInput: models.EffectInputPoolable{
									FromUserID: mockOwnerID,
									Amount:     1,
								},
							},
						},
					}

					mockConditionParticipants := []*models.ConditionParticipant{
						{
							ConditionParticipantID:              "1",
							ConditionParticipantEndState:        models.ConditionParticipantEndStatePendingCompletion,
							ConditionParticipantProcessingState: models.ConditionParticipantProcessingStatePending,
							Effect:                              mockEffect,
						},
						{
							ConditionParticipantID:              "2",
							ConditionParticipantEndState:        models.ConditionParticipantEndStatePendingCompletion,
							ConditionParticipantProcessingState: models.ConditionParticipantProcessingStatePending,
							Effect:                              mockEffect,
						},
					}

					mockNewCursor := "some-cursor"

					conditionParticipantGetter.On("GetConditionParticipantsByOwnerIDAndConditionOwnerID", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockConditionParticipants, mockNewCursor, nil)
					getConditionParticipantsCache.On("Set", mock.Anything, mock.Anything).Return(nil)

					Convey("we should return the conditionParticipants and a cursor and cache the response", func() {
						val, err := api.GetConditionParticipants(context.Background(), &pagle.GetConditionParticipantsReq{
							ConditionParticipantOwnerId: mockOwnerID,
							ConditionOwnerId:            mockConditionOwnerID,
							Domain:                      mockDomain,
						})
						So(err, ShouldBeNil)
						So(val.ConditionParticipants, ShouldHaveLength, 2)
						So(val.Cursor, ShouldEqual, mockNewCursor)
						getConditionParticipantsCache.AssertNumberOfCalls(t, "Set", 1)
					})
				})
			})
		})
	})
}

func TestConditionAPI_GetConditionParticipantsByOwnerID(t *testing.T) {
	Convey("given a conditionParticipant api", t, func() {
		conditionParticipantGetter := new(conditionparticipantgetter_mock.Getter)

		mockConfig, err := config.LoadConfig(config.UnitTest)
		So(err, ShouldBeNil)

		api := conditionParticipantAPIImpl{
			ConditionParticipantGetter: conditionParticipantGetter,
			Config:                     mockConfig,
		}

		mockOwnerID := "foo"
		mockDomain := "test-domain"

		Convey("when request is nil", func() {
			Convey("we should return an error", func() {
				val, err := api.GetConditionParticipantsByOwnerID(context.Background(), nil)
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when conditionParticipantOwnerID is blank", func() {
			Convey("we should return an error", func() {
				val, err := api.GetConditionParticipantsByOwnerID(context.Background(), &pagle.GetConditionParticipantsByOwnerIDReq{
					ConditionParticipantOwnerId: "",
					Domain:                      mockDomain,
				})
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when domain is blank", func() {
			Convey("we should return an error", func() {
				val, err := api.GetConditionParticipantsByOwnerID(context.Background(), &pagle.GetConditionParticipantsByOwnerIDReq{
					ConditionParticipantOwnerId: mockOwnerID,
					Domain:                      "",
				})
				So(err, ShouldNotBeNil)
				So(val, ShouldBeNil)
			})
		})

		Convey("when request is valid", func() {
			Convey("when ConditionController.GetConditionParticipantsByDomainAndOwnerID returns an error", func() {
				conditionParticipantGetter.On("GetConditionParticipantsByDomainAndOwnerID", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return([]*models.ConditionParticipant{}, "", errors.New("ERROR"))

				Convey("we should return the error", func() {
					val, err := api.GetConditionParticipantsByOwnerID(context.Background(), &pagle.GetConditionParticipantsByOwnerIDReq{
						ConditionParticipantOwnerId: mockOwnerID,
					})
					So(err, ShouldNotBeNil)
					So(val, ShouldBeNil)
				})
			})

			Convey("when ConditionController.GetConditionParticipantsByDomainAndOwnerID does not return an error", func() {
				mockEffect := models.Effect{
					EffectType: models.GiveBits,
					Params: models.EffectParams{
						GiveBits: &models.EffectParamsGiveBits{
							PoolableInput: models.EffectInputPoolable{
								FromUserID: mockOwnerID,
								Amount:     1,
							},
						},
					},
				}

				mockConditionParticipants := []*models.ConditionParticipant{
					{
						ConditionParticipantID:              "1",
						OwnerID:                             "test-owner",
						ConditionParticipantProcessingState: models.ConditionParticipantProcessingStatePending,
						ConditionParticipantEndState:        models.ConditionParticipantEndStatePendingCompletion,
						Effect:                              mockEffect,
					},
					{
						ConditionParticipantID:              "2",
						OwnerID:                             "test-owner",
						ConditionParticipantProcessingState: models.ConditionParticipantProcessingStatePending,
						ConditionParticipantEndState:        models.ConditionParticipantEndStatePendingCompletion,
						Effect:                              mockEffect,
					},
				}

				mockNewCursor := "some-cursor"

				conditionParticipantGetter.On("GetConditionParticipantsByDomainAndOwnerID", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockConditionParticipants, mockNewCursor, nil)

				Convey("we should return the conditionParticipants and a cursor", func() {
					val, err := api.GetConditionParticipantsByOwnerID(context.Background(), &pagle.GetConditionParticipantsByOwnerIDReq{
						ConditionParticipantOwnerId: mockOwnerID,
						Domain:                      mockDomain,
					})
					time.Sleep(time.Second) // Wait for goroutine to run
					So(err, ShouldBeNil)
					So(val.ConditionParticipants, ShouldHaveLength, 2)
					So(val.Cursor, ShouldEqual, mockNewCursor)
				})
			})
		})
	})
}
