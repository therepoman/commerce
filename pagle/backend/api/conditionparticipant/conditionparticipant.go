package conditionparticipant

import (
	"context"

	log "github.com/sirupsen/logrus"

	conditionparticipantcreator "code.justin.tv/commerce/pagle/backend/conditionparticipant/create"
	conditionparticipantgetter "code.justin.tv/commerce/pagle/backend/conditionparticipant/get"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/pagle/backend/cache/getconditionparticipants"
	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/utils/proto"
	"code.justin.tv/commerce/pagle/config"
	pagle "code.justin.tv/commerce/pagle/rpc"
	"github.com/twitchtv/twirp"
)

type ConditionParticipantAPI interface {
	CreateConditionParticipant(ctx context.Context, req *pagle.CreateConditionParticipantReq) (*pagle.CreateConditionParticipantResp, error)
	GetConditionParticipant(ctx context.Context, req *pagle.GetConditionParticipantReq) (*pagle.GetConditionParticipantResp, error)
	GetConditionParticipants(ctx context.Context, req *pagle.GetConditionParticipantsReq) (*pagle.GetConditionParticipantsResp, error)
	GetConditionParticipantsByOwnerID(ctx context.Context, req *pagle.GetConditionParticipantsByOwnerIDReq) (*pagle.GetConditionParticipantsByOwnerIDResp, error)
}

type conditionParticipantAPIImpl struct {
	ConditionParticipantCreator   conditionparticipantcreator.Creator `inject:"conditionparticipantcreator"`
	ConditionParticipantGetter    conditionparticipantgetter.Getter   `inject:"conditionparticipantgetter"`
	GetConditionParticipantsCache getconditionparticipants.Cache      `inject:""`
	Config                        *config.Config                      `inject:""`
}

func NewConditionParticipantAPI() ConditionParticipantAPI {
	return &conditionParticipantAPIImpl{}
}

func (api conditionParticipantAPIImpl) CreateConditionParticipant(ctx context.Context, req *pagle.CreateConditionParticipantReq) (*pagle.CreateConditionParticipantResp, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if strings.Blank(req.ConditionId) {
		return nil, twirp.RequiredArgumentError("conditionId")
	}

	if strings.Blank(req.ConditionParticipantOwnerId) {
		return nil, twirp.RequiredArgumentError("conditionParticipantOwnerId")
	}

	if strings.Blank(req.ConditionOwnerId) {
		return nil, twirp.RequiredArgumentError("conditionOwnerId")
	}

	if strings.Blank(req.Domain) {
		return nil, twirp.RequiredArgumentError("domain")
	}

	if req.Effect == nil {
		return nil, twirp.RequiredArgumentError("effect")
	}

	if req.TtlSeconds == 0 {
		return nil, twirp.RequiredArgumentError("ttlSeconds")
	}

	effect, err := proto.ProtoEffectToEffect(*req.Effect)

	if err != nil {
		return nil, twirp.InvalidArgumentError("effect.params", err.Error())
	}

	conditionParticipant, err := api.ConditionParticipantCreator.CreateConditionParticipant(ctx, req.Domain, req.ConditionId, req.ConditionOwnerId, req.ConditionParticipantOwnerId, effect, req.TtlSeconds)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	protoConditionParticipant, err := proto.ConditionParticipantToProtoConditionParticipant(conditionParticipant)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	return &pagle.CreateConditionParticipantResp{
		ConditionParticipant: &protoConditionParticipant,
	}, nil
}

func (api conditionParticipantAPIImpl) GetConditionParticipant(ctx context.Context, req *pagle.GetConditionParticipantReq) (*pagle.GetConditionParticipantResp, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if strings.Blank(req.ConditionParticipantId) {
		return nil, twirp.RequiredArgumentError("conditionParticipantId")
	}

	if strings.Blank(req.ConditionParticipantOwnerId) {
		return nil, twirp.RequiredArgumentError("conditionParticipantOwnerId")
	}

	if strings.Blank(req.Domain) {
		return nil, twirp.RequiredArgumentError("domain")
	}

	conditionParticipant, err := api.ConditionParticipantGetter.GetConditionParticipantByDomainAndOwnerAndID(ctx, req.Domain, req.ConditionParticipantId, req.ConditionParticipantOwnerId)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	protoConditionParticipant, err := proto.ConditionParticipantToProtoConditionParticipant(conditionParticipant)

	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	return &pagle.GetConditionParticipantResp{
		ConditionParticipant: &protoConditionParticipant,
	}, nil
}

func (api conditionParticipantAPIImpl) GetConditionParticipants(ctx context.Context, req *pagle.GetConditionParticipantsReq) (*pagle.GetConditionParticipantsResp, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if strings.Blank(req.ConditionParticipantOwnerId) {
		return nil, twirp.RequiredArgumentError("conditionParticipantOwnerId")
	}

	if strings.Blank(req.ConditionOwnerId) {
		return nil, twirp.RequiredArgumentError("conditionOwnerId")
	}

	if strings.Blank(req.Domain) {
		return nil, twirp.RequiredArgumentError("domain")
	}

	// Check if the GetConditionParticipants response has a cached version first
	cachedConditionParticipantsRes := api.GetConditionParticipantsCache.Get(req)
	if cachedConditionParticipantsRes != nil {
		return cachedConditionParticipantsRes, nil
	}

	endState, err := proto.ProtoConditionParticipantEndStateToConditionParticipantEndState(req.ConditionParticipantEndState)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	conditionParticipants, cursor, err := api.ConditionParticipantGetter.GetConditionParticipantsByOwnerIDAndConditionOwnerID(ctx, req.Domain, req.ConditionParticipantOwnerId, req.ConditionOwnerId, req.Cursor, endState)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	protoConditionParticipants, err := proto.ConditionParticipantsToProtoConditionParticipants(conditionParticipants)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	res := &pagle.GetConditionParticipantsResp{
		ConditionParticipants: protoConditionParticipants,
		Cursor:                cursor,
	}

	if api.Config.Cache.GetConditionParticipants.SetEnabled {
		// Cache the GetConditionParticipants response
		err = api.GetConditionParticipantsCache.Set(req, res)
		if err != nil {
			log.WithError(err).WithFields(log.Fields{
				"conditionParticipantOwnerId": req.ConditionParticipantOwnerId,
				"conditionOwnerID":            req.ConditionOwnerId,
				"endState":                    int32(req.ConditionParticipantEndState),
				"cursor":                      req.Cursor,
			}).Error("Failed to cache GetConditionParticipants response")
		}
	}

	return res, nil
}

func (api conditionParticipantAPIImpl) GetConditionParticipantsByOwnerID(ctx context.Context, req *pagle.GetConditionParticipantsByOwnerIDReq) (*pagle.GetConditionParticipantsByOwnerIDResp, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("must provide a request")
	}

	if strings.Blank(req.ConditionParticipantOwnerId) {
		return nil, twirp.RequiredArgumentError("conditionParticipantOwnerId")
	}

	if strings.Blank(req.Domain) {
		return nil, twirp.RequiredArgumentError("domain")
	}

	conditionParticipants, cursor, err := api.ConditionParticipantGetter.GetConditionParticipantsByDomainAndOwnerID(ctx, req.Domain, req.ConditionParticipantOwnerId, req.Cursor)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	protoConditionParticipants, err := proto.ConditionParticipantsToProtoConditionParticipants(conditionParticipants)

	if err != nil {
		return nil, errors.InternalPagleErrorToTwirpError(err)
	}

	return &pagle.GetConditionParticipantsByOwnerIDResp{
		ConditionParticipants: protoConditionParticipants,
		Cursor:                cursor,
	}, nil

}
