package create

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/pagle/backend/models"
	effecttotals_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/condition/effecttotals"
	conditiongetter_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/condition/get"
	conditionparticipantdao_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/dynamo/conditionparticipant"
	effectcontroller_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/effect"
	pubsub_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/pubsub"
	sfnclient_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/sfn"
	snsclient_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/sns"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestConditionParticipantController_CreateConditionParticipant(t *testing.T) {
	Convey("given a conditionParticipant controller", t, func() {
		conditionGetter := new(conditiongetter_mock.Getter)
		effectController := new(effectcontroller_mock.EffectController)
		conditionAggregatesController := new(effecttotals_mock.EffectTotals)
		conditionParticipantDAO := new(conditionparticipantdao_mock.DAO)
		sfn := new(sfnclient_mock.SFNClient)
		sns := new(snsclient_mock.SNSClient)
		pubsubClient := new(pubsub_mock.PubsubClient)

		controller := creatorImpl{
			ConditionParticipantDAO: conditionParticipantDAO,
			SFNClient:               sfn,
			EffectController:        effectController,
			EffectTotals:            conditionAggregatesController,
			ConditionGetter:         conditionGetter,
			SNSClient:               sns,
			PubsubClient:            pubsubClient,
		}

		mockConditionID := "some-condition"
		mockDomain := "test-domain"
		mockConditionOwnerID := "some-condition-owner"
		mockConditionParticipantOwnerID := "some-conditionParticipant-owner"
		mockEffect := models.Effect{
			EffectType: models.GiveBits,
			Params: models.EffectParams{
				GiveBits: &models.EffectParamsGiveBits{},
			},
		}
		mockTTL := models.ConditionParticipantMaxTTL - 1

		pubsubClient.On("PublishConditionParticipantCreate", mock.Anything).Return(nil)
		conditionAggregatesController.On("TryCalculateAndStore", mock.Anything, mock.Anything).Return(nil)

		Convey("when the ttl is less than the min allowable, we should error", func() {
			cond, err := controller.CreateConditionParticipant(context.Background(), mockDomain, mockConditionID, mockConditionOwnerID, mockConditionParticipantOwnerID, mockEffect, models.ConditionParticipantMinTTL-1)

			So(cond, ShouldResemble, models.ConditionParticipant{})
			So(err, ShouldNotBeNil)
		})

		Convey("when the ttl is greater than the max allowable, we should error", func() {
			cond, err := controller.CreateConditionParticipant(context.Background(), mockDomain, mockConditionID, mockConditionOwnerID, mockConditionParticipantOwnerID, mockEffect, models.ConditionParticipantMaxTTL+1)

			So(cond, ShouldResemble, models.ConditionParticipant{})
			So(err, ShouldNotBeNil)
		})

		Convey("when the ttl passes validation", func() {
			Convey("when ConditionController.GetActiveCondition returns an error", func() {
				conditionGetter.On("GetActiveCondition", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(models.Condition{}, errors.New("ERROR"))

				Convey("we should error", func() {
					cond, err := controller.CreateConditionParticipant(context.Background(), mockDomain, mockConditionID, mockConditionOwnerID, mockConditionParticipantOwnerID, mockEffect, mockTTL)

					So(cond, ShouldResemble, models.ConditionParticipant{})
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when ConditionController.GetActiveCondition does not return an error", func() {
				Convey("when the associated condition does not support the effect type", func() {
					mockCondition := models.Condition{
						SupportedEffects: []models.EffectType{
							models.EffectType("FAKE"),
						},
					}

					conditionGetter.On("GetActiveCondition", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockCondition, nil)

					Convey("we should error", func() {
						cond, err := controller.CreateConditionParticipant(context.Background(), mockDomain, mockConditionID, mockConditionOwnerID, mockConditionParticipantOwnerID, mockEffect, mockTTL)

						So(cond, ShouldResemble, models.ConditionParticipant{})
						So(err, ShouldNotBeNil)
					})
				})

				Convey("when the effect type is supported", func() {
					mockCondition := models.Condition{
						SupportedEffects: []models.EffectType{
							models.GiveBits,
						},
					}

					conditionGetter.On("GetActiveCondition", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockCondition, nil)

					Convey("when the effect fails validation", func() {
						effectController.On("ValidateEffect", mock.Anything, mock.Anything).Return(errors.New("ERROR"))

						Convey("we should error", func() {
							cond, err := controller.CreateConditionParticipant(context.Background(), mockDomain, mockConditionID, mockConditionOwnerID, mockConditionParticipantOwnerID, mockEffect, mockTTL)

							So(cond, ShouldResemble, models.ConditionParticipant{})
							So(err, ShouldNotBeNil)
						})
					})

					Convey("when the effect passes validation", func() {
						effectController.On("ValidateEffect", mock.Anything, mock.Anything).Return(nil)

						Convey("when ConditionParticipantDAO.CreateConditionParticipant returns an error", func() {
							conditionParticipantDAO.On("CreateConditionParticipant", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("ERROR"))

							Convey("we should error", func() {
								cond, err := controller.CreateConditionParticipant(context.Background(), mockDomain, mockConditionID, mockConditionOwnerID, mockConditionParticipantOwnerID, mockEffect, mockTTL)

								So(cond, ShouldResemble, models.ConditionParticipant{})
								So(err, ShouldNotBeNil)
							})
						})

						Convey("when ConditionParticipantDAO.CreateConditionParticipant returns nil", func() {
							conditionParticipantDAO.On("CreateConditionParticipant", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

							Convey("we should error", func() {
								cond, err := controller.CreateConditionParticipant(context.Background(), mockDomain, mockConditionID, mockConditionOwnerID, mockConditionParticipantOwnerID, mockEffect, mockTTL)

								So(cond, ShouldResemble, models.ConditionParticipant{})
								So(err, ShouldNotBeNil)
							})
						})

						Convey("when ConditionParticipantDAO.CreateConditionParticipant does not return an error", func() {
							mockConditionParticipant := models.ConditionParticipant{
								TTLSeconds: 60,
							}

							conditionParticipantDAO.On("CreateConditionParticipant", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&mockConditionParticipant, nil)

							Convey("when SFNClient.ExecuteTimeoutConditionParticipantStateMachine returns an error", func() {
								sfn.On("ExecuteTimeoutConditionParticipantStateMachine", mock.Anything).Return(errors.New("ERROR"))
								conditionParticipantDAO.On("UpdateConditionParticipant", mock.Anything, mock.Anything).Return(&models.ConditionParticipant{}, nil)
								sns.On("PublishConditionParticipantCleanup", mock.Anything, mock.Anything).Return(nil)

								Convey("we should error", func() {
									cond, err := controller.CreateConditionParticipant(context.Background(), mockDomain, mockConditionID, mockConditionOwnerID, mockConditionParticipantOwnerID, mockEffect, mockTTL)

									So(cond, ShouldResemble, models.ConditionParticipant{})
									So(err, ShouldNotBeNil)

									Convey("we should send a ConditionParticipantCleanup sns message", func() {
										time.Sleep(time.Second) // await goroutine execution
										sns.AssertCalled(t, "PublishConditionParticipantCleanup", mock.Anything, mock.Anything)
									})
								})
							})

							Convey("when SFNClient.ExecuteTimeoutConditionParticipantStateMachine does not return an error", func() {
								sfn.On("ExecuteTimeoutConditionParticipantStateMachine", mock.Anything).Return(nil)

								Convey("when EffectController.CreateEffectHold errors", func() {
									effectController.On("CreateEffectHold", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(models.Effect{}, nil, errors.New("ERROR"))
									conditionParticipantDAO.On("UpdateConditionParticipant", mock.Anything, mock.Anything).Return(nil, errors.New("ERROR"))
									sns.On("PublishConditionParticipantCleanup", mock.Anything, mock.Anything).Return(nil)

									Convey("we should error", func() {
										cond, err := controller.CreateConditionParticipant(context.Background(), mockDomain, mockConditionID, mockConditionOwnerID, mockConditionParticipantOwnerID, mockEffect, mockTTL)

										So(cond, ShouldResemble, models.ConditionParticipant{})
										So(err, ShouldNotBeNil)

										Convey("we should send a ConditionParticipantCleanup sns message", func() {
											time.Sleep(time.Second) // await goroutine execution
											sns.AssertCalled(t, "PublishConditionParticipantCleanup", mock.Anything, mock.Anything)
										})
									})
								})

								Convey("when EffectController.CreateEffectHold does not error", func() {
									mockTransactionID := "some-transaction-id"
									mockEffect := models.Effect{
										EffectType: models.GiveBits,
										Params: models.EffectParams{
											GiveBits: &models.EffectParamsGiveBits{},
										},
										TransactionID: mockTransactionID,
									}

									effectController.On("CreateEffectHold", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(mockEffect, nil, nil)

									Convey("when ConditionParticipantDAO.UpdateConditionParticipant errors", func() {
										conditionParticipantDAO.On("UpdateConditionParticipant", mock.Anything, mock.Anything).Return(nil, errors.New("ERROR"))
										sns.On("PublishConditionParticipantCleanup", mock.Anything, mock.Anything).Return(nil)

										Convey("we should error", func() {
											cond, err := controller.CreateConditionParticipant(context.Background(), mockDomain, mockConditionID, mockConditionOwnerID, mockConditionParticipantOwnerID, mockEffect, mockTTL)

											So(cond, ShouldResemble, models.ConditionParticipant{})
											So(err, ShouldNotBeNil)

											Convey("we should send a ConditionParticipantCleanup sns message", func() {
												time.Sleep(time.Second) // await goroutine execution
												sns.AssertCalled(t, "PublishConditionParticipantCleanup", mock.Anything, mock.Anything)
											})
										})
									})

									Convey("when ConditionParticipantDAO.UpdateConditionParticipant does not error", func() {
										expectedConditionParticipant := models.ConditionParticipant{
											Effect:                              mockEffect,
											ConditionParticipantProcessingState: models.ConditionParticipantProcessingStatePending,
											TTLSeconds:                          60,
										}

										conditionParticipantDAO.On("UpdateConditionParticipant", mock.Anything, &expectedConditionParticipant).Return(&expectedConditionParticipant, nil)

										Convey("we should return the conditionParticipant", func() {
											cond, err := controller.CreateConditionParticipant(context.Background(), mockDomain, mockConditionID, mockConditionOwnerID, mockConditionParticipantOwnerID, mockEffect, mockTTL)

											So(cond, ShouldResemble, expectedConditionParticipant)
											So(err, ShouldBeNil)

											Convey("we should send a pubsub message", func() {
												time.Sleep(time.Second) // await goroutine execution
												pubsubClient.AssertNumberOfCalls(t, "PublishConditionParticipantCreate", 1)
											})

											Convey("we should calculate and store condition aggregates", func() {
												time.Sleep(time.Second) // await goroutine execution
												conditionAggregatesController.AssertNumberOfCalls(t, "TryCalculateAndStore", 1)
											})
										})
									})
								})
							})
						})
					})
				})
			})
		})
	})
}
