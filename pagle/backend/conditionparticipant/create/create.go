package create

import (
	"context"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend/condition/effecttotals"
	conditiongetter "code.justin.tv/commerce/pagle/backend/condition/get"
	"code.justin.tv/commerce/pagle/backend/dynamo/conditionparticipant"
	"code.justin.tv/commerce/pagle/backend/effect"
	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/backend/pubsub"
	"code.justin.tv/commerce/pagle/backend/sfn"
	"code.justin.tv/commerce/pagle/backend/sns"
	"code.justin.tv/commerce/pagle/backend/utils/effect/settings"
)

type Creator interface {
	CreateConditionParticipant(ctx context.Context, domain, conditionID, conditionOwnerID, conditionParticipantOwnerID string, effect models.Effect, ttlSeconds int32) (models.ConditionParticipant, error)
}

type creatorImpl struct {
	ConditionParticipantDAO conditionparticipant.DAO  `inject:""`
	ConditionGetter         conditiongetter.Getter    `inject:"conditiongetter"`
	EffectController        effect.EffectController   `inject:"effectcontroller"`
	PubsubClient            pubsub.PubsubClient       `inject:""`
	EffectTotals            effecttotals.EffectTotals `inject:"conditionaggregatescontroller"`
	SNSClient               sns.SNSClient             `inject:""`
	SFNClient               sfn.SFNClient             `inject:""`
}

func NewCreator() Creator {
	return &creatorImpl{}
}

/**
High level steps:
	1) Validates conditionParticipant settings
	2) Validates associated condition is active
	3) Validates effect
	4) Creates ConditionParticipant in Dynamo
	5) Creates step function execution to handle conditionParticipant ttl
	6) Creates hold on effect, and updates effect with hold transactionID
	7) Updates ConditionParticipant in Dynamo with updated effect

If any of steps 5-7 fail, we enqueue work to clean up the conditionParticipant and return an error
*/
func (c *creatorImpl) CreateConditionParticipant(ctx context.Context, domain, conditionID, conditionOwnerID, conditionParticipantOwnerID string, effect models.Effect, ttlSeconds int32) (models.ConditionParticipant, error) {
	condition, validationErr := c.validateConditionParticipantParams(ctx, domain, conditionID, conditionOwnerID, effect, ttlSeconds)

	if validationErr != nil {
		return models.ConditionParticipant{}, validationErr
	}

	conditionParticipant, err := c.ConditionParticipantDAO.CreateConditionParticipant(ctx, condition.Domain, condition.ConditionID, condition.ConditionRunID, conditionParticipantOwnerID, condition.OwnerID, ttlSeconds, effect)

	if err != nil {
		return models.ConditionParticipant{}, err
	}

	if conditionParticipant == nil {
		return models.ConditionParticipant{}, errors.InternalError.New("CreateConditionParticipant returned a nil conditionParticipant")
	}

	_, err = c.conditionParticipantCreationPostProcessing(ctx, conditionParticipant, effect, ttlSeconds, condition)

	if err != nil {
		// Process async as we don't care about the return value
		// Passing background context so that error handler is not halted upon request cancellation
		go c.recordConditionParticipantCreationPostProcessingError(context.Background(), conditionParticipant, err)
		return models.ConditionParticipant{}, err
	}

	// Send pubsub event
	go func() {
		_ = c.PubsubClient.PublishConditionParticipantCreate(*conditionParticipant)
	}()

	// Update aggregates for associated condition
	go func() {
		_ = c.EffectTotals.TryCalculateAndStore(context.Background(), *conditionParticipant)
	}()

	return *conditionParticipant, nil
}

func (c *creatorImpl) recordConditionParticipantCreationPostProcessingError(ctx context.Context, conditionParticipant *models.ConditionParticipant, err error) {
	if conditionParticipant == nil {
		logrus.Error("recordConditionParticipantCreationPostProcessingError received a nil conditionParticipant")
		return
	}

	conditionParticipant.Error = err.Error()
	conditionParticipant.ConditionParticipantProcessingState = models.ConditionParticipantProcessingStateFailedToCreate

	logFields := logrus.Fields{
		"conditionParticipantId":                          conditionParticipant.ConditionParticipantID,
		"conditionParticipantCreationPostProcessingError": conditionParticipant.Error,
	}

	_, updateErr := c.ConditionParticipantDAO.UpdateConditionParticipant(ctx, conditionParticipant)

	if updateErr != nil {
		logrus.WithFields(logFields).WithError(updateErr).Error("UpdateConditionParticipant errored in recordConditionParticipantCreationPostProcessingError")
	}

	snsErr := c.SNSClient.PublishConditionParticipantCleanup(ctx, models.ConditionParticipantCleanupSNSMessage{
		ConditionParticipantID: conditionParticipant.ConditionParticipantID,
		ConditionRunID:         conditionParticipant.ConditionRunID,
	})

	if snsErr != nil {
		logrus.WithFields(logFields).WithError(snsErr).Error("UpdateConditionParticipant errored in recordConditionParticipantCreationPostProcessingError")
	}
}

func (c *creatorImpl) conditionParticipantCreationPostProcessing(
	ctx context.Context,
	conditionParticipant *models.ConditionParticipant,
	effect models.Effect,
	ttlSeconds int32,
	condition models.Condition) (*models.ConditionParticipant, error) {
	if conditionParticipant == nil {
		return nil, errors.IllegalArgument.New("conditionParticipantCreationPostProcessing received a nil conditionParticipant")
	}

	logFields := logrus.Fields{
		"conditionParticipantId": conditionParticipant.ConditionParticipantID,
		"conditionId":            conditionParticipant.ConditionID,
	}

	err := c.SFNClient.ExecuteTimeoutConditionParticipantStateMachine(models.TimeoutConditionParticipantSFNInput{
		ConditionParticipantID: conditionParticipant.ConditionParticipantID,
		ConditionRunID:         conditionParticipant.ConditionRunID,
		TTLSeconds:             conditionParticipant.TTLSeconds,
	})

	if err != nil {
		logrus.WithFields(logFields).WithError(err).Error("unable to create conditionParticipant timeout step function")

		return conditionParticipant, err
	}

	createdEffect, validationErr, err := c.EffectController.CreateEffectHold(ctx, effect, ttlSeconds, condition)

	if validationErr != nil {
		logrus.WithFields(logFields).WithError(validationErr).Warn("validation error creating effect hold")
		return conditionParticipant, validationErr
	}

	if err != nil {
		logrus.WithFields(logFields).WithError(err).Error("error creating effect hold")
		return conditionParticipant, err
	}

	conditionParticipant.Effect = createdEffect
	conditionParticipant.ConditionParticipantProcessingState = models.ConditionParticipantProcessingStatePending

	_, err = c.ConditionParticipantDAO.UpdateConditionParticipant(ctx, conditionParticipant)

	if err != nil {
		return conditionParticipant, err
	}

	return conditionParticipant, nil
}

func (c *creatorImpl) validateConditionParticipantParams(ctx context.Context, domain, conditionID, conditionOwnerID string, effect models.Effect, ttlSeconds int32) (models.Condition, error) {
	if ttlSeconds < models.ConditionParticipantMinTTL || ttlSeconds > models.ConditionParticipantMaxTTL {
		return models.Condition{}, errors.IllegalArgument.New("conditionParticipant ttl out of bounds")
	}

	condition, err := c.ConditionGetter.GetActiveCondition(ctx, domain, conditionID, conditionOwnerID)

	if err != nil {
		return models.Condition{}, err
	}

	isEffectSupported := settings.IsEffectTypeSupportedByCondition(condition, effect.EffectType)

	if !isEffectSupported {
		return models.Condition{}, errors.EffectTypeUnsupportedByCondition.New("effectType not supported for condition")
	}

	err = c.EffectController.ValidateEffect(ctx, effect)

	if err != nil {
		return models.Condition{}, err
	}

	return condition, nil
}
