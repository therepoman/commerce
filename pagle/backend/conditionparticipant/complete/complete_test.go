package complete

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pagle/backend/models"
	effecttotals_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/condition/effecttotals"
	conditiongetter_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/condition/get"
	conditionparticipantgetter_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/conditionparticipant/get"
	conditionrungetter_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/conditionrun/get"
	conditionparticipantdao_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/dynamo/conditionparticipant"
	effectcontroller_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/effect"
	pubsub_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/pubsub"
	snsclient_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/sns"
	statter_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/stats"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestConditionParticipantController_ProcessConditionParticipant(t *testing.T) {
	Convey("given a conditionParticipant controller", t, func() {
		conditionParticipantDAO := new(conditionparticipantdao_mock.DAO)
		effectController := new(effectcontroller_mock.EffectController)
		conditionAggregatesController := new(effecttotals_mock.EffectTotals)
		conditionGetter := new(conditiongetter_mock.Getter)
		conditionRunGetter := new(conditionrungetter_mock.Getter)
		snsClient := new(snsclient_mock.SNSClient)
		statter := new(statter_mock.Statter)
		pubsubClient := new(pubsub_mock.PubsubClient)
		conditionParticipantGetter := new(conditionparticipantgetter_mock.Getter)

		controller := completerImpl{
			ConditionParticipantDAO:    conditionParticipantDAO,
			EffectController:           effectController,
			EffectTotals:               conditionAggregatesController,
			ConditionRunGetter:         conditionRunGetter,
			ConditionGetter:            conditionGetter,
			SNSClient:                  snsClient,
			Statter:                    statter,
			PubsubClient:               pubsubClient,
			ConditionParticipantGetter: conditionParticipantGetter,
		}

		statter.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything)
		statter.On("Inc", mock.Anything, mock.Anything, mock.Anything)
		pubsubClient.On("PublishConditionParticipantEndStateUpdate", mock.Anything).Return(nil)
		conditionAggregatesController.On("TryCalculateAndStore", mock.Anything, mock.Anything).Return(nil)

		mockConditionID := "some-condition-id"
		mockConditionOwnerID := "some-condition-owner"
		mockDomain := "some-domain"
		mockConditionRunID := "condition-run-id"
		mockConditionParticipantID := "condition-participant-id"
		mockEffectOutput := models.EffectOutput{}

		Convey("when GetConditionParticipant returns an error", func() {
			conditionParticipantGetter.On("GetConditionParticipant", mock.Anything, mock.Anything, mock.Anything).Return(models.ConditionParticipant{}, errors.New("ERROR"))

			Convey("we should return an error", func() {
				err := controller.ProcessConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndStateSatisfied, nil, true)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the conditionParticipant has already been processed", func() {
			mockConditionParticipant := models.ConditionParticipant{
				ConditionParticipantID:              mockConditionParticipantID,
				ConditionParticipantProcessingState: models.ConditionParticipantProcessingStateSuccess,
				ConditionParticipantEndState:        models.ConditionParticipantEndStateCanceled,
			}

			conditionParticipantGetter.On("GetConditionParticipant", mock.Anything, mock.Anything, mock.Anything).Return(mockConditionParticipant, nil)
			effectController.On("ReleaseEffect", mock.Anything, mock.Anything).Return(nil)

			Convey("we should do nothing and return nil", func() {
				err := controller.ProcessConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndStateConditionCanceled, nil, true)

				So(err, ShouldBeNil)
				effectController.AssertNotCalled(t, "ReleaseEffect", mock.Anything, mock.Anything)
			})
		})

		Convey("when GetConditionParticipant returns the conditionParticipant, and it has not been processed", func() {
			Convey("when the call to ConditionRunController.GetConditionRun errors, we should return the error", func() {
				mockConditionParticipant := models.ConditionParticipant{
					ConditionParticipantID:              mockConditionParticipantID,
					ConditionParticipantProcessingState: models.ConditionParticipantProcessingStatePending,
					ConditionParticipantEndState:        models.ConditionParticipantEndStatePendingCompletion,
				}

				conditionParticipantGetter.On("GetConditionParticipant", mock.Anything, mock.Anything, mock.Anything).Return(mockConditionParticipant, nil)
				conditionRunGetter.On("GetConditionRun", mock.Anything, mockConditionRunID).Return(models.ConditionRun{}, errors.New("ERROR"))

				err := controller.ProcessConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndStateConditionCanceled, nil, true)
				So(err, ShouldNotBeNil)
			})

			Convey("when the condition run is pending and the change was triggered by a condition run completion, we should return an error", func() {
				mockConditionParticipant := models.ConditionParticipant{
					ConditionParticipantID:              mockConditionParticipantID,
					ConditionParticipantProcessingState: models.ConditionParticipantProcessingStatePending,
					ConditionParticipantEndState:        models.ConditionParticipantEndStatePendingCompletion,
				}

				conditionParticipantGetter.On("GetConditionParticipant", mock.Anything, mock.Anything, mock.Anything).Return(mockConditionParticipant, nil)

				mockConditionRun := models.ConditionRun{
					ConditionRunEndState: models.ConditionRunEndStatePending,
				}

				conditionRunGetter.On("GetConditionRun", mock.Anything, mockConditionRunID).Return(mockConditionRun, nil)

				err := controller.ProcessConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndStateCanceled, nil, true)
				So(err, ShouldNotBeNil)
			})

			Convey("when the condition run is not pending and the change was not triggered by a condition run completion, we should return nil", func() {
				mockConditionParticipant := models.ConditionParticipant{
					ConditionParticipantID:              mockConditionParticipantID,
					ConditionParticipantProcessingState: models.ConditionParticipantProcessingStatePending,
					ConditionParticipantEndState:        models.ConditionParticipantEndStatePendingCompletion,
				}

				conditionParticipantGetter.On("GetConditionParticipant", mock.Anything, mock.Anything, mock.Anything).Return(mockConditionParticipant, nil)

				mockConditionRun := models.ConditionRun{
					ConditionRunEndState: models.ConditionRunEndStateSatisfied,
				}

				conditionRunGetter.On("GetConditionRun", mock.Anything, mockConditionRunID).Return(mockConditionRun, nil)

				err := controller.ProcessConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndStateCanceled, nil, false)
				So(err, ShouldBeNil)
			})

			Convey("when the participant is in a valid state to be worked on", func() {
				mockConditionParticipant := models.ConditionParticipant{
					ConditionParticipantID:              mockConditionParticipantID,
					ConditionParticipantProcessingState: models.ConditionParticipantProcessingStatePending,
					ConditionParticipantEndState:        models.ConditionParticipantEndStatePendingCompletion,
					ConditionOwnerID:                    mockConditionOwnerID,
					Domain:                              mockDomain,
					ConditionID:                         mockConditionID,
				}

				mockConditionRun := models.ConditionRun{
					ConditionRunEndState: models.ConditionRunEndStateSatisfied,
				}

				mockCondition := models.Condition{}

				conditionRunGetter.On("GetConditionRun", mock.Anything, mockConditionRunID).Return(mockConditionRun, nil)

				Convey("when updating the conditionParticipant prior to processing errors, we should return the error", func() {
					conditionParticipantGetter.On("GetConditionParticipant", mock.Anything, mock.Anything, mock.Anything).Return(mockConditionParticipant, nil)
					conditionParticipantDAO.On("UpdateConditionParticipant", mock.Anything, mock.Anything).Return(nil, errors.New("ERROR"))

					err := controller.ProcessConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndStateConditionCanceled, nil, true)

					So(err, ShouldNotBeNil)
				})

				Convey("when updating the conditionParticipant prior to processing does not error", func() {
					Convey("we should store the conditionParticipantEndState", func() {
						conditionParticipantGetter.On("GetConditionParticipant", mock.Anything, mock.Anything, mock.Anything).Return(mockConditionParticipant, nil)
						conditionParticipantDAO.On("UpdateConditionParticipant", mock.Anything, mock.MatchedBy(func(conditionParticipant *models.ConditionParticipant) bool {
							return conditionParticipant.ConditionParticipantEndState == models.ConditionParticipantEndStateConditionCanceled
						})).Return(nil, nil).Once()
						effectController.On("ReleaseEffect", mock.Anything, mock.Anything).Return(nil)
						conditionParticipantDAO.On("UpdateConditionParticipant", mock.Anything, mock.Anything).Return(nil, nil).Once()
						snsClient.On("PublishConditionParticipantCompletion", mock.Anything, mock.Anything).Return(nil)
						conditionGetter.On("GetCondition", mock.Anything, mockDomain, mockConditionID, mockConditionOwnerID).Return(mockCondition, nil)

						err := controller.ProcessConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndStateConditionCanceled, nil, true)
						So(err, ShouldBeNil)
						conditionParticipantDAO.AssertNumberOfCalls(t, "UpdateConditionParticipant", 2)
					})

					Convey("we should store the effectOutput if not nil", func() {
						conditionParticipantGetter.On("GetConditionParticipant", mock.Anything, mock.Anything, mock.Anything).Return(mockConditionParticipant, nil)
						conditionParticipantDAO.On("UpdateConditionParticipant", mock.Anything, mock.MatchedBy(func(conditionParticipant *models.ConditionParticipant) bool {
							return conditionParticipant.ConditionParticipantEndState == models.ConditionParticipantEndStateConditionCanceled && conditionParticipant.Effect.Output == mockEffectOutput
						})).Return(nil, nil).Once()
						effectController.On("ReleaseEffect", mock.Anything, mock.Anything).Return(nil)
						conditionParticipantDAO.On("UpdateConditionParticipant", mock.Anything, mock.Anything).Return(nil, nil).Once()
						snsClient.On("PublishConditionParticipantCompletion", mock.Anything, mock.Anything).Return(nil)
						conditionGetter.On("GetCondition", mock.Anything, mockDomain, mockConditionID, mockConditionOwnerID).Return(mockCondition, nil)

						err := controller.ProcessConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndStateConditionCanceled, &mockEffectOutput, true)
						So(err, ShouldBeNil)
						conditionParticipantDAO.AssertNumberOfCalls(t, "UpdateConditionParticipant", 2)
					})

					Convey("when the processing state is unhandled", func() {
						conditionGetter.On("GetCondition", mock.Anything, mockDomain, mockConditionID, mockConditionOwnerID).Return(mockCondition, nil)
						conditionParticipantDAO.On("UpdateConditionParticipant", mock.Anything, mock.Anything).Return(nil, nil).Once()

						mockConditionParticipant := models.ConditionParticipant{
							ConditionParticipantID: mockConditionParticipantID,
							ConditionOwnerID:       mockConditionOwnerID,
							Domain:                 mockDomain,
							ConditionID:            mockConditionID,
						}

						conditionParticipantGetter.On("GetConditionParticipant", mock.Anything, mock.Anything, mock.Anything).Return(mockConditionParticipant, nil)

						Convey("we should return an error", func() {
							err := controller.ProcessConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndState("FAKE"), nil, true)

							So(err, ShouldNotBeNil)
						})
					})

					Convey("when processing a satisfied condition", func() {
						conditionParticipantDAO.On("UpdateConditionParticipant", mock.Anything, mock.Anything).Return(nil, nil).Once()
						conditionParticipantGetter.On("GetConditionParticipant", mock.Anything, mock.Anything, mock.Anything).Return(mockConditionParticipant, nil)

						Convey("when GetCondition errors, we should error", func() {
							conditionGetter.On("GetCondition", mock.Anything, mockDomain, mockConditionID, mockConditionOwnerID).Return(mockCondition, errors.New("ERROR"))

							err := controller.ProcessConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndStateConditionCanceled, nil, true)

							So(err, ShouldNotBeNil)
						})

						Convey("when GetCondition does not error", func() {
							conditionGetter.On("GetCondition", mock.Anything, mockDomain, mockConditionID, mockConditionOwnerID).Return(mockCondition, nil)

							Convey("when the call to FinalizeEffect returns an error", func() {
								mockError := errors.New("ERROR")
								effectController.On("FinalizeEffect", mock.Anything, mock.Anything, mock.Anything, mockCondition).Return(mockError)
								conditionParticipantDAO.On("UpdateConditionParticipant", mock.Anything, mock.MatchedBy(func(conditionParticipant *models.ConditionParticipant) bool {
									return conditionParticipant.Error == mockError.Error() && conditionParticipant.ConditionParticipantProcessingState == models.ConditionParticipantProcessingStateProcessingError
								})).Return(&mockConditionParticipant, nil).Once()

								Convey("we should set the conditionParticipant state to error", func() {
									err := controller.ProcessConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndStateSatisfied, nil, true)

									So(err, ShouldNotBeNil)
									conditionParticipantDAO.AssertNumberOfCalls(t, "UpdateConditionParticipant", 2)
								})

							})

							Convey("when the call to FinalizeEffect does not return an error", func() {
								snsClient.On("PublishConditionParticipantCompletion", mock.Anything, mock.Anything).Return(nil)
								effectController.On("FinalizeEffect", mock.Anything, mock.Anything, mock.Anything, mockCondition).Return(nil)
								conditionParticipantDAO.On("UpdateConditionParticipant", mock.Anything, mock.MatchedBy(func(conditionParticipant *models.ConditionParticipant) bool {
									return conditionParticipant.ConditionParticipantProcessingState == models.ConditionParticipantProcessingStateSuccess
								})).Return(&mockConditionParticipant, nil).Once()

								Convey("we should set the conditionParticipant state to success", func() {
									err := controller.ProcessConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndStateSatisfied, nil, true)

									So(err, ShouldBeNil)
									conditionParticipantDAO.AssertNumberOfCalls(t, "UpdateConditionParticipant", 2)
								})
							})
						})
					})

					Convey("when processing a terminated condition", func() {
						conditionParticipantDAO.On("UpdateConditionParticipant", mock.Anything, mock.Anything).Return(nil, nil).Once()
						conditionParticipantGetter.On("GetConditionParticipant", mock.Anything, mock.Anything, mock.Anything).Return(mockConditionParticipant, nil)

						Convey("when GetCondition errors, we should error", func() {
							conditionGetter.On("GetCondition", mock.Anything, mockDomain, mockConditionID, mockConditionOwnerID).Return(mockCondition, errors.New("ERROR"))

							err := controller.ProcessConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndStateConditionCanceled, nil, true)

							So(err, ShouldNotBeNil)
						})

						Convey("when GetCondition does not error", func() {
							conditionGetter.On("GetCondition", mock.Anything, mockDomain, mockConditionID, mockConditionOwnerID).Return(mockCondition, nil)

							Convey("when the call to ReleaseEffect returns an error", func() {
								mockError := errors.New("ERROR")
								effectController.On("ReleaseEffect", mock.Anything, mock.Anything).Return(mockError)
								conditionParticipantDAO.On("UpdateConditionParticipant", mock.Anything, mock.MatchedBy(func(conditionParticipant *models.ConditionParticipant) bool {
									return conditionParticipant.Error == mockError.Error() && conditionParticipant.ConditionParticipantProcessingState == models.ConditionParticipantProcessingStateProcessingError
								})).Return(&mockConditionParticipant, nil).Once()

								Convey("we should set the conditionParticipant state to error", func() {
									err := controller.ProcessConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndStateConditionCanceled, nil, true)

									So(err, ShouldNotBeNil)
									conditionParticipantDAO.AssertNumberOfCalls(t, "UpdateConditionParticipant", 2)
								})

							})

							Convey("when the call to ReleaseEffect does not return an error", func() {
								snsClient.On("PublishConditionParticipantCompletion", mock.Anything, mock.Anything).Return(nil)
								effectController.On("ReleaseEffect", mock.Anything, mock.Anything).Return(nil)
								conditionParticipantDAO.On("UpdateConditionParticipant", mock.Anything, mock.MatchedBy(func(conditionParticipant *models.ConditionParticipant) bool {
									return conditionParticipant.ConditionParticipantProcessingState == models.ConditionParticipantProcessingStateSuccess
								})).Return(&mockConditionParticipant, nil).Once()

								Convey("we should set the conditionParticipant state to terminated", func() {
									err := controller.ProcessConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndStateConditionCanceled, nil, true)

									So(err, ShouldBeNil)
									conditionParticipantDAO.AssertNumberOfCalls(t, "UpdateConditionParticipant", 2)
								})
							})
						})
					})

					conditionGetter.On("GetCondition", mock.Anything, mockDomain, mockConditionID, mockConditionOwnerID).Return(mockCondition, nil)

					Convey("when UpdateConditionParticipant returns an error", func() {
						conditionParticipantDAO.On("UpdateConditionParticipant", mock.Anything, mock.Anything).Return(nil, nil).Once()

						conditionParticipantGetter.On("GetConditionParticipant", mock.Anything, mock.Anything, mock.Anything).Return(mockConditionParticipant, nil)
						effectController.On("ReleaseEffect", mock.Anything, mock.Anything).Return(nil)
						conditionParticipantDAO.On("UpdateConditionParticipant", mock.Anything, mock.Anything).Return(nil, errors.New("ERROR")).Once()

						Convey("we should return an error", func() {
							err := controller.ProcessConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndStateConditionCanceled, nil, true)

							So(err, ShouldNotBeNil)
						})
					})

					Convey("when UpdateConditionParticipant does not return an error", func() {
						conditionParticipantDAO.On("UpdateConditionParticipant", mock.Anything, mock.Anything).Return(nil, nil).Once()

						mockConditionParticipant := models.ConditionParticipant{
							ConditionParticipantID:       mockConditionParticipantID,
							ConditionParticipantEndState: models.ConditionParticipantEndStateConditionCanceled,
							ConditionOwnerID:             mockConditionOwnerID,
							Domain:                       mockDomain,
							ConditionID:                  mockConditionID,
						}

						conditionParticipantGetter.On("GetConditionParticipant", mock.Anything, mock.Anything, mock.Anything).Return(mockConditionParticipant, nil)
						effectController.On("ReleaseEffect", mock.Anything, mock.Anything).Return(nil)
						conditionParticipantDAO.On("UpdateConditionParticipant", mock.Anything, mock.Anything).Return(&mockConditionParticipant, nil).Once()

						Convey("we should send a pubsub message", func() {
							snsClient.On("PublishConditionParticipantCompletion", mock.Anything, mock.Anything).Return(nil)
							err := controller.ProcessConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndStateConditionCanceled, nil, true)
							So(err, ShouldBeNil)

							pubsubClient.AssertNumberOfCalls(t, "PublishConditionParticipantEndStateUpdate", 1)
						})

						Convey("we should calculate and store condition aggregates", func() {
							snsClient.On("PublishConditionParticipantCompletion", mock.Anything, mock.Anything).Return(nil)
							err := controller.ProcessConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndStateConditionCanceled, nil, true)
							So(err, ShouldBeNil)

							conditionAggregatesController.AssertNumberOfCalls(t, "TryCalculateAndStore", 1)
						})

						Convey("when SNS client returns an error", func() {
							snsClient.On("PublishConditionParticipantCompletion", mock.Anything, mock.Anything).Return(errors.New("ERROR"))

							Convey("we should return an error", func() {
								err := controller.ProcessConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndStateConditionCanceled, nil, true)

								So(err, ShouldNotBeNil)
							})
						})

						Convey("when SNS client does not return an error", func() {
							Convey("we should return nil", func() {
								snsClient.On("PublishConditionParticipantCompletion", mock.Anything, mock.Anything).Return(nil)

								Convey("we should return nil", func() {
									err := controller.ProcessConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndStateConditionCanceled, nil, true)

									So(err, ShouldBeNil)
								})
							})
						})
					})
				})
			})
		})
	})
}

func TestConditionParticipantController_ExpireConditionParticipant(t *testing.T) {
	Convey("given a conditionParticipant controller", t, func() {
		conditionParticipantDAO := new(conditionparticipantdao_mock.DAO)
		conditionRunGetter := new(conditionrungetter_mock.Getter)
		sns := new(snsclient_mock.SNSClient)
		conditionParticipantGetter := new(conditionparticipantgetter_mock.Getter)

		controller := completerImpl{
			ConditionParticipantDAO:    conditionParticipantDAO,
			ConditionRunGetter:         conditionRunGetter,
			SNSClient:                  sns,
			ConditionParticipantGetter: conditionParticipantGetter,
		}

		mockConditionRunID := "some-run"
		mockConditionParticipantID := "some-participant"

		Convey("when GetConditionParticipant returns an error", func() {
			conditionParticipantGetter.On("GetConditionParticipant", mock.Anything, mock.Anything, mock.Anything).Return(models.ConditionParticipant{}, errors.New("ERROR"))

			Convey("we should return an error", func() {
				err := controller.ExpireConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when GetConditionParticipant returns a conditionParticipant that has a processing state other than pending, we should return an error", func() {
			mockConditionParticipant := models.ConditionParticipant{
				ConditionParticipantProcessingState: models.ConditionParticipantProcessingStateSuccess,
			}

			conditionParticipantGetter.On("GetConditionParticipant", mock.Anything, mock.Anything, mock.Anything).Return(mockConditionParticipant, nil)

			err := controller.ExpireConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID)
			So(err, ShouldNotBeNil)
		})

		Convey("when GetConditionParticipant returns a pending conditionParticipant", func() {
			mockConditionParticipant := models.ConditionParticipant{
				ConditionParticipantProcessingState: models.ConditionParticipantProcessingStatePending,
				ConditionParticipantID:              mockConditionParticipantID,
				ConditionRunID:                      mockConditionRunID,
			}

			conditionParticipantGetter.On("GetConditionParticipant", mock.Anything, mock.Anything, mock.Anything).Return(mockConditionParticipant, nil)

			Convey("when GetConditionRun returns an error, we should return an error", func() {
				conditionRunGetter.On("GetConditionRun", mock.Anything, mockConditionRunID).Return(models.ConditionRun{}, errors.New("ERROR"))

				err := controller.ExpireConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID)
				So(err, ShouldNotBeNil)
			})

			Convey("when GetConditionRun returns a run in a state other than pending, we should return an error", func() {
				mockConditionRun := models.ConditionRun{
					ConditionRunEndState: models.ConditionRunEndStateSatisfied,
				}

				conditionRunGetter.On("GetConditionRun", mock.Anything, mockConditionRunID).Return(mockConditionRun, nil)

				err := controller.ExpireConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID)
				So(err, ShouldNotBeNil)
			})

			Convey("when GetConditionRun returns a pending run, we should enqueue the participant for processing", func() {
				mockConditionRun := models.ConditionRun{
					ConditionRunEndState: models.ConditionRunEndStatePending,
				}

				expectedSNSMessage := models.ConditionParticipantProcessingSNSMessage{
					ConditionParticipantID:            mockConditionParticipantID,
					ConditionRunID:                    mockConditionRunID,
					ConditionParticipantEndState:      models.ConditionParticipantEndStateExpired,
					EffectOutput:                      nil,
					TriggeredByConditionRunCompletion: false,
				}

				conditionRunGetter.On("GetConditionRun", mock.Anything, mockConditionRunID).Return(mockConditionRun, nil)
				sns.On("PublishConditionParticipantProcessing", mock.Anything, expectedSNSMessage).Return(nil)

				err := controller.ExpireConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID)
				So(err, ShouldBeNil)
				sns.AssertNumberOfCalls(t, "PublishConditionParticipantProcessing", 1)
			})
		})
	})
}

func TestConditionParticipantController_EnqueueForProcessing(t *testing.T) {
	Convey("given a conditionParticipant controller", t, func() {
		conditionParticipantDAO := new(conditionparticipantdao_mock.DAO)
		sns := new(snsclient_mock.SNSClient)
		statter := new(statter_mock.Statter)

		controller := completerImpl{
			ConditionParticipantDAO: conditionParticipantDAO,
			SNSClient:               sns,
			Statter:                 statter,
		}

		statter.On("Inc", mock.Anything, mock.Anything, mock.Anything)

		mockConditionParticipantID := "some-participant"
		mockConditionRunID := "some-run"

		Convey("when we fail to publish to processing sns topic", func() {
			sns.On("PublishConditionParticipantProcessing", mock.Anything, mock.Anything).Return(errors.New("ERROR"))

			Convey("we should error", func() {
				err := controller.EnqueueForProcessing(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndStateSatisfied, &models.EffectOutput{}, false)

				So(err, ShouldNotBeNil)
			})
		})

		Convey("when we succeed publishing to sns processing topic", func() {
			sns.On("PublishConditionParticipantProcessing", mock.Anything, mock.Anything).Return(nil)

			Convey("we should return nil", func() {
				err := controller.EnqueueForProcessing(context.Background(), mockConditionRunID, mockConditionParticipantID, models.ConditionParticipantEndStateSatisfied, &models.EffectOutput{}, false)

				So(err, ShouldBeNil)
			})
		})
	})
}

func TestConditionParticipantController_CleanupConditionParticipant(t *testing.T) {
	Convey("given a conditionParticipant controller", t, func() {
		conditionParticipantDAO := new(conditionparticipantdao_mock.DAO)
		effectController := new(effectcontroller_mock.EffectController)
		statter := new(statter_mock.Statter)
		conditionParticipantGetter := new(conditionparticipantgetter_mock.Getter)

		controller := completerImpl{
			ConditionParticipantDAO:    conditionParticipantDAO,
			EffectController:           effectController,
			Statter:                    statter,
			ConditionParticipantGetter: conditionParticipantGetter,
		}

		conditionRunID := "some-condition-run"
		mockConditionParticipantID := "abc"

		statter.On("TimingDuration", mock.Anything, mock.Anything, mock.Anything)
		statter.On("Inc", mock.Anything, mock.Anything, mock.Anything)

		Convey("when GetConditionParticipant fails, we should error", func() {
			conditionParticipantGetter.On("GetConditionParticipant", mock.Anything, conditionRunID, mockConditionParticipantID).Return(models.ConditionParticipant{}, errors.New("ERROR"))

			err := controller.CleanupConditionParticipant(context.Background(), conditionRunID, mockConditionParticipantID)
			So(err, ShouldNotBeNil)
		})

		Convey("when GetConditionParticipant does not fail", func() {
			Convey("when processing state is not FailedToCreate, we should return nil and do nothing", func() {
				mockConditionParticipant := models.ConditionParticipant{
					ConditionParticipantID:              mockConditionParticipantID,
					ConditionParticipantProcessingState: models.ConditionParticipantProcessingStatePending,
				}

				conditionParticipantGetter.On("GetConditionParticipant", mock.Anything, conditionRunID, mockConditionParticipantID).Return(mockConditionParticipant, nil)
				effectController.On("ReleaseEffect", mock.Anything, mock.Anything).Return(nil)

				err := controller.CleanupConditionParticipant(context.Background(), conditionRunID, mockConditionParticipantID)
				So(err, ShouldBeNil)

				effectController.AssertNotCalled(t, "ReleaseEffect", mock.Anything, mock.Anything)
			})

			Convey("when processing state is FailedToCreate", func() {
				Convey("when effect.TransactionID is blank, we should return nil and do nothing", func() {
					mockConditionParticipant := models.ConditionParticipant{
						ConditionParticipantID:              mockConditionParticipantID,
						ConditionParticipantProcessingState: models.ConditionParticipantProcessingStateFailedToCreate,
						Effect: models.Effect{
							TransactionID: "",
						},
					}

					conditionParticipantGetter.On("GetConditionParticipant", mock.Anything, conditionRunID, mockConditionParticipantID).Return(mockConditionParticipant, nil)
					effectController.On("ReleaseEffect", mock.Anything, mock.Anything).Return(nil)

					err := controller.CleanupConditionParticipant(context.Background(), conditionRunID, mockConditionParticipantID)
					So(err, ShouldBeNil)

					effectController.AssertNotCalled(t, "ReleaseEffect", mock.Anything, mock.Anything)
				})

				Convey("when effect.TransactionID is not blank, we should release the effect hold", func() {
					mockConditionParticipant := models.ConditionParticipant{
						ConditionParticipantID:              mockConditionParticipantID,
						ConditionParticipantProcessingState: models.ConditionParticipantProcessingStateFailedToCreate,
						Effect: models.Effect{
							TransactionID: "123",
						},
					}

					conditionParticipantGetter.On("GetConditionParticipant", mock.Anything, conditionRunID, mockConditionParticipantID).Return(mockConditionParticipant, nil)
					effectController.On("ReleaseEffect", mock.Anything, mock.Anything).Return(nil)

					err := controller.CleanupConditionParticipant(context.Background(), conditionRunID, mockConditionParticipantID)
					So(err, ShouldBeNil)

					effectController.AssertCalled(t, "ReleaseEffect", mock.Anything, mock.Anything)
				})
			})
		})
	})
}
