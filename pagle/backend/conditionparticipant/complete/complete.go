package complete

import (
	"context"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend/condition/effecttotals"
	conditiongetter "code.justin.tv/commerce/pagle/backend/condition/get"
	conditionparticipantgetter "code.justin.tv/commerce/pagle/backend/conditionparticipant/get"
	conditionrungetter "code.justin.tv/commerce/pagle/backend/conditionrun/get"
	"code.justin.tv/commerce/pagle/backend/dynamo/conditionparticipant"
	"code.justin.tv/commerce/pagle/backend/effect"
	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/backend/pubsub"
	"code.justin.tv/commerce/pagle/backend/sns"
	"code.justin.tv/commerce/pagle/backend/stats"
)

const (
	enqueueForProcessingErrorStatName = "condition_participant_enqueue_for_processing_error"
	processingErrorStatName           = "condition_participant_processing_error"
	processingLatencyStatName         = "condition_participant_processing_latency"
	cleanupLatencyStatName            = "condition_participant_cleanup_latency"
	cleanupErrorStatName              = "condition_participant_cleanup_error"
)

type Completer interface {
	ProcessConditionParticipant(ctx context.Context, conditionRunID, conditionParticipantID string, conditionParticipantEndState models.ConditionParticipantEndState, effectOutput *models.EffectOutput, triggeredByConditionRunCompletion bool) error
	EnqueueForProcessing(ctx context.Context, conditionRunID, conditionParticipantID string, conditionParticipantEndState models.ConditionParticipantEndState, effectOutput *models.EffectOutput, triggeredByConditionRunCompletion bool) error
	ExpireConditionParticipant(ctx context.Context, conditionRunID, conditionParticipantID string) error
	CleanupConditionParticipant(ctx context.Context, conditionRunID, conditionParticipantID string) error
}

type completerImpl struct {
	ConditionParticipantDAO    conditionparticipant.DAO          `inject:""`
	SNSClient                  sns.SNSClient                     `inject:""`
	PubsubClient               pubsub.PubsubClient               `inject:""`
	EffectController           effect.EffectController           `inject:"effectcontroller"`
	Statter                    stats.Statter                     `inject:""`
	ConditionParticipantGetter conditionparticipantgetter.Getter `inject:"conditionparticipantgetter"`
	EffectTotals               effecttotals.EffectTotals         `inject:"conditionaggregatescontroller"`
	ConditionGetter            conditiongetter.Getter            `inject:"conditiongetter"`
	ConditionRunGetter         conditionrungetter.Getter         `inject:"conditionrungetter"`
}

func NewCompleter() Completer {
	return &completerImpl{}
}

// When we encounter an error in the middle conditionParticipant creation, we enqueue work to cleanup the conditionParticipant. Namely,
// release any holds established.
func (c *completerImpl) CleanupConditionParticipant(ctx context.Context, conditionRunID, conditionParticipantID string) error {
	startTime := time.Now()

	defer func() {
		c.Statter.TimingDuration(cleanupLatencyStatName, time.Since(startTime), 1)
	}()

	logFields := logrus.Fields{
		"condition_run_id":         conditionRunID,
		"condition_participant_id": conditionParticipantID,
	}

	conditionParticipant, err := c.ConditionParticipantGetter.GetConditionParticipant(ctx, conditionRunID, conditionParticipantID)

	if err != nil {
		c.logAndStatCleanupConditionParticipantError(logFields, err)
		return err
	}

	if conditionParticipant.ConditionParticipantProcessingState != models.ConditionParticipantProcessingStateFailedToCreate {
		logrus.WithFields(logFields).Warn("Attempting to cleanup a conditionParticipant not in the FailedToCreate state")
		return nil
	}

	// At this point in time, the only necessary cleanup step is to release a hold if one was created. If conditionParticipant creation
	// failed before that point, there is nothing to do.
	if strings.Blank(conditionParticipant.Effect.TransactionID) {
		return nil
	}

	err = c.EffectController.ReleaseEffect(ctx, conditionParticipant.Effect)

	if err != nil {
		c.logAndStatCleanupConditionParticipantError(logFields, err)
		return err
	}

	return nil
}

// Should only be called by ConditionTimeout Lambda
func (c *completerImpl) ExpireConditionParticipant(ctx context.Context, conditionRunID, conditionParticipantID string) error {
	conditionParticipant, err := c.ConditionParticipantGetter.GetConditionParticipant(ctx, conditionRunID, conditionParticipantID)

	if err != nil {
		return err
	}

	if conditionParticipant.ConditionParticipantProcessingState != models.ConditionParticipantProcessingStatePending {
		return errors.ConditionParticipantNotPending.New("attempting to expire a condition participant that has already been processed")
	}

	conditionRun, err := c.ConditionRunGetter.GetConditionRun(ctx, conditionRunID)

	if err != nil {
		return err
	}

	if conditionRun.ConditionRunEndState != models.ConditionRunEndStatePending {
		return errors.ConditionRunNotPending.New("attempting to expire a condition participant whose run is no longer pending")
	}

	return c.EnqueueForProcessing(
		ctx,
		conditionParticipant.ConditionRunID,
		conditionParticipant.ConditionParticipantID,
		models.ConditionParticipantEndStateExpired,
		nil,
		false)
}

func (c *completerImpl) ProcessConditionParticipant(
	ctx context.Context,
	conditionRunID,
	conditionParticipantID string,
	conditionParticipantEndState models.ConditionParticipantEndState,
	effectOutput *models.EffectOutput, triggeredByConditionRunCompletion bool) error {

	startTime := time.Now()
	defer func() {
		c.Statter.TimingDuration(processingLatencyStatName, time.Since(startTime), 1)
	}()

	logFields := logrus.Fields{
		"condition_run_id":                conditionRunID,
		"condition_participant_id":        conditionParticipantID,
		"condition_participant_end_state": conditionParticipantEndState,
		"effect_output":                   effectOutput,
	}

	conditionParticipant, err := c.ConditionParticipantGetter.GetConditionParticipant(ctx, conditionRunID, conditionParticipantID)

	if err != nil {
		c.logAndStatProcessConditionParticipantError(logFields, err)
		return err
	}

	if shouldRejectWorkForConditionParticipant(conditionParticipant) {
		logrus.
			WithFields(logFields).
			WithField("condition_participant_processing_state", conditionParticipant.ConditionParticipantProcessingState).
			Warn("attempting to work on a conditionParticipant in an invalid state")
		return nil
	}

	conditionRun, err := c.ConditionRunGetter.GetConditionRun(ctx, conditionRunID)

	if err != nil {
		c.logAndStatProcessConditionParticipantError(logFields, err)
		return err
	}

	canConditionParticipantFromRunBeWorkedOn, err := c.canConditionParticipantFromRunBeWorkedOn(conditionRun, triggeredByConditionRunCompletion)

	if err != nil {
		logrus.WithFields(logFields).WithField("condition_run_end_state", conditionRun.ConditionRunEndState).WithError(err).Error()
		return err
	}

	if !canConditionParticipantFromRunBeWorkedOn {
		logrus.WithFields(logFields).Warn("attempting to work on a conditionParticipant whose run is no longer pending")
		return nil
	}

	// EffectOutput and end state only exist on the SQS message until this point - persist to Dynamo
	conditionParticipant, err = c.setConditionParticipantEndState(ctx, conditionParticipant, conditionParticipantEndState, effectOutput)

	if err != nil {
		c.logAndStatProcessConditionParticipantError(logFields, err)
		return err
	}

	conditionParticipant, err = c.finalizeOrReleaseConditionParticipantEffect(ctx, conditionParticipant, conditionRun)

	if err != nil {
		c.logAndStatProcessConditionParticipantError(logFields, err)
		return err
	}

	// Send pubsub event
	_ = c.PubsubClient.PublishConditionParticipantEndStateUpdate(conditionParticipant)

	// Update aggregates for associated condition
	_ = c.EffectTotals.TryCalculateAndStore(ctx, conditionParticipant)

	err = c.SNSClient.PublishConditionParticipantCompletion(ctx, models.ConditionParticipantCompletionSNSMessage{
		ConditionParticipantID:              conditionParticipant.ConditionParticipantID,
		ConditionParticipantProcessingState: conditionParticipant.ConditionParticipantProcessingState,
		ConditionID:                         conditionParticipant.ConditionID,
		ConditionParticipantEndState:        conditionParticipant.ConditionParticipantEndState,
	})

	if err != nil {
		c.logAndStatProcessConditionParticipantError(logFields, err)
		return err
	}

	return nil
}

func (c *completerImpl) EnqueueForProcessing(
	ctx context.Context,
	conditionRunID,
	conditionParticipantID string,
	conditionParticipantEndState models.ConditionParticipantEndState,
	effectOutput *models.EffectOutput,
	triggeredByConditionRunCompletion bool) error {

	logFields := logrus.Fields{
		"condition_participant_id":       conditionParticipantID,
		"condition_run_id":               conditionRunID,
		"conditionParticipant_end_state": conditionParticipantEndState,
		"effect_output":                  effectOutput,
		"triggered_by_owning_condition":  triggeredByConditionRunCompletion,
	}

	err := c.SNSClient.PublishConditionParticipantProcessing(ctx, models.ConditionParticipantProcessingSNSMessage{
		ConditionParticipantID:            conditionParticipantID,
		ConditionRunID:                    conditionRunID,
		ConditionParticipantEndState:      conditionParticipantEndState,
		EffectOutput:                      effectOutput,
		TriggeredByConditionRunCompletion: triggeredByConditionRunCompletion,
	})

	if err != nil {
		c.Statter.Inc(enqueueForProcessingErrorStatName, 1, 1)
		logrus.WithFields(logFields).WithError(err).Error("failed to enqueue conditionParticipant for processing")

		return err
	}

	return nil
}

func (c *completerImpl) logAndStatProcessConditionParticipantError(logFields logrus.Fields, err error) {
	logrus.WithFields(logFields).WithError(err).Error("error processing condition participant")
	c.Statter.Inc(processingErrorStatName, 1, 1)
}

func (c *completerImpl) setConditionParticipantEndState(
	ctx context.Context,
	conditionParticipant models.ConditionParticipant,
	conditionParticipantEndState models.ConditionParticipantEndState,
	effectOutput *models.EffectOutput) (models.ConditionParticipant, error) {

	conditionParticipant.ConditionParticipantEndState = conditionParticipantEndState

	if effectOutput != nil {
		conditionParticipant.Effect.Output = *effectOutput
	}

	_, err := c.ConditionParticipantDAO.UpdateConditionParticipant(ctx, &conditionParticipant)

	return conditionParticipant, err
}

func (c *completerImpl) finalizeOrReleaseConditionParticipantEffect(
	ctx context.Context,
	conditionParticipant models.ConditionParticipant,
	conditionRun models.ConditionRun) (models.ConditionParticipant, error) {

	condition, err := c.ConditionGetter.GetCondition(ctx, conditionParticipant.Domain, conditionParticipant.ConditionID, conditionParticipant.ConditionOwnerID)

	if err != nil {
		return models.ConditionParticipant{}, err
	}

	var processingErr error

	switch conditionParticipant.ConditionParticipantEndState {
	case models.ConditionParticipantEndStateSatisfied:
		{
			processingErr = c.EffectController.FinalizeEffect(ctx, conditionParticipant.Effect, conditionRun.EffectSettings, condition)
		}
	case models.ConditionParticipantEndStateExpired:
		fallthrough
	case models.ConditionParticipantEndStateConditionCanceled:
		fallthrough
	case models.ConditionParticipantEndStateFailedValidation:
		fallthrough
	case models.ConditionParticipantEndStateCanceled:
		fallthrough
	case models.ConditionParticipantEndStateConditionExpired:
		{
			processingErr = c.EffectController.ReleaseEffect(ctx, conditionParticipant.Effect)
		}
	default:
		{
			return models.ConditionParticipant{}, errors.IllegalArgument.New("unhandled end state while processing conditionParticipant")
		}
	}

	// If we encounter an error processing the condition participant, we attempt to store it in dynamo and then
	// fail.
	if processingErr != nil {
		conditionParticipant.ConditionParticipantProcessingState = models.ConditionParticipantProcessingStateProcessingError
		conditionParticipant.Error = processingErr.Error()
	} else {
		conditionParticipant.ConditionParticipantProcessingState = models.ConditionParticipantProcessingStateSuccess
	}

	_, err = c.ConditionParticipantDAO.UpdateConditionParticipant(ctx, &conditionParticipant)

	if err != nil {
		return models.ConditionParticipant{}, err
	}

	return conditionParticipant, processingErr
}

func (c *completerImpl) canConditionParticipantFromRunBeWorkedOn(conditionRun models.ConditionRun, triggeredByConditionRunCompletion bool) (canWork bool, err error) {
	// If participant processing was triggered by condition run completion, the state should have been changed from PENDING
	if conditionRun.ConditionRunEndState == models.ConditionRunEndStatePending && triggeredByConditionRunCompletion {
		return false, errors.IllegalState.New("attempting to work on a conditionParticipant that belongs to a run in an illegal state")
		// State changes triggered by completion of the condition run take precedence over changes to individual condition participants.
		// If a condition participant is canceled/expires after a condition run is completed, the event is ignored.
	} else if conditionRun.ConditionRunEndState != models.ConditionRunEndStatePending && !triggeredByConditionRunCompletion {
		return false, nil
	}

	return true, nil
}

func (c *completerImpl) logAndStatCleanupConditionParticipantError(logFields logrus.Fields, err error) {
	logrus.WithFields(logFields).WithError(err).Error("error cleaning up condition participant")
	c.Statter.Inc(cleanupErrorStatName, 1, 1)
}

func shouldRejectWorkForConditionParticipant(conditionParticipant models.ConditionParticipant) bool {
	return conditionParticipant.ConditionParticipantProcessingState == models.ConditionParticipantProcessingStateCreating ||
		conditionParticipant.ConditionParticipantProcessingState == models.ConditionParticipantProcessingStateFailedToCreate ||
		conditionParticipant.ConditionParticipantProcessingState == models.ConditionParticipantProcessingStateSuccess
}
