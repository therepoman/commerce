package get

import (
	"context"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend/dynamo/conditionparticipant"
	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/models"
)

type Getter interface {
	GetPendingConditionParticipantsByConditionRunID(ctx context.Context, domain, conditionOwnerID, conditionRunID string) (*[]*models.ConditionParticipant, error)
	GetConditionParticipantsByOwnerIDAndConditionOwnerID(ctx context.Context, domain, conditionParticipantOwnerID, conditionOwnerID, cursor string, conditionParticipantEndState models.ConditionParticipantEndState) ([]*models.ConditionParticipant, string, error)
	GetConditionParticipant(ctx context.Context, conditionRunID, conditionParticipantID string) (models.ConditionParticipant, error)
	GetConditionParticipantByDomainAndOwnerAndID(ctx context.Context, domain, conditionParticipantID, conditionParticipantOwnerID string) (models.ConditionParticipant, error)
	GetConditionParticipantsByDomainAndOwnerID(ctx context.Context, domain, conditionParticipantOwnerID, cursor string) ([]*models.ConditionParticipant, string, error)
}

type getterImpl struct {
	ConditionParticipantDAO conditionparticipant.DAO `inject:""`
}

func NewGetter() Getter {
	return &getterImpl{}
}

func (c *getterImpl) GetPendingConditionParticipantsByConditionRunID(ctx context.Context, domain, conditionOwnerID, conditionRunID string) (*[]*models.ConditionParticipant, error) {
	return c.ConditionParticipantDAO.GetPendingConditionParticipantsByConditionRunID(ctx, domain, conditionOwnerID, conditionRunID)
}

func (c *getterImpl) GetConditionParticipant(ctx context.Context, conditionRunID, conditionParticipantID string) (models.ConditionParticipant, error) {
	conditionParticipant, err := c.ConditionParticipantDAO.GetConditionParticipant(ctx, conditionRunID, conditionParticipantID)

	if err != nil {
		return models.ConditionParticipant{}, err
	}

	if conditionParticipant == nil {
		message := "conditionParticipant does not exist"

		logrus.WithFields(logrus.Fields{
			"condition_participant_id": conditionParticipantID,
			"condition_run_id":         conditionRunID,
		}).Warn(message)

		return models.ConditionParticipant{}, errors.ConditionParticipantNotFound.New(message)
	}

	return *conditionParticipant, nil
}

func (c *getterImpl) GetConditionParticipantByDomainAndOwnerAndID(ctx context.Context, domain, conditionParticipantID, conditionParticipantOwnerID string) (models.ConditionParticipant, error) {
	conditionParticipant, err := c.ConditionParticipantDAO.GetConditionParticipantByDomainAndOwnerAndID(ctx, domain, conditionParticipantID, conditionParticipantOwnerID)

	if err != nil {
		return models.ConditionParticipant{}, err
	}

	if conditionParticipant == nil {
		message := "conditionParticipant does not exist"

		logrus.WithFields(logrus.Fields{
			"conditionParticipantId":      conditionParticipantID,
			"domain":                      domain,
			"conditionParticipantOwnerID": conditionParticipantOwnerID,
		}).Warn(message)

		return models.ConditionParticipant{}, errors.NotFound.New(message)
	}

	return *conditionParticipant, nil
}

func (c *getterImpl) GetConditionParticipantsByOwnerIDAndConditionOwnerID(ctx context.Context, domain, conditionParticipantOwnerID, conditionOwnerID, cursor string, conditionParticipantEndState models.ConditionParticipantEndState) ([]*models.ConditionParticipant, string, error) {
	return c.ConditionParticipantDAO.GetConditionParticipantsByOwnerIDAndConditionOwnerID(ctx, domain, conditionParticipantOwnerID, conditionOwnerID, cursor, conditionParticipantEndState)
}

func (c *getterImpl) GetConditionParticipantsByDomainAndOwnerID(ctx context.Context, domain, conditionParticipantOwnerID, cursor string) ([]*models.ConditionParticipant, string, error) {
	return c.ConditionParticipantDAO.GetConditionParticipantsByDomainAndOwnerID(ctx, domain, conditionParticipantOwnerID, cursor)
}
