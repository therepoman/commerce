package get

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pagle/backend/models"
	conditionparticipantdao_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/dynamo/conditionparticipant"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestConditionParticipantController_GetConditionParticipantsByOwnerIDAndConditionOwnerID(t *testing.T) {
	Convey("given a conditionParticipant controller", t, func() {
		conditionParticipantDAO := new(conditionparticipantdao_mock.DAO)

		controller := getterImpl{
			ConditionParticipantDAO: conditionParticipantDAO,
		}

		Convey("we should return the tuple returned from ConditionParticipantDAO.GetConditionParticipantsByOwnerIDAndConditionOwnerID", func() {
			mockConditionParticipantOwnerID := "123"
			mockConditionOwnerID := "456"
			mockCursor := "old-curosr"
			mockNewCursor := "new-cursor"
			mockDomain := "new-domain"
			var mockConditionParticipantEndState models.ConditionParticipantEndState
			mockConditionParticipants := []*models.ConditionParticipant{
				{ConditionParticipantID: "foo"},
			}

			conditionParticipantDAO.On("GetConditionParticipantsByOwnerIDAndConditionOwnerID", mock.Anything, mockDomain, mockConditionParticipantOwnerID, mockConditionOwnerID, mockCursor, mockConditionParticipantEndState).Return(mockConditionParticipants, mockNewCursor, nil)
			val, cursor, err := controller.GetConditionParticipantsByOwnerIDAndConditionOwnerID(context.Background(), mockDomain, mockConditionParticipantOwnerID, mockConditionOwnerID, mockCursor, mockConditionParticipantEndState)
			So(err, ShouldBeNil)
			So(cursor, ShouldEqual, mockNewCursor)
			So(val, ShouldResemble, mockConditionParticipants)
		})
	})
}

func TestConditionParticipantController_GetConditionParticipant(t *testing.T) {
	Convey("given a conditionParticipant controller", t, func() {
		conditionParticipantDAO := new(conditionparticipantdao_mock.DAO)

		controller := getterImpl{
			ConditionParticipantDAO: conditionParticipantDAO,
		}

		mockConditionRunID := "some-condition-run"
		mockConditionParticipantID := "condition-id"
		mockConditionParticipant := models.ConditionParticipant{
			ConditionParticipantID: mockConditionParticipantID,
		}

		Convey("when the call to ConditionParticipantDAO.GetConditionParticipant errors", func() {
			conditionParticipantDAO.On("GetConditionParticipant", mock.Anything, mockConditionRunID, mockConditionParticipantID).Return(nil, errors.New("ERROR"))

			Convey("we should return the error", func() {
				conditionParticipant, err := controller.GetConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID)

				So(err, ShouldNotBeNil)
				So(conditionParticipant, ShouldResemble, models.ConditionParticipant{})
			})
		})

		Convey("when the call to ConditionParticipantDAO.GetConditionParticipant returns a nil conditionParticipant", func() {
			conditionParticipantDAO.On("GetConditionParticipant", mock.Anything, mockConditionRunID, mockConditionParticipantID).Return(nil, nil)

			Convey("we should return an error", func() {
				conditionParticipant, err := controller.GetConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID)

				So(err, ShouldNotBeNil)
				So(conditionParticipant, ShouldResemble, models.ConditionParticipant{})
			})
		})

		Convey("when the call to ConditionParticipantDAO.GetConditionParticipant succeeds", func() {
			conditionParticipantDAO.On("GetConditionParticipant", mock.Anything, mockConditionRunID, mockConditionParticipantID).Return(&mockConditionParticipant, nil)

			Convey("we return the conditionParticipant", func() {
				conditionParticipant, err := controller.GetConditionParticipant(context.Background(), mockConditionRunID, mockConditionParticipantID)

				So(err, ShouldBeNil)
				So(conditionParticipant, ShouldNotBeNil)
				So(conditionParticipant.ConditionParticipantID, ShouldEqual, mockConditionParticipantID)
			})
		})
	})
}

func TestConditionParticipantController_GetConditionParticipantByDomainAndOwnerAndID(t *testing.T) {
	Convey("given a conditionParticipant controller", t, func() {
		conditionParticipantDAO := new(conditionparticipantdao_mock.DAO)

		controller := getterImpl{
			ConditionParticipantDAO: conditionParticipantDAO,
		}

		mockConditionParticipantID := "condition-id"
		mockDomain := "test-domain"
		mockConditionParticipantOwnerID := "participant-owner"
		mockConditionParticipant := models.ConditionParticipant{
			ConditionParticipantID: mockConditionParticipantID,
		}

		Convey("when the call to ConditionParticipantDAO.GetConditionParticipantByDomainAndOwnerAndID errors", func() {
			conditionParticipantDAO.On("GetConditionParticipantByDomainAndOwnerAndID", mock.Anything, mockDomain, mockConditionParticipantID, mockConditionParticipantOwnerID).Return(nil, errors.New("ERROR"))

			Convey("we should return the error", func() {
				conditionParticipant, err := controller.GetConditionParticipantByDomainAndOwnerAndID(context.Background(), mockDomain, mockConditionParticipantID, mockConditionParticipantOwnerID)

				So(err, ShouldNotBeNil)
				So(conditionParticipant, ShouldResemble, models.ConditionParticipant{})
			})
		})

		Convey("when the call to ConditionParticipantDAO.GetConditionParticipant returns a nil conditionParticipant", func() {
			conditionParticipantDAO.On("GetConditionParticipantByDomainAndOwnerAndID", mock.Anything, mockDomain, mockConditionParticipantID, mockConditionParticipantOwnerID).Return(nil, nil)

			Convey("we should return an error", func() {
				conditionParticipant, err := controller.GetConditionParticipantByDomainAndOwnerAndID(context.Background(), mockDomain, mockConditionParticipantID, mockConditionParticipantOwnerID)

				So(err, ShouldNotBeNil)
				So(conditionParticipant, ShouldResemble, models.ConditionParticipant{})
			})
		})

		Convey("when the call to ConditionParticipantDAO.GetConditionParticipant succeeds", func() {
			conditionParticipantDAO.On("GetConditionParticipantByDomainAndOwnerAndID", mock.Anything, mockDomain, mockConditionParticipantID, mockConditionParticipantOwnerID).Return(&mockConditionParticipant, nil)

			Convey("we return the conditionParticipant", func() {
				conditionParticipant, err := controller.GetConditionParticipantByDomainAndOwnerAndID(context.Background(), mockDomain, mockConditionParticipantID, mockConditionParticipantOwnerID)

				So(err, ShouldBeNil)
				So(conditionParticipant, ShouldNotBeNil)
				So(conditionParticipant.ConditionParticipantID, ShouldEqual, mockConditionParticipantID)
			})
		})
	})
}

func TestConditionParticipantController_GetConditionParticipantsByDomainAndOwnerID(t *testing.T) {
	Convey("given a conditionParticipant controller", t, func() {
		conditionParticipantDAO := new(conditionparticipantdao_mock.DAO)

		controller := getterImpl{
			ConditionParticipantDAO: conditionParticipantDAO,
		}

		Convey("we should return the tuple returned from ConditionParticipantDAO.GetConditionParticipantsByDomainAndOwnerID", func() {
			mockConditionParticipantOwnerID := "123"
			mockCursor := "old-curosr"
			mockNewCursor := "new-cursor"
			mockDomain := "new-domain"
			mockConditionParticipants := []*models.ConditionParticipant{
				{ConditionParticipantID: "foo"},
			}

			conditionParticipantDAO.On("GetConditionParticipantsByDomainAndOwnerID", mock.Anything, mockDomain, mockConditionParticipantOwnerID, mockCursor).Return(mockConditionParticipants, mockNewCursor, nil)
			val, cursor, err := controller.GetConditionParticipantsByDomainAndOwnerID(context.Background(), mockDomain, mockConditionParticipantOwnerID, mockCursor)
			So(err, ShouldBeNil)
			So(cursor, ShouldEqual, mockNewCursor)
			So(val, ShouldResemble, mockConditionParticipants)
		})
	})
}
