package redis

import (
	"time"

	"code.justin.tv/commerce/pagle/backend/hystrix/commands"
	"code.justin.tv/commerce/pagle/backend/stats"
	"code.justin.tv/commerce/pagle/config"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/go-redis/redis"
)

const (
	getLatencyStatName    = "redis_get_latency"
	setLatencyStatName    = "redis_set_latency"
	deleteLatencyStatName = "redis_del_latency"
)

type Client interface {
	Ping() error
	Get(key string) (string, error)
	Set(key string, value interface{}, expiration time.Duration) error
	Delete(key string) error
}

type client struct {
	redisClient redis.Cmdable
	Statter     stats.Statter `inject:""`
}

func NewClient(cfg *config.Config) Client {
	if cfg.UseRedisClusterMode {
		redisClient := redis.NewClusterClient(&redis.ClusterOptions{
			Addrs: []string{cfg.RedisEndpoint},
		})

		return &client{
			redisClient: redis.Cmdable(redisClient),
		}
	}

	redisClient := redis.NewClient(&redis.Options{
		Addr: cfg.RedisEndpoint,
	})

	return &client{
		redisClient: redis.Cmdable(redisClient),
	}
}

func (c *client) Ping() error {
	_, err := c.redisClient.Ping().Result()
	return err
}

func (c *client) Get(key string) (string, error) {
	var cachedValue string

	startTime := time.Now()
	defer func() {
		c.Statter.TimingDuration(getLatencyStatName, time.Since(startTime), 1)
	}()

	err := hystrix.Do(commands.RedisGetCommand, func() error {
		var err error

		cachedValue, err = c.redisClient.Get(key).Result()

		if err != nil && err != redis.Nil {
			return err
		}

		return nil
	}, nil)

	return cachedValue, err
}

func (c *client) Set(key string, value interface{}, expiration time.Duration) error {
	startTime := time.Now()
	defer func() {
		c.Statter.TimingDuration(setLatencyStatName, time.Since(startTime), 1)
	}()

	err := hystrix.Do(commands.RedisSetCommand, func() error {
		return c.redisClient.Set(key, value, expiration).Err()
	}, nil)

	return err
}

func (c *client) Delete(key string) error {
	startTime := time.Now()
	defer func() {
		c.Statter.TimingDuration(deleteLatencyStatName, time.Since(startTime), 1)
	}()

	err := hystrix.Do(commands.RedisDelCommand, func() error {
		return c.redisClient.Del(key).Err()
	}, nil)

	return err
}
