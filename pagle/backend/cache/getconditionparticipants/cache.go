package getconditionparticipants

import (
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	redis_client "code.justin.tv/commerce/pagle/backend/redis"
	pagle "code.justin.tv/commerce/pagle/rpc"
	"github.com/go-redis/redis"
)

const (
	getConditionParticipantsCacheKeyFormat = "getconditionparticipants.%s.%s.%s.%d.%s"
	getConditionParticipantsCacheTTL       = 15 * time.Second
)

type Cache interface {
	Get(req *pagle.GetConditionParticipantsReq) *pagle.GetConditionParticipantsResp
	Set(req *pagle.GetConditionParticipantsReq, res *pagle.GetConditionParticipantsResp) error
	Delete(req *pagle.GetConditionParticipantsReq) error
}

func NewCache() Cache {
	return &cache{}
}

type cache struct {
	RedisClient redis_client.Client `inject:""`
}

func (c *cache) Get(req *pagle.GetConditionParticipantsReq) *pagle.GetConditionParticipantsResp {
	logFields := log.Fields{
		"domain":                      req.Domain,
		"conditionParticipantOwnerID": req.ConditionParticipantOwnerId,
		"conditionOwnerID":            req.ConditionOwnerId,
		"endState":                    int32(req.ConditionParticipantEndState),
		"cursor":                      req.Cursor,
	}

	cacheValueStr, err := c.RedisClient.Get(getConditionParticipantsCacheKey(req))
	if err == redis.Nil {
		return nil
	} else if err != nil {
		// Actual redis failure, not a "key does not exist" error
		log.WithError(err).WithFields(logFields).Error("Error getting GetConditionParticipants response out of cache")
		return nil
	}

	if strings.Blank(cacheValueStr) {
		return nil
	}

	var getConditionParticipantsRes pagle.GetConditionParticipantsResp
	err = json.Unmarshal([]byte(cacheValueStr), &getConditionParticipantsRes)
	if err != nil {
		delErr := c.Delete(req)
		if delErr != nil {
			log.WithError(delErr).WithFields(logFields).Error("Error deleting GetConditionParticipants response from cache")
		}

		log.WithError(err).WithFields(logFields).Error("Error unmarshaling GetConditionParticipants response cache value")

		return nil
	}

	return &getConditionParticipantsRes
}

func (c *cache) Set(req *pagle.GetConditionParticipantsReq, res *pagle.GetConditionParticipantsResp) error {
	logFields := log.Fields{
		"domain":                      req.Domain,
		"conditionParticipantOwnerID": req.ConditionParticipantOwnerId,
		"conditionOwnerID":            req.ConditionOwnerId,
		"endState":                    int32(req.ConditionParticipantEndState),
		"cursor":                      req.Cursor,
	}

	getConditionParticipantsResJSON, err := json.Marshal(res)
	if err != nil {
		log.WithError(err).WithFields(logFields).Error("Error marshaling GetConditionParticipants response cache value")
		return err
	}

	err = c.RedisClient.Set(getConditionParticipantsCacheKey(req), string(getConditionParticipantsResJSON), getConditionParticipantsCacheTTL)
	if err != nil {
		log.WithError(err).WithFields(logFields).Error("Error setting GetConditionParticipants response in cache")
	}

	return err
}

func (c *cache) Delete(req *pagle.GetConditionParticipantsReq) error {
	err := c.RedisClient.Delete(getConditionParticipantsCacheKey(req))
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"domain":                      req.Domain,
			"conditionParticipantOwnerID": req.ConditionParticipantOwnerId,
			"conditionOwnerID":            req.ConditionOwnerId,
			"endState":                    int32(req.ConditionParticipantEndState),
			"cursor":                      req.Cursor,
		}).Error("Error deleting GetConditionParticipants response from cache")
	}

	return err
}

func getConditionParticipantsCacheKey(req *pagle.GetConditionParticipantsReq) string {
	return fmt.Sprintf(getConditionParticipantsCacheKeyFormat, req.Domain, req.ConditionParticipantOwnerId, req.ConditionOwnerId, int32(req.ConditionParticipantEndState), req.Cursor)
}
