package getconditions

import (
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	redis_client "code.justin.tv/commerce/pagle/backend/redis"
	pagle "code.justin.tv/commerce/pagle/rpc"
	"github.com/go-redis/redis"
)

const (
	getConditionsCacheKeyFormat = "getconditions.%s.%s.%d.%s"
	getConditionsCacheTTL       = 15 * time.Second
)

type Cache interface {
	Get(req *pagle.GetConditionsReq) *pagle.GetConditionsResp
	Set(req *pagle.GetConditionsReq, res *pagle.GetConditionsResp) error
	Delete(req *pagle.GetConditionsReq) error
}

func NewCache() Cache {
	return &cache{}
}

type cache struct {
	RedisClient redis_client.Client `inject:""`
}

func (c *cache) Get(req *pagle.GetConditionsReq) *pagle.GetConditionsResp {
	logFields := log.Fields{
		"domain":  req.Domain,
		"ownerID": req.OwnerId,
		"state":   int32(req.ConditionState),
		"cursor":  req.Cursor,
	}

	cacheValueStr, err := c.RedisClient.Get(getConditionsCacheKey(req))
	if err == redis.Nil {
		return nil
	} else if err != nil {
		// Actual redis failure, not a "key does not exist" error
		log.WithError(err).WithFields(logFields).Error("Error getting GetConditions response out of cache")
		return nil
	}

	if strings.Blank(cacheValueStr) {
		return nil
	}

	var getConditionsRes pagle.GetConditionsResp
	err = json.Unmarshal([]byte(cacheValueStr), &getConditionsRes)
	if err != nil {
		delErr := c.Delete(req)
		if delErr != nil {
			log.WithError(delErr).WithFields(logFields).Error("Error deleting GetConditions response from cache")
		}

		log.WithError(err).WithFields(logFields).Error("Error unmarshaling GetConditions response cache value")

		return nil
	}

	return &getConditionsRes
}

func (c *cache) Set(req *pagle.GetConditionsReq, res *pagle.GetConditionsResp) error {
	logFields := log.Fields{
		"domain":  req.Domain,
		"ownerID": req.OwnerId,
		"state":   int32(req.ConditionState),
		"cursor":  req.Cursor,
	}

	getConditionsResJSON, err := json.Marshal(res)
	if err != nil {
		log.WithError(err).WithFields(logFields).Error("Error marshaling GetConditions response cache value")
		return err
	}

	err = c.RedisClient.Set(getConditionsCacheKey(req), string(getConditionsResJSON), getConditionsCacheTTL)
	if err != nil {
		log.WithError(err).WithFields(logFields).Error("Error setting GetConditions response in cache")
	}

	return err
}

func (c *cache) Delete(req *pagle.GetConditionsReq) error {
	err := c.RedisClient.Delete(getConditionsCacheKey(req))
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"domain":  req.Domain,
			"ownerID": req.OwnerId,
			"state":   int32(req.ConditionState),
			"cursor":  req.Cursor,
		}).Error("Error deleting GetConditions response from cache")
	}

	return err
}

func getConditionsCacheKey(req *pagle.GetConditionsReq) string {
	return fmt.Sprintf(getConditionsCacheKeyFormat, req.Domain, req.OwnerId, int32(req.ConditionState), req.Cursor)
}
