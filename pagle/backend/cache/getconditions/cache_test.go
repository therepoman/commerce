package getconditions

import (
	"errors"
	"testing"

	redis_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/redis"
	pagle "code.justin.tv/commerce/pagle/rpc"
	"github.com/go-redis/redis"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestCache_Get(t *testing.T) {
	Convey("Given a GetConditions cache", t, func() {
		req := &pagle.GetConditionsReq{
			OwnerId:        "1234",
			ConditionState: pagle.ConditionState_ACTIVE,
			Cursor:         "",
			Domain:         "some-domain",
		}

		redisClient := new(redis_mock.Client)

		getConditionsCache := &cache{
			RedisClient: redisClient,
		}

		Convey("When the RedisClient fails", func() {
			redisClient.On("Get", mock.Anything).Return("", errors.New("test error"))

			res := getConditionsCache.Get(req)
			So(res, ShouldBeNil)
		})

		Convey("When the key is not found in Redis", func() {
			redisClient.On("Get", mock.Anything).Return("", redis.Nil)

			res := getConditionsCache.Get(req)
			So(res, ShouldBeNil)
		})

		Convey("When an empty string is returned from Redis", func() {
			redisClient.On("Get", mock.Anything).Return("", nil)

			res := getConditionsCache.Get(req)
			So(res, ShouldBeNil)
		})

		Convey("When a non-empty string is returned from Redis", func() {
			redisClient.On("Get", mock.Anything).Return("{\"conditions\":[{\"ownerId\":\"1234\"}]}", nil)

			res := getConditionsCache.Get(req)
			So(res, ShouldNotBeNil)
		})
	})
}
