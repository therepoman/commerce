package poller

import (
	"context"
	"math/rand"
	"time"

	"code.justin.tv/chat/golibs/graceful"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend/ecs"
	"code.justin.tv/commerce/pagle/config"
)

const (
	refreshDelayMaxRandomOffset = time.Second * 30
	refreshDelay                = time.Minute * 5
	fallbackInstanceCount       = 2
)

type Poller interface {
	Init() error
	RefreshAtInterval()
	GetInstanceCount() int
}

type pollerImpl struct {
	containerInstanceCount int
	ECSClient              ecs.Client     `inject:""`
	Config                 *config.Config `inject:""`
}

func NewPoller() Poller {
	return &pollerImpl{}
}

func (p *pollerImpl) Init() error {
	return p.refreshInstanceCount()
}

func (p *pollerImpl) RefreshAtInterval() {
	delayOffset := rand.Int63n(int64(refreshDelayMaxRandomOffset))
	timer := time.NewTicker(refreshDelay + time.Duration(delayOffset))

	for {
		select {
		case <-graceful.ShuttingDown():
			return
		case <-timer.C:
			err := p.refreshInstanceCount()
			if err != nil {
				log.WithError(err).Error("Error refreshing container instance count")
			}
		}
	}
}

func (p *pollerImpl) GetInstanceCount() int {
	if p.containerInstanceCount == 0 {
		// If the poller has not refreshed yet, make sure to return the default number of instances
		return fallbackInstanceCount
	}

	return p.containerInstanceCount
}

func (p *pollerImpl) refreshInstanceCount() error {
	containerInstances, err := p.ECSClient.ListContainerInstances(context.Background(), p.Config.PagleECSCluster)
	if err != nil {
		return err
	}

	p.containerInstanceCount = len(containerInstances)
	return nil
}
