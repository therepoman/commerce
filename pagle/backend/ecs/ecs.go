package ecs

import (
	"context"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ecs"
)

type Client interface {
	ListContainerInstances(ctx context.Context, cluster string) ([]*string, error)
}

type clientImpl struct {
	SDKClient *ecs.ECS
}

func NewClient(sess *session.Session, cfg *aws.Config) Client {
	return &clientImpl{
		SDKClient: ecs.New(sess, cfg),
	}
}

func (c *clientImpl) ListContainerInstances(ctx context.Context, cluster string) ([]*string, error) {
	out, err := c.SDKClient.ListContainerInstancesWithContext(ctx, &ecs.ListContainerInstancesInput{
		Cluster: &cluster,
	})

	if err != nil {
		return nil, err
	}

	return out.ContainerInstanceArns, nil
}
