package transaction

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/utils/dynamo/transactionerrors"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type daoImpl struct {
	*dynamodb.DynamoDB
}

type DAO interface {
	Transact(ctx context.Context, transactionItems []*dynamodb.TransactWriteItem) error
}

func NewDAO(sess *session.Session) DAO {
	db := transactionerrors.NewTxErrorAwareDynamoDBClient(sess)
	return &daoImpl{
		DynamoDB: db,
	}
}

func (dao *daoImpl) Transact(ctx context.Context, transactionItems []*dynamodb.TransactWriteItem) error {
	_, err := dao.TransactWriteItemsWithContext(ctx, &dynamodb.TransactWriteItemsInput{
		TransactItems: transactionItems,
	})

	if err != nil {
		txErr, isTxErr := err.(transactionerrors.TxRequestFailure)

		if isTxErr {
			return errors.InternalError.Wrap(err, fmt.Sprintf("%+v", txErr.CancellationReasons()))
		}
		return err
	}

	return nil
}
