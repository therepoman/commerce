package condition

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/hystrix/commands"
	"code.justin.tv/commerce/pagle/backend/models"
	dynamoutils "code.justin.tv/commerce/pagle/backend/utils/dynamo"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/gofrs/uuid"
	"github.com/joomcode/errorx"
)

const (
	tableNamePrefix                     = "conditions"
	domainOwnerIDStateIndexName         = "domain-owner_id-condition_state-index"
	ownerIdIndex                        = "owner_id-condition_id-index"
	extensionInstallationChannelIdIndex = "extension_installation_channel_id-condition_id-index"
	getConditionsQueryLimit             = 100
)

type daoImpl struct {
	*dynamodb.DynamoDB
	tableName string
}

type DAO interface {
	GetCondition(ctx context.Context, domain, ownerID, conditionID string) (*models.Condition, error)
	BatchGetCondition(ctx context.Context, batchGets []models.BatchGetConditionParams) ([]*models.Condition, error)
	CreateConditionPutTransaction(
		domain, ownerID, name, description string,
		disableWhenSatisfied, defineEffectSettingsWhenSatisfied bool,
		timeoutAt *time.Time,
		effectSettings []models.EffectSettings,
		supportedEffects []models.EffectType,
		extension *models.Extension) (models.Condition, *dynamodb.TransactWriteItem, error)
	SetConditionRunID(ctx context.Context, domain, ownerID, conditionID, runID string) (*models.Condition, error)
	SetConditionState(ctx context.Context, domain, ownerID, conditionID string, conditionState models.ConditionState) (*models.Condition, error)
	GetConditionsByOwnerID(ctx context.Context, domain, ownerID, cursor string, conditionState models.ConditionState) ([]*models.Condition, string, error)
	QueryConditionsByExtensionInstallationChannelId(ctx context.Context, extensionInstallationChannelId, cursor string) ([]*models.Condition, string, error)
	QueryConditionsByOwnerId(ctx context.Context, ownerId, cursor string) ([]*models.Condition, string, error)
	DeleteCondition(ctx context.Context, domain, ownerID, conditionID string) error
}

func NewDAO(sess *session.Session, suffix string) DAO {
	db := dynamodb.New(sess)
	tableName := fmt.Sprintf("%s-%s", tableNamePrefix, suffix)
	return &daoImpl{
		DynamoDB:  db,
		tableName: tableName,
	}
}

func (dao *daoImpl) GetCondition(ctx context.Context, domain, ownerID, conditionID string) (*models.Condition, error) {
	var cond models.Condition
	var output *dynamodb.GetItemOutput

	err := hystrix.Do(commands.DynamoGetConditionCommand, func() error {
		var err error

		output, err = dao.GetItemWithContext(ctx, &dynamodb.GetItemInput{
			Key:            makeKey(domain, ownerID, conditionID),
			TableName:      aws.String(dao.tableName),
			ConsistentRead: aws.Bool(true),
		})

		return err
	}, nil)

	if err != nil {
		return nil, errorx.Decorate(err, "error getting condition")
	}

	if len(output.Item) == 0 {
		return nil, nil
	}

	err = dynamodbattribute.UnmarshalMap(output.Item, &cond)

	if err != nil {
		return nil, errorx.Decorate(err, "error marshaling item returned from GetCondition")
	}

	return &cond, err
}

func (dao *daoImpl) BatchGetCondition(ctx context.Context, batchGets []models.BatchGetConditionParams) ([]*models.Condition, error) {
	if len(batchGets) > models.MaxBatchGetItemCount {
		return nil, errors.IllegalArgument.New(fmt.Sprintf("exceed max BatchGet count of %d", models.MaxBatchGetItemCount))
	}

	var conditions []*models.Condition

	var keys []map[string]*dynamodb.AttributeValue

	for _, req := range batchGets {
		keys = append(keys, makeKey(req.Domain, req.OwnerID, req.ConditionID))
	}

	var output *dynamodb.BatchGetItemOutput

	err := hystrix.Do(commands.DynamoBatchGetConditionCommand, func() error {
		var err error
		output, err = dao.BatchGetItemWithContext(ctx, &dynamodb.BatchGetItemInput{
			RequestItems: map[string]*dynamodb.KeysAndAttributes{
				dao.tableName: {
					Keys: keys,
				},
			},
		})

		return err
	}, nil)

	if err != nil {
		return conditions, errorx.Decorate(err, "error calling BatchGetItemWithContext from BatchGetCondition")
	}

	if output == nil ||
		output.Responses == nil ||
		len(output.Responses) == 0 ||
		output.Responses[dao.tableName] == nil ||
		len(output.Responses[dao.tableName]) == 0 {
		return conditions, nil
	}

	err = dynamodbattribute.UnmarshalListOfMaps(output.Responses[dao.tableName], &conditions)

	if err != nil {
		return conditions, errorx.Decorate(err, "error marshaling items returned from BatchGetCondition")
	}

	return conditions, nil
}

func (dao *daoImpl) GetConditionsByOwnerID(ctx context.Context, domain, ownerID, cursor string, conditionState models.ConditionState) ([]*models.Condition, string, error) {
	var conditions []*models.Condition

	domainOwner := makeDomainOwnerIDKey(domain, ownerID)

	startFrom, err := dynamoutils.DecodePagingKey(cursor)

	if err != nil {
		return conditions, "", errorx.Decorate(err, "error decoding paging key in GetConditionsByOwnerID")
	}

	keyCondition := expression.KeyAnd(
		expression.Key("domain-owner_id").Equal(expression.Value(domainOwner)),
		expression.Key("condition_state").Equal(expression.Value(conditionState)),
	)

	expr, err := expression.NewBuilder().WithKeyCondition(keyCondition).Build()

	if err != nil {
		return conditions, "", errorx.Decorate(err, "failed to build GetConditionsByOwnerID expression")
	}

	var output *dynamodb.QueryOutput

	err = hystrix.Do(commands.DynamoGetConditionsByOwnerCommand, func() error {
		var err error

		output, err = dao.QueryWithContext(ctx, &dynamodb.QueryInput{
			TableName:                 aws.String(dao.tableName),
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
			KeyConditionExpression:    expr.KeyCondition(),
			ExclusiveStartKey:         startFrom,
			Limit:                     aws.Int64(getConditionsQueryLimit),
			IndexName:                 aws.String(domainOwnerIDStateIndexName),
		})

		return err
	}, nil)

	if err != nil {
		return conditions, "", errorx.Decorate(err, "failed to GetConditionsByOwnerID from dynamo")
	}

	if output == nil || len(output.Items) == 0 {
		return conditions, "", nil
	}

	err = dynamodbattribute.UnmarshalListOfMaps(output.Items, &conditions)

	if err != nil {
		return nil, "", errorx.Decorate(err, "error marshaling items returned from GetConditionsByOwnerID")
	}

	newCursor, err := dynamoutils.EncodePagingKey(output.LastEvaluatedKey)

	if err != nil {
		return nil, "", errorx.Decorate(err, "error encoding paging key in GetConditionsByOwnerID")
	}

	return conditions, newCursor, nil
}

func (dao *daoImpl) QueryConditionsByExtensionInstallationChannelId(ctx context.Context, extensionInstallationChannelId, cursor string) ([]*models.Condition, string, error) {
	var conditions []*models.Condition

	startFrom, err := dynamoutils.DecodePagingKey(cursor)

	if err != nil {
		return conditions, "", errorx.Decorate(err, "error decoding paging key in QueryConditionsByExtensionInstallationChannelId")
	}

	keyCondition := expression.Key("extension_installation_channel_id").Equal(expression.Value(extensionInstallationChannelId))

	expr, err := expression.NewBuilder().WithKeyCondition(keyCondition).Build()

	if err != nil {
		return conditions, "", errorx.Decorate(err, "failed to build QueryConditionsByExtensionInstallationChannelId expression")
	}

	var output *dynamodb.QueryOutput

	err = hystrix.Do(commands.DynamoQueryConditionsByExtensionOwnerIdCommand, func() error {
		var err error

		output, err = dao.QueryWithContext(ctx, &dynamodb.QueryInput{
			TableName:                 aws.String(dao.tableName),
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
			KeyConditionExpression:    expr.KeyCondition(),
			ExclusiveStartKey:         startFrom,
			Limit:                     aws.Int64(getConditionsQueryLimit),
			IndexName:                 aws.String(extensionInstallationChannelIdIndex),
		})

		return err
	}, nil)

	if err != nil {
		return conditions, "", errorx.Decorate(err, "failed to QueryConditionsByExtensionInstallationChannelId from dynamo")
	}

	if output == nil || len(output.Items) == 0 {
		return conditions, "", nil
	}

	err = dynamodbattribute.UnmarshalListOfMaps(output.Items, &conditions)

	if err != nil {
		return nil, "", errorx.Decorate(err, "error marshaling items returned from QueryConditionsByExtensionInstallationChannelId")
	}

	newCursor, err := dynamoutils.EncodePagingKey(output.LastEvaluatedKey)

	if err != nil {
		return nil, "", errorx.Decorate(err, "error encoding paging key in QueryConditionsByExtensionInstallationChannelId")
	}

	return conditions, newCursor, nil
}

func (dao *daoImpl) QueryConditionsByOwnerId(ctx context.Context, ownerId, cursor string) ([]*models.Condition, string, error) {
	var conditions []*models.Condition

	startFrom, err := dynamoutils.DecodePagingKey(cursor)

	if err != nil {
		return conditions, "", errorx.Decorate(err, "error decoding paging key in QueryConditionsByOwnerId")
	}

	keyCondition := expression.Key("owner_id").Equal(expression.Value(ownerId))

	expr, err := expression.NewBuilder().WithKeyCondition(keyCondition).Build()

	if err != nil {
		return conditions, "", errorx.Decorate(err, "failed to build QueryConditionsByOwnerId expression")
	}

	var output *dynamodb.QueryOutput

	err = hystrix.Do(commands.DynamoQueryConditionsByOwnerIdCommand, func() error {
		var err error

		output, err = dao.QueryWithContext(ctx, &dynamodb.QueryInput{
			TableName:                 aws.String(dao.tableName),
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
			KeyConditionExpression:    expr.KeyCondition(),
			ExclusiveStartKey:         startFrom,
			Limit:                     aws.Int64(getConditionsQueryLimit),
			IndexName:                 aws.String(ownerIdIndex),
		})

		return err
	}, nil)

	if err != nil {
		return conditions, "", errorx.Decorate(err, "failed to QueryConditionsByOwnerId from dynamo")
	}

	if output == nil || len(output.Items) == 0 {
		return conditions, "", nil
	}

	err = dynamodbattribute.UnmarshalListOfMaps(output.Items, &conditions)

	if err != nil {
		return nil, "", errorx.Decorate(err, "error marshaling items returned from QueryConditionsByOwnerId")
	}

	newCursor, err := dynamoutils.EncodePagingKey(output.LastEvaluatedKey)

	if err != nil {
		return nil, "", errorx.Decorate(err, "error encoding paging key in QueryConditionsByOwnerId")
	}

	return conditions, newCursor, nil
}

func (dao *daoImpl) CreateConditionPutTransaction(
	domain, ownerID, name, description string,
	disableWhenSatisfied, defineEffectSettingsWhenSatisfied bool,
	timeoutAt *time.Time,
	effectSettings []models.EffectSettings,
	supportedEffects []models.EffectType,
	extension *models.Extension) (models.Condition, *dynamodb.TransactWriteItem, error) {

	now := time.Now()

	id, err := uuid.NewV4()

	if err != nil {
		return models.Condition{}, nil, errorx.Decorate(err, "failed to create uuid for condition")
	}

	var extensionInstallationChannelId *string

	if extension != nil {
		extensionInstallationChannelId = &extension.InstallationChannelID
	} else {
		none := "none"
		extensionInstallationChannelId = &none
	}

	cond := models.Condition{
		ConditionID:                       id.String(),
		OwnerID:                           ownerID,
		Domain:                            domain,
		DomainOwnerID:                     makeDomainOwnerIDKey(domain, ownerID),
		Name:                              name,
		CreatedAt:                         now,
		ConditionState:                    models.ConditionActive,
		EffectSettings:                    effectSettings,
		SupportedEffects:                  supportedEffects,
		TimeoutAt:                         timeoutAt,
		DisableWhenSatisfied:              disableWhenSatisfied,
		DefineEffectSettingsWhenSatisfied: defineEffectSettingsWhenSatisfied,
		Extension:                         extension,
		// pull the extension channel id out of the json blob above because we want to key on this channel id
		ExtensionInstallationChannelId: extensionInstallationChannelId,
	}

	dynamoCondition, err := dynamodbattribute.MarshalMap(&cond)

	if err != nil {
		return models.Condition{}, nil, errorx.Decorate(err, "error marshaling condition")
	}

	return cond, &dynamodb.TransactWriteItem{
		Put: &dynamodb.Put{
			Item:      dynamoCondition,
			TableName: aws.String(dao.tableName),
		},
	}, nil
}

func (dao *daoImpl) SetConditionRunID(ctx context.Context, domain, ownerID, conditionID, runID string) (*models.Condition, error) {
	var cond models.Condition

	updateExpr := createUpdateExpression().Set(expression.Name("condition_run_id"), expression.Value(runID))

	expr, err := expression.NewBuilder().WithUpdate(updateExpr).WithCondition(createActiveConditionExpression()).Build()

	if err != nil {
		return nil, errorx.Decorate(err, "failed to build SetConditionRunID expression")
	}

	var output *dynamodb.UpdateItemOutput

	err = hystrix.Do(commands.DynamoSetConditionRunIDCommand, func() error {
		var err error

		output, err = dao.UpdateItemWithContext(ctx, &dynamodb.UpdateItemInput{
			TableName: aws.String(dao.tableName),
			Key: map[string]*dynamodb.AttributeValue{
				"domain-owner_id": {
					S: aws.String(makeDomainOwnerIDKey(domain, ownerID)),
				},
				"condition_id": {
					S: aws.String(conditionID),
				},
			},
			UpdateExpression:          expr.Update(),
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
			ConditionExpression:       expr.Condition(),
			ReturnValues:              aws.String(dynamodb.ReturnValueAllNew),
		})

		return err
	}, nil)

	if err != nil {
		return nil, errorx.Decorate(err, "failed to update item in SetConditionRunID")
	}

	err = dynamodbattribute.UnmarshalMap(output.Attributes, &cond)

	if err != nil {
		return nil, errorx.Decorate(err, "error marshaling item returned from SetConditionRunID")
	}

	return &cond, nil
}

func (dao *daoImpl) SetConditionState(ctx context.Context, domain, ownerID, conditionID string, conditionState models.ConditionState) (*models.Condition, error) {
	var cond models.Condition

	updateExpr := createUpdateExpression().Set(expression.Name("condition_state"), expression.Value(conditionState))

	expr, err := expression.NewBuilder().WithUpdate(updateExpr).WithCondition(createActiveConditionExpression()).Build()

	if err != nil {
		return nil, errorx.Decorate(err, "failed to build SetConditionState expression")
	}

	var output *dynamodb.UpdateItemOutput

	err = hystrix.Do(commands.DynamoSetConditionStateCommand, func() error {
		var err error

		output, err = dao.UpdateItemWithContext(ctx, &dynamodb.UpdateItemInput{
			TableName: aws.String(dao.tableName),
			Key: map[string]*dynamodb.AttributeValue{
				"domain-owner_id": {
					S: aws.String(makeDomainOwnerIDKey(domain, ownerID)),
				},
				"condition_id": {
					S: aws.String(conditionID),
				},
			},
			UpdateExpression:          expr.Update(),
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
			ConditionExpression:       expr.Condition(),
			ReturnValues:              aws.String(dynamodb.ReturnValueAllNew),
		})

		return err
	}, nil)

	if err != nil {
		return nil, errorx.Decorate(err, "failed to update item in SetConditionState")
	}

	err = dynamodbattribute.UnmarshalMap(output.Attributes, &cond)

	if err != nil {
		return nil, errorx.Decorate(err, "error marshaling item returned from SetConditionState")
	}

	return &cond, nil
}

func (dao *daoImpl) DeleteCondition(ctx context.Context, domain, ownerID, conditionID string) error {
	err := hystrix.Do(commands.DynamoDeleteConditionCommand, func() error {
		_, innerErr := dao.DeleteItemWithContext(ctx, &dynamodb.DeleteItemInput{
			Key:       makeKey(domain, ownerID, conditionID),
			TableName: aws.String(dao.tableName),
		})

		return innerErr
	}, nil)

	if err != nil {
		return errorx.Decorate(err, "error getting condition participant")
	}

	return nil
}

func createUpdateExpression() expression.UpdateBuilder {
	return expression.Set(expression.Name("updated_at"), expression.Value(time.Now()))
}

func createActiveConditionExpression() expression.ConditionBuilder {
	return expression.Name("condition_state").Equal(expression.Value(models.ConditionActive))
}

func makeDomainOwnerIDKey(domain, ownerID string) string {
	return fmt.Sprintf("%s-%s", domain, ownerID)
}

func makeKey(domain, ownerID, conditionID string) map[string]*dynamodb.AttributeValue {
	return map[string]*dynamodb.AttributeValue{
		"domain-owner_id": {
			S: aws.String(makeDomainOwnerIDKey(domain, ownerID)),
		},
		"condition_id": {
			S: aws.String(conditionID),
		},
	}
}
