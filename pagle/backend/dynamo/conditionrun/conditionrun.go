package conditionrun

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/pagle/backend/hystrix/commands"
	"code.justin.tv/commerce/pagle/backend/models"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/gofrs/uuid"
	"github.com/joomcode/errorx"
)

const (
	tableNamePrefix = "condition-runs"
)

type daoImpl struct {
	*dynamodb.DynamoDB
	tableName string
}

type DAO interface {
	GetConditionRun(ctx context.Context, conditionRunID string) (*models.ConditionRun, error)
	CreateConditionRun(ctx context.Context, conditionID string) (*models.ConditionRun, error)
	CompleteConditionRun(ctx context.Context, conditionRunID string, conditionRunEndState models.ConditionRunEndState, effectSettings []models.EffectSettings) error
}

func NewDAO(sess *session.Session, suffix string) DAO {
	db := dynamodb.New(sess)
	tableName := fmt.Sprintf("%s-%s", tableNamePrefix, suffix)
	return &daoImpl{
		DynamoDB:  db,
		tableName: tableName,
	}
}

func (dao *daoImpl) GetConditionRun(ctx context.Context, conditionRunID string) (*models.ConditionRun, error) {
	var run models.ConditionRun
	var output *dynamodb.GetItemOutput

	err := hystrix.Do(commands.DynamoGetConditionRunCommand, func() error {
		var err error

		output, err = dao.GetItemWithContext(ctx, &dynamodb.GetItemInput{
			Key: map[string]*dynamodb.AttributeValue{
				"condition_run_id": {
					S: aws.String(conditionRunID),
				},
			},
			TableName: aws.String(dao.tableName),
		})

		return err
	}, nil)

	if err != nil {
		return nil, errorx.Decorate(err, "error getting condition run")
	}

	if len(output.Item) == 0 {
		return nil, nil
	}

	err = dynamodbattribute.UnmarshalMap(output.Item, &run)

	if err != nil {
		return nil, errorx.Decorate(err, "error marshaling item returned from GetConditionRun")
	}

	return &run, err
}

func (dao *daoImpl) CreateConditionRun(ctx context.Context, conditionID string) (*models.ConditionRun, error) {
	now := time.Now()

	id, err := uuid.NewV4()

	if err != nil {
		return nil, errorx.Decorate(err, "failed to create uuid for condition run")
	}

	run := &models.ConditionRun{
		ConditionRunID:       id.String(),
		ConditionID:          conditionID,
		CreatedAt:            now,
		ConditionRunEndState: models.ConditionRunEndStatePending,
		TTL:                  now.AddDate(0, 3, 0),
	}

	dynamoConditionRun, err := dynamodbattribute.MarshalMap(run)

	if err != nil {
		return nil, errorx.Decorate(err, "error marshaling condition run")
	}

	err = hystrix.Do(commands.DynamoCreateConditionRunCommand, func() error {
		_, err = dao.PutItemWithContext(ctx, &dynamodb.PutItemInput{
			TableName: aws.String(dao.tableName),
			Item:      dynamoConditionRun,
		})

		return err
	}, nil)

	if err != nil {
		return nil, errorx.Decorate(err, "failed to create condition run")
	}

	return run, nil
}

func (dao *daoImpl) CompleteConditionRun(ctx context.Context, conditionRunID string, conditionRunEndState models.ConditionRunEndState, effectSettings []models.EffectSettings) error {
	now := time.Now()

	updateExpr := expression.
		Set(expression.Name("completed_at"), expression.Value(now)).
		Set(expression.Name("effect_settings"), expression.Value(effectSettings)).
		Set(expression.Name("is_open"), expression.Value(false)).
		Set(expression.Name("condition_run_end_state"), expression.Value(conditionRunEndState)).
		Set(expression.Name("ttl"), expression.Value(now.AddDate(0, 3, 0)))

	expr, err := expression.NewBuilder().WithUpdate(updateExpr).Build()

	if err != nil {
		return errorx.Decorate(err, "failed to build CompleteConditionRun expression")
	}

	err = hystrix.Do(commands.DynamoCompleteConditionRunCommand, func() error {
		_, err = dao.UpdateItemWithContext(ctx, &dynamodb.UpdateItemInput{
			TableName: aws.String(dao.tableName),
			Key: map[string]*dynamodb.AttributeValue{
				"condition_run_id": {
					S: aws.String(conditionRunID),
				},
			},
			UpdateExpression:          expr.Update(),
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
		})

		return err
	}, nil)

	if err != nil {
		return errorx.Decorate(err, "failed to update item in CompleteConditionRun")
	}

	return nil
}
