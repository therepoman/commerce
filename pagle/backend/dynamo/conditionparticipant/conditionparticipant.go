package conditionparticipant

import (
	"context"
	"errors"
	"fmt"
	"time"

	"code.justin.tv/commerce/pagle/backend/hystrix/commands"
	"code.justin.tv/commerce/pagle/backend/models"
	dynamoutils "code.justin.tv/commerce/pagle/backend/utils/dynamo"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/gofrs/uuid"
	"github.com/joomcode/errorx"
)

const (
	tableNamePrefix                                   = "condition-participants"
	domainOwnerIDConditionParticipantIDIndexName      = "domain-owner_id-condition_participant_id-index"
	domainConditionOwnerIDParticipantOwnerIDIndexName = "domain-condition_owner_id-condition_participant_owner_id-condition_participant_end_state-index"

	ownerIDIndexName          = "owner_id-condition_participant_id-index"
	conditionOwnerIDIndexName = "condition_owner_id-condition_participant_id-index"

	getConditionParticipantsQueryLimit                   = 100
	getConditionParticipantsByDomainAndOwnerIDQueryLimit = 100
	getPendingConditionParticipantsQueryLimit            = 250
)

type daoImpl struct {
	*dynamodb.DynamoDB
	tableName string
}

type DAO interface {
	GetConditionParticipant(ctx context.Context, conditionRunID, conditionParticipantID string) (*models.ConditionParticipant, error)
	GetConditionParticipantByDomainAndOwnerAndID(ctx context.Context, domain, conditionParticipantID, conditionParticipantOwnerID string) (*models.ConditionParticipant, error)
	GetPendingConditionParticipantsByConditionRunID(ctx context.Context, domain, conditionOwnerID, conditionRunID string) (*[]*models.ConditionParticipant, error)
	CreateConditionParticipant(ctx context.Context, domain, conditionID, conditionRunID, ownerID, conditionOwnerID string, ttlSeconds int32, effect models.Effect) (*models.ConditionParticipant, error)
	UpdateConditionParticipant(ctx context.Context, conditionParticipant *models.ConditionParticipant) (*models.ConditionParticipant, error)
	GetConditionParticipantsByOwnerIDAndConditionOwnerID(ctx context.Context, domain, conditionParticipantOwnerID, conditionOwnerID, cursor string, conditionParticipantEndState models.ConditionParticipantEndState) ([]*models.ConditionParticipant, string, error)
	GetConditionParticipantsByDomainAndOwnerID(ctx context.Context, domain, conditionParticipantOwnerID, cursor string) ([]*models.ConditionParticipant, string, error)
	GetConditionParticipantsByOwnerID(ctx context.Context, conditionOwnerID, cursor string) ([]*models.ConditionParticipant, string, error)
	GetConditionParticipantsByConditionOwnerID(ctx context.Context, conditionOwnerID, cursor string) ([]*models.ConditionParticipant, string, error)
	DeleteConditionParticipant(ctx context.Context, conditionRunID, conditionParticipantID string) error
}

func NewDAO(sess *session.Session, suffix string) DAO {
	db := dynamodb.New(sess)
	tableName := fmt.Sprintf("%s-%s", tableNamePrefix, suffix)
	return &daoImpl{
		DynamoDB:  db,
		tableName: tableName,
	}
}

func (dao *daoImpl) GetConditionParticipant(ctx context.Context, conditionRunID, conditionParticipantID string) (*models.ConditionParticipant, error) {
	var cond models.ConditionParticipant
	var output *dynamodb.GetItemOutput

	err := hystrix.Do(commands.DynamoGetConditionParticipantCommand, func() error {
		var innerErr error

		output, innerErr = dao.GetItemWithContext(ctx, &dynamodb.GetItemInput{
			Key: map[string]*dynamodb.AttributeValue{
				"condition_run_id": {
					S: aws.String(conditionRunID),
				},
				"condition_participant_id": {
					S: aws.String(conditionParticipantID),
				},
			},
			TableName:      aws.String(dao.tableName),
			ConsistentRead: aws.Bool(true),
		})

		return innerErr
	}, nil)

	if err != nil {
		return nil, errorx.Decorate(err, "error getting condition participant")
	}

	if len(output.Item) == 0 {
		return nil, nil
	}

	err = dynamodbattribute.UnmarshalMap(output.Item, &cond)

	if err != nil {
		return nil, errorx.Decorate(err, "error marshaling item returned from GetConditionParticipant")
	}

	return &cond, err
}

func (dao *daoImpl) GetConditionParticipantByDomainAndOwnerAndID(ctx context.Context, domain, conditionParticipantID, conditionParticipantOwnerID string) (*models.ConditionParticipant, error) {
	var cond models.ConditionParticipant

	keyCondition := expression.KeyAnd(
		expression.Key("domain-owner_id").Equal(expression.Value(makeDomainOwnerIDKey(domain, conditionParticipantOwnerID))),
		expression.Key("condition_participant_id").Equal(expression.Value(conditionParticipantID)),
	)

	expr, err := expression.NewBuilder().WithKeyCondition(keyCondition).Build()

	if err != nil {
		return nil, errorx.Decorate(err, "failed to build GetConditionParticipantByDomainAndOwnerAndID expression")
	}

	var output *dynamodb.QueryOutput

	err = hystrix.Do(commands.DynamoGetConditionParticipantByDomainAndOwnerAndIDCommand, func() error {
		var err error

		output, err = dao.QueryWithContext(ctx, &dynamodb.QueryInput{
			TableName:                 aws.String(dao.tableName),
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
			KeyConditionExpression:    expr.KeyCondition(),
			Limit:                     aws.Int64(1),
			IndexName:                 aws.String(domainOwnerIDConditionParticipantIDIndexName),
		})

		return err
	}, nil)

	if err != nil {
		return nil, errorx.Decorate(err, "failed to GetConditionParticipantByDomainAndOwnerAndID from dynamo")
	}

	if output == nil || len(output.Items) == 0 {
		return nil, nil
	}

	err = dynamodbattribute.UnmarshalMap(output.Items[0], &cond)

	if err != nil {
		return nil, errorx.Decorate(err, "error marshaling item returned from GetConditionParticipantByDomainAndOwnerAndID")
	}

	return &cond, err
}

func (dao *daoImpl) GetPendingConditionParticipantsByConditionRunID(ctx context.Context, domain, conditionOwnerID, conditionRunID string) (*[]*models.ConditionParticipant, error) {
	var conditionParticipants []*models.ConditionParticipant

	keyCondition := expression.Key("condition_run_id").Equal(expression.Value(conditionRunID))

	stateFilter := expression.Name("condition_participant_processing_state").Equal(expression.Value(string(models.ConditionParticipantProcessingStatePending)))

	expr, err := expression.NewBuilder().WithKeyCondition(keyCondition).WithFilter(stateFilter).Build()

	if err != nil {
		return nil, errorx.Decorate(err, "failed to build GetPendingConditionParticipantsByConditionRunID expression")
	}

	items, err := dynamoutils.QueryAll(ctx, commands.DynamoGetPendingConditionParticipantsByRunCommand, dao.QueryWithContext, dynamodb.QueryInput{
		TableName:                 aws.String(dao.tableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		KeyConditionExpression:    expr.KeyCondition(),
		FilterExpression:          expr.Filter(),
		ConsistentRead:            aws.Bool(true),
		// For very large conditions, an unbound QueryAll could take 40s+. We prefer many smaller (but fast) queries, rather than
		// trying to set a reasonable timeout for an unbound one.
		Limit: aws.Int64(getPendingConditionParticipantsQueryLimit),
	})

	if err != nil {
		return nil, errorx.Decorate(err, "failed to GetPendingConditionParticipantsByConditionRunID from dynamo")
	}

	if len(items) == 0 {
		return nil, nil
	}

	err = dynamodbattribute.UnmarshalListOfMaps(items, &conditionParticipants)

	if err != nil {
		return nil, errorx.Decorate(err, "error marshaling items returned from GetPendingConditionParticipantsByConditionRunID")
	}

	return &conditionParticipants, nil
}

func (dao *daoImpl) CreateConditionParticipant(ctx context.Context, domain, conditionID, conditionRunID, ownerID, conditionOwnerID string, ttlSeconds int32, effect models.Effect) (*models.ConditionParticipant, error) {
	now := time.Now()
	id, err := uuid.NewV4()

	if err != nil {
		return nil, errorx.Decorate(err, "failed to create dynamo uuid for ConditionParticipant")
	}

	conditionParticipant := &models.ConditionParticipant{
		ConditionParticipantID:              id.String(),
		ConditionID:                         conditionID,
		ConditionRunID:                      conditionRunID,
		OwnerID:                             ownerID,
		Domain:                              domain,
		DomainOwnerID:                       makeDomainOwnerIDKey(domain, ownerID),
		ConditionOwnerID:                    conditionOwnerID,
		ConditionParticipantProcessingState: models.ConditionParticipantProcessingStateCreating,
		ConditionParticipantEndState:        models.ConditionParticipantEndStatePendingCompletion,
		CreatedAt:                           now,
		UpdatedAt:                           now,
		TTLSeconds:                          ttlSeconds,
		Effect:                              effect,

		DomainConditionOwnerIDConditionParticipantOwnerID: makeDomainConditionOwnerIDConditionParticipantOwnerIDKey(domain, conditionOwnerID, ownerID),
	}

	dynamoConditionParticipant, err := dynamodbattribute.MarshalMap(conditionParticipant)

	if err != nil {
		return nil, errorx.Decorate(err, "error marshaling ConditionParticipant")
	}

	err = hystrix.Do(commands.DynamoCreateConditionParticipantCommand, func() error {
		_, err := dao.PutItemWithContext(ctx, &dynamodb.PutItemInput{
			Item:      dynamoConditionParticipant,
			TableName: aws.String(dao.tableName),
		})

		return err
	}, nil)

	if err != nil {
		return nil, errorx.Decorate(err, "failed to CreateConditionParticipant in dynamo")
	}

	return conditionParticipant, nil
}

func (dao *daoImpl) UpdateConditionParticipant(ctx context.Context, conditionParticipant *models.ConditionParticipant) (*models.ConditionParticipant, error) {
	if conditionParticipant == nil {
		return nil, errors.New("UpdateConditionParticipant received a nil conditionParticipant")
	}

	conditionParticipant.UpdatedAt = time.Now()

	dynamoConditionParticipant, err := dynamodbattribute.MarshalMap(conditionParticipant)

	if err != nil {
		return nil, errorx.Decorate(err, "error marshaling ConditionParticipant")
	}

	err = hystrix.Do(commands.DynamoUpdateConditionParticipantCommand, func() error {
		_, err := dao.PutItemWithContext(ctx, &dynamodb.PutItemInput{
			Item:      dynamoConditionParticipant,
			TableName: aws.String(dao.tableName),
		})

		return err
	}, nil)

	if err != nil {
		return conditionParticipant, errorx.Decorate(err, "failed to UpdateConditionParticipant in dynamo")
	}

	return conditionParticipant, nil
}

func (dao *daoImpl) GetConditionParticipantsByOwnerIDAndConditionOwnerID(ctx context.Context, domain, conditionParticipantOwnerID, conditionOwnerID, cursor string, conditionParticipantEndState models.ConditionParticipantEndState) ([]*models.ConditionParticipant, string, error) {
	var conditionParticipants []*models.ConditionParticipant

	hashKey := makeDomainConditionOwnerIDConditionParticipantOwnerIDKey(domain, conditionOwnerID, conditionParticipantOwnerID)
	startFrom, err := dynamoutils.DecodePagingKey(cursor)

	if err != nil {
		return conditionParticipants, "", errorx.Decorate(err, "error decoding paging key in GetConditionParticipantsByOwnerIDAndConditionOwnerID")
	}

	keyCondition := expression.KeyAnd(
		expression.Key("domain-condition_owner_id-condition_participant_owner_id").Equal(expression.Value(hashKey)),
		expression.Key("condition_participant_end_state").Equal(expression.Value(conditionParticipantEndState)),
	)

	expr, err := expression.NewBuilder().WithKeyCondition(keyCondition).Build()

	if err != nil {
		return conditionParticipants, "", errorx.Decorate(err, "failed to build GetConditionParticipantsByOwnerIDAndConditionOwnerID expression")
	}

	var output *dynamodb.QueryOutput

	err = hystrix.Do(commands.DynamoGetConditionParticipantsByOwnerAndConditionOwnerCommand, func() error {
		var err error

		output, err = dao.QueryWithContext(ctx, &dynamodb.QueryInput{
			TableName:                 aws.String(dao.tableName),
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
			KeyConditionExpression:    expr.KeyCondition(),
			ExclusiveStartKey:         startFrom,
			Limit:                     aws.Int64(getConditionParticipantsQueryLimit),
			IndexName:                 aws.String(domainConditionOwnerIDParticipantOwnerIDIndexName),
		})

		return err
	}, nil)

	if err != nil {
		return conditionParticipants, "", errorx.Decorate(err, "failed to GetConditionParticipantsByOwnerIDAndConditionOwnerID from dynamo")
	}

	if output == nil || len(output.Items) == 0 {
		return conditionParticipants, "", nil
	}

	err = dynamodbattribute.UnmarshalListOfMaps(output.Items, &conditionParticipants)

	if err != nil {
		return nil, "", errorx.Decorate(err, "error marshaling items returned from GetConditionParticipantsByOwnerIDAndConditionOwnerID")
	}

	newCursor, err := dynamoutils.EncodePagingKey(output.LastEvaluatedKey)

	if err != nil {
		return nil, "", errorx.Decorate(err, "error encoding paging key in GetConditionParticipantsByOwnerIDAndConditionOwnerID")
	}

	return conditionParticipants, newCursor, nil
}

func (dao *daoImpl) GetConditionParticipantsByDomainAndOwnerID(ctx context.Context, domain, conditionParticipantOwnerID, cursor string) ([]*models.ConditionParticipant, string, error) {
	var conditionParticipants []*models.ConditionParticipant

	startFrom, err := dynamoutils.DecodePagingKey(cursor)

	if err != nil {
		return conditionParticipants, "", errorx.Decorate(err, "error decoding paging key in GetConditionParticipantsByDomainAndOwnerID")
	}
	keyCondition := expression.Key("domain-owner_id").Equal(expression.Value(makeDomainOwnerIDKey(domain, conditionParticipantOwnerID)))

	expr, err := expression.NewBuilder().WithKeyCondition(keyCondition).Build()

	if err != nil {
		return conditionParticipants, "", errorx.Decorate(err, "failed to build GetConditionParticipantsByDomainAndOwnerID expression")
	}

	var output *dynamodb.QueryOutput

	err = hystrix.Do(commands.DynamoGetConditionParticipantsByDomainAndOwnerIDCommand, func() error {
		var err error

		output, err = dao.QueryWithContext(ctx, &dynamodb.QueryInput{
			TableName:                 aws.String(dao.tableName),
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
			KeyConditionExpression:    expr.KeyCondition(),
			ExclusiveStartKey:         startFrom,
			Limit:                     aws.Int64(getConditionParticipantsByDomainAndOwnerIDQueryLimit),
			IndexName:                 aws.String(domainOwnerIDConditionParticipantIDIndexName),
		})

		return err
	}, nil)

	if err != nil {
		return conditionParticipants, "", errorx.Decorate(err, "failed to GetConditionParticipantsByDomainAndOwnerID from dynamo")
	}

	if output == nil || len(output.Items) == 0 {
		return conditionParticipants, "", nil
	}

	err = dynamodbattribute.UnmarshalListOfMaps(output.Items, &conditionParticipants)

	if err != nil {
		return nil, "", errorx.Decorate(err, "error marshaling items returned from GetConditionParticipantsByDomainAndOwnerID")
	}

	newCursor, err := dynamoutils.EncodePagingKey(output.LastEvaluatedKey)

	if err != nil {
		return nil, "", errorx.Decorate(err, "error encoding paging key in GetConditionParticipantsByDomainAndOwnerID")
	}

	return conditionParticipants, newCursor, nil
}

func (dao *daoImpl) GetConditionParticipantsByOwnerID(ctx context.Context, ownerID, cursor string) ([]*models.ConditionParticipant, string, error) {
	var conditionParticipants []*models.ConditionParticipant

	hashKey := ownerID
	startFrom, err := dynamoutils.DecodePagingKey(cursor)

	if err != nil {
		return conditionParticipants, "", errorx.Decorate(err, "error decoding paging key in GetConditionParticipantsByOwnerID")
	}

	keyCondition := expression.Key("owner_id").Equal(expression.Value(hashKey))

	expr, err := expression.NewBuilder().WithKeyCondition(keyCondition).Build()

	if err != nil {
		return conditionParticipants, "", errorx.Decorate(err, "failed to build GetConditionParticipantsByOwnerID expression")
	}

	var output *dynamodb.QueryOutput

	err = hystrix.Do(commands.DynamoGetConditionParticipantsByOwnerIDCommand, func() error {
		var err error

		output, err = dao.QueryWithContext(ctx, &dynamodb.QueryInput{
			TableName:                 aws.String(dao.tableName),
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
			KeyConditionExpression:    expr.KeyCondition(),
			ExclusiveStartKey:         startFrom,
			Limit:                     aws.Int64(getConditionParticipantsQueryLimit),
			IndexName:                 aws.String(ownerIDIndexName),
		})

		return err
	}, nil)

	if err != nil {
		return conditionParticipants, "", errorx.Decorate(err, "failed to GetConditionParticipantsByOwnerID from dynamo")
	}

	if output == nil || len(output.Items) == 0 {
		return conditionParticipants, "", nil
	}

	err = dynamodbattribute.UnmarshalListOfMaps(output.Items, &conditionParticipants)

	if err != nil {
		return nil, "", errorx.Decorate(err, "error marshaling items returned from GetConditionParticipantsByOwnerID")
	}

	newCursor, err := dynamoutils.EncodePagingKey(output.LastEvaluatedKey)

	if err != nil {
		return nil, "", errorx.Decorate(err, "error encoding paging key in GetConditionParticipantsByOwnerID")
	}

	return conditionParticipants, newCursor, nil
}

func (dao *daoImpl) GetConditionParticipantsByConditionOwnerID(ctx context.Context, conditionOwnerID, cursor string) ([]*models.ConditionParticipant, string, error) {
	var conditionParticipants []*models.ConditionParticipant

	hashKey := conditionOwnerID
	startFrom, err := dynamoutils.DecodePagingKey(cursor)

	if err != nil {
		return conditionParticipants, "", errorx.Decorate(err, "error decoding paging key in GetConditionParticipantsByConditionOwnerID")
	}

	keyCondition := expression.Key("condition_owner_id").Equal(expression.Value(hashKey))

	expr, err := expression.NewBuilder().WithKeyCondition(keyCondition).Build()

	if err != nil {
		return conditionParticipants, "", errorx.Decorate(err, "failed to build GetConditionParticipantsByConditionOwnerID expression")
	}

	var output *dynamodb.QueryOutput

	err = hystrix.Do(commands.DynamoGetConditionParticipantsByConditionOwnerIDCommand, func() error {
		var err error

		output, err = dao.QueryWithContext(ctx, &dynamodb.QueryInput{
			TableName:                 aws.String(dao.tableName),
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
			KeyConditionExpression:    expr.KeyCondition(),
			ExclusiveStartKey:         startFrom,
			Limit:                     aws.Int64(getConditionParticipantsQueryLimit),
			IndexName:                 aws.String(conditionOwnerIDIndexName),
		})

		return err
	}, nil)

	if err != nil {
		return conditionParticipants, "", errorx.Decorate(err, "failed to GetConditionParticipantsByConditionOwnerID from dynamo")
	}

	if output == nil || len(output.Items) == 0 {
		return conditionParticipants, "", nil
	}

	err = dynamodbattribute.UnmarshalListOfMaps(output.Items, &conditionParticipants)

	if err != nil {
		return nil, "", errorx.Decorate(err, "error marshaling items returned from GetConditionParticipantsByConditionOwnerID")
	}

	newCursor, err := dynamoutils.EncodePagingKey(output.LastEvaluatedKey)

	if err != nil {
		return nil, "", errorx.Decorate(err, "error encoding paging key in GetConditionParticipantsByConditionOwnerID")
	}

	return conditionParticipants, newCursor, nil
}

func (dao *daoImpl) DeleteConditionParticipant(ctx context.Context, conditionRunID, conditionParticipantID string) error {
	err := hystrix.Do(commands.DynamoDeleteConditionParticipantCommand, func() error {
		var innerErr error

		_, innerErr = dao.DeleteItemWithContext(ctx, &dynamodb.DeleteItemInput{
			Key: map[string]*dynamodb.AttributeValue{
				"condition_run_id": {
					S: aws.String(conditionRunID),
				},
				"condition_participant_id": {
					S: aws.String(conditionParticipantID),
				},
			},
			TableName: aws.String(dao.tableName),
		})

		return innerErr
	}, nil)

	if err != nil {
		return errorx.Decorate(err, "error getting condition participant")
	}

	return nil
}

func makeDomainConditionOwnerIDConditionParticipantOwnerIDKey(domain, conditionOwnerID, conditionParticipantOwnerID string) string {
	return fmt.Sprintf("%s-%s-%s", domain, conditionOwnerID, conditionParticipantOwnerID)
}

func makeDomainOwnerIDKey(domain, conditionParticipantOwnerID string) string {
	return fmt.Sprintf("%s-%s", domain, conditionParticipantOwnerID)
}
