package conditionaggregates

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/hystrix/commands"
	"code.justin.tv/commerce/pagle/backend/models"
	dynamoutils "code.justin.tv/commerce/pagle/backend/utils/dynamo"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/joomcode/errorx"
)

const (
	tableNamePrefix = "condition-aggregates"

	pendingField           = "pending"
	conditionCanceledField = "condition_canceled"
	conditionTimeoutField  = "condition_timeout"
	failedValidationField  = "failed_validation"
	canceledField          = "canceled"
	timeoutField           = "timeout"
	satisfiedField         = "satisfied"

	ownerIdIndex = "owner_id-condition_id-index"
)

type daoImpl struct {
	*dynamodb.DynamoDB
	tableName string
}

type DAO interface {
	CreateConditionAggregatesPutTransaction(domain, ownerID, conditionID string) (*dynamodb.TransactWriteItem, error)
	UpdateIncrementPending(ctx context.Context, domain, ownerID, conditionID string, effectAggregateGetter models.EffectAggregateGetter) error
	UpdateIncrementConditionCanceled(ctx context.Context, domain, ownerID, conditionID string, effectAggregateGetter models.EffectAggregateGetter) error
	UpdateIncrementConditionTimeout(ctx context.Context, domain, ownerID, conditionID string, effectAggregateGetter models.EffectAggregateGetter) error
	UpdateIncrementFailedValidation(ctx context.Context, domain, ownerID, conditionID string, effectAggregateGetter models.EffectAggregateGetter) error
	UpdateIncrementCanceled(ctx context.Context, domain, ownerID, conditionID string, effectAggregateGetter models.EffectAggregateGetter) error
	UpdateIncrementTimeout(ctx context.Context, domain, ownerID, conditionID string, effectAggregateGetter models.EffectAggregateGetter) error
	UpdateIncrementSatisfied(ctx context.Context, domain, ownerID, conditionID string, effectAggregateGetter models.EffectAggregateGetter) error
	GetConditionAggregates(ctx context.Context, domain, ownerID, conditionID string) (models.ConditionAggregates, error)
	GetConditionAggregatesByOwnerID(ctx context.Context, ownerId string, cursor string) ([]models.ConditionAggregates, string, error)
	BatchGetConditionAggregates(ctx context.Context, batchGets []models.BatchGetConditionAggregateParams) ([]models.ConditionAggregates, error)
	DeleteConditionAggregate(ctx context.Context, domain, ownerID, conditionID string) error
}

func NewDAO(sess *session.Session, suffix string) DAO {
	db := dynamodb.New(sess)
	tableName := fmt.Sprintf("%s-%s", tableNamePrefix, suffix)
	return &daoImpl{
		DynamoDB:  db,
		tableName: tableName,
	}
}

func (dao *daoImpl) GetConditionAggregates(ctx context.Context, domain, ownerID, conditionID string) (models.ConditionAggregates, error) {
	var aggregates models.ConditionAggregates
	var output *dynamodb.GetItemOutput

	err := hystrix.Do(commands.DynamoGetConditionAggregatesCommand, func() error {
		var err error

		output, err = dao.GetItemWithContext(ctx, &dynamodb.GetItemInput{
			Key:       makeKey(domain, ownerID, conditionID),
			TableName: aws.String(dao.tableName),
		})

		return err
	}, nil)

	if err != nil {
		return aggregates, errorx.Decorate(err, "error getting condition aggregates")
	}

	if len(output.Item) == 0 {
		return aggregates, nil
	}

	err = dynamodbattribute.UnmarshalMap(output.Item, &aggregates)

	if err != nil {
		return aggregates, errorx.Decorate(err, "error marshaling item returned from GetConditionAggregates")
	}

	return aggregates, err
}

func (dao *daoImpl) BatchGetConditionAggregates(ctx context.Context, batchGets []models.BatchGetConditionAggregateParams) ([]models.ConditionAggregates, error) {
	if len(batchGets) > models.MaxBatchGetItemCount {
		return nil, errors.IllegalArgument.New(fmt.Sprintf("exceed max BatchGet count of %d", models.MaxBatchGetItemCount))
	}

	var batchAggregates []models.ConditionAggregates

	var keys []map[string]*dynamodb.AttributeValue

	for _, req := range batchGets {
		keys = append(keys, makeKey(req.Domain, req.OwnerID, req.ConditionID))
	}

	var output *dynamodb.BatchGetItemOutput

	err := hystrix.Do(commands.DynamoBatchGetConditionAggregatesCommand, func() error {
		var err error
		output, err = dao.BatchGetItemWithContext(ctx, &dynamodb.BatchGetItemInput{
			RequestItems: map[string]*dynamodb.KeysAndAttributes{
				dao.tableName: {
					Keys: keys,
				},
			},
		})

		return err
	}, nil)

	if err != nil {
		return batchAggregates, errorx.Decorate(err, "error calling BatchGetItemWithContext from BatchGetConditionAggregates")
	}

	if output == nil ||
		output.Responses == nil ||
		len(output.Responses) == 0 ||
		output.Responses[dao.tableName] == nil ||
		len(output.Responses[dao.tableName]) == 0 {
		return batchAggregates, nil
	}

	err = dynamodbattribute.UnmarshalListOfMaps(output.Responses[dao.tableName], &batchAggregates)

	if err != nil {
		return batchAggregates, errorx.Decorate(err, "error marshaling items returned from BatchGetConditionAggregates")
	}

	return batchAggregates, nil

}

func (dao *daoImpl) GetConditionAggregatesByOwnerID(ctx context.Context, ownerId string, cursor string) ([]models.ConditionAggregates, string, error) {
	var batchAggregates []models.ConditionAggregates

	keyCondition := expression.Key("owner_id").Equal(expression.Value(ownerId))

	expr, err := expression.NewBuilder().WithKeyCondition(keyCondition).Build()

	if err != nil {
		return batchAggregates, "", errorx.Decorate(err, "failed to build GetConditionAggregatesByOwnerID expression")
	}

	startFrom, err := dynamoutils.DecodePagingKey(cursor)

	if err != nil {
		return batchAggregates, "", errorx.Decorate(err, "error decoding paging key in GetConditionAggregatesByOwnerID")
	}
	var output *dynamodb.QueryOutput

	err = hystrix.Do(commands.DynamoGetConditionAggregatesByOwnerIDCommand, func() error {
		var err error
		output, err = dao.QueryWithContext(ctx, &dynamodb.QueryInput{
			TableName:                 aws.String(dao.tableName),
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
			KeyConditionExpression:    expr.KeyCondition(),
			ExclusiveStartKey:         startFrom,
			Limit:                     aws.Int64(1000),
			IndexName:                 aws.String(ownerIdIndex),
		})

		return err
	}, nil)

	if err != nil {
		return batchAggregates, "", errorx.Decorate(err, "failed to GetConditionAggregatesByOwnerID from dynamo")
	}

	if output == nil || len(output.Items) == 0 {
		return batchAggregates, "", nil
	}

	err = dynamodbattribute.UnmarshalListOfMaps(output.Items, &batchAggregates)

	if err != nil {
		return nil, "", errorx.Decorate(err, "error marshaling items returned from GetConditionAggregatesByOwnerID")
	}

	newCursor, err := dynamoutils.EncodePagingKey(output.LastEvaluatedKey)

	if err != nil {
		return nil, "", errorx.Decorate(err, "error encoding paging key in GetConditionAggregatesByOwnerID")
	}

	return batchAggregates, newCursor, nil
}

func (dao *daoImpl) DeleteConditionAggregate(ctx context.Context, domain, ownerID, conditionID string) error {
	err := hystrix.Do(commands.DynamoDeleteConditionAggregateCommand, func() error {
		var innerErr error

		_, innerErr = dao.DeleteItemWithContext(ctx, &dynamodb.DeleteItemInput{
			Key:       makeKey(domain, ownerID, conditionID),
			TableName: aws.String(dao.tableName),
		})

		return innerErr
	}, nil)

	if err != nil {
		return errorx.Decorate(err, "error getting condition participant")
	}

	return nil
}

// Special case and handled separately from rest of state transitions. When a condition participant moves to pending state, we
//   * have no corresponding values to decrement
//   * update the total count of condition participants attached to the condition
func (dao *daoImpl) UpdateIncrementPending(ctx context.Context, domain, ownerID, conditionID string, effectAggregateGetter models.EffectAggregateGetter) error {
	stateFieldToIncrement := fmt.Sprintf(models.StateAggregateTotalDocumentPattern, pendingField)

	update := expression.
		Set(expression.Name(stateFieldToIncrement), expression.Name(stateFieldToIncrement).Plus(expression.Value(1))).
		Set(expression.Name(models.AggregateTotalDocumentPattern), expression.Name(models.AggregateTotalDocumentPattern).Plus(expression.Value(1))).
		Set(expression.Name("updated_at"), expression.Value(time.Now()))

	// Increment effect aggregates within the pending field, and decrement nothing
	effectUpdates, err := effectAggregateGetter(pointers.StringP(pendingField), nil)

	if err != nil {
		return err
	}

	for _, effectUpdate := range effectUpdates {
		update = update.Set(effectUpdate.NameBuilder, effectUpdate.OperandBuilder)
	}

	expr, err := expression.NewBuilder().WithUpdate(update).Build()

	if err != nil {
		return err
	}

	_, err = dao.UpdateItemWithContext(ctx, &dynamodb.UpdateItemInput{
		TableName:                 aws.String(dao.tableName),
		Key:                       makeKey(domain, ownerID, conditionID),
		UpdateExpression:          expr.Update(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	})

	return err
}

// Decrements the pending field's total count, as well as the associated effect aggregates
// Increments the new state's total count, as well as the associated effect aggregates
func (dao *daoImpl) createAggregationUpdateForStateTransaction(ctx context.Context, domain, ownerID, conditionID string, effectAggregateGetter models.EffectAggregateGetter, stateField string) error {
	stateFieldToIncrement := fmt.Sprintf(models.StateAggregateTotalDocumentPattern, stateField)
	stateFieldToDecrement := fmt.Sprintf(models.StateAggregateTotalDocumentPattern, pendingField)

	update := expression.
		Set(expression.Name(stateFieldToDecrement), expression.Name(stateFieldToDecrement).Plus(expression.Value(-1))).
		Set(expression.Name(stateFieldToIncrement), expression.Name(stateFieldToIncrement).Plus(expression.Value(1))).
		Set(expression.Name("updated_at"), expression.Value(time.Now()))

	effectUpdates, err := effectAggregateGetter(pointers.StringP(stateField), pointers.StringP(pendingField))

	if err != nil {
		return err
	}

	for _, effectUpdate := range effectUpdates {
		update = update.Set(effectUpdate.NameBuilder, effectUpdate.OperandBuilder)
	}

	expr, err := expression.NewBuilder().WithUpdate(update).Build()

	if err != nil {
		return err
	}

	_, err = dao.UpdateItemWithContext(ctx, &dynamodb.UpdateItemInput{
		TableName:                 aws.String(dao.tableName),
		Key:                       makeKey(domain, ownerID, conditionID),
		UpdateExpression:          expr.Update(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	})

	return err
}

func (dao *daoImpl) UpdateIncrementConditionCanceled(ctx context.Context, domain, ownerID, conditionID string, effectAggregateGetter models.EffectAggregateGetter) error {
	return dao.createAggregationUpdateForStateTransaction(ctx, domain, ownerID, conditionID, effectAggregateGetter, conditionCanceledField)
}

func (dao *daoImpl) UpdateIncrementConditionTimeout(ctx context.Context, domain, ownerID, conditionID string, effectAggregateGetter models.EffectAggregateGetter) error {
	return dao.createAggregationUpdateForStateTransaction(ctx, domain, ownerID, conditionID, effectAggregateGetter, conditionTimeoutField)
}

func (dao *daoImpl) UpdateIncrementFailedValidation(ctx context.Context, domain, ownerID, conditionID string, effectAggregateGetter models.EffectAggregateGetter) error {
	return dao.createAggregationUpdateForStateTransaction(ctx, domain, ownerID, conditionID, effectAggregateGetter, failedValidationField)
}

func (dao *daoImpl) UpdateIncrementCanceled(ctx context.Context, domain, ownerID, conditionID string, effectAggregateGetter models.EffectAggregateGetter) error {
	return dao.createAggregationUpdateForStateTransaction(ctx, domain, ownerID, conditionID, effectAggregateGetter, canceledField)
}

func (dao *daoImpl) UpdateIncrementTimeout(ctx context.Context, domain, ownerID, conditionID string, effectAggregateGetter models.EffectAggregateGetter) error {
	return dao.createAggregationUpdateForStateTransaction(ctx, domain, ownerID, conditionID, effectAggregateGetter, timeoutField)
}

func (dao *daoImpl) UpdateIncrementSatisfied(ctx context.Context, domain, ownerID, conditionID string, effectAggregateGetter models.EffectAggregateGetter) error {
	return dao.createAggregationUpdateForStateTransaction(ctx, domain, ownerID, conditionID, effectAggregateGetter, satisfiedField)
}

func (dao *daoImpl) CreateConditionAggregatesPutTransaction(domain, ownerID, conditionID string) (*dynamodb.TransactWriteItem, error) {
	aggregates := &models.ConditionAggregates{
		ConditionID:           conditionID,
		Domain:                domain,
		OwnerID:               ownerID,
		DomainOwnerID:         makeDomainOwnerIDKey(domain, ownerID),
		UpdatedAt:             time.Now(),
		ConditionParticipants: models.ConditionParticipantAggregates{},
	}

	dynamoAggregates, err := dynamodbattribute.MarshalMap(aggregates)

	if err != nil {
		return nil, errorx.Decorate(err, "error marshaling ConditionAggregates")
	}

	return &dynamodb.TransactWriteItem{
		Put: &dynamodb.Put{
			Item:      dynamoAggregates,
			TableName: aws.String(dao.tableName),
		},
	}, nil
}

func makeDomainOwnerIDKey(domain, ownerID string) string {
	return fmt.Sprintf("%s-%s", domain, ownerID)
}

func makeKey(domain, ownerID, conditionID string) map[string]*dynamodb.AttributeValue {
	return map[string]*dynamodb.AttributeValue{
		"domain-owner_id": {
			S: aws.String(makeDomainOwnerIDKey(domain, ownerID)),
		},
		"condition_id": {
			S: aws.String(conditionID),
		},
	}
}
