package auth

import (
	"context"
	"net/http"
)

const (
	TwitchAuthHeader = "Twitch-Authorization"
)

func WithCartmanToken(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		jwt := r.Header.Get(TwitchAuthHeader)

		ctx = context.WithValue(ctx, TwitchAuthHeader, jwt)
		r = r.WithContext(ctx)

		next.ServeHTTP(w, r)
	})
}
