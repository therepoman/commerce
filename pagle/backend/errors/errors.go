package errors

import (
	pagle "code.justin.tv/commerce/pagle/rpc"
	"code.justin.tv/commerce/pagle/rpc/errors"
	"github.com/joomcode/errorx"
	"github.com/twitchtv/twirp"
)

var (
	PagleCustomError = errorx.NewNamespace("PagleCustomError")

	NotFound         = PagleCustomError.NewType("NotFound")
	PermissionDenied = PagleCustomError.NewType("PermissionDenied")

	IllegalArgument = errorx.IllegalArgument
	InternalError   = errorx.InternalError
	IllegalState    = errorx.IllegalState

	// Condition errors
	ConditionNotActive = IllegalState.NewSubtype("ConditionNotActive")
	ConditionNotFound  = NotFound.NewSubtype("ConditionNotFound")

	// ConditionParticipant errors
	ConditionParticipantNotPending = IllegalState.NewSubtype("ConditionParticipantNotPending")
	ConditionParticipantNotFound   = IllegalState.NewSubtype("ConditionParticipantNotFound")

	// ConditionRun errors
	ConditionRunNotPending = IllegalState.NewSubtype("ConditionRunNotPending")
	ConditionRunNotFound   = IllegalState.NewSubtype("ConditionRunNotFound")

	// CreateCondition errors
	BitsRecipientIneligible = IllegalState.NewSubtype("BitsRecipientIneligible")

	// CreateConditionParticipant errors
	BitsBenefactorIneligible         = IllegalState.NewSubtype("BitsBenefactorIneligible")
	BitsAmountOutOfRange             = IllegalState.NewSubtype("BitsAmountOutOfRange")
	InsufficientBitsBalance          = IllegalArgument.NewSubtype("InsufficientBitsBalance")
	EffectTypeUnsupportedByCondition = IllegalArgument.NewSubtype("EffectTypeUnsupportedByCondition")

	// Extensions
	ChannelExtensionInstallationNotBitsEnabled = IllegalState.NewSubtype("ChannelExtensionInstallationNotBitsEnabled")
	ChannelExtensionNotInstalled               = IllegalState.NewSubtype("ChannelExtensionNotInstalled")
)

// This is used in the switch statement in InternalPagleErrorToTwirpError to avoid having a giant list of error types in
// the switch statement itself. When adding a new error type, make sure to add it to this list.
// NOTE: The order of this list matters. Subtypes should be listed before their parent types in order to prioritize
// choosing them before their parent types. The library does not automatically choose subtypes first.
var errorTypes = []*errorx.Type{
	// CreateCondition errors
	BitsRecipientIneligible,
	// CreateConditionParticipant errors
	BitsBenefactorIneligible,
	BitsAmountOutOfRange,
	EffectTypeUnsupportedByCondition,
	InsufficientBitsBalance,
	// Condition errors
	ConditionNotFound,
	ConditionNotActive,
	// ConditionParticipant errors
	ConditionParticipantNotPending,
	ConditionParticipantNotFound,
	// ConditionRun errors
	ConditionRunNotPending,
	ConditionRunNotFound,
	// Extensions,
	ChannelExtensionInstallationNotBitsEnabled,
	ChannelExtensionNotInstalled,
	// Global errors
	IllegalArgument,
	NotFound,
	PermissionDenied,
	IllegalState,
	InternalError,
}

func InternalPagleErrorToTwirpError(err error) twirp.Error {
	switch errorx.TypeSwitch(err, errorTypes...) {
	// CreateCondition errors
	case BitsRecipientIneligible:
		errorResponse := errors.PagleError{
			ErrorCode: pagle.CreateConditionErrorCode_BITS_RECIPIENT_INELIGIBLE.String(),
		}
		return errors.NewPagleError(twirp.NewError(twirp.FailedPrecondition, err.Error()), errorResponse)
	// CreateConditionParticipant errors
	case BitsBenefactorIneligible:
		errorResponse := errors.PagleError{
			ErrorCode: pagle.CreateConditionParticipantErrorCode_BITS_BENEFACTOR_INELIGIBLE.String(),
		}
		return errors.NewPagleError(twirp.NewError(twirp.FailedPrecondition, err.Error()), errorResponse)
	case BitsAmountOutOfRange:
		errorResponse := errors.PagleError{
			ErrorCode: pagle.CreateConditionParticipantErrorCode_BITS_AMOUNT_OUT_OF_RANGE.String(),
		}
		return errors.NewPagleError(twirp.NewError(twirp.InvalidArgument, err.Error()), errorResponse)
	case EffectTypeUnsupportedByCondition:
		errorResponse := errors.PagleError{
			ErrorCode: pagle.CreateConditionParticipantErrorCode_EFFECT_TYPE_UNSUPPORTED_BY_CONDITION.String(),
		}
		return errors.NewPagleError(twirp.NewError(twirp.InvalidArgument, err.Error()), errorResponse)
	case InsufficientBitsBalance:
		errorResponse := errors.PagleError{
			ErrorCode: pagle.CreateConditionParticipantErrorCode_INSUFFICIENT_BITS_BALANCE.String(),
		}
		return errors.NewPagleError(twirp.NewError(twirp.InvalidArgument, err.Error()), errorResponse)
	// Condition errors
	case ConditionNotFound:
		errorResponse := errors.PagleError{
			ErrorCode: pagle.ConditionErrorCode_CONDITION_NOT_FOUND.String(),
		}
		return errors.NewPagleError(twirp.NewError(twirp.NotFound, err.Error()), errorResponse)
	case ConditionNotActive:
		errorResponse := errors.PagleError{
			ErrorCode: pagle.ConditionErrorCode_CONDITION_NOT_ACTIVE.String(),
		}
		return errors.NewPagleError(twirp.NewError(twirp.FailedPrecondition, err.Error()), errorResponse)
	// ConditionParticipant errors
	case ConditionParticipantNotFound:
		errorResponse := errors.PagleError{
			ErrorCode: pagle.ConditionParticipantErrorCode_CONDITION_PARTICIPANT_NOT_FOUND.String(),
		}
		return errors.NewPagleError(twirp.NewError(twirp.NotFound, err.Error()), errorResponse)
	case ConditionParticipantNotPending:
		errorResponse := errors.PagleError{
			ErrorCode: pagle.ConditionParticipantErrorCode_CONDITION_PARTICIPANT_NOT_PENDING.String(),
		}
		return errors.NewPagleError(twirp.NewError(twirp.FailedPrecondition, err.Error()), errorResponse)
		// Extensions
	case ChannelExtensionInstallationNotBitsEnabled:
		errorResponse := errors.PagleError{
			ErrorCode: pagle.ExtensionErrorCode_CHANNEL_EXTENSION_INSTALLATION_NOT_BITS_ELIGIBLE.String(),
		}
		return errors.NewPagleError(twirp.NewError(twirp.FailedPrecondition, err.Error()), errorResponse)
	case ChannelExtensionNotInstalled:
		errorResponse := errors.PagleError{
			ErrorCode: pagle.ExtensionErrorCode_CHANNEL_EXTENSION_NOT_INSTALLED.String(),
		}
		return errors.NewPagleError(twirp.NewError(twirp.FailedPrecondition, err.Error()), errorResponse)
	// Global errors
	case IllegalArgument:
		return twirp.NewError(twirp.InvalidArgument, err.Error())
	case NotFound:
		return twirp.NewError(twirp.NotFound, err.Error())
	case PermissionDenied:
		return twirp.NewError(twirp.PermissionDenied, err.Error())
	case IllegalState:
		return twirp.NewError(twirp.FailedPrecondition, err.Error())
	case InternalError:
		fallthrough
	case errorx.NotRecognisedType():
		fallthrough
	default:
		return twirp.InternalErrorWith(err)
	}
}
