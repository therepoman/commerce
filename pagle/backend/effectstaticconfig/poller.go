package effectstaticconfig

import (
	"context"
	"math/rand"
	"time"

	"code.justin.tv/commerce/pagle/backend/models"

	"code.justin.tv/chat/golibs/graceful"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend/s3"
	"code.justin.tv/commerce/pagle/config"
)

const (
	refreshDelayMaxRandomOffset = time.Second * 30
	refreshDelay                = time.Minute * 5
)

type Poller interface {
	Init() error
	RefreshAtInterval()
	GetEffectStaticConfig() (*models.EffectStaticConfig, error)
}

type pollerImpl struct {
	effectStaticConfig *models.EffectStaticConfig
	S3Client           s3.S3Client    `inject:""`
	Config             *config.Config `inject:""`
}

func NewPoller() Poller {
	return &pollerImpl{}
}

func (p *pollerImpl) Init() error {
	return p.refreshEffectStaticConfig()
}

func (p *pollerImpl) RefreshAtInterval() {
	delayOffset := rand.Int63n(int64(refreshDelayMaxRandomOffset))
	timer := time.NewTicker(refreshDelay + time.Duration(delayOffset))

	for {
		select {
		case <-graceful.ShuttingDown():
			return
		case <-timer.C:
			err := p.refreshEffectStaticConfig()
			if err != nil {
				log.WithError(err).Error("Error refreshing effect static config")
			}
		}
	}
}

func (p *pollerImpl) GetEffectStaticConfig() (*models.EffectStaticConfig, error) {
	// If we are running in an environment that does not update effect config via the poller (e.g. Lambda),
	// we should query the config synchronously.
	if p.effectStaticConfig == nil {
		config, err := p.S3Client.GetEffectStaticConfig(context.Background())
		if err != nil {
			return nil, err
		}

		return config, nil
	}

	return p.effectStaticConfig, nil
}

func (p *pollerImpl) refreshEffectStaticConfig() error {
	config, err := p.S3Client.GetEffectStaticConfig(context.Background())
	if err != nil {
		return err
	}

	p.effectStaticConfig = config
	return nil
}
