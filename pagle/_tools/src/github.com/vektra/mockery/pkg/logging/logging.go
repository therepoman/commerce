package logging

const (
	LogKeyBaseDir       = "base-dir"
	LogKeyDir           = "dir"
	LogKeyDryRun        = "dry-run"
	LogKeyFile          = "file"
	LogKeyInterface     = "interface"
	LogKeyPath          = "path"
	LogKeyQualifiedName = "qualified-name"
)
