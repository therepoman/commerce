// nolint unused, megacheck
package main

import (
	"context"
	"errors"

	conditionruncompleter "code.justin.tv/commerce/pagle/backend/conditionrun/complete"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/config"
	"github.com/aws/aws-lambda-go/lambda"
)

type lambdaHandler struct {
	ConditionRunCompleter conditionruncompleter.Completer
}

func (l *lambdaHandler) Handler(ctx context.Context, req models.ConditionRunProcessingSFNInput) (models.FinalizeConditionRunSFNOutput, error) {
	if strings.Blank(req.ConditionID) {
		return models.FinalizeConditionRunSFNOutput{}, errors.New("missing condition_id")
	}

	if strings.Blank(req.ConditionRunID) {
		return models.FinalizeConditionRunSFNOutput{}, errors.New("missing condition_run_id")
	}

	if strings.Blank(req.OwnerID) {
		return models.FinalizeConditionRunSFNOutput{}, errors.New("missing owner_id")
	}

	if strings.Blank(req.Domain) {
		return models.FinalizeConditionRunSFNOutput{}, errors.New("missing domain")
	}

	hasParticipantsToProcess, err := l.ConditionRunCompleter.FinalizeConditionRun(ctx, req.Domain, req.ConditionID, req.ConditionRunID, req.OwnerID)

	if err != nil {
		return models.FinalizeConditionRunSFNOutput{}, err
	}

	return models.FinalizeConditionRunSFNOutput{
		HasParticipantsToProcess: hasParticipantsToProcess,
		Domain:                   req.Domain,
		OwnerID:                  req.OwnerID,
		ConditionRunID:           req.ConditionRunID,
		ConditionID:              req.ConditionID,
	}, nil
}

func main() {
	env := config.GetEnv()

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		log.WithError(err).Panic("error loading config")
	}

	be, err := backend.NewBackend(cfg)
	if err != nil {
		log.WithError(err).Panic("error initializing backend")
	}
	h := lambdaHandler{
		ConditionRunCompleter: be.GetConditionRunCompleter(),
	}

	lambda.Start(h.Handler)
}
