package main

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pagle/config"
	pdms_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/privacy"
	sfnclient_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/sfn"
	"code.justin.tv/eventbus/schema/pkg/user"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/sfn"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestHandler_Handle(t *testing.T) {
	Convey("given a start user deletion handler", t, func() {
		pdmsClient := &pdms_mock.PDMSClient{}
		sfnClient := &sfnclient_mock.SFNClient{}
		cfg := &config.Config{
			PDMS: &config.PDMS{
				DryRun:         false,
				ReportDeletion: true,
			},
		}

		h := lambdaHandler{
			Config:    cfg,
			SFNClient: sfnClient,
			Privacy:   pdmsClient,
		}

		ctx := context.Background()
		userID := "123123123"

		Convey("returns nil", func() {
			callsToPDMS := 0

			Convey("when we successfully start the step function and promise deletion", func() {
				sfnClient.On("ExecutePDMSUserDeletion", userID, mock.Anything).Return(nil)
				pdmsClient.On("PromiseDeletions", ctx, []string{userID}, mock.Anything).Return(nil)
				callsToPDMS = 1
			})

			Convey("when we have already started the step function and then promise deletion", func() {
				sfnClient.On("ExecutePDMSUserDeletion", userID, mock.Anything).Return(awserr.New(sfn.ErrCodeExecutionAlreadyExists, "WALRUS STRIKE", nil))
				pdmsClient.On("PromiseDeletions", ctx, []string{userID}, mock.Anything).Return(nil)
				callsToPDMS = 1
			})

			Convey("when we successfully start the step function and don't promise deletion since it's a dry run", func() {
				h.Config.PDMS.DryRun = true
				sfnClient.On("ExecutePDMSUserDeletion", userID, mock.Anything).Return(nil)
				callsToPDMS = 0
			})

			Convey("when we successfully start the step function and don't promise deletion since it's disabled", func() {
				h.Config.PDMS.ReportDeletion = false
				sfnClient.On("ExecutePDMSUserDeletion", userID, mock.Anything).Return(nil)
				callsToPDMS = 0
			})

			err := h.Handle(ctx, nil, &user.Destroy{
				UserId: userID,
			})

			So(err, ShouldBeNil)
			pdmsClient.AssertNumberOfCalls(t, "PromiseDeletions", callsToPDMS)
		})

		Convey("returns error", func() {
			Convey("when we fail to invoke the step function", func() {
				sfnClient.On("ExecutePDMSUserDeletion", userID, mock.Anything).Return(errors.New("WALRUS STRIKE"))
			})

			Convey("when we successfully start the step function and fail to promise deletion", func() {
				sfnClient.On("ExecutePDMSUserDeletion", userID, mock.Anything).Return(nil)
				pdmsClient.On("PromiseDeletions", ctx, []string{userID}, mock.Anything).Return(errors.New("WALRUS STRIKE"))
			})

			Convey("when we have already started the step function and fail to promise deletion", func() {
				sfnClient.On("ExecutePDMSUserDeletion", userID, mock.Anything).Return(nil)
				pdmsClient.On("PromiseDeletions", ctx, []string{userID}, mock.Anything).Return(errors.New("WALRUS STRIKE"))
			})

			err := h.Handle(ctx, nil, &user.Destroy{
				UserId: userID,
			})

			So(err, ShouldNotBeNil)
		})
	})
}
