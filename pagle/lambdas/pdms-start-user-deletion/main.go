package main

import (
	"context"
	"os"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend"
	"code.justin.tv/commerce/pagle/backend/models"
	pdms "code.justin.tv/commerce/pagle/backend/privacy"
	pagle_sfn "code.justin.tv/commerce/pagle/backend/sfn"
	"code.justin.tv/commerce/pagle/config"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/client/subscriber/lambdafunc"
	"code.justin.tv/eventbus/schema/pkg/user"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/urfave/cli"
)

const (
	promiseDeleteNumOfDays = 30 * 24 * time.Hour // this value may change depending on recommendations from PDMS
)

var environment string
var userID string
var dryrun bool
var local bool

func main() {
	app := cli.NewApp()
	app.Name = "StartDeletion"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production', and 'staging'. Defaults to 'staging' if unset.",
			EnvVar:      config.EnvironmentEnvironmentVariable,
			Destination: &environment,
		},
		cli.StringFlag{
			Name:        "userID, u",
			Usage:       "The user ID to start deletion on; only used for local execution",
			Destination: &userID,
		},
		cli.BoolFlag{
			Name:        "dryrun",
			Usage:       "Pass in this flag to not actually report to PDMS deletions",
			Destination: &dryrun,
		},
		cli.BoolFlag{
			Name:        "local",
			Usage:       "Pass in this flag to run the handler directly instead of through lambda.Start()",
			Destination: &local,
		},
	}

	app.Action = createBackend

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func createBackend(c *cli.Context) {
	env := config.GetEnv()

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		log.WithError(err).Panic("error loading config")
	}

	be, err := backend.NewBackend(cfg)

	if err != nil {
		log.WithError(err).Panic("error loading backend")
	}

	pdmsClient := pdms.NewClient(*cfg)

	handler := lambdaHandler{
		Privacy:   pdmsClient,
		SFNClient: be.GetSFNClient(),
		Config:    cfg,
	}

	if local {
		ctx, cancel := context.WithTimeout(context.Background(), time.Duration(2)*time.Minute)
		defer cancel()
		err := handler.Handle(ctx, &eventbus.Header{}, &user.Destroy{
			UserId: userID,
		})
		if err != nil {
			log.WithError(err).Error("Lambda Handler Execution Failed")
		}
		log.Info("Execution Complete")
	} else {
		mux := eventbus.NewMux()

		user.RegisterDestroyHandler(mux, handler.Handle)

		lambda.Start(lambdafunc.NewSQS(mux.Dispatcher()))
	}
}

type lambdaHandler struct {
	Privacy   pdms.PDMSClient
	SFNClient pagle_sfn.SFNClient
	Config    *config.Config
}

func (l *lambdaHandler) Handle(ctx context.Context, header *eventbus.Header, event *user.Destroy) error {
	log.WithField("userID", event.UserId).Info("Starting User Destroy Step Function")
	err := l.SFNClient.ExecutePDMSUserDeletion(event.UserId, models.StartUserDeletionRequest{
		UserDeletionRequestData: models.UserDeletionRequestData{
			UserIDs:        []string{event.UserId},
			IsDryRun:       l.Config.PDMS.DryRun,
			ReportDeletion: l.Config.PDMS.ReportDeletion,
		},
	})
	if err != nil {
		var existingExecutionError bool
		if awsErr, ok := err.(awserr.Error); ok {
			if awsErr.Code() == sfn.ErrCodeExecutionAlreadyExists {
				existingExecutionError = true
			}
		}

		if existingExecutionError {
			log.WithField("userID", event.UserId).Info("found existing User Destroy SFN execution, skipping.")
		} else {
			log.WithField("userID", event.UserId).WithError(err).Error("error creating User Destroy SFN execution")
			return err
		}
	}
	timeOfDeletionPromise := time.Now().Add(promiseDeleteNumOfDays)
	log := log.WithFields(log.Fields{
		"userID":    event.UserId,
		"timestamp": timeOfDeletionPromise,
	})
	if l.Config.PDMS.DryRun {
		log.Info("Not sending to PDMS to promise deletion due to Dry Run")
		return nil
	} else if !l.Config.PDMS.ReportDeletion {
		log.Info("Not sending to PDMS to promise deletion due to reporting deletion being disabled")
		return nil
	} else {
		log.Info("Sending PromiseDeletion to PDMS")
		err = l.Privacy.PromiseDeletions(ctx, []string{event.UserId}, timeOfDeletionPromise)

		if err != nil {
			log.WithError(err).Error("error sending user PromiseDeletion to PDMS")
			return err
		}

		return nil
	}
}
