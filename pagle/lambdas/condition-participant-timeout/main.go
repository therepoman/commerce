// nolint unused, megacheck
package main

import (
	"context"
	"errors"

	conditionparticipantcompleter "code.justin.tv/commerce/pagle/backend/conditionparticipant/complete"
	"github.com/joomcode/errorx"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend"
	pagleErrors "code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/config"
	"github.com/aws/aws-lambda-go/lambda"
)

type lambdaHandler struct {
	ConditionParticipantCompleter conditionparticipantcompleter.Completer
}

func (l *lambdaHandler) Handler(ctx context.Context, req models.TimeoutConditionParticipantSFNInput) error {
	if strings.Blank(req.ConditionParticipantID) {
		return errors.New("missing condition_participant_id")
	}

	if strings.Blank(req.ConditionRunID) {
		return errors.New("missing condition_run_id")
	}

	err := l.ConditionParticipantCompleter.ExpireConditionParticipant(ctx, req.ConditionRunID, req.ConditionParticipantID)

	if err == nil {
		return nil
	}

	logFields := log.Fields{
		"condition_participant_id": req.ConditionParticipantID,
		"condition_run_id":         req.ConditionRunID,
	}

	switch errorx.TypeSwitch(err, pagleErrors.ConditionParticipantNotPending, pagleErrors.ConditionRunNotPending) {
	case pagleErrors.ConditionParticipantNotPending:
		log.WithFields(logFields).Info("attempting to expire a condition participant that is no longer pending")
		return nil
	case pagleErrors.ConditionRunNotPending:
		log.WithFields(logFields).Warn("attempting to expire a condition participant whose run is no longer pending")
		return nil
	default:
		return err
	}
}

func main() {
	env := config.GetEnv()

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		log.WithError(err).Panic("error loading config")
	}

	be, err := backend.NewBackend(cfg)
	if err != nil {
		log.WithError(err).Panic("error initializing backend")
	}
	h := lambdaHandler{
		ConditionParticipantCompleter: be.GetConditionParticipantCompleter(),
	}

	lambda.Start(h.Handler)
}
