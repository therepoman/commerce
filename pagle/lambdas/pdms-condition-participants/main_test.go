package main

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pagle/_tools/src/github.com/pkg/errors"
	"code.justin.tv/commerce/pagle/backend/models"
	conditionparticipantdao_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/dynamo/conditionparticipant"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/assert"
)

func TestConditionParticipantPDMSHandler(t *testing.T) {
	Convey("given a conditionParticipant pdms handler", t, func() {
		conditionParticipantDAO := new(conditionparticipantdao_mock.DAO)

		handler := conditionsParticipantsDeletionLambdaHandler{
			ConditionParticipantDAO: conditionParticipantDAO,
		}

		Convey("when the user list is empty returns an erro from having an empty list", func() {
			response, err := handler.Handle(context.Background(), &models.CleanConditionParticipantsRequest{})
			assert.NotNil(t, err)
			assert.Nil(t, response)
		})

		Convey("when the user list is not empty, but no entries found", func() {
			ctx := context.Background()
			conditionParticipantDAO.On("GetConditionParticipantsByOwnerID", ctx, "user_id", "").Return(nil, "", nil)
			conditionParticipantDAO.On("GetConditionParticipantsByConditionOwnerID", ctx, "user_id", "").Return(nil, "", nil)
			response, err := handler.Handle(ctx, &models.CleanConditionParticipantsRequest{
				UserDeletionRequestData: models.UserDeletionRequestData{
					UserIDs:  []string{"user_id"},
					IsDryRun: dryrun,
				},
			})
			assert.NotNil(t, response)
			assert.Nil(t, err)
		})

		Convey("when the user list is not empty, but dao errors on first call", func() {
			ctx := context.Background()
			conditionParticipantDAO.On("GetConditionParticipantsByOwnerID", ctx, "user_id", "").Return(nil, "", errors.New("error"))
			response, err := handler.Handle(ctx, &models.CleanConditionParticipantsRequest{
				UserDeletionRequestData: models.UserDeletionRequestData{
					UserIDs:  []string{"user_id"},
					IsDryRun: dryrun,
				},
			})
			assert.NotNil(t, err)
			assert.Nil(t, response)
		})

		Convey("when the user list is not empty, but dao errors on second call", func() {
			ctx := context.Background()
			conditionParticipantDAO.On("GetConditionParticipantsByOwnerID", ctx, "user_id", "").Return(nil, "", nil)
			conditionParticipantDAO.On("GetConditionParticipantsByConditionOwnerID", ctx, "user_id", "").Return(nil, "", errors.New("error"))
			response, err := handler.Handle(ctx, &models.CleanConditionParticipantsRequest{
				UserDeletionRequestData: models.UserDeletionRequestData{
					UserIDs:  []string{"user_id"},
					IsDryRun: dryrun,
				},
			})
			assert.NotNil(t, err)
			assert.Nil(t, response)
		})

		listOfUsers := []*models.ConditionParticipant{{ConditionParticipantID: "a", ConditionRunID: "1"}}
		otherListOfUsers := []*models.ConditionParticipant{{ConditionParticipantID: "z", ConditionRunID: "9"}}

		Convey("when the user list is not empty, and entries are found and deleted", func() {
			ctx := context.Background()
			conditionParticipantDAO.On("GetConditionParticipantsByOwnerID", ctx, "user_id", "").Return(listOfUsers, "", nil)
			conditionParticipantDAO.On("GetConditionParticipantsByConditionOwnerID", ctx, "user_id", "").Return(otherListOfUsers, "", nil)

			conditionParticipantDAO.On("DeleteConditionParticipant", ctx, "1", "a").Return(nil)
			conditionParticipantDAO.On("DeleteConditionParticipant", ctx, "9", "z").Return(nil)
			response, err := handler.Handle(ctx, &models.CleanConditionParticipantsRequest{
				UserDeletionRequestData: models.UserDeletionRequestData{
					UserIDs:  []string{"user_id"},
					IsDryRun: dryrun,
				},
			})
			conditionParticipantDAO.AssertNumberOfCalls(t, "DeleteConditionParticipant", 2)
			assert.NotNil(t, response)
			assert.Nil(t, err)
		})

		Convey("when the user list is not empty, and entries are found and deleted, but first delete fails", func() {
			ctx := context.Background()
			conditionParticipantDAO.On("GetConditionParticipantsByOwnerID", ctx, "user_id", "").Return(listOfUsers, "", nil)
			conditionParticipantDAO.On("GetConditionParticipantsByConditionOwnerID", ctx, "user_id", "").Return(otherListOfUsers, "", nil)

			conditionParticipantDAO.On("DeleteConditionParticipant", ctx, "1", "a").Return(errors.New("oh no"))
			response, err := handler.Handle(ctx, &models.CleanConditionParticipantsRequest{
				UserDeletionRequestData: models.UserDeletionRequestData{
					UserIDs:  []string{"user_id"},
					IsDryRun: dryrun,
				},
			})
			conditionParticipantDAO.AssertNumberOfCalls(t, "DeleteConditionParticipant", 1)
			assert.NotNil(t, err)
			assert.Nil(t, response)
		})

		Convey("when the user list is not empty, and entries are found and deleted, but second delete fails", func() {
			ctx := context.Background()
			conditionParticipantDAO.On("GetConditionParticipantsByOwnerID", ctx, "user_id", "").Return(listOfUsers, "", nil)
			conditionParticipantDAO.On("GetConditionParticipantsByConditionOwnerID", ctx, "user_id", "").Return(otherListOfUsers, "", nil)

			conditionParticipantDAO.On("DeleteConditionParticipant", ctx, "1", "a").Return(nil)
			conditionParticipantDAO.On("DeleteConditionParticipant", ctx, "9", "z").Return(errors.New("oh no"))
			response, err := handler.Handle(ctx, &models.CleanConditionParticipantsRequest{
				UserDeletionRequestData: models.UserDeletionRequestData{
					UserIDs:  []string{"user_id"},
					IsDryRun: dryrun,
				},
			})

			conditionParticipantDAO.AssertNumberOfCalls(t, "DeleteConditionParticipant", 2)
			assert.NotNil(t, err)
			assert.Nil(t, response)
		})

		listOfUsers = append(listOfUsers, &models.ConditionParticipant{ConditionParticipantID: "m", ConditionRunID: "4"})

		Convey("when the user list is not empty, multiple entries are found and deleted", func() {
			ctx := context.Background()
			conditionParticipantDAO.On("GetConditionParticipantsByOwnerID", ctx, "user_id", "").Return(listOfUsers, "", nil)
			conditionParticipantDAO.On("GetConditionParticipantsByConditionOwnerID", ctx, "user_id", "").Return(otherListOfUsers, "", nil)

			conditionParticipantDAO.On("DeleteConditionParticipant", ctx, "1", "a").Return(nil)
			conditionParticipantDAO.On("DeleteConditionParticipant", ctx, "4", "m").Return(nil)
			conditionParticipantDAO.On("DeleteConditionParticipant", ctx, "9", "z").Return(nil)
			response, err := handler.Handle(ctx, &models.CleanConditionParticipantsRequest{
				UserDeletionRequestData: models.UserDeletionRequestData{
					UserIDs:  []string{"user_id"},
					IsDryRun: dryrun,
				},
			})
			conditionParticipantDAO.AssertNumberOfCalls(t, "DeleteConditionParticipant", 3)
			assert.NotNil(t, response)
			assert.Nil(t, err)
		})
	})
}
