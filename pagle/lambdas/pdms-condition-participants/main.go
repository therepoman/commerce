package main

import (
	"context"
	"os"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend"
	"code.justin.tv/commerce/pagle/backend/dynamo/conditionparticipant"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/backend/utils/privacy"
	"code.justin.tv/commerce/pagle/config"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
)

var environment string
var dryrun bool
var local bool

func main() {
	app := cli.NewApp()
	app.Name = "CleanConditionPariticpants"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production', and 'staging'. Defaults to 'staging' if unset.",
			EnvVar:      config.EnvironmentEnvironmentVariable,
			Destination: &environment,
		},
		cli.StringSliceFlag{
			Name:  "userIDs",
			Usage: "Runtime user IDs to report deletion for. Only works if local is set to true.",
		},
		cli.BoolFlag{
			Name:        "dryrun",
			Usage:       "Pass in this flag to not actually report to PDMS deletions",
			Destination: &dryrun,
		},
		cli.BoolFlag{
			Name:        "local",
			Usage:       "Pass in this flag to run the handler directly instead of through lambda.Start()",
			Destination: &local,
		},
	}

	app.Action = createBackend

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func createBackend(c *cli.Context) {
	cfg, err := config.LoadConfig(config.Environment(environment))
	if err != nil || cfg == nil {
		log.WithError(err).Panic("error loading config")
	}

	be, err := backend.NewBackend(cfg)

	if err != nil {
		log.WithError(err).Panic("error loading backend")
	}

	handler := NewConditionsParticipantsDeletionLambdaHandler(be.GetConditionParticipantDAO())

	if local {
		ctx, cancel := context.WithTimeout(context.Background(), time.Duration(2)*time.Minute)
		defer cancel()
		output, err := handler.Handle(ctx, &models.CleanConditionParticipantsRequest{
			UserDeletionRequestData: models.UserDeletionRequestData{
				UserIDs:  c.StringSlice("userIDs"),
				IsDryRun: dryrun,
			},
		})
		if err != nil {
			log.WithError(err).Error("Lambda Handler Execution Failed")
		}
		log.WithField("output", output).Info("Execution Output")
	} else {
		lambda.Start(handler.Handle)
	}
}

type conditionsParticipantsDeletionLambdaHandler struct {
	ConditionParticipantDAO conditionparticipant.DAO
}

func NewConditionsParticipantsDeletionLambdaHandler(dao conditionparticipant.DAO) *conditionsParticipantsDeletionLambdaHandler {
	return &conditionsParticipantsDeletionLambdaHandler{
		ConditionParticipantDAO: dao,
	}
}

func (h *conditionsParticipantsDeletionLambdaHandler) Handle(ctx context.Context, input *models.CleanConditionParticipantsRequest) (*models.CleanConditionParticipantsResponse, error) {
	err := privacy.IsValidUserDeletionRequest(input)

	if err != nil {
		log.WithFields(log.Fields{
			"user_ids":        input.UserIDs,
			"is_dry_run":      input.IsDryRun,
			"report_deletion": input.ReportDeletion,
		}).WithError(err).Error("Error bad pdms deletion request")
		return nil, err
	}

	for _, userID := range input.UserIDs {
		cursor := ""
		for {
			conditions, newCursor, err := h.ConditionParticipantDAO.GetConditionParticipantsByOwnerID(ctx, userID, cursor)
			cursor = newCursor

			if err != nil {
				log.WithFields(log.Fields{
					"owner_id": userID,
				}).WithError(err).Error("Error querying for conditions by owner Id for user in pdms flow.")
				return nil, err
			}

			if input.IsDryRun {
				logrus.Infof("not initiating delete for users records, was dry run. Found %d records", len(conditions))
			} else {
				for _, con := range conditions {
					err = h.ConditionParticipantDAO.DeleteConditionParticipant(ctx, con.ConditionRunID, con.ConditionParticipantID)
					if err != nil {
						log.WithFields(log.Fields{
							"condition_run_id":         con.ConditionRunID,
							"condition_participant_id": con.ConditionParticipantID,
						}).WithError(err).Error("Error deleting condition for user in pdms flow.")
						return nil, err
					}
				}
			}

			if cursor == "" {
				break
			}
		}

		cursor = ""
		for {
			conditions, newCursor, err := h.ConditionParticipantDAO.GetConditionParticipantsByConditionOwnerID(ctx, userID, cursor)
			cursor = newCursor

			if err != nil {
				log.WithFields(log.Fields{
					"extension_installation_channel_id": userID,
				}).WithError(err).Error("Error querying for conditions by ext channel id for user in pdms flow.")
				return nil, err
			}

			if input.IsDryRun {
				logrus.Infof("not initiating delete for users records, was dry run. Found %d records", len(conditions))
			} else {
				for _, con := range conditions {
					err = h.ConditionParticipantDAO.DeleteConditionParticipant(ctx, con.ConditionRunID, con.ConditionParticipantID)
					if err != nil {
						log.WithFields(log.Fields{
							"condition_run_id":         con.ConditionRunID,
							"condition_participant_id": con.ConditionParticipantID,
						}).WithError(err).Error("Error deleting condition for user in pdms flow.")
						return nil, err
					}
				}
			}

			if cursor == "" {
				break
			}
		}
	}

	return &models.CleanConditionParticipantsResponse{
		UserDeletionResponseData: models.UserDeletionResponseData{
			UserIDs:        input.UserIDs,
			IsDryRun:       input.IsDryRun,
			ReportDeletion: input.ReportDeletion,
		},
	}, nil
}
