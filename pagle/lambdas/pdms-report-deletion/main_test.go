package main

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pagle/backend/models"
	pdms_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/privacy"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("Given a PDMS delete user leaderboard entries handler", t, func() {
		pdmsClient := new(pdms_mock.PDMSClient)

		handler := NewReportDeletionLambdaHandler(pdmsClient)

		ctx := context.Background()
		userID := "123123123"

		Convey("returns error", func() {
			var input *models.ReportDeletionRequest

			Convey("when the input is nil", func() {
				input = nil
			})

			Convey("when the input contains no user ID", func() {
				input = &models.ReportDeletionRequest{}
			})

			Convey("when we get an error from PDMS when reporting deletion is enabled", func() {
				input = &models.ReportDeletionRequest{
					UserDeletionRequestData: models.UserDeletionRequestData{
						UserIDs:        []string{userID},
						ReportDeletion: true,
					},
				}
				pdmsClient.On("ReportDeletions", ctx, []string{userID}, mock.Anything).Return(errors.New("WALRUS STRIKE"))
			})

			_, err := handler.Handle(ctx, input)

			So(err, ShouldNotBeNil)
		})

		Convey("returns success", func() {
			var input *models.ReportDeletionRequest
			var numberOfCalls int

			Convey("when we successfully call Pantheon with valid input", func() {
				input = &models.ReportDeletionRequest{
					UserDeletionRequestData: models.UserDeletionRequestData{
						UserIDs:        []string{userID},
						ReportDeletion: true,
					},
				}
				pdmsClient.On("ReportDeletions", ctx, []string{userID}, mock.Anything).Return(nil)
				numberOfCalls = 1
			})

			Convey("when it is a dry run", func() {
				input = &models.ReportDeletionRequest{
					UserDeletionRequestData: models.UserDeletionRequestData{
						UserIDs:  []string{userID},
						IsDryRun: true,
					},
				}
				numberOfCalls = 0
			})

			Convey("when reporting deletion is disabled and it's not a dry run", func() {
				input = &models.ReportDeletionRequest{
					UserDeletionRequestData: models.UserDeletionRequestData{
						UserIDs: []string{userID},
					},
				}
				numberOfCalls = 0
			})

			resp, err := handler.Handle(ctx, input)

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.UserIDs, ShouldResemble, []string{userID})
			pdmsClient.AssertNumberOfCalls(t, "ReportDeletions", numberOfCalls)
		})
	})
}
