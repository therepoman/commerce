package main

import (
	"context"
	"os"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend/models"
	pdms "code.justin.tv/commerce/pagle/backend/privacy"
	pdms_utils "code.justin.tv/commerce/pagle/backend/utils/privacy"
	"code.justin.tv/commerce/pagle/config"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
)

var environment string
var dryrun bool
var local bool

func main() {
	app := cli.NewApp()
	app.Name = "ReportDeletion"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production', and 'staging'. Defaults to 'staging' if unset.",
			EnvVar:      config.EnvironmentEnvironmentVariable,
			Destination: &environment,
		},
		cli.StringSliceFlag{
			Name:  "userID",
			Usage: "Runtime user IDs to report deletion for. Only works if local is set to true.",
		},
		cli.BoolFlag{
			Name:        "dryrun",
			Usage:       "Pass in this flag to not actually report to PDMS deletions",
			Destination: &dryrun,
		},
		cli.BoolFlag{
			Name:        "local",
			Usage:       "Pass in this flag to run the handler directly instead of through lambda.Start()",
			Destination: &local,
		},
	}

	app.Action = createBackend

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func createBackend(c *cli.Context) {
	cfg, err := config.LoadConfig(config.Environment(environment))
	if err != nil || cfg == nil {
		log.WithError(err).Panic("error loading config")
	}

	pdmsClient := pdms.NewClient(*cfg)

	handler := NewReportDeletionLambdaHandler(pdmsClient)

	if local {
		ctx, cancel := context.WithTimeout(context.Background(), time.Duration(2)*time.Minute)
		defer cancel()
		output, err := handler.Handle(ctx, &models.ReportDeletionRequest{
			UserDeletionRequestData: models.UserDeletionRequestData{
				UserIDs:  c.StringSlice("userIDs"),
				IsDryRun: dryrun,
			},
		})
		if err != nil {
			log.WithError(err).Error("Lambda Handler Execution Failed")
		}
		log.WithField("output", output).Info("Execution Output")
	} else {
		lambda.Start(handler.Handle)
	}
}

type reportDeletionLambdaHandler struct {
	Privacy pdms.PDMSClient
}

func NewReportDeletionLambdaHandler(privacy pdms.PDMSClient) *reportDeletionLambdaHandler {
	return &reportDeletionLambdaHandler{
		Privacy: privacy,
	}
}

func (h *reportDeletionLambdaHandler) Handle(ctx context.Context, input *models.ReportDeletionRequest) (*models.ReportDeletionResponse, error) {
	err := pdms_utils.IsValidUserDeletionRequest(input)
	if err != nil {
		return nil, err
	}

	timeOfDeletion := time.Now()

	if input.IsDryRun {
		logrus.Info("Dry Run, not reporting deletion to PDMS")
	} else if !input.ReportDeletion {
		logrus.Info("not reporting deletion to PDMS due to input has it disabled")
	} else {
		err = h.Privacy.ReportDeletions(ctx, input.UserIDs, timeOfDeletion)
		if err != nil {
			return nil, errors.Wrap(err, "failed to report deletion to PDMS")
		}
	}

	return &models.ReportDeletionResponse{
		UserDeletionResponseData: models.UserDeletionResponseData{
			UserIDs:        input.UserIDs,
			IsDryRun:       input.IsDryRun,
			ReportDeletion: input.ReportDeletion,
		},
		Timestamp: timeOfDeletion,
	}, nil
}
