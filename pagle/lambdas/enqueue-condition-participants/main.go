// nolint unused, megacheck
package main

import (
	"context"
	"errors"

	conditionruncompleter "code.justin.tv/commerce/pagle/backend/conditionrun/complete"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/config"
	"github.com/aws/aws-lambda-go/lambda"
)

type lambdaHandler struct {
	ConditionRunCompleter conditionruncompleter.Completer
}

func (l *lambdaHandler) Handler(ctx context.Context, req models.FinalizeConditionRunSFNOutput) error {
	if strings.Blank(req.ConditionID) {
		return errors.New("missing condition_id")
	}

	if strings.Blank(req.ConditionRunID) {
		return errors.New("missing condition_run_id")
	}

	if strings.Blank(req.OwnerID) {
		return errors.New("missing owner_id")
	}

	if strings.Blank(req.Domain) {
		return errors.New("missing domain")
	}

	return l.ConditionRunCompleter.EnqueueConditionParticipants(ctx, req.Domain, req.ConditionID, req.ConditionRunID, req.OwnerID)
}

func main() {
	env := config.GetEnv()

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		log.WithError(err).Panic("error loading config")
	}

	be, err := backend.NewBackend(cfg)
	if err != nil {
		log.WithError(err).Panic("error initializing backend")
	}
	h := lambdaHandler{
		ConditionRunCompleter: be.GetConditionRunCompleter(),
	}

	lambda.Start(h.Handler)
}
