// nolint unused, megacheck
package main

import (
	"context"
	"encoding/json"

	conditionparticipantcompleter "code.justin.tv/commerce/pagle/backend/conditionparticipant/complete"

	"code.justin.tv/commerce/gogogadget/aws/sqs/sns"
	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/config"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/pkg/errors"
)

type lambdaHandler struct {
	ConditionParticipantCompleter conditionparticipantcompleter.Completer
}

func (l *lambdaHandler) Handler(ctx context.Context, sqsEvent models.SQSEvent) error {
	if len(sqsEvent.Records) == 0 {
		return nil
	}

	if len(sqsEvent.Records) != 1 {
		return errors.New("lambda can only handle a batch size of 1")
	}

	snsMsg, err := sns.Extract(&sqsEvent.Records[0])

	if err != nil {
		return err
	}

	message, err := messageFromJSON(snsMsg.Message)

	if err != nil {
		return errors.Wrap(err, "error extracting message from sns json")
	}

	if strings.Blank(message.ConditionParticipantID) {
		return errors.New("missing condition_participant_id")
	}

	if strings.Blank(message.ConditionRunID) {
		return errors.New("missing condition_run_id")
	}

	log.WithFields(log.Fields{
		"condition_run_id":         message.ConditionRunID,
		"condition_participant_id": message.ConditionParticipantID,
	}).Info("starting condition participant cleanup work")

	return l.ConditionParticipantCompleter.CleanupConditionParticipant(context.Background(), message.ConditionRunID, message.ConditionParticipantID)
}

// Retry processing for erred Dynamo stream records. Event source is condition-aggregates-retry queue.
func main() {
	env := config.GetEnv()

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		log.WithError(err).Panic("error loading config")
	}

	be, err := backend.NewBackend(cfg)
	if err != nil {
		log.WithError(err).Panic("error initializing backend")
	}
	h := lambdaHandler{
		ConditionParticipantCompleter: be.GetConditionParticipantCompleter(),
	}

	lambda.Start(h.Handler)
}

func messageFromJSON(jsonStr string) (models.ConditionParticipantCleanupSNSMessage, error) {
	var message models.ConditionParticipantCleanupSNSMessage

	err := json.Unmarshal([]byte(jsonStr), &message)

	if err != nil {
		return models.ConditionParticipantCleanupSNSMessage{}, err
	}

	return message, nil
}
