package main

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pagle/_tools/src/github.com/pkg/errors"
	"code.justin.tv/commerce/pagle/backend/models"
	conditionaggregatesdao_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/dynamo/conditionaggregates"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/assert"
)

func TestConditionAggregatesPDMSHandler(t *testing.T) {
	Convey("given a conditionParticipant pdms handler", t, func() {
		conditionAggregatesDAO := new(conditionaggregatesdao_mock.DAO)

		handler := conditionsAggregatesDeletionLambdaHandler{
			ConditionAggregatesDAO: conditionAggregatesDAO,
		}

		Convey("when the user list is empty returns an erro from having an empty list", func() {
			response, err := handler.Handle(context.Background(), &models.CleanConditionAggregatesRequest{})
			assert.NotNil(t, err)
			assert.Nil(t, response)
		})

		Convey("when the user list is not empty, but no entries found", func() {
			ctx := context.Background()
			conditionAggregatesDAO.On("GetConditionAggregatesByOwnerID", ctx, "user_id", "").Return(nil, "", nil)
			response, err := handler.Handle(ctx, &models.CleanConditionAggregatesRequest{
				UserDeletionRequestData: models.UserDeletionRequestData{
					UserIDs:  []string{"user_id"},
					IsDryRun: dryrun,
				},
			})
			assert.NotNil(t, response)
			assert.Nil(t, err)
		})

		Convey("when the user list is not empty, but dao errors on first call", func() {
			ctx := context.Background()
			conditionAggregatesDAO.On("GetConditionAggregatesByOwnerID", ctx, "user_id", "").Return(nil, "", errors.New("error"))
			response, err := handler.Handle(ctx, &models.CleanConditionAggregatesRequest{
				UserDeletionRequestData: models.UserDeletionRequestData{
					UserIDs:  []string{"user_id"},
					IsDryRun: dryrun,
				},
			})
			assert.NotNil(t, err)
			assert.Nil(t, response)
		})

		listOfUsers := []models.ConditionAggregates{{OwnerID: "a", Domain: "1", ConditionID: "A"}, {OwnerID: "z", Domain: "9", ConditionID: "Z"}}

		Convey("when the user list is not empty, and entries are found and deleted", func() {
			ctx := context.Background()
			conditionAggregatesDAO.On("GetConditionAggregatesByOwnerID", ctx, "user_id", "").Return(listOfUsers, "", nil)

			conditionAggregatesDAO.On("DeleteConditionAggregate", ctx, "1", "a", "A").Return(nil)
			conditionAggregatesDAO.On("DeleteConditionAggregate", ctx, "9", "z", "Z").Return(nil)
			response, err := handler.Handle(ctx, &models.CleanConditionAggregatesRequest{
				UserDeletionRequestData: models.UserDeletionRequestData{
					UserIDs:  []string{"user_id"},
					IsDryRun: dryrun,
				},
			})
			conditionAggregatesDAO.AssertNumberOfCalls(t, "DeleteConditionAggregate", 2)
			assert.NotNil(t, response)
			assert.Nil(t, err)
		})

		Convey("when the user list is not empty, and entries are found and deleted, but first delete fails", func() {
			ctx := context.Background()
			conditionAggregatesDAO.On("GetConditionAggregatesByOwnerID", ctx, "user_id", "").Return(listOfUsers, "", nil)

			conditionAggregatesDAO.On("DeleteConditionAggregate", ctx, "1", "a", "A").Return(errors.New("oh no"))
			response, err := handler.Handle(ctx, &models.CleanConditionAggregatesRequest{
				UserDeletionRequestData: models.UserDeletionRequestData{
					UserIDs:  []string{"user_id"},
					IsDryRun: dryrun,
				},
			})
			conditionAggregatesDAO.AssertNumberOfCalls(t, "DeleteConditionAggregate", 1)
			assert.NotNil(t, err)
			assert.Nil(t, response)
		})

		Convey("when the user list is not empty, and entries are found and deleted, but second delete fails", func() {
			ctx := context.Background()
			conditionAggregatesDAO.On("GetConditionAggregatesByOwnerID", ctx, "user_id", "").Return(listOfUsers, "", nil)

			conditionAggregatesDAO.On("DeleteConditionAggregate", ctx, "1", "a", "A").Return(nil)
			conditionAggregatesDAO.On("DeleteConditionAggregate", ctx, "9", "z", "Z").Return(errors.New("oh no"))
			response, err := handler.Handle(ctx, &models.CleanConditionAggregatesRequest{
				UserDeletionRequestData: models.UserDeletionRequestData{
					UserIDs:  []string{"user_id"},
					IsDryRun: dryrun,
				},
			})

			conditionAggregatesDAO.AssertNumberOfCalls(t, "DeleteConditionAggregate", 2)
			assert.NotNil(t, err)
			assert.Nil(t, response)
		})

		listOfUsers = append(listOfUsers, models.ConditionAggregates{OwnerID: "m", Domain: "4", ConditionID: "M"})

		Convey("when the user list is not empty, multiple entries are found and deleted", func() {
			ctx := context.Background()
			conditionAggregatesDAO.On("GetConditionAggregatesByOwnerID", ctx, "user_id", "").Return(listOfUsers, "", nil)

			conditionAggregatesDAO.On("DeleteConditionAggregate", ctx, "1", "a", "A").Return(nil)
			conditionAggregatesDAO.On("DeleteConditionAggregate", ctx, "4", "m", "M").Return(nil)
			conditionAggregatesDAO.On("DeleteConditionAggregate", ctx, "9", "z", "Z").Return(nil)
			response, err := handler.Handle(ctx, &models.CleanConditionAggregatesRequest{
				UserDeletionRequestData: models.UserDeletionRequestData{
					UserIDs:  []string{"user_id"},
					IsDryRun: dryrun,
				},
			})
			conditionAggregatesDAO.AssertNumberOfCalls(t, "DeleteConditionAggregate", 3)
			assert.NotNil(t, response)
			assert.Nil(t, err)
		})
	})
}
