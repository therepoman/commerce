package main

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pagle/_tools/src/github.com/pkg/errors"
	"code.justin.tv/commerce/pagle/backend/models"
	conditiondao_mock "code.justin.tv/commerce/pagle/mocks/code.justin.tv/commerce/pagle/backend/dynamo/condition"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/assert"
)

func TestConditionsPDMSHandler(t *testing.T) {
	Convey("given a condition pdms handler", t, func() {
		conditionParticipantDAO := new(conditiondao_mock.DAO)

		handler := conditionsDeletionLambdaHandler{
			ConditionDAO: conditionParticipantDAO,
		}

		Convey("when the user list is empty returns an error from having an empty list", func() {
			response, err := handler.Handle(context.Background(), &models.CleanConditionsRequest{})
			assert.NotNil(t, err)
			assert.Nil(t, response)
		})

		Convey("when the user list is not empty, but no entries found", func() {
			ctx := context.Background()
			conditionParticipantDAO.On("QueryConditionsByOwnerId", ctx, "user_id", "").Return(nil, "", nil)
			conditionParticipantDAO.On("QueryConditionsByExtensionInstallationChannelId", ctx, "user_id", "").Return(nil, "", nil)
			response, err := handler.Handle(ctx, &models.CleanConditionsRequest{
				UserDeletionRequestData: models.UserDeletionRequestData{
					UserIDs:  []string{"user_id"},
					IsDryRun: dryrun,
				},
			})
			assert.NotNil(t, response)
			assert.Nil(t, err)
		})

		Convey("when the user list is not empty, but dao errors on first call", func() {
			ctx := context.Background()
			conditionParticipantDAO.On("QueryConditionsByOwnerId", ctx, "user_id", "").Return(nil, "", errors.New("error"))
			response, err := handler.Handle(ctx, &models.CleanConditionsRequest{
				UserDeletionRequestData: models.UserDeletionRequestData{
					UserIDs:  []string{"user_id"},
					IsDryRun: dryrun,
				},
			})
			assert.NotNil(t, err)
			assert.Nil(t, response)
		})

		Convey("when the user list is not empty, but dao errors on second call", func() {
			ctx := context.Background()
			conditionParticipantDAO.On("QueryConditionsByOwnerId", ctx, "user_id", "").Return(nil, "", nil)
			conditionParticipantDAO.On("QueryConditionsByExtensionInstallationChannelId", ctx, "user_id", "").Return(nil, "", errors.New("error"))
			response, err := handler.Handle(ctx, &models.CleanConditionsRequest{
				UserDeletionRequestData: models.UserDeletionRequestData{
					UserIDs:  []string{"user_id"},
					IsDryRun: dryrun,
				},
			})
			assert.NotNil(t, err)
			assert.Nil(t, response)
		})

		listOfUsers := []*models.Condition{{OwnerID: "a", Domain: "1", ConditionID: "A"}}
		otherListOfUsers := []*models.Condition{{OwnerID: "z", Domain: "9", ConditionID: "Z"}}

		Convey("when the user list is not empty, and entries are found and deleted", func() {
			ctx := context.Background()
			conditionParticipantDAO.On("QueryConditionsByOwnerId", ctx, "user_id", "").Return(listOfUsers, "", nil)
			conditionParticipantDAO.On("QueryConditionsByExtensionInstallationChannelId", ctx, "user_id", "").Return(otherListOfUsers, "", nil)

			conditionParticipantDAO.On("DeleteCondition", ctx, "1", "a", "A").Return(nil)
			conditionParticipantDAO.On("DeleteCondition", ctx, "9", "z", "Z").Return(nil)
			response, err := handler.Handle(ctx, &models.CleanConditionsRequest{
				UserDeletionRequestData: models.UserDeletionRequestData{
					UserIDs:  []string{"user_id"},
					IsDryRun: dryrun,
				},
			})
			conditionParticipantDAO.AssertNumberOfCalls(t, "DeleteCondition", 2)
			assert.NotNil(t, response)
			assert.Nil(t, err)
		})

		Convey("when the user list is not empty, and entries are found and deleted, but first delete fails", func() {
			ctx := context.Background()
			conditionParticipantDAO.On("QueryConditionsByOwnerId", ctx, "user_id", "").Return(listOfUsers, "", nil)
			conditionParticipantDAO.On("QueryConditionsByExtensionInstallationChannelId", ctx, "user_id", "").Return(otherListOfUsers, "", nil)

			conditionParticipantDAO.On("DeleteCondition", ctx, "1", "a", "A").Return(errors.New("oh no"))
			response, err := handler.Handle(ctx, &models.CleanConditionsRequest{
				UserDeletionRequestData: models.UserDeletionRequestData{
					UserIDs:  []string{"user_id"},
					IsDryRun: dryrun,
				},
			})
			conditionParticipantDAO.AssertNumberOfCalls(t, "DeleteCondition", 1)
			assert.NotNil(t, err)
			assert.Nil(t, response)
		})

		Convey("when the user list is not empty, and entries are found and deleted, but second delete fails", func() {
			ctx := context.Background()
			conditionParticipantDAO.On("QueryConditionsByOwnerId", ctx, "user_id", "").Return(listOfUsers, "", nil)
			conditionParticipantDAO.On("QueryConditionsByExtensionInstallationChannelId", ctx, "user_id", "").Return(otherListOfUsers, "", nil)

			conditionParticipantDAO.On("DeleteCondition", ctx, "1", "a", "A").Return(nil)
			conditionParticipantDAO.On("DeleteCondition", ctx, "9", "z", "Z").Return(errors.New("oh no"))
			response, err := handler.Handle(ctx, &models.CleanConditionsRequest{
				UserDeletionRequestData: models.UserDeletionRequestData{
					UserIDs:  []string{"user_id"},
					IsDryRun: dryrun,
				},
			})

			conditionParticipantDAO.AssertNumberOfCalls(t, "DeleteCondition", 2)
			assert.NotNil(t, err)
			assert.Nil(t, response)
		})

		listOfUsers = append(listOfUsers, &models.Condition{OwnerID: "m", Domain: "4", ConditionID: "M"})

		Convey("when the user list is not empty, multiple entries are found and deleted", func() {
			ctx := context.Background()
			conditionParticipantDAO.On("QueryConditionsByOwnerId", ctx, "user_id", "").Return(listOfUsers, "", nil)
			conditionParticipantDAO.On("QueryConditionsByExtensionInstallationChannelId", ctx, "user_id", "").Return(otherListOfUsers, "", nil)

			conditionParticipantDAO.On("DeleteCondition", ctx, "1", "a", "A").Return(nil)
			conditionParticipantDAO.On("DeleteCondition", ctx, "4", "m", "M").Return(nil)
			conditionParticipantDAO.On("DeleteCondition", ctx, "9", "z", "Z").Return(nil)
			response, err := handler.Handle(ctx, &models.CleanConditionsRequest{
				UserDeletionRequestData: models.UserDeletionRequestData{
					UserIDs:  []string{"user_id"},
					IsDryRun: dryrun,
				},
			})
			conditionParticipantDAO.AssertNumberOfCalls(t, "DeleteCondition", 3)
			assert.NotNil(t, response)
			assert.Nil(t, err)
		})
	})
}
