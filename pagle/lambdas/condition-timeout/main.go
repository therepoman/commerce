// nolint unused, megacheck
package main

import (
	"context"
	"errors"

	conditioncompletor "code.justin.tv/commerce/pagle/backend/condition/complete"

	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend"
	pagleErrors "code.justin.tv/commerce/pagle/backend/errors"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/config"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/joomcode/errorx"
)

type lambdaHandler struct {
	ConditionCompleter conditioncompletor.Completer
}

func (l *lambdaHandler) Handler(ctx context.Context, req models.TimeoutConditionSFNInput) error {
	if strings.Blank(req.ConditionID) {
		return errors.New("missing condition_id")
	}

	if strings.Blank(req.OwnerID) {
		return errors.New("missing owner_id")
	}

	if strings.Blank(req.Domain) {
		return errors.New("missing domain")
	}

	err := l.ConditionCompleter.TerminateCondition(ctx, req.Domain, req.ConditionID, req.OwnerID, models.ConditionExpired)

	if err == nil {
		return nil
	}

	logFields := log.Fields{
		"condition_id": req.ConditionID,
		"owner_id":     req.OwnerID,
		"domain":       req.Domain,
	}

	switch errorx.TypeSwitch(err, pagleErrors.ConditionNotActive) {
	case pagleErrors.ConditionNotActive:
		log.WithFields(logFields).Info("attempting to expire a condition that is no longer active")
		return nil
	default:
		return err
	}
}

func main() {
	env := config.GetEnv()

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		log.WithError(err).Panic("error loading config")
	}

	be, err := backend.NewBackend(cfg)
	if err != nil {
		log.WithError(err).Panic("error initializing backend")
	}
	h := lambdaHandler{
		ConditionCompleter: be.GetConditionCompleter(),
	}

	lambda.Start(h.Handler)
}
