// nolint unused, megacheck
package main

import (
	"context"
	"encoding/json"
	"errors"

	"code.justin.tv/commerce/pagle/backend/condition/effecttotals"

	"code.justin.tv/commerce/gogogadget/aws/sqs/sns"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/config"
	"github.com/aws/aws-lambda-go/lambda"
)

type lambdaHandler struct {
	ConditionAggregatesController effecttotals.EffectTotals
}

func (l *lambdaHandler) Handler(ctx context.Context, sqsEvent models.SQSEvent) error {
	if len(sqsEvent.Records) == 0 {
		return nil
	}

	if len(sqsEvent.Records) != 1 {
		return errors.New("lambda can only handle a batch size of 1")
	}

	snsMsg, err := sns.Extract(&sqsEvent.Records[0])

	if err != nil {
		return err
	}

	message, err := messageFromJSON(snsMsg.Message)

	if err != nil {
		return err
	}

	return l.ConditionAggregatesController.CalculateAndStore(ctx, message.ConditionParticipant)
}

func main() {
	env := config.GetEnv()

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		log.WithError(err).Panic("error loading config")
	}

	be, err := backend.NewBackend(cfg)
	if err != nil {
		log.WithError(err).Panic("error initializing backend")
	}
	h := lambdaHandler{
		ConditionAggregatesController: be.GetConditionAggregatesController(),
	}

	lambda.Start(h.Handler)
}

func messageFromJSON(jsonStr string) (models.ConditionAggregatesRetrySNSMessage, error) {
	var message models.ConditionAggregatesRetrySNSMessage

	err := json.Unmarshal([]byte(jsonStr), &message)

	if err != nil {
		return models.ConditionAggregatesRetrySNSMessage{}, err
	}

	return message, nil
}
