// nolint unused, megacheck
package main

import (
	"context"
	"encoding/json"
	"errors"

	conditionruncompleter "code.justin.tv/commerce/pagle/backend/conditionrun/complete"

	"code.justin.tv/commerce/gogogadget/aws/sqs/sns"
	"code.justin.tv/commerce/gogogadget/strings"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pagle/backend"
	"code.justin.tv/commerce/pagle/backend/models"
	"code.justin.tv/commerce/pagle/config"
	"github.com/aws/aws-lambda-go/lambda"
)

type lambdaHandler struct {
	ConditionRunCompleter conditionruncompleter.Completer
}

func (l *lambdaHandler) Handler(ctx context.Context, sqsEvent models.SQSEvent) error {
	if len(sqsEvent.Records) == 0 {
		return nil
	}

	if len(sqsEvent.Records) != 1 {
		return errors.New("lambda can only handle a batch size of 1")
	}

	snsMsg, err := sns.Extract(&sqsEvent.Records[0])

	if err != nil {
		return err
	}

	message, err := messageFromJSON(snsMsg.Message)

	if err != nil {
		return err
	}

	if strings.Blank(message.ConditionID) {
		return errors.New("missing condition_id")
	}

	if strings.Blank(message.ConditionRunID) {
		return errors.New("missing condition_run_id")
	}

	if strings.Blank(message.OwnerID) {
		return errors.New("missing owner_id")
	}

	if strings.Blank(message.Domain) {
		return errors.New("missing domain")
	}

	log.WithFields(log.Fields{
		"domain":       message.Domain,
		"owner_id":     message.OwnerID,
		"condition_id": message.ConditionID,
	}).Info("starting condition processing work")

	return l.ConditionRunCompleter.ProcessConditionRun(ctx, message.Domain, message.ConditionID, message.ConditionRunID, message.OwnerID)
}

func main() {
	env := config.GetEnv()

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		log.WithError(err).Panic("error loading config")
	}

	be, err := backend.NewBackend(cfg)
	if err != nil {
		log.WithError(err).Panic("error initializing backend")
	}
	h := lambdaHandler{
		ConditionRunCompleter: be.GetConditionRunCompleter(),
	}

	lambda.Start(h.Handler)
}

func messageFromJSON(jsonStr string) (models.ConditionRunProcessingSNSMessage, error) {
	var message models.ConditionRunProcessingSNSMessage

	err := json.Unmarshal([]byte(jsonStr), &message)

	if err != nil {
		return models.ConditionRunProcessingSNSMessage{}, err
	}

	return message, nil
}
