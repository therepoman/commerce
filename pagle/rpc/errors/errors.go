package errors

import (
	"encoding/json"

	"github.com/twitchtv/twirp"
)

const (
	pagleErrorMetaKey = "pagle_error"
)

type PagleError struct {
	ErrorCode string `json:"error_code"`
}

// NewPagleError returns a new ClientError that wraps a twirp.Error and stores
// an error code enum.
func NewPagleError(twErr twirp.Error, pagleError PagleError) twirp.Error {
	b, err := json.Marshal(&pagleError)
	if err != nil {
		return twErr
	}

	return twErr.WithMeta(pagleErrorMetaKey, string(b))
}

// ParsePagleError takes an error, and if it is a PagleError returns the PagleError.
func ParsePagleError(err error) (*PagleError, error) {
	twErr, ok := err.(twirp.Error)
	if !ok {
		return nil, err
	}

	pagleErrValue := twErr.Meta(pagleErrorMetaKey)
	if pagleErrValue == "" {
		return nil, err
	}

	var pagleError PagleError
	jsonErr := json.Unmarshal([]byte(pagleErrValue), &pagleError)

	return &pagleError, jsonErr
}
