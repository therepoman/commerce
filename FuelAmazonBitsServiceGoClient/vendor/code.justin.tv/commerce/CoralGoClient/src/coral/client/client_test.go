package client

import (
	"code.justin.tv/commerce/CoralGoClient/src/coral/dialer"
	"code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	"errors"
	"io"
	"net"
	"strconv"
	"testing"
)

type roundTripHandler func(*codec.Request, io.ReadWriter) error

type testCodec struct {
	handler roundTripHandler
}

func (c *testCodec) RoundTrip(req *codec.Request, rw io.ReadWriter) error {
	return c.handler(req, rw)
}

func forceErrorRoundTrip(req *codec.Request, rw io.ReadWriter) error {
	return errors.New("force error")
}

func forceSuccessRoundTrip(req *codec.Request, rw io.ReadWriter) error {
	return nil
}

func prepareTestDialer(addr net.Addr) (dialer.Dialer, error) {
	host, strPort, err := net.SplitHostPort(addr.String())
	if err != nil {
		return nil, err
	}
	port, err := strconv.Atoi(strPort)
	if err != nil {
		return nil, err
	}
	d, err := dialer.TCP(host, uint16(port))
	if err != nil {
		return nil, err
	}
	return d, nil
}

func prepareTestClient(listener net.Listener, handler roundTripHandler) (Client, error) {
	d, err := prepareTestDialer(listener.Addr())
	if err != nil {
		return nil, err
	}
	c := testCodec{
		handler: handler,
	}
	return NewClient("testAssembly", "testName", d, &c), nil
}

func listenAcceptAndClose(listener net.Listener, t *testing.T) {
	for {
		conn, err := listener.Accept()
		if err != nil {
			t.Error(err)
			return
		}
		err = conn.Close()
		if err != nil {
			t.Error(err)
			return
		}
	}
}

func TestClientErrorHandling(t *testing.T) {
	listener, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		t.Fatal(err)
	}
	defer listener.Close()
	go listenAcceptAndClose(listener, t)

	testCases := []struct {
		handler          roundTripHandler
		errorExpected    bool
		expectedPoolSize int
	}{
		{forceErrorRoundTrip, true, 0},
		{forceSuccessRoundTrip, false, 1},
	}

	for _, testCase := range testCases {
		testClient, err := prepareTestClient(listener, testCase.handler)
		if err != nil {
			t.Fatal(err)
		}
		err = testClient.Call("this", "input", "doesn't", "matter")
		if err == nil && testCase.errorExpected {
			t.Fatal("expected the client to return an error")
		} else if err != nil && !testCase.errorExpected {
			t.Fatal("expected the client to not return an error")
		}
		poolSize := len(testClient.(*client).connPool)
		if poolSize != testCase.expectedPoolSize {
			t.Fatalf("expected the client's connection pool size to be %d but received %d", testCase.expectedPoolSize, poolSize)
		}
	}
}
