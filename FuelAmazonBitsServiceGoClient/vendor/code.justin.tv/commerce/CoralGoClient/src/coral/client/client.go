package client

import (
	"code.justin.tv/commerce/CoralGoClient/src/coral/dialer"
	"code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	"io"
)

type Client interface {
	Call(asmName, operation string, input interface{}, output interface{}) error
}

type OperationRef struct {
	AssemblyName, Name string
}

type ServiceRef struct {
	AssemblyName, ServiceName string
}

type client struct {
	serviceRef codec.ShapeRef
	connPool   chan io.ReadWriter
	dialer     dialer.Dialer
	codec      codec.Codec
}

func NewClient(serviceAssembly, serviceName string, dialer dialer.Dialer, c codec.Codec) Client {
	poolSize := 5
	return &client{
		codec.ShapeRef{serviceAssembly, serviceName},
		make(chan io.ReadWriter, poolSize),
		dialer,
		c,
	}
}

//consume a connection from the pool
//if no connection is available, a new one is created from the dialer
func (c *client) consumeConn() (io.ReadWriter, error) {
	var conn io.ReadWriter
	var err error = nil
	select {
	case conn = <-c.connPool:
	default:
		conn, err = c.dialer.Dial()
	}
	return conn, err
}

//return a conn to the pool
//if the conn pool is full and the ReadWriter is able to be closed, it closes it
func (c *client) returnConn(rw io.ReadWriter) {
	select {
	case c.connPool <- rw:
	default:
		//Pool of connections is full, close it if we can and move on
		if closer, ok := rw.(io.Closer); ok {
			closer.Close()
		}
	}
}

func (c *client) Call(asmName, operation string, in interface{}, out interface{}) error {
	conn, err := c.consumeConn()
	if err != nil {
		return err
	}

	req := &codec.Request{
		Service:   c.serviceRef,
		Operation: codec.ShapeRef{asmName, operation},
		Input:     in,
		Output:    out,
	}
	err = c.codec.RoundTrip(req, conn)

	if err == nil {
		c.returnConn(conn)
	}

	return err
}
