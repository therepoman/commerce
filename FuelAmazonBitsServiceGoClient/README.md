How to Update FABS Go Client in 21 Steps or So
---------------
This makes a few assumptions about your folder structure, change to match your folder structure.

On a dev desktop
================
Building go clients requires something on devdesk's, tooling is not available on Mac for this.

1. Create a new workspace <br> `brazil workspace create --name TwitchGitFarmImporter --root ~/Developer/amazon/TwitchGitFarmImporter --versionset TwitchGitFarmImporter/development`
2. Move into your new workspace <br> `cd ~/Developer/amazon/TwitchGitFarmImporter`
3. Pull down the go client <br> `brazil workspace use --package FuelAmazonBitsServiceGoClient --branch mainline`
4. Move into the directory for the go client <br> `cd src/FuelAmazonBitsServiceGoClient`
5. Build it <br> `brazil-build`
6. Commit changes changes directly to mainline (but do not push) <br> `git commit -a`
7. Push a CR. <br> `cr`
8. Get your PR approved and merge.

On your machine
===============
We'll need to repeat steps 1-4 again on your local machine, on WPA2.

1. Join WPA2.
2. Create a new workspace <br> `brazil workspace create --name TwitchGitFarmImporter --root ~/Developer/amazon/TwitchGitFarmImporter --versionset TwitchGitFarmImporter/development`
3. Move into your new workspace <br> `cd ~/Developer/amazon/TwitchGitFarmImporter`
4. Pull down the go client <br> `brazil workspace use --package FuelAmazonBitsServiceGoClient --branch mainline`
5. Move into the directory for the go client <br> `cd src/FuelAmazonBitsServiceGoClient`
6. Switch to Twitch VPN or Enable Teleport-Bastion
7. Remove remotes <br> `git remote rm share` <br> `git remote rm origin` <br> `git remote rm backup` 
8. Add twitch git as your new origin. <br> `git remote set-url origin git@git-aws.internal.justin.tv:commerce/FuelAmazonBitsServiceGoClient.git`
9. Make a new branch <br> `git checkout -b updates`
10. Run Twitchify.sh script <br> `./twitchify.sh`
11. Git changes <br> `git commit -a`
12. Push branch <br> `git push --set-upstream origin updates`
13. Go to [twitch github](https://git-aws.internal.justin.tv/commerce/FuelAmazonBitsServiceGoClient) and create a PR, get it approved and merged.
14. After approval and merge you'll want to clean up your workspace <br> `cd ~` <br> `rm -rf ~/Developer/amazon/TwitchGitFarmImporter`.