package com_amazon_fuelamazonbitsservice

import (
	__model__ "code.justin.tv/commerce/CoralGoModel/src/coral/model"
	__reflect__ "reflect"
	__time__ "time"
)

func init() {
	var val *bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *bool
		if f, ok := from.Interface().(*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to IsAlive")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *bool
		if f, ok := from.Interface().(*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Boolean")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *__time__.Time
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *__time__.Time
		if f, ok := from.Interface().(*__time__.Time); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Timestamp")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int32
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int32
		if f, ok := from.Interface().(*int32); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Integer")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ErrorMessage")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to RequiredString")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to NullableString")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to MarketplaceId")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to BitsAccountTypeStrings")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

type ViewBitsTransactionsRequest interface {
	__type()
	SetIteratorKey(v *string)
	IteratorKey() *string
	SetMaxResultsPerPage(v *int32)
	MaxResultsPerPage() *int32
	SetTwitchUserId(v *string)
	TwitchUserId() *string
}
type _ViewBitsTransactionsRequest struct {
	Ị_iteratorKey       *string `coral:"iteratorKey" json:"iteratorKey"`
	Ị_maxResultsPerPage *int32  `coral:"maxResultsPerPage" json:"maxResultsPerPage"`
	Ị_twitchUserId      *string `coral:"twitchUserId" json:"twitchUserId"`
}

func (this *_ViewBitsTransactionsRequest) TwitchUserId() *string {
	return this.Ị_twitchUserId
}
func (this *_ViewBitsTransactionsRequest) SetTwitchUserId(v *string) {
	this.Ị_twitchUserId = v
}
func (this *_ViewBitsTransactionsRequest) IteratorKey() *string {
	return this.Ị_iteratorKey
}
func (this *_ViewBitsTransactionsRequest) SetIteratorKey(v *string) {
	this.Ị_iteratorKey = v
}
func (this *_ViewBitsTransactionsRequest) MaxResultsPerPage() *int32 {
	return this.Ị_maxResultsPerPage
}
func (this *_ViewBitsTransactionsRequest) SetMaxResultsPerPage(v *int32) {
	this.Ị_maxResultsPerPage = v
}
func (this *_ViewBitsTransactionsRequest) __type() {
}
func NewViewBitsTransactionsRequest() ViewBitsTransactionsRequest {
	return &_ViewBitsTransactionsRequest{}
}
func init() {
	var val ViewBitsTransactionsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("ViewBitsTransactionsRequest", t, func() interface{} {
		return NewViewBitsTransactionsRequest()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("ViewBitsTransactionsRequest", t, func() interface{} {
		return NewViewBitsTransactionsRequest()
	})
}

type DuplicateTransactionException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _DuplicateTransactionException struct {
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_DuplicateTransactionException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_DuplicateTransactionException) Message() *string {
	return this.Ị_message
}
func (this *_DuplicateTransactionException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_DuplicateTransactionException) __type() {
}
func NewDuplicateTransactionException() DuplicateTransactionException {
	return &_DuplicateTransactionException{}
}
func init() {
	var val DuplicateTransactionException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("DuplicateTransactionException", t, func() interface{} {
		return NewDuplicateTransactionException()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("DuplicateTransactionException", t, func() interface{} {
		return NewDuplicateTransactionException()
	})
}

type GetBitsBalanceResult interface {
	__type()
	SetMaximumBitsInventoryLimit(v *int32)
	MaximumBitsInventoryLimit() *int32
	SetMostRecentPurchaseBySuperPlatformMap(v map[string]*__time__.Time)
	MostRecentPurchaseBySuperPlatformMap() map[string]*__time__.Time
	SetActiveMarketplaceList(v []*string)
	ActiveMarketplaceList() []*string
	SetCanPurchase(v map[string]*bool)
	CanPurchase() map[string]*bool
	SetCantPurchaseReason(v map[string]*string)
	CantPurchaseReason() map[string]*string
	SetProductInformation(v map[string]BitsProductInformation)
	ProductInformation() map[string]BitsProductInformation
	SetTotalBits(v *int32)
	TotalBits() *int32
	SetTotalBitsWithBroadcaster(v *int32)
	TotalBitsWithBroadcaster() *int32
}
type _GetBitsBalanceResult struct {
	Ị_maximumBitsInventoryLimit            *int32                            `coral:"maximumBitsInventoryLimit" json:"maximumBitsInventoryLimit"`
	Ị_mostRecentPurchaseBySuperPlatformMap map[string]*__time__.Time         `coral:"mostRecentPurchaseBySuperPlatformMap" json:"mostRecentPurchaseBySuperPlatformMap"`
	Ị_activeMarketplaceList                []*string                         `coral:"activeMarketplaceList" json:"activeMarketplaceList"`
	Ị_canPurchase                          map[string]*bool                  `coral:"canPurchase" json:"canPurchase"`
	Ị_cantPurchaseReason                   map[string]*string                `coral:"cantPurchaseReason" json:"cantPurchaseReason"`
	Ị_productInformation                   map[string]BitsProductInformation `coral:"productInformation" json:"productInformation"`
	Ị_totalBits                            *int32                            `coral:"totalBits" json:"totalBits"`
	Ị_totalBitsWithBroadcaster             *int32                            `coral:"totalBitsWithBroadcaster" json:"totalBitsWithBroadcaster"`
}

func (this *_GetBitsBalanceResult) TotalBits() *int32 {
	return this.Ị_totalBits
}
func (this *_GetBitsBalanceResult) SetTotalBits(v *int32) {
	this.Ị_totalBits = v
}
func (this *_GetBitsBalanceResult) TotalBitsWithBroadcaster() *int32 {
	return this.Ị_totalBitsWithBroadcaster
}
func (this *_GetBitsBalanceResult) SetTotalBitsWithBroadcaster(v *int32) {
	this.Ị_totalBitsWithBroadcaster = v
}
func (this *_GetBitsBalanceResult) MaximumBitsInventoryLimit() *int32 {
	return this.Ị_maximumBitsInventoryLimit
}
func (this *_GetBitsBalanceResult) SetMaximumBitsInventoryLimit(v *int32) {
	this.Ị_maximumBitsInventoryLimit = v
}
func (this *_GetBitsBalanceResult) MostRecentPurchaseBySuperPlatformMap() map[string]*__time__.Time {
	return this.Ị_mostRecentPurchaseBySuperPlatformMap
}
func (this *_GetBitsBalanceResult) SetMostRecentPurchaseBySuperPlatformMap(v map[string]*__time__.Time) {
	this.Ị_mostRecentPurchaseBySuperPlatformMap = v
}
func (this *_GetBitsBalanceResult) ActiveMarketplaceList() []*string {
	return this.Ị_activeMarketplaceList
}
func (this *_GetBitsBalanceResult) SetActiveMarketplaceList(v []*string) {
	this.Ị_activeMarketplaceList = v
}
func (this *_GetBitsBalanceResult) CanPurchase() map[string]*bool {
	return this.Ị_canPurchase
}
func (this *_GetBitsBalanceResult) SetCanPurchase(v map[string]*bool) {
	this.Ị_canPurchase = v
}
func (this *_GetBitsBalanceResult) CantPurchaseReason() map[string]*string {
	return this.Ị_cantPurchaseReason
}
func (this *_GetBitsBalanceResult) SetCantPurchaseReason(v map[string]*string) {
	this.Ị_cantPurchaseReason = v
}
func (this *_GetBitsBalanceResult) ProductInformation() map[string]BitsProductInformation {
	return this.Ị_productInformation
}
func (this *_GetBitsBalanceResult) SetProductInformation(v map[string]BitsProductInformation) {
	this.Ị_productInformation = v
}
func (this *_GetBitsBalanceResult) __type() {
}
func NewGetBitsBalanceResult() GetBitsBalanceResult {
	return &_GetBitsBalanceResult{}
}
func init() {
	var val GetBitsBalanceResult
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("GetBitsBalanceResult", t, func() interface{} {
		return NewGetBitsBalanceResult()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("GetBitsBalanceResult", t, func() interface{} {
		return NewGetBitsBalanceResult()
	})
}

type GiveBitsToBroadcasterRequest interface {
	__type()
	SetSourceChannelStatus(v *string)
	SourceChannelStatus() *string
	SetExtraContext(v *string)
	ExtraContext() *string
	SetAnonymous(v *bool)
	Anonymous() *bool
	SetSenderTwitchUserId(v *string)
	SenderTwitchUserId() *string
	SetBitsAmount(v *int32)
	BitsAmount() *int32
	SetSponsoredAmounts(v map[string]*int32)
	SponsoredAmounts() map[string]*int32
	SetTransactionId(v *string)
	TransactionId() *string
	SetReceiverTwitchUserId(v *string)
	ReceiverTwitchUserId() *string
	SetPublicMessage(v *string)
	PublicMessage() *string
	SetEventTypeId(v *string)
	EventTypeId() *string
}
type _GiveBitsToBroadcasterRequest struct {
	Ị_sponsoredAmounts     map[string]*int32 `coral:"sponsoredAmounts" json:"sponsoredAmounts"`
	Ị_transactionId        *string           `coral:"transactionId" json:"transactionId"`
	Ị_sourceChannelStatus  *string           `coral:"sourceChannelStatus" json:"sourceChannelStatus"`
	Ị_extraContext         *string           `coral:"extraContext" json:"extraContext"`
	Ị_anonymous            *bool             `coral:"anonymous" json:"anonymous"`
	Ị_senderTwitchUserId   *string           `coral:"senderTwitchUserId" json:"senderTwitchUserId"`
	Ị_bitsAmount           *int32            `coral:"bitsAmount" json:"bitsAmount"`
	Ị_eventTypeId          *string           `coral:"eventTypeId" json:"eventTypeId"`
	Ị_receiverTwitchUserId *string           `coral:"receiverTwitchUserId" json:"receiverTwitchUserId"`
	Ị_publicMessage        *string           `coral:"publicMessage" json:"publicMessage"`
}

func (this *_GiveBitsToBroadcasterRequest) ReceiverTwitchUserId() *string {
	return this.Ị_receiverTwitchUserId
}
func (this *_GiveBitsToBroadcasterRequest) SetReceiverTwitchUserId(v *string) {
	this.Ị_receiverTwitchUserId = v
}
func (this *_GiveBitsToBroadcasterRequest) PublicMessage() *string {
	return this.Ị_publicMessage
}
func (this *_GiveBitsToBroadcasterRequest) SetPublicMessage(v *string) {
	this.Ị_publicMessage = v
}
func (this *_GiveBitsToBroadcasterRequest) EventTypeId() *string {
	return this.Ị_eventTypeId
}
func (this *_GiveBitsToBroadcasterRequest) SetEventTypeId(v *string) {
	this.Ị_eventTypeId = v
}
func (this *_GiveBitsToBroadcasterRequest) SenderTwitchUserId() *string {
	return this.Ị_senderTwitchUserId
}
func (this *_GiveBitsToBroadcasterRequest) SetSenderTwitchUserId(v *string) {
	this.Ị_senderTwitchUserId = v
}
func (this *_GiveBitsToBroadcasterRequest) BitsAmount() *int32 {
	return this.Ị_bitsAmount
}
func (this *_GiveBitsToBroadcasterRequest) SetBitsAmount(v *int32) {
	this.Ị_bitsAmount = v
}
func (this *_GiveBitsToBroadcasterRequest) SponsoredAmounts() map[string]*int32 {
	return this.Ị_sponsoredAmounts
}
func (this *_GiveBitsToBroadcasterRequest) SetSponsoredAmounts(v map[string]*int32) {
	this.Ị_sponsoredAmounts = v
}
func (this *_GiveBitsToBroadcasterRequest) TransactionId() *string {
	return this.Ị_transactionId
}
func (this *_GiveBitsToBroadcasterRequest) SetTransactionId(v *string) {
	this.Ị_transactionId = v
}
func (this *_GiveBitsToBroadcasterRequest) SourceChannelStatus() *string {
	return this.Ị_sourceChannelStatus
}
func (this *_GiveBitsToBroadcasterRequest) SetSourceChannelStatus(v *string) {
	this.Ị_sourceChannelStatus = v
}
func (this *_GiveBitsToBroadcasterRequest) ExtraContext() *string {
	return this.Ị_extraContext
}
func (this *_GiveBitsToBroadcasterRequest) SetExtraContext(v *string) {
	this.Ị_extraContext = v
}
func (this *_GiveBitsToBroadcasterRequest) Anonymous() *bool {
	return this.Ị_anonymous
}
func (this *_GiveBitsToBroadcasterRequest) SetAnonymous(v *bool) {
	this.Ị_anonymous = v
}
func (this *_GiveBitsToBroadcasterRequest) __type() {
}
func NewGiveBitsToBroadcasterRequest() GiveBitsToBroadcasterRequest {
	return &_GiveBitsToBroadcasterRequest{}
}
func init() {
	var val GiveBitsToBroadcasterRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("GiveBitsToBroadcasterRequest", t, func() interface{} {
		return NewGiveBitsToBroadcasterRequest()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("GiveBitsToBroadcasterRequest", t, func() interface{} {
		return NewGiveBitsToBroadcasterRequest()
	})
}

type UseBitsOnExtensionRequest interface {
	__type()
	SetExtensionId(v *string)
	ExtensionId() *string
	SetSKU(v *string)
	SKU() *string
	SetTransactionId(v *string)
	TransactionId() *string
	SetPollId(v *string)
	PollId() *string
	SetSenderTwitchUserId(v *string)
	SenderTwitchUserId() *string
	SetReceiverTwitchUserId(v *string)
	ReceiverTwitchUserId() *string
	SetBitsAmount(v *int32)
	BitsAmount() *int32
	SetExtraContext(v *string)
	ExtraContext() *string
	SetPollChoiceId(v *string)
	PollChoiceId() *string
}
type _UseBitsOnExtensionRequest struct {
	Ị_senderTwitchUserId   *string `coral:"senderTwitchUserId" json:"senderTwitchUserId"`
	Ị_receiverTwitchUserId *string `coral:"receiverTwitchUserId" json:"receiverTwitchUserId"`
	Ị_bitsAmount           *int32  `coral:"bitsAmount" json:"bitsAmount"`
	Ị_extraContext         *string `coral:"extraContext" json:"extraContext"`
	Ị_pollChoiceId         *string `coral:"pollChoiceId" json:"pollChoiceId"`
	Ị_extensionId          *string `coral:"extensionId" json:"extensionId"`
	Ị_SKU                  *string `coral:"SKU" json:"SKU"`
	Ị_transactionId        *string `coral:"transactionId" json:"transactionId"`
	Ị_pollId               *string `coral:"pollId" json:"pollId"`
}

func (this *_UseBitsOnExtensionRequest) PollChoiceId() *string {
	return this.Ị_pollChoiceId
}
func (this *_UseBitsOnExtensionRequest) SetPollChoiceId(v *string) {
	this.Ị_pollChoiceId = v
}
func (this *_UseBitsOnExtensionRequest) SenderTwitchUserId() *string {
	return this.Ị_senderTwitchUserId
}
func (this *_UseBitsOnExtensionRequest) SetSenderTwitchUserId(v *string) {
	this.Ị_senderTwitchUserId = v
}
func (this *_UseBitsOnExtensionRequest) ReceiverTwitchUserId() *string {
	return this.Ị_receiverTwitchUserId
}
func (this *_UseBitsOnExtensionRequest) SetReceiverTwitchUserId(v *string) {
	this.Ị_receiverTwitchUserId = v
}
func (this *_UseBitsOnExtensionRequest) BitsAmount() *int32 {
	return this.Ị_bitsAmount
}
func (this *_UseBitsOnExtensionRequest) SetBitsAmount(v *int32) {
	this.Ị_bitsAmount = v
}
func (this *_UseBitsOnExtensionRequest) ExtraContext() *string {
	return this.Ị_extraContext
}
func (this *_UseBitsOnExtensionRequest) SetExtraContext(v *string) {
	this.Ị_extraContext = v
}
func (this *_UseBitsOnExtensionRequest) ExtensionId() *string {
	return this.Ị_extensionId
}
func (this *_UseBitsOnExtensionRequest) SetExtensionId(v *string) {
	this.Ị_extensionId = v
}
func (this *_UseBitsOnExtensionRequest) SKU() *string {
	return this.Ị_SKU
}
func (this *_UseBitsOnExtensionRequest) SetSKU(v *string) {
	this.Ị_SKU = v
}
func (this *_UseBitsOnExtensionRequest) TransactionId() *string {
	return this.Ị_transactionId
}
func (this *_UseBitsOnExtensionRequest) SetTransactionId(v *string) {
	this.Ị_transactionId = v
}
func (this *_UseBitsOnExtensionRequest) PollId() *string {
	return this.Ị_pollId
}
func (this *_UseBitsOnExtensionRequest) SetPollId(v *string) {
	this.Ị_pollId = v
}
func (this *_UseBitsOnExtensionRequest) __type() {
}
func NewUseBitsOnExtensionRequest() UseBitsOnExtensionRequest {
	return &_UseBitsOnExtensionRequest{}
}
func init() {
	var val UseBitsOnExtensionRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("UseBitsOnExtensionRequest", t, func() interface{} {
		return NewUseBitsOnExtensionRequest()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("UseBitsOnExtensionRequest", t, func() interface{} {
		return NewUseBitsOnExtensionRequest()
	})
}

type UseBitsOnExtensionResult interface {
	__type()
	SetUpdatedTotal(v *int32)
	UpdatedTotal() *int32
}
type _UseBitsOnExtensionResult struct {
	Ị_updatedTotal *int32 `coral:"updatedTotal" json:"updatedTotal"`
}

func (this *_UseBitsOnExtensionResult) UpdatedTotal() *int32 {
	return this.Ị_updatedTotal
}
func (this *_UseBitsOnExtensionResult) SetUpdatedTotal(v *int32) {
	this.Ị_updatedTotal = v
}
func (this *_UseBitsOnExtensionResult) __type() {
}
func NewUseBitsOnExtensionResult() UseBitsOnExtensionResult {
	return &_UseBitsOnExtensionResult{}
}
func init() {
	var val UseBitsOnExtensionResult
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("UseBitsOnExtensionResult", t, func() interface{} {
		return NewUseBitsOnExtensionResult()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("UseBitsOnExtensionResult", t, func() interface{} {
		return NewUseBitsOnExtensionResult()
	})
}

//This exception is thrown on a database (or other dependency) error
type DependencyFailureException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _DependencyFailureException struct {
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_DependencyFailureException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_DependencyFailureException) Message() *string {
	return this.Ị_message
}
func (this *_DependencyFailureException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_DependencyFailureException) __type() {
}
func NewDependencyFailureException() DependencyFailureException {
	return &_DependencyFailureException{}
}
func init() {
	var val DependencyFailureException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("DependencyFailureException", t, func() interface{} {
		return NewDependencyFailureException()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("DependencyFailureException", t, func() interface{} {
		return NewDependencyFailureException()
	})
}

type InvalidInputException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _InvalidInputException struct {
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_InvalidInputException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_InvalidInputException) Message() *string {
	return this.Ị_message
}
func (this *_InvalidInputException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_InvalidInputException) __type() {
}
func NewInvalidInputException() InvalidInputException {
	return &_InvalidInputException{}
}
func init() {
	var val InvalidInputException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("InvalidInputException", t, func() interface{} {
		return NewInvalidInputException()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("InvalidInputException", t, func() interface{} {
		return NewInvalidInputException()
	})
}

type GetBitsBalanceAndPurchasableAsinsResult interface {
	__type()
	SetCantPurchaseReason(v map[string]*string)
	CantPurchaseReason() map[string]*string
	SetTotalBits(v *int32)
	TotalBits() *int32
	SetCanPurchaseTargetAsin(v *bool)
	CanPurchaseTargetAsin() *bool
	SetCantPurchaseTargetAsinReason(v *string)
	CantPurchaseTargetAsinReason() *string
	SetTargetAsinPurchasableQuantity(v *int32)
	TargetAsinPurchasableQuantity() *int32
	SetActiveMarketplaceList(v []*string)
	ActiveMarketplaceList() []*string
	SetCanPurchase(v map[string]*bool)
	CanPurchase() map[string]*bool
}
type _GetBitsBalanceAndPurchasableAsinsResult struct {
	Ị_totalBits                     *int32             `coral:"totalBits" json:"totalBits"`
	Ị_canPurchaseTargetAsin         *bool              `coral:"canPurchaseTargetAsin" json:"canPurchaseTargetAsin"`
	Ị_cantPurchaseTargetAsinReason  *string            `coral:"cantPurchaseTargetAsinReason" json:"cantPurchaseTargetAsinReason"`
	Ị_targetAsinPurchasableQuantity *int32             `coral:"targetAsinPurchasableQuantity" json:"targetAsinPurchasableQuantity"`
	Ị_activeMarketplaceList         []*string          `coral:"activeMarketplaceList" json:"activeMarketplaceList"`
	Ị_canPurchase                   map[string]*bool   `coral:"canPurchase" json:"canPurchase"`
	Ị_cantPurchaseReason            map[string]*string `coral:"cantPurchaseReason" json:"cantPurchaseReason"`
}

func (this *_GetBitsBalanceAndPurchasableAsinsResult) CantPurchaseTargetAsinReason() *string {
	return this.Ị_cantPurchaseTargetAsinReason
}
func (this *_GetBitsBalanceAndPurchasableAsinsResult) SetCantPurchaseTargetAsinReason(v *string) {
	this.Ị_cantPurchaseTargetAsinReason = v
}
func (this *_GetBitsBalanceAndPurchasableAsinsResult) TargetAsinPurchasableQuantity() *int32 {
	return this.Ị_targetAsinPurchasableQuantity
}
func (this *_GetBitsBalanceAndPurchasableAsinsResult) SetTargetAsinPurchasableQuantity(v *int32) {
	this.Ị_targetAsinPurchasableQuantity = v
}
func (this *_GetBitsBalanceAndPurchasableAsinsResult) ActiveMarketplaceList() []*string {
	return this.Ị_activeMarketplaceList
}
func (this *_GetBitsBalanceAndPurchasableAsinsResult) SetActiveMarketplaceList(v []*string) {
	this.Ị_activeMarketplaceList = v
}
func (this *_GetBitsBalanceAndPurchasableAsinsResult) CanPurchase() map[string]*bool {
	return this.Ị_canPurchase
}
func (this *_GetBitsBalanceAndPurchasableAsinsResult) SetCanPurchase(v map[string]*bool) {
	this.Ị_canPurchase = v
}
func (this *_GetBitsBalanceAndPurchasableAsinsResult) CantPurchaseReason() map[string]*string {
	return this.Ị_cantPurchaseReason
}
func (this *_GetBitsBalanceAndPurchasableAsinsResult) SetCantPurchaseReason(v map[string]*string) {
	this.Ị_cantPurchaseReason = v
}
func (this *_GetBitsBalanceAndPurchasableAsinsResult) TotalBits() *int32 {
	return this.Ị_totalBits
}
func (this *_GetBitsBalanceAndPurchasableAsinsResult) SetTotalBits(v *int32) {
	this.Ị_totalBits = v
}
func (this *_GetBitsBalanceAndPurchasableAsinsResult) CanPurchaseTargetAsin() *bool {
	return this.Ị_canPurchaseTargetAsin
}
func (this *_GetBitsBalanceAndPurchasableAsinsResult) SetCanPurchaseTargetAsin(v *bool) {
	this.Ị_canPurchaseTargetAsin = v
}
func (this *_GetBitsBalanceAndPurchasableAsinsResult) __type() {
}
func NewGetBitsBalanceAndPurchasableAsinsResult() GetBitsBalanceAndPurchasableAsinsResult {
	return &_GetBitsBalanceAndPurchasableAsinsResult{}
}
func init() {
	var val GetBitsBalanceAndPurchasableAsinsResult
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("GetBitsBalanceAndPurchasableAsinsResult", t, func() interface{} {
		return NewGetBitsBalanceAndPurchasableAsinsResult()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("GetBitsBalanceAndPurchasableAsinsResult", t, func() interface{} {
		return NewGetBitsBalanceAndPurchasableAsinsResult()
	})
}

type AdminRemoveBitsRequest interface {
	__type()
	SetAmountOfBitsToRemove(v *int32)
	AmountOfBitsToRemove() *int32
	SetTransactionId(v *string)
	TransactionId() *string
	SetTwitchUserId(v *string)
	TwitchUserId() *string
	SetAdminUserId(v *string)
	AdminUserId() *string
	SetAdminReason(v *string)
	AdminReason() *string
	SetRelatedTransactionId(v *string)
	RelatedTransactionId() *string
	SetExtraContext(v *string)
	ExtraContext() *string
	SetEventTypeId(v *string)
	EventTypeId() *string
}
type _AdminRemoveBitsRequest struct {
	Ị_transactionId        *string `coral:"transactionId" json:"transactionId"`
	Ị_twitchUserId         *string `coral:"twitchUserId" json:"twitchUserId"`
	Ị_adminUserId          *string `coral:"adminUserId" json:"adminUserId"`
	Ị_adminReason          *string `coral:"adminReason" json:"adminReason"`
	Ị_relatedTransactionId *string `coral:"relatedTransactionId" json:"relatedTransactionId"`
	Ị_extraContext         *string `coral:"extraContext" json:"extraContext"`
	Ị_eventTypeId          *string `coral:"eventTypeId" json:"eventTypeId"`
	Ị_amountOfBitsToRemove *int32  `coral:"amountOfBitsToRemove" json:"amountOfBitsToRemove"`
}

func (this *_AdminRemoveBitsRequest) EventTypeId() *string {
	return this.Ị_eventTypeId
}
func (this *_AdminRemoveBitsRequest) SetEventTypeId(v *string) {
	this.Ị_eventTypeId = v
}
func (this *_AdminRemoveBitsRequest) AmountOfBitsToRemove() *int32 {
	return this.Ị_amountOfBitsToRemove
}
func (this *_AdminRemoveBitsRequest) SetAmountOfBitsToRemove(v *int32) {
	this.Ị_amountOfBitsToRemove = v
}
func (this *_AdminRemoveBitsRequest) TransactionId() *string {
	return this.Ị_transactionId
}
func (this *_AdminRemoveBitsRequest) SetTransactionId(v *string) {
	this.Ị_transactionId = v
}
func (this *_AdminRemoveBitsRequest) TwitchUserId() *string {
	return this.Ị_twitchUserId
}
func (this *_AdminRemoveBitsRequest) SetTwitchUserId(v *string) {
	this.Ị_twitchUserId = v
}
func (this *_AdminRemoveBitsRequest) AdminUserId() *string {
	return this.Ị_adminUserId
}
func (this *_AdminRemoveBitsRequest) SetAdminUserId(v *string) {
	this.Ị_adminUserId = v
}
func (this *_AdminRemoveBitsRequest) AdminReason() *string {
	return this.Ị_adminReason
}
func (this *_AdminRemoveBitsRequest) SetAdminReason(v *string) {
	this.Ị_adminReason = v
}
func (this *_AdminRemoveBitsRequest) RelatedTransactionId() *string {
	return this.Ị_relatedTransactionId
}
func (this *_AdminRemoveBitsRequest) SetRelatedTransactionId(v *string) {
	this.Ị_relatedTransactionId = v
}
func (this *_AdminRemoveBitsRequest) ExtraContext() *string {
	return this.Ị_extraContext
}
func (this *_AdminRemoveBitsRequest) SetExtraContext(v *string) {
	this.Ị_extraContext = v
}
func (this *_AdminRemoveBitsRequest) __type() {
}
func NewAdminRemoveBitsRequest() AdminRemoveBitsRequest {
	return &_AdminRemoveBitsRequest{}
}
func init() {
	var val AdminRemoveBitsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("AdminRemoveBitsRequest", t, func() interface{} {
		return NewAdminRemoveBitsRequest()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("AdminRemoveBitsRequest", t, func() interface{} {
		return NewAdminRemoveBitsRequest()
	})
}

type BitsProductInformation interface {
	__type()
	SetPromoType(v *string)
	PromoType() *string
	SetPromoId(v *string)
	PromoId() *string
	SetPurchasableQuantity(v *int32)
	PurchasableQuantity() *int32
	SetPlatform(v *string)
	Platform() *string
	SetPlatforms(v []*string)
	Platforms() []*string
	SetNumberOfBits(v *int32)
	NumberOfBits() *int32
	SetPromotional(v *bool)
	Promotional() *bool
}
type _BitsProductInformation struct {
	Ị_platform            *string   `coral:"platform" json:"platform"`
	Ị_platforms           []*string `coral:"platforms" json:"platforms"`
	Ị_numberOfBits        *int32    `coral:"numberOfBits" json:"numberOfBits"`
	Ị_promotional         *bool     `coral:"promotional" json:"promotional"`
	Ị_promoType           *string   `coral:"promoType" json:"promoType"`
	Ị_promoId             *string   `coral:"promoId" json:"promoId"`
	Ị_purchasableQuantity *int32    `coral:"purchasableQuantity" json:"purchasableQuantity"`
}

func (this *_BitsProductInformation) NumberOfBits() *int32 {
	return this.Ị_numberOfBits
}
func (this *_BitsProductInformation) SetNumberOfBits(v *int32) {
	this.Ị_numberOfBits = v
}
func (this *_BitsProductInformation) Promotional() *bool {
	return this.Ị_promotional
}
func (this *_BitsProductInformation) SetPromotional(v *bool) {
	this.Ị_promotional = v
}
func (this *_BitsProductInformation) PromoType() *string {
	return this.Ị_promoType
}
func (this *_BitsProductInformation) SetPromoType(v *string) {
	this.Ị_promoType = v
}
func (this *_BitsProductInformation) PromoId() *string {
	return this.Ị_promoId
}
func (this *_BitsProductInformation) SetPromoId(v *string) {
	this.Ị_promoId = v
}
func (this *_BitsProductInformation) PurchasableQuantity() *int32 {
	return this.Ị_purchasableQuantity
}
func (this *_BitsProductInformation) SetPurchasableQuantity(v *int32) {
	this.Ị_purchasableQuantity = v
}
func (this *_BitsProductInformation) Platform() *string {
	return this.Ị_platform
}
func (this *_BitsProductInformation) SetPlatform(v *string) {
	this.Ị_platform = v
}
func (this *_BitsProductInformation) Platforms() []*string {
	return this.Ị_platforms
}
func (this *_BitsProductInformation) SetPlatforms(v []*string) {
	this.Ị_platforms = v
}
func (this *_BitsProductInformation) __type() {
}
func NewBitsProductInformation() BitsProductInformation {
	return &_BitsProductInformation{}
}
func init() {
	var val BitsProductInformation
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("BitsProductInformation", t, func() interface{} {
		return NewBitsProductInformation()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("BitsProductInformation", t, func() interface{} {
		return NewBitsProductInformation()
	})
}

type ViewBitsTransactionsResult interface {
	__type()
	SetIteratorKey(v *string)
	IteratorKey() *string
	SetTransactions(v []Transaction)
	Transactions() []Transaction
}
type _ViewBitsTransactionsResult struct {
	Ị_iteratorKey  *string       `coral:"iteratorKey" json:"iteratorKey"`
	Ị_transactions []Transaction `coral:"transactions" json:"transactions"`
}

func (this *_ViewBitsTransactionsResult) IteratorKey() *string {
	return this.Ị_iteratorKey
}
func (this *_ViewBitsTransactionsResult) SetIteratorKey(v *string) {
	this.Ị_iteratorKey = v
}
func (this *_ViewBitsTransactionsResult) Transactions() []Transaction {
	return this.Ị_transactions
}
func (this *_ViewBitsTransactionsResult) SetTransactions(v []Transaction) {
	this.Ị_transactions = v
}
func (this *_ViewBitsTransactionsResult) __type() {
}
func NewViewBitsTransactionsResult() ViewBitsTransactionsResult {
	return &_ViewBitsTransactionsResult{}
}
func init() {
	var val ViewBitsTransactionsResult
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("ViewBitsTransactionsResult", t, func() interface{} {
		return NewViewBitsTransactionsResult()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("ViewBitsTransactionsResult", t, func() interface{} {
		return NewViewBitsTransactionsResult()
	})
}

type AddBitsRequest interface {
	__type()
	SetSKU(v *string)
	SKU() *string
	SetSkuQuantity(v *int32)
	SkuQuantity() *int32
	SetPurchasePrice(v *string)
	PurchasePrice() *string
	SetPurchasePriceCurrency(v *string)
	PurchasePriceCurrency() *string
	SetTransactionId(v *string)
	TransactionId() *string
	SetTwitchUserId(v *string)
	TwitchUserId() *string
	SetExtraContext(v *string)
	ExtraContext() *string
	SetAmountOfBitsToAdd(v *int32)
	AmountOfBitsToAdd() *int32
	SetTypeOfBitsToAddEnumeration(v *string)
	TypeOfBitsToAddEnumeration() *string
	SetPlatform(v *string)
	Platform() *string
	SetEventTypeId(v *string)
	EventTypeId() *string
}
type _AddBitsRequest struct {
	Ị_purchasePriceCurrency      *string `coral:"purchasePriceCurrency" json:"purchasePriceCurrency"`
	Ị_transactionId              *string `coral:"transactionId" json:"transactionId"`
	Ị_twitchUserId               *string `coral:"twitchUserId" json:"twitchUserId"`
	Ị_extraContext               *string `coral:"extraContext" json:"extraContext"`
	Ị_amountOfBitsToAdd          *int32  `coral:"amountOfBitsToAdd" json:"amountOfBitsToAdd"`
	Ị_typeOfBitsToAddEnumeration *string `coral:"typeOfBitsToAddEnumeration" json:"typeOfBitsToAddEnumeration"`
	Ị_platform                   *string `coral:"platform" json:"platform"`
	Ị_purchasePrice              *string `coral:"purchasePrice" json:"purchasePrice"`
	Ị_eventTypeId                *string `coral:"eventTypeId" json:"eventTypeId"`
	Ị_SKU                        *string `coral:"SKU" json:"SKU"`
	Ị_skuQuantity                *int32  `coral:"skuQuantity" json:"skuQuantity"`
}

func (this *_AddBitsRequest) ExtraContext() *string {
	return this.Ị_extraContext
}
func (this *_AddBitsRequest) SetExtraContext(v *string) {
	this.Ị_extraContext = v
}
func (this *_AddBitsRequest) AmountOfBitsToAdd() *int32 {
	return this.Ị_amountOfBitsToAdd
}
func (this *_AddBitsRequest) SetAmountOfBitsToAdd(v *int32) {
	this.Ị_amountOfBitsToAdd = v
}
func (this *_AddBitsRequest) TypeOfBitsToAddEnumeration() *string {
	return this.Ị_typeOfBitsToAddEnumeration
}
func (this *_AddBitsRequest) SetTypeOfBitsToAddEnumeration(v *string) {
	this.Ị_typeOfBitsToAddEnumeration = v
}
func (this *_AddBitsRequest) Platform() *string {
	return this.Ị_platform
}
func (this *_AddBitsRequest) SetPlatform(v *string) {
	this.Ị_platform = v
}
func (this *_AddBitsRequest) PurchasePrice() *string {
	return this.Ị_purchasePrice
}
func (this *_AddBitsRequest) SetPurchasePrice(v *string) {
	this.Ị_purchasePrice = v
}
func (this *_AddBitsRequest) PurchasePriceCurrency() *string {
	return this.Ị_purchasePriceCurrency
}
func (this *_AddBitsRequest) SetPurchasePriceCurrency(v *string) {
	this.Ị_purchasePriceCurrency = v
}
func (this *_AddBitsRequest) TransactionId() *string {
	return this.Ị_transactionId
}
func (this *_AddBitsRequest) SetTransactionId(v *string) {
	this.Ị_transactionId = v
}
func (this *_AddBitsRequest) TwitchUserId() *string {
	return this.Ị_twitchUserId
}
func (this *_AddBitsRequest) SetTwitchUserId(v *string) {
	this.Ị_twitchUserId = v
}
func (this *_AddBitsRequest) EventTypeId() *string {
	return this.Ị_eventTypeId
}
func (this *_AddBitsRequest) SetEventTypeId(v *string) {
	this.Ị_eventTypeId = v
}
func (this *_AddBitsRequest) SKU() *string {
	return this.Ị_SKU
}
func (this *_AddBitsRequest) SetSKU(v *string) {
	this.Ị_SKU = v
}
func (this *_AddBitsRequest) SkuQuantity() *int32 {
	return this.Ị_skuQuantity
}
func (this *_AddBitsRequest) SetSkuQuantity(v *int32) {
	this.Ị_skuQuantity = v
}
func (this *_AddBitsRequest) __type() {
}
func NewAddBitsRequest() AddBitsRequest {
	return &_AddBitsRequest{}
}
func init() {
	var val AddBitsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("AddBitsRequest", t, func() interface{} {
		return NewAddBitsRequest()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("AddBitsRequest", t, func() interface{} {
		return NewAddBitsRequest()
	})
}

type AdminAddBitsResult interface {
	__type()
	SetBitsBalance(v *int32)
	BitsBalance() *int32
}
type _AdminAddBitsResult struct {
	Ị_bitsBalance *int32 `coral:"bitsBalance" json:"bitsBalance"`
}

func (this *_AdminAddBitsResult) BitsBalance() *int32 {
	return this.Ị_bitsBalance
}
func (this *_AdminAddBitsResult) SetBitsBalance(v *int32) {
	this.Ị_bitsBalance = v
}
func (this *_AdminAddBitsResult) __type() {
}
func NewAdminAddBitsResult() AdminAddBitsResult {
	return &_AdminAddBitsResult{}
}
func init() {
	var val AdminAddBitsResult
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("AdminAddBitsResult", t, func() interface{} {
		return NewAdminAddBitsResult()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("AdminAddBitsResult", t, func() interface{} {
		return NewAdminAddBitsResult()
	})
}

type GetAccountTypeForAccountIdRequest interface {
	__type()
	SetAccountId(v *string)
	AccountId() *string
}
type _GetAccountTypeForAccountIdRequest struct {
	Ị_accountId *string `coral:"accountId" json:"accountId"`
}

func (this *_GetAccountTypeForAccountIdRequest) AccountId() *string {
	return this.Ị_accountId
}
func (this *_GetAccountTypeForAccountIdRequest) SetAccountId(v *string) {
	this.Ị_accountId = v
}
func (this *_GetAccountTypeForAccountIdRequest) __type() {
}
func NewGetAccountTypeForAccountIdRequest() GetAccountTypeForAccountIdRequest {
	return &_GetAccountTypeForAccountIdRequest{}
}
func init() {
	var val GetAccountTypeForAccountIdRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("GetAccountTypeForAccountIdRequest", t, func() interface{} {
		return NewGetAccountTypeForAccountIdRequest()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("GetAccountTypeForAccountIdRequest", t, func() interface{} {
		return NewGetAccountTypeForAccountIdRequest()
	})
}

type GetEventContextForTransactionRequest interface {
	__type()
	SetTwitchUserId(v *string)
	TwitchUserId() *string
	SetTransactionId(v *string)
	TransactionId() *string
}
type _GetEventContextForTransactionRequest struct {
	Ị_twitchUserId  *string `coral:"twitchUserId" json:"twitchUserId"`
	Ị_transactionId *string `coral:"transactionId" json:"transactionId"`
}

func (this *_GetEventContextForTransactionRequest) TwitchUserId() *string {
	return this.Ị_twitchUserId
}
func (this *_GetEventContextForTransactionRequest) SetTwitchUserId(v *string) {
	this.Ị_twitchUserId = v
}
func (this *_GetEventContextForTransactionRequest) TransactionId() *string {
	return this.Ị_transactionId
}
func (this *_GetEventContextForTransactionRequest) SetTransactionId(v *string) {
	this.Ị_transactionId = v
}
func (this *_GetEventContextForTransactionRequest) __type() {
}
func NewGetEventContextForTransactionRequest() GetEventContextForTransactionRequest {
	return &_GetEventContextForTransactionRequest{}
}
func init() {
	var val GetEventContextForTransactionRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("GetEventContextForTransactionRequest", t, func() interface{} {
		return NewGetEventContextForTransactionRequest()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("GetEventContextForTransactionRequest", t, func() interface{} {
		return NewGetEventContextForTransactionRequest()
	})
}

type SendBalanceUpdateNotificationRequest interface {
	__type()
	SetAdgReceiptId(v *string)
	AdgReceiptId() *string
	SetAccountBalanceChangeMap(v map[string]*int32)
	AccountBalanceChangeMap() map[string]*int32
	SetRequestedChangeAmount(v *int32)
	RequestedChangeAmount() *int32
	SetProcessedChangeAmount(v *int32)
	ProcessedChangeAmount() *int32
	SetTotalBitsAfterTransaction(v *int32)
	TotalBitsAfterTransaction() *int32
	SetTransactionId(v *string)
	TransactionId() *string
}
type _SendBalanceUpdateNotificationRequest struct {
	Ị_processedChangeAmount     *int32            `coral:"processedChangeAmount" json:"processedChangeAmount"`
	Ị_totalBitsAfterTransaction *int32            `coral:"totalBitsAfterTransaction" json:"totalBitsAfterTransaction"`
	Ị_transactionId             *string           `coral:"transactionId" json:"transactionId"`
	Ị_adgReceiptId              *string           `coral:"adgReceiptId" json:"adgReceiptId"`
	Ị_accountBalanceChangeMap   map[string]*int32 `coral:"accountBalanceChangeMap" json:"accountBalanceChangeMap"`
	Ị_requestedChangeAmount     *int32            `coral:"requestedChangeAmount" json:"requestedChangeAmount"`
}

func (this *_SendBalanceUpdateNotificationRequest) TotalBitsAfterTransaction() *int32 {
	return this.Ị_totalBitsAfterTransaction
}
func (this *_SendBalanceUpdateNotificationRequest) SetTotalBitsAfterTransaction(v *int32) {
	this.Ị_totalBitsAfterTransaction = v
}
func (this *_SendBalanceUpdateNotificationRequest) TransactionId() *string {
	return this.Ị_transactionId
}
func (this *_SendBalanceUpdateNotificationRequest) SetTransactionId(v *string) {
	this.Ị_transactionId = v
}
func (this *_SendBalanceUpdateNotificationRequest) AdgReceiptId() *string {
	return this.Ị_adgReceiptId
}
func (this *_SendBalanceUpdateNotificationRequest) SetAdgReceiptId(v *string) {
	this.Ị_adgReceiptId = v
}
func (this *_SendBalanceUpdateNotificationRequest) AccountBalanceChangeMap() map[string]*int32 {
	return this.Ị_accountBalanceChangeMap
}
func (this *_SendBalanceUpdateNotificationRequest) SetAccountBalanceChangeMap(v map[string]*int32) {
	this.Ị_accountBalanceChangeMap = v
}
func (this *_SendBalanceUpdateNotificationRequest) RequestedChangeAmount() *int32 {
	return this.Ị_requestedChangeAmount
}
func (this *_SendBalanceUpdateNotificationRequest) SetRequestedChangeAmount(v *int32) {
	this.Ị_requestedChangeAmount = v
}
func (this *_SendBalanceUpdateNotificationRequest) ProcessedChangeAmount() *int32 {
	return this.Ị_processedChangeAmount
}
func (this *_SendBalanceUpdateNotificationRequest) SetProcessedChangeAmount(v *int32) {
	this.Ị_processedChangeAmount = v
}
func (this *_SendBalanceUpdateNotificationRequest) __type() {
}
func NewSendBalanceUpdateNotificationRequest() SendBalanceUpdateNotificationRequest {
	return &_SendBalanceUpdateNotificationRequest{}
}
func init() {
	var val SendBalanceUpdateNotificationRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("SendBalanceUpdateNotificationRequest", t, func() interface{} {
		return NewSendBalanceUpdateNotificationRequest()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("SendBalanceUpdateNotificationRequest", t, func() interface{} {
		return NewSendBalanceUpdateNotificationRequest()
	})
}

type SendBalanceUpdateNotificationResult interface {
	__type()
}
type _SendBalanceUpdateNotificationResult struct {
}

func (this *_SendBalanceUpdateNotificationResult) __type() {
}
func NewSendBalanceUpdateNotificationResult() SendBalanceUpdateNotificationResult {
	return &_SendBalanceUpdateNotificationResult{}
}
func init() {
	var val SendBalanceUpdateNotificationResult
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("SendBalanceUpdateNotificationResult", t, func() interface{} {
		return NewSendBalanceUpdateNotificationResult()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("SendBalanceUpdateNotificationResult", t, func() interface{} {
		return NewSendBalanceUpdateNotificationResult()
	})
}

type GetBitsBalanceRequest interface {
	__type()
	SetTwitchUserId(v *string)
	TwitchUserId() *string
	SetBroadcasterId(v *string)
	BroadcasterId() *string
}
type _GetBitsBalanceRequest struct {
	Ị_twitchUserId  *string `coral:"twitchUserId" json:"twitchUserId"`
	Ị_broadcasterId *string `coral:"broadcasterId" json:"broadcasterId"`
}

func (this *_GetBitsBalanceRequest) TwitchUserId() *string {
	return this.Ị_twitchUserId
}
func (this *_GetBitsBalanceRequest) SetTwitchUserId(v *string) {
	this.Ị_twitchUserId = v
}
func (this *_GetBitsBalanceRequest) BroadcasterId() *string {
	return this.Ị_broadcasterId
}
func (this *_GetBitsBalanceRequest) SetBroadcasterId(v *string) {
	this.Ị_broadcasterId = v
}
func (this *_GetBitsBalanceRequest) __type() {
}
func NewGetBitsBalanceRequest() GetBitsBalanceRequest {
	return &_GetBitsBalanceRequest{}
}
func init() {
	var val GetBitsBalanceRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("GetBitsBalanceRequest", t, func() interface{} {
		return NewGetBitsBalanceRequest()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("GetBitsBalanceRequest", t, func() interface{} {
		return NewGetBitsBalanceRequest()
	})
}

type GiveBitsToBroadcasterResult interface {
	__type()
	SetUpdatedTotal(v *int32)
	UpdatedTotal() *int32
}
type _GiveBitsToBroadcasterResult struct {
	Ị_updatedTotal *int32 `coral:"updatedTotal" json:"updatedTotal"`
}

func (this *_GiveBitsToBroadcasterResult) UpdatedTotal() *int32 {
	return this.Ị_updatedTotal
}
func (this *_GiveBitsToBroadcasterResult) SetUpdatedTotal(v *int32) {
	this.Ị_updatedTotal = v
}
func (this *_GiveBitsToBroadcasterResult) __type() {
}
func NewGiveBitsToBroadcasterResult() GiveBitsToBroadcasterResult {
	return &_GiveBitsToBroadcasterResult{}
}
func init() {
	var val GiveBitsToBroadcasterResult
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("GiveBitsToBroadcasterResult", t, func() interface{} {
		return NewGiveBitsToBroadcasterResult()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("GiveBitsToBroadcasterResult", t, func() interface{} {
		return NewGiveBitsToBroadcasterResult()
	})
}

type AdminAddBitsRequest interface {
	__type()
	SetTypeOfBitsToAddEnumeration(v *string)
	TypeOfBitsToAddEnumeration() *string
	SetTransactionId(v *string)
	TransactionId() *string
	SetTwitchUserId(v *string)
	TwitchUserId() *string
	SetAdminUserId(v *string)
	AdminUserId() *string
	SetAdminReason(v *string)
	AdminReason() *string
	SetExtraContext(v *string)
	ExtraContext() *string
	SetEventTypeId(v *string)
	EventTypeId() *string
	SetAmountOfBitsToAdd(v *int32)
	AmountOfBitsToAdd() *int32
}
type _AdminAddBitsRequest struct {
	Ị_extraContext               *string `coral:"extraContext" json:"extraContext"`
	Ị_eventTypeId                *string `coral:"eventTypeId" json:"eventTypeId"`
	Ị_amountOfBitsToAdd          *int32  `coral:"amountOfBitsToAdd" json:"amountOfBitsToAdd"`
	Ị_typeOfBitsToAddEnumeration *string `coral:"typeOfBitsToAddEnumeration" json:"typeOfBitsToAddEnumeration"`
	Ị_transactionId              *string `coral:"transactionId" json:"transactionId"`
	Ị_twitchUserId               *string `coral:"twitchUserId" json:"twitchUserId"`
	Ị_adminUserId                *string `coral:"adminUserId" json:"adminUserId"`
	Ị_adminReason                *string `coral:"adminReason" json:"adminReason"`
}

func (this *_AdminAddBitsRequest) AdminUserId() *string {
	return this.Ị_adminUserId
}
func (this *_AdminAddBitsRequest) SetAdminUserId(v *string) {
	this.Ị_adminUserId = v
}
func (this *_AdminAddBitsRequest) AdminReason() *string {
	return this.Ị_adminReason
}
func (this *_AdminAddBitsRequest) SetAdminReason(v *string) {
	this.Ị_adminReason = v
}
func (this *_AdminAddBitsRequest) ExtraContext() *string {
	return this.Ị_extraContext
}
func (this *_AdminAddBitsRequest) SetExtraContext(v *string) {
	this.Ị_extraContext = v
}
func (this *_AdminAddBitsRequest) EventTypeId() *string {
	return this.Ị_eventTypeId
}
func (this *_AdminAddBitsRequest) SetEventTypeId(v *string) {
	this.Ị_eventTypeId = v
}
func (this *_AdminAddBitsRequest) AmountOfBitsToAdd() *int32 {
	return this.Ị_amountOfBitsToAdd
}
func (this *_AdminAddBitsRequest) SetAmountOfBitsToAdd(v *int32) {
	this.Ị_amountOfBitsToAdd = v
}
func (this *_AdminAddBitsRequest) TypeOfBitsToAddEnumeration() *string {
	return this.Ị_typeOfBitsToAddEnumeration
}
func (this *_AdminAddBitsRequest) SetTypeOfBitsToAddEnumeration(v *string) {
	this.Ị_typeOfBitsToAddEnumeration = v
}
func (this *_AdminAddBitsRequest) TransactionId() *string {
	return this.Ị_transactionId
}
func (this *_AdminAddBitsRequest) SetTransactionId(v *string) {
	this.Ị_transactionId = v
}
func (this *_AdminAddBitsRequest) TwitchUserId() *string {
	return this.Ị_twitchUserId
}
func (this *_AdminAddBitsRequest) SetTwitchUserId(v *string) {
	this.Ị_twitchUserId = v
}
func (this *_AdminAddBitsRequest) __type() {
}
func NewAdminAddBitsRequest() AdminAddBitsRequest {
	return &_AdminAddBitsRequest{}
}
func init() {
	var val AdminAddBitsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("AdminAddBitsRequest", t, func() interface{} {
		return NewAdminAddBitsRequest()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("AdminAddBitsRequest", t, func() interface{} {
		return NewAdminAddBitsRequest()
	})
}

type GetEventContextForTransactionResult interface {
	__type()
	SetSenderTwitchUserId(v *string)
	SenderTwitchUserId() *string
	SetReceiverTwitchUserId(v *string)
	ReceiverTwitchUserId() *string
	SetBitsNetChangeAmount(v *int32)
	BitsNetChangeAmount() *int32
	SetTransactionId(v *string)
	TransactionId() *string
	SetSourceChannelStatus(v *string)
	SourceChannelStatus() *string
	SetBitsChangeAmount(v *int32)
	BitsChangeAmount() *int32
	SetEventTime(v *__time__.Time)
	EventTime() *__time__.Time
	SetTransactionType(v *string)
	TransactionType() *string
	SetPublicMessage(v *string)
	PublicMessage() *string
}
type _GetEventContextForTransactionResult struct {
	Ị_bitsNetChangeAmount  *int32         `coral:"bitsNetChangeAmount" json:"bitsNetChangeAmount"`
	Ị_transactionId        *string        `coral:"transactionId" json:"transactionId"`
	Ị_sourceChannelStatus  *string        `coral:"sourceChannelStatus" json:"sourceChannelStatus"`
	Ị_senderTwitchUserId   *string        `coral:"senderTwitchUserId" json:"senderTwitchUserId"`
	Ị_receiverTwitchUserId *string        `coral:"receiverTwitchUserId" json:"receiverTwitchUserId"`
	Ị_transactionType      *string        `coral:"transactionType" json:"transactionType"`
	Ị_publicMessage        *string        `coral:"publicMessage" json:"publicMessage"`
	Ị_bitsChangeAmount     *int32         `coral:"bitsChangeAmount" json:"bitsChangeAmount"`
	Ị_eventTime            *__time__.Time `coral:"eventTime" json:"eventTime"`
}

func (this *_GetEventContextForTransactionResult) BitsChangeAmount() *int32 {
	return this.Ị_bitsChangeAmount
}
func (this *_GetEventContextForTransactionResult) SetBitsChangeAmount(v *int32) {
	this.Ị_bitsChangeAmount = v
}
func (this *_GetEventContextForTransactionResult) EventTime() *__time__.Time {
	return this.Ị_eventTime
}
func (this *_GetEventContextForTransactionResult) SetEventTime(v *__time__.Time) {
	this.Ị_eventTime = v
}
func (this *_GetEventContextForTransactionResult) TransactionType() *string {
	return this.Ị_transactionType
}
func (this *_GetEventContextForTransactionResult) SetTransactionType(v *string) {
	this.Ị_transactionType = v
}
func (this *_GetEventContextForTransactionResult) PublicMessage() *string {
	return this.Ị_publicMessage
}
func (this *_GetEventContextForTransactionResult) SetPublicMessage(v *string) {
	this.Ị_publicMessage = v
}
func (this *_GetEventContextForTransactionResult) SenderTwitchUserId() *string {
	return this.Ị_senderTwitchUserId
}
func (this *_GetEventContextForTransactionResult) SetSenderTwitchUserId(v *string) {
	this.Ị_senderTwitchUserId = v
}
func (this *_GetEventContextForTransactionResult) ReceiverTwitchUserId() *string {
	return this.Ị_receiverTwitchUserId
}
func (this *_GetEventContextForTransactionResult) SetReceiverTwitchUserId(v *string) {
	this.Ị_receiverTwitchUserId = v
}
func (this *_GetEventContextForTransactionResult) BitsNetChangeAmount() *int32 {
	return this.Ị_bitsNetChangeAmount
}
func (this *_GetEventContextForTransactionResult) SetBitsNetChangeAmount(v *int32) {
	this.Ị_bitsNetChangeAmount = v
}
func (this *_GetEventContextForTransactionResult) TransactionId() *string {
	return this.Ị_transactionId
}
func (this *_GetEventContextForTransactionResult) SetTransactionId(v *string) {
	this.Ị_transactionId = v
}
func (this *_GetEventContextForTransactionResult) SourceChannelStatus() *string {
	return this.Ị_sourceChannelStatus
}
func (this *_GetEventContextForTransactionResult) SetSourceChannelStatus(v *string) {
	this.Ị_sourceChannelStatus = v
}
func (this *_GetEventContextForTransactionResult) __type() {
}
func NewGetEventContextForTransactionResult() GetEventContextForTransactionResult {
	return &_GetEventContextForTransactionResult{}
}
func init() {
	var val GetEventContextForTransactionResult
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("GetEventContextForTransactionResult", t, func() interface{} {
		return NewGetEventContextForTransactionResult()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("GetEventContextForTransactionResult", t, func() interface{} {
		return NewGetEventContextForTransactionResult()
	})
}

type AddBitsResult interface {
	__type()
	SetBitsBalance(v *int32)
	BitsBalance() *int32
}
type _AddBitsResult struct {
	Ị_bitsBalance *int32 `coral:"bitsBalance" json:"bitsBalance"`
}

func (this *_AddBitsResult) BitsBalance() *int32 {
	return this.Ị_bitsBalance
}
func (this *_AddBitsResult) SetBitsBalance(v *int32) {
	this.Ị_bitsBalance = v
}
func (this *_AddBitsResult) __type() {
}
func NewAddBitsResult() AddBitsResult {
	return &_AddBitsResult{}
}
func init() {
	var val AddBitsResult
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("AddBitsResult", t, func() interface{} {
		return NewAddBitsResult()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("AddBitsResult", t, func() interface{} {
		return NewAddBitsResult()
	})
}

type InsufficientBalanceException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _InsufficientBalanceException struct {
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_InsufficientBalanceException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_InsufficientBalanceException) Message() *string {
	return this.Ị_message
}
func (this *_InsufficientBalanceException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_InsufficientBalanceException) __type() {
}
func NewInsufficientBalanceException() InsufficientBalanceException {
	return &_InsufficientBalanceException{}
}
func init() {
	var val InsufficientBalanceException
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("InsufficientBalanceException", t, func() interface{} {
		return NewInsufficientBalanceException()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("InsufficientBalanceException", t, func() interface{} {
		return NewInsufficientBalanceException()
	})
}

type GetBitsBalanceAndPurchasableAsinsRequest interface {
	__type()
	SetTargetAsinPriceAmount(v *string)
	TargetAsinPriceAmount() *string
	SetTargetedAsin(v *string)
	TargetedAsin() *string
	SetTwitchUserId(v *string)
	TwitchUserId() *string
	SetAmazonCustomerId(v *string)
	AmazonCustomerId() *string
}
type _GetBitsBalanceAndPurchasableAsinsRequest struct {
	Ị_twitchUserId          *string `coral:"twitchUserId" json:"twitchUserId"`
	Ị_amazonCustomerId      *string `coral:"amazonCustomerId" json:"amazonCustomerId"`
	Ị_targetAsinPriceAmount *string `coral:"targetAsinPriceAmount" json:"targetAsinPriceAmount"`
	Ị_targetedAsin          *string `coral:"targetedAsin" json:"targetedAsin"`
}

func (this *_GetBitsBalanceAndPurchasableAsinsRequest) TwitchUserId() *string {
	return this.Ị_twitchUserId
}
func (this *_GetBitsBalanceAndPurchasableAsinsRequest) SetTwitchUserId(v *string) {
	this.Ị_twitchUserId = v
}
func (this *_GetBitsBalanceAndPurchasableAsinsRequest) AmazonCustomerId() *string {
	return this.Ị_amazonCustomerId
}
func (this *_GetBitsBalanceAndPurchasableAsinsRequest) SetAmazonCustomerId(v *string) {
	this.Ị_amazonCustomerId = v
}
func (this *_GetBitsBalanceAndPurchasableAsinsRequest) TargetAsinPriceAmount() *string {
	return this.Ị_targetAsinPriceAmount
}
func (this *_GetBitsBalanceAndPurchasableAsinsRequest) SetTargetAsinPriceAmount(v *string) {
	this.Ị_targetAsinPriceAmount = v
}
func (this *_GetBitsBalanceAndPurchasableAsinsRequest) TargetedAsin() *string {
	return this.Ị_targetedAsin
}
func (this *_GetBitsBalanceAndPurchasableAsinsRequest) SetTargetedAsin(v *string) {
	this.Ị_targetedAsin = v
}
func (this *_GetBitsBalanceAndPurchasableAsinsRequest) __type() {
}
func NewGetBitsBalanceAndPurchasableAsinsRequest() GetBitsBalanceAndPurchasableAsinsRequest {
	return &_GetBitsBalanceAndPurchasableAsinsRequest{}
}
func init() {
	var val GetBitsBalanceAndPurchasableAsinsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("GetBitsBalanceAndPurchasableAsinsRequest", t, func() interface{} {
		return NewGetBitsBalanceAndPurchasableAsinsRequest()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("GetBitsBalanceAndPurchasableAsinsRequest", t, func() interface{} {
		return NewGetBitsBalanceAndPurchasableAsinsRequest()
	})
}

type AdminRemoveBitsResult interface {
	__type()
	SetBitsBalance(v *int32)
	BitsBalance() *int32
}
type _AdminRemoveBitsResult struct {
	Ị_bitsBalance *int32 `coral:"bitsBalance" json:"bitsBalance"`
}

func (this *_AdminRemoveBitsResult) BitsBalance() *int32 {
	return this.Ị_bitsBalance
}
func (this *_AdminRemoveBitsResult) SetBitsBalance(v *int32) {
	this.Ị_bitsBalance = v
}
func (this *_AdminRemoveBitsResult) __type() {
}
func NewAdminRemoveBitsResult() AdminRemoveBitsResult {
	return &_AdminRemoveBitsResult{}
}
func init() {
	var val AdminRemoveBitsResult
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("AdminRemoveBitsResult", t, func() interface{} {
		return NewAdminRemoveBitsResult()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("AdminRemoveBitsResult", t, func() interface{} {
		return NewAdminRemoveBitsResult()
	})
}

type GetAccountTypeForAccountIdResult interface {
	__type()
	SetTwitchUserId(v *string)
	TwitchUserId() *string
	SetAccountType(v *string)
	AccountType() *string
}
type _GetAccountTypeForAccountIdResult struct {
	Ị_twitchUserId *string `coral:"twitchUserId" json:"twitchUserId"`
	Ị_accountType  *string `coral:"accountType" json:"accountType"`
}

func (this *_GetAccountTypeForAccountIdResult) TwitchUserId() *string {
	return this.Ị_twitchUserId
}
func (this *_GetAccountTypeForAccountIdResult) SetTwitchUserId(v *string) {
	this.Ị_twitchUserId = v
}
func (this *_GetAccountTypeForAccountIdResult) AccountType() *string {
	return this.Ị_accountType
}
func (this *_GetAccountTypeForAccountIdResult) SetAccountType(v *string) {
	this.Ị_accountType = v
}
func (this *_GetAccountTypeForAccountIdResult) __type() {
}
func NewGetAccountTypeForAccountIdResult() GetAccountTypeForAccountIdResult {
	return &_GetAccountTypeForAccountIdResult{}
}
func init() {
	var val GetAccountTypeForAccountIdResult
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("GetAccountTypeForAccountIdResult", t, func() interface{} {
		return NewGetAccountTypeForAccountIdResult()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("GetAccountTypeForAccountIdResult", t, func() interface{} {
		return NewGetAccountTypeForAccountIdResult()
	})
}

type Transaction interface {
	__type()
	SetTransactionType(v *string)
	TransactionType() *string
	SetTimeSent(v *__time__.Time)
	TimeSent() *__time__.Time
	SetExtensionId(v *string)
	ExtensionId() *string
	SetPollChoiceId(v *string)
	PollChoiceId() *string
	SetNetChangeInAmountOfBits(v *int32)
	NetChangeInAmountOfBits() *int32
	SetReceviersTwitchId(v *string)
	ReceviersTwitchId() *string
	SetId(v *string)
	Id() *string
	SetAdminId(v *string)
	AdminId() *string
	SetAdminReason(v *string)
	AdminReason() *string
	SetExtensionSKU(v *string)
	ExtensionSKU() *string
	SetPollId(v *string)
	PollId() *string
	SetPublicMessage(v *string)
	PublicMessage() *string
}
type _Transaction struct {
	Ị_netChangeInAmountOfBits *int32         `coral:"netChangeInAmountOfBits" json:"netChangeInAmountOfBits"`
	Ị_transactionType         *string        `coral:"transactionType" json:"transactionType"`
	Ị_timeSent                *__time__.Time `coral:"timeSent" json:"timeSent"`
	Ị_extensionId             *string        `coral:"extensionId" json:"extensionId"`
	Ị_pollChoiceId            *string        `coral:"pollChoiceId" json:"pollChoiceId"`
	Ị_publicMessage           *string        `coral:"publicMessage" json:"publicMessage"`
	Ị_receviersTwitchId       *string        `coral:"receviersTwitchId" json:"receviersTwitchId"`
	Ị_id                      *string        `coral:"id" json:"id"`
	Ị_adminId                 *string        `coral:"adminId" json:"adminId"`
	Ị_adminReason             *string        `coral:"adminReason" json:"adminReason"`
	Ị_extensionSKU            *string        `coral:"extensionSKU" json:"extensionSKU"`
	Ị_pollId                  *string        `coral:"pollId" json:"pollId"`
}

func (this *_Transaction) NetChangeInAmountOfBits() *int32 {
	return this.Ị_netChangeInAmountOfBits
}
func (this *_Transaction) SetNetChangeInAmountOfBits(v *int32) {
	this.Ị_netChangeInAmountOfBits = v
}
func (this *_Transaction) TransactionType() *string {
	return this.Ị_transactionType
}
func (this *_Transaction) SetTransactionType(v *string) {
	this.Ị_transactionType = v
}
func (this *_Transaction) TimeSent() *__time__.Time {
	return this.Ị_timeSent
}
func (this *_Transaction) SetTimeSent(v *__time__.Time) {
	this.Ị_timeSent = v
}
func (this *_Transaction) ExtensionId() *string {
	return this.Ị_extensionId
}
func (this *_Transaction) SetExtensionId(v *string) {
	this.Ị_extensionId = v
}
func (this *_Transaction) PollChoiceId() *string {
	return this.Ị_pollChoiceId
}
func (this *_Transaction) SetPollChoiceId(v *string) {
	this.Ị_pollChoiceId = v
}
func (this *_Transaction) PublicMessage() *string {
	return this.Ị_publicMessage
}
func (this *_Transaction) SetPublicMessage(v *string) {
	this.Ị_publicMessage = v
}
func (this *_Transaction) ReceviersTwitchId() *string {
	return this.Ị_receviersTwitchId
}
func (this *_Transaction) SetReceviersTwitchId(v *string) {
	this.Ị_receviersTwitchId = v
}
func (this *_Transaction) Id() *string {
	return this.Ị_id
}
func (this *_Transaction) SetId(v *string) {
	this.Ị_id = v
}
func (this *_Transaction) AdminId() *string {
	return this.Ị_adminId
}
func (this *_Transaction) SetAdminId(v *string) {
	this.Ị_adminId = v
}
func (this *_Transaction) AdminReason() *string {
	return this.Ị_adminReason
}
func (this *_Transaction) SetAdminReason(v *string) {
	this.Ị_adminReason = v
}
func (this *_Transaction) ExtensionSKU() *string {
	return this.Ị_extensionSKU
}
func (this *_Transaction) SetExtensionSKU(v *string) {
	this.Ị_extensionSKU = v
}
func (this *_Transaction) PollId() *string {
	return this.Ị_pollId
}
func (this *_Transaction) SetPollId(v *string) {
	this.Ị_pollId = v
}
func (this *_Transaction) __type() {
}
func NewTransaction() Transaction {
	return &_Transaction{}
}
func init() {
	var val Transaction
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("Transaction", t, func() interface{} {
		return NewTransaction()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuelamazonbitsservice").RegisterShape("Transaction", t, func() interface{} {
		return NewTransaction()
	})
}
func init() {
	var val map[string]*int32
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[string]*int32
		if f, ok := from.Interface().(map[string]*int32); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to SpecificBitTotals")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val map[string]*bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[string]*bool
		if f, ok := from.Interface().(map[string]*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to CanPurchaseMap")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val map[string]*string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[string]*string
		if f, ok := from.Interface().(map[string]*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to CantPurchaseReasonMap")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val map[string]BitsProductInformation
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[string]BitsProductInformation
		if f, ok := from.Interface().(map[string]BitsProductInformation); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ProductInformationMap")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val map[string]*__time__.Time
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[string]*__time__.Time
		if f, ok := from.Interface().(map[string]*__time__.Time); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to MostRecentPurchaseBySuperPlatformMap")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val map[string]*int32
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[string]*int32
		if f, ok := from.Interface().(map[string]*int32); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to SponsoredAmounts")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []*string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []*string
		if f, ok := from.Interface().([]*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ActiveMarketplaceList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []*string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []*string
		if f, ok := from.Interface().([]*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Platforms")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []Transaction
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []Transaction
		if f, ok := from.Interface().([]Transaction); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Transactions")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

//A service that manages all back-end bits transactions
type FuelAmazonBitsService interface { //Removes bits from a given Twitch User Id on behalf of an admin, must specify the userId
	//of the requesting admin, the reason bits are being removed and an idempotent transactionId.
	AdminRemoveBits(input AdminRemoveBitsRequest) (AdminRemoveBitsResult, error)                                                       //Gets the total number of bits that a user can spend as well as the asins they are allowed to purchase
	GetBitsBalanceAndPurchasableAsins(input GetBitsBalanceAndPurchasableAsinsRequest) (GetBitsBalanceAndPurchasableAsinsResult, error) //Gives a list of transactions, starting newest first.
	//Can be paginated with multiple call using the iteratorKey.
	ViewBitsTransactions(input ViewBitsTransactionsRequest) (ViewBitsTransactionsResult, error)
	GetEventContextForTransaction(input GetEventContextForTransactionRequest) (GetEventContextForTransactionResult, error)
	GetAccountTypeForAccountId(input GetAccountTypeForAccountIdRequest) (GetAccountTypeForAccountIdResult, error)
	SendBalanceUpdateNotification(input SendBalanceUpdateNotificationRequest) (SendBalanceUpdateNotificationResult, error) //Adds bits to a given Twitch User Id, must specify an idempotent transactionId as well as an
	//enumeration of the type of bit to add (currently supported: PURCHASED/TURBO/PROMOTIONAL).
	AddBits(input AddBitsRequest) (AddBitsResult, error)                      //Gets the total number of bits that a user can spend.
	GetBitsBalance(input GetBitsBalanceRequest) (GetBitsBalanceResult, error) //Attempts to send the requested number of bits owned by a user to a broadcaster.
	//Returns the updated balance post-transaction.
	//Fails if the user doesn't have enough bits. Input must be a positive number.
	GiveBitsToBroadcaster(input GiveBitsToBroadcasterRequest) (GiveBitsToBroadcasterResult, error) //Sends bits to extension for use.
	UseBitsOnExtension(input UseBitsOnExtensionRequest) (UseBitsOnExtensionResult, error)          //Adds bits to a given Twitch User Id on behalf of an admin, must specify the userId of the
	//requesting admin, the reason bits are being added, an idempotent transactionId and an
	//enumeration of the type of bit to add (currently supported: PURCHASED/TURBO/PROMOTIONAL).
	AdminAddBits(input AdminAddBitsRequest) (AdminAddBitsResult, error)
}

//Provides the bits service with a Coral SQS consumer.
type FuelAmazonBitsServiceSQS interface { //Adds bits to a given Twitch User Id, must specify an idempotent transactionId as well as an
	//enumeration of the type of bit to add (currently supported: PURCHASED/TURBO/PROMOTIONAL).
	AddBits(input AddBitsRequest) (AddBitsResult, error)
}
