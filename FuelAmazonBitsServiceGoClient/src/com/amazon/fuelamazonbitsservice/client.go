package com_amazon_fuelamazonbitsservice

import (
	__client__ "code.justin.tv/commerce/CoralGoClient/src/coral/client"
	__codec__ "code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	__dialer__ "code.justin.tv/commerce/CoralGoClient/src/coral/dialer"
)

//A service that manages all back-end bits transactions
type FuelAmazonBitsServiceClient struct {
	C __client__.Client
}

//Creates a new FuelAmazonBitsServiceClient
func NewFuelAmazonBitsServiceClient(dialer __dialer__.Dialer, codec __codec__.RoundTripper) (service *FuelAmazonBitsServiceClient) {
	return &FuelAmazonBitsServiceClient{__client__.NewClient("com.amazon.fuelamazonbitsservice", "FuelAmazonBitsService", dialer, codec)}
}

//Removes bits from a given Twitch User Id on behalf of an admin, must specify the userId
//of the requesting admin, the reason bits are being removed and an idempotent transactionId.
func (this *FuelAmazonBitsServiceClient) AdminRemoveBits(input AdminRemoveBitsRequest) (AdminRemoveBitsResult, error) {
	var output AdminRemoveBitsResult
	err := this.C.Call("com.amazon.fuelamazonbitsservice", "AdminRemoveBits", input, &output)
	return output, err
}

//Gets the total number of bits that a user can spend as well as the asins they are allowed to purchase
func (this *FuelAmazonBitsServiceClient) GetBitsBalanceAndPurchasableAsins(input GetBitsBalanceAndPurchasableAsinsRequest) (GetBitsBalanceAndPurchasableAsinsResult, error) {
	var output GetBitsBalanceAndPurchasableAsinsResult
	err := this.C.Call("com.amazon.fuelamazonbitsservice", "GetBitsBalanceAndPurchasableAsins", input, &output)
	return output, err
}

//Gives a list of transactions, starting newest first.
//Can be paginated with multiple call using the iteratorKey.
func (this *FuelAmazonBitsServiceClient) ViewBitsTransactions(input ViewBitsTransactionsRequest) (ViewBitsTransactionsResult, error) {
	var output ViewBitsTransactionsResult
	err := this.C.Call("com.amazon.fuelamazonbitsservice", "ViewBitsTransactions", input, &output)
	return output, err
}
func (this *FuelAmazonBitsServiceClient) GetEventContextForTransaction(input GetEventContextForTransactionRequest) (GetEventContextForTransactionResult, error) {
	var output GetEventContextForTransactionResult
	err := this.C.Call("com.amazon.fuelamazonbitsservice", "GetEventContextForTransaction", input, &output)
	return output, err
}
func (this *FuelAmazonBitsServiceClient) GetAccountTypeForAccountId(input GetAccountTypeForAccountIdRequest) (GetAccountTypeForAccountIdResult, error) {
	var output GetAccountTypeForAccountIdResult
	err := this.C.Call("com.amazon.fuelamazonbitsservice", "GetAccountTypeForAccountId", input, &output)
	return output, err
}
func (this *FuelAmazonBitsServiceClient) SendBalanceUpdateNotification(input SendBalanceUpdateNotificationRequest) (SendBalanceUpdateNotificationResult, error) {
	var output SendBalanceUpdateNotificationResult
	err := this.C.Call("com.amazon.fuelamazonbitsservice", "SendBalanceUpdateNotification", input, &output)
	return output, err
}

//Adds bits to a given Twitch User Id, must specify an idempotent transactionId as well as an
//enumeration of the type of bit to add (currently supported: PURCHASED/TURBO/PROMOTIONAL).
func (this *FuelAmazonBitsServiceClient) AddBits(input AddBitsRequest) (AddBitsResult, error) {
	var output AddBitsResult
	err := this.C.Call("com.amazon.fuelamazonbitsservice", "AddBits", input, &output)
	return output, err
}

//Gets the total number of bits that a user can spend.
func (this *FuelAmazonBitsServiceClient) GetBitsBalance(input GetBitsBalanceRequest) (GetBitsBalanceResult, error) {
	var output GetBitsBalanceResult
	err := this.C.Call("com.amazon.fuelamazonbitsservice", "GetBitsBalance", input, &output)
	return output, err
}

//Attempts to send the requested number of bits owned by a user to a broadcaster.
//Returns the updated balance post-transaction.
//Fails if the user doesn't have enough bits. Input must be a positive number.
func (this *FuelAmazonBitsServiceClient) GiveBitsToBroadcaster(input GiveBitsToBroadcasterRequest) (GiveBitsToBroadcasterResult, error) {
	var output GiveBitsToBroadcasterResult
	err := this.C.Call("com.amazon.fuelamazonbitsservice", "GiveBitsToBroadcaster", input, &output)
	return output, err
}

//Sends bits to extension for use.
func (this *FuelAmazonBitsServiceClient) UseBitsOnExtension(input UseBitsOnExtensionRequest) (UseBitsOnExtensionResult, error) {
	var output UseBitsOnExtensionResult
	err := this.C.Call("com.amazon.fuelamazonbitsservice", "UseBitsOnExtension", input, &output)
	return output, err
}

//Adds bits to a given Twitch User Id on behalf of an admin, must specify the userId of the
//requesting admin, the reason bits are being added, an idempotent transactionId and an
//enumeration of the type of bit to add (currently supported: PURCHASED/TURBO/PROMOTIONAL).
func (this *FuelAmazonBitsServiceClient) AdminAddBits(input AdminAddBitsRequest) (AdminAddBitsResult, error) {
	var output AdminAddBitsResult
	err := this.C.Call("com.amazon.fuelamazonbitsservice", "AdminAddBits", input, &output)
	return output, err
}

//Provides the bits service with a Coral SQS consumer.
type FuelAmazonBitsServiceSQSClient struct {
	C __client__.Client
}

//Creates a new FuelAmazonBitsServiceSQSClient
func NewFuelAmazonBitsServiceSQSClient(dialer __dialer__.Dialer, codec __codec__.RoundTripper) (service *FuelAmazonBitsServiceSQSClient) {
	return &FuelAmazonBitsServiceSQSClient{__client__.NewClient("com.amazon.fuelamazonbitsservice", "FuelAmazonBitsServiceSQS", dialer, codec)}
}

//Adds bits to a given Twitch User Id, must specify an idempotent transactionId as well as an
//enumeration of the type of bit to add (currently supported: PURCHASED/TURBO/PROMOTIONAL).
func (this *FuelAmazonBitsServiceSQSClient) AddBits(input AddBitsRequest) (AddBitsResult, error) {
	var output AddBitsResult
	err := this.C.Call("com.amazon.fuelamazonbitsservice", "AddBits", input, &output)
	return output, err
}
