package com_amazon_fuel_common

import (
	__model__ "code.justin.tv/commerce/CoralGoModel/src/coral/model"
	__reflect__ "reflect"
)

func init() {
	var val *bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *bool
		if f, ok := from.Interface().(*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Boolean")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int32
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int32
		if f, ok := from.Interface().(*int32); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Integer")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to NullableString")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to RequiredString")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

type BalanceUpdateNotification interface {
	__type()
	SetEventTypeId(v *string)
	EventTypeId() *string
	SetPurchasePrice(v *string)
	PurchasePrice() *string
	SetPollId(v *string)
	PollId() *string
	SetTransactionType(v *string)
	TransactionType() *string
	SetGiveBitsSourceChannelStatus(v *string)
	GiveBitsSourceChannelStatus() *string
	SetSkuQuantity(v *int32)
	SkuQuantity() *int32
	SetAccountTypeToBitAmount(v map[string]*int32)
	AccountTypeToBitAmount() map[string]*int32
	SetSKU(v *string)
	SKU() *string
	SetTransactionId(v *string)
	TransactionId() *string
	SetGiveBitsPublicMessage(v *string)
	GiveBitsPublicMessage() *string
	SetExtraContext(v *string)
	ExtraContext() *string
	SetTotalBitsAfterTransaction(v *int32)
	TotalBitsAfterTransaction() *int32
	SetSponsoredAmounts(v map[string]*int32)
	SponsoredAmounts() map[string]*int32
	SetAdminId(v *string)
	AdminId() *string
	SetAnonymous(v *bool)
	Anonymous() *bool
	SetProcessedBits(v *int32)
	ProcessedBits() *int32
	SetExtensionId(v *string)
	ExtensionId() *string
	SetAccountTypeSpendOrderPriorityValueMap(v map[string]*int32)
	AccountTypeSpendOrderPriorityValueMap() map[string]*int32
	SetPurchaseMarketplace(v *string)
	PurchaseMarketplace() *string
	SetPollChoiceId(v *string)
	PollChoiceId() *string
	SetRequestingTwitchUserId(v *string)
	RequestingTwitchUserId() *string
	SetAdminRelatedTransactionId(v *string)
	AdminRelatedTransactionId() *string
	SetAdminReason(v *string)
	AdminReason() *string
	SetTotalBitsGivenFromUserToBroadcaster(v *int32)
	TotalBitsGivenFromUserToBroadcaster() *int32
	SetPurchasePriceCurrency(v *string)
	PurchasePriceCurrency() *string
	SetTargetTwitchUserId(v *string)
	TargetTwitchUserId() *string
	SetTimeOfEvent(v *string)
	TimeOfEvent() *string
	SetTotalBits(v *int32)
	TotalBits() *int32
}
type _BalanceUpdateNotification struct {
	Ị_targetTwitchUserId                    *string           `coral:"targetTwitchUserId" json:"targetTwitchUserId"`
	Ị_timeOfEvent                           *string           `coral:"timeOfEvent" json:"timeOfEvent"`
	Ị_totalBits                             *int32            `coral:"totalBits" json:"totalBits"`
	Ị_eventTypeId                           *string           `coral:"eventTypeId" json:"eventTypeId"`
	Ị_purchasePrice                         *string           `coral:"purchasePrice" json:"purchasePrice"`
	Ị_pollId                                *string           `coral:"pollId" json:"pollId"`
	Ị_transactionType                       *string           `coral:"transactionType" json:"transactionType"`
	Ị_giveBitsSourceChannelStatus           *string           `coral:"giveBitsSourceChannelStatus" json:"giveBitsSourceChannelStatus"`
	Ị_skuQuantity                           *int32            `coral:"skuQuantity" json:"skuQuantity"`
	Ị_accountTypeToBitAmount                map[string]*int32 `coral:"accountTypeToBitAmount" json:"accountTypeToBitAmount"`
	Ị_SKU                                   *string           `coral:"SKU" json:"SKU"`
	Ị_transactionId                         *string           `coral:"transactionId" json:"transactionId"`
	Ị_giveBitsPublicMessage                 *string           `coral:"giveBitsPublicMessage" json:"giveBitsPublicMessage"`
	Ị_extraContext                          *string           `coral:"extraContext" json:"extraContext"`
	Ị_totalBitsAfterTransaction             *int32            `coral:"totalBitsAfterTransaction" json:"totalBitsAfterTransaction"`
	Ị_sponsoredAmounts                      map[string]*int32 `coral:"sponsoredAmounts" json:"sponsoredAmounts"`
	Ị_adminId                               *string           `coral:"adminId" json:"adminId"`
	Ị_anonymous                             *bool             `coral:"anonymous" json:"anonymous"`
	Ị_processedBits                         *int32            `coral:"processedBits" json:"processedBits"`
	Ị_extensionId                           *string           `coral:"extensionId" json:"extensionId"`
	Ị_accountTypeSpendOrderPriorityValueMap map[string]*int32 `coral:"accountTypeSpendOrderPriorityValueMap" json:"accountTypeSpendOrderPriorityValueMap"`
	Ị_requestingTwitchUserId                *string           `coral:"requestingTwitchUserId" json:"requestingTwitchUserId"`
	Ị_adminRelatedTransactionId             *string           `coral:"adminRelatedTransactionId" json:"adminRelatedTransactionId"`
	Ị_adminReason                           *string           `coral:"adminReason" json:"adminReason"`
	Ị_totalBitsGivenFromUserToBroadcaster   *int32            `coral:"totalBitsGivenFromUserToBroadcaster" json:"totalBitsGivenFromUserToBroadcaster"`
	Ị_purchasePriceCurrency                 *string           `coral:"purchasePriceCurrency" json:"purchasePriceCurrency"`
	Ị_purchaseMarketplace                   *string           `coral:"purchaseMarketplace" json:"purchaseMarketplace"`
	Ị_pollChoiceId                          *string           `coral:"pollChoiceId" json:"pollChoiceId"`
}

func (this *_BalanceUpdateNotification) TransactionId() *string {
	return this.Ị_transactionId
}
func (this *_BalanceUpdateNotification) SetTransactionId(v *string) {
	this.Ị_transactionId = v
}
func (this *_BalanceUpdateNotification) GiveBitsPublicMessage() *string {
	return this.Ị_giveBitsPublicMessage
}
func (this *_BalanceUpdateNotification) SetGiveBitsPublicMessage(v *string) {
	this.Ị_giveBitsPublicMessage = v
}
func (this *_BalanceUpdateNotification) ExtraContext() *string {
	return this.Ị_extraContext
}
func (this *_BalanceUpdateNotification) SetExtraContext(v *string) {
	this.Ị_extraContext = v
}
func (this *_BalanceUpdateNotification) TotalBitsAfterTransaction() *int32 {
	return this.Ị_totalBitsAfterTransaction
}
func (this *_BalanceUpdateNotification) SetTotalBitsAfterTransaction(v *int32) {
	this.Ị_totalBitsAfterTransaction = v
}
func (this *_BalanceUpdateNotification) SponsoredAmounts() map[string]*int32 {
	return this.Ị_sponsoredAmounts
}
func (this *_BalanceUpdateNotification) SetSponsoredAmounts(v map[string]*int32) {
	this.Ị_sponsoredAmounts = v
}
func (this *_BalanceUpdateNotification) AdminId() *string {
	return this.Ị_adminId
}
func (this *_BalanceUpdateNotification) SetAdminId(v *string) {
	this.Ị_adminId = v
}
func (this *_BalanceUpdateNotification) Anonymous() *bool {
	return this.Ị_anonymous
}
func (this *_BalanceUpdateNotification) SetAnonymous(v *bool) {
	this.Ị_anonymous = v
}
func (this *_BalanceUpdateNotification) ProcessedBits() *int32 {
	return this.Ị_processedBits
}
func (this *_BalanceUpdateNotification) SetProcessedBits(v *int32) {
	this.Ị_processedBits = v
}
func (this *_BalanceUpdateNotification) ExtensionId() *string {
	return this.Ị_extensionId
}
func (this *_BalanceUpdateNotification) SetExtensionId(v *string) {
	this.Ị_extensionId = v
}
func (this *_BalanceUpdateNotification) AccountTypeSpendOrderPriorityValueMap() map[string]*int32 {
	return this.Ị_accountTypeSpendOrderPriorityValueMap
}
func (this *_BalanceUpdateNotification) SetAccountTypeSpendOrderPriorityValueMap(v map[string]*int32) {
	this.Ị_accountTypeSpendOrderPriorityValueMap = v
}
func (this *_BalanceUpdateNotification) RequestingTwitchUserId() *string {
	return this.Ị_requestingTwitchUserId
}
func (this *_BalanceUpdateNotification) SetRequestingTwitchUserId(v *string) {
	this.Ị_requestingTwitchUserId = v
}
func (this *_BalanceUpdateNotification) AdminRelatedTransactionId() *string {
	return this.Ị_adminRelatedTransactionId
}
func (this *_BalanceUpdateNotification) SetAdminRelatedTransactionId(v *string) {
	this.Ị_adminRelatedTransactionId = v
}
func (this *_BalanceUpdateNotification) AdminReason() *string {
	return this.Ị_adminReason
}
func (this *_BalanceUpdateNotification) SetAdminReason(v *string) {
	this.Ị_adminReason = v
}
func (this *_BalanceUpdateNotification) TotalBitsGivenFromUserToBroadcaster() *int32 {
	return this.Ị_totalBitsGivenFromUserToBroadcaster
}
func (this *_BalanceUpdateNotification) SetTotalBitsGivenFromUserToBroadcaster(v *int32) {
	this.Ị_totalBitsGivenFromUserToBroadcaster = v
}
func (this *_BalanceUpdateNotification) PurchasePriceCurrency() *string {
	return this.Ị_purchasePriceCurrency
}
func (this *_BalanceUpdateNotification) SetPurchasePriceCurrency(v *string) {
	this.Ị_purchasePriceCurrency = v
}
func (this *_BalanceUpdateNotification) PurchaseMarketplace() *string {
	return this.Ị_purchaseMarketplace
}
func (this *_BalanceUpdateNotification) SetPurchaseMarketplace(v *string) {
	this.Ị_purchaseMarketplace = v
}
func (this *_BalanceUpdateNotification) PollChoiceId() *string {
	return this.Ị_pollChoiceId
}
func (this *_BalanceUpdateNotification) SetPollChoiceId(v *string) {
	this.Ị_pollChoiceId = v
}
func (this *_BalanceUpdateNotification) TargetTwitchUserId() *string {
	return this.Ị_targetTwitchUserId
}
func (this *_BalanceUpdateNotification) SetTargetTwitchUserId(v *string) {
	this.Ị_targetTwitchUserId = v
}
func (this *_BalanceUpdateNotification) TimeOfEvent() *string {
	return this.Ị_timeOfEvent
}
func (this *_BalanceUpdateNotification) SetTimeOfEvent(v *string) {
	this.Ị_timeOfEvent = v
}
func (this *_BalanceUpdateNotification) TotalBits() *int32 {
	return this.Ị_totalBits
}
func (this *_BalanceUpdateNotification) SetTotalBits(v *int32) {
	this.Ị_totalBits = v
}
func (this *_BalanceUpdateNotification) EventTypeId() *string {
	return this.Ị_eventTypeId
}
func (this *_BalanceUpdateNotification) SetEventTypeId(v *string) {
	this.Ị_eventTypeId = v
}
func (this *_BalanceUpdateNotification) PurchasePrice() *string {
	return this.Ị_purchasePrice
}
func (this *_BalanceUpdateNotification) SetPurchasePrice(v *string) {
	this.Ị_purchasePrice = v
}
func (this *_BalanceUpdateNotification) PollId() *string {
	return this.Ị_pollId
}
func (this *_BalanceUpdateNotification) SetPollId(v *string) {
	this.Ị_pollId = v
}
func (this *_BalanceUpdateNotification) TransactionType() *string {
	return this.Ị_transactionType
}
func (this *_BalanceUpdateNotification) SetTransactionType(v *string) {
	this.Ị_transactionType = v
}
func (this *_BalanceUpdateNotification) GiveBitsSourceChannelStatus() *string {
	return this.Ị_giveBitsSourceChannelStatus
}
func (this *_BalanceUpdateNotification) SetGiveBitsSourceChannelStatus(v *string) {
	this.Ị_giveBitsSourceChannelStatus = v
}
func (this *_BalanceUpdateNotification) SkuQuantity() *int32 {
	return this.Ị_skuQuantity
}
func (this *_BalanceUpdateNotification) SetSkuQuantity(v *int32) {
	this.Ị_skuQuantity = v
}
func (this *_BalanceUpdateNotification) AccountTypeToBitAmount() map[string]*int32 {
	return this.Ị_accountTypeToBitAmount
}
func (this *_BalanceUpdateNotification) SetAccountTypeToBitAmount(v map[string]*int32) {
	this.Ị_accountTypeToBitAmount = v
}
func (this *_BalanceUpdateNotification) SKU() *string {
	return this.Ị_SKU
}
func (this *_BalanceUpdateNotification) SetSKU(v *string) {
	this.Ị_SKU = v
}
func (this *_BalanceUpdateNotification) __type() {
}
func NewBalanceUpdateNotification() BalanceUpdateNotification {
	return &_BalanceUpdateNotification{}
}
func init() {
	var val BalanceUpdateNotification
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuel.common").RegisterShape("BalanceUpdateNotification", t, func() interface{} {
		return NewBalanceUpdateNotification()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuel.common").RegisterShape("BalanceUpdateNotification", t, func() interface{} {
		return NewBalanceUpdateNotification()
	})
}

type FuelEventNotification interface {
	__type()
	SetDoxOrderId(v *string)
	DoxOrderId() *string
	SetAdgReceiptId(v *string)
	AdgReceiptId() *string
	SetRelatedTransactionId(v *string)
	RelatedTransactionId() *string
	SetExtraContext(v *string)
	ExtraContext() *string
	SetTransactionType(v *string)
	TransactionType() *string
	SetFuelProductLine(v *string)
	FuelProductLine() *string
	SetCustomerTwitchUserId(v *string)
	CustomerTwitchUserId() *string
	SetAsin(v *string)
	Asin() *string
	SetPurchasePrice(v *int32)
	PurchasePrice() *int32
	SetListPrice(v *int32)
	ListPrice() *int32
	SetTransactionId(v *string)
	TransactionId() *string
	SetTimeOfEvent(v *string)
	TimeOfEvent() *string
	SetBroadcasterTwitchUserId(v *string)
	BroadcasterTwitchUserId() *string
}
type _FuelEventNotification struct {
	Ị_broadcasterTwitchUserId *string `coral:"broadcasterTwitchUserId" json:"broadcasterTwitchUserId"`
	Ị_asin                    *string `coral:"asin" json:"asin"`
	Ị_purchasePrice           *int32  `coral:"purchasePrice" json:"purchasePrice"`
	Ị_listPrice               *int32  `coral:"listPrice" json:"listPrice"`
	Ị_transactionId           *string `coral:"transactionId" json:"transactionId"`
	Ị_timeOfEvent             *string `coral:"timeOfEvent" json:"timeOfEvent"`
	Ị_customerTwitchUserId    *string `coral:"customerTwitchUserId" json:"customerTwitchUserId"`
	Ị_doxOrderId              *string `coral:"doxOrderId" json:"doxOrderId"`
	Ị_adgReceiptId            *string `coral:"adgReceiptId" json:"adgReceiptId"`
	Ị_relatedTransactionId    *string `coral:"relatedTransactionId" json:"relatedTransactionId"`
	Ị_extraContext            *string `coral:"extraContext" json:"extraContext"`
	Ị_transactionType         *string `coral:"transactionType" json:"transactionType"`
	Ị_fuelProductLine         *string `coral:"fuelProductLine" json:"fuelProductLine"`
}

func (this *_FuelEventNotification) BroadcasterTwitchUserId() *string {
	return this.Ị_broadcasterTwitchUserId
}
func (this *_FuelEventNotification) SetBroadcasterTwitchUserId(v *string) {
	this.Ị_broadcasterTwitchUserId = v
}
func (this *_FuelEventNotification) Asin() *string {
	return this.Ị_asin
}
func (this *_FuelEventNotification) SetAsin(v *string) {
	this.Ị_asin = v
}
func (this *_FuelEventNotification) PurchasePrice() *int32 {
	return this.Ị_purchasePrice
}
func (this *_FuelEventNotification) SetPurchasePrice(v *int32) {
	this.Ị_purchasePrice = v
}
func (this *_FuelEventNotification) ListPrice() *int32 {
	return this.Ị_listPrice
}
func (this *_FuelEventNotification) SetListPrice(v *int32) {
	this.Ị_listPrice = v
}
func (this *_FuelEventNotification) TransactionId() *string {
	return this.Ị_transactionId
}
func (this *_FuelEventNotification) SetTransactionId(v *string) {
	this.Ị_transactionId = v
}
func (this *_FuelEventNotification) TimeOfEvent() *string {
	return this.Ị_timeOfEvent
}
func (this *_FuelEventNotification) SetTimeOfEvent(v *string) {
	this.Ị_timeOfEvent = v
}
func (this *_FuelEventNotification) CustomerTwitchUserId() *string {
	return this.Ị_customerTwitchUserId
}
func (this *_FuelEventNotification) SetCustomerTwitchUserId(v *string) {
	this.Ị_customerTwitchUserId = v
}
func (this *_FuelEventNotification) DoxOrderId() *string {
	return this.Ị_doxOrderId
}
func (this *_FuelEventNotification) SetDoxOrderId(v *string) {
	this.Ị_doxOrderId = v
}
func (this *_FuelEventNotification) AdgReceiptId() *string {
	return this.Ị_adgReceiptId
}
func (this *_FuelEventNotification) SetAdgReceiptId(v *string) {
	this.Ị_adgReceiptId = v
}
func (this *_FuelEventNotification) RelatedTransactionId() *string {
	return this.Ị_relatedTransactionId
}
func (this *_FuelEventNotification) SetRelatedTransactionId(v *string) {
	this.Ị_relatedTransactionId = v
}
func (this *_FuelEventNotification) ExtraContext() *string {
	return this.Ị_extraContext
}
func (this *_FuelEventNotification) SetExtraContext(v *string) {
	this.Ị_extraContext = v
}
func (this *_FuelEventNotification) TransactionType() *string {
	return this.Ị_transactionType
}
func (this *_FuelEventNotification) SetTransactionType(v *string) {
	this.Ị_transactionType = v
}
func (this *_FuelEventNotification) FuelProductLine() *string {
	return this.Ị_fuelProductLine
}
func (this *_FuelEventNotification) SetFuelProductLine(v *string) {
	this.Ị_fuelProductLine = v
}
func (this *_FuelEventNotification) __type() {
}
func NewFuelEventNotification() FuelEventNotification {
	return &_FuelEventNotification{}
}
func init() {
	var val FuelEventNotification
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuel.common").RegisterShape("FuelEventNotification", t, func() interface{} {
		return NewFuelEventNotification()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuel.common").RegisterShape("FuelEventNotification", t, func() interface{} {
		return NewFuelEventNotification()
	})
}

type EncryptedNotification interface {
	__type()
	SetPayload(v *string)
	Payload() *string
	SetMessageType(v *string)
	MessageType() *string
	SetKey(v *string)
	Key() *string
	SetIv(v *string)
	Iv() *string
	SetKeyId(v *string)
	KeyId() *string
}
type _EncryptedNotification struct {
	Ị_key         *string `coral:"key" json:"key"`
	Ị_iv          *string `coral:"iv" json:"iv"`
	Ị_keyId       *string `coral:"keyId" json:"keyId"`
	Ị_payload     *string `coral:"payload" json:"payload"`
	Ị_messageType *string `coral:"messageType" json:"messageType"`
}

func (this *_EncryptedNotification) MessageType() *string {
	return this.Ị_messageType
}
func (this *_EncryptedNotification) SetMessageType(v *string) {
	this.Ị_messageType = v
}
func (this *_EncryptedNotification) Key() *string {
	return this.Ị_key
}
func (this *_EncryptedNotification) SetKey(v *string) {
	this.Ị_key = v
}
func (this *_EncryptedNotification) Iv() *string {
	return this.Ị_iv
}
func (this *_EncryptedNotification) SetIv(v *string) {
	this.Ị_iv = v
}
func (this *_EncryptedNotification) KeyId() *string {
	return this.Ị_keyId
}
func (this *_EncryptedNotification) SetKeyId(v *string) {
	this.Ị_keyId = v
}
func (this *_EncryptedNotification) Payload() *string {
	return this.Ị_payload
}
func (this *_EncryptedNotification) SetPayload(v *string) {
	this.Ị_payload = v
}
func (this *_EncryptedNotification) __type() {
}
func NewEncryptedNotification() EncryptedNotification {
	return &_EncryptedNotification{}
}
func init() {
	var val EncryptedNotification
	t := __reflect__.TypeOf(&val)
	__model__.LookupService("FuelAmazonBitsService").Assembly("com.amazon.fuel.common").RegisterShape("EncryptedNotification", t, func() interface{} {
		return NewEncryptedNotification()
	})
	__model__.LookupService("FuelAmazonBitsServiceSQS").Assembly("com.amazon.fuel.common").RegisterShape("EncryptedNotification", t, func() interface{} {
		return NewEncryptedNotification()
	})
}
func init() {
	var val map[string]*int32
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[string]*int32
		if f, ok := from.Interface().(map[string]*int32); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to SpecificBitTotals")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val map[string]*int32
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[string]*int32
		if f, ok := from.Interface().(map[string]*int32); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to PriorityOrderMap")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val map[string]*int32
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to map[string]*int32
		if f, ok := from.Interface().(map[string]*int32); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to SponsoredAmounts")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
