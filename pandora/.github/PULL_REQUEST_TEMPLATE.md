# Change Request Info 
## Mandatory Fields 
Change Start Date/Time: 2018-MM-DD HH:mm 
Change End Date/Time: 2018-MM-DD HH:mm 
Change Type: Product Launch/Feature Release/Bug Fix/Configuration Change/Datacenter/Network/Maintenance/Experiments/Other 
Change Risk: Low/Medium/High 
Success Criteria:   
 
## Optional Fields 
Self Approve?: Yes/No 
Evidence of Testing:    
Architectural Specification: 
Product Specification:  
Deployment Plan:  
Rollback Plan:

Jira Project Key: PAY