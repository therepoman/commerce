function branch_name() {
    git rev-parse --abbrev-ref HEAD
}

function debug_env() {
    echo -e "env:\n  AWS_PROFILE=$AWS_PROFILE\n  AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION"
}

function debug() {
    [[ $DEBUG == "1" ]] && echo -e "$@" >&2
}

function die() {
    echo -e "$@" >&2
    debug_env >&2
    exit 1
}

function validate_branch_name() {
    branch="$1"

    if [[ "$branch" == "master" ]]
    then
        die "Create a new branch before running this script."
    elif ! ( echo "$branch" | egrep -q '^[a-zA-Z0-9-]{1,64}$' )
    then
        die "Invalid characters in branch name: $branch"
    fi
}

function validate_creds() {
    if ! err=$(aws sts get-caller-identity 2>&1)
    then
        die "Error with AWS creds: $err"
    fi
}

function default_env() {
    export AWS_PROFILE=${AWS_PROFILE:-twitch-fortuna-aws}
    export AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:-us-west-2}
}

function check_exists() {
    aws cloudformation describe-stacks --stack-name $1 &>/dev/null
}

function require_exists() {
    if ! check_exists $1
    then
        die "Stack $1 doesn't exist"
    fi
}

function require_not_exists() {
    if check_exists $1
    then
        die "Stack $1 already exists"
    fi
}

function param() {
    echo "ParameterKey=$1,ParameterValue=$2"
}

function create_stack() {
    stack=$1
    template=$2
    params=$3

    confirm "Create stack '$stack' in $AWS_PROFILE/$AWS_DEFAULT_REGION"
    debug "Creating stack $stack"
    if ! err=$(aws cloudformation create-stack --stack-name $stack --template-body file://$template --parameters $params --capabilities CAPABILITY_NAMED_IAM 2>&1)
    then
        die "Error creating stack $stack: $err"
    fi

    debug "Waiting for stack $stack"
    aws cloudformation wait stack-create-complete --stack-name $stack
}

function update_stack() {
    stack=$1
    template=$2
    params=$3

    confirm "Update stack '$stack' in $AWS_PROFILE/$AWS_DEFAULT_REGION"
    debug "Updating stack $stack"
    if ! err=$(aws cloudformation update-stack --stack-name $stack --template-body file://$template --parameters $params --capabilities CAPABILITY_NAMED_IAM 2>&1)
    then
        die "Error updating stack $stack: $err"
    fi

    debug "Waiting for stack $stack"
    aws cloudformation wait stack-update-complete --stack-name $stack
}

function delete_stack() {
    stack=$1

    debug "Deleting stack $stack"
    if ! err=$(aws cloudformation delete-stack --stack-name $stack 2>&1)
    then
        die "Error deleting stack $stack: $err"
    fi

    debug "Waiting for stack delete $stack"
    aws cloudformation wait stack-delete-complete --stack-name $stack
}

function confirm() {
    p="$1"

    read -n 1 -p "$p (y/N)? " yn
    echo
    if [[ "$yn" != [yY] ]]
    then
        die "Exiting"
    fi
}