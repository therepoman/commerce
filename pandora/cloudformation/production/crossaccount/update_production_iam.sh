#!/bin/bash

export AWS_PROFILE=twitch-commerce-aws
export AWS_DEFAULT_REGION=us-west-2

cf_dir=$(dirname $0)
DEBUG=1

. $cf_dir/../../pipeline_lib.sh

function usage() {
    echo "Usage: $0"
    echo "Updates production cloudformation stack"
    echo "Configure with AWS_PROFILE and AWS_DEFAULT_REGION environment variables."
}

function main() {
    debug "Validating creds with $(debug_env)"
    validate_creds

    pipeline_stack="pandora-iam"

    debug "Checking if stacks exist: $pipeline_stack"
    require_exists $pipeline_stack

    update_stack "$pipeline_stack" "$cf_dir/production_iam.yaml" "file://$cf_dir/production_iam.json"
}

main
