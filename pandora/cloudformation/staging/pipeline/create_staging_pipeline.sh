#!/bin/bash

# Note, this script should only be run to set up the initial staging pipeline and shouldn't have to be used
# during regular development for this codebase.
cf_dir=$(dirname $0)
DEBUG=1

. $cf_dir/../../pipeline_lib.sh

function usage() {
    echo "Usage: $0"
    echo "Creates a cloudformation stack for staging!"
    echo "Configure with AWS_PROFILE and AWS_DEFAULT_REGION environment variables."
}

function main() {
    default_env
    debug "Validating creds with $(debug_env)"
    validate_creds

    pipeline_stack="pandora-pipeline"

    debug "Checking if stacks exist: $pipeline_stack"
    require_not_exists $pipeline_stack

    create_stack "$pipeline_stack" "$cf_dir/staging_pipeline.yaml" "file://$cf_dir/staging_pipeline.json"
}

main
