AWSTemplateFormatVersion: '2010-09-09'
Description: Serverless continuous deployment with CodeCommit, CodePipeline, CodeBuild, and SAM.

Parameters:
  # Params for both staging and prod
  ProjectName:
    Type: String
    Description: The name of the project being built.  This name will be used on a number of resources.
  RepoName:
    Type: String
    Description: Git repo.
  BranchName:
    Type: String
    Description: Git branch to build off of.
    Default: master
  ProdAWSAccountId:
    Type: String
    Description: Production AWS account ID
  StatsdHostPort:
    Type: String
    Description: Statsd host and port

  # Params for staging
  Environment:
    Type: String
    Description: Environment of the deployed Lambda function.
  VpcID:
    Type: AWS::EC2::VPC::Id
    Description: VPC ID for configuring CodeBuild VPC access
  SecurityGroupID:
    Type: AWS::EC2::SecurityGroup::Id
    Description: Security group ID for configuring CodeBuild VPC access
  SubnetA:
    Type: AWS::EC2::Subnet::Id
    Description: A Subnet ID for configuring CodeBuild VPC access
  SubnetB:
    Type: AWS::EC2::Subnet::Id
    Description: A Subnet ID for configuring CodeBuild VPC access
  SubnetC:
    Type: AWS::EC2::Subnet::Id
    Description: A Subnet ID for configuring CodeBuild VPC access
  PaymentsServiceHost:
    Type: String
    Description: Payments service host
  SandstormRoleArn:
    Type: String
    Description: The role that sandstorm created for us to assume
  SandstormAssumeRolePolicyArn:
    Type: String
    Description: Policy that allows us to assume the sandstorm role
  PaymentsDBUserKey:
    Type: String
    Description: The sandstorm key name to retrieve payments db user

  # Params for prod
  EnvironmentProd:
    Type: String
    Description: The environment of the deployed Lambda function.
  VpcIDProd:
    Type: String
    Description: The VPC ID for configuring CodeBuild VPC access
  SecurityGroupIDProd:
    Type: String
    Description: The security group ID for configuring CodeBuild VPC access
  SubnetAProd:
    Type: String
    Description: The Subnet ID for configuring CodeBuild VPC access
  SubnetBProd:
    Type: String
    Description: The Subnet ID for configuring CodeBuild VPC access
  SubnetCProd:
    Type: String
    Description: The Subnet ID for configuring CodeBuild VPC access
  PaymentsServiceHostProd:
    Type: String
    Description: The Payments service host
  SandstormRoleArnProd:
    Type: String
    Description: Role that sandstorm created for us to assume
  SandstormAssumeRolePolicyArnProd:
    Type: String
    Description: The policy that allows us to assume the sandstorm role
  PaymentsDBUserKeyProd:
    Type: String
    Description: Sandstorm key name to retrieve payments db user

Resources:

  S3EncryptionKey:
    Type: AWS::KMS::Key
    Properties:
      Description: KMS Key that staging pipeline can use for encryption when deploying to prod account
      KeyPolicy:
        Statement:
          -
            Sid: Admin of key permissions
            Effect: Allow
            Principal:
              AWS: !Sub "arn:aws:iam::${AWS::AccountId}:root"
            Action:
              - kms:Create*
              - kms:Describe*
              - kms:Enable*
              - kms:List*
              - kms:Put*
              - kms:Update*
              - kms:Revoke*
              - kms:Disable*
              - kms:Get*
              - kms:Delete*
              - kms:ScheduleKeyDeletion
              - kms:CancelKeyDeletion
            Resource: "*"
          -
            Sid: Allows both staging and production to use this key
            Effect: Allow
            Principal:
              AWS:
                - !Sub "arn:aws:iam::${ProdAWSAccountId}:root"
                - !Sub "arn:aws:iam::${AWS::AccountId}:root"
            Action:
              - kms:Encrypt
              - kms:Decrypt
              - kms:ReEncrypt*
              - kms:GenerateDataKey*
              - kms:DescribeKey
            Resource: "*"

  S3EncryptionKeyAlias:
    Type: AWS::KMS::Alias
    Properties:
      AliasName: alias/pandoraS3EncryptionKey
      TargetKeyId: !Ref S3EncryptionKey

  CodeBuildServiceRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Action:
              - sts:AssumeRole
            Principal:
              Service:
                - codebuild.amazonaws.com

  CodeBuildGHERole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Action:
              - sts:AssumeRole
            Principal:
              Service:
                - codebuild.amazonaws.com

  CodeBuildServicePolicy:
    Type: AWS::IAM::Policy
    Properties:
      PolicyName: CodeBuildServicePolicy
      Roles:
        - !Ref CodeBuildServiceRole
      PolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Action:
              - kms:Encrypt
              - kms:Decrypt
              - kms:ReEncrypt*
              - kms:GenerateDataKey*
              - kms:DescribeKey
            Resource: !GetAtt S3EncryptionKey.Arn
          - Effect: Allow
            Action:
              - logs:CreateLogGroup
              - logs:CreateLogStream
              - logs:PutLogEvents
            Resource: "arn:aws:logs:*:*:*"
          - Effect: Allow
            Action:
              - s3:ListBucket
              - s3:ListObjects
              - s3:GetObject
              - s3:GetObjectVersion
              - s3:PutObject
            Resource:
              - !Sub "arn:aws:s3:::codepipeline-${AWS::Region}-*/*"
              - Fn::Sub:
                - "arn:aws:s3:::${DeploymentArtifactBucketName}/*"
                - DeploymentArtifactBucketName: !ImportValue pandoraDeploymentArtifactBucket

  CodeBuildGHEPolicy:
    Type: AWS::IAM::Policy
    Properties:
      PolicyName: CodeBuildGHEPolicy
      Roles:
        - !Ref CodeBuildGHERole
      PolicyDocument:
        Version: 2012-10-17
        Statement:
          # Allow CodeBuild to execute in VPC
          - Effect: Allow
            Action:
              - ec2:CreateNetworkInterface
              - ec2:DescribeDhcpOptions
              - ec2:DescribeNetworkInterfaces
              - ec2:DeleteNetworkInterface
              - ec2:DescribeSubnets
              - ec2:DescribeSecurityGroups
              - ec2:DescribeVpcs
            Resource: "*"
          - Effect: Allow
            Action:
              - ec2:CreateNetworkInterfacePermission
            Resource: !Sub "arn:aws:ec2:${AWS::Region}:${AWS::AccountId}:network-interface/*"
            Condition:
              StringEquals:
                "ec2:AuthorizedService": "codebuild.amazonaws.com"
                "ec2:Subnet":
                  - !Sub "arn:aws:ec2:${AWS::Region}:${AWS::AccountId}:subnet/${SubnetA}"
                  - !Sub "arn:aws:ec2:${AWS::Region}:${AWS::AccountId}:subnet/${SubnetB}"
                  - !Sub "arn:aws:ec2:${AWS::Region}:${AWS::AccountId}:subnet/${SubnetC}"
          - Effect: Allow
            Action:
              - s3:GetObject
              - s3:GetObjectVersion
              - s3:PutObject
            Resource:
              - Fn::Sub:
                - "arn:aws:s3:::${GHEArtifactBucketName}/*"
                - GHEArtifactBucketName: !ImportValue pandoraGHEArtifactBucket
          - Effect: Allow
            Action:
              - logs:CreateLogGroup
              - logs:CreateLogStream
              - logs:PutLogEvents
            Resource: "arn:aws:logs:*:*:*"

  # CodeBuild that triggers as part of CodePipeline process
  CodeBuildProjectPipeline:
    Type: AWS::CodeBuild::Project
    Properties:
      Name: !Sub "${ProjectName}-pipeline"
      Description: Support builds in the pipeline
      ServiceRole: !GetAtt CodeBuildServiceRole.Arn
      EncryptionKey: !Ref S3EncryptionKey
      Artifacts:
        Type: CODEPIPELINE
      Environment:
        Type: linuxContainer
        # ComputeType options: BUILD_GENERAL1_SMALL, BUILD_GENERAL1_MEDIUM, BUILD_GENERAL1_LARGE
        ComputeType: BUILD_GENERAL1_SMALL
        # Run `aws codebuild list-curated-environment-images` for a complete list of images provided.
        Image: aws/codebuild/golang:1.10
        EnvironmentVariables:
          - Name: BUILD_ARTIFACT_BUCKET
            Value: !ImportValue pandoraDeploymentArtifactBucket
            Value: !ImportValue pandoraDeploymentArtifactBucket
      Source:
        Type: CODEPIPELINE
        BuildSpec: buildspec.yml
      TimeoutInMinutes: 10

  # CodeBuild that triggers as part of GitHub Enterprise pushes to branches
  CodeBuildProjectGHE:
    Type: AWS::CodeBuild::Project
    DependsOn: CodeBuildGHEPolicy
    Properties:
      Name: !Sub "${ProjectName}-ghe"
      Description: Support GHE build status webhooks on PR pushes
      ServiceRole: !GetAtt CodeBuildGHERole.Arn
      Artifacts:
        Type: NO_ARTIFACTS
      Environment:
        Type: linuxContainer
        # ComputeType options: BUILD_GENERAL1_SMALL, BUILD_GENERAL1_MEDIUM, BUILD_GENERAL1_LARGE
        ComputeType: BUILD_GENERAL1_SMALL
        # Run `aws codebuild list-curated-environment-images` for a complete list of images provided.
        Image: aws/codebuild/golang:1.10
        EnvironmentVariables:
          - Name: BUILD_ARTIFACT_BUCKET
            Value: !ImportValue pandoraGHEArtifactBucket
      Source:
        Type: GITHUB_ENTERPRISE
        Location: !Sub "https://git-aws.internal.justin.tv/commerce/${RepoName}"
        GitCloneDepth: 1
        BuildSpec: buildspec.yml
      VpcConfig:
        SecurityGroupIds:
          - !Ref SecurityGroupID
        Subnets:
          - !Ref SubnetA
          - !Ref SubnetB
          - !Ref SubnetC
        VpcId: !Ref VpcID
      TimeoutInMinutes: 10
      BadgeEnabled: true

  # CodePipeline
  CodePipelineServiceRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Action:
              - sts:AssumeRole
            Principal:
              Service:
                - codepipeline.amazonaws.com

  CodePipelineServicePolicy:
    # This policy orchestrates CloudFormation, CodeBuild, and CodeCommit.
    Type: AWS::IAM::Policy
    Properties:
      PolicyName: CodePipelineServicePolicy
      Roles:
        - !Ref CodePipelineServiceRole
      PolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Action:
              - sts:AssumeRole
            Resource: !Sub "arn:aws:iam::${ProdAWSAccountId}:role/${ProjectName}-codepipeline-cross-account-role-access"
          - Effect: Allow
            Action:
              - logs:CreateLogGroup
              - logs:CreateLogStream
              - logs:PutLogEvents
            Resource: "arn:aws:logs:*:*:*"
          - Effect: Allow
            Action:
              - kms:Encrypt
              - kms:Decrypt
              - kms:ReEncrypt*
              - kms:GenerateDataKey*
              - kms:DescribeKey
            Resource: !GetAtt S3EncryptionKey.Arn
          - Effect: Allow
            Action:
              - s3:ListBucket
              - s3:ListObjects
              - s3:GetObject
              - s3:GetObjectVersion
              - s3:PutObject
            Resource:
              - Fn::Sub:
                - "arn:aws:s3:::${DeploymentArtifactBucketName}/*"
                - DeploymentArtifactBucketName: !ImportValue pandoraDeploymentArtifactBucket
          - Effect: Allow
            Action:
              - cloudformation:CreateStack
              - cloudformation:DescribeStacks
              - cloudformation:DeleteStack
              - cloudformation:UpdateStack
              - cloudformation:CreateChangeSet
              - cloudformation:ExecuteChangeSet
              - cloudformation:DeleteChangeSet
              - cloudformation:DescribeChangeSet
              - cloudformation:ValidateTemplate
            Resource: !Sub "arn:aws:cloudformation:${AWS::Region}:${AWS::AccountId}:stack/${ProjectName}-app/*"
          - Effect: Allow
            Action:
              - codebuild:BatchGetBuilds
              - codebuild:StartBuild
              - codebuild:StopBuild
            Resource:
              - !GetAtt CodeBuildProjectPipeline.Arn
          - Effect: Allow
            Action:
              - iam:PassRole
            Resource: !GetAtt CloudFormationServiceRole.Arn
          - Effect: Allow
            Action:
              - codecommit:GetBranch
              - codecommit:GetCommit
              - codecommit:UploadArchive
              - codecommit:GetUploadArchiveStatus
              - codecommit:CancelUploadArchive
            Resource:
              - !Sub "arn:aws:codecommit:${AWS::Region}:${AWS::AccountId}:${RepoName}"

  CloudFormationServiceRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Action:
              - sts:AssumeRole
            Principal:
              Service:
                - cloudformation.amazonaws.com

  CloudFormationServicePolicy:
    # This policy deploys the project's SAM template and needs permissions to create all services defined there.
    # A number of common blocks are provided as examples.
    Type: AWS::IAM::Policy
    Properties:
      PolicyName: CloudFormationServicePolicy
      Roles:
        - !Ref CloudFormationServiceRole
      PolicyDocument:
        Version: 2012-10-17
        Statement:
          # Allow Lambda to execute in VPC
          - Effect: Allow
            Action:
              - ec2:DescribeNetworkInterfaces
              - ec2:DescribeSecurityGroups
              - ec2:DescribeSubnets
              - ec2:DescribeVpcs
            Resource: "*"
          - Effect: Allow
            Action:
              - logs:CreateLogGroup
              - logs:CreateLogStream
              - logs:PutLogEvents
            Resource: "arn:aws:logs:*:*:*"
          - Effect: Allow
            Action:
              - cloudformation:CreateChangeSet
              - cloudformation:ListChangeSets
            Resource: !Sub "arn:aws:cloudformation:${AWS::Region}:aws:transform/*"
          - Effect: Allow
            Action:
              - s3:GetObject
            Resource:
              - !Sub "arn:aws:s3:::codepipeline-${AWS::Region}-*/*"
              - Fn::Sub:
                - "arn:aws:s3:::${DeploymentArtifactBucketName}/*"
                - DeploymentArtifactBucketName: !ImportValue pandoraDeploymentArtifactBucket
          - Effect: Allow
            Action:
              - iam:AttachRolePolicy
              - iam:CreateRole
              - iam:DeleteRole
              - iam:DeleteRolePolicy
              - iam:DetachRolePolicy
              - iam:GetRole
              - iam:PassRole
              - iam:PutRolePolicy
            Resource: !Sub "arn:aws:iam::${AWS::AccountId}:role/${ProjectName}-*"
          - Effect: Allow
            Action:
              - lambda:AddPermission
              - lambda:CreateFunction
              - lambda:DeleteFunction
              - lambda:GetFunction
              - lambda:GetFunctionConfiguration
              - lambda:InvokeFunction
              - lambda:RemovePermission
              - lambda:TagResource
              - lambda:UntagResource
              - lambda:UpdateFunctionCode
              - lambda:UpdateFunctionConfiguration
            Resource: !Sub "arn:aws:lambda:${AWS::Region}:${AWS::AccountId}:function:*"
          # This block is necessary for any event mapping
          - Effect: Allow
            Action:
              - lambda:CreateEventSourceMapping
              - lambda:DeleteEventSourceMapping
              - lambda:GetEventSourceMapping
            Resource: "*"
          # Lambda tagging
          - Effect: Allow
            Action:
              - lambda:ListTags
              - lambda:TagResource
              - lambda:UntagResource
            Resource: "*"
          # Other examples below
          # API Gateway (REST events)
#          - Effect: Allow
#            Action:
#              - apigateway:*
#            Resource: !Sub "arn:aws:apigateway:${AWS::Region}::/*"
          # CloudWatch event triggers
#          - Effect: Allow
#            Action:
#              - events:DeleteRule
#              - events:DescribeRule
#              - events:DisableRule
#              - events:EnableRule
#              - events:PutRule
#              - events:PutTargets
#              - events:RemoveTargets
#            Resource: !Sub "arn:aws:events:${AWS::Region}:${AWS::AccountId}:rule/${ProjectName}-*"
          # Kinesis stream event mapping
#          - Effect: Allow
#            Action:
#              - kinesis:DescribeStreams
#              - kinesis:DescribeStream
#              - kinesis:ListStreams
#              - kinesis:GetShardIterator
#              - kinesis:GetRecords
#            Resource: !Sub "arn:aws:kinesis:${AWS::Region}:${AWS::AccountId}:stream/*"
          # DynamoBD event mapping
#          - Effect: Allow
#            Action:
#              - dynamodb:DescribeStreams
#              - dynamodb:DescribeStream
#              - dynamodb:ListStreams
#              - dynamodb:GetShardIterator
#              - dynamodb:GetRecords
#            Resource: !Sub "arn:aws:dynamodb:${AWS::Region}:${AWS::AccountId}:table/*"
          # DynamoDB tables
#          - Effect: Allow
#            Action:
#              - dynamodb:CreateTable
#              - dynamodb:DeleteTable
#              - dynamodb:DescribeTable
#              - dynamodb:DescribeTimeToLive
#              - dynamodb:ListTagsOfResource
#              - dynamodb:TagResource
#              - dynamodb:UntagResource
#              - dynamodb:UpdateTable
#              - dynamodb:UpdateTimeToLive
#            Resource: !Sub "arn:aws:dynamodb:${AWS::Region}:${AWS::AccountId}:table/${ProjectName}-*"
          # DynamoDB table tagging
#          - Effect: Allow
#            Action:
#              - dynamodb:ListTags
#              - dynamodb:TagResource
#              - dynamodb:UntagResource
#            Resource: "*"
          # DynamoDB autoscaling
#          - Effect: Allow
#            Action:
#              - application-autoscaling:*
#            Resource: *
          # S3
#          - Effect: Allow
#            Action:
#              - s3:CreateBucket
#              - s3:GetBucketAcl
#              - s3:GetBucketNotification
#              - s3:GetBucketTagging
#              - s3:PutBucketAcl
#              - s3:PutBucketNotification
#              - s3:PutBucketTagging
#            Resource: "*"
          # SQS
#          - Effect: Allow
#            Action:
#              - sqs:AddPermission
#              - sqs:CreateQueue
#              - sqs:DeleteQueue
#              - sqs:GetQueueAttributes
#              - sqs:RemovePermission
#              - sqs:SetQueueAttributes
#            Resource: !Sub "arn:aws:sqs:${AWS::Region}:${AWS::AccountId}:${ProjectName}-*"
          # ElasticsearchService
#          - Effect: Allow
#            Action:
#              - es:*
#            Resource: "*"

  CodePipeline:
    Type: AWS::CodePipeline::Pipeline
    DependsOn: CodePipelineServicePolicy
    Properties:
      Name: !Ref ProjectName
      RoleArn: !GetAtt CodePipelineServiceRole.Arn
      ArtifactStore:
        Type: S3
        Location: !ImportValue pandoraDeploymentArtifactBucket
        EncryptionKey:
          Id: !GetAtt S3EncryptionKey.Arn
          Type: KMS
      Stages:
        - Name: Source
          Actions:
            - Name: Source
              ActionTypeId:
                Category: Source
                Owner: AWS
                Provider: CodeCommit
                Version: 1
              Configuration:
                RepositoryName: !Ref RepoName
                BranchName: !Ref BranchName
              OutputArtifacts:
                - Name: SourceCodeOutputArtifact
              RunOrder: 1
        - Name: Build
          Actions:
            - Name: CodeBuild
              ActionTypeId:
                Category: Build
                Owner: AWS
                Version: 1
                Provider: CodeBuild
              InputArtifacts:
                - Name: SourceCodeOutputArtifact
              OutputArtifacts:
                - Name: BuildOutputArtifact
              Configuration:
                ProjectName: !Sub "${ProjectName}-pipeline"
              RunOrder: 1
        - Name: Staging
          Actions:
            - Name: CreateStageChangeSet
              ActionTypeId:
                Category: Deploy
                Owner: AWS
                Version: 1
                Provider: CloudFormation
              InputArtifacts:
                - Name: BuildOutputArtifact
              Configuration:
                ActionMode: CHANGE_SET_REPLACE
                ChangeSetName: !Sub "${ProjectName}-staging-changeset"
                RoleArn: !GetAtt CloudFormationServiceRole.Arn
                Capabilities: CAPABILITY_IAM
                StackName: !Sub "${ProjectName}-app"
                ParameterOverrides: !Sub |
                  {
                    "PaymentsServiceHost": "${PaymentsServiceHost}",
                    "StatsdHostPort": "${StatsdHostPort}",
                    "App": "${ProjectName}",
                    "Environment": "${Environment}",
                    "PaymentsDBUserKey": "${PaymentsDBUserKey}",
                    "SecurityGroupIDs": "${SecurityGroupID}",
                    "SubnetIDs": "${SubnetA},${SubnetB},${SubnetC}",
                    "SandstormRoleArn": "${SandstormRoleArn}",
                    "SandstormAssumeRolePolicyArn": "${SandstormAssumeRolePolicyArn}"
                  }
                TemplatePath: BuildOutputArtifact::packaged_sam.yaml
              RunOrder: 1
            - Name: ApprovalForStaging
              ActionTypeId:
                Category: Approval
                Owner: AWS
                Version: 1
                Provider: Manual
              RunOrder: 2
            - Name: ExecuteStageChangeSet
              ActionTypeId:
                Category: Deploy
                Owner: AWS
                Version: 1
                Provider: CloudFormation
              Configuration:
                StackName: !Sub "${ProjectName}-app"
                ActionMode: CHANGE_SET_EXECUTE
                ChangeSetName: !Sub "${ProjectName}-staging-changeset"
              RunOrder: 3
        - Name: PROD
          Actions:
            - Name: CreateProdChangeSet
              RoleArn: !Sub "arn:aws:iam::${ProdAWSAccountId}:role/${ProjectName}-codepipeline-cross-account-role-access"
              ActionTypeId:
                Category: Deploy
                Owner: AWS
                Version: 1
                Provider: CloudFormation
              InputArtifacts:
                - Name: BuildOutputArtifact
              Configuration:
                ActionMode: CHANGE_SET_REPLACE
                ChangeSetName: !Sub "${ProjectName}-prod-changeset"
                RoleArn: !Sub "arn:aws:iam::${ProdAWSAccountId}:role/${ProjectName}-codepipeline-cloudformation-deploy-role"
                Capabilities: CAPABILITY_IAM
                StackName: !Sub "${ProjectName}-app"
                ParameterOverrides: !Sub |
                  {
                    "PaymentsServiceHost": "${PaymentsServiceHostProd}",
                    "StatsdHostPort": "${StatsdHostPort}",
                    "App": "${ProjectName}",
                    "Environment": "${EnvironmentProd}",
                    "PaymentsDBUserKey": "${PaymentsDBUserKeyProd}",
                    "SecurityGroupIDs": "${SecurityGroupIDProd}",
                    "SubnetIDs": "${SubnetAProd},${SubnetBProd},${SubnetCProd}",
                    "SandstormRoleArn": "${SandstormRoleArnProd}",
                    "SandstormAssumeRolePolicyArn": "${SandstormAssumeRolePolicyArnProd}"
                  }
                TemplatePath: BuildOutputArtifact::packaged_sam.yaml
              RunOrder: 1
            - Name: ApprovalForProduction
              ActionTypeId:
                Category: Approval
                Owner: AWS
                Version: 1
                Provider: Manual
              RunOrder: 2
            - Name: ExecuteProdChangeSet
              RoleArn: !Sub "arn:aws:iam::${ProdAWSAccountId}:role/${ProjectName}-codepipeline-cross-account-role-access"
              ActionTypeId:
                Category: Deploy
                Owner: AWS
                Version: 1
                Provider: CloudFormation
              Configuration:
                StackName: !Sub "${ProjectName}-app"
                ActionMode: CHANGE_SET_EXECUTE
                ChangeSetName: !Sub "${ProjectName}-prod-changeset"
                RoleArn: !Sub "arn:aws:iam::${ProdAWSAccountId}:role/${ProjectName}-codepipeline-cloudformation-deploy-role"
              RunOrder: 3
