#!/bin/bash

cf_dir=$(dirname $0)
DEBUG=1

. $cf_dir/../../pipeline_lib.sh

function usage() {
    echo "Usage: $0"
    echo "Updates the staging cloudformation stack"
    echo "Configure with AWS_PROFILE and AWS_DEFAULT_REGION environment variables."
}

function main() {
    default_env
    debug "Validating creds with $(debug_env)"
    validate_creds

    pipeline_stack="pandora-pipeline"

    debug "Checking if stacks exist: $pipeline_stack"
    require_exists $pipeline_stack

    update_stack "$pipeline_stack" "$cf_dir/staging_pipeline.yaml" "file://$cf_dir/staging_pipeline.json"
}

main
