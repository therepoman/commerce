# cloudformation

Steps and scripts to create or update your staging pipeline and production IAM resources. This documents how this repo was initially set up.
You do not need to follow these instructions if you are doing regular development.

The staging account will contain the AWS CodePipeline that deploys Lambda functions to both staging and production. It assumes
a production role in order to deploy to that environment. The CloudFormation scripts should be properly configured with all the
correct permissions.

## Prerequisites

Make sure your staging and production AWS profile keys are set up correctly before running any scripts.

## Set Up

### Pipeline Deployment Bucket in Staging Acc

We created two S3 buckets with Cloudformation, defined in `staging_base.yaml`. This is being used by the pipeline for CodePipeline and Lambda artifacts.
Run `./create_staging_base.sh`

### AWS CodeCommit

We need to mirror this repository into staging AWS CodeCommit.

Run the second script (`subscribe.sh`) mentioned [here](https://git-aws.internal.justin.tv/vod/github-import).
Now CodeCommit can pick up any new code pushed to any branch and CodePipeline can use it as a source.

The first script `build.sh` only needs to be run once per AWS account, so ignore it since it should already have been run by the time you read this.

### Cross Account IAM in Production Acc

Spin up the cross account IAM roles and policies with `./create_production_iam.sh`. After you spin up the staging pipeline, come back to `production_iam.json` and update the `StagingKMSKeyArn` param. 

### Create Pipeline in Staging Acc

Spin up the staging pipeline stack defined by the Cloudformation configuration defined in `staging_pipeline.yaml`

```
./create_staging_pipeline.sh
``` 

### Update Pipeline

If you make a change to the pipeline configuration defined in `primary.yaml`, you'll need to run the update script.
```
./update_staging_pipeline.sh
```

## Tampermonkey Script

If you want your CodePipelines to flow from left to right instead of vertically, you can use [this Tampermonkey script](https://gist.githubusercontent.com/mfield/9ee5d23871f9debfa5c70461d715c482/raw).

## GitHub Enterprise CodeBuild Hooks

The CodeBuild hooks have to be set up manually after all the resources in `primary.yaml` are spun up. 
Generate a GitHub `Personal Access Token` and update the CodeBuild `pandora-ghe` project's web hook, as well as adding new hook on GHE repo.

