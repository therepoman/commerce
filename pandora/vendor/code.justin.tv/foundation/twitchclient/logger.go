package twitchclient

import (
	"context"
	"log"
)

type defaultLogger struct{}

func (l *defaultLogger) DebugCtx(ctx context.Context, params ...interface{}) {}
func (l *defaultLogger) Debug(params ...interface{})                         {}

func (l *defaultLogger) LogCtx(ctx context.Context, params ...interface{}) {
	log.Println(params)
}

func (l *defaultLogger) Log(params ...interface{}) {
	log.Println(params)
}
