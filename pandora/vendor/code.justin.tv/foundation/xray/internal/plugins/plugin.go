package plugins

import (
	"github.com/aws/aws-sdk-go/aws/ec2metadata"
)

var InstancePluginMetadata *PluginMetadata = &PluginMetadata{}

type PluginMetadata struct {
	IdentityDocument  *ec2metadata.EC2InstanceIdentityDocument
	BeanstalkMetadata *BeanstalkMetadata
	ECSContainerName  string
}

type BeanstalkMetadata struct {
	Environment  string `json:"environment_name"`
	VersionLabel string `json:"version_label"`
	DeploymentID int    `json:"deployment_id"`
}
