package chitin

import (
	"net"
	"time"
)

type listener struct {
	net.Listener

	keepalivePeriod time.Duration
}

func (l listener) Accept() (net.Conn, error) {
	conn, err := l.Listener.Accept()
	if err != nil {
		return conn, err
	}

	type ka interface {
		SetKeepalive(bool) error
		SetKeepalivePeriod(time.Duration) error
	}

	if l.keepalivePeriod > 0 {
		if kaConn, ok := conn.(ka); ok {
			err := kaConn.SetKeepalive(true)
			err = kaConn.SetKeepalivePeriod(l.keepalivePeriod)
			if err != nil {
				// There's not much we can do here. It's not serious enough to
				// close the connection, and it might be too frequent to log.
				// We'll settle with keeping errcheck happy.
			}
		}
	}
	return conn, nil
}
