# Chitin releases

## v0.9.0

This marks the first official release of the chitin package, nearly 25 months
after its inception.

The most important recent change is to support the Go core packages' use of
Context. Starting with Go 1.7, http.Request values include a context.Context
value tied to the lifecycle of the request. Upgrade to at least Go 1.7 and
chitin v0.9.0 to access the following:

- The chitin RoundTripper can now be shared between many independent requests,
since it can access a request-scoped Context value from the http.Request
rather than having one curried via the call to chitin.RoundTripper. To use it
in this way, create the RoundTripper with context.Background.

- The chitin Handler now attaches its additional values (for Trace
instrumentation) to that Context, making it available via http.Request's
Context method. This should allow chitin.Handler to be used with other
middleware that uses Go 1.7's context support.

Additionally, depending on code.justin.tv/common/chitin no longer creates a
transitive dependency on google.golang.org/grpc/metadata. The dependency now
only exists when using chitin to instrument gRPC interactions via the
grpctrace package, in which case the application would already have a
dependency on the gRPC packages.
