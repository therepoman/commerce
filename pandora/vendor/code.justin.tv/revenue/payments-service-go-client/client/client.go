// Package payments contains a client for the Rails payments-service service intended for use by Visage and possibly
// other internal twitch services. It also contains shared models for response objects.
package payments

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"

	"net/url"

	"strings"

	"time"

	"strconv"

	"code.justin.tv/foundation/twitchclient"
)

const (
	defaultStatSampleRate = 0.1
	defaultTimingXactName = "payments-service-rails"
)

type Client interface {
	// For the checkout flow on web-client
	GetPurchase(ctx context.Context, userID string, productShortName string, reqOpts *twitchclient.ReqOpts) (*GetPurchaseResponse, error)
	GetInitiatePurchase(ctx context.Context, userID string, productShortName string, request InitiatePurchaseRequest, reqOpts *twitchclient.ReqOpts) (*GetInitiatePurchaseResponse, error)
	CompletePurchase(ctx context.Context, userID string, productShortName string, request CompletePurchaseRequest, reqOpts *twitchclient.ReqOpts) (*CompletePurchaseResponse, error)
	CancelPurchase(ctx context.Context, userID string, productShortName string, request CancelPurchaseRequest, reqOpts *twitchclient.ReqOpts) error
	DoNotRenewPurchase(ctx context.Context, userID string, productShortName string, reason string, reqOpts *twitchclient.ReqOpts) error
	GetProductChannelName(ctx context.Context, productShortName string, reqOpts *twitchclient.ReqOpts) (*GetProductChannelNameResponse, error)
	ProcessReceipt(ctx context.Context, userID string, request ProcessReceiptRequest, reqOpts *twitchclient.ReqOpts) (*ProcessReceiptResponse, error)

	// For PayPal Bits checkout
	GetUserResidence(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*UserResidenceResponse, error)
	SetUserResidence(ctx context.Context, userID string, request UserResidenceRequest, reqOpts *twitchclient.ReqOpts) (*UserResidenceResponse, error)
	GetPayPalBillingAgreement(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*PayPalBillingAgreement, error)
	SetPayPalBillingAgreement(ctx context.Context, userID string, request SetBillingAgreementRequest, reqOpts *twitchclient.ReqOpts) (*PayPalBillingAgreement, error)
	GetInitPayPalBillingAgreementURL(ctx context.Context, userID string, returnURL string, cancelURL string, locale string, reqOpts *twitchclient.ReqOpts) (*BillingAgreementURL, error)
	PreviewBitsPurchase(ctx context.Context, userID string, ipCountryCode string, productID string, reqOpts *twitchclient.ReqOpts) (*ProductPricing, error)
	PurchaseBits(ctx context.Context, userID string, productID string, request BitsPurchaseRequest, reqOpts *twitchclient.ReqOpts) (*BitsPurchaseResponse, error)
	GetProductsPrice(ctx context.Context, userID string, ipCountryCode string, productIDs []string, reqOpts *twitchclient.ReqOpts) ([]ProductPricing, error)

	// For Mobile Subs
	GetUnacknowledgedEvents(ctx context.Context, request GetUnacknowledgedEventsRequest, reqOpts *twitchclient.ReqOpts) (*GetUnacknowledgedEventsResponse, error)
	CancelSubscription(ctx context.Context, id string, request CancelSubscriptionRequest, reqOpts *twitchclient.ReqOpts) (*CancelSubscriptionResponse, error)

	// For payments management page
	GetPaymentMethods(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*GetPaymentMethodsResponse, error)
	GetPaymentMethodConfigs(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*GetPaymentMethodConfigsResponse, error)
	SetDefaultPaymentMethod(ctx context.Context, request SetDefaultPaymentMethodRequest, reqOpts *twitchclient.ReqOpts) (*SetDefaultPaymentMethodResponse, error)
	DeleteDefaultPaymentMethod(ctx context.Context, request DeleteDefaultPaymentMethodRequest, reqOpts *twitchclient.ReqOpts) (*DeleteDefaultPaymentMethodResponse, error)
	GetPaymentTransactionIDs(ctx context.Context, request GetPaymentTransactionsRequest, reqOpts *twitchclient.ReqOpts) (*GetPaymentTransactionsResponse, error)
	GetPaymentTransactionByID(ctx context.Context, id string, reqOpts *twitchclient.ReqOpts) (*PaymentTransaction, error)
	GetRecurringPaymentDetail(ctx context.Context, subscription_id string, reqOpts *twitchclient.ReqOpts) (*RecurringPaymentDetail, error)

	// Various Payments DB models
	GetPurchaseProfile(ctx context.Context, purchaseProfileID string, extraKeys []string, reqOpts *twitchclient.ReqOpts) (*PurchaseProfile, error)
	GetPurchaseProfilesByPurchasableIDs(ctx context.Context, purchasableIDs []string, reqOpts *twitchclient.ReqOpts) (*PurchaseProfiles, error)
	GetSubscriptions(ctx context.Context, request GetSubscriptionsRequest, reqOpts *twitchclient.ReqOpts) (*GetSubscriptionsResponse, error)

	// For Mass Mystery Gifting - order fulfillment updates
	UpdateOrderLineFulfillmentStatus(ctx context.Context, request UpdateOrderLineFulfillmentStatusRequest, reqOpts *twitchclient.ReqOpts) (*UpdateOrderLineFulfillmentStatusResponse, error)
}

type client struct {
	twitchclient.Client
}

// NewClient creates a new client for use in making service calls.
func NewClient(conf twitchclient.ClientConf) (Client, error) {
	if conf.TimingXactName == "" {
		conf.TimingXactName = defaultTimingXactName
	}
	twitchClient, err := twitchclient.NewClient(conf)
	return &client{twitchClient}, err
}

func (c *client) GetPurchase(ctx context.Context, userID string, productShortName string, reqOpts *twitchclient.ReqOpts) (*GetPurchaseResponse, error) {
	apiURL := fmt.Sprintf("/users/%s/products/%s/purchase", userID, productShortName)

	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_purchase",
		StatSampleRate: defaultStatSampleRate,
	})

	var getPurchaseResponse GetPurchaseResponse
	_, err = c.DoJSON(ctx, &getPurchaseResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getPurchaseResponse, nil
}

func (c *client) GetInitiatePurchase(ctx context.Context, userID string, productShortName string, request InitiatePurchaseRequest, reqOpts *twitchclient.ReqOpts) (*GetInitiatePurchaseResponse, error) {
	apiURL := fmt.Sprintf("/users/%s/products/%s/purchase/initiate?platform=%s&is_gift=%v&recipient_id=%s&with_special_promo=%s",
		userID, productShortName, request.Platform, request.IsGift, request.RecipientID, request.WithSpecialPromo)

	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_initiate_purchase",
		StatSampleRate: defaultStatSampleRate,
	})

	var getInitiatePurchaseResponse GetInitiatePurchaseResponse
	_, err = c.DoJSON(ctx, &getInitiatePurchaseResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getInitiatePurchaseResponse, nil
}

func (c *client) CompletePurchase(ctx context.Context, userID string, productShortName string, request CompletePurchaseRequest, reqOpts *twitchclient.ReqOpts) (*CompletePurchaseResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	apiURL := fmt.Sprintf("/users/%s/products/%s/purchase/complete", userID, productShortName)

	req, err := c.NewRequest("POST", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.complete_purchase",
		StatSampleRate: defaultStatSampleRate,
	})

	var completePurchaseResponse CompletePurchaseResponse
	_, err = c.DoJSON(ctx, &completePurchaseResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &completePurchaseResponse, nil
}

func (c *client) CancelPurchase(ctx context.Context, userID string, productShortName string, request CancelPurchaseRequest, reqOpts *twitchclient.ReqOpts) error {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return err
	}

	apiURL := fmt.Sprintf("/users/%s/products/%s/purchase/cancel", userID, productShortName)

	req, err := c.NewRequest("PUT", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.cancel_purchase",
		StatSampleRate: defaultStatSampleRate,
	})

	var cancelPurchaseResponse map[string]interface{}
	_, err = c.DoJSON(ctx, &cancelPurchaseResponse, req, combinedReqOpts)
	if err != nil {
		return err
	}

	return nil
}

func (c *client) DoNotRenewPurchase(ctx context.Context, userID string, productShortName string, reason string, reqOpts *twitchclient.ReqOpts) error {
	body := make(map[string]string)
	if reason != "" {
		body["reason"] = reason
	}

	jsonBody, err := json.Marshal(body)
	if err != nil {
		return err
	}

	apiURL := fmt.Sprintf("/users/%s/products/%s/purchase/do_not_renew", userID, productShortName)

	req, err := c.NewRequest("PUT", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.do_not_renew_purchase",
		StatSampleRate: defaultStatSampleRate,
	})

	var doNotRenewPurchaseResponse map[string]interface{}
	_, err = c.DoJSON(ctx, &doNotRenewPurchaseResponse, req, combinedReqOpts)
	if err != nil {
		return err
	}

	return nil
}

func (c *client) GetProductChannelName(ctx context.Context, productShortName string, reqOpts *twitchclient.ReqOpts) (*GetProductChannelNameResponse, error) {
	apiURL := fmt.Sprintf("/products/%s/channel_name", productShortName)
	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_product_channel_name",
		StatSampleRate: defaultStatSampleRate,
	})

	var getProductChannelNameResponse GetProductChannelNameResponse
	_, err = c.DoJSON(ctx, &getProductChannelNameResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getProductChannelNameResponse, nil
}

func (c *client) ProcessReceipt(ctx context.Context, userID string, request ProcessReceiptRequest, reqOpts *twitchclient.ReqOpts) (*ProcessReceiptResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	apiURL := fmt.Sprintf("/users/%s/receipt", userID)
	req, err := c.NewRequest("POST", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.process_receipt",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp ProcessReceiptResponse
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) GetUnacknowledgedEvents(ctx context.Context, request GetUnacknowledgedEventsRequest, reqOpts *twitchclient.ReqOpts) (*GetUnacknowledgedEventsResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	apiURL := "/subscriptions/unacknowledged"
	req, err := c.NewRequest("GET", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.unacknowledged_events",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp GetUnacknowledgedEventsResponse
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) CancelSubscription(ctx context.Context, id string, request CancelSubscriptionRequest, reqOpts *twitchclient.ReqOpts) (*CancelSubscriptionResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	apiURL := fmt.Sprintf("/subscriptions/%s/cancel", id)
	req, err := c.NewRequest("PUT", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.cancel_subscriptions",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp CancelSubscriptionResponse
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}
func (c *client) GetUserResidence(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*UserResidenceResponse, error) {
	apiURL := fmt.Sprintf("/users/%s/residence", userID)
	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_user_residence",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp UserResidenceResponse
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) SetUserResidence(ctx context.Context, userID string, request UserResidenceRequest, reqOpts *twitchclient.ReqOpts) (*UserResidenceResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	apiURL := fmt.Sprintf("/users/%s/residence", userID)
	req, err := c.NewRequest("PUT", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.set_user_residence",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp UserResidenceResponse
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) GetPayPalBillingAgreement(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*PayPalBillingAgreement, error) {
	apiURL := fmt.Sprintf("/paypal/%s/billing_agreement", userID)
	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_billing_agreement",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp PayPalBillingAgreement
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) SetPayPalBillingAgreement(ctx context.Context, userID string, request SetBillingAgreementRequest, reqOpts *twitchclient.ReqOpts) (*PayPalBillingAgreement, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	apiURL := fmt.Sprintf("/paypal/%s/billing_agreement", userID)
	req, err := c.NewRequest("PUT", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.set_billing_agreement",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp PayPalBillingAgreement
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) GetInitPayPalBillingAgreementURL(ctx context.Context, userID string, returnURL string, cancelURL string, locale string, reqOpts *twitchclient.ReqOpts) (*BillingAgreementURL, error) {
	query := url.Values{}
	query.Set("return_url", returnURL)
	query.Set("cancel_url", cancelURL)
	query.Set("locale", locale)

	apiURL := fmt.Sprintf("/paypal/%s/billing_agreement/url?%s", userID, query.Encode())

	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_billing_agreement_url",
		StatSampleRate: defaultStatSampleRate,
	})

	var response BillingAgreementURL
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func (c *client) PreviewBitsPurchase(ctx context.Context, userID string, ipCountryCode string, productID string, reqOpts *twitchclient.ReqOpts) (*ProductPricing, error) {
	query := url.Values{}
	query.Set("ip_country_code", ipCountryCode)

	apiURL := fmt.Sprintf("/users/%s/products/%s/preview_purchase?%s", userID, productID, query.Encode())

	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.preview_bits_purchase",
		StatSampleRate: defaultStatSampleRate,
	})

	var response ProductPricing
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func (c *client) PurchaseBits(ctx context.Context, userID string, productID string, request BitsPurchaseRequest, reqOpts *twitchclient.ReqOpts) (*BitsPurchaseResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	apiURL := fmt.Sprintf("/users/%s/products/%s/bits_purchase", userID, productID)

	req, err := c.NewRequest("POST", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.bits_purchase",
		StatSampleRate: defaultStatSampleRate,
	})

	var response BitsPurchaseResponse
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func (c *client) GetProductsPrice(ctx context.Context, userID string, ipCountryCode string, productIDs []string, reqOpts *twitchclient.ReqOpts) ([]ProductPricing, error) {
	query := url.Values{}
	query.Set("ip_country_code", ipCountryCode)
	for i := range productIDs {
		query.Add("product_ids[]", productIDs[i])
	}

	apiURL := fmt.Sprintf("/users/%s/products_price?%s", userID, query.Encode())

	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_products_price",
		StatSampleRate: defaultStatSampleRate,
	})

	response := make([]ProductPricing, len(productIDs))
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (c *client) GetPaymentMethods(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*GetPaymentMethodsResponse, error) {
	query := url.Values{}
	query.Set("user_id", userID)

	apiURL := fmt.Sprintf("/payment_methods?%s", query.Encode())

	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_payment_methods",
		StatSampleRate: defaultStatSampleRate,
	})

	var response GetPaymentMethodsResponse
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func (c *client) GetPaymentMethodConfigs(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*GetPaymentMethodConfigsResponse, error) {
	query := url.Values{}
	query.Set("user_id", userID)

	apiURL := fmt.Sprintf("/payment_methods/configs?%s", query.Encode())

	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_payment_method_configs",
		StatSampleRate: defaultStatSampleRate,
	})

	var response GetPaymentMethodConfigsResponse
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func (c *client) SetDefaultPaymentMethod(ctx context.Context, request SetDefaultPaymentMethodRequest, reqOpts *twitchclient.ReqOpts) (*SetDefaultPaymentMethodResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("PUT", "/payment_methods", bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.set_default_payment_method",
		StatSampleRate: defaultStatSampleRate,
	})

	var res SetDefaultPaymentMethodResponse
	_, err = c.DoJSON(ctx, &res, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (c *client) DeleteDefaultPaymentMethod(ctx context.Context, request DeleteDefaultPaymentMethodRequest, reqOpts *twitchclient.ReqOpts) (*DeleteDefaultPaymentMethodResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("DELETE", "/payment_methods", bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	combineReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.delete_default_payment_method",
		StatSampleRate: defaultStatSampleRate,
	})

	var res DeleteDefaultPaymentMethodResponse
	_, err = c.DoJSON(ctx, &res, req, combineReqOpts)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (c *client) GetPaymentTransactionIDs(ctx context.Context, request GetPaymentTransactionsRequest, reqOpts *twitchclient.ReqOpts) (*GetPaymentTransactionsResponse, error) {
	query := url.Values{}
	query.Set("user_id", request.UserID)
	query.Set("sort_key", strings.Join(request.SortKey, ","))

	if request.PurchasedBeforeDate != nil {
		query.Set("purchased_before_date", request.PurchasedBeforeDate.Format(time.RFC1123))
	}

	if request.PurchasedAfterDate != nil {
		query.Set("purchased_after_date", request.PurchasedAfterDate.Format(time.RFC1123))
	}

	if request.Limit != nil {
		query.Set("limit", strconv.Itoa(*request.Limit))
	}

	if request.Offset != nil {
		query.Set("offset", strconv.Itoa(*request.Offset))
	}

	apiURL := fmt.Sprintf("/transactions?%s", query.Encode())

	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_payment_transactions",
		StatSampleRate: defaultStatSampleRate,
	})

	var response GetPaymentTransactionsResponse
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func (c *client) GetPaymentTransactionByID(ctx context.Context, id string, reqOpts *twitchclient.ReqOpts) (*PaymentTransaction, error) {
	apiURL := fmt.Sprintf("/transactions/%s", id)
	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_payment_transaction_by_id",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp PaymentTransaction
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) GetRecurringPaymentDetail(ctx context.Context, subscription_id string, reqOpts *twitchclient.ReqOpts) (*RecurringPaymentDetail, error) {
	query := url.Values{}
	query.Set("subscription_id", subscription_id)

	apiURL := fmt.Sprintf("/payment_methods/recurring_payment_detail?%s", query.Encode())
	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_recurring_payment_detail",
		StatSampleRate: defaultStatSampleRate,
	})

	var response RecurringPaymentDetail
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func (c *client) GetPurchaseProfile(ctx context.Context, purchaseProfileID string, extraKeys []string, reqOpts *twitchclient.ReqOpts) (*PurchaseProfile, error) {
	query := url.Values{}

	if extraKeys != nil && len(extraKeys) > 0 {
		query.Set("extra_keys", strings.Join(extraKeys, ","))
	}

	apiURL := fmt.Sprintf("/purchase_profiles/%s?%s", purchaseProfileID, query.Encode())
	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_purchase_profile",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp PurchaseProfile
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) GetPurchaseProfilesByPurchasableIDs(ctx context.Context, purchasableIDs []string, reqOpts *twitchclient.ReqOpts) (*PurchaseProfiles, error) {
	type reqBody struct {
		PurchasableIDs []string `json:"purchasable_id"`
		FullDetails    bool     `json:"full_details"`
		PerPage        string   `json:"per_page"`
	}

	jsonBody, err := json.Marshal(reqBody{
		PurchasableIDs: purchasableIDs,
		FullDetails:    true,
		PerPage:        "all",
	})
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("POST", "/query/purchase_profiles", bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.query_purchase_profiles",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp PurchaseProfiles
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) GetSubscriptions(ctx context.Context, request GetSubscriptionsRequest, reqOpts *twitchclient.ReqOpts) (*GetSubscriptionsResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("POST", "/internal/subscriptions/query", bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_payments_subscriptions",
		StatSampleRate: defaultStatSampleRate,
	})

	var res GetSubscriptionsResponse
	_, err = c.DoJSON(ctx, &res, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (c *client) UpdateOrderLineFulfillmentStatus(ctx context.Context, request UpdateOrderLineFulfillmentStatusRequest, reqOpts *twitchclient.ReqOpts) (*UpdateOrderLineFulfillmentStatusResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest(
		"POST",
		fmt.Sprintf("/purchase_order_line_items/%s/status", request.PurchaseOrderLineItemID),
		bytes.NewBuffer(jsonBody),
	)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.update_order_line_fulfillment_status",
		StatSampleRate: defaultStatSampleRate,
	})

	var res UpdateOrderLineFulfillmentStatusResponse
	_, err = c.DoJSON(ctx, &res, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &res, nil
}
