package payments

import "time"

// For InitiatePurchase API
type InitiatePurchaseRequest struct {
	Platform         string `json:"platform"`
	IsGift           bool   `json:"is_gift,omitempty"`
	RecipientID      string `json:"recipient_id,omitempty"`
	WithSpecialPromo string `json:"with_special_promo,omitempty"`
}

// For CompletePurchase API
type CompletePurchaseRequest struct {
	PaymentProvider  string      `json:"payment_provider"`
	PaymentInfo      PaymentInfo `json:"payment_info"`
	Platform         string      `json:"platform"`
	IsGift           bool        `json:"is_gift,omitempty"`
	RecipientID      string      `json:"recipient_id,omitempty"`
	WithSpecialPromo string      `json:"with_special_promo,omitempty"`
}

// For CancelPurchase API
type CancelPurchaseRequest struct {
	// Used to form the base_key for notifying Subscriptions Service
	GiftPurchaseProfileID string `json:"gift_purchase_profile_id,omitempty"`
	// The actual purchase profile to cancel, also helps form the upgrade_key for notifying Subscriptions Service
	PurchaseProfileID string `json:"purchase_profile_id,omitempty"`
	Reason            string `json:"reason,omitempty"`
}

type PaymentInfo struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Token     string `json:"token"`
	Gateway   string `json:"gateway"`
	State     string `json:"state"`
	Country   string `json:"country"`
}

// For GetPurchase API
type GetPurchaseResponse struct {
	PurchaseDetails
	// If there are no purchase details, show the compatible purchases if they exist (i.e. user is subscribed to a different tier)
	CompatiblePurchases []PurchaseDetails `json:"compatible_purchases,omitempty"`
	Error               string            `json:"error,omitempty"`
}

// For GetInitiatePurchase API
type GetInitiatePurchaseResponse struct {
	Recurly          RecurlyParams `json:"recurly,omitempty"`
	XsollaV3         XsollaParams  `json:"xsolla_v3,omitempty"`
	Zuora            ZuoraParams   `json:"zuora,omitempty"`
	Error            string        `json:"error,omitempty"`
	CanGift          bool          `json:"can_gift,omitempty"`
	CanPurchasePromo bool          `json:"can_purchase_promo,omitempty"`
}

type RecurlyParams struct {
	PublicKey           string              `json:"public_key"`
	BraintreeClientAuth string              `json:"braintree_client_authorization"`
	PayWithAmazonConfig PayWithAmazonConfig `json:"pay_with_amazon_config"`
}

type XsollaParams struct {
	URL string `json:"url"`
}

type ZuoraParams struct {
	ExtAccountID  string `json:"ext_account_id"`
	Signature     string `json:"signature"`
	Token         string `json:"token"`
	PublicKey     string `json:"public_key"`
	TenantID      string `json:"tenant_id"`
	HostedPageID  string `json:"hosted_page_id"`
	HostedPageURL string `json:"hosted_page_url"`
}

type PayWithAmazonConfig struct {
	SellerID   string `json:"seller_id"`
	ClientID   string `json:"client_id"`
	Production bool   `json:"production"`
}

// For CompletePurchase API
type CompletePurchaseResponse struct {
	PurchaseDetails

	Error string `json:"error,omitempty"`
}

// For GetProductChannelName API
type GetProductChannelNameResponse struct {
	ChannelName string `json:"channel_name"`
}

// For multiple API responses
type InvoiceTotal struct {
	Price    int    `json:"price"`
	Currency string `json:"currency"`
}

type PurchaseDetails struct {
	PaymentProvider string       `json:"payment_provider,omitempty"`
	State           string       `json:"state,omitempty"`
	InvoiceTotal    InvoiceTotal `json:"invoice_total,omitempty"`
	CancelDate      string       `json:"cancel_date,omitempty"`
	ProductType     string       `json:"product_type,omitempty"`
	ProductTier     string       `json:"product_tier"`
}

// For ProcessReceipt API
type ProcessReceiptRequest struct {
	// Android: directly correlates to INAPP_DATA_SIGNATURE, iOS: base64 encoded receipt-data
	SignedReceipt string `json:"signed_receipt"`
	// Raw unsigned purchase data that Android provides. It is basically the unsigned version of signed_receipt
	AndroidIABReceipt string `json:"android_iab_receipt"`
	// The type of receipt. Currently only support "bits"
	Type string `json:"type"`
	// The platform calling this API, currently only "android" or "ios"
	Platform string `json:"platform"`
	// Mobile device ID
	DeviceID string `json:"device_id"`
	// The channel category type when user made purchase
	ChannelCategory string `json:"channel_category"`
	// The value of the local price
	PurchasePrice *int `json:"purchase_price,omitempty"`
	// The currency unit of the local price
	PurchaseCurrency string `json:"purchase_currency,omitempty"`
	// Only used for iOS currently, this is the purchase transaction ID that iOS wants us to process inside the receipt
	TransactionID string `json:"transaction_id"`
	// The ID of the twitch product to subscribe to.
	ProductID string `json:"product_id,omitempty"`
}

type ProcessReceiptResponse struct {
	// The type of receipt. Currently only support "bits"
	Type string `json:"type,omitempty"`
	// Current user Bits balance
	Balance int `json:"balance,omitempty"`
}

type GetUnacknowledgedEventsRequest struct {
	UserID   string `json:"user_id"`
	Platform string `json:"platform"`
}

type GetUnacknowledgedEventsResponse struct {
	UnacknowledgedEvents []SubscriptionEvent `json:"unacknowledged_events"`
}

type SubscriptionEvent struct {
	ExtProductID string     `json:"ext_product_id"`
	Status       string     `json:"status"`
	ChannelName  string     `json:"channel_name"`
	EndDate      *time.Time `json:"end_date"`
}

type CancelSubscriptionRequest struct {
	UserID                string `json:"user_id"`
	CancellationDirective string `json:"cancellation_directive"`
	BenefitsDirective     string `json:"benefits_directive"`
}

type CancelSubscriptionResponse struct {
	ID        string `json:"id"`
	WillRenew bool   `json:"will_renew"`
}

type UserResidenceRequest struct {
	UserID      string `json:"user_id"`
	CountryCode string `json:"country_code"`
	ZipCode     string `json:"zip_code,omitempty"`
}

type UserResidenceResponse struct {
	UserID      string `json:"user_id"`
	CountryCode string `json:"country_code"`
	ZipCode     string `json:"zip_code,omitempty"`
}

type SetBillingAgreementRequest struct {
	Token string `json:"token"`
}

type PayPalBillingAgreement struct {
	UserID             string `json:"user_id"`
	BillingAgreementID string `json:"billing_agreement_id"`
	Email              string `json:"email"`
	FirstName          string `json:"first_name"`
	LastName           string `json:"last_name"`
	CountryCode        string `json:"country_code"`
}

type BillingAgreementURL struct {
	URL string `json:"url"`
}

type ProductPricing struct {
	ProductID      string  `json:"product_id"`
	ProductName    string  `json:"product_name"`
	ProductPricing Pricing `json:"product_pricing"`
	TaxInclusive   bool    `json:"tax_inclusive"`
}

// All price is in cents
type Pricing struct {
	// price in base price
	Price int `json:"price"`
	// currency of base price
	Currency string `json:"currency"`
	// price in localized currency
	LocalizedPrice int `json:"localized_price"`
	// localized currency
	LocalizedCurrency string `json:"localized_currency"`
	Tax               int    `json:"tax"`
	Total             int    `json:"total"`
}

type BitsPurchaseRequest struct {
	IpCountryCode string `json:"ip_country_code"`
}

type BitsPurchaseResponse struct {
	OrderID     string `json:"order_id"`
	BitsBalance int    `json:"bits_balance"`
}

// Other payments models
type PurchaseProfile struct {
	ID                   int        `json:"id"`
	State                string     `json:"state"`
	PaymentProvider      string     `json:"payment_provider"`
	ExtSubscriptionID    *string    `json:"ext_subscription_id"`
	ExtPurchaserID       *string    `json:"ext_purchaser_id"`
	TicketID             int        `json:"ticket_id"`
	PurchaserID          int        `json:"purchaser_id"`
	PurchaserName        *string    `json:"purchaser_name"`
	PurchaserEmail       string     `json:"purchaser_email"`
	CreatedOn            time.Time  `json:"created_on"`
	UpdatedOn            *time.Time `json:"updated_on"`
	WillRenew            bool       `json:"will_renew"`
	PurchaseDateTime     *time.Time `json:"purchase_datetime"`
	CancelDateTime       *time.Time `json:"cancel_datetime"`
	DoNotRenewDateTime   *time.Time `json:"do_not_renew_datetime"`
	IsPaying             bool       `json:"is_paying"`
	IsRecurring          bool       `json:"is_recurring"`
	PaidOn               *time.Time `json:"paid_on"`
	ExpiresOn            *time.Time `json:"expires_on"`
	IsRefundable         *bool      `json:"is_refundable"`
	IsExpired            *bool      `json:"is_expired"`
	LastPaymentDate      *time.Time `json:"last_payment_date"`
	IsGift               *bool      `json:"is_gift"`
	TicketOwnerID        *int       `json:"ticket_owner_id"`
	TicketProductOwnerID *int       `json:"ticket_product_owner_id"`
	ProductPrice         *int       `json:"product_price"`          // e.g. 499
	ProductPriceDivisor  *int       `json:"product_price_divisor"`  // e.g. 100
	ProductPriceCurrency *string    `json:"product_price_currency"` // e.g. "USD"
	ProductType          string     `json:"product_type"`
}

type PurchaseProfiles struct {
	PurchaseProfiles []PurchaseProfile `json:"purchase_profiles"`
}

type Subscription struct {
	ID                string     `json:"id"`
	UserID            string     `json:"user_id"`
	Platform          string     `json:"platform"`
	ProductID         string     `json:"product_id"`
	ExternalProductID string     `json:"ext_product_id"`
	IsRefundable      bool       `json:"is_refundable"`
	RenewalDate       *time.Time `json:"renewal_date"`
}

type GetSubscriptionsRequest struct {
	IDs      []string `json:"ids"`
	Platform string   `json:"platform,omitempty"`
	Page     *int     `json:"page,omitempty"`
	PerPage  *int     `json:"per_page,omitempty"`
}

type GetSubscriptionsResponse struct {
	Subscriptions []Subscription `json:"subscriptions"`
}

// Payment methods APIs
type GetPaymentMethodsResponse struct {
	PaymentMethods []PaymentMethod `json:"payment_methods"`
}

type PaymentMethod struct {
	Provider           string   `json:"provider"`
	PaymentType        string   `json:"payment_type"`
	BillingEmail       *string  `json:"billing_email"`
	CardType           *string  `json:"card_type"`
	LastFour           *string  `json:"last_four"`
	ExpirationMonth    *int     `json:"expiration_month"`
	ExpirationYear     *int     `json:"expiration_year"`
	ExtMethodID        *string  `json:"ext_method_id"`
	PurchaseProfileIDs []string `json:"purchase_profile_ids"`
	SubscriptionIDs    []string `json:"subscription_ids"`
}

type RecurringPaymentDetail struct {
	WillRenew            bool       `json:"will_renew"`
	ExpiresAt            *time.Time `json:"expires_at"`
	TicketOwnerID        *int       `json:"ticket_owner_id"`
	TicketProductOwnerID *int       `json:"ticket_product_owner_id"`
	RenewalPrice         *int       `json:"renewal_price"`         // e.g. 499
	RenewalPriceDivisor  *int       `json:"renewal_price_divisor"` // e.g. 100
	RenewalCurrency      *string    `json:"renewal_currency"`      // e.g. "USD"
	ProductType          string     `json:"product_type"`
}

type GetPaymentMethodConfigsResponse struct {
	Recurly  RecurlyParams `json:"recurly,omitempty"`
	XsollaV3 XsollaParams  `json:"xsolla_v3,omitempty"`
	Zuora    ZuoraParams   `json:"zuora,omitempty"`
	Error    string        `json:"error,omitempty"`
}

type SetDefaultPaymentMethodRequest struct {
	// Twitch user ID
	UserID string `json:"user_id"`

	// "recurly" or "zuora"
	Provider string `json:"provider"`

	// If provider is recurly, then token is the Recurly token.
	// If provider is zuora, then token is the new payment method ID
	Token string `json:"token"`

	// If provider is recurly, we need gateway
	Gateway string `json:"gateway"`
}

type SetDefaultPaymentMethodResponse struct {
	PaymentMethod *PaymentMethod `json:"payment_method"`
	UpdatedAt     *time.Time     `json:"updated_at"`
}

type DeleteDefaultPaymentMethodRequest struct {
	UserID string `json:"user_id"`
	// "recurly" or "zuora"
	Provider string `json:"provider"`
}

type DeleteDefaultPaymentMethodResponse struct {
	UpdatedAt *time.Time `json:"updated_at"`
}

type GetPaymentTransactionsRequest struct {
	UserID              string     `json:"user_id"`
	SortKey             []string   `json:"sort_key,omitempty"`
	PurchasedBeforeDate *time.Time `json:"purchased_before_date,omitempty"`
	PurchasedAfterDate  *time.Time `json:"purchased_after_date,omitempty"`
	Limit               *int       `json:"limit,omitempty"`
	Offset              *int       `json:"offset,omitempty"`
}

type GetPaymentTransactionsResponse struct {
	FirstPurchaseDate *time.Time `json:"first_purchase_date"`
	LastPurchaseDate  *time.Time `json:"last_purchase_date"`
	TransactionIDs    []string   `json:"transaction_ids"`
	Total             int        `json:"_total"`
}

type PaymentTransaction struct {
	ID              string    `json:"id"`
	PurchasedAt     time.Time `json:"purchased_at"`
	ProductType     string    `json:"product_type"`
	ProductName     string    `json:"product_name"`
	ProductTier     *string   `json:"product_tier"`
	ProductOwnerID  *int      `json:"product_owner_id"`
	PaymentProvider string    `json:"payment_provider"`
	PaymentType     *string   `json:"payment_type"`
	CardType        *string   `json:"card_type"`
	LastFour        *string   `json:"last_four"`
	GrossAmount     *int      `json:"gross_amount"`
	Currency        *string   `json:"currency"`
	IsGift          bool      `json:"is_gift"`
	RecipientUserID *string   `json:"recipient_user_id"`
}

// Update purchase order fulfillment (and line item) status
type UpdateOrderLineFulfillmentStatusRequest struct {
	PurchaseOrderLineItemID string `json:"purchase_order_line_item_id"`
	Status                  string `json:"status"`
	RecipientID             string `json:"recipient_id"`
}

type UpdateOrderLineFulfillmentStatusResponse struct {
	ItemFulfillmentID string `json:"item_fulfillment_id"`
	Status            string `json:"status"`
}
