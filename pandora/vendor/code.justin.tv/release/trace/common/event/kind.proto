syntax = "proto3";

package code.justin.tv.release.trace.common.event;
option go_package = "event";

// Kind describes the type of event instrumented.
//
// When a request-response RPC takes place, there are several identifiable
// instants that can be observed by the client, and several that can be
// observed by the server.  Each of those is assigned a different Kind
// value.  An program may only be instrumented to measure a small subset
// of the possible events.  The goal of the Kind enumeration is to
// document all known events precisely, so that if two programs emit an
// event with a particular Kind, that the results will be comparable.
//
// If a program wishes to emit an event which is not described by an
// existing Kind value, a new Kind should be written to fit the bill.  The
// new value should be reusable across other programs with similar
// requirements and should not be particular to the language or framework
// used.
enum Kind {
  UNKNOWN = 0;

  // Server-sent events

  // Server reads the beginning of a request.  The server has received
  // sufficient data from the client to determine which Trace
  // transaction the request should be attributed to, if any.  If the
  // protocol differentiates between a metadata preamble and a payload
  // that follows, this event is emitted immediately after the entire
  // metadata has been received.
  //
  // For an HTTP request, this would be when the transport has presented
  // the request URI and headers are presented to the application.
  REQUEST_HEAD_RECEIVED = 20;

  // Server finishes reading the last of the request body.
  //
  // For an HTTP request, this would be when the transport has presented
  // the last of the request body to the application and notified it
  // that no more body data will be received.  Trailers may be yet to
  // come.
  REQUEST_BODY_RECEIVED = 21;

  // Server responds to a request.  There has not yet been any direct
  // communication to the client about the client's request on the
  // application layer, but the server will change that immediately
  // after generating this event.  Transport-layer acknowledgement of
  // data receipt does not trigger this event.
  //
  // For an HTTP response, this would be when the status code and
  // headers are presented to the HTTP transport, before any bytes are
  // written to the wire.
  RESPONSE_HEAD_PREPARED = 30;

  // Server has sent the last of the response body to the client.
  //
  // For an HTTP response, this would be when the last of the response
  // body has been accepted by the HTTP transport, and the application
  // has notified the transport that no more body data will be sent.
  RESPONSE_BODY_SENT = 31;

  // Client-sent events

  // Client initiates a request.  There has been no indication given to
  // the outside world that this particular request is about to take
  // place.  The application knows enough about the request to begin
  // making externally-visible actions.
  //
  // For an HTTP request, this would be when the request URI and headers
  // are presented to the HTTP transport, before any bytes are written
  // to the wire.  The hostname may still need to be resolved, a TCP
  // connection may still need to be established, a TLS handshake may
  // still need to take place.
  REQUEST_HEAD_PREPARED = 40;

  // Client finishes the externally-visible actions required for the
  // request.
  //
  // For an HTTP request, this would be when the last of the request
  // body has been accepted by the HTTP transport, and the application
  // has notified the transport that no more body data will be sent.
  REQUEST_BODY_SENT = 41;

  // Client reads the beginning of the response.  The response metadata
  // has been read and is ready to be presented to the application's
  // business logic, giving some initial indication of if the RPC
  // succeeded.  The response body has not yet been read.
  //
  // For an HTTP response, this would be when the client's HTTP
  // transport has read and decoded the full response headers from the
  // wire and is presenting them to the application, but before the
  // response body has been consumed.
  RESPONSE_HEAD_RECEIVED = 50;

  // Client finishes reading the last of the response body.
  //
  // For an HTTP response, this would be when the transport has
  // presented the last of the response body to the application and
  // notified it that no more body data will be received.  Trailers may
  // be yet to come.
  RESPONSE_BODY_RECEIVED = 51;
}