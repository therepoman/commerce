Contributing to Trace
===

Filing issues
---

We may be able to help you immediately in the #trace slack channel, or over
email at edge-reliability@twitch.tv.

Our task tracking is under
[the EAR Jira queue](https://twitchtv.atlassian.net/projects/EAR).

GHE issues aren't currently part of our daily workflow, so you may experience
high response latency for them.

Contributing code
---

We do code review via
[phabricator](https://phabricator.internal.justin.tv/differential/).

Once you've installed Arcanist (one approach listed
[in the twitch/docs repo](https://git-aws.internal.justin.tv/twitch/docs/blob/master/web/dev_environment_setup.md#arcanist)),
you can

Send code for review:
```
arc diff --create origin/master
```

Send updates to an outstanding review:
```
arc diff --update Dxxxx origin/master
```

Merge your changes into the master branch once the review is complete:
```
arc land --squash --onto master
```

You can also subscribe to all code reviews for this project or others. On
phabricator,
[create a new herald rule](https://phabricator.internal.justin.tv/herald/new/).
You'll want a rule for "Differential Revisions", and for it to be a "Personal"
rule. The condition for triggering should be when "Arcanist Project" is
"trace", and you can request that herald "Add [you] to the CC" list.

This repo includes a number of automated tests - please make sure they
continue to pass. You can run the build and test process under manta. This will also ensure that any dependencies you've added are correctly vendored via godep.

```
rm -rf .manta ; manta -v
```

Code should pass `gofmt` and `go vet`. Passing
[`errcheck`](https://godoc.org/github.com/kisielk/errcheck) and doing well
under [`golint`](https://godoc.org/github.com/golang/lint/golint) are goals,
though they're not currently enforced.

Contributing routes
---

When instrumenting HTTP calls, Trace relies on route definitions provided by
service owners. These are stored as json files in the
[code.justin.tv/release/trace/analysis/routes/data](analysis/routes/data)
package. After modifying/updating/deleting a json file in that directory, you
must run `go generate code.justin.tv/release/trace/analysis/routes` before the
changes will be usable. This requires the
[go-bindata](https://godoc.org/github.com/jteeuwen/go-bindata) command.

We happily accept pull requests for changes to the route json files and the
resulting generated code.
