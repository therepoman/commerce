#!/bin/bash
which dep &> /dev/null || curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

dep ensure
