#!/bin/bash

# Make ./scripts the working directory
source ./scripts/codebuild_set_git_env.sh

GITHUB_TYPE=ghe
REPO=commerce/pandora
COVERAGE_FILE_LOC=.
FILE_PATTERN=*.xml

if [ -z "$CODEBUILD_SOURCE_REPO_URL" ]; then
  echo "Skipping codecov publish since CodeBuild was not triggered by GHE -> CodeBuild Webhook"
  exit 0
fi

if [ -z "$CODEBUILD_GIT_COMMIT" ]; then
  echo "Codecov can't get coverage, missing CODEBUILD_GIT_COMMIT environment variable"
  exit 0
fi

if [ ! -d "../.git" ]; then
  if [ -z "$CODEBUILD_GIT_BRANCH" ]; then
    echo "Codecov can't get coverage, missing CODEBUILD_GIT_BRANCH environment variable"
    exit 0
  fi
fi

curl -s https://codecov.internal.justin.tv/bash | bash -s - -t ${GITHUB_TYPE} -r ${REPO} -s ${COVERAGE_FILE_LOC} -f "${FILE_PATTERN}"

sleep 3
curl -s https://codecov.internal.justin.tv/api/${GITHUB_TYPE}/${REPO}/commit/\$GIT_COMMIT?access_token=9916c5c17be744039297352bbe6faa1e > codecov_coverage.json
exit 0