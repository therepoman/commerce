package main

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestParseMessage(t *testing.T) {
	Convey("Given valid JSON message", t, func() {
		m := "{\"origin_id\":\"3930\",\"status\":\"fulfilled\",\"recipient_id\":\"144364159\"}"

		Convey("When parsing the message", func() {
			mf, err := parseMessage(m)

			Convey("It should return proper struct", func() {
				So(err, ShouldBeNil)
				So(mf.OriginID, ShouldEqual, "3930")
				So(mf.RecipientID, ShouldEqual, "144364159")
				So(mf.Status, ShouldEqual, "fulfilled")
			})
		})
	})

	Convey("Given invalid JSON message", t, func() {
		m := "lul"

		Convey("When parsing the message", func() {
			mf, err := parseMessage(m)

			Convey("It should return err", func() {
				So(err, ShouldNotBeNil)
				So(mf, ShouldBeNil)
			})
		})
	})

	Convey("Given JSON message with missing params", t, func() {
		m := "{}"

		Convey("When parsing the message", func() {
			mf, err := parseMessage(m)

			Convey("It should return err", func() {
				So(err, ShouldNotBeNil)
				So(mf, ShouldBeNil)
			})
		})
	})
}
