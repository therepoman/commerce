package main

import (
	"encoding/json"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type messageFormat struct {
	OriginID    string `json:"origin_id"`
	RecipientID string `json:"recipient_id"`
	Status      string `json:"status"`
}

func parseMessage(message string) (*messageFormat, error) {
	var input messageFormat
	err := json.Unmarshal([]byte(message), &input)
	if err != nil {
		logrus.WithError(err).Errorf("Failed to parse SNS message to JSON: message=%s", message)
		return nil, err
	}

	if input.Status == "" || input.RecipientID == "" || input.OriginID == "" {
		err = errors.New("Missing message params")
		logrus.WithError(err).Errorf("Missing SNS message params: %+v", input)
		return nil, err
	}
	return &input, nil
}
