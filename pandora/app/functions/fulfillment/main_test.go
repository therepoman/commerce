package main

import (
	"testing"

	"errors"

	"code.justin.tv/commerce/pandora/mocks"
	"github.com/aws/aws-lambda-go/events"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestHandler(t *testing.T) {
	Convey("Given a valid SNS event", t, func() {
		m := events.SNSEvent{
			Records: []events.SNSEventRecord{
				{
					SNS: events.SNSEntity{
						Message: "{\"origin_id\":\"3930\",\"status\":\"fulfilled\",\"recipient_id\":\"144364159\"}",
					},
				},
			},
		}

		Convey("with invalid Payments Client call", func() {
			paymentsMock := &mocks.IPaymentsClient{}
			paymentsMock.On("UpdateOrderLineFulfillmentStatus", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("dnsajdsanj"))

			h := lambdaHandler{
				paymentsClient: paymentsMock,
			}

			Convey("When the lambda handler is called", func() {
				err := h.Handler(m)

				Convey("It should return error", func() {
					So(err, ShouldNotBeNil)
				})
			})
		})

		Convey("with valid Payments Client call", func() {
			paymentsMock := &mocks.IPaymentsClient{}
			paymentsMock.On("UpdateOrderLineFulfillmentStatus", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

			h := lambdaHandler{
				paymentsClient: paymentsMock,
			}

			Convey("When the lambda handler is called", func() {
				err := h.Handler(m)

				Convey("It not return error", func() {
					So(err, ShouldBeNil)
				})
			})
		})
	})

	Convey("Given an invalid SNS event", t, func() {
		Convey("with missing params", func() {
			m := events.SNSEvent{
				Records: []events.SNSEventRecord{
					{
						SNS: events.SNSEntity{
							Message: "{}",
						},
					},
				},
			}

			h := lambdaHandler{}

			Convey("When the lambda handler is called", func() {
				err := h.Handler(m)

				Convey("It should return error", func() {
					So(err, ShouldNotBeNil)
				})
			})
		})

		Convey("with invalid JSON", func() {
			m := events.SNSEvent{
				Records: []events.SNSEventRecord{
					{
						SNS: events.SNSEntity{
							Message: "dnsajkdnsajkdnsajadnsjk",
						},
					},
				},
			}

			h := lambdaHandler{}

			Convey("When the lambda handler is called", func() {
				err := h.Handler(m)

				Convey("It should return error", func() {
					So(err, ShouldNotBeNil)
				})
			})
		})
	})
}
