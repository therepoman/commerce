package main

import (
	"context"

	"os"

	"code.justin.tv/commerce/pandora/app/clients"
	"code.justin.tv/common/config"
	payments "code.justin.tv/revenue/payments-service-go-client/client"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/sirupsen/logrus"
)

type lambdaHandler struct {
	paymentsClient clients.IPaymentsClient
}

// Handler for purchase order fulfillment status update events
func (h *lambdaHandler) Handler(event events.SNSEvent) error {
	logrus.Infof("Processing SNS event: %+v", event)

	for _, r := range event.Records {
		message := r.SNS.Message
		m, err := parseMessage(message)
		if err != nil {
			return err
		}

		resp, err := h.paymentsClient.UpdateOrderLineFulfillmentStatus(
			context.Background(),
			payments.UpdateOrderLineFulfillmentStatusRequest{
				PurchaseOrderLineItemID: m.OriginID,
				RecipientID:             m.RecipientID,
				Status:                  m.Status,
			},
			nil,
		)
		if err != nil {
			logrus.WithError(err).Errorf("Failed to update order line fulfillment status: message=%s", r.SNS.Message)
			return err
		}

		logrus.Infof("Updated order line item fulfillment status: resp=%+v", resp)
	}

	return nil
}

func main() {
	config.Parse()
	paymentsClient, err := clients.NewPaymentsClient(os.Getenv("PAYMENTS_SERVICE_HOST"))
	if err != nil {
		logrus.WithError(err).Fatal("Failed to init payments client")
	}

	h := lambdaHandler{paymentsClient}
	lambda.Start(h.Handler)
}
