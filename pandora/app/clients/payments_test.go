package clients

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestPayments(t *testing.T) {
	Convey("Given the host string", t, func() {
		host := "testhosturl"

		Convey("When the a new payments client is instantiated", func() {
			client, err := NewPaymentsClient(host)

			Convey("It should be successful", func() {
				So(err, ShouldBeNil)
				So(client, ShouldNotBeNil)
			})
		})
	})
}
