package clients

import (
	"context"

	"code.justin.tv/common/config"
	"code.justin.tv/foundation/twitchclient"
	payments "code.justin.tv/revenue/payments-service-go-client/client"
)

type IPaymentsClient interface {
	UpdateOrderLineFulfillmentStatus(ctx context.Context, request payments.UpdateOrderLineFulfillmentStatusRequest, reqOpts *twitchclient.ReqOpts) (*payments.UpdateOrderLineFulfillmentStatusResponse, error)
}

func NewPaymentsClient(host string) (IPaymentsClient, error) {
	httpConf := twitchclient.ClientConf{
		Host: host,
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: 1000,
		},
		Stats: config.Statsd(),
	}

	paymentsClient, err := payments.NewClient(httpConf)
	if err != nil {
		return nil, err
	}

	return paymentsClient, nil
}
