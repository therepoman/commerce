# pandora

[![Build Status](https://codebuild.us-west-2.amazonaws.com/badges?uuid=eyJlbmNyeXB0ZWREYXRhIjoiT0o2TXArS2xQdWthbW9DVjlKVnd3WnBTbG5XQlhRR0M4RVliVzJobUg2Ny83QXRScGo3R0ViRmxLWkxSVHNhc04xaFd5ZjcwRUovTTFQaG1IZWZhUmlnPSIsIml2UGFyYW1ldGVyU3BlYyI6ImVJOXZoblFUY1l3SDJVbFQiLCJtYXRlcmlhbFNldFNlcmlhbCI6MX0%3D&branch=master)](https://us-west-2.console.aws.amazon.com/codebuild/home?region=us-west-2#/projects/pandora-ghe/view)
[![codecov](https://codecov.internal.justin.tv/ghe/commerce/pandora/branch/master/graph/badge.svg)](https://codecov.internal.justin.tv/ghe/commerce/pandora)

Lambdas functions and step functions for the Rewards team.

<a href="https://docs.google.com/document/d/1gICmTdZveMvtlNbfexJzYJDwnbAkHSX7OmGShCzJCLg/edit?usp=sharing">
    <img src="https://secure.i.telegraph.co.uk/multimedia/archive/01498/layton-box1_1498836c.jpg" data-canonical-src="https://secure.i.telegraph.co.uk/multimedia/archive/01498/layton-box1_1498836c.jpg" width="230" height="144" />
</a>

## IMPORTANT -- Current State

10/12/2018 - Pandora development has been put on hold because it is not required for OWL All-Access Pass Season 2 with the reduced IGC content and because we already have a fulfiller that works. This project is in the state of the repo is branched from bubbles/lambdwich repos, README is up to date, and the AWS account is set up. The pipeline has NOT been created and is NOT in a state to run the commands below.

Research Notes: 
- [SAM CLI](https://github.com/awslabs/aws-sam-cli)
- [Building pipeline for serverless](https://www.slideshare.net/AmazonWebServices/building-cicd-pipelines-for-serverless-applications-78648527)
- [AWS Deployment of Lambdas](https://docs.aws.amazon.com/lambda/latest/dg/automating-deployment.html)

## The black box

Serverless development workflow with continuous deployment.

Note that all AWS resources related to the pipeline and code building live in our `twitch-fortuna-aws` AWS account.

The production `twitch-commerce-aws` account only contains the prod Lambda functions and IAM role for the staging pipeline to assume for prod deployment.

- [Grafana graphs](https://grafana.internal.justin.tv/dashboard/db/pandora?refresh=5s&orgId=1&var-AWS_PROFILE=CloudWatch%20(twitch-fortuna-aws))
- [Pipeline (twitch-fortuna-aws)](https://us-west-2.console.aws.amazon.com/codepipeline/home?region=us-west-2#/view/pandora) 

## Technologies
- Golang
- AWS Lambda
- AWS CodeCommit
- AWS CodePipeline
- AWS CodeBuild
- AWS CloudFormation
- dep (Go dependency management tool)

## Development

### Package Management

Import new package to code and the updated dep packages
```
make install
```
or
```
dep ensure
```

### Adding New Lambda Functions

- Add the function to `sam.yaml`
- Create a directory in `app/functions`
- Implement the `main.go` handler and unit tests

### Manual Testing

See [here](https://github.com/awslabs/aws-sam-local) for more details about the tool used to invoke AWS Lambda functions locally.
```
npm install -g aws-sam-local
```

### Example

- Make sure the Go binaries are built for each Lambda function
```
make build
```
- Invoke the function binaries with AWS SAM Local passing in the SAM template and environment variables
```
echo '{}' | sam local invoke "PurchaseOrderFulfillmentStatusFunction" --template sam.yaml --env-vars sam_local_env_vars.json
```

## Tests

Run unit tests locally. They'll also run as part of the pipeline build and PR branch build.

```
make test
```

### Mocks

- Generate mocks for interfaces using [mockery](https://github.com/vektra/mockery)
```
make mocks
```

## Deployment

### Test on Staging

After you push commits to a remote branch, you can edit the staging CodePipeline's CodeCommit source to pull source code from your branch [here](https://us-west-2.console.aws.amazon.com/codepipeline/home?region=us-west-2#/edit/pandora). 
Approve the change set to deploy to staging.

Change it back to master when you are done testing.

### Prod Deployment

The CodePipeline is located in the `twitch-fortuna-aws` staging AWS account [here](https://us-west-2.console.aws.amazon.com/codepipeline/home?region=us-west-2#/view/pandora). 
It deploys to production by assuming a role configured by the production `twitch-fortuna-aws` AWS account.

Push to master, make sure AWS CodeCommit in our CodePipeline is pointing to master, and then approve the changeset to deploy to production.
