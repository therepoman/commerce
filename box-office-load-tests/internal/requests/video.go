package requests

import (
	"encoding/json"
	"net/http"

	borpc "code.justin.tv/commerce/box-office/rpc"

	"github.com/pkg/errors"
	"github.com/tsenart/vegeta/lib"
)

const (
	videoEndpoint = "http://box-office-prod.internal.justin.tv/twirp/code.justin.tv.commerce.boxoffice.BoxOffice/GetVideoAccessToken"
)

func GenerateVideoTargets(userIDList []string, channelIDList []string) ([]vegeta.Target, error) {
	targets := []vegeta.Target{}

	for _, userID := range userIDList {
		for _, channelID := range channelIDList {
			target, err := GenerateVideoTarget(userID, channelID)
			if err != nil {
				return nil, errors.Wrap(err, "Failed to generate request target")
			}

			targets = append(targets, *target)

			// Generate another target right next to this one for an anonymous user. Do this because
			// we want 50% of traffic to be unauthenticated
			anonTarget, err := GenerateVideoTarget("", channelID)
			if err != nil {
				return nil, errors.Wrap(err, "Failed to generate request target")
			}
			targets = append(targets, *anonTarget)
		}
	}

	return targets, nil
}

func GenerateVideoTarget(userID string, channelID string) (*vegeta.Target, error) {
	request := borpc.GetVideoAccessTokenRequest{
		UserId:      userID,
		ChannelId:   channelID,
		HasAdblock:  false,
		NeedHttps:   false,
		Platform:    "loadtest",
		PlayerType:  "loadtest",
		CountryCode: "US",
		DeviceId:    "1239285209358239058235",
	}

	requestBytes, err := json.Marshal(request)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to marshal request")
	}

	headers := http.Header{
		"Content-Type": {"application/json"},
	}
	target := &vegeta.Target{
		Method: "POST",
		URL:    videoEndpoint,
		Body:   requestBytes,
		Header: headers,
	}
	return target, nil
}
