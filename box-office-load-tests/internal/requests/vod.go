package requests

import (
	"encoding/json"
	"net/http"

	borpc "code.justin.tv/commerce/box-office/rpc"

	"github.com/pkg/errors"
	"github.com/tsenart/vegeta/lib"
)

const (
	vodEndpoint = "http://box-office-prod.internal.justin.tv/twirp/code.justin.tv.commerce.boxoffice.BoxOffice/GetVODAccessToken"
)

func GenerateVODTargets(userIDList []string, vodIDList []string) ([]vegeta.Target, error) {
	targets := []vegeta.Target{}

	for _, userID := range userIDList {
		for _, vodID := range vodIDList {
			target, err := GenerateVODTarget(userID, vodID)
			if err != nil {
				return nil, errors.Wrap(err, "Failed to generate request target")
			}
			targets = append(targets, *target)

			// Generate another target right next to this one for an anonymous user. Do this because
			// we want 50% of traffic to be unauthenticated
			anonTarget, err := GenerateVODTarget("", vodID)
			if err != nil {
				return nil, errors.Wrap(err, "Failed to generate request target")
			}
			targets = append(targets, *anonTarget)
		}
	}

	return targets, nil
}

func GenerateVODTarget(userID string, vodID string) (*vegeta.Target, error) {
	request := borpc.GetVODAccessTokenRequest{
		UserId:     userID,
		VodId:      vodID,
		NeedHttps:  false,
		Platform:   "loadtest",
		PlayerType: "loadtest",
	}

	requestBytes, err := json.Marshal(request)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to marshal request")
	}

	headers := http.Header{
		"Content-Type": {"application/json"},
	}
	target := &vegeta.Target{
		Method: "POST",
		URL:    vodEndpoint,
		Body:   requestBytes,
		Header: headers,
	}

	return target, nil
}
