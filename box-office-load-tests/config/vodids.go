package config

// VODIDData is the vodID test data
type VODIDData struct {
	VODIDs []string `validate:"nonzero"`
}
