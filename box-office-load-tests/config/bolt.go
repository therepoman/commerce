package config

// BOLTConfig is the Box Office Load Test configuration
type BOLTConfig struct {
	VideoRequestsPerSecond uint64 `validate:"nonzero"`
	VODRequestsPerSecond   uint64 `validate:"nonzero"`
	DurationSeconds        int    `validate:"nonzero"`
}
