package config

// UserIDData is the userID test data
type UserIDData struct {
	UserIDs []string `validate:"nonzero"`
}
