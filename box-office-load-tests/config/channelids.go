package config

// ChannelIDData is the channelID test data
type ChannelIDData struct {
	ChannelIDs []string `validate:"nonzero"`
}
