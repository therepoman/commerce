package main

import (
	"fmt"
	"os"
	"os/signal"
	"time"

	boltConfig "code.justin.tv/commerce/box-office-load-tests/config"
	"code.justin.tv/commerce/box-office-load-tests/internal/requests"
	"code.justin.tv/commerce/config"
	log "github.com/sirupsen/logrus"
	"github.com/tsenart/vegeta/lib"
)

const (
	configPath = "config/%s.json"
)

func main() {

	log.Info("---- Starting Box Office load tests ----")

	// Load BOLT config
	boltFile := "bolt"
	boltOptions := &config.Options{
		RepoOrganization:   "commerce",
		RepoService:        "box-office-load-tests",
		Environment:        boltFile,
		GlobalPathOverride: fmt.Sprintf(configPath, boltFile),
	}
	boltCfg := boltConfig.BOLTConfig{}
	if err := config.LoadConfigForEnvironment(&boltCfg, boltOptions); err != nil {
		log.Fatalf("Failed to initialize Bolt config: %+v", err)
	}

	log.WithFields(log.Fields{
		"VideoRequestsPerSecond": boltCfg.VideoRequestsPerSecond,
		"VODRequestsPerSecond":   boltCfg.VODRequestsPerSecond,
		"DurationSeconds":        boltCfg.DurationSeconds,
	}).Info("Successfully loaded BOLT config.")

	duration := time.Duration(boltCfg.DurationSeconds) * time.Second

	// Load userID Data
	userIDFile := "userids"
	userIDOptions := &config.Options{
		RepoOrganization:   "commerce",
		RepoService:        "box-office-load-tests",
		Environment:        userIDFile,
		GlobalPathOverride: fmt.Sprintf(configPath, userIDFile),
	}
	userIDData := boltConfig.UserIDData{}
	if err := config.LoadConfigForEnvironment(&userIDData, userIDOptions); err != nil {
		log.Fatalf("Failed to initialize UserID data: %+v", err)
	}
	log.WithField("Num UserIDs", len(userIDData.UserIDs)).Info("Successfully loaded UserID data.")

	// Load channelID Data
	channelIDFile := "channelids"
	channelIDOptions := &config.Options{
		RepoOrganization:   "commerce",
		RepoService:        "box-office-load-tests",
		Environment:        channelIDFile,
		GlobalPathOverride: fmt.Sprintf(configPath, channelIDFile),
	}
	channelIDData := boltConfig.ChannelIDData{}
	if err := config.LoadConfigForEnvironment(&channelIDData, channelIDOptions); err != nil {
		log.Fatalf("Failed to initialize ChannelID data: %+v", err)
	}
	log.WithField("Num ChannelIDs", len(channelIDData.ChannelIDs)).Info("Successfully loaded ChannelID data.")

	// Load vodID Data
	vodIDFile := "vodids"
	vodIDOptions := &config.Options{
		RepoOrganization:   "commerce",
		RepoService:        "box-office-load-tests",
		Environment:        vodIDFile,
		GlobalPathOverride: fmt.Sprintf(configPath, vodIDFile),
	}
	vodIDData := boltConfig.VODIDData{}
	if err := config.LoadConfigForEnvironment(&vodIDData, vodIDOptions); err != nil {
		log.Fatalf("Failed to initialize VODID data: %+v", err)
	}
	log.WithField("Num VODIDs", len(vodIDData.VODIDs)).Info("Successfully loaded VODID data.")

	// Build targets
	log.Info("Building Video targets...")
	videoTargets, err := requests.GenerateVideoTargets(userIDData.UserIDs, channelIDData.ChannelIDs)
	if err != nil {
		log.Fatalf("Failed to generate Video targets. Terminating. Error: %+v", err)
	}
	log.WithField("Num Targets", len(videoTargets)).Info("Successfully generated Video targets")
	log.Info("Successfully built Video targets")

	log.Info("Building VOD targets...")
	vodTargets, err := requests.GenerateVODTargets(userIDData.UserIDs, vodIDData.VODIDs)
	if err != nil {
		log.Fatalf("Failed to generate VOD targets. Terminating. Error: %+v", err)
	}
	log.WithField("Num Targets", len(vodTargets)).Info("Successfully generated VOD targets")
	log.Info("Successfully built VOD targets")

	videoTargeter := vegeta.NewStaticTargeter(videoTargets...)
	vodTargeter := vegeta.NewStaticTargeter(vodTargets...)

	doneChan := make(chan int, 2)
	videoGenerator := NewGenerator("video", &videoTargeter, boltCfg.VideoRequestsPerSecond, duration, doneChan)
	vodGenerator := NewGenerator("vod", &vodTargeter, boltCfg.VODRequestsPerSecond, duration, doneChan)

	go videoGenerator.Run()
	go vodGenerator.Run()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for range c {
			log.Warn("Caught SIGINT. Shutting down...")
			videoGenerator.Shutdown()
			vodGenerator.Shutdown()
		}
	}()

	<-doneChan
	<-doneChan

	log.Info("Video report:")
	videoReporter := vegeta.NewTextReporter(videoGenerator.Metrics)
	err = videoReporter.Report(os.Stdout)
	if err != nil {
		log.Fatalf("Failed to report metrics. Terminating. Error: %+v", err)
	}

	log.Info("VOD report:")
	vodReporter := vegeta.NewTextReporter(vodGenerator.Metrics)
	err = vodReporter.Report(os.Stdout)
	if err != nil {
		log.Fatalf("Failed to report metrics. Terminating. Error: %+v", err)
	}

	log.Info("-- Box Office load testing run complete --")
}

type Generator struct {
	Name              string
	Targeter          *vegeta.Targeter
	RequestsPerSecond uint64
	Duration          time.Duration

	Attacker *vegeta.Attacker
	Metrics  *vegeta.Metrics
	DoneChan chan int
}

func NewGenerator(name string, targeter *vegeta.Targeter, requestsPerSecond uint64, duration time.Duration, doneChan chan int) *Generator {
	return &Generator{
		Name:              name,
		Targeter:          targeter,
		RequestsPerSecond: requestsPerSecond,
		Duration:          duration,
		Attacker:          vegeta.NewAttacker(),
		Metrics:           &vegeta.Metrics{},
		DoneChan:          doneChan,
	}
}

func (g *Generator) Run() {
	// Generate load
	log.Infof("Generating load for %s...", g.Name)
	for res := range g.Attacker.Attack(*g.Targeter, g.RequestsPerSecond, g.Duration) {
		g.Metrics.Add(res)
	}

	log.Infof("Load generation for %s complete", g.Name)

	g.Metrics.Close()
	g.DoneChan <- 1
}

func (g *Generator) Shutdown() {
	g.Attacker.Stop()
}
