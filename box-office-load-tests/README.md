# box-office-load-tests
BOLT (TM) - Box Office Load Tests

## Overview
This repo contains load generation tools for Box Office. It uses a compiled executable built on the Vegeta library and config files to specify configuration such as Requests Per Second and Duration.

See Also:
* [Box Office](https://git-aws.internal.justin.tv/commerce/box-office)
* [Vegeta](https://github.com/tsenart/vegeta)

## Usage

### Retool
This project uses [Retool](https://github.com/twitchtv/retool) to ensure collaborators use the same versions of development tools.

Note that running `make setup` will install Retool for you.

 - Install Retool:

 ```
 go get -u github.com/twitchtv/retool
 ```

  - Use Retool to ensure your installed tools match those specified in `tools.json`:

```
retool sync
```

 - Execute a command with a versioned tool:

 ```
retool do dep ensure
 ```

### Running load generation locally
Load generation is generally a hardware intensive task, and can't be done effectively at high load on a laptop. I have seen my MacBook max out at about ~3k RPS. If you need more than that, spin up a beefy EC2 host and generate load from there. I have successfully generated up to about 60k TPS from a c5.9xl. Nonetheless, testing locally is useful for development and testing.

- To run the load generator locally using the configuration specified in config/:

```
$ make run
```

### Generating load from EC2

#### Setting up an EC2 host
1. Go to the AWS console for the `twitch-box-office-dev` account.
2. Select `EC2`
3. Spin up the instance of your choosing. Ensure that it is in the VPC, subnet, security group, etc. so that it can target Box Office prod. I recommend the `c5.9xl` instance type`
4. Save the PEM identity file locally and the IP address of the new host. You will need these for syncing to the host
5. Create BOLT environment variables using these fields. Here are mine (I put them in my ~/.zshrc):
```
export BOLT_IDENTITY="~/Downloads/box_office_loadgen.pem"
export BOLT_DESTINATION="ec2-user@10.202.240.137"
```

#### Running BOLT on an EC2 host
6. From your workspace, run `make sync`. This wil automatically build the executable and scp it and the config files over to the host to the `~/bolt` directory
7. SSH onto the host with `ssh -i $BOLT_IDENTITY $BOLT_DESTINATION`
8. `cd ~/bolt`
9. Run the executable: `./bolt`


### Updating Dependencies
This repo's dependencies are managed with [`dep`](https://github.com/golang/dep).

#### Installing Dependencies
Run `make dep` to ensure `vendor/` is in the correct state for your configuration.

```
$ make dep
```

**Note**: This process may be slow for the first time if dependencies are cloned or if it hasn't been run in awhile with many changes to fetch.

#### Adding Dependencies
Run `dep ensure -add` to update your configuration with new dependencies

```
$ dep ensure -add github.com/foo/bar github.com/foo/baz
```

`dep` will update `Gopkg.toml`, `Gopkg.lock`, and your `vendor/` files to the latest version unless a version constraint is specified, IE:

```
$ dep ensure -add github.com/foo/bar@1.0.0
```

**Note**: If you do not use `-add`, this package will not get saved in the `gopkg.toml` file

#### Checking Dependencies' Status
You can check if there is a mismatch between the configuration and the state of your project by checking the status.

```
$ dep status
```

#### Removing Dependencies
1. Remove any `import` package statements and usage from the code
2. Remove relevant `[[contraint]]` rules from `Gopkg.toml`
3. Run `dep ensure`

#### Further Reading
- [dep Github](https://github.com/golang/dep)
- [dep FAQ](https://github.com/golang/dep/blob/master/docs/FAQ.md)
