package config

import (
	"encoding/json"
	"fmt"
	"os"

	"gopkg.in/validator.v2"
)

type Options struct {
	RepoOrganization   string // e.g. commerce
	RepoService        string // e.g. fortuna
	Environment        string
	LocalPathOverride  string
	GlobalPathOverride string
}

const (
	localConfigFilePath    = "/src/code.justin.tv/%s/%s/config/%s.json"
	globalConfigFilePath   = "/etc/%s/config/%s.json"
	unspecifiedEnvironment = ""
	devEnvironment         = "dev"
)

func GetEnvironment() string {
	env := os.Getenv("ENVIRONMENT")
	if env == unspecifiedEnvironment {
		return devEnvironment
	}
	return env
}

func LoadConfigForEnvironment(configuration interface{}, options *Options) error {
	if options.RepoOrganization == "" || options.RepoService == "" {
		return fmt.Errorf("missing organization name or service name")
	}

	configFilePath, err := getConfigFilePath(options)
	if err != nil {
		return err
	}
	return loadConfig(configFilePath, configuration)
}

func getConfigFilePath(options *Options) (string, error) {
	localFileName := ""
	if options.LocalPathOverride != "" {
		localFileName = options.LocalPathOverride
	} else {
		localFileName = os.Getenv("GOPATH") + getLocalFileFilePath(options)
	}

	if _, err := os.Stat(localFileName); !os.IsNotExist(err) {
		return localFileName, nil
	}

	globalFileName := ""
	if options.GlobalPathOverride != "" {
		globalFileName = options.GlobalPathOverride
	} else {
		globalFileName = fmt.Sprintf(globalConfigFilePath, options.RepoService, options.Environment)
	}

	if _, err := os.Stat(globalFileName); os.IsNotExist(err) {
		return "", err
	}
	return globalFileName, nil
}

func getLocalFileFilePath(options *Options) string {
	return fmt.Sprintf(localConfigFilePath, options.RepoOrganization, options.RepoService, options.Environment)
}

func loadConfig(filePath string, configOptions interface{}) error {
	file, err := os.Open(filePath)
	if err != nil {
		return fmt.Errorf("failed to open config file: %v", err)
	}

	decoder := json.NewDecoder(file)

	if err := decoder.Decode(configOptions); err != nil {
		return fmt.Errorf("failed to parse configuration file: %v", err)
	}

	if err := validator.Validate(configOptions); err != nil {
		return fmt.Errorf("failed to validate configuration file: %v", err)
	}

	return nil
}
