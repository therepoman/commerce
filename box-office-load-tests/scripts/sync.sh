#!/bin/bash

# Usage:
# ./sync.sh <identity> <destination>
# e.g.:
# ./sync.sh ~/Downloads/box_office_loadgen.pem ec2-user@10.202.240.137

echo "Num arguments: $#"
if [ "$#" -ne 2 ]; then
    echo "Illegal number of arguments"
    exit
fi
identity=$1
echo "Identity file location: $identity"
destination=$2
echo "SCP Destination: $destination"

directory="~/bolt/"

echo "Syncing to remote"
# make the directory
ssh -i $identity $destination "mkdir $directory"

# copy executable
scp -i $identity bin/bolt $destination:$directory

# copy config
scp -r -i $identity config/ $destination:$directory
echo "Sync completed"