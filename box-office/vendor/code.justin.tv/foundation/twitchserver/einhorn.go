// build !windows

package twitchserver

import (
	"os"
	"strconv"
	"syscall"
)

// NOTE: this is here only to keep the behavior from goji/graceful.
// This should be removed in a future refactor.
func init() {
	mpid, err := strconv.Atoi(os.Getenv("EINHORN_MASTER_PID"))
	if err != nil || mpid != os.Getppid() {
		return
	}

	stdSignals = append(stdSignals, syscall.SIGUSR2)
}
