// Code generated by protoc-gen-go. DO NOT EDIT.
// source: proto/dads.proto

/*
Package code_justin_tv_ads_dads_proto is a generated protocol buffer package.

It is generated from these files:
	proto/dads.proto

It has these top-level messages:
	ShowAdsResponse
	ShowAdsInput
	ShowAdsOutput
	GetAdblockStatusReq
	GetAdblockStatusResp
	Device
	ShowServerAdsReq
	ShowServerAdsResp
*/
package code_justin_tv_ads_dads_proto

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import code_justin_tv_ads_fring_grpc "code.justin.tv/ads/fring/grpc"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type PlayerType int32

const (
	PlayerType_SITE        PlayerType = 0
	PlayerType_SITE_MINI   PlayerType = 1
	PlayerType_EMBED       PlayerType = 2
	PlayerType_POPOUT      PlayerType = 3
	PlayerType_FRONTPAGE   PlayerType = 4
	PlayerType_DASHBOARD   PlayerType = 5
	PlayerType_CREATIVE    PlayerType = 6
	PlayerType_CURSE       PlayerType = 7
	PlayerType_FACEBOOK    PlayerType = 8
	PlayerType_HIGHLIGHTER PlayerType = 9
	PlayerType_ONBOARDING  PlayerType = 10
)

var PlayerType_name = map[int32]string{
	0:  "SITE",
	1:  "SITE_MINI",
	2:  "EMBED",
	3:  "POPOUT",
	4:  "FRONTPAGE",
	5:  "DASHBOARD",
	6:  "CREATIVE",
	7:  "CURSE",
	8:  "FACEBOOK",
	9:  "HIGHLIGHTER",
	10: "ONBOARDING",
}
var PlayerType_value = map[string]int32{
	"SITE":        0,
	"SITE_MINI":   1,
	"EMBED":       2,
	"POPOUT":      3,
	"FRONTPAGE":   4,
	"DASHBOARD":   5,
	"CREATIVE":    6,
	"CURSE":       7,
	"FACEBOOK":    8,
	"HIGHLIGHTER": 9,
	"ONBOARDING":  10,
}

func (x PlayerType) String() string {
	return proto.EnumName(PlayerType_name, int32(x))
}
func (PlayerType) EnumDescriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

type PlatformType int32

const (
	PlatformType_WEB         PlatformType = 0
	PlatformType_MOBILE_WEB  PlatformType = 1
	PlatformType_IOS         PlatformType = 2
	PlatformType_ANDROID     PlatformType = 3
	PlatformType_PLAYSTATION PlatformType = 4
	PlatformType_XBOXONE     PlatformType = 5
)

var PlatformType_name = map[int32]string{
	0: "WEB",
	1: "MOBILE_WEB",
	2: "IOS",
	3: "ANDROID",
	4: "PLAYSTATION",
	5: "XBOXONE",
}
var PlatformType_value = map[string]int32{
	"WEB":         0,
	"MOBILE_WEB":  1,
	"IOS":         2,
	"ANDROID":     3,
	"PLAYSTATION": 4,
	"XBOXONE":     5,
}

func (x PlatformType) String() string {
	return proto.EnumName(PlatformType_name, int32(x))
}
func (PlatformType) EnumDescriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

type ShowAdsResponse struct {
	ShowAds bool           `protobuf:"varint,1,opt,name=show_ads,json=showAds" json:"show_ads,omitempty"`
	Output  *ShowAdsOutput `protobuf:"bytes,2,opt,name=output" json:"output,omitempty"`
}

func (m *ShowAdsResponse) Reset()                    { *m = ShowAdsResponse{} }
func (m *ShowAdsResponse) String() string            { return proto.CompactTextString(m) }
func (*ShowAdsResponse) ProtoMessage()               {}
func (*ShowAdsResponse) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *ShowAdsResponse) GetShowAds() bool {
	if m != nil {
		return m.ShowAds
	}
	return false
}

func (m *ShowAdsResponse) GetOutput() *ShowAdsOutput {
	if m != nil {
		return m.Output
	}
	return nil
}

type ShowAdsInput struct {
	UserId       string                                    `protobuf:"bytes,1,opt,name=user_id,json=userId" json:"user_id,omitempty"`
	ChannelId    string                                    `protobuf:"bytes,2,opt,name=channel_id,json=channelId" json:"channel_id,omitempty"`
	DeviceId     string                                    `protobuf:"bytes,3,opt,name=device_id,json=deviceId" json:"device_id,omitempty"`
	ForcePreroll bool                                      `protobuf:"varint,4,opt,name=force_preroll,json=forcePreroll" json:"force_preroll,omitempty"`
	ForceMidroll bool                                      `protobuf:"varint,5,opt,name=force_midroll,json=forceMidroll" json:"force_midroll,omitempty"`
	PlayerType   PlayerType                                `protobuf:"varint,6,opt,name=player_type,json=playerType,enum=code.justin.tv.ads.dads.proto.PlayerType" json:"player_type,omitempty"`
	RollType     code_justin_tv_ads_fring_grpc.RollType    `protobuf:"varint,7,opt,name=roll_type,json=rollType,enum=code.justin.tv.ads.fring.grpc.RollType" json:"roll_type,omitempty"`
	PlatformType PlatformType                              `protobuf:"varint,8,opt,name=platform_type,json=platformType,enum=code.justin.tv.ads.dads.proto.PlatformType" json:"platform_type,omitempty"`
	Turbo        bool                                      `protobuf:"varint,9,opt,name=turbo" json:"turbo,omitempty"`
	Subscriber   bool                                      `protobuf:"varint,10,opt,name=subscriber" json:"subscriber,omitempty"`
	HideAds      bool                                      `protobuf:"varint,11,opt,name=hide_ads,json=hideAds" json:"hide_ads,omitempty"`
	ContentType  code_justin_tv_ads_fring_grpc.ContentType `protobuf:"varint,14,opt,name=content_type,json=contentType,enum=code.justin.tv.ads.fring.grpc.ContentType" json:"content_type,omitempty"`
	VodId        string                                    `protobuf:"bytes,15,opt,name=vod_id,json=vodId" json:"vod_id,omitempty"`
	AdSessionId  string                                    `protobuf:"bytes,16,opt,name=ad_session_id,json=adSessionId" json:"ad_session_id,omitempty"`
}

func (m *ShowAdsInput) Reset()                    { *m = ShowAdsInput{} }
func (m *ShowAdsInput) String() string            { return proto.CompactTextString(m) }
func (*ShowAdsInput) ProtoMessage()               {}
func (*ShowAdsInput) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *ShowAdsInput) GetUserId() string {
	if m != nil {
		return m.UserId
	}
	return ""
}

func (m *ShowAdsInput) GetChannelId() string {
	if m != nil {
		return m.ChannelId
	}
	return ""
}

func (m *ShowAdsInput) GetDeviceId() string {
	if m != nil {
		return m.DeviceId
	}
	return ""
}

func (m *ShowAdsInput) GetForcePreroll() bool {
	if m != nil {
		return m.ForcePreroll
	}
	return false
}

func (m *ShowAdsInput) GetForceMidroll() bool {
	if m != nil {
		return m.ForceMidroll
	}
	return false
}

func (m *ShowAdsInput) GetPlayerType() PlayerType {
	if m != nil {
		return m.PlayerType
	}
	return PlayerType_SITE
}

func (m *ShowAdsInput) GetRollType() code_justin_tv_ads_fring_grpc.RollType {
	if m != nil {
		return m.RollType
	}
	return code_justin_tv_ads_fring_grpc.RollType_MIDROLL
}

func (m *ShowAdsInput) GetPlatformType() PlatformType {
	if m != nil {
		return m.PlatformType
	}
	return PlatformType_WEB
}

func (m *ShowAdsInput) GetTurbo() bool {
	if m != nil {
		return m.Turbo
	}
	return false
}

func (m *ShowAdsInput) GetSubscriber() bool {
	if m != nil {
		return m.Subscriber
	}
	return false
}

func (m *ShowAdsInput) GetHideAds() bool {
	if m != nil {
		return m.HideAds
	}
	return false
}

func (m *ShowAdsInput) GetContentType() code_justin_tv_ads_fring_grpc.ContentType {
	if m != nil {
		return m.ContentType
	}
	return code_justin_tv_ads_fring_grpc.ContentType_LIVE
}

func (m *ShowAdsInput) GetVodId() string {
	if m != nil {
		return m.VodId
	}
	return ""
}

func (m *ShowAdsInput) GetAdSessionId() string {
	if m != nil {
		return m.AdSessionId
	}
	return ""
}

// these are properties that are needed to complete the DFP request
type ShowAdsOutput struct {
	PartnerStatus bool   `protobuf:"varint,1,opt,name=partner_status,json=partnerStatus" json:"partner_status,omitempty"`
	CurrentGame   string `protobuf:"bytes,2,opt,name=current_game,json=currentGame" json:"current_game,omitempty"`
	MatureStatus  bool   `protobuf:"varint,3,opt,name=mature_status,json=matureStatus" json:"mature_status,omitempty"`
	ChannelName   string `protobuf:"bytes,4,opt,name=channel_name,json=channelName" json:"channel_name,omitempty"`
}

func (m *ShowAdsOutput) Reset()                    { *m = ShowAdsOutput{} }
func (m *ShowAdsOutput) String() string            { return proto.CompactTextString(m) }
func (*ShowAdsOutput) ProtoMessage()               {}
func (*ShowAdsOutput) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{2} }

func (m *ShowAdsOutput) GetPartnerStatus() bool {
	if m != nil {
		return m.PartnerStatus
	}
	return false
}

func (m *ShowAdsOutput) GetCurrentGame() string {
	if m != nil {
		return m.CurrentGame
	}
	return ""
}

func (m *ShowAdsOutput) GetMatureStatus() bool {
	if m != nil {
		return m.MatureStatus
	}
	return false
}

func (m *ShowAdsOutput) GetChannelName() string {
	if m != nil {
		return m.ChannelName
	}
	return ""
}

type GetAdblockStatusReq struct {
	DeviceId string `protobuf:"bytes,1,opt,name=device_id,json=deviceId" json:"device_id,omitempty"`
	Country  string `protobuf:"bytes,2,opt,name=country" json:"country,omitempty"`
}

func (m *GetAdblockStatusReq) Reset()                    { *m = GetAdblockStatusReq{} }
func (m *GetAdblockStatusReq) String() string            { return proto.CompactTextString(m) }
func (*GetAdblockStatusReq) ProtoMessage()               {}
func (*GetAdblockStatusReq) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{3} }

func (m *GetAdblockStatusReq) GetDeviceId() string {
	if m != nil {
		return m.DeviceId
	}
	return ""
}

func (m *GetAdblockStatusReq) GetCountry() string {
	if m != nil {
		return m.Country
	}
	return ""
}

type GetAdblockStatusResp struct {
	Adblock bool `protobuf:"varint,1,opt,name=adblock" json:"adblock,omitempty"`
}

func (m *GetAdblockStatusResp) Reset()                    { *m = GetAdblockStatusResp{} }
func (m *GetAdblockStatusResp) String() string            { return proto.CompactTextString(m) }
func (*GetAdblockStatusResp) ProtoMessage()               {}
func (*GetAdblockStatusResp) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{4} }

func (m *GetAdblockStatusResp) GetAdblock() bool {
	if m != nil {
		return m.Adblock
	}
	return false
}

type Device struct {
	DeviceId string `protobuf:"bytes,1,opt,name=device_id,json=deviceId" json:"device_id,omitempty"`
	Adblock  bool   `protobuf:"varint,2,opt,name=adblock" json:"adblock,omitempty"`
}

func (m *Device) Reset()                    { *m = Device{} }
func (m *Device) String() string            { return proto.CompactTextString(m) }
func (*Device) ProtoMessage()               {}
func (*Device) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{5} }

func (m *Device) GetDeviceId() string {
	if m != nil {
		return m.DeviceId
	}
	return ""
}

func (m *Device) GetAdblock() bool {
	if m != nil {
		return m.Adblock
	}
	return false
}

type ShowServerAdsReq struct {
	UserId        string `protobuf:"bytes,1,opt,name=user_id,json=userId" json:"user_id,omitempty"`
	ChannelId     string `protobuf:"bytes,2,opt,name=channel_id,json=channelId" json:"channel_id,omitempty"`
	DeviceId      string `protobuf:"bytes,3,opt,name=device_id,json=deviceId" json:"device_id,omitempty"`
	Country       string `protobuf:"bytes,4,opt,name=country" json:"country,omitempty"`
	Turbo         bool   `protobuf:"varint,5,opt,name=turbo" json:"turbo,omitempty"`
	Subscriber    bool   `protobuf:"varint,6,opt,name=subscriber" json:"subscriber,omitempty"`
	AdsFree       bool   `protobuf:"varint,7,opt,name=ads_free,json=adsFree" json:"ads_free,omitempty"`
	Platform      string `protobuf:"bytes,8,opt,name=platform" json:"platform,omitempty"`
	PlayerType    string `protobuf:"bytes,9,opt,name=player_type,json=playerType" json:"player_type,omitempty"`
	PlayerBackend string `protobuf:"bytes,10,opt,name=player_backend,json=playerBackend" json:"player_backend,omitempty"`
}

func (m *ShowServerAdsReq) Reset()                    { *m = ShowServerAdsReq{} }
func (m *ShowServerAdsReq) String() string            { return proto.CompactTextString(m) }
func (*ShowServerAdsReq) ProtoMessage()               {}
func (*ShowServerAdsReq) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{6} }

func (m *ShowServerAdsReq) GetUserId() string {
	if m != nil {
		return m.UserId
	}
	return ""
}

func (m *ShowServerAdsReq) GetChannelId() string {
	if m != nil {
		return m.ChannelId
	}
	return ""
}

func (m *ShowServerAdsReq) GetDeviceId() string {
	if m != nil {
		return m.DeviceId
	}
	return ""
}

func (m *ShowServerAdsReq) GetCountry() string {
	if m != nil {
		return m.Country
	}
	return ""
}

func (m *ShowServerAdsReq) GetTurbo() bool {
	if m != nil {
		return m.Turbo
	}
	return false
}

func (m *ShowServerAdsReq) GetSubscriber() bool {
	if m != nil {
		return m.Subscriber
	}
	return false
}

func (m *ShowServerAdsReq) GetAdsFree() bool {
	if m != nil {
		return m.AdsFree
	}
	return false
}

func (m *ShowServerAdsReq) GetPlatform() string {
	if m != nil {
		return m.Platform
	}
	return ""
}

func (m *ShowServerAdsReq) GetPlayerType() string {
	if m != nil {
		return m.PlayerType
	}
	return ""
}

func (m *ShowServerAdsReq) GetPlayerBackend() string {
	if m != nil {
		return m.PlayerBackend
	}
	return ""
}

type ShowServerAdsResp struct {
	ServerAds bool `protobuf:"varint,1,opt,name=server_ads,json=serverAds" json:"server_ads,omitempty"`
	Adblock   bool `protobuf:"varint,2,opt,name=adblock" json:"adblock,omitempty"`
}

func (m *ShowServerAdsResp) Reset()                    { *m = ShowServerAdsResp{} }
func (m *ShowServerAdsResp) String() string            { return proto.CompactTextString(m) }
func (*ShowServerAdsResp) ProtoMessage()               {}
func (*ShowServerAdsResp) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{7} }

func (m *ShowServerAdsResp) GetServerAds() bool {
	if m != nil {
		return m.ServerAds
	}
	return false
}

func (m *ShowServerAdsResp) GetAdblock() bool {
	if m != nil {
		return m.Adblock
	}
	return false
}

func init() {
	proto.RegisterType((*ShowAdsResponse)(nil), "code.justin.tv.ads.dads.proto.ShowAdsResponse")
	proto.RegisterType((*ShowAdsInput)(nil), "code.justin.tv.ads.dads.proto.ShowAdsInput")
	proto.RegisterType((*ShowAdsOutput)(nil), "code.justin.tv.ads.dads.proto.ShowAdsOutput")
	proto.RegisterType((*GetAdblockStatusReq)(nil), "code.justin.tv.ads.dads.proto.GetAdblockStatusReq")
	proto.RegisterType((*GetAdblockStatusResp)(nil), "code.justin.tv.ads.dads.proto.GetAdblockStatusResp")
	proto.RegisterType((*Device)(nil), "code.justin.tv.ads.dads.proto.Device")
	proto.RegisterType((*ShowServerAdsReq)(nil), "code.justin.tv.ads.dads.proto.ShowServerAdsReq")
	proto.RegisterType((*ShowServerAdsResp)(nil), "code.justin.tv.ads.dads.proto.ShowServerAdsResp")
	proto.RegisterEnum("code.justin.tv.ads.dads.proto.PlayerType", PlayerType_name, PlayerType_value)
	proto.RegisterEnum("code.justin.tv.ads.dads.proto.PlatformType", PlatformType_name, PlatformType_value)
}

func init() { proto.RegisterFile("proto/dads.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 952 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xb4, 0x55, 0xdd, 0x72, 0xe3, 0x34,
	0x14, 0x6e, 0xfe, 0xe3, 0x93, 0xa4, 0x15, 0x62, 0x19, 0x42, 0x98, 0x85, 0x62, 0x86, 0xd9, 0xd2,
	0x65, 0x92, 0x9d, 0xee, 0x03, 0x30, 0x4e, 0xe3, 0xa6, 0x86, 0x24, 0xce, 0x38, 0x59, 0x58, 0x86,
	0x8b, 0x8c, 0x62, 0xa9, 0x4d, 0xd8, 0xc4, 0xf6, 0x4a, 0x76, 0x76, 0x7a, 0xc1, 0xc3, 0xc0, 0x43,
	0xf0, 0x46, 0xdc, 0xf1, 0x10, 0x8c, 0x24, 0xa7, 0x49, 0x4a, 0x97, 0xb4, 0x17, 0x7b, 0xa7, 0xf3,
	0x7d, 0xe7, 0x7c, 0xd6, 0x91, 0xbe, 0x23, 0x03, 0x8a, 0x78, 0x18, 0x87, 0x2d, 0x4a, 0xa8, 0x68,
	0xaa, 0x25, 0x7e, 0xea, 0x87, 0x94, 0x35, 0x7f, 0x4b, 0x44, 0x3c, 0x0f, 0x9a, 0xf1, 0xaa, 0x29,
	0x99, 0x0d, 0xdd, 0x78, 0xb6, 0x4b, 0xb7, 0x08, 0x15, 0xad, 0x2b, 0x3e, 0x0f, 0xae, 0x5b, 0xd7,
	0x3c, 0xf2, 0x5b, 0x64, 0x99, 0x26, 0x9a, 0x1c, 0x8e, 0x46, 0xb3, 0xf0, 0x9d, 0x45, 0x85, 0xc7,
	0x44, 0x14, 0x06, 0x82, 0xe1, 0xcf, 0xa0, 0x2c, 0x66, 0xe1, 0xbb, 0x09, 0xa1, 0xa2, 0x9e, 0x39,
	0xce, 0x9c, 0x94, 0xbd, 0x92, 0xd0, 0x29, 0xb8, 0x03, 0xc5, 0x30, 0x89, 0xa3, 0x24, 0xae, 0x67,
	0x8f, 0x33, 0x27, 0x95, 0xb3, 0xef, 0x9a, 0xff, 0xbb, 0x8d, 0x66, 0x2a, 0xed, 0xaa, 0x1a, 0x2f,
	0xad, 0x35, 0xff, 0xc9, 0x43, 0x35, 0x65, 0x9c, 0x20, 0x4a, 0x62, 0xfc, 0x29, 0x94, 0x12, 0xc1,
	0xf8, 0x64, 0x4e, 0xd5, 0x07, 0x0d, 0xaf, 0x28, 0x43, 0x87, 0xe2, 0xa7, 0x00, 0xfe, 0x8c, 0x04,
	0x01, 0x5b, 0x48, 0x2e, 0xab, 0x38, 0x23, 0x45, 0x1c, 0x8a, 0x3f, 0x07, 0x83, 0xb2, 0xd5, 0xdc,
	0x67, 0x92, 0xcd, 0x29, 0xb6, 0xac, 0x01, 0x87, 0xe2, 0xaf, 0xa1, 0x76, 0x15, 0x72, 0x9f, 0x4d,
	0x22, 0xce, 0x78, 0xb8, 0x58, 0xd4, 0xf3, 0xaa, 0x97, 0xaa, 0x02, 0x87, 0x1a, 0xdb, 0x24, 0x2d,
	0xe7, 0x54, 0x25, 0x15, 0xb6, 0x92, 0xfa, 0x1a, 0xc3, 0x3f, 0x40, 0x25, 0x5a, 0x90, 0x1b, 0xc6,
	0x27, 0xf1, 0x4d, 0xc4, 0xea, 0xc5, 0xe3, 0xcc, 0xc9, 0xe1, 0xd9, 0xb7, 0x7b, 0x5a, 0x1f, 0xaa,
	0x8a, 0xf1, 0x4d, 0xc4, 0x3c, 0x88, 0x6e, 0xd7, 0xb8, 0x03, 0x86, 0xd4, 0xd4, 0x4a, 0x25, 0xa5,
	0xf4, 0xec, 0x3e, 0x25, 0x75, 0x59, 0x4d, 0x79, 0x59, 0x4d, 0x2f, 0x5c, 0x2c, 0x94, 0x4e, 0x99,
	0xa7, 0x2b, 0x3c, 0x84, 0x5a, 0xb4, 0x20, 0xf1, 0x55, 0xc8, 0x97, 0x5a, 0xa9, 0xac, 0x94, 0x9e,
	0xef, 0xdf, 0x93, 0xaa, 0x51, 0x6a, 0xd5, 0x68, 0x2b, 0xc2, 0x4f, 0xa0, 0x10, 0x27, 0x7c, 0x1a,
	0xd6, 0x0d, 0x75, 0x00, 0x3a, 0xc0, 0x5f, 0x00, 0x88, 0x64, 0x2a, 0x7c, 0x3e, 0x9f, 0x32, 0x5e,
	0x07, 0x45, 0x6d, 0x21, 0xd2, 0x2a, 0xb3, 0x39, 0x65, 0xca, 0x2a, 0x15, 0x6d, 0x15, 0x19, 0x4b,
	0xab, 0xf4, 0xa1, 0xea, 0x87, 0x41, 0xcc, 0x82, 0x58, 0xef, 0xf0, 0x50, 0xed, 0xf0, 0x74, 0x4f,
	0xaf, 0xe7, 0xba, 0x44, 0x6d, 0xb0, 0xe2, 0x6f, 0x02, 0xfc, 0x09, 0x14, 0x57, 0x21, 0x95, 0xf7,
	0x7c, 0xa4, 0xee, 0xb9, 0xb0, 0x0a, 0xa9, 0x43, 0xb1, 0x09, 0x35, 0x42, 0x27, 0x82, 0x09, 0x31,
	0x0f, 0x03, 0xc9, 0x22, 0xc5, 0x56, 0x08, 0x1d, 0x69, 0xcc, 0xa1, 0xe6, 0x1f, 0x19, 0xa8, 0xed,
	0x18, 0x11, 0x7f, 0x03, 0x87, 0x11, 0xe1, 0x71, 0xc0, 0xf8, 0x44, 0xc4, 0x24, 0x4e, 0xd6, 0x3e,
	0xaf, 0xa5, 0xe8, 0x48, 0x81, 0xf8, 0x2b, 0xa8, 0xfa, 0x09, 0xe7, 0xb2, 0x85, 0x6b, 0xb2, 0x64,
	0xa9, 0xff, 0x2a, 0x29, 0xd6, 0x25, 0x4b, 0x26, 0xfd, 0xb3, 0x24, 0x71, 0xc2, 0xd9, 0x5a, 0x28,
	0xa7, 0xfd, 0xa3, 0xc1, 0x2d, 0x9d, 0xd4, 0xc5, 0x81, 0xd4, 0xc9, 0xa7, 0x3a, 0x1a, 0x1b, 0x90,
	0x25, 0x33, 0x7b, 0xf0, 0x71, 0x97, 0xc5, 0x16, 0x9d, 0x2e, 0x42, 0xff, 0x8d, 0x2e, 0xf3, 0xd8,
	0xdb, 0x5d, 0x83, 0x67, 0xee, 0x18, 0xbc, 0x0e, 0x25, 0x3f, 0x4c, 0x82, 0x98, 0xdf, 0xa4, 0x3b,
	0x5b, 0x87, 0xe6, 0x0b, 0x78, 0xf2, 0x5f, 0x35, 0x11, 0xc9, 0x0a, 0xa2, 0xc1, 0xf5, 0x60, 0xa7,
	0xa1, 0xf9, 0x3d, 0x14, 0x3b, 0x4a, 0x77, 0xef, 0x27, 0xd7, 0x02, 0xd9, 0x5d, 0x81, 0xbf, 0xb2,
	0x80, 0xe4, 0x21, 0x8f, 0x18, 0x5f, 0x31, 0xae, 0x9e, 0x93, 0xb7, 0x1f, 0x66, 0xae, 0xb7, 0xda,
	0xce, 0xef, 0xb4, 0xbd, 0xf1, 0x70, 0xe1, 0xfd, 0x1e, 0x2e, 0xde, 0xe7, 0x61, 0x42, 0xc5, 0xe4,
	0x8a, 0x33, 0x3d, 0x90, 0xaa, 0x29, 0x71, 0xc1, 0x19, 0xc3, 0x0d, 0x28, 0xaf, 0x87, 0x44, 0x4d,
	0x98, 0xe1, 0xdd, 0xc6, 0xf8, 0xcb, 0xdd, 0x47, 0xc1, 0x50, 0xf4, 0xf6, 0xa4, 0x4b, 0x93, 0xe9,
	0x84, 0x29, 0xf1, 0xdf, 0xb0, 0x80, 0xaa, 0xf9, 0x31, 0xbc, 0x9a, 0x46, 0xdb, 0x1a, 0x34, 0x7b,
	0xf0, 0xd1, 0x9d, 0x73, 0x13, 0x91, 0x3c, 0x1f, 0xa1, 0x80, 0xad, 0x47, 0xd8, 0x10, 0xeb, 0x94,
	0xf7, 0x5f, 0xc3, 0xe9, 0x9f, 0x19, 0x80, 0xcd, 0xcb, 0x83, 0xcb, 0x90, 0x1f, 0x39, 0x63, 0x1b,
	0x1d, 0xe0, 0x1a, 0x18, 0x72, 0x35, 0xe9, 0x3b, 0x03, 0x07, 0x65, 0xb0, 0x01, 0x05, 0xbb, 0xdf,
	0xb6, 0x3b, 0x28, 0x8b, 0x01, 0x8a, 0x43, 0x77, 0xe8, 0xbe, 0x1a, 0xa3, 0x9c, 0xcc, 0xba, 0xf0,
	0xdc, 0xc1, 0x78, 0x68, 0x75, 0x6d, 0x94, 0x97, 0x61, 0xc7, 0x1a, 0x5d, 0xb6, 0x5d, 0xcb, 0xeb,
	0xa0, 0x02, 0xae, 0x42, 0xf9, 0xdc, 0xb3, 0xad, 0xb1, 0xf3, 0x93, 0x8d, 0x8a, 0x52, 0xe2, 0xfc,
	0x95, 0x37, 0xb2, 0x51, 0x49, 0x12, 0x17, 0xd6, 0xb9, 0xdd, 0x76, 0xdd, 0x1f, 0x51, 0x19, 0x1f,
	0x41, 0xe5, 0xd2, 0xe9, 0x5e, 0xf6, 0x9c, 0xee, 0xe5, 0xd8, 0xf6, 0x90, 0x81, 0x0f, 0x01, 0xdc,
	0x81, 0x12, 0x71, 0x06, 0x5d, 0x04, 0xa7, 0xbf, 0x42, 0x75, 0xfb, 0x25, 0xc2, 0x25, 0xc8, 0xfd,
	0x6c, 0xb7, 0xd1, 0x81, 0x4c, 0xec, 0xbb, 0x6d, 0xa7, 0x67, 0x4f, 0x64, 0x9c, 0x91, 0x84, 0xe3,
	0x8e, 0x50, 0x16, 0x57, 0xa0, 0x64, 0x0d, 0x3a, 0x9e, 0xeb, 0x74, 0x50, 0x4e, 0xea, 0x0f, 0x7b,
	0xd6, 0x2f, 0xa3, 0xb1, 0x35, 0x76, 0xdc, 0x01, 0xca, 0x4b, 0xf6, 0x75, 0xdb, 0x7d, 0xed, 0x0e,
	0x6c, 0x54, 0x38, 0xfb, 0x3b, 0x0b, 0xf9, 0x0e, 0xa1, 0x02, 0xcf, 0xa0, 0x94, 0x4e, 0x3d, 0x7e,
	0xfe, 0xb0, 0xdf, 0x94, 0xfa, 0x19, 0x35, 0x9a, 0x0f, 0x4b, 0x5e, 0xff, 0x2e, 0xcd, 0x03, 0xfc,
	0x3b, 0xa0, 0xbb, 0xe3, 0x86, 0xcf, 0xf6, 0xa8, 0xdc, 0x33, 0xed, 0x8d, 0x97, 0x8f, 0xae, 0x11,
	0x91, 0x79, 0x80, 0x63, 0xfd, 0xbc, 0xdd, 0x3a, 0x08, 0xb7, 0x1e, 0xd0, 0xc1, 0xf6, 0x9c, 0x36,
	0x5e, 0x3c, 0xae, 0x40, 0x7e, 0x75, 0x5a, 0x54, 0xd4, 0xcb, 0x7f, 0x03, 0x00, 0x00, 0xff, 0xff,
	0xda, 0x94, 0x14, 0xe0, 0x9b, 0x08, 0x00, 0x00,
}
