package code_justin_tv_ads_fring_proto

import (
	"code.justin.tv/video/protocols/weaver"

	"github.com/golang/protobuf/proto"
)

// AdParamsFromRequestInfo generates the message that will be sent to `ads/fring` from the information contained in the
// encrypted payload associated with each Weaver-format request.
func AdParamsFromRequestInfo(ri *weaver.RequestInfo) (*AdParams, error) {
	adParams := &AdParams{
		PlaybackSession: ri.Session,
	}
	err := proto.Unmarshal(ri.AdvertisingMetadata, adParams.Metadata)
	if err != nil {
		return adParams, err
	}

	return adParams, nil
}
