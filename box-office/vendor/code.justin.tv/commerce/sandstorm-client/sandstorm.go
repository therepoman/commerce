package sandstorm

import (
	"time"

	"code.justin.tv/systems/sandstorm/manager"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
)

type Config struct {
	AWSRegion    string
	Duration     time.Duration
	ExpiryWindow time.Duration
	RoleARN      string
}

func DefaultConfig(roleArn string) Config {
	return Config{
		AWSRegion:    "us-west-2",
		Duration:     900 * time.Second,
		ExpiryWindow: 10 * time.Second,
		RoleARN:      roleArn,
	}
}

// NewSandstormManager connects to sandstorm and returns a sandstorm manager which can be used to
// retrieve secrets.
func New(config Config) (*manager.Manager, error) {
	awsConfig := &aws.Config{
		Region: aws.String(config.AWSRegion),
		STSRegionalEndpoint: endpoints.RegionalSTSEndpoint,
	}

	awsSession, err := session.NewSession(awsConfig)
	if err != nil {
		return nil, err
	}

	stsClient := sts.New(awsSession)

	arp := &stscreds.AssumeRoleProvider{
		Duration:     config.Duration,
		ExpiryWindow: config.ExpiryWindow,
		RoleARN:      config.RoleARN,
		Client:       stsClient,
	}

	creds := credentials.NewCredentials(arp)
	awsConfig.WithCredentials(creds)

	managerConfig := manager.Config{
		AWSConfig: awsConfig,
		TableName: "sandstorm-production",
		KeyID:     "alias/sandstorm-production",
	}

	m := manager.New(managerConfig)

	return m, nil
}
