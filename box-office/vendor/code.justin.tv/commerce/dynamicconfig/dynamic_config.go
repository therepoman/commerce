package dynamicconfig

import (
	"context"
	"reflect"
	"sync"

	"github.com/pkg/errors"
)

// Transform is triggered when a value has changed. It will replace the value with the value returned
// in the transform. If an error is returned it will not update the parameter in the local store.
type Transform func(key string, value interface{}) (interface{}, error)

// OnChangeHook is triggered when a value has changed.
type OnChangeHook func(value interface{})

// Source is the backing store of the dynamic config.
type Source interface {
	// FetchValues returns a key value map given a list of keys.
	FetchValues(ctx context.Context) (map[string]interface{}, error)
}

// DynamicConfig is a hot reloadable config with pluggable sources and reloading options.
type DynamicConfig struct {
	// store is a key value store of the parameters
	store   map[string]interface{}
	storeMu *sync.RWMutex

	// transforms are called when a parameter has changed before updating it in the store
	transforms map[string]Transform

	// onChangeHooks are called when a field they are registered to is modified
	onChangeHooks   map[string][]OnChangeHook
	onChangeHooksMu *sync.RWMutex

	// source is the backing store of
	source Source
}

// NewDynamicConfig creates a new instance of DynamicConfig given a source and keys to fetch.
// The context passed in is used when initially fetching the values from the source.
// If the intial reload fails, a working DynamicConfig object will be returned, but with all values
// set to nil.
func NewDynamicConfig(ctx context.Context, source Source, transforms map[string]Transform) (*DynamicConfig, error) {
	config := &DynamicConfig{
		storeMu:         &sync.RWMutex{},
		onChangeHooks:   make(map[string][]OnChangeHook),
		onChangeHooksMu: &sync.RWMutex{},
		source:          source,
		transforms:      transforms,
	}
	err := config.Reload(ctx)
	if err != nil {
		return config, errors.Wrap(err, "initializing config values")
	}
	return config, nil
}

// OnChange registers on change event for a given key
func (d *DynamicConfig) OnChange(key string, hook OnChangeHook) {
	d.onChangeHooksMu.Lock()
	defer d.onChangeHooksMu.Unlock()

	if hooks, ok := d.onChangeHooks[key]; ok {
		d.onChangeHooks[key] = append(hooks, hook)
	} else {
		d.onChangeHooks[key] = []OnChangeHook{hook}
	}
}

// Get retreives the value of a given key
func (d *DynamicConfig) Get(key string) interface{} {
	d.storeMu.RLock()
	defer d.storeMu.RUnlock()
	return d.store[key]
}

// Reload will fetch all values for the given keys from SSM, update the store, and then trigger onChangeHooks.
func (d *DynamicConfig) Reload(ctx context.Context) error {
	// Fetch new store from source
	newStore, err := d.source.FetchValues(ctx)
	if err != nil {
		return err
	}

	// Copy old store to use for validation
	d.storeMu.RLock()
	oldStore := make(map[string]interface{}, len(d.store))
	for k, v := range d.store {
		oldStore[k] = v
	}
	d.storeMu.RUnlock()

	transformedStore := d.transformStore(oldStore, newStore)

	// Switch pointer of store to new store
	d.storeMu.Lock()
	d.store = transformedStore
	d.storeMu.Unlock()

	// Trigger on change hooks
	d.triggerOnChangeHooks(oldStore, transformedStore)
	return nil
}

func (d *DynamicConfig) changedKeys(oldStore, newStore map[string]interface{}) []string {
	changedKeys := make([]string, 0, len(oldStore))

	keys := make(map[string]struct{}, len(oldStore))
	for k := range oldStore {
		keys[k] = struct{}{}
	}
	for k := range newStore {
		keys[k] = struct{}{}
	}

	for k := range keys {
		// Use reflect.DeepEqual to avoid the folowing error: panic: runtime error: comparing uncomparable type []string
		if !reflect.DeepEqual(oldStore[k], newStore[k]) {
			changedKeys = append(changedKeys, k)
		}
	}

	return changedKeys
}

func (d *DynamicConfig) transformStore(oldStore, newStore map[string]interface{}) map[string]interface{} {
	validatedStore := make(map[string]interface{}, len(newStore))
	for k, v := range newStore {
		validatedStore[k] = v
	}

	changedKeys := d.changedKeys(oldStore, newStore)

	for _, k := range changedKeys {
		if transform, ok := d.transforms[k]; ok {
			transformedValue, err := transform(k, newStore[k])
			if err != nil {
				validatedStore[k] = oldStore[k]
			} else {
				validatedStore[k] = transformedValue
			}
		}
	}

	return validatedStore
}

func (d *DynamicConfig) triggerOnChangeHooks(oldStore, newStore map[string]interface{}) {
	changedKeys := d.changedKeys(oldStore, newStore)

	d.onChangeHooksMu.RLock()
	defer d.onChangeHooksMu.RUnlock()

	for _, k := range changedKeys {
		if hooks, ok := d.onChangeHooks[k]; ok {
			for _, hook := range hooks {
				hook(newStore[k])
			}
		}
	}
}
