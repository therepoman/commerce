package dynamicconfig

import (
	"context"
	"io/ioutil"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/hashicorp/go-multierror"
	"github.com/pkg/errors"
)

// S3MultiFileSource sources values as files from filepaths as keys from S3 storage.
// Note: The output value for each key is []byte so you can decode/unmarshal to your expected format.
type S3MultiFileSource struct {
	s3     *s3.S3
	bucket *string
	keys   []*string
}

// NewS3MultiFileSource returns a new NewS3MultiFileSource given aws region, endpoint, bucket, and keys.
// Note that keys are filenames in s3.
func NewS3MultiFileSource(region, endpoint, bucket string, keys []string) (*S3MultiFileSource, error) {
	awsConfig := &aws.Config{
		Region:           aws.String(region),
		Endpoint:         aws.String(endpoint),
		S3ForcePathStyle: aws.Bool(true),
	}
	awsSession := session.New()
	wrappedKeys := make([]*string, 0, len(keys))
	for _, key := range keys {
		wrappedKeys = append(wrappedKeys, aws.String(key))
	}
	newSource := &S3MultiFileSource{
		s3:     s3.New(awsSession, awsConfig),
		bucket: aws.String(bucket),
		keys:   wrappedKeys,
	}

	bucketExists, err := newSource.bucketExists()
	if err != nil {
		return nil, err
	} else if !bucketExists {
		return nil, errors.New("given bucket does not exist")
	}

	return newSource, nil
}

// FetchValues returns key -> value map for each of the key found in the source
func (s *S3MultiFileSource) FetchValues(ctx context.Context) (map[string]interface{}, error) {
	valueMap := make(map[string]interface{}, len(s.keys))
	var multiErr error
	for _, key := range s.keys {
		output, err := s.loadFileForKey(key)
		if err != nil {
			multiErr = multierror.Append(multiErr, err)
		} else {
			valueMap[*key] = output
		}
	}
	return valueMap, multiErr
}

// loadFileForKey attempts to load the file located at the source bucket with a given key
func (s *S3MultiFileSource) loadFileForKey(key *string) ([]byte, error) {
	input := &s3.GetObjectInput{
		Bucket: s.bucket,
		Key:    key,
	}
	out, err := s.s3.GetObject(input)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to get object from s3 for key %s", *key)
	}

	defer out.Body.Close()
	bytes, err := ioutil.ReadAll(out.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to read output for key %s", *key)
	}

	return bytes, nil
}

// bucketExists checks if the source's bucket actually exists
func (s *S3MultiFileSource) bucketExists() (bool, error) {
	out, err := s.s3.ListBuckets(&s3.ListBucketsInput{})
	if err != nil {
		return false, errors.Wrap(err, "failed to list buckets")
	}
	for _, bucket := range out.Buckets {
		if *bucket.Name == *s.bucket {
			return true, nil
		}
	}
	return false, nil
}
