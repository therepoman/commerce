package dynamicconfig

import (
	"context"

	"github.com/aws/aws-sdk-go/service/ssm"
	"github.com/pkg/errors"
)

// SSMSource sources values from SSM Parameter Store.
type SSMSource struct {
	// ssmClient is used to communicate with AWS SSM
	ssmClient *ssm.SSM

	// keys to fetch from SSM
	keys []string
}

// NewSSMSource creates a source for DynamicConfig backed by SSM.
func NewSSMSource(keys []string, ssmClient *ssm.SSM) *SSMSource {
	return &SSMSource{
		ssmClient: ssmClient,
		keys:      keys,
	}
}

// FetchValues from SSM Parameter Store and return a key->value map.
func (s *SSMSource) FetchValues(ctx context.Context) (map[string]interface{}, error) {
	// Short circuit if there are no keys. Necessary since GetParameters errors out if an empty
	// list of keys is passed in.
	if len(s.keys) == 0 {
		return make(map[string]interface{}), nil
	}

	// fetch params from SSM
	ssmKeys := make([]*string, 0, len(s.keys))
	for _, key := range s.keys {
		key := key // Since we are taking the address of key, we must init new variable so we don't use the same pointer
		ssmKeys = append(ssmKeys, &key)
	}
	ssmResp, err := s.ssmClient.GetParametersWithContext(ctx, &ssm.GetParametersInput{
		Names: ssmKeys,
	})
	if err != nil {
		return nil, errors.Wrap(err, "getting parameters from SSM")
	}

	newStore := make(map[string]interface{}, len(ssmResp.Parameters))
	for _, param := range ssmResp.Parameters {
		if param.Value != nil {
			newStore[*param.Name] = *param.Value
		}
	}

	return newStore, nil
}
