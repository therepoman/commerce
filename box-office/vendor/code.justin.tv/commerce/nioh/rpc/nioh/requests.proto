syntax = "proto3";
package nioh;
option go_package = "nioh";

import "models.proto";

message GetRecordRequest{
	string channel_id = 1;
	string record_id = 2;
}

message GetUserAuthorizationRequest {
	string user_id = 1;
	oneof resource {
		Channel channel = 2;
		VOD vod = 3;
	}
	bool consume_preview = 4;

	// Optional origin parameter used generate free preview tokens
	string origin = 5;
}

message GetUserAuthorizationsByResourcesRequest {
	string user_id = 1;
	// Channels and VODs have very different data (channels just have IDs where vods have reason data, etc)
	// It's more convenient to keep them separate especially when upstreams typically care only about one type
	repeated Channel channels = 2;
	repeated VOD vods = 3;
}

message ListAuthorizedUsersRequest {
	uint32 limit = 1;
	uint32 offset = 2;
	oneof resource {
		Channel channel = 3;
		VOD vod = 4;
	}
}

message ListAuthorizedUsersRequestV2 {
	int32 limit = 1;
	string cursor = 2;
	oneof resource {
		Channel channel = 3;
		VOD vod = 4;
	}
}

message GetChanletsRequest{
	string channel_id = 1;
	ChanletSort sort = 2;
	string user_id = 3; // Optional (bypass_disable_flag has no effect if user_id is not the owner)
	bool bypass_disable_flag = 4; // Makes the chanlets return even when owner disabled it (for management)
}

message ListAllChanletsRequest{
	string cursor = 1;
}

message IsHiddenChanletRequest{
	string channel_id = 1;
}

message BulkIsHiddenChanletRequest{
	repeated string channel_ids = 1;
}

message CreateChanletRequest{
	string channel_id = 1;
}

message ArchiveChanletRequest{
	string user_id = 1;
	string chanlet_id = 2;
}

message UpdateChanletContentAttributesRequest {
	// identifier of the chanlet to be updated
	string owner_channel_id = 1;
	string chanlet_id = 2;

	// the list of new content attribute IDs that are to override the existing ones stored on the chanlet
	repeated string content_attribute_ids = 3;
}

message CreateContentAttributesRequest {
	// the channel ID that initiated the request
	// the channel ID that will be used to lookup the owner channel ID
	string channel_id = 1; // required

	// list of new content attributes to be created
	// owner_channel_id and id on them should be ignored
	repeated ContentAttribute content_attributes = 2;
}

message GetContentAttributesForChannelRequest {
	string channel_id = 1; // required
}

message GetContentAttributesForChanletRequest {
	string chanlet_id = 1; // required
}

message UpdateContentAttributesRequest {
	// the channel ID that initiated the request
	// the channel ID that will be used to lookup the owner channel ID
	string channel_id = 1; // required

	// content attributes to be updated
	repeated ContentAttribute content_attributes = 2;
}

message DeleteContentAttributesRequest {
	// the channel ID that initiated the request
	// the channel ID that will be used to lookup the owner channel ID
	string channel_id = 1; // required

	// IDs of content attributes to be deleted
	repeated ContentAttribute content_attributes = 2;
}

message CreateContentAttributeImageUploadConfigRequest {
	// the channel ID requesting the new upload
	string channel_id = 1;
}

message CreateChannelRelationRequest {
	string channel_id = 1;
	string related_channel_id = 2;
}

message DeleteChannelRelationRequest {
	string channel_id = 1;
	string related_channel_id = 2;
}

message GetChannelRelationsRequest {
	oneof id {
		string channel_id = 1;
		string related_channel_id = 2;
	}
}

message GetChanletStreamKeyRequest {
	string owner_channel_id = 1;
	string chanlet_id = 2;
}

message GetChannelRestrictionRequest {
	string channel_id = 1;
}

message GetChannelRestrictionsRequest {
	repeated string channel_ids = 1;
}

message GetRestrictionsByResourcesRequest {
	repeated RestrictionResource resources = 1;
	bool is_playback = 2;
}

message SetResourceRestrictionRequest {
	string user_id = 1;  // User who is trying to set restriction
	string owner_id = 2; // Owner of the resource being set
	RestrictionResource resource = 3;
	Restriction restriction = 4;
}

message DeleteResourceRestrictionRequest {
	string user_id = 1;
	string owner_id = 2;
	RestrictionResource resource = 3;
}

message GetPermissionsRequest {
	string user_id = 1;
	string channel_id = 2;
}

message GetOwnerChanletAttributesRequest {
	string user_id = 1;
	string owner_chanlet_id = 2; // This is just the channel id that has chanlets
}

message UpdateOwnerChanletAttributesRequest {
	string user_id = 1;
	string owner_chanlet_id = 2; // This is just the channel id that has chanlets
	OwnerChanletAttributes attributes = 3;
}

// GeoblockRequest represents a geoblock request we receive from Aegis
message GeoblockRequest {
  string action = 1;
  string content_id = 2;
  string content_owner_id = 3;
  string content_type = 4;
  repeated string country_codes = 5;
  string requester_id = 6;
}
