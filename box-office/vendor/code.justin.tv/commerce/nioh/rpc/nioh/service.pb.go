// Code generated by protoc-gen-go. DO NOT EDIT.
// source: service.proto

package nioh

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

func init() { proto.RegisterFile("service.proto", fileDescriptor3) }

var fileDescriptor3 = []byte{
	// 720 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xac, 0x57, 0x6d, 0x6f, 0xd3, 0x30,
	0x10, 0xd6, 0x04, 0x9a, 0x84, 0x61, 0x9b, 0x64, 0x60, 0x6c, 0xd9, 0x40, 0x7b, 0x63, 0xe3, 0x65,
	0x54, 0xa2, 0xc0, 0x0f, 0x68, 0x8b, 0xc8, 0xa6, 0x31, 0x98, 0x3a, 0xb5, 0x1f, 0x26, 0x21, 0x91,
	0x25, 0xb7, 0x35, 0x22, 0x8d, 0x8b, 0xed, 0x0c, 0x0d, 0x3e, 0x21, 0xfe, 0x03, 0xbf, 0x17, 0x35,
	0xb6, 0x63, 0xd7, 0x75, 0x92, 0x22, 0xf8, 0x7a, 0xcf, 0x73, 0xcf, 0x73, 0xbe, 0x4b, 0xce, 0x09,
	0x5a, 0x60, 0x40, 0xaf, 0xe2, 0x10, 0x1a, 0x23, 0x4a, 0x38, 0xc1, 0x37, 0xd3, 0x98, 0x0c, 0xbc,
	0x45, 0x0a, 0x5f, 0x33, 0x60, 0x9c, 0x89, 0xa8, 0xb7, 0x44, 0x81, 0x8d, 0x48, 0xca, 0x40, 0x06,
	0x9a, 0xbf, 0x56, 0xd0, 0x8d, 0xd6, 0xc9, 0x21, 0x7e, 0x89, 0x6e, 0xf9, 0xc0, 0xbb, 0x10, 0x12,
	0x1a, 0xe1, 0xe5, 0xc6, 0x38, 0xb9, 0x51, 0x04, 0xba, 0x42, 0xc4, 0xbb, 0x23, 0xe2, 0x92, 0xf5,
	0x09, 0xdd, 0xf3, 0x81, 0xf7, 0x18, 0xd0, 0x56, 0xc6, 0x07, 0x84, 0xc6, 0xdf, 0x03, 0x1e, 0x93,
	0x14, 0x6f, 0x16, 0xd9, 0x53, 0x98, 0x12, 0xda, 0xaa, 0xa2, 0x88, 0x02, 0xf1, 0xcf, 0x39, 0xb4,
	0xe1, 0x22, 0xb0, 0xf6, 0x75, 0x17, 0x18, 0xc9, 0x68, 0x08, 0x0c, 0xbf, 0x28, 0x17, 0x32, 0x79,
	0xca, 0xb7, 0x31, 0x2b, 0x5d, 0xd6, 0x70, 0x86, 0xee, 0xbe, 0x8f, 0x19, 0x57, 0x44, 0x88, 0xc6,
	0x69, 0x0c, 0x6f, 0x08, 0x19, 0x07, 0xa4, 0x8c, 0x36, 0x2b, 0x18, 0x52, 0xfb, 0x33, 0xba, 0xef,
	0x80, 0xfb, 0x4d, 0xbc, 0x55, 0xa7, 0xde, 0x6f, 0x7a, 0xdb, 0xb5, 0xfa, 0xfd, 0x26, 0x6e, 0xa3,
	0xdb, 0x3e, 0xf0, 0xce, 0x20, 0x48, 0x13, 0xe0, 0x0c, 0xaf, 0x14, 0x87, 0x57, 0x21, 0x55, 0xed,
	0xaa, 0x03, 0x91, 0x55, 0x7e, 0x40, 0x4b, 0xb9, 0x49, 0x92, 0x14, 0x3a, 0xeb, 0x86, 0xb7, 0x0e,
	0x2b, 0xad, 0x87, 0x25, 0xa8, 0xd6, 0x3b, 0x64, 0x07, 0x71, 0x14, 0x41, 0x2a, 0x31, 0xa5, 0x67,
	0x85, 0x2d, 0xbd, 0x29, 0x54, 0x4f, 0xa8, 0x9d, 0x25, 0x5f, 0x6c, 0x4d, 0x39, 0x21, 0x07, 0x64,
	0x4d, 0xc8, 0xc9, 0x90, 0xda, 0x07, 0x68, 0xa1, 0x43, 0x21, 0xe0, 0xa0, 0x54, 0x3d, 0x91, 0x33,
	0x11, 0x54, 0x7a, 0x6b, 0x4e, 0x4c, 0x2a, 0x1d, 0xa1, 0xc5, 0x16, 0x0d, 0x07, 0xf1, 0x55, 0x21,
	0x25, 0xe9, 0x93, 0x51, 0xa5, 0xb5, 0xee, 0x06, 0xa5, 0xd8, 0x31, 0xc2, 0xc2, 0xe5, 0xe3, 0xb7,
	0x14, 0xe8, 0x3f, 0xd7, 0xf6, 0x03, 0x3d, 0xea, 0x8d, 0x22, 0x0d, 0x74, 0x48, 0xca, 0x21, 0xe5,
	0x2d, 0xce, 0x69, 0x7c, 0x9e, 0x71, 0x60, 0xf8, 0xb9, 0x48, 0xaf, 0x66, 0x29, 0xaf, 0xfd, 0xd9,
	0xc8, 0xd2, 0xfc, 0x02, 0x3d, 0x90, 0x55, 0x4d, 0xb9, 0xee, 0x4c, 0x14, 0x5d, 0x66, 0xf7, 0xb8,
	0x86, 0xa5, 0x0f, 0xe9, 0x3b, 0xea, 0x78, 0x47, 0xf2, 0xfe, 0xa5, 0x90, 0xa8, 0x43, 0x56, 0xb3,
	0xac, 0x43, 0xd6, 0x91, 0x67, 0x32, 0x1f, 0x0f, 0xaf, 0xde, 0xdc, 0x98, 0xe6, 0xfe, 0x6c, 0x64,
	0xdd, 0x61, 0x39, 0x8b, 0xb2, 0x0e, 0x97, 0xc0, 0x56, 0x87, 0x4b, 0x59, 0xda, 0xe7, 0x2d, 0x24,
	0x50, 0xe1, 0x53, 0x02, 0x5b, 0x3e, 0xa5, 0x2c, 0xe9, 0xf3, 0x7b, 0x0e, 0xed, 0xb9, 0xa7, 0x7d,
	0x38, 0x0c, 0x2e, 0xa1, 0x37, 0x4a, 0x48, 0x10, 0x75, 0x48, 0x7a, 0x11, 0x5f, 0xe2, 0xd7, 0x55,
	0x0f, 0xc7, 0x14, 0x5d, 0x15, 0xf2, 0xe6, 0x2f, 0xb3, 0xf4, 0x3e, 0xd7, 0x2f, 0x58, 0x3e, 0xfe,
	0x44, 0xdc, 0x87, 0x5b, 0xf6, 0xdb, 0x67, 0x80, 0xca, 0x73, 0xbb, 0x92, 0xa3, 0x1d, 0x64, 0x77,
	0xdc, 0x0e, 0x4e, 0xd0, 0x72, 0x28, 0xe1, 0xe8, 0x6d, 0x2a, 0x2f, 0x01, 0x03, 0x2d, 0xee, 0x3b,
	0x07, 0x64, 0x6d, 0x53, 0x27, 0x63, 0x4a, 0x3b, 0x01, 0x7e, 0xca, 0x29, 0x04, 0xc3, 0x23, 0xb8,
	0xb6, 0xb4, 0x4d, 0xc8, 0xad, 0x3d, 0xc9, 0xd0, 0x9d, 0x31, 0xad, 0x19, 0xa7, 0x71, 0x68, 0x76,
	0xc6, 0x09, 0x5a, 0x9d, 0x29, 0xe1, 0x48, 0x87, 0x10, 0x2d, 0x3b, 0x09, 0x0c, 0x57, 0xa5, 0x17,
	0xfd, 0xd9, 0xa9, 0x26, 0x49, 0x13, 0x82, 0xbc, 0xfc, 0x9b, 0x4b, 0x43, 0xe6, 0xb7, 0xce, 0x9e,
	0xf1, 0x55, 0xe6, 0x64, 0x28, 0xb3, 0x27, 0xf5, 0xc4, 0xe2, 0xa5, 0x5d, 0x3e, 0xcd, 0x59, 0x79,
	0xdc, 0x6c, 0x9c, 0x3c, 0x95, 0x1b, 0x55, 0x46, 0xb2, 0xa2, 0xe3, 0x8c, 0x07, 0x1c, 0x9c, 0x3c,
	0xe9, 0x93, 0xa0, 0x55, 0xf1, 0xe0, 0xb9, 0xac, 0x76, 0xcd, 0x27, 0xf3, 0x7f, 0xb8, 0x1d, 0xa1,
	0x45, 0x1f, 0xf8, 0x09, 0xd0, 0x61, 0xcc, 0x58, 0x3e, 0xa3, 0xb5, 0xa2, 0x23, 0x46, 0xd4, 0xba,
	0x6d, 0x6d, 0x50, 0x97, 0xee, 0x03, 0x37, 0xaf, 0x5a, 0x63, 0xb3, 0xed, 0x16, 0xa9, 0x6e, 0x82,
	0x55, 0x7a, 0x05, 0x4f, 0xba, 0x65, 0x68, 0x5d, 0x2c, 0xda, 0x12, 0xc3, 0xa7, 0xe6, 0x32, 0xae,
	0xf6, 0x7c, 0x36, 0x0b, 0x55, 0xd8, 0xb6, 0xe7, 0xcf, 0xf2, 0xdf, 0x85, 0xf3, 0xf9, 0xfc, 0xa7,
	0xe0, 0xd5, 0x9f, 0x00, 0x00, 0x00, 0xff, 0xff, 0x2d, 0x38, 0x2e, 0xfc, 0x4c, 0x0c, 0x00, 0x00,
}
