syntax = "proto3";
// need separate proto package to namespace variant members
package nioh.restriction_option;
option go_package = "nioh";

// Option represents a _modulation_ of a restriction type's built-in access policy. They allow clients limited control
// over the Exemptions that a restriction will include upon creation. After restriction creation, the Options are
// stored and surfaced to clients so that the original intent of the restriction can be interpreted as necessary.
enum Option {
    // UNKNOWN is the catch-all default when parsing malformed or incompatible Option values.
    // It should *never* appear in business logic, as it stands for an unrepresentable and uninterpretable value.
    UNKNOWN = 0;
    // ALLOW_CHANNEL_VIP is an option indicating the restriction should allow access to channel VIPs.
    // ALLOW_CHANNEL_VIP is currently only valid for SUB_ONLY_LIVE restrictions.
    ALLOW_CHANNEL_VIP = 1;
    // ALLOW_CHANNEL_MODERATOR is an option indicating the restriction should allow access to channel moderators.
    // ALLOW_CHANNEL_MODERATOR is currently only valid for SUB_ONLY_LIVE restrictions.
    ALLOW_CHANNEL_MODERATOR = 2;

    ///////////////////////////
    // PRODUCT SPECIFIC OPTIONS
    ///////////////////////////
    // Only one of the following three should be set

    // ALLOW_TIER_3_ONLY option indicates that the restriction is exempt only for the tier 3 subscriptions
    ALLOW_TIER_3_ONLY = 3;
    // ALLOW_TIER_2_AND_3_ONLY option indicates that the restriction is exempt only for the tier 2 and tier 3 subscriptions
    ALLOW_TIER_2_AND_3_ONLY = 4;
    // ALLOW_ALL_TIERS option indicates that the restriction is exempt for any subscription tiers
    ALLOW_ALL_TIERS = 5;
}
