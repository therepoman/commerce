// Code generated by protoc-gen-go. DO NOT EDIT.
// source: restriction_option.proto

package nioh

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// Option represents a _modulation_ of a restriction type's built-in access policy. They allow clients limited control
// over the Exemptions that a restriction will include upon creation. After restriction creation, the Options are
// stored and surfaced to clients so that the original intent of the restriction can be interpreted as necessary.
type Option int32

const (
	// UNKNOWN is the catch-all default when parsing malformed or incompatible Option values.
	// It should *never* appear in business logic, as it stands for an unrepresentable and uninterpretable value.
	Option_UNKNOWN Option = 0
	// ALLOW_CHANNEL_VIP is an option indicating the restriction should allow access to channel VIPs.
	// ALLOW_CHANNEL_VIP is currently only valid for SUB_ONLY_LIVE restrictions.
	Option_ALLOW_CHANNEL_VIP Option = 1
	// ALLOW_CHANNEL_MODERATOR is an option indicating the restriction should allow access to channel moderators.
	// ALLOW_CHANNEL_MODERATOR is currently only valid for SUB_ONLY_LIVE restrictions.
	Option_ALLOW_CHANNEL_MODERATOR Option = 2
	// ALLOW_TIER_3_ONLY option indicates that the restriction is exempt only for the tier 3 subscriptions
	Option_ALLOW_TIER_3_ONLY Option = 3
	// ALLOW_TIER_2_AND_3_ONLY option indicates that the restriction is exempt only for the tier 2 and tier 3 subscriptions
	Option_ALLOW_TIER_2_AND_3_ONLY Option = 4
	// ALLOW_ALL_TIERS option indicates that the restriction is exempt for any subscription tiers
	Option_ALLOW_ALL_TIERS Option = 5
)

var Option_name = map[int32]string{
	0: "UNKNOWN",
	1: "ALLOW_CHANNEL_VIP",
	2: "ALLOW_CHANNEL_MODERATOR",
	3: "ALLOW_TIER_3_ONLY",
	4: "ALLOW_TIER_2_AND_3_ONLY",
	5: "ALLOW_ALL_TIERS",
}
var Option_value = map[string]int32{
	"UNKNOWN":                 0,
	"ALLOW_CHANNEL_VIP":       1,
	"ALLOW_CHANNEL_MODERATOR": 2,
	"ALLOW_TIER_3_ONLY":       3,
	"ALLOW_TIER_2_AND_3_ONLY": 4,
	"ALLOW_ALL_TIERS":         5,
}

func (x Option) String() string {
	return proto.EnumName(Option_name, int32(x))
}
func (Option) EnumDescriptor() ([]byte, []int) { return fileDescriptor4, []int{0} }

func init() {
	proto.RegisterEnum("nioh.restriction_option.Option", Option_name, Option_value)
}

func init() { proto.RegisterFile("restriction_option.proto", fileDescriptor4) }

var fileDescriptor4 = []byte{
	// 170 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x92, 0x28, 0x4a, 0x2d, 0x2e,
	0x29, 0xca, 0x4c, 0x2e, 0xc9, 0xcc, 0xcf, 0x8b, 0xcf, 0x2f, 0x00, 0x51, 0x7a, 0x05, 0x45, 0xf9,
	0x25, 0xf9, 0x42, 0xe2, 0x79, 0x99, 0xf9, 0x19, 0x7a, 0x98, 0xd2, 0x5a, 0x93, 0x18, 0xb9, 0xd8,
	0xfc, 0xc1, 0x4c, 0x21, 0x6e, 0x2e, 0xf6, 0x50, 0x3f, 0x6f, 0x3f, 0xff, 0x70, 0x3f, 0x01, 0x06,
	0x21, 0x51, 0x2e, 0x41, 0x47, 0x1f, 0x1f, 0xff, 0xf0, 0x78, 0x67, 0x0f, 0x47, 0x3f, 0x3f, 0x57,
	0x9f, 0xf8, 0x30, 0xcf, 0x00, 0x01, 0x46, 0x21, 0x69, 0x2e, 0x71, 0x54, 0x61, 0x5f, 0x7f, 0x17,
	0xd7, 0x20, 0xc7, 0x10, 0xff, 0x20, 0x01, 0x26, 0x84, 0x9e, 0x10, 0x4f, 0xd7, 0xa0, 0x78, 0xe3,
	0x78, 0x7f, 0x3f, 0x9f, 0x48, 0x01, 0x66, 0x84, 0x1e, 0xb0, 0xb0, 0x51, 0xbc, 0xa3, 0x9f, 0x0b,
	0x4c, 0x92, 0x45, 0x48, 0x98, 0x8b, 0x1f, 0x22, 0xe9, 0xe8, 0xe3, 0x03, 0x56, 0x10, 0x2c, 0xc0,
	0xea, 0xc4, 0x16, 0xc5, 0x02, 0x72, 0x6f, 0x12, 0x1b, 0xd8, 0xf1, 0xc6, 0x80, 0x00, 0x00, 0x00,
	0xff, 0xff, 0x78, 0x1a, 0xe3, 0xb8, 0xd8, 0x00, 0x00, 0x00,
}
