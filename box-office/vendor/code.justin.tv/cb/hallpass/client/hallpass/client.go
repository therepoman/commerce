package hallpass

import (
	"context"

	"code.justin.tv/cb/hallpass/view"
	"code.justin.tv/foundation/twitchclient"
)

// Client is the client interface for use of permissions
type Client interface {
	GetV1Editors(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*view.GetEditorsResponse, error)
	GetV1IsEditor(ctx context.Context, channelID string, editorID string, reqOpts *twitchclient.ReqOpts) (*view.GetIsEditorResponse, error)
	CreateV1Editor(ctx context.Context, grantedForID string, params *view.CreateEditorRequest, reqOpts *twitchclient.ReqOpts) (*view.CreateEditorResponse, error)
	DeleteV1Editor(ctx context.Context, channelID string, editorID string, reqOpts *twitchclient.ReqOpts) (*view.DeleteEditorResponse, error)
	GetV1EditableChannels(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*view.GetEditableChannelsResponse, error)
}

type client struct {
	twitchclient.Client
}

// NewClient creates a new client
func NewClient(config twitchclient.ClientConf) (Client, error) {
	if config.TimingXactName == "" {
		config.TimingXactName = "cb-hallpass"
	}

	twitchClient, err := twitchclient.NewClient(config)
	if err != nil {
		return nil, err
	}

	return &client{
		Client: twitchClient,
	}, nil
}
