package substwirp

// These error codes are meant to be returned within a substwirp.ClientError type.

const (
	// Generic error codes
	ErrCodeInvalidUser         = "INVALID_USER"
	ErrCodeInvalidProductOwner = "INVALID_PRODUCT_OWNER"

	// CreateEmoticon
	ErrCodeInvalidEmoticonPrefix = "INVALID_PREFIX"
	ErrCodeEmoticonLimit         = "EMOTICON_LIMIT_REACHED"
	ErrCodeCodeNotUnique         = "CODE_NOT_UNIQUE"
	ErrCodeInvalidSuffixFormat   = "INVALID_SUFFIX_FORMAT"
	ErrCodeImage28IDNotFound     = "IMAGE28ID_NOT_FOUND"
	ErrCodeImage56IDNotFound     = "IMAGE56ID_NOT_FOUND"
	ErrCodeImage112IDNotFound    = "IMAGE112ID_NOT_FOUND"
)

type ClientError struct {
	ErrorCode string `json:"error_code"`
}
