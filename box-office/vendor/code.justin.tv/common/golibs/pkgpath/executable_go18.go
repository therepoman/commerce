// +build go1.8

package pkgpath

import "os"

func osExecutable() (string, error) {
	return os.Executable()
}
