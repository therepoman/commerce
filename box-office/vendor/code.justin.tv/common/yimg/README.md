```
  yaml
+  img
------
  yimg
```
# Usage
- Converts YAML into Image URLs
- Get Image metadata
- Converts Image metadata to YAML format strings 

[![GoDoc](http://godoc.internal.justin.tv/code.justin.tv/common/yimg?status.svg)](http://godoc.internal.justin.tv/code.justin.tv/common/yimg)
