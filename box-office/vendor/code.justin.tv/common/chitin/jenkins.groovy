
    job {
	name "common-chitin"
	using 'TEMPLATE-autobuild'
	scm {
            git {
		remote {
			github 'common/chitin', 'ssh', 'git-aws.internal.justin.tv'
			credentials 'git-aws-read-key'
		}
		clean true
	    }
	}
	steps {
shell 'manta -proxy -f _build/mantas/manta.go1.7.1.linux-amd64.json'
shell 'manta -proxy -f _build/mantas/manta.go1.8.3.linux-amd64.json'
shell 'manta -proxy -f _build/mantas/manta.go1.9beta1.linux-amd64.json'
	}
    }

    job {
	name 'common-chitin-deploy'
	using 'TEMPLATE-deploy'
	steps {
	}
    }
    