package channels

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"code.justin.tv/common/yimg"
	"code.justin.tv/foundation/twitchclient"
	util "code.justin.tv/web/users-service/client"
	"code.justin.tv/web/users-service/models"
)

const (
	defaultStatSampleRate = 0.1
	defaultTimingXactName = "channels"
	batchSize             = 100
)

type ErrChannelNotFound struct{}

func (e *ErrChannelNotFound) Error() string {
	return "Channel not found"
}

func (e *ErrChannelNotFound) StatusCode() int {
	return http.StatusNotFound
}

type ChannelsResult struct {
	Results        []Channel                 `json:"results"`
	BadIdentifiers []models.ErrBadIdentifier `json:"bad_identifiers"`
}

func (cr *ChannelsResult) ToMapByID() map[string]*Channel {
	channelResultMap := map[string]*Channel{}
	if cr == nil {
		return channelResultMap
	}

	for i := range cr.Results {
		channelResultMap[strconv.Itoa(cr.Results[i].ID)] = &cr.Results[i]
	}
	return channelResultMap
}

//go:generate retool do mockery -name Client
type Client interface {
	Get(ctx context.Context, id string, reqOpts *twitchclient.ReqOpts) (*Channel, error)
	GetByIDAndParams(ctx context.Context, id string, params *models.ChannelFilterParams, reqOpts *twitchclient.ReqOpts) (*Channel, error)
	GetAll(ctx context.Context, ids []string, reqOpts *twitchclient.ReqOpts) (*ChannelsResult, error)
	GetAllByParams(ctx context.Context, ids []string, params *models.ChannelFilterParams, reqOpts *twitchclient.ReqOpts) (*ChannelsResult, error)
	GetByLogin(ctx context.Context, login string, reqOpts *twitchclient.ReqOpts) (*Channel, error)
	GetByLoginAndParams(ctx context.Context, login string, params *models.ChannelFilterParams, reqOpts *twitchclient.ReqOpts) (*Channel, error)
	GetAllByLogin(ctx context.Context, logins []string, reqOpts *twitchclient.ReqOpts) (*ChannelsResult, error)
	GetAllByLoginAndParams(ctx context.Context, logins []string, params *models.ChannelFilterParams, reqOpts *twitchclient.ReqOpts) (*ChannelsResult, error)
	Set(ctx context.Context, up models.UpdateChannelProperties, reqOpts *twitchclient.ReqOpts) error
	SetChannelWithAuth(ctx context.Context, up models.UpdateChannelProperties, editor string, reqOpts *twitchclient.ReqOpts) error
}

type client struct {
	twitchclient.Client
}

type Channel struct {
	Broadcaster                  string      `json:"broadcaster"`
	BroadcasterLanguage          string      `json:"broadcaster_language"`
	BroadcasterSoftware          string      `json:"broadcaster_software"`
	CreatedOn                    time.Time   `json:"created_on"`
	DirectoryHidden              bool        `json:"directory_hidden"`
	DisableChat                  bool        `json:"disable_chat"`
	Game                         string      `json:"game"`
	GameID                       int         `json:"game_id"`
	ID                           int         `json:"id"`
	LastBroadcastID              string      `json:"last_broadcast_id"`
	LastBroadcastTime            string      `json:"last_broadcast_time"`
	LastLiveNotificationSent     string      `json:"last_live_notification_sent"`
	Mature                       bool        `json:"mature"`
	Name                         string      `json:"name"`
	OfflineImage                 yimg.Images `json:"channel_offline_image"`
	PrimaryTeamID                int         `json:"primary_team_id"`
	About                        string      `json:"about"`
	ProfileBanner                yimg.Images `json:"profile_banner"`
	ProfileBannerBackgroundColor string      `json:"profile_banner_background_color"`
	RedirectChannel              *string     `json:"redirect_channel"`
	Status                       string      `json:"status"`
	Title                        string      `json:"title"`
	UpdatedOn                    time.Time   `json:"updated_on"`
	ViewsCount                   int         `json:"views_count"`
	PrivateVideo                 bool        `json:"private_video"`
	PrivacyOptionsEnabled        bool        `json:"privacy_options_enabled"`
}

type batchResult struct {
	*ChannelsResult
	err error
}

func NewClient(conf twitchclient.ClientConf) (Client, error) {
	if conf.TimingXactName == "" {
		conf.TimingXactName = defaultTimingXactName
	}
	twitchClient, err := twitchclient.NewClient(conf)
	return &client{twitchClient}, err
}

func (c *client) Get(ctx context.Context, id string, reqOpts *twitchclient.ReqOpts) (*Channel, error) {
	return c.GetByIDAndParams(ctx, id, nil, reqOpts)
}

func (c *client) GetByIDAndParams(ctx context.Context, id string, params *models.ChannelFilterParams, reqOpts *twitchclient.ReqOpts) (*Channel, error) {
	result, err := c.get(ctx, "id", []string{id}, params, "service.channels.get", reqOpts)
	if err != nil {
		return nil, err
	}

	return firstResult(result)
}

func (c *client) GetAll(ctx context.Context, ids []string, reqOpts *twitchclient.ReqOpts) (*ChannelsResult, error) {
	return c.GetAllByParams(ctx, ids, nil, reqOpts)
}

func (c *client) GetAllByParams(ctx context.Context, ids []string, params *models.ChannelFilterParams, reqOpts *twitchclient.ReqOpts) (*ChannelsResult, error) {
	batchCount := (len(ids) / batchSize) + 1
	ch := make(chan *batchResult, batchCount)

	for i := 0; i < batchCount; i++ {
		batch := getBatchSlice(ids, i)
		subCtx, cancel := context.WithCancel(ctx)
		defer cancel()
		go c.getAsync(subCtx, "id", batch, params, "service.channels.get_all_batched", ch, reqOpts)
	}

	return combineAsync(ch, batchCount)
}

func (c *client) GetByLogin(ctx context.Context, login string, reqOpts *twitchclient.ReqOpts) (*Channel, error) {
	return c.GetByLoginAndParams(ctx, login, nil, reqOpts)
}

func firstResult(result *ChannelsResult) (*Channel, error) {
	if len(result.Results) == 0 {
		return nil, &ErrChannelNotFound{}
	}

	return &result.Results[0], nil
}

func (c *client) GetByLoginAndParams(ctx context.Context, login string, params *models.ChannelFilterParams, reqOpts *twitchclient.ReqOpts) (*Channel, error) {
	result, err := c.get(ctx, "name", []string{login}, params, "service.channels.get_by_login", reqOpts)
	if err != nil {
		return nil, err
	}
	return firstResult(result)
}

func (c *client) GetAllByLogin(ctx context.Context, logins []string, reqOpts *twitchclient.ReqOpts) (*ChannelsResult, error) {
	return c.GetAllByLoginAndParams(ctx, logins, nil, reqOpts)
}

func (c *client) GetAllByLoginAndParams(ctx context.Context, logins []string, params *models.ChannelFilterParams, reqOpts *twitchclient.ReqOpts) (*ChannelsResult, error) {
	batchCount := (len(logins) / batchSize) + 1
	ch := make(chan *batchResult, batchCount)

	for i := 0; i < batchCount; i++ {
		batch := getBatchSlice(logins, i)
		subCtx, cancel := context.WithCancel(ctx)
		defer cancel()
		go c.getAsync(subCtx, "name", batch, params, "service.channels.get_all_by_login_batched", ch, reqOpts)
	}

	return combineAsync(ch, batchCount)
}

func (c *client) Set(ctx context.Context, upProps models.UpdateChannelProperties, reqOpts *twitchclient.ReqOpts) error {
	bodyJson, err := json.Marshal(upProps)
	if err != nil {
		return err
	}

	body := bytes.NewBuffer(bodyJson)

	req, err := c.NewRequest("PATCH", "/channels", body)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.channels.update",
		StatSampleRate: defaultStatSampleRate,
	})

	_, err = c.DoJSON(ctx, nil, req, combinedReqOpts)
	return err
}

func (c *client) SetChannelWithAuth(ctx context.Context, up models.UpdateChannelProperties, editor string, reqOpts *twitchclient.ReqOpts) error {
	bodyJson, err := json.Marshal(up)
	if err != nil {
		return err
	}

	body := bytes.NewBuffer(bodyJson)

	req, err := c.NewRequest("PATCH", fmt.Sprintf("/channels/editor/%s", editor), body)
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.channels.update_with_auth",
		StatSampleRate: defaultStatSampleRate,
	})

	_, err = c.DoJSON(ctx, nil, req, combinedReqOpts)
	return err
}

func getBatchSlice(values []string, batchNum int) []string {
	batchStart := batchNum * batchSize
	batchEnd := (batchNum + 1) * batchSize

	if batchEnd > len(values) {
		return values[batchStart:]
	}
	return values[batchStart:batchEnd]
}

func (c *client) getAsync(ctx context.Context, key string, value []string, params *models.ChannelFilterParams, stat string, ch chan *batchResult, reqOpts *twitchclient.ReqOpts) {
	defer func() {
		r := recover()
		if r != nil {
			err := fmt.Errorf("panic in users service client: %v", r)
			ch <- &batchResult{err: err}
		}
	}()

	result, err := c.get(ctx, key, value, params, stat, reqOpts)
	ch <- &batchResult{ChannelsResult: result, err: err}
}

func (c *client) get(ctx context.Context, key string, value []string, params *models.ChannelFilterParams, stat string, reqOpts *twitchclient.ReqOpts) (*ChannelsResult, error) {
	if len(value) == 0 {
		return nil, nil
	}

	query := url.Values{
		key: value,
	}
	modifyQuery(&query, params)
	req, err := c.NewRequest("GET", "/channels?"+query.Encode(), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       stat,
		StatSampleRate: defaultStatSampleRate,
	})

	httpResp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer func() {
		err = httpResp.Body.Close()
	}()

	switch httpResp.StatusCode {
	case http.StatusOK:
		var decoded ChannelsResult
		if err := json.NewDecoder(httpResp.Body).Decode(&decoded); err != nil {
			return nil, err
		}
		return &decoded, nil

	default:
		// Unexpected result
		return nil, util.HandleUnexpectedResult(httpResp)
	}
}

func combineAsync(ch chan *batchResult, batchCount int) (*ChannelsResult, error) {
	var r ChannelsResult
	for i := 0; i < batchCount; i++ {
		result := <-ch
		if result == nil {
			continue
		}
		if result.err != nil {
			return nil, result.err
		}
		if result.ChannelsResult != nil {
			r.Results = append(r.Results, result.Results...)
			r.BadIdentifiers = append(r.BadIdentifiers, result.BadIdentifiers...)
		}
	}
	return &r, nil
}

func modifyQuery(query *url.Values, params *models.ChannelFilterParams) {
	if params == nil {
		return
	}

	if params.NotDeleted {
		query.Add(models.NotDeletedParam, "true")
	}

	if params.NoTOSViolation {
		query.Add(models.NoTOSViolationParam, "true")
	}

	if params.NoDMCAViolation {
		query.Add(models.NoDMCAViolationParam, "true")
	}

	if params.InlineIdentifierValidation {
		query.Add(models.InlineIdentifierValidation, "true")
	}
}
