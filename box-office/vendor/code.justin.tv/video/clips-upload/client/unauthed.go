package clips

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"code.justin.tv/foundation/twitchclient"
	"golang.org/x/net/context"
)

type UnauthedBatchDeleteClipsV3Params struct {
	Slugs []string `json:"slugs"`
}

// UnauthedBatchDeleteClipsV3 is the unauthed version of BatchDeleteClipsV3.
// This function is intended for internal use only, e.g. for DMCA'd clips.
func (c *client) UnauthedBatchDeleteClipsV3(ctx context.Context, slugs []string, opts *twitchclient.ReqOpts) (DeletedClips, error) {
	path := "/api/v3/clips"

	params := UnauthedBatchDeleteClipsV3Params{
		Slugs: slugs,
	}
	body, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("DELETE", path, bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.unauthed_batch_delete_clips_v3",
		StatSampleRate: defaultStatSampleRate,
	})

	deletedClips := DeletedClips{}
	resp, err := c.DoJSON(ctx, &deletedClips, req, combinedOpts)
	if err != nil {
		if resp != nil && resp.StatusCode == http.StatusNotFound {
			return nil, &twitchclient.Error{StatusCode: http.StatusNotFound, Message: "no clips were found"}
		}
		return nil, err
	}
	return deletedClips, nil
}

// UnauthedDeleteClipsByVODV3 is the unauthed version of DeleteClipsByVODV3.
// This function is intended for internal use only, e.g. for clips on a DMCA'd VODs.
func (c *client) UnauthedDeleteClipsByVODV3(ctx context.Context, vodID string, opts *twitchclient.ReqOpts) (DeletedClips, error) {
	path := fmt.Sprintf("/api/v3/vods/%s/clips", vodID)

	req, err := c.NewRequest("DELETE", path, nil)
	if err != nil {
		return nil, err
	}

	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.unauthed_delete_clips_by_vod_v3",
		StatSampleRate: defaultStatSampleRate,
	})

	deletedClips := DeletedClips{}
	resp, err := c.DoJSON(ctx, &deletedClips, req, combinedOpts)
	if err != nil {
		if resp != nil && resp.StatusCode == http.StatusNotFound {
			return nil, &twitchclient.Error{StatusCode: http.StatusNotFound, Message: fmt.Sprintf("no clips were found for vod %s", vodID)}
		}
		return nil, err
	}
	return deletedClips, nil
}

// UnauthedDeleteClipsByBroadcastV3 is the unauthed version of DeleteClipsByBroadcastV3.
// This function is intended for internal use only, e.g. for clips on a DMCA'd broadcast.
func (c *client) UnauthedDeleteClipsByBroadcastV3(ctx context.Context, broadcastID string, opts *twitchclient.ReqOpts) (DeletedClips, error) {
	path := fmt.Sprintf("/api/v3/broadcasts/%s/clips", broadcastID)

	req, err := c.NewRequest("DELETE", path, nil)
	if err != nil {
		return nil, err
	}

	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.unauthed_delete_clips_by_broadcast_v3",
		StatSampleRate: defaultStatSampleRate,
	})

	deletedClips := DeletedClips{}
	resp, err := c.DoJSON(ctx, &deletedClips, req, combinedOpts)
	if err != nil {
		if resp != nil && resp.StatusCode == http.StatusNotFound {
			return nil, &twitchclient.Error{StatusCode: http.StatusNotFound, Message: fmt.Sprintf("no clips were found for broadcast %s", broadcastID)}
		}
		return nil, err
	}
	return deletedClips, nil
}

// DeleteClipMediaItem contains the slugs and media keys for deletion
type DeleteClipMediaItem struct {
	Slug     string `json:"slug"`
	MediaKey string `json:"media_key"`
}

// DeleteClipsMediaV3 deletes clip media from s3 and invalidates CDN
// this is an internal API and should not be exposed publicly
func (c *client) UnauthedDeleteClipsMediaV3(ctx context.Context, items []DeleteClipMediaItem, opts *twitchclient.ReqOpts) error {
	body, err := json.Marshal(struct {
		Items []DeleteClipMediaItem `json:"items"`
	}{items})
	if err != nil {
		return err
	}

	req, err := c.NewRequest("DELETE", "/api/v3/clips_media", bytes.NewBuffer(body))
	if err != nil {
		return err
	}

	combinedOpts := twitchclient.MergeReqOpts(opts, twitchclient.ReqOpts{
		StatName:       "service.clips.delete_clips_media_v3",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.Do(ctx, req, combinedOpts)
	if err != nil {
		return err
	}

	if resp.StatusCode >= 400 {
		return twitchclient.HandleFailedResponse(resp)
	}
	return nil
}
