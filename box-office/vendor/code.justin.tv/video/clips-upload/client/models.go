package clips

import "time"

// Clip is a client-friendly representation of a Clip.
type Clip struct {
	ID        string `json:"id"`
	Slug      string `json:"slug"`
	URL       string `json:"url"`
	EmbedURL  string `json:"embed_url"`
	EmbedHTML string `json:"embed_html"`
	ViewURL   string `json:"view_url"`
	EditURL   string `json:"edit_url"`

	BroadcasterID          string  `json:"broadcaster_id"`
	BroadcasterDisplayName string  `json:"broadcaster_display_name"`
	BroadcasterLogin       string  `json:"broadcaster_login"`
	BroadcasterChannelURL  string  `json:"broadcaster_channel_url"`
	BroadcasterLogo        *string `json:"broadcaster_logo"`
	CuratorID              string  `json:"curator_id"`
	CuratorDisplayName     string  `json:"curator_display_name"`
	CuratorLogin           string  `json:"curator_login"`
	CuratorChannelURL      string  `json:"curator_channel_url"`
	CuratorLogo            *string `json:"curator_logo"`

	VodID       *string `json:"vod_id"`
	VodURL      *string `json:"vod_url"`
	VodOffset   *int64  `json:"vod_offset"`
	BroadcastID *string `json:"broadcast_id"`

	Game      string    `json:"game"`
	Title     string    `json:"title"`
	Views     int64     `json:"views"`
	Duration  float64   `json:"duration"`
	Published bool      `json:"published"`
	CreatedAt time.Time `json:"created_at"`
	Language  string    `json:"language"`

	PreviewImage string            `json:"preview_image"`
	Thumbnails   map[string]string `json:"thumbnails"`
}

type ClipMetadata struct {
	FbAppID       string `json:"fb_app_id"`
	OgURL         string `json:"og_url"`
	OgSiteName    string `json:"og_site_name"`
	OgType        string `json:"og_type"`
	OgImage       string `json:"og_image"`
	OgTitle       string `json:"og_title"`
	OgDescription string `json:"og_description"`

	TwitterCard              string `json:"twitter_card"`
	TwitterSite              string `json:"twitter_site"`
	TwitterImage             string `json:"twitter_image"`
	TwitterPlayer            string `json:"twitter_player"`
	TwitterPlayerWidth       string `json:"twitter_player_width"`
	TwitterPlayerHeight      string `json:"twitter_player_height"`
	TwitterImagePartnerBadge string `json:"twitter_image_partner_badge"`
	TwitterTitle             string `json:"twitter_title"`
	TwitterDescription       string `json:"twitter_description"`

	Viewport string `json:"viewport"`
}

// DeletedClip is a metadata for a Clip that is returned on deletion.
type DeletedClip struct {
	ID   string `json:"id"`
	Slug string `json:"slug"`
}

// DeletedClips is a list of deleted clips.
type DeletedClips []*DeletedClip

// PaginatedClipsResponse is a list of clips on the current page. Pass in cursor
// to get the next page of clips.
type PaginatedClipsResponse struct {
	Clips  []*Clip `json:"clips"`
	Cursor string  `json:"cursor"`
}

// SegmentInfo contains the information to denote the segment of the video. By
// default, Speed is 1x
type SegmentInfos []SegmentInfo

type SegmentInfo struct {
	Offset   float64  `json:"offset"`
	Duration float64  `json:"duration"`
	Speed    *float64 `json:"speed"`
}

type RawStatus struct {
	Status                   string           `json:"raw_media_status"`
	DefaultClipURL           string           `json:"default_clip_url"`
	DefaultClipInitialOffset float64          `json:"default_clip_initial_offset"`
	DefaultClipDuration      float64          `json:"default_clip_duration"`
	RawMediaVideoURL         string           `json:"raw_media_video_url"`
	RawMediaSpriteSheetURL   string           `json:"raw_media_spritesheet_url"`
	RawMediaDuration         float64          `json:"raw_media_duration"`
	FilmStripSecondsPerFrame int              `json:"film_strip_seconds_per_frame"`
	FilmStripNumFrames       int              `json:"film_strip_num_frames"`
	FrameWidth               int              `json:"frame_width"`
	FrameHeight              int              `json:"frame_height"`
	QualityOptions           []*QualityOption `json:"quality_options"`
}

// Status is the client-friendly representation of a Clip's status.
type Status struct {
	Status         string          `json:"status"`
	QualityOptions []QualityOption `json:"quality_options"`
}

// QualityOption is the client-friendly representation of a Clip's video quality option.
type QualityOption struct {
	Quality   string   `json:"quality"`
	Source    string   `json:"source"`
	FrameRate *float64 `json:"frame_rate"`
}

// RelatedClipsSets is a struct containing a list of clips, for each algorithm that the relatedClipsSet function could return.
type RelatedClipsSets struct {
	Similar  []*Clip `json:"similar"`
	Game     []*Clip `json:"game"`
	Top      []*Clip `json:"top"`
	Channel  []*Clip `json:"channel"`
	Curator  []*Clip `json:"curator"`
	Combined []*Clip `json:"combined"`
}

// ClipCount is the client-friendly representation of a broadcast's clip count.
type ClipCount struct {
	Count int `json:"count"`
}

// ChannelConfig is currently used to enable/disable clips on a channel.
type ChannelConfig struct {
	ChannelID            string `json:"channel_id"`
	ClipCreationDisabled bool   `json:"clip_creation_disabled"`
}
