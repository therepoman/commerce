package common

import (
	"fmt"
	"regexp"
	"time"

	"github.com/golang/protobuf/ptypes"
	ptypes_timestamp "github.com/golang/protobuf/ptypes/timestamp"
)

var (
	// NodeRegexp matches internal machine names (i.e. two-element names: "foo-a9891b.sfo01").
	NodeRegexp = regexp.MustCompile(`(?i)^[a-z0-9_-]+\.[a-z]{3}\d{2}$`)

	// FQDNRegexp matches fully-qualified domain names.
	//
	// TODO: This is very messy, and obviously there are many valid FQDNs that are not associated with these TLDs, but
	// hopefully this will prevent us from accidentally sticking a non-fully-qualified name in here.
	FQDNRegexp = regexp.MustCompile(`(?i)^[a-z0-9_-]+(?:\.[a-z0-9_-]+)*\.(?:com|net|org|tv)$`)

	// OriginResourcePathRegexp matches values that neither begin nor end with a slash ("/").
	OriginResourcePathRegexp = regexp.MustCompile(`^[^/].*[^/]$`)

	// MULTIREP_MIGRATION
	// OriginResourcePathOriginDcRegexp matches values whose first path component is a dot-delimited pair. The first
	// and second part of the pair may contain any character which isn't a slash "/" or a dot "." because those are
	// used as delimiters. See the tests for examples.
	OriginResourcePathOriginDcRegexp = regexp.MustCompile(`^[^./]+\.([^./]+)/.*$`)
)

// TODO: It would be very nice to use tags for this, but per https://github.com/golang/protobuf/issues/52, the protobuf
//       team does not intend to support that.  Perhaps we can extend e.g. protoc_gen to do it?

var (
	// ErrFieldMissing is used to indicate that a non-optional field was not specified.
	ErrFieldMissing = fmt.Errorf("no value given")
)

// FieldErrorf formats an error describing an issue with one of the message's fields.  err is optional; if it is nil, a
// generic message is used instead.
func FieldErrorf(fieldName string, val interface{}, err error) error {
	if err == nil {
		err = fmt.Errorf("unexpected value")
	}
	return fmt.Errorf("field %q: %s: %q", fieldName, err, val)
}

// SubmessageErrorf formats an error returned from FieldErrorf to indicate that the field described in the error belongs
// to a submessage.  This is intended to help in making validation composable.
//
// TODO: Should be able to generate much nicer messages; e.g. "field foo.bar.baz: unexpected value: 3"
func SubmessageErrorf(fieldName string, err error) error {
	return fmt.Errorf("submessage %q: %s", fieldName, err)
}

var (
	// MinValidTimestamp is the earliest time that validateTimestamp() considers reasonable.
	MinValidTimestamp = time.Date(2000, 01, 01, 0, 0, 0, 0, time.UTC)
	// MaxValidTimestamp is the latest time that validateTimestamp() considers reasonable.
	MaxValidTimestamp = time.Date(2100, 01, 01, 0, 0, 0, 0, time.UTC)
)

// ValidateTimestamp checks that the value given is a valid timestamp and (as a sanity check) that it falls in the
// present century.  If optional is true, the zero value is also accepted.
func ValidateTimestamp(fieldName string, val *ptypes_timestamp.Timestamp, optional bool) error {
	if optional && val == nil {
		return nil
	}
	goVal, err := ptypes.Timestamp(val)
	if err != nil {
		return FieldErrorf(fieldName, val, err) // TODO: would be nice to include all of this information
	}
	if goVal.Before(MinValidTimestamp) || goVal.After(MaxValidTimestamp) {
		return FieldErrorf(fieldName, goVal, fmt.Errorf("out of range"))
	}
	return nil
}

// ValidateUUID returns an error iff the value is not exactly 16 bytes long; since UUIDs are 128-bit integers, any 16
// bytes can be interpreted as a UUID.  If optional is true, zero-byte (unset) values are also accepted.
func ValidateUUID(fieldName string, val []byte, optional bool) error {
	if !(len(val) == 16 || (optional && len(val) == 0)) {
		return FieldErrorf(fieldName, val, fmt.Errorf("expected exactly 16 bytes"))
	}
	return nil
}

// Validator is implemented by any value that has a Validate method.
type Validator interface {
	// Validate checks that the message is well-formed.  It returns two values: a message version and an error.  If the
	// error is not nil, the message is invalid.  The message version is the highest version for which the message is
	// valid; the definition fo each version is defined on a per-message-type basis.  If the message is invalid, a value
	// of -1 may (but is not required to be) returned for the version.
	Validate() (int64, error)
}

// Validate checks that the message is well-formed; it returns nil iff so, or a descriptive error otherwise.
//
// The only valid message version is 0.
func (r *UserEdgeRequest) Validate() (int64, error) {
	if r == nil {
		return -1, fmt.Errorf("message is nil")
	}

	if !(r.Protocol == UserEdgeRequest_HTTP || r.Protocol == UserEdgeRequest_HTTPS) {
		return -1, FieldErrorf("protocol", r.Protocol, nil)
	}

	if !NodeRegexp.MatchString(r.Node) {
		return -1, FieldErrorf("node", r.Node, nil)
	}

	if r.ContentHost != "" && !FQDNRegexp.MatchString(r.ContentHost) {
		return -1, FieldErrorf("content_host", r.ContentHost, nil)
	}

	err := ValidateTimestamp("expiration", r.Expiration, true)
	if err != nil {
		return -1, err
	}

	if r.EventNode != "" && !NodeRegexp.MatchString(r.EventNode) {
		return -1, err
	}

	return 0, nil
}

var _ Validator = (*UserEdgeRequest)(nil)

// Validate checks that the message is well-formed; it returns nil iff so, or a descriptive error otherwise.
//
// The only valid version of this message is 0.  However, keep in mind that legacy (pre-Weaver) segment requests do not
// have this message associated with them; only playlist requests do.
func (s *PlaybackSession) Validate() (int64, error) {
	if s == nil {
		return -1, fmt.Errorf("message is nil")
	}

	// TODO: All valid uint64s are valid session IDs.  It would be very nice if 0 were a reserved value so that we could
	// assert that the ID is present.

	if len(s.Ppid) > 16 {
		return -1, FieldErrorf("ppid", s.Ppid, fmt.Errorf("must not be longer than 16 bytes"))
	}

	// The entire range (i.e. both true and false) is valid for authenticated and stitching_ads.

	return 0, nil
}

var _ Validator = (*PlaybackSession)(nil)

// Validate checks that the message is well-formed; it returns nil iff so, or a descriptive error otherwise.  N.B.: The
// 'quality' field MUST be empty if this message describes a broadcast and MUST NOT be empty if this message describes a
// stream.  This condition MUST be enforced by the Validate() function associated with the message that embeds this
// submessage.
//
// Version 0 indicates that this is a legacy (pre-Weaver) message.
//
// Version 1 indicates that this is a Weaver-era message.
func (s *Stream) Validate() (int64, error) {
	if s == nil {
		return -1, fmt.Errorf("message is nil")
	}

	err := ValidateUUID("broadcast_id", s.BroadcastId, true)
	if err != nil {
		return -1, err
	}

	if s.UsherStreamId == 0 {
		return -1, FieldErrorf("usher_stream_id", s.UsherStreamId, ErrFieldMissing)
	}

	if len(s.Channel) == 0 {
		return -1, FieldErrorf("channel", s.Channel, ErrFieldMissing)
	}

	if !OriginResourcePathRegexp.MatchString(s.OriginResourcePath) {
		return -1, FieldErrorf("origin_resource_path", s.OriginResourcePath, nil)
	}

	if err := s.ValidateOrigin(); err != nil {
		return -1, err
	}

	// Legacy messages do not include the broadcast ID.
	if len(s.BroadcastId) == 0 {
		return 0, nil
	}
	return 1, nil
}

// ValidateOrigin checks that `origin_dc` and `origin_resource_path` are in
// agreement about the Stream's origin when an origin is extracted from
// `origin_resource_path`. MULTIREP_MIGRATION
func (s *Stream) ValidateOrigin() error {
	originMatch := OriginResourcePathOriginDcRegexp.FindStringSubmatch(s.OriginResourcePath)
	var extractedOrigin string
	if len(originMatch) > 1 {
		extractedOrigin = originMatch[1]
	}
	if extractedOrigin != "" && s.OriginDc != extractedOrigin {
		return FieldErrorf("origin_resource_path", s.OriginResourcePath,
			fmt.Errorf("contains origin %q but doesn't match origin_dc %q", extractedOrigin, s.OriginDc))
	}
	return nil
}

var _ Validator = (*Stream)(nil)
