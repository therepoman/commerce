package common

import "fmt"

// PlaylistFormat is an enumerated type that describes how playlist data was serialized.
type PlaylistFormat int

const (
	// M3U8PlaylistFormat indicates that the playlist is in the HLS-spec-compliant M3U8 format.
	M3U8PlaylistFormat PlaylistFormat = iota
	// ProtoPlaylistFormat indicates that the playlist is a binary-serialized `hls.Playlist` message.
	ProtoPlaylistFormat
	// JSONProtoPlaylistFormat indicates that the playlist is a JSON-serialized `hls.Playlist` message.
	JSONProtoPlaylistFormat
)

func (f PlaylistFormat) String() string {
	switch f {
	case M3U8PlaylistFormat:
		return "M3U8PlaylistFormat"
	case ProtoPlaylistFormat:
		return "ProtoPlaylistFormat"
	case JSONProtoPlaylistFormat:
		return "JSONProtoPlaylistFormat"
	default:
		return "UNKNOWN_VALUE"
	}
}

// FileExtension returns the customary file extension (including the dot) of a playlist in the given format.
func (f PlaylistFormat) FileExtension() (string, error) {
	switch f {
	case M3U8PlaylistFormat:
		return ".m3u8", nil
	case ProtoPlaylistFormat:
		return ".proto", nil
	case JSONProtoPlaylistFormat:
		return ".json", nil
	default:
		return "", fmt.Errorf("unexpected value for playlist format: %d", f)
	}
}

// UpstreamPlaylistURI builds a URI appropriate for elements of the replication system to use when requesting live
// playlists for this stream.
func (s *Stream) UpstreamPlaylistURI(format PlaylistFormat, prefetch bool) (string, error) {
	if s.OriginResourcePath == "" || s.Quality == "" {
		return "", fmt.Errorf("required fields are unset")
	}

	ext, err := format.FileExtension()
	if err != nil {
		return "", err
	}

	prefetchSuffix := ""
	if prefetch {
		prefetchSuffix = "-prefetch"
	}

	return fmt.Sprintf("/%s/%s/index-live%s%s", s.OriginResourcePath, s.Quality, prefetchSuffix, ext), nil
}
