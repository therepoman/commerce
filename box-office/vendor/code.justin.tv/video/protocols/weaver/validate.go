package weaver

import (
	"fmt"

	"code.justin.tv/video/protocols/common"
)

// Validate checks that the message is well-formed; it returns nil iff so, or a descriptive error otherwise.
func (r *RequestInfo) Validate() (int64, error) {
	if r == nil {
		return -1, fmt.Errorf("message is nil")
	}

	if _, err := r.Request.Validate(); err != nil {
		return -1, common.SubmessageErrorf("request", err)
	}

	if _, err := r.Session.Validate(); err != nil {
		return -1, common.SubmessageErrorf("session", err)
	}

	if _, err := r.Stream.Validate(); err != nil {
		return -1, common.SubmessageErrorf("stream", err)
	}

	// TODO: Check that exactly one of the Content types is set an that whichever is set validates.

	return 0, nil
}

var _ common.Validator = (*RequestInfo)(nil)

// Validate is presently a no-op.  (TODO: Write me!)
func (r *SegmentRequest) Validate() (int64, error) {
	return 0, nil
}

var _ common.Validator = (*SegmentRequest)(nil)

// Validate is presently a no-op.  (TODO: Write me!)
func (r *PlaylistRequest) Validate() (int64, error) {
	return 0, nil
}

var _ common.Validator = (*PlaylistRequest)(nil)

// Validate is presently a no-op.  (TODO: Write me!)
func (r *Beacon) Validate() (int64, error) {
	return 0, nil
}

var _ common.Validator = (*Beacon)(nil)
