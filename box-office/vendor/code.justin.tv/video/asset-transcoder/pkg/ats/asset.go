package ats

import "strings"

// JSONAssetURI returns the URI where the asset for a request will be
// found. It can return an error if the request has an invalid
// OutputS3Prefix.
func JSONAssetURI(req *Request) (*S3URI, error) {
	base, err := S3URIFromURL(req.OutputS3Prefix)
	if err != nil {
		return nil, err
	}

	if !strings.HasSuffix(base.Key, "/") {
		base.Key += "/"
	}
	base.Key += "asset.json"
	return base, nil
}
