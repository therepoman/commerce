package ats

import (
	"fmt"
	"net/url"
	"path"
	"strings"
)

// S3URI represents a location in S3. It exists mostly for its helper
// methods which can interchange between representations used in
// asset-transcoder and its clients.
type S3URI struct {
	Bucket string
	Key    string
}

// S3URIFromPath parses a 'path-style' string like '/s3/bucket/key'
// into an S3URI. It errors if the string does not match its
// expectations of path style, namely that the string should start
// with "/s3/".
func S3URIFromPath(path string) (*S3URI, error) {
	if !strings.HasPrefix(path, "/s3/") {
		return nil, fmt.Errorf("malformed S3 path %q, missing /s3/ prefix", path)
	}
	path = strings.TrimPrefix(path, "/s3/")

	parts := strings.SplitN(path, "/", 2)

	uri := &S3URI{}
	uri.Bucket = parts[0]
	if len(parts) > 1 {
		uri.Key = "/" + parts[1]
	}
	return uri, nil
}

// S3URIFromURL parses a 'URL-style' string like 's3://bucket/key'
// into an S3URI. It errors if the string is not parsable as a URL, or
// if it doesn't have the 's3://' scheme.
func S3URIFromURL(u string) (*S3URI, error) {
	parsed, err := url.Parse(u)
	if err != nil {
		return nil, err
	}
	if parsed.Scheme != "s3" {
		return nil, fmt.Errorf("invalid S3 URL scheme %q", parsed.Scheme)
	}
	if parsed.Host == "" {
		return nil, fmt.Errorf("missing bucket in S3 URI %q", u)
	}
	return &S3URI{
		Bucket: parsed.Host,
		Key:    parsed.Path,
	}, nil
}

// Path returns the path-style format of the S3URI: /s3/bucket/key
func (u S3URI) Path() string {
	return path.Join("/s3", u.Bucket, u.Key)
}

// URL returns the url-style format of the S3URI: s3://bucket/key
func (u S3URI) URL() string {
	return (&url.URL{Scheme: "s3", Host: u.Bucket, Path: u.Key}).String()
}

// Subkey returns a copy of the S3URI, appending k to u.Key on the copy.
func (u *S3URI) Subkey(k string) *S3URI {
	u = u.copy()
	u.Key = path.Join(u.Key, k)
	return u
}

// String returns the URL-style format of the S3URI.
func (u *S3URI) String() string {
	return u.URL()
}

func (u *S3URI) copy() *S3URI {
	out := new(S3URI)
	*out = *u
	return out
}
