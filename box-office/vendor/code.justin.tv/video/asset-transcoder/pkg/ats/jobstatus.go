package ats

// JobSuccess is a helper to create a JobStatusUpdate saying that req
// succeeded and produced result
func JobSuccess(req *Request, result *Asset) *JobStatusUpdate {
	// If the job succeeded, then the request was valid, so we can
	// safely ignore the error here.
	uri, _ := JSONAssetURI(req)
	return &JobStatusUpdate{
		Request: req,
		State:   State_SUCCESS,
		Result: &JobStatusUpdate_JsonAssetManifestUri{
			JsonAssetManifestUri: uri.Path(),
		},
	}
}

// JobFail is a helper to create a JobStatusUpdate saying that req
// failed and produced err
func JobFail(req *Request, err *Error) *JobStatusUpdate {
	return &JobStatusUpdate{
		Request: req,
		State:   State_FAIL,
		Result:  &JobStatusUpdate_Error{Error: err},
	}
}
