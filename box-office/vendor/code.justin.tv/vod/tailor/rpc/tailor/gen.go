package tailor

//go:generate protoc -I .:$GOPATH:../../vendor models.proto responses.proto requests.proto service.proto --go_out=. --twirp_out=.
//go:generate sh -c "cat responses.pb.go | sed 's/,omitempty//' | sed 's/omitempty//' > responses.pb.go.temp && mv responses.pb.go.temp responses.pb.go"
//go:generate sh -c "cat service.twirp.go | sed 's/jsonpb.Marshaler{/jsonpb.Marshaler{EmitDefaults: true, /' > service.twirp.go.temp && mv service.twirp.go.temp service.twirp.go"
