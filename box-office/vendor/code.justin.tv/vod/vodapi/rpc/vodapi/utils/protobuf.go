package utils

import (
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/golang/protobuf/ptypes/wrappers"
)

// ProtobufBoolValue returns a Protobuf bool wrapper around a bool pointer
func ProtobufBoolValue(v *bool) *wrappers.BoolValue {
	if v == nil {
		return nil
	}

	return &wrappers.BoolValue{
		Value: *v,
	}
}

// ProtobufStringValue returns a Protobuf string wrapper around a string pointer
func ProtobufStringValue(v *string) *wrappers.StringValue {
	if v == nil {
		return nil
	}

	return &wrappers.StringValue{
		Value: *v,
	}
}

// ProtobufInt64Value returns a Protobuf wrapper around a int64 pointer
func ProtobufInt64Value(v *int64) *wrappers.Int64Value {
	if v == nil {
		return nil
	}

	return &wrappers.Int64Value{
		Value: *v,
	}
}

// ProtobufFloat64Value returns a Protobuf wrapper around a float64 pointer
func ProtobufFloat64Value(v *float64) *wrappers.DoubleValue {
	if v == nil {
		return nil
	}

	return &wrappers.DoubleValue{
		Value: *v,
	}
}

// ProtobufTimeAsTimestamp returns timestamp version of the go time object
func ProtobufTimeAsTimestamp(tm *time.Time) *timestamp.Timestamp {
	if tm == nil {
		return nil
	}
	if tm.IsZero() {
		return nil
	}
	ts, err := ptypes.TimestampProto(*tm)
	if err != nil {
		return nil
	}
	return ts
}

// FromProtobufTimestampToTime returns a Time from a protobuf timestamp
func FromProtobufTimestampToTime(ts *timestamp.Timestamp) *time.Time {
	if ts == nil {
		return nil
	}
	t, err := ptypes.Timestamp(ts)
	if err != nil {
		return nil
	}
	return &t
}

// FromProtobufStringValue returns a string from Protobuf wrapped string
func FromProtobufStringValue(str *wrappers.StringValue) *string {
	if str == nil {
		return nil
	}
	return &str.Value
}

// FromProtobufInt64Value returns a int64 from Protobuf wrapped int64
func FromProtobufInt64Value(val *wrappers.Int64Value) *int64 {
	if val == nil {
		return nil
	}
	return &val.Value
}

// FromProtobufFloat64Value returns a float from Protobuf wrapped float64
func FromProtobufFloat64Value(val *wrappers.DoubleValue) *float64 {
	if val == nil {
		return nil
	}
	return &val.Value
}

// FromProtobufBoolValue returns a bool from Protobuf wrapped bool
func FromProtobufBoolValue(val *wrappers.BoolValue) *bool {
	if val == nil {
		return nil
	}
	return &val.Value
}
