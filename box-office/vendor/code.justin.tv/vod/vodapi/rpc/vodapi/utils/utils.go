package utils

import (
	"strconv"
	"time"

	"github.com/golang/protobuf/ptypes/timestamp"
)

// Now is the interface to time.Now to allow for stubbing
var Now func() time.Time

func init() {
	Now = func() time.Time {
		return time.Now().UTC()
	}
}

// StubTimeNow is a shorthand for stubbing out the time.Now
func StubTimeNow() time.Time {
	Now = func() time.Time {
		return time.Date(2017, 12, 12, 12, 12, 0, 0, time.UTC)
	}
	return Now()
}

// StrPtr converts a string to a pointer
func StrPtr(str string) *string {
	return &str
}

// Int64Ptr converts int64 to a pointer
func Int64Ptr(num int64) *int64 {
	return &num
}

// TimePtr converts Time to a pointer
func TimePtr(tm time.Time) *time.Time {
	return &tm
}

// FromTimePtr converts a Time pointer to a regular Time
func FromTimePtr(tm *time.Time) time.Time {
	if tm != nil {
		return *tm
	}
	return time.Time{}
}

// TimestampPtr converts timestamp to a pointer
func TimestampPtr(tm timestamp.Timestamp) *timestamp.Timestamp {
	return &tm
}

// ConvertStrPtrToStr converts a string pointer to string that's empty when pointer is nil
func ConvertStrPtrToStr(str *string) string {
	if str == nil {
		return ""
	}
	return *str
}

// ConvertInt64PtrToInt64 converts an int64 pointer to string that's empty when pointer is nil
func ConvertInt64PtrToInt64(val *int64) int64 {
	if val == nil {
		return 0
	}
	return *val
}

// StrToInt64 converts a string to an int
func StrToInt64(val string) int64 {
	ret, err := strconv.ParseInt(val, 10, 64)
	if err != nil {
		return 0
	}
	return ret
}

// Int64ToStr converts an int to a string
func Int64ToStr(val int64) string {
	return strconv.FormatInt(val, 10)
}

// StrPtrToInt64Ptr converts a string pointer to an int pointer
func StrPtrToInt64Ptr(val *string) *int64 {
	var ret *int64
	if val != nil {
		val := StrToInt64(*val)
		ret = &val
	}
	return ret
}

// Int64PtrToStrPtr converts an int pointer to a string pointer
func Int64PtrToStrPtr(val *int64) *string {
	var ret *string
	if val != nil {
		str := Int64ToStr(*val)
		ret = &str
	}
	return ret
}

// StrArrayToIntArray converts a string array to an int array
func StrArrayToInt64Array(array []string) []int64 {
	ret := make([]int64, len(array))
	for i, val := range array {
		ret[i] = StrToInt64(val)
	}
	return ret
}
