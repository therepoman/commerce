syntax = "proto3";
package vodapi;
option go_package = "vodapi";

import "google/protobuf/wrappers.proto";
import "google/protobuf/timestamp.proto";
import "consts.proto";

message CreateUploadRequest {
    // broadcast_type is the broadcast type of the vod
    VodType broadcast_type = 1;
    // broadcast_id is the id related to the broadcast of the vod
    string broadcast_id = 2;
    // owner_id is the owner of the vod
    string owner_id = 3;
    // description is a user provided description of the vod
    string description = 4;
    // title is the vod title that shows up in directory listings
    string title = 5;
    // game is the game for the vod
    string game = 6;
    // communities is what communities the vod is related to
    repeated string communities = 7;
    // language is the language of the vod
    string language = 8;
    // tag_list is a list of tag descriptors of the vod
    repeated string tag_list = 9;
    // viewable is the viewability configuration for the vod
    VodViewable viewable = 10;
    // viewable_at is when the vod became publicly viewable
    google.protobuf.Timestamp viewable_at = 11;
    // duration is the duration of the vod
    int64 duration = 12;
    // uri is the path to the raw archive data
    string uri = 13;
    // manifest is the file describing the different ts chunks
    string manifest = 14;
}

message ShowFormatRequest {
    // playlist_preference is an index into the formats for order to play the formats
    int64 playlist_preference = 1;
    // display_name is the display name of the format
    string display_name = 2;
    // bitrate is the bitrate of the format
    int64 bitrate = 3;
    // fps is the frames per second of the format
    double fps = 4;
    // max_idr_interval is the max idr interval for the format
    int64 max_idr_interval = 5;
    // codecs is the list of codecs related to this format
    string codecs = 6;
    // resolution is the resolution of the format
    string resolution = 7;
}

message CreateArchiveRequest {
    // broadcast_id is the id related to the broadcast of the vod
    string broadcast_id = 1;
    // broadcaster_software is what software was used to broadcast the archive
    string broadcaster_software = 2;
    // recorded_on is when the archive became recorded
    google.protobuf.Timestamp recorded_on = 3;
    // title is the title of the archive
    string title = 4;
    // channel_id is the channel creating the archive
    string channel_id = 5;
    // duration is the duration of the vod
    int64 duration = 7;
    // uri is the path to the raw archive data
    string uri = 8;
    // manifest is the file describing the different ts chunks
    string manifest = 9;
    // deleted is whether the vod is deleted
    bool deleted = 10;
    // handwave_latency_ms is the handwave latency milliseconds
    int64 handwave_latency_ms = 11;
}

message InternalCreateHighlightRequest {
    // source_archive_id is the id of the original source vod
    string source_archive_id = 1;
    // owner_id is the owner of the vod
    string owner_id = 2;
    // uri is the path to the raw data
    string uri = 3;
    // broadcast_id is the id related to the broadcast of the vod
    string broadcast_id = 4;
    // show_formats is the new show_formats value for the vod
    map<string, ShowFormatRequest> show_formats = 5;
    // title is the title for the highlight
    string title = 6;
    // duration is the duration of the vod
    int64 duration = 7;
}

message ManagerCreateHighlightRequest {
    // vod_id is the id of the original source vod
    string vod_id = 1;
    // description is the description for the highlight
    string description = 2;
    // game is the game for the highlight
    string game = 3;
    // title is the title for the highlight
    string title = 4;
    // language is the language for the highlight
    string language = 5;
    // start_seconds is the start time in the original vod for the highlight
    int64 start_seconds = 6;
    // end_seconds is the end time in the original vod for the highlight
    int64 end_seconds = 7;
    // tag_list is a list of tag descriptors of the vod
    repeated string tag_list = 8;
    // created_by is the user that created this highlight
    string created_by = 9;
    // user_id is the user id making the create highlight request (could be an editor or staff)
    string user_id = 10;
}

message PublicGetVodByIDRequest {
    // vod_id is the id of the vod that is being requested
    string vod_id = 1;
}

message InternalGetVodByIDRequest {
    // vod_id is the id of the vod that is being requested
    string vod_id = 1;
}

message InternalGetVodByIDIncludeBannedUsersRequest {
    // vod_id is the id of the vod that is being requested
    string vod_id = 1;
}

message ManagerInternalGetVodByIDRequest {
    // vod_id is the id of the vod that is being requested
    string vod_id = 1;
    // user_id is the user id making request (could be an editor or staff)
    string user_id = 2;
}

message ManagerGetVodMuteInfoRequest {
    // vod_id is the id of the vod that is being requested
    string vod_id = 1;
    // user_id is the user id making request (could be an editor or staff)
    string user_id = 2;
}

message InternalGetVodMuteInfoRequest {
    // vod_id is the id of the vod that is being requested
    string vod_id = 1;
}

message ManagerGetVodMuteInfosRequest {
    // vod_ids is the ids of the vods that is being requested
    repeated string vod_ids = 1;
    // user_id is the user id making request (could be an editor or staff)
    string user_id = 2;
}

message GetVodByIDIncludeBannedUsersRequest {
    // vod_id is the id of the vod that is being requested
    string vod_id = 1;
}

message ManagerGetVodByIDRequest {
    // vod_id is the id of the vod that is being requested
    string vod_id = 1;
    // user_id is the user id making request (could be an editor or staff)
    string user_id = 2;
}

message ManagerSearchVodsRequest {
    // channel_id is the id of the channel we are making the request for
    string channel_id = 1;
    // search is the search string
    string search = 2;
    // limit is the maximum number of response objects from the response list, used for pagination
    int64 limit = 3;
    // offset is the offset into the response list, used for pagination
    int64 offset = 4;
    // user_id is the user id making request (could be an editor or staff)
    string user_id = 5;
}

message PublicGetVodsByUserRequest {
    // channel_id is the id of the channel we are making the request for
    string channel_id = 1;
    // broadcast_types is the list of broadcast types that we want to filter by
    repeated VodType broadcast_types = 2;
    // language is the language filter
    string language = 3;
    // status is the filter based on vod status
    VodStatus status = 4;
    // sort is the sort format of the response objects
    VodSort sort = 5;
    // limit is the maximum number of response objects from the response list, used for pagination
    int64 limit = 6;
    // offset is the offset into the response list, used for pagination
    int64 offset = 7;
    // recorded_after is a filter for vods recorded after a time
    google.protobuf.Timestamp recorded_after = 8;
    // recorded_before is a filter for vods recorded before a time
    google.protobuf.Timestamp recorded_before = 9;
}

message ManagerInternalGetVodsByUserRequest {
    // channel_id is the id of the channel we are making the request for
    string channel_id = 1;
    // broadcast_types is the list of broadcast types that we want to filter by
    repeated VodType broadcast_types = 2;
    // language is the language filter
    string language = 3;
    // sort is the sort format of the response objects
    VodSort sort = 5;
    // limit is the maximum number of response objects from the response list, used for pagination
    int64 limit = 6;
    // offset is the offset into the response list, used for pagination
    int64 offset = 7;
    // user_id is the user id making request (could be an editor or staff)
    string user_id = 8;
    // published_within is a filter on responses published within some threshold
    string published_within = 9;
    // recorded_after is a filter for vods recorded after a time
    google.protobuf.Timestamp recorded_after = 10;
    // recorded_before is a filter for vods recorded before a time
    google.protobuf.Timestamp recorded_before = 11;
    // statuses is the filter based on vod statuses
    repeated VodStatus statuses = 12;

    // DEPRECATED

    // status is the filter based on vod status
    VodStatus status = 4;
}

message ManagerGetVodsByUserIncludeBannedUsersRequest {
    // channel_id is the id of the channel we are making the request for
    string channel_id = 1;
    // broadcast_types is the list of broadcast types that we want to filter by
    repeated VodType broadcast_types = 2;
    // language is the language filter
    string language = 3;
    // status is the filter based on vod status
    VodStatus status = 4;
    // sort is the sort format of the response objects
    VodSort sort = 5;
    // limit is the maximum number of response objects from the response list, used for pagination
    int64 limit = 6;
    // offset is the offset into the response list, used for pagination
    int64 offset = 7;
    // user_id is the user id making request (could be an editor or staff)
    string user_id = 8;
}

message InternalGetVodsByBroadcastIDsRequest {
    // broadcast_ids is the list of broadcast ids to make the request for
    repeated string broadcast_ids = 1;
}

message PublicGetVodsByIDsRequest {
    // vod_ids is the ids of the vods that is being requested
    repeated string vod_ids = 1;
}

message InternalGetVodsByIDsRequest {
    // vod_ids is the ids of the vods that is being requested
    repeated string vod_ids = 1;
}

message InternalGetPrivateVodsByIDsRequest {
    // vod_ids is the ids of the vods that is being requested
    repeated string vod_ids = 1;
}

message InternalGetPrivateAndDeletedVodsByIDsRequest {
    // vod_ids is the ids of the vods that is being requested
    repeated string vod_ids = 1;
}

message GetVodsByIDsIncludeBannedUsersRequest {
    // vod_ids is the ids of the vods that is being requested
    repeated string vod_ids = 1;
}

message PublicGetVodsByUsersRequest {
    // channel_ids is the ids of the channels we are making the request for
    repeated string channel_ids = 1;
    // broadcast_types is the list of broadcast types that we want to filter by
    repeated VodType broadcast_types = 2;
    // language is the language filter
    string language = 3;
    // sort is the sort format of the response objects
    VodSort sort = 5;
    // limit is the maximum number of response objects from the response list, used for pagination
    int64 limit = 6;
    // offset is the offset into the response list, used for pagination
    int64 offset = 7;
}

message GetTopVodsRequest {
    // broadcast_types is the list of broadcast types that we want to filter by
    repeated VodType broadcast_types = 1;
    // language is the language filter
    string language = 2;
    // game is the game filter
    string game = 3;
    // period is the period when we are making the request for
    string period = 4;
    // sort is the sort format of the response objects
    VodSort sort = 5;
    // limit is the maximum number of response objects from the response list, used for pagination
    int64 limit = 6;
    // offset is the offset into the response list, used for pagination
    int64 offset = 7;
}

message ThumbnailRequest {
    // path is the path of the thumbnail
    string path = 1;
    // offset is the offset into the thumbnail
    google.protobuf.DoubleValue offset = 2;
}
message ManagerUpdateVodRequest {
    // vod_id is the id of the vod that is being requested
    string vod_id = 1;
    // broadcast_type is the new broadcast type for the vod
    VodType broadcast_type = 2;
    // description is the new description value for the vod
    google.protobuf.StringValue description = 3;
    // game is the new game value for the vod
    google.protobuf.StringValue game = 4;
    // communities is the new communities value for the vod
    repeated string communities = 5;
    // language is the new language value for the vod
    google.protobuf.StringValue language = 6;
    // tag_list is the new tag_list value for the vod
    repeated string tag_list = 7;
    // thumbnail_path is the new thumbnail_path value for the vod
    google.protobuf.StringValue thumbnail_path = 8;
    // title is the new title value for the vod
    google.protobuf.StringValue title = 9;
    // viewable is the new viewable value for the vod
    VodViewable viewable = 10;
    // viewable_at is the new viewable_at value for the vod
    google.protobuf.Timestamp viewable_at = 11;
    // broadcast_id is the new broadcast_id value for the vod
    google.protobuf.StringValue broadcast_id = 12;
    // offset is the new offset value for the vod
    google.protobuf.Int64Value offset = 13;
    // status is the new status value for the vod
    VodStatus status = 14;
    // duration is the new duration value for the vod
    google.protobuf.Int64Value duration = 16;
    // show_formats is the new show_formats value for the vod
    map<string, ShowFormatRequest> show_formats = 17;
    // user_id is the user id making request (could be an editor or staff)
    string user_id = 18;
    // uri is the uri of the vod
    google.protobuf.StringValue uri = 19;
}

message UndeleteVodsRequest {
    // vod_ids is the ids of the vods that is being requested
    repeated string vod_ids = 1;
}

message SoftDeleteVodsRequest {
    // vod_ids is the ids of the vods that is being requested
    repeated string vod_ids = 1;
}

message InternalVodRemoveRecordsRequest {
    // vod_ids is the ids of the vods that is being requested
    repeated string vod_ids = 1;
}

message HardDeleteVodsRequest {
    // vod_ids is the ids of the vods that is being requested
    repeated string vod_ids = 1;
}

message SoftDeleteVodsInIntervalRequest {
    // start_time_unix_seconds is the start time in unix seconds for the delete request
    int64 start_time_unix_seconds = 1;
    // end_time_unix_seconds is the end time in unix seconds for the delete request
    int64 end_time_unix_seconds = 2;
}

message ManagerSoftDeleteVodsRequest {
    // vod_ids is the ids of the vods that is being requested
    repeated string vod_ids = 1;
    // user_id is the user id making request (could be an editor or staff)
    string user_id = 2;
}

message YoutubeExportRequest {
    // vod_id is the id of the vod that is being requested
    string vod_id = 1;
    // title is the title of the youtube exported vod
    string title = 2;
    // description is the description of the youtube exported vod
    string description = 3;
    // tag_list is the tag list of the youtube exported vod
    repeated string tag_list = 4;
    // private is whether the vod is private
    bool private = 5;
    // do_split is whether the vod should be split up
    bool do_split = 6;
    // user_id is the user id making request (could be an editor or staff)
    string user_id = 7;
}

message FinalizeUploadRequest {
    // vod_id is the id of the vod that is being requested
    string vod_id = 1;
    // generate_storyboard is a flag to indicate whether storyboards should be generated
    bool generate_storyboard = 2;
}

message UpdateManifestRequest {
    // vod_id is the id of the vod that is being requested
    string vod_id = 1;
    // old_manifest is the old manifest
    string old_manifest = 2;
    // new_manifest is the new manifest
    string new_manifest = 3;
}

message ViewcountsUpdate {
    // vod_id is the id of the vod that is being requested
    string vod_id = 1;
    // count is the view count of the vod
    int64 count = 2;
}

message SetViewcountsRequest {
    // viewcounts is the view counts update list
    repeated ViewcountsUpdate viewcounts = 1;
}

message GetPublicVodAggregationsByIDsRequest {
    // vod_ids is the ids of the vods that is being requested
    repeated string vod_ids = 1;
}

message GetVodPopularityRequest {
    // vod_id is the id of the vod that is being requested
    string vod_id = 1;
}

message CreateErrorRequest {
    // vod_id is the id of the vod that is being requested
    string vod_id = 1;
    // error_code is the error code
    string error_code = 2;
    // error_message is the error message
    string error_message = 3;
}

message CreateThumbnailsRequest {
    // vod_id is the id of the vod that is being requested
    string vod_id = 1;
    // thumbnails is the list of thumbnails to create
    repeated ThumbnailRequest thumbnails = 2;
}

message ManagerCreateCustomThumbnailUploadRequestRequest {
    // vod_id is the id of the vod that is being requested
    string vod_id = 1;
    // crop_x is the crop x coordinate
    int64 crop_x = 2;
    // crop_y is the crop y coordinate
    int64 crop_y = 3;
    // crop_w is the crop width
    int64 crop_w = 4;
    // crop_h is the crop height
    int64 crop_h = 5;
    // user_id is the user id making request (could be an editor or staff)
    string user_id = 6;
}

message ManagerDeleteThumbnailsRequest {
    // vod_id is the id of the vod that is being requested
    string vod_id = 1;
    // path is the path to delete
    string path = 2;
    // user_id is the user id making request (could be an editor or staff)
    string user_id = 3;
}

message DeleteThumbnailsRequest {
    // vod_id is the id of the vod that is being requested
    string vod_id = 1;
}

message ManagerGetUserVodPropertiesRequest {
    // channel_id is the channel that we are getting properties for
    string channel_id = 1;
    // user_id is the user id making request (could be an editor or staff)
    string user_id = 2;
}

message GetUserVodPropertiesRequest {
    // channel_id is the channel that we are getting properties for
    string channel_id = 1;
}

message ManagerUpdateUserVodPropertiesRequest {
    // channel_id is the channel that we are updating properties for
    string channel_id = 1;
    // user_id is the user id making request (could be an editor or staff)
    string user_id = 2;
    // save_vods_forever is a flag to indicate if vods should be saved forever
    google.protobuf.BoolValue save_vods_forever = 3;
    // vod_storage_days is how many days a vod should be stored
    google.protobuf.Int64Value vod_storage_days = 4;
    // can_upload_vod is a flag to indicate if the user can upload vods
    google.protobuf.BoolValue can_upload_vod = 5;
    // youtube_exporting_disabled is a flag to indicate if the user can export to youtube
    google.protobuf.BoolValue youtube_exporting_disabled = 6;
    // skip_upload_moderation is a flag to indicate if the user's vods skip moderation
    google.protobuf.BoolValue skip_upload_moderation = 7;
    // skip_muting is a flag to indicate if the user's vods skip muting
    google.protobuf.BoolValue skip_muting = 8;
    // can_upload_vod is a flag to indicate if the user can upload unlimited vods
    google.protobuf.BoolValue can_upload_unlimited = 9;
}

message ManagerGetUserVideoPrivacyPropertiesRequest {
    // channel_id is the channel that we are getting properties for
    string channel_id = 1;
    // user_id is the user id making request (could be an editor or staff)
    string user_id = 2;
}

message ManagerUpdateUserVideoPrivacyPropertiesRequest {
    // channel_id is the channel that we are updating properties for
    string channel_id = 1;
    // hide_archives is whether the user has archives hidden
    google.protobuf.BoolValue hide_archives = 2;
    // user_id is the user id making request (could be an editor or staff)
    string user_id = 3;
    // hide_archives_enabled is whether the user has archives hidden enabled
    google.protobuf.BoolValue hide_archives_enabled = 4;
}

message TrackAppealRequest {
    // audible_magic_response_id is the id of the audible magic response
    string audible_magic_response_id = 1;
    // reason is the reason the track is being appealed
    string reason = 2;
}

message ManagerCreateVodAppealsRequest {
    // vod_id is the id of the vod that this appeal is for
    string vod_id = 1;
    // city is the appeal city
    string city = 2;
    // country is the appeal country
    string country = 3;
    // full_name is the appeal full name
    string full_name = 4;
    // state is the appeal state
    string state = 5;
    // street_address1 is the appeal street address 1
    string street_address1 = 6;
    // street_address2 is the appeal street address 2
    string street_address2 = 7;
    // zipcode is the appeal zipcode
    string zipcode = 8;
    // track_appeals is list of tracks that are being appealed
    repeated TrackAppealRequest track_appeals = 9;
    // user_id is the user id making request (could be an editor or staff)
    string user_id = 10;
}

message GetVodAppealsRequest {
    // priority is whether this appeal is a priority
    google.protobuf.BoolValue priority = 1;
    // resolved is whether this appeal is already resolved
    google.protobuf.BoolValue resolved = 2;
    // limit is the maximum number of response objects from the response list, used for pagination
    int64 limit = 3;
    // offset is the offset into the response list, used for pagination
    int64 offset = 4;
    // vod_id is the id of the vod that this appeal is for
    string vod_id = 5;
    // user_info is a string containing the user info for the appeal
    string user_info = 6;
}

message ResolveTrackAppealRequest {
    // track_appeal_id is the track appeal id
    string track_appeal_id = 1;
    // action is the action being taken on this track appeal
    string action = 2;
}

message ResolveVodAppealRequest {
    // vod_appeal_id is the vod appeal id
    string vod_appeal_id = 1;
}

message CreateAudibleMagicResponseRequest {
    // vod_id is the id of the vod that this audible magic response request is for
    string vod_id = 1;
    // title is the amr title
    string title = 2;
    // performer is the amr performer
    string performer = 3;
    // genre is the amr genre
    string genre = 4;
    // artist is the amr artist
    string artist = 5;
    // album_title is the amr album title
    string album_title = 6;
    // song is the amr song
    string song = 7;
    // isrc is the amr isrc
    string isrc = 8;
    // is_match is whether there is a match for this amr
    bool is_match = 9;
    // match_offset_seconds is the match offset into the vod
    int64 match_offset_seconds = 10;
    // match_duration_seconds is the match duration
    int64 match_duration_seconds = 11;
    // scan_offset_seconds is the scan offset into the vod
    int64 scan_offset_seconds = 12;
    // scan_duration_seconds is the scan duration
    int64 scan_duration_seconds = 13;
    // mute_offset_seconds is the mute offset into the vod
    int64 mute_offset_seconds = 14;
    // mute_duration_seconds is the mute duration
    int64 mute_duration_seconds = 15;
    // audible_magic_item_id is the audible magic item id
    string audible_magic_item_id = 16;
}

message CreateAudibleMagicResponsesRequest {
    // audible_magic_responses is the list of audible magic responses
    repeated CreateAudibleMagicResponseRequest audible_magic_responses = 1;
}

message GetAudibleMagicResponsesRequest {
    // vod_ids is the ids of the vods that is being requested
    repeated string vod_ids = 1;
}

message UpdateAudibleMagicResponseRequest {
    // audible_magic_response_id is the audible magic response id
    string audible_magic_response_id = 1;
    // title is the new title for the amr
    google.protobuf.StringValue title = 3;
    // performer is the new performer for the amr
    google.protobuf.StringValue performer = 4;
    // genre is the new genre for the amr
    google.protobuf.StringValue genre = 5;
    // artist is the new artist for the amr
    google.protobuf.StringValue artist = 6;
    // album_title is the new album title for the amr
    google.protobuf.StringValue album_title = 7;
    // song is the new song for the amr
    google.protobuf.StringValue song = 8;
    // isrc is the new isrc for the amr
    google.protobuf.StringValue isrc = 9;
    // is_match is the whether there is a match
    bool is_match = 10;
    // match_offset_seconds is the match offset into the vod
    google.protobuf.Int64Value match_offset_seconds = 11;
    // match_duration_seconds is the match duration
    google.protobuf.Int64Value match_duration_seconds = 12;
    // scan_offset_seconds is the scan offset into the vod
    google.protobuf.Int64Value scan_offset_seconds = 13;
    // scan_duration_seconds is the scan duration
    google.protobuf.Int64Value scan_duration_seconds = 14;
    // mute_offset_seconds is the mute offset into the vod
    google.protobuf.Int64Value mute_offset_seconds = 15;
    // mute_duration_seconds is the mute duration
    google.protobuf.Int64Value mute_duration_seconds = 16;
    // audible_magic_item_id is the audible magic item id
    google.protobuf.StringValue audible_magic_item_id = 17;
}

message UpdateAudibleMagicResponsesRequest {
    // update_audible_magic_responses is the list of update audible magic responses
    repeated UpdateAudibleMagicResponseRequest update_audible_magic_responses = 1;
}
