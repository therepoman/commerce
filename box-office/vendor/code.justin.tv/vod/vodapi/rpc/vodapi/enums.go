package vodapi

import "strings"

const (
	vodStatusRecorded         = "recorded"
	vodStatusRecording        = "recording"
	vodStatusUnprocessed      = "unprocessed"
	vodStatusCreated          = "created"
	vodStatusUnderReview      = "under_review"
	vodStatusUploading        = "uploading"
	vodStatusPendingTranscode = "pending_transcode"
	vodStatusFailed           = "failed"
	vodStatusTranscoding      = "transcoding"
)

const (
	vodTypeArchive        = "archive"
	vodTypeHighlight      = "highlight"
	vodTypeUpload         = "upload"
	vodTypePastPremiere   = "past_premiere"
	vodTypePremiereUpload = "premiere_upload"
)

const (
	vodSortTime       = "time"
	vodSortTimeAsc    = "time_asc"
	vodSortViews      = "views"
	vodSortTrending   = "trending"
	vodSortTrendingV3 = "trending_v3"
)

const (
	vodCreateErrorFormatNotSupported = "FORMAT_NOT_SUPPORTED"
	vodCreateErrorBadAsset           = "BAD_ASSET"
	vodCreateErrorNotAuthorized      = "NOT_AUTHORIZED"
	vodCreateErrorBadRequest         = "BAD_REQUEST"
	vodCreateErrorInternalError      = "INTERNAL_ERROR"
)

const (
	vodViewablePublic  = "public"
	vodViewablePrivate = "private"
)

const (
	thumbnailTypeGenerated = "generated"
	thumbnailTypeCustom    = "custom"
)

const (
	notificationSettingEmail       = "email"
	notificationSettingChannelFeed = "channel_feed"
)

// ConvertVodStatus converts from rpc to internal
func ConvertVodStatus(status VodStatus) string {
	switch status {
	case VodStatus_RECORDED:
		return vodStatusRecorded
	case VodStatus_RECORDING:
		return vodStatusRecording
	case VodStatus_UNPROCESSED:
		return vodStatusUnprocessed
	case VodStatus_CREATED:
		return vodStatusCreated
	case VodStatus_UNDER_REVIEW:
		return vodStatusUnderReview
	case VodStatus_UPLOADING:
		return vodStatusUploading
	case VodStatus_PENDING_TRANSCODE:
		return vodStatusPendingTranscode
	case VodStatus_FAILED:
		return vodStatusFailed
	case VodStatus_TRANSCODING:
		return vodStatusTranscoding
	default:
		return ""
	}
}

// ConvertInternalVodStatus converts from internal to rpc
func ConvertInternalVodStatus(status string) VodStatus {
	status = strings.ToLower(status)
	switch status {
	case vodStatusRecorded:
		return VodStatus_RECORDED
	case vodStatusRecording:
		return VodStatus_RECORDING
	case vodStatusUnprocessed:
		return VodStatus_UNPROCESSED
	case vodStatusCreated:
		return VodStatus_CREATED
	case vodStatusUnderReview:
		return VodStatus_UNDER_REVIEW
	case vodStatusUploading:
		return VodStatus_UPLOADING
	case vodStatusPendingTranscode:
		return VodStatus_PENDING_TRANSCODE
	case vodStatusFailed:
		return VodStatus_FAILED
	case vodStatusTranscoding:
		return VodStatus_TRANSCODING
	default:
		return VodStatus_INVALID_STATUS
	}
}

// ConvertVodType converts from rpc to internal
func ConvertVodType(vodType VodType) string {
	switch vodType {
	case VodType_ARCHIVE:
		return vodTypeArchive
	case VodType_HIGHLIGHT:
		return vodTypeHighlight
	case VodType_UPLOAD:
		return vodTypeUpload
	case VodType_PAST_PREMIERE:
		return vodTypePastPremiere
	case VodType_PREMIERE_UPLOAD:
		return vodTypePremiereUpload
	default:
		return ""
	}
}

// ConvertVodTypes converts multiple from rpc to internal
func ConvertVodTypes(vodTypes []VodType) []string {
	var convertedTypes []string
	for _, vodType := range vodTypes {
		convertedType := ConvertVodType(vodType)
		if convertedType != "" {
			convertedTypes = append(convertedTypes, convertedType)
		}
	}
	return convertedTypes
}

// ConvertInternalVodType converts from internal to rpc
func ConvertInternalVodType(vodType string) VodType {
	vodType = strings.ToLower(vodType)
	switch vodType {
	case vodTypeArchive:
		return VodType_ARCHIVE
	case vodTypeHighlight:
		return VodType_HIGHLIGHT
	case vodTypeUpload:
		return VodType_UPLOAD
	case vodTypePastPremiere:
		return VodType_PAST_PREMIERE
	case vodTypePremiereUpload:
		return VodType_PREMIERE_UPLOAD
	default:
		return VodType_INVALID_TYPE
	}
}

// ConvertVodSort converts from rpc to internal
func ConvertVodSort(sort VodSort) string {
	switch sort {
	case VodSort_TIME:
		return vodSortTime
	case VodSort_TIME_ASC:
		return vodSortTimeAsc
	case VodSort_VIEWS:
		return vodSortViews
	case VodSort_TRENDING:
		return vodSortTrending
	case VodSort_TRENDING_V3:
		return vodSortTrendingV3
	default:
		return ""
	}
}

// ConvertInternalVodSort converts from internal to rpc
func ConvertInternalVodSort(sort string) VodSort {
	sort = strings.ToLower(sort)
	switch sort {
	case vodSortTime:
		return VodSort_TIME
	case vodSortTimeAsc:
		return VodSort_TIME_ASC
	case vodSortViews:
		return VodSort_VIEWS
	case vodSortTrending:
		return VodSort_TRENDING
	case vodSortTrendingV3:
		return VodSort_TRENDING_V3
	default:
		return VodSort_INVALID_SORT
	}
}

// ConvertVodViewable converts from rpc to internal
func ConvertVodViewable(viewable VodViewable) string {
	switch viewable {
	case VodViewable_PUBLIC:
		return vodViewablePublic
	case VodViewable_PRIVATE:
		return vodViewablePrivate
	default:
		return ""
	}
}

// ConvertInternalVodViewable converts from internal to rpc
func ConvertInternalVodViewable(viewable string) VodViewable {
	viewable = strings.ToLower(viewable)
	switch viewable {
	case vodViewablePublic:
		return VodViewable_PUBLIC
	case vodViewablePrivate:
		return VodViewable_PRIVATE
	default:
		return VodViewable_INVALID_VIEWABLE
	}
}

// ConvertVodThumbnailType converts from rpc to internal
func ConvertVodThumbnailType(thumbnailType VodThumbnailType) string {
	switch thumbnailType {
	case VodThumbnailType_GENERATED:
		return thumbnailTypeGenerated
	case VodThumbnailType_CUSTOM:
		return thumbnailTypeCustom
	default:
		return ""
	}
}

// ConvertInternalVodThumbnailType converts from internal to rpc
func ConvertInternalVodThumbnailType(thumbnailType string) VodThumbnailType {
	thumbnailType = strings.ToLower(thumbnailType)
	switch thumbnailType {
	case thumbnailTypeGenerated:
		return VodThumbnailType_GENERATED
	case thumbnailTypeCustom:
		return VodThumbnailType_CUSTOM
	default:
		return VodThumbnailType_INVALID_THUMBNAIL_TYPE
	}
}

// ConvertVodNotificationSettingType converts from rpc to internal
func ConvertVodNotificationSettingType(notificationSettingType VodNotificationSettingType) string {
	switch notificationSettingType {
	case VodNotificationSettingType_EMAIL:
		return notificationSettingEmail
	case VodNotificationSettingType_CHANNEL_FEED:
		return notificationSettingChannelFeed
	default:
		return ""
	}
}

// ConvertInternalVodNotificationSettingType converts from internal to rpc
func ConvertInternalVodNotificationSettingType(notificationSettingType string) VodNotificationSettingType {
	notificationSettingType = strings.ToLower(notificationSettingType)
	switch notificationSettingType {
	case notificationSettingEmail:
		return VodNotificationSettingType_EMAIL
	case notificationSettingChannelFeed:
		return VodNotificationSettingType_CHANNEL_FEED
	default:
		return VodNotificationSettingType_INVALID_NOTIFICATION_SETTING_TYPE
	}
}

func ConvertVodCreateError(vodCreateError VodCreateError) string {
	switch vodCreateError {
	case VodCreateError_FORMAT_NOT_SUPPORTED:
		return vodCreateErrorFormatNotSupported
	case VodCreateError_BAD_ASSET:
		return vodCreateErrorBadAsset
	case VodCreateError_NOT_AUTHORIZED:
		return vodCreateErrorNotAuthorized
	case VodCreateError_BAD_REQUEST:
		return vodCreateErrorBadRequest
	case VodCreateError_INTERNAL_ERROR:
		return vodCreateErrorInternalError
	default:
		return ""
	}
}

func ConvertInternalVodCreateError(vodCreateError string) VodCreateError {
	vodCreateError = strings.ToUpper(vodCreateError)
	switch vodCreateError {
	case vodCreateErrorFormatNotSupported:
		return VodCreateError_FORMAT_NOT_SUPPORTED
	case vodCreateErrorBadAsset:
		return VodCreateError_BAD_ASSET
	case vodCreateErrorNotAuthorized:
		return VodCreateError_NOT_AUTHORIZED
	case vodCreateErrorBadRequest:
		return VodCreateError_BAD_REQUEST
	case vodCreateErrorInternalError:
		return VodCreateError_INTERNAL_ERROR
	default:
		return VodCreateError_INVALID_CREATE_ERROR
	}
}
