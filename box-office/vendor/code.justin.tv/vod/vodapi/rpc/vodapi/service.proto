syntax = "proto3";
package vodapi;
option go_package = "vodapi";

import "responses.proto";
import "requests.proto";

service VodApi {
    // Methods related to creating a vod

    // CreateUpload will create a vod upload
    rpc CreateUpload (CreateUploadRequest) returns (CreateUploadResponse) {}

    // NOTE: in the long run this should be an infra endpoint, we are keeping it here to allow deprecating vinyl
    // without an infra dependency, but the ingest team may be reluctant to migrate twice so this api endpoint
    // might just end up being deleted and unused depending on timeline

    // CreateArchive will create a vod archive
    rpc CreateArchive (CreateArchiveRequest) returns (CreateArchiveResponse) {}

    // InternalCreateHighlight will create a highlight
    // NOTE: this endpoint is only used for an integration tests in the vod workers and should be removed when
    // that test is updated to hit visage directly.  The jira to track this is http://jira.twitch.com/browse/xxx-xxx
    rpc InternalCreateHighlight (InternalCreateHighlightRequest) returns (InternalCreateHighlightResponse) {}

    // ManagerCreateHighlight will create a highlight
    rpc ManagerCreateHighlight (ManagerCreateHighlightRequest) returns (ManagerCreateHighlightResponse) {}

    // Methods related to getting a single vod

    // PublicGetVodByID gets a single vod by id, will not get hidden or processing vods
    rpc PublicGetVodByID (PublicGetVodByIDRequest) returns (PublicGetVodByIDResponse) {}

    // InternalGetVodByID gets a single vod by id, will get hidden and processing vods
    rpc InternalGetVodByID (InternalGetVodByIDRequest) returns (InternalGetVodByIDResponse) {}

    // InternalGetVodByIDIncludeBannedUsers gets a single vod by id, will get hidden, processing, and banned user's vods
    rpc InternalGetVodByIDIncludeBannedUsers (InternalGetVodByIDIncludeBannedUsersRequest) returns (InternalGetVodByIDIncludeBannedUsersResponse) {}

    // ManagerInternalGetVodByID gets a single vod by id, will get hidden and processing vods and populate all user information
    rpc ManagerInternalGetVodByID (ManagerInternalGetVodByIDRequest) returns (ManagerInternalGetVodByIDResponse) {}

    // ManagerGetVodMuteInfo gets the muting and appeal information for a vod
    rpc ManagerGetVodMuteInfo (ManagerGetVodMuteInfoRequest) returns (ManagerGetVodMuteInfoResponse) {}

    // InternalGetVodMuteInfo gets the muting and appeal information for a vod
    rpc InternalGetVodMuteInfo (InternalGetVodMuteInfoRequest) returns (InternalGetVodMuteInfoResponse) {}

    // ManagerGetVodMuteInfos gets the muting and appeal information for vods
    rpc ManagerGetVodMuteInfos (ManagerGetVodMuteInfosRequest) returns (ManagerGetVodMuteInfosResponse) {}

    // GetVodByIDIncludeBannedUsers gets a single vod by id even if the user is banned
    rpc GetVodByIDIncludeBannedUsers (GetVodByIDIncludeBannedUsersRequest) returns (GetVodByIDIncludeBannedUsersResponse) {}

    // ManagerGetVodByID gets a single vod by id populating user information, but does not include hidden or processing vods
    rpc ManagerGetVodByID (ManagerGetVodByIDRequest) returns (ManagerGetVodByIDResponse) {}

    // Methods related to getting multiple vods

    // ManagerSearchVods searches for vods matching a search string
    rpc ManagerSearchVods (ManagerSearchVodsRequest) returns (ManagerSearchVodsResponse) {}

    // PublicGetVodsByUser gets vods by a user, omitting hidden and processing vods
    rpc PublicGetVodsByUser (PublicGetVodsByUserRequest) returns (PublicGetVodsByUserResponse) {}

    // ManagerInternalGetVodsByUser gets vods by a user, including hidden, processing and full user info
    rpc ManagerInternalGetVodsByUser (ManagerInternalGetVodsByUserRequest) returns (ManagerInternalGetVodsByUserResponse) {}

    // ManagerGetVodsByUserIncludeBannedUsers gets vods by a user even if the user is banned
    rpc ManagerGetVodsByUserIncludeBannedUsers (ManagerGetVodsByUserIncludeBannedUsersRequest) returns (ManagerGetVodsByUserIncludeBannedUsersResponse) {}

    // InternalGetVodsByBroadcastIDs gets vods by broadcast ids including hidden and processing
    rpc InternalGetVodsByBroadcastIDs (InternalGetVodsByBroadcastIDsRequest) returns (InternalGetVodsByBroadcastIDsResponse) {}

    // PublicGetVodsByIDs gets vods by ids will omit hidden and processing
    rpc PublicGetVodsByIDs (PublicGetVodsByIDsRequest) returns (PublicGetVodsByIDsResponse) {}

    // InternalGetVodsByIDs gets vods by ids including hidden, processing, and deleted
    rpc InternalGetVodsByIDs (InternalGetVodsByIDsRequest) returns (InternalGetVodsByIDsResponse) {}

    // InternalGetPrivateVodsByIDs gets vods by ids including unpublished vods
    rpc InternalGetPrivateVodsByIDs (InternalGetPrivateVodsByIDsRequest) returns (InternalGetPrivateVodsByIDsResponse) {}

    // InternalGetPrivateAndDeletedVodsByIDs gets vods by ids including unpublished and deleted vods
    rpc InternalGetPrivateAndDeletedVodsByIDs (InternalGetPrivateAndDeletedVodsByIDsRequest) returns (InternalGetPrivateAndDeletedVodsByIDsResponse) {}

    // GetVodsByIDsIncludeBannedUsers gets vods by ids even if the user is banned
    rpc GetVodsByIDsIncludeBannedUsers (GetVodsByIDsIncludeBannedUsersRequest) returns (GetVodsByIDsIncludeBannedUsersResponse) {}

    // ManagerGetVodsByUserIncludeBannedUsers gets vods by a user even if the user is banned
    rpc PublicGetVodsByUsers (PublicGetVodsByUsersRequest) returns (PublicGetVodsByUsersResponse) {}

    // GetTopVods gets the top vods for twitch
    rpc GetTopVods (GetTopVodsRequest) returns (GetTopVodsResponse) {}

    // Methods related to updating vods

    // ManagerUpdateVod updates the fields on a vod
    rpc ManagerUpdateVod (ManagerUpdateVodRequest) returns (ManagerUpdateVodResponse) {}

    // Methods related to deleting vods

    // UndeleteVods removes the deleted flag on a vod
    rpc UndeleteVods (UndeleteVodsRequest) returns (UndeleteVodsResponse) {}

    // SoftDeleteVods marks a vod as deleted
    rpc SoftDeleteVods (SoftDeleteVodsRequest) returns (SoftDeleteVodsResponse) {}

    // InternalVodRemoveRecords removes the vod records from the database
    rpc InternalVodRemoveRecords (InternalVodRemoveRecordsRequest) returns (InternalVodRemoveRecordsResponse) {}

    // HardDeleteVods removes the underlying vod manifest and ts data
    rpc HardDeleteVods (HardDeleteVodsRequest) returns (HardDeleteVodsResponse) {}

    // SoftDeleteVodsInInterval marks vods within a certain interval to be deleted
    rpc SoftDeleteVodsInInterval (SoftDeleteVodsInIntervalRequest) returns (SoftDeleteVodsInIntervalResponse) {}

    // ManagerSoftDeleteVods marks a vod as deleted if the user owns or can edit the vod
    rpc ManagerSoftDeleteVods (ManagerSoftDeleteVodsRequest) returns (ManagerSoftDeleteVodsResponse) {}

    // Methods related to actions on vods

    // YoutubeExport sends a vod to youtube
    rpc YoutubeExport (YoutubeExportRequest) returns (YoutubeExportResponse) {}

    // FinalizeUpload finishes a vod upload
    rpc FinalizeUpload (FinalizeUploadRequest) returns (FinalizeUploadResponse) {}

    // UpdateManifest updates the vod manifest
    rpc UpdateManifest (UpdateManifestRequest) returns (UpdateManifestResponse) {}

    // SetViewcounts sets the view counts for a vod
    rpc SetViewcounts (SetViewcountsRequest) returns (SetViewcountsResponse) {}

    // Methods related to vod metadata

    // GetPublicVodAggregationsByIDs gets an aggregated representation of multiple vods
    rpc GetPublicVodAggregationsByIDs (GetPublicVodAggregationsByIDsRequest) returns (PublicVodAggregationsByIDsResponse) {}

    // GetVodPopularity gets whether the vod is popular
    rpc GetVodPopularity (GetVodPopularityRequest) returns (VodPopularityResponse) {}

    // CreateError creates an error associated with a vod when uploading
    rpc CreateError (CreateErrorRequest) returns (CreateErrorResponse) {}

    // Methods related to vod thumbnails

    // CreateThumbnails creates vod thumbnails
    rpc CreateThumbnails (CreateThumbnailsRequest) returns (CreateThumbnailsResponse) {}

    // ManagerCreateCustomThumbnailUploadRequest creates a request to upload a new vod thumbnail
    rpc ManagerCreateCustomThumbnailUploadRequest (ManagerCreateCustomThumbnailUploadRequestRequest) returns (CreateCustomThumbnailUploadRequestResponse) {}

    // ManagerDeleteThumbnails deletes vod thumbnails if the user owns or can edit the vod
    rpc ManagerDeleteThumbnails (ManagerDeleteThumbnailsRequest) returns (ManagerDeleteThumbnailsResponse) {}

    // DeleteThumbnails deletes vod thumbnails
    rpc DeleteThumbnails (DeleteThumbnailsRequest) returns (DeleteThumbnailsResponse) {}

    // Methods related to vod user properties

    // GetUserVodProperties gets the user vod properties
    rpc GetUserVodProperties (GetUserVodPropertiesRequest) returns (UserVodPropertiesResponse) {}

    // ManagerGetUserVodProperties gets the user vod properties
    rpc ManagerGetUserVodProperties (ManagerGetUserVodPropertiesRequest) returns (UserVodPropertiesResponse) {}

    // ManagerGetUserVodProperties updates the user vod properties
    rpc ManagerUpdateUserVodProperties (ManagerUpdateUserVodPropertiesRequest) returns (UserVodPropertiesResponse) {}

    // ManagerGetUserVodProperties gets the user video properties
    rpc ManagerGetUserVideoPrivacyProperties (ManagerGetUserVideoPrivacyPropertiesRequest) returns (UserVideoPrivacyPropertiesResponse) {}

    // ManagerGetUserVodProperties updates the user video properties
    rpc ManagerUpdateUserVideoPrivacyProperties (ManagerUpdateUserVideoPrivacyPropertiesRequest) returns (UserVideoPrivacyPropertiesResponse) {}

    // Methods related to vod appeals

    // ManagerCreateVodAppeals creates vod appeals
    rpc ManagerCreateVodAppeals (ManagerCreateVodAppealsRequest) returns (CreateVodAppealsResponse) {}

    // GetVodAppeals gets vod appeals
    rpc GetVodAppeals (GetVodAppealsRequest) returns (VodAppealsResponse) {}

    // ResolveTrackAppeal resolves a track appeal
    rpc ResolveTrackAppeal (ResolveTrackAppealRequest) returns (ResolveTrackAppealResponse) {}

    // ResolveVodAppeal resolves a vod appeal
    rpc ResolveVodAppeal (ResolveVodAppealRequest) returns (ResolveVodAppealResponse) {}

    // CreateAudibleMagicResponses creates audible magic response objects for vods
    rpc CreateAudibleMagicResponses (CreateAudibleMagicResponsesRequest) returns (AudibleMagicResponsesResponse) {}

    // GetAudibleMagicResponses gets the audible magic response objects for vods
    rpc GetAudibleMagicResponses (GetAudibleMagicResponsesRequest) returns (AudibleMagicResponsesResponse) {}

    // UpdateAudibleMagicResponses updates the audible magic response objects for vods
    rpc UpdateAudibleMagicResponses (UpdateAudibleMagicResponsesRequest) returns (AudibleMagicResponsesResponse) {}
}
