package statsdpoolmetrics

import (
	"time"

	"code.justin.tv/chat/rediczar/poolmetrics"
)

// StatSender sends stats
type StatSender interface {
	Inc(string, int64, float32) error
	Gauge(string, int64, float32) error
	TimingDuration(string, time.Duration, float32) error
}

// Logger logs values
type Logger interface {
	Log(vals ...interface{})
}

// StatTracker tracks pool stats
type StatTracker struct {
	// Stats sends metrics. If nil, no stats are sent
	Stats StatSender
	// SampleRate of stat sending. Defaults to 1.0
	SampleRate float32
	// Logger logs values. If nil, logging is off
	Logger Logger
}

// Track pool stats
func (s *StatTracker) Track(stats poolmetrics.Stats) {
	s.inc("pool.hits", stats.Hits)
	s.inc("pool.misses", stats.Misses)
	s.inc("pool.timeouts", stats.Timeouts)
	s.inc("pool.stale_conns", stats.StaleConns)

	s.gauge("pool.total_conns", stats.TotalConns)
	s.gauge("pool.idle_conns", stats.IdleConns)
}

func (s *StatTracker) inc(stat string, val int64) {
	if s.Stats == nil {
		return
	}

	if err := s.Stats.Inc(stat, val, s.sampleRate()); err != nil {
		s.log("err", err, "incrementing pool stat")
	}
}

func (s *StatTracker) gauge(stat string, val int64) {
	if s.Stats == nil {
		return
	}

	if err := s.Stats.Gauge(stat, val, s.sampleRate()); err != nil {
		s.log("err", err, "gauging pool stat")
	}
}

func (s *StatTracker) sampleRate() float32 {
	if s.SampleRate <= 0 {
		return 1.0
	}

	return s.SampleRate
}

func (s *StatTracker) log(vals ...interface{}) {
	if s.Logger != nil {
		s.Logger.Log(vals...)
	}
}
