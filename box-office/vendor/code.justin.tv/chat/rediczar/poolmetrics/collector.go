package poolmetrics

import (
	"errors"
	"time"

	"github.com/go-redis/redis/v7"
)

// Stats contains metrics during one collection cycle
type Stats struct {
	// Hits is the number of times a free connection was found in the pool since the previous collection
	Hits int64
	// Misses is the number of times a free connection was NOT found since the previous collection
	Misses int64
	// Timeouts is the number of wait timeouts since the previous collection
	Timeouts int64
	// TotalConns is the number of connections in the pool
	TotalConns int64
	// IdleConns is the number of idle connections in the pool
	IdleConns int64
	// StaleConns is the number of stale connections removed from the pool
	StaleConns int64
}

// StatTracker tracks pool stats
type StatTracker interface {
	// Track pool stats
	Track(stats Stats)
}

// Client provides pool stats
type Client interface {
	PoolStats() *redis.PoolStats
}

// Collector collects and tracks cluster client pool stats
type Collector struct {
	// Client provides pool stats. This must be set.
	Client Client
	// StatTracker tracks pool metrics. If nil, no stats are tracked
	StatTracker StatTracker
	// Interval is how often to track stats . Defaults to 5 seconds
	Interval time.Duration

	onDone  chan struct{}
	onClose chan struct{}
}

// Setup initializes
func (c *Collector) Setup() error {
	if c.Client == nil {
		return errors.New("missing Client")
	}

	c.onDone = make(chan struct{})
	c.onClose = make(chan struct{})
	return nil
}

// Start collecting pool metrics
func (c *Collector) Start() error {
	defer close(c.onDone)

	ticker := time.NewTicker(c.interval())
	defer ticker.Stop()

	pstats := c.Client.PoolStats()

	for {
		select {
		case <-ticker.C:
			pstats = c.track(pstats)
		case <-c.onClose:
			return nil
		}
	}
}

// Close stops collecting pool metrics
func (c *Collector) Close() error {
	close(c.onClose)
	<-c.onDone
	return nil
}

func (c *Collector) interval() time.Duration {
	if c.Interval <= 0 {
		return 5 * time.Second
	}

	return c.Interval
}

func (c *Collector) track(previous *redis.PoolStats) *redis.PoolStats {
	pstats := c.Client.PoolStats()

	// Avoid unsigned overflow. This can happen on node failover where the pool stats for
	// nodes reset.
	if previous.Hits > pstats.Hits ||
		previous.Misses > pstats.Misses ||
		previous.Timeouts > pstats.Timeouts ||
		previous.StaleConns > pstats.StaleConns {
		return pstats
	}

	stats := Stats{
		Hits:       int64(pstats.Hits - previous.Hits),
		Misses:     int64(pstats.Misses - previous.Misses),
		Timeouts:   int64(pstats.Timeouts - previous.Timeouts),
		StaleConns: int64(pstats.StaleConns - previous.StaleConns),
		TotalConns: int64(pstats.TotalConns),
		IdleConns:  int64(pstats.IdleConns),
	}

	if c.StatTracker != nil {
		c.StatTracker.Track(stats)
	}

	return pstats
}
