module code.justin.tv/chat/rediczar

go 1.12

require (
	code.justin.tv/chat/ratecache v1.0.0
	github.com/alicebob/gopher-json v0.0.0-20180125190556-5a6b3ba71ee6 // indirect
	github.com/alicebob/miniredis v2.5.0+incompatible
	github.com/cactus/go-statsd-client/statsd v0.0.0-20190805010426-5089fcbbe532
	github.com/go-redis/redis/v7 v7.0.0-beta.4
	github.com/gomodule/redigo v2.0.0+incompatible // indirect
	github.com/kisielk/errcheck v1.2.0
	github.com/maxbrunsfeld/counterfeiter/v6 v6.2.2
	github.com/stretchr/testify v1.4.0
	github.com/yuin/gopher-lua v0.0.0-20190514113301-1cd887cd7036 // indirect
	golang.org/x/lint v0.0.0-20190909230951-414d861bb4ac
	golang.org/x/time v0.0.0-20190308202827-9d24e82272b4
	golang.org/x/tools v0.0.0-20190815235612-5b08f89bfc0c
	honnef.co/go/tools v0.0.1-2019.2.3
)

replace github.com/go-redis/redis/v7 => code.justin.tv/chat/goredis/v7 v7.0.0-beta.4
