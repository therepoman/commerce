package manager

import (
	"code.justin.tv/systems/sandstorm/resource"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
)

// AWSConfig returns a config with an assume role chain
func AWSConfig(creds *credentials.Credentials, roleArns ...string) *aws.Config {
	return resource.AWSConfig(creds, roleArns...)
}
