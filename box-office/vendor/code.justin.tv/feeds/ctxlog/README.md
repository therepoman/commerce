#ctxlog

ctxlog manages request scoped logging variables across many servers. It provides

- Support for maintaining a request ID.
- A configuration for elevating logs to a higher log level.
- A Ctxhandler which reads incoming headers and annotate the request's context appropriately.
- A LoggedDoer which wraps an HTTP request with headers based on the request's context.

# Request ID

A Ctxhandler will get its request ID from, in order of preference

- A function which will fetch the ID from the context
- The configured Ctxlog header
- A new random ID
