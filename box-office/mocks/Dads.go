// Code generated by mockery v1.0.0. DO NOT EDIT.
package mocks

import code_justin_tv_ads_dads_proto "code.justin.tv/ads/dads/proto"
import context "context"
import mock "github.com/stretchr/testify/mock"

// Dads is an autogenerated mock type for the Dads type
type Dads struct {
	mock.Mock
}

// GetAdblockStatus provides a mock function with given fields: _a0, _a1
func (_m *Dads) GetAdblockStatus(_a0 context.Context, _a1 *code_justin_tv_ads_dads_proto.GetAdblockStatusReq) (*code_justin_tv_ads_dads_proto.GetAdblockStatusResp, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *code_justin_tv_ads_dads_proto.GetAdblockStatusResp
	if rf, ok := ret.Get(0).(func(context.Context, *code_justin_tv_ads_dads_proto.GetAdblockStatusReq) *code_justin_tv_ads_dads_proto.GetAdblockStatusResp); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*code_justin_tv_ads_dads_proto.GetAdblockStatusResp)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *code_justin_tv_ads_dads_proto.GetAdblockStatusReq) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ShowAds provides a mock function with given fields: _a0, _a1
func (_m *Dads) ShowAds(_a0 context.Context, _a1 *code_justin_tv_ads_dads_proto.ShowAdsInput) (*code_justin_tv_ads_dads_proto.ShowAdsResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *code_justin_tv_ads_dads_proto.ShowAdsResponse
	if rf, ok := ret.Get(0).(func(context.Context, *code_justin_tv_ads_dads_proto.ShowAdsInput) *code_justin_tv_ads_dads_proto.ShowAdsResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*code_justin_tv_ads_dads_proto.ShowAdsResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *code_justin_tv_ads_dads_proto.ShowAdsInput) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ShowServerAds provides a mock function with given fields: _a0, _a1
func (_m *Dads) ShowServerAds(_a0 context.Context, _a1 *code_justin_tv_ads_dads_proto.ShowServerAdsReq) (*code_justin_tv_ads_dads_proto.ShowServerAdsResp, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *code_justin_tv_ads_dads_proto.ShowServerAdsResp
	if rf, ok := ret.Get(0).(func(context.Context, *code_justin_tv_ads_dads_proto.ShowServerAdsReq) *code_justin_tv_ads_dads_proto.ShowServerAdsResp); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*code_justin_tv_ads_dads_proto.ShowServerAdsResp)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *code_justin_tv_ads_dads_proto.ShowServerAdsReq) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
