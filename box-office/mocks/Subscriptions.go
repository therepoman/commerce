// Code generated by mockery v1.0.0
package mocks

import context "golang.org/x/net/context"
import mock "github.com/stretchr/testify/mock"
import substwirp "code.justin.tv/revenue/subscriptions/twirp"

// Subscriptions is an autogenerated mock type for the Subscriptions type
type Subscriptions struct {
	mock.Mock
}

// CalculateSubscriberCount provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) CalculateSubscriberCount(_a0 context.Context, _a1 *substwirp.CalculateSubscriberCountRequest) (*substwirp.CalculateSubscriberCountResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.CalculateSubscriberCountResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.CalculateSubscriberCountRequest) *substwirp.CalculateSubscriberCountResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.CalculateSubscriberCountResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.CalculateSubscriberCountRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// CanGift provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) CanGift(_a0 context.Context, _a1 *substwirp.CanGiftRequest) (*substwirp.CanGiftResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.CanGiftResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.CanGiftRequest) *substwirp.CanGiftResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.CanGiftResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.CanGiftRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// CanSubscribe provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) CanSubscribe(_a0 context.Context, _a1 *substwirp.CanSubscribeRequest) (*substwirp.CanSubscribeResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.CanSubscribeResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.CanSubscribeRequest) *substwirp.CanSubscribeResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.CanSubscribeResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.CanSubscribeRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// CancelGift provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) CancelGift(_a0 context.Context, _a1 *substwirp.CancelGiftRequest) (*substwirp.CancelGiftResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.CancelGiftResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.CancelGiftRequest) *substwirp.CancelGiftResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.CancelGiftResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.CancelGiftRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// CancelPaidUpgrade provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) CancelPaidUpgrade(_a0 context.Context, _a1 *substwirp.CancelPaidUpgradeRequest) (*substwirp.CancelPaidUpgradeResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.CancelPaidUpgradeResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.CancelPaidUpgradeRequest) *substwirp.CancelPaidUpgradeResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.CancelPaidUpgradeResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.CancelPaidUpgradeRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// CancelSubscription provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) CancelSubscription(_a0 context.Context, _a1 *substwirp.CancelSubscriptionRequest) (*substwirp.CancelSubscriptionResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.CancelSubscriptionResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.CancelSubscriptionRequest) *substwirp.CancelSubscriptionResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.CancelSubscriptionResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.CancelSubscriptionRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// CreateEmoticon provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) CreateEmoticon(_a0 context.Context, _a1 *substwirp.CreateEmoticonRequest) (*substwirp.CreateEmoticonReponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.CreateEmoticonReponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.CreateEmoticonRequest) *substwirp.CreateEmoticonReponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.CreateEmoticonReponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.CreateEmoticonRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// CreateGift provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) CreateGift(_a0 context.Context, _a1 *substwirp.CreateGiftRequest) (*substwirp.CreateGiftResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.CreateGiftResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.CreateGiftRequest) *substwirp.CreateGiftResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.CreateGiftResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.CreateGiftRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// CreatePaidUpgrade provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) CreatePaidUpgrade(_a0 context.Context, _a1 *substwirp.CreatePaidUpgradeRequest) (*substwirp.CreatePaidUpgradeResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.CreatePaidUpgradeResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.CreatePaidUpgradeRequest) *substwirp.CreatePaidUpgradeResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.CreatePaidUpgradeResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.CreatePaidUpgradeRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// FulfillSubscription provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) FulfillSubscription(_a0 context.Context, _a1 *substwirp.FulfillSubscriptionRequest) (*substwirp.FulfillSubscriptionResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.FulfillSubscriptionResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.FulfillSubscriptionRequest) *substwirp.FulfillSubscriptionResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.FulfillSubscriptionResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.FulfillSubscriptionRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetChannelProducts provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) GetChannelProducts(_a0 context.Context, _a1 *substwirp.GetChannelProductsRequest) (*substwirp.GetChannelProductsResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.GetChannelProductsResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.GetChannelProductsRequest) *substwirp.GetChannelProductsResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.GetChannelProductsResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.GetChannelProductsRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetEmotePrefix provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) GetEmotePrefix(_a0 context.Context, _a1 *substwirp.GetEmotePrefixRequest) (*substwirp.GetEmotePrefixResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.GetEmotePrefixResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.GetEmotePrefixRequest) *substwirp.GetEmotePrefixResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.GetEmotePrefixResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.GetEmotePrefixRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetExternalProductPurchasableSKU provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) GetExternalProductPurchasableSKU(_a0 context.Context, _a1 *substwirp.GetExternalProductPurchasableSKURequest) (*substwirp.GetExternalProductPurchasableSKUResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.GetExternalProductPurchasableSKUResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.GetExternalProductPurchasableSKURequest) *substwirp.GetExternalProductPurchasableSKUResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.GetExternalProductPurchasableSKUResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.GetExternalProductPurchasableSKURequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetExternalProductTemplateSKU provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) GetExternalProductTemplateSKU(_a0 context.Context, _a1 *substwirp.GetExternalProductTemplateSKURequest) (*substwirp.GetExternalProductTemplateSKUResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.GetExternalProductTemplateSKUResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.GetExternalProductTemplateSKURequest) *substwirp.GetExternalProductTemplateSKUResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.GetExternalProductTemplateSKUResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.GetExternalProductTemplateSKURequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetGiftPromotion provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) GetGiftPromotion(_a0 context.Context, _a1 *substwirp.GetGiftPromotionRequest) (*substwirp.GetGiftPromotionResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.GetGiftPromotionResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.GetGiftPromotionRequest) *substwirp.GetGiftPromotionResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.GetGiftPromotionResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.GetGiftPromotionRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetGifts provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) GetGifts(_a0 context.Context, _a1 *substwirp.GetGiftsRequest) (*substwirp.GetGiftsResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.GetGiftsResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.GetGiftsRequest) *substwirp.GetGiftsResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.GetGiftsResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.GetGiftsRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetPaidUpgrades provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) GetPaidUpgrades(_a0 context.Context, _a1 *substwirp.GetPaidUpgradesRequest) (*substwirp.GetPaidUpgradesResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.GetPaidUpgradesResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.GetPaidUpgradesRequest) *substwirp.GetPaidUpgradesResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.GetPaidUpgradesResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.GetPaidUpgradesRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetProductsByIDs provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) GetProductsByIDs(_a0 context.Context, _a1 *substwirp.GetProductsByIDsRequest) (*substwirp.GetProductsByIDsResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.GetProductsByIDsResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.GetProductsByIDsRequest) *substwirp.GetProductsByIDsResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.GetProductsByIDsResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.GetProductsByIDsRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetUserChannelSubscription provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) GetUserChannelSubscription(_a0 context.Context, _a1 *substwirp.GetUserChannelSubscriptionRequest) (*substwirp.GetUserChannelSubscriptionResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.GetUserChannelSubscriptionResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.GetUserChannelSubscriptionRequest) *substwirp.GetUserChannelSubscriptionResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.GetUserChannelSubscriptionResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.GetUserChannelSubscriptionRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetUserChannelSubscriptionWithPaymentsData provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) GetUserChannelSubscriptionWithPaymentsData(_a0 context.Context, _a1 *substwirp.GetUserChannelSubscriptionWithPaymentsDataRequest) (*substwirp.GetUserChannelSubscriptionWithPaymentsDataResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.GetUserChannelSubscriptionWithPaymentsDataResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.GetUserChannelSubscriptionWithPaymentsDataRequest) *substwirp.GetUserChannelSubscriptionWithPaymentsDataResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.GetUserChannelSubscriptionWithPaymentsDataResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.GetUserChannelSubscriptionWithPaymentsDataRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetUserSettings provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) GetUserSettings(_a0 context.Context, _a1 *substwirp.GetUserSettingsRequest) (*substwirp.GetUserSettingsResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.GetUserSettingsResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.GetUserSettingsRequest) *substwirp.GetUserSettingsResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.GetUserSettingsResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.GetUserSettingsRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetUserSubscriptions provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) GetUserSubscriptions(_a0 context.Context, _a1 *substwirp.GetUserSubscriptionsRequest) (*substwirp.GetUserSubscriptionsResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.GetUserSubscriptionsResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.GetUserSubscriptionsRequest) *substwirp.GetUserSubscriptionsResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.GetUserSubscriptionsResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.GetUserSubscriptionsRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ListUserSubscriptions provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) ListUserSubscriptions(_a0 context.Context, _a1 *substwirp.ListUserSubscriptionsRequest) (*substwirp.ListUserSubscriptionsResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.ListUserSubscriptionsResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.ListUserSubscriptionsRequest) *substwirp.ListUserSubscriptionsResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.ListUserSubscriptionsResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.ListUserSubscriptionsRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// SetScore provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) SetScore(_a0 context.Context, _a1 *substwirp.SetScoreRequest) (*substwirp.SetScoreResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.SetScoreResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.SetScoreRequest) *substwirp.SetScoreResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.SetScoreResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.SetScoreRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// SetUserSettings provides a mock function with given fields: _a0, _a1
func (_m *Subscriptions) SetUserSettings(_a0 context.Context, _a1 *substwirp.SetUserSettingsRequest) (*substwirp.GetUserSettingsResponse, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *substwirp.GetUserSettingsResponse
	if rf, ok := ret.Get(0).(func(context.Context, *substwirp.SetUserSettingsRequest) *substwirp.GetUserSettingsResponse); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*substwirp.GetUserSettingsResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *substwirp.SetUserSettingsRequest) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
