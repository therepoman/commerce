// Code generated by mockery v1.0.0
package mocks

import channels "code.justin.tv/web/users-service/client/channels"
import context "context"
import mock "github.com/stretchr/testify/mock"
import models "code.justin.tv/web/users-service/models"
import twitchclient "code.justin.tv/foundation/twitchclient"

// Client is an autogenerated mock type for the Client type
type Client struct {
	mock.Mock
}

// Get provides a mock function with given fields: ctx, id, reqOpts
func (_m *Client) Get(ctx context.Context, id string, reqOpts *twitchclient.ReqOpts) (*channels.Channel, error) {
	ret := _m.Called(ctx, id, reqOpts)

	var r0 *channels.Channel
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchclient.ReqOpts) *channels.Channel); ok {
		r0 = rf(ctx, id, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*channels.Channel)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, id, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetAll provides a mock function with given fields: ctx, ids, reqOpts
func (_m *Client) GetAll(ctx context.Context, ids []string, reqOpts *twitchclient.ReqOpts) (*channels.ChannelsResult, error) {
	ret := _m.Called(ctx, ids, reqOpts)

	var r0 *channels.ChannelsResult
	if rf, ok := ret.Get(0).(func(context.Context, []string, *twitchclient.ReqOpts) *channels.ChannelsResult); ok {
		r0 = rf(ctx, ids, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*channels.ChannelsResult)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, []string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, ids, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetAllByLogin provides a mock function with given fields: ctx, logins, reqOpts
func (_m *Client) GetAllByLogin(ctx context.Context, logins []string, reqOpts *twitchclient.ReqOpts) (*channels.ChannelsResult, error) {
	ret := _m.Called(ctx, logins, reqOpts)

	var r0 *channels.ChannelsResult
	if rf, ok := ret.Get(0).(func(context.Context, []string, *twitchclient.ReqOpts) *channels.ChannelsResult); ok {
		r0 = rf(ctx, logins, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*channels.ChannelsResult)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, []string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, logins, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetAllByLoginAndParams provides a mock function with given fields: ctx, logins, params, reqOpts
func (_m *Client) GetAllByLoginAndParams(ctx context.Context, logins []string, params *models.ChannelFilterParams, reqOpts *twitchclient.ReqOpts) (*channels.ChannelsResult, error) {
	ret := _m.Called(ctx, logins, params, reqOpts)

	var r0 *channels.ChannelsResult
	if rf, ok := ret.Get(0).(func(context.Context, []string, *models.ChannelFilterParams, *twitchclient.ReqOpts) *channels.ChannelsResult); ok {
		r0 = rf(ctx, logins, params, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*channels.ChannelsResult)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, []string, *models.ChannelFilterParams, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, logins, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetAllByParams provides a mock function with given fields: ctx, ids, params, reqOpts
func (_m *Client) GetAllByParams(ctx context.Context, ids []string, params *models.ChannelFilterParams, reqOpts *twitchclient.ReqOpts) (*channels.ChannelsResult, error) {
	ret := _m.Called(ctx, ids, params, reqOpts)

	var r0 *channels.ChannelsResult
	if rf, ok := ret.Get(0).(func(context.Context, []string, *models.ChannelFilterParams, *twitchclient.ReqOpts) *channels.ChannelsResult); ok {
		r0 = rf(ctx, ids, params, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*channels.ChannelsResult)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, []string, *models.ChannelFilterParams, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, ids, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetByIDAndParams provides a mock function with given fields: ctx, id, params, reqOpts
func (_m *Client) GetByIDAndParams(ctx context.Context, id string, params *models.ChannelFilterParams, reqOpts *twitchclient.ReqOpts) (*channels.Channel, error) {
	ret := _m.Called(ctx, id, params, reqOpts)

	var r0 *channels.Channel
	if rf, ok := ret.Get(0).(func(context.Context, string, *models.ChannelFilterParams, *twitchclient.ReqOpts) *channels.Channel); ok {
		r0 = rf(ctx, id, params, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*channels.Channel)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *models.ChannelFilterParams, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, id, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetByLogin provides a mock function with given fields: ctx, login, reqOpts
func (_m *Client) GetByLogin(ctx context.Context, login string, reqOpts *twitchclient.ReqOpts) (*channels.Channel, error) {
	ret := _m.Called(ctx, login, reqOpts)

	var r0 *channels.Channel
	if rf, ok := ret.Get(0).(func(context.Context, string, *twitchclient.ReqOpts) *channels.Channel); ok {
		r0 = rf(ctx, login, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*channels.Channel)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, login, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetByLoginAndParams provides a mock function with given fields: ctx, login, params, reqOpts
func (_m *Client) GetByLoginAndParams(ctx context.Context, login string, params *models.ChannelFilterParams, reqOpts *twitchclient.ReqOpts) (*channels.Channel, error) {
	ret := _m.Called(ctx, login, params, reqOpts)

	var r0 *channels.Channel
	if rf, ok := ret.Get(0).(func(context.Context, string, *models.ChannelFilterParams, *twitchclient.ReqOpts) *channels.Channel); ok {
		r0 = rf(ctx, login, params, reqOpts)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*channels.Channel)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string, *models.ChannelFilterParams, *twitchclient.ReqOpts) error); ok {
		r1 = rf(ctx, login, params, reqOpts)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Set provides a mock function with given fields: ctx, up, reqOpts
func (_m *Client) Set(ctx context.Context, up models.UpdateChannelProperties, reqOpts *twitchclient.ReqOpts) error {
	ret := _m.Called(ctx, up, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, models.UpdateChannelProperties, *twitchclient.ReqOpts) error); ok {
		r0 = rf(ctx, up, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// SetChannelWithAuth provides a mock function with given fields: ctx, up, editor, reqOpts
func (_m *Client) SetChannelWithAuth(ctx context.Context, up models.UpdateChannelProperties, editor string, reqOpts *twitchclient.ReqOpts) error {
	ret := _m.Called(ctx, up, editor, reqOpts)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, models.UpdateChannelProperties, string, *twitchclient.ReqOpts) error); ok {
		r0 = rf(ctx, up, editor, reqOpts)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
