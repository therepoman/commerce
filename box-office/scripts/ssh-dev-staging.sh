#!/bin/bash

# .ebextensions are in deploy
pushd deploy
eb ssh --profile twitch-box-office-dev staging-commerce-box-office-env
popd deploy
