#!/bin/bash

# .ebextensions are in deploy
pushd deploy
eb ssh --profile twitch-box-office-aws prod-commerce-box-office-env
popd deploy
