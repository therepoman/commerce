#! /usr/bin/python

import requests
import demjson
import sys
import urllib

CLIENT_ID = 'ltoxwr5a0sxj4ug7zvgjnl28gtpasym'

TOKEN = '{\"chansub\":{\"restricted_bitrates\":[]},\"expires\":1524937924,\"https_required\":true,\"privileged\":false,\"user_id\":145809986,\"Version\":2,\"vod_id\":253811533}'
SIG = '7bca30fe809c6e155b603afc836592f489decf2d'

# Video
VALIDATE_VIDEO = True
CHANNEL = 'ninja'
USHER_VIDEO_ENDPOINT = 'http://usher.ttvnw.net/api/channel/hls/%s.m3u8?allow_spectre=true&allow_audio_only=true&allow_source=true&type=any&token=%s&sig=%s'

# VOD
VALIDATE_VOD = False
VOD_ID = '253811533'
USHER_VOD_ENDPOINT = 'http://usher.ttvnw.net/vod/%s.m3u8?allow_spectre=true&allow_audio_only=true&allow_source=true&type=any&token=%s&sig=%s'

def call_usher(usher_endpoint, channel):
        url = usher_endpoint % (channel, TOKEN, SIG)
        headers = {'X-Source-Cluster': 'sfo01'}
        r = requests.get(url, headers=headers)

        print 'url: ' + url
        print r
        print r.text
        print r.status_code
        if r.status_code == 200:
                print 'TOKEN SUCCEEDED'
        else:
                print 'TOKEN FAILED'


# Simple script for plugging a token+sig into usher to make sure it works
# Note that Usher returns 404 if the stream is offline, so make sure you use a channel
# that is online.
#
# usage: 
# $ python simple_response_validator.py
print 'Validating Box Office token'
print 'TOKEN: ' + TOKEN
print 'SIG: ' + SIG

if VALIDATE_VOD:
        print 'Validating VOD'
        call_usher(USHER_VOD_ENDPOINT, VOD_ID)

if VALIDATE_VIDEO:
        print 'Validating Video'
        call_usher(USHER_VIDEO_ENDPOINT, CHANNEL)

