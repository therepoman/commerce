#!/bin/bash

# .ebextensions are in deploy
pushd deploy
eb ssh --profile twitch-box-office-dev dev-commerce-box-office-env
popd deploy
