#!/bin/bash

OUT="$(pwd)/output"

mkdir -p ${OUT}
echo 'building ...'
go build $(glide novendor)
echo 'vetting ...'
go vet $(glide novendor)
echo 'errchecking ...'
for pkg in $(glide novendor); do
  echo "golinting $pkg ..."
  golint $pkg
done
echo 'testing ...'
go test -race $(glide novendor)
CMD_PKGS=$(go list -f '{{if eq .Name "main"}}{{.ImportPath}}{{end}}' -- $(glide novendor))
for cmd in $CMD_PKGS; do
  echo "installing $cmd ..."
  GOBIN="${OUT}" go install "${cmd}"
done
