#!/bin/bash

go get github.com/sgotti/glide-vc
go get github.com/FiloSottile/vendorcheck

glide-vc --only-code --no-tests
vendorcheck ./...
