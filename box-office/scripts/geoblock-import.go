package main

import (
	"bufio"
	"bytes"
	"encoding/csv"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"reflect"
	"strings"
	"time"

	"code.justin.tv/commerce/box-office/internal/geoblock"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials/processcreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"

	"github.com/nsf/jsondiff"
	"github.com/pkg/errors"
)

func main() {
	// Input validation
	inputFile := flag.String("inputCSV", "", "path to the CSV file that contains the roster data")
	originalDynamicConfigFile := flag.String("inputJSON", "", "path to the existing geoblock.json")
	outputFile := flag.String("outputJSON", "", "path to output file")
	env := flag.String("env", "dev", "prod or dev environment (defaults to dev)")
	flag.Parse()

	if *inputFile == "" {
		fmt.Fprintf(os.Stderr, "Must include a -inputCSV flag\n")
		os.Exit(1)
	}

	var err error
	var sess *session.Session

	// Only make an AWS session if needed
	if !(*originalDynamicConfigFile != "" && *outputFile != "") {
		sess, err = generateAWSSession(*env)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Could not generate AWS session %v\n", err.Error())
			os.Exit(1)
		}
	}

	// Download current geoblock from S3
	var originalDynamicConfig []byte
	originalGeoblocks := map[string]geoblock.Geoblock{}

	if *originalDynamicConfigFile != "" {
		originalGeoblocksJSON, err := os.Open(*originalDynamicConfigFile)
		defer originalGeoblocksJSON.Close()
		if err != nil {
			fmt.Fprintf(os.Stderr, "Could not open inputJSON file %v\n", err.Error())
			os.Exit(1)
		}

		originalDynamicConfig, err = ioutil.ReadAll(originalGeoblocksJSON)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Could not read inputJSON file %v\n", err.Error())
			os.Exit(1)
		}
	} else {
		originalDynamicConfig, err = downloadDynamicConfig(*env, sess)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Could not download dynamic config %v\n", err.Error())
			os.Exit(1)
		}
	}
	originalGeoblocks = unmarshalGeoblockConfig(originalDynamicConfig)

	// Load new CSV
	file, err := os.Open(*inputFile)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not open input file %v\n", err.Error())
		os.Exit(1)
	}

	// Parse CSV entries into new geoblocks
	newGeoblocks, unprocessedLines := parseCSV(file)
	if unprocessedLines != nil {
		fmt.Fprintf(os.Stderr, "There were unprocessed lines in the CSV\n")
		for _, line := range unprocessedLines {
			fmt.Fprintf(os.Stderr, "%v", line)
		}
		os.Exit(1)
	}

	// Delete expired entries
	newGeoblocks = removeExpiredGeoblocks(newGeoblocks)
	originalGeoblocks = removeExpiredGeoblocks(originalGeoblocks)

	// Upsert new entries
	mergedGeoblocks, err := mergeGeoblockMap(originalGeoblocks, newGeoblocks)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed merging geoblocks %v\n", err.Error())
		os.Exit(1)
	}
	// Dedupe timeWindows
	mergedGeoblocks = dedupeTimeWindows(mergedGeoblocks)

	// Create new DynamicConfig
	updatedDynamicConfig := map[string]map[string]geoblock.Geoblock{
		"GeoblockedChannels": mergedGeoblocks,
	}

	// Convert merged geoblocks to JSON
	updatedDynamicConfigJSON, err := json.MarshalIndent(updatedDynamicConfig, "", "  ")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error marshaling merged geoblocks %v\n", err.Error())
		os.Exit(1)
	}

	// Display diff
	diffOptions := jsondiff.DefaultConsoleOptions()
	diffType, formattedDiff := jsondiff.Compare(originalDynamicConfig, updatedDynamicConfigJSON, &diffOptions)
	switch diffType {
	case jsondiff.FirstArgIsInvalidJson, jsondiff.SecondArgIsInvalidJson, jsondiff.BothArgsAreInvalidJson:
		fmt.Fprintf(os.Stderr, "Error performing diff %v\n", diffType)
		os.Exit(1)
	}
	fmt.Println(formattedDiff)

	// Confirm diff looks good
	if !confirm("Examine the diff. Proceed?") {
		os.Exit(0)
	}

	// Write to file if requested
	if *outputFile != "" {
		writeOutput(*outputFile, updatedDynamicConfigJSON)
	} else if confirm("Upload to " + *env + "?") {
		err = uploadDynamicConfig(*env, sess, updatedDynamicConfigJSON)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error uploading dynamic config %v\n", diffType)
			os.Exit(1)
		}

		fmt.Println("Upload complete")
	}

	os.Exit(0)
}

func generateAWSSession(env string) (*session.Session, error) {
	fmt.Println("Generating AWS session for", env, "environment")
	account := "664665898521"
	if env == "prod" {
		account = "964932369655"
	}

	isengardScriptLocation, err := exec.LookPath("isengard_credentials")
	if err != nil {
		return nil, err
	}

	creds := processcreds.NewCredentials(isengardScriptLocation + " --account-id " + account + " --role Admin")

	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String("us-west-2"),
		Credentials: creds,
	})
	if err != nil {
		return nil, err
	}

	return sess, err
}

func downloadDynamicConfig(env string, sess *session.Session) ([]byte, error) {
	buff := &aws.WriteAtBuffer{}
	downloader := s3manager.NewDownloader(sess)

	bucket := "box-office-dev-geoblocking"
	if env == "prod" {
		bucket = "box-office-prod-geoblocking"
	}

	_, err := downloader.Download(buff,
		&s3.GetObjectInput{
			Bucket: aws.String(bucket),
			Key:    aws.String("geoblock.json"),
		})

	return buff.Bytes(), err
}

func uploadDynamicConfig(env string, sess *session.Session, dynamicConfig []byte) error {
	uploader := s3manager.NewUploader(sess)

	bucket := "box-office-dev-geoblocking"
	if env == "prod" {
		bucket = "box-office-prod-geoblocking"
	}

	_, err := uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(bucket),
		Key:    aws.String("geoblock.json"),
		Body:   bytes.NewReader(dynamicConfig),
	})

	return err
}

func unmarshalGeoblockConfig(byteArray []byte) map[string]geoblock.Geoblock {
	intermediate := map[string]map[string]geoblock.Geoblock{}

	json.Unmarshal(byteArray, &intermediate)

	return intermediate["GeoblockedChannels"]
}

func parseCSV(file *os.File) (map[string]geoblock.Geoblock, [][]string) {
	output := map[string]geoblock.Geoblock{}
	var unprocessedLines [][]string

	// initialize csv reader
	reader := csv.NewReader(bufio.NewReader(file))

	channelNameHeader := "channelname"
	startTimeHeader := "isostarttime"
	endTimeHeader := "isoendtime"
	allowlistCountriesHeader := "allowlistedcountries"
	blocklistCountriesHeader := "blocklistedcountries"
	allowlistPlatformsHeader := "allowlistedplatforms"
	blocklistZipcodesHeader := "blocklistedzipcodes"
	// Old language
	whitelistCountriesHeader := "whitelistedcountries"
	blacklistCountriesHeader := "blacklistedcountries"
	whitelistPlatformsHeader := "whitelistedplatforms"
	blacklistZipcodesHeader := "blacklistedzipcodes"

	channelNameIndex := -1
	startTimeIndex := -1
	endTimeIndex := -1
	allowlistCountriesIndex := -1
	blocklistCountriesIndex := -1
	allowlistPlatformsIndex := -1
	blocklistZipcodesIndex := -1

	// Do validation and header assignment
	headers, err := reader.Read()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not read from input file %v\n", err.Error())
		os.Exit(1)
	}

	for index, header := range headers {
		switch lowerHeader := strings.ToLower(header); lowerHeader {
		case channelNameHeader:
			channelNameIndex = index
		case startTimeHeader:
			startTimeIndex = index
		case endTimeHeader:
			endTimeIndex = index
		case allowlistCountriesHeader, whitelistCountriesHeader:
			allowlistCountriesIndex = index
		case blocklistCountriesHeader, blacklistCountriesHeader:
			blocklistCountriesIndex = index
		case allowlistPlatformsHeader, whitelistPlatformsHeader:
			allowlistPlatformsIndex = index
		case blocklistZipcodesHeader, blacklistZipcodesHeader:
			blocklistZipcodesIndex = index
		}
	}

	// Check for required ChannelID, StartTime and EndTime
	if channelNameIndex < 0 || startTimeIndex < 0 || endTimeIndex < 0 {
		fmt.Fprintf(os.Stderr, "Missing required headers: %v %v %v\n", channelNameHeader, startTimeHeader, endTimeHeader)
		os.Exit(1)
	}

	// Check for some restriction definition
	if allowlistCountriesIndex < 0 && blocklistCountriesIndex < 0 && allowlistPlatformsIndex < 0 && blocklistZipcodesIndex < 0 {
		fmt.Fprintf(os.Stderr, "Must have at least one restriction header: %v %v %v %v\n", allowlistCountriesHeader, blocklistCountriesHeader, allowlistPlatformsHeader, blocklistZipcodesHeader)
		os.Exit(1)
	}

	// Read content
	for {
		line, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			fmt.Fprintf(os.Stderr, "Could not read from input file %v\n", err.Error())
			os.Exit(1)
		}

		emptyRow := true
		for _, v := range line {
			if v != "" {
				emptyRow = false
				break
			}
		}
		if emptyRow {
			fmt.Fprintf(os.Stderr, "Skipping empty line %v\n", line)
			continue
		}

		channelName := strings.TrimSpace(line[channelNameIndex])
		channelName = strings.ToLower(channelName)
		if channelName == "" {
			fmt.Fprintf(os.Stderr, "Could not construct geoblock without channelName %v\n", line)
			unprocessedLines = append(unprocessedLines, line)
			continue
		}

		newGeoblock, err := constructGeoBlockFromCSVLine(line, channelNameIndex, startTimeIndex, endTimeIndex, allowlistCountriesIndex, blocklistCountriesIndex, allowlistPlatformsIndex, blocklistZipcodesIndex)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Could not construct geoblock from line %v %v\n", line, err.Error())
			unprocessedLines = append(unprocessedLines, line)
			continue
		}

		if existingGeoblock, ok := output[channelName]; ok {
			mergedGeoblock, err := mergeGeoblocks(existingGeoblock, newGeoblock)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Could not merge geoblock in CSV %v %v\n", line, err.Error())
				continue
			}
			output[channelName] = mergedGeoblock
		} else {
			output[channelName] = newGeoblock
		}
	}

	return output, unprocessedLines
}

func constructGeoBlockFromCSVLine(line []string, channelNameIndex int, startTimeIndex int, endTimeIndex int, allowlistCountriesIndex int, blocklistCountriesIndex int, allowlistPlatformsIndex int, blocklistZipcodesIndex int) (geoblock.Geoblock, error) {
	var err error

	channelName := strings.TrimSpace(line[channelNameIndex])
	startTime := strings.TrimSpace(line[startTimeIndex])
	endTime := strings.TrimSpace(line[endTimeIndex])

	parsedStartTime, err := time.Parse(time.RFC3339Nano, startTime)
	if err != nil {
		err = errors.Wrapf(err, "Could not parse startTime for %v in CSV", channelName)
		return geoblock.Geoblock{}, err
	}

	parsedEndTime, err := time.Parse(time.RFC3339Nano, endTime)
	if err != nil {
		err = errors.Wrapf(err, "Could not parse endTime for %v in CSV", channelName)
		return geoblock.Geoblock{}, err
	}

	allowlistCountries := map[string]bool{}
	if allowlistCountriesIndex > 0 {
		allowlistCountries = boolMapFromString(line[allowlistCountriesIndex], ForceUpper)
	}

	blocklistCountries := map[string]bool{}
	if blocklistCountriesIndex > 0 {
		blocklistCountries = boolMapFromString(line[blocklistCountriesIndex], ForceUpper)
	}

	blocklistZipcodes := map[string]bool{}
	if blocklistZipcodesIndex > 0 {
		blocklistZipcodes = boolMapFromString(line[blocklistZipcodesIndex], ForceUpper)
	}

	var allowlistPlatforms *map[string]bool
	if allowlistPlatformsIndex > 0 {
		converted := boolMapFromString(line[allowlistPlatformsIndex], ForceLower)
		allowlistPlatforms = &converted
	}

	return geoblock.Geoblock{
		TimeWindows: []geoblock.TimeWindow{
			{
				StartTime: parsedStartTime,
				EndTime:   parsedEndTime,
			},
		},
		AllowlistedCountries: allowlistCountries,
		AllowlistedPlatforms: allowlistPlatforms,
		BlocklistedCountries: blocklistCountries,
		BlocklistedZipCodes:  blocklistZipcodes,
		BlockAnonymousIPs:    true,
	}, err
}

type EnforceCase string

const (
	ForceUpper EnforceCase = "forceUpper"
	ForceLower EnforceCase = "forceLower"
	Mixed      EnforceCase = "forceNone"
)

func boolMapFromString(input string, keyCase EnforceCase) map[string]bool {
	newMap := map[string]bool{}

	for _, value := range strings.Split(input, ",") {
		value = strings.TrimSpace(value)
		if value == "" {
			continue
		}

		switch keyCase {
		case ForceUpper:
			value = strings.ToUpper(value)
		case ForceLower:
			value = strings.ToLower(value)
		}

		newMap[value] = true
	}

	return newMap
}

func removeExpiredGeoblocks(geoblocks map[string]geoblock.Geoblock) map[string]geoblock.Geoblock {
	for key, currentGeoblock := range geoblocks {
		validTimeWindows := []geoblock.TimeWindow{}
		for _, timeWindow := range currentGeoblock.TimeWindows {
			if timeWindow.EndTime.After(time.Now()) {
				validTimeWindows = append(validTimeWindows, timeWindow)
			} else {
				fmt.Fprintf(os.Stderr, "Removing expired timewindow: %v %v\n", key, timeWindow)
			}
		}

		if len(validTimeWindows) == 0 {
			fmt.Fprintf(os.Stderr, "Removing expired geoblock: %v %v\n", key, geoblocks[key])
			delete(geoblocks, key)
		} else {
			currentGeoblock.TimeWindows = validTimeWindows
			geoblocks[key] = currentGeoblock
		}
	}
	return geoblocks
}

func mergeGeoblocks(originalGeoblock geoblock.Geoblock, newGeoblock geoblock.Geoblock) (geoblock.Geoblock, error) {
	var err error
	if !reflect.DeepEqual(originalGeoblock.BlocklistedCountries, newGeoblock.BlocklistedCountries) {
		err = errors.New("Can not merge geoblocks with different BlocklistCountries")
		err = errors.Wrapf(err, ": %v %v", originalGeoblock.BlocklistedCountries, newGeoblock.BlocklistedCountries)
		return originalGeoblock, err
	}

	if !reflect.DeepEqual(originalGeoblock.AllowlistedCountries, newGeoblock.AllowlistedCountries) {
		err = errors.New("Can not merge geoblocks with different AllowlistedCountries")
		err = errors.Wrapf(err, ": %v %v", originalGeoblock.AllowlistedCountries, newGeoblock.AllowlistedCountries)
		return originalGeoblock, err
	}

	if !reflect.DeepEqual(originalGeoblock.BlocklistedZipCodes, newGeoblock.BlocklistedZipCodes) {
		err = errors.New("Can not merge geoblocks with different BlocklistedZipCodes")
		err = errors.Wrapf(err, ": %v %v", originalGeoblock.BlocklistedZipCodes, newGeoblock.BlocklistedZipCodes)
		return originalGeoblock, err
	}

	if !reflect.DeepEqual(&originalGeoblock.AllowlistedPlatforms, &newGeoblock.AllowlistedPlatforms) {
		err = errors.New("Can not merge geoblocks with different AllowlistedPlatforms")
		err = errors.Wrapf(err, ": %v %v", &originalGeoblock.AllowlistedPlatforms, &newGeoblock.AllowlistedPlatforms)
		return originalGeoblock, err
	}

	originalGeoblock.TimeWindows = append(originalGeoblock.TimeWindows, newGeoblock.TimeWindows...)

	return originalGeoblock, nil
}

func mergeGeoblockMap(originalGeoblocks map[string]geoblock.Geoblock, newGeoblocks map[string]geoblock.Geoblock) (map[string]geoblock.Geoblock, error) {
	var err error
	for key, newGeoblock := range newGeoblocks {
		originalGeoblock, keyExists := originalGeoblocks[key]
		// If there's already a geoblock
		if keyExists {
			mergedGeoblock, err := mergeGeoblocks(originalGeoblock, newGeoblock)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Could not merge geoblock in map %v %v %v %v\n", key, originalGeoblock, newGeoblock, err.Error())
				return originalGeoblocks, err
			}

			originalGeoblocks[key] = mergedGeoblock
		} else {
			originalGeoblocks[key] = newGeoblock
		}
	}

	return originalGeoblocks, err
}

func dedupeTimeWindows(originalGeoblocks map[string]geoblock.Geoblock) map[string]geoblock.Geoblock {
	for key, dedupedGeoblock := range originalGeoblocks {
		dedupedGeoblock.TimeWindows = uniqueTimeWindows(dedupedGeoblock.TimeWindows)
		originalGeoblocks[key] = dedupedGeoblock
	}

	return originalGeoblocks
}

func uniqueTimeWindows(sample []geoblock.TimeWindow) []geoblock.TimeWindow {
	unique := []geoblock.TimeWindow{}
	m := map[geoblock.TimeWindow]bool{}
	for _, timeWindow := range sample {
		if _, ok := m[timeWindow]; !ok {
			m[timeWindow] = true
			unique = append(unique, timeWindow)
		}
	}
	return unique
}

func writeOutput(outputFile string, bytesToWrite []byte) {
	_, outputStatErr := os.Stat(outputFile)

	// fmt.Println("file exists", outputStatErr, info)
	if !os.IsNotExist(outputStatErr) {
		if !confirm("File exists. Overwite?") {
			os.Exit(1)
		}
	}

	outputFilePointer, err := os.Create(outputFile)
	defer outputFilePointer.Close()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not create output file %v\n%v\n", outputFile, err)
		os.Exit(1)
	}

	writer := bufio.NewWriter(outputFilePointer)
	writtenByteCount, err := writer.Write(bytesToWrite)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not write output file %v\n%v\n", outputFile, err)
		os.Exit(1)
	}
	writer.Flush()

	fmt.Printf("Successfully wrote %d bytes to %v\n", writtenByteCount, outputFile)
}

func confirm(prompt string) bool {
	reader := bufio.NewReader(os.Stdin)

	okayResponses := map[string]bool{
		"y":   true,
		"yes": true,
	}
	nokayResponses := map[string]bool{
		"":   true,
		"n":  true,
		"no": true,
	}

	for {
		fmt.Print(prompt, " y/[n]: ")

		answer, _ := reader.ReadString('\n')
		answer = strings.TrimSpace(answer)
		answer = strings.ToLower(answer)

		if _, found := okayResponses[answer]; found {
			return true
		} else if _, found := nokayResponses[answer]; found {
			return false
		} else {
			fmt.Println("Please enter y or n")
			continue
		}
	}
}
