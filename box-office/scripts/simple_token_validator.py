#! /usr/bin/python

import requests
import json
import sys
import urllib

CLIENT_ID = 'ltoxwr5a0sxj4ug7zvgjnl28gtpasym'
CHANNEL = 'ninja'

USHER_ENDPOINT = 'http://usher.ttvnw.net/api/channel/hls/%s.m3u8?allow_spectre=true&allow_audio_only=true&allow_source=true&type=any&token=%s&sig=%s'

BOX_OFFICE_LOCAL_ENDPOINT = 'http://localhost:8000/twirp/code.justin.tv.commerce.boxoffice.BoxOffice/GetVideoAccessToken'
BOX_OFFICE_DEV_ENDPOINT = 'http://box-office-dev.internal.justin.tv/twirp/code.justin.tv.commerce.boxoffice.BoxOffice/GetVideoAccessToken'
BOX_OFFICE_STAGING_ENDPOINT = 'http://box-office-staging.internal.justin.tv/twirp/code.justin.tv.commerce.boxoffice.BoxOffice/GetVideoAccessToken'
BOX_OFFICE_PROD_ENDPOINT = 'http://box-office-prod.internal.justin.tv/twirp/code.justin.tv.commerce.boxoffice.BoxOffice/GetVideoAccessToken'

def get_box_office_token(channel, endpoint):
        headers = { 'Content-Type' : 'application/json' }
        request = { "channel_name": channel }
        payload = json.dumps(request, indent=4, sort_keys=True)
        print 'url: ' + endpoint
        r = requests.post(endpoint, data=payload, headers=headers)
        token = json.loads(r.text)
        print 'Box Office access token response:'
        print json.dumps(token, indent=4, sort_keys=True)
        print "\n"
        return token

def call_usher(channel, token_response):
        url = USHER_ENDPOINT % (channel, token_response['token'], token_response['sig'])
        headers = {'X-Source-Cluster': 'sfo01'}
        r = requests.get(url, headers=headers)

        print 'url: ' + url
        print r
        print r.text
        print r.status_code
        if r.status_code == 200:
                print 'TOKEN SUCCEEDED'
        else:
                print 'TOKEN FAILED'


# Simple script for generating a token for an anonymous user and sending it to
# Usher to make sure it works
# Note that Usher returns 404 if the stream is offline, so make sure you use a channel
# that is online.
#
# usage: 
# $ python simple_token_validator.py
print 'Getting token from Box Office'
endpoint = BOX_OFFICE_DEV_ENDPOINT
box_office_token = get_box_office_token(CHANNEL, endpoint)

print 'Validating Box Office token'
call_usher(CHANNEL, box_office_token)

