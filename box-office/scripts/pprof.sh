#!/bin/bash

# This script downloads go profile data from a running endpoint

HOSTNAME="$1"

URL="http://$HOSTNAME/debug2/pprof"

PROFILE_SECONDS=30
TRACE_SECONDS=5

OUT_DIR="/tmp/pprof/$HOSTNAME"

if [ ! $# -eq 1 ]; then
  printf "Usage:\n"
  printf "./pprof.sh box-office-prod.internal.justin.tv\n"
  exit 1
fi

printf "Profiling %s...\n" "$HOSTNAME"

if [ ! -d "$OUT_DIR" ]; then
  printf "Creating directory %s for output\n" "$OUT_DIR"
  mkdir -p "$OUT_DIR"
fi

printf "Requesting heap usage profile...\n"
curl "$URL/heap" > "$OUT_DIR/heap.pb.gz"

printf "Requesting goroutine profile...\n"
curl "$URL/goroutine" > "$OUT_DIR/goroutine.pb.gz"

printf "Requesting %ds CPU profile...\n" "$PROFILE_SECONDS"
curl "$URL/profile?seconds=$PROFILE_SECONDS" > "$OUT_DIR/profile-${PROFILE_SECONDS}s.pb.gz"

printf "Requesting %d-second execution trace...\n" "$TRACE_SECONDS"
curl "$URL/trace?seconds=$TRACE_SECONDS" > "$OUT_DIR/${TRACE_SECONDS}s.trace"

printf "Requesting goroutine stack dump...\n"
curl "$URL/goroutine" > "$OUT_DIR/goroutine.pb.gz"

printf "Done! Files were written to %s.\n" "$OUT_DIR"