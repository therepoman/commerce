require 'openssl'

# This script uses the same signing logic as the web/web token signing.
# Reference: https://git-aws.internal.justin.tv/web/web/blob/master/lib/crypto.rb#L5
#
# Use this to help test to make sure that the Golang signing logic produces the
# same result as the legacy web/web logic.
#
# Run with:
#   $ ruby scripts/signer.rb
#
class HMAC
   def initialize(key, data)
      @key = key
      @data = data
   end
   def sign
      puts "Key: #{@key}"
      puts "Data: #{@data}"
      digest = OpenSSL::Digest.new("sha1")
      hmac = OpenSSL::HMAC.hexdigest(digest, @key, @data)
      puts "HMAC signature: #{hmac}"
   end
end

puts "Running signer in ruby"
signer = HMAC.new("testkey999", "testdata555")
signer.sign