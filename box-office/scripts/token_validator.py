#! /usr/bin/python

import requests
import json
import sys
import urllib

CLIENT_ID = 'ltoxwr5a0sxj4ug7zvgjnl28gtpasym'
RAILS_TOKEN_ENDPOINT = 'http://api.twitch.tv/api/channels/%s/access_token?'
BOX_OFFICE_TOKEN_ENDPOINT = 'http://localhost:8000/twirp/code.justin.tv.commerce.boxoffice.BoxOffice/GetVideoAccessToken'
VISAGE_TOKEN_ENDPOINT = 'https://visage.prod.us-west2.justin.tv/api/channels/%s/access_token?'

# Get Rails token
def get_rails_token(channel, oauth):
        headers = {}
        headers['Client-ID'] = CLIENT_ID
        query_params = { "oauth_token": oauth }
        base_url = RAILS_TOKEN_ENDPOINT % channel
        full_url = base_url + urllib.urlencode(query_params)
        print 'url: ' + full_url
        r = requests.get(full_url, headers=headers)
        token = json.loads(r.text)
        print 'Rails access token response:'
        print json.dumps(token, indent=4, sort_keys=True)
        print "\n"
        return token

# Get Visage token
def get_visage_token(channel, oauth):
        headers = {}
        headers['Client-ID'] = CLIENT_ID
        query_params = { "oauth_token": oauth }
        base_url = VISAGE_TOKEN_ENDPOINT % channel
        full_url = base_url + urllib.urlencode(query_params)
        print 'url: ' + full_url
        r = requests.get(full_url, headers=headers)
        token = json.loads(r.text)
        print 'Visage access token response:'
        print json.dumps(token, indent=4, sort_keys=True)
        print "\n"
        return token

# Get Box Office token
def get_box_office_token(channel, tuid):
        headers = { 'Content-Type' : 'application/json' }
        request = { "channel_name": channel, "user_id": tuid }
        payload = json.dumps(request, indent=4, sort_keys=True)
        print 'url: ' + BOX_OFFICE_TOKEN_ENDPOINT
        r = requests.post(BOX_OFFICE_TOKEN_ENDPOINT, data=payload, headers=headers)
        token = json.loads(r.text)
        print 'Box Office access token response:'
        print json.dumps(token, indent=4, sort_keys=True)
        print "\n"
        return token

def call_usher(channel, token_response):
        url = "http://usher.ttvnw.net/api/channel/hls/%s.m3u8?allow_spectre=true&allow_audio_only=true&allow_source=true&type=any&token=%s&sig=%s" % (channel, token_response['token'], token_response['sig'])
        headers = {'X-Source-Cluster': 'sfo01'}
        r = requests.get(url, headers=headers)

        print 'url: ' + url
        print r
        print r.text


# usage: 
# $ python token_validator.py <channel name> <your oauth token> <your tuid>
channel = sys.argv[1]

oauth_token = ''
if len(sys.argv) == 3:
        oauth_token = sys.argv[2]

print 'Getting token from Rails'
rails_token = get_rails_token(channel, oauth_token)

print 'Validating Rails token'
call_usher(channel, rails_token)

print 'Validating Visage token'
visage_token = get_visage_token(channel, oauth_token)

# Uncomment if you want to run against local Box Office
# print 'Getting token from Box Office'
#box_office_token = get_box_office_token(channel, sys.argv[3])

print 'Validating Box Office token'
call_usher(channel, visage_token)

