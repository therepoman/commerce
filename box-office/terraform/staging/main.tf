provider "aws" {
  region  = "us-west-2"
  profile = "twitch-box-office-dev"
}

module "box-office" {
  source                     = "../modules/box-office"
  account_id                 = "664665898521"
  owner                      = "twitch-box-office-dev@amazon.com"
  env                        = "staging"
  dns                        = "box-office-staging.internal.justin.tv"
  vpc_id                     = "vpc-7e9ce907"
  private_subnets            = ["subnet-22cc3769", "subnet-31815948", "subnet-b2a723e8"]
  sg_id                      = "sg-83edeafc"
  instance_type              = "c5.xlarge"
  min_size                   = 2
  max_size                   = 4
  asg_canary_size            = 1
  root_volume_size           = 20
  redis5_instance_type       = "cache.r5.large"
  redis5_groups              = "1"
  redis5_replicas_per_group  = "2"
  ru_min_instance_in_service = 1
  ru_max_batch_size          = 1
  eb_deployment_percent      = 25
  internal_alb_dns_name      = "internal-awseb-e-d-AWSEBLoa-18G3DMRWD3RLS-1564858668.us-west-2.elb.amazonaws.com"

  vpc_endpoint_service_allowed_principals = [
    "arn:aws:iam::761942825647:root", // twitch-clips-workers-dev
    "arn:aws:iam::989252031013:root", // twitch-clips-workers-prod
    "arn:aws:iam::085436627129:root",
    "arn:aws:iam::666942537705:root", // twitch-clips-workers-beta-new
    "arn:aws:iam::645130450452:root", // twitch-graphql-dev
    "arn:aws:iam::848744099708:root", // twitch-video-aws
    "arn:aws:iam::948329120781:root", // twitch-clips-aws
    "arn:aws:iam::327140220177:root", // twitch-api-dev
    "arn:aws:iam::763322040041:root", // twitch-video-clients-ci
  ]
}

resource "aws_ssm_parameter" "staging-primary-cache-redis5-percentage" {
  name  = "staging-primary-cache-redis5-percentage"
  type  = "String"
  value = "0.0"

  lifecycle {
    ignore_changes = ["value"]
  }
}

resource "aws_ssm_parameter" "staging-fallback-cache-redis5-percentage" {
  name  = "staging-fallback-cache-redis5-percentage"
  type  = "String"
  value = "0.0"

  lifecycle {
    ignore_changes = ["value"]
  }
}
