provider "aws" {
  region  = "us-west-2"
  profile = "twitch-box-office-dev"
}

module "box-office" {
  source                     = "../modules/box-office"
  account_id                 = "664665898521"
  owner                      = "twitch-box-office-dev@amazon.com"
  env                        = "dev"
  dns                        = "box-office-dev.internal.justin.tv"
  vpc_id                     = "vpc-7e9ce907"
  private_subnets            = ["subnet-22cc3769", "subnet-31815948", "subnet-b2a723e8"]
  sg_id                      = "sg-83edeafc"
  instance_type              = "t2.large"
  min_size                   = 2
  max_size                   = 4
  asg_canary_size            = 1
  root_volume_size           = 20
  redis5_instance_type       = "cache.r5.large"
  redis5_groups              = "1"
  redis5_replicas_per_group  = "2"
  ru_min_instance_in_service = 1
  ru_max_batch_size          = 1
  internal_alb_dns_name      = "internal-awseb-e-m-AWSEBLoa-8UDZVFT0PJI5-1770077973.us-west-2.elb.amazonaws.com"

  vpc_endpoint_service_allowed_principals = [
    "arn:aws:iam::761942825647:root", //twitch-clips-workers-dev
    "arn:aws:iam::666942537705:root", // twitch-clips-workers-beta-new
  ]
}
