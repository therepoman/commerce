resource "aws_elasticache_subnet_group" "redis-subnet-group" {
  name        = "${var.cluster_name}-subnet-group"
  description = "subnet group for redis cluster"
  subnet_ids  = "${var.subnet_ids}"
}

resource "aws_elasticache_replication_group" "redis" {
  replication_group_id          = "${var.cluster_name}"
  replication_group_description = "box office redis cache"
  node_type                     = "${var.instance_type}"
  port                          = 6379
  parameter_group_name          = "default.redis5.0.cluster.on"
  # availability_zones            = ["us-west-2a", "us-west-2b", "us-west-2c"]
  automatic_failover_enabled    = true
  subnet_group_name             = "${aws_elasticache_subnet_group.redis-subnet-group.name}"

  engine = "redis"
  engine_version = "5.0.6"

  security_group_ids = [
    "${var.security_group_ids}",
  ]

  cluster_mode {
    replicas_per_node_group = "${var.replicas_per_shard}"
    num_node_groups         = "${var.shard_count}"
  }

  lifecycle {
    prevent_destroy = true
  }
}
