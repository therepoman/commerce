variable "cluster_name" {
  type = "string"
}

variable "instance_type" {
  type = "string"
}

variable "security_group_ids" {
  type = "string"
}

variable "subnet_ids" {
  type = "list"
}

variable "shard_count" {
  type = "string"
}

variable "replicas_per_shard" {
  type = "string"
}
