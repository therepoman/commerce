module "beanstalk" {
  source                           = "../beanstalk"
  aws_account_id                   = "${var.account_id}"
  eb_application_name              = "box-office"
  vpc_id                           = "${var.vpc_id}"
  ec2_subnet_ids                   = "${var.private_subnets}"
  elb_subnet_ids                   = "${var.private_subnets}"
  elb_loadbalancer_security_groups = "${var.sg_id}"
  auto_scaling_lc_security_groups  = "${var.sg_id}"
  solution_stack_name              = "64bit Amazon Linux 2018.03 v2.16.5 running Docker 19.03.13-ce"
  auto_scaling_lc_keypair_name     = "box-office"
  auto_scaling_lc_instance_type    = "${var.instance_type}"
  auto_scaling_lc_root_volume_size = "${var.root_volume_size}"
  min_size                         = "${var.min_size}"
  max_size                         = "${var.max_size}"
  asg_canary_size                  = "${var.asg_canary_size}"
  owner                            = "${var.owner}"
  service                          = "${var.service}"
  env                              = "${var.env}"
  dns                              = "${var.dns}"
  associate_public_address         = "false"
  asgtrigger_measure_name          = "${var.asgtrigger_measure_name}"
  asgtrigger_unit                  = "${var.asgtrigger_unit}"
  asgtrigger_lower_threshold       = "${var.asgtrigger_lower_threshold}"
  asgtrigger_upper_threshold       = "${var.asgtrigger_upper_threshold}"
  ru_min_instance_in_service       = "${var.ru_min_instance_in_service}"
  eb_deployment_percent            = "${var.eb_deployment_percent}"
  ru_max_batch_size                = "${var.ru_max_batch_size}"
}

module "cloudwatch" {
  source = "../cloudwatch"
}

module "redis5" {
  source             = "../redis5"
  cluster_name       = "bo-${var.env}-redis5"
  instance_type      = "${var.redis5_instance_type}"
  shard_count        = "${var.redis5_groups}"
  replicas_per_shard = "${var.redis5_replicas_per_group}"
  subnet_ids         = "${var.private_subnets}"
  security_group_ids = "${var.sg_id}"
}

module "s3" {
  source = "../s3"
  env    = "${var.env}"
}

module "nlb_to_clb" {
  source                                  = "../nlb_to_clb_privatelink"
  app_name                                = "box-office"
  aws_account_id                          = "${var.account_id}"
  env                                     = "${var.env}"
  internal_alb_dns_name                   = "${var.internal_alb_dns_name}"
  region                                  = "us-west-2"
  subnet_ids                              = "${var.private_subnets}"
  vpc_id                                  = "${var.vpc_id}"
  vpc_endpoint_service_allowed_principals = "${var.vpc_endpoint_service_allowed_principals}"
}

module "privatelink" {
  source         = "../privatelink"
  environment    = "${var.env}"
  region         = "us-west-2"
  elb_subnets    = "${var.private_subnets}"
  vpc_id         = "${var.vpc_id}"
  load_balancer  = "${module.beanstalk.load_balancers[0]}"
  security_group = "${var.sg_id}"

  allowed_principals = var.vpc_endpoint_service_allowed_principals
}

module "vpc" {
  source          = "../vpc"
  env             = "${var.env}"
  region          = "us-west-2"
  private_subnets = "${var.private_subnets}"
  vpc_id          = "${var.vpc_id}"
  sg_id           = "${var.sg_id}"
}
