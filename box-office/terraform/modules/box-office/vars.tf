# AWS general Config
variable "service" {
  description = "Name of the service being built. Should probably leave this as the default"
  default     = "commerce/box-office"
}

variable "env" {
  description = "Whether it's production, dev, etc"
}

variable "dns" {
  description = "Internal domain name. Currently managed in Twitch dashboard. e.g. box-office-prod.internal.justin.tv. If this is the first time creating a cert for this stack, the cert validator will pause the terraform creation and poll for the DNS validation. You must then go to https://dashboard.internal.justin.tv/dns/ and enter the CNAME entry as directed by the newly created cert in the ACM console."
}

variable "account_id" {
  description = "AWS account ID"
}

variable "owner" {
  description = "Owner of the account"
}

# VPC Config
variable "vpc_id" {
  description = "Id of the systems-created VPC"
}

variable "private_subnets" {
  description = "Comma-separated list of private subnets"
  type        = "list"
}

variable "sg_id" {
  description = "Id of the security group"
}

# ASG Config
variable "instance_type" {
  description = "EC2 instance type"
}

variable "root_volume_size" {
  description = "Storage capacity of the root Amazon EBS volume in whole GiB"
}

variable "min_size" {
  description = "Minimum ASG fleet size"
}

variable "max_size" {
  description = "Maximum ASG fleet size"
}

variable "asg_canary_size" {
  description = "Canary ASG fleet size"
}

variable "asgtrigger_measure_name" {
  description = "Metric used for your Auto Scaling trigger."
  default     = "CPUUtilization"
}

variable "asgtrigger_unit" {
  description = "Unit for the trigger measurement, such as Bytes."
  default     = "Percent"
}

variable "asgtrigger_lower_threshold" {
  description = "If the measurement falls below this number for the breach duration, a trigger is fired."
  default     = "5"
}

variable "asgtrigger_upper_threshold" {
  description = "If the measurement is higher than this number for the breach duration, a trigger is fired."
  default     = "50"
}

# Redis config
variable "redis5_instance_type" {
  description = "type of instance to use for the Redis 5 cluster"
  type        = "string"
}

variable "redis5_groups" {
  type = "string"
  description = "shard count for Redis 5 cluster"
}

variable "redis5_replicas_per_group" {
  type = "string"
  description = "replica count per shard for Redis 5 cluster"
}

variable "ru_min_instance_in_service" {
  type        = "string"
  description = "Rolling Update Miniumum Instances in service."
}

variable "ru_max_batch_size" {
  type        = "string"
  description = "Rolling Update Max Batch Size."
}

variable "eb_deployment_percent" {
  type        = "string"
  default     = 25
  description = "The percent of the environment that is deployed at once"
}

variable "vpc_endpoint_service_allowed_principals" {
  type        = "list"
  description = "Comma separated list of AWS accounts that can create PrivateLinks to us."

}

variable "internal_alb_dns_name" {
  type        = "string"
  description = "DNS name of the load balancer for beanstalk"
}
