locals {
  environment = {
    staging = "staging"
    prod    = "production"
  }

  vodapi = {
    staging = "com.amazonaws.vpce.us-west-2.vpce-svc-05b8c3b3eaaeaa9dc"
    prod    = "com.amazonaws.vpce.us-west-2.vpce-svc-0d03f1366bb1966a9"
  }

  vodapi_dns = {
    staging = "staging.vodapi.twitch.a2z.com"
    prod    = "production.vodapi.twitch.a2z.com"
  }

  tailor = {
    staging = "com.amazonaws.vpce.us-west-2.vpce-svc-00e41c5bad3654ac9"
    prod    = "com.amazonaws.vpce.us-west-2.vpce-svc-02eb5bac4425a9f35"
  }

  tailor_dns = {
    staging = "staging.tailor.twitch.a2z.com"
    prod    = "production.tailor.twitch.a2z.com"
  }

  voyager = {
    staging = "com.amazonaws.vpce.us-west-2.vpce-svc-0d91222878361af55"
    prod    = "com.amazonaws.vpce.us-west-2.vpce-svc-0f85c6b7d64b4cf96"
  }

  voyager_dns = {
    staging = "main.us-west-2.beta.voyager.s.twitch.a2z.com"
    prod    = "main.us-west-2.prod.voyager.s.twitch.a2z.com"
  }

  clips = {
    staging = "com.amazonaws.vpce.us-west-2.vpce-svc-0906ecd06bb6a31ae"
    prod    = "com.amazonaws.vpce.us-west-2.vpce-svc-04e8d94a8f6fc80ad"
  }

  clips_dns = {
    staging = "staging.clips.twitch.a2z.com"
    prod    = "production.clips.twitch.a2z.com"
  }

  hallpass = {
    staging = "com.amazonaws.vpce.us-west-2.vpce-svc-05c9a9682cec9fa80"
    prod    = "com.amazonaws.vpce.us-west-2.vpce-svc-051cf257eb25dd6d5"
  }

  hallpass_dns = {
    staging = "staging.hallpass.cb.twitch.a2z.com"
    prod    = "prod.hallpass.cb.twitch.a2z.com"
  }
}

data "aws_elb" "beanstalk_lb" {
  name = "${var.load_balancer}"
}

data "aws_lb" "privatelink_lb" {
  name = "box-office-a2z-${local.environment[var.environment]}-PLELB"
}

module "privatelink-zone" {
  source      = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-zone"
  name        = "box-office"
  environment = "${local.environment[var.environment]}"
}

data "aws_route53_zone" "legacy_zone" {
  name = "box-office-${var.environment}.internal.justin.tv"
}

module "privatelink-cert" {
  source = "../privatelink-cert"

  name        = "box-office"
  environment = "${local.environment[var.environment]}"
  zone_id     = "${module.privatelink-zone.zone_id}"

  alb_dns = "${data.aws_lb.privatelink_lb.dns_name}"

  legacy_zone_id = var.environment == "prod" ? "${data.aws_route53_zone.legacy_zone.id}" : ""
}

module "privatelink" {
  source      = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink?ref=0.8.0"
  name        = "box-office-a2z"
  region      = "${var.region}"
  environment = "${local.environment[var.environment]}"
  alb_dns     = "${data.aws_elb.beanstalk_lb.dns_name}"
  cert_arn    = "${module.privatelink-cert.arn}"
  vpc_id      = "${var.vpc_id}"
  subnets     = "${var.elb_subnets}"

  whitelisted_arns = var.allowed_principals
}

resource "aws_vpc_endpoint" "ldap-vpc-endpoint" {
  vpc_id              = "${var.vpc_id}"
  service_name        = "com.amazonaws.vpce.us-west-2.vpce-svc-0437151f68c61b808"
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = true
  security_group_ids  = ["${var.security_group}"]
  subnet_ids          = "${var.elb_subnets}"
  tags = {
    Name = "twitch-ldap"
  }
}

module "privatelink-vodapi" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=0.7.8"
  name            = "vodapi"
  endpoint        = "${local.vodapi[var.environment]}"
  security_groups = ["${var.security_group}"]
  subnets         = "${var.elb_subnets}"
  vpc_id          = "${var.vpc_id}"
  dns             = "${local.vodapi_dns[var.environment]}"
}

module "privatelink-tailor" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=0.7.8"
  name            = "tailor"
  endpoint        = "${local.tailor[var.environment]}"
  security_groups = ["${var.security_group}"]
  subnets         = "${var.elb_subnets}"
  vpc_id          = "${var.vpc_id}"
  dns             = "${local.tailor_dns[var.environment]}"
}

module "privatelink-voyager" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=0.7.8"
  name            = "voyager"
  endpoint        = "${local.voyager[var.environment]}"
  security_groups = ["${var.security_group}"]
  subnets         = "${var.elb_subnets}"
  vpc_id          = "${var.vpc_id}"
  dns             = "${local.voyager_dns[var.environment]}"
}

module "privatelink-clips" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=0.7.8"
  name            = "clips"
  endpoint        = "${local.clips[var.environment]}"
  security_groups = ["${var.security_group}"]
  subnets         = "${var.elb_subnets}"
  vpc_id          = "${var.vpc_id}"
  dns             = "${local.clips_dns[var.environment]}"
}

module "privatelink-hallpass" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=0.7.8"
  name            = "hallpass"
  endpoint        = "${local.hallpass[var.environment]}"
  security_groups = ["${var.security_group}"]
  subnets         = "${var.elb_subnets}"
  vpc_id          = "${var.vpc_id}"
  dns             = "${local.hallpass_dns[var.environment]}"
}
