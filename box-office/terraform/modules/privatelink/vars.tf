variable "environment" {
  type = "string"
}

variable "region" {
  type = "string"
}

variable "load_balancer" {
  description = "beanstalk load balancer"
}

variable "elb_subnets" {
  description = "elb subnets"
}

variable "vpc_id" {
  description = "vpc_id"
}

variable "security_group" {
  description = "security group id"
}

variable "allowed_principals" {
  description = "list of arns allowed to access privatelink"
  type        = list(string)
}
