data "aws_iam_policy_document" "grafana-cloudwatch-arp" {
  statement {
    effect = "Allow"

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "AWS"

      identifiers = [
        "arn:aws:iam::007917851548:role/grafana-production",
      ]
    }
  }

  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      type = "AWS"
      identifiers = [
        "arn:aws:iam::109561199089:role/grafana-production"
      ]
    }
  }

  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      type = "AWS"
      identifiers = [
        "arn:aws:iam::963768028156:role/grafana-development"
      ]
    }
  }
}
resource "aws_iam_role" "grafana-cloudwatch-read-only" {
  name               = "grafana-cloudwatch-read-only"
  assume_role_policy = "${data.aws_iam_policy_document.grafana-cloudwatch-arp.json}"
}
resource "aws_iam_role_policy_attachment" "grafana-cloudwatch-read-only" {
  role       = "${aws_iam_role.grafana-cloudwatch-read-only.name}"
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchReadOnlyAccess"
}
