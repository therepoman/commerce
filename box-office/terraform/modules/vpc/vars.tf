variable "env" {
  type = "string"
}

variable "region" {
  type = "string"
}

variable "private_subnets" {
  description = "private subnets"
}

variable "vpc_id" {
  description = "vpc_id"
}

variable "sg_id" {
  description = "sg_id"
}
