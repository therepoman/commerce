resource "aws_lambda_function" "pl_lambda_function" {
  function_name = "${var.app_name}-${var.env}-PrivateLinkLambda"
  s3_bucket     = "exampleloadbalancer-us-west-2"
  s3_key        = "blog-posts/static-ip-for-application-load-balancer/populate_NLB_TG_with_ALB.zip"
  description   = "Register Application Load Balancer to Network Load Balancer"
  handler       = "populate_NLB_TG_with_ALB.lambda_handler"
  role          = "${aws_iam_role.pl_lambda_iam_role.arn}"
  runtime       = "python2.7"
  timeout       = "300"

  environment {
    variables = {
      ALB_DNS_NAME                      = "${var.internal_alb_dns_name}"
      ALB_LISTENER                      = "80"
      CW_METRIC_FLAG_IP_COUNT           = "True"
      INVOCATIONS_BEFORE_DEREGISTRATION = "3"
      MAX_LOOKUP_PER_INVOCATION         = "50"
      NLB_TG_ARN                        = "${aws_lb_target_group.pl_target_group.arn}"
      S3_BUCKET                         = "${aws_s3_bucket.elb_ips_bucket.id}"
    }
  }
}

resource "aws_iam_role" "pl_lambda_iam_role" {
  name = "${var.app_name}-${var.env}-PrivateLink"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Principal": {
      "Service": "lambda.amazonaws.com"
    },
    "Action": "sts:AssumeRole"
  }
}
EOF
}

resource "aws_iam_role_policy" "pl_lambda_iam_role_policy" {
  name = "Lambda-ALBasTarget"
  role = "${aws_iam_role.pl_lambda_iam_role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [{
      "Sid": "LambdaLogging",
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": [
        "arn:aws:logs:*:*:*"
      ]
    },
    {
      "Sid": "S3",
      "Action": [
        "s3:Get*",
        "s3:PutObject",
        "s3:CreateBucket",
        "s3:ListBucket",
        "s3:ListAllMyBuckets"
      ],
      "Effect": "Allow",
      "Resource": "*"
    },
    {
      "Sid": "ELB",
      "Action": [
        "elasticloadbalancing:Describe*",
        "elasticloadbalancing:RegisterTargets",
        "elasticloadbalancing:DeregisterTargets"
      ],
      "Effect": "Allow",
      "Resource": "*"
    },
    {
      "Sid": "CW",
      "Action": [
        "cloudwatch:putMetricData"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_lambda_permission" "pl_lambda_permission" {
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.pl_lambda_function.function_name}"
  principal     = "events.amazonaws.com"
  source_arn    = "${aws_cloudwatch_event_rule.pl_event_rule.arn}"
}

resource "aws_cloudwatch_event_rule" "pl_event_rule" {
  description         = "ScheduledRule"
  schedule_expression = "rate(1 minute)"
}

resource "aws_cloudwatch_event_target" "pl_event_target" {
  rule = "${aws_cloudwatch_event_rule.pl_event_rule.name}"
  arn  = "${aws_lambda_function.pl_lambda_function.arn}"
}

resource "aws_s3_bucket" "elb_ips_bucket" {
  bucket = "${var.app_name}.${var.env}.${var.region}.${var.aws_account_id}.elb-ips"
  acl    = "private"
}

resource "aws_lb" "pl_nlb" {
  name                             = "${var.app_name}-${var.env}-PL-NLB"
  internal                         = true
  load_balancer_type               = "network"
  enable_cross_zone_load_balancing = true

  subnets = "${var.subnet_ids}"
}

resource "aws_lb_target_group" "pl_target_group" {
  name                 = "${var.app_name}-${var.env}-PL-TG"
  port                 = 80
  protocol             = "TCP"
  vpc_id               = "${var.vpc_id}"
  target_type          = "ip"
  deregistration_delay = 300

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 3
  }
}

resource "aws_lb_listener" "pl_listener" {
  load_balancer_arn = "${aws_lb.pl_nlb.arn}"
  port              = "80"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.pl_target_group.arn}"
  }
}

resource "aws_vpc_endpoint_service" "vpc_endpoint_service" {
  acceptance_required        = false
  network_load_balancer_arns = ["${aws_lb.pl_nlb.arn}"]

  allowed_principals = "${var.vpc_endpoint_service_allowed_principals}"
}
