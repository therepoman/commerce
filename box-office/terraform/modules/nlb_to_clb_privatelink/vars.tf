variable "app_name" {
  type        = "string"
  description = "Application name to create the PrivateLink setup."
}

variable "aws_account_id" {
  type = "string"
  description = "AWS account ID"
}

variable "env" {
  type        = "string"
  default     = "dev"
  description = "Environment PrivateLink setup is for."
}

variable "internal_alb_dns_name" {
  type = "string"
  description = "DNS name to forward traffic from the NLB to."
}

variable "region" {
  type = "string"
  description = "Region to deploy this PrivateLink setup."
}

variable "subnet_ids" {
  type = "list"
  description = "Comma-separated list of private subnets"
}

variable "vpc_id" {
  type        = "string"
  description = "VPC the PrivateLink should be setup."
}

variable "vpc_endpoint_service_allowed_principals" {
  type        = "list"
  description = "The ARNs that are whitelisted to access the VPC endpoint service"
}
