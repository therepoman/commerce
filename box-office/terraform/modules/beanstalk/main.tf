resource "aws_acm_certificate" "cert" {
  domain_name       = "${var.dns}"
  validation_method = "DNS"

  tags = {
    Environment = "${var.env}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn = "${aws_acm_certificate.cert.arn}"
}

locals {
  common_beanstalk_settings = [
    # Vpc  and network related settings
    {
      namespace = "aws:ec2:vpc"
      name      = "VPCId"
      value     = "${var.vpc_id}"
    },
    {
      namespace = "aws:ec2:vpc"
      name      = "Subnets"
      value     = "${join(",", var.ec2_subnet_ids)}"
    },
    {
      namespace = "aws:ec2:vpc"
      name      = "ELBSubnets"
      value     = "${join(",", var.ec2_subnet_ids)}"
    },
    {
      namespace = "aws:ec2:vpc"
      name      = "AssociatePublicIpAddress"
      value     = "${var.associate_public_address}"
    },
    {
      namespace = "aws:ec2:vpc"
      name      = "ELBScheme"
      value     = "${var.elb_scheme}"
    },
    # Load Balancer Related Settings
    {
      namespace = "aws:elb:loadbalancer"
      name      = "SecurityGroups"
      value     = "${var.elb_loadbalancer_security_groups}"
    },
    {
      namespace = "aws:elb:loadbalancer"
      name      = "CrossZone"
      value     = "true"
    },
    {
      namespace = "aws:elb:policies"
      name      = "ConnectionDrainingEnabled"
      value     = "true"
    },
    {
      namespace = "aws:elb:policies"
      name      = "ConnectionDrainingTimeout"
      value     = "10"                        # 10 second timeout
    },
    {
      namespace = "aws:elb:listener:443"
      name      = "ListenerProtocol"
      value     = "HTTPS"
    },
    {
      namespace = "aws:elb:listener:443"
      name      = "SSLCertificateId"
      value     = "${aws_acm_certificate_validation.cert.certificate_arn}"
    },
    {
      namespace = "aws:elb:listener:443"
      name      = "ListenerEnabled"
      value     = "${aws_acm_certificate_validation.cert.certificate_arn == "" ? false : true}"
    },
    # Health
    {
      namespace = "aws:elasticbeanstalk:application"
      name      = "Application Healthcheck URL"
      value     = "/health"
    },
    {
      namespace = "aws:elb:healthcheck"
      name      = "Interval"
      value     = "30"
    },
    {
      namespace = "aws:elasticbeanstalk:healthreporting:system"
      name      = "SystemType"
      value     = "enhanced"
    },
    # ASG launch configuration settings
    {
      namespace = "aws:autoscaling:launchconfiguration"
      name      = "IamInstanceProfile"
      value     = "${coalesce(var.auto_scaling_lc_iam_instance_profile, aws_iam_instance_profile.beanstalk.name)}"
    },
    {
      namespace = "aws:autoscaling:launchconfiguration"
      name      = "EC2KeyName"
      value     = "${var.auto_scaling_lc_keypair_name}"
    },
    {
      namespace = "aws:autoscaling:launchconfiguration"
      name      = "InstanceType"
      value     = "${var.auto_scaling_lc_instance_type}"
    },
    {
      namespace = "aws:autoscaling:launchconfiguration"
      name      = "MonitoringInterval"
      value     = "${var.auto_scaling_lc_monitoring_interval}"
    },
    {
      namespace = "aws:autoscaling:launchconfiguration"
      name      = "SecurityGroups"
      value     = "${var.auto_scaling_lc_security_groups}"
    },
    {
      namespace = "aws:autoscaling:launchconfiguration"
      name      = "RootVolumeType"
      value     = "${var.auto_scaling_lc_root_volume_type}"
    },
    {
      namespace = "aws:autoscaling:launchconfiguration"
      name      = "RootVolumeSize"
      value     = "${var.auto_scaling_lc_root_volume_size}"
    },
    {
      namespace = "aws:autoscaling:launchconfiguration"
      name      = "RootVolumeIOPS"
      value     = "${var.auto_scaling_lc_root_volume_iops}"
    },
    # Environment Variables
    {
      namespace = "aws:elasticbeanstalk:application:environment"
      name      = "APP"
      value     = "${var.eb_application_name}"
    },
    {
      namespace = "aws:elasticbeanstalk:application:environment"
      name      = "STATSD_HOST_PORT"
      value     = "${var.statsd_host}"
    },
    # eb environment settings
    {
      namespace = "aws:elasticbeanstalk:environment"
      name      = "ServiceRole"
      value     = "${var.eb_environment_service_role}"
    },
    # Cloudwatch settings
    {
      namespace = "aws:elasticbeanstalk:cloudwatch:logs"
      name      = "StreamLogs"
      value     = "true"
    },
    {
      namespace = "aws:elasticbeanstalk:cloudwatch:logs"
      name      = "DeleteOnTerminate"
      value     = "false"
    },
    {
      namespace = "aws:elasticbeanstalk:cloudwatch:logs"
      name      = "RetentionInDays"
      value     = "60"
    },
  ]
}

resource "aws_elastic_beanstalk_environment" "beanstalk" {
  name                = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}-env"
  description         = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}"
  application         = "${var.eb_application_name}"
  solution_stack_name = "${var.solution_stack_name}"
  tier                = "WebServer"

  tags = {
    # Name        = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}"
    Environment = "${var.env}"
    Service     = "${var.service}"
    Owner       = "${var.owner}"
  }

  wait_for_ready_timeout = "${var.wait_for_ready_timeout}"

  # Common settings shared by canary and regular environment
  dynamic "setting" {
    for_each = local.common_beanstalk_settings
    content {
      namespace = setting.value.namespace
      name      = setting.value.name
      value     = setting.value.value
    }
  }

  # ASG configuration settings
  setting {
    namespace = "aws:autoscaling:asg"
    name      = "MinSize"
    value     = "${var.min_size}"
  }

  setting {
    namespace = "aws:autoscaling:asg"
    name      = "MaxSize"
    value     = "${var.max_size}"
  }

  # ASG trigger configuration settings
  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "MeasureName"
    value     = "${var.asgtrigger_measure_name}"
  }

  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "Unit"
    value     = "${var.asgtrigger_unit}"
  }

  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "LowerThreshold"
    value     = "${var.asgtrigger_lower_threshold}"
  }

  setting {
    namespace = "aws:autoscaling:trigger"
    name      = "UpperThreshold"
    value     = "${var.asgtrigger_upper_threshold}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "ENV_NAME"
    value     = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}-env"
  }

  # Environment Variables
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "ENVIRONMENT"
    value     = "${var.env}"
  }

  # Rolling Deployments
  setting {
    namespace = "aws:elasticbeanstalk:command"
    name      = "DeploymentPolicy"
    value     = "Rolling"
  }

  setting {
    namespace = "aws:elasticbeanstalk:command"
    name      = "BatchSize"
    value     = "${var.eb_deployment_percent}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:command"
    name      = "BatchSizeType"
    value     = "Percentage"
  }

  # Rolling Update
  setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name      = "RollingUpdateEnabled"
    value     = "true"
  }

  setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name      = "RollingUpdateType"
    value     = "Health"
  }

  setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name      = "MinInstancesInService"
    value     = "${var.ru_min_instance_in_service}"
  }

  setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name      = "MaxBatchSize"
    value     = "${var.ru_max_batch_size}"
  }

  setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name      = "Timeout"
    value     = "PT10M"
  }
}

resource "aws_elastic_beanstalk_environment" "beanstalk-canary" {
  name                = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}-canary-env"
  description         = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}"
  application         = "${var.eb_application_name}"
  solution_stack_name = "${var.solution_stack_name}"
  tier                = "WebServer"

  tags = {
    # Name        = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}"
    Environment = "${var.env}"
    Service     = "${var.service}"
    Owner       = "${var.owner}"
  }

  wait_for_ready_timeout = "${var.wait_for_ready_timeout}"

  # Common settings shared by canary and regular environment
  dynamic "setting" {
    for_each = local.common_beanstalk_settings
    content {
      namespace = setting.value.namespace
      name      = setting.value.name
      value     = setting.value.value
    }
  }

  # ASG configuration settings
  setting {
    namespace = "aws:autoscaling:asg"
    name      = "MinSize"
    value     = "${var.asg_canary_size}"
  }

  setting {
    namespace = "aws:autoscaling:asg"
    name      = "MaxSize"
    value     = "${var.asg_canary_size}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "ENV_NAME"
    value     = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}-canary-env"
  }

  # Environment Variables
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "ENVIRONMENT"
    value     = "${var.env}-canary"
  }
}

# # Create a new load balancer attachment
resource "aws_autoscaling_attachment" "asg_attachment_bar" {
  autoscaling_group_name = "${aws_elastic_beanstalk_environment.beanstalk-canary.autoscaling_groups[0]}"
  elb                    = "${aws_elastic_beanstalk_environment.beanstalk.load_balancers[0]}"
}
