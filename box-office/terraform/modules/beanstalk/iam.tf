resource "aws_iam_role" "beanstalk" {
  name = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}"
  path = "/"

  assume_role_policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": { "Service": "ec2.amazonaws.com"},
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT
}

resource "aws_iam_role_policy" "beanstalk" {
  name   = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}"
  policy = "${coalesce(var.iam_role_policy, file("${path.module}/files/iam_role"))}"
  role   = "${aws_iam_role.beanstalk.id}"
}

resource "aws_iam_instance_profile" "beanstalk" {
  depends_on = ["aws_iam_role.beanstalk"]
  name       = "${coalesce(var.common_name, null_resource.vars.triggers.cn)}"
  role       = "${aws_iam_role.beanstalk.name}"
}

// Grant the Instance Profile full cloudwatch access
resource "aws_iam_role_policy_attachment" "cw_policy" {
  role       = "${aws_iam_role.beanstalk.name}"
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchFullAccess"
}

data "aws_iam_policy_document" "sandstorm" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    resources = [
      "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/box-office-${var.env}",
    ]

    effect = "Allow"
  }

  statement {
    sid = "Stmt1473817796000"

    actions = [
      "s3:GetObject",
    ]

    resources = [
      "arn:aws:s3:::twitch-sandstorm/sandstorm-agent.rpm",
    ]

    effect = "Allow"
  }
}

// Policy necessary for sandstorm to start properly
resource "aws_iam_role_policy" "sandstorm" {
  name   = "sandstorm-${var.env}"
  role   = "${aws_iam_role.beanstalk.id}"
  policy = "${data.aws_iam_policy_document.sandstorm.json}"
}
