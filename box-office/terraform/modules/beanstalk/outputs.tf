output "beanstalk_environment_name" {
  value = "${aws_elastic_beanstalk_environment.beanstalk.name}"
}

output "beanstalk_environment_cname" {
  value = "${aws_elastic_beanstalk_environment.beanstalk.cname}"
}

output "autoscaling_groups" {
  value = "${aws_elastic_beanstalk_environment.beanstalk.autoscaling_groups}"
}

output "instances" {
  value = "${aws_elastic_beanstalk_environment.beanstalk.instances}"
}

output "launch_configurations" {
  value = "${aws_elastic_beanstalk_environment.beanstalk.launch_configurations}"
}

output "load_balancers" {
  value = "${aws_elastic_beanstalk_environment.beanstalk.load_balancers}"
}

output "iam_role_id" {
  value = "${aws_iam_role.beanstalk.id}"
}

output "iam_role_arn" {
  value = "${aws_iam_role.beanstalk.arn}"
}

output "cname" {
  value = "${aws_elastic_beanstalk_environment.beanstalk.cname}"
}
