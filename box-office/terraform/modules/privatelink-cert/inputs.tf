variable "region" {
  type        = "string"
  default     = "us-west-2"
  description = "AWS region the PrivateLink should be setup"
}

variable "domain" {
  type    = "string"
  default = "twitch.a2z.com"
}

variable "name" {
  type = "string"
}

variable "environment" {
  type        = "string"
  description = "staging/production"
}

variable "target" {
  type    = "string"
  default = "main"
}

variable "zone_id" {
  type        = "string"
  description = "Route53 Zone ID"
}

variable "legacy_zone_id" {
  type        = "string"
  description = "Route53 Zone ID"
}

variable "alb_dns" {
  type        = "string"
  description = "ALB DNS to use for zone entry"
}
