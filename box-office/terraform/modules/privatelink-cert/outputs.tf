output "arn" {
  value = "${aws_acm_certificate.cert.arn}"
}

output "dns" {
  value = "${aws_acm_certificate.cert.domain_name}"
}
