resource "aws_s3_bucket" "geoblocking-bucket" {
  bucket = "box-office-${var.env}-geoblocking"
  acl = "private"

  versioning {
    enabled = true
  }
}
