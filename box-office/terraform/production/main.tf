provider "aws" {
  region     = "us-west-2"
  profile    = "twitch-box-office-aws"
}

module "box-office" {
  source                     = "../modules/box-office"
  account_id                 = "964932369655"
  owner                      = "twitch-box-office-aws@amazon.com"
  env                        = "prod"
  dns                        = "box-office-prod.internal.justin.tv"
  vpc_id                     = "vpc-8b9ce9f2"
  private_subnets            = ["subnet-90835be9", "subnet-30cc377b", "subnet-28a92d72"]
  sg_id                      = "sg-b6ebecc9"
  instance_type              = "c5.xlarge"
  min_size                   = 84
  max_size                   = 120
  asg_canary_size            = 1
  root_volume_size           = 100
  redis5_instance_type        = "cache.r5.2xlarge"
  redis5_groups               = "20"
  redis5_replicas_per_group   = "2"
  ru_min_instance_in_service = 1
  ru_max_batch_size          = 1
  eb_deployment_percent      = 25
  internal_alb_dns_name      = "internal-awseb-e-w-AWSEBLoa-1XP5SDPN289XD-1926117417.us-west-2.elb.amazonaws.com"

  vpc_endpoint_service_allowed_principals   = [
    "arn:aws:iam::761942825647:root", // twitch-clips-workers-dev
    "arn:aws:iam::989252031013:root", // twitch-clips-workers-prod
    "arn:aws:iam::085436627129:root", // twitch-shoryuken-aws
    "arn:aws:iam::450476180835:root", // twitch-clips-workers-prod-new
    "arn:aws:iam::787149559823:root", // twitch-graphql-prod
    "arn:aws:iam::848744099708:root", // twitch-video-aws
    "arn:aws:iam::948329120781:root", // twitch-clips-aws
    "arn:aws:iam::028439334451:root", // twitch-api-prod
    "arn:aws:iam::763322040041:root", // twitch-video-clients-ci
  ]
}

resource "aws_ssm_parameter" "canary-primary-cache-redis5-percentage" {
  name = "canary-primary-cache-redis5-percentage"
  type = "String"
  value = "0.0"

  lifecycle {
    ignore_changes = ["value"]
  }
}

resource "aws_ssm_parameter" "prod-primary-cache-redis5-percentage" {
  name = "prod-primary-cache-redis5-percentage"
  type = "String"
  value = "0.0"

  lifecycle {
    ignore_changes = ["value"]
  }
}

resource "aws_ssm_parameter" "canary-fallback-cache-redis5-percentage" {
  name = "canary-fallback-cache-redis5-percentage"
  type = "String"
  value = "0.0"

  lifecycle {
    ignore_changes = ["value"]
  }
}

resource "aws_ssm_parameter" "prod-fallback-cache-redis5-percentage" {
  name = "prod-fallback-cache-redis5-percentage"
  type = "String"
  value = "0.0"

  lifecycle {
    ignore_changes = ["value"]
  }
}
