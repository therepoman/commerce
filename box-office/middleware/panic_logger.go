package middleware

import (
	"net/http"
	"runtime/debug"

	log "github.com/sirupsen/logrus"
)

// PanicLogger logs panics as errors and prints stack trace. Note that it does
// not do any recovery. Recovery is handled by Twirp.
func PanicLogger(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				log.WithField("stack", string(debug.Stack())).Errorf("Panic encountered: %+v", err)
			}
		}()

		h.ServeHTTP(w, r)
	})
}
