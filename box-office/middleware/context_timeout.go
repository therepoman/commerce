package middleware

import (
	"context"
	"net/http"
	"time"
)

const (
	contextTimeout = 3 * time.Second
)

// ContextTimeout sets a context timeout on all requests
func ContextTimeout(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithTimeout(r.Context(), contextTimeout)
		defer cancel()
		r = r.WithContext(ctx)
		h.ServeHTTP(w, r)
	})
}
