package loaders

import (
	"context"
	"errors"
	"net/url"

	"code.justin.tv/cb/hallpass/view"
	"code.justin.tv/commerce/box-office/internal/clients"
	"code.justin.tv/foundation/twitchclient"

	"github.com/nicksrandall/dataloader"
	gerrors "github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

const (
	channelIDParam = "channelID"
	editorIDParam  = "editorID"
)

// PermissionsLoader loads user permissions such as IsEditor
type PermissionsLoader struct {
	Clients *clients.Clients
}

// Attach returns the data loader function
func (v *PermissionsLoader) Attach(ctx context.Context) dataloader.BatchFunc {
	return func(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
		result := make([]*dataloader.Result, 0, len(keys))
		for _, key := range keys {
			vals, err := url.ParseQuery(key.String())
			if err != nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: gerrors.Wrap(err, "Error parsing values"),
				})
				continue
			}

			channelID := vals.Get(channelIDParam)
			editorID := vals.Get(editorIDParam)

			log.WithFields(log.Fields{
				"channelID": channelID,
				"editorID":  editorID,
			}).Debug("Calling Hallpass::GetV1IsEditor")

			isEditorResponse, err := v.Clients.Hallpass.GetV1IsEditor(ctx, channelID, editorID, &twitchclient.ReqOpts{StatSampleRate: 1.0})
			if err != nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: gerrors.Wrapf(err, "Error calling Hallpass with channelID: %v, editorID: %v", channelID, editorID),
				})
				continue
			}

			result = append(result, &dataloader.Result{
				Data:  isEditorResponse,
				Error: nil,
			})

		}
		return result
	}
}

// LoadPermissions loads user permissions such as IsEditor
func LoadPermissions(ctx context.Context, channelID string, editorID string) (*view.GetIsEditorResponse, error) {
	if channelID == "" || editorID == "" {
		return nil, errors.New("channelID and editorID must not be empty")
	}
	vals := url.Values{}
	vals.Set(channelIDParam, channelID)
	vals.Set(editorIDParam, editorID)

	data, err := loadData(ctx, Permissions, vals.Encode())
	if err != nil || data == nil {
		return nil, err
	}

	obj, ok := data.(*view.GetIsEditorResponse)
	if !ok {
		return nil, errors.New("unexpected loader response")
	}
	return obj, nil
}
