package loaders

import (
	"context"
	"errors"
	"net/url"

	"code.justin.tv/commerce/box-office/internal/clients"
	"code.justin.tv/commerce/box-office/internal/stats"
	nitro "code.justin.tv/samus/nitro/rpc"

	gerrors "github.com/pkg/errors"

	log "github.com/sirupsen/logrus"

	"github.com/nicksrandall/dataloader"
)

const (
	nitroServiceName             = "nitro"
	getPremiumStatusesMethodName = "GetPremiumStatuses"
)

// PremiumLoader loads Prime+Turbo status for a user
type PremiumLoader struct {
	Clients *clients.Clients
}

// Attach returns the data loader function
func (v *PremiumLoader) Attach(ctx context.Context) dataloader.BatchFunc {
	return func(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
		result := make([]*dataloader.Result, 0, len(keys))
		for _, key := range keys {
			vals, err := url.ParseQuery(key.String())
			if err != nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: gerrors.Wrap(err, "Error parsing values"),
				})
				continue
			}

			userID := vals.Get(userIDParam)

			log.WithFields(log.Fields{
				"userID": userID,
			}).Debug("Calling Nitro::GetPremiumStatuses")

			req := &nitro.GetPremiumStatusesRequest{
				TwitchUserID: userID,
			}

			ctx = stats.WithStats(ctx, nitroServiceName, getPremiumStatusesMethodName)
			resp, err := v.Clients.Nitro.GetPremiumStatuses(ctx, req)
			if err != nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: gerrors.Wrapf(err, "Error calling Nitro::GetPremiumStatuses for userID: %v", userID),
				})
				continue
			}

			result = append(result, &dataloader.Result{
				Data:  resp,
				Error: nil,
			})
		}
		return result
	}
}

// LoadPremium requests Prime status for a user
func LoadPremium(ctx context.Context, userID string) (*nitro.GetPremiumStatusesResponse, error) {
	if userID == "" {
		return nil, errors.New("userID input must not be empty")
	}
	vals := url.Values{}
	vals.Set(userIDParam, userID)

	data, err := loadData(ctx, Premium, vals.Encode())
	if err != nil || data == nil {
		return nil, err
	}

	obj, ok := data.(*nitro.GetPremiumStatusesResponse)
	if !ok {
		return nil, errors.New("unexpected loader response")
	}
	return obj, nil
}
