package loaders

import (
	"context"
	"errors"
	"net/url"

	"code.justin.tv/commerce/box-office/internal/clients"

	gerrors "github.com/pkg/errors"

	log "github.com/sirupsen/logrus"

	"code.justin.tv/web/users-service/client/channels"
	"github.com/nicksrandall/dataloader"
)

// ChannelPropertiesLoader loads Channel Properties by channel ID
type ChannelPropertiesLoader struct {
	Clients *clients.Clients
}

// Attach returns the data loader function
func (v *ChannelPropertiesLoader) Attach(ctx context.Context) dataloader.BatchFunc {
	return func(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
		result := make([]*dataloader.Result, 0, len(keys))
		for _, key := range keys {
			vals, err := url.ParseQuery(key.String())
			if err != nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: gerrors.Wrap(err, "Error parsing values"),
				})
				continue
			}

			channelID := vals.Get(channelIDParam)

			log.WithFields(log.Fields{
				"channelID": channelID,
			}).Debug("Calling Channels::Get")

			channelResponse, err := v.Clients.Channels.Get(ctx, channelID, nil)
			if err != nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: gerrors.Wrapf(err, "Error calling Channels with ID: %v", channelID),
				})
				continue
			}

			result = append(result, &dataloader.Result{
				Data:  channelResponse,
				Error: nil,
			})

		}
		return result
	}
}

// LoadChannelProperties loads Channel Properties by channel ID
func LoadChannelProperties(ctx context.Context, channelID string) (*channels.Channel, error) {
	if channelID == "" {
		return nil, errors.New("channelID must not be empty")
	}
	vals := url.Values{}
	vals.Set(channelIDParam, channelID)

	data, err := loadData(ctx, ChannelProperties, vals.Encode())
	if err != nil || data == nil {
		return nil, err
	}

	obj, ok := data.(*channels.Channel)
	if !ok {
		return nil, errors.New("unexpected loader response")
	}
	return obj, nil
}
