package loaders

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/box-office/internal/clients"
	"code.justin.tv/commerce/box-office/internal/loaders/helpers"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"github.com/nicksrandall/dataloader"
	"github.com/pkg/errors"
)

// UserChannelAuthorizationsLoader loads user channel authorization messages from Nioh
type UserChannelAuthorizationsLoader struct {
	Clients *clients.Clients
}

// Attach returns the UserChannelAuthorization batch loader function
func (u *UserChannelAuthorizationsLoader) Attach(ctx context.Context) dataloader.BatchFunc {
	return helpers.NewUserAuthorizationBatchFunc(ctx, u.Clients.Nioh)
}

// ChannelAuthorizationKey is the dataloader.Key used to load a user channel authorization via Nioh.
type ChannelAuthorizationKey struct {
	UserID         string
	ChannelID      string
	ConsumePreview bool
	Origin         string
}

// implement dataloader.Key
func (c *ChannelAuthorizationKey) String() string {
	return fmt.Sprintf("channel-authorization:%s:%s:%t", c.UserID, c.ChannelID, c.ConsumePreview)
}

// implement dataloader.Key
func (c *ChannelAuthorizationKey) Raw() interface{} {
	return c
}

// implement helpers.NiohUserAuthorizationDataKey
func (c *ChannelAuthorizationKey) AsNiohRequest() (*nioh.GetUserAuthorizationRequest, error) {
	if !c.isValid() {
		return nil, errors.New("all fields of ChannelAuthorizationKey must be populated to generate Nioh request")
	}

	return &nioh.GetUserAuthorizationRequest{
		UserId: c.UserID,
		Resource: &nioh.GetUserAuthorizationRequest_Channel{
			Channel: &nioh.Channel{
				Id: c.ChannelID,
			},
		},
		ConsumePreview: c.ConsumePreview,
		Origin:         c.Origin,
	}, nil
}

func (c *ChannelAuthorizationKey) isValid() bool {
	return c.ChannelID != ""
}

func LoadUserChannelAuthorization(ctx context.Context, userID string, channelID string, consumePreview bool, origin string) (*nioh.GetUserAuthorizationResponse, error) {
	if channelID == "" {
		return nil, errors.New("channelID cannot be empty")
	}

	key := &ChannelAuthorizationKey{
		UserID:         userID,
		ChannelID:      channelID,
		ConsumePreview: consumePreview,
		Origin:         origin,
	}

	loader, err := extract(ctx, UserChannelAuthorizations)
	if err != nil {
		return nil, errors.Wrap(err, "failed to extract UserChannelAuthorizations loader")
	}

	thunk := loader.Load(ctx, key)

	data, err := thunk()
	if err != nil {
		return nil, err
	}

	response, ok := data.(*nioh.GetUserAuthorizationResponse)
	if !ok {
		return nil, errors.New("unexpected loader response")
	}

	return response, nil
}
