package loaders

import (
	"context"
	"net/url"

	"code.justin.tv/commerce/box-office/internal/clients"
	"code.justin.tv/vod/vodapi/rpc/vodapi"

	"github.com/nicksrandall/dataloader"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

const (
	vodIDParam = "vodID"
)

// VODByIDLoader loads VOD metadata by ID
type VODByIDLoader struct {
	Clients *clients.Clients
}

// Attach returns the data loader function
func (v *VODByIDLoader) Attach(ctx context.Context) dataloader.BatchFunc {
	return func(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
		result := make([]*dataloader.Result, 0, len(keys))
		for _, key := range keys {
			vals, err := url.ParseQuery(key.String())
			if err != nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: errors.Wrap(err, "Error parsing values"),
				})
				continue
			}

			vodID := vals.Get(vodIDParam)

			log.WithFields(log.Fields{"vodID": vodID}).Debug("Calling VodAPI.InternalGetVodByID")

			request := &vodapi.InternalGetVodByIDRequest{
				VodId: vodID,
			}

			vodResponse, err := v.Clients.VodAPI.InternalGetVodByID(ctx, request)
			if err != nil {
				twirpErr, ok := err.(twirp.Error)
				if ok && twirpErr.Code() == twirp.NotFound {
					result = append(result, &dataloader.Result{})
				} else {
					result = append(result, &dataloader.Result{
						Data:  nil,
						Error: errors.Wrapf(err, "Error calling VodAPI with vodID: %v", vodID),
					})
				}
				continue
			}

			result = append(result, &dataloader.Result{
				Data:  vodResponse.GetVod(),
				Error: nil,
			})

		}
		return result
	}
}

// LoadVODByID loads VOD metadata by ID
func LoadVODByID(ctx context.Context, vodID string) (*vodapi.Vod, error) {
	if vodID == "" {
		return nil, errors.New("vodID must not be empty")
	}
	vals := url.Values{}
	vals.Set(vodIDParam, vodID)

	data, err := loadData(ctx, VODByID, vals.Encode())
	if err != nil || data == nil {
		return nil, err
	}

	obj, ok := data.(*vodapi.Vod)
	if !ok {
		return nil, errors.New("unexpected response from vodapi")
	}

	return obj, nil
}
