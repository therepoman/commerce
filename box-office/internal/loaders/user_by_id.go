package loaders

import (
	"context"
	"errors"
	"net/url"

	"code.justin.tv/commerce/box-office/internal/clients"

	gerrors "github.com/pkg/errors"

	log "github.com/sirupsen/logrus"

	"code.justin.tv/web/users-service/models"
	"github.com/nicksrandall/dataloader"
)

// UserByIDLoader loads Users data by ID
type UserByIDLoader struct {
	Clients *clients.Clients
}

// Attach returns the data loader function
func (v *UserByIDLoader) Attach(ctx context.Context) dataloader.BatchFunc {
	return func(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
		result := make([]*dataloader.Result, 0, len(keys))
		for _, key := range keys {
			vals, err := url.ParseQuery(key.String())
			if err != nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: gerrors.Wrap(err, "Error parsing values"),
				})
				continue
			}

			userID := vals.Get(userIDParam)

			log.WithFields(log.Fields{
				"userID": userID,
			}).Debug("Calling UsersService::GetUserByID")

			userResponse, err := v.Clients.Users.GetUserByID(ctx, userID, nil)
			if err != nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: gerrors.Wrapf(err, "Error calling user service with ID: %v", userID),
				})
				continue
			}

			result = append(result, &dataloader.Result{
				Data:  userResponse,
				Error: nil,
			})

		}
		return result
	}
}

// LoadUserByID loads user details from the input userID
func LoadUserByID(ctx context.Context, userID string) (*models.Properties, error) {
	if userID == "" {
		return nil, errors.New("userID must not be empty")
	}
	vals := url.Values{}
	vals.Set(userIDParam, userID)

	data, err := loadData(ctx, UserByID, vals.Encode())
	if err != nil || data == nil {
		return nil, err
	}

	obj, ok := data.(*models.Properties)
	if !ok {
		return nil, errors.New("unexpected loader response")
	}
	return obj, nil
}
