package loaders_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/vod/tailor/rpc/tailor"

	"code.justin.tv/commerce/box-office/internal/clients"
	. "code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/mocks"

	"code.justin.tv/commerce/box-office/internal/test"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestChannelTrailerLoader(t *testing.T) {
	Convey("Test LoadChannelTrailer", t, func() {
		Convey("Happy case", func() {
			tailorResp := &tailor.PublicGetChannelTrailerResponse{
				VodId: vodID,
			}

			context := test.SetupLoaderContext(context.Background(), ChannelTrailer, tailorResp, nil)
			result, err := LoadChannelTrailer(context, channelID)
			So(err, ShouldBeNil)
			So(result.VodId, ShouldEqual, vodID)
		})

		Convey("Missing channelID input", func() {
			tailorResp := &tailor.PublicGetChannelTrailerResponse{
				VodId: vodID,
			}

			context := test.SetupLoaderContext(context.Background(), ChannelTrailer, tailorResp, nil)
			_, err := LoadChannelTrailer(context, "")
			So(err, ShouldNotBeNil)
		})

		Convey("Error in loader", func() {
			context := test.SetupLoaderContext(context.Background(), ChannelTrailer, nil, errors.New("Mock Error"))
			_, err := LoadChannelTrailer(context, channelID)
			So(err, ShouldNotBeNil)
		})

		Convey("Unexpected response type", func() {
			tailorResp := "Unexpected response"
			context := test.SetupLoaderContext(context.Background(), ChannelTrailer, tailorResp, nil)
			_, err := LoadChannelTrailer(context, channelID)
			So(err, ShouldNotBeNil)
		})
	})

	Convey("Test ChannelTrailerLoader", t, func() {
		mockTailorClient := new(mocks.Tailor)

		clients := &clients.Clients{
			Tailor: mockTailorClient,
		}

		loader := &ChannelTrailerLoader{
			Clients: clients,
		}

		Convey("Happy case", func() {
			tailorResp := &tailor.PublicGetChannelTrailerResponse{
				VodId: vodID,
			}
			mockTailorClient.On("PublicGetChannelTrailer", mock.Anything, mock.Anything, mock.Anything).Return(tailorResp, nil)

			data, err := test.RunLoader(loader, "test")
			So(err, ShouldBeNil)

			tailorData, ok := data.(*tailor.PublicGetChannelTrailerResponse)
			So(ok, ShouldBeTrue)
			So(tailorData.VodId, ShouldEqual, vodID)
			mockTailorClient.AssertNumberOfCalls(t, "PublicGetChannelTrailer", 1)
		})

		Convey("Call to Tailor fails", func() {
			mockTailorClient.On("PublicGetChannelTrailer", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("Error test"))

			_, err := test.RunLoader(loader, "test")
			So(err, ShouldNotBeNil)
			mockTailorClient.AssertNumberOfCalls(t, "PublicGetChannelTrailer", 1)
		})

		Convey("Bad input params", func() {
			_, err := test.RunLoader(loader, "%?&%?&%&?")
			So(err, ShouldNotBeNil)
			mockTailorClient.AssertNumberOfCalls(t, "PublicGetChannelTrailer", 0)
		})
	})
}
