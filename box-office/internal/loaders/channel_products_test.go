package loaders_test

import (
	"context"
	"errors"
	"testing"

	substwirp "code.justin.tv/revenue/subscriptions/twirp"

	"code.justin.tv/commerce/box-office/internal/clients"
	. "code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/mocks"

	"code.justin.tv/commerce/box-office/internal/test"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestChannelProductsLoader(t *testing.T) {
	Convey("Test ChannelProductsLoader", t, func() {
		Convey("empty products response", func() {
			productsResp := []*substwirp.Product{}
			context := test.SetupLoaderContext(context.Background(), ChannelProducts, productsResp, nil)
			result, err := LoadChannelProducts(context, channelID)
			So(err, ShouldBeNil)
			So(len(result), ShouldEqual, 0)
		})

		Convey("nonempty products response", func() {
			productsResp := []*substwirp.Product{
				{
					Id: productID,
				},
			}
			context := test.SetupLoaderContext(context.Background(), ChannelProducts, productsResp, nil)
			result, err := LoadChannelProducts(context, channelID)
			So(err, ShouldBeNil)
			So(len(result), ShouldEqual, 1)
		})

		Convey("Missing channelID input", func() {
			_, err := LoadChannelProducts(context.Background(), "")
			So(err, ShouldNotBeNil)
		})

		Convey("Error in loader", func() {
			context := test.SetupLoaderContext(context.Background(), ChannelProducts, nil, errors.New("test"))
			_, err := LoadChannelProducts(context, channelID)
			So(err, ShouldNotBeNil)
		})

		Convey("Unexpected response type", func() {
			productsResp := "this is the wrong response type"
			context := test.SetupLoaderContext(context.Background(), ChannelProducts, productsResp, nil)
			_, err := LoadChannelProducts(context, channelID)
			So(err, ShouldNotBeNil)
		})
	})

	Convey("Test ChannelProductsLoader", t, func() {
		mockSubsClient := new(mocks.Subscriptions)

		clients := &clients.Clients{
			Subscriptions: mockSubsClient,
		}

		loader := &ChannelProductsLoader{
			Clients: clients,
		}

		Convey("Happy case", func() {

			productsResp := &substwirp.GetChannelProductsResponse{
				Products: []*substwirp.Product{
					{
						Id: productID,
					},
				},
			}

			mockSubsClient.On("GetChannelProducts", mock.Anything, mock.Anything).Return(productsResp, nil)
			data, err := test.RunLoader(loader, "test")
			So(err, ShouldBeNil)

			products, ok := data.([]*substwirp.Product)
			So(ok, ShouldBeTrue)
			So(len(products), ShouldEqual, 1)
			So(products[0].Id, ShouldEqual, productID)
			mockSubsClient.AssertNumberOfCalls(t, "GetChannelProducts", 1)
		})

		Convey("Call to Subscriptions fails", func() {
			mockSubsClient.On("GetChannelProducts", mock.Anything, mock.Anything).Return(nil, errors.New("test"))

			_, err := test.RunLoader(loader, "test")
			So(err, ShouldNotBeNil)

			mockSubsClient.AssertNumberOfCalls(t, "GetChannelProducts", 1)
		})

		Convey("Bad input params", func() {
			_, err := test.RunLoader(loader, "%?&%?&%&?")
			So(err, ShouldNotBeNil)
			mockSubsClient.AssertNumberOfCalls(t, "GetChannelProducts", 0)
		})
	})
}
