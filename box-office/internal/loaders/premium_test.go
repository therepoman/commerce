package loaders_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/box-office/internal/clients"
	. "code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/mocks"
	nitro "code.justin.tv/samus/nitro/rpc"

	"code.justin.tv/commerce/box-office/internal/test"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestPremiumLoader(t *testing.T) {
	Convey("Test LoadPremium", t, func() {
		Convey("Happy case", func() {
			premiumResp := &nitro.GetPremiumStatusesResponse{
				HasPrime: true,
				HasTurbo: true,
			}
			context := test.SetupLoaderContext(context.Background(), Premium, premiumResp, nil)
			result, err := LoadPremium(context, userID)
			So(err, ShouldBeNil)
			So(result.HasPrime, ShouldBeTrue)
			So(result.HasTurbo, ShouldBeTrue)
		})

		Convey("Missing userID input", func() {
			_, err := LoadPremium(context.Background(), "")
			So(err, ShouldNotBeNil)
		})

		Convey("Error in loader", func() {
			context := test.SetupLoaderContext(context.Background(), Premium, nil, errors.New("test"))
			_, err := LoadPremium(context, userID)
			So(err, ShouldNotBeNil)
		})

		Convey("Unexpected response type", func() {
			premiumResp := "this is the wrong response type"
			context := test.SetupLoaderContext(context.Background(), Premium, premiumResp, nil)
			_, err := LoadPremium(context, userID)
			So(err, ShouldNotBeNil)
		})
	})

	Convey("Test PremiumLoader", t, func() {
		mockNitroClient := new(mocks.Nitro)

		clients := &clients.Clients{
			Nitro: mockNitroClient,
		}

		loader := &PremiumLoader{
			Clients: clients,
		}

		Convey("Happy case", func() {
			premiumResp := &nitro.GetPremiumStatusesResponse{
				HasPrime: true,
				HasTurbo: true,
			}
			mockNitroClient.On("GetPremiumStatuses", mock.Anything, mock.Anything).Return(premiumResp, nil)

			data, err := test.RunLoader(loader, "test")
			So(err, ShouldBeNil)

			premiumData, ok := data.(*nitro.GetPremiumStatusesResponse)
			So(ok, ShouldBeTrue)
			So(premiumData.HasPrime, ShouldBeTrue)
			So(premiumData.HasTurbo, ShouldBeTrue)
			mockNitroClient.AssertNumberOfCalls(t, "GetPremiumStatuses", 1)
		})

		Convey("Call to Nitro fails", func() {
			mockNitroClient.On("GetPremiumStatuses", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test"))

			_, err := test.RunLoader(loader, "test")
			So(err, ShouldNotBeNil)

			mockNitroClient.AssertNumberOfCalls(t, "GetPremiumStatuses", 1)
		})

		Convey("Bad input params", func() {
			_, err := test.RunLoader(loader, "%?&%?&%&?")
			So(err, ShouldNotBeNil)
			mockNitroClient.AssertNumberOfCalls(t, "GetPremiumStatuses", 0)
		})
	})
}
