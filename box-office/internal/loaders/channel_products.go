package loaders

import (
	"context"
	"errors"
	"net/url"

	"code.justin.tv/commerce/box-office/internal/clients"
	"code.justin.tv/commerce/box-office/internal/stats"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	"github.com/nicksrandall/dataloader"
	gerrors "github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

const (
	getChannelProductsMethodName = "GetChannelProducts"
	subscriptionsServiceName     = "subscriptions"
)

// ChannelProductsLoader loads Channel product data
type ChannelProductsLoader struct {
	Clients *clients.Clients
}

// Attach returns the data loader function
func (v *ChannelProductsLoader) Attach(ctx context.Context) dataloader.BatchFunc {
	return func(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
		result := make([]*dataloader.Result, 0, len(keys))
		for _, key := range keys {
			vals, err := url.ParseQuery(key.String())
			if err != nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: gerrors.Wrap(err, "Error parsing values"),
				})
				continue
			}

			channelID := vals.Get(channelIDParam)

			log.WithFields(log.Fields{
				"channelID": channelID,
			}).Debug("Calling Subscriptions::GetChannelProducts")

			request := &substwirp.GetChannelProductsRequest{
				ChannelId: channelID,
			}

			ctx = stats.WithStats(ctx, subscriptionsServiceName, getChannelProductsMethodName)
			response, err := v.Clients.Subscriptions.GetChannelProducts(ctx, request)

			if err != nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: gerrors.Wrapf(err, "Error calling Subscriptions::GetChannelProducts for channelID: %v", channelID),
				})
				continue
			}

			result = append(result, &dataloader.Result{
				Data:  response.Products,
				Error: nil,
			})

		}
		return result
	}
}

// LoadChannelProducts requests ticket products for a channel
func LoadChannelProducts(ctx context.Context, channelID string) ([]*substwirp.Product, error) {
	if channelID == "" {
		return nil, errors.New("channelID input must not be empty")
	}
	vals := url.Values{}
	vals.Set(channelIDParam, channelID)

	data, err := loadData(ctx, ChannelProducts, vals.Encode())
	if err != nil || data == nil {
		return nil, err
	}

	obj, ok := data.([]*substwirp.Product)
	if !ok {
		return nil, errors.New("unexpected loader response")
	}
	return obj, nil
}
