package loaders_test

import (
	"context"
	"errors"
	"testing"

	dads "code.justin.tv/ads/dads/proto"
	"code.justin.tv/commerce/box-office/internal/clients"
	. "code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/mocks"

	"code.justin.tv/commerce/box-office/internal/test"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	deviceID      = "deviceID"
	countryCode   = "US"
	platform      = "deviceID"
	playerType    = "US"
	turbo         = false
	subscriber    = false
	adsFree       = false
	playerBackend = "mediaplayer"
)

func TestAdsLoader(t *testing.T) {
	Convey("Test LoadServerAds", t, func() {
		Convey("With device ID and country code", func() {
			adsResp := &dads.ShowServerAdsResp{
				Adblock:   true,
				ServerAds: false,
			}
			context := test.SetupLoaderContext(context.Background(), ServerAds, adsResp, nil)
			result, err := LoadServerAds(context, userID, channelID, deviceID, countryCode, platform, playerType, turbo, subscriber, adsFree, playerBackend)
			So(err, ShouldBeNil)
			So(result.Adblock, ShouldBeTrue)
			So(result.ServerAds, ShouldBeFalse)
		})

		Convey("With missing device ID", func() {
			adsResp := &dads.ShowServerAdsResp{
				Adblock:   true,
				ServerAds: false,
			}
			context := test.SetupLoaderContext(context.Background(), ServerAds, adsResp, nil)
			result, err := LoadServerAds(context, userID, channelID, "", countryCode, platform, playerType, turbo, subscriber, adsFree, playerBackend)
			So(err, ShouldBeNil)
			So(result.Adblock, ShouldBeTrue)
		})

		Convey("With missing country code", func() {
			adsResp := &dads.ShowServerAdsResp{
				Adblock:   true,
				ServerAds: false,
			}
			context := test.SetupLoaderContext(context.Background(), ServerAds, adsResp, nil)
			result, err := LoadServerAds(context, userID, channelID, deviceID, "", platform, playerType, turbo, subscriber, adsFree, playerBackend)
			So(err, ShouldBeNil)
			So(result.Adblock, ShouldBeTrue)
		})

		Convey("With error in loader", func() {
			adsResp := &dads.ShowServerAdsResp{
				Adblock:   true,
				ServerAds: false,
			}
			context := test.SetupLoaderContext(context.Background(), ServerAds, adsResp, errors.New("loader error"))
			result, err := LoadServerAds(context, userID, channelID, deviceID, countryCode, platform, playerType, turbo, subscriber, adsFree, playerBackend)
			So(err, ShouldNotBeNil)
			So(result, ShouldBeNil)
		})

		Convey("With unexpected response type", func() {
			adsResp := "unexpected response"
			context := test.SetupLoaderContext(context.Background(), ServerAds, adsResp, nil)
			result, err := LoadServerAds(context, userID, channelID, deviceID, countryCode, platform, playerType, turbo, subscriber, adsFree, playerBackend)
			So(err, ShouldNotBeNil)
			So(result, ShouldBeNil)
		})
	})

	Convey("Test ServerAdsLoader", t, func() {
		mockDadsClient := new(mocks.Dads)

		clients := &clients.Clients{
			Dads: mockDadsClient,
		}

		loader := &ServerAdsLoader{
			Clients: clients,
		}

		Convey("with successful call to Dads", func() {
			adsResp := &dads.ShowServerAdsResp{
				Adblock:   true,
				ServerAds: true,
			}
			mockDadsClient.On("ShowServerAds", mock.Anything, mock.Anything).Return(adsResp, nil)

			data, err := test.RunLoader(loader, "test")
			So(err, ShouldBeNil)

			adsData, ok := data.(*dads.ShowServerAdsResp)
			So(ok, ShouldBeTrue)
			So(adsData.Adblock, ShouldBeTrue)
			So(adsData.Adblock, ShouldBeTrue)
			mockDadsClient.AssertNumberOfCalls(t, "ShowServerAds", 1)
		})

		Convey("with failed call to Dads", func() {
			mockDadsClient.On("ShowServerAds", mock.Anything, mock.Anything).Return(nil, errors.New("dads error"))
			_, err := test.RunLoader(loader, "test")
			So(err, ShouldNotBeNil)
			mockDadsClient.AssertNumberOfCalls(t, "ShowServerAds", 1)
		})

		Convey("with bad input params", func() {
			_, err := test.RunLoader(loader, "!@#$%^&*()")
			So(err, ShouldNotBeNil)
			mockDadsClient.AssertNumberOfCalls(t, "ShowServerAds", 0)
		})
	})
}
