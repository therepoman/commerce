package loaders_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"

	. "github.com/smartystreets/goconvey/convey"
)

const (
	clipSlug = "TestClipSlug"
	clipURI  = "https://TestSourceURI.mp4"
)

func TestClipURILoader(t *testing.T) {
	Convey("Test LoadClipURI", t, func() {
		Convey("when all calls succeed", func() {
			ctx := test.SetupLoaderContext(context.Background(), loaders.ClipURI, clipURI, nil)

			result, err := loaders.LoadClipURI(ctx, clipSlug)
			So(err, ShouldBeNil)
			So(result, ShouldEqual, clipURI)
		})

		Convey("when clip slug is empty", func() {
			ctx := test.SetupLoaderContext(context.Background(), loaders.ClipURI, clipURI, nil)

			_, err := loaders.LoadClipURI(ctx, "")
			So(err, ShouldNotBeNil)
		})

		Convey("when the loader errors", func() {
			ctx := test.SetupLoaderContext(context.Background(), loaders.ClipURI, "", errors.New("error"))

			_, err := loaders.LoadClipURI(ctx, clipSlug)
			So(err, ShouldNotBeNil)
		})
	})
}
