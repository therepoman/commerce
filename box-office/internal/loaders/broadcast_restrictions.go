package loaders

import (
	"context"

	"github.com/nicksrandall/dataloader"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"

	"code.justin.tv/commerce/box-office/internal/clients"
	"code.justin.tv/commerce/nioh/rpc/nioh"
)

type BroadcastRestrictionsLoader struct {
	Clients *clients.Clients
}

func (b *BroadcastRestrictionsLoader) Attach(ctx context.Context) dataloader.BatchFunc {
	return func(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
		result := make([]*dataloader.Result, 0, len(keys))
		resources := make([]*nioh.RestrictionResource, 0, len(keys))

		for _, key := range keys {
			resources = append(resources, &nioh.RestrictionResource{
				Id:   key.String(),
				Type: nioh.ResourceType_BROADCAST,
			})
		}

		log.WithFields(log.Fields{"resources": resources}).Debug("calling Nioh::GetRestrictionsByResources")

		request := &nioh.GetRestrictionsByResourcesRequest{
			IsPlayback: true,
			Resources:  resources,
		}

		resp, err := b.Clients.Nioh.GetRestrictionsByResources(ctx, request)
		if err != nil {
			return fillErrorResponse(keys, err)
		}

		for _, restriction := range resp.Restrictions {
			if restriction.Error != "" {
				result = append(result, &dataloader.Result{
					Error: errors.New(restriction.Error),
				})
			} else {
				result = append(result, &dataloader.Result{
					Data: restriction,
				})
			}
		}

		return result
	}
}

func LoadBroadcastRestriction(ctx context.Context, broadcastID string) (*nioh.RestrictionResourceResponse, error) {
	if broadcastID == "" {
		return nil, errors.New("broadcastID must not be empty")
	}

	data, err := loadData(ctx, BroadcastRestrictions, broadcastID)
	if err != nil || data == nil {
		return nil, err
	}

	obj, ok := data.(*nioh.RestrictionResourceResponse)
	if !ok {
		return nil, errors.New("unexpected loader response")
	}
	return obj, nil
}
