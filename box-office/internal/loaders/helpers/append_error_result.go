package helpers

import "github.com/nicksrandall/dataloader"

// AppendErrorResult is a helper for populating a []*dataloader.Result with error results
func AppendErrorResult(results []*dataloader.Result, err error) []*dataloader.Result {
	return append(results, &dataloader.Result{
		Data:  nil,
		Error: err,
	})
}
