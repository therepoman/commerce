package helpers

import (
	"context"

	"code.justin.tv/commerce/box-office/internal/stats"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"github.com/nicksrandall/dataloader"
	"github.com/pkg/errors"
)

const NiohServiceName = "nioh"
const GetUserAuthorizationMethodName = "GetUserAuthorization"

type NiohUserAuthorizationDataKey interface {
	// AsNiohRequest should return a non-nil error if the implementation cannot return a valid request
	AsNiohRequest() (*nioh.GetUserAuthorizationRequest, error)
}

func NewUserAuthorizationBatchFunc(ctx context.Context, client nioh.API) dataloader.BatchFunc {
	return func(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
		results := make([]*dataloader.Result, 0, len(keys))

		for _, key := range keys {
			authKey, ok := key.(NiohUserAuthorizationDataKey)
			if !ok {
				results = AppendErrorResult(results, errors.New("could not convert dataloader.Key into NiohUserAuthorizationDataKey"))
				continue
			}

			request, err := authKey.AsNiohRequest()
			if err != nil {
				results = AppendErrorResult(results, err)
				continue
			}

			ctx = stats.WithStats(ctx, NiohServiceName, GetUserAuthorizationMethodName)

			response, err := client.GetUserAuthorization(ctx, request)
			if err != nil {
				results = AppendErrorResult(results, errors.Wrapf(err, "error calling GetUserAuthorization. request=%+v", request))
				continue
			}

			results = append(results, &dataloader.Result{
				Data:  response,
				Error: nil,
			})
		}

		return results
	}
}

// GetPreviewOrigin returns a scoped key for the origin of the video request
func GetPreviewOrigin(platform string, playerType string) string {
	if platform == "" || playerType == "" {
		return ""
	}
	return platform + ":" + playerType
}
