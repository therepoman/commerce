package loaders

import (
	"context"
	"errors"

	"github.com/nicksrandall/dataloader"
	log "github.com/sirupsen/logrus"

	"code.justin.tv/commerce/box-office/internal/clients"
	"code.justin.tv/commerce/box-office/internal/stats"
	"code.justin.tv/commerce/nioh/rpc/nioh"
)

const (
	vodRestrictionsServiceName = "nioh"
	getRestrictionMethodName   = "GetRestrictionsByResources"
)

// VodRestrictionsLoader loads VodRestrictions data
type VodRestrictionsLoader struct {
	Clients *clients.Clients
}

// Attach returns the data loader function
func (v *VodRestrictionsLoader) Attach(ctx context.Context) dataloader.BatchFunc {
	return func(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
		result := make([]*dataloader.Result, 0, len(keys))
		resources := make([]*nioh.RestrictionResource, 0, len(keys))

		for _, vodID := range keys {
			resources = append(resources, &nioh.RestrictionResource{
				Id:   vodID.String(),
				Type: nioh.ResourceType_VIDEO,
			})
		}

		log.WithFields(log.Fields{
			"resources": resources,
		}).Debug("Calling Nioh::GetRestrictionsByResources")

		request := &nioh.GetRestrictionsByResourcesRequest{
			IsPlayback: true,
			Resources:  resources,
		}

		ctx = stats.WithStats(ctx, vodRestrictionsServiceName, getRestrictionMethodName)
		resp, err := v.Clients.Nioh.GetRestrictionsByResources(ctx, request)
		if err != nil {
			return fillErrorResponse(keys, err)
		}

		// Nioh returns restrictions in order they are received.
		for _, restriction := range resp.Restrictions {
			if restriction.Error != "" {
				result = append(result, &dataloader.Result{
					Error: errors.New(restriction.Error),
				})
			} else {
				result = append(result, &dataloader.Result{
					Data: restriction,
				})
			}
		}
		return result
	}
}

// LoadVODRestriction requests restrictions for a vod
func LoadVODRestriction(ctx context.Context, vodID string) (*nioh.RestrictionResourceResponse, error) {
	if vodID == "" {
		return nil, errors.New("vodID input must not be empty")
	}

	data, err := loadData(ctx, VodRestrictions, vodID)
	if err != nil || data == nil {
		return nil, err
	}

	obj, ok := data.(*nioh.RestrictionResourceResponse)
	if !ok {
		return nil, errors.New("unexpected loader response")
	}
	return obj, nil
}
