package loaders

import (
	"context"
	"errors"

	"code.justin.tv/vod/tailor/rpc/tailor"

	"code.justin.tv/commerce/box-office/internal/clients"

	gerrors "github.com/pkg/errors"
	log "github.com/sirupsen/logrus"

	"github.com/nicksrandall/dataloader"
)

const (
	channelTrailerServiceName         = "tailor"
	publicGetChannelTrailerMethodName = "PublicGetChannelTrailer"
)

// ChannelTrailerLoader loads Channel Trailer data
type ChannelTrailerLoader struct {
	Clients *clients.Clients
}

// Attach returns the data loader function
func (v *ChannelTrailerLoader) Attach(ctx context.Context) dataloader.BatchFunc {
	return func(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
		result := make([]*dataloader.Result, 0, len(keys))

		for _, channelID := range keys {
			log.WithFields(log.Fields{
				"channelID": channelID.String(),
			}).Debug("Calling Tailor::PublicGetChannelTrailer")

			request := &tailor.PublicGetChannelTrailerRequest{
				ChannelId: channelID.String(),
			}

			resp, err := v.Clients.Tailor.PublicGetChannelTrailer(ctx, request)

			if err != nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: gerrors.Wrapf(err, "Error calling Tailor with channelID: %v", channelID),
				})
				continue
			}

			result = append(result, &dataloader.Result{
				Data:  resp,
				Error: nil,
			})
		}
		return result
	}
}

// LoadChannelTrailer loads Channel Trailer data given a channel ID
func LoadChannelTrailer(ctx context.Context, channelID string) (*tailor.PublicGetChannelTrailerResponse, error) {
	if channelID == "" {
		return nil, errors.New("channelID must not be empty")
	}

	data, err := loadData(ctx, ChannelTrailer, channelID)
	if err != nil || data == nil {
		return nil, err
	}

	obj, ok := data.(*tailor.PublicGetChannelTrailerResponse)
	if !ok {
		return nil, errors.New("unexpected loader response")
	}
	return obj, nil
}
