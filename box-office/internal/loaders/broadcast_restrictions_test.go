package loaders_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"
	"code.justin.tv/commerce/nioh/rpc/nioh"

	. "github.com/smartystreets/goconvey/convey"
)

func TestBroadcastRestrictionsLoader(t *testing.T) {
	Convey("TestLoadBroadcastRestriction", t, func() {
		niohResp := &nioh.RestrictionResourceResponse{
			Id: broadcastID,
		}

		Convey("when all calls succeed", func() {
			ctx := test.SetupLoaderContext(context.Background(), loaders.BroadcastRestrictions, niohResp, nil)

			result, err := loaders.LoadBroadcastRestriction(ctx, broadcastID)
			So(err, ShouldBeNil)
			So(result.Id, ShouldEqual, broadcastID)
		})

		Convey("when broadcast ID is empty", func() {
			ctx := test.SetupLoaderContext(context.Background(), loaders.BroadcastRestrictions, niohResp, nil)

			_, err := loaders.LoadBroadcastRestriction(ctx, "")
			So(err, ShouldNotBeNil)
		})

		Convey("when the loader errors", func() {
			ctx := test.SetupLoaderContext(context.Background(), loaders.BroadcastRestrictions, nil, errors.New("error"))

			_, err := loaders.LoadBroadcastRestriction(ctx, broadcastID)
			So(err, ShouldNotBeNil)
		})
	})
}
