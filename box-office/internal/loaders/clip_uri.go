package loaders

import (
	"context"
	"errors"
	"net/url"

	"github.com/nicksrandall/dataloader"
	gerrors "github.com/pkg/errors"
	log "github.com/sirupsen/logrus"

	"code.justin.tv/commerce/box-office/internal/clients"
)

const (
	clipSlugParam = "clipSlug"
)

type ClipURILoader struct {
	Clients *clients.Clients
}

func (c *ClipURILoader) Attach(ctx context.Context) dataloader.BatchFunc {
	return func(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
		result := make([]*dataloader.Result, 0, len(keys))

		for _, key := range keys {
			vals, err := url.ParseQuery(key.String())
			if err != nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: gerrors.Wrap(err, "error parsing values"),
				})
				continue
			}

			clipSlug := vals.Get(clipSlugParam)

			log.WithFields(log.Fields{"clipSlug": clipSlug}).Info("Calling Clips::GetClipStatusV3")

			status, err := c.Clients.Clips.GetClipStatusV3(ctx, clipSlug, nil)
			if err != nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: gerrors.Wrapf(err, "error calling clips with slug: %v", clipSlug),
				})
				continue
			}

			log.Infof("Clips::GetClipStatusV3 response: %v", status)

			if len(status.QualityOptions) == 0 {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: gerrors.Wrapf(err, "no clip quality data found for slug: %v", clipSlug),
				})
			} else {
				result = append(result, &dataloader.Result{
					Data:  status.QualityOptions[0].Source,
					Error: nil,
				})
			}
		}

		return result
	}
}

// LoadClipURI returns the URI of the clip's source quality
func LoadClipURI(ctx context.Context, clipSlug string) (string, error) {
	if clipSlug == "" {
		return "", errors.New("clip slug must not be empty")
	}

	vals := url.Values{}
	vals.Set(clipSlugParam, clipSlug)

	data, err := loadData(ctx, ClipURI, vals.Encode())
	if err != nil || data == nil {
		return "", err
	}

	obj, ok := data.(string)
	if !ok {
		return "", errors.New("unexpected loader response")
	}
	return obj, nil
}
