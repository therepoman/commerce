package loaders_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/vod/vodapi/rpc/vodapi"

	"code.justin.tv/commerce/box-office/internal/clients"
	. "code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/mocks"

	"code.justin.tv/commerce/box-office/internal/test"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	vodID = "vodID"
)

var (
	qualities = []string{"a", "b", "c"}
)

func TestVODByIDLoader(t *testing.T) {
	Convey("Test LoadVODByID", t, func() {
		Convey("Happy case", func() {
			vodapiResp := &vodapi.Vod{
				Qualities: qualities,
			}

			context := test.SetupLoaderContext(context.Background(), VODByID, vodapiResp, nil)
			result, err := LoadVODByID(context, vodID)
			So(err, ShouldBeNil)
			So(len(result.Qualities), ShouldEqual, len(qualities))
		})

		Convey("Missing vodID input", func() {
			vodapiResp := &vodapi.Vod{
				Qualities: qualities,
			}

			context := test.SetupLoaderContext(context.Background(), VODByID, vodapiResp, nil)
			_, err := LoadVODByID(context, "")
			So(err, ShouldNotBeNil)
		})

		Convey("Error in loader", func() {
			context := test.SetupLoaderContext(context.Background(), VODByID, nil, errors.New("test"))
			_, err := LoadVODByID(context, vodID)
			So(err, ShouldNotBeNil)
		})

		Convey("Unexpected response type", func() {
			vodapiResp := "this is the wrong response type"
			context := test.SetupLoaderContext(context.Background(), VODByID, vodapiResp, nil)
			_, err := LoadVODByID(context, vodID)
			So(err, ShouldNotBeNil)
		})
	})

	Convey("Test VODByIDLoader", t, func() {
		mockVodApiClient := new(mocks.VodApi)

		clients := &clients.Clients{
			VodAPI: mockVodApiClient,
		}

		loader := &VODByIDLoader{
			Clients: clients,
		}

		Convey("Happy case", func() {
			vodapiResp := &vodapi.InternalGetVodByIDResponse{
				Vod: &vodapi.Vod{
					Qualities: qualities,
				},
			}
			mockVodApiClient.On("InternalGetVodByID", mock.Anything, mock.Anything, mock.Anything).Return(vodapiResp, nil)

			data, err := test.RunLoader(loader, "test")
			So(err, ShouldBeNil)

			vodapiData, ok := data.(*vodapi.Vod)
			So(ok, ShouldBeTrue)
			So(len(vodapiData.Qualities), ShouldEqual, len(qualities))
			mockVodApiClient.AssertNumberOfCalls(t, "InternalGetVodByID", 1)
		})

		Convey("Call to VodApi fails", func() {
			mockVodApiClient.On("InternalGetVodByID", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test"))

			_, err := test.RunLoader(loader, "test")
			So(err, ShouldNotBeNil)
			mockVodApiClient.AssertNumberOfCalls(t, "InternalGetVodByID", 1)
		})

		Convey("Bad input params", func() {
			_, err := test.RunLoader(loader, "%?&%?&%&?")
			So(err, ShouldNotBeNil)
			mockVodApiClient.AssertNumberOfCalls(t, "GetVodByID", 0)
		})
	})
}
