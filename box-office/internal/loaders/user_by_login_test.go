package loaders_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/box-office/internal/clients"
	. "code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/mocks"

	"code.justin.tv/commerce/box-office/internal/test"
	"code.justin.tv/web/users-service/models"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestUserByLoginLoader(t *testing.T) {
	Convey("Test LoadUserByLogin", t, func() {

		Convey("Happy case", func() {
			userResp := &models.Properties{
				ID: userID,
			}
			context := test.SetupLoaderContext(context.Background(), UserByLogin, userResp, nil)
			result, err := LoadUserByLogin(context, userLogin)
			So(err, ShouldBeNil)
			So(result.ID, ShouldEqual, userID)
		})

		Convey("Missing userLogin input", func() {
			userResp := &models.Properties{
				ID: userID,
			}
			context := test.SetupLoaderContext(context.Background(), UserByLogin, userResp, nil)
			_, err := LoadUserByLogin(context, "")
			So(err, ShouldNotBeNil)
		})

		Convey("Error in loader", func() {
			context := test.SetupLoaderContext(context.Background(), UserByLogin, nil, errors.New("test"))
			_, err := LoadUserByLogin(context, userLogin)
			So(err, ShouldNotBeNil)
		})

		Convey("Unexpected response type", func() {
			userResp := "this is the wrong response type"
			context := test.SetupLoaderContext(context.Background(), UserByLogin, userResp, nil)
			_, err := LoadUserByLogin(context, userLogin)
			So(err, ShouldNotBeNil)
		})
	})

	Convey("Test UserByLoginLoader", t, func() {
		mockUsersClient := new(mocks.InternalClient)

		clients := &clients.Clients{
			Users: mockUsersClient,
		}

		loader := &UserByLoginLoader{
			Clients: clients,
		}

		Convey("Happy case", func() {
			userResp := &models.Properties{
				ID: userID,
			}
			mockUsersClient.On("GetUserByLogin", mock.Anything, mock.Anything, mock.Anything).Return(userResp, nil)

			data, err := test.RunLoader(loader, "test")
			So(err, ShouldBeNil)

			userData, ok := data.(*models.Properties)
			So(ok, ShouldBeTrue)
			So(userData.ID, ShouldEqual, userID)
			mockUsersClient.AssertNumberOfCalls(t, "GetUserByLogin", 1)
		})

		Convey("Call to UsersClient fails", func() {
			mockUsersClient.On("GetUserByLogin", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test"))

			_, err := test.RunLoader(loader, "test")
			So(err, ShouldNotBeNil)
			mockUsersClient.AssertNumberOfCalls(t, "GetUserByLogin", 1)
		})

		Convey("Bad input params", func() {
			_, err := test.RunLoader(loader, "%?&%?&%&?")
			So(err, ShouldNotBeNil)
			mockUsersClient.AssertNumberOfCalls(t, "GetUserByLogin", 0)
		})
	})
}
