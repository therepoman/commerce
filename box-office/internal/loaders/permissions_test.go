package loaders_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/cb/hallpass/view"

	"code.justin.tv/commerce/box-office/internal/clients"
	. "code.justin.tv/commerce/box-office/internal/loaders"

	"code.justin.tv/commerce/box-office/internal/test"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	users_mocks "code.justin.tv/commerce/box-office/mocks/users"
)

const (
	channelID = "12345"
	editorID  = "6789"
)

func TestPermissionsLoader(t *testing.T) {
	Convey("Test LoadPermissions", t, func() {
		Convey("Happy case", func() {
			permsResp := &view.GetIsEditorResponse{
				IsEditor: true,
			}
			context := test.SetupLoaderContext(context.Background(), Permissions, permsResp, nil)
			result, err := LoadPermissions(context, channelID, editorID)
			So(err, ShouldBeNil)
			So(result.IsEditor, ShouldEqual, true)
		})

		Convey("Missing channelID input", func() {
			permsResp := &view.GetIsEditorResponse{
				IsEditor: true,
			}
			context := test.SetupLoaderContext(context.Background(), Permissions, permsResp, nil)
			_, err := LoadPermissions(context, "", editorID)
			So(err, ShouldNotBeNil)
		})

		Convey("Missing editorID input", func() {
			permsResp := &view.GetIsEditorResponse{
				IsEditor: true,
			}
			context := test.SetupLoaderContext(context.Background(), Permissions, permsResp, nil)
			_, err := LoadPermissions(context, channelID, "")
			So(err, ShouldNotBeNil)
		})

		Convey("Error in loader", func() {
			context := test.SetupLoaderContext(context.Background(), Permissions, nil, errors.New("test"))
			_, err := LoadPermissions(context, channelID, editorID)
			So(err, ShouldNotBeNil)
		})

		Convey("Unexpected response type", func() {
			permsResp := "this is the wrong response type"
			context := test.SetupLoaderContext(context.Background(), Permissions, permsResp, nil)
			_, err := LoadPermissions(context, channelID, editorID)
			So(err, ShouldNotBeNil)
		})
	})

	Convey("Test PermissionsLoader", t, func() {
		mockHallpassClient := new(users_mocks.Client)

		clients := &clients.Clients{
			Hallpass: mockHallpassClient,
		}

		loader := &PermissionsLoader{
			Clients: clients,
		}

		Convey("Happy case", func() {
			permsResp := &view.GetIsEditorResponse{
				IsEditor: true,
			}
			mockHallpassClient.On("GetV1IsEditor", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(permsResp, nil)

			data, err := test.RunLoader(loader, "test")
			So(err, ShouldBeNil)

			permsData, ok := data.(*view.GetIsEditorResponse)
			So(ok, ShouldBeTrue)
			So(permsData.IsEditor, ShouldBeTrue)
			mockHallpassClient.AssertNumberOfCalls(t, "GetV1IsEditor", 1)
		})

		Convey("Call to Hallpass fails", func() {
			mockHallpassClient.On("GetV1IsEditor", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test"))

			_, err := test.RunLoader(loader, "test")
			So(err, ShouldNotBeNil)

			mockHallpassClient.AssertNumberOfCalls(t, "GetV1IsEditor", 1)
		})

		Convey("Bad input params", func() {
			_, err := test.RunLoader(loader, "%?&%?&%&?")
			So(err, ShouldNotBeNil)
			mockHallpassClient.AssertNumberOfCalls(t, "GetV1IsEditor", 0)
		})
	})
}
