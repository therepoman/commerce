package loaders_test

import (
	"context"
	"testing"

	"code.justin.tv/commerce/box-office/internal/clients"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"
	nioh_mocks "code.justin.tv/commerce/box-office/mocks/nioh"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"github.com/nicksrandall/dataloader"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestUserChannelAuthorizationsLoader(t *testing.T) {
	testUserID := "test-user"
	testChannelID := "test-channel"
	testOrigin := ""

	testChannelResource := &nioh.Channel{Id: testChannelID}

	Convey("Test UserChannelAuthorizationsLoader", t, func() {
		mockNiohClient := new(nioh_mocks.API)
		loader := &loaders.UserVODAuthorizationsLoader{
			Clients: &clients.Clients{
				Nioh: mockNiohClient,
			},
		}

		batchFn := loader.Attach(context.Background())
		runBatchFn := func(key dataloader.Key) *dataloader.Result {
			results := batchFn(context.Background(), dataloader.Keys{key})
			return results[0]
		}

		expectedRequest := &nioh.GetUserAuthorizationRequest{
			UserId: testUserID,
			Resource: &nioh.GetUserAuthorizationRequest_Channel{
				Channel: testChannelResource,
			},
		}

		expectedResponse := &nioh.GetUserAuthorizationResponse{
			UserId: testUserID,
			Resource: &nioh.GetUserAuthorizationResponse_Channel{
				Channel: testChannelResource,
			},
		}

		key := &loaders.ChannelAuthorizationKey{
			UserID:    testUserID,
			ChannelID: testChannelID,
		}

		Convey("happy path", func() {
			mockNiohClient.On("GetUserAuthorization", mock.Anything, expectedRequest).Return(expectedResponse, nil)

			result := runBatchFn(key)
			So(result.Error, ShouldBeNil)

			_, ok := result.Data.(*nioh.GetUserAuthorizationResponse)
			So(ok, ShouldBeTrue)

			mockNiohClient.AssertExpectations(t)
		})

		Convey("with an error from Nioh", func() {
			mockNiohClient.On("GetUserAuthorization", mock.Anything, expectedRequest).
				Return(nil, errors.New("RIP it was lit fam"))

			result := runBatchFn(key)
			So(result.Error, ShouldNotBeNil)
			So(result.Data, ShouldBeNil)

			mockNiohClient.AssertExpectations(t)
		})

		Convey("with an invalid key", func() {
			Convey("with empty ChannelID", func() {
				key.ChannelID = ""
			})

			result := runBatchFn(key)
			So(result.Error, ShouldNotBeNil)
			So(result.Data, ShouldBeNil)
		})
	})

	Convey("Test LoadUserChannelAuthorization", t, func() {
		niohResponse := &nioh.GetUserAuthorizationResponse{
			UserId:               testUserID,
			UserIsAuthorized:     false,
			ResourceIsRestricted: false,
			Resource: &nioh.GetUserAuthorizationResponse_Channel{
				Channel: testChannelResource,
			},
		}

		testContext := test.SetupLoaderContext(context.Background(), loaders.UserChannelAuthorizations, niohResponse, nil)

		Convey("happy path", func() {
			result, err := loaders.LoadUserChannelAuthorization(testContext, testUserID, testChannelID, false, testOrigin)
			So(err, ShouldBeNil)
			So(result, ShouldResemble, niohResponse)
		})

		Convey("with empty user ID", func() {
			niohResponse.UserId = ""

			result, err := loaders.LoadUserChannelAuthorization(testContext, "", testChannelID, false, testOrigin)
			So(err, ShouldBeNil)
			So(result, ShouldResemble, niohResponse)
		})

		Convey("with empty channel ID", func() {
			result, err := loaders.LoadUserChannelAuthorization(testContext, testUserID, "", false, testOrigin)
			So(err, ShouldNotBeNil)
			So(result, ShouldBeNil)
		})

		Convey("with an error from Nioh", func() {
			testContext := test.SetupLoaderContext(context.Background(), loaders.UserChannelAuthorizations, nil, errors.New("RIP it was lit fam"))
			result, err := loaders.LoadUserChannelAuthorization(testContext, testUserID, testChannelID, false, testOrigin)
			So(err, ShouldNotBeNil)
			So(result, ShouldBeNil)
		})
	})
}
