package loaders

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/box-office/internal/clients"
	"code.justin.tv/commerce/box-office/internal/loaders/helpers"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"github.com/nicksrandall/dataloader"
	"github.com/pkg/errors"
)

// UserVODAuthorizationsLoader loads user authorization messages from Nioh
type UserVODAuthorizationsLoader struct {
	Clients *clients.Clients
}

// Attach returns the UserVODAuthorizations batch loader function
func (u *UserVODAuthorizationsLoader) Attach(ctx context.Context) dataloader.BatchFunc {
	return helpers.NewUserAuthorizationBatchFunc(ctx, u.Clients.Nioh)
}

// VODAuthorizationKey is the dataloader.Key used to load a user VOD authorization via Nioh.
type VODAuthorizationKey struct {
	UserID, VODID string
}

// implement dataloader.Key
func (v *VODAuthorizationKey) String() string {
	return fmt.Sprintf("vod-authorization:%s:%s", v.UserID, v.VODID)
}

// implement dataloader.Key
func (v *VODAuthorizationKey) Raw() interface{} {
	return v
}

// implement helpers.NiohUserAuthorizationDataKey
func (v *VODAuthorizationKey) AsNiohRequest() (*nioh.GetUserAuthorizationRequest, error) {
	if !v.isValid() {
		return nil, errors.New("all fields of VODAuthorizationKey must be populated to generate Nioh request")
	}

	return &nioh.GetUserAuthorizationRequest{
		UserId: v.UserID,
		Resource: &nioh.GetUserAuthorizationRequest_Vod{
			Vod: &nioh.VOD{
				VideoId: v.VODID,
			},
		},
	}, nil
}

func (v *VODAuthorizationKey) isValid() bool {
	return v.VODID != ""
}

func LoadUserVODAuthorization(ctx context.Context, userID string, vodID string) (*nioh.GetUserAuthorizationResponse, error) {
	// TODO: add some backed-off retries after https://git-aws.internal.justin.tv/commerce/box-office/pull/159 is in

	key := &VODAuthorizationKey{
		UserID: userID,
		VODID:  vodID,
	}

	loader, err := extract(ctx, UserVODAuthorizations)
	if err != nil {
		return nil, errors.Wrap(err, "failed to extract UserVODAuthorizations loader")
	}

	thunk := loader.Load(ctx, key)

	data, err := thunk()
	if err != nil {
		return nil, err
	}

	response, ok := data.(*nioh.GetUserAuthorizationResponse)
	if !ok {
		return nil, errors.New("unexpected loader response")
	}

	return response, nil
}
