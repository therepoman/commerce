package loaders_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/web/users-service/client/channels"

	"code.justin.tv/commerce/box-office/internal/clients"
	. "code.justin.tv/commerce/box-office/internal/loaders"

	"code.justin.tv/commerce/box-office/internal/test"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	channel_mocks "code.justin.tv/commerce/box-office/mocks/channels"
)

const (
	game = "game"
)

func TestChannelPropertiesLoader(t *testing.T) {
	Convey("Test LoadChannelProperties", t, func() {
		Convey("Happy case", func() {
			channelResp := &channels.Channel{
				Game: game,
			}
			context := test.SetupLoaderContext(context.Background(), ChannelProperties, channelResp, nil)
			result, err := LoadChannelProperties(context, userID)
			So(err, ShouldBeNil)
			So(result.Game, ShouldEqual, game)
		})

		Convey("Missing channelID input", func() {
			_, err := LoadChannelProperties(context.Background(), "")
			So(err, ShouldNotBeNil)
		})

		Convey("Error in loader", func() {
			context := test.SetupLoaderContext(context.Background(), ChannelProperties, nil, errors.New("test"))
			_, err := LoadChannelProperties(context, userID)
			So(err, ShouldNotBeNil)
		})

		Convey("Unexpected response type", func() {
			channelResp := "this is the wrong response type"
			context := test.SetupLoaderContext(context.Background(), ChannelProperties, channelResp, nil)
			_, err := LoadChannelProperties(context, channelID)
			So(err, ShouldNotBeNil)
		})
	})

	Convey("Test ChannelPropertiesLoader", t, func() {
		mockChannelsClient := new(channel_mocks.Client)

		clients := &clients.Clients{
			Channels: mockChannelsClient,
		}

		loader := &ChannelPropertiesLoader{
			Clients: clients,
		}

		Convey("Happy case", func() {
			channelsResp := &channels.Channel{
				Game: game,
			}

			mockChannelsClient.On("Get", mock.Anything, mock.Anything, mock.Anything).Return(channelsResp, nil)
			data, err := test.RunLoader(loader, "test")
			So(err, ShouldBeNil)

			channel, ok := data.(*channels.Channel)
			So(ok, ShouldBeTrue)
			So(channel.Game, ShouldEqual, game)
			mockChannelsClient.AssertNumberOfCalls(t, "Get", 1)
		})

		Convey("Call to Subscriptions fails", func() {
			mockChannelsClient.On("Get", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test"))

			_, err := test.RunLoader(loader, "test")
			So(err, ShouldNotBeNil)

			mockChannelsClient.AssertNumberOfCalls(t, "Get", 1)
		})

		Convey("Bad input params", func() {
			_, err := test.RunLoader(loader, "%?&%?&%&?")
			So(err, ShouldNotBeNil)
			mockChannelsClient.AssertNumberOfCalls(t, "Get", 0)
		})

	})
}
