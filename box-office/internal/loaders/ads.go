package loaders

import (
	"context"
	"errors"
	"net/url"
	"strconv"

	"code.justin.tv/commerce/box-office/internal/clients"
	"code.justin.tv/commerce/box-office/internal/stats"

	gerrors "github.com/pkg/errors"

	log "github.com/sirupsen/logrus"

	"github.com/nicksrandall/dataloader"

	dads "code.justin.tv/ads/dads/proto"
)

const (
	deviceIDParam      = "deviceID"
	countryCodeParam   = "countryCode"
	platformParam      = "platform"
	playerTypeParam    = "playerType"
	turboParam         = "turbo"
	subscriberParam    = "subscriber"
	adsFreeParam       = "adsFree"
	playerBackendParam = "playerBackend"

	dadsServiceName            = "dads"
	getAdblockStatusMethodName = "GetAdblockStatus"
)

// ServerAdsLoader loads server ads properties for a request
type ServerAdsLoader struct {
	Clients *clients.Clients
}

// Attach returns the data loader function
func (v *ServerAdsLoader) Attach(ctx context.Context) dataloader.BatchFunc {
	return func(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
		result := make([]*dataloader.Result, 0, len(keys))
		for _, key := range keys {
			vals, err := url.ParseQuery(key.String())
			if err != nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: gerrors.Wrap(err, "Error parsing values"),
				})
				continue
			}

			userID := vals.Get(userIDParam)
			channelID := vals.Get(channelIDParam)
			deviceID := vals.Get(deviceIDParam)
			country := vals.Get(countryCodeParam)
			platform := vals.Get(platformParam)
			playerType := vals.Get(playerTypeParam)
			turboBool, _ := strconv.ParseBool(vals.Get(turboParam))
			subscriberBool, _ := strconv.ParseBool(vals.Get(subscriberParam))
			adsFreeBool, _ := strconv.ParseBool(vals.Get(adsFreeParam))
			playerBackend := vals.Get(playerBackendParam)

			log.WithFields(log.Fields{
				"userID":        userID,
				"channelID":     channelID,
				"deviceID":      deviceID,
				"country":       country,
				"platform":      platform,
				"turbo":         turboBool,
				"subscriber":    subscriberBool,
				"adsFree":       adsFreeBool,
				"playerBackend": playerBackend,
			}).Debug("Calling Dads::ShowServerAds")

			req := &dads.ShowServerAdsReq{
				UserId:        userID,
				ChannelId:     channelID,
				DeviceId:      deviceID,
				Country:       country,
				Platform:      platform,
				PlayerType:    playerType,
				Turbo:         turboBool,
				Subscriber:    subscriberBool,
				AdsFree:       adsFreeBool,
				PlayerBackend: playerBackend,
			}

			ctx = stats.WithStats(ctx, dadsServiceName, getAdblockStatusMethodName)
			resp, err := v.Clients.Dads.ShowServerAds(ctx, req)
			if err != nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: gerrors.Wrapf(err, "Error calling Dads::ShowServerAds"),
				})
				continue
			}

			result = append(result, &dataloader.Result{
				Data:  resp,
				Error: nil,
			})
		}
		return result
	}
}

// LoadServerAds requests server ads status
func LoadServerAds(ctx context.Context, userID string, channelID string, deviceID string, countryCode string, platform string, playerType string, turbo bool, subscriber bool, adsFree bool, playerBackend string) (*dads.ShowServerAdsResp, error) {
	vals := url.Values{}
	vals.Set(userIDParam, userID)
	vals.Set(channelIDParam, channelID)
	vals.Set(deviceIDParam, deviceID)
	vals.Set(countryCodeParam, countryCode)
	vals.Set(platformParam, platform)
	vals.Set(playerTypeParam, playerType)
	vals.Set(turboParam, strconv.FormatBool(turbo))
	vals.Set(subscriberParam, strconv.FormatBool(subscriber))
	vals.Set(adsFreeParam, strconv.FormatBool(adsFree))
	vals.Set(playerBackendParam, playerBackend)

	data, err := loadData(ctx, ServerAds, vals.Encode())
	if err != nil || data == nil {
		return nil, err
	}

	obj, ok := data.(*dads.ShowServerAdsResp)
	if !ok {
		return nil, errors.New("unexpected loader response")
	}
	return obj, nil
}
