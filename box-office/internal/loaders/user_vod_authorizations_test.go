package loaders_test

import (
	"context"
	"testing"

	"github.com/nicksrandall/dataloader"

	"code.justin.tv/commerce/box-office/internal/clients"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"
	nioh_mocks "code.justin.tv/commerce/box-office/mocks/nioh"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestUserVODAuthorizationsLoader(t *testing.T) {
	testUserID := "test-user"
	testVodID := "test-vod"

	testVodResource := &nioh.VOD{
		VideoId: testVodID,
	}

	Convey("Test UserVODAuthorizationsLoader", t, func() {
		mockNiohClient := new(nioh_mocks.API)
		loader := &loaders.UserVODAuthorizationsLoader{
			Clients: &clients.Clients{
				Nioh: mockNiohClient,
			},
		}

		batchFn := loader.Attach(context.Background())
		runBatchFn := func(key dataloader.Key) *dataloader.Result {
			results := batchFn(context.Background(), dataloader.Keys{key})
			return results[0]
		}

		expectedRequest := &nioh.GetUserAuthorizationRequest{
			UserId: testUserID,
			Resource: &nioh.GetUserAuthorizationRequest_Vod{
				Vod: testVodResource,
			},
		}

		expectedResponse := &nioh.GetUserAuthorizationResponse{
			UserId: testUserID,
			Resource: &nioh.GetUserAuthorizationResponse_Vod{
				Vod: testVodResource,
			},
		}

		key := &loaders.VODAuthorizationKey{
			UserID: testUserID,
			VODID:  testVodID,
		}

		Convey("happy path", func() {
			Convey("with anonymous user", func() {
				expectedRequest.UserId = ""
				key.UserID = ""
			})

			mockNiohClient.On("GetUserAuthorization", mock.Anything, expectedRequest).Return(expectedResponse, nil)

			result := runBatchFn(key)
			So(result.Error, ShouldBeNil)

			_, ok := result.Data.(*nioh.GetUserAuthorizationResponse)
			So(ok, ShouldBeTrue)

			mockNiohClient.AssertExpectations(t)
		})

		Convey("with an error from Nioh", func() {
			mockNiohClient.On("GetUserAuthorization", mock.Anything, expectedRequest).
				Return(nil, errors.New("RIP it was lit fam"))

			result := runBatchFn(key)
			So(result.Error, ShouldNotBeNil)
			So(result.Data, ShouldBeNil)

			mockNiohClient.AssertExpectations(t)
		})

		Convey("with an invalid key", func() {
			Convey("with empty VODID", func() {
				key.VODID = ""
			})

			result := runBatchFn(key)
			So(result.Error, ShouldNotBeNil)
			So(result.Data, ShouldBeNil)
		})
	})

	Convey("Test LoadUserVODAuthorization", t, func() {
		niohResponse := &nioh.GetUserAuthorizationResponse{
			UserId:               testUserID,
			UserIsAuthorized:     false,
			ResourceIsRestricted: false,
			Resource: &nioh.GetUserAuthorizationResponse_Vod{
				Vod: testVodResource,
			},
		}

		testContext := test.SetupLoaderContext(context.Background(), loaders.UserVODAuthorizations, niohResponse, nil)

		Convey("happy path", func() {
			Convey("with empty user ID", func() {
				testUserID = ""
			})
			result, err := loaders.LoadUserVODAuthorization(testContext, testUserID, testVodID)
			So(err, ShouldBeNil)
			So(result, ShouldResemble, niohResponse)
		})

		Convey("unhappy paths", func() {
			Convey("with error from loader", func() {
				testContext = test.SetupLoaderContext(context.Background(), loaders.UserVODAuthorizations, nil, errors.New("RIP it was lit fam"))
			})

			result, err := loaders.LoadUserVODAuthorization(testContext, "", testVodID)
			So(err, ShouldNotBeNil)
			So(result, ShouldBeNil)
		})
	})
}
