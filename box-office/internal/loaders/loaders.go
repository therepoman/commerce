package loaders

import (
	"context"
	"errors"
	"fmt"

	"code.justin.tv/commerce/box-office/internal/clients"
	"github.com/nicksrandall/dataloader"
)

const (
	// Data loader keys
	BroadcastRestrictions     LoaderKey = LoaderKey("BroadcastRestrictions")
	ChannelProducts           LoaderKey = LoaderKey("ChannelProducts")
	ChannelProperties         LoaderKey = LoaderKey("ChannelProperties")
	ChannelTrailer            LoaderKey = LoaderKey("ChannelTrailer")
	ClipBroadcastID           LoaderKey = LoaderKey("ClipBroadcastID")
	ClipURI                   LoaderKey = LoaderKey("ClipURI")
	Permissions               LoaderKey = LoaderKey("Permissions")
	Premium                   LoaderKey = LoaderKey("Premium")
	ServerAds                 LoaderKey = LoaderKey("ServerAds")
	Subscriptions             LoaderKey = LoaderKey("Subscriptions")
	UserByLogin               LoaderKey = LoaderKey("UserByLogin")
	UserByID                  LoaderKey = LoaderKey("UserByID")
	VODByID                   LoaderKey = LoaderKey("VODByID")
	VodRestrictions           LoaderKey = LoaderKey("VodRestrictions")
	UserChannelAuthorizations LoaderKey = LoaderKey("UserChannelAuthorizations")
	UserVODAuthorizations     LoaderKey = LoaderKey("UserVODAuthorizations")
)

// LoaderKey uniquely identifies a loader within the map of loaders
type LoaderKey string

// Attacher attaches a context to a dataloader batch function.
// This allows the batch function to leverage context functionality (eg. deadlines, cancellations, request-scoped values)
type Attacher interface {
	Attach(context.Context) dataloader.BatchFunc
}

// Attachers holds a mapping of loader keys to attacher interfaces.
type Attachers struct {
	LoaderMap map[LoaderKey]Attacher
}

// NewAttachers initializes a mapping of context keys to attacher functions.
// The clients are injected into the attacher functions so that the dataloader
// batch functions can use them.
func NewAttachers(clients *clients.Clients) *Attachers {
	return &Attachers{map[LoaderKey]Attacher{
		BroadcastRestrictions:     &BroadcastRestrictionsLoader{Clients: clients},
		ChannelProperties:         &ChannelPropertiesLoader{Clients: clients},
		ChannelProducts:           &ChannelProductsLoader{Clients: clients},
		ChannelTrailer:            &ChannelTrailerLoader{Clients: clients},
		ClipBroadcastID:           &ClipBroadcastIDLoader{Clients: clients},
		ClipURI:                   &ClipURILoader{Clients: clients},
		Permissions:               &PermissionsLoader{Clients: clients},
		Premium:                   &PremiumLoader{Clients: clients},
		ServerAds:                 &ServerAdsLoader{Clients: clients},
		Subscriptions:             &SubscriptionsLoader{Clients: clients},
		UserByLogin:               &UserByLoginLoader{Clients: clients},
		UserByID:                  &UserByIDLoader{Clients: clients},
		VODByID:                   &VODByIDLoader{Clients: clients},
		VodRestrictions:           &VodRestrictionsLoader{Clients: clients},
		UserChannelAuthorizations: &UserChannelAuthorizationsLoader{Clients: clients},
		UserVODAuthorizations:     &UserVODAuthorizationsLoader{Clients: clients},
	}}
}

// Attach batched data loaders to the request context by their respective keys.
func (a *Attachers) Attach(ctx context.Context) context.Context {
	for loaderKey, attacher := range a.LoaderMap {
		loader := dataloader.NewBatchedLoader(attacher.Attach(ctx))
		ctx = context.WithValue(ctx, loaderKey, loader)
	}
	return ctx
}

// extract a dataloader instance from the request context.
func extract(ctx context.Context, loaderKey LoaderKey) (*dataloader.Loader, error) {
	loader, found := ctx.Value(loaderKey).(*dataloader.Loader)
	if !found {
		return nil, fmt.Errorf("unable to find %q loader on the request context", loaderKey)
	}
	return loader, nil
}

// load data for the given dataKey.
func loadData(ctx context.Context, loaderKey LoaderKey, dataKey string) (interface{}, error) {
	if dataKey == "" {
		return nil, errors.New("no data key specified")
	}

	loader, err := extract(ctx, loaderKey)
	if err != nil {
		return nil, err
	}

	loadFn := loader.Load(ctx, dataloader.StringKey(dataKey))
	data, err := loadFn()
	if err != nil {
		return nil, err
	}

	return data, nil
}

// fillErrorResponse creates a dataloader result slice with given keys and fills them with given error
func fillErrorResponse(keys dataloader.Keys, err error) []*dataloader.Result {
	res := make([]*dataloader.Result, len(keys))

	for i := range keys {
		res[i] = &dataloader.Result{
			Error: err,
		}
	}

	return res
}
