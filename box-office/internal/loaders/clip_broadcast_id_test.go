package loaders_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"
	. "github.com/smartystreets/goconvey/convey"
)

var (
	broadcastID = "testBroadcastID"
)

func TestClipBroadcastIDLoader(t *testing.T) {
	Convey("TestLoadClipBroadcastID", t, func() {
		Convey("when all calls succeed", func() {
			ctx := test.SetupLoaderContext(context.Background(), loaders.ClipBroadcastID, &broadcastID, nil)

			result, err := loaders.LoadClipBroadcastID(ctx, clipSlug)
			So(err, ShouldBeNil)
			So(result, ShouldEqual, broadcastID)
		})

		Convey("when clip slug is empty", func() {
			ctx := test.SetupLoaderContext(context.Background(), loaders.ClipBroadcastID, &broadcastID, nil)

			_, err := loaders.LoadClipBroadcastID(ctx, "")
			So(err, ShouldNotBeNil)
		})

		Convey("when the loader errors", func() {
			ctx := test.SetupLoaderContext(context.Background(), loaders.ClipBroadcastID, "", errors.New("error"))

			_, err := loaders.LoadClipBroadcastID(ctx, clipSlug)
			So(err, ShouldNotBeNil)
		})
	})
}
