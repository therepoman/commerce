package loaders_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/nioh/rpc/nioh"

	"code.justin.tv/commerce/box-office/internal/clients"
	. "code.justin.tv/commerce/box-office/internal/loaders"
	niohMock "code.justin.tv/commerce/box-office/mocks/nioh"

	"code.justin.tv/commerce/box-office/internal/test"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestVodRestrictionsLoader(t *testing.T) {
	Convey("Test VodRestrictionsLoader", t, func() {
		restriction := &nioh.RestrictionResourceResponse{
			Id: vodID,
		}

		Convey("Happy case", func() {
			context := test.SetupLoaderContext(context.Background(), VodRestrictions, restriction, nil)
			result, err := LoadVODRestriction(context, vodID)
			So(err, ShouldBeNil)
			So(result, ShouldNotBeNil)
			So(result.Id, ShouldEqual, vodID)
		})

		Convey("Missing vodID input", func() {
			context := test.SetupLoaderContext(context.Background(), VodRestrictions, restriction, nil)
			_, err := LoadVODRestriction(context, "")
			So(err, ShouldNotBeNil)
		})

		Convey("Error in loader", func() {
			context := test.SetupLoaderContext(context.Background(), VodRestrictions, nil, errors.New("test"))
			_, err := LoadVODRestriction(context, vodID)
			So(err, ShouldNotBeNil)
		})

		Convey("Unexpected response type", func() {
			vodRestrictionsResp := "this is the wrong response type"
			context := test.SetupLoaderContext(context.Background(), VodRestrictions, vodRestrictionsResp, nil)
			_, err := LoadVODRestriction(context, vodID)
			So(err, ShouldNotBeNil)
		})
	})

	Convey("Test VodRestrictionsLoader", t, func() {
		mockNiohClient := new(niohMock.API)

		clients := &clients.Clients{
			Nioh: mockNiohClient,
		}

		loader := &VodRestrictionsLoader{
			Clients: clients,
		}

		Convey("Happy case", func() {
			restriction := &nioh.RestrictionResourceResponse{
				Id: vodID,
			}

			response := &nioh.GetRestrictionsByResourcesResponse{
				Restrictions: []*nioh.RestrictionResourceResponse{restriction},
			}

			mockNiohClient.On("GetRestrictionsByResources", mock.Anything, mock.Anything).Return(response, nil)

			data, err := test.RunLoader(loader, "test")
			So(err, ShouldBeNil)

			result, ok := data.(*nioh.RestrictionResourceResponse)
			So(ok, ShouldBeTrue)
			So(result, ShouldNotBeNil)
			So(result.Id, ShouldEqual, vodID)
			mockNiohClient.AssertNumberOfCalls(t, "GetRestrictionsByResources", 1)
		})

		Convey("Call to Nioh fails", func() {
			mockNiohClient.On("GetRestrictionsByResources", mock.Anything, mock.Anything).Return(nil, errors.New("test"))

			_, err := test.RunLoader(loader, "test")
			So(err, ShouldNotBeNil)

			mockNiohClient.AssertNumberOfCalls(t, "GetRestrictionsByResources", 1)
		})

		Convey("Bad input params", func() {
			_, err := test.RunLoader(loader, "%?&%?&%&?")
			So(err, ShouldNotBeNil)
			mockNiohClient.AssertNumberOfCalls(t, "GetRestrictionsByResources", 0)
		})
	})
}
