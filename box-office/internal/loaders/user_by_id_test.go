package loaders_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/box-office/internal/clients"
	. "code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"
	"code.justin.tv/commerce/box-office/mocks"
	"code.justin.tv/web/users-service/models"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	userID = "123456"
)

var (
	userLogin = "testuser"
)

func TestUserByIDLoader(t *testing.T) {
	Convey("Test LoadUserByID", t, func() {

		Convey("Happy case", func() {
			userResp := &models.Properties{
				Login: &userLogin,
			}
			context := test.SetupLoaderContext(context.Background(), UserByID, userResp, nil)
			result, err := LoadUserByID(context, userID)
			So(err, ShouldBeNil)
			So(*result.Login, ShouldEqual, userLogin)
		})

		Convey("Missing userID input", func() {
			userResp := &models.Properties{
				Login: &userLogin,
			}
			context := test.SetupLoaderContext(context.Background(), UserByID, userResp, nil)
			_, err := LoadUserByID(context, "")
			So(err, ShouldNotBeNil)
		})

		Convey("Error in loader", func() {
			context := test.SetupLoaderContext(context.Background(), UserByID, nil, errors.New("test"))
			_, err := LoadUserByID(context, userID)
			So(err, ShouldNotBeNil)
		})

		Convey("Unexpected response type", func() {
			userResp := "this is the wrong response type"
			context := test.SetupLoaderContext(context.Background(), UserByID, userResp, nil)
			_, err := LoadUserByID(context, userID)
			So(err, ShouldNotBeNil)
		})
	})

	Convey("Test UserByIDLoader", t, func() {
		mockUsersClient := new(mocks.InternalClient)

		clients := &clients.Clients{
			Users: mockUsersClient,
		}

		loader := &UserByIDLoader{
			Clients: clients,
		}

		Convey("Happy case", func() {
			userResp := &models.Properties{
				Login: &userLogin,
			}
			mockUsersClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).Return(userResp, nil)

			data, err := test.RunLoader(loader, "test")
			So(err, ShouldBeNil)

			userData, ok := data.(*models.Properties)
			So(ok, ShouldBeTrue)
			So(*userData.Login, ShouldEqual, userLogin)
			mockUsersClient.AssertNumberOfCalls(t, "GetUserByID", 1)
		})

		Convey("Call to UsersClient fails", func() {
			mockUsersClient.On("GetUserByID", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("test"))

			_, err := test.RunLoader(loader, "test")
			So(err, ShouldNotBeNil)
			mockUsersClient.AssertNumberOfCalls(t, "GetUserByID", 1)
		})

		Convey("Bad input params", func() {
			_, err := test.RunLoader(loader, "%?&%?&%&?")
			So(err, ShouldNotBeNil)
			mockUsersClient.AssertNumberOfCalls(t, "GetUserByID", 0)
		})
	})
}
