package loaders

import (
	"context"
	"net/url"

	"code.justin.tv/commerce/box-office/internal/clients"
	"github.com/nicksrandall/dataloader"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

type ClipBroadcastIDLoader struct {
	Clients *clients.Clients
}

func (c *ClipBroadcastIDLoader) Attach(ctx context.Context) dataloader.BatchFunc {
	return func(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
		result := make([]*dataloader.Result, 0, len(keys))

		for _, key := range keys {
			vals, err := url.ParseQuery(key.String())
			if err != nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: errors.Wrap(err, "error parsing values"),
				})
				continue
			}

			clipSlug := vals.Get(clipSlugParam)

			log.WithFields(log.Fields{"clipSlug": clipSlug}).Debug("calling Clips::GetClipInfoV3")

			clip, err := c.Clients.Clips.GetClipInfoV3(ctx, clipSlug, nil, nil)
			if err != nil || clip == nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: errors.Wrapf(err, "error getting clip info for slug: %v", clipSlug),
				})
				continue
			}

			log.Debugf("Clips::GetClipInfoV3 response: %v", clip)

			result = append(result, &dataloader.Result{
				Data:  clip.BroadcastID,
				Error: nil,
			})
		}

		return result
	}
}

func LoadClipBroadcastID(ctx context.Context, clipSlug string) (string, error) {
	if clipSlug == "" {
		return "", errors.New("clipSlug must not be empty")
	}

	vals := url.Values{}
	vals.Set(clipSlugParam, clipSlug)

	data, err := loadData(ctx, ClipBroadcastID, vals.Encode())
	if err != nil || data == nil {
		return "", err
	}

	obj, ok := data.(*string)
	if !ok {
		return "", errors.New("unexpected loader response")
	}

	if obj == nil {
		return "", errors.New("no broadcast ID found for slug")
	}

	return *obj, nil
}
