package loaders

import (
	"context"
	"errors"
	"net/url"

	"code.justin.tv/commerce/box-office/internal/clients"

	gerrors "github.com/pkg/errors"

	log "github.com/sirupsen/logrus"

	"code.justin.tv/web/users-service/models"
	"github.com/nicksrandall/dataloader"
)

const (
	userLoginParam = "userLogin"
)

// UserByLoginLoader loads Users data by login
type UserByLoginLoader struct {
	Clients *clients.Clients
}

// Attach returns the data loader function
func (v *UserByLoginLoader) Attach(ctx context.Context) dataloader.BatchFunc {
	return func(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
		result := make([]*dataloader.Result, 0, len(keys))
		for _, key := range keys {
			vals, err := url.ParseQuery(key.String())
			if err != nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: gerrors.Wrap(err, "Error parsing values"),
				})
				continue
			}

			userLogin := vals.Get(userLoginParam)

			log.WithFields(log.Fields{
				"userLogin": userLogin,
			}).Debug("Calling UsersService::GetUserByLogin")

			userResponse, err := v.Clients.Users.GetUserByLogin(ctx, userLogin, nil)
			if err != nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: gerrors.Wrapf(err, "Error calling user service with login: %v", userLogin),
				})
				continue
			}

			result = append(result, &dataloader.Result{
				Data:  userResponse,
				Error: nil,
			})

		}
		return result
	}
}

// LoadUserByLogin loads user details from the input user login
func LoadUserByLogin(ctx context.Context, userLogin string) (*models.Properties, error) {
	if userLogin == "" {
		return nil, errors.New("userLogin must not be empty")
	}
	vals := url.Values{}
	vals.Set(userLoginParam, userLogin)

	data, err := loadData(ctx, UserByLogin, vals.Encode())
	if err != nil || data == nil {
		return nil, err
	}

	obj, ok := data.(*models.Properties)
	if !ok {
		return nil, errors.New("unexpected loader response")
	}
	return obj, nil
}
