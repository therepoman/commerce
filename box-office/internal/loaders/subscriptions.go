package loaders

import (
	"context"
	"errors"
	"net/url"

	voyager "code.justin.tv/amzn/TwitchVoyagerTwirp"
	"code.justin.tv/commerce/box-office/internal/clients"
	"code.justin.tv/commerce/box-office/internal/stats"
	"github.com/nicksrandall/dataloader"
	gerrors "github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

const (
	getUserChannelSubscriptionMethodName = "GetUserChannelSubscription"
	userIDParam                          = "userID"
	voyagerServiceName                   = "voyager"
)

// SubscriptionsLoader loads Subscriptions data
type SubscriptionsLoader struct {
	Clients *clients.Clients
}

// Attach returns the data loader function
func (v *SubscriptionsLoader) Attach(ctx context.Context) dataloader.BatchFunc {
	return func(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
		result := make([]*dataloader.Result, 0, len(keys))
		for _, key := range keys {
			vals, err := url.ParseQuery(key.String())
			if err != nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: gerrors.Wrap(err, "Error parsing values"),
				})
				continue
			}

			userID := vals.Get(userIDParam)
			channelID := vals.Get(channelIDParam)

			log.WithFields(log.Fields{"userID": userID, "channelID": channelID}).
				Debug("Calling Voyager::GetUserChannelSubscription")

			request := &voyager.GetUserChannelSubscriptionRequest{
				UserId:    userID,
				ChannelId: channelID,
			}

			ctx = stats.WithStats(ctx, voyagerServiceName, getUserChannelSubscriptionMethodName)
			response, err := v.Clients.Voyager.GetUserChannelSubscription(ctx, request)
			if err != nil {
				result = append(result, &dataloader.Result{
					Data:  nil,
					Error: gerrors.Wrapf(err, "Error calling Voyager::GetUserChannelSubscription for user: %v and channelID: %v", userID, channelID),
				})
				continue
			}

			result = append(result, &dataloader.Result{
				Data:  response.Subscription,
				Error: nil,
			})
		}

		return result
	}
}

// LoadSubscription requests subscription status for a user on a channel
func LoadSubscription(ctx context.Context, userID string, channelID string) (*voyager.Subscription, error) {
	if userID == "" || channelID == "" {
		return nil, errors.New("userID and channelID inputs must not be empty")
	}
	vals := url.Values{}
	vals.Set(userIDParam, userID)
	vals.Set(channelIDParam, channelID)

	data, err := loadData(ctx, Subscriptions, vals.Encode())
	if err != nil || data == nil {
		return nil, err
	}

	obj, ok := data.(*voyager.Subscription)
	if !ok {
		return nil, errors.New("unexpected loader response")
	}
	return obj, nil
}
