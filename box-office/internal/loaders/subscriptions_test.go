package loaders_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/box-office/internal/clients"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"
	"code.justin.tv/commerce/box-office/mocks"
	// substwirp "code.justin.tv/revenue/subscriptions/twirp"
	voyager "code.justin.tv/amzn/TwitchVoyagerTwirp"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	productID = "productid"
)

func TestSubscriptionsLoader(t *testing.T) {
	Convey("Test SubscriptionsLoader", t, func() {
		Convey("Happy case", func() {
			subsResp := &voyager.Subscription{
				ProductId: productID,
			}
			context := test.SetupLoaderContext(context.Background(), loaders.Subscriptions, subsResp, nil)
			result, err := loaders.LoadSubscription(context, userID, channelID)
			So(err, ShouldBeNil)
			So(result, ShouldNotBeNil)
			So(result.ProductId, ShouldEqual, productID)
		})

		Convey("Missing channelID input", func() {
			subsResp := &voyager.Subscription{
				ProductId: productID,
			}
			context := test.SetupLoaderContext(context.Background(), loaders.Subscriptions, subsResp, nil)
			_, err := loaders.LoadSubscription(context, userID, "")
			So(err, ShouldNotBeNil)
		})

		Convey("Missing userID input", func() {
			subsResp := &voyager.Subscription{
				ProductId: productID,
			}
			context := test.SetupLoaderContext(context.Background(), loaders.Subscriptions, subsResp, nil)
			_, err := loaders.LoadSubscription(context, "", channelID)
			So(err, ShouldNotBeNil)
		})

		Convey("Error in loader", func() {
			context := test.SetupLoaderContext(context.Background(), loaders.Subscriptions, nil, errors.New("test"))
			_, err := loaders.LoadSubscription(context, userID, channelID)
			So(err, ShouldNotBeNil)
		})

		Convey("Unexpected response type", func() {
			subsResp := "this is the wrong response type"
			context := test.SetupLoaderContext(context.Background(), loaders.Subscriptions, subsResp, nil)
			_, err := loaders.LoadSubscription(context, userID, channelID)
			So(err, ShouldNotBeNil)
		})
	})

	Convey("Test SubscriptionsLoader", t, func() {
		mockVoyagerClient := new(mocks.TwitchVoyager)

		clients := &clients.Clients{
			Voyager: mockVoyagerClient,
		}

		loader := &loaders.SubscriptionsLoader{
			Clients: clients,
		}

		Convey("Happy case", func() {
			subsResp := &voyager.GetUserChannelSubscriptionResponse{
				Subscription: &voyager.Subscription{
					ProductId: productID,
				},
			}
			mockVoyagerClient.On("GetUserChannelSubscription", mock.Anything, mock.Anything).Return(subsResp, nil)

			data, err := test.RunLoader(loader, "test")
			So(err, ShouldBeNil)

			subscription, ok := data.(*voyager.Subscription)
			So(ok, ShouldBeTrue)
			So(subscription, ShouldNotBeNil)
			So(subscription.ProductId, ShouldEqual, productID)
			mockVoyagerClient.AssertNumberOfCalls(t, "GetUserChannelSubscription", 1)
		})

		Convey("Call to Subscriptions fails", func() {
			mockVoyagerClient.On("GetUserChannelSubscription", mock.Anything, mock.Anything).Return(nil, errors.New("test"))

			_, err := test.RunLoader(loader, "test")
			So(err, ShouldNotBeNil)

			mockVoyagerClient.AssertNumberOfCalls(t, "GetUserChannelSubscription", 1)
		})

		Convey("Bad input params", func() {
			_, err := test.RunLoader(loader, "%?&%?&%&?")
			So(err, ShouldNotBeNil)
			mockVoyagerClient.AssertNumberOfCalls(t, "GetUserChannelSubscription", 0)
		})
	})
}
