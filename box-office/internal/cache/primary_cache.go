package cache

import (
	"net/http"
	"time"

	"code.justin.tv/commerce/box-office/config"
	log "github.com/sirupsen/logrus"
)

// PrimaryCacheRoundTripWrapper is a wrapper around an http round tripper which utilizes
// a shared cache in order to store and retrieve dependent service responses. This is
// meant to store service responses so that we can reduce request load on downstream
// services and reduce latency of our own service.
// The caching strategy:
//   - Cache Key: Copy of the http.Request
//   - Cache Value: Copy of the http.Response
type PrimaryCacheRoundTripWrapper struct {
	Next  http.RoundTripper
	Cache IHTTPCache
	TTL   time.Duration
}

// NewPrimaryCacheRoundTripWrapper returns a new PrimaryCacheRoundTripWrapper. The input time-to-live (TTL) is applied to
// all cache Set calls from this wrapper.
func NewPrimaryCacheRoundTripWrapper(cache IHTTPCache, isCacheEnabled bool, ttl time.Duration, cfg *config.Configuration) func(c http.RoundTripper) http.RoundTripper {
	return func(next http.RoundTripper) http.RoundTripper {
		if isCacheEnabled {
			return &PrimaryCacheRoundTripWrapper{
				Next:  next,
				Cache: cache,
				TTL:   ttl,
			}
		}

		return &PassThroughRoundTripWrapper{
			Next: next,
		}
	}
}

// RoundTrip wraps HTTP client calls. It will attempt to find a cached response in the Redis cache,
// and return it if it exists. If it doesn't exist, the downstream call will be made and the response
// will be cached in Redis. 5XX service responses are not cached. An error is returned only if there
// was an error calling the downstream service. Cache-related errors are swallowed.
func (c *PrimaryCacheRoundTripWrapper) RoundTrip(req *http.Request) (*http.Response, error) {
	cachedRequest, err := NewCachedRequest(req)
	if err != nil {
		log.Errorf("[PRIMARY] Error building cached request. Error: %+v", err)
		return c.Next.RoundTrip(req)
	}

	ctx := req.Context()

	cacheClient := c.Cache

	log.Debug("[PRIMARY] Checking for a cached response.")
	cachedResponse, err := cacheClient.Get(ctx, cachedRequest)
	if err != nil {
		log.Errorf("[PRIMARY] Error checking for a cached response. Error: %+v", err)
		return c.Next.RoundTrip(req)
	}

	if cachedResponse != nil {
		log.Debug("[PRIMARY] Found a cached response. Returning it.")
		return cachedResponse.CopyToHTTPResponse(req), nil
	}

	log.Debug("[PRIMARY] No cached response found for this call. Calling the downstream service")
	resp, err := c.Next.RoundTrip(req)
	if err != nil {
		return resp, err
	}

	// 5XX responses don't get cached
	if resp.StatusCode >= http.StatusInternalServerError {
		return resp, nil
	}

	log.Debug("[PRIMARY] Received a healthy response from the downstream service. Storing it in the cache.")
	newCachedResponse, err := NewCachedResponse(resp)
	if err != nil {
		log.Errorf("[PRIMARY] Error building cache value from HTTP response. Error: %+v", err)
		return resp, nil
	}

	cacheClient.SetAsync(ctx, cachedRequest, newCachedResponse, c.TTL)

	return resp, nil
}
