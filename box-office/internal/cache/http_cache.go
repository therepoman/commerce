package cache

import (
	"context"
	"encoding/json"
	"time"

	"github.com/afex/hystrix-go/hystrix"

	"code.justin.tv/chat/rediczar"
	redis "github.com/go-redis/redis/v7"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

const (
	httpCacheHystrixCommand      = "redis-http-cache"
	DefaultHystrixTimeout        = 500   // slightly higher than the Redis client timeout
	DefaultMaxConcurrentRequest  = 20000 // make this extremely high - use redis connection pool config for this
	DefaultVolumeThreshold       = 20
	DefaultSleepWindow           = 10000
	DefaultErrorPercentThreshold = 50
)

// IHTTPCache is an interface for accessing a cache using CachedRequest/CachedResponse objects
type IHTTPCache interface {
	Get(ctx context.Context, req *CachedRequest) (*CachedResponse, error)
	SetAsync(ctx context.Context, req *CachedRequest, resp *CachedResponse, ttl time.Duration)
}

// HTTPCache implements the IHTTPCache interface. It utilizes a Redis cache to Set/Get request and
// response objects. All objects are serialized.
type HTTPCache struct {
	Redis rediczar.ThickClient
}

// NewHTTPCache returns a new HTTPCache.
func NewHTTPCache(redis rediczar.ThickClient) *HTTPCache {
	config := hystrix.CommandConfig{
		Timeout:                DefaultHystrixTimeout,
		MaxConcurrentRequests:  DefaultMaxConcurrentRequest,
		RequestVolumeThreshold: DefaultVolumeThreshold,
		SleepWindow:            DefaultSleepWindow,
		ErrorPercentThreshold:  DefaultErrorPercentThreshold,
	}
	hystrix.ConfigureCommand(httpCacheHystrixCommand, config)
	return &HTTPCache{
		Redis: redis,
	}
}

// Get attempts to retrieve the provided key from the cache.
//   - If it exists, it is returned
//   - If it does not exist, nil is returned
//   - If an unexpected error is encountered, an error is returned
func (h *HTTPCache) Get(ctx context.Context, req *CachedRequest) (*CachedResponse, error) {
	serializedRequest := req.Serialized()

	var serializedResp string
	exists := false
	err := hystrix.Do(httpCacheHystrixCommand, func() (e error) {
		var err error
		serializedResp, err = h.Redis.Get(ctx, serializedRequest)
		if err != nil {
			if err == redis.Nil {
				return nil
			}
			return errors.Wrap(err, "Unexpected error from Redis::Get")
		}
		exists = true
		return nil
	}, nil)

	if err != nil {
		return nil, errors.Wrap(err, "Error while doing hystrixified Redis::Get")
	}

	if !exists {
		log.WithField("request", serializedRequest).Debug("No cached response found for this request")
		return nil, nil
	}

	log.WithField("request", serializedRequest).Debug("Found cached response for this request")

	cachedResponse, err := DeserializeCachedResponse(serializedResp)
	if err != nil {
		return nil, errors.Wrap(err, "Error deserializing cached response")
	}
	return cachedResponse, nil
}

// Set attempts to store the provided key and value in the cache asynchronously.
func (h *HTTPCache) SetAsync(ctx context.Context, req *CachedRequest, resp *CachedResponse, ttl time.Duration) {
	go func() {
		serializedRequest := req.Serialized()

		serializedResponse, err := Serialize(resp)
		if err != nil {
			log.WithField("response", serializedResponse).
				WithError(err).
				Error("Error serializing response in set")
		}

		if err = hystrix.Do(httpCacheHystrixCommand, func() (e error) {
			if _, err := h.Redis.Set(ctx, serializedRequest, serializedResponse, ttl); err != nil {
				return errors.Wrap(err, "Unexpected error from Redis::Set")
			}
			return nil
		}, nil); err != nil {
			log.WithField("request", serializedRequest).
				WithField("response", serializedResponse).
				WithError(err).
				Debug("Error while doing hystrixified Redis::Set")
		}

		log.WithField("request", serializedRequest).Debug("Successfully stored this request in the cache")
	}()
}

// Serialize converts an object into a string representation. It currently uses JSON representation
// for debugging simplicity and readability. Consider something like MessagePack if we need to optimize
// payload byte size.
// Reference: https://msgpack.org/index.html
func Serialize(data interface{}) (string, error) {
	dataBytes, err := json.Marshal(data)
	if err != nil {
		return "", errors.Wrap(err, "Error marshalling data")
	}
	return string(dataBytes), nil
}
