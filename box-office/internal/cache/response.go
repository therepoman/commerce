package cache

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"

	bohttp "code.justin.tv/commerce/box-office/internal/http"

	"github.com/pkg/errors"
)

// CachedResponse is a value in the cache which contains a copy of the HTTP response
// An http.Response object can't be directly gob encoded due to https://github.com/golang/go/issues/5819
type CachedResponse struct {
	Body             []byte
	Status           string // e.g. "200 OK"
	StatusCode       int
	Proto            string // e.g. "HTTP/1.0"
	ProtoMajor       int    // e.g. 1
	ProtoMinor       int    // e.g. 0
	Header           http.Header
	ContentLength    int64
	TransferEncoding []string
	Trailer          http.Header
	MediaType        string
}

// NewCachedResponse builds a new CachedResponse from an HTTP Response
func NewCachedResponse(resp *http.Response) (*CachedResponse, error) {
	if resp == nil {
		return nil, errors.New("resp must not be nil")
	}

	body, err := bohttp.ExtractHTTPResponseBody(resp)
	if err != nil {
		return nil, errors.Wrap(err, "Error building cached response")
	}
	return &CachedResponse{
		Status:           resp.Status,
		StatusCode:       resp.StatusCode,
		Proto:            resp.Proto,
		ProtoMajor:       resp.ProtoMajor,
		ProtoMinor:       resp.ProtoMinor,
		Header:           resp.Header,
		ContentLength:    resp.ContentLength,
		TransferEncoding: resp.TransferEncoding,
		Trailer:          resp.Trailer,
		Body:             body,
	}, nil
}

// DeserializeCachedResponse builds a CachedResponse from a serialized string version of the object
func DeserializeCachedResponse(serializedResp string) (*CachedResponse, error) {
	resp := &CachedResponse{}

	err := json.Unmarshal([]byte(serializedResp), resp)
	if err != nil {
		return nil, errors.Wrap(err, "Error unmarshalling cache value")
	}
	return resp, nil
}

// CopyToHTTPResponse returns a copy of this CachedResponse as an http.Response
func (cr *CachedResponse) CopyToHTTPResponse(req *http.Request) *http.Response {
	return &http.Response{
		Status:           cr.Status,
		StatusCode:       cr.StatusCode,
		Proto:            cr.Proto,
		ProtoMajor:       cr.ProtoMajor,
		ProtoMinor:       cr.ProtoMinor,
		Header:           cr.Header,
		ContentLength:    cr.ContentLength,
		TransferEncoding: cr.TransferEncoding,
		Trailer:          cr.Trailer,
		Request:          req,
		Body:             ioutil.NopCloser(bytes.NewBuffer(cr.Body)),
	}
}
