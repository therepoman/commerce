package cache

import (
	"bytes"
	"io/ioutil"
	"net/http/httptest"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

const (
	method = "GET"
	target = "http://example.com/foo"
)

func TestRequest(t *testing.T) {
	Convey("Test NewCachedRequest", t, func() {
		bodyBytes := []byte("this is the body in byte array format")

		Convey("With healthy request", func() {
			body := ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
			req := httptest.NewRequest(method, target, body)

			cachedReq, err := NewCachedRequest(req)
			So(err, ShouldBeNil)
			So(cachedReq.Method, ShouldEqual, method)
			So(cachedReq.URL, ShouldEqual, target)
			So(bytes.Compare(cachedReq.Body, bodyBytes), ShouldEqual, 0)

			// Build a new cached request from the same http request.
			// This is done to verify that the request body isnt damaged and can be done repeatedly
			cachedReq, err = NewCachedRequest(req)
			So(err, ShouldBeNil)
			So(cachedReq.Method, ShouldEqual, method)
			So(cachedReq.URL, ShouldEqual, target)
			So(bytes.Compare(cachedReq.Body, bodyBytes), ShouldEqual, 0)
		})

		Convey("With nil request", func() {
			_, err := NewCachedRequest(nil)
			So(err, ShouldNotBeNil)
		})

		Convey("With nil body", func() {
			req := httptest.NewRequest(method, target, nil)

			cachedReq, err := NewCachedRequest(req)
			So(err, ShouldBeNil)
			So(cachedReq.Method, ShouldEqual, method)
			So(cachedReq.URL, ShouldEqual, target)
			So(bytes.Compare(cachedReq.Body, []byte{}), ShouldEqual, 0)
		})
	})
}
