package cache_test

import (
	"bytes"
	"io/ioutil"
	"testing"
	"time"

	. "code.justin.tv/commerce/box-office/internal/cache"

	"code.justin.tv/commerce/box-office/mocks"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestFallbackCacheRoundTripper(t *testing.T) {
	Convey("Test RoundTrip", t, func() {
		defaultReqBodyBytes := []byte("this is the request body in byte array format")
		defaultRespBodyBytes := []byte("this is the response body in byte array format. http 200 ok lul")
		defaultHTTPRequest := generateHTTPRequest(defaultReqBodyBytes)

		mockHTTPCache := new(mocks.IHTTPCache)
		mockDownstreamResponse := new(mocks.RoundTripper)
		wrapper := FallbackCacheRoundTripWrapper{
			Next:  mockDownstreamResponse,
			Cache: mockHTTPCache,
			TTL:   1 * time.Minute,
		}

		Convey("With nil request", func() {
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(generateHTTPResponse(statusCode, defaultRespBodyBytes), nil)

			resp, err := wrapper.RoundTrip(nil)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)

			respBodyBytes, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.StatusCode, ShouldEqual, statusCode)
			So(bytes.Compare(respBodyBytes, defaultRespBodyBytes), ShouldEqual, 0)
			mockHTTPCache.AssertNumberOfCalls(t, "Get", 0)
			mockHTTPCache.AssertNumberOfCalls(t, "SetAsync", 0)
			mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
		})

		Convey("With downstream return an uncacheable response", func() {
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(nil, nil)

			mockHTTPCache.On("SetAsync", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

			resp, err := wrapper.RoundTrip(defaultHTTPRequest)
			So(err, ShouldBeNil)
			So(resp, ShouldBeNil)

			mockHTTPCache.AssertNumberOfCalls(t, "Get", 0)
			mockHTTPCache.AssertNumberOfCalls(t, "SetAsync", 0)
			mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
		})

		Convey("With downstream return healthy response", func() {
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(generateHTTPResponse(statusCode, defaultRespBodyBytes), nil)

			Convey("With cache return nil", func() {
				mockHTTPCache.On("SetAsync", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

				resp, err := wrapper.RoundTrip(defaultHTTPRequest)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)

				respBodyBytes, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.StatusCode, ShouldEqual, statusCode)
				So(bytes.Compare(respBodyBytes, defaultRespBodyBytes), ShouldEqual, 0)
				mockHTTPCache.AssertNumberOfCalls(t, "Get", 0)
				mockHTTPCache.AssertNumberOfCalls(t, "SetAsync", 1)
				mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
			})

			Convey("With cache return error", func() {
				mockHTTPCache.On("SetAsync", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test"))

				resp, err := wrapper.RoundTrip(defaultHTTPRequest)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)

				respBodyBytes, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.StatusCode, ShouldEqual, statusCode)
				So(bytes.Compare(respBodyBytes, defaultRespBodyBytes), ShouldEqual, 0)
				mockHTTPCache.AssertNumberOfCalls(t, "Get", 0)
				mockHTTPCache.AssertNumberOfCalls(t, "SetAsync", 1)
				mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
			})
		})

		Convey("With downstream return error response", func() {
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(nil, errors.New("test"))

			Convey("With cache get return error", func() {
				mockHTTPCache.On("Get", mock.Anything, mock.Anything).Return(nil, errors.New("test"))

				resp, err := wrapper.RoundTrip(defaultHTTPRequest)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)

				mockHTTPCache.AssertNumberOfCalls(t, "Get", 1)
				mockHTTPCache.AssertNumberOfCalls(t, "SetAsync", 0)
				mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
			})

			Convey("With cache get return nil", func() {
				mockHTTPCache.On("Get", mock.Anything, mock.Anything).Return(nil, nil)

				resp, err := wrapper.RoundTrip(defaultHTTPRequest)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)

				mockHTTPCache.AssertNumberOfCalls(t, "Get", 1)
				mockHTTPCache.AssertNumberOfCalls(t, "SetAsync", 0)
				mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
			})

			Convey("With cache get return response", func() {
				cachedResp := &CachedResponse{
					StatusCode: statusCode,
					Body:       defaultRespBodyBytes,
				}
				mockHTTPCache.On("Get", mock.Anything, mock.Anything).Return(cachedResp, nil)

				resp, err := wrapper.RoundTrip(defaultHTTPRequest)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)

				respBodyBytes, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.StatusCode, ShouldEqual, statusCode)
				So(bytes.Compare(respBodyBytes, defaultRespBodyBytes), ShouldEqual, 0)
				mockHTTPCache.AssertNumberOfCalls(t, "Get", 1)
				mockHTTPCache.AssertNumberOfCalls(t, "SetAsync", 0)
				mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
			})
		})

		Convey("With downstream return HTTP 500", func() {
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(generateHTTPResponse(500, defaultRespBodyBytes), nil)

			Convey("With cache get return error", func() {
				mockHTTPCache.On("Get", mock.Anything, mock.Anything).Return(nil, errors.New("test"))

				resp, err := wrapper.RoundTrip(defaultHTTPRequest)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.StatusCode, ShouldEqual, 500)

				mockHTTPCache.AssertNumberOfCalls(t, "Get", 1)
				mockHTTPCache.AssertNumberOfCalls(t, "SetAsync", 0)
				mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
			})

			Convey("With cache get return nil", func() {
				mockHTTPCache.On("Get", mock.Anything, mock.Anything).Return(nil, nil)

				resp, err := wrapper.RoundTrip(defaultHTTPRequest)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.StatusCode, ShouldEqual, 500)

				mockHTTPCache.AssertNumberOfCalls(t, "Get", 1)
				mockHTTPCache.AssertNumberOfCalls(t, "SetAsync", 0)
				mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
			})

			Convey("With cache get return response", func() {
				cachedResp := &CachedResponse{
					StatusCode: statusCode,
					Body:       defaultRespBodyBytes,
				}
				mockHTTPCache.On("Get", mock.Anything, mock.Anything).Return(cachedResp, nil)

				resp, err := wrapper.RoundTrip(defaultHTTPRequest)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)

				respBodyBytes, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.StatusCode, ShouldEqual, statusCode)
				So(bytes.Compare(respBodyBytes, defaultRespBodyBytes), ShouldEqual, 0)
				mockHTTPCache.AssertNumberOfCalls(t, "Get", 1)
				mockHTTPCache.AssertNumberOfCalls(t, "SetAsync", 0)
				mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
			})
		})
	})
}
