package cache_test

import (
	"bytes"
	"context"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"

	. "code.justin.tv/commerce/box-office/internal/cache"
	"code.justin.tv/commerce/box-office/mocks"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/go-redis/redis/v7"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	method                  = "GET"
	target                  = "myurl"
	statusCode              = 200
	httpCacheHystrixCommand = "redis-http-cache"
)

func TestHTTPCache(t *testing.T) {
	Convey("Test HTTPCache", t, func() {
		defaultReqBodyBytes := []byte("this is the request body in byte array format")
		defaultRespBodyBytes := []byte("this is the response body in byte array format. http 200 ok lul")

		cachedRequest, err := NewCachedRequest(&http.Request{
			Method: method,
			URL:    &url.URL{Host: target},
			Body:   ioutil.NopCloser(bytes.NewReader(defaultReqBodyBytes)),
		})
		So(err, ShouldBeNil)

		mockRedis := new(mocks.ThickClient)

		httpCache := HTTPCache{
			Redis: mockRedis,
		}

		ctx := context.Background()

		Convey("Test Get", func() {
			Convey("With redis returns the resp", func() {
				serializedResp := generateSerializedCachedResponse(statusCode, defaultRespBodyBytes)
				mockRedis.On("Get", mock.Anything, mock.Anything).Return(serializedResp, nil)

				cachedResp, err := httpCache.Get(ctx, cachedRequest)
				So(err, ShouldBeNil)
				So(cachedResp, ShouldNotBeNil)
				So(cachedResp.StatusCode, ShouldEqual, statusCode)
				So(bytes.Compare(cachedResp.Body, defaultRespBodyBytes), ShouldEqual, 0)
				mockRedis.AssertNumberOfCalls(t, "Get", 1)
			})

			Convey("With redis returns an error", func() {
				mockRedis.On("Get", mock.Anything, mock.Anything).Return("", errors.New("test"))

				_, err := httpCache.Get(ctx, cachedRequest)

				So(err, ShouldNotBeNil)
				mockRedis.AssertNumberOfCalls(t, "Get", 1)
			})

			Convey("With redis returns nil", func() {
				mockRedis.On("Get", mock.Anything, mock.Anything).Return("", redis.Nil)

				cachedResp, err := httpCache.Get(ctx, cachedRequest)

				So(err, ShouldBeNil)
				So(cachedResp, ShouldBeNil)
				mockRedis.AssertNumberOfCalls(t, "Get", 1)
			})

			Convey("With bad resp deserialization", func() {
				serializedResp := "thisisabadresponse"
				mockRedis.On("Get", mock.Anything, mock.Anything).Return(serializedResp, nil)

				_, err := httpCache.Get(ctx, cachedRequest)
				So(err, ShouldNotBeNil)
				mockRedis.AssertNumberOfCalls(t, "Get", 1)
			})
		})

		Convey("Test Hystrix - redis returns multiple errors - open circuit", func() {
			config := hystrix.CommandConfig{
				Timeout:                DefaultHystrixTimeout,
				MaxConcurrentRequests:  DefaultMaxConcurrentRequest,
				RequestVolumeThreshold: 2,
				SleepWindow:            DefaultSleepWindow,
				ErrorPercentThreshold:  DefaultErrorPercentThreshold,
			}
			hystrix.ConfigureCommand(httpCacheHystrixCommand, config)

			mockRedis.On("Get", mock.Anything, mock.Anything).Return("", errors.New("test"))

			_, err := httpCache.Get(ctx, cachedRequest)

			So(err, ShouldNotBeNil)
			mockRedis.AssertNumberOfCalls(t, "Get", 1)

			_, err = httpCache.Get(ctx, cachedRequest)

			So(err, ShouldNotBeNil)
			mockRedis.AssertNumberOfCalls(t, "Get", 2)

			_, err = httpCache.Get(ctx, cachedRequest)

			So(err, ShouldNotBeNil)
			mockRedis.AssertNumberOfCalls(t, "Get", 2)
		})
	})
}

func generateSerializedCachedResponse(statusCode int, bodyBytes []byte) string {
	cachedResp := &CachedResponse{
		StatusCode: statusCode,
		Body:       bodyBytes,
	}
	serializedResp, err := Serialize(cachedResp)
	So(err, ShouldBeNil)
	return serializedResp
}
