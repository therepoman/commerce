package cache

import (
	"context"
	"net/http"
	"time"

	"code.justin.tv/commerce/box-office/config"
	log "github.com/sirupsen/logrus"
)

// FallbackCacheRoundTripWrapper is a wrapper around an http round tripper which utilizes
// a shared cache in order to store and retrieve dependent service responses. It stores old
// downstream responses and returns cached responses if the downstream service goes down.
type FallbackCacheRoundTripWrapper struct {
	Next    http.RoundTripper
	Cache   IHTTPCache
	TTL     time.Duration
	Timeout time.Duration
}

// NewFallbackCacheRoundTripWrapper returns a new FallbackCacheRoundTripWrapper. The input time-to-live (TTL) is applied to
// all cache Set calls from this wrapper.
func NewFallbackCacheRoundTripWrapper(cache IHTTPCache, isCacheEnabled bool, ttl, timeout time.Duration, cfg *config.Configuration) func(c http.RoundTripper) http.RoundTripper {
	return func(next http.RoundTripper) http.RoundTripper {
		if isCacheEnabled {
			return &FallbackCacheRoundTripWrapper{
				Next:    next,
				Cache:   cache,
				TTL:     ttl,
				Timeout: timeout,
			}
		}
		return &PassThroughRoundTripWrapper{
			Next: next,
		}
	}
}

// RoundTrip wraps HTTP client calls. It will always make the downstream call and will store a healthy
// response in the cache for a long duration (e.g. 30 days). The cache will only be checked for a response
// if the downstream service returns an error response (e.g. HTTP 500). If the cache entry is found, it will be
// returned instead of the downstream service's error response. Cache-related errors are swallowed.
func (f *FallbackCacheRoundTripWrapper) RoundTrip(req *http.Request) (*http.Response, error) {
	// Build the cached request before making downstream call. This will make a copy of the request body
	// prior to the HTTP client consuming it.
	cachedRequest, err := NewCachedRequest(req)
	if err != nil {
		log.Errorf("[FALLBACK] Error building cached request. Error: %+v", err)
		return f.Next.RoundTrip(req)
	}

	cacheClient := f.Cache

	ctx := req.Context()
	resp, nextErr := f.Next.RoundTrip(req)
	if nextErr != nil || (resp != nil && resp.StatusCode >= http.StatusInternalServerError) {
		// Create new context since req.Context() might already be canceled if the request timed out.
		// See JIRA SUBS-3416.
		fallbackCtx, cancel := context.WithTimeout(context.Background(), f.Timeout)
		defer cancel()

		// An error response was received from the downstream service, try to get a healthy
		// response from the fallback cache.
		if nextErr != nil {
			log.Errorf("[FALLBACK] Error calling downstream service: %+v", nextErr)
		} else {
			log.Warnf("[FALLBACK] Error response received from downstream service: %v", resp.StatusCode)
		}

		log.Debug("[FALLBACK] Downstream call failed. Checking for a cached response.")
		cachedResponse, err := cacheClient.Get(fallbackCtx, cachedRequest)
		if err != nil {
			log.Errorf("[FALLBACK] Error checking for a cached response. Error: %+v", err)
			return resp, nextErr
		}

		if cachedResponse != nil {
			log.Debug("[FALLBACK] Found a cached response. Returning it.")
			return cachedResponse.CopyToHTTPResponse(req), nil
		}

		log.Debug("[FALLBACK] No cached response found, returning the downstream response.")
		return resp, nextErr
	}

	// A healthy response was received from the downstream service, store it.
	log.Debug("[FALLBACK] Received a healthy response from the downstream service. Storing it in the cache.")
	newCachedResponse, err := NewCachedResponse(resp)
	if err != nil {
		log.Errorf("[FALLBACK] Error building cache value from HTTP response. Error: %+v", err)
		return resp, nil
	}

	cacheClient.SetAsync(ctx, cachedRequest, newCachedResponse, f.TTL)

	return resp, nil
}
