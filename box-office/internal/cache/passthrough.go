package cache

import (
	"net/http"
)

// PassThroughRoundTripWrapper is a wrapper around an http round tripper which simply
// acts as a pass through to the next call. This can be swapped in for another round tripper
// in order to disable its functionality.
type PassThroughRoundTripWrapper struct {
	Next http.RoundTripper
}

// RoundTrip wraps HTTP client calls. It will simply pass the request through to the downstream call.
func (p *PassThroughRoundTripWrapper) RoundTrip(req *http.Request) (*http.Response, error) {
	return p.Next.RoundTrip(req)
}
