package cache

import (
	"net/http"

	bohttp "code.justin.tv/commerce/box-office/internal/http"
	"github.com/pkg/errors"
)

// CachedRequest is a key in the cache which contains a copy of the HTTP request
type CachedRequest struct {
	Method     string
	URL        string
	Body       []byte
	serialized string // Memoized JSON form. Private to prevent recursive serialization
}

func (c CachedRequest) Serialized() string {
	return c.serialized
}

// NewCachedRequest generates a CachedRequest from an HTTP request
func NewCachedRequest(req *http.Request) (*CachedRequest, error) {
	if req == nil {
		return nil, errors.New("req must not be nil")
	}
	req.URL.RawQuery = req.URL.Query().Encode()

	bodyBytes, err := bohttp.ExtractHTTPRequestBody(req)
	if err != nil {
		return nil, errors.Wrap(err, "Error extracting request body")
	}

	c := &CachedRequest{
		Method: req.Method,
		URL:    req.URL.String(),
		Body:   bodyBytes,
	}

	serialized, err := Serialize(c)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to serialize *CachedRequest")
	}

	c.serialized = serialized

	return c, nil
}
