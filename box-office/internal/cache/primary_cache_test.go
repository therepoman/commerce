package cache_test

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"github.com/pkg/errors"

	. "code.justin.tv/commerce/box-office/internal/cache"

	"code.justin.tv/commerce/box-office/mocks"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestPrimaryCacheRoundTripper(t *testing.T) {
	Convey("Test RoundTrip", t, func() {
		defaultReqBodyBytes := []byte("this is the request body in byte array format")
		defaultRespBodyBytes := []byte("this is the response body in byte array format. http 200 ok lul")
		defaultHTTPRequest := generateHTTPRequest(defaultReqBodyBytes)

		mockHTTPCache := new(mocks.IHTTPCache)
		mockDownstreamResponse := new(mocks.RoundTripper)
		wrapper := PrimaryCacheRoundTripWrapper{
			Next:  mockDownstreamResponse,
			Cache: mockHTTPCache,
			TTL:   1 * time.Minute,
		}

		Convey("With Cache::Get returns the resp", func() {
			cachedResp := &CachedResponse{
				StatusCode: statusCode,
				Body:       defaultRespBodyBytes,
			}
			mockHTTPCache.On("Get", mock.Anything, mock.Anything).Return(cachedResp, nil)

			resp, err := wrapper.RoundTrip(defaultHTTPRequest)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)

			respBodyBytes, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.StatusCode, ShouldEqual, statusCode)
			So(bytes.Compare(respBodyBytes, defaultRespBodyBytes), ShouldEqual, 0)
			mockHTTPCache.AssertNumberOfCalls(t, "Get", 1)
			mockHTTPCache.AssertNumberOfCalls(t, "SetAsync", 0)
			mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 0)
		})

		Convey("With Cache::Get returns an error", func() {
			mockHTTPCache.On("Get", mock.Anything, mock.Anything).Return(nil, errors.New("test"))
			mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(generateHTTPResponse(statusCode, defaultRespBodyBytes), nil)

			resp, err := wrapper.RoundTrip(defaultHTTPRequest)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)

			respBodyBytes, err := ioutil.ReadAll(resp.Body)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.StatusCode, ShouldEqual, statusCode)
			So(bytes.Compare(respBodyBytes, defaultRespBodyBytes), ShouldEqual, 0)
			mockHTTPCache.AssertNumberOfCalls(t, "Get", 1)
			mockHTTPCache.AssertNumberOfCalls(t, "SetAsync", 0)
			mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
		})

		Convey("With Cache::Get returns nil", func() {
			mockHTTPCache.On("Get", mock.Anything, mock.Anything).Return(nil, nil)

			Convey("With downstream returns error", func() {
				mockHTTPCache.On("SetAsync", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

				mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(nil, errors.New("test"))

				_, err := wrapper.RoundTrip(defaultHTTPRequest)
				So(err, ShouldNotBeNil)
				mockHTTPCache.AssertNumberOfCalls(t, "Get", 1)
				mockHTTPCache.AssertNumberOfCalls(t, "SetAsync", 0)
				mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
			})

			Convey("With downstream returns HTTP 500", func() {
				http500 := 500
				mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(generateHTTPResponse(http500, defaultRespBodyBytes), nil)

				resp, err := wrapper.RoundTrip(defaultHTTPRequest)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)

				respBodyBytes, err := ioutil.ReadAll(resp.Body)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.StatusCode, ShouldEqual, http500)
				So(bytes.Compare(respBodyBytes, defaultRespBodyBytes), ShouldEqual, 0)
				mockHTTPCache.AssertNumberOfCalls(t, "Get", 1)
				mockHTTPCache.AssertNumberOfCalls(t, "SetAsync", 0)
				mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
			})

			Convey("With downstream returns HTTP 200", func() {
				mockDownstreamResponse.On("RoundTrip", mock.Anything).Return(generateHTTPResponse(statusCode, defaultRespBodyBytes), nil)

				Convey("With Cache::Set returning error", func() {
					mockHTTPCache.On("SetAsync", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("test"))

					resp, err := wrapper.RoundTrip(defaultHTTPRequest)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)

					respBodyBytes, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(resp.StatusCode, ShouldEqual, statusCode)
					So(bytes.Compare(respBodyBytes, defaultRespBodyBytes), ShouldEqual, 0)
					mockHTTPCache.AssertNumberOfCalls(t, "Get", 1)
					mockHTTPCache.AssertNumberOfCalls(t, "SetAsync", 1)
					mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
				})

				Convey("With Cache::Set returning nil", func() {
					mockHTTPCache.On("SetAsync", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

					resp, err := wrapper.RoundTrip(defaultHTTPRequest)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)

					respBodyBytes, err := ioutil.ReadAll(resp.Body)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(resp.StatusCode, ShouldEqual, statusCode)
					So(bytes.Compare(respBodyBytes, defaultRespBodyBytes), ShouldEqual, 0)
					mockHTTPCache.AssertNumberOfCalls(t, "Get", 1)
					mockHTTPCache.AssertNumberOfCalls(t, "SetAsync", 1)
					mockDownstreamResponse.AssertNumberOfCalls(t, "RoundTrip", 1)
				})
			})
		})
	})
}

func generateHTTPResponse(statusCode int, bodyBytes []byte) *http.Response {
	respBody := ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
	resp := &http.Response{
		StatusCode: statusCode,
		Body:       respBody,
	}
	return resp
}

func generateHTTPRequest(bodyBytes []byte) *http.Request {
	reqBody := ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
	req, err := http.NewRequest(method, target, reqBody)
	So(err, ShouldBeNil)
	return req
}
