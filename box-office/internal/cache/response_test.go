package cache

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

const (
	statusCode = 200
)

func TestResponse(t *testing.T) {
	Convey("Test NewCachedResponse", t, func() {
		bodyBytes := []byte("this is the body in byte array format")

		Convey("With healthy response", func() {
			body := ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
			resp := &http.Response{
				StatusCode: statusCode,
				Body:       body,
			}

			cachedResp, err := NewCachedResponse(resp)
			So(err, ShouldBeNil)
			So(cachedResp.StatusCode, ShouldEqual, statusCode)
			So(bytes.Compare(cachedResp.Body, bodyBytes), ShouldEqual, 0)

			// Build a new cached response from the same http response.
			// This is done to verify that the response body isnt damaged and can be done repeatedly
			cachedResp, err = NewCachedResponse(resp)
			So(err, ShouldBeNil)
			So(cachedResp.StatusCode, ShouldEqual, statusCode)
			So(bytes.Compare(cachedResp.Body, bodyBytes), ShouldEqual, 0)
		})

		Convey("With nil response", func() {
			_, err := NewCachedResponse(nil)
			So(err, ShouldNotBeNil)
		})

		Convey("With nil body", func() {
			resp := &http.Response{
				StatusCode: statusCode,
				Body:       nil,
			}

			cachedResp, err := NewCachedResponse(resp)
			So(err, ShouldBeNil)
			So(cachedResp.StatusCode, ShouldEqual, statusCode)
			So(bytes.Compare(cachedResp.Body, []byte{}), ShouldEqual, 0)
		})
	})
}
