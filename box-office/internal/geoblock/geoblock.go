package geoblock

import (
	"time"
)

// error codes for geoblock_reason
const (
	ContentRestricted = "content_geoblocked"
	VPNBlocking       = "anonymizer_blocked"
)

// Geoblock allows for blocklisting and allowlisting of channels for specific periods of time.
type Geoblock struct {
	TimeWindows       []TimeWindow
	BlockAnonymousIPs bool

	AllowlistedCountries map[string]bool
	BlocklistedCountries map[string]bool
	BlocklistedZipCodes  map[string]bool
	AllowlistedPlatforms *map[string]bool `json:",omitempty"`
}

// Go through all TimeWindow and check if the provided time is inside any of them.
func (g *Geoblock) SomeWindowIncludes(time time.Time) bool {
	for _, window := range g.TimeWindows {
		if window.Includes(time) {
			return true
		}
	}
	return false
}

// IsAllowlistedPlatform will only return false if:
//   1. The Geoblock is active (current time is inside the bounds of start and end time for at least one time window), AND
//   2. The AllowlistPlatforms field is present in g, AND
//   3. The provided platform string is not a member of g's AllowlistPlatforms
func (g *Geoblock) IsAllowlistedPlatform(platform string, currentTime time.Time) bool {
	if !g.SomeWindowIncludes(currentTime) {
		return true
	}
	if g.AllowlistedPlatforms == nil || len(*g.AllowlistedPlatforms) == 0 {
		// No platform allowlist is enforced or it is empty.
		return true
	}
	if _, ok := (*g.AllowlistedPlatforms)[platform]; ok {
		// platform is in the AllowlistPlatforms map; platform is allowlisted
		return true
	}
	// AllowlistPlatforms exists, but the given platform is not among it.
	return false
}

// IsBlocklisted returns true if the input countryCode is blocklisted. A countryCode is considered blocklisted if:
//   1. The Geoblock is active (current time is inside the bounds of start and end time for at least one time window), AND
//   2. The Geoblock's blocklist is not empty, AND
//   3. The Geoblock's blocklist contains the country code
// Reference legacy code: https://git-aws.internal.justin.tv/web/web/blob/master/app/controllers/api/channels_controller.rb#L355
func (g *Geoblock) IsBlocklisted(countryCode string, currentTime time.Time) bool {
	if !g.SomeWindowIncludes(currentTime) {
		return false
	}

	if len(g.BlocklistedCountries) == 0 {
		return false
	}

	_, exists := g.BlocklistedCountries[countryCode]
	if exists {
		return true
	}

	return false
}

// IsAllowlistedCountry returns true if the input countryCode is allowlisted. A countryCode is considered allowlisted if:
//   1. The Geoblock is inactive (current time is outside the bounds of start and end time for all time windows), OR
//   2. The Geoblock's allowlist is empty, OR
//   3. The Geoblock's allowlist contains the country code
// Reference legacy code: https://git-aws.internal.justin.tv/web/web/blob/master/app/controllers/api/channels_controller.rb#L361
func (g *Geoblock) IsAllowlistedCountry(countryCode string, currentTime time.Time) bool {
	if !g.SomeWindowIncludes(currentTime) {
		return true
	}

	if len(g.AllowlistedCountries) == 0 {
		return true
	}

	_, exists := g.AllowlistedCountries[countryCode]
	if exists {
		return true
	}

	return false
}

// BlockAnon returns true if the input bool is true while the geoblock  is configured to block anonymous ips
// and geoblock is active (inside bounds of start and end time for at least one TimeWindow)
func (g *Geoblock) BlockAnon(ipIsAnonymous bool, currentTime time.Time) bool {
	if !g.BlockAnonymousIPs {
		return false
	}

	if !g.SomeWindowIncludes(currentTime) {
		return false
	}

	return ipIsAnonymous
}

// IsBlocklistedZipCode returns true if the input zip code is blocklisted. A zip code is considered blocklisted if:
//   1. The Geoblock is active (current time is inside the bounds of start and end time for at least one TimeWindow), AND
//   2. The Geoblock's blocklistzipcodes is not empty, AND
//   3. The Geoblock's blocklistzipcodes contains the zip code
func (g *Geoblock) IsBlocklistedZipCode(zipCode string, currentTime time.Time) bool {
	if !g.SomeWindowIncludes(currentTime) {
		return false
	}

	if len(g.BlocklistedZipCodes) == 0 {
		return false
	}

	_, exists := g.BlocklistedZipCodes[zipCode]
	if exists {
		return true
	}

	return false
}

// BlackoutEnabled returns true if zip code level blocking is enabled
func (g *Geoblock) BlackoutEnabled(currentTime time.Time) bool {
	if !g.SomeWindowIncludes(currentTime) {
		return false
	}

	if len(g.BlocklistedZipCodes) > 0 {
		return true
	}

	return false
}
