package geoblock

import "time"

// Represents a single "window" of time encapsulated by a start and end time.
type TimeWindow struct {
	StartTime time.Time
	EndTime   time.Time
}

func (t *TimeWindow) IsValid() bool {
	return t.StartTime.Before(t.EndTime)
}

// Determines if a given time is inside the window of time defined by this TimeWindow.
// If the TimeWindow is invalid, the answer will always be false.
func (t *TimeWindow) Includes(time time.Time) bool {
	return t.IsValid() && t.StartTime.Before(time) && t.EndTime.After(time)
}
