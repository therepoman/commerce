package geoblock

import (
	"encoding/json"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

const (
	countryCodeUS   = "US"
	countryCodeDE   = "DE"
	countryCodeES   = "ES"
	postalCodeSF    = "94104"
	postalCodeNY    = "10001"
	postalCodeEmpty = ""
)

func TestGeoblock(t *testing.T) {
	Convey("Test Geoblock", t, func() {
		firstTime := time.Date(2010, 1, 1, 1, 1, 1, 1, time.UTC)
		secondTime := time.Date(2020, 1, 1, 1, 1, 1, 1, time.UTC)
		thirdTime := time.Date(2030, 1, 1, 1, 1, 1, 1, time.UTC)
		emptyMap := map[string]bool{}
		populatedMap := map[string]bool{}
		populatedMap[countryCodeUS] = true
		populatedMap[countryCodeDE] = true
		blocklistZipCodes := map[string]bool{}
		blocklistZipCodes[postalCodeNY] = true

		Convey("Test IsBlocklisted", func() {
			Convey("Test before start date", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: secondTime,
							EndTime:   thirdTime,
						},
					},
					AllowlistedCountries: emptyMap,
					BlocklistedCountries: populatedMap,
					BlocklistedZipCodes:  blocklistZipCodes,
				}

				isBlocklisted := geoblock.IsBlocklisted(countryCodeUS, firstTime)
				So(isBlocklisted, ShouldBeFalse)
			})

			Convey("Test after end date", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   secondTime,
						},
					},
					AllowlistedCountries: emptyMap,
					BlocklistedCountries: populatedMap,
					BlocklistedZipCodes:  blocklistZipCodes,
				}

				isBlocklisted := geoblock.IsBlocklisted(countryCodeUS, thirdTime)
				So(isBlocklisted, ShouldBeFalse)
			})

			Convey("Test within both dates - US", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   thirdTime,
						},
					},
					AllowlistedCountries: emptyMap,
					BlocklistedCountries: populatedMap,
					BlocklistedZipCodes:  blocklistZipCodes,
				}

				isBlocklisted := geoblock.IsBlocklisted(countryCodeUS, secondTime)
				So(isBlocklisted, ShouldBeTrue)
			})

			Convey("Test within both dates - DE", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   thirdTime,
						},
					},
					AllowlistedCountries: emptyMap,
					BlocklistedCountries: populatedMap,
					BlocklistedZipCodes:  blocklistZipCodes,
				}

				isBlocklisted := geoblock.IsBlocklisted(countryCodeDE, secondTime)
				So(isBlocklisted, ShouldBeTrue)
			})

			Convey("Test empty blocklist", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   thirdTime,
						},
					},
					AllowlistedCountries: emptyMap,
					BlocklistedCountries: emptyMap,
					BlocklistedZipCodes:  emptyMap,
				}

				isBlocklisted := geoblock.IsBlocklisted(countryCodeUS, secondTime)
				So(isBlocklisted, ShouldBeFalse)
			})

			Convey("Test with non-blocklisted country code", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   thirdTime,
						},
					},
					AllowlistedCountries: emptyMap,
					BlocklistedCountries: populatedMap,
					BlocklistedZipCodes:  blocklistZipCodes,
				}

				isBlocklisted := geoblock.IsBlocklisted(countryCodeES, secondTime)
				So(isBlocklisted, ShouldBeFalse)
			})
		})

		Convey("Test IsBlocklistedZipCode", func() {
			Convey("Test before start date", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   thirdTime,
						},
					},
					AllowlistedCountries: emptyMap,
					BlocklistedCountries: populatedMap,
					BlocklistedZipCodes:  blocklistZipCodes,
				}

				isBlocklisted := geoblock.IsBlocklistedZipCode(postalCodeNY, firstTime)
				So(isBlocklisted, ShouldBeFalse)
			})

			Convey("Test after end date", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   secondTime,
						},
					},
					AllowlistedCountries: emptyMap,
					BlocklistedCountries: populatedMap,
					BlocklistedZipCodes:  blocklistZipCodes,
				}

				isBlocklisted := geoblock.IsBlocklistedZipCode(postalCodeNY, thirdTime)
				So(isBlocklisted, ShouldBeFalse)
			})

			Convey("Test within both dates - NY postal code", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   thirdTime,
						},
					},
					AllowlistedCountries: emptyMap,
					BlocklistedCountries: populatedMap,
					BlocklistedZipCodes:  blocklistZipCodes,
				}

				isBlocklisted := geoblock.IsBlocklistedZipCode(postalCodeNY, secondTime)
				So(isBlocklisted, ShouldBeTrue)
			})

			Convey("Test with non-blocklisted postal code", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   thirdTime,
						},
					},
					AllowlistedCountries: emptyMap,
					BlocklistedCountries: populatedMap,
					BlocklistedZipCodes:  blocklistZipCodes,
				}

				isBlocklisted := geoblock.IsBlocklistedZipCode(postalCodeSF, secondTime)
				So(isBlocklisted, ShouldBeFalse)
			})

			Convey("Test with empty postal code", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   thirdTime,
						},
					},
					AllowlistedCountries: emptyMap,
					BlocklistedCountries: populatedMap,
					BlocklistedZipCodes:  blocklistZipCodes,
				}

				isBlocklisted := geoblock.IsBlocklistedZipCode(postalCodeEmpty, secondTime)
				So(isBlocklisted, ShouldBeFalse)
			})
		})

		Convey("Test IsAllowlistedCountry", func() {
			Convey("Test before start date", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   thirdTime,
						},
					},
					AllowlistedCountries: populatedMap,
					BlocklistedCountries: emptyMap,
					BlocklistedZipCodes:  emptyMap,
				}

				isAllowlisted := geoblock.IsAllowlistedCountry(countryCodeUS, firstTime)
				So(isAllowlisted, ShouldBeTrue)
			})

			Convey("Test after end date", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   secondTime,
						},
					},
					AllowlistedCountries: populatedMap,
					BlocklistedCountries: emptyMap,
					BlocklistedZipCodes:  emptyMap,
				}

				isAllowlisted := geoblock.IsAllowlistedCountry(countryCodeUS, thirdTime)
				So(isAllowlisted, ShouldBeTrue)
			})

			Convey("Test within both dates - US", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   thirdTime,
						},
					},
					AllowlistedCountries: populatedMap,
					BlocklistedCountries: emptyMap,
					BlocklistedZipCodes:  emptyMap,
				}

				isAllowlisted := geoblock.IsAllowlistedCountry(countryCodeUS, secondTime)
				So(isAllowlisted, ShouldBeTrue)
			})

			Convey("Test within both dates - DE", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   thirdTime,
						},
					},
					AllowlistedCountries: populatedMap,
					BlocklistedCountries: emptyMap,
					BlocklistedZipCodes:  emptyMap,
				}

				isAllowlisted := geoblock.IsAllowlistedCountry(countryCodeDE, secondTime)
				So(isAllowlisted, ShouldBeTrue)
			})

			Convey("Test empty allowlist", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   thirdTime,
						},
					},
					AllowlistedCountries: emptyMap,
					BlocklistedCountries: emptyMap,
					BlocklistedZipCodes:  emptyMap,
				}

				isAllowlisted := geoblock.IsAllowlistedCountry(countryCodeES, secondTime)
				So(isAllowlisted, ShouldBeTrue)
			})

			Convey("Test with non-allowlisted country code", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   thirdTime,
						},
					},
					AllowlistedCountries: populatedMap,
					BlocklistedCountries: emptyMap,
					BlocklistedZipCodes:  emptyMap,
				}

				isAllowlisted := geoblock.IsAllowlistedCountry(countryCodeES, secondTime)
				So(isAllowlisted, ShouldBeFalse)
			})
		})

		Convey("Test BlockAnon", func() {
			isAnon := true
			isKnown := false
			Convey("Test before start date", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   thirdTime,
						},
					},
					BlockAnonymousIPs: true,
				}

				blockAnon := geoblock.BlockAnon(isAnon, firstTime)
				So(blockAnon, ShouldBeFalse)
			})

			Convey("Test after end date", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   secondTime,
						},
					},
					BlockAnonymousIPs: true,
				}

				blockAnon := geoblock.BlockAnon(isAnon, thirdTime)
				So(blockAnon, ShouldBeFalse)
			})

			Convey("Test within both dates - isAnon", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   thirdTime,
						},
					},
					BlockAnonymousIPs: true,
				}

				blockAnon := geoblock.BlockAnon(isAnon, secondTime)
				So(blockAnon, ShouldBeTrue)
			})

			Convey("Test within both dates - isKnown", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   thirdTime,
						},
					},
					BlockAnonymousIPs: true,
				}

				blockAnon := geoblock.BlockAnon(isKnown, secondTime)
				So(blockAnon, ShouldBeFalse)
			})

			Convey("Test no blocking", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   thirdTime,
						},
					},
					BlockAnonymousIPs: false,
				}

				blockAnon := geoblock.BlockAnon(isAnon, secondTime)
				So(blockAnon, ShouldBeFalse)
			})
		})

		Convey("Test IsAllowlistedPlatform", func() {
			Convey("When AllowlistlistPlatforms is nil", func() {
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   thirdTime,
						},
					},
				}

				allowlisted := geoblock.IsAllowlistedPlatform("derp", secondTime)
				So(allowlisted, ShouldBeTrue)
			})
			Convey("When AllowlistPlatforms is not nil", func() {
				platforms := make(map[string]bool)
				platforms["web"] = true
				platforms["somethingElse"] = true
				geoblock := Geoblock{
					TimeWindows: []TimeWindow{
						{
							StartTime: firstTime,
							EndTime:   thirdTime,
						},
					},
					AllowlistedPlatforms: &platforms,
				}
				webAllowlisted := geoblock.IsAllowlistedPlatform("web", secondTime)
				So(webAllowlisted, ShouldBeTrue)
				somethingElse := geoblock.IsAllowlistedPlatform("somethingElse", secondTime)
				So(somethingElse, ShouldBeTrue)
				// A time outside the window should always be true
				webOutside := geoblock.IsAllowlistedPlatform("web", thirdTime.AddDate(1, 0, 0))
				So(webOutside, ShouldBeTrue)
				another := geoblock.IsAllowlistedPlatform("another", secondTime)
				So(another, ShouldBeFalse)
			})
			Convey("Test deserialization with nonempty AllowlistPlatforms", func() {
				var geoblockRawData = []byte(`{"TimeWindows":[{"StartTime":"2010-01-01T00:00:00-08:00","EndTime":"2030-01-01T00:00:00-08:00"}],"BlockAnonymousIPs":true,"BlocklistedZipCodes":{},"AllowlistedCountries":{},"BlocklistedCountries":{"CN":true},"AllowlistedPlatforms":{"web":true}}`)
				var geoblock = Geoblock{}
				err := json.Unmarshal(geoblockRawData, &geoblock)
				if err != nil {
					t.Error(err)
				}
				webAllowlisted := geoblock.IsAllowlistedPlatform("web", secondTime)
				otherAllowlisted := geoblock.IsAllowlistedPlatform("other", secondTime)
				So(webAllowlisted, ShouldBeTrue)
				So(otherAllowlisted, ShouldBeFalse)
			})
			Convey("Test deserialization with omitted AllowlistPlatforms", func() {
				var geoblockRawData = []byte(`{"TimeWindows":[{"StartTime":"2010-01-01T00:00:00-08:00","EndTime":"2030-01-01T00:00:00-08:00"}],"BlockAnonymousIPs":true,"BlocklistedZipCodes":{},"AllowlistedCountries":{},"BlocklistedCountries":{"CN":true}}`)
				var geoblock = Geoblock{}
				err := json.Unmarshal(geoblockRawData, &geoblock)
				if err != nil {
					t.Error(err)
				}
				webAllowlisted := geoblock.IsAllowlistedPlatform("web", secondTime)
				otherAllowlisted := geoblock.IsAllowlistedPlatform("other", secondTime)
				So(webAllowlisted, ShouldBeTrue)
				So(otherAllowlisted, ShouldBeTrue)
			})
			Convey("Test deserialization with present but empty AllowlistPlatforms", func() {
				var geoblockRawData = []byte(`{"TimeWindows":[{"StartTime":"2010-01-01T00:00:00-08:00","EndTime":"2030-01-01T00:00:00-08:00"}],"BlockAnonymousIPs":true,"BlocklistedZipCodes":{},"AllowlistedCountries":{},"BlocklistedCountries":{"CN":true},"AllowlistedPlatforms":{}}`)
				var geoblock = Geoblock{}
				err := json.Unmarshal(geoblockRawData, &geoblock)
				if err != nil {
					t.Error(err)
				}
				webAllowlisted := geoblock.IsAllowlistedPlatform("web", secondTime)
				otherAllowlisted := geoblock.IsAllowlistedPlatform("other", secondTime)
				So(webAllowlisted, ShouldBeTrue)
				So(otherAllowlisted, ShouldBeTrue)
			})
		})
	})
}

func TestTimeWindows(t *testing.T) {
	Convey("Test SomeWindowIncludes", t, func() {
		Convey("When there is only one TimeWindow", func() {
			geoblock := Geoblock{
				TimeWindows: []TimeWindow{
					{
						StartTime: OctFirst,
						EndTime:   NovFirst,
					},
				},
			}

			So(geoblock.SomeWindowIncludes(Oct15), ShouldBeTrue)
		})
		Convey("When there are multiple TimeWindows", func() {
			geoblock := Geoblock{
				TimeWindows: []TimeWindow{
					{
						StartTime: OctFirst,
						EndTime:   Oct15,
					},
					{
						StartTime: NovFirst,
						EndTime:   Nov15,
					},
				},
			}
			So(geoblock.SomeWindowIncludes(Oct7), ShouldBeTrue)
			So(geoblock.SomeWindowIncludes(Oct21), ShouldBeFalse)
			So(geoblock.SomeWindowIncludes(Nov7), ShouldBeTrue)
			So(geoblock.SomeWindowIncludes(Nov21), ShouldBeFalse)
		})
		Convey("When there are multiple TimeWindows, but some are invalid", func() {
			geoblock := Geoblock{
				TimeWindows: []TimeWindow{
					{
						StartTime: NovFirst,
						EndTime:   Oct15,
					},
					{
						StartTime: NovFirst,
						EndTime:   Nov15,
					},
				},
			}
			So(geoblock.SomeWindowIncludes(Oct7), ShouldBeFalse)
			So(geoblock.SomeWindowIncludes(Oct21), ShouldBeFalse)
			So(geoblock.SomeWindowIncludes(Nov7), ShouldBeTrue)
			So(geoblock.SomeWindowIncludes(Nov21), ShouldBeFalse)
		})
	})
}
