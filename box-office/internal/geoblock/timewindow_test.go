package geoblock

import (
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

var (
	OctFirst = time.Date(2018, 10, 1, 0, 0, 0, 0, time.UTC)
	Oct7     = time.Date(2018, 10, 7, 0, 0, 0, 0, time.UTC)
	Oct15    = time.Date(2018, 10, 15, 0, 0, 0, 0, time.UTC)
	Oct21    = time.Date(2018, 10, 21, 0, 0, 0, 0, time.UTC)
	NovFirst = time.Date(2018, 11, 1, 0, 0, 0, 0, time.UTC)
	Nov7     = time.Date(2018, 11, 7, 0, 0, 0, 0, time.UTC)
	Nov15    = time.Date(2018, 11, 15, 0, 0, 0, 0, time.UTC)
	Nov21    = time.Date(2018, 11, 21, 0, 0, 0, 0, time.UTC)
)

// Tests for single time windows
func TestTimeWindow(t *testing.T) {
	Convey("Test isValid", t, func() {
		Convey("When TimeWindow is valid", func() {
			timewindow := TimeWindow{
				StartTime: OctFirst,
				EndTime:   NovFirst,
			}
			So(timewindow.IsValid(), ShouldBeTrue)
		})
		Convey("When TimeWindow is invalid", func() {
			timewindow := TimeWindow{
				StartTime: NovFirst,
				EndTime:   OctFirst,
			}
			So(timewindow.IsValid(), ShouldBeFalse)
		})
		Convey("When TimeWindow is missing data", func() {
			timewindow := TimeWindow{
				StartTime: OctFirst,
			}
			So(timewindow.IsValid(), ShouldBeFalse)
		})
	})
	Convey("Test Includes", t, func() {
		Convey("When time is inside the window", func() {
			timewindow := TimeWindow{
				StartTime: OctFirst,
				EndTime:   NovFirst,
			}
			timeToTest := Oct15
			So(timewindow.Includes(timeToTest), ShouldBeTrue)
		})
		Convey("When time is outside the window", func() {
			timewindow := TimeWindow{
				StartTime: OctFirst,
				EndTime:   NovFirst,
			}
			timeToTest := Nov15
			So(timewindow.Includes(timeToTest), ShouldBeFalse)
		})
		Convey("When window is invalid", func() {
			timewindow := TimeWindow{
				StartTime: NovFirst,
				EndTime:   OctFirst,
			}
			timeToTest := Oct15
			So(timewindow.Includes(timeToTest), ShouldBeFalse)
		})
	})
}
