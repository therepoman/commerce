package sandstorm

import "fmt"

const (
	sandstormPathBoxOffice = "commerce/box-office/%s/%s"
)

func BuildSandstormSecretName(environment string, key string) string {
	return fmt.Sprintf(sandstormPathBoxOffice, environment, key)
}
