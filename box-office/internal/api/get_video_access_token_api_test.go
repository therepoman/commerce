package api

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/cb/hallpass/view"
	"code.justin.tv/commerce/box-office/config"
	"code.justin.tv/commerce/box-office/internal/clients"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"
	"code.justin.tv/commerce/box-office/mocks"
	borpc "code.justin.tv/commerce/box-office/rpc"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	"code.justin.tv/web/users-service/client"
	"code.justin.tv/web/users-service/models"
	"github.com/twitchtv/twirp"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	userLogin   = "testuser"
	userID      = "123456"
	channelName = "channelName"
)

func TestVideoAccessTokenAPI(t *testing.T) {
	Convey("Test VideoAccessTokenAPI", t, func() {
		mockSigner := new(mocks.ISigner)
		mockPlatformRecorder := new(mocks.IPlatformRecorder)

		mockPermissionsLoader := new(mocks.Attacher)
		mockSubsLoader := new(mocks.Attacher)
		mockUsersLoader := new(mocks.Attacher)
		mockChannelProductsLoader := new(mocks.Attacher)
		mockStatter := new(mocks.Statter)
		mockStatter.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		clients := &clients.Clients{
			Signer: mockSigner,
		}

		loaderMap := &loaders.Attachers{
			LoaderMap: map[loaders.LoaderKey]loaders.Attacher{
				loaders.Permissions:     mockPermissionsLoader,
				loaders.Subscriptions:   mockSubsLoader,
				loaders.UserByLogin:     mockUsersLoader,
				loaders.ChannelProducts: mockChannelProductsLoader,
			},
		}

		api := GetVideoAccessTokenAPI{
			Clients:          clients,
			Config:           &config.Configuration{},
			Loaders:          loaderMap,
			PlatformRecorder: mockPlatformRecorder,
			Stats:            mockStatter,
		}
		mockPlatformRecorder.On("RecordPlatformMetrics", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return()

		Convey("Test GetVideoAccessToken", func() {
			permData := &view.GetIsEditorResponse{}
			mockPermissionsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(permData, nil))

			subsData := &substwirp.Subscription{}
			mockSubsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(subsData, nil))

			productsData := []*substwirp.Product{}
			mockChannelProductsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(productsData, nil))

			signature := "signature"
			mockSigner.On("Sign", mock.Anything).Return(signature, nil)

			Convey("Happy case", func() {
				userData := &models.Properties{
					ID: userID,
				}
				mockUsersLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(userData, nil))

				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
				}

				resp, err := api.GetVideoAccessToken(context.Background(), request)
				So(err, ShouldBeNil)
				So(resp.Token, ShouldNotBeEmpty)
				So(resp.Sig, ShouldNotBeEmpty)
			})

			Convey("Missing channel name and channel ID", func() {
				userData := &models.Properties{}
				mockUsersLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(userData, nil))

				request := &borpc.GetVideoAccessTokenRequest{}

				_, err := api.GetVideoAccessToken(context.Background(), request)
				So(err, ShouldNotBeNil)
				twirpErr, ok := err.(twirp.Error)
				So(ok, ShouldBeTrue)
				So(twirpErr.Code(), ShouldEqual, twirp.NotFound)
			})

			Convey("Channel not found error", func() {
				mockUsersLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(nil, &client.UserNotFoundError{}))

				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
				}

				_, err := api.GetVideoAccessToken(context.Background(), request)
				So(err, ShouldNotBeNil)
				twirpErr, ok := err.(twirp.Error)
				So(ok, ShouldBeTrue)
				So(twirpErr.Code(), ShouldEqual, twirp.NotFound)
			})
		})

		Convey("Test internalGetVideoAccessToken", func() {
			Convey("Happy case", func() {
				permData := &view.GetIsEditorResponse{}
				mockPermissionsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(permData, nil))

				subsData := &substwirp.Subscription{}
				mockSubsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(subsData, nil))

				userData := &models.Properties{
					ID: userID,
				}
				mockUsersLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(userData, nil))

				productsData := []*substwirp.Product{}
				mockChannelProductsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(productsData, nil))

				signature := "signature"
				mockSigner.On("Sign", mock.Anything).Return(signature, nil)

				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
				}

				resp, err := api.internalGetVideoAccessToken(context.Background(), request)
				So(err, ShouldBeNil)
				So(resp.Token, ShouldNotBeEmpty)
				So(resp.Sig, ShouldEqual, signature)
			})

			Convey("Error in loaders", func() {
				mockPermissionsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(nil, errors.New("test")))

				mockSubsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(nil, errors.New("test")))

				mockUsersLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(nil, errors.New("test")))

				mockChannelProductsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(nil, errors.New("test")))

				signature := "signature"
				mockSigner.On("Sign", mock.Anything).Return(signature, nil)

				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
					ChannelId:   userID,
				}

				resp, err := api.internalGetVideoAccessToken(context.Background(), request)
				So(err, ShouldBeNil)
				So(resp.Token, ShouldNotBeEmpty)
				So(resp.Sig, ShouldEqual, signature)
			})

			Convey("Error in signer", func() {
				permData := &view.GetIsEditorResponse{}
				mockPermissionsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(permData, nil))

				subsData := &substwirp.Subscription{}
				mockSubsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(subsData, nil))

				userData := &models.Properties{
					ID: userID,
				}
				mockUsersLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(userData, nil))

				productsData := []*substwirp.Product{}
				mockChannelProductsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(productsData, nil))

				mockSigner.On("Sign", mock.Anything).Return("", errors.New("test"))

				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
				}

				_, err := api.internalGetVideoAccessToken(context.Background(), request)
				So(err, ShouldNotBeNil)
			})

			Convey("Missing ChannelName", func() {
				permData := &view.GetIsEditorResponse{}
				mockPermissionsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(permData, nil))

				subsData := &substwirp.Subscription{}
				mockSubsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(subsData, nil))

				userData := &models.Properties{
					ID: userID,
				}
				mockUsersLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(userData, nil))

				productsData := []*substwirp.Product{}
				mockChannelProductsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(productsData, nil))

				request := &borpc.GetVideoAccessTokenRequest{}
				_, err := api.internalGetVideoAccessToken(context.Background(), request)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("Test GenerateFallbackVideoAccessToken", func() {
			signature := "signature"
			mockSigner.On("Sign", mock.Anything).Return(signature, nil)

			request := &borpc.GetVideoAccessTokenRequest{}

			resp := api.GenerateFallbackResponse(request)
			So(resp.Sig, ShouldEqual, signature)
		})
	})
}
