package api

import (
	"context"
	"errors"
	"net/http"
	"testing"

	"code.justin.tv/commerce/box-office/config"
	"code.justin.tv/commerce/box-office/internal/clients"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"
	"code.justin.tv/commerce/box-office/mocks"
	borpc "code.justin.tv/commerce/box-office/rpc"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"code.justin.tv/foundation/twitchclient"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

var (
	testClipBroadcastID = "testClipBroadcastID"
	testClipSlug        = "testClipSlug"
	testClipURI         = "https://test-clip-uri.mp4"
)

func TestClipAccessTokenAPI(t *testing.T) {
	Convey("test ClipAccessTokenAPI", t, func() {
		mockSigner := new(mocks.ISigner)
		mockBroadcastRestrictionsLoader := new(mocks.Attacher)
		mockClipBroadcastIDLoader := new(mocks.Attacher)
		mockClipURILoader := new(mocks.Attacher)

		mockPlatformRecorder := new(mocks.IPlatformRecorder)
		mockPlatformRecorder.On("RecordPlatformMetrics", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return()

		mockStatter := new(mocks.Statter)
		mockStatter.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)
		clients := &clients.Clients{
			Signer: mockSigner,
		}

		loaderMap := &loaders.Attachers{
			LoaderMap: map[loaders.LoaderKey]loaders.Attacher{
				loaders.BroadcastRestrictions: mockBroadcastRestrictionsLoader,
				loaders.ClipBroadcastID:       mockClipBroadcastIDLoader,
				loaders.ClipURI:               mockClipURILoader,
			},
		}

		api := GetClipAccessTokenAPI{
			Clients:          clients,
			Loaders:          loaderMap,
			Config:           &config.Configuration{},
			PlatformRecorder: mockPlatformRecorder,
			Stats:            mockStatter,
		}

		Convey("test GetClipAccessToken", func() {
			niohData := &nioh.RestrictionResourceResponse{}

			signature := "signature"
			mockSigner.On("Sign", mock.Anything).Return(signature, nil)

			Convey("happy case", func() {
				mockBroadcastRestrictionsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(niohData, nil))
				mockClipBroadcastIDLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(&testClipBroadcastID, nil))
				mockClipURILoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(testClipURI, nil))

				request := &borpc.GetClipAccessTokenRequest{
					ClipSlug: testClipSlug,
				}

				resp, err := api.GetClipAccessToken(context.Background(), request)
				So(err, ShouldBeNil)
				So(resp.Token, ShouldNotBeEmpty)
				So(resp.Sig, ShouldEqual, signature)
			})

			Convey("when clip slug is empty return an error", func() {
				request := &borpc.GetClipAccessTokenRequest{}

				resp, err := api.GetClipAccessToken(context.Background(), request)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("when the clip isn't found return an error", func() {
				mockBroadcastRestrictionsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(niohData, nil))
				mockClipBroadcastIDLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(&testClipBroadcastID, nil))
				mockClipURILoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(nil, &twitchclient.Error{StatusCode: http.StatusNotFound}))

				request := &borpc.GetClipAccessTokenRequest{
					ClipSlug: testClipSlug,
				}

				resp, err := api.GetClipAccessToken(context.Background(), request)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
			})

			Convey("when a loader errors a token is still returned", func() {
				mockBroadcastRestrictionsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(niohData, nil))
				mockClipBroadcastIDLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(&testClipBroadcastID, nil))
				mockClipURILoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(nil, errors.New("test error")))
				request := &borpc.GetClipAccessTokenRequest{
					ClipSlug: testClipSlug,
				}

				resp, err := api.GetClipAccessToken(context.Background(), request)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
			})
		})
	})
}
