package api

import (
	"context"
	"encoding/json"
	"strconv"

	"code.justin.tv/commerce/box-office/config"
	"code.justin.tv/commerce/box-office/internal/clients"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/resolvers"
	"code.justin.tv/commerce/box-office/internal/stats"
	borpc "code.justin.tv/commerce/box-office/rpc"
	"code.justin.tv/commerce/box-office/token"
	"code.justin.tv/web/users-service/client"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
	gerrors "github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

const (
	defaultMobileRestricted = false
	fallbackVideoExpiry     = 4677266623
	videoCacheControl       = "public, max-age=10"
)

// GetVideoAccessTokenAPI Get Access token for live video
// Reference documentation:
//  - https://docs.google.com/document/d/1GPXym_q-MDrax6TMGx0yqB7J88nOnyjzfSmuaoA6pRg/edit#
//  - https://docs.google.com/document/d/17Mu9OqS_BKZFhFJrNjI6w2OFSgtX6co7MZWQb1oDUmc/edit#
type GetVideoAccessTokenAPI struct {
	Clients          *clients.Clients
	Config           *config.Configuration
	Loaders          *loaders.Attachers
	PlatformRecorder stats.IPlatformRecorder
	Stats            statsd.Statter
}

// GetVideoAccessToken returns an access token that determines the user access to a channel's video.
//                It also includes a signature that is verified in Usher (the subsequent call from the
//                client) to verify that the token has not been manipulated.
func (gvat *GetVideoAccessTokenAPI) GetVideoAccessToken(context context.Context, request *borpc.GetVideoAccessTokenRequest) (*borpc.GetVideoAccessTokenResponse, error) {
	gvat.PlatformRecorder.RecordPlatformMetrics("video", request.Platform, request.PlayerType, request.DeviceId)

	response, err := gvat.internalGetVideoAccessToken(context, request)
	if err != nil {
		rootErr := gerrors.Cause(err)
		switch rootErr.(type) {
		case *client.UserNotFoundError:
			return nil, twirp.NotFoundError("Channel not found")
		default:
			log.Errorf("Error getting video access token. Returning fallback token. Error: %+v", err)
			return gvat.GenerateFallbackResponse(request), nil
		}
	}

	return response, nil
}

func (gvat *GetVideoAccessTokenAPI) internalGetVideoAccessToken(context context.Context, request *borpc.GetVideoAccessTokenRequest) (*borpc.GetVideoAccessTokenResponse, error) {
	context = gvat.Loaders.Attach(context)
	resolver, err := resolvers.NewVideoAccessTokenResolver(context, gvat.Config, request, gvat.Stats)
	if err != nil {
		return nil, errors.Wrap(err, "Error creating new video access token resolver")
	}

	token, err := resolver.Resolve(context)
	if err != nil {
		return nil, errors.Wrap(err, "Error resolving video access token")
	}

	// Token cache control logic. Tokens which forbid the user should _not_ be cached, so that if the user
	// becomes eligible (via subscribing to a channel, for example), they gain access as fast as possible.
	var cacheControlHeaderValue string
	if token.Authorization.Forbidden {
		cacheControlHeaderValue = cacheControlNoCache
	} else {
		cacheControlHeaderValue = videoCacheControl
	}

	if err := twirp.SetHTTPResponseHeader(context, "Cache-Control", cacheControlHeaderValue); err != nil {
		log.Errorf("Error setting cache control header for video. Ignoring. Error: %+v", err)
	}

	tokenBytes, err := json.Marshal(token)
	if err != nil {
		return nil, errors.Wrap(err, "Error marshalling video access token")
	}

	sig, err := gvat.Clients.Signer.Sign(tokenBytes)
	if err != nil {
		return nil, errors.Wrap(err, "Error signing token")
	}

	return &borpc.GetVideoAccessTokenResponse{
		Token:            string(tokenBytes),
		Sig:              sig,
		MobileRestricted: defaultMobileRestricted, // hard-coded to false, unused.
		ExpiresAt:        token.Expires,
	}, nil
}

// GenerateFallbackResponse generates a static fallback token with hard-coded fail-open values.
// Fail-open in this context means allow the video to be shown.
func (gvat *GetVideoAccessTokenAPI) GenerateFallbackResponse(request *borpc.GetVideoAccessTokenRequest) *borpc.GetVideoAccessTokenResponse {
	token, tokenBytes := getFallbackToken(request)

	sig, err := gvat.Clients.Signer.Sign(tokenBytes)
	if err != nil {
		log.Errorf("Error signing the fallback token. Swallowing the error and returning empty signature. Error: %+v", err)
		sig = ""
	}

	return &borpc.GetVideoAccessTokenResponse{
		Token:            string(tokenBytes),
		Sig:              sig,
		MobileRestricted: defaultMobileRestricted, // hard-coded to false, unsused.
		ExpiresAt:        token.Expires,
	}
}

func getFallbackToken(request *borpc.GetVideoAccessTokenRequest) (token.VideoAccessToken, []byte) {
	channelIDInt, err := strconv.ParseInt(request.ChannelId, 10, 64)
	if err != nil {
		log.Errorf("Error parsing channelID from request. Swallowing the error and returning empty token. Error: %+v", err)
		return token.VideoAccessToken{}, []byte("{}")
	}

	fallbackPrivate := token.Private{
		AllowedToView: true,
	}

	fallbackChanSub := token.VideoChanSub{
		RestrictedBitrates: []string{},
		ViewUntil:          resolvers.DefaultViewUntil,
	}

	fallbackToken := token.VideoAccessToken{
		Adblock:                false,
		Channel:                request.ChannelName,
		ChannelID:              &channelIDInt,
		ChanSub:                fallbackChanSub,
		ChannelIsGeoBlocked:    false,
		GeoblockReason:         "",
		DeviceID:               nil,
		Expires:                fallbackVideoExpiry,
		ExtendedHistoryAllowed: false,
		Game:                   "",
		HideAds:                false,
		HTTPSRequired:          false,
		Mature:                 false,
		Partner:                false,
		Platform:               nil,
		PlayerType:             nil,
		Private:                fallbackPrivate,
		Privileged:             false,
		Role:                   "",
		ShowAds:                true,
		Subscriber:             false,
		Turbo:                  false,
		UserID:                 nil,
		UserIP:                 "",
		Version:                resolvers.TokenVersion,
	}

	tokenBytes, err := json.Marshal(fallbackToken)
	if err != nil {
		log.Errorf("Error marshalling the fallback token. Swallowing the error and returning empty token. Error: %+v", err)
		return token.VideoAccessToken{}, []byte("{}")
	}
	return fallbackToken, tokenBytes
}
