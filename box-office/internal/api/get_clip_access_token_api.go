package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"code.justin.tv/commerce/box-office/config"
	"code.justin.tv/commerce/box-office/internal/clients"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/resolvers"
	"code.justin.tv/commerce/box-office/internal/stats"
	borpc "code.justin.tv/commerce/box-office/rpc"
	"code.justin.tv/commerce/box-office/token"
	"code.justin.tv/common/twirp"
	"code.justin.tv/foundation/twitchclient"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

const (
	clipCacheControl   = "public, max-age=60"
	clipFallbackExpiry = 1 * time.Hour
)

type GetClipAccessTokenAPI struct {
	Clients          *clients.Clients
	Config           *config.Configuration
	Loaders          *loaders.Attachers
	PlatformRecorder stats.IPlatformRecorder
	Stats            statsd.Statter
}

func (gcat *GetClipAccessTokenAPI) GetClipAccessToken(ctx context.Context, request *borpc.GetClipAccessTokenRequest) (*borpc.GetClipAccessTokenResponse, error) {
	gcat.PlatformRecorder.RecordPlatformMetrics("clip", request.Platform, request.PlayerType, request.DeviceId)

	if request == nil || request.ClipSlug == "" {
		return nil, twirp.RequiredArgumentError("clip_slug")
	}

	response, err := gcat.getClipAccessToken(ctx, request)
	if err != nil {
		rootErr := errors.Cause(err)
		tcErr, ok := rootErr.(*twitchclient.Error)
		if ok && tcErr.StatusCode == http.StatusNotFound {
			return nil, twirp.NotFoundError("clip not found")
		}

		log.Errorf("error getting clip access token. returning fallback token. error: %v", err)
		return gcat.GenerateFallbackResponse(request), nil
	}

	return response, nil

}

func (gcat *GetClipAccessTokenAPI) getClipAccessToken(ctx context.Context, request *borpc.GetClipAccessTokenRequest) (*borpc.GetClipAccessTokenResponse, error) {
	ctx = gcat.Loaders.Attach(ctx)
	resolver, err := resolvers.NewClipAccessTokenResolver(ctx, request, gcat.Stats)
	if err != nil {
		return nil, errors.Wrap(err, "error creating new clip access token resolver")
	}

	token, err := resolver.Resolve(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "error resolving clip access token")
	}

	var cacheControlHeaderValue string
	if token.Authorization.Forbidden {
		cacheControlHeaderValue = cacheControlNoCache
	} else {
		cacheControlHeaderValue = clipCacheControl
	}

	if err := twirp.SetHTTPResponseHeader(ctx, "Cache-Control", cacheControlHeaderValue); err != nil {
		log.Errorf("error setting cache control header for clip token. ignoring. error :%+v", err)
	}

	tokenBytes, err := json.Marshal(token)
	if err != nil {
		return nil, errors.Wrap(err, "error marshalling clip access token")
	}

	sig, err := gcat.Clients.Signer.Sign(tokenBytes)
	if err != nil {
		return nil, errors.Wrap(err, "error signing token")
	}

	return &borpc.GetClipAccessTokenResponse{
		Token:     string(tokenBytes),
		Sig:       sig,
		ExpiresAt: token.Expires,
	}, nil
}

// GenerateFallbackResponse returns a default token that expires
// in a short duration than a normal token does. ClipURI is empty
// because a network call to clips might have failed, which
// prevents us from populating the ClipURI field
func (gcat *GetClipAccessTokenAPI) GenerateFallbackResponse(request *borpc.GetClipAccessTokenRequest) *borpc.GetClipAccessTokenResponse {
	fallbackToken := token.ClipAccessToken{
		Authorization: token.Authorization{},
		ClipURI:       "",
		DeviceID:      &request.DeviceId,
		Expires:       time.Now().Add(clipFallbackExpiry).Unix(),
		UserID:        request.UserId,
		Version:       resolvers.TokenVersion,
	}

	tokenBytes, err := json.Marshal(fallbackToken)
	if err != nil {
		log.Errorf("error marshalling clip fallback token. returning an empty token")
		tokenBytes = []byte("{}")
	}

	sig, err := gcat.Clients.Signer.Sign(tokenBytes)
	if err != nil {
		log.Errorf("error signing clip fallback token. returning empty signature")
		sig = ""
	}

	return &borpc.GetClipAccessTokenResponse{
		Token:     string(tokenBytes),
		Sig:       sig,
		ExpiresAt: fallbackToken.Expires,
	}
}
