package api

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"testing"

	voyager "code.justin.tv/amzn/TwitchVoyagerTwirp"
	"code.justin.tv/commerce/box-office/config"
	"code.justin.tv/commerce/box-office/internal/clients"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"
	"code.justin.tv/commerce/box-office/mocks"
	borpc "code.justin.tv/commerce/box-office/rpc"
	"code.justin.tv/commerce/box-office/token"
	"code.justin.tv/foundation/twitchclient"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	"code.justin.tv/vod/vodapi/rpc/vodapi"
	"code.justin.tv/web/users-service/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

const (
	vodID    = "123456"
	deviceID = "09398-22484-59623"
)

func TestVODAccessTokenAPI(t *testing.T) {
	Convey("Test VODAccessTokenAPI", t, func() {
		mockSigner := new(mocks.ISigner)
		mockPlatformRecorder := new(mocks.IPlatformRecorder)
		mockSubsLoader := new(mocks.Attacher)
		mockUsersLoader := new(mocks.Attacher)
		mockVODByIDLoader := new(mocks.Attacher)
		mockChannelProductsLoader := new(mocks.Attacher)
		mockStatter := new(mocks.Statter)
		mockStatter.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		clients := &clients.Clients{
			Signer: mockSigner,
		}

		loaderMap := &loaders.Attachers{
			LoaderMap: map[loaders.LoaderKey]loaders.Attacher{
				loaders.Subscriptions:   mockSubsLoader,
				loaders.UserByLogin:     mockUsersLoader,
				loaders.VODByID:         mockVODByIDLoader,
				loaders.ChannelProducts: mockChannelProductsLoader,
			},
		}

		api := GetVODAccessTokenAPI{
			Clients:          clients,
			Loaders:          loaderMap,
			Config:           &config.Configuration{},
			PlatformRecorder: mockPlatformRecorder,
			Stats:            mockStatter,
		}
		mockPlatformRecorder.On("RecordPlatformMetrics", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return()

		Convey("Test GetVODAccessToken", func() {
			subsData := &substwirp.Subscription{}
			mockSubsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(subsData, nil))

			userData := &models.Properties{}
			mockUsersLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(userData, nil))

			productsData := []*substwirp.Product{}
			mockChannelProductsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(productsData, nil))

			signature := "signature"
			mockSigner.On("Sign", mock.Anything).Return(signature, nil)

			Convey("Happy case", func() {
				request := &borpc.GetVODAccessTokenRequest{
					VodId: vodID,
				}

				vodapiData := &vodapi.Vod{
					OwnerId: "12345",
				}
				mockVODByIDLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(vodapiData, nil))

				resp, err := api.GetVODAccessToken(context.Background(), request)
				So(err, ShouldBeNil)
				So(resp.Token, ShouldNotBeEmpty)
				So(resp.Sig, ShouldNotBeEmpty)
			})

			Convey("Pass Device ID through", func() {
				request := &borpc.GetVODAccessTokenRequest{
					VodId:    vodID,
					DeviceId: deviceID,
				}

				vodapiData := &vodapi.Vod{
					OwnerId: "12345",
				}
				mockVODByIDLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(vodapiData, nil))

				resp, err := api.GetVODAccessToken(context.Background(), request)
				So(err, ShouldBeNil)
				So(resp.Token, ShouldNotBeEmpty)

				unmarshalled := token.VODAccessToken{}
				err = json.Unmarshal([]byte(resp.Token), &unmarshalled)
				So(err, ShouldBeNil)

				So(unmarshalled.DeviceID, ShouldNotBeNil)
				So(*unmarshalled.DeviceID, ShouldEqual, deviceID)
			})

			Convey("Pass missing Device ID through", func() {
				request := &borpc.GetVODAccessTokenRequest{
					VodId: vodID,
				}

				vodapiData := &vodapi.Vod{
					OwnerId: "12345",
				}
				mockVODByIDLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(vodapiData, nil))

				resp, err := api.GetVODAccessToken(context.Background(), request)
				So(err, ShouldBeNil)
				So(resp.Token, ShouldNotBeEmpty)

				unmarshalled := token.VODAccessToken{}
				err = json.Unmarshal([]byte(resp.Token), &unmarshalled)
				So(err, ShouldBeNil)

				So(unmarshalled.DeviceID, ShouldBeNil)
			})

			Convey("Missing VODID", func() {
				request := &borpc.GetVODAccessTokenRequest{}

				vodapiData := &vodapi.Vod{
					OwnerId: "12345",
				}
				mockVODByIDLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(vodapiData, nil))

				_, err := api.GetVODAccessToken(context.Background(), request)
				So(err, ShouldNotBeNil)
				twirpErr, ok := err.(twirp.Error)
				So(ok, ShouldBeTrue)
				So(twirpErr.Code(), ShouldEqual, twirp.NotFound)
			})

			Convey("VODID not found error", func() {
				request := &borpc.GetVODAccessTokenRequest{
					VodId: vodID,
				}

				mockVODByIDLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(nil, &twitchclient.Error{StatusCode: http.StatusNotFound}))

				_, err := api.GetVODAccessToken(context.Background(), request)
				So(err, ShouldNotBeNil)
				twirpErr, ok := err.(twirp.Error)
				So(ok, ShouldBeTrue)
				So(twirpErr.Code(), ShouldEqual, twirp.NotFound)
			})

			Convey("VODID other error", func() {
				request := &borpc.GetVODAccessTokenRequest{
					VodId: vodID,
				}

				mockVODByIDLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(nil, errors.New("test error")))

				resp, err := api.GetVODAccessToken(context.Background(), request)
				So(err, ShouldBeNil)
				So(resp.Token, ShouldNotBeEmpty)
				So(resp.Sig, ShouldNotBeEmpty)
			})
		})

		Convey("Test internalGetVODAccessToken", func() {
			Convey("Happy case", func() {
				subsData := &voyager.Subscription{}
				mockSubsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(subsData, nil))

				userData := &models.Properties{}
				mockUsersLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(userData, nil))

				vodapiData := &vodapi.Vod{
					OwnerId: "12345",
				}
				mockVODByIDLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(vodapiData, nil))

				productsData := []*substwirp.Product{}
				mockChannelProductsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(productsData, nil))

				signature := "signature"
				mockSigner.On("Sign", mock.Anything).Return(signature, nil)

				request := &borpc.GetVODAccessTokenRequest{
					VodId: vodID,
				}

				resp, err := api.internalGetVODAccessToken(context.Background(), request)
				So(err, ShouldBeNil)
				So(resp.Token, ShouldNotBeEmpty)
				So(resp.Sig, ShouldEqual, signature)
			})

			Convey("Happy case w/ legacy vod id", func() {
				subsData := &voyager.Subscription{}
				mockSubsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(subsData, nil))

				userData := &models.Properties{}
				mockUsersLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(userData, nil))

				vodapiData := &vodapi.Vod{
					OwnerId: "12345",
				}
				mockVODByIDLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(vodapiData, nil))

				productsData := []*substwirp.Product{}
				mockChannelProductsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(productsData, nil))

				signature := "signature"
				mockSigner.On("Sign", mock.Anything).Return(signature, nil)

				request := &borpc.GetVODAccessTokenRequest{
					VodId: "v123456",
				}

				resp, err := api.internalGetVODAccessToken(context.Background(), request)
				So(err, ShouldBeNil)
				So(resp.Token, ShouldNotBeEmpty)
				So(resp.Sig, ShouldEqual, signature)
			})

			Convey("Error in secondary loaders", func() {
				mockSubsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(nil, errors.New("test")))
				mockUsersLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(nil, errors.New("test")))

				vodapiData := &vodapi.Vod{
					OwnerId: "12345",
				}
				mockVODByIDLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(vodapiData, nil))

				mockChannelProductsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(nil, errors.New("test")))

				signature := "signature"
				mockSigner.On("Sign", mock.Anything).Return(signature, nil)

				request := &borpc.GetVODAccessTokenRequest{
					VodId: vodID,
				}

				resp, err := api.internalGetVODAccessToken(context.Background(), request)
				So(err, ShouldBeNil)
				So(resp.Token, ShouldNotBeEmpty)
				So(resp.Sig, ShouldEqual, signature)
			})

			Convey("Error in critical loader", func() {
				subsData := &voyager.Subscription{}
				mockSubsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(subsData, nil))

				userData := &models.Properties{}
				mockUsersLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(userData, nil))

				mockVODByIDLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(nil, errors.New("test")))

				productsData := []*substwirp.Product{}
				mockChannelProductsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(productsData, nil))

				signature := "signature"
				mockSigner.On("Sign", mock.Anything).Return(signature, nil)

				request := &borpc.GetVODAccessTokenRequest{
					VodId: vodID,
				}

				_, err := api.internalGetVODAccessToken(context.Background(), request)
				So(err, ShouldNotBeNil)
			})

			Convey("Error in signer", func() {
				subsData := &voyager.Subscription{}
				mockSubsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(subsData, nil))

				userData := &models.Properties{}
				mockUsersLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(userData, nil))

				vodapiData := &vodapi.Vod{
					OwnerId: "12345",
				}
				mockVODByIDLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(vodapiData, nil))

				mockSigner.On("Sign", mock.Anything).Return("", errors.New("test"))

				request := &borpc.GetVODAccessTokenRequest{
					VodId: vodID,
				}

				productsData := []*substwirp.Product{}
				mockChannelProductsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(productsData, nil))

				_, err := api.internalGetVODAccessToken(context.Background(), request)
				So(err, ShouldNotBeNil)
			})

			Convey("Missing VODID", func() {
				subsData := &voyager.Subscription{}
				mockSubsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(subsData, nil))

				userData := &models.Properties{}
				mockUsersLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(userData, nil))

				vodapiData := &vodapi.Vod{
					OwnerId: "12345",
				}
				mockVODByIDLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(vodapiData, nil))

				productsData := []*substwirp.Product{}
				mockChannelProductsLoader.On("Attach", mock.Anything).Return(test.BuildBatchFunc(productsData, nil))

				request := &borpc.GetVODAccessTokenRequest{}
				_, err := api.internalGetVODAccessToken(context.Background(), request)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("Test GenerateFallbackVODAccessToken", func() {
			signature := "signature"
			mockSigner.On("Sign", mock.Anything).Return(signature, nil)

			request := &borpc.GetVODAccessTokenRequest{}

			resp := api.GenerateFallbackResponse(request)
			So(resp.Sig, ShouldEqual, signature)
		})

		Convey("Test stripVODIDPrefix", func() {
			result := stripVODIDPrefix(vodID)
			So(result, ShouldEqual, vodID)

			result = stripVODIDPrefix("v123456")
			So(result, ShouldEqual, vodID)

			emptyVODID := ""
			result = stripVODIDPrefix(emptyVODID)
			So(result, ShouldEqual, emptyVODID)
		})
	})
}
