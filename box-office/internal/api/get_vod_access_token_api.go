package api

import (
	"context"
	"encoding/json"
	"net/http"
	"strconv"
	"strings"

	"code.justin.tv/commerce/box-office/config"
	"code.justin.tv/commerce/box-office/internal/clients"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/resolvers"
	"code.justin.tv/commerce/box-office/internal/stats"
	borpc "code.justin.tv/commerce/box-office/rpc"
	"code.justin.tv/commerce/box-office/token"
	"code.justin.tv/foundation/twitchclient"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

const (
	fallbackVODExpiry = 4677266623
	vodCacheControl   = "public, max-age=60"
)

// GetVODAccessTokenAPI Get Access token for VODs
// Reference documentation:
//  - https://docs.google.com/document/d/17c_-22VGjQCO4qUjvZDCUxuJNui5JnCn5CBEjKp9Prg/edit#
type GetVODAccessTokenAPI struct {
	Loaders          *loaders.Attachers
	Clients          *clients.Clients
	Config           *config.Configuration
	PlatformRecorder stats.IPlatformRecorder
	Stats            statsd.Statter
}

// GetVODAccessToken returns an access token that determines the user access to a VOD.
// It also includes a signature to verify that the token has not been tampered with.
func (gvat *GetVODAccessTokenAPI) GetVODAccessToken(ctx context.Context, request *borpc.GetVODAccessTokenRequest) (*borpc.GetVODAccessTokenResponse, error) {
	gvat.PlatformRecorder.RecordPlatformMetrics("vod", request.Platform, request.PlayerType, request.DeviceId)

	response, err := gvat.internalGetVODAccessToken(ctx, request)
	if err != nil {
		rootErr := errors.Cause(err)
		tcErr, ok := rootErr.(*twitchclient.Error)
		if ok && (tcErr.StatusCode == http.StatusNotFound || tcErr.StatusCode == http.StatusBadRequest) {
			return nil, twirp.NotFoundError("VOD not found")
		}

		log.Errorf("Error getting VOD access token. Returning fallback token. Error: %+v", err)
		return gvat.GenerateFallbackResponse(request), nil
	}

	return response, nil
}

func (gvat *GetVODAccessTokenAPI) internalGetVODAccessToken(ctx context.Context, request *borpc.GetVODAccessTokenRequest) (*borpc.GetVODAccessTokenResponse, error) {
	ctx = gvat.Loaders.Attach(ctx)
	request.VodId = stripVODIDPrefix(request.VodId)
	resolver, err := resolvers.NewVODAccessTokenResolver(ctx, gvat.Config, request, gvat.Stats)
	if err != nil {
		return nil, errors.Wrap(err, "Error creating new VOD access token resolver")
	}

	token, err := resolver.Resolve(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "Error resolving VOD access token")
	}

	// Token cache control logic. Tokens which forbid the user should _not_ be cached, so that if the user
	// becomes eligible (via subscribing to a channel, for example), they gain access as fast as possible.
	var cacheControlHeaderValue string
	if token.Authorization.Forbidden {
		cacheControlHeaderValue = cacheControlNoCache
	} else {
		cacheControlHeaderValue = vodCacheControl
	}

	if err := twirp.SetHTTPResponseHeader(ctx, "Cache-Control", cacheControlHeaderValue); err != nil {
		log.Errorf("Error setting cache control header for VOD. Ignoring. Error: %+v", err)
	}

	tokenBytes, err := json.Marshal(token)
	if err != nil {
		return nil, errors.Wrap(err, "Error marshalling VOD access token")
	}

	sig, err := gvat.Clients.Signer.Sign(tokenBytes)
	if err != nil {
		return nil, errors.Wrap(err, "Error signing token")
	}

	return &borpc.GetVODAccessTokenResponse{
		Token:     string(tokenBytes),
		Sig:       sig,
		ExpiresAt: token.Expires,
	}, nil
}

// GenerateFallbackResponse generates a static fallback token with hard-coded fail-open values.
// Fail-open in this context means allow the VOD to be shown.
func (gvat *GetVODAccessTokenAPI) GenerateFallbackResponse(request *borpc.GetVODAccessTokenRequest) *borpc.GetVODAccessTokenResponse {
	vodIDInt, err := strconv.ParseInt(request.VodId, 10, 64)
	if err != nil {
		vodIDInt = int64(0)
	}

	fallbackChanSub := token.VODChanSub{}

	fallbackToken := token.VODAccessToken{
		ChanSub:       fallbackChanSub,
		Expires:       fallbackVODExpiry,
		HTTPSRequired: false,
		Privileged:    false,
		UserID:        nil,
		Version:       resolvers.TokenVersion,
		VODID:         &vodIDInt,
	}

	tokenBytes, err := json.Marshal(fallbackToken)
	if err != nil {
		log.Errorf("Error marshalling the fallback token. Swallowing the error and returning empty token")
		tokenBytes = []byte("{}")
	}

	sig, err := gvat.Clients.Signer.Sign(tokenBytes)
	if err != nil {
		log.Errorf("Error signing the fallback token. Swallowing the error and returning empty signature")
		sig = ""
	}

	return &borpc.GetVODAccessTokenResponse{
		Token:     string(tokenBytes),
		Sig:       sig,
		ExpiresAt: fallbackToken.Expires,
	}
}

// Some legacy VOD IDs are prefixed by the letter "v". Example "v231211173"
// This is no longer used by VodApi, and should be stripped away.
func stripVODIDPrefix(vodID string) string {
	return strings.TrimPrefix(vodID, "v")
}
