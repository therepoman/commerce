package api

import (
	"log"
	"net/http"

	goji "goji.io"
	"goji.io/pat"

	"github.com/cactus/go-statsd-client/statsd"

	"code.justin.tv/commerce/box-office/middleware"
	borpc "code.justin.tv/commerce/box-office/rpc"
	twirp_statsd "github.com/twitchtv/twirp/hooks/statsd"

	"code.justin.tv/foundation/twitchserver"

	"net/http/pprof"
)

const cacheControlNoCache = "no-cache"

// BoxOfficeServer API
type BoxOfficeServer struct {
	GetClipAccessTokenAPI
	GetVideoAccessTokenAPI
	GetVODAccessTokenAPI
}

// Server struct
type Server struct {
	*goji.Mux
}

// NewServer defines handlers
func (bo *BoxOfficeServer) NewServer(stats statsd.Statter) (*Server, error) {
	server := twitchserver.NewServer()

	twitchserver.AddDefaultSignalHandlers()
	s := &Server{
		server,
	}

	// middleware
	s.Use(middleware.PanicLogger)
	s.Use(middleware.ContextTimeout)

	s.HandleFunc(pat.Get("/"), s.everythingIsFine)
	s.HandleFunc(pat.Get("/health"), s.everythingIsFine)
	s.HandleFunc(pat.Get("/debug/running"), s.everythingIsFine)

	attachPprofHandlers(s.Mux)

	twirpStatsHook := twirp_statsd.NewStatsdServerHooks(stats)
	twirpHandler := borpc.NewBoxOfficeServer(bo, twirpStatsHook)
	s.Handle(pat.Post(borpc.BoxOfficePathPrefix+"*"), twirpHandler)

	return s, nil
}

// manually attach the pprof handlers in a location other than /debug so that we can attach
// them to the :8000 port. TwitchServer forces them into the :6000 port which is not exposed
// for usage where we need it
func attachPprofHandlers(s *goji.Mux) {
	s.Handle(pat.Get("/debug2/pprof/cmdline"), http.HandlerFunc(pprof.Cmdline))
	s.Handle(pat.Get("/debug2/pprof/cmdline/*"), http.HandlerFunc(pprof.Cmdline))
	s.Handle(pat.Get("/debug2/pprof/profile"), http.HandlerFunc(pprof.Profile))
	s.Handle(pat.Get("/debug2/pprof/profile/*"), http.HandlerFunc(pprof.Profile))
	s.Handle(pat.Get("/debug2/pprof/symbol"), http.HandlerFunc(pprof.Symbol))
	s.Handle(pat.Get("/debug2/pprof/symbol/*"), http.HandlerFunc(pprof.Symbol))
	s.Handle(pat.Get("/debug2/pprof/trace"), http.HandlerFunc(pprof.Trace))
	s.Handle(pat.Get("/debug2/pprof/trace/*"), http.HandlerFunc(pprof.Trace))
	s.Handle(pat.Get("/debug2/pprof"), http.HandlerFunc(pprof.Index))
	s.Handle(pat.Get("/debug2/pprof/*"), http.HandlerFunc(pprof.Index))
}

func (s *Server) everythingIsFine(w http.ResponseWriter, r *http.Request) {
	_, err := w.Write([]byte("OK"))
	if err != nil {
		log.Fatal(err)
	}
}
