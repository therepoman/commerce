package resolvers

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/cb/hallpass/view"
	"code.justin.tv/commerce/box-office/config"
	"code.justin.tv/web/users-service/client/channels"
	"code.justin.tv/web/users-service/models"

	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"
	"code.justin.tv/commerce/box-office/mocks"
	borpc "code.justin.tv/commerce/box-office/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestVideoPrivateResolver(t *testing.T) {
	Convey("Test VideoPrivateResolver", t, func() {
		simpleConfig := &config.Configuration{}
		mockStatter := new(mocks.Statter)
		mockStatter.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("Test constructor", func() {
			Convey("Happy case", func() {
				tokenResolver := &VideoAccessTokenResolver{}
				resolver, err := NewVideoPrivateResolver(context.Background(), tokenResolver)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)
			})

			Convey("Bad input", func() {
				resolver, err := NewVideoPrivateResolver(context.Background(), nil)
				So(err, ShouldNotBeNil)
				So(resolver, ShouldBeNil)
			})
		})

		Convey("Test AllowedToView", func() {
			Convey("channelID fails", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
				}

				context := test.SetupLoaderContext(context.Background(), loaders.UserByLogin, nil, errors.New("test"))

				tokenResolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(tokenResolver, ShouldNotBeNil)

				resolver, err := NewVideoPrivateResolver(context, tokenResolver)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.AllowedToView(context)
				So(resp, ShouldBeTrue)
			})

			Convey("error fetching channel properties", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelId: channelID,
				}

				context := test.SetupLoaderContext(context.Background(), loaders.ChannelProperties, nil, errors.New("test"))

				tokenResolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(tokenResolver, ShouldNotBeNil)

				resolver, err := NewVideoPrivateResolver(context, tokenResolver)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.AllowedToView(context)
				So(resp, ShouldBeTrue)
			})

			Convey("received channel properties response", func() {
				Convey("video is not private", func() {
					request := &borpc.GetVideoAccessTokenRequest{
						ChannelId: channelID,
					}

					channelResp := &channels.Channel{
						PrivateVideo: false,
					}
					context := test.SetupLoaderContext(context.Background(), loaders.ChannelProperties, channelResp, nil)

					tokenResolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(tokenResolver, ShouldNotBeNil)

					resolver, err := NewVideoPrivateResolver(context, tokenResolver)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					resp := resolver.AllowedToView(context)
					So(resp, ShouldBeTrue)
				})

				Convey("video is private", func() {
					isTrue := true
					isFalse := false

					channelResp := &channels.Channel{
						PrivateVideo: true,
					}
					context := test.SetupLoaderContext(context.Background(), loaders.ChannelProperties, channelResp, nil)
					Convey("user doesnt have private permissions", func() {
						request := &borpc.GetVideoAccessTokenRequest{
							ChannelId: channelID,
							UserId:    userID,
						}

						userResp := &models.Properties{
							Subadmin: &isFalse,
							Admin:    &isFalse,
						}
						context = test.SetupLoaderContext(context, loaders.UserByID, userResp, nil)

						permsResp := &view.GetIsEditorResponse{
							IsEditor: false,
						}
						context = test.SetupLoaderContext(context, loaders.Permissions, permsResp, nil)

						tokenResolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
						So(err, ShouldBeNil)
						So(tokenResolver, ShouldNotBeNil)

						resolver, err := NewVideoPrivateResolver(context, tokenResolver)
						So(err, ShouldBeNil)
						So(resolver, ShouldNotBeNil)

						resp := resolver.AllowedToView(context)
						So(resp, ShouldBeFalse)

					})

					Convey("user has private permissions", func() {
						request := &borpc.GetVideoAccessTokenRequest{
							ChannelId: channelID,
							UserId:    userID,
						}

						userResp := &models.Properties{
							Subadmin: &isTrue,
							Admin:    &isFalse,
						}
						context = test.SetupLoaderContext(context, loaders.UserByID, userResp, nil)

						tokenResolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
						So(err, ShouldBeNil)
						So(tokenResolver, ShouldNotBeNil)

						resolver, err := NewVideoPrivateResolver(context, tokenResolver)
						So(err, ShouldBeNil)
						So(resolver, ShouldNotBeNil)

						resp := resolver.AllowedToView(context)
						So(resp, ShouldBeTrue)
					})
				})
			})
		})
	})
}
