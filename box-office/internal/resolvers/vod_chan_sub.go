package resolvers

import (
	"context"
	"errors"

	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/resolvers/helpers"
	log "github.com/sirupsen/logrus"

	"code.justin.tv/commerce/box-office/token"
)

// VODChanSubResolver resolves VODChanSub objects
type VODChanSubResolver struct {
	TokenResolver *VODAccessTokenResolver
}

// NewVODChanSubResolver loads a VOD access token and returns a new resolver.
func NewVODChanSubResolver(ctx context.Context, tokenResolver *VODAccessTokenResolver) (*VODChanSubResolver, error) {
	if tokenResolver == nil {
		return nil, errors.New("tokenResolver must not be nil")
	}
	return &VODChanSubResolver{
		TokenResolver: tokenResolver,
	}, nil
}

// Resolve the token. Returns a resolved token
func (v *VODChanSubResolver) Resolve(ctx context.Context) *token.VODChanSub {
	return &token.VODChanSub{
		RestrictedBitrates: v.RestrictedBitrates(ctx),
	}
}

// RestrictedBitrates resolves the RestrictedBitrates field
// Default Value: []
func (v *VODChanSubResolver) RestrictedBitrates(ctx context.Context) []string {
	vodID := v.TokenResolver.VODIDString(ctx)

	// lookup VOD metadata
	vod, err := loaders.LoadVODByID(ctx, vodID)
	if err != nil {
		log.Errorf("Error loading VOD metadata. Using default value. Error: %+v", err)
		return defaultRestrictedBitrates
	}

	channelID := vod.OwnerId
	userID := v.TokenResolver.UserIDString(ctx)
	channelRestrictedBitrates := helpers.GetRestrictedBitratesForUserAndVod(ctx, channelID, userID, vodID)
	if containsString(channelRestrictedBitrates, "archives") {
		return vod.Qualities
	}
	return defaultRestrictedBitrates
}

func containsString(slice []string, s string) bool {
	for _, item := range slice {
		if item == s {
			return true
		}
	}
	return false
}
