package resolvers

import (
	"context"

	"code.justin.tv/commerce/box-office/internal/resolvers/helpers"
	borpc "code.justin.tv/commerce/box-office/rpc"
	"code.justin.tv/commerce/box-office/token"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
)

type ClipAuthorizationResolver struct {
	Request *borpc.GetClipAccessTokenRequest
	Stats   statsd.Statter
}

func NewClipAuthorizationResolver(ctx context.Context, request *borpc.GetClipAccessTokenRequest, stats statsd.Statter) (*ClipAuthorizationResolver, error) {
	if request == nil || request.ClipSlug == "" {
		return nil, errors.New("clipSlug must not be empty")
	}

	return &ClipAuthorizationResolver{
		Request: request,
		Stats:   stats,
	}, nil
}

func (c *ClipAuthorizationResolver) Resolve(ctx context.Context) *token.Authorization {
	forbidden := make(chan bool, 1)
	go func() {
		forbidden <- c.Forbidden(ctx)
	}()

	reason := make(chan string, 1)
	go func() {
		reason <- c.Reason(ctx)
	}()

	return &token.Authorization{
		Forbidden: <-forbidden,
		Reason:    <-reason,
	}
}

func (c *ClipAuthorizationResolver) Forbidden(ctx context.Context) bool {
	forbidden, _ := helpers.ClipIsForbidden(ctx, c.Request.ClipSlug, c.Request.CountryCode, c.Stats)
	return forbidden
}

func (c *ClipAuthorizationResolver) Reason(ctx context.Context) string {
	_, reason := helpers.ClipIsForbidden(ctx, c.Request.ClipSlug, c.Request.CountryCode, c.Stats)
	return reason
}
