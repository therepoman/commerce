package resolvers

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"
	"code.justin.tv/commerce/box-office/mocks"
	borpc "code.justin.tv/commerce/box-office/rpc"

	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	testClipSlug = "TestClipSlug"
	testClipURI  = "https://test-clip-uri.mp4"
)

var (
	// this is not a const because we need to
	// take its address
	testClipBroadcastID = "testClipBroadcastID"
)

func TestClipAccessTokenResolver(t *testing.T) {
	Convey("TestClipAccessTokenResolver", t, func() {
		mockStatter := new(mocks.Statter)
		mockStatter.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("test constructor", func() {
			Convey("happy case", func() {
				request := &borpc.GetClipAccessTokenRequest{
					ClipSlug: testClipSlug,
				}

				resolver, err := NewClipAccessTokenResolver(context.Background(), request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)
			})

			Convey("when request is nil", func() {
				resolver, err := NewClipAccessTokenResolver(context.Background(), nil, mockStatter)
				So(err, ShouldNotBeNil)
				So(resolver, ShouldBeNil)
			})
		})

		Convey("test validateClip", func() {
			Convey("happy case", func() {
				ctx := test.SetupLoaderContext(context.Background(), loaders.ClipURI, testClipURI, nil)
				ctx = test.SetupLoaderContext(ctx, loaders.ClipBroadcastID, &testClipBroadcastID, nil)

				request := &borpc.GetClipAccessTokenRequest{
					ClipSlug: testClipSlug,
				}

				resolver, err := NewClipAccessTokenResolver(ctx, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				err = resolver.validateClip(ctx)
				So(err, ShouldBeNil)
			})

			Convey("when clip slug is empty", func() {
				ctx := test.SetupLoaderContext(context.Background(), loaders.ClipURI, testClipURI, nil)
				ctx = test.SetupLoaderContext(ctx, loaders.ClipBroadcastID, &testClipBroadcastID, nil)

				request := &borpc.GetClipAccessTokenRequest{
					ClipSlug: "",
				}

				resolver, err := NewClipAccessTokenResolver(ctx, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				err = resolver.validateClip(ctx)
				So(err, ShouldNotBeNil)
			})

			Convey("when clip URI loader errors", func() {
				ctx := test.SetupLoaderContext(context.Background(), loaders.ClipURI, testClipURI, errors.New("loader error"))
				ctx = test.SetupLoaderContext(ctx, loaders.ClipBroadcastID, &testClipBroadcastID, nil)

				request := &borpc.GetClipAccessTokenRequest{
					ClipSlug: testClipSlug,
				}

				resolver, err := NewClipAccessTokenResolver(ctx, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				err = resolver.validateClip(ctx)
				So(err, ShouldNotBeNil)
			})

			Convey("when clip broadcast ID loader errors", func() {
				ctx := test.SetupLoaderContext(context.Background(), loaders.ClipURI, testClipURI, nil)
				ctx = test.SetupLoaderContext(ctx, loaders.ClipBroadcastID, nil, errors.New("loader error"))

				request := &borpc.GetClipAccessTokenRequest{
					ClipSlug: testClipSlug,
				}

				resolver, err := NewClipAccessTokenResolver(ctx, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				err = resolver.validateClip(ctx)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("test resolving fields", func() {
			ctx := test.SetupLoaderContext(context.Background(), loaders.ClipURI, testClipURI, nil)
			ctx = test.SetupLoaderContext(ctx, loaders.ClipBroadcastID, &testClipBroadcastID, nil)

			request := &borpc.GetClipAccessTokenRequest{
				UserId:      "testUserId",
				ClipSlug:    testClipSlug,
				Platform:    "testPlatform",
				PlayerType:  "testPlayerType",
				DeviceId:    "testDeviceId",
				CountryCode: "US",
			}

			resolver, err := NewClipAccessTokenResolver(ctx, request, mockStatter)
			So(err, ShouldBeNil)
			So(resolver, ShouldNotBeNil)

			// Note: we don't test that Authorization resolves
			// because we have a separate test file for it

			Convey("ClipURI resolves", func() {
				clipURI := resolver.ClipURI(ctx)
				So(clipURI, ShouldEqual, testClipURI)
			})

			Convey("DeviceID resolves", func() {
				deviceID := resolver.DeviceID(ctx)
				So(*deviceID, ShouldEqual, request.DeviceId)
			})

			Convey("Expires resolves", func() {
				expectedExpires := time.Now().Add(clipExpirationOffset)

				expires := resolver.Expires(ctx)
				So(expires, ShouldAlmostEqual, expectedExpires.Unix(), 10)
			})

			Convey("UserID resolves", func() {
				userID := resolver.UserID(ctx)
				So(userID, ShouldEqual, request.UserId)
			})

			Convey("Version resolves", func() {
				version := resolver.Version(ctx)
				So(version, ShouldEqual, TokenVersion)
			})
		})
	})
}
