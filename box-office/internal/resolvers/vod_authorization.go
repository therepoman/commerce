package resolvers

import (
	"context"
	"errors"

	"code.justin.tv/commerce/box-office/internal/resolvers/helpers"

	"code.justin.tv/commerce/box-office/token"
)

// VODAuthorizationResolver resolves Authorization objects
type VODAuthorizationResolver struct {
	TokenResolver *VODAccessTokenResolver
}

// NewVODAuthorizationResolver returns a new resolver.
func NewVODAuthorizationResolver(ctx context.Context, tokenResolver *VODAccessTokenResolver) (*VODAuthorizationResolver, error) {
	if tokenResolver == nil {
		return nil, errors.New("tokenResolver must not be nil")
	}
	return &VODAuthorizationResolver{
		TokenResolver: tokenResolver,
	}, nil
}

// Resolve the token. Returns a resolved token
func (v *VODAuthorizationResolver) Resolve(ctx context.Context) *token.Authorization {
	return &token.Authorization{
		Forbidden: v.Forbidden(ctx),
		Reason:    v.Reason(ctx),
	}
}

// Forbidden resolves the Forbidden field
// Default value: false
func (v *VODAuthorizationResolver) Forbidden(ctx context.Context) bool {
	vodID := v.TokenResolver.VODIDString(ctx)

	forbidden, _ := helpers.VODIsForbidden(
		ctx,
		v.TokenResolver.UserIDString(ctx),
		vodID,
		v.TokenResolver.request.CountryCode,
		v.TokenResolver.stats,
	)

	return forbidden
}

// Reason resolves the Reason field
// Default value: ""
func (v *VODAuthorizationResolver) Reason(ctx context.Context) string {
	vodID := v.TokenResolver.VODIDString(ctx)

	_, reason := helpers.VODIsForbidden(
		ctx,
		v.TokenResolver.UserIDString(ctx),
		vodID,
		v.TokenResolver.request.CountryCode,
		v.TokenResolver.stats,
	)

	return reason
}
