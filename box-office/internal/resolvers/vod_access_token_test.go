package resolvers

import (
	"context"
	"errors"
	"net/http"
	"testing"
	"time"

	"code.justin.tv/commerce/box-office/config"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"
	"code.justin.tv/commerce/box-office/mocks"
	borpc "code.justin.tv/commerce/box-office/rpc"
	"code.justin.tv/foundation/twitchclient"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	"code.justin.tv/vod/vodapi/rpc/vodapi"
	"code.justin.tv/web/users-service/models"

	"github.com/golang/protobuf/ptypes/wrappers"
	gerrors "github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	vodID       = "9898765432"
	vodIDInt    = int64(9898765432)
	badVODID    = "notanumber"
	vodIDZeroes = "00009898765432"
)

func TestVODAccessTokenResolver(t *testing.T) {
	Convey("Test VODAccessTokenResolver", t, func() {
		isTrue := true
		isFalse := false
		simpleConfig := &config.Configuration{}
		mockStatter := new(mocks.Statter)
		mockStatter.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("Test constructor", func() {
			Convey("Happy case", func() {
				request := &borpc.GetVODAccessTokenRequest{
					VodId: vodID,
				}
				resolver, err := NewVODAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)
			})

			Convey("nil request", func() {
				resolver, err := NewVODAccessTokenResolver(context.Background(), simpleConfig, nil, mockStatter)
				So(err, ShouldNotBeNil)
				So(resolver, ShouldBeNil)
			})
		})

		Convey("Test validateVOD", func() {
			Convey("Happy case", func() {
				vodapiResponse := &vodapi.Vod{
					OwnerId: "12345",
				}
				context := test.SetupLoaderContext(context.Background(), loaders.VODByID, vodapiResponse, nil)

				request := &borpc.GetVODAccessTokenRequest{
					VodId: vodID,
				}
				resolver, err := NewVODAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				err = resolver.validateVOD(context)
				So(err, ShouldBeNil)
			})

			Convey("vod response 400", func() {
				context := test.SetupLoaderContext(context.Background(), loaders.VODByID, nil, &twitchclient.Error{StatusCode: http.StatusBadRequest})

				request := &borpc.GetVODAccessTokenRequest{
					VodId: vodID,
				}
				resolver, err := NewVODAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				err = resolver.validateVOD(context)
				So(err, ShouldNotBeNil)
				rootErr := gerrors.Cause(err)
				tcErr, ok := rootErr.(*twitchclient.Error)
				So(ok, ShouldBeTrue)
				So(tcErr.StatusCode, ShouldEqual, http.StatusBadRequest)
			})

			Convey("vod response 404", func() {
				context := test.SetupLoaderContext(context.Background(), loaders.VODByID, nil, &twitchclient.Error{StatusCode: http.StatusNotFound})

				request := &borpc.GetVODAccessTokenRequest{
					VodId: vodID,
				}
				resolver, err := NewVODAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				err = resolver.validateVOD(context)
				rootErr := gerrors.Cause(err)
				tcErr, ok := rootErr.(*twitchclient.Error)
				So(ok, ShouldBeTrue)
				So(tcErr.StatusCode, ShouldEqual, http.StatusNotFound)
			})

			Convey("empty VODID", func() {
				request := &borpc.GetVODAccessTokenRequest{}
				resolver, err := NewVODAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				err = resolver.validateVOD(context.Background())
				So(err, ShouldNotBeNil)
				tcErr, ok := err.(*twitchclient.Error)
				So(ok, ShouldBeTrue)
				So(tcErr.StatusCode, ShouldEqual, http.StatusBadRequest)
			})
		})

		Convey("Test canUserViewVOD", func() {
			request := &borpc.GetVODAccessTokenRequest{
				VodId: vodID,
			}
			resolver, err := NewVODAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
			So(err, ShouldBeNil)
			So(resolver, ShouldNotBeNil)

			Convey("nil vod", func() {
				canUserViewVOD := resolver.canUserViewVOD(context.Background(), nil, userID)
				So(canUserViewVOD, ShouldBeFalse)
			})

			Convey("with deleted nil", func() {
				vod := &vodapi.Vod{}
				canUserViewVOD := resolver.canUserViewVOD(context.Background(), vod, userID)
				So(canUserViewVOD, ShouldBeTrue)
			})

			Convey("with deleted true", func() {
				deleted := true
				vod := &vodapi.Vod{
					Deleted: &wrappers.BoolValue{Value: deleted},
				}
				canUserViewVOD := resolver.canUserViewVOD(context.Background(), vod, userID)
				So(canUserViewVOD, ShouldBeFalse)
			})

			Convey("with deleted false", func() {
				deleted := false
				vod := &vodapi.Vod{
					Deleted: &wrappers.BoolValue{Value: deleted},
				}
				canUserViewVOD := resolver.canUserViewVOD(context.Background(), vod, userID)
				So(canUserViewVOD, ShouldBeTrue)
			})

			Convey("invalid viewable", func() {
				vod := &vodapi.Vod{
					Viewable: vodapi.VodViewable_INVALID_VIEWABLE,
				}
				canUserViewVOD := resolver.canUserViewVOD(context.Background(), vod, userID)
				So(canUserViewVOD, ShouldBeTrue)
			})

			Convey("with public viewable", func() {
				vod := &vodapi.Vod{
					Viewable: vodapi.VodViewable_PUBLIC,
				}
				canUserViewVOD := resolver.canUserViewVOD(context.Background(), vod, userID)
				So(canUserViewVOD, ShouldBeTrue)
			})

			Convey("with private viewable", func() {
				vod := &vodapi.Vod{
					Viewable: vodapi.VodViewable_PRIVATE,
				}
				Convey("with no userID", func() {
					canUserViewVOD := resolver.canUserViewVOD(context.Background(), vod, "")
					So(canUserViewVOD, ShouldBeFalse)
				})

				Convey("with userID", func() {
					Convey("user is admin", func() {
						userResponse := &models.Properties{
							ID:       userID,
							Admin:    &isTrue,
							Subadmin: &isFalse,
						}
						context := test.SetupLoaderContext(context.Background(), loaders.UserByID, userResponse, nil)

						canUserViewVOD := resolver.canUserViewVOD(context, vod, userID)
						So(canUserViewVOD, ShouldBeTrue)
					})

					Convey("user is not admin", func() {
						userResponse := &models.Properties{
							ID:       userID,
							Admin:    &isFalse,
							Subadmin: &isFalse,
						}
						context := test.SetupLoaderContext(context.Background(), loaders.UserByID, userResponse, nil)

						canUserViewVOD := resolver.canUserViewVOD(context, vod, userID)
						So(canUserViewVOD, ShouldBeFalse)
					})
				})
			})
		})

		Convey("Test Expires", func() {
			expiryTime := time.Now().Add(vodExpirationOffset)
			request := &borpc.GetVODAccessTokenRequest{
				VodId: vodID,
			}
			resolver, err := NewVODAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
			So(err, ShouldBeNil)
			So(resolver, ShouldNotBeNil)

			resp := resolver.Expires(context.Background())
			So(resp, ShouldAlmostEqual, expiryTime.Unix(), 10)
		})

		Convey("Test Privileged", func() {
			Convey("happy case", func() {
				request := &borpc.GetVODAccessTokenRequest{
					VodId:  vodID,
					UserId: userID,
				}

				vodapiResponse := &vodapi.Vod{
					OwnerId: "12345",
				}
				context := test.SetupLoaderContext(context.Background(), loaders.VODByID, vodapiResponse, nil)

				userResponse := &models.Properties{
					ID:       userID,
					Admin:    &isTrue,
					Subadmin: &isFalse,
				}
				context = test.SetupLoaderContext(context, loaders.UserByID, userResponse, nil)

				productsResp := []*substwirp.Product{
					{
						BitrateAccess: []string{"archives"},
					},
				}
				context = test.SetupLoaderContext(context, loaders.ChannelProducts, productsResp, nil)

				resolver, err := NewVODAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				isPrivileged := resolver.Privileged(context)
				So(isPrivileged, ShouldEqual, privileged)
			})

			Convey("vodapi fails", func() {
				request := &borpc.GetVODAccessTokenRequest{
					VodId:  vodID,
					UserId: userID,
				}

				context := test.SetupLoaderContext(context.Background(), loaders.VODByID, nil, errors.New("test"))

				resolver, err := NewVODAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				isPrivileged := resolver.Privileged(context)
				So(isPrivileged, ShouldEqual, false)
			})
		})

		Convey("Test UserID", func() {
			Convey("happy case", func() {
				request := &borpc.GetVODAccessTokenRequest{
					VodId:  vodID,
					UserId: userID,
				}
				resolver, err := NewVODAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.UserID(context.Background())
				So(*resp, ShouldEqual, userIDInt)
			})

			Convey("default response", func() {
				request := &borpc.GetVODAccessTokenRequest{
					VodId: vodID,
				}
				resolver, err := NewVODAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.UserID(context.Background())
				So(resp, ShouldBeNil)
			})
		})

		Convey("Test Version", func() {
			request := &borpc.GetVODAccessTokenRequest{
				VodId: vodID,
			}
			resolver, err := NewVODAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
			So(err, ShouldBeNil)
			So(resolver, ShouldNotBeNil)

			resp := resolver.Version(context.Background())
			So(resp, ShouldEqual, version)
		})

		Convey("Test VOD ID", func() {
			Convey("happy case", func() {
				request := &borpc.GetVODAccessTokenRequest{
					VodId: vodID,
				}

				vodapiResponse := &vodapi.Vod{
					Id: vodID,
				}
				context := test.SetupLoaderContext(context.Background(), loaders.VODByID, vodapiResponse, nil)

				resolver, err := NewVODAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.VODID(context)
				So(*resp, ShouldEqual, vodIDInt)
			})

			Convey("default response", func() {
				request := &borpc.GetVODAccessTokenRequest{
					VodId: badVODID,
				}
				resolver, err := NewVODAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.VODID(context.Background())
				So(resp, ShouldBeNil)
			})

			Convey("Trimming of leading zeroes test case", func() {
				request := &borpc.GetVODAccessTokenRequest{
					VodId: vodIDZeroes,
				}
				vodapiResponse := &vodapi.Vod{
					Id: vodID,
				}
				ctx := test.SetupLoaderContext(context.Background(), loaders.VODByID, vodapiResponse, nil)

				resolver, err := NewVODAccessTokenResolver(ctx, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.VODID(ctx)
				So(*resp, ShouldEqual, vodIDInt)
			})

		})
	})
}
