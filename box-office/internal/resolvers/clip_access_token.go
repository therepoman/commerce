package resolvers

import (
	"context"
	"net/http"
	"time"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"

	"code.justin.tv/commerce/box-office/internal/loaders"
	borpc "code.justin.tv/commerce/box-office/rpc"
	"code.justin.tv/commerce/box-office/token"
	"code.justin.tv/foundation/twitchclient"
)

const (
	clipExpirationOffset = 20 * time.Hour
)

type ClipAccessTokenResolver struct {
	request *borpc.GetClipAccessTokenRequest
	stats   statsd.Statter
}

var (
	clipBlocklist = map[string]string{
		"UnusualAltruisticFishBrokeBack":    "https://production.assets.clips.twitchcdn.net/vod-323455275-offset-198.mp4",
		"FlaccidKindFloofWoofer":            "https://production.assets.clips.twitchcdn.net/vod-773293743-offset-33522.mp4",
		"LaconicSilkyAardvarkLeeroyJenkins": "https://production.assets.clips.twitchcdn.net/40566085134-offset-132370.mp4",
	}
)

func NewClipAccessTokenResolver(ctx context.Context, request *borpc.GetClipAccessTokenRequest, stats statsd.Statter) (*ClipAccessTokenResolver, error) {
	if request == nil {
		return nil, errors.New("request must not be nil")
	}

	return &ClipAccessTokenResolver{
		request: request,
		stats:   stats,
	}, nil
}

func (c *ClipAccessTokenResolver) Resolve(ctx context.Context) (*token.ClipAccessToken, error) {
	err := c.validateClip(ctx)
	if err != nil {
		return nil, err
	}

	authorization := make(chan token.Authorization, 1)
	go func() {
		authorization <- c.Authorization(ctx)
	}()

	clipURI := make(chan string, 1)
	go func() {
		clipURI <- c.ClipURI(ctx)
	}()

	deviceID := make(chan *string, 1)
	go func() {
		deviceID <- c.DeviceID(ctx)
	}()

	expires := make(chan int64, 1)
	go func() {
		expires <- c.Expires(ctx)
	}()

	userID := make(chan string, 1)
	go func() {
		userID <- c.UserID(ctx)
	}()

	version := make(chan int, 1)
	go func() {
		version <- c.Version(ctx)
	}()

	return &token.ClipAccessToken{
		Authorization: <-authorization,
		ClipURI:       <-clipURI,
		DeviceID:      <-deviceID,
		Expires:       <-expires,
		UserID:        <-userID,
		Version:       <-version,
	}, nil
}

func (c *ClipAccessTokenResolver) Authorization(ctx context.Context) token.Authorization {
	resolver, err := NewClipAuthorizationResolver(ctx, c.request, c.stats)
	if err != nil {
		log.Errorf("Error resolve clip authorization. Using default value. Error: %+v", err)
		return token.Authorization{}
	}

	return *resolver.Resolve(ctx)
}

func (c *ClipAccessTokenResolver) ClipURI(ctx context.Context) string {
	// this should never error as we check in validateClip
	// that a clip uri exists before we try and resolve this field
	uri, _ := loaders.LoadClipURI(ctx, c.request.ClipSlug)
	return uri
}

func (c *ClipAccessTokenResolver) DeviceID(ctx context.Context) *string {
	if c.request.DeviceId == "" {
		return nil
	}

	return &c.request.DeviceId
}

func (c *ClipAccessTokenResolver) Expires(ctx context.Context) int64 {
	expiration := time.Now().Add(clipExpirationOffset)
	return expiration.Unix()
}

func (c *ClipAccessTokenResolver) UserID(ctx context.Context) string {
	return c.request.UserId
}

func (c *ClipAccessTokenResolver) Version(ctx context.Context) int {
	return TokenVersion
}

func (c *ClipAccessTokenResolver) validateClip(ctx context.Context) error {
	if c.request.ClipSlug == "" {
		return &twitchclient.Error{
			StatusCode: http.StatusBadRequest,
			Message:    "clip slug must not be empty",
		}
	}

	clipURI, err := loaders.LoadClipURI(ctx, c.request.ClipSlug)
	if err != nil || clipURI == "" {
		return errors.Wrap(err, "could not find clip uri")
	}

	broadcastID, err := loaders.LoadClipBroadcastID(ctx, c.request.ClipSlug)
	if err != nil || broadcastID == "" {
		return errors.Wrap(err, "could not find broadcast ID for clip")
	}

	return nil
}
