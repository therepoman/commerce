package resolvers

import (
	"context"
	"errors"

	"code.justin.tv/commerce/box-office/internal/resolvers/helpers"
	log "github.com/sirupsen/logrus"

	"code.justin.tv/commerce/box-office/token"
)

// VideoPrivateResolver resolves VideoPrivate objects
type VideoPrivateResolver struct {
	TokenResolver *VideoAccessTokenResolver
}

// NewVideoPrivateResolver loads a video access token and returns a new resolver.
func NewVideoPrivateResolver(ctx context.Context, tokenResolver *VideoAccessTokenResolver) (*VideoPrivateResolver, error) {
	if tokenResolver == nil {
		return nil, errors.New("tokenResolver must not be nil")
	}

	return &VideoPrivateResolver{
		TokenResolver: tokenResolver,
	}, nil
}

// Resolve the token. Returns a resolved token
func (v *VideoPrivateResolver) Resolve(ctx context.Context) *token.Private {
	return &token.Private{
		AllowedToView: v.AllowedToView(ctx),
	}
}

// AllowedToView resolves the AllowedToView field
// Default value: true
func (v *VideoPrivateResolver) AllowedToView(ctx context.Context) bool {
	channelID, err := v.TokenResolver.ChannelIDString(ctx)
	if err != nil || channelID == "" {
		log.Errorf("Error getting channel ID. Using default value. Error: %+v", err)
		return true
	}

	// if the video isn't private then everyone is alllowed to view it
	if !helpers.IsVideoPrivate(ctx, channelID) {
		return true
	}

	userID := v.TokenResolver.UserIDString(ctx)

	// else if the user has private permissions then they can view it
	return helpers.UserHasPrivatePermissions(ctx, channelID, userID)

}
