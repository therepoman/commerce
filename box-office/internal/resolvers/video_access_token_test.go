package resolvers

import (
	"context"
	"errors"
	"testing"
	"time"

	dads "code.justin.tv/ads/dads/proto"
	voyager "code.justin.tv/amzn/TwitchVoyagerTwirp"
	"code.justin.tv/cb/hallpass/view"
	"code.justin.tv/commerce/box-office/config"
	"code.justin.tv/commerce/box-office/internal/geoblock"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"
	"code.justin.tv/commerce/box-office/mocks"
	borpc "code.justin.tv/commerce/box-office/rpc"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	nitro "code.justin.tv/samus/nitro/rpc"
	"code.justin.tv/web/users-service/client"
	"code.justin.tv/web/users-service/models"
	gerrors "github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	adblock      = true
	channelIDInt = int64(1234)
	partner      = false
	playerType   = "playertype"
	platform     = "platform"
	privileged   = true
	userID       = "12345"
	userIDInt    = int64(12345)
	userIP       = "52.95.255.100"
	version      = 2
)

var (
	channelID   = "1234"
	channelName = "channelname"
	deviceID    = "deviceid"
)

func TestVideoAccessTokenResolver(t *testing.T) {
	Convey("Test VideoAccessTokenResolver", t, func() {
		simpleConfig := &config.Configuration{}
		mockStatter := new(mocks.Statter)
		mockStatter.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("Test constructor", func() {
			Convey("Happy case", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
				}
				resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)
			})

			Convey("nil request", func() {
				resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, nil, mockStatter)
				So(err, ShouldNotBeNil)
				So(resolver, ShouldBeNil)
			})

			Convey("nil config", func() {
				request := &borpc.GetVideoAccessTokenRequest{}
				resolver, err := NewVideoAccessTokenResolver(context.Background(), nil, request, mockStatter)
				So(err, ShouldNotBeNil)
				So(resolver, ShouldBeNil)
			})
		})

		Convey("Test Adblock", func() {
			request := &borpc.GetVideoAccessTokenRequest{
				ChannelName: channelName,
			}

			resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
			So(err, ShouldBeNil)
			So(resolver, ShouldNotBeNil)

			resp := resolver.Adblock(context.Background())
			So(resp, ShouldEqual, defaultHasAdblock)
		})

		Convey("Test Channel", func() {
			Convey("when in the request", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
				}
				resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp, err := resolver.Channel(context.Background())
				So(err, ShouldBeNil)
				So(resp, ShouldEqual, channelName)

			})

			Convey("When not in request", func() {
				Convey("valid login response", func() {
					request := &borpc.GetVideoAccessTokenRequest{
						ChannelId: channelID,
					}

					userResponse := &models.Properties{
						Login: &channelName,
					}

					context := test.SetupLoaderContext(context.Background(), loaders.UserByID, userResponse, nil)

					resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					resp, err := resolver.Channel(context)
					So(err, ShouldBeNil)
					So(resp, ShouldEqual, channelName)
				})

				Convey("user not found", func() {
					request := &borpc.GetVideoAccessTokenRequest{
						ChannelId: channelID,
					}

					context := test.SetupLoaderContext(context.Background(), loaders.UserByID, nil, &client.UserNotFoundError{})

					resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					_, err = resolver.Channel(context)
					So(err, ShouldNotBeNil)
					rootErr := gerrors.Cause(err)
					userErr, ok := rootErr.(*client.UserNotFoundError)
					So(ok, ShouldBeTrue)
					So(client.IsUserNotFound(userErr), ShouldBeTrue)

				})

				Convey("unknown error", func() {
					request := &borpc.GetVideoAccessTokenRequest{
						ChannelId: channelID,
					}

					context := test.SetupLoaderContext(context.Background(), loaders.UserByID, nil, errors.New("test"))

					resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					_, err = resolver.Channel(context)
					So(err, ShouldNotBeNil)
					rootErr := gerrors.Cause(err)
					userErr, ok := rootErr.(*client.UserNotFoundError)
					So(ok, ShouldBeTrue)
					So(client.IsUserNotFound(userErr), ShouldBeTrue)
				})

				Convey("nil response", func() {
					request := &borpc.GetVideoAccessTokenRequest{
						ChannelId: channelID,
					}

					context := test.SetupLoaderContext(context.Background(), loaders.UserByID, nil, nil)

					resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					_, err = resolver.Channel(context)
					So(err, ShouldNotBeNil)
					rootErr := gerrors.Cause(err)
					userErr, ok := rootErr.(*client.UserNotFoundError)
					So(ok, ShouldBeTrue)
					So(client.IsUserNotFound(userErr), ShouldBeTrue)
				})

				Convey("nil login", func() {
					request := &borpc.GetVideoAccessTokenRequest{
						ChannelId: channelID,
					}

					userResponse := &models.Properties{
						Login: nil,
					}

					context := test.SetupLoaderContext(context.Background(), loaders.UserByID, userResponse, nil)

					resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					_, err = resolver.Channel(context)
					So(err, ShouldNotBeNil)
					rootErr := gerrors.Cause(err)
					userErr, ok := rootErr.(*client.UserNotFoundError)
					So(ok, ShouldBeTrue)
					So(client.IsUserNotFound(userErr), ShouldBeTrue)
				})
			})
		})

		Convey("Test ChannelID", func() {
			Convey("when channelID in the request", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelId: channelID,
				}
				resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp, err := resolver.ChannelID(context.Background())
				So(err, ShouldBeNil)
				So(*resp, ShouldEqual, channelIDInt)
			})

			Convey("when channelID not in the request", func() {
				Convey("Happy case", func() {
					request := &borpc.GetVideoAccessTokenRequest{
						ChannelName: channelName,
					}

					userResponse := &models.Properties{
						ID: channelID,
					}

					context := test.SetupLoaderContext(context.Background(), loaders.UserByLogin, userResponse, nil)

					resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					resp, err := resolver.ChannelID(context)
					So(err, ShouldBeNil)
					So(*resp, ShouldEqual, channelIDInt)
				})

				Convey("non-numeric channel id", func() {
					request := &borpc.GetVideoAccessTokenRequest{
						ChannelName: channelName,
					}

					userResponse := &models.Properties{
						ID: "notanumber",
					}

					context := test.SetupLoaderContext(context.Background(), loaders.UserByLogin, userResponse, nil)

					resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					_, err = resolver.ChannelID(context)
					So(err, ShouldNotBeNil)
				})

				Convey("user not found error", func() {
					request := &borpc.GetVideoAccessTokenRequest{
						ChannelName: channelName,
					}

					context := test.SetupLoaderContext(context.Background(), loaders.UserByLogin, nil, &client.UserNotFoundError{})

					resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					_, err = resolver.ChannelID(context)
					So(err, ShouldNotBeNil)
					rootErr := gerrors.Cause(err)
					userErr, ok := rootErr.(*client.UserNotFoundError)
					So(ok, ShouldBeTrue)
					So(client.IsUserNotFound(userErr), ShouldBeTrue)
				})

				Convey("error response from loader", func() {
					request := &borpc.GetVideoAccessTokenRequest{
						ChannelName: channelName,
					}

					context := test.SetupLoaderContext(context.Background(), loaders.UserByLogin, nil, errors.New("test"))

					resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					_, err = resolver.ChannelID(context)
					So(err, ShouldNotBeNil)
					rootErr := gerrors.Cause(err)
					userErr, ok := rootErr.(*client.UserNotFoundError)
					So(ok, ShouldBeTrue)
					So(client.IsUserNotFound(userErr), ShouldBeTrue)
				})

				Convey("nil response from loader", func() {
					request := &borpc.GetVideoAccessTokenRequest{
						ChannelName: channelName,
					}

					context := test.SetupLoaderContext(context.Background(), loaders.UserByLogin, nil, nil)

					resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					_, err = resolver.ChannelID(context)
					So(err, ShouldNotBeNil)
					rootErr := gerrors.Cause(err)
					userErr, ok := rootErr.(*client.UserNotFoundError)
					So(ok, ShouldBeTrue)
					So(client.IsUserNotFound(userErr), ShouldBeTrue)
				})
			})
		})

		Convey("Test ChannelIsGeoBlocked", func() {
			Convey("with no geoblocks", func() {
				geoConfig := &config.Configuration{}

				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
					CountryCode: "US",
				}
				resolver, err := NewVideoAccessTokenResolver(context.Background(), geoConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				isGeoblocked := resolver.ChannelIsGeoBlocked(context.Background())
				So(isGeoblocked, ShouldBeFalse)
			})

			Convey("with geoblocks", func() {
				firstTime := time.Date(2010, 1, 1, 1, 1, 1, 1, time.UTC)
				thirdTime := time.Date(2030, 1, 1, 1, 1, 1, 1, time.UTC)
				blocklist := map[string]bool{
					"CN": true,
				}
				blocklistZipCodes := map[string]bool{
					"10001": true,
				}
				allowlist := map[string]bool{
					"US": true,
				}

				geoConfig := &config.Configuration{
					GeoblockedChannels: map[string]geoblock.Geoblock{
						channelName: {
							TimeWindows: []geoblock.TimeWindow{
								{
									StartTime: firstTime,
									EndTime:   thirdTime,
								},
							},
							AllowlistedCountries: allowlist,
							BlocklistedCountries: blocklist,
							BlocklistedZipCodes:  blocklistZipCodes,
						},
					},
				}

				Convey("Test with allowlisted", func() {
					Convey("allowlisted country", func() {
						request := &borpc.GetVideoAccessTokenRequest{
							ChannelName: channelName,
							CountryCode: "US",
						}
						resolver, err := NewVideoAccessTokenResolver(context.Background(), geoConfig, request, mockStatter)
						So(err, ShouldBeNil)
						So(resolver, ShouldNotBeNil)

						isGeoblocked := resolver.ChannelIsGeoBlocked(context.Background())
						So(isGeoblocked, ShouldBeFalse)
					})

					Convey("allowlisted country US but blocklisted postal code", func() {
						request := &borpc.GetVideoAccessTokenRequest{
							ChannelName: channelName,
							CountryCode: "US",
							PostalCode:  "10001",
						}
						resolver, err := NewVideoAccessTokenResolver(context.Background(), geoConfig, request, mockStatter)
						So(err, ShouldBeNil)
						So(resolver, ShouldNotBeNil)

						isGeoblocked := resolver.ChannelIsGeoBlocked(context.Background())
						So(isGeoblocked, ShouldBeTrue)
					})
				})

				Convey("Test with blocklisted", func() {
					Convey("blocklisted country", func() {
						request := &borpc.GetVideoAccessTokenRequest{
							ChannelName: channelName,
							CountryCode: "CN",
						}
						resolver, err := NewVideoAccessTokenResolver(context.Background(), geoConfig, request, mockStatter)
						So(err, ShouldBeNil)
						So(resolver, ShouldNotBeNil)

						isGeoblocked := resolver.ChannelIsGeoBlocked(context.Background())
						So(isGeoblocked, ShouldBeTrue)
					})

					Convey("blocklisted postal code", func() {
						request := &borpc.GetVideoAccessTokenRequest{
							ChannelName: channelName,
							CountryCode: "US",
							PostalCode:  "10001",
						}
						resolver, err := NewVideoAccessTokenResolver(context.Background(), geoConfig, request, mockStatter)
						So(err, ShouldBeNil)
						So(resolver, ShouldNotBeNil)

						isGeoblocked := resolver.ChannelIsGeoBlocked(context.Background())
						So(isGeoblocked, ShouldBeTrue)
					})
				})

				Convey("Test with not allowlisted", func() {
					Convey("not allowlisted country", func() {
						request := &borpc.GetVideoAccessTokenRequest{
							ChannelName: channelName,
							CountryCode: "DE",
						}
						resolver, err := NewVideoAccessTokenResolver(context.Background(), geoConfig, request, mockStatter)
						So(err, ShouldBeNil)
						So(resolver, ShouldNotBeNil)

						isGeoblocked := resolver.ChannelIsGeoBlocked(context.Background())
						So(isGeoblocked, ShouldBeTrue)
					})
				})

				Convey("Test with channel without geoblock", func() {
					request := &borpc.GetVideoAccessTokenRequest{
						ChannelName: "otherchannel",
						CountryCode: "DE",
						PostalCode:  "10001",
					}
					resolver, err := NewVideoAccessTokenResolver(context.Background(), geoConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					isGeoblocked := resolver.ChannelIsGeoBlocked(context.Background())
					So(isGeoblocked, ShouldBeFalse)
				})

				Convey("Test with block anonymous ip's", func() {
					blockAnonIPGeoConfig := &config.Configuration{
						GeoblockedChannels: map[string]geoblock.Geoblock{
							channelName: {
								TimeWindows: []geoblock.TimeWindow{
									{
										StartTime: firstTime,
										EndTime:   thirdTime,
									},
								},
								BlockAnonymousIPs: true,
							},
						},
					}
					request := &borpc.GetVideoAccessTokenRequest{
						ChannelName:   channelName,
						IpIsAnonymous: true,
					}

					resolver, err := NewVideoAccessTokenResolver(context.Background(), blockAnonIPGeoConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					isGeoblocked := resolver.ChannelIsGeoBlocked(context.Background())
					So(isGeoblocked, ShouldBeTrue)

					Convey("Test with block anonymous ip's but from allowlisted IP address", func() {
						blockAnonIPGeoConfig.AnonymousIPAllowlist = []string{
							"192.168.0.0/24",
						}

						request := &borpc.GetVideoAccessTokenRequest{
							ChannelName:   channelName,
							UserIp:        "192.168.0.48",
							IpIsAnonymous: true,
						}

						resolver, err := NewVideoAccessTokenResolver(context.Background(), blockAnonIPGeoConfig, request, mockStatter)
						So(err, ShouldBeNil)
						So(resolver, ShouldNotBeNil)

						isGeoblocked := resolver.ChannelIsGeoBlocked(context.Background())
						So(isGeoblocked, ShouldBeFalse)

						Convey("Test with block anonymous ip's from IP address outside of allowlisted networks", func() {
							blockAnonIPGeoConfig.AnonymousIPAllowlist = []string{
								"192.168.0.0/24",
							}

							request := &borpc.GetVideoAccessTokenRequest{
								ChannelName:   channelName,
								UserIp:        "192.168.99.48",
								IpIsAnonymous: true,
							}

							resolver, err := NewVideoAccessTokenResolver(context.Background(), blockAnonIPGeoConfig, request, mockStatter)
							So(err, ShouldBeNil)
							So(resolver, ShouldNotBeNil)

							isGeoblocked := resolver.ChannelIsGeoBlocked(context.Background())
							So(isGeoblocked, ShouldBeTrue)
						})
					})

					Convey("Test with block anonymous ip's with invalid config", func() {
						blockAnonIPGeoConfig.AnonymousIPAllowlist = []string{
							"this is an invalid CIDR",
						}

						request := &borpc.GetVideoAccessTokenRequest{
							ChannelName:   channelName,
							UserIp:        "192.168.0.48",
							IpIsAnonymous: true,
						}

						resolver, err := NewVideoAccessTokenResolver(context.Background(), blockAnonIPGeoConfig, request, mockStatter)
						So(err, ShouldBeNil)
						So(resolver, ShouldNotBeNil)

						isGeoblocked := resolver.ChannelIsGeoBlocked(context.Background())
						So(isGeoblocked, ShouldBeTrue)
					})
				})
			})
		})

		Convey("Test GeoblockReason", func() {
			Convey("with no geoblocks", func() {
				geoConfig := &config.Configuration{}

				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName:   channelName,
					CountryCode:   "US",
					IpIsAnonymous: true,
				}
				resolver, err := NewVideoAccessTokenResolver(context.Background(), geoConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				geoblockReason := resolver.GeoblockReason(context.Background())
				So(geoblockReason, ShouldEqual, "")
			})

			Convey("with geoblocks", func() {
				firstTime := time.Date(2010, 1, 1, 1, 1, 1, 1, time.UTC)
				thirdTime := time.Date(2030, 1, 1, 1, 1, 1, 1, time.UTC)

				Convey("with blocked region", func() {
					blocklist := map[string]bool{
						"CN": true,
					}
					allowlist := map[string]bool{
						"US": true,
					}
					blocklistZipCodes := map[string]bool{
						"10001": true,
					}

					geoConfig := &config.Configuration{
						GeoblockedChannels: map[string]geoblock.Geoblock{
							channelName: {
								TimeWindows: []geoblock.TimeWindow{
									{
										StartTime: firstTime,
										EndTime:   thirdTime,
									},
								},
								AllowlistedCountries: allowlist,
								BlocklistedCountries: blocklist,
								BlocklistedZipCodes:  blocklistZipCodes,
							},
						},
					}

					request := &borpc.GetVideoAccessTokenRequest{
						ChannelName: channelName,
						CountryCode: "CN",
						PostalCode:  "10001",
					}
					resolver, err := NewVideoAccessTokenResolver(context.Background(), geoConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					isGeoblocked := resolver.ChannelIsGeoBlocked(context.Background())
					So(isGeoblocked, ShouldBeTrue)
					geoblockReason := resolver.GeoblockReason(context.Background())
					So(geoblockReason, ShouldEqual, geoblock.ContentRestricted)
				})

				Convey("with blocked anonymous ip's", func() {
					geoConfig := &config.Configuration{
						GeoblockedChannels: map[string]geoblock.Geoblock{
							channelName: {
								TimeWindows: []geoblock.TimeWindow{
									{
										StartTime: firstTime,
										EndTime:   thirdTime,
									},
								},
								BlockAnonymousIPs: true,
							},
						},
					}

					Convey("with anonymous ip request", func() {
						request := &borpc.GetVideoAccessTokenRequest{
							ChannelName:   channelName,
							IpIsAnonymous: true,
						}

						resolver, err := NewVideoAccessTokenResolver(context.Background(), geoConfig, request, mockStatter)
						So(err, ShouldBeNil)
						So(resolver, ShouldNotBeNil)

						isGeoblocked := resolver.ChannelIsGeoBlocked(context.Background())
						So(isGeoblocked, ShouldBeTrue)
						geoblockReason := resolver.GeoblockReason(context.Background())
						So(geoblockReason, ShouldEqual, geoblock.VPNBlocking)
					})

					Convey("with normal request", func() {
						request := &borpc.GetVideoAccessTokenRequest{
							ChannelName:   channelName,
							IpIsAnonymous: false,
						}

						resolver, err := NewVideoAccessTokenResolver(context.Background(), geoConfig, request, mockStatter)
						So(err, ShouldBeNil)
						So(resolver, ShouldNotBeNil)

						isGeoblocked := resolver.ChannelIsGeoBlocked(context.Background())
						So(isGeoblocked, ShouldBeFalse)
						geoblockReason := resolver.GeoblockReason(context.Background())
						So(geoblockReason, ShouldEqual, "")
					})
				})
			})
		})

		Convey("Test BlackoutEnabled", func() {
			Convey("with no geoblocks", func() {
				geoConfig := &config.Configuration{}

				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
					CountryCode: "US",
				}
				resolver, err := NewVideoAccessTokenResolver(context.Background(), geoConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				isBlocked := resolver.BlackoutEnabled(context.Background())
				So(isBlocked, ShouldBeFalse)
			})

			Convey("with geoblocks", func() {
				firstTime := time.Date(2010, 1, 1, 1, 1, 1, 1, time.UTC)
				thirdTime := time.Date(2030, 1, 1, 1, 1, 1, 1, time.UTC)
				blocklistZipCodes := map[string]bool{
					"10001": true,
				}

				geoConfig := &config.Configuration{
					GeoblockedChannels: map[string]geoblock.Geoblock{
						channelName: {
							TimeWindows: []geoblock.TimeWindow{
								{
									StartTime: firstTime,
									EndTime:   thirdTime,
								},
							},
							BlocklistedZipCodes: blocklistZipCodes,
						},
					},
				}

				Convey("Test with blocklisted", func() {
					Convey("blocklisted postal code", func() {
						request := &borpc.GetVideoAccessTokenRequest{
							ChannelName: channelName,
							CountryCode: "US",
							PostalCode:  "10001",
						}
						resolver, err := NewVideoAccessTokenResolver(context.Background(), geoConfig, request, mockStatter)
						So(err, ShouldBeNil)
						So(resolver, ShouldNotBeNil)

						isBlocked := resolver.BlackoutEnabled(context.Background())
						So(isBlocked, ShouldBeTrue)
					})
				})

				Convey("Test with channel without geoblock", func() {
					request := &borpc.GetVideoAccessTokenRequest{
						ChannelName: "otherchannel",
						CountryCode: "DE",
						PostalCode:  "10001",
					}
					resolver, err := NewVideoAccessTokenResolver(context.Background(), geoConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					isBlocked := resolver.BlackoutEnabled(context.Background())
					So(isBlocked, ShouldBeFalse)
				})
			})

			Convey("with expired time window", func() {
				firstTime := time.Date(2010, 1, 1, 1, 1, 1, 1, time.UTC)
				thirdTime := time.Date(2011, 1, 1, 1, 1, 1, 1, time.UTC)
				blocklistZipCodes := map[string]bool{
					"10001": true,
				}

				geoConfig := &config.Configuration{
					GeoblockedChannels: map[string]geoblock.Geoblock{
						channelName: {
							TimeWindows: []geoblock.TimeWindow{
								{
									StartTime: firstTime,
									EndTime:   thirdTime,
								},
							},
							BlocklistedZipCodes: blocklistZipCodes,
						},
					},
				}

				Convey("Test with blocklisted", func() {
					Convey("blocklisted postal code", func() {
						request := &borpc.GetVideoAccessTokenRequest{
							ChannelName: channelName,
							CountryCode: "US",
							PostalCode:  "10001",
						}
						resolver, err := NewVideoAccessTokenResolver(context.Background(), geoConfig, request, mockStatter)
						So(err, ShouldBeNil)
						So(resolver, ShouldNotBeNil)

						isBlocked := resolver.BlackoutEnabled(context.Background())
						So(isBlocked, ShouldBeFalse)
					})
				})
			})
		})

		Convey("Test DeviceID", func() {
			Convey("with device ID in request", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
					DeviceId:    deviceID,
				}
				resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.DeviceID(context.Background())
				So(*resp, ShouldEqual, deviceID)
			})

			Convey("without device ID in request", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
				}
				resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.DeviceID(context.Background())
				So(resp, ShouldEqual, nil)
			})
		})

		Convey("Test Expires", func() {
			expiryTime := time.Now().Add(videoExpirationOffset)
			request := &borpc.GetVideoAccessTokenRequest{
				ChannelName: channelName,
			}
			resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
			So(err, ShouldBeNil)
			So(resolver, ShouldNotBeNil)

			resp := resolver.Expires(context.Background())
			So(resp, ShouldAlmostEqual, expiryTime.Unix(), 10)
		})

		Convey("Test Game", func() {
			request := &borpc.GetVideoAccessTokenRequest{
				ChannelName: channelName,
			}
			resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
			So(err, ShouldBeNil)
			So(resolver, ShouldNotBeNil)

			resp := resolver.Game(context.Background())
			So(resp, ShouldEqual, defaultGame)
		})

		Convey("Test Mature", func() {
			request := &borpc.GetVideoAccessTokenRequest{
				ChannelName: channelName,
			}
			resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
			So(err, ShouldBeNil)
			So(resolver, ShouldNotBeNil)

			resp := resolver.Mature(context.Background())
			So(resp, ShouldBeFalse)
		})

		Convey("Test Partner", func() {
			request := &borpc.GetVideoAccessTokenRequest{
				ChannelName: channelName,
			}
			resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
			So(err, ShouldBeNil)
			So(resolver, ShouldNotBeNil)

			resp := resolver.Partner(context.Background())
			So(resp, ShouldEqual, partner)
		})

		Convey("Test Player Type", func() {
			Convey("happy case", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
					PlayerType:  playerType,
				}
				resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.PlayerType(context.Background())
				So(*resp, ShouldEqual, playerType)
			})

			Convey("default response", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
				}
				resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				playerType := resolver.PlayerType(context.Background())
				So(playerType, ShouldBeNil)
			})
		})

		Convey("Test Platform", func() {
			Convey("happy case", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
					Platform:    platform,
				}
				resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.Platform(context.Background())
				So(*resp, ShouldEqual, platform)
			})

			Convey("default response", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
				}
				resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.Platform(context.Background())
				So(resp, ShouldBeNil)
			})
		})

		Convey("Test Privileged", func() {
			isTrue := true
			isFalse := false
			Convey("happy case", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
					ChannelId:   channelID,
					UserId:      userID,
				}

				userResponse := &models.Properties{
					ID:       userID,
					Admin:    &isTrue,
					Subadmin: &isFalse,
				}
				context := test.SetupLoaderContext(context.Background(), loaders.UserByID, userResponse, nil)

				productsResp := []*substwirp.Product{
					{
						BitrateAccess: []string{"archives"},
					},
				}
				context = test.SetupLoaderContext(context, loaders.ChannelProducts, productsResp, nil)

				resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.Privileged(context)
				So(resp, ShouldEqual, privileged)
			})

			Convey("missing channel ID", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
					UserId:      userID,
				}

				permsResp := &view.GetIsEditorResponse{
					IsEditor: privileged,
				}
				context := test.SetupLoaderContext(context.Background(), loaders.Permissions, permsResp, nil)

				userResponse := &models.Properties{
					ID: "",
				}
				context = test.SetupLoaderContext(context, loaders.UserByLogin, userResponse, nil)

				resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.Privileged(context)
				So(resp, ShouldEqual, false)
			})
		})

		Convey("Test ServerAds", func() {
			Convey("with all required parameters available and server_ads true", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					UserId:      userID,
					ChannelId:   channelID,
					ChannelName: channelName,
					DeviceId:    deviceID,
					CountryCode: "US",
					Platform:    platform,
					PlayerType:  playerType,
				}

				adsResp := &dads.ShowServerAdsResp{
					Adblock:   true,
					ServerAds: true,
				}

				premiumResp := &nitro.GetPremiumStatusesResponse{
					HasPrime: true,
					HasTurbo: true,
				}

				productsResp := []*substwirp.Product{
					{
						HideAds: false,
					},
				}

				userResponse := &models.Properties{
					ID: channelID,
				}

				context := test.SetupLoaderContext(context.Background(), loaders.ServerAds, adsResp, nil)
				context = test.SetupLoaderContext(context, loaders.Premium, premiumResp, nil)
				context = test.SetupLoaderContext(context, loaders.UserByLogin, userResponse, nil)
				context = test.SetupLoaderContext(context, loaders.ChannelProducts, productsResp, nil)

				resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.ServerAds(context)
				So(resp, ShouldBeTrue)
			})

			Convey("with all required parameters available and server_ads false", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					UserId:      userID,
					ChannelId:   channelID,
					ChannelName: channelName,
					DeviceId:    deviceID,
					CountryCode: "US",
					Platform:    platform,
					PlayerType:  playerType,
				}

				adsResp := &dads.ShowServerAdsResp{
					Adblock:   true,
					ServerAds: false,
				}

				premiumResp := &nitro.GetPremiumStatusesResponse{
					HasPrime: true,
					HasTurbo: true,
				}

				productsResp := []*substwirp.Product{
					{
						HideAds: false,
					},
				}

				userResponse := &models.Properties{
					ID: channelID,
				}

				context := test.SetupLoaderContext(context.Background(), loaders.ServerAds, adsResp, nil)
				context = test.SetupLoaderContext(context, loaders.Premium, premiumResp, nil)
				context = test.SetupLoaderContext(context, loaders.UserByLogin, userResponse, nil)
				context = test.SetupLoaderContext(context, loaders.ChannelProducts, productsResp, nil)

				resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.ServerAds(context)
				So(resp, ShouldBeFalse)
			})

			Convey("with missing channel id", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					UserId:      userID,
					ChannelName: channelName,
					DeviceId:    deviceID,
					CountryCode: "US",
					Platform:    platform,
					PlayerType:  playerType,
				}

				resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.ServerAds(context.Background())
				So(resp, ShouldBeFalse)
			})

			Convey("with missing device id", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					UserId:      userID,
					ChannelId:   channelID,
					ChannelName: channelName,
					CountryCode: "US",
					Platform:    platform,
					PlayerType:  playerType,
				}

				adsResp := &dads.ShowServerAdsResp{
					Adblock:   true,
					ServerAds: true,
				}

				premiumResp := &nitro.GetPremiumStatusesResponse{
					HasPrime: true,
					HasTurbo: true,
				}

				productsResp := []*substwirp.Product{
					{
						HideAds: false,
					},
				}

				userResponse := &models.Properties{
					ID: channelID,
				}

				context := test.SetupLoaderContext(context.Background(), loaders.ServerAds, adsResp, nil)
				context = test.SetupLoaderContext(context, loaders.Premium, premiumResp, nil)
				context = test.SetupLoaderContext(context, loaders.UserByLogin, userResponse, nil)
				context = test.SetupLoaderContext(context, loaders.ChannelProducts, productsResp, nil)

				resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.ServerAds(context)
				So(resp, ShouldBeTrue)
			})

			Convey("with missing country code", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					UserId:      userID,
					ChannelId:   channelID,
					ChannelName: channelName,
					DeviceId:    deviceID,
					Platform:    platform,
					PlayerType:  playerType,
				}

				adsResp := &dads.ShowServerAdsResp{
					Adblock:   true,
					ServerAds: true,
				}

				premiumResp := &nitro.GetPremiumStatusesResponse{
					HasPrime: true,
					HasTurbo: true,
				}

				productsResp := []*substwirp.Product{
					{
						HideAds: false,
					},
				}

				userResponse := &models.Properties{
					ID: channelID,
				}

				context := test.SetupLoaderContext(context.Background(), loaders.ServerAds, adsResp, nil)
				context = test.SetupLoaderContext(context, loaders.Premium, premiumResp, nil)
				context = test.SetupLoaderContext(context, loaders.UserByLogin, userResponse, nil)
				context = test.SetupLoaderContext(context, loaders.ChannelProducts, productsResp, nil)

				resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.ServerAds(context)
				So(resp, ShouldBeTrue)
			})

			Convey("with error getting channel products", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					UserId:      userID,
					ChannelId:   channelID,
					ChannelName: channelName,
					DeviceId:    deviceID,
					CountryCode: "US",
					Platform:    platform,
					PlayerType:  playerType,
				}

				resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.ServerAds(context.Background())
				So(resp, ShouldBeFalse)
			})

			Convey("with LoadServerAds returning error", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					UserId:      userID,
					ChannelId:   channelID,
					ChannelName: channelName,
					DeviceId:    deviceID,
					CountryCode: "US",
					Platform:    platform,
					PlayerType:  playerType,
				}

				adsResp := &dads.ShowServerAdsResp{
					Adblock:   true,
					ServerAds: true,
				}

				premiumResp := &nitro.GetPremiumStatusesResponse{
					HasPrime: true,
					HasTurbo: true,
				}

				productsResp := []*substwirp.Product{
					{
						HideAds: false,
					},
				}

				userResponse := &models.Properties{
					ID: channelID,
				}

				context := test.SetupLoaderContext(context.Background(), loaders.ServerAds, adsResp, errors.New("error returning ads"))
				context = test.SetupLoaderContext(context, loaders.Premium, premiumResp, nil)
				context = test.SetupLoaderContext(context, loaders.UserByLogin, userResponse, nil)
				context = test.SetupLoaderContext(context, loaders.ChannelProducts, productsResp, nil)

				resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.ServerAds(context)
				So(resp, ShouldBeFalse)
			})

			Convey("with LoadServerAds returning nil", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					UserId:      userID,
					ChannelId:   channelID,
					ChannelName: channelName,
					DeviceId:    deviceID,
					CountryCode: "US",
					Platform:    platform,
					PlayerType:  playerType,
				}

				premiumResp := &nitro.GetPremiumStatusesResponse{
					HasPrime: true,
					HasTurbo: true,
				}

				productsResp := []*substwirp.Product{
					{
						HideAds: false,
					},
				}

				userResponse := &models.Properties{
					ID: channelID,
				}

				context := test.SetupLoaderContext(context.Background(), loaders.ServerAds, nil, nil)
				context = test.SetupLoaderContext(context, loaders.Premium, premiumResp, nil)
				context = test.SetupLoaderContext(context, loaders.UserByLogin, userResponse, nil)
				context = test.SetupLoaderContext(context, loaders.ChannelProducts, productsResp, nil)

				resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.ServerAds(context)
				So(resp, ShouldBeFalse)
			})

			Convey("with playerType set to picture-by-picture", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					UserId:      userID,
					ChannelId:   channelID,
					ChannelName: channelName,
					DeviceId:    deviceID,
					CountryCode: "US",
					Platform:    platform,
					PlayerType:  "picture-by-picture",
				}

				resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.ServerAds(context.Background())
				So(resp, ShouldBeFalse)
			})
		})

		Convey("Test ShowAds", func() {
			Convey("without userID", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
				}
				resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.ShowAds(context.Background())
				So(resp, ShouldBeTrue)
			})

			Convey("user has turbo", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelId: channelID,
					UserId:    userID,
				}

				premiumResp := &nitro.GetPremiumStatusesResponse{
					HasPrime: false,
					HasTurbo: true,
				}
				context := test.SetupLoaderContext(context.Background(), loaders.Premium, premiumResp, nil)

				resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.ShowAds(context)
				So(resp, ShouldBeFalse)
			})

			Convey("user doesnt have turbo", func() {
				premiumResp := &nitro.GetPremiumStatusesResponse{
					HasPrime: false,
					HasTurbo: false,
				}
				context := test.SetupLoaderContext(context.Background(), loaders.Premium, premiumResp, nil)

				Convey("user doesnt have a subscription", func() {
					request := &borpc.GetVideoAccessTokenRequest{
						ChannelId: channelID,
						UserId:    userID,
					}

					context := test.SetupLoaderContext(context, loaders.Subscriptions, nil, nil)

					resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					resp := resolver.ShowAds(context)
					So(resp, ShouldBeTrue)
				})

				Convey("user has a subscription", func() {
					Convey("error loading channel products", func() {
						request := &borpc.GetVideoAccessTokenRequest{
							ChannelId: channelID,
							UserId:    userID,
						}

						subsResp := &voyager.Subscription{}
						context := test.SetupLoaderContext(context, loaders.Subscriptions, subsResp, nil)

						context = test.SetupLoaderContext(context, loaders.ChannelProducts, nil, errors.New("test"))

						resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
						So(err, ShouldBeNil)
						So(resolver, ShouldNotBeNil)

						resp := resolver.ShowAds(context)
						So(resp, ShouldBeTrue)
					})

					Convey("channel has no products", func() {
						request := &borpc.GetVideoAccessTokenRequest{
							ChannelId: channelID,
							UserId:    userID,
						}

						subsResp := &substwirp.Subscription{}
						context := test.SetupLoaderContext(context, loaders.Subscriptions, subsResp, nil)

						productsResp := []*substwirp.Product{}
						context = test.SetupLoaderContext(context, loaders.ChannelProducts, productsResp, nil)

						resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
						So(err, ShouldBeNil)
						So(resolver, ShouldNotBeNil)

						resp := resolver.ShowAds(context)
						So(resp, ShouldBeTrue)
					})

					Convey("channel has HideAds=false", func() {
						request := &borpc.GetVideoAccessTokenRequest{
							ChannelId: channelID,
							UserId:    userID,
						}

						subsResp := &voyager.Subscription{}
						context := test.SetupLoaderContext(context, loaders.Subscriptions, subsResp, nil)

						productsResp := []*substwirp.Product{
							{
								HideAds: false,
							},
						}
						context = test.SetupLoaderContext(context, loaders.ChannelProducts, productsResp, nil)

						resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
						So(err, ShouldBeNil)
						So(resolver, ShouldNotBeNil)

						resp := resolver.ShowAds(context)
						So(resp, ShouldBeTrue)
					})

					Convey("channel has HideAds=true", func() {
						request := &borpc.GetVideoAccessTokenRequest{
							ChannelId: channelID,
							UserId:    userID,
						}

						subsResp := &voyager.Subscription{}
						context := test.SetupLoaderContext(context, loaders.Subscriptions, subsResp, nil)

						productsResp := []*substwirp.Product{
							{
								HideAds: true,
							},
						}
						context = test.SetupLoaderContext(context, loaders.ChannelProducts, productsResp, nil)

						resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
						So(err, ShouldBeNil)
						So(resolver, ShouldNotBeNil)

						resp := resolver.ShowAds(context)
						So(resp, ShouldBeFalse)
					})
				})
			})

			Convey("playerType set to picture-by-picture", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					PlayerType: "picture-by-picture",
				}
				resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.ShowAds(context.Background())
				So(resp, ShouldBeFalse)
			})
		})

		Convey("Test Subscriber", func() {
			Convey("user has a subscription", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelId: channelID,
					UserId:    userID,
				}

				subsResp := &voyager.Subscription{}
				context := test.SetupLoaderContext(context.Background(), loaders.Subscriptions, subsResp, nil)

				resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.Subscriber(context)
				So(resp, ShouldBeTrue)
			})

			Convey("user doesnt have a subscription", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelId: channelID,
					UserId:    userID,
				}

				context := test.SetupLoaderContext(context.Background(), loaders.Subscriptions, nil, nil)

				resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.Subscriber(context)
				So(resp, ShouldBeFalse)
			})
		})

		Convey("Test Turbo", func() {
			Convey("missing userID", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
				}
				resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.Turbo(context.Background())
				So(resp, ShouldBeFalse)
			})

			Convey("error response from loader", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelId: channelID,
					UserId:    userID,
				}

				context := test.SetupLoaderContext(context.Background(), loaders.Premium, nil, errors.New("test"))

				resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.Turbo(context)
				So(resp, ShouldBeFalse)
			})

			Convey("user is turbo", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelId: channelID,
					UserId:    userID,
				}

				premiumResp := &nitro.GetPremiumStatusesResponse{
					HasPrime: false,
					HasTurbo: true,
				}
				context := test.SetupLoaderContext(context.Background(), loaders.Premium, premiumResp, nil)

				resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.Turbo(context)
				So(resp, ShouldBeTrue)
			})

			Convey("user is not turbo", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelId: channelID,
					UserId:    userID,
				}

				premiumResp := &nitro.GetPremiumStatusesResponse{
					HasPrime: false,
					HasTurbo: false,
				}
				context := test.SetupLoaderContext(context.Background(), loaders.Premium, premiumResp, nil)

				resolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.Turbo(context)
				So(resp, ShouldBeFalse)
			})
		})

		Convey("Test UserID", func() {
			Convey("happy case", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
					UserId:      userID,
				}
				resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.UserID(context.Background())
				So(*resp, ShouldEqual, userIDInt)
			})

			Convey("default response", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
				}
				resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.UserID(context.Background())
				So(resp, ShouldBeNil)
			})
		})

		Convey("Test UserIP", func() {
			Convey("happy case", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
					UserIp:      userIP,
				}
				resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.UserIP(context.Background())
				So(resp, ShouldEqual, userIP)
			})

			Convey("default response", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
				}
				resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.UserIP(context.Background())
				So(resp, ShouldEqual, "")
			})
		})

		Convey("Test Version", func() {
			request := &borpc.GetVideoAccessTokenRequest{
				ChannelName: channelName,
			}
			resolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
			So(err, ShouldBeNil)
			So(resolver, ShouldNotBeNil)

			resp := resolver.Version(context.Background())
			So(resp, ShouldEqual, version)
		})
	})
}
