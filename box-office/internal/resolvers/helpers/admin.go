package helpers

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"

	"code.justin.tv/commerce/box-office/internal/loaders"
	gerrors "github.com/pkg/errors"
)

// UserIsAdminOrSubadmin returns true if the input user is an admin or subadmin. Returns an
// error if a failure happened.
func UserIsAdminOrSubadmin(ctx context.Context, userID string) (bool, error) {
	if userID == "" {
		return false, nil // anonymous users are not admins
	}

	userResp, err := loaders.LoadUserByID(ctx, userID)
	if err != nil {
		return false, gerrors.Wrapf(err, "Error loading user data for user %s", userID)
	}

	if userResp == nil {
		return false, fmt.Errorf("Receieved nil user data for user %s", userID)
	}

	if userResp.Subadmin != nil && *userResp.Subadmin {
		log.WithField("userID", userID).Debug("User is a subadmin.")
		return true, nil
	}

	if userResp.Admin != nil && *userResp.Admin {
		log.WithField("userID", userID).Debug("User is an admin.")
		return true, nil
	}

	return false, nil
}
