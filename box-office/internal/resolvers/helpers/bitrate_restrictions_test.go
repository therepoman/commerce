package helpers

import (
	"context"
	"errors"
	"testing"

	voyager "code.justin.tv/amzn/TwitchVoyagerTwirp"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	"code.justin.tv/vod/tailor/rpc/tailor"
	"code.justin.tv/web/users-service/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/assert"
)

const (
	archives = "archives"
	vodID    = "12345"
)

func TestBitrateRestrictions(t *testing.T) {
	Convey("Test BitrateRestrictions", t, func() {
		Convey("Test GetRestrictedBitrates", func() {
			Convey("error from loader", func() {

				context := test.SetupLoaderContext(context.Background(), loaders.ChannelProducts, nil, errors.New("test"))

				restrictedBitrates := GetRestrictedBitrates(context, channelID)
				So(len(restrictedBitrates), ShouldEqual, 0)
			})

			Convey("empty ticket product", func() {

				productsResp := []*substwirp.Product{}
				context := test.SetupLoaderContext(context.Background(), loaders.ChannelProducts, productsResp, nil)

				restrictedBitrates := GetRestrictedBitrates(context, channelID)
				So(len(restrictedBitrates), ShouldEqual, 0)
			})

			Convey("ticket product has bitrate restrictions", func() {

				productsResp := []*substwirp.Product{
					{
						BitrateAccess: []string{archives},
					},
				}
				context := test.SetupLoaderContext(context.Background(), loaders.ChannelProducts, productsResp, nil)

				restrictedBitrates := GetRestrictedBitrates(context, channelID)
				So(len(restrictedBitrates), ShouldEqual, 1)
				So(restrictedBitrates[0], ShouldEqual, archives)
			})

		})

		Convey("Test GetRestrictedBitratesWithVod", func() {
			Convey("channel has base restrictions", func() {
				channelRestrictions := []string{archives, "1080p60"}
				productsResp := []*substwirp.Product{
					{
						BitrateAccess: channelRestrictions,
					},
				}

				ctx := test.SetupLoaderContext(context.Background(), loaders.ChannelProducts, productsResp, nil)

				restrictedBitrates := GetRestrictedBitratesWithVod(ctx, channelID, vodID)
				So(restrictedBitrates, ShouldResemble, channelRestrictions)
			})

			Convey("error from loader", func() {
				productsResp := []*substwirp.Product{}
				context := test.SetupLoaderContext(context.Background(), loaders.ChannelProducts, productsResp, nil)
				context = test.SetupLoaderContext(context, loaders.VodRestrictions, nil, errors.New("test"))

				restrictedBitrates := GetRestrictedBitratesWithVod(context, channelID, vodID)
				So(len(restrictedBitrates), ShouldEqual, 0)
			})

			Convey("empty restrictions response", func() {
				productsResp := []*substwirp.Product{}
				context := test.SetupLoaderContext(context.Background(), loaders.ChannelProducts, productsResp, nil)
				context = test.SetupLoaderContext(context, loaders.VodRestrictions, &nioh.RestrictionResourceResponse{}, nil)

				restrictedBitrates := GetRestrictedBitratesWithVod(context, channelID, vodID)
				So(len(restrictedBitrates), ShouldEqual, 0)
			})

			Convey("vod has product restrictions", func() {
				productsResp := []*substwirp.Product{}
				context := test.SetupLoaderContext(context.Background(), loaders.ChannelProducts, productsResp, nil)

				restriction := &nioh.RestrictionResourceResponse{
					Id:   vodID,
					Type: nioh.ResourceType_VIDEO,
					Restriction: &nioh.Restriction{
						RestrictionType: nioh.Restriction_SUB_ONLY_LIVE,
					},
				}
				context = test.SetupLoaderContext(context, loaders.VodRestrictions, restriction, nil)

				restrictedBitrates := GetRestrictedBitratesWithVod(context, channelID, vodID)
				So(len(restrictedBitrates), ShouldEqual, 1)
				So(restrictedBitrates[0], ShouldEqual, archives)
			})

			Convey("vod has geoblock restrictions", func() {
				productsResp := []*substwirp.Product{}
				context := test.SetupLoaderContext(context.Background(), loaders.ChannelProducts, productsResp, nil)

				restriction := &nioh.RestrictionResourceResponse{
					Id:   vodID,
					Type: nioh.ResourceType_VIDEO,
					Restriction: &nioh.Restriction{
						RestrictionType: nioh.Restriction_GEOBLOCK,
					},
				}
				context = test.SetupLoaderContext(context, loaders.VodRestrictions, restriction, nil)

				restrictedBitrates := GetRestrictedBitratesWithVod(context, channelID, vodID)
				So(len(restrictedBitrates), ShouldEqual, 0)
			})
		})

		Convey("Test GetRestrictedBitratesForUser", func() {
			productsResp := []*substwirp.Product{
				{
					BitrateAccess: []string{archives},
				},
			}
			context := test.SetupLoaderContext(context.Background(), loaders.ChannelProducts, productsResp, nil)

			isTrue := true
			isFalse := false
			Convey("missing userID", func() {

				restrictedBitrates := GetRestrictedBitratesForUser(context, channelID, "")
				So(len(restrictedBitrates), ShouldEqual, 1)
				So(restrictedBitrates[0], ShouldEqual, archives)
			})

			Convey("error from loader", func() {
				context = test.SetupLoaderContext(context, loaders.UserByID, nil, errors.New("test"))

				restrictedBitrates := GetRestrictedBitratesForUser(context, channelID, userID)
				So(len(restrictedBitrates), ShouldEqual, 1)
				So(restrictedBitrates[0], ShouldEqual, archives)
			})

			Convey("user returns is admin", func() {
				userResp := &models.Properties{
					Subadmin: &isTrue,
					Admin:    &isFalse,
				}
				context = test.SetupLoaderContext(context, loaders.UserByID, userResp, nil)

				restrictedBitrates := GetRestrictedBitratesForUser(context, channelID, userID)
				So(len(restrictedBitrates), ShouldEqual, 0)
			})

			Convey("user returns is not admin", func() {
				userResp := &models.Properties{
					Subadmin: &isFalse,
					Admin:    &isFalse,
				}
				context = test.SetupLoaderContext(context, loaders.UserByID, userResp, nil)

				Convey("user is sub", func() {
					subsResp := &voyager.Subscription{}
					context = test.SetupLoaderContext(context, loaders.Subscriptions, subsResp, nil)

					restrictedBitrates := GetRestrictedBitratesForUser(context, channelID, userID)
					So(len(restrictedBitrates), ShouldEqual, 0)
				})

				Convey("user is not sub", func() {
					context = test.SetupLoaderContext(context, loaders.Subscriptions, nil, nil)

					restrictedBitrates := GetRestrictedBitratesForUser(context, channelID, userID)
					So(len(restrictedBitrates), ShouldEqual, 1)
					So(restrictedBitrates[0], ShouldEqual, archives)
				})
			})
		})
	})
}

func TestGetRestrictedBitratesForUser(t *testing.T) {
	populatedChanBitrateRestrictions := []string{archives, "1080p60"}
	emptyBitrateRestrictions := []string{}

	vodRestriction := &nioh.RestrictionResourceResponse{
		Id:   vodID,
		Type: nioh.ResourceType_VIDEO,
		Restriction: &nioh.Restriction{
			Id:              "vod:123",
			RestrictionType: nioh.Restriction_SUB_ONLY_LIVE,
		},
	}

	var adminUser, nonadminUser *models.Properties
	{
		addrTrue := true
		addrFalse := false

		adminUser = &models.Properties{
			Admin:    &addrTrue,
			Subadmin: &addrTrue,
		}
		nonadminUser = &models.Properties{
			Admin:    &addrFalse,
			Subadmin: &addrFalse,
		}
	}

	type userFixture struct {
		data *models.Properties
		err  error
	}

	type niohRestrictionFixture struct {
		data *nioh.RestrictionResourceResponse
		err  error
	}

	type niohAuthFixture struct {
		data *nioh.GetUserAuthorizationResponse
		err  error
	}

	type subsFixture struct {
		data *voyager.Subscription
		err  error
	}

	type tailorFixture struct {
		data *tailor.PublicGetChannelTrailerResponse
		err  error
	}

	testCases := []struct {
		description string

		userID string

		user userFixture

		channelRestrictions []string // Blanket restrictions on channel bitrates or "archives"

		vodRestriction niohRestrictionFixture

		vodAuthorization niohAuthFixture

		subscription subsFixture

		channelTrailer tailorFixture

		want []string
	}{
		{
			description: "with channel restrictions, restricted VOD, and authorized nonadmin user",
			userID:      userID,
			user: userFixture{
				data: nonadminUser,
				err:  nil,
			},
			channelRestrictions: populatedChanBitrateRestrictions,
			vodRestriction: niohRestrictionFixture{
				data: vodRestriction,
				err:  nil,
			},
			vodAuthorization: niohAuthFixture{
				data: &nioh.GetUserAuthorizationResponse{
					UserIsAuthorized: true,
				},
				err: nil,
			},
			want: emptyBitrateRestrictions,
		},
		{
			description: "with channel restrictions, restricted VOD, and unauthorized nonadmin user",
			userID:      userID,
			user: userFixture{
				data: nonadminUser,
				err:  nil,
			},
			channelRestrictions: populatedChanBitrateRestrictions,
			vodRestriction: niohRestrictionFixture{
				data: vodRestriction,
				err:  nil,
			},
			vodAuthorization: niohAuthFixture{
				data: &nioh.GetUserAuthorizationResponse{
					UserIsAuthorized: false,
				},
				err: nil,
			},
			want: populatedChanBitrateRestrictions,
		},
		{
			description: "with channel restrictions, unrestricted VOD, and subbed nonadmin user",
			userID:      userID,
			user: userFixture{
				data: nonadminUser,
				err:  nil,
			},
			channelRestrictions: populatedChanBitrateRestrictions,
			subscription: subsFixture{
				data: &voyager.Subscription{},
				err:  nil,
			},
			want: emptyBitrateRestrictions,
		},
		{
			description: "with channel restrictions, unrestricted VOD, and unsubbed nonadmin user",
			userID:      userID,
			user: userFixture{
				data: nonadminUser,
				err:  nil,
			},
			channelRestrictions: populatedChanBitrateRestrictions,
			want:                populatedChanBitrateRestrictions,
		},
		{
			description: "with no channel restrictions and unrestricted VOD",
			userID:      userID,
			user: userFixture{
				data: nonadminUser,
				err:  nil,
			},
			channelRestrictions: emptyBitrateRestrictions,
			want:                emptyBitrateRestrictions,
		},
		{
			description: "with no channel restrictions, restricted VOD, and authorized nonadmin user",
			userID:      userID,
			user: userFixture{
				data: nonadminUser,
				err:  nil,
			},
			channelRestrictions: emptyBitrateRestrictions,
			vodRestriction: niohRestrictionFixture{
				data: vodRestriction,
				err:  nil,
			},
			vodAuthorization: niohAuthFixture{
				data: &nioh.GetUserAuthorizationResponse{
					UserIsAuthorized: true,
				},
				err: nil,
			},
			want: emptyBitrateRestrictions,
		},
		{
			description: "with no channel restrictions, restricted VOD, and unauthorized nonadmin user",
			userID:      userID,
			user: userFixture{
				data: nonadminUser,
				err:  nil,
			},
			channelRestrictions: emptyBitrateRestrictions,
			vodRestriction: niohRestrictionFixture{
				data: vodRestriction,
				err:  nil,
			},
			vodAuthorization: niohAuthFixture{
				data: &nioh.GetUserAuthorizationResponse{
					UserIsAuthorized: false,
				},
				err: nil,
			},
			want: prohibitedVODRestrictedBitrates,
		},
		{
			description: "with channel restrictions, unrestricted VOD, and admin user",
			userID:      userID,
			user: userFixture{
				data: adminUser,
				err:  nil,
			},
			channelRestrictions: populatedChanBitrateRestrictions,
			want:                emptyBitrateRestrictions,
		},
		{
			description: "with no channel restrictions, restricted VOD, and unauthorized admin user",
			userID:      userID,
			user: userFixture{
				data: adminUser,
				err:  nil,
			},
			channelRestrictions: emptyBitrateRestrictions,
			vodRestriction: niohRestrictionFixture{
				data: vodRestriction,
				err:  nil,
			},
			vodAuthorization: niohAuthFixture{
				data: &nioh.GetUserAuthorizationResponse{
					UserIsAuthorized: false,
				},
				err: nil,
			},
			want: emptyBitrateRestrictions,
		},
		{
			description: "with no channel restrictions, restricted VOD, and authorized admin user",
			userID:      userID,
			user: userFixture{
				data: adminUser,
				err:  nil,
			},
			channelRestrictions: emptyBitrateRestrictions,
			vodRestriction: niohRestrictionFixture{
				data: vodRestriction,
				err:  nil,
			},
			vodAuthorization: niohAuthFixture{
				data: &nioh.GetUserAuthorizationResponse{
					UserIsAuthorized: true,
				},
				err: nil,
			},
			want: emptyBitrateRestrictions,
		},
		{
			description:         "with channel restrictions and anonymous user",
			userID:              "",
			channelRestrictions: populatedChanBitrateRestrictions,
			want:                populatedChanBitrateRestrictions,
		},
		{
			description:         "with no channel restrictions and anonymous user",
			userID:              "",
			channelRestrictions: emptyBitrateRestrictions,
			want:                emptyBitrateRestrictions,
		},
		{
			description: "with no channel restrictions, restricted VOD, and error from Nioh",
			userID:      userID,
			user: userFixture{
				data: nonadminUser,
			},
			channelRestrictions: emptyBitrateRestrictions,
			vodRestriction: niohRestrictionFixture{
				data: vodRestriction,
			},
			vodAuthorization: niohAuthFixture{
				err: errors.New("RIP it was lit fam"),
			},
			want: emptyBitrateRestrictions,
		},
		// All cases for when VOD is not channel trailer are handled by default when channelTrailer not specified, so
		// we only explicitly test cases when Vod is trailer.
		{
			description: "with no channel restrictions, restricted VOD, vod is channel trailer, and unauthorized nonadmin user",
			userID:      userID,
			user: userFixture{
				data: nonadminUser,
				err:  nil,
			},
			channelRestrictions: emptyBitrateRestrictions,
			vodRestriction: niohRestrictionFixture{
				data: vodRestriction,
				err:  nil,
			},
			vodAuthorization: niohAuthFixture{
				data: &nioh.GetUserAuthorizationResponse{
					UserIsAuthorized: false,
				},
				err: nil,
			},
			channelTrailer: tailorFixture{
				data: &tailor.PublicGetChannelTrailerResponse{
					VodId: vodID,
				},
				err: nil,
			},
			want: emptyBitrateRestrictions,
		},
		{
			description:         "with no channel restrictions, restricted VOD, vod is channel trailer, and anonymous user",
			userID:              "",
			channelRestrictions: emptyBitrateRestrictions,
			vodRestriction: niohRestrictionFixture{
				data: vodRestriction,
				err:  nil,
			},
			vodAuthorization: niohAuthFixture{
				data: &nioh.GetUserAuthorizationResponse{
					UserIsAuthorized: false,
				},
				err: nil,
			},
			channelTrailer: tailorFixture{
				data: &tailor.PublicGetChannelTrailerResponse{
					VodId: vodID,
				},
				err: nil,
			},
			want: emptyBitrateRestrictions,
		},
		{
			description:         "with channel restrictions, restricted VOD, vod is channel trailer, and anonymous user",
			userID:              "",
			channelRestrictions: populatedChanBitrateRestrictions,
			vodRestriction: niohRestrictionFixture{
				data: vodRestriction,
				err:  nil,
			},
			vodAuthorization: niohAuthFixture{
				data: &nioh.GetUserAuthorizationResponse{
					UserIsAuthorized: false,
				},
				err: nil,
			},
			channelTrailer: tailorFixture{
				data: &tailor.PublicGetChannelTrailerResponse{
					VodId: vodID,
				},
				err: nil,
			},
			want: emptyBitrateRestrictions,
		},
		{
			description: "with channel restrictions, restricted VOD, vod is channel trailer, and admin user",
			userID:      userID,
			user: userFixture{
				data: adminUser,
				err:  nil,
			},
			channelRestrictions: populatedChanBitrateRestrictions,
			vodRestriction: niohRestrictionFixture{
				data: vodRestriction,
				err:  nil,
			},
			vodAuthorization: niohAuthFixture{
				data: &nioh.GetUserAuthorizationResponse{
					UserIsAuthorized: false,
				},
				err: nil,
			},
			channelTrailer: tailorFixture{
				data: &tailor.PublicGetChannelTrailerResponse{
					VodId: vodID,
				},
				err: nil,
			},
			want: emptyBitrateRestrictions,
		},
		{
			description: "restricted VOD, anonymous user, and error from Tailor",
			userID:      "",
			vodRestriction: niohRestrictionFixture{
				data: vodRestriction,
				err:  nil,
			},
			vodAuthorization: niohAuthFixture{
				data: &nioh.GetUserAuthorizationResponse{
					UserIsAuthorized: false,
				},
				err: nil,
			},
			channelTrailer: tailorFixture{
				err: errors.New("RIP it was lit fam"),
			},
			want: prohibitedVODRestrictedBitrates,
		},
	}

	for _, tt := range testCases {
		t.Run(tt.description, func(t *testing.T) {
			ctx := context.Background()

			// set up mock ChannelProducts response
			ctx = test.SetupLoaderContext(ctx, loaders.ChannelProducts, []*substwirp.Product{{BitrateAccess: tt.channelRestrictions}}, nil)

			// set up mock Users response
			ctx = test.SetupLoaderContext(ctx, loaders.UserByID, tt.user.data, tt.user.err)

			// set up mock Nioh restriction response
			ctx = test.SetupLoaderContext(ctx, loaders.VodRestrictions, tt.vodRestriction.data, tt.vodRestriction.err)

			// set up mock Nioh Auth response
			ctx = test.SetupLoaderContext(ctx, loaders.UserVODAuthorizations, tt.vodAuthorization.data, tt.vodAuthorization.err)

			// set up mock Subscriptions response
			ctx = test.SetupLoaderContext(ctx, loaders.Subscriptions, tt.subscription.data, tt.subscription.err)

			// set up mock Tailor response
			ctx = test.SetupLoaderContext(ctx, loaders.ChannelTrailer, tt.channelTrailer.data, tt.channelTrailer.err)

			got := GetRestrictedBitratesForUserAndVod(ctx, channelID, tt.userID, vodID)
			//So(got, ShouldResemble, tt.want)
			assert.Equal(t, tt.want, got)
		})
	}
}
