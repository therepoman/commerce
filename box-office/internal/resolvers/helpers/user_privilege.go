package helpers

import (
	"context"
)

// UserHasEscalatedChanSubPrivileges returns true if the user has escalated privileges in the channel. Otherwise, returns false.
// Reference web/web: https://git-aws.internal.justin.tv/web/web/blob/master/engines/payments/app/adapters/bitrate_restrictions_adapter.rb#L17
func UserHasEscalatedChanSubPrivileges(ctx context.Context, channelID string, userID string) bool {
	// anonymous users are always unprivileged
	if userID == "" {
		return false
	}

	if channelID == "" {
		return false
	}

	channelRestrictedBitrates := GetRestrictedBitrates(ctx, channelID)
	if len(channelRestrictedBitrates) == 0 {
		// either there is no ticket product or the channel hasn't specified any bitrate restrictions
		// that means there is no concept of escalated privilege.
		return false
	}

	// else this channel has some bitrate restrictions, so load the user's bitrate restrictions
	userRestrictedBitrates := GetRestrictedBitratesForUser(ctx, channelID, userID)

	// if a user has fewer restricted bitrates than the channel default, then they are considered privileged
	return len(userRestrictedBitrates) < len(channelRestrictedBitrates)
}
