package helpers

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/web/users-service/models"

	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"
	. "github.com/smartystreets/goconvey/convey"
)

func TestAdmin(t *testing.T) {
	Convey("Test Admin", t, func() {
		Convey("Test UserIsAdminOrSubadmin", func() {

			context := context.Background()
			isTrue := true
			isFalse := false
			Convey("missing userID", func() {

				isAdminOrSubadmin, err := UserIsAdminOrSubadmin(context, "")
				So(err, ShouldBeNil)
				So(isAdminOrSubadmin, ShouldBeFalse)

			})

			Convey("error from loader", func() {
				context = test.SetupLoaderContext(context, loaders.UserByID, nil, errors.New("test"))

				_, err := UserIsAdminOrSubadmin(context, userID)
				So(err, ShouldNotBeNil)
			})

			Convey("user returns nil response", func() {
				context = test.SetupLoaderContext(context, loaders.UserByID, nil, nil)

				_, err := UserIsAdminOrSubadmin(context, userID)
				So(err, ShouldNotBeNil)
			})

			Convey("user returns neither", func() {
				userResp := &models.Properties{
					Subadmin: &isFalse,
					Admin:    &isFalse,
				}
				context = test.SetupLoaderContext(context, loaders.UserByID, userResp, nil)

				isAdminOrSubadmin, err := UserIsAdminOrSubadmin(context, userID)
				So(err, ShouldBeNil)
				So(isAdminOrSubadmin, ShouldBeFalse)
			})

			Convey("user returns is subadmin", func() {
				userResp := &models.Properties{
					Subadmin: &isTrue,
					Admin:    &isFalse,
				}
				context = test.SetupLoaderContext(context, loaders.UserByID, userResp, nil)

				isAdminOrSubadmin, err := UserIsAdminOrSubadmin(context, userID)
				So(err, ShouldBeNil)
				So(isAdminOrSubadmin, ShouldBeTrue)
			})

			Convey("user returns is admin", func() {
				userResp := &models.Properties{
					Subadmin: &isFalse,
					Admin:    &isTrue,
				}
				context = test.SetupLoaderContext(context, loaders.UserByID, userResp, nil)

				isAdminOrSubadmin, err := UserIsAdminOrSubadmin(context, userID)
				So(err, ShouldBeNil)
				So(isAdminOrSubadmin, ShouldBeTrue)
			})

			Convey("user returns both", func() {
				userResp := &models.Properties{
					Subadmin: &isTrue,
					Admin:    &isTrue,
				}
				context = test.SetupLoaderContext(context, loaders.UserByID, userResp, nil)

				isAdminOrSubadmin, err := UserIsAdminOrSubadmin(context, userID)
				So(err, ShouldBeNil)
				So(isAdminOrSubadmin, ShouldBeTrue)
			})

			Convey("user returns subadmin nil", func() {
				userResp := &models.Properties{
					Subadmin: nil,
					Admin:    &isTrue,
				}
				context = test.SetupLoaderContext(context, loaders.UserByID, userResp, nil)

				isAdminOrSubadmin, err := UserIsAdminOrSubadmin(context, userID)
				So(err, ShouldBeNil)
				So(isAdminOrSubadmin, ShouldBeTrue)
			})

			Convey("user returns admin nil", func() {
				userResp := &models.Properties{
					Subadmin: &isTrue,
					Admin:    nil,
				}
				context = test.SetupLoaderContext(context, loaders.UserByID, userResp, nil)

				isAdminOrSubadmin, err := UserIsAdminOrSubadmin(context, userID)
				So(err, ShouldBeNil)
				So(isAdminOrSubadmin, ShouldBeTrue)
			})

			Convey("user returns both nil", func() {
				userResp := &models.Properties{
					Subadmin: nil,
					Admin:    nil,
				}
				context = test.SetupLoaderContext(context, loaders.UserByID, userResp, nil)

				isAdminOrSubadmin, err := UserIsAdminOrSubadmin(context, userID)
				So(err, ShouldBeNil)
				So(isAdminOrSubadmin, ShouldBeFalse)
			})
		})
	})
}
