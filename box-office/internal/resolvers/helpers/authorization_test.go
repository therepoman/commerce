package helpers

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/nioh/rpc/nioh"

	"code.justin.tv/commerce/box-office/config"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"

	"code.justin.tv/commerce/box-office/mocks"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	paywalledChannelID = "123"
	paywalledVODID     = "456"
)

func TestAuthorization(t *testing.T) {
	mockStatter := new(mocks.Statter)
	mockStatter.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

	Convey("Test Authorization", t, func() {
		Convey("Test UserIsForbidden", func() {
			testOrigin := ""
			context := context.Background()
			cfg := &config.Configuration{}

			Convey("missing channelID", func() {
				forbidden, reason := UserIsForbidden(context, cfg, userID, "", testOrigin, mockStatter)
				So(forbidden, ShouldBeFalse)
				So(reason, ShouldEqual, ReasonNone)
			})

			niohResp := &nioh.GetUserAuthorizationResponse{
				UserIsAuthorized: false,
			}

			Convey("with authorized user ID", func() {
				niohResp.UserIsAuthorized = true

				context = test.SetupLoaderContext(context, loaders.UserChannelAuthorizations, niohResp, nil)

				forbidden, reason := UserIsForbidden(context, cfg, userID, paywalledChannelID, testOrigin, mockStatter)
				So(forbidden, ShouldBeFalse)
				So(reason, ShouldEqual, ReasonNone)
			})

			Convey("with unauthorized user ID", func() {
				context = test.SetupLoaderContext(context, loaders.UserChannelAuthorizations, niohResp, nil)

				forbidden, reason := UserIsForbidden(context, cfg, userID, paywalledChannelID, testOrigin, mockStatter)
				So(forbidden, ShouldBeTrue)
				So(reason, ShouldEqual, ReasonPremiumContent)
			})

			Convey("with error from Nioh", func() {
				context = test.SetupLoaderContext(context, loaders.UserChannelAuthorizations, niohResp, errors.New("rip it was lit fam"))

				forbidden, reason := UserIsForbidden(context, cfg, userID, paywalledChannelID, testOrigin, mockStatter)
				So(forbidden, ShouldBeFalse)
				So(reason, ShouldEqual, ReasonNone)
			})
		})
	})

	Convey("Test VODAuthorization", t, func() {
		Convey("Test VODIsForbidden", func() {

			context := context.Background()

			Convey("missing vodID", func() {
				forbidden, reason := VODIsForbidden(context, userID, "", "US", mockStatter)
				So(forbidden, ShouldBeFalse)
				So(reason, ShouldEqual, ReasonNone)
			})

			Convey("valid parameters", func() {

				niohResp := &nioh.GetUserAuthorizationResponse{}

				Convey("with userid is authorized via Nioh", func() {
					niohResp.UserIsAuthorized = true
					context = test.SetupLoaderContext(context, loaders.UserVODAuthorizations, niohResp, nil)

					forbidden, reason := VODIsForbidden(context, userID, paywalledVODID, "US", mockStatter)
					So(forbidden, ShouldBeFalse)
					So(reason, ShouldEqual, ReasonNone)
				})

				Convey("with userid is not authorized via Nioh", func() {
					context = test.SetupLoaderContext(context, loaders.UserVODAuthorizations, niohResp, nil)
					context = test.SetupLoaderContext(context, loaders.VodRestrictions, nil, nil)

					niohResp.UserIsAuthorized = false
					forbidden, reason := VODIsForbidden(context, userID, paywalledVODID, "US", mockStatter)
					So(forbidden, ShouldBeTrue)
					So(reason, ShouldEqual, ReasonPremiumContent)
				})

				Convey("with Nioh call failure", func() {
					context = test.SetupLoaderContext(context, loaders.UserVODAuthorizations, nil, errors.New("test"))

					forbidden, reason := VODIsForbidden(context, userID, paywalledVODID, "US", mockStatter)
					So(forbidden, ShouldBeFalse)
					So(reason, ShouldEqual, ReasonNone)
				})
			})
		})
	})
}
