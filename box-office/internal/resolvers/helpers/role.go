package helpers

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"time"

	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/foundation/twitchclient"
	users "code.justin.tv/web/users-service/client/usersclient_internal"
	"github.com/cactus/go-statsd-client/statsd"
	log "github.com/sirupsen/logrus"
)

const (
	staffSearchParam      = "admin"
	RoleNone         Role = ""
	RoleStaff        Role = "staff"
)

type roleMapSafe struct {
	Map  map[string]Role
	Lock sync.RWMutex
}

var userRoles = roleMapSafe{
	Map:  map[string]Role{},
	Lock: sync.RWMutex{},
}

// RoleReloader is responsible for querying users service for the list of staff members and caching the results.
type RoleReloader struct {
	client users.InternalClient
}

// Role represents the role a user fills
type Role string

func (r Role) String() string {
	return string(r)
}

// GetRole returns the role of the user, for now it only supplies whether or not a user is a Staff member.
func GetRole(ctx context.Context, userID string) (Role, error) {
	if userID == "" {
		return RoleNone, nil // Anonymous users get no special role
	}
	// Staff list is only returned as a list of userLogins, use userID to get login if not supplied
	userProps, err := loaders.LoadUserByID(ctx, userID) // This should cache hit, hopefully...
	if err != nil || userProps.Login == nil {
		return RoleNone, errors.New("cannot get role, userID lookup failed")
	}
	userLogin := *userProps.Login

	// Just in case
	if userLogin == "" {
		return RoleNone, nil
	}

	// goMaps are not threadsafe, be safe. This lock is read concurrent
	userRoles.Lock.RLock()
	userRole, found := userRoles.Map[userLogin]
	userRoles.Lock.RUnlock()
	if !found {
		return RoleNone, nil
	}
	return userRole, nil
}

// NewRolesReloader loads the client used by the Reload function and returns a RolesReloader with a Reload function used in the Poller interface
func NewRolesReloader(endpoint string, stats statsd.Statter) (*RoleReloader, error) {
	config := twitchclient.ClientConf{
		Host:  endpoint,
		Stats: stats,
	}
	usersClient, err := users.NewClient(config)
	if err != nil {
		return nil, fmt.Errorf("Failed to set up Users client: %v", err)
	}

	roleReloader := &RoleReloader{
		usersClient,
	}
	return roleReloader, nil
}

// Reload fits the reloader interface from dynamic config and refreshes the roleMap
func (r *RoleReloader) Reload(ctx context.Context) error {
	privileged, err := r.client.GetGlobalPrivilegedUsers(ctx, []string{staffSearchParam}, nil)
	if err != nil {
		return err
	}

	if privileged == nil {
		return errors.New("error fetching global priveleged users")
	}

	if len(privileged.Admins) == 0 {
		return errors.New("error fetching global priveleged users, list is empty")
	}

	staffList := privileged.Admins
	tempMap := map[string]Role{}
	for _, staffMember := range staffList {
		if staffMember != "" { // Lets avoid a possible disaster later
			tempMap[staffMember] = RoleStaff
		}
	}

	// Replace the cached data with fresh data. This will also expire any people removed from the Staff List. This will block any read locks from GetRole
	userRoles.Lock.Lock()
	userRoles.Map = tempMap
	userRoles.Lock.Unlock()

	return nil
}

// HandleError is passed into the Poller interface to log errors
func (r *RoleReloader) HandleError(err error) {
	// If users is unreachable, expire the map
	userRoles.Lock.Lock()
	userRoles.Map = map[string]Role{}
	userRoles.Lock.Unlock()

	log.Error(fmt.Errorf("error from roles reloader, %v", err))
}

// RetryAndHandleError retries the reload func before returning the error handler
func (r *RoleReloader) RetryAndHandleError(maxRetries int) func(error) {
	success := false
	for n := 0; n < maxRetries; n++ {
		ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
		defer cancel()
		err := r.Reload(ctx)
		if err == nil {
			success = true
			break
		}
	}
	if !success {
		return r.HandleError
	}
	return func(err error) {} // Retry worked
}
