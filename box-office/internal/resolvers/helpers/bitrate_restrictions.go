package helpers

import (
	"context"

	log "github.com/sirupsen/logrus"

	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/nioh/rpc/nioh"
)

var (
	defaultRestrictedBitrates       = []string{}
	prohibitedVODRestrictedBitrates = []string{"archives"}
)

// GetRestrictedBitrates returns the restricted bitrates for a channel. The input channelID is used
// to look up channel properties to determine the restrictions.
// Reference web/web: https://git-aws.internal.justin.tv/web/web/blob/master/engines/payments/app/interactors/payments/calculate_restricted_bitrates.rb
func GetRestrictedBitrates(ctx context.Context, channelID string) []string {
	// lookup bitrate restrictions for channel
	products, err := loaders.LoadChannelProducts(ctx, channelID)
	if err != nil {
		log.Errorf("Error loading channel products. Using default value. Error: %+v", err)
		return defaultRestrictedBitrates
	}
	if len(products) == 0 {
		log.Debug("This channel has no products. Using default value.")
		return defaultRestrictedBitrates
	}

	// grab the bitrate access for any of the channel's products
	restrictedBitrates := products[0].BitrateAccess

	if restrictedBitrates == nil {
		// No restrictions, so return an empty list, not nil
		return defaultRestrictedBitrates
	}

	return restrictedBitrates
}

// GetRestrictedBitratesWithVod returns the restricted bitrates for a channel or vod.
func GetRestrictedBitratesWithVod(ctx context.Context, channelID string, vodID string) []string {
	restrictedBitrates := GetRestrictedBitrates(ctx, channelID)
	if len(restrictedBitrates) != 0 {
		return restrictedBitrates
	}

	restriction, err := loaders.LoadVODRestriction(ctx, vodID)
	if err != nil {
		log.Errorf("Error loading restriction for channel id %s and vod id %s, using default value. Error: %+v", channelID, vodID, err)
		return defaultRestrictedBitrates
	}

	if restriction == nil || restriction.GetRestriction() == nil {
		return defaultRestrictedBitrates
	}

	// if the restriction is a geoblock, don't restrict bitrates b/c we set the token's
	// Authorization field to represent geoblock restrictions
	if restriction.GetRestriction().GetRestrictionType() == nioh.Restriction_GEOBLOCK {
		return defaultRestrictedBitrates
	}

	return prohibitedVODRestrictedBitrates
}

// GetRestrictedBitratesForUser returns the restricted bitrates for a channel+user. The input channelID and userID are used
// to look up channel and user properties to determine the restrictions.
// Reference web/web: https://git-aws.internal.justin.tv/web/web/blob/master/engines/payments/app/interactors/payments/calculate_restricted_bitrates_for_user.rb
func GetRestrictedBitratesForUser(ctx context.Context, channelID string, userID string) []string {
	channelRestrictedBitrates := GetRestrictedBitrates(ctx, channelID)
	resource := &channel{id: channelID}
	return getRestrictedBitratesForUserHelper(ctx, resource, userID, channelRestrictedBitrates)
}

// GetRestrictedBitratesForUserAndVod returns the restricted bitrates for a channel+user or vod.
func GetRestrictedBitratesForUserAndVod(ctx context.Context, channelID string, userID string, vodID string) []string {
	channelRestrictedBitrates := GetRestrictedBitratesWithVod(ctx, channelID, vodID)
	resource := &vod{channelID: channelID, vodID: vodID}

	// Channel Trailers are Vods and so users may be unable to view them on channels with restricted bitrates active.
	// We check if the Vod is a trailer and short circuit if so, since it won't matter if the user is authenticated,
	// is admin, or has a sub or not.
	if vodIsChannelTrailer(ctx, channelID, vodID) {
		return defaultRestrictedBitrates
	}

	return getRestrictedBitratesForUserHelper(ctx, resource, userID, channelRestrictedBitrates)
}

func getRestrictedBitratesForUserHelper(ctx context.Context, resource restrictedResource, userID string, channelRestrictedBitrates []string) []string {
	if userID == "" {
		log.Debug("This user is anonymous and will receive the channel's bitrate restrictions.")
		return channelRestrictedBitrates
	}

	isAdminOrSubadmin, err := UserIsAdminOrSubadmin(ctx, userID)
	if err != nil {
		log.Errorf("Error determining if user: %s is an admin. Returning channel bitrate restrictions. Error %+v", userID, err)
		return channelRestrictedBitrates
	}

	if isAdminOrSubadmin {
		log.WithField("userID", userID).Debug("This user is an admin and receives special bitrate restrictions.")
		return defaultRestrictedBitrates
	}

	isAuthorizedByResource := resource.userIsAuthorized(ctx, userID)
	if isAuthorizedByResource {
		log.WithFields(log.Fields{
			"userID": userID,
		}).Debug("This user is authorized to access the resource and receives special bitrate restrictions.")
		return defaultRestrictedBitrates
	}

	// this user should get the default experience, which is using the channel's bitrate restriction settings
	log.WithFields(log.Fields{
		"userID": userID,
	}).Debug("This user has been given the channel's bitrate restrictions.")
	return channelRestrictedBitrates
}

type restrictedResource interface {
	userIsAuthorized(ctx context.Context, userID string) bool
}

type channel struct {
	id string
}

func (c channel) userIsAuthorized(ctx context.Context, userID string) bool {
	return IsSubscriber(ctx, c.id, userID)
}

type vod struct {
	channelID string
	vodID     string
}

func (v vod) userIsAuthorized(ctx context.Context, userID string) bool {
	logger := log.WithFields(log.Fields{
		"channelID": v.channelID,
		"vodID":     v.vodID,
		"userID":    userID,
	})

	restriction, err := loaders.LoadVODRestriction(ctx, v.vodID)
	if err != nil {
		logger.WithError(err).Error("Error loading VOD restriction; defaulting to authorized.")
		return true
	}
	if restriction == nil || restriction.Restriction == nil {
		logger.Debug("Received empty VOD restriction; falling back to subscription status.")
		return IsSubscriber(ctx, v.channelID, userID)
	}

	userAuthorization, err := loaders.LoadUserVODAuthorization(ctx, userID, v.vodID)
	if err != nil {
		logger.WithError(err).Error("Error loading user VOD authorization; defaulting to authorized.")
		return true
	}
	if userAuthorization == nil {
		// This is warn-level since an empty pointer with no error indicates something going pretty wrong.
		logger.Warn("Received empty user VOD authorization; defaulting to authorized.")
		return true
	}

	return userAuthorization.UserIsAuthorized
}

func vodIsChannelTrailer(ctx context.Context, channelID string, vodID string) bool {
	logger := log.WithFields(log.Fields{
		"channelID": channelID,
		"vodID":     vodID,
	})

	channelTrailer, err := loaders.LoadChannelTrailer(ctx, channelID)

	if err != nil {
		logger.WithError(err).Error("Error loading channel trailer for channel. Assuming vod is not a trailer.")
		return false
	}
	if channelTrailer.GetVodId() == vodID {
		return true
	}

	return false
}
