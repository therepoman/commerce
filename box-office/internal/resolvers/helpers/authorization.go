package helpers

import (
	"context"

	"fmt"

	"code.justin.tv/commerce/box-office/config"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	"github.com/cactus/go-statsd-client/statsd"
	log "github.com/sirupsen/logrus"
)

const (
	ReasonNone           = ""
	ReasonPremiumContent = "PREMIUM_CONTENT" // Paywalled channels like OWL Premium
	ReasonGeoblocked     = "GEOBLOCKED"
)

// readability helpers for terminal points of these functions
const FORBIDDEN = true
const NOT_FORBIDDEN = false

// stats enum constants
type StatsContentType string

const (
	StatsChannel StatsContentType = "channel"
	StatsClip    StatsContentType = "clip"
	StatsVOD     StatsContentType = "vod"
)

func (s StatsContentType) ToString() string {
	return string(s)
}

// UserIsForbidden returns whether or not the user is forbidden from viewing the channel video. Usher will use this field
// to block users from viewing the channel content.
// Returns:
//   - Forbidden (bool): True if the user is forbidden from viewing the channel video. False otherwise.
//   - Reason (string): If the user is forbidden, the reason contains a code informing why the user is forbidden. If the
//                      user is not forbidden, this is an empty string.
func UserIsForbidden(ctx context.Context, cfg *config.Configuration, userID string, channelID string, origin string, stats statsd.Statter) (bool, string) {
	niohResp, err := loaders.LoadUserChannelAuthorization(ctx, userID, channelID, cfg.EnablePremiumContentPreview, origin)
	if err != nil {
		logger := log.WithError(err).WithFields(log.Fields{
			"userID":    userID,
			"channelID": channelID,
		})

		logger.Info("could not contact Nioh for authorization for channel; failing open")
		incFailOpenStat(StatsChannel, stats)
		return NOT_FORBIDDEN, ReasonNone
	}

	if niohResp.UserIsAuthorized {
		log.WithField("userID", userID).Debug("This user is authorized for the premium content via Nioh.")
		return NOT_FORBIDDEN, ReasonNone
	}

	log.WithField("userID", userID).Debug("This user is not subscribed to the premium content. Returning forbidden.")
	return FORBIDDEN, ReasonPremiumContent
}

// VODIsForbidden returns whether or not the user is forbidden from viewing the vod. Usher will use this field
// to block users from viewing the vod content.
// Returns:
//   - Forbidden (bool): True if the user is forbidden from viewing the vod. False otherwise.
//   - Reason (string): If the user is forbidden, the reason contains a code informing why the
//                      user is forbidden. If the user is not forbidden, this is an empty string.
func VODIsForbidden(ctx context.Context, userID string, vodID string, countryCode string, stats statsd.Statter) (bool, string) {
	logger := log.WithFields(log.Fields{"userID": userID, "vodID": vodID})

	if vodID == "" {
		logger.Debug("Blank vod id is not paywalled.")
		return NOT_FORBIDDEN, ReasonNone
	}

	restriction, err := loaders.LoadVODRestriction(ctx, vodID)
	if err != nil {
		logger.WithError(err).Error("Error checking vod geoblock restriction")
		incFailOpenStat(StatsVOD, stats)
		return NOT_FORBIDDEN, ReasonNone
	}

	if restriction != nil && isGeoblocked(restriction.Restriction, countryCode) {
		return FORBIDDEN, ReasonGeoblocked
	}

	vodAuthorization, err := loaders.LoadUserVODAuthorization(ctx, userID, vodID)
	if err != nil {
		logger.WithError(err).Error("Error loading user VOD authorization")
		incFailOpenStat(StatsVOD, stats)
		return NOT_FORBIDDEN, ReasonNone
	}

	if vodAuthorization.UserIsAuthorized {
		logger.Debug("User is authorized by Nioh to view vod")
		return NOT_FORBIDDEN, ReasonNone
	}

	logger.Debug("User is not authorized by Nioh to view vod")
	return FORBIDDEN, ReasonPremiumContent
}

func ClipIsForbidden(ctx context.Context, clipSlug string, countryCode string, stats statsd.Statter) (bool, string) {
	logger := log.WithFields(log.Fields{"clipSlug": clipSlug})

	if clipSlug == "" {
		logger.Debug("empty clip slug, returning not forbidden")
		return NOT_FORBIDDEN, ReasonNone
	}

	broadcastID, err := loaders.LoadClipBroadcastID(ctx, clipSlug)
	if err != nil {
		logger.WithError(err).Error("error converting clip slug to broadcast ID")
		incFailOpenStat(StatsClip, stats)
		return NOT_FORBIDDEN, ReasonNone
	}

	restriction, err := loaders.LoadBroadcastRestriction(ctx, broadcastID)
	if err != nil {
		logger.WithError(err).Error("error checking clip restriction")
		incFailOpenStat(StatsClip, stats)
		return NOT_FORBIDDEN, ReasonNone
	}

	if restriction != nil && isGeoblocked(restriction.Restriction, countryCode) {
		return FORBIDDEN, ReasonGeoblocked
	}

	return NOT_FORBIDDEN, ReasonNone
}

func isGeoblocked(restriction *nioh.Restriction, countryCode string) bool {
	// if restriction is nil there isn't a geoblock
	if restriction == nil {
		return false
	}

	if len(restriction.Geoblocks) > 0 {
		// if the user's country code is empty, default
		// to assuming that they should be geoblocked
		if countryCode == "" {
			return true
		}

		for _, g := range restriction.Geoblocks {
			for _, cc := range g.CountryCodes {
				if countryCode == cc {
					if g.Operator == nioh.Geoblock_BLOCK {
						return true
					}

					if g.Operator == nioh.Geoblock_ALLOW {
						return false
					}
				}
			}
		}
	}

	return false
}

// Emits a stat for when an authorization check fails open
func incFailOpenStat(contentType StatsContentType, stats statsd.Statter) {
	go func() {
		statName := fmt.Sprintf("resolver.access-token.authorization.failed-open.%s", contentType.ToString())
		stats.Inc(statName, 1, 1.0)
	}()
}
