package helpers

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/cb/hallpass/view"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"
	"code.justin.tv/web/users-service/client/channels"
	"code.justin.tv/web/users-service/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPrivatePermissions(t *testing.T) {
	Convey("Test PrivatePermissions", t, func() {
		isTrue := true
		isFalse := false
		Convey("Test UserHasPrivatePermissions", func() {

			Convey("video is private", func() {
				channelResp := &channels.Channel{
					PrivateVideo: true,
				}
				context := test.SetupLoaderContext(context.Background(), loaders.ChannelProperties, channelResp, nil)

				Convey("users doesnt have permission", func() {
					userResp := &models.Properties{
						Subadmin: &isFalse,
						Admin:    &isFalse,
					}
					context = test.SetupLoaderContext(context, loaders.UserByID, userResp, nil)

					permsResp := &view.GetIsEditorResponse{
						IsEditor: false,
					}
					context = test.SetupLoaderContext(context, loaders.Permissions, permsResp, nil)

					hasPerms := UserHasPrivatePermissions(context, channelID, userID)
					So(hasPerms, ShouldBeFalse)
				})
				Convey("user has permission", func() {
					userResp := &models.Properties{
						Subadmin: &isTrue,
						Admin:    &isFalse,
					}
					context = test.SetupLoaderContext(context, loaders.UserByID, userResp, nil)

					hasPerms := UserHasPrivatePermissions(context, channelID, userID)
					So(hasPerms, ShouldBeTrue)

				})
			})
			Convey("video is not private", func() {
				channelResp := &channels.Channel{
					PrivateVideo: false,
				}
				context := test.SetupLoaderContext(context.Background(), loaders.ChannelProperties, channelResp, nil)

				hasPerms := UserHasPrivatePermissions(context, channelID, userID)
				So(hasPerms, ShouldBeFalse)
			})
		})

		Convey("Test IsVideoEditableByUser", func() {

			Convey("userid is missing", func() {
				isEditable := IsVideoEditableByUser(context.Background(), channelID, "")
				So(isEditable, ShouldBeFalse)
			})

			Convey("channelid is missing", func() {
				isEditable := IsVideoEditableByUser(context.Background(), "", userID)
				So(isEditable, ShouldBeFalse)
			})

			Convey("inputs are present", func() {
				Convey("loader returns error", func() {
					context := test.SetupLoaderContext(context.Background(), loaders.UserByID, nil, errors.New("test"))

					isEditable := IsVideoEditableByUser(context, channelID, userID)
					So(isEditable, ShouldBeFalse)
				})

				Convey("loader returns admin", func() {
					userResp := &models.Properties{
						Subadmin: &isTrue,
						Admin:    &isFalse,
					}
					context := test.SetupLoaderContext(context.Background(), loaders.UserByID, userResp, nil)

					isEditable := IsVideoEditableByUser(context, channelID, userID)
					So(isEditable, ShouldBeTrue)
				})

				Convey("loader returns not admin", func() {
					userResp := &models.Properties{
						Subadmin: &isFalse,
						Admin:    &isFalse,
					}
					context := test.SetupLoaderContext(context.Background(), loaders.UserByID, userResp, nil)

					Convey("user has permission", func() {
						permsResp := &view.GetIsEditorResponse{
							IsEditor: true,
						}
						context := test.SetupLoaderContext(context, loaders.Permissions, permsResp, nil)

						isEditable := IsVideoEditableByUser(context, channelID, userID)
						So(isEditable, ShouldBeTrue)
					})

					Convey("user doesnt have permission", func() {
						permsResp := &view.GetIsEditorResponse{
							IsEditor: false,
						}
						context := test.SetupLoaderContext(context, loaders.Permissions, permsResp, nil)

						isEditable := IsVideoEditableByUser(context, channelID, userID)
						So(isEditable, ShouldBeFalse)
					})
				})
			})
		})

		Convey("Test IsVideoPrivate", func() {
			Convey("channelid is missing", func() {
				isPrivate := IsVideoPrivate(context.Background(), "")
				So(isPrivate, ShouldBeFalse)
			})

			Convey("loader returns error", func() {
				context := test.SetupLoaderContext(context.Background(), loaders.ChannelProperties, nil, errors.New("test"))
				isPrivate := IsVideoPrivate(context, channelID)
				So(isPrivate, ShouldBeFalse)
			})

			Convey("loader returns true", func() {
				channelResp := &channels.Channel{
					PrivateVideo: true,
				}
				context := test.SetupLoaderContext(context.Background(), loaders.ChannelProperties, channelResp, nil)

				isPrivate := IsVideoPrivate(context, channelID)
				So(isPrivate, ShouldBeTrue)
			})

			Convey("loader returns false", func() {
				channelResp := &channels.Channel{
					PrivateVideo: false,
				}
				context := test.SetupLoaderContext(context.Background(), loaders.ChannelProperties, channelResp, nil)

				isPrivate := IsVideoPrivate(context, channelID)
				So(isPrivate, ShouldBeFalse)
			})
		})
	})
}
