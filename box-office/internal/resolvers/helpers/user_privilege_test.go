package helpers

import (
	"context"
	"testing"

	voyager "code.justin.tv/amzn/TwitchVoyagerTwirp"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	"code.justin.tv/web/users-service/models"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	channelID = "123456"
	userID    = "567890"
)

func TestUserPrivilege(t *testing.T) {
	Convey("Test UserHasEscalatedChanSubPrivileges", t, func() {

		Convey("missing userid", func() {
			hasPrivileges := UserHasEscalatedChanSubPrivileges(context.Background(), channelID, "")
			So(hasPrivileges, ShouldEqual, false)
		})

		Convey("missing channelID", func() {
			hasPrivileges := UserHasEscalatedChanSubPrivileges(context.Background(), "", userID)
			So(hasPrivileges, ShouldEqual, false)
		})

		Convey("channel has no ticket products", func() {
			productsResp := []*substwirp.Product{}
			context := test.SetupLoaderContext(context.Background(), loaders.ChannelProducts, productsResp, nil)

			hasPrivileges := UserHasEscalatedChanSubPrivileges(context, channelID, userID)
			So(hasPrivileges, ShouldEqual, false)
		})

		Convey("channel has ticket product with no restrictions", func() {
			productsResp := []*substwirp.Product{
				{
					BitrateAccess: []string{},
				},
			}
			context := test.SetupLoaderContext(context.Background(), loaders.ChannelProducts, productsResp, nil)

			hasPrivileges := UserHasEscalatedChanSubPrivileges(context, channelID, userID)
			So(hasPrivileges, ShouldEqual, false)
		})

		Convey("channel has ticket product with restriction", func() {
			isTrue := true
			isFalse := false
			productsResp := []*substwirp.Product{
				{
					BitrateAccess: []string{"archives"},
				},
			}
			context := test.SetupLoaderContext(context.Background(), loaders.ChannelProducts, productsResp, nil)

			Convey("user is admin", func() {
				userResp := &models.Properties{
					Subadmin: &isTrue,
					Admin:    &isFalse,
				}
				context = test.SetupLoaderContext(context, loaders.UserByID, userResp, nil)

				hasPrivileges := UserHasEscalatedChanSubPrivileges(context, channelID, userID)
				So(hasPrivileges, ShouldEqual, true)
			})

			Convey("user is not admin", func() {
				userResp := &models.Properties{
					Subadmin: &isFalse,
					Admin:    &isFalse,
				}

				context = test.SetupLoaderContext(context, loaders.UserByID, userResp, nil)

				Convey("user is sub", func() {
					subsResp := &voyager.Subscription{}
					context = test.SetupLoaderContext(context, loaders.Subscriptions, subsResp, nil)

					hasPrivileges := UserHasEscalatedChanSubPrivileges(context, channelID, userID)
					So(hasPrivileges, ShouldEqual, true)
				})

				Convey("user is not sub", func() {
					context = test.SetupLoaderContext(context, loaders.Subscriptions, nil, nil)

					hasPrivileges := UserHasEscalatedChanSubPrivileges(context, channelID, userID)
					So(hasPrivileges, ShouldEqual, false)
				})
			})
		})
	})
}
