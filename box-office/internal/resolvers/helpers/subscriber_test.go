package helpers

import (
	"context"
	"errors"
	"testing"

	voyager "code.justin.tv/amzn/TwitchVoyagerTwirp"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSubscriber(t *testing.T) {
	Convey("Test IsSubscriber", t, func() {

		Convey("missing userid", func() {
			isSubscriber := IsSubscriber(context.Background(), channelID, "")
			So(isSubscriber, ShouldEqual, false)
		})

		Convey("missing channelID", func() {
			isSubscriber := IsSubscriber(context.Background(), "", userID)
			So(isSubscriber, ShouldEqual, false)
		})

		Convey("loader returns error", func() {
			context := test.SetupLoaderContext(context.Background(), loaders.Subscriptions, nil, errors.New("test"))

			isSubscriber := IsSubscriber(context, channelID, userID)
			So(isSubscriber, ShouldEqual, false)
		})

		Convey("user is sub", func() {
			subsResp := &voyager.Subscription{}
			context := test.SetupLoaderContext(context.Background(), loaders.Subscriptions, subsResp, nil)

			isSubscriber := IsSubscriber(context, channelID, userID)
			So(isSubscriber, ShouldEqual, true)
		})

		Convey("user is not sub", func() {
			context := test.SetupLoaderContext(context.Background(), loaders.Subscriptions, nil, nil)

			isSubscriber := IsSubscriber(context, channelID, userID)
			So(isSubscriber, ShouldEqual, false)
		})
	})
}
