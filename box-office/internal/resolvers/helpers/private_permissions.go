package helpers

import (
	"context"

	"code.justin.tv/commerce/box-office/internal/loaders"
	log "github.com/sirupsen/logrus"
)

// UserHasPrivatePermissions returns true if the video is private and the user has permission to view it.
// Otherwise, returns false
// Reference web/web: https://git-aws.internal.justin.tv/web/web/blob/master/app/models/concerns/user/videos_methods.rb
func UserHasPrivatePermissions(ctx context.Context, channelID string, userID string) bool {
	return IsVideoPrivate(ctx, channelID) && IsVideoEditableByUser(ctx, channelID, userID)
}

// IsVideoEditableByUser returns true if the video is editable by the user. Otherwise, returns false.
// Users who can edit a video:
//   - Channel owner
//   - Admins and Subadmins
//   - Users with explicitly granted edit permission
// Reference web/web: https://git-aws.internal.justin.tv/web/web/blob/master/engines/editors/app/interactors/editors/check_channel_editable_by_user.rb
func IsVideoEditableByUser(ctx context.Context, channelID string, userID string) bool {
	if userID == "" {
		return false
	}

	if userID == channelID {
		// channel owners can edit their own channels
		return true
	}

	isAdminOrSubadmin, err := UserIsAdminOrSubadmin(ctx, userID)
	if err != nil {
		log.Errorf("Error determining if user: %s is an admin. Returning default value. Error %+v", userID, err)
		return false
	}

	if isAdminOrSubadmin {
		log.WithField("userID", userID).Debug("This user is an admin and can edit everything.")
		return true
	}

	permissionsResponse, err := loaders.LoadPermissions(ctx, channelID, userID)
	if err != nil || permissionsResponse == nil {
		log.Errorf("Error loading permission data. Using default value. Error: %+v", err)
		return false
	}

	// users with edit permissions on the channel can edit
	return permissionsResponse.IsEditor
}

// IsVideoPrivate returns true if the video has been set to private. Otherwise, returns false.
func IsVideoPrivate(ctx context.Context, channelID string) bool {
	channelResp, err := loaders.LoadChannelProperties(ctx, channelID)
	if err != nil || channelResp == nil {
		log.Errorf("Error loading channel properties. Using default value. Error: %+v", err)
		return false
	}

	return channelResp.PrivateVideo
}
