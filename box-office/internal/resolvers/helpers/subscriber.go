package helpers

import (
	"context"

	log "github.com/sirupsen/logrus"

	"code.justin.tv/commerce/box-office/internal/loaders"
)

// IsSubscriber returns true if the user is subscribed to the channel. Otherwise, returns false.
func IsSubscriber(ctx context.Context, channelID string, userID string) bool {
	if userID == "" {
		return false // anonymous users are never subscribers
	}
	if channelID == "" {
		return false // can't be subscribed to a channel that doesn't exist
	}
	subscription, err := loaders.LoadSubscription(ctx, userID, channelID)
	if err != nil {
		log.Errorf("Error loading subscriptions. Using default value. Error: %+v", err)
		return false
	}

	return subscription != nil // if the subscription response exists, the user is subscribed
}
