package resolvers

import (
	"context"
	"testing"

	"code.justin.tv/commerce/box-office/config"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/resolvers/helpers"
	"code.justin.tv/commerce/box-office/internal/test"
	"code.justin.tv/commerce/box-office/mocks"
	borpc "code.justin.tv/commerce/box-office/rpc"
	"code.justin.tv/commerce/nioh/rpc/nioh"

	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestVODAuthorizationResolver(t *testing.T) {
	Convey("Test VODAuthorizationResolver", t, func() {
		context := context.Background()
		simpleConfig := &config.Configuration{}
		mockStatter := new(mocks.Statter)
		mockStatter.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("Test constructor", func() {
			Convey("Happy case", func() {
				tokenResolver := &VODAccessTokenResolver{}
				resolver, err := NewVODAuthorizationResolver(context, tokenResolver)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)
			})

			Convey("Bad input", func() {
				resolver, err := NewVODAuthorizationResolver(context, nil)
				So(err, ShouldNotBeNil)
				So(resolver, ShouldBeNil)
			})
		})

		Convey("Test Forbidden and Reason", func() {
			request := &borpc.GetVODAccessTokenRequest{
				UserId:      "1234",
				VodId:       "5678",
				CountryCode: "US",
			}
			auth := &nioh.GetUserAuthorizationResponse{
				UserIsAuthorized:     false,
				ResourceIsRestricted: true,
			}

			Convey("error loading vod authorization", func() {
				context = test.SetupLoaderContext(context, loaders.UserVODAuthorizations, nil, errors.New("nioh asplode"))

				tokenResolver, err := NewVODAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(tokenResolver, ShouldNotBeNil)

				resolver, err := NewVODAuthorizationResolver(context, tokenResolver)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.Forbidden(context)
				So(resp, ShouldBeFalse)

				reasonResp := resolver.Reason(context)
				So(reasonResp, ShouldEqual, helpers.ReasonNone)
			})

			Convey("vod is not paywalled", func() {
				auth.UserIsAuthorized = true
				auth.ResourceIsRestricted = false
				context = test.SetupLoaderContext(context, loaders.UserVODAuthorizations, auth, nil)

				tokenResolver, err := NewVODAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(tokenResolver, ShouldNotBeNil)

				resolver, err := NewVODAuthorizationResolver(context, tokenResolver)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.Forbidden(context)
				So(resp, ShouldBeFalse)

				reasonResp := resolver.Reason(context)
				So(reasonResp, ShouldEqual, helpers.ReasonNone)
			})

			Convey("vod is paywalled", func() {
				auth.ResourceIsRestricted = true

				Convey("user is forbidden", func() {
					auth.UserIsAuthorized = false
					context = test.SetupLoaderContext(context, loaders.UserVODAuthorizations, auth, nil)
					context = test.SetupLoaderContext(context, loaders.VodRestrictions, nil, nil)

					tokenResolver, err := NewVODAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(tokenResolver, ShouldNotBeNil)

					resolver, err := NewVODAuthorizationResolver(context, tokenResolver)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					resp := resolver.Forbidden(context)
					So(resp, ShouldBeTrue)

					reasonResp := resolver.Reason(context)
					So(reasonResp, ShouldEqual, helpers.ReasonPremiumContent)
				})

				Convey("user is not forbidden", func() {
					auth.UserIsAuthorized = true
					context = test.SetupLoaderContext(context, loaders.UserVODAuthorizations, auth, nil)

					tokenResolver, err := NewVODAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(tokenResolver, ShouldNotBeNil)

					resolver, err := NewVODAuthorizationResolver(context, tokenResolver)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					resp := resolver.Forbidden(context)
					So(resp, ShouldBeFalse)

					reasonResp := resolver.Reason(context)
					So(reasonResp, ShouldEqual, helpers.ReasonNone)
				})
			})

			Convey("vod is geoblocked", func() {
				auth.ResourceIsRestricted = true
				auth.UserIsAuthorized = true

				// when the user is not authorized to view the resource but a geoblock exists,
				// we want to return geoblock as the reason b/c that takes the highest precedence
				Convey("when the user is not authorized to view the resource", func() {
					auth.UserIsAuthorized = false
					restriction := &nioh.RestrictionResourceResponse{
						Id:   vodID,
						Type: nioh.ResourceType_VIDEO,
						Restriction: &nioh.Restriction{
							RestrictionType: nioh.Restriction_GEOBLOCK,
							Geoblocks: []*nioh.Geoblock{
								{
									Operator:     nioh.Geoblock_BLOCK,
									CountryCodes: []string{"US"},
								},
							},
						},
					}

					context = test.SetupLoaderContext(context, loaders.UserVODAuthorizations, auth, nil)
					context = test.SetupLoaderContext(context, loaders.VodRestrictions, restriction, nil)

					tokenResolver, err := NewVODAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(tokenResolver, ShouldNotBeNil)

					resolver, err := NewVODAuthorizationResolver(context, tokenResolver)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					forbidden := resolver.Forbidden(context)
					So(forbidden, ShouldBeTrue)

					reason := resolver.Reason(context)
					So(reason, ShouldEqual, helpers.ReasonGeoblocked)
				})
				// even when the user is authorized to view the resource (i.e. via a subscription),
				// geoblocks must be respected and take precedence
				Convey("even when the user is authorized to view the resource", func() {
					restriction := &nioh.RestrictionResourceResponse{
						Id:   vodID,
						Type: nioh.ResourceType_VIDEO,
						Restriction: &nioh.Restriction{
							RestrictionType: nioh.Restriction_GEOBLOCK,
							Geoblocks: []*nioh.Geoblock{
								{
									Operator:     nioh.Geoblock_BLOCK,
									CountryCodes: []string{"US"},
								},
							},
						},
					}

					context = test.SetupLoaderContext(context, loaders.UserVODAuthorizations, auth, nil)
					context = test.SetupLoaderContext(context, loaders.VodRestrictions, restriction, nil)

					tokenResolver, err := NewVODAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(tokenResolver, ShouldNotBeNil)

					resolver, err := NewVODAuthorizationResolver(context, tokenResolver)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					forbidden := resolver.Forbidden(context)
					So(forbidden, ShouldBeTrue)

					reason := resolver.Reason(context)
					So(reason, ShouldEqual, helpers.ReasonGeoblocked)
				})
			})
		})
	})
}
