package resolvers

import (
	"context"
	"errors"
	"testing"

	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	"code.justin.tv/vod/vodapi/rpc/vodapi"

	"code.justin.tv/commerce/box-office/config"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"
	"code.justin.tv/commerce/box-office/mocks"
	borpc "code.justin.tv/commerce/box-office/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestVODChanSubResolver(t *testing.T) {
	Convey("Test TestVODChanSubResolver", t, func() {
		simpleConfig := &config.Configuration{}
		mockStatter := new(mocks.Statter)
		mockStatter.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("Test constructor", func() {
			Convey("Happy case", func() {
				tokenResolver := &VODAccessTokenResolver{}
				resolver, err := NewVODChanSubResolver(context.Background(), tokenResolver)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)
			})

			Convey("Bad input", func() {
				resolver, err := NewVODChanSubResolver(context.Background(), nil)
				So(err, ShouldNotBeNil)
				So(resolver, ShouldBeNil)
			})
		})

		Convey("Test RestrictedBitrates", func() {
			Convey("vodapi fails", func() {
				request := &borpc.GetVODAccessTokenRequest{
					VodId:  vodID,
					UserId: userID,
				}

				context := test.SetupLoaderContext(context.Background(), loaders.VODByID, nil, errors.New("test"))

				tokenResolver, err := NewVODAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(tokenResolver, ShouldNotBeNil)

				resolver, err := NewVODChanSubResolver(context, tokenResolver)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.RestrictedBitrates(context)
				So(len(resp), ShouldEqual, 0)
			})

			Convey("vodapi succeeds", func() {
				vodapiResponse := &vodapi.Vod{
					OwnerId:   "12345",
					Qualities: []string{"a", "b", "c"},
				}
				context := test.SetupLoaderContext(context.Background(), loaders.VODByID, vodapiResponse, nil)

				Convey("bitrates contains archives", func() {
					request := &borpc.GetVODAccessTokenRequest{
						VodId:  vodID,
						UserId: userID,
					}

					productsResp := []*substwirp.Product{
						{
							BitrateAccess: []string{"archives"},
						},
					}
					context = test.SetupLoaderContext(context, loaders.ChannelProducts, productsResp, nil)

					tokenResolver, err := NewVODAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(tokenResolver, ShouldNotBeNil)

					resolver, err := NewVODChanSubResolver(context, tokenResolver)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					resp := resolver.RestrictedBitrates(context)
					So(len(resp), ShouldEqual, 3)
				})

				Convey("no bitrate restrictions", func() {
					request := &borpc.GetVODAccessTokenRequest{
						VodId:  vodID,
						UserId: userID,
					}

					productsResp := []*substwirp.Product{
						{
							BitrateAccess: []string{},
						},
					}
					context = test.SetupLoaderContext(context, loaders.ChannelProducts, productsResp, nil)

					tokenResolver, err := NewVODAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(tokenResolver, ShouldNotBeNil)

					resolver, err := NewVODChanSubResolver(context, tokenResolver)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					resp := resolver.RestrictedBitrates(context)
					So(len(resp), ShouldEqual, 0)
				})
			})
		})
	})
}
