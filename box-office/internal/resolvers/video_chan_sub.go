package resolvers

import (
	"context"
	"errors"

	log "github.com/sirupsen/logrus"

	"code.justin.tv/commerce/box-office/internal/resolvers/helpers"
	"code.justin.tv/commerce/box-office/token"
)

const (
	DefaultViewUntil = 1924905600 // December 30, 2030
)

var (
	defaultRestrictedBitrates = []string{}
)

// VideoChanSubResolver resolves VideoChanSub objects
type VideoChanSubResolver struct {
	TokenResolver *VideoAccessTokenResolver
}

// NewVideoChanSubResolver loads a video access token and returns a new resolver.
func NewVideoChanSubResolver(ctx context.Context, tokenResolver *VideoAccessTokenResolver) (*VideoChanSubResolver, error) {
	if tokenResolver == nil {
		return nil, errors.New("tokenResolver must not be nil")
	}
	return &VideoChanSubResolver{
		TokenResolver: tokenResolver,
	}, nil
}

// Resolve the token. Returns a resolved token
func (v *VideoChanSubResolver) Resolve(ctx context.Context) *token.VideoChanSub {
	return &token.VideoChanSub{
		RestrictedBitrates: v.RestrictedBitrates(ctx),
		ViewUntil:          v.ViewUntil(ctx),
	}
}

// RestrictedBitrates resolves the RestrictedBitrates field
// Default Value: []
func (v *VideoChanSubResolver) RestrictedBitrates(ctx context.Context) []string {
	channelID, err := v.TokenResolver.ChannelIDString(ctx)
	if err != nil || channelID == "" {
		log.Errorf("Error getting channel ID. Using default value. Error: %+v", err)
		return defaultRestrictedBitrates
	}

	userID := v.TokenResolver.UserIDString(ctx)
	return helpers.GetRestrictedBitratesForUser(ctx, channelID, userID)
}

// ViewUntil resolves the ViewUntil field
// Default value: 1924905600
func (v *VideoChanSubResolver) ViewUntil(ctx context.Context) int64 {
	return DefaultViewUntil
}
