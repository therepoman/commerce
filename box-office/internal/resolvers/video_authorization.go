package resolvers

import (
	"context"
	"errors"

	timestamp "github.com/golang/protobuf/ptypes"

	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/resolvers/helpers"
	log "github.com/sirupsen/logrus"

	"code.justin.tv/commerce/box-office/token"
)

const (
	defaultAuthorizationForbidden = false
	defaultAuthorizationReason    = ""
)

// VideoAuthorizationResolver resolves Authorization objects
type VideoAuthorizationResolver struct {
	TokenResolver *VideoAccessTokenResolver
}

// NewVideoAuthorizationResolver returns a new resolver.
func NewVideoAuthorizationResolver(ctx context.Context, tokenResolver *VideoAccessTokenResolver) (*VideoAuthorizationResolver, error) {
	if tokenResolver == nil {
		return nil, errors.New("tokenResolver must not be nil")
	}
	return &VideoAuthorizationResolver{
		TokenResolver: tokenResolver,
	}, nil
}

// Resolve the token. Returns a resolved token
func (v *VideoAuthorizationResolver) Resolve(ctx context.Context) *token.Authorization {
	return &token.Authorization{
		AccessExpiration: v.AccessExpiration(ctx),
		Forbidden:        v.Forbidden(ctx),
		Reason:           v.Reason(ctx),
	}
}

// AccessExpiration resolves the AccessExpiration field
// Default value: nil
func (v *VideoAuthorizationResolver) AccessExpiration(ctx context.Context) *int64 {
	userID := v.TokenResolver.UserIDString(ctx)
	channelID, err := v.TokenResolver.ChannelIDString(ctx)
	if err != nil {
		log.Errorf("Error loading channel for authorization. Returning default. Error: %+v", err)
		return nil
	}

	config := v.TokenResolver.config
	niohResp, err := loaders.LoadUserChannelAuthorization(ctx, userID, channelID, config.EnablePremiumContentPreview, v.GetPreviewOrigin(ctx))
	if err != nil {
		log.WithError(err).Warn("received error from Nioh when loading user channel authorization")
		return nil
	}

	accessExpiration := niohResp.GetAccessExpiration()

	if accessExpiration != nil {
		convertedAccessExpiration, err := timestamp.Timestamp(niohResp.AccessExpiration)
		if err != nil {
			log.Errorf("Error converting Nioh protobuf timestamp to epoch. Returning default. Error: %+v", err)
			return nil
		}
		unix := convertedAccessExpiration.Unix()
		return &unix
	}

	return nil
}

// Forbidden resolves the Forbidden field
// Default value: false
func (v *VideoAuthorizationResolver) Forbidden(ctx context.Context) bool {
	channelID, err := v.TokenResolver.ChannelIDString(ctx)
	if err != nil {
		log.Errorf("Error loading channel for authorization. Returning default. Error: %+v", err)
		return defaultAuthorizationForbidden
	}

	forbidden, _ := helpers.UserIsForbidden(ctx, v.TokenResolver.config, v.TokenResolver.UserIDString(ctx), channelID, v.GetPreviewOrigin(ctx), v.TokenResolver.stats)
	return forbidden
}

// Reason resolves the Reason field
// Default value: ""
func (v *VideoAuthorizationResolver) Reason(ctx context.Context) string {
	channelID, err := v.TokenResolver.ChannelIDString(ctx)
	if err != nil {
		log.Errorf("Error loading channel for authorization. Returning default. Error: %+v", err)
		return defaultAuthorizationReason
	}

	_, reason := helpers.UserIsForbidden(ctx, v.TokenResolver.config, v.TokenResolver.UserIDString(ctx), channelID, v.GetPreviewOrigin(ctx), v.TokenResolver.stats)
	return reason
}

// GetPreviewOrigin returns a scoped key for the origin of the video request
func (v *VideoAuthorizationResolver) GetPreviewOrigin(ctx context.Context) string {
	if v.TokenResolver.Platform(ctx) == nil || v.TokenResolver.PlayerType(ctx) == nil {
		return ""
	}

	if *v.TokenResolver.Platform(ctx) == "" || *v.TokenResolver.PlayerType(ctx) == "" {
		return ""
	}

	return *v.TokenResolver.Platform(ctx) + ":" + *v.TokenResolver.PlayerType(ctx)
}
