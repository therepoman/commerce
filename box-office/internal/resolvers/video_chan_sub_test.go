package resolvers

import (
	"context"
	"errors"
	"testing"

	voyager "code.justin.tv/amzn/TwitchVoyagerTwirp"
	"code.justin.tv/commerce/box-office/config"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"
	"code.justin.tv/commerce/box-office/mocks"
	borpc "code.justin.tv/commerce/box-office/rpc"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	"code.justin.tv/web/users-service/models"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestVideoChanSubResolver(t *testing.T) {
	Convey("Test TestVideoChanSubResolver", t, func() {
		simpleConfig := &config.Configuration{}
		mockStatter := new(mocks.Statter)
		mockStatter.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		Convey("Test constructor", func() {
			Convey("Happy case", func() {
				tokenResolver := &VideoAccessTokenResolver{}
				resolver, err := NewVideoChanSubResolver(context.Background(), tokenResolver)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)
			})

			Convey("Bad input", func() {
				resolver, err := NewVideoChanSubResolver(context.Background(), nil)
				So(err, ShouldNotBeNil)
				So(resolver, ShouldBeNil)
			})
		})

		Convey("Test RestrictedBitrates", func() {
			Convey("channelID fails", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
				}

				context := test.SetupLoaderContext(context.Background(), loaders.UserByLogin, nil, errors.New("test"))

				tokenResolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(tokenResolver, ShouldNotBeNil)

				resolver, err := NewVideoChanSubResolver(context, tokenResolver)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.RestrictedBitrates(context)
				So(len(resp), ShouldEqual, 0)
			})

			Convey("channelID in request", func() {
				Convey("received empty channel products", func() {
					request := &borpc.GetVideoAccessTokenRequest{
						ChannelId: channelID,
					}

					productsResp := []*substwirp.Product{}
					context := test.SetupLoaderContext(context.Background(), loaders.ChannelProducts, productsResp, nil)

					tokenResolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(tokenResolver, ShouldNotBeNil)

					resolver, err := NewVideoChanSubResolver(context, tokenResolver)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					resp := resolver.RestrictedBitrates(context)
					So(len(resp), ShouldEqual, 0)
				})
			})

			Convey("received nil bitrates", func() {
				productsResp := []*substwirp.Product{
					{
						BitrateAccess: nil,
					},
				}
				context := test.SetupLoaderContext(context.Background(), loaders.ChannelProducts, productsResp, nil)

				Convey("with anonymous user", func() {
					request := &borpc.GetVideoAccessTokenRequest{
						ChannelId: channelID,
					}

					tokenResolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(tokenResolver, ShouldNotBeNil)

					resolver, err := NewVideoChanSubResolver(context, tokenResolver)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					resp := resolver.RestrictedBitrates(context)
					So(resp, ShouldNotBeNil)
					So(len(resp), ShouldEqual, 0)
				})
			})

			Convey("received populated bitrates", func() {
				productsResp := []*substwirp.Product{
					{
						BitrateAccess: []string{"archives"},
					},
				}
				context := test.SetupLoaderContext(context.Background(), loaders.ChannelProducts, productsResp, nil)

				Convey("with anonymous user", func() {
					request := &borpc.GetVideoAccessTokenRequest{
						ChannelId: channelID,
					}

					tokenResolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(tokenResolver, ShouldNotBeNil)

					resolver, err := NewVideoChanSubResolver(context, tokenResolver)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					resp := resolver.RestrictedBitrates(context)
					So(len(resp), ShouldEqual, 1)
				})

				Convey("with userID in the request", func() {
					request := &borpc.GetVideoAccessTokenRequest{
						ChannelId: channelID,
						UserId:    userID,
					}
					isTrue := true
					isFalse := false

					Convey("with error loading user", func() {
						context = test.SetupLoaderContext(context, loaders.UserByID, nil, errors.New("test"))

						tokenResolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
						So(err, ShouldBeNil)
						So(tokenResolver, ShouldNotBeNil)

						resolver, err := NewVideoChanSubResolver(context, tokenResolver)
						So(err, ShouldBeNil)
						So(resolver, ShouldNotBeNil)

						resp := resolver.RestrictedBitrates(context)
						So(len(resp), ShouldEqual, 1)
					})

					Convey("with healthy user response", func() {
						Convey("user is subadmin", func() {
							userResp := &models.Properties{
								Subadmin: &isTrue,
								Admin:    &isFalse,
							}
							context = test.SetupLoaderContext(context, loaders.UserByID, userResp, nil)

							tokenResolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
							So(err, ShouldBeNil)
							So(tokenResolver, ShouldNotBeNil)

							resolver, err := NewVideoChanSubResolver(context, tokenResolver)
							So(err, ShouldBeNil)
							So(resolver, ShouldNotBeNil)

							resp := resolver.RestrictedBitrates(context)
							So(len(resp), ShouldEqual, 0)
						})

						Convey("user is subscribed", func() {
							userResp := &models.Properties{
								Subadmin: &isFalse,
								Admin:    &isFalse,
							}
							context = test.SetupLoaderContext(context, loaders.UserByID, userResp, nil)

							subsResp := &voyager.Subscription{}
							context = test.SetupLoaderContext(context, loaders.Subscriptions, subsResp, nil)

							tokenResolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
							So(err, ShouldBeNil)
							So(tokenResolver, ShouldNotBeNil)

							resolver, err := NewVideoChanSubResolver(context, tokenResolver)
							So(err, ShouldBeNil)
							So(resolver, ShouldNotBeNil)

							resp := resolver.RestrictedBitrates(context)
							So(len(resp), ShouldEqual, 0)
						})

						Convey("user is not subscribed not admin", func() {
							userResp := &models.Properties{
								Subadmin: &isFalse,
								Admin:    &isFalse,
							}
							context = test.SetupLoaderContext(context, loaders.UserByID, userResp, nil)

							context = test.SetupLoaderContext(context, loaders.Subscriptions, nil, nil)

							tokenResolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
							So(err, ShouldBeNil)
							So(tokenResolver, ShouldNotBeNil)

							resolver, err := NewVideoChanSubResolver(context, tokenResolver)
							So(err, ShouldBeNil)
							So(resolver, ShouldNotBeNil)

							resp := resolver.RestrictedBitrates(context)
							So(len(resp), ShouldEqual, 1)
						})
					})
				})
			})
		})

		Convey("Test ViewUntil", func() {
			Convey("happy case", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelId: channelID,
				}

				tokenResolver, err := NewVideoAccessTokenResolver(context.Background(), simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(tokenResolver, ShouldNotBeNil)

				resolver, err := NewVideoChanSubResolver(context.Background(), tokenResolver)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.ViewUntil(context.Background())
				So(resp, ShouldEqual, DefaultViewUntil)
			})
		})
	})
}
