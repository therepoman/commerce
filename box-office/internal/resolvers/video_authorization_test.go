package resolvers

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/nioh/rpc/nioh"

	"code.justin.tv/commerce/box-office/internal/resolvers/helpers"

	"code.justin.tv/commerce/box-office/config"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/test"
	"code.justin.tv/commerce/box-office/mocks"
	borpc "code.justin.tv/commerce/box-office/rpc"
	timestamp "github.com/golang/protobuf/ptypes"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	paywalledChannel = "123"
)

func TestVideoAuthorizationResolver(t *testing.T) {
	Convey("Test VideoAuthorizationResolver", t, func() {
		simpleConfig := &config.Configuration{}
		mockStatter := new(mocks.Statter)
		mockStatter.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		context := context.Background()
		Convey("Test constructor", func() {
			Convey("Happy case", func() {
				tokenResolver := &VideoAccessTokenResolver{}
				resolver, err := NewVideoAuthorizationResolver(context, tokenResolver)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)
			})

			Convey("Bad input", func() {
				resolver, err := NewVideoAuthorizationResolver(context, nil)
				So(err, ShouldNotBeNil)
				So(resolver, ShouldBeNil)
			})
		})

		Convey("Test Forbidden and Reason", func() {
			Convey("channelID fails", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelName: channelName,
				}

				context := test.SetupLoaderContext(context, loaders.UserByLogin, nil, errors.New("test"))

				tokenResolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(tokenResolver, ShouldNotBeNil)

				resolver, err := NewVideoAuthorizationResolver(context, tokenResolver)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.Forbidden(context)
				So(resp, ShouldBeFalse)

				reasonResp := resolver.Reason(context)
				So(reasonResp, ShouldEqual, helpers.ReasonNone)
			})

			Convey("channel is not  paywalled", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelId: "5678",
				}

				tokenResolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
				So(err, ShouldBeNil)
				So(tokenResolver, ShouldNotBeNil)

				resolver, err := NewVideoAuthorizationResolver(context, tokenResolver)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				resp := resolver.Forbidden(context)
				So(resp, ShouldBeFalse)

				reasonResp := resolver.Reason(context)
				So(reasonResp, ShouldEqual, helpers.ReasonNone)
			})

			Convey("channel is paywalled", func() {
				request := &borpc.GetVideoAccessTokenRequest{
					ChannelId: paywalledChannel,
				}
				niohResp := &nioh.GetUserAuthorizationResponse{}

				Convey("user is forbidden", func() {
					request.UserId = userID
					context = test.SetupLoaderContext(context, loaders.UserChannelAuthorizations, niohResp, nil)

					tokenResolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(tokenResolver, ShouldNotBeNil)

					resolver, err := NewVideoAuthorizationResolver(context, tokenResolver)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					resp := resolver.Forbidden(context)
					So(resp, ShouldBeTrue)

					reasonResp := resolver.Reason(context)
					So(reasonResp, ShouldEqual, helpers.ReasonPremiumContent)
				})

				Convey("user is not forbidden", func() {
					request.UserId = userID
					niohResp.UserIsAuthorized = true
					context = test.SetupLoaderContext(context, loaders.UserChannelAuthorizations, niohResp, nil)

					tokenResolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
					So(err, ShouldBeNil)
					So(tokenResolver, ShouldNotBeNil)

					resolver, err := NewVideoAuthorizationResolver(context, tokenResolver)
					So(err, ShouldBeNil)
					So(resolver, ShouldNotBeNil)

					resp := resolver.Forbidden(context)
					So(resp, ShouldBeFalse)

					reasonResp := resolver.Reason(context)
					So(reasonResp, ShouldEqual, helpers.ReasonNone)

					accessExpirationResp := resolver.AccessExpiration(context)
					So(accessExpirationResp, ShouldBeNil)

					Convey("user is using preview", func() {
						now := time.Now()
						accessExpiration, _ := timestamp.TimestampProto(now)

						niohResp.AccessExpiration = accessExpiration
						context = test.SetupLoaderContext(context, loaders.UserChannelAuthorizations, niohResp, nil)

						tokenResolver, err := NewVideoAccessTokenResolver(context, simpleConfig, request, mockStatter)
						So(err, ShouldBeNil)
						So(tokenResolver, ShouldNotBeNil)

						resolver, err := NewVideoAuthorizationResolver(context, tokenResolver)
						So(err, ShouldBeNil)
						So(resolver, ShouldNotBeNil)

						resp := resolver.Forbidden(context)
						So(resp, ShouldBeFalse)

						reasonResp := resolver.Reason(context)
						So(reasonResp, ShouldEqual, helpers.ReasonNone)

						accessExpirationResp := resolver.AccessExpiration(context)
						So(*accessExpirationResp, ShouldEqual, now.Unix())
					})
				})
			})
		})
	})
}
