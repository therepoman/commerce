package resolvers

import (
	"context"
	"testing"

	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/resolvers/helpers"
	"code.justin.tv/commerce/box-office/internal/test"
	"code.justin.tv/commerce/box-office/mocks"
	borpc "code.justin.tv/commerce/box-office/rpc"
	"code.justin.tv/commerce/nioh/rpc/nioh"

	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestClipAuthorizationResolver(t *testing.T) {
	Convey("test ClipAuthorizationResolver", t, func() {
		ctx := context.Background()
		mockStatter := new(mocks.Statter)
		mockStatter.On("Inc", mock.Anything, mock.Anything, mock.Anything).Return(nil)

		request := &borpc.GetClipAccessTokenRequest{
			ClipSlug:    testClipSlug,
			CountryCode: "US",
		}

		Convey("test constructor", func() {
			Convey("happy case", func() {
				resolver, err := NewClipAuthorizationResolver(ctx, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)
			})

			Convey("when clip slug is empty", func() {
				request.ClipSlug = ""

				resolver, err := NewClipAuthorizationResolver(ctx, request, mockStatter)
				So(err, ShouldNotBeNil)
				So(resolver, ShouldBeNil)
			})
		})

		Convey("test fields resolve", func() {
			Convey("when clip is geoblocked", func() {
				niohGeoblock := &nioh.RestrictionResourceResponse{
					Id:   testClipBroadcastID,
					Type: nioh.ResourceType_BROADCAST,
					Restriction: &nioh.Restriction{
						RestrictionType: nioh.Restriction_GEOBLOCK,
						Geoblocks: []*nioh.Geoblock{
							{
								Operator:     nioh.Geoblock_BLOCK,
								CountryCodes: []string{"US"},
							},
						},
					},
				}
				ctx = test.SetupLoaderContext(ctx, loaders.ClipBroadcastID, &testClipBroadcastID, nil)
				ctx = test.SetupLoaderContext(ctx, loaders.BroadcastRestrictions, niohGeoblock, nil)

				resolver, err := NewClipAuthorizationResolver(ctx, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				forbidden := resolver.Forbidden(ctx)
				So(forbidden, ShouldBeTrue)

				reason := resolver.Reason(ctx)
				So(reason, ShouldEqual, helpers.ReasonGeoblocked)
			})

			Convey("when clip is not geoblocked", func() {
				niohResp := &nioh.RestrictionResourceResponse{
					Id:   testClipBroadcastID,
					Type: nioh.ResourceType_BROADCAST,
				}

				ctx = test.SetupLoaderContext(ctx, loaders.ClipBroadcastID, &testClipBroadcastID, nil)
				ctx = test.SetupLoaderContext(ctx, loaders.BroadcastRestrictions, niohResp, nil)

				resolver, err := NewClipAuthorizationResolver(ctx, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				forbidden := resolver.Forbidden(ctx)
				So(forbidden, ShouldBeFalse)

				reason := resolver.Reason(ctx)
				So(reason, ShouldEqual, helpers.ReasonNone)
			})

			Convey("when loading clip broadcast ID fails", func() {
				niohResp := &nioh.RestrictionResourceResponse{
					Id:   testClipBroadcastID,
					Type: nioh.ResourceType_BROADCAST,
				}

				ctx = test.SetupLoaderContext(ctx, loaders.ClipBroadcastID, nil, errors.New("loader error"))
				ctx = test.SetupLoaderContext(ctx, loaders.BroadcastRestrictions, niohResp, nil)

				resolver, err := NewClipAuthorizationResolver(ctx, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				forbidden := resolver.Forbidden(ctx)
				So(forbidden, ShouldBeFalse)

				reason := resolver.Reason(ctx)
				So(reason, ShouldEqual, helpers.ReasonNone)
			})

			Convey("when loading nioh restriction fails", func() {
				ctx = test.SetupLoaderContext(ctx, loaders.ClipBroadcastID, &testClipBroadcastID, nil)
				ctx = test.SetupLoaderContext(ctx, loaders.BroadcastRestrictions, nil, errors.New("loader error"))

				resolver, err := NewClipAuthorizationResolver(ctx, request, mockStatter)
				So(err, ShouldBeNil)
				So(resolver, ShouldNotBeNil)

				forbidden := resolver.Forbidden(ctx)
				So(forbidden, ShouldBeFalse)

				reason := resolver.Reason(ctx)
				So(reason, ShouldEqual, helpers.ReasonNone)
			})
		})
	})
}
