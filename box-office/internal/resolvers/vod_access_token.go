package resolvers

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"code.justin.tv/commerce/box-office/config"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/resolvers/helpers"
	borpc "code.justin.tv/commerce/box-office/rpc"
	"code.justin.tv/commerce/box-office/token"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/vod/vodapi/rpc/vodapi"
	vodapi_utils "code.justin.tv/vod/vodapi/rpc/vodapi/utils"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

const (
	vodExpirationOffset = 20 * time.Hour
)

// VODAccessTokenResolver resolves VODAccessToken objects
type VODAccessTokenResolver struct {
	request *borpc.GetVODAccessTokenRequest
	stats   statsd.Statter
}

// NewVODAccessTokenResolver loads a VOD access token and returns a new resolver.
func NewVODAccessTokenResolver(ctx context.Context, config *config.Configuration, request *borpc.GetVODAccessTokenRequest, stats statsd.Statter) (*VODAccessTokenResolver, error) {
	if request == nil {
		return nil, errors.New("Request must not be nil")
	}

	return &VODAccessTokenResolver{
		request: request,
		stats:   stats,
	}, nil
}

// Resolve returns a resolved token
func (v *VODAccessTokenResolver) Resolve(ctx context.Context) (*token.VODAccessToken, error) {
	err := v.validateVOD(ctx)
	if err != nil {
		return nil, err
	}

	// Resolve fields concurrently below.
	// v.X(ctx) function calls will never return an error. If there is a timeout or error in a
	// downstream, v.X(ctx) will return a hardcoded default.
	// =============================================

	// Authorization
	authorization := make(chan token.Authorization, 1)
	go func() {
		authorization <- v.Authorization(ctx)
	}()

	// ChanSub
	chanSub := make(chan token.VODChanSub, 1)
	go func() {
		chanSub <- v.ChanSub(ctx)
	}()

	// DeviceID
	deviceID := make(chan *string, 1)
	go func() {
		deviceID <- v.DeviceID(ctx)
	}()

	// Expires
	expires := make(chan int64, 1)
	go func() {
		expires <- v.Expires(ctx)
	}()

	// HTTPSRequired
	httpsRequired := make(chan bool, 1)
	go func() {
		httpsRequired <- v.HTTPSRequired(ctx)
	}()

	// Privileged
	privileged := make(chan bool, 1)
	go func() {
		privileged <- v.Privileged(ctx)
	}()

	// UserID
	userID := make(chan *int64, 1)
	go func() {
		userID <- v.UserID(ctx)
	}()

	// Version
	version := make(chan int, 1)
	go func() {
		version <- v.Version(ctx)
	}()

	// VodID
	vodID := make(chan *int64, 1)
	go func() {
		vodID <- v.VODID(ctx)
	}()

	// Wait on all channels before return result.
	return &token.VODAccessToken{
		Authorization: <-authorization,
		ChanSub:       <-chanSub,
		DeviceID:      <-deviceID,
		Expires:       <-expires,
		HTTPSRequired: <-httpsRequired,
		Privileged:    <-privileged,
		UserID:        <-userID,
		Version:       <-version,
		VODID:         <-vodID,
	}, nil
}

// Authorization resolves the Authorization field
func (v *VODAccessTokenResolver) Authorization(ctx context.Context) token.Authorization {
	resolver, err := NewVODAuthorizationResolver(ctx, v)
	if err != nil {
		log.Errorf("Error resolving Authorization. Using default value. Error: %+v", err)
		return token.Authorization{}
	}

	return *resolver.Resolve(ctx)
}

// ChanSub resolves the ChanSub field
func (v *VODAccessTokenResolver) ChanSub(ctx context.Context) token.VODChanSub {
	resolver, err := NewVODChanSubResolver(ctx, v)
	if err != nil {
		log.Errorf("Error resolving VideoChanSub. Using default value. Error: %+v", err)
		return token.VODChanSub{}
	}

	return *resolver.Resolve(ctx)
}

func (v *VODAccessTokenResolver) MediaType(ctx context.Context) string {
	if v.request.VodId == "" {
		return defaultMediaType
	}

	vodID := v.VODIDString(ctx)
	vod, err := loaders.LoadVODByID(ctx, vodID)
	if err != nil || vod == nil {
		return defaultMediaType
	}

	return vod.MediaType
}

func (v *VODAccessTokenResolver) BroadcastingSoftware(ctx context.Context) string {
	if v.request.VodId == "" {
		return defaultGame
	}

	vodID := v.VODIDString(ctx)
	vod, err := loaders.LoadVODByID(ctx, vodID)
	if err != nil || vod == nil {
		return defaultGame
	}

	if vod.BroadcasterSoftware != nil {
		return vod.BroadcasterSoftware.Value
	}

	return defaultGame
}

// DeviceID resolves the DeviceID field
// Default value: nil
func (v *VODAccessTokenResolver) DeviceID(ctx context.Context) *string {
	if v.request.DeviceId == "" {
		return nil
	}
	return &v.request.DeviceId
}

// Expires resolves the Expires field
// Default value: timestamp 20 hours from now
func (v *VODAccessTokenResolver) Expires(ctx context.Context) int64 {
	expiration := time.Now().Add(vodExpirationOffset)
	return expiration.Unix()
}

// HTTPSRequired resolves the HTTPSRequired field
// Default value: false
func (v *VODAccessTokenResolver) HTTPSRequired(ctx context.Context) bool {
	return v.request.NeedHttps
}

// Privileged resolves the Privileged field
// Default value: false
func (v *VODAccessTokenResolver) Privileged(ctx context.Context) bool {
	vodID := v.VODIDString(ctx)
	vod, err := loaders.LoadVODByID(ctx, vodID)
	if err != nil || vod == nil {
		return false
	}

	channelID := vod.OwnerId
	userID := v.UserIDString(ctx)
	return helpers.UserHasEscalatedChanSubPrivileges(ctx, channelID, userID)
}

// UserID resolves the UserID field as an *int64
// Default value: null
func (v *VODAccessTokenResolver) UserID(ctx context.Context) *int64 {
	userIDInt, err := strconv.ParseInt(v.UserIDString(ctx), 10, 64)
	if err != nil {
		return nil
	}
	return &userIDInt
}

// UserIDString resolves the UserID field as a string
// Default value: ""
func (v *VODAccessTokenResolver) UserIDString(ctx context.Context) string {
	return v.request.UserId
}

// Version resolves the Version field
// Default value: 2
func (v *VODAccessTokenResolver) Version(ctx context.Context) int {
	return TokenVersion
}

// VODID resolves the VODID field
// Default value: null
func (v *VODAccessTokenResolver) VODID(ctx context.Context) *int64 {
	vodID := v.VODIDString(ctx)
	vod, err := loaders.LoadVODByID(ctx, vodID)
	if err != nil {
		return nil
	}
	vodIDInt, err := strconv.ParseInt(vod.Id, 10, 64)
	if err != nil {
		return nil
	}
	return &vodIDInt
}

// VODIDString resolves the VODID field as a string
// Default value: ""
func (v *VODAccessTokenResolver) VODIDString(ctx context.Context) string {
	return v.request.VodId
}

func (v *VODAccessTokenResolver) validateVOD(ctx context.Context) error {
	if v.request.VodId == "" {
		return &twitchclient.Error{
			StatusCode: http.StatusBadRequest,
			Message:    "vod_id is a required argument",
		}
	}

	vodID := v.VODIDString(ctx)
	vod, err := loaders.LoadVODByID(ctx, vodID)
	if err != nil {
		return err
	}

	if vod == nil {
		return &twitchclient.Error{
			StatusCode: http.StatusNotFound,
			Message:    fmt.Sprintf("vod %s not found", vodID),
		}
	}

	userID := v.UserIDString(ctx)
	canViewVOD := v.canUserViewVOD(ctx, vod, userID)
	if !canViewVOD {
		log.WithField("userID", userID).Debug("User is not allowed to view this VOD")
		return &twitchclient.Error{StatusCode: http.StatusNotFound, Message: "VOD not found"}
	}

	// VOD is viewable
	return nil
}

// canUserViewVOD Returns true if a user is allowed to view the VOD. Otherwise, returns false.
func (v *VODAccessTokenResolver) canUserViewVOD(ctx context.Context, vod *vodapi.Vod, userID string) bool {
	if vod == nil {
		log.Debug("This VOD doesn't exist, so it is not viewable")
		return false
	}

	deleted := vodapi_utils.FromProtobufBoolValue(vod.Deleted)
	if deleted != nil && *deleted == true {
		// the vod has been deleted, never allow users to view it.
		log.WithField("vod_id", vod.Id).Debug("This VOD has been deleted, so it is not viewable")
		return false
	}

	if vod.Viewable != vodapi.VodViewable_PRIVATE {
		log.WithField("vod_id", vod.Id).Debug("This VOD is publicly viewable")
		return true
	}

	if userID == "" {
		return false
	}

	channelIDStr := vod.OwnerId
	return helpers.IsVideoEditableByUser(ctx, channelIDStr, userID)
}
