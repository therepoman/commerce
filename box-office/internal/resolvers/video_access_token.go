package resolvers

import (
	"context"
	"errors"
	"net"
	"strconv"
	"strings"
	"time"

	"code.justin.tv/commerce/box-office/config"

	log "github.com/sirupsen/logrus"

	"code.justin.tv/commerce/box-office/internal/geoblock"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/resolvers/helpers"
	"code.justin.tv/web/users-service/client"

	"code.justin.tv/commerce/box-office/token"

	borpc "code.justin.tv/commerce/box-office/rpc"
	"github.com/cactus/go-statsd-client/statsd"
	gerrors "github.com/pkg/errors"
)

const (
	// TokenVersion represents the current token version being returned
	TokenVersion = 2

	defaultChannel        = ""
	defaultChannelID      = ""
	defaultGame           = ""
	defaultHasAdblock     = false
	defaultMediaType      = ""
	videoExpirationOffset = 20 * time.Minute
)

// VideoAccessTokenResolver resolves VideoAccessToken objects
type VideoAccessTokenResolver struct {
	config  *config.Configuration
	request *borpc.GetVideoAccessTokenRequest
	stats   statsd.Statter
}

// NewVideoAccessTokenResolver loads a video access token and returns a new resolver.
func NewVideoAccessTokenResolver(ctx context.Context, config *config.Configuration, request *borpc.GetVideoAccessTokenRequest, stats statsd.Statter) (*VideoAccessTokenResolver, error) {
	if config == nil {
		return nil, errors.New("Config must not be nil")
	}

	if request == nil {
		return nil, errors.New("Request must not be nil")
	}

	return &VideoAccessTokenResolver{
		config:  config,
		request: request,
		stats:   stats,
	}, nil
}

// Resolve the token. Returns a resolved token
func (v *VideoAccessTokenResolver) Resolve(ctx context.Context) (*token.VideoAccessToken, error) {
	// Validation on the channel inputs happens upstream. If both channel inputs are sent to us in the request
	// we will assume they are valid. If only one is sent, we will lookup the other. If both are missing, return an error
	if v.request.ChannelName == "" && v.request.ChannelId == "" {
		return nil, &client.UserNotFoundError{} // return this error type to ensure that a 404 is returned to the user
	}

	channel, err := v.Channel(ctx)
	if err != nil {
		return nil, err
	}

	channelID, err := v.ChannelID(ctx)
	if err != nil {
		return nil, err
	}

	adblock := make(chan bool, 1)
	go func() { adblock <- v.Adblock(ctx) }()

	authorization := make(chan token.Authorization, 1)
	go func() { authorization <- v.Authorization(ctx) }()

	blackoutEnabled := make(chan bool, 1)
	go func() { blackoutEnabled <- v.BlackoutEnabled(ctx) }()

	chanSub := make(chan token.VideoChanSub, 1)
	go func() { chanSub <- v.ChanSub(ctx) }()

	channelIsGeoBlocked := make(chan bool, 1)
	go func() { channelIsGeoBlocked <- v.ChannelIsGeoBlocked(ctx) }()

	geoblockReason := make(chan string, 1)
	go func() { geoblockReason <- v.GeoblockReason(ctx) }()

	deviceID := make(chan *string, 1)
	go func() { deviceID <- v.DeviceID(ctx) }()

	expires := make(chan int64, 1)
	go func() { expires <- v.Expires(ctx) }()

	extendedHistoryAllowed := make(chan bool, 1)
	go func() { extendedHistoryAllowed <- v.ExtendedHistoryAllowed(ctx) }()

	game := make(chan string, 1)
	go func() { game <- v.Game(ctx) }()

	hideAds := make(chan bool, 1)
	go func() { hideAds <- v.HideAds(ctx) }()

	httpsRequired := make(chan bool, 1)
	go func() { httpsRequired <- v.HTTPSRequired(ctx) }()

	mature := make(chan bool, 1)
	go func() { mature <- v.Mature(ctx) }()

	partner := make(chan bool, 1)
	go func() { partner <- v.Partner(ctx) }()

	platform := make(chan *string, 1)
	go func() { platform <- v.Platform(ctx) }()

	playerType := make(chan *string, 1)
	go func() { playerType <- v.PlayerType(ctx) }()

	private := make(chan token.Private, 1)
	go func() { private <- v.Private(ctx) }()

	privileged := make(chan bool, 1)
	go func() { privileged <- v.Privileged(ctx) }()

	role := make(chan string, 1)
	go func() { role <- v.Role(ctx) }()

	serverAds := make(chan bool, 1)
	go func() { serverAds <- v.ServerAds(ctx) }()

	showAds := make(chan bool, 1)
	go func() { showAds <- v.ShowAds(ctx) }()

	subscriber := make(chan bool, 1)
	go func() { subscriber <- v.Subscriber(ctx) }()

	turbo := make(chan bool, 1)
	go func() { turbo <- v.Turbo(ctx) }()

	userID := make(chan *int64, 1)
	go func() { userID <- v.UserID(ctx) }()

	userIP := make(chan string, 1)
	go func() { userIP <- v.UserIP(ctx) }()

	version := make(chan int, 1)
	go func() { version <- v.Version(ctx) }()

	return &token.VideoAccessToken{
		Adblock:                <-adblock,
		Authorization:          <-authorization,
		BlackoutEnabled:        <-blackoutEnabled,
		Channel:                channel,
		ChannelID:              channelID,
		ChanSub:                <-chanSub,
		ChannelIsGeoBlocked:    <-channelIsGeoBlocked,
		GeoblockReason:         <-geoblockReason,
		DeviceID:               <-deviceID,
		Expires:                <-expires,
		ExtendedHistoryAllowed: <-extendedHistoryAllowed,
		Game:                   <-game,
		HideAds:                <-hideAds,
		HTTPSRequired:          <-httpsRequired,
		Mature:                 <-mature,
		Partner:                <-partner,
		Platform:               <-platform,
		PlayerType:             <-playerType,
		Private:                <-private,
		Privileged:             <-privileged,
		Role:                   <-role,
		ServerAds:              <-serverAds,
		ShowAds:                <-showAds,
		Subscriber:             <-subscriber,
		Turbo:                  <-turbo,
		UserID:                 <-userID,
		UserIP:                 <-userIP,
		Version:                <-version,
	}, nil
}

// Adblock resolves the Adblock field
// Default value: false
// Deprecated: This field is no longer used in downstream services - ads services
//             have their own mechanism for determining whether users have adblock,
//             so we no longer need to send this field downstream
func (v *VideoAccessTokenResolver) Adblock(ctx context.Context) bool {
	return defaultHasAdblock
}

// Authorization resolves the Authorization field
func (v *VideoAccessTokenResolver) Authorization(ctx context.Context) token.Authorization {
	resolver, err := NewVideoAuthorizationResolver(ctx, v)
	if err != nil {
		log.Errorf("Error resolving Authorization. Using default value. Error: %+v", err)
		return token.Authorization{}
	}

	return *resolver.Resolve(ctx)
}

// BlackoutEnabled resolves the BlackoutEnabled field. This value is set to true only when zipcode level geoblocking is set
func (v *VideoAccessTokenResolver) BlackoutEnabled(ctx context.Context) bool {
	geoblockMap := v.config.GeoblockedChannels
	channelName, err := v.Channel(ctx)
	if err != nil {
		log.Errorf("Error resolving channel for geoblock blackout check. Using default value. Error: %+v", err)
		return false
	}

	// If there is not Geoblock config for this channel, we should not Geoblock this user
	geoblockEntry, exists := geoblockMap[channelName]
	if !exists {
		log.WithField("channelName", channelName).Debug("No geoblock entry exists for this channel")
		return false
	}

	now := time.Now()
	if geoblockEntry.BlackoutEnabled(now) {
		return true
	}

	return false
}

// Channel resolves the Channel field
// Default value: empty string
// Critical field: Without a valid channel in the token, the call to usher will fail
// If an error occurs while populating this field, a UserNotFound error is returned
// to give the user a 404.
func (v *VideoAccessTokenResolver) Channel(ctx context.Context) (string, error) {
	if v.request.ChannelName != "" {
		return strings.ToLower(v.request.ChannelName), nil
	}

	// Channel name wasn't in the request, so look it up instead.
	userResponse, err := loaders.LoadUserByID(ctx, v.request.ChannelId)
	if err != nil {
		rootErr := gerrors.Cause(err)
		switch rootErr.(type) {
		case *client.UserNotFoundError:
			return defaultChannel, err
		default:
			log.Errorf("Error loading user data. Returning not found. Error: %+v", err)
			return defaultChannel, &client.UserNotFoundError{}
		}
	}

	if userResponse == nil || userResponse.Login == nil {
		log.Warn("Received empty response from LoadUserByID. Returning not found.")
		return defaultChannel, &client.UserNotFoundError{}
	}

	return strings.ToLower(*userResponse.Login), nil
}

// ChannelID resolves the ChannelID field as a *int64
// Default value: nil
// Critical field: Without a valid channel ID in the token, the call to usher will fail.
// If an error occurs while populating this field, a UserNotFound error is returned
// to give the user a 404.
func (v *VideoAccessTokenResolver) ChannelID(ctx context.Context) (*int64, error) {
	channelID, err := v.ChannelIDString(ctx)
	if err != nil {
		return nil, err
	}
	channelIDInt, err := strconv.ParseInt(channelID, 10, 64)
	if err != nil {
		log.Errorf("Error parsing channelID from string. Returning 404. %+v", err)
		return nil, &client.UserNotFoundError{}
	}
	return &channelIDInt, nil
}

// ChannelIDString resolves the ChannelID field as a string
// Default value: ""
// Critical field: Without a valid channel ID in the token, the call to usher will fail.
// If an error occurs while populating this field, a UserNotFound error is returned
// to give the user a 404.
func (v *VideoAccessTokenResolver) ChannelIDString(ctx context.Context) (string, error) {
	if v.request.ChannelId != "" {
		return v.request.ChannelId, nil
	}

	// Channel ID wasn't in the request, so look it up instead.
	userResponse, err := loaders.LoadUserByLogin(ctx, v.request.ChannelName)
	if err != nil {
		rootErr := gerrors.Cause(err)
		switch rootErr.(type) {
		case *client.UserNotFoundError:
			return defaultChannelID, err
		default:
			log.Errorf("Error loading user data. Returning not found. Error: %+v", err)
			return defaultChannelID, &client.UserNotFoundError{}
		}
	}

	if userResponse == nil {
		log.Warn("Received empty response from LoadUserByLogin. Returning not found.")
		return defaultChannelID, &client.UserNotFoundError{}
	}

	return userResponse.ID, nil
}

// ChanSub resolves the ChanSub field
func (v *VideoAccessTokenResolver) ChanSub(ctx context.Context) token.VideoChanSub {
	resolver, err := NewVideoChanSubResolver(ctx, v)
	if err != nil {
		log.Errorf("Error resolving VideoChanSub. Using default value. Error: %+v", err)
		return token.VideoChanSub{}
	}

	return *resolver.Resolve(ctx)
}

// ChannelIsGeoBlocked resolves the ChannelIsGeoBlocked field
// Default value: false
func (v *VideoAccessTokenResolver) ChannelIsGeoBlocked(ctx context.Context) bool {
	config := v.config
	geoblockMap := config.GeoblockedChannels
	if config.DynamicGeoblockingDarklaunch != nil && config.DynamicGeoblockingDarklaunch.UseDynamicGeoblocking() && config.DynamicGeoblocking != nil {
		geoblockMap = config.DynamicGeoblocking.GetGeoblockedChannels()
	}
	channelName, err := v.Channel(ctx)
	if err != nil {
		log.Errorf("Error resolving channel for geoblock check. Using default value. Error: %+v", err)
		return false
	}

	// If there is not Geoblock config for this channel, we should not Geoblock this user
	geoblockEntry, exists := geoblockMap[channelName]
	if !exists {
		log.WithField("channelName", channelName).Debug("No geoblock entry exists for this channel")
		return false
	}

	now := time.Now()
	countryCode := v.request.CountryCode
	postalCode := v.request.PostalCode
	userIP := v.request.UserIp
	platform := v.request.Platform

	// If IsAllowlistedPlatform is false, we know that this specific request is for a channel that has a PlatformAllowlist,
	// and the platform included in v.request.Platform is not on said allowlist.
	if !geoblockEntry.IsAllowlistedPlatform(platform, now) {
		log.WithFields(log.Fields{
			"channelName": channelName,
			"platform":    platform,
		}).Debug("This platform is not allowlisted for this channel.")
		return true
	}

	// If the channel does not allow ip's from known VPN / Proxy, we should block this user
	if geoblockEntry.BlockAnon(v.request.IpIsAnonymous, now) {
		// Check to see if the IP is within the allowlist for anonymous IPs as we allow some IP addresses tagged as Anonymous (e.g. WPA2)
		ip := net.ParseIP(userIP)

		if !v.ipIsAllowlistedAnonymousIP(ip) {
			log.WithFields(log.Fields{
				"channelName":   channelName,
				"ipIsAnonymous": v.request.IpIsAnonymous,
			}).Debug("This channel does not allow anonymous IPs.")
			return true
		}
	}

	// If the channel is blocklisted, we should Geoblock this user
	// Check country code is blocklisted
	if geoblockEntry.IsBlocklisted(countryCode, now) {
		log.WithFields(log.Fields{
			"channelName": channelName,
			"countryCode": countryCode,
		}).Debug("This country code for this channel is blocklisted.")
		return true
	}

	// Check if postal code is blocklisted
	if geoblockEntry.IsBlocklistedZipCode(postalCode, now) {
		log.WithFields(log.Fields{
			"channelName": channelName,
			"postalCode":  postalCode,
		}).Debug("This postal code for this channel is blocklisted.")
		return true
	}

	// If the channel is allowlisted, we should not Geoblock this user
	// Check country code is allowlisted
	if geoblockEntry.IsAllowlistedCountry(countryCode, now) {
		log.WithFields(log.Fields{
			"channelName": channelName,
			"countryCode": countryCode,
		}).Debug("This country code for this channel is allowlisted.")
		return false
	}

	// There must be a Geoblock for this channel and this channel is not allowlisted. We should Geoblock this user
	log.WithFields(log.Fields{
		"channelName": channelName,
		"countryCode": countryCode,
		"postalCode":  postalCode,
	}).Debug("This combination of country code or postal code for this channel is not allowlisted.")
	return true
}

// Checks to see if the supplied net.IP is within the Allowlist for Anonymous IPs
func (v *VideoAccessTokenResolver) ipIsAllowlistedAnonymousIP(ip net.IP) bool {
	for _, network := range v.config.AnonymousIPAllowlist {
		_, ipNet, err := net.ParseCIDR(network)
		if err != nil {
			// What should I do here?
			log.WithFields(log.Fields{
				"network": network,
			}).Debug("Could not parse CIDR from config")
		} else if ipNet.Contains(ip) {
			return true
		}
	}
	return false
}

// GeoblockReason resolves the GeoblockReason field
// Default value: empty string
func (v *VideoAccessTokenResolver) GeoblockReason(ctx context.Context) string {
	geoblockMap := v.config.GeoblockedChannels
	channelName, err := v.Channel(ctx)
	if err != nil {
		log.Errorf("Error resolving channel for geoblock reason. Using default value. Error: %+v", err)
		return ""
	}

	// If there is no Geoblock config for this channel, there is no geoblock reason
	geoblockEntry, exists := geoblockMap[channelName]
	if !exists {
		log.WithField("channelName", channelName).Debug("No geoblock entry exists for this channel")
		return ""
	}

	now := time.Now()
	if geoblockEntry.BlockAnon(v.request.IpIsAnonymous, now) {
		return geoblock.VPNBlocking
	}

	if v.ChannelIsGeoBlocked(ctx) {
		return geoblock.ContentRestricted
	}

	return ""
}

// DeviceID resolves the DeviceID field
// Default value: nil
func (v *VideoAccessTokenResolver) DeviceID(ctx context.Context) *string {
	if v.request.DeviceId == "" {
		return nil
	}
	return &v.request.DeviceId
}

// Expires resolves the Expires field
// Default value: timestamp 20 minutes from now
func (v *VideoAccessTokenResolver) Expires(ctx context.Context) int64 {
	expiration := time.Now().Add(videoExpirationOffset)
	return expiration.Unix()
}

// ExtendedHistoryAllowed resolves the ExtendedHistoryAllowed field
// Default value: false
func (v *VideoAccessTokenResolver) ExtendedHistoryAllowed(ctx context.Context) bool {
	return v.request.ExtendedHistoryAllowed
}

// Game resolves the Game field
// Default value : empty string
// Deprecated: This field is no longer used in downstream services, so we
// 			   don't need to actually to populate it correctly. It is hard-coded
//			   and included in the token for backwards compatibility.
func (v *VideoAccessTokenResolver) Game(ctx context.Context) string {
	return defaultGame
}

func (v *VideoAccessTokenResolver) BroadcastingSoftware(ctx context.Context) string {
	channelID, err := v.ChannelIDString(ctx)
	if err != nil {
		log.Errorf("Error getting channel ID. Using default value. Error: %+v", err)
		return defaultGame
	}

	channelResponse, err := loaders.LoadChannelProperties(ctx, channelID)
	if err != nil {
		rootErr := gerrors.Cause(err)
		switch rootErr.(type) {
		case *client.UserNotFoundError:
			return defaultGame
		default:
			log.Errorf("Error loading channel data. Returning not found. Error: %+v", err)
			return defaultGame
		}
	}

	if channelResponse == nil {
		log.Warn("Received empty response from LoadChannelProperties. Returning not found.")
		return defaultGame
	}

	return channelResponse.BroadcasterSoftware
}

// HideAds resolves the HideAds field
// Default value: !ShowAds()
// Deprecated: This field is no longer used in downstream services, so we
// 			   don't need to actually to populate it correctly. It is hydrated with
//			   the negation of the ShowAds() field which is still used.
//			   It is included in the token for backwards compatibility.
func (v *VideoAccessTokenResolver) HideAds(ctx context.Context) bool {
	return !v.ShowAds(ctx)
}

// HTTPSRequired resolves the HTTPSRequired field
// Default value: false
func (v *VideoAccessTokenResolver) HTTPSRequired(ctx context.Context) bool {
	return v.request.NeedHttps
}

// Mature resolves the Mature field
// Default value : false
// Deprecated: This field is no longer used in downstream services, so we
// 			   don't need to actually to populate it correctly. It is hard-coded
//			   and included in the token for backwards compatibility.
func (v *VideoAccessTokenResolver) Mature(ctx context.Context) bool {
	return false
}

// Partner resolves the Partner field
// Default value: false
// Deprecated: This field is no longer used in downstream services, so we
// 			   don't need to actually to populate it correctly. It is hard-coded
//			   to false and included in the token for backwards compatibility.
func (v *VideoAccessTokenResolver) Partner(ctx context.Context) bool {
	return false
}

// Platform resolves the Platform field
// Default value: null
func (v *VideoAccessTokenResolver) Platform(ctx context.Context) *string {
	var result *string
	if v.request.Platform != "" {
		result = &v.request.Platform
	}
	return result
}

// PlayerType resolves the PlayerType field
// Default value: null
func (v *VideoAccessTokenResolver) PlayerType(ctx context.Context) *string {
	var result *string
	if v.request.PlayerType != "" {
		result = &v.request.PlayerType
	}
	return result
}

// Private resolves the Private field
func (v *VideoAccessTokenResolver) Private(ctx context.Context) token.Private {
	resolver, err := NewVideoPrivateResolver(ctx, v)
	if err != nil {
		log.Errorf("Error resolving Private. Using default value. Error: %+v", err)
		return token.Private{}
	}

	return *resolver.Resolve(ctx)
}

// Privileged resolves the Privileged field
// Default value: false
func (v *VideoAccessTokenResolver) Privileged(ctx context.Context) bool {
	channelID, err := v.ChannelIDString(ctx)
	if err != nil {
		log.Errorf("Error getting channel ID. Using default value. Error: %+v", err)
		return false
	}

	userID := v.UserIDString(ctx)

	// users are privileged if they have escalated ChanSub privileges or they have private permissions
	return helpers.UserHasEscalatedChanSubPrivileges(ctx, channelID, userID) || helpers.UserHasPrivatePermissions(ctx, channelID, userID)
}

// Role resolves the role field
// Default value: ""
func (v *VideoAccessTokenResolver) Role(ctx context.Context) string {
	role, err := helpers.GetRole(ctx, v.request.GetUserId())
	if err != nil {
		log.Errorf("Error getting role. Defaulting to no special role. Error: %+v", err)
		return helpers.RoleNone.String()
	}
	return role.String()
}

// ServerAds resolves the ServerAds field
// Default value: false
func (v *VideoAccessTokenResolver) ServerAds(ctx context.Context) bool {
	channelID, err := v.ChannelIDString(ctx)
	if err != nil || channelID == "" {
		log.Errorf("Error getting channel ID. Using default value. Error: %+v", err)
		return false
	}

	products, err := loaders.LoadChannelProducts(ctx, channelID)
	if err != nil {
		log.Errorf("Error loading channel products. Using default value. Error: %+v", err)
		return false
	}

	hideAds := false
	if len(products) == 0 {
		log.Debug("This channel has no products. Using default value.")
		hideAds = false
	} else {
		hideAds = products[0].HideAds
	}

	platformStr := ""
	platform := v.Platform(ctx)
	if platform != nil {
		platformStr = *platform
	}

	playerTypeStr := ""
	playerType := v.PlayerType(ctx)
	if playerType != nil {
		playerTypeStr = *playerType
	}

	if playerTypeStr == "picture-by-picture" {
		return false
	}

	adsResp, err := loaders.LoadServerAds(ctx, v.request.UserId, channelID, v.request.DeviceId, v.request.CountryCode, platformStr, playerTypeStr, v.Turbo(ctx), v.Subscriber(ctx), hideAds, v.request.PlayerBackend)
	if err != nil {
		log.Errorf("Error loading ads data for server_ads. Returning request value. Error: %+v", err)
		return false
	}

	if adsResp == nil {
		log.Warn("Received empty response from LoadServerAds when fetching server_ads. Returning request value.")
		return false
	}

	return adsResp.ServerAds
}

// ShowAds resolves the ShowAds field
// Default value: true
func (v *VideoAccessTokenResolver) ShowAds(ctx context.Context) bool {
	if v.Turbo(ctx) {
		// Turbo/Prime users never get ads.
		return false
	}

	playerTypeStr := ""
	playerType := v.PlayerType(ctx)
	if playerType != nil {
		playerTypeStr = *playerType
	}
	// Picture-by-Picture players should never get ads.
	if playerTypeStr == "picture-by-picture" {
		return false
	}

	if !v.Subscriber(ctx) {
		// Non-subscribers will always get ads (assuming not Turbo/Prime)
		return true
	}

	// Give non-Turbo/Prime channel subscribers ads only if the channel product has HideAds=false
	channelID, err := v.ChannelIDString(ctx)
	if err != nil || channelID == "" {
		log.Errorf("Error getting channel ID. Using default value. Error: %+v", err)
		return true
	}

	products, err := loaders.LoadChannelProducts(ctx, channelID)
	if err != nil {
		log.Errorf("Error loading channel products. Using default value. Error: %+v", err)
		return true
	}
	if len(products) == 0 {
		log.Debug("This channel has no products. Using default value.")
		return true
	}

	return !products[0].HideAds
}

// Subscriber resolves the Subscriber field
// Default value: false
func (v *VideoAccessTokenResolver) Subscriber(ctx context.Context) bool {
	channelID, err := v.ChannelIDString(ctx)
	if err != nil {
		log.Errorf("Error getting channel ID. Using default value. Error: %+v", err)
		return false
	}
	return helpers.IsSubscriber(ctx, channelID, v.UserIDString(ctx))
}

// Turbo resolves the Turbo field
// Default value: false
func (v *VideoAccessTokenResolver) Turbo(ctx context.Context) bool {
	if v.request.UserId == "" {
		return false // anonymous users are never prime
	}

	premiumResp, err := loaders.LoadPremium(ctx, v.request.UserId)
	if err != nil || premiumResp == nil {
		log.Errorf("Error loading prime status. Using default value. Error: %+v", err)
		return false
	}

	return premiumResp.HasTurbo
}

// UserID resolves the UserID field as an *int64
// Default value: null
func (v *VideoAccessTokenResolver) UserID(ctx context.Context) *int64 {
	userIDInt, err := strconv.ParseInt(v.UserIDString(ctx), 10, 64)
	if err != nil {
		return nil
	}
	return &userIDInt
}

// UserIP resolves the UserIP field
// Default value: ""
func (v *VideoAccessTokenResolver) UserIP(ctx context.Context) string {
	return v.request.UserIp
}

// UserIDString resolves the UserID field as a string
// Default value: ""
func (v *VideoAccessTokenResolver) UserIDString(ctx context.Context) string {
	return v.request.UserId
}

// Version resolves the Version field
// Default value: 2
func (v *VideoAccessTokenResolver) Version(ctx context.Context) int {
	return TokenVersion
}
