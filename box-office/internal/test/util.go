package test

import (
	"context"

	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/mocks"
	"github.com/nicksrandall/dataloader"
	"github.com/stretchr/testify/mock"
)

// -- TESTING ONLY --
// Note: These utils are intended to be used for unit testing only.
// -- TESTING ONLY --

// SetupLoaderContext sets ups a mock loader that returns the input data and err. The loader is then mapped input loaderKey.
// The loader is attached to the context, and the context is returned.
func SetupLoaderContext(ctx context.Context, loaderKey loaders.LoaderKey, data interface{}, err error) context.Context {
	mockAttacher := new(mocks.Attacher)
	mockAttacher.On("Attach", mock.Anything).Return(BuildBatchFunc(data, err))

	attachers := &loaders.Attachers{
		LoaderMap: map[loaders.LoaderKey]loaders.Attacher{
			loaderKey: mockAttacher,
		},
	}

	ctx = attachers.Attach(ctx)
	return ctx
}

// BuildBatchFunc returns input data from the built BatchFunc
func BuildBatchFunc(data interface{}, err error) dataloader.BatchFunc {
	var resultFunc dataloader.BatchFunc
	resultFunc = func(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
		result := []*dataloader.Result{}
		result = append(result, &dataloader.Result{
			Data:  data,
			Error: err,
		})
		return result
	}
	return resultFunc
}

// RunLoader runs the input loader, passing it in the input data key.
func RunLoader(attacher loaders.Attacher, dataKey string) (interface{}, error) {
	batchLoader := dataloader.NewBatchedLoader(attacher.Attach(context.Background()))
	loadFn := batchLoader.Load(context.Background(), dataloader.StringKey(dataKey))
	return loadFn()
}
