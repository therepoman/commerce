package stats

import (
	"fmt"

	"github.com/cactus/go-statsd-client/statsd"
)

const (
	fallbackName = "none"
	deviceIDName = "deviceid"
)

// IPlatformRecorder is an interface for recording platform metrics
type IPlatformRecorder interface {
	RecordPlatformMetrics(string, string, string, string)
}

// PlatformRecorder records platform metrics from requests
type PlatformRecorder struct {
	Stats statsd.Statter
}

// RecordPlatformMetrics records metrics that help to identify the different platforms that are calling platform and establish
// basic client traffic metrics
func (p *PlatformRecorder) RecordPlatformMetrics(tokenType string, platform string, playerType string, deviceID string) {
	recordedPlatform := platform
	if recordedPlatform == "" {
		recordedPlatform = fallbackName
	}

	recordedPlayerType := playerType
	if recordedPlayerType == "" {
		recordedPlayerType = fallbackName
	}

	platformStatName := fmt.Sprintf("platforms.%s.%s.%s", tokenType, recordedPlatform, recordedPlayerType)
	p.Stats.Inc(platformStatName, 1, 1.0)

	if deviceID == "" {
		p.Stats.Inc(fmt.Sprintf("%s.%s", platformStatName, deviceIDName), 0, 1.0)
	} else {
		p.Stats.Inc(fmt.Sprintf("%s.%s", platformStatName, deviceIDName), 1, 1.0)
	}
}
