package stats

import (
	"log"
	"time"

	metricCollector "github.com/afex/hystrix-go/hystrix/metric_collector"
	"github.com/cactus/go-statsd-client/statsd"
)

// This is a implementation of Hystrix metricCollector interface.
// The plugin that comes with only supports creating one with statsd server address.
// Since we want to pass in our own statsd statter with TwitchTelemetry, this is necessary.
// Reference: https://github.com/afex/hystrix-go/blob/master/plugins/statsd_collector.go
type HystrixStatsdCollector struct {
	client                  statsd.Statter
	circuitOpenPrefix       string
	attemptsPrefix          string
	errorsPrefix            string
	successesPrefix         string
	failuresPrefix          string
	rejectsPrefix           string
	shortCircuitsPrefix     string
	timeoutsPrefix          string
	fallbackSuccessesPrefix string
	fallbackFailuresPrefix  string
	canceledPrefix          string
	deadlinePrefix          string
	totalDurationPrefix     string
	runDurationPrefix       string
	sampleRate              float32
}

type HystrixStatsdCollectorClient struct {
	client     statsd.Statter
	sampleRate float32
}

// HystrixStatsdCollectorConfig provides configuration that the Statsd client will need.
type HystrixStatsdCollectorConfig struct {
	// Statter is the statsd statter client
	Statter statsd.Statter
	// Prefix is the prefix that will be prepended to all metrics sent from this collector.
	Prefix string
	// StatsdSampleRate sets statsd sampling. If 0, defaults to 1.0. (no sampling)
	SampleRate float32
	// FlushBytes sets message size for statsd packets. If 0, defaults to LANFlushSize.
	FlushBytes int
}

// InitializeHystrixStatsdCollector creates the client that has a handle for registering with hystrix metrics collector.
func InitializeHystrixStatsdCollector(config *HystrixStatsdCollectorConfig) *HystrixStatsdCollectorClient {
	sampleRate := config.SampleRate
	if sampleRate == 0 {
		sampleRate = 1
	}
	return &HystrixStatsdCollectorClient{
		client:     config.Statter,
		sampleRate: sampleRate,
	}
}

// NewStatsdCollector creates a collector for a specific circuit. The
// prefix given to this circuit will be {statter prefix}.{circuit_name}.{metric}.
func (s *HystrixStatsdCollectorClient) NewStatsdCollector(name string) metricCollector.MetricCollector {
	if s.client == nil {
		log.Fatalf("Statsd client must be initialized before circuits are created.")
	}

	return &HystrixStatsdCollector{
		client:                  s.client,
		circuitOpenPrefix:       name + ".circuitOpen",
		attemptsPrefix:          name + ".attempts",
		errorsPrefix:            name + ".errors",
		successesPrefix:         name + ".successes",
		failuresPrefix:          name + ".failures",
		rejectsPrefix:           name + ".rejects",
		shortCircuitsPrefix:     name + ".shortCircuits",
		timeoutsPrefix:          name + ".timeouts",
		fallbackSuccessesPrefix: name + ".fallbackSuccesses",
		fallbackFailuresPrefix:  name + ".fallbackFailures",
		canceledPrefix:          name + ".contextCanceled",
		deadlinePrefix:          name + ".contextDeadlineExceeded",
		totalDurationPrefix:     name + ".totalDuration",
		runDurationPrefix:       name + ".runDuration",
		sampleRate:              s.sampleRate,
	}
}

func (g *HystrixStatsdCollector) setGauge(prefix string, value int64) {
	err := g.client.Gauge(prefix, value, g.sampleRate)
	if err != nil {
		log.Printf("Error sending statsd metrics %s", prefix)
	}
}

func (g *HystrixStatsdCollector) incrementCounterMetric(prefix string, i float64) {
	if i == 0 {
		return
	}
	err := g.client.Inc(prefix, int64(i), g.sampleRate)
	if err != nil {
		log.Printf("Error sending statsd metrics %s", prefix)
	}
}

func (g *HystrixStatsdCollector) updateTimerMetric(prefix string, dur time.Duration) {
	err := g.client.TimingDuration(prefix, dur, g.sampleRate)
	if err != nil {
		log.Printf("Error sending statsd metrics %s", prefix)
	}
}

func (g *HystrixStatsdCollector) Update(r metricCollector.MetricResult) {
	if r.Successes > 0 {
		g.setGauge(g.circuitOpenPrefix, 0)
	} else if r.ShortCircuits > 0 {
		g.setGauge(g.circuitOpenPrefix, 1)
	}

	g.incrementCounterMetric(g.attemptsPrefix, r.Attempts)
	g.incrementCounterMetric(g.errorsPrefix, r.Errors)
	g.incrementCounterMetric(g.successesPrefix, r.Successes)
	g.incrementCounterMetric(g.failuresPrefix, r.Failures)
	g.incrementCounterMetric(g.rejectsPrefix, r.Rejects)
	g.incrementCounterMetric(g.shortCircuitsPrefix, r.ShortCircuits)
	g.incrementCounterMetric(g.timeoutsPrefix, r.Timeouts)
	g.incrementCounterMetric(g.fallbackSuccessesPrefix, r.FallbackSuccesses)
	g.incrementCounterMetric(g.fallbackFailuresPrefix, r.FallbackFailures)
	g.incrementCounterMetric(g.canceledPrefix, r.ContextCanceled)
	g.incrementCounterMetric(g.deadlinePrefix, r.ContextDeadlineExceeded)
	g.updateTimerMetric(g.totalDurationPrefix, r.TotalDuration)
	g.updateTimerMetric(g.runDurationPrefix, r.RunDuration)
}

// Reset is a noop operation in this collector.
func (g *HystrixStatsdCollector) Reset() {}
