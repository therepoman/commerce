package stats

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/box-office/config"
	"code.justin.tv/commerce/splatter"
	"code.justin.tv/foundation/twitchclient"
	"github.com/cactus/go-statsd-client/statsd"

	metricCollector "github.com/afex/hystrix-go/hystrix/metric_collector"
)

const (
	metricPattern         = "service.%s.%s"
	defaultStatSampleRate = 1.0

	namespace             = "box-office"
	prodCanaryEnvironment = "prod-canary"
	prodEnvironment       = "prod"

	canarySubstage  = "canary"
	primarySubstage = "primary"

	twitchTelemetryMinBuffer = 100000 // Define a min buffer size to ensure there is enough room for metrics while flushing
)

// SetupStats returns an initialized statter
func SetupStats(cfg config.Configuration, env string) (statsd.Statter, error) {
	stats := setupTwitchTelemetryStatter(cfg, env)

	// Register the statter to hystrix metrics collector
	collector := InitializeHystrixStatsdCollector(&HystrixStatsdCollectorConfig{
		Statter:    stats,
		SampleRate: 1,
	})
	metricCollector.Registry.Register(collector.NewStatsdCollector)

	return stats, nil
}

func setupTwitchTelemetryStatter(cfg config.Configuration, env string) statsd.Statter {
	bufferSize := cfg.CloudwatchBufferSize
	if bufferSize < twitchTelemetryMinBuffer {
		bufferSize = twitchTelemetryMinBuffer
	}

	telemetryStage := env    // prod
	telemetrySubstage := env // prod-canary | primary

	if env == prodCanaryEnvironment {
		telemetryStage = prodEnvironment
		telemetrySubstage = canarySubstage
	} else if env == prodEnvironment {
		telemetrySubstage = primarySubstage
	}

	twitchTelemetryConfig := &splatter.BufferedTelemetryConfig{
		FlushPeriod:       cfg.TwitchTelemetryFlush * time.Second,
		BufferSize:        bufferSize,
		AggregationPeriod: time.Minute,
		ServiceName:       namespace,
		AWSRegion:         cfg.CloudwatchRegion,
		Stage:             telemetryStage,
		Substage:          telemetrySubstage,
		Prefix:            "", // no need for prefix
	}
	return splatter.NewBufferedTelemetryCloudWatchStatter(twitchTelemetryConfig, map[string]bool{})
}

// WithStats manually injects the request options into the context that the twitch client needs to
// generate stats. This should be used with twirp clients, but not legacy clients. Without this,
// twirp clients won't generate their own stats. Legacy clients (hand-rolled, no twirp model) usually
// don't need this because it is done by the author of the client.
func WithStats(ctx context.Context, serviceName string, methodName string) context.Context {
	reqOpts := twitchclient.ReqOpts{
		StatName:       fmt.Sprintf(metricPattern, serviceName, methodName),
		StatSampleRate: defaultStatSampleRate,
	}
	return twitchclient.WithReqOpts(ctx, reqOpts)
}
