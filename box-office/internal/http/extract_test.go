package http_test

import (
	"bytes"
	"errors"
	"io/ioutil"
	"testing"

	"code.justin.tv/commerce/box-office/internal/http"
	"code.justin.tv/commerce/box-office/mocks"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

var (
	sampleBody = []byte("abcde")
)

func TestExtract(t *testing.T) {
	Convey("Test Extract", t, func() {
		Convey("Test ExtractBody", func() {
			Convey("with nil body", func() {
				readCloser, bytes, err := http.ExtractBody(nil)
				So(readCloser, ShouldBeNil)
				So(bytes, ShouldBeNil)
				So(err, ShouldBeNil)
			})

			Convey("with error reading", func() {
				mockReadCloser := new(mocks.ReadCloser)
				mockReadCloser.On("Read", mock.Anything).Return(0, errors.New("test"))

				readCloser, bytes, err := http.ExtractBody(mockReadCloser)
				So(readCloser, ShouldBeNil)
				So(bytes, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

			Convey("with successful reading", func() {
				readCloser := ioutil.NopCloser(bytes.NewBuffer(sampleBody))

				readCloser, body, err := http.ExtractBody(readCloser)
				So(readCloser, ShouldNotBeNil)
				So(err, ShouldBeNil)
				So(bytes.Compare(body, sampleBody), ShouldEqual, 0)
			})
		})
	})
}
