package http

import (
	"bytes"
	"io"
	"io/ioutil"
	"net/http"

	"github.com/pkg/errors"
)

// ExtractHTTPRequestBody reads the request body into a []byte, and reset the http.Request body with a copy
// of the body.
func ExtractHTTPRequestBody(req *http.Request) ([]byte, error) {
	if req == nil {
		return nil, nil
	}

	reqBody, bodyBytes, err := ExtractBody(req.Body)
	if err != nil {
		return nil, errors.Wrap(err, "Error extracting request body")
	}
	req.Body = reqBody
	return bodyBytes, nil
}

// ExtractHTTPResponseBody reads the response body into a []byte, and reset the http.Response body with a copy
// of the body.
func ExtractHTTPResponseBody(resp *http.Response) ([]byte, error) {
	if resp == nil {
		return nil, nil
	}

	respBody, bodyBytes, err := ExtractBody(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "Error extracting response body")
	}
	resp.Body = respBody
	return bodyBytes, nil
}

// ExtractBody reads a io.ReadCloser into a []byte, then return a copy of the io.ReadCloser and the []byte.
// Returns an error if reading from the io.ReadCloser fails.
func ExtractBody(body io.ReadCloser) (io.ReadCloser, []byte, error) {
	if body == nil {
		return nil, nil, nil
	}

	bodyBytes, err := ioutil.ReadAll(body)
	if err != nil {
		return nil, nil, err
	}
	err = body.Close()
	if err != nil {
		return nil, nil, err
	}
	body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
	return body, bodyBytes, nil
}
