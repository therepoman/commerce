package signer

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestSigner(t *testing.T) {
	Convey("Test Signer", t, func() {

		Convey("happy case", func() {
			signer, err := NewSigner([]byte("testkey"))
			So(err, ShouldBeNil)

			signature, err := signer.Sign([]byte("testdata"))
			So(err, ShouldBeNil)
			So(signature, ShouldEqual, "e1df1976238aa72bd56a05a850c78c7be58812e8")
		})

		Convey("happy case2", func() {
			signer, err := NewSigner([]byte("testkey999"))
			So(err, ShouldBeNil)

			signature, err := signer.Sign([]byte("testdata555"))
			So(err, ShouldBeNil)
			So(signature, ShouldEqual, "f14025f0727db4c2cba0ca6be2db0535518eb8c0")
		})
	})
}
