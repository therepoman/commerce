package signer

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/hex"

	"github.com/pkg/errors"
)

// ISigner is an interface for creating a signature from data
type ISigner interface {
	Sign(data []byte) (string, error)
}

// Signer is an implementation of ISigner that uses a stored key
// to create an HMAC signature using a SHA-1 hash function.
// This implementation is intended to be functionally identical
// to the legacy web/web signing logic here:
//   - https://git-aws.internal.justin.tv/web/web/blob/master/lib/crypto.rb#L5
type Signer struct {
	key []byte
}

// NewSigner returns a new Signer which uses the provided key to sign
// data.
func NewSigner(key []byte) (*Signer, error) {
	return &Signer{
		key: key,
	}, nil
}

// Sign creates an HMAC signature using a SHA-1 hash function.
// The signature is created using the Signer's key and the input data.
// The signature is returned in a hexadecimal encoded string.
func (s *Signer) Sign(data []byte) (string, error) {
	hash := hmac.New(sha1.New, s.key)
	_, err := hash.Write(data)
	if err != nil {
		return "", errors.Wrap(err, "Error writing HMAC")
	}

	signatureBytes := hash.Sum(nil)
	signatureHex := hex.EncodeToString(signatureBytes)

	return signatureHex, nil
}
