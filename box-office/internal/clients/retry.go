package clients

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	bohttp "code.justin.tv/commerce/box-office/internal/http"

	"github.com/afex/hystrix-go/hystrix"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
)

const (
	numRetries = 1                      // how many times to retry before returning the error
	sleepTime  = 100 * time.Millisecond // how long to sleep before retrying
)

// RetryRoundTripWrapper is a wrapper around an http round tripper which retries
// calls unders certain conditions
type RetryRoundTripWrapper struct {
	Next  http.RoundTripper
	Name  string
	Stats statsd.Statter
}

// NewRetryRoundTripWrapper returns a new RetryRoundTripWrapper.
func NewRetryRoundTripWrapper(stats statsd.Statter, name string) func(c http.RoundTripper) http.RoundTripper {
	return func(next http.RoundTripper) http.RoundTripper {
		return &RetryRoundTripWrapper{
			Next:  next,
			Name:  name,
			Stats: stats,
		}
	}
}

// RoundTrip wraps HTTP client calls. It retries the downstream call when necessary.
func (r *RetryRoundTripWrapper) RoundTrip(req *http.Request) (*http.Response, error) {
	requestBody, err := bohttp.ExtractHTTPRequestBody(req)
	if err != nil {
		return nil, errors.Wrap(err, "Error extracting HTTP request for retrying")
	}

	resp, nextErr := r.Next.RoundTrip(req)

	retries := numRetries
	for retries > 0 && (isResponseRetriable(resp) || isErrorRetriable(nextErr)) {
		time.Sleep(sleepTime)
		statName := fmt.Sprintf("retries.%s", r.Name)
		_ = r.Stats.Inc(statName, 1, 1.0)

		// reset the body with a fresh buffer any time we retry the call
		req.Body = ioutil.NopCloser(bytes.NewBuffer(requestBody))
		resp, nextErr = r.Next.RoundTrip(req)
		retries--
	}

	return resp, nextErr
}

// isResponseRetriable returns true if the http response is retriable. A healthy response is not retriable.
func isResponseRetriable(resp *http.Response) bool {
	if resp == nil {
		return false
	}

	return resp.StatusCode >= 500
}

// isErrorRetriable returns true if the error is retriable. A nil error is not retriable.
func isErrorRetriable(err error) bool {
	if err == nil {
		return false
	}

	rootErr := errors.Cause(err)
	if _, ok := rootErr.(hystrix.CircuitError); ok {
		return true
	}

	return false
}
