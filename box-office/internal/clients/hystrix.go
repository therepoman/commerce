package clients

import (
	"net/http"

	log "github.com/sirupsen/logrus"

	"github.com/pkg/errors"

	"github.com/afex/hystrix-go/hystrix"
)

const (
	DefaultMaxConcurrentRequest  = 200
	DefaultVolumeThreshold       = 20
	DefaultSleepWindow           = 5000 // Milliseconds
	DefaultErrorPercentThreshold = 20
)

var errHystrix5XX = errors.New("hystrix encountered an HTTP 5XX")

// HystrixRoundTripWrapper is a wrapper around an http round tripper which utilizes
// a hystrix circuit to allow timeouts, circuit breakers, connection pool, etc.
type HystrixRoundTripWrapper struct {
	Next http.RoundTripper
	Name string
}

// NewHystrixRoundTripWrapper returns a new HystrixRoundTripWrapper.
func NewHystrixRoundTripWrapper(name string, timeout int) func(c http.RoundTripper) http.RoundTripper {
	return func(next http.RoundTripper) http.RoundTripper {
		config := hystrix.CommandConfig{
			Timeout:                timeout,
			MaxConcurrentRequests:  DefaultMaxConcurrentRequest,
			RequestVolumeThreshold: DefaultVolumeThreshold,
			SleepWindow:            DefaultSleepWindow,
			ErrorPercentThreshold:  DefaultErrorPercentThreshold,
		}
		hystrix.ConfigureCommand(name, config)
		return &HystrixRoundTripWrapper{
			Next: next,
			Name: name,
		}
	}
}

// RoundTrip wraps HTTP client calls. It wraps the downstream call in a Hystrix circuit.
func (h *HystrixRoundTripWrapper) RoundTrip(req *http.Request) (*http.Response, error) {
	var resp *http.Response
	err := hystrix.Do(h.Name, func() (e error) {
		var nextErr error
		resp, nextErr = h.Next.RoundTrip(req)
		if nextErr != nil {
			return errors.Wrap(nextErr, "Error from next round tripper")
		}
		if resp.StatusCode >= http.StatusInternalServerError {
			// 5XX errors count towards hystrix errors so that we can break the circuit on them
			// when we see too many
			return errHystrix5XX
		}
		return nil
	}, nil)

	if err != nil {
		if err == errHystrix5XX {
			// Swallowing the hystrix error and allowing the 5XX response to propagate out.
			// This allows other handlers and wrappers to handle the status code at their discretion
			log.Errorf("Hystrix encountered an HTTP 5XX from service: %s", h.Name)
			return resp, nil
		}
		return nil, errors.Wrap(err, "Error from hystrix circuit")
	}

	return resp, nil
}
