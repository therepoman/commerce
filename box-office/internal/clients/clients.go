package clients

import (
	"fmt"
	"net/http"
	"time"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"

	dads "code.justin.tv/ads/dads/proto"
	voyager "code.justin.tv/amzn/TwitchVoyagerTwirp"
	"code.justin.tv/cb/hallpass/client/hallpass"
	"code.justin.tv/chat/rediczar"
	"code.justin.tv/chat/rediczar/poolmetrics"
	"code.justin.tv/chat/rediczar/poolmetrics/statsdpoolmetrics"
	"code.justin.tv/chat/rediczar/redefault"
	"code.justin.tv/chat/rediczar/runmetrics/statsdrunmetrics"
	boConfig "code.justin.tv/commerce/box-office/config"
	"code.justin.tv/commerce/box-office/internal/cache"
	sandstormHelper "code.justin.tv/commerce/box-office/internal/sandstorm"
	"code.justin.tv/commerce/box-office/internal/signer"
	"code.justin.tv/commerce/nioh/rpc/nioh"
	sandstorm "code.justin.tv/commerce/sandstorm-client"
	"code.justin.tv/foundation/twitchclient"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	nitro "code.justin.tv/samus/nitro/rpc"
	"code.justin.tv/systems/sandstorm/manager"
	clips "code.justin.tv/video/clips-upload/client"
	"code.justin.tv/vod/tailor/rpc/tailor"
	"code.justin.tv/vod/vodapi/rpc/vodapi"
	"code.justin.tv/web/users-service/client/channels"
	"code.justin.tv/web/users-service/client/usersclient_internal"
)

const (
	hystrixPrefix = "hystrix"
)

// Clients contains the interfaces to all external clients.
type Clients struct {
	Channels      channels.Client
	Hallpass      hallpass.Client
	Nitro         nitro.Nitro
	Sandstorm     *manager.Manager
	Signer        signer.ISigner
	Stats         statsd.Statter
	Subscriptions substwirp.Subscriptions
	Voyager       voyager.TwitchVoyager
	Users         usersclient_internal.InternalClient
	VodAPI        vodapi.VodApi
	Dads          dads.Dads
	Nioh          nioh.API
	Tailor        tailor.Tailor
	Clips         clips.Client
}

// SetupClients sets up all the clients in the Clients struct. A Clients struct is built and returned.
func SetupClients(cfg *boConfig.Configuration, stats statsd.Statter) (*Clients, error) {
	sandstormConfig := sandstorm.Config{
		AWSRegion:    cfg.AWSRegion,
		Duration:     900 * time.Second,
		ExpiryWindow: 10 * time.Second,
		RoleARN:      cfg.SandstormRoleARN,
	}

	sandstorm, err := sandstorm.New(sandstormConfig)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to initialize Sandstorm client")
	}

	signerClient, err := setupSigner(cfg, sandstorm)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to set up Signer")
	}

	// One redis client per cache. This allows for key prefixes to be specified which
	// prevent namespace collisions.
	primaryCacheRedisClient, err := setupRedisClient(cfg, cfg.RedisHost, "primary", stats)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to set up primary redis client")
	}

	primaryHTTPCache := cache.NewHTTPCache(primaryCacheRedisClient)

	fallbackCacheRedisClient, err := setupRedisClient(cfg, cfg.RedisHost, "fallback", stats)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to set up fallback redis client")
	}

	fallbackHTTPCache := cache.NewHTTPCache(fallbackCacheRedisClient)

	defaultFallbackTTL := time.Duration(cfg.FallbackCacheTTLMinutes) * time.Minute
	defaultFallbackTimeout := time.Duration(cfg.FallbackCacheTimeout) * time.Millisecond

	// Hallpass client
	hallpassName := "hallpass"
	hallpassTTL := time.Duration(cfg.HallpassServiceCacheTTLMinutes) * time.Minute
	hallpassConf := twitchclient.ClientConf{
		Host: cfg.HallpassServiceEndpoint,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			NewHystrixRoundTripWrapper(hystrixCircuitName(hallpassName), cfg.HallpassServiceClientTimeout),
			NewRetryRoundTripWrapper(stats, hallpassName),
			cache.NewFallbackCacheRoundTripWrapper(fallbackHTTPCache, cfg.EnableFallbackCache, defaultFallbackTTL, defaultFallbackTimeout, cfg),
			cache.NewPrimaryCacheRoundTripWrapper(primaryHTTPCache, cfg.EnablePrimaryCache, hallpassTTL, cfg),
		},
		Stats: stats,
	}
	hallpassClient, err := hallpass.NewClient(hallpassConf)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to set up Hallpass client")
	}

	// Nitro client
	nitroName := "nitro"
	nitroTTL := time.Duration(cfg.NitroServiceCacheTTLMinutes) * time.Minute
	nitroConf := twitchclient.ClientConf{
		Host: cfg.NitroServiceEndpoint,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			NewHystrixRoundTripWrapper(hystrixCircuitName(nitroName), cfg.NitroServiceClientTimeout),
			NewRetryRoundTripWrapper(stats, nitroName),
			cache.NewFallbackCacheRoundTripWrapper(fallbackHTTPCache, cfg.EnableFallbackCache, defaultFallbackTTL, defaultFallbackTimeout, cfg),
			cache.NewPrimaryCacheRoundTripWrapper(primaryHTTPCache, cfg.EnablePrimaryCache, nitroTTL, cfg),
		},
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: cfg.NitroServiceMaxIdleConnections,
		},
		Stats: stats,
	}
	nitroClient := nitro.NewNitroProtobufClient(cfg.NitroServiceEndpoint, twitchclient.NewHTTPClient(nitroConf))

	// Subscriptions client
	subsName := "subscriptions"
	subsTTL := time.Duration(cfg.SubscriptionsCacheTTLMinutes) * time.Minute
	subsConf := twitchclient.ClientConf{
		Host: cfg.SubscriptionsEndpoint,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			NewHystrixRoundTripWrapper(hystrixCircuitName(subsName), cfg.SubscriptionsClientTimeout),
			NewRetryRoundTripWrapper(stats, subsName),
			cache.NewFallbackCacheRoundTripWrapper(fallbackHTTPCache, cfg.EnableFallbackCache, defaultFallbackTTL, defaultFallbackTimeout, cfg),
			cache.NewPrimaryCacheRoundTripWrapper(primaryHTTPCache, cfg.EnablePrimaryCache, subsTTL, cfg),
		},
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: cfg.SubscriptionsMaxIdleConnections,
		},
		Stats: stats,
	}
	subsClient := substwirp.NewSubscriptionsProtobufClient(cfg.SubscriptionsEndpoint, twitchclient.NewHTTPClient(subsConf))

	// Voyager client
	voyagerName := "voyager"
	voyagerTTL := time.Duration(cfg.VoyagerCacheTTLMinutes) * time.Minute
	voyagerConf := twitchclient.ClientConf{
		Host: cfg.VoyagerEndpoint,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			NewHystrixRoundTripWrapper(hystrixCircuitName(voyagerName), cfg.VoyagerClientTimeout),
			NewRetryRoundTripWrapper(stats, voyagerName),
			cache.NewFallbackCacheRoundTripWrapper(fallbackHTTPCache, cfg.EnableFallbackCache, defaultFallbackTTL, defaultFallbackTimeout, cfg),
			cache.NewPrimaryCacheRoundTripWrapper(primaryHTTPCache, cfg.EnablePrimaryCache, voyagerTTL, cfg),
		},
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: cfg.VoyagerMaxIdleConnections,
		},
		Stats: stats,
	}
	voyagerClient := voyager.NewTwitchVoyagerProtobufClient(cfg.VoyagerEndpoint, twitchclient.NewHTTPClient(voyagerConf))

	// Users client
	usersServiceName := "users"
	usersServiceTTL := time.Duration(cfg.UsersServiceCacheTTLMinutes) * time.Minute
	usersServiceConf := twitchclient.ClientConf{
		Host: cfg.UsersServiceEndpoint,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			NewHystrixRoundTripWrapper(hystrixCircuitName(usersServiceName), cfg.UsersServiceClientTimeout),
			NewRetryRoundTripWrapper(stats, usersServiceName),
			cache.NewFallbackCacheRoundTripWrapper(fallbackHTTPCache, cfg.EnableFallbackCache, defaultFallbackTTL, defaultFallbackTimeout, cfg),
			cache.NewPrimaryCacheRoundTripWrapper(primaryHTTPCache, cfg.EnablePrimaryCache, usersServiceTTL, cfg),
		},
		Stats: stats,
	}
	usersClient, err := usersclient_internal.NewClient(usersServiceConf)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to set up Users client")
	}

	// Channels client
	// Note: This is a separate client from Users client, but both point to users service
	channelsName := "channels"
	channelsTTL := time.Duration(cfg.ChannelsCacheTTLMinutes) * time.Minute
	channelsConf := twitchclient.ClientConf{
		Host: cfg.UsersServiceEndpoint,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			NewHystrixRoundTripWrapper(hystrixCircuitName(channelsName), cfg.UsersServiceClientTimeout),
			NewRetryRoundTripWrapper(stats, channelsName),
			cache.NewFallbackCacheRoundTripWrapper(fallbackHTTPCache, cfg.EnableFallbackCache, defaultFallbackTTL, defaultFallbackTimeout, cfg),
			cache.NewPrimaryCacheRoundTripWrapper(primaryHTTPCache, cfg.EnablePrimaryCache, channelsTTL, cfg),
		},
		Stats: stats,
	}
	channelsClient, err := channels.NewClient(channelsConf)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to set up Channels client")
	}

	// VodApi client
	vodapiName := "vodapi"
	vodapiTTL := time.Duration(cfg.VodAPICacheTTLMinutes) * time.Minute
	vodapiConf := twitchclient.ClientConf{
		Host: cfg.VodAPIEndpoint,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			NewHystrixRoundTripWrapper(hystrixCircuitName(vodapiName), cfg.VodAPIClientTimeout),
			NewRetryRoundTripWrapper(stats, vodapiName),
			cache.NewFallbackCacheRoundTripWrapper(fallbackHTTPCache, cfg.EnableFallbackCache, defaultFallbackTTL, defaultFallbackTimeout, cfg),
			cache.NewPrimaryCacheRoundTripWrapper(primaryHTTPCache, cfg.EnablePrimaryCache, vodapiTTL, cfg),
		},
		Stats: stats,
	}

	vodapiClient := vodapi.NewVodApiProtobufClient(cfg.VodAPIEndpoint, twitchclient.NewHTTPClient(vodapiConf))

	// DADS client
	dadsName := "dads"
	dadsTTL := time.Duration(cfg.DadsCacheTTLMinutes) * time.Minute
	dadsConf := twitchclient.ClientConf{
		Host: cfg.DadsEndpoint,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			NewHystrixRoundTripWrapper(hystrixCircuitName(dadsName), cfg.DadsClientTimeout),
			NewRetryRoundTripWrapper(stats, dadsName),
			cache.NewFallbackCacheRoundTripWrapper(fallbackHTTPCache, cfg.EnableFallbackCache, defaultFallbackTTL, defaultFallbackTimeout, cfg),
			cache.NewPrimaryCacheRoundTripWrapper(primaryHTTPCache, cfg.EnablePrimaryCache, dadsTTL, cfg),
		},
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: cfg.DadsMaxIdleConnections,
		},
		Stats: stats,
	}
	dadsClient := dads.NewDadsProtobufClient(cfg.DadsEndpoint, twitchclient.NewHTTPClient(dadsConf))

	// Nioh client
	niohName := "nioh"
	niohCacheTTL := time.Duration(cfg.NiohCacheTTLMilliseconds) * time.Millisecond
	niohConf := twitchclient.ClientConf{
		Host: cfg.NiohEndpoint,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			NewHystrixRoundTripWrapper(hystrixCircuitName(niohName), cfg.NiohClientTimeout),
			NewRetryRoundTripWrapper(stats, niohName),
			// No fallback cache for Nioh – we fail open at the application layer when a Nioh error surfaces
			cache.NewPrimaryCacheRoundTripWrapper(primaryHTTPCache, cfg.EnablePrimaryCache, niohCacheTTL, cfg),
		},
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: cfg.NiohMaxIdleConnections,
		},
		Stats: stats,
	}
	niohClient := nioh.NewAPIProtobufClient(cfg.NiohEndpoint, twitchclient.NewHTTPClient(niohConf))

	// Tailor client
	tailorName := "tailor"
	tailorCacheTTL := time.Duration(cfg.TailorCacheTTLMinutes) * time.Minute
	tailorConf := twitchclient.ClientConf{
		Host: cfg.TailorEndpoint,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			NewHystrixRoundTripWrapper(hystrixCircuitName(tailorName), cfg.TailorClientTimeout),
			NewRetryRoundTripWrapper(stats, tailorName),
			cache.NewFallbackCacheRoundTripWrapper(fallbackHTTPCache, cfg.EnableFallbackCache, defaultFallbackTTL, defaultFallbackTimeout, cfg),
			cache.NewPrimaryCacheRoundTripWrapper(primaryHTTPCache, cfg.EnablePrimaryCache, tailorCacheTTL, cfg),
		},
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: cfg.TailorMaxIdleConnections,
		},
		Stats: stats,
	}
	tailorClient := tailor.NewTailorProtobufClient(cfg.TailorEndpoint, twitchclient.NewHTTPClient(tailorConf))

	// Clips client
	clipsName := "clips"
	clipsCacheTTL := time.Duration(cfg.ClipsCacheTTLMinutes) * time.Minute
	clipsConf := twitchclient.ClientConf{
		Host: cfg.ClipsEndpoint,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			NewHystrixRoundTripWrapper(hystrixCircuitName(clipsName), cfg.ClipsClientTimeout),
			NewRetryRoundTripWrapper(stats, clipsName),
			cache.NewFallbackCacheRoundTripWrapper(fallbackHTTPCache, cfg.EnableFallbackCache, defaultFallbackTTL, defaultFallbackTimeout, cfg),
			cache.NewPrimaryCacheRoundTripWrapper(primaryHTTPCache, cfg.EnablePrimaryCache, clipsCacheTTL, cfg),
		},
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: cfg.ClipsMaxIdleConnections,
		},
		Stats: stats,
	}
	clipsClient, err := clips.NewClient(clipsConf)
	if err != nil {
		return nil, err
	}

	return &Clients{
		Clips:         clipsClient,
		Channels:      channelsClient,
		Hallpass:      hallpassClient,
		Nitro:         nitroClient,
		Sandstorm:     sandstorm,
		Signer:        signerClient,
		Stats:         stats,
		Subscriptions: subsClient,
		Voyager:       voyagerClient,
		Users:         usersClient,
		VodAPI:        vodapiClient,
		Dads:          dadsClient,
		Nioh:          niohClient,
		Tailor:        tailorClient,
	}, nil
}

func setupSigner(cfg *boConfig.Configuration, sandstorm *manager.Manager) (*signer.Signer, error) {
	secretName := sandstormHelper.BuildSandstormSecretName(cfg.SandstormEnvironment, cfg.SandstormKeyTokenSignatureKey)
	secret, err := sandstorm.Get(secretName)
	if err != nil {
		return nil, errors.Wrap(err, "Error getting sandstorm secret for signer key")
	}

	key := secret.Plaintext
	return signer.NewSigner(key)
}

func setupRedisClient(cfg *boConfig.Configuration, host string, cachePrefix string, stats statsd.Statter) (rediczar.ThickClient, error) {
	opts := &redefault.ClusterOpts{
		PoolSize: cfg.RedisNodeConnectionPoolSize,
		ReadOnly: true,
	}

	// redefault.NewClusterClient creates a redis cluster client with defaults (timeouts, redirects/retries, pool size) that are load-tested,
	// and rate-limits connection creation to keep a redis cluster from death spiraling.
	// Reference: https://git-aws.internal.justin.tv/chat/rediczar/blob/master/redefault/cluster.go
	client := redefault.NewClusterClient(host, opts)

	p := poolmetrics.Collector{
		Client: client,
		StatTracker: &statsdpoolmetrics.StatTracker{
			Stats: stats.NewSubStatter(fmt.Sprintf("redis.%s", cachePrefix)),
		},
		Interval: time.Duration(cfg.RedisMonitorIntervalMilliseconds) * time.Millisecond,
	}

	if err := p.Setup(); err != nil {
		return nil, err
	}

	// Start collecting pool metrics
	go func() {
		err := p.Start()
		if err != nil {
			fmt.Printf("error starting pool metrics: %v\n", err)
		}
	}()

	return &rediczar.Client{
		Commands: &rediczar.PrefixedCommands{
			KeyPrefix: cachePrefix,
			Redis:     client,
			RunMetrics: &statsdrunmetrics.StatTracker{
				Stats: stats.NewSubStatter(fmt.Sprintf("redis.%s.commands", cachePrefix)),
			},
		},
	}, nil
}

func hystrixCircuitName(serviceName string) string {
	return fmt.Sprintf("%s.%s", hystrixPrefix, serviceName)
}
