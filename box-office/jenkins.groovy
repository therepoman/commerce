job {
    name 'commerce-box-office'
    using 'TEMPLATE-autobuild'
    concurrentBuild true

    scm {
        git {
            remote {
                github 'commerce/box-office', 'ssh', 'git.xarth.tv'
                credentials 'git-aws-read-key'
            }
            clean true
        }
    }

    wrappers {
        sshAgent 'git-aws-read-key'
        preBuildCleanup()
        timestamps()
        credentialsBinding {
            file('COURIERD_PRIVATE_KEY', 'courierd')
            file('AWS_CONFIG_FILE', 'aws_config')
        }
    }

    steps {
        shell './scripts/docker_build.sh'
        shell './scripts/docker_push.sh'
        saveDeployArtifact 'commerce/box-office-deploy', 'deploy'
    }
}




freeStyleJob('commerce-box-office-prod-deploy') {
    using 'TEMPLATE-deploy-aws'

    wrappers {
        credentialsBinding {
            file('COURIERD_PRIVATE_KEY', 'courierd')
            file('AWS_CONFIG_FILE', 'aws_config')
            string 'AWS_ACCESS_KEY', 'twitch-box-office-aws-tcs-access-key'
            string 'AWS_SECRET_KEY', 'twitch-box-office-aws-tcs-secret-key'
        }
    }

    steps {
        // download .ebextensions into deploy/.ebextensions
        shell 'mkdir -p .ebextensions'
        downloadDeployArtifact 'commerce/box-office-deploy'
        shell 'rm -f *.manifest*'
        // deploy server
        shell """set +x
                |export AWS_SECRET_ACCESS_KEY=\$AWS_SECRET_KEY
                |export AWS_ACCESS_KEY_ID=\$AWS_ACCESS_KEY
                |export PYTHONUNBUFFERED=1
                |set -x
                |export IMAGE=docker.pkgs.xarth.tv/commerce-box-office:\$GIT_COMMIT
                |
                |cat <<EOF > Dockerrun.aws.json
                |{
                |  "AWSEBDockerrunVersion": "1",
                |  "Image": {
                |    "Name": "\$IMAGE",
                |    "Update": "false"
                |  },
                |  "Ports": [
                |    {
                |      "ContainerPort": "8000"
                |    }
                |  ],
                |  "Volumes": [
                |    {
                |      "ContainerDirectory": "/var/app",
                |      "HostDirectory": "/var/app"
                |    },
                |    {
                |      "ContainerDirectory": "/etc/ssl/certs/ca-bundle.crt",
                |      "HostDirectory": "/etc/ssl/certs/ca-bundle.crt"
                |    }
                |  ],
                |  "Logging": "/var/log/"
                |}
                |EOF
                |
                | zip -r artifact.zip Dockerrun.aws.json .ebextensions
                | eb labs cleanup-versions --num-to-leave 10 --older-than 5 --force -v --region us-west-2
                | eb deploy --timeout 30 prod-commerce-box-office-env
                | eb deploy --timeout 30 prod-commerce-box-office-canary-env""".stripMargin()
    }
}

freeStyleJob('commerce-box-office-prod-canary-deploy') {
    using 'TEMPLATE-deploy-aws'

    wrappers {
        credentialsBinding {
            file('COURIERD_PRIVATE_KEY', 'courierd')
            file('AWS_CONFIG_FILE', 'aws_config')
            string 'AWS_ACCESS_KEY', 'twitch-box-office-aws-tcs-access-key'
            string 'AWS_SECRET_KEY', 'twitch-box-office-aws-tcs-secret-key'
        }
    }

    steps {
        // download .ebextensions into deploy/.ebextensions
        shell 'mkdir -p .ebextensions'
        downloadDeployArtifact 'commerce/box-office-deploy'
        shell 'rm -f *.manifest*'
        // deploy server
        shell """set +x
                |export AWS_SECRET_ACCESS_KEY=\$AWS_SECRET_KEY
                |export AWS_ACCESS_KEY_ID=\$AWS_ACCESS_KEY
                |export PYTHONUNBUFFERED=1
                |set -x
                |export IMAGE=docker.pkgs.xarth.tv/commerce-box-office:\$GIT_COMMIT
                |
                |cat <<EOF > Dockerrun.aws.json
                |{
                |  "AWSEBDockerrunVersion": "1",
                |  "Image": {
                |    "Name": "\$IMAGE",
                |    "Update": "false"
                |  },
                |  "Ports": [
                |    {
                |      "ContainerPort": "8000"
                |    }
                |  ],
                |  "Volumes": [
                |    {
                |      "ContainerDirectory": "/var/app",
                |      "HostDirectory": "/var/app"
                |    },
                |    {
                |      "ContainerDirectory": "/etc/ssl/certs/ca-bundle.crt",
                |      "HostDirectory": "/etc/ssl/certs/ca-bundle.crt"
                |    }
                |  ],
                |  "Logging": "/var/log/"
                |}
                |EOF
                |
                | zip -r artifact.zip Dockerrun.aws.json .ebextensions
                | eb labs cleanup-versions --num-to-leave 10 --older-than 5 --force -v --region us-west-2
                | eb deploy --timeout 30 prod-commerce-box-office-canary-env""".stripMargin()
    }
}


freeStyleJob('commerce-box-office-dev-deploy') {
    using 'TEMPLATE-deploy-aws'

    wrappers {
        credentialsBinding {
            file('COURIERD_PRIVATE_KEY', 'courierd')
            file('AWS_CONFIG_FILE', 'aws_config')
            string 'AWS_ACCESS_KEY', 'twitch-box-office-dev-tcs-access-key'
            string 'AWS_SECRET_KEY', 'twitch-box-office-dev-tcs-secret-key'
        }
    }

    steps {
        // download .ebextensions into deploy/.ebextensions
        shell 'mkdir -p .ebextensions'
        downloadDeployArtifact 'commerce/box-office-deploy'
        shell 'rm -f *.manifest*'
        // deploy server
        shell """set +x
                |export AWS_SECRET_ACCESS_KEY=\$AWS_SECRET_KEY
                |export AWS_ACCESS_KEY_ID=\$AWS_ACCESS_KEY
                |export PYTHONUNBUFFERED=1
                |set -x
                |export IMAGE=docker.pkgs.xarth.tv/commerce-box-office:\$GIT_COMMIT
                |
                |cat <<EOF > Dockerrun.aws.json
                |{
                |  "AWSEBDockerrunVersion": "1",
                |  "Image": {
                |    "Name": "\$IMAGE",
                |    "Update": "false"
                |  },
                |  "Ports": [
                |    {
                |      "ContainerPort": "8000"
                |    }
                |  ],
                |  "Volumes": [
                |    {
                |      "ContainerDirectory": "/var/app",
                |      "HostDirectory": "/var/app"
                |    },
                |    {
                |      "ContainerDirectory": "/etc/ssl/certs/ca-bundle.crt",
                |      "HostDirectory": "/etc/ssl/certs/ca-bundle.crt"
                |    }
                |  ],
                |  "Logging": "/var/log/"
                |}
                |EOF
                |
                | zip -r artifact.zip Dockerrun.aws.json .ebextensions
                | eb labs cleanup-versions --num-to-leave 10 --older-than 5 --force -v --region us-west-2
                | eb deploy dev-commerce-box-office-env""".stripMargin()
    }
}


freeStyleJob('commerce-box-office-staging-deploy') {
    using 'TEMPLATE-deploy-aws'

    wrappers {
        credentialsBinding {
            file('COURIERD_PRIVATE_KEY', 'courierd')
            file('AWS_CONFIG_FILE', 'aws_config')
            string 'AWS_ACCESS_KEY', 'twitch-box-office-dev-tcs-access-key'
            string 'AWS_SECRET_KEY', 'twitch-box-office-dev-tcs-secret-key'
        }
    }

    steps {
        // download .ebextensions into deploy/.ebextensions
        shell 'mkdir -p .ebextensions'
        downloadDeployArtifact 'commerce/box-office-deploy'
        shell 'rm -f *.manifest*'
        // deploy server
        shell """set +x
                |export AWS_SECRET_ACCESS_KEY=\$AWS_SECRET_KEY
                |export AWS_ACCESS_KEY_ID=\$AWS_ACCESS_KEY
                |export PYTHONUNBUFFERED=1
                |set -x
                |export IMAGE=docker.pkgs.xarth.tv/commerce-box-office:\$GIT_COMMIT
                |
                |cat <<EOF > Dockerrun.aws.json
                |{
                |  "AWSEBDockerrunVersion": "1",
                |  "Image": {
                |    "Name": "\$IMAGE",
                |    "Update": "false"
                |  },
                |  "Ports": [
                |    {
                |      "ContainerPort": "8000"
                |    }
                |  ],
                |  "Volumes": [
                |    {
                |      "ContainerDirectory": "/var/app",
                |      "HostDirectory": "/var/app"
                |    },
                |    {
                |      "ContainerDirectory": "/etc/ssl/certs/ca-bundle.crt",
                |      "HostDirectory": "/etc/ssl/certs/ca-bundle.crt"
                |    }
                |  ],
                |  "Logging": "/var/log/"
                |}
                |EOF
                |
                | zip -r artifact.zip Dockerrun.aws.json .ebextensions
                | eb labs cleanup-versions --num-to-leave 10 --older-than 5 --force -v --region us-west-2
                | eb deploy staging-commerce-box-office-env
                | eb deploy staging-commerce-box-office-canary-env""".stripMargin()
    }
}

freeStyleJob('commerce-box-office-staging-canary-deploy') {
    using 'TEMPLATE-deploy-aws'

    wrappers {
        credentialsBinding {
            file('COURIERD_PRIVATE_KEY', 'courierd')
            file('AWS_CONFIG_FILE', 'aws_config')
            string 'AWS_ACCESS_KEY', 'twitch-box-office-dev-tcs-access-key'
            string 'AWS_SECRET_KEY', 'twitch-box-office-dev-tcs-secret-key'
        }
    }

    steps {
        // download .ebextensions into deploy/.ebextensions
        shell 'mkdir -p .ebextensions'
        downloadDeployArtifact 'commerce/box-office-deploy'
        shell 'rm -f *.manifest*'
        // deploy server
        shell """set +x
                |export AWS_SECRET_ACCESS_KEY=\$AWS_SECRET_KEY
                |export AWS_ACCESS_KEY_ID=\$AWS_ACCESS_KEY
                |export PYTHONUNBUFFERED=1
                |set -x
                |export IMAGE=docker.pkgs.xarth.tv/commerce-box-office:\$GIT_COMMIT
                |
                |cat <<EOF > Dockerrun.aws.json
                |{
                |  "AWSEBDockerrunVersion": "1",
                |  "Image": {
                |    "Name": "\$IMAGE",
                |    "Update": "false"
                |  },
                |  "Ports": [
                |    {
                |      "ContainerPort": "8000"
                |    }
                |  ],
                |  "Volumes": [
                |    {
                |      "ContainerDirectory": "/var/app",
                |      "HostDirectory": "/var/app"
                |    },
                |    {
                |      "ContainerDirectory": "/etc/ssl/certs/ca-bundle.crt",
                |      "HostDirectory": "/etc/ssl/certs/ca-bundle.crt"
                |    }
                |  ],
                |  "Logging": "/var/log/"
                |}
                |EOF
                |
                | zip -r artifact.zip Dockerrun.aws.json .ebextensions
                | eb labs cleanup-versions --num-to-leave 10 --older-than 5 --force -v --region us-west-2
                | eb deploy staging-commerce-box-office-canary-env""".stripMargin()
    }
}

