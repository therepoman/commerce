package token

// VODAccessToken is the NAuth access token used to grant access to VODs on twitch. This token
// is consumed by Usher to grant/reject access to video.
// Reference links:
//   - Rails legacy VOD token hydration: https://git-aws.internal.justin.tv/web/web/blob/master/app/controllers/api/vods_controller.rb#L28
type VODAccessToken struct {

	// Authorization represents whether or not a user is authorized to view this video
	Authorization Authorization `json:"authorization"`

	// ChanSub is Channel Subscription information
	ChanSub VODChanSub `json:"chansub"`

	// DeviceID is the unique identifier of the device requesting the token
	// Example: "4f492cafac28d907"
	DeviceID *string `json:"device_id"`

	// Expires specifies when this access token expires.
	// Example: 1519311745
	Expires int64 `json:"expires"`

	// HTTPSRequired specifies whether HTTPS must be used for this video.
	// Example: false
	HTTPSRequired bool `json:"https_required"`

	// Privileged specifies whether the user has elevated video privileges or not
	// Example: false
	Privileged bool `json:"privileged"`

	// UserID is the ID of the user requesting this access token.
	// Example: 129802180
	UserID *int64 `json:"user_id"`

	// Version of the token. Allows the token schema to be updated and inform Usher towards which
	// schema it should process.
	Version int `json:"version"`

	// VODID is the ID of the VOD being requested.
	// Example: 238564302
	VODID *int64 `json:"vod_id"`
}

// VODChanSub is Channel Subscription information
type VODChanSub struct {
	// RestrictedBitrates specifies which video bitrates the user is prohibited from viewing.
	// Example: ["high", "source", "chunked", "archives"]
	RestrictedBitrates []string `json:"restricted_bitrates"`
}
