package token

// VideoAccessToken is the NAuth access token used to grant access to live video on twitch. This token
// is consumed by Usher to grant/reject access to video.
// Reference links:
//   - Rails legacy video token hydration: https://git-aws.internal.justin.tv/web/web/blob/master/app/controllers/api/channels_controller.rb#L79
type VideoAccessToken struct {

	// Adblock specifies whether or not the requesting user has Adblock enabled.
	// Example: false
	Adblock bool `json:"adblock"`

	// Authorization represents whether or not a user is authorized to view this video
	Authorization Authorization `json:"authorization"`

	// If zipcode level geoblocking is enabled for the channel, BlackoutEnabled is set to true
	BlackoutEnabled bool `json:"blackout_enabled"`

	// Channel is the name of the channel that this token is granting access to.
	// Example: "lirik"
	Channel string `json:"channel"`

	// ChannelID is the unique identifier of the channel that this token is granting access to.
	// Example: 23161357
	ChannelID *int64 `json:"channel_id"`

	// ChanSub is Channel Subscription information
	ChanSub VideoChanSub `json:"chansub"`

	// ChannelIsGeoBlocked specifies whether or not the channel is geo-blocked
	// Example: false
	ChannelIsGeoBlocked bool `json:"ci_gb"`

	// GeoblockReason is the reason why the request is blocked
	// Example: "VPN / Proxy Blocking"
	GeoblockReason string `json:"geoblock_reason"`

	// DeviceID is the unique identifier of the device requesting the token
	// Example: "4f492cafac28d907"
	DeviceID *string `json:"device_id"`

	// Expires specifies when this access token expires.
	// Example: 1519311745
	Expires int64 `json:"expires"`

	// ExtendedHistoryAllowed specifies whether this token can fetch the extended manifest
	// Example: false
	ExtendedHistoryAllowed bool `json:"extended_history_allowed"`

	// Game specifies the game being played on the requested channel.
	// Example: "playerunknown_s_battlegrounds"
	Game string `json:"game"`

	// HideAds specifies whether or not Ads should be hidden.
	// Example: false
	HideAds bool `json:"hide_ads"`

	// HTTPSRequired specifies whether HTTPS must be used for this video.
	// Example: false
	HTTPSRequired bool `json:"https_required"`

	// Mature specifies whether the requested channel has set the Mature flag.
	// Example: false
	Mature bool `json:"mature"`

	// Partner specifies whether or not the requested channel is in the partner program.
	// Example: false
	Partner bool `json:"partner"`

	// Platform is the plaftorm the requesting user is on.
	// Example: "web"
	Platform *string `json:"platform"`

	// PlayerType is the type type of player the requesting user is using.
	// Example: "site"
	PlayerType *string `json:"player_type"`

	// Private encompasses fields related to channel privacy
	Private Private `json:"private"`

	// Privileged specifies whether the user has elevated video privileges or not
	// Example: false
	Privileged bool `json:"privileged"`

	// Role specifies the possible role that a user could be, currently it is used to determine whether a user is Staff
	// Example: "" or "staff"
	Role string `json:"role"`

	// ServerAds specifies whether or not the requesting user should receive a stream that supports server-side ad-insertion.
	// Example: false
	ServerAds bool `json:"server_ads"`

	// ShowAds specifies whether or not Ads should be shown.
	// Example: false
	ShowAds bool `json:"show_ads"`

	// Subscriber specifies whether or not the requesting user is subscribed to the requested channel.
	// Example: false
	Subscriber bool `json:"subscriber"`

	// Turbo specifies whether or not the requesting user has a Turbo account.
	// Example: false
	Turbo bool `json:"turbo"`

	// UserID is the ID of the user requesting this access token.
	// Example: 129802180
	UserID *int64 `json:"user_id"`

	// UserIP is the ip address of the user requesting this access token.
	// Example: 52.95.255.100
	UserIP string `json:"user_ip"`

	// Version of the token. Allows the token schema to be updated and inform Usher towards which
	// schema it should process.
	// Example: 2
	Version int `json:"version"`
}

// VideoChanSub is Channel Subscription information
type VideoChanSub struct {

	// RestrictedBitrates specifies which video bitrates the user is prohibited from viewing.
	// Example: ["high", "source", "chunked", "archives"]
	RestrictedBitrates []string `json:"restricted_bitrates"`

	// ViewUntil specifies until what timestamp the user is allowed to view the video.
	// Example: 1924905600
	ViewUntil int64 `json:"view_until"`
}

// Private encompasses fields related to channel privacy
type Private struct {

	// AllowedToView specifies whether the user is allowed to view this video.
	// Example: false
	AllowedToView bool `json:"allowed_to_view"`
}

// Authorization represents whether or not the user is authorized to view the video.
//
// Note: there are many fields in this legacy token that try to answer this question of authorization.
// This is an attempt to try a new generic approach with a reason without impacting any legacy fields.
// This should help influence future token schema decisions depending on how successful this approach is.
type Authorization struct {

	// AccessExpiration specifies when the generated Gatekeeper token should expire.
	// Used for free previews of restricted, premium content. Omitted if no restriction is necessary.
	// Example: 1924905600
	AccessExpiration *int64 `json:"access_expiration,omitempty"`

	// Forbidden specifies whether or not the user is forbidden from viewing the video.
	Forbidden bool `json:"forbidden"`

	// Reason specifies the reason why the user is forbidden. This string is empty if the user is not forbidden.
	Reason string `json:"reason"`
}
