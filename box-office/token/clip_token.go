package token

type ClipAccessToken struct {
	// Authorization represents whether or not a user is
	// authorized to view this clip
	Authorization Authorization `json:"authorization"`

	// ClipURI is the S3 URI of the clip content that this token is for
	ClipURI string `json:"clip_uri"`

	// DeviceID is the unique identifier of the device requesting the token
	DeviceID *string `json:"device_id"`

	// Expires specifies when this access token expires.
	Expires int64 `json:"expires"`

	// UserID is the ID of the user requesting this access token.
	UserID string `json:"user_id"`

	// Version of the token. Allows the token schema to be updated
	// and inform Usher towards which schema it should process.
	Version int `json:"version"`
}
