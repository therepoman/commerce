# box-office
| Environment     | Endpoint                                              |
| --------------- |-------------------------------------------------------|
| Staging         | http://main.us-west-2.beta.box-office.twitch.a2z.com  |
| Prod Canary     | http://prod-commerce-box-office-canary-env.n6aa8jvzf3.us-west-2.elasticbeanstalk.com |
| Production      | http://main.us-west-2.prod.box-office.twitch.a2z.com  |

## Overview

Box Office is the Twitch video access token service. Access tokens are the mechanism by which a user obtains access to a video. Currently, there are separate access tokens for live video and VODs. This service vends both tokens.

Further reading:
 - [Video NAuth token overview](https://docs.google.com/document/d/17Mu9OqS_BKZFhFJrNjI6w2OFSgtX6co7MZWQb1oDUmc/edit#)
 - [Video NAuth token wexit doc](https://docs.google.com/document/d/1GPXym_q-MDrax6TMGx0yqB7J88nOnyjzfSmuaoA6pRg/edit#)
 - [VOD NAuth token wexit doc](https://docs.google.com/document/d/17c_-22VGjQCO4qUjvZDCUxuJNui5JnCn5CBEjKp9Prg/edit#)

## Setting up your workspace

### Go
 - Install Go:

```
$ brew install go
```

 - Create a Go workspace folder in your home directory. Ex:

```
$ mkdir /Users/<username>/GoWorkspace
```

 - Set your environment variable for GOPATH (in your .bashrc, .zshrc, or etc.)

```
$ export GOPATH=/Users/<username>/GoWorkspace
```

 - Append GOPATH/bin to PATH environment variable

```
$ export PATH=$PATH:$GOPATH/bin
```

 - Clone down Box Office source within the new commerce folder. Make sure you added your ssh key to Github (https://git.xarth.tv/settings/keys)

```
$ git clone git@git.xarth.tv:commerce/box-office.git $GOPATH/src/code.justin.tv/commerce/box-office
```

 - Directory structure will now look like this:

```
$ /Users/<username>/GoWorkspace/src/code.justin.tv/commerce/box-office
```

 ### AWS

**NOTE**: This section is vastly outdated. See [this page](https://git.xarth.tv/pages/subs/docs/getting-started/aws-console/) for details on the new method of AWS authentication.


Running Box Office locally requires an IAM user with access credentials on the appropriate AWS account. [Ensure you have the AWS CLI installed](https://docs.aws.amazon.com/cli/latest/userguide/installing.html), then set up your IAM user and credentials:

1. In [Isengard](https://isengard.amazon.com), access the Admin console for the Box Office account you need credentials for (either `twitch-box-office-aws` for production or `twitch-box-office-dev` for development).
2. Navigate to [IAM](https://console.aws.amazon.com/iam/home) and visit the Users tab on the left.
3. Click "Add user" in the top left. Enter a username and check "Programmatic access" as the access type. Go to the next step.
4. Attach "AdministratorAccess" permission policy. Complete the creation flow.
5. At the final step, you should be shown the new user's `AWS_ACCESS_KEY` and `AWS_SECRET_ACCESS_KEY`. You'll use these to create your local AWS profile. Note that after this point you cannot view the secret access key again; if you lose the secret access key, you'll have to create new credentials.
6. In your terminal, run `aws configure --profile twitch-box-office-dev` (or `--profile twitch-box-office-aws` as appropriate). Enter the access key and secret access key you just created when the CLI prompts you.

## Developing in Box Office
### Make sure you are running go1.15.4 or above

### Running the service locally
- To run the service locally:

```
$ make dev
```

**Note**: Make sure you have the right AWS credentials profile (**twitch-box-office-dev**) before running this command. These will be the stored in the `~/.aws/credentials` file.

- To perform all the validation and static analysis prior to PR:

```
$ make release
```

### Testing
```
$ make test
```

### Calling the service in staging

You must be on the **TwitchVPN** in order to call the APIs in Staging or Prod. Deploy your feature branch to staging as described below,
then you can use the auto-generated Insomnia environment to send requests to the service.

### Deployment
1. Deploy your feature branch to staging using the [deployment tool](https://deploy.xarth.tv/#/commerce/box-office).
2. Perform necessary tests.
3. Open a Pull Request (run `make release` before you submit PR).
4. Once PR is approved, create a [Change Request](https://docs.google.com/document/d/1EO47I3dntqyhGJkeow_OPLliP_Jb56lFhZQd4DbNReE/edit#) on SUBS project.
6. Rebase master on your pull request branch.
7. Notify #box-office slack channel that you are taking canary.
7. Deploy PR branch to prod-canary.
8. Test your changes on Canary host, monitor Grafana and Rollbar.
9. Squash and merge your feature branch into `master`.
10. Ensure that your build succeeds on `master`.
11. Deploy `master` to production using the [deployment tool](https://deploy.xarth.tv/#/commerce/box-office).
12. Monitor [Grafana](https://grafana.internal.justin.tv/dashboard/db/box-office)

### Dependency Management
Box Office's dependencies are managed with [go modules](https://golang.org/ref/mod)

#### Updating Dependencies
If you make changes that require adding a new dependency or updating an existing one, running the following command will update go.mod and add them to the vendor folder:
```
$ make mod
```

#### Adding Specific Dependencies
If you need to grab a specific version of a dependency, run `go get` and specify a version or a commit hash to add a new dependency or update an existing one. Example:
```
$ go get github.com/golang/protobuf@v1.4.3
```

You must then run `make vendor` to update the dependency in the vendor folder.

### Mocks
- Use [Testify](https://github.com/stretchr/testify) to mock out behaviour in tests
- Use [Mockery](https://github.com/vektra/mockery) to autogenerate mock classes

Example mockery generation:

```
$ mockery -dir=clients/sns -name=ISNSClient
```

### Running Terraform

__WARNING__: Running terraform can be extremely dangerous. If you're unfamiliar with making changes to Terraform in Box Office, please ask for help in #box-office channel on Slack

Note: Box Office uses Terraform 0.13.4

1. Go to box-office/terraform/development (or whichever environment you want to change)
2. Run `terraform get` to pull in any terraform modules
3. Run `terraform init` to initialize the working directory
4. Run `terraform plan` to examine what changes are about to be made. Read the output careful to make sure it is correct.
   *Note*: You may need to target a specific sub-module, like `terraform plan -target=module.box-office.module.privatelink.module.privatelink`
5. Run `terraform apply` to make the changes.

Note: the human-readable endpoints such as box-office-dev.internal.justin.tv were manually created in Twitch's DNS tool on the dashboard, not through terraform.

### Sample Requests
Below are some sample cURL requests to box office's staging environment provided for convenience:

#### Get a Video Access Token
```
$ curl -POST http://main.us-west-2.beta.box-office.twitch.a2z.com/twirp/code.justin.tv.commerce.boxoffice.BoxOffice/GetVideoAccessToken -H 'content-type: application/json' -d '{"user_id": "56972503", "channel_id": "39894746"}'
```

#### Get a VOD Access Token
```
$ curl -POST https://main.us-west-2.beta.box-office.twitch.a2z.com/twirp/code.justin.tv.commerce.boxoffice.BoxOffice/GetVODAccessToken -H 'content-type: application/json' -d '{"vod_id": "813831147", "country_code": "US"}'
```

## Operations
The Box-Office runbook can be found [here](https://docs.google.com/document/d/1B9z6OVhMZOv-H4hRQlsCX4JIkrfuBR3AuF8HiWjHCk0/edit#heading=h.u4rgt75bnoj1). It includes basic operational information regarding deployments, monitoring, scaling up/down, and emergency contact information.
