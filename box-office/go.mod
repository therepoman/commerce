module code.justin.tv/commerce/box-office

go 1.16

require (
	code.justin.tv/ads/dads v0.0.0-20180709214922-55c9e93d1a10
	code.justin.tv/ads/ehrmantraut v0.0.0-20180414174855-80ad1e128cc7 // indirect
	code.justin.tv/ads/fring v0.0.0-20180414015022-fc3654a191ec // indirect
	code.justin.tv/amzn/TwitchLogging v0.0.0-20190731182733-d8aae132db1f // indirect
	code.justin.tv/amzn/TwitchProcessIdentifier v0.0.0-20191004180637-dc817f563e55 // indirect
	code.justin.tv/amzn/TwitchTelemetry v0.0.0-20191113205906-40e3e1aa55c5 // indirect
	code.justin.tv/amzn/TwitchTelemetryCloudWatchMetricsSender v0.0.0-20190822201853-9acf2b6ccaa1 // indirect
	code.justin.tv/amzn/TwitchTelemetryMWSMetricsSender v0.0.0-20190731182749-aa2dfbb7fe29 // indirect
	code.justin.tv/amzn/TwitchVoyagerTwirp v0.0.0-20210304185727-d1b4c5bfb6e0
	code.justin.tv/cb/hallpass v0.0.0-20180308223643-1dc35f38973f
	code.justin.tv/chat/rediczar v1.6.0
	code.justin.tv/chat/timing v1.0.1 // indirect
	code.justin.tv/commerce/config v0.0.0-20180202175027-9870c66b280a
	code.justin.tv/commerce/dynamicconfig v1.1.1
	code.justin.tv/commerce/nioh v1.15.0
	code.justin.tv/commerce/sandstorm-client v0.0.0-20200521180957-61f3fd64c3ee
	code.justin.tv/commerce/splatter v1.5.3-0.20200128212244-2db0e28eda75
	code.justin.tv/common/chitin v0.15.0 // indirect
	code.justin.tv/common/config v0.0.0-20171107233232-08df7fd7798a // indirect
	code.justin.tv/common/golibs v1.0.3 // indirect
	code.justin.tv/common/gometrics v0.0.0-20171010235942-7491402d56a5 // indirect
	code.justin.tv/common/twirp v4.9.0+incompatible
	code.justin.tv/common/yimg v0.0.0-20170724203250-43a144c6fe85 // indirect
	code.justin.tv/feeds/ctxlog v0.0.0-20170328220514-514fab8fb5b7 // indirect
	code.justin.tv/feeds/log v1.1.1 // indirect
	code.justin.tv/foundation/twitchclient v4.11.1-0.20200529172019-3413b9e34bbc+incompatible
	code.justin.tv/foundation/twitchserver v1.8.0
	code.justin.tv/foundation/xray v0.0.0-20171002153549-42df0dc4ee6b // indirect
	code.justin.tv/release/trace v0.0.0-20180309214621-3bebff953ce8 // indirect
	code.justin.tv/revenue/subscriptions v0.0.0-20180327021047-13718d286438
	code.justin.tv/samus/nitro v0.0.0-20180406211729-74c636604c96
	code.justin.tv/systems/sandstorm v1.6.6
	code.justin.tv/video/asset-transcoder v0.0.0-20180502000252-544d6072aeae // indirect
	code.justin.tv/video/clips-upload v1.0.4
	code.justin.tv/video/mwsclient v1.1.0 // indirect
	code.justin.tv/video/protocols v0.0.0-20180419195237-9d8ae0788302 // indirect
	code.justin.tv/vod/tailor v0.0.0-20191206225056-4c124e22c942
	code.justin.tv/vod/vodapi v0.0.0-20181011223247-ff22c07d87da
	code.justin.tv/web/users-service v0.0.0-20180410230441-91f1390f2928
	github.com/afex/hystrix-go v0.0.0-20180406012432-f86abeeb9f72
	github.com/aws/aws-lambda-go v1.23.0 // indirect
	github.com/aws/aws-sdk-go v1.33.19
	github.com/cactus/go-statsd-client/statsd v0.0.0-20190805010426-5089fcbbe532
	github.com/cenkalti/backoff v2.2.1+incompatible // indirect
	github.com/go-redis/redis/v7 v7.0.0-beta.4
	github.com/gofrs/uuid v3.3.0+incompatible // indirect
	github.com/golang/protobuf v1.4.3
	github.com/hashicorp/go-multierror v1.0.0 // indirect
	github.com/heroku/rollbar v0.5.0 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/kr/logfmt v0.0.0-20210122060352-19f9bcb100e6 // indirect
	github.com/mitchellh/mapstructure v1.1.2
	github.com/nicksrandall/dataloader v4.1.1-0.20180104184831-78139374585c+incompatible
	github.com/nsf/jsondiff v0.0.0-20200515183724-f29ed568f4ce
	github.com/opentracing/opentracing-go v1.0.2 // indirect
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.4.2
	github.com/smartystreets/goconvey v1.6.4-0.20180222194500-ef6db91d284a
	github.com/stretchr/testify v1.6.1
	github.com/twitchtv/twirp v5.10.1+incompatible
	goji.io v2.0.0+incompatible
	golang.org/x/net v0.0.0-20200202094626-16171245cfb2
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.0.0-00010101000000-000000000000 // indirect
	gopkg.in/validator.v2 v2.0.0-20180205153750-59c90c7046f6 // indirect
)

replace github.com/go-redis/redis/v7 => code.justin.tv/chat/goredis/v7 v7.0.0-beta.4

replace gopkg.in/DATA-DOG/go-sqlmock.v1 => github.com/DATA-DOG/go-sqlmock v1.3.3
