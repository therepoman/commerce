package main

import (
	"time"

	boConfig "code.justin.tv/commerce/box-office/config"
	"code.justin.tv/commerce/box-office/config/constants"
	"code.justin.tv/commerce/box-office/config/dynamic"
	"code.justin.tv/commerce/box-office/internal/api"
	"code.justin.tv/commerce/box-office/internal/clients"
	"code.justin.tv/commerce/box-office/internal/loaders"
	"code.justin.tv/commerce/box-office/internal/resolvers/helpers"
	"code.justin.tv/commerce/box-office/internal/stats"
	boStats "code.justin.tv/commerce/box-office/internal/stats"
	"code.justin.tv/commerce/config"
	"code.justin.tv/commerce/dynamicconfig"
	"code.justin.tv/foundation/twitchserver"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

var (
	ballast      []byte
	ballastBytes = map[string]int{
		constants.ProdEnvironment:          3 << 30,
		constants.ProdCanaryEnvironment:    3 << 30,
		constants.StagingEnvironement:      3 << 30,
		constants.StagingCanaryEnvironment: 3 << 30,
		constants.Development:              0,
	}
)

func main() {
	environment := config.GetEnvironment()
	logLevel := log.DebugLevel
	if environment == constants.ProdEnvironment || environment == constants.ProdCanaryEnvironment {
		logLevel = log.InfoLevel
	}

	log.SetLevel(logLevel)
	log.SetFormatter(&log.TextFormatter{FullTimestamp: true, TimestampFormat: "02/Jan/2006:15:04:05 -0700"})
	log.Infof("Running Box Office in environment: %s", environment)

	log.Info("Allocating memory ballast")
	// Intentionally initialized and not used, forces a large allocation on to the heap
	ballast = makeBallast(config.GetEnvironment())
	log.Info("Finished allocating memory ballast")

	cfg, err := setupConfiguration()
	if err != nil {
		log.Fatalf("Failed to initialize configuration: %+v", err)
	}

	darklaunchDynamicConfig, err := dynamic.GetDarklaunchDynamicConfig(cfg.AWSRegion, cfg.DynamicGeoblockingDarklaunchKey)
	if err != nil {
		log.Warnf("Could not create geoblocking darklaunch dynamic config, will fall back to fixed geoblocking config. %+v", err)
	} else {
		darklaunchPoller := dynamicconfig.Poller{
			Reloader: darklaunchDynamicConfig,
			Interval: 30 * time.Second,
			Timeout:  10 * time.Second,
		}
		go darklaunchPoller.Poll()
		defer darklaunchPoller.Stop()

		cfg.DynamicGeoblockingDarklaunch = dynamic.NewDarklaunchGetter(darklaunchDynamicConfig, cfg.DynamicGeoblockingDarklaunchKey)
	}

	geoblockingDynamicConfig, err := dynamic.GetGeoblockingDynamicConfig(cfg.DynamicGeoblockingBucket, cfg.DynamicGeoblockingFilename)
	if err != nil {
		log.Errorf("Failed to initialize geoblocking dynamic config: %+v", err)
	} else {
		poller := dynamicconfig.Poller{
			Reloader: geoblockingDynamicConfig,
			Interval: 30 * time.Second,
			Timeout:  10 * time.Second,
		}
		go poller.Poll()
		defer poller.Stop()

		cfg.DynamicGeoblocking = dynamic.NewDynamicGeoblocking(geoblockingDynamicConfig)
	}

	stats, err := stats.SetupStats(*cfg, config.GetEnvironment())
	if err != nil {
		log.Fatalf("Failed to initialize statsd client: %+v", err)
	}

	clients, err := clients.SetupClients(cfg, stats)
	if err != nil {
		log.Fatalf("Failed to initialize clients: %+v", err)
	}

	roleReloader, err := helpers.NewRolesReloader(cfg.UsersServiceEndpoint, stats)
	if err != nil {
		log.Errorf("Failed to initialize staff roles reloader: %+v", err)
	} else {
		rolePoller := dynamicconfig.Poller{
			Reloader: roleReloader,
			Interval: 1 * time.Hour,
			Timeout:  3 * time.Second,
			OnError:  roleReloader.RetryAndHandleError(2),
		}
		go rolePoller.Poll()
		defer rolePoller.Stop()
	}

	err = startWebServer(cfg, clients, stats)
	if err != nil {
		log.Fatalf("Error during server shutdown: %+v", err)
	}
	log.Info("Shutdown complete.")
}

// Set up Box Office service configuration. Returns the built configuration.
func setupConfiguration() (*boConfig.Configuration, error) {
	env := config.GetEnvironment()
	options := &config.Options{
		RepoOrganization: constants.OrganizationName,
		RepoService:      constants.ServiceName,
		Environment:      env,
	}
	log.Infof("Loading configuration for environment: %s", env)
	cfg := &boConfig.Configuration{}
	err := config.LoadConfigForEnvironment(cfg, options)
	if err != nil {
		return nil, errors.Wrap(err, "Error loading configuration")
	}

	return cfg, nil
}

// Creates a large heap allocation - See https://git-aws.internal.justin.tv/edge/visage/pull/3538
func makeBallast(environment string) []byte {
	return make([]byte, ballastBytes[environment])
}

// Set up the web server start listening for requests. The main thread will not return out of this method until
// the web server is shut down via a termination signal.
func startWebServer(config *boConfig.Configuration, clients *clients.Clients, stats statsd.Statter) error {
	loaders := loaders.NewAttachers(clients)

	platformRecorder := &boStats.PlatformRecorder{
		Stats: stats,
	}

	getClipAccessTokenAPI := api.GetClipAccessTokenAPI{
		Clients:          clients,
		Config:           config,
		Loaders:          loaders,
		PlatformRecorder: platformRecorder,
		Stats:            stats,
	}

	getVideoAccessTokenAPI := api.GetVideoAccessTokenAPI{
		Clients:          clients,
		Config:           config,
		Loaders:          loaders,
		PlatformRecorder: platformRecorder,
		Stats:            stats,
	}

	getVODAccessTokenAPI := api.GetVODAccessTokenAPI{
		Clients:          clients,
		Config:           config,
		Loaders:          loaders,
		PlatformRecorder: platformRecorder,
		Stats:            stats,
	}

	boxOfficeServer := api.BoxOfficeServer{
		GetClipAccessTokenAPI:  getClipAccessTokenAPI,
		GetVideoAccessTokenAPI: getVideoAccessTokenAPI,
		GetVODAccessTokenAPI:   getVODAccessTokenAPI,
	}

	server, err := boxOfficeServer.NewServer(stats)
	if err != nil {
		log.Fatal(err)
	}

	log.Info("Starting Box Office server")

	serverConfig := twitchserver.NewConfig()
	serverConfig.Statter = stats
	twitchserver.AddDefaultSignalHandlers()
	return twitchserver.ListenAndServe(server, serverConfig)
}
