package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"code.justin.tv/commerce/box-office/token"

	log "github.com/sirupsen/logrus"

	borpc "code.justin.tv/commerce/box-office/rpc"
)

const (
	railsEndpoint  = "api.twitch.tv"
	visageEndpoint = "visage.prod.us-west2.justin.tv"
)

func main() {
	log.Info("Running Token Validator.")

	// Validator Config
	fastMode := true    // running all combinations of inputs is several thousand tests, and takes a while. Fast mode is only core use cases
	endOnError := false // end the run when an error is found. if false, will keep running remaining tests
	runVODTests := true
	runVideoTests := true

	if runVODTests {
		runAllVODTests(fastMode, endOnError)
	}

	if runVideoTests {
		runAllVideoTests(fastMode, endOnError)
	}

	log.Info("Token Validator complete.")
}

func runAllVODTests(fastMode bool, endOnError bool) {
	log.Info("=== Running VOD Tests ===")
	totalRuns := 0
	failedRuns := 0
	vodList := []string{
		"249666950", // ryanresivod private
		"249667006", // ryanresivod public
		"249612060", // geekandsundry sub-only archive
		"244387641", // geekandsundry sub-only upload
		"231906452", // geekandsundry sub-only premiere
		"40790514",  // shroud public highlight
	}

	oauthList := []string{
		"7ggbcm1cnhdbg43h237os1dygoiuv2", // ryanresi
		"t4hip66breinu56akaf186xzrx3kph", // ryanresivod
		"jlimuqqzatg90n4vhqxl7ui0vr997w", // ryanresieditor
		"hjdhickhbv7jvnqp7yu59ke9tseshj", // ryanresiturbo2
		"zyzqe7ulzv5ee3wehspltq09amvdwk", // ryanresisubg
		"pr8t9op66sq7i9bunmrmmy3kldwq3e", // ryanresisubsh
		"",                               // anonymous
	}

	shortNeedHTTPSList := []string{
		"true",
	}

	comprehensiveNeedHTTPSList := []string{
		"true",
		"false",
		"",
	}

	shortPlatformList := []string{
		"web",
	}

	comprehensivePlatformList := []string{
		"web",
		"other_platform",
		"",
	}

	shortPlayerTypeList := []string{
		"site",
	}

	comprehensivePlayerTypeList := []string{
		"site",
		"other_player",
		"",
	}

	var needHTTPSList []string
	var platformList []string
	var playerTypeList []string
	if fastMode {
		needHTTPSList = shortNeedHTTPSList
		platformList = shortPlatformList
		playerTypeList = shortPlayerTypeList
	} else {
		needHTTPSList = comprehensiveNeedHTTPSList
		platformList = comprehensivePlatformList
		playerTypeList = comprehensivePlayerTypeList
	}

	for _, vodID := range vodList {
		for _, oauth_token := range oauthList {
			for _, need_https := range needHTTPSList {
				for _, platform := range platformList {
					for _, player_type := range playerTypeList {
						err := runSingleVODTest(vodID, oauth_token, need_https, platform, player_type)
						if err != nil {
							if endOnError {
								log.Info("Error encountered. Ending run.")
								return
							}
							failedRuns++
						}
						totalRuns++
					}
				}
			}
		}
	}
	log.Infof("Total number of runs: %v", totalRuns)
	log.Infof("Total number of failed runs: %v", failedRuns)
	log.Infof("Total number of successful runs: %v", totalRuns-failedRuns)
	log.Info("=== Finished VOD Tests ===")

}

func runAllVideoTests(fastMode bool, endOnError bool) {
	log.Info("=== Running Video Tests ===")
	totalRuns := 0
	failedRuns := 0
	channelList := []string{
		"shroud",                 // happy case
		"ninja",                  // happy case
		"esl_fifa_oja",           // private video
		"geekandsundry",          // archives restricted
		"twitchmedia_gblocktest", // US geoblock blocklist
		"squigglepoof",           // affiliate
		"naturalprimex",          // non-partner
	}

	oauthList := []string{
		"t4hip66breinu56akaf186xzrx3kph", // ryanresivod
		"jlimuqqzatg90n4vhqxl7ui0vr997w", // ryanresieditor
		"hjdhickhbv7jvnqp7yu59ke9tseshj", // ryanresiturbo2
		"zyzqe7ulzv5ee3wehspltq09amvdwk", // ryanresisubg
		"pr8t9op66sq7i9bunmrmmy3kldwq3e", // ryanresisubsh
		"",                               // anonymous
	}

	shortAdblockList := []string{
		"true",
	}

	comprehensiveAdblockList := []string{
		"true",
		"false",
		"",
	}

	shortNeedHTTPSList := []string{
		"true",
	}

	comprehensiveNeedHTTPSList := []string{
		"true",
		"false",
		"",
	}

	shortPlatformList := []string{
		"web",
	}

	comprehensivePlatformList := []string{
		"web",
		"other_platform",
		"",
	}

	shortPlayerTypeList := []string{
		"site",
	}

	comprehensivePlayerTypeList := []string{
		"site",
		"other_player",
		"",
	}

	var adblockList []string
	var needHTTPSList []string
	var platformList []string
	var playerTypeList []string
	if fastMode {
		adblockList = shortAdblockList
		needHTTPSList = shortNeedHTTPSList
		platformList = shortPlatformList
		playerTypeList = shortPlayerTypeList
	} else {
		adblockList = comprehensiveAdblockList
		needHTTPSList = comprehensiveNeedHTTPSList
		platformList = comprehensivePlatformList
		playerTypeList = comprehensivePlayerTypeList
	}

	for _, channelLogin := range channelList {
		for _, oauth_token := range oauthList {
			for _, adblock := range adblockList {
				for _, need_https := range needHTTPSList {
					for _, platform := range platformList {
						for _, player_type := range playerTypeList {
							err := runSingleVideoTest(channelLogin, oauth_token, adblock, need_https, platform, player_type)
							if err != nil {
								if endOnError {
									log.Info("Error encountered. Ending run.")
									return
								}
								failedRuns++
							}
							totalRuns++
						}
					}
				}
			}
		}
	}
	log.Infof("Total number of runs: %v", totalRuns)
	log.Infof("Total number of failed runs: %v", failedRuns)
	log.Infof("Total number of successful runs: %v", totalRuns-failedRuns)
	log.Info("=== Finished Video Tests ===")

}

func runSingleVODTest(vodID string, oauth_token string, need_https string, platform string, player_type string) error {
	log.Info("----- Starting new test -----")
	log.WithFields(log.Fields{
		"vodID":       vodID,
		"oauth_token": oauth_token,
		"need_https":  need_https,
		"platform":    platform,
		"player_type": player_type,
	}).Info("Inputs for this test")
	queryParams := buildRawQueryParams("", need_https, oauth_token, platform, player_type)

	railsStatus, railsToken, err := getVODToken(railsEndpoint, vodID, queryParams)
	if err != nil {
		log.Errorf("[FAILED] Failed to get rails token: %+v", err)
		return err
	}

	visageStatus, visageToken, err := getVODToken(visageEndpoint, vodID, queryParams)
	if err != nil {
		log.Errorf("[FAILED] Failed to get visage token: %+v", err)
		return err
	}

	if railsStatus != visageStatus {
		log.Errorf("[FAILED] Different status codes. Rails: %v, Visage: %v", railsStatus, visageStatus)
		return errors.New("different status codes")
	}

	if railsToken != nil && visageToken != nil {
		log.Infof("Rails token: %+v", railsToken)
		log.Infof("Visage token: %+v", visageToken)

		err = diffVODTokens(railsToken, visageToken)
		if err != nil {
			log.Errorf("[FAILED] Tokens are different: %+v", err)
			return err
		}
		log.Info("[SUCCESS] Tokens are identical")
	}
	log.Info("------- Ending test -------")
	return nil
}

func runSingleVideoTest(channelLogin string, oauth_token string, adblock string, need_https string, platform string, player_type string) error {
	log.Info("----- Starting new test -----")
	log.WithFields(log.Fields{
		"channelLogin": channelLogin,
		"oauth_token":  oauth_token,
		"adblock":      adblock,
		"need_https":   need_https,
		"platform":     platform,
		"player_type":  player_type,
	}).Info("Inputs for this test")
	queryParams := buildRawQueryParams(adblock, need_https, oauth_token, platform, player_type)

	railsStatus, railsToken, err := getVideoToken(railsEndpoint, channelLogin, queryParams)
	if err != nil {
		log.Errorf("[FAILED] Failed to get rails token: %+v", err)
		return err
	}

	visageStatus, visageToken, err := getVideoToken(visageEndpoint, channelLogin, queryParams)
	if err != nil {
		log.Errorf("[FAILED] Failed to get visage token: %+v", err)
		return err
	}

	if railsStatus != visageStatus {
		log.Errorf("[FAILED] Different status codes. Rails: %v, Visage: %v", railsStatus, visageStatus)
		return errors.New("different status codes")
	}

	if railsToken != nil && visageToken != nil {
		log.Infof("Rails token: %+v", railsToken)
		log.Infof("Visage token: %+v", visageToken)

		err = diffVideoTokens(railsToken, visageToken)
		if err != nil {
			log.Errorf("[FAILED] Tokens are different: %+v", err)
			return err
		}
		log.Info("[SUCCESS] Tokens are identical")
	}
	log.Info("------- Ending test -------")
	return nil
}

func areBoolsEqual(rails bool, visage bool, title string) bool {
	if rails != visage {
		log.Errorf("%s is different. Rails: %v, Visage: %v", title, rails, visage)
		return false
	}
	return true
}

func areStringsEqual(rails string, visage string, title string) bool {
	if rails != visage {
		log.Errorf("%s is different. Rails: %v, Visage: %v", title, rails, visage)
		return false
	}
	return true
}

func arePInt64sEqual(rails *int64, visage *int64, title string) bool {
	if rails == nil && visage == nil {
		return true
	}
	if rails == nil || visage == nil {
		log.Errorf("%s is different. Rails: %v, Visage: %v", title, rails, visage)
		return false
	}
	if *rails != *visage {
		log.Errorf("%s is different. Rails: %v, Visage: %v", title, *rails, *visage)
		return false
	}
	return true
}

func arePStringsEqual(rails *string, visage *string, title string) bool {
	if rails == nil && visage == nil {
		return true
	}
	if rails == nil || visage == nil {
		log.Errorf("%s is different. Rails: %v, Visage: %v", title, rails, visage)
		return false
	}
	if *rails != *visage {
		log.Errorf("%s is different. Rails: %v, Visage: %v", title, *rails, *visage)
		return false
	}
	return true
}

func areStringListsEqual(rails []string, visage []string, title string) bool {
	if len(rails) != len(visage) {
		log.Errorf("%s is different. Rails: %v, Visage: %v", title, len(rails), len(visage))
		return false
	}
	for i := range rails {
		if rails[i] != visage[i] {
			log.Errorf("%s is different. Rails: %v, Visage: %v", title, rails[i], visage[i])
			return false
		}
	}
	return true
}

func diffVODTokens(railsToken *token.VODAccessToken, visageToken *token.VODAccessToken) error {

	differentList := []string{}
	if !areStringListsEqual(railsToken.ChanSub.RestrictedBitrates, visageToken.ChanSub.RestrictedBitrates, "ChanSub.RestrictedBitrates") {
		differentList = append(differentList, "ChanSub.RestrictedBitrates")
	}

	if !areBoolsEqual(railsToken.HTTPSRequired, visageToken.HTTPSRequired, "HTTPSRequired") {
		differentList = append(differentList, "HTTPSRequired")
	}

	if !areBoolsEqual(railsToken.Privileged, visageToken.Privileged, "Privileged") {
		differentList = append(differentList, "Privileged")
	}

	if !arePInt64sEqual(railsToken.UserID, visageToken.UserID, "UserID") {
		differentList = append(differentList, "UserID")
	}

	if !arePInt64sEqual(railsToken.VODID, visageToken.VODID, "VODID") {
		differentList = append(differentList, "VODID")
	}

	if len(differentList) > 0 {
		err := fmt.Errorf("These fields are different: %v", differentList)
		return err
	}
	log.Info("Tokens are identical!")
	return nil
}

func diffVideoTokens(railsToken *token.VideoAccessToken, visageToken *token.VideoAccessToken) error {

	differentList := []string{}
	if !areBoolsEqual(railsToken.Adblock, visageToken.Adblock, "Adblock") {
		differentList = append(differentList, "Adblock")
	}

	if !areStringsEqual(railsToken.Channel, visageToken.Channel, "Channel") {
		differentList = append(differentList, "Channel")
	}

	if !arePInt64sEqual(railsToken.ChannelID, visageToken.ChannelID, "ChannelID") {
		differentList = append(differentList, "ChannelID")
	}

	if !areStringListsEqual(railsToken.ChanSub.RestrictedBitrates, visageToken.ChanSub.RestrictedBitrates, "ChanSub.RestrictedBitrates") {
		differentList = append(differentList, "ChanSub.RestrictedBitrates")
	}

	if !areBoolsEqual(railsToken.ChannelIsGeoBlocked, visageToken.ChannelIsGeoBlocked, "ChannelIsGeoBlocked") {
		differentList = append(differentList, "ChannelIsGeoBlocked")
	}

	if !arePStringsEqual(railsToken.DeviceID, visageToken.DeviceID, "DeviceID") {
		differentList = append(differentList, "DeviceID")
	}

	if !areBoolsEqual(railsToken.HTTPSRequired, visageToken.HTTPSRequired, "HTTPSRequired") {
		differentList = append(differentList, "HTTPSRequired")
	}

	if !arePStringsEqual(railsToken.Platform, visageToken.Platform, "Platform") {
		differentList = append(differentList, "Platform")
	}

	if !arePStringsEqual(railsToken.PlayerType, visageToken.PlayerType, "PlayerType") {
		differentList = append(differentList, "PlayerType")
	}

	if !areBoolsEqual(railsToken.Private.AllowedToView, visageToken.Private.AllowedToView, "Private.AllowedToView") {
		differentList = append(differentList, "Private.AllowedToView")
	}

	if !areBoolsEqual(railsToken.Privileged, visageToken.Privileged, "Privileged") {
		differentList = append(differentList, "Privileged")
	}

	if !areBoolsEqual(railsToken.ShowAds, visageToken.ShowAds, "ShowAds") {
		differentList = append(differentList, "ShowAds")
	}

	if !areBoolsEqual(railsToken.Subscriber, visageToken.Subscriber, "Subscriber") {
		differentList = append(differentList, "Subscriber")
	}

	if !areBoolsEqual(railsToken.Turbo, visageToken.Turbo, "Turbo") {
		differentList = append(differentList, "Turbo")
	}

	if !arePInt64sEqual(railsToken.UserID, visageToken.UserID, "UserID") {
		differentList = append(differentList, "UserID")
	}

	if len(differentList) > 0 {
		err := fmt.Errorf("These fields are different: %v", differentList)
		return err
	}
	log.Info("Tokens are identical!")
	return nil
}

func getVODToken(host string, vodID string, queryParams string) (int, *token.VODAccessToken, error) {
	pathPattern := "api/vods/%s/access_token"
	request, err := buildRequest(host, pathPattern, vodID, queryParams)
	if err != nil {
		return -1, nil, err
	}
	log.Infof("URL built for host: %s, URL: %s", host, request.URL.String())
	statusCode, rawToken := fetchToken(request)
	if statusCode != 200 {
		return statusCode, nil, err
	}

	response, err := parseVODResponse(rawToken)
	if err != nil {
		return statusCode, nil, err
	}

	token, err := parseVODToken(response)
	if err != nil {
		return statusCode, nil, err
	}

	return statusCode, token, nil
}

func getVideoToken(host string, channelLogin string, queryParams string) (int, *token.VideoAccessToken, error) {
	pathPattern := "api/channels/%s/access_token"
	request, err := buildRequest(host, pathPattern, channelLogin, queryParams)
	if err != nil {
		return -1, nil, err
	}
	log.Infof("URL built for host: %s, URL: %s", host, request.URL.String())
	statusCode, rawToken := fetchToken(request)
	if statusCode != 200 {
		return statusCode, nil, err
	}

	response, err := parseVideoResponse(rawToken)
	if err != nil {
		return statusCode, nil, err
	}

	token, err := parseVideoToken(response)
	if err != nil {
		return statusCode, nil, err
	}
	return statusCode, token, nil
}

func buildRawQueryParams(adblock string, need_https string, oauth_token string, platform string, player_type string) string {
	queryParams := url.Values{}
	if adblock != "" {
		queryParams.Set("adblock", adblock)
	}
	if need_https != "" {
		queryParams.Set("need_https", need_https)
	}
	queryParams.Set("oauth_token", oauth_token)

	if platform != "" {
		queryParams.Set("platform", platform)
	}
	if player_type != "" {
		queryParams.Set("player_type", player_type)
	}
	return queryParams.Encode()
}

func buildRequest(host string, pathPattern string, channelLogin string, rawQueryParams string) (*http.Request, error) {

	path := fmt.Sprintf(pathPattern, channelLogin)
	url := url.URL{
		Scheme: "https",
		Host:   host,
		Path:   path,
	}

	url.RawQuery = rawQueryParams
	req, err := http.NewRequest("GET", url.String(), nil)
	req.Header = map[string][]string{
		"Client-ID": {"ltoxwr5a0sxj4ug7zvgjnl28gtpasym"},
	}

	cookie := &http.Cookie{
		Name:  "unique_id",
		Value: "MYDEVICEID",
	}
	req.AddCookie(cookie)
	if err != nil {
		return nil, err
	}
	return req, nil
}

func fetchToken(req *http.Request) (int, []byte) {
	log.Info("Fetching Token")

	httpClient := http.Client{}

	resp, err := httpClient.Do(req)
	if err != nil {
		log.Errorf("Error response from http client do: %+v", err)
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Errorf("Error reading response body: %+v", err)
	}

	log.Infof("Resp status: %v", resp.StatusCode)
	return resp.StatusCode, bodyBytes
}

func parseVODResponse(rawResponse []byte) (*borpc.GetVODAccessTokenResponse, error) {
	response := &borpc.GetVODAccessTokenResponse{}
	err := json.Unmarshal(rawResponse, response)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func parseVideoResponse(rawResponse []byte) (*borpc.GetVideoAccessTokenResponse, error) {
	response := &borpc.GetVideoAccessTokenResponse{}
	err := json.Unmarshal(rawResponse, response)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func parseVODToken(resp *borpc.GetVODAccessTokenResponse) (*token.VODAccessToken, error) {
	token := &token.VODAccessToken{}
	rawToken := []byte(resp.Token)
	err := json.Unmarshal(rawToken, token)
	if err != nil {
		return nil, err
	}
	return token, nil
}

func parseVideoToken(resp *borpc.GetVideoAccessTokenResponse) (*token.VideoAccessToken, error) {
	token := &token.VideoAccessToken{}
	rawToken := []byte(resp.Token)
	err := json.Unmarshal(rawToken, token)
	if err != nil {
		return nil, err
	}
	return token, nil
}
