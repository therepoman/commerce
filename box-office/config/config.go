package config

import (
	"time"

	"code.justin.tv/commerce/box-office/config/dynamic"
	"code.justin.tv/commerce/box-office/internal/geoblock"
)

// Configuration for the service. Endpoints, timeouts, cache config, etc.
type Configuration struct {
	// AWS region that this service is running in
	AWSRegion string `validate:"nonzero"`

	// Channels client config
	ChannelsCacheTTLMinutes int `validate:"nonzero"`

	// Cloudwatch
	CloudwatchBufferSize int           `validate:"nonzero"`
	CloudwatchBatchSize  int           `validate:"nonzero"`
	CloudwatchRegion     string        `validate:"nonzero"`
	TwitchTelemetryFlush time.Duration `validate:"nonzero"`

	// Cache enable/disable config
	EnableFallbackCache bool
	EnablePrimaryCache  bool

	// Fallback cache config
	FallbackCacheTTLMinutes int `validate:"nonzero"`
	FallbackCacheTimeout    int `validate:"nonzero"`

	// Old, config-file-based Geoblocking
	GeoblockedChannels map[string]geoblock.Geoblock `validate:"nonzero"`
	// Resource names for S3, SSM
	DynamicGeoblockingDarklaunchKey string `validate:"nonzero"`
	DynamicGeoblockingBucket        string `validate:"nonzero"`
	DynamicGeoblockingFilename      string `validate:"nonzero"`
	// Dynamic configs for dynamic geoblocking
	DynamicGeoblocking           dynamic.DynamicGeoblocking
	DynamicGeoblockingDarklaunch dynamic.DarklaunchGetter

	// Enable/Disable previews for premium content
	EnablePremiumContentPreview bool

	// Anonymous IP Allowlist for IP ranges that we consider ok despite maxmind reporting them as anonymous (WPA2)
	AnonymousIPAllowlist []string `validate:"nonzero"`

	// Hallpass client config
	HallpassServiceCacheTTLMinutes int    `validate:"nonzero"`
	HallpassServiceClientTimeout   int    `validate:"nonzero"`
	HallpassServiceEndpoint        string `validate:"nonzero"`

	// Nitro service client config
	NitroServiceCacheTTLMinutes    int    `validate:"nonzero"`
	NitroServiceClientTimeout      int    `validate:"nonzero"`
	NitroServiceEndpoint           string `validate:"nonzero"`
	NitroServiceMaxIdleConnections int    `validate:"nonzero"`

	// Redis config
	RedisHost                        string  `validate:"nonzero"`
	RedisNodeConnectionPoolSize      int     `validate:"nonzero"`
	RedisPoolTimeoutMilliseconds     int     `validate:"nonzero"` // Currently ignored, using rediczar default as recommended
	RedisStatSampleRate              float32 `validate:"nonzero"` // Currently ignored, using rediczar default as recommended
	RedisMonitorIntervalMilliseconds int     `validate:"nonzero"`
	RedisDialTimeoutMilliseconds     int     `validate:"nonzero"` // Currently ignored, using rediczar default as recommended
	RedisReadTimeoutMilliseconds     int     `validate:"nonzero"` // Currently ignored, using rediczar default as recommended
	RedisWriteTimeoutMilliseconds    int     `validate:"nonzero"` // Currently ignored, using rediczar default as recommended
	RedisMaxRedirects                int     `validate:"nonzero"` // Currently ignored, using rediczar default as recommended

	// Sandstorm config
	SandstormEnvironment          string `validate:"nonzero"`
	SandstormKeyRollbarToken      string `validate:"nonzero"`
	SandstormKeyTokenSignatureKey string `validate:"nonzero"`
	SandstormRoleARN              string `validate:"nonzero"`

	// Statsd config
	StatsURL    string `validate:"nonzero"`
	StatsPrefix string `validate:"nonzero"`

	// Subscriptions client config
	SubscriptionsCacheTTLMinutes    int    `validate:"nonzero"`
	SubscriptionsClientTimeout      int    `validate:"nonzero"`
	SubscriptionsEndpoint           string `validate:"nonzero"`
	SubscriptionsMaxIdleConnections int    `validate:"nonzero"`

	// Voyager client config
	VoyagerCacheTTLMinutes    int    `validate:"nonzero"`
	VoyagerClientTimeout      int    `validate:"nonzero"`
	VoyagerEndpoint           string `validate:"nonzero"`
	VoyagerMaxIdleConnections int    `validate:"nonzero"`

	// Users Service client config
	UsersServiceCacheTTLMinutes int    `validate:"nonzero"`
	UsersServiceClientTimeout   int    `validate:"nonzero"`
	UsersServiceEndpoint        string `validate:"nonzero"`

	// VodAPI cleint config
	VodAPICacheTTLMinutes int    `validate:"nonzero"`
	VodAPIClientTimeout   int    `validate:"nonzero"`
	VodAPIEndpoint        string `validate:"nonzero"`

	// Dads service client config
	DadsCacheTTLMinutes    int    `validate:"nonzero"`
	DadsClientTimeout      int    `validate:"nonzero"`
	DadsEndpoint           string `validate:"nonzero"`
	DadsMaxIdleConnections int    `validate:"nonzero"`

	// Nioh client config
	NiohCacheTTLMilliseconds int    `validate:"nonzero"`
	NiohClientTimeout        int    `validate:"nonzero"`
	NiohEndpoint             string `validate:"nonzero"`
	NiohMaxIdleConnections   int    `validate:"nonzero"`

	// Tailor client config
	TailorCacheTTLMinutes    int    `validate:"nonzero"`
	TailorClientTimeout      int    `validate:"nonzero"`
	TailorEndpoint           string `validate:"nonzero"`
	TailorMaxIdleConnections int    `validate:"nonzero"`

	// Clips client config
	ClipsCacheTTLMinutes    int    `validate:"nonzero"`
	ClipsClientTimeout      int    `validate:"nonzero"`
	ClipsEndpoint           string `validate:"nonzero"`
	ClipsMaxIdleConnections int    `validate:"nonzero"`
}
