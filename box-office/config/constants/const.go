package constants

const (
	ProdEnvironment          = "prod"
	ProdCanaryEnvironment    = "prod-canary"
	StagingEnvironement      = "staging"
	StagingCanaryEnvironment = "staging-canary"
	Development              = "dev"
	OrganizationName         = "commerce"
	ServiceName              = "box-office"
)
