package dynamic

import (
	"strings"
	"time"

	"code.justin.tv/commerce/box-office/internal/geoblock"
	"code.justin.tv/commerce/dynamicconfig"
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

type DynamicGeoblocking interface {
	GetGeoblockedChannels() map[string]geoblock.Geoblock
	RefreshMapping(value interface{})
}

type dynamicGeoblocking struct {
	dynamicConfig *dynamicconfig.DynamicConfig
	latestMapping map[string]geoblock.Geoblock
}

func NewDynamicGeoblocking(dynamicConfig *dynamicconfig.DynamicConfig) DynamicGeoblocking {
	dynamicGeoblocking := dynamicGeoblocking{
		dynamicConfig: dynamicConfig,
		latestMapping: make(map[string]geoblock.Geoblock),
	}
	// First time fetching
	dynamicGeoblocking.RefreshMapping(dynamicConfig.Get("GeoblockedChannels"))
	// OnChange hook
	dynamicGeoblocking.dynamicConfig.OnChange("GeoblockedChannels", func(value interface{}) {
		dynamicGeoblocking.RefreshMapping(value)
	})

	return &dynamicGeoblocking
}

func (d *dynamicGeoblocking) GetGeoblockedChannels() map[string]geoblock.Geoblock {
	return d.latestMapping
}

// Attempts to make a map[string]Geoblock from the provided value, and overwrites the latest mapping if successful.
func (d *dynamicGeoblocking) RefreshMapping(value interface{}) {
	geoblocks := map[string]geoblock.Geoblock{}

	geoblockEntries, ok := value.(map[string]interface{})
	if !ok {
		log.Errorf("DynamicConfig's GeoblockedChannels value could not be converted into a generic map[string]interface{}, possibly malformed")
		return
	}
	for channelName, entry := range geoblockEntries {
		// Ensure that channelName is lowercase
		channelName = strings.ToLower(channelName)
		// Cast the value (type interface{}) into an intermediate generic value for a JSON object: map[string]interface{}
		intermediateConversion, ok := entry.(map[string]interface{})
		if !ok {
			log.Errorf("Entry for key %s appears to be malformed, skipping.", channelName)
			continue
		}
		gb, err := d.convertToGeoblock(intermediateConversion)
		if err != nil {
			log.Errorf("Could not convert entry for key %s to geoblock %+v\n", channelName, err)
			continue
		}
		geoblocks[channelName] = *gb
	}
	log.Infof("Refreshing geoblocks: %+v", geoblocks)
	d.latestMapping = geoblocks
}

// Converts a single intermediate JSON object into a Geoblock
func (d *dynamicGeoblocking) convertToGeoblock(entry map[string]interface{}) (*geoblock.Geoblock, error) {
	gb := &geoblock.Geoblock{}

	decoderConfig := &mapstructure.DecoderConfig{
		DecodeHook: mapstructure.StringToTimeHookFunc(time.RFC3339),
		Result:     gb,
	}

	decoder, err := mapstructure.NewDecoder(decoderConfig)
	if err != nil || decoder == nil {
		return nil, errors.Wrap(err, "Decoder was not constructed")
	}
	err = decoder.Decode(entry)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to decode entry")
	}

	return gb, nil
}
