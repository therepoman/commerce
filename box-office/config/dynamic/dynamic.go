package dynamic

import (
	"context"
	"time"

	"code.justin.tv/commerce/dynamicconfig"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ssm"
	"github.com/pkg/errors"
)

func GetDarklaunchDynamicConfig(region string, darklaunchKey string) (*dynamicconfig.DynamicConfig, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	sess, err := session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			Region: aws.String(region),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "Failed to initialize SSM session")
	}
	ssmClient := ssm.New(sess)
	ssmSource := dynamicconfig.NewSSMSource([]string{darklaunchKey}, ssmClient)

	config, err := dynamicconfig.NewDynamicConfig(ctx, ssmSource, nil)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to create dynamic config from source")
	}
	return config, nil
}

func GetGeoblockingDynamicConfig(s3Bucket string, geoblockingFilename string) (*dynamicconfig.DynamicConfig, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	source, err := dynamicconfig.NewS3JSONSource("us-west-2", "s3-us-west-2.amazonaws.com", s3Bucket, geoblockingFilename)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to initialize S3 JSON source")
	}

	dynamicConfig, err := dynamicconfig.NewDynamicConfig(ctx, source, nil)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to create dynamic config from source")
	}

	return dynamicConfig, nil
}
