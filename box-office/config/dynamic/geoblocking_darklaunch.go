package dynamic

import (
	"strconv"

	"code.justin.tv/commerce/dynamicconfig"
)

type DarklaunchGetter interface {
	UseDynamicGeoblocking() bool
}

type darklaunchConfig struct {
	darklaunchKey string
	dynamicConfig *dynamicconfig.DynamicConfig
}

func (d *darklaunchConfig) UseDynamicGeoblocking() bool {
	if darklaunchKey, ok := d.dynamicConfig.Get(d.darklaunchKey).(string); ok {
		enabled, err := strconv.ParseBool(darklaunchKey)
		if err != nil {
			return false
		}
		return enabled
	}
	return false
}

func NewDarklaunchGetter(dynamicConfig *dynamicconfig.DynamicConfig, darklaunchKey string) DarklaunchGetter {
	return &darklaunchConfig{
		darklaunchKey: darklaunchKey,
		dynamicConfig: dynamicConfig,
	}
}
