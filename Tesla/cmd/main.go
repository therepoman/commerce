package main

import (
	"log"

	"math/rand"

	"code.justin.tv/commerce/Tesla/api"

	"code.justin.tv/common/config"

	"code.justin.tv/foundation/twitchserver"
)

//Generator is a random number generator.
type Generator struct {
	generator rand.Rand
}

func (g Generator) flipCoin(prob float64) bool {
	if g.generator.Float64() < prob {
		return true
	}

	return false
}

func (g Generator) randomize(value float64) float64 {
	return g.generator.Float64() * value
}

func (g Generator) getNormallyDistributedValue(base float64, variation float64) float64 {
	factor := 1.0
	if g.generator.Float64() < 0.5 {
		factor = -1.0
	}

	return base + factor*g.generator.Float64()*variation
}

func main() {
	// To add app-specific config flags, you can call config.Register()
	// https://godoc.internal.justin.tv/code.justin.tv/common/config
	err := config.Parse()
	if err != nil {
		log.Fatal(err)
	}

	server, err := api.NewServer()
	if err != nil {
		log.Fatal(err)
	}

	var twitchServiceConfig *twitchserver.ServerConfig
	if server.GetConfiguration().IsLocal() {
		twitchServiceConfig = twitchserver.NewConfig()
		twitchServiceConfig.Addr = ":8000" // Change this is you want to run on a different port.
	}

	log.Fatal(twitchserver.ListenAndServe(server, twitchServiceConfig))
}
