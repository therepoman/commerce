package api

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"code.justin.tv/commerce/Tesla/cache"
	"code.justin.tv/commerce/Tesla/config"

	log "github.com/Sirupsen/logrus"
)

type testLoggingHook struct {
	expectedMessage    string
	expectedFieldValue string
	expectedFieldKey   string
	testSuite          *testing.T
}

func (h testLoggingHook) Levels() []log.Level {
	return []log.Level{log.ErrorLevel}
}

func (h testLoggingHook) Fire(entry *log.Entry) error {
	if strings.Compare(h.expectedFieldValue, getStringFromMapOrEmpty(entry.Data, h.expectedFieldKey)) != 0 {
		h.testSuite.Error("Logging hook received unexpected field for key ", h.expectedFieldKey, ".\nExpected : ", h.expectedFieldValue, "\nGot : ", getStringFromMapOrEmpty(entry.Data, "rID"))
	}

	if strings.Compare(h.expectedMessage, entry.Message) != 0 {
		h.testSuite.Error("Logging hook received unexpected message.\nExpected : ", h.expectedMessage, "\nGot : ", entry.Message)
	}

	return nil
}

func testCacheRatios(t *testing.T, prefix string, stats *cache.ClientDetailsCacheStats, expectedLRU float64, expectedRedis float64) {
	lruRatio, redisRatio := computeCacheRatios(stats)

	if lruRatio != expectedLRU {
		t.Error(prefix, "Unexpected LRU Ratio : Expected : ", expectedLRU, " Got : ", lruRatio)
	}

	if redisRatio != expectedRedis {
		t.Error(prefix, "Unexpected Redis Ratio : Expected : ", expectedRedis, " Got : ", redisRatio)
	}
}

func TestComputeCacheRatios(t *testing.T) {
	// Default to zeros.
	stats := cache.ClientDetailsCacheStats{}

	testCacheRatios(t, "[ZeroTest]", &stats, 1.0, 1.0)

	stats = cache.ClientDetailsCacheStats{
		Hits:   1,
		Misses: 1,
	}

	testCacheRatios(t, "[OnlyRedisTest]", &stats, 1.0, 0.5)

	stats = cache.ClientDetailsCacheStats{
		LocalHits:   1,
		LocalMisses: 1,
	}

	testCacheRatios(t, "[OnlyLRUTest]", &stats, 0.5, 1.0)

	stats = cache.ClientDetailsCacheStats{
		Hits:        1,
		Misses:      1,
		LocalHits:   1,
		LocalMisses: 1,
	}

	testCacheRatios(t, "[CombinedTest]", &stats, 0.5, 0.5)
}

func TestHandleAPIError(t *testing.T) {
	recorder := httptest.NewRecorder()
	configuration := config.GetTestConfiguration()
	clientID := "testClientId"
	logString := "myLogString"
	errorString := "myErrorString"
	requestID := "test-request-id"
	requestIDKey := "rID"
	status := http.StatusForbidden
	apiName := config.SetStatusForReceiptsAPI

	// http.Error appends a newline, so the expected string will be the error string appended with a newline.
	expectedErrorString := strings.Join([]string{errorString, "\n"}, "")

	logger := log.WithFields(log.Fields{
		requestIDKey: requestID,
	})

	hook := testLoggingHook{
		expectedMessage:    logString,
		expectedFieldValue: requestID,
		expectedFieldKey:   requestIDKey,
		testSuite:          t,
	}
	logger.Logger.Hooks.Add(hook)

	handleAPIError(recorder, logger, configuration, apiName, clientID, logString, errorString, status)

	if recorder.Code != status {
		t.Error("HandleAPIError responded with an unexpected status. Expected : ", status, " Got : ", recorder.Code)
	}

	if bytes.Compare([]byte(expectedErrorString), recorder.Body.Bytes()) != 0 {
		t.Error("HandleAPIError responded with an unexpected body.\nExpected : ", expectedErrorString, "\nGot : ", recorder.Body.String())
	}

	// Now we test the safe error handler
	// Reset the recorder and logger.
	recorder = httptest.NewRecorder()
	expectedErrorString = strings.Join([]string{errorString, "\n"}, "")

	logger = log.WithFields(log.Fields{
		requestIDKey: requestID,
	})

	hook = testLoggingHook{
		expectedMessage:    errorString,
		expectedFieldValue: requestID,
		expectedFieldKey:   requestIDKey,
		testSuite:          t,
	}
	logger.Logger.Hooks = make(log.LevelHooks)
	logger.Logger.Hooks.Add(hook)

	handleSafeAPIError(recorder, logger, configuration, apiName, clientID, errorString, status)

	if bytes.Compare([]byte(expectedErrorString), recorder.Body.Bytes()) != 0 {
		t.Error("HandleSafeAPIError responded with an unexpected body.\nExpected : ", expectedErrorString, "\nGot : ", recorder.Body.String())
	}
}

func getStringFromMapOrEmpty(m map[string]interface{}, key string) string {
	v, exists := m[key]

	// Key does not exist in map, return empty string.
	if !exists {
		return ""
	}

	switch v.(type) {
	case string:
		return v.(string)
	default:
		// Value is not a string, return empty string.
		return ""
	}
}
