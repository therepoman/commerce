package api

import (
	tesla "code.justin.tv/commerce/Tesla/client"
	"github.com/stretchr/testify/assert"
	"testing"
)

func getValidReceiptUpdate(receiptId string) tesla.ReceiptUpdateResult {
	receiptUpdate := tesla.ReceiptUpdateResult{
		ReceiptID:       receiptId,
		Result:          "testResult",
	}

	return receiptUpdate
}

func TestCompareResultingReceiptUpdatesValid(t *testing.T) {
	var sesReceipts []tesla.ReceiptUpdateResult
	var adgReceipts []tesla.ReceiptUpdateResult

	sesReceipts = append(sesReceipts, getValidReceiptUpdate("valid"))
	adgReceipts = append(adgReceipts, getValidReceiptUpdate("valid"))

	result := compareResultingReceipts("requestId", sesReceipts, adgReceipts)

	assert.Equal(t, true, result.Matches)
	assert.Equal(t, "requestId", result.RequestId)
}

func TestCompareResultingReceiptUpdatesInvalidDifferentLength(t *testing.T) {
	var sesReceipts []tesla.ReceiptUpdateResult
	var adgReceipts []tesla.ReceiptUpdateResult

	sesReceipts = append(sesReceipts, getValidReceiptUpdate("valid"))
	adgReceipts = append(adgReceipts, getValidReceiptUpdate("valid"))
	adgReceipts = append(adgReceipts, getValidReceiptUpdate("valid"))

	result := compareResultingReceipts("requestId", sesReceipts, adgReceipts)

	assert.Equal(t, false, result.Matches)
	assert.Equal(t, "requestId", result.RequestId)
}

func TestCompareResultingReceiptUpdatesValidDifferentOrder(t *testing.T) {
	var sesReceipts []tesla.ReceiptUpdateResult
	var adgReceipts []tesla.ReceiptUpdateResult

	sesReceipts = append(sesReceipts, getValidReceiptUpdate("valid2"))
	sesReceipts = append(sesReceipts, getValidReceiptUpdate("valid"))
	adgReceipts = append(adgReceipts, getValidReceiptUpdate("valid"))
	adgReceipts = append(adgReceipts, getValidReceiptUpdate("valid2"))

	result := compareResultingReceipts("requestId", sesReceipts, adgReceipts)

	assert.Equal(t, true, result.Matches)
	assert.Equal(t, "requestId", result.RequestId)
}

func TestCompareResultingReceiptUpdatesInvalidSameLength(t *testing.T) {
	var sesReceipts []tesla.ReceiptUpdateResult
	var adgReceipts []tesla.ReceiptUpdateResult

	sesReceipts = append(sesReceipts, getValidReceiptUpdate("valid1"))
	sesReceipts = append(sesReceipts, getValidReceiptUpdate("valid2"))
	adgReceipts = append(adgReceipts, getValidReceiptUpdate("valid3"))
	adgReceipts = append(adgReceipts, getValidReceiptUpdate("valid4"))

	result := compareResultingReceipts("requestId", sesReceipts, adgReceipts)

	assert.Equal(t, false, result.Matches)
	assert.Equal(t, "requestId", result.RequestId)
}

func TestCompareResultingReceiptUpdatesInvalidSameLengthOneDifferent(t *testing.T) {
	var sesReceipts []tesla.ReceiptUpdateResult
	var adgReceipts []tesla.ReceiptUpdateResult

	sesReceipts = append(sesReceipts, getValidReceiptUpdate("valid1"))
	sesReceipts = append(sesReceipts, getValidReceiptUpdate("valid2"))
	adgReceipts = append(adgReceipts, getValidReceiptUpdate("valid1"))
	adgReceipts = append(adgReceipts, getValidReceiptUpdate("valid3"))

	result := compareResultingReceipts("requestId", sesReceipts, adgReceipts)

	assert.Equal(t, false, result.Matches)
	assert.Equal(t, "requestId", result.RequestId)
}
