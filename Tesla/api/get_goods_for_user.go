package api

import (
	"code.justin.tv/commerce/Tesla/application"
	"code.justin.tv/commerce/Tesla/externaladg"
	"code.justin.tv/commerce/Tesla/externalses"
	"encoding/json"
	"net/http"
	"strings"
	"sync"
	"time"

	"code.justin.tv/commerce/Tesla/cache"
	tesla "code.justin.tv/commerce/Tesla/client"
	"code.justin.tv/commerce/Tesla/config"
	"code.justin.tv/commerce/Tesla/external"
	"code.justin.tv/commerce/Tesla/externalaws"

	log "github.com/Sirupsen/logrus"
	uuid "github.com/satori/go.uuid"
)

const GetGoodsForUser = "GET_GOODS_FOR_USER"

type compareGoodsResultLogEntry struct {
	RequestId string
	Matches   bool
	SesGoods  []tesla.Good
	AdgGoods  []tesla.Good
	TimeStamp int64
	ApiCall	  string
}

func (s *Server) getGoodsForUserSigV4(w http.ResponseWriter, r *http.Request) {
	requestID := uuid.NewV4().String()
	startTime := time.Now()
	tuid := r.URL.Query()["tuid"][0]
	clientID := r.Header.Get("Twitch-Client-ID")
	logger := log.WithFields(log.Fields{
		"rID":      requestID,
		"clientID": clientID,
	})

	apiContext := external.APIContext{Configuration: s.serverConfiguration, MethodName: string(config.GetGoodsForUserAPI), ClientID: clientID}
	if clientID == "" {
		handleGetGoodsSafeAPIError(w, logger, s.serverConfiguration, config.UnknownClient, "Bad request: Missing twitch client id parameter", http.StatusBadRequest)
		return
	}

	// Check to see if the client is blacklisted before continuing
	if s.serverConfiguration.IsClientBlacklistedForAPI(clientID, config.GetGoodsForUserAPI) {
		postCountMetrics(s.serverConfiguration, config.GetGoodsForUserAPI, clientID, 1, config.BlacklistedClientMetric)
		errorString := "Forbidden client calling " + string(config.GetGoodsForUserAPI)
		logString := errorString + " : " + clientID
		// We pass clientID instead of clientMetric name specifically for this metric.
		handleGetGoodsAPIError(w, logger, s.serverConfiguration, clientID, logString, errorString, http.StatusForbidden)
		return
	}

	// Check throttling.
	if isAPIThrottledForClient(w, logger, s.serverConfiguration, config.GetGoodsForUserAPI, clientID) {
		// API is throttled. Errors and everything should already have been populated. Simply return.
		return
	}

	clientDetails, clientDetailsErr := cache.ClientDetailsCache().GetClientDetails(apiContext)

	if clientDetailsErr != nil {
		logString := "An error occurred while fetching client details : " + clientDetailsErr.Error()
		handleGetGoodsAPIError(w, logger, s.serverConfiguration, config.UnknownClient, logString, clientDetailsErr.Error(), http.StatusInternalServerError)
		return
	}

	clientMetricName := clientDetails.ClientMetricName

	if tuid == "" {
		handleGetGoodsSafeAPIError(w, logger, s.serverConfiguration, clientMetricName, "Bad request: Missing twitch user id parameter", http.StatusBadRequest)
		return
	}

	logger.Info("Received valid call: GetGoodsForUser - " + tuid)
	goodsForUserResponse, err := s.processGetGoodsForUserSigV4Request(requestID, tuid, logger, apiContext, clientMetricName)

	if err != nil {
		logString := "An error was returned during the ADGEnt call : " + err.Error()
		extErr, isExtErr := err.(external.Error)
		if isExtErr {
			handleGetGoodsAPIError(w, logger, s.serverConfiguration, clientMetricName, logString, extErr.Error(), extErr.GetErrorCode())
			return
		}

		handleGetGoodsAPIError(w, logger, s.serverConfiguration, clientMetricName, logString, err.Error(), http.StatusInternalServerError)
		return
	}

	logger.Info("Successfully returned from ADGEnt call, returning response.")
	data, jsonErr := json.Marshal(goodsForUserResponse)

	if jsonErr != nil {
		handleGetGoodsAPIError(w, logger, s.serverConfiguration, clientMetricName, "Error serializing ADGEnt response : "+jsonErr.Error(), jsonErr.Error(), http.StatusInternalServerError)
		return
	}

	_, err = w.Write(data)
	if err != nil {
		handleGetGoodsAPIError(w, logger, s.serverConfiguration, clientMetricName, "Error writing response : "+err.Error(), err.Error(), http.StatusInternalServerError)
	}

	handleGetGoodsAPISuccess(s.serverConfiguration, clientMetricName, time.Since(startTime))

	return
}

func (s *Server) processGetGoodsForUserSigV4Request(requestID string, tuid string, logger *log.Entry, apiContext external.APIContext, clientMetricName string) (*tesla.GetGoodsForUserResponse, error) {
	var wg sync.WaitGroup

	var sesGoods = make([]tesla.Good, 0)
	var sesErr = external.Error(nil)

	var adgGoods = make([]tesla.Good, 0)
	var adgErr = external.Error(nil)

	sesApplication := application.SESApplication{
		RequestID:                           requestID,
		Tuid:                                tuid,
		ApiContext:                          apiContext,
		ClientMetricName:                    clientMetricName,
		SesGetAccountEntitlementInterface:   externalses.NewSESGetAccountEntitlements(apiContext, clientMetricName),
		SesUpdateFulfillmentStatusInterface: externalses.NewSESUpdateFulfillmentStatus(apiContext, clientMetricName),
		ClientProductMappingInterface:       externalaws.NewDynamoClientProductMappingService(apiContext, clientMetricName),
	}

	adgApplication := application.ADGApplication{
		RequestID:        requestID,
		Tuid:             tuid,
		ApiContext:       apiContext,
		ClientMetricName: clientMetricName,
		ADGOService:      nil,
		ADGEService:      externaladg.NewADGEntitlementService(apiContext, clientMetricName),
		RevokeService:    externalaws.NewDynamoRevokeService(apiContext, clientMetricName),
	}


	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//  Thread SES/ADG
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	wg.Add(1)
	go func(sesGoods *[]tesla.Good, sesErr *external.Error) {
		defer wg.Done()

		// This defer function is to catch any panics from the SES call
		// We want this here since we do not want to impact the API call with the new SES call
		defer func() {
			if r := recover(); r != nil {
				var ok bool
				err, ok := r.(error)
				if !ok {
					log.Error("There was an issue parsing as an error.")
				}

				log.Error("there was an error in calling TPROX/SES: " + err.Error())
			}
		}()

		*sesGoods, *sesErr = processGetGoodsFromApplication(sesApplication)
	}(&sesGoods, &sesErr)

	wg.Add(1)
	go func(adgGoods *[]tesla.Good, adgErr *external.Error) {
		defer wg.Done()
		*adgGoods, *adgErr = processGetGoodsFromApplication(adgApplication)
	}(&adgGoods, &adgErr)

	wg.Wait()
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//  Thread SES/ADG
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	logResultString, _ := json.Marshal(compareResultingGoods(requestID, sesGoods, adgGoods))
	log.Info(config.SESADGComparisonLogTag + string(logResultString) + config.SESADGComparisonLogTag)

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//  SES
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO Verify the receipts match up.
	for _, good := range sesGoods {
		good.ReceiptID = "valid"
	}

	if sesErr != nil {
		log.Warnf("Error in getting entitlements from SES.")
		log.Warnf(sesErr.Error())
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//  ADG
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if adgErr != nil {
		return nil, adgErr
	}

	return &tesla.GetGoodsForUserResponse{Goods: adgGoods}, adgErr
}

func processGetGoodsFromApplication(callApplication application.EntitlementApplicationInterface) ([]tesla.Good, external.Error) {
	entitlementApplication := application.EntitlementApplication{
		Application: callApplication,
	}

	entitlements, getEntitlementErr := entitlementApplication.Application.GetAccountEntitlements()

	if getEntitlementErr != nil {
		// TODO update this so that it throws actual errors when we deprecate adg
		// We do not want to error out right now, we can just log this error and move on
		log.Warnf("There was an error in the GetAccountsEntitlements Call: " + getEntitlementErr.Error())
		//return nil, sesErr
	}
	return entitlements, getEntitlementErr
}

func extractAuthToken(authToken string) string {
	OAuth := strings.Fields(authToken)
	// either the Parameter will be of the form
	// <authtoken>
	// OAuth <authtoken>
	return OAuth[len(OAuth)-1]
}

func handleGetGoodsAPISuccess(configuration *config.TeslaConfiguration, clientMetricName string, duration time.Duration) {
	postDurationMetrics(configuration, config.GetGoodsForUserAPI, clientMetricName, duration)
	postStatusMetrics(configuration, config.GetGoodsForUserAPI, clientMetricName, http.StatusOK)
	postCacheMetrics(configuration)
}

func handleGetGoodsSafeAPIError(w http.ResponseWriter, logger *log.Entry, configuration *config.TeslaConfiguration, clientMetricName string, errorString string, errorStatus int) {
	handleSafeAPIError(w, logger, configuration, config.GetGoodsForUserAPI, clientMetricName, errorString, errorStatus)
}

func handleGetGoodsAPIError(w http.ResponseWriter, logger *log.Entry, configuration *config.TeslaConfiguration, clientMetricName string, logString string, errorString string, errorStatus int) {
	handleAPIError(w, logger, configuration, config.GetGoodsForUserAPI, clientMetricName, logString, errorString, errorStatus)
}

func compareResultingGoods(requestId string, sesGoods []tesla.Good, adgGoods []tesla.Good) compareGoodsResultLogEntry {
	logResult := compareGoodsResultLogEntry{
		RequestId: requestId,
		Matches:   false,
		SesGoods:  sesGoods,
		AdgGoods:  adgGoods,
		TimeStamp: time.Now().Unix(),
		ApiCall:   GetGoodsForUser,
	}

	if len(sesGoods) != len(adgGoods) {
		log.Warnf("The results from ADG and SES differ in length")
	} else {
		for _, sesGood := range sesGoods {
			contains := false

			for _, adgGood := range adgGoods {
				if adgGood == sesGood {
					contains = true
				}
			}

			if !contains {
				log.Warnf("The results from ADG and SES differ in resulting goods")
				logResult.Matches = false
				break
			}

			logResult.Matches = contains
		}
	}

	return logResult
}
