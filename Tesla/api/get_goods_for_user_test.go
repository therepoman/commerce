package api

import (
	tesla "code.justin.tv/commerce/Tesla/client"
	"github.com/stretchr/testify/assert"
	"testing"
)

func getValidGood(receiptId string) tesla.Good {
	good := tesla.Good{
		ReceiptID:       receiptId,
		SKU:             "sku",
		NextInstruction: "next",
	}

	return good
}

func TestCompareResultingGoodsValid(t *testing.T) {
	var sesGoods []tesla.Good
	var adgGoods []tesla.Good

	sesGoods = append(sesGoods, getValidGood("valid"))
	adgGoods = append(adgGoods, getValidGood("valid"))

	result := compareResultingGoods("requestId", sesGoods, adgGoods)

	assert.Equal(t, true, result.Matches)
	assert.Equal(t, adgGoods, result.AdgGoods)
	assert.Equal(t, sesGoods, result.SesGoods)
	assert.Equal(t, "requestId", result.RequestId)
}

func TestCompareResultingGoodsInvalidDifferentLength(t *testing.T) {
	var sesGoods []tesla.Good
	var adgGoods []tesla.Good

	sesGoods = append(sesGoods, getValidGood("valid"))
	adgGoods = append(adgGoods, getValidGood("valid"))
	adgGoods = append(adgGoods, getValidGood("valid"))

	result := compareResultingGoods("requestId", sesGoods, adgGoods)

	assert.Equal(t, false, result.Matches)
	assert.Equal(t, adgGoods, result.AdgGoods)
	assert.Equal(t, sesGoods, result.SesGoods)
	assert.Equal(t, "requestId", result.RequestId)
}

func TestCompareResultingGoodsValidDifferentOrder(t *testing.T) {
	var sesGoods []tesla.Good
	var adgGoods []tesla.Good

	sesGoods = append(sesGoods, getValidGood("valid2"))
	sesGoods = append(sesGoods, getValidGood("valid"))
	adgGoods = append(adgGoods, getValidGood("valid"))
	adgGoods = append(adgGoods, getValidGood("valid2"))

	result := compareResultingGoods("requestId", sesGoods, adgGoods)

	assert.Equal(t, true, result.Matches)
	assert.Equal(t, adgGoods, result.AdgGoods)
	assert.Equal(t, sesGoods, result.SesGoods)
	assert.Equal(t, "requestId", result.RequestId)
}

func TestCompareResultingGoodsInvalidSameLength(t *testing.T) {
	var sesGoods []tesla.Good
	var adgGoods []tesla.Good

	sesGoods = append(sesGoods, getValidGood("valid1"))
	sesGoods = append(sesGoods, getValidGood("valid2"))
	adgGoods = append(adgGoods, getValidGood("valid3"))
	adgGoods = append(adgGoods, getValidGood("valid4"))

	result := compareResultingGoods("requestId", sesGoods, adgGoods)

	assert.Equal(t, false, result.Matches)
	assert.Equal(t, adgGoods, result.AdgGoods)
	assert.Equal(t, sesGoods, result.SesGoods)
	assert.Equal(t, "requestId", result.RequestId)
}

func TestCompareResultingGoodsInvalidSameLengthOneDifferent(t *testing.T) {
	var sesGoods []tesla.Good
	var adgGoods []tesla.Good

	sesGoods = append(sesGoods, getValidGood("valid1"))
	sesGoods = append(sesGoods, getValidGood("valid2"))
	adgGoods = append(adgGoods, getValidGood("valid1"))
	adgGoods = append(adgGoods, getValidGood("valid3"))

	result := compareResultingGoods("requestId", sesGoods, adgGoods)

	assert.Equal(t, false, result.Matches)
	assert.Equal(t, adgGoods, result.AdgGoods)
	assert.Equal(t, sesGoods, result.SesGoods)
	assert.Equal(t, "requestId", result.RequestId)
}
