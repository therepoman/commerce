package api

import (
	"net/http"
	"strconv"
	"time"

	"code.justin.tv/commerce/Tesla/cache"
	"code.justin.tv/commerce/Tesla/config"
	"code.justin.tv/commerce/Tesla/metrics"

	log "github.com/Sirupsen/logrus"
)

const (
	authHeader     = "Authorization"
	clientIDHeader = "Twitch-Client-Row-ID"
)

func handleAPIError(w http.ResponseWriter, logger *log.Entry, configuration *config.TeslaConfiguration, apiName config.APIName, clientID string, logString string, errorString string, errorStatus int) {
	logger.Error(logString)
	postStatusMetrics(configuration, apiName, clientID, errorStatus)
	postCacheMetrics(configuration)
	http.Error(w, errorString, errorStatus)
}

func handleSafeAPIError(w http.ResponseWriter, logger *log.Entry, configuration *config.TeslaConfiguration, apiName config.APIName, clientID string, errorString string, errorStatus int) {
	handleAPIError(w, logger, configuration, apiName, clientID, errorString, errorString, errorStatus)
}

//handleThrottlingError handles a throttling error, populating the appropriate headers and posting the appropriate metrics.
func handleThrottlingError(w http.ResponseWriter, logger *log.Entry, configuration *config.TeslaConfiguration, apiName config.APIName, clientID string, allowedRate int64, currentRate int64, remainingTimeInPeriod time.Duration) {
	h := w.Header()
	h.Set("X-RateLimit-Limit", strconv.FormatInt(allowedRate, 10))
	h.Set("X-RateLimit-CurrentRate", strconv.FormatInt(currentRate, 10))
	remainingTimeInPeriodSec := int64(remainingTimeInPeriod / time.Second)
	h.Set("X-RateLimit-Delay", strconv.FormatInt(remainingTimeInPeriodSec, 10))
	postCountMetrics(configuration, apiName, clientID, 1, config.ThrottledClientMetric)
	handleSafeAPIError(w, logger, configuration, apiName, clientID, "Too many requests at a time.", http.StatusTooManyRequests)
}

//isAPIThrottled will verify if a given API is being throttled, and will error accordingly if it is. This applies only to API-level throttling,
//client-level throttles should use isAPIThrottledForClient.
func isAPIThrottled(w http.ResponseWriter, logger *log.Entry, configuration *config.TeslaConfiguration, apiName config.APIName) bool {
	allowedRate := configuration.GetAPIRateLimit(apiName)

	if allowedRate != config.UnlimitedRate {
		// Need to check throttling.
		currentRate, remainingTimeInPeriod, isThrottled := ThrottlingClient().IsAPIThrottled(apiName, allowedRate)

		if isThrottled {
			// Throttled. Handle the throttling error.
			handleThrottlingError(w, logger, configuration, apiName, "ALL", allowedRate, currentRate, remainingTimeInPeriod)
			return true
		}
	}

	return false
}

func isAPIThrottledForClient(w http.ResponseWriter, logger *log.Entry, configuration *config.TeslaConfiguration, apiName config.APIName, clientID string) bool {
	throttledClient := configuration.GetThrottlingClientFromClientID(clientID)
	allowedRate := configuration.GetClientRateLimit(apiName, throttledClient)

	if allowedRate != config.UnlimitedRate {
		// Need to check throttling.
		currentRate, remainingTimeInPeriod, isThrottled := ThrottlingClient().IsClientThrottled(throttledClient, apiName, allowedRate)

		if isThrottled {
			// Throttled. Handle the throttling error.
			handleThrottlingError(w, logger, configuration, apiName, clientID, allowedRate, currentRate, remainingTimeInPeriod)
			return true
		}
	}

	return false
}

func postCacheMetrics(configuration *config.TeslaConfiguration) {
	stats := cache.ClientDetailsCache().GetCacheStats()

	lruRatio, redisRatio := computeCacheRatios(stats)

	metrics.PostDecimalMetricWithMetricName(configuration, "ALL", "ALL", lruRatio, "Owl_LRUCache_Ratio")
	metrics.PostDecimalMetricWithMetricName(configuration, "ALL", "ALL", redisRatio, "Owl_RedisCache_Ratio")
}

func computeCacheRatios(stats *cache.ClientDetailsCacheStats) (lruRatio float64, redisRatio float64) {
	lruHits := stats.LocalHits
	lruTotal := stats.LocalMisses + lruHits
	lruRatio = 1.0

	if lruTotal > 0 {
		lruRatio = float64(lruHits) / float64(lruTotal)
	}

	redisHits := stats.Hits
	redisTotal := stats.Misses + redisHits
	redisRatio = 1.0

	if redisTotal > 0 {
		redisRatio = float64(redisHits) / float64(redisTotal)
	}

	return lruRatio, redisRatio
}

func postDurationMetrics(configuration *config.TeslaConfiguration, apiName config.APIName, clientString string, duration time.Duration) {
	// Post PMET Metrics
	metrics.PostLatencyMetric(configuration, string(apiName), clientString, duration)
}

func postStatusMetrics(configuration *config.TeslaConfiguration, apiName config.APIName, clientString string, status int) {
	// Post PMET Metrics
	metrics.PostStatusMetric(configuration, string(apiName), clientString, status)
}

func postInvalidReceiptsCountMetrics(configuration *config.TeslaConfiguration, apiName config.APIName, clientString string, count int) {
	// Post PMET Metrics
	metrics.PostCountMetric(configuration, string(apiName), clientString, float64(count), metrics.MetricInvalidReceiptsType)
}

func postCountMetrics(configuration *config.TeslaConfiguration, apiName config.APIName, clientString string, count int, metricType string) {
	// Post PMET Metrics
	metrics.PostCountMetric(configuration, string(apiName), clientString, float64(count), metricType)
}
