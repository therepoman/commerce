package api

import (
	"sync"
	"time"

	"github.com/go-redis/redis"
	"github.com/go-redis/redis_rate"

	"code.justin.tv/commerce/Tesla/config"
)

const (
	throttlingRedisHostDev string = "dev-throttle-redis.dbnchr.ng.0001.usw2.cache.amazonaws.com:6379"
	throttlingRedisPwDev   string = ""

	throttlingRedisHostProd string = "prod-throttle-redis.uki5zr.ng.0001.usw2.cache.amazonaws.com:6379"
	throttlingRedisPwProd   string = ""

	throttlingEvaluationDuration time.Duration = 1 * time.Second
	redisKeyDelimiter            string        = ":"
	redisUnscopedClientSuffix    string        = "#UNSCOPED#"
)

var (
	throttlingInterfaceInstance ThrottlingInterface
	throttlingInterfaceOnce     sync.Once
)

// ThrottlingClient is the client that allows throttling operations.
func ThrottlingClient() ThrottlingInterface {
	throttlingInterfaceOnce.Do(func() {
		// Check if prod and swap.
		host := throttlingRedisHostDev
		pw := throttlingRedisPwDev

		if config.GetCurrentConfiguration().IsProd() {
			host = throttlingRedisHostProd
			pw = throttlingRedisPwProd
		}

		throttlingInterfaceInstance = newRedisThrottlingInterface(host, pw, throttlingEvaluationDuration)
	})

	return throttlingInterfaceInstance
}

// ThrottlingInterface in an interface we can use for throttling.
type ThrottlingInterface interface {
	Ping() bool
	IsClientThrottled(clientID config.ThrottledClient, apiName config.APIName, maxRate int64) (int64, time.Duration, bool)
	IsAPIThrottled(apiName config.APIName, maxRate int64) (int64, time.Duration, bool)
}

type redisThrottlingInterface struct {
	redisClient             *redis.Client
	rateLimiter             *redis_rate.Limiter
	rateEvalutationDuration time.Duration
}

func (r redisThrottlingInterface) Ping() bool {
	_, err := r.redisClient.Ping().Result()

	if err == nil {
		return true
	}

	return false
}

func (r redisThrottlingInterface) IsClientThrottled(clientID config.ThrottledClient, apiName config.APIName, maxRate int64) (int64, time.Duration, bool) {
	currentRate, throttleDuration, isAllowed := r.rateLimiter.Allow(string(apiName)+redisKeyDelimiter+string(clientID), maxRate, r.rateEvalutationDuration)
	return currentRate, throttleDuration, !isAllowed
}

func (r redisThrottlingInterface) IsAPIThrottled(apiName config.APIName, maxRate int64) (int64, time.Duration, bool) {
	currentRate, throttleDuration, isAllowed := r.rateLimiter.Allow(string(apiName)+redisUnscopedClientSuffix, maxRate, r.rateEvalutationDuration)
	return currentRate, throttleDuration, !isAllowed
}

func newRedisThrottlingInterface(redisHost string, redisPassword string, rateEvaluationDuration time.Duration) ThrottlingInterface {
	client := redis.NewClient(&redis.Options{
		Addr:     redisHost,
		Password: redisPassword,
		DB:       0, // use default DB
	})

	rateLimiter := redis_rate.NewLimiter(client)

	return redisThrottlingInterface{client, rateLimiter, rateEvaluationDuration}
}
