package api

import (
	"net/http"
	"strconv"

	"code.justin.tv/commerce/Tesla/cache"
	"code.justin.tv/commerce/Tesla/config"

	"code.justin.tv/foundation/twitchserver"

	"goji.io"
	"goji.io/pat"

	log "github.com/Sirupsen/logrus"
	"github.com/satori/go.uuid"
)

//Server is the API server class.
type Server struct {
	*goji.Mux
	serverConfiguration *config.TeslaConfiguration
}

//GetConfiguration will return the TeslaConfiguration this server is running under.
func (s Server) GetConfiguration() *config.TeslaConfiguration {
	return s.serverConfiguration
}

//NewServer creates a new server object for the Tesla service.
func NewServer() (*Server, error) {
	configuration := config.GetCurrentConfiguration()
	server := twitchserver.NewServer()

	s := &Server{
		server,
		configuration,
	}

	// Health API
	s.HandleFunc(pat.Get("/health"), s.EverythingIsFine)
	// Status API
	s.HandleFunc(pat.Get("/status"), s.CheckHostStatus)
	// User goods retrieval API
	s.HandleFunc(pat.Post("/commerce/user/goods"), s.getGoodsForUserSigV4)
	// User goods fulfillment API
	s.HandleFunc(pat.Post("/commerce/user/goods/fulfill"), s.setStatusForReceiptsSigV4)
	return s, nil
}

//EverythingIsFine is a general API health function. Detailing the health of the service.
func (s *Server) EverythingIsFine(w http.ResponseWriter, r *http.Request) {
	s.safeWrite(w, "OK\n")
	return
}

//CheckHostStatus is a general check of the host function. Detailing the status of the service.
func (s *Server) CheckHostStatus(w http.ResponseWriter, r *http.Request) {
	requestID := uuid.NewV4().String()
	logger := log.WithFields(log.Fields{
		"rID": requestID,
	})

	if isAPIThrottled(w, logger, s.serverConfiguration, config.StatusCheckAPI) {
		// API is throttled. Errors and everything should already have been populated. Simply return.
		return
	}

	// Note the host configuration
	if s.serverConfiguration.IsProd() {
		s.safeWrite(w, "Configuration : PROD\n")
	}
	if s.serverConfiguration.IsBeta() {
		s.safeWrite(w, "Configuration : BETA\n")
	}
	if s.serverConfiguration.IsLocal() {
		s.safeWrite(w, "Configuration : LOCAL\n")
	}

	creds := s.serverConfiguration.GetTProxSigV4Signer().Credentials
	// Verify credentials and aws region
	s.safeWrite(w, "Credentials expired ? "+strconv.FormatBool(creds.IsExpired())+"\n")
	s.safeWrite(w, "AWS Region : "+s.serverConfiguration.GetAwsRegion()+"\n")

	s.safeWrite(w, "Rate Limiting cache online ? "+strconv.FormatBool(ThrottlingClient().Ping())+"\n")
	s.safeWrite(w, "Client Details cache available ? "+strconv.FormatBool(cache.ClientDetailsCache().Ping())+"\n")

	stats := cache.ClientDetailsCache().GetCacheStats()
	lruRatio, redisRatio := computeCacheRatios(stats)

	s.safeWrite(w, "ClientDetailsCache LRU hits : "+strconv.FormatUint(stats.LocalHits, 10)+"\n")
	s.safeWrite(w, "ClientDetailsCache LRU misses : "+strconv.FormatUint(stats.LocalMisses, 10)+"\n")
	s.safeWrite(w, "ClientDetailsCache LRU hit ratio : "+strconv.FormatFloat(lruRatio, 'f', 4, 64)+"\n")
	s.safeWrite(w, "ClientDetailsCache Redis hits : "+strconv.FormatUint(stats.Hits, 10)+"\n")
	s.safeWrite(w, "ClientDetailsCache Redis misses : "+strconv.FormatUint(stats.Misses, 10)+"\n")
	s.safeWrite(w, "ClientDetailsCache Redis hit ratio : "+strconv.FormatFloat(redisRatio, 'f', 4, 64)+"\n")

	// Post a metric for verfication of metric reporting
	postStatusMetrics(s.serverConfiguration, config.StatusCheckAPI, "ALL", http.StatusOK)
	return
}

func (s *Server) safeWrite(w http.ResponseWriter, str string) {
	_, err := w.Write([]byte(str))
	if err != nil {
		panic(err)
	}
}
