package api

import (
	"code.justin.tv/commerce/Tesla/application"
	"code.justin.tv/commerce/Tesla/externalses"
	"encoding/json"
	"io"
	"net/http"
	"sync"
	"time"

	"code.justin.tv/commerce/Tesla/cache"
	tesla "code.justin.tv/commerce/Tesla/client"
	"code.justin.tv/commerce/Tesla/config"
	"code.justin.tv/commerce/Tesla/external"
	"code.justin.tv/commerce/Tesla/externaladg"
	"code.justin.tv/commerce/Tesla/externalaws"

	log "github.com/Sirupsen/logrus"
	uuid "github.com/satori/go.uuid"
)

const SetStatusForReceipts = "SET_STATUS_FOR_RECEIPTS"

type compareReceiptUpdatesLogEntry struct {
	RequestId string
	Matches   bool
	SesGoods  []tesla.ReceiptUpdateResult
	AdgGoods  []tesla.ReceiptUpdateResult
	TimeStamp int64
	ApiCall	  string
}

func (s *Server) setStatusForReceiptsSigV4(w http.ResponseWriter, r *http.Request) {
	requestID := uuid.NewV4().String()
	startTime := time.Now()
	clientID := r.Header.Get("Twitch-Client-ID")
	tuid := r.URL.Query()["tuid"][0]
	logger := log.WithFields(log.Fields{
		"rID":      requestID,
		"clientID": clientID,
	})

	if clientID == "" {
		handleSetStatusForReceiptsSafeAPIError(w, logger, s.serverConfiguration, config.UnknownClient, "Bad request: Missing client id param.", http.StatusBadRequest)
		return
	}

	// Check to see if the client is blacklisted before continuing
	if s.serverConfiguration.IsClientBlacklistedForAPI(clientID, config.SetStatusForReceiptsAPI) {
		postCountMetrics(s.serverConfiguration, config.SetStatusForReceiptsAPI, clientID, 1, config.BlacklistedClientMetric)
		errorString := "Forbidden client calling " + string(config.SetStatusForReceiptsAPI)
		logString := errorString + " : " + clientID
		// We pass clientID instead of clientMetric name specifically for this metric.
		handleSetStatusForReceiptsAPIError(w, logger, s.serverConfiguration, clientID, logString, errorString, http.StatusForbidden)
		return
	}

	// Check throttling.
	if isAPIThrottledForClient(w, logger, s.serverConfiguration, config.SetStatusForReceiptsAPI, clientID) {
		// API is throttled. Errors and everything should already have been populated. Simply return.
		return
	}

	apiContext := external.APIContext{Configuration: s.serverConfiguration, MethodName: string(config.SetStatusForReceiptsAPI), ClientID: clientID}
	clientDetails, clientDetailsErr := cache.ClientDetailsCache().GetClientDetails(apiContext)

	if clientDetailsErr != nil {
		logString := "An error occurred while fetching client details : " + clientDetailsErr.Error()
		handleSetStatusForReceiptsAPIError(w, logger, s.serverConfiguration, config.UnknownClient, logString, clientDetailsErr.Error(), http.StatusInternalServerError)
		return
	}

	clientMetricName := clientDetails.ClientMetricName

	if tuid == "" {
		handleSetStatusForReceiptsSafeAPIError(w, logger, s.serverConfiguration, clientMetricName, "Bad request: Missing tuid param.", http.StatusBadRequest)
		return
	}

	logger.Info("Received valid call: SetStatusForReceipts - " + tuid)

	// Call to ADG Order Service for setReceiptsFulfillmentStatus
	request, parseError := parseSetStatusForReceiptsRequest(r)

	if parseError != nil {
		errorString := "Bad request: Could not parse request body."
		handleSetStatusForReceiptsAPIError(w, logger, s.serverConfiguration, clientMetricName, errorString+" : "+parseError.Error(), errorString, http.StatusBadRequest)
		return
	}

	setStatusForReceiptsResponse, errorResponse, noOpReceiptCount := callSetStatusForReceiptsSigV4(apiContext, clientMetricName, requestID, tuid, request)

	if errorResponse != nil {
		handleSetStatusForReceiptsAPIError(w, logger, s.serverConfiguration, clientMetricName, "An error was returned during the ADGOS call : "+errorResponse.Error(), errorResponse.Error(), errorResponse.GetErrorCode())
		return
	}

	logger.Info("Successfully returned from ADGOS call, returning response.")
	data, jsonErr := json.Marshal(setStatusForReceiptsResponse)
	if jsonErr != nil {
		errorString := "An error occurred during the response serialization"
		handleSetStatusForReceiptsAPIError(w, logger, s.serverConfiguration, clientMetricName, errorString+" : "+jsonErr.Error(), errorString, http.StatusInternalServerError)
		return
	}

	_, err := w.Write(data)
	if err != nil {
		handleSetStatusForReceiptsAPIError(w, logger, s.serverConfiguration, clientMetricName, "Error writing response : "+err.Error(), err.Error(), http.StatusInternalServerError)
		return
	}

	// Handle success
	handleSetStatusForReceiptsAPISuccess(s.serverConfiguration, clientMetricName, noOpReceiptCount, time.Since(startTime))
}

func callSetStatusForReceiptsSigV4(apiContext external.APIContext, clientMetricName string, requestID string, tuid string, request tesla.SetStatusForReceiptsRequest) (*tesla.SetStatusForReceiptsResponse, external.Error, int) {
	var wg sync.WaitGroup

	var sesReceiptUpdateResults = make([]tesla.ReceiptUpdateResult, 0)
	var sesErr = external.Error(nil)
	var sesNoOpCount = 0

	var adgReceiptUpdateResults = make([]tesla.ReceiptUpdateResult, 0)
	var adgErr = external.Error(nil)
	var adgNoOpCount = 0

	sesApplication := application.SESApplication{
		RequestID:                           requestID,
		Tuid:                                tuid,
		ApiContext:                          apiContext,
		ClientMetricName:                    clientMetricName,
		SesGetAccountEntitlementInterface:   externalses.NewSESGetAccountEntitlements(apiContext, clientMetricName),
		SesUpdateFulfillmentStatusInterface: externalses.NewSESUpdateFulfillmentStatus(apiContext, clientMetricName),
		ClientProductMappingInterface:       externalaws.NewDynamoClientProductMappingService(apiContext, clientMetricName),
	}

	adgApplication := application.ADGApplication{
		RequestID:        requestID,
		Tuid:             tuid,
		ApiContext:       apiContext,
		ClientMetricName: clientMetricName,
		ADGOService:      externaladg.NewADGOrderService(apiContext, clientMetricName),
		ADGEService:      nil,
		RevokeService:    externalaws.NewDynamoRevokeService(apiContext, clientMetricName),
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//  Thread SES/ADG
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	wg.Add(1)
	go func(sesUpdatedEntitlements *[]tesla.ReceiptUpdateResult, sesErr *external.Error, sesNoOpCount *int) {
		defer wg.Done()

		// This defer function is to catch any panics from the SES call
		// We want this here since we do not want to impact the API call with the new SES call
		defer func() {
			if r := recover(); r != nil {
				var ok bool
				err, ok := r.(error)
				if !ok {
					log.Error("There was an issue parsing as an error.")
				}

				log.Error("there was an error in calling TPROX/SES: " + err.Error())
			}
		}()

		*sesUpdatedEntitlements, *sesErr, *sesNoOpCount = callSetStatusForReceiptsInApplication(sesApplication, request)
	}(&sesReceiptUpdateResults, &sesErr, &sesNoOpCount)

	wg.Add(1)
	go func(adgUpdatedEntitlements *[]tesla.ReceiptUpdateResult, adgErr *external.Error, adgNoOpCount *int) {
		defer wg.Done()
		*adgUpdatedEntitlements, *adgErr, *adgNoOpCount = callSetStatusForReceiptsInApplication(adgApplication, request)
	}(&adgReceiptUpdateResults, &adgErr, &sesNoOpCount)

	wg.Wait()
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//  Thread SES/ADG
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	logResultString, _ := json.Marshal(compareResultingReceipts(requestID, sesReceiptUpdateResults, adgReceiptUpdateResults))
	log.Info(config.SESADGComparisonLogTag + string(logResultString) + config.SESADGComparisonLogTag)

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//  SES
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO Verify the receipts match up.
	for _, receipt := range sesReceiptUpdateResults {
		receipt.ReceiptID = "valid"

	}

	if sesErr != nil {
		log.Warnf("Error in getting entitlements from SES.")
		log.Warnf(sesErr.Error())
	}

	if sesNoOpCount != 0 {
		log.Warnf("There were %d noOps from SES.", sesNoOpCount)
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//  ADG
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if adgErr != nil {
		return nil, adgErr, 0
	}
	return &tesla.SetStatusForReceiptsResponse{ReceiptUpdateResults: adgReceiptUpdateResults}, nil, adgNoOpCount
}

func callSetStatusForReceiptsInApplication(callApplication application.EntitlementApplicationInterface, request tesla.SetStatusForReceiptsRequest) ([]tesla.ReceiptUpdateResult, external.Error, int) {
	entitlementApplication := application.EntitlementApplication{
		Application: callApplication,
	}

	updatedEntitlements, updateFulfillmentErr, noOpCount := entitlementApplication.Application.UpdateFulfillmentStatus(request)

	if updateFulfillmentErr != nil {
		// TODO update this so that it throws actual errors when we deprecate adg
		// We do not want to error out right now, we can just log this error and move on
		log.Warnf("There was an error in the UpdateFulfillmentStatusSigV4 Call: " + updateFulfillmentErr.Error())
		//return nil, sesUpdateFulfillmentStatusErr, 0
	}

	if noOpCount > 0 {
		log.Warnf("There were %d noOps.", noOpCount)
	}

	return updatedEntitlements, updateFulfillmentErr, noOpCount
}

func parseSetStatusForReceiptsRequest(r *http.Request) (tesla.SetStatusForReceiptsRequest, error) {
	decoder := json.NewDecoder(r.Body)
	var teslaRequest tesla.SetStatusForReceiptsRequest
	err := decoder.Decode(&teslaRequest)
	if err != nil {
		return tesla.SetStatusForReceiptsRequest{}, err
	}

	defer closeBody(r.Body)
	return teslaRequest, nil
}

func closeBody(body io.ReadCloser) {
	err := body.Close()
	if err != nil {
		panic("There was an error closing a request body : " + err.Error())
	}
}

func handleSetStatusForReceiptsAPISuccess(configuration *config.TeslaConfiguration, clientMetricName string, noOpReceiptCount int, duration time.Duration) {
	postDurationMetrics(configuration, config.SetStatusForReceiptsAPI, clientMetricName, duration)
	postStatusMetrics(configuration, config.SetStatusForReceiptsAPI, clientMetricName, http.StatusOK)
	postInvalidReceiptsCountMetrics(configuration, config.SetStatusForReceiptsAPI, clientMetricName, noOpReceiptCount)
	postCacheMetrics(configuration)
}

func handleSetStatusForReceiptsSafeAPIError(w http.ResponseWriter, logger *log.Entry, configuration *config.TeslaConfiguration, clientMetricName string, errorString string, errorStatus int) {
	handleSafeAPIError(w, logger, configuration, config.SetStatusForReceiptsAPI, clientMetricName, errorString, errorStatus)
}

func handleSetStatusForReceiptsAPIError(w http.ResponseWriter, logger *log.Entry, configuration *config.TeslaConfiguration, clientMetricName string, logString string, errorString string, errorStatus int) {
	handleAPIError(w, logger, configuration, config.SetStatusForReceiptsAPI, clientMetricName, logString, errorString, errorStatus)
}

func compareResultingReceipts(requestId string, sesGoods []tesla.ReceiptUpdateResult, adgGoods []tesla.ReceiptUpdateResult) compareReceiptUpdatesLogEntry {
	logResult := compareReceiptUpdatesLogEntry{
		RequestId: requestId,
		Matches:   false,
		SesGoods:  sesGoods,
		AdgGoods:  adgGoods,
		TimeStamp: time.Now().Unix(),
		ApiCall:   SetStatusForReceipts,
	}

	if len(sesGoods) != len(adgGoods) {
		log.Warnf("The results from ADG and SES differ in length")
	} else {
		for _, sesGood := range sesGoods {
			contains := false

			for _, adgGood := range adgGoods {
				if adgGood == sesGood {
					contains = true
				}
			}

			if !contains {
				log.Warnf("The results from ADG and SES differ in resulting goods")
				logResult.Matches = false
				break
			}

			logResult.Matches = contains
		}
	}

	return logResult
}