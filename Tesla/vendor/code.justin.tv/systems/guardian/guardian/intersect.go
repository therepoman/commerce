package guardian

// StringSliceIntersect returns true if a member of slice a is present in slice b
func StringSliceIntersect(a, b []string) (ok bool) {
	for _, x := range a {
		for _, y := range b {
			if y == x {
				return true
			}
		}
	}
	return false
}
