package guardian

import "golang.org/x/oauth2"

// TokenCheck is the object returned from CheckToken call. includes info on
// access token and user associated with token
type TokenCheck struct {
	Token *oauth2.Token `json:"token"`
	User  *User         `json:"user"`
}
