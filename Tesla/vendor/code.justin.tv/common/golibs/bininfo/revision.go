package bininfo

// Please never change this var without very careful thought.
// This var is private to prevent it from changing during runtime.
// You'll never change your revision in production. However, it is
// psuedo-public in the sense that you'll set it during builds. Given
// builds needs to reference this var directly changing it is likely
// to break integrations.
var revision string

// Revision returns the indentifier set during build which
// references this version of the code. To set this during build:
// `go build -ldflags "-X code.justin.tv/common/golibs/bininfo.revision <sha>"`
// If it is not set the default is an empty string
func Revision() string {
	return revision
}
