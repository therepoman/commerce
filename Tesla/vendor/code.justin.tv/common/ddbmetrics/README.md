ddbmetrics
=======================

Reports DynamoDB provisioned and consumed read/write capacity units to statsd.

[API Documentation](https://godoc.internal.justin.tv/code.justin.tv/common/ddbmetrics)

```go
import (
  "code.justin.tv/common/ddbmetrics"
  "github.com/aws/aws-sdk-go/service/dynamodb"
  "github.com/cactus/go-statsd-client/statsd"
)

// Create publisher and start listening for capacity event
db := dynamodb.New()
stats, _ := statsd.NewClient("127.0.0.1:8125", "exampleprefix")

m := ddbmetrics.New(db, stats)
m.Start()

// send consumption metrics after you do a query.
out, _ := db.GetItem(&dynamodb.GetItemInput{
  ReturnConsumedCapacity: aws.String("INDEXES"),
})

m.Report(ddbmetrics.Read, out.ConsumedCapacity)
```
