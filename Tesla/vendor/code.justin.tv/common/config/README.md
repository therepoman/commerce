# config

[![GoDoc](http://godoc.internal.justin.tv/code.justin.tv/common/config?status.svg)](http://godoc.internal.justin.tv/code.justin.tv/common/config)

Enables packages to define configuration keys with defaults. These keys are read in during application start from either command-line flags or environment variables. 

Since the configuration keys are defined per-package, this allows applications which import packages integrating with common/config to automatically become configurable without adding boilerplate code in main.go.