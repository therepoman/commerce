
GIT_COMMIT ?= $(shell git rev-parse --verify HEAD)
ENVIRONMENT ?= development
LOCAL_PACKAGES ?= $(shell glide novendor)
GOIMPORTS_REV ?= 5e2ae75
GOCOVMERGE_REV ?= b5bfa59
GOMETALINTER_REV ?= 25d9c68

go_get_rev = go get -d $(1)/... && cd "$(GOPATH)/src/$(1)" && git reset --hard $(2) && go install ./...

precommit: fix lint test build

setup:
	$(call go_get_rev,"golang.org/x/tools/cmd/goimports",$(GOIMPORTS_REV))
	$(call go_get_rev,"github.com/wadey/gocovmerge",$(GOCOVMERGE_REV))
	$(call go_get_rev,"github.com/alecthomas/gometalinter",$(GOMETALINTER_REV))
	gometalinter --install

lint:
	gometalinter --vendor --dupl-threshold=150 --min-confidence=.3 --tests --deadline=90s --disable-all -Egolint -Etest -Eineffassign -Etestify -Eunconvert -Estaticcheck -Egoconst -Egocyclo -Eerrcheck -Egofmt -Evet -Edupl -Einterfacer -Estructcheck -Evetshadow -Egosimple -Egoimports -Evarcheck -Emisspell -Ealigncheck -Etest ./...

test:
	go test -race $(LOCAL_PACKAGES)

integration_test:
	env ENVIRONMENT=$(ENVIRONMENT) go test -race -tags=integration $(LOCAL_PACKAGES)

build:
	go build -ldflags "-X main.CodeVersion=$(GIT_COMMIT)" $(MAIN_DIR)

run:
	env ENVIRONMENT=$(ENVIRONMENT) go run -race -ldflags "-X main.CodeVersion=$(GIT_COMMIT)" $(MAIN_DIR)/main.go

fix:
	git ls-files -- *.go | grep -v vendor | xargs gofmt -s -w
	git ls-files -- *.go | grep -v vendor | xargs goimports -w

integration_test_cover:
	rm -f cover.out uncovered_functions.txt uncovered_functions.txt function_coverage.txt
	go list -f "env ENVIRONMENT=$(ENVIRONMENT) go test -race -timeout 3m -tags=integration -coverprofile {{.Name}}_{{len .Imports}}_{{len .Deps}}.coverprofile -coverpkg ./cmd/... {{.ImportPath}}" ./cmd/... | xargs -I {} bash -c {}
	gocovmerge `ls *.coverprofile` > cover.out
	rm *.coverprofile
	go tool cover -func cover.out > function_coverage.txt
	grep '\t0.0%' function_coverage.txt | cat > uncovered_functions.txt
	rm cover.out
	if [ -s uncovered_functions.txt ]; then (echo "There are uncovered functions" && cat uncovered_functions.txt && exit 1) fi;
	rm -f cover.out uncovered_functions.txt uncovered_functions.txt function_coverage.txt
