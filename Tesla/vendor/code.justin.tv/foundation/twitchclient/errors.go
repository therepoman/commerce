package twitchclient

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// Error is the expected format of a twitchhttp server error
type Error struct {
	Message    string `json:"message"`
	StatusCode int    `json:"-"`
}

func (e *Error) Error() string {
	return fmt.Sprintf("%d: %s", e.StatusCode, e.Message)
}

// HandleFailedResponse decodes a response body from a failed HTTP request into
// an Error structure. If the body is not readable, the message will be replaced by
// a summary of the failure. The body must be application/json.
func HandleFailedResponse(resp *http.Response) *Error {
	httpError := &Error{StatusCode: resp.StatusCode}

	var rErr error
	var body []byte
	switch resp.Header.Get("Content-Type") {
	case "application/json":
		rErr = json.NewDecoder(resp.Body).Decode(httpError)
	default:
		body, rErr = ioutil.ReadAll(resp.Body)
		httpError.Message = string(body)
	}
	if rErr != nil {
		httpError.Message = fmt.Sprintf("Unable to read response body: %s", rErr)
	}

	return httpError
}
