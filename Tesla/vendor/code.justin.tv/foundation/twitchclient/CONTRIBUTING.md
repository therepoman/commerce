## Contributing

twitchclient is a core library at twitch and its stability and quality must reflect that.
Bugs may cause critical systems to fail and hasty design decisions can lead to massive
migration efforts after the next iteration. As a result, the process to contribute
is stricter than most projects.

Before writing any code, please open a Github issue. If it's a feature request, explain the
feature that you're asking for and propose a design. If it's a bug, explain the failure
and show steps to reproduce. While a Github issue is not mandatory, it may save you from
having to rethink your changes after you implement and test them.

When you're ready, open a pull request. It _must_ be reviewed and approved by a foundation
engineering member (you can find most of us in #api). Pull requests will be rejected unless
they're accompanied by tests which demonstrate the changes being made to the library.
Changes which can't be tested (e.g. fixing a typo in README.md are exempt.)

When your changes are merged in, if any changes have been made to production code, a
new release following [Semantic Versioning](http://semver.org/) will be created.

## Suggestions

If you'd just like to leave feedback or put something on our radar, please open a Github issue.
