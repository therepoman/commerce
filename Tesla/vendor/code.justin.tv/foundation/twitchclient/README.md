# twitchclient

[![GoDoc](http://godoc.internal.justin.tv/code.justin.tv/foundation/twitchclient?status.svg)](http://godoc.internal.justin.tv/code.justin.tv/foundation/twitchclient)

Package twitchclient and twitchserver enables quicker creation of production-ready HTTP clients and servers.

This package contains only the client implementation of the client-server

You only need to depend on this library if you are building / using a client to talk to a twitchserver
