package twitchclient

// ReqOpts contains common request options used to interface with Twitch services.
type ReqOpts struct {
	// AuthorizationToken is the token received from our Authorization service
	// which can be forwarded to other Twitch services. Do not set this if
	// the request leaves the Twitch infrastructure.
	AuthorizationToken string

	// ClientID is the alphanumeric ID of the client making the request.
	ClientID string

	// ClientRowID is the database row ID of the client making the request.
	ClientRowID string

	// Name of request timing stats. Stats are emitted in the format
	// {StatName}.{Response.StatusCode} or {StatName}.0 (for errors).
	StatName string

	// Sample rate of successful request stats, errors are never sampled.
	StatSampleRate float32
}

// MergeReqOpts merges the provided request options with reasonable defaults.
func MergeReqOpts(input *ReqOpts, defaults ReqOpts) ReqOpts {
	if input == nil {
		return defaults
	}
	return ReqOpts{
		AuthorizationToken: mergeString(input.AuthorizationToken, defaults.AuthorizationToken),
		ClientRowID:        mergeString(input.ClientRowID, defaults.ClientRowID),
		StatName:           mergeString(input.StatName, defaults.StatName),
		StatSampleRate:     mergeFloat32(input.StatSampleRate, defaults.StatSampleRate),
	}
}

func mergeString(input, defaults string) string {
	if input != "" {
		return input
	}
	return defaults
}

func mergeFloat32(input, defaults float32) float32 {
	if input != 0 {
		return input
	}
	return defaults
}
