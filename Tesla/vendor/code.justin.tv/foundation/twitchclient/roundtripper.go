package twitchclient

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"code.justin.tv/chat/timing"
	"code.justin.tv/common/golibs/pkgpath"
	"code.justin.tv/feeds/ctxlog"
	"github.com/cactus/go-statsd-client/statsd"
)

const (
	// TwitchAuthorizationHeader is the header used when forwarding Authorization tokens.
	TwitchAuthorizationHeader = "Twitch-Authorization"
	// TwitchClientRowIDHeader is the header used when passing client row IDs.
	TwitchClientRowIDHeader = "Twitch-Client-Row-ID"
	// TwitchClientIDHeader is the header used when passing client row IDs.
	TwitchClientIDHeader = "Twitch-Client-ID"
	// TwitchRepositoryHeader is a header with the name of the repository that is using the client.
	TwitchRepositoryHeader = "Twitch-Repository"
)

type contextKey int // unexported to prevent collisions with other packages

var (
	headersAuthorizationKey = new(contextKey)
	headersClientRowIDKey   = new(contextKey)
	headersClientIDKey      = new(contextKey)
	timingStatsKey          = new(contextKey)
)

type timingStatsConf struct {
	name       string
	sampleRate float32
}

// WrapWithTwitchHTTPRoundTripper adds a twitchhttp RoundTripper
// that handles stats, auth headers and xact.
func wrapWithTwitchHTTPRoundTripper(baseRT http.RoundTripper, stats statsd.Statter, xactName string, suppressRepositoryHeader bool, ctxLog *ctxlog.Ctxlog) http.RoundTripper {
	if xactName == "" {
		xactName = "twitchhttp"
	}
	return &twitchHTTPRoundTripper{BaseRT: baseRT, Stats: stats, XactName: xactName, SuppressRepositoryHeader: suppressRepositoryHeader, ctxlog: ctxLog}
}

// Adds twitchhttp specific functionality
type twitchHTTPRoundTripper struct {
	BaseRT http.RoundTripper

	Stats    statsd.Statter
	XactName string

	ctxlog                   *ctxlog.Ctxlog
	SuppressRepositoryHeader bool // true to avoid auto-sending the Twitch-Repository header (needed for secutiry when calling into 3rd party services)
}

func (rt *twitchHTTPRoundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	startTime := time.Now()
	// NewHTTPRequest is idempotent
	req = ctxlog.NewHTTPRequest(rt.ctxlog, req)
	ctx := req.Context()

	// Xact (track if enabled in the ctx)
	xact, xactEnabled := timing.XactFromContext(ctx)
	var sub *timing.SubXact
	if xactEnabled {
		sub = xact.Sub(rt.XactName)
		sub.Start()
	}

	// Set headers if needed
	rt.setHeaders(req)

	// Call baseRT
	resp, err := rt.BaseRT.RoundTrip(req)
	statusCode := 0
	if err == nil && resp != nil {
		statusCode = resp.StatusCode
	}

	// Timing Stats
	rt.timingStats(req, startTime, statusCode)

	// Xact End
	if xactEnabled {
		sub.End()
	}

	// Make sure to discard the remaining http body upon calling Close()
	// NOTE: Re-evaluate if this behavior is still needed on Go1.7
	if resp != nil {
		resp.Body = &readAllOnCloseBody{body: resp.Body}
	}

	return resp, err
}

// setHeaders adds client-Row, auth and repo HTTP headers to the request if needed.
func (rt *twitchHTTPRoundTripper) setHeaders(req *http.Request) {
	ctx := req.Context()

	if clientID := twitchClientIDFromContext(ctx); clientID != "" {
		req.Header.Set(TwitchClientIDHeader, clientID)
	}

	if clientRowID := twitchClientRowIDFromContext(ctx); clientRowID != "" {
		req.Header.Set(TwitchClientRowIDHeader, clientRowID)
	}

	if authHeader := twitchAuthorizationFromContext(ctx); authHeader != "" {
		req.Header.Set(TwitchAuthorizationHeader, authHeader)
	}

	if repo, ok := pkgpath.Main(); ok && !rt.SuppressRepositoryHeader {
		req.Header.Set(TwitchRepositoryHeader, repo)
	}
}

// timingStats times duration of the http request,
// given a startTime (recorded before the base RoundTrip request was made) and a
// response statusCode (which is 0 if there was an error).
func (rt *twitchHTTPRoundTripper) timingStats(req *http.Request, startTime time.Time, statusCode int) {
	ctx := req.Context()

	statName, sampleRate := timingStatsFromContext(ctx)
	if rt.Stats != nil && statName != "" && sampleRate != 0 {
		duration := time.Since(startTime)

		stat := fmt.Sprintf("%s.%d", statName, statusCode)
		_ = rt.Stats.TimingDuration(stat, duration, sampleRate)
	}
}

// WithReqOpts is a convenience function to annotate the context with the available ReqOpts
func WithReqOpts(ctx context.Context, opts ReqOpts) context.Context {
	if opts.StatName != "" {
		ctx = WithTimingStats(ctx, opts.StatName, opts.StatSampleRate)
	}
	if opts.AuthorizationToken != "" {
		ctx = WithTwitchAuthorization(ctx, opts.AuthorizationToken)
	}
	if opts.ClientID != "" {
		ctx = WithTwitchClientID(ctx, opts.ClientID)
	}
	if opts.ClientRowID != "" {
		ctx = WithTwitchClientRowID(ctx, opts.ClientRowID)
	}
	return ctx
}

// WithTwitchAuthorization annotates the context to add the auth header
func WithTwitchAuthorization(ctx context.Context, authorizationToken string) context.Context {
	return context.WithValue(ctx, headersAuthorizationKey, authorizationToken)
}

// WithTwitchClientID adds the client id to the context
func WithTwitchClientID(ctx context.Context, clientID string) context.Context {
	return context.WithValue(ctx, headersClientIDKey, clientID)
}

// WithTwitchClientRowID annotates the context to add the client row id header
func WithTwitchClientRowID(ctx context.Context, clientRowID string) context.Context {
	return context.WithValue(ctx, headersClientRowIDKey, clientRowID)
}

// WithTimingStats annotates the context with a stat name and sample rate to allow collecting timing stats.
// i.e. ctx = WithStats(ctx, "my_cool_stat", 0.1)
func WithTimingStats(ctx context.Context, statName string, statSampleRate float32) context.Context {
	return context.WithValue(ctx, timingStatsKey, timingStatsConf{statName, statSampleRate})
}

func twitchAuthorizationFromContext(ctx context.Context) string {
	if val, ok := ctx.Value(headersAuthorizationKey).(string); ok {
		return val
	}
	return ""
}

func twitchClientIDFromContext(ctx context.Context) string {
	if val, ok := ctx.Value(headersClientIDKey).(string); ok {
		return val
	}
	return ""
}

func twitchClientRowIDFromContext(ctx context.Context) string {
	if val, ok := ctx.Value(headersClientRowIDKey).(string); ok {
		return val
	}
	return ""
}

func timingStatsFromContext(ctx context.Context) (string, float32) {
	if val, ok := ctx.Value(timingStatsKey).(timingStatsConf); ok {
		return val.name, val.sampleRate
	}
	return "", 0
}
