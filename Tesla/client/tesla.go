package tesla

import (
	"bytes"
	"encoding/json"
	"net/url"

	"code.justin.tv/foundation/twitchclient"

	"golang.org/x/net/context"
)

const (
	defaultTimingXactName = "TwitchEntitlementService"
	defaultStatSampleRate = 0.1
)

// Client provides an interface for the service client
type Client interface {
	GetGoodsForUser(ctx context.Context, tuid string, clientID string, reqOpts *twitchclient.ReqOpts) (*GetGoodsForUserResponse, error)
	SetStatusForReceipts(ctx context.Context, tuid string, clientID string, request SetStatusForReceiptsRequest, reqOpts *twitchclient.ReqOpts) (*SetStatusForReceiptsResponse, error)
}

type client struct {
	twitchclient.Client
}

// NewClient creates a new client for use in making service calls.
func NewClient(conf twitchclient.ClientConf) (Client, error) {
	if conf.TimingXactName == "" {
		conf.TimingXactName = defaultTimingXactName
	}
	twitchClient, err := twitchclient.NewClient(conf)

	return &client{twitchClient}, err
}

func (c *client) GetGoodsForUser(ctx context.Context, tuid string, clientID string, reqOpts *twitchclient.ReqOpts) (*GetGoodsForUserResponse, error) {
	query := url.Values{}
	query.Set("tuid", tuid)
	url := "/commerce/user/goods?" + query.Encode()

	req, err := c.NewRequest("POST", url, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.twitch-entitlement-service.get_goods_for_user",
		StatSampleRate: defaultStatSampleRate,
	})

	var getGoodsForUserResponse GetGoodsForUserResponse
	_, err = c.DoJSON(ctx, &getGoodsForUserResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getGoodsForUserResponse, nil
}

// SetStatusForReceipts API Call using twitch HTTP client
func (c *client) SetStatusForReceipts(ctx context.Context, tuid string, clientID string, request SetStatusForReceiptsRequest, reqOpts *twitchclient.ReqOpts) (*SetStatusForReceiptsResponse, error) {
	query := url.Values{}
	query.Set("tuid", tuid)
	url := "/commerce/user/goods/fulfill?" + query.Encode()

	requestBody, jsonErr := json.Marshal(request)
	if jsonErr != nil {
		return nil, jsonErr
	}

	req, err := c.NewRequest("POST", url, bytes.NewBuffer(requestBody))
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.twitch-entitlement-service.set_status_for_receipts",
		StatSampleRate: defaultStatSampleRate,
	})

	var setStatusForReceiptsResponse SetStatusForReceiptsResponse
	_, err = c.DoJSON(ctx, &setStatusForReceiptsResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &setStatusForReceiptsResponse, nil
}
