package external

import (
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"code.justin.tv/commerce/Tesla/metrics"
)

// Metric constants
const (
	httpRequestMetric          = ":RequestCreation"
	httpCallMetric             = ":RequestNetworkCall"
	httpParseMetric            = ":RequestParse"
	createRequestErrorMessage  = "There was an error creating the internal request: "
	performRequestErrorMessage = "There was an error making the internal request: "
	readResponseErrorMessage   = "There was an error while reading in the response body: "
	stepFailedMetric           = 1.0
	stepSucceededMetric        = 0.0
)

// Call interface. Represent a call to an external service of some sort.
type Call interface {
	Call(Request) (Response, Error)
}

// EmptyCall is a basic implementation of call that doesn't know what to do
// but fail. Should only be used in CallFactories when a better, more
// appriopriate call can't be found.
type EmptyCall struct{}

// Call method for the EmptyCall.
// Serves the provided request by throwing an InternalServerError.
func (c EmptyCall) Call(r Request) (Response, Error) {
	return nil, NewInternalServerError("The service is misconfigured; it can't find the appropriate way to call internal dependencies")
}

// BaseCall is a simple implementation of an HTTP call.
type BaseCall struct{}

// BaseCallWithMetrics is a simple implementation of an HTTP call that additionally
// emits call-related metrics.
type BaseCallWithMetrics struct {
	BaseCall      BaseCall
	MetricContext MetricContext
}

// Call  method for BaseCall. Serves the provided request using HTTP.
func (c BaseCall) Call(r Request) (Response, Error) {
	// Create an HTTP Request (this includes signing the request when necessary)
	req, err := c.makeRequest(r)
	if err != nil {
		return nil, NewInternalServerError(createRequestErrorMessage + err.Error())
	}

	// Perform the network call
	resp, err := c.performRequest(req)
	if err != nil {
		return nil, NewInternalServerError(performRequestErrorMessage + err.Error())
	}

	// Read the response
	body, err := c.readResponse(resp)
	if err != nil {
		return nil, NewInternalServerError(readResponseErrorMessage + err.Error())
	}
	return baseResponse{status: resp.Status, statusCode: resp.StatusCode, body: body}, nil
}

// Helper method to create a request
func (c BaseCall) makeRequest(r Request) (*http.Request, error) {
	return r.ToHTTPRequest()
}

// Helper method to perform a request
func (c BaseCall) performRequest(req *http.Request) (*http.Response, error) {
	return http.DefaultClient.Do(req)
}

// Helper method to read a response
func (c BaseCall) readResponse(resp *http.Response) ([]byte, error) {
	defer closeBody(resp.Body)
	return ioutil.ReadAll(resp.Body)
}

// Helper method to post a count metric
func (c BaseCallWithMetrics) postBaseCallCountMetric(value float64, suffix string) {
	metrics.PostCountMetric(
		c.MetricContext.APIContext.Configuration,
		c.MetricContext.APIContext.MethodName,
		c.MetricContext.ClientMetricName,
		value,
		c.MetricContext.MetricNames.FailureName+suffix)
}

// Helper method to post a latency metric
func (c BaseCallWithMetrics) postBaseCallLatencyMetric(startTime time.Time, suffix string) {
	metrics.PostLatencyMetricWithMetricName(
		c.MetricContext.APIContext.Configuration,
		c.MetricContext.APIContext.MethodName,
		c.MetricContext.ClientMetricName,
		time.Since(startTime),
		c.MetricContext.MetricNames.LatencyName+suffix)
}

// Call  method for BaseCallWithMetrics. Serves the provided request using HTTP.
func (c BaseCallWithMetrics) Call(r Request) (Response, Error) {
	// Create an HTTP Request (this includes signing the request when necessary)
	httpRequestCreationStart := time.Now()
	req, err := c.BaseCall.makeRequest(r)
	if err != nil {
		c.postBaseCallCountMetric(stepFailedMetric, httpRequestMetric)
		return nil, NewInternalServerError(createRequestErrorMessage + err.Error())
	}
	c.postBaseCallLatencyMetric(httpRequestCreationStart, httpRequestMetric)
	c.postBaseCallCountMetric(stepSucceededMetric, httpRequestMetric)

	// Perform the network call
	httpCallStart := time.Now()
	resp, err := c.BaseCall.performRequest(req)
	if err != nil {
		c.postBaseCallCountMetric(stepFailedMetric, httpCallMetric)
		return nil, NewInternalServerError(performRequestErrorMessage + err.Error())
	}
	c.postBaseCallLatencyMetric(httpCallStart, httpCallMetric)
	c.postBaseCallCountMetric(stepSucceededMetric, httpCallMetric)

	// Read the response
	httpResponseReadStart := time.Now()
	body, err := c.BaseCall.readResponse(resp)
	if err != nil {
		c.postBaseCallCountMetric(stepFailedMetric, httpParseMetric)
		return nil, NewInternalServerError(readResponseErrorMessage + err.Error())
	}
	c.postBaseCallLatencyMetric(httpResponseReadStart, httpParseMetric)
	c.postBaseCallCountMetric(stepSucceededMetric, httpParseMetric)

	return baseResponse{status: resp.Status, statusCode: resp.StatusCode, body: body}, nil
}

func closeBody(body io.ReadCloser) {
	err := body.Close()
	if err != nil {
		panic("There was an error closing a request body : " + err.Error())
	}
}
