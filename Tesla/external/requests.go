package external

import (
	"errors"
	"io"
	"net/http"
	"time"

	"github.com/aws/aws-sdk-go/aws/signer/v4"
)

// Request is the abstract request interface.
// This represents the input just before it is to be sent to an external service
type Request interface {
	GetCallMethod() string
	GetCallURL() string
	GetPayload() (io.ReadSeeker, error)
	GetHeaders() map[string]string
	ToHTTPRequest() (*http.Request, error)
}

// BaseRequest is a default implementation of the external.Request interface,
// omitting the getPaylod() call, as that will differ for different requests.
type BaseRequest struct {
	callMethod string
	callURL    string
	headers    map[string]string
}

// GetCallMethod returns the call method specified in the request.
func (r BaseRequest) GetCallMethod() string {
	return r.callMethod
}

// GetCallURL returns the URL to which the request should be made.
func (r BaseRequest) GetCallURL() string {
	return r.callURL
}

// GetHeaders returs the map of headers meant to be used with the request.
func (r BaseRequest) GetHeaders() map[string]string {
	r.generateDefaultHeaders()

	return r.headers
}

func (r *BaseRequest) generateDefaultHeaders() {
	if r.headers == nil {
		r.headers = make(map[string]string)
	}

	// These headers will be the same on every request, so set them here.
	// (This also overwrites any value that may have been previously set)
	r.headers["Content-Encoding"] = "amz-1.0"
	r.headers["User-Agent"] = "TwitchEntitlementService"
}

// SerializableRequest is a request whose payload is meant to be JSON serialized
// prior to sending. The input parameter provided during construction will be
// used as the serialized payload.
type serializableRequest struct {
	input interface{}
	BaseRequest
}

func (r serializableRequest) GetPayload() (io.ReadSeeker, error) {
	reader, err := serializeToReader(r.input)
	if err != nil {
		return nil, err
	}
	return reader, nil
}

func (r serializableRequest) ToHTTPRequest() (*http.Request, error) {
	// Create the request.
	payload, err := r.GetPayload()
	if err != nil {
		return nil, errors.New("There was an error creating the payload for the internal request: " + err.Error())
	}

	return r.ToHTTPRequestWithPayload(payload)
}

func (r serializableRequest) ToHTTPRequestWithPayload(payload io.Reader) (*http.Request, error) {
	// Create the request.
	req, err := http.NewRequest(r.GetCallMethod(), r.GetCallURL(), payload)
	if err != nil {
		return nil, errors.New("There was an error creating the internal request: " + err.Error())
	}

	// Set the headers for the request.
	for k, v := range r.GetHeaders() {
		req.Header.Set(k, v)
	}

	return req, nil
}

// NewSerializableRequest creates a new SerializableRequest and returns it.
func NewSerializableRequest(m string, u string, h map[string]string, i interface{}) Request {
	request := serializableRequest{input: i}
	request.callMethod = m
	request.callURL = u
	request.headers = h
	return request
}

// NewSerializableRequest creates a new SerializableRequest and returns it.
func NewSerializableRequestWithModel(m string, u string, h map[string]string, i interface{}) Request {
	request := serializableRequest{input: i}
	request.callMethod = m
	request.callURL = u
	request.headers = h
	return request
}

type sigV4SerializableRequest struct {
	awsRegion     string
	serviceName   string
	signatureTime time.Time
	signer        *v4.Signer
	serializableRequest
}

func (r sigV4SerializableRequest) ToHTTPRequest() (*http.Request, error) {
	// Create the payload for the POST request.
	payload, err := r.serializableRequest.GetPayload()
	if err != nil {
		return nil, err
	}

	// Create the http request.
	postRequest, err := r.serializableRequest.ToHTTPRequestWithPayload(payload)
	if err != nil {
		return nil, err
	}

	// Sign the request with sigV4.
	_, err = r.signer.Sign(postRequest, payload, r.serviceName, r.awsRegion, r.signatureTime)
	if err != nil {
		return nil, err
	}

	return postRequest, nil
}

// NewSigV4SerializableRequest creates a new serializable request signed by sigV4. The timestamp of the signature is assumed
// to be time.Now()
type NewSigV4SerializableRequestMaker func (m string, u string, h map[string]string, i interface{}, signer *v4.Signer, service string, region string) Request

func NewSigV4SerializableRequest(m string, u string, h map[string]string, i interface{}, signer *v4.Signer, service string, region string) Request {
	request := sigV4SerializableRequest{awsRegion: region, serviceName: service, signatureTime: time.Now(), signer: signer}
	request.input = i
	request.callMethod = m
	request.callURL = u
	request.headers = h
	return request
}

// NewSigV4SerializableRequestWithTimestamp creates a new serializable request signed by sigV4.
func NewSigV4SerializableRequestWithTimestamp(m string, u string, h map[string]string, i interface{}, signer *v4.Signer, service string, region string, t time.Time) Request {
	request := sigV4SerializableRequest{awsRegion: region, serviceName: service, signatureTime: t, signer: signer}
	request.input = i
	request.callMethod = m
	request.callURL = u
	request.headers = h
	return request
}
