package external

import (
	"io"
	"net/http"
)

type EmptyRequest struct {
	BaseRequest
}

func (r EmptyRequest) GetPayload() (io.ReadSeeker, error) {
	return nil, nil
}

func (r EmptyRequest) ToHTTPRequest() (*http.Request, error) {
	return nil, nil
}

type TestPayload struct {
	Value string `json:"test"`
}
