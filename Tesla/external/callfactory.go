package external

import (
	"code.justin.tv/commerce/Tesla/config"
)

// CallFactory describes an interface that generates appriopriate Call objects
// based on passed in request objects. This can be used to customize calling
// mechanisms in different clients.
type CallFactory interface {
	CreateCall(Request) Call
}

// DefaultCallFactory is a basic implementation of the CallFactory interface.
// It simply returns the required call based on the call method specified in
// the request.
type DefaultCallFactory struct{}

// APIContext represents the API context for metrics
// i.e. part of metrics that comes from the API that a customer called
type APIContext struct {
	Configuration *config.TeslaConfiguration
	MethodName    string
	ClientID      string
}

// MetricNames represents the dependency-specific names to use for metrics
// i.e. what are latency/ failure metrics called for a given service?
type MetricNames struct {
	LatencyName string
	FailureName string
}

// MetricContext represents the parameters required to emit call-related metrics
type MetricContext struct {
	APIContext       APIContext
	ClientMetricName string
	MetricNames      MetricNames
}

// MetricsCallFactory is similar to the basic implementation of the CallFactory interface.
// However, it maintains a metrics object to report relevant metrics when making requests/ calls.
type MetricsCallFactory struct {
	MetricContext MetricContext
}

// CreateCall is the defaut call factory's implementation of CreateCall. Simply returns the
// appropriate call based on the desired call metohd.
func (cf DefaultCallFactory) CreateCall(r Request) Call {
	switch r.GetCallMethod() {
	case "POST":
		return BaseCall{}
	case "GET":
		return BaseCall{}
	default:
		return EmptyCall{}
	}
}

// CreateCall is the metric call factory's implementation of CreateCall. Simply returns the
// appropriate call based on the desired call metohd.
func (cf MetricsCallFactory) CreateCall(r Request) Call {
	switch r.GetCallMethod() {
	case "POST":
		return BaseCallWithMetrics{BaseCall: BaseCall{}, MetricContext: cf.MetricContext}
	case "GET":
		return BaseCallWithMetrics{BaseCall: BaseCall{}, MetricContext: cf.MetricContext}
	default:
		return EmptyCall{}
	}
}
