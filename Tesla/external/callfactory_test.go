package external

import (
	"reflect"
	"testing"

	"code.justin.tv/commerce/Tesla/config"
)

func TestDefaultCallFactoryPOSTCallCreation(t *testing.T) {
	factory := DefaultCallFactory{}
	request := EmptyRequest{}
	request.callMethod = "POST"
	call := factory.CreateCall(request)
	callType := reflect.TypeOf(call)
	expectedCallType := "BaseCall"

	if callType.Name() != expectedCallType {
		t.Error("POST Factory create call of type " + callType.Name() + " when expected " + expectedCallType)
	}
}

func TestDefaultCallFactoryGETCallCreation(t *testing.T) {
	factory := DefaultCallFactory{}
	request := EmptyRequest{}
	request.callMethod = "GET"
	call := factory.CreateCall(request)
	callType := reflect.TypeOf(call)
	expectedCallType := "BaseCall"

	if callType.Name() != expectedCallType {
		t.Error("GET Factory create call of type " + callType.Name() + " when expected " + expectedCallType)
	}
}

func TestDefaultCallFactoryCallCreationEmptyCall(t *testing.T) {
	factory := DefaultCallFactory{}
	emptyRequest := EmptyRequest{}
	emptyRequest.callMethod = "BLA"
	emptyCall := factory.CreateCall(emptyRequest)
	emptyCallType := reflect.TypeOf(emptyCall)
	expectedEmptyCallType := "EmptyCall"

	if emptyCallType.Name() != expectedEmptyCallType {
		t.Error("Factory create call of type " + emptyCallType.Name() + " when expected " + expectedEmptyCallType)
	}
}

func TestMetricsCallFactoryPOSTCallCreation(t *testing.T) {
	configuration := config.GetTestConfiguration()
	methodName := "someMethod"
	clientString := "someClient"
	latencyMetricName := "DependencyCallLatency:someDependency"
	failureMetricName := "DependencyCallFailure:someDependency"
	apiContext := APIContext{Configuration: configuration, MethodName: methodName, ClientID: clientString}
	metricNames := MetricNames{LatencyName: latencyMetricName, FailureName: failureMetricName}
	metricContext := MetricContext{APIContext: apiContext, MetricNames: metricNames}
	factory := MetricsCallFactory{MetricContext: metricContext}
	request := EmptyRequest{}
	request.callMethod = "POST"
	call := factory.CreateCall(request)
	callType := reflect.TypeOf(call)
	expectedCallType := "BaseCallWithMetrics"

	if callType.Name() != expectedCallType {
		t.Error("POST Factory create call of type " + callType.Name() + " when expected " + expectedCallType)
	}
}

func TestMetricsCallFactoryGETCallCreation(t *testing.T) {
	configuration := config.GetTestConfiguration()
	methodName := "someMethod"
	clientString := "someClient"
	latencyMetricName := "DependencyCallLatency:someDependency"
	failureMetricName := "DependencyCallFailure:someDependency"
	apiContext := APIContext{Configuration: configuration, MethodName: methodName, ClientID: clientString}
	metricNames := MetricNames{LatencyName: latencyMetricName, FailureName: failureMetricName}
	metricContext := MetricContext{APIContext: apiContext, MetricNames: metricNames}
	factory := MetricsCallFactory{MetricContext: metricContext}
	request := EmptyRequest{}
	request.callMethod = "GET"
	call := factory.CreateCall(request)
	callType := reflect.TypeOf(call)
	expectedCallType := "BaseCallWithMetrics"

	if callType.Name() != expectedCallType {
		t.Error("GET Factory create call of type " + callType.Name() + " when expected " + expectedCallType)
	}
}

func TestMetricsCallFactoryCallCreationEmptyCall(t *testing.T) {
	configuration := config.GetTestConfiguration()
	methodName := "someMethod"
	clientString := "someClient"
	latencyMetricName := "DependencyCallLatency:someDependency"
	failureMetricName := "DependencyCallFailure:someDependency"
	apiContext := APIContext{Configuration: configuration, MethodName: methodName, ClientID: clientString}
	metricNames := MetricNames{LatencyName: latencyMetricName, FailureName: failureMetricName}
	metricContext := MetricContext{APIContext: apiContext, MetricNames: metricNames}
	factory := MetricsCallFactory{MetricContext: metricContext}
	emptyRequest := EmptyRequest{}
	emptyRequest.callMethod = "BLA"
	emptyCall := factory.CreateCall(emptyRequest)
	emptyCallType := reflect.TypeOf(emptyCall)
	expectedEmptyCallType := "EmptyCall"

	if emptyCallType.Name() != expectedEmptyCallType {
		t.Error("Factory create call of type " + emptyCallType.Name() + " when expected " + expectedEmptyCallType)
	}
}
