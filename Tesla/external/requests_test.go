package external

import (
	"encoding/json"
	"io/ioutil"
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/signer/v4"
)

func VerifyPayload(t *testing.T, b []byte, expectedValue string, errorPrefix string) {
	var payload TestPayload
	err := json.Unmarshal(b, &payload)

	if err != nil {
		t.Error(errorPrefix + " Error during deserialization : " + err.Error())
	}

	if payload.Value != expectedValue {
		t.Error(errorPrefix + " Serialized value does not match original input value")
	}
}

func VerifyExistsInMap(t *testing.T, m map[string]string, key string, expectedValue string, errorPrefix string) {
	if value, valuePresent := m[key]; !valuePresent {
		t.Error(errorPrefix + " Key : " + key + " is absent from the map.")
	} else {
		if value != expectedValue {
			t.Error(errorPrefix + " Key : " + key + " returned an expected value. Got : " + value + " Should be : " + expectedValue)
		}
	}
}

func TestSigV4Request(t *testing.T) {
	// Taken from the AWS SDK unit tests. I derived the test from the "TestSignRequest", adjusting the request until I got the same signature
	// as that which was in the test here : https://github.com/aws/aws-sdk-go/blob/master/aws/signer/v4/v4_test.go#L169.
	// Once that was done, I reverted back to my own handling (which more or less handles the same cases, just differently), and updated the
	// expected signature until I got the proper test.
	signer := v4.Signer{
		Credentials: credentials.NewStaticCredentials("AKID", "SECRET", "SESSION"),
	}
	endpoint := "https://dynamodb.us-east-1.amazonaws.com"
	value := "something clever"
	payload := TestPayload{Value: value}
	method := "POST"
	var headers map[string]string
	headers = make(map[string]string)
	headers["X-Amz-Target"] = "prefix.Operation"
	headers["Content-Type"] = "application/x-amz-json-1.0"
	headers["X-Amz-Meta-Other-Header"] = "some-value=!@#$%^&* (+)"
	headers["X-Amz-Meta-Other-Header_With_Underscore"] = "some-value=!@#$%^&* (+)"

	sigV4Request := NewSigV4SerializableRequestWithTimestamp(method, endpoint, headers, payload, &signer, "dynamodb", "us-east-1", time.Unix(0, 0))

	expectedDate := "19700101T000000Z"
	expectedSig := "AWS4-HMAC-SHA256 Credential=AKID/19700101/us-east-1/dynamodb/aws4_request," +
		" SignedHeaders=content-encoding;content-type;host;x-amz-date;x-amz-meta-other-header;x-amz-meta-other-header_with_underscore;x-amz-security-token;x-amz-target," +
		" Signature=ab540a496e486b07e5f880b4890e3a2f07375e0530c23cd56d3936f4ee129990"

	httpRequest, err := sigV4Request.ToHTTPRequest()
	if err != nil {
		t.Error("TestSigV4Request:: Error while fetcing request : " + err.Error())
	}

	q := httpRequest.Header
	if e, a := expectedSig, q.Get("Authorization"); e != a {
		t.Errorf("TestSigV4Request:: Expected Authorization header\n%v\nGot :\n%v\n", e, a)
	}
	if e, a := expectedDate, q.Get("X-Amz-Date"); e != a {
		t.Errorf("TestSigV4Request:: Expected Date header : %v | Got : %v", e, a)
	}
}

func TestSerializableRequest(t *testing.T) {
	value := "something clever"
	payload := TestPayload{Value: value}
	headerKey := "key to the city"
	headerValue := "Narnia"

	var headers map[string]string
	headers = make(map[string]string)
	headers[headerKey] = headerValue
	request := serializableRequest{input: payload}
	request.callMethod = "METHOD IS MADNESS"
	request.callURL = "U R LOVED"
	request.headers = headers

	// First we check that the payload is serialized
	reader, err := request.GetPayload()
	if err != nil {
		t.Error("TestSerializableRequest:: Error while fetcing payload from request : " + err.Error())
	}

	var b []byte
	b, err = ioutil.ReadAll(reader)
	if err != nil {
		t.Error("TestSerializableRequest:: Error while reading serialize io.Reader : " + err.Error())
	}

	VerifyPayload(t, b, value, "TestSerializableRequest::")

	// Next we check that our headers is still in the header object
	VerifyExistsInMap(t, request.GetHeaders(), headerKey, headerValue, "TestSerializableRequest::")
}

func TestBaseRequest(t *testing.T) {
	callMethod := "TESTMETHOD"
	callURL := "www.world.wide.wed.com"

	request := BaseRequest{callMethod: callMethod, callURL: callURL}

	if request.GetCallURL() != callURL {
		t.Error("TestBaseRequest:: Unexpected callURL.")
	}

	if request.GetCallMethod() != callMethod {
		t.Error("TestBaseRequest:: Unexpected callMethod.")
	}

	if request.GetHeaders() == nil {
		t.Error("TestBaseRequest:: Header map not created.")
	}

	VerifyExistsInMap(t, request.GetHeaders(), "Content-Encoding", "amz-1.0", "TestBaseRequest::")
	VerifyExistsInMap(t, request.GetHeaders(), "User-Agent", "TwitchEntitlementService", "TestBaseRequest::")
}

func TestSerializeToReaderError(t *testing.T) {
	reader, err := serializeToReader(make(chan int))

	if reader != nil {
		t.Error("TestSerializeToReaderError:: Serialized object returned : ", reader)
	}

	if err == nil {
		t.Error("TestSerializeToReaderError:: No error returned")
	}
}

func TestSerializeToReader(t *testing.T) {
	testPayloadValue := "test"
	input := TestPayload{Value: testPayloadValue}
	reader, err := serializeToReader(input)
	if err != nil {
		t.Error("TestSerializeToReader:: Serialization failed.")
	}

	b, err := ioutil.ReadAll(reader)
	if err != nil {
		t.Error("TestSerializeToReader:: Error while reading serialize io.Reader : " + err.Error())
	}

	VerifyPayload(t, b, input.Value, "TestSerializeToReader::")
}

func TestSerializeError(t *testing.T) {
	b, err := serialize(make(chan int))

	if b != nil {
		t.Error("TestSerializeError:: Serialized object returned : " + string(b))
	}

	if err == nil {
		t.Error("TestSerializeError:: No error returned")
	}
}

func TestSerialize(t *testing.T) {
	testPayloadValue := "test"
	input := TestPayload{Value: testPayloadValue}
	b, err := serialize(input)
	if err != nil {
		t.Error("TestSerialize:: Serialization failed.")
	}

	VerifyPayload(t, b, input.Value, "TestSerialize::")
}
