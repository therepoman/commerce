package external

// Response is the abstract response interface.
// It is meant to represent raw output returned from an external call.
// This response is then to be parsed in the service client to generate output.
type Response interface {
	GetStatus() string
	GetStatusCode() int
	GetBody() []byte
}

// BaseResponse is a default implementation of the external.Response interface.
type baseResponse struct {
	status     string
	statusCode int
	body       []byte
}

// GetStatus returns the status the external service responded with.
func (r baseResponse) GetStatus() string {
	return r.status
}

// GetStatusCode returns the status code the external service responded with.
func (r baseResponse) GetStatusCode() int {
	return r.statusCode
}

func (r baseResponse) GetBody() []byte {
	return r.body
}

func NewBaseResponse (status string, code int, body []byte) baseResponse {
	return baseResponse{
		status:     status,
		statusCode: code,
		body:       body,
	}
}
