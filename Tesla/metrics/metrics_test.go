package metrics

import (
	"testing"
)

type statusTypePair struct {
	status             int
	expectedMetricType string
}

var statusToTypeTests = []statusTypePair{
	{200, MetricTypeSuccesses},
	{250, MetricTypeSuccesses},
	{399, MetricTypeClientError},
	{400, MetricTypeClientError},
	{401, MetricTypeClientError},
	{500, MetricTypeServerError},
	{501, MetricTypeServerError},
	{550, MetricTypeServerError},
}

func TestGetMetricTypeFromStatus(t *testing.T) {
	for _, statusTypePair := range statusToTypeTests {
		actualMetricType := statusToMetricType(statusTypePair.status)

		if actualMetricType != statusTypePair.expectedMetricType {
			t.Error("For", statusTypePair.status, "expected", statusTypePair.expectedMetricType, "got", actualMetricType)
		}
	}
}
