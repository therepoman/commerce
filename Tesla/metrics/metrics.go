package metrics

import (
	"time"

	"code.justin.tv/commerce/AmazonMWSGoClient/mws"

	"code.justin.tv/commerce/Tesla/config"
)

const (
	hostSpecific  = false
	serviceName   = "TESla"
	environment   = "TESla/NA"
	hostname      = "AWS-ELB"
	partitionID   = ""
	customerID    = ""
	metricsRegion = "PDX" // PDX is us-west-2.

	minPeriod = mws.PeriodFiveMinute
	maxPeriod = mws.PeriodFiveMinute

	metricTypeTime = "Time"

	// Exported metrics types that can be used for counts.

	// MetricTypeSuccesses is the number of successes for a provided API.
	MetricTypeSuccesses = "Success"
	// MetricTypeServerError the number of server errors for a provided API.
	MetricTypeServerError = "Server_Error"
	// MetricTypeClientError the number of client errors for a provided API.
	MetricTypeClientError = "Client_Error"
	// MetricInvalidReceiptsType the number of invalid receipts (with NOOP as next instruction) passed to the /fulfill endpoint.
	MetricInvalidReceiptsType = "Invalid_Receipts"
)

func statusToMetricType(status int) string {
	var metricType = ""
	if status < 300 {
		// Anything below 300 is a success.
		metricType = MetricTypeSuccesses
	} else if status < 500 {
		// Anything between 300 and 500 is a client error of some sort.
		metricType = MetricTypeClientError
	} else {
		// Anything above 500 is a server error.
		metricType = MetricTypeServerError
	}

	return metricType
}

// PostLatencyMetricWithMetricName wll post the latency for a given method and client to PMET, using a different name than Time
func PostLatencyMetricWithMetricName(configuration *config.TeslaConfiguration, methodName string, clientString string, latency time.Duration, metricName string) {
	postDecimalMetricWithMetricNameAndUnit(configuration, methodName, clientString, latency.Seconds()*1000.0, metricName, mws.UnitMilliseconds)
}

// PostDecimalMetricWithMetricName will post a decimal metric for a given method and client to PMET, using the given metric name
func PostDecimalMetricWithMetricName(configuration *config.TeslaConfiguration, methodName string, clientString string, decimal float64, metricName string) {
	postDecimalMetricWithMetricNameAndUnit(configuration, methodName, clientString, decimal, metricName, "Count")
}

func postDecimalMetricWithMetricNameAndUnit(configuration *config.TeslaConfiguration, methodName string, clientString string, decimal float64, metricName string, unit string) {
	// Initialize the metric object.
	decimalMetric := mws.NewMetric(configuration.IsProd(), hostname, hostSpecific, configuration.GetAwsRegion(), serviceName, methodName, clientString, metricName)

	// Add the actual decimal value to the metric.
	decimalMetric.AddValue(decimal)
	decimalMetric.Unit = unit
	decimalMetric.Timestamp = time.Now().UTC()

	// Post it !
	go postMetric(configuration, decimalMetric)
}

// PostLatencyMetric wll post the latency for a given method and client to PMET with a metric name of Time.
func PostLatencyMetric(configuration *config.TeslaConfiguration, methodName string, clientString string, latency time.Duration) {
	PostLatencyMetricWithMetricName(configuration, methodName, clientString, latency, metricTypeTime)
}

// PostStatusMetric will post the appropriate metric for a given method and client to PMET given the provided return status.
func PostStatusMetric(configuration *config.TeslaConfiguration, methodName string, clientString string, status int) {
	PostCountMetric(configuration, methodName, clientString, 1.0, statusToMetricType(status))
}

// PostCountMetric posts a counter metric to PMET, the type is provided as a parameter.
func PostCountMetric(configuration *config.TeslaConfiguration, methodName string, clientString string, value float64, metricType string) {
	// Initialize the metric
	countMetric := mws.NewMetric(configuration.IsProd(), hostname, hostSpecific, configuration.GetAwsRegion(), serviceName, methodName, clientString, metricType)

	// Add the success value.
	countMetric.AddValue(value)
	countMetric.Unit = "Count"
	countMetric.Timestamp = time.Now().UTC()

	// Post it !
	go postMetric(configuration, countMetric)
}

func postMetric(configuration *config.TeslaConfiguration, metric mws.Metric) {
	if configuration.IsLocal() || configuration.IsTest() {
		// No metrics locally or for tests.
		return
	}

	// Initialize MetricReport and add the Metric.
	metricReport := mws.NewMetricReport(configuration.IsProd(), hostname, minPeriod, maxPeriod, partitionID, environment, customerID)
	metricReport.AddMetric(metric)

	// Wrap the MetricReport in a request object.
	req := mws.NewPutMetricsForAggregationRequest()
	req.AddMetricReport(metricReport)

	_, metricsErr := mws.PutMetricsForAggregation(req, mws.Regions[metricsRegion], configuration.GetAwsMetricsCredentials())
	if metricsErr != nil {
		return // Do nothing for now...but the metrics publish has failed.
	}
}
