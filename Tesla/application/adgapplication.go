package application

import (
	tesla "code.justin.tv/commerce/Tesla/client"
	"code.justin.tv/commerce/Tesla/external"
	"code.justin.tv/commerce/Tesla/externaladg"
	"code.justin.tv/commerce/Tesla/externalaws"
	"strings"

	log "github.com/Sirupsen/logrus"
)

type ADGApplication struct {
	RequestID        string
	Tuid             string
	ApiContext       external.APIContext
	ClientMetricName string
	ADGOService      externaladg.ADGOrderServiceInterface
	ADGEService      externaladg.ADGEntitlementServiceInterface
	RevokeService    externalaws.RevokeServiceInterface
}

func (adg ADGApplication) GetAccountEntitlements() ([]tesla.Good, external.Error) {
	adgesOutput, adgesErr := adg.ADGEService.GetGoodsForUserSigV4(externaladg.GetGoodsForUserSigV4Input{RequestID: adg.RequestID, Tuid: adg.Tuid, ClientID: adg.ApiContext.ClientID})
	if adgesErr != nil {
		return nil, adgesErr
	}

	// Grab all revoked goods for this user
	dynamoRevokeGoods, revokeErr := adg.RevokeService.GetRevokesByUser(adg.Tuid)
	if revokeErr != nil {
		return nil, revokeErr
	}
	dynamoRevokeMap := make(map[string]bool)
	for _, dynamoRevokeGood := range dynamoRevokeGoods {
		dynamoRevokeMap[dynamoRevokeGood.ReceiptID] = dynamoRevokeGood.Completed
	}

	var resultGoods []tesla.Good
	for _, good := range adgesOutput.Goods {
		completed, isInRevokeDynamo := dynamoRevokeMap[good.ReceiptID]
		if isInRevokeDynamo && completed {
			// If any of the adgesOutput has goods that have already been revoked, set it as noop
			good.NextInstruction = externaladg.Noop
		} else if !isInRevokeDynamo && good.NextInstruction == externaladg.Revoke {
			// If state is in revoke but not already in dynamodb, put in dynamodb
			_, err := adg.RevokeService.CreateRevoke(adg.Tuid, good.ReceiptID, good.SKU)
			if err != nil {
				return nil, err
			}
		}
		resultGoods = append(resultGoods, convertModelToGood(good))
	}
	return resultGoods, nil
}

func (adg ADGApplication) UpdateFulfillmentStatus(request tesla.SetStatusForReceiptsRequest) ([]tesla.ReceiptUpdateResult, external.Error, int) {
	var receiptUpdate externaladg.ReceiptUpdate
	var receiptToInstructions = request.ReceiptUpdates
	var receiptUpdateResults = make([]tesla.ReceiptUpdateResult, 0)

	receiptUpdates := make([]externaladg.ReceiptUpdate, 0)
	revokedReceipts := make([]string, 0)
	receiptFulfillMap := make(map[string]string)
	for _, receiptRequestUpdate := range receiptToInstructions {
		if receiptRequestUpdate.ReceiptID == "" || receiptRequestUpdate.FulfillmentAddress == "" || receiptRequestUpdate.LastInstruction == "" {
			return nil, external.NewBadRequestError("Invalid request: Missing json parameters"), 0
		}
		lastInstruction := strings.ToUpper(receiptRequestUpdate.LastInstruction)
		if lastInstruction == "FULFILL" {
			receiptUpdate = externaladg.ReceiptUpdate{}
			receiptUpdate.ReceiptID = receiptRequestUpdate.ReceiptID
			receiptFulfillMap[receiptUpdate.ReceiptID] = receiptRequestUpdate.FulfillmentAddress
			receiptUpdate.Status = "FULFILLED"
			receiptUpdates = append(receiptUpdates, receiptUpdate)
		} else if lastInstruction == "REVOKE" {
			revokedReceipts = append(revokedReceipts, receiptRequestUpdate.ReceiptID)
		}
	}

	noOpReceiptCount := len(receiptToInstructions) - len(receiptUpdates) - len(revokedReceipts)

	if len(receiptUpdates) == 0 && len(revokedReceipts) == 0 {
		return nil, external.NewBadRequestError("Invalid request: No Incoming Receipts were in a valid FULFILL/REVOKE state"), 0
	}

	// Call on ADG to fulfill receipts
	if len(receiptUpdates) > 0 {
		setFulfillmentAddress(adg.ApiContext, adg.ClientMetricName, adg.Tuid, receiptFulfillMap)
		adgosInput := externaladg.SetStatusForReceiptsSigV4Input{RequestID: adg.RequestID, Tuid: adg.Tuid, ClientID: adg.ApiContext.ClientID, ReceiptUpdates: receiptUpdates}
		adgosOutput, adgosErr := adg.ADGOService.SetStatusForReceiptsSigV4(adgosInput)

		if adgosErr != nil {
			return nil, adgosErr, 0
		}

		receiptUpdateResults = append(receiptUpdateResults, convertSetReceiptsOutputToResponse(adgosOutput)...)
	}

	// Add revokes that were added to dynamodb to receipts response
	if len(revokedReceipts) > 0 {
		revokedResults := make([]tesla.ReceiptUpdateResult, 0)
		for _, receiptID := range revokedReceipts {
			_, err := adg.RevokeService.CompleteRevokeByReceipt(adg.Tuid, receiptID)

			if err != nil {
				log.Error("Error updating revoke for " + adg.Tuid + " :: " + receiptUpdate.ReceiptID)
				return nil, err, 0
			}
			currReceiptUpdate := tesla.ReceiptUpdateResult{ReceiptID: receiptID, Result: "Success"}
			revokedResults = append(revokedResults, currReceiptUpdate)
		}
		receiptUpdateResults = append(receiptUpdateResults, revokedResults...)
	}
	return receiptUpdateResults, nil, noOpReceiptCount
}

func convertModelToGood(model externaladg.GetGoodsForUserOutputModel) tesla.Good {
	return tesla.Good{ReceiptID: model.ReceiptID, SKU: model.SKU, NextInstruction: model.NextInstruction.String()}
}

func setFulfillmentAddress(apiContext external.APIContext, clientMetricName string, tuID string, receiptFulfillMap map[string]string) {
	service := externalaws.NewDynamoFulfillmentAddressService(apiContext, clientMetricName)

	for receiptID, fulfillmentAddress := range receiptFulfillMap {
		input := externalaws.UpdateFulfillmentAddressInput{Tuid: tuID, ClientID: apiContext.ClientID, FulfillmentAddress: &fulfillmentAddress, ReceiptID: &receiptID}
		_, err := service.UpdateFulfillmentAddress(input)
		if err != nil {
			// For now we just warned that a failure occured. Since this is solely for auditing, we do not want to fail the whole call if this fails.
			// The Lambda should be capable of recovering from failures on its own.
			log.Warnf("Error updating fulfillment address for tuid=%v, clientId=%v, fulfillmentAddress=%v, receiptId=%v", tuID, apiContext.ClientID, fulfillmentAddress, receiptID)
		}
	}
}

// Convert the ADG output to the response we want to return to the 3p Vendor
func convertSetReceiptsOutputToResponse(output externaladg.SetStatusForReceiptsOutput) []tesla.ReceiptUpdateResult {
	receiptUpdateResults := make([]tesla.ReceiptUpdateResult, 0)
	for _, receiptStatusUpdate := range output.ReceiptStatusUpdates {
		receiptUpdateResult := tesla.ReceiptUpdateResult{}
		receiptUpdateResult.ReceiptID = receiptStatusUpdate.ReceiptID
		receiptUpdateResult.Result = receiptStatusUpdate.Result
		receiptUpdateResults = append(receiptUpdateResults, receiptUpdateResult)
	}
	return receiptUpdateResults
}
