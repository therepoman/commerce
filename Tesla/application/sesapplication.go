package application

import (
	"code.justin.tv/commerce/Tesla/client"
	"code.justin.tv/commerce/Tesla/config"
	"code.justin.tv/commerce/Tesla/external"
	"code.justin.tv/commerce/Tesla/externaladg"
	"code.justin.tv/commerce/Tesla/externalaws"
	"code.justin.tv/commerce/Tesla/externalses"
	"encoding/json"
	"regexp"
	"strings"

	log "github.com/Sirupsen/logrus"
)

const adgGoods = "ADGGoods"
const productInstances = "ProductInstances"
const twitch = "TWITCH"

type SESApplication struct {
	RequestID                           string
	Tuid                                string
	ApiContext                          external.APIContext
	ClientMetricName                    string
	SesGetAccountEntitlementInterface   externalses.SESGetAccountEntitlementsInterface
	SesUpdateFulfillmentStatusInterface externalses.SESUpdateFulfillmentStatusInterface
	ClientProductMappingInterface       externalaws.ClientProductMappingInterface
}

type goodEntry struct {
	Account         account `json:"account"`
	Vendor          string  `json:"vendor"`
	Sku             string  `json:"sku"`
	GoodId          string  `json:"goodId"`
	VendorSkuString string  `json:"vendorSkuString"`
}

type productEntry struct {
	Account               account `json:"account"`
	ProductId             string  `json:"productId"`
	ProductInstanceId     string  `json:"productInstanceId"`
	ProductInstanceIdType string  `json:"productInstanceIdType"`
}

type account struct {
	AccountType string `json:"accountType"`
	AccountId   string `json:"accountId"`
}

func (ses SESApplication) GetAccountEntitlements() ([]tesla.Good, external.Error) {
	ses.ApiContext.MethodName = string(config.GetAccountEntitlements)
	productIdMapping, mappingErr := ses.getProductIdMapping()

	// TODO if we wanted to have more complicated logic on what to do when mapping returns issue here would be the place to put it
	if mappingErr != nil {
		return nil, mappingErr
	} else if productIdMapping.ProductIds == nil {
		return nil, external.NewBadRequestError("There are no productId's associated with this clientId.")
	} else {
		sesOutput, sesErr := ses.SesGetAccountEntitlementInterface.GetAccountEntitlementsSigV4(externalses.GetAccountEntitlementsSigV4Input{RequestID: ses.RequestID, Tuid: ses.Tuid, ClientID: ses.ApiContext.ClientID, ProductIds: productIdMapping.ProductIds})
		if sesErr != nil {
			log.Warnf("There was an error in the GetAccountsEntitlements Call: " + sesErr.Error())
			return nil, sesErr
		}

		// Return list of entitlements
		return getGoodsFromEntitlements(sesOutput.Entitlements), sesErr
	}
}

func (ses SESApplication) UpdateFulfillmentStatus(request tesla.SetStatusForReceiptsRequest) ([]tesla.ReceiptUpdateResult, external.Error, int) {
	var fulfillmentStatusObjects = make([]externalses.FulfillmentStatusObject, 0)
	var metadataFilters = make([]externalses.MetadataFilter, 0)
	var receiptIdToStatus = make(map[string]string)

	ses.ApiContext.MethodName = string(config.UpdateFulfillmentStatus)
	receiptsToUpdate, noOpCount := getReceiptsToUpdate(request.ReceiptUpdates)

	for _, receiptUpdate := range receiptsToUpdate {
		// If the receiptId is valid we are looking at a adg receipt id, if not we are dealing with a sesGetAccountEntitlements
		// entitlementId. The naming of receipt id is a legacy byproduct that we are maintaining as it would
		// be too costly to change.
		if isADGId(receiptUpdate.ReceiptID) {
			metadataFilters = append(metadataFilters, getFiltersForReceiptId(receiptUpdate.ReceiptID)...)
			receiptIdToStatus[receiptUpdate.ReceiptID] = receiptUpdate.LastInstruction
		} else {
			// Check and translate 3P lastop
			fulfillmentStatusObjects = append(fulfillmentStatusObjects, createFulfillmentStatusObject(receiptUpdate.ReceiptID, receiptUpdate.LastInstruction))
		}
	}

	fulfillmentStatusObjectsFromEntitlements, sesGetAccountEntitlementsErr := ses.getFulfillmentStatusObjectsFromMetadataFilters(receiptIdToStatus, metadataFilters)

	if sesGetAccountEntitlementsErr != nil {
		log.Warnf("There was an error in the GetAccountsEntitlements Call: " + sesGetAccountEntitlementsErr.Error())
		return nil, sesGetAccountEntitlementsErr, noOpCount
	}

	fulfillmentStatusObjects = append(fulfillmentStatusObjects, fulfillmentStatusObjectsFromEntitlements...)

	if len(fulfillmentStatusObjects) == 0 {
		log.Warnf("There were %d fulfillmentStatusObject/s that was not a FULFILL/REVOKE.", noOpCount)
		log.Warnf("Invalid request: No Incoming Receipts were in a valid FULFILL/REVOKE state")
		return getReceiptsFromEntitlements(make([]externalses.Entitlement, 0)), nil, noOpCount
	} else {
		sesUpdateFulfillmentStatusOutput, sesUpdateFulfillmentStatusErr := ses.SesUpdateFulfillmentStatusInterface.UpdateFulfillmentStatusSigV4(ses.ApiContext.ClientID, ses.RequestID, fulfillmentStatusObjects)

		if sesUpdateFulfillmentStatusErr != nil {
			log.Warnf("There was an error in the UpdateFulfillmentStatusSigV4 Call: " + sesUpdateFulfillmentStatusErr.Error())
			return nil, sesUpdateFulfillmentStatusErr, noOpCount
		}

		return getReceiptsFromEntitlements(sesUpdateFulfillmentStatusOutput.Entitlements), sesUpdateFulfillmentStatusErr, noOpCount
	}
}

func (ses SESApplication) getProductIdMapping() (externalaws.GetClientProductMappingOutput, external.Error) {
	return ses.ClientProductMappingInterface.GetClientProductMapping(externalaws.GetClientProductMappingInput{ClientID: ses.ApiContext.ClientID})
}

func (ses SESApplication) getFulfillmentStatusObjectsFromMetadataFilters(receiptIdToStatus map[string]string, metadataFilters []externalses.MetadataFilter) ([]externalses.FulfillmentStatusObject, external.Error) {
	var fulfillmentStatusObjectsToReturn = make([]externalses.FulfillmentStatusObject, 0)
	var sesGetAccountEntitlementsErr external.Error

	// If metadataFilters is not empty, we have SES entitlements we need to retrieve from SES
	// We should now make the call
	if len(metadataFilters) > 0 {
		// We have a full list of metadatafilters that we can then call into SES to get the list of entitlements
		sesGetAccountEntitlementsOutput, sesGetAccountEntitlementsErr := ses.SesGetAccountEntitlementInterface.GetAccountEntitlementsSigV4(externalses.GetAccountEntitlementsSigV4Input{RequestID: ses.RequestID, Tuid: ses.Tuid, ClientID: ses.ApiContext.ClientID, MetaDataFilters: metadataFilters})
		if sesGetAccountEntitlementsErr != nil {
			// TODO update this so that it throws actual errors when we deprecate adg
			log.Warnf("There was an error in the GetAccountsEntitlements Call: " + sesGetAccountEntitlementsErr.Error())
			return nil, sesGetAccountEntitlementsErr
		}

		for key := range receiptIdToStatus {
			for _, entitlement := range sesGetAccountEntitlementsOutput.Entitlements {
				if strings.Contains(entitlement.Metadata[adgGoods], key) || strings.Contains(entitlement.Metadata[adgGoods], key) {
					// Check and translate 3P lastOp
					fulfillmentStatusObjectsToReturn = append(fulfillmentStatusObjectsToReturn, createFulfillmentStatusObject(entitlement.EntitlementId, receiptIdToStatus[key]))
				}
			}
		}
	}

	return fulfillmentStatusObjectsToReturn, sesGetAccountEntitlementsErr
}

func getReceiptsToUpdate(receipts []tesla.ReceiptToInstruction) ([]tesla.ReceiptToInstruction, int) {
	var receiptsToReturn = make([]tesla.ReceiptToInstruction, 0)
	var noOpCount = 0

	for _, receipt := range receipts {
		if translate3PLastOp(strings.ToUpper(receipt.LastInstruction)) != "" {
			receiptsToReturn = append(receiptsToReturn, receipt)
		} else {
			noOpCount++
		}
	}

	return receiptsToReturn, noOpCount
}

func createFulfillmentStatusObject(entitlementId string, fulfillmentStatus string) externalses.FulfillmentStatusObject {
	var newFulfillmentStatusObject externalses.FulfillmentStatusObject
	newFulfillmentStatusObject.EntitlementId = entitlementId
	// Doing best effort to sanitize the last instructions from 3P
	newFulfillmentStatusObject.FulfillmentStatus = translate3PLastOp(strings.ToUpper(fulfillmentStatus))
	return newFulfillmentStatusObject
}

// This is used to determine whether we are dealing with a receipt/goods uuid or a enitlementid
// We know that entitlementid is not an uuid (it is two linked together) and that receipt/goods is
func isADGId(toTest string) bool {
	r := regexp.MustCompile("([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}")
	return r.MatchString(toTest)
}

// The receipt/goods id can exist in multiple locations inside an entitlement,
// therefore two filters need to be created to check both locations
func getFiltersForReceiptId(receiptId string) []externalses.MetadataFilter {
	const contains = "CONTAINS"

	var metadataFilters []externalses.MetadataFilter

	var adgGoodsFilterRule externalses.MetadataFilterRule
	adgGoodsFilterRule.MetadataFilterRuleType = contains
	adgGoodsFilterRule.MetadataFilterRuleValue = receiptId

	var adgGoodsFilter externalses.MetadataFilter
	adgGoodsFilter.MetadataKey = adgGoods
	adgGoodsFilter.MetadataFilterRule = adgGoodsFilterRule

	var productInstancesFilterRule externalses.MetadataFilterRule
	productInstancesFilterRule.MetadataFilterRuleType = contains
	productInstancesFilterRule.MetadataFilterRuleValue = receiptId

	var productInstancesFilter externalses.MetadataFilter
	productInstancesFilter.MetadataKey = productInstances
	productInstancesFilter.MetadataFilterRule = productInstancesFilterRule

	metadataFilters = append(metadataFilters, adgGoodsFilter)
	metadataFilters = append(metadataFilters, productInstancesFilter)

	return metadataFilters
}

// Translate 3P LastOp to fulfillmentStatus
func translate3PLastOp(lastOp string) string {
	if lastOp == string(externaladg.Fulfill) {
		return string(externalses.Fulfilled)
	} else if lastOp == string(externaladg.Revoke) {
		return string(externalses.Revoked)
	} else {
		return ""
	}
}

func getGoodsFromEntitlements(entitlements []externalses.Entitlement) []tesla.Good {
	var resultGoodsFromSES []tesla.Good

	// Creating the same response as we have been giving to 3P
	for _, entitlement := range entitlements {
		var adgGoodId = getAdgGoodIdIfExist(entitlement)
		var good tesla.Good

		if adgGoodId != "" {
			good.ReceiptID = adgGoodId
		} else {
			good.ReceiptID = entitlement.EntitlementId
		}

		good.NextInstruction = getNextInstructionFromFulfillmentStatus(entitlement.FulfillmentStatus)

		good.SKU = entitlement.Product.Metadata["VendorProductId"]

		resultGoodsFromSES = append(resultGoodsFromSES, good)
	}

	return resultGoodsFromSES
}

func getReceiptsFromEntitlements(entitlements []externalses.Entitlement) []tesla.ReceiptUpdateResult {
	receiptUpdateFromSES := make([]tesla.ReceiptUpdateResult, 0)

	// Here we create the exact same response that we have been giving to 3P
	for _, entitlement := range entitlements {
		var adgGoodId = getAdgGoodIdIfExist(entitlement)
		var receipt tesla.ReceiptUpdateResult

		if adgGoodId != "" {
			receipt.ReceiptID = adgGoodId
		} else {
			receipt.ReceiptID = entitlement.EntitlementId
		}

		receipt.Result = "Success"

		receiptUpdateFromSES = append(receiptUpdateFromSES, receipt)
	}

	return receiptUpdateFromSES
}

// Function for translating SES fulfillmentStatus to nextOp
func getNextInstructionFromFulfillmentStatus(fulfillmentStatus string) string {
	if fulfillmentStatus == string(externalses.PendingFulfillment) {
		return string(externaladg.Fulfill)
	} else if fulfillmentStatus == string(externalses.PendingRevoke) {
		return string(externaladg.Revoke)
	} else if fulfillmentStatus == string(externalses.Fulfilled) ||
		fulfillmentStatus == string(externalses.Revoked) {
		return string(externaladg.Noop)
	} else {
		return string(externaladg.Unknown)
	}
}

// There should only be one adgGood or one productInstance there should not be more in either field.
// We will start with the productInstance and default to the adgGood if it exist
// We will be logging an warning here should there be multiples as this should not exist
// We will always expect an account object
// TODO we should emit a metric in the future
func getAdgGoodIdIfExist(entitlement externalses.Entitlement) string {
	if entitlement.Metadata[productInstances] != "" {
		var productEntries []productEntry

		if json.Unmarshal([]byte(entitlement.Metadata[productInstances]), &productEntries) != nil {
			log.Error("There was an error in unmarshalling the product instances metadata.")
		}

		if len(productEntries) == 1 {
			for i := range productEntries {
				if productEntries[i].Account.AccountType == twitch {
					return productEntries[i].ProductInstanceId
				}
			}
		} else if len(productEntries) > 1 {
			log.Warnf("Entitlement with entitlementId: %s has multiple productInstance entries.", entitlement.EntitlementId)
		}
	}

	if entitlement.Metadata[adgGoods] != "" {
		var goodEntries []goodEntry

		if json.Unmarshal([]byte(entitlement.Metadata[adgGoods]), &goodEntries) != nil {
			log.Error("There was an error in unmarshalling the adgoods metadata.")
		}

		if len(goodEntries) == 1 {
			for i := range goodEntries {
				return goodEntries[i].GoodId
			}
		} else if len(goodEntries) > 1 {
			log.Warnf("Entitlement with entitlementId: %s has multiple adgGood entries.", entitlement.EntitlementId)
		}
	}

	return ""
}
