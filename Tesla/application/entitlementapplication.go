package application

import (
	"code.justin.tv/commerce/Tesla/client"
	"code.justin.tv/commerce/Tesla/external"
)

type EntitlementApplicationInterface interface {
	GetAccountEntitlements() ([]tesla.Good, external.Error)
	UpdateFulfillmentStatus(request tesla.SetStatusForReceiptsRequest) ([]tesla.ReceiptUpdateResult, external.Error, int)
}

type EntitlementApplication struct {
	Application EntitlementApplicationInterface
}

func (entitlementApplication EntitlementApplication) GetEntitlements() ([]tesla.Good, external.Error) {
	return entitlementApplication.Application.GetAccountEntitlements()
}

func (entitlementApplication EntitlementApplication) UpdateFulfillmentStatus(request tesla.SetStatusForReceiptsRequest) ([]tesla.ReceiptUpdateResult, external.Error, int) {
	return entitlementApplication.Application.UpdateFulfillmentStatus(request)
}
