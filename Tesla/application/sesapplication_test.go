package application

import (
	tesla "code.justin.tv/commerce/Tesla/client"
	"code.justin.tv/commerce/Tesla/external"
	"code.justin.tv/commerce/Tesla/externaladg"
	"code.justin.tv/commerce/Tesla/externalaws"
	"code.justin.tv/commerce/Tesla/externalses"
	"github.com/stretchr/testify/assert"
	"testing"
)

type mockSESGetGetAccountEntitlementInterface struct{}
type mockSESUpdateFulfillmentStatusInterface struct{}
type mockSESClientProductMappingInterface struct{}

func (mock mockSESGetGetAccountEntitlementInterface) GetAccountEntitlementsSigV4(input externalses.GetAccountEntitlementsSigV4Input) (externalses.GetAccountEntitlementsOutput, external.Error) {
	var entitlements = make([]externalses.Entitlement, 0)
	var destinations = make([]string, 0)

	metadata := make(map[string]string)
	metadataProduct := make(map[string]string)
	metadata[adgGoods] = "testGoodId"

	if len(input.ProductIds) == 0 {
		entitlements = append(entitlements, externalses.Entitlement{
			EntitlementId:            "testId1",
			EntitlementAccounts:      nil,
			RelatedAccounts:          nil,
			EntitlementStatus:        "",
			FulfillmentStatus:        "",
			CreatedDate:              "",
			LastUpdatedDate:          "",
			EntitlementStatusHistory: nil,
			FulfillmentStatusHistory: nil,
			Metadata:                 metadata,
			Domain:                   "",
			Product:                  externalses.EntitlementProduct{},
		})
	} else if input.ProductIds[0] == "testProduct" {
		metadata = map[string]string{"ADGGoods": "[{\"vendor\":\"PLWVE\",\"sku\":\"hover_game\",\"goodId\":\"34b763fe-a304-60c1-b236-b794e3941b5a\",\"account\":{\"accountType\":\"TWITCH\",\"accountId\":\"testId\"},\"vendorSkuString\":\"PLWVE/hover_game\"}]"}
		metadataProduct = map[string]string{"VendorProductId": "testsku"}

		entitlements = append(entitlements, externalses.Entitlement{
			EntitlementId:            "testId2",
			EntitlementAccounts:      nil,
			RelatedAccounts:          nil,
			EntitlementStatus:        "",
			FulfillmentStatus:        "",
			CreatedDate:              "",
			LastUpdatedDate:          "",
			EntitlementStatusHistory: nil,
			FulfillmentStatusHistory: nil,
			Metadata:                 metadata,
			Domain:                   "",
			Product:                  externalses.EntitlementProduct{
				ProductId:                "testProduct",
				ProductName:              "",
				Destinations:             destinations,
				Metadata:                 metadataProduct,
			},
		})
	} else {
		return externalses.GetAccountEntitlementsOutput{Entitlements: entitlements}, nil
	}

	return externalses.GetAccountEntitlementsOutput{Entitlements: entitlements}, nil
}

func (mock mockSESUpdateFulfillmentStatusInterface) UpdateFulfillmentStatusSigV4(clientId string, receiptId string, fulfillmentStatuses []externalses.FulfillmentStatusObject) (externalses.UpdateFulfillmentStatusOutput, external.Error) {
	entitlements := make([]externalses.Entitlement, 0)

	if clientId == "testClientId" && receiptId == "testRequestId" {
		if fulfillmentStatuses[0].EntitlementId == "testReceiptId" && fulfillmentStatuses[0].FulfillmentStatus == string(externalses.Fulfilled) {
			updatedEntitlement := externalses.Entitlement{
				EntitlementId:            "testId",
				EntitlementAccounts:      nil,
				RelatedAccounts:          nil,
				EntitlementStatus:        "",
				FulfillmentStatus:        "FULFILLED",
				CreatedDate:              "",
				LastUpdatedDate:          "",
				EntitlementStatusHistory: nil,
				FulfillmentStatusHistory: nil,
				Metadata:                 nil,
				Domain:                   "",
				Product:                  externalses.EntitlementProduct{},
			}
			entitlements = append(entitlements, updatedEntitlement)

			return externalses.UpdateFulfillmentStatusOutput{Entitlements: entitlements}, nil
		}
	} else if clientId == "testClientId" && receiptId == "testRequestId2" {
		metadata := map[string]string{"ADGGoods": "[{\"vendor\":\"PLWVE\",\"sku\":\"hover_game\",\"goodId\":\"34b763fe-a304-60c1-b236-b794e3941b5a\",\"account\":{\"accountType\":\"TWITCH\",\"accountId\":\"testId\"},\"vendorSkuString\":\"PLWVE/hover_game\"}]"}
		if fulfillmentStatuses[0].EntitlementId == "testReceiptId2" && fulfillmentStatuses[0].FulfillmentStatus == string(externalses.Fulfilled) {
			updatedEntitlement := externalses.Entitlement{
				EntitlementId:            "testId",
				EntitlementAccounts:      nil,
				RelatedAccounts:          nil,
				EntitlementStatus:        "",
				FulfillmentStatus:        "FULFILLED",
				CreatedDate:              "",
				LastUpdatedDate:          "",
				EntitlementStatusHistory: nil,
				FulfillmentStatusHistory: nil,
				Metadata:                 metadata,
				Domain:                   "",
				Product:                  externalses.EntitlementProduct{},
			}
			entitlements = append(entitlements, updatedEntitlement)

			return externalses.UpdateFulfillmentStatusOutput{Entitlements: entitlements}, nil
		}
	}

	return externalses.UpdateFulfillmentStatusOutput{Entitlements: entitlements}, nil
}

func (mock mockSESClientProductMappingInterface) GetClientProductMapping(input externalaws.GetClientProductMappingInput) (externalaws.GetClientProductMappingOutput, external.Error) {
	var productIds = make([]string, 0)

	if input.ClientID == "" {
		return externalaws.GetClientProductMappingOutput{ProductIds: productIds}, nil
	} else if input.ClientID == "testClientId" {
		productIds = append(productIds, "testProduct")
		return externalaws.GetClientProductMappingOutput{ProductIds: productIds}, nil
	} else {
		productIds = append(productIds, "testProduct2")
		return externalaws.GetClientProductMappingOutput{ProductIds: productIds}, nil
	}
}

func getTestSESApplication() SESApplication {
	return SESApplication{
		RequestID: "testRequestId",
		Tuid:      "testTuId",
		ApiContext: external.APIContext{
			Configuration: nil,
			MethodName:    "testing",
			ClientID:      "testClientId",
		},
		ClientMetricName:                    "testClientMetric",
		SesGetAccountEntitlementInterface:   mockSESGetGetAccountEntitlementInterface{},
		SesUpdateFulfillmentStatusInterface: mockSESUpdateFulfillmentStatusInterface{},
		ClientProductMappingInterface:       mockSESClientProductMappingInterface{},
	}
}

func getTestSESApplication2() SESApplication {
	return SESApplication{
		RequestID: "testRequestId2",
		Tuid:      "testTuId",
		ApiContext: external.APIContext{
			Configuration: nil,
			MethodName:    "testing",
			ClientID:      "testClientId",
		},
		ClientMetricName:                    "testClientMetric",
		SesGetAccountEntitlementInterface:   mockSESGetGetAccountEntitlementInterface{},
		SesUpdateFulfillmentStatusInterface: mockSESUpdateFulfillmentStatusInterface{},
		ClientProductMappingInterface:       mockSESClientProductMappingInterface{},
	}
}

func TestSESApplication_GetAccountEntitlements(t *testing.T) {
	sesApplication := getTestSESApplication()
	goods, err := sesApplication.GetAccountEntitlements()

	if err != nil {
		assert.Fail(t, "There should be no errors")
	}

	assert.Equal(t, 1, len(goods))
	assert.Equal(t, "34b763fe-a304-60c1-b236-b794e3941b5a", goods[0].ReceiptID)

	sesApplication.ApiContext.ClientID = ""
	goods, err = sesApplication.GetAccountEntitlements()

	assert.Equal(t, 1, len(goods))
	assert.Equal(t, "testId1", goods[0].ReceiptID)

	sesApplication.ApiContext.ClientID = "asdfasdfasdf"
	goods, err = sesApplication.GetAccountEntitlements()

	assert.Equal(t, 0, len(goods))

	sesApplication.ApiContext.ClientID = "testClientId"
	goods, err = sesApplication.GetAccountEntitlements()

	assert.Equal(t, 1, len(goods))
	assert.Equal(t, "testsku", goods[0].SKU)

}

func TestSESApplication_UpdateSESFulfillmentStatus_ValidUpdate(t *testing.T) {
	sesApplication := getTestSESApplication()
	receiptUpdates := make([]tesla.ReceiptToInstruction, 0)

	receipt := tesla.ReceiptToInstruction{
		FulfillmentAddress: "testAddress",
		ReceiptID:          "testReceiptId",
		LastInstruction:    "fulfill",
	}

	receiptUpdates = append(receiptUpdates, receipt)

	request := tesla.SetStatusForReceiptsRequest{ReceiptUpdates: receiptUpdates}

	entitlements, err, count := sesApplication.UpdateFulfillmentStatus(request)

	if err != nil {
		assert.Fail(t, "There should be no errors")
	}

	assert.Equal(t, 1, len(entitlements))
	assert.Equal(t, "testId", entitlements[0].ReceiptID)
	assert.Equal(t, string(externaladg.Success), entitlements[0].Result)
	assert.Equal(t, 0, count)
}

func TestSESApplication_UpdateSESFulfillmentStatus_ValidUpdateWithMetadata(t *testing.T) {
	sesApplication := getTestSESApplication2()
	receiptUpdates := make([]tesla.ReceiptToInstruction, 0)

	receipt := tesla.ReceiptToInstruction{
		FulfillmentAddress: "testAddress",
		ReceiptID:          "testReceiptId2",
		LastInstruction:    "fulfill",
	}

	receiptUpdates = append(receiptUpdates, receipt)

	request := tesla.SetStatusForReceiptsRequest{ReceiptUpdates: receiptUpdates}

	entitlements, err, count := sesApplication.UpdateFulfillmentStatus(request)

	if err != nil {
		assert.Fail(t, "There should be no errors")
	}

	assert.Equal(t, 1, len(entitlements))
	assert.Equal(t, "34b763fe-a304-60c1-b236-b794e3941b5a", entitlements[0].ReceiptID)
	assert.Equal(t, string(externaladg.Success), entitlements[0].Result)
	assert.Equal(t, 0, count)
}

func TestSESApplication_UpdateSESFulfillmentStatus_NoOp(t *testing.T) {
	sesApplication := getTestSESApplication()
	receiptUpdates := make([]tesla.ReceiptToInstruction, 0)

	receipt := tesla.ReceiptToInstruction{
		FulfillmentAddress: "testAddress",
		ReceiptID:          "testReceiptId",
		LastInstruction:    "noOp",
	}

	receiptUpdates = append(receiptUpdates, receipt)

	request := tesla.SetStatusForReceiptsRequest{ReceiptUpdates: receiptUpdates}

	entitlements, err, count := sesApplication.UpdateFulfillmentStatus(request)

	if err != nil {
		assert.Fail(t, "There should be no errors")
	}

	assert.Equal(t, 0, len(entitlements))
	assert.Equal(t, 1, count)
}

func TestSESApplication_UpdateSESFulfillmentStatus_NoOpAndValid(t *testing.T) {
	sesApplication := getTestSESApplication()
	receiptUpdates := make([]tesla.ReceiptToInstruction, 0)

	receipt := tesla.ReceiptToInstruction{
		FulfillmentAddress: "testAddress",
		ReceiptID:          "testReceiptId",
		LastInstruction:    "fulfill",
	}

	noOpReceipt := tesla.ReceiptToInstruction{
		FulfillmentAddress: "testAddress",
		ReceiptID:          "testReceiptId",
		LastInstruction:    "noOp",
	}

	receiptUpdates = append(receiptUpdates, receipt)
	receiptUpdates = append(receiptUpdates, noOpReceipt)

	request := tesla.SetStatusForReceiptsRequest{ReceiptUpdates: receiptUpdates}

	receiptUpdateResults, err, count := sesApplication.UpdateFulfillmentStatus(request)

	if err != nil {
		assert.Fail(t, "There should be no errors")
	}

	assert.Equal(t, 1, len(receiptUpdateResults))
	assert.Equal(t, "testId", receiptUpdateResults[0].ReceiptID)
	assert.Equal(t, string(externaladg.Success), receiptUpdateResults[0].Result)
	assert.Equal(t, 1, count)
}

func TestGetProductsValid(t *testing.T) {
	sesApplication := getTestSESApplication()
	clientProductMappingInput := externalaws.GetClientProductMappingInput{ClientID: sesApplication.ApiContext.ClientID}
	productMapping, err := sesApplication.ClientProductMappingInterface.GetClientProductMapping(clientProductMappingInput)

	if err != nil {
		assert.Fail(t, "There should be no errors")
	}

	assert.Equal(t, 1, len(productMapping.ProductIds))
	assert.Equal(t, "testProduct", productMapping.ProductIds[0])

	clientProductMappingInput = externalaws.GetClientProductMappingInput{ClientID: ""}
	productMapping, err = sesApplication.ClientProductMappingInterface.GetClientProductMapping(clientProductMappingInput)

	if err != nil {
		assert.Fail(t, "There should be no errors")
	}

	assert.Equal(t, 0, len(productMapping.ProductIds))
}

func TestGetFulfillmentStatusObjectsFromMetadataFiltersValid(t *testing.T) {
	sesApplication := getTestSESApplication()
	sesApplication.ApiContext.ClientID = "testClientId"
	receiptMapping := make(map[string]string)
	metadataFilters := make([]externalses.MetadataFilter, 0)

	var adgGoodsFilterRule externalses.MetadataFilterRule
	adgGoodsFilterRule.MetadataFilterRuleValue = "testGoodId"

	var adgGoodsFilter externalses.MetadataFilter
	adgGoodsFilter.MetadataKey = adgGoods
	adgGoodsFilter.MetadataFilterRule = adgGoodsFilterRule

	metadataFilters = append(metadataFilters, adgGoodsFilter)

	receiptMapping["testGoodId"] = "fulfill"

	fulfillmentStatusObjects, err := sesApplication.getFulfillmentStatusObjectsFromMetadataFilters(receiptMapping, metadataFilters)

	if err != nil {
		assert.Fail(t, "There should be no errors")
	}

	assert.Equal(t, 1, len(fulfillmentStatusObjects))
	assert.Equal(t, "testId1", fulfillmentStatusObjects[0].EntitlementId)
	assert.Equal(t, "FULFILLED", fulfillmentStatusObjects[0].FulfillmentStatus)
}

func TestGetFulfillmentStatusObjectsFromMetadataFiltersNoFilters(t *testing.T) {
	sesApplication := getTestSESApplication()
	sesApplication.ApiContext.ClientID = "testClientId"
	receiptMapping := make(map[string]string)
	metadataFilters := make([]externalses.MetadataFilter, 0)

	fulfillmentStatusObjects, err := sesApplication.getFulfillmentStatusObjectsFromMetadataFilters(receiptMapping, metadataFilters)

	if err != nil {
		assert.Fail(t, "There should be no errors")
	}

	assert.Equal(t, 0, len(fulfillmentStatusObjects))
}

func TestGetFulfillmentStatusObjectsFromMetadataFiltersInvalidValue(t *testing.T) {
	sesApplication := getTestSESApplication()
	sesApplication.ApiContext.ClientID = "testClientId"
	receiptMapping := make(map[string]string)
	metadataFilters := make([]externalses.MetadataFilter, 0)

	var adgGoodsFilterRule externalses.MetadataFilterRule
	adgGoodsFilterRule.MetadataFilterRuleValue = "inavlid"

	var adgGoodsFilter externalses.MetadataFilter
	adgGoodsFilter.MetadataKey = adgGoods
	adgGoodsFilter.MetadataFilterRule = adgGoodsFilterRule

	metadataFilters = append(metadataFilters, adgGoodsFilter)

	receiptMapping["invalid"] = "fulfill"

	fulfillmentStatusObjects, err := sesApplication.getFulfillmentStatusObjectsFromMetadataFilters(receiptMapping, metadataFilters)

	if err != nil {
		assert.Fail(t, "There should be no errors")
	}

	assert.Equal(t, 0, len(fulfillmentStatusObjects))
}

func TestGetReceiptsToUpdateValid(t *testing.T) {
	receiptsToReturn := make([]tesla.ReceiptToInstruction, 0)

	receiptValid := tesla.ReceiptToInstruction{
		FulfillmentAddress: "testAddress",
		ReceiptID:          "testId",
		LastInstruction:    "fulfill",
	}

	receiptsToReturn = append(receiptsToReturn, receiptValid)

	returnedReceipts, noOpCount := getReceiptsToUpdate(receiptsToReturn)

	assert.Equal(t, 1, len(returnedReceipts))
	assert.Equal(t, "testId", returnedReceipts[0].ReceiptID)
	assert.Equal(t, "testAddress", returnedReceipts[0].FulfillmentAddress)
	assert.Equal(t, "fulfill", returnedReceipts[0].LastInstruction)
	assert.Equal(t, 0, noOpCount)
}

func TestGetReceiptsToUpdateInvalid(t *testing.T) {
	receiptsToReturn := make([]tesla.ReceiptToInstruction, 0)

	receiptInValid := tesla.ReceiptToInstruction{
		FulfillmentAddress: "testAddress",
		ReceiptID:          "testId",
		LastInstruction:    "invalid",
	}

	receiptsToReturn = append(receiptsToReturn, receiptInValid)

	returnedReceipts, noOpCount := getReceiptsToUpdate(receiptsToReturn)

	assert.Equal(t, 0, len(returnedReceipts))
	assert.Equal(t, 1, noOpCount)
}

func TestGetReceiptsToUpdateBoth(t *testing.T) {
	receiptsToReturn := make([]tesla.ReceiptToInstruction, 0)

	receiptValid := tesla.ReceiptToInstruction{
		FulfillmentAddress: "testAddress",
		ReceiptID:          "testId",
		LastInstruction:    "fulfill",
	}

	receiptInValid := tesla.ReceiptToInstruction{
		FulfillmentAddress: "testAddress",
		ReceiptID:          "testId",
		LastInstruction:    "invalid",
	}

	receiptsToReturn = append(receiptsToReturn, receiptInValid)
	receiptsToReturn = append(receiptsToReturn, receiptValid)

	returnedReceipts, noOpCount := getReceiptsToUpdate(receiptsToReturn)

	assert.Equal(t, 1, len(returnedReceipts))
	assert.Equal(t, "testId", returnedReceipts[0].ReceiptID)
	assert.Equal(t, "testAddress", returnedReceipts[0].FulfillmentAddress)
	assert.Equal(t, "fulfill", returnedReceipts[0].LastInstruction)
	assert.Equal(t, 1, noOpCount)
}

func TestCreateFulfillmentStatusObjectFulfill(t *testing.T) {
	fulfillmentStatusObject := createFulfillmentStatusObject("testId", "fulfill")

	assert.Equal(t, "testId", fulfillmentStatusObject.EntitlementId)
	assert.Equal(t, "FULFILLED", fulfillmentStatusObject.FulfillmentStatus)
}

func TestCreateFulfillmentStatusObjectRevoke(t *testing.T) {
	fulfillmentStatusObject := createFulfillmentStatusObject("testId", "revoke")

	assert.Equal(t, "testId", fulfillmentStatusObject.EntitlementId)
	assert.Equal(t, "REVOKED", fulfillmentStatusObject.FulfillmentStatus)
}

func TestCreateFulfillmentStatusObjectBadInput(t *testing.T) {
	fulfillmentStatusObject := createFulfillmentStatusObject("testId", "asdfasdfasdfasdf")

	assert.Equal(t, "testId", fulfillmentStatusObject.EntitlementId)
	assert.Equal(t, "", fulfillmentStatusObject.FulfillmentStatus)
}

func TestIsADGId(t *testing.T) {
	assert.Equal(t, true, isADGId("72b48be3-7aad-4db7-54ae-312033ffd083"), "This should be a valid uuid.")
	assert.Equal(t, false, isADGId("72b48be3-7aad-4db7-54ae-312033ffd08!"), "This is an invalid uuid.")
}

func TestGetFiltersForReceiptId(t *testing.T) {
	const contains = "CONTAINS"
	receiptId := "72b48be3-7aad-4db7-54ae-312033ffd083"

	var adgGoodsFilterRule externalses.MetadataFilterRule
	adgGoodsFilterRule.MetadataFilterRuleType = contains
	adgGoodsFilterRule.MetadataFilterRuleValue = receiptId

	var adgGoodsFilter externalses.MetadataFilter
	adgGoodsFilter.MetadataKey = adgGoods
	adgGoodsFilter.MetadataFilterRule = adgGoodsFilterRule

	var productInstancesFilterRule externalses.MetadataFilterRule
	productInstancesFilterRule.MetadataFilterRuleType = contains
	productInstancesFilterRule.MetadataFilterRuleValue = receiptId

	var productInstancesFilter externalses.MetadataFilter
	productInstancesFilter.MetadataKey = productInstances
	productInstancesFilter.MetadataFilterRule = productInstancesFilterRule

	filters := getFiltersForReceiptId(receiptId)

	assert.Equal(t, 2, len(filters), "There should always be two filters per receipt id.")
	assert.Equal(t, adgGoodsFilter, filters[0], "These should be the same filter.")
	assert.Equal(t, productInstancesFilter, filters[1], "These should be the same filter.")
}

func TestTranslate3PLastOp(t *testing.T) {
	assert.Equal(t, string(externalses.Fulfilled), translate3PLastOp(string(externaladg.Fulfill)), "Fulfill should translate to Fulfilled")
	assert.Equal(t, string(externalses.Revoked), translate3PLastOp(string(externaladg.Revoke)), "Revoke should translate to Revoked")
	assert.Equal(t, "", translate3PLastOp("anythingElse"), "Anything else should translate to empty string")
}

func TestGetAdgGoodIdIfExistBothProductAndAdgGoodEntriesWithTwoAccounts(t *testing.T) {
	metaData := map[string]string{"ProductInstances": "[{\"productInstanceIdType\":\"adg_good_id\",\"productInstanceId\":\"c8b763fe-a304-5b9d-90d7-4b34df8a74fb\",\"productId\":\"PLWVE/hover_game\",\"account\":{\"accountType\":\"AMAZON\",\"accountId\":\"testestIdtid\"}},{\"productInstanceIdType\":\"adg_good_id\",\"productInstanceId\":\"34b763fe-a304-60c1-b236-b794e3941b5a\",\"productId\":\"PLWVE/hover_game\",\"account\":{\"accountType\":\"TWITCH\",\"accountId\":\"testId\"}}]", "ADGGoods": "[{\"vendor\":\"PLWVE\",\"sku\":\"hover_game\",\"goodId\":\"34b763fe-a304-60c1-b236-b794e3941b5a\",\"account\":{\"accountType\":\"TWITCH\",\"accountId\":\"testId\"},\"vendorSkuString\":\"PLWVE/hover_game\"},{\"vendor\":\"PLWVE\",\"sku\":\"hover_game\",\"goodId\":\"c8b763fe-a304-5b9d-90d7-4b34df8a74fb\",\"account\":{\"accountType\":\"AMAZON\",\"accountId\":\"testId\"},\"vendorSkuString\":\"PLWVE/hover_game\"}]"}

	testEntitlement := externalses.Entitlement{
		EntitlementId:            "testId",
		EntitlementAccounts:      nil,
		RelatedAccounts:          nil,
		EntitlementStatus:        "",
		FulfillmentStatus:        "FULFILLED",
		CreatedDate:              "",
		LastUpdatedDate:          "",
		EntitlementStatusHistory: nil,
		FulfillmentStatusHistory: nil,
		Metadata:                 metaData,
		Domain:                   "",
		Product:                  externalses.EntitlementProduct{},
	}

	assert.Equal(t, "", getAdgGoodIdIfExist(testEntitlement))
}

func TestGetAdgGoodIdIfExistProductEntryWithTwoAccounts(t *testing.T) {
	metaData := map[string]string{"ProductInstances": "[{\"productInstanceIdType\":\"adg_good_id\",\"productInstanceId\":\"c8b763fe-a304-5b9d-90d7-4b34df8a74fb\",\"productId\":\"PLWVE/hover_game\",\"account\":{\"accountType\":\"AMAZON\",\"accountId\":\"A2TBFIHY1XPAAX\"}},{\"productInstanceIdType\":\"adg_good_id\",\"productInstanceId\":\"34b763fe-a304-60c1-b236-b794e3941b5a\",\"productId\":\"PLWVE/hover_game\",\"account\":{\"accountType\":\"TWITCH\",\"accountId\":\"59018922\"}}]"}
	testEntitlement := externalses.Entitlement{
		EntitlementId:            "testId",
		EntitlementAccounts:      nil,
		RelatedAccounts:          nil,
		EntitlementStatus:        "",
		FulfillmentStatus:        "FULFILLED",
		CreatedDate:              "",
		LastUpdatedDate:          "",
		EntitlementStatusHistory: nil,
		FulfillmentStatusHistory: nil,
		Metadata:                 metaData,
		Domain:                   "",
		Product:                  externalses.EntitlementProduct{},
	}

	assert.Equal(t, "", getAdgGoodIdIfExist(testEntitlement))
}

func TestGetAdgGoodIdIfExistAdgGoodEntryWithTwoAccounts(t *testing.T) {
	metaData := map[string]string{"ADGGoods": "[{\"vendor\":\"PLWVE\",\"sku\":\"hover_game\",\"goodId\":\"34b763fe-a304-60c1-b236-b794e3941b5a\",\"account\":{\"accountType\":\"TWITCH\",\"accountId\":\"testId\"},\"vendorSkuString\":\"PLWVE/hover_game\"},{\"vendor\":\"PLWVE\",\"sku\":\"hover_game\",\"goodId\":\"c8b763fe-a304-5b9d-90d7-4b34df8a74fb\",\"account\":{\"accountType\":\"AMAZON\",\"accountId\":\"testId\"},\"vendorSkuString\":\"PLWVE/hover_game\"}]"}
	testEntitlement := externalses.Entitlement{
		EntitlementId:            "testId",
		EntitlementAccounts:      nil,
		RelatedAccounts:          nil,
		EntitlementStatus:        "",
		FulfillmentStatus:        "FULFILLED",
		CreatedDate:              "",
		LastUpdatedDate:          "",
		EntitlementStatusHistory: nil,
		FulfillmentStatusHistory: nil,
		Metadata:                 metaData,
		Domain:                   "",
		Product:                  externalses.EntitlementProduct{},
	}

	assert.Equal(t, "", getAdgGoodIdIfExist(testEntitlement))
}

func TestGetAdgGoodIdIfExistAdgGoodEntryWithOneAccounts(t *testing.T) {
	metaData := map[string]string{"ADGGoods": "[{\"vendor\":\"PLWVE\",\"sku\":\"hover_game\",\"goodId\":\"34b763fe-a304-60c1-b236-b794e3941b5a\",\"account\":{\"accountType\":\"TWITCH\",\"accountId\":\"testId\"},\"vendorSkuString\":\"PLWVE/hover_game\"}]"}
	testEntitlement := externalses.Entitlement{
		EntitlementId:            "testId",
		EntitlementAccounts:      nil,
		RelatedAccounts:          nil,
		EntitlementStatus:        "",
		FulfillmentStatus:        "FULFILLED",
		CreatedDate:              "",
		LastUpdatedDate:          "",
		EntitlementStatusHistory: nil,
		FulfillmentStatusHistory: nil,
		Metadata:                 metaData,
		Domain:                   "",
		Product:                  externalses.EntitlementProduct{},
	}

	assert.Equal(t, "34b763fe-a304-60c1-b236-b794e3941b5a", getAdgGoodIdIfExist(testEntitlement))
}

func TestGetAdgGoodIdIfExistNoMetadata(t *testing.T) {
	metaData := map[string]string{}
	testEntitlement := externalses.Entitlement{
		EntitlementId:            "testId",
		EntitlementAccounts:      nil,
		RelatedAccounts:          nil,
		EntitlementStatus:        "",
		FulfillmentStatus:        "FULFILLED",
		CreatedDate:              "",
		LastUpdatedDate:          "",
		EntitlementStatusHistory: nil,
		FulfillmentStatusHistory: nil,
		Metadata:                 metaData,
		Domain:                   "",
		Product:                  externalses.EntitlementProduct{},
	}

	assert.Equal(t, "", getAdgGoodIdIfExist(testEntitlement))
}
