### .ebextensions
EBExtensions are extension scripts to configure the beanstalk environment and customer the AWS resources it uses.
Further reading: http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/ebextensions.html

### cwl-eb-docker-stdouterr-logs-v1.config
This file contains configuration for the Cloudwatch logs agent to pick up the docker application logs. This is how we get Cloudwatch log aggregation for our application logs.

### 00_docker_run_options.config
We use this script to inject some `docker run` options into the beanstalk init script. This is how we do log rotation of the docker output logs. For more background on this script and why it's necessary, check out: https://tt.amazon.com/0115687833
