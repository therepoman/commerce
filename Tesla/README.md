# Tesla

## Setting Up the Go Service

### Setting up your Github account

 - Go to your account settings page (https://git.xarth.tv/settings/keys) and follow the instructions there to add a new SSH key to your account.

### Setting up your workspace

 - Install go:

		brew install go

 - Create a Go workspace folder in your home directory. Ex:

		mkdir /Users/<username>/<GoWorkspace>

 - Set your environment variable for GOPATH (in your .bashrc, .zshrc, or etc.)

		export GOPATH=/Users/<username>/<GoWorkspace>

 - Append GOPATH/bin to PATH environment variable

		export PATH=$PATH:$GOPATH/bin

 - To check if Go installed correctly, run go version from commandline. This version should more than 1.6

 - Clone down Tesla source within the new gopath. You'll need to be on the twitch VPN for this to work.

		go get code.justin.tv/commerce/Tesla

 - Directory structure will now look like this:

		/Users/<username>/GoWorkspace/src/code.justin.tv/commerce/Tesla

### Setup for development

 - Install dep

		brew install dep

 - Navigate to your workspace

		cd /Users/<username>/GoWorkspace/src/code.justin.tv/commerce/Tesla

 - Run

    `make dev`

### Making a code change
 - Create a new git branch : git checkout -b <branch name>

 - git push origin <branch name>

 - Click compare and pull request on the git website for your package

 - Make changes and commit to the branch. Do not use commit --amend --no-edit. Push the commits

 - When you get a +1 to your pull request, do "Squash and merge your changes"
 Note: Make a separate branch/commit for your vendor changes if possible to avoid reviewers having to see massive file changes

### Building, Compiling, Testing, Running

To build and run the service, you'll first need to:

 - Install AWS CLI - https://docs.aws.amazon.com/cli/latest/userguide/installing.html
 - Gain access to the dev AWS accounts, create an IAM user and create an acccess_key and secret for the AWS account:

		fuel-tes-dev@amazon.com

 - Configure the fuel-tes-dev profile using the AWS cli

		aws configure --profile fuel-tes-dev

 - Install tools for linting and error checking:

		go get -u github.com/kisielk/errcheck

		go get -u github.com/golang/lint/golint

Once complete, you can take a look at the Makefile to see the targets that are available. Examples:  	
		make fmt   // runs formatting on your Go files
		make lint  // checks for mechanical errors and ill-formatted conventions
		make release // similar to brazil-build release, the target to run in preparation for a release candidate

### debugging
- Install [Delve](https://github.com/go-delve/delve/blob/master/Documentation/usage/dlv.md)

        go get -u github.com/go-delve/delve/cmd/dlv

- Run TES under debug mode

        make debug

- Attach IDE to the debugger (localhost:2345). Instructions vary depending on the debugger, but should be simple. Attach the IDE debugger to the process on localhost.

    - [Intellij/GoLand](https://blog.jetbrains.com/go/2019/02/06/debugging-with-goland-getting-started/#debugging-a-running-application-on-a-remote-machine)

### Adding new dependecies

 - Should be as simple as running :

    glide get <go-pkg>

You'll need to commit the glide.lock and glide.yaml files in order for the dependencies to be picked up by other users.

### Useful Tools and Wikis

 - https://twitchtv.atlassian.net/wiki/display/ENG/Useful+Tools
