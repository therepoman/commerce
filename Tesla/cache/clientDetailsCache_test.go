package cache

import (
	"encoding/json"
	"testing"
	"time"

	"code.justin.tv/commerce/Tesla/config"
	"code.justin.tv/commerce/Tesla/external"

	"github.com/go-redis/redis"
)

const ()

const (
	// We use the Hi-Rez client ID for our testing.
	testClientID       string = "t11hnc4bu1xj2yljd1t4dbruut4srkf"
	expectedMetricName string = "Hi-Rez_Studios"
)

func createTestAPIContext(clientID string) external.APIContext {
	return external.APIContext{
		Configuration: config.GetTestConfiguration(),
		MethodName:    string(config.GetGoodsForUserAPI),
		ClientID:      clientID,
	}
}

func cleanCache() error {
	return ClientDetailsCache().DeleteClientDetails(createTestAPIContext(testClientID))
}

func verifyCacheStats(t *testing.T, initialStats *ClientDetailsCacheStats, stats *ClientDetailsCacheStats, expectedHitsDelta uint64, expectedMissesDelta uint64, expectedLocalHitsDelta uint64, expectedLocalMissesDelta uint64) {
	if (stats.Hits - initialStats.Hits) != expectedHitsDelta {
		t.Error("Unexpected number of cache hits. Expected : ", expectedHitsDelta, " Got : ", (stats.Hits - initialStats.Hits))
		return
	}

	if (stats.Misses - initialStats.Misses) != expectedMissesDelta {
		t.Error("Unexpected number of cache misses. Expected : ", expectedMissesDelta, " Got : ", (stats.Misses - initialStats.Misses))
		return
	}

	if (stats.LocalHits - initialStats.LocalHits) != expectedLocalHitsDelta {
		t.Error("Unexpected number of local cache hits. Expected : ", expectedLocalHitsDelta, " Got : ", (stats.LocalHits - initialStats.LocalHits))
		return
	}

	if (stats.LocalMisses - initialStats.LocalMisses) != expectedLocalMissesDelta {
		t.Error("Unexpected number of local cache misses. Expected : ", expectedLocalMissesDelta, " Got : ", (stats.LocalMisses - initialStats.LocalMisses))
		return
	}

	return
}

func TestGetClientMetricNameFromOwl(t *testing.T) {
	err := cleanCache()
	// err here could be because key doesn't exist (as we'd want).

	initialStats := ClientDetailsCache().GetCacheStats()

	// Deep check. Should hit owl service.
	// This will cause 3 misses, two local and one non-local. There will be no hits as the item is neither in the LRU
	// or in Redis. The misses are explained as follows:
	// During a get, the item will first try and be retrieved from the LRU, this will fail (one local miss). It
	// will then try and fetch it from Redis, which will also fail and then cause Owl to be called (one non-local-miss).
	// Prior to calling redis, it will also check the LRU cache (another local miss). While this is redundant in our case,
	// in a multi-threaded scenario, the LRU could be updated between calls which is why the library performs this.
	details, err := ClientDetailsCache().GetClientDetails(createTestAPIContext(testClientID))
	if err != nil {
		t.Error("Error while getting initial client details (cleaned cached) : ", err)
	}

	if details.ClientMetricName != expectedMetricName {
		t.Error("Unexpected client metric name. Expected : ", expectedMetricName, " Got : ", details.ClientMetricName)
	}

	stats := ClientDetailsCache().GetCacheStats()

	// Verify our stats are as expected.
	verifyCacheStats(t, initialStats, stats, 0, 1, 0, 2)

	// Should hit LRU cache.
	// This will generate 1 hit.
	details, err = ClientDetailsCache().GetClientDetails(createTestAPIContext(testClientID))
	if err != nil {
		t.Error("Error while getting initial client details from LRU cache : ", err)
	}

	if details.ClientMetricName != expectedMetricName {
		t.Error("Unexpected client metric name. Expected : ", expectedMetricName, " Got : ", details.ClientMetricName)
	}

	stats = ClientDetailsCache().GetCacheStats()

	// Verify our expected stats.
	verifyCacheStats(t, initialStats, stats, 0, 1, 1, 2)

	// Now we clean up the entry we're testing.
	err = cleanCache()
	if err != nil {
		t.Error("Error cleaning the cache : ", err)
	}
}

func TestGetClientMetricNameFromRedis(t *testing.T) {
	// Clean cache for sanity. err here could be because key doesn't exist (as we'd want).
	err := cleanCache()

	// Get the stats of the cache initially.
	initialStats := ClientDetailsCache().GetCacheStats()

	// And re-insert it simulate reading directly from redis.
	testClientDetails := ClientDetails{
		ClientID:         testClientID,
		ClientMetricName: expectedMetricName,
	}

	// Create a redis client.
	client := redis.NewClient(&redis.Options{
		Addr:     clientDetailsRedisHostDev,
		Password: clientDetailsRedisPwDev,
		DB:       0,
	})

	detailsBytes, err := json.Marshal(testClientDetails)
	if err != nil {
		t.Error("Error marshalling test details : ", err)
	}

	// Store the entry in redis.
	setStatus := client.Set(testClientID, detailsBytes, 24*time.Hour)
	if setStatus.Err() != nil {
		t.Error("Error setting details directly from redis : ", err)
	}

	//Now this test will miss on the LRU, but hit in redis. So 2 more local misses and 1 hit.
	details, err := ClientDetailsCache().GetClientDetails(createTestAPIContext(testClientID))
	if err != nil {
		t.Error("Error while getting initial client details (cleaned cached) : ", err)
	}

	if details.ClientMetricName != expectedMetricName {
		t.Error("Unexpected client metric name. Expected : ", expectedMetricName, " Got : ", details.ClientMetricName)
	}

	stats := ClientDetailsCache().GetCacheStats()

	// Verify our expected stats.
	verifyCacheStats(t, initialStats, stats, 1, 0, 0, 2)
}
