package cache

import (
	"encoding/json"
	"sync"
	"time"

	"code.justin.tv/commerce/Tesla/config"
	"code.justin.tv/commerce/Tesla/external"
	"code.justin.tv/commerce/Tesla/externaltwitch"

	"github.com/go-redis/cache"
	"github.com/go-redis/redis"
)

const (
	clientDetailsRedisHostDev string = "tesla-dev-owl-redis.dbnchr.ng.0001.usw2.cache.amazonaws.com:6379"
	clientDetailsRedisPwDev   string = ""

	clientDetailsRedisHostProd string = "tesla-prod-owl-redis.uki5zr.ng.0001.usw2.cache.amazonaws.com:6379"
	clientDetailsRedisPwProd   string = ""

	clientDetailsCacheSize       int           = 100
	clientDetailsCacheExpiration time.Duration = 24 * time.Hour
)

var (
	clientDetailsCacheInstance ClientDetailsCacheInterface
	clientDetailsCacheOnce     sync.Once
)

// ClientDetailsCache Singleton cache of client details.
func ClientDetailsCache() ClientDetailsCacheInterface {
	clientDetailsCacheOnce.Do(func() {
		// Check if prod and swap.
		host := clientDetailsRedisHostDev
		pw := clientDetailsRedisPwDev

		if config.GetCurrentConfiguration().IsProd() {
			host = clientDetailsRedisHostProd
			pw = clientDetailsRedisPwProd
		}

		clientDetailsCacheInstance = newClientDetailsCache(host, pw, clientDetailsCacheSize, clientDetailsCacheExpiration)
	})

	return clientDetailsCacheInstance
}

//ClientDetails is the structure that is maintained within this cache.
type ClientDetails struct {
	ClientID         string
	ClientMetricName string
}

//ClientDetailsCacheStats encapsulates statistics about the cache being used.
type ClientDetailsCacheStats struct {
	Hits        uint64 // The number of hits using the redis cluster
	Misses      uint64 // The number of misses at the redis cluster level
	LocalHits   uint64 // The number of hits to the local LRU cache
	LocalMisses uint64 // The number of misses to the local LRU cache
}

//ClientDetailsCacheInterface is the interface to a cache managing client details.
type ClientDetailsCacheInterface interface {
	Ping() bool
	GetCacheStats() *ClientDetailsCacheStats
	GetClientDetails(external.APIContext) (*ClientDetails, error)
	DeleteClientDetails(external.APIContext) error
}

//A client details cache using an LRU cache backed by a redis instance.
type clientDetailsCache struct {
	redisClient *redis.Client
	cacheCodec  *cache.Codec
}

func (c clientDetailsCache) Ping() bool {
	_, err := c.redisClient.Ping().Result()

	if err == nil {
		return true
	}

	return false
}

func (c clientDetailsCache) GetCacheStats() *ClientDetailsCacheStats {
	stats := c.cacheCodec.Stats()
	cacheStats := ClientDetailsCacheStats{
		Hits:        stats.Hits,
		Misses:      stats.Misses,
		LocalHits:   stats.LocalHits,
		LocalMisses: stats.LocalMisses,
	}

	return &cacheStats
}

func (c clientDetailsCache) DeleteClientDetails(context external.APIContext) error {
	return c.cacheCodec.Delete(context.ClientID)
}

func (c clientDetailsCache) GetClientDetails(context external.APIContext) (*ClientDetails, error) {
	details := new(ClientDetails)
	item := cache.Item{
		Key:    context.ClientID,
		Object: details,
		Func: func() (interface{}, error) {
			owlInterface := externaltwitch.NewOwlService(context)

			input := externaltwitch.GetClientDetailsInput{ClientID: context.ClientID}
			output, err := owlInterface.GetClientDetails(input)

			if err != nil {
				return ClientDetails{}, err
			}

			details := ClientDetails{
				ClientID:         context.ClientID,
				ClientMetricName: output.ClientMetricName,
			}

			return details, err // err should be nil here, or it would have been caught in the else block.
		},
	}

	err := c.cacheCodec.Once(&item)

	if err != nil {
		return &ClientDetails{}, err
	}

	return details, nil
}

//NewClientDetailsCache creates a client details cache. The configuration passed in will be used
//when making calls to refresh the cache values.
func newClientDetailsCache(redisHost string, redisPassword string, cacheSize int, cacheExpiration time.Duration) ClientDetailsCacheInterface {
	client := redis.NewClient(&redis.Options{
		Addr:     redisHost,
		Password: redisPassword,
		DB:       0, // use default DB
	})

	codec := cache.Codec{
		Redis: client,
		// We marshal and unmarshal the objects using JSON in this cache
		Marshal: func(v interface{}) ([]byte, error) {
			return json.Marshal(v)
		},
		Unmarshal: func(b []byte, v interface{}) error {
			return json.Unmarshal(b, v)
		},
	}

	codec.UseLocalCache(cacheSize, cacheExpiration)

	return clientDetailsCache{
		redisClient: client,
		cacheCodec:  &codec,
	}
}
