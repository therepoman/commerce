if [ -z "$1" ]
  then
    echo "No Bearer token was supplied."
    exit 1
fi

if [ -z "$2" ]
  then
    echo "No Client Id was supplied."
    exit 1
fi

curl -v -H "Accept: application/vnd.twitchtv.v5+json" \
-H "Client-ID: $2" \
-H "Authorization: Bearer $1" -X POST "https://api.twitch.tv/kraken/commerce/user/goods" | python -m json.tool
