if [ -z "$1" ]
  then
    echo "No OAuth token was supplied."
    exit 1
fi

curl -v \
-H "Content-Type: application/json" \
-H "Authorization: $1" "https://api-adg.prod.us-west2.twitch.tv/kraken/user" | python -m json.tool
