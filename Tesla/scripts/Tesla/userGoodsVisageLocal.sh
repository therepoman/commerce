if [ -z "$1" ]
  then
    echo "No OAuth token was supplied."
    exit 1
fi

curl -v \
-H "Authorization: OAuth $1" \
-X POST "localhost:8000/v5/commerce/user/goods?using_tesla_client=true&api_version=5" | python -m json.tool
