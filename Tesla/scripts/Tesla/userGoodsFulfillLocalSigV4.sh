if [ -z "$1" ]
  then
    echo "No TUID was given."
    exit 1
fi

if [ -z "$2" ]
  then
    echo "No clientid was given."
    exit 1
fi

if [ -z "$3" ]
  then
    echo "No data was supplied."
    exit 1
fi

curl  -v --data "@$3" \
-H "Content-Type: application/json; charset=UTF-8" \
-H "Twitch-Client-ID: $2" \
-X POST "localhost:8000/commerce/user/goods/fulfill?tuid=$1" | python -m json.tool
