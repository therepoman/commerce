if [ -z "$1" ]
  then
    echo "No OAuth token was supplied."
    exit 1
fi

if [ -z "$2" ]
  then
    echo "No clientid token was supplied."
    exit 1
fi

if [ -z "$3" ]
  then
    echo "No data was supplied."
    exit 1
fi

curl -v --data "@$3" \
-H "Accept: application/vnd.twitchtv.v5+json" \
-H "Client-ID: $2" \
-H "Content-Type: application/json; charset=UTF-8" \
-H "Authorization: OAuth $1" \
-X POST "https://visage.staging.us-west2.justin.tv/kraken/commerce/user/goods/fulfill?using_tesla_client=true" | python -m json.tool
