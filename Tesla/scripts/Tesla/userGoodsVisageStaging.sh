if [ -z "$1" ]
  then
    echo "No OAuth token was supplied."
    exit 1
fi

if [ -z "$2" ]
  then
    echo "No Client Id token was supplied."
    exit 1
fi

curl -v \
-H "Client-ID: $2" \
-H "Authorization: OAuth $1" \
-X POST "https://visage.staging.us-west2.justin.tv/kraken/commerce/user/goods?api_version=5" | python -m json.tool
