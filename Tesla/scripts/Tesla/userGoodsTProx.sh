#!/bin/bash

HOST=http://dev-dsk-ryanmeye-2a-242321ad.us-west-2.amazon.com:8000
#HOST=https://tprox-beta-na-backend.integ.amazon.com
#HOST=https://dev-dsk-cioc-2a-67d9c3ba.us-west-2.amazon.com

curl "${HOST}/adg/" \
-H 'Cookie: aws-target-static-id=1504304718836-621312; __utmz=194891197.1505152348.1.1.utmccn=(direct)|utmcsr=(direct)|utmcmd=(none); x-wl-uid=1UgSoDkmCX830tMEqzXfnYPT1Fu8Gt09xYiqemPWprgGyU+gF6RX1yVpAH04SfmrEA4icexISvDs=; amzn_meetings_announcement_version=3; session-id=144-2258659-9530118; session-id-time=2082787201l; session-token=BDzx6zvfcBAfjFDtGv7ISFLVQubtB1phVkbLbRhVYSrlv8se2pGIjmYF/T52iyWBiTAjSEDmdMAvCvxR69gmwbuYqK3PR36szrj0D0d1f9Ek3HTHrs+U3JUNt9hMTqPmOm5pTDFYsNXcMOGf+HauG6vIrnL1wzKIpnAwq/aQ9DZa3JKQxvMCNQeSWSdzQ18iFe09K5gB7wLjx2Eivl2rt53JI+5oKJALXGNhq1ru4LKZJSmwUdRCx7IEDlQmwMrn; aws_lang=en; s_vn=1535233684369%26vn%3D8; aws-target-visitor-id=1504304718841-407951.17_64; aws-target-data=%7B%22support%22%3A%221%22%7D; s_fid=3C3AB3DD0E5640E2-34C88E7D11B551FE; s_dslv=1506028507000; c_m=undefinedstackoverflow.comOther%20Natural%20Referrersundefined; aws-mkto-trk=id%3A112-TZM-766%26token%3A_mch-aws.amazon.com-1506028459652-57135; pN=21; s_pers=%20s_vnum%3D1506896498258%2526vn%253D3%7C1506896498258%3B%20s_invisit%3Dtrue%7C1506032777576%3B%20s_nr%3D1506030977579-New%7C1513806977579%3B; regStatus=pre-register; s_sess=%20s_cc%3Dtrue%3B%20s_sq%3Dawsamazonprod1%252Cawsamazonallprod1%253D%252526pid%25253Dawsjavasdk%2525252Flatest%2525252Fjavadoc%2525252Fcom%2525252Famazonaws%2525252Fmetrics%2525252Frequestmetriccollector%252526pidt%25253D1%252526oid%25253Dhttp%2525253A%2525252F%2525252Fdocs.aws.amazon.com%2525252FAWSJavaSDK%2525252Flatest%2525252Fjavadoc%2525252Fcom%2525252Famazonaws%2525252FResponse.html%252526ot%25253DA%3B; ubid-main=133-3306077-6653246; __utma=194891197.1977011639.1505152348.1506022849.1506446413.14; __utmc=194891197; __utmb=194891197' \
-H 'Content-Encoding: amz-1.0' \
-H "Origin: ${HOST}" \
-H 'Accept-Encoding: gzip, deflate' \
-H 'Accept-Language: en-US,en;q=0.8,fr;q=0.6' \
-H 'Authorization: AWS4-HMAC-SHA256 Credential=ASIAJQCNPBHI4MKZBKHA/20170926/us-east-1/TwitchProxyService/aws4_request SignedHeaders=content-encoding;host;x-amz-date;x-amz-requestsupertrace;x-amz-target  Signature=33c097fbc566c8a6ac84f135c39166f03627ea6bd28b3a244fca145ff5f84a6a' \
-H 'X-Amz-Security-Token: FQoDYXdzEGIaDOTRFVADTqVwwUuGXSKsATd+F/XwEsfmJTN29H0edAVnGL/w6ndJiSIMfCIn+X/Fnw8cM0GnLVQ7vaSZL3A4nlaYEX36ehWBN2yB2DEMP6rrrwTgFKYaSkAYwAyJF2hNnbUo80NnQjvWOVKCbLYRm5lxe5uU2i6K9x5cgc1XPNE1NUdssvz6T8s1uyMNsROkBlJWoJ10noHhjxP9gPZu5gGy1M7p9nHXeNTfuSQ/d8KzT4AjALwR8Kwz2OUo2ZiqzgU=' \
-H 'X-Amz-Target: com.amazonaws.twitchproxyservice.TwitchProxyService.SyncGoods' \
-H 'X-Requested-With: XMLHttpRequest' \
-H 'Connection: keep-alive' \
-H 'X-Amz-Date: 20170926T172100Z' \
-H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.79 Safari/537.36' -H 'Content-Type: application/json; charset=UTF-8' \
-H 'Accept: application/json, text/javascript, */*' \
-H 'Referer: ${HOST}/adg/explorer/index.html' \
-H 'x-amz-requestsupertrace: true' --data-binary $'{"SyncGoodsRequest": \n  {"client": \n    {"clientId": "Fuel"}, \n   "customer": \n    {"id": "amzn1.twitch.1495148729"}, \n   "startSync": \n    {"syncPoint": null, \n     "syncToken": null}\n  }\n}' \
--compressed
