if [ -z "$1" ]
  then
    echo "No OAuth token was supplied."
    exit 1
fi

if [ -z "$2" ]
  then
    echo "No clientid token was supplied."
    exit 1
fi

if [ -z "$3" ]
  then
    echo "No tuid token was supplied."
    exit 1
fi

if [ -z "$4" ]
  then
    echo "No data was supplied."
    exit 1
fi

curl  -v --data "@$4" \
-H "Content-Type: application/json; charset=UTF-8" \
-H "Authorization: $1" \
-X POST "localhost:8000/commerce/user/goods/fulfill?clientid=$2&tuid=$3" | python -m json.tool
