if [ -z "$1" ]
  then
    echo "No TUID was given."
    exit 1
fi

if [-z "$2" ]
  then
    echo "No ClientID was given."
    exit 1
fi

curl -v  \
-H "Twitch-Client-ID: $2" \
-X POST "localhost:8000/commerce/user/goods?tuid=$1" | python -m json.tool
