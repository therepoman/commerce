if [ -z "$1" ]
  then
    echo "No Bearer token was supplied."
    exit 1
fi

if [ -z "$2" ]
  then
    echo "No Client Id was supplied."
    exit 1
fi

curl -v \
-H "Client-ID: $2" \
-H "Authorization: Bearer $1" \
-X POST "localhost:8000/v5/commerce/user/goods?using_tesla_client=true&api_version=5" | python -m json.tool
