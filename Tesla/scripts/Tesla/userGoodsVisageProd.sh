if [ -z "$1" ]
  then
    echo "No OAuth token was supplied."
    exit 1
fi

curl -v -H "Accept: application/vnd.twitchtv.v5+json" \
-H "Authorization: OAuth $1" -X POST "https://api.twitch.tv/kraken/commerce/user/goods" | python -m json.tool
