#!/bin/bash

set -e

ACCOUNT=$1

./scripts/terraform_apply_${ACCOUNT}.sh
