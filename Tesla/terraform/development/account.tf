provider "aws" {
  region  = "us-west-2"
  profile = "fuel-tes-dev"
}

module "aws_account" {
  source               = "git::git+ssh://git@git.xarth.tv/systems/terraform.git//modules/account_template/region/us-west-2?ref=master"
  region               = "us-west-2"
  account_name         = "fuel-tes-dev"
  vpc_cidr             = "10.201.100.0/22"
  account_id           = "987702982876"
  owner                = "fuel-tes-dev@twitch.tv"
  service              = "Tesla"
  environment          = "development"
  credential_profile   = "fuel-tes-dev"
  public_subnet_shift  = 4
  private_subnet_shift = 2
  public_subnet_offset = 12
}
