resource "aws_elasticache_subnet_group" "owl_redis_subnet_group" {
  name       = "tesla-prod-owl-cache-subnet"
  subnet_ids = ["subnet-00d64c49"]

  lifecycle = {
    prevent_destroy = true
  }
}

resource "aws_elasticache_replication_group" "owl_redis" {
  replication_group_id          = "tesla-prod-owl-redis"
  replication_group_description = "tesla-prod-owl-redis"
  node_type                     = "cache.t2.medium"
  number_cache_clusters         = 1
  port                          = 6379
  availability_zones            = ["us-west-2b"]
  automatic_failover_enabled    = false
  subnet_group_name = "tesla-prod-owl-cache-subnet"
  security_group_ids = ["${module.aws_account.sg_id}"]

  lifecycle = {
    prevent_destroy = true
  }
}

resource "aws_elasticache_subnet_group" "tesla_throttling_redis_subnet_group" {
  name       = "tesla-prod-throttling-cache-subnet"
  subnet_ids = ["${split(",", module.aws_account.private_subnets)}"]

  lifecycle = {
    prevent_destroy = true
  }
}

resource "aws_elasticache_replication_group" "tesla_throttling_redis" {
  replication_group_id          = "prod-throttle-redis"
  replication_group_description = "prod-throttle-redis"
  node_type                     = "cache.r3.2xlarge"
  number_cache_clusters         = 3
  port                          = 6379
  availability_zones            = ["us-west-2a", "us-west-2b", "us-west-2c"]
  automatic_failover_enabled    = true
  subnet_group_name = "tesla-prod-throttling-cache-subnet"
  security_group_ids = ["${module.aws_account.sg_id}"]

  lifecycle = {
    prevent_destroy = true
  }
}
