provider "aws" {
  region  = "us-west-2"
  profile = "fuel-tes"
}

module "aws_account" {
  source               = "git::git+ssh://git@git.xarth.tv/systems/terraform.git//modules/account_template/region/us-west-2?ref=master"
  region               = "us-west-2"
  account_name         = "fuel-tes"
  vpc_cidr             = "10.201.104.0/22"
  account_id           = "339420647183"
  owner                = "fuel-tes-dev@twitch.tv"
  service              = "Tesla"
  environment          = "production"
  credential_profile   = "fuel-tes"
  public_subnet_shift  = 4
  private_subnet_shift = 2
  public_subnet_offset = 12
}
