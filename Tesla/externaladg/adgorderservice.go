package externaladg

import (
	"encoding/json"
	"strings"

	"code.justin.tv/commerce/Tesla/config"
	"code.justin.tv/commerce/Tesla/external"
)

// Metrics Constants
const (
	setStatusForReceiptsLatencyMetricName = "DependencyLatency:SetStatusForReceipts"
	setStatusForReceiptsFailureMetricName = "DependencyCallFailure:SetStatusForReceipts"
)

// SetStatusForReceiptsOutputModel is the data model used in the SetStatusForReceipts call.
// It contains a list of the ReceiptID and the results of the attempt to set fulfillment status
// for the vendor
type SetStatusForReceiptsOutputModel struct {
	ProductID string
	ReceiptID string
	Result    string
}

// SetStatusForReceiptsOutput is the output structure returned from the SetStatusForReceipts call.
// It contains a map of receiptIDs to the resulting fulfillment status
type SetStatusForReceiptsOutput struct {
	ReceiptStatusUpdates []SetStatusForReceiptsOutputModel
}

// ReceiptUpdate is the model structure to SetStatusForReceiptsInput
type ReceiptUpdate struct {
	ReceiptID string
	Status    string
}

// SetStatusForReceiptsSigV4Input is the structure provided to the SetStatusForReceiptsSigV4 call.
type SetStatusForReceiptsSigV4Input struct {
	RequestID      string
	Tuid           string
	ClientID       string
	ReceiptUpdates []ReceiptUpdate
}

////////////////////////
// ADG Model Section ///
type adgReceiptModel struct {
	product       adgProductModel
	receiptID     string
	requestResult string
	valid         bool
}

type adgReceipt struct {
	ReceiptID *string `json:"receiptId"`
	Status    *string `json:"status"` //DELIVERED,FULFILLED,CANNOT_FULFILL
}

// This is what were calling ADG with as the input body
type adgSetReceiptsFulfillmentInputModel struct {
	Client         *adgClientModel   `json:"client"`
	Customer       *adgCustomerModel `json:"customer"`
	ReceiptUpdates []adgReceipt      `json:"receiptUpdates"`
}

type tProxSetReceiptsFulfillmentStatusModel struct {
	Request   *adgSetReceiptsFulfillmentInputModel `json:"SetReceiptsFulfillmentStatusRequest"`
	ClientID  *string                              `json:"clientID"`
	RequestID *string                              `json:"requestID"`
}

// ADGOrderServiceInterface is our interface to ADG order service.
type ADGOrderServiceInterface interface {
	SetStatusForReceiptsSigV4(SetStatusForReceiptsSigV4Input) (SetStatusForReceiptsOutput, external.Error)
}

type adgOrderService struct {
	callFactory   external.CallFactory
	configuration *config.TeslaConfiguration
}

func (a adgOrderService) createRequestFromSigV4Input(i SetStatusForReceiptsSigV4Input) external.Request {
	clientID := "Tesla"
	callURL := "https://twitch-proxy.amazon.com/adg/"
	callMethod := "POST"
	custID := "amzn1.twitch." + i.Tuid
	headers := make(map[string]string)
	headers["Content-Type"] = "application/json; charset=UTF-8"
	headers["X-Requested-With"] = "XMLHttpRequest"
	headers["X-Amz-Target"] = "com.amazonaws.twitchproxyservice.TwitchProxyService.SetReceiptsFulfillmentStatus"
	headers["x-amzn-RequestId"] = i.RequestID
	headers["x-amz-client-id"] = i.ClientID

	// convert input to expected ADG modeled Payloads
	updates := make([]adgReceipt, 0)
	for _, receiptUpdate := range i.ReceiptUpdates {
		receiptID := receiptUpdate.ReceiptID
		status := receiptUpdate.Status
		adgReceiptUpdate := adgReceipt{&receiptID, &status}
		updates = append(updates, adgReceiptUpdate)
	}

	model := tProxSetReceiptsFulfillmentStatusModel{Request: &adgSetReceiptsFulfillmentInputModel{Client: &adgClientModel{ClientID: &clientID, Properties: &adgClientProperties{TwitchClientID: &i.ClientID}}, Customer: &adgCustomerModel{ID: &custID}, ReceiptUpdates: updates}, ClientID: &i.ClientID, RequestID: &i.RequestID}
	request := external.NewSigV4SerializableRequest(callMethod, callURL, headers, model, a.configuration.GetTProxSigV4Signer(), a.configuration.GetTProxSigV4ServiceName(), a.configuration.GetTProxSigV4AwsRegion())
	return request
}

// Parses Output from ADG and creates response types to return to vendors
// What we care about from response:
// { "receipt": { "receiptId": "string", requestResult": "OrderingRequestResult" }
func (a adgOrderService) createOutputFromResponse(r external.Response) (SetStatusForReceiptsOutput, external.Error) {
	var data map[string]interface{}
	var receipts []interface{}
	serviceError := a.determineError(r)
	if serviceError != nil {
		return SetStatusForReceiptsOutput{}, serviceError
	}

	if err := json.Unmarshal(r.GetBody(), &data); err != nil {
		return SetStatusForReceiptsOutput{}, external.NewInternalServerError("There was an error deserializing the ADG response : " + err.Error())
	}

	receipts = getListFromMapOrEmpty(data, goodReceiptsParam)
	resultReceipts := make([]SetStatusForReceiptsOutputModel, 0)
	for i := range receipts {
		// convert to receipt by parsing out 2 strings 'result' and 'receipt'
		receipt := convertToReceipt(receipts[i].(map[string]interface{}))
		resultReceipts = append(resultReceipts, SetStatusForReceiptsOutputModel{ReceiptID: receipt.receiptID, Result: receipt.requestResult, ProductID: receipt.product.id})
	}

	return SetStatusForReceiptsOutput{ReceiptStatusUpdates: resultReceipts}, nil
}

func (a adgOrderService) determineError(r external.Response) external.Error {
	var data map[string]interface{}
	codeError := getErrorFromCode(r.GetStatusCode())
	if codeError != nil {
		return codeError
	}

	// Errors from ADGES sometimes have a 200 response, but will have specific __type
	// values based on the errors they can return from their model.
	responseType := getStringFromMapOrEmpty(data, "__type")
	errorSuffixes := [4]string{internalFailureSuffix, exceptionSuffix, errorSuffix, failureSuffix}
	badRequestSuffixes := [2]string{invalidRequestSuffix, invalidParameterSuffix}
	for _, errorSuffix := range errorSuffixes {
		if strings.HasSuffix(responseType, errorSuffix) {
			return external.NewInternalServerError("The external service responded with a " + errorSuffix)
		}
	}
	for _, badRequestSuffix := range badRequestSuffixes {
		if strings.HasSuffix(responseType, badRequestSuffix) {
			return external.NewBadRequestError("The external service responded with a " + badRequestSuffix)
		}
	}

	return nil
}

// SetStatusForReceiptsSigV4 : TPROX setReceiptsFulfillmentStatus API Wrapper
func (a adgOrderService) SetStatusForReceiptsSigV4(i SetStatusForReceiptsSigV4Input) (SetStatusForReceiptsOutput, external.Error) {
	request := a.createRequestFromSigV4Input(i)

	// We now have the request, get the call we should use for this request, and call it.
	call := a.callFactory.CreateCall(request)
	response, err := call.Call(request)
	if err != nil {
		return SetStatusForReceiptsOutput{}, err
	}

	// Now that we have the abstract call response, we convert it into something
	// slightly more legible.
	output, err := a.createOutputFromResponse(response)
	return output, err
}

func convertToReceipt(receipt map[string]interface{}) adgReceiptModel {
	ret := adgReceiptModel{}
	itemMap := getMapFromMapOrEmpty(receipt, receiptsItemParam)
	productMap := getMapFromMapOrEmpty(itemMap, itemProductParam)
	ret.product = convertToProduct(productMap)
	ret.receiptID = getStringFromMapOrEmpty(receipt, goodReceiptIDParam)
	ret.requestResult = getStringFromMapOrEmpty(receipt, goodRequestResultParam)
	return ret
}

// NewADGOrderService returns an adg order service interface that
// can be used to call ADG.
func NewADGOrderService(apiContext external.APIContext, clientMetricName string) ADGOrderServiceInterface {
	metricNames := external.MetricNames{LatencyName: setStatusForReceiptsLatencyMetricName, FailureName: setStatusForReceiptsFailureMetricName}
	metricContext := external.MetricContext{APIContext: apiContext, ClientMetricName: clientMetricName, MetricNames: metricNames}
	factory := external.MetricsCallFactory{MetricContext: metricContext}
	return adgOrderService{callFactory: factory, configuration: apiContext.Configuration}
}
