package externaladg

import (
	"code.justin.tv/commerce/Tesla/external"
)

// ADG Common models
type adgProductModel struct {
	id  string
	sku string
}

type adgGoodModel struct {
	product          adgProductModel
	receiptID        string
	transactionState string
	state            string
}

type adgClientModel struct {
	ClientID   *string              `json:"clientId"`
	Properties *adgClientProperties `json:"properties"`
}

type adgClientProperties struct {
	TwitchClientID *string `json:"twitchClientId"`
}

type adgCustomerModel struct {
	ID *string `json:"id"`
}

// FulfillmentInstruction represents the next step to be made on a sku, with regards
// to fullfilment
type FulfillmentInstruction string

// FulfillmentResult is the response type for updating fulfillment status
type FulfillmentResult string

const (
	// Fulfill : The item should be fulfilled.
	Fulfill FulfillmentInstruction = "FULFILL"
	// Revoke : The item should be revoked.
	Revoke FulfillmentInstruction = "REVOKE"
	// Noop : Nothing needs to be done.
	Noop FulfillmentInstruction = "NOOP"
	// Unknown : It is not known what needs to be done next.
	Unknown FulfillmentInstruction = "UNKNOWN"
	// SuccessModifiedRequest : Successfully updated state but state was different than expected
	SuccessModifiedRequest FulfillmentResult = "SuccessModifiedRequest"
	// Success : Successfully set the state/status
	Success FulfillmentResult = "Success"
	// Error : Unable to update the fulfillment status
	Error FulfillmentResult = "Error"
)

const (
	internalFailureSuffix  string = "InternalFailure"
	exceptionSuffix        string = "Exception"
	errorSuffix            string = "Error"
	failureSuffix          string = "Failure"
	invalidRequestSuffix   string = "InvalidRequestException"
	invalidParameterSuffix string = "InvalidParameterException"
)

const (
	productIDParam  string = "id"
	productSKUParam string = "sku"

	receiptsItemParam string = "item"

	itemProductParam string = "product"

	goodProductParam          string = "product"
	goodReceiptIDParam        string = "receiptId"
	goodTransactionStateParam string = "transactionState"
	goodStateParam            string = "state"
	goodRequestResultParam    string = "requestResult"
	goodReceiptsParam         string = "receipts"
)

func (e FulfillmentInstruction) String() string {
	switch e {
	case Fulfill:
		return "FULFILL"
	case Revoke:
		return "REVOKE"
	case Noop:
		return "NOOP"
	case Unknown:
		return "UNKNOWN"
	default:
		return ""
	}
}

func getFulfillmentInstructionFromState(state string) FulfillmentInstruction {
	switch state {
	case "ORIGIN", "DELIVERED", "PENDING_DELIVERY", "DELIVERY_ATTEMPTED", "RESTORED", "SHARED":
		return Fulfill
	case "CANCELLED", "CANCELLED_BACKFILL", "TRANSFERRED", "REVOKED", "UNSHARED", "DELETED":
		return Revoke
	case "EXPIRED", "REFILLED", "RENEWED", "FULFILLED", "FULFILLED_NO_ACK", "FULFILLED_BACKFILL", "CANNOT_FULFILL", "CONSUMED", "LOANED":
		return Noop
	default:
		return Unknown
	}
}

func getMapFromMapOrEmpty(m map[string]interface{}, key string) map[string]interface{} {
	v, exists := m[key]
	var defaultMap map[string]interface{}
	defaultMap = make(map[string]interface{})

	// Key does not exist in map, return empty map.
	if !exists {
		return defaultMap
	}

	switch v.(type) {
	case (map[string]interface{}):
		return v.(map[string]interface{})
	default:
		// Key in map is not a map, so return default map.
		return defaultMap
	}
}

func getListFromMapOrEmpty(m map[string]interface{}, key string) []interface{} {
	v, exists := m[key]
	var defaultList []interface{}

	// Key does not exist in map, return empty object list.
	if !exists {
		return defaultList
	}

	switch v.(type) {
	case ([]interface{}):
		return v.([]interface{})
	default:
		// Value is not a list, return empty object list.
		return defaultList
	}
}

func getStringFromMapOrEmpty(m map[string]interface{}, key string) string {
	v, exists := m[key]

	// Key does not exist in map, return empty string.
	if !exists {
		return ""
	}

	switch v.(type) {
	case string:
		return v.(string)
	default:
		// Value is not a string, return empty string.
		return ""
	}
}

func getErrorFromCode(code int) external.Error {
	if code < 300 {
		return nil // Strictly below 300 are all successes.
	}

	// At this point we know that code is at least greater or equal to 300
	if code < 400 {
		return external.NewServiceUnavailableError("The service is currently unavailable.") // All 300s are represented as 503s
	}

	if code < 500 {
		switch code {
		case 401:
			return external.NewUnauthorizedError("You are not autorized to access this API.") // 401
		case 403:
			return external.NewForbiddenError("You are forbidden from accessing this API.") // 403
		case 404:
			return external.NewNotFoundError("The API could not be found.") // 404
		default:
			return external.NewBadRequestError("The request was invalid.") // 400
		}
	}

	// Now we just handle the 500s
	switch code {
	case 502:
		return external.NewBadGatewayError("A bad gateway was encountered while making the request.") // 502
	case 503:
		return external.NewServiceUnavailableError("The service is currently unavailable.") // 503
	default:
		return external.NewInternalServerError("An internal error occured.") // 500
	}
}

// Conversion method. Takes in a deserialized map meant to represent a good
// and turns it into a good object.
func convertToGood(good map[string]interface{}) adgGoodModel {
	var product map[string]interface{}
	ret := adgGoodModel{}
	ret.receiptID = getStringFromMapOrEmpty(good, goodReceiptIDParam)

	fetchedProduct, exists := good[goodProductParam]
	if !exists {
		product = make(map[string]interface{})
	} else {
		switch fetchedProduct.(type) {
		case map[string]interface{}:
			product = fetchedProduct.(map[string]interface{})
		default:
			product = make(map[string]interface{})
		}
	}

	ret.product = convertToProduct(product)
	ret.transactionState = getStringFromMapOrEmpty(good, goodTransactionStateParam)
	ret.state = getStringFromMapOrEmpty(good, goodStateParam)
	return ret
}

func convertToProduct(product map[string]interface{}) adgProductModel {
	return adgProductModel{sku: getStringFromMapOrEmpty(product, productSKUParam), id: getStringFromMapOrEmpty(product, productIDParam)}
}

// GetStatusFromInstruction - 3P Vendor will pass in the string instruction/action
// that they took Convert this back to a FulfillmentStatus that ADG will understand
func GetStatusFromInstruction(instruction string) (string, external.Error) {
	switch instruction {
	case "FULFILL":
		return "FULFILLED", nil
	case "REVOKE":
		return "CANNOT_FULFILL", nil
	default:
		// if we get bad input instruction or unknown, don't call ADG
		return "NOOP", external.NewBadRequestError("The last instruction was not valid, must be in [FULFILL] Cannot update receipt status.") // 400
	}
}
