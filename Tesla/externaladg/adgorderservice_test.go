package externaladg

import (
	"encoding/json"
	"testing"

	"code.justin.tv/commerce/Tesla/config"
	"code.justin.tv/commerce/Tesla/external"
)

func TestCreateRequestFromSigV4Input(t *testing.T) {
	tuid := "aTuid"
	clientID := "aClientID"
	requestID := "aRequestID"
	apiName := "aSomeAPIName"
	testReceipt := ReceiptUpdate{ReceiptID: "c7cca5b9-b12c-41c7-a3c7-13872e17facd", Status: "FULFILLED"}
	receiptUpdates := make([]ReceiptUpdate, 0)
	receiptUpdates = append(receiptUpdates, testReceipt)
	input := SetStatusForReceiptsSigV4Input{RequestID: requestID, Tuid: tuid, ClientID: clientID, ReceiptUpdates: receiptUpdates}

	configuration := config.GetTestConfiguration()
	apiContext := external.APIContext{Configuration: configuration, MethodName: apiName, ClientID: clientID}
	metricNames := external.MetricNames{LatencyName: setStatusForReceiptsLatencyMetricName, FailureName: setStatusForReceiptsFailureMetricName}
	metricContext := external.MetricContext{APIContext: apiContext, MetricNames: metricNames}
	factory := external.MetricsCallFactory{MetricContext: metricContext}

	service := adgOrderService{factory, configuration}
	request := service.createRequestFromSigV4Input(input)

	VerifyExistsInMap(t, request.GetHeaders(), "Content-Type", "application/json; charset=UTF-8", "TestCreateRequestFromSigV4Input::")
	VerifyExistsInMap(t, request.GetHeaders(), "X-Requested-With", "XMLHttpRequest", "TestCreateRequestFromSigV4Input::")
	VerifyExistsInMap(t, request.GetHeaders(), "X-Amz-Target", "com.amazonaws.twitchproxyservice.TwitchProxyService.SetReceiptsFulfillmentStatus", "TestCreateRequestFromSigV4Input::")
	VerifyExistsInMap(t, request.GetHeaders(), "x-amzn-RequestId", requestID, "TestCreateRequestFromSigV4Input::")
	VerifyExistsInMap(t, request.GetHeaders(), "x-amz-client-id", clientID, "TestCreateRequestFromSigV4Input::")

	if request.GetCallMethod() != "POST" {
		t.Error("TestCreateRequestFromSigV4Input::Call method should be POST")
	}
}

func TestConvertToReceipt(t *testing.T) {
	testReceiptJSON := []byte(`{"item":{"product":{"id":"theID"}}, "receiptId": "c7cca5b9-b12c-41c7-a3c7-13872e17facd", "requestResult": "Success"}`)
	var testReceiptMap map[string]interface{}
	if err := json.Unmarshal(testReceiptJSON, &testReceiptMap); err != nil {
		t.Error("Test Setup Failure: Unable to create test setup")
	}
	receipt := convertToReceipt(testReceiptMap)
	if receipt.product.id != "theID" {
		t.Error("TestConvertToReceipt:: Unexpected product id. Expected theID but got : " + receipt.product.id)
	}

	if receipt.receiptID != "c7cca5b9-b12c-41c7-a3c7-13872e17facd" {
		t.Error("Unexpected receiptID. Got " + receipt.receiptID + " but expected c7cca5b9-b12c-41c7-a3c7-13872e17facd")
	}
	if receipt.requestResult != "Success" {
		t.Error("Unexpected requestResult. Got " + receipt.requestResult + " but expected Sucess")
	}
}
