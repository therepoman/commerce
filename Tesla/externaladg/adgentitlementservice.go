package externaladg

import (
	"encoding/json"
	"strings"

	"code.justin.tv/commerce/Tesla/config"
	"code.justin.tv/commerce/Tesla/external"
)

// Metric Constants
const (
	getGoodsForUserLatencyMetricName = "DependencyLatency:GetGoodsForUser"
	getGoodsForUserFailureMetricName = "DependencyCallFailure:GetGoodsForUser"
)

// GetGoodsForUserOutputModel is the data model used in the GetGoodsForUser call.
// It contains the receiptID, sku, and the next step needed to be done with regards
// to fulfillment of the item.
type GetGoodsForUserOutputModel struct {
	ReceiptID       string
	NextInstruction FulfillmentInstruction
	SKU             string
}

// GetGoodsForUserOutput is the output structure returned from the GetGoodsForUser call.
type GetGoodsForUserOutput struct {
	Goods []GetGoodsForUserOutputModel
}

// GetGoodsForUserSigV4Input is the structure provided to the GetGoodsForUser call
// when calling through sigV4.
type GetGoodsForUserSigV4Input struct {
	RequestID string
	Tuid      string
	ClientID  string
}

type adgSyncPointModel struct {
	SyncToken *string `json:"syncToken"`
	SyncPoint *uint64 `json:"syncPoint"`
}

type adgSyncGoodsInputModel struct {
	Client    *adgClientModel    `json:"client"`
	Customer  *adgCustomerModel  `json:"customer"`
	SyncPoint *adgSyncPointModel `json:"startSync"`
}

type tProxSyncGoodsRequestModel struct {
	Request   *adgSyncGoodsInputModel `json:"SyncGoodsRequest"`
	ClientID  *string                 `json:"clientID"`
	RequestID *string                 `json:"requestID"`
}

// ADGEntitlementServiceInterface is our interface to ADG entitlement service.
type ADGEntitlementServiceInterface interface {
	GetGoodsForUserSigV4(GetGoodsForUserSigV4Input) (GetGoodsForUserOutput, external.Error)
}

type adgEntitlementService struct {
	callFactory   external.CallFactory
	configuration *config.TeslaConfiguration
}

func (a adgEntitlementService) createRequestFromGetGoodsForUserSigV4Input(i GetGoodsForUserSigV4Input) external.Request {
	adgClientID := "Tesla"
	callURL := "https://twitch-proxy.amazon.com/adg/"
	callMethod := "POST"
	custID := "amzn1.twitch." + i.Tuid
	model := tProxSyncGoodsRequestModel{Request: &adgSyncGoodsInputModel{Client: &adgClientModel{ClientID: &adgClientID, Properties: &adgClientProperties{TwitchClientID: &i.ClientID}}, Customer: &adgCustomerModel{ID: &custID}, SyncPoint: &adgSyncPointModel{}}, ClientID: &i.ClientID, RequestID: &i.RequestID}
	headers := make(map[string]string)
	headers["Content-Type"] = "application/json; charset=UTF-8"
	headers["X-Requested-With"] = "XMLHttpRequest"
	headers["X-Amz-Target"] = "com.amazonaws.twitchproxyservice.TwitchProxyService.SyncGoods"
	headers["x-amzn-RequestId"] = i.RequestID
	headers["x-amz-client-id"] = i.ClientID
	request := external.NewSigV4SerializableRequest(callMethod, callURL, headers, model, a.configuration.GetTProxSigV4Signer(), a.configuration.GetTProxSigV4ServiceName(), a.configuration.GetTProxSigV4AwsRegion())
	return request
}

// Determines whether the deserialized response is an error, and returns the appropriate error interface.
// Returns nil if there is no error.
func (a adgEntitlementService) determineError(code int, data map[string]interface{}) external.Error {
	codeError := getErrorFromCode(code)
	if codeError != nil {
		return codeError
	}

	// Errors from ADGES sometimes have a 200 response, but will have specific __type
	// values based on the errors they can return from their model.
	responseType := getStringFromMapOrEmpty(data, "__type")
	errorSuffixes := [4]string{internalFailureSuffix, exceptionSuffix, errorSuffix, failureSuffix}
	for _, suffix := range errorSuffixes {
		if strings.HasSuffix(responseType, suffix) {
			return external.NewInternalServerError("The external service responded with a " + suffix)
		}
	}

	return nil
}

func (a adgEntitlementService) createOutputFromResponse(r external.Response) (GetGoodsForUserOutput, external.Error) {
	var data map[string]interface{}
	var goods []interface{}
	if err := json.Unmarshal(r.GetBody(), &data); err != nil {
		return GetGoodsForUserOutput{}, external.NewInternalServerError("There was an error deserializing the ADG response : " + err.Error())
	}

	serviceError := a.determineError(r.GetStatusCode(), data)
	if serviceError != nil {
		return GetGoodsForUserOutput{}, serviceError
	}

	goods = data["goods"].([]interface{})

	resultGoods := make([]GetGoodsForUserOutputModel, 0)
	for i := range goods {
		good := convertToGood(goods[i].(map[string]interface{}))
		resultGoods = append(resultGoods, GetGoodsForUserOutputModel{ReceiptID: good.receiptID, SKU: good.product.sku, NextInstruction: getFulfillmentInstructionFromState(good.transactionState)})
	}

	return GetGoodsForUserOutput{Goods: resultGoods}, nil
}

func (a adgEntitlementService) GetGoodsForUserSigV4(i GetGoodsForUserSigV4Input) (GetGoodsForUserOutput, external.Error) {
	request := a.createRequestFromGetGoodsForUserSigV4Input(i)

	// We now have the request, get the call we should use for this request, and call it.
	call := a.callFactory.CreateCall(request)
	response, err := call.Call(request)
	if err != nil {
		return GetGoodsForUserOutput{}, err
	}
	// Now that we have the abstract call response, we convert it into something
	// slightly more legible.
	output, err := a.createOutputFromResponse(response)
	return output, err
}

// NewADGEntitlementService returns an adg entitlement service interface that
// can be used to call ADG.
func NewADGEntitlementService(apiContext external.APIContext, clientMetricName string) ADGEntitlementServiceInterface {
	metricNames := external.MetricNames{LatencyName: getGoodsForUserLatencyMetricName, FailureName: getGoodsForUserFailureMetricName}
	metricContext := external.MetricContext{APIContext: apiContext, ClientMetricName: clientMetricName, MetricNames: metricNames}
	factory := external.MetricsCallFactory{MetricContext: metricContext}
	return adgEntitlementService{callFactory: factory, configuration: apiContext.Configuration}
}
