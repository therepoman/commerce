package externaladg

import (
	"code.justin.tv/commerce/Tesla/external"
)

// ADGOrderServiceMock provides mocked interface to test APIs
type adgOrderServiceStub struct {
	output    SetStatusForReceiptsOutput
	extError  external.Error
	hasError  bool
	callCount int
}

func (a adgOrderServiceStub) GetCallCount() int {
	return a.callCount
}

// SetStatusForReceiptsSigV4 stubbed out interface.
func (a adgOrderServiceStub) SetStatusForReceiptsSigV4(i SetStatusForReceiptsSigV4Input) (SetStatusForReceiptsOutput, external.Error) {
	return SetStatusForReceiptsOutput{}, nil
}

// NewADGOrderServiceStub returns a stubbed out ADGOrderServiceInterface
func NewADGOrderServiceStub(setStatusOuptut SetStatusForReceiptsOutput, externalError external.Error, hasErr bool) ADGOrderServiceInterface {
	return adgOrderServiceStub{output: setStatusOuptut, extError: externalError, hasError: hasErr}
}
