package externaladg

import (
	"net/http"
	"strconv"
	"testing"

	"code.justin.tv/commerce/Tesla/config"
	"code.justin.tv/commerce/Tesla/external"
)

func VerifyExistsInMap(t *testing.T, m map[string]string, key string, expectedValue string, errorPrefix string) {
	if value, valuePresent := m[key]; !valuePresent {
		t.Error(errorPrefix + " Key : " + key + " is absent from the map.")
	} else {
		if value != expectedValue {
			t.Error(errorPrefix + " Key : " + key + " returned an expected value. Got : " + value + " Should be : " + expectedValue)
		}
	}
}

func TestDetermineError(t *testing.T) {
	clientID := "aClientID"
	methodName := "someAPICaller"
	configuration := config.GetTestConfiguration()
	apiContext := external.APIContext{Configuration: configuration, MethodName: methodName, ClientID: clientID}
	metricNames := external.MetricNames{LatencyName: getGoodsForUserLatencyMetricName, FailureName: getGoodsForUserFailureMetricName}
	metricContext := external.MetricContext{APIContext: apiContext, MetricNames: metricNames}
	factory := external.MetricsCallFactory{MetricContext: metricContext}
	service := adgEntitlementService{factory, configuration}
	data := make(map[string]interface{})

	errorCodes := [3]int{http.StatusMultipleChoices, http.StatusNotFound, http.StatusInternalServerError}
	// Regardless of what's in data, status codes >= 300 should return errors.
	for _, code := range errorCodes {
		if service.determineError(code, data) == nil {
			t.Error("TestDetermineError:: status code " + strconv.Itoa(code) + " should return an error.")
		}
	}

	// Now lets put some __type vars to verify that errors are also returned as appropriate.
	typeValues := [4]string{"com.amazon.coral.com#InternalFailure", "someKindOfError", "UnknownException", "some.kind.of#Failure"}
	for _, value := range typeValues {
		data["__type"] = value
		if service.determineError(200, data) == nil {
			t.Error("TestDetermineError:: type value " + value + " should return an error.")
		}
	}

	// Finally, check that a 200 with no type returns no error.
	data["__type"] = "some.legal.type.MyDao"
	if service.determineError(200, data) != nil {
		t.Error("TestDetermineError:: Happy case with __type value returned an error.")
	}

	delete(data, "__type")
	if service.determineError(200, data) != nil {
		t.Error("TestDetermineError:: Happy case with no __type value returned an error.")
	}
}

func TestCreateRequestFromGetGoodsForUserSigV4Input(t *testing.T) {
	tuid := "aTuid"
	clientID := "aClientID"
	requestID := "aRequestID"
	methodName := "someAPICaller"
	input := GetGoodsForUserSigV4Input{RequestID: requestID, Tuid: tuid, ClientID: clientID}
	configuration := config.GetTestConfiguration()
	apiContext := external.APIContext{Configuration: configuration, MethodName: methodName, ClientID: clientID}
	metricNames := external.MetricNames{LatencyName: getGoodsForUserLatencyMetricName, FailureName: getGoodsForUserFailureMetricName}
	metricContext := external.MetricContext{APIContext: apiContext, MetricNames: metricNames}
	factory := external.MetricsCallFactory{MetricContext: metricContext}
	service := adgEntitlementService{factory, configuration}
	request := service.createRequestFromGetGoodsForUserSigV4Input(input)

	VerifyExistsInMap(t, request.GetHeaders(), "Content-Type", "application/json; charset=UTF-8", "TestCreateRequestFromGetGoodsForUserSigV4Input::")
	VerifyExistsInMap(t, request.GetHeaders(), "X-Requested-With", "XMLHttpRequest", "TestCreateRequestFromGetGoodsForUserSigV4Input::")
	VerifyExistsInMap(t, request.GetHeaders(), "X-Amz-Target", "com.amazonaws.twitchproxyservice.TwitchProxyService.SyncGoods", "TestCreateRequestFromGetGoodsForUserSigV4Input::")
	VerifyExistsInMap(t, request.GetHeaders(), "x-amzn-RequestId", requestID, "TestCreateRequestFromGetGoodsForUserSigV4Input::")
	VerifyExistsInMap(t, request.GetHeaders(), "x-amz-client-id", clientID, "TestCreateRequestFromGetGoodsForUserSigV4Input::")

	//TODO: Verify request payload contains tuid.

	if request.GetCallMethod() != "POST" {
		t.Error("TestCreateRequestFromGetGoodsForUserSigV4Input::Call method should be POST")
	}
}
