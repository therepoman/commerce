package externaladg

import (
	"encoding/json"
	"fmt"
	"testing"

	"code.justin.tv/commerce/Tesla/external"
)

type errorCodePair struct {
	testCode          int
	expectedErrorCode int
}

type stateInstructionPair struct {
	State               string
	ExpectedInstruction FulfillmentInstruction
}

type instructionStringPair struct {
	Instruction    FulfillmentInstruction
	ExpectedString string
}

var instructionStringTests = []instructionStringPair{
	{Fulfill, "FULFILL"},
	{Revoke, "REVOKE"},
	{Noop, "NOOP"},
	{Unknown, "UNKNOWN"},
}

var stateInstructionTests = []stateInstructionPair{
	{"ORIGIN", Fulfill},
	{"PENDING_DELIVERY", Fulfill},
	{"DELIVERY_ATTEMPTED", Fulfill},
	{"DELIVERED", Fulfill},
	{"FULFILLED", Noop},
	{"FULFILLED_NO_ACK", Noop},
	{"FULFILLED_BACKFILL", Noop},
	{"CANNOT_FULFILL", Noop},
	{"CANCELLED", Revoke},
	{"CANCELLED_BACKFILL", Revoke},
	{"CONSUMED", Noop},
	{"REFILLED", Noop},
	{"RENEWED", Noop},
	{"EXPIRED", Noop},
	{"TRANSFERRED", Revoke},
	{"LOANED", Noop},
	{"SHARED", Fulfill},
	{"UNSHARED", Revoke},
	{"REVOKED", Revoke},
	{"RESTORED", Fulfill},
	{"DELETED", Revoke},
	{"BLABLABLA", Unknown},
}

var codeToErrorTests = []errorCodePair{
	{300, 503},
	{301, 503},
	{350, 503},
	{399, 503},
	{400, 400},
	{401, 401},
	{402, 400},
	{403, 403},
	{404, 404},
	{405, 400},
	{450, 400},
	{499, 400},
	{500, 500},
	{501, 500},
	{502, 502},
	{503, 503},
	{504, 500},
	{550, 500},
	{600, 500},
}

type instructionStatusPair struct {
	InstructionTaken string
	ExpectedStatus   string
	ExternalError    external.Error
}

var instructionStatusTests = []instructionStatusPair{
	{"FULFILL", "FULFILLED", nil},
	{"REVOKE", "CANNOT_FULFILL", nil},
	{"NOOP", "NOOP", external.NewBadRequestError("The last instruction was not valid, must be in [FULFILL] Cannot update receipt status.")},
	{"UNKNOWN", "NOOP", external.NewBadRequestError("The last instruction was not valid, must be in [FULFILL] Cannot update receipt status.")},
}

func copyMap(m map[string]interface{}) map[string]interface{} {
	cm := make(map[string]interface{})
	for k, v := range m {
		cm[k] = v
	}

	return cm
}

// Takes in a map a returns a list of 3 maps. The first is the map with the given key set to goodValue. The
// second is the map with the given key assigned to errorValue. The last is the map with the given key
// removed (if it existed)
func generateTestMaps(m map[string]interface{}, key string, goodValue interface{}, errorValue interface{}) []map[string]interface{} {
	resultMaps := make([]map[string]interface{}, 0)

	goodValueMap := copyMap(m)
	goodValueMap[key] = goodValue
	resultMaps = append(resultMaps, goodValueMap)

	errorValueMap := copyMap(m)
	errorValueMap[key] = errorValue
	resultMaps = append(resultMaps, errorValueMap)

	absentValueMap := copyMap(m)
	delete(absentValueMap, key)
	resultMaps = append(resultMaps, absentValueMap)
	return resultMaps
}

func TestGetErrorFromCode(t *testing.T) {
	for _, value := range [7]int{100, 150, 199, 200, 202, 250, 299} {
		if getErrorFromCode(value) != nil {
			t.Error("TestGetErrorFromCode:: Unexpected error code when sending ", value, ". Should be nil.")
		}
	}

	for _, value := range codeToErrorTests {
		actual := getErrorFromCode(value.testCode)
		expected := value.expectedErrorCode
		if actual.GetErrorCode() != expected {
			t.Error("TestGetErrorFromCode:: Unexpected error code when sending ", value, " Expected : ", expected, " Got : ", actual.GetErrorCode())
		}
	}
}

func TestFulfillmentInstructionToString(t *testing.T) {
	for _, pair := range instructionStringTests {
		stringValue := pair.Instruction.String()
		if stringValue != pair.ExpectedString {
			t.Error("For", pair.Instruction, "expected", pair.ExpectedString, "got", stringValue)
		}
	}
}

func TestGetFulfillmentInstructionFromState(t *testing.T) {
	for _, stateInstructionPair := range stateInstructionTests {
		instruction := getFulfillmentInstructionFromState(stateInstructionPair.State)
		if instruction != stateInstructionPair.ExpectedInstruction {
			t.Error("For", stateInstructionPair.State, "expected", stateInstructionPair.ExpectedInstruction, "got", instruction)
		}
	}
}

func TestGetMapFromMapOrEmpty(t *testing.T) {
	var theMap map[string]interface{}
	theMap = make(map[string]interface{})
	testKey := "TestKey"
	if len(getMapFromMapOrEmpty(theMap, testKey)) > 0 {
		t.Error("TestGetMapFromMapOrEmpty:: When the key doesn't exist, a non-empty map was returned.")
	}

	theMap[testKey] = "Something that's not a map."
	if len(getMapFromMapOrEmpty(theMap, testKey)) > 0 {
		t.Error("TestGetMapFromMapOrEmpty:: When the key is not a map, a non-empty map was returned.")
	}

	var otherMap map[string]interface{}
	otherMap = make(map[string]interface{})
	expectedOtherValue := "Some value"
	otherMap[testKey] = expectedOtherValue
	theMap[testKey] = otherMap

	fetchedMap := getMapFromMapOrEmpty(theMap, testKey)
	if len(fetchedMap) == 0 {
		t.Error("TestGetMapFromMapOrEmpty:: When the key exists and is a map, an empty map was returned.")
	} else {
		if fetchedMap[testKey] != expectedOtherValue {
			t.Error("TestGetMapFromMapOrEmpty:: The fetched map does not have the expected value.")
		}
	}
}

func TestGetListFromMapOrEmpty(t *testing.T) {
	testInputJSON := []byte(`{"receiptId": "fb3fc2f9-e0cb-4b96-a003-f6ccb268a0e3", "status": "FULFILLED"}`)
	var testInput map[string]interface{}
	if err := json.Unmarshal(testInputJSON, &testInput); err != nil {
		fmt.Println("Test Setup Failure: Unable to create test setup")
		return
	}
	testListJSON := []byte(`{"receipts":[{"receiptId": "fb3fc2f9-e0cb-4b96-a003-f6ccb268a0e3", "status": "FULFILLED"}]}`)
	var testListInput map[string]interface{}
	if err := json.Unmarshal(testListJSON, &testListInput); err != nil {
		fmt.Println("Test Setup Failure: Unable to create test setup")
		return
	}

	shouldBeEmpty := getListFromMapOrEmpty(testInput, goodReceiptsParam)
	if len(shouldBeEmpty) != 0 {
		t.Error("For TestGetListFromMapOrEmpty", "expected []", "got", shouldBeEmpty)
	}
	shouldNotBeEmpty := getListFromMapOrEmpty(testListInput, goodReceiptsParam)
	if len(shouldNotBeEmpty) == 0 {
		t.Error("For TestGetListFromMapOrEmpty", "expected", testListInput, "got", shouldNotBeEmpty)
	}
}

func TestGetStringFromMapOrEmpty(t *testing.T) {
	existingKey := "SOME KEY THAT EXISTS"
	existingValue := "THE KEY TO THE CITY"
	nonStringKey := "THIS IS NOT A STRING"
	nonStringValue := 500
	unexistingKey := "THERE IS NO SPOON"

	m := make(map[string]interface{})
	m[existingKey] = existingValue
	m[nonStringKey] = nonStringValue

	if getStringFromMapOrEmpty(m, existingKey) != existingValue {
		t.Error("TestGetStringFromMapOrEmpty:: Unexpected value for key with string value.")
	}

	if getStringFromMapOrEmpty(m, nonStringKey) != "" {
		t.Error("TestGetStringFromMapOrEmpty:: Unexpected value for key with non-string value.")
	}

	if getStringFromMapOrEmpty(m, unexistingKey) != "" {
		t.Error("TestGetStringFromMapOrEmpty:: Unexpected value for non-existing key.")
	}
}

func TestConvertToGood(t *testing.T) {
	badValue := 55
	expectedReceiptID := "RE-SEAT MY ID"
	expectedSKU := "SKUBBA DIVING"
	expectedState := "STATE OF THE UNION"
	expectedTransactionState := "TRANS-ACT STATE"

	product := make(map[string]interface{})
	product[productSKUParam] = expectedSKU

	good := make(map[string]interface{})
	good[goodReceiptIDParam] = expectedReceiptID
	good[goodTransactionStateParam] = expectedTransactionState
	good[goodStateParam] = expectedState
	good[goodProductParam] = product

	// Test that all possible receiptID values are handled.
	expectedReceiptIDValues := [3]string{expectedReceiptID, "", ""}
	for index, m := range generateTestMaps(good, goodReceiptIDParam, expectedReceiptID, badValue) {
		actualReceiptID := convertToGood(m).receiptID
		if actualReceiptID != expectedReceiptIDValues[index] {
			t.Error("TestConvertToGood:: For index ", index, ", unexpected receipt ID. Expected : ", expectedReceiptIDValues[index], " Got : ", actualReceiptID)
		}
	}

	// Test that all possible transactionState values are handled.
	expectedTransactionStateValues := [3]string{expectedTransactionState, "", ""}
	for index, m := range generateTestMaps(good, goodTransactionStateParam, expectedTransactionState, badValue) {
		actualTransactionState := convertToGood(m).transactionState
		if actualTransactionState != expectedTransactionStateValues[index] {
			t.Error("TestConvertToGood:: For index ", index, ", unexpected transactionState. Expected : ", expectedTransactionStateValues[index], " Got : ", actualTransactionState)
		}
	}

	// Test that all possible state values are handled.
	expectedStateValues := [3]string{expectedState, "", ""}
	for index, m := range generateTestMaps(good, goodStateParam, expectedState, badValue) {
		actualState := convertToGood(m).state
		if actualState != expectedStateValues[index] {
			t.Error("TestConvertToGood:: For index ", index, ", unexpected state. Expected : ", expectedStateValues[index], " Got : ", actualState)
		}
	}

	// Test that all possible SKU values are handled.
	expectedSKUValues := [3]string{expectedSKU, "", ""}
	for index, m := range generateTestMaps(product, productSKUParam, expectedSKU, badValue) {
		// Set the product in the good to the generated product
		good[goodProductParam] = m
		actualSKU := convertToGood(good).product.sku
		if actualSKU != expectedSKUValues[index] {
			t.Error("TestConvertToGood:: For index ", index, ", unexpected sku. Expected : ", expectedSKUValues[index], " Got : ", actualSKU)
		}
		// Reset to original product, so that further tests are not affected.
		good[goodProductParam] = product
	}

	// Finally, test that if the product is the wrong type or absent altogether that everything is ok.
	good[goodProductParam] = "something that's not a map"
	actualSKU := convertToGood(good).product.sku
	if actualSKU != "" {
		t.Error("TestConvertToGood:: Unexpected SKU returned when providing an imporperly typed product. Expected : " + "" + " Got : " + actualSKU)
	}

	delete(good, goodProductParam)
	actualSKU = convertToGood(good).product.sku
	if actualSKU != "" {
		t.Error("TestConvertToGood:: Unexpected SKU returned when providing an absent product. Expected : " + "" + " Got : " + actualSKU)
	}
}

func TestConvertToProduct(t *testing.T) {
	badValue := 55
	expectedSKU := "SKUBBA DIVING"
	expectedID := "I D SIDE WAT THE ID IS"

	product := make(map[string]interface{})
	product[productSKUParam] = expectedSKU
	product[productIDParam] = expectedID

	// Test that all possible SKU values are handled.
	expectedSKUValues := [3]string{expectedSKU, "", ""}
	for index, m := range generateTestMaps(product, productSKUParam, expectedSKU, badValue) {
		// Set the product in the good to the generated product
		actualSKU := convertToProduct(m).sku
		if actualSKU != expectedSKUValues[index] {
			t.Error("TestConvertToGood:: For index ", index, ", unexpected sku. Expected : ", expectedSKUValues[index], " Got : ", actualSKU)
		}
	}

	// Test that all possible ID values are handled.
	expectedIDValues := [3]string{expectedID, "", ""}
	for index, m := range generateTestMaps(product, productIDParam, expectedID, badValue) {
		// Set the product in the good to the generated product
		actualSKU := convertToProduct(m).id
		if actualSKU != expectedIDValues[index] {
			t.Error("TestConvertToGood:: For index ", index, ", unexpected id. Expected : ", expectedIDValues[index], " Got : ", actualSKU)
		}
	}
}

func TestGetStatusFromInstruction(t *testing.T) {
	for _, instructionStatusPair := range instructionStatusTests {
		status, extError := GetStatusFromInstruction(instructionStatusPair.InstructionTaken)
		if extError != nil && extError != instructionStatusPair.ExternalError {
			t.Error("For", instructionStatusPair.InstructionTaken, "expected", instructionStatusPair.ExternalError, "got", extError)
		} else if status != instructionStatusPair.ExpectedStatus {
			t.Error("For", instructionStatusPair.InstructionTaken, "expected", instructionStatusPair.ExpectedStatus, "got", status)
		}
	}
}
