package externalses

import (
	"code.justin.tv/commerce/Tesla/config"
	"code.justin.tv/commerce/Tesla/external"
	"encoding/json"
	v4 "github.com/aws/aws-sdk-go/aws/signer/v4"
	"github.com/stretchr/testify/assert"
	"net/http/httputil"
	"strings"
	"testing"
)

var ses samusEntitlementService

var responseBody = `{"entitlements":[{
            "createdDate": "2019-12-02T17:24:41.402Z",
            "entitlementAccounts": [
            {
                "accountId": "468790947",
                "accountType": "TWITCH"
            },
            {
                "accountId": "A1JIGSWROO5NHU",
                "accountType": "AMAZON"
            }],
            "entitlementId": "amzn1.spid.ffa9a2f8-954a-427e-b52a-0b11cc2f334c_6f54b708-889f-4d61-bbbb-aadbcd81244f",
            "entitlementStatus": "ENTITLED",
            "entitlementStatusHistory": [
            {
                "entitlementStatus": "ENTITLED",
                "timeStamp": "2019-12-02T17:24:41.402Z"
            }],
            "fulfillmentStatusHistory": [],
            "lastUpdatedDate": "2019-12-02T17:24:41.505Z",
            "metadata":
            {
                "EntityType": "OFFER",
                "OfferId": "4ab7548a-ac39-3713-ba45-5c63975725f3",
                "UserCountryCode": "US",
                "OfferGroup": "FGWP",
                "MarketplaceId": "ATVPDKIKX0DER",
                "SOSContentId": "f6b7548a-ac1e-71a4-6908-4b60a264d221",
                "SOSEntitlementFulfillmentStatus": "finalized",
                "ProductInstances": "[{\"productInstanceIdType\":\"adg_good_id\",\"productInstanceId\":\"04b763d2-489b-aed3-7bef-5de0a97b4439\",\"productId\":\"ZU2A7/toejam_earl_groove_game\",\"account\":{\"accountType\":\"TWITCH\",\"accountId\":\"468790947\"}},{\"productInstanceIdType\":\"adg_good_id\",\"productInstanceId\":\"24b763d2-489b-d4dc-3480-12a48343cef6\",\"productId\":\"ZU2A7/toejam_earl_groove_game\",\"account\":{\"accountType\":\"AMAZON\",\"accountId\":\"A1JIGSWROO5NHU\"}}]",
                "ADGGoods": "[{\"vendor\":\"ZU2A7\",\"sku\":\"toejam_earl_groove_game\",\"goodId\":\"24b763d2-489b-d4dc-3480-12a48343cef6\",\"account\":{\"accountType\":\"AMAZON\",\"accountId\":\"A1JIGSWROO5NHU\"},\"vendorSkuString\":\"ZU2A7/toejam_earl_groove_game\"},{\"vendor\":\"ZU2A7\",\"sku\":\"toejam_earl_groove_game\",\"goodId\":\"04b763d2-489b-aed3-7bef-5de0a97b4439\",\"account\":{\"accountType\":\"TWITCH\",\"accountId\":\"468790947\"},\"vendorSkuString\":\"ZU2A7/toejam_earl_groove_game\"}]",
                "OriginPageUri": "UNKNOWN"
            },
            "product":
            {
                "destinations": ["SamusFulfillmentService"],
                "metadata":
                {},
                "productId": "amzn1.spid.ffa9a2f8-954a-427e-b52a-0b11cc2f334c",
                "productName": "ToeJam & Earl 2019 FGWP"
            },
            "relatedAccounts": [
            {
                "accountId": "A1JIGSWROO5NHU",
                "accountType": "AMAZON_CONSUMER"
            }]
        }]}`

var errorResponseBody = `{
			"__type": "com.amazon.coral.validate#ValidationException",
			"message": "4 validation errors detected: Value 'Rufus' at 'entitlementAccount.accountType' failed to satisfy constraint: Member must satisfy enum value set: [AMAZON_GIFTSENDER, RARE, PSYONIX, AMAZON, UBISOFT, BUNGIE, TWITCH_GIFTSENDER, AMAZON_CONSUMER, TWITCH, BLIZZARD, NINTENDO, TOMBSTONE, GEARBOX, RIOT, MOCKED_VENDOR, ACTIVISION]; Value 'Rufus' at 'metadataFilters.1.member.metadataFilterRule.type' failed to satisfy constraint: Member must satisfy enum value set: [CONTAINS]; Value 'Rufus' at 'metadataFilters.2.member.metadataFilterRule.type' failed to satisfy constraint: Member must satisfy enum value set: [CONTAINS]; Value 'Rufus' at 'metadataFilters.3.member.metadataFilterRule.type' failed to satisfy constraint: Member must satisfy enum value set: [CONTAINS]"
		}`

var serializeableRequestForGetAccount = `external.serializableRequest( 
   external.serializableRequest   { 
      input:externalses.tProxGetAccountEntitlementsRequestModel{ 
         Request:(*externalses.GetAccountEntitlementsRequestModel)(0xc000013200),
         ClientID:(*string)(0xc00005ea40),
         RequestID:(*string)(0xc00005ea20)
      },
      BaseRequest:external.BaseRequest{ 
         callMethod:"POST",
         callURL:"https://twitch-proxy.amazon.com/samus/",
         headers:map[ 
            string
         ]         string{ 
            "Content-Type":"application/json; charset=UTF-8",
            "X-Amz-Target":"com.amazonaws.twitchproxyservice.TwitchProxyService.GetAccountEntitlements",
            "X-Requested-With":"XMLHttpRequest",
            "x-amz-client-id":"clientId",
            "x-amzn-RequestId":"requestId"
         }
      }
   }
)`

var byteResponseBody = []byte(responseBody)
var byteErrorResponseBody = []byte(errorResponseBody)

const contains = "CONTAINS"
const adgGoods = "ADGGoods"
const productInstances = "ProductInstances"

func testNewGetSigV4SerializableRequest(m string, u string, h map[string]string, i interface{}, signer *v4.Signer, service string, region string) external.Request {
	return external.NewSerializableRequestWithModel(m, u, h, i)
}

func TestUnmarshalToEntitlements(t *testing.T) {
	var getAccountEntitlementsOutput GetAccountEntitlementsOutput

	if err := json.Unmarshal(byteResponseBody, &getAccountEntitlementsOutput); err != nil {
		println(err.Error())
		t.Error("Failed to unmarshal to entitlement")
	}
}

func TestCreateRequestFromGetAccountEntitlementsSigV4Input(t *testing.T) {
	var input GetAccountEntitlementsSigV4Input
	var metadataFilters []MetadataFilter

	var adgGoodsFilterRule MetadataFilterRule
	adgGoodsFilterRule.MetadataFilterRuleType = contains
	adgGoodsFilterRule.MetadataFilterRuleValue = input.RequestID

	var adgGoodsFilter MetadataFilter
	adgGoodsFilter.MetadataKey = adgGoods
	adgGoodsFilter.MetadataFilterRule = adgGoodsFilterRule

	var productInstancesFilterRule MetadataFilterRule
	productInstancesFilterRule.MetadataFilterRuleType = contains
	productInstancesFilterRule.MetadataFilterRuleValue = input.RequestID

	var productInstancesFilter MetadataFilter
	productInstancesFilter.MetadataKey = productInstances
	productInstancesFilter.MetadataFilterRule = productInstancesFilterRule

	metadataFilters = append(metadataFilters, adgGoodsFilter)
	metadataFilters = append(metadataFilters, productInstancesFilter)

	teslaConfig := config.TeslaConfiguration{}
	ses.configuration = &teslaConfig

	input.RequestID = "requestId"
	input.ProductIds = make([]string, 0)
	input.MetaDataFilters = metadataFilters
	input.Tuid = "tuid"
	input.ClientID = "clientId"

	out := ses.createRequestFromGetAccountEntitlementsSigV4Input(input, NewGetSigV4SerializableRequest(testNewGetSigV4SerializableRequest))
	httpExpected := `POST /samus/ HTTP/1.1
					Host: twitch-proxy.amazon.com
					Content-Encoding: amz-1.0
					Content-Type: application/json; charset=UTF-8
					User-Agent: TwitchEntitlementService
					X-Amz-Client-Id: clientId
					X-Amz-Target: com.amazonaws.twitchproxyservice.TwitchProxyService.GetAccountEntitlements
					X-Amzn-Requestid: requestId
					X-Requested-With: XMLHttpRequest
					
					{"GetAccountEntitlementsRequest":{"entitlementAccount":{"accountId":"tuid","accountType":"TWITCH"},"productIds":[],"filterCleared":true,"metadataFilters":[{"metadataKey":"ADGGoods","metadataFilterRule":{"type":"CONTAINS","value":""}},{"metadataKey":"ProductInstances","metadataFilterRule":{"type":"CONTAINS","value":""}}]},"clientID":"clientId","requestID":"requestId"}`

	httpExpected = strings.Join(strings.Fields(httpExpected), "")

	httpRequest, _ := out.ToHTTPRequest()

	// Save a copy of this request for debugging.
	httpRequestActualByte, err := httputil.DumpRequest(httpRequest, true)
	if err != nil {
		assert.Fail(t, err.Error())
	}

	httpRequestActual := strings.Join(strings.Fields(string(httpRequestActualByte)), "")

	assert.Equal(t, httpExpected, string(httpRequestActual), "Should be the same.")
}

func TestCreateRequestForUpdateFulfillmentStatus(t *testing.T) {
	fulfillmentStatusObject := FulfillmentStatusObject{
		EntitlementId:     "entitlementId",
		FulfillmentStatus: "fulfillmentStatus",
	}
	fulfillmentStatusObjects := make([]FulfillmentStatusObject, 0)
	fulfillmentStatusObjects = append(fulfillmentStatusObjects, fulfillmentStatusObject)

	out := ses.createRequestFromUpdateFulfillmentStatusSigV4Input("clientId", "RequestId", fulfillmentStatusObjects, NewGetSigV4SerializableRequest(testNewGetSigV4SerializableRequest))
	httpExpected := `POST /samus/ HTTP/1.1
					Host: twitch-proxy.amazon.com
					Content-Encoding: amz-1.0
					Content-Type: application/json; charset=UTF-8
					User-Agent: TwitchEntitlementService
					X-Amz-Client-Id: clientId
					X-Amz-Target: com.amazonaws.twitchproxyservice.TwitchProxyService.UpdateFulfillmentStatusSigV4
					X-Amzn-Requestid: RequestId
					X-Requested-With: XMLHttpRequest
					
					{"UpdateFulfillmentStatusRequest":{"fulfillmentStatuses":[{"entitlementId":"entitlementId","fulfillmentStatus":"fulfillmentStatus"}]},"clientID":"clientId","requestID":"RequestId"}`

	httpExpected = strings.Join(strings.Fields(httpExpected), "")

	httpRequest, _ := out.ToHTTPRequest()

	// Save a copy of this request for debugging.
	httpRequestActualByte, err := httputil.DumpRequest(httpRequest, true)
	if err != nil {
		assert.Fail(t, err.Error())
	}

	httpRequestActual := strings.Join(strings.Fields(string(httpRequestActualByte)), "")

	assert.Equal(t, httpExpected, string(httpRequestActual), "Should be the same.")
}

func TestCreateGetAccountEntitlementsOutputFromResponseGood(t *testing.T) {
	var response = external.NewBaseResponse("status", 200, byteResponseBody)

	out, err := ses.createGetAccountEntitlementsOutputFromResponse(response)

	assert.Equal(t, len(out.Entitlements), 1, "These should be the same length")
	assert.Equal(t, out.Entitlements[0].EntitlementId,
		"amzn1.spid.ffa9a2f8-954a-427e-b52a-0b11cc2f334c_6f54b708-889f-4d61-bbbb-aadbcd81244f",
		"We should see the same enitltmentId")
	assert.Equal(t, nil, err, "There should be no errors.")
}

func TestCreateGetAccountEntitlementsOutputFromResponseBad(t *testing.T) {
	var response = external.NewBaseResponse("status", 200, byteErrorResponseBody)

	_, err := ses.createGetAccountEntitlementsOutputFromResponse(response)

	assert.Equal(t, "The external service responded with a Exception", err.Error(), "There should be an errors.")
}

func TestCreateGetAccountEntitlementsOutputFromResponseBadCall(t *testing.T) {
	var response = external.NewBaseResponse("status", 500, byteErrorResponseBody)

	_, err := ses.createGetAccountEntitlementsOutputFromResponse(response)

	assert.Equal(t, "An internal error occured.", err.Error(), "There should be an errors.")
}

func TestCreateUpdateFulfillmentStatusOutputFromResponseGood(t *testing.T) {
	var response = external.NewBaseResponse("status", 200, byteResponseBody)

	out, err := ses.createUpdateFulfillmentStatusOutputFromResponse(response)

	assert.Equal(t, len(out.Entitlements), 1, "These should be the same length")
	assert.Equal(t, out.Entitlements[0].EntitlementId,
		"amzn1.spid.ffa9a2f8-954a-427e-b52a-0b11cc2f334c_6f54b708-889f-4d61-bbbb-aadbcd81244f",
		"We should see the same enitltmentId")
	assert.Equal(t, nil, err, "There should be no errors.")
}

func TestCreateUpdateFulfillmentStatusOutputFromResponseBad(t *testing.T) {
	var response = external.NewBaseResponse("status", 200, byteErrorResponseBody)

	_, err := ses.createUpdateFulfillmentStatusOutputFromResponse(response)

	assert.Equal(t, "The external service responded with a Exception", err.Error(), "There should be an errors.")
}

func TestCreateUpdateFulfillmentStatusOutputFromResponseBadCall(t *testing.T) {
	var response = external.NewBaseResponse("status", 500, byteErrorResponseBody)

	_, err := ses.createUpdateFulfillmentStatusOutputFromResponse(response)

	assert.Equal(t, "An internal error occured.", err.Error(), "There should be an errors.")
}
