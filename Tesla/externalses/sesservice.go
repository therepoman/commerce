package externalses

import (
	"code.justin.tv/commerce/Tesla/config"
	"code.justin.tv/commerce/Tesla/external"
	"encoding/json"
	"strings"
)

// Types used by go
// GetAccountEntitlementsOutput is the output structure returned from the GetAccountEntitlements call.
type GetAccountEntitlementsOutput struct {
	Entitlements []Entitlement `json:"entitlements"`
}

// GetAccountEntitlementsOutput is the output structure returned from the UpdateFulfillmentStatusSigV4 call.
type UpdateFulfillmentStatusOutput struct {
	Entitlements []Entitlement `json:"entitlements"`
}

// GetAccountEntitlementsSigV4Input is the structure provided to the GetAccountEntitlements call
// when calling through sigV4.
type GetAccountEntitlementsSigV4Input struct {
	RequestID       string
	Tuid            string
	ClientID        string
	ProductIds      []string
	MetaDataFilters []MetadataFilter
}

// FulfillmentInstruction represents the next step to be made on a sku, with regards
// to fullfilment
type FulfillmentInstruction string

// FulfillmentResult is the response type for updating fulfillment status
type FulfillmentResult string

const (
	// Fulfill : The item should be fulfilled.
	Fulfilled FulfillmentInstruction = "FULFILLED"
	// Revoke : The item should be revoked.
	Revoked FulfillmentInstruction = "REVOKED"
	// PENDING_FULFILLMENT : Nothing needs to be done.
	PendingFulfillment FulfillmentInstruction = "PENDING_FULFILLMENT"
	// PENDING_REVOKE : Nothing needs to be done.
	PendingRevoke FulfillmentInstruction = "PENDING_REVOKE"
)

const (
	internalFailureSuffix string = "InternalFailure"
	exceptionSuffix       string = "Exception"
	errorSuffix           string = "Error"
	failureSuffix         string = "Failure"
)

// Metric Constants
const (
	GetAccountEntitlementsLatencyMetricName  = "DependencyLatency:GetAccountEntitlements"
	GetAccountEntitlementsFailureMetricName  = "DependencyCallFailure:GetAccountEntitlements"
	UpdateFulfillmentStatusLatencyMetricName = "DependencyLatency:UpdateFulfillmentStatusSigV4"
	UpdateFulfillmentStatusFailureMetricName = "DependencyCallFailure:UpdateFulfillmentStatusSigV4"
)

// Common types used by both api
type EntitlementAccount struct {
	AccountId   string  `json:"accountId"`
	AccountType *string `json:"accountType"`
}

type MetadataFilter struct {
	MetadataKey        string             `json:"metadataKey"`
	MetadataFilterRule MetadataFilterRule `json:"metadataFilterRule"`
}

type MetadataFilterRule struct {
	MetadataFilterRuleType  string `json:"type"`
	MetadataFilterRuleValue string `json:"value"`
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The following are to be used for updateFulfillmentStatus API and as of yet have not been completely flushed out
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type EntitlementStatus struct {
	EntitlementStatus string
}

type EntitlementProduct struct {
	ProductId    string
	ProductName string
	Destinations []string
	Metadata     map[string]string
}

type EntitlementStatusHistoryEntry struct {
	EntitlementStatus string
	TimeStamp         string
}

type FulfillmentStatusHistoryEntry struct {
	FulfillmentStatus string
	TimeStamp         string
}

type Entitlement struct {
	EntitlementId            string
	EntitlementAccounts      []EntitlementAccount
	RelatedAccounts          []EntitlementAccount
	EntitlementStatus        string
	FulfillmentStatus        string
	CreatedDate              string
	LastUpdatedDate          string
	EntitlementStatusHistory []EntitlementStatusHistoryEntry
	FulfillmentStatusHistory []FulfillmentStatusHistoryEntry
	Metadata                 map[string]string
	Domain                   string
	Product                  EntitlementProduct
}

type FulfillmentStatusObject struct {
	EntitlementId     string `json:"entitlementId"`
	FulfillmentStatus string `json:"fulfillmentStatus"`
}

type UpdateFulfillmentStatusRequestModel struct {
	FulfillmentStatuses []FulfillmentStatusObject `json:"fulfillmentStatuses"`
}

type tProxUpdateFulfillmentStatusRequestModel struct {
	Request   UpdateFulfillmentStatusRequestModel `json:"UpdateFulfillmentStatusRequest"`
	ClientID  *string                             `json:"clientID"`
	RequestID *string                             `json:"requestID"`
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  The following are to be used for getAccountEntitlements API
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type GetAccountEntitlementsRequestModel struct {
	EntitlementAccount *EntitlementAccount `json:"entitlementAccount"`
	ProductIds         []string            `json:"productIds"`
	FilterCleared      *bool               `json:"filterCleared"`
	MetaDataFilters    []MetadataFilter    `json:"metadataFilters"`
}

type GetAccountEntitlementsResponseModel struct {
	Entitlements []Entitlement
}

type tProxGetAccountEntitlementsRequestModel struct {
	Request   *GetAccountEntitlementsRequestModel `json:"GetAccountEntitlementsRequest"`
	ClientID  *string                             `json:"clientID"`
	RequestID *string                             `json:"requestID"`
}

// SESGetAccountEntitlementsInterface is our interface to SamusEntitlementService to access GetAccountEntitlement.
type SESGetAccountEntitlementsInterface interface {
	GetAccountEntitlementsSigV4(GetAccountEntitlementsSigV4Input) (GetAccountEntitlementsOutput, external.Error)
}

// SESUpdateFulfillmentStatusInterface is our interface to SamusEntitlementService to access GetAccountEntitlement.
type SESUpdateFulfillmentStatusInterface interface {
	UpdateFulfillmentStatusSigV4(clientId string, receiptId string, fulfillmentStatusObjects []FulfillmentStatusObject) (UpdateFulfillmentStatusOutput, external.Error)
}

// Setting up struct to use for testing purposes
type sigV4SerializableRequestGetter struct {
	makeSigV4SerializableRequest external.NewSigV4SerializableRequestMaker
}

func NewGetSigV4SerializableRequest(maker external.NewSigV4SerializableRequestMaker) sigV4SerializableRequestGetter {
	return sigV4SerializableRequestGetter{makeSigV4SerializableRequest: maker}
}

type samusEntitlementService struct {
	callFactory   external.CallFactory
	configuration *config.TeslaConfiguration
}

// NewSESGetAccountEntitlements returns a SamusEntitlementService interface that can be used to call GetAccountEntitlements.
func NewSESGetAccountEntitlements(apiContext external.APIContext, clientMetricName string) SESGetAccountEntitlementsInterface {
	metricNames := external.MetricNames{LatencyName: GetAccountEntitlementsLatencyMetricName, FailureName: GetAccountEntitlementsFailureMetricName}
	metricContext := external.MetricContext{APIContext: apiContext, ClientMetricName: clientMetricName, MetricNames: metricNames}
	factory := external.MetricsCallFactory{MetricContext: metricContext}
	return samusEntitlementService{callFactory: factory, configuration: apiContext.Configuration}
}

// NewSESUpdateFulfillmentStatus returns a SamusEntitlementService interface that can be used to call UpdateFulfillmentStatusSigV4.
func NewSESUpdateFulfillmentStatus(apiContext external.APIContext, clientMetricName string) SESUpdateFulfillmentStatusInterface {
	metricNames := external.MetricNames{LatencyName: UpdateFulfillmentStatusLatencyMetricName, FailureName: UpdateFulfillmentStatusFailureMetricName}
	metricContext := external.MetricContext{APIContext: apiContext, ClientMetricName: clientMetricName, MetricNames: metricNames}
	factory := external.MetricsCallFactory{MetricContext: metricContext}
	return samusEntitlementService{callFactory: factory, configuration: apiContext.Configuration}
}

func (ses samusEntitlementService) GetAccountEntitlementsSigV4(i GetAccountEntitlementsSigV4Input) (GetAccountEntitlementsOutput, external.Error) {
	request := ses.createRequestFromGetAccountEntitlementsSigV4Input(i, NewGetSigV4SerializableRequest(external.NewSigV4SerializableRequest))

	// We now have the request, get the call we should use for this request, and call it.
	call := ses.callFactory.CreateCall(request)
	response, err := call.Call(request)
	if err != nil {
		return GetAccountEntitlementsOutput{}, err
	}

	// Now that we have the abstract call response, we convert it into something
	// slightly more legible.
	output, err := ses.createGetAccountEntitlementsOutputFromResponse(response)
	return output, err
}

func (ses samusEntitlementService) UpdateFulfillmentStatusSigV4(clientId string, receiptId string, fulfillmentObjects []FulfillmentStatusObject) (UpdateFulfillmentStatusOutput, external.Error) {
	request := ses.createRequestFromUpdateFulfillmentStatusSigV4Input(clientId, receiptId, fulfillmentObjects, NewGetSigV4SerializableRequest(external.NewSigV4SerializableRequest))

	// We now have the request, get the call we should use for this request, and call it.
	call := ses.callFactory.CreateCall(request)
	response, err := call.Call(request)
	if err != nil {
		return UpdateFulfillmentStatusOutput{}, err
	}

	// Now that we have the abstract call response, we convert it into something
	// slightly more legible.
	output, err := ses.createUpdateFulfillmentStatusOutputFromResponse(response)
	return output, err
}

func (ses samusEntitlementService) createRequestFromGetAccountEntitlementsSigV4Input(i GetAccountEntitlementsSigV4Input, getter sigV4SerializableRequestGetter) external.Request {
	accountType := "TWITCH"
	filterCleared := true
	callURL := "https://twitch-proxy.amazon.com/samus/"
	callMethod := "POST"
	model := tProxGetAccountEntitlementsRequestModel{
		Request: &GetAccountEntitlementsRequestModel{
			EntitlementAccount: &EntitlementAccount{
				AccountId:   i.Tuid,
				AccountType: &accountType,
			},
			ProductIds:      i.ProductIds,
			MetaDataFilters: i.MetaDataFilters,
			FilterCleared:   &filterCleared,
		},
		ClientID:  &i.ClientID,
		RequestID: &i.RequestID,
	}
	headers := make(map[string]string)
	headers["Content-Type"] = "application/json; charset=UTF-8"
	headers["X-Requested-With"] = "XMLHttpRequest"
	headers["X-Amz-Target"] = "com.amazonaws.twitchproxyservice.TwitchProxyService.GetAccountEntitlements"
	headers["x-amzn-RequestId"] = i.RequestID
	headers["x-amz-client-id"] = i.ClientID
	request := getter.makeSigV4SerializableRequest(callMethod, callURL, headers, model, ses.configuration.GetTProxSigV4Signer(), ses.configuration.GetTProxSigV4ServiceName(), ses.configuration.GetTProxSigV4AwsRegion())

	return request
}

func (ses samusEntitlementService) createRequestFromUpdateFulfillmentStatusSigV4Input(clientId string, requestId string, fulfillmentStatusObjects []FulfillmentStatusObject, getter sigV4SerializableRequestGetter) external.Request {
	callURL := "https://twitch-proxy.amazon.com/samus/"
	callMethod := "POST"
	model := tProxUpdateFulfillmentStatusRequestModel{
		Request: UpdateFulfillmentStatusRequestModel{
			FulfillmentStatuses: fulfillmentStatusObjects,
		},
		ClientID:  &clientId,
		RequestID: &requestId,
	}
	headers := make(map[string]string)
	headers["Content-Type"] = "application/json; charset=UTF-8"
	headers["X-Requested-With"] = "XMLHttpRequest"
	headers["X-Amz-Target"] = "com.amazonaws.twitchproxyservice.TwitchProxyService.UpdateFulfillmentStatus"
	headers["x-amzn-RequestId"] = requestId
	headers["x-amz-client-id"] = clientId
	request := getter.makeSigV4SerializableRequest(callMethod, callURL, headers, model, ses.configuration.GetTProxSigV4Signer(), ses.configuration.GetTProxSigV4ServiceName(), ses.configuration.GetTProxSigV4AwsRegion())

	return request
}

func (ses samusEntitlementService) createGetAccountEntitlementsOutputFromResponse(r external.Response) (GetAccountEntitlementsOutput, external.Error) {
	var getAccountEntitlementsOutput GetAccountEntitlementsOutput

	serviceError := ses.determineError(r.GetStatusCode(), r)
	if serviceError != nil {
		return GetAccountEntitlementsOutput{}, serviceError
	}

	if err := json.Unmarshal(r.GetBody(), &getAccountEntitlementsOutput); err != nil {
		return GetAccountEntitlementsOutput{}, external.NewInternalServerError("There was an error deserializing the SES response : " + err.Error())
	}

	return getAccountEntitlementsOutput, nil
}

func (ses samusEntitlementService) createUpdateFulfillmentStatusOutputFromResponse(r external.Response) (UpdateFulfillmentStatusOutput, external.Error) {
	var updateFulfillmentStatusOutput UpdateFulfillmentStatusOutput

	serviceError := ses.determineError(r.GetStatusCode(), r)
	if serviceError != nil {
		return UpdateFulfillmentStatusOutput{}, serviceError
	}

	if err := json.Unmarshal(r.GetBody(), &updateFulfillmentStatusOutput); err != nil {
		return UpdateFulfillmentStatusOutput{}, external.NewInternalServerError("There was an error deserializing the SES response : " + err.Error())
	}

	return updateFulfillmentStatusOutput, nil
}

// Determines whether the deserialized response is an error, and returns the appropriate error interface.
// Returns nil if there is no error.
func (ses samusEntitlementService) determineError(code int, r external.Response) external.Error {
	var data map[string]interface{}

	if err := json.Unmarshal(r.GetBody(), &data); err != nil {
		return external.NewInternalServerError("There was an error deserializing the SES response : " + err.Error())
	}

	codeError := getErrorFromCode(code)
	if codeError != nil {
		return codeError
	}

	responseType := getStringFromMapOrEmpty(data, "__type")

	errorSuffixes := [4]string{internalFailureSuffix, exceptionSuffix, errorSuffix, failureSuffix}
	for _, suffix := range errorSuffixes {
		if strings.HasSuffix(responseType, suffix) {
			return external.NewInternalServerError("The external service responded with a " + suffix)
		}
	}

	return nil
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This is in the common.go package in externaladg as well we could consider creating a more general common package or change this to be more
// service specific. Have not dived too deep here.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func getErrorFromCode(code int) external.Error {
	if code < 300 {
		return nil // Strictly below 300 are all successes.
	}

	// At this point we know that code is at least greater or equal to 300
	if code < 400 {
		return external.NewServiceUnavailableError("The service is currently unavailable.") // All 300s are represented as 503s
	}

	if code < 500 {
		switch code {
		case 401:
			return external.NewUnauthorizedError("You are not autorized to access this API.") // 401
		case 403:
			return external.NewForbiddenError("You are forbidden from accessing this API.") // 403
		case 404:
			return external.NewNotFoundError("The API could not be found.") // 404
		default:
			return external.NewBadRequestError("The request was invalid.") // 400
		}
	}

	// Now we just handle the 500s
	switch code {
	case 502:
		return external.NewBadGatewayError("A bad gateway was encountered while making the request.") // 502
	case 503:
		return external.NewServiceUnavailableError("The service is currently unavailable.") // 503
	default:
		return external.NewInternalServerError("An internal error occured.") // 500
	}
}

func getStringFromMapOrEmpty(m map[string]interface{}, key string) string {
	v, exists := m[key]

	// Key does not exist in map, return empty string.
	if !exists {
		return ""
	}

	switch v.(type) {
	case string:
		return v.(string)
	default:
		// Value is not a string, return empty string.
		return ""
	}
}
