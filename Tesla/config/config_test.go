package config

import (
	"os"
	"testing"
)

const (
	testClient string  = "randomClient"
	someNewAPI APIName = "someNewAPI"
)

func TestGetCurrentEnvironment(t *testing.T) {
	envVarName := "ENVIRONMENT"

	// Test that if the var is unset, we get a local env.
	err := os.Unsetenv(envVarName)
	if err != nil {
		t.Error("Error unsetting environment variable.")
	}

	currentEnv := getCurrentEnvironment()
	if currentEnv != TeslaLocal {
		t.Error("Expected", TeslaLocal, "environment but got :", currentEnv)
	}

	// Test that if the env is set to junk, we get local.
	err = os.Setenv(envVarName, "junk dumpster")
	if err != nil {
		t.Error("Error setting environment variable")
	}

	currentEnv = getCurrentEnvironment()
	if currentEnv != TeslaLocal {
		t.Error("Expected", TeslaLocal, "environment but got :", currentEnv)
	}

	// Test that if the env is set to tesla-dev, we get beta
	err = os.Setenv(envVarName, envNameTeslaDev)
	if err != nil {
		t.Error("Error setting environment variable")
	}

	currentEnv = getCurrentEnvironment()
	if currentEnv != TeslaBeta {
		t.Error("Expected", TeslaBeta, "environment but got :", currentEnv)
	}

	// Test that if the env is set to tesla-prod, we get prod
	err = os.Setenv(envVarName, envNameTeslaProd)
	if err != nil {
		t.Error("Error setting environment variable")
	}

	currentEnv = getCurrentEnvironment()
	if currentEnv != TeslaProd {
		t.Error("Expected", TeslaProd, "environment but got :", currentEnv)
	}
}

func TestTeslaTestConfig(t *testing.T) {
	// Create (and obtain) the test configuration
	configuration := GetTestConfiguration()

	// Verify that we are using the test config
	if !configuration.IsTest() {
		t.Error("Expected test environment but got ", getCurrentEnvironment())
	}
}

func TestBlacklisting(t *testing.T) {
	// Create (and obtain) the test configuration
	configuration := GetTestConfiguration()

	// Verify global blacklist (for existing APIs)
	if !configuration.IsClientBlacklistedForAPI(string(blackListingTestGlobalClient), SetStatusForReceiptsAPI) {
		t.Error("Expected ", string(blackListingTestGlobalClient), " to be blocked for ", SetStatusForReceiptsAPI)
	}

	if !configuration.IsClientBlacklistedForAPI(string(blackListingTestGlobalClient), GetGoodsForUserAPI) {
		t.Error("Expected ", string(blackListingTestGlobalClient), " to be blocked for ", GetGoodsForUserAPI)
	}

	// Verify API specific blacklists (for existing APIs)
	if !configuration.IsClientBlacklistedForAPI(string(blackListingTestFulfillClient), SetStatusForReceiptsAPI) {
		t.Error("Expected ", string(blackListingTestFulfillClient), " to be blocked for ", SetStatusForReceiptsAPI)
	}

	if configuration.IsClientBlacklistedForAPI(string(blackListingTestGoodsClient), SetStatusForReceiptsAPI) {
		t.Error("Expected ", string(blackListingTestGoodsClient), " to NOT be blocked for ", SetStatusForReceiptsAPI)
	}

	if !configuration.IsClientBlacklistedForAPI(string(blackListingTestGoodsClient), GetGoodsForUserAPI) {
		t.Error("Expected ", string(blackListingTestGoodsClient), " to be blocked for ", GetGoodsForUserAPI)
	}

	if configuration.IsClientBlacklistedForAPI(string(blackListingTestFulfillClient), GetGoodsForUserAPI) {
		t.Error("Expected ", string(blackListingTestFulfillClient), " to NOT be blocked for ", GetGoodsForUserAPI)
	}

	if configuration.IsClientBlacklistedForAPI(testClient, GetGoodsForUserAPI) {
		t.Error("Expected ", testClient, " to NOT be blocked for ", GetGoodsForUserAPI)
	}

	if configuration.IsClientBlacklistedForAPI(testClient, SetStatusForReceiptsAPI) {
		t.Error("Expected ", testClient, " to NOT be blocked for ", SetStatusForReceiptsAPI)
	}

	// Verify new APIs work for clients (no blacklist)
	if configuration.IsClientBlacklistedForAPI(testClient, someNewAPI) {
		t.Error("Expected ", testClient, " to NOT be blocked for ", someNewAPI)
	}

	if configuration.IsClientBlacklistedForAPI(string(blackListingTestFulfillClient), someNewAPI) {
		t.Error("Expected ", string(blackListingTestFulfillClient), " to NOT be blocked for ", someNewAPI)
	}

	// Verify global blacklist works for new APIs
	if !configuration.IsClientBlacklistedForAPI(string(blackListingTestGlobalClient), someNewAPI) {
		t.Error("Expected ", string(blackListingTestGlobalClient), " to be blocked for ", someNewAPI)
	}
}

func testThrottle(prefix string, t *testing.T, config *TeslaConfiguration, apiName APIName, client ThrottledClient, expectedRate int64) {
	rate := config.GetClientRateLimit(apiName, client)

	if rate != expectedRate {
		t.Error(prefix, "Unexpected rate. Expected ", expectedRate, " Got ", rate)
	}
}

func TestThrottling(t *testing.T) {
	configuration := GetTestConfiguration()
	expectedRate := int64(5)
	expectedRate2 := int64(10)
	apiName := APIName("myTestAPI")
	otherAPI := APIName("otherAPI")
	client := ThrottledClient("aClient")
	otherClient := ThrottledClient("OtherClient")

	// Now we throttle a specific API
	throttleAPI(configuration.throttledClientMap, apiName, expectedRate)
	// Since we throttled the API, any client should get the rate we specified, regardless of the client.
	testThrottle("[APIThrottle]", t, configuration, apiName, otherClient, expectedRate)

	// Now we throttle a specific client in the API API
	throttleClientOnAPI(configuration.throttledClientMap, apiName, client, expectedRate2)
	// Now for that specific client, the rate should be different.
	testThrottle("[APIClientThrottle]", t, configuration, apiName, client, expectedRate2)
	// And any other client will have the old rate.
	testThrottle("[APIClientThrottleUnscoped]", t, configuration, apiName, otherClient, expectedRate)
	// Unscoped apis should also return the default limit in the config.
	testThrottle("[APIThrottleDefault]", t, configuration, otherAPI, otherClient, configuration.defaultThrottle)

	// Now we scope a specific client on an API, but *not* the API itself.
	throttleClientOnAPI(configuration.throttledClientMap, otherAPI, client, expectedRate2)
	// So now the rate on the specific client of the api will be expectedRate2
	testThrottle("[APIThrottleOnlyClient]", t, configuration, otherAPI, client, expectedRate2)
	// And everythign else will return the default.
	testThrottle("[APIThrottleOnlyClient]", t, configuration, otherAPI, otherClient, configuration.defaultThrottle)
}

func TestProgrammaticThrottling(t *testing.T) {
	configuration := GetTestConfiguration()
	expectedRate := int64(5)
	expectedRate2 := int64(10)
	apiName := APIName("myTestAPI")
	otherAPI := APIName("otherAPI")
	client := ThrottledClient("aClient")
	otherClient := ThrottledClient("OtherClient")

	// Now we throttle a specific API
	configuration.ThrottleAPI(apiName, expectedRate)
	// Since we throttled the API, any client should get the rate we specified, regardless of the client.
	testThrottle("[APIThrottle]", t, configuration, apiName, otherClient, expectedRate)

	// Now we throttle a specific client in the API API
	configuration.ThrottleAPIForClient(apiName, client, expectedRate2)
	// Now for that specific client, the rate should be different.
	testThrottle("[APIClientThrottle]", t, configuration, apiName, client, expectedRate2)
	// And any other client will have the old rate.
	testThrottle("[APIClientThrottleUnscoped]", t, configuration, apiName, otherClient, expectedRate)
	// Unscoped apis should also return the default limit in the config.
	testThrottle("[APIThrottleDefault]", t, configuration, otherAPI, otherClient, configuration.defaultThrottle)

	// Now we scope a specific client on an API, but *not* the API itself.
	configuration.ThrottleAPIForClient(otherAPI, client, expectedRate2)
	// So now the rate on the specific client of the api will be expectedRate2
	testThrottle("[APIThrottleOnlyClient]", t, configuration, otherAPI, client, expectedRate2)
	// And everythign else will return the default.
	testThrottle("[APIThrottleOnlyClient]", t, configuration, otherAPI, otherClient, configuration.defaultThrottle)
}
