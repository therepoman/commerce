package config

import (
	"os"
	"time"

	"code.justin.tv/systems/sandstorm/manager"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/aws/signer/v4"
	"github.com/aws/aws-sdk-go/service/sts"

	log "github.com/Sirupsen/logrus"
)

// TeslaEnvironment describes the environment in which the service is running.
type TeslaEnvironment string

// BlacklistedClient is the type designating a client who has been blacklisted.
type BlacklistedClient string

// ThrottledClient is the type designating a client who has a throttling limitation.
type ThrottledClient string

// APIName reqpresents the name of an API provided in TESla
type APIName string

const (
	// TeslaTest is the unit testing configuration, used specifically when unit testing.
	TeslaTest TeslaEnvironment = "UnitTest"
	// TeslaLocal is the local configuration, run from developer environments.
	TeslaLocal TeslaEnvironment = "Local"
	// TeslaBeta is the Beta EB stage. Used for testing prior to a push to Prod.
	TeslaBeta TeslaEnvironment = "Beta"
	// TeslaProd is the Production EB stage.
	TeslaProd TeslaEnvironment = "Prod"

	// Environment names
	envNameTeslaTest string = "tesla-test"
	envNameTeslaDev  string = "tesla-dev"
	envNameTeslaProd string = "tesla-prod"

	sandstormTableName = "sandstorm-production"
	sandstormTableID   = "alias/sandstorm-production"

	devoSandstormARN string = "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/commerce-tesla-dev-Tesla"
	prodSandstormARN string = "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/commerce-tesla-prod-Tesla"

	tProxAccessARN        string = "arn:aws:iam::461497548915:role/TESlaAccessRole"
	tProxSigV4ServiceName string = "TwitchProxyService"
	tProxSigV4AwsRegion   string = "us-east-1"

	// Blacklisting test clients. To add / change clients, edit the maps in createBlacklistedClientMap
	blackListingTestGlobalClient  BlacklistedClient = "TestGlobalBlacklist"
	blackListingTestGoodsClient   BlacklistedClient = "TestGoodsBlacklist"
	blackListingTestFulfillClient BlacklistedClient = "TestFulfillBlacklist"

	// Blacklisting real clients
	kappaPetBlacklistedClient BlacklistedClient = "0biqu5sb4f8fq6pxxg09xix27d0uo2" //Porcupine's Kappa Pet
	bnetTestBlacklistedClient BlacklistedClient = "dfsj66sgvvvmpy4h3kwba1njvqog4m" //Battle Net testing client.

	// UnlimitedRate is the rate for a client that is not throttled.
	UnlimitedRate int64 = -1

	// GlobalClient is the key in the throttling map that applies to all clients that don't have a more specific configuration.
	GlobalClient ThrottledClient = "GLOBAL_CLIENT"

	// GlobalAPI is an API that applies to all APIs. (AKA global blacklist)
	GlobalAPI APIName = "GLOBAL"

	// HealthCheckAPI is the name of the api that will determine the health of the host (AKA Ping)
	HealthCheckAPI APIName = "HealthCheck"
	// StatusCheckAPI is the name for the api that checks the host's status (AKA deep Ping)
	StatusCheckAPI APIName = "StatusCheck"
	// SetStatusForReceiptsAPI is the name for the fulfillment api, used to mark entitlements as fulfilled.
	SetStatusForReceiptsAPI APIName = "SetStatusForReceipts"
	// GetGoodsForUserAPI is the name of the API to return a list of the user's entitlements.
	GetGoodsForUserAPI APIName = "GetGoodsForUser"

	// API Names used for SES
	// UpdateFulfillmentStatusAPI is the name for the fulfillment api in SES, used to update entitlement's fulfillment status'.
	UpdateFulfillmentStatus APIName = "UpdateFulfillmentStatus"
	// GetAccountEntitlementsAPI is the name of the API to return a list of the accounts's entitlements.
	GetAccountEntitlements APIName = "GetAccountEntitlements"

	// TODO remove this when we are no longer calling both services
	// String to tag comparison of SES and ADG Calls
	SESADGComparisonLogTag string = "[SESCOMPARISON]"

	// UnknownClient is used for the clientID when it is not known or provided.
	UnknownClient string = "UnknownClient"

	// BlacklistedClientMetric is the metric to be used when metrics are published indication a client is blacklisted.
	BlacklistedClientMetric string = "Blacklisted"
	// ThrottledClientMetric is the metrics to be used when metrics are published to say a client was throttled.
	ThrottledClientMetric string = "Throttled"
)

// TeslaConfiguration contains all configuration-related information for Tesla
type TeslaConfiguration struct {
	environment           TeslaEnvironment
	awsRegion             string
	awsMetricsCredentials *credentials.Credentials
	tProxSigV4Signer      *v4.Signer
	tProxSigV4ServiceName string
	tProxSigV4AwsRegion   string
	blacklistedClientMap  map[APIName][]BlacklistedClient
	throttledClientMap    map[APIName]map[ThrottledClient]int64
	defaultThrottle       int64
}

// IsProd will return true if the environment is Prod.
func (t *TeslaConfiguration) IsProd() bool {
	return t.environment == TeslaProd
}

// IsBeta will return true if the environment is Beta.
func (t *TeslaConfiguration) IsBeta() bool {
	return t.environment == TeslaBeta
}

// IsLocal will return true if the environment is Local.
func (t *TeslaConfiguration) IsLocal() bool {
	return t.environment == TeslaLocal
}

// IsTest will return true if the environment is for unit tests (Test).
func (t *TeslaConfiguration) IsTest() bool {
	return t.environment == TeslaTest
}

// GetAwsRegion will return the aws region used by the service.
func (t *TeslaConfiguration) GetAwsRegion() string {
	return t.awsRegion
}

// GetAwsMetricsCredentials will return the awsCredentials to use to publish metrics.
func (t *TeslaConfiguration) GetAwsMetricsCredentials() *credentials.Credentials {
	return t.awsMetricsCredentials
}

// GetTProxSigV4Signer returns the signer to be used to sign requests to TPROX.
func (t *TeslaConfiguration) GetTProxSigV4Signer() *v4.Signer {
	return t.tProxSigV4Signer
}

// GetTProxSigV4ServiceName returns the signer to be used to sign requests to TPROX.
func (t *TeslaConfiguration) GetTProxSigV4ServiceName() string {
	return t.tProxSigV4ServiceName
}

// GetTProxSigV4AwsRegion returns the signer to be used to sign requests to TPROX.
func (t *TeslaConfiguration) GetTProxSigV4AwsRegion() string {
	return t.tProxSigV4AwsRegion
}

// ThrottleAPI allows to programatically throttle an API.
func (t *TeslaConfiguration) ThrottleAPI(api APIName, rate int64) {
	throttleAPI(t.throttledClientMap, api, rate)
}

// ThrottleAPIForClient allows to programaticaly throttle an API for a client.
func (t *TeslaConfiguration) ThrottleAPIForClient(api APIName, clientID ThrottledClient, rate int64) {
	throttleClientOnAPI(t.throttledClientMap, api, clientID, rate)
}

// GetClientRateLimit will return the rate limit for a given client.
func (t *TeslaConfiguration) GetClientRateLimit(apiName APIName, client ThrottledClient) int64 {
	throttledAPIClients := t.throttledClientMap[apiName]
	if len(throttledAPIClients) > 0 {
		if rate, exists := throttledAPIClients[client]; exists {
			// There is a rate defined specifically for the client.
			return rate
		}

		// There is no rate specified for the client. Is there a rate on the global client (aka the default for all clients of this API ?)
		if rate, exists := throttledAPIClients[GlobalClient]; exists {
			// There is a global rate, return it.
			return rate
		}
	}

	// No client or global rate. Return the default.
	return t.defaultThrottle
}

// GetAPIRateLimit will return the rate limit for a given api.
func (t *TeslaConfiguration) GetAPIRateLimit(apiName APIName) int64 {
	return t.GetClientRateLimit(apiName, GlobalClient)
}

// IsClientBlacklistedForAPI checks if a client is permitted to use a given an API
func (t *TeslaConfiguration) IsClientBlacklistedForAPI(clientName string, apiName APIName) bool {
	// Check if the client is part of an API blacklist
	apiBlackList := t.blacklistedClientMap[apiName]
	if len(apiBlackList) > 0 {
		if isClientInBlacklist(clientName, apiBlackList) {
			return true
		}
	}
	// Check if the client is part of a global blacklist
	globalBlacklist := t.blacklistedClientMap[GlobalAPI]
	if len(globalBlacklist) > 0 {
		if isClientInBlacklist(clientName, globalBlacklist) {
			return true
		}
	}
	return false
}

// GetThrottlingClientFromClientID provides a throttled client for the given client ID.
func (t *TeslaConfiguration) GetThrottlingClientFromClientID(s string) ThrottledClient {
	return ThrottledClient(s)
}

// Helper method to check if a client is part of a given blacklist
func isClientInBlacklist(clientName string, blacklist []BlacklistedClient) bool {
	for _, b := range blacklist {
		if clientName == string(b) {
			return true
		}
	}
	return false
}

// Create a map of api to blacklisted clients, including a global blacklist, given a TeslaEnvironment
func createBlacklistedClientMap(environment TeslaEnvironment) map[APIName][]BlacklistedClient {
	if environment == TeslaProd {
		prodBlacklistMap := make(map[APIName][]BlacklistedClient)
		// Set the global blacklist
		prodBlacklistMap[GlobalAPI] = []BlacklistedClient{kappaPetBlacklistedClient, bnetTestBlacklistedClient}
		return prodBlacklistMap
	} else if environment == TeslaBeta {
		return make(map[APIName][]BlacklistedClient)
	} else if environment == TeslaTest {
		blacklistMap := make(map[APIName][]BlacklistedClient)
		// Set the global blacklist
		blacklistMap[GlobalAPI] = []BlacklistedClient{blackListingTestGlobalClient}
		// Set API specific blacklists
		blacklistMap[GetGoodsForUserAPI] = []BlacklistedClient{blackListingTestGoodsClient}
		blacklistMap[SetStatusForReceiptsAPI] = []BlacklistedClient{blackListingTestFulfillClient}
		return blacklistMap
	}

	return make(map[APIName][]BlacklistedClient)
}

func createThrottledClientMap() map[APIName]map[ThrottledClient]int64 {
	// Creates the default throttling map. Any throttling that should occur in every configuration should be setup here.
	// Throttles for specific configurations should be created in the analoguous configuration setup using throttleClientOnAPI
	// and throttleAPI

	// Throttling on status check. This applies to every environment.
	statusCheckMap := make(map[ThrottledClient]int64)
	statusCheckMap[GlobalClient] = 5 // Global throttle on /status of 5 calls per second.

	throttlingMap := make(map[APIName]map[ThrottledClient]int64)
	throttlingMap[StatusCheckAPI] = statusCheckMap

	return throttlingMap
}

// Throttles a specific client on a specific API to a given rate.
func throttleClientOnAPI(throttlingMap map[APIName]map[ThrottledClient]int64, api APIName, clientID ThrottledClient, rate int64) {
	if apiMap, exists := throttlingMap[api]; exists {
		// A map for the API already exists. Append to it.
		apiMap[clientID] = rate
	} else {
		apiMap = make(map[ThrottledClient]int64)
		apiMap[clientID] = rate
		throttlingMap[api] = apiMap
	}
}

// Throttles a specific API to a given rate (will apply to all clients which do not have more specific rates applied to them).
func throttleAPI(throttlingMap map[APIName]map[ThrottledClient]int64, api APIName, rate int64) {
	throttleClientOnAPI(throttlingMap, api, GlobalClient, rate)
}

func getCurrentEnvironment() TeslaEnvironment {
	env := os.Getenv("ENVIRONMENT")
	if env == envNameTeslaProd {
		return TeslaProd
	} else if env == envNameTeslaDev {
		return TeslaBeta
	} else if env == envNameTeslaTest {
		return TeslaTest
	}

	return TeslaLocal
}

func getSandstormManager(awsRegion string, awsStsEndpoint string, sandstormARN string) *manager.Manager {
	log.Info("Getting sandstorm manager for region : " + awsRegion + " and ARN : " + sandstormARN)

	// Config for us-west-2 for STS
	awsStsConfig := &aws.Config{Region: aws.String(awsRegion), Endpoint: aws.String(awsStsEndpoint)}
	awsConfig := &aws.Config{Region: aws.String(awsRegion)}
	stsclient := sts.New(session.New(awsStsConfig))

	// Create stscreds.AssumeRoleProvider with a 15 minute duration
	arp := &stscreds.AssumeRoleProvider{
		Duration:     900 * time.Second,
		ExpiryWindow: 10 * time.Second,
		RoleARN:      sandstormARN,
		Client:       stsclient,
	}

	// Create credentials and config using our Assumed Role that we
	// will use to access the main account with
	sandstormCredentials := credentials.NewCredentials(arp)
	awsConfig.WithCredentials(sandstormCredentials)

	// Finally, create a secret manager that uses the cross-account
	// config.
	return manager.New(manager.Config{
		AWSConfig: awsConfig,
		TableName: sandstormTableName,
		KeyID:     sandstormTableID,
	})
}

func getTProxSigV4Credentials() *credentials.Credentials {
	awsConfig := &aws.Config{Region: aws.String("us-west-2"), Endpoint: aws.String("sts.us-west-2.amazonaws.com")}
	stsClient := sts.New(session.New(awsConfig))

	arp := &stscreds.AssumeRoleProvider{
		Duration:     900 * time.Second,
		ExpiryWindow: 10 * time.Second,
		RoleARN:      tProxAccessARN,
		Client:       stsClient,
	}

	credentials := credentials.NewCredentials(arp)

	return credentials
}

// GetTestConfiguration returns the configuration to use for unit tests.
func GetTestConfiguration() *TeslaConfiguration {
	envVarName := "ENVIRONMENT"
	// Force the environment to be test
	err := os.Setenv(envVarName, envNameTeslaTest)
	if err != nil {
		log.Error("Error setting test environment variable")
	}
	return GetCurrentConfiguration()
}

// GetCurrentConfiguration gets the current configuration of the envrionment.
func GetCurrentConfiguration() *TeslaConfiguration {
	currentEnvironment := getCurrentEnvironment()
	return createConfiguration(currentEnvironment)
}

func createConfiguration(environment TeslaEnvironment) *TeslaConfiguration {
	if environment == TeslaProd {
		return createProdConfiguration()
	} else if environment == TeslaBeta {
		return createBetaConfiguration()
	} else if environment == TeslaTest {
		return createTestConfiguration()
	}

	return createLocalConfiguration()
}

func createTestConfiguration() *TeslaConfiguration {
	awsRegion := "us-west-2"
	awsMetricsIDSecret := "METRICS_AKID"
	awsMetricsSecretSecret := "METRICS_SECRET"
	signerCredsID := "AKID"
	signerCredsSecret := "SECRET"
	signerCredsToken := "TOKEN"

	sigV4Creds := credentials.NewStaticCredentials(signerCredsID, signerCredsSecret, signerCredsToken)

	configuration := TeslaConfiguration{
		environment:           TeslaTest,
		awsRegion:             awsRegion,
		awsMetricsCredentials: credentials.NewStaticCredentials(awsMetricsIDSecret, awsMetricsSecretSecret, ""),
		tProxSigV4Signer:      v4.NewSigner(sigV4Creds),
		tProxSigV4ServiceName: tProxSigV4ServiceName,
		tProxSigV4AwsRegion:   tProxSigV4AwsRegion,
		blacklistedClientMap:  createBlacklistedClientMap(TeslaTest),
		throttledClientMap:    createThrottledClientMap(),
		defaultThrottle:       UnlimitedRate,
	}

	return &configuration
}

func createLocalConfiguration() *TeslaConfiguration {
	awsRegion := "us-west-2"
	awsStsEndpoint := "sts.us-west-2.amazonaws.com"
	sandstormManager := getSandstormManager(awsRegion, awsStsEndpoint, devoSandstormARN)
	awsMetricsIDSecret, err := sandstormManager.Get("commerce/Tesla/tesla-dev/mws-access-key-id")
	if err != nil {
		log.Error("Error creating configuration. Couldn't fetch Id.", err)
		panic("Error creating configuration. Couldn't fetch Id.")
	}

	awsMetricsSecretSecret, err := sandstormManager.Get("commerce/Tesla/tesla-dev/mws-access-key-secret")
	if err != nil {
		log.Error("Error creating configuration. Couldn't fetch password.")
		panic("Error creating configuration. Couldn't fetch password.")
	}

	throttlingMap := createThrottledClientMap()

	configuration := TeslaConfiguration{
		environment:           TeslaLocal,
		awsRegion:             awsRegion,
		awsMetricsCredentials: credentials.NewStaticCredentials(string(awsMetricsIDSecret.Plaintext), string(awsMetricsSecretSecret.Plaintext), ""),
		tProxSigV4Signer:      v4.NewSigner(getTProxSigV4Credentials()),
		tProxSigV4ServiceName: tProxSigV4ServiceName,
		tProxSigV4AwsRegion:   tProxSigV4AwsRegion,
		blacklistedClientMap:  createBlacklistedClientMap(TeslaLocal),
		throttledClientMap:    throttlingMap,
		defaultThrottle:       UnlimitedRate,
	}

	return &configuration
}

func createBetaConfiguration() *TeslaConfiguration {
	awsRegion := "us-west-2"
	awsStsEndpoint := "sts.us-west-2.amazonaws.com"
	sandstormManager := getSandstormManager(awsRegion, awsStsEndpoint, devoSandstormARN)
	awsMetricsIDSecret, err := sandstormManager.Get("commerce/Tesla/tesla-dev/mws-access-key-id")
	if err != nil {
		log.Error("Error creating configuration. Couldn't fetch Id.")
		panic("Error creating configuration. Couldn't fetch Id.")
	}

	awsMetricsSecretSecret, err := sandstormManager.Get("commerce/Tesla/tesla-dev/mws-access-key-secret")
	if err != nil {
		log.Error("Error creating configuration. Couldn't fetch password.")
		panic("Error creating configuration. Couldn't fetch password.")
	}

	throttlingMap := createThrottledClientMap()

	configuration := TeslaConfiguration{
		environment:           TeslaBeta,
		awsRegion:             awsRegion,
		awsMetricsCredentials: credentials.NewStaticCredentials(string(awsMetricsIDSecret.Plaintext), string(awsMetricsSecretSecret.Plaintext), ""),
		tProxSigV4Signer:      v4.NewSigner(getTProxSigV4Credentials()),
		tProxSigV4ServiceName: tProxSigV4ServiceName,
		tProxSigV4AwsRegion:   tProxSigV4AwsRegion,
		blacklistedClientMap:  createBlacklistedClientMap(TeslaBeta),
		throttledClientMap:    throttlingMap,
		defaultThrottle:       UnlimitedRate,
	}

	return &configuration
}

func createProdConfiguration() *TeslaConfiguration {
	awsRegion := "us-west-2"
	awsStsEndpoint := "sts.us-west-2.amazonaws.com"
	sandstormManager := getSandstormManager(awsRegion, awsStsEndpoint, prodSandstormARN)
	awsMetricsIDSecret, err := sandstormManager.Get("commerce/Tesla/tesla-prod/mws-access-key-id")
	if err != nil {
		log.Error("Error creating configuration. Couldn't fetch Id.")
		panic("Error creating configuration. Couldn't fetch Id.")
	}

	awsMetricsSecretSecret, err := sandstormManager.Get("commerce/Tesla/tesla-prod/mws-access-key-secret")
	if err != nil {
		log.Error("Error creating configuration. Couldn't fetch password.")
		panic("Error creating configuration. Couldn't fetch password.")
	}

	throttlingMap := createThrottledClientMap()
	// To throttle a specific client on an API, call this :
	//throttleClientOnAPI(throttlingMap, <the API>, <the Client ID>, <the TPS at which the client should be throttled>)

	configuration := TeslaConfiguration{
		environment:           TeslaProd,
		awsRegion:             awsRegion,
		awsMetricsCredentials: credentials.NewStaticCredentials(string(awsMetricsIDSecret.Plaintext), string(awsMetricsSecretSecret.Plaintext), ""),
		tProxSigV4Signer:      v4.NewSigner(getTProxSigV4Credentials()),
		tProxSigV4ServiceName: tProxSigV4ServiceName,
		tProxSigV4AwsRegion:   tProxSigV4AwsRegion,
		blacklistedClientMap:  createBlacklistedClientMap(TeslaProd),
		throttledClientMap:    throttlingMap,
		defaultThrottle:       UnlimitedRate,
	}

	return &configuration
}
