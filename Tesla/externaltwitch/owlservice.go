package externaltwitch

import (
	"bytes"
	"encoding/json"
	"net/http"
	"strconv"

	"code.justin.tv/commerce/Tesla/config"
	"code.justin.tv/commerce/Tesla/external"
)

const (
	maxClientMetricNameLength = 50
	// Metrics Constants
	owlServiceLatencyMetricName = "DependencyLatency:OwlService"
	owlServiceFailureMetricName = "DependencyCallFailure:OwlService"
)

// GetClientDetailsInput is the input structure which will specify the details of the GetDetails call.
type GetClientDetailsInput struct {
	ClientID string
}

// GetClientDetailsOutput is the output structure returned from the owl call.
type GetClientDetailsOutput struct {
	ClientName       string `json:"name"`
	ClientMetricName string
}

// OwlServiceInterface are the operations supported by the owl client.
type OwlServiceInterface interface {
	GetClientDetails(GetClientDetailsInput) (GetClientDetailsOutput, external.Error)
}

type owlService struct {
	callFactory   external.CallFactory
	configuration *config.TeslaConfiguration
}

func getClientMetricName(clientName string) string {
	var buffer bytes.Buffer
	for i, c := range clientName {
		if i >= maxClientMetricNameLength {
			// Reached max length.
			break
		}

		if c >= 'A' && c <= 'Z' {
			// Alphabet chars are legal.
			buffer.WriteRune(c)
			continue
		}

		if c >= 'a' && c <= 'z' {
			// Alphabet chars are legal.
			buffer.WriteRune(c)
			continue
		}

		if c >= '0' && c <= '9' {
			// Digits are allowed
			buffer.WriteRune(c)
			continue
		}
		if c == '.' || c == ':' || c == '@' || c == '_' || c == '-' || c == '/' {
			// Legal special characters :   . : @ _ - /
			buffer.WriteRune(c)
			continue
		}

		// Everything else is replaced with _
		buffer.WriteRune('_')
	}

	return buffer.String()
}

func getStringFromMapOrEmpty(m map[string]interface{}, key string) string {
	v, exists := m[key]

	// Key does not exist in map, return empty string.
	if !exists {
		return ""
	}

	switch v.(type) {
	case string:
		return v.(string)
	default:
		// Value is not a string, return empty string.
		return ""
	}
}

func (o owlService) createRequestFromGetClientDetailsInput(input GetClientDetailsInput) external.Request {
	callMethod := "GET"
	callURL := "https://prod.owl.twitch.a2z.com/clients/" + input.ClientID
	request := external.NewSerializableRequest(callMethod, callURL, nil, nil)
	return request
}

func (o owlService) createClientDetailsOutputFromResponseString(responseBody []byte) (GetClientDetailsOutput, external.Error) {
	var data map[string]interface{}
	if err := json.Unmarshal(responseBody, &data); err != nil {
		return GetClientDetailsOutput{}, external.NewInternalServerError("There was an error deserializing the Owl response : " + err.Error())
	}

	clientName := getStringFromMapOrEmpty(data, "name")
	metricName := getClientMetricName(clientName)
	return GetClientDetailsOutput{clientName, metricName}, nil
}

func (o owlService) GetClientDetails(input GetClientDetailsInput) (GetClientDetailsOutput, external.Error) {
	request := o.createRequestFromGetClientDetailsInput(input)

	// We now have the request, get the call we should use for this request, and call it.
	call := o.callFactory.CreateCall(request)
	response, err := call.Call(request)
	if err != nil {
		return GetClientDetailsOutput{}, err
	}

	if response.GetStatusCode() != http.StatusOK {
		return GetClientDetailsOutput{}, external.NewErrorFromStatusCode(response.GetStatusCode(), "Error getting client details. Unexpected status : "+strconv.Itoa(response.GetStatusCode()))
	}

	// Now that we have the abstract call response, we convert it into something
	// slightly more legible.
	output, err := o.createClientDetailsOutputFromResponseString(response.GetBody())
	return output, err
}

// NewOwlService returns an owl service client which can make calls to owl.
func NewOwlService(apiContext external.APIContext) OwlServiceInterface {
	metricNames := external.MetricNames{LatencyName: owlServiceLatencyMetricName, FailureName: owlServiceFailureMetricName}
	// For Owl, tbere is no client scoping.
	metricContext := external.MetricContext{APIContext: apiContext, ClientMetricName: "ALL", MetricNames: metricNames}
	factory := external.MetricsCallFactory{MetricContext: metricContext}
	return owlService{callFactory: factory, configuration: apiContext.Configuration}
}
