package externaltwitch

import (
	"testing"

	"code.justin.tv/commerce/Tesla/config"
	"code.justin.tv/commerce/Tesla/external"
)

func testClientMetricName(t *testing.T, testString string, expectedString string) {
	resultString := getClientMetricName(testString)
	if resultString != expectedString {
		t.Error("TestGetClientMetricName:: Unexpected metric name. Got :", resultString, "expected :", expectedString)
	}
}

func TestGetClientMetricName(t *testing.T) {
	testClientMetricName(t, "AaBbCcxXyYzZ", "AaBbCcxXyYzZ")
	testClientMetricName(t, "AaBbCcxXyYzZ01239", "AaBbCcxXyYzZ01239")
	testClientMetricName(t, "0123456789", "0123456789")
	testClientMetricName(t, "A-Z-0-9", "A-Z-0-9")
	testClientMetricName(t, "A.Z:0@9_X-Y/Z", "A.Z:0@9_X-Y/Z")
	testClientMetricName(t, "Test with spaces", "Test_with_spaces")
	testClientMetricName(t, "Test with apostrophe's", "Test_with_apostrophe_s")
	testClientMetricName(t, "Test with dollar$ & cents", "Test_with_dollar____cents")
	testClientMetricName(t, "Test with # signs", "Test_with___signs")
	testClientMetricName(t, "!Test%", "_Test_")
	testClientMetricName(t, "(Test)", "_Test_")
	testClientMetricName(t, "*Test^", "_Test_")
	testClientMetricName(t, "5db1b951dc1dced57aec03f3a68e680b6d6c235083b8d9addc70", "5db1b951dc1dced57aec03f3a68e680b6d6c235083b8d9addc")
	testClientMetricName(t, "$db1b951dc1dced57aec03f3a68e680b6d6c235083b8d9add 70", "_db1b951dc1dced57aec03f3a68e680b6d6c235083b8d9add_")
	testClientMetricName(t, "abcăîșțà", "abc_____")
	testClientMetricName(t, "字昨夜の123汉字漢", "____123___")
	testClientMetricName(t, "コンサート-----は最高でした。", "_____-----_______")
}

func TestCreateGetClientDetailsRequest(t *testing.T) {
	configuration := config.GetTestConfiguration()
	testClientID := "testClientID"
	apiName := "testAPI"
	expectedURL := "https://prod.owl.twitch.a2z.com/clients/" + testClientID
	input := GetClientDetailsInput{testClientID}
	apiContext := external.APIContext{Configuration: configuration, MethodName: apiName, ClientID: testClientID}
	metricNames := external.MetricNames{LatencyName: owlServiceLatencyMetricName, FailureName: owlServiceFailureMetricName}
	metricContext := external.MetricContext{APIContext: apiContext, MetricNames: metricNames}
	factory := external.MetricsCallFactory{MetricContext: metricContext}
	service := owlService{factory, configuration}

	request := service.createRequestFromGetClientDetailsInput(input)

	if request.GetCallMethod() != "GET" {
		t.Error("TestCreateGetClientDetailsRequest:: should be a GET call. Got : ", request.GetCallMethod())
	}

	if request.GetCallURL() != expectedURL {
		t.Error("TestCreateGetClientDetailsRequest:: unexpected call URL. Should be : ", expectedURL, " Got : ", request.GetCallURL())
	}
}

func TestCreateClientDetailsOutputFromResponse(t *testing.T) {
	configuration := config.GetTestConfiguration()
	testClientID := "testClientID"
	apiName := "testAPI"
	apiContext := external.APIContext{Configuration: configuration, MethodName: apiName, ClientID: testClientID}
	metricNames := external.MetricNames{LatencyName: owlServiceLatencyMetricName, FailureName: owlServiceFailureMetricName}
	metricContext := external.MetricContext{APIContext: apiContext, MetricNames: metricNames}
	factory := external.MetricsCallFactory{MetricContext: metricContext}
	service := owlService{factory, configuration}
	goodJSON := []byte(`{"name":"test"}`)
	output, err := service.createClientDetailsOutputFromResponseString(goodJSON)

	if err != nil {
		t.Error("TestCreateClientDetailsOutputFromResponse:: Error creating output from : ", string(goodJSON))
	}

	if output.ClientName != "test" {
		t.Error("TestCreateClientDetailsOutputFromResponse:: Unexpected client name ", output.ClientName, " expected test")
	}

	goodJSON2 := []byte(`{"name":"test", "otherField":"test"}`)
	output, err = service.createClientDetailsOutputFromResponseString(goodJSON2)

	if err != nil {
		t.Error("TestCreateClientDetailsOutputFromResponse:: Error creating output from : ", string(goodJSON2))
	}

	if output.ClientName != "test" {
		t.Error("TestCreateClientDetailsOutputFromResponse:: Unexpected client name ", output.ClientName, " expected test")
	}

	badJSON := []byte(`{"otherField":"test"}`)
	output, err = service.createClientDetailsOutputFromResponseString(badJSON)

	if err != nil {
		t.Error("TestCreateClientDetailsOutputFromResponse:: Error creating output from : ", string(badJSON))
	}

	if output.ClientName != "" {
		t.Error("TestCreateClientDetailsOutputFromResponse:: Unexpected client name ", output.ClientName, " expected empty string")
	}

	badJSON2 := []byte(`{}`)
	output, err = service.createClientDetailsOutputFromResponseString(badJSON2)

	if err != nil {
		t.Error("TestCreateClientDetailsOutputFromResponse:: Error creating output from : ", string(badJSON2))
	}

	if output.ClientName != "" {
		t.Error("TestCreateClientDetailsOutputFromResponse:: Unexpected client name ", output.ClientName, " expected empty string")
	}
}

func TestOwlCall(t *testing.T) {
	configuration := config.GetTestConfiguration()
	testClientID := "t11hnc4bu1xj2yljd1t4dbruut4srkf"
	input := GetClientDetailsInput{testClientID}
	expectedClientName := "Hi-Rez Studios"
	expectedClientMetricName := "Hi-Rez_Studios"
	apiName := "testAPI"
	apiContext := external.APIContext{Configuration: configuration, MethodName: apiName, ClientID: testClientID}
	metricNames := external.MetricNames{LatencyName: owlServiceLatencyMetricName, FailureName: owlServiceFailureMetricName}
	metricContext := external.MetricContext{APIContext: apiContext, MetricNames: metricNames}
	factory := external.MetricsCallFactory{MetricContext: metricContext}
	service := owlService{factory, configuration}
	output, err := service.GetClientDetails(input)

	if err != nil {
		t.Error("TestOwlCall:: Error while making call.")
	}

	if output.ClientName != expectedClientName {
		t.Error("TestOwlCall:: Unexpected ClientName returned :", output.ClientName, "expected :", expectedClientName)
	}

	if output.ClientMetricName != expectedClientMetricName {
		t.Error("TestOwlCall:: Unexpected ClientMetrciName returned :", output.ClientMetricName, "expected :", expectedClientMetricName)
	}
}
