import json, argparse, os, sys, random

def getTuidToUse(tuidsDict, tuidsUsed, ensureUniqueTuids):
    # Ensure the same tuid isn't used multiple times for any given second
    if ensureUniqueTuids:
        tuidsToUse = random.choice(tuidsDict.keys())
        # This is super slow, but ensures greater randomness than always converting to a list
        # and grabbing the first X many elements
        # TODO: Improve this 
        while tuidsToUse in tuidsUsed:
            tuidsToUse = random.choice(tuidsDict.keys())
        tuidsUsed[tuidsToUse] = True
        return tuidsToUse
    else:
        return random.choice(tuidsDict.keys())

def generateLoadTestScript (tpsTarget, desiredRuntime):
    print "Generating load test script..."

    fileName = "loadTest-" + desiredRuntime + '-' + str(tpsTarget) + "TPS.sh"
    try:
        os.remove(fileName)
    except OSError:
        pass

    # Create the load test script
    with open(fileName, 'w') as f:
        f.write('./parallel.sh ')
        for x in range(1, int(tpsTarget) + 1):
            f.write("\"./loadTest.sh " + desiredRuntime + " 1 " + str(x) + " " + desiredRuntime + "-" + str(tpsTarget) + "TPS\" ")
        f.close()
    os.chmod(fileName, 0777)
    print "Finished generating load test script"

def generateClientIdsDictionary (target):
    print "Generating clientID dictionary..."
    clientIdDict = generateDictionary(target)
    print "Generated the following number of clientID: " + str(len(clientIdDict))
    return clientIdDict

def generateTuidsDictionary (target):
    print "Generating tuid dictionary..."
    tuidsDict = generateDictionary(target)
    print "Generated the following number of tuids: " + str(len(tuidsDict))
    return tuidsDict

def generateDictionary (target):
    someDict = {}
    with open(target, 'r') as f:
        content = f.readlines()
        content = [x.strip() for x in content]
        f.close()
    for i in content:
        someDict[i] = True
    return someDict
