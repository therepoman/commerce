if [ -z "$1" ]
  then
    echo "No duration specified."
    exit 1
fi

if [ -z "$2" ]
  then
    echo "No load test rate specified."
    exit 1
fi

if [ -z "$3" ]
  then
    echo "No target identifier supplied."
    exit 1
fi

vegeta attack -lazy -duration=$1 -rate=$2 < targets/target$3.txt > load_test_$1_$2_$3.bin
(
  flock 200
  echo "------- Target : $3 | target$3.txt --------" >> runReport.txt
  vegeta report load_test_$1_$2_$3.bin >> runReport.txt
  vegeta encode -to=csv -output=load_test_$1_$2_$3.csv load_test_$1_$2_$3.bin
  cat load_test_$1_$2_$3.csv >> runReport.csv
  echo "-------------------------------------------" >> runReport.txt
) 200>loadTest.lock

# Cleanup temp files.
rm -f load_test_$1_$2_$3.csv
rm -f load_test_$1_$2_$3.bin
