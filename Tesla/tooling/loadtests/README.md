# TES Load Tests

## Prerequisites:

- Install [vegeta](https://github.com/tsenart/vegeta). If you are on a Mac you can do a brew install.
    - brew install vegeta

- Install [flock](https://github.com/discoteq/flock). If you are on a Mac you can do a brew install. 
    - brew tap discoteq/discoteq
    - brew install flock

- Need [to get temporary credentials](https://w.amazon.com/bin/view/AWS_IT_Security/Isengard/FAQ/#I.27m_using_the_AWS_CLI_and_need_to_get_the_temporary_credentials_for_a_role.2C_where_are_they.3F) for the tes isengard account to get tuids from cloudwatch logs (instructions below).

- Everything should be done from the `tooling/loadtests` directory.

## To run a load test:

- Get the cloudwatch log output which we will pull tuids from.
    - `aws configure`
    ```
            AWS Access Key ID [****************LVKU]:
            AWS Secret Access Key [****************18LC]:
            Default region name [us-east-1]: us-west-2
            Default output format [json]:
    ```
    - to get a start time of one day from now in milliseconds go to a js console and run `Date.now() - 86400000`
    - `aws logs filter-log-events --log-group-name /aws/elasticbeanstalk/tesla-prod-commerce-Tesla-env/var/log/eb-docker/containers/eb-current-app/stdouterr.log --start-time <start time in ms> --end-time <end time in ms> --max-items 2000 --filter-pattern "Received valid call" >> <output log file>`

- Get unique tuids in their own file.
    - `python generateUniqueTuidFile.py <log file output> <file to output tuids into>`
    
- Create a file with clientId's seperated by newline <file containing client ids>
    ```
    euq9t2u79ne77aw5s5qtu3sinciodg
    v2cr7jjy56uvdjnmd9nbeabp64wzz9
    qpwtcnxpaxrd7b17342d7l2prb39qg
    117biisdsm4k40goic577hinmflxkz
    ndup1vj45moqjotjfwdgyfawov87mr
    mfieqnayt3vog4ucp023xjmdcoh35nu
    0h4q6v988okbfeohzzdkggybyi56c2
    reqyg5nncuvro31ptiu55q88ule5v3
    rg5jwqeyulmyy5s5zsu7juu2l3q1ue
    0xlguzmhp5zfpv91dzmn6wlrkqqohf
    4k6yw8c8c88ayysruxa891daspvgh5
    f079npi11eairurnpwx2b7ndtjpjef
    9cf74odawp2u15kaylmcs9sqq4bxyt
    t11hnc4bu1xj2yljd1t4dbruut4srkf
    5tktwmis1hnnduydd73w3dygb1gddc
    dw1segz99n0yo3y4lngcph092b232v1
    71xzlye6l5oxlcenljb0xp52r771fc3
    mlpjzuoqffzsj3obfbtwhlxeuepuvh
    vedkfdtbe4owz3qlhuu93tij5nml8l
    s8erqjfwkt7es7qndatltyh950cq8f
    mlizwkzacbu7hixt8zrgx716onsmch
    upzawv62czzo3go4sum19jidmybenr
    jxon6abzs4puuhidszg6lcgt7uq1hr
    qkypmhc5pr4znswk9t06ygvcxytdvg
    g3gny3ygsa7twz3dl14se8hs57g5qy
    h4q7uu618wvg1cgbk67k76biwo7f0m
    vknv6dhtwq1frhaapk2n2ij5jmsf6y
    30a2xrlu4o1nhs0bpvnwn0oogfzyn0
    hayh5z45olzruje9af7td468zlf4ga
    7emdh4zhj7vf2v5yxuqozn6io86ida
    mqc6bgt8qjqjp5r4cb2yrnldby54om
    vycw96u8frpppozb02h9j0tid8fivg
    za0jchcwkz6d8ynho175jb7p6h2p20
    ci5x30imit586d5sb4qr5e10rrmkin
    s5nmfixcax6f9zez4qdcm8ihoh5qxc
    kimne78kx3ncx6brgo4mv6wki5h1ko
    ik4kgnclcg9gv59ft7voz7ho5zzyuo2
    07uher3blowv7sn6lowd3d5gq5i1zp
    fnkzwj509yyr3u76ltk57nzhkhdd8g
    1nttdjtscx5lm7cmaxzu1f12ng75b3
    zvfmcv3xtwfkkh00djx519bout0q3i
    2hfbh2la7li2v18rw47rdpqw1569el
    61jo7c3xdo5k2vd50oqiqbg80omjdz
    uun1etb5kef2xqcckzs5junqouzint
    ```

- Get the shell script to actually begin running the load tests.
    - `python generateTeslaLoad.py <file containing tuids> <file containing client ids> <target tps> <duration> (5m for minutes) <environment: local, staging or prod> (staging for dev)`

- Run the script that is spit out of the generateTeslaLoad command. Example: `./loadTest-1m-100TPS.sh`

- Check runReport.csv and runReport.txt for information on the run.


