import sys, argparse, os

def generateUniqueTuidsFile (tuidsDict, outputFile):
    print "Generating unique tuid file..."

    try:
        os.remove(outputFile)
    except OSError:
        pass

    # Create the unique tuid file
    with open(outputFile, 'w') as f:
        firstLine = True
        for x in tuidsDict:
            if firstLine:
                f.write(x)
                firstLine = False
            else:
                f.write("\n" + x) 
        f.close()
    print "Finished generating unique tuid script"

def generateTuidDictionary (target):
    print "Generating tuid dictionary..."
    tuidsDict = {}
    # Create the dictionary of unique tuids
    with open(target, 'r') as f:
        content = f.readlines()
        content = [x.strip() for x in content]
        f.close()
    for i in content:
        if "message" in i and "GetGoodsForUser" in i:
            tuidPart = i.split(" - ")[1]
            tuid = tuidPart.split("\\")[0]
            tuidsDict[tuid] = True
    print "Generated the following number of tuids: " + str(len(tuidsDict))
    return tuidsDict

def main(argv):
    usage = '''
    Take a CloudWatch log ouput for a 'Received valid call' search and generate a file of unique tuids
    Call with -h for argument help.
    Sample call: python generateUniqueTuidFile.py cloudwatchData.txt tuids.txt
    '''
    parser = argparse.ArgumentParser(usage=usage)
    parser.add_argument('inputFile', help='''
        the file containing the CloudWatch search data
        Example: cloudwatchData.txt''')
    parser.add_argument('outputFile', help='''
        the file to generate (the file that will have the unique tuids)
        Example: tuids.txt''')
    args = parser.parse_args()

    tuidsDict = generateTuidDictionary(args.inputFile)
    generateUniqueTuidsFile(tuidsDict, args.outputFile)
    return

if __name__ == "__main__":
    main(sys.argv)