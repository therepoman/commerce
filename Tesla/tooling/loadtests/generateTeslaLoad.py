import json, argparse, os, sys, random, shutil, generateGenericTargets


endpointMap = {
    'local': 'http://localhost:8000/commerce/user/goods?tuid=',
    'staging' : 'http://tesla-dev-commerce-Tesla-env.9apa4ngbzg.us-west-2.elasticbeanstalk.com/commerce/user/goods?tuid=',
    'prod' : 'http://tesla-prod-commerce-Tesla-env.9pmqp95afb.us-west-2.elasticbeanstalk.com/commerce/user/goods?tuid='
}

def generateTargets(tuidsDict, clientIdsDict, tpsTarget, desiredRuntime, url, ensureUniqueTuids):
    print "Generating targets for: " + url

    runtime = int(filter(str.isdigit, desiredRuntime))

    # Construct the required directory (and delete any old one)
    try:
        shutil.rmtree('targets')
    except OSError:
        pass
    os.mkdir('targets')


    tuidsUsed = {}
    # Delete old files and prep them for usage
    for x in range(1, int(tpsTarget) + 1):
        targetName = 'targets/target' + str(x) + '.txt'
        # Sanity removal of existing files
        try:
            os.remove(targetName)
        except OSError:
            pass

        tuidsToUse = generateGenericTargets.getTuidToUse(tuidsDict, tuidsUsed, ensureUniqueTuids)
        with open(targetName, 'w') as f:
            f.write('POST ' + url + tuidsToUse + '\nTwitch-Client-ID: ' + random.choice(clientIdsDict.keys()) + '\n')
            f.close()
    print "Wrote " + str(tpsTarget) + " initial files with a single request each."

    # Fully form the targets
    requestNum = 1
    print "Generating enough requests to handle 1.5x the TPS required for the following time: " + desiredRuntime
    # Generate 1.5x the number of requests needed for your TPS, targets combo
    while requestNum < (60 * runtime * 1.5):
        # Reset the dictionary for every second
        tuidsUsed = {}
        for x in range(1, int(tpsTarget) + 1):
            targetName = 'targets/target' + str(x) + '.txt'
            tuidsToUse = generateGenericTargets.getTuidToUse(tuidsDict, tuidsUsed, ensureUniqueTuids)

            with open(targetName, 'a') as f:
                f.write('\nPOST ' + url + tuidsToUse + '\nTwitch-Client-ID: ' + random.choice(clientIdsDict.keys()) + '\n')
                f.close()
        requestNum += 1
        print "Wrote " + str(requestNum) + " requests for each thread."
    print "Wrote " + str(tpsTarget) + " files with " + str(requestNum) + " requests each."

def main(argv):
    usage = '''
    Take a file of tuids and generate load test scripts (and load test resources) for the file
    Call with -h for argument help.
    Sample call: python generateTeslaLoad.py tuids.txt clients.txt 100 10m prod --uniqueTuids=True
    '''
    parser = argparse.ArgumentParser(usage=usage)
    parser.add_argument('tuidFile', help='''
        the file containing the tuids (one tuid per line, newline separated)
        Example: tuids.txt''')
    parser.add_argument('clientsFile', help='''
        the file containing the clientIds (one clientId per line, newline separated)
        Example: clients.txt''')
    parser.add_argument('tps', help='''
        the desired tps. Must be an int
        Example: 100''')
    parser.add_argument('runTime', help='''
        the desired time to run the load test in minutes
        Example: 10m''')
    parser.add_argument('stage', help='''
        the desired stage to run the test against ('local', 'staging', or 'prod')
        Example: prod''')
    parser.add_argument('--ensureUnique', required=False, help='''
        Optional parameter on whether to ensure unique tuids PER TPS
        e.g. if True, then there should NEVER be any requests in the same second with the same tuid
        Note: this can be VERY slow and means that the number of tuids passed in MUST be greater than the desired TPS
        Example: --ensureUnique=True''')
    args = parser.parse_args()

    if args.stage.lower() not in endpointMap:
        print "The desired stage must be 'local', 'staging', or 'prod'"
        return
    # Construct the relevant dictionaries
    tuidsDict = generateGenericTargets.generateTuidsDictionary(args.tuidFile)
    clientIdsDict = generateGenericTargets.generateClientIdsDictionary(args.clientsFile)
    # Construct the load test script
    generateGenericTargets.generateLoadTestScript(args.tps, args.runTime)

    # Determine whether to follow the unique tuids constraint
    ensureUniqueTuids = False
    if args.ensureUnique != None and args.ensureUnique.lower() == 'true':
        ensureUniqueTuids = True

    # Construct the requests
    url = endpointMap[args.stage.lower()]
    generateTargets(tuidsDict, clientIdsDict, args.tps, args.runTime, url, ensureUniqueTuids)
    return

if __name__ == "__main__":
    main(sys.argv)


