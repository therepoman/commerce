import sys, argparse, json, requests

def main(argv):
    usage = '''
    Script to call the TESla Prod fulfillment lambda (update the fulfillment DynamoDB table) directly
    NOTE: this will edit a prod database. Use at your own risk!
    Call with -h for argument help.
    Sample call: python teslaFulfillmentLambdaCall.py SOME_API_KEY setFulfillmentLambdaBody.json
    '''
    parser = argparse.ArgumentParser(usage=usage)
    parser.add_argument('key', help='''
        the api key to call the lambda with.
        Can be found in fuel-tes AWS account under API Gateway settings
        Example: SOME_API_KEY''')
    parser.add_argument('jsonFile', help='''
        json file that acts as the body of the fulfill lambda call
        Example: setFulfillmentLambdaBody.json''')
    args = parser.parse_args()

    # Establish call
    url = 'https://f4792tk68a.execute-api.us-west-2.amazonaws.com/prod/set-fulfillment-address'
    headers = {'Content-Type': 'application/json','x-api-key': args.key}
    body = json.load(open(args.jsonFile))

    print "Making service call..."
    r = requests.post(url, headers=headers, data=json.dumps(body))

    # Parse the response
    print "Obtained response:"
    jsonResponse = json.loads(r.text)
    print json.dumps(jsonResponse, indent=4)

if __name__ == "__main__":
    main(sys.argv)