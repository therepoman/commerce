import sys, argparse, json, requests

endpointMap = {
    'local': 'http://localhost:8000/v5/commerce/user/goods/fulfill?api_version=5',
    'staging' : 'https://visage.staging.us-west2.justin.tv/kraken/commerce/user/goods/fulfill?api_version=5',
    'prod' : 'https://api.twitch.tv/kraken/commerce/user/goods/fulfill?api_version=5'
}

def main(argv):
    usage = '''
    Script to call a Visage setReceiptsFulfillmentStatus (/commerce/user/goods/fulfill) endpoint directly 
    Call with -h for argument help.
    Sample call: python userGoodsFulfillVisage.py staging OAuth SOME_TOKEN userGoodsFulfillBody.json --clientID=OPTIONAL_CLIENT_ID
    '''
    parser = argparse.ArgumentParser(usage=usage)
    parser.add_argument('environment', help='''
        Endpoint to hit
        Example: 'local', 'staging', or 'prod''')
    parser.add_argument('authType', help='''
        type of auth, either OAuth or 'Bearer'/ 'extJWT'.
        Note: Bearer = extJWT
        Note: Bearer paths are untested...
        Example: 'OAuth', 'Bearer', 'extJWT''')
    parser.add_argument('token', help='''
        the auth token. Either a Bearer token or OAuth token
        Example: SOME_TOKEN''')
    parser.add_argument('jsonFile', help='''
        json file that acts as the body of the fulfill call
        Example: userGoodsFulfillBody.json''')
    parser.add_argument('--clientID', required=False, help='''
        Optional Client ID, required for Bearer token calls.
        This is recommended if you have it
        Example: ABC123CLIENTID''')
    args = parser.parse_args()

    # Ensure we only have acceptable environments
    if args.environment.lower() not in endpointMap:
        print 'Please pass either "local," "staging," or "prod" for the environment'
        return

    # Ensure we only have supported auth
    isOauth = False
    if args.authType.lower() == 'oauth':
        isOauth = True
    elif args.authType.lower() == 'bearer' or args.authType.lower() == 'extjwt':
        isOauth = False
    else:
        print 'Please pass either "OAuth," "Bearer," or "extJWT" for the authType'
        return

    # Set the URL and auth
    url = endpointMap[args.environment.lower()]
    headers = {'Accept': 'application/vnd.twitchtv.v5+json', 'Content-Type': 'application/json', 'Accept-Charset': 'UTF-8'}
    auth = ''
    if isOauth:
        auth = 'OAuth ' + args.token
    else:
        auth = 'Bearer ' + args.token
    headers['Authorization'] = auth

    # Add the Client-ID if possible
    if not isOauth and args.clientID is None:
        print 'clientID is required for Bearer calls. Please add --clientID=SOME_CLIENT_ID to script parameters'
        return
    if args.clientID is not None:
        headers['Client-ID'] = args.clientID

    body = json.load(open(args.jsonFile))

    print "Making service call..."
    r = requests.post(url, headers=headers, data=json.dumps(body))

    # Parse the response
    print "Obtained response:"
    jsonResponse = json.loads(r.text)
    print json.dumps(jsonResponse, indent=4)

if __name__ == "__main__":
    main(sys.argv)