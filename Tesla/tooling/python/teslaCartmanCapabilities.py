import sys, argparse, json, requests

def main(argv):
    usage = '''
    Script to call the internal Cartman endpoint to verify TESla API capabilities
    Call with -h for argument help.
    Sample call: python teslaCartmanCapabilities.py OAuth SOME_TOKEN --clientID=OPTIONAL_CLIENT_ID
    '''
    parser = argparse.ArgumentParser(usage=usage)
    parser.add_argument('authType', help='''
        type of auth, either OAuth or 'Bearer'/ 'extJWT'. Note: Bearer = extJWT
        Example: 'OAuth', 'Bearer', 'extJWT''')
    parser.add_argument('token', help='''
        the auth token. Either a Bearer token or OAuth token
        Example: SOME_TOKEN''')
    parser.add_argument('--clientID', required=False, help='''
        Optional Client ID, required for Bearer token calls.
        This is recommended if you have it
        Example: ABC123CLIENTID''')
    args = parser.parse_args()


    # Ensure we only have supported auth
    isOauth = False
    if args.authType.lower() == 'oauth':
        isOauth = True
    elif args.authType.lower() == 'bearer' or args.authType.lower() == 'extjwt':
        isOauth = False
    else:
        print 'Please pass either "OAuth," "Bearer," or "extJWT" for the authType'
        return

    # Set the URL and auth
    url = 'https://cartman-dev.twitch.a2z.com/authorization_token?key=hmac.key&capabilities=tesla::get_goods_for_user,tesla::set_status_for_receipts'
    headers = {'Content-Type': 'application/json'}
    auth = ''
    if isOauth:
        auth = 'OAuth ' + args.token
    else:
        auth = 'Bearer ' + args.token
    headers['Authorization'] = auth

    # Add the Client-ID if possible
    if not isOauth and args.clientID is None:
        print 'clientID is required for Bearer calls. Please add --clientID=SOME_CLIENT_ID to script parameters'
        return
    if args.clientID is not None:
        headers['Client-ID'] = args.clientID

    print "Making service call..."
    r = requests.get(url, headers=headers)

    # Parse the response
    print "Obtained response:"
    jsonResponse = json.loads(r.text)
    print json.dumps(jsonResponse, indent=4)
    return

if __name__ == "__main__":
    main(sys.argv)
