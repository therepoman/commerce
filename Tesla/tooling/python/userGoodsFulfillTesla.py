import sys, argparse, json, requests

endpointMap = {
	'local': 'http://localhost:8000/commerce/user/goods/fulfill?tuid=',
	'staging' : 'http://tesla-dev-commerce-Tesla-env.9apa4ngbzg.us-west-2.elasticbeanstalk.com/commerce/user/goods/fulfill?tuid=',
	'prod' : 'http://tesla-prod-commerce-Tesla-env.9pmqp95afb.us-west-2.elasticbeanstalk.com/commerce/user/goods/fulfill?tuid='
}

def main(argv):
    usage = '''
    Script to call a TESla setReceiptsFulfillmentStatus (/commerce/user/goods/fulfill) endpoint directly 
    Call with -h for argument help.
    Sample call: python userGoodsFulfillTesla.py staging 1234 ABC123CLIENTID userGoodsFulfillBody.json
    '''
    parser = argparse.ArgumentParser(usage=usage)
    parser.add_argument('environment', help='''
        Endpoint to hit
        Example: 'local', 'staging', or 'prod''')
    parser.add_argument('tuid', help='''
        tuid associated with the entitlements
        Example: 1234''')
    parser.add_argument('clientId', help='''
        tuid associated with the desired skus
        Example: ABC123CLIENTID''')
    parser.add_argument('jsonFile', help='''
        json file that acts as the body of the fulfill call
        Example: userGoodsFulfillBody.json''')
    args = parser.parse_args()

    # Ensure we only have acceptable environments
    if args.environment.lower() not in endpointMap:
        print 'Please pass either "local," "staging," or "prod" for the environment'
        return

    url = endpointMap[args.environment.lower()] + args.tuid
    headers = {'Content-Type': 'application/json', 'Accept-Charset': 'UTF-8', 'Twitch-Client-ID' : args.clientId}
    body = json.load(open(args.jsonFile))

    print "Making service call..."
    r = requests.post(url, headers=headers, data=json.dumps(body))

    # Parse the response
    print "Obtained response:"
    jsonResponse = json.loads(r.text)
    print json.dumps(jsonResponse, indent=4)
    return

if __name__ == "__main__":
    main(sys.argv)