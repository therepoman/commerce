import sys, userGoodsCommon, argparse

endpointMap = {
	'local': 'http://localhost:8000/commerce/user/goods?tuid=',
	'staging' : 'http://tesla-dev-commerce-Tesla-env.9apa4ngbzg.us-west-2.elasticbeanstalk.com/commerce/user/goods?tuid=',
	'prod' : 'http://tesla-prod-commerce-Tesla-env.9pmqp95afb.us-west-2.elasticbeanstalk.com/commerce/user/goods?tuid='
}

def main(argv):
    usage = '''
    Script to call a TESla userGoods (/commerce/user/goods) endpoint directly 
    Call with -h for argument help.
    Sample call: python userGoodsTesla.py staging 1234 ABC123CLIENTID
    '''
    parser = argparse.ArgumentParser(usage=usage)
    parser.add_argument('environment', help='''
        Endpoint to hit
        Example: 'local', 'staging', or 'prod''')
    parser.add_argument('tuid', help='''
        tuid associated with the entitlements
        Example: 1234''')
    parser.add_argument('clientId', help='''
        tuid associated with the desired skus
        Example: ABC123CLIENTID''')
    args = parser.parse_args()

    # Ensure we only have acceptable environments
    if args.environment.lower() not in endpointMap:
    	print 'Please pass either "local," "staging," or "prod" for the environment'
    	return

    url = endpointMap[args.environment.lower()] + args.tuid
    headers = {"Twitch-Client-ID" : args.clientId}
    return userGoodsCommon.commonUserGoods(url, headers)

if __name__ == "__main__":
    main(sys.argv)