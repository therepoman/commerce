import json, os, requests

def commonUserGoods(url, headersDictionary):
    print "Making service call..."
    r = requests.post(url, headers=headersDictionary)

    # Parse the response
    print "Obtained response:"
    jsonResponse = json.loads(r.text)
    print json.dumps(jsonResponse, indent=4)

    # Determine which goods require fulfillment
    toUpdate = []
    for i in jsonResponse['goods']:
        if (i['next_instruction'] == 'FULFILL'):
            print "Will need to fulfill " + i['receipt_id']
            tempDictionary = {'last_instruction':'FULFILL', 'fulfillment_address':"fulfillment address", 'receipt_id':i['receipt_id']}
            toUpdate.append(tempDictionary)

    if len(toUpdate) > 0:
        # Update the userGoodsFulfillBody JSON file
        print 'Updating userGoodsFulfillBody.json with new receipt_ids...'
        userGoodsFulfillBody = 'userGoodsFulfillBody.json'
        with open(userGoodsFulfillBody, 'r') as f:
            data = json.load(f)
            data['receipt_updates'] = toUpdate

        os.remove(userGoodsFulfillBody)
        with open(userGoodsFulfillBody, 'w') as f:
            json.dump(data, f, indent=4)

        # Update the setFulfillmentLambdaBody JSON file
        print 'Updating setFulfillmentLambdaBody.json with new receipt_ids...'
        setFulfillmentLambdaBody = 'setFulfillmentLambdaBody.json'
        with open(setFulfillmentLambdaBody, 'r') as f:
            data = json.load(f)
            data['receiptId'] = toUpdate[0]['receipt_id']

        os.remove(setFulfillmentLambdaBody)
        with open(setFulfillmentLambdaBody, 'w') as f:
            json.dump(data, f, indent=4)
            
        print 'Finished. Run userGoodsFulfill to fulfill.'
    else:
        print 'Nothing requires fulfillment...'



