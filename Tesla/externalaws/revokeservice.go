package externalaws

import (
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"

	"code.justin.tv/commerce/Tesla/external"

	log "github.com/Sirupsen/logrus"
	"github.com/guregu/dynamo"
)

// Constants for dynamodb and metrics
const (
	DynamoDBRevokeTableName = "tes-revoke"
	DynamoDBUserColumn      = "userId"
	DynamoDBReceiptIDColumn = "receiptId"
	DynamoDBSkuColumn       = "sku"
	DynamoDBUserSkuIndex    = "userId-sku-index"

	revokeDynamoLatencyMetricName = "DependencyLatency:RevokeDynamo"
	revokeDynamoFailureMetricName = "DependencyCallFailure:RevokeDynamo"
)

// RevokedGood is the general revoked good structure
type RevokedGood struct {
	UserID    string `json:"userId"`
	ReceiptID string `json:"receiptId"`
	Sku       string `json:"sku"`
	Completed bool   `json:"completed"`
}

// DynamoRevokedGood is the dynamo revoked good structure
type DynamoRevokedGood struct {
	UserID    string `dynamo:"userId,hash" index:"userId-sku-index,hash"`
	ReceiptID string `dynamo:"receiptId,range"`
	Sku       string `dynamo:"sku" index:"userId-sku-index,range"`
	Completed bool   `dynamo:"completed"`
}

type dynamoRevokeService struct {
	revokeTable   dynamo.Table
	metricContext external.MetricContext
}

// RevokeServiceInterface is our interface to the revoke lambda.
type RevokeServiceInterface interface {
	CreateRevoke(UserID string, ReceiptID string, Sku string) (bool, external.Error)
	GetRevokesByUser(UserID string) ([]RevokedGood, external.Error)
	GetRevokesByUserAndSku(UserID string, Sku string) ([]RevokedGood, external.Error)
	CompleteRevokeByReceipt(userID string, receiptID string) (bool, external.Error)
}

func convertRevokeDynamoToJSON(revoke *DynamoRevokedGood) *RevokedGood {
	if revoke == nil {
		return nil
	}

	return &RevokedGood{
		UserID:    revoke.UserID,
		ReceiptID: revoke.ReceiptID,
		Sku:       revoke.Sku,
		Completed: revoke.Completed,
	}
}

func (r dynamoRevokeService) CreateRevoke(userID string, receiptID string, sku string) (bool, external.Error) {
	metricName := ":CreateRevoke"
	createStart := time.Now()
	entry := &DynamoRevokedGood{
		UserID:    userID,
		ReceiptID: receiptID,
		Sku:       sku,
		Completed: false,
	}

	err := r.revokeTable.Put(entry).Run()

	if err != nil {
		postDynamoPutFailureCountMetric(metricName, r.metricContext)
		log.Error("Error storing revoke for " + receiptID + " :: " + sku)
		return false, external.NewInternalServerError("Error while creating revoke.")
	}

	postLatencyMetric(createStart, metricName, r.metricContext)
	postDynamoPutSuccessCountMetric(metricName, r.metricContext)

	log.Info("Revoke created in dynamoDB for receiptID " + receiptID)

	return true, nil
}

func (r dynamoRevokeService) CompleteRevokeByReceipt(userID string, receiptID string) (bool, external.Error) {
	metricName := ":CompleteRevokeByReceipt"
	completeStart := time.Now()
	var good DynamoRevokedGood
	query := r.revokeTable.Get(DynamoDBUserColumn, userID).
		Range(DynamoDBReceiptIDColumn, dynamo.Equal, receiptID)
	err := query.One(&good)

	if err != nil {
		postDynamoPutFailureCountMetric(metricName, r.metricContext)
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case "ValidationException":
				return false, external.NewBadRequestError("Unable to find revoke object for receiptID " + receiptID)
			default:
				return false, external.NewInternalServerError("Issue finding revoke object for receiptID " + receiptID)
			}
		}
	}

	// We don't want to update anything if the status is already completed
	if good.Completed {
		postDynamoPutFailureCountMetric(metricName, r.metricContext)
		log.Error("ReceiptID " + receiptID + " revoking status is already completed")
		return false, external.NewBadRequestError("ReceiptID " + receiptID + " revoking status is already completed")
	}

	good.Completed = true
	err = r.revokeTable.Put(good).Run()

	if err != nil {
		postDynamoPutFailureCountMetric(metricName, r.metricContext)
		log.Error("Error storing revoke fulfillment status for receiptID " + receiptID)
		return false, external.NewInternalServerError("Error storing revoke fulfillment status")
	}

	postLatencyMetric(completeStart, metricName, r.metricContext)
	postDynamoPutSuccessCountMetric(metricName, r.metricContext)

	log.Info("Completed revoke in dynamoDB for receiptID " + receiptID)

	return true, nil
}

func (r dynamoRevokeService) GetRevokesByUser(userID string) ([]RevokedGood, external.Error) {
	metricName := ":GetRevokesByUser"
	getStart := time.Now()
	var results []DynamoRevokedGood
	var resultRevokes = make([]RevokedGood, 0)

	query := r.revokeTable.Get(DynamoDBUserColumn, userID)
	err := query.All(&results)

	if err != nil {
		postDynamoPutFailureCountMetric(metricName, r.metricContext)
		log.Error("Error getting revoke for user "+userID, err)
		return nil, external.NewInternalServerError("Error while getting revoke by user id.")
	}

	for i := range results {
		convertedRevoke := convertRevokeDynamoToJSON(&results[i])
		resultRevokes = append(resultRevokes, *convertedRevoke)
	}

	postLatencyMetric(getStart, metricName, r.metricContext)
	postDynamoPutSuccessCountMetric(metricName, r.metricContext)

	log.Info("Successfully returned revokes from DynamoDB for user " + userID)

	return resultRevokes, nil
}

func (r dynamoRevokeService) GetRevokesByUserAndSku(userID string, sku string) ([]RevokedGood, external.Error) {
	metricName := ":GetRevokesByUserAndSku"
	getStart := time.Now()
	var results []DynamoRevokedGood
	var resultRevokes = make([]RevokedGood, 0)

	query := r.revokeTable.Get(DynamoDBUserColumn, userID).
		Range(DynamoDBSkuColumn, dynamo.Equal, sku).
		Index(DynamoDBUserSkuIndex)
	err := query.All(&results)

	if err != nil {
		postDynamoPutFailureCountMetric(metricName, r.metricContext)
		log.Error("Error getting revoke for user with sku " + sku)
		return nil, external.NewInternalServerError("Error while getting revoke by user id and sku.")
	}

	for i := range results {
		convertedRevoke := convertRevokeDynamoToJSON(&results[i])
		resultRevokes = append(resultRevokes, *convertedRevoke)
	}

	postLatencyMetric(getStart, metricName, r.metricContext)
	postDynamoPutSuccessCountMetric(metricName, r.metricContext)

	log.Info("Successfully returned revokes from DynamoDB for user " + userID + "and sku " + sku)

	return resultRevokes, nil
}

// NewDynamoRevokeService returns a revoke service interface that can be use to store the revoke by calling a dynamo table.
func NewDynamoRevokeService(apiContext external.APIContext, clientMetricName string) RevokeServiceInterface {
	metricNames := external.MetricNames{LatencyName: revokeDynamoLatencyMetricName, FailureName: revokeDynamoFailureMetricName}
	metricContext := external.MetricContext{APIContext: apiContext, ClientMetricName: clientMetricName, MetricNames: metricNames}

	db := dynamo.New(session.New(), &aws.Config{Region: aws.String(apiContext.Configuration.GetAwsRegion())})
	revokeTable := db.Table(DynamoDBRevokeTableName)

	return &dynamoRevokeService{revokeTable: revokeTable, metricContext: metricContext}
}
