package externalaws

import (
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"

	"code.justin.tv/commerce/Tesla/external"

	log "github.com/Sirupsen/logrus"
	"github.com/guregu/dynamo"
)

// Constants for dynamodb and metrics
const (
	DynamoDBFulfillmentAddressTableName = "tuid-receipt-fulfillment"

	fulfillmentAddressDynamoLatencyMetricName = "DependencyLatency:FulfillmentAddressDynamo"
	fulfillmentAddressDynamoFailureMetricName = "DependencyCallFailure:FulfillmentAddressDynamo"
)

// UpdateFulfillmentAddressInput is the input structure to be posted to the fulfillment address lambda.
type UpdateFulfillmentAddressInput struct {
	Tuid               string  `json:"tuid"`
	ReceiptID          *string `json:"receiptId"`
	ClientID           string  `json:"clientId"`
	FulfillmentAddress *string `json:"fulfillmentAddress"`
}

// UpdateFulfillmentAddressOutput is the output structure returned from the lambda. It solely returns if it was successful
type UpdateFulfillmentAddressOutput struct {
	Success bool
}

// FullfillmentAddressServiceInterface is our interface to the fulfillment address lambda.
type FullfillmentAddressServiceInterface interface {
	UpdateFulfillmentAddress(UpdateFulfillmentAddressInput) (UpdateFulfillmentAddressOutput, external.Error)
}

type dynamoFulfillmentAddressService struct {
	fulfillmentAddressTable dynamo.Table
	metricContext           external.MetricContext
}

type dynamoFulfillmentAddressEntry struct {
	Tuid                 string `dynamo:"tuid,hash"`
	ClientIDAndReceiptID string `dynamo:"clientId:receiptId,range"`
	FulfillmentAddress   string `dynamo:"fulfillmentAddress"`
}

func (f dynamoFulfillmentAddressService) UpdateFulfillmentAddress(input UpdateFulfillmentAddressInput) (UpdateFulfillmentAddressOutput, external.Error) {
	metricName := ":UpdateFulfillmentAddress"
	updateStart := time.Now()
	entry := &dynamoFulfillmentAddressEntry{
		Tuid:                 input.Tuid,
		ClientIDAndReceiptID: input.ClientID + ":" + *input.ReceiptID,
		FulfillmentAddress:   *input.FulfillmentAddress,
	}

	err := f.fulfillmentAddressTable.Put(entry).Run()

	if err != nil {
		postDynamoPutFailureCountMetric(metricName, f.metricContext)
		log.Error("Error storing fulfillment address for " + input.Tuid + " :: " + input.ClientID + " :: " + *input.ReceiptID + " :: " + *input.FulfillmentAddress)
		return UpdateFulfillmentAddressOutput{false}, external.NewInternalServerError("Error while updating fulfillment address.")
	}

	postLatencyMetric(updateStart, metricName, f.metricContext)
	postDynamoPutSuccessCountMetric(metricName, f.metricContext)
	return UpdateFulfillmentAddressOutput{true}, nil
}

// NewDynamoFulfillmentAddressService returns a fullfilment service interface that can be use to store the fulfillment address for a given SKU / user combination. It stores it by calling a dynamo table.
func NewDynamoFulfillmentAddressService(apiContext external.APIContext, clientMetricName string) FullfillmentAddressServiceInterface {
	metricNames := external.MetricNames{LatencyName: fulfillmentAddressDynamoLatencyMetricName, FailureName: fulfillmentAddressDynamoFailureMetricName}
	metricContext := external.MetricContext{APIContext: apiContext, ClientMetricName: clientMetricName, MetricNames: metricNames}

	db := dynamo.New(session.New(), &aws.Config{Region: aws.String(apiContext.Configuration.GetAwsRegion())})
	fulfillmentAddressTable := db.Table(DynamoDBFulfillmentAddressTableName)

	return &dynamoFulfillmentAddressService{fulfillmentAddressTable: fulfillmentAddressTable, metricContext: metricContext}
}
