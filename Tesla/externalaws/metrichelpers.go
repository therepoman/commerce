package externalaws

import (
	"time"

	"code.justin.tv/commerce/Tesla/external"
	"code.justin.tv/commerce/Tesla/metrics"
)

func postDynamoPutSuccessCountMetric(metricName string, metricContext external.MetricContext) {
	postCountMetric(0.0, metricName, metricContext)
}

func postDynamoPutFailureCountMetric(metricName string, metricContext external.MetricContext) {
	postCountMetric(1.0, metricName, metricContext)
}

func postGetCountMetric(metricName string, metricContext external.MetricContext) {
	metrics.PostCountMetric(
		metricContext.APIContext.Configuration,
		metricContext.APIContext.MethodName,
		metricContext.ClientMetricName,
		1.0,
		metricName)
}

// Helper method to post a count metric
func postCountMetric(value float64, metricName string, metricContext external.MetricContext) {
	metrics.PostCountMetric(
		metricContext.APIContext.Configuration,
		metricContext.APIContext.MethodName,
		metricContext.ClientMetricName,
		value,
		metricContext.MetricNames.FailureName+metricName)
}

// Helper method to post a latency metric
func postLatencyMetric(startTime time.Time, metricName string, metricContext external.MetricContext) {
	metrics.PostLatencyMetricWithMetricName(
		metricContext.APIContext.Configuration,
		metricContext.APIContext.MethodName,
		metricContext.ClientMetricName,
		time.Since(startTime),
		metricContext.MetricNames.LatencyName+metricName)
}
