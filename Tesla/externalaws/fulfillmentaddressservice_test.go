package externalaws

import (
	"testing"

	"github.com/guregu/dynamo"

	"code.justin.tv/commerce/Tesla/config"
	"code.justin.tv/commerce/Tesla/external"
)

func createFulfillmentAddressTable(db *dynamo.DB) error {
	return db.CreateTable(DynamoDBFulfillmentAddressTableName, dynamoFulfillmentAddressEntry{}).Provision(5, 5).Run()
}

func TestUpdateFulfillmentAddressDynamo(t *testing.T) {
	testTUID := "Et tu Id?"
	testReceiptID := "Receipt ID"
	testClientID := "Id'd by the client"
	testFulfillmentAddress := "Some address"
	testAPICallerName := "It's me! A test API"

	input := UpdateFulfillmentAddressInput{testTUID, &testReceiptID, testClientID, &testFulfillmentAddress}
	configuration := config.GetTestConfiguration()
	apiContext := external.APIContext{Configuration: configuration, MethodName: testAPICallerName, ClientID: testClientID}
	metricNames := external.MetricNames{LatencyName: fulfillmentAddressDynamoLatencyMetricName, FailureName: fulfillmentAddressDynamoFailureMetricName}
	metricContext := external.MetricContext{APIContext: apiContext, MetricNames: metricNames}

	dynamoClient := createTestDynamoClient()
	tableCreateErr := createFulfillmentAddressTable(dynamoClient)

	if tableCreateErr != nil {
		t.Error("TestUpdateFulfillmentAddressDynamo::Error creating fulfillment table.")
	}

	fulfillmentAddressTable := dynamoClient.Table(DynamoDBFulfillmentAddressTableName)
	service := dynamoFulfillmentAddressService{fulfillmentAddressTable: fulfillmentAddressTable, metricContext: metricContext}
	output, err := service.UpdateFulfillmentAddress(input)

	if !output.Success {
		t.Error("TestUpdateFulfillmentAddressDynamo::Save was not successful")
	}

	if err != nil {
		t.Error("TestUpdateFulfillmentAddressDynamo::Error updating address")
	}

	// After updating, the item should be in the dynamo table.
	var result dynamoFulfillmentAddressEntry
	expectedClientIDAndReceiptID := testClientID + ":" + testReceiptID
	query := fulfillmentAddressTable.Get("tuid", testTUID).Range("clientId:receiptId", dynamo.Equal, expectedClientIDAndReceiptID)
	dynamoErr := query.One(&result)

	if dynamoErr != nil {
		t.Error("TestUpdateFulfillmentAddressDynamo::Error fetching fulfillment address dynamo item")
	}

	if result.Tuid != testTUID {
		t.Error("TestUpdateFulfillmentAddressDynamo::Unexpected TUID in stored result Expected " + testTUID + " but got : " + result.Tuid)
	}

	if result.ClientIDAndReceiptID != expectedClientIDAndReceiptID {
		t.Error("TestUpdateFulfillmentAddressDynamo::Unexpected clientID/receiptID in stored result Expected " + expectedClientIDAndReceiptID + " but got : " + result.ClientIDAndReceiptID)
	}

	if result.FulfillmentAddress != testFulfillmentAddress {
		t.Error("TestUpdateFulfillmentAddressDynamo::Unexpected fulfillment address in stored result Expected " + testFulfillmentAddress + " but got : " + result.FulfillmentAddress)
	}
}
