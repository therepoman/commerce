package externalaws

import (
	"os"
	"testing"

	"code.justin.tv/common/go_test_dynamo"
	"github.com/guregu/dynamo"
)

func VerifyExistsInMap(t *testing.T, m map[string]string, key string, expectedValue string, errorPrefix string) {
	if value, valuePresent := m[key]; !valuePresent {
		t.Error(errorPrefix + " Key : " + key + " is absent from the map.")
	} else {
		if value != expectedValue {
			t.Error(errorPrefix + " Key : " + key + " returned an expected value. Got : " + value + " Should be : " + expectedValue)
		}
	}
}

func TestMain(m *testing.M) {
	ddb := go_test_dynamo.Instance()
	code := m.Run()
	err := ddb.Shutdown()
	if err != nil {
		os.Exit(1)
	}
	os.Exit(code)
}

func createTestDynamoClient() *dynamo.DB {
	return dynamo.NewFromIface(go_test_dynamo.Instance().Dynamo)
}
