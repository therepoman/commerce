package externalaws

import (
	"code.justin.tv/commerce/Tesla/external"
	log "github.com/Sirupsen/logrus"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/guregu/dynamo"
)

// Constants for dynamodb and metrics
const (
	DynamoDBClientProductMappingTableName = "client-to-ses-product-mapping"

	clientProductMappingDynamoLatencyMetricName   = "DependencyLatency:ClientProductMappingDynamo"
	clientProductMappingDynamoFailureMetricName   = "DependencyCallFailure:ClientProductMappingDynamo"
	clientProductMappingDynamoNoMappingMetricName = "NoMappingFailure:ClientProductMappingDynamo"
)

// UpdateFulfillmentAddressInput is the input structure to be posted to the fulfillment address lambda.
type GetClientProductMappingInput struct {
	ClientID string `json:"clientId"`
}

// UpdateFulfillmentAddressOutput is the output structure returned from the lambda. It solely returns if it was successful
type GetClientProductMappingOutput struct {
	ProductIds []string `json:"productIds"`
}

// FullfillmentAddressServiceInterface is our interface to the fulfillment address lambda.
type ClientProductMappingInterface interface {
	GetClientProductMapping(GetClientProductMappingInput) (GetClientProductMappingOutput, external.Error)
}

type dynamoClientProductMappingService struct {
	clientProductMappingTable dynamo.Table
	metricContext             external.MetricContext
}

type dynamoClientProductMappingEntry struct {
	ClientId   string   `dynamo:"clientId,hash"`
	ProductIds []string `dynamo:"productIds"`
	ClientName string   `dynamo:"clientName"`
}

func (f dynamoClientProductMappingService) GetClientProductMapping(input GetClientProductMappingInput) (GetClientProductMappingOutput, external.Error) {
	var mapping dynamoClientProductMappingEntry;
	err := f.clientProductMappingTable.Get("clientId", input.ClientID).One(&mapping)

	if err != nil {
		log.Error("error with get from table" + err.Error())
		log.Info("failed to get product ids for the client: " + input.ClientID)
		postGetCountMetric(clientProductMappingDynamoNoMappingMetricName, f.metricContext)
		return GetClientProductMappingOutput{ProductIds: nil}, external.NewInternalServerError("Error while getting client id to ses product id mapping.")
	}

	return GetClientProductMappingOutput{
		ProductIds: mapping.ProductIds,
	}, nil
}

// NewDynamoFulfillmentAddressService returns a fullfilment service interface that can be use to store the fulfillment address for a given SKU / user combination. It stores it by calling a dynamo table.
func NewDynamoClientProductMappingService(apiContext external.APIContext, clientMetricName string) ClientProductMappingInterface {
	metricNames := external.MetricNames{LatencyName: clientProductMappingDynamoLatencyMetricName, FailureName: clientProductMappingDynamoFailureMetricName}
	metricContext := external.MetricContext{APIContext: apiContext, ClientMetricName: clientMetricName, MetricNames: metricNames}

	db := dynamo.New(session.New(), &aws.Config{Region: aws.String(apiContext.Configuration.GetAwsRegion())})
	fulfillmentAddressTable := db.Table(DynamoDBClientProductMappingTableName)

	return &dynamoClientProductMappingService{clientProductMappingTable: fulfillmentAddressTable, metricContext: metricContext}
}
