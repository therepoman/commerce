package externalaws

import (
	"testing"

	"code.justin.tv/commerce/Tesla/config"
	"code.justin.tv/commerce/Tesla/external"
	log "github.com/Sirupsen/logrus"
	"github.com/guregu/dynamo"
)

func createRevokeTable(db *dynamo.DB) error {
	return db.CreateTable(DynamoDBRevokeTableName, DynamoRevokedGood{}).
		Provision(5, 5).
		Run()
}

func initializeRevokeTests() (dynamo.Table, dynamoRevokeService) {
	testClientID := "nnelodyrocks"
	testAPICallerName := "op"

	configuration := config.GetTestConfiguration()
	apiContext := external.APIContext{Configuration: configuration, MethodName: testAPICallerName, ClientID: testClientID}
	metricNames := external.MetricNames{LatencyName: revokeDynamoLatencyMetricName, FailureName: revokeDynamoFailureMetricName}
	metricContext := external.MetricContext{APIContext: apiContext, MetricNames: metricNames}

	dynamoClient := createTestDynamoClient()
	tableCreateErr := createRevokeTable(dynamoClient)

	if tableCreateErr != nil {
		log.Error("Issue initializing test")
	}

	revokeTable := dynamoClient.Table(DynamoDBRevokeTableName)
	service := dynamoRevokeService{revokeTable: revokeTable, metricContext: metricContext}

	return revokeTable, service
}

func TestCreateRevokeDynamo(t *testing.T) {
	testUserID := "nnelody"
	testReceiptID := "a receipt"
	testSku := "A sku"

	revokeTable, service := initializeRevokeTests()

	success, err := service.CreateRevoke(testUserID, testReceiptID, testSku)

	if !success {
		t.Error("TestCreateRevokeDynamo::Save was not successful")
	}

	if err != nil {
		t.Error("TestCreateRevokeDynamo::Error creating new revoke")
	}

	var result DynamoRevokedGood
	query := revokeTable.Get(DynamoDBUserColumn, testUserID).
		Range(DynamoDBReceiptIDColumn, dynamo.Equal, testReceiptID)
	dynamoErr := query.One(&result)

	if dynamoErr != nil {
		t.Error("TestCreateRevokeDynamo::Error fetching revoke dynamo item")
	}

	if result.UserID != testUserID {
		t.Error("TestCreateRevokeDynamo::Unexpected UserID in stored result Expected " + testUserID + " but got: " + result.UserID)
	}

	if result.ReceiptID != testReceiptID {
		t.Error("TestCreateRevokeDynamo::Unexpected ReceiptID in stored result Expected " + testReceiptID + " but got: " + result.ReceiptID)
	}

	if result.Sku != testSku {
		t.Error("TestCreateRevokeDynamo::Unexpected Sku in stored result Expected " + testSku + " but got: " + result.Sku)
	}

	if result.Completed {
		t.Error("TestCreateRevokeDynamo::Newly created receipt should not be have completed status of true")
	}

}

func TestCompleteRevokeByReceipt(t *testing.T) {
	testUserID := "nnelody"
	testReceiptID := "a receipt"
	testSku := "A sku"

	revokeTable, service := initializeRevokeTests()

	success, err := service.CreateRevoke(testUserID, testReceiptID, testSku)

	if !success {
		t.Error("TestCompleteRevokeByReceipt::Save was not successful")
	}

	if err != nil {
		t.Error("TestCompleteRevokeByReceipt::Error creating new revoke")
	}

	var result DynamoRevokedGood
	query := revokeTable.Get(DynamoDBUserColumn, testUserID).
		Range(DynamoDBReceiptIDColumn, dynamo.Equal, testReceiptID)
	dynamoErr := query.One(&result)

	if dynamoErr != nil {
		t.Error("TestCompleteRevokeByReceipt::Error fetching revoke dynamo item")
	}

	if result.ReceiptID != testReceiptID {
		t.Error("TestCompleteRevokeByReceipt::Unexpected ReceiptID in stored result Expected " + testReceiptID + " but got: " + result.ReceiptID)
	}

	success, err = service.CompleteRevokeByReceipt(testUserID, testReceiptID)

	if !success {
		t.Error("TestCompleteRevokeByReceipt::Save was not successful")
	}

	if err != nil {
		t.Error("TestCompleteRevokeByReceipt::Error completing revoke")
	}

	var completedResult DynamoRevokedGood
	query = revokeTable.Get(DynamoDBUserColumn, testUserID).
		Range(DynamoDBReceiptIDColumn, dynamo.Equal, testReceiptID)
	dynamoErr = query.One(&completedResult)

	if dynamoErr != nil {
		t.Error("TestCreateRevokeDynamo::Error fetching revoke dynamo item")
	}

	if !completedResult.Completed {
		t.Error("TestCompleteRevokeByReceipt::Expected a completed revoke")
	}
}

func TestGetRevokesByUser(t *testing.T) {
	testUserID := "nnelody"
	testReceiptID := "a receipt"
	testSku := "A sku"

	testReceiptID2 := "a receipt 2"
	testSku2 := "A sku 2"

	_, service := initializeRevokeTests()

	success, err := service.CreateRevoke(testUserID, testReceiptID, testSku)

	if !success {
		t.Error("TestGetRevokesByUser::Save was not successful")
	}

	if err != nil {
		t.Error("TestGetRevokesByUser::Error creating new revoke")
	}

	success, err = service.CreateRevoke(testUserID, testReceiptID2, testSku2)

	if !success {
		t.Error("TestGetRevokesByUser::Save was not successful")
	}

	if err != nil {
		t.Error("TestGetRevokesByUser::Error creating new revoke")
	}

	revokes, err := service.GetRevokesByUser(testUserID)

	if err != nil {
		t.Error("TestGetRevokesByUser::Error getting revoke by user")
	}

	if len(revokes) != 2 {
		t.Error("TestGetRevokesByUser::Expected to get array of 2 revokes")
	}
}

func TestGetRevokesByUserAndSku(t *testing.T) {
	testUserID := "nnelody"
	testReceiptID := "a receipt"
	testSku := "The same sku"

	testReceiptID2 := "a receipt 2"

	testReceiptID3 := "a receipt 3"
	testDifferentSku := "Different sku"

	_, service := initializeRevokeTests()

	success, err := service.CreateRevoke(testUserID, testReceiptID, testSku)

	if !success {
		t.Error("TestGetRevokesByUserAndSku::Save was not successful")
	}

	if err != nil {
		t.Error("TestGetRevokesByUserAndSku::Error creating new revoke")
	}

	success, err = service.CreateRevoke(testUserID, testReceiptID2, testSku)

	if !success {
		t.Error("TestGetRevokesByUserAndSku::Save was not successful")
	}

	if err != nil {
		t.Error("TestGetRevokesByUserAndSku::Error creating new revoke")
	}

	success, err = service.CreateRevoke(testUserID, testReceiptID3, testDifferentSku)

	if !success {
		t.Error("TestGetRevokesByUserAndSku::Save was not successful")
	}

	if err != nil {
		t.Error("TestGetRevokesByUserAndSku::Error creating new revoke")
	}

	revokes, err := service.GetRevokesByUserAndSku(testUserID, testSku)

	if err != nil {
		t.Error("TestGetRevokesByUserAndSku::Error getting revoke by user")
	}

	if len(revokes) != 2 {
		t.Error("TestGetRevokesByUserAndSku::Expected to get array of 2 revokes")
	}
}
