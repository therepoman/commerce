package externalaws

import (
	"reflect"
	"testing"

	log "github.com/Sirupsen/logrus"
	"github.com/guregu/dynamo"

	"code.justin.tv/commerce/Tesla/config"
	"code.justin.tv/commerce/Tesla/external"
)

func createClientProductMappingTable(db *dynamo.DB) error {
	return db.CreateTable(DynamoDBClientProductMappingTableName, dynamoClientProductMappingEntry{}).Provision(5, 5).Run()
}

func TestGetClientProductMappingEntryDynamo(t *testing.T) {
	testProductIds := []string{"123", "125"}
	testClientID := "Id'd by the client"
	testAPICallerName := "It's me! A test API"
	testDynamoEntry := dynamoClientProductMappingEntry{
		ClientId:   testClientID,
		ProductIds: testProductIds,
	}

	input := GetClientProductMappingInput{ClientID: testClientID}
	configuration := config.GetTestConfiguration()
	apiContext := external.APIContext{Configuration: configuration, MethodName: testAPICallerName, ClientID: testClientID}
	metricNames := external.MetricNames{LatencyName: clientProductMappingDynamoLatencyMetricName, FailureName: clientProductMappingDynamoFailureMetricName}
	metricContext := external.MetricContext{APIContext: apiContext, MetricNames: metricNames}

	dynamoClient := createTestDynamoClient()
	tableCreateErr := createClientProductMappingTable(dynamoClient)

	if tableCreateErr != nil {
		t.Error("TestGetClientProductMappingEntryDynamo::Error creating fulfillment table.")
	}

	clientProductMappingTable := dynamoClient.Table(DynamoDBClientProductMappingTableName)

	// put a test dynamo entry
	putErr := clientProductMappingTable.Put(testDynamoEntry).Run()

	if putErr != nil {
		t.Error("TestGetClientProductMappingEntryDynamo::Error putting test entry.")
	}

	// good get
	service := dynamoClientProductMappingService{clientProductMappingTable: clientProductMappingTable, metricContext: metricContext}
	output, err := service.GetClientProductMapping(input)

	log.Info(output.ProductIds)

	// todo check equivalence of arrays
	if (!reflect.DeepEqual(output.ProductIds, testProductIds)) {
		t.Error("TestGetClientProductMappingEntryDynamo::Get Arrays are not equal")
	}

	if (err != nil) ||
		(output.ProductIds == nil) ||
		(len(output.ProductIds) != len(testProductIds)) {
		t.Error("TestGetClientProductMappingEntryDynamo::Get was not successful")
	}

	// bad get
	badInput := GetClientProductMappingInput{ClientID: "badClient"}
	badOutput, badOutputErr := service.GetClientProductMapping(badInput)

	if (badOutputErr == nil) ||
		(len(badOutput.ProductIds) != 0) {
		t.Error("TestGetClientProductMappingEntryDynamo::Get bad client id got response")
	}
}
