package dominos

import (
	consts "code.justin.tv/commerce/pizza/constants/dominos"
	models "code.justin.tv/commerce/pizza/models/dominos"
	"code.justin.tv/commerce/pizza/utils"
)

func GetPrice(req models.CustomerRequest) (resp models.DominoOrderResponse, serviceAvailable bool, err error) {
	validationResp, serviceAvailable, err := Validate(req)
	if err != nil {
		return
	}

	request := models.DominoOrderRequest{
		Order: validationResp.Order,
	}

	request.Order.LanguageCode = consts.LANGUAGE_CODE_ENGLISH //TODO: could be configurable
	request.Order.OrderMethod = consts.ORDER_METHOD_WEB
	request.Order.Version = "1.0"
	request.Order.OrderChannel = "OLO"
	request.Order.SourceURI = "order.dominos.com"
	request.Order.NoCombine = true
	request.Order.NewUser = true

	err = utils.HttpPost(consts.PRICE_URL, request, &resp)
	return
}
