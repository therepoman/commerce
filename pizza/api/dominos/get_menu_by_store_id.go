package dominos

import (
	"errors"
	"fmt"

	consts "code.justin.tv/commerce/pizza/constants/dominos"
	models "code.justin.tv/commerce/pizza/models/dominos"
	"code.justin.tv/commerce/pizza/utils"
)

func GetMenuByStoreID(req models.GetMenuByStoreIDRequest) (resp models.MenuResponse, err error) {
	if req.StoreID == "" {
		err = errors.New("store ID cannot be empty")
		return
	}

	resp = models.MenuResponse{}
	err = utils.HttpGet(fmt.Sprintf(consts.MENU_URL, req.StoreID), &resp)
	return
}
