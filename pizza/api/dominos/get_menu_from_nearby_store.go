package dominos

import (
	models "code.justin.tv/commerce/pizza/models/dominos"
)

func GetMenuFromNearbyStore(req models.NearbyStoresRequest) (resp models.MenuResponse, err error) {
	stores, err := FindNearbyStores(req)
	if err != nil {
		return
	}
	if len(stores.Stores) == 0 {
		return
	}

	return GetMenuByStoreID(models.GetMenuByStoreIDRequest{
		StoreID: stores.Stores[0].StoreID,
	})
}
