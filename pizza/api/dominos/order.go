package dominos

import (
	"errors"
	"fmt"

	consts "code.justin.tv/commerce/pizza/constants/dominos"
	models "code.justin.tv/commerce/pizza/models/dominos"
	"code.justin.tv/commerce/pizza/utils"
)

func Order(req models.OrderRequest) (resp models.DominoOrderResponse, err error) {
	resp, serviceAvailable, err := GetPrice(req.CustomerRequest)
	if err != nil {
		return
	}
	if resp.Status == -1 {
		err = errors.New("invalid request")
		return
	}
	if !serviceAvailable {
		err = errors.New(fmt.Sprintf("The service method not available: %s", resp.Order.ServiceMethod))
		return
	}
	if !consts.IsPaymentTypeValid(req.PaymentType) {
		err = errors.New(fmt.Sprintf("invalid payment type: %s", req.PaymentType))
		return
	}
	cardType := consts.GetCardType(req.CardNumber)
	if cardType == "" {
		err = errors.New(fmt.Sprintf("invalid card type"))
		return
	}

	// Get full postal code
	validateResp, err := FindNearbyStores(models.NearbyStoresRequest{
		Street:        req.BillingStreetAddress,
		PostalCode:    req.PaymentPostalCode,
		ServiceMethod: req.CustomerRequest.ServiceMethod,
	})
	if err != nil {
		return
	}

	payment := models.Payment{}
	payment.Amount = resp.Order.Amounts.Customer
	payment.Type = req.PaymentType
	payment.Number = req.CardNumber
	payment.CardType = cardType
	payment.Expiration = req.Expiration
	payment.SecurityCode = req.SecurityCode
	payment.PostalCode = validateResp.Address.PostalCode

	resp.Order.Payments = append(resp.Order.Payments, payment)
	request := models.DominoOrderRequest{
		Order: resp.Order,
	}

	err = utils.HttpPost(consts.ORDER_URL, request, &resp)
	return
}
