package dominos

import (
	"errors"
	"fmt"

	consts "code.justin.tv/commerce/pizza/constants/dominos"
	models "code.justin.tv/commerce/pizza/models/dominos"
	"code.justin.tv/commerce/pizza/utils"
)

func Validate(req models.CustomerRequest) (resp models.DominoOrderResponse, serviceAvailable bool, err error) {
	if req.ServiceMethod == "" {
		req.ServiceMethod = consts.SERVICE_METHOD_DELIVERY
	}
	if !consts.IsServiceMethodValid(req.ServiceMethod) {
		err = errors.New(fmt.Sprintf("service method invalid: %s", req.ServiceMethod))
		return
	}
	if !consts.IsAddressTypeValid(req.AddressType) {
		err = errors.New(fmt.Sprintf("address type invalid: %s", req.AddressType))
		return
	}

	nearbyStores, err := FindNearbyStores(models.NearbyStoresRequest{
		Street:        req.Street,
		PostalCode:    req.PostalCode,
		ServiceMethod: req.ServiceMethod,
	})
	if err != nil {
		return
	}
	if len(nearbyStores.Stores) == 0 {
		err = errors.New("no store is available right now")
		return
	}

	// Domino's only accepts the first store returned for the store ID
	closestStore := nearbyStores.Stores[0]
	variantProductMap, serviceAvailable, err := getProducts(req.ServiceMethod, req.VariantCodes, closestStore)
	if err != nil {
		return
	}
	if len(variantProductMap) == 0 {
		err = errors.New("failed to obtain variantProductMap")
		return
	}

	request := models.DominoOrderRequest{
		Order: models.PricedOrder{
			FirstName:     req.FirstName,
			LastName:      req.LastName,
			Email:         req.Email,
			Phone:         req.Phone,
			StoreID:       closestStore.StoreID,
			ServiceMethod: req.ServiceMethod,
			Address:       nearbyStores.Address,
		},
	}

	for _, variantCode := range req.VariantCodes {
		product, ok := variantProductMap[variantCode]
		if !ok {
			err = errors.New("something went wrong")
			return
		}

		// Default toppings for pizza are Cheese and Robust Inspired Tomato Sauce
		var options map[string]map[string]string
		if product.ProductType == consts.PRODUCT_TYPE_PIZZA {
			options = consts.DefaultOptions
		}

		request.Order.Products = append(request.Order.Products, models.PricedProduct{
			Code:       variantCode,
			Quantity:   1,
			ID:         1,
			Options:    options,
			IsNewReq:   true,
			AutoRemove: false,
		})
	}

	err = utils.HttpPost(consts.VALIDATION_URL, request, &resp)
	return
}

func getProducts(serviceMethod string, variantCodes []string, store models.Store) (productsMap map[string]models.Product, serviceAvailable bool, err error) {
	serviceOpen, ok := store.ServiceIsOpen[serviceMethod]
	if !ok {
		err = errors.New(fmt.Sprintf("invalid service method: %s", serviceMethod))
		return
	}

	serviceAvailable = store.IsOpen && store.IsOnlineNow && serviceOpen

	menuResp, apiErr := GetMenuByStoreID(models.GetMenuByStoreIDRequest{
		StoreID: store.StoreID,
	})
	if apiErr != nil {
		err = apiErr
		return
	}

	variantToProducts := make(map[string]models.Product, 0)
	var products []models.Product
	for _, product := range menuResp.Products {
		products = append(products, product)
	}

	if hasAllVariantsWanted(menuResp.Variants, variantCodes) {
		for _, variantCode := range variantCodes {
			targetProduct := models.GetProductFromVariant(products, variantCode)
			variantToProducts[variantCode] = targetProduct
		}
		productsMap = variantToProducts
		return
	}

	return
}

func hasAllVariantsWanted(availableVariants map[string]models.Variant, wantedVariants []string) bool {
	for _, variantCode := range wantedVariants {
		_, ok := availableVariants[variantCode]
		if !ok {
			return false
		}
	}
	return true
}
