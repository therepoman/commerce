package dominos

import (
	"errors"
	"fmt"

	consts "code.justin.tv/commerce/pizza/constants/dominos"
	models "code.justin.tv/commerce/pizza/models/dominos"
	"code.justin.tv/commerce/pizza/utils"
)

func FindNearbyStores(req models.NearbyStoresRequest) (resp models.FindNearbyStoresResponse, err error) {
	if req.PostalCode == "" {
		err = errors.New("postal code cannot be empty")
		return
	}

	resp = models.FindNearbyStoresResponse{}
	err = utils.HttpGet(fmt.Sprintf(consts.FIND_URL, req.Street, req.PostalCode, req.ServiceMethod), &resp)
	return
}
