# :pizza: :pizza: Pizza :pizza: :pizza:

![Pizza](/assets/pagliacci-meat-lovers.png)

Orders pizzas.

## :pizza: Introduction :pizza:
This is a Golang library of generic pizza ordering APIs such as Domino's.

Supported restaurants:
  - Dominos

Supported APIs:
  - Dominos
    - FindNearbyStores
    - GetMenuByStoreID
    - GetMenuFromNearbyStore
    - GetPrice
    - Order
  
## :pizza: Usage :pizza:
After importing this library to your project, calling an API should be as simple as:
```go
import "dominos"
    
    // some codes
    
    req := dominos.FindNearbyStoresReqest{
        Street:     "",
        PostalCode: "98109",
        OrderType:  "Delivery",
    }

    resp, err := dominos.FindNearbyStores(req)
    
```
For more examples, refer to the `./scripts` folder.
