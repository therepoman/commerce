package dominos

import "regexp"

// URLs
const (
	FIND_URL       = "https://order.dominos.com/power/store-locator?s=%s&c=%s&type=%s"
	MENU_URL       = "https://order.dominos.com/power/store/%s/menu?lang=en&structured=true"
	VALIDATION_URL = "https://order.dominos.com/power/validate-order"
	PRICE_URL      = "https://order.dominos.com/power/price-order"
	ORDER_URL      = "https://order.dominos.com/power/place-order"
)

// Order types. Delivery or carryout?
const (
	SERVICE_METHOD_DELIVERY = "Delivery"
	SERVICE_METHOD_CARRYOUT = "Carryout"
)

func IsServiceMethodValid(method string) bool {
	return method == SERVICE_METHOD_DELIVERY ||
		method == SERVICE_METHOD_CARRYOUT
}

// Product Types
const (
	PRODUCT_TYPE_PIZZA    = "Pizza"
	PRODUCT_TYPE_DESSERT  = "Dessert"
	PRODUCT_TYPE_BREAD    = "Bread"
	PRODUCT_TYPE_GSALAD   = "GSalad"
	PRODUCT_TYPE_PASTA    = "Pasta"
	PRODUCT_TYPE_WINGS    = "Wings"
	PRODUCT_TYPE_DRINKS   = "Drinks"
	PRODUCT_TYPE_SANDWICH = "Sandwich"
	PRODUCT_TYPE_SIDES    = "Sides"
)

// Pizza's default toppings
var DefaultOptions = map[string]map[string]string{
	// Cheese
	"C": {
		"1/1": "1",
	},
	// Robust Inspired Tomato Sauce
	"X": {
		"1/1": "1",
	},
}

// Address Types
const (
	ADDRESS_TYPE_HOUSE     = "House"
	ADDRESS_TYPE_APARTMENT = "Apartment"
	ADDRESS_TYPE_BUSINESS  = "Business"
	ADDRESS_TYPE_CAMPUS    = "Campus/Base"
	ADDRESS_TYPE_HOTEL     = "Hotel"
	ADDRESS_TYPE_OTHER     = "Other"
)

func IsAddressTypeValid(adrsType string) bool {
	return adrsType == ADDRESS_TYPE_HOUSE ||
		adrsType == ADDRESS_TYPE_APARTMENT ||
		adrsType == ADDRESS_TYPE_BUSINESS ||
		adrsType == ADDRESS_TYPE_CAMPUS ||
		adrsType == ADDRESS_TYPE_HOTEL ||
		adrsType == ADDRESS_TYPE_OTHER
}

// Currencies
const (
	CURRENCY_USD = "USD"
)

// Card Types
const (
	CARD_TYPE_VISA       = "VISA"
	CARD_TYPE_MASTERCARD = "MASTERCARD"
	CARD_TYPE_AMEX       = "AMEX"
	CARD_TYPE_DINERS     = "DINERS"
	CARD_TYPE_DISCOVER   = "DISCOVER"
	CARD_TYPE_JCB        = "JCB"
)

func isVisa(number string) bool {
	re := regexp.MustCompile(`^4[0-9]{12}(?:[0-9]{3})?$`)
	return re.MatchString(number)
}

func isMasterCard(number string) bool {
	re := regexp.MustCompile(`^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$`)
	return re.MatchString(number)
}

func isAmex(number string) bool {
	re := regexp.MustCompile(`^3[47][0-9]{13}$`)
	return re.MatchString(number)
}

func isDiners(number string) bool {
	re := regexp.MustCompile(`^3(?:0[0-5]|[68][0-9])[0-9]{11}$`)
	return re.MatchString(number)
}

func isDiscover(number string) bool {
	re := regexp.MustCompile(`^65[4-9][0-9]{13}|64[4-9][0-9]{13}|6011[0-9]{12}|(622(?:12[6-9]|1[3-9][0-9]|[2-8][0-9][0-9]|9[01][0-9]|92[0-5])[0-9]{10})$`)
	return re.MatchString(number)
}

func isJCB(number string) bool {
	re := regexp.MustCompile(`^(?:2131|1800|35\d{3})\d{11}$`)
	return re.MatchString(number)
}

func GetCardType(number string) string {
	if isVisa(number) {
		return CARD_TYPE_VISA
	}
	if isMasterCard(number) {
		return CARD_TYPE_MASTERCARD
	}
	if isAmex(number) {
		return CARD_TYPE_AMEX
	}
	if isDiners(number) {
		return CARD_TYPE_DINERS
	}
	if isDiscover(number) {
		return CARD_TYPE_DISCOVER
	}
	if isJCB(number) {
		return CARD_TYPE_JCB
	}
	return ""
}

// Payment Types
const (
	PAYMENT_METHOD_CREDITCARD = "CreditCard"
)

func IsPaymentTypeValid(pt string) bool {
	return pt == PAYMENT_METHOD_CREDITCARD
}

// Order Methods
const (
	ORDER_METHOD_WEB = "Web"
)

// Language Codes
const (
	LANGUAGE_CODE_ENGLISH = "en"
)

// Specialty Pizzas quick reference of `Menu name - ID of medium size (12")`
var Specialties_ProductName = map[string]string{
	"ExtravaganZZa":         "12SCEXTRAV",
	"MeatZZa":               "12SCMEATZA",
	"PhillyCheeseSteak":     "P12IREPH",
	"PacificVeggie":         "P12IREPV",
	"HonoluluHawaiian":      "P12IREUH",
	"Deluxe":                "12SCDELUX",
	"CaliChickenBaconRanch": "P12IRECR",
	"BuffaloChicken":        "P12IREBP",
	"UltimatePepperoni":     "12SCPFEAST",
	"MemphisBBQChicken":     "P12IRECK",
	"Wisconsin6Cheese":      "P12IRECZ",
	"Spinach&Feta":          "P12IRESPF",
}

// Specialty Pizzas quick reference of `Product ID - ID of medium size (12")`
var Specialties_ProductID = map[string]string{
	"S_ZZ":    "12SCEXTRAV",
	"S_MX":    "12SCMEATZA",
	"S_PIZPH": "P12IREPH",
	"S_PIZPV": "P12IREPV",
	"S_PIZUH": "P12IREUH",
	"S_DX":    "12SCDELUX",
	"S_PIZCR": "P12IRECR",
	"S_PIZBP": "P12IREBP",
	"S_PIZPX": "12SCPFEAST",
	"S_PIZCK": "P12IRECK",
	"S_PIZCZ": "P12IRECZ",
	"S_PISPF": "P12IRESPF",
}
