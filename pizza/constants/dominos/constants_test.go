package dominos

import (
	"fmt"
	"testing"
)

func TestConstants(t *testing.T) {
	visa := "4111111111111111"
	mastercard := "5500000000000004"
	amex := "340000000000009"
	diners := "30000000000004"
	discover := "6011111111111117"
	jcb := "3530111333300000"

	if GetCardType(visa) != CARD_TYPE_VISA {
		t.Errorf(fmt.Sprintf("Visa regex not working for %s", CARD_TYPE_VISA))
	}

	if GetCardType(mastercard) != CARD_TYPE_MASTERCARD {
		t.Errorf(fmt.Sprintf("Visa regex not working for %s", CARD_TYPE_MASTERCARD))
	}

	if GetCardType(amex) != CARD_TYPE_AMEX {
		t.Errorf(fmt.Sprintf("Visa regex not working for %s", CARD_TYPE_AMEX))
	}

	if GetCardType(diners) != CARD_TYPE_DINERS {
		t.Errorf(fmt.Sprintf("Visa regex not working for %s", CARD_TYPE_DINERS))
	}

	if GetCardType(discover) != CARD_TYPE_DISCOVER {
		t.Errorf(fmt.Sprintf("Visa regex not working for %s", CARD_TYPE_DISCOVER))
	}

	if GetCardType(jcb) != CARD_TYPE_JCB {
		t.Errorf(fmt.Sprintf("Visa regex not working for %s", CARD_TYPE_JCB))
	}

	if GetCardType("whatever") != "" {
		t.Errorf("'whatever' should not be a valid card type but is deemed valid")
	}
}
