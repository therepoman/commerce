package utils

import (
	"bytes"
	"encoding/json"
	"net/http"
	"strings"
)

func HttpGet(url string, target interface{}) error {
	url = strings.Replace(url, " ", "%20", -1)

	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return json.NewDecoder(resp.Body).Decode(target)
}

func HttpPost(url string, data, target interface{}) error {
	jsonData, err := json.Marshal(data)
	if err != nil {
		return err
	}

	resp, err := http.Post(url, "application/json", bytes.NewBuffer(jsonData))
	defer resp.Body.Close()

	return json.NewDecoder(resp.Body).Decode(target)
}
