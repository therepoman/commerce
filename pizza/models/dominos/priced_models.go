package dominos

type PricedStatusItem struct {
	Code string `json:"Code"`
}

type PricedAmounts struct {
	Menu       float32 `json:"Menu"`
	Discount   float32 `json:"Discount"`
	Surcharge  float32 `json:"Surcharge"`
	Adjustment float32 `json:"Adjustment"`
	Net        float32 `json:"Net"`
	Tax        float32 `json:"Tax"`
	Tax1       float32 `json:"Tax1"`
	Tax2       float32 `json:"Tax2"`
	Bottle     float32 `json:"Bottle"`
	Customer   float32 `json:"Customer"`
	Payment    float32 `json:"Payment"`
}

type PricedAmountsBreakdown struct {
	FoodAndBeverage string  `json:"FoodAndBeverage"`
	Adjustment      string  `json:"Adjustment"` // "0.00"
	Surcharge       string  `json:"Surcharge"`
	DeliveryFee     string  `json:"DeliveryFee"` // "4.99"
	Tax             float32 `json:"Tax"`
	Tax1            float32 `json:"Tax1"`
	Tax2            float32 `json:"Tax2"`
	Bottle          float32 `json:"Bottle"`
	Customer        float32 `json:"Customer"`
	Savings         string  `json:"Savings"` // "0.00"
}

type ProductDescription struct {
	PortionCode string `json:"portionCode"`
	Value       string `json:"value"`
}

type PricedProduct struct {
	ID                 int                          `json:"ID"`
	Code               string                       `json:"Code"`
	Quantity           int                          `json:"Qty"`
	CategoryCode       string                       `json:"CategoryCode"`
	FlavorCode         string                       `json:"FlavorCode"`
	Price              float32                      `json:"Price"`
	Status             int                          `json:"Status"`
	LikeProductID      int                          `json:"LikeProductID"`
	Name               string                       `json:"Name"`
	IsNewReq           bool                         `json:"isNew"`
	IsNewResp          bool                         `json:"IsNew"`
	NeedsCustomization bool                         `json:"NeedsCustomization"`
	AutoRemove         bool                         `json:"AutoRemove"`
	Fulfilled          bool                         `json:"Fulfilled"`
	Options            map[string]map[string]string `json:"Options"`
	Descriptions       []ProductDescription         `json:"descriptions"`
}

type Payment struct {
	Type         string  `json:"Type"`
	Amount       float32 `json:"Amount"`
	Number       string  `json:"Number"`
	CardType     string  `json:"CardType"`
	Expiration   string  `json:"Expiration"`
	SecurityCode string  `json:"SecurityCode"`
	PostalCode   string  `json:"PostalCode"`
}

type CorrectiveAction struct {
	Action string `json:"Action"`
	Code   string `json:"Code"`
	Detail string `json:"Detail"`
}

// This model is used for response of Domino's price API, and for both request AND response on Domino's order APIs
type PricedOrder struct {
	FirstName     string  `json:"FirstName"`
	LastName      string  `json:"LastName"`
	Email         string  `json:"Email"`
	Phone         string  `json:"Phone"`
	StoreID       string  `json:"StoreID"`
	ServiceMethod string  `json:"ServiceMethod"`
	Address       Address `json:"Address"`
	NewUser       bool    `json:"NewUser"`
	LanguageCode  string  `json:"LanguageCode"`
	OrderMethod   string  `json:"OrderMethod"`
	Version       string  `json:"Version"`
	IP            string  `json:"IP"`
	Market        string  `json:"Market"`
	Currency      string  `json:"Currency"`
	NoCombine     bool    `json:"NoCombine"`
	OrderChannel  string  `json:"OrderChannel"`
	SourceURI     string  `json:"SourceOrganizationURI"`

	// Promotions -- add this when necessary
	OrderID              string                 `json:"OrderID"`
	Status               int                    `json:"Status"`
	StatusItems          []PricedStatusItem     `json:"StatusItems"`
	AvailablePromos      map[string]string      `json:"AvailablePromos"`
	Amounts              PricedAmounts          `json:"Amounts"`
	BusinessDate         string                 `json:"BusinessDate"`         // "2019-02-07"
	EstimatedWaitMinutes string                 `json:"EstimatedWaitMinutes"` // "31-43"
	PriceOrderTime       string                 `json:"PriceOrderTime"`       // "2019-02-07 16:24:10"
	PriceOrderMs         int                    `json:"PriceOrderMs"`         // 1097
	AmountsBreakdown     PricedAmountsBreakdown `json:"AmountsBreakdown"`
	Products             []PricedProduct        `json:"Products"`
	// Below fields would be used only for placing an order
	Payments         []Payment        `json:"Payments"`
	CorrectiveAction CorrectiveAction `json:"CorrectiveAction"`
	StoreOrderID     string           `json:"StoreOrderID"`
	PulseOrderGuid   string           `json:"PulseOrderGuid"`
}
