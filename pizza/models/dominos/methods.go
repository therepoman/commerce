package dominos

import "fmt"

// models
func (p Product) Equal(another Product) bool {
	return p.Code == another.Code
}

func (p Product) HasVariant(variantCode string) bool {
	for _, variant := range p.Variants {
		if variantCode == variant {
			return true
		}
	}
	return false
}

func (s Store) Equal(another Store) bool {
	return s.StoreID == another.StoreID
}

func GetProductFromVariant(products []Product, targetVariantCode string) (targetProduct Product) {
	for _, product := range products {
		for _, variant := range product.Variants {
			if variant == targetVariantCode {
				targetProduct = product
				return
			}
		}
	}
	return
}

// api models
func (p PricedOrder) PrettyPrint() {
	fmt.Print(fmt.Sprintf("\tFirstName: %s\n", p.FirstName))
	fmt.Print(fmt.Sprintf("\tLastName: %s\n", p.LastName))
	fmt.Print(fmt.Sprintf("\tEmail: %s\n", p.Email))
	fmt.Print(fmt.Sprintf("\tPhone: %s\n", p.Phone))
	fmt.Print(fmt.Sprintf("\tStoreID: %s\n", p.StoreID))
	fmt.Print(fmt.Sprintf("\tServiceMethod: %s\n", p.ServiceMethod))
	fmt.Print(fmt.Sprintf("\tAddress:\n"))
	fmt.Print(fmt.Sprintf("\t\tStreet: %s\n", p.Address.Street))
	fmt.Print(fmt.Sprintf("\t\tStreetNumber: %s\n", p.Address.StreetNumber))
	fmt.Print(fmt.Sprintf("\t\tStreetName: %s\n", p.Address.StreetName))
	fmt.Print(fmt.Sprintf("\t\tUnitType: %s\n", p.Address.UnitType))
	fmt.Print(fmt.Sprintf("\t\tUnitNumber: %s\n", p.Address.UnitNumber))
	fmt.Print(fmt.Sprintf("\t\tCity: %s\n", p.Address.City))
	fmt.Print(fmt.Sprintf("\t\tRegion: %s\n", p.Address.Region))
	fmt.Print(fmt.Sprintf("\t\tPostalCode: %s\n", p.Address.PostalCode))
	fmt.Print(fmt.Sprintf("\tIP: %s\n", p.IP))
	fmt.Print(fmt.Sprintf("\tMarket: %s\n", p.Market))
	fmt.Print(fmt.Sprintf("\tCurrency: %s\n", p.Currency))
	fmt.Print(fmt.Sprintf("\tOrderID: %s\n", p.OrderID))
	fmt.Print(fmt.Sprintf("\tStatus: %d\n", p.Status))
	fmt.Print(fmt.Sprintf("\tAvailablePromos:\n"))
	for key, value := range p.AvailablePromos {
		fmt.Print(fmt.Sprintf("\t\t%s: %s\n", key, value))
	}
	fmt.Print(fmt.Sprintf("\tAmounts:\n"))
	fmt.Print(fmt.Sprintf("\t\tMenu: %.02f\n", p.Amounts.Menu))
	fmt.Print(fmt.Sprintf("\t\tDiscount: %.02f\n", p.Amounts.Discount))
	fmt.Print(fmt.Sprintf("\t\tSurcharge: %.02f\n", p.Amounts.Surcharge))
	fmt.Print(fmt.Sprintf("\t\tAdjustment: %.02f\n", p.Amounts.Adjustment))
	fmt.Print(fmt.Sprintf("\t\tNet: %.02f\n", p.Amounts.Net))
	fmt.Print(fmt.Sprintf("\t\tTax: %.02f\n", p.Amounts.Tax))
	fmt.Print(fmt.Sprintf("\t\tTax1: %.02f\n", p.Amounts.Tax1))
	fmt.Print(fmt.Sprintf("\t\tTax2: %.02f\n", p.Amounts.Tax2))
	fmt.Print(fmt.Sprintf("\t\tBottle: %.02f\n", p.Amounts.Bottle))
	fmt.Print(fmt.Sprintf("\t\tCustomer: %.02f\n", p.Amounts.Customer))
	fmt.Print(fmt.Sprintf("\t\tPayment: %.02f\n", p.Amounts.Payment))
	fmt.Print(fmt.Sprintf("\tBusinessDate: %s\n", p.BusinessDate))
	fmt.Print(fmt.Sprintf("\tEstimatedWaitMinutes: %s\n", p.EstimatedWaitMinutes))
	fmt.Print(fmt.Sprintf("\tPriceOrderTime: %s\n", p.PriceOrderTime))
	fmt.Print(fmt.Sprintf("\tPriceOrderMs: %d\n", p.PriceOrderMs))
	fmt.Print(fmt.Sprintf("\tProducts:\n"))
	for i, product := range p.Products {
		fmt.Print(fmt.Sprintf("\t\t[Product #%d]\n", i))
		fmt.Print(fmt.Sprintf("\t\tID: %d\n", product.ID))
		fmt.Print(fmt.Sprintf("\t\tCode: %s\n", product.Code))
		fmt.Print(fmt.Sprintf("\t\tName: %s\n", product.Name))
		fmt.Print(fmt.Sprintf("\t\tQuantity: %d\n", product.Quantity))
		fmt.Print(fmt.Sprintf("\t\tCategoryCode: %s\n", product.CategoryCode))
		fmt.Print(fmt.Sprintf("\t\tFlavorCode: %s\n", product.FlavorCode))
		fmt.Print(fmt.Sprintf("\t\tPrice: %.02f\n", product.Price))
		fmt.Print(fmt.Sprintf("\t\tStatus: %d\n", product.Status))
		fmt.Print(fmt.Sprintf("\t\tLikeProductID: %d\n", product.LikeProductID))
		fmt.Print(fmt.Sprintf("\t\tIsNew: %t\n", product.IsNewResp))
		fmt.Print(fmt.Sprintf("\t\tisNew: %t\n", product.IsNewReq))
		fmt.Print(fmt.Sprintf("\t\tNeedsCustomization: %t\n", product.NeedsCustomization))
		fmt.Print(fmt.Sprintf("\t\tAutoRemove: %t\n", product.AutoRemove))
		fmt.Print(fmt.Sprintf("\t\tFulfilled: %t\n", product.Fulfilled))
		fmt.Print(fmt.Sprintf("\t\tDescriptions:\n"))
		for _, description := range product.Descriptions {
			fmt.Print(fmt.Sprintf("\t\t\tportionCode: %s\n", description.PortionCode))
			fmt.Print(fmt.Sprintf("\t\t\tvalue: %s\n", description.Value))
		}
	}
	fmt.Print(fmt.Sprintf("\tPayments:\n"))
	for i, payment := range p.Payments {
		fmt.Print(fmt.Sprintf("\t\t[Payment #%d]\n", i))
		fmt.Print(fmt.Sprintf("\t\tType: %s\n", payment.Type))
		fmt.Print(fmt.Sprintf("\t\tCardType: %s\n", payment.CardType))
		fmt.Print(fmt.Sprintf("\t\tNumber: %s\n", payment.Number))
		fmt.Print(fmt.Sprintf("\t\tExpiration: %s\n", payment.Expiration))
		fmt.Print(fmt.Sprintf("\t\tSecurityCode: %s\n", payment.SecurityCode))
		fmt.Print(fmt.Sprintf("\t\tPostalCode: %s\n", payment.PostalCode))
	}
	fmt.Print(fmt.Sprintf("\tStatusItems:\n"))
	for _, statusItem := range p.StatusItems {
		fmt.Print(fmt.Sprintf("\t\tCode: %s\n", statusItem.Code))
	}
	fmt.Print(fmt.Sprintf("\tCorrective Action:\n"))
	fmt.Print(fmt.Sprintf("\t\tCode: %s\n", p.CorrectiveAction.Code))
	fmt.Print(fmt.Sprintf("\t\tAction: %s\n", p.CorrectiveAction.Action))
	fmt.Print(fmt.Sprintf("\t\tDetail: %s\n", p.CorrectiveAction.Detail))
}
