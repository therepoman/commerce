package dominos

// FindNearbyStores API models (request is shared with GetMenuFromNearbyStore req as well)
type NearbyStoresRequest struct {
	Street        string
	PostalCode    string
	ServiceMethod string
}

type FindNearbyStoresResponse struct {
	Granularity string  `json:"Granularity"`
	Address     Address `json:"Address"`
	Stores      []Store `json:"Stores"`
}

// GetMenuByStoreID API models (response is shared withe GetMenuFromNearbyStore resp as well)
type GetMenuByStoreIDRequest struct {
	StoreID string
}

type MenuResponse struct {
	Misc                     StoreMisc                          `json:"Misc"`
	Coupons                  map[string]Coupon                  `json:"Coupons"`
	Flavors                  map[string]map[string]Flavor       `json:"Flavors"` // {`productType`: {`flavorCode`: Flavor}}
	Products                 map[string]Product                 `json:"Products"`
	Sizes                    map[string]map[string]Size         `json:"Sizes"`    // {`productType`: {`sizeCode`: Size}}
	Toppings                 map[string]map[string]Topping      `json:"Toppings"` // {`productType`: {`toppingCode`: Topping}}
	Variants                 map[string]Variant                 `json:"Variants"`
	CookingInstructions      map[string]CookingInstruction      `json:"CookingInstructions"`
	CookingInstructionGroups map[string]CookingInstructionGroup `json:"CookingInstructionGroups"`
}

// Request model for Validation, GetPrice
type CustomerRequest struct {
	Street        string
	PostalCode    string
	ServiceMethod string
	FirstName     string
	LastName      string
	Email         string
	Phone         string
	AddressType   string
	VariantCodes  []string
}

// Request model for Order API
type OrderRequest struct {
	CustomerRequest      CustomerRequest
	PaymentType          string
	CardNumber           string
	Expiration           string
	SecurityCode         string
	BillingStreetAddress string
	PaymentPostalCode    string
}

// Request model of Price and Order for Domino's endpoints
type DominoOrderRequest struct {
	Order PricedOrder `json:"Order"`
}

// Request model of Validate for Domino's endpoint
type ValidateRequestConfig struct {
	FirstName string  `json:"FirstName"`
	LastName  string  `json:"LastName"`
	Email     string  `json:"Email"`
	Phone     string  `json:"Phone"`
	Address   Address `json:"Address"`
	StoreID   string  `json:"StoreID"`
}

type ValidateDominoRequest struct {
	Order ValidateRequestConfig `json:"Order"`
}

// Response model for our/Domino's Validation, GetPrice, Order
type DominoOrderResponse struct {
	Order  PricedOrder `json:"Order"`
	Status int         `json:"Status"`
}
