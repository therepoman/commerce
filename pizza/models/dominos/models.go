package dominos

type Address struct {
	Street       string `json:"Street"`
	StreetNumber string `json:"StreetNumber"`
	StreetName   string `json:"StreetName"`
	UnitType     string `json:"UnitType"`
	UnitNumber   string `json:"UnitNumber"`
	City         string `json:"City"`
	Region       string `json:"Region"`
	PostalCode   string `json:"PostalCode"`
	Type         string `json:"Type"`
}

type WaitMinutes struct {
	Min int `json:"Min"`
	Max int `json:"Max"`
}

type EstimatedWaitMinutes struct {
	Delivery WaitMinutes `json:"Delivery"`
	Carryout WaitMinutes `json:"Carryout"`
}

type Store struct {
	StoreID                           string               `json:"StoreID"`
	IsDeliveryStore                   bool                 `json:"IsDeliveryStore"`
	MinDistance                       float32              `json:"MinDistance"`
	MaxDistance                       float32              `json:"MaxDistance"`
	Phone                             string               `json:"Phone"`
	AddressDescription                string               `json:"AddressDescription"` // "112 1st ave S\nSeattle, WA 98104\nNear Century Link Field. "
	HolidaysDescription               string               `json:"HolidaysDescription"`
	HoursDescription                  string               `json:"HoursDescription"` // "Su-Th 10:00am-1:00am\nFr-Sa 10:00am-2:00am"
	IsOnlineCapable                   bool                 `json:"IsOnlineCapable"`
	IsOnlineNow                       bool                 `json:"IsOnlineNow"`
	IsNEONow                          bool                 `json:"IsNEONow"`
	IsSpanish                         bool                 `json:"IsSpanish"`
	LocationInfo                      string               `json:"LocationInfo"` // "Near Century Link Field. "
	AllowDeliveryOrders               bool                 `json:"AllowDeliveryOrders"`
	AllowCarryoutOrders               bool                 `json:"AllowCarryoutOrders"`
	ServiceMethodEstimatedWaitMinutes EstimatedWaitMinutes `json:"ServiceMethodEstimatedWaitMinutes"`
	AllowPickupWindowOrders           bool                 `json:"AllowPickupWindowOrders"`
	IsOpen                            bool                 `json:"IsOpen"`
	ServiceIsOpen                     map[string]bool      `json:"ServiceIsOpen"` // {"Carryout": true, "Delivery": true}
}

type CartItem struct {
	VariantCode string                       `json:"Code"`
	Quantity    int                          `json:"Qty"`
	ID          int                          `json:"ID"`
	Options     map[string]map[string]string `json:"Options"`
	IsNew       bool                         `json:"isNew"`
}

type StoreMisc struct {
	Status        int    `json:"Status"`
	StoreID       string `json:"StoreID"`
	LanguageCode  string `json:"LanguageCode"`  // "en"
	StoreAsOfTime string `json:"StoreAsOfTime"` // "2019-02-05 14:54:45"
	ExpiresOn     string `json:"ExpiresOn"`
}

type CouponTags struct {
	ValidServiceMethods  interface{} `json:"ValidServiceMethods"` // ["Carryout", "Delivery", "Hotspot"] -- sometimes just a single string, thus the filed is interface
	EffectiveOn          string      `json:"EffectiveOn"`         // "2013-09-24"
	ExpiresOn            string      `json:"ExpiresOn"`           // "2013-09-24"
	MultiSame            bool        `json:"MultiSame"`
	Combine              string      `json:"Combine"` // "Complementary"
	NoPulseDefaults      bool        `json:"NoPulseDefaults"`
	CombinedSizeAndCrust bool        `json:"combinedSizeAndCrust"`
}

type Coupon struct {
	Code        string     `json:"Code"`
	ImageCode   string     `json:"ImageCode"`
	Description string     `json:"Description"`
	Name        string     `json:"Name"`
	Price       string     `json:"Price"`
	Tags        CouponTags `json:"Tags"`
	Local       bool       `json:"Local"`
	Bundle      bool       `json:"Bundle"`
}

type Flavor struct {
	Code        string `json:"Code"` // "THIN"
	Name        string `json:"Name"` // "Crunchy Thin Crust"
	Description string `json:"Description"`
	Local       bool   `json:"Local"`
	SortSeq     string `json:"SortSeq"`
}

type Product struct {
	Code              string   `json:"Code"` // "14TDELUX"
	Description       string   `json:"Description"`
	ImageCode         string   `json:"ImageCode"`
	Local             bool     `json:"Local"`
	Name              string   `json:"Name"`              // "Deluxe"
	ProductType       string   `json:"ProductType"`       // "Pizza"
	AvailableToppings string   `json:"AvailableToppings"` // "X=0:0.5:1:1.5,Xm=0:0.5:1:1.5,Bq,Xw=0:0.5:1:1.5,C,P,M,O,G,S,Xf=0:0.5:1:1.5"
	DefaultToppings   string   `json:"DefaultToppings"`   // "X=1,C=1,P=1,M=1,O=1,G=1,S=1"
	Variants          []string `json:"Variants"`
}

type Size struct {
	Code        string `json:"Code"` // "12"
	Name        string `json:"Name"` // "Medium (12\")"
	Description string `json:"Description"`
	Local       bool   `json:"Local"`
	SortSeq     string `json:"SortSeq"` // "04"
}

type ToppingTag struct {
	WholeOnly      bool   `json:"WholeOnly"`
	IgnoreQty      bool   `json:"IgnoreQty"` // true: no price change regardless of the quantity
	Cheese         bool   `json:"Cheese"`
	Sauce          bool   `json:"Sauce"`
	Meat           bool   `json:"Meat"`
	NonMeat        bool   `json:"NonMeat"`
	Vege           bool   `json:"Vege"`
	ExclusiveGroup string `json:"ExclusiveGroup"` // "Sauce"
	BaseOptionQty  string `json:"BaseOptionQty"`  // "1"
}

type Topping struct {
	Code        string     `json:"Code"` // "X"
	Description string     `json:"Description"`
	Name        string     `json:"Name"` // "Robust Inspired Tomato Sauce"
	Tags        ToppingTag `json:"Tags"`
}

type Variant struct {
	Code                       string `json:"Code"`
	FlavorCode                 string `json:"FlavorCode"` // "THIN"
	ImageCode                  string `json:"ImageCode"`
	Local                      bool   `json:"Local"`
	Name                       string `json:"Name"`  // "Large (14\") Thin Deluxe"
	Price                      string `json:"Price"` // "18.99"
	ProductCode                string `json:"ProductCode"`
	SizeCode                   string `json:"SizeCode"`                   // "12"
	AllowedCookingInstructions string `json:"AllowedCookingInstructions"` // "PIECT,SQCT,UNCT,RGO,NOOR"
	DefaultCookingInstructions string `json:"DefaultCookingInstructions"` // "SQCT,RGO"
	Prepared                   bool   `json:"Prepared"`
}

type CookingInstruction struct {
	Code        string `json:"Code"` // "PIECT"
	Name        string `json:"Name"` // "Pie Cut"
	Description string `json:"Description"`
	Group       string `json:"Group"` // "CUT"
}

type CookingInstructionGroup struct {
	Code string            `json:"Code"` // "CUT"
	Name string            `json:"Name"` // "Cut"
	Tags map[string]string `json:"Tags"` // {"MaxOptions": "1"}
}

type Menu struct {
	Misc                     StoreMisc                          `json:"Misc"`
	Coupons                  map[string]Coupon                  `json:"Coupons"`
	Flavors                  map[string]map[string]Flavor       `json:"Flavors"` // {`productType`: {`flavorCode`: Flavor}}
	Products                 map[string]Product                 `json:"Products"`
	Sizes                    map[string]map[string]Size         `json:"Sizes"`    // {`productType`: {`sizeCode`: Size}}
	Toppings                 map[string]map[string]Topping      `json:"Toppings"` // {`productType`: {`toppingCode`: Topping}}
	Variants                 map[string]Variant                 `json:"Variants"`
	CookingInstructions      map[string]CookingInstruction      `json:"CookingInstructions"`
	CookingInstructionGroups map[string]CookingInstructionGroup `json:"CookingInstructionGroups"`
}
