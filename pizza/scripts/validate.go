// Calls Validate API
// To run, do `go run ./scripts/validate.go` from the root directory

package main

import (
	"fmt"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pizza/api/dominos"
	consts "code.justin.tv/commerce/pizza/constants/dominos"
	models "code.justin.tv/commerce/pizza/models/dominos"
)

func main() {
	resp, serviceAvailable, err := dominos.Validate(models.CustomerRequest{
		FirstName:    "Sokihalisan",
		LastName:     "Nokihalisan",
		Email:        "whatever@fakeemail",
		Phone:        "1-000-000-0000",
		Street:       "1007 Stewart St",
		PostalCode:   "98101",
		AddressType:  consts.ADDRESS_TYPE_BUSINESS,
		VariantCodes: []string{"P14IREUH"},
	})
	if err != nil {
		log.WithError(err).Panic("Failed to validate")
	}

	if resp.Status > -1 {
		fmt.Print("Validation succeeded!\n")
	} else {
		fmt.Print("Validation failed...\n")
	}
	fmt.Print(fmt.Sprintf("Status: %d\n", resp.Status))
	fmt.Print(fmt.Sprintf("Service Availability (%s): %t\n", resp.Order.ServiceMethod, serviceAvailable))
	fmt.Print(fmt.Sprintf("Order:\n"))
	resp.Order.PrettyPrint()
}
