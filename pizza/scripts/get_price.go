// Calls GetPrice API
// To run, do `go run ./scripts/get_price.go` from the root directory

package main

import (
	"fmt"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pizza/api/dominos"
	consts "code.justin.tv/commerce/pizza/constants/dominos"
	models "code.justin.tv/commerce/pizza/models/dominos"
)

func main() {
	req := models.CustomerRequest{
		FirstName:    "Sokihalisan",
		LastName:     "Nokihalisan",
		Email:        "sokihalisan@gmail.com",
		Phone:        "0000000000",
		Street:       "1007 Stewart St",
		PostalCode:   "98101",
		AddressType:  consts.ADDRESS_TYPE_BUSINESS,
		VariantCodes: []string{"P14IREUH"},
	}

	resp, serviceAvailable, err := dominos.GetPrice(req)
	if err != nil {
		log.WithError(err).Panic("API call failed")
	}

	fmt.Print(fmt.Sprintf("Status: %d\n", resp.Status))
	fmt.Print(fmt.Sprintf("Service Availability (%s): %t\n", resp.Order.ServiceMethod, serviceAvailable))
	fmt.Print(fmt.Sprintf("Order:\n"))
	resp.Order.PrettyPrint()
}
