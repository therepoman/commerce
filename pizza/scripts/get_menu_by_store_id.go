// Calls GetMenuByStoreID API
// To run, do `go run ./scripts/get_menu_by_store_id.go` from the root directory

package main

import (
	"fmt"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pizza/api/dominos"
	consts "code.justin.tv/commerce/pizza/constants/dominos"
	models "code.justin.tv/commerce/pizza/models/dominos"
)

func main() {
	req := models.GetMenuByStoreIDRequest{
		StoreID: "7063",
	}

	resp, err := dominos.GetMenuByStoreID(req)
	if err != nil {
		log.WithError(err).Panic("FindNearbyStores failed.")
	}

	for productID, product := range resp.Products {
		if product.ProductType != consts.PRODUCT_TYPE_PIZZA {
			continue
		}
		log.Info(fmt.Sprintf("ID: %s", productID))
		log.Info(fmt.Sprintf("\tCode: %s", product.Code))
		log.Info(fmt.Sprintf("\tImageCode: %s", product.ImageCode))
		log.Info(fmt.Sprintf("\tName: %s", product.Name))
		log.Info(fmt.Sprintf("\tDescription: %s", product.Description))
		log.Info(fmt.Sprintf("\tProductType: %s", product.ProductType))
		log.Info(fmt.Sprintf("\tAvailableToppings: %s", product.AvailableToppings))
		log.Info(fmt.Sprintf("\tDefaultToppings: %s", product.DefaultToppings))
		for index, variant := range product.Variants {
			log.Info(fmt.Sprintf("\t\t%d. Variant ID: %s", index, variant))
		}
	}
}
