// Calls FindNearbyStores API
// To run, do `go run ./scripts/find_nearby_stores.go` from the root directory

package main

import (
	"fmt"
	"strings"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pizza/api/dominos"
	consts "code.justin.tv/commerce/pizza/constants/dominos"
	models "code.justin.tv/commerce/pizza/models/dominos"
)

func main() {
	req := models.NearbyStoresRequest{
		Street:        "1007 Stewart St",
		PostalCode:    "98101",
		ServiceMethod: consts.SERVICE_METHOD_DELIVERY,
	}

	resp, err := dominos.FindNearbyStores(req)
	if err != nil {
		log.WithError(err).Panic("FindNearbyStores failed.")
	}

	log.Info(fmt.Sprintf("Granularity: %s", resp.Granularity))
	log.Info(fmt.Sprintf("Address:"))
	log.Info(fmt.Sprintf("\tStreet: %s", resp.Address.Street))
	log.Info(fmt.Sprintf("\tStreetNumber: %s", resp.Address.StreetNumber))
	log.Info(fmt.Sprintf("\tStreetName: %s", resp.Address.StreetName))
	log.Info(fmt.Sprintf("\tUnitType: %s", resp.Address.UnitType))
	log.Info(fmt.Sprintf("\tUnitNumber: %s", resp.Address.UnitNumber))
	log.Info(fmt.Sprintf("\tCity: %s", resp.Address.City))
	log.Info(fmt.Sprintf("\tRegion: %s", resp.Address.Region))
	log.Info(fmt.Sprintf("\tPostalCode: %s", resp.Address.PostalCode))

	for index, store := range resp.Stores {
		log.Info(fmt.Sprintf("[Store #%d]", index+1))
		log.Info(fmt.Sprintf("\tStore ID: %s", store.StoreID))
		log.Info(fmt.Sprintf("\tIsDeliveryStore: %t", store.IsDeliveryStore))
		log.Info(fmt.Sprintf("\tMinDistance: %f", store.MinDistance))
		log.Info(fmt.Sprintf("\tMaxDistance: %f", store.MaxDistance))
		log.Info(fmt.Sprintf("\tPhone: %s", store.Phone))
		log.Info(fmt.Sprintf("\tAddressDescription: %s", strings.Replace(store.AddressDescription, "\n", ", ", -1)))
		log.Info(fmt.Sprintf("\tHolidaysDescription: %s", store.HolidaysDescription))
		log.Info(fmt.Sprintf("\tHoursDescription: %s", strings.Replace(store.HoursDescription, "\n", ", ", -1)))
		log.Info(fmt.Sprintf("\tIsOnlineCapable: %t", store.IsOnlineCapable))
		log.Info(fmt.Sprintf("\tIsOnlineNow: %t", store.IsOnlineNow))
		log.Info(fmt.Sprintf("\tIsNEONow: %t", store.IsNEONow))
		log.Info(fmt.Sprintf("\tIsSpanish: %t", store.IsSpanish))
		log.Info(fmt.Sprintf("\tLocationInfo: %s", store.LocationInfo))
		log.Info(fmt.Sprintf("\tAllowDeliveryOrders: %t", store.AllowDeliveryOrders))
		log.Info(fmt.Sprintf("\tAllowCarryoutOrders: %t", store.AllowCarryoutOrders))
		log.Info(fmt.Sprintf("\t\tDelivery:"))
		log.Info(fmt.Sprintf("\t\t\tMinimum: %d minute(s)", store.ServiceMethodEstimatedWaitMinutes.Delivery.Min))
		log.Info(fmt.Sprintf("\t\t\tMaximum: %d minute(s)", store.ServiceMethodEstimatedWaitMinutes.Delivery.Max))
		log.Info(fmt.Sprintf("\t\tCarryout:"))
		log.Info(fmt.Sprintf("\t\t\tMinimum: %d minute(s)", store.ServiceMethodEstimatedWaitMinutes.Carryout.Min))
		log.Info(fmt.Sprintf("\t\t\tMaximum: %d minute(s)", store.ServiceMethodEstimatedWaitMinutes.Carryout.Max))
		log.Info(fmt.Sprintf("\tAllowPickupWindowOrders: %t", store.AllowPickupWindowOrders))
		log.Info(fmt.Sprintf("\tIsOpen: %t", store.IsOpen))
		log.Info(fmt.Sprintf("\tServiceIsOpen:"))
		for service, open := range store.ServiceIsOpen {
			log.Info(fmt.Sprintf("\t\t%s: %t", service, open))
		}
	}
}
