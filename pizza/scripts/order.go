// Calls Order API
// To run, do `go run ./scripts/order.go` from the root directory

package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pizza/api/dominos"
	consts "code.justin.tv/commerce/pizza/constants/dominos"
	models "code.justin.tv/commerce/pizza/models/dominos"
)

func main() {
	var street, addressType, postalCode, firstName, lastName, email, phone, itemCode string

	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter your street address: ")
	street, _ = reader.ReadString('\n')
	street = strings.Replace(street, " ", "%20", -1)
	removeNewline(&street)

	fmt.Print("Enter address type (Business, Apartment, House, Hotel, Campus/Base, Other): ")
	addressType, _ = reader.ReadString('\n')
	removeNewline(&addressType)

	fmt.Print("Enter postal code: ")
	postalCode, _ = reader.ReadString('\n')
	removeNewline(&postalCode)

	fmt.Print("Enter your first name: ")
	firstName, _ = reader.ReadString('\n')
	removeNewline(&firstName)

	fmt.Print("Enter your last name: ")
	lastName, _ = reader.ReadString('\n')
	removeNewline(&lastName)

	fmt.Print("Enter your email: ")
	email, _ = reader.ReadString('\n')
	removeNewline(&email)

	fmt.Print("Enter phone number: ")
	phone, _ = reader.ReadString('\n')
	removeNewline(&phone)

	fmt.Printf("Enter the product code you want to order: ")
	itemCode, _ = reader.ReadString('\n')
	removeNewline(&itemCode)

	// Service method is assumed to be 'Delivery' here
	customerRequest := models.CustomerRequest{
		Street:       street,
		PostalCode:   postalCode,
		FirstName:    firstName,
		LastName:     lastName,
		Email:        email,
		Phone:        phone,
		AddressType:  addressType,
		VariantCodes: []string{itemCode},
	}

	priceResp, serviceAvailable, err := dominos.GetPrice(customerRequest)
	if err != nil {
		log.WithError(err).Error("Failed to get price information. Fix your problem")
		return
	}

	fmt.Print(fmt.Sprintf("Service Availability (%s): %t\n", priceResp.Order.ServiceMethod, serviceAvailable))
	fmt.Print("Confirm the following information:\n")
	fmt.Print(fmt.Sprintf("========================================================\n"))
	priceResp.Order.PrettyPrint()
	fmt.Print(fmt.Sprintf("========================================================\n"))
	fmt.Print(fmt.Sprintf("Total: %.02f %s\n", priceResp.Order.Amounts.Customer, priceResp.Order.Currency))
	fmt.Print(fmt.Sprintf("Confirm (y/n): "))

	confirmed, _ := reader.ReadString('\n')
	if confirmed != "y\n" {
		fmt.Print("\nOk, bye\n\n")
		return
	}

	var cardNumber, expiration, securityCode, billingStreet, zipCode string

	fmt.Print("Enter card number: ")
	cardNumber, _ = reader.ReadString('\n')
	removeNewline(&cardNumber)

	fmt.Print("Enter expiration (e.g. 09/21): ")
	expiration, _ = reader.ReadString('\n')
	removeNewline(&expiration)

	fmt.Print("Enter security code: ")
	securityCode, _ = reader.ReadString('\n')
	removeNewline(&securityCode)

	fmt.Print("Enter billing street: ")
	billingStreet, _ = reader.ReadString('\n')
	removeNewline(&billingStreet)

	fmt.Print("Enter postal code: ")
	zipCode, _ = reader.ReadString('\n')
	removeNewline(&zipCode)

	resp, err := dominos.Order(models.OrderRequest{
		CustomerRequest:      customerRequest,
		PaymentType:          consts.PAYMENT_METHOD_CREDITCARD,
		CardNumber:           cardNumber,
		Expiration:           expiration,
		SecurityCode:         securityCode,
		BillingStreetAddress: billingStreet,
		PaymentPostalCode:    zipCode,
	})
	if err != nil {
		log.WithFields(log.Fields{
			"Action": resp.Order.CorrectiveAction.Action,
			"Code":   resp.Order.CorrectiveAction.Code,
			"Detail": resp.Order.CorrectiveAction.Detail,
		}).Error("Corrective action implied")
		log.WithError(err).Panic("Failed to order")
	}

	if resp.Status != -1 && resp.Order.Status != -1 {
		log.Info("Order placed! See the details below:\n")
	} else {
		log.Info("Order has gone wrong. See the details below:\n")
	}

	fmt.Print(fmt.Sprintf("========================================================\n"))
	resp.Order.PrettyPrint()
}

func removeNewline(s *string) {
	cpy := *s
	size := len(cpy)
	*s = cpy[:size-1]
}
