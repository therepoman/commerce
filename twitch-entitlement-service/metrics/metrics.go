package metrics

import (
	"time"

	"github.com/aws/aws-sdk-go/aws/credentials"

	"code.justin.tv/commerce/AmazonMWSGoClient/mws"
)

const (
	awsID     = "AKIAJQT6A3TIK2ODXYZA"
	awsSecret = "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"
	awsToken  = ""

	hostSpecific = false
	marketplace  = "us-west-2"
	serviceName  = "TwitchEntitlementService"
	client       = ""
	environment  = "TwitchEntitlementService/NA"
	hostname     = "Visage"
	partitionID  = ""
	customerID   = ""

	minPeriod = mws.PeriodFiveMinute
	maxPeriod = mws.PeriodFiveMinute

	metricTypeTime = "Time"

	// Exported metrics types that can be used for counts.

	// MetricTypeSuccesses is the number of successes for a provided API.
	MetricTypeSuccesses = "Success"
	// MetricTypeServerError the number of server errors for a provided API.
	MetricTypeServerError = "Server_Error"
	// MetricTypeClientError the number of client errors for a provided API.
	MetricTypeClientError = "Client_Error"
	// MetricInvalidReceiptsType the number of invalid receipts (with NOOP as next instruction) passed to the /fulfill endpoint.
	MetricInvalidReceiptsType = "Invalid_Receipts"
)

func statusToMetricType(status int) string {
	var metricType = ""
	if status < 300 {
		// Anything below 300 is a success.
		metricType = MetricTypeSuccesses
	} else if status < 500 {
		// Anything between 300 and 500 is a client error of some sort.
		metricType = MetricTypeClientError
	} else {
		// Anything above 500 is a server error.
		metricType = MetricTypeServerError
	}

	return metricType
}

// PostLatencyMetric wll post the latency for a given method and client to PMET.
func PostLatencyMetric(isProd bool, clientString string, methodName string, latency time.Duration) {
	// Initialize the Latency metric object.
	latencyMetric := mws.NewMetric(isProd, hostname, hostSpecific, marketplace, serviceName, methodName, clientString, metricTypeTime)

	// Add the actual latency value to the metric.
	latencyMetric.AddValue(latency.Seconds() * 1000.0)
	latencyMetric.Unit = mws.UnitMilliseconds
	latencyMetric.Timestamp = time.Now().UTC()

	// Post it !
	postMetric(isProd, methodName, latencyMetric)
}

// PostStatusMetric wll post the appropriate metric for a given method and client to PMET given the provided return status.
func PostStatusMetric(isProd bool, clientString string, methodName string, status int) {
	PostCountMetric(isProd, clientString, methodName, 1.0, statusToMetricType(status))
}

// PostCountMetric posts a counter metric to PMET, the type is provided as a parameter.
func PostCountMetric(isProd bool, clientString string, methodName string, value float64, metricType string) {
	// Initialize the metric
	countMetric := mws.NewMetric(isProd, hostname, hostSpecific, marketplace, serviceName, methodName, clientString, metricType)

	// Add the success value.
	countMetric.AddValue(value)
	countMetric.Unit = "Count"
	countMetric.Timestamp = time.Now().UTC()

	// Post it !
	postMetric(isProd, methodName, countMetric)
}

func postMetric(isProd bool, methodName string, metric mws.Metric) {
	// Initialize MetricReport and add the Metric.
	metricReport := mws.NewMetricReport(isProd, hostname, minPeriod, maxPeriod, partitionID, environment, customerID)
	metricReport.AddMetric(metric)

	// Wrap the MetricReport in a request object.
	req := mws.NewPutMetricsForAggregationRequest()
	req.AddMetricReport(metricReport)

	// Initialize credentials. They need to be from the same region we're posting in.
	creds := credentials.NewStaticCredentials(awsID, awsSecret, awsToken)

	_, metricsErr := mws.PutMetricsForAggregation(req, mws.Regions["CMH"], creds) // CMH is us-west-2.
	if metricsErr != nil {
		return // Do nothing for now...but the metrics publish has failed.
	}
}
