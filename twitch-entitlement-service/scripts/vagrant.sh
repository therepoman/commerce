apt-get install -y unzip curl
wget https://releases.hashicorp.com/terraform/0.6.11/terraform_0.6.11_linux_amd64.zip
sudo unzip -d /usr/local/bin terraform_0.6.11_linux_amd64.zip
curl -O https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py
sudo pip install awscli
