package entitlements

// GetGoodsForUserResponse is the response structure for the GetGoodsForUser API.
type GetGoodsForUserResponse struct {
	Goods []Goods `json:"goods"`
}

// Goods model aggregates the user entitlements
type Goods struct {
	ReceiptID       string `json:"receipt_id"`
	SKU             string `json:"sku"`
	NextInstruction string `json:"next_instruction"`
}

// SetStatusForReceiptsRequest is the input request body from the vendor client
type SetStatusForReceiptsRequest struct {
	ReceiptUpdates []ReceiptToInstruction `json:"receipt_updates"`
}

// ReceiptToInstruction is the struct created from the input request body
type ReceiptToInstruction struct {
	FulfillmentAddress string `json:"fulfillment_address"`
	ReceiptID          string `json:"receipt_id"`
	LastInstruction    string `json:"last_instruction"`
}

// SetStatusForReceiptsResponse for TES SetStatusForReceipts API
// This can be called by multiple Twitch endpoints (user/goods/fulfill, user/goods/revoke)
type SetStatusForReceiptsResponse struct {
	ReceiptUpdateResults []ReceiptUpdateResult `json:"receipt_update_results"`
}

// ReceiptUpdateResult aggregates results for fulfilling receipts
type ReceiptUpdateResult struct {
	ReceiptID string `json:"receipt_id"`
	Result    string `json:"result"` // SuccessModifiedRequest, Success, Error
}
