package entitlements

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"code.justin.tv/commerce/twitch-entitlement-service/external"
	"code.justin.tv/commerce/twitch-entitlement-service/externaladg"
	"code.justin.tv/commerce/twitch-entitlement-service/externalaws"
	"code.justin.tv/commerce/twitch-entitlement-service/metrics"

	"code.justin.tv/common/twitchhttp"

	"github.com/cactus/go-statsd-client/statsd"

	"golang.org/x/net/context"
)

const (
	defaultTimingXactName = "TwitchEntitlementService"
	defaultStatSampleRate = 0.1

	statsHost                = "statsd.internal.justin.tv:8125"
	statsPrefix              = ""
	userGoodsStatusMetric    = "amzn-entitlements.user.goods.status"
	userGoodsLatencyMetric   = "amzn-entitlements.user.goods.latency"
	userFulfillStatusMetric  = "amzn-entitlements.user.fulfill.status"
	userFulfillLatencyMetric = "amzn-entitlements.user.fulfill.latency"
	noOpReceiptCountMetric   = "amzn-entitlements.user.fulfill.receipts.expected.count"

	unscopedClientID = ""
	getGoodsForUserAPI = "GetGoodsForUser"
	setStatusForReceiptsAPI = "SetStatusForReceipts"
)

// Client provides an interface for the service client
type Client interface {
	GetGoodsForUser(ctx context.Context, authToken string, clientID string, isProd bool, reqOpts *twitchhttp.ReqOpts) (*GetGoodsForUserResponse, error)
	SetStatusForReceipts(ctx context.Context, authToken string, tuid string, clientID string, request SetStatusForReceiptsRequest, isProd bool, reqOpts *twitchhttp.ReqOpts) (*SetStatusForReceiptsResponse, error)
}

type client struct {
	twitchhttp.Client
}

// NewClient creates a new client for use in making service calls.
func NewClient(conf twitchhttp.ClientConf) (Client, error) {
	if conf.TimingXactName == "" {
		conf.TimingXactName = defaultTimingXactName
	}
	twitchClient, err := twitchhttp.NewClient(conf)

	return &client{twitchClient}, err
}

func postDurationMetrics(isProd bool, clientName string, methodName string, metricName string, duration time.Duration) {
	statsClient, statsErr := statsd.NewClient(statsHost, statsPrefix)
	if statsErr != nil {
		// Do nothing atm...but def not gonna collect stats
		return
	}

	defer closeStatsClient(statsClient)
	statsErr = statsClient.TimingDuration(metricName, duration, defaultStatSampleRate)
	if statsErr != nil {
		// Do nothing atm...but def not gonna collect stats.
		return
	}

	// Post PMET Metrics
	// Unscoped metric.
	metrics.PostLatencyMetric(isProd, unscopedClientID, methodName, duration)
	// Scoped metric.
	metrics.PostLatencyMetric(isProd, clientName, methodName, duration)
}

func postStatusMetrics(isProd bool, clientName string, methodName string, metricName string, status int) {
	statsClient, statsErr := statsd.NewClient(statsHost, statsPrefix)
	if statsErr != nil {
		// Do nothing atm...but def not gonna collect stats.
		return
	}

	defer closeStatsClient(statsClient)
	actualMetricName := fmt.Sprintf("%s-%d", metricName, status)
	statsErr = statsClient.Inc(actualMetricName, 1.0, defaultStatSampleRate)
	if statsErr != nil {
		// Do nothing atm...but def not gonna collect stats.
		return
	}

	// Post PMET Metrics
	// Unscoped metric
	metrics.PostStatusMetric(isProd, unscopedClientID, methodName, status)
	// Scoped metric
	metrics.PostStatusMetric(isProd, clientName, methodName, status)
}

func postCountMetrics(isProd bool, clientName string, methodName string, metricName string, count int) {
	statsClient, statsErr := statsd.NewClient(statsHost, statsPrefix)
	if statsErr != nil {
		// Do nothing atm...but def not gonna collect stats.
		return
	}

	defer closeStatsClient(statsClient)
	actualMetricName := fmt.Sprintf("%s", metricName)
	i64Count := int64(count)
	statsErr = statsClient.Inc(actualMetricName, i64Count, defaultStatSampleRate)
	if statsErr != nil {
		// Do nothing atm...but def not gonna collect stats.
		return
	}

	// Post PMET Metrics
	// Unscoped metric
	metrics.PostCountMetric(isProd, unscopedClientID, methodName, float64(count), metrics.MetricInvalidReceiptsType)
	// Scoped metric
	metrics.PostCountMetric(isProd, clientName, methodName, float64(count), metrics.MetricInvalidReceiptsType)
}

func closeStatsClient(statsClient statsd.Statter) {
	err := statsClient.Close()
	if err != nil {
		// If this fails, something major fucked up is happening...
	}
}

func (c *client) GetGoodsForUser(ctx context.Context, authToken string, clientID string, isProd bool, reqOpts *twitchhttp.ReqOpts) (*GetGoodsForUserResponse, error) {
	startTime := time.Now()

	adges := externaladg.NewADGEntitlementService()

	adgesOutput, adgesErr := adges.GetGoodsForUser(externaladg.GetGoodsForUserInput{OAuthToken: extractAuthToken(authToken)})
	if adgesErr != nil {
		// Don't post Latency metrics for failures.
		postStatusMetrics(isProd, clientID, getGoodsForUserAPI, userGoodsStatusMetric, adgesErr.GetErrorCode())
		return nil, adgesErr
	}

	goodsForUserResponse := convertOutputToResponse(adgesOutput)

	_, jsonErr := json.Marshal(goodsForUserResponse)

	if jsonErr != nil {
		// Don't post Latency metrics for failures.
		postStatusMetrics(isProd, clientID, getGoodsForUserAPI, userGoodsStatusMetric, http.StatusInternalServerError)
		return nil, jsonErr
	}

	postDurationMetrics(isProd, clientID, getGoodsForUserAPI, userGoodsLatencyMetric, time.Since(startTime))
	postStatusMetrics(isProd, clientID, getGoodsForUserAPI, userGoodsStatusMetric, http.StatusOK)
	return &goodsForUserResponse, nil
}

func convertModelToGood(model externaladg.GetGoodsForUserOutputModel) Goods {
	return Goods{ReceiptID: model.ReceiptID, SKU: model.SKU, NextInstruction: model.NextInstruction.String()}
}

func convertOutputToResponse(output externaladg.GetGoodsForUserOutput) GetGoodsForUserResponse {
	var resultGoods []Goods
	for _, v := range output.Goods {
		resultGoods = append(resultGoods, convertModelToGood(v))
	}

	return GetGoodsForUserResponse{Goods: resultGoods}
}

func extractAuthToken(authToken string) string {
	OAuth := strings.Fields(authToken)
	// either the Parameter will be of the form
	// <authtoken>
	// OAuth <authtoken>
	return OAuth[len(OAuth)-1]
}

// SetStatusForReceipts API Call using twitch HTTP client
func (c *client) SetStatusForReceipts(ctx context.Context, authToken string, tuid string, clientID string, request SetStatusForReceiptsRequest, isProd bool, reqOpts *twitchhttp.ReqOpts) (*SetStatusForReceiptsResponse, error) {
	startTime := time.Now()

	// Call to ADG Order Service for setReceiptsFulfillmentStatus
	adgos := externaladg.NewADGOrderService()
	setStatusForReceiptsResponse, errorResponse, noOpReceiptCount := callSetStatusForReceipts(ctx, authToken, tuid, clientID, request, reqOpts, adgos)

	if errorResponse == nil {
		postDurationMetrics(isProd, clientID, setStatusForReceiptsAPI, userFulfillLatencyMetric, time.Since(startTime))
		postStatusMetrics(isProd, clientID, setStatusForReceiptsAPI, userFulfillStatusMetric, http.StatusOK)
		postCountMetrics(isProd, clientID, setStatusForReceiptsAPI, noOpReceiptCountMetric, noOpReceiptCount)
	} else {
		postStatusMetrics(isProd, clientID, setStatusForReceiptsAPI, userFulfillStatusMetric, errorResponse.GetErrorCode())
	}

	return setStatusForReceiptsResponse, errorResponse
}

// callSetStatusForReceipts is a wrapper function to enable mocking out the adgos interface for testing, it returns the model response, an error, and the count of Receipts sent to ADG
func callSetStatusForReceipts(ctx context.Context, authToken string, tuid string, clientID string, request SetStatusForReceiptsRequest, reqOpts *twitchhttp.ReqOpts, adgos externaladg.ADGOrderServiceInterface) (*SetStatusForReceiptsResponse, external.Error, int) {

	var receiptUpdate externaladg.ReceiptUpdate
	receiptUpdates := make([]externaladg.ReceiptUpdate, 0)
	receiptFulfillMap := make(map[string]string)
	for _, receiptRequestUpdate := range request.ReceiptUpdates {
		if receiptRequestUpdate.ReceiptID == "" || receiptRequestUpdate.FulfillmentAddress == "" || receiptRequestUpdate.LastInstruction == "" {
			return nil, external.NewBadRequestError("Invalid request: Missing json parameters"), 0
		}
		lastInstruction := strings.ToUpper(receiptRequestUpdate.LastInstruction)
		if lastInstruction == "FULFILL" {
			receiptUpdate = externaladg.ReceiptUpdate{}
			receiptUpdate.ReceiptID = receiptRequestUpdate.ReceiptID
			receiptFulfillMap[receiptUpdate.ReceiptID] = receiptRequestUpdate.FulfillmentAddress
			receiptUpdate.Status = "FULFILLED"
			receiptUpdates = append(receiptUpdates, receiptUpdate)
		}
	}

	noOpReceiptCount := len(request.ReceiptUpdates) - len(receiptUpdates)
	if len(receiptUpdates) == 0 {
		// Need at least 1 Receipt in state of FULFILL to call ADG
		return nil, external.NewBadRequestError("Invalid request: No Incoming Receipts were in a valid FULFILL state"), 0
	}

	setFulfillmentAddress(tuid, clientID, receiptFulfillMap)
	adgosInput := externaladg.SetStatusForReceiptsInput{OAuthToken: extractAuthToken(authToken), ReceiptUpdates: receiptUpdates}
	adgosOutput, adgosErr := adgos.SetStatusForReceipts(adgosInput)
	if adgosErr != nil {
		return nil, adgosErr, 0
	}

	setStatusForReceiptsResponse := convertSetReceiptsOutputToResponse(adgosOutput)

	_, jsonErr := json.Marshal(setStatusForReceiptsResponse)

	if jsonErr != nil {
		return nil, external.NewInternalServerError("There was an error deserializing the ADG response : " + jsonErr.Error()), 0
	}

	return &setStatusForReceiptsResponse, nil, noOpReceiptCount
}

// Convert the ADG output to the response we want to return to the 3p Vendor
func convertSetReceiptsOutputToResponse(output externaladg.SetStatusForReceiptsOutput) SetStatusForReceiptsResponse {
	receiptUpdateResults := make([]ReceiptUpdateResult, 0)
	for _, receiptStatusUpdate := range output.ReceiptStatusUpdates {
		receiptUpdateResult := ReceiptUpdateResult{}
		receiptUpdateResult.ReceiptID = receiptStatusUpdate.ReceiptID
		receiptUpdateResult.Result = receiptStatusUpdate.Result
		receiptUpdateResults = append(receiptUpdateResults, receiptUpdateResult)
	}
	return SetStatusForReceiptsResponse{ReceiptUpdateResults: receiptUpdateResults}
}

func setFulfillmentAddress(tuID string, clientID string, receiptFulfillMap map[string]string) {
	service := externalaws.NewFulfillAddressService()
	for receiptID, fulfillmentAddress := range receiptFulfillMap {
		input := externalaws.UpdateFulfillmentAddressInput{Tuid: tuID, ClientID: clientID, FulfillmentAddress: &fulfillmentAddress, ReceiptID: &receiptID}
		_, err := service.UpdateFulfillmentAddress(input)
		if err != nil {
			// Do nothing for the moment
		}
	}
}
