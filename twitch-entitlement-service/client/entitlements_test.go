package entitlements

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
	"testing"

	"code.justin.tv/commerce/twitch-entitlement-service/externaladg"
	"code.justin.tv/common/twitchhttp"
	"golang.org/x/net/context"
)

const (
	expectedResponse                   = `{"goods":[{"receipt_id":"ec8ef5af-1555-00c3-c78e-1fe2979b6daa","sku":"B01J5WAHYW","next_instruction":"NOOP"},{"receipt_id":"7719ebb2-29b9-d169-ccfe-e08cf2f2ed3c","sku":"B01J5WAH5G","next_instruction":"NOOP"},{"receipt_id":"e37fba3d-db53-a032-e86a-17fbd262017a","sku":"B01J5WAH5G","next_instruction":"NOOP"},{"receipt_id":"1cabbe84-7ea8-2470-a87f-add234e7d36f","sku":"com.proletariat.streamline.prime_bundle","next_instruction":"NOOP"},{"receipt_id":"adfc5d01-9fc4-44a4-920e-10c6c15a932e","sku":"com.proletariat.streamline.prime_bundle","next_instruction":"NOOP"},{"receipt_id":"df2de667-af70-6caa-1d96-05ce58e21d80","sku":"B01J5WAH5G","next_instruction":"NOOP"},{"receipt_id":"6a7fc6df-59e2-464a-b3e6-7ef3f23896e4","sku":"com.proletariat.streamline","next_instruction":"NOOP"},{"receipt_id":"fb3fc2f9-e0cb-4b96-a003-f6ccb268a0e3","sku":"B01J5WAH5G","next_instruction":"NOOP"},{"receipt_id":"36abbe84-7ea8-63d6-49af-b133c14ae28e","sku":"com.proletariat.streamline","next_instruction":"NOOP"},{"receipt_id":"fe14cc10-6a24-d938-46cf-13e0af7c2a57","sku":"B01J5WAH5G","next_instruction":"NOOP"},{"receipt_id":"f0ac3747-16c7-e272-ea52-c94ce27c7a8a","sku":"This War of Mine Anniversary Edition","next_instruction":"NOOP"},{"receipt_id":"aeac52eb-dbd6-2473-1f6a-abfe264df5cc","sku":"TwitchPrimeSku","next_instruction":"FULFILL"}]}`
	sampleURL                          = "dummyUrl"
	sampleAuthToken                    = "OAuth f0uysmopidxsa2fud0v1kfedisyvb0"
	sampleTUID                         = "SampleTUID"
	sampleClientID                     = "SampleClientID"
	setReceiptsRequestBody             = `{"receipt_updates":[{"receipt_id":"f0ac3747-16c7-e272-ea52-c94ce27c7a8a","last_instruction":"FULFILL", "fulfillment_address":"SampleGamer"}]}`
	setReceiptsRequestBodyInvalid      = `{"receipt_updates":[{"receipt_id":"f0ac3747-16c7-e272-ea52-c94ce27c7a8a","last_instruction":"INRUCT-SHIN"}]}`
	setMultiReceiptsRequestBody        = `{"receipt_updates":[{"receipt_id":"f0ac3747-16c7-e272-ea52-c94ce27c7a8a","last_instruction":"FULFILL","fulfillment_address":"SampleGamer1"},{"receipt_id":"ec8ef5af-1555-00c3-c78e-1fe2979b6daa","last_instruction":"FULFILL","fulfillment_address":"SampleGamer2"}]}`
	setMultiReceiptsRequestBodyInvalid = `{"receipt_updates":[{"receipt_id":"f0ac3747-16c7-e272-ea52-c94ce27c7a8a","last_instruction":"NOOP","fulfillment_address":"SampleGamer1"},{"receipt_id":"ec8ef5af-1555-00c3-c78e-1fe2979b6daa","last_instruction":"FULFILL","fulfillment_address":"SampleGamer1"}]}`
)

// Test the GetGoodsForUser API
func TestGetGoodsForUser(t *testing.T) {
	t.Skip() // Skipping this test for now. Should use integ test instead for this.

	tesClient, err := NewClient(twitchhttp.ClientConf{Host: sampleURL})
	if err != nil {
		t.Fatal(err)
	}

	reqOpts := twitchhttp.ReqOpts{
		StatName:       "service.twitch-entitlement-service.get_goods_for_user",
		StatSampleRate: defaultStatSampleRate,
	}

	goodsForUser, extError := tesClient.GetGoodsForUser(context.Background(), sampleAuthToken, sampleClientID, false, &reqOpts)

	if extError != nil {
		t.Error("Error calling GetGoodsForUser")
	}

	data, jsonErr := json.Marshal(goodsForUser)
	if jsonErr != nil {
		t.Error("Error Marshalling goodsForUserResponse")
	}

	if strings.Compare(string(data), expectedResponse) != 0 {
		t.Error("\nExpected response: " + expectedResponse + " \nActual response: " + string(data))
	}
}

func verifyGood(t *testing.T, prefix string, good Goods, expectedReceiptID string, expectedSKU string, expectedNextInstruction string) {
	if good.ReceiptID != expectedReceiptID {
		t.Error(prefix + "Unexpected ReceiptId. Got " + good.ReceiptID + " but expected " + expectedReceiptID)
	}

	if good.SKU != expectedSKU {
		t.Error(prefix + "Unexpected SKU. Got " + good.SKU + " but expected " + expectedSKU)
	}

	if good.NextInstruction != expectedNextInstruction {
		t.Error(prefix + "Unexpected NextInstruction. Got " + good.NextInstruction + " but expected " + expectedNextInstruction)
	}
}

func TestConvertOutputToResponse(t *testing.T) {
	expectedReceiptID1 := "RE-SEAT ID"
	expectedSKU1 := "SKUSE ME !?!?"
	nextInstruction1 := externaladg.Fulfill
	expectedNextInstruction1 := nextInstruction1.String()
	model1 := externaladg.GetGoodsForUserOutputModel{ReceiptID: expectedReceiptID1, SKU: expectedSKU1, NextInstruction: nextInstruction1}

	expectedReceiptID2 := "RE-SEAT ID 2"
	expectedSKU2 := "SKUSE U !?!?"
	nextInstruction2 := externaladg.Revoke
	expectedNextInstruction2 := nextInstruction2.String()
	model2 := externaladg.GetGoodsForUserOutputModel{ReceiptID: expectedReceiptID2, SKU: expectedSKU2, NextInstruction: nextInstruction2}

	output := externaladg.GetGoodsForUserOutput{Goods: []externaladg.GetGoodsForUserOutputModel{model1, model2}}

	response := convertOutputToResponse(output)

	if len(response.Goods) != len(output.Goods) {
		t.Error("TestConvertOutputToResponse:: Unequal amount of goods in output and response.")
	}

	verifyGood(t, "TestConvertOutputToResponse::", response.Goods[0], expectedReceiptID1, expectedSKU1, expectedNextInstruction1)
	verifyGood(t, "TestConvertOutputToResponse::", response.Goods[1], expectedReceiptID2, expectedSKU2, expectedNextInstruction2)
}

func TestConvertModelToGoods(t *testing.T) {
	expectedReceiptID := "RE-SEAT ID"
	expectedSKU := "SKUSE ME !?!?"
	nextInstruction := externaladg.Fulfill
	expectedNextInstruction := nextInstruction.String()

	model := externaladg.GetGoodsForUserOutputModel{ReceiptID: expectedReceiptID, SKU: expectedSKU, NextInstruction: nextInstruction}

	good := convertModelToGood(model)

	verifyGood(t, "TestConvertModelToGoods::", good, expectedReceiptID, expectedSKU, expectedNextInstruction)
}

func TestSetStatusForReceiptsSuccess(t *testing.T) {

	_, err := NewClient(twitchhttp.ClientConf{Host: sampleURL})
	if err != nil {
		t.Fatal(err)
	}

	reqOpts := twitchhttp.ReqOpts{
		StatName:       "service.twitch-entitlement-service.set_status_for_receipts",
		StatSampleRate: defaultStatSampleRate,
	}

	var setStatusForReceiptsRequest SetStatusForReceiptsRequest
	jsonErr := json.NewDecoder(strings.NewReader(setReceiptsRequestBody)).Decode(&setStatusForReceiptsRequest)
	if jsonErr != nil {
		t.Fatal(jsonErr)
	}
	adgResponse := externaladg.SetStatusForReceiptsOutput{ReceiptStatusUpdates: []externaladg.SetStatusForReceiptsOutputModel{externaladg.SetStatusForReceiptsOutputModel{ReceiptID: "f0ac3747-16c7-e272-ea52-c94ce27c7a8a", Result: "Success"}}}
	adgosStub := externaladg.NewADGOrderServiceStub(adgResponse, nil, false)
	resp, externalErr, _ := callSetStatusForReceipts(context.Background(), sampleAuthToken, sampleTUID, sampleClientID, setStatusForReceiptsRequest, &reqOpts, adgosStub)
	if externalErr != nil {
		t.Fatal(externalErr)
	}
	if len(resp.ReceiptUpdateResults) != 1 {
		t.Error("TestSetStatusForReceiptsSuccess:: Unexpected Response. Got len " + strconv.Itoa(len(resp.ReceiptUpdateResults)) + " but expected 1")
	}
	for _, receiptUpdateResult := range resp.ReceiptUpdateResults {
		if receiptUpdateResult.ReceiptID != "f0ac3747-16c7-e272-ea52-c94ce27c7a8a" {
			t.Error("TestSetStatusForReceiptsSuccess:: Unexpected ReceiptID. Got " + receiptUpdateResult.ReceiptID + " but expected f0ac3747-16c7-e272-ea52-c94ce27c7a8a")
		}
		if receiptUpdateResult.Result != "Success" {
			t.Error("TestSetStatusForReceiptsSuccess:: Unexpected Result. Got " + receiptUpdateResult.Result + " but expected Success")
		}
	}
}

func TestSetStatusForReceiptsSuccessMultipleReceipts(t *testing.T) {

	_, err := NewClient(twitchhttp.ClientConf{Host: sampleURL})
	if err != nil {
		t.Fatal(err)
	}

	reqOpts := twitchhttp.ReqOpts{
		StatName:       "service.twitch-entitlement-service.set_status_for_receipts",
		StatSampleRate: defaultStatSampleRate,
	}

	var setStatusForReceiptsRequest SetStatusForReceiptsRequest
	jsonErr := json.NewDecoder(strings.NewReader(setMultiReceiptsRequestBody)).Decode(&setStatusForReceiptsRequest)
	if jsonErr != nil {
		t.Fatal(jsonErr)
	}
	output1 := externaladg.SetStatusForReceiptsOutputModel{ReceiptID: "ec8ef5af-1555-00c3-c78e-1fe2979b6daa", Result: "Success", ProductID: "SampleProduct1"}
	output2 := externaladg.SetStatusForReceiptsOutputModel{ReceiptID: "f0ac3747-16c7-e272-ea52-c94ce27c7a8a", Result: "Success", ProductID: "SampleProduct2"}
	adgResponse := externaladg.SetStatusForReceiptsOutput{ReceiptStatusUpdates: []externaladg.SetStatusForReceiptsOutputModel{output1, output2}}
	adgosStub := externaladg.NewADGOrderServiceStub(adgResponse, nil, false)
	resp, externalErr, noOpReceiptCount := callSetStatusForReceipts(context.Background(), sampleAuthToken, sampleTUID, sampleClientID, setStatusForReceiptsRequest, &reqOpts, adgosStub)
	if externalErr != nil {
		t.Fatal(externalErr)
	}
	if noOpReceiptCount != 0 {
		t.Error("TestSetStatusForReceiptsSuccess:: Unexpected Response. Got len " + strconv.Itoa(noOpReceiptCount) + " but expected 0")
	}
	if len(resp.ReceiptUpdateResults) != 2 {
		t.Error("TestSetStatusForReceiptsSuccess:: Unexpected Response. Got len " + strconv.Itoa(len(resp.ReceiptUpdateResults)) + " but expected 2")
	}
	if resp.ReceiptUpdateResults[0].ReceiptID != "ec8ef5af-1555-00c3-c78e-1fe2979b6daa" {
		t.Error("TestSetStatusForReceiptsSuccess:: Unexpected ReceiptID. Got " + resp.ReceiptUpdateResults[0].ReceiptID + " but expected ec8ef5af-1555-00c3-c78e-1fe2979b6daa")
	}
	if resp.ReceiptUpdateResults[1].ReceiptID != "f0ac3747-16c7-e272-ea52-c94ce27c7a8a" {
		t.Error("TestSetStatusForReceiptsSuccess:: Unexpected ReceiptID. Got " + resp.ReceiptUpdateResults[1].ReceiptID + " but expected f0ac3747-16c7-e272-ea52-c94ce27c7a8a")
	}
	for _, receiptUpdateResult := range resp.ReceiptUpdateResults {
		if receiptUpdateResult.Result != "Success" {
			t.Error("TestSetStatusForReceiptsSuccess:: Unexpected Result. Got " + receiptUpdateResult.Result + " but expected Success")
		}
	}
}

func TestSetStatusForReceiptsSuccessMultipleReceiptsWithOneNoop(t *testing.T) {

	_, err := NewClient(twitchhttp.ClientConf{Host: sampleURL})
	if err != nil {
		t.Fatal(err)
	}

	reqOpts := twitchhttp.ReqOpts{
		StatName:       "service.twitch-entitlement-service.set_status_for_receipts",
		StatSampleRate: defaultStatSampleRate,
	}

	var setStatusForReceiptsRequest SetStatusForReceiptsRequest
	jsonErr := json.NewDecoder(strings.NewReader(setMultiReceiptsRequestBodyInvalid)).Decode(&setStatusForReceiptsRequest)
	if jsonErr != nil {
		t.Fatal(jsonErr)
	}
	output1 := externaladg.SetStatusForReceiptsOutputModel{ReceiptID: "ec8ef5af-1555-00c3-c78e-1fe2979b6daa", Result: "Success", ProductID: "SampleProduct1"}
	adgResponse := externaladg.SetStatusForReceiptsOutput{ReceiptStatusUpdates: []externaladg.SetStatusForReceiptsOutputModel{output1}}
	adgosStub := externaladg.NewADGOrderServiceStub(adgResponse, nil, false)
	resp, externalErr, noOpReceiptCount := callSetStatusForReceipts(context.Background(), sampleAuthToken, sampleTUID, sampleClientID, setStatusForReceiptsRequest, &reqOpts, adgosStub)
	if externalErr != nil {
		t.Fatal(externalErr)
	}
	if noOpReceiptCount != 1 {
		t.Error("TestSetStatusForReceiptsSuccess:: Unexpected Response. Got len " + strconv.Itoa(noOpReceiptCount) + " but expected 1")
	}
	if len(resp.ReceiptUpdateResults) != 1 {
		t.Error("TestSetStatusForReceiptsSuccess:: Unexpected Response. Got len " + strconv.Itoa(len(resp.ReceiptUpdateResults)) + " but expected 1")
	}
	if resp.ReceiptUpdateResults[0].ReceiptID != "ec8ef5af-1555-00c3-c78e-1fe2979b6daa" {
		t.Error("TestSetStatusForReceiptsSuccess:: Unexpected ReceiptID. Got " + resp.ReceiptUpdateResults[0].ReceiptID + " but expected ec8ef5af-1555-00c3-c78e-1fe2979b6daa")
	}
	for _, receiptUpdateResult := range resp.ReceiptUpdateResults {
		if receiptUpdateResult.Result != "Success" {
			t.Error("TestSetStatusForReceiptsSuccess:: Unexpected Result. Got " + receiptUpdateResult.Result + " but expected Success")
		}
	}
}

func TestSetStatusForReceiptsBadInstructionInRequest(t *testing.T) {

	_, err := NewClient(twitchhttp.ClientConf{Host: sampleURL})
	if err != nil {
		t.Fatal(err)
	}

	reqOpts := twitchhttp.ReqOpts{
		StatName:       "service.twitch-entitlement-service.set_status_for_receipts",
		StatSampleRate: defaultStatSampleRate,
	}

	var setStatusForReceiptsRequest SetStatusForReceiptsRequest
	jsonErr := json.NewDecoder(strings.NewReader(setReceiptsRequestBodyInvalid)).Decode(&setStatusForReceiptsRequest)
	if jsonErr != nil {
		t.Fatal(jsonErr)
	}
	output1 := externaladg.SetStatusForReceiptsOutputModel{ReceiptID: "ec8ef5af-1555-00c3-c78e-1fe2979b6daa", Result: "Success"}
	adgResponse := externaladg.SetStatusForReceiptsOutput{ReceiptStatusUpdates: []externaladg.SetStatusForReceiptsOutputModel{output1}}
	adgosStub := externaladg.NewADGOrderServiceStub(adgResponse, nil, false)
	_, externalErr, noOpReceiptCount := callSetStatusForReceipts(context.Background(), sampleAuthToken, sampleTUID, sampleClientID, setStatusForReceiptsRequest, &reqOpts, adgosStub)
	if externalErr == nil {
		t.Error("TestSetStatusForReceiptsBadInstructionInRequest:: Expected Error response but no error present")
	}
	if noOpReceiptCount != 0 {
		t.Error("TestSetStatusForReceiptsBadInstructionInRequest:: Expected 0 receipts to be present")
	}
	if externalErr.GetErrorCode() != http.StatusBadRequest {
		expected := strconv.Itoa(http.StatusBadRequest)
		actual := strconv.Itoa(externalErr.GetErrorCode())
		t.Error("TestSetStatusForReceiptsBadInstructionInRequest:: Expected " + expected + " Got " + actual)
	}
}
