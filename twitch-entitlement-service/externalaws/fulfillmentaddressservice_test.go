package externalaws

import (
	"encoding/json"
	"io/ioutil"
	"testing"

	"code.justin.tv/commerce/twitch-entitlement-service/external"
)

func VerifyExistsInMap(t *testing.T, m map[string]string, key string, expectedValue string, errorPrefix string) {
	if value, valuePresent := m[key]; !valuePresent {
		t.Error(errorPrefix + " Key : " + key + " is absent from the map.")
	} else {
		if value != expectedValue {
			t.Error(errorPrefix + " Key : " + key + " returned an expected value. Got : " + value + " Should be : " + expectedValue)
		}
	}
}

func TestCreateRequestFromUpdateInput(t *testing.T) {
	testTUID := "Et tu Id ?"
	testReceiptID := "Receipt ID"
	testClientID := "Id'd by the client"
	testFulfillmentAddress := "Some address"

	input := UpdateFulfillmentAddressInput{testTUID, &testReceiptID, testClientID, &testFulfillmentAddress}
	service := fulfillmentAddressService{external.DefaultCallFactory{}}

	request := service.createRequestFromUpdateInput(input)

	VerifyExistsInMap(t, request.GetHeaders(), "Content-Type", "application/json", "TestCreateRequestFromUpdateInput::")
	VerifyExistsInMap(t, request.GetHeaders(), "x-api-key", fulfillAPIKey, "TestCreateRequestFromUpdateInput::")

	if request.GetCallMethod() != "POST" {
		t.Error("TestCreateRequestFromUpdateInput::Call method should be POST")
	}

	reader, payloadErr := request.GetPayload()
	if payloadErr != nil {
		t.Error("TestCreateRequestFromUpdateInput::Error getting request payload.")
	}

	bytes, err := ioutil.ReadAll(reader)
	if err != nil {
		t.Error("TestCreateRequestFromUpdateInput::Error reading request payload.")
	}

	var payloadInput UpdateFulfillmentAddressInput
	err = json.Unmarshal(bytes, &payloadInput)
	if err != nil {
		t.Error("TestCreateRequestFromUpdateInput::Error unmarshalling payload")
	}

	if payloadInput.Tuid != testTUID {
		t.Error("TestCreateRequestFromUpdateInput::Unexpected TUID in serialized payload. Expected " + testTUID + " but got : " + payloadInput.Tuid)
	}

	if *payloadInput.ReceiptID != testReceiptID {
		t.Error("TestCreateRequestFromUpdateInput::Unexpected ProductID in serialized payload. Expected " + testReceiptID + " but got : " + *payloadInput.ReceiptID)
	}

	if payloadInput.ClientID != testClientID {
		t.Error("TestCreateRequestFromUpdateInput::Unexpected ClientID in serialized payload. Expected " + testClientID + " but got : " + payloadInput.ClientID)
	}

	if *payloadInput.FulfillmentAddress != testFulfillmentAddress {
		t.Error("TestCreateRequestFromUpdateInput::Unexpected TUID in serialized payload. Expected " + testFulfillmentAddress + " but got : " + *payloadInput.FulfillmentAddress)
	}
}
