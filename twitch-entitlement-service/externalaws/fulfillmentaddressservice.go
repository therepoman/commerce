package externalaws

import (
	"net/http"

	"code.justin.tv/commerce/twitch-entitlement-service/external"
)

const (
	fulfillAPIKey string = "73yZkLGAwa6slODacJ68a7kP1IyLq3dF9wLpYDQU"
)

// UpdateFulfillmentAddressInput is the input structure to be posted to the fulfillment address lambda.
type UpdateFulfillmentAddressInput struct {
	Tuid               string  `json:"tuid"`
	ReceiptID          *string `json:"receiptId"`
	ClientID           string  `json:"clientId"`
	FulfillmentAddress *string `json:"fulfillmentAddress"`
}

// UpdateFulfillmentAddressOutput is the output structure returned from the lambda. It solely returns if it was successful
type UpdateFulfillmentAddressOutput struct {
	Success bool
}

// FullfillmentAddressServiceInterface is our interface to the fulfillment address lambda.
type FullfillmentAddressServiceInterface interface {
	UpdateFulfillmentAddress(UpdateFulfillmentAddressInput) (UpdateFulfillmentAddressOutput, external.Error)
}

type fulfillmentAddressService struct {
	callFactory external.CallFactory
}

func (f fulfillmentAddressService) createRequestFromUpdateInput(input UpdateFulfillmentAddressInput) external.Request {
	headers := make(map[string]string)
	headers["Content-Type"] = "application/json"
	headers["x-api-key"] = fulfillAPIKey
	callMethod := "POST"
	callURL := "https://gx1pqc2ybk.execute-api.us-west-2.amazonaws.com/SetFulfillmentAddressProd/fulfillment-address" //TODO:: This should be switched on stage.
	request := external.NewSerializableRequest(callMethod, callURL, headers, input)
	return request
}

func (f fulfillmentAddressService) UpdateFulfillmentAddress(input UpdateFulfillmentAddressInput) (UpdateFulfillmentAddressOutput, external.Error) {
	request := f.createRequestFromUpdateInput(input)

	// We now have the request, get the call we should use for this request, and call it.
	call := f.callFactory.CreateCall(request)
	response, err := call.Call(request)
	if err != nil {
		return UpdateFulfillmentAddressOutput{false}, err
	}

	if response.GetStatusCode() == http.StatusOK {
		return UpdateFulfillmentAddressOutput{true}, nil
	}

	return UpdateFulfillmentAddressOutput{false}, external.NewInternalServerError("Error while updating fulfillment address.") // Update to output an error.
}

// NewFulfillAddressService returns a fullfilment service interface that
// can be used to call the fulfillment address lambda.
func NewFulfillAddressService() FullfillmentAddressServiceInterface {
	return fulfillmentAddressService{callFactory: external.DefaultCallFactory{}}
}
