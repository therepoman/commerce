package external

import (
	"encoding/json"
	"io/ioutil"
	"testing"
)

func VerifyPayload(t *testing.T, b []byte, expectedValue string, errorPrefix string) {
	var payload TestPayload
	err := json.Unmarshal(b, &payload)

	if err != nil {
		t.Error(errorPrefix + " Error during deserialization : " + err.Error())
	}

	if payload.Value != expectedValue {
		t.Error(errorPrefix + " Serialized value does not match original input value")
	}
}

func VerifyExistsInMap(t *testing.T, m map[string]string, key string, expectedValue string, errorPrefix string) {
	if value, valuePresent := m[key]; !valuePresent {
		t.Error(errorPrefix + " Key : " + key + " is absent from the map.")
	} else {
		if value != expectedValue {
			t.Error(errorPrefix + " Key : " + key + " returned an expected value. Got : " + value + " Should be : " + expectedValue)
		}
	}
}

func TestSerializableRequest(t *testing.T) {
	value := "something clever"
	payload := TestPayload{Value: value}
	headerKey := "key to the city"
	headerValue := "Narnia"

	var headers map[string]string
	headers = make(map[string]string)
	headers[headerKey] = headerValue
	request := serializableRequest{input: payload}
	request.callMethod = "METHOD IS MADNESS"
	request.callURL = "U R LOVED"
	request.headers = headers

	// First we check that the payload is serialized
	reader, err := request.GetPayload()
	if err != nil {
		t.Error("TestSerializableRequest:: Error while fetcing payload from request : " + err.Error())
	}

	var b []byte
	b, err = ioutil.ReadAll(reader)
	if err != nil {
		t.Error("TestSerializableRequest:: Error while reading serialize io.Reader : " + err.Error())
	}

	VerifyPayload(t, b, value, "TestSerializableRequest::")

	// Next we check that our headers is still in the header object
	VerifyExistsInMap(t, request.GetHeaders(), headerKey, headerValue, "TestSerializableRequest::")
}

func TestBaseRequest(t *testing.T) {
	callMethod := "TESTMETHOD"
	callURL := "www.world.wide.wed.com"

	request := BaseRequest{callMethod: callMethod, callURL: callURL}

	if request.GetCallURL() != callURL {
		t.Error("TestBaseRequest:: Unexpected callURL.")
	}

	if request.GetCallMethod() != callMethod {
		t.Error("TestBaseRequest:: Unexpected callMethod.")
	}

	if request.GetHeaders() == nil {
		t.Error("TestBaseRequest:: Header map not created.")
	}

	VerifyExistsInMap(t, request.GetHeaders(), "Content-Encoding", "amz-1.0", "TestBaseRequest::")
	VerifyExistsInMap(t, request.GetHeaders(), "User-Agent", "TwitchEntitlementService", "TestBaseRequest::")
}

func TestSerializeToReaderError(t *testing.T) {
	reader, err := serializeToReader(make(chan int))

	if reader != nil {
		t.Error("TestSerializeToReaderError:: Serialized object returned : ", reader)
	}

	if err == nil {
		t.Error("TestSerializeToReaderError:: No error returned")
	}
}

func TestSerializeToReader(t *testing.T) {
	testPayloadValue := "test"
	input := TestPayload{Value: testPayloadValue}
	reader, err := serializeToReader(input)
	if err != nil {
		t.Error("TestSerializeToReader:: Serialization failed.")
	}

	b, err := ioutil.ReadAll(reader)
	if err != nil {
		t.Error("TestSerializeToReader:: Error while reading serialize io.Reader : " + err.Error())
	}

	VerifyPayload(t, b, input.Value, "TestSerializeToReader::")
}

func TestSerializeError(t *testing.T) {
	b, err := serialize(make(chan int))

	if b != nil {
		t.Error("TestSerializeError:: Serialized object returned : " + string(b))
	}

	if err == nil {
		t.Error("TestSerializeError:: No error returned")
	}
}

func TestSerialize(t *testing.T) {
	testPayloadValue := "test"
	input := TestPayload{Value: testPayloadValue}
	b, err := serialize(input)
	if err != nil {
		t.Error("TestSerialize:: Serialization failed.")
	}

	VerifyPayload(t, b, input.Value, "TestSerialize::")
}
