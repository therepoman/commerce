package external

import (
	"io"
)

// Request is the abstract request interface.
// This represents the input just before it is to be sent to an external service
type Request interface {
	GetCallMethod() string
	GetCallURL() string
	GetPayload() (io.Reader, error)
	GetHeaders() map[string]string
}

// BaseRequest is a default implementation of the external.Request interface,
// omitting the getPaylod() call, as that will differ for different requests.
type BaseRequest struct {
	callMethod string
	callURL    string
	headers    map[string]string
}

// GetCallMethod returns the call method specified in the request.
func (r BaseRequest) GetCallMethod() string {
	return r.callMethod
}

// GetCallURL returns the URL to which the request should be made.
func (r BaseRequest) GetCallURL() string {
	return r.callURL
}

// GetHeaders returs the map of headers meant to be used with the request.
func (r BaseRequest) GetHeaders() map[string]string {
	r.generateDefaultHeaders()

	return r.headers
}

func (r *BaseRequest) generateDefaultHeaders() {
	if r.headers == nil {
		r.headers = make(map[string]string)
	}

	// These headers will be the same on every request, so set them here.
	// (This also overwrites any value that may have been previously set)
	r.headers["Content-Encoding"] = "amz-1.0"
	r.headers["User-Agent"] = "TwitchEntitlementService"
}

// SerializableRequest is a request whose payload is meant to be JSON serialized
// prior to sending. The input parameter provided during construction will be
// used as the serialized payload.
type serializableRequest struct {
	input interface{}
	BaseRequest
}

func (r serializableRequest) GetPayload() (io.Reader, error) {
	reader, err := serializeToReader(r.input)
	if err != nil {
		return nil, err
	}
	return reader, nil
}

// NewSerializableRequest creates a new SerializableRequest and returns it.
func NewSerializableRequest(m string, u string, h map[string]string, i interface{}) Request {
	request := serializableRequest{input: i}
	request.callMethod = m
	request.callURL = u
	request.headers = h
	return request
}
