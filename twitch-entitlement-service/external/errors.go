package external

import (
	"net/http"
)

// Error inteface. Meant to encapsulate errors from an external service. These
// can then be examined to determine what to return to the caller.
type Error interface {
	GetErrorCode() int
	Error() string
}

// BaseError is a default implementation of the external.Error interface.
type baseError struct {
	errorCode   int
	errorString string
}

func (e baseError) GetErrorCode() int {
	return e.errorCode
}

func (e baseError) Error() string {
	return e.errorString
}

type internalServerError struct {
	baseError
}

func (e internalServerError) GetErrorCode() int {
	return http.StatusInternalServerError
}

// NewInternalServerError creates a new instance of an Error interface representing
// a 500 error (InternalServerError).
func NewInternalServerError(s string) Error {
	ret := internalServerError{}
	ret.errorString = s

	return ret
}

// InvalidRequest.
type invalidRequestError struct {
	baseError
}

func (e invalidRequestError) GetErrorCode() int {
	return http.StatusBadRequest
}

// NewInvalidRequestError creates a new instance of an Error interface using the
// provided error string.
func NewInvalidRequestError(s string) Error {
	ret := invalidRequestError{}
	ret.errorString = s

	return ret
}

type serviceUnavailableError struct {
	baseError
}

func (e serviceUnavailableError) GetErrorCode() int {
	return http.StatusServiceUnavailable
}

// NewServiceUnavailableError creates a new instance of an Error interface representing
// a 503 error (ServiceUnavailable).
func NewServiceUnavailableError(s string) Error {
	ret := serviceUnavailableError{}
	ret.errorString = s

	return ret
}

type unauthorizedError struct {
	baseError
}

func (e unauthorizedError) GetErrorCode() int {
	return http.StatusUnauthorized
}

// NewUnauthorizedError creates a new instance of an Error interface representing
// a 401 error (Unauthorized).
func NewUnauthorizedError(s string) Error {
	ret := unauthorizedError{}
	ret.errorString = s

	return ret
}

type forbiddenError struct {
	baseError
}

func (e forbiddenError) GetErrorCode() int {
	return http.StatusForbidden
}

// NewForbiddenError creates a new instance of an Error interface representing
// a 403 error (Forbidden).
func NewForbiddenError(s string) Error {
	ret := forbiddenError{}
	ret.errorString = s

	return ret
}

type notFoundError struct {
	baseError
}

func (e notFoundError) GetErrorCode() int {
	return http.StatusNotFound
}

// NewNotFoundError creates a new instance of an Error interface representing
// a 404 error (NotFound).
func NewNotFoundError(s string) Error {
	ret := notFoundError{}
	ret.errorString = s

	return ret
}

type badRequestError struct {
	baseError
}

func (e badRequestError) GetErrorCode() int {
	return http.StatusBadRequest
}

// NewBadRequestError creates a new instance of an Error interface representing
// a 400 error (BadRequest).
func NewBadRequestError(s string) Error {
	ret := badRequestError{}
	ret.errorString = s

	return ret
}

type badGatewayError struct {
	baseError
}

func (e badGatewayError) GetErrorCode() int {
	return http.StatusBadGateway
}

// NewBadGatewayError creates a new instance of an Error interface representing
// a 502 error (BadGateway).
func NewBadGatewayError(s string) Error {
	ret := badGatewayError{}
	ret.errorString = s

	return ret
}
