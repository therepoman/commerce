package external

import (
	"io"
	"io/ioutil"
	"net/http"
)

// Call interface. Represent a call to an external service of some sort.
type Call interface {
	Call(Request) (Response, Error)
}

// EmptyCall is a basic implementation of call that doesn't know what to do
// but fail. Should only be used in CallFactories when a better, more
// appriopriate call can't be found.
type EmptyCall struct{}

// Call method for the EmptyCall.
// Serves the provided request by throwing an InternalServerError.
func (c EmptyCall) Call(r Request) (Response, Error) {
	return nil, NewInternalServerError("The service is misconfigured; it can't find the appropriate way to call internal dependencies")
}

// POSTCall is a simple implementation of an HTTP POST call.
type POSTCall struct{}

// Call  method for POSTCall. Serves the provided request using HTTP POST.
func (c POSTCall) Call(r Request) (Response, Error) {
	// Create the POST request.
	payload, err := r.GetPayload()
	if err != nil {
		return nil, NewInternalServerError("There was an error creating the payload for the internal request: " + err.Error())
	}

	req, err := http.NewRequest(r.GetCallMethod(), r.GetCallURL(), payload)
	if err != nil {
		return nil, NewInternalServerError("There was an error creating the internal request: " + err.Error())
	}

	// Set the headers for the request.
	for k, v := range r.GetHeaders() {
		req.Header.Set(k, v)
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, NewInternalServerError("There was an error making the internal request: " + err.Error())
	}

	defer closeBody(resp.Body)

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, NewInternalServerError("There was an error while reading in the response body: " + err.Error())
	}

	return baseResponse{status: resp.Status, statusCode: resp.StatusCode, body: body}, nil
}

func closeBody(body io.ReadCloser) {
	err := body.Close()
	if err != nil {
		panic("There was an error closing a request body : " + err.Error())
	}
}
