package external

import (
	"bytes"
	"encoding/json"
	"io"
)

// Helper serialization methods.
func serialize(v interface{}) ([]byte, error) {
	b, err := json.Marshal(v)
	if err != nil {
		return nil, err
	}

	return b, nil
}

func serializeToReader(v interface{}) (io.Reader, error) {
	payload, err := serialize(v)
	if err != nil {
		return nil, err
	}

	return bytes.NewReader(payload), nil
}
