package external

import (
	"io"
)

type EmptyRequest struct {
	BaseRequest
}

func (r EmptyRequest) GetPayload() (io.Reader, error) {
	return nil, nil
}

type TestPayload struct {
	Value string `json:"test"`
}
