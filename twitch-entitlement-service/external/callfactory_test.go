package external

import (
	"reflect"
	"testing"
)

func TestDefaultCallFactoryCallCreation(t *testing.T) {
	factory := DefaultCallFactory{}
	request := EmptyRequest{}
	request.callMethod = "POST"
	call := factory.CreateCall(request)
	callType := reflect.TypeOf(call)
	expectedCallType := "POSTCall"

	if callType.Name() != expectedCallType {
		t.Error("Factory create call of type " + callType.Name() + " when expected " + expectedCallType)
	}
}

func TestDefaultCallFactoryCallCreationEmptyCall(t *testing.T) {
	factory := DefaultCallFactory{}
	emptyRequest := EmptyRequest{}
	emptyRequest.callMethod = "BLA"
	emptyCall := factory.CreateCall(emptyRequest)
	emptyCallType := reflect.TypeOf(emptyCall)
	expectedEmptyCallType := "EmptyCall"

	if emptyCallType.Name() != expectedEmptyCallType {
		t.Error("Factory create call of type " + emptyCallType.Name() + " when expected " + expectedEmptyCallType)
	}
}
