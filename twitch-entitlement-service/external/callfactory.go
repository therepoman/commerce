package external

// CallFactory describes an interface that generates appriopriate Call objects
// based on passed in request objects. This can be used to customize calling
// mechanisms in different clients.
type CallFactory interface {
	CreateCall(Request) Call
}

// DefaultCallFactory is a basic implementation of the CallFactory interface.
// It simply returns the required call based on the call method specified in
// the request.
type DefaultCallFactory struct{}

// CreateCall is the defaut call factory's implmentation of CreateCall. Simply returns the
// appropriate call based on the desired call metohd.
func (cf DefaultCallFactory) CreateCall(r Request) Call {
	switch r.GetCallMethod() {
	case "POST":
		return POSTCall{}
	// Eventually could have GET method option, but we don't use GETs anywhere at
	// the moment.
	default:
		return EmptyCall{}
	}
}
