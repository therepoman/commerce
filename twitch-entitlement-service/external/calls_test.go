package external

import (
	"encoding/json"
	"net/http"
	"testing"
)

func TestEmptyCall(t *testing.T) {
	call := EmptyCall{}
	request := EmptyRequest{}

	response, err := call.Call(request)
	if response != nil {
		t.Error("TestEmptyCall::Unexpected response returned.")
	}

	if err.GetErrorCode() != http.StatusInternalServerError {
		t.Error("TestEmptyCall::Unexpected error code : ", err.GetErrorCode())
	}
}

func TestPOSTCallBadRequest(t *testing.T) {
	call := POSTCall{}
	input := TestPayload{Value: "test"}
	request := serializableRequest{input: input}
	request.callMethod = "POST"
	request.callURL = "http://THISDOESNOTEXIST"

	response, err := call.Call(request)
	if response != nil {
		t.Error("TestPOSTCallBadRequest::Unexpected response returned.")
	}

	if err.GetErrorCode() != http.StatusInternalServerError {
		t.Error("TestPOSTCallBadRequest::Unexpected error code : ", err.GetErrorCode())
	}
}

func TestPOSTCallBadPayload(t *testing.T) {
	call := POSTCall{}
	request := serializableRequest{input: make(chan int)}
	request.callMethod = "POST"
	request.callURL = "http://httpbin.org/post"

	// The payload here is bad (it shouldn't serialize). We should be
	// getting a 500 here.
	response, err := call.Call(request)
	if response != nil {
		t.Error("TestPOSTCallBadPayload::Unexpected response when given bad payload.")
	}

	if err.GetErrorCode() != http.StatusInternalServerError {
		t.Error("TestPOSTCallBadPayload::Unexpected error code : ", err.GetErrorCode())
	}
}

func TestHTTPSPOSTCall(t *testing.T) {
	call := POSTCall{}
	testPayloadValue := "test"
	input := TestPayload{Value: testPayloadValue}
	request := serializableRequest{input: input}
	request.callMethod = "POST"
	request.callURL = "https://httpbin.org/post"

	response, err := call.Call(request)
	if response == nil {
		t.Error("TestHTTPSPOSTCall::Expected a response but didn't receive one.")
	}

	if err != nil {
		t.Error("TestHTTPSPOSTCall::Unexpected error while making HTTPS POST call.")
	}
}

func TestPOSTCall(t *testing.T) {
	call := POSTCall{}
	testPayloadValue := "test"
	testHeaderName := "Test-Header"
	testHeaderValue := "test-header-value"
	input := TestPayload{Value: testPayloadValue}
	request := serializableRequest{input: input}
	request.callMethod = "POST"
	request.callURL = "http://httpbin.org/post"

	request.headers = make(map[string]string)
	request.headers[testHeaderName] = testHeaderValue

	response, err := call.Call(request)
	if err != nil {
		t.Error("TestPOSTCall::Unexpected error while making POST call.")
	}

	// Calls to the httpbin post endpoint will return the data that was sent in the "data"
	// and "json" fields in the response. It will also return the headers that were sent in
	// the "headers" field
	var data map[string]interface{}
	jsonErr := json.Unmarshal(response.GetBody(), &data)
	if jsonErr != nil {
		t.Error("TestPOSTCall::Error deserializing response.")
	}

	// First lets check that the payload was sent correctly.
	if data["json"] != nil {
		jsonData := data["json"].(map[string]interface{})
		if jsonData["test"] != testPayloadValue {
			value := "nil"
			if jsonData["test"] != nil {
				value = jsonData["test"].(string)
			}
			t.Error("TestPOSTCall::Payload sent during post call incorrect. Expected " + testPayloadValue + " but got " + value)
		}
	} else {
		t.Error("TestPOSTCall::json field does not exist in response. Was it sent during the request ?")
	}

	// Next that the headers were sent as well.
	if data["headers"] != nil {
		headers := data["headers"].(map[string]interface{})
		if headers[testHeaderName] != testHeaderValue {
			value := "nil"
			if headers[testHeaderName] != nil {
				value = headers[testHeaderName].(string)
			}
			t.Error("Header named " + testHeaderName + " sent during post call incorrect. Expected " + testHeaderValue + " but got " + value)
		}
	} else {
		t.Error("headers field does not exist in response. Were headers sent during the request ?")
	}
}
