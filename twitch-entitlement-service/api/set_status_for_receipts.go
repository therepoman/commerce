package api

import (
	"net/http"

	"encoding/json"

	"code.justin.tv/commerce/twitch-entitlement-service/client"
	"code.justin.tv/commerce/twitch-entitlement-service/external"
	"code.justin.tv/common/twitchhttp"
	"golang.org/x/net/context"
)

const (
	sampleURL2       = "dummyUrl"
	statsSampleRate2 = 0.1
	authParam2       = "Authorization"
	sampleTUID       = "dummyTUID"
	sampleClientID   = "dummyClientID"
)

// Used only within this repository to test call to entitlement.SetStatusForReceipts
func (s *Server) setStatusForReceipts(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	tesClient, err := entitlements.NewClient(twitchhttp.ClientConf{Host: sampleURL2})
	if err != nil {
		http.Error(w, "Error creating client for SetStatusForReceipts", http.StatusInternalServerError)
	}
	reqOpts := twitchhttp.ReqOpts{
		StatName:       "service.twitch-entitlement-service.set_status_for_receipts",
		StatSampleRate: statsSampleRate2,
	}
	authToken := r.Header.Get(authParam2)
	if authToken == "" {
		http.Error(w, "Bad request: Missing 'Authorization' header", http.StatusBadRequest)
		return
	}

	// Parse the input of the request body to send to our ADG Client
	var setStatusForReceiptsRequest entitlements.SetStatusForReceiptsRequest
	jsonErr := json.NewDecoder(r.Body).Decode(&setStatusForReceiptsRequest)
	if jsonErr != nil {
		externalError := external.NewInternalServerError("There was an error deserializing the request : " + jsonErr.Error())
		http.Error(w, externalError.Error(), http.StatusInternalServerError)
		return
	}

	setStatusForReceiptsResponse, err := tesClient.SetStatusForReceipts(context.Background(), authToken, sampleTUID, sampleClientID, setStatusForReceiptsRequest, false, &reqOpts)

	if err != nil {
		extErr, isExtErr := err.(external.Error)
		if isExtErr {
			http.Error(w, extErr.Error(), extErr.GetErrorCode())
			return
		} else {
			/*
				from visage the error string will never appear to the end user
				To see how this would look like when run from visage
				replace the error statement below to
				http.Error(w, nil, http.StatusInternalServerError)
			*/
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	data, jsonErr := json.Marshal(setStatusForReceiptsResponse)
	if jsonErr != nil {
		http.Error(w, "Error Marshalling setStatusForReceiptsResponse: "+jsonErr.Error(), http.StatusInternalServerError)
	}

	_, err = w.Write(data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
