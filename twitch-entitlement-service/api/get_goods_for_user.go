package api

import (
	"encoding/json"
	"net/http"

	"code.justin.tv/commerce/twitch-entitlement-service/client"
	"code.justin.tv/commerce/twitch-entitlement-service/external"
	"code.justin.tv/common/twitchhttp"
	"golang.org/x/net/context"
)

const (
	sampleURL       = "dummyUrl"
	statsSampleRate = 0.1
	authParam       = "Authorization"
)

// Used only within this repository to test call to entitlement.GetGoodsForUser
func (s *Server) getGoodsForUser(c context.Context, w http.ResponseWriter, r *http.Request) {
	tesClient, err := entitlements.NewClient(twitchhttp.ClientConf{Host: sampleURL})
	if err != nil {
		http.Error(w, "Error creating client for GetGoodsForUser", http.StatusInternalServerError)
	}

	reqOpts := twitchhttp.ReqOpts{
		StatName:       "service.twitch-entitlement-service.get_goods_for_user",
		StatSampleRate: statsSampleRate,
	}
	authToken := r.Header.Get(authParam)
	if authToken == "" {
		http.Error(w, "Bad request: Missing 'Authorization' header", http.StatusBadRequest)
		return
	}

	goodsForUser, err := tesClient.GetGoodsForUser(context.Background(), authToken, sampleClientID, false, &reqOpts)

	if err != nil {
		extErr, isExtErr := err.(external.Error)
		if isExtErr {
			http.Error(w, extErr.Error(), extErr.GetErrorCode())
			return
		} else {
			/*
				from visage the error string will never appear to the end user
				To see how this would look like when run from visage
				replace the error statement below to
				http.Error(w, nil, http.StatusInternalServerError)
			*/
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

	}

	data, jsonErr := json.Marshal(goodsForUser)
	if jsonErr != nil {
		http.Error(w, "Error Marshalling goodsForUserResponse "+jsonErr.Error(), http.StatusInternalServerError)
	}

	_, err = w.Write(data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
