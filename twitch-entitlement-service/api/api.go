package api

import (
	"code.justin.tv/common/twitchhttp"
	"goji.io"
	"goji.io/pat"
)

// Server Mux
type Server struct {
	*goji.Mux
}

// NewServer sets up new server
func NewServer() (*Server, error) {
	server := twitchhttp.NewServer()

	s := &Server{
		server,
	}

	// Add your own endpoints here. Endpoint implementation should be defined as functions on Server.
	// For example: s.HandleFuncC(pat.Get("/my/path"), s.myEndpoint)
	// https://godoc.org/goji.io

	s.HandleFuncC(pat.Post("/commerce/user/goods"), s.getGoodsForUser)
	s.HandleFuncC(pat.Post("/commerce/user/goods/fulfill"), s.setStatusForReceipts)
	return s, nil
}
