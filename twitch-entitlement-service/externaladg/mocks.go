package externaladg

import (
	"code.justin.tv/commerce/twitch-entitlement-service/external"
)

// ADGOrderServiceMock provides mocked interface to test APIs
type adgOrderServiceStub struct {
	output    SetStatusForReceiptsOutput
	extError  external.Error
	hasError  bool
	callCount int
}

func (a adgOrderServiceStub) GetCallCount() int {
	return a.callCount
}

// SetStatusForReceipts stubbed out interface
func (a adgOrderServiceStub) SetStatusForReceipts(i SetStatusForReceiptsInput) (SetStatusForReceiptsOutput, external.Error) {
	a.callCount++
	if a.hasError {
		return SetStatusForReceiptsOutput{}, a.extError
	}
	return a.output, nil
}

// NewADGOrderServiceStub returns a stubbed out ADGOrderServiceInterface
func NewADGOrderServiceStub(setStatusOuptut SetStatusForReceiptsOutput, externalError external.Error, hasErr bool) ADGOrderServiceInterface {
	return adgOrderServiceStub{output: setStatusOuptut, extError: externalError, hasError: hasErr}
}
