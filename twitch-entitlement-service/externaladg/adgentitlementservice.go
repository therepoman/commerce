package externaladg

import (
	"encoding/json"
	"strings"

	"code.justin.tv/commerce/twitch-entitlement-service/external"
)

// GetGoodsForUserOutputModel is the data model used in the GetGoodsForUser call.
// It contains the receiptID, sku, and the next step needed to be done with regards
// to fulfillment of the item.
type GetGoodsForUserOutputModel struct {
	ReceiptID       string
	NextInstruction FulfillmentInstruction
	SKU             string
}

// GetGoodsForUserOutput is the output structure returned from the GetGoodsForUser call.
type GetGoodsForUserOutput struct {
	Goods []GetGoodsForUserOutputModel
}

// GetGoodsForUserInput is the structure provided to the GetGoodsForUser call.
// The OAuthToken param represents
type GetGoodsForUserInput struct {
	OAuthToken string
}

type adgSyncPointModel struct {
	SyncToken *string `json:"syncToken"`
	SyncPoint *uint64 `json:"syncPoint"`
}

type adgSyncGoodsInputModel struct {
	Client    *adgClientModel    `json:"client"`
	SyncPoint *adgSyncPointModel `json:"startSync"`
}

// ADGEntitlementServiceInterface is our interface to ADG entitlement service.
type ADGEntitlementServiceInterface interface {
	GetGoodsForUser(GetGoodsForUserInput) (GetGoodsForUserOutput, external.Error)
}

type adgEntitlementService struct {
	callFactory external.CallFactory
}

func (a adgEntitlementService) createRequestFromSyncGoodsInput(i GetGoodsForUserInput) external.Request {
	clientID := "Fuel"
	headers := make(map[string]string)
	headers["Content-Type"] = "application/json"
	headers["X-Amz-Target"] = "com.amazon.adg.entitlement.model.ADGEntitlementService.syncGoods"
	headers["TwitchOAuth"] = i.OAuthToken
	headers["X-ADG-Oauth-Headers"] = "TwitchOAuth"
	callMethod := "POST"
	callURL := "https://adg-entitlement.amazon.com/twitch/syncGoods/" //TODO:: This should be switched on stage.
	model := adgSyncGoodsInputModel{Client: &adgClientModel{ClientID: &clientID}, SyncPoint: &adgSyncPointModel{}}
	request := external.NewSerializableRequest(callMethod, callURL, headers, model)
	return request
}

// Determines whether the deserialized response is an error, and returns the appropriate error interface.
// Returns nil if there is no error.
func (a adgEntitlementService) determineError(code int, data map[string]interface{}) external.Error {
	codeError := getErrorFromCode(code)
	if codeError != nil {
		return codeError
	}

	// Errors from ADGES sometimes have a 200 response, but will have specific __type
	// values based on the errors they can return from their model.
	responseType := getStringFromMapOrEmpty(data, "__type")
	errorSuffixes := [4]string{internalFailureSuffix, exceptionSuffix, errorSuffix, failureSuffix}
	for _, suffix := range errorSuffixes {
		if strings.HasSuffix(responseType, suffix) {
			return external.NewInternalServerError("The external service responded with a " + suffix)
		}
	}

	return nil
}

func (a adgEntitlementService) createOutputFromResponse(r external.Response) (GetGoodsForUserOutput, external.Error) {
	var data map[string]interface{}
	var goods []interface{}
	if err := json.Unmarshal(r.GetBody(), &data); err != nil {
		return GetGoodsForUserOutput{}, external.NewInternalServerError("There was an error deserializing the ADG response : " + err.Error())
	}

	serviceError := a.determineError(r.GetStatusCode(), data)
	if serviceError != nil {
		return GetGoodsForUserOutput{}, serviceError
	}

	goods = data["goods"].([]interface{})

	resultGoods := make([]GetGoodsForUserOutputModel, 0)
	for i := range goods {
		good := convertToGood(goods[i].(map[string]interface{}))
		resultGoods = append(resultGoods, GetGoodsForUserOutputModel{ReceiptID: good.receiptID, SKU: good.product.sku, NextInstruction: getFulfillmentInstructionFromState(good.transactionState)})
	}

	return GetGoodsForUserOutput{Goods: resultGoods}, nil
}

func (a adgEntitlementService) GetGoodsForUser(i GetGoodsForUserInput) (GetGoodsForUserOutput, external.Error) {
	request := a.createRequestFromSyncGoodsInput(i)

	// We now have the request, get the call we should use for this request, and call it.
	call := a.callFactory.CreateCall(request)
	response, err := call.Call(request)
	if err != nil {
		return GetGoodsForUserOutput{}, err
	}

	// Now that we have the abstract call response, we convert it into something
	// slightly more legible.
	output, err := a.createOutputFromResponse(response)
	return output, err
}

// NewADGEntitlementService returns an adg entitlement service interface that
// can be used to call ADG.
func NewADGEntitlementService() ADGEntitlementServiceInterface {
	return adgEntitlementService{callFactory: external.DefaultCallFactory{}}
}
