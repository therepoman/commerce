package externaladg

import (
	"encoding/json"
	"net/http"
	"strings"

	"code.justin.tv/commerce/twitch-entitlement-service/external"
)

// SetStatusForReceiptsOutputModel is the data model used in the SetStatusForReceipts call.
// It contains a list of the ReceiptID and the results of the attempt to set fulfillment status
// for the vendor
type SetStatusForReceiptsOutputModel struct {
	ProductID string
	ReceiptID string
	Result    string
}

// SetStatusForReceiptsOutput is the output structure returned from the SetStatusForReceipts call.
// It contains a map of receiptIDs to the resulting fulfillment status
type SetStatusForReceiptsOutput struct {
	ReceiptStatusUpdates []SetStatusForReceiptsOutputModel
}

// ReceiptUpdate is the model structure to SetStatusForReceiptsInput
type ReceiptUpdate struct {
	ReceiptID string
	Status    string
}

// SetStatusForReceiptsInput is the structure provided to the SetStatusForReceipts call.
type SetStatusForReceiptsInput struct {
	OAuthToken     string
	ReceiptUpdates []ReceiptUpdate
}

////////////////////////
// ADG Model Section ///
type adgReceiptModel struct {
	product       adgProductModel
	receiptID     string
	requestResult string
	valid         bool
}

type adgReceipt struct {
	ReceiptID *string `json:"receiptId"`
	Status    *string `json:"status"` //DELIVERED,FULFILLED,CANNOT_FULFILL
}

// This is what were calling ADG with as the input body
type adgSetReceiptsFulfillmentInputModel struct {
	Client         *adgClientModel `json:"client"`
	ReceiptUpdates []adgReceipt    `json:"receiptUpdates"`
}

// ADGOrderServiceInterface is our interface to ADG order service.
type ADGOrderServiceInterface interface {
	SetStatusForReceipts(SetStatusForReceiptsInput) (SetStatusForReceiptsOutput, external.Error)
}

type adgOrderService struct {
	callFactory external.CallFactory
}

// Parses input and creates ADG API request
func (a adgOrderService) createRequestFromInput(i SetStatusForReceiptsInput) external.Request {
	clientID := "Fuel"
	headers := make(map[string]string)
	headers["Content-Type"] = "application/json"
	headers["X-Amz-Target"] = "com.amazon.adg.order.model.ADGOrderService.setReceiptsFulfillmentStatus"
	headers["TwitchOAuth"] = i.OAuthToken
	headers["X-ADG-Oauth-Headers"] = "TwitchOAuth"
	callURL := "https://adg-order.amazon.com/twitch/setReceiptsFulfillmentStatus/"

	// convert input to expected ADG modeled Payloads
	updates := make([]adgReceipt, 0)
	for _, receiptUpdate := range i.ReceiptUpdates {
		receiptID := receiptUpdate.ReceiptID
		status := receiptUpdate.Status
		adgReceiptUpdate := adgReceipt{&receiptID, &status}
		updates = append(updates, adgReceiptUpdate)
	}
	model := adgSetReceiptsFulfillmentInputModel{Client: &adgClientModel{ClientID: &clientID}, ReceiptUpdates: updates}
	request := external.NewSerializableRequest(http.MethodPost, callURL, headers, model)
	return request
}

// Parses Output from ADG and creates response types to return to vendors
// What we care about from response:
// { "receipt": { "receiptId": "string", requestResult": "OrderingRequestResult" }
func (a adgOrderService) createOutputFromResponse(r external.Response) (SetStatusForReceiptsOutput, external.Error) {
	var data map[string]interface{}
	var receipts []interface{}
	serviceError := a.determineError(r)
	if serviceError != nil {
		return SetStatusForReceiptsOutput{}, serviceError
	}

	if err := json.Unmarshal(r.GetBody(), &data); err != nil {
		return SetStatusForReceiptsOutput{}, external.NewInternalServerError("There was an error deserializing the ADG response : " + err.Error())
	}

	receipts = getListFromMapOrEmpty(data, goodReceiptsParam)
	resultReceipts := make([]SetStatusForReceiptsOutputModel, 0)
	for i := range receipts {
		// convert to receipt by parsing out 2 strings 'result' and 'receipt'
		receipt := convertToReceipt(receipts[i].(map[string]interface{}))
		resultReceipts = append(resultReceipts, SetStatusForReceiptsOutputModel{ReceiptID: receipt.receiptID, Result: receipt.requestResult, ProductID: receipt.product.id})
	}

	return SetStatusForReceiptsOutput{ReceiptStatusUpdates: resultReceipts}, nil
}

func (a adgOrderService) determineError(r external.Response) external.Error {
	var data map[string]interface{}
	codeError := getErrorFromCode(r.GetStatusCode())
	if codeError != nil {
		return codeError
	}

	// Errors from ADGES sometimes have a 200 response, but will have specific __type
	// values based on the errors they can return from their model.
	responseType := getStringFromMapOrEmpty(data, "__type")
	errorSuffixes := [4]string{internalFailureSuffix, exceptionSuffix, errorSuffix, failureSuffix}
	badRequestSuffixes := [2]string{invalidRequestSuffix, invalidParameterSuffix}
	for _, errorSuffix := range errorSuffixes {
		if strings.HasSuffix(responseType, errorSuffix) {
			return external.NewInternalServerError("The external service responded with a " + errorSuffix)
		}
	}
	for _, badRequestSuffix := range badRequestSuffixes {
		if strings.HasSuffix(responseType, badRequestSuffix) {
			return external.NewInvalidRequestError("The external service responded with a " + badRequestSuffix)
		}
	}

	return nil
}

// SetStatusForReceipts : ADG setReceiptsFulfillmentStatus API Wrapper
func (a adgOrderService) SetStatusForReceipts(i SetStatusForReceiptsInput) (SetStatusForReceiptsOutput, external.Error) {
	request := a.createRequestFromInput(i)

	// We now have the request, get the call we should use for this request, and call it.
	call := a.callFactory.CreateCall(request)
	response, err := call.Call(request)
	if err != nil {
		return SetStatusForReceiptsOutput{}, err
	}

	// Now that we have the abstract call response, we convert it into something
	// slightly more legible.
	output, err := a.createOutputFromResponse(response)
	return output, err
}

func convertToReceipt(receipt map[string]interface{}) adgReceiptModel {
	ret := adgReceiptModel{}
	itemMap := getMapFromMapOrEmpty(receipt, receiptsItemParam)
	productMap := getMapFromMapOrEmpty(itemMap, itemProductParam)
	ret.product = convertToProduct(productMap)
	ret.receiptID = getStringFromMapOrEmpty(receipt, goodReceiptIDParam)
	ret.requestResult = getStringFromMapOrEmpty(receipt, goodRequestResultParam)
	return ret
}

// NewADGOrderService returns an adg order service interface that
// can be used to call ADG.
func NewADGOrderService() ADGOrderServiceInterface {
	return adgOrderService{callFactory: external.DefaultCallFactory{}}
}
