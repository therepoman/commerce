package externaladg

import (
	"net/http"
	"strconv"
	"testing"

	"code.justin.tv/commerce/twitch-entitlement-service/external"
)

func VerifyExistsInMap(t *testing.T, m map[string]string, key string, expectedValue string, errorPrefix string) {
	if value, valuePresent := m[key]; !valuePresent {
		t.Error(errorPrefix + " Key : " + key + " is absent from the map.")
	} else {
		if value != expectedValue {
			t.Error(errorPrefix + " Key : " + key + " returned an expected value. Got : " + value + " Should be : " + expectedValue)
		}
	}
}

func TestDetermineError(t *testing.T) {
	service := adgEntitlementService{external.DefaultCallFactory{}}
	data := make(map[string]interface{})

	errorCodes := [3]int{http.StatusMultipleChoices, http.StatusNotFound, http.StatusInternalServerError}
	// Regardless of what's in data, status codes >= 300 should return errors.
	for _, code := range errorCodes {
		if service.determineError(code, data) == nil {
			t.Error("TestDetermineError:: status code " + strconv.Itoa(code) + " should return an error.")
		}
	}

	// Now lets put some __type vars to verify that errors are also returned as appropriate.
	typeValues := [4]string{"com.amazon.coral.com#InternalFailure", "someKindOfError", "UnknownException", "some.kind.of#Failure"}
	for _, value := range typeValues {
		data["__type"] = value
		if service.determineError(200, data) == nil {
			t.Error("TestDetermineError:: type value " + value + " should return an error.")
		}
	}

	// Finally, check that a 200 with no type returns no error.
	data["__type"] = "some.legal.type.MyDao"
	if service.determineError(200, data) != nil {
		t.Error("TestDetermineError:: Happy case with __type value returned an error.")
	}

	delete(data, "__type")
	if service.determineError(200, data) != nil {
		t.Error("TestDetermineError:: Happy case with no __type value returned an error.")
	}
}

func TestCreateRequestFromSyncGoodsInput(t *testing.T) {
	testToken := "token"
	input := GetGoodsForUserInput{OAuthToken: testToken}

	service := adgEntitlementService{external.DefaultCallFactory{}}
	request := service.createRequestFromSyncGoodsInput(input)

	VerifyExistsInMap(t, request.GetHeaders(), "Content-Type", "application/json", "TestCreateRequestFromSyncGoodsInput::")
	VerifyExistsInMap(t, request.GetHeaders(), "X-Amz-Target", "com.amazon.adg.entitlement.model.ADGEntitlementService.syncGoods", "TestCreateRequestFromSyncGoodsInput::")
	VerifyExistsInMap(t, request.GetHeaders(), "TwitchOAuth", testToken, "TestCreateRequestFromSyncGoodsInput::")
	VerifyExistsInMap(t, request.GetHeaders(), "X-ADG-Oauth-Headers", "TwitchOAuth", "TestCreateRequestFromSyncGoodsInput::")

	if request.GetCallMethod() != "POST" {
		t.Error("TestCreateRequestFromSyncGoodsInput::Call method should be POST")
	}
}
