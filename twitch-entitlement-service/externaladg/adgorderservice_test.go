package externaladg

import (
	"encoding/json"
	"testing"

	"code.justin.tv/commerce/twitch-entitlement-service/external"
)

func TestCreateRequestFromInput(t *testing.T) {
	testToken := "token"
	testReceipt := ReceiptUpdate{ReceiptID: "c7cca5b9-b12c-41c7-a3c7-13872e17facd", Status: "FULFILLED"}
	receiptUpdates := make([]ReceiptUpdate, 0)
	receiptUpdates = append(receiptUpdates, testReceipt)
	input := SetStatusForReceiptsInput{OAuthToken: testToken, ReceiptUpdates: receiptUpdates}

	service := adgOrderService{external.DefaultCallFactory{}}
	request := service.createRequestFromInput(input)

	VerifyExistsInMap(t, request.GetHeaders(), "Content-Type", "application/json", "TestCreateRequestFromInput::")
	VerifyExistsInMap(t, request.GetHeaders(), "X-Amz-Target", "com.amazon.adg.order.model.ADGOrderService.setReceiptsFulfillmentStatus", "TestCreateRequestFromInput::")
	VerifyExistsInMap(t, request.GetHeaders(), "TwitchOAuth", testToken, "TestCreateRequestFromInput::")
	VerifyExistsInMap(t, request.GetHeaders(), "X-ADG-Oauth-Headers", "TwitchOAuth", "TestCreateRequestFromInput::")

	if request.GetCallMethod() != "POST" {
		t.Error("TestCreateRequestFromInput::Call method should be POST")
	}
}

func TestConvertToReceipt(t *testing.T) {
	testReceiptJSON := []byte(`{"item":{"product":{"id":"theID"}}, "receiptId": "c7cca5b9-b12c-41c7-a3c7-13872e17facd", "requestResult": "Success"}`)
	var testReceiptMap map[string]interface{}
	if err := json.Unmarshal(testReceiptJSON, &testReceiptMap); err != nil {
		t.Error("Test Setup Failure: Unable to create test setup")
	}
	receipt := convertToReceipt(testReceiptMap)
	if receipt.product.id != "theID" {
		t.Error("TestConvertToReceipt:: Unexpected product id. Expected theID but got : " + receipt.product.id)
	}

	if receipt.receiptID != "c7cca5b9-b12c-41c7-a3c7-13872e17facd" {
		t.Error("Unexpected receiptID. Got " + receipt.receiptID + " but expected c7cca5b9-b12c-41c7-a3c7-13872e17facd")
	}
	if receipt.requestResult != "Success" {
		t.Error("Unexpected requestResult. Got " + receipt.requestResult + " but expected Sucess")
	}
}
