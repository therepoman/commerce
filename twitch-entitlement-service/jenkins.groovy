freeStyleJob('commerce-twitch-entitlement-service') {
  using 'TEMPLATE-autobuild'
  scm {
    git {
      remote {
        github 'commerce/twitch-entitlement-service', 'ssh', 'git-aws.internal.justin.tv'
        credentials 'git-aws-read-key'
      }
      clean true
    }
  }
  steps {
    shell 'manta -v -f build.json'
    saveDeployArtifact 'commerce/twitch-entitlement-service', '.manta'
  }
}

freeStyleJob('commerce-twitch-entitlement-service-deploy') {
  using 'TEMPLATE-deploy-aws'
  steps {
    shell 'courier deploy --repo commerce/twitch-entitlement-service --dir /opt/twitch/twitch-entitlement-service'
  }
}
 