package errorlogger

import "net/http"

// ErrorLogger send errors to external tracking services
type ErrorLogger interface {
	Error(error)
	RequestError(*http.Request, error)
	RequestPanic(*http.Request, interface{})
}
