freeStyleJob('common-twitchhttp') {
  using 'TEMPLATE-autobuild'
  scm {
    git {
      remote {
        github 'common/twitchhttp', 'ssh', 'git-aws.internal.justin.tv'
        credentials 'git-aws-read-key'
      }
      clean true
    }
  }
  steps {
    shell 'manta -v -f build.json'
  }
}
