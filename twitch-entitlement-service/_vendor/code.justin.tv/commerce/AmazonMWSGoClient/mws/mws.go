package mws

import (
	"bytes"
	"compress/flate"
	"compress/gzip"
	"encoding/json"
	"encoding/xml"
	"errors"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/aws/aws-sdk-go/aws/credentials"
	v4Signer "github.com/aws/aws-sdk-go/aws/signer/v4"
	"github.com/sethgrid/pester"
)

const (
	httpTimeoutMs = 1500
	attemptLimit  = 3
)

// PutMetricsForAggregation sends metrics to MWS for the given region, using the supplied credentials.
// See https://w.amazon.com/index.php/Monitoring/Documentation/MonitoringWebService/PutMetricDataForAggregation for details.
// If the HTTP response was successful but the body couldn't be translated, the response will be nil.
func PutMetricsForAggregation(request *PutMetricsForAggregationRequest, region Region, creds *credentials.Credentials) (response *PutMetricsForAggregationResponse, err error) {

	// Can't do anything with a nil request.
	if request == nil {
		err = errors.New("nil request provided")
		return
	}

	// Format metrics for the request body.
	var body []byte
	body, err = serializeRequestBody(request)
	if err != nil {
		log.Fatal(err)
		return
	}

	bodyReader := bytes.NewReader(body)
	bodyWriter := ioutil.NopCloser(bodyReader)

	// Create the request.
	req, err := http.NewRequest("POST", region.getEndpoint(), bodyWriter)
	if err != nil {
		log.Fatal(err)
		return
	}

	// Add parameters by altering the query and shoving it back into the request as a raw query.
	query := req.URL.Query()
	query.Add("Version", "2007-07-07")
	query.Add("Action", "PutMetricDataForAggregation")
	req.URL.RawQuery = query.Encode()

	// Content-type must be manually set.
	req.Header.Add("content-type", "application/x-gzip")

	// Sign request.
	signer := v4Signer.NewSigner(creds)
	_, err = signer.Sign(req, bodyReader, "monitor-api", region.SigV4Region, time.Now())
	if err != nil {
		log.Fatal(err)
		return
	}

	// Make the call.
	client := getHTTPClient()
	resp, err := client.Do(req)
	defer func() { err = resp.Body.Close() }()
	if err != nil {
		log.Fatal(err)
		return
	}

	// Check for the right response code.
	if resp.StatusCode != 200 {
		err = errors.New("error response from MWS: " + resp.Status)
		log.Fatal(err)
		return
	}

	// Parse response body XML to get attempt and commit counts.
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Warn("unable to read successful response body: ", err)
		return
	}

	// Deserialize and log the MWS response.
	err = xml.Unmarshal(bodyBytes, &response)
	if err != nil {
		log.Warn("unable to unmarshal successful response body: ", err)
		return
	}

	logResponse(response)

	return
}

func getHTTPClient() (client *pester.Client) {
	client = pester.New()
	client.Concurrency = 1
	client.Backoff = pester.LinearJitterBackoff
	client.MaxRetries = attemptLimit // MaxRetries is actually the number of attempts total.
	client.KeepLog = true
	client.Timeout = time.Duration(httpTimeoutMs) * time.Millisecond

	return
}

func serializeRequestBody(req *PutMetricsForAggregationRequest) ([]byte, error) {
	marshalledBytes, err := json.Marshal(req)

	var buffer bytes.Buffer
	writer, err := gzip.NewWriterLevel(&buffer, flate.BestSpeed)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	_, err = writer.Write(marshalledBytes)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	err = writer.Close()
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	return buffer.Bytes(), nil
}

func logResponse(response *PutMetricsForAggregationResponse) {
	var infoMsgBuffer bytes.Buffer

	infoMsgBuffer.WriteString("records attempted/committed: ")
	infoMsgBuffer.WriteString(strconv.Itoa(response.Attempted))
	infoMsgBuffer.WriteString("/")
	infoMsgBuffer.WriteString(strconv.Itoa(response.Committed))

	log.Info(infoMsgBuffer.String())
}

// A Region represents a monitoring region.
type Region struct {
	SigV4Region         string // Region used for request signing.
	MonitorRegion       string // Region as reported to monitoring (usually the same as the signing region).
	ExternalEndpoint    string // Endpoint to call outside the Amazon network.
	InternalEndpoint    string // Endpoint to call inside the Amazon network.
	UseInternalEndpoint bool   // Flag to indicate which endpoint to use.
}

// Convenience method to supply the endpoint specified by the internal endpoint flag.
func (r Region) getEndpoint() string {
	if r.UseInternalEndpoint {
		return r.InternalEndpoint
	}

	return r.ExternalEndpoint
}

// Regions indexes region data by cluster name.  Cluster name is an internal Amazon convention, but it's the only non-colliding field.
// UseInternalEndpoint flag initializes to false, but this may be overridden after looking up the cluster.
var Regions = map[string]Region{
	"BJS":   Region{SigV4Region: "cn-north-1", MonitorRegion: "cn-north-1", ExternalEndpoint: "https://monitor-api-public.cn-north-1.amazonaws.com.cn", InternalEndpoint: "http://monitor-api-bjs.amazon.com", UseInternalEndpoint: false},
	"BOM":   Region{SigV4Region: "ap-south-1", MonitorRegion: "ap-south-1", ExternalEndpoint: "https://monitor-api-public.ap-south-1.amazonaws.com", InternalEndpoint: "http://monitor-api.bom.amazon.com", UseInternalEndpoint: false},
	"CMH":   Region{SigV4Region: "us-east-2", MonitorRegion: "us-east-2", ExternalEndpoint: "https://monitor-api-public.us-east-2.amazonaws.com", InternalEndpoint: "http://monitor-api.cmh.amazon.com", UseInternalEndpoint: false},
	"DCA":   Region{SigV4Region: "us-iso-east-1", MonitorRegion: "us-iso-east-1", ExternalEndpoint: "https://monitor-api-public.us-iso-east-1.c2s.ic.gov", InternalEndpoint: "http://monitor-api.dca.amazon.com", UseInternalEndpoint: false},
	"DUB":   Region{SigV4Region: "eu-west-1", MonitorRegion: "eu-west-1", ExternalEndpoint: "https://monitor-api-public-dub.amazon.com", InternalEndpoint: "http://monitor-api-dub.amazon.com", UseInternalEndpoint: false},
	"FRA":   Region{SigV4Region: "eu-central-1", MonitorRegion: "eu-central-1", ExternalEndpoint: "https://monitor-api-public.eu-central-1.amazonaws.com", InternalEndpoint: "http://monitor-api.fra.amazon.com", UseInternalEndpoint: false},
	"GRU":   Region{SigV4Region: "sa-east-1", MonitorRegion: "sa-east-1", ExternalEndpoint: "https://monitor-api-public-gru.amazon.com", InternalEndpoint: "http://monitor-api-gru.amazon.com", UseInternalEndpoint: false},
	"IAD":   Region{SigV4Region: "us-east-1", MonitorRegion: "us-east-1", ExternalEndpoint: "https://monitor-api-public-iad.amazon.com", InternalEndpoint: "http://monitor-api.amazon.com", UseInternalEndpoint: false},
	"ICN":   Region{SigV4Region: "ap-northeast-2", MonitorRegion: "ap-northeast-2", ExternalEndpoint: "https://monitor-api-public.ap-northeast-2.amazonaws.com", InternalEndpoint: "http://monitor-api.icn.amazon.com", UseInternalEndpoint: false},
	"NRT":   Region{SigV4Region: "ap-northeast-1", MonitorRegion: "ap-northeast-1", ExternalEndpoint: "https://monitor-api-public-nrt.amazon.com", InternalEndpoint: "http://monitor-api-nrt.amazon.com", UseInternalEndpoint: false},
	"LHR":   Region{SigV4Region: "eu-west-2", MonitorRegion: "eu-west-2", ExternalEndpoint: "https://monitor-api-public.eu-west-2.amazonaws.com", InternalEndpoint: "http://monitor-api.lhr.amazon.com", UseInternalEndpoint: false},
	"PDX":   Region{SigV4Region: "us-west-2", MonitorRegion: "us-west-2", ExternalEndpoint: "https://monitor-api-public-pdx.amazon.com", InternalEndpoint: "http://monitor-api-pdx.amazon.com", UseInternalEndpoint: false},
	"PEK":   Region{SigV4Region: "cn-amazon", MonitorRegion: "cn-amazon", ExternalEndpoint: "https://monitor-api-public-pek.amazon.com", InternalEndpoint: "http://monitor-api-pek.amazon.com", UseInternalEndpoint: false},
	"SEA":   Region{SigV4Region: "us-seattle", MonitorRegion: "us-seattle", ExternalEndpoint: "https://monitor-api-public-sea.amazon.com", InternalEndpoint: "http://monitor-api-sea.amazon.com", UseInternalEndpoint: false},
	"SFO":   Region{SigV4Region: "us-west-1", MonitorRegion: "us-west-1", ExternalEndpoint: "https://monitor-api-public-sfo.amazon.com", InternalEndpoint: "http://monitor-api-sfo.amazon.com", UseInternalEndpoint: false},
	"SIN":   Region{SigV4Region: "ap-southeast-1", MonitorRegion: "ap-southeast-1", ExternalEndpoint: "https://monitor-api-public-sin.amazon.com", InternalEndpoint: "http://monitor-api-sin.amazon.com", UseInternalEndpoint: false},
	"SYD":   Region{SigV4Region: "ap-southeast-2", MonitorRegion: "ap-southeast-2", ExternalEndpoint: "https://monitor-api-public-syd.amazon.com", InternalEndpoint: "http://monitor-api-syd.amazon.com", UseInternalEndpoint: false},
	"TITAN": Region{SigV4Region: "us-seattle", MonitorRegion: "us-titan", ExternalEndpoint: "https://monitor-api-public-titan.amazon.com", InternalEndpoint: "http://monitor-api-titan.amazon.com", UseInternalEndpoint: false},
	"ZHY":   Region{SigV4Region: "cn-northwest-1", MonitorRegion: "cn-northwest-1", ExternalEndpoint: "https://monitor-api-public.cn-northwest-1.amazonaws.com.cn", InternalEndpoint: "http://monitor-api.zhy.amazon.com", UseInternalEndpoint: false},
}
