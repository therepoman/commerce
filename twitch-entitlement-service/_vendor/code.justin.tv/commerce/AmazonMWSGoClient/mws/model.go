package mws

import (
	"time"
)

// Common metric string values provided for convenience and consistency.
// These are not the only values supported by MWS, but using them will produce
// metrics consistent with standard Amazon service metrics.
const (
	PeriodOneMinute  = "OneMinute"
	PeriodFiveMinute = "FiveMinute"
	PeriodOneHour    = "OneHour"

	StageTest = "Test"
	StageProd = "Prod"

	MetricTime = "Time"

	UnitMilliseconds = "Milliseconds"

	NamespaceNetwork = "Amazon/Schema/Network"
	NamespaceService = "Amazon/Schema/Service"
)

// A MetricReport is a set of Metrics pushed to MWS in a single request.
// See https://w.amazon.com/index.php/Monitoring/Documentation/MonitoringWebService/PutMetricDataForAggregation for details.
type MetricReport struct {
	Metadata  MetricMetadata `json:"Metadata"`
	Namespace string         `json:"Namespace"`
	Metrics   []Metric       `json:"Metrics"`
}

var defaultMetricReport = MetricReport{
	// We will almost always want the service schema.  Use "Amazon/Schema/Network" for network data.
	Namespace: NamespaceService,
}

// NewMetricReport creates a new MetricReport with provided metadata.
func NewMetricReport(prod bool, hostname, minPeriod, maxPeriod, partitionID, environment, customerID string) MetricReport {
	var metadata = defaultMetricMetadata

	if prod {
		metadata.Stage = StageProd
	}
	if minPeriod != "" {
		metadata.MinPeriod = minPeriod
	}
	if maxPeriod != "" {
		metadata.MaxPeriod = maxPeriod
	}
	if partitionID != "" {
		metadata.PartitionID = partitionID
	} else {
		metadata.PartitionID = hostname
	}

	metadata.Host = hostname
	metadata.Environment = environment
	metadata.CustomerID = customerID // We don't care if this is an empty string.

	var report = defaultMetricReport
	report.Metadata = metadata
	report.Metrics = make([]Metric, 0, 1)

	return report
}

// AddMetric adds a metric to a report.
func (m *MetricReport) AddMetric(metric Metric) {
	m.Metrics = append(m.Metrics, metric)
}

// A MetricMetadata holds information common to a set of metrics.  From the perspective of a single service host, all
// the values in a MetricMetadata object should be the same.
// See https://w.amazon.com/index.php/Monitoring/Documentation/MonitoringWebService/PutMetricDataForAggregation for details.
type MetricMetadata struct {
	CustomerID  string `json:"CustomerId"`
	Host        string `json:"Host"`
	PartitionID string `json:"PartitionId"`
	Environment string `json:"Environment"`
	Stage       string `json:"Stage"`
	MinPeriod   string `json:"MinPeriod"`
	MaxPeriod   string `json:"MaxPeriod"`
}

var defaultMetricMetadata = MetricMetadata{
	//CustomerId: "TUID",
	//Host: os.Hostname(),
	//PartitionId: os.Hostname(),
	//Environment: "Janus/Prod",
	Stage:     StageTest,
	MinPeriod: PeriodOneMinute,
	MaxPeriod: PeriodOneHour,
}

// A Metric represents a value being recorded, such as the latency of a service call.
// See https://w.amazon.com/index.php/Monitoring/Documentation/MonitoringWebService/PutMetricDataForAggregation for details.
type Metric struct {
	Dimensions MetricDimensions `json:"Dimensions"`
	MetricName string           `json:"MetricName"`
	Timestamp  time.Time        `json:"Timestamp"`
	Unit       string           `json:"Unit"`
	Values     []float64        `json:"Values"`
}

// NewMetric creates a new Metric with provided metadata.
func NewMetric(prod bool, hostname string, hostSpecific bool, marketplace, serviceName, methodName, client, metricName string) Metric {
	var dim = defaultMetricDimensions

	if prod {
		dim.DataSet = StageProd
	}
	if hostSpecific {
		dim.Host = hostname
		dim.HostGroup = "NONE"
	}
	if methodName != "" {
		dim.MethodName = methodName
	}
	if client != "" {
		dim.Client = client
	}

	dim.Marketplace = marketplace
	dim.ServiceName = serviceName

	var metric Metric
	metric.MetricName = metricName
	metric.Dimensions = dim
	metric.Values = make([]float64, 0, 1)

	return metric
}

// AddValue adds a value to a Metric.
func (m *Metric) AddValue(val float64) {
	// float64 value will not be nil, so no nil check.

	m.Values = append(m.Values, val)
}

// MetricDimensions contain the classification axes used to sort metrics and look them up in iGraph.
// See https://w.amazon.com/index.php/Monitoring/Documentation/MonitoringWebService/PutMetricDataForAggregation for details.
type MetricDimensions struct {
	DataSet     string `json:"DataSet"`
	Marketplace string `json:"Marketplace"`
	HostGroup   string `json:"HostGroup"`
	Host        string `json:"Host"`
	ServiceName string `json:"ServiceName"`
	MethodName  string `json:"MethodName"`
	Client      string `json:"Client"`
	MetricClass string `json:"MetricClass"`
	Instance    string `json:"Instance"`
}

var defaultMetricDimensions = MetricDimensions{
	DataSet: StageTest,
	//Marketplace: "us-west-2",
	HostGroup: "ALL",
	Host:      "ALL",
	//ServiceName: "TwitchJanusService",
	MethodName:  "ALL",
	Client:      "ALL",
	MetricClass: "NONE",
	Instance:    "NONE",
}

// PutMetricsForAggregationRequest wraps the MetricReports in a request objects to ensure the results come out right
// after serialization.
type PutMetricsForAggregationRequest struct {
	MetricReports []MetricReport `json:"MetricReports"`
}

// NewPutMetricsForAggregationRequest initializes a new request object.
func NewPutMetricsForAggregationRequest() *PutMetricsForAggregationRequest {
	var p = PutMetricsForAggregationRequest{}
	p.MetricReports = make([]MetricReport, 0, 1)
	return &p
}

// AddMetricReport adds a copy of a MetricReport to a PutMetricsForAggregationRequest object.
func (req *PutMetricsForAggregationRequest) AddMetricReport(m MetricReport) {
	req.MetricReports = append(req.MetricReports, m)
}

// PutMetricsForAggregationResponse encapsulates the response from MWS.
// Response contains the following XML fragment:
// <PutMetricDataForAggregationResponse xmlns="http://mws.amazonaws.com/doc/2007-07-07/">
//   <NumberOfAttempted>1</NumberOfAttempted><NumberOfCommitted>1</NumberOfCommitted>
// </PutMetricDataForAggregationResponse>
// This struct represents the response and contains markups for XML parsing.
type PutMetricsForAggregationResponse struct {
	Attempted int `xml:"NumberOfAttempted"`
	Committed int `xml:"NumberOfCommitted"`
}
