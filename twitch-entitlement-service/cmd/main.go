package main

import (
	"log"

	"code.justin.tv/commerce/twitch-entitlement-service/api"
	"code.justin.tv/common/config"
	"code.justin.tv/common/twitchhttp"
)

func main() {
	// To add app-specific config flags, you can call config.Register()
	// https://godoc.internal.justin.tv/code.justin.tv/common/config
	err := config.Parse()
	if err != nil {
		log.Fatal(err)
	}

	server, err := api.NewServer()
	if err != nil {
		log.Fatal(err)
	}

	log.Fatal(twitchhttp.ListenAndServe(server))
}
