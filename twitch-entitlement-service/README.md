# DEPRECATED

twitch-entitlement-service (TES) is being deprecated and is being replaced by [TESla](https://git-aws.internal.justin.tv/commerce/Tesla)

The following setup should only be performed if you are intentionally trying to edit this deprecated service.

## Setting Up the Go Service

### Setting up your Github account

 - Go to your account settings page (https://git-aws.internal.justin.tv/settings/keys) and follow the instructions there to add a new SSH key to your account.

### Setting up your workspace

 - Install go:

		brew install go

 - Create a Go workspace folder in your home directory. Ex:

		mkdir /Users/<username>/<GoWorkspace>

 - Set your environment variable for GOPATH (in your .bashrc, .zshrc, or etc.)

		export GOPATH=/Users/<username>/<GoWorkspace>

 - Append GOPATH/bin to PATH environment variable

		export PATH=$PATH:$GOPATH/bin
 - To check if Go installed correctly, run go version from commandline. This version should more than 1.6

 - Create src, code.justin.tv, and commerce directories in GoWorkspace. IE:

		/Users/<username>/GoWorkspace/src/code.justin.tv/commerce

 - Clone down TwitchEntitlementService source within the new commerce folder

		git clone https://git-aws.internal.justin.tv/commerce/twitch-entitlement-service.git

 - Directory structure will now look like this:

		/Users/<username>/GoWorkspace/src/code.justin.tv/commerce/twitch-entitlement-service

### Setup dependencies

 - Install godep

		go get github.com/tools/godep

 - Navigate to your workspace

		cd /Users/<username>/GoWorkspace/src/code.justin.tv/commerce/twitch-entitlement-service

 - Clone down all the dependencies into your GoWorkspace directory by running

		godep restore

 - You will need to be on the Twitch VPN for this step to work properly.

### Making a code change
 - Create a new git branch : git checkout -b <branch name>
 - git push origin <branch name>
 - Click compare and pull request on the git website for your package
 - Make changes and commit to the branch. Do not use commit --amend --no-edit. Push the commits
 - When you get a +1 to your pull request, do "Squash and merge your changes"
 Note: Make a separate branch for your godep changes if possible to avoid reviewers having to see massive file changes

### Building, Compiling, Testing, Running

 - In order to use the tools described in this section, you must install them as a one time setup step. Use the following commnads to install the tools:
		go get -u github.com/kisielk/errcheck
		go get -u github.com/golang/lint/golint
		go get golang.org/x/tools/cmd/goimports
 - If you see an error like below, you need to make sure to re-run the commands above:
 		find . -name "*.go" | grep -v _vendor | xargs goimports -w
 		xargs: goimports: No such file or directory
 		make: *** [fmt] Error 127

 - Take a look at the Makefile to see the targets that are available. Examples:  	
		make fmt   // runs formatting on your Go files
		make lint  // checks for mechanical errors and ill-formatted conventions
		make release // similar to brazil-build release, the target to run in preparation for a release candidate

### Useful Tools and Wikis
 - https://twitchtv.atlassian.net/wiki/display/ENG/Useful+Tools

### Pull Request and Pushing Code to Master

 - Push your changes to a remote branch using:

		git push origin new-branch

 - Use Github to create a pull request: https://git-aws.internal.justin.tv/commerce/twitch-entitlement-service/compare/new-branch
 - Ask people to look over your pull request and approve it!
 - Make sure to get proper sign off before you merge
 - This might take multiple iterations until your team is satisfied ¯\\\_(ツ)\_/¯

### Adding new dependecies
 - Go get <new dependency>
 - Edit code to import the dependency in your code
 - Run godep save ./...

### Setting up Intellij

 - Download the Go plugin for Intellij: [https://plugins.jetbrains.com/plugin/5047?pr=idea](https://plugins.jetbrains.com/plugin/5047?pr=idea)
 - Or go to **Intellij IDEA -> Preferences... -> Plugins -> Browse repositories... -> Search 'Go' -> Go Custom Languages -> Install**
 - Set up the Go SDK
   - Go to **File -> Project Structures -> SDK**
   - Click the `+` icon
   - If Go was installed with homebrew, add the Go SDK as `/usr/local/Cellar/go/<version>/libexec`
 - Open your GoWorkspace in Intellij
   - Create a new project
   - Select Go as the language
   - Choose the SDK you configured above
   - Set the project location as the Go src directory (`/Users/<username>/GoWorkspace/src` using the example above)
 - (Optional) Configure GOPATH if not launching IntelliJ from an environment with GOPATH set
   - Preferences -> Appearance and Behavior -> Path Variables

### Making a visage code change
 - Get the visage code base in your workspace: 
   - go get -d code.justin.tv/edge/visage/...
   - cd $GOPATH/src/code.justin.tv/edge/visage
 - Get latest changes in twitch-entitlement-service into visage
   - All dependencies for visage exist in __vendor folder. For our service the path will be <visage code base>/__vendor/code.justin.tv/commerce/twitch-entitlement-service
   - To get a code change to reflect into the above folder, update the commit hash for our package in the glide.yaml file. Then run make upgrade. (Note: if there are files in the new commit which were not referenced previously from the code in visage, upgrade will not work. In that scenario you will have to write the code in visage that references it and then run make upgrade )
   - Bonus hack: You can also simply edit or copy into this folder for testing only. For the actual pr make upgrade must have been run though.
 - Rebuilding Clients
   - After make upgrade is run, if there is any change in the APIs (entitlements.go) you must run make clients
 - If the visage code base needs to change, make the required changes. Our visage code which does the part of calling the APIs in entitlements.go are in api/v5/fuel
 - Testing
   - To test code locally, run:
     - make setup (one time only)
     - make dev
     - on another terminal run curl
   - To test on development, staging, and production you need to deploy and then hit the curls: https://visagedev.development.us-west2.justin.tv/kraken/<api url>; https://visage.staging.us-west2.justin.tv/kraken/<api url>; https://api.twitch.tv/kraken/<api url> respectively.
 - Deploying to visage
  - After you get a LGTM from visage devs, you can deploy using clean-deploy: clean-deploy.internal.justin.tv/#/edge/visage
  - First deploy your branch only to development. This can be done by clicking on the drop down for your branch and selecting development. Test the change in development.
  - Merge your branch into master.
  - Then deploy master to development, staging and production from clean-deploy
