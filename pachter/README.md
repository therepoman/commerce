# pachter
💸 Bits Accounting &amp; Reporting Service

[![Build Status](https://jenkins.internal.justin.tv/job/commerce/job/pachter/job/master/badge/icon)](https://jenkins.internal.justin.tv/job/commerce/job/pachter/job/master/)

## Endpoints
| Environment | Endpoint |
| --- | --- |
| Staging | https://main.us-west-2.beta.pachter.twitch.a2z.com |
| Prod | https://main.us-west-2.prod.pachter.twitch.a2z.com |

## Teleport Bastion
Pachter has two teleport bastion clusters configured with jumpbox support.

| Environment | Cluster Name | SSH Command |
| --- | --- | --- |
| Staging | twitch-pachter-devo | `TC=twitch-pachter-devo ssh jumpbox` |
| Prod | twitch-pachter-prod | `TC=twitch-pachter-prod ssh jumpbox` |

## RDS
Pachter has two RDS (postgres) endpoints.

| Environment | Endpoint | Port |
| --- | --- | -- |
| Staging | `rds-instance-pachter-staging.cxjtc5cp2ibu.us-west-2.rds.amazonaws.com` | 5432 |
| Prod | `rds-instance-pachter-production.cyp8xvcbw7cz.us-west-2.rds.amazonaws.com` | 5432 |

Usernames and passwords to the RDS endpoints can be found in Bits team's shared 1Password vault. The Teleport Bastions jumpboxes have psql installed for query executions.

Staing:
```
psql -h rds-instance-pachter-staging.cxjtc5cp2ibu.us-west-2.rds.amazonaws.com -p 5432 -U pachter_staging -d PachterStaging
```
Production:
```
psql -h rds-instance-pachter-production.cyp8xvcbw7cz.us-west-2.rds.amazonaws.com -p 5432 -U pachter_production PachterProduction
```

The jumpboxes can also be used as SSH tunnels to establish connection from a dev laptop to the RDS instance. To create a SSH tunnel using the staging jumpbox:
```
TC=twitch-pachter-devo ssh -L 5432:rds-instance-pachter-staging.cxjtc5cp2ibu.us-west-2.rds.amazonaws.com:5432 jumpbox
```
Once the proxy is up and running, start you local SQL client and simply connect to `localhost:5432` with the appropriate username and password.

## Terraform 
### Planning 
Staging: Run `make plan env=staging`

Production: Run `make plan env=production`
### Applying 
Staging: Run `make apply env=staging`

Production: Run `make apply env=production`
