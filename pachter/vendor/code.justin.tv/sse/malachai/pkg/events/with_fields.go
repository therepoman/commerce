package events

// WithFieldsEventWriter wraps an EventWriter with a default set of fields for Event
type WithFieldsEventWriter struct {
	EventWriter   EventWriter
	EventDefaults DefaultableEvent
}

// WriteEvent implements EventWriter
func (ew *WithFieldsEventWriter) WriteEvent(event interface{}) {
	ew.EventDefaults.FillDefaults(event)
	ew.EventWriter.WriteEvent(event)
}

// WithFields wraps this in a WithFieldsEventWriter
func (ew *WithFieldsEventWriter) WithFields(defaults DefaultableEvent) EventWriter {
	return &WithFieldsEventWriter{
		EventWriter:   ew,
		EventDefaults: defaults,
	}
}
