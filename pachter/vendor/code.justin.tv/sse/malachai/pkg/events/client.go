package events

import (
	"context"
	"encoding/json"

	"code.justin.tv/sse/malachai/pkg/internal/closer"
	"code.justin.tv/sse/malachai/pkg/internal/stats/statsiface"
	"code.justin.tv/sse/malachai/pkg/log"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
)

// Logger describes an event logger
type Logger interface {
	Run()
	WriteEvent(interface{})
	WithFields(DefaultableEvent) EventWriter
}

// BatchShipper ships logs to firehose
type BatchShipper interface {
	Add(context.Context, []byte) error
	Run()
	Init(*session.Session, statsiface.ReporterAPI) error
}

// Client writes events directly to firehose
type Client struct {
	BatchShipper   BatchShipper
	Closer         *closer.Closer
	Logger         log.S2SLogger
	cfg            Config
	serviceRoleArn string
}

// Run starts the background updater
func (c *Client) Run() {
	if c.Closer == nil {
		c.Closer = closer.New()
	}
	c.BatchShipper.Run()
}

// Close closes the client
func (c *Client) Close() error {
	return c.Closer.Close()
}

// WriteEvent writes a list of events to file
func (c *Client) WriteEvent(event interface{}) {
	var (
		raw []byte
		err error
	)

	if raw, err = json.Marshal(event); err != nil {
		c.Logger.Debugf("error marshaling event for logs: %s", err.Error())
	}

	if err = c.BatchShipper.Add(context.Background(), append(raw, []byte("\n")...)); err != nil {
		c.Logger.Debugf("error writing event to logs: %s", err.Error())
	}
}

// Init the client with private session.
// should only be called in library.
func (c *Client) Init(sess *session.Session, statter statsiface.ReporterAPI) error {
	if err := c.cfg.fillDefaults(); err != nil {
		return err
	}

	sess, err := session.NewSession(&aws.Config{
		Credentials:         stscreds.NewCredentials(sess, c.cfg.resources.FirehoseLoggerRoleArn),
		Region:              aws.String(c.cfg.resources.Region),
		STSRegionalEndpoint: endpoints.RegionalSTSEndpoint,
	})
	if err != nil {
		return err
	}
	return c.BatchShipper.Init(sess, statter)
}

// WithFields wraps this client in a client that provides defaults
func (c *Client) WithFields(defaults DefaultableEvent) EventWriter {
	return &WithFieldsEventWriter{
		EventWriter:   c,
		EventDefaults: defaults,
	}
}
