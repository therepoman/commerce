package capabilities

import (
	"code.justin.tv/sse/malachai/pkg/config"
	"github.com/aws/aws-sdk-go/aws"
)

// Config is a struct for configuring the inventory logger
type Config struct {
	// AWS config structure. If left nil, will be replaced by a
	// default config with region us-west-2.
	AWSRegion string
	// testing or production
	Environment string
	// DynamoDB Table name (will have default value: DefaultInventory)
	TableName string

	// optional override for i.e. isengard credentials
	AWSConfigBase *aws.Config

	RoleArn string
}

// FillDefaults fills defaults
func (cfg *Config) FillDefaults() (err error) {
	resources, err := config.GetResources(cfg.Environment)
	if err != nil {
		return
	}

	if cfg.AWSRegion == "" {
		cfg.AWSRegion = resources.Region
	}
	if cfg.TableName == "" {
		cfg.TableName = resources.CapabilitiesTable
	}
	return
}
