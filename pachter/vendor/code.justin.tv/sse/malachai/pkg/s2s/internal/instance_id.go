package internal

import "github.com/gofrs/uuid"

// NewInstanceID returns a new instance id
func NewInstanceID() (string, error) {
	id, err := uuid.NewV4()
	if err != nil {
		return "", err
	}
	return id.String(), nil
}
