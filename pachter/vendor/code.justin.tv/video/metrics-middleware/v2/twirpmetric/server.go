package twirpmetric

import (
	"context"
	"path"

	"code.justin.tv/video/metrics-middleware/v2/operation"
	"code.justin.tv/video/metrics-middleware/v2/status"
	"github.com/twitchtv/twirp"
)

type Server struct {
	Starter *operation.Starter
}

func (s *Server) ServerHooks() *twirp.ServerHooks {
	opKey := new(int)

	return &twirp.ServerHooks{
		RequestRouted: func(ctx context.Context) (context.Context, error) {
			op := getOp(ctx)
			op.Kind = operation.KindServer

			ctx, span := s.Starter.StartOp(ctx, op)
			ctx = context.WithValue(ctx, opKey, span)
			return ctx, nil
		},
		Error: func(ctx context.Context, err twirp.Error) context.Context {
			if span, _ := ctx.Value(opKey).(*operation.Op); span != nil {
				span.SetStatus(statusFromTwirp(err.Code()))
			}
			return ctx
		},
		ResponseSent: func(ctx context.Context) {
			if span, _ := ctx.Value(opKey).(*operation.Op); span != nil {
				span.End()
			}
		},
	}
}

func getOp(ctx context.Context) operation.Name {
	var name operation.Name

	name.Method, _ = twirp.MethodName(ctx)
	name.Group, _ = twirp.ServiceName(ctx)
	if pkg, ok := twirp.PackageName(ctx); ok {
		name.Group = path.Join(pkg, name.Group)
	}

	return name
}

func statusFromTwirp(code twirp.ErrorCode) operation.Status {
	if s, ok := status.ByName(string(code)); ok {
		return s
	}
	return status.Unknown()
}
