// Package status contains functions for mapping Twirp and gRPC error codes to metrics-middleware Statuses.
package status

import "code.justin.tv/video/metrics-middleware/v2/operation"

// NB: The ordering of items in this slice is important. For the cases where Twirp-added
// Statuses share a gRPC error code, the first matching Status is returned when performing
// a look-up by error code.
var statuses = []operation.Status{
	OK(),
	Canceled(),
	Unknown(),
	InvalidArgument(),
	Malformed(),
	DeadlineExceeded(),
	NotFound(),
	AlreadyExists(),
	PermissionDenied(),
	ResourceExhausted(),
	FailedPrecondition(),
	Aborted(),
	OutOfRange(),
	Unimplemented(),
	BadRoute(),
	Internal(),
	Unavailable(),
	DataLoss(),
	Unauthenticated(),
}

// ByName returns the Status corresponding to the provided Twirp error name.
// If a matching status is found, it is returned and the boolean is true.
// Otherwise an Unknown status is returned and the boolean will be false.
//
// Alternative spellings are not considered matches, but this functionality
// may be provided in the future.
func ByName(name string) (operation.Status, bool) {
	for _, item := range statuses {
		if item.Message == name {
			return item, true
		}
	}
	return Unknown(), false
}

// ByCode returns the Status corresponding to the provided gRPC error code.
// If a matching Status is found, it is returned and the boolean is true.
// Otherwise the Unknown Status is returned and the boolean will be false.
//
// If multiple Statuses map to an error code, the first matching Status
// (according to the internal ordering of the Statuses in this package)
// is returned.
func ByCode(code int32) (operation.Status, bool) {
	for _, item := range statuses {
		if item.Code == code {
			return item, true
		}
	}
	return Unknown(), false
}

// KnownValues returns a slice of all mapped statuses.
func KnownValues() []operation.Status {
	values := make([]operation.Status, len(statuses))
	for i := range statuses {
		values[i] = statuses[i]
	}
	return values

}

// OK returns an empty Status (gRPC response code 0).
//
// OK is returned on success.
func OK() operation.Status {
	return operation.Status{}
}

// Canceled returns a Status corresponding to gRPC response code 1.
//
// Canceled indicates the operation was canceled (typically by the caller).
func Canceled() operation.Status {
	return operation.Status{Message: "canceled", Code: 1}
}

// Unknown returns a Status corresponding to gRPC response code 2.
//
// Unknown error. An example of where this error may be returned is
// if a Status value received from another address space belongs to
// an error-space that is not known in this address space. Also
// errors raised by APIs that do not return enough error information
// may be converted to this error.
func Unknown() operation.Status {
	return operation.Status{Message: "unknown", Code: 2}
}

// InvalidArgument returns a Status corresponding to gRPC response code 3.
//
// InvalidArgument indicates client specified an invalid argument.
// Note that this differs from FailedPrecondition. It indicates arguments
// that are problematic regardless of the state of the system
// (e.g., a malformed file name).
func InvalidArgument() operation.Status {
	return operation.Status{Message: "invalid_argument", Code: 3}
}

// Malformed returns the Twirp 'malformed' Status, which maps to gRPC response code 3.
//
// This error should both be treated as a client error, see
// TwitchTelemetryMetricsMiddleware: https://tiny.amazon.com/65ofc8lj.
//
// Malformed means the client sent a message which could not be decoded.
// This may mean that the message was encoded improperly or that the
// client and server have incompatible message definitions.
func Malformed() operation.Status {
	return operation.Status{Message: "malformed", Code: 3}
}

// DeadlineExceeded returns a Status corresponding to gRPC response code 4.
//
// DeadlineExceeded means operation expired before completion.
// For operations that change the state of the system, this error may be
// returned even if the operation has completed successfully. For
// example, a successful response from a server could have been delayed
// long enough for the deadline to expire.
func DeadlineExceeded() operation.Status {
	return operation.Status{Message: "deadline_exceeded", Code: 4}
}

// NotFound returns a Status corresponding to gRPC response code 5.
//
// NotFound means some requested entity (e.g., file or directory) was
// not found.
func NotFound() operation.Status {
	return operation.Status{Message: "not_found", Code: 5}
}

// AlreadyExists returns a Status corresponding to gRPC response code 6.
//
// AlreadyExists means an attempt to create an entity failed because one
// already exists.
func AlreadyExists() operation.Status {
	return operation.Status{Message: "already_exists", Code: 6}
}

// PermissionDenied returns a Status corresponding to gRPC response code 7.
//
// PermissionDenied indicates the caller does not have permission to
// execute the specified operation. It must not be used for rejections
// caused by exhausting some resource (use ResourceExhausted
// instead for those errors). It must not be
// used if the caller cannot be identified (use Unauthenticated
// instead for those errors).
func PermissionDenied() operation.Status {
	return operation.Status{Message: "permission_denied", Code: 7}
}

// ResourceExhausted returns a Status corresponding to gRPC response code 8.
//
// ResourceExhausted indicates some resource has been exhausted, perhaps
// a per-user quota, or perhaps the entire file system is out of space.
func ResourceExhausted() operation.Status {
	return operation.Status{Message: "resource_exhausted", Code: 8}
}

// FailedPrecondition returns a Status corresponding to gRPC response code 9.
//
// FailedPrecondition indicates operation was rejected because the
// system is not in a state required for the operation's execution.
// For example, directory to be deleted may be non-empty, an rmdir
// operation is applied to a non-directory, etc.
//
// A litmus test that may help a service implementor in deciding
// between FailedPrecondition, Aborted, and Unavailable:
//  (a) Use Unavailable if the client can retry just the failing call.
//  (b) Use Aborted if the client should retry at a higher-level
//      (e.g., restarting a read-modify-write sequence).
//  (c) Use FailedPrecondition if the client should not retry until
//      the system state has been explicitly fixed. E.g., if an "rmdir"
//      fails because the directory is non-empty, FailedPrecondition
//      should be returned since the client should not retry unless
//      they have first fixed up the directory by deleting files from it.
//  (d) Use FailedPrecondition if the client performs conditional
//      REST Get/Update/Delete on a resource and the resource on the
//      server does not match the condition. E.g., conflicting
//      read-modify-write on the same resource.
func FailedPrecondition() operation.Status {
	return operation.Status{Message: "failed_precondition", Code: 9}
}

// Aborted returns a Status corresponding to gRPC response code 10.
//
// Aborted indicates the operation was aborted, typically due to a
// concurrency issue like sequencer check failures, transaction aborts,
// etc.
//
// See litmus test above for deciding between FailedPrecondition,
// Aborted, and Unavailable.
func Aborted() operation.Status {
	return operation.Status{Message: "aborted", Code: 10}
}

// OutOfRange returns a Status corresponding to gRPC response code 11.
//
// OutOfRange means operation was attempted past the valid range.
// E.g., seeking or reading past end of file.
//
// Unlike InvalidArgument, this error indicates a problem that may
// be fixed if the system state changes. For example, a 32-bit file
// system will generate InvalidArgument if asked to read at an
// offset that is not in the range [0,2^32-1], but it will generate
// OutOfRange if asked to read from an offset past the current
// file size.
//
// There is a fair bit of overlap between FailedPrecondition and
// OutOfRange. We recommend using OutOfRange (the more specific
// error) when it applies so that callers who are iterating through
// a space can easily look for an OutOfRange error to detect when
// they are done.
func OutOfRange() operation.Status {
	return operation.Status{Message: "out_of_range", Code: 11}
}

// Unimplemented returns a Status corresponding to gRPC response code 12.
//
// Unimplemented indicates operation is not implemented or not
// supported/enabled in this service.
func Unimplemented() operation.Status {
	return operation.Status{Message: "unimplemented", Code: 12}
}

// BadRoute returns the Twirp 'bad_route' Status, which maps to gRPC response code 12.
//
// This error should both be treated as a client error, see
// TwitchTelemetryMetricsMiddleware: https://tiny.amazon.com/65ofc8lj.
//
// BadRoute means the requested URL path wasn't routable to a Twirp
// service and method. This is returned by generated server code and
// should not be returned by application code (use "not_found" or
// "unimplemented" instead).
func BadRoute() operation.Status {
	return operation.Status{Message: "bad_route", Code: 12}
}

// Internal returns a Status corresponding to gRPC response code 13.
//
// Internal errors. Means some invariants expected by underlying
// system has been broken. If you see one of these errors,
// something is very broken.
func Internal() operation.Status {
	return operation.Status{Message: "internal", Code: 13}
}

// Unavailable returns a Status corresponding to gRPC response code 14.
//
// Unavailable indicates the service is currently unavailable.
// This is a most likely a transient condition and may be corrected
// by retrying with a backoff. Note that it is not always safe to retry
// non-idempotent operations.
//
// See litmus test above for deciding between FailedPrecondition,
// Aborted, and Unavailable.
func Unavailable() operation.Status {
	return operation.Status{Message: "unavailable", Code: 14}
}

// DataLoss returns a Status corresponding to gRPC response code 15.
//
// DataLoss indicates unrecoverable data loss or corruption.
func DataLoss() operation.Status {
	return operation.Status{Message: "data_loss", Code: 15}
}

// Unauthenticated returns a Status corresponding to gRPC response code 16.
//
// Unauthenticated indicates the request does not have valid
// authentication credentials for the operation.
func Unauthenticated() operation.Status {
	return operation.Status{Message: "unauthenticated", Code: 16}
}
