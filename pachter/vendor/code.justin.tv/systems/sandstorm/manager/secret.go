package manager

import (
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"path"
	"strconv"
	"strings"
	"time"

	"code.justin.tv/systems/sandstorm/pkg/envelope"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

// SecretClass Type for secret Category
type SecretClass int8

// Secret classes
const (
	ClassPublic     SecretClass = 1
	ClassInternal   SecretClass = 2
	ClassRestricted SecretClass = 3
	ClassCustomer   SecretClass = 4
	ClassDefault    SecretClass = ClassRestricted

	DepreciatedClassUnknown   SecretClass = 0
	DepreciatedClassTopSecret SecretClass = 5
)

var errInvalidSecretName = errors.New("Invalid Secret Name: Secrets cannot include '$'")

// ParseSecretClass parses a secret class from a string
func ParseSecretClass(strValue string) (value SecretClass, err error) {
	switch strValue {
	case "Public":
		value = ClassPublic
	case "Internal":
		value = ClassInternal
	case "Restricted":
		value = ClassRestricted
	case "Customer":
		value = ClassCustomer
	case "":
		value = ClassDefault
	case "Default":
		value = ClassDefault
	default:
		err = fmt.Errorf("invalid secret class: '%s'", strValue)
	}
	return
}

const (
	// PlaintextLengthMin is the minimum length that a plaintext secret can be
	// when generated using FillPlaintext
	PlaintextLengthMin = 2
	// PlaintextLengthMax is the maxmimum length that a plaintext secret can be
	// when generated using FillPlaintext
	PlaintextLengthMax = 4096
)

const jsonClassField = "class"

// Secret represents a secret. It can be either in the plain or
// enciphered.
type Secret struct {
	// Name of secret
	Name string `json:"name"`
	// Plaintext to be enciphered or read by user
	Plaintext []byte `json:"plaintext,omitempty"`
	// Unix timestamp of when the key was created/updated
	UpdatedAt int64 `json:"updated_at"`
	// Ciphertext to be deciphered or uploaded
	DoNotBroadcast bool `json:"do_not_broadcast"`
	// Tombstone signifies that a secret has been deleted
	Tombstone bool `json:"tombstone"`
	// CrossEnv signifies that a secret can be used in any environment
	CrossEnv bool `json:"cross_env"`
	// KeyARN is the key used to encrypt
	KeyARN string `json:"key_arn"`

	// Class of the document. update const jsonClassField if you change the json field name.
	Class SecretClass `json:"class"`

	ciphertext []byte
	// Enciphered key returned by kms.GenerateDataKey
	key []byte

	//Actionuser temporarily store action user.
	ActionUser string

	//selectedAt specifies when a secret was selected as current secret.
	selectedAt int64
}

type regionPriority map[string]int

func (r regionPriority) getRank(region string) int {
	rank, ok := r[region]
	if !ok {
		return -1
	}
	return rank
}

func (s *Secret) asDynamoSecret() *dynamoSecret {
	if s.Class < 1 || 4 < s.Class {
		s.Class = ClassDefault
	}

	return &dynamoSecret{
		Tombstone:      s.Tombstone,
		Name:           s.Name,
		Namespace:      SecretNameToNamespace(s.Name),
		UpdatedAt:      s.UpdatedAt,
		Key:            base64.StdEncoding.EncodeToString(s.key),
		Value:          base64.StdEncoding.EncodeToString(s.ciphertext),
		KeyARN:         s.KeyARN,
		DoNotBroadcast: s.DoNotBroadcast,
		Class:          int(s.Class),
		CrossEnv:       s.CrossEnv,
		SelectedAt:     s.selectedAt,
	}
}

type dynamoSecret struct {
	Tombstone      bool   `dynamodbav:"tombstone"`
	Name           string `dynamodbav:"name"`
	Namespace      string `dynamodbav:"namespace"`
	UpdatedAt      int64  `dynamodbav:"updated_at"`
	Value          string `dynamodbav:"value"`
	Key            string `dynamodbav:"key"`
	KeyARN         string `dynamodbav:"key_arn"`
	DoNotBroadcast bool   `dynamodbav:"do_not_broadcast"`
	Class          int    `dynamodbav:"class"`
	CrossEnv       bool   `dynamodbav:"cross_env"`
	SelectedAt     int64  `dynamodbav:"selected_at"`
}

func (ds *dynamoSecret) validate() error {
	if ds.Name == "" {
		return errors.New("name column value required")
	}
	if ds.UpdatedAt == 0 {
		return errors.New("updated_at column value required")
	}
	if ds.Value == "" {
		return errors.New("value column value required")
	}
	if ds.Key == "" {
		return errors.New("key column value required")
	}
	return nil
}

type dynamoSecretAudit struct {
	dynamoSecret
	ActionUser      string `dynamodbav:"action_user"`
	ServiceName     string `dynamodbav:"service_name"`
	PreviousVersion int64  `dynamodbav:"previous_version"`
}

func unmarshalSecret(item map[string]*dynamodb.AttributeValue) (secret *Secret, err error) {
	var s dynamoSecret
	err = dynamodbattribute.UnmarshalMap(item, &s)
	if err != nil {
		return
	}

	if s.Tombstone {
		err = ErrSecretTombstoned
		return
	}

	err = s.validate()
	if err != nil {
		err = fmt.Errorf("error validating secret from dynamodb: %s", err.Error())
		return
	}

	secret = &Secret{
		Name:           s.Name,
		UpdatedAt:      s.UpdatedAt,
		DoNotBroadcast: s.DoNotBroadcast,
		KeyARN:         s.KeyARN,
		Tombstone:      s.Tombstone,
		Class:          SecretClass(s.Class),
		CrossEnv:       s.CrossEnv,
		selectedAt:     s.SelectedAt,
	}

	// prior to V1, secrets had levels [1, 5]. need to map to new values when
	// unmarshalling.
	switch secret.Class {
	case DepreciatedClassUnknown:
		fallthrough
	case DepreciatedClassTopSecret:
		secret.Class = ClassDefault
	}

	ciphertext, err := base64.StdEncoding.DecodeString(s.Value)
	if err != nil {
		err = fmt.Errorf("error base64 decoding ciphertext for secret %s: %s", secret.Name, err.Error())
		return
	}
	secret.ciphertext = ciphertext

	key, err := base64.StdEncoding.DecodeString(s.Key)
	if err != nil {
		err = fmt.Errorf("error base64 decoding key for secret %s: %s", secret.Name, err.Error())
		return
	}
	secret.key = key

	return
}

// FillPlaintextRequest used for FillPlaintext
type FillPlaintextRequest struct {
	Length    int `json:"length"`
	validated bool
}

// Validate Length
func (r FillPlaintextRequest) Validate() error {
	if r.validated {
		return nil
	}
	if r.Length < PlaintextLengthMin {
		return errors.New("plaintext length is less than minumum length")
	}
	if r.Length > PlaintextLengthMax {
		return errors.New("plaintext length is greater than maximum length")
	}
	r.validated = true
	return nil
}

// ValidateSecretName checks the secret name for any illegal characters
func validateSecretName(name string) bool {
	return !strings.ContainsAny(name, "$")
}

// FillPlaintext fills the Plaintext field with a random byte combination
// that is base64 decodable (i.e. guaranteed printable). Note that this wil be
// double encoded in Sandstorm -- this is intentional!
func (s *Secret) FillPlaintext(r *FillPlaintextRequest) error {
	if err := r.Validate(); err != nil {
		return err
	}
	plaintextRaw := make([]byte, r.Length)
	_, err := rand.Read(plaintextRaw)
	if err != nil {
		return err
	}

	// base64 decodable since we want to be printable.
	s.Plaintext = make([]byte, base64.StdEncoding.EncodedLen(r.Length))
	base64.StdEncoding.Encode(s.Plaintext, plaintextRaw)

	return nil
}

// Timestamp updates the UpdatedAt field to current unix timestamp
func (s *Secret) Timestamp() {
	s.UpdatedAt = time.Now().Unix()
}

// Seal takes a secret, timestamps it, creates an encryption context
// and passes to envelope.Seal
func (m *Manager) Seal(secret *Secret) error {

	return m.statter.WithMeasuredResult("secret.Seal", []*cloudwatch.Dimension{{
		Name:  aws.String("SecretName"),
		Value: aws.String(secret.Name),
	}}, func() error {

		secret.Timestamp()

		encryptedDataKey, ciphertext, err := m.enveloper.Seal(secret.EncryptionContext(), secret.Plaintext)
		if err != nil {
			return err
		}
		// XXX perform base64 here?
		secret.key = encryptedDataKey.Value
		secret.ciphertext = ciphertext
		Zero(secret.Plaintext)
		secret.Plaintext = nil
		secret.KeyARN = encryptedDataKey.KeyARN
		return nil
	})
}

// Decrypt the Ciphertext field and store it in Plaintext
func (m *Manager) Decrypt(secret *Secret) error {
	if secret == nil {
		return nil
	}
	return m.statter.WithMeasuredResult("secret.Decrypt", []*cloudwatch.Dimension{{
		Name:  aws.String("SecretName"),
		Value: aws.String(secret.Name),
	}}, func() (err error) {

		secret.Plaintext, err = m.enveloper.Open(secret.getEncryptedDataKey(), secret.ciphertext, secret.EncryptionContext())
		return
	})
}

// EncryptionContext generates the encryption context map to pass to
// kms.GenerateDataKey / kms.Decrypt.
func (s *Secret) EncryptionContext() (context map[string]string) {
	context = map[string]string{
		"name":       s.Name,
		"updated_at": strconv.FormatInt(s.UpdatedAt, 10),
	}
	return
}

// SecretNamespace contains information about the different parts of the name of a secret.
type SecretNamespace struct {
	Team        string `json:"team"`
	System      string `json:"system"`
	Environment string `json:"environment"`
	Name        string `json:"name"`
}

func (s SecretNamespace) String() string { return path.Join(s.Team, s.System, s.Environment, s.Name) }

// ParseSecretNamespace extracts namespace information from the name of a secret.
func ParseSecretNamespace(secretName string) (*SecretNamespace, error) {
	splitted := strings.SplitN(secretName, "/", 4)
	if len(splitted) != 4 {
		return nil, InvalidRequestError{Message: fmt.Errorf("sandstorm: expected 4 namespace elements, got %d", len(splitted))}
	}
	sn := &SecretNamespace{
		Team:        splitted[0],
		System:      splitted[1],
		Environment: splitted[2],
		Name:        splitted[3],
	}
	return sn, nil
}

func (s *Secret) getEncryptedDataKey() envelope.EncryptedDataKey {
	return envelope.EncryptedDataKey{
		KeyARN: s.KeyARN,
		Value:  s.key,
		Region: awsRegionUsWest2,
	}
}
