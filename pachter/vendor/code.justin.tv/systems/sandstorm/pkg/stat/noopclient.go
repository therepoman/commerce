package stat

import (
	"github.com/aws/aws-sdk-go/service/cloudwatch"
)

// NoopClient implements statiface.API with no operation
type NoopClient struct {
}

// Increment implements statiface.API
func (c NoopClient) Increment(Metric) {
}

// Measure implements statiface.API
func (c NoopClient) Measure(Metric, float64) {
}

// WithMeasuredResult implements statiface.API
func (c NoopClient) WithMeasuredResult(
	metricName string,
	dimensions []*cloudwatch.Dimension,
	fn func() error,
) error {
	return fn()
}
