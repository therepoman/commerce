FROM docker.internal.justin.tv/devtools/bionic/go1.12.0:latest as builder

ADD . /build/go/src/code.justin.tv/commerce/pachter
WORKDIR /build/go/src/code.justin.tv/commerce/pachter

RUN GIT_COMMIT=$(git rev-list -1 HEAD) && \
    go build -ldflags "-X main.Version=$GIT_COMMIT" -o /pachter ./main.go

# Runner Container
FROM docker.internal.justin.tv/devtools/bionic/go1.12.0:latest as runner

ENV appDir /opt/twitch/pachter/current
RUN mkdir -p $appDir

COPY --from=builder /pachter $appDir
COPY --from=builder /build/go/src/code.justin.tv/commerce/pachter/config/data /etc/pachter/config

RUN groupadd -r web && useradd --no-log-init -r -g web web
RUN chown -R web:web $appDir

USER web
EXPOSE 8080
ENTRYPOINT ["/opt/twitch/pachter/current/pachter"]