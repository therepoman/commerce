package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"sort"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachter/backend/clients/rds"
	r "code.justin.tv/commerce/pachter/backend/clients/redis"
	"code.justin.tv/commerce/pachter/backend/dynamo/worker_audit"
	"code.justin.tv/commerce/pachter/backend/rds/bits_spend"
	"code.justin.tv/commerce/pachter/backend/rds/bits_transaction"
	rds_models "code.justin.tv/commerce/pachter/backend/rds/models"
	"code.justin.tv/commerce/pachter/backend/worker"
	"code.justin.tv/commerce/pachter/config"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/gofrs/uuid"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"golang.org/x/time/rate"
)

const (
	environment    = "LAMBDA_ENVIRONMENT" // "staging" or "prod"
	prod           = "prod"
	redisKeyFormat = "user-bits-transactions-%s"
	batchSize      = 10

	redisEndpointProd    = "pachter-redis.sfonzr.clustercfg.usw2.cache.amazonaws.com:6379"
	redisEndpointStaging = "pachter-redis.mvep6k.clustercfg.usw2.cache.amazonaws.com:6379"

	rdsAwsSecretStoreArnProd    = "arn:aws:secretsmanager:us-west-2:458492755823:secret:pachter-rds-credentials-2AUg2L"
	rdsAwsSecretStoreArnStaging = "arn:aws:secretsmanager:us-west-2:721938063588:secret:pachter-rds-credentials-Kh5FrK"

	paydayHostProd    = "https://prod.payday.twitch.a2z.com"
	paydayHostStaging = "https://main.us-west-2.beta.payday.twitch.a2z.com"

	sqsQueueUrlProd    = "https://sqs.us-west-2.amazonaws.com/458492755823/bits-spend-prod-v2.fifo"
	sqsQueueUrlStaging = "https://sqs.us-west-2.amazonaws.com/721938063588/bits-spend-staging-v2.fifo"
)

var sqsQueueURL string
var payday paydayrpc.Payday
var redis r.RedisClient
var bitsSpendRDS bits_spend.DAO
var transactionRDS bits_transaction.DAO
var auditDAO worker_audit.DAO
var sqsClient *sqs.SQS
var rateLimiter *rate.Limiter

func main() {
	redisEndpoint := redisEndpointStaging
	rdsAwsSecretStoreArn := rdsAwsSecretStoreArnStaging
	paydayHost := paydayHostStaging
	sqsQueueURL = sqsQueueUrlStaging

	if os.Getenv(environment) == prod {
		redisEndpoint = redisEndpointProd
		rdsAwsSecretStoreArn = rdsAwsSecretStoreArnProd
		paydayHost = paydayHostProd
		sqsQueueURL = sqsQueueUrlProd
	}

	cfg := &config.Config{
		RedisEndpoint:          redisEndpoint,
		RedisConnectionPerNode: 50,
		RDSAWSSecretStoreARN:   rdsAwsSecretStoreArn,
		UseClusteredRedis:      true,
	}

	var redisErr error
	redis, redisErr = r.NewRedisClient(cfg)
	if redisErr != nil {
		logrus.WithError(redisErr).Panic("Failed to initialize Redis client")
	}

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2"),
	})
	if err != nil {
		logrus.WithError(err).Panic("failed to create AWS session")
	}

	rdsClient, err := rds.NewRDSClient(cfg, sess)
	if err != nil {
		logrus.WithError(err).Panic("Failed to initialize RDS client")
	}

	bitsSpendRDS = bits_spend.NewDAOWithClient(rdsClient)
	transactionRDS = bits_transaction.NewDAOWithClient(rdsClient)
	payday = paydayrpc.NewPaydayProtobufClient(paydayHost, http.DefaultClient)
	auditDAO = worker_audit.NewDAO(sess, &config.Config{
		EnvironmentName: os.Getenv(environment),
	})
	sqsClient = sqs.New(sess)
	rateLimiter = rate.NewLimiter(rate.Limit(300), 300) // 10 messages per batch, 3000 messages per second is limit

	lambda.Start(Handler)
}

// Can either provide a list of users or a single user (if provided, ignores the former).
// Why can't we just pass a list of one in the single user case? This implementation was chosen because it was the simplest option for the SFN Map function.
type Request struct {
	Users []string `json:"users"`
	User  string   `json:"user"`
}

func Handler(ctx context.Context, req Request) error {
	logrus.Infoln("starting targeted resetter")

	if req.User != "" {
		req.Users = []string{req.User}
	}

	for _, user := range req.Users {
		logrus.Infof("resetting user: %s \n", user)
		err := recalculateBitsSpendOrder(ctx, user)
		if err != nil {
			return err
		}
	}

	return nil
}

func recalculateBitsSpendOrder(ctx context.Context, userID string) error {
	log := logrus.WithField("userID", userID)
	allTransactions, err := clearData(ctx, userID)
	if err != nil {
		log.WithError(err).Error("Error while clearing data for this user")
		return err
	}

	batchRequest := make([]*sqs.SendMessageBatchRequestEntry, 0, batchSize)
	for i, transaction := range allTransactions {
		transactionJson, err := json.Marshal(transaction)
		if err != nil {
			log.WithError(err).Error("Error marshaling transaction")
			return err
		}

		entry := &sqs.SendMessageBatchRequestEntry{
			Id:             aws.String(uuid.Must(uuid.NewV4()).String()),
			MessageBody:    aws.String(string(transactionJson)),
			MessageGroupId: aws.String(worker.SPEND_ORDER_GROUP_ID_PREFIX + userID),

			// Since we are recalculating everything for this user, there may be a transaction
			// of his that was processed by the spend order worker in the past 5 minutes, which
			// will be ignored by the FIFO queue for its content based deduplication setting. In order to
			// make sure that all the transactions are re-processed by the spend order worker,
			// we need to override the message deduplication id manually which otherwise will
			// default to the system generated one based on the message body.
			MessageDeduplicationId: aws.String(uuid.Must(uuid.NewV4()).String()),
		}
		batchRequest = append(batchRequest, entry)

		if len(batchRequest) == batchSize || i == len(allTransactions)-1 {
			_ = rateLimiter.Wait(ctx)
			batchSendOutput, err := sqsClient.SendMessageBatch(&sqs.SendMessageBatchInput{
				Entries:  batchRequest,
				QueueUrl: &sqsQueueURL,
			})

			if err != nil || len(batchSendOutput.Failed) > 0 {
				log.WithError(err).Error("Error sending to SQS queue")
				return errors.Wrap(err, "Error sending to SQS queue")
			}

			batchRequest = []*sqs.SendMessageBatchRequestEntry{}
		}
	}

	return nil
}

func clearData(ctx context.Context, userID string) ([]rds_models.BitsTransaction, error) {
	allTransactions, err := getAllTransactionsForUser(userID)
	if err != nil {
		logrus.WithError(err).Error("clearData: getAllTransactionIDsForUser failed")
		return nil, err
	}

	// Reset Dynamo audit table
	for _, transaction := range allTransactions {
		err = auditDAO.Remove(ctx, transaction.TransactionID, []string{"spend"})
		if err != nil {
			logrus.WithError(err).Error("clearData: auditDAO.Remove failed")
			return nil, err
		}
	}

	// Reset Redis
	err = redis.Del(r.BitsTransactionDelCmd, fmt.Sprintf(redisKeyFormat, userID))
	if err != nil {
		logrus.WithError(err).Error("clearData: resetting redis failed")
		return nil, err
	}

	// Reset RDS bits_spends table
	err = bitsSpendRDS.DeleteAllByRequestingUserID(userID)
	if err != nil {
		logrus.WithError(err).Error("clearData: resetting RDS failed")
		return nil, err
	}

	// Reset RDS table
	return allTransactions, nil
}

func getAllTransactionsForUser(userID string) ([]rds_models.BitsTransaction, error) {
	transactions, err := transactionRDS.GetByRequestingTwitchUserID(userID)
	if err != nil && !gorm.IsRecordNotFoundError(err) {
		return nil, err
	}

	sort.Slice(transactions, func(i, j int) bool {
		return transactions[i].Datetime.Before(transactions[j].Datetime)
	})

	return transactions, nil
}
