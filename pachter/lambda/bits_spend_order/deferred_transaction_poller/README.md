# Lambda - Deferred Bits Transaction Poller (v2)
We have an SQS worker (bits_transaction_deferrer_v2) that receives and puts transaction from Payday into a Redis sorted set. That is to ensure
the transactions are in chronological order. This Lambda function is triggered to pull the oldest transactions
from the Redis sorted set and sends them to the Bits spend order FIFO queue v2.

The step to setup this lambda function is to:
1. Make the change you want
2. Build the new change and zip it by `$ ./zip.sh`
3. That will output `deferred_transaction_poller_lambda.zip` file. Copy and paste it under `terraform/pachter/`
4. Run Terraform