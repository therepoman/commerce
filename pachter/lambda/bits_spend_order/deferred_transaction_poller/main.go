package main

import (
	"encoding/json"
	"os"
	"strings"
	"time"

	"code.justin.tv/commerce/logrus"
	r "code.justin.tv/commerce/pachter/backend/clients/redis"
	rds_models "code.justin.tv/commerce/pachter/backend/rds/models"
	"code.justin.tv/commerce/pachter/backend/worker"
	"code.justin.tv/commerce/pachter/backend/worker/bits_transaction_deferrer_v2"
	"code.justin.tv/commerce/pachter/config"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
)

const (
	environment = "LAMBDA_ENVIRONMENT"
	prod        = "prod"

	redisEndpointProd    = "pachter-redis.sfonzr.clustercfg.usw2.cache.amazonaws.com:6379"
	redisEndpointStaging = "pachter-redis.mvep6k.clustercfg.usw2.cache.amazonaws.com:6379"

	bitsSpendOrderSqsUrlStaging = "https://sqs.us-west-2.amazonaws.com/721938063588/bits-spend-staging-v2.fifo"
	bitsSpendOrderSqsUrlProd    = "https://sqs.us-west-2.amazonaws.com/458492755823/bits-spend-prod-v2.fifo"
)

var redis r.RedisClient
var sqsClient *sqs.SQS
var sqsURL string

func init() {
	redisEndpoint := redisEndpointStaging
	sqsURL = bitsSpendOrderSqsUrlStaging
	if os.Getenv(environment) == prod {
		redisEndpoint = redisEndpointProd
		sqsURL = bitsSpendOrderSqsUrlProd
	}

	var err error
	redis, err = r.NewRedisClient(&config.Config{
		RedisEndpoint:          redisEndpoint,
		RedisConnectionPerNode: 50,
		UseClusteredRedis:      true,
	})
	if err != nil {
		logrus.WithError(err).Panic("Failed to create Redis client instance")
	}

	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))
	sqsClient = sqs.New(sess)
}

func main() {
	lambda.Start(Handler)
}

func Handler() error {
	logrus.Info("Poller starting")
	threeMinutesAgo := time.Now().Add(time.Minute * -3)

	for {
		tx, sortedSetMember, err := getOldestTransaction()
		if err != nil {
			logrus.WithError(err).Error("Error getting the first element from deferred pool")
			return err
		}
		if tx == nil {
			logrus.Info("No more transactions in deferred pool")
			return nil
		}

		logrus.WithField("oldestTx", tx).Info("Got the oldest transaction")
		logger := logrus.WithField("oldestTx", tx)

		if tx.Datetime.Before(threeMinutesAgo) {
			logger.Info("Oldest transaction is sufficiently old, sending it to FIFO queue v2")
			err = sendMessage(*tx)
			if err != nil {
				logger.WithError(err).Error("Error sending message to FIFO queue v2")
				return err
			}

			logger.Info("Transaction sent to FIFO queue v2, removing it from the deferred pool")
			err = redis.ZRem(r.BitsDeferredTransactionZRemCmd,
				bits_transaction_deferrer_v2.DeferRedisSortedKey, sortedSetMember)
			if err != nil {
				logger.WithError(err).Error("Error removing transaction from the deferred pool")
				return err
			}

			logger.Info("Transaction successfully removed from the deferred pool")
		} else {
			logger.Info("Oldest transaction is not sufficiently old. Exiting")
			break
		}
	}

	return nil
}

func getOldestTransaction() (*rds_models.BitsTransaction, string, error) {
	vals, err := redis.ZRange(bits_transaction_deferrer_v2.DeferRedisSortedKey,
		0, 0)
	if err != nil {
		return nil, "", err
	}

	txs, err := extract(vals)
	if err != nil {
		return nil, "", err
	}

	if len(txs) > 0 {
		return &txs[0], vals[0], nil
	}

	return nil, "", nil
}

func extract(tranStrs []string) ([]rds_models.BitsTransaction, error) {
	transactions := make([]rds_models.BitsTransaction, len(tranStrs))
	for i, tranStr := range tranStrs {
		transaction := rds_models.BitsTransaction{}
		err := json.Unmarshal([]byte(tranStr), &transaction)
		if err != nil {
			return nil, err
		}

		transactions[i] = transaction
	}

	return transactions, nil
}

func sendMessage(transaction rds_models.BitsTransaction) (err error) {
	transactionJson, err := json.Marshal(transaction)
	if err != nil {
		return
	}

	// An invalid transaction id got through with a trailing whitespace character
	// Whitespace characters are not allowed in the SQS dedupe id
	// TODO: Remove this once we have proper validation for transaction ids
	dedupeID := strings.TrimSpace(transaction.TransactionID)

	_, err = sqsClient.SendMessage(&sqs.SendMessageInput{
		DelaySeconds:           aws.Int64(0),
		MessageBody:            aws.String(string(transactionJson)),
		MessageGroupId:         aws.String(worker.SPEND_ORDER_GROUP_ID_PREFIX + transaction.RequestingTwitchUserID),
		QueueUrl:               &sqsURL,
		MessageDeduplicationId: aws.String(dedupeID),
	})

	return
}
