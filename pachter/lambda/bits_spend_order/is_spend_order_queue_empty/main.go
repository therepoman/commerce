package main

import (
	"context"
	"errors"
	"os"
	"strconv"

	"github.com/aws/aws-sdk-go/service/sqs"

	"code.justin.tv/commerce/logrus"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
)

const (
	environment = "LAMBDA_ENVIRONMENT" // "staging" or "prod"
	prod        = "prod"

	sqsQueueUrlProd    = "https://sqs.us-west-2.amazonaws.com/458492755823/bits-spend-prod-v2.fifo"
	sqsQueueUrlStaging = "https://sqs.us-west-2.amazonaws.com/721938063588/bits-spend-staging-v2.fifo"

	queueNotEmptyError = "QUEUE_NOT_EMPTY"
	// We consider the queue empty when it has <= 100 items (the queue might never hit 0 because of bits usage TPS)
	emptyishThreshold = 100
)

type request struct{}

type handler struct {
	sqsClient   *sqs.SQS
	sqsQueueURL string
}

func main() {
	awsConfig := aws.NewConfig().WithRegion("us-west-2")
	sess, err := session.NewSession(awsConfig)
	if err != nil {
		logrus.Panic("failed to create aws session")
	}

	sqsClient := sqs.New(sess, awsConfig)

	sqsQueueURL := sqsQueueUrlStaging
	if os.Getenv(environment) == prod {
		sqsQueueURL = sqsQueueUrlProd
	}

	h := handler{
		sqsClient:   sqsClient,
		sqsQueueURL: sqsQueueURL,
	}

	lambda.Start(h.handle)
}

func (h *handler) handle(ctx context.Context, req request) error {
	logrus.WithField("req", req).Info("starting is_spend_order_queue_empty lambda")

	count, err := getMessagesVisible(h.sqsClient, h.sqsQueueURL)
	if err != nil {
		return err
	}

	if count > emptyishThreshold {
		return errors.New(queueNotEmptyError)
	}

	return nil
}

func getMessagesVisible(sqsClient *sqs.SQS, queue string) (int64, error) {
	output, err := sqsClient.GetQueueAttributes(&sqs.GetQueueAttributesInput{
		AttributeNames: []*string{aws.String(sqs.QueueAttributeNameApproximateNumberOfMessages)},
		QueueUrl:       aws.String(queue),
	})

	if err != nil {
		return 0, err
	}

	val, ok := output.Attributes[sqs.QueueAttributeNameApproximateNumberOfMessages]

	if !ok || val == nil {
		return 0, errors.New("GetQueueAttributes missing ApproximateNumberOfMessages")
	}

	count, err := strconv.ParseInt(*val, 10, 64)

	if err != nil {
		return 0, errors.New("error parsing ApproximateNumberOfMessages")
	}

	return count, nil
}
