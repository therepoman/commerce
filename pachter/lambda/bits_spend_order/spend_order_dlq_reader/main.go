package main

import (
	"context"
	"os"

	"code.justin.tv/commerce/pachter/backend/worker"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"

	"code.justin.tv/commerce/logrus"
	"github.com/aws/aws-lambda-go/lambda"
)

const (
	environment = "LAMBDA_ENVIRONMENT" // "staging" or "prod"
	prod        = "prod"

	dlqUrlProd    = "https://sqs.us-west-2.amazonaws.com/458492755823/bits-spend-prod-v2-dlq.fifo"
	dlqUrlStaging = "https://sqs.us-west-2.amazonaws.com/721938063588/bits-spend-staging-v2-dlq.fifo"

	visibilityTimeoutSeconds = 60 * 10
	maxBatchDeletionMessages = 10
)

type request struct {
	Users []string `json:"users"`
}

type response struct {
	Users       []string `json:"users"`
	DlqWasEmpty bool     `json:"dlqWasEmpty"`
}

type handler struct {
	sqsClient *sqs.SQS
	dlqURL    string
}

func main() {
	awsConfig := aws.NewConfig().WithRegion("us-west-2")
	sess, err := session.NewSession(awsConfig)
	if err != nil {
		logrus.Panic("failed to create aws session")
	}

	sqsClient := sqs.New(sess, awsConfig)

	dlqURL := dlqUrlStaging
	if os.Getenv(environment) == prod {
		dlqURL = dlqUrlProd
	}

	h := handler{
		sqsClient: sqsClient,
		dlqURL:    dlqURL,
	}

	lambda.Start(h.handle)
}

/**
Pulls all messages off DLQ, accumulating unique users on the input user list
*/
func (h *handler) handle(ctx context.Context, req request) (response, error) {
	logrus.WithField("req", req).Info("starting spend_order_dlq_reader lambda")

	users := req.Users

	// In the no items case, SFN Map wants an empty (rather a nil) slice
	if users == nil {
		users = []string{}
	}

	dlqWasEmpty := true
	var deletions []*sqs.DeleteMessageBatchRequestEntry

	for {
		receiveOutput, err := h.sqsClient.ReceiveMessage(&sqs.ReceiveMessageInput{
			QueueUrl:            aws.String(h.dlqURL),
			MaxNumberOfMessages: aws.Int64(10), //max is 10 per library specification
			WaitTimeSeconds:     aws.Int64(20), //max is 20 per library specification
			VisibilityTimeout:   aws.Int64(visibilityTimeoutSeconds),
			AttributeNames:      []*string{aws.String(sqs.MessageSystemAttributeNameMessageGroupId)},
		})

		if err != nil {
			return response{
				Users: users,
			}, err
		}

		messagesReceived := len(receiveOutput.Messages)

		if messagesReceived == 0 {
			break
		} else {
			dlqWasEmpty = false
		}

		for _, message := range receiveOutput.Messages {
			if message == nil {
				continue
			}

			deletions = append(deletions, &sqs.DeleteMessageBatchRequestEntry{
				Id:            aws.String(*message.MessageId),
				ReceiptHandle: aws.String(*message.ReceiptHandle),
			})

			messageUserID := worker.GetUserIDFromSpendOrderMessage(message)
			messageUserIsUnique := true
			for _, userID := range users {
				if userID == messageUserID {
					messageUserIsUnique = false
					break
				}
			}

			if messageUserIsUnique {
				users = append(users, messageUserID)
			}
		}
	}

	if len(deletions) > 0 {
		h.deleteMessages(deletions)
	}

	return response{
		Users:       users,
		DlqWasEmpty: dlqWasEmpty,
	}, nil
}

func (h *handler) deleteMessages(deletions []*sqs.DeleteMessageBatchRequestEntry) {
	chunks := [][]*sqs.DeleteMessageBatchRequestEntry{
		{},
	}
	currentChunkIndex := 0

	for _, deletion := range deletions {
		if len(chunks[currentChunkIndex]) == maxBatchDeletionMessages {
			currentChunkIndex++
			chunks = append(chunks, []*sqs.DeleteMessageBatchRequestEntry{})
		}

		chunks[currentChunkIndex] = append(chunks[currentChunkIndex], deletion)
	}

	for _, chunk := range chunks {
		_, err := h.sqsClient.DeleteMessageBatch(&sqs.DeleteMessageBatchInput{
			Entries:  chunk,
			QueueUrl: aws.String(h.dlqURL),
		})

		// Since we recurse if !DlqWasEmpty and duplicate userIDs are ignored, we get retries for "free" and don't
		// need to error.
		if err != nil {
			logrus.WithError(err).Error("Failed to batch delete dlq messages")
		}
	}
}
