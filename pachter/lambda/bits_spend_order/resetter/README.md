# Lambda - Bits Spend Order Resetter
This Lambda function is for 1. resetting and 2. re-calculating Bits spends order for users who didn't get processed successfully by the spend order worker and ended up in the DLQ.

The step to setup this lambda function is to:
1. Make the change you want
2. Build the new change and zip it by `$ ./zip.sh`
3. That will output `bits_spend_order_resetter_lambda.zip` file. Copy and paste it under `terraform/pachter/`
4. Run Terraform

## Description:
Here is the way it works:
This Lambda is to be either manually triggered by clicking the 'Run' button on the Lambda console
or by CloudWatch event (e.g. hourly). When the Lambda is triggered, it:
- first retrieves up to 10 messages (failed transaction IDs) from the spend order DLQ
- for each failed user, resets 1. Dynamo audit table 2. Redis sorted set 3. RDS entry
- gets ALL the past transactions for each user from RDS `bits_transactions` table
- sends those transactions to spend order FIFO queue in chronological order (from oldest to latest) for each user
- repeats the steps above until either Lambda times out (15 minutes) or the DLQ is empty

Long story short is that this Lambda basically does the backfill for the failed users!

## Note:
When this Lambda function fails to process a transaction, all the transactions that were in the DLQ that belong to the
same user will be sent right back to the DLQ, respecting the chronological order. That is the kind of case where you
need to manually inspect what is going on with the user. Most likely there is (a) missing transaction(s) for this user
in the `bits_transactions` table.