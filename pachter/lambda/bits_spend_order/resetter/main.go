package main

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"sort"
	"strings"
	"sync"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachter/backend/clients/rds"
	r "code.justin.tv/commerce/pachter/backend/clients/redis"
	"code.justin.tv/commerce/pachter/backend/dynamo/worker_audit"
	"code.justin.tv/commerce/pachter/backend/rds/bits_spend"
	"code.justin.tv/commerce/pachter/backend/rds/bits_transaction"
	"code.justin.tv/commerce/pachter/backend/worker"
	"code.justin.tv/commerce/pachter/config"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/gofrs/uuid"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"golang.org/x/time/rate"
)

const (
	environment    = "LAMBDA_ENVIRONMENT" // "staging" or "prod"
	prod           = "prod"
	redisKeyFormat = "user-bits-transactions-%s"
	batchSize      = 10

	redisEndpointProd    = "pachter-redis.sfonzr.clustercfg.usw2.cache.amazonaws.com:6379"
	redisEndpointStaging = "pachter-redis.mvep6k.clustercfg.usw2.cache.amazonaws.com:6379"

	rdsAwsSecretStoreArnProd    = "arn:aws:secretsmanager:us-west-2:458492755823:secret:pachter-rds-credentials-2AUg2L"
	rdsAwsSecretStoreArnStaging = "arn:aws:secretsmanager:us-west-2:721938063588:secret:pachter-rds-credentials-Kh5FrK"

	dlqUrlProd    = "https://sqs.us-west-2.amazonaws.com/458492755823/bits-spend-prod-dlq.fifo"
	dlqUrlStaging = "https://sqs.us-west-2.amazonaws.com/721938063588/bits-spend-staging-dlq.fifo"

	sqsQueueUrlProd    = "https://sqs.us-west-2.amazonaws.com/458492755823/bits-spend-prod.fifo"
	sqsQueueUrlStaging = "https://sqs.us-west-2.amazonaws.com/721938063588/bits-spend-staging.fifo"
)

var sqsQueueURL string
var dlqURL string
var redis r.RedisClient
var bitsSpendRDS bits_spend.DAO
var transactionRDS bits_transaction.DAO
var auditDAO worker_audit.DAO
var sqsClient *sqs.SQS
var rateLimiter *rate.Limiter

func main() {
	redisEndpoint := redisEndpointStaging
	rdsAwsSecretStoreArn := rdsAwsSecretStoreArnStaging
	sqsQueueURL = sqsQueueUrlStaging
	dlqURL = dlqUrlStaging

	if os.Getenv(environment) == prod {
		redisEndpoint = redisEndpointProd
		rdsAwsSecretStoreArn = rdsAwsSecretStoreArnProd
		sqsQueueURL = sqsQueueUrlProd
		dlqURL = dlqUrlProd
	}

	cfg := &config.Config{
		RedisEndpoint:          redisEndpoint,
		RedisConnectionPerNode: 50,
		RDSAWSSecretStoreARN:   rdsAwsSecretStoreArn,
		UseClusteredRedis:      true,
	}

	var redisErr error
	redis, redisErr = r.NewRedisClient(cfg)
	if redisErr != nil {
		logrus.WithError(redisErr).Panic("Failed to initialize Redis client")
	}

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2"),
	})
	if err != nil {
		logrus.WithError(err).Panic("failed to create AWS session")
	}

	rdsClient, err := rds.NewRDSClient(cfg, sess)
	if err != nil {
		logrus.WithError(err).Panic("Failed to initialize RDS client")
	}

	bitsSpendRDS = bits_spend.NewDAOWithClient(rdsClient)
	transactionRDS = bits_transaction.NewDAOWithClient(rdsClient)
	auditDAO = worker_audit.NewDAO(sess, &config.Config{
		EnvironmentName: os.Getenv(environment),
	})
	sqsClient = sqs.New(sess)
	rateLimiter = rate.NewLimiter(rate.Limit(300), 300) // 10 messages per batch, 3000 messages per second is limit

	lambda.Start(Handler)
}

func Handler() error {
	logrus.Info("starting resetter.")

	ctx := context.Background()

	for {
		params := &sqs.ReceiveMessageInput{
			QueueUrl:            aws.String(dlqURL),
			MaxNumberOfMessages: aws.Int64(10),  //max is 10 per library specification
			WaitTimeSeconds:     aws.Int64(20),  //max is 20 per library specification
			VisibilityTimeout:   aws.Int64(900), //makes sense to sync this with the Lambda timeout
			AttributeNames:      []*string{aws.String(worker.MESSAGE_GROUP_ID_ATTRIBUTE)},
		}
		dlqResp, err := sqsClient.ReceiveMessage(params)
		if err != nil {
			logrus.WithError(err).Error("Failed to receive DLQ messages")
			continue
		}
		if dlqResp == nil || len(dlqResp.Messages) == 0 {
			logrus.Info("Got no messages from the DLQ, exiting handler.")
			return nil
		}

		userToDLQMessages := make(map[string][]*string)
		wg := sync.WaitGroup{}
		for _, dlqMessage := range dlqResp.Messages {
			userID := worker.GetUserIDFromSpendOrderMessage(dlqMessage)
			if userID == "" {
				logrus.Warn("Group ID does not have proper value")
				continue
			}

			userToDLQMessages[userID] = append(userToDLQMessages[userID], dlqMessage.ReceiptHandle)
		}

		for userID, dlqMessages := range userToDLQMessages {
			wg.Add(1)
			go func(user string, dlqMsgs []*string) {
				logger := logrus.WithField("userID", user)
				logger.Info("resetting user")
				err = recalculateBitsSpendOrder(ctx, user)
				if err != nil {
					logger.WithError(err).Error("failed on user recalculation.")
				} else {
					logger.WithField("receiptHandles", dlqMsgs).Info("Deleting message")

					_, err = sqsClient.DeleteMessageBatch(&sqs.DeleteMessageBatchInput{
						QueueUrl: aws.String(dlqURL),
						Entries:  makeDeleteBatchRequest(dlqMsgs),
					})
					if err != nil {
						logger.WithError(err).Error("failed on batch message delete")
					}
				}
				wg.Done()
			}(userID, dlqMessages)
		}
		wg.Wait()
	}
}

func recalculateBitsSpendOrder(ctx context.Context, userID string) error {
	log := logrus.WithField("userID", userID)
	allTransactionIDs, err := clearData(ctx, userID)
	if err != nil {
		log.WithError(err).Error("Error while clearing data for this user")
		return err
	}

	batchRequest := make([]*sqs.SendMessageBatchRequestEntry, 0, batchSize)
	for i, transactionID := range allTransactionIDs {
		sqsBody, err := getSQSBody([]string{transactionID})
		if err != nil {
			log.WithError(err).Error("Error parsing transaction ID into SQS message")
			return err
		}

		entry := &sqs.SendMessageBatchRequestEntry{
			Id:             aws.String(uuid.Must(uuid.NewV4()).String()),
			MessageBody:    sqsBody,
			MessageGroupId: aws.String("bits-spend-order-" + userID),
		}
		batchRequest = append(batchRequest, entry)

		if len(batchRequest) == batchSize || i == len(allTransactionIDs)-1 {
			_ = rateLimiter.Wait(ctx)
			batchSendOutput, err := sqsClient.SendMessageBatch(&sqs.SendMessageBatchInput{
				Entries:  batchRequest,
				QueueUrl: &sqsQueueURL,
			})

			if err != nil || len(batchSendOutput.Failed) > 0 {
				log.WithError(err).Error("Error sending to SQS queue")
				return errors.Wrap(err, "Error sending to SQS queue")
			}

			batchRequest = []*sqs.SendMessageBatchRequestEntry{}
		}
	}

	return nil
}

func clearData(ctx context.Context, userID string) ([]string, error) {
	allTransactionIDs, err := getAllTransactionIDsForUser(userID)
	if err != nil {
		logrus.WithError(err).Error("clearData: getAllTransactionIDsForUser failed")
		return nil, err
	}

	// Reset Dynamo audit table
	for _, transactionID := range allTransactionIDs {
		err = auditDAO.Remove(ctx, transactionID, []string{"spend"})
		if err != nil {
			logrus.WithError(err).Error("clearData: auditDAO.Remove failed")
			return nil, err
		}
	}

	// Reset Redis
	err = redis.Del(r.BitsTransactionDelCmd, fmt.Sprintf(redisKeyFormat, userID))
	if err != nil {
		logrus.WithError(err).Error("clearData: resetting redis failed")
		return nil, err
	}

	// Reset RDS bits_spends table
	err = bitsSpendRDS.DeleteAllByRequestingUserID(userID)
	if err != nil {
		logrus.WithError(err).Error("clearData: resetting RDS failed")
		return nil, err
	}

	// Reset RDS table
	return allTransactionIDs, nil
}

func getAllTransactionIDsForUser(userID string) ([]string, error) {
	transactions, err := transactionRDS.GetByRequestingTwitchUserID(userID)
	if err != nil && !gorm.IsRecordNotFoundError(err) {
		return nil, err
	}

	sort.Slice(transactions, func(i, j int) bool {
		return transactions[i].Datetime.Before(transactions[j].Datetime)
	})

	transactionIDs := make([]string, 0)
	for _, transaction := range transactions {
		transactionIDs = append(transactionIDs, strings.TrimSpace(transaction.TransactionID))
	}

	return transactionIDs, nil
}

func getSQSBody(transactionIDs []string) (*string, error) {
	balanceUpdate := worker.BalanceUpdateMessage{
		TransactionIDs: transactionIDs,
	}
	balanceUpdateJson, err := json.Marshal(&balanceUpdate)
	if err != nil {
		return nil, err
	}

	snsMessage := worker.SNSMessage{
		MessageID: uuid.Must(uuid.NewV4()).String(),
		Message:   string(balanceUpdateJson),
	}
	snsMessageJson, err := json.Marshal(&snsMessage)
	if err != nil {
		return nil, err
	}

	return aws.String(string(snsMessageJson)), nil
}

func makeDeleteBatchRequest(receiptHandles []*string) []*sqs.DeleteMessageBatchRequestEntry {
	deleteEntries := make([]*sqs.DeleteMessageBatchRequestEntry, 0)
	for _, receiptHandle := range receiptHandles {
		deleteEntries = append(deleteEntries, &sqs.DeleteMessageBatchRequestEntry{
			Id:            aws.String(uuid.Must(uuid.NewV4()).String()),
			ReceiptHandle: receiptHandle,
		})
	}
	return deleteEntries
}
