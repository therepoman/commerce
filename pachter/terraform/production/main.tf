module "pachter" {
  source                               = "../pachter"
  commit                               = "latest"
  name                                 = "pachter"
  region                               = "us-west-2"
  backup_region                        = "us-east-2"
  environment                          = "production"
  environment_short                    = "prod"
  account_id                           = "458492755823"
  vpc_id                               = "vpc-0bab2e20261adf6c7"
  security_group                       = "sg-03820cfe396aca1de"
  subnets                              = "subnet-05269401a9c38f861,subnet-0babb08afbfd34f31,subnet-097cd69fe48f60fda"
  availability_zones                   = ["us-west-2a", "us-west-2b", "us-west-2c"]
  vpc_cidr_block                       = "10.205.208.0/22"
  private_cidr_blocks                  = ["10.205.208.0/24", "10.205.209.0/24", "10.205.210.0/24"]
  route_table_external_id              = "rtb-03a651176340413cf"
  route_table_internal_a_id            = "rtb-07329ce26b2ae45d5"
  route_table_internal_b_id            = "rtb-0b023f03d230252b4"
  route_table_internal_c_id            = "rtb-080bcecf0c8480eb1"
  pachter_payday_vpc_peering_id        = "pcx-0a3787ab3a643297b"
  bits_balance_update_sns_topic        = "arn:aws:sns:us-west-2:702056039310:bits-prod-updates"
  payday_bits_balance_update_sns_topic = "arn:aws:sns:us-west-2:021561903526:pachter-ingestion-prod"

  ecs_task_cpu    = 4096
  ecs_task_memory = 16384

  # PrivateLink
  petozi_vpc_endpoint_service_name   = "com.amazonaws.vpce.us-west-2.vpce-svc-0e5642c1438fcc295"
  petozi_dns                         = "main.us-west-2.prod.petozi.twitch.a2z.com"
  payday_vpc_endpoint_service_name   = "com.amazonaws.vpce.us-west-2.vpce-svc-07a49906f8e043689"
  payday_dns                         = "prod.payday.twitch.a2z.com"
  payday_private_zone                = "payday.twitch.a2z.com"
  pachinko_vpc_endpoint_service_name = "com.amazonaws.vpce.us-west-2.vpce-svc-0d53a5c5263a303e6"
  pachinko_dns                       = "bits.prod.pachinko.twitch.a2z.com"
  pachinko_private_zone              = "pachinko.twitch.a2z.com"

  # Dynamo
  dynamo_audit_table_read_capacity             = "400"
  dynamo_audit_table_write_capacity            = "2000"
  dynamo_lock_table_read_capacity              = "100"
  dynamo_lock_table_write_capacity             = "100"
  dynamo_cheermote_config_table_read_capacity  = "50"
  dynamo_cheermote_config_table_write_capacity = "5"
  dynamo_actuals_table_read_capacity           = "100"
  dynamo_actuals_table_write_capacity          = "5"
  dynamo_autoscaling_role                      = "arn:aws:iam::458492755823:role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable"

  # RDS
  rds_instance_identifier       = "rds-instance-pachter-production"
  rds_name                      = "PachterProduction"
  rds_username                  = "terraform"
  rds_glue_export_job_username  = "gluedbexport_production"
  trigger_schedule              = "5 7 * * ? *" // Once per day at 12:05am PST
  rds_password                  = "user-terraform-temp-password"
  rds_allocated_storage         = 5000
  rds_backup_retention_period   = 7
  rds_secret_arn                = "arn:aws:secretsmanager:us-west-2:458492755823:secret:pachter-rds-credentials-2AUg2L"
  rds_instance_class            = "db.r5.24xlarge"
  rds_storage_type              = "io1"
  rds_iops                      = 80000
  redis_instance_type           = "cache.m4.large"
  redis_num_node_groups         = "5"
  redis_replicas_per_node_group = "2"

  # Private Links
  whitelisted_arns_for_privatelink = [
    "arn:aws:iam::021561903526:root", // payday
    "arn:aws:iam::196915980276:root", // admin-panel production
  ]

  # Tahoe
  tahoe_producer_name = "pachterdbexportproduction"

  providers = {
    aws        = aws
    aws.backup = aws.backup
  }
}

terraform {
  backend "s3" {
    bucket  = "twitch-pachter-aws-prod"
    key     = "tfstate/commerce/pachter/terraform/prod"
    region  = "us-west-2"
    profile = "pachter-prod"
  }
}

provider "aws" {
  region  = "us-west-2"
  profile = "pachter-prod"
  version = ">= 2.64.0"
}

provider "aws" {
  alias   = "backup"
  region  = "us-east-2"
  profile = "pachter-prod"
  version = ">= 2.64.0"
}

