module "pachter" {
  source                               = "../pachter"
  commit                               = "latest"
  name                                 = "pachter"
  region                               = "us-west-2"
  backup_region                        = "us-east-2"
  environment                          = "staging"
  environment_short                    = "staging"
  account_id                           = "721938063588"
  vpc_id                               = "vpc-08e709775fdcd16d0"
  security_group                       = "sg-02f39ee10e0b0de0a"
  subnets                              = "subnet-0dc9cbd1a04f5ca3a,subnet-06dba552b94e818ef,subnet-00ef08340071b93bc"
  availability_zones                   = ["us-west-2a", "us-west-2b", "us-west-2c"]
  vpc_cidr_block                       = "10.205.188.0/22"
  private_cidr_blocks                  = ["10.205.188.0/24", "10.205.189.0/24", "10.205.190.0/24"]
  route_table_external_id              = "rtb-0da9ed2e4525ccfa7"
  route_table_internal_a_id            = "rtb-0e15a31fe360e87f0"
  route_table_internal_b_id            = "rtb-0bd6bb32bde35d506"
  route_table_internal_c_id            = "rtb-0066341250aa5de32"
  pachter_payday_vpc_peering_id        = "pcx-000204df19eede834"
  bits_balance_update_sns_topic        = "arn:aws:sns:us-west-2:946879801923:bits-staging-updates"
  payday_bits_balance_update_sns_topic = "arn:aws:sns:us-west-2:021561903526:pachter-ingestion-staging"

  ecs_task_cpu    = 256
  ecs_task_memory = 1024

  # PrivateLink
  petozi_vpc_endpoint_service_name   = "com.amazonaws.vpce.us-west-2.vpce-svc-07b64710c641bdf45"
  petozi_dns                         = "main.us-west-2.beta.petozi.twitch.a2z.com"
  payday_vpc_endpoint_service_name   = "com.amazonaws.vpce.us-west-2.vpce-svc-019e5066db9348615"
  payday_dns                         = "main.us-west-2.beta.payday.twitch.a2z.com"
  payday_private_zone                = "us-west2.justin.tv"
  pachinko_vpc_endpoint_service_name = "com.amazonaws.vpce.us-west-2.vpce-svc-050b2e14908931ed8"
  pachinko_dns                       = "bits.staging.pachinko.twitch.a2z.com"
  pachinko_private_zone              = "pachinko.twitch.a2z.com"

  # Dynamo
  dynamo_audit_table_read_capacity             = "10"
  dynamo_audit_table_write_capacity            = "5"
  dynamo_lock_table_read_capacity              = "10"
  dynamo_lock_table_write_capacity             = "5"
  dynamo_cheermote_config_table_read_capacity  = "10"
  dynamo_cheermote_config_table_write_capacity = "5"
  dynamo_actuals_table_read_capacity           = "10"
  dynamo_actuals_table_write_capacity          = "5"
  dynamo_autoscaling_role                      = "arn:aws:iam::721938063588:role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable"

  # RDS
  rds_instance_identifier       = "rds-instance-pachter-staging"
  rds_name                      = "PachterStaging"
  rds_username                  = "terraform"
  rds_glue_export_job_username  = "gluedbexport_staging"
  trigger_schedule              = "5 7 * * ? *" // Once per day at 12:05am PST
  rds_password                  = "user-terraform-temp-password"
  rds_allocated_storage         = 500
  rds_backup_retention_period   = 7
  rds_secret_arn                = "arn:aws:secretsmanager:us-west-2:721938063588:secret:pachter-rds-credentials-Kh5FrK"
  redis_instance_type           = "cache.m3.medium"
  redis_num_node_groups         = "2"
  redis_replicas_per_node_group = "1"

  # Tahoe
  tahoe_producer_name = "pachterdbexportstaging"

  # Private Links
  whitelisted_arns_for_privatelink = [
    "arn:aws:iam::021561903526:root", // payday
    "arn:aws:iam::219087926005:root", // admin-panel staging
  ]

  providers = {
    aws        = aws
    aws.backup = aws.backup
  }
}

provider "aws" {
  region  = "us-west-2"
  profile = "pachter-devo"
  version = ">= 2.64.0"
}

provider "aws" {
  alias   = "backup"
  region  = "us-east-2"
  profile = "pachter-devo"
  version = ">= 2.64.0"
}

terraform {
  backend "s3" {
    bucket  = "twitch-pachter-aws-devo"
    key     = "tfstate/commerce/pachter/terraform/staging"
    region  = "us-west-2"
    profile = "pachter-devo"
  }
}

