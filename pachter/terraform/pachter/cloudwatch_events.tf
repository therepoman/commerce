resource "aws_cloudwatch_event_rule" "previous_month_report_generation_rule" {
  name                = "BitsMonthlyReportLastMonth"
  description         = "Every noon generates previous month's Bits mega report"
  schedule_expression = "cron(0 12 * * ? *)"
}

resource "aws_cloudwatch_event_target" "previous_month_report_generation_target" {
  arn   = "arn:aws:sqs:us-west-2:${var.account_id}:bits-report-generator-${var.environment_short}"
  rule  = aws_cloudwatch_event_rule.previous_month_report_generation_rule.name
  input = "{\"period\":\"last-month\"}"
}

resource "aws_cloudwatch_event_rule" "current_month_report_generation_rule" {
  name                = "BitsMonthlyReportCurrentMonth"
  description         = "Every midnight generates current month's Bits mega report"
  schedule_expression = "cron(0 0 * * ? *)"
}

resource "aws_cloudwatch_event_target" "current_month_report_generation_target" {
  arn   = "arn:aws:sqs:us-west-2:${var.account_id}:bits-report-generator-${var.environment_short}"
  rule  = aws_cloudwatch_event_rule.current_month_report_generation_rule.name
  input = "{\"period\":\"current-month\"}"
}

