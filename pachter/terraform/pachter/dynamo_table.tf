module "bits-cheermote-payout-configs-table" {
  source    = "./modules/dynamo_global_table"
  name      = "bits-cheermote-payout-configs-${var.environment_short}"
  hash_key  = "prefix"
  range_key = "twitchUserId"

  attributes = [
    {
      name = "prefix"
      type = "S"
    },
    {
      name = "twitchUserId"
      type = "S"
    },
  ]

  read_capacity                    = var.dynamo_cheermote_config_table_read_capacity
  write_capacity                   = var.dynamo_cheermote_config_table_write_capacity
  autoscaling_role                 = var.dynamo_autoscaling_role
  region                           = var.region
  backup_region                    = var.backup_region
  dynamo_backup_lambda_arn         = module.dynamo_backup_lambda.lambda_arn
  replica_dynamo_backup_lambda_arn = module.replica_dynamo_backup_lambda.lambda_arn

  providers = {
    aws        = aws
    aws.backup = aws.backup
  }
}

module "worker-audit-table" {
  source   = "./modules/dynamo_global_table"
  name     = "worker-audit-${var.environment_short}"
  hash_key = "transactionId"

  attributes = [
    {
      name = "transactionId"
      type = "S"
    },
  ]

  read_capacity                    = var.dynamo_audit_table_read_capacity
  write_capacity                   = var.dynamo_audit_table_write_capacity
  autoscaling_role                 = var.dynamo_autoscaling_role
  region                           = var.region
  backup_region                    = var.backup_region
  dynamo_backup_lambda_arn         = module.dynamo_backup_lambda.lambda_arn
  replica_dynamo_backup_lambda_arn = module.replica_dynamo_backup_lambda.lambda_arn

  providers = {
    aws        = aws
    aws.backup = aws.backup
  }
}

module "bits-accounting-actuals-table" {
  source    = "./modules/dynamo_global_table"
  name      = "bits-accounting-actuals-${var.environment_short}"
  hash_key  = "platform"
  range_key = "yearMonth"

  attributes = [
    {
      name = "platform"
      type = "S"
    },
    {
      name = "yearMonth"
      type = "S"
    },
  ]

  read_capacity                    = var.dynamo_actuals_table_read_capacity
  write_capacity                   = var.dynamo_actuals_table_write_capacity
  autoscaling_role                 = var.dynamo_autoscaling_role
  region                           = var.region
  backup_region                    = var.backup_region
  dynamo_backup_lambda_arn         = module.dynamo_backup_lambda.lambda_arn
  replica_dynamo_backup_lambda_arn = module.replica_dynamo_backup_lambda.lambda_arn

  providers = {
    aws        = aws
    aws.backup = aws.backup
  }
}
