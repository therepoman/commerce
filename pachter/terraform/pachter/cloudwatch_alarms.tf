# Alarms on log scans
resource "aws_cloudwatch_metric_alarm" "logscan_errors_alarm" {
  alarm_name          = "logscan_error"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "15"
  metric_name         = "pachter-logscan-errors"
  namespace           = "pachter"
  period              = "60"
  statistic           = "Sum"
  threshold           = "20"
  alarm_description   = "This alarms when there are too many logscan errors"
}

resource "aws_cloudwatch_metric_alarm" "logscan_panics_alarm" {
  alarm_name          = "logscan_panics"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "pachter-logscan-panics"
  namespace           = "pachter"
  period              = "60"
  statistic           = "Sum"
  threshold           = "1"
  alarm_description   = "This alarms when there is a panic"
}

# Alarms on DLQ
resource "aws_cloudwatch_metric_alarm" "bits_spend_dlq_not_empty" {
  alarm_name          = "bits_spend_dlq_not_empty"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  datapoints_to_alarm = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = "bits-spend-${var.environment_short}-v2-dlq.fifo"
  }

  period             = "300"
  statistic          = "Sum"
  threshold          = "1"
  alarm_description  = "This metric monitors spend order DLQ. It will alarm if there is a message in that queue."
  treat_missing_data = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "bits_transaction_dlq_not_empty" {
  alarm_name          = "bits_transaction_dlq_not_empty"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  datapoints_to_alarm = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = "bits-transaction-${var.environment_short}-v2-deadletter"
  }

  period             = "300"
  statistic          = "Sum"
  threshold          = "1"
  alarm_description  = "This metric monitors transaction DLQ. It will alarm if there is a message in that queue."
  treat_missing_data = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "bits_cheermote_dlq_not_empty" {
  alarm_name          = "bits_cheermote_dlq_not_empty"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  datapoints_to_alarm = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = "cheermote-usage-${var.environment_short}-v2-deadletter"
  }

  period             = "300"
  statistic          = "Sum"
  threshold          = "1"
  alarm_description  = "This metric monitors cheermote DLQ. It will alarm if there is a message in that queue."
  treat_missing_data = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "bits_transaction_audit_dlq_not_empty" {
  alarm_name          = "bits_transaction_audit_dlq_not_empty"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  datapoints_to_alarm = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = "bits-transaction-audit-${var.environment_short}-v2-deadletter"
  }

  period             = "300"
  statistic          = "Sum"
  threshold          = "1"
  alarm_description  = "This metric monitors bits transaction audit DLQ. It will alarm if there is a message in that queue."
  treat_missing_data = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "bits_transaction_deferrer_dlq_not_empty" {
  alarm_name          = "bits_transaction_deferrer_dlq_not_empty"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  datapoints_to_alarm = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = "bits-transaction-deferrer-${var.environment_short}-v2-deadletter"
  }

  period             = "300"
  statistic          = "Sum"
  threshold          = "1"
  alarm_description  = "This metric monitors bits transaction deferrer DLQ. It will alarm if there is a message in that queue."
  treat_missing_data = "notBreaching"
}

# Alarms on APIs
resource "aws_cloudwatch_metric_alarm" "get_spend_order_latency" {
  alarm_name          = "get_spend_order_latency"
  alarm_description   = "Alarms when GetBitsSpendOrder API latency breaches threshold"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "timing.twirp.GetBitsSpendOrder.response"
  namespace           = "pachter"
  period              = "300"
  threshold           = "1"
  extended_statistic  = "p99"
  treat_missing_data  = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "get_bits_amount_for_channel_latency" {
  alarm_name          = "get_bits_amount_for_channel_latency"
  alarm_description   = "Alarms when GetBitsAmountsForChannel API latency breaches threshold"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "timing.twirp.GetBitsAmountsForChannel.response"
  namespace           = "pachter"
  period              = "300"
  threshold           = "1"
  extended_statistic  = "p99"
  treat_missing_data  = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "rds_export_glue_error_queue_not_empty" {
  alarm_name          = "rds_export_glue_error_queue_not_empty"
  alarm_description   = "Alarms when RDS export glue job errors"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  datapoints_to_alarm = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"

  dimensions = {
    QueueName = "${aws_sns_topic.db_export_glue_errors.name}"
  }

  period             = "300"
  statistic          = "Sum"
  threshold          = "1"
  treat_missing_data = "notBreaching"
}

# Alarms on Lambdas
resource "aws_cloudwatch_metric_alarm" "deferred_transaction_poller_lambda_failures" {
  alarm_name          = "deferred_transaction_poller_lambda_failures"
  alarm_description   = "Alarms when DeferredTransactionPoller Lambda fails"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "5"
  datapoints_to_alarm = "2"
  metric_name         = "Errors"
  namespace           = "AWS/Lambda"

  dimensions = {
    FunctionName = "${aws_lambda_function.deferred_transaction_poller.function_name}"
  }

  period              = "60"
  statistic           = "Sum"
  threshold           = "1"
  treat_missing_data  = "notBreaching"
}

# Alarms on StepFunctions
resource "aws_cloudwatch_metric_alarm" "spend_order_resetter_sfn_invocations" {
  alarm_name          = "spend_order_resetter_sfn_invocations"
  alarm_description   = "Alarms when SpendOrderResetter is invoked too many times"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  period              = "8640" # 2.4 hours in seconds
  evaluation_periods  = "10"   # Total of 24 hours
  threshold           = "1"
  datapoints_to_alarm = "10"
  metric_name         = "ExecutionsStarted"
  namespace           = "AWS/States"

  dimensions = {
    StateMachineArn = "${aws_sfn_state_machine.spend_order_resetter.id}"
  }

  statistic           = "Sum"
  treat_missing_data  = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "spend_order_resetter_sfn_failures" {
  alarm_name          = "spend_order_resetter_sfn_failures"
  alarm_description   = "Alarms when SpendOrderResetter fails even for once"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  period              = "60"
  evaluation_periods  = "1"
  threshold           = "1"
  datapoints_to_alarm = "1"
  metric_name         = "ExecutionsFailed"
  namespace           = "AWS/States"

  dimensions = {
    StateMachineArn = "${aws_sfn_state_machine.spend_order_resetter.id}"
  }

  statistic           = "Sum"
  treat_missing_data  = "notBreaching"
}

