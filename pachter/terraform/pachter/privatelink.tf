module "privatelink" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink"

  region      = var.region
  name        = var.name
  environment = var.environment
  alb_dns     = aws_alb.pachter_alb.dns_name

  // TODO: once all callers are on a2z, we can pass the a2z cert here instead. We are
  //   currently setting the a2z cert in alb.tf, and hand jamming it for the nlb.
  cert_arn         = aws_acm_certificate.pachter_ssl_cert.arn
  vpc_id           = var.vpc_id
  subnets          = split(",", var.subnets)
  whitelisted_arns = var.whitelisted_arns_for_privatelink
}

module "privatelink-zone" {
  source      = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-zone"
  name        = "pachter"
  environment = var.environment
}

module "privatelink-cert" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-cert"

  name        = "pachter"
  environment = var.environment
  zone_id     = module.privatelink-zone.zone_id
  alb_dns     = aws_alb.pachter_alb.dns_name
}

// privatelinks with downstreams

module "privatelink-ldap" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint"
  name            = "ldap-a2z"
  endpoint        = "com.amazonaws.vpce.us-west-2.vpce-svc-0437151f68c61b808"
  security_groups = [var.security_group]
  vpc_id          = var.vpc_id
  subnets         = split(",", var.subnets)
  dns             = "ldap.twitch.a2z.com"
}