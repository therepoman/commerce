resource "aws_iam_role" "pachter_ecs_task_execution_role" {
  name = "${var.name}-ecs-task-execution"

  assume_role_policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "${aws_iam_role.pachter_ecr_pusher_role.arn}"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT

}

resource "aws_iam_role_policy_attachment" "pachter_ecs_task_execution_role_attachment" {
  role       = aws_iam_role.pachter_ecs_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role_policy_attachment" "ecs_task_execution_role_cloudwatch_attachment" {
  role       = aws_iam_role.pachter_ecs_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchLogsReadOnlyAccess"
}

resource "aws_iam_role" "pachter_ecs_task_role" {
  name = "${var.name}-ecs-task"

  assume_role_policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "${aws_iam_role.pachter_ecr_pusher_role.arn}"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT

}

resource "aws_iam_role_policy_attachment" "pachter_s2s_policy_attachment" {
  policy_arn = aws_iam_policy.malachai_assume_role_policy.arn
  role       = aws_iam_role.pachter_ecs_task_role.name
}

resource "aws_iam_role_policy_attachment" "pachter_ecs_task_role_dynamo_policy_attachment" {
  role       = aws_iam_role.pachter_ecs_task_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

resource "aws_iam_role_policy_attachment" "pachter_ecs_task_sqs_policy_attachment" {
  role       = aws_iam_role.pachter_ecs_task_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
}

resource "aws_iam_role_policy_attachment" "pachter_ecs_task_sns_policy_attachment" {
  role       = aws_iam_role.pachter_ecs_task_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSNSFullAccess"
}

resource "aws_iam_role_policy_attachment" "pachter_ecs_task_s3_policy_attachment" {
  role       = aws_iam_role.pachter_ecs_task_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_role_policy_attachment" "pachter_ecs_task_firehose_policy_attachment" {
  role       = aws_iam_role.pachter_ecs_task_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonKinesisFirehoseFullAccess"
}

resource "aws_iam_role_policy_attachment" "pachter_ecs_task_elasticache_policy_attachment" {
  role       = aws_iam_role.pachter_ecs_task_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonElastiCacheFullAccess"
}

resource "aws_iam_role_policy_attachment" "pachter_ecs_taks_cloudwatch_policy_attachment" {
  role       = aws_iam_role.pachter_ecs_task_role.name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchFullAccess"
}

resource "aws_iam_role_policy_attachment" "pachter_ecs_taks_rds_policy_attachment" {
  role       = aws_iam_role.pachter_ecs_task_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonRDSDataFullAccess"
}

resource "aws_iam_role_policy_attachment" "pachter_ecs_task_step_function_policy_attachment" {
  role       = aws_iam_role.pachter_ecs_task_role.name
  policy_arn = "arn:aws:iam::aws:policy/AWSStepFunctionsFullAccess"
}

resource "aws_iam_role_policy" "pachter_ecs_secrets_manager_policy" {
  role = aws_iam_role.pachter_ecs_task_role.name

  policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "secretsmanager:GetSecretValue",
        "secretsmanager:GetSecret"
      ],
      "Resource": "${var.rds_secret_arn}"
    }
  ]
}
EOT

}

resource "aws_iam_role" "pachter_ecs_autoscaling_role" {
  name = "${var.name}-ecs-scaler"

  assume_role_policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": { "Service": "application-autoscaling.amazonaws.com"},
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT

}

resource "aws_iam_role_policy" "pachter_ecs_autoscaling_role_policy" {
  name = "${var.name}-ecs-scaler-policy"
  role = aws_iam_role.pachter_ecs_autoscaling_role.id

  policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "application-autoscaling:*",
        "cloudwatch:DescribeAlarms",
        "cloudwatch:PutMetricAlarm",
        "ecs:DescribeServices",
        "ecs:UpdateService"
      ],
      "Resource": "*"
    }
  ]
}
EOT

}

resource "aws_iam_role" "jenkins-assume-role" {
  name = "${var.name}-jenkins-assume-role"

  assume_role_policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::${var.account_id}:user/JenkinsLimitedUser"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT

}

resource "aws_iam_role" "pachter_ecr_pusher_role" {
  name = "${var.name}-ecr-pusher"

  assume_role_policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "${aws_iam_role.jenkins-assume-role.arn}",
          "arn:aws:iam::946879801923:role/Admin"
        ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT

}

resource "aws_iam_role_policy" "pachter_pipeline_builder_push" {
  role = aws_iam_role.jenkins-assume-role.name

  policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Resource": "*",
      "Action": "ecr:*"
    }
  ]
}
EOT

}

resource "aws_iam_role_policy" "pachter_ecr_pusher_role_policy" {
  name = "${var.name}-ecr-pusher"
  role = aws_iam_role.pachter_ecr_pusher_role.id

  policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "iam:PassRole"
      ],
      "Effect": "Allow",
      "Resource": "${aws_iam_role.pachter_ecs_autoscaling_role.arn}"
    },
    {
      "Action": [
        "ecr:*"
      ],
      "Effect": "Allow",
      "Resource": "${aws_ecr_repository.pachter_ecr_repo.arn}"
    },
    {
      "Action": [
        "autoscaling:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOT

}

resource "aws_iam_role" "pachter_ecs_instance_role" {
  name = "${var.name}-ecs-instance-role"

  assume_role_policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT

}

resource "aws_iam_role_policy_attachment" "ecs_instance_role_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
  role       = aws_iam_role.pachter_ecs_instance_role.name
}

resource "aws_iam_policy" "malachai_assume_role_policy" {
  name        = "malachai-assume-role-policy"
  description = "policy necessary for S2S"

  policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Action": "sts:AssumeRole",
    "Resource": "arn:aws:iam::180116294062:role/malachai/*"
  }
}
EOT

}

