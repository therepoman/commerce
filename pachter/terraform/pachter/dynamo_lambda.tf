module "dynamo_backup_lambda" {
  source = "./modules/dynamo_backup_lambda"
  region = var.region
}

module "replica_dynamo_backup_lambda" {
  source = "./modules/dynamo_backup_lambda"
  region = var.backup_region

  providers = {
    aws = aws.backup
  }
}

