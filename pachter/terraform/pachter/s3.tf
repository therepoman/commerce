resource "aws_s3_bucket" "emote_report_bucket" {
  bucket = "bits-emote-usage-monthly-reports-${var.environment_short}"
  acl    = "private"
}

resource "aws_s3_bucket" "monthly_report_bucket" {
  bucket = "bits-monthly-reports-${var.environment_short}"
  acl    = "private"
}

