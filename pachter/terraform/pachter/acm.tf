resource "aws_acm_certificate" "pachter_ssl_cert" {
  domain_name       = "*.${var.environment}.${lower(var.name)}.internal.justin.tv"
  validation_method = "DNS"
}

resource "aws_acm_certificate" "pachter_ssl_cert_2" {
  domain_name       = "${var.environment}.${lower(var.name)}.internal.justin.tv"
  validation_method = "DNS"
}

