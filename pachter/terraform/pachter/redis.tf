resource "aws_elasticache_parameter_group" "pachter_redis_param_group" {
  family = "redis5.0"
  name   = "pachter-redis-parameter-group-${var.environment_short}"

  parameter {
    name  = "maxmemory-policy"
    value = "allkeys-lru"
  }

  parameter {
    name  = "cluster-enabled"
    value = "yes"
  }
}

resource "aws_elasticache_subnet_group" "pachter_redis_subnet_group" {
  name       = "pachter-redis-subnet-group-${var.environment_short}"
  subnet_ids = split(",", var.subnets)
}

resource "aws_security_group" "pachter_redis_security_group" {
  name        = "pachter-redis-security-group-${var.environment_short}"
  vpc_id      = var.vpc_id
  description = "allows communication with redis server"

  ingress {
    from_port   = 6379
    protocol    = "tcp"
    to_port     = 6379
    cidr_blocks = var.private_cidr_blocks
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "pachter-redis-security-group"
  }
}

resource "aws_elasticache_replication_group" "pachter_redis" {
  replication_group_id          = "pachter-redis"
  replication_group_description = "pachter clustered redis"
  automatic_failover_enabled    = true
  engine                        = "redis"
  engine_version                = "5.0"
  port                          = 6379
  node_type                     = var.redis_instance_type
  parameter_group_name          = aws_elasticache_parameter_group.pachter_redis_param_group.name
  subnet_group_name             = aws_elasticache_subnet_group.pachter_redis_subnet_group.name
  security_group_ids            = [aws_security_group.pachter_redis_security_group.id]

  cluster_mode {
    num_node_groups         = var.redis_num_node_groups
    replicas_per_node_group = var.redis_replicas_per_node_group
  }

  lifecycle {
    ignore_changes = [engine_version]
  }
}

