resource "aws_db_subnet_group" "rds_db_subnet_group" {
  name        = "pachter-rds-subnet-group-${var.environment_short}"
  description = "subnet group used by rds, ${var.environment}"
  subnet_ids  = split(",", var.subnets)
}

resource "aws_db_instance" "pachter_db_instance" {
  identifier     = var.rds_instance_identifier
  name           = var.rds_name
  username       = var.rds_username
  password       = var.rds_password
  port           = 5432
  engine         = "postgres"
  engine_version = "10.15"

  instance_class = var.rds_instance_class
  multi_az       = var.rds_multi_az_enabled

  storage_encrypted = false

  # gp2: General Purpose - suitable for a broad range of database workloads.
  # Provides baseline of 3 IOPS/GiB and ability to burst to 3K IOPS

  # io1: Provisioned IOPS SSD Storage - suitable for production appllication
  # that requires fast and consistent I/O performance. Delivers predictable performance,
  # and consistently low latency. Optimized for online transaction processing workloads.
  storage_type = var.rds_storage_type
  iops         = var.rds_iops

  # 100  GB ~ $1,400 /mo
  # 1000 GB ~ $1,600 /mo
  # 5000 GB ~ $2,600 /mo
  allocated_storage               = var.rds_allocated_storage
  backup_retention_period         = var.rds_backup_retention_period
  db_subnet_group_name            = aws_db_subnet_group.rds_db_subnet_group.name
  vpc_security_group_ids          = [var.security_group]
  publicly_accessible             = false
  auto_minor_version_upgrade      = false
  allow_major_version_upgrade     = false
  apply_immediately               = true
  deletion_protection             = true
  enabled_cloudwatch_logs_exports = ["postgresql"]
  performance_insights_enabled    = true

  # The name of the final DB snapshot when this DB instance is deleted
  final_snapshot_identifier = "${var.rds_instance_identifier}-final-snapshot"
  ca_cert_identifier        = "rds-ca-2019"
}

module "rds_copy_snapshot_lambda" {
  source                  = "./modules/rds_snapshot_lambda"
  region                  = var.region
  backup_region           = var.backup_region
  rds_instance_identifier = var.rds_instance_identifier
}

module "scheduled_snapshot_copies" {
  source                       = "./modules/rds_snapshot_backup"
  rds_instance_identifier      = var.rds_instance_identifier
  rds_copy_snapshot_lambda_arn = module.rds_copy_snapshot_lambda.lambda_arn
  region                       = var.region
  backup_region                = var.backup_region
  rds_backup_retention_period  = var.rds_backup_retention_period
}

module "pachter_db_export_via_glue" {
  source                       = "git::git+ssh://git@git.xarth.tv/stats/db-s3-glue.git?ref=f82a5db445fa05d8a5a144fb26cc1c672d916467"
  database_type                = "rds"
  skip_snapshot                = "True"
  use_latest_snapshot          = "True"
  job_name                     = "${var.rds_instance_identifier}-dbexport"
  cluster_name                 = var.rds_instance_identifier
  trigger_schedule             = var.trigger_schedule
  tahoe_max_concurrent_imports = "4" //We have 4 tables, do this in parallel

  table_config = {
    "bits_transactions" = <<EOF
      {
        "dpu_count": 16,
        "worker_type": "G.1X",
        "version": 8,
        "spark_optimization": true,
        "hashfield": "transaction_id",
        "hashpartitions": 80,
        "tahoe_view_name": "bits_transactions_${var.environment}",
        "schema": [
          {"name": "transaction_id", "type": "string", "sensitivity": "sessionid"},
          {"name": "transaction_type", "type": "string"},
          {"name": "requesting_twitch_user_id", "type": "string", "sensitivity": "userid"},
          {"name": "receiving_twitch_user_id", "type": "string", "sensitivity": "userid"},
          {"name": "datetime", "type": "timestamp"},
          {"name": "cheermote_usages", "type": "string"},
          {"name": "quantity", "type": "int"},
          {"name": "bits_type", "type": "string"},
          {"name": "message", "type": "string"},
          {"name": "platform", "type": "string"},
          {"name": "product_id", "type": "string"},
          {"name": "extension_client_id", "type": "string", "sensitivity": "otherid"},
          {"name": "purchase_price_usc", "type": "int"},
          {"name": "sponsored_quantity", "type": "int"},
          {"name": "updated_at", "type": "timestamp"},
          {"name": "update_reason", "type": "string"},
          {"name": "requesting_twitch_user_id_bigint", "type": "bigint", "sensitivity": "userid", "is_derived": true},
          {"name": "receiving_twitch_user_id_bigint", "type": "bigint", "sensitivity": "userid", "is_derived": true}
        ]
      }
EOF

    "bits_spends" = <<EOF
      {
        "dpu_count": 15,
        "worker_type": "G.1X",
        "version": 4,
        "spark_optimization": true,
        "hashfield": "transaction_id",
        "hashpartitions": 48,
        "tahoe_view_name": "bits_spends_${var.environment}",
        "schema": [
          {"name": "transaction_id", "type": "string", "sensitivity": "sessionid"},
          {"name": "reference_transaction_id", "type": "string", "sensitivity": "sessionid"},
          {"name": "requesting_twitch_user_id", "type": "string", "sensitivity": "userid"},
          {"name": "receiving_twitch_user_id", "type": "string", "sensitivity": "userid"},
          {"name": "datetime", "type": "timestamp"},
          {"name": "quantity", "type": "int"},
          {"name": "purchase_price_usc", "type": "int"},
          {"name": "requesting_twitch_user_id_bigint", "type": "bigint", "sensitivity": "userid", "is_derived": true},
          {"name": "receiving_twitch_user_id_bigint", "type": "bigint", "sensitivity": "userid", "is_derived": true}
        ]
      }
EOF

    "cheermote_usages" = <<EOF
      {
        "dpu_count": 15,
        "worker_type": "G.1X",
        "version": 3,
        "spark_optimization": true,
        "hashfield": "transaction_id",
        "hashpartitions": 48,
        "tahoe_view_name": "cheermote_usages_${var.environment}",
        "schema": [
          {"name": "transaction_id", "type": "string", "sensitivity": "sessionid"},
          {"name": "cheermote", "type": "string"},
          {"name": "datetime", "type": "timestamp"},
          {"name": "quantity", "type": "int"},
          {"name": "purchase_price_usc", "type": "int"}
        ]
      }
EOF

    "bits_breakages" = <<EOF
      {
        "dpu_count": 10,
        "worker_type": "Standard",
        "version": 3,
        "spark_optimization": true,
        "hashfield": "reference_transaction_id",
        "hashpartitions": 1,
        "tahoe_view_name": "bits_breakages_${var.environment}",
        "schema": [
          {"name": "reference_transaction_id", "type": "string", "sensitivity": "sessionid"},
          {"name": "twitch_user_id", "type": "string", "sensitivity": "userid"},
          {"name": "breakage_datetime", "type": "timestamp"},
          {"name": "quantity", "type": "int"},
          {"name": "purchase_price_usc", "type": "int"}
        ]
      }
EOF
  }

  spark_cleaning_code = <<CODE
if table_name == 'bits_transactions':
    df = df.withColumn('transaction_id', F.rtrim(F.col('transaction_id')))
    df = df.withColumn('transaction_type', F.rtrim(F.col('transaction_type')))
    df = df.withColumn('requesting_twitch_user_id', F.rtrim(F.col('requesting_twitch_user_id')))
    df = df.withColumn('receiving_twitch_user_id', F.rtrim(F.col('receiving_twitch_user_id')))
    df = df.withColumn('bits_type', F.rtrim(F.col('bits_type')))
    df = df.withColumn('platform', F.rtrim(F.col('platform')))
    df = df.withColumn('product_id', F.rtrim(F.col('product_id')))
    df = df.withColumn('extension_client_id', F.rtrim(F.col('extension_client_id')))
    df = df.withColumn('requesting_twitch_user_id_bigint', F.col('requesting_twitch_user_id').cast('long'))
    df = df.withColumn('receiving_twitch_user_id_bigint', F.col('receiving_twitch_user_id').cast('long'))
    return df
elif table_name == 'bits_spends':
    df = df.withColumn('requesting_twitch_user_id_bigint', F.col('requesting_twitch_user_id').cast('long'))
    df = df.withColumn('receiving_twitch_user_id_bigint', F.col('receiving_twitch_user_id').cast('long'))
    return df
elif table_name == 'bits_breakages':
    df = df.withColumn('twitch_user_id', F.rtrim(F.col('twitch_user_id')))
    return df
elif table_name == 'cheermote_usages':
    df = df.withColumn('transaction_id', F.rtrim(F.col('transaction_id')))
    df = df.withColumn('cheermote', F.rtrim(F.col('cheermote')))
    return df
else:
    return df
CODE

  create_s3_output_bucket    = 1
  s3_output_bucket           = "${var.rds_instance_identifier}-dbexport"
  s3_output_key              = "exports/${var.rds_instance_identifier}"
  create_s3_script_bucket    = 1
  s3_script_bucket           = "${var.rds_instance_identifier}-dbexport-scripts"
  error_sns_topic_name       = aws_sns_topic.db_export_glue_errors.name
  account_number             = var.account_id
  vpc_id                     = var.vpc_id
  subnet_id                  = element(split(",", var.subnets), 0)
  availability_zone          = element(var.availability_zones, 0)
  rds_subnet_group           = aws_db_subnet_group.rds_db_subnet_group.name
  cluster_username           = var.rds_glue_export_job_username
  db_password_parameter_name = aws_ssm_parameter.pachter_export_password.name
  db_password_key_id         = aws_kms_alias.pachter_export.target_key_id
  api_key_parameter_name     = aws_ssm_parameter.tahoe_api_password.name
  api_key_kms_key_id         = aws_kms_alias.tahoe_api.target_key_id
  tahoe_producer_name        = var.tahoe_producer_name
  tahoe_producer_role_arn    = "arn:aws:iam::331582574546:role/producer-${var.tahoe_producer_name}"
}

module "pachter_db_export_via_glue_for_qa" {
  source                       = "git::git+ssh://git@git.xarth.tv/stats/db-s3-glue.git?ref=f82a5db445fa05d8a5a144fb26cc1c672d916467"
  database_type                = "rds"
  skip_snapshot                = "True"
  use_latest_snapshot          = "True"
  job_name                     = "${var.rds_instance_identifier}-dbexport-qa"
  qa_for_job_name              = "${var.rds_instance_identifier}-dbexport"
  cluster_name                 = var.rds_instance_identifier
  trigger_schedule             = var.trigger_schedule
  tahoe_max_concurrent_imports = "4" //We have 4 tables, do this in parallel

table_config = {
"bits_transactions" = <<EOF
      {
        "dpu_count": 16,
        "worker_type": "G.1X",
        "version": 15,
        "spark_optimization": true,
        "hashfield": "transaction_id",
        "hashpartitions": 80,
        "tahoe_view_name": "bits_transactions_${var.environment}",
        "schema": [
          {"name": "transaction_id", "type": "string", "sensitivity": "sessionid"},
          {"name": "transaction_type", "type": "string"},
          {"name": "requesting_twitch_user_id", "type": "string", "sensitivity": "userid"},
          {"name": "receiving_twitch_user_id", "type": "string", "sensitivity": "userid"},
          {"name": "datetime", "type": "timestamp"},
          {"name": "cheermote_usages", "type": "string"},
          {"name": "quantity", "type": "int"},
          {"name": "bits_type", "type": "string"},
          {"name": "message", "type": "string"},
          {"name": "platform", "type": "string"},
          {"name": "product_id", "type": "string"},
          {"name": "extension_client_id", "type": "string", "sensitivity": "otherid"},
          {"name": "purchase_price_usc", "type": "int"},
          {"name": "sponsored_quantity", "type": "int"},
          {"name": "updated_at", "type": "timestamp"},
          {"name": "update_reason", "type": "string"},
          {"name": "requesting_twitch_user_id_bigint", "type": "bigint", "sensitivity": "userid", "is_derived": true},
          {"name": "receiving_twitch_user_id_bigint", "type": "bigint", "sensitivity": "userid", "is_derived": true}
        ]
      }
EOF

"bits_spends" = <<EOF
      {
        "dpu_count": 15,
        "worker_type": "G.1X",
        "version": 11,
        "spark_optimization": true,
        "hashfield": "transaction_id",
        "hashpartitions": 48,
        "tahoe_view_name": "bits_spends_${var.environment}",
        "schema": [
          {"name": "transaction_id", "type": "string", "sensitivity": "sessionid"},
          {"name": "reference_transaction_id", "type": "string", "sensitivity": "sessionid"},
          {"name": "requesting_twitch_user_id", "type": "string", "sensitivity": "userid"},
          {"name": "receiving_twitch_user_id", "type": "string", "sensitivity": "userid"},
          {"name": "datetime", "type": "timestamp"},
          {"name": "quantity", "type": "int"},
          {"name": "purchase_price_usc", "type": "int"},
          {"name": "requesting_twitch_user_id_bigint", "type": "bigint", "sensitivity": "userid", "is_derived": true},
          {"name": "receiving_twitch_user_id_bigint", "type": "bigint", "sensitivity": "userid", "is_derived": true}
        ]
      }
EOF

"cheermote_usages" = <<EOF
      {
        "dpu_count": 15,
        "worker_type": "G.1X",
        "version": 11,
        "spark_optimization": true,
        "hashfield": "transaction_id",
        "hashpartitions": 48,
        "tahoe_view_name": "cheermote_usages_${var.environment}",
        "schema": [
          {"name": "transaction_id", "type": "string", "sensitivity": "sessionid"},
          {"name": "cheermote", "type": "string"},
          {"name": "datetime", "type": "timestamp"},
          {"name": "quantity", "type": "int"},
          {"name": "purchase_price_usc", "type": "int"}
        ]
      }
EOF

"bits_breakages" = <<EOF
      {
        "dpu_count": 10,
        "worker_type": "Standard",
        "version": 10,
        "spark_optimization": true,
        "hashfield": "reference_transaction_id",
        "hashpartitions": 1,
        "tahoe_view_name": "bits_breakages_${var.environment}",
        "schema": [
          {"name": "reference_transaction_id", "type": "string", "sensitivity": "sessionid"},
          {"name": "twitch_user_id", "type": "string", "sensitivity": "userid"},
          {"name": "breakage_datetime", "type": "timestamp"},
          {"name": "quantity", "type": "int"},
          {"name": "purchase_price_usc", "type": "int"}
        ]
      }
EOF
}

spark_cleaning_code = <<CODE
if table_name == 'bits_transactions':
    df = df.withColumn('transaction_id', F.rtrim(F.col('transaction_id')))
    df = df.withColumn('transaction_type', F.rtrim(F.col('transaction_type')))
    df = df.withColumn('requesting_twitch_user_id', F.rtrim(F.col('requesting_twitch_user_id')))
    df = df.withColumn('receiving_twitch_user_id', F.rtrim(F.col('receiving_twitch_user_id')))
    df = df.withColumn('bits_type', F.rtrim(F.col('bits_type')))
    df = df.withColumn('platform', F.rtrim(F.col('platform')))
    df = df.withColumn('product_id', F.rtrim(F.col('product_id')))
    df = df.withColumn('extension_client_id', F.rtrim(F.col('extension_client_id')))
    df = df.withColumn('requesting_twitch_user_id_bigint', F.col('requesting_twitch_user_id').cast('long'))
    df = df.withColumn('receiving_twitch_user_id_bigint', F.col('receiving_twitch_user_id').cast('long'))
    return df
elif table_name == 'bits_spends':
    df = df.withColumn('requesting_twitch_user_id_bigint', F.col('requesting_twitch_user_id').cast('long'))
    df = df.withColumn('receiving_twitch_user_id_bigint', F.col('receiving_twitch_user_id').cast('long'))
    return df
elif table_name == 'bits_breakages':
    df = df.withColumn('twitch_user_id', F.rtrim(F.col('twitch_user_id')))
    return df
elif table_name == 'cheermote_usages':
    df = df.withColumn('transaction_id', F.rtrim(F.col('transaction_id')))
    df = df.withColumn('cheermote', F.rtrim(F.col('cheermote')))
    return df
else:
    return df
CODE

  create_s3_output_bucket    = 0
  s3_output_bucket           = "${var.rds_instance_identifier}-dbexport"
  s3_output_key              = "exports/${var.rds_instance_identifier}"
  create_s3_script_bucket    = 0
  s3_script_bucket           = "${var.rds_instance_identifier}-dbexport-scripts"
  error_sns_topic_name       = aws_sns_topic.db_export_glue_errors.name
  account_number             = var.account_id
  vpc_id                     = var.vpc_id
  subnet_id                  = element(split(",", var.subnets), 0)
  availability_zone          = element(var.availability_zones, 0)
  rds_subnet_group           = aws_db_subnet_group.rds_db_subnet_group.name
  cluster_username           = var.rds_glue_export_job_username
  db_password_parameter_name = aws_ssm_parameter.pachter_export_password.name
  db_password_key_id         = aws_kms_alias.pachter_export.target_key_id
  api_key_parameter_name     = aws_ssm_parameter.tahoe_api_password.name
  api_key_kms_key_id         = aws_kms_alias.tahoe_api.target_key_id
  tahoe_producer_name        = var.tahoe_producer_name
  tahoe_producer_role_arn    = "arn:aws:iam::331582574546:role/producer-${var.tahoe_producer_name}"
  s3_output_kms_key_arn      = "arn:aws:kms:us-west-2:458492755823:key/1e324a58-b403-4134-86b0-5c9ab59a8204"
}

output "s3_kms_key" {
  value = module.pachter_db_export_via_glue.s3_kms_key
}

output "s3_output_bucket" {
  value = module.pachter_db_export_via_glue.s3_output_bucket
}

output "glue_role" {
  value = module.pachter_db_export_via_glue.glue_role
}
