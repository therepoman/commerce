#
# Used for the dbexport job defined in rds.tf
#

resource "aws_kms_key" "pachter_export" {
  description = "${var.rds_instance_identifier} export job database password"
}

resource "aws_kms_alias" "pachter_export" {
  name          = "alias/${var.rds_instance_identifier}/export"
  target_key_id = aws_kms_key.pachter_export.key_id
}

resource "aws_ssm_parameter" "pachter_export_password" {
  name        = "/database/${var.rds_instance_identifier}/${var.rds_glue_export_job_username}/password"
  description = "${var.rds_glue_export_job_username}'s password for ${var.rds_instance_identifier}'"
  type        = "SecureString"
  value       = "CHANGE_ME"
  key_id      = aws_kms_key.pachter_export.key_id

  lifecycle {
    ignore_changes = [value]
  }

  tags = {
    environment = var.environment
  }
}

resource "aws_kms_key" "tahoe_api" {
  description = "${var.rds_instance_identifier} tahoe key"
}

resource "aws_kms_alias" "tahoe_api" {
  name          = "alias/${var.rds_instance_identifier}/tahoe"
  target_key_id = aws_kms_key.tahoe_api.key_id
}

resource "aws_ssm_parameter" "tahoe_api_password" {
  name        = "/tahoe/${var.rds_instance_identifier}/password"
  description = "${var.rds_instance_identifier}'s password for the tahoe api"
  type        = "SecureString"
  value       = "CHANGE_ME"
  key_id      = aws_kms_key.tahoe_api.key_id

  lifecycle {
    ignore_changes = [value]
  }

  tags = {
    environment = var.environment
  }
}

