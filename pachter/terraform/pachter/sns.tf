#
# Used for the dbexport job defined in rds.tf
#

resource "aws_sns_topic" "db_export_glue_errors" {
  name = "${var.rds_instance_identifier}-glue-export-errors"
}

resource "aws_sns_topic_policy" "default" {
  arn    = aws_sns_topic.db_export_glue_errors.arn
  policy = data.aws_iam_policy_document.glue_topic_policy.json
}

data "aws_iam_policy_document" "glue_topic_policy" {
  statement {
    actions = [
      "SNS:Publish",
    ]

    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["events.amazonaws.com"]
    }

    resources = [
      aws_sns_topic.db_export_glue_errors.arn,
    ]

    sid = "TrustCWEToPublishEventsToMyTopic"
  }
}

