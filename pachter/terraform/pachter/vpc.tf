// TODO: move to terracode
resource "aws_vpc_endpoint" "pachter-petozi-private-link" {
  service_name       = var.petozi_vpc_endpoint_service_name
  vpc_id             = var.vpc_id
  vpc_endpoint_type  = "Interface"
  security_group_ids = [var.security_group]
  subnet_ids         = split(",", var.subnets)
}

resource "aws_route53_zone" "petozi_twitch_a2z_com" {
  name    = "petozi.twitch.a2z.com"
  comment = "Private zone for VPC endpoints"

  vpc {
    vpc_id = var.vpc_id
  }
}

resource "aws_route53_record" "main" {
  zone_id = aws_route53_zone.petozi_twitch_a2z_com.id
  name    = var.petozi_dns
  type    = "CNAME"
  ttl     = "300"

  # TF-UPGRADE-TODO: In Terraform v0.10 and earlier, it was sometimes necessary to
  # force an interpolation expression to be interpreted as a list by wrapping it
  # in an extra set of list brackets. That form was supported for compatibility in
  # v0.11, but is no longer supported in Terraform v0.12.
  #
  # If the expression in the following list itself returns a list, remove the
  # brackets to avoid interpretation as a list of lists. If the expression
  # returns a single list item then leave it as-is and remove this TODO comment.
  records = [
    aws_vpc_endpoint.pachter-petozi-private-link.dns_entry[0]["dns_name"],
  ]
}

module "privatelink-payday" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-service?ref=980609112b9b584795efc720221af72b81e928ef"

  name            = "payday"
  zone            = var.payday_private_zone
  dns             = var.payday_dns
  endpoint        = var.payday_vpc_endpoint_service_name
  environment     = var.environment
  security_groups = [var.security_group]
  subnets         = split(",", var.subnets)
  vpc_id          = var.vpc_id
}

module "privatelink-pachinko" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-service?ref=980609112b9b584795efc720221af72b81e928ef"

  name            = "pachinko"
  dns             = var.pachinko_dns
  zone            = var.pachinko_private_zone
  endpoint        = var.pachinko_vpc_endpoint_service_name
  environment     = var.environment
  security_groups = [var.security_group]
  subnets         = split(",", var.subnets)
  vpc_id          = var.vpc_id
}

