# API alarms
module "get_bits_amounts_for_channel_availability_high_sev" {
  source                     = "./modules/api_availability"
  api_name                   = "GetBitsAmountsForChannel"
  environment                = var.environment_short
  low_watermark              = "0.99"
  traffic_low_watermark      = "100"
  requests_metric_name_5xx   = "counter.twirp.status_codes.GetBitsAmountsForChannel.500"
  requests_metric_name_total = "counter.twirp.GetBitsAmountsForChannel.requests"
}

resource "aws_cloudwatch_metric_alarm" "get_bits_amounts_for_channel_high_sev" {
  count                     = 1
  alarm_name                = "${var.environment}-get-bits-amounts-for-channel-high-sev"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetBitsAmountsForChannel.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.3"
  unit                      = "Seconds"
  alarm_description         = "GetBitsAmountsForChannel latency is higher"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment_short
    Substage = "primary"
  }
}

module "GetBitsSpendOrder_availability_high_sev" {
  source                     = "./modules/api_availability"
  api_name                   = "GetBitsSpendOrder"
  environment                = var.environment_short
  low_watermark              = "0.99"
  traffic_low_watermark      = "100"
  requests_metric_name_5xx   = "counter.twirp.status_codes.GetBitsSpendOrder.500"
  requests_metric_name_total = "counter.twirp.GetBitsSpendOrder.requests"
}

resource "aws_cloudwatch_metric_alarm" "get_bits_spend_order_latency_high_sev" {
  count                     = 1
  alarm_name                = "${var.environment}-get-bits-spend-order-latency-high-sev"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetBitsSpendOrder.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.3"
  unit                      = "Seconds"
  alarm_description         = "GetBitsSpendOrder latency is higher"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment_short
    Substage = "primary"
  }
}
