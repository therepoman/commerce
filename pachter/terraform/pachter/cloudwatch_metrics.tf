resource "aws_cloudwatch_log_metric_filter" "logscan_errors" {
  log_group_name = aws_cloudwatch_log_group.pachter_cw_logs.name
  name           = "${var.name}-logscan-errors-filter"
  pattern        = "level=error"

  metric_transformation {
    name          = "${var.name}-logscan-errors"
    namespace     = "pachter"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "logscan_panics" {
  log_group_name = aws_cloudwatch_log_group.pachter_cw_logs.name
  name           = "${var.name}-logscan-panics-filter"
  pattern        = "?\"panic:\" ?panic.go"

  metric_transformation {
    name          = "${var.name}-logscan-panics"
    namespace     = "pachter"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "logscan_warnings" {
  log_group_name = aws_cloudwatch_log_group.pachter_cw_logs.name
  name           = "${var.name}-logscan-warnings-filter"
  pattern        = "level=warn"

  metric_transformation {
    name          = "${var.name}-logscan-warnings"
    namespace     = "pachter"
    value         = "1"
    default_value = "0"
  }
}

