######################## Resetter Lambda ########################

resource "aws_iam_role" "bits_spend_order_resetter_role" {
  name = "bits-spend-order-resetter-lambda-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_iam_role_policy_attachment" "bits_spend_order_resetter_allow_sqs" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
  role       = aws_iam_role.bits_spend_order_resetter_role.name
}

resource "aws_iam_role_policy_attachment" "bits_spend_order_resetter_allow_ec2" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
  role       = aws_iam_role.bits_spend_order_resetter_role.name
}

resource "aws_iam_role_policy_attachment" "bits_spend_order_resetter_allow_dynamo" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
  role       = aws_iam_role.bits_spend_order_resetter_role.name
}

resource "aws_iam_role_policy_attachment" "bits_spend_order_resetter_allow_redis" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonElastiCacheFullAccess"
  role       = aws_iam_role.bits_spend_order_resetter_role.name
}

resource "aws_iam_role_policy_attachment" "bits_spend_order_resetter_allow_rds" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonRDSFullAccess"
  role       = aws_iam_role.bits_spend_order_resetter_role.name
}

resource "aws_iam_role_policy_attachment" "bits_spend_order_resetter_allow_secrets_manager" {
  policy_arn = "arn:aws:iam::aws:policy/SecretsManagerReadWrite"
  role       = aws_iam_role.bits_spend_order_resetter_role.name
}

resource "aws_iam_role_policy" "bits_spend_order_resetter_policy" {
  policy = data.aws_iam_policy_document.bits_spend_order_resetter_policy_doc.json
  role   = aws_iam_role.bits_spend_order_resetter_role.name
}

data "aws_iam_policy_document" "bits_spend_order_resetter_policy_doc" {
  statement {
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams",
      "cloudwatch:PutMetricData",
    ]

    resources = ["*"]
  }
}

resource "aws_lambda_function" "bits_spend_order_resetter" {
  function_name    = "BitsSpendOrderResetter"
  handler          = "main"
  role             = aws_iam_role.bits_spend_order_resetter_role.arn
  runtime          = "go1.x"
  filename         = "${path.module}/bits_spend_order_resetter_lambda.zip"
  source_code_hash = filebase64sha256("${path.module}/bits_spend_order_resetter_lambda.zip")
  timeout          = 900  # Lambda allows max of 15 minutes (900 seconds) timeouts
  memory_size      = 1024 # In MB. Default 128 MB, max 3008 MB

  vpc_config {
    security_group_ids = split(",", var.security_group)
    subnet_ids         = split(",", var.subnets)
  }

  environment {
    variables = {
      LAMBDA_ENVIRONMENT = var.environment_short
    }
  }
}

resource "aws_lambda_function" "bits_spend_order_targeted_resetter" {
  function_name    = "BitsSpendOrderTargetedResetter"
  handler          = "main"
  role             = aws_iam_role.bits_spend_order_resetter_role.arn
  runtime          = "go1.x"
  filename         = "${path.module}/bits_spend_order_targeted_resetter_lambda.zip"
  source_code_hash = filebase64sha256("${path.module}/bits_spend_order_targeted_resetter_lambda.zip")
  timeout          = 900  # Lambda allows max of 15 minutes (900 seconds) timeouts
  memory_size      = 2048 # In MB. Default 128 MB, max 3008 MB

  vpc_config {
    security_group_ids = split(",", var.security_group)
    subnet_ids         = split(",", var.subnets)
  }

  environment {
    variables = {
      LAMBDA_ENVIRONMENT = var.environment_short
    }
  }
}

resource "aws_lambda_permission" "spend_order_resetter_allow_cw_event" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.bits_spend_order_resetter.function_name
  principal     = "events.amazonaws.com"
  statement_id  = "AllowExecutionFromCloudWatch"
}

######################## SpendOrderDLQReader Lambda ########################

resource "aws_iam_role" "bits_spend_order_dlq_reader_role" {
  name = "bits-spend-order-dlq-reader-lambda-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_iam_role_policy_attachment" "bits_spend_order_dlq_reader_execution_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
  role       = aws_iam_role.bits_spend_order_dlq_reader_role.name
}

resource "aws_iam_role_policy_attachment" "bits_spend_order_dlq_reader_allow_sqs" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
  role       = aws_iam_role.bits_spend_order_dlq_reader_role.name
}

resource "aws_iam_role_policy" "bits_spend_order_dlq_reader_policy" {
  policy = data.aws_iam_policy_document.bits_spend_order_dlq_reader_policy_doc.json
  role   = aws_iam_role.bits_spend_order_dlq_reader_role.name
}

data "aws_iam_policy_document" "bits_spend_order_dlq_reader_policy_doc" {
  statement {
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams",
      "cloudwatch:PutMetricData",
    ]

    resources = ["*"]
  }
}

resource "aws_lambda_function" "spend_order_dlq_reader" {
  function_name    = "SpendOrderDLQReader"
  handler          = "main"
  role             = aws_iam_role.bits_spend_order_dlq_reader_role.arn
  runtime          = "go1.x"
  filename         = "${path.module}/spend_order_dlq_reader_lambda.zip"
  source_code_hash = filebase64sha256("${path.module}/spend_order_dlq_reader_lambda.zip")
  timeout          = 60   # Lambda allows max of 1 minutes (60 seconds) timeouts
  memory_size      = 1024 # In MB. Default 128 MB, max 3008 MB

  vpc_config {
    security_group_ids = split(",", var.security_group)
    subnet_ids         = split(",", var.subnets)
  }

  environment {
    variables = {
      LAMBDA_ENVIRONMENT = var.environment_short
    }
  }
}

######################## IsSpendOrderQueueEmpty Lambda ########################

resource "aws_iam_role" "is_spend_order_queue_empty_role" {
  name = "is-spend-order-queue-empty-lambda-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_iam_role_policy_attachment" "is_spend_order_queue_empty_execution_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
  role       = aws_iam_role.is_spend_order_queue_empty_role.name
}

resource "aws_iam_role_policy_attachment" "is_spend_order_queue_empty_allow_sqs" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
  role       = aws_iam_role.is_spend_order_queue_empty_role.name
}

resource "aws_iam_role_policy" "is_spend_order_queue_empty_policy" {
  policy = data.aws_iam_policy_document.is_spend_order_queue_empty_policy_doc.json
  role   = aws_iam_role.is_spend_order_queue_empty_role.name
}

data "aws_iam_policy_document" "is_spend_order_queue_empty_policy_doc" {
  statement {
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams",
      "cloudwatch:PutMetricData",
    ]

    resources = ["*"]
  }
}

resource "aws_lambda_function" "is_spend_order_queue_empty" {
  function_name    = "IsSpendOrderQueueEmpty"
  handler          = "main"
  role             = aws_iam_role.is_spend_order_queue_empty_role.arn
  runtime          = "go1.x"
  filename         = "${path.module}/is_spend_order_queue_empty_lambda.zip"
  source_code_hash = filebase64sha256("${path.module}/is_spend_order_queue_empty_lambda.zip")
  timeout          = 60   # Lambda allows max of 1 minutes (60 seconds) timeouts
  memory_size      = 1024 # In MB. Default 128 MB, max 3008 MB

  vpc_config {
    security_group_ids = split(",", var.security_group)
    subnet_ids         = split(",", var.subnets)
  }

  environment {
    variables = {
      LAMBDA_ENVIRONMENT = var.environment_short
    }
  }
}

######################## Poller Lambda v2 ########################

resource "aws_iam_role" "deferred_transaction_poller_role" {
  name = "deferred-transaction-poller-lambda-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_iam_role_policy_attachment" "deferred_transaction_poller_allow_sqs" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
  role       = aws_iam_role.deferred_transaction_poller_role.name
}

resource "aws_iam_role_policy_attachment" "deferred_transaction_poller_allow_ec2" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
  role       = aws_iam_role.deferred_transaction_poller_role.name
}

resource "aws_iam_role_policy_attachment" "deferred_transaction_poller_allow_redis" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonElastiCacheFullAccess"
  role       = aws_iam_role.deferred_transaction_poller_role.name
}

resource "aws_iam_role_policy" "deferred_transaction_poller_policy" {
  policy = data.aws_iam_policy_document.deferred_transaction_poller_policy_doc.json
  role   = aws_iam_role.deferred_transaction_poller_role.name
}

data "aws_iam_policy_document" "deferred_transaction_poller_policy_doc" {
  statement {
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams",
      "cloudwatch:PutMetricData",
    ]

    resources = ["*"]
  }
}

resource "aws_lambda_function" "deferred_transaction_poller" {
  function_name                  = "DeferredTransactionPoller"
  handler                        = "main"
  role                           = aws_iam_role.deferred_transaction_poller_role.arn
  runtime                        = "go1.x"
  filename                       = "${path.module}/deferred_transaction_poller_lambda.zip"
  source_code_hash               = filebase64sha256("${path.module}/deferred_transaction_poller_lambda.zip")
  timeout                        = 900  # Lambda allows max of 15 minutes (900 seconds) timeouts
  memory_size                    = 2048 # In MB. Default 128 MB, max 3008 MB
  reserved_concurrent_executions = 1

  vpc_config {
    security_group_ids = split(",", var.security_group)
    subnet_ids         = split(",", var.subnets)
  }

  environment {
    variables = {
      LAMBDA_ENVIRONMENT = var.environment_short
    }
  }
}

resource "aws_cloudwatch_event_rule" "deferred_transaction_poller_lambda_invocation_rule" {
  name                = "deferred-transaction-poller-periodical-invocation"
  description         = "Invocate the DeferredTransactionPoller Lambda function every minute"
  schedule_expression = "rate(1 minute)"
}

resource "aws_cloudwatch_event_target" "deferred_transaction_poller_lambda_invocation_target" {
  target_id = "deferred-transaction-poller-lambda-invocation-target"
  rule      = aws_cloudwatch_event_rule.deferred_transaction_poller_lambda_invocation_rule.name
  arn       = aws_lambda_function.deferred_transaction_poller.arn
}

resource "aws_lambda_permission" "deferred_transaction_poller_allow_cw_event" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.deferred_transaction_poller.function_name
  principal     = "events.amazonaws.com"
  statement_id  = "AllowExecutionFromCloudWatch"
}
