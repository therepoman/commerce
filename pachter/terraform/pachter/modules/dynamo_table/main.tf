variable "name" {
  description = "the name of the table"
}

variable "hash_key" {
  description = "the hash key of the table"
}

variable "range_key" {
  default = ""
}

variable "attributes" {
  type        = list
  description = "the attributes of the table, including the hash key"
}

variable "read_capacity" {
  description = "the minimum provisioned read capacity"
}

variable "write_capacity" {
  description = "the minimum provisioned write capacity"
}

variable "autoscaling_role" {
}

variable "dynamo_backup_lambda_arn" {
}

variable "region" {
}

variable "ttl_enabled" {
  default = false
}

variable "ttl_attribute_name" {
  default = ""
}

resource "aws_dynamodb_table" "dynamo_table" {
  name           = var.name
  hash_key       = var.hash_key
  range_key      = var.range_key
  read_capacity  = var.read_capacity
  write_capacity = var.write_capacity
  dynamic "attribute" {
    for_each = var.attributes
    content {
      # TF-UPGRADE-TODO: The automatic upgrade tool can't predict
      # which keys might be set in maps assigned here, so it has
      # produced a comprehensive set here. Consider simplifying
      # this after confirming which keys can be set in practice.

      name = attribute.value.name
      type = attribute.value.type
    }
  }

  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  point_in_time_recovery {
    enabled = "true"
  }

  lifecycle {
    ignore_changes = [
      read_capacity,
      write_capacity,
    ]
  }

  ttl {
    enabled        = var.ttl_enabled
    attribute_name = var.ttl_attribute_name
  }
}

output "table_name" {
  value = aws_dynamodb_table.dynamo_table.name
}

module "autoscaling" {
  source            = "../dynamo_autoscaling"
  table_name        = var.name
  min_read_capacity = var.read_capacity
  autoscaling_role  = var.autoscaling_role
  region            = var.region
}

module "scheduled_backups" {
  source                   = "../dynamo_backup"
  table_name               = var.name
  dynamo_backup_lambda_arn = var.dynamo_backup_lambda_arn
  region                   = var.region
}

