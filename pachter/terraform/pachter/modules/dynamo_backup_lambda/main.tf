variable "region" {
  description = "region"
}

resource "aws_iam_role" "dynamo_backup_lambda" {
  name = "dynamo_${var.region}_backup_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_lambda_function" "dynamo_backup_lambda" {
  function_name    = "dynamo_${var.region}_backup"
  filename         = "${path.module}/deployment.zip"
  source_code_hash = filebase64sha256("${path.module}/deployment.zip")
  role             = aws_iam_role.dynamo_backup_lambda.arn
  handler          = "main"
  runtime          = "go1.x"
}

output "lambda_arn" {
  value = aws_lambda_function.dynamo_backup_lambda.arn
}

resource "aws_lambda_permission" "allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.dynamo_backup_lambda.function_name
  principal     = "events.amazonaws.com"
}

resource "aws_iam_policy" "dynamo_manage_backups_policy" {
  name        = "dynamo_${var.region}_manage_backups_policy"
  description = "manage ${var.region} dynamo table backups"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "dynamodb:CreateBackup",
        "dynamodb:ListBackups",
        "dynamodb:DeleteBackup"
      ],
      "Resource": "*"
    }
  ]
}
EOF

}

resource "aws_iam_policy_attachment" "dynamo_manage_backups_policy_attachment" {
  name       = "dynamo_${var.region}_manage_backups_policy_attachment"
  policy_arn = aws_iam_policy.dynamo_manage_backups_policy.arn
  roles      = [aws_iam_role.dynamo_backup_lambda.name]
}

resource "aws_iam_policy" "dynamo_backups_write_cloudwatch_logs_policy" {
  name        = "dynamo_${var.region}_backups_write_cloudwatch_logs_policy"
  description = "Write logs to cloudwatch"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:logs:*:*:*"
    }
  ]
}
EOF

}

resource "aws_iam_policy_attachment" "dynamo_backups_write_cloudwatch_logs_policy_attachment" {
  name       = "dynamo_${var.region}_backups_write_cloudwatch_logs_policy_attachment"
  policy_arn = aws_iam_policy.dynamo_backups_write_cloudwatch_logs_policy.arn
  roles      = [aws_iam_role.dynamo_backup_lambda.name]
}

