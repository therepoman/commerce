variable "environment" {}

variable "api_name" {
  description = "Name of API"
}

variable "low_watermark" {
  description = "0-100. Dropping below this rate will trigger alarm."
}

variable "requests_metric_name_total" {
  description = "Name of metric representing total requests to a single API."
}

variable "requests_metric_name_5xx" {
  description = "Name of metric representing total 5xx requests to a single API."
}

variable "traffic_low_watermark" {
  description = "When requests_metric_name_total is below this value in a 300s period, we will consider availability to be 100% and not alarm."
  default     = 50
}