variable "region" {
  description = "region"
}

variable "backup_region" {
  description = "backup region"
}

variable "rds_instance_identifier" {
  description = "database name for the RDS instance"
}

resource "aws_iam_role" "rds_copy_latest_snapshot_x_region_lambda" {
  name = "rds_copy_latest_snapshot_x_region_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_lambda_function" "rds_copy_latest_snapshot_x_region_lambda" {
  function_name    = "rds_copy_latest_snapshot_x_region"
  filename         = "${path.module}/deployment.zip"
  source_code_hash = filebase64sha256("${path.module}/deployment.zip")
  role             = aws_iam_role.rds_copy_latest_snapshot_x_region_lambda.arn
  handler          = "main"
  runtime          = "go1.x"
}

output "lambda_arn" {
  value = aws_lambda_function.rds_copy_latest_snapshot_x_region_lambda.arn
}

resource "aws_lambda_permission" "allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.rds_copy_latest_snapshot_x_region_lambda.function_name
  principal     = "events.amazonaws.com"
}

resource "aws_iam_policy" "rds_manage_snapshots_policy" {
  name        = "rds_manage_snapshots_policy"
  description = "manage RDS table snapshots"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "rds:CopyDBSnapshot",
        "rds:DeleteDBSnapshot",
        "rds:DescribeDBSnapshots"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF

}

resource "aws_iam_policy_attachment" "rds_manage_snapshot_policy_attachment" {
  name       = "rds_manage_snapshot_policy_attachment"
  policy_arn = aws_iam_policy.rds_manage_snapshots_policy.arn
  roles      = [aws_iam_role.rds_copy_latest_snapshot_x_region_lambda.name]
}

resource "aws_iam_policy" "rds_copy_latest_snapshot_write_cloudwatch_logs_policy" {
  name        = "rds_copy_latest_snapshot_write_cloudwatch_logs_policy"
  description = "Write logs to cloudwatch"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:logs:*:*:*"
    }
  ]
}
EOF

}

resource "aws_iam_policy_attachment" "rds_copy_latest_snapshot_write_cloudwatch_logs_policy_attachment" {
  name       = "rds_copy_latest_snapshot_write_cloudwatch_logs_policy_attachment"
  policy_arn = aws_iam_policy.rds_copy_latest_snapshot_write_cloudwatch_logs_policy.arn
  roles      = [aws_iam_role.rds_copy_latest_snapshot_x_region_lambda.name]
}

