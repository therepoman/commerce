variable "name" {
  description = "the name of the table"
}

provider "aws" {
}

provider "aws" {
  alias = "backup"
}

variable "hash_key" {
  description = "the hash key of the table"
}

variable "range_key" {
  default = ""
}

variable "attributes" {
  type        = list
  description = "the attributes of the table, including the hash key"
}

variable "read_capacity" {
  description = "the minimum provisioned read capacity"
}

variable "write_capacity" {
  description = "the minimum provisioned write capacity"
}

variable "region" {
  description = "the AWS region to create resources"
}

variable "backup_region" {
  description = "the AWS region to create backup resources"
}

variable "autoscaling_role" {
}

variable "dynamo_backup_lambda_arn" {
}

variable "replica_dynamo_backup_lambda_arn" {
}

module "primary_table" {
  source         = "../dynamo_table"
  name           = var.name
  hash_key       = var.hash_key
  range_key      = var.range_key
  read_capacity  = var.read_capacity
  write_capacity = var.write_capacity
  attributes     = var.attributes

  autoscaling_role         = var.autoscaling_role
  dynamo_backup_lambda_arn = var.dynamo_backup_lambda_arn

  region = var.region
}

module "replica_table" {
  source         = "../dynamo_table"
  name           = var.name
  hash_key       = var.hash_key
  range_key      = var.range_key
  read_capacity  = var.read_capacity
  write_capacity = var.write_capacity
  attributes     = var.attributes

  autoscaling_role         = var.autoscaling_role
  dynamo_backup_lambda_arn = var.replica_dynamo_backup_lambda_arn
  region                   = var.backup_region

  providers = {
    aws = aws.backup
  }
}

resource "aws_dynamodb_global_table" "gloabl_table" {
  depends_on = [
    module.primary_table,
    module.replica_table,
  ]

  name = var.name

  replica {
    region_name = var.region
  }

  replica {
    region_name = var.backup_region
  }
}

