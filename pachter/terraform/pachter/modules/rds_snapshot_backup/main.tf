variable "rds_instance_identifier" {
}

variable "rds_copy_snapshot_lambda_arn" {
}

variable "rds_backup_retention_period" {
}

variable "region" {
}

variable "backup_region" {
}

resource "aws_cloudwatch_event_rule" "rds_copy_snapshot_event_daily_rule" {
  name                = "rds-snapshot-copy-daily-${var.rds_instance_identifier}"
  description         = "RDS cross-region snapshot copy daily event rule for database ${var.rds_instance_identifier}"
  schedule_expression = "rate(1 day)"
}

resource "aws_cloudwatch_event_target" "rds_copy_snapshot_event_daily_target" {
  rule      = aws_cloudwatch_event_rule.rds_copy_snapshot_event_daily_rule.name
  target_id = "rds-snapshot-copy-daily-${var.rds_instance_identifier}"
  arn       = var.rds_copy_snapshot_lambda_arn

  input = <<EOF
{
  "db_name"           : "${var.rds_instance_identifier}",
  "src_region"        : "${var.region}",
  "dst_region"        : "${var.backup_region}",
  "retention_days"    : ${var.rds_backup_retention_period}
}
EOF

}

