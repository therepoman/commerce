resource "aws_route" "external_payday_peering" {
  route_table_id            = var.route_table_external_id
  destination_cidr_block    = var.payday_cidr_block
  vpc_peering_connection_id = var.pachter_payday_vpc_peering_id
}

resource "aws_route" "internal_a_payday_peering" {
  route_table_id            = var.route_table_internal_a_id
  destination_cidr_block    = var.payday_cidr_block
  vpc_peering_connection_id = var.pachter_payday_vpc_peering_id
}

resource "aws_route" "internal_b_payday_peering" {
  route_table_id            = var.route_table_internal_b_id
  destination_cidr_block    = var.payday_cidr_block
  vpc_peering_connection_id = var.pachter_payday_vpc_peering_id
}

resource "aws_route" "internal_c_payday_peering" {
  route_table_id            = var.route_table_internal_c_id
  destination_cidr_block    = var.payday_cidr_block
  vpc_peering_connection_id = var.pachter_payday_vpc_peering_id
}

