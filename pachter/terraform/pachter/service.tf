resource "aws_cloudwatch_log_group" "pachter_cw_logs" {
  name              = "${var.name}-ECSLogGroup"
  retention_in_days = 90
}

resource "aws_ecs_cluster" "pachter_ecs_cluster" {
  name = "${lower(var.name)}-ecs-cluster"
}

data "template_file" "pachter_container_def" {
  template = file("${path.module}/container_defs.json")

  vars = {
    name       = var.name
    account_id = var.account_id
    region     = var.region
    log_group  = aws_cloudwatch_log_group.pachter_cw_logs.name
    env        = var.environment_short
  }
}

resource "aws_ecs_task_definition" "pachter_ecs_task" {
  family                   = var.name
  network_mode             = "awsvpc"
  execution_role_arn       = aws_iam_role.pachter_ecs_task_execution_role.arn
  task_role_arn            = aws_iam_role.pachter_ecs_task_role.arn
  requires_compatibilities = ["FARGATE", "EC2"]
  cpu                      = var.ecs_task_cpu
  memory                   = var.ecs_task_memory

  lifecycle {
    create_before_destroy = true
  }

  container_definitions = data.template_file.pachter_container_def.rendered
}

resource "aws_ecs_service" "pachter_ecs_service" {
  desired_count                      = 1
  name                               = "${var.name}-service"
  task_definition                    = aws_ecs_task_definition.pachter_ecs_task.arn
  cluster                            = aws_ecs_cluster.pachter_ecs_cluster.arn
  deployment_minimum_healthy_percent = 100
  launch_type                        = "FARGATE"
  platform_version                   = "1.3.0"
  health_check_grace_period_seconds  = "60"

  load_balancer {
    target_group_arn = aws_alb_target_group.pachter_alb_target_group.arn
    container_name   = var.name
    container_port   = 8080
  }

  lifecycle {
    create_before_destroy = true
    ignore_changes = [
      desired_count,
      task_definition,
    ]
  }

  network_configuration {
    subnets          = split(",", var.subnets)
    security_groups  = [var.security_group, aws_security_group.pachter_security_group.id]
    assign_public_ip = false
  }
}

resource "aws_appautoscaling_target" "pachter_ecs_app_autoscaling_target" {
  max_capacity       = var.max_count
  min_capacity       = var.min_count
  resource_id        = "service/${aws_ecs_cluster.pachter_ecs_cluster.name}/${aws_ecs_service.pachter_ecs_service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
  role_arn           = aws_iam_role.pachter_ecs_autoscaling_role.arn
}

resource "aws_appautoscaling_policy" "pachter_ecs_app_autoscaling_policy" {
  name        = "${var.name}-ecs-tracker"
  policy_type = "TargetTrackingScaling"

  target_tracking_scaling_policy_configuration {
    scale_in_cooldown  = 60
    scale_out_cooldown = 60
    target_value       = 50

    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
  }

  resource_id        = aws_appautoscaling_target.pachter_ecs_app_autoscaling_target.resource_id
  scalable_dimension = aws_appautoscaling_target.pachter_ecs_app_autoscaling_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.pachter_ecs_app_autoscaling_target.service_namespace
}

