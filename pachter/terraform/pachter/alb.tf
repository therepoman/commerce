resource "aws_security_group" "pachter_security_group" {
  name        = "${var.name}-alb"
  description = "controls access to the ALB"
  vpc_id      = var.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = "443"
    to_port     = "443"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_alb" "pachter_alb" {
  name               = "${var.name}-alb"
  internal           = true
  load_balancer_type = "application"
  security_groups    = [var.security_group, aws_security_group.pachter_security_group.id]
  subnets            = split(",", var.subnets)
  idle_timeout       = 3600 //60 minutes, so that we can wait for the mega/chungus report.

  enable_deletion_protection = true

  tags = {
    Environment = var.environment
  }
}

resource "aws_alb_listener" "pachter_alb_listener" {
  load_balancer_arn = aws_alb.pachter_alb.arn
  protocol          = "HTTPS"
  port              = 443
  certificate_arn   = aws_acm_certificate.pachter_ssl_cert.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.pachter_alb_target_group.arn
  }
}

resource "aws_alb_listener" "pachter_alb_listener_nossl" {
  load_balancer_arn = aws_alb.pachter_alb.arn
  protocol          = "HTTP"
  port              = 80

  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.pachter_alb_target_group.arn
  }
}

resource "aws_alb_listener_certificate" "alb_listener_cert" {
  certificate_arn = aws_acm_certificate.pachter_ssl_cert.arn
  listener_arn    = aws_alb_listener.pachter_alb_listener.arn
}

resource "aws_alb_listener_certificate" "alb_listener_cert_2" {
  certificate_arn = aws_acm_certificate.pachter_ssl_cert_2.arn
  listener_arn    = aws_alb_listener.pachter_alb_listener.arn
}

// TODO: This will not be needed once we pass the a2z cert to module.privatelink
resource "aws_alb_listener_certificate" "alb_listener_cert_a2z" {
  certificate_arn = module.privatelink-cert.arn
  listener_arn    = aws_alb_listener.pachter_alb_listener.arn
}

resource "aws_alb_listener_rule" "alb_listener_rule" {
  listener_arn = aws_alb_listener.pachter_alb_listener.arn

  condition {
    path_pattern {
      values = ["/*"]
    }
  }

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.pachter_alb_target_group.arn
  }
}

resource "aws_alb_target_group" "pachter_alb_target_group" {
  name                 = "${var.name}-alb-target-group"
  target_type          = "ip"
  port                 = 1
  protocol             = "HTTP"
  vpc_id               = var.vpc_id
  deregistration_delay = 20

  health_check {
    path = "/ping"
  }
}

