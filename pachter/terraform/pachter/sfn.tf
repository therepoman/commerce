resource "aws_iam_role" "step_functions_role" {
  name = "pachter_step_functions_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "states.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "step_functions_policy" {
  name        = "pachter-StepFunctionsPolicy"
  description = "Step function access to invoke Pachter Lambda functions"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "lambda:InvokeFunction",
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "step-functions-role-policy-attach" {
  role       = "${aws_iam_role.step_functions_role.name}"
  policy_arn = "${aws_iam_policy.step_functions_policy.arn}"
}

resource "aws_sfn_state_machine" "spend_order_resetter" {
  name     = "SpendOrderResetter"
  role_arn = "${aws_iam_role.step_functions_role.arn}"

  definition = <<EOF
  {
    "Comment": "Reads all messages in spend order DLQ, dedupes on userID, and resets spend order for each",
    "StartAt": "AccumulateMessagesFromDLQ",
    "States": {
      "FailState": {
        "Comment": "Failed execution",
        "Type": "Fail"
      },
      "SuccessState": {
        "Type": "Succeed"
      },
      "AccumulateMessagesFromDLQ": {
        "Type": "Task",
        "Next": "ChoiceStateShouldReset",
        "Resource": "${aws_lambda_function.spend_order_dlq_reader.arn}",
        "Catch": [{
          "ErrorEquals": ["States.ALL"],
          "Next": "FailState",
          "ResultPath": "$.error"
        }],
        "Retry": [${local.default_retry}]
      },
      "ChoiceStateShouldReset": {
        "Type": "Choice",
        "Default": "FailState",
        "Choices": [
          {
            "Variable": "$.dlqWasEmpty",
            "BooleanEquals": true,
            "Next": "ResetSpendOrderAll"
          },
          {
            "Variable": "$.dlqWasEmpty",
            "BooleanEquals": false,
            "Next": "AccumulateMessagesFromDLQ"
          }
        ]
      },
      "ResetSpendOrderAll": {
        "Type": "Map",
        "ItemsPath": "$.users",
        "MaxConcurrency": 1,
        "ResultPath": "$.output",
        "Next": "SuccessState",
        "Parameters": {
          "user.$": "$$.Map.Item.Value"
        },
        "Iterator": {
          "StartAt": "AwaitEmptySpendOrderQueue",
          "States": {
            "AwaitEmptySpendOrderQueueFailState": {
              "Comment": "Failed AwaitEmptySpendOrderQueue",
              "Type": "Fail"
            },
            "ResetSpendOrderFailState": {
              "Comment": "Failed ResetSpendOrder",
              "Type": "Fail"
            },
            "AwaitEmptySpendOrderQueue": {
              "Type": "Task",
              "Next": "ResetSpendOrder",
              "Resource": "${aws_lambda_function.is_spend_order_queue_empty.arn}",
              "ResultPath": "$.awaitEmptySpendOrderQueueOuput",
              "Catch": [{
                "ErrorEquals": ["States.ALL"],
                "Next": "AwaitEmptySpendOrderQueueFailState",
                "ResultPath": "$.error"
              }],
              "Retry" : [
                {
                  "ErrorEquals": ["QUEUE_NOT_EMPTY"],
                  "IntervalSeconds": 20,
                  "BackoffRate": 2.0,
                  "MaxAttempts": 6
                },
                ${local.default_retry}
              ]
            },
            "ResetSpendOrder": {
              "Type": "Task",
              "End": true,
              "Resource": "${aws_lambda_function.bits_spend_order_targeted_resetter.arn}",
              "ResultPath": "$.resetSpendOrderOutput",
              "Catch": [{
                "ErrorEquals": ["States.ALL"],
                "Next": "ResetSpendOrderFailState",
                "ResultPath": "$.error"
              }],
              "Retry": [${local.default_retry}]
            }
          }
        }
      }
    }
  }
EOF
}

locals {
  default_retry = <<EOF
    {
      "ErrorEquals": [
        "States.ALL"
      ],
      "IntervalSeconds": 5,
      "MaxAttempts": 3,
      "BackoffRate": 3.0
    }
EOF
}