provider "aws" {
}

provider "aws" {
  alias = "backup"
}

variable "commit" {
  type        = string
  description = "git sha commit of the service"
}

variable "region" {
  type        = string
  description = "AWS region (e.g., us-west-2)"
}

variable "backup_region" {
  type        = string
  description = "AWS region used for replica and recovery (e.g., us-east-2)"
}

variable "environment" {
  type        = string
  description = "e.g., production/staging"
}

variable "environment_short" {
  type        = string
  description = "e.g., prod/staging"
}

variable "account_id" {
  description = "the account ID used for auto filling in some roles"
}

variable "vpc_id" {
  type        = string
  description = "Private network to create resources in"
}

variable "security_group" {
  type        = string
  description = "Security group to attach to the ECS services"
}

variable "name" {
  type        = string
  default     = "pachter"
  description = "Optional namespace to group resources with"
}

variable "subnets" {
  type = string
}

variable "availability_zones" {
  description = "The availability_zones corresponding to each subnet"
  type        = list(string)
}

variable "vpc_cidr_block" {
  description = "The CIDR block of the VPC"
}

variable "private_cidr_blocks" {
  description = "The private subnet cidr blocks for this stage"
  type        = list(string)
}

variable "min_count" {
  default     = 3
  description = "Minimum number of pachter containers"
}

variable "max_count" {
  default     = 300
  description = "Maximum number of pachter containers"
}

variable "ecs_task_cpu" {
  description = "CPU allocation for ECS task"
}

variable "ecs_task_memory" {
  description = "Memory allocation for ECS task"
}

variable "petozi_vpc_endpoint_service_name" {
  type        = string
  description = "The name of the Petozi VPC endpoint service"
}

variable "petozi_dns" {
  type        = string
  description = "DNS for Petozi"
}

variable "payday_vpc_endpoint_service_name" {
  type        = string
  description = "The name of the Payday VPC endpoint service"
}

variable "payday_dns" {
  type        = string
  description = "DNS for Payday"
}

variable "payday_private_zone" {
  type        = string
  description = "Zone for Payday"
}

variable "pachinko_vpc_endpoint_service_name" {
  type        = string
  description = "The name of the Pachinko VPC endpoint service"
}

variable "pachinko_dns" {
  type        = string
  description = "DNS for Pachinko"
}

variable "pachinko_private_zone" {
  type        = string
  description = "Zone for Pachinko"
}

variable "route_table_external_id" {
  type = string
}

variable "route_table_internal_a_id" {
  type = string
}

variable "route_table_internal_b_id" {
  type = string
}

variable "route_table_internal_c_id" {
  type = string
}

variable "payday_cidr_block" {
  type    = string
  default = "10.193.64.0/18"
}

variable "pachter_payday_vpc_peering_id" {
  type        = string
  description = "the vpc peerinc connectino id"
}

variable "bits_balance_update_sns_topic" {
  description = "the SNS topic Pachinko publishes to when bits balance is updated"
}

variable "payday_bits_balance_update_sns_topic" {
  description = "the SNS topic Payday publishes to when Bits balance is updated"
}

variable "whitelisted_arns_for_privatelink" {
  type    = list(string)
  default = []
}

# Dynamo
variable "dynamo_audit_table_read_capacity" {
}

variable "dynamo_lock_table_read_capacity" {
}

variable "dynamo_cheermote_config_table_read_capacity" {
}

variable "dynamo_actuals_table_read_capacity" {
}

variable "dynamo_audit_table_write_capacity" {
}

variable "dynamo_lock_table_write_capacity" {
}

variable "dynamo_cheermote_config_table_write_capacity" {
}

variable "dynamo_actuals_table_write_capacity" {
}

variable "dynamo_autoscaling_role" {
}

# RDS
variable "rds_instance_identifier" {
  description = "the name of the database instance"
}

variable "rds_name" {
  description = "the name of the database"
}

variable "rds_username" {
}

variable "rds_glue_export_job_username" {
  description = "the name of user to use for the glue export job"
}

variable "trigger_schedule" {
  description = "the trigger schedule "
}

variable "rds_password" {
}

variable "tahoe_producer_name" {
}

variable "rds_instance_class" {
  description = "computational, network, memory capacity. default is memory: 16GiB, network perf: high, EBS: 750 Mbps"
  default     = "db.m4.4xlarge"
}

variable "rds_storage_type" {
  description = "one of three storage types: General Purpose SSD ('gp2'), Provisioned IOPS SSD ('io1'), and magnetic"
  default     = "gp2"
}

variable "rds_iops" {
  description = "the amount of provisioned IOPS. Setting this implies a storage type of 'io1'"
  default     = 0
}

variable "rds_multi_az_enabled" {
  description = "creates a replica in a different availability zone for data redundancy and minimizing latency spikes during system backups"
  default     = true
}

variable "rds_allocated_storage" {
  description = "the amount of storage in GB to allocate for the DB instance. for postgreSQL with gp2, the minimum 20 and the max 32768"
}

variable "rds_backup_retention_period" {
  description = "the number of days that RDS should retain automatic backups of this DB instance"
}

variable "rds_secret_arn" {
  description = "the ARN of the secret manager secret for RDS instance access"
}

# AWS Redis
variable "redis_instance_type" {
  description = "The type of instance (size) to be used for the redis cluster"
}

variable "redis_num_node_groups" {
  description = "Number of redis node groups"
}

variable "redis_replicas_per_node_group" {
  description = "Number of read replicas per redis node group"
}

