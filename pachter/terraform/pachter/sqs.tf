# bits_spend v2 queue
module "bits_spend_fifo_queue_v2" {
  source                     = "./modules/sqs_fifo"
  queue_name                 = "bits-spend-${var.environment_short}-v2.fifo"
  allowed_resource_arn       = aws_lambda_function.deferred_transaction_poller.arn
  dlq_name                   = "bits-spend-${var.environment_short}-v2-dlq.fifo"
  visibility_timeout_seconds = "270"
}

# bits_transaction v2 queue
module "bits_transaction_queue_v2" {
  source                = "./modules/sqs"
  queue_name            = "bits-transaction-${var.environment_short}-v2"
  send_allowed_resource = [var.payday_bits_balance_update_sns_topic]
}

resource "aws_sns_topic_subscription" "bits_transaction_queue_v2_subscription" {
  topic_arn = var.payday_bits_balance_update_sns_topic
  protocol  = "sqs"
  endpoint  = module.bits_transaction_queue_v2.queue_arn
}

# cheermote_usage v2 queue
module "cheermote_usage_queue_v2" {
  source                = "./modules/sqs"
  queue_name            = "cheermote-usage-${var.environment_short}-v2"
  send_allowed_resource = [var.payday_bits_balance_update_sns_topic]
}

resource "aws_sns_topic_subscription" "cheermote_usage_queue_v2_subscription" {
  topic_arn = var.payday_bits_balance_update_sns_topic
  protocol  = "sqs"
  endpoint  = module.cheermote_usage_queue_v2.queue_arn
}

# bits_transaction_audit v2 queue
module "bits_transaction_audit_queue_v2" {
  source                = "./modules/sqs"
  queue_name            = "bits-transaction-audit-${var.environment_short}-v2"
  send_allowed_resource = [var.payday_bits_balance_update_sns_topic]
  delay_seconds         = 900 # Max delay is 15 minutes
}

resource "aws_sns_topic_subscription" "bits_transaction_audit_queue_v2_subscription" {
  topic_arn = var.payday_bits_balance_update_sns_topic
  protocol  = "sqs"
  endpoint  = module.bits_transaction_audit_queue_v2.queue_arn
}

# transaction deferrer v2 queue
module "bits_transaction_deferrer_queue_v2" {
  source                = "./modules/sqs"
  queue_name            = "bits-transaction-deferrer-${var.environment_short}-v2"
  send_allowed_resource = [var.payday_bits_balance_update_sns_topic]
}

resource "aws_sns_topic_subscription" "bits_transaction_deferrer_queue_v2_subscription" {
  topic_arn = var.payday_bits_balance_update_sns_topic
  protocol  = "sqs"
  endpoint  = module.bits_transaction_deferrer_queue_v2.queue_arn
}

# Monthly accounting report generation queue
module "bits_report_generator_queue" {
  source                     = "./modules/sqs"
  queue_name                 = "bits-report-generator-${var.environment_short}"
  visibility_timeout_seconds = 7200
  send_allowed_resource = [
    "arn:aws:events:${var.region}:${var.account_id}:rule/BitsMonthlyReportLastMonth",
    "arn:aws:events:${var.region}:${var.account_id}:rule/BitsMonthlyReportCurrentMonth",
  ]
}

# sqs to store SNS topic publishes
module "rds_export_glue_error_queue" {
  source                = "./modules/sqs"
  queue_name            = "${aws_sns_topic.db_export_glue_errors.name}"
  send_allowed_resource = ["${aws_sns_topic.db_export_glue_errors.arn}"]
}

resource "aws_sns_topic_subscription" "db_export_glue_errors" {
  topic_arn = "${aws_sns_topic.db_export_glue_errors.arn}"
  protocol  = "sqs"
  endpoint  = "${module.rds_export_glue_error_queue.queue_arn}"
}

resource "aws_sqs_queue_policy" "db_export_glue_errors" {
  queue_url = "${module.rds_export_glue_error_queue.queue_url}"
  policy    = "${data.aws_iam_policy_document.db_export_glue_errors.json}"
}

data "aws_iam_policy_document" "db_export_glue_errors" {
  statement {
    effect = "Allow"
    principals {
      identifiers = ["*"]
      type        = "AWS"
    }
    actions = [
      "SQS:SendMessage"
    ]
    resources = ["${module.rds_export_glue_error_queue.queue_arn}"]
    condition {
      test     = "ArnEquals"
      variable = "aws:SourceArn"
      values = [
        "${aws_sns_topic.db_export_glue_errors.arn}"
      ]
    }
  }
}
