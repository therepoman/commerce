package mocks

// Use command: make generate_mocks
// To regenerate the mocks defined in this file

// Petozi RPC Client
//go:generate $GOPATH/src/code.justin.tv/commerce/pachter/_tools/bin/retool do mockery -name=Petozi -dir=$GOPATH/src/code.justin.tv/commerce/pachter/vendor/code.justin.tv/commerce/petozi/rpc -output=$GOPATH/src/code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/petozi/rpc -outpkg=petozi_mock

// Payday RPC Client
//go:generate $GOPATH/src/code.justin.tv/commerce/pachter/_tools/bin/retool do mockery -name=Payday -dir=$GOPATH/src/code.justin.tv/commerce/pachter/vendor/code.justin.tv/commerce/payday/rpc/payday -output=$GOPATH/src/code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/payday/rpc/payday -outpkg=payday_mock

// Payday Client
//go:generate $GOPATH/src/code.justin.tv/commerce/pachter/_tools/bin/retool do mockery -name=PaydayClient -dir=$GOPATH/src/code.justin.tv/commerce/pachter/backend/clients/payday -output=$GOPATH/src/code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/clients/payday -outpkg=payday_mock

// Payday Client Converter
//go:generate $GOPATH/src/code.justin.tv/commerce/pachter/_tools/bin/retool do mockery -name=Converter -dir=$GOPATH/src/code.justin.tv/commerce/pachter/backend/clients/payday -output=$GOPATH/src/code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/clients/payday -outpkg=payday_mock

// Pachinko RPC Client
//go:generate $GOPATH/src/code.justin.tv/commerce/pachter/_tools/bin/retool do mockery -name=Pachinko -dir=$GOPATH/src/code.justin.tv/commerce/pachter/vendor/code.justin.tv/commerce/pachinko/rpc -output=$GOPATH/src/code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachinko/rpc -outpkg=pachinko_mock

// Redis Client
//go:generate $GOPATH/src/code.justin.tv/commerce/pachter/_tools/bin/retool do mockery -name=RedisClient -dir=$GOPATH/src/code.justin.tv/commerce/pachter/backend/clients/redis -output=$GOPATH/src/code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/clients/redis -outpkg=redis_mock

// Transactions DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/pachter/_tools/bin/retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/pachter/backend/rds/bits_transaction -output=$GOPATH/src/code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/rds/bits_transaction -outpkg=dao_mock

// Bits Spend DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/pachter/_tools/bin/retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/pachter/backend/rds/bits_spend -output=$GOPATH/src/code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/rds/bits_spend -outpkg=dao_mock

// Worker Audit DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/pachter/_tools/bin/retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/pachter/backend/dynamo/worker_audit -output=$GOPATH/src/code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/dynamo/worker_audit -outpkg=dao_mock

// Cheermote Usage DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/pachter/_tools/bin/retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/pachter/backend/rds/cheermote_usage -output=$GOPATH/src/code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/rds/cheermote_usage -outpkg=dao_mock

// Actuals DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/pachter/_tools/bin/retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/pachter/backend/dynamo/actuals -output=$GOPATH/src/code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/dynamo/actuals -outpkg=dao_mock

// Bits Spend DAO
//go:generate $GOPATH/src/code.justin.tv/commerce/pachter/_tools/bin/retool do mockery -name=DAO -dir=$GOPATH/src/code.justin.tv/commerce/pachter/backend/dynamo/bits_cheermote_payout_config -output=$GOPATH/src/code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/dynamo/bits_cheermote_payout_config -outpkg=dao_mock

// S3 client
//go:generate $GOPATH/src/code.justin.tv/commerce/pachter/_tools/bin/retool do mockery -name=Client -dir=$GOPATH/src/code.justin.tv/commerce/gogogadget/aws/s3 -output=$GOPATH/src/code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/gogogadget/aws/s3 -outpkg=s3_mock

// Statter
//go:generate $GOPATH/src/code.justin.tv/commerce/pachter/_tools/bin/retool do mockery -name=Statter -dir=$GOPATH/src/code.justin.tv/commerce/pachter/backend/metrics -output=$GOPATH/src/code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/metrics -outpkg=metrics_mock

// SNF Client
//go:generate $GOPATH/src/code.justin.tv/commerce/pachter/_tools/bin/retool do mockery -name=SFNClient -dir=$GOPATH/src/code.justin.tv/commerce/pachter/backend/clients/sfn -output=$GOPATH/src/code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/clients/sfn -outpkg=sfn_mock
