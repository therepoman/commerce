package main

import (
	"net/http"
	"os"
	"strings"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachter/backend"
	"code.justin.tv/commerce/pachter/backend/metrics"
	"code.justin.tv/commerce/pachter/config"
	pachter "code.justin.tv/commerce/pachter/rpc"
	"code.justin.tv/commerce/splatter"
	"code.justin.tv/sse/malachai/pkg/events"
	"code.justin.tv/sse/malachai/pkg/s2s/callee"
	"github.com/twitchtv/twirp"
	goji_graceful "github.com/zenazn/goji/graceful"
	goji "goji.io"
	"goji.io/pat"
)

var s2sEnabledOperations = map[string]bool{
	"HealthCheck": false,
}

func getEnv() config.Environment {
	env := os.Getenv(config.EnvironmentVariable)
	if env != "" {
		log.Infof("Found ENVIRONMENT environment variable: %s", env)
	}

	args := os.Args
	if len(args) > 2 {
		log.Panic("Received too many CLI args")
	} else if len(args) == 2 {
		env = args[1]
		log.Infof("Using environment from CLI arg: %s", env)
	}

	return config.GetOrDefaultEnvironment(env)
}

func main() {
	env := getEnv()
	log.Infof("Starting the server in %s environment", env)

	cfg, err := config.LoadConfig(env)
	if err != nil {
		log.WithError(err).Panic("Failed to load config file")
	}

	be, err := backend.NewBackend(cfg)
	if err != nil {
		log.WithError(err).Panic("Error initializing backend")
	}

	telemetryMiddleware, telemetryObserver, err := metrics.NewTwirpTelemetryHook(cfg.AWSRegion, cfg.EnvironmentName)
	if err != nil {
		log.WithError(err).Panic("Error initializing backend")
	}

	twirpStatsHook := twirp.ChainHooks(splatter.NewStatsdServerHooks(be.Statter()), telemetryMiddleware)
	twirpHandler := pachter.NewPachterServer(be.TwirpServer(), twirpStatsHook)
	defer func() {
		telemetryObserver.Stop()
		err := be.Statter().Close()
		if err != nil {
			log.WithError(err).Error("Error closing statter")
		}
	}()

	var s2sTwirpHandler http.Handler
	if cfg.S2SEnabled {
		eventsWriterClient, err := events.NewEventLogger(events.Config{
			Environment:     "production",
			BufferSizeLimit: 0,
		}, log.New())
		if err != nil {
			log.WithError(err).Panic("Error initializing s2s event logger")
		}

		s2sCalleeClient := &callee.Client{
			ServiceName:        cfg.S2SServiceName,
			Logger:             log.New(),
			EventsWriterClient: eventsWriterClient,
		}
		err = s2sCalleeClient.Start()
		if err != nil {
			log.WithError(err).Panic("Error starting s2s callee client")
		}

		s2sTwirpHandler = s2sCalleeClient.RequestValidatorMiddleware(twirpHandler)
	} else {
		s2sTwirpHandler = twirpHandler
	}

	mux := goji.NewMux()
	mux.HandleFunc(pat.Get("/ping"), be.Ping)
	mux.Handle(pat.Post(pachter.PachterPathPrefix+"*"), getTwirpHandler(twirpHandler, s2sTwirpHandler))

	gojiServer := goji_graceful.Server{
		Addr:         ":8080",
		Handler:      mux,
		ReadTimeout:  60 * time.Minute,
		WriteTimeout: 60 * time.Minute,
	}
	goji_graceful.HandleSignals()

	if err := gojiServer.ListenAndServe(); err != nil {
		log.WithError(err).Panic("ListenAndServe failed")
	}

	log.Info("Shutting down the server")
}

func getTwirpHandler(nonS2SHandler, s2sHandler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if isS2SRequest(r) {
			s2sHandler.ServeHTTP(w, r)
		} else {
			nonS2SHandler.ServeHTTP(w, r)
		}
	})
}

func isS2SRequest(r *http.Request) bool {
	if r == nil {
		return false
	}
	if r.URL == nil {
		return false
	}
	path := r.URL.Path
	operation := strings.TrimPrefix(path, pachter.PachterPathPrefix)

	enabled, ok := s2sEnabledOperations[operation]
	return ok && enabled
}
