package rds

import "strings"

func IsPKViolationError(err error) bool {
	return strings.Contains(err.Error(), "pq: duplicate key value violates unique constraint")
}
