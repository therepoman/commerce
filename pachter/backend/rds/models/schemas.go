package models

import (
	"time"

	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/jinzhu/gorm/dialects/postgres"
)

type TransactionType string

const (
	TransactionType_Cheering           TransactionType = "GiveBitsToBroadcaster"
	TransactionType_AddBits            TransactionType = "AddBits"
	TransactionType_AdminAddBits       TransactionType = "AdminAddBits"
	TransactionType_UseBitsOnExtension TransactionType = "UseBitsOnExtension"
)

type BitsTransaction struct {
	TransactionID          string `gorm:"primary_key"` // specifying PK here to make the gorm.Save do upsert
	TransactionType        string
	RequestingTwitchUserID string
	ReceivingTwitchUserID  string
	Datetime               time.Time
	CheermoteUsages        []*CheermoteUsage `gorm:"-"`
	CheermoteUsagesJSON    postgres.Jsonb    `gorm:"column:cheermote_usages"`
	Quantity               int64
	SponsoredQuantity      int64
	BitsType               *string
	Message                *string
	Platform               *string
	ProductID              *string
	ExtensionClientID      *string
	PurchasePriceUSC       int64
}

func (bt BitsTransaction) ShouldBeSkipped() bool {
	return bt.TransactionType == paydayrpc.TransactionType_CreateHold.String() ||
		bt.TransactionType == paydayrpc.TransactionType_ReleaseHold.String()
}

type BitsSpend struct {
	TransactionID          string
	ReferenceTransactionID string
	RequestingTwitchUserID string
	ReceivingTwitchUserID  string
	Datetime               time.Time
	Quantity               int64
	PurchasePriceUSC       int64
}

type CheermoteUsage struct {
	TransactionID    string    `gorm:"primary_key" json:"transaction_id"`
	Cheermote        string    `gorm:"primary_key" json:"cheermote"`
	Datetime         time.Time `json:"datetime"`
	Quantity         int64     `json:"quantity"`
	PurchasePriceUSC int64     `json:"purchase_price_usc"`
}

type BitsBreakage struct {
}
