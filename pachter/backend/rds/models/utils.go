package models

import (
	"encoding/json"
	"strings"

	"github.com/jinzhu/gorm/dialects/postgres"
)

func (tx *BitsTransaction) MarshallCheermoteUsage() error {
	if tx.CheermoteUsages == nil {
		return nil
	}

	marshalled, err := json.Marshal(tx.CheermoteUsages)
	if err != nil {
		return err
	}

	tx.CheermoteUsagesJSON = postgres.Jsonb{RawMessage: marshalled}
	return nil
}

func (tx *BitsTransaction) UnmarshallCheermoteUsage() error {
	if tx.CheermoteUsagesJSON.RawMessage == nil {
		return nil
	}

	var cheermoteUsages []*CheermoteUsage
	err := json.Unmarshal(tx.CheermoteUsagesJSON.RawMessage, &cheermoteUsages)
	if err != nil {
		return err
	}

	tx.CheermoteUsages = cheermoteUsages
	return nil
}

func (tx *BitsTransaction) RemoveSpacePaddings() {
	tx.TransactionID = strings.TrimSpace(tx.TransactionID)
	tx.TransactionType = strings.TrimSpace(tx.TransactionType)
	tx.RequestingTwitchUserID = strings.TrimSpace(tx.RequestingTwitchUserID)
	tx.ReceivingTwitchUserID = strings.TrimSpace(tx.ReceivingTwitchUserID)
	tx.BitsType = trimSpace(tx.BitsType)
	tx.Message = trimSpace(tx.Message)
	tx.Platform = trimSpace(tx.Platform)
	tx.ProductID = trimSpace(tx.ProductID)
	tx.ExtensionClientID = trimSpace(tx.ExtensionClientID)
}

func (tx BitsTransaction) ToBitsSpend() BitsSpend {
	return BitsSpend{
		TransactionID:          tx.TransactionID,
		ReferenceTransactionID: "",
		RequestingTwitchUserID: tx.RequestingTwitchUserID,
		ReceivingTwitchUserID:  tx.ReceivingTwitchUserID,
		Datetime:               tx.Datetime,
		Quantity:               tx.Quantity,
		PurchasePriceUSC:       tx.PurchasePriceUSC,
	}
}

func trimSpace(s *string) *string {
	if s == nil {
		return s
	}

	trimmed := strings.TrimSpace(*s)
	return &trimmed
}

func (c *CheermoteUsage) RemoveSpacePaddings() {
	c.TransactionID = strings.TrimSpace(c.TransactionID)
	c.Cheermote = strings.TrimSpace(c.Cheermote)
}
