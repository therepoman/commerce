package rds

import (
	"math/rand"
	"os"
	"strings"
	"testing"
	"time"

	"code.justin.tv/commerce/pachter/backend/clients/rds"
	"code.justin.tv/commerce/pachter/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/jinzhu/gorm"
)

const (
	RunRDSTestsEnvVar   = "RUN_RDS_TESTS"
	TestTablesSuffix    = "staging"
	TestAWSNamedProfile = "pachter-devo"
	TestAWSRegion       = "us-west-2"
	SecretStoreARN      = "arn:aws:secretsmanager:us-west-2:721938063588:secret:pachter-rds-credentials-Kh5FrK"
)

func GetRDSClient(t *testing.T) (*gorm.DB, error) {
	rand.Seed(time.Now().UnixNano())

	runTests := os.Getenv(RunRDSTestsEnvVar)
	if strings.ToLower(runTests) != "true" {
		t.Skip("Skipping RDS test")
	}

	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Config:  aws.Config{Region: aws.String(TestAWSRegion)},
		Profile: TestAWSNamedProfile,
	}))
	return rds.NewRDSClient(&config.Config{
		RDSAWSSecretStoreARN: SecretStoreARN,
		RDSEndpointOverride:  "localhost",
		RDSPortOverride:      5432,
	}, sess)
}
