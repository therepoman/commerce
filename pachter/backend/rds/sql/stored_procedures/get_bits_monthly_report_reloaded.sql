CREATE OR REPLACE FUNCTION get_bits_monthly_report_reloaded(start_time timestamp, end_time timestamp)
    RETURNS TABLE (
        bits_type     VARCHAR(50),
        platform      VARCHAR(50),
        entitled_qtty BIGINT,
        entitled_cost FLOAT,
        cheering_qtty BIGINT,
        cheering_cost FLOAT,
        deleted_qtty  BIGINT,
        deleted_cost  FLOAT
)
AS $$
BEGIN


-- Filter the bits_spends table by datetime range
CREATE TEMP TABLE bits_spends_filtered_temp (
    transaction_id           VARCHAR(255),
    reference_transaction_id VARCHAR(255),
    quantity                 INT
);

INSERT INTO
    bits_spends_filtered_temp
SELECT
    transaction_id, reference_transaction_id, quantity
FROM
    bits_spends
WHERE
    $1 <= datetime
AND
    datetime < $2;

-- Get all the transaction_ids and reference_transaction_ids from the data above
CREATE TEMP TABLE all_transaction_ids_temp (
    transaction_id VARCHAR(255)
);

INSERT INTO
    all_transaction_ids_temp
SELECT DISTINCT
    transaction_id
FROM
    bits_spends_filtered_temp;

INSERT INTO
    all_transaction_ids_temp
SELECT DISTINCT
    reference_transaction_id
FROM
    bits_spends_filtered_temp
WHERE
    reference_transaction_id <> '';

-- Get all transaction_ids from above from the bits_transactions table
CREATE TEMP TABLE bits_transactions_filtered_temp (
    transaction_id     VARCHAR(255),
    transaction_type   VARCHAR(50),
    bits_type          VARCHAR(50),
    purchase_price_usc INT,
    quantity           INT
);

INSERT INTO
    bits_transactions_filtered_temp
SELECT
    RTRIM(bits_transactions.transaction_id, ' '), RTRIM(bits_transactions.transaction_type, ' '), RTRIM(bits_transactions.bits_type, ' '), bits_transactions.purchase_price_usc, bits_transactions.quantity
FROM
    bits_transactions
WHERE
    bits_transactions.transaction_id
IN
(
    SELECT DISTINCT
        transaction_id
    FROM
        all_transaction_ids_temp
);

-- Get all deductions with their corresponding reference transaction_ids (bits acquired transactions)
CREATE TEMP TABLE all_monthly_deduct_bits_transactions_temp (
    transaction_type         VARCHAR(50),
    quantity                 INT,
    reference_transaction_id VARCHAR(255)
);

INSERT INTO
    all_monthly_deduct_bits_transactions_temp
SELECT
    bits_transactions_filtered_temp.transaction_type,
    bits_spends_filtered_temp.quantity,
    bits_spends_filtered_temp.reference_transaction_id
FROM
    bits_spends_filtered_temp
INNER JOIN
    bits_transactions_filtered_temp
ON
    bits_spends_filtered_temp.transaction_id = bits_transactions_filtered_temp.transaction_id
WHERE
    bits_spends_filtered_temp.reference_transaction_id <> ''
AND
    bits_transactions_filtered_temp.transaction_type
IN
(
    'GiveBitsToBroadcaster',
    'GiveBitsToBroadcasterType',
    'UseBitsOnExtension',
    'UseBitsOnPoll',
    'AdminRemoveBits',
    'CustomerChargeback',
    'CustomerRevoke'
);

-- Join the reference transaction data
CREATE TEMP TABLE deduct_bits_transactions_temp (
    transaction_type                         VARCHAR(50),
    bits_type                                VARCHAR(50),
    quantity                                 INT,
    cost                                     FLOAT
);

INSERT INTO
    deduct_bits_transactions_temp
SELECT
    all_monthly_deduct_bits_transactions_temp.transaction_type,
    reference_transaction.bits_type,
    all_monthly_deduct_bits_transactions_temp.quantity,
    (reference_transaction.purchase_price_usc / reference_transaction.quantity) * all_monthly_deduct_bits_transactions_temp.quantity as cost
FROM
    all_monthly_deduct_bits_transactions_temp
INNER JOIN
    bits_transactions_filtered_temp as reference_transaction
ON
    all_monthly_deduct_bits_transactions_temp.reference_transaction_id = reference_transaction.transaction_id
WHERE
    reference_transaction.transaction_type
IN
(
    'AddBits',
    'AdminAddBits'
);


-- Get all bits types entitled in the time range
CREATE TEMP TABLE entitlement_qtty (
    bits_type                VARCHAR(50),
    quantity                 BIGINT,
    cost                     FLOAT
);

INSERT INTO
    entitlement_qtty
SELECT
    bits_transactions.bits_type, SUM(bits_transactions.quantity) as quantity, SUM(bits_transactions.purchase_price_usc) as cost
FROM
    bits_transactions
WHERE
    $1 <= datetime
AND
    datetime < $2
AND
    bits_transactions.transaction_type
IN
(
    'AddBits',
    'AdminAddBits'
)
GROUP BY
    bits_transactions.bits_type;

-- Summation of all cheering by bit type
CREATE TEMP TABLE cheering_qtty (
    bits_type VARCHAR(50),
    quantity  BIGINT,
    cost      FLOAT
);

INSERT INTO
    cheering_qtty
SELECT
    deduct_bits_transactions_temp.bits_type,
    SUM(deduct_bits_transactions_temp.quantity) as quantity,
    SUM(deduct_bits_transactions_temp.cost) as cost
FROM
    deduct_bits_transactions_temp
WHERE
    deduct_bits_transactions_temp.transaction_type IN
(
    'GiveBitsToBroadcaster',
    'GiveBitsToBroadcasterType',
    'UseBitsOnExtension',
    'UseBitsOnPoll'
)
GROUP BY
    deduct_bits_transactions_temp.bits_type;

-- Summation of all deletion by bit type
CREATE TEMP TABLE deletion_qtty (
    bits_type VARCHAR(50),
    quantity  BIGINT,
    cost      FLOAT
);

INSERT INTO
    deletion_qtty
SELECT
    deduct_bits_transactions_temp.bits_type,
    SUM(deduct_bits_transactions_temp.quantity) as quantity,
    SUM(deduct_bits_transactions_temp.cost) as cost
FROM
    deduct_bits_transactions_temp
WHERE
    deduct_bits_transactions_temp.transaction_type IN
(
    'AdminRemoveBits',
    'CustomerChargeback',
    'CustomerRevoke'
)
GROUP BY
    deduct_bits_transactions_temp.bits_type;


-- Get all bit bit types from  all transactions ever
CREATE TEMP TABLE bits_type_platform (
    bits_type VARCHAR(50),
    platform  VARCHAR(50)
);

INSERT INTO
    bits_type_platform
SELECT DISTINCT
    RTRIM(bits_transactions.bits_type, ' '), RTRIM(bits_transactions.platform, ' ')
FROM
    bits_transactions
WHERE
    bits_transactions.bits_type <> ''
ORDER BY
    RTRIM(bits_transactions.bits_type, ' ');


CREATE TEMP TABLE monthly_report (
    bits_type     VARCHAR(50),
    platform      VARCHAR(50),
    entitled_qtty BIGINT DEFAULT 0,
    entitled_cost FLOAT  DEFAULT 0,
    cheering_qtty BIGINT DEFAULT 0,
    cheering_cost FLOAT  DEFAULT 0,
    deleted_qtty  BIGINT DEFAULT 0,
    deleted_cost  FLOAT  DEFAULT 0
);

-- The final report
INSERT INTO
    monthly_report
SELECT
    bits_type_platform.bits_type,
    bits_type_platform.platform,
    entitlement_qtty.quantity as entitled_qtty,
    entitlement_qtty.cost     as entitled_cost,
    cheering_qtty.quantity    as cheering_qtty,
    cheering_qtty.cost        as cheering_cost,
    deletion_qtty.quantity    as deleted_qtty,
    deletion_qtty.cost        as deleted_cost
FROM
    bits_type_platform
LEFT OUTER JOIN
    entitlement_qtty
ON
    bits_type_platform.bits_type = entitlement_qtty.bits_type
LEFT OUTER JOIN
    cheering_qtty
ON
    bits_type_platform.bits_type = cheering_qtty.bits_type
LEFT OUTER JOIN
    deletion_qtty
ON
    bits_type_platform.bits_type = deletion_qtty.bits_type;


-- Add sponsored bits row to the final report
INSERT INTO
    monthly_report (bits_type, cheering_qtty)
SELECT
    'SponsoredBits', SUM(sponsored_quantity)
FROM
    bits_transactions
WHERE
    $1 <= datetime
AND
    datetime < $2;


RETURN QUERY SELECT * FROM monthly_report;

DROP TABLE bits_transactions_filtered_temp;
DROP TABLE bits_spends_filtered_temp;
DROP TABLE all_transaction_ids_temp;
DROP TABLE all_monthly_deduct_bits_transactions_temp;
DROP TABLE deduct_bits_transactions_temp;
DROP TABLE entitlement_qtty;
DROP TABLE cheering_qtty;
DROP TABLE deletion_qtty;
DROP TABLE bits_type_platform;
DROP TABLE monthly_report;

END; $$

LANGUAGE 'plpgsql';
-- SELECT get_bits_monthly_report_reloaded('2020-01-01 00:00:00+00', '2020-02-01 00:00:00+00');