CREATE TABLE bits_transactions (
    transaction_id            CHAR(255) PRIMARY KEY,
    transaction_type          CHAR(50) NOT NULL,
    requesting_twitch_user_id CHAR(50) NOT NULL,
    receiving_twitch_user_id  CHAR(50) NOT NULL,
    datetime                  TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
    cheermote_usages          JSONB,
    quantity                  INT,
    bits_type                 CHAR(50),  -- for AddBits only
    message                   TEXT,      -- for cheering only
    platform                  CHAR(50),  -- for entitlements only
    product_id                CHAR(50),  -- for Bits in Extension and Phanto SKU's
    extension_client_id       CHAR(255), -- for extensions only
    purchase_price_usc        INT
);
CREATE INDEX CONCURRENTLY bits_transactions_datetime_idx ON bits_transactions (datetime);
CREATE INDEX CONCURRENTLY bits_transactions_receiving_twitch_user_id_datetime_idx ON bits_transactions (receiving_twitch_user_id, datetime);
CREATE INDEX CONCURRENTLY bits_transactions_requesting_twitch_user_id_idx ON bits_transactions (requesting_twitch_user_id);

ALTER TABLE bits_transactions ADD COLUMN sponsored_quantity INT NOT NULL DEFAULT 0;
ALTER TABLE bits_transactions ALTER COLUMN bits_type TYPE VARCHAR(255);

-- adding in the updated_at and update_reason fields for SAP compliance
ALTER TABLE bits_transactions ADD updated_at TIMESTAMP WITH TIME ZONE;
ALTER TABLE bits_transactions ADD update_reason VARCHAR(50);

CREATE TABLE bits_spends (
    transaction_id            VARCHAR(255) NOT NULL,
    reference_transaction_id  VARCHAR(255) NOT NULL,
    requesting_twitch_user_id VARCHAR(50) NOT NULL,
    receiving_twitch_user_id  VARCHAR(50) NOT NULL,
    datetime                  TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
    quantity                  INT,
    purchase_price_usc        INT,
    PRIMARY KEY (transaction_id, reference_transaction_id)
);
CREATE INDEX CONCURRENTLY bits_spends_requesting_twitch_user_id_idx ON bits_spends (requesting_twitch_user_id);
CREATE INDEX CONCURRENTLY bits_spends_datetime_idx ON bits_spends (datetime);
CREATE INDEX CONCURRENTLY bits_spends_transaction_id_idx ON bits_spends (transaction_id);
CREATE INDEX CONCURRENTLY bits_spends_reference_transaction_id_idx ON bits_spends (reference_transaction_id);

CREATE TABLE cheermote_usages (
    transaction_id     CHAR(255) NOT NULL,
    cheermote          CHAR(50) NOT NULL,
    datetime           TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
    quantity           INT,
    purchase_price_usc INT,
    PRIMARY KEY (transaction_id, cheermote)
);
CREATE INDEX CONCURRENTLY cheermote_usages_datetime_idx ON cheermote_usages (datetime);

CREATE TABLE bits_breakages (
    reference_transaction_id CHAR(255) REFERENCES bits_transactions(transaction_id),
    twitch_user_id           CHAR(50) NOT NULL,
    breakage_datetime        TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
    quantity                 INT,
    purchase_price_usc       INT
);