package bits_transaction

import (
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachter/backend/rds"
	"code.justin.tv/commerce/pachter/backend/rds/models"
	"github.com/gofrs/uuid"
	"github.com/jinzhu/gorm/dialects/postgres"
	. "github.com/smartystreets/goconvey/convey"
)

func TestDAO(t *testing.T) {
	client, err := rds.GetRDSClient(t)
	if err != nil {
		t.Errorf("Failed to initialize RDS client for tests %+v", err)
		return
	}
	dao := dao{
		Table: client,
	}

	Convey("bits transactions DAO", t, func() {
		transactionID := uuid.Must(uuid.NewV4()).String()
		transactionID2 := uuid.Must(uuid.NewV4()).String()
		transactionID3 := uuid.Must(uuid.NewV4()).String()
		requester := random.String(10)
		receiver := random.String(10)
		message := "just some message"
		platform := "my_great_platform"
		bitsType := "test-bit-type"
		cheermoteUsage := &models.CheermoteUsage{
			TransactionID:    transactionID,
			Cheermote:        "lul",
			Datetime:         time.Now().UTC(),
			Quantity:         100,
			PurchasePriceUSC: 5,
		}

		input := &models.BitsTransaction{
			TransactionID:          transactionID,
			TransactionType:        "TEST",
			RequestingTwitchUserID: requester,
			ReceivingTwitchUserID:  receiver,
			Datetime:               time.Now().UTC(),
			Quantity:               100,
			PurchasePriceUSC:       5,
			Message:                &message,
			Platform:               &platform,
			CheermoteUsages:        []*models.CheermoteUsage{cheermoteUsage},
		}

		input2 := &models.BitsTransaction{
			TransactionID:          transactionID2,
			TransactionType:        "TEST",
			RequestingTwitchUserID: requester,
			ReceivingTwitchUserID:  receiver,
			Datetime:               time.Now().UTC(),
			Quantity:               150,
			PurchasePriceUSC:       5,
			Message:                &message,
			Platform:               &platform,
			CheermoteUsages:        []*models.CheermoteUsage{cheermoteUsage},
			BitsType:               &bitsType,
		}

		input3 := &models.BitsTransaction{
			TransactionID:          transactionID3,
			RequestingTwitchUserID: requester,
			ReceivingTwitchUserID:  receiver,
			Datetime:               time.Now().UTC(),
			Quantity:               150,
			PurchasePriceUSC:       5,
			Message:                &message,
			Platform:               &platform,
			CheermoteUsages:        []*models.CheermoteUsage{cheermoteUsage},
		}

		Convey("#Put", func() {
			Convey("writes a new entry", func() {
				Convey("with cheermote usage", func() {
					input.CheermoteUsages = []*models.CheermoteUsage{cheermoteUsage}
				})

				Convey("without cheermote usage", func() {
					input.CheermoteUsages = nil
				})

				Convey("with zero time", func() {
					input.Datetime = time.Time{}
				})

				Convey("with unsanitized message input", func() {
					newMsg := "\x00" + *input.Message
					input.Message = &newMsg
				})

				err := dao.Put(input)
				So(err, ShouldBeNil)

				savedTransaction, err := dao.Get(transactionID)
				So(err, ShouldBeNil)
				So(savedTransaction, ShouldNotBeNil)

				savedTransaction.CheermoteUsagesJSON = postgres.Jsonb{}
				input.CheermoteUsagesJSON = postgres.Jsonb{}
				So(savedTransaction, ShouldResemble, input)
			})

			Convey("can write the same entry multiple times", func() {
				err := dao.Put(input)
				So(err, ShouldBeNil)
				err = dao.Put(input)
				So(err, ShouldBeNil)
				err = dao.Put(input)
				So(err, ShouldBeNil)
			})

			Convey("returns an error if", func() {
				Convey("transaction ID is blank", func() {
					input.TransactionID = ""
				})

				err := dao.Put(input)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("#BatchPut", func() {
			Convey("saves all", func() {
				var inputs []*models.BitsTransaction

				Convey("given a list of transactions", func() {
					inputs = []*models.BitsTransaction{
						input,
						input2,
						input3,
					}
				})

				Convey("given a list of transactions with duplicates", func() {
					inputs = []*models.BitsTransaction{
						input,
						input2,
						input3,
						input2,
					}
				})
				Convey("given a list of transactions with null unicode character messages", func() {
					msg := "\x00" + *input2.Message
					input2.Message = &msg
					inputs = []*models.BitsTransaction{
						input,
						input2,
						input3,
					}
				})
				err := dao.BatchPut(inputs)
				So(err, ShouldBeNil)

				for _, i := range inputs {
					savedTransaction, err := dao.Get(i.TransactionID)
					So(err, ShouldBeNil)
					So(savedTransaction, ShouldNotBeNil)
					savedTransaction.CheermoteUsagesJSON = postgres.Jsonb{}
					i.CheermoteUsagesJSON = postgres.Jsonb{}
					So(savedTransaction, ShouldResemble, i)
				}
			})
		})

		Convey("#Get", func() {
			Convey("given an existing transaction", func() {
				Convey("retrives an existing transaction", func() {
					Convey("that has cheermote usage", func() {
						input.CheermoteUsages = []*models.CheermoteUsage{cheermoteUsage}
					})

					Convey("that has no cheermote usage", func() {
						input.CheermoteUsages = nil
					})

					err := dao.Put(input)
					So(err, ShouldBeNil)

					transaction, err := dao.Get(transactionID)
					So(err, ShouldBeNil)

					// clear the JSON field and compare
					transaction.CheermoteUsagesJSON = postgres.Jsonb{}
					input.CheermoteUsagesJSON = postgres.Jsonb{}

					So(transaction, ShouldResemble, input)
				})
			})

			Convey("given a non-existent ID", func() {
				transaction, err := dao.Get(transactionID)
				So(err, ShouldBeNil)
				So(transaction, ShouldBeNil)
			})

			Convey("returns an error if", func() {
				Convey("transaction ID is blank", func() {
					_, err := dao.Get("")
					So(err, ShouldNotBeNil)
				})
			})
		})

		Convey("#GetByRequestingTwitchUserID", func() {
			Convey("given an existing transaction", func() {
				err := dao.Put(input)
				So(err, ShouldBeNil)

				transactions, err := dao.GetByRequestingTwitchUserID(requester)
				So(err, ShouldBeNil)

				So(len(transactions), ShouldBeGreaterThan, 0)
			})

			Convey("given a non-existing ID", func() {
				transactions, err := dao.GetByRequestingTwitchUserID(requester)
				So(err, ShouldBeNil)
				So(len(transactions), ShouldEqual, 0)
			})

			Convey("returns an error when user id is blank", func() {
				_, err := dao.GetByRequestingTwitchUserID("")
				So(err, ShouldNotBeNil)
			})
		})
	})
}
