package bits_transaction

import (
	"strings"

	stringUtils "code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/pachter/backend/rds/models"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

// Used for sanitizing transaction messages which may contain messages that the underlying DB doesn't accept.
// runes can map to replacement runes, or a negative value which indicates removing without replacement
var runeMapping = map[rune]rune{
	'\x00': -1,
}

// Implements the mapping function needed for strings.Map(mapping func (r rune) rune, s string) string
func sanitizeMessageMapping(r rune) rune {
	if mapping, ok := runeMapping[r]; ok {
		return mapping
	}
	return r
}

type DAO interface {
	Put(transaction *models.BitsTransaction) error
	BatchPut(transactions []*models.BitsTransaction) error
	Get(transactionID string) (*models.BitsTransaction, error)
	GetByRequestingTwitchUserID(requestingTwitchUserID string) ([]models.BitsTransaction, error)
}

type dao struct {
	Table *gorm.DB `inject:""`
}

func NewDAO() DAO {
	return &dao{}
}

func NewDAOWithClient(rdsClient *gorm.DB) DAO {
	return &dao{
		Table: rdsClient,
	}
}

func (d *dao) Put(transaction *models.BitsTransaction) error {
	if transaction == nil {
		return errors.New("transaction is missing")
	}
	if stringUtils.Blank(transaction.TransactionID) {
		return errors.New("transaction ID is blank")
	}

	// makes a copy here because GORM pads the PK field on the struct with spaces
	copy := *transaction
	err := copy.MarshallCheermoteUsage()
	if err != nil {
		return errors.Wrap(err, "failed to marshall cheermote usage to JSON")
	}

	if copy.Message != nil {
		sanitized := strings.Map(sanitizeMessageMapping, *copy.Message)
		copy.Message = &sanitized
	}

	return d.Table.Save(&copy).Error
}

func (d *dao) BatchPut(transactions []*models.BitsTransaction) error {
	// GORM does not currently support a batch insert solution
	// opening up one transaction to insert all transactions here,
	// instead of one transaction per transaction which is what Table.Create does
	tx := d.Table.Begin()
	if err := tx.Error; err != nil {
		return err
	}

	for _, transaction := range transactions {
		if transaction == nil {
			continue
		}

		// makes a copy here because GORM pads the PK field on the struct with spaces
		copy := *transaction
		err := copy.MarshallCheermoteUsage()
		if err != nil {
			tx.Rollback()
			return errors.Wrap(err, "failed to marshall cheermote usage to JSON")
		}

		if copy.Message != nil {
			sanitized := strings.Map(sanitizeMessageMapping, *copy.Message)
			copy.Message = &sanitized
		}

		err = tx.Save(&copy).Error
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	return tx.Commit().Error
}

func (d *dao) Get(transactionID string) (*models.BitsTransaction, error) {
	if stringUtils.Blank(transactionID) {
		return nil, errors.New("transaction ID is missing")
	}

	var transaction models.BitsTransaction
	err := d.Table.Where("transaction_id = ?", transactionID).First(&transaction).Error

	if gorm.IsRecordNotFoundError(err) {
		return nil, nil
	}

	transaction.RemoveSpacePaddings()
	err = transaction.UnmarshallCheermoteUsage()
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshall cheermote usage from JSON")
	}

	return &transaction, nil
}

func (d *dao) GetByRequestingTwitchUserID(requestingTwitchUserID string) ([]models.BitsTransaction, error) {
	if stringUtils.Blank(requestingTwitchUserID) {
		return nil, errors.New("requestingTwitchUserID is missing")
	}

	var transactions []models.BitsTransaction
	err := d.Table.Where("requesting_twitch_user_id = ?", requestingTwitchUserID).Order("datetime desc").Find(&transactions).Error

	if gorm.IsRecordNotFoundError(err) {
		return nil, nil
	}

	for i := range transactions {
		transactions[i].RemoveSpacePaddings()
	}

	return transactions, nil
}
