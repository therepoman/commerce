package cheermote_usage

import (
	"context"
	"errors"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/pachter/backend/rds/models"
	"github.com/jinzhu/gorm"
)

type DAO interface {
	GetByTransactionID(ctx context.Context, transactionID string) ([]*models.CheermoteUsage, error)
	GetByTransactionIDAndCheermote(ctx context.Context, transactionID, cheermote string) (*models.CheermoteUsage, error)
	BatchPut(ctx context.Context, cheermoteUsages []*models.CheermoteUsage) error
}

type dao struct {
	Table *gorm.DB `inject:""`
}

func NewDAO() DAO {
	return &dao{}
}

func (d *dao) GetByTransactionID(ctx context.Context, transactionID string) ([]*models.CheermoteUsage, error) {
	if strings.Blank(transactionID) {
		return nil, errors.New("transaction ID is missing")
	}

	var cheermoteUsages []*models.CheermoteUsage
	err := d.Table.Where(&models.CheermoteUsage{TransactionID: transactionID}).Find(&cheermoteUsages).Error
	if err != nil {
		return nil, err
	}

	for _, usage := range cheermoteUsages {
		usage.RemoveSpacePaddings()
	}
	return cheermoteUsages, err
}

func (d *dao) GetByTransactionIDAndCheermote(ctx context.Context, transactionID, cheermote string) (*models.CheermoteUsage, error) {
	if strings.Blank(transactionID) || strings.Blank(cheermote) {
		return nil, errors.New("transaction ID and cheermote must be provided")
	}

	var cheermoteUsages []*models.CheermoteUsage
	err := d.Table.Where(&models.CheermoteUsage{TransactionID: transactionID, Cheermote: cheermote}).First(&cheermoteUsages).Error
	if err != nil {
		return nil, err
	}

	if len(cheermoteUsages) == 0 {
		return nil, nil
	}

	cheermoteUsages[0].RemoveSpacePaddings()
	return cheermoteUsages[0], err
}

func (d *dao) BatchPut(ctx context.Context, cheermoteUsages []*models.CheermoteUsage) error {
	if len(cheermoteUsages) == 0 {
		return nil
	}

	tx := d.Table.Begin()
	if err := tx.Error; err != nil {
		return err
	}

	for _, usage := range cheermoteUsages {
		if usage == nil {
			continue
		}

		// make a copy here because GORM pads the PK field on the struct with spaces
		copy := *usage
		err := tx.Save(&copy).Error
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	return tx.Commit().Error
}
