package cheermote_usage

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/pachter/backend/rds"
	"code.justin.tv/commerce/pachter/backend/rds/models"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func TestDAO_BatchPut(t *testing.T) {
	client, err := rds.GetRDSClient(t)
	if err != nil {
		t.Errorf("Failed to initialize RDS client for tests %+v", err)
		return
	}
	dao := dao{
		Table: client,
	}

	ctx := context.Background()

	id1 := uuid.Must(uuid.NewV4()).String()
	input := &models.CheermoteUsage{
		TransactionID: id1,
		Cheermote:     "cheer",
		Datetime:      time.Now().UTC(),
		Quantity:      100,
	}

	id2 := uuid.Must(uuid.NewV4()).String()
	input2 := &models.CheermoteUsage{
		TransactionID: id2,
		Cheermote:     "hype",
		Datetime:      time.Now().UTC(),
		Quantity:      999,
	}

	id3 := uuid.Must(uuid.NewV4()).String()
	input3 := &models.CheermoteUsage{
		TransactionID: id3,
		Cheermote:     "fail",
		Datetime:      time.Now().UTC(),
		Quantity:      666,
	}

	Convey("given a list of cheermote usages", t, func() {
		inputs := []*models.CheermoteUsage{
			input,
			input2,
			input3,
		}

		Convey("saves all", func() {
			err := dao.BatchPut(ctx, inputs)
			So(err, ShouldBeNil)

			for _, i := range inputs {
				usage, err := dao.GetByTransactionIDAndCheermote(ctx, i.TransactionID, i.Cheermote)
				So(err, ShouldBeNil)
				So(usage, ShouldNotBeNil)
				So(usage, ShouldResemble, i)
			}
		})
	})

	Convey("given a list of cheermote usages with duplicates", t, func() {
		inputs := []*models.CheermoteUsage{
			input,
			input2,
			input3,
			input2,
		}

		Convey("saves all", func() {
			err := dao.BatchPut(ctx, inputs)
			So(err, ShouldBeNil)

			for _, i := range inputs {
				usage, err := dao.GetByTransactionIDAndCheermote(ctx, i.TransactionID, i.Cheermote)
				So(err, ShouldBeNil)
				So(usage, ShouldNotBeNil)
				So(usage, ShouldResemble, i)
			}
		})
	})

}
