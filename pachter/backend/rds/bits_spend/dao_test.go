package bits_spend

import (
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachter/backend/rds"
	"code.justin.tv/commerce/pachter/backend/rds/models"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func TestDAO(t *testing.T) {
	rdsClient, err := rds.GetRDSClient(t)
	if err != nil {
		t.Fail()
	}
	dao := dao{
		Table: rdsClient,
	}

	Convey("Given bits spend DAO", t, func() {
		transactionID := uuid.Must(uuid.NewV4()).String()
		requester := random.String(10)
		receiver := random.String(10)
		message := "just some message"
		cheermoteUsage := &models.CheermoteUsage{
			TransactionID:    transactionID,
			Cheermote:        "lul",
			Datetime:         time.Now().UTC(),
			Quantity:         100,
			PurchasePriceUSC: 5,
		}

		transaction := models.BitsTransaction{
			TransactionID:          transactionID,
			TransactionType:        "TEST",
			RequestingTwitchUserID: requester,
			ReceivingTwitchUserID:  receiver,
			Datetime:               time.Now().UTC(),
			Quantity:               100,
			Message:                &message,
			CheermoteUsages:        []*models.CheermoteUsage{cheermoteUsage},
		}

		Convey("Put", func() {
			err := dao.Put(transaction, "test-reference-tran-id", int64(140))
			So(err, ShouldBeNil)
		})

		Convey("BatchPut", func() {
			transactionID := uuid.Must(uuid.NewV4()).String()
			transactionID2 := uuid.Must(uuid.NewV4()).String()
			spends := []models.BitsSpend{
				{
					TransactionID: transactionID,
				},
				{
					TransactionID: transactionID2,
				},
			}

			err := dao.BatchPut(spends)
			So(err, ShouldBeNil)
		})
	})
}
