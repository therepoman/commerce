package bits_spend

import (
	"errors"

	"code.justin.tv/commerce/pachter/backend/rds/models"
	"github.com/jinzhu/gorm"
)

type DAO interface {
	Put(tx models.BitsTransaction, referenceTranID string, costUsc int64) error
	BatchPut(transactions []models.BitsSpend) error
	DeleteAllByRequestingUserID(userID string) error
}

type dao struct {
	Table *gorm.DB `inject:""`
}

func NewDAO() DAO {
	return &dao{}
}

func NewDAOWithClient(rdsClient *gorm.DB) DAO {
	return &dao{
		Table: rdsClient,
	}
}

func (dao *dao) Put(tx models.BitsTransaction, referenceTranID string, costUsc int64) error {
	if referenceTranID == "" {
		return errors.New("referenceTranID cannot be blank")
	}

	return dao.Table.Save(&models.BitsSpend{
		TransactionID:          tx.TransactionID,
		ReferenceTransactionID: referenceTranID,
		RequestingTwitchUserID: tx.RequestingTwitchUserID,
		ReceivingTwitchUserID:  tx.ReceivingTwitchUserID,
		Datetime:               tx.Datetime,
		Quantity:               tx.Quantity,
		PurchasePriceUSC:       costUsc,
	}).Error
}

func (dao *dao) BatchPut(transactions []models.BitsSpend) error {
	// GORM does not currently support a batch insert solution
	// opening up one transaction to insert all transactions here,
	// instead of one transaction per transaction which is what Table.Create does
	tx := dao.Table.Begin()

	for _, transaction := range transactions {
		t := transaction
		err := tx.Save(&t).Error
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	return tx.Commit().Error
}

func (dao *dao) DeleteAllByRequestingUserID(userID string) error {
	tx := dao.Table.Begin()
	tx.Delete(models.BitsSpend{}, "requesting_twitch_user_id = ?", userID)
	return tx.Commit().Error
}
