package get_bits_monthly_amounts_for_all_channels

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pachter/backend/rds"
	"code.justin.tv/commerce/pachter/config"
	pachter "code.justin.tv/commerce/pachter/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestAPI(t *testing.T) {
	rdsClient, err := rds.GetRDSClient(t)
	if err != nil {
		t.Errorf("Failed to initialize RDS client for tests %+v", err)
		return
	}

	Convey("Given report API", t, func() {
		cfg := &config.Config{}

		api := api{
			Config: cfg,
			RDS:    rdsClient,
		}
		ctx := context.Background()

		Convey("errors when request is nil", func() {
			_, err := api.Get(ctx, nil)
			So(err, ShouldNotBeNil)
		})

		Convey("errors when month is too low", func() {
			_, err := api.Get(ctx, &pachter.GetBitsMonthlyAmountsForAllChannelsReq{
				Month: 0,
				Year:  2049,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("errors when month is too high", func() {
			_, err := api.Get(ctx, &pachter.GetBitsMonthlyAmountsForAllChannelsReq{
				Month: 13,
				Year:  2049,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("errors when the year is too low", func() {
			_, err := api.Get(ctx, &pachter.GetBitsMonthlyAmountsForAllChannelsReq{
				Month: 2,
				Year:  1991,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("errors when the year is too high", func() {
			_, err := api.Get(ctx, &pachter.GetBitsMonthlyAmountsForAllChannelsReq{
				Month: 1,
				Year:  3001,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("when the request is valid", func() {
			req := &pachter.GetBitsMonthlyAmountsForAllChannelsReq{
				Month: 11,
				Year:  2019,
			}

			Convey("we should return without error", func() {
				resp, err := api.Get(ctx, req)
				So(err, ShouldBeNil)
				So(len(resp.BitsAmounts), ShouldBeGreaterThan, 0)
			})
		})
	})
}
