package get_bits_monthly_amounts_for_all_channels

import (
	"context"
	"strings"
	"time"

	"code.justin.tv/commerce/pachter/backend/rds/models"
	time_utils "code.justin.tv/commerce/pachter/backend/utils/time"
	"code.justin.tv/commerce/pachter/config"
	pachter "code.justin.tv/commerce/pachter/rpc"
	"github.com/jinzhu/gorm"
	"github.com/twitchtv/twirp"
)

const (
	// Due to a backfill issue, some records have this incorrect transaction type instead of "GiveBitsToBroadcaster"
	// TODO: Remove this once we have performed a backfill to update these transactions to the correct type
	TransactionType_Cheering_BackfillArtifact models.TransactionType = "GiveBitsToBroadcasterType"
)

type API interface {
	Get(context.Context, *pachter.GetBitsMonthlyAmountsForAllChannelsReq) (*pachter.GetBitsMonthlyAmountsForAllChannelsResp, error)
}

type api struct {
	Config *config.Config `inject:""`
	RDS    *gorm.DB       `inject:""`
}

func NewAPI() API {
	return &api{}
}

type ChannelBitsAmount struct {
	ChannelID  string
	BitsAmount uint32
}

func (api *api) Get(ctx context.Context, req *pachter.GetBitsMonthlyAmountsForAllChannelsReq) (*pachter.GetBitsMonthlyAmountsForAllChannelsResp, error) {
	_, cancel := context.WithTimeout(ctx, 30*time.Second)
	defer cancel()

	if req == nil {
		return nil, twirp.InvalidArgumentError("request", "cannot be empty")
	}
	if req.Month <= 0 || req.Month > 12 {
		return nil, twirp.InvalidArgumentError("requested month", "is invalid")
	}
	if req.Year < 2000 || req.Year > 3000 {
		return nil, twirp.InvalidArgumentError("requested year", "is invalid")
	}

	var rows []ChannelBitsAmount
	startDate, endDate := time_utils.GetStartEndDate(req.Month, req.Year)
	err := api.RDS.Table("bits_transactions").
		Select("receiving_twitch_user_id as channel_id, sum(quantity + sponsored_quantity) as bits_amount").
		Where("(transaction_type = ? OR transaction_type = ?) AND (? <= datetime AND datetime <= ?)",
			string(models.TransactionType_Cheering), string(TransactionType_Cheering_BackfillArtifact), startDate, endDate).
		Group("receiving_twitch_user_id").
		Scan(&rows).Error
	if err != nil {
		return nil, twirp.InternalError("Error getting data from RDS")
	}

	channelBitsMap := make(map[string]uint32)
	for _, row := range rows {
		channelBitsMap[strings.TrimSpace(row.ChannelID)] = row.BitsAmount
	}

	return &pachter.GetBitsMonthlyAmountsForAllChannelsResp{
		BitsAmounts: channelBitsMap,
	}, nil
}
