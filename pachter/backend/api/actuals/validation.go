package actuals

import (
	pachter "code.justin.tv/commerce/pachter/rpc"
	"github.com/twitchtv/twirp"
)

func validateMonth(month uint32) error {
	if month == 0 || month > 12 {
		return twirp.InvalidArgumentError("month", "is invalid")
	}
	return nil
}

func validateYear(year uint32) error {
	if year < 2016 {
		return twirp.InvalidArgumentError("year", "is invalid")
	}
	return nil
}

func validatePlatform(platform pachter.Platform) error {
	_, ok := pachter.Platform_name[int32(platform)]

	if !ok || platform == pachter.Platform_NONE {
		return twirp.InvalidArgumentError("platform", "is invalid")
	}
	return nil
}
