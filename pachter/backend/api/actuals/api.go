package actuals

import (
	"context"

	"code.justin.tv/commerce/pachter/backend/dynamo/actuals"
	pachter "code.justin.tv/commerce/pachter/rpc"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/twitchtv/twirp"
)

type API interface {
	Get(ctx context.Context, req *pachter.GetActualsReq) (*pachter.GetActualsResp, error)
	Set(ctx context.Context, req *pachter.SetActualsReq) (*pachter.SetActualsResp, error)
}

type api struct {
	DAO    actuals.DAO   `inject:""`
	Petozi petozi.Petozi `inject:""`
}

func NewAPI() API {
	return &api{}
}

func (a *api) Get(ctx context.Context, req *pachter.GetActualsReq) (*pachter.GetActualsResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("request", "cannot be empty")
	}

	if err := validateYear(req.Year); err != nil {
		return nil, err
	} else if err := validateMonth(req.Month); err != nil {
		return nil, err
	} else if err := validatePlatform(req.Platform); err != nil {
		return nil, err
	}

	values, err := a.DAO.Get(ctx, req.Platform, req.Year, req.Month)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	petoziProductIds, err := a.getProductsByPlatform(ctx, req.Platform)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	return &pachter.GetActualsResp{Actuals: combine(petoziProductIds, values)}, nil
}

func (a *api) Set(ctx context.Context, req *pachter.SetActualsReq) (*pachter.SetActualsResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("request", "cannot be empty")
	}

	if err := validateYear(req.Year); err != nil {
		return nil, err
	} else if err := validateMonth(req.Month); err != nil {
		return nil, err
	} else if err := validatePlatform(req.Platform); err != nil {
		return nil, err
	}

	err := a.DAO.Put(ctx, req.Platform, req.Year, req.Month, sanitizeActuals(req.Actuals))
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	return &pachter.SetActualsResp{}, nil
}

func (a *api) getProductsByPlatform(ctx context.Context, platform pachter.Platform) ([]string, error) {
	resp, err := a.Petozi.GetBitsProducts(ctx, &petozi.GetBitsProductsReq{})
	if err != nil || resp == nil || resp.BitsProducts == nil {
		return nil, err
	}

	petoziPlatform := petozizePlatform(platform)
	productIds := make([]string, 0)
	for _, product := range resp.BitsProducts {
		if product != nil && product.Platform == petoziPlatform {
			productIds = append(productIds, product.Id)
		}
	}

	return productIds, nil
}

func combine(productIds []string, values map[string]uint32) map[string]uint32 {
	result := make(map[string]uint32)

	for _, productId := range productIds {
		value, ok := values[productId]
		if ok {
			result[productId] = value
		} else {
			result[productId] = uint32(0)
		}
	}

	return result
}

func sanitizeActuals(actuals map[string]uint32) map[string]uint32 {
	sanitizedActuals := make(map[string]uint32)
	for key, entry := range actuals {
		if entry > 0 {
			sanitizedActuals[key] = entry
		}
	}
	return sanitizedActuals
}
