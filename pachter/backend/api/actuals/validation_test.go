package actuals

import (
	"math/rand"
	"testing"

	pachter "code.justin.tv/commerce/pachter/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestMonthValidation(t *testing.T) {
	monthError := twirp.InvalidArgumentError("month", "is invalid")

	Convey("given nothing", t, func() {
		So(validateMonth(0), ShouldResemble, monthError)
		So(validateMonth(13), ShouldResemble, monthError)
		So(validateMonth(7), ShouldBeNil)
	})
}

func TestYearValidation(t *testing.T) {
	yearError := twirp.InvalidArgumentError("year", "is invalid")

	Convey("given nothing", t, func() {
		So(validateYear(0), ShouldResemble, yearError)
		So(validateYear(2006), ShouldResemble, yearError)
		So(validateYear(uint32(rand.Int()+2015)), ShouldBeNil)
	})
}

func TestPlatformValidation(t *testing.T) {
	platformError := twirp.InvalidArgumentError("platform", "is invalid")

	Convey("given nothing", t, func() {
		So(validatePlatform(0), ShouldResemble, platformError)
		So(validatePlatform(1099), ShouldResemble, platformError)
		So(validatePlatform(pachter.Platform_AMAZON), ShouldBeNil)
		So(validatePlatform(pachter.Platform_XSOLLA), ShouldBeNil)
	})
}
