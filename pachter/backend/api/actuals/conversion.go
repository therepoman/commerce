package actuals

import (
	pachter "code.justin.tv/commerce/pachter/rpc"
	petozi "code.justin.tv/commerce/petozi/rpc"
)

// I was trying to avoid writing this block.
func petozizePlatform(platform pachter.Platform) petozi.Platform {
	switch platform {
	case pachter.Platform_AMAZON:
		return petozi.Platform_AMAZON
	case pachter.Platform_PAYPAL:
		return petozi.Platform_PAYPAL
	case pachter.Platform_IOS:
		return petozi.Platform_IOS
	case pachter.Platform_ANDROID:
		return petozi.Platform_ANDROID
	case pachter.Platform_XSOLLA:
		return petozi.Platform_XSOLLA
	case pachter.Platform_PHANTO:
		return petozi.Platform_PHANTO
	case pachter.Platform_STRIPE:
		return petozi.Platform_STRIPE
	case pachter.Platform_VANTIV:
		return petozi.Platform_VANTIV
	case pachter.Platform_UNKNOWN:
		return petozi.Platform_UNKNOWN
	case pachter.Platform_ADYEN:
		return petozi.Platform_ADYEN
	case pachter.Platform_WORLDPAY:
		return petozi.Platform_WORLDPAY
	case pachter.Platform_ADYEN_LUX:
		return petozi.Platform_ADYEN_LUX
	case pachter.Platform_WORLDPAY_LUX:
		return petozi.Platform_WORLDPAY_LUX
	case pachter.Platform_NONE:
		return petozi.Platform_NONE
	default:
		return petozi.Platform_NONE
	}
}
