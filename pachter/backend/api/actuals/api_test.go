package actuals

import (
	"context"
	"errors"
	"fmt"
	"testing"

	dao_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/dynamo/actuals"
	petozi_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/petozi/rpc"
	pachter "code.justin.tv/commerce/pachter/rpc"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"github.com/twitchtv/twirp"
)

func TestAPI(t *testing.T) {
	ctx := context.Background()

	animeBearError := errors.New("NO MORE ANIME BEARS")
	year := uint32(2099)
	month := uint32(9)
	platform := pachter.Platform_AMAZON
	product := petozi.BitsProduct{
		Id:       "SPECIAL-SKU-1",
		Platform: petozi.Platform_AMAZON,
	}

	products := make([]*petozi.BitsProduct, 1)
	products[0] = &product

	Convey("Given actuals API", t, func() {
		petoziClient := new(petozi_mock.Petozi)
		dao := new(dao_mock.DAO)

		api := api{
			DAO:    dao,
			Petozi: petoziClient,
		}

		var req *pachter.GetActualsReq
		Convey("get", func() {
			_, err := api.Get(ctx, req)
			So(err, ShouldNotBeNil)

			twirpErr := unpackTwirpError(err)
			So(twirpErr.Code(), ShouldEqual, twirp.InvalidArgument)
			So(twirpErr.Meta("argument"), ShouldEqual, "request")

			Convey("with a request", func() {
				req := &pachter.GetActualsReq{}

				_, err := api.Get(ctx, req)
				So(err, ShouldNotBeNil)

				twirpErr := unpackTwirpError(err)
				So(twirpErr.Code(), ShouldEqual, twirp.InvalidArgument)
				So(twirpErr.Meta("argument"), ShouldEqual, "year")

				Convey("with a year", func() {
					req.Year = year
					_, err := api.Get(ctx, req)
					So(err, ShouldNotBeNil)

					twirpErr := unpackTwirpError(err)
					So(twirpErr.Code(), ShouldEqual, twirp.InvalidArgument)
					So(twirpErr.Meta("argument"), ShouldEqual, "month")

					Convey("with a month", func() {
						req.Month = month
						_, err := api.Get(ctx, req)
						So(err, ShouldNotBeNil)

						twirpErr := unpackTwirpError(err)
						So(twirpErr.Code(), ShouldEqual, twirp.InvalidArgument)
						So(twirpErr.Meta("argument"), ShouldEqual, "platform")

						Convey("with a platform", func() {
							req.Platform = platform

							Convey("with a failing DAO", func() {
								dao.On("Get", ctx, platform, year, month).Return(nil, animeBearError)
								resp, err := api.Get(ctx, req)
								So(resp, ShouldBeNil)
								twirpErr := unpackTwirpError(err)
								So(twirpErr.Meta("cause"), ShouldEqual, fmt.Sprintf("%T", animeBearError))
							})

							Convey("with a DAO that returns nil", func() {
								dao.On("Get", ctx, platform, year, month).Return(nil, nil)

								Convey("with a Petozi client that returns an error", func() {
									petoziClient.On("GetBitsProducts", ctx, mock.Anything).Return(nil, animeBearError)
									resp, err := api.Get(ctx, req)
									So(resp, ShouldBeNil)
									twirpErr := unpackTwirpError(err)
									So(twirpErr.Meta("cause"), ShouldEqual, fmt.Sprintf("%T", animeBearError))
								})

								Convey("with a Petozi client that returns nil", func() {
									petoziClient.On("GetBitsProducts", ctx, mock.Anything).Return(nil, nil)
									resp, err := api.Get(ctx, req)
									So(err, ShouldBeNil)
									So(resp, ShouldResemble, &pachter.GetActualsResp{Actuals: map[string]uint32{}})
								})

								Convey("with a Petozi that returns a product", func() {
									petoziClient.On("GetBitsProducts", ctx, mock.Anything).Return(&petozi.GetBitsProductsResp{
										BitsProducts: products,
									}, nil)
									resp, err := api.Get(ctx, req)
									So(err, ShouldBeNil)
									So(resp, ShouldResemble, &pachter.GetActualsResp{Actuals: map[string]uint32{
										"SPECIAL-SKU-1": uint32(0),
									}})
								})
							})
						})
					})
				})
			})
		})
	})
}

func unpackTwirpError(err error) twirp.Error {
	So(err, ShouldNotBeNil)
	twirpErr, ok := err.(twirp.Error)
	So(ok, ShouldBeTrue)
	return twirpErr
}
