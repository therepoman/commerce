package actuals

import (
	"testing"

	pachter "code.justin.tv/commerce/pachter/rpc"
	petozi "code.justin.tv/commerce/petozi/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPetozification(t *testing.T) {
	Convey("given nothing", t, func() {
		So(petozizePlatform(pachter.Platform_NONE), ShouldEqual, petozi.Platform_NONE)
		So(petozizePlatform(pachter.Platform_AMAZON), ShouldEqual, petozi.Platform_AMAZON)
		So(petozizePlatform(pachter.Platform_PAYPAL), ShouldEqual, petozi.Platform_PAYPAL)
		So(petozizePlatform(pachter.Platform_IOS), ShouldEqual, petozi.Platform_IOS)
		So(petozizePlatform(pachter.Platform_ANDROID), ShouldEqual, petozi.Platform_ANDROID)
		So(petozizePlatform(pachter.Platform_XSOLLA), ShouldEqual, petozi.Platform_XSOLLA)
		So(petozizePlatform(pachter.Platform_PHANTO), ShouldEqual, petozi.Platform_PHANTO)
		So(petozizePlatform(pachter.Platform(-1)), ShouldEqual, petozi.Platform_NONE)
	})
}
