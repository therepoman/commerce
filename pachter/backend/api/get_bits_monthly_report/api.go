package get_bits_monthly_report

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/aws/s3"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachter/backend/worker/bits_monthly_report"
	"code.justin.tv/commerce/pachter/config"
	pachter "code.justin.tv/commerce/pachter/rpc"
	"github.com/twitchtv/twirp"
)

const (
	TempSignedURLExpiryTime = 30 * time.Minute
)

type API interface {
	Get(context.Context, *pachter.GetBitsMonthlyReportReq) (*pachter.GetBitsMonthlyReportResp, error)
}

type api struct {
	Config   *config.Config `inject:""`
	S3Client s3.Client      `inject:""`
}

func NewAPI() API {
	return &api{}
}

func (api *api) Get(ctx context.Context, req *pachter.GetBitsMonthlyReportReq) (*pachter.GetBitsMonthlyReportResp, error) {
	_, cancel := context.WithTimeout(ctx, 30*time.Second)
	defer cancel()

	if req == nil {
		return &pachter.GetBitsMonthlyReportResp{}, twirp.InvalidArgumentError("request", "cannot be empty")
	}
	if req.Month == 0 || req.Month > 12 {
		return &pachter.GetBitsMonthlyReportResp{}, twirp.InvalidArgumentError("requested month", "is invalid")
	}

	s3Key := fmt.Sprintf(bits_monthly_report.ReportS3KeyFormat, req.Year, req.Month)
	reportURL, err := api.S3Client.GetTempSignedURL(ctx, api.Config.BitsMonthlyReportBucket, s3Key, TempSignedURLExpiryTime)
	if err != nil {
		logrus.WithError(err).Error("Error getting s3 temp signed URL")
		return &pachter.GetBitsMonthlyReportResp{}, twirp.InternalError("failed to get report")
	}

	return &pachter.GetBitsMonthlyReportResp{
		ReportUrl: reportURL,
	}, nil
}
