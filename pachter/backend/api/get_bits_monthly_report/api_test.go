package get_bits_monthly_report

import (
	"context"
	"errors"
	"fmt"
	"testing"

	"code.justin.tv/commerce/pachter/backend/worker/bits_monthly_report"
	"code.justin.tv/commerce/pachter/config"
	s3_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/gogogadget/aws/s3"
	pachter "code.justin.tv/commerce/pachter/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestAPI(t *testing.T) {
	Convey("Given report API", t, func() {
		year := uint32(2020)
		month := uint32(2)
		s3Key := fmt.Sprintf(bits_monthly_report.ReportS3KeyFormat, year, month)
		bucket := "test-non-existent-bucket"
		cfg := &config.Config{
			BitsMonthlyReportBucket: bucket,
		}
		s3Client := new(s3_mock.Client)
		api := api{
			Config:   cfg,
			S3Client: s3Client,
		}
		ctx := context.Background()

		Convey("when request is nil", func() {
			resp, err := api.Get(ctx, nil)

			So(err, ShouldNotBeNil)
			So(resp, ShouldResemble, &pachter.GetBitsMonthlyReportResp{})
			s3Client.AssertNumberOfCalls(t, "GetTempSignedURL", 0)
		})

		Convey("when requested month is invalid", func() {
			resp, err := api.Get(ctx, &pachter.GetBitsMonthlyReportReq{
				Month: 13,
			})

			So(err, ShouldNotBeNil)
			So(resp, ShouldResemble, &pachter.GetBitsMonthlyReportResp{})
			s3Client.AssertNumberOfCalls(t, "GetTempSignedURL", 0)
		})

		Convey("when s3 client fails", func() {
			s3Client.On("GetTempSignedURL", ctx, bucket, s3Key, TempSignedURLExpiryTime).
				Return("", errors.New("shame")).Once()

			resp, err := api.Get(ctx, &pachter.GetBitsMonthlyReportReq{
				Month: month,
				Year:  year,
			})

			So(err, ShouldNotBeNil)
			So(resp, ShouldResemble, &pachter.GetBitsMonthlyReportResp{})
			s3Client.AssertNumberOfCalls(t, "GetTempSignedURL", 1)
		})

		Convey("when s3 client succeeds", func() {
			s3Client.On("GetTempSignedURL", ctx, bucket, s3Key, TempSignedURLExpiryTime).
				Return("http://ohkicomics.com", nil).Once()

			resp, err := api.Get(ctx, &pachter.GetBitsMonthlyReportReq{
				Month: month,
				Year:  year,
			})

			So(err, ShouldBeNil)
			So(resp.ReportUrl, ShouldEqual, "http://ohkicomics.com")
			s3Client.AssertNumberOfCalls(t, "GetTempSignedURL", 1)
		})
	})
}
