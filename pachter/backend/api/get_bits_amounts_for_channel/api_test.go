package get_bits_amounts_for_channel

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/pachter/backend/rds"
	"code.justin.tv/commerce/pachter/config"
	pachter "code.justin.tv/commerce/pachter/rpc"
	"github.com/golang/protobuf/ptypes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestAPI(t *testing.T) {
	rdsClient, err := rds.GetRDSClient(t)
	if err != nil {
		t.Errorf("Failed to initialize RDS client for tests %+v", err)
		return
	}

	Convey("Given report API", t, func() {
		cfg := &config.Config{}
		api := api{
			Config: cfg,
			RDS:    rdsClient,
		}
		ctx := context.Background()

		startDate, err := ptypes.TimestampProto(time.Now().Add(-5 * 365 * 24 * time.Hour))
		So(err, ShouldBeNil)

		endDate, err := ptypes.TimestampProto(time.Now().Add(5 * 365 * 24 * time.Hour))
		So(err, ShouldBeNil)

		channelID := "145903336" // qa_bits_tests
		period := pachter.Period_MONTH

		Convey("errors when request is nil", func() {
			_, err := api.Get(ctx, nil)
			So(err, ShouldNotBeNil)
		})

		Convey("errors when the channel is blank", func() {
			_, err = api.Get(ctx, &pachter.GetBitsAmountsForChannelReq{
				StartDate: startDate,
				EndDate:   endDate,
				ChannelId: "",
				Period:    period,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("errors when requested start date is invalid", func() {
			_, err := api.Get(ctx, &pachter.GetBitsAmountsForChannelReq{
				StartDate: nil,
				EndDate:   endDate,
				ChannelId: channelID,
				Period:    period,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("errors when requested end date is invalid", func() {
			_, err := api.Get(ctx, &pachter.GetBitsAmountsForChannelReq{
				StartDate: startDate,
				EndDate:   nil,
				ChannelId: channelID,
				Period:    period,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("errors when the start date is too far in the past", func() {
			theDistantPast, err := ptypes.TimestampProto(time.Date(1, time.February, 3, 4, 5, 6, 7, time.UTC))
			So(err, ShouldBeNil)
			_, err = api.Get(ctx, &pachter.GetBitsAmountsForChannelReq{
				StartDate: theDistantPast,
				EndDate:   endDate,
				ChannelId: channelID,
				Period:    period,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("errors when the end date is too far in the future", func() {
			theDistantFuture, err := ptypes.TimestampProto(time.Date(3001, time.April, 20, 0, 0, 0, 0, time.UTC))
			So(err, ShouldBeNil)
			_, err = api.Get(ctx, &pachter.GetBitsAmountsForChannelReq{
				StartDate: startDate,
				EndDate:   theDistantFuture,
				ChannelId: channelID,
				Period:    period,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("errors when the period is invalid", func() {
			So(err, ShouldBeNil)
			_, err = api.Get(ctx, &pachter.GetBitsAmountsForChannelReq{
				StartDate: startDate,
				EndDate:   endDate,
				ChannelId: channelID,
				Period:    pachter.Period(9999),
			})
			So(err, ShouldNotBeNil)
		})

		Convey("when the request is valid", func() {
			req := &pachter.GetBitsAmountsForChannelReq{
				ChannelId: channelID,
				StartDate: startDate,
				EndDate:   endDate,
			}

			Convey("we should return without error", func() {
				resp, err := api.Get(ctx, req)
				So(err, ShouldBeNil)
				So(len(resp.BitsAmounts), ShouldBeGreaterThan, 0)
			})
		})
	})
}
