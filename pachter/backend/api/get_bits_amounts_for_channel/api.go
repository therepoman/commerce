package get_bits_amounts_for_channel

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/pachter/backend/rds/models"
	time_utils "code.justin.tv/commerce/pachter/backend/utils/time"
	"code.justin.tv/commerce/pachter/config"
	pachter "code.justin.tv/commerce/pachter/rpc"
	"github.com/golang/protobuf/ptypes"
	"github.com/jinzhu/gorm"
	"github.com/twitchtv/twirp"
)

const (
	// Due to a backfill issue, some records have this incorrect transaction type instead of "GiveBitsToBroadcaster"
	// TODO: Remove this once we have performed a backfill to update these transactions to the correct type
	TransactionType_Cheering_BackfillArtifact models.TransactionType = "GiveBitsToBroadcasterType"
)

type API interface {
	Get(context.Context, *pachter.GetBitsAmountsForChannelReq) (*pachter.GetBitsAmountsForChannelResp, error)
}

type api struct {
	Config *config.Config `inject:""`
	RDS    *gorm.DB       `inject:""`
}

func NewAPI() API {
	return &api{}
}

type PeriodBitsAmount struct {
	Time       time.Time
	BitsAmount uint32
}

func (api *api) Get(ctx context.Context, req *pachter.GetBitsAmountsForChannelReq) (*pachter.GetBitsAmountsForChannelResp, error) {
	_, cancel := context.WithTimeout(ctx, 30*time.Second)
	defer cancel()

	if req == nil {
		return nil, twirp.InvalidArgumentError("request", "cannot be empty")
	}
	if req.ChannelId == "" {
		return nil, twirp.InvalidArgumentError("requested channel_id", "cannot be blank")
	}

	startDate, err := ptypes.Timestamp(req.StartDate)
	if err != nil {
		return nil, twirp.InvalidArgumentError("request start_date", "is invalid")
	}

	endDate, err := ptypes.Timestamp(req.EndDate)
	if err != nil {
		return nil, twirp.InvalidArgumentError("request end_date", "is invalid")
	}

	if startDate.Year() < 2000 || endDate.Year() > 3000 {
		return nil, twirp.InvalidArgumentError("request date range", "is invalid")
	}

	periodName, validPeriod := pachter.Period_name[int32(req.Period)]
	if !validPeriod {
		return nil, twirp.InvalidArgumentError("request period", "is invalid")
	}

	startTime, err := time_utils.RoundDown(startDate, req.Period)
	if err != nil {
		return nil, twirp.InternalError("error rounding start_date")
	}

	endTime, err := time_utils.RoundUp(endDate, req.Period)
	if err != nil {
		return nil, twirp.InternalError("error rounding end_date")
	}

	var rows []PeriodBitsAmount
	err = api.RDS.Table("bits_transactions").
		Select(fmt.Sprintf("date_trunc('%s', datetime) as time, sum(quantity + sponsored_quantity) as bits_amount", periodName)).
		Where("receiving_twitch_user_id = ? AND (transaction_type = ? OR transaction_type = ?) AND (? <= datetime AND datetime <= ?)",
			req.ChannelId, string(models.TransactionType_Cheering), string(TransactionType_Cheering_BackfillArtifact), startTime, endTime).
		Group(fmt.Sprintf("date_trunc('%s', datetime)", periodName)).
		Scan(&rows).Error
	if err != nil {
		return nil, twirp.InternalError("error getting data from RDS")
	}

	bitsAmounts := make([]*pachter.BitsPeriodAmount, len(rows))
	for i, row := range rows {
		timeProto, err := ptypes.TimestampProto(row.Time)
		if err != nil {
			return nil, twirp.InternalError("rds returned invalid time format")
		}

		bitsAmounts[i] = &pachter.BitsPeriodAmount{
			Time:   timeProto,
			Amount: row.BitsAmount,
		}
	}

	return &pachter.GetBitsAmountsForChannelResp{
		BitsAmounts: bitsAmounts,
	}, nil
}
