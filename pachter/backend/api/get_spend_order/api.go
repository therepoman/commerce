package get_spend_order

import (
	"context"
	"strings"

	"code.justin.tv/commerce/logrus"
	pachter "code.justin.tv/commerce/pachter/rpc"
	"github.com/jinzhu/gorm"
	"github.com/twitchtv/twirp"
)

type API interface {
	Get(ctx context.Context, req *pachter.GetBitsSpendOrderReq) (*pachter.GetBitsSpendOrderResp, error)
}

type api struct {
	RDS *gorm.DB `inject:""`
}

func NewAPI() API {
	return &api{}
}

func (api *api) Get(ctx context.Context, req *pachter.GetBitsSpendOrderReq) (*pachter.GetBitsSpendOrderResp, error) {
	type r struct {
		BitsType string `gorm:"bits_type"`
		Quantity uint32 `gorm:"quantity"`
	}

	queryResult := []r{}
	err := api.RDS.Table("bits_spends").
		Select("bits_transactions.bits_type, bits_spends.quantity").
		Joins("inner join bits_transactions on bits_spends.reference_transaction_id = bits_transactions.transaction_id").
		Where("bits_spends.transaction_id = ?", req.TransactionId).Find(&queryResult).Error

	if err != nil {
		logrus.WithField("transactionID", req.TransactionId).
			WithError(err).Error("Failed to get originating Bits type for transaction")

		return nil, twirp.InternalError("Failed to query data")
	}

	bitsTypeToAmount := make(map[string]uint32)
	for _, record := range queryResult {
		bitsTypeToAmount[strings.TrimSpace(record.BitsType)] += record.Quantity
	}

	if len(bitsTypeToAmount) == 0 {
		logrus.WithField("transactionID", req.TransactionId).Warn("SpendOrder API: returning no result")
	}

	return &pachter.GetBitsSpendOrderResp{
		BitsTypes: bitsTypeToAmount,
	}, nil
}
