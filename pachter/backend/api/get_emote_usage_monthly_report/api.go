package get_emote_usage_monthly_report

import (
	"bytes"
	"context"
	"encoding/csv"
	"fmt"
	"strconv"
	"time"

	"code.justin.tv/commerce/gogogadget/aws/s3"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachter/backend/dynamo/bits_cheermote_payout_config"
	dynamo_models "code.justin.tv/commerce/pachter/backend/dynamo/models"
	"code.justin.tv/commerce/pachter/backend/rds/models"
	time_utils "code.justin.tv/commerce/pachter/backend/utils/time"
	"code.justin.tv/commerce/pachter/config"
	pachter "code.justin.tv/commerce/pachter/rpc"
	"github.com/gofrs/uuid"
	"github.com/jinzhu/gorm"
	"github.com/twitchtv/twirp"
)

const (
	TempSignedURLExpiryTime = 5 * time.Minute
)

type API interface {
	Get(ctx context.Context, req *pachter.GetEmoteUsageMonthlyReportReq) (*pachter.GetEmoteUsageMonthlyReportResp, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	Config   *config.Config                   `inject:""`
	S3Client s3.Client                        `inject:""`
	RDS      *gorm.DB                         `inject:""`
	DAO      bits_cheermote_payout_config.DAO `inject:""`
}

func (a *api) Get(ctx context.Context, req *pachter.GetEmoteUsageMonthlyReportReq) (*pachter.GetEmoteUsageMonthlyReportResp, error) {
	if req == nil {
		return nil, twirp.InvalidArgumentError("request", "cannot be empty")
	}
	if req.Year < 1 {
		return nil, twirp.InvalidArgumentError("year", "is invalid")
	}
	if req.Month == 0 || req.Month > 12 {
		return nil, twirp.InvalidArgumentError("requested month", "is invalid")
	}

	startDate, endDate := time_utils.GetStartEndDate(req.Month, req.Year)

	var cheermoteUsages []models.CheermoteUsage
	err := a.RDS.Where("? <= datetime AND datetime <= ?", startDate, endDate).Find(&cheermoteUsages).Error
	if err != nil && !gorm.IsRecordNotFoundError(err) {
		logrus.WithError(err).Error("Error querying cheermote usages table")
		return nil, twirp.InternalError("downstream dependency error")
	}

	cheermoteUsageMap := map[string]int64{}
	for _, cheermoteUsage := range cheermoteUsages {
		cheermoteUsage.RemoveSpacePaddings()
		cheermoteUsageMap[cheermoteUsage.Cheermote] += cheermoteUsage.Quantity
	}

	byteBuffer := bytes.NewBuffer([]byte{})
	csvWriter := csv.NewWriter(byteBuffer)

	err = csvWriter.Write(getReportCSVHeader())
	if err != nil {
		logrus.WithError(err).Error("error generating emote usage report with header")
		return nil, twirp.InternalError("report initialization failed")
	}

	for cheermote, usage := range cheermoteUsageMap {
		rows, err := a.makeRows(ctx, cheermote, usage)
		if err != nil {
			logrus.WithField("cheermote", cheermote).WithError(err).Error("could not make row for emote usage report")
			return nil, twirp.InternalError("could not make row for emote usage report")
		}

		for _, row := range rows {
			err = csvWriter.Write(row)
			if err != nil {
				logrus.WithField("cheermote", cheermote).WithError(err).Error("could not write row for emote usage report")
				return nil, twirp.InternalError("could not write row for emote usage report")
			}
		}
	}
	csvWriter.Flush()

	reportID := fmt.Sprintf("%d-%d-%s", req.Year, req.Month, uuid.Must(uuid.NewV4()).String())
	err = a.S3Client.PutFile(ctx, a.Config.BitsEmoteUsageMonthlyReportBucket, s3Key(reportID), bytes.NewReader(byteBuffer.Bytes()))
	if err != nil {
		logrus.WithField("reportID", reportID).WithError(err).Error("Error putting emote usage monthly report into S3 bucket")
		return nil, twirp.InternalError("downstream dependency error (s3)")
	}

	reportURL, err := a.S3Client.GetTempSignedURL(ctx, a.Config.BitsEmoteUsageMonthlyReportBucket, s3Key(reportID), TempSignedURLExpiryTime)
	if err != nil {
		logrus.WithField("reportID", reportID).WithError(err).Error("Error getting temporary signed URL")
		return nil, twirp.InternalError("failed to get report")
	}

	return &pachter.GetEmoteUsageMonthlyReportResp{
		ReportUrl: reportURL,
	}, nil
}

func (a *api) makeRows(ctx context.Context, cheermote string, bitsConsumed int64) ([][]string, error) {
	rows := make([][]string, 0)

	payoutConfigResp, err := a.DAO.GetByPrefix(ctx, cheermote)
	if err != nil {
		return rows, err
	}

	if len(payoutConfigResp) == 0 {
		return rows, nil
	}

	for _, payoutConfig := range payoutConfigResp {
		row := make([]string, 6)

		if payoutConfig.TwitchUserID != dynamo_models.EmptyTwitchUserID {
			row[0] = payoutConfig.TwitchUserID
		}
		row[1] = payoutConfig.PayoutEntity
		row[2] = cheermote
		row[3] = strconv.FormatInt(bitsConsumed, 10)
		row[4] = strconv.FormatFloat(payoutConfig.Ratio, 'f', 3, 64)

		totalPayout := float64(bitsConsumed) * payoutConfig.Ratio * 0.01
		row[5] = strconv.FormatFloat(totalPayout, 'f', 2, 64)

		rows = append(rows, row)
	}

	return rows, nil
}

func getReportCSVHeader() []string {
	return []string{
		"Twitch ID",
		"Other Payout Entity",
		"Emote",
		"Bits Consumed",
		"Revenue Share",
		"Total Payout",
	}
}

func s3Key(id string) string {
	return fmt.Sprintf("bits-emote-usage-report-%s.csv", id)
}
