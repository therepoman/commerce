package get_emote_usage_monthly_report

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pachter/backend/dynamo/models"
	"code.justin.tv/commerce/pachter/backend/rds"
	"code.justin.tv/commerce/pachter/config"
	s3_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/gogogadget/aws/s3"
	dao_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/dynamo/bits_cheermote_payout_config"
	pachter "code.justin.tv/commerce/pachter/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestApi_Get(t *testing.T) {
	rdsClient, err := rds.GetRDSClient(t)
	if err != nil {
		t.Errorf("Failed to initialize RDS client for tests %+v", err)
		return
	}

	Convey("given a monthly emote report generator api", t, func() {
		cfg := &config.Config{
			BitsEmoteUsageMonthlyReportBucket: "test-non-existent-bucket",
		}
		s3Client := new(s3_mock.Client)
		dao := new(dao_mock.DAO)

		a := &api{
			Config:   cfg,
			S3Client: s3Client,
			RDS:      rdsClient,
			DAO:      dao,
		}

		ctx := context.Background()

		Convey("when the request is nil", func() {
			_, err := a.Get(ctx, nil)
			So(err, ShouldNotBeNil)
		})

		Convey("when the request has an invalid year", func() {
			_, err := a.Get(ctx, &pachter.GetEmoteUsageMonthlyReportReq{})
			So(err, ShouldNotBeNil)
		})

		Convey("when the request has an invalid month", func() {
			_, err := a.Get(ctx, &pachter.GetEmoteUsageMonthlyReportReq{Year: 2019, Month: 15})
			So(err, ShouldNotBeNil)
		})

		Convey("when the request is valid", func() {
			req := &pachter.GetEmoteUsageMonthlyReportReq{
				Year:  2019,
				Month: 6,
			}

			Convey("when we fail to look up emote payout info", func() {
				dao.On("GetByPrefix", ctx, mock.Anything).Return(nil, errors.New("WALRUS STRIKE"))

				_, err := a.Get(ctx, req)
				So(err, ShouldNotBeNil)
			})

			Convey("when the emote payout info is blank", func() {
				dao.On("GetByPrefix", ctx, mock.Anything).Return(nil, nil)

				Convey("when we fail to put the file", func() {
					s3Client.On("PutFile", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("s3 ouch")).Once()
					_, err := a.Get(ctx, req)

					So(err, ShouldNotBeNil)
				})

				Convey("when we succeed to put the file", func() {
					s3Client.On("PutFile", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()

					Convey("when we fail to get the presigned URL", func() {
						s3Client.On("GetTempSignedURL", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return("", errors.New("s3 ouch")).Once()

						_, err := a.Get(ctx, req)
						So(err, ShouldNotBeNil)
					})

					Convey("when we succeed to get the presigned URL", func() {
						s3Client.On("GetTempSignedURL", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return("test-report-URL", nil).Once()

						resp, err := a.Get(ctx, req)
						So(err, ShouldBeNil)
						So(resp, ShouldNotBeNil)
						So(resp.ReportUrl, ShouldEqual, "test-report-URL")
					})
				})
			})

			Convey("when the emote payout info is successfully retrieved", func() {
				dao.On("GetByPrefix", ctx, mock.Anything).Return([]*models.BitsCheermotePayoutConfig{
					{Ratio: 0.01, PayoutEntity: "walrus"},
				})

				Convey("when we fail to put the file", func() {
					s3Client.On("PutFile", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("s3 ouch")).Once()
					_, err := a.Get(ctx, req)

					So(err, ShouldNotBeNil)
				})

				Convey("when we succeed to put the file", func() {
					s3Client.On("PutFile", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()

					Convey("when we fail to get the presigned URL", func() {
						s3Client.On("GetTempSignedURL", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return("", errors.New("s3 ouch")).Once()

						_, err := a.Get(ctx, req)
						So(err, ShouldNotBeNil)
					})

					Convey("when we succeed to get the presigned URL", func() {
						s3Client.On("GetTempSignedURL", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return("test-report-URL", nil).Once()

						resp, err := a.Get(ctx, req)
						So(err, ShouldBeNil)
						So(resp, ShouldNotBeNil)
						So(resp.ReportUrl, ShouldEqual, "test-report-URL")
					})
				})
			})
		})
	})
}
