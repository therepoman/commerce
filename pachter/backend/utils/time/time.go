package time

import (
	"errors"
	"time"

	pachter "code.justin.tv/commerce/pachter/rpc"
	"github.com/jinzhu/now"
)

func GetStartEndDate(month, year uint32) (time.Time, time.Time) {
	t := time.Date(int(year), time.Month(int(month)), 1, 0, 0, 1, 0, time.UTC)
	return now.New(t).BeginningOfMonth(), now.New(t).EndOfMonth()
}

func RoundDown(in time.Time, period pachter.Period) (time.Time, error) {
	switch period {
	case pachter.Period_HOUR:
		return now.New(in).BeginningOfHour(), nil
	case pachter.Period_DAY:
		return now.New(in).BeginningOfDay(), nil
	case pachter.Period_WEEK:
		return now.New(in).BeginningOfWeek(), nil
	case pachter.Period_MONTH:
		return now.New(in).BeginningOfMonth(), nil
	case pachter.Period_YEAR:
		return now.New(in).BeginningOfYear(), nil
	default:
		return in, errors.New("received invalid period")
	}
}

func RoundUp(in time.Time, period pachter.Period) (time.Time, error) {
	switch period {
	case pachter.Period_HOUR:
		return now.New(in).EndOfHour(), nil
	case pachter.Period_DAY:
		return now.New(in).EndOfDay(), nil
	case pachter.Period_WEEK:
		return now.New(in).EndOfWeek(), nil
	case pachter.Period_MONTH:
		return now.New(in).EndOfMonth(), nil
	case pachter.Period_YEAR:
		return now.New(in).EndOfYear(), nil
	default:
		return in, errors.New("received invalid period")
	}
}
