package backend

import (
	"errors"
	"io"
	"net/http"
	"time"

	"code.justin.tv/commerce/gogogadget/aws/s3"
	sqsWorker "code.justin.tv/commerce/gogogadget/aws/sqs/worker"
	log "code.justin.tv/commerce/logrus"
	pachinko "code.justin.tv/commerce/pachinko/rpc"
	"code.justin.tv/commerce/pachter/backend/api/actuals"
	"code.justin.tv/commerce/pachter/backend/api/get_bits_amounts_for_channel"
	"code.justin.tv/commerce/pachter/backend/api/get_bits_monthly_amounts_for_all_channels"
	"code.justin.tv/commerce/pachter/backend/api/get_bits_monthly_report"
	"code.justin.tv/commerce/pachter/backend/api/get_emote_usage_monthly_report"
	"code.justin.tv/commerce/pachter/backend/api/get_spend_order"
	"code.justin.tv/commerce/pachter/backend/clients/payday"
	"code.justin.tv/commerce/pachter/backend/clients/rds"
	"code.justin.tv/commerce/pachter/backend/clients/redis"
	"code.justin.tv/commerce/pachter/backend/clients/sfn"
	actuals_dao "code.justin.tv/commerce/pachter/backend/dynamo/actuals"
	"code.justin.tv/commerce/pachter/backend/dynamo/bits_cheermote_payout_config"
	worker_audit_dao "code.justin.tv/commerce/pachter/backend/dynamo/worker_audit"
	"code.justin.tv/commerce/pachter/backend/metrics"
	bits_spend_dao "code.justin.tv/commerce/pachter/backend/rds/bits_spend"
	transactions_dao "code.justin.tv/commerce/pachter/backend/rds/bits_transaction"
	cheermote_usage_dao "code.justin.tv/commerce/pachter/backend/rds/cheermote_usage"
	"code.justin.tv/commerce/pachter/backend/server"
	"code.justin.tv/commerce/pachter/backend/worker/bits_monthly_report"
	"code.justin.tv/commerce/pachter/backend/worker/bits_spend_v2"
	"code.justin.tv/commerce/pachter/backend/worker/bits_transaction_audit_v2"
	"code.justin.tv/commerce/pachter/backend/worker/bits_transaction_deferrer_v2"
	"code.justin.tv/commerce/pachter/backend/worker/bits_transaction_v2"
	"code.justin.tv/commerce/pachter/backend/worker/cheermote_usage_v2"
	"code.justin.tv/commerce/pachter/config"
	pachter "code.justin.tv/commerce/pachter/rpc"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"code.justin.tv/commerce/splatter"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/facebookgo/inject"
	"github.com/jinzhu/gorm"
)

type Backend interface {
	Ping(w http.ResponseWriter, r *http.Request)
	TwirpServer() pachter.Pachter
	Statter() statsd.Statter
}

type backend struct {
	Stats  statsd.Statter  `inject:""`
	SQS    sqsiface.SQSAPI `inject:""`
	Config *config.Config  `inject:""`
	Server pachter.Pachter `inject:""`

	BitsTransactionDeferrerWorkerV2 *bits_transaction_deferrer_v2.Worker `inject:""`
	BitsSpendWorkerV2               *bits_spend_v2.Worker                `inject:""`
	BitsTransactionWorkerV2         *bits_transaction_v2.Worker          `inject:""`
	CheermoteUsageWorkerV2          *cheermote_usage_v2.Worker           `inject:""`
	BitsTransactionAuditWorkerV2    *bits_transaction_audit_v2.Worker    `inject:""`
	BitsMonthlyReportWorker         *bits_monthly_report.Worker          `inject:""`

	RDSClient *gorm.DB `inject:""`
}

func NewBackend(cfg *config.Config) (Backend, error) {
	if cfg == nil {
		return nil, errors.New("config is nil")
	}

	be := &backend{}
	statters := make([]statsd.Statter, 0)
	cloudwatchStatter := splatter.NewBufferedTelemetryCloudWatchStatter(&splatter.BufferedTelemetryConfig{
		AWSRegion:         cfg.AWSRegion,
		ServiceName:       "pachter",
		Stage:             cfg.EnvironmentName,
		Substage:          "primary",
		BufferSize:        100000,
		AggregationPeriod: time.Minute,
		FlushPeriod:       time.Second * 30,
	}, map[string]bool{})
	statters = append(statters, cloudwatchStatter)

	stats, err := splatter.NewCompositeStatter(statters)
	if err != nil {
		return nil, err
	}

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.AWSRegion),
	})
	if err != nil {
		return nil, err
	}

	rdsClient, err := rds.NewRDSClient(cfg, sess)
	if err != nil {
		return nil, err
	}

	redisClient, err := redis.NewRedisClient(cfg)
	if err != nil {
		return nil, err
	}

	sfnClient, err := sfn.NewDefaultClient()
	if err != nil {
		return nil, err
	}

	sqsClient := sqs.New(sess, &aws.Config{
		Region: aws.String(cfg.AWSRegion),
	})

	rt, err := caller.NewRoundTripper(cfg.S2SServiceName, &caller.Config{
		// Disabling as statsd is not reachable using r53 DNS.
		// TODO: Newer versions of s2s support setting a custom stats reporter
		DisableStatsClient: true,
	}, log.New())
	if err != nil {
		return nil, err
	}

	httpClient := &http.Client{
		Transport: rt,
	}

	deps := []*inject.Object{
		{Value: be},
		{Value: stats},
		{Value: sqsClient},
		{Value: cfg},
		{Value: metrics.NewStatter()},

		// Twirp APIs, server
		{Value: get_bits_monthly_report.NewAPI()},
		{Value: get_emote_usage_monthly_report.NewAPI()},
		{Value: actuals.NewAPI()},
		{Value: get_spend_order.NewAPI()},
		{Value: get_bits_monthly_amounts_for_all_channels.NewAPI()},
		{Value: get_bits_amounts_for_channel.NewAPI()},
		{Value: server.NewServer()},

		// RDS client
		{Value: rdsClient},

		// SQS workers
		{Value: &bits_spend_v2.Worker{}},
		{Value: &bits_transaction_v2.Worker{}},
		{Value: &bits_transaction_audit_v2.Worker{}},
		{Value: &cheermote_usage_v2.Worker{}},
		{Value: &bits_transaction_deferrer_v2.Worker{}},
		{Value: &bits_monthly_report.Worker{}},

		// DAOs
		{Value: transactions_dao.NewDAO()},
		{Value: bits_spend_dao.NewDAO(), Name: "bits-spend-dao"},
		{Value: cheermote_usage_dao.NewDAO()},
		{Value: worker_audit_dao.NewDAO(sess, cfg)},
		{Value: bits_cheermote_payout_config.NewDAO(sess, cfg)},
		{Value: actuals_dao.NewDAO(sess, cfg)},

		// Payday
		{Value: paydayrpc.NewPaydayProtobufClient(cfg.PaydayEndpoint, http.DefaultClient)},
		{Value: payday.NewConverter()},
		{Value: payday.NewPaydayClient()},

		// Pachinko
		{Value: pachinko.NewPachinkoProtobufClient(cfg.PachinkoEndpoint, httpClient)},

		// Petozi
		{Value: petozi.NewPetoziProtobufClient(cfg.PetoziEndpoint, http.DefaultClient)},

		// Redis client
		{Value: redisClient},

		// S3 client
		{Value: s3.NewDefaultClient()},

		// SFN client
		{Value: sfnClient},
	}

	var graph inject.Graph
	err = graph.Provide(deps...)
	if err != nil {
		return nil, err
	}

	err = graph.Populate()
	if err != nil {
		return nil, err
	}

	if !cfg.DisableSQSWorker {
		be.setupWorkers()
	} else {
		log.Infof("Disabling SQS workers")
	}

	return be, nil
}

func (be *backend) setupWorkers() {
	be.setupSQSWorker(be.Config.BitsSpendWorkerV2Config, be.BitsSpendWorkerV2)
	be.setupSQSWorker(be.Config.BitsTransactionDeferrerWorkerV2Config, be.BitsTransactionDeferrerWorkerV2)
	be.setupSQSWorker(be.Config.BitsTransactionWorkerV2Config, be.BitsTransactionWorkerV2)
	be.setupSQSWorker(be.Config.CheermoteUsageWorkerV2Config, be.CheermoteUsageWorkerV2)
	be.setupSQSWorker(be.Config.BitsTransactionAuditWorkerV2Config, be.BitsTransactionAuditWorkerV2)
	be.setupSQSWorker(be.Config.BitsMonthlyReportWorkerConfig, be.BitsMonthlyReportWorker)
}

func (be *backend) setupSQSWorker(cfg *config.SQSConfig, worker sqsWorker.Worker) {
	sqsWorker.NewSQSWorkerManager(cfg.Name, cfg.NumWorkers, 1, be.SQS, worker, be.Stats)
}

func (b backend) Ping(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	_, err := io.WriteString(w, "pong")
	if err != nil {
		log.WithError(err).Error("Error writing http response with status 200")
	}
}

func (b backend) TwirpServer() pachter.Pachter {
	return b.Server
}

func (b backend) Statter() statsd.Statter {
	return b.Stats
}
