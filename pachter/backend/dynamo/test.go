package dynamo

import (
	"math/rand"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
)

const (
	RunDynamoTestsEnvVar = "RUN_DYNAMO_TESTS"
	TestTablesSuffix     = "staging"
	TestAWSNamedProfile  = "pachter-devo"
	TestAWSRegion        = "us-west-2"
)

func GetAWSSession(t *testing.T) *session.Session {
	rand.Seed(time.Now().UnixNano())

	runDynamoTests := os.Getenv(RunDynamoTestsEnvVar)
	if strings.ToLower(runDynamoTests) != "true" {
		t.Skip("Skipping dynamo test")
	}

	return session.Must(session.NewSessionWithOptions(session.Options{
		Config:  aws.Config{Region: aws.String(TestAWSRegion)},
		Profile: TestAWSNamedProfile,
	}))
}
