package bits_cheermote_payout_config

import (
	"context"
	"math/rand"
	"testing"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachter/backend/dynamo"
	"code.justin.tv/commerce/pachter/backend/dynamo/models"
	"code.justin.tv/commerce/pachter/config"
	. "github.com/smartystreets/goconvey/convey"
)

func TestDAO(t *testing.T) {
	session := dynamo.GetAWSSession(t)
	dao := NewDAO(session, &config.Config{
		EnvironmentName: dynamo.TestTablesSuffix,
	})
	ctx := context.Background()

	Convey("bits cheermote payout configs DAO", t, func() {
		prefix := random.String(16)
		twitchUserID := random.String(16)
		entity := random.String(8)
		payout := rand.Float64()
		config := &models.BitsCheermotePayoutConfig{
			Prefix:       prefix,
			TwitchUserID: twitchUserID,
			PayoutEntity: entity,
			Ratio:        payout,
		}

		Convey("#Put", func() {
			Convey("writes a new config", func() {
				Convey("with both prefix and twitchUserID", func() {
				})

				Convey("with empty twitchUserID", func() {
					config.TwitchUserID = ""
				})

				err := dao.Put(ctx, config)
				So(err, ShouldBeNil)
			})

			Convey("returns an error if", func() {
				Convey("config is nil", func() {
					err := dao.Put(ctx, nil)
					So(err, ShouldNotBeNil)
				})
			})
		})

		Convey("#Get", func() {
			Convey("given an existing entry", func() {
				Convey("retrieves the config", func() {
					Convey("with twitchUserID", func() {
						err := dao.Put(ctx, config)
						So(err, ShouldBeNil)
					})

					Convey("with no twitchUserID", func() {
						twitchUserID = ""
						config.TwitchUserID = ""
						err := dao.Put(ctx, config)
						So(err, ShouldBeNil)

					})

					result, err := dao.Get(ctx, prefix, twitchUserID)
					So(err, ShouldBeNil)
					So(result, ShouldResemble, config)
				})
			})

			Convey("given a non-existent prefix", func() {
				prefix = random.String(16)

				Convey("returns a nil entry", func() {
					result, err := dao.Get(ctx, prefix, twitchUserID)
					So(err, ShouldBeNil)
					So(result, ShouldBeNil)
				})
			})

			Convey("returns an error if", func() {
				Convey("prefix is blank", func() {
					_, err := dao.Get(ctx, "", twitchUserID)
					So(err, ShouldNotBeNil)
				})
			})
		})

		Convey("#GetByPrefix", func() {
			Convey("given at least two entries with the same prefix", func() {
				err := dao.Put(ctx, config)
				So(err, ShouldBeNil)

				config.TwitchUserID = random.String(16)
				err = dao.Put(ctx, config)
				So(err, ShouldBeNil)

				Convey("returns a list of configs", func() {
					configs, err := dao.GetByPrefix(ctx, config.Prefix)
					So(err, ShouldBeNil)
					So(configs, ShouldHaveLength, 2)
				})
			})

			Convey("given no entry with the prefix", func() {
				configs, err := dao.GetByPrefix(ctx, config.Prefix)
				So(err, ShouldBeNil)
				So(configs, ShouldHaveLength, 0)
			})

			Convey("returns an error if", func() {
				Convey("prefix is blank", func() {
					_, err := dao.GetByPrefix(ctx, "")
					So(err, ShouldNotBeNil)
				})
			})
		})
	})
}
