package bits_cheermote_payout_config

import (
	"context"
	"errors"
	"fmt"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/pachter/backend/dynamo/models"
	"code.justin.tv/commerce/pachter/config"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/guregu/dynamo"
)

const (
	DBFormat            = "bits-cheermote-payout-configs-%s"
	HashKey             = "prefix"
	RangeKey            = "twitchUserId"
	RangeKeyPlaceholder = "NULL"
)

type DAO interface {
	Put(ctx context.Context, snapshot *models.BitsCheermotePayoutConfig) error
	Get(ctx context.Context, prefix, twitchUserID string) (*models.BitsCheermotePayoutConfig, error)
	GetByPrefix(ctx context.Context, prefix string) ([]*models.BitsCheermotePayoutConfig, error)
}

type dao struct {
	Table dynamo.Table
}

func NewDAO(sess *session.Session, config *config.Config) DAO {
	return &dao{
		Table: dynamo.New(sess).Table(fmt.Sprintf(DBFormat, config.EnvironmentName)),
	}
}

func (d *dao) Put(ctx context.Context, config *models.BitsCheermotePayoutConfig) error {
	if config == nil {
		return errors.New("snapshot is missing")
	}

	return d.Table.Put(serializeConfig(config)).RunWithContext(ctx)
}

func (d *dao) Get(ctx context.Context, prefix string, twitchUserID string) (*models.BitsCheermotePayoutConfig, error) {
	if strings.Blank(prefix) {
		return nil, errors.New("prefix is blank")
	}

	userID := twitchUserID
	if strings.Blank(userID) {
		userID = RangeKeyPlaceholder
	}

	var config models.BitsCheermotePayoutConfig
	err := d.Table.Get(HashKey, prefix).Range(RangeKey, dynamo.Equal, userID).OneWithContext(ctx, &config)

	if err == dynamo.ErrNotFound {
		return nil, nil
	}

	return deserializeConfig(&config), err
}

func (d *dao) GetByPrefix(ctx context.Context, prefix string) ([]*models.BitsCheermotePayoutConfig, error) {
	if strings.Blank(prefix) {
		return nil, errors.New("prefix is blank")
	}

	var configs []*models.BitsCheermotePayoutConfig
	err := d.Table.Get(HashKey, prefix).AllWithContext(ctx, &configs)
	if err != nil {
		return nil, err
	}

	for i, config := range configs {
		configs[i] = deserializeConfig(config)
	}

	return configs, err
}

func serializeConfig(config *models.BitsCheermotePayoutConfig) *models.BitsCheermotePayoutConfig {
	userID := config.TwitchUserID
	if strings.Blank(userID) {
		userID = RangeKeyPlaceholder
	}

	return &models.BitsCheermotePayoutConfig{
		Prefix:       config.Prefix,
		TwitchUserID: userID,
		Ratio:        config.Ratio,
		PayoutEntity: config.PayoutEntity,
	}
}

func deserializeConfig(config *models.BitsCheermotePayoutConfig) *models.BitsCheermotePayoutConfig {
	userID := config.TwitchUserID
	if userID == RangeKeyPlaceholder {
		userID = ""
	}

	return &models.BitsCheermotePayoutConfig{
		Prefix:       config.Prefix,
		TwitchUserID: userID,
		Ratio:        config.Ratio,
		PayoutEntity: config.PayoutEntity,
	}
}
