package actuals

import (
	"context"
	"math/rand"
	"testing"

	"code.justin.tv/commerce/pachter/backend/dynamo"
	"code.justin.tv/commerce/pachter/config"
	pachter "code.justin.tv/commerce/pachter/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestDAO(t *testing.T) {
	session := dynamo.GetAWSSession(t)
	dao := NewDAO(session, &config.Config{
		EnvironmentName: dynamo.TestTablesSuffix,
	})
	ctx := context.Background()

	updatedValues := make(map[string]uint32, uint32(0))
	updatedValues["SKU-00001"] = 1000

	month := uint32(9)

	Convey("working DAO", t, func() {
		Convey("#Put", func() {
			Convey("a brand new entry", func() {
				year := uint32(rand.Int() + 2015)
				err := dao.Put(ctx, pachter.Platform_AMAZON, year, month, updatedValues)
				So(err, ShouldBeNil)

				resp, err := dao.Get(ctx, pachter.Platform_AMAZON, year, month)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp, ShouldContainKey, "SKU-00001")
				So(resp["SKU-00001"], ShouldEqual, uint32(1000))

				Convey("overwrittening an existing entry", func() {
					updatedValues := make(map[string]uint32, uint32(0))
					updatedValues["SKU-0007"] = 700

					err := dao.Put(ctx, pachter.Platform_AMAZON, year, month, updatedValues)
					So(err, ShouldBeNil)

					resp, err := dao.Get(ctx, pachter.Platform_AMAZON, year, month)
					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(resp, ShouldNotContainKey, "SKU-00001")
					So(resp, ShouldContainKey, "SKU-0007")
					So(resp["SKU-0007"], ShouldEqual, uint32(700))
				})
			})
		})

		Convey("#Get", func() {
			year := uint32(rand.Int() + 2015)

			Convey("when it's not in the DB", func() {
				resp, err := dao.Get(ctx, pachter.Platform_PAYPAL, year, month)
				So(err, ShouldBeNil)
				So(resp, ShouldBeNil)
			})
		})
	})
}
