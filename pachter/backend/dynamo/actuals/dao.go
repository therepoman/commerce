package actuals

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/pachter/config"
	pachter "code.justin.tv/commerce/pachter/rpc"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/guregu/dynamo"
)

const (
	DBFormat = "bits-accounting-actuals-%s"
	RKFormat = "%d-%d"

	HashKey  = "platform"
	RangeKey = "yearMonth"
)

type ActualEntry struct {
	Platform  string            `dynamo:"platform"`
	YearMonth string            `dynamo:"yearMonth"`
	Year      uint32            `dynamo:"year"`
	Month     uint32            `dynamo:"month"`
	Values    map[string]uint32 `dynamo:"actuals"`
}

type DAO interface {
	Put(ctx context.Context, platform pachter.Platform, year, month uint32, actualValues map[string]uint32) error
	Get(ctx context.Context, platform pachter.Platform, year, month uint32) (map[string]uint32, error)
}

type dao struct {
	Table dynamo.Table
}

func NewDAO(sess *session.Session, config *config.Config) DAO {
	return &dao{
		Table: dynamo.New(sess).Table(fmt.Sprintf(DBFormat, config.EnvironmentName)),
	}
}

func (d *dao) Put(ctx context.Context, platform pachter.Platform, year, month uint32, actualValues map[string]uint32) error {
	return d.Table.Put(&ActualEntry{
		Platform:  platform.String(),
		YearMonth: formatRangeKey(year, month),
		Year:      year,
		Month:     month,
		Values:    actualValues,
	}).RunWithContext(ctx)
}

func (d *dao) Get(ctx context.Context, platform pachter.Platform, year, month uint32) (map[string]uint32, error) {
	var entry ActualEntry
	err := d.Table.Get(HashKey, platform.String()).
		Range(RangeKey, dynamo.Equal, formatRangeKey(year, month)).
		OneWithContext(ctx, &entry)
	if err == dynamo.ErrNotFound {
		return nil, nil
	}

	return entry.Values, err
}

// Range(r) Key(obi)
func formatRangeKey(year, month uint32) string {
	return fmt.Sprintf(RKFormat, year, month)
}
