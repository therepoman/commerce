package models

import "time"

const (
	EmptyTwitchUserID = "NULL"
)

type BitsCheermotePayoutConfig struct {
	Prefix       string  `dynamo:"prefix"`
	TwitchUserID string  `dynamo:"twitchUserId"`
	PayoutEntity string  `dynamo:"payoutEntity"`
	Ratio        float64 `dynamo:"ratio"`
}

type WorkerAuditEntry struct {
	TransactionID string     `dynamo:"transactionId"`
	Gringotts     *time.Time `dynamo:"gringotts"`
	Transactions  *time.Time `dynamo:"transactions"`
	Cheermote     *time.Time `dynamo:"cheermote"`
	Spend         *time.Time `dynamo:"spend"`
	Raincatcher   *time.Time `dynamo:"raincatcher"`
}
