package worker_audit

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/pachter/backend/dynamo"
	"code.justin.tv/commerce/pachter/backend/dynamo/models"
	"code.justin.tv/commerce/pachter/config"
	"github.com/gofrs/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func TestDAO(t *testing.T) {
	session := dynamo.GetAWSSession(t)
	dao := NewDAO(session, &config.Config{
		EnvironmentName: dynamo.TestTablesSuffix,
	})
	ctx := context.Background()

	Convey("worker audit DAO", t, func() {
		Convey("#Put", func() {
			entry := &models.WorkerAuditEntry{
				TransactionID: uuid.Must(uuid.NewV4()).String(),
			}
			now := time.Now().UTC()

			Convey("given no existing entry for this transactionID", func() {
				Convey("writes a new entry with worker timestamps", func() {

					Convey("for gringotts", func() {
						entry.Gringotts = &now
					})

					Convey("for transactions", func() {
						entry.Transactions = &now
					})

					Convey("for cheermote", func() {
						entry.Cheermote = &now
					})

					Convey("for spend", func() {
						entry.Spend = &now
					})

					Convey("for raincatcher", func() {
						entry.Raincatcher = &now
					})

					Convey("for multiple worker", func() {
						entry.Cheermote = &now
						entry.Raincatcher = &now
					})

					Convey("for all worker timestamps", func() {
						entry.Gringotts = &now
						entry.Transactions = &now
						entry.Cheermote = &now
						entry.Spend = &now
						entry.Raincatcher = &now
					})

					updated, err := dao.Put(ctx, entry)
					So(err, ShouldBeNil)
					So(updated, ShouldResemble, entry)
				})
			})

			Convey("given an existing audit entry", func() {
				entry.Cheermote = &now
				updated, err := dao.Put(ctx, entry)
				So(err, ShouldBeNil)
				So(updated, ShouldResemble, entry)

				Convey("add new timestamps to existing audit entry", func() {
					entry.Raincatcher = &now
					updated, err = dao.Put(ctx, entry)
					So(updated, ShouldResemble, entry)
				})
			})

			Convey("returns an error if", func() {
				Convey("audit entry is nil", func() {
					entry = nil
				})

				_, err := dao.Put(ctx, entry)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("#BatchPut", func() {
			now := time.Now()
			entries := []*models.WorkerAuditEntry{
				{
					TransactionID: uuid.Must(uuid.NewV4()).String(),
					Transactions:  &now,
				},
				{
					TransactionID: uuid.Must(uuid.NewV4()).String(),
					Transactions:  &now,
				},
				{
					TransactionID: uuid.Must(uuid.NewV4()).String(),
					Transactions:  &now,
				},
			}

			Convey("saves all entries", func() {
				err := dao.BatchPut(ctx, entries)
				So(err, ShouldBeNil)

				for _, entry := range entries {
					tx, err := dao.Get(ctx, entry.TransactionID)
					So(err, ShouldBeNil)
					So(tx.Transactions.Equal(*entry.Transactions), ShouldBeTrue)
				}
			})
		})

		Convey("#Get", func() {
			now := time.Now().UTC()
			transactionID := uuid.Must(uuid.NewV4()).String()
			entry := &models.WorkerAuditEntry{
				TransactionID: transactionID,
				Spend:         &now,
				Raincatcher:   &now,
			}

			Convey("given an exsting entry", func() {
				_, err := dao.Put(ctx, entry)
				So(err, ShouldBeNil)

				Convey("retrieves the entry", func() {
					result, err := dao.Get(ctx, transactionID)
					So(err, ShouldBeNil)
					So(result, ShouldResemble, entry)
				})
			})

			Convey("returns a nil entry given a non-existing transactionID", func() {
				result, err := dao.Get(ctx, transactionID)
				So(err, ShouldBeNil)
				So(result, ShouldBeNil)
			})

			Convey("returns an error if", func() {
				Convey("transacitonID is blank", func() {
					_, err := dao.Get(ctx, "")
					So(err, ShouldNotBeNil)
				})
			})
		})
	})
}
