package worker_audit

import (
	"context"
	"errors"
	"fmt"
	"sync"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/pachter/backend/dynamo/models"
	"code.justin.tv/commerce/pachter/config"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/guregu/dynamo"
	multierror "github.com/hashicorp/go-multierror"
)

const (
	DBFormat = "worker-audit-%s"
	HashKey  = "transactionId"

	maxConcurrentWorkers = 4
)

type DAO interface {
	Put(ctx context.Context, entry *models.WorkerAuditEntry) (*models.WorkerAuditEntry, error)
	BatchPut(ctx context.Context, entries []*models.WorkerAuditEntry) error
	Get(ctx context.Context, transactionID string) (*models.WorkerAuditEntry, error)
	Remove(ctx context.Context, transactionID string, fields []string) error
}

type dao struct {
	Table dynamo.Table
}

func NewDAO(sess *session.Session, config *config.Config) DAO {
	return &dao{
		Table: dynamo.New(sess).Table(fmt.Sprintf(DBFormat, config.EnvironmentName)),
	}
}

func (d *dao) Put(ctx context.Context, entry *models.WorkerAuditEntry) (*models.WorkerAuditEntry, error) {
	if entry == nil {
		return nil, errors.New("entry is missing")
	}

	update := d.Table.Update(HashKey, entry.TransactionID)
	if entry.Gringotts != nil {
		update = update.Set("gringotts", entry.Gringotts)
	}
	if entry.Transactions != nil {
		update = update.Set("transactions", entry.Transactions)
	}
	if entry.Cheermote != nil {
		update = update.Set("cheermote", entry.Cheermote)
	}
	if entry.Spend != nil {
		update = update.Set("spend", entry.Spend)
	}
	if entry.Raincatcher != nil {
		update = update.Set("raincatcher", entry.Raincatcher)
	}

	var updatedEntry models.WorkerAuditEntry
	err := update.ValueWithContext(ctx, &updatedEntry)
	if err != nil {
		return nil, err
	}

	return &updatedEntry, nil
}

func (d *dao) BatchPut(ctx context.Context, entries []*models.WorkerAuditEntry) error {
	entriesChan := make(chan *models.WorkerAuditEntry, len(entries))
	errsChan := make(chan error, len(entries))

	wg := &sync.WaitGroup{}
	wg.Add(maxConcurrentWorkers)

	for _, entry := range entries {
		entriesChan <- entry
	}
	close(entriesChan)

	// spawn workers to process all audit entries
	for i := 0; i < maxConcurrentWorkers; i++ {
		go func() {
			for {
				entry, ok := <-entriesChan
				if !ok {
					wg.Done()
					return
				}

				_, err := d.Put(ctx, entry)
				if err != nil {
					errsChan <- err
				}
			}
		}()
	}
	wg.Wait()

	// collect all errors
	var multierr *multierror.Error
	for len(errsChan) > 0 {
		err := <-errsChan
		multierr = multierror.Append(multierr, err)
	}

	return multierr.ErrorOrNil()
}

func (d *dao) Get(ctx context.Context, transactionID string) (*models.WorkerAuditEntry, error) {
	if strings.Blank(transactionID) {
		return nil, errors.New("transactionID is blank")
	}

	var entry models.WorkerAuditEntry
	err := d.Table.Get(HashKey, transactionID).OneWithContext(ctx, &entry)

	if err == dynamo.ErrNotFound {
		return nil, nil
	}

	return &entry, err
}

func (d *dao) Remove(ctx context.Context, transactionID string, fields []string) error {
	entry, err := d.Get(ctx, transactionID)
	if err != nil || entry == nil {
		return err
	}

	err = d.Table.Delete(HashKey, transactionID).RunWithContext(ctx)
	if err != nil {
		return err
	}

	if entry.Transactions != nil && contains(fields, "transactions") {
		entry.Transactions = nil
	}
	if entry.Spend != nil && contains(fields, "spend") {
		entry.Spend = nil
	}
	if entry.Cheermote != nil && contains(fields, "cheermote") {
		entry.Cheermote = nil
	}
	if entry.Raincatcher != nil && contains(fields, "raincatcher") {
		entry.Raincatcher = nil
	}
	if entry.Gringotts != nil && contains(fields, "gringotts") {
		entry.Gringotts = nil
	}

	if entry.Transactions == nil && entry.Spend == nil && entry.Cheermote == nil &&
		entry.Raincatcher == nil && entry.Gringotts == nil {
		return nil
	}

	_, err = d.Put(ctx, entry)

	return err
}

func contains(slice []string, target string) bool {
	for _, value := range slice {
		if value == target {
			return true
		}
	}
	return false
}
