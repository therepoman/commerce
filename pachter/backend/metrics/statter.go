package metrics

import (
	"time"

	"code.justin.tv/commerce/logrus"
	"github.com/cactus/go-statsd-client/statsd"
)

type Statter interface {
	Duration(name string, dur time.Duration)
	Inc(name string, value int64)
}

func NewStatter() Statter {
	return &statter{}
}

type statter struct {
	Statsd statsd.Statter `inject:""`
}

func (s *statter) Duration(name string, dur time.Duration) {
	err := s.Statsd.TimingDuration(name, dur, 1.0)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"name": name,
			"dur":  dur,
		}).WithError(err).Error("failed to record duration stat")
	}
}

func (s *statter) Inc(name string, value int64) {
	err := s.Statsd.Inc(name, value, 1.0)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"name":  name,
			"value": value,
		}).WithError(err).Error("failed to inc stat")
	}
}
