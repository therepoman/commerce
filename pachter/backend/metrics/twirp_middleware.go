package metrics

import (
	"fmt"
	"os"
	"time"

	identifier "code.justin.tv/amzn/TwitchProcessIdentifier"
	telemetry "code.justin.tv/amzn/TwitchTelemetry"
	cw "code.justin.tv/amzn/TwitchTelemetryCloudWatchMetricsSender"
	metricsmiddleware "code.justin.tv/amzn/TwitchTelemetryMetricsMiddleware"
	"code.justin.tv/video/metrics-middleware/v2/operation"
	"code.justin.tv/video/metrics-middleware/v2/twirpmetric"
	"github.com/twitchtv/twirp"
)

const (
	// Default settings
	metricFlushInterval     = 30 * time.Second
	metricBufferSize        = 100000
	metricAggregationPeriod = time.Minute
)

var metricHostname = os.Hostname

// NewTwirpTelemetryHook creates a TwitchTelemetry hook for Twirp.
// It uses default setting.
func NewTwirpTelemetryHook(awsRegion string, env string) (*twirp.ServerHooks, telemetry.SampleObserver, error) {
	if awsRegion == "" {
		return nil, nil, fmt.Errorf("missing argument: awsRegion=%s", awsRegion)
	}

	// Set up telemetry
	hostname, err := metricHostname()
	if err != nil {
		hostname = "unknown"
	}

	pid := &identifier.ProcessIdentifier{
		Service: "pachter",
		Stage:   env,
		Region:  awsRegion,
		Machine: hostname,
	}

	sender := cw.NewUnbuffered(pid, nil)
	sampleObserver := telemetry.NewBufferedAggregator(metricFlushInterval, metricBufferSize, metricAggregationPeriod, sender, nil)
	sampleReporter := telemetry.SampleReporter{
		SampleBuilder:  telemetry.SampleBuilder{ProcessIdentifier: *pid},
		SampleObserver: sampleObserver,
	}

	// Telemetry middleware
	metricsOpMonitor := &metricsmiddleware.OperationMonitor{
		SampleReporter: sampleReporter,
		AutoFlush:      false,
	}

	opStarter := &operation.Starter{OpMonitors: []operation.OpMonitor{metricsOpMonitor}}
	telemetryMiddleware := &twirpmetric.Server{Starter: opStarter}
	return telemetryMiddleware.ServerHooks(), sampleObserver, nil
}
