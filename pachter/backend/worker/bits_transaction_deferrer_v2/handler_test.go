package bits_transaction_deferrer_v2

import (
	"encoding/json"
	"errors"
	"testing"

	"code.justin.tv/commerce/pachter/backend/worker"
	redis_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/clients/redis"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/golang/protobuf/ptypes"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func Test_Handle(t *testing.T) {
	Convey("Given deferrer worker v2", t, func() {
		mockRedis := new(redis_mock.RedisClient)
		w := Worker{
			RedisClient: mockRedis,
		}

		Convey("when redis fails", func() {
			mockRedis.On("ZAdd", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
				Return(0, errors.New("lesad redis")).Once()
			err := w.Handle(SampleSQSMsg())

			So(err, ShouldNotBeNil)
		})

		Convey("when redis succeeds", func() {
			mockRedis.On("ZAdd", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
				Return(1, nil).Once()
			err := w.Handle(SampleSQSMsg())

			So(err, ShouldBeNil)
		})
	})
}

func SampleSQSMsg() *sqs.Message {
	txJson, _ := json.Marshal(&paydayrpc.Transaction{
		TransactionId:   "test-tran-id",
		TransactionType: "AddBits",
		TimeOfEvent:     ptypes.TimestampNow(),
		LastUpdated:     ptypes.TimestampNow(),
	})

	snsMsg := worker.SNSMessage{
		Message: string(txJson),
	}
	snsMsgJson, _ := json.Marshal(&snsMsg)

	return &sqs.Message{
		Body: aws.String(string(snsMsgJson)),
	}
}
