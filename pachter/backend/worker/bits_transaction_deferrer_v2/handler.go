package bits_transaction_deferrer_v2

import (
	"code.justin.tv/commerce/pachter/backend/clients/redis"
	"code.justin.tv/commerce/pachter/backend/worker"
	"github.com/aws/aws-sdk-go/service/sqs"
)

const (
	DeferRedisSortedKey = "deferred-transactions-v2" // exported
	workerName          = "transaction-deferrer-v2"
)

type Worker struct {
	RedisClient redis.RedisClient `inject:""`
}

func (w *Worker) Handle(msg *sqs.Message) error {
	trx, logger, err := worker.ParsePaydayBalanceUpdateMessage(msg, workerName)
	if err != nil {
		return err
	}

	numOfRecordAdded, err := w.RedisClient.ZAdd(redis.BitsDeferredTransactionZAddCmd,
		DeferRedisSortedKey, trx.Datetime.UnixNano(), trx)
	if err != nil {
		logger.WithError(err).Error("Failed to ZAdd deferred transaction")
		return err
	}
	logger.WithField("numOfRecordAdded", numOfRecordAdded).
		Info("Successfully added deferred transaction to Redis")

	return nil
}
