package worker

import (
	"encoding/json"
	"errors"
	"strings"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachter/backend/clients/payday"
	rds_models "code.justin.tv/commerce/pachter/backend/rds/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/aws/aws-sdk-go/service/sqs"
)

// These are used for spend order FIFO queue
const (
	MESSAGE_GROUP_ID_ATTRIBUTE  = "MessageGroupId"
	SPEND_ORDER_GROUP_ID_PREFIX = "bits-spend-order-"
	REPORT_PERIOD_LAST_MONTH    = "last-month"
	REPORT_PERIOD_CURRENT_MONTH = "current-month"
)

type SNSMessage struct {
	MessageID string `json:"MessageID"`
	Message   string `json:"Message"`
}

type BalanceUpdateMessage struct {
	TransactionIDs []string `json:"transaction_ids"`
}

type BitsMonthlyReportRequest struct {
	ReportPeriod *string `json:"period,omitempty"`
	TargetMonth  *uint32 `json:"month,omitempty"`
	TargetYear   *uint32 `json:"year,omitempty"`
}

func ParseBalanceUpdateMessage(msg *sqs.Message) (*BalanceUpdateMessage, error) {
	var snsMsg SNSMessage
	err := json.Unmarshal([]byte(*msg.Body), &snsMsg)
	if err != nil {
		return nil, err
	}

	var balanceUpdate BalanceUpdateMessage
	err = json.Unmarshal([]byte(snsMsg.Message), &balanceUpdate)
	if err != nil {
		return nil, err
	}

	return &balanceUpdate, nil
}

// https://git.xarth.tv/commerce/payday/blob/44416f0c9b4bcb592875e0557155f07c11253d8e/sqs/handlers/pachter/handler.go#L56-L62
func ParsePaydayBalanceUpdateMessage(msg *sqs.Message, wokerName string) (rds_models.BitsTransaction, *logrus.Entry, error) {
	var snsMsg SNSMessage
	var paydayTransaction paydayrpc.Transaction
	err := json.Unmarshal([]byte(*msg.Body), &snsMsg)
	if err != nil {
		logrus.WithError(err).Error("Unmarshaling SQS message into SNS message failed")
		return rds_models.BitsTransaction{}, nil, err
	}

	err = json.Unmarshal([]byte(snsMsg.Message), &paydayTransaction)
	if err != nil {
		logrus.WithError(err).Error("Unmarshaling SNS message into transaction failed")
		return rds_models.BitsTransaction{}, nil, err
	}

	logger := logrus.WithField("paydayTransaction", paydayTransaction)
	if paydayTransaction.TransactionId == "" {
		logger.Error("Transaction id is missing")
		return rds_models.BitsTransaction{}, nil, errors.New("transaction id is missing")
	}

	converter := payday.NewConverter()
	transactions, err := converter.ConvertTransactions([]*paydayrpc.Transaction{
		&paydayTransaction,
	})
	if err != nil {
		logger.WithError(err).Error("Transaction cannot be converted to Pachter model")
		return rds_models.BitsTransaction{}, nil, err
	}

	if len(transactions) != 1 {
		logger.WithField("transactions", transactions).
			Error("There is unexpected number of transaction(s) after Converter")
		return rds_models.BitsTransaction{}, nil, errors.New("conversion failure")
	}

	if transactions[0] == nil {
		logger.Error("Transaction somehow become nil during Converter")
		return rds_models.BitsTransaction{}, nil, errors.New("conversion failure")
	}

	workerLogger := logrus.WithFields(logrus.Fields{
		"worker":      wokerName,
		"transaction": *transactions[0],
	})
	workerLogger.Info("Received SQS message.")

	return *transactions[0], workerLogger, nil
}

func ParseDeferredBalanceUpdateMessage(msg *sqs.Message) (rds_models.BitsTransaction, error) {
	var bitsTransaction rds_models.BitsTransaction
	err := json.Unmarshal([]byte(*msg.Body), &bitsTransaction)
	if err != nil {
		return rds_models.BitsTransaction{}, err
	}

	return bitsTransaction, nil
}

func ParseReportGenerationRequest(msg *sqs.Message) (*BitsMonthlyReportRequest, error) {
	if msg.Body == nil || *msg.Body == "" {
		return nil, errors.New("empty and bad request")
	}

	var req BitsMonthlyReportRequest
	err := json.Unmarshal([]byte(*msg.Body), &req)
	if err != nil {
		return nil, err
	}

	return &req, nil
}

func GetUserIDFromSpendOrderMessage(msg *sqs.Message) string {
	groupID := msg.Attributes[MESSAGE_GROUP_ID_ATTRIBUTE]
	if groupID == nil {
		return ""
	}

	// the group id for spend order queue looks like "bits-spend-order-{userID}"
	if strings.Contains(*groupID, SPEND_ORDER_GROUP_ID_PREFIX) {
		splits := strings.Split(*groupID, "-")
		return splits[3]
	}

	return ""
}
