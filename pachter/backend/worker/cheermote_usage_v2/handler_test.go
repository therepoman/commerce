package cheermote_usage_v2

import (
	"encoding/json"
	"errors"
	"testing"
	"time"

	"github.com/golang/protobuf/ptypes"

	dynamo_models "code.justin.tv/commerce/pachter/backend/dynamo/models"
	"code.justin.tv/commerce/pachter/backend/worker"
	audit_dao_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/dynamo/worker_audit"
	metrics_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/metrics"
	cheermote_dao_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/rds/cheermote_usage"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func Test_Handle(t *testing.T) {
	Convey("Given cheermote worker v2", t, func() {
		mockCheermoteDAO := new(cheermote_dao_mock.DAO)
		mockAuditDAO := new(audit_dao_mock.DAO)
		mockStatter := new(metrics_mock.Statter)
		w := Worker{
			CheermoteUsagesDAO: mockCheermoteDAO,
			AuditDAO:           mockAuditDAO,
			Statter:            mockStatter,
		}
		now := time.Now()
		mockStatter.On("Duration", mock.Anything, mock.Anything).
			Return(nil)

		Convey("when AuditDAO.Get fails", func() {
			mockAuditDAO.On("Get", mock.Anything, mock.Anything).
				Return(nil, errors.New("lesad audit")).Once()
			err := w.Handle(SampleSQSMsg())

			So(err, ShouldNotBeNil)
			mockCheermoteDAO.AssertNumberOfCalls(t, "BatchPut", 0)
			mockAuditDAO.AssertNumberOfCalls(t, "Put", 0)
		})

		Convey("when an audit record already exists", func() {
			mockAuditDAO.On("Get", mock.Anything, mock.Anything).
				Return(&dynamo_models.WorkerAuditEntry{
					Cheermote: &now,
				}, nil).Once()
			err := w.Handle(SampleSQSMsg())

			So(err, ShouldBeNil)
			mockCheermoteDAO.AssertNumberOfCalls(t, "BatchPut", 0)
			mockAuditDAO.AssertNumberOfCalls(t, "Put", 0)
		})

		mockAuditDAO.On("Get", mock.Anything, mock.Anything).
			Return(nil, nil)
		Convey("when the transaction is not cheering", func() {
			Convey("when AuditDAO.Put fails", func() {
				mockAuditDAO.On("Put", mock.Anything, mock.Anything).
					Return(nil, errors.New("lesad audit put")).Once()
				err := w.Handle(SampleSQSMsg(paydayrpc.TransactionType_AddBits.String()))

				So(err, ShouldNotBeNil)
				mockCheermoteDAO.AssertNumberOfCalls(t, "BatchPut", 0)
				mockAuditDAO.AssertNumberOfCalls(t, "Put", 1)
			})

			Convey("when AuditDAO.Put succeeds", func() {
				mockAuditDAO.On("Put", mock.Anything, mock.Anything).
					Return(nil, nil).Once()
				err := w.Handle(SampleSQSMsg(paydayrpc.TransactionType_AdminRemoveBits.String()))

				So(err, ShouldBeNil)
				mockCheermoteDAO.AssertNumberOfCalls(t, "BatchPut", 0)
				mockAuditDAO.AssertNumberOfCalls(t, "Put", 1)
			})
		})

		Convey("when the transaction is cheering", func() {
			Convey("when RDS.BatchPut fails", func() {
				mockCheermoteDAO.On("BatchPut", mock.Anything, mock.Anything).
					Return(errors.New("lesad RDS #1")).Once()
				err := w.Handle(SampleSQSMsg(paydayrpc.TransactionType_GiveBitsToBroadcaster.String()))

				So(err, ShouldNotBeNil)
				mockCheermoteDAO.AssertNumberOfCalls(t, "BatchPut", 1)
				mockAuditDAO.AssertNumberOfCalls(t, "Put", 0)
			})

			Convey("when RDS.BatchPut succeeds", func() {
				mockCheermoteDAO.On("BatchPut", mock.Anything, mock.Anything).
					Return(nil).Once()
				mockAuditDAO.On("Put", mock.Anything, mock.Anything).
					Return(nil, nil).Once()
				err := w.Handle(SampleSQSMsg(paydayrpc.TransactionType_GiveBitsToBroadcaster.String()))

				So(err, ShouldBeNil)
				mockCheermoteDAO.AssertCalled(t, "BatchPut", mock.Anything, mock.Anything)
				mockAuditDAO.AssertCalled(t, "Put", mock.Anything, mock.Anything)
			})
		})
	})
}

func Test_validate(t *testing.T) {
	Convey("Given cheermote worker validator", t, func() {
		w := Worker{}

		Convey("when transaction is cheering with no emote totals", func() {
			_, _, err := w.validate(SampleSQSMsgWithEmoteTotals(
				map[string]int64{},
				paydayrpc.TransactionType_GiveBitsToBroadcaster.String(),
			))
			So(err, ShouldNotBeNil)
		})

		Convey("when transaction is cheering with some emote totals", func() {
			_, _, err := w.validate(SampleSQSMsgWithEmoteTotals(
				map[string]int64{"showlove": 5},
				paydayrpc.TransactionType_GiveBitsToBroadcaster.String(),
			))
			So(err, ShouldBeNil)
		})

		Convey("when transaction is not cheering", func() {
			_, _, err := w.validate(SampleSQSMsgWithEmoteTotals(
				map[string]int64{},
				paydayrpc.TransactionType_AdminRemoveBits.String(),
			))
			So(err, ShouldBeNil)
		})
	})
}

func SampleSQSMsg(transactionType ...string) *sqs.Message {
	tranType := ""
	emoteTotals := make(map[string]int64)
	if len(transactionType) > 0 {
		tranType = transactionType[0]
	}

	if tranType == paydayrpc.TransactionType_GiveBitsToBroadcaster.String() {
		emoteTotals["pogchamp"] = 10
	}

	txJson, _ := json.Marshal(&paydayrpc.Transaction{
		TransactionId:   "test-tran-id",
		TransactionType: tranType,
		EmoteTotals:     emoteTotals,
		TimeOfEvent:     ptypes.TimestampNow(),
		LastUpdated:     ptypes.TimestampNow(),
	})

	snsMsg := worker.SNSMessage{
		Message: string(txJson),
	}
	snsMsgJson, _ := json.Marshal(&snsMsg)

	return &sqs.Message{
		Body: aws.String(string(snsMsgJson)),
	}
}

func SampleSQSMsgWithEmoteTotals(emoteTotals map[string]int64, transactionType ...string) *sqs.Message {
	tranType := ""
	if len(transactionType) > 0 {
		tranType = transactionType[0]
	}

	txJson, _ := json.Marshal(&paydayrpc.Transaction{
		TransactionId:   "test-tran-id",
		TransactionType: tranType,
		EmoteTotals:     emoteTotals,
		TimeOfEvent:     ptypes.TimestampNow(),
		LastUpdated:     ptypes.TimestampNow(),
	})

	snsMsg := worker.SNSMessage{
		Message: string(txJson),
	}
	snsMsgJson, _ := json.Marshal(&snsMsg)

	return &sqs.Message{
		Body: aws.String(string(snsMsgJson)),
	}
}
