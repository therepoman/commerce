package cheermote_usage_v2

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/logrus"
	dynamo_models "code.justin.tv/commerce/pachter/backend/dynamo/models"
	"code.justin.tv/commerce/pachter/backend/dynamo/worker_audit"
	"code.justin.tv/commerce/pachter/backend/metrics"
	"code.justin.tv/commerce/pachter/backend/rds"
	"code.justin.tv/commerce/pachter/backend/rds/cheermote_usage"
	rds_models "code.justin.tv/commerce/pachter/backend/rds/models"
	"code.justin.tv/commerce/pachter/backend/worker"
	"github.com/aws/aws-sdk-go/service/sqs"
)

const (
	rdsLatency    = "rds-cheermote-batch-put-latency"
	dynamoLatency = "dynamo-audit-put-latency"
	workerName    = "bits-cheermote-v2"
)

type Worker struct {
	CheermoteUsagesDAO cheermote_usage.DAO `inject:""`
	AuditDAO           worker_audit.DAO    `inject:""`
	Statter            metrics.Statter     `inject:""`
}

func (w *Worker) Handle(msg *sqs.Message) error {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	transaction, logger, err := w.validate(msg)
	if err != nil {
		return err
	}

	// check if this transaction has already been processed
	existingTrx, err := w.AuditDAO.Get(ctx, transaction.TransactionID)
	if err != nil {
		logger.WithError(err).Error("Failed to get audit record")
		return err
	}
	if existingTrx != nil && existingTrx.Cheermote != nil {
		return nil
	}

	// save to RDS
	if isCheering(transaction) {
		rdsStartTime := time.Now()
		err = w.CheermoteUsagesDAO.BatchPut(ctx, transaction.CheermoteUsages)
		go w.Statter.Duration(rdsLatency, time.Since(rdsStartTime))
		if err != nil && !rds.IsPKViolationError(err) {
			logger.WithError(err).Errorf("Failed to batch save cheermote usages")
			return err
		}
	}

	// save to audit table
	dynamoStartTime := time.Now()
	_, err = w.AuditDAO.Put(ctx, &dynamo_models.WorkerAuditEntry{
		TransactionID: transaction.TransactionID,
		Cheermote:     &dynamoStartTime,
	})
	go w.Statter.Duration(dynamoLatency, time.Since(dynamoStartTime))
	if err != nil {
		logger.WithError(err).Error("Failed to record worker audit entry")
		return err
	}

	return nil
}

func (w *Worker) validate(msg *sqs.Message) (rds_models.BitsTransaction, *logrus.Entry, error) {
	transaction, logger, err := worker.ParsePaydayBalanceUpdateMessage(msg, workerName)
	if err != nil {
		return rds_models.BitsTransaction{}, nil, err
	}

	if isCheering(transaction) && len(transaction.CheermoteUsages) == 0 {
		logger.Error("This cheering transaction doesn't have emote totals populated")
		return rds_models.BitsTransaction{}, nil, errors.New("empty emote totals")
	}

	return transaction, logger, nil
}

func isCheering(tx rds_models.BitsTransaction) bool {
	return rds_models.TransactionType(tx.TransactionType) == rds_models.TransactionType_Cheering
}
