package bits_monthly_report

import (
	"bytes"
	"context"
	"encoding/csv"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"code.justin.tv/commerce/gogogadget/aws/s3"
	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachter/backend/dynamo/actuals"
	time_utils "code.justin.tv/commerce/pachter/backend/utils/time"
	"code.justin.tv/commerce/pachter/backend/worker"
	"code.justin.tv/commerce/pachter/config"
	pachter "code.justin.tv/commerce/pachter/rpc"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/jinzhu/gorm"
)

const (
	ReportS3KeyFormat = "bits-monthly-report-%d-%d.csv"
)

type Worker struct {
	Config     *config.Config `inject:""`
	S3Client   s3.Client      `inject:""`
	RDS        *gorm.DB       `inject:""`
	Petozi     petozi.Petozi  `inject:""`
	ActualsDAO actuals.DAO    `inject:""`
}

type ReportEntry struct {
	BitsTypeMonthYear *time.Time
	BitsType          *string
	Platform          *string
	EntitledQtty      *int64
	EntitledCost      *float64
	CheeringQtty      *int64
	CheeringCost      *float64
	DeletedQtty       *int64
	DeletedCost       *float64
}

// This queue has visibility timeout second of 7200
func (w *Worker) Handle(msg *sqs.Message) error {
	startDate, endDate, targetMonth, targetYear, err := targetDates(msg)
	if err != nil {
		logrus.WithError(err).Error("Failed to get start/end date")
		return err
	}

	logger := logrus.WithFields(logrus.Fields{
		"Report start date": startDate,
		"Report end date":   endDate,
	})
	logger.Info("Generating Bits monthly report")

	var reportEntries []ReportEntry
	err = w.RDS.Raw("SELECT * FROM get_bits_monthly_report_revolutions(?, ?)", startDate, endDate).Scan(&reportEntries).Error
	if err != nil {
		logger.WithError(err).Error("Error calling stored procedure for report generation")
		return err
	}

	byteBuffer := bytes.NewBuffer([]byte{})
	csvWriter := csv.NewWriter(byteBuffer)

	err = csvWriter.Write(getReportCSVHeader())
	if err != nil {
		logger.WithError(err).Error("Error generating Bits monthly report with header")
		return err
	}

	calculatedRecords, err := w.calculateActuals(reportEntries)
	if err != nil {
		logger.WithError(err).Error("Error performing approximation / actuals calculations")
		return err
	}

	csvRows := w.makeCSVRows(calculatedRecords)
	for _, row := range csvRows {
		err = csvWriter.Write(row)
		if err != nil {
			logger.WithError(err).Error("Error writing a record to the csv")
			return err
		}
	}
	csvWriter.Flush()

	s3Key := fmt.Sprintf(ReportS3KeyFormat, targetYear, targetMonth)
	err = w.S3Client.PutFile(context.Background(), w.Config.BitsMonthlyReportBucket, s3Key, bytes.NewReader(byteBuffer.Bytes()))
	if err != nil {
		logger.WithError(err).Error("Error putting Bits monthly report into S3 bucket")
		return err
	}
	logger.Info("Successfully generated Bits monthly report")

	return nil
}

func targetDates(msg *sqs.Message) (startDate, endDate time.Time, targetMonth, targetYear uint32, err error) {
	req, err := worker.ParseReportGenerationRequest(msg)
	if err != nil {
		return
	}
	if err = validateInput(req); err != nil {
		return
	}

	if req.ReportPeriod != nil && *req.ReportPeriod == worker.REPORT_PERIOD_CURRENT_MONTH {
		targetMonth = uint32(time.Now().Month())
		targetYear = uint32(time.Now().Year())
		startDate, endDate = time_utils.GetStartEndDate(targetMonth, targetYear)
		return
	}

	if req.ReportPeriod != nil && *req.ReportPeriod == worker.REPORT_PERIOD_LAST_MONTH {
		someTimeLastMonth := time.Now().AddDate(0, -1, 0)
		targetMonth = uint32(someTimeLastMonth.Month())
		targetYear = uint32(someTimeLastMonth.Year())
		startDate, endDate = time_utils.GetStartEndDate(targetMonth, targetYear)
		return
	}

	targetMonth = *req.TargetMonth
	targetYear = *req.TargetYear
	startDate, endDate = time_utils.GetStartEndDate(targetMonth, targetYear)
	return
}

func validateInput(req *worker.BitsMonthlyReportRequest) error {
	if req == nil {
		return errors.New("invalid empty request")
	}
	if req.ReportPeriod == nil && (req.TargetYear == nil || req.TargetMonth == nil) {
		return errors.New("must specify either report period or both year and month")
	}
	if req.TargetMonth != nil && (*req.TargetMonth == 0 || *req.TargetMonth > 12) {
		return fmt.Errorf("invalid requested month: %d", *req.TargetMonth)
	}
	if req.ReportPeriod != nil &&
		(*req.ReportPeriod != worker.REPORT_PERIOD_LAST_MONTH && *req.ReportPeriod != worker.REPORT_PERIOD_CURRENT_MONTH) {
		return fmt.Errorf("invalid report period: %s", *req.ReportPeriod)
	}

	return nil
}

func getReportCSVHeader() []string {
	return []string{
		"Bits Type (legacy string)",
		"Payment Platform",
		"Business Attribution",
		"Entitlement Method",
		"Entitlement Qtty",
		"Entitled Approx. Value USD",
		"Entitled Actual Value USD",
		"Used Qtty",
		"Used Approx. Revenue USD",
		"Used Actual Revenue USD",
		"Breakage Qtty",
		"Breakage Approx. Revenue USD",
		"Breakage Actual Revenue USD",
		"Deletion Qtty",
		"Deletion Approx. Revenue USD",
		"Deletion Actual Revenue USD",
	}
}

type CalculatedRecord struct {
	BitsType            string
	BitsTypeMonthYear   string
	Platform            string
	BusinessAttribution string
	EntitlementMethod   string
	EntitledQuantity    int64
	EntitledApprox      float64
	EntitledActual      float64
	UsedQuantity        int64
	UsedApprox          float64
	UsedActual          float64
	DeletionQuantity    int64
	DeletionApprox      float64
	DeletionActual      float64
}

// Calculates approximations and actuals for each BitsType in each MonthYear
// Returns a map[BitsType]map[MonthYear]CalculatedRecord
func (w *Worker) calculateActuals(reportEntires []ReportEntry) (map[string]map[string]CalculatedRecord, error) {
	petoziBitsTypesResp, err := w.Petozi.GetBitsTypes(context.Background(), &petozi.GetBitsTypesReq{})
	if err != nil {
		return nil, err
	}
	if petoziBitsTypesResp == nil {
		return nil, errors.New("petozi returned nil where it shouldn't")
	}
	petoziBitsTypes := petoziBitsTypesResp.BitsTypes

	petoziBitsProductsResp, err := w.Petozi.GetBitsProducts(context.Background(), &petozi.GetBitsProductsReq{})
	if err != nil {
		return nil, err
	}
	if petoziBitsProductsResp == nil {
		return nil, errors.New("petozi returned nil where it shouldn't")
	}
	petoziBitsProducts := reduceToUniqueBitsType(petoziBitsProductsResp.BitsProducts)

	allMonthYears := getAllMonthYears(reportEntires)

	actualsByPlatformByMonthYear := make(map[pachter.Platform]map[string]map[string]uint32)

	for platformInt := range pachter.Platform_name {
		platform := pachter.Platform(platformInt)
		if platform != pachter.Platform_NONE {
			actualsByPlatformByMonthYear[platform] = make(map[string]map[string]uint32)

			for monthYearStr := range allMonthYears {
				month, year, err := getMonthYearFromString(monthYearStr)
				if err != nil {
					return nil, err
				}

				actualsForPlatform, err := w.ActualsDAO.Get(context.Background(), pachter.Platform(platform), uint32(year), uint32(month))
				if err != nil {
					logrus.WithError(err).WithFields(logrus.Fields{
						"year":     year,
						"month":    month,
						"platform": platform,
					}).Error("Error getting actuals from dao")
					return nil, err
				}

				actualsByPlatformByMonthYear[platform][monthYearStr] = actualsForPlatform
			}
		}
	}

	results := make(map[string]map[string]CalculatedRecord)
	for _, reportEntry := range reportEntires {
		if reportEntry.BitsType == nil || *reportEntry.BitsType == "" {
			continue
		}

		monthYearString := toMonthYearString(reportEntry)

		var petoziBitsType *petozi.BitsType
		for _, pBitsType := range petoziBitsTypes {
			if pBitsType.Id == *reportEntry.BitsType {
				petoziBitsType = pBitsType
			}
		}

		if petoziBitsType == nil {
			return nil, errors.New("bitsType not found in petozi catalog: " + *reportEntry.BitsType)
		}

		platformStr := ""
		if reportEntry.Platform != nil {
			platformStr = *reportEntry.Platform
		}

		platform := pachter.Platform_NONE
		platformInt, ok := pachter.Platform_value[strings.ToUpper(platformStr)]
		if ok {
			platform = pachter.Platform(platformInt)
		}

		var actualCostPerBitUsc float64

		actualsByMonthYearForPlatform, ok := actualsByPlatformByMonthYear[platform]
		if ok {
			actualValue, ok := actualsByMonthYearForPlatform[monthYearString][*reportEntry.BitsType]
			if ok && actualValue > 0 {
				var petoziBitsProduct *petozi.BitsProduct
				for _, pBitsProduct := range petoziBitsProducts {
					if pBitsProduct.BitsTypeId == *reportEntry.BitsType {
						petoziBitsProduct = pBitsProduct
					}
				}

				if petoziBitsProduct == nil {
					return nil, errors.New("bitsProduct not found in petozi catalog with corresponding bitsType: " + *reportEntry.BitsType)
				}

				actualCostPerBitUsc = float64(actualValue) / float64(petoziBitsProduct.Quantity)
			}
		}

		entitledQtty := pointers.Int64OrDefault(reportEntry.EntitledQtty, 0)
		cheeringQtty := pointers.Int64OrDefault(reportEntry.CheeringQtty, 0)
		deletedQtty := pointers.Int64OrDefault(reportEntry.DeletedQtty, 0)
		entitledCost := pointers.Float64OrDefault(reportEntry.EntitledCost, 0)
		cheeringCost := pointers.Float64OrDefault(reportEntry.CheeringCost, 0)
		deletedCost := pointers.Float64OrDefault(reportEntry.DeletedCost, 0)

		approxEntitledValue := entitledCost / 100
		approxUsedValue := cheeringCost / 100
		approxDeleteValue := deletedCost / 100

		actualEntitledValue := actualCostPerBitUsc * float64(entitledQtty) / 100
		actualUsedValue := actualCostPerBitUsc * float64(cheeringQtty) / 100
		actualDeleteValue := actualCostPerBitUsc * float64(deletedQtty) / 100

		if _, ok := results[*reportEntry.BitsType]; !ok {
			results[*reportEntry.BitsType] = make(map[string]CalculatedRecord)
		}

		results[*reportEntry.BitsType][monthYearString] = CalculatedRecord{
			BitsType:            *reportEntry.BitsType,
			BitsTypeMonthYear:   monthYearString,
			Platform:            platformStr,
			BusinessAttribution: petoziBitsType.BusinessAttribution.String(),
			EntitlementMethod:   petoziBitsType.EntitlementSourceType.String(),
			EntitledQuantity:    entitledQtty,
			EntitledApprox:      approxEntitledValue,
			EntitledActual:      actualEntitledValue,
			UsedQuantity:        cheeringQtty,
			UsedApprox:          approxUsedValue,
			UsedActual:          actualUsedValue,
			DeletionQuantity:    deletedQtty,
			DeletionApprox:      approxDeleteValue,
			DeletionActual:      actualDeleteValue,
		}
	}

	return results, nil
}

func reduceToUniqueBitsType(bitsProducts []*petozi.BitsProduct) []*petozi.BitsProduct {
	bitsTypeCounts := make(map[string]int)
	for _, bitsProduct := range bitsProducts {
		bitsTypeCounts[bitsProduct.BitsTypeId]++
	}

	bitsProductsWithUniqueBitsType := make([]*petozi.BitsProduct, 0)
	for _, bitsProduct := range bitsProducts {
		count := bitsTypeCounts[bitsProduct.BitsTypeId]
		if count == 1 {
			bitsProductsWithUniqueBitsType = append(bitsProductsWithUniqueBitsType, bitsProduct)
		}
	}

	return bitsProductsWithUniqueBitsType
}

func getAllMonthYears(reportEntries []ReportEntry) map[string]interface{} {
	monthYears := make(map[string]interface{})
	for _, entry := range reportEntries {
		monthYears[toMonthYearString(entry)] = nil
	}
	return monthYears
}

func toMonthYearString(reportEntry ReportEntry) string {
	return fmt.Sprintf("%02d-%d", reportEntry.BitsTypeMonthYear.Month(), reportEntry.BitsTypeMonthYear.Year())
}

func getMonthYearFromString(str string) (int, int, error) {
	parts := strings.Split(str, "-")
	if len(parts) != 2 {
		return 0, 0, errors.New("invalid string format")
	}

	month, err := strconv.Atoi(parts[0])
	if err != nil {
		return 0, 0, err
	}

	year, err := strconv.Atoi(parts[1])
	if err != nil {
		return 0, 0, err
	}

	return month, year, err
}

// Sums up quantity/approximation/actuals calculations for each bits type
// Returns the csv report where each line represents a bits type
func (w Worker) makeCSVRows(calculatedRecords map[string]map[string]CalculatedRecord) [][]string {
	rows := make([][]string, 0)

	for bitsType, byMonthYear := range calculatedRecords {
		totalEntitledQtty := int64(0)
		totalUsedQtty := int64(0)
		totalDeletedQtty := int64(0)

		totalEntitledApprox := float64(0)
		totalUsedApprox := float64(0)
		totalDeletedApprox := float64(0)

		totalEntitledActual := float64(0)
		totalUsedActual := float64(0)
		totalDeletedActual := float64(0)

		var calculatedRecordForBitsType CalculatedRecord
		for _, calculatedRecord := range byMonthYear {
			totalEntitledQtty += calculatedRecord.EntitledQuantity
			totalUsedQtty += calculatedRecord.UsedQuantity
			totalDeletedQtty += calculatedRecord.DeletionQuantity

			totalEntitledApprox += calculatedRecord.EntitledApprox
			totalUsedApprox += calculatedRecord.UsedApprox
			totalDeletedApprox += calculatedRecord.DeletionApprox

			totalEntitledActual += calculatedRecord.EntitledActual
			totalUsedActual += calculatedRecord.UsedActual
			totalDeletedActual += calculatedRecord.DeletionActual

			calculatedRecordForBitsType = calculatedRecord
		}

		rows = append(rows, []string{
			bitsType,                             // Bits Type (legacy string)
			calculatedRecordForBitsType.Platform, // Payment Platform
			calculatedRecordForBitsType.BusinessAttribution, // Business Attribution
			calculatedRecordForBitsType.EntitlementMethod,   // Entitlement Method
			fmt.Sprintf("%d", totalEntitledQtty),            // Entitlement Qtty
			fmt.Sprintf("%.2f", totalDeletedApprox),         // Entitled Approx. Value USD
			fmt.Sprintf("%.2f", totalEntitledActual),        // Entitled Actual Value USD
			fmt.Sprintf("%d", totalUsedQtty),                // Used Qtty
			fmt.Sprintf("%.2f", totalUsedApprox),            // Used Approx. Revenue USD
			fmt.Sprintf("%.2f", totalUsedActual),            // Used Actual Revenue USD
			"",                                              // Breakage Qtty
			"",                                              // Breakage Approx. Revenue USD
			"",                                              // Breakage Actual Revenue USD
			fmt.Sprintf("%d", totalDeletedQtty),             // Deletion Qtty
			fmt.Sprintf("%.2f", totalDeletedApprox),         // Deletion Approx. Revenue USD
			fmt.Sprintf("%.2f", totalDeletedActual),         // Deletion Actual Revenue USD
		})
	}

	return rows
}
