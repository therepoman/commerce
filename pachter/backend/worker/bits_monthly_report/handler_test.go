package bits_monthly_report

import (
	"encoding/json"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/pachter/backend/rds"
	"code.justin.tv/commerce/pachter/backend/utils/time"
	workermodel "code.justin.tv/commerce/pachter/backend/worker"
	"code.justin.tv/commerce/pachter/config"
	s3_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/gogogadget/aws/s3"
	dao_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/dynamo/actuals"
	petozi_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/petozi/rpc"
	pachter "code.justin.tv/commerce/pachter/rpc"
	petozi "code.justin.tv/commerce/petozi/rpc"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestWorker(t *testing.T) {
	rdsClient, err := rds.GetRDSClient(t)
	if err != nil {
		t.Errorf("Failed to initialize RDS client for tests %+v", err)
		return
	}

	Convey("Given report worker", t, func() {
		Convey("With month/year specified in the request", func() {
			cfg := &config.Config{
				BitsMonthlyReportBucket: "test-non-existent-bucket",
			}
			s3Client := new(s3_mock.Client)
			petoziClient := new(petozi_mock.Petozi)
			actualsDAO := new(dao_mock.DAO)

			worker := Worker{
				Config:     cfg,
				S3Client:   s3Client,
				RDS:        rdsClient,
				Petozi:     petoziClient,
				ActualsDAO: actualsDAO,
			}
			request := workermodel.BitsMonthlyReportRequest{
				TargetMonth: uint32P(6),
				TargetYear:  uint32P(2019),
			}
			requestJson, err := json.Marshal(request)
			So(err, ShouldNotBeNil)

			req := &sqs.Message{
				Body: aws.String(string(requestJson)),
			}
			petoziBitsTypes, monthYears := getBitsTypes(worker, *request.TargetMonth, *request.TargetYear)

			Convey("when petozi GetBitsTypes errors", func() {
				petoziClient.On("GetBitsTypes", mock.Anything, mock.Anything).Return(nil, errors.New("petozi do-zi")).Once()

				err := worker.Handle(req)
				So(err, ShouldNotBeNil)
				petoziClient.AssertNumberOfCalls(t, "GetBitsTypes", 1)
				s3Client.AssertNumberOfCalls(t, "PutFile", 0)
			})

			Convey("when petozi doesn't have the bitsType found in RDS", func() {
				actualsDAO.On("Get", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(map[string]uint32{}, nil)
				petoziClient.On("GetBitsProducts", mock.Anything, mock.Anything).Return(&petozi.GetBitsProductsResp{
					BitsProducts: []*petozi.BitsProduct{},
				}, nil).Once()
				petoziClient.On("GetBitsTypes", mock.Anything, mock.Anything).Return(&petozi.GetBitsTypesResp{
					BitsTypes: nil,
				}, nil).Once()
				err := worker.Handle(req)

				So(err, ShouldNotBeNil)
				petoziClient.AssertNumberOfCalls(t, "GetBitsTypes", 1)
				s3Client.AssertNumberOfCalls(t, "PutFile", 0)
			})

			Convey("when petozi GetBitsTypes returns expected values", func() {
				petoziClient.On("GetBitsTypes", mock.Anything, mock.Anything).Return(&petozi.GetBitsTypesResp{
					BitsTypes: petoziBitsTypes,
				}, nil).Once()

				Convey("when petozi GetBitsProducts returns an error", func() {
					petoziClient.On("GetBitsProducts", mock.Anything, mock.Anything).Return(nil, errors.New("petozi test error")).Once()

					err := worker.Handle(req)
					So(err, ShouldNotBeNil)
					petoziClient.AssertNumberOfCalls(t, "GetBitsTypes", 1)
					petoziClient.AssertNumberOfCalls(t, "GetBitsProducts", 1)
					s3Client.AssertNumberOfCalls(t, "PutFile", 0)
				})

				Convey("when petozi GetBitsProducts returns successfully", func() {
					petoziClient.On("GetBitsProducts", mock.Anything, mock.Anything).Return(&petozi.GetBitsProductsResp{
						BitsProducts: []*petozi.BitsProduct{},
					}, nil).Once()

					Convey("when actuals dao errors on first call", func() {
						actualsDAO.On("Get", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("actuals dao err")).Once()

						err := worker.Handle(req)
						So(err, ShouldNotBeNil)
						actualsDAO.AssertNumberOfCalls(t, "Get", 1)
					})

					Convey("when actuals dao errors on second call", func() {
						actualsDAO.On("Get", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(map[string]uint32{}, nil).Once()
						actualsDAO.On("Get", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("actuals dao err")).Once()

						err := worker.Handle(req)
						So(err, ShouldNotBeNil)
						actualsDAO.AssertNumberOfCalls(t, "Get", 2)
					})

					Convey("whan all actual dao calls succeed", func() {
						for _, monthYear := range monthYears {
							for platformInt := range pachter.Platform_name {
								platform := pachter.Platform(platformInt)
								actualsDAO.On("Get", mock.Anything, platform, monthYear.Year, monthYear.Month).Return(map[string]uint32{}, nil).Once()
							}
						}

						Convey("when S3 client fails to PutFile", func() {
							s3Client.On("PutFile", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("s3 ouch")).Once()

							err := worker.Handle(req)
							So(err, ShouldNotBeNil)
							s3Client.AssertNumberOfCalls(t, "PutFile", 1)
							s3Client.AssertNumberOfCalls(t, "GetTempSignedURL", 0)
						})

						Convey("when S3 client successfully PutFile", func() {
							s3Client.On("PutFile", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()

							err := worker.Handle(req)
							So(err, ShouldBeNil)
						})
					})
				})

				Convey("when there is no corresponding bits product for a configured actual", func() {
					petoziClient.On("GetBitsProducts", mock.Anything, mock.Anything).Return(&petozi.GetBitsProductsResp{
						BitsProducts: []*petozi.BitsProduct{},
					}, nil).Once()

					for _, monthYear := range monthYears {
						for platformInt := range pachter.Platform_name {
							platform := pachter.Platform(platformInt)
							actualsDAO.On("Get", mock.Anything, platform, monthYear.Year, monthYear.Month).Return(map[string]uint32{
								"tv.twitch.ios.iap.bits.everyday.t2": 2,
							}, nil).Once()
						}
					}

					err := worker.Handle(req)
					So(err, ShouldNotBeNil)
				})

				Convey("when there is a corresponding bits product for a configured actual, but it has a duplicate bit type id", func() {
					petoziClient.On("GetBitsProducts", mock.Anything, mock.Anything).Return(&petozi.GetBitsProductsResp{
						BitsProducts: []*petozi.BitsProduct{
							{
								Id:         "id-1",
								BitsTypeId: "tv.twitch.ios.iap.bits.everyday.t2",
							},
							{
								Id:         "id-2",
								BitsTypeId: "tv.twitch.ios.iap.bits.everyday.t2",
							},
						},
					}, nil).Once()

					for _, monthYear := range monthYears {
						for platformInt := range pachter.Platform_name {
							platform := pachter.Platform(platformInt)
							actualsDAO.On("Get", mock.Anything, platform, monthYear.Year, monthYear.Month).Return(map[string]uint32{
								"tv.twitch.ios.iap.bits.everyday.t2": 2,
							}, nil).Once()
						}
					}

					err := worker.Handle(req)
					So(err, ShouldNotBeNil)
				})

				Convey("when there is a corresponding bits product for a configured actual", func() {
					petoziClient.On("GetBitsProducts", mock.Anything, mock.Anything).Return(&petozi.GetBitsProductsResp{
						BitsProducts: []*petozi.BitsProduct{
							{
								Id:         "id-1",
								BitsTypeId: "tv.twitch.ios.iap.bits.everyday.t2",
							},
						},
					}, nil).Once()

					for _, monthYear := range monthYears {
						for platformInt := range pachter.Platform_name {
							platform := pachter.Platform(platformInt)
							actualsDAO.On("Get", mock.Anything, platform, monthYear.Year, monthYear.Month).Return(map[string]uint32{
								"tv.twitch.ios.iap.bits.everyday.t2": 2,
							}, nil).Once()
						}
					}

					s3Client.On("PutFile", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()
					s3Client.On("GetTempSignedURL", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return("test-report-URL", nil).Once()

					err := worker.Handle(req)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}

func TestReduceToUniqueBitsType(t *testing.T) {
	Convey("Test various cases", t, func() {
		Convey("When it is empty", func() {
			So(reduceToUniqueBitsType([]*petozi.BitsProduct{}), ShouldHaveLength, 0)
		})

		Convey("When there are no duplicates", func() {
			So(reduceToUniqueBitsType([]*petozi.BitsProduct{
				{
					Id:         "1",
					BitsTypeId: "BT1",
				},
				{
					Id:         "2",
					BitsTypeId: "BT2",
				},
			}), ShouldHaveLength, 2)
		})

		Convey("When there is a duplicate", func() {
			So(reduceToUniqueBitsType([]*petozi.BitsProduct{
				{
					Id:         "1",
					BitsTypeId: "BT1",
				},
				{
					Id:         "2",
					BitsTypeId: "BT2",
				},
				{
					Id:         "2",
					BitsTypeId: "BT2",
				},
			}), ShouldHaveLength, 1)
		})
	})
}

func TestValidateInput(t *testing.T) {
	Convey("Test various cases", t, func() {
		Convey("when the request is nil", func() {
			err := validateInput(nil)
			So(err, ShouldNotBeNil)
		})

		Convey("when the request doesn't have any thing in it", func() {
			err := validateInput(&workermodel.BitsMonthlyReportRequest{})
			So(err, ShouldNotBeNil)
		})

		Convey("when the request has invalid report period", func() {
			err := validateInput(&workermodel.BitsMonthlyReportRequest{
				ReportPeriod: pointers.StringP("lower back pain"),
			})
			So(err, ShouldNotBeNil)
		})

		Convey("when the request is missing the year", func() {
			err := validateInput(&workermodel.BitsMonthlyReportRequest{
				TargetMonth: uint32P(3),
			})
			So(err, ShouldNotBeNil)
		})

		Convey("when the request has all fields populated", func() {
			err := validateInput(&workermodel.BitsMonthlyReportRequest{
				ReportPeriod: pointers.StringP(workermodel.REPORT_PERIOD_CURRENT_MONTH),
				TargetMonth:  uint32P(2),
				TargetYear:   uint32P(2020),
			})
			So(err, ShouldBeNil)
		})

		Convey("when the request has invalid month", func() {
			err := validateInput(&workermodel.BitsMonthlyReportRequest{
				TargetYear:  uint32P(2020),
				TargetMonth: uint32P(15),
			})
			So(err, ShouldNotBeNil)
		})
	})
}

type monthYear struct {
	Month uint32
	Year  uint32
}

func getBitsTypes(w Worker, month uint32, year uint32) ([]*petozi.BitsType, []monthYear) {
	startDate, endDate := time.GetStartEndDate(month, year)
	var reportEntries []ReportEntry
	err := w.RDS.Raw("SELECT * FROM get_bits_monthly_report_revolutions(?, ?)", startDate, endDate).Scan(&reportEntries).Error
	So(err, ShouldBeNil)

	uniquePetoziBitsTypes := make(map[string]*petozi.BitsType)
	uniqueMonthYears := make(map[string]monthYear)

	for _, reportEntry := range reportEntries {
		uniquePetoziBitsTypes[*reportEntry.BitsType] = &petozi.BitsType{
			Id:                    *reportEntry.BitsType,
			BusinessAttribution:   petozi.BusinessAttribution_PAYMENT_TRANSACTION,
			EntitlementSourceType: petozi.EntitlementSourceType_DIRECT_ENTITLEMENT,
		}

		uniqueMonthYears[toMonthYearString(reportEntry)] = monthYear{
			Month: uint32(reportEntry.BitsTypeMonthYear.Month()),
			Year:  uint32(reportEntry.BitsTypeMonthYear.Year()),
		}
	}

	petoziBitsTypes := make([]*petozi.BitsType, 0)
	for _, petoziBitsType := range uniquePetoziBitsTypes {
		petoziBitsTypes = append(petoziBitsTypes, petoziBitsType)
	}

	monthYears := make([]monthYear, 0)
	for _, monthYear := range uniqueMonthYears {
		monthYears = append(monthYears, monthYear)
	}

	return petoziBitsTypes, monthYears
}

func uint32P(x uint32) *uint32 {
	return &x
}
