package bits_spend_v2

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachter/backend/clients/redis"
	"code.justin.tv/commerce/pachter/backend/clients/sfn"
	dynamo_models "code.justin.tv/commerce/pachter/backend/dynamo/models"
	"code.justin.tv/commerce/pachter/backend/dynamo/worker_audit"
	"code.justin.tv/commerce/pachter/backend/metrics"
	"code.justin.tv/commerce/pachter/backend/rds/bits_spend"
	"code.justin.tv/commerce/pachter/backend/rds/models"
	"code.justin.tv/commerce/pachter/backend/worker"
	"code.justin.tv/commerce/pachter/config"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
)

const (
	redisKeyFormat        = "user-bits-transactions-%s"
	zRangeBatchSize       = 50
	rdsLatency            = "rds-spend-batch-put-latency"
	dynamoLatency         = "dynamo-audit-put-latency"
	zPopMinLatency        = "z-pop-min-latency"
	workerName            = "bits-spend-v2"
	resetterExecutionName = "spend-order-reset-%s-%s"
	resetterTimeout       = 60 * time.Second
	pinataBitsType        = "PinataBits"
)

type Worker struct {
	BitsSpendDAO bits_spend.DAO    `inject:"bits-spend-dao"`
	AuditDAO     worker_audit.DAO  `inject:""`
	RedisClient  redis.RedisClient `inject:""`
	Statter      metrics.Statter   `inject:""`
	Config       *config.Config    `inject:""`
	SFNClient    sfn.SFNClient     `inject:""`
}

// Looks up the user's past Bits transactions and determines from which Bits purchase
// this Bits being spent originates
func (w *Worker) Handle(msg *sqs.Message) error {
	ctx, cancel := context.WithTimeout(context.Background(), 270*time.Second)
	defer cancel()

	trx, err := worker.ParseDeferredBalanceUpdateMessage(msg)
	if err != nil {
		logrus.WithField("msg", *msg.Body).WithError(err).
			Error("Failed to parse deferred transaction")
		return err
	}
	logger := logrus.WithFields(logrus.Fields{
		"worker":      workerName,
		"transaction": trx,
	})
	logger.Info("Received SQS message.")

	// check if this transaction has already been processed
	existingTrx, err := w.AuditDAO.Get(ctx, trx.TransactionID)
	if err != nil {
		logger.WithError(err).Error("Failed to get audit record")
		return err
	}
	if existingTrx != nil && existingTrx.Spend != nil {
		return nil
	}

	// we don't want to process cheers of type pinata
	// (normally we don't get bits type on cheer, this processor is supposed to determine it).
	if pointers.StringOrDefault(trx.BitsType, "") == pinataBitsType &&
		trx.TransactionType == paydayrpc.TransactionType_GiveBitsToBroadcaster.String() {
		logger.Info("Skipping processing spend order on Bits Pinata type cheer")

		// e.g. q == 0 when old Respawn customer charge back with no Bits balance change
	} else if trx.Quantity != 0 && !trx.ShouldBeSkipped() {
		// calculate spend order
		bitsSpends, err := w.process(ctx, trx)
		if err != nil {
			// this still doesn't necessarily guarantee that it will succeed.
			// if there is a missing transaction, the resetting will fail too.
			return w.resetSpendOrder(ctx, trx, logger)
		}

		// save to RDS
		rdsStartTime := time.Now()
		err = w.BitsSpendDAO.BatchPut(bitsSpends)
		go w.Statter.Duration(rdsLatency, time.Since(rdsStartTime))
		if err != nil {
			logger.WithError(err).Errorf("Failed to record Bits spends: %+v", bitsSpends)
			return err
		}
	}

	// save to audit table
	dynamoStartTime := time.Now()
	_, err = w.AuditDAO.Put(ctx, &dynamo_models.WorkerAuditEntry{
		TransactionID: trx.TransactionID,
		Spend:         &dynamoStartTime,
	})
	go w.Statter.Duration(dynamoLatency, time.Since(dynamoStartTime))
	if err != nil {
		logger.WithError(err).Error("Failed to record worker audit entry")
		return err
	}

	return nil
}

func (w *Worker) process(ctx context.Context, event models.BitsTransaction) ([]models.BitsSpend, error) {
	redisKey := redisKey(event.RequestingTwitchUserID)
	logger := logrus.WithFields(logrus.Fields{
		"worker":        workerName,
		"redisKey":      redisKey,
		"transactionId": event.TransactionID,
		"event":         event,
	})
	var err error

	// "add bits" transaction gets automatically added to the sorted set
	txType := models.TransactionType(event.TransactionType)
	if txType == models.TransactionType_AddBits || txType == models.TransactionType_AdminAddBits {
		bitsSpent := event.ToBitsSpend()
		_, err = w.RedisClient.ZAdd(redis.BitsTransactionZAddCmd, redisKey, bitsSpent.Datetime.Unix(), bitsSpent)
		if err != nil {
			logger.WithError(err).Error("Error adding 'Add Bits' transaction to sorted set")
			return nil, err
		}

		return []models.BitsSpend{
			bitsSpent,
		}, nil
	}

	var bitsSpendsToAdd []models.BitsSpend
	remainingAmountToBurn := -1 * event.Quantity
	for remainingAmountToBurn < 0 {
		pastBitsAdds, err := w.zRange(redisKey, 0, zRangeBatchSize-1)
		if err != nil {
			logger.WithError(err).Error("error doing Z-range")
			return nil, err
		}

		if len(pastBitsAdds) == 0 {
			logger.WithFields(logrus.Fields{
				"bitsSpendstoAdd":       bitsSpendsToAdd,
				"remainingAmountToBurn": remainingAmountToBurn,
			}).Error("found no past entitlements for the remaining spending")
			return bitsSpendsToAdd, errors.New("found no past entitlements for the remaining spending")
		}

		for _, pastBitsAdd := range pastBitsAdds {
			previousTotalSpent := remainingAmountToBurn
			remainingAmountToBurn += pastBitsAdd.Quantity

			if remainingAmountToBurn >= 0 {
				bitsSpendNewEntry := event.ToBitsSpend()
				bitsSpendNewEntry.ReferenceTransactionID = pastBitsAdd.TransactionID
				bitsSpendNewEntry.PurchasePriceUSC = pastBitsAdd.PurchasePriceUSC
				bitsSpendNewEntry.Quantity = -1 * previousTotalSpent
				bitsSpendsToAdd = append(bitsSpendsToAdd, bitsSpendNewEntry)

				if remainingAmountToBurn == 0 {
					zpopMinStart := time.Now()
					_, err = w.RedisClient.ZPopMin(redisKey)
					go w.Statter.Duration(zPopMinLatency, time.Since(zpopMinStart))
					if err != nil {
						logger.WithError(err).Error("ZPopMin failed")
						return nil, err
					}
				} else {
					// update the oldest transaction
					zpopMinStart := time.Now()
					_, err = w.RedisClient.ZPopMin(redisKey)
					go w.Statter.Duration(zPopMinLatency, time.Since(zpopMinStart))
					if err != nil {
						logger.WithError(err).Error("ZPopMin failed")
						return nil, err
					}

					pastBitsAdd.Quantity += previousTotalSpent
					_, err = w.RedisClient.ZAdd(redis.BitsTransactionZAddCmd, redisKey, pastBitsAdd.Datetime.Unix(), pastBitsAdd)
					if err != nil {
						logger.WithError(err).Error("ZAdd failed")
						return nil, err
					}
				}

				break
			} else {
				bitsSpendNewEntry := event.ToBitsSpend()
				bitsSpendNewEntry.ReferenceTransactionID = pastBitsAdd.TransactionID
				bitsSpendNewEntry.PurchasePriceUSC = pastBitsAdd.PurchasePriceUSC
				bitsSpendNewEntry.Quantity = pastBitsAdd.Quantity
				bitsSpendsToAdd = append(bitsSpendsToAdd, bitsSpendNewEntry)

				zpopMinStart := time.Now()
				_, err = w.RedisClient.ZPopMin(redisKey)
				go w.Statter.Duration(zPopMinLatency, time.Since(zpopMinStart))
				if err != nil {
					logger.WithError(err).Error("ZPopMin failed")
					return nil, err
				}
			}
		}
	}

	return bitsSpendsToAdd, nil
}

func redisKey(userID string) string {
	return fmt.Sprintf(redisKeyFormat, userID)
}

func (w *Worker) zRange(redisKey string, startIndex, endIndex int64) ([]models.BitsSpend, error) {
	values, err := w.RedisClient.ZRange(redisKey, startIndex, endIndex)
	if err != nil {
		return nil, err
	}

	bitsSpends := make([]models.BitsSpend, len(values))
	for i, cachedValue := range values {
		bitsSpend := models.BitsSpend{}
		err = json.Unmarshal([]byte(cachedValue), &bitsSpend)
		if err != nil {
			return nil, err
		}

		bitsSpends[i] = bitsSpend
	}

	return bitsSpends, nil
}

func (w *Worker) resetSpendOrder(ctx context.Context, tx models.BitsTransaction, logger *logrus.Entry) error {
	type SFNInput struct {
		Users []string `json:"users"`
	}

	logger.Info("Starting SpendOrderResetter SFN")
	executionName := fmt.Sprintf(resetterExecutionName, tx.RequestingTwitchUserID, uuid.Must(uuid.NewV4()).String())

	executionARN, err := w.SFNClient.Execute(ctx, w.Config.SpendOrderResetterSfnARN, executionName, SFNInput{
		Users: []string{tx.RequestingTwitchUserID},
	})
	if err != nil {
		logger.WithError(err).Error("Failed to start SpendOrderResetter SFN")
		return err
	}

	sfnExecutionComplete := false
	sfnSucceeded := false
	timeOut := false
	loopStart := time.Now()
	for {
		if sfnExecutionComplete {
			break
		}

		time.Sleep(5 * time.Second)

		sfnEvents, err := w.SFNClient.GetExecutionHistory(ctx, executionARN)
		if err != nil {
			logger.WithError(err).Error("Failed to GetExecutionHistory")
			return err
		}

		if len(sfnEvents) > 0 {
			eventType := *(sfnEvents[0].Type)
			if eventType == "ExecutionSucceeded" {
				sfnExecutionComplete = true
				sfnSucceeded = true
			}

			if eventType == "ExecutionFailed" ||
				eventType == "ExecutionAborted" ||
				eventType == "ExecutionTimedOut" {
				sfnExecutionComplete = true
			}
		}

		// resetter takes ~35s at most
		if time.Since(loopStart).Seconds() > resetterTimeout.Seconds() {
			timeOut = true
			break
		}
	}

	// these two are the error cases where there is likely a missing "AddBits"
	// transaction for the user, who have a lot of transactions in his history
	if !sfnSucceeded && !timeOut {
		return errors.New("Resetter failed")
	}

	if !sfnSucceeded && timeOut {
		return errors.New("Resetter is taking too long")
	}

	return nil
}
