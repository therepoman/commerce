package bits_spend_v2

import (
	"encoding/json"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	dynamo_models "code.justin.tv/commerce/pachter/backend/dynamo/models"
	rds_models "code.justin.tv/commerce/pachter/backend/rds/models"
	"code.justin.tv/commerce/pachter/config"
	redis_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/clients/redis"
	sfn_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/clients/sfn"
	audit_dao_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/dynamo/worker_audit"
	metrics_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/metrics"
	spend_dao_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/rds/bits_spend"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func Test_Handle(t *testing.T) {
	Convey("Given spend worker v2", t, func() {
		mockSpendDAO := new(spend_dao_mock.DAO)
		mockAuditDAO := new(audit_dao_mock.DAO)
		mockRedis := new(redis_mock.RedisClient)
		mockStatter := new(metrics_mock.Statter)
		mockSFN := new(sfn_mock.SFNClient)
		w := Worker{
			BitsSpendDAO: mockSpendDAO,
			AuditDAO:     mockAuditDAO,
			RedisClient:  mockRedis,
			Statter:      mockStatter,
			Config: &config.Config{
				SpendOrderResetterSfnARN: "arn:lesad-spend-order",
			},
			SFNClient: mockSFN,
		}
		now := time.Now()
		mockStatter.On("Duration", mock.Anything, mock.Anything).
			Return(nil)

		Convey("when AuditDAO.Get fails", func() {
			mockAuditDAO.On("Get", mock.Anything, mock.Anything).
				Return(nil, errors.New("lesad audit")).Once()
			err := w.Handle(SampleSQSMsg(0))

			So(err, ShouldNotBeNil)
			mockRedis.AssertNumberOfCalls(t, "ZAdd", 0)
			mockSpendDAO.AssertNumberOfCalls(t, "BatchPut", 0)
			mockAuditDAO.AssertNumberOfCalls(t, "Put", 0)
		})

		Convey("when an audit record already exists", func() {
			mockAuditDAO.On("Get", mock.Anything, mock.Anything).
				Return(&dynamo_models.WorkerAuditEntry{
					Spend: &now,
				}, nil).Once()
			err := w.Handle(SampleSQSMsg(0))

			So(err, ShouldBeNil)
			mockRedis.AssertNumberOfCalls(t, "ZAdd", 0)
			mockSpendDAO.AssertNumberOfCalls(t, "BatchPut", 0)
			mockAuditDAO.AssertNumberOfCalls(t, "Put", 0)
		})

		mockAuditDAO.On("Get", mock.Anything, mock.Anything).
			Return(nil, nil)
		Convey("when the transaction type is CreateHold (skipped)", func() {

			Convey("when AuditDAO.Put fails", func() {
				mockAuditDAO.On("Put", mock.Anything, mock.Anything).
					Return(nil, errors.New("lesad audit put")).Once()
				err := w.Handle(SampleSQSMsg(0, paydayrpc.TransactionType_CreateHold.String()))

				So(err, ShouldNotBeNil)
				mockRedis.AssertNumberOfCalls(t, "ZAdd", 0)
				mockSpendDAO.AssertNumberOfCalls(t, "BatchPut", 0)
				mockAuditDAO.AssertNumberOfCalls(t, "Put", 1)
			})

			Convey("when AuditDAO.Put succeeds", func() {
				mockAuditDAO.On("Put", mock.Anything, mock.Anything).
					Return(nil, nil).Once()
				err := w.Handle(SampleSQSMsg(100, paydayrpc.TransactionType_CreateHold.String()))

				So(err, ShouldBeNil)
				mockRedis.AssertNumberOfCalls(t, "ZAdd", 0)
				mockSpendDAO.AssertNumberOfCalls(t, "BatchPut", 0)
				mockAuditDAO.AssertNumberOfCalls(t, "Put", 1)
			})
		})

		Convey("when the transaction type is Cheer with PinataBits (skipped)", func() {
			mockAuditDAO.On("Put", mock.Anything, mock.Anything).
				Return(nil, nil).Once()

			deferredTrxJson, _ := json.Marshal(rds_models.BitsTransaction{
				Quantity:        1000,
				TransactionType: paydayrpc.TransactionType_GiveBitsToBroadcaster.String(),
				BitsType:        pointers.StringP("PinataBits"),
			})

			err := w.Handle(&sqs.Message{
				Body: aws.String(string(deferredTrxJson)),
			})
			So(err, ShouldBeNil)
			mockRedis.AssertNumberOfCalls(t, "ZAdd", 0)
			mockSpendDAO.AssertNumberOfCalls(t, "BatchPut", 0)
			mockAuditDAO.AssertNumberOfCalls(t, "Put", 1)
		})

		Convey("when the transaction type is AddBits (non-skippable)", func() {
			Convey("when spend order fails and SFN is initiated", func() {
				Convey("when the resetter SFN fails", func() {
					mockRedis.On("ZAdd", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
						Return(0, errors.New("lesad spend order")).Once()
					mockSFN.On("Execute", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
						Return("", errors.New("lesad sfn")).Once()
					err := w.Handle(SampleSQSMsg(100, paydayrpc.TransactionType_AddBits.String()))

					So(err, ShouldNotBeNil)
					mockRedis.AssertNumberOfCalls(t, "ZAdd", 1)
					mockSFN.AssertNumberOfCalls(t, "Execute", 1)
					mockSpendDAO.AssertNumberOfCalls(t, "BatchPut", 0)
					mockAuditDAO.AssertNumberOfCalls(t, "Put", 0)
				})

				Convey("when the resetter SFN succeeds", func() {
					mockRedis.On("ZAdd", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
						Return(0, errors.New("lesad spend order")).Once()
					mockSFN.On("Execute", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
						Return("sfn-arn", nil).Once()

					Convey("when GetExecutionHistory fails", func() {
						mockSFN.On("GetExecutionHistory", mock.Anything, mock.Anything).
							Return([]*sfn.HistoryEvent{}, errors.New("lesad history")).Once()

						err := w.Handle(SampleSQSMsg(100, paydayrpc.TransactionType_AddBits.String()))

						So(err, ShouldNotBeNil)
						mockRedis.AssertNumberOfCalls(t, "ZAdd", 1)
						mockSFN.AssertNumberOfCalls(t, "Execute", 1)
						mockSFN.AssertNumberOfCalls(t, "GetExecutionHistory", 1)
						mockSpendDAO.AssertNumberOfCalls(t, "BatchPut", 0)
						mockAuditDAO.AssertNumberOfCalls(t, "Put", 0)
					})

					Convey("when GetExecutionHistory succeeds (SFN success)", func() {
						mockSFN.On("GetExecutionHistory", mock.Anything, mock.Anything).
							Return([]*sfn.HistoryEvent{
								{
									Id:   aws.Int64(10),
									Type: aws.String("ExecutionSucceeded"),
								},
							}, nil).Once()

						err := w.Handle(SampleSQSMsg(100, paydayrpc.TransactionType_AddBits.String()))

						So(err, ShouldBeNil)
						mockRedis.AssertNumberOfCalls(t, "ZAdd", 1)
						mockSFN.AssertNumberOfCalls(t, "Execute", 1)
						mockSFN.AssertNumberOfCalls(t, "GetExecutionHistory", 1)
						mockSpendDAO.AssertNumberOfCalls(t, "BatchPut", 0)
						mockAuditDAO.AssertNumberOfCalls(t, "Put", 0)
					})

					Convey("when GetExecutionHistory succeeds (SFN fail)", func() {
						mockSFN.On("GetExecutionHistory", mock.Anything, mock.Anything).
							Return([]*sfn.HistoryEvent{
								{
									Id:   aws.Int64(10),
									Type: aws.String("ExecutionFailed"),
								},
							}, nil).Once()

						err := w.Handle(SampleSQSMsg(100, paydayrpc.TransactionType_AddBits.String()))

						if err == nil {
							t.Fail()
						}
						So(err.Error(), ShouldEqual, "Resetter failed")
						mockRedis.AssertNumberOfCalls(t, "ZAdd", 1)
						mockSFN.AssertNumberOfCalls(t, "Execute", 1)
						mockSFN.AssertNumberOfCalls(t, "GetExecutionHistory", 1)
						mockSpendDAO.AssertNumberOfCalls(t, "BatchPut", 0)
						mockAuditDAO.AssertNumberOfCalls(t, "Put", 0)
					})

					// WARNING: this test takes 1 minute to finish
					Convey("when the SFN is taking too long to finish", func() {
						mockSFN.On("GetExecutionHistory", mock.Anything, mock.Anything).
							Return([]*sfn.HistoryEvent{
								{
									Id:   aws.Int64(10),
									Type: aws.String("ExecutionStarted"),
								},
							}, nil)

						err := w.Handle(SampleSQSMsg(100, paydayrpc.TransactionType_AddBits.String()))

						if err == nil {
							t.Fail()
						}
						So(err.Error(), ShouldEqual, "Resetter is taking too long")
						mockRedis.AssertNumberOfCalls(t, "ZAdd", 1)
						mockSFN.AssertNumberOfCalls(t, "Execute", 1)
						mockSFN.AssertNumberOfCalls(t, "GetExecutionHistory", 12)
						mockSpendDAO.AssertNumberOfCalls(t, "BatchPut", 0)
						mockAuditDAO.AssertNumberOfCalls(t, "Put", 0)
					})
				})
			})

			Convey("when RDS.Put fails", func() {
				mockRedis.On("ZAdd", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
					Return(1, nil)
				mockSpendDAO.On("BatchPut", mock.Anything).
					Return(errors.New("lesad RDS")).Once()
				err := w.Handle(SampleSQSMsg(100, paydayrpc.TransactionType_AddBits.String()))

				So(err, ShouldNotBeNil)
				mockRedis.AssertNumberOfCalls(t, "ZAdd", 1)
				mockSpendDAO.AssertNumberOfCalls(t, "BatchPut", 1)
				mockAuditDAO.AssertNumberOfCalls(t, "Put", 0)
			})

			Convey("when RDS.Put succeeds", func() {
				mockRedis.On("ZAdd", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
					Return(1, nil)
				mockSpendDAO.On("BatchPut", mock.Anything).
					Return(nil).Once()
				mockAuditDAO.On("Put", mock.Anything, mock.Anything).
					Return(nil, nil).Once()
				err := w.Handle(SampleSQSMsg(100, paydayrpc.TransactionType_AddBits.String()))

				So(err, ShouldBeNil)
				mockRedis.AssertNumberOfCalls(t, "ZAdd", 1)
				mockSpendDAO.AssertNumberOfCalls(t, "BatchPut", 1)
				mockAuditDAO.AssertNumberOfCalls(t, "Put", 1)
			})
		})
	})
}

func SampleSQSMsg(quantity int64, transactionType ...string) *sqs.Message {
	tranType := ""
	if len(transactionType) > 0 {
		tranType = transactionType[0]
	}

	deferredTrxJson, _ := json.Marshal(rds_models.BitsTransaction{
		Quantity:        quantity,
		TransactionType: tranType,
	})

	return &sqs.Message{
		Body: aws.String(string(deferredTrxJson)),
	}
}
