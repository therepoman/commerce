package bits_transaction_v2

import (
	"encoding/json"
	"errors"
	"testing"
	"time"

	dynamo_models "code.justin.tv/commerce/pachter/backend/dynamo/models"
	"code.justin.tv/commerce/pachter/backend/worker"
	audit_dao_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/dynamo/worker_audit"
	metrics_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/metrics"
	tx_dao_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/rds/bits_transaction"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/golang/protobuf/ptypes"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func Test_Handle(t *testing.T) {
	Convey("Given transaction worker v2", t, func() {
		mockTxDAO := new(tx_dao_mock.DAO)
		mockAuditDAO := new(audit_dao_mock.DAO)
		mockStatter := new(metrics_mock.Statter)
		w := Worker{
			TransactionsDAO: mockTxDAO,
			AuditDAO:        mockAuditDAO,
			Statter:         mockStatter,
		}
		now := time.Now()
		mockStatter.On("Duration", mock.Anything, mock.Anything).
			Return(nil)

		Convey("when AuditDAO.Get fails", func() {
			mockAuditDAO.On("Get", mock.Anything, mock.Anything).
				Return(nil, errors.New("lesad audit")).Once()
			err := w.Handle(SampleSQSMsg())

			So(err, ShouldNotBeNil)
			mockTxDAO.AssertNumberOfCalls(t, "Put", 0)
			mockAuditDAO.AssertNumberOfCalls(t, "Put", 0)
		})

		Convey("when an audit record already exists", func() {
			mockAuditDAO.On("Get", mock.Anything, mock.Anything).
				Return(&dynamo_models.WorkerAuditEntry{
					Transactions: &now,
				}, nil).Once()
			err := w.Handle(SampleSQSMsg())

			So(err, ShouldBeNil)
			mockTxDAO.AssertNumberOfCalls(t, "Put", 0)
			mockAuditDAO.AssertNumberOfCalls(t, "Put", 0)
		})

		mockAuditDAO.On("Get", mock.Anything, mock.Anything).
			Return(nil, nil)
		Convey("when the transaction should be skipped", func() {
			Convey("when AuditDAO.Put fails", func() {
				mockAuditDAO.On("Put", mock.Anything, mock.Anything).
					Return(nil, errors.New("lesad audit put")).Once()
				err := w.Handle(SampleSQSMsg(paydayrpc.TransactionType_CreateHold.String()))

				So(err, ShouldNotBeNil)
				mockTxDAO.AssertNumberOfCalls(t, "Put", 0)
			})

			Convey("when AuditDAO.Put succeeds", func() {
				mockAuditDAO.On("Put", mock.Anything, mock.Anything).
					Return(nil, nil).Once()
				err := w.Handle(SampleSQSMsg(paydayrpc.TransactionType_CreateHold.String()))

				So(err, ShouldBeNil)
				mockTxDAO.AssertNumberOfCalls(t, "Put", 0)
			})
		})

		Convey("when the transaction should not be skipped", func() {
			Convey("when RDS.Put fails", func() {
				mockTxDAO.On("Put", mock.Anything).
					Return(errors.New("lesad RDS #1")).Once()
				err := w.Handle(SampleSQSMsg(paydayrpc.TransactionType_GiveBitsToBroadcaster.String()))

				So(err, ShouldNotBeNil)
				mockAuditDAO.AssertNumberOfCalls(t, "Put", 0)
			})

			Convey("when RDS.Put succeeds", func() {
				mockTxDAO.On("Put", mock.Anything).Return(nil).Once()
				mockAuditDAO.On("Put", mock.Anything, mock.Anything).
					Return(nil, nil).Once()
				err := w.Handle(SampleSQSMsg(paydayrpc.TransactionType_GiveBitsToBroadcaster.String()))

				So(err, ShouldBeNil)
				mockTxDAO.AssertCalled(t, "Put", mock.Anything)
				mockAuditDAO.AssertCalled(t, "Put", mock.Anything, mock.Anything)
			})
		})
	})
}

func SampleSQSMsg(transactionType ...string) *sqs.Message {
	tranType := ""
	if len(transactionType) > 0 {
		tranType = transactionType[0]
	}

	txJson, _ := json.Marshal(&paydayrpc.Transaction{
		TransactionId:   "test-tran-id",
		TransactionType: tranType,
		TimeOfEvent:     ptypes.TimestampNow(),
		LastUpdated:     ptypes.TimestampNow(),
	})

	snsMsg := worker.SNSMessage{
		Message: string(txJson),
	}
	snsMsgJson, _ := json.Marshal(&snsMsg)

	return &sqs.Message{
		Body: aws.String(string(snsMsgJson)),
	}
}
