package bits_transaction_v2

import (
	"context"
	"time"

	dynamo_models "code.justin.tv/commerce/pachter/backend/dynamo/models"
	"code.justin.tv/commerce/pachter/backend/dynamo/worker_audit"
	"code.justin.tv/commerce/pachter/backend/metrics"
	"code.justin.tv/commerce/pachter/backend/rds"
	"code.justin.tv/commerce/pachter/backend/rds/bits_transaction"
	"code.justin.tv/commerce/pachter/backend/worker"
	"github.com/aws/aws-sdk-go/service/sqs"
)

const (
	workerName    = "bits-transactions-v2"
	rdsLatency    = "rds-transactions-put-latency"
	dynamoLatency = "dynamo-audit-put-latency"
)

type Worker struct {
	TransactionsDAO bits_transaction.DAO `inject:""`
	AuditDAO        worker_audit.DAO     `inject:""`
	Statter         metrics.Statter      `inject:""`
}

func (w *Worker) Handle(msg *sqs.Message) error {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	trx, logger, err := worker.ParsePaydayBalanceUpdateMessage(msg, workerName)
	if err != nil {
		return err
	}

	// check if this transaction has already been processed
	existingTrx, err := w.AuditDAO.Get(ctx, trx.TransactionID)
	if err != nil {
		logger.WithError(err).Error("Failed to get audit record")
		return err
	}
	if existingTrx != nil && existingTrx.Transactions != nil {
		return nil
	}

	// save to RDS
	if !trx.ShouldBeSkipped() {
		rdsStartTime := time.Now()
		err = w.TransactionsDAO.Put(&trx)
		go w.Statter.Duration(rdsLatency, time.Since(rdsStartTime))
		if err != nil && !rds.IsPKViolationError(err) {
			logger.WithError(err).Error("Failed to save a transaction")
			return err
		}
	}

	// save to audit table
	dynamoStartTime := time.Now()
	_, err = w.AuditDAO.Put(ctx, &dynamo_models.WorkerAuditEntry{
		TransactionID: trx.TransactionID,
		Transactions:  &dynamoStartTime,
	})
	go w.Statter.Duration(dynamoLatency, time.Since(dynamoStartTime))
	if err != nil {
		logger.WithError(err).Error("Failed to record worker audit entry")
		return err
	}

	return nil
}
