package bits_transaction_audit_v2

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/pachter/backend/dynamo/models"
	"code.justin.tv/commerce/pachter/backend/dynamo/worker_audit"
	"code.justin.tv/commerce/pachter/backend/worker"
	"github.com/aws/aws-sdk-go/service/sqs"
)

const (
	workerName = "bits-transaction-audit-v2"
)

type Worker struct {
	AuditDAO worker_audit.DAO `inject:""`
}

func (w *Worker) Handle(msg *sqs.Message) error {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	trx, logger, err := worker.ParsePaydayBalanceUpdateMessage(msg, workerName)
	if err != nil {
		return err
	}

	auditEntry, err := w.AuditDAO.Get(ctx, trx.TransactionID)
	if err != nil {
		logger.WithError(err).Error("Error getting audit entry for transaction")
		return err
	}

	err = validateAuditRecord(auditEntry)
	if err != nil {
		logger.WithError(err).Error("Incomplete audit entry for transaction")
		return err
	}

	return nil
}

func validateAuditRecord(auditEntry *models.WorkerAuditEntry) error {
	if auditEntry == nil {
		return errors.New("audit entry not found for transaction (v2)")
	}

	if auditEntry.Transactions == nil {
		return errors.New("audit entry does not have a timestamp for transactions worker v2")
	}

	if auditEntry.Cheermote == nil {
		return errors.New("audit entry does not have a timestamp for cheermote worker v2")
	}

	if auditEntry.Spend == nil {
		return errors.New("audit entry does not have a timestamp for spend worker v2")
	}

	return nil
}
