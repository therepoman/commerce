package bits_transaction_audit_v2

import (
	"encoding/json"
	"errors"
	"testing"
	"time"

	dynamo_models "code.justin.tv/commerce/pachter/backend/dynamo/models"
	"code.justin.tv/commerce/pachter/backend/worker"
	audit_dao_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/dynamo/worker_audit"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/golang/protobuf/ptypes"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func Test_Handle(t *testing.T) {
	Convey("Given audit worker v2", t, func() {
		mockAuditDAO := new(audit_dao_mock.DAO)
		w := Worker{
			AuditDAO: mockAuditDAO,
		}
		now := time.Now()

		Convey("when AuditDAO.Get fails", func() {
			mockAuditDAO.On("Get", mock.Anything, mock.Anything).
				Return(nil, errors.New("lesad audit")).Once()
			err := w.Handle(SampleSQSMsg())

			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "lesad audit")
		})

		Convey("when AuditDAO.Get returns nil record for some reason", func() {
			mockAuditDAO.On("Get", mock.Anything, mock.Anything).
				Return(nil, nil).Once()
			err := w.Handle(SampleSQSMsg())

			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "audit entry not found for transaction (v2)")
		})

		Convey("when transaction is missing", func() {
			mockAuditDAO.On("Get", mock.Anything, mock.Anything).
				Return(&dynamo_models.WorkerAuditEntry{}, nil).Once()
			err := w.Handle(SampleSQSMsg())

			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual,
				"audit entry does not have a timestamp for transactions worker v2")
		})

		Convey("when cheermote is missing", func() {
			mockAuditDAO.On("Get", mock.Anything, mock.Anything).
				Return(&dynamo_models.WorkerAuditEntry{
					Transactions: &now,
				}, nil).Once()
			err := w.Handle(SampleSQSMsg())

			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual,
				"audit entry does not have a timestamp for cheermote worker v2")
		})

		Convey("when spend is missing", func() {
			mockAuditDAO.On("Get", mock.Anything, mock.Anything).
				Return(&dynamo_models.WorkerAuditEntry{
					Transactions: &now,
					Cheermote:    &now,
				}, nil).Once()
			err := w.Handle(SampleSQSMsg())

			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual,
				"audit entry does not have a timestamp for spend worker v2")
		})

		Convey("when nothing is missing", func() {
			mockAuditDAO.On("Get", mock.Anything, mock.Anything).
				Return(&dynamo_models.WorkerAuditEntry{
					Transactions: &now,
					Cheermote:    &now,
					Spend:        &now,
				}, nil).Once()
			err := w.Handle(SampleSQSMsg())

			So(err, ShouldBeNil)
		})
	})
}

func SampleSQSMsg(transactionType ...string) *sqs.Message {
	tranType := ""
	if len(transactionType) > 0 {
		tranType = transactionType[0]
	}

	txJson, _ := json.Marshal(&paydayrpc.Transaction{
		TransactionId:   "test-tran-id",
		TransactionType: tranType,
		TimeOfEvent:     ptypes.TimestampNow(),
		LastUpdated:     ptypes.TimestampNow(),
	})

	snsMsg := worker.SNSMessage{
		Message: string(txJson),
	}
	snsMsgJson, _ := json.Marshal(&snsMsg)

	return &sqs.Message{
		Body: aws.String(string(snsMsgJson)),
	}
}
