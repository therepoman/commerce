package worker

import (
	"encoding/json"
	"testing"

	rds_models "code.justin.tv/commerce/pachter/backend/rds/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/golang/protobuf/ptypes"
	. "github.com/smartystreets/goconvey/convey"
)

func Test_ParsePaydayBalanceUpdateMessage(t *testing.T) {
	Convey("Given ParsePaydayBalanceUpdateMessage utility function", t, func() {
		testWorker := "test-worker"
		Convey("when SQS message is corrupted", func() {
			_, _, err := ParsePaydayBalanceUpdateMessage(&sqs.Message{
				Body: aws.String("lesad SQS"),
			}, testWorker)

			So(err, ShouldNotBeNil)
		})

		Convey("when SNS message is corrupted", func() {
			snsMsg := SNSMessage{
				Message: "lesad SNS",
			}
			snsMsgJson, _ := json.Marshal(&snsMsg)
			_, _, err := ParsePaydayBalanceUpdateMessage(&sqs.Message{
				Body: aws.String(string(snsMsgJson)),
			}, testWorker)

			So(err, ShouldNotBeNil)
		})

		Convey("when transaction id is missing", func() {
			_, _, err := ParsePaydayBalanceUpdateMessage(SampleSQSMsg(), testWorker)

			So(err.Error(), ShouldEqual, "transaction id is missing")
		})

		Convey("when all goes well", func() {
			transaction, _, err := ParsePaydayBalanceUpdateMessage(
				SampleSQSMsg("123"), testWorker)

			So(err, ShouldBeNil)
			So(transaction.TransactionID, ShouldEqual, "123")
		})
	})
}

func Test_ParseDeferredBalanceUpdateMessage(t *testing.T) {
	Convey("Given ParseDeferredBalanceUpdateMessage utility function", t, func() {
		Convey("when SQS message body is corrupted", func() {
			_, err := ParseDeferredBalanceUpdateMessage(&sqs.Message{
				Body: aws.String("abc"),
			})

			So(err, ShouldNotBeNil)
		})

		Convey("when all goes well", func() {
			deferredTrxJson, _ := json.Marshal(rds_models.BitsTransaction{
				TransactionID: "xyz",
			})

			tx, err := ParseDeferredBalanceUpdateMessage(&sqs.Message{
				Body: aws.String(string(deferredTrxJson)),
			})

			So(err, ShouldBeNil)
			So(tx.TransactionID, ShouldEqual, "xyz")
		})
	})
}

func SampleSQSMsg(transactionIDs ...string) *sqs.Message {
	tranID := ""
	if len(transactionIDs) > 0 {
		tranID = transactionIDs[0]
	}

	txJson, _ := json.Marshal(&paydayrpc.Transaction{
		TransactionId: tranID,
		TimeOfEvent:   ptypes.TimestampNow(),
		LastUpdated:   ptypes.TimestampNow(),
	})

	snsMsg := SNSMessage{
		Message: string(txJson),
	}
	snsMsgJson, _ := json.Marshal(&snsMsg)

	return &sqs.Message{
		Body: aws.String(string(snsMsgJson)),
	}
}
