package server

import (
	"context"

	"code.justin.tv/commerce/pachter/backend/api/actuals"
	"code.justin.tv/commerce/pachter/backend/api/get_bits_amounts_for_channel"
	"code.justin.tv/commerce/pachter/backend/api/get_bits_monthly_amounts_for_all_channels"
	"code.justin.tv/commerce/pachter/backend/api/get_bits_monthly_report"
	"code.justin.tv/commerce/pachter/backend/api/get_emote_usage_monthly_report"
	"code.justin.tv/commerce/pachter/backend/api/get_spend_order"
	pachter "code.justin.tv/commerce/pachter/rpc"
)

type server struct {
	BitsMonthlyReportAPI                   get_bits_monthly_report.API                   `inject:""`
	EmoteUsageMonthlyReportAPI             get_emote_usage_monthly_report.API            `inject:""`
	ActualsAPI                             actuals.API                                   `inject:""`
	BitsSpendOrderAPI                      get_spend_order.API                           `inject:""`
	GetBitsAmountsForChannelAPI            get_bits_amounts_for_channel.API              `inject:""`
	GetBitsMonthlyAmountsForAllChannelsAPI get_bits_monthly_amounts_for_all_channels.API `inject:""`
}

func NewServer() pachter.Pachter {
	return &server{}
}

func (s *server) HealthCheck(ctx context.Context, req *pachter.HealthCheckReq) (*pachter.HealthCheckResp, error) {
	return &pachter.HealthCheckResp{}, nil
}

func (s *server) GetBitsMonthlyReport(ctx context.Context, req *pachter.GetBitsMonthlyReportReq) (*pachter.GetBitsMonthlyReportResp, error) {
	return s.BitsMonthlyReportAPI.Get(ctx, req)
}

func (s *server) GetEmoteUsageMonthlyReport(ctx context.Context, req *pachter.GetEmoteUsageMonthlyReportReq) (*pachter.GetEmoteUsageMonthlyReportResp, error) {
	return s.EmoteUsageMonthlyReportAPI.Get(ctx, req)
}

func (s *server) GetBitsMonthlyAmountsForAllChannels(ctx context.Context, req *pachter.GetBitsMonthlyAmountsForAllChannelsReq) (*pachter.GetBitsMonthlyAmountsForAllChannelsResp, error) {
	return s.GetBitsMonthlyAmountsForAllChannelsAPI.Get(ctx, req)
}

func (s *server) GetBitsAmountsForChannel(ctx context.Context, req *pachter.GetBitsAmountsForChannelReq) (*pachter.GetBitsAmountsForChannelResp, error) {
	return s.GetBitsAmountsForChannelAPI.Get(ctx, req)
}

func (s *server) GetActuals(ctx context.Context, req *pachter.GetActualsReq) (*pachter.GetActualsResp, error) {
	return s.ActualsAPI.Get(ctx, req)
}

func (s *server) SetActuals(ctx context.Context, req *pachter.SetActualsReq) (*pachter.SetActualsResp, error) {
	return s.ActualsAPI.Set(ctx, req)
}

func (s *server) GetBitsSpendOrder(ctx context.Context, req *pachter.GetBitsSpendOrderReq) (*pachter.GetBitsSpendOrderResp, error) {
	return s.BitsSpendOrderAPI.Get(ctx, req)
}
