package rds

import (
	"encoding/json"
	"errors"
	"fmt"

	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/pachter/config"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type secretsManagerData struct {
	UserName string `json:"username"`
	Password string `json:"password"`
	Host     string `json:"host"`
	Port     int    `json:"port"`
	DBName   string `json:"dbname"`
}

func NewRDSClient(cfg *config.Config, sess *session.Session) (*gorm.DB, error) {
	dbCredentials, err := getCredentials(cfg, sess)
	if err != nil {
		return nil, err
	}

	host := dbCredentials.Host
	port := dbCredentials.Port

	if !strings.Blank(cfg.RDSEndpointOverride) {
		host = cfg.RDSEndpointOverride
	}
	if cfg.RDSPortOverride != 0 {
		port = cfg.RDSPortOverride
	}

	dbArgs := fmt.Sprintf("host=%s port=%d user=%s dbname=%s password=%s",
		host,
		port,
		dbCredentials.UserName,
		dbCredentials.DBName,
		dbCredentials.Password)
	db, err := gorm.Open("postgres", dbArgs)

	//TODO: figure out and set max open/idle connections like following. (https://jira.twitch.com/browse/BITS-4506)
	//db.DB().SetMaxOpenConns(10)
	//db.DB().SetMaxIdleConns(10)

	if err != nil {
		return nil, err
	}

	return db, nil
}

func getCredentials(cfg *config.Config, sess *session.Session) (*secretsManagerData, error) {
	secretsManager := secretsmanager.New(sess)
	secrets, err := secretsManager.GetSecretValue(&secretsmanager.GetSecretValueInput{
		SecretId: &cfg.RDSAWSSecretStoreARN,
	})
	if err != nil {
		return nil, err
	}

	if secrets == nil {
		return nil, errors.New("secret manager returned nil")
	}

	dbCredentials := secretsManagerData{}
	err = json.Unmarshal([]byte(*secrets.SecretString), &dbCredentials)
	if err != nil {
		return nil, err
	}
	return &dbCredentials, nil
}
