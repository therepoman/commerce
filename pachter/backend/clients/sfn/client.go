package sfn

import (
	"context"
	"encoding/json"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/aws/aws-sdk-go/service/sfn/sfniface"
	"github.com/pkg/errors"
)

const (
	defaultRegion = "us-west-2"
)

type SFNClient interface {
	Execute(ctx context.Context, stateMachineARN string, executionName string, executionInput interface{}) (string, error)
	GetExecutionHistory(ctx context.Context, executionARN string) ([]*sfn.HistoryEvent, error)
}

type sfnClient struct {
	SDKClient sfniface.SFNAPI
}

func NewDefaultClient() (SFNClient, error) {
	sess, err := session.NewSession()
	if err != nil {
		return nil, err
	}
	return &sfnClient{
		SDKClient: sfn.New(sess, &aws.Config{
			Region: aws.String(defaultRegion),
		}),
	}, nil
}

func (c *sfnClient) Execute(ctx context.Context, stateMachineARN string, executionName string, executionInput interface{}) (string, error) {
	inputBytes, err := json.Marshal(executionInput)
	if err != nil {
		return "", errors.Wrapf(err, "failed to marshall SFN input. sfnARN: %s input: %+v", stateMachineARN, executionInput)
	}

	opt, err := c.SDKClient.StartExecutionWithContext(ctx, &sfn.StartExecutionInput{
		Input:           aws.String(string(inputBytes)),
		Name:            &executionName,
		StateMachineArn: &stateMachineARN,
	})

	return *opt.ExecutionArn, err
}

func (c *sfnClient) GetExecutionHistory(ctx context.Context, executionARN string) ([]*sfn.HistoryEvent, error) {
	opt, err := c.SDKClient.GetExecutionHistoryWithContext(ctx, &sfn.GetExecutionHistoryInput{
		ExecutionArn: &executionARN,
		ReverseOrder: aws.Bool(true),
	})

	return opt.Events, err
}
