package redis

import "github.com/afex/hystrix-go/hystrix"

type HystrixSetCmd interface {
	Set() string
}

type HystrixGetCmd interface {
	Get() string
	Del() string
}

type HystrixDelCmd interface {
	Del() string
}

type HystrixZAddCmd interface {
	Get() string
}

type HystrixZPopMinCmd interface {
	Get() string
}

type HystrixZRemCmd interface {
	Get() string
}

const DefaultMaxConcurrent = 1000

const (
	BitsTransactionZAdd                  = "bits_transaction_zadd"
	BitsTransactionZRange                = "bits_transaction_zrange"
	BitsTransactionDel                   = "bits_transaction_del"
	BitsDeferredTransactionZAdd          = "bits_deferred_transaction_zadd"
	BitsDeferredTransactionZRangeByScore = "bits_deferred_transaction_zrange_by_score"
	BitsDeferredTransactionZPopMin       = "bits_deferred_transaction_zpopmin"
	BitsDeferredTransactionZRem          = "bits_deferred_transaction_zrem"
)

type bitsTransactionZAddCmd struct{}
type bitsTransactionZPopMinCmd struct{}
type bitsTransactionDelCmd struct{}
type bitsDeferredTransactionZAddCmd struct{}
type bitsDeferredTransactionZPopMin struct{}
type bitsDeferredTransactionZRemCmd struct{}

func (bt bitsTransactionZAddCmd) Get() string {
	return BitsTransactionZAdd
}

func (bt bitsTransactionDelCmd) Del() string {
	return BitsTransactionDel
}

func (bdt bitsDeferredTransactionZAddCmd) Get() string {
	return BitsDeferredTransactionZAdd
}

func (bdt bitsDeferredTransactionZPopMin) Get() string {
	return BitsDeferredTransactionZPopMin
}

func (bdt bitsDeferredTransactionZRemCmd) Get() string {
	return BitsDeferredTransactionZRem
}

var BitsTransactionZAddCmd = bitsTransactionZAddCmd{}
var BitsTransactionZPopMinCmd = bitsTransactionZPopMinCmd{}
var BitsTransactionDelCmd = bitsTransactionDelCmd{}
var BitsDeferredTransactionZAddCmd = bitsDeferredTransactionZAddCmd{}
var BitsDeferredTransactionZPopMinCmd = bitsDeferredTransactionZPopMin{}
var BitsDeferredTransactionZRemCmd = bitsDeferredTransactionZRemCmd{}

func init() {
	configureCmd(BitsTransactionZAdd, 500)
	configureCmd(BitsTransactionZRange, 500)
	configureCmd(BitsTransactionDel, 500)
	configureCmd(BitsDeferredTransactionZAdd, 500)
	configureCmd(BitsDeferredTransactionZRangeByScore, 500)
	configureCmd(BitsDeferredTransactionZPopMin, 500)
	configureCmd(BitsDeferredTransactionZRem, 500)
}

func configureCmd(cmd string, timeout int) {
	hystrix.ConfigureCommand(cmd, hystrix.CommandConfig{
		Timeout:               timeout, // in milliseconds
		MaxConcurrentRequests: DefaultMaxConcurrent,
		ErrorPercentThreshold: hystrix.DefaultErrorPercentThreshold,
	})
}
