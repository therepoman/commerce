package redis

import (
	"encoding/json"
	"math"
	"strconv"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachter/config"
	"code.justin.tv/whpatr/radix"
	"github.com/afex/hystrix-go/hystrix"
)

const (
	// after 13th retry, it will have slept about 15 seconds in total
	retry_number = 13
)

type RedisClient interface {
	Set(cmd HystrixSetCmd, key string, value interface{}, ttlSeconds int32) error
	Get(cmd HystrixGetCmd, key string, value interface{}) error
	Del(cmd HystrixDelCmd, key string) error
	ZAdd(cmd HystrixZAddCmd, key string, score int64, data interface{}) (int, error)
	ZPopMin(key string) ([]string, error)
	ZRange(key string, startIndex, endIndex int64) ([]string, error)
	ZRangeByScore(hystrixCmd, zRangeKey string, min, max int64) ([]string, error)
	ZRem(cmd HystrixZRemCmd, key string, member string) error
}

type redisClient struct {
	radix.Client
}

func NewRedisClient(cfg *config.Config) (RedisClient, error) {
	if !cfg.UseClusteredRedis {
		pool, err := radix.NewPool("tcp", cfg.RedisEndpoint, cfg.RedisConnectionPerNode)
		if err != nil {
			return nil, err
		}

		return &redisClient{
			Client: pool,
		}, nil
	}

	poolFunc := func(network, addr string) (radix.Client, error) {
		return radix.NewPool(network, addr, cfg.RedisConnectionPerNode)
	}

	client, err := radix.NewCluster([]string{cfg.RedisEndpoint}, radix.ClusterPoolFunc(poolFunc))
	if err != nil {
		return nil, err
	}

	return &redisClient{
		Client: client,
	}, nil
}

func (c *redisClient) Set(cmd HystrixSetCmd, key string, value interface{}, ttlSeconds int32) error {
	valueJson, err := json.Marshal(value)
	if err != nil {
		return err
	}

	ttlStr := strconv.FormatInt(int64(ttlSeconds), 10)

	return hystrix.Do(cmd.Set(), func() error {
		return c.Client.Do(radix.Cmd(nil, "SETEX", key, ttlStr, string(valueJson)))
	}, nil)
}

func (c *redisClient) Get(cmd HystrixGetCmd, key string, value interface{}) error {
	var cachedValue string

	err := hystrix.Do(cmd.Get(), func() error {
		return c.Client.Do(radix.Cmd(&cachedValue, "GET", key))
	}, nil)
	if err != nil {
		return err
	}

	if cachedValue == "" {
		return nil
	}

	err = json.Unmarshal([]byte(cachedValue), &value)
	if err != nil {
		err = c.Del(cmd, key)
		if err != nil {
			logrus.WithError(err).WithField("key", key).Error("failed to delete key that we couldn't unmarshal")
		}
	}

	return err
}

func (c *redisClient) Del(cmd HystrixDelCmd, key string) error {
	return hystrix.Do(cmd.Del(), func() error {
		return c.Client.Do(radix.Cmd(nil, "DEL", key))
	}, nil)
}

func (c *redisClient) ZAdd(cmd HystrixZAddCmd, key string, score int64, data interface{}) (int, error) {
	scoreStr := strconv.FormatInt(score, 10)
	dataJson, err := json.Marshal(data)
	if err != nil {
		return 0, err
	}

	var numOfRecordAdded int
	retry := true
	for i := 0; retry; i++ {
		err = hystrix.Do(cmd.Get(), func() error {
			return c.Client.Do(radix.Cmd(&numOfRecordAdded, "ZADD", key, scoreStr, string(dataJson)))
		}, nil)
		if err == nil {
			return numOfRecordAdded, nil
		}
		retry = expBackOff(retry_number, i)
	}
	return 0, err
}

func (c *redisClient) ZPopMin(key string) ([]string, error) {
	var err error

	var poppedValues []string
	retry := true
	for i := 0; retry; i++ {
		// We specifically don't want to timeout due to the nature of ZPopMin
		err = c.Client.Do(radix.Cmd(&poppedValues, "ZPOPMIN", key, "1"))
		if err == nil {
			return poppedValues, nil
		}

		retry = expBackOff(retry_number, i)
	}
	return poppedValues, err
}

func (c *redisClient) ZRange(key string, startIndex, endIndex int64) ([]string, error) {
	startStr := strconv.FormatInt(startIndex, 10)
	endStr := strconv.FormatInt(endIndex, 10)

	cachedValues := []string{}
	var err error
	retry := true
	for i := 0; retry; i++ {
		err = hystrix.Do(BitsTransactionZRange, func() error {
			return c.Client.Do(radix.Cmd(&cachedValues, "ZRANGE", key, startStr, endStr))
		}, nil)
		if err == nil {
			break
		}
		retry = expBackOff(retry_number, i)
	}

	return cachedValues, err
}

func (c *redisClient) ZRangeByScore(hystrixCmd, zRangeKey string, min, max int64) ([]string, error) {
	minStr := strconv.FormatInt(min, 10)
	maxStr := strconv.FormatInt(max, 10)

	var results []string
	var err error
	retry := true
	for i := 0; retry; i++ {
		err = hystrix.Do(hystrixCmd, func() error {
			return c.Client.Do(radix.Cmd(&results, "ZRANGEBYSCORE", zRangeKey, minStr, maxStr))
		}, nil)
		if err == nil {
			break
		}
		retry = expBackOff(retry_number, i)
	}

	return results, err
}

func (c *redisClient) ZRem(cmd HystrixZRemCmd, key string, member string) error {
	var err error
	retry := true
	for i := 0; retry; i++ {
		err = hystrix.Do(cmd.Get(), func() error {
			return c.Client.Do(radix.Cmd(nil, "ZREM", key, member))
		}, nil)
		if err == nil {
			break
		}
		retry = expBackOff(retry_number, i)
	}
	return err
}

// if returns true, we retry
func expBackOff(maxRetries, retryIndex int) bool {
	if retryIndex >= maxRetries-1 {
		return false
	}
	sleepForSeconds := math.Pow(2, float64(retryIndex))
	time.Sleep(time.Duration(sleepForSeconds) * time.Millisecond)
	return true
}
