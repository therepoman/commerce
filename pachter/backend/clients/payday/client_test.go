package payday

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/pachter/backend/rds/models"
	payday_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/pachter/clients/payday"
	payday_rpc_mock "code.justin.tv/commerce/pachter/mocks/code.justin.tv/commerce/payday/rpc/payday"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestClient_GetTransactionsByID(t *testing.T) {
	Convey("given a client", t, func() {
		payday := new(payday_rpc_mock.Payday)
		converter := new(payday_mock.Converter)

		client := &client{
			Payday:    payday,
			Converter: converter,
		}

		ids := []string{"1", "2", "3"}
		paydayTransactions := []*paydayrpc.Transaction{
			{
				TransactionId: "1",
			},
			{
				TransactionId: "2",
			},
			{
				TransactionId: "3",
			},
		}

		paydayReq := &paydayrpc.GetTransactionsByIDReq{
			TransactionIds: ids,
		}

		paydayResp := &paydayrpc.GetTransactionsByIDResp{
			Transactions: paydayTransactions,
		}

		convertedTransactions := []*models.BitsTransaction{
			{
				TransactionID: "1",
			},
			{
				TransactionID: "2",
			},
			{
				TransactionID: "3",
			},
		}

		ctx := context.Background()

		Convey("retrieves transactions from payday and convert", func() {
			payday.On("GetTransactionsByID", ctx, paydayReq).Return(paydayResp, nil)
			converter.On("ConvertTransactions", paydayTransactions).Return(convertedTransactions, nil, nil)

			transactions, missingTransactionIDs, err := client.GetTransactionsByID(ctx, ids)
			So(err, ShouldBeNil)
			So(missingTransactionIDs, ShouldHaveLength, 0)
			So(transactions, ShouldResemble, convertedTransactions)
		})

		Convey("returns transaction IDs that are not found", func() {
			paydayResp.Transactions = nil
			payday.On("GetTransactionsByID", ctx, paydayReq).Return(paydayResp, nil)
			converter.On("ConvertTransactions", mock.Anything).Return(convertedTransactions, nil, nil)

			foundTransactions, missingTransactionIDs, err := client.GetTransactionsByID(ctx, ids)
			So(err, ShouldBeNil)
			So(missingTransactionIDs, ShouldHaveLength, len(paydayReq.TransactionIds))
			So(foundTransactions, ShouldResemble, convertedTransactions)
		})

		Convey("returns an error if", func() {
			Convey("payday fails", func() {
				payday.On("GetTransactionsByID", ctx, paydayReq).Return(nil, errors.New("no pay"))

				_, _, err := client.GetTransactionsByID(ctx, ids)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
