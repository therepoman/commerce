package payday

import (
	"context"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachter/backend/rds/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
)

type PaydayClient interface {
	GetTransactionsByID(ctx context.Context, ids []string) (transactions []*models.BitsTransaction, missingTransactionIDs []string, err error)
}

type client struct {
	Payday    paydayrpc.Payday `inject:""`
	Converter Converter        `inject:""`
}

func NewPaydayClient() PaydayClient {
	return &client{}
}

func (c *client) GetTransactionsByID(ctx context.Context, ids []string) ([]*models.BitsTransaction, []string, error) {
	if len(ids) == 0 {
		return nil, nil, nil
	}

	idsMap := make(map[string]bool)
	for _, id := range ids {
		idsMap[id] = true
	}

	resp, err := c.Payday.GetTransactionsByID(ctx, &paydayrpc.GetTransactionsByIDReq{
		TransactionIds: ids,
	})
	if err != nil {
		return nil, nil, err
	}

	for _, tx := range resp.Transactions {
		delete(idsMap, tx.TransactionId)
	}

	missingTransactionIDs := make([]string, 0, len(idsMap))
	for id := range idsMap {
		missingTransactionIDs = append(missingTransactionIDs, id)
	}

	transactions, err := c.Converter.ConvertTransactions(resp.Transactions)

	if err != nil {
		logrus.WithError(err).Error("failed to convert transaction")
		return nil, nil, err
	}
	return transactions, missingTransactionIDs, nil
}
