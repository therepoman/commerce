package payday

import (
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	"code.justin.tv/commerce/pachter/backend/rds/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/gofrs/uuid"
	"github.com/golang/protobuf/ptypes/timestamp"
	. "github.com/smartystreets/goconvey/convey"
)

func TestConverter_ConvertTransaction(t *testing.T) {
	Convey("given a converter", t, func() {
		converter := &converter{}

		Convey("fails to convert a transaction with bad purchase price", func() {
			id := uuid.Must(uuid.NewV4()).String()
			paydayTransaction := getTestTransaction(id, models.TransactionType_UseBitsOnExtension)
			paydayTransaction.PurchasePrice = "foobar"

			transaction, err := converter.ConvertTransaction(paydayTransaction)
			So(err, ShouldNotBeNil)
			So(transaction, ShouldBeNil)
		})

		Convey("Converts a Bits on Extension transaction", func() {
			id := uuid.Must(uuid.NewV4()).String()
			paydayTransaction := getTestTransaction(id, models.TransactionType_UseBitsOnExtension)

			transaction, err := converter.ConvertTransaction(paydayTransaction)
			So(err, ShouldBeNil)
			So(transaction, ShouldNotBeNil)
			So(transaction.Datetime, ShouldEqual, time.Unix(200, 1))
		})

		Convey("converts a payday transaction", func() {
			id := uuid.Must(uuid.NewV4()).String()
			paydayTransaction := getTestTransaction(id, models.TransactionType_AddBits)

			transaction, err := converter.ConvertTransaction(paydayTransaction)
			So(err, ShouldBeNil)
			So(transaction, ShouldNotBeNil)
			So(transaction.TransactionID, ShouldEqual, paydayTransaction.TransactionId)
			So(transaction.TransactionType, ShouldEqual, paydayTransaction.TransactionType)
			So(transaction.RequestingTwitchUserID, ShouldEqual, paydayTransaction.UserId)
			So(transaction.ReceivingTwitchUserID, ShouldEqual, paydayTransaction.ChannelId)
			So(transaction.Datetime, ShouldEqual, time.Unix(100, 1))
			So(transaction.Quantity, ShouldEqual, paydayTransaction.NumberOfBits)
			So(transaction.CheermoteUsages, ShouldHaveLength, 2)
			So(transaction.CheermoteUsages, ShouldContain, &models.CheermoteUsage{
				TransactionID: transaction.TransactionID,
				Cheermote:     "cheer",
				Datetime:      transaction.Datetime,
				Quantity:      100,
			})
			So(transaction.CheermoteUsages, ShouldContain, &models.CheermoteUsage{
				TransactionID: transaction.TransactionID,
				Cheermote:     "what",
				Datetime:      transaction.Datetime,
				Quantity:      66,
			})
			So(*transaction.BitsType, ShouldEqual, paydayTransaction.BitsType)
			So(*transaction.Message, ShouldEqual, paydayTransaction.RawMessage)
			So(*transaction.Platform, ShouldEqual, paydayTransaction.Platform)
			So(*transaction.ProductID, ShouldEqual, paydayTransaction.ProductId)
			So(*transaction.ExtensionClientID, ShouldEqual, paydayTransaction.ExtensionClientId)
			So(transaction.PurchasePriceUSC, ShouldEqual, 12345)
		})
	})
}

func TestConverter_ConvertTransactionWithRecipients(t *testing.T) {
	Convey("given a converter", t, func() {
		converter := &converter{}

		Convey("converts a payday transaction with multiple recipients", func() {
			id := uuid.Must(uuid.NewV4()).String()
			paydayTransaction := &paydayrpc.Transaction{
				TransactionId:   id,
				TransactionType: "some type",
				UserId:          random.String(10),
				ChannelId:       random.String(10),
				LastUpdated: &timestamp.Timestamp{
					Seconds: 200,
					Nanos:   1,
				},
				TimeOfEvent: &timestamp.Timestamp{
					Seconds: 100,
					Nanos:   1,
				},
				NumberOfBits: 123,
				Recipients: []*paydayrpc.Recipient{
					{
						Id:              "recipient-1",
						Amount:          100,
						TransactionType: paydayrpc.TransactionType_GiveBitsToBroadcaster,
					},
					{
						Id:              "recipient-2",
						Amount:          20,
						TransactionType: paydayrpc.TransactionType_GiveBitsToBroadcaster,
					},
					{
						Id:              "recipient-3",
						Amount:          3,
						TransactionType: paydayrpc.TransactionType_UseBitsOnExtension,
					},
				},
				PurchasePrice: "123.45",
				PayoutIgnored: true,
			}

			transactions, err := converter.ConvertTransactionWithRecipients(paydayTransaction)
			So(err, ShouldBeNil)
			So(transactions, ShouldHaveLength, 1)
		})
	})
}

func getTestTransaction(id string, transactionType models.TransactionType) *paydayrpc.Transaction {
	return &paydayrpc.Transaction{
		TransactionId:   id,
		TransactionType: string(transactionType),
		UserId:          random.String(10),
		ChannelId:       random.String(10),
		LastUpdated: &timestamp.Timestamp{
			Seconds: 200,
			Nanos:   1,
		},
		TimeOfEvent: &timestamp.Timestamp{
			Seconds: 100,
			Nanos:   1,
		},
		NumberOfBits: 123,
		EmoteTotals: map[string]int64{
			"cheer": int64(100),
			"what":  int64(66),
		},
		BitsType:          "some type",
		RawMessage:        "cheer123",
		Platform:          "some platform",
		ProductId:         "some product ID",
		ExtensionClientId: "some extension client ID",
		PurchasePrice:     "123.45",
	}
}
