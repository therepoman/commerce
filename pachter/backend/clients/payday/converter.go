package payday

import (
	"errors"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/gogogadget/strings"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachter/backend/rds/models"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
)

/*
const (
	recipientTransactionIDFormat = "%s##%s"
)
*/

type Converter interface {
	ConvertTransaction(input *paydayrpc.Transaction) (*models.BitsTransaction, error)
	ConvertTransactionWithRecipients(input *paydayrpc.Transaction) ([]*models.BitsTransaction, error)
	ConvertTransactions(inputs []*paydayrpc.Transaction) ([]*models.BitsTransaction, error)
}

type converter struct {
}

func NewConverter() Converter {
	return &converter{}
}

func (c *converter) ConvertTransaction(input *paydayrpc.Transaction) (*models.BitsTransaction, error) {
	if input == nil {
		return nil, nil
	}

	// Bits on Extension are two-phase, so need to use the last updated time
	// Everything else should be using TimeOfEvent
	var timeStampToUse *timestamp.Timestamp
	if input.TransactionType == string(models.TransactionType_UseBitsOnExtension) {
		timeStampToUse = input.LastUpdated
	} else {
		timeStampToUse = input.TimeOfEvent
	}

	if timeStampToUse == nil {
		logrus.WithField("transaction_id", input.TransactionId).Error("received a transaction with nil timestamp")
		return nil, errors.New("received a transaction with nil timestamp")
	}

	datetime, err := ptypes.Timestamp(timeStampToUse)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"transaction_id": input.TransactionId,
			"timestamp":      timeStampToUse.String(),
		}).Error("failed to parse transaction timestamp")
		return nil, err
	}

	cheermoteUsages := make([]*models.CheermoteUsage, 0, len(input.EmoteTotals))
	for prefix, total := range input.EmoteTotals {
		cheermoteUsages = append(cheermoteUsages, &models.CheermoteUsage{
			TransactionID: input.TransactionId,
			Cheermote:     prefix,
			Datetime:      datetime,
			Quantity:      total,
		})
	}

	bitsAmount := input.NumberOfBits
	if input.NumberOfBits < 0 {
		bitsAmount = -1 * input.NumberOfBits
	}

	var purchasePriceUSC int64
	if strings.NotBlank(input.PurchasePrice) {
		purchasePriceUSC, err = strings.USDPriceStringToCents(input.PurchasePrice)
		if err != nil {
			logrus.WithError(err).WithField("purchase_price", input.PurchasePrice).Error("failed to parse purchase price")
			return nil, err
		}
	}

	return &models.BitsTransaction{
		TransactionID:          input.TransactionId,
		TransactionType:        input.TransactionType,
		RequestingTwitchUserID: input.UserId,
		ReceivingTwitchUserID:  input.ChannelId,
		Datetime:               datetime,
		Quantity:               bitsAmount,
		SponsoredQuantity:      input.NumberOfBitsSponsored,
		CheermoteUsages:        cheermoteUsages,
		BitsType:               pointers.StringP(input.BitsType),
		Message:                pointers.StringP(input.RawMessage),
		Platform:               pointers.StringP(input.Platform),
		ProductID:              pointers.StringP(input.ProductId),
		ExtensionClientID:      pointers.StringP(input.ExtensionClientId),
		PurchasePriceUSC:       purchasePriceUSC,
	}, nil
}

// ConvertTransactionWithRecipients breaks a single Payday transaction with multiple recipients (e.g. a hold) into multiple transaction entries
func (c *converter) ConvertTransactionWithRecipients(input *paydayrpc.Transaction) ([]*models.BitsTransaction, error) {
	if input == nil {
		return nil, nil
	}

	if input.PayoutIgnored {
		transactions := make([]*models.BitsTransaction, 0, 1)
		transaction, err := c.ConvertTransaction(input)
		if err != nil {
			return nil, err
		}

		transactions = append(transactions, transaction)
		return transactions, nil
	} else {
		// This logic has problems with mapping the transactionIDs with the fmt adding the RecipientIDs in the
		// Audit DAO record which stores the tx's by the original TXID. It's being left here as a loose record of what is needed in the future.

		/*
			datetime, err := ptypes.Timestamp(input.LastUpdated)
			if err != nil {
				logrus.WithError(err).WithField("transaction_id", input.TransactionId).Warn("failed to parse last updated timestamp, using the current timestamp instead")
				datetime = time.Now()
			}

			var purchasePriceUSC int64
			if strings.NotBlank(input.PurchasePrice) {
				purchasePriceUSC, err = strings.USDPriceStringToCents(input.PurchasePrice)
				if err != nil {
					logrus.WithError(err).WithField("purchase_price", input.PurchasePrice).Error("failed to parse purchase price")
				}
			}

			transactions := make([]*models.BitsTransaction, 0, len(input.Recipients))
			for _, recipient := range input.Recipients {
				transactions = append(transactions, &models.BitsTransaction{
					TransactionID:          fmt.Sprintf(recipientTransactionIDFormat, input.TransactionId, recipient.Id), // use a unique transaction ID for each recipient
					TransactionType:        recipient.TransactionType.String(),
					RequestingTwitchUserID: input.UserId,
					ReceivingTwitchUserID:  recipient.Id,
					Quantity:               recipient.Amount,
					Datetime:               datetime,
					BitsType:               pointers.StringP(input.BitsType),
					Message:                pointers.StringP(input.RawMessage),
					Platform:               pointers.StringP(input.Platform),
					ProductID:              pointers.StringP(input.ProductId),
					ExtensionClientID:      pointers.StringP(input.ExtensionClientId),
					PurchasePriceUSC:       purchasePriceUSC,
				})
			}
		*/
		return nil, errors.New("this usecase is not supported yet and has bugs in it")
	}
}

func (c *converter) ConvertTransactions(inputs []*paydayrpc.Transaction) ([]*models.BitsTransaction, error) {
	if len(inputs) == 0 {
		return nil, nil
	}

	transactions := make([]*models.BitsTransaction, 0, len(inputs))
	for _, input := range inputs {
		if shouldSkipTransaction(input) {
			continue
		}

		if len(input.Recipients) > 0 {
			recipients, err := c.ConvertTransactionWithRecipients(input)
			if err != nil {
				return nil, err
			}

			transactions = append(transactions, recipients...)
		} else {
			transaction, err := c.ConvertTransaction(input)
			if err != nil {
				return nil, err
			}
			transactions = append(transactions, transaction)
		}
	}

	return transactions, nil
}

func shouldSkipTransaction(input *paydayrpc.Transaction) bool {
	return input == nil
}
