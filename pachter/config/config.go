package config

import (
	"fmt"
	"io/ioutil"
	"os"

	log "code.justin.tv/commerce/logrus"
	"github.com/go-yaml/yaml"
)

const (
	EnvironmentVariable  = "ENVIRONMENT"
	LocalConfigFilePath  = "/src/code.justin.tv/commerce/pachter/config/data/%s.yaml"
	GlobalConfigFilePath = "/etc/pachter/config/%s.yaml"

	UnitTest  = Environment("unit-test")
	SmokeTest = Environment("smoke-test")
	Local     = Environment("local")
	Staging   = Environment("staging")
	Prod      = Environment("prod")
	Default   = Local
)

type Config struct {
	EnvironmentName  string `yaml:"environment-name"`
	AWSRegion        string `yaml:"aws-region"`
	DisableSQSWorker bool   `yaml:"disable-sqs-worker"`
	S2SEnabled       bool   `yaml:"s2s-enabled"`
	S2SServiceName   string `yaml:"s2s-service-name"`

	// SQS worker configs
	BitsTransactionDeferrerWorkerV2Config *SQSConfig `yaml:"bits-transaction-deferrer-worker-v2"`
	BitsSpendWorkerV2Config               *SQSConfig `yaml:"bits-spend-worker-v2"`
	BitsTransactionWorkerV2Config         *SQSConfig `yaml:"bits-transaction-worker-v2"`
	CheermoteUsageWorkerV2Config          *SQSConfig `yaml:"cheermote-usage-worker-v2"`
	BitsTransactionAuditWorkerV2Config    *SQSConfig `yaml:"bits-transaction-audit-worker-v2"`
	BitsMonthlyReportWorkerConfig         *SQSConfig `yaml:"bits-monthly-report-worker"`

	RDSAWSSecretStoreARN              string `yaml:"rds-secret-store-arn"`
	RDSEndpointOverride               string `yaml:"rds-endpoint-override"`
	RDSPortOverride                   int    `yaml:"rds-port-override"`
	PaydayEndpoint                    string `yaml:"payday-endpoint"`
	PetoziEndpoint                    string `yaml:"petozi-endpoint"`
	PachterEndpoint                   string `yaml:"pachter-endpoint"`
	PachinkoEndpoint                  string `yaml:"pachinko-endpoint"`
	UseClusteredRedis                 bool   `yaml:"use-clustered-redis"`
	RedisEndpoint                     string `yaml:"redis-endpoint"`
	RedisConnectionPerNode            int    `yaml:"redis-connection-per-pool"`
	BitsMonthlyReportBucket           string `yaml:"bits-monthly-report-bucket"`
	BitsEmoteUsageMonthlyReportBucket string `yaml:"bits-emote-usage-monthly-report-bucket"`
	SpendOrderResetterSfnARN          string `yaml:"spend-order-resetter-sfn-arn"`
}

type SQSConfig struct {
	Name       string `yaml:"name"`
	NumWorkers int    `yaml:"num-workers"`
}

type Environment string

var Environments = map[Environment]interface{}{UnitTest: nil, SmokeTest: nil, Local: nil, Staging: nil, Prod: nil}

func LoadConfig(env Environment) (*Config, error) {
	if !isValidEnvironment(env) {
		log.Errorf("Invalid environment: %s. Falling back to local", env)
		env = Local
	}

	baseFileName := string(env)
	filePath, err := getConfigFilePath(baseFileName)
	if err != nil {
		return nil, err
	}

	return loadConfig(filePath)
}

func getConfigFilePath(baseFilename string) (string, error) {
	localFname := os.Getenv("GOPATH") + fmt.Sprintf(LocalConfigFilePath, baseFilename)
	if _, err := os.Stat(localFname); !os.IsNotExist(err) {
		return localFname, nil
	}

	globalFname := fmt.Sprintf(GlobalConfigFilePath, baseFilename)
	if _, err := os.Stat(globalFname); os.IsNotExist(err) {
		return "", err
	}

	return globalFname, nil
}

func loadConfig(path string) (*Config, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer func() {
		err := file.Close()
		if err != nil {
			log.WithError(err).Error("Error closing config file")
		}
	}()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var cfg Config
	err = yaml.Unmarshal(fileBytes, &cfg)
	if err != nil {
		return nil, err
	}

	return &cfg, nil
}

func GetOrDefaultEnvironment(env string) Environment {
	if !isValidEnvironment(Environment(env)) {
		log.Errorf("Invalid environment: %s", env)
		log.Infof("Falling back to default environment: %s", string(Default))
		return Default
	}

	return Environment(env)
}

func isValidEnvironment(env Environment) bool {
	_, ok := Environments[env]
	return ok
}
