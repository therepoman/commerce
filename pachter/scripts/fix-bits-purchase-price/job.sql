CREATE TABLE my_corrections_table (transaction_id varchar(128), purchase_price_currency varchar(32), purchase_price_ varchar(32), gross_amount_usd integer);
        
SELECT aws_s3.table_import_from_s3('my_corrections_table', '', '(format csv)',aws_commons.create_s3_uri('pachter-staging-temp-data', 'report_updated.csv', 'us-west-2'), aws_commons.create_aws_credentials('', '', ''));
        
UPDATE bits_transactions SET purchase_price_usc = mct.gross_amount_usd, update_reason = 'Fixed purchase price', updated_at = CURRENT_TIMESTAMP  FROM my_corrections_table mct WHERE bits_transactions.transaction_id = mct.transaction_id;


