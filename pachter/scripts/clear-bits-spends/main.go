/*
 * Script to clear all bits spends data for a given list of users, deletes data from redis and RDS
 *
 * Usage: ./main -i <input_file> -e <environment>
 * - input_file should be a file with one or more TUIDs, one ID on each line
 * - environment should be either "production" or "staging", defaults to "staging"
 * - you should run this in Pachter jumbbox. In order to do that,
 * - 1. Build the script with `env GOOS=linux GOARCH=amd64 GOARM=7 go build main.go`
 * - 2. Send the excutable to jumpbox with `TC=twitch-pachter-prod scp main hiranoo@jumpbox:/home/hiranoo`
 * - 3. Similarily send the input file to jumbbox
 * - 4. SSH into the jumbbox with `TC=twitch-pachter-prod ssh jumpbox`
 * - 5. Set the Pachter's aws credentials before running the script
 */

package main

import (
	"bufio"
	"context"
	"fmt"
	"log"
	"os"
	"sync"

	"code.justin.tv/commerce/pachter/backend/clients/rds"
	"code.justin.tv/commerce/pachter/backend/clients/redis"
	"code.justin.tv/commerce/pachter/backend/rds/bits_spend"
	"code.justin.tv/commerce/pachter/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/codegangsta/cli"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"golang.org/x/time/rate"
)

var (
	env       string
	inputFile string

	redisClient   redis.RedisClient
	bitsSpendsDAO bits_spend.DAO

	redisEndpoint     string
	rdsSecretStoreARN string
)

const (
	numOfWorkers = 4
	workerTPS    = 50

	redisEndpointStaging     = "pachter-redis.mvep6k.clustercfg.usw2.cache.amazonaws.com:6379"
	rdsSecretStoreARNStaging = "arn:aws:secretsmanager:us-west-2:721938063588:secret:pachter-rds-credentials-Kh5FrK"

	redisEndpointProd     = "pachter-redis.sfonzr.clustercfg.usw2.cache.amazonaws.com:6379"
	rdsSecretStoreARNProd = "arn:aws:secretsmanager:us-west-2:458492755823:secret:pachter-rds-credentials-2AUg2L"

	redisKeyFormat = "user-bits-transactions-%s"
)

func main() {
	logrus.SetOutput(os.Stdout)

	script := cli.NewApp()
	script.Name = "Clear bits spends data for user(s)"
	script.Usage = "Script"

	script.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "inputFile, i",
			Usage:       "the input file with user IDs to clear data for",
			EnvVar:      "INPUT_FILE",
			Destination: &inputFile,
		},
		cli.StringFlag{
			Name:        "env, e",
			Usage:       "the target environment the run the script",
			EnvVar:      "BITS_SPENDS_SCRIPT_ENVIRONMENT",
			Destination: &env,
		},
	}

	// start the script
	script.Action = run
	logrus.Info("Starting to clear bits spends data for users...")

	err := script.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Panic("Failed to execute script")
	}
}

func run(c *cli.Context) {
	// configure environment
	if env == "production" {
		redisEndpoint = redisEndpointProd
		rdsSecretStoreARN = rdsSecretStoreARNProd
	} else {
		redisEndpoint = redisEndpointStaging
		rdsSecretStoreARN = rdsSecretStoreARNStaging
	}

	logrus.Infof("Environment: %s", env)
	logrus.Infof("Redis endpoint: %s", redisEndpoint)
	logrus.Infof("RDS secret store ARN: %s", rdsSecretStoreARN)

	// configure clients
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2"),
	})
	if err != nil {
		logrus.WithError(err).Panic("failed to create AWS session")
	}

	cfg := &config.Config{
		RedisEndpoint:          redisEndpoint,
		RedisConnectionPerNode: 50,
		RDSAWSSecretStoreARN:   rdsSecretStoreARN,
	}

	redisClient, err = redis.NewRedisClient(cfg)
	if err != nil {
		logrus.WithError(err).Panic("Failed to initialize Redis client")
	}

	rdsClient, err := rds.NewRDSClient(cfg, sess)
	if err != nil {
		logrus.WithError(err).Panic("Failed to initialize RDS client")
	}

	bitsSpendsDAO = bits_spend.NewDAOWithClient(rdsClient)

	// read input file
	file, err := os.Open(inputFile)
	if err != nil {
		log.Fatal("Cannot open input file")
	}
	defer file.Close()

	wg := &sync.WaitGroup{}
	workChan := make(chan string, numOfWorkers)
	rateLimiter := rate.NewLimiter(rate.Limit(workerTPS), workerTPS)

	// spawn worker threads
	startWorkers(workChan, wg, rateLimiter)

	// distribute jobs
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		userID := scanner.Text()
		workChan <- userID
	}
	close(workChan)

	// wait for completion
	wg.Wait()
}

func startWorkers(workChan chan string, wg *sync.WaitGroup, rateLimiter *rate.Limiter) {
	wg.Add(numOfWorkers)
	for i := 0; i < numOfWorkers; i++ {
		go func() {
			defer wg.Done()
			for {
				userID, ok := <-workChan
				if !ok {
					return
				}

				_ = rateLimiter.Wait(context.Background())
				err := clearData(userID)
				if err != nil {
					logrus.WithError(err).Errorf("Failed to clear bits spends data for user %s", userID)
				} else {
					logrus.Infof("Cleared data for user %s", userID)
				}
			}
		}()
	}
}

func clearData(userID string) error {
	redisKey := fmt.Sprintf(redisKeyFormat, userID)
	// clear user data from Redis
	err := redisClient.Del(redis.BitsTransactionDelCmd, redisKey)
	if err != nil {
		return errors.Wrap(err, "failed to clear Redis data")
	}

	// clear user data from RDS
	err = bitsSpendsDAO.DeleteAllByRequestingUserID(userID)
	if err != nil {
		return errors.Wrap(err, "failed to clear RDS data")
	}

	return nil
}
