/*
	The input file should be a CSV file with a column for transaction ids
		amzn1.twitch.payments.order.321fa29c-24e4-4d08-a34b-6e4ee0e942db
		amzn1.twitch.payments.order.5ec8ac2f-7fed-4cd8-a19d-a0491a4182e9
*/
package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachter/backend/clients/rds"
	"code.justin.tv/commerce/pachter/backend/rds/bits_transaction"
	"code.justin.tv/commerce/pachter/backend/rds/models"
	"code.justin.tv/commerce/pachter/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/codegangsta/cli"
)

var (
	env               string
	inputFile         string
	rdsSecretStoreARN string
	dryRun            bool
)

const (
	rdsSecretStoreARNStaging = "arn:aws:secretsmanager:us-west-2:721938063588:secret:pachter-rds-credentials-Kh5FrK"
	rdsSecretStoreARNProd    = "arn:aws:secretsmanager:us-west-2:458492755823:secret:pachter-rds-credentials-2AUg2L"
	adyenSuffix              = "_ADYEN"
	unknownSuffix            = "_UNKNOWN"
	amazonSuffix             = "_AMAZON"
	amazonPlatform           = "AMAZON"
)

func main() {
	logrus.SetOutput(os.Stdout)

	script := cli.NewApp()
	script.Name = "Backfill fix Bits types"
	script.Usage = "go run ./main.go -e prod[staging] -i inputFile"

	script.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "inputFile, i",
			Usage:       "the input file with purchase prices to backfill",
			Destination: &inputFile,
		},
		cli.StringFlag{
			Name:        "env, e",
			Usage:       "the target environment (either 'staging' or 'prod')",
			Destination: &env,
		},
		cli.BoolFlag{
			Name:        "dryrun, r",
			Usage:       "Whether to run a dryrun or not.",
			Destination: &dryRun,
		},
	}

	script.Action = run
	logrus.Info("Starting to backfill bits types")

	err := script.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Panic("Failed to execute script")
	}
}

func run(c *cli.Context) {
	logrus.Info(fmt.Sprintf("env (%s)", env))
	if env == "prod" {
		rdsSecretStoreARN = rdsSecretStoreARNProd
	} else if env == "staging" {
		rdsSecretStoreARN = rdsSecretStoreARNStaging
	} else {
		panic("Invalid env")
	}

	logrus.Infof("Environment: %s", env)
	logrus.Infof("RDS secret store ARN: %s", rdsSecretStoreARN)

	// configure clients
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2"),
	})
	if err != nil {
		logrus.WithError(err).Panic("failed to create AWS session")
	}

	cfg := &config.Config{
		RDSAWSSecretStoreARN: rdsSecretStoreARN,
	}

	logrus.Infof("Creating RDS client")
	rdsClient, err := rds.NewRDSClient(cfg, sess)
	if err != nil {
		logrus.WithError(err).Panic("Failed to initialize RDS client")
	}

	transactionRDS := bits_transaction.NewDAOWithClient(rdsClient)

	// read input file
	file, err := os.Open(inputFile)
	if err != nil {
		log.Fatal("Cannot open input file")
	}
	defer file.Close()

	logrus.Infof("Starting to read transactions")
	transactionsToUpdate := make([]*models.BitsTransaction, 0)
	csvReader := csv.NewReader(bufio.NewReader(file))
	missingTransactions := make([]string, 0)
	skippedTransactions := make([]string, 0)
	for {
		row, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			logrus.WithError(err).Error("Failed to read a row")
			continue
		}

		if len(row) < 1 {
			logrus.Error("Row doesn't have enough columns")
			continue
		}

		transactionID := row[0]
		transaction, err := transactionRDS.Get(transactionID)
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"transactionID": transactionID,
			}).WithError(err).Error("Failed to get transactions")
		}

		if transaction == nil {
			logrus.WithFields(logrus.Fields{
				"transactionID": transactionID,
			}).Warn("Transaction does not exist, skipping")
			missingTransactions = append(missingTransactions, transactionID)
			continue
		}

		if !strings.HasSuffix(*transaction.BitsType, adyenSuffix) && *transaction.Platform == amazonPlatform {
			if strings.HasSuffix(*transaction.BitsType, unknownSuffix) {
				logrus.WithFields(logrus.Fields{
					"transactionID": transactionID,
				}).Info(fmt.Sprintf("Found (%s) Bits Type, trimming", unknownSuffix))
				*transaction.BitsType = strings.TrimSuffix(*transaction.BitsType, unknownSuffix)
			} else if strings.HasSuffix(*transaction.BitsType, amazonSuffix) {
				logrus.WithFields(logrus.Fields{
					"transactionID": transactionID,
				}).Info(fmt.Sprintf("Found (%s) Bits Type, trimming", amazonSuffix))
				*transaction.BitsType = strings.TrimSuffix(*transaction.BitsType, amazonSuffix)
			}

			*transaction.BitsType = *transaction.BitsType + adyenSuffix

			logrus.WithFields(logrus.Fields{
				"transactionID": transactionID,
			}).Info(fmt.Sprintf("New Bits type for transaction %s", *transaction.BitsType))
			transactionsToUpdate = append(transactionsToUpdate, transaction)
		} else {
			logrus.WithFields(logrus.Fields{
				"transactionID": transactionID,
			}).Info(fmt.Sprintf("Skipping transaction with Bits Type (%s) and platform (%s)", *transaction.BitsType, *transaction.Platform))
			skippedTransactions = append(skippedTransactions, transactionID)
		}
	}

	logrus.Info("-----------------------------")

	logrus.WithFields(logrus.Fields{
		"missingTransactions": missingTransactions,
	}).Warn(fmt.Sprintf("Could not find (%d) transactions", len(missingTransactions)))

	logrus.WithFields(logrus.Fields{
		"skippedTransactions": skippedTransactions,
	}).Warn(fmt.Sprintf("Skipping (%d) transactions that already have correct Bits Type, or are not Amazon Platform", len(skippedTransactions)))

	logrus.Info("-----------------------------")

	if dryRun {
		logrus.WithFields(logrus.Fields{
			"transactionsToUpdate": len(transactionsToUpdate),
		}).Info("Skipping updating transactions due to dry run")
	} else {
		logrus.WithFields(logrus.Fields{
			"transactionsToUpdate": len(transactionsToUpdate),
		}).Info("Processing transactions")
		err := transactionRDS.BatchPut(transactionsToUpdate)
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"transactionsToUpdate": transactionsToUpdate,
			}).WithError(err).Error("Failed to batch put transactions")
			panic(err)
		}
	}

	logrus.WithFields(logrus.Fields{
		"TransactionsUpdated": len(transactionsToUpdate),
	}).Info("Finished!")
}
