package main

import (
	"bufio"
	"context"
	"encoding/csv"
	"io"
	"os"
	"sync"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachter/backend/dynamo/worker_audit"
	"code.justin.tv/commerce/pachter/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/codegangsta/cli"
	"golang.org/x/time/rate"
)

const (
	aws_region = "us-west-2"
)

var inputFile string
var auditDAO worker_audit.DAO
var resetField string
var env string

func main() {
	script := cli.NewApp()
	script.Name = "Pachter Audit table resetter"
	script.Usage = "Resetter"

	script.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "inputFile, i",
			Usage:       "the input file with transaction ids on each line",
			EnvVar:      "INPUT_FILE",
			Destination: &inputFile,
		},
		cli.StringFlag{
			Name:        "env, e",
			Usage:       "the environment",
			EnvVar:      "DYNAMO_ENVIRONMENT",
			Value:       "staging",
			Destination: &env,
		},
		cli.StringFlag{
			Name:        "reset, r",
			Usage:       "the name of the field to be reset",
			EnvVar:      "RESET_FIELD",
			Destination: &resetField,
		},
	}

	script.Action = run
	err := script.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Panic("Failed to run this script!")
	}
}

func run(c *cli.Context) {
	ctx := context.Background()

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(aws_region),
	})
	if err != nil {
		logrus.WithError(err).Panic("Failed to initiate aws session")
	}

	auditDAO = worker_audit.NewDAO(sess, &config.Config{
		EnvironmentName: env,
	})

	logrus.Info("Start reading CSV")
	startTime := time.Now()
	csvFile, _ := os.Open(inputFile)
	csvReader := csv.NewReader(bufio.NewReader(csvFile))
	defer csvFile.Close()
	logrus.Infof("CSV read. Took %.2f seconds", time.Since(startTime).Seconds())

	outputFile, err := os.Create("out.csv")
	if err != nil {
		logrus.WithError(err).Panic("could not create output file")
	}
	csvWriter := csv.NewWriter(outputFile)
	defer outputFile.Close()
	defer csvWriter.Flush()

	wg := &sync.WaitGroup{}
	counter := 0
	rateLimiter := rate.NewLimiter(rate.Limit(200), 200)
	for {
		_ = rateLimiter.Wait(context.Background())
		csvRow, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if len(csvRow) < 1 {
			logrus.Warn("found a row with no transaction id")
			continue
		}

		transactionID := csvRow[0]
		wg.Add(1)
		go func() {
			err := updateRecord(ctx, transactionID)
			if err != nil {
				logrus.WithError(err).Errorf("failed on %s", transactionID)
				err := csvWriter.Write([]string{transactionID})
				if err != nil {
					logrus.WithError(err).WithField("transactionID", transactionID).Error("failed to write transaction ID to output")
				}
			}

			wg.Done()
		}()

		counter++
		if counter%10000 == 0 {
			logrus.Infof("%d entries processed", counter)
		}
	}
	wg.Wait()

	logrus.Infof("Completed. %d records processed", counter)
}

func updateRecord(ctx context.Context, transactionID string) error {
	err := auditDAO.Remove(ctx, transactionID, []string{resetField})
	if err != nil {
		return err
	}

	return nil
}
