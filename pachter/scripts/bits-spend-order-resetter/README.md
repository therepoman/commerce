# Bits Spend Order Resetter Script
This is a script to send transaction ids to `bits-spend-staging-dlq.fifo` queue.
Why? Because you want to reset and re-calculate the Bits spend order for the user
to whom the transaction belongs.

Consider this case: you want to reset and re-calcuate the Bits spend order of the
user `ohkichan` (user id: 205613736). Let's say you want to do it in staging.
In that case all you have to do is to send _any_ transaction id (in staging) of `ohkichan`, as well as the
user id along with it, with this script
to the spend order DLQ and manually trigger the Lambda function `BitsSpendOrderResetter`.

e.g. input.txt
```
c13a2f74-e5ee-4e5c-a5f4-d2dcedbb5c9a,205613736
```

e.g. execution
```
~$ AWS_PROFILE=pachter-devo
~$ go run scripts/bits-spend-order-resetter/main.go -i input.txt -e staging
```

Now, go to the Lambda console and select `BitsSpendOrderResetter` function, and click the "Test" button
at the top. When you do that for the first time, you would have to configure the input of the test event,
but the Resetter Lambda doesn't care what the input is, so just make the input an empty json and name the
test event whatever you want (e.g. "Run"):

![Lambda Instruction - configure test event](/assets/configure-test-event.png)

![Lambda Instruction - run](/assets/lambda-instruction.png)

When you click the "Test" button, the Lambda function will then takes care of everything for
you. For more detail of how the function works, refer to the [actual Lambda implementaion](/lambda/bits_spend_order/resetter/READEME.md).

If you have a list of users that you want to reset, and there are
non-trivial number of them for you to manually look up an arbitrary transaction id for each of them,
there is a stored procedure for that:

```postgresql
SELECT get_user_transaction_ids(ARRAY['453710151']);
```
This would return a transaction id for the user:
```
             get_user_transaction_ids
--------------------------------------------------
 (E714E9D3-D889-4232-8FBC-3DD24AE52C30,453710151)
(1 row)
```
that would make it easier to make the input file for this script.
