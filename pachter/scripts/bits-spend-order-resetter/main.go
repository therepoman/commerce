package main

import (
	"bufio"
	"encoding/csv"
	"encoding/json"
	"io"
	"os"

	"code.justin.tv/commerce/logrus"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/codegangsta/cli"
	"github.com/gofrs/uuid"
)

const (
	bits_spend_order_dlq_prod_url    = "https://sqs.us-west-2.amazonaws.com/458492755823/bits-spend-prod-dlq.fifo"
	bits_spend_order_dlq_staging_url = "https://sqs.us-west-2.amazonaws.com/721938063588/bits-spend-staging-dlq.fifo"
)

var inputFile string
var env string
var sqsURL string

type SNSMessage struct {
	MessageID string `json:"MessageID"`
	Message   string `json:"Message"`
}

type BalanceUpdateMessage struct {
	TransactionIDs []string `json:"transaction_ids"`
}

func main() {
	script := cli.NewApp()
	script.Name = "Pachter bits spends resetter script"
	script.Usage = "Resets spend order"

	script.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "inputFile, i",
			Usage:       "the input file with transactions for which users to be re-calculated of their spend order",
			EnvVar:      "INPUT_FILE",
			Destination: &inputFile,
		},
		cli.StringFlag{
			Name:        "environment, e",
			Usage:       "environment in which the reset takes place. Either 'staging' or 'prod' (defaults to staging)",
			Value:       "staging",
			Destination: &env,
		},
	}

	script.Action = run
	err := script.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Error("Run returned an err")
	}
}

func run(c *cli.Context) {
	logrus.SetOutput(os.Stdout)

	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))
	sqsClient := sqs.New(sess)
	sqsURL = bits_spend_order_dlq_staging_url
	if env == "prod" {
		sqsURL = bits_spend_order_dlq_prod_url
	}
	csvFile, _ := os.Open(inputFile)
	csvReader := csv.NewReader(bufio.NewReader(csvFile))
	counter := 0

	logrus.Info("starting the process")
	for {
		row, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			logrus.WithError(err).Errorf("failed to read transactionID at transaction counter: %d", counter)
			return
		}
		if len(row) < 1 {
			logrus.Errorf("transactionID is not present in the input file at transaction counter: %d", counter)
			return
		}

		transactionID := row[0]
		userID := "default"
		if len(row) > 1 {
			userID = row[1]
		}

		_, err = sqsClient.SendMessage(&sqs.SendMessageInput{
			QueueUrl:       &sqsURL,
			MessageGroupId: aws.String("bits-spend-order-" + userID),
			MessageBody:    getSQSBody([]string{transactionID}, counter),
		})
		if err != nil {
			logrus.WithField("transactionID", transactionID).WithError(err).Error("failed to send transaction ID")
		} else {
			counter++
		}
	}

	logrus.Infof("finished! %d records sent.", counter)
}

func getSQSBody(transactionIDs []string, index int) *string {
	balanceUpdate := BalanceUpdateMessage{
		transactionIDs,
	}
	balanceUpdateJson, err := json.Marshal(&balanceUpdate)
	if err != nil {
		logrus.WithError(err).Panicf("failed to marshal transaction at transaction counter: %d", index)
	}

	snsMessage := SNSMessage{
		MessageID: uuid.Must(uuid.NewV4()).String(),
		Message:   string(balanceUpdateJson),
	}
	snsMessageJson, err := json.Marshal(&snsMessage)
	if err != nil {
		logrus.WithError(err).Panicf("failed to marshal sns message at transaction counter: %d", index)
	}

	return aws.String(string(snsMessageJson))
}
