CREATE OR REPLACE FUNCTION get_user_transaction_ids(user_ids TEXT[])
    RETURNS TABLE (
        transaction_id VARCHAR(255),
        user_id        VARCHAR(50)
    )
AS $$
DECLARE
    array_index INT := 1;
    array_size  INT := array_length(user_ids, 1);
BEGIN

CREATE TEMP TABLE user_transaction (
    transaction_id VARCHAR(255),
    user_id        VARCHAR(50)
);

RAISE NOTICE 'array size: %', array_size;

WHILE array_index <= array_size LOOP
    RAISE NOTICE 'array index: %', array_index;
    INSERT INTO
        user_transaction(transaction_id, user_id)
    SELECT
        RTRIM(t.transaction_id), RTRIM(t.requesting_twitch_user_id)
    FROM
        bits_transactions t
    WHERE
        t.requesting_twitch_user_id = user_ids[array_index]
    LIMIT 1;
    array_index := array_index + 1;
END LOOP;

RETURN QUERY SELECT user_transaction.transaction_id, user_transaction.user_id FROM user_transaction ORDER BY user_id;

DROP TABLE user_transaction;

END; $$

LANGUAGE 'plpgsql';
-- SELECT get_user_transaction_ids(ARRAY['453710151']);