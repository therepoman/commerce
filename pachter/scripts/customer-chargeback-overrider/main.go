package main

import (
	"bufio"
	"encoding/csv"
	"io"
	"os"

	"code.justin.tv/commerce/logrus"
	"github.com/codegangsta/cli"
)

var inputFile string
var outputFile string

type csvEntry struct {
	TransactionID          string
	RequestingTwitchUserID string
}

func main() {
	script := cli.NewApp()
	script.Name = "Customer Chargeback Timestamp Overrider"
	script.Usage = "Reads an input CSV and outputs one with CB moved to the end with updated timestamp"

	script.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "inputFile, i",
			Usage:       "the input file with transactions that contain customer chargeback",
			EnvVar:      "INPUT_FILE",
			Destination: &inputFile,
		},
		cli.StringFlag{
			Name:        "outputFile, o",
			Usage:       "the output file with modified customer chargeback timestamps",
			EnvVar:      "OUTPUT_FILE",
			Value:       "out.csv",
			Destination: &outputFile,
		},
	}

	script.Action = run
	err := script.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Error("Run returned an err")
	}
}

func run(c *cli.Context) {
	logrus.SetOutput(os.Stdout)

	csvInputFile, err := os.Open(inputFile)
	if err != nil {
		logrus.WithError(err).Fatalf("Failed to read %s", inputFile)
	}
	defer csvInputFile.Close()

	csvOutputFile, err := os.Create(outputFile)
	if err != nil {
		logrus.WithError(err).Fatalf("Failed to create %s", outputFile)
	}
	defer csvOutputFile.Close()

	csvReader := csv.NewReader(bufio.NewReader(csvInputFile))
	csvWriter := csv.NewWriter(csvOutputFile)
	defer csvWriter.Flush()

	readCounter := 0
	writeCounter := 0
	entriesToAppendAtTheEnd := make([]csvEntry, 0)
	for {
		row, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			logrus.WithError(err).Fatalf("Failed to read csv entry at index: %d", readCounter)
		}
		if len(row) < 3 {
			logrus.Fatalf("There are less than 3 columns at the entry index: %d", readCounter)
		}

		readCounter++
		transactionID := row[0]
		transactionType := row[1]
		requestingTwitchUserID := row[2]

		if transactionType == "CustomerChargeback" || transactionType == "CustomerRevoke" {
			entriesToAppendAtTheEnd = append(entriesToAppendAtTheEnd, csvEntry{
				TransactionID:          transactionID,
				RequestingTwitchUserID: requestingTwitchUserID,
			})
		} else {
			err = csvWriter.Write([]string{transactionID, requestingTwitchUserID})
			if err != nil {
				logrus.WithError(err).Fatalf("Failed to write csv entry at index: %d", writeCounter)
			}
			writeCounter++
		}
	}

	cr_cc_csvOutputFile, err := os.Create(outputFile + "_customer-revoke-chargebacks")
	if err != nil {
		logrus.WithError(err).Fatalf("Failed to create %s", outputFile+"_customer-revoke-chargebacks")
	}
	defer cr_cc_csvOutputFile.Close()

	cr_cc_csvWriter := csv.NewWriter(cr_cc_csvOutputFile)
	defer cr_cc_csvWriter.Flush()

	for _, entry := range entriesToAppendAtTheEnd {
		err = cr_cc_csvWriter.Write([]string{entry.TransactionID, entry.RequestingTwitchUserID})
		if err != nil {
			logrus.WithError(err).Fatalf("Failed to write csv entry at index: %d", writeCounter)
		}
		writeCounter++
	}

	logrus.Infof("Finished! %d records read and %d written", readCounter, writeCounter)
}
