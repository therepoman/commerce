# Customer Chargeback Overrider

This script is essentially for Pachter bits spend order backfill, which you want to generally reference
`/scripts/bits-spend-order-backll` folder for the the details.
The problem with the backfill was that some of the old transactions from the Redshift are of type
`"CustomerChargeback"` or `"CustomerRevoke"`, which in some cases, have wrong timestamps. More specifically, they had _earlier_ timestamps
than they should have been. That resulted in negative balances
when computing spend order based on FIFO logic, when the bits spend order encountered such customer chargebacks.

Since we do not have a good way to know/fix the actual timestamps of such customer chargeback transactions, what we agreed
to do is to treat them as if they happened at the end of all transaction history of given user (or at the end of a given
time frame). This way, although it will compromise the bits spend order, we won't lose the transaction.

The way to do it is to simply change the order of transaction ids in the input file for the backfill. Consider this case:

**input.csv**
```
cc4d58af-681e-4840-8331-e2c6c7681cb4,AddBitsToCustomerAccount,153015056
f0d83f2e-7bf3-417c-92bf-d089ba2297b5,GiveBitsToBroadcaster,153015056
097a6d30-fafa-4991-ad90-dec3ca93ddc8,CustomerChargeback,153015056 //this one is customer chargeback with wrong timestamp!
33ab8326-d99b-4954-81ba-652099a79dfc,AddBitsToCustomerAccount,153015056
```

What we want to do is to change the order of this file content and put the result in the output file so that any customer chargeback will come at the end as such:

**output.csv**
```
cc4d58af-681e-4840-8331-e2c6c7681cb4,153015056
f0d83f2e-7bf3-417c-92bf-d089ba2297b5,153015056
33ab8326-d99b-4954-81ba-652099a79dfc,153015056
097a6d30-fafa-4991-ad90-dec3ca93ddc8,153015056 //this one is customer chargeback!
```

We can now use the output file to start the bits spend order backfill found at `/scripts/bits-spend-order-backll`.

To obtain the input file data, run the following query against the Redshift with the desired user ids:
```$sql
SELECT DISTINCT
  transactionid, transactiontype, requestingtwitchuserid
FROM
  transactioninfo
WHERE
  requestingtwitchuserid
IN
(
    '78050140',
    '26652161',
    '87118093',
    '411692857',
    '206935137',
    '141993646'  -- add whatever user ids here
)
AND
    timeofevent < 1563917182 -- in this case we are looking at data before Tuesday, July 23, 2019 2:26:22 PM GMT-07:00 DST
ORDER BY
    timeofevent, transactiontype
ASC;
```
for this script expects an input csv file with 3 columns: `transactionid`, `transactiontype`, `requestingtwitchuserid`.