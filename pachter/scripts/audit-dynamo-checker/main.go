package main

import (
	"bufio"
	"context"
	"encoding/csv"
	"os"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachter/backend/dynamo/worker_audit"
	"code.justin.tv/commerce/pachter/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/codegangsta/cli"
)

const (
	aws_region = "us-west-2"
)

var inputFile string
var auditDAO worker_audit.DAO
var env string

func main() {
	script := cli.NewApp()
	script.Name = "Pachter Audit table checker"
	script.Usage = "Checker"

	script.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "inputFile, i",
			Usage:       "the input file with transaction ids on each line",
			EnvVar:      "INPUT_FILE",
			Destination: &inputFile,
		},
		cli.StringFlag{
			Name:        "env, e",
			Usage:       "the environment",
			EnvVar:      "DYNAMO_ENVIRONMENT",
			Value:       "staging",
			Destination: &env,
		},
	}

	script.Action = run
	err := script.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Panic("Failed to run this script!")
	}
}

func run(c *cli.Context) {
	ctx := context.Background()

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(aws_region),
	})
	if err != nil {
		logrus.WithError(err).Panic("Failed to initiate aws session")
	}

	auditDAO = worker_audit.NewDAO(sess, &config.Config{
		EnvironmentName: env,
	})

	logrus.Info("Start reading CSV")
	startTime := time.Now()
	csvFile, _ := os.Open(inputFile)
	csvReader := csv.NewReader(bufio.NewReader(csvFile))
	defer csvFile.Close()
	logrus.Infof("CSV read. Took %.2f seconds", time.Since(startTime).Seconds())

	rows, err := csvReader.ReadAll()
	if err != nil {
		logrus.WithError(err).Panic("Failed to read CSV rows")
	}

	missingRecordsOut, err := os.Create("missing_records.csv")
	if err != nil {
		logrus.WithError(err).Panic("could not create output file")
	}
	missingRecordCsvWriter := csv.NewWriter(missingRecordsOut)
	defer missingRecordsOut.Close()
	defer missingRecordCsvWriter.Flush()

	missingSpendsOut, err := os.Create("missing_spends.csv")
	if err != nil {
		logrus.WithError(err).Panic("could not create output file")
	}
	missingSpendsCsvWriter := csv.NewWriter(missingSpendsOut)
	defer missingSpendsOut.Close()
	defer missingSpendsCsvWriter.Flush()

	missingTransactionsOut, err := os.Create("missing_transactions.csv")
	if err != nil {
		logrus.WithError(err).Panic("could not create output file")
	}
	missingTransactionsCsvWriter := csv.NewWriter(missingTransactionsOut)
	defer missingTransactionsOut.Close()
	defer missingTransactionsCsvWriter.Flush()

	missingCheermotesOut, err := os.Create("missing_cheermotes.csv")
	if err != nil {
		logrus.WithError(err).Panic("could not create output file")
	}
	missingCheermotesCsvWriter := csv.NewWriter(missingCheermotesOut)
	defer missingCheermotesOut.Close()
	defer missingCheermotesCsvWriter.Flush()

	missingRecords := make(map[string]interface{})
	missingTransactions := make(map[string]interface{})
	missingSpends := make(map[string]interface{})
	missingCheermotes := make(map[string]interface{})

	for i, row := range rows {
		transactionID := row[0]

		entry, err := auditDAO.Get(ctx, transactionID)
		if err != nil {
			logrus.WithError(err).Panic("Failed to read CSV rows")
		}

		if entry == nil {
			missingRecords[transactionID] = nil
			continue
		}

		if entry.Spend == nil {
			missingSpends[transactionID] = nil
		}

		if entry.Transactions == nil {
			missingTransactions[transactionID] = nil
		}

		if entry.Cheermote == nil {
			missingCheermotes[transactionID] = nil
		}

		if i%20 == 0 {
			logrus.Infof("Progress %d / %d \n", i+1, len(rows))
		}
	}

	logrus.Infof("Missing Full Aduit Record: %d \n", len(missingRecords))
	for txID := range missingRecords {
		err = missingRecordCsvWriter.Write([]string{txID})
		if err != nil {
			panic(err)
		}
	}

	logrus.Infof("Missing Spends Audit: %d \n", len(missingSpends))
	for txID := range missingSpends {
		err = missingSpendsCsvWriter.Write([]string{txID})
		if err != nil {
			panic(err)
		}
	}

	logrus.Infof("Missing Transactions Audit: %d \n", len(missingTransactions))
	for txID := range missingTransactions {
		err = missingTransactionsCsvWriter.Write([]string{txID})
		if err != nil {
			panic(err)
		}
	}

	logrus.Infof("Missing Cheermotes Audit: %d \n", len(missingCheermotes))
	for txID := range missingCheermotes {
		err = missingCheermotesCsvWriter.Write([]string{txID})
		if err != nil {
			panic(err)
		}
	}

}
