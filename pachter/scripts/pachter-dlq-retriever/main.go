package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"os"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachter/backend/worker"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/codegangsta/cli"
)

const (
	bits_spend_order_dlq_url = "https://sqs.us-west-2.amazonaws.com/458492755823/bits-spend-prod-dlq.fifo"
	auditor_dlq_url          = "https://sqs.us-west-2.amazonaws.com/458492755823/bits-transaction-audit-prod-deadletter"
	bits_transaction_dlq_url = "https://sqs.us-west-2.amazonaws.com/458492755823/bits-transaction-prod-deadletter"
)

type SQSMessage struct {
	MessageID string `json:"MessageID"`
	Message   string `json:"Message"`
}

type SQSMessageBody struct {
	TransactionIDs []string `json:"transaction_ids"`
}

var targetDLQ string

func main() {
	script := cli.NewApp()
	script.Name = "Pachter DLQ message retriever script"
	script.Usage = "Pulls DLQ messages and clears them"

	script.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "queue, q",
			Usage:       "the target DLQ. Either 'spend','audit', or 'transaction'",
			Destination: &targetDLQ,
		},
	}

	script.Action = run
	err := script.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Error("Run returned an err")
	}
}

func run(c *cli.Context) {
	sess, _ := session.NewSession()
	sqsClient := sqs.New(sess, &aws.Config{Region: aws.String("us-west-2")})

	var targetDLQURL string
	if targetDLQ == "spend" {
		targetDLQURL = bits_spend_order_dlq_url
	} else if targetDLQ == "audit" {
		targetDLQURL = auditor_dlq_url
	} else if targetDLQ == "transaction" {
		targetDLQURL = bits_transaction_dlq_url
	} else {
		logrus.WithField("targetDLQ", targetDLQ).Panic("Invalid target DLQ")
	}

	logrus.WithField("DLQ", targetDLQURL).Info("Start polling messages")
	receiveCounter := 0
	csvFile, err := os.Create("transaction_ids.csv")
	if err != nil {
		logrus.WithError(err).Panic("Failed to create output file")
	}
	defer csvFile.Close()

	csvWriter := csv.NewWriter(csvFile)
	defer csvWriter.Flush()

	for {
		params := &sqs.ReceiveMessageInput{
			QueueUrl:            aws.String(targetDLQURL),
			MaxNumberOfMessages: aws.Int64(10), //max is 10 per library specification
			WaitTimeSeconds:     aws.Int64(20), //max is 20 per library specification
			VisibilityTimeout:   aws.Int64(60),
			AttributeNames:      []*string{aws.String(worker.MESSAGE_GROUP_ID_ATTRIBUTE)},
		}

		resp, err := sqsClient.ReceiveMessage(params)
		if err != nil {
			logrus.WithError(err).Panic("Failed to receive message")
		}

		if len(resp.Messages) > 0 {
			receiveCounter += len(resp.Messages)
			writeCSV(resp.Messages, csvWriter)

			var batchDeleteEntries []*sqs.DeleteMessageBatchRequestEntry
			for _, msg := range resp.Messages {
				batchDeleteEntries = append(batchDeleteEntries, &sqs.DeleteMessageBatchRequestEntry{
					Id:            msg.MessageId,
					ReceiptHandle: msg.ReceiptHandle,
				})
			}
			_, err = sqsClient.DeleteMessageBatch(&sqs.DeleteMessageBatchInput{
				QueueUrl: aws.String(targetDLQURL),
				Entries:  batchDeleteEntries,
			})
			if err != nil {
				logrus.WithError(err).Panicf("Failed to delete messages: %+v", resp.Messages)
			}
		} else {
			break
		}

		if receiveCounter%100 == 0 {
			logrus.Infof("Polled %d messages...", receiveCounter)
		}
	}
	logrus.Infof("Finished polling: %d messages", receiveCounter)
}

func writeCSV(dlqMsgs []*sqs.Message, csvWriter *csv.Writer) {
	for _, dlqMsg := range dlqMsgs {
		var sqsMsg SQSMessage
		err := json.Unmarshal([]byte(*dlqMsg.Body), &sqsMsg)
		if err != nil {
			logrus.WithError(err).WithField(
				"*dlqMessage.Body", *dlqMsg.Body,
			).Panic("Failed to unmarshal dlq message")
		}

		var sqsBody SQSMessageBody
		err = json.Unmarshal([]byte(sqsMsg.Message), &sqsBody)
		if err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{
				"*dlqMessage.Body": *dlqMsg.Body,
				"sqsMsg.Message":   sqsBody,
			}).Panic("Failed to unmarshal dlq message body")
		}

		for _, txID := range sqsBody.TransactionIDs {
			if targetDLQ == "spend" {
				err = csvWriter.Write([]string{txID, worker.GetUserIDFromSpendOrderMessage(dlqMsg)})
			} else if targetDLQ == "audit" {
				err = csvWriter.Write([]string{txID})
			} else if targetDLQ == "transaction" {
				err = csvWriter.Write([]string{txID})
			}

			if err != nil {
				logrus.WithError(err).Panic(fmt.Sprintf("Failed to write %v to csv output file", txID))
			}
		}
	}
}
