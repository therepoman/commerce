package main

import (
	"bufio"
	"encoding/csv"
	"encoding/json"
	"io"
	"os"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachter/backend/worker"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/codegangsta/cli"
	"github.com/gofrs/uuid"
)

const (
	PACHTER_INGESTION_QUEUE_URL_STAGING = "https://sqs.us-west-2.amazonaws.com/021561903526/pachter_ingestion_staging"
	PACHTER_INGESTION_QUEUE_URL_PROD    = "https://sqs.us-west-2.amazonaws.com/021561903526/pachter_ingestion"
)

var inputFile string
var env string
var sqsURL string
var dryRun bool

func main() {
	script := cli.NewApp()
	script.Name = "Pachter Bits transactions and cheermote backfill script"
	script.Usage = "Use this script to backfill transactions and cheermote table in Pachter through Payday"

	script.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "inputFile, i",
			Usage:       "The input file with transaction ids to backfill",
			Destination: &inputFile,
		},
		cli.StringFlag{
			Name:        "environment, e",
			Usage:       "The environment in which backfill takes place. Either 'production' or 'staging'",
			Destination: &env,
		},
		cli.BoolFlag{
			Name:        "dry, d",
			Usage:       "True if dry run, false if not. Default to false",
			Destination: &dryRun,
		},
	}

	script.Action = run
	err := script.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Error("Run returned an err")
	}
}

func run(c *cli.Context) {
	logrus.SetOutput(os.Stdout)

	if env != "staging" && env != "production" {
		logrus.WithField("environment", env).
			Error("Invalid 'environment'. Try either 'staging' or 'production'")
		return
	}

	if env == "staging" {
		sqsURL = PACHTER_INGESTION_QUEUE_URL_STAGING
	} else {
		sqsURL = PACHTER_INGESTION_QUEUE_URL_PROD
	}

	logrus.WithFields(logrus.Fields{
		"environment": env,
		"target SQS":  sqsURL,
	}).Info("starting the script")

	awsConfig := aws.NewConfig().WithRegion("us-west-2")
	sess, err := session.NewSession(awsConfig)
	if err != nil {
		logrus.Panic("failed to create aws session")
	}
	sqsClient := sqs.New(sess, awsConfig)

	csvFile, _ := os.Open(inputFile)
	csvReader := csv.NewReader(bufio.NewReader(csvFile))
	transactionIDs := make([]string, 0)

	for {
		logger := logrus.WithField("counter", len(transactionIDs))
		row, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			logger.WithError(err).Errorf("failed to read transaction id")
			return
		}
		if len(row) < 1 {
			logger.Error("transaction id is not present in the input file")
			return
		}

		transactionID := row[0]
		transactionIDs = append(transactionIDs, transactionID)
	}

	logrus.Infof("%d transactions read: %+v", len(transactionIDs), transactionIDs)

	if !dryRun {
		_, err = sqsClient.SendMessage(&sqs.SendMessageInput{
			MessageBody: getSQSBody(transactionIDs),
			QueueUrl:    &sqsURL,
		})
		if err != nil {
			logrus.WithError(err).Error("Failed to send transaction ids to SQS")
		} else {
			logrus.Infof("%d transaction successfully sent", len(transactionIDs))
		}
	} else {
		logrus.Info("This is a dry run. Exiting.")
	}
}

func getSQSBody(transactionIDs []string) *string {
	balanceUpdate := worker.BalanceUpdateMessage{
		TransactionIDs: transactionIDs,
	}
	balanceUpdateJson, err := json.Marshal(&balanceUpdate)
	if err != nil {
		logrus.WithError(err).Panic("failed to marshal transaction")
	}

	snsMessage := worker.SNSMessage{
		MessageID: uuid.Must(uuid.NewV4()).String(),
		Message:   string(balanceUpdateJson),
	}
	snsMessageJson, err := json.Marshal(&snsMessage)
	if err != nil {
		logrus.WithError(err).Panic("failed to marshal sns message")
	}

	return aws.String(string(snsMessageJson))
}
