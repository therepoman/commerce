package main

import (
	"bufio"
	"encoding/csv"
	"io"
	"os"
	"strconv"
	"strings"
	"time"

	log "code.justin.tv/commerce/logrus"
	"github.com/codegangsta/cli"
)

/**
  This is the example query this is based on, if you change any of this logic you'll need to update the pachter report parsing logic.

SELECT
	RTRIM(t.receiving_twitch_user_id, ' ') as user_id,
	SUM(COALESCE(bs.quantity, t.quantity)),
	CASE
      WHEN btt.bits_type LIKE '%BitsForAds%' THEN 'BitsForAds'
      WHEN btt.bits_type IS NULL THEN 'UnknownBitType'
      ELSE 'PurchasedOrGrantedBits'
  END AS BitType
FROM bits_transactions t
  LEFT JOIN bits_spends bs ON bs.transaction_id = t.transaction_id
  LEFT JOIN bits_transactions btt ON bs.reference_transaction_id = btt.transaction_id
WHERE t.datetime > '2019-10-31 23:59:59'
  	AND t.datetime < '2019-12-01'
  	AND t.transaction_type IN ('GiveBitsToBroadcaster',
  	                           'GiveBitsToBroadcasterType',
  	                           'UseBitsOnPoll')
GROUP BY user_id, BitType

------
For sponsored cheermote values run this:

SELECT
	RTRIM(t.receiving_twitch_user_id, ' ') as user_id,
	SUM(t.sponsored_quantity) as sponsored_sum
FROM bits_transactions t
WHERE t.datetime > '2019-10-31 23:59:59'
  	AND t.datetime < '2019-12-01'
  	AND t.transaction_type IN ('GiveBitsToBroadcaster',
  	                           'GiveBitsToBroadcasterType',
  	                           'UseBitsOnPoll')

  Example run for consolidated report
    go run scripts/report-rewriter/main.go -r ~/Downloads/2019-11-01_bits_combined_payout.csv -p ~/Downloads/2019-11-01_pachterReport.csv -o ~/Downloads/2019-11-01_bits_combined_payout_corrected.csv  -t bits-consolidated

  Example for net-15
    go run scripts/report-rewriter/main.go -r ~/Downloads/2019-11-01_combination_revenue.csv -p ~/Downloads/2019-11-01_pachterReport.csv -o ~/Downloads/2019-11-01_combination_revenue_corrected.csv  -t net-15
*/

var pachterFile string
var reportFile string
var sponsoredCheermoteFile string
var reportType string
var outputFile string

func main() {
	script := cli.NewApp()
	script.Name = "Pachter Transaction Report Over writer"
	script.Usage = "Given a dump of data from Pachter's RDS table it'll rewrite the reports with the pachter data (in place)."

	script.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "pachterFile, p",
			Usage:       "File of bits totals from Pachter.",
			Destination: &pachterFile,
		},
		cli.StringFlag{
			Name:        "reportFile, r",
			Usage:       "Report from admin panel.",
			Destination: &reportFile,
		},
		cli.StringFlag{
			Name:        "sponsoredCheermoteFile, c",
			Usage:       "Sponsored cheermote totals from Pachter.",
			Destination: &sponsoredCheermoteFile,
		},
		cli.StringFlag{
			Name:        "outputFile, o",
			Usage:       "the outputFile",
			Value:       "new_report.csv",
			Destination: &outputFile,
		},
		cli.StringFlag{
			Name:        "reportType, t",
			Usage:       "The report type we're updating",
			Value:       "none",
			Destination: &reportType,
		},
	}

	script.Action = run
	err := script.Run(os.Args)
	if err != nil {
		log.WithError(err).Panic("Failed to run this script!")
	}
}

func run(c *cli.Context) {
	startTime := time.Now()
	pachterMap := generatePachterDataMap()
	log.WithFields(log.Fields{
		"pachterMap size": len(pachterMap),
		"runtime":         time.Since(startTime).Seconds(),
	}).Info("Pachter map generated")

	startTime = time.Now()
	sponsoredCheermoteMap := generateSponsoredCheermoteDataMap()
	log.WithFields(log.Fields{
		"sponsoredCheermoteMap size": len(sponsoredCheermoteMap),
		"runtime":                    time.Since(startTime).Seconds(),
	}).Info("Sponsored cheermote map generated")

	if reportType == "none" {
		updateNoneReportType(pachterMap, sponsoredCheermoteMap)
	} else if reportType == "net-15" {
		updateNet15ReportType(pachterMap, sponsoredCheermoteMap)
	} else if reportType == "bits-consolidated" {
		updateBitsConsolidatedReportType(pachterMap, sponsoredCheermoteMap)
	} else {
		log.WithFields(log.Fields{
			"reportType": reportType,
		}).Panic("Invalid report type passed in.")
	}
}

// this just does a pivot in go lang.
func updateNoneReportType(pachterMap map[string]pachterEntry, sponsoredCheermoteMap map[string]int64) {
	logger := log.WithField("outputFile", outputFile)

	outputFile, err := os.Create(outputFile)
	if err != nil {
		logger.WithError(err).Panic("could not create output file")
	}
	defer func() {
		_ = outputFile.Close()
	}()
	csvWriter := csv.NewWriter(outputFile)
	defer csvWriter.Flush()

	err = csvWriter.Write([]string{"userId", "nonAdsBits", "adsBits", "unknownBits", "sponsoredBits", "totalBits"})
	if err != nil {
		logger.Panic("Failed to write header entry in CSV file.")
	}

	for userId, entry := range pachterMap {
		sponsoredAmount := sponsoredCheermoteMap[userId]

		err = csvWriter.Write([]string{
			userId,
			formatInt(entry.nonAdsBits),
			formatInt(entry.adsBits),
			formatInt(entry.unknownBits),
			formatInt(sponsoredAmount),
			formatInt(getTotal(entry) + sponsoredAmount),
		})
		if err != nil {
			logger.Panic("Failed to write entry in CSV file.")
		}
	}
}

// this report has the structure of user_id,payout_entity_id,ad_share_gross,sub_share_gross,bits_share_gross,bits_developer_share_gross,bits_extension_share_gross,prime_sub_share_gross,bit_share_ad_gross,fuel_rev_gross,bb_rev_gross,report_date
// bits_share_gross is the only column we need to update
// it's at position 4
func updateNet15ReportType(pachterMap map[string]pachterEntry, sponsoredCheermoteMap map[string]int64) {
	logger := log.WithFields(log.Fields{
		"outputFile":      outputFile,
		"reportFile":      pachterFile,
		"reportType":      reportType,
		"pachterMap size": len(pachterMap),
	})

	reportCsvFile, err := os.Open(reportFile)
	if err != nil {
		logger.Panic("Couldn't open net-15 report file.")
	}
	defer func() {
		_ = reportCsvFile.Close()
	}()

	reportCsvReader := csv.NewReader(bufio.NewReader(reportCsvFile))

	outputFile, err := os.Create(outputFile)
	if err != nil {
		logger.WithError(err).Panic("could not file to write updated report to.")
	}
	defer func() {
		_ = outputFile.Close()
	}()
	csvWriter := csv.NewWriter(outputFile)
	defer csvWriter.Flush()

	for {
		csvRow, err := reportCsvReader.Read()
		if err == io.EOF {
			break
		}

		// write the header row in the new one if it was in the old one.
		if strings.Contains(csvRow[0], "user_id") || strings.Contains(csvRow[0], "userId") {
			err = csvWriter.Write(csvRow)
			if err != nil {
				logger.Panic("Failed to write header entry in CSV file.")
			}
			continue
		}

		userId := strings.TrimSpace(csvRow[0])
		pachterData, ok := pachterMap[userId]
		if ok && pachterData.processed {
			logger.WithField("userId", userId).Warn("Found duplicate")
		}

		sponsoredAmount := sponsoredCheermoteMap[userId]

		// first four are the same so just copy them over
		csvEntry := []string{userId, csvRow[1], csvRow[2], csvRow[3]}

		// non-ads bits
		csvEntry = append(csvEntry, formatFloat(float64(pachterData.unknownBits+pachterData.nonAdsBits+sponsoredAmount)/100.0))

		for i := 5; i < 8; i++ {
			csvEntry = append(csvEntry, csvRow[i])
		}

		// add bits
		csvEntry = append(csvEntry, formatFloat(float64(pachterData.adsBits)/100.0))

		// attach the rest of the rows as is until we run out.
		for i := 9; i < len(csvRow); i++ {
			csvEntry = append(csvEntry, csvRow[i])
		}

		err = csvWriter.Write(csvEntry)
		if err != nil {
			logger.Panic("Failed to write CSV row.")
		}

		pachterData.processed = true
		pachterMap[userId] = pachterData
	}

	logger.Info("finished pulling from existing report.")

	for userId, pachterData := range pachterMap {
		if !pachterData.processed {
			logger.WithField("userId", userId).Error("Found bits entry that didn't have an existing net-15 entry.")
		}
	}
}

// this report has the structure of userId, partnerType, numBits, payoutCents, bitsForAds, bitsForAdsPayoutCents, payoutCentsLessBitsForAds, eligbleForPayouts
// we will need to update numBits, payoutCents, bitsForAds, bitsForAdsPayoutCents, payoutCentsLessBitsForAds
// columns 2 - 6
func updateBitsConsolidatedReportType(pachterMap map[string]pachterEntry, sponsoredCheermoteMap map[string]int64) {
	startTime := time.Now()

	logger := log.WithFields(log.Fields{
		"outputFile":      outputFile,
		"reportFile":      pachterFile,
		"reportType":      reportType,
		"pachterMap size": len(pachterMap),
	})

	reportCsvFile, err := os.Open(reportFile)
	if err != nil {
		logger.Panic("Couldn't open bits-consolidated report file.")
	}
	defer func() {
		_ = reportCsvFile.Close()
	}()

	reportCsvReader := csv.NewReader(bufio.NewReader(reportCsvFile))

	outputFile, err := os.Create(outputFile)
	if err != nil {
		logger.WithError(err).Panic("could not file to write updated report to.")
	}
	defer func() {
		_ = outputFile.Close()
	}()
	csvWriter := csv.NewWriter(outputFile)
	defer csvWriter.Flush()

	var processedCount int
	for {
		csvRow, err := reportCsvReader.Read()
		if err == io.EOF {
			break
		}

		// write the header row in the new one if it was in the old one, same exact scheme too
		if strings.Contains(csvRow[0], "user_id") || strings.Contains(csvRow[0], "userId") {
			err = csvWriter.Write(csvRow)
			if err != nil {
				logger.Panic("Failed to write header entry in CSV file.")
			}
			continue
		}

		userId := strings.TrimSpace(csvRow[0])
		pachterData, ok := pachterMap[userId]
		if !ok {
			logger.WithField("userId", userId).Panic("pachter data missing for row in consolidated report")
		} else if pachterData.processed {
			logger.WithField("userId", userId).Warn("Found duplicate")
		} else {
			processedCount++
		}

		sponsoredAmount := sponsoredCheermoteMap[userId]

		// first two are the same so just copy them over
		csvEntry := []string{userId, csvRow[1]}

		csvEntry = append(csvEntry, formatInt(getTotal(pachterData)))
		csvEntry = append(csvEntry, formatFloat(float64(getTotal(pachterData)+sponsoredAmount)/100.0))
		csvEntry = append(csvEntry, formatInt(pachterData.adsBits))
		csvEntry = append(csvEntry, formatFloat(float64((pachterData.adsBits)/100.0)))
		csvEntry = append(csvEntry, formatFloat(float64((pachterData.unknownBits+pachterData.nonAdsBits+sponsoredAmount)/100.0)))

		// so is the last one
		csvEntry = append(csvEntry, csvRow[7])

		err = csvWriter.Write(csvEntry)
		if err != nil {
			logger.Panic("Failed to write row in CSV file.")
		}

		pachterData.processed = true
		pachterMap[userId] = pachterData
	}

	//TODO: find everyone not in the original report
	logger.WithFields(log.Fields{
		"processedEntries": processedCount,
		"delta":            len(pachterMap) - processedCount,
	}).Info("finished pulling from existing report.")

	for userId, pachterData := range pachterMap {
		if !pachterData.processed {
			csvEntry := []string{userId, "UNKNOWN"}

			csvEntry = append(csvEntry, formatInt(getTotal(pachterData)))
			csvEntry = append(csvEntry, formatFloat(float64(getTotal(pachterData))/100.0))
			csvEntry = append(csvEntry, formatInt(pachterData.adsBits))
			csvEntry = append(csvEntry, formatFloat(float64((pachterData.adsBits)/100.0)))
			csvEntry = append(csvEntry, formatFloat(float64((pachterData.unknownBits+pachterData.nonAdsBits)/100.0)))

			// so is the last one
			csvEntry = append(csvEntry, "UNKNOWN")

			err = csvWriter.Write(csvEntry)
			if err != nil {
				logger.Panic("Failed to write header entry in CSV file.")
			}
		}
	}

	logger.WithField("runtime", time.Since(startTime).Seconds()).Info("Wrote updated consolidated report")
}

type pachterEntry struct {
	adsBits     int64
	nonAdsBits  int64
	unknownBits int64
	processed   bool
}

func generateSponsoredCheermoteDataMap() map[string]int64 {
	csvFile, err := os.Open(sponsoredCheermoteFile)
	if err != nil {
		log.WithField("sponsoredCheermoteFile", sponsoredCheermoteFile).Panic("Couldn't open SponsoredCheermote file.")
	}
	defer func() {
		_ = csvFile.Close()
	}()

	csvReader := csv.NewReader(bufio.NewReader(csvFile))

	sponsoredCheermoteValues := make(map[string]int64)
	for {
		csvRow, err := csvReader.Read()
		if err == io.EOF {
			break
		}

		// skip the header row if present
		if strings.Contains(csvRow[0], "user_id") || strings.Contains(csvRow[0], "userId") {
			continue
		}

		userId := strings.TrimSpace(csvRow[0])
		quantity, err := strconv.ParseInt(csvRow[1], 10, 64)

		logger := log.WithFields(log.Fields{
			"userId":          userId,
			"quantity string": csvRow[1],
			"quantity":        quantity,
		})

		if err != nil {
			logger.WithError(err).Panic("Failed parse quantity")
			break
		}
		sponsoredCheermoteValues[userId] = quantity
	}
	return sponsoredCheermoteValues
}

func generatePachterDataMap() map[string]pachterEntry {
	csvFile, err := os.Open(pachterFile)
	if err != nil {
		log.WithField("pachterFile", pachterFile).Panic("Couldn't open Pachter file.")
	}
	defer func() {
		_ = csvFile.Close()
	}()

	csvReader := csv.NewReader(bufio.NewReader(csvFile))

	pachterTotals := make(map[string]pachterEntry)
	for {
		csvRow, err := csvReader.Read()
		if err == io.EOF {
			break
		}

		// skip the header row if present
		if strings.Contains(csvRow[0], "user_id") || strings.Contains(csvRow[0], "userId") {
			continue
		}

		userId := strings.TrimSpace(csvRow[0])
		quantity, err := strconv.ParseInt(csvRow[1], 10, 64)
		useType := strings.TrimSpace(csvRow[2])

		logger := log.WithFields(log.Fields{
			"userId":          userId,
			"quantity string": csvRow[1],
			"quantity":        quantity,
			"useType":         useType,
		})

		if err != nil {
			logger.WithError(err).Panic("Failed parse quantity")
			break
		}
		total := pachterTotals[userId]
		if useType == "PurchasedOrGrantedBits" {
			total.nonAdsBits += quantity
		} else if useType == "BitsForAds" {
			total.adsBits += quantity
		} else if useType == "UnknownBitType" {
			total.unknownBits += quantity
		} else {
			logger.Error("Unhandled bits type")
		}
		pachterTotals[userId] = total
	}
	return pachterTotals
}

func getTotal(pachterData pachterEntry) int64 {
	return pachterData.adsBits + pachterData.nonAdsBits + pachterData.unknownBits
}

func formatInt(number int64) string {
	return strconv.FormatInt(number, 10)
}

func formatFloat(number float64) string {
	return strconv.FormatFloat(number, 'f', 2, 64)
}
