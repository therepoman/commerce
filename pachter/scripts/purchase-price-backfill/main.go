/*
	The input file should be a CSV file with two columns each row, first column being
    the transaction id and the second purchase price (USC) for that entitlement transaction
    e.g.)
         d402c0ce-8cc3-4c79-b990-538131706aab,172
         GPA.3361-9605-1478-06823,199
         848A9488-510C-4334-837A-F9D1813B7D57,213
*/
package main

import (
	"bufio"
	"encoding/csv"
	"io"
	"log"
	"os"
	"strconv"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachter/backend/clients/rds"
	"code.justin.tv/commerce/pachter/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/codegangsta/cli"
)

var (
	env               string
	inputFile         string
	rdsSecretStoreARN string
)

const (
	rdsSecretStoreARNStaging = "arn:aws:secretsmanager:us-west-2:721938063588:secret:pachter-rds-credentials-Kh5FrK"
	rdsSecretStoreARNProd    = "arn:aws:secretsmanager:us-west-2:458492755823:secret:pachter-rds-credentials-2AUg2L"
)

func main() {
	logrus.SetOutput(os.Stdout)

	script := cli.NewApp()
	script.Name = "Backfill purchase price per transaction"
	script.Usage = "go run ./main.go -e prod[staging] -i inputFile"

	script.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "inputFile, i",
			Usage:       "the input file with purchase prices to backfill",
			Destination: &inputFile,
		},
		cli.StringFlag{
			Name:        "env, e",
			Usage:       "the target environment (either 'staging' or 'prod')",
			Destination: &env,
		},
	}

	script.Action = run
	logrus.Info("Starting to backfill purchase prices")

	err := script.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Panic("Failed to execute script")
	}
}

func run(c *cli.Context) {
	if env == "prod" {
		rdsSecretStoreARN = rdsSecretStoreARNProd
	} else {
		rdsSecretStoreARN = rdsSecretStoreARNStaging
	}

	logrus.Infof("Environment: %s", env)
	logrus.Infof("RDS secret store ARN: %s", rdsSecretStoreARN)

	// configure clients
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2"),
	})
	if err != nil {
		logrus.WithError(err).Panic("failed to create AWS session")
	}

	cfg := &config.Config{
		RDSAWSSecretStoreARN: rdsSecretStoreARN,
	}

	rdsClient, err := rds.NewRDSClient(cfg, sess)
	if err != nil {
		logrus.WithError(err).Panic("Failed to initialize RDS client")
	}

	// read input file
	file, err := os.Open(inputFile)
	if err != nil {
		log.Fatal("Cannot open input file")
	}
	defer file.Close()

	counter := 0
	tranSuccessCounter := 0
	csvReader := csv.NewReader(bufio.NewReader(file))
	for {
		row, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			logrus.WithField("counter", counter).WithError(err).Error("Failed to read a row")
			continue
		}

		counter++

		if len(row) < 2 {
			logrus.WithField("counter", counter).Error("Row doesn't have enough columns")
			continue
		}

		transactionID := row[0]
		purchasePriceUSD := row[1]

		priceUSC, err := strconv.Atoi(purchasePriceUSD)
		if err != nil {
			logrus.WithError(err).Error("Failed to convert string to cents")
			continue
		}

		tranErr := rdsClient.Exec("UPDATE bits_transactions SET purchase_price_usc = ? WHERE transaction_id = ?", priceUSC, transactionID).Error
		if tranErr != nil {
			logrus.WithFields(logrus.Fields{
				"counter":       counter,
				"transactionID": transactionID,
			}).WithError(err).Error("Failed to update bits_transactions")
		} else {
			tranSuccessCounter++
		}
	}

	logrus.WithFields(logrus.Fields{
		"transaction count":     counter,
		"transaction processed": tranSuccessCounter,
	}).Info("Finished!")
}
