package main

import (
	"bufio"
	"context"
	"encoding/csv"
	"errors"
	"io"
	"os"
	"strconv"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachter/backend/dynamo/bits_cheermote_payout_config"
	"code.justin.tv/commerce/pachter/backend/dynamo/models"
	"code.justin.tv/commerce/pachter/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/codegangsta/cli"
)

var inputFile string
var env string

func main() {
	script := cli.NewApp()
	script.Name = "Pachter Audit table resetter"
	script.Usage = "Resetter"

	script.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "inputFile, i",
			Usage:       "the input file CSV",
			EnvVar:      "INPUT_FILE",
			Destination: &inputFile,
		},
		cli.StringFlag{
			Name:        "env, e",
			Usage:       "the environment",
			EnvVar:      "DYNAMO_ENVIRONMENT",
			Value:       "staging",
			Destination: &env,
		},
	}

	script.Action = run
	err := script.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Panic("Failed to run this script!")
	}
}

func run(c *cli.Context) {
	cfg, err := config.LoadConfig(config.Environment(env))
	if err != nil {
		logrus.WithError(err).Panic("could not load config")
	}

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.AWSRegion),
	})
	if err != nil {
		logrus.WithError(err).Panic("failed to initiate aws session")
	}

	dao := bits_cheermote_payout_config.NewDAO(sess, cfg)

	file, err := os.Open(inputFile)
	if err != nil {
		logrus.WithError(err).Panic("could not load input file")
	}

	fileBuffer := bufio.NewReader(file)
	csvReader := csv.NewReader(fileBuffer)

	rowIndex := 0
	for {
		row, csvErr := csvReader.Read()

		if csvErr != nil && csvErr != io.EOF {
			logrus.WithField("rowIndex", rowIndex).WithError(err).Error("failed to read row from input file")
			continue
		}

		if row != nil {
			payoutConfig, err := parseRow(row)
			if err != nil {
				logrus.WithField("rowIndex", rowIndex).WithError(err).Error("failed to parse row from input file")
				continue
			}

			err = dao.Put(context.Background(), payoutConfig)
			if err != nil {
				logrus.WithError(err).WithField("rowIndex", rowIndex).Error("failed to put payout config in dynamo")
			}
		}

		if csvErr == io.EOF {
			logrus.Infof("Hit EOF after row #%d. Terminating!", rowIndex)
			break
		}

		rowIndex++
	}

	logrus.Info("finished script.")
}

func parseRow(input []string) (*models.BitsCheermotePayoutConfig, error) {
	if len(input) < 6 {
		return nil, errors.New("not enough fields in input")
	}

	ratio, err := strconv.ParseFloat(input[4], 64)
	if err != nil {
		return nil, errors.New("could not parse payout rev share ratio")
	}

	payoutConfig := &models.BitsCheermotePayoutConfig{
		Prefix: input[2],
		Ratio:  ratio,
	}

	if input[0] != "" {
		payoutConfig.TwitchUserID = input[0]
	} else {
		payoutConfig.TwitchUserID = models.EmptyTwitchUserID
	}

	if input[1] != "" {
		payoutConfig.PayoutEntity = input[1]
	}

	return payoutConfig, nil
}
