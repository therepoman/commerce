# Pachter Bits Spend Order Backfill

This script is to backfill the emote payout configs in Pachter. We need this to be in our system correctly so we can payout the appropriate parties for cheermote usage of their cheermotes.

To run this script, follow the next steps:
1. Make sure you are using **Pachter**'s aws credentials on your laptop.
1. Get a copy of the emote royalty report we currently generate revenue report from [Admin Panel](https://admin-panel.internal.justin.tv/revenue_reporting/reports) by selecting the `Bits: Emote Royaly` option from the `Report Types` dropdown, then clicking the `Run Report` button. This will generate the report for you, which then you should download after it has finished generating.
1. Now you can just run this script by 
```bash
AWS_PROFILE=pachter-prod-profile-name go run scripts/backfill-emote-payout-info/main.go -e prod -i path/to/the/report/you/downloaded
```
It should not take that long to run and it will backfill the emote payouts configs in Pachter based on the old report.
