package main

import (
	"bufio"
	"context"
	"encoding/csv"
	"encoding/json"
	"io"
	"os"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachter/backend/worker"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/codegangsta/cli"
	"github.com/gofrs/uuid"
	"golang.org/x/time/rate"
)

const (
	BATCH_SIZE               = 10
	MESSAGES_PER_BATCH_ENTRY = 10

	TRANSACTION_QUEUE_URL_STAGING = "https://sqs.us-west-2.amazonaws.com/721938063588/bits-transaction-staging"
	TRANSACTION_QUEUE_URL_PROD    = "https://sqs.us-west-2.amazonaws.com/458492755823/bits-transaction-prod"

	CHEERMOTE_QUEUE_URL_STAGING = "https://sqs.us-west-2.amazonaws.com/721938063588/cheermote-usage-staging"
	CHEERMOTE_QUEUE_URL_PROD    = "https://sqs.us-west-2.amazonaws.com/458492755823/cheermote-usage-prod"

	AUDITOR_QUEUE_URL_STAGING = "https://sqs.us-west-2.amazonaws.com/721938063588/bits-transaction-audit-staging"
	AUDITOR_QUEUE_URL_PROD    = "https://sqs.us-west-2.amazonaws.com/458492755823/bits-transaction-audit-prod"
)

var inputFile string
var env string
var target string
var sqsURL string

func main() {
	script := cli.NewApp()
	script.Name = "Pachter bits transactions/cheermote backfill script"
	script.Usage = "Use this script to backfill either transactions or cheermote table (or both)"

	script.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "inputFile, i",
			Usage:       "The input file with transaction ids to backfill",
			Destination: &inputFile,
		},
		cli.StringFlag{
			Name:        "environment, e",
			Usage:       "The environment in which backfill takes place. Either 'production' or 'staging'",
			Destination: &env,
		},
		cli.StringFlag{
			Name:        "target, t",
			Usage:       "The table you want to backfill. Either 'transaction' or 'cheermote' or 'audit'",
			Destination: &target,
		},
	}

	script.Action = run
	err := script.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Error("Run returned an err")
	}
}

func run(c *cli.Context) {
	logrus.SetOutput(os.Stdout)

	if env != "staging" && env != "production" {
		logrus.WithField("environment", env).Error("Invalid 'environment'. Try either 'staging' or 'production'")
		return
	}

	if target != "transaction" && target != "cheermote" && target != "audit" {
		logrus.WithField("target", target).Error("Invalid 'target'. Try either 'transaction' or 'cheermote' or 'audit'")
		return
	}

	if env == "staging" {
		if target == "transaction" {
			sqsURL = TRANSACTION_QUEUE_URL_STAGING
		} else if target == "cheermote" {
			sqsURL = CHEERMOTE_QUEUE_URL_STAGING
		} else {
			sqsURL = AUDITOR_QUEUE_URL_STAGING
		}
	} else {
		if target == "transaction" {
			sqsURL = TRANSACTION_QUEUE_URL_PROD
		} else if target == "cheermote" {
			sqsURL = CHEERMOTE_QUEUE_URL_PROD
		} else {
			sqsURL = AUDITOR_QUEUE_URL_PROD
		}
	}

	logrus.WithFields(logrus.Fields{
		"environment": env,
		"target sqs":  sqsURL,
	}).Info("starting the script")

	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))
	sqsClient := sqs.New(sess)

	csvFile, _ := os.Open(inputFile)
	csvReader := csv.NewReader(bufio.NewReader(csvFile))
	counter := 0
	batchCounter := 0

	batch := make([]*sqs.SendMessageBatchRequestEntry, 0)
	rateLimiter := rate.NewLimiter(rate.Limit(100), 100) // 1K TPS should be fine because AWS doc says standard SQS queue has no limit on that
	transactionIDs := make([]string, 0, MESSAGES_PER_BATCH_ENTRY)

	for {
		logger := logrus.WithFields(logrus.Fields{
			"counter":      counter,
			"batchCounter": batchCounter,
		})
		row, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			logger.WithError(err).Errorf("failed to read transaction id")
			return
		}
		if len(row) < 1 {
			logger.Error("transaction id is not present in the input file")
			return
		}

		transactionID := row[0]
		transactionIDs = append(transactionIDs, transactionID)
		counter++

		if len(transactionIDs) == MESSAGES_PER_BATCH_ENTRY {
			entry := &sqs.SendMessageBatchRequestEntry{
				Id:          aws.String(uuid.Must(uuid.NewV4()).String()),
				MessageBody: getSQSBody(transactionIDs, counter),
			}
			batch = append(batch, entry)

			if len(batch) == BATCH_SIZE {
				err = sendBatch(sqsClient, batch, rateLimiter)
				if err != nil {
					logger.WithError(err).Error("failed to send sqs message")
					return
				}
				batch = make([]*sqs.SendMessageBatchRequestEntry, 0, BATCH_SIZE)
				batchCounter++
			}
			transactionIDs = make([]string, 0, MESSAGES_PER_BATCH_ENTRY)
		}

		if counter%100000 == 0 {
			logrus.Infof("processed %d records and %d batches\n", counter, batchCounter)
		}
	}

	// send the remaining ones
	if len(transactionIDs) > 0 || len(batch) > 0 {
		if len(transactionIDs) > 0 {
			entry := &sqs.SendMessageBatchRequestEntry{
				Id:          aws.String(uuid.Must(uuid.NewV4()).String()),
				MessageBody: getSQSBody(transactionIDs, counter),
			}
			batch = append(batch, entry)
		}
		err := sendBatch(sqsClient, batch, rateLimiter)
		if err != nil {
			logrus.WithField("batchCounter", batchCounter).WithError(err).Error("failed to send sqs message")
			return
		}
		batchCounter++
	}

	logrus.WithFields(logrus.Fields{
		"counter":      counter,
		"batchCounter": batchCounter,
	}).Info("script complete")
}

func sendBatch(client *sqs.SQS, entries []*sqs.SendMessageBatchRequestEntry, limiter *rate.Limiter) error {
	_ = limiter.Wait(context.Background())

	var err error
	for i := 0; i < 3; i++ {
		batchSendOutput, err := client.SendMessageBatch(&sqs.SendMessageBatchInput{
			Entries:  entries,
			QueueUrl: &sqsURL,
		})
		if err != nil || len(batchSendOutput.Failed) > 0 {
			for failIndex, fail := range batchSendOutput.Failed {
				logrus.Infof("Failed Message #: %d - %s", failIndex, *fail.Message)
			}

			time.Sleep(100 * time.Millisecond)
		}

		if err == nil {
			return nil
		}
	}

	return err
}

func getSQSBody(transactionIDs []string, index int) *string {
	balanceUpdate := worker.BalanceUpdateMessage{
		TransactionIDs: transactionIDs,
	}
	balanceUpdateJson, err := json.Marshal(&balanceUpdate)
	if err != nil {
		logrus.WithError(err).Panicf("failed to marshal transaction at transaction counter: %d", index)
	}

	snsMessage := worker.SNSMessage{
		MessageID: uuid.Must(uuid.NewV4()).String(),
		Message:   string(balanceUpdateJson),
	}
	snsMessageJson, err := json.Marshal(&snsMessage)
	if err != nil {
		logrus.WithError(err).Panicf("failed to marshal sns message at transaction counter: %d", index)
	}

	return aws.String(string(snsMessageJson))
}
