unload
($$
WITH distinct_transactions AS
(
  SELECT DISTINCT
    transactionid, requestingtwitchuserid, MIN(timeofevent) AS timeofevent, transactiontype
  FROM
    transactioninfo
  WHERE
  	processedbitamount != 0
  AND
  	timeofevent < 1563917182 -- July 23, 2019 2:26:22 PM GMT-07:00 (change this time as needed)
  GROUP BY
    transactionid, requestingtwitchuserid, transactiontype
), transactioncount AS
(
  SELECT
    COUNT(transactionid) AS t_count, transactionid
  FROM
    distinct_transactions
  GROUP BY
    transactionid
), duplicate_ids AS
(
  SELECT
    transactionid
  FROM
    transactioncount
  WHERE
    t_count > 1
), non_duplicate_ids AS
(
  SELECT
    transactionid
  FROM
    transactioncount
  WHERE
    t_count = 1
), duplicate_transactioninfo AS
(
  SELECT
    distinct_transactions.transactionid + ':DEDUPEFIX:' +  distinct_transactions.requestingtwitchuserid as transactionid,
    distinct_transactions.requestingtwitchuserid
  FROM
    duplicate_ids
  INNER JOIN
    distinct_transactions
  ON
    duplicate_ids.transactionid = distinct_transactions.transactionid
), non_duplicate_transactioninfo AS
(
  SELECT
    distinct_transactions.transactionid,
    distinct_transactions.requestingtwitchuserid
  FROM
    non_duplicate_ids
  INNER JOIN
    distinct_transactions
  ON
    non_duplicate_ids.transactionid = distinct_transactions.transactionid
), de_duped_transactioninfo AS
(
	SELECT
	  *
  FROM
    duplicate_transactioninfo
  UNION SELECT
    *
  FROM
    non_duplicate_transactioninfo
)

SELECT
  de_duped_transactioninfo.transactionid, distinct_transactions.transactiontype, distinct_transactions.requestingtwitchuserid
FROM
  distinct_transactions
INNER JOIN
  de_duped_transactioninfo
ON
  distinct_transactions.requestingtwitchuserid = de_duped_transactioninfo.requestingtwitchuserid
AND
  position(distinct_transactions.transactionid in de_duped_transactioninfo.transactionid) > 0
ORDER BY
  distinct_transactions.timeofevent, distinct_transactions.transactiontype
ASC;
$$)
to 's3://pachter-spend-order-backfill/dump_test/'
credentials 'aws_access_key_id=<access_key_id>;aws_secret_access_key=<secret_access_key>'
delimiter as ','
parallel off