package main

import (
	"bufio"
	"context"
	"encoding/csv"
	"encoding/json"
	"io"
	"os"
	"strconv"
	"time"

	"code.justin.tv/commerce/logrus"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/codegangsta/cli"
	"github.com/gofrs/uuid"
	"golang.org/x/time/rate"
)

const (
	BATCH_SIZE = 10

	SQS_QUEUE_URL_STAGING     = "https://sqs.us-west-2.amazonaws.com/721938063588/bits-spend-staging.fifo"
	SQS_QUEUE_URL_PROD_FORMAT = "https://sqs.us-west-2.amazonaws.com/458492755823/bits-spend-backfill-prod-%s.fifo"
)

var inputFile string
var env string
var sqsURL string

type SNSMessage struct {
	MessageID string `json:"MessageID"`
	Message   string `json:"Message"`
}

type BalanceUpdateMessage struct {
	TransactionIDs []string `json:"transaction_ids"`
}

func main() {
	script := cli.NewApp()
	script.Name = "Pachter bits spends backfill script"
	script.Usage = "Backfill"

	script.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "inputFile, i",
			Usage:       "the input file with transactions to backfill",
			EnvVar:      "INPUT_FILE",
			Destination: &inputFile,
		},
		cli.StringFlag{
			Name:        "environment, e",
			Usage:       "environment in which the backfill takes place. Either 'staging' or 'prod' (defaults to staging)",
			Value:       "staging",
			Destination: &env,
		},
	}

	script.Action = run
	err := script.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Error("Run returned an err")
	}
}

func run(c *cli.Context) {
	logrus.SetOutput(os.Stdout)

	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))
	sqsClient := sqs.New(sess)
	sqsURL = SQS_QUEUE_URL_STAGING
	if env == "prod" {
		sqsURL = SQS_QUEUE_URL_PROD_FORMAT
	}

	csvFile, _ := os.Open(inputFile)
	csvReader := csv.NewReader(bufio.NewReader(csvFile))
	counter := 0
	batchCounter := 0

	batchesByShard := make(map[string][]*sqs.SendMessageBatchRequestEntry)
	limitersByShard := make(map[string]*rate.Limiter)
	for i := 0; i < 10; i++ {
		shard := strconv.Itoa(i)
		batchesByShard[shard] = make([]*sqs.SendMessageBatchRequestEntry, 0, BATCH_SIZE)
		limitersByShard[shard] = rate.NewLimiter(rate.Limit(300), 300) // 10 messages per batch, 3000 messages per second is limit
	}

	defer flushRemainingBatches(sqsClient, batchesByShard, limitersByShard)

	logrus.Info("starting the process")
	for {
		row, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			logrus.WithError(err).Errorf("Failed to read transactionid at transaction counter: %d", counter)
			return
		}
		if len(row) != 2 {
			logrus.Errorf("transactionid is not present in the input file at transaction counter: %d", counter)
			return
		}

		transactionID := row[0]
		userID := row[1]
		shard := userID[len(userID)-1:]

		transactionIDs := []string{transactionID}
		entry := &sqs.SendMessageBatchRequestEntry{
			Id:             getStrP(uuid.Must(uuid.NewV4()).String()),
			MessageBody:    getSQSBody(transactionIDs, counter),
			MessageGroupId: getStrP("bits-spend-order-" + userID),
		}
		batchesByShard[shard] = append(batchesByShard[shard], entry)
		counter++

		if len(batchesByShard[shard]) == BATCH_SIZE {
			err = sendBatch(sqsClient, batchesByShard[shard], shard, limitersByShard[shard])
			if err != nil {
				logrus.WithError(err).Errorf("failed to send sqs message for shard %s at batch counter: %d", shard, batchCounter)
				return
			}
			batchesByShard[shard] = make([]*sqs.SendMessageBatchRequestEntry, 0, BATCH_SIZE)
			batchCounter++
		}

		if counter%10000 == 0 {
			logrus.Infof("processed %d records and %d batches\n", counter, batchCounter)
		}
	}

	flushRemainingBatches(sqsClient, batchesByShard, limitersByShard)

	logrus.Infof("finished! %d records sent.", counter)
}

func sendBatch(client *sqs.SQS, entries []*sqs.SendMessageBatchRequestEntry, shard string, limiter *rate.Limiter) error {
	_ = limiter.Wait(context.Background())

	var err error
	for i := 0; i < 3; i++ {
		batchSendOutput, err := client.SendMessageBatch(&sqs.SendMessageBatchInput{
			Entries:  entries,
			QueueUrl: &sqsURL,
		})
		if err != nil || len(batchSendOutput.Failed) > 0 {
			for failIndex, fail := range batchSendOutput.Failed {
				logrus.Infof("Failed Message #: %d - %s", failIndex, *fail.Message)
			}

			time.Sleep(100 * time.Millisecond)
		}

		if err == nil {
			return nil
		}
	}

	return err
}

func flushRemainingBatches(client *sqs.SQS, batches map[string][]*sqs.SendMessageBatchRequestEntry, limiters map[string]*rate.Limiter) {
	for shard, entries := range batches {
		if len(entries) > 0 {
			err := sendBatch(client, entries, shard, limiters[shard])
			if err != nil {
				logrus.WithError(err).Errorf("failed to flush remaining unsent batches for shard %s, %+v", shard, batches)
			}
		}
	}
}

func getSQSBody(transactionIDs []string, index int) *string {
	balanceUpdate := BalanceUpdateMessage{
		transactionIDs,
	}
	balanceUpdateJson, err := json.Marshal(&balanceUpdate)
	if err != nil {
		logrus.WithError(err).Panicf("failed to marshal transaction at transaction counter: %d", index)
	}

	snsMessage := SNSMessage{
		MessageID: uuid.Must(uuid.NewV4()).String(),
		Message:   string(balanceUpdateJson),
	}
	snsMessageJson, err := json.Marshal(&snsMessage)
	if err != nil {
		logrus.WithError(err).Panicf("failed to marshal sns message at transaction counter: %d", index)
	}

	return aws.String(string(snsMessageJson))
}

func getStrP(str string) *string {
	return &str
}
