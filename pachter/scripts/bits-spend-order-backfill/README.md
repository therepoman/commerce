# Pachter Bits Spend Order Backfill

This script is to backfill the bits_spends RDS table in Pachter. We need to send the transaction id of every
single bits transaction from the beginning **strictly in chronological order**.

To run this script, follow the next steps:
1. Make sure you are using **Pachter**'s aws credentials on your laptop.
2. Run the sql in this directory to read all transaction ids as well as the corresponding requestingTwitchUserID (up until the specified time) from Redshift and put them in Payday S3 bucket `pachter-spend-order-backfill/dump` as a csv file.
   Before runnign the sql, make sure:
   - the bucket `dump` is empty
   - replace `<access_key_id>` and `<secret_access_key>` in the sql with your **Payday** aws credentials
   - replace the number (Epoch Unix Time) at `timeofevent <= 1465492621` with whatever the date timestamp you desire to get the transactions until.

3. When sql is finished (it will take a while), you should see the output csv file named `000`, `001`, ... under the `dump` folder. Download those files and place them in this directory.

4. Now you can just run this script by `go run main.go -i 000` (change the input file name as necessary). The last thing to make sure is what environment you are doing this (staging/prod). Make sure to chagne the SQS URL:
```
	sqsUrl := SQS_QUEUE_URL_STAGING //for staging	
	// 	sqsUrl := SQS_QUEUE_URL_PROD //for prod
```