package main

import (
	"bufio"
	"context"
	"encoding/csv"
	"encoding/json"
	"io"
	"os"
	"sync"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachter/backend/dynamo/worker_audit"
	"code.justin.tv/commerce/pachter/backend/worker"
	"code.justin.tv/commerce/pachter/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/codegangsta/cli"
	"github.com/gofrs/uuid"
	"golang.org/x/time/rate"
)

const (
	SQS_QUEUE_URL_STAGING = "https://sqs.us-west-2.amazonaws.com/721938063588/bits-transaction-staging"
	SQS_QUEUE_URL_PROD    = "https://sqs.us-west-2.amazonaws.com/458492755823/bits-transaction-prod"
)

var inputFile string
var outputFile string
var env string
var modifyDatabase bool
var auditDAO worker_audit.DAO
var sqsClient *sqs.SQS
var sqsURL string

func main() {
	script := cli.NewApp()
	script.Name = "Pachter Transaction Auditor"
	script.Usage = "Audit Pachter transaction table by syncing with Pachinko"

	script.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "inputFile, i",
			Usage:       "the input file with transaction ids from Pachinko on each line",
			Destination: &inputFile,
		},
		cli.StringFlag{
			Name:        "env, e",
			Usage:       "the environment (either 'staging' or 'prod')",
			Value:       "staging",
			Destination: &env,
		},
		cli.BoolFlag{
			Name:        "modifyDatabase, m",
			Usage:       "'true' to actually modify stuff to sync with Pachinko. 'false' by default, meaning, all the suspicious transactions will simply get printed out on an output file",
			Destination: &modifyDatabase,
		},
		cli.StringFlag{
			Name:        "outputFile, o",
			Usage:       "the outputFile",
			Value:       "suspicious_transactions.csv",
			Destination: &outputFile,
		},
	}

	script.Action = run
	err := script.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Panic("Failed to run this script!")
	}
}

func run(c *cli.Context) {
	ctx := context.Background()

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2"),
	})
	if err != nil {
		logrus.WithError(err).Panic("Failed to initiate aws session")
	}

	environment := "staging"
	sqsURL = SQS_QUEUE_URL_STAGING
	if env == "prod" {
		environment = "prod"
		sqsURL = SQS_QUEUE_URL_PROD
	}
	auditDAO = worker_audit.NewDAO(sess, &config.Config{
		EnvironmentName: environment,
	})
	sqsClient = sqs.New(sess)
	logrus.WithFields(logrus.Fields{
		"Environment": environment,
		"SQS URL":     sqsURL,
	}).Info("Script initiated")

	logrus.Info("Start reading CSV")
	startTime := time.Now()
	csvFile, _ := os.Open(inputFile)
	csvReader := csv.NewReader(bufio.NewReader(csvFile))
	defer func() {
		_ = csvFile.Close()
	}()
	logrus.Infof("CSV read. Took %.2f seconds", time.Since(startTime).Seconds())

	outputFile, err := os.Create(outputFile)
	if err != nil {
		logrus.WithError(err).Panic("could not create output file")
	}
	csvWriter := csv.NewWriter(outputFile)
	defer func() {
		_ = outputFile.Close()
	}()
	defer csvWriter.Flush()

	_ = csvWriter.Write([]string{"transactionID", "transactions", "spend", "cheermote"})
	wg := &sync.WaitGroup{}
	counter := 0
	successCounter := 0
	rateLimiter := rate.NewLimiter(rate.Limit(1000), 1000)
	startTime = time.Now()
	missingTransactions := make([]string, 0)
	for {
		_ = rateLimiter.Wait(context.Background())
		csvRow, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if len(csvRow) < 1 {
			logrus.Warn("found a row with no transaction id")
			continue
		}

		pachinkoTransactionID := csvRow[0]
		wg.Add(1)
		go func() {
			err := syncTransactionWithPachinko(ctx, pachinkoTransactionID, csvWriter)
			if err != nil {
				missingTransactions = append(missingTransactions, pachinkoTransactionID)
				logrus.WithField("pachinkoTransactionID", pachinkoTransactionID).WithError(err).
					Error("failed to process this transaction")
			} else {
				successCounter++
			}

			wg.Done()
		}()

		counter++
		if counter%10000 == 0 {
			logrus.Infof("%d entries started getting processed", counter)
		}
	}
	wg.Wait()

	if counter != successCounter {
		logrus.WithFields(logrus.Fields{
			"Records processed":      counter,
			"Records succeeded":      successCounter,
			"Delta":                  counter - successCounter,
			"Elapsed time (seconds)": time.Since(startTime).Seconds(),
			"Missing Transactions":   missingTransactions,
		}).Error("Script failed")
	} else {
		logrus.WithFields(logrus.Fields{
			"Records processed":      counter,
			"Records succeeded":      successCounter,
			"Elapsed time (seconds)": time.Since(startTime).Seconds(),
		}).Info("Script completed")
	}
}

// This function takes a transaction ID and checks its existence in Pachter's audit Dynamo table,
// and if the audit Dynamo table does not have entry for "transactions" column for this transaction ID,
// that means this transaction is missing from Pachter's RDS bits_transactions table, in which case,
// this function will backfill it by sending the transaction ID to the transaction SQS queue.
func syncTransactionWithPachinko(ctx context.Context, transactionID string, csvWriter *csv.Writer) error {
	entry, err := auditDAO.Get(ctx, transactionID)
	if err != nil {
		return err
	}

	if entry == nil || entry.Transactions == nil || entry.Spend == nil || entry.Cheermote == nil {
		if !modifyDatabase {
			csvEntry := []string{transactionID}
			if entry == nil {
				emptySet := []string{"", "", ""}
				csvEntry = append(csvEntry, emptySet...)
				return csvWriter.Write(csvEntry)
			}

			if entry.Transactions == nil {
				csvEntry = append(csvEntry, "")
			} else {
				csvEntry = append(csvEntry, entry.Transactions.String())
			}

			if entry.Spend == nil {
				csvEntry = append(csvEntry, "")
			} else {
				csvEntry = append(csvEntry, entry.Spend.String())
			}

			if entry.Cheermote == nil {
				csvEntry = append(csvEntry, "")
			} else {
				csvEntry = append(csvEntry, entry.Cheermote.String())
			}

			return csvWriter.Write(csvEntry)
		} else {
			sqsBody, err := getSQSBody(transactionID)
			if err != nil {
				return err
			}

			_, err = sqsClient.SendMessage(&sqs.SendMessageInput{
				MessageBody: sqsBody,
				QueueUrl:    aws.String(sqsURL),
			})
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func getSQSBody(transactionID string) (*string, error) {
	balanceUpdate := worker.BalanceUpdateMessage{
		TransactionIDs: []string{transactionID},
	}
	balanceUpdateJson, err := json.Marshal(&balanceUpdate)
	if err != nil {
		logrus.WithError(err).Error("Could not marshal transaction ids")
		return nil, err
	}

	snsMessage := worker.SNSMessage{
		MessageID: uuid.Must(uuid.NewV4()).String(),
		Message:   string(balanceUpdateJson),
	}
	snsMessageJson, err := json.Marshal(&snsMessage)
	if err != nil {
		logrus.WithError(err).Error("Could not marshal sns message")
		return nil, err
	}

	return aws.String(string(snsMessageJson)), nil
}
