# Bits Transactions Table Audit Script
In the post Andes world, the source of truth when it comes to Bits transaction is Pachinko's
Redshift ledger (think of it as Fabs/BTS in the pre Andes world). However, the Bits payouts
to streamers are based on Pachter's `bits_transactions` table. That said, we really want to make
sure that Pachter's `bits_transactions` table has _all_ the transactions recorded from Pachinko.

This script is used to make sure all the Pachinko transactions are also in Pachter's `bits_transactions`
table (also some other RDS tables in Pachter too). What it does is to compare the transaction ids from
Pachinko Redshift ledger with the audit table in Pachter to see if all transactions/spend order/cheermote
for each transaction is processed or not. This script would output the result into a csv file:

(Order: transaction id, timestamp of transaction, bits spend, cheermote)
```
53a6ba7b-8276-4fce-b123-79b0333f18fe,2019-09-28 11:15:20.07805114 +0000 UTC,,2019-09-28 11:15:20.079484393 +0000 UTC
```
Notice in this case that the third column (the timestamp of Bits spend order calculation) is missing - what it
indicates is that the transaction and cheermote workers have successfully processed this transaction
`53a6ba7b-8276-4fce-b123-79b0333f18fe` but spend order was not calculated successfully. Now that you
know which ids have problems with which RDS table in Pachter - you can investigate/backfill them however
you want.

## How to Run the Script
What you need is:
- AWS credentials of Pachter for the environment you desire
- The transactions ids in csv file, one transaction id per line, from Pachinko for however conditions (by time range, by user, etc)

Once you have them, you can simply run them:
(e.g.)
```
$ AWS_PROFILE=pachter-prod go run scripts/transaction-auditor -e prod -i input.csv
```
By default, the script will try to call Pachter's Dynamo with 1,000 TPS, but the audit table
usually has around 400 as the read capacity. **So make sure to adjust the dynamo capacity first
before running the script**

Also, if you are running this script on your laptop, it would typically run into a problem because
it tries to call a remote host in the default TPS of 1,000 and your laptop reaches the max open file
limit. What I do instead is to run this in the Pachter's jumpbox:
```
yourlaptop: ~$ TC=twitch-pachter-prod ssh jumpbox
[userldap@jumpbox-twitch-pachter-prod ~]$ ./transaction-auditor -e prod -i input.csv
```
You can build the script into an executable file and send it to the remote host before:
```
yourlaptop: ~$ env GOOS=linux GOARCH=amd64 GOARM=7 go build
yourlaptop: ~$ TC=twitch-pachter-prod scp transaction-auditor userldap@jumpbox:/home/userldap/
```

## How to Get the Pachinko Redshift Transaction IDs
You can just go to Redshift console on the Pachinko AWS account and use `prod` as the database,
`admin` as the user, and click "Use Temp password" - that will let you query directly against
the Pachinko ledger data. It would also let you download the output as a csv too.
If your query would result in a huge amount of data, it probably won't finish running or at least
you wouldn't be able to download it as csv. In that case, you can use Redshifts' `unload` command
to write the output data into S3 files that you can download.

e.g.
```postgresql
unload
($$
select distinct
	transaction_id
from
	transactions
where
	domain_name = 'BITS'
and
	timestamp >= 1572591600 -- Friday, November 1, 2019 12:00:00 AM GMT-07:00 DST
and
	timestamp < 1575187200; -- Sunday, December 1, 2019 12:00:00 AM GMT-08:00
$$)
to 's3://ohki-andes-battle/ohki_november_research_'
credentials 'aws_access_key_id=***;aws_secret_access_key=***'
delimiter as ','
parallel off
```
