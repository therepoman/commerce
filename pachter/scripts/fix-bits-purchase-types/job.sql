
CREATE TABLE my_staging_table
    transaction_id varchar(128),
    user_id varchar(32),
    gateway varchar(32),
    payment_gateway varchar(32),
    product_id varchar(32)
);


# see https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/PostgreSQL.Procedural.Importing.html#aws_s3.table_import_from_s3 for docs on this function
SELECT aws_s3.table_import_from_s3(
   'my_staging_table',
   '',
   '(format csv)',
   :'s3://backfill-stuff/payments_purchase_data_final.csv',
   aws_commons.create_aws_credentials('access_key', 'secret_key', '');


# These queries take the above data and update needed rows
UPDATE bits_transactions
SET bits_type = CASE
	WHEN my_staging_table.product_id = 'B017L2UX4C' then 'CB_140_CBP_00140_XSOLLA'
	WHEN my_staging_table.product_id = 'B018WMZKR0' then 'CB_700_CBP_00140_XSOLLA'
	WHEN my_staging_table.product_id = 'B018WMZN5E' then 'CB_1995_CBP_00133_XSOLLA
'	WHEN my_staging_table.product_id = 'B018WMZSPY' then 'CB_6440_CBP_00129_XSOLLA'
	WHEN my_staging_table.product_id = 'B018WMZYPI' then 'CB_12600_CBP_00126_XSOLLA'
	WHEN my_staging_table.product_id = 'B01G4BISOS' then 'CB_30800_CBP_00120_XSOLLA'
	WHEN my_staging_table.product_id = 'B073RTRHTZ' then 'CB_1000_CBP_00100_XSOLLA'
	WHEN my_staging_table.product_id = 'B019WMZ15D' then 'CB_300_CBP_00100_XSOLLA'

FROM bits_transactions, my_staging_table
WHERE datetime > '2020-02-01'
AND datetime < '2020-05-02'
AND my_staging_table.transaction_id = bits_transactions.transaction_id
AND my_staging_table.payment_gateway = 'xsolla_v3';

UPDATE bits_transactions
SET bits_type = CASE
	WHEN my_staging_table.product_id = 'B017L2UX4C' then 'CB_140_CBP_00140_PAYPAL'
	WHEN my_staging_table.product_id = 'B018WMZKR0' then 'CB_700_CBP_00140_PAYPAL'
	WHEN my_staging_table.product_id = 'B018WMZN5E' then 'CB_1995_CBP_00133_PAYPAL'
	WHEN my_staging_table.product_id = 'B018WMZSPY' then 'CB_6440_CBP_00129_PAYPAL'
	WHEN my_staging_table.product_id = 'B018WMZYPI' then 'CB_12600_CBP_00126_PAYPAL'
	WHEN my_staging_table.product_id = 'B01G4BISOS' then 'CB_30800_CBP_00120_PAYPAL'
	WHEN my_staging_table.product_id = 'B073RTRHTZ' then 'CB_1000_CBP_00100_PAYPAL'
	WHEN my_staging_table.product_id = 'B019WMZ15D' then 'CB_300_CBP_00100_PAYPAL'

FROM bits_transactions, my_staging_table
WHERE datetime > '2020-02-01'
AND datetime < '2020-05-02'
AND my_staging_table.transaction_id = bits_transactions.transaction_id
AND my_staging_table.gateway = 'recurly'
AND my_staging_table.payment_gateway = 'braintree';

UPDATE bits_transactions
SET bits_type = CASE
	WHEN my_staging_table.product_id = 'B017L2UX4C' then 'CB_140_CBP_00140'
	WHEN my_staging_table.product_id = 'B018WMZKR0' then 'CB_700_CBP_00140'
	WHEN my_staging_table.product_id = 'B018WMZN5E' then 'CB_1995_CBP_00133'
	WHEN my_staging_table.product_id = 'B018WMZSPY' then 'CB_6440_CBP_00129'
	WHEN my_staging_table.product_id = 'B018WMZYPI' then 'CB_12600_CBP_00126'
	WHEN my_staging_table.product_id = 'B01G4BISOS' then 'CB_30800_CBP_00120'
	WHEN my_staging_table.product_id = 'B073RTRHTZ' then 'CB_1000_CBP_00100'
	WHEN my_staging_table.product_id = 'B019WMZ15D' then 'CB_300_CBP_00100'

FROM bits_transactions, my_staging_table
WHERE datetime > '2020-02-01'
AND datetime < '2020-05-02'
AND my_staging_table.transaction_id = bits_transactions.transaction_id
AND my_staging_table.gateway = 'recurly'
AND my_staging_table.payment_gateway = 'amazon';

UPDATE bits_transactions
SET bits_type = CASE
	WHEN my_staging_table.product_id = 'B017L2UX4C' then 'CB_140_CBP_00140_VANTIV'
	WHEN my_staging_table.product_id = 'B018WMZKR0' then 'CB_700_CBP_00140_VANTIV'
	WHEN my_staging_table.product_id = 'B018WMZN5E' then 'CB_1995_CBP_00133_VANTIV'
	WHEN my_staging_table.product_id = 'B018WMZSPY' then 'CB_6440_CBP_00129_VANTIV'
	WHEN my_staging_table.product_id = 'B018WMZYPI' then 'CB_12600_CBP_00126_VANTIV'
	WHEN my_staging_table.product_id = 'B01G4BISOS' then 'CB_30800_CBP_00120_VANTIV'
	WHEN my_staging_table.product_id = 'B073RTRHTZ' then 'CB_1000_CBP_00100_VANTIV'
	WHEN my_staging_table.product_id = 'B019WMZ15D' then 'CB_300_CBP_00100_VANTIV'

FROM bits_transactions, my_staging_table
WHERE datetime > '2020-02-01'
AND datetime < '2020-05-02'
AND my_staging_table.transaction_id = bits_transactions.transaction_id
AND my_staging_table.gateway = 'recurly'
AND my_staging_table.payment_gateway = 'vantiv';


UPDATE bits_transactions
SET bits_type = CASE
	WHEN my_staging_table.product_id = 'B017L2UX4C' then 'CB_140_CBP_00140_STRIPE'
	WHEN my_staging_table.product_id = 'B018WMZKR0' then 'CB_700_CBP_00140_STRIPE'
	WHEN my_staging_table.product_id = 'B018WMZN5E' then 'CB_1995_CBP_00133_STRIPE'
	WHEN my_staging_table.product_id = 'B018WMZSPY' then 'CB_6440_CBP_00129_STRIPE'
	WHEN my_staging_table.product_id = 'B018WMZYPI' then 'CB_12600_CBP_00126_STRIPE'
	WHEN my_staging_table.product_id = 'B01G4BISOS' then 'CB_30800_CBP_00120_STRIPE'
	WHEN my_staging_table.product_id = 'B073RTRHTZ' then 'CB_1000_CBP_00100_STRIPE'
	WHEN my_staging_table.product_id = 'B019WMZ15D' then 'CB_300_CBP_00100_STRIPE'
FROM bits_transactions, my_staging_table
WHERE datetime > '2020-02-01'
AND datetime < '2020-05-02'
AND my_staging_table.transaction_id = bits_transactions.transaction_id
AND my_staging_table.gateway = 'recurly'
AND my_staging_table.payment_gateway = 'stripe';


