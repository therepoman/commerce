query=$1
csvname="$query.csv"
queryfile="$query.sql"
temp=temp.sql

function main() {
  echo "You are about to run the following query against Pachter Prod RDS:"
  echo -------------------------------
  cat $queryfile
  echo ""
  echo -------------------------------
  read -p "Does the above look right? (y/n) " ans
  if [ $ans == 'y' ]; then
    build_temp_query
    start_remote_execution
    clean_csv
    echo "Finished, if successful, query result is saved to $csvname"
    read -p "Open saved CSV? (y/n) " opencsv
    if [ $opencsv == 'y' ]; then
      open "$csvname"
    fi
  fi
}

function build_temp_query() {
  # just build a temp query file with the csv export decoration, this temp file is what will be SCP'd to Pachter
  query="$(cat $queryfile)"
  echo "\copy ($query) to './$csvname' csv header;" > "$temp"
  # psql likes queries in a single line
  echo -n "$(tr '\n' ' ' < $temp)" > $temp
}

function start_remote_execution() {
  cmd="psql -h rds-instance-pachter-production.cyp8xvcbw7cz.us-west-2.rds.amazonaws.com -p 5432 -U pachter_production PachterProduction"
  echo "Enter Pachter Prod RDS Password (Hidden)"
  read -s pw
  TC=twitch-pachter-prod scp ./$temp jumpbox:
  TC=twitch-pachter-prod ssh jumpbox "PGPASSWORD=$pw $cmd -f $temp"
  TC=twitch-pachter-prod scp jumpbox:$csvname $csvname
  TC=twitch-pachter-prod ssh jumpbox "rm $temp && rm $csvname"
  rm $temp
}

function clean_csv() {
  # removes leading/trailing spaces around comma
  sed -i.bak -E 's/(^|,)[[:blank:]]+/\1/g; s/[[:blank:]]+(,|$)/\1/g' $csvname
  rm *.bak
}

main