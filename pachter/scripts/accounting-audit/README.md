# Accounting Audit Script

For SOX Compliance, Accounting needs to audit how we store data in Pachter. Because we need to do this multiple times and we may have to do this in the future, I'm putting it in a script and documenting how it can be used.

You create a `.sql` file (say `cool-query.sql`) that you want to execute.

Example
```
SELECT * FROM bits_transactions
    WHERE transaction_type = 'UseBitsOnExtension'
    AND datetime >= TO_DATE('2020-10-4', 'YYYY-MM-DD')
    AND datetime <= TO_DATE('2020-10-10', 'YYYY-MM-DD')
    ORDER BY transaction_id LIMIT 10
```

Then you just execute the script `bash remote_exec.sh cool-query`, it will prompt you for DB password (see [README](https://git.xarth.tv/commerce/pachter#rds)), it will run it and download the csv to a `cool-query.csv` file.

This can also be helpful for oncall tasks, though it might be easier to do things in the psql shell when you don't know exactly what queries to run.
