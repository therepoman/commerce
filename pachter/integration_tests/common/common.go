package common

import (
	"net/http"
	"os"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/pachter/config"
	pachter "code.justin.tv/commerce/pachter/rpc"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
)

func GetPachterClient() pachter.Pachter {
	envVar := os.Getenv(config.EnvironmentVariable)
	if envVar != "" {
		log.Infof("Found ENVIRONMENT environment variable: %s", envVar)
	}

	env := config.GetOrDefaultEnvironment(envVar)

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		log.WithError(err).Panic("Error loading config")
	}

	log.Infof("Loaded config: %s", cfg.EnvironmentName)

	if env == config.Prod {
		log.Panic("Please don't run these integration tests against prod!")
	}

	s2sConfig := caller.Config{
		DisableStatsClient: true,
	}

	httpClient := http.DefaultClient
	if cfg.S2SEnabled {
		rt, err := caller.NewRoundTripper(cfg.S2SServiceName, &s2sConfig, log.New())
		if err != nil {
			log.Panicf("Failed to create s2s round tripper: %v", err)
		}

		httpClient = &http.Client{
			Transport: rt,
		}
	}
	return pachter.NewPachterProtobufClient(cfg.PachterEndpoint, httpClient)
}
