// +build integration

package actuals_test

import (
	"context"
	"math/rand"
	"testing"

	"code.justin.tv/commerce/pachter/integration_tests/common"
	pachter "code.justin.tv/commerce/pachter/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestActuals(t *testing.T) {
	ctx := context.Background()

	Convey("Given a pachter client", t, func() {
		pachterClient := common.GetPachterClient()

		Convey("test the get actuals endpoint before doing anything", func() {
			resp, err := pachterClient.GetActuals(ctx, &pachter.GetActualsReq{
				Platform: pachter.Platform_AMAZON,
				Month:    1,
				Year:     2019,
			})

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.Actuals, ShouldNotBeEmpty)
		})

		Convey("test the set actuals endpoint", func() {
			year := uint32(rand.Int() + 2015)
			month := uint32(7)

			sku := "B077H4S2KH"
			actualValue := uint32(877)
			platform := pachter.Platform_PAYPAL

			resp, err := pachterClient.SetActuals(ctx, &pachter.SetActualsReq{
				Platform: platform,
				Month:    month,
				Year:     year,
				Actuals: map[string]uint32{
					sku: actualValue,
				},
			})

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)

			Convey("test it actually set the thing", func() {
				resp, err := pachterClient.GetActuals(ctx, &pachter.GetActualsReq{
					Platform: platform,
					Month:    month,
					Year:     year,
				})

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.Actuals, ShouldNotBeEmpty)
				So(resp.Actuals, ShouldContainKey, sku)
				So(resp.Actuals[sku], ShouldEqual, actualValue)
			})

		})
	})
}
