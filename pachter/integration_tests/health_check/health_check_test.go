// +build integration

package health_check_test

import (
	"context"
	"testing"

	"code.justin.tv/commerce/pachter/integration_tests/common"
	pachter "code.justin.tv/commerce/pachter/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestHealthCheck(t *testing.T) {
	Convey("Given a pachter client", t, func() {
		pachterClient := common.GetPachterClient()

		Convey("Test the health check endpoint", func() {
			_, err := pachterClient.HealthCheck(context.Background(), &pachter.HealthCheckReq{})
			So(err, ShouldBeNil)
		})
	})
}
