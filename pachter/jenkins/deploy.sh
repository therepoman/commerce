#!/bin/bash

set -e

DEPLOYER_VERSION="5.0.0"

if [ -z "$TASK" ]
then
    echo "$$TASK is required!"
    exit 1
fi

if [ -z "$SERVICE" ]
then
    echo "$$SERVICE is required!"
    exit 1
fi

if [ -z "$SHA" ]
then
    echo "$$SHA is required!"
    exit 1
fi

if [ -z "$CLUSTER" ]
then
    echo "$$CLUSTER is required!"
    exit 1
fi

if [ -z "$REGION" ]
then
    echo "$$REGION is required!"
    exit 1
fi

if [ -z "$AWS_PROFILE" ]
then
    echo "$$AWS_PROFILE is required!"
    exit 1
fi

echo "Deploying to $ENVIRONMENT..."

docker run \
 -e AWS_ACCESS_KEY=$(aws configure get aws_access_key_id --profile $AWS_PROFILE) \
 -e AWS_SECRET_KEY=$(aws configure get aws_secret_access_key --profile $AWS_PROFILE) \
 docker-registry.internal.justin.tv/subs/deployer:$DEPLOYER_VERSION \
    -service=$SERVICE \
    -cluster=$CLUSTER \
    -task=$TASK \
    -region=$REGION \
    -tag=$SHA
