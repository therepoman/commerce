#!/bin/bash -e
set -o xtrace

pip3 config set global.index-url https://pypi.pkgs.xarth.tv/simple
npm config set registry https://npm.pkgs.xarth.tv/

npm install -g aws-cdk@1.45.0
pip3 install -r requirements.txt

if [[ "$ENV" != "prod" ]]; then
  npm install -g cdk-plugin-isengard
  pip3 install -r requirements-dev.txt
fi
