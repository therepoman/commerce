import json
import os
from pprint import pprint

from boto3.session import Session

# Create lambda client with temp credentials provided by Data Infrastructure
# These were provided to you in the "Register a Producer" step.
session = Session(
    aws_access_key_id=os.getenv("AWS_ACCESS_KEY_ID"),
    aws_secret_access_key=os.getenv("AWS_SECRET_ACCESS_KEY"),
    aws_session_token=os.getenv("AWS_SESSION_TOKEN"),
)
client = session.client("lambda", region_name="us-west-2")

# CHANGEME Enter your producer attributes here
body = {
    "application_name": "commercescienceaws",  # Format: ^[a-z][a-z0-9]{2,47}$
    "bindle_id": "amzn1.bindle.resource.c5727hl6bcoaywfxo5ga",  # Format: amzn1.bindle.resource.<20-alphanum>
    "principal_role": "arn:aws:iam::348802193101:root",  # Format: arn:aws:iam::<aws_account_id>:<user|group|role>
}

# Configure producer registration request
registration_arn = (
    "arn:aws:lambda:us-west-2:331582574546:function:RegistrationLambdaFunction"
)
payload = {
    "httpMethod": "POST",
    "path": "/twirp/twitch.fulton.example.twitchtahoeapiservice.TwitchTahoeAPIService/RegisterProducer",
    "headers": {"Content-Type": "application/json"},
    "body": json.dumps(body),
}

# Invoke lambda to register new producer
response = client.invoke(
    FunctionName=registration_arn,
    InvocationType="RequestResponse",
    Payload=json.dumps(payload),
)
result = json.loads(response.get("Payload").read())

# Display producer attributes cleanly on success, otherwise dump response
if result["statusCode"] == 200:
    pprint(json.loads(result["body"]))
    print("Successfully created producer")
else:
    pprint(result)
    print("Failed to create producer")

"""
Key is stored in secret manager under tahoe_producer_key

Response:
{'application_name': 'commercescienceaws',
 'backup_stack_name': 'producer-backup-commercescienceaws',
 'bindle_id': 'amzn1.bindle.resource.c5727hl6bcoaywfxo5ga',
 'create_time': '2019-11-04T21:56:01.414230938Z',
 'iam_role': 'arn:aws:iam::331582574546:role/producer-commercescienceaws',
 'id': 'producer_tgazjbkqdgvqtqrpvwzqvkngyshajbuelcdcososhgbgncjueuamgkkunivkyqoz',
 'principal_role': 'arn:aws:iam::348802193101:root',
 's3_backup_bucket': 'backup-commercescienceaws-331582574546',
 's3_dest_bucket': 'commercescienceaws-331582574546',
 's3_input_uri': 's3://tahoe-input-331582574546/commercescienceaws/',
 's3_log_uri': 's3://tahoe-logs-331582574546/commercescienceaws/',
 'schema': 'commercescienceaws',
 'sensitive_types': {'IP': {'encryption_key_arn': 'arn:aws:kms:us-west-2:331582574546:key/2ac53517-0189-4562-ad53-857c1aa7087a',
                            'salt_parameter_arn': 'arn:aws:secretsmanager:us-west-2:331582574546:secret:IPSalt-JZVXNi'},
                     'OTHERID': {'encryption_key_arn': 'arn:aws:kms:us-west-2:331582574546:key/66d591ad-3929-4282-83d0-3a9ff65c9e75',
                                 'salt_parameter_arn': 'arn:aws:secretsmanager:us-west-2:331582574546:secret:OTHERIDSalt-snpw7B'},
                     'PAYOUT': {'encryption_key_arn': 'arn:aws:kms:us-west-2:331582574546:key/f7102a51-d385-460a-9d8d-8c65d073afe2',
                                'salt_parameter_arn': 'arn:aws:secretsmanager:us-west-2:331582574546:secret:PAYOUTSalt-b9o9sK'},
                     'SESSIONID': {'encryption_key_arn': 'arn:aws:kms:us-west-2:331582574546:key/d4d4ae19-b4d6-4bc7-98ac-3841bc5ef8b7',
                                   'salt_parameter_arn': 'arn:aws:secretsmanager:us-west-2:331582574546:secret:SESSIONIDSalt-07A43Y'},
                     'USERID': {'encryption_key_arn': 'arn:aws:kms:us-west-2:331582574546:key/d9275fc6-71a1-4233-806d-5bc332cb3383',
                                'salt_parameter_arn': 'arn:aws:secretsmanager:us-west-2:331582574546:secret:USERIDSalt-ZyIugA'},
                     'USERTEXT': {'encryption_key_arn': 'arn:aws:kms:us-west-2:331582574546:key/045fab1f-d4d8-4e01-9736-481fe1ba8da8',
                                  'salt_parameter_arn': 'arn:aws:secretsmanager:us-west-2:331582574546:secret:USERTEXTSalt-gAZuZF'}},
 'stack_name': 'producer-commercescienceaws'}
"""
