# USAGE: PYTHONPATH=.:$PYTHONPATH python3 bin/backfill.py

import datetime
from time import sleep

import boto3
from mantra.plugins.hooks.aws_hook import get_aws_session

import workflows.lib.utils as utils
import workflows.lib.vantiv_chargeback as cb

boto3.setup_default_session(profile_name="twitch-commerce-science-aws")
session = get_aws_session()
s3 = session.client("s3")
api_key = utils.get_payments_producer_key(session)

start_date = datetime.date(2021, 7, 6)
end_date = datetime.date(2021, 9, 1)

current_date = start_date

while current_date != end_date:
    date_str = current_date.strftime("%Y-%m-%d")
    print(f"backfilling {date_str}...")
    response = s3.list_objects_v2(
        Bucket="commercescienceaws-tahoe-export",
        Prefix=f"prod/payment_chargebacks_vantiv/date={date_str}/",
    )
    if response["KeyCount"] == 0:
        print(f"no keys for {date_str}")
        current_date = current_date + datetime.timedelta(days=1)
        continue

    path_to_parquet = response["Contents"][0]["Key"]

    job_id = utils.copy_partition_tahoe(
        api_key,
        "payment_chargebacks_vantiv",
        3,
        cb.table_column_definitions,
        f"s3://commercescienceaws-tahoe-export/{path_to_parquet}",
        date_str,
    )

    print(f"Submitted {job_id}...")
    status = utils.check_tahoe_job(session, job_id)
    print(f"Job {job_id} status: {status}\n")
    if status != "SUCCESS":
        exit(1)

    current_date = current_date + datetime.timedelta(days=1)

