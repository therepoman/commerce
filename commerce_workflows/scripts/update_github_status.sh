#!/bin/bash -e
set -o pipefail
STATUS=$1
URL=$2
CONTEXT=${3:-"mantra-ci-pipeline"}
echo $CONTEXT
CONTENT="{\"state\": \"${STATUS}\", \"context\": \"${CONTEXT}\", \"target_url\": \"${URL}\"}"
echo "Posting '${STATUS}' status to GitHub"
curl -X POST \
  -H "Content-Type: application/json" \
  -d "${CONTENT}" \
  "https://$GITHUB_TOKEN@git.xarth.tv/api/v3/repos/$REPO_ORG/$REPO_NAME/statuses/$COMMIT_ID"
