import typing

from aws_cdk import aws_ec2 as ec2
from aws_cdk import aws_ecr as ecr
from aws_cdk import aws_iam as iam
from aws_cdk import aws_sagemaker as sagemaker
from aws_cdk import aws_route53 as route53
from aws_cdk import core

from mantra.workflow.stack.base_stack import BaseStack


class CustomStack(BaseStack):

    # stack name that will show up in CloudFormation Console. Prefix the cli_name with `app_name`, or `resource_prefix`
    @classmethod
    def cf_stack_name(cls, profile_params, extra_args: typing.List[str]):
        app_name = profile_params["app_name"].replace("_", "-")
        return f"{app_name}-custom"

    def __init__(self, cdk_app: core.App, id: str, profile_params, extra_args: typing.List[str], **kwargs):
        super().__init__(cdk_app, id, profile_params, **kwargs)
        app_name = profile_params["app_name"].replace("_", "-")

        endpoints = {
            "personalization-prod": "com.amazonaws.vpce.us-west-2.vpce-svc-0637d9166777dc9da",
            "personalization-dev": "com.amazonaws.vpce.us-west-2.vpce-svc-0fae49be5957f8db3",
        }

        for id, service_name in endpoints.items():
            ec2.CfnVPCEndpoint(
                scope=self,
                id=id,
                service_name=service_name,
                subnet_ids=[
                    profile_params["network"]["vpc_private_subnet_1"],
                    profile_params["network"]["vpc_private_subnet_2"],
                    profile_params["network"]["vpc_private_subnet_3"]
                ],
                security_group_ids=[
                    profile_params["network"]["sg"]
                ],
                vpc_id=profile_params["network"]["vpc_id"],
                vpc_endpoint_type="Interface",
                private_dns_enabled=False,
            )

        internal_hosted_zone = route53.HostedZone(
            scope=self, id="internal-hosted-zone", zone_name="s.twitch.a2z.com"
        )
        internal_hosted_zone.add_vpc(self.resources.vpc)

        route53.RecordSet(
            scope=self,
            id="git-record-set",
            zone=internal_hosted_zone,
            record_name="us-west-2.prod.twitchpersonalization.s.twitch.a2z.com",
            record_type=route53.RecordType.CNAME,
            ttl=core.Duration.minutes(5),
            target=route53.RecordTarget.from_values(
                "vpce-0e9b671692b60a0f8-0nklm7hr.vpce-svc-0637d9166777dc9da.us-west-2.vpce.amazonaws.com"
            ),
        )

        iam.Role(
            scope=self,
            id="personalization-producer-role-dev",
            assumed_by=iam.AccountRootPrincipal(),
            role_name=f'{app_name}-personalization-producer-dev',
            managed_policies=[
                iam.ManagedPolicy.from_aws_managed_policy_name(
                    managed_policy_name="AmazonS3FullAccess"
                )
            ]
        )

        iam.Role(
            scope=self,
            id="personalization-producer-role-prod",
            assumed_by=iam.AccountRootPrincipal(),
            role_name=f'{app_name}-personalization-producer-prod',
            managed_policies=[
                iam.ManagedPolicy.from_aws_managed_policy_name(
                    managed_policy_name="AmazonS3FullAccess"
                )
            ]
        )
