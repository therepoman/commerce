import datetime
import json
import os
import time
from time import sleep

import boto3
import dateutil.parser
import pyarrow as pa

STACK_ACCOUNT_ID = 331582574546  # tahoe prod
TAHOE_SERVICE = (
    "/twirp/twitch.fulton.example.twitchtahoeapiservice.TwitchTahoeAPIService"
)
TAHOE_REGISTRATION_LAMBDA = (
    "arn:aws:lambda:us-west-2:331582574546:function:RegistrationLambdaFunction"
)
TAHOE_PRODUCER_LAMBDA = (
    "arn:aws:lambda:us-west-2:331582574546:function:ProducerLambdaFunction"
)
app_name = "commercescienceaws"
tahoe_producer_role = "arn:aws:iam::331582574546:role/producer-commercescienceaws"
tahoe_producer = {
    "s3_input_uri": "s3://tahoe-input-331582574546/commercescienceaws/",
    "s3_dest_bucket": "commercescienceaws-331582574546",
}


def assume_role_chain(
    role_arn,
    role_session_name=None,
    sts_assumed_role=None,
    region_name="us-west-2",
    endpoint_url="https://sts.us-west-2.amazonaws.com",
):
    """
    Assumes a new role with the option to assume another role from an already assumed role
    :param role_arn: name of the role for example arn:aws:iam::345253255562:role/xxxx"
    :type role_arn: str
    :param role_session_name: name of session you want
    :type role_session_name: str
    :param sts_assumed_role: optional assumed role if you want to assume a new role form a current assumed
    role
    :type sts_assumed_role: boto3.Session
    :param region_name: region_name string
    :param endpoint_url : url of sts endpoint
    :return: new assumed role
    :rtype: boto3.Session
    """
    if role_session_name is None:
        role_session_name = str(int(time.time()))
    if sts_assumed_role:
        response = sts_assumed_role.client(
            "sts", region_name=region_name, endpoint_url=endpoint_url
        ).assume_role(RoleArn=role_arn, RoleSessionName=role_session_name)
    else:
        response = boto3.client(
            "sts", region_name=region_name, endpoint_url=endpoint_url
        ).assume_role(RoleArn=role_arn, RoleSessionName=role_session_name)

    credentials = response["Credentials"]

    assumed_session = boto3.Session(
        aws_access_key_id=credentials["AccessKeyId"],
        aws_secret_access_key=credentials["SecretAccessKey"],
        aws_session_token=credentials["SessionToken"],
    )
    sts_assumed_role = assumed_session.client(
        "sts", region_name=region_name, endpoint_url=endpoint_url
    )

    # print("Assumed Role Identity: " + sts_assumed_role.get_caller_identity()["Arn"])

    return assumed_session


def get_secret(session, key):
    return (
        session.client("secretsmanager")
        .get_secret_value(SecretId=key)
        .get("SecretString")
    )


def flatten_dict(d, sep="_"):
    items = []
    for k, v in d.items():
        for item in v:
            new_value = k + sep + item
            items.append(new_value)
    return items


def arr_to_dict(line, headers):
    result = {}
    for i in range(len(headers)):
        result[headers[i]] = line[i]
    return result


# get nested value from dict of dicts, with depth of 1 or 2
def get_value(obj, path):
    parts = path.split(".")
    if type(obj) is dict:
        nest1 = obj.get(parts[0])
    else:
        nest1 = getattr(obj, parts[0])
    if len(parts) == 1:
        return nest1

    if type(obj) is dict:
        return nest1.get(parts[1])
    else:
        return getattr(nest1, parts[1])


# write_field takes a field from obj and return value
def write_field(column, obj):
    api_field_name = column.get("api_field") or column.get("name")
    field_type = column.get("field_type") or "string"
    tahoe_field_type = column.get("type")
    # hack to get the start date in
    if field_type == "current_date":
        field_value = datetime.datetime.now(datetime.timezone.utc)
        field_type = "date"
    else:
        field_value = get_value(obj, api_field_name)

    if field_value is None or field_value == "":
        return None

    if field_type == "string":
        if tahoe_field_type == "TIMESTAMP":
            return dateutil.parser.parse(field_value).strftime("%Y-%m-%dT%H:%M:%SZ")
        elif tahoe_field_type == "DATE":
            return dateutil.parser.parse(field_value).strftime("%Y-%m-%d")
        elif tahoe_field_type == "LONG" and not field_value.isnumeric():
            return "0"
        else:
            # fix for vantiv
            return field_value.replace("'", "")
    # Dump list fields into JSON since we need a defined # of columns for Glue tables
    elif field_type == "list":
        if isinstance(field_value, list):
            return json.dumps([vars(obj) for obj in field_value], default=str)
        else:
            return json.dumps(vars(field_value), default=str)
    elif field_type == "date":
        if tahoe_field_type == "TIMESTAMP":
            return field_value.strftime("%Y-%m-%dT%H:%M:%SZ")
        else:
            return field_value.strftime("%Y-%m-%d")
    elif field_type == "intunixtimestamp":
        return datetime.datetime.fromtimestamp(field_value).strftime(
            "%Y-%m-%dT%H:%M:%SZ"
        )
    else:
        print("unsupported field", field_type)
        exit(1)


def generate_tahoe_request(method, body):
    """Generate the payload of a Tahoe request."""
    return {
        "httpMethod": "POST",
        "path": "{}/{}".format(TAHOE_SERVICE, method),
        "headers": {"Content-Type": "application/json"},
        "body": json.dumps(body),
    }


def call_tahoe(session, tahoe_lambda, payload):
    """Invoke a Tahoe Lambda and return the parsed response."""
    lambda_client = session.client("lambda", region_name="us-west-2")
    resp = lambda_client.invoke(FunctionName=tahoe_lambda, Payload=json.dumps(payload))
    r_payload = resp["Payload"].read()
    if resp["StatusCode"] >= 300:
        raise RuntimeError(
            "Response code {} from {}: {}".format(
                resp["StatusCode"], payload["path"], r_payload
            )
        )
    r_payload = json.loads(r_payload)
    if r_payload["statusCode"] >= 300:
        raise RuntimeError(
            "Response code {} from {}: {}".format(
                resp["StatusCode"], payload["path"], r_payload
            )
        )
    return json.loads(r_payload["body"])


# map_schema returns pandas schema for each Redshift data type
def map_schema(table_column_definitions):
    schema_map = {
        "BOOL": "bool",
        "STRING": "str",
        "DOUBLE": "float64",
        "INT": "int64",
        "TIMESTAMP": "str",
        "LONG": "int64",
        "DATE": "str",
    }

    return {
        column["name"]: schema_map[column["type"]]
        for column in table_column_definitions
    }


def map_schema_pyarrow(table_column_definitions):
    schema_map = {
        "BOOL": pa.bool_(),
        "STRING": pa.string(),
        "DOUBLE": pa.float64(),
        "INT": pa.int32(),
        "TIMESTAMP": pa.timestamp("ms"),
        "LONG": pa.int64(),
        "DATE": pa.date64(),
    }

    return pa.schema(
        [
            pa.field(column["name"], schema_map[column["type"]])
            for column in table_column_definitions
        ]
    )


def map_date_columns(table_column_definitions):
    return [
        column["name"]
        for column in table_column_definitions
        if column["type"] == "TIMESTAMP" or column["type"] == "DATE"
    ]


# copy_prefix copies all files in one s3 bucket to destination
def copy_prefix(session, src_uri, dest_uri):
    s3_resource = session.resource("s3")
    src_bucket_name, src_prefix = src_uri[5:].split("/", 1)
    dest_bucket_name, dest_prefix = dest_uri[5:].split("/", 1)

    src_bucket = s3_resource.Bucket(src_bucket_name)
    dst_bucket = s3_resource.Bucket(dest_bucket_name)

    for obj in src_bucket.objects.filter(Prefix=src_prefix):
        file_name = obj.key.split("/")[-1]
        print(
            f"copy {src_bucket_name}/{obj.key} to {dest_bucket_name}/{dest_prefix}/{file_name}"
        )
        dst_bucket.copy(
            {"Bucket": src_bucket_name, "Key": obj.key}, f"{dest_prefix}/{file_name}"
        )


def upload_folder_to_s3(session: boto3.Session, local_folder: str, dest_uri: str):
    dest_bucket_name, dest_prefix = dest_uri[5:].split("/", 1)
    client = session.client("s3")
    for root, dirs, files in os.walk(local_folder):
        for filename in files:
            local_path = os.path.join(root, filename)
            s3_path = os.path.join(dest_prefix, filename)
            print("uploading", local_path, " to ", s3_path)
            client.upload_file(local_path, dest_bucket_name, s3_path)


# get_payments_producer_key returns tahoe producer API key
def get_payments_producer_key(session):
    return get_secret(session, "tahoe_producer_key")


# copy_partition_tahoe calls tahoe API with partition request
# https://data.xarth.tv/tahoe_producers/dag_catalogue.html#parquet_partition
def copy_partition_tahoe(
    api_key, table_name, version, table_column_definitions, input_s3_uri, date
):
    dag_name = "parquet_partition"
    identifier = str(int(time.time()))

    tahoe_input_path = "{}{}/{}/{}".format(
        tahoe_producer["s3_input_uri"], table_name, date, identifier
    )
    table_name_and_version = "{}_v{}".format(table_name, str(version))

    session = assume_role_chain(tahoe_producer_role)
    copy_prefix(session, input_s3_uri, tahoe_input_path)

    # Configure RunDag request body with DAG parameters
    body = {
        "api_key": api_key,
        "dag_name": dag_name,
        "parameters": [
            {"name": "S3_SOURCE_PATH", "value_string": "{}/".format(tahoe_input_path)},
            {
                "name": "S3_DESTINATION_PATH",
                "value_string": "s3://{}/{}/{}/{}/".format(
                    tahoe_producer["s3_dest_bucket"],
                    table_name_and_version,
                    date,
                    identifier,
                ),
            },
            {
                "name": "COLUMN_DEFINITIONS",
                "value_column_definitions": [
                    dict(
                        name=column["name"],
                        type=column["type"],
                        sensitivity=column["sensitivity"],
                    )
                    for column in table_column_definitions
                ],
            },
            {"name": "NUM_FILE_PARTITIONS", "value_int": 1},
            {"name": "TABLE_NAME", "value_string": table_name_and_version},
            {"name": "TABLE_SCHEMA", "value_string": app_name},
            {
                "name": "S3_TABLE_PATH",
                "value_string": "s3://{}/{}/".format(
                    tahoe_producer["s3_dest_bucket"], table_name_and_version
                ),
            },
            {
                "name": "PARTITION_KEY_VALUES",
                "value_partition_key_values": [
                    {"key": {"name": "download_day", "type": "DATE"}, "value": date}
                ],
            },
        ],
    }

    payload = generate_tahoe_request("RunDag", body)
    response = call_tahoe(session, TAHOE_PRODUCER_LAMBDA, payload)

    # Display job information
    print(
        "job_id: {}\n start_time: {}\n status: {}\n logs: {}".format(
            response["job_id"],
            response["start_time"],
            response["status"],
            response["log_location"],
        )
    )
    return response["job_id"]


check_retries = 30
polling_interval_in_seconds = 30


def check_tahoe_job(session, job_id):
    session = assume_role_chain(tahoe_producer_role, None, session)

    body = {"job_id": job_id}
    for _ in range(check_retries):
        # Invoke QueryJobs with parameters
        payload = generate_tahoe_request("QueryJobs", body)
        response = call_tahoe(session, TAHOE_PRODUCER_LAMBDA, payload)
        status = response["jobs"][0]["status"]
        if status == "SUCCESS" or status == "FAILED" or status == "CANCELLED":
            return status

        sleep(polling_interval_in_seconds)

    return status

