import base64
import csv
import os
import time
from datetime import datetime, timedelta

import pandas as pd
import paramiko
import pyarrow as pa
import pyarrow.parquet as pq
import pysftp
import pytz
from mantra.plugins.hooks.aws_hook import get_aws_session

import workflows.lib.utils as utils

provider = "vantiv"
chargeback_file_name = f"{provider}_report.csv"
local_folder = "/tmp/parquet-output"
output_bucket = "chargeback-vantiv"
tahoe_export_bucket = "commercescienceaws-tahoe-export"
tahoe_table = f"payment_chargebacks_{provider}"

# TODO - support start/end date arg so this loops through daily reports in FTP folder
yesterday = datetime.now(pytz.timezone("US/Pacific")) - timedelta(1)
start_date_str = yesterday.strftime("%Y-%m-%d")

table_column_definitions = [
    {
        "api_field": "Case ID",
        "name": "case_id",
        "type": "STRING",
        "sensitivity": "SESSIONID",
    },
    {
        "api_field": "Vantiv Payment ID",
        "name": "vantiv_payment_id",
        "type": "STRING",
        "sensitivity": "SESSIONID",
    },
    {
        "api_field": "Merchant Order Number",
        "name": "merchant_order_number",
        "type": "STRING",
        "sensitivity": "SESSIONID",
    },
    {
        "api_field": "Account Suffix",
        "name": "account_suffix",
        "type": "LONG",
        "sensitivity": "NONE",
    },
    {"api_field": "Bin", "name": "bin", "type": "STRING", "sensitivity": "NONE"},
    {
        "api_field": "Payment Type",
        "name": "payment_type",
        "type": "STRING",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Reason Code",
        "name": "reason_code",
        "type": "STRING",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Reason Description",
        "name": "reason_description",
        "type": "STRING",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Date Issued",
        "name": "date_issued",
        "type": "DATE",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Date Received",
        "name": "date_received",
        "type": "DATE",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Reply By Date",
        "name": "reply_by_date",
        "type": "DATE",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Chargeback Currency",
        "name": "chargeback_currency",
        "type": "STRING",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Chargeback Amt",
        "name": "chargeback_amt",
        "type": "DOUBLE",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Auth Date",
        "name": "auth_date",
        "type": "DATE",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Auth Currency",
        "name": "auth_currency",
        "type": "STRING",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Auth Amt",
        "name": "auth_amt",
        "type": "DOUBLE",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Transaction Date",
        "name": "transaction_date",
        "type": "DATE",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Transaction Purchase Currency",
        "name": "transaction_purchase_currency",
        "type": "STRING",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Transaction Purchase Amt",
        "name": "transaction_purchase_amt",
        "type": "DOUBLE",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Transaction Settlement Currency",
        "name": "transaction_settlement_currency",
        "type": "STRING",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Transaction Settlement Amt",
        "name": "transaction_settlement_amt",
        "type": "DOUBLE",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Activity Date",
        "name": "activity_date",
        "type": "DATE",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Activity",
        "name": "activity",
        "type": "STRING",
        "sensitivity": "NONE",
    },
    {"api_field": "Cycle", "name": "cycle", "type": "STRING", "sensitivity": "NONE"},
    {
        "api_field": "Campaign",
        "name": "campaign",
        "type": "STRING",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Merchant Name",
        "name": "merchant_name",
        "type": "STRING",
        "sensitivity": "NONE",
    },
    {"name": "affiliate", "type": "STRING", "sensitivity": "NONE"},
    {
        "api_field": "Merchant Grouping ID",
        "name": "merchant_grouping_id",
        "type": "STRING",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Txn Type",
        "name": "txn_type",
        "type": "STRING",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Batch ID",
        "name": "batch_id",
        "type": "STRING",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Session ID",
        "name": "session_id",
        "type": "STRING",
        "sensitivity": "NONE",
    },
    {"api_field": "ARN", "name": "arn", "type": "STRING", "sensitivity": "SESSIONID"},
    {
        "api_field": "Customer ID",
        "name": "customer_id",
        "type": "LONG",
        "sensitivity": "OTHERID",
    },
    {
        "api_field": "Merchant Transaction ID",
        "name": "merchant_transaction_id",
        "type": "LONG",
        "sensitivity": "SESSIONID",
    },
    {
        "api_field": "Billing Descriptor",
        "name": "billing_descriptor",
        "type": "STRING",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Assigned To",
        "name": "assigned_to",
        "type": "STRING",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Assigned To User Type",
        "name": "assigned_to_user_type",
        "type": "STRING",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Issuing Bank Country Code",
        "name": "issuing_bank_country_code",
        "type": "STRING",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Merchant ID",
        "name": "merchant_id",
        "type": "LONG",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Issuing Bank",
        "name": "issuing_bank",
        "type": "STRING",
        "sensitivity": "NONE",
    },
    {"name": "presenter", "type": "STRING", "sensitivity": "NONE"},
    {
        "api_field": "Txn Secondary Amt",
        "name": "txn_secondary_amt",
        "type": "DOUBLE",
        "sensitivity": "NONE",
    },
    {
        "api_field": "Txn Secondary Settlement Amt",
        "name": "txn_secondary_settlement_amt",
        "type": "DOUBLE",
        "sensitivity": "NONE",
    },
    {
        "name": "downloaded_at",
        "type": "TIMESTAMP",
        "field_type": "current_date",
        "sensitivity": "NONE",
    },
]


def fetch_data(session, output_bucket, profile_params):
    s3 = session.resource("s3")

    hostname = "reports.iq.vantivcnp.com"

    # Add host key so we can SSH securely to the correct server
    key = paramiko.RSAKey(data=base64.b64decode(profile_params["vantiv.ftp.hostkey"]))
    cnopts = pysftp.CnOpts()
    cnopts.hostkeys.add("reports.iq.vantivcnp.com", "ssh-rsa", key)

    with pysftp.Connection(
        host=hostname,
        username=profile_params["vantiv.ftp.login"],
        password=profile_params["vantiv.ftp.password"],
        cnopts=cnopts,
    ) as sftp:
        # Load chargebacks to local dir
        date_string = yesterday.strftime("%Y%m%d")
        ftp_chargeback_file_name = f"Transactional_Detail_ChargebackStatusByActivityDate_74700_{date_string}.CSV"
        sftp.get(f"reports/{ftp_chargeback_file_name}")

        chargeback_count = 0
        with open(ftp_chargeback_file_name, "r") as ftp_chargeback_report, open(
            chargeback_file_name, "w"
        ) as chargeback_report:
            reader = csv.reader(ftp_chargeback_report)
            headers = next(reader)

            writer = csv.writer(chargeback_report, dialect="excel")
            writer.writerow([column["name"] for column in table_column_definitions])

            for line in reader:
                chargeback_count += 1
                dispute = utils.arr_to_dict(line, headers)
                writer.writerow(
                    [
                        utils.write_field(column, dispute)
                        for column in table_column_definitions
                    ]
                )

    print(f"Wrote {chargeback_count} chargebacks")

    if chargeback_count == 0:
        print(f"no chargebacks found for {start_date_str}")
    else:
        env_name = profile_params["env_name"]
        timestamp = str(int(time.time()))
        # Push to S3
        csv_s3_uri = f"{env_name}/date={start_date_str}/{timestamp}-csv"
        s3.Bucket(output_bucket).upload_file(
            chargeback_file_name, f"{csv_s3_uri}/{chargeback_file_name}"
        )
        s3.Bucket(output_bucket).upload_file(
            ftp_chargeback_file_name,
            f"{env_name}/date={start_date_str}/{ftp_chargeback_file_name}",
        )
    return chargeback_count


def convert_to_parquet(session, profile_params):
    timestamp = str(int(time.time()))
    env_name = profile_params["env_name"]
    parquet_s3_uri = (
        f"{env_name}/{tahoe_table}/date={start_date_str}/{timestamp}-parquet"
    )
    full_parquet_s3_uri = f"s3://{tahoe_export_bucket}/{parquet_s3_uri}"
    df = pd.read_csv(
        chargeback_file_name,
        dtype=utils.map_schema(table_column_definitions),
        parse_dates=utils.map_date_columns(table_column_definitions),
    )
    os.makedirs(local_folder, exist_ok=True)
    table = pa.Table.from_pandas(
        df, schema=utils.map_schema_pyarrow(table_column_definitions)
    )
    pq.write_table(
        table, f"{local_folder}/vantiv.paraquet", use_deprecated_int96_timestamps=True
    )

    print("wrote paraquet files")
    utils.upload_folder_to_s3(session, local_folder, full_parquet_s3_uri)
    return full_parquet_s3_uri


def publish_to_tahoe(session, full_parquet_s3_uri):
    api_key = utils.get_payments_producer_key(session)
    return utils.copy_partition_tahoe(
        api_key,
        tahoe_table,
        3,
        table_column_definitions,
        full_parquet_s3_uri,
        start_date_str,
    )


def perform_extract_vantiv(profile_params):

    session = get_aws_session()
    cloudwatch = session.client("cloudwatch", region_name="us-west-2")

    try:
        chargeback_count = fetch_data(session, output_bucket, profile_params)
        if chargeback_count > 0:
            full_parquet_s3_uri = convert_to_parquet(session, profile_params)
            job_id = publish_to_tahoe(session, full_parquet_s3_uri)
            status = utils.check_tahoe_job(session, job_id)
            if status != "SUCCESS":
                raise RuntimeError(f"{job_id} failed with status {status}")
            print(f"job published to airflow with status {status}")

        # Put success metrics
        cloudwatch.put_metric_data(
            MetricData=[
                {
                    "MetricName": "CHARGEBACK_COUNT",
                    "Dimensions": [
                        {"Name": "Unique Chargebacks", "Value": "CHARGEBACKS"}
                    ],
                    "Unit": "None",
                    "Value": chargeback_count,
                }
            ],
            Namespace=f"CHARGEBACKS_V2/{provider.upper()}",
        )
        cloudwatch.put_metric_data(
            MetricData=[
                {
                    "MetricName": "EXTRACT_SUCCESS",
                    "Dimensions": [{"Name": "Successful Runs", "Value": "SUCCESSES"}],
                    "Unit": "None",
                    "Value": 1.0,
                }
            ],
            Namespace=f"CHARGEBACKS_V2/{provider.upper()}",
        )
    except Exception:
        # Put failure metrics
        cloudwatch.put_metric_data(
            MetricData=[
                {
                    "MetricName": "EXTRACT_SUCCESS",
                    "Dimensions": [{"Name": "Successful Runs", "Value": "SUCCESSES"}],
                    "Unit": "None",
                    "Value": 0.0,
                }
            ],
            Namespace=f"CHARGEBACKS_V2/{provider.upper()}",
        )
        raise
