import os

from mantra.workflow.components.cloudwatch.alarm import alarm_on_step_failed
from mantra.workflow.components.cloudwatch.events import schedule_cron
from mantra.workflow.decorators.decorator import step, workflow
from mantra.workflow.utils.base_workflow import BaseWorkflow

from workflows.lib.vantiv_chargeback import perform_extract_vantiv


class VantivWorkflow(BaseWorkflow):
    @classmethod
    def dag_name(cls):
        return os.path.splitext(os.path.basename(__file__))[0]

    def __init__(self, profile_params):
        super().__init__(profile_params)

    @step
    def extract_vantiv(self):
        perform_extract_vantiv(self.profile_params)

    @alarm_on_step_failed
    @schedule_cron(day="*", hour="16", minute="30", year="*")
    @workflow
    def workflow_dag(self):
        return ("extract_vantiv",)
