from workflows.dags.vantiv import VantivWorkflow


# basic test - verify dag is a tuple, and all functions in the dag exist
def test_workflow_dag():
    workflow = VantivWorkflow({"resource_prefix": "123"})
    dag = workflow.workflow_dag()
    assert type(dag) is tuple
    for func in dag:
        assert hasattr(workflow, func)
