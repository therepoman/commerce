# Tachanka Developer Guide
This document is intended to give an overview of the structure of tachanka code and design

## Design
Refer to DevPortal Design document for the <a href="https://docs.google.com/document/d/1tdjP_hqMpYLa4pE5uUD_RH5d2szVBmvA0LyVBInVUv4/edit">original design</a>. This design has gone through some iterations. Notably as part of launch for extensions and an effort to refactor the code base, the design and code was modified. Refer to <a href="https://wiki.twitch.com/display/ENG/Operation+Bob+the+Document+Builder">Operation Bob the Document Builder for details</a>.

## Code
Important Code packages in tachanka:
* ```api``` - contains handlers for the various apis tachanka vends. Refer to <a href="https://git-aws.internal.justin.tv/commerce/tachanka-client">tachanka-client</a> for all the APIs. 
* ```cmd``` - configures and starts up tachanka service
* ```config``` - Contains config values used by the service, document defaults and tables for metadata
* ```dynamo``` - DAOs to dynamo tables
* ```models``` - Models the different objects/products managed by tachanka. For now that includes Game, IGC, Extension, Extension Item
* ```service``` - logical components that manage different aspects of the devportal. Typically API handlers will make use of the various services. Services will make use of the dynamo DAOs and models to execute business logic.
* ```validation``` - contains the valid and invalid document schema

Sample flow for submitting a new managedProduct:
1. Retrieving the managedProduct:
* SubmitManagedProduct API will call the ManagedProductService to retrieve the ManagedProduct.
* ManagedProductService gets the ManagedProduct from the database using the DAO.
* ManagedProductService calls the DocumentService to get the document. ManagedProductService then determines the type of the ManagedProduct and instantiates an object of that type and returns it.

2. Submitting the managedProduct:
* SubmitManagedProduct API then calls Submission Service to ```submitManagedProduct```
* Submission Service checks that the model object is a valid devportal product and also a valid managed product. Submission service then creates a new base adg document which will have all defaults set. It then calls the ```PopulateADGDocument``` on the object. Each model is responsible for populating the adg document with the data that is unique to the submission of that type. The result is that we have an ADG document which has attributes merged from the defaults and well as the ones populated by the model.
* The resulting ADG document is then json marshalled into a serialized document. A new submission record is created using the DAOs.
* Submission service then makes an async call to the the appropriate downstream system to submit the product.

## Database Design
Currently all of devportal data is managed in DynamoDB

* ```external-mapping```: Maps managed product ids to external vendor ids and their types (ADG_PRODUCT_ID, ADG_VENDOR_ID, etc.)
```(parent-id, type, lastUpdated, value)```. Going forward the plan is to merge this with managed-products table.

* ```managed-products```: Managed Product encapsulates each product managed through dev portal
```(id, draft-document-id, lastUpdated, type)```

* ```dev-portal-ownership```: Maps dev portal products to managed products
```(dev-portal-product-id, managed-product-id, lastUpdated)```

* ```documents```: contains the actual json document that is submitted to downstream systems. Also contains a reference to the managed product it belongs to
```(id, documentData, documentVersion, lastUpdated, managedProductId)```

* ```submissions```: Contains an entry for each submission made to the downstream systems for ingestion/updation. ```(id, document, lastUpdated, managed-product-id, submisssion-steps, created_date, last_updated, managed_product_id, status)```

## Other dependencies
* TPIGS: Tachanka calls TPIGS, an Amazon service via SWS. TPIGS acts as a proxy to call ADGSubmission. ADGSubmission processes the JSON document we sent and calls downstream services in Amazon to create the product, most notably Mulligan.
* ADGSubmissionNotification: ADGSubmissions sents a notification via SNS about the status of the submission. We have an SQS subscribed to that and also a Lambda that polls the SQS periodically and updates the submission status for that managed product using Tachanka APIs.

## Infrastructure
@TODO: Add Code pipeline, Code deploy stuff

## Concepts and glossary
@TODO fill up with additional information about important concepts like Title document, CIC, etc.


