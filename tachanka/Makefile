# This makefile is setup to work from both a host directly with
# make local
# and with manta with
# make
#
# This causes a bit of oddness in setup as we have multiple entry points and the build
# is actually re-entrant as make on host calls manta which in turn calls manta inside of the
# docker container

novendor = $(shell go list ./... | grep -v vendor/)

all: fmt manta_bootstrap

export GOBIN=${GOPATH}/bin

# Remove manta artifacts
manta_cleanup:
	rm -rf .manta/bin
	rm -rf .manta/courier
	rm -rf .manta/config
	rm -rf .manta/country
	rm -rf .manta/experiments
	rm -rf .manta/template
	rm -rf .manta/action

# Kick off a full manta build
manta_bootstrap: manta_cleanup
	manta -v -f .manta.json

# Task run inside of the manta build to do the actual build work
manta: vet test install delete_source_aka_wreck_your_day courier save_cmd

# Task run inside of the manta build for running integration tests
manta_integration: go_integration_tests delete_source_aka_wreck_your_day

# Run integration tests
integration_tests: manta_cleanup
	manta -v -f .manta_integration.json

# Build the executables we want to distribute
install:
	go install -v -a -tags 'netgo' -ldflags "-X code.justin.tv/common/golibs/bininfo.revision=$$GIT_COMMIT" code.justin.tv/commerce/tachanka/cmd/...

# Remove src code along with certain build artifacts. Leave configuration and several build files left
delete_source_aka_wreck_your_day:
	find . -mindepth 1 -maxdepth 1 -not -name bin -not -name config -not -name country -not -name experiments -not -name action -not -name template -exec rm -rf {} \;

# Copy files for courier
courier:
	#mkdir -p ./courier && mv ${GOPATH}/bin/restart.sh ./courier/
	mv -f ${GOPATH}/bin/tachanka .

# Save cmd binary files we want deployed
save_cmd:
	mkdir -p ./cmd

# Format the code
fmt:
	go fmt $(novendor)

# Vet everything
vet:
	go vet -composites=false $(novendor)

# If the dynamodb_local folder does not exist, download and unpack
dynamodb_local:
	wget -q https://s3-us-west-2.amazonaws.com/dynamodb-local/dynamodb_local_latest.tar.gz
	mkdir -p dynamodb_local
	tar -C dynamodb_local -xzf dynamodb_local_latest.tar.gz

# Stop all running dynamo local instances and wait for them to shut down
dynamo_cleanup_%:
	pkill -f "^(\/usr\/bin\/)?java.*DynamoDBLocal.jar" ; true
	sleep 1

# Start a dynamo local in-memory instance
dynamo_start: dynamodb_local dynamo_cleanup_1
	java -Djava.library.path=dynamodb_local/DynamoDBLocal_lib -jar dynamodb_local/DynamoDBLocal.jar -port 9001 -inMemory &

# Runs before tests
test_setup: dynamo_start

# Runs after tests
test_cleanup: dynamo_cleanup_2

generate:
	retool do mockery -dir dynamo -all
	retool do mockery -dir service -all
	retool do mockery -dir vendor/code.justin.tv/common/goauthorization -name=IDecoder
	retool do mockery -dir client/adg -all

# Run go tests
go_test:
	go test -race $(novendor)

# Run go integration tests
go_integration_tests:
	go test -tags=integration code.justin.tv/commerce/tachanka/integration/... -v

install_server:
	go install code.justin.tv/commerce/tachanka/cmd/tachanka/...

install_smoke_test:
	go install code.justin.tv/commerce/tachanka/cmd/smoke_test/...

smoke_tests: install_server install_smoke_test
	echo `pkill tachanka`
	export ENVIRONMENT=local; ${GOPATH}/bin/tachanka &
	sleep 3
	export ENVIRONMENT=local; ${GOPATH}/bin/smoke_test
	pkill tachanka

# Run tests with setup and cleanup
test: test_setup go_test smoke_tests test_cleanup

# Makes sure all necessary prerequisites are installed locally
ensure_local_dependencies:
	command -v java >/dev/null 2>&1 || { echo >&2 "java required, but is not configured on your path. Please configure java and rerun."; exit 1; }
	command -v wget >/dev/null 2>&1 || { echo >&2 "wget required, but it is not installed. Install with command 'brew install wget'. Please install wget and rerun."; exit 1; }

local_no_mocks: ensure_local_dependencies fmt vet test

# Local build if you want to work locally or have to. Currently only does up to the test loop
local: setup fmt vet test

# Utility commands

local_server: dynamo_start cleanup_local_server install_server
	export ENVIRONMENT=local; ${GOPATH}/bin/tachanka > local.log 2>&1 &

tail_local_server:
	tail -f local.log | while read line; do echo "$$line" | python -mjson.tool; done

cleanup_local_server:
	pkill -f "${GOPATH}/bin/tachanka"; true

setup:
	./install_proto.bash
	GOPATH=$(CURDIR)/_tools go install code.justin.tv/common/retool/...
	$(RETOOL) sync

update-go-dependencies: setup
	GOPATH=$(CURDIR)/_tools glide update --strip-vendor
	GOPATH=$(CURDIR)/_tools glide-vc --only-code --no-tests --use-lock-file
	echo "# Docker fails if this file is not here right now, but it doesn't need to have anything" > vendor/.dockerignore
