package mocks

import (
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/AmazonMWSGoClient/mws"
)

// AmazonMWSClient is an autogenerated mock type for the AmazonMWSClient type
type IAmazonMWSGoClient struct {
	mock.Mock
}

// PutMetricsForAggregation provides a mock function with given fields: _a0, _a1, _a2
func (_m *IAmazonMWSGoClient) PutMetricsForAggregation(_a0 *mws.PutMetricsForAggregationRequest, _a1 mws.Region, _a2 *credentials.Credentials) (*mws.PutMetricsForAggregationResponse, error) {
	ret := _m.Called(_a0, _a1, _a2)

	var r0 *mws.PutMetricsForAggregationResponse
	if rf, ok := ret.Get(0).(func(*mws.PutMetricsForAggregationRequest, mws.Region, *credentials.Credentials) *mws.PutMetricsForAggregationResponse); ok {
		r0 = rf(_a0, _a1, _a2)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*mws.PutMetricsForAggregationResponse)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(*mws.PutMetricsForAggregationRequest, mws.Region, *credentials.Credentials) error); ok {
		r1 = rf(_a0, _a1, _a2)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

var _ mws.IAmazonMWSGoClient = (*IAmazonMWSGoClient)(nil)
