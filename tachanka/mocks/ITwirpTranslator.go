// Code generated by mockery v1.0.0
package mocks

import mock "github.com/stretchr/testify/mock"
import models "code.justin.tv/commerce/tachanka/models"
import service "code.justin.tv/commerce/tachanka/service"
import tachanka_client "code.justin.tv/commerce/tachanka-client"

// ITwirpTranslator is an autogenerated mock type for the ITwirpTranslator type
type ITwirpTranslator struct {
	mock.Mock
}

// ConvertManagedProductTypeFromTwirp provides a mock function with given fields: _a0
func (_m *ITwirpTranslator) ConvertManagedProductTypeFromTwirp(_a0 tachanka_client.ManagedProductType) (models.ManagedProductType, error) {
	ret := _m.Called(_a0)

	var r0 models.ManagedProductType
	if rf, ok := ret.Get(0).(func(tachanka_client.ManagedProductType) models.ManagedProductType); ok {
		r0 = rf(_a0)
	} else {
		r0 = ret.Get(0).(models.ManagedProductType)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(tachanka_client.ManagedProductType) error); ok {
		r1 = rf(_a0)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ConvertManagedProductTypeToTwirp provides a mock function with given fields: _a0
func (_m *ITwirpTranslator) ConvertManagedProductTypeToTwirp(_a0 models.ManagedProductType) (tachanka_client.ManagedProductType, error) {
	ret := _m.Called(_a0)

	var r0 tachanka_client.ManagedProductType
	if rf, ok := ret.Get(0).(func(models.ManagedProductType) tachanka_client.ManagedProductType); ok {
		r0 = rf(_a0)
	} else {
		r0 = ret.Get(0).(tachanka_client.ManagedProductType)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(models.ManagedProductType) error); ok {
		r1 = rf(_a0)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ConvertServiceValidationErrorToAPIValidationError provides a mock function with given fields: _a0
func (_m *ITwirpTranslator) ConvertServiceValidationErrorToAPIValidationError(_a0 *service.ValidationError) *tachanka_client.ValidationError {
	ret := _m.Called(_a0)

	var r0 *tachanka_client.ValidationError
	if rf, ok := ret.Get(0).(func(*service.ValidationError) *tachanka_client.ValidationError); ok {
		r0 = rf(_a0)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*tachanka_client.ValidationError)
		}
	}

	return r0
}

// ConvertSubmissionStepToTwirp provides a mock function with given fields: _a0
func (_m *ITwirpTranslator) ConvertSubmissionStepToTwirp(_a0 *models.SubmissionStep) (*tachanka_client.SubmissionStep, error) {
	ret := _m.Called(_a0)

	var r0 *tachanka_client.SubmissionStep
	if rf, ok := ret.Get(0).(func(*models.SubmissionStep) *tachanka_client.SubmissionStep); ok {
		r0 = rf(_a0)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*tachanka_client.SubmissionStep)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(*models.SubmissionStep) error); ok {
		r1 = rf(_a0)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ConvertSubmissionToTwirp provides a mock function with given fields: _a0
func (_m *ITwirpTranslator) ConvertSubmissionToTwirp(_a0 *models.Submission) (*tachanka_client.Submission, error) {
	ret := _m.Called(_a0)

	var r0 *tachanka_client.Submission
	if rf, ok := ret.Get(0).(func(*models.Submission) *tachanka_client.Submission); ok {
		r0 = rf(_a0)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*tachanka_client.Submission)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(*models.Submission) error); ok {
		r1 = rf(_a0)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ExtractDomainAndVendorIds provides a mock function with given fields: _a0
func (_m *ITwirpTranslator) ExtractDomainAndVendorIds(_a0 *tachanka_client.ManagedProduct) (string, string, error) {
	ret := _m.Called(_a0)

	var r0 string
	if rf, ok := ret.Get(0).(func(*tachanka_client.ManagedProduct) string); ok {
		r0 = rf(_a0)
	} else {
		r0 = ret.Get(0).(string)
	}

	var r1 string
	if rf, ok := ret.Get(1).(func(*tachanka_client.ManagedProduct) string); ok {
		r1 = rf(_a0)
	} else {
		r1 = ret.Get(1).(string)
	}

	var r2 error
	if rf, ok := ret.Get(2).(func(*tachanka_client.ManagedProduct) error); ok {
		r2 = rf(_a0)
	} else {
		r2 = ret.Error(2)
	}

	return r0, r1, r2
}

// TranslateManagedProduct provides a mock function with given fields: _a0, _a1
func (_m *ITwirpTranslator) TranslateManagedProduct(_a0 *service.DevPortalProduct, _a1 models.ManagedProduct) (*tachanka_client.ManagedProduct, error) {
	ret := _m.Called(_a0, _a1)

	var r0 *tachanka_client.ManagedProduct
	if rf, ok := ret.Get(0).(func(*service.DevPortalProduct, models.ManagedProduct) *tachanka_client.ManagedProduct); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*tachanka_client.ManagedProduct)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(*service.DevPortalProduct, models.ManagedProduct) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
