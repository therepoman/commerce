// Code generated by mockery v1.0.0
package mocks

import mock "github.com/stretchr/testify/mock"
import models "code.justin.tv/commerce/tachanka/models"

// ExternalMappingDao is an autogenerated mock type for the ExternalMappingDao type
type ExternalMappingDao struct {
	mock.Mock
}

// CreateTable provides a mock function with given fields:
func (_m *ExternalMappingDao) CreateTable() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// DeleteTable provides a mock function with given fields:
func (_m *ExternalMappingDao) DeleteTable() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Get provides a mock function with given fields: parentId, mappingType
func (_m *ExternalMappingDao) Get(parentId string, mappingType models.ExternalMappingTypeEnum) (*models.ExternalMapping, error) {
	ret := _m.Called(parentId, mappingType)

	var r0 *models.ExternalMapping
	if rf, ok := ret.Get(0).(func(string, models.ExternalMappingTypeEnum) *models.ExternalMapping); ok {
		r0 = rf(parentId, mappingType)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.ExternalMapping)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, models.ExternalMappingTypeEnum) error); ok {
		r1 = rf(parentId, mappingType)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetByMappingValue provides a mock function with given fields: value, mappingType
func (_m *ExternalMappingDao) GetByMappingValue(value string, mappingType models.ExternalMappingTypeEnum) (*[]models.ExternalMapping, error) {
	ret := _m.Called(value, mappingType)

	var r0 *[]models.ExternalMapping
	if rf, ok := ret.Get(0).(func(string, models.ExternalMappingTypeEnum) *[]models.ExternalMapping); ok {
		r0 = rf(value, mappingType)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*[]models.ExternalMapping)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, models.ExternalMappingTypeEnum) error); ok {
		r1 = rf(value, mappingType)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Put provides a mock function with given fields: externalMapping
func (_m *ExternalMappingDao) Put(externalMapping *models.ExternalMapping) error {
	ret := _m.Called(externalMapping)

	var r0 error
	if rf, ok := ret.Get(0).(func(*models.ExternalMapping) error); ok {
		r0 = rf(externalMapping)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Update provides a mock function with given fields: externalMapping
func (_m *ExternalMappingDao) Update(externalMapping *models.ExternalMapping) (*models.ExternalMapping, error) {
	ret := _m.Called(externalMapping)

	var r0 *models.ExternalMapping
	if rf, ok := ret.Get(0).(func(*models.ExternalMapping) *models.ExternalMapping); ok {
		r0 = rf(externalMapping)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.ExternalMapping)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(*models.ExternalMapping) error); ok {
		r1 = rf(externalMapping)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
