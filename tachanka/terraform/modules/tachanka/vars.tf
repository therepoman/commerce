variable "aws_credentials" {
  description = "The name of the AWS credentials to use for the AWS account"
}

variable "account_id" {
  description = "The Accound ID of this AWS account"
}

variable "vpc_id" {
  description = "The id of the VPC to use"
}

variable "private_subnets" {
  description = "The private subnets to use"
}

variable "sg_id" {
  description = "the Security Group ID to use"
}

variable "environment" {
  description = "Whether it's production, dev, etc"
}