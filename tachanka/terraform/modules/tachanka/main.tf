resource "aws_elastic_beanstalk_application" "tachanka_beanstalk_app" {
  name = "tachanka_${var.environment}"
  description = "tachanka ${var.environment} app"
}

provider "aws" {
  region = "us-west-2"
  profile = "${var.aws_credentials}"
}

module "bs-dev" {
  source = "git::git+ssh://git@git-aws.internal.justin.tv/dta/tf_beanstalk_webserver_env.git//?ref=master"
  aws_profile = "${var.aws_credentials}"
  eb_application_name = "tachanka_${var.environment}"
  vpc_id = "${var.vpc_id}"
  ec2_subnet_ids = "${var.private_subnets}"
  elb_subnet_ids = "${var.private_subnets}"
  elb_loadbalancer_security_groups = "${var.sg_id}"
  auto_scaling_lc_security_groups = "${var.sg_id}"
  solution_stack_name = "64bit Amazon Linux 2017.09 v2.8.0 running Docker 17.06.2-ce"
  auto_scaling_lc_keypair_name = "tachanka"
  auto_scaling_lc_instance_type = "t2.micro"
  min_size = "2"
  max_size = "20"
  asgtrigger_upper_threshold = "35"
  auto_scaling_lc_root_volume_size = "50"
  owner = "fuel-devportal@amazon.com"
  service = "commerce/tachanka"
  env = "${var.environment}"
  associate_public_address = "false"
  eb_environment_service_role="${aws_iam_role.health_service_role.name}"
  auto_scaling_lc_iam_instance_profile="${aws_iam_instance_profile.ec2_machine_role_instance_profile.name}"
  cw_streaming_logs_enabled = "true"
  cw_delete_on_terminate = "false"
  cw_retention_in_days = "30"
}

resource "aws_dynamodb_table" "dev-portal-ownership-dynamo-table" {
  name = "${var.environment}_dev-portal-ownership"
  read_capacity = 100
  write_capacity = 100
  hash_key = "dev-portal-product-id"
  range_key = "managed-product-id"
  attribute {
    name = "dev-portal-product-id"
    type = "S"
  }
  attribute {
    name = "managed-product-id"
    type = "S"
  }
}

resource "aws_dynamodb_table" "documents-dynamo-table" {
  name = "${var.environment}_documents"
  read_capacity = 100
  write_capacity = 100
  hash_key = "id"
  attribute {
    name = "id"
    type = "S"
  }
}

resource "aws_dynamodb_table" "external-mapping-dynamo-table" {
  name = "${var.environment}_external-mapping"
  read_capacity = 100
  write_capacity = 100
  hash_key = "parent-id"
  range_key = "type"
  attribute {
    name = "parent-id"
    type = "S"
  }
  attribute {
    name = "type"
    type = "S"
  }
  attribute {
    name = "value"
    type = "S"
  }
  global_secondary_index {
    name = "ValueIndex"
    hash_key = "value"
    write_capacity = 50
    read_capacity = 50
    projection_type = "ALL"
  }
}

resource "aws_dynamodb_table" "managed-products-dynamo-table" {
  name = "${var.environment}_managed-products"
  read_capacity = 100
  write_capacity = 100
  hash_key = "id"
  attribute {
    name = "id"
    type = "S"
  }
}

resource "aws_dynamodb_table" "product-vendors-dynamo-table" {
  name = "${var.environment}_product-vendors"
  read_capacity = 100
  write_capacity = 100
  hash_key = "parent-id"
  attribute {
    name = "parent-id"
    type = "S"
  }
}

resource "aws_dynamodb_table" "submission-steps-dynamo-table" {
  name = "${var.environment}_submission-steps"
  read_capacity = 100
  write_capacity = 100
  hash_key = "id"
  attribute {
    name = "id"
    type = "S"
  }
}

resource "aws_dynamodb_table" "submissions-dynamo-table" {
  name = "${var.environment}_submissions"
  read_capacity = 100
  write_capacity = 100
  hash_key = "id"
  attribute {
    name = "id"
    type = "S"
  }
  attribute {
    name = "managed-product-id"
    type = "S"
  }
  global_secondary_index {
    name = "ManagedProductIndex"
    hash_key = "managed-product-id"
    write_capacity = 100
    read_capacity = 100
    projection_type = "ALL"
  }
}

resource "aws_dynamodb_table" "admins-table" {
  name = "${var.environment}_admins"
  read_capacity = 100
  write_capacity = 100
  hash_key = "tuid"
  attribute {
    name = "tuid"
    type = "S"
  }
}

resource "aws_dynamodb_table" "dev-portal-products-table" {
  name = "${var.environment}_dev-portal-products"
  read_capacity = 100
  write_capacity = 100
  hash_key = "id"
  attribute {
    name = "id"
    type = "S"
  }
}