variable "aws_account_id" {
 default = "870981134180"
}

variable "aws_credentials" {
 default = "fuel-devportal-dev"
}

module "aws_account" {
  source = "git::git+ssh://git@git-aws.internal.justin.tv/systems/terraform.git//modules/account_template/region/us-west-2?ref=a009869d8feffd1c4f2d8e1b959398cb9b4e5c6e"
  region = "us-west-2"
  account_name = "fuel-devportal@amazon.com"
  vpc_cidr = "10.201.68.0/22"
  account_id = "${var.aws_account_id}"
  owner = "fuel-devportal@amazon.com"
  service = "tachanka"
  environment = "dev"
  credential_profile="${var.aws_credentials}"
  public_subnet_shift = 4
  private_subnet_shift = 2
  public_subnet_offset = 12
}

module "tachanka_dev" {
  source = "../modules/tachanka"
  account_id = "${var.aws_account_id}"
  aws_credentials = "${var.aws_credentials}"
  vpc_id = "${module.aws_account.vpc_id}"
  private_subnets = "${module.aws_account.private_subnets}"
  sg_id = "${module.aws_account.sg_id}"
  environment = "dev"
}

module "tachanka_staging" {
  source = "../modules/tachanka"
  account_id = "${var.aws_account_id}"
  aws_credentials = "${var.aws_credentials}"
  vpc_id = "${module.aws_account.vpc_id}"
  private_subnets = "${module.aws_account.private_subnets}"
  sg_id = "${module.aws_account.sg_id}"
  environment = "staging"
}