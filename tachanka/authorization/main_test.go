package authorization_test

import (
	dynamoTesting "code.justin.tv/common/godynamo/testing"
	"os"
	"testing"
)

var dynamoContext *dynamoTesting.DynamoTestContext

func TestMain(m *testing.M) {
	var err error

	dynamoContext, err = dynamoTesting.NewDynamoTestContext("authorization")
	if err != nil {
		os.Exit(1)
	}

	exitCode := m.Run()

	err = dynamoContext.Cleanup()
	if err != nil {
		os.Exit(1)
	}

	os.Exit(exitCode)
}
