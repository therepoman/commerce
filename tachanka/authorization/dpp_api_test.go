package authorization_test

import (
	"code.justin.tv/commerce/tachanka/api"
	"github.com/satori/go.uuid"
	. "github.com/smartystreets/goconvey/convey"
	"testing"

	"code.justin.tv/commerce/tachanka/authorization"
	"code.justin.tv/gds/gds/portal/dpp/dpptwirp"
	"fmt"
)

func TestDPPAuthorizationAPI(t *testing.T) {
	mockApi := newDppAPI_mockedAtDynamo()

	validDpps := make([]*authorization.DevPortalProduct, 0)
	for i := 0; i < 10; i++ {
		newProduct := &authorization.DevPortalProduct{
			Id:   uuid.NewV4().String(),
			Name: fmt.Sprintf("Game %d", i),
		}
		mockApi.DevPortalProductDao.Put(newProduct)
		validDpps = append(validDpps, newProduct)
	}

	validAdmins := make([]*authorization.Admin, 0)
	for i := 0; i < 3; i++ {
		newAdmin := &authorization.Admin{
			Tuid: uuid.NewV4().String(),
		}
		mockApi.AdminDao.Put(newAdmin)
		validAdmins = append(validAdmins, newAdmin)
	}

	Convey("Given a new AuthorizationService", t, func() {
		cut := authorization.NewAuthorizationService(mockApi.AdminDao, mockApi.DevPortalProductDao)

		Convey("We should be able to authorize all known users", func() {
			for _, admin := range validAdmins {
				validRequest := &dpptwirp.HasPermissionRequest{
					UserId:      admin.Tuid,
					DpProductId: uuid.NewV4().String(),
					Permission:  "Literally Anything",
				}

				response, err := cut.HasPermission(nil, validRequest)
				So(err, ShouldBeNil)
				So(response.Result, ShouldBeTrue)
			}
		})

		Convey("We should not be able to authorize unknown users", func() {
			invalidRequest := &dpptwirp.HasPermissionRequest{
				UserId:      "Gaffo",
				DpProductId: uuid.NewV4().String(),
				Permission:  ":giveplz: permission",
			}

			response, err := cut.HasPermission(nil, invalidRequest)
			So(err, ShouldBeNil)
			So(response.Result, ShouldBeFalse)
		})

		Convey("We should be able to get all the DPPs", func() {
			validListRequest := &dpptwirp.ProductsRequest{
				UserId: validAdmins[0].Tuid,
			}

			response, err := cut.Products(nil, validListRequest)
			So(err, ShouldBeNil)
			So(len(response.Products), ShouldEqual, len(validDpps))
			cutProducts := response.Products
			var found bool
			for i := 0; i < len(validDpps); i++ {
				found, cutProducts = findDPPAndRemoveFromProducts(cutProducts, validDpps[i])
				So(found, ShouldBeTrue)
			}
		})

	})
}

func newDppAPI_mockedAtDynamo() *api.ServerAPI {
	adminDao := authorization.NewAdminDao(dynamoContext.CreateTestClient(dynamoContext.WaitTimeout, dynamoContext.WaitTimeout, "test-auth-admins"))
	dynamoContext.RegisterTestingDao(adminDao)
	devPortalProductsDao := authorization.NewDevPortalProductDao(dynamoContext.CreateTestClient(dynamoContext.WaitTimeout, dynamoContext.WaitTimeout, "test-auth-dpp"))
	dynamoContext.RegisterTestingDao(devPortalProductsDao)

	return &api.ServerAPI{
		AdminDao:            adminDao,
		DevPortalProductDao: devPortalProductsDao,
	}
}

func findDPPAndRemoveFromProducts(items []*dpptwirp.Product, item *authorization.DevPortalProduct) (bool, []*dpptwirp.Product) {
	foundIndex := 0
	for i, listItem := range items {
		if listItem.Id == item.Id && listItem.Title == item.Name {
			foundIndex = i
			break
		}
	}

	if foundIndex != -1 {
		items = append(items[:foundIndex], items[foundIndex+1:]...)
		return true, items
	} else {
		return false, items
	}
}
