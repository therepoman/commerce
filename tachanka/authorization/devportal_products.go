package authorization

import (
	"code.justin.tv/common/godynamo/client"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"

	"errors"
	"sort"
)

const (
	DevPortalProductHashKeyColumn string = "id"
	DevPortalProductNameColumn    string = "name"
	DevPortalProductReadCapacity  int64  = 100
	DevPortalProductWriteCapacity int64  = 100
)

type DevPortalProductDao struct {
	table  client.DynamoTable
	client *client.DynamoClient
}

type IDevPortalProductDao interface {
	List() ([]*DevPortalProduct, error)
	Get(tuid string) (*DevPortalProduct, error)
	Put(dpp *DevPortalProduct) error
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *client.DynamoClientConfig
}

func (dao *DevPortalProductDao) DeleteTable() error {
	return dao.client.DeleteTable(dao.table)
}

func (dao *DevPortalProductDao) SetupTable() error {
	return dao.client.SetupTable(dao.table)
}

func (dao *DevPortalProductDao) GetClientConfig() *client.DynamoClientConfig {
	return dao.client.ClientConfig
}

func (dao *DevPortalProductDao) List() ([]*DevPortalProduct, error) {
	emptyFilter := client.ScanFilter{}
	results, err := dao.client.ScanTable(&DevPortalProductsTable{}, emptyFilter)

	if err != nil {
		return nil, err
	}

	listResult := make([]*DevPortalProduct, len(results))
	for i, record := range results {
		var isProduct bool
		listResult[i], isProduct = record.(*DevPortalProduct)
		if !isProduct {
			return nil, errors.New("Encountered an expected type while converting dynamo result to item record")
		}
	}

	sort.Slice(listResult, func(i, j int) bool {
		return listResult[i].Name < listResult[j].Name
	})

	return listResult, nil
}

func (dao *DevPortalProductDao) Put(dpp *DevPortalProduct) error {
	return dao.client.PutItem(dpp)
}

func (dao *DevPortalProductDao) Get(tuid string) (*DevPortalProduct, error) {
	admin := &DevPortalProduct{
		Id: tuid,
	}

	result, err := dao.client.GetItem(admin)
	if err != nil {
		return nil, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	adminResult, isDevPortalProduct := result.(*DevPortalProduct)
	if !isDevPortalProduct {
		return adminResult, errors.New("Encountered an unexpected type while converting dynamo result to DevPortalProduct")
	}

	return adminResult, nil
}

func NewDevPortalProductDao(client *client.DynamoClient) IDevPortalProductDao {
	dao := &DevPortalProductDao{}
	dao.client = client
	dao.table = &DevPortalProductsTable{}
	return dao
}

//DocumentsTable is the primary data store of documents throughout Tachanka. Documents
//are purposefully abstract. It can be viewed as a large hashmap keyed by document Id.
type DevPortalProductsTable struct{}

type DevPortalProduct struct {
	client.BaseDynamoTableRecord
	Id   string
	Name string
}

func (t *DevPortalProductsTable) GetTableName() string {
	return "dev-portal-products"
}

func (t *DevPortalProductsTable) NewCreateTableInput() *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(DevPortalProductReadCapacity),
			WriteCapacityUnits: aws.Int64(DevPortalProductWriteCapacity),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String(DevPortalProductHashKeyColumn),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String(DevPortalProductHashKeyColumn),
				KeyType:       aws.String("HASH"),
			},
		},
	}
}

func (t *DevPortalProductsTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (client.DynamoTableRecord, error) {
	id := client.StringFromAttributes(attributeMap, DevPortalProductHashKeyColumn)
	name := client.OptionalStringFromAttributes(attributeMap, DevPortalProductNameColumn)

	return &DevPortalProduct{
		Id:   id,
		Name: *name,
	}, nil
}

func (a *DevPortalProduct) GetTable() client.DynamoTable {
	return &DevPortalProductsTable{}
}

func (a *DevPortalProduct) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)

	client.PopulateAttributesString(itemMap, DevPortalProductHashKeyColumn, a.Id)
	client.PopulateAttributesOptionalString(itemMap, DevPortalProductNameColumn, &a.Name)
	return itemMap
}

func (a *DevPortalProduct) NewItemKey() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)

	client.PopulateAttributesString(itemMap, DevPortalProductHashKeyColumn, a.Id)
	return itemMap
}

func (a *DevPortalProduct) Equals(other client.DynamoTableRecord) bool {
	otherDevPortalProduct, isDevPortalProduct := other.(*DevPortalProduct)
	if !isDevPortalProduct {
		return false
	} else {
		return a.Id == otherDevPortalProduct.Id
	}
}
