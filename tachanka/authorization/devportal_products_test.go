package authorization_test

import (
	"github.com/satori/go.uuid"
	"testing"

	"code.justin.tv/commerce/tachanka/authorization"
	"fmt"
	. "github.com/smartystreets/goconvey/convey"
)

func TestDevPortalProducts(t *testing.T) {
	dao := authorization.NewDevPortalProductDao(dynamoContext.CreateTestClient(dynamoContext.WaitTimeout, dynamoContext.WaitTimeout, "test-products"))
	err := dynamoContext.RegisterTestingDao(dao)
	Convey("Given a new Dynamo Client", t, func() {
		So(err, ShouldBeNil)

		Convey("When we put multiple mappings in the DAO", func() {
			for i := 1; i <= 10; i++ {
				dppId := uuid.NewV4().String()

				item := &authorization.DevPortalProduct{
					Id:   dppId,
					Name: fmt.Sprintf("Name %d", i),
				}

				err = dao.Put(item)
				So(err, ShouldBeNil)
			}

			Convey("We should be able to retrieve it", func() {
				dpps, err := dao.List()
				So(err, ShouldBeNil)
				So(len(dpps), ShouldEqual, 10)
			})
		})
	})
}
