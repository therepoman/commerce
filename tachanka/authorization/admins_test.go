package authorization_test

import (
	"github.com/satori/go.uuid"
	"testing"

	"code.justin.tv/commerce/tachanka/authorization"
	. "github.com/smartystreets/goconvey/convey"
)

func TestAdmin(t *testing.T) {
	dao := authorization.NewAdminDao(dynamoContext.CreateTestClient(dynamoContext.WaitTimeout, dynamoContext.WaitTimeout, "test-admins"))
	err := dynamoContext.RegisterTestingDao(dao)
	Convey("Given a new Dynamo Client", t, func() {
		So(err, ShouldBeNil)

		Convey("When we put an admin in the DAO", func() {
			admin := &authorization.Admin{
				Tuid: uuid.NewV4().String(),
			}
			err := dao.Put(admin)
			So(err, ShouldBeNil)

			Convey("We should be able to retrieve it", func() {
				fromDao, err := dao.Get(admin.Tuid)
				So(err, ShouldBeNil)
				So(fromDao.Equals(admin), ShouldBeTrue)
			})

			Convey("We should not be able to get a non existant tuid", func() {
				fromDao, err := dao.Get("GAFFO-ED")
				So(err, ShouldBeNil)
				So(fromDao, ShouldBeNil)
			})
		})
	})
}
