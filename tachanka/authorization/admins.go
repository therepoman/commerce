package authorization

import (
	"code.justin.tv/common/godynamo/client"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"

	"errors"
)

const (
	AdminHashKeyColumn string = "tuid"
	AdminReadCapacity  int64  = 100
	AdminWriteCapacity int64  = 100
)

type AdminDao struct {
	table  client.DynamoTable
	client *client.DynamoClient
}

type IAdminDao interface {
	Contains(tuid string) (bool, error)
	Get(tuid string) (*Admin, error)
	Put(admin *Admin) error
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *client.DynamoClientConfig
}

func (dao *AdminDao) DeleteTable() error {
	return dao.client.DeleteTable(dao.table)
}

func (dao *AdminDao) SetupTable() error {
	return dao.client.SetupTable(dao.table)
}

func (dao *AdminDao) GetClientConfig() *client.DynamoClientConfig {
	return dao.client.ClientConfig
}

func (dao *AdminDao) Contains(tuid string) (bool, error) {
	admin, err := dao.Get(tuid)
	if err != nil {
		return false, err
	}

	return admin != nil, nil
}

func (dao *AdminDao) Put(admin *Admin) error {
	return dao.client.PutItem(admin)
}

func (dao *AdminDao) Get(tuid string) (*Admin, error) {
	admin := &Admin{
		Tuid: tuid,
	}

	result, err := dao.client.GetItem(admin)
	if err != nil {
		return nil, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	adminResult, isAdmin := result.(*Admin)
	if !isAdmin {
		return adminResult, errors.New("Encountered an unexpected type while converting dynamo result to Admin")
	}

	return adminResult, nil
}

func NewAdminDao(client *client.DynamoClient) IAdminDao {
	dao := &AdminDao{}
	dao.client = client
	dao.table = &AdminsTable{}
	return dao
}

//DocumentsTable is the primary data store of documents throughout Tachanka. Documents
//are purposefully abstract. It can be viewed as a large hashmap keyed by document Id.
type AdminsTable struct{}

type Admin struct {
	client.BaseDynamoTableRecord
	Tuid string
}

func (t *AdminsTable) GetTableName() string {
	return "admins"
}

func (t *AdminsTable) NewCreateTableInput() *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(AdminReadCapacity),
			WriteCapacityUnits: aws.Int64(AdminWriteCapacity),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String(AdminHashKeyColumn),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String(AdminHashKeyColumn),
				KeyType:       aws.String("HASH"),
			},
		},
	}
}

func (t *AdminsTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (client.DynamoTableRecord, error) {
	tuid := client.StringFromAttributes(attributeMap, AdminHashKeyColumn)

	return &Admin{
		Tuid: tuid,
	}, nil
}

func (a *Admin) GetTable() client.DynamoTable {
	return &AdminsTable{}
}

func (a *Admin) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)

	client.PopulateAttributesString(itemMap, AdminHashKeyColumn, a.Tuid)
	return itemMap
}

func (a *Admin) NewItemKey() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)

	client.PopulateAttributesString(itemMap, AdminHashKeyColumn, a.Tuid)
	return itemMap
}

func (a *Admin) Equals(other client.DynamoTableRecord) bool {
	otherAdmin, isAdmin := other.(*Admin)
	if !isAdmin {
		return false
	} else {
		return a.Tuid == otherAdmin.Tuid
	}
}
