package authorization

import (
	"code.justin.tv/common/twirp"
	"code.justin.tv/gds/gds/portal/dpp/dpptwirp"
	"github.com/Sirupsen/logrus"
	"golang.org/x/net/context"
)

// Service object for hanging apis off of
type AuthService struct {
	adminDao IAdminDao
	dppDao   IDevPortalProductDao
}

func NewAuthorizationService(adminDao IAdminDao, dppDao IDevPortalProductDao) *AuthService {
	return &AuthService{
		adminDao: adminDao,
		dppDao:   dppDao,
	}
}

// HasPermission performs HasPermission query and returns response using twirp
func (S *AuthService) HasPermission(ctx context.Context, request *dpptwirp.HasPermissionRequest) (*dpptwirp.HasPermissionResponse, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":             "AuthService.HasPermission",
		"HasPermissionRequest": *request,
	})
	log.Debug("Entering")
	admin, err := S.adminDao.Get(request.UserId)
	if err != nil {
		log.WithError(err).Warn("Error fetching admin from adminDao")
		return nil, twirp.InternalError("Error reading from Dynamo. Please Retry.")
	}

	authorized := false
	if admin != nil {
		authorized = true
	}

	log.Debug("Exiting")
	return &dpptwirp.HasPermissionResponse{
		Result: authorized,
	}, nil
}

// Products performs Products query and returns response using twirp
func (S *AuthService) Products(ctx context.Context, request *dpptwirp.ProductsRequest) (*dpptwirp.ProductsResponse, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":        "AuthService.Products",
		"ProductsRequest": *request,
	})
	log.Debug("Entering")

	admin, err := S.adminDao.Get(request.UserId)
	if err != nil {
		log.WithError(err).Warn("Error fetching admin from adminDao")
		return nil, twirp.InternalError("Error reading from Dynamo. Please Retry.")
	}
	var outProducts []*dpptwirp.Product
	if admin != nil {
		products, err := S.dppDao.List()
		if err != nil {
			log.WithError(err).Warn("Error fetching Dev Portal Products from devPortalProductsDao")
			return nil, twirp.InternalError("Error reading from Dynamo. Please Retry")
		}
		outProducts = make([]*dpptwirp.Product, len(products))
		for i, product := range products {
			outProducts[i] = &dpptwirp.Product{
				Id:    product.Id,
				Title: product.Name,
			}
		}
	}

	log.Debug("Exiting")
	return &dpptwirp.ProductsResponse{
		Products: outProducts,
	}, nil
}
