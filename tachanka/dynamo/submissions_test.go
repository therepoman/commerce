package dynamo

import (
	"sort"
	gotesting "testing"
	"time"

	dynamoTest "code.justin.tv/commerce/tachanka/dynamo/test"
	"code.justin.tv/commerce/tachanka/models"
	"github.com/davecgh/go-spew/spew"
	"github.com/go-test/deep"
	"github.com/stretchr/testify/assert"
)

const (
	SubmissionTestTablePrefix string = "test-submission"
	SubmissionTestId          string = "TestSubmissionId"
	SubmissionTestDocId       string = "TestDocumentId1"
	SubmissionTestProductId   string = "TestProductId"
	SubmissionTestStepId1     string = "TestSubmissionStepId1"
	SubmissionTestStepId2     string = "TestSubmissionStepId2"
	SubmissionTestStepId3     string = "TestSubmissionStepId3"
)

func TestSubmissionTableEmptyGet(t *gotesting.T) {
	dao := NewSubmissionDao(dynamoTest.CreateTestClient(), dynamoTest.CreateTestConfiguration(SubmissionTestTablePrefix))
	assert.Nil(t, dynamoContext.RegisterDao(dao))

	submission, err := dao.Get(SubmissionTestId)
	assert.Nil(t, err)
	assert.Nil(t, submission)
}

func TestSubmissionPutAndGet(t *gotesting.T) {
	dao := NewSubmissionDao(dynamoTest.CreateTestClient(), dynamoTest.CreateTestConfiguration(SubmissionTestTablePrefix))
	assert.Nil(t, dynamoContext.RegisterDao(dao))

	submission := &models.Submission{
		Id:               SubmissionTestId,
		DocumentId:       SubmissionTestDocId,
		ManagedProductId: SubmissionTestProductId,
		SubmissionSteps: []models.SubmissionStep{{
			Id:          SubmissionTestStepId1,
			CreatedDate: time.Now(),
			Status:      models.SubmissionStepStatus_Waiting,
			Type:        models.SubmissionStepType_AdgSubmit,
		}},
	}

	err := dao.Put(submission)
	assert.Nil(t, err)

	fetchedSubmission, err := dao.Get(SubmissionTestId)
	assert.Nil(t, err)
	assert.NotNil(t, fetchedSubmission)

	diff := deep.Equal(fetchedSubmission, submission)
	assert.Nil(t, diff, "The Submissions should be equal: \n"+spew.Sdump(fetchedSubmission, submission))
}

func TestAddSubmissionStep(t *gotesting.T) {
	dao := NewSubmissionDao(dynamoTest.CreateTestClient(), dynamoTest.CreateTestConfiguration(SubmissionTestTablePrefix))
	assert.Nil(t, dynamoContext.RegisterDao(dao))

	submission := &models.Submission{
		Id:               SubmissionTestId,
		DocumentId:       SubmissionTestDocId,
		ManagedProductId: SubmissionTestProductId,
		SubmissionSteps: []models.SubmissionStep{{
			Id:          SubmissionTestStepId1,
			CreatedDate: time.Now(),
			Status:      models.SubmissionStepStatus_Running,
			Type:        models.SubmissionStepType_AdgCreateProduct,
		}},
	}

	err := dao.Put(submission)
	assert.Nil(t, err)

	submissionStep := models.SubmissionStep{
		Id:          SubmissionTestStepId2,
		CreatedDate: time.Now(),
		Status:      models.SubmissionStepStatus_Complete,
		Type:        models.SubmissionStepType_AdgCreateProduct,
	}

	// Add a step
	submission.SubmissionSteps = append(submission.SubmissionSteps, submissionStep)
	fetchedSubmission, err := dao.AddStep(submission.Id, &submissionStep)
	assert.Nil(t, err)
	assert.NotNil(t, fetchedSubmission)

	// Check the timestamps
	assert.True(t, submission.LastUpdated.Before(fetchedSubmission.LastUpdated), "Updated Submission should have more recent LastUpdated time")

	// Reset timestamp before doing comparison
	submission.LastUpdated = fetchedSubmission.LastUpdated

	diff := deep.Equal(fetchedSubmission, submission)
	assert.Nil(t, diff, "The Submissions should be equal: \n"+spew.Sdump(fetchedSubmission, submission))
}

func TestSubmissionStatusUpdate(t *gotesting.T) {
	dao := NewSubmissionDao(dynamoTest.CreateTestClient(), dynamoTest.CreateTestConfiguration(SubmissionTestTablePrefix))
	assert.Nil(t, dynamoContext.RegisterDao(dao))

	submission := &models.Submission{
		Id:               SubmissionTestId,
		DocumentId:       SubmissionTestDocId,
		ManagedProductId: SubmissionTestProductId,
		Status:           models.SubmissionStatus_InProgress,
		SubmissionSteps: []models.SubmissionStep{{
			Id:          SubmissionTestStepId1,
			CreatedDate: time.Now(),
			Status:      models.SubmissionStepStatus_Running,
			Type:        models.SubmissionStepType_AdgCreateProduct,
		}},
	}

	err := dao.Put(submission)
	assert.Nil(t, err)

	// Update the status
	submission.Status = models.SubmissionStatus_Complete
	fetchedSubmission, err := dao.UpdateStatus(submission.Id, models.SubmissionStatus_Complete)
	assert.Nil(t, err)
	assert.NotNil(t, fetchedSubmission)

	// Check the timestamps
	assert.True(t, submission.LastUpdated.Before(fetchedSubmission.LastUpdated), "Updated Submission should have more recent LastUpdated time")

	// Reset timestamp before doing comparison
	submission.LastUpdated = fetchedSubmission.LastUpdated

	diff := deep.Equal(fetchedSubmission, submission)
	assert.Nil(t, diff, "The Submissions should be equal: \n"+spew.Sdump(fetchedSubmission, submission))
}

func TestGetSubmissionsByManagedProduct(t *gotesting.T) {
	dao := NewSubmissionDao(dynamoTest.CreateTestClient(), dynamoTest.CreateTestConfiguration(SubmissionTestTablePrefix))
	assert.Nil(t, dynamoContext.RegisterDao(dao))

	// Create a couple submissions with the same managed product id
	var submissions []models.Submission

	submission := newValidSubmission()
	err := dao.Put(submission)
	assert.Nil(t, err)

	submissions = append(submissions, *submission)

	submission = newValidSubmission()
	submission.Id = "RandomId"
	err = dao.Put(submission)
	assert.Nil(t, err)

	submissions = append(submissions, *submission)

	// Create a submission that has a different managed product id
	submission = newValidSubmission()
	submission.Id = "AnotherRandomId"
	submission.ManagedProductId = "DifferentManagedProductId"
	err = dao.Put(submission)
	assert.Nil(t, err)

	// Query by managed product id
	results, err := dao.GetByManagedProduct(SubmissionTestProductId)
	assert.Nil(t, err)
	assert.NotNil(t, results)
	assert.Len(t, results, 2)

	// Sort the slices for easy comparison
	sort.Slice(submissions, func(i, j int) bool {
		return submissions[i].Id < submissions[j].Id
	})

	diff := deep.Equal(results, submissions)
	assert.Nil(t, diff, "The Submissions should be equal: \n"+spew.Sdump(results, submissions))
}

func TestSubmissionUpdateAndGet(t *gotesting.T) {
	dao := NewSubmissionDao(dynamoTest.CreateTestClient(), dynamoTest.CreateTestConfiguration(SubmissionTestTablePrefix))
	assert.Nil(t, dynamoContext.RegisterDao(dao))

	submission := &models.Submission{
		Id:               SubmissionTestId,
		DocumentId:       SubmissionTestDocId,
		ManagedProductId: SubmissionTestProductId,
		Status:           models.SubmissionStatus_InProgress,
		SubmissionSteps: []models.SubmissionStep{{
			Id:          SubmissionTestStepId1,
			CreatedDate: time.Now(),
			Status:      models.SubmissionStepStatus_Error,
			Type:        models.SubmissionStepType_AdgCreateProduct,
		}, {
			Id:          SubmissionTestStepId2,
			CreatedDate: time.Now(),
			Status:      models.SubmissionStepStatus_Running,
			Type:        models.SubmissionStepType_AdgSubmit,
		}},
	}

	err := dao.Put(submission)
	assert.Nil(t, err)

	// Update the steps
	submission.SubmissionSteps = append(submission.SubmissionSteps, models.SubmissionStep{
		Id:          SubmissionTestStepId3,
		CreatedDate: time.Now(),
		Status:      models.SubmissionStepStatus_Complete,
		Type:        models.SubmissionStepType_AdgSubmit,
	})

	fetchedSubmission, err := dao.Update(submission)
	assert.Nil(t, err, "Error encountered while updating the submission")
	assert.NotNil(t, fetchedSubmission)

	// Check the timestamps
	assert.True(t, submission.LastUpdated.Before(fetchedSubmission.LastUpdated), "Updated Submission should have more recent LastUpdated time")

	// Reset timestamp before doing comparison
	submission.LastUpdated = fetchedSubmission.LastUpdated

	diff := deep.Equal(fetchedSubmission, submission)
	assert.Nil(t, diff, "The Submissions should be equal: \n"+spew.Sdump(fetchedSubmission, submission))
}

func newValidSubmission() *models.Submission {
	return &models.Submission{
		Id:               SubmissionTestId,
		DocumentId:       SubmissionTestDocId,
		ManagedProductId: SubmissionTestProductId,
		Status:           models.SubmissionStatus_InProgress,
		SubmissionSteps: []models.SubmissionStep{{
			Id:          SubmissionTestStepId1,
			CreatedDate: time.Now(),
			Status:      models.SubmissionStepStatus_Running,
			Type:        models.SubmissionStepType_AdgCreateProduct,
		}},
	}
}
