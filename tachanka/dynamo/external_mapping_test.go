package dynamo

import (
	"fmt"
	gotesting "testing"
	"time"

	dynamoTest "code.justin.tv/commerce/tachanka/dynamo/test"
	"code.justin.tv/commerce/tachanka/models"
	"github.com/davecgh/go-spew/spew"
	"github.com/go-test/deep"
	"github.com/stretchr/testify/assert"
)

const (
	ExternalMappingTestTablePrefix string                         = "test-external-mapping"
	ExternalMappingTestId          string                         = "TestParentId"
	ExternalMappingTestType        models.ExternalMappingTypeEnum = models.EM_ADGProductId
	ExternalMappingTestValue1      string                         = "TestMappingValue1"
	ExternalMappingTestValue2      string                         = "TestMappingValue2"
)

func TestExternalMappingTableEmptyGet(t *gotesting.T) {
	dao := NewExternalMappingDao(dynamoTest.CreateTestClient(), dynamoTest.CreateTestConfiguration(ExternalMappingTestTablePrefix))
	assert.Nil(t, dynamoContext.RegisterDao(dao))

	mapping, err := dao.Get(ExternalMappingTestId, ExternalMappingTestType)
	assert.Nil(t, err)
	assert.Nil(t, mapping)
}

func TestPutAndGetMapping(t *gotesting.T) {
	dao := NewExternalMappingDao(dynamoTest.CreateTestClient(), dynamoTest.CreateTestConfiguration(ExternalMappingTestTablePrefix))
	assert.Nil(t, dynamoContext.RegisterDao(dao))

	mapping1 := &models.ExternalMapping{
		ParentId: ExternalMappingTestId,
		Type:     ExternalMappingTestType,
		Value:    ExternalMappingTestValue1,
	}
	err := dao.Put(mapping1)
	assert.Nil(t, err)

	mapping2 := &models.ExternalMapping{
		ParentId: ExternalMappingTestId,
		Type:     models.EM_ADGDomainId,
		Value:    ExternalMappingTestValue2,
	}
	err = dao.Put(mapping2)
	assert.Nil(t, err)

	fetchedMapping1, err := dao.Get(ExternalMappingTestId, ExternalMappingTestType)
	fmt.Println("fetchedMapping: ", fetchedMapping1)
	assert.Nil(t, err)
	assert.NotNil(t, fetchedMapping1)
	diff := deep.Equal(fetchedMapping1, mapping1)
	assert.Nil(t, diff, "The Mappings should be equal: \n"+spew.Sdump(fetchedMapping1, mapping1))

	fetchedMapping2, err := dao.Get(ExternalMappingTestId, models.EM_ADGDomainId)
	assert.Nil(t, err)
	assert.NotNil(t, fetchedMapping2)
	diff = deep.Equal(fetchedMapping2, mapping2)
	assert.Nil(t, diff, "The Mappings should be equal: \n"+spew.Sdump(fetchedMapping2, mapping2))
}

func TestUpdateMapping(t *gotesting.T) {
	dao := NewExternalMappingDao(dynamoTest.CreateTestClient(), dynamoTest.CreateTestConfiguration(ExternalMappingTestTablePrefix))
	assert.Nil(t, dynamoContext.RegisterDao(dao))

	firstUpdated := time.Now()

	mapping := &models.ExternalMapping{
		ParentId:    ExternalMappingTestId,
		Type:        ExternalMappingTestType,
		Value:       ExternalMappingTestValue1,
		LastUpdated: firstUpdated,
	}
	err := dao.Put(mapping)
	assert.Nil(t, err)

	// Update the value
	mapping.Value = ExternalMappingTestValue2
	fetchedMapping, err := dao.Update(mapping)
	assert.Nil(t, err)
	assert.NotNil(t, fetchedMapping)

	// Check the timestamps
	assert.True(t, firstUpdated.Before(fetchedMapping.LastUpdated), "Updated Mapping should have more recent LastUpdated time")

	// Reset timestamp before doing comparison
	mapping.LastUpdated = fetchedMapping.LastUpdated

	diff := deep.Equal(fetchedMapping, mapping)
	assert.Nil(t, diff, "The Mappings should be equal: \n"+spew.Sdump(fetchedMapping, mapping))
}

func TestGetByValueMapping(t *gotesting.T) {
	dao := NewExternalMappingDao(dynamoTest.CreateTestClient(), dynamoTest.CreateTestConfiguration(ExternalMappingTestTablePrefix))
	assert.Nil(t, dynamoContext.RegisterDao(dao))

	mapping := &models.ExternalMapping{
		ParentId: ExternalMappingTestId,
		Type:     ExternalMappingTestType,
		Value:    ExternalMappingTestValue1,
	}
	err := dao.Put(mapping)
	assert.Nil(t, err)

	fetchedMappings, err := dao.GetByMappingValue(ExternalMappingTestValue1, ExternalMappingTestType)
	assert.Nil(t, err)
	assert.NotNil(t, fetchedMappings)
	fetchedMapping := (*fetchedMappings)[0]
	diff := deep.Equal(fetchedMapping, *mapping)
	assert.Nil(t, diff, "The Mappings should be equal: \n"+spew.Sdump(fetchedMapping, mapping))
}
