package dynamo

import (
	"code.justin.tv/common/godynamo/client"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"

	"code.justin.tv/commerce/tachanka/models"
	"errors"
)

const (
	ManagedProductsHashKeyColumn string = "id"
	ManagedProductTypeColumn     string = "type"
	LiveSubmissionIdColumn       string = "live-submission-id"
	DraftDocumentIdColumn        string = "draft-document-id"
	WorkingSubmissionIdColumn    string = "working-submission-id"
	ManagedProductADGSkuColumn   string = "adg-sku"

	ManagedProductsReadCapacity  int64 = 100
	ManagedProductsWriteCapacity int64 = 100
)

type ManagedProductDao struct {
	table  client.DynamoTable
	client *client.DynamoClient
}

type IManagedProductDao interface {
	Get(managedProductId string) (*ManagedProductDynamoEntry, error)
	Put(managedProduct *ManagedProductDynamoEntry) error
	Update(managedProduct *ManagedProductDynamoEntry) error
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *client.DynamoClientConfig
}

func (dao *ManagedProductDao) DeleteTable() error {
	return dao.client.DeleteTable(dao.table)
}

func (dao *ManagedProductDao) SetupTable() error {
	return dao.client.SetupTable(dao.table)
}

func (dao *ManagedProductDao) GetClientConfig() *client.DynamoClientConfig {
	return dao.client.ClientConfig
}

func (dao *ManagedProductDao) Put(product *ManagedProductDynamoEntry) error {
	return dao.client.PutItem(product)
}

func (dao *ManagedProductDao) Update(product *ManagedProductDynamoEntry) error {
	return dao.client.UpdateItem(product)
}

func (dao *ManagedProductDao) Get(managedProductId string) (*ManagedProductDynamoEntry, error) {
	managedProduct := &ManagedProductDynamoEntry{
		Id: managedProductId,
	}

	result, err := dao.client.GetItem(managedProduct)
	if err != nil {
		return nil, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	managedProductResult, isManagedProduct := result.(*ManagedProductDynamoEntry)
	if !isManagedProduct {
		return nil, errors.New("Encountered an unexpected type while converting dynamo result to Managed Product")
	}

	return managedProductResult, nil
}

func NewManagedProductDao(client *client.DynamoClient) IManagedProductDao {
	dao := &ManagedProductDao{}
	dao.client = client
	dao.table = &ManagedProductsTable{}
	return dao
}

// This table maintains all the products managed within our service.
// Each dev portal product can manage multiple products that are managed in Tachanka, a row in this table
// represents one such product, uniquely defined by its ManagedProductId. The relationship between these
// managed products and dev portal identifiers is maintained in the DevPortalOwnership table.
type ManagedProductsTable struct{}

type ManagedProductDynamoEntry struct {
	client.BaseDynamoTableRecord
	Id                  string
	ManagedProductType  models.ManagedProductType
	LiveSubmissionId    *string
	DraftDocumentId     *string
	WorkingSubmissionId *string
}

func (t *ManagedProductsTable) GetTableName() string {
	return "managed-products"
}

func (t *ManagedProductsTable) NewCreateTableInput() *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(int64(ManagedProductsReadCapacity)),
			WriteCapacityUnits: aws.Int64(int64(ManagedProductsWriteCapacity)),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String(ManagedProductsHashKeyColumn),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String(ManagedProductsHashKeyColumn),
				KeyType:       aws.String("HASH"),
			},
		},
	}
}

func (t *ManagedProductsTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (client.DynamoTableRecord, error) {
	item, err := t.convertAttributeMapToRecord(attributeMap)
	return item, err
}

func (t *ManagedProductsTable) convertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (*ManagedProductDynamoEntry, error) {
	productType, err := models.StringToManagedProductType(client.StringFromAttributes(attributeMap, ManagedProductTypeColumn))

	if err != nil {
		return nil, err
	}

	return &ManagedProductDynamoEntry{
		Id:                  client.StringFromAttributes(attributeMap, ManagedProductsHashKeyColumn),
		ManagedProductType:  productType,
		LiveSubmissionId:    client.OptionalStringFromAttributes(attributeMap, LiveSubmissionIdColumn),
		DraftDocumentId:     client.OptionalStringFromAttributes(attributeMap, DraftDocumentIdColumn),
		WorkingSubmissionId: client.OptionalStringFromAttributes(attributeMap, WorkingSubmissionIdColumn),
	}, nil
}

func (mp *ManagedProductDynamoEntry) GetTable() client.DynamoTable {
	return &ManagedProductsTable{}
}

func (mp *ManagedProductDynamoEntry) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	client.PopulateAttributesString(itemMap, ManagedProductsHashKeyColumn, mp.Id)
	client.PopulateAttributesString(itemMap, ManagedProductTypeColumn, string(mp.ManagedProductType))
	client.PopulateAttributesOptionalString(itemMap, LiveSubmissionIdColumn, mp.LiveSubmissionId)
	client.PopulateAttributesOptionalString(itemMap, DraftDocumentIdColumn, mp.DraftDocumentId)
	client.PopulateAttributesOptionalString(itemMap, WorkingSubmissionIdColumn, mp.WorkingSubmissionId)
	return itemMap
}

func (mp *ManagedProductDynamoEntry) NewItemKey() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	client.PopulateAttributesString(itemMap, ManagedProductsHashKeyColumn, mp.Id)
	return itemMap
}

func (mp *ManagedProductDynamoEntry) Equals(other client.DynamoTableRecord) bool {
	otherProduct, isProduct := other.(*ManagedProductDynamoEntry)
	if !isProduct {
		return false
	} else {
		return mp.Id == otherProduct.Id &&
			mp.ManagedProductType == otherProduct.ManagedProductType
	}
}
