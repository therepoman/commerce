package dynamo

const (
	noItemFoundString        string = "dynamo: no item found"
	tableNamePrefixSeperator string = "_"
)

func IsNoItemFoundError(err error) bool {
	return err.Error() == noItemFoundString
}

func FormatTableName(prefix string, name string) string {
	return prefix + tableNamePrefixSeperator + name
}
