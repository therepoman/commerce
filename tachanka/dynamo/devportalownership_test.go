package dynamo

import (
	"code.justin.tv/common/godynamo/testing"

	gotesting "testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestDevPortalOwnershipEmptyGet(t *gotesting.T) {
	dao := NewDevPortalOwnershipDao(deprecatedDynamoContext.CreateTestClient(deprecatedDynamoContext.WaitTimeout, deprecatedDynamoContext.WaitTimeout, "test-empty-dev-ownership-get"))
	assert.Nil(t, deprecatedDynamoContext.RegisterTestingDao(dao))

	dppId := "Pannick Yannick"
	product, err := dao.Get(dppId)
	assert.Nil(t, err)
	assert.Equal(t, 0, len(product))
}

func TestDevPortalOwnershipPut(t *gotesting.T) {
	dao := NewDevPortalOwnershipDao(deprecatedDynamoContext.CreateTestClient(deprecatedDynamoContext.WaitTimeout, deprecatedDynamoContext.WaitTimeout, "test-update-dev-ownership-get"))
	assert.Nil(t, deprecatedDynamoContext.RegisterTestingDao(dao))

	dppId := "portal id"
	mpId := "Pannick Yannick"

	item := &DevPortalOwnershipEntry{
		DevPortalProductId: dppId,
		ManagedProductId:   mpId,
	}

	err := dao.Put(item)
	assert.Nil(t, err)

	// Now we get it back. It should be equal to what we started with.
	entries, err := dao.Get(dppId)

	assert.Nil(t, err)
	assert.Equal(t, 1, len(entries))
	assert.True(t, item.Equals(entries[0]))
}

func TestDevPortalOwnershipPutThenGetList(t *gotesting.T) {
	dao := NewDevPortalOwnershipDao(deprecatedDynamoContext.CreateTestClient(deprecatedDynamoContext.WaitTimeout, deprecatedDynamoContext.WaitTimeout, "test-put-dev-ownership-get-list"))
	assert.Nil(t, deprecatedDynamoContext.RegisterTestingDao(dao))

	now := time.Now()

	dppId := "portal id"
	mpId1 := "Pannick Yannick"
	mpId2 := "Titanic Yannick"
	mpId3 := "Slammit Yannick"

	item1 := &DevPortalOwnershipEntry{
		DevPortalProductId: dppId,
		ManagedProductId:   mpId1,
	}

	item2 := &DevPortalOwnershipEntry{
		DevPortalProductId: dppId,
		ManagedProductId:   mpId2,
	}

	item3 := &DevPortalOwnershipEntry{
		DevPortalProductId: dppId,
		ManagedProductId:   mpId3,
	}

	err := dao.Put(item1)
	assert.Nil(t, err)

	err = dao.Put(item2)
	assert.Nil(t, err)

	err = dao.Put(item3)
	assert.Nil(t, err)

	// 3 items are in the table now, lets see if we can scan them.
	// First we create a filter.
	products, err := dao.Get(dppId)
	assert.Nil(t, err)

	// We should get back 3 items.
	assert.Equal(t, 3, len(products))
	testResult, products := findEqualItemAndRemove(products, item1, now)
	assert.True(t, testResult)
	testResult, products = findEqualItemAndRemove(products, item2, now)
	assert.True(t, testResult)
	testResult, products = findEqualItemAndRemove(products, item3, now)
	assert.True(t, testResult)
	assert.Equal(t, 0, len(products))
}

func findEqualItemAndRemove(items []*DevPortalOwnershipEntry, item *DevPortalOwnershipEntry, now time.Time) (bool, []*DevPortalOwnershipEntry) {
	foundIndex := 0
	for i, listItem := range items {
		if testing.EqualsWithExpectedLastUpdated(listItem, item, now, deprecatedDynamoContext.AcceptableUpdateTimeDelta) {
			foundIndex = i
			break
		}
	}

	if foundIndex != -1 {
		items = append(items[:foundIndex], items[foundIndex+1:]...)
		return true, items
	} else {
		return false, items
	}
}
