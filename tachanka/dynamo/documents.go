package dynamo

import (
	"code.justin.tv/common/godynamo/client"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"

	"encoding/json"
	"errors"
)

const (
	DocumentVersion1_0     DocumentVersion = "v1.0"
	DocumentVersion2_0     DocumentVersion = "v2.0"
	DocumentVersionUnknown DocumentVersion = "Unknown"

	DocumentHashKeyColumn    string = "id"
	DocumentVersionColumn    string = "documentVersion"
	DocumentDataColumn       string = "documentData"
	DocumentManagedProductID string = "managedProductId"
	DocumentsReadCapacity    int64  = 100
	DocumentsWriteCapacity   int64  = 100
)

type DocumentVersion string

type DocumentDao struct {
	table  client.DynamoTable
	client *client.DynamoClient
}

type IDocumentDao interface {
	Get(documentId string) (*Document, error)
	Put(document *Document) error
	Update(document *Document) error
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *client.DynamoClientConfig
}

func (dao *DocumentDao) DeleteTable() error {
	return dao.client.DeleteTable(dao.table)
}

func (dao *DocumentDao) SetupTable() error {
	return dao.client.SetupTable(dao.table)
}

func (dao *DocumentDao) GetClientConfig() *client.DynamoClientConfig {
	return dao.client.ClientConfig
}

func (dao *DocumentDao) Put(document *Document) error {
	return dao.client.PutItem(document)
}

func (dao *DocumentDao) Update(document *Document) error {
	return dao.client.UpdateItem(document)
}

func (dao *DocumentDao) Get(documentId string) (*Document, error) {
	document := &Document{
		DocumentId: documentId,
	}

	result, err := dao.client.GetItem(document)
	if err != nil {
		return nil, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	documentResult, isDocument := result.(*Document)
	if !isDocument {
		return documentResult, errors.New("Encountered an unexpected type while converting dynamo result to Document")
	}

	return documentResult, nil
}

func NewDocumentDao(client *client.DynamoClient) IDocumentDao {
	dao := &DocumentDao{}
	dao.client = client
	dao.table = &DocumentsTable{}
	return dao
}

//DocumentsTable is the primary data store of documents throughout Tachanka. Documents
//are purposefully abstract. It can be viewed as a large hashmap keyed by document id.
type DocumentsTable struct{}

const (
	DocumentType string = "Document-Type"
)

type Document struct {
	client.BaseDynamoTableRecord
	DocumentId       string
	ManagedProductId string
	DocumentVersion  DocumentVersion
	DocumentData     string
}

func documentVersionFromString(s string) DocumentVersion {
	if string(DocumentVersion1_0) == s {
		return DocumentVersion1_0
	} else if string(DocumentVersion2_0) == s {
		return DocumentVersion2_0
	} else {
		return DocumentVersionUnknown
	}
}

func (t *DocumentsTable) GetTableName() string {
	return "documents"
}

func (t *DocumentsTable) NewCreateTableInput() *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(DocumentsReadCapacity),
			WriteCapacityUnits: aws.Int64(DocumentsWriteCapacity),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String(DocumentHashKeyColumn),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String(DocumentHashKeyColumn),
				KeyType:       aws.String("HASH"),
			},
		},
	}
}

func (t *DocumentsTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (client.DynamoTableRecord, error) {
	serializedDoc := client.StringFromAttributes(attributeMap, DocumentDataColumn)
	documentId := client.StringFromAttributes(attributeMap, DocumentHashKeyColumn)
	managedProductId := client.StringFromAttributes(attributeMap, DocumentManagedProductID)
	documentVersion := documentVersionFromString(client.StringFromAttributes(attributeMap, DocumentVersionColumn))

	return &Document{
		DocumentId:       documentId,
		DocumentVersion:  documentVersion,
		DocumentData:     serializedDoc,
		ManagedProductId: managedProductId,
	}, nil
}

func (d *Document) GetTable() client.DynamoTable {
	return &DocumentsTable{}
}

func (d *Document) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)

	client.PopulateAttributesString(itemMap, DocumentVersionColumn, string(d.DocumentVersion))
	client.PopulateAttributesString(itemMap, DocumentHashKeyColumn, d.DocumentId)
	client.PopulateAttributesString(itemMap, DocumentDataColumn, d.DocumentData)
	client.PopulateAttributesString(itemMap, DocumentManagedProductID, d.ManagedProductId)
	return itemMap
}

func (d *Document) NewItemKey() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	client.PopulateAttributesString(itemMap, DocumentHashKeyColumn, d.DocumentId)
	return itemMap
}

func (d *Document) Equals(other client.DynamoTableRecord) bool {
	otherDocument, isDocument := other.(*Document)
	if !isDocument {
		return false
	} else {
		// Ignoring doc data for performance considerations. Don't really want to do a deep map compare...
		return d.DocumentId == otherDocument.DocumentId &&
			d.ManagedProductId == otherDocument.ManagedProductId &&
			string(d.DocumentVersion) == string(otherDocument.DocumentVersion)
	}
}

// This will have to be something smarter likely, but for now we are assuming our document data is identical
func (d *Document) ToADGJsonBytes() ([]byte, error) {
	var data, err = json.Marshal(d.DocumentData)
	if err != nil {
		return nil, err
	}

	return data, nil
}
