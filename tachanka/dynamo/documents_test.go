package dynamo

import (
	"github.com/stretchr/testify/assert"

	"encoding/json"
	gotesting "testing"

	"github.com/satori/go.uuid"
)

func TestDocumentTableEmptyGet(t *gotesting.T) {
	dao := NewDocumentDao(deprecatedDynamoContext.CreateTestClient(deprecatedDynamoContext.WaitTimeout, deprecatedDynamoContext.WaitTimeout, "test-empty-document-get"))
	assert.Nil(t, deprecatedDynamoContext.RegisterTestingDao(dao))

	docId := "Pannick Yannick"
	document, err := dao.Get(docId)
	assert.Nil(t, err)
	assert.Nil(t, document)
}

func TestDocumentPutAndGet(t *gotesting.T) {
	dao := NewDocumentDao(deprecatedDynamoContext.CreateTestClient(deprecatedDynamoContext.WaitTimeout, deprecatedDynamoContext.WaitTimeout, "test-put-document-get"))
	assert.Nil(t, deprecatedDynamoContext.RegisterTestingDao(dao))

	theMap := make(map[string]interface{})
	title := "A game too cool for school"
	isGood := true
	price := 3.46
	managedProductId := uuid.NewV4().String()
	theMap["title"] = title
	theMap["isGood"] = isGood
	theMap["price"] = price

	theData, err := json.Marshal(theMap)
	assert.Nil(t, err)

	documentId := "Pannick Yannick"
	documentVersion := DocumentVersion1_0
	document := &Document{
		DocumentId:       documentId,
		DocumentVersion:  documentVersion,
		DocumentData:     string(theData),
		ManagedProductId: managedProductId,
	}

	err = dao.Put(document)
	assert.Nil(t, err)

	fetchedDocument, err := dao.Get(documentId)
	assert.Nil(t, err)
	assert.Equal(t, documentId, fetchedDocument.DocumentId)
	assert.Equal(t, string(documentVersion), string(fetchedDocument.DocumentVersion))

	var docStruct map[string]interface{}
	err = json.Unmarshal([]byte(fetchedDocument.DocumentData), &docStruct)

	assert.Equal(t, title, docStruct["title"])
	assert.Equal(t, isGood, docStruct["isGood"])
	assert.Equal(t, price, docStruct["price"])
	assert.Equal(t, managedProductId, fetchedDocument.ManagedProductId)
}

func TestDocumentUpdateAndGet(t *gotesting.T) {
	dao := NewDocumentDao(deprecatedDynamoContext.CreateTestClient(deprecatedDynamoContext.WaitTimeout, deprecatedDynamoContext.WaitTimeout, "test-update-document-get"))
	assert.Nil(t, deprecatedDynamoContext.RegisterTestingDao(dao))

	theMap := make(map[string]interface{})
	title := "A game too cool for school"
	isGood := true
	price := 3.46
	price2 := 5.02
	theMap["title"] = title
	theMap["isGood"] = isGood
	theMap["price"] = price

	theData, err := json.Marshal(theMap)
	assert.Nil(t, err)

	documentId := "Pannick Yannick"
	documentVersion := DocumentVersion1_0
	documentVersion2 := DocumentVersionUnknown
	managedProductId := uuid.NewV4().String()
	document := &Document{
		DocumentId:       documentId,
		DocumentVersion:  documentVersion,
		DocumentData:     string(theData),
		ManagedProductId: managedProductId,
	}

	err = dao.Put(document)
	assert.Nil(t, err)

	// Update the doc now.
	theMap["price"] = price2

	theData2, err := json.Marshal(theMap)
	assert.Nil(t, err)

	document.DocumentData = string(theData2)
	document.DocumentVersion = documentVersion2
	err = dao.Update(document)
	assert.Nil(t, err)

	fetchedDocument, err := dao.Get(documentId)
	assert.Nil(t, err)
	assert.Equal(t, documentId, fetchedDocument.DocumentId)
	assert.Equal(t, string(documentVersion2), string(fetchedDocument.DocumentVersion))

	var docStruct map[string]interface{}
	err = json.Unmarshal([]byte(fetchedDocument.DocumentData), &docStruct)
	assert.Nil(t, err)

	assert.Equal(t, title, docStruct["title"])
	assert.Equal(t, isGood, docStruct["isGood"])
	assert.Equal(t, price2, docStruct["price"])
	assert.Equal(t, managedProductId, fetchedDocument.ManagedProductId)
}
