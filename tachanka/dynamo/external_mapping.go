package dynamo

import (
	"time"

	"code.justin.tv/commerce/tachanka/config"
	test "code.justin.tv/commerce/tachanka/dynamo/test"
	"code.justin.tv/commerce/tachanka/models"
	"github.com/Sirupsen/logrus"
	db "github.com/guregu/dynamo"
)

type ExternalProductTypeEnum string

const (
	ExternalMappingTableName         string = "external-mapping"
	ExternalMappingHashKeyColumn     string = "parent-id"
	ExternalMappingTypeColumn        string = "type"
	ExternalMappingValueColumn       string = "value"
	ExternalMappingLastUpdatedColumn string = "lastUpdated"
	ExternalMappingReadCapacity      int64  = 100
	ExternalMappingWriteCapacity     int64  = 100
)

type (
	DynamoExternalMappingDao struct {
		table  db.Table
		client *db.DB
	}

	ExternalMappingDao interface {
		Put(externalMapping *models.ExternalMapping) error
		Get(parentId string, mappingType models.ExternalMappingTypeEnum) (*models.ExternalMapping, error)
		Update(externalMapping *models.ExternalMapping) (*models.ExternalMapping, error)
		GetByMappingValue(value string, mappingType models.ExternalMappingTypeEnum) (*[]models.ExternalMapping, error)
		DeleteTable() error
		CreateTable() error
	}
)

func (dao *DynamoExternalMappingDao) Put(externalMapping *models.ExternalMapping) error {
	externalMapping.LastUpdated = time.Now()

	return dao.table.Put(externalMapping).Run()
}

func (dao *DynamoExternalMappingDao) Get(parentId string, mappingType models.ExternalMappingTypeEnum) (*models.ExternalMapping, error) {
	var result models.ExternalMapping

	err := dao.table.Get(ExternalMappingHashKeyColumn, parentId).Range(ExternalMappingTypeColumn, db.Equal, mappingType).One(&result)
	if err != nil && IsNoItemFoundError(err) {
		return nil, nil
	}
	return &result, err
}

func (dao *DynamoExternalMappingDao) GetByMappingValue(value string, mappingType models.ExternalMappingTypeEnum) (*[]models.ExternalMapping, error) {
	var results []models.ExternalMapping

	err := dao.table.Scan().Filter("$ = ? AND $ = ?", ExternalMappingValueColumn, value, ExternalMappingTypeColumn, mappingType).All(&results)
	if err != nil && IsNoItemFoundError(err) {
		return &results, nil
	}
	return &results, err
}

func (dao *DynamoExternalMappingDao) Update(externalMapping *models.ExternalMapping) (*models.ExternalMapping, error) {
	var result models.ExternalMapping

	err := dao.table.Update(ExternalMappingHashKeyColumn, externalMapping.ParentId).
		Range(ExternalMappingTypeColumn, externalMapping.Type).
		Set(ExternalMappingValueColumn, externalMapping.Value).
		Set(ExternalMappingLastUpdatedColumn, time.Now()).
		Value(&result)
	return &result, err
}

func NewExternalMappingDao(client *db.DB, serviceConfig *config.Configuration) ExternalMappingDao {
	dao := &DynamoExternalMappingDao{}
	dao.client = client
	dao.table = client.Table(FormatTableName(serviceConfig.DynamoTablePrefix, ExternalMappingTableName))
	return dao
}

// Deletes the ExternalMapping table. FOR TESTING ONLY. DON'T EVEN
func (dao *DynamoExternalMappingDao) DeleteTable() error {
	return dao.table.DeleteTable().Run()
}

// Creates a table for ExternalMappingDao. FOR TESTING ONLY
func (dao *DynamoExternalMappingDao) CreateTable() error {
	if test.DoesTableExist(dao.table.Name(), dao.client) {
		return nil
	}

	err := dao.client.CreateTable(dao.table.Name(), models.ExternalMapping{}).
		Provision(ExternalMappingReadCapacity, ExternalMappingWriteCapacity).
		Run()
	if err != nil {
		logrus.Error("Table creation error: ", err)
	}

	return err
}
