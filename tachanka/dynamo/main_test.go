package dynamo

import (
	dynamoTest "code.justin.tv/commerce/tachanka/dynamo/test"
	dynamoTesting "code.justin.tv/common/godynamo/testing"

	"os"

	"testing"
)

var deprecatedDynamoContext *dynamoTesting.DynamoTestContext // Deprecated
var dynamoContext dynamoTest.DynamoTestContext

func TestMain(m *testing.M) {
	var err error

	// Deprecated - TODO remove after dynamo migration is complete
	deprecatedDynamoContext, err = dynamoTesting.NewDynamoTestContext("dynamo")
	if err != nil {
		os.Exit(1)
	}
	// END Deprecated

	dynamoContext := dynamoTest.DynamoTestContext{}

	exitCode := m.Run()

	// Deprecated - TODO remove after dynamo migration is complete
	err = deprecatedDynamoContext.Cleanup()
	if err != nil {
		os.Exit(1)
	}
	// END Deprecated

	err = dynamoContext.Cleanup()
	if err != nil {
		os.Exit(1)
	}

	os.Exit(exitCode)
}
