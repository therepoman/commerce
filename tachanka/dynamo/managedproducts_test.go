package dynamo

import (
	gotesting "testing"

	"code.justin.tv/commerce/tachanka/models"
	"github.com/stretchr/testify/assert"
)

func TestManagedProductsEmptyGet(t *gotesting.T) {
	dao := NewManagedProductDao(deprecatedDynamoContext.CreateTestClient(deprecatedDynamoContext.WaitTimeout, deprecatedDynamoContext.WaitTimeout, "test-empty-product-get"))
	assert.Nil(t, deprecatedDynamoContext.RegisterTestingDao(dao))

	mpId := "Pannick Yannick"
	product, err := dao.Get(mpId)
	assert.Nil(t, err)
	assert.Nil(t, product)
}

func TestManagedProductsPutAndUpdate(t *gotesting.T) {
	dao := NewManagedProductDao(deprecatedDynamoContext.CreateTestClient(deprecatedDynamoContext.WaitTimeout, deprecatedDynamoContext.WaitTimeout, "test-update-product-get"))
	assert.Nil(t, deprecatedDynamoContext.RegisterTestingDao(dao))

	mpId := "Pannick Yannick"
	submissionValue := "submit"
	updatedSubmissionValue := "updatedSubmissionValue"

	item := &ManagedProductDynamoEntry{
		Id:                 mpId,
		ManagedProductType: models.ModelType_Game,
		LiveSubmissionId:   &submissionValue,
	}

	err := dao.Put(item)
	assert.Nil(t, err)

	// Now we update the item
	item.LiveSubmissionId = &updatedSubmissionValue
	err = dao.Update(item)
	assert.Nil(t, err)

	// Now we get it back. It should have the updated value.
	product, err := dao.Get(mpId)
	assert.Nil(t, err)

	item.LastUpdated = product.LastUpdated
	assert.Equal(t, item, product)
	assert.Equal(t, updatedSubmissionValue, *item.LiveSubmissionId)
}
