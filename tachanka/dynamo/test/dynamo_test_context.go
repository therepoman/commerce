package dynamo_test

type (
	DynamoDao interface {
		DeleteTable() error
		CreateTable() error
	}

	DynamoTestContext struct {
		daos []DynamoDao
	}
)

func (context DynamoTestContext) RegisterDao(dao DynamoDao) error {
	err := dao.CreateTable()
	if err != nil {
		return err
	}

	context.daos = append(context.daos, dao)
	return nil
}

func (context DynamoTestContext) Cleanup() error {
	for _, dao := range context.daos {
		err := dao.DeleteTable()
		if err != nil {
			return err
		}
	}

	return nil
}
