package dynamo

import (
	"code.justin.tv/common/godynamo/client"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"

	"errors"
)

const (
	DevPortalOwnershipHashKeyColumn  string = "dev-portal-product-id"
	DevPortalOwnershipRangeKeyColumn string = "managed-product-id"
	DevPortalOwnershipReadCapacity   int64  = 100
	DevPortalOwnershipWriteCapacity  int64  = 100
)

type DevPortalOwnershipDao struct {
	table  client.DynamoTable
	client *client.DynamoClient
}

type IDevPortalOwnershipDao interface {
	Get(dppId string) ([]*DevPortalOwnershipEntry, error)
	Put(entry *DevPortalOwnershipEntry) error
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *client.DynamoClientConfig
}

func (dao *DevPortalOwnershipDao) DeleteTable() error {
	return dao.client.DeleteTable(dao.table)
}

func (dao *DevPortalOwnershipDao) SetupTable() error {
	return dao.client.SetupTable(dao.table)
}

func (dao *DevPortalOwnershipDao) GetClientConfig() *client.DynamoClientConfig {
	return dao.client.ClientConfig
}

func (dao *DevPortalOwnershipDao) Put(entry *DevPortalOwnershipEntry) error {
	return dao.client.PutItem(entry)
}

func (dao *DevPortalOwnershipDao) Get(DevPortalProductId string) ([]*DevPortalOwnershipEntry, error) {
	filter := client.QueryFilter{
		Names: map[string]*string{
			"#ID": aws.String(DevPortalOwnershipHashKeyColumn),
		},
		Values: map[string]*dynamodb.AttributeValue{
			":id": {S: aws.String(string(DevPortalProductId))},
		},
		KeyConditionExpression: aws.String("#ID = :id"),
	}

	result, err := dao.client.QueryTable(&DevPortalOwnershipTable{}, filter)
	if err != nil {
		return nil, err
	}

	// The query returns DynamoTableRecord types, which should be underlying Items.
	// Need to cast to check.
	entryResult := make([]*DevPortalOwnershipEntry, len(result))
	for i, record := range result {
		var isOwnershipEntry bool
		entryResult[i], isOwnershipEntry = record.(*DevPortalOwnershipEntry)
		if !isOwnershipEntry {
			return entryResult, errors.New("Encountered an unexpected type while converting dynamo result to ownership entry.")
		}
	}

	return entryResult, nil
}

func NewDevPortalOwnershipDao(client *client.DynamoClient) IDevPortalOwnershipDao {
	dao := &DevPortalOwnershipDao{}
	dao.client = client
	dao.table = &DevPortalOwnershipTable{}
	return dao
}

//DevPortalOwnershipTable maintains the relationship betweend dev portal ids and
//managed product ids.
type DevPortalOwnershipTable struct{}

func (t *DevPortalOwnershipTable) GetTableName() string {
	return "dev-portal-ownership"
}

func (t *DevPortalOwnershipTable) NewCreateTableInput() *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(DevPortalOwnershipReadCapacity),
			WriteCapacityUnits: aws.Int64(DevPortalOwnershipWriteCapacity),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String(DevPortalOwnershipHashKeyColumn),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String(DevPortalOwnershipRangeKeyColumn),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String(DevPortalOwnershipHashKeyColumn),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String(DevPortalOwnershipRangeKeyColumn),
				KeyType:       aws.String("RANGE"),
			},
		},
	}
}

func (t *DevPortalOwnershipTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (client.DynamoTableRecord, error) {
	dppId := client.StringFromAttributes(attributeMap, DevPortalOwnershipHashKeyColumn)
	managedProductId := client.StringFromAttributes(attributeMap, DevPortalOwnershipRangeKeyColumn)

	return &DevPortalOwnershipEntry{
		DevPortalProductId: dppId,
		ManagedProductId:   managedProductId,
	}, nil
}

type DevPortalOwnershipEntry struct {
	client.BaseDynamoTableRecord
	DevPortalProductId string
	ManagedProductId   string
}

func (d *DevPortalOwnershipEntry) GetTable() client.DynamoTable {
	return &DevPortalOwnershipTable{}
}

func (d *DevPortalOwnershipEntry) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)

	client.PopulateAttributesString(itemMap, DevPortalOwnershipHashKeyColumn, d.DevPortalProductId)
	client.PopulateAttributesString(itemMap, DevPortalOwnershipRangeKeyColumn, d.ManagedProductId)
	return itemMap
}

func (d *DevPortalOwnershipEntry) NewItemKey() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	client.PopulateAttributesString(itemMap, DevPortalOwnershipHashKeyColumn, d.DevPortalProductId)
	client.PopulateAttributesString(itemMap, DevPortalOwnershipRangeKeyColumn, d.ManagedProductId)
	return itemMap
}

func (d *DevPortalOwnershipEntry) Equals(other client.DynamoTableRecord) bool {
	otherDevPortalOwnershipEntry, isDevPortalOwnershipEntry := other.(*DevPortalOwnershipEntry)
	if !isDevPortalOwnershipEntry {
		return false
	} else {
		return d.DevPortalProductId == otherDevPortalOwnershipEntry.DevPortalProductId &&
			d.ManagedProductId == otherDevPortalOwnershipEntry.ManagedProductId
	}
}
