package dynamo

import (
	"time"

	"code.justin.tv/commerce/tachanka/config"
	test "code.justin.tv/commerce/tachanka/dynamo/test"
	"code.justin.tv/commerce/tachanka/models"
	"github.com/Sirupsen/logrus"
	db "github.com/guregu/dynamo"
)

const (
	SubmissionTableName              string = "submissions"
	SubmissionHashKeyColumn          string = "id"
	SubmissionManagedProductIdColumn string = "managed_product_id"
	SubmissionManagedProductIdIndex  string = "managed_product_id-index"
	SubmissionDocumentIdColumn       string = "document"
	SubmissionStatusColumn           string = "status"
	SubmissionSubmissionStepsColumn  string = "submission_steps"
	SubmissionLastUpdatedColumn      string = "last_updated"
	SubmissionCreatedColumn          string = "created_on"
	SubmissionReadCapacity           int64  = 100
	SubmissionWriteCapacity          int64  = 100
)

type (
	DynamoSubmissionDao struct {
		table  db.Table
		client *db.DB
	}

	SubmissionDao interface {
		Get(submissionId string) (*models.Submission, error)
		GetByManagedProduct(managedProductId string) ([]models.Submission, error)
		Put(submission *models.Submission) error
		Update(submission *models.Submission) (*models.Submission, error)
		AddStep(submissionId string, step *models.SubmissionStep) (*models.Submission, error)
		UpdateStatus(submissionId string, status models.SubmissionStatus) (*models.Submission, error)
		Delete(submissionId string) error
		CreateTable() error
		DeleteTable() error
	}
)

func (dao *DynamoSubmissionDao) Get(submissionId string) (*models.Submission, error) {
	var result models.Submission

	err := dao.table.Get(SubmissionHashKeyColumn, submissionId).
		One(&result)
	if err != nil && IsNoItemFoundError(err) {
		return nil, nil
	}

	return &result, err
}

func (dao *DynamoSubmissionDao) GetByManagedProduct(managedProductId string) ([]models.Submission, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":          "DynamoSubmissionDao.GetByManagedProduct",
		"ManagedProduct Id": managedProductId,
	})

	var result []models.Submission
	err := dao.table.Get(SubmissionManagedProductIdColumn, managedProductId).
		Index(SubmissionManagedProductIdIndex).
		All(&result)
	if err != nil {
		if IsNoItemFoundError(err) {
			return result, nil
		}

		log.Warn("Error getting submissions by managed product id: ", err)
		return nil, err
	}

	return result, nil
}

func (dao *DynamoSubmissionDao) Put(submission *models.Submission) error {
	submission.CreatedDate = time.Now()
	submission.LastUpdated = submission.CreatedDate

	return dao.table.Put(submission).Run()
}

func (dao *DynamoSubmissionDao) Update(submission *models.Submission) (*models.Submission, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":         "DynamoSubmissionDao.Update",
		"Submission Id":    submission.Id,
		"SubmissionStatus": submission.Status,
		"SubmissionSteps":  submission.SubmissionSteps,
	})

	var result models.Submission

	err := dao.table.Update(SubmissionHashKeyColumn, submission.Id).
		Set(SubmissionStatusColumn, submission.Status).
		Set(SubmissionSubmissionStepsColumn, submission.SubmissionSteps).
		Set(SubmissionLastUpdatedColumn, time.Now()).
		Value(&result)

	if err != nil {
		log.Warn("Error updating Submission: ", err)
		return nil, err
	}

	return &result, nil
}

// Adds a SubmissionStep to a Submission. This method should be preferred over calls to Update(..)
func (dao *DynamoSubmissionDao) AddStep(submissionId string, step *models.SubmissionStep) (*models.Submission, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":       "DynamoSubmissionDao.AddStep",
		"Submission Id":  submissionId,
		"SubmissionStep": step,
	})

	var result models.Submission

	err := dao.table.Update(SubmissionHashKeyColumn, submissionId).
		Append(SubmissionSubmissionStepsColumn, []models.SubmissionStep{*step}).
		Set(SubmissionLastUpdatedColumn, time.Now()).
		Value(&result)

	if err != nil {
		log.Warn("Error adding submission step: ", err)
		return nil, err
	}

	return &result, nil
}

// Updates the SubmissionStatus
func (dao *DynamoSubmissionDao) UpdateStatus(submissionId string, status models.SubmissionStatus) (*models.Submission, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":         "DynamoSubmissionDao.UpdateStatus",
		"Submission Id":    submissionId,
		"SubmissionStatus": status,
	})

	var result models.Submission

	// Only update the status if it hasn't completed or failed
	err := dao.table.Update(SubmissionHashKeyColumn, submissionId).
		Set(SubmissionStatusColumn, status).
		Set(SubmissionLastUpdatedColumn, time.Now()).
		If("$ IN (?)", SubmissionStatusColumn, models.SubmissionStatus_InProgress).
		Value(&result)

	if err != nil {
		log.Warn("Error updating submission status: ", err)
		return nil, err
	}

	return &result, nil
}

func (dao *DynamoSubmissionDao) Delete(submissionId string) error {
	return dao.table.Delete(SubmissionHashKeyColumn, submissionId).
		Run()
}

func NewSubmissionDao(client *db.DB, serviceConfig *config.Configuration) SubmissionDao {
	dao := &DynamoSubmissionDao{}
	dao.client = client
	dao.table = client.Table(FormatTableName(serviceConfig.DynamoTablePrefix, SubmissionTableName))
	return dao
}

// Creates a table for Submissions. FOR TESTING ONLY
func (dao *DynamoSubmissionDao) CreateTable() error {
	if test.DoesTableExist(dao.table.Name(), dao.client) {
		return nil
	}

	err := dao.client.CreateTable(dao.table.Name(), models.Submission{}).
		Provision(SubmissionReadCapacity, SubmissionWriteCapacity).
		Run()
	if err != nil {
		logrus.Error("Table creation error: ", err)
	}
	return err
}

// Deletes the Submission table. FOR TESTING ONLY
func (dao *DynamoSubmissionDao) DeleteTable() error {
	return dao.table.DeleteTable().Run()
}
