package api

import (
	pb "code.justin.tv/commerce/tachanka-client"
	"code.justin.tv/common/twirp"
	"github.com/Sirupsen/logrus"
	"golang.org/x/net/context"
)

func (this *FuelManagedProductAPI) BackfillManagedProduct(ctx context.Context, bmpr *pb.BackfillManagedProductRequest) (*pb.ManagedProduct, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":                      "FuelManagedProductAPI.BackfillManagedProduct",
		"BackfillManagedProductRequest": *bmpr,
	})
	log.Debug("Entering")

	twerr := this.validateAuthForDPPId(ctx, bmpr.DevPortalProduct.DppId)
	if twerr != nil {
		log.Warn("Invalid auth for request", twerr)
		return nil, twerr
	}

	dpp, twerr := this.fetchDevPortalProduct(bmpr.DevPortalProduct.DppId)
	if twerr != nil {
		log.Warn("Error fetching Dev Portal Product", twerr)
		return nil, twerr
	}

	mpType, err := this.tts.ConvertManagedProductTypeFromTwirp(bmpr.Type)
	if err != nil {
		log.Warn("Error converting PB Enum to Dynamo Enum: ", err)
		return nil, twirp.InvalidArgumentError("ManagedProduct.Type", "Invalid type provided.")
	}

	mp, err := this.pbs.BackfillManagedProduct(dpp, mpType, bmpr.AdgVendorId, bmpr.AdgDomainId, bmpr.AdgProductId, bmpr.Tuid, bmpr.AdgDocument)
	if err != nil {
		log.Warn("Error Backfilling Managed Product: ", err)
		return nil, twirp.InternalError("Unable to retrieve data from Dynamo")
	}

	if mp == nil {
		log.Warn("No Managed Product found")
		return nil, twirp.NotFoundError("No Managed Product found even after being created")
	}

	response, err := this.tts.TranslateManagedProduct(dpp, mp)
	if err != nil {
		log.Warn("Error translating Managed Product", err)
		return nil, twirp.InternalError("Error translating Managed Product.")
	}

	log.WithField("Response", response).Debug("Exiting")
	return response, nil
}
