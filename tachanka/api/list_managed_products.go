package api

import (
	"strings"

	pb "code.justin.tv/commerce/tachanka-client"
	"code.justin.tv/commerce/tachanka/models"
	"code.justin.tv/common/twirp"
	"github.com/Sirupsen/logrus"
	"golang.org/x/net/context"
)

func (this *FuelManagedProductAPI) ListManagedProducts(ctx context.Context, lmpr *pb.ListManagedProductsRequest) (*pb.ListManagedProductsResponse, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":                   "ListManagedProducts",
		"ListManagedProductsRequest": *lmpr,
	})
	log.Debug("Entering")
	twerr := this.validateAuthForDPPId(ctx, lmpr.DevPortalProduct.DppId)
	if twerr != nil {
		log.Warn("Error validating authorization token for Dev Portal Product: ", twerr)
		return nil, twerr
	}

	dpp, twerr := this.fetchDevPortalProduct(lmpr.DevPortalProduct.DppId)
	if twerr != nil {
		log.Warn("Error Validating Dev Portal Product ID: ", twerr)
		return nil, twerr
	}
	managedProducts, err := this.mps.GetManagedProducts(dpp)
	if err != nil {
		log.Warn("Error getting a list of managed products: ", err)
		if strings.Contains(err.Error(), "ResourceNotFoundException") {
			return nil, twirp.NotFoundError("Specified products not found")
		}
		return nil, twirp.InternalError("Unable to retrieve data from Dynamo")
	}

	// Check the filter to see if it's valid or not. This is optional, so an error here is indicative that
	// they didn't pass a filter, rather than an actual error.
	desiredType, err := this.tts.ConvertManagedProductTypeFromTwirp(lmpr.Filter)
	if err == nil { // Client has provided a valid filter
		var filteredManagedProducts []models.ManagedProduct
		for _, mp := range managedProducts {
			if mp.GetType() == desiredType {
				filteredManagedProducts = append(filteredManagedProducts, mp)
			}
		}
		managedProducts = filteredManagedProducts
	}

	var pbManagedProducts []*pb.ManagedProduct
	for _, dmp := range managedProducts {
		pbmp, err := this.tts.TranslateManagedProduct(dpp, dmp)
		if err != nil {
			log.Warn("Error Translating Managed Product: ", err)
			return nil, twirp.InternalError("Error translating Managed Product.")
		}
		pbManagedProducts = append(pbManagedProducts, pbmp)
	}
	response := &pb.ListManagedProductsResponse{
		ManagedProducts: pbManagedProducts,
	}
	log.WithField("Response", response).Debug("Exiting")
	return response, nil
}
