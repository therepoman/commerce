package api

import (
	pb "code.justin.tv/commerce/tachanka-client"
	"code.justin.tv/common/twirp"
	"github.com/Sirupsen/logrus"
	"golang.org/x/net/context"
)

func (this *FuelManagedProductAPI) UpdateManagedProduct(ctx context.Context, umpr *pb.UpdateManagedProductRequest) (*pb.ManagedProduct, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":                    "UpdateManagedProduct",
		"UpdateManagedProductRequest": *umpr,
	})
	log.Debug("Entering")
	dpp, twerr := this.validateAuthForMPId(ctx, umpr.ManagedProduct.Id)
	if twerr != nil {
		log.Warn("Error validating authorization token for Managed Product: ", twerr)
		return nil, twerr
	}

	desiredMP, err := this.mps.GetManagedProduct(umpr.ManagedProduct.Id)
	if err != nil {
		log.Warn("Error Fetching Managed Product: ", err)
		return nil, twirp.InternalError("Unable to retrieve data from Dynamo")
	}

	if umpr.ManagedProduct.Document.DocumentData == "" {
		log.Warn("Error updating product with null document data")
		return nil, twirp.InvalidArgumentError("ManagedProduct.Document.DocumentData", "Nil value")
	}
	err = desiredMP.SetData(umpr.ManagedProduct.Document.DocumentData)
	if err != nil {
		log.Warn("Error setting data for managed product: ", err)
		return nil, twirp.InternalError("Error unmarshalling json in Document.")
	}

	err = this.mps.UpdateManagedProduct(desiredMP)
	if err != nil {
		log.Warn("Error saving managed product: ", err)
		return nil, twirp.InternalError("Error Updating Managed Product")
	}

	translatedMp, err := this.tts.TranslateManagedProduct(dpp, desiredMP)
	if err != nil {
		log.Warn("Error Translating Updated Document: ", err)
		return nil, twirp.InternalError("Error translating Managed Product.")
	}
	log.WithField("Response", translatedMp).Debug("Exiting")
	return translatedMp, nil
}
