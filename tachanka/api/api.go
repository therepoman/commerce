package api

import (
	"net/http"

	"code.justin.tv/commerce/AmazonMWSGoClient/mws"
	pb "code.justin.tv/commerce/tachanka-client"
	"code.justin.tv/commerce/tachanka/authorization"
	adgClient "code.justin.tv/commerce/tachanka/client/adg"
	"code.justin.tv/commerce/tachanka/dynamo"
	"code.justin.tv/commerce/tachanka/mwsstatter"
	"code.justin.tv/commerce/tachanka/service"
	"code.justin.tv/common/goauthorization"
	"code.justin.tv/common/twitchhttp"
	"code.justin.tv/gds/gds/portal/dpp/dpptwirp"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"goji.io"
	"goji.io/pat"
	"golang.org/x/net/context"
)

type ServerAPI struct {
	IsProd                     bool
	Hostname                   string
	MWSClient                  mws.IAmazonMWSGoClient
	AWSCredentials             *credentials.Credentials
	DevPortalOwnershipDao      dynamo.IDevPortalOwnershipDao
	DocumentDao                dynamo.IDocumentDao
	ExternalMappingDao         dynamo.ExternalMappingDao
	ManagedProductDao          dynamo.IManagedProductDao
	SubmissionDao              dynamo.SubmissionDao
	ADGSubmissionServiceClient adgClient.ADGSubmissionServiceClient
	Decoder                    goauthorization.IDecoder

	// Authorization Daos
	AdminDao            authorization.IAdminDao
	DevPortalProductDao authorization.IDevPortalProductDao
}

type Server struct {
	*goji.Mux
}

func (serverAPI *ServerAPI) NewServer() (*Server, error) {
	server := twitchhttp.NewServer()

	s := &Server{
		server,
	}

	ds := service.NewDocumentService(serverAPI.DocumentDao, serverAPI.ManagedProductDao, serverAPI.SubmissionDao)
	tt := service.NewTwirpTranslator()
	em := service.NewExternalMappingService(serverAPI.ExternalMappingDao)
	dpps := service.NewDevPortalProductService(serverAPI.DevPortalOwnershipDao, serverAPI.ExternalMappingDao)
	mps := service.NewManagedProductsService(ds, serverAPI.ManagedProductDao, serverAPI.DocumentDao, dpps, serverAPI.ExternalMappingDao)
	dbs := service.NewADGDocumentBuilder(mps)
	sb := service.NewADGSubmissionBuilder(mps)
	vs := service.NewValidationService(dbs)
	ss := service.NewSubmissionService(mps, serverAPI.DocumentDao, serverAPI.SubmissionDao, serverAPI.ADGSubmissionServiceClient, sb)
	pbs := service.NewProductBackfillService(em, mps, dpps, ds, ss)

	pfe := NewFuelManagedProductAPIServer(dpps, mps, ss, tt, em, pbs, vs, serverAPI.Decoder)

	s.HandleFuncC(pat.Get("/"), s.EverythingIsFine)
	s.HandleFuncC(pat.Get("/health"), s.EverythingIsFine)
	s.HandleFuncC(pat.Get("/debug/running"), s.EverythingIsFine)

	httpHandler := pb.NewTachankaClientServer(pfe, nil, contextSource)
	fuelAPIMux := goji.NewMux()
	fuelAPIMux.UseC(mwsstatter.NewMWSMetricsMiddleware(serverAPI.MWSClient, serverAPI.IsProd, serverAPI.Hostname, serverAPI.AWSCredentials))
	fuelAPIMux.Handle(pat.Post(pb.TachankaClientPathPrefix+"*"), httpHandler)
	s.Handle(pat.Post(pb.TachankaClientPathPrefix+"*"), fuelAPIMux)

	// Endpoint for Authorization using the DPP Repo
	authService := authorization.NewAuthorizationService(serverAPI.AdminDao, serverAPI.DevPortalProductDao)
	authorizationHandler := dpptwirp.NewDppServer(authService, nil, nil)
	s.Handle(pat.Post(dpptwirp.DppPathPrefix+"*"), authorizationHandler)

	return s, nil
}

func (s *Server) EverythingIsFine(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	s.safeWrite(w, "OK")
	return
}

func contextSource(_ http.ResponseWriter, r *http.Request) context.Context {
	ctx := r.Context()
	ctx = context.WithValue(ctx, authKey, r.Header.Get("Twitch-Authorization"))
	return ctx
}

func (s *Server) safeWrite(w http.ResponseWriter, str string) {
	_, err := w.Write([]byte(str))
	if err != nil {
		panic(err)
	}
}
