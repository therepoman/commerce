package api

import (
	pb "code.justin.tv/commerce/tachanka-client"
	"code.justin.tv/common/twirp"
	"github.com/Sirupsen/logrus"
	"golang.org/x/net/context"
)

func (this *FuelManagedProductAPI) ValidateManagedProduct(ctx context.Context, validateRequest *pb.ValidateManagedProductRequest) (*pb.ValidationResult, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":                      "ValidateManagedProduct",
		"ValidateManagedProductRequest": *validateRequest,
	})
	log.Debug("Entering")

	// Todo: Implement this once we actually need it
	return nil, twirp.InternalError("Method not implemented")

	// Preconditions
	//if validateRequest.Document == nil {
	//	log.Error("Request didn't include a document to validate")
	//	return nil, twirp.InvalidArgumentError("ValidateDocumentRequest.Document", "Nil value")
	//}
	//
	//if validateRequest.Document.DocumentData == "" {
	//	log.Error("Unable to validate a document with an empty or nil DocumentData")
	//	return nil, twirp.InvalidArgumentError("ValidateDocumentRequest.Document.DocumentData", "Missing value")
	//}
	//
	//if validateRequest.ManagedProductType == pb.ManagedProductType_UNSPECIFIED {
	//	log.Error("Unable to validate a document with an unspecified ManagedProductType")
	//	return nil, twirp.InvalidArgumentError("ValidateDocumentRequest.ManagedProductType", "Unspecified value")
	//}
	//
	//// Convert API to domain objects
	//dynamoDocument, twerr := this.tts.ConvertAPIDocumentToDynamoDocument(validateRequest.Document)
	//if twerr != nil {
	//	log.Error("Error converting API Document to Dynamo Document", twerr)
	//	return nil, twerr
	//}
	//
	//dynamoManagedProductType, twerr := this.tts.ConvertPBEnumToDynamoEnum(validateRequest.ManagedProductType)
	//if twerr != nil {
	//	log.Error("Error converting API ManagedProductType to Dynamo ManagedProductType", twerr)
	//	return nil, twerr
	//}
	//
	//// Perform validation
	//validationErrors, twerr := this.vs.ValidateManagedProduct(dynamoDocument, dynamoManagedProductType)
	//if twerr != nil {
	//	log.Error("Error validating document", twerr)
	//	return nil, twerr
	//}
	//
	//// Build response
	//responseValidationErrors := make([]*pb.ValidationError, len(validationErrors))
	//for i, validationError := range validationErrors {
	//	responseValidationErrors[i] = this.tts.ConvertServiceValidationErrorToAPIValidationError(validationError)
	//}
	//
	//validationResult := &pb.ValidationResult{
	//	ValidationErrors: responseValidationErrors,
	//}
	//log.WithField("Response", validationResult).Debug("Exiting")
	//
	//return validationResult, nil
}
