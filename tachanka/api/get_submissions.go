package api

import (
	pb "code.justin.tv/commerce/tachanka-client"
	"code.justin.tv/common/twirp"
	"github.com/Sirupsen/logrus"
	"golang.org/x/net/context"
)

func (this *FuelManagedProductAPI) GetSubmissions(ctx context.Context, request *pb.GetSubmissionsRequest) (*pb.Submissions, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":              "GetSubmissions",
		"GetSubmissionsRequest": *request,
	})
	log.Debug("Entering")

	// Preconditions
	managedProductId := request.ManagedProductId
	if len(managedProductId) == 0 {
		log.Error("ManagedProductId had a length of zero")
		return nil, twirp.InvalidArgumentError("ManagedProductId", "Must provide a valid managed product id")
	}

	_, twerr := this.validateAuthForMPId(ctx, request.ManagedProductId)
	if twerr != nil {
		log.Warn("Error validating authorization token for Managed Product: ", twerr)
		return nil, twerr
	}

	// Get the submissions
	submissions, err := this.subs.GetSubmissionsByManagedProduct(managedProductId)
	if err != nil {
		log.Error("Error retrieving submissions", err)
		return nil, twirp.InternalError("Error retrieving submissions")
	}

	// Create response
	result := &pb.Submissions{
		Submissions: make([]*pb.Submission, len(submissions)),
	}

	for i, value := range submissions {
		result.Submissions[i], err = this.tts.ConvertSubmissionToTwirp(&value)
		if err != nil {
			log.Error("Error converting submissions", err)
			return nil, twirp.InternalError("Error converting submissions")
		}
	}

	return result, nil
}
