package api

import (
	pb "code.justin.tv/commerce/tachanka-client"
	"code.justin.tv/common/twirp"
	"github.com/Sirupsen/logrus"
	"github.com/satori/go.uuid"
	"golang.org/x/net/context"
)

func (this *FuelManagedProductAPI) CreateManagedProduct(ctx context.Context, cmpr *pb.CreateManagedProductRequest) (*pb.ManagedProduct, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":                    "FuelManagedProductAPI.CreateManagedProduct",
		"CreateManagedProductRequest": *cmpr,
	})
	log.Debug("Entering")

	// Validate
	twerr := validateRequiredFields(cmpr)
	if twerr != nil {
		return nil, twerr
	}

	mpType, err := this.tts.ConvertManagedProductTypeFromTwirp(cmpr.ManagedProduct.Type)
	if err != nil {
		log.Warn("Error converting managed product type: ", err)
		return nil, twirp.InvalidArgumentError("ManagedProduct.Type", "Invalid type provided.")
	}

	// Get the Dev Portal Product
	devPortalProduct, twerr := this.fetchDevPortalProduct(cmpr.DevPortalProduct.DppId)
	if twerr != nil {
		log.Warn("Error Fetching Dev Portal Product: ", twerr)
		return nil, twerr
	}

	// Authentication
	twerr = this.validateAuthForDPPId(ctx, devPortalProduct.Id)
	if twerr != nil {
		log.Warn("Invalid auth for request", twerr)
		return nil, twerr
	}

	// Update the Vendor Id - REMOVE ONCE DPPs ARE FIRST CLASS
	if devPortalProduct.ADGVendorId == nil {
		// Get the Vendor Id
		vendorId, twerr := this.tts.ExtractVendorId(cmpr.ManagedProduct)
		if twerr != nil {
			log.Error("Unable to retrieve vendor Id: ", twerr)
			return nil, twerr
		}

		// Update the product
		devPortalProduct.ADGVendorId = &vendorId

		err = this.dpps.UpdateDevPortalProduct(devPortalProduct)
		if err != nil {
			log.Warn("Error updating Dev Portal Product: ", err)
			return nil, twirp.InternalError("Error updating Dev Portal Product.")
		}
	}

	// Create the ManagedProduct
	mpid := uuid.NewV4().String()
	nMP, err := this.mps.CreateManagedProduct(devPortalProduct, mpid, mpType)
	if err != nil {
		log.Warn("Error creating Managed Product: ", err)
		return nil, twirp.InternalError("Error creating Managed Product.")
	}

	// Construct the response
	response, err := this.tts.TranslateManagedProduct(devPortalProduct, nMP)
	if err != nil {
		log.Warn("Error translating Managed Product", err)
		return nil, twirp.InternalError("Error translating Managed Product.")
	}

	log.WithField("Response", response).Debug("Exiting")
	return response, nil
}

// Validate that all required fields are present
func validateRequiredFields(request *pb.CreateManagedProductRequest) error {
	log := logrus.WithFields(logrus.Fields{
		"Function":                    "validateRequiredFields",
		"CreateManagedProductRequest": *request,
	})
	log.Debug("Entering")

	if request.DevPortalProduct == nil {
		log.Warn("Dev Portal Product is nil")
		return twirp.InvalidArgumentError("DevPortalProduct", "Dev Portal Product is nil.")
	}

	// This is only necessary since we are bundling the Vendor ID with the ManagedProduct
	// DocumentData.
	// TODO - Don't bundle Vendor ID with ManagedProduct
	if request.ManagedProduct == nil {
		log.Warn("ManagedProduct is nil")
		return twirp.InvalidArgumentError("ManagedProduct", "ManagedProduct is nil.")
	}

	if request.ManagedProduct.Document == nil {
		log.Warn("ManagedProduct Document is nil")
		return twirp.InvalidArgumentError("ManagedProduct.Document", "Document is nil.")
	}

	if request.ManagedProduct.Document.DocumentData == "" {
		log.Warn("ManagedProduct DocumentData is nil")
		return twirp.InvalidArgumentError("ManagedProduct.Document.DocumentData", "DocumentData is nil.")
	}

	return nil
}
