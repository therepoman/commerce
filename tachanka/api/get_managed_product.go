package api

import (
	pb "code.justin.tv/commerce/tachanka-client"
	"code.justin.tv/common/twirp"
	"github.com/Sirupsen/logrus"
	"golang.org/x/net/context"
)

func (this *FuelManagedProductAPI) GetManagedProduct(ctx context.Context, mpr *pb.GetManagedProductRequest) (*pb.ManagedProduct, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":                 "GetManagedProduct",
		"GetManagedProductRequest": *mpr,
	})
	log.Debug("Entering")
	dpp, twerr := this.validateAuthForMPId(ctx, mpr.ManagedProductId)
	if twerr != nil {
		log.Warn("Error validating authorization token for Managed Product: ", twerr)
		return nil, twerr
	}
	desiredMP, err := this.mps.GetManagedProduct(mpr.ManagedProductId)
	if err != nil {
		log.Warn("Error Fetching Managed Product: ", err)
		return nil, twirp.InternalError("Unable to retrieve data from Dynamo")
	}
	if desiredMP == nil {
		log.Warn("No Managed Product found")
		return nil, twirp.NotFoundError("No Managed Product found for the given Id")
	}
	response, err := this.tts.TranslateManagedProduct(dpp, desiredMP)
	if err != nil {
		log.Warn("Error translating Managed Product", err)
		return nil, twirp.InternalError("Error translating Managed Product.")
	}
	log.WithField("Response", response).Debug("Exiting")
	return response, nil
}
