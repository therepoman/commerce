package api

import (
	"fmt"

	pb "code.justin.tv/commerce/tachanka-client"
	"code.justin.tv/commerce/tachanka/models"
	"code.justin.tv/common/twirp"
	"github.com/Sirupsen/logrus"
	"github.com/golang/protobuf/ptypes/empty"
	"golang.org/x/net/context"
)

const (
	defaultExternalMappingType models.ExternalMappingTypeEnum = models.EM_ADGProductId
	defaultSubmissionStepType  models.SubmissionStepType      = models.SubmissionStepType_AdgSubmit
)

func (this *FuelManagedProductAPI) UpdateADGSubmissionStatus(ctx context.Context, updateSubRequest *pb.UpdateADGSubmissionStatusRequest) (*empty.Empty, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":                         "UpdateADGSubmissionStatus",
		"UpdateADGSubmissionStatusRequest": *updateSubRequest,
	})

	submissionStepStatus, err := models.StringToSubmissionStepStatus(updateSubRequest.Status)
	if err != nil {
		log.Warn("Error updating submission status: ", err)
		return nil, twirp.InvalidArgumentError(updateSubRequest.Status, fmt.Sprintf("Invalid Status type %s", updateSubRequest.Status))
	}

	// Scan external mapping table and get parent-id where value = productId
	mappings, err := this.ems.GetExternalMappingByValue(updateSubRequest.ProductId, defaultExternalMappingType)
	if err != nil {
		log.Warn("Error fetching Mapping for ProductId: ", err)
		return nil, twirp.InternalError("Unable to retrieve data from Dynamo")
	}
	if len(*mappings) <= 0 {
		log.Warn("No mapping found for ProductId: ", err)
		return nil, twirp.InternalError("Unable to retrieve data from Dynamo")
	}

	// Query managed products table and get working-submission-id where id = parent-id
	managedProductId := (*mappings)[0].ParentId
	managedProduct, err := this.mps.GetManagedProduct(managedProductId)
	if err != nil {
		log.Warn("Error Fetching Managed Product: ", err)
		return nil, twirp.InternalError("Unable to retrieve data from Dynamo")
	}
	// update submission status where id = working-submission-id
	_, err = this.subs.AddSubmissionStep(managedProduct.GetWorkingSubmissionId(), defaultSubmissionStepType, submissionStepStatus)
	if err != nil {
		log.Warn("Error updating submission status: ", err)
		return nil, twirp.InternalError("Unable to update status")
	}
	// @TODO if status is COMPLETED, poke Janus with the ASIN
	return &empty.Empty{}, nil
}
