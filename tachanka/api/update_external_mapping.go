package api

import (
	pb "code.justin.tv/commerce/tachanka-client"
	"code.justin.tv/commerce/tachanka/models"
	"code.justin.tv/common/twirp"
	"github.com/Sirupsen/logrus"
	"golang.org/x/net/context"
)

func (this *FuelManagedProductAPI) UpdateVendorMapping(ctx context.Context, mapping *pb.VendorMapping) (*pb.VendorMapping, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function": "UpdateVendorMapping",
		"Mapping":  *mapping,
	})
	log.Debug("Entering")
	twerr := this.validateAuthForDPPId(ctx, mapping.DevPortalProduct.DppId)
	if twerr != nil {
		log.Warn("Error validating authorization token for Dev Portal Product: ", twerr)
		return nil, twerr
	}

	fetchedMapping, err := this.ems.PutExternalMapping(mapping.DevPortalProduct.DppId, models.EM_ADGVendorId, mapping.VendorId)
	if err != nil {
		log.Warn("Error creating Vendor Mapping: ", err)
		return nil, twirp.InternalError(err.Error())
	}
	response := &pb.VendorMapping{
		DevPortalProduct: &pb.DevPortalProduct{
			DppId: fetchedMapping.ParentId,
		},
		VendorId: fetchedMapping.Value,
	}
	log.WithField("Response", *response).Debug("Exiting")
	return response, nil
}
