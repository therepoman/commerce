package api_test

import (
	"context"
	"errors"
	"fmt"
	"os"
	"testing"

	"io/ioutil"

	"encoding/json"
	"reflect"

	pb "code.justin.tv/commerce/tachanka-client"
	"code.justin.tv/commerce/tachanka/api"
	"code.justin.tv/commerce/tachanka/dynamo"
	dynamoTest "code.justin.tv/commerce/tachanka/dynamo/test"
	"code.justin.tv/commerce/tachanka/mocks"
	"code.justin.tv/commerce/tachanka/models"
	"code.justin.tv/commerce/tachanka/service"
	"code.justin.tv/common/goauthorization"
	"code.justin.tv/common/twirp"
	"github.com/satori/go.uuid"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	NewManagedProductDocumentData = "{\"adgVendorId\":\"TestVendorId\"}"
	TestADGProductId              = "testADGProductID"
)

func TestFuelManagedProductAPI(t *testing.T) {
	mockApi := newServerAPI_mockedAtDynamo()

	response := new(mocks.Response)
	response.On("GetADGProductId").Return(TestADGProductId, nil)

	// Nothing happening in the tpigs client yet
	adgSubmissionServiceClient := new(mocks.ADGSubmissionServiceClient)
	adgSubmissionServiceClient.On("CreateProduct", mock.Anything).Return(response, nil)
	adgSubmissionServiceClient.On("CreateRevision", mock.Anything).Return(response, nil)

	// Note: A single convey statement MUST be used or else this fails
	Convey("Given a new ManagedProductAPI", t, func() {

		ds := service.NewDocumentService(mockApi.DocumentDao, mockApi.ManagedProductDao, mockApi.SubmissionDao)
		tt := service.NewTwirpTranslator()
		em := service.NewExternalMappingService(mockApi.ExternalMappingDao)
		dpps := service.NewDevPortalProductService(mockApi.DevPortalOwnershipDao, mockApi.ExternalMappingDao)
		mps := service.NewManagedProductsService(ds, mockApi.ManagedProductDao, mockApi.DocumentDao, dpps, mockApi.ExternalMappingDao)
		dbs := service.NewADGDocumentBuilder(mps)
		sb := service.NewADGSubmissionBuilder(mps)
		vs := service.NewValidationService(dbs)
		ss := service.NewSubmissionService(mps, mockApi.DocumentDao, mockApi.SubmissionDao, adgSubmissionServiceClient, sb)
		pbs := service.NewProductBackfillService(em, mps, dpps, ds, ss)

		decoder := new(mocks.IDecoder)

		cut := api.NewFuelManagedProductAPIServer(dpps, mps, ss, tt, em, pbs, vs, decoder)

		Convey("Testing the API with a game", func() {
			fuelManagedProductAPIServerActions(cut, decoder, em, tt, dpps, pb.ManagedProductType_ADG_GAME, newValidGameString(), models.ModelType_Game)
		})

		Convey("Testing the API with an IGC", func() {
			fuelManagedProductAPIServerActions(cut, decoder, em, tt, dpps, pb.ManagedProductType_ADG_IGC, newValidIGCString(), models.ModelType_InGameContent)
		})

		Convey("Testing the API with an extension", func() {
			fuelManagedProductAPIServerActions(cut, decoder, em, tt, dpps, pb.ManagedProductType_ADG_EXTENSION, newValidExtensionsString(), models.ModelType_Extension)
		})

		Convey("Testing the API with an extension IAP", func() {
			fuelManagedProductAPIServerActions(cut, decoder, em, tt, dpps, pb.ManagedProductType_ADG_EXTENSION_IAP, newValidExtensionIAPString(), models.ModelType_ExtensionInAppPurchase)
		})
	})
}

func fuelManagedProductAPIServerActions(cut *api.FuelManagedProductAPI, decoder *mocks.IDecoder, em service.IExternalMappingService, tt service.ITwirpTranslator, dpps service.IDevPortalProductService, pbManagedProductType pb.ManagedProductType, documentString string, expectedManagedProductType models.ManagedProductType) {
	dppId := uuid.NewV4().String()
	Convey("And given proper authorization", func() {
		decoder.On("Decode", mock.Anything).Return(newValidAuthToken(dppId), nil)
		decoder.On("Validate", mock.Anything, mock.Anything).Return(nil)

		Convey("And given a seeded DPP", func() {
			_, err := em.PutExternalMapping(dppId, models.EM_ADGVendorId, "validvendor")
			So(err, ShouldBeNil)

			_, err = em.PutExternalMapping(dppId, models.EM_ADGDomainId, "validdomain")
			So(err, ShouldBeNil)

			Convey("When we create a new Managed Product", func() {
				mpRequest := newValidCreateManagedProductRequest(dppId, pbManagedProductType)

				newMP, err := cut.CreateManagedProduct(context.Background(), mpRequest)
				So(err, ShouldBeNil)
				Convey("We should get a valid Managed Product Back", func() {
					_, err := uuid.FromString(newMP.Id)
					So(err, ShouldBeNil)
				})

				Convey("When we update that Managed Product", func() {
					// Switch depending on the ManagedProductType
					newCrazyDevUUID := uuid.NewV4().String()
					switch expectedManagedProductType {
					case models.ModelType_Game:
						newData := "{\"developerName\":\"" + newCrazyDevUUID + "\"}"
						newMP.Document.DocumentData = newData

						umpr := newValidUpdateManagedProductRequest(newMP)
						updatedMP, err := cut.UpdateManagedProduct(context.Background(), umpr)
						So(err, ShouldBeNil)

						Convey("We should get the updated Managed Product Back", func() {
							So(updatedMP.Document.DocumentData, ShouldContainSubstring, "\"developerName\":\""+newCrazyDevUUID+"\"")
						})
					case models.ModelType_InGameContent:
						newData := "{\"categoryInfoId\": \"game-microtransactions-for-currency\"}"
						newMP.Document.DocumentData = newData

						umpr := newValidUpdateManagedProductRequest(newMP)
						updatedMP, err := cut.UpdateManagedProduct(context.Background(), umpr)
						So(err, ShouldBeNil)

						Convey("We should get yet another updated Managed Product Back", func() {
							So(updatedMP.Document.DocumentData, ShouldContainSubstring, "\"categoryInfoId\":\"game-microtransactions-for-currency\"")
						})
					case models.ModelType_Extension:
						newData := "{\"developerName\":\"" + newCrazyDevUUID + "\"}"
						newMP.Document.DocumentData = newData

						umpr := newValidUpdateManagedProductRequest(newMP)
						updatedMP, err := cut.UpdateManagedProduct(context.Background(), umpr)
						So(err, ShouldBeNil)

						Convey("We should get another updated Managed Product Back", func() {
							So(updatedMP.Document.DocumentData, ShouldContainSubstring, "\"developerName\":\""+newCrazyDevUUID+"\"")
						})
					case models.ModelType_ExtensionInAppPurchase:
						newData := "{\"itemType\":\"Other\"}"
						newMP.Document.DocumentData = newData

						umpr := newValidUpdateManagedProductRequest(newMP)
						updatedMP, err := cut.UpdateManagedProduct(context.Background(), umpr)
						So(err, ShouldBeNil)

						Convey("We should get yet another updated Managed Product Back", func() {
							So(updatedMP.Document.DocumentData, ShouldContainSubstring, "\"itemType\":\"Other\"")
						})
					default:
						// Intentionally break
						So(false, ShouldBeTrue)
					}
				})

				Convey("When we update the Managed Product with a valid Document", func() {
					newMP.Document = newValidDocument(newMP.Document.Id, documentString)
					umpr := newValidUpdateManagedProductRequest(newMP)
					updatedMP, err := cut.UpdateManagedProduct(context.Background(), umpr)
					So(err, ShouldBeNil)

					Convey("We should get an even newer updated Managed Product Back", func() {
						So(updatedMP.Document.DocumentData, ShouldNotBeBlank)
						// Ensure the updated MP represents the expected managed product type
						managedProductModel, err := tt.ConvertManagedProductTypeFromTwirp(updatedMP.GetType())
						So(err, ShouldBeNil)
						So(managedProductModel, ShouldEqual, expectedManagedProductType)
						So(updatedMP.Document.DocumentData, shouldEqualDocumentData, newMP.Document.DocumentData, expectedManagedProductType)
					})

					Convey("When we submit that Managed Product", func() {
						submissionRequest := &pb.SubmitManagedProductRequest{
							ManagedProductId: newMP.Id,
							Tuid:             "testerinotuid" + string(expectedManagedProductType),
						}
						createParentIfRequired(dppId, cut, pbManagedProductType)

						Convey("It should work", func() {
							submitManagedProductResponse, err := cut.SubmitManagedProductWithResponse(context.Background(), submissionRequest)
							So(err, ShouldBeNil)
							So(submitManagedProductResponse.ValidationErrors, ShouldBeEmpty)
							So(submitManagedProductResponse.ManagedProduct, ShouldNotBeNil)

							Convey("And when we query for the submission by managed product id", func() {
								getSubmissionsRequest := &pb.GetSubmissionsRequest{
									ManagedProductId: submitManagedProductResponse.ManagedProduct.Id,
								}

								Convey("It should work", func() {
									getSubmssionsResult, err := cut.GetSubmissions(context.Background(), getSubmissionsRequest)
									So(err, ShouldBeNil)
									So(getSubmssionsResult, ShouldNotBeNil)
									So(getSubmssionsResult.Submissions, ShouldNotBeEmpty)
									So(getSubmssionsResult.Submissions, ShouldHaveLength, 1)
								})

							})
						})

					})
				})
			})
			Convey("When we create a bunch of Managed Products", func() {
				rounds := 10
				var mpids []string
				for i := 0; i < rounds; i++ {
					thisRequest := newValidCreateManagedProductRequest(dppId, pbManagedProductType)
					nmp, err := cut.CreateManagedProduct(context.Background(), thisRequest)
					So(err, ShouldBeNil)
					mpids = append(mpids, nmp.Id)
				}

				Convey("We should get them all back when we ask for them", func() {
					lmpr := &pb.ListManagedProductsRequest{
						DevPortalProduct: &pb.DevPortalProduct{
							DppId: dppId,
						},
					}

					products, err := cut.ListManagedProducts(context.Background(), lmpr)
					So(err, ShouldBeNil)
					for _, prod := range products.ManagedProducts {
						So(listContains(mpids, prod.Id), ShouldBeTrue)
						So(prod.Document.DocumentData, ShouldNotBeEmpty)
					}
				})
			})
		})

		Convey("When we make a new Managed Product with ADG Info", func() {
			mpRequest := newValidCreateManagedProductRequest(dppId, pbManagedProductType)
			testVendorId := "TestVendorId"
			mpRequest.AdgVendorId = testVendorId

			Convey("We should get a valid Managed Product back, and the DPP should be updated", func() {
				newMP, err := cut.CreateManagedProduct(context.Background(), mpRequest)
				So(err, ShouldBeNil)
				So(newMP, ShouldNotBeNil)

				dpp, err := dpps.GetDevPortalProduct(dppId)
				So(err, ShouldBeNil)
				So(*dpp.ADGVendorId, ShouldEqual, testVendorId)
			})
		})
	})

	Convey("And given improper authorization", func() {
		decoder.On("Decode", mock.Anything).Return(newValidAuthToken(dppId), nil)
		decoder.On("Validate", mock.Anything, mock.Anything).Return(errors.New("Unauthorized"))

		Convey("When we create a new Managed Product with invalid authorization", func() {
			mpRequest := newValidCreateManagedProductRequest(dppId, pbManagedProductType)
			newMP, err := cut.CreateManagedProduct(context.Background(), mpRequest)

			Convey("It should fail", func() {
				So(newMP, ShouldBeNil)
				twerr, ok := err.(twirp.Error)
				So(ok, ShouldBeTrue)
				So(twerr.Code(), ShouldEqual, twirp.PermissionDenied)
			})
		})

		Convey("When we request any Managed Product", func() {
			fetchRequest := &pb.GetManagedProductRequest{
				ManagedProductId: uuid.NewV4().String(),
			}

			fetchedMP, err := cut.GetManagedProduct(context.Background(), fetchRequest)

			Convey("We should get a Twirp Unauthorized error", func() {
				So(fetchedMP, ShouldBeNil)
				twerr, ok := err.(twirp.Error)
				So(ok, ShouldBeTrue)
				So(twerr.Code(), ShouldEqual, twirp.PermissionDenied)
			})
		})

		Convey("When we attempt to list Managed Products", func() {
			listRequest := &pb.ListManagedProductsRequest{
				DevPortalProduct: &pb.DevPortalProduct{
					DppId: dppId,
				},
			}

			fetchedList, err := cut.ListManagedProducts(context.Background(), listRequest)

			Convey("We should get a Twirp Unauthorized error for the listing", func() {
				So(fetchedList, ShouldBeNil)
				twerr, ok := err.(twirp.Error)
				So(ok, ShouldBeTrue)
				So(twerr.Code(), ShouldEqual, twirp.PermissionDenied)
			})
		})

		Convey("When we attempt to Submit a Managed Product", func() {
			submitRequest := &pb.SubmitManagedProductRequest{
				ManagedProductId: uuid.NewV4().String(),
			}

			submittedMP, err := cut.SubmitManagedProductWithResponse(context.Background(), submitRequest)

			Convey("We should get a Twirp Unauthorized error for the submission", func() {
				So(submittedMP, ShouldBeNil)
				twerr, ok := err.(twirp.Error)
				So(ok, ShouldBeTrue)
				So(twerr.Code(), ShouldEqual, twirp.PermissionDenied)
			})
		})

		Convey("When we attempt to Update a Managed Product", func() {
			updateRequest := &pb.UpdateManagedProductRequest{
				ManagedProduct: &pb.ManagedProduct{
					Id: uuid.NewV4().String(),
				},
			}

			updatedMP, err := cut.UpdateManagedProduct(context.Background(), updateRequest)

			Convey("We should get a Twirp Unauthorized error for the update for"+string(expectedManagedProductType), func() {
				So(updatedMP, ShouldBeNil)
				twerr, ok := err.(twirp.Error)
				So(ok, ShouldBeTrue)
				So(twerr.Code(), ShouldEqual, twirp.PermissionDenied)
			})
		})
	})
}

func newServerAPI_mockedAtDynamo() *api.ServerAPI {
	testClient := dynamoTest.CreateTestClient()
	testConfiguration := dynamoTest.CreateTestConfiguration("test-fuel-managed-api")

	managedProductDao := dynamo.NewManagedProductDao(deprecatedDynamoContext.CreateTestClient(deprecatedDynamoContext.WaitTimeout, deprecatedDynamoContext.WaitTimeout, "test-fuel-managed-api-mp"))
	_ = deprecatedDynamoContext.RegisterTestingDao(managedProductDao)
	documentDao := dynamo.NewDocumentDao(deprecatedDynamoContext.CreateTestClient(deprecatedDynamoContext.WaitTimeout, deprecatedDynamoContext.WaitTimeout, "test-fuel-managed-api-doc"))
	_ = deprecatedDynamoContext.RegisterTestingDao(documentDao)
	externalMappingDao := dynamo.NewExternalMappingDao(testClient, testConfiguration)
	dynamoContext.RegisterDao(externalMappingDao)
	submissionDao := dynamo.NewSubmissionDao(testClient, testConfiguration)
	dynamoContext.RegisterDao(submissionDao)
	devPortalOwnershipDao := dynamo.NewDevPortalOwnershipDao(deprecatedDynamoContext.CreateTestClient(deprecatedDynamoContext.WaitTimeout, deprecatedDynamoContext.WaitTimeout, "test-fuel-managed-api-dpo"))
	_ = deprecatedDynamoContext.RegisterTestingDao(devPortalOwnershipDao)

	return &api.ServerAPI{
		ManagedProductDao:     managedProductDao,
		DocumentDao:           documentDao,
		ExternalMappingDao:    externalMappingDao,
		SubmissionDao:         submissionDao,
		DevPortalOwnershipDao: devPortalOwnershipDao,
	}
}

func newValidUpdateManagedProductRequest(product *pb.ManagedProduct) *pb.UpdateManagedProductRequest {
	return &pb.UpdateManagedProductRequest{
		ManagedProduct: product,
	}
}

func newValidCreateManagedProductRequest(dppId string, managedProductType pb.ManagedProductType) *pb.CreateManagedProductRequest {
	mpRequest := &pb.CreateManagedProductRequest{
		DevPortalProduct: &pb.DevPortalProduct{
			DppId: dppId,
		},
		ManagedProduct: &pb.ManagedProduct{
			Type: managedProductType,
			Document: &pb.Document{
				DocumentData: NewManagedProductDocumentData,
			},
		},
	}

	return mpRequest
}

func newValidDocument(docId string, documentData string) *pb.Document {
	return &pb.Document{
		Id:              docId,
		DocumentVersion: string(dynamo.DocumentVersion2_0),
		DocumentData:    documentData,
	}
}

var validGameDocumentFile string = os.Getenv("GOPATH") + "/src/code.justin.tv/commerce/tachanka/config/data/front_end_requests/valid_game_data_1.json"
var validIGCDocumentFile string = os.Getenv("GOPATH") + "/src/code.justin.tv/commerce/tachanka/config/data/front_end_requests/valid_igc_data_1.json"
var validExtensionsDocumentFile string = os.Getenv("GOPATH") + "/src/code.justin.tv/commerce/tachanka/config/data/front_end_requests/valid_extensions_data.json"
var validExtensionIAPDocumentFile string = os.Getenv("GOPATH") + "/src/code.justin.tv/commerce/tachanka/config/data/front_end_requests/valid_extension_iap_data.json"

func newValidDocumentString(filePath string) string {
	validJsonFile, err := ioutil.ReadFile(filePath)
	if err != nil {
		panic(err)
	}
	return string(validJsonFile)
}

func newValidExtensionIAPString() string {
	return newValidDocumentString(validExtensionIAPDocumentFile)
}

func newValidExtensionsString() string {
	return newValidDocumentString(validExtensionsDocumentFile)
}

func newValidGameString() string {
	return newValidDocumentString(validGameDocumentFile)
}

func newValidIGCString() string {
	return newValidDocumentString(validIGCDocumentFile)
}

func newValidAuthToken(dppid string) *goauthorization.AuthorizationToken {
	return &goauthorization.AuthorizationToken{
		Claims: goauthorization.TokenClaims{
			Authorizations: goauthorization.CapabilityClaims{
				"edit_product": {
					"product_id": dppid,
				},
			},
		},
	}
}

func newInvalidAuthToken() *goauthorization.AuthorizationToken {
	return &goauthorization.AuthorizationToken{
		Claims: goauthorization.TokenClaims{
			Authorizations: goauthorization.CapabilityClaims{
				"edit_product": {
					"product_id": ":gaffo-d:",
				},
			},
		},
	}
}

func listContains(list []string, value string) bool {
	for _, item := range list {
		if item == value {
			return true
		}
	}
	return false
}

func shouldEqualDocumentData(actual interface{}, expected ...interface{}) string {
	// Verify the inputs. Convert to strings since documents are just unmodeled strings
	actualString, ok := actual.(string)
	if !ok {
		return "The actual document could not be converted properly"
	}

	expectedString, ok := expected[0].(string)
	if !ok {
		return "The expected document could not be converted properly"
	}

	managedProductType, ok := expected[1].(models.ManagedProductType)
	if !ok {
		return "The expected managed product type could not be converted properly"
	}

	var updatedMetadataManager models.MetadataManager
	var newMetadataManager models.MetadataManager
	// Switch based on the ManagedProductType
	switch managedProductType {
	case models.ModelType_Game:
		// Unmarshal the updated (actual) Game
		var updatedGame models.Game
		err := json.Unmarshal([]byte(actualString), &updatedGame)
		if err != nil {
			return "Unmarshal updated game failed: " + err.Error()
		}
		updatedMetadataManager = &updatedGame

		// Unmarshal the expected Game
		var newGame models.Game
		err = json.Unmarshal([]byte(expectedString), &newGame)
		if err != nil {
			return "Unmarshal expected game failed: " + err.Error()
		}
		newMetadataManager = &newGame
	case models.ModelType_InGameContent:
		// Unmarshal the updated (actual) IGC
		var updatedIGC models.InGameContent
		err := json.Unmarshal([]byte(actualString), &updatedIGC)
		if err != nil {
			return "Unmarshal updated extension iap failed: " + err.Error()
		}
		updatedMetadataManager = &updatedIGC

		// Unmarshal the expected IGC
		var newIGC models.InGameContent
		err = json.Unmarshal([]byte(expectedString), &newIGC)
		if err != nil {
			return "Unmarshal expected igc failed: " + err.Error()
		}
		newMetadataManager = &newIGC
	case models.ModelType_Extension:
		// Unmarshal the updated (actual) Extension
		var updatedExtension models.Extension
		err := json.Unmarshal([]byte(actualString), &updatedExtension)
		if err != nil {
			return "Unmarshal updated extension failed: " + err.Error()
		}
		updatedMetadataManager = &updatedExtension

		// Unmarshal the expected Extension
		var newExtension models.Extension
		err = json.Unmarshal([]byte(expectedString), &newExtension)
		if err != nil {
			return "Unmarshal expected extension failed: " + err.Error()
		}
		newMetadataManager = &newExtension
	case models.ModelType_ExtensionInAppPurchase:
		// Unmarshal the updated (actual) Extension IAP
		var updatedExtensionIAP models.ExtensionIAP
		err := json.Unmarshal([]byte(actualString), &updatedExtensionIAP)
		if err != nil {
			return "Unmarshal updated extension iap failed: " + err.Error()
		}
		updatedMetadataManager = &updatedExtensionIAP

		// Unmarshal the expected Extension IAP
		var newExtensionIAP models.ExtensionIAP
		err = json.Unmarshal([]byte(expectedString), &newExtensionIAP)
		if err != nil {
			return "Unmarshal expected extension iap failed: " + err.Error()
		}
		newMetadataManager = &newExtensionIAP
	default:
		// Intentionally break
		return "No valid ManagedProductType passed in"
	}

	// This is required since these fields would not exist in the parsed test document
	// (and fields like id are also unique between different products)
	newMetadataManager.SetId(updatedMetadataManager.GetId())
	newMetadataManager.SetLastUpdated(updatedMetadataManager.GetLastUpdated())

	// Perform a deep equals to ensure both structs are the same
	eq := reflect.DeepEqual(updatedMetadataManager, newMetadataManager)
	if !eq {
		actualGame := fmt.Sprintf("Actual:\n%+v\n", updatedMetadataManager)
		expectedGame := fmt.Sprintf("Expected:\n%+v\n", newMetadataManager)
		return "Updated game != expected game." + actualGame + expectedGame
	} else {
		return ""
	}
}

func deleteSynthesizedKeys(toBeCleaned map[string]interface{}) map[string]interface{} {
	delete(toBeCleaned, "adgDomainId")
	delete(toBeCleaned, "adgProductId")
	delete(toBeCleaned, "adgVendorId")
	delete(toBeCleaned, "lastUpdated")
	return toBeCleaned
}

func createParentIfRequired(dppId string, cut *api.FuelManagedProductAPI, pbManagedProductType pb.ManagedProductType) {
	var parentType pb.ManagedProductType
	var validParentString string

	switch pbManagedProductType {
	case pb.ManagedProductType_ADG_IGC:
		parentType = pb.ManagedProductType_ADG_GAME
		validParentString = newValidGameString()
	case pb.ManagedProductType_ADG_EXTENSION_IAP:
		parentType = pb.ManagedProductType_ADG_EXTENSION
		validParentString = newValidExtensionsString()
	default: // Don't need to create parent products
		return
	}

	// The parent needs an ADGProduct Id, since we can't submit IGC or IAP without one
	var requestJsonMap map[string]interface{}
	err := json.Unmarshal([]byte(validParentString), &requestJsonMap)
	So(err, ShouldBeNil)

	requestJsonMap["adgProductId"] = "testADGProductId"

	requestJson, err := json.Marshal(requestJsonMap)
	So(err, ShouldBeNil)

	validParentString = string(requestJson)

	mpParentRequest := newValidCreateManagedProductRequest(dppId, parentType)
	newMPParent, err := cut.CreateManagedProduct(context.Background(), mpParentRequest)
	So(err, ShouldBeNil)

	newMPParent.Document = newValidDocument(newMPParent.Document.Id, validParentString)
	umprParent := newValidUpdateManagedProductRequest(newMPParent)
	_, err = cut.UpdateManagedProduct(context.Background(), umprParent)
	So(err, ShouldBeNil)
}
