package api

import (
	"code.justin.tv/commerce/tachanka/service"
	"code.justin.tv/common/goauthorization"
	"code.justin.tv/common/twirp"
	"github.com/Sirupsen/logrus"
	"github.com/satori/go.uuid"
	"golang.org/x/net/context"
)

type AuthorizationContextKey string

const (
	authKey = AuthorizationContextKey("TwitchAuthorizationToken")
)

type FuelManagedProductAPI struct {
	dpps    service.IDevPortalProductService
	mps     service.IManagedProductsService
	subs    service.ISubmissionService
	tts     service.ITwirpTranslator
	ems     service.IExternalMappingService
	pbs     service.IProductBackfillService
	vs      service.IValidationService
	decoder goauthorization.IDecoder
}

func NewFuelManagedProductAPIServer(dpps service.IDevPortalProductService, ms service.IManagedProductsService, ss service.ISubmissionService, tt service.ITwirpTranslator, em service.IExternalMappingService, pb service.IProductBackfillService, vs service.IValidationService, decoder goauthorization.IDecoder) *FuelManagedProductAPI {
	return &FuelManagedProductAPI{
		dpps:    dpps,
		mps:     ms,
		subs:    ss,
		tts:     tt,
		ems:     em,
		pbs:     pb,
		vs:      vs,
		decoder: decoder,
	}
}

func (this *FuelManagedProductAPI) getAuthorizationToken(ctx context.Context) string {
	// https://git-aws.internal.justin.tv/edge/visage/blob/master/internal/auth/context.go#L21
	if cartman, ok := ctx.Value(authKey).(string); ok {
		return cartman
	}
	// Return an empty string because the library we pass this into will handle this for us.
	return ""
}

func (this *FuelManagedProductAPI) validateAuthForDPPId(ctx context.Context, dppId string) twirp.Error {
	log := logrus.WithFields(logrus.Fields{
		"Function":           "validateAuthForDPPId",
		"DevPortalProductId": dppId,
	})
	log.Debug("Entering")
	token, err := this.decoder.Decode(this.getAuthorizationToken(ctx))
	if err != nil {
		log.Warn("Error decoding authorization token: ", err)
		return twirp.NewError(twirp.Unauthenticated, "Error decoding authorization token.")
	}

	capabilities := goauthorization.CapabilityClaims{
		"edit_product": goauthorization.CapabilityClaim{
			"product_id": dppId,
		},
	}

	err = this.decoder.Validate(token, capabilities)
	if err != nil {
		log.Warn("Error validating token or capabilities: ", err)
		return twirp.NewError(twirp.PermissionDenied, "Validation Failed: Permission Denied.")
	}
	log.Debug("Exiting")
	return nil
}

func (this *FuelManagedProductAPI) validateAuthForMPId(ctx context.Context, managedProductId string) (*service.DevPortalProduct, twirp.Error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":         "validateAuth",
		"ManagedProductId": managedProductId,
	})
	log.Debug("Entering")
	token, err := this.decoder.Decode(this.getAuthorizationToken(ctx))
	if err != nil {
		log.Warn("Error decoding authorization token: ", err)
		return nil, twirp.NewError(twirp.Unauthenticated, "Error decoding authorization token.")
	}

	// https://git-aws.internal.justin.tv/web/cartman-configs/blob/master/capabilities/devportal.json
	editProductClaim, ok := token.Claims.Authorizations["edit_product"]
	if !ok {
		log.Warn("Error checking authorization claim: ", err)
		return nil, twirp.NewError(twirp.PermissionDenied, "Invalid authorization token")
	}

	claimDevPortalProductId, ok := editProductClaim["product_id"].(string)
	if !ok {
		log.Warn("Error fetching claimed product: ", err)
		return nil, twirp.NewError(twirp.PermissionDenied, "Invalid authorization token")
	}
	claimDevPortalProduct, err := this.dpps.GetDevPortalProduct(claimDevPortalProductId)
	if err != nil {
		log.Warn("Error retrieving Dev Portal Product", err)
		return nil, twirp.InternalError("Error retrieving Dev Portal Product for this request")
	}

	found := false
	for _, mpId := range claimDevPortalProduct.ManagedProductIds {
		if mpId == managedProductId {
			found = true
			break
		}
	}
	if found == false {
		log.WithField("Dev Portal Product Id", claimDevPortalProductId).Warn("No Valid Managed Products under Dev Portal Product")
		return nil, twirp.NewError(twirp.PermissionDenied, "Permission Denied")
	}

	capabilities := goauthorization.CapabilityClaims{}
	err = this.decoder.Validate(token, capabilities)
	if err != nil {
		log.Warn("Forbidden request: ", err)
		return nil, twirp.NewError(twirp.PermissionDenied, "Permission Denied")
	}
	log.Debug("Exiting")
	return claimDevPortalProduct, nil

}

func (this *FuelManagedProductAPI) fetchDevPortalProduct(devPortalProductId string) (*service.DevPortalProduct, twirp.Error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":         "fetchDevPortalProduct",
		"DevPortalProduct": devPortalProductId,
	})
	log.Debug("Entering")
	dppuuid, err := uuid.FromString(devPortalProductId)
	if err != nil {
		log.Warn("Error parsing UUID from DPP ID: ", err)
		twerr := twirp.InvalidArgumentError("DevPortalProduct.DppID", "DppID is not a valid UUID")
		return nil, twerr
	}
	dppstring := dppuuid.String()

	fetchedProduct, err := this.dpps.GetDevPortalProduct(dppstring)
	if err != nil {
		log.Warn("Error retrieving Dev Portal Product: ", err)
		return nil, twirp.InternalError("Error Retrieving Dev Portal Product.")
	}
	log.Debug("Exiting")
	return fetchedProduct, nil
}
