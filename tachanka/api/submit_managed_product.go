package api

import (
	pb "code.justin.tv/commerce/tachanka-client"
	"code.justin.tv/common/twirp"
	"github.com/Sirupsen/logrus"
	"golang.org/x/net/context"
)

// Deprecated - Should use SubmitManagedProductWithResponse and delete this method once visage has upgraded
func (this *FuelManagedProductAPI) SubmitManagedProduct(ctx context.Context, smpr *pb.SubmitManagedProductRequest) (*pb.ManagedProduct, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":                    "SubmitManagedProduct",
		"SubmitManagedProductRequest": *smpr,
	})
	log.Debug("Entering")

	response, err := this.SubmitManagedProductWithResponse(ctx, smpr)
	if err != nil {
		log.Error("Error submiting managed product", err)
		log.Debug("Exiting")
		return nil, err
	}

	log.WithField("Response", response).Debug("Exiting")
	return response.ManagedProduct, nil
}

func (this *FuelManagedProductAPI) SubmitManagedProductWithResponse(ctx context.Context, smpr *pb.SubmitManagedProductRequest) (*pb.SubmitManagedProductResponse, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":                    "SubmitManagedProductWithResponse",
		"SubmitManagedProductRequest": *smpr,
	})
	log.Debug("Entering")
	dpp, twerr := this.validateAuthForMPId(ctx, smpr.ManagedProductId)
	if twerr != nil {
		log.Warn("Error validating authorization token for Managed Product: ", twerr)
		return nil, twerr
	}

	managedProductToSubmit, err := this.mps.GetManagedProduct(smpr.ManagedProductId)
	if err != nil {
		log.Warn("Error Fetching Managed Product: ", err)
		return nil, twirp.InternalError("Error Fetching Managed Product.")
	}

	// Validate objects required for submission
	validationErrors, err := this.vs.ValidateForSubmission(dpp, managedProductToSubmit)
	if err != nil {
		log.Error("Error validating submission", err)
		return nil, twirp.InternalError("Error validating submission")
	}

	if len(validationErrors) > 0 {
		log.Error("Submission didn't pass validation. See response for validation errors")

		responseValidationErrors := make([]*pb.ValidationError, len(validationErrors))
		for i, validationError := range validationErrors {
			responseValidationErrors[i] = this.tts.ConvertServiceValidationErrorToAPIValidationError(validationError)
		}
		response := &pb.SubmitManagedProductResponse{
			ValidationErrors: responseValidationErrors,
		}

		log.WithField("Response", response).Debug("Exiting")
		return response, twirp.NewError(twirp.FailedPrecondition, "Product is not ready for submission. See validation errors.")
	}

	// The product is ready for submission
	newsub, err := this.subs.SubmitManagedProduct(dpp, managedProductToSubmit, smpr.Tuid)
	if err != nil {
		log.Warn("Error Submitting Managed Product: ", err)
		return nil, twirp.InternalError("Error Submitting Managed Product.")
	}
	managedProductToSubmit.SetWorkingSubmissionId(newsub.Id)
	err = this.mps.UpdateManagedProduct(managedProductToSubmit)
	if err != nil {
		log.Warn("Error Updating Managed Product: ", err)
		return nil, twirp.InternalError("Error Updating Managed Product.")
	}

	translatedManagedProduct, err := this.tts.TranslateManagedProduct(dpp, managedProductToSubmit)
	if err != nil {
		log.Warn("Error translating Managed Product", err)
		return nil, twirp.InternalError("Error translating Managed Product.")
	}

	response := &pb.SubmitManagedProductResponse{
		ManagedProduct: translatedManagedProduct,
	}
	log.WithField("Response", response).Debug("Exiting")
	return response, nil
}
