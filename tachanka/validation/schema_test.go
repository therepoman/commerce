package validation

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"code.justin.tv/commerce/tachanka/models/adg"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/xeipuuv/gojsonschema"
)

var schema string = "file://" + os.Getenv("GOPATH") + "/src/code.justin.tv/commerce/tachanka/config/data/mulligan_respawn_game_schema.json"
var validDocumentFolder string = os.Getenv("GOPATH") + "/src/code.justin.tv/commerce/tachanka/validation/valid/"
var invalidDocumentFolder string = os.Getenv("GOPATH") + "/src/code.justin.tv/commerce/tachanka/validation/invalid/"

func TestValidDocument(t *testing.T) {
	validFiles, err := ioutil.ReadDir(validDocumentFolder)
	if err != nil {
		t.Log(err)
		t.Fatal("Failed to read validation files from valid folder")
	}
	var validTestFiles []os.FileInfo
	for _, file := range validFiles {
		if strings.HasSuffix(strings.ToLower(file.Name()), ".json") {
			validTestFiles = append(validTestFiles, file)
		}
	}

	invalidFiles, err := ioutil.ReadDir(invalidDocumentFolder)
	if err != nil {
		t.Log(err)
		t.Fatal("Failed to read validation files from invalid folder")
	}
	var invalidTestFiles []os.FileInfo
	for _, file := range invalidFiles {
		if strings.HasSuffix(strings.ToLower(file.Name()), ".json") {
			invalidTestFiles = append(invalidTestFiles, file)
		}
	}

	Convey("Given a valid schema", t, func() {
		Convey("When we test all valid documents", func() {
			for _, validFile := range validTestFiles {
				result, err := testDocument("file://"+validDocumentFolder+validFile.Name(), schema)
				So(err, ShouldBeNil)
				var resultStrings []string
				for _, res := range result.Errors() {
					resultStrings = append(resultStrings, res.Field()+": "+fmt.Sprintf("%v", res.Value())+"\n"+res.String())
				}

				if !result.Valid() {
					t.Log(validFile.Name() + "\n" + strings.Join(resultStrings, ",\n"))
				}

				So(result.Valid(), ShouldBeTrue)
			}
		})

		Convey("When we test all invalid documents", func() {
			for _, invalidFile := range invalidTestFiles {
				result, err := testDocument("file://"+invalidDocumentFolder+invalidFile.Name(), schema)
				So(err, ShouldBeNil)
				So(result.Valid(), ShouldBeFalse)
				var resultStrings []string
				for _, res := range result.Errors() {
					resultStrings = append(resultStrings, res.String())
				}

				if result.Valid() {
					t.Log(invalidFile.Name() + " should have failed validation but did not. \n")
				}
			}
		})

		Convey("When we test all valid models", func() {
			for _, validFile := range validTestFiles {
				bytes, err := ioutil.ReadFile(validDocumentFolder + validFile.Name())
				// There should be no error reading this file.
				So(err, ShouldBeNil)
				// Now we unmarshal into our model.
				model := adg.ADGDocument{}
				json.Unmarshal(bytes, &model)
				// Now that we have data into our model. We serialize it back.
				modeledBytes, err := json.Marshal(model)
				// Should be no error serializing....
				So(err, ShouldBeNil)
				// Now the serialized bytes should be valid.
				result, err := testByteDocument(modeledBytes, schema)
				var resultStrings []string
				for _, res := range result.Errors() {
					resultStrings = append(resultStrings, res.Field()+": "+fmt.Sprintf("%v", res.Value())+"\n"+res.String())
				}

				if !result.Valid() {
					t.Log(validFile.Name() + "\n" + strings.Join(resultStrings, ",\n"))
				}

				So(err, ShouldBeNil)
				So(result.Valid(), ShouldBeTrue)
			}
		})

		Convey("When we test all invalid models", func() {
			for _, invalidFile := range invalidTestFiles {
				bytes, err := ioutil.ReadFile(invalidDocumentFolder + invalidFile.Name())
				// There should be no error reading this file.
				So(err, ShouldBeNil)
				// Now we unmarshal into our model.
				model := adg.ADGDocument{}
				json.Unmarshal(bytes, &model)
				// Now that we have data into our model. We serialize it back.
				modeledBytes, err := json.MarshalIndent(model, "", "   ")
				// Should be no error serializing....
				So(err, ShouldBeNil)

				// Now the serialized bytes should be valid.
				result, err := testByteDocument(modeledBytes, schema)
				var resultStrings []string
				for _, res := range result.Errors() {
					resultStrings = append(resultStrings, res.Field()+": "+fmt.Sprintf("%v", res.Value())+"\n"+res.String())
				}

				if result.Valid() {
					t.Log(invalidFile.Name() + " should have failed validation but did not. \n")
				}

				So(err, ShouldBeNil)
				So(result.Valid(), ShouldBeFalse)

			}
		})
	})
}

func testByteDocument(bytes []byte, schemaUri string) (*gojsonschema.Result, error) {
	byteLoader := gojsonschema.NewBytesLoader(bytes)
	schemaLoader := gojsonschema.NewReferenceLoader(schema)

	result, err := gojsonschema.Validate(schemaLoader, byteLoader)
	if err != nil {
		return nil, err
	}

	return result, err
}

func testDocument(documentUri string, schemaUri string) (*gojsonschema.Result, error) {
	schemaLoader := gojsonschema.NewReferenceLoader(schemaUri)
	documentLoader := gojsonschema.NewReferenceLoader(documentUri)

	result, err := gojsonschema.Validate(schemaLoader, documentLoader)
	if err != nil {
		return nil, err
	}

	return result, err
}
