package client

import "code.justin.tv/commerce/tachanka/models/adg"

type (
	Request interface {
		GetRequestId() string
		GetClientID() string
	}

	CreateProductRequest struct {
		ClientID   string             `json:"clientID"`
		RequestID  string             `json:"requestID"`
		Submission *adg.ADGSubmission `json:"CreateProductRequest"`
	}
)

func (request CreateProductRequest) GetClientID() string {
	return request.ClientID
}

func (request CreateProductRequest) GetRequestId() string {
	return request.RequestID
}
