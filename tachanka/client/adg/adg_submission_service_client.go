package client

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"time"

	"code.justin.tv/commerce/tachanka/models/adg"
	"github.com/Sirupsen/logrus"
	"github.com/aws/aws-sdk-go/aws/signer/v4"
	uuid "github.com/satori/go.uuid"
)

const (
	twitchProxyService     = "TwitchProxyService"
	clientId               = "Tachanka"
	url                    = "https://twitch-proxy.amazon.com/adg/"
	method                 = "POST"
	createProductActivity  = "com.amazonaws.twitchproxyservice.TwitchProxyService.CreateProduct"
	createRevisionActivity = "com.amazonaws.twitchproxyservice.TwitchProxyService.CreateRevision"
)

type (
	ADGSubmissionServiceClient interface {
		CreateProduct(submission *adg.ADGSubmission) (Response, error)
		CreateRevision(submission *adg.ADGSubmission) (Response, error)
	}

	TProxADGSubmissionServiceClient struct {
		sigv4AWSRegion string
		sigV4Signer    *v4.Signer
	}
)

// Client

func NewTProxADGSubmissionServiceClient(sigV4AWSRegion string, sigV4Signer *v4.Signer) ADGSubmissionServiceClient {
	return TProxADGSubmissionServiceClient{
		sigv4AWSRegion: sigV4AWSRegion,
		sigV4Signer:    sigV4Signer,
	}
}

// Public

func (client TProxADGSubmissionServiceClient) CreateProduct(submission *adg.ADGSubmission) (Response, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function": "TProxADGSubmissionServiceClient.CreateProduct",
	})
	log.WithField("Submission", *submission).Debug("Entering")

	// Generate a request id
	requestId := uuid.NewV4().String()
	log.WithField("RequestId", requestId).Debug("Creating request for createProduct")

	// Create the request
	createProductRequest := &CreateProductRequest{
		ClientID:   clientId,
		RequestID:  requestId,
		Submission: submission,
	}

	httpRequest, err := client.createHTTPRequest(createProductActivity, createProductRequest)
	if err != nil {
		log.Warn("Error when creating request for createProduct call: ", err)
		return nil, err
	}

	// Call ADGSubmissionService
	response, err := client.call(httpRequest)
	if err != nil {
		log.Warn("Error when calling ADGSubmissionService", err)
		return nil, err
	}

	log.WithField("Response", response).Debug("Exiting")

	return response, nil
}

func (client TProxADGSubmissionServiceClient) CreateRevision(submission *adg.ADGSubmission) (Response, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function": "TProxADGSubmissionServiceClient.CreateRevision",
	})
	log.WithField("Submission", *submission).Debug("Entering")

	// Generate a request id
	requestId := uuid.NewV4().String()
	log.WithField("RequestId", requestId).Debug("Creating request for createProduct")

	// Create the request
	createRevisionRequest := &CreateProductRequest{
		ClientID:   clientId,
		RequestID:  requestId,
		Submission: submission,
	}

	httpRequest, err := client.createHTTPRequest(createProductActivity, createRevisionRequest)
	if err != nil {
		log.Warn("Error when creating request for createProduct call", err)
		return nil, err
	}

	// Call ADGSubmissionService
	response, err := client.call(httpRequest)
	if err != nil {
		log.Warn("Error when calling ADGSubmissionService", err)
		return nil, err
	}

	log.WithField("Response", response).Debug("Exiting")

	return response, nil
}

// Private

func (client TProxADGSubmissionServiceClient) call(request *http.Request) (*ADGSubmissionServiceResponse, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function": "TProxADGSubmissionServiceClient.performRequest",
	})
	log.Debug("Entering")

	// Make the call
	response, err := http.DefaultClient.Do(request)
	if err != nil {
		log.Warn("Error when making request", err)
		return nil, err
	}
	log.Debug("Finished calling ADGSubmissionService")

	return client.parseResponse(response)
}

func (client TProxADGSubmissionServiceClient) parseResponse(response *http.Response) (*ADGSubmissionServiceResponse, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function": "TProxADGSubmissionServiceClient.parseResponse",
	})
	log.Debug("Entering")

	// Read the body and log the results
	defer response.Body.Close()
	bodyBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Warn("Error when readying body of response", err)
		return nil, err
	}

	log.WithFields(logrus.Fields{
		"StatusCode": response.StatusCode,
		"Status":     response.Status,
		"Body":       string(bodyBytes),
	}).Debug("ADGSubmissionService Response")

	// Check the response code - TODO parse the error code and build a list of errors OR just return the response
	if 200 != response.StatusCode {
		log.WithField("StatusCode", response.StatusCode).Error("Error calling ADGSubmissionService")
		return nil, errors.New("Error calling ADGSubmissionService")
	}

	// Parse the response
	var parsedResponse ADGSubmissionServiceResponse
	err = json.Unmarshal(bodyBytes, &parsedResponse)
	if err != nil {
		log.Warn("Error when unmarshalling response body", err)
		return nil, err
	}

	log.WithField("ADGSubmissionServiceReponse", parsedResponse).Debug("Exiting")
	return &parsedResponse, nil
}

func (client TProxADGSubmissionServiceClient) createHTTPRequest(activity string, request Request) (*http.Request, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function": "TProxADGSubmissionServiceClient.createRequest",
		"Activity": activity,
	})
	log.WithField("Request", request).Debug("Entering")

	// Serialize body
	body, err := json.Marshal(request)
	if err != nil {
		log.Warn("Error when serializing submission", err)
		return nil, err
	}

	log.WithField("Request Body", string(body)).Debug("serialized request body")

	// Create a reader of the payload (needed for requests and signing)
	bodyReader := bytes.NewReader(body)

	// Create the http request
	httpRequest, err := http.NewRequest(method, url, bodyReader)
	if err != nil {
		log.Warn("Error when creating http request", err)
		return nil, err
	}

	// Set the headers for the request
	for header, value := range client.getHeaders(activity, request.GetRequestId()) {
		httpRequest.Header.Set(header, value)
	}

	// Sign the request with sigV4.
	_, err = client.sigV4Signer.Sign(httpRequest, bodyReader, twitchProxyService, client.sigv4AWSRegion, time.Now())
	if err != nil {
		log.Warn("Error signing the httpRequest with sigV4", err)
		return nil, err
	}

	log.Debug("Exiting")
	return httpRequest, nil
}

func (client TProxADGSubmissionServiceClient) getHeaders(activity, requestId string) map[string]string {
	headers := make(map[string]string)

	headers["Content-Type"] = "application/json; charset=UTF-8"
	headers["X-Requested-With"] = "XMLHttpRequest"
	headers["Content-Encoding"] = "amz-1.0"
	headers["User-Agent"] = clientId
	headers["X-Amz-Target"] = activity
	headers["x-amzn-RequestId"] = requestId
	headers["x-amz-client-id"] = clientId

	return headers
}
