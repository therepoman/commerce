package client

type (
	Response interface {
		GetADGProductId() string
	}

	ADGSubmissionServiceResponse struct {
		Product struct {
			Id  string `json:"id"`
			Sku string `json:"externalProductId"`
		} `json:"product"`
		Revision struct {
			Id            string `json:"id"`
			Sku           string `json:"externalProductId"`
			RevisionState string `json:"revisionState"`
		} `json:"revision"`
		Submission struct {
			Id string `json:"id"`
		}
		Errors []struct {
			Field   string `json:"field"`
			Message string `json:"message"`
			Type    string `json:"type"`
		} `json:"errors"`
		ValidationWarnings []string `json:"validationWarnings"`
		Message            string   `json:"message"`
	}
)

func (response ADGSubmissionServiceResponse) GetADGProductId() string {
	return response.Product.Id
}
