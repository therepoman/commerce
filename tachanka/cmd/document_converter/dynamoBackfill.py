import argparse
import boto3
from datetime import datetime
import json

dynamodb = boto3.resource('dynamodb')

def get_document_id(mp_id):
  mp_table = dynamodb.Table('prod_managed-products')
  mp_item = mp_table.get_item(
    Key={
      'id': mp_id
    },
    AttributesToGet=[
      'draft-document-id'
    ]
  )
  return mp_item['Item']['draft-document-id']

def get_document_data_id(doc_id):
  doc_table = dynamodb.Table('prod_documents')
  doc_item = doc_table.get_item(
    Key={
      'id': doc_id
    },
    AttributesToGet=[
      'documentData'
    ]
  )
  doc_json = json.loads(doc_item['Item']['documentData'])
  return doc_json['id']

def update_document_data(doc_id, doc_data_id, path_to_json_file):
  with open(path_to_json_file) as json_data:
    doc_table = dynamodb.Table('prod_documents')
    new_doc_data = json.load(json_data)
    new_doc_data['id'] = doc_data_id
    doc_table.update_item(
      Key={
        'id': doc_id
      },
      UpdateExpression='SET documentData = :val1',
      ExpressionAttributeValues={
        ':val1': json.dumps(new_doc_data)
      }
    )

def update_external_mapping(mp_id, adg_prod_id):
  ext_map_table = dynamodb.Table('prod_external-mapping')
  ext_map_table.put_item(
    Item={
      'parent-id': mp_id,
      'type': 'ADG_PRODUCT_ID',
      'value': adg_prod_id,
      'last_updated': datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
    }
  )

def main(mp_id, adg_prod_id, path_to_json_file):
  doc_id = get_document_id(mp_id)
  doc_data_id = get_document_data_id(doc_id)
  update_document_data(doc_id, doc_data_id, path_to_json_file)
  update_external_mapping(mp_id, adg_prod_id)

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description = 'Backfill managed product into DynamoDB')
  parser.add_argument('mp_id', help='Managed Product ID')
  parser.add_argument('adg_prod_id', help='ADG Product ID')
  parser.add_argument('path_to_json_file', help='Path to JSON file to save to DynamoDB')
  args = parser.parse_args()

  main(args.mp_id, args.adg_prod_id, args.path_to_json_file)