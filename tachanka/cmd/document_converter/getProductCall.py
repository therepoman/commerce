import json, os, requests

CallHeaders = { 
'Host': 'coral.amazon.com',
'Accept': 'application/json, text/javascript, */*; q=0.01',
'Accept-Language': 'en-US,en;q=0.5',
'Accept-Encoding': 'gzip, deflate, br',
'Content-Type': 'application/json',
'x-amz-explorer-tags': '{"diver_service_name":"ADGProductService","deployment_friendly_name":"NA/Prod","options":{"endpoint":"https://adg-product-backend.iad.amazon.com:443/aaa/","target_network":"iad","protocol":"coralrpc","request_id":"00000","request_timeout":"30","gat_trace":true,"pretty_print":true,"omit_nulls":false,"cardinality":"single","auth_type":"aaa","auth_details":{},"set_test_call":false,"custom_headers":[]},"operation":{"name":"getProduct","operation_id":"com.amazon.adg.product.service.model#getProduct","service_id":"com.amazon.adg.product.service.model#ADGProductService"}}',
'x-amz-explorer-protocol': 'coralrpc',
'X-Amz-Target': 'com.amazon.adg.product.service.model.ADGProductService.getProduct',
'x-amz-diver-request-timeout': '30',
'X-Requested-With': 'XMLHttpRequest',
'Content-Length': '205',
'x-amzn-RequestId': '00000',
'Cookie': 'SOME_Cookie',
'Connection': 'keep-alive'
}

BODY_PATH = 'getProductBody.json'

def makeProductCall(cookie, asin):
    url = 'https://coral.amazon.com/proxy/call_service/json'

    # Edit the JSON Request
    with open(BODY_PATH, 'r') as f:
        data = json.load(f)
        # Update the request with the given asin
        idMap = {"asin" : asin}
        data['Input']['product'] = idMap
    try:
        os.remove(BODY_PATH)
    except Exception as e:
        print 'File did not exit.'

    with open(BODY_PATH, 'w') as f:
        json.dump(data, f, indent=4)

    body = json.load(open(BODY_PATH))

    # Update the cookie
    CallHeaders['Cookie'] = cookie
    r = requests.post(url, headers=CallHeaders, data=json.dumps(body))

    # Parse the response
    jsonResponse = json.loads(r.content)
    # Have to load twice to maintain the dictionary
    responseBody = json.loads(jsonResponse['response']['body'])
    productBody = responseBody['Output']['product']
    return productBody


