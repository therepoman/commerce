package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"code.justin.tv/commerce/tachanka/models"
	"code.justin.tv/commerce/tachanka/models/adg"
	"code.justin.tv/commerce/tachanka/models/currency"
)

const (
	TitleDocumentGameType            string = "game"
	TitleDocumentExtensionType       string = "extension"
	TitleDocumentConsumableType      string = "consumable"
	TitleDocumentEntitlementType     string = "entitlement"
	TitleDocumentVirtualCurrencyType string = "virtual_currency"
	AmazonMarketplace                string = "amazon.com"
	GameTitleType                    string = "RESPAWN_GAME"
	ConsumableProductType            string = "consumable"
)

type Vendor struct {
	Id string `json:"id"`
}

type TitleDocument struct {
	Id                    string   `json:"id"`
	Name                  string   `json:"name"`
	Sku                   string   `json:"sku"`
	FulfillmentMethod     string   `json:"fuelStoreAvailability"`
	ContentType           string   `json:"contentType"`
	GlProductGroup        string   `json:"glProductGroup"`
	GlpgCategory          string   `json:"glpgCategory"`
	GlpgSubcategory       string   `json:"glpgSubcategory"`
	Format                string   `json:"format"`
	TitleTypes            []string `json:"titleTypes"`
	Vendor                Vendor   `json:"vendor"`
	PurchaseLifecycleType string   `json:"purchaseLifecycleType"`
}

func main() {
	v1Document := flag.String("v1", "", "Version 1 GAME Document to Convert")
	mulliganTitle := flag.String("title", "", "Mulligan title document to convert")
	mulliganBase := flag.String("base", "", "Mulligan base document to convert")
	flag.Parse()
	if *v1Document != "" {
		convertV1Document(*v1Document)
	} else if *mulliganBase != "" && *mulliganTitle != "" {
		convertMulliganDocs(*mulliganBase, *mulliganTitle)
	} else {
		fmt.Println("Must provide either a v1 document or a mulligan title AND mulligan base document")
	}
}

func convertMulliganDocs(baseFileName string, titleFileName string) {
	// Create the TitleDocument
	titleDocument := generateTitle(titleFileName)

	// Create the ADG Document from the base document
	base := generateBaseDocument(baseFileName, titleDocument.ContentType, titleDocument.TitleTypes[0])

	// Combine the two documents into the ADG Document
	base.FulfillmentMethod = titleDocument.FulfillmentMethod

	// Convert the ADG Document to the appropriate model
	managedProduct := constructManagedProductMetadata(titleDocument)

	suffix := fmt.Sprintf("_VENDORID_" + titleDocument.Vendor.Id + "_SKU_" + titleDocument.Sku + ".json")
	if areEqualIgnoreCase(titleDocument.ContentType, TitleDocumentGameType) {
		// Construct the game
		convertedTitle := fmt.Sprintf("convertedModels/GAME_MODEL" + suffix)
		game := models.NewGame(managedProduct)
		reverseConvertGame(base, titleDocument, game)
		generateTachankaModelJsonFile(game, convertedTitle)
	} else if areEqualIgnoreCase(titleDocument.ContentType, TitleDocumentExtensionType) {
		if areEqualIgnoreCase(titleDocument.TitleTypes[0], GameTitleType) {
			// If it's the actual extension
			convertedTitle := fmt.Sprintf("convertedModels/EXT_MODEL" + suffix)
			extension := models.NewExtension(managedProduct)
			reverseConvertExtension(base, titleDocument, extension)
			generateTachankaModelJsonFile(extension, convertedTitle)
		} else {
			// Otherwise, it's an in-app purchase
			convertedTitle := fmt.Sprintf("convertedModels/EXT_IAP_MODEL" + suffix)
			extensionIAP := models.NewExtensionIAP(managedProduct)
			reverseConvertExtensionIAP(base, titleDocument, extensionIAP)
			generateTachankaModelJsonFile(extensionIAP, convertedTitle)
		}
	} else if isIGC(titleDocument.ContentType) {
		// Construct the IGC
		convertedTitle := fmt.Sprintf("convertedModels/IGC_MODEL" + suffix)
		igc := models.NewInGameContent(managedProduct)
		reverseConvertIGC(base, titleDocument, igc)
		generateTachankaModelJsonFile(igc, convertedTitle)
	} else {
		message := fmt.Sprintf("Unexpected contentType: %s\n", titleDocument.ContentType)
		panic(message)
	}
}

func convertV1Document(fileName string) {
	bytes, err := ioutil.ReadFile(fileName)
	if err != nil {
		message := fmt.Sprintf("Failed to open %s!\n", fileName)
		panic(message)
	}

	var base adg.ADGDocument
	err = json.Unmarshal(bytes, &base)
	if err != nil {
		panic(err)
	}

	convertedTitle := fmt.Sprintf("convertedModels/converted_GAME_MODEL_" + fileName)
	var titleDocument TitleDocument
	managedProduct := constructManagedProductMetadata(titleDocument)
	game := models.NewGame(managedProduct)
	reverseConvertGame(base, titleDocument, game)
	generateTachankaModelJsonFile(game, convertedTitle)
}

// Helper method to determine if the content type is IGC
func isIGC(contentType string) bool {
	// If any of these checks are true, then the item is IGC
	return areEqualIgnoreCase(contentType, TitleDocumentEntitlementType) ||
		areEqualIgnoreCase(contentType, TitleDocumentConsumableType) ||
		areEqualIgnoreCase(contentType, TitleDocumentVirtualCurrencyType)
}

// Helper method to generate the final JSON file from a given interface
func generateTachankaModelJsonFile(t interface{}, fileName string) {
	// Output the file
	// A custom JSON marshaller is required
	converted, err := JSONMarshal(t)
	if err != nil {
		panic(err)
	}

	fmt.Printf("Conversion complete! Writing to %s\n", fileName)
	// Panic if the file exists, so we can easily tell if we have incorrect, duplicate files
	if fileExists(fileName) {
		panic("File already exists!")
	}
	err = ioutil.WriteFile(fileName, converted, 0644)
	if err != nil {
		panic(err)
	}
	return
}

// Helper method to generate the TitleDocument from the given titleFileName
func generateTitle(titleFileName string) TitleDocument {
	titleFile, err := ioutil.ReadFile(titleFileName)
	if err != nil {
		message := fmt.Sprintf("Failed to open %s!\n", titleFileName)
		panic(message)
	}

	var title TitleDocument
	err = json.Unmarshal(titleFile, &title)
	if err != nil {
		panic(err)
	}

	// Sanity check since this is used for various pivots
	if len(title.TitleTypes) != 1 {
		message := fmt.Sprintf("There can only be a single item in TitleTypes %s\n", title.TitleTypes)
		panic(message)
	}
	return title
}

// Helper method to generate the base ADGDocument from the baseFileName
func generateBaseDocument(baseFileName string, contentType string, titleType string) adg.ADGDocument {
	baseFile, err := ioutil.ReadFile(baseFileName)
	if err != nil {
		message := fmt.Sprintf("Failed to open %s!\n", baseFileName)
		panic(message)
	}

	// Generate the base ADG document
	base := generateBaseADGDocument(contentType, titleType)
	err = json.Unmarshal(baseFile, &base)
	if err != nil {
		panic(err)
	}
	return base
}

// Helper method to generate the base ADGDocument for a given contentType
func generateBaseADGDocument(contentType string, titleType string) adg.ADGDocument {
	var base adg.ADGDocument
	// Support games
	if areEqualIgnoreCase(contentType, TitleDocumentGameType) {
		base = adg.NewBaseGameDocument()
	} else if areEqualIgnoreCase(contentType, TitleDocumentExtensionType) {
		if areEqualIgnoreCase(titleType, TitleDocumentGameType) {
			base = adg.NewBaseExtensionDocument()
		} else {
			base = adg.NewBaseExtensionIAPDocument()
		}
	} else if isIGC(contentType) {
		base = adg.NewBaseIGCDocument()
	} else {
		// Panic on everything else
		message := fmt.Sprintf("Unsupported contentType passed in %s\n", contentType)
		panic(message)
	}
	return base
}

// Helper method to generate the ManagedProductMetadata from the TitleDocument
func constructManagedProductMetadata(titleDocument TitleDocument) models.ManagedProductMetadata {
	var managedProductMetadata models.ManagedProductMetadata
	managedProductMetadata.Id = titleDocument.Id
	managedProductMetadata.LastUpdated = models.ReadOnlyTime{Time: time.Now()}
	return managedProductMetadata
}

// Helper method to generate an IGC from the given documents
func reverseConvertIGC(document adg.ADGDocument, titleDocument TitleDocument, igc *models.InGameContent) {
	igc.MultiplePurchases = reverseDetermineIGCAllowsMultiplePurchases(titleDocument)
	igc.InGameCurrency = reverseDetermineIGCIsInGameCurrency(titleDocument)
	igc.Durable = reverseDetermineIGCISDurable(titleDocument)
	igc.CategoryInfoId = document.CategoryInfo.CategoryId
	igc.DefaultPrice = currency.Amount(convertADGDefaultPrice(document.PricingInfo.PricesByMarketplaceAndCountry))
	igc.AvailabilityInfo = convertADGAvailabilityInfo(document.PricingInfo.PricesByMarketplaceAndCountry)
	igc.ReleaseDate = document.ReleaseDate
	igc.LocaleListings = convertADGLocaleListingIGC(document.ListingsByLocale)
	igc.FulfillmentMethod = document.FulfillmentMethod
	igc.SKU = titleDocument.Sku
}

// Helper method to generate an ExtensionIAP from the given documents
func reverseConvertExtensionIAP(document adg.ADGDocument, titleDocument TitleDocument, extensionIAP *models.ExtensionIAP) {
	extensionIAP.SKU = titleDocument.Sku
	extensionIAP.MultiplePurchases = reverseDetermineExtensionsIAPAllowsMultiplePurchases(titleDocument)
	extensionIAP.Durable = reverseDetermineExtensionsIAPIsDurable(titleDocument)
	extensionIAP.DefaultPrice = currency.Amount(convertADGDefaultPrice(document.PricingInfo.PricesByMarketplaceAndCountry))
	extensionIAP.AvailabilityInfo = convertADGAvailabilityInfoExtensionIAP(document.PricingInfo.PricesByMarketplaceAndCountry)
	extensionIAP.LocaleListings = convertADGLocaleListingExtensionIAP(document.ListingsByLocale)
	extensionIAP.ItemType = reverseDetermineExtensionsIAPItemType(titleDocument)
}

// Helper method to generate an Extension from the given documents
func reverseConvertExtension(document adg.ADGDocument, titleDocument TitleDocument, extension *models.Extension) {
	extension.Sku = titleDocument.Sku
	extension.DeveloperName = document.DeveloperInfo.DeveloperName
	extension.DeveloperWebsiteUrl = document.DeveloperWebsiteUrl
	extension.CustomerSupportUrl = document.CustomerSupportUrl
	extension.CustomerSupportEmail = document.CustomerSupportEmail
	extension.LocaleListings = convertADGLocaleListingExtensions(document.ListingsByLocale)
}

// Helper method to generate a Game from the given documents
func reverseConvertGame(document adg.ADGDocument, titleDocument TitleDocument, game *models.Game) {
	game.PCRequirementsInfo = convertADGPCRequiremtnsInfo(document.BinaryInfo.PCRequirementInfo)
	game.RequiresContinuousConnectivity = document.BinaryInfo.RequiresContinuousConnectivity
	game.NumberOfPlayersSupported = document.BinaryInfo.NumberOfPlayersSupported
	game.LanguageSupport = convertADGLanguageSupport(document.BinaryInfo.LanguageSupport)
	game.Version = document.BinaryInfo.Version
	game.SupportedControllers = document.BinaryInfo.SupportedControllers
	game.CategoryInfoId = document.CategoryInfo.CategoryId
	game.DeveloperName = document.DeveloperInfo.DeveloperName
	game.ContentRatingInfo = convertADGContentRatingInfo(document.ContentRatingInfo)
	game.DefaultPrice = currency.Amount(convertADGDefaultPrice(document.PricingInfo.PricesByMarketplaceAndCountry))
	game.AvailabilityInfo = convertADGAvailabilityInfo(document.PricingInfo.PricesByMarketplaceAndCountry)
	game.ReleaseDate = document.ReleaseDate
	game.AnnounceDate = document.AnnounceDate
	game.PreviousReleaseDate = document.PreviousReleaseDate
	game.DeveloperWebsiteURL = document.DeveloperWebsiteUrl
	game.BroadcasterVODUrls = document.BroadcasterVodUrls
	game.NewsAndReviewsURLs = convertToListOfUrls(document.NewsAndReviewsUrls)
	game.CustomerSupportURL = document.CustomerSupportUrl
	game.CustomerSupportEmail = document.CustomerSupportEmail
	game.LocaleListings = convertADGLocaleListing(document.ListingsByLocale)
	game.FulfillmentMethod = document.FulfillmentMethod
	game.SKU = titleDocument.Sku
}

func reverseDetermineIGCAllowsMultiplePurchases(titleDocument TitleDocument) bool {
	// Assume 63402000 means consumable (multiple purchases)
	if titleDocument.GlpgCategory == strconv.FormatInt(adg.ProductCategory_2000, 10) {
		return true
	}
	return false
}

func reverseDetermineIGCISDurable(titleDocument TitleDocument) bool {
	// Assume 63403000 means durable
	if titleDocument.GlpgCategory == strconv.FormatInt(adg.ProductCategory_3000, 10) {
		return true
	}
	return false
}

func reverseDetermineIGCIsInGameCurrency(titleDocument TitleDocument) bool {
	// Assume 63404000 means in game currency
	if titleDocument.GlpgCategory == strconv.FormatInt(adg.ProductCategory_4000, 10) {
		return true
	}
	return false
}

func reverseDetermineExtensionsIAPAllowsMultiplePurchases(titleDocument TitleDocument) bool {
	// Consumables allow multiple purchases
	return areEqualIgnoreCase(titleDocument.PurchaseLifecycleType, ConsumableProductType)
}

func reverseDetermineExtensionsIAPIsDurable(titleDocument TitleDocument) bool {
	// This value doesn't really matter and will likely be removed soon. However,
	// it is safe to assume that consumables are generally not durable while entitlements are
	return !areEqualIgnoreCase(titleDocument.PurchaseLifecycleType, ConsumableProductType)
}

func reverseDetermineExtensionsIAPItemType(titleDocument TitleDocument) models.ExtensionItemType {
	// Switch based on the category. See Extensions tab in:
	// https://docs.google.com/spreadsheets/d/13CG9LT1E-a7VuG4dudU9O9ywFEN4I6kv8MNfITIi_YQ/
	switch titleDocument.GlpgCategory {
	case strconv.FormatInt(adg.ProductCategoryExt_10000, 10):
		return models.ExtensionItemType_Other
	case strconv.FormatInt(adg.ProductCategoryExt_10100, 10):
		return models.ExtensionItemType_RemotelyAccessedDigitalGood
	case strconv.FormatInt(adg.ProductCategoryExt_10200, 10):
		return models.ExtensionItemType_RemotelyAccessedDigitalGame
	case strconv.FormatInt(adg.ProductCategoryExt_10300, 10):
		return models.ExtensionItemType_GeneralDigitalGood
	case strconv.FormatInt(adg.ProductCategoryExt_10400, 10):
		return models.ExtensionItemType_StreamingAudio
	case strconv.FormatInt(adg.ProductCategoryExt_10500, 10):
		return models.ExtensionItemType_Services
	case strconv.FormatInt(adg.ProductCategoryExt_10600, 10):
		return models.ExtensionItemType_Subscriptions
	default:
		message := fmt.Sprintf("Invalid extensions IAP GlpgCategory %s for sku: with name %s%s\n", titleDocument.GlpgCategory, titleDocument.Sku, titleDocument.Name)
		panic(message)

	}
}

func convertADGLocaleListingIGC(adgListingByLocale map[string]adg.LocaleListing) []models.IGCLocaleListing {
	var igcLocalListingList []models.IGCLocaleListing
	for localeKey, adgLocaleListing := range adgListingByLocale {
		igcLocalListingList = append(igcLocalListingList, models.IGCLocaleListing{
			Locale:           localeKey,
			DisplayName:      adgLocaleListing.DisplayName,
			ShortDescription: adgLocaleListing.ShortDescription,
			LongDescription:  adgLocaleListing.LongDescription,
			Keywords:         adgLocaleListing.Keywords,
			IconId:           adgLocaleListing.IconId,
		})
	}
	return igcLocalListingList
}

func convertADGLocaleListingExtensions(adgListingByLocale map[string]adg.LocaleListing) []models.Listing {
	var listingList []models.Listing
	for localeKey, adgLocaleListing := range adgListingByLocale {
		listingList = append(listingList, models.Listing{
			Locale:      localeKey,
			DisplayName: adgLocaleListing.DisplayName,
			Keywords:    adgLocaleListing.Keywords,
		})
	}
	return listingList
}

func convertADGLocaleListingExtensionIAP(adgListingByLocale map[string]adg.LocaleListing) []models.ExtensionIAPListing {
	var extensionsIAPListing []models.ExtensionIAPListing
	for localeKey, adgLocaleListing := range adgListingByLocale {
		extensionsIAPListing = append(extensionsIAPListing, models.ExtensionIAPListing{
			BoxshotId:        adgLocaleListing.BoxshotId,
			Locale:           localeKey,
			DisplayName:      adgLocaleListing.DisplayName,
			ShortDescription: adgLocaleListing.ShortDescription,
			LongDescription:  adgLocaleListing.LongDescription,
			Keywords:         adgLocaleListing.Keywords,
		})
	}
	return extensionsIAPListing
}

func convertADGLocaleListing(adgListingByLocale map[string]adg.LocaleListing) []models.Locale {
	var localeListing []models.Locale
	for localeKey, adgLocaleListing := range adgListingByLocale {
		localeListing = append(localeListing, models.Locale{
			Locale:                   localeKey,
			DisplayName:              adgLocaleListing.DisplayName,
			ShortDescription:         adgLocaleListing.ShortDescription,
			LongDescription:          adgLocaleListing.LongDescription,
			ProductFeatures:          adgLocaleListing.ProductFeatures,
			Keywords:                 adgLocaleListing.Keywords,
			GameEdition:              adgLocaleListing.GameEdition,
			GameEditionDetails:       adgLocaleListing.GameEditionDetails,
			IconId:                   adgLocaleListing.IconId,
			ScreenshotIds:            adgLocaleListing.ScreenshotIds,
			PageBackgroundId:         adgLocaleListing.PageBackgroundId,
			EulaUrl:                  adgLocaleListing.EulaUrl,
			ReleaseNotesId:           adgLocaleListing.ReleaseNotesId,
			ThankYouPageBackgroundId: adgLocaleListing.ThankYouPageBackgroundImageId,
			BoxShotId:                adgLocaleListing.BoxshotId,
		})
	}
	return localeListing
}

func convertADGAvailabilityInfoExtensionIAP(adgMarketplacePrices map[string]adg.MarketplacePrices) []models.ExtensionIAPCountry {
	marketplace, ok := adgMarketplacePrices[AmazonMarketplace]
	if !ok {
		panic("No Amazon Marketplace exists")
	}

	var extensionIAPCountry []models.ExtensionIAPCountry
	for _, countryCode := range adg.GetAllSellableCountries() {
		prices, ok := marketplace.PricesByCountry[countryCode]
		if ok {
			extensionIAPCountry = append(extensionIAPCountry, models.ExtensionIAPCountry{
				CountryCode: countryCode,
				Price:       prices.Amount,
				Available:   true,
			})
		} else {
			extensionIAPCountry = append(extensionIAPCountry, models.ExtensionIAPCountry{
				CountryCode: countryCode,
				Price:       prices.Amount,
				Available:   false,
			})
		}
	}
	return extensionIAPCountry
}

func convertADGAvailabilityInfo(adgMarketplacePrices map[string]adg.MarketplacePrices) []models.AvailabilityEntry {
	marketplace, ok := adgMarketplacePrices[AmazonMarketplace]
	if !ok {
		panic("No Amazon Marketplace exists")
	}

	var marketplacePrices []models.AvailabilityEntry
	for _, countryCode := range adg.GetAllSellableCountries() {
		prices, ok := marketplace.PricesByCountry[countryCode]
		if ok {
			marketplacePrices = append(marketplacePrices, models.AvailabilityEntry{
				CountryCode: countryCode,
				Price:       prices.Amount,
				Available:   true,
			})
		} else {
			marketplacePrices = append(marketplacePrices, models.AvailabilityEntry{
				CountryCode: countryCode,
				Price:       prices.Amount,
				Available:   false,
			})
		}
	}
	return marketplacePrices
}

func convertADGDefaultPrice(adgMarketplacePrices map[string]adg.MarketplacePrices) float64 {
	marketplace, ok := adgMarketplacePrices[AmazonMarketplace]
	if !ok {
		panic("No Amazon Marketplace exists")
	}
	return float64(marketplace.DefaultListPrice.Amount)
}

func convertToListOfUrls(values []string) []string {
	urls := []string{}
	if values != nil {
		for _, url := range values {
			if isValidUrl(url) {
				urls = append(urls, url)
			}
		}
	}

	return urls
}

func convertADGContentRatingInfo(adgCRI adg.ContentRatingInfo) models.ContentRatingInfo {
	esrbRating := adgCRI.EsrbRatingInfo.RatingCategory
	if len(esrbRating) == 0 {
		esrbRating = adgCRI.EsrbRating
	}
	if len(esrbRating) == 0 {
		esrbRating = "rating_pending"
	}

	pegiRating := adgCRI.PegiRating
	if len(pegiRating) == 0 {
		pegiRating = "unknown"
	}

	var contentRatingInfo models.ContentRatingInfo
	contentRatingInfo = models.ContentRatingInfo{
		PEGIRating: pegiRating,
		USKRating:  "unknown",
		ESRBRatingInfo: models.ESRBRatingInfo{
			RatingCategory: esrbRating,
		},
	}
	return contentRatingInfo
}

func convertADGPCRequiremtnsInfo(info adg.PCRequirementsInfo) models.PCRequirementsInfo {
	var reqInfo models.PCRequirementsInfo
	reqInfo.MinimumRequirements = convertRequirementsInfo(info.MinimumRequirements)
	reqInfo.RecommendedRequirements = convertRequirementsInfo(info.RecommendedRequirements)
	reqInfo.OtherRequirements = info.OtherRequirements
	return reqInfo
}

func convertRequirementsInfo(info adg.RequirementsInfo) models.PCRequirements {
	var req models.PCRequirements
	req.DirectX = info.DirectX
	req.OS = info.OS
	req.Processor = info.Processor
	req.HardDriveSpace = info.HardDriveSpace
	req.RAM = info.RAM
	req.VideoCard = info.VideoCard
	return req
}

func convertADGLanguageSupport(support adg.LanguageSupport) []models.Language {
	var supportedLanguages []models.Language
	for _, language := range adg.GetAllLanguages() {
		supportedLanguages = append(supportedLanguages, models.Language{
			LanguageCode:         language,
			Instructions:         stringInSlice(language, support.Instructions),
			Audio:                stringInSlice(language, support.Audio),
			SubtitlesAndCaptions: stringInSlice(language, support.SubtitlesAndCaptions),
		})
	}
	return supportedLanguages
}

// Helper method to compare two string without caring about case
func areEqualIgnoreCase(a string, b string) bool {
	return strings.ToLower(a) == strings.ToLower(b)
}

func isValidUrl(toTest string) bool {
	_, err := url.ParseRequestURI(toTest)
	if err != nil {
		return false
	} else {
		return true
	}
}

// Helper method to check whether a string is in a given slice (list)
func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func fileExists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	// If a different error occurred, assume the file exists
	return true
}

// Helper method to Marshal JSON properly
// See https://stackoverflow.com/questions/28595664/how-to-stop-json-marshal-from-escaping-and
func JSONMarshal(t interface{}) ([]byte, error) {
	buffer := &bytes.Buffer{}
	encoder := json.NewEncoder(buffer)
	encoder.SetEscapeHTML(false)
	err := encoder.Encode(t)
	return buffer.Bytes(), err
}
