import os, subprocess, sys, csv, getProductCall, time, argparse, json, io, uuid, datetime

# Conversion folder paths
NEW_JSON_FOLDER = 'convertedJSON'
NEW_MODELS_FOLDER = 'convertedModels'
NEW_SUBMISSION_FOLDER = 'convertedSubmissions'

# Conversion errors
UNSUPPORTED_CONTENT_TYPE = 'Unsupported contentType passed in'
ADG_CALL_CSV = 'adgProductInformation.csv'
INVALID_EXTENSION_IAP_CATEGORY = 'Invalid extensions IAP GlpgCategory'

# CSV column names
ADG_PRODUCT_ID = 'ADG_PRODUCT_ID'
ADG_DOMAIN_ID = 'ADG_DOMAIN_ID'
ADG_SKU = 'ADG_SKU'
ADG_VENDOR_ID = 'ADG_VENDOR_ID'
ADG_ASIN = 'ADG_ASIN'
ASIN = 'asin'
SKU = 'sku'

# File delimeters
SKU_DELIMETER = '_SKU_'
VENDOR_DELIMETER = '_VENDORID_'

# See https://jira.twitch.com/browse/SHOP-377 for ignore list
IGNORE_DOCUMENTS = ['SubmissionBaseTest@0.1.ion', '_contentType@0.1.ion', '_ryan@0.1.ion','.json', 'validNeptuniaRebirth3']

# A handful of Mulligan skus are incorrectly named
SKU_TRANSLATION_DICTIONARY = {
'Steep - Road to The Olympics':'Steep - Road to the Olympics',
'System Shock 2 - Germany': 'System Shock 2 Germany',
'frozensynapse': 'frozen synapse',
"Assassin's Creed Origins - HELIX CREDITS BASE PACK ": "Assassin's Creed Origins - HELIX CREDITS BASE PACK",
'battlerite_twitch': 'battlerite_game',
'elex_game_3': 'elex_game',
'Relics of Zaron \xe2\x80\x93 Stick of Truth Costumes and Perks Pack': 'Relics of Zaron _ Stick of Truth Costumes and Perks Pack'}

# Progress checks, sleep timers
CONVERSION_PROGRESS_CHECK = 250
ADG_CALL_PROGRESS_CHECK = 15
SLEEP_TIME = 3

# Convert the given targetFolder's ion files into JSON, excluding anything that matches IGNORE_DOCUMENTS
def generateJSON(targetFolder):
    if not os.path.exists(NEW_JSON_FOLDER):
        os.makedirs(NEW_JSON_FOLDER)

    numTitles = 0
    failures = []
    productList = []
    for filename in os.listdir(targetFolder):
        # Ignore unnecessary documents
        if shouldIgnoreFile(filename):
            continue
        # Also handle the fact that only "valid" titles are required
        if not filename.lower().startswith('valid'):
            continue
        # Generate the new JSON file
        newPath = NEW_JSON_FOLDER + '/' +  filename.replace('@0.1.ion', '.json')
        try:
            subprocess.check_output(['java', '-jar', 'ion-to-json.jar', targetFolder + '/' + filename, newPath], stderr=subprocess.STDOUT)
            numTitles += 1
        except subprocess.CalledProcessError as e:
            failures.append(filename)
        # Maintain the list of titles
        if 'TitleBaseTest@' in filename:
            product = filename.split('TitleBaseTest@')[0]
            productList.append(product)
        # Another sanity check
        if 'BaseTest@' not in filename:
            print filename
        if numTitles % CONVERSION_PROGRESS_CHECK == 0:
            print 'Completed converting ' + str(numTitles) + ' files'

    print 'Failed to convert the following files: ' + str(failures)
    print 'Done! Converted the following number of titles from ion to json: ' + str(len(productList))
    print 'Sanity check for the number of files converted (should be 2x list length): ' + str(numTitles)
    return productList

# Convert the JSON mulligan documents into tachanka models
def generateTachankaModels(productList):
    if not os.path.exists(NEW_MODELS_FOLDER):
        os.makedirs(NEW_MODELS_FOLDER)

    numCompleted = 0
    unsupportedContentType = {}
    extensionIAPErrors = {}
    otherIssue = {}
    successfulModels = []
    for product in productList:
        # Generate the new model
        base = NEW_JSON_FOLDER + '/' + product + 'BaseTest.json'
        title = NEW_JSON_FOLDER + '/' + product + 'TitleBaseTest.json'
        try:
            subprocess.check_output(['go', 'run', 'main.go', '-title', title, '-base', base], stderr=subprocess.STDOUT)
            successfulModels.append(product)
            numCompleted += 1
        except subprocess.CalledProcessError as e:
            err = e.output

            # Handle the two most common errors
            if UNSUPPORTED_CONTENT_TYPE in str(err):
                # TODO: Make this actually decent and not prone to really simple errors
                contentType = str(err).split(UNSUPPORTED_CONTENT_TYPE)[1].split("\n")[0]
                safeAddToDictionaryList(unsupportedContentType, contentType, product)
            elif INVALID_EXTENSION_IAP_CATEGORY in str(err):
                # TODO: Make this actually decent and not prone to really simple errors
                extensionIAPCategory = str(err).split(INVALID_EXTENSION_IAP_CATEGORY)[1].split("for sku:")[0]
                safeAddToDictionaryList(extensionIAPErrors, extensionIAPCategory, product)
            else:
                otherIssue[product] = str(err)
        if numCompleted % CONVERSION_PROGRESS_CHECK == 0:
            print 'Completed converting ' + str(numCompleted) + ' mulligan titles'

    print 'Successful list: ' + str(successfulModels)
    print 'Done! Converted the following number of titles to the new tachanka model: ' + str(len(successfulModels))
    print 'Unsupported Content Types: ' + str(unsupportedContentType)
    print 'Extension IAP Categories are Incorrect: ' + str(extensionIAPErrors)
    print 'Other issues: ' + str(otherIssue)
    return successfulModels

'''
Finalize the tachanka models using the (source-of-truth) information from ADG, and
generate relevant external mapping dynamo JSONs
'''
def finalizeTachankaModels(vendorDictionary, productInfoDict, ignoreSkus):
    if not os.path.exists(NEW_SUBMISSION_FOLDER):
        os.makedirs(NEW_SUBMISSION_FOLDER)

    noProductInfo = []
    failedFileGeneration = []
    convertedSubmissions = 0
    for filename in os.listdir(NEW_MODELS_FOLDER):
        if '.json' not in filename:
            continue

        fileSku = ""
        fileVendor = ""
        if VENDOR_DELIMETER in filename:
            fileSku = filename.split(SKU_DELIMETER)[1].replace('.json', '')
            fileVendor = filename.split(VENDOR_DELIMETER)[1].split(SKU_DELIMETER)[0]
        else:
            print 'Unable to determine sku or vendor from file ' + filename
            continue

        # Skip certain files
        if fileSku in ignoreSkus:
            continue

        # Determine which products require a sku translation and translate them
        if fileSku in SKU_TRANSLATION_DICTIONARY:
            fileSku = SKU_TRANSLATION_DICTIONARY[fileSku]

        # If translation still fails, note it
        if fileSku not in productInfoDict:
            noProductInfo.append(fileSku)
            continue

        # Get the correct product info
        productInfo = productInfoDict[fileSku]

        # Santity check the vendor mapping and don't update if they fail
        if fileVendor not in vendorDictionary:
            print 'The vendor mapping should not exist for ' + fileVendor + ' for sku ' + fileSku
            continue
        if productInfo[ADG_VENDOR_ID] != vendorDictionary[fileVendor]:
            print 'Vendor id does not match: ' + productInfo[ADG_VENDOR_ID] + ' vs. ' + vendorDictionary[fileVendor] + ' for sku ' + fileSku
            continue

        try:
            # Load the existing JSON, edit the required fields, and generate the new file
            with open(NEW_MODELS_FOLDER + '/' + filename, 'r') as f:
                data = json.load(f)
                # Update the ADG Product ID
                data['adgProductId'] = productInfo[ADG_PRODUCT_ID]
                # Remove the id, since these are all currently wrong
                data['id'] = None
                # Update the sku if necessary
                if SKU in data:
                    data[SKU] = productInfo[ADG_SKU]

            filePrefix = NEW_SUBMISSION_FOLDER + '/' + filename.split(VENDOR_DELIMETER)[0] + '_' + productInfo[ADG_SKU]
            
            # Generate the new dynamo mappings
            generateDynamoMapping(productInfo, data, filePrefix)
            convertedSubmissions += 1
        except Exception as e:
            # If something happened with the file encoding/ decoding, print the stacktrace
            print e
            failedFileGeneration.append(fileSku)

    print 'Successfully updated the following number of files: ' + str(convertedSubmissions)
    if len(noProductInfo) > 0:
        print 'No product info for the following skus ' + str(noProductInfo)
    if len(failedFileGeneration) > 0:
        print 'Failed json file generation/ ingestion for the following skus: ' + str(failedFileGeneration)

# Helper method to generate dynamo mappings
def generateDynamoMapping(productInfo, documentData, filePrefix):
    # Dev Portal Product
    devPortalProduct = {
        'id': str(uuid.uuid4()),
        'name': '<enter_product_name>'
    }

    # Product id External Mapping
    dyanmoInfo = {
    ADG_PRODUCT_ID: productInfo[ADG_PRODUCT_ID],
    ADG_VENDOR_ID: productInfo[ADG_VENDOR_ID],
    ADG_SKU: productInfo[ADG_SKU],
    ADG_ASIN: productInfo[ASIN]}

    newFilePath = filePrefix +  '_dynamo.json'
    with io.open(newFilePath, 'w', encoding='utf8') as f:
        # Product info dump
        f.write(unicode('// Product info dump\n'))
        data = json.dumps(dyanmoInfo, indent=4, ensure_ascii=False)
        f.write(unicode(data))

        # Instructions
        f.write(unicode('\n// Backfill steps\n'))

        # Dev Portal Product
        f.write(unicode('// 1. Replace <enter_product_name> with display name and add as new entry to the prod_dev-portal-products table.\n'))
        data = json.dumps(devPortalProduct, indent=4, ensure_ascii=False)
        f.write(unicode(data))

        # Vendor ID
        f.write(unicode('\n// 2. Open https://devportal2.justin.tv/, select product on the left and select edit Game/Extension on the right. Enter ' + productInfo[ADG_VENDOR_ID] + ' as vendor code.\n'))

        # ManagedProduct Id
        f.write(unicode('// 3. On the edit product page, copy the second UUID that appears in the URL, this is the managed product id. For https://devportal2.justin.tv/products/deffb21d-cdec-4200-b776-962d2dae0764/commerce/079750ab-5e57-4ed8-8fd4-65bfd58ae17c/distribution it would be 079750ab-5e57-4ed8-8fd4-65bfd58ae17c.\n'))

        # Final step
        f.write(unicode('\n// 4. Open the prod_managed-products table. Query by managed_product_id, using the id from step #3. Record the value under the draft-document-id column. Open the prod_documents table (grab a coffee this will take a while to load), query by id, and use the draft-document-id. Click the edit button next to value in the documentData column, copy and paste the below JSON into it and save.\n'))
        data = json.dumps(documentData, indent=4, ensure_ascii=False)
        f.write(unicode(data))                

# Generate a dictionary with a mapping of masVendorId to vendor id (vendorCode)
def generateVendorIdMappingDictionary(csvFile):
    # The CSV is formatted as: masVendorId,vendorCode
    vendorDictionary = {}
    with open(csvFile) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            masVendorId = row['masVendorId']
            vendorCode = row['vendorCode']
            vendorDictionary[masVendorId] = vendorCode
    print 'Generated a mapping of masVendorId to vendor id of size ' + str(len(vendorDictionary))
    return vendorDictionary

# Generate a list with SKUs to ignore
def generateIgnoreSkuList(txtFile):
    ignoreFile = open(txtFile, "r")
    ignoreList = ignoreFile.read().split('\n')
    print 'Generate a list that will ignore the following number of skus: ' + str(len(ignoreList))
    return ignoreList

# Generate a dictionary with a mapping of SKU to ASIN
def generateSkuASINMappingDictionary(csvFile):
    # The CSV is formatted as: SKU,ASIN
    skuDictionary = {}
    duplicates = []
    with open(csvFile) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            sku = row['SKU']
            if sku in skuDictionary:
                # Delete the item if it already exists. Make a note of the error.
                del skuDictionary[sku]
                duplicates.append(row)
                continue
            skuDictionary[sku] = row['ASIN']
    print 'Generated a mapping of Sku to ASIN of size ' + str(len(skuDictionary))
    if len(duplicates) > 0:
        print 'Found the following duplicates in the sku mapping ' + str(duplicates)
    return skuDictionary

# Generate a dictionary with a given product mapping from the local ADG map CSV (instead of calling ADG to generate it)
def generateProductMappingFromLocalCSV():
    # The CSV is formatted as: sku,asin,ADG_PRODUCT_ID,ADG_DOMAIN_ID,ADG_SKU,ADG_VENDOR_ID
    productDictionary = {}
    with open(ADG_CALL_CSV) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            sku = row[SKU]
            infoDict = {}
            infoDict[ADG_PRODUCT_ID] = row[ADG_PRODUCT_ID]
            infoDict[ADG_DOMAIN_ID] = row[ADG_DOMAIN_ID]
            infoDict[ADG_SKU] = row[ADG_SKU]
            infoDict[ADG_VENDOR_ID] = row[ADG_VENDOR_ID]
            infoDict[ASIN] = row[ASIN]
            if sku in productDictionary:
                print 'Duplicate in local product mapping CSV for ' + sku
            if sku != infoDict[ADG_SKU]:
                print 'Sku name does not match: ' + sku + ' vs. ' + infoDict[ADG_SKU]
            productDictionary[sku] = infoDict
    print 'Generated a mapping of Sku to productInfo ' + str(len(productDictionary))
    return productDictionary

# Call ADG Product Service's getProduct API to determine the ADG info for each sku in the given skuDict
def obtainAsinInfoFromADG(skuDict, cookie):
    skuInfoDict = {}
    failingProducts = []
    for sku in skuDict:
        # Make the call to adg
        try:
            productInfo = getProductCall.makeProductCall(cookie, skuDict[sku])
            adgProductId = productInfo['id']
            domainId = productInfo['productDomain']['id']
            # As a sanity check, note the vendor id for future comparison
            vendorId = productInfo['vendor']['id']
            # As a sanity check, note what the actual ADG sku is
            skuName = productInfo[SKU]
            infoDict = {}
            infoDict[ADG_PRODUCT_ID] = adgProductId
            infoDict[ADG_DOMAIN_ID] = domainId
            infoDict[ADG_SKU] = skuName
            infoDict[ADG_VENDOR_ID] = vendorId
            infoDict[ASIN] = skuDict[sku]
            skuInfoDict[sku] = infoDict
            print "Valid response obtained from ADG for " + sku
        except Exception as e:
            "Exception caught when calling adg for sku " + sku + " and asin " + skuDict[sku]
            failingProducts.append(sku)
        # Sleep so we don't hit ADG with more than 1 TPS
        time.sleep(SLEEP_TIME)

        # Write the progress to a file to avoid restarting calls
        if len(skuInfoDict) % ADG_CALL_PROGRESS_CHECK == 0:
            writeADGProductInfoToCSV(skuInfoDict)

    # Write the final CSV
    writeADGProductInfoToCSV(skuInfoDict)
    print 'Successfully obtained information from ADG for the following number of products: ' + str(len(skuInfoDict))
    if len(failingProducts) > 0:
        print 'Found errors in asin products call ' + str(failingProducts)
    return skuInfoDict

# Helper method to write the ADG Product Info to a CSV file (to avoid making ADG calls in the future)
def writeADGProductInfoToCSV(skuInfoDict):
    try:
        os.remove(ADG_CALL_CSV)
    except Exception as e:
        print 'File did not exit.'
    with open(ADG_CALL_CSV, 'w') as productInfoCSV:
        fieldnames = [SKU, ASIN, ADG_PRODUCT_ID, ADG_DOMAIN_ID, ADG_SKU, ADG_VENDOR_ID]
        writer = csv.DictWriter(productInfoCSV, fieldnames=fieldnames)

        writer.writeheader()
        for sku in skuInfoDict:
            try:
                adgProductId = skuInfoDict[sku][ADG_PRODUCT_ID]
                domainId = skuInfoDict[sku][ADG_DOMAIN_ID]
                asin = skuInfoDict[sku][ASIN]
                skuName = skuInfoDict[sku][ADG_SKU]
                vendorId = skuInfoDict[sku][ADG_VENDOR_ID]
                writer.writerow({SKU: sku, ASIN: asin, ADG_PRODUCT_ID: adgProductId, ADG_DOMAIN_ID: domainId, ADG_SKU: skuName, ADG_VENDOR_ID: vendorId})
            except Exception as e:
                print "Exception caught when trying to write CSV for the following sku: " + sku

    print 'Successfully wrote CSV with ADG info'

# Helper method to determine whether an ion file should be ignored for JSON conversion
def shouldIgnoreFile(fileName):
    for ignore in IGNORE_DOCUMENTS:
        if ignore in fileName:
            return True
    return False

# Helper method to add a new value to a dictionary of arrays
def safeAddToDictionaryList(myDict, myKey, myVal):
    if myKey in myDict:
        myDict[myKey].append(myVal)
    else:
        myDict[myKey] = [myVal]

# Take a given mulligan data folder and necessary mappings to generate new tachanka ingestion information
def main(argv):
    usage = '''
    Call with -h for argument help.
    Sample call: python mulliganDocumentConverter.py data Vendor_ID_Mapping.csv Sku_Asin_Map.csv ignoreSkuList.txt True --cookie="SOME_COOKIE_HERE"
    '''
    parser = argparse.ArgumentParser(usage=usage)
    parser.add_argument('mulligan', help='''
        Folder including all mulligan ion docs.
        Example: data''')
    parser.add_argument('vendorMap', help='''
        CSV including 1:1 mapping of masVendorId to Vendor.Id used for sanity checks.
        Column titles are masVendorId,vendorCode
        Example: Vendor_ID_Mapping.csv''')
    parser.add_argument('skuAsinMap', help='''
        CSV including 1:1 mapping of Sku to ASIN.
        Column titles are SKU,ASIN
        Example: Sku_Asin_Map.csv''')
    parser.add_argument('ignoreSkus', help='''
        Txt file including 1 sku to ignore per line.
        Example: ignoreSkuList.txt''')
    parser.add_argument('callAdg', help='''
        Whether to call ADG or use the local adgProductInformation.csv document.
        Example: True or False''')
    parser.add_argument('--cookie', required=False, help='''
        Cookie for calling ADG Product Service.
        You can copy/paste this from a Coral Explorer browser request.''')
    args = parser.parse_args()

    if args.cookie == None and args.callAdg.lower() == 'true':
        print 'Please pass in a cookie when calling ADG'
        return

    # Generate the new JSON files
    productList = generateJSON(args.mulligan)

    # Convert the JSON files into Tachanka models
    successfulModels = generateTachankaModels(productList)

    # Construct necessary dictionaries/ lists
    vendorDictionary = generateVendorIdMappingDictionary(args.vendorMap)
    skuDictionary = generateSkuASINMappingDictionary(args.skuAsinMap)
    ignoreSkus = generateIgnoreSkuList(args.ignoreSkus)

    productInfoDict = {}
    if args.callAdg.lower() == 'true':
        productInfoDict = obtainAsinInfoFromADG(skuDictionary, args.cookie)
    else:
        productInfoDict = generateProductMappingFromLocalCSV()

    # Finalize the submission information
    finalizeTachankaModels(vendorDictionary, productInfoDict, ignoreSkus)

if __name__ == "__main__":
    # See https://stackoverflow.com/questions/18649512/unicodedecodeerror-ascii-codec-cant-decode-byte-0xe2-in-position-13-ordinal
    # TODO: If upgrading to Python 3, this will no longer work and isn't recommended anyways according to
    # https://stackoverflow.com/questions/3828723/why-should-we-not-use-sys-setdefaultencodingutf-8-in-a-py-script
    # However, this is currently the fix to support weird encoding/ characters for at least one title
    # (Relics of Zaron _ Stick of Truth Costumes and Perks Pack)
    reload(sys)
    sys.setdefaultencoding('utf8')
    main(sys.argv)
