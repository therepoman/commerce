package main

import (
	"code.justin.tv/commerce/tachanka/config"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

func fetchHealth() error {
	environment := config.GetCurrentEnvironment()

	fmt.Println("Pinging Health Check")
	var resp *http.Response
	var err error
	if environment == "local" {
		resp, err = http.Get("http://localhost:8010/health")
	} else {
		resp, err = http.Get("http://localhost:8000/health")
	}
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if string(body) != "OK" {
		return errors.New("Bad response")
	}
	return nil
}

func main() {
	for i := 0; i < 20; i++ {
		err := fetchHealth()
		if err == nil {
			fmt.Println("Success!")
			os.Exit(0)
		}
		fmt.Println(err)
		time.Sleep(time.Second)
	}
	os.Exit(1)
}
