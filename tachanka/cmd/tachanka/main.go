package main

import (
	"net/http"
	"os"
	"time"

	log "github.com/Sirupsen/logrus"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/corehandlers"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/defaults"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/aws/signer/v4"
	"github.com/aws/aws-sdk-go/service/sts"

	"fmt"

	"code.justin.tv/commerce/AmazonMWSGoClient/mws"
	"code.justin.tv/commerce/tachanka/api"
	"code.justin.tv/commerce/tachanka/authorization"
	adgClient "code.justin.tv/commerce/tachanka/client/adg"
	"code.justin.tv/commerce/tachanka/config"
	"code.justin.tv/commerce/tachanka/dynamo"
	"code.justin.tv/commerce/tachanka/mocks"
	"code.justin.tv/common/goauthorization"
	godynamo "code.justin.tv/common/godynamo/client"
	dynamoTesting "code.justin.tv/common/godynamo/testing"
	"code.justin.tv/common/twitchhttp"
	guregudynamo "github.com/guregu/dynamo"
	"github.com/satori/go.uuid"
	"github.com/stretchr/testify/mock"
	"golang.org/x/net/context"
)

func main() {
	environment := config.GetCurrentEnvironment()
	cfg, err := config.LoadEnvironmentConfig(environment)
	if err != nil {
		log.Fatal(err)
	}

	var decoder goauthorization.IDecoder
	if environment == "development" || environment == "local" {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}
	log.SetFormatter(&log.JSONFormatter{})

	hostname, err := os.Hostname()
	if err != nil {
		log.Fatal(err)
	}

	mwsClient := mws.AmazonMWSGoClient{}

	awsCredentials := Credentials()

	serverAPI := api.ServerAPI{
		IsProd:         environment == "production",
		Hostname:       hostname,
		MWSClient:      mwsClient,
		AWSCredentials: awsCredentials,
		Decoder:        decoder,
	}

	// Dao Configuration
	db := NewDynamoClient(cfg)

	serverAPI.SubmissionDao = dynamo.NewSubmissionDao(db, cfg)
	serverAPI.ExternalMappingDao = dynamo.NewExternalMappingDao(db, cfg)
	serverAPI.ADGSubmissionServiceClient = adgClient.NewTProxADGSubmissionServiceClient(cfg.TProxSigV4AWSRegion, getTProxSigV4Credentials(cfg))

	if environment == "local" {
		log.Info("Setting Dynamo to use local Dynamo instance")

		// Need to create dao tables for local development
		serverAPI.SubmissionDao.CreateTable()
		serverAPI.ExternalMappingDao.CreateTable()

		dynamoContext, err := dynamoTesting.NewDynamoTestContext("localhost")
		if err != nil {
			os.Exit(1)
		}

		managedProductDao := dynamo.NewManagedProductDao(dynamoContext.CreateTestClient(dynamoContext.WaitTimeout, dynamoContext.WaitTimeout, "local-managed-product"))
		dynamoContext.RegisterTestingDao(managedProductDao)
		documentDao := dynamo.NewDocumentDao(dynamoContext.CreateTestClient(dynamoContext.WaitTimeout, dynamoContext.WaitTimeout, "local-document"))
		dynamoContext.RegisterTestingDao(documentDao)
		devPortalOwnershipDao := dynamo.NewDevPortalOwnershipDao(dynamoContext.CreateTestClient(dynamoContext.WaitTimeout, dynamoContext.WaitTimeout, "local-devportal-ownership"))
		dynamoContext.RegisterTestingDao(devPortalOwnershipDao)

		// Authorization Daos
		adminDao := authorization.NewAdminDao(dynamoContext.CreateTestClient(dynamoContext.WaitTimeout, dynamoContext.WaitTimeout, "local-authorization"))
		dynamoContext.RegisterTestingDao(adminDao)
		devPortalProductsDao := authorization.NewDevPortalProductDao(dynamoContext.CreateTestClient(dynamoContext.WaitTimeout, dynamoContext.WaitTimeout, "local-dev-portal-products"))
		dynamoContext.RegisterTestingDao(devPortalProductsDao)
		serverAPI.AdminDao = adminDao
		serverAPI.DevPortalProductDao = devPortalProductsDao

		// Seed the DevPortalProductsDao with some reasonable products
		for i := 0; i < 10; i++ {
			devPortalProductsDao.Put(&authorization.DevPortalProduct{
				Id:   uuid.NewV4().String(),
				Name: fmt.Sprintf("Game %v", i),
			})
		}

		// Seed the Admins table with an admin
		adminDao.Put(&authorization.Admin{
			Tuid: "Admin",
		})

		serverAPI.ManagedProductDao = managedProductDao
		serverAPI.DocumentDao = documentDao
		serverAPI.DevPortalOwnershipDao = devPortalOwnershipDao

		log.Info("Setting goauthorization to be mocked")
		decoderMock := new(mocks.IDecoder)
		decoderMock.On("Decode", mock.AnythingOfType("context")).Return(authMocker)
		decoderMock.On("Decode", mock.Anything).Return(&goauthorization.AuthorizationToken{
			Claims: goauthorization.TokenClaims{
				Authorizations: goauthorization.CapabilityClaims{
					"edit_product": {
						"product_id": "test",
					},
				},
			},
		}, nil)
		decoderMock.On("Validate", mock.Anything, mock.Anything).Return(nil)

		serverAPI.Decoder = decoderMock

		serverConfig := twitchhttp.NewConfig()
		serverConfig.Addr = cfg.LocalHostPort
		serverConfig.DebugAddr = cfg.LocalDebugPort

		server, err := serverAPI.NewServer()
		if err != nil {
			log.Fatal(err)
		}

		log.Fatal(twitchhttp.ListenAndServe(server, serverConfig))
	} else {
		log.Info("Setting dynamo to use AWS Dynamo instance")

		dynamoClient := godynamo.NewClient(&godynamo.DynamoClientConfig{
			AwsRegion:   cfg.DynamoRegion,
			TablePrefix: cfg.DynamoTablePrefix,
		})

		documentDao := dynamo.NewDocumentDao(dynamoClient)
		managedProductDao := dynamo.NewManagedProductDao(dynamoClient)

		devPortalOwnershipDao := dynamo.NewDevPortalOwnershipDao(dynamoClient)

		// Authorization Daos
		adminDao := authorization.NewAdminDao(dynamoClient)
		devPortalProductDao := authorization.NewDevPortalProductDao(dynamoClient)
		serverAPI.AdminDao = adminDao
		serverAPI.DevPortalProductDao = devPortalProductDao

		serverAPI.ManagedProductDao = managedProductDao
		serverAPI.DocumentDao = documentDao
		serverAPI.DevPortalOwnershipDao = devPortalOwnershipDao

		log.Info("Setting goauthorization to use prod key")
		decoder, err = goauthorization.NewDecoder("ES256", "/var/app/ecc_public_key", "code.justin.tv/commerce/tachanka", "code.justin.tv/web/cartman")
		if err != nil {
			log.Fatal("error initializing decoder: ", err)
		}

		serverAPI.Decoder = decoder

		server, err := serverAPI.NewServer()
		if err != nil {
			log.Fatal(err)
		}

		log.Fatal(twitchhttp.ListenAndServe(server, nil))
	}
}

func NewDynamoClient(config *config.Configuration) *guregudynamo.DB {
	awsConfig := aws.NewConfig().
		WithRegion(config.DynamoRegion).
		WithEndpoint(config.DynamoEndpointOverride)

	return guregudynamo.New(session.New(), awsConfig)
}

func Config() *aws.Config {
	return aws.NewConfig().
		WithCredentials(credentials.AnonymousCredentials).
		WithRegion(os.Getenv("AWS_REGION")).
		WithHTTPClient(http.DefaultClient).
		WithMaxRetries(aws.UseServiceDefaultRetries).
		WithLogger(aws.NewDefaultLogger()).
		WithLogLevel(aws.LogOff).
		WithSleepDelay(time.Sleep)
}

func Handlers() request.Handlers {
	var handlers request.Handlers

	handlers.Validate.PushBackNamed(corehandlers.ValidateEndpointHandler)
	handlers.Build.PushBackNamed(corehandlers.SDKVersionUserAgentHandler)
	handlers.Sign.PushBackNamed(corehandlers.BuildContentLengthHandler)
	handlers.Send.PushBackNamed(corehandlers.SendHandler)
	handlers.AfterRetry.PushBackNamed(corehandlers.AfterRetryHandler)
	handlers.ValidateResponse.PushBackNamed(corehandlers.ValidateResponseHandler)

	return handlers
}

func Credentials() *credentials.Credentials {
	return defaults.CredChain(Config(), Handlers())
}

func authMocker(ctx context.Context) (*goauthorization.AuthorizationToken, error) {
	return &goauthorization.AuthorizationToken{
		Claims: goauthorization.TokenClaims{
			Authorizations: goauthorization.CapabilityClaims{
				"edit_product": {
					"product_id": ctx.Value("TwitchAuthorizationToken").(string),
				},
			},
		},
	}, nil
}

func getTProxSigV4Credentials(cfg *config.Configuration) *v4.Signer {
	awsConfig := &aws.Config{Region: aws.String(cfg.TProxAWSRegion)}
	stsClient := sts.New(session.New(awsConfig))

	arp := &stscreds.AssumeRoleProvider{
		Duration:     900 * time.Second,
		ExpiryWindow: 10 * time.Second,
		RoleARN:      cfg.TProxAccessRoleARN,
		Client:       stsClient,
	}

	credentials := credentials.NewCredentials(arp)

	return v4.NewSigner(credentials)
}
