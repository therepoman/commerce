package models

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"code.justin.tv/commerce/tachanka/models/adg"
	"code.justin.tv/commerce/tachanka/models/currency"
	"github.com/Sirupsen/logrus"
)

const (
	ModelType_InGameContent      ManagedProductType = "IN_GAME_CONTENT"
	DefaultIGC_CategoryInfoId    string             = "game-microtransactions-for-currency"
	DefaultIGC_FulfillmentMethod string             = "fulfilled_FuelLauncher_default"
)

type IGCLocaleListing struct {
	Locale           string   `json:"locale"`
	DisplayName      string   `json:"displayName"`
	ShortDescription string   `json:"shortDescription"`
	LongDescription  string   `json:"longDescription"`
	Keywords         []string `json:"keywords"`
	IconId           string   `json:"iconId"`
}

type InGameContent struct {
	adg.TaxInformation
	ManagedProductMetadata
	DomainIdOverride  string              `json:"domainIdOverride"`
	CategoryInfoId    string              `json:"categoryInfoId"`
	DefaultPrice      currency.Amount     `json:"defaultPrice"`
	AvailabilityInfo  []AvailabilityEntry `json:"availabilityInfo"`
	ReleaseDate       string              `json:"releaseDate"`
	LocaleListings    []IGCLocaleListing  `json:"localeListings"`
	FulfillmentMethod string              `json:"fulfillmentMethod"`
	SKU               string              `json:"sku"`
}

func (igc *InGameContent) GetType() ManagedProductType {
	return ModelType_InGameContent
}

func (igc *InGameContent) GetFulfillmentMethod() string {
	return igc.FulfillmentMethod
}

func (igc *InGameContent) GetSKU() string {
	return igc.SKU
}

func (igc *InGameContent) GetTaxInformation() adg.TaxInformation {
	return igc.TaxInformation
}

func (igc *InGameContent) GetTitle() string {
	for _, locale := range igc.LocaleListings {
		if locale.Locale == DefaultLocale {
			return locale.DisplayName
		}
	}
	return ""
}

func (igc *InGameContent) SetData(newData string) error {
	err := json.Unmarshal([]byte(newData), &igc)
	if err != nil {
		return err
	}
	return nil
}

func (igc *InGameContent) GetData() (string, error) {
	igcData, err := json.Marshal(igc)
	if err != nil {
		return "", err
	}
	return string(igcData), nil
}

func (igc *InGameContent) PopulateADGSubmission(submission adg.ADGSubmission) (*adg.ADGSubmission, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":        "InGameContent.PopulateADGSubmission",
		"MangedProductId": igc.Id,
	})
	log.Debug("Entering")

	// Product Info
	productInfo := &submission.Document.ProductInfo
	productInfo.CategoryId = igc.CategoryInfoId
	productInfo.ItemBadgeDetails = []string{igc.FulfillmentMethod}

	if len(igc.DomainIdOverride) > 0 {
		log.WithField("DomainIdOverride", igc.DomainIdOverride).Debug("Overriding Domain Id for submission")
		productInfo.DomainId = igc.DomainIdOverride
	}

	// Listings - IGC listings must be a subset of parent game listings.
	parentGameListings := submission.Document.Listings
	mergedListings := make(map[string]adg.Listing)

	for _, listing := range igc.LocaleListings {
		parentListing, exists := parentGameListings[listing.Locale]

		if !exists {
			log.WithField("Parent Locale Listings", parentGameListings).Error("No parent listing for: " + listing.Locale)
			return nil, errors.New(fmt.Sprintf("IGC %s cannot have listing in locale %s without a corresponding parent game listing", igc.Id, listing.Locale))
		}

		parentListing.DisplayTitle = listing.DisplayName
		parentListing.ShortDescription = listing.ShortDescription
		parentListing.LongDescription = listing.LongDescription
		parentListing.Keywords = listing.Keywords
		parentListing.BoxShotAssetId = listing.IconId // Yeah this is confusing but IGC use boxshot id for the icon id.

		mergedListings[listing.Locale] = parentListing
	}
	submission.Document.Listings = mergedListings

	// Offerings and Version For Country
	var offerings []adg.Offering
	var versionForCountry []string

	for _, country := range igc.AvailabilityInfo {
		if country.Available {
			versionForCountry = append(versionForCountry, country.CountryCode)
			offerings = append(offerings, adg.Offering{
				AnnounceDate:        igc.ReleaseDate,
				ReleaseDate:         igc.ReleaseDate,
				PreviousReleaseDate: igc.ReleaseDate,
				PublicationDate:     igc.ReleaseDate,
				Countries:           []string{country.CountryCode},
				Pricing: map[string]adg.ADGPrice{
					country.CountryCode: adg.ADGPrice{
						Currency: adg.DefaultCurrencyCode,
						Price:    float64(country.Price),
					}},
			})
		}
	}

	submission.Document.Offerings = offerings
	submission.Document.ProductInfo.VersionForCountry = versionForCountry

	log.WithField("Submission", submission).Debug("Exiting")

	return &submission, nil
}

func (igc *InGameContent) PopulateADGDocument(partialDoc adg.ADGDocument) adg.ADGDocument {

	log := logrus.WithFields(logrus.Fields{
		"Function": "PopulateADGDocument",
	})

	partialDoc.ReleaseDate = igc.ReleaseDate
	partialDoc.DefaultLocale = DefaultLocale
	partialDoc.FulfillmentMethod = igc.FulfillmentMethod
	partialDoc.CategoryInfo.CategoryId = igc.CategoryInfoId
	partialDoc.ListingsByLocale = getLocaleListings(*igc, partialDoc)

	var availabilityInfo adg.AvailabilityInfo
	pricesByCountry := make(map[string]adg.Price)

	availableInParent := make(map[string]bool) // map to store all countries where parent item is available in
	for _, parentCountry := range partialDoc.AvailabilityInfo.AvailableCountries {
		availableInParent[parentCountry] = true
	}

	// loop through the ones set in igc
	for _, country := range igc.AvailabilityInfo {
		if country.Available {
			if !availableInParent[country.CountryCode] {
				log.Warn(fmt.Sprintf("IGC %s cannot be available in country %s where parent is not available", igc.Id, country.CountryCode))
				continue
			}

			// if this is also available in parent, set it in the doc
			availabilityInfo.AvailableCountries = append(availabilityInfo.AvailableCountries, country.CountryCode)
			price := adg.Price{
				CurrencyCode: adg.DefaultCurrencyCode,
				Amount:       country.Price,
			}
			pricesByCountry[country.CountryCode] = price
		}
	}

	partialDoc.AvailabilityInfo = availabilityInfo // Overwrite since we will use availability info only from the igc

	partialDoc.PricingInfo.PricesByMarketplaceAndCountry = make(map[string]adg.MarketplacePrices) // Overwrite since we will use price info only from the igc
	partialDoc.PricingInfo.PricesByMarketplaceAndCountry[adg.DefaultMarketplace] = adg.MarketplacePrices{
		DefaultListPrice: adg.Price{
			CurrencyCode: adg.DefaultCurrencyCode,
			Amount:       igc.DefaultPrice,
		},
		PricesByCountry: pricesByCountry,
	}
	return partialDoc
}

func getLocaleListings(igc InGameContent, partialDoc adg.ADGDocument) map[string]adg.LocaleListing {
	mergedListings := make(map[string]adg.LocaleListing) //Create a new listing
	for _, listing := range igc.LocaleListings {
		localeListing, present := partialDoc.ListingsByLocale[listing.Locale]

		// If the parent game didn't have this listing, then set some sane defaults
		if !present {
			localeListing = adg.LocaleListing{}
			localeListing.ProductFeatures = []string{}
			localeListing.ScreenshotIds = adg.DefaultScreenshotIds
			localeListing.PageBackgroundId = adg.DefaultPageBackgroundId
			localeListing.VideoIds = adg.DefaultVideoIds
			localeListing.ReleaseNotesId = adg.DefaultReleaseNotesId
			localeListing.BoxshotId = adg.DefaultBoxshotId
		}

		localeListing.DisplayName = listing.DisplayName
		localeListing.ShortDescription = listing.ShortDescription
		localeListing.LongDescription = listing.LongDescription
		localeListing.Keywords = append(localeListing.Keywords, listing.Keywords...)
		localeListing.IconId = listing.IconId

		mergedListings[listing.Locale] = localeListing
	}
	return mergedListings
}

func (igc InGameContent) GetADGMetadata(fulfilledByTwitch bool) adg.Metadata {
	if igc.InGameCurrency {
		return igc.makeVirtualCurrencyMetadata(fulfilledByTwitch)
	} else if igc.MultiplePurchases {
		return igc.makeIGCConsumableMetadata(fulfilledByTwitch)
	} else {
		return igc.makeIGCEntitlementMetadata(fulfilledByTwitch)
	}
}

func (igc InGameContent) makeVirtualCurrencyMetadata(fulfilledByTwitch bool) adg.Metadata {
	var gameMetadata adg.Metadata
	gameMetadata.ContentType = adg.ContentType_VirtualCurrency
	gameMetadata.ProductLine = adg.ProductLine_VirtualCurrency
	gameMetadata.ProductCategory = adg.ProductCategory_4000
	if igc.MultiplePurchases {
		gameMetadata.ProductType = adg.ProductType_Consumable
	} else {
		gameMetadata.ProductType = adg.ProductType_Entitlement
	}
	gameMetadata.Subcategory = calcualateSubcategory(gameMetadata.ProductCategory, fulfilledByTwitch)
	return gameMetadata
}

func (igc InGameContent) makeIGCConsumableMetadata(fulfilledByTwitch bool) adg.Metadata {
	var gameMetadata adg.Metadata
	gameMetadata.ContentType = adg.ContentType_Consumable
	gameMetadata.ProductLine = adg.ProductLine_Consumable
	gameMetadata.ProductType = adg.ProductType_Consumable
	if igc.Durable {
		gameMetadata.ProductCategory = adg.ProductCategory_3000
	} else {
		gameMetadata.ProductCategory = adg.ProductCategory_2000
	}
	gameMetadata.Subcategory = calcualateSubcategory(gameMetadata.ProductCategory, fulfilledByTwitch)
	return gameMetadata
}

func (igc InGameContent) makeIGCEntitlementMetadata(fulfilledByTwitch bool) adg.Metadata {
	var gameMetadata adg.Metadata
	gameMetadata.ContentType = adg.ContentType_Entitlement
	gameMetadata.ProductLine = adg.ProductLine_Entitlement
	gameMetadata.ProductType = adg.ProductType_Entitlement
	if igc.Durable {
		gameMetadata.ProductCategory = adg.ProductCategory_3000
	} else {
		gameMetadata.ProductCategory = adg.ProductCategory_2000
	}
	gameMetadata.Subcategory = calcualateSubcategory(gameMetadata.ProductCategory, fulfilledByTwitch)
	return gameMetadata
}

func calcualateSubcategory(mainCategory adg.ProductCategory, fulfilledByTwitch bool) adg.Subcategory {
	if fulfilledByTwitch {
		return adg.Subcategory(mainCategory) + adg.Subcategory_FulfilledByTwitch
	} else {
		return adg.Subcategory(mainCategory) + adg.Subcategory_FulfilledBy3P
	}
}

func NewInGameContent(metadata ManagedProductMetadata) *InGameContent {
	defaultLocale := IGCLocaleListing{
		Locale:   "en-US",
		Keywords: make([]string, 0),
	}

	sellableCountries := adg.GetAllSellableCountries()
	availabilityInfo := make([]AvailabilityEntry, len(sellableCountries))
	for i, countryCode := range sellableCountries {
		availabilityInfo[i] = AvailabilityEntry{
			Available:   false,
			CountryCode: countryCode,
			Price:       0,
		}
	}

	// Default to same time yesterday
	defaultTime := time.Now().AddDate(0, 0, -1).UTC().Format(time.RFC3339)

	return &InGameContent{
		ManagedProductMetadata: metadata,
		LocaleListings:         []IGCLocaleListing{defaultLocale},
		AvailabilityInfo:       availabilityInfo,
		CategoryInfoId:         DefaultIGC_CategoryInfoId,
		ReleaseDate:            defaultTime,
		FulfillmentMethod:      DefaultIGC_FulfillmentMethod,
	}
}
