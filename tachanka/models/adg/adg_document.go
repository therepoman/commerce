package adg

import "code.justin.tv/commerce/tachanka/models/currency"

// Built off schema_game_release.json and schema_game_release_1.json. Fields may not be in optimal order.
//ADGDocument is a struct representing a document to be submitted to ADG.
type (
	ADGDocument struct {
		FulfillmentMethod    string                   `json:"fulfillmentMethod"` // optimize to enum ?
		BinaryInfo           BinaryInfo               `json:"binaryInfo"`
		CategoryInfo         CategoryInfo             `json:"categoryInfo"`
		DeveloperInfo        DeveloperInfo            `json:"developerInfo"`
		ContentRatingInfo    ContentRatingInfo        `json:"contentRatingInfo"`
		PricingInfo          PricingInfo              `json:"pricingInfo"`
		AvailabilityInfo     AvailabilityInfo         `json:"availabilityInfo"`
		ReleaseDate          string                   `json:"releaseDate"`
		AnnounceDate         string                   `json:"announceDate"`
		PreviousReleaseDate  string                   `json:"previousReleaseDate"`
		DeveloperWebsiteUrl  string                   `json:"developerWebsiteUrl"`
		BroadcasterVodUrls   []string                 `json:"broadcasterVodUrls"`
		NewsAndReviewsUrls   []string                 `json:"newsAndReviewsUrls"`
		CustomerSupportUrl   string                   `json:"customerSupportUrl"`
		CustomerSupportEmail string                   `json:"customerSupportEmail"`
		DefaultLocale        string                   `json:"defaultLocale"`              // Locale enum ?
		ListingsByLocale     map[string]LocaleListing `json:"listingsByLocale"`           //optimize to use locale enum ?
		CasePackQuantity     int                      `json:"casePackQuantity,omitempty"` // required for IGC/IAP ¯\_(ツ)_/¯
	}

	LocaleListing struct {
		DisplayName                   string   `json:"displayName"`
		ShortDescription              string   `json:"shortDescription"`
		LongDescription               string   `json:"longDescription"`
		ProductFeatures               []string `json:"productFeatures"`
		Keywords                      []string `json:"keywords"`
		GameEdition                   string   `json:"gameEdition"`
		GameEditionDetails            string   `json:"gameEditionDetails"`
		IconId                        string   `json:"iconId"`
		ScreenshotIds                 []string `json:"screenshotIds"`
		ThankYouPageBackgroundImageId string   `json:"thankYouPageBackgroundImageId"`
		VideoIds                      []string `json:"videoIds"`
		BoxshotId                     string   `json:"boxshotId"`
		PageBackgroundId              string   `json:"pageBackgroundId"`
		GameEditionBadgeId            string   `json:"gameEditionBadgeId"`
		EulaUrl                       string   `json:"eulaUrl"`
		ReleaseNotesId                string   `json:"releaseNotesId"`
	}

	AvailabilityInfo struct {
		AvailableCountries []string `json:"availableCountries"` // use country codes ?
	}

	PricingInfo struct {
		PricesByMarketplaceAndCountry map[string]MarketplacePrices `json:"pricesByMarketplaceAndCountry"` // use enum for marketplaces ?
	}

	MarketplacePrices struct {
		DefaultListPrice Price            `json:"defaultListPrice"`
		PricesByCountry  map[string]Price `json:"pricesByCountry"` // could optimize this to use country code enum rather than strings ?
	}

	Price struct {
		CurrencyCode string          `json:"currencyCode"` // use enum ?
		Amount       currency.Amount `json:"amount"`
	}

	ContentRatingInfo struct {
		EsrbRatingInfo EsrbRatingInfo `json:"esrbRatingInfo"`
		EsrbRating     string         `json:"esrbRating"`
		PegiRating     string         `json:"pegiRating"` // use enum ?
		UskRating      string         `json:"uskRating"`  // use enum ?
		AmazonRating   string         `json:"amazonRating"`
	}

	EsrbRatingInfo struct {
		RatingCategory     string   `json:"ratingCategory"` // use enum ?
		ContentDescriptors []string `json:"contentDescriptors"`
	}

	DeveloperInfo struct {
		DeveloperName string `json:"developerName"`
	}

	CategoryInfo struct {
		CategoryId                 string   `json:"categoryId"`
		RefinementIds              []string `json:"refinementIds"`
		SuggestedCategoryHierarchy []string `json:"suggestedCategoryHierarchy"`
	}

	BinaryInfo struct {
		PCRequirementInfo              PCRequirementsInfo `json:"pcRequirementsInfo"`
		RequiresContinuousConnectivity bool               `json:"requiresContinuousConnectivity"`
		NumberOfPlayersSupported       string             `json:"numberOfPlayersSupported"`
		LanguageSupport                LanguageSupport    `json:"languageSupport"`
		Version                        string             `json:"version"`
		SupportedControllers           string             `json:"supportedControllers"`
	}

	LanguageSupport struct {
		Instructions         []string `json:"instructions"` // use an enum to specific languages ?
		SubtitlesAndCaptions []string `json:"subtitlesAndCaptions"`
		Audio                []string `json:"audio"`
	}

	PCRequirementsInfo struct {
		MinimumRequirements     RequirementsInfo `json:"minimumRequirements"`
		RecommendedRequirements RequirementsInfo `json:"recommendedRequirements,omitempty"`
		OtherRequirements       string           `json:"otherRequirements"`
	}

	RequirementsInfo struct {
		VideoCard      string `json:"videoCard"`
		RAM            string `json:"ram"`
		HardDriveSpace string `json:"hardDriveSpace"`
		Processor      string `json:"processor"`
		OS             string `json:"os"`
		DirectX        string `json:"directX"`
	}

	baseDocumentType int
)

const (
	baseTypeGame         baseDocumentType = iota
	baseTypeIGC          baseDocumentType = iota
	baseTypeIAP          baseDocumentType = iota
	baseTypeExtension    baseDocumentType = iota
	baseTypeExtensionIAP baseDocumentType = iota

	// Default values for elements where there is no clear default value.
	DefaultBoolean bool   = false
	DefaultString  string = "default_empty_string"

	// Values pulled from validExtensionKappaPetGameReleaseBaseTest
	DefaultExtensionFulfillmentMethod    string          = "fulfilled_FuelLauncher_default"
	DefaultIGCFulfillmentMethod          string          = "fulfilled_FuelLauncher_default"
	DefaultExtensionIAPFulfillmentMethod string          = DefaultExtensionFulfillmentMethod
	DefaultCurrencyCode                  string          = "USD"
	DefaultPriceAmount                   currency.Amount = 0

	DefaultMarketplace string = "amazon.com"

	DefaultEsrbRating   string = "rating_pending"
	DefaultPegiRating   string = "unknown"
	DefaultAmazonRating string = "rating_pending"

	DefaultCategoryId             string = "twitch_fuel_action_game"
	DefaultIGCCategoryId          string = "twitch_IGC"
	DefaultExtensionCategoryId    string = "twitch_extension"
	DefaultExtensionIAPCategoryId string = "game-microtransactions-for-loot-item-bundles-and-collectibles"

	DefaultVideoCard                             string = "N/A"
	DefaultRAM                                   string = "N/A"
	DefaultHardDriveSpace                        string = "N/A"
	DefaultProcessor                             string = "N/A"
	DefaultOS                                    string = "N/A"
	DefaultDirectX                               string = "N/A"
	DefaultOtherRequirements                     string = "None"
	DefaultRequiresContinuousConnectivity        bool   = true
	DefaultNumberOfPlayersSupported              string = "6"
	DefaultExtensionsIAPNumberOfPlayersSupported string = "MMO"
	DefaultBinaryVersion                         string = "1.0.1"
	DefaultExtensionsIAPBinaryVersion            string = "null"
	DefaultSupportedControllers                  string = "Keyboard, Mouse"

	DefaultReleaseDate         string = "2016-09-14T07:00:00Z"
	DefaultAnnounceDate        string = "2016-09-30T07:00:00Z"
	DefaultPreviousReleaseDate string = "2016-08-01T07:00:00Z"

	DefaultLocaleValue                   string = "en-US"
	DefaultBroadCasterVodUrl             string = "https://www.twitch.tv/videos/160935826"
	DefaultNewsAndReviewsUrl             string = "https://www.twitch.tv/"
	DefaultIconId                        string = "amzn1.dex.asset.f0d4f9e62c964c0b92108c453fa70e17"
	DefaultReleaseNotesId                string = "amzn1.dex.asset.5b72f947d259434d89dbce4906411e79"
	DefaultGameEditionBadgeId            string = "amzn1.dex.asset.5b72f947d259434d89dbce4906411e79"
	DefaultBoxshotId                     string = "amzn1.dex.asset.5b72f947d259434d89dbce4906411e79"
	DefaultPageBackgroundId              string = "amzn1.dex.asset.5b72f947d259434d89dbce4906411e79"
	DefaultThankYouPageBackgroundImageId string = "amzn1.dex.asset.5b72f947d259434d89dbce4906411e79"

	DefaultCasePackQuantity int = 1
)

var (
	// Values pulled from validExtensionKappaPetGameReleaseBaseTest
	// These can't be constants as arrays are not immutable types.

	DefaultAvailableCountries []string = []string{"AE", "AZ", "AX", "AL", "AS", "AD", "AO", "AI", "AQ", "AG", "AR", "AM", "AW", "AU", "AT", "BD", "BF", "BH", "BN", "BS", "BB", "BY", "BE", "BZ", "BJ", "BM", "BT", "BO", "BA", "BW", "BV", "BR", "IO", "VG", "BG", "BI", "KH", "CM", "CA", "CV", "KY", "CF", "CL", "CX", "CC", "CO", "CG", "CD", "CK", "CR", "CI", "HR", "CY", "CZ", "DE", "DJ", "DZ", "DK", "DM", "DO", "EC", "SV", "GQ", "EG", "ER", "EE", "ET", "FK", "FO", "FJ", "FI", "FR", "TF", "GM", "GN", "GA", "GE", "GH", "GI", "GR", "GL", "GD", "GU", "GT", "GG", "GW", "GY", "HT", "HM", "HN", "HK", "HU", "ID", "IS", "IN", "IE", "IM", "IL", "IT", "JO", "JM", "JP", "JE", "KG", "KW", "KZ", "KE", "KI", "LB", "LA", "LV", "LS", "LR", "LI", "LT", "LU", "MO", "MK", "MG", "MW", "MT", "MP", "MH", "MU", "MX", "FM", "MA", "ML", "MR", "MV", "MY", "MD", "MC", "MN", "ME", "MS", "MZ", "MM", "NA", "NR", "NP", "NL", "AN", "NE", "NG", "NZ", "NI", "NU", "NF", "NO", "OM", "PK", "PW", "PA", "PG", "PY", "PE", "PH", "PN", "PL", "PT", "PR", "QA", "RO", "RU", "RW", "SH", "KN", "LC", "VC", "WS", "SM", "ST", "RS", "SA", "SG", "SL", "SN", "SO", "SC", "SK", "SI", "SB", "ZA", "GS", "KR", "ES", "LK", "BL", "MF", "SR", "SJ", "SZ", "SE", "CH", "TD", "TJ", "TM", "TN", "TR", "TW", "TZ", "TH", "TL", "TG", "TK", "TO", "TT", "TC", "TV", "UG", "UA", "GB", "US", "UM", "UY", "UZ", "VI", "VU", "VA", "VE", "VN", "YE", "EH", "ZM", "ZW"}

	DefaultPricingCountries []string = DefaultAvailableCountries

	DefaultEsrbRatingDescriptors []string = []string{}

	DefaultIGCRefinementIds                  []string = []string{"twitch_fuel_igc"}
	DefaultExtensionRefinementIds            []string = []string{"twitch_extension"}
	DefaultExtensionIAPRefinementIds         []string = []string{"game-microtransactions-for-random-multi-item-packs"}
	DefaultSuggestedCategoryHierarchy        []string = []string{"Animals", "Puppies"}
	DefaultInstructions                      []string = []string{"english"}
	DefaultSubtitlesAndCaptions              []string = []string{"none"}
	DefaultExtensionsIAPSubtitlesAndCaptions []string = []string{"english"}
	DefaultAudio                             []string = []string{"english"}
	DefaultVideoIds                          []string = []string{"amzn1.dex.asset.c07caf06f33946ad86201427e1ff024e"}
	DefaultProductFeatures                   []string = []string{"amazing", "awesome"}
	DefaultScreenshotIds                     []string = []string{"amzn1.dex.asset.5b72f947d259434d89dbce4906411e79"}

	DefaultMarketplaces       []string = []string{DefaultMarketplace}
	DefaultBroadCasterVodUrls []string = []string{DefaultBroadCasterVodUrl}
	DefaultNewsAndReviewsUrls []string = []string{DefaultNewsAndReviewsUrl}
)

func newDefaultRequirementsInfo() RequirementsInfo {
	return RequirementsInfo{
		VideoCard:      DefaultVideoCard,
		RAM:            DefaultRAM,
		HardDriveSpace: DefaultHardDriveSpace,
		Processor:      DefaultProcessor,
		OS:             DefaultOS,
		DirectX:        DefaultDirectX,
	}
}

func newDefaultPCRequirementsInfo() PCRequirementsInfo {
	return PCRequirementsInfo{
		MinimumRequirements:     newDefaultRequirementsInfo(),
		RecommendedRequirements: newDefaultRequirementsInfo(),
		OtherRequirements:       DefaultOtherRequirements,
	}
}

func newDefaultLanguageSupport(docType baseDocumentType) LanguageSupport {
	if docType == baseTypeExtensionIAP {
		return LanguageSupport{
			Instructions:         DefaultInstructions,
			SubtitlesAndCaptions: DefaultExtensionsIAPSubtitlesAndCaptions,
			Audio:                DefaultAudio,
		}
	} else {
		return LanguageSupport{
			Instructions:         DefaultInstructions,
			SubtitlesAndCaptions: DefaultSubtitlesAndCaptions,
			Audio:                DefaultAudio,
		}
	}

}

func newDefaultAvailabilityInfo() AvailabilityInfo {
	var info AvailabilityInfo
	info.AvailableCountries = DefaultAvailableCountries
	return info
}

func newDefaultEsrbRatingInfo() EsrbRatingInfo {
	return EsrbRatingInfo{
		RatingCategory:     DefaultEsrbRating,
		ContentDescriptors: DefaultEsrbRatingDescriptors,
	}
}

func newDefaultContentRatingInfo(docType baseDocumentType) ContentRatingInfo {
	return ContentRatingInfo{
		EsrbRatingInfo: newDefaultEsrbRatingInfo(),
		EsrbRating:     DefaultEsrbRating,
		PegiRating:     DefaultPegiRating,
		AmazonRating:   DefaultAmazonRating,
	}
}

func newDefaultPrice() Price {
	return Price{DefaultCurrencyCode, DefaultPriceAmount}
}

func newDefaultPricesByCountry() map[string]Price {
	prices := make(map[string]Price)

	for _, countryCode := range DefaultPricingCountries {
		prices[countryCode] = newDefaultPrice()
	}

	return prices
}

func newDefaultMarketplacePrices() MarketplacePrices {
	return MarketplacePrices{
		DefaultListPrice: newDefaultPrice(),
		PricesByCountry:  newDefaultPricesByCountry(),
	}
}

func newDefaultPricingInfo() PricingInfo {
	pricesByMarketplace := make(map[string]MarketplacePrices)

	for _, marketplace := range DefaultMarketplaces {
		pricesByMarketplace[marketplace] = newDefaultMarketplacePrices()
	}

	return PricingInfo{pricesByMarketplace}
}

func newDefaultCategoryInfo(docType baseDocumentType) CategoryInfo {
	var categoryId string = DefaultCategoryId
	var refinementIds []string = []string{defaultGameRefinementId, defaultGameRefinementId}
	var hiearchy []string = DefaultSuggestedCategoryHierarchy

	switch docType {
	case baseTypeIGC:
		categoryId = DefaultIGCCategoryId
		refinementIds = DefaultIGCRefinementIds
	case baseTypeExtension:
		categoryId = DefaultExtensionCategoryId
		refinementIds = DefaultExtensionRefinementIds
	case baseTypeExtensionIAP:
		categoryId = DefaultExtensionIAPCategoryId
		refinementIds = DefaultExtensionIAPRefinementIds
	}

	return CategoryInfo{
		CategoryId:                 categoryId,
		RefinementIds:              refinementIds,
		SuggestedCategoryHierarchy: hiearchy,
	}
}

func newDefaultBinaryInfo(docType baseDocumentType) BinaryInfo {
	playersSupported := DefaultNumberOfPlayersSupported
	version := DefaultBinaryVersion

	if docType == baseTypeExtensionIAP {
		playersSupported = DefaultExtensionsIAPNumberOfPlayersSupported
		version = DefaultExtensionsIAPBinaryVersion
	}

	return BinaryInfo{
		PCRequirementInfo:              newDefaultPCRequirementsInfo(),
		RequiresContinuousConnectivity: DefaultRequiresContinuousConnectivity,
		NumberOfPlayersSupported:       playersSupported,
		LanguageSupport:                newDefaultLanguageSupport(docType),
		Version:                        version,
		SupportedControllers:           DefaultSupportedControllers,
	}
}

//NewBaseExtensionDocument returns a new extension document with fields populated with default values as
//necessary.
//The fields it will not populate are :
// - SKU
// - DeveloperName (DeveloperInfo object)
// - DeveloperWebsiteUrl
// - CustomerSupportUrl
// = CustomerSupportEmail
// - MultiplePurchasesAllowed
// - IsInGameCurrency
// - IsFixedTimeline
// - ListingsByLocale
//These fields will maintain the default go value for the corresponding type. AKA "" for string, false for bools, etc...
func NewBaseExtensionDocument() ADGDocument {
	extension := NewADGDocument()
	extension.FulfillmentMethod = DefaultExtensionFulfillmentMethod
	extension.BinaryInfo = newDefaultBinaryInfo(baseTypeExtension)
	extension.CategoryInfo = newDefaultCategoryInfo(baseTypeExtension)
	extension.ContentRatingInfo = newDefaultContentRatingInfo(baseTypeExtension)
	extension.PricingInfo = newDefaultPricingInfo()
	extension.AvailabilityInfo = newDefaultAvailabilityInfo()
	extension.ReleaseDate = DefaultReleaseDate
	extension.AnnounceDate = DefaultAnnounceDate
	extension.PreviousReleaseDate = DefaultPreviousReleaseDate
	extension.BroadcasterVodUrls = DefaultBroadCasterVodUrls
	extension.NewsAndReviewsUrls = DefaultNewsAndReviewsUrls
	extension.DefaultLocale = DefaultLocaleValue

	return extension
}

//NewBaseExtensionIAPDocument returns a new extension IAP document with fields populated with default values as
//necessary.
//The fields it will not populate are :
// - SKU
// - DeveloperName (DeveloperInfo object)
// - DeveloperWebsiteUrl
// - CustomerSupportUrl
// = CustomerSupportEmail
// - MultiplePurchasesAllowed
// - IsInGameCurrency
// - IsFixedTimeline
// - ListingsByLocale
// - PricingInfo
// - AvailabilityInfo
//These fields will maintain the default go value for the corresponding type. AKA "" for string, false for bools, etc...
func NewBaseExtensionIAPDocument() ADGDocument {
	extensionIAP := NewADGDocument()
	extensionIAP.FulfillmentMethod = DefaultExtensionIAPFulfillmentMethod
	extensionIAP.BinaryInfo = newDefaultBinaryInfo(baseTypeExtensionIAP)
	extensionIAP.CategoryInfo = newDefaultCategoryInfo(baseTypeExtensionIAP)
	extensionIAP.ContentRatingInfo = newDefaultContentRatingInfo(baseTypeExtensionIAP)
	extensionIAP.ReleaseDate = DefaultReleaseDate
	extensionIAP.AnnounceDate = DefaultAnnounceDate
	extensionIAP.PreviousReleaseDate = DefaultPreviousReleaseDate
	extensionIAP.BroadcasterVodUrls = DefaultBroadCasterVodUrls
	extensionIAP.NewsAndReviewsUrls = DefaultNewsAndReviewsUrls
	extensionIAP.DefaultLocale = DefaultLocaleValue
	extensionIAP.CasePackQuantity = DefaultCasePackQuantity

	return extensionIAP
}

// This is where I would comment my code if I was as cool as yannick
func NewBaseGameDocument() ADGDocument {
	game := NewADGDocument()
	game.CategoryInfo.RefinementIds = []string{defaultGameRefinementId, defaultGameRefinementId}
	game.CategoryInfo.SuggestedCategoryHierarchy = DefaultSuggestedCategoryHierarchy
	return game
}

//NewBaseInGameContentDocument returns a new In Game Content document with fields populated with default values
func NewBaseIGCDocument() ADGDocument {
	igc := NewADGDocument()
	igc.FulfillmentMethod = DefaultIGCFulfillmentMethod
	igc.BinaryInfo = newDefaultBinaryInfo(baseTypeIGC)
	igc.CategoryInfo = newDefaultCategoryInfo(baseTypeIGC)
	igc.ContentRatingInfo = newDefaultContentRatingInfo(baseTypeIGC)
	igc.ReleaseDate = DefaultReleaseDate
	igc.AnnounceDate = DefaultAnnounceDate
	igc.PreviousReleaseDate = DefaultPreviousReleaseDate
	igc.BroadcasterVodUrls = DefaultBroadCasterVodUrls
	igc.NewsAndReviewsUrls = DefaultNewsAndReviewsUrls
	igc.DefaultLocale = DefaultLocaleValue
	igc.CasePackQuantity = DefaultCasePackQuantity

	return igc
}

// This is necessary to prevent panics when downstream users of the docuent attempt to fetch or set elements of this array without ensuring
// that it was properly initialized.
func NewADGDocument() ADGDocument {
	var doc ADGDocument
	doc.PricingInfo.PricesByMarketplaceAndCountry = make(map[string]MarketplacePrices)
	doc.ListingsByLocale = make(map[string]LocaleListing)
	return doc
}
