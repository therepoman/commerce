package adg

import "strconv"

type ContentType string
type ProductLine string
type ProductCategory int
type Subcategory int
type ProductType string

type Metadata struct {
	ContentType     ContentType
	ProductLine     ProductLine
	ProductCategory ProductCategory
	Subcategory     Subcategory
	ProductType     ProductType
}

const (
	ContentType_Game            ContentType = "game"
	ContentType_VirtualCurrency ContentType = "virtual_currency"
	ContentType_Consumable      ContentType = "consumable"
	ContentType_Entitlement     ContentType = "entitlement"
	ContentType_Extension       ContentType = "extension"

	ProductLine_Game            ProductLine = "Twitch:FuelGame"
	ProductLine_VirtualCurrency ProductLine = "Twitch:FuelVirtualCurrency"
	ProductLine_Consumable      ProductLine = "Twitch:FuelConsumable"
	ProductLine_Entitlement     ProductLine = "Twitch:FuelEntitlement"

	ProductCategory_1000 = 63401000
	ProductCategory_2000 = 63402000
	ProductCategory_3000 = 63403000
	ProductCategory_4000 = 63404000

	ProductCategoryExt_10000 = 63410000
	ProductCategoryExt_10100 = 63410100
	ProductCategoryExt_10200 = 63410200
	ProductCategoryExt_10300 = 63410300
	ProductCategoryExt_10400 = 63410400
	ProductCategoryExt_10500 = 63410500
	ProductCategoryExt_10600 = 63410600

	Subcategory_FulfilledByTwitch = 100
	Subcategory_FulfilledBy3P     = 200
	Subcategory_Extensions        = 1

	ProductType_Entitlement = "Entitlement"
	ProductType_Consumable  = "Consumable"
)

func (ct ContentType) ToString() string {
	return string(ct)
}

func (pl ProductLine) ToString() string {
	return string(pl)
}

func (pc ProductCategory) ToString() string {
	return strconv.Itoa(int(pc))
}

func (sc Subcategory) ToString() string {
	return strconv.Itoa(int(sc))
}

func (pt ProductType) ToString() string {
	return string(pt)
}
