package adg

import (
	"bytes"
	"encoding/json"
	"errors"
	"reflect"

	"github.com/Sirupsen/logrus"
)

const (
	tenantId = "5bd4863c-8083-453e-b832-e01b902d970f"

	defaultOfferingMarketplace        = "amazon.com"
	defaultGameRefinementId           = "twitch_fuel_action_game"
	defaultIGCRefinementId            = "twitch_fuel_igc"
	defaultIAPRefinementId            = "game-microtransactions-for-random-multi-item-packs"
	defaultExtensionIAPCategoryId     = "game-microtransactions-for-loot-item-bundles-and-collectibles"
	defaultExtensionCategoryId        = "twitch_extension"
	defaultExtensionRefinementId      = "twitch_extension"
	defaultExtensionFulfillmentMethod = "fulfilled_FuelLauncher_default"
	defaultNumberOfPlayersSupported   = "6"
	defaultGameVersion                = "1"
	defaultPCRequirement              = "N/A"
	defaultESRBRating                 = "rating_pending"
	defaultPEGIRating                 = "unknown"
	defaultUSKRating                  = "unknown"
	defaultAmazonRating               = "rating_pending"
	defaultSupportedControllers       = "Keyboard, Mouse"
	defaultRevisionNumber             = 1
	defaultVideoUrl                   = "https://www.twitch.tv/videos/179721048"

	DefaultExtensionReleaseDate = "2016-09-14T07:00:00Z"

	ItemBadgeDetails_InGameCurrenyNotAccepted   = "twitch.feature.in_game_currency.not_accepted"
	ItemBadgeDetails_RequiresOnlineConnectivity = "requires_online_connectivity" // if this product requires you to always be online this must be present in the item badge details list
	NotApplicable                               = "N/A"
)

var (
	defaultAvailableCountries []string = []string{"AE", "AZ", "AX", "AL", "AS", "AD", "AO", "AI", "AQ", "AG", "AR", "AM", "AW", "AU", "AT", "BD", "BF", "BH", "BN", "BS", "BB", "BY", "BE", "BZ", "BJ", "BM", "BT", "BO", "BA", "BW", "BV", "BR", "IO", "VG", "BG", "BI", "KH", "CM", "CA", "CV", "KY", "CF", "CL", "CX", "CC", "CO", "CG", "CD", "CK", "CR", "CI", "HR", "CY", "CZ", "DE", "DJ", "DZ", "DK", "DM", "DO", "EC", "SV", "GQ", "EG", "ER", "EE", "ET", "FK", "FO", "FJ", "FI", "FR", "TF", "GM", "GN", "GA", "GE", "GH", "GI", "GR", "GL", "GD", "GU", "GT", "GG", "GW", "GY", "HT", "HM", "HN", "HK", "HU", "ID", "IS", "IN", "IE", "IM", "IL", "IT", "JO", "JM", "JP", "JE", "KG", "KW", "KZ", "KE", "KI", "LB", "LA", "LV", "LS", "LR", "LI", "LT", "LU", "MO", "MK", "MG", "MW", "MT", "MP", "MH", "MU", "MX", "FM", "MA", "ML", "MR", "MV", "MY", "MD", "MC", "MN", "ME", "MS", "MZ", "MM", "NA", "NR", "NP", "NL", "AN", "NE", "NG", "NZ", "NI", "NU", "NF", "NO", "OM", "PK", "PW", "PA", "PG", "PY", "PE", "PH", "PN", "PL", "PT", "PR", "QA", "RO", "RU", "RW", "SH", "KN", "LC", "VC", "WS", "SM", "ST", "RS", "SA", "SG", "SL", "SN", "SO", "SC", "SK", "SI", "SB", "ZA", "GS", "KR", "ES", "LK", "BL", "MF", "SR", "SJ", "SZ", "SE", "CH", "TD", "TJ", "TM", "TN", "TR", "TW", "TZ", "TH", "TL", "TG", "TK", "TO", "TT", "TC", "TV", "UG", "UA", "GB", "US", "UM", "UY", "UZ", "VI", "VU", "VA", "VE", "VN", "YE", "EH", "ZM", "ZW"}
)

type (
	ADGSubmission struct {
		Tenant   Tenant   `json:"tenant"`
		Vendor   Vendor   `json:"vendor"`
		Product  Product  `json:"product"`
		Document Document `json:"document"`
		Options  Options  `json:"options"`
	}

	Tenant struct {
		Id string `json:"id"` // always hardcode
	}

	Vendor struct {
		Id string `json:"id"`
	}

	Product struct {
		Id                string      `json:"id,omitempty"`
		Sku               string      `json:"sku"`
		ExternalProductId string      `json:"externalProductId"` // Always the same as the sku -- will be set when marshalling
		Name              string      `json:"name"`
		ProductLine       ProductLine `json:"productLine"`
	}

	Document struct {
		Offerings     []Offering         `json:"offering"` // Needs to be converted to a JSON map with the same keys "amazon.com"
		Listings      map[string]Listing `json:"listing"`
		ProductInfo   ProductInfo        `json:"productInfo"`
		TaxCategories TaxCategories      `json:"taxInformation"`          // Consider changing the package so we can have this be named TaxInformation
		Relationships *Relationships     `json:"relationships,omitempty"` // Only need to fill out if this is an IGC or IAP and has a parent game/extension
	}

	Offering struct {
		AnnounceDate        string              `json:"announceDate"`
		ReleaseDate         string              `json:"releaseDate"`
		PublicationDate     string              `json:"publicationDate"`
		PreviousReleaseDate string              `json:"previousReleaseDate"`
		Countries           []string            `json:"countries"` // Should only ever have one country (where it is available)
		Pricing             map[string]ADGPrice `json:"pricing"`   // Should only ever have one map entry which should be the same as the single entry in the countries list
	}

	ADGPrice struct {
		Currency string  `json:"currency"`
		Price    float64 `json:"price"`
	}

	Listing struct {
		DisplayTitle                  string          `json:"displayTitle"`
		ShortDescription              string          `json:"shortDescription"`
		LongDescription               string          `json:"longDescription"`
		Features                      []string        `json:"bulletPointByLanguage,omitempty"`
		SupportedControllers          []string        `json:"controllerType,omitempty"`
		GameEdition                   string          `json:"editionNumber,omitempty"`
		GameEditionDetails            []string        `json:"generalFeature,omitempty"` // Should always be a single string
		Keywords                      []string        `json:"genericKeywordsByLanguage,omitempty"`
		IconAssetId                   string          `json:"iconAssetId,omitempty"`
		BoxShotAssetId                string          `json:"mainAssetId,omitempty"`
		PageBackgroundAssetId         string          `json:"bannerAssetId,omitempty"`
		ThankYouPageBackgroundAssetId string          `json:"glmrAssetId,omitempty"`
		ReleaseNotesAssetId           string          `json:"releaseNotesAssetId,omitempty"`
		ScreenshotAssetIds            []string        `json:"screenshotAssetIds,omitempty"`
		EulaUrl                       string          `json:"eulaUrl,omitempty"`
		PCRequirements                *PCRequirements `json:"technicalDetailsFormatted,omitempty"` // This is going to hurt -- Only need to populate this for a US listing
	}

	PCRequirements struct {
		Minimum           Requirements
		Recommended       Requirements
		OtherRequirements string
	}

	Requirements struct {
		OS             string `json:"OS"`
		Processor      string `json:"Processer"` // This really should be Processor but web incorrectly spelled it and the data migration is too extreme
		RAM            string `json:"RAM"`
		HardDriveSpace string `json:"HardDriveSpace"`
		VideoCard      string `json:"VideoCard"`
		DirectX        string `json:"DirectX"`
	}

	ProductInfo struct {
		NumberOfPlayersSupported      string   `json:"numberOfPlayersSupported"`
		ItemBadgeDetails              []string `json:"itemBadgeDetails"` // I'm guessing this only has requires_online_connectivity if it actually does. Fulfillment method as well
		AmazonMaturityRating          string   `json:"amazonMaturityRating"`
		USKRating                     string   `json:"uskRating"`
		PEGIRating                    string   `json:"pegiRating"`
		ESRBRating                    []string `json:"esrbRating"`
		DeveloperName                 string   `json:"developer"`
		DeveloperSiteUrl              string   `json:"developerSiteUrl"`
		DeveloperSiteUrlDuplicate     string   `json:"externalProductDescriptionUrl"` // Must always be the same as DeveloperSiteUrl cause that make sense... right... RIGHT!!sdjgfjksdklg
		CustomerSupportUrl            string   `json:"url,omitempty"`
		CustomerSupportEmailAddress   string   `json:"developerEmailAddress"`
		IsExtension                   bool     `json:"isExtensions"`         // Pluralization is hard
		DomainId                      string   `json:"domainName,omitempty"` // Its the domain id. jk its actually the DomainName which is the domain's id, not the name. Drinking yet? Oh yeah, and it only works for IGC/IAP
		VideoUrls                     []string `json:"videoUrls,omitempty"`
		GameVersion                   string   `json:"gameVersion,omitempty"`
		SupportedLanguageInstructions []string `json:"languageManual,omitempty"`
		SupportedLanguageSubtitles    []string `json:"languageSubtitled,omitempty"`
		SupportedLanguageAudio        []string `json:"languageAudioDescription,omitempty"`
		CategoryId                    string   `json:"itemTypeKeyword,omitempty"`
		RefinementIds                 []string `json:"thesaurusSubjectKeyword,omitempty"` // This appears to be the Category Info Id twice? Me not understands
		VersionForCountry             []string `json:"versionForCountry"`                 // I guess this is just an availability lists aka every country we have an offering for
		RevisionNumber                int      `json:"volumeNumber"`                      // This is supposed to be how many submissions we've made for this product so far -- probably fine to hardcode now
	}

	TaxCategories struct {
		ProductCategory    string `json:"productCategory"`
		ProductSubCategory string `json:"productSubCategory"`
	}

	Relationships struct {
		Parent Parent `json:"parent,omitempty"`
	}

	Parent struct {
		ADGProductId string `json:"id,omitempty"`
	}

	Options struct {
		AutoSubmit bool `json:"autoSubmit"` // Should always be true unless ADG is being used as a document store
	}
)

func NewADGSubmission() *ADGSubmission {
	submission := &ADGSubmission{}

	// Defaults
	submission.Options.AutoSubmit = true
	submission.Tenant.Id = tenantId
	submission.Document.ProductInfo.RevisionNumber = defaultRevisionNumber

	return submission
}

func (requirements PCRequirements) MarshalJSON() ([]byte, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function": "PCRequirements.MarshalJSON",
	})
	var requirementsList []string

	// Minimum
	minimumRequirementsList, err := requirements.Minimum.getJSONTagDelimitedList("PCRequirements-Minimum-")
	if err != nil {
		log.Warn("Error getting minimum PCRequirements list", err)
		return nil, err
	}
	requirementsList = append(requirementsList, minimumRequirementsList...)

	// Recommended
	recommendedRequirementsList, err := requirements.Recommended.getJSONTagDelimitedList("PCRequirements-Recommended-")
	if err != nil {
		log.Warn("Error getting minimum PCRequirements list", err)
		return nil, err
	}
	requirementsList = append(requirementsList, recommendedRequirementsList...)

	// Other
	if len(requirements.OtherRequirements) > 0 {
		requirementsList = append(requirementsList, "OtherRequirements:"+requirements.OtherRequirements)
	}

	return json.Marshal(requirementsList)
}

// Please forgive me lord
func (requirements Requirements) getJSONTagDelimitedList(prefix string) ([]string, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function": "getJSONTagDelimitedList",
	})

	value := reflect.ValueOf(requirements)
	valueTypes := value.Type()

	var results []string
	for i := 0; i < value.NumField(); i++ {
		field := value.Field(i)

		// skip fields that are empty
		if len(field.String()) > 0 {
			tagValue, ok := valueTypes.Field(i).Tag.Lookup("json")
			if !ok {
				log.Error("Unable to retrieve json tag for PCRequirement. Review tags")
				return nil, errors.New("Error retrieving json tag for PCRequirement")
			}

			results = append(results, prefix+tagValue+":"+field.String())
		}
	}

	return results, nil
}

func (document Document) MarshalJSON() ([]byte, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function": "Document.MarshalJSON",
	})

	// Build document map
	documentMap := make(map[string]string)

	// Offering
	offeringString, err := document.buildOfferingString()
	if err != nil {
		log.Warn("Error marshaling the Offering", err)
		return nil, err
	}
	documentMap["offering"] = *offeringString

	// Listing
	listingJSON, err := json.Marshal(document.Listings)
	if err != nil {
		log.Warn("Error marshaling the Listings", err)
		return nil, err
	}
	documentMap["listing"] = string(listingJSON)

	// ProductInfo
	productInfoJSON, err := json.Marshal(document.ProductInfo)
	if err != nil {
		log.Warn("Error marshaling the ProductInfo", err)
		return nil, err
	}
	documentMap["productInfo"] = string(productInfoJSON)

	// TaxCategories
	taxCategoriesJSON, err := json.Marshal(document.TaxCategories)
	if err != nil {
		log.Warn("Error marshaling the TaxCategories", err)
		return nil, err
	}
	documentMap["taxInformation"] = string(taxCategoriesJSON)

	// Relationships - Only used for IAP and IGC
	if document.Relationships != nil {
		relationshipsJSON, err := json.Marshal(document.Relationships)
		if err != nil {
			log.Warn("Error marshaling the Relationships", err)
			return nil, err
		}
		documentMap["relationships"] = string(relationshipsJSON)
	}

	return json.Marshal(documentMap)
}

// Constructs a string representation of the offerings struct. This is required because the
// ADGSubmissionService model isn't valid JSON.
// I think lesser of myself for writing this
func (document Document) buildOfferingString() (*string, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function": "Document.buildOfferingString",
	})
	log.WithField("Offering", document.Offerings).Debug("Entering")

	var offeringByteBuffer bytes.Buffer

	_, err := offeringByteBuffer.WriteString("{")
	if err != nil {
		log.Warn("Error writing to byte buffer", err)
		return nil, err
	}

	for index, offering := range document.Offerings {
		offeringByteBuffer.WriteString(`"amazon.com":`)

		offeringJSON, err := json.Marshal(offering)
		if err != nil {
			log.WithField("Offering", offering).Warn("Failed to marshal JSON for offering", err)
			return nil, err
		}

		_, err = offeringByteBuffer.Write(offeringJSON)
		if err != nil {
			log.Warn("Error writing to byte buffer", err)
			return nil, err
		}

		if index < (len(document.Offerings) - 1) {
			_, err = offeringByteBuffer.WriteString(`,`)
			if err != nil {
				log.Warn("Error writing to byte buffer", err)
				return nil, err
			}
		}
	}

	_, err = offeringByteBuffer.WriteString("}")
	if err != nil {
		log.Warn("Error writing to byte buffer", err)
		return nil, err
	}

	offeringString := offeringByteBuffer.String()
	log.WithField("Offering String", offeringString).Debug("Exiting")
	return &offeringString, nil
}

func NewDefaultPCRequirements() *PCRequirements {
	return &PCRequirements{
		Minimum: Requirements{
			OS:             defaultPCRequirement,
			Processor:      defaultPCRequirement,
			RAM:            defaultPCRequirement,
			HardDriveSpace: defaultPCRequirement,
			VideoCard:      defaultPCRequirement,
			DirectX:        defaultPCRequirement,
		},
		Recommended: Requirements{
			OS:             defaultPCRequirement,
			Processor:      defaultPCRequirement,
			RAM:            defaultPCRequirement,
			HardDriveSpace: defaultPCRequirement,
			VideoCard:      defaultPCRequirement,
			DirectX:        defaultPCRequirement,
		},
	}

}

func NewExtensionSubmission() *ADGSubmission {
	submission := NewADGSubmission()

	// Product Info
	productInfo := &submission.Document.ProductInfo
	productInfo.CategoryId = defaultExtensionCategoryId
	productInfo.RefinementIds = []string{defaultExtensionRefinementId}
	productInfo.ItemBadgeDetails = []string{defaultExtensionFulfillmentMethod, ItemBadgeDetails_InGameCurrenyNotAccepted, ItemBadgeDetails_RequiresOnlineConnectivity}
	productInfo.NumberOfPlayersSupported = defaultNumberOfPlayersSupported
	productInfo.GameVersion = defaultGameVersion
	productInfo.SupportedLanguageAudio = []string{"english"}
	productInfo.SupportedLanguageInstructions = []string{"english"}
	productInfo.SupportedLanguageSubtitles = []string{"english"}
	productInfo.IsExtension = true
	productInfo.AmazonMaturityRating = defaultAmazonRating
	productInfo.ESRBRating = []string{defaultESRBRating}
	productInfo.PEGIRating = defaultPEGIRating
	productInfo.USKRating = defaultUSKRating
	productInfo.RevisionNumber = defaultRevisionNumber
	productInfo.VersionForCountry = defaultAvailableCountries

	// Offerings
	var offerings []Offering

	for _, country := range productInfo.VersionForCountry {
		offerings = append(offerings, Offering{
			AnnounceDate:        DefaultExtensionReleaseDate,
			ReleaseDate:         DefaultExtensionReleaseDate,
			PreviousReleaseDate: DefaultExtensionReleaseDate,
			PublicationDate:     DefaultExtensionReleaseDate,
			Countries:           []string{country},
			Pricing: map[string]ADGPrice{
				country: ADGPrice{
					Currency: "USD",
					Price:    float64(0),
				}},
		})
	}

	submission.Document.Offerings = offerings
	submission.Document.Listings = make(map[string]Listing)

	return submission
}

func NewExtensionIAPSubmission() *ADGSubmission {
	submission := NewExtensionSubmission()
	submission.Document.ProductInfo.CategoryId = defaultExtensionIAPCategoryId
	submission.Document.ProductInfo.RefinementIds = []string{defaultIAPRefinementId}
	submission.Document.Listings = make(map[string]Listing)
	return submission
}

func NewGameSubmission() *ADGSubmission {
	submission := NewADGSubmission()
	submission.Document.ProductInfo.RefinementIds = []string{defaultGameRefinementId, defaultGameRefinementId}
	submission.Document.ProductInfo.RevisionNumber = defaultRevisionNumber
	submission.Document.Listings = make(map[string]Listing)
	return submission
}

func NewIGCSubmission() *ADGSubmission {
	submission := NewADGSubmission()
	submission.Document.ProductInfo.CategoryId = DefaultIGCCategoryId
	submission.Document.ProductInfo.RefinementIds = []string{defaultIGCRefinementId}
	submission.Document.ProductInfo.RevisionNumber = defaultRevisionNumber
	submission.Document.Listings = make(map[string]Listing)
	return submission
}
