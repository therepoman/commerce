package adg

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"reflect"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

const (
	// Vendor
	vendorId string = "vendorId"

	// Product
	sku         string = "sku"
	productName string = "productName"

	// Listing
	displayTitle                  string = "display title"
	shortDescription              string = "short description"
	longDescription               string = "long Description"
	developerName                 string = "developerName"
	firstFeature                  string = "first feature"
	secondFeature                 string = "second feature"
	mouse                         string = "mouse"
	keyboard                      string = "keyboard"
	gameEdition                   string = "game edition"
	gameEditionDetails            string = "game edition details"
	firstKeyword                  string = "firstKeyword"
	secondKeyword                 string = "secondKeyword"
	iconAssetId                   string = "amzn1.dex.asset.iconassetid"
	boxshotAssetId                string = "amzn1.dex.asset.boxshotassid"
	pageBackgroundAssetId         string = "amzn1.dex.asset.pagebackgroundassetid"
	thankYouPageBackgroundAssetId string = "amzn1.dex.asset.thankyoupagebackgroundassetid"
	releaseNotesAssetId           string = "amzn1.dex.asset.releasenotesassetid"
	eulaURL                       string = "www.eula.url.com"
	firstScreenshotId             string = "amzn1.dex.asset.firstscreenshotid"
	secondScreenshotId            string = "amzn1.dex.asset.secondscreenshotid"

	// PC Requirements
	minimumOS             string = "minimum OS"
	minimumProcessor      string = "minimum processor"
	minimumRAM            string = "minimum RAM"
	minimumHardDriveSpace string = "minimum hard drive space"
	minimumVideoCard      string = "minimum video card"
	minimumDirectX        string = "minimum directx"

	recommendedOS             string = "recommended OS"
	recommendedProcessor      string = "recommended processor"
	recommendedRAM            string = "recommended RAM"
	recommendedHardDriveSpace string = "recommended hard drive space"
	recommendedVideoCard      string = "recommended video card"
	recommendedDirectX        string = "recommended directx"

	// ProductInfo
	requiresOnline              bool   = true
	numberOfPlayersSupported    string = "4 players"
	amazonMaturityRating        string = "amazing_rating_pending"
	uskRating                   string = "usk_rating_pending"
	pegiRating                  string = "pegi_rating_pending"
	esrbRating                  string = "esrb_rating_pending"
	developerSiteUrl            string = "www.developersiteurl.com"
	customerSupportUrl          string = "www.customersupporturl.com"
	customerSupportEmailAddress string = "supportemail@test_twitch.tv"
	domainId                    string = "domainId"
	firstVideoUrl               string = "www.firstvideourl.com"
	secondVideoUrl              string = "www.secondvideourl.com"
	gameTestFulfillmentMethod   string = "fulfilled_FuelLauncher_default"
	gameVersion                 string = "game verison"
	english                     string = "english"
	spanish                     string = "spanish"
	categoryInfoId              string = "twitch_fuel_action_game"

	// Offering
	announceDate          string  = "2006-01-02T15:04:05Z"
	releaseDate           string  = "2007-01-02T15:04:05Z"
	previousReleaseDate   string  = "2008-01-02T15:04:05Z"
	publicationDate       string  = "2009-01-02T15:04:05Z"
	usdCurrency           string  = "USD"
	price                 float64 = 9.99
	firstOfferingCountry  string  = "US"
	secondOfferingCountry string  = "IT"
	usLocale              string  = "en-US"
	itLocale              string  = "it-IT"

	// Tax Categories
	taxProductCategory    string = "64961000"
	taxProductSubCategory string = "64961001"

	// Relationships
	parentADGProductId string = "ParentADGProductId"

	// Submission path
	submissionJSONPath string = "/src/code.justin.tv/commerce/tachanka/test/data/adg/adg_game_submission.json"
)

func TestFullADGSubmissionModelMarshalJSON(t *testing.T) {
	Convey("Given a game ADGSubmission", t, func() {
		submission := newGameSubmission()
		submission.Product.ProductLine = ProductLine_Game
		submission.Document.ProductInfo.IsExtension = false

		//	spew.Dump(submission)
		Convey("It should serialize into expected JSON", func() {
			var gameSub, validGameSub interface{}

			gameSubmissionJSON, err := json.Marshal(submission)
			So(err, ShouldBeNil)

			err = json.Unmarshal(gameSubmissionJSON, &gameSub)
			So(err, ShouldBeNil)

			validGameSubmissionJSON := newValidADGSubmissionJSON()
			err = json.Unmarshal(validGameSubmissionJSON, &validGameSub)
			So(err, ShouldBeNil)

			equal := reflect.DeepEqual(gameSub, validGameSub)
			if !equal {
				fmt.Printf("Actual:\n %v \n Expected: \n %v", string(gameSubmissionJSON), string(validGameSubmissionJSON))
			}
			So(equal, ShouldBeTrue)
		})

		Convey("That is a game", func() {

		})

	})
}

func newGameSubmission() *ADGSubmission {
	submission := NewGameSubmission()

	populateVendor(submission)
	populateProduct(submission)
	populateDocument(submission)
	populateOptions(submission)

	return submission
}

func populateProduct(submission *ADGSubmission) {
	submission.Product.Sku = sku
	submission.Product.ExternalProductId = submission.Product.Sku
	submission.Product.Name = productName
}

func populateVendor(submission *ADGSubmission) {
	submission.Vendor.Id = vendorId
}

func populateOptions(submission *ADGSubmission) {
	submission.Options.AutoSubmit = true
}

func populateDocument(submission *ADGSubmission) {
	submission.Document.Offerings = []Offering{
		*newOffering(firstOfferingCountry),
		*newOffering(secondOfferingCountry),
	}

	submission.Document.Listings = map[string]Listing{
		usLocale: *newListing(usLocale),
		itLocale: *newListing(itLocale),
	}

	populateProductInfo(submission)
	populateTaxCategories(submission)
}

func newOffering(country string) *Offering {
	pricingMap := make(map[string]ADGPrice)
	pricingMap[country] = ADGPrice{
		Currency: usdCurrency,
		Price:    price,
	}

	return &Offering{
		AnnounceDate:        announceDate,
		ReleaseDate:         releaseDate,
		PublicationDate:     publicationDate,
		PreviousReleaseDate: previousReleaseDate,
		Countries:           []string{country},
		Pricing:             pricingMap,
	}
}

func newListing(locale string) *Listing {
	return &Listing{
		DisplayTitle:                  displayTitle,
		ShortDescription:              shortDescription,
		LongDescription:               longDescription,
		Features:                      []string{firstFeature, secondFeature},
		GameEdition:                   gameEdition,
		GameEditionDetails:            []string{gameEditionDetails},
		Keywords:                      []string{firstKeyword, secondKeyword},
		IconAssetId:                   iconAssetId,
		BoxShotAssetId:                boxshotAssetId,
		PageBackgroundAssetId:         pageBackgroundAssetId,
		ThankYouPageBackgroundAssetId: thankYouPageBackgroundAssetId,
		ReleaseNotesAssetId:           releaseNotesAssetId,
		EulaUrl:                       eulaURL,
		ScreenshotAssetIds:            []string{firstScreenshotId, secondScreenshotId},
		SupportedControllers:          []string{mouse, keyboard},
		PCRequirements: &PCRequirements{
			Minimum: Requirements{
				OS:             minimumOS,
				Processor:      minimumProcessor,
				RAM:            minimumRAM,
				HardDriveSpace: minimumHardDriveSpace,
				VideoCard:      minimumVideoCard,
				DirectX:        minimumDirectX,
			},
			Recommended: Requirements{
				OS:             recommendedOS,
				Processor:      recommendedProcessor,
				RAM:            recommendedRAM,
				HardDriveSpace: recommendedHardDriveSpace,
				VideoCard:      recommendedVideoCard,
				DirectX:        recommendedDirectX,
			},
		},
	}
}

func populateProductInfo(submission *ADGSubmission) {
	productInfo := &submission.Document.ProductInfo

	productInfo.NumberOfPlayersSupported = numberOfPlayersSupported
	productInfo.AmazonMaturityRating = amazonMaturityRating
	productInfo.USKRating = uskRating
	productInfo.PEGIRating = pegiRating
	productInfo.ESRBRating = []string{esrbRating}
	productInfo.DeveloperName = developerName
	productInfo.DeveloperSiteUrl = developerSiteUrl
	productInfo.DeveloperSiteUrlDuplicate = productInfo.DeveloperSiteUrl
	productInfo.CustomerSupportUrl = customerSupportUrl
	productInfo.CustomerSupportEmailAddress = customerSupportEmailAddress
	productInfo.VideoUrls = []string{firstVideoUrl, secondVideoUrl}
	productInfo.ItemBadgeDetails = []string{gameTestFulfillmentMethod}
	productInfo.IsExtension = false
	productInfo.SupportedLanguageInstructions = []string{english}
	productInfo.SupportedLanguageSubtitles = []string{english, spanish}
	productInfo.SupportedLanguageAudio = []string{spanish}
	productInfo.CategoryId = categoryInfoId
	productInfo.VersionForCountry = []string{firstOfferingCountry, secondOfferingCountry}
}

func populateTaxCategories(submission *ADGSubmission) {
	submission.Document.TaxCategories.ProductCategory = taxProductCategory
	submission.Document.TaxCategories.ProductSubCategory = taxProductSubCategory
}

func newRelationships() *Relationships {
	return &Relationships{
		Parent: Parent{
			ADGProductId: parentADGProductId,
		},
	}
}

func newValidADGSubmissionJSON() []byte {
	var filePath string = os.Getenv("GOPATH") + submissionJSONPath

	jsonBytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		panic(err)
	}

	return jsonBytes
}
