package currency

import "fmt"

type Amount float64

// You're gonna have a bad time with this if you use a float with more than 2 points of precision
func (n Amount) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("%.2f", n)), nil
}
