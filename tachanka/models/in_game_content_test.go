package models_test

import (
	"bufio"
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"strconv"
	"strings"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"

	"fmt"

	models "code.justin.tv/commerce/tachanka/models"
	adg "code.justin.tv/commerce/tachanka/models/adg"
	"github.com/stretchr/testify/assert"
)

const (
	igcId                  string = "id"
	igcTitle               string = "FancyPantsIGC"
	igcProductId           string = "igcProductId"
	igcDraftDocumentId     string = "igcDraftDocumentId"
	igcLiveId              string = "igcLiveId"
	igcWorkingSubmissionId string = "igcWorkingSubmissionId"
	igcSku                 string = "TestSku1"
	igcContentType         string = "virtual_currency"
	igcProductLine         string = "Twitch:FuelVirtualCurrency"
	igcProductType         string = "Entitlement"
	igcFulfillment         string = "fulfilled_FuelLauncher_default"
)

var igcFile string = os.Getenv("GOPATH") + "/src/code.justin.tv/commerce/tachanka/config/data/metadata_truthtables/igc.txt"

func TestIGCImplementsExpectedInterfaces(t *testing.T) {
	igc := newTestIGC()

	Convey("Verifying the object is an igc", t, func() {
		So(igc.GetType(), ShouldEqual, models.ModelType_InGameContent)
	})

	Convey("Verifying the igc has the expected data", t, func() {
		// Since there are a LOT of fields here, we perform comparisons against
		// a map[string]interface{} from the same file instead of hardcoding strings
		testMap := newIGCTestMap()

		// Test the struct's strings
		So(igc.SKU, ShouldEqual, testMap["sku"])
		So(igc.FulfillmentMethod, ShouldEqual, testMap["fulfillmentMethod"])
		So(igc.ReleaseDate, ShouldEqual, testMap["releaseDate"])

		// Test locale
		So(len(igc.LocaleListings), ShouldEqual, 2)

		// Test category info
		So(igc.CategoryInfoId, ShouldEqual, testMap["categoryInfoId"])

		// Test availability info
		So(len(igc.AvailabilityInfo), ShouldEqual, 2)
		So(igc.AvailabilityInfo[0], shouldEqualAvailabilityEntry, testMap["availabilityInfo"].([]interface{})[0])
		So(igc.AvailabilityInfo[1], shouldEqualAvailabilityEntry, testMap["availabilityInfo"].([]interface{})[1])
	})

	Convey("Verifying the igc implements ManagedProduct", t, func() {
		var managedProduct models.ManagedProduct = &igc

		// Verify the conversion worked as expected
		eq := reflect.DeepEqual(managedProduct, &igc)
		So(eq, ShouldBeTrue)
		// Verify the managed product fields
		So(managedProduct.GetId(), ShouldEqual, gameId)
		So(managedProduct.GetDraftDocumentId(), ShouldEqual, gameDraftDocumentId)
		So(managedProduct.GetADGProductId(), ShouldEqual, gameProductId)
		So(managedProduct.GetLiveSubmissionId(), ShouldEqual, gameLiveId)
		So(managedProduct.GetWorkingSubmissionId(), ShouldEqual, gameWorkingSubmissionId)
		// ShouldEqual fails to properly compare these, so DeepEqual is required
		eq = reflect.DeepEqual(managedProduct.GetLastUpdated(), time.Time{})
		So(eq, ShouldBeTrue)
	})

	Convey("Verifying the igc implements ADGSellable", t, func() {
		var adgSellable adg.Sellable = &igc

		// Verify the conversion worked as expected
		eq := reflect.DeepEqual(adgSellable, &igc)
		So(eq, ShouldBeTrue)
		// Verify the managed product fields
		So(adgSellable.GetSKU(), ShouldEqual, gameSku)
		So(adgSellable.GetTitle(), ShouldEqual, gameTitle)
		// ShouldEqual fails to properly compare these, so DeepEqual is required
		eq = reflect.DeepEqual(adgSellable.GetTaxInformation(), adg.TaxInformation{
			MultiplePurchases: true,
			InGameCurrency:    true,
			Durable:           true,
		})
		fmt.Println("TaxInfo: ", adgSellable.GetTaxInformation())
		So(eq, ShouldBeTrue)
		So(adgSellable.GetFulfillmentMethod(), ShouldEqual, igcFulfillment)
	})
}

func TestCorrectInheritance(t *testing.T) {
	igc := newTestIGC()
	partialDoc := adg.NewBaseGameDocument()
	partialDoc.ListingsByLocale["parentLocale1"] = adg.LocaleListing{}
	partialDoc.ListingsByLocale["parentLocale2"] = adg.LocaleListing{}
	partialDoc.ListingsByLocale["en-US"] = adg.LocaleListing{
		GameEdition:        "parentGameEdition",
		GameEditionDetails: "parentGameEditionDetails",
		ShortDescription:   "parentGameShortDescription",
		LongDescription:    "parentGameLongDescription",
		DisplayName:        "parentGameDisplayName",
	}

	partialDoc.AvailabilityInfo.AvailableCountries = append(partialDoc.AvailabilityInfo.AvailableCountries, "ES")
	partialDoc.AvailabilityInfo.AvailableCountries = append(partialDoc.AvailabilityInfo.AvailableCountries, "US")

	partialDoc = igc.PopulateADGDocument(partialDoc)

	listings := partialDoc.ListingsByLocale
	availabilityInfo := partialDoc.AvailabilityInfo
	Convey("Verifying the igc populates listing correctly", t, func() {
		/* parent localeListing parentLocale1, parentLocale2, en-US
		IGC available in en-US and de-DE */
		So(len(listings), ShouldEqual, 2)

		USlisting := listings["en-US"]
		So(USlisting.GameEdition, ShouldEqual, "parentGameEdition")
		So(USlisting.GameEditionDetails, ShouldEqual, "parentGameEditionDetails")
		So(USlisting.ShortDescription, ShouldNotEqual, "parentGameShortDescription")
		So(USlisting.LongDescription, ShouldNotEqual, "parentGameLongDescription")
		So(USlisting.DisplayName, ShouldNotEqual, "parentGameDisplayName")

		/* parent available in ES and US
		IGC available in US and DE */
		So(len(availabilityInfo.AvailableCountries), ShouldEqual, 1)
	})
}

func TestIGCMetadata(t *testing.T) {
	f, err := os.Open(igcFile)
	if err != nil {
		log.Fatal("Failed to open truth table", err)
	}
	fileScanner := bufio.NewScanner(f)

	for fileScanner.Scan() {
		thisLine := fileScanner.Text()
		values := strings.Split(thisLine, "\t")
		multiplePurchases, err := strconv.ParseBool(values[0])
		if err != nil {
			log.Fatal("Non bool at position 0", err)
		}
		inGameCurrency, err := strconv.ParseBool(values[1])
		if err != nil {
			log.Fatal("Non bool at position 1", err)
		}
		durable, err := strconv.ParseBool(values[2])
		if err != nil {
			log.Fatal("Non bool at position 2", err)
		}
		fulfilledByTwitch, err := strconv.ParseBool(values[3])
		if err != nil {
			log.Fatal("Non bool at position 3", err)
		}
		taxInfo := adg.TaxInformation{
			Durable:           durable,
			InGameCurrency:    inGameCurrency,
			MultiplePurchases: multiplePurchases,
		}

		desiredProductType := values[4]
		desiredContentType := values[5]
		desiredProductLine := values[6]
		desiredProductCategory := values[7]
		desiredSubcategory := values[8]

		igc := models.InGameContent{}
		igc.TaxInformation = taxInfo
		metadata := igc.GetADGMetadata(fulfilledByTwitch)
		assert.Equal(t, desiredContentType, metadata.ContentType.ToString(), "IGC should have desired ContentType")
		assert.Equal(t, desiredProductLine, metadata.ProductLine.ToString(), "IGC should have desired ProductLine")
		assert.Equal(t, desiredProductCategory, metadata.ProductCategory.ToString(), "IGC should have desired ProductCategory")
		assert.Equal(t, desiredSubcategory, metadata.Subcategory.ToString(), "IGC should have desired Subcategory")
		assert.Equal(t, desiredProductType, metadata.ProductType.ToString(), "IGC should have desired ProductType")
	}
}

func newTestIGC() models.InGameContent {
	var igc models.InGameContent
	err := json.Unmarshal([]byte(newIGCDocumentString()), &igc)

	if err != nil {
		panic(err)
	}

	igc.SetId(gameId)
	igc.SetADGProductId(gameProductId)
	igc.SetDraftDocumentId(gameDraftDocumentId)
	igc.SetLiveSubmissionId(gameLiveId)
	igc.SetWorkingSubmissionId(gameWorkingSubmissionId)
	igc.SetLastUpdated(time.Time{})
	return igc
}

func newIGCDocumentString() string {
	var validIGCDocumentFile string = os.Getenv("GOPATH") + "/src/code.justin.tv/commerce/tachanka/config/data/front_end_requests/valid_igc_data_1.json"
	validJsonFile, err := ioutil.ReadFile(validIGCDocumentFile)
	if err != nil {
		panic(err)
	}
	return string(validJsonFile)
}

func newIGCTestMap() map[string]interface{} {
	var testMap map[string]interface{}
	err := json.Unmarshal([]byte(newIGCDocumentString()), &testMap)
	if err != nil {
		panic(err)
	}
	return testMap
}
