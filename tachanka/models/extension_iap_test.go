package models_test

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"reflect"
	"testing"
	"time"

	"code.justin.tv/commerce/tachanka/models"

	"code.justin.tv/commerce/tachanka/models/adg"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/assert"
)

const (
	extIAPBoxshotId                   string  = "amzn1.dex.asset.5b72f947d259434d89dbce4906411e79"
	extIAPMultiplePurchases           bool    = true
	extIAPMarketplace                 string  = "amazon.com"
	extIAPDataItemType                string  = "GeneralDigitalGood"
	extIAPPrice                       float64 = 4.99
	extIAPUSCode                      string  = "US"
	extIAPDECode                      string  = "DE"
	extIAPUS                          string  = "en-US"
	extIAPDE                          string  = "de-DE"
	extIAPDisplayNameDE               string  = "UAT Game under $40 GERMAN"
	extIAPKeyword                     string  = "game"
	extIAPID                          string  = "testid"
	extIAPAdgProductId                string  = "testadgProductId"
	extIAPSku                         string  = "TestSku1"
	extIAPTitle                       string  = "UAT Game under $40"
	extIAPContentType                 string  = "extension"
	extIAPProductLineMultiPurchase    string  = "Twitch:FuelConsumable"
	extIAPProductLineNonMultiPurchase string  = "Twitch:FuelEntitlement"
	extIAPProductTypeMultiPurchase    string  = "Consumable"
	extIAPProductTypeNonMultiPurchase string  = "Entitlement"
	extIAPFulfillmentMethod           string  = "fulfilled_FuelLauncher_default"
	extIAPShortDescriptionUS          string  = "This game is for testing purpose, do not use in other places"
	extIAPLongDescriptionUS           string  = "This game is for testing purpose, do not use in other places, also, it should pass the ingestion part"
	extIAPShortDescriptionDE          string  = "This game is for testing purpose, do not use in other places, and also this is translated"
	extIAPLongDescriptionDE           string  = "This game is for testing purpose, do not use in other places, also, it should pass the ingestion part, and also this is translated"
)

func TestExtensionIAPToADGProduct(t *testing.T) {
	extensionIAP := newTestExtensionIAP()
	baseextIAPADGDocument := adg.NewBaseExtensionIAPDocument()
	adgDocument := extensionIAP.PopulateADGDocument(baseextIAPADGDocument)
	assert.Equal(t, 1, len(adgDocument.PricingInfo.PricesByMarketplaceAndCountry[extIAPMarketplace].PricesByCountry), "ADGDocument should have correct info about countries")

	_, isUSPresent := adgDocument.PricingInfo.PricesByMarketplaceAndCountry[extIAPMarketplace].PricesByCountry[extIAPUS]
	assert.Equal(t, false, isUSPresent, "ADGDocument should have correct info about countries")

	_, isINPresent := adgDocument.PricingInfo.PricesByMarketplaceAndCountry[extIAPMarketplace].PricesByCountry[extIAPDE]
	assert.Equal(t, false, isINPresent, "ADGDocument should have correct info about countries")

	assert.Equal(t, extensionIAP.LocaleListings[0].DisplayName, adgDocument.ListingsByLocale[extIAPUS].DisplayName, "ADGDocument should have correct info about locales")
	assert.Equal(t, extensionIAP.LocaleListings[0].BoxshotId, adgDocument.ListingsByLocale[extIAPUS].BoxshotId, "ADGDocument should have boxshotId for US locale")
	assert.Equal(t, extensionIAP.LocaleListings[0].ShortDescription, adgDocument.ListingsByLocale[extIAPUS].ShortDescription, "ADGDocument should have ShortDescription for US locale")
	assert.Equal(t, extensionIAP.LocaleListings[0].LongDescription, adgDocument.ListingsByLocale[extIAPUS].LongDescription, "ADGDocument should have LongDescription for US locale")
	assert.Equal(t, extensionIAP.LocaleListings[1].DisplayName, adgDocument.ListingsByLocale[extIAPDE].DisplayName, "ADGDocument should have correct info about locales")
	assert.Equal(t, extensionIAP.LocaleListings[1].BoxshotId, adgDocument.ListingsByLocale[extIAPDE].BoxshotId, "ADGDocument should have boxshotId for DE locale")
	assert.Equal(t, extensionIAP.LocaleListings[1].ShortDescription, adgDocument.ListingsByLocale[extIAPDE].ShortDescription, "ADGDocument should have ShortDescription for DE locale")
	assert.Equal(t, extensionIAP.LocaleListings[1].LongDescription, adgDocument.ListingsByLocale[extIAPDE].LongDescription, "ADGDocument should have LongDescription for DE locale")
}

func TestExtensionIAPModel(t *testing.T) {
	extIAP := newTestExtensionIAP()

	Convey("Verifying the extension iap is an extension iap", t, func() {
		So(extIAP.GetType(), ShouldEqual, models.ModelType_ExtensionInAppPurchase)
	})

	Convey("Verifying the extension iap has the expected data", t, func() {
		So(extIAP.SKU, ShouldEqual, extIAPSku)
		So(extIAP.MultiplePurchases, ShouldEqual, extIAPMultiplePurchases)
		So(extIAP.DefaultPrice, ShouldEqual, extIAPPrice)
		So(string(extIAP.ItemType), ShouldEqual, extIAPDataItemType)
		So(extIAP.AvailabilityInfo, ShouldNotBeEmpty)
		So(len(extIAP.AvailabilityInfo), ShouldEqual, 2)
		So(extIAP.AvailabilityInfo[0].CountryCode, ShouldEqual, extIAPUSCode)
		So(extIAP.AvailabilityInfo[0].Available, ShouldEqual, true)
		So(extIAP.AvailabilityInfo[0].Price, ShouldEqual, extIAPPrice)
		So(extIAP.AvailabilityInfo[1].CountryCode, ShouldEqual, extIAPDECode)
		So(extIAP.AvailabilityInfo[1].Available, ShouldEqual, false)
		So(extIAP.AvailabilityInfo[1].Price, ShouldEqual, extIAPPrice)
		So(extIAP.LocaleListings, ShouldNotBeEmpty)
		So(len(extIAP.LocaleListings), ShouldEqual, 2)
		So(extIAP.LocaleListings[0].Locale, ShouldEqual, extIAPUS)
		So(extIAP.LocaleListings[0].DisplayName, ShouldEqual, extIAPTitle)
		So(extIAP.LocaleListings[0].Keywords, ShouldNotBeEmpty)
		So(extIAP.LocaleListings[0].Keywords[0], ShouldEqual, extIAPKeyword)
		So(extIAP.LocaleListings[0].BoxshotId, ShouldEqual, extIAPBoxshotId)
		So(extIAP.LocaleListings[0].ShortDescription, ShouldEqual, extIAPShortDescriptionUS)
		So(extIAP.LocaleListings[0].LongDescription, ShouldEqual, extIAPLongDescriptionUS)
		So(extIAP.LocaleListings[1].Locale, ShouldEqual, extIAPDE)
		So(extIAP.LocaleListings[1].DisplayName, ShouldEqual, extIAPDisplayNameDE)
		So(extIAP.LocaleListings[1].Keywords, ShouldNotBeEmpty)
		So(extIAP.LocaleListings[1].Keywords[0], ShouldEqual, extIAPKeyword)
		So(extIAP.LocaleListings[1].BoxshotId, ShouldEqual, extIAPBoxshotId)
		So(extIAP.LocaleListings[1].ShortDescription, ShouldEqual, extIAPShortDescriptionDE)
		So(extIAP.LocaleListings[1].LongDescription, ShouldEqual, extIAPLongDescriptionDE)
	})

	Convey("Verifying the extension iap implements ManagedProduct", t, func() {
		var managedProduct models.ManagedProduct = &extIAP

		// Verify the conversion worked as expected
		eq := reflect.DeepEqual(managedProduct, &extIAP)
		So(eq, ShouldBeTrue)
		// Verify the managed product fields
		So(managedProduct.GetADGProductId(), ShouldEqual, extIAPAdgProductId)
		// ShouldEqual fails to properly compare these, so DeepEqual is required
		eq = reflect.DeepEqual(managedProduct.GetLastUpdated(), time.Time{})
		So(eq, ShouldBeTrue)
	})

	Convey("Verifying the extension iap implements ADGSellable", t, func() {
		var adgSellable adg.Sellable = &extIAP

		// Verify the conversion worked as expected
		eq := reflect.DeepEqual(adgSellable, &extIAP)
		So(eq, ShouldBeTrue)
		// Verify the managed product fields
		So(adgSellable.GetSKU(), ShouldEqual, extIAPSku)
		So(adgSellable.GetTitle(), ShouldEqual, extIAPTitle)
		// ShouldEqual fails to properly compare these, so DeepEqual is required
		eq = reflect.DeepEqual(adgSellable.GetTaxInformation(), adg.TaxInformation{
			Durable:           false,
			MultiplePurchases: true,
			InGameCurrency:    false,
		})
		So(eq, ShouldBeTrue)
		So(adgSellable.GetFulfillmentMethod(), ShouldEqual, extIAPFulfillmentMethod)
		// Note: since metadata is a more involved check, this is performed separately
	})
}

func TestExtensionsIAPModelMetadata(t *testing.T) {

	extItemTypeCategory := map[models.ExtensionItemType]adg.ProductCategory{
		models.ExtensionItemType_RemotelyAccessedDigitalGood: adg.ProductCategoryExt_10100,
		models.ExtensionItemType_RemotelyAccessedDigitalGame: adg.ProductCategoryExt_10200,
		models.ExtensionItemType_GeneralDigitalGood:          adg.ProductCategoryExt_10300,
		models.ExtensionItemType_StreamingAudio:              adg.ProductCategoryExt_10400,
		models.ExtensionItemType_Services:                    adg.ProductCategoryExt_10500,
		models.ExtensionItemType_Subscriptions:               adg.ProductCategoryExt_10600,
		models.ExtensionItemType_Other:                       adg.ProductCategoryExt_10000,
	}

	fulfilledByTwitch := true
	for itemType, prodCategory := range extItemTypeCategory {
		Convey("Verifying the extension iap metadata is valid for itemType "+string(itemType)+" and prodCategory "+prodCategory.ToString(), t, func() {
			extIAP := newTestExtensionIAP()
			extIAP.ItemType = itemType
			extIAP.MultiplePurchases = true

			metadata := extIAP.GetADGMetadata(fulfilledByTwitch)
			So(metadata.ContentType.ToString(), ShouldEqual, extIAPContentType)
			So(metadata.ProductLine.ToString(), ShouldEqual, extIAPProductLineMultiPurchase)
			So(metadata.ProductCategory.ToString(), ShouldEqual, prodCategory.ToString())
			So(metadata.ProductType.ToString(), ShouldEqual, extIAPProductTypeMultiPurchase)

			extIAP.MultiplePurchases = false

			metadata = extIAP.GetADGMetadata(fulfilledByTwitch)
			So(metadata.ContentType.ToString(), ShouldEqual, extIAPContentType)
			So(metadata.ProductLine.ToString(), ShouldEqual, extIAPProductLineNonMultiPurchase)
			So(metadata.ProductCategory.ToString(), ShouldEqual, prodCategory.ToString())
			So(metadata.ProductType.ToString(), ShouldEqual, extIAPProductTypeNonMultiPurchase)
		})
	}
}

func newTestExtensionIAP() models.ExtensionIAP {
	var extIAP models.ExtensionIAP
	err := json.Unmarshal([]byte(newExtIAPDocumentString()), &extIAP)
	if err != nil {
		panic(err)
	}
	return extIAP
}

func newExtIAPDocumentString() string {
	var validExtensionsIAPDocumentFile string = os.Getenv("GOPATH") + "/src/code.justin.tv/commerce/tachanka/config/data/front_end_requests/valid_extension_iap_data.json"
	validJsonFile, err := ioutil.ReadFile(validExtensionsIAPDocumentFile)
	if err != nil {
		panic(err)
	}
	return string(validJsonFile)
}
