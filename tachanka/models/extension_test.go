package models_test

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"reflect"
	"testing"
	"time"

	"code.justin.tv/commerce/tachanka/models"
	"code.justin.tv/commerce/tachanka/models/adg"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/assert"
)

const (
	extId                   string = "id"
	extDeveloperName        string = "Twitch"
	extDeveloperWebsiteUrl  string = "https://www.twitch.tv/"
	extCustomerSupportUrl   string = "http://www.amazon.com"
	extCustomerSupportEmail string = "support@twtich.tv"
	extLocalCodeUS          string = "en-US"
	extLocalCodeDE          string = "de-DE"
	extIconId               string = "amzn1.dex.asset.f0d4f9e62c964c0b92108c453fa70e17"
	extKeyword              string = "game"
	extProductId            string = "extProductId"
	extDraftDocumentId      string = "extDraftDocumentId"
	extLiveId               string = "extLiveId"
	extWorkingSubmissionId  string = "extWorkingSubmissionId"
	extSku                  string = "TestSku1"
	extDisplayName          string = "UAT Game under $40"
	extContentType          string = "extension"
	extProductLine          string = "Twitch:FuelGame"
	extProductCategory      string = "63410000"
	extSubCategory          string = "63410001"
	extproductType          string = "Entitlement"
	extFulfillmentMethod    string = "fulfilled_FuelLauncher_default"
)

func TestExtensionModel(t *testing.T) {
	extension := newTestExtension()

	Convey("Verifying the extension is an extension", t, func() {
		So(extension.GetType(), ShouldEqual, models.ModelType_Extension)
	})

	Convey("Verifying the extension has the expected data", t, func() {
		So(extension.Sku, ShouldEqual, extSku)
		So(extension.DeveloperName, ShouldEqual, extDeveloperName)
		So(extension.DeveloperWebsiteUrl, ShouldEqual, extDeveloperWebsiteUrl)
		So(extension.CustomerSupportUrl, ShouldEqual, extCustomerSupportUrl)
		So(extension.CustomerSupportEmail, ShouldEqual, extCustomerSupportEmail)
		So(extension.LocaleListings, ShouldNotBeEmpty)
		So(len(extension.LocaleListings), ShouldEqual, 2)
		So(extension.LocaleListings[0].Locale, ShouldEqual, extLocalCodeUS)
		So(extension.LocaleListings[0].DisplayName, ShouldEqual, extDisplayName)
		So(extension.LocaleListings[0].Keywords, ShouldNotBeEmpty)
		So(extension.LocaleListings[0].Keywords[0], ShouldEqual, extKeyword)
		So(extension.LocaleListings[1].Locale, ShouldEqual, extLocalCodeDE)
		So(extension.LocaleListings[1].DisplayName, ShouldEqual, extDisplayName)
		So(extension.LocaleListings[1].Keywords, ShouldNotBeEmpty)
		So(extension.LocaleListings[1].Keywords[0], ShouldEqual, extKeyword)
	})

	Convey("Verifying the extension implements ManagedProduct", t, func() {
		var managedProduct models.ManagedProduct = &extension

		// Verify the conversion worked as expected
		eq := reflect.DeepEqual(managedProduct, &extension)
		So(eq, ShouldBeTrue)
		// Verify the managed product fields
		So(managedProduct.GetId(), ShouldEqual, extId)
		So(managedProduct.GetDraftDocumentId(), ShouldEqual, extDraftDocumentId)
		So(managedProduct.GetADGProductId(), ShouldEqual, extProductId)
		So(managedProduct.GetLiveSubmissionId(), ShouldEqual, extLiveId)
		So(managedProduct.GetWorkingSubmissionId(), ShouldEqual, extWorkingSubmissionId)
		// ShouldEqual fails to properly compare these, so DeepEqual is required
		eq = reflect.DeepEqual(managedProduct.GetLastUpdated(), time.Time{})
		So(eq, ShouldBeTrue)
	})

	Convey("Verifying the extension implements ADGSellable", t, func() {
		var adgSellable adg.Sellable = &extension

		// Verify the conversion worked as expected
		eq := reflect.DeepEqual(adgSellable, &extension)
		So(eq, ShouldBeTrue)
		// Verify the managed product fields
		So(adgSellable.GetSKU(), ShouldEqual, extSku)
		So(adgSellable.GetTitle(), ShouldEqual, extDisplayName)
		// ShouldEqual fails to properly compare these, so DeepEqual is required
		eq = reflect.DeepEqual(adgSellable.GetTaxInformation(), adg.TaxInformation{
			MultiplePurchases: false,
			Durable:           true,
			InGameCurrency:    false,
		})
		So(eq, ShouldBeTrue)
		So(adgSellable.GetFulfillmentMethod(), ShouldEqual, extFulfillmentMethod)

		// Verify the ADG metadata
		fulfilledByTwitch := true
		metadata := adgSellable.GetADGMetadata(fulfilledByTwitch)
		So(metadata.ContentType.ToString(), ShouldEqual, extContentType)
		So(metadata.ProductLine.ToString(), ShouldEqual, extProductLine)
		So(metadata.ProductCategory.ToString(), ShouldEqual, extProductCategory)
		So(metadata.Subcategory.ToString(), ShouldEqual, extSubCategory)
		So(metadata.ProductType.ToString(), ShouldEqual, extproductType)
	})
}

func TestGetADGDocumentWithoutMerge(t *testing.T) {
	extension := newTestExtension()

	adgDocument := extension.PopulateADGDocument(adg.ADGDocument{})

	// Assert top level fields
	assert.Equal(t, extDeveloperName, adgDocument.DeveloperInfo.DeveloperName)
	assert.Equal(t, extDeveloperWebsiteUrl, adgDocument.DeveloperWebsiteUrl)
	assert.Equal(t, extCustomerSupportEmail, adgDocument.CustomerSupportEmail)
	assert.Equal(t, extCustomerSupportUrl, adgDocument.CustomerSupportUrl)

	// Assert listings
	ListingsByLocale := adgDocument.ListingsByLocale
	assert.NotEmpty(t, ListingsByLocale)
	assert.Equal(t, extDisplayName, ListingsByLocale[extLocalCodeUS].DisplayName)
	assert.Equal(t, extIconId, ListingsByLocale[extLocalCodeUS].IconId)
	assert.Equal(t, extDisplayName, ListingsByLocale[extLocalCodeUS].LongDescription)
	assert.Equal(t, extDisplayName, ListingsByLocale[extLocalCodeUS].ShortDescription)

	assert.NotEmpty(t, extDisplayName, ListingsByLocale[extLocalCodeUS].Keywords)
	assert.Equal(t, extKeyword, ListingsByLocale[extLocalCodeUS].Keywords[0])
}

func newTestExtension() models.Extension {
	var extension models.Extension
	err := json.Unmarshal([]byte(newExtDocumentString()), &extension)
	if err != nil {
		panic(err)
	}
	extension.SetId(extId)
	extension.SetADGProductId(extProductId)
	extension.SetDraftDocumentId(extDraftDocumentId)
	extension.SetLiveSubmissionId(extLiveId)
	extension.SetWorkingSubmissionId(extWorkingSubmissionId)
	extension.SetLastUpdated(time.Time{})
	return extension
}

func newExtDocumentString() string {
	var validExtensionsDocumentFile string = os.Getenv("GOPATH") + "/src/code.justin.tv/commerce/tachanka/config/data/front_end_requests/valid_extensions_data.json"
	validJsonFile, err := ioutil.ReadFile(validExtensionsDocumentFile)
	if err != nil {
		panic(err)
	}
	return string(validJsonFile)
}
