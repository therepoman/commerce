package models_test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"reflect"
	"testing"
	"time"

	"code.justin.tv/commerce/tachanka/models"
	"code.justin.tv/commerce/tachanka/models/adg"
	"code.justin.tv/commerce/tachanka/models/currency"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	gameId                      string = "id"
	gameTitle                   string = "UAT Game under $40"
	gameProductId               string = "gameProductId"
	gameDraftDocumentId         string = "gameDraftDocumentId"
	gameLiveId                  string = "gameLiveId"
	gameWorkingSubmissionId     string = "gameWorkingSubmissionId"
	gameSku                     string = "TestSku1"
	gameContentType             string = "game"
	gameProductLine             string = "Twitch:FuelGame"
	gameProductCategory         string = "63401000"
	gameSubCategoryFulfilled    string = "63401100"
	gameSubCategoryNotFulfilled string = "63401200"
	gameProductType             string = "Entitlement"
	gameFulfillment             string = "fulfilled_FuelLauncher_default"
)

func TestGameImplementsExpectedInterfaces(t *testing.T) {
	game := newTestGame()

	Convey("Verifying the game is a game", t, func() {
		So(game.GetType(), ShouldEqual, models.ModelType_Game)
	})

	Convey("Verifying the game has the expected data", t, func() {
		// Since there are a LOT of fields here, we perform comparisons against
		// a map[string]interface{} from the same file instead of hardcoding strings
		testMap := newTestMap()

		// Test the struct's strings
		So(game.GetSKU(), ShouldEqual, testMap["sku"])
		So(game.GetFulfillmentMethod(), ShouldEqual, testMap["fulfillmentMethod"])
		So(game.ReleaseDate, ShouldEqual, testMap["releaseDate"])
		So(game.AnnounceDate, ShouldEqual, testMap["announceDate"])
		So(game.PreviousReleaseDate, ShouldEqual, testMap["previousReleaseDate"])
		So(game.DeveloperWebsiteURL, ShouldEqual, testMap["developerWebsiteUrl"])
		So(game.CustomerSupportURL, ShouldEqual, testMap["customerSupportUrl"])
		So(game.BroadcasterVODUrls, ShouldEqualStringLists, testMap["broadcasterVodUrls"])
		So(game.NewsAndReviewsURLs, ShouldEqualStringLists, testMap["newsAndReviewsUrls"])

		// Test locale
		So(len(game.LocaleListings), ShouldEqual, 2)
		So(game.LocaleListings[0], shouldEqualLocale, testMap["localeListings"].([]interface{})[0])
		So(game.LocaleListings[1], shouldEqualLocale, testMap["localeListings"].([]interface{})[1])

		// Test developer info
		So(game.DeveloperName, ShouldEqual, testMap["developerName"])

		// Test category info
		So(game.CategoryInfoId, ShouldEqual, testMap["categoryInfoId"])

		// Test availability info
		So(len(game.AvailabilityInfo), ShouldEqual, 2)
		So(game.AvailabilityInfo[0], shouldEqualAvailabilityEntry, testMap["availabilityInfo"].([]interface{})[0])
		So(game.AvailabilityInfo[1], shouldEqualAvailabilityEntry, testMap["availabilityInfo"].([]interface{})[1])

		// Test content rating info
		So(game.ContentRatingInfo, shouldEqualContentRatingInfo, testMap["contentRatingInfo"])

		// Test stuff formerly in binary info
		So(game, shouldEqualBinaryInfo, testMap)
	})

	Convey("Verifying the game implements ManagedProduct", t, func() {
		var managedProduct models.ManagedProduct = &game

		// Verify the conversion worked as expected
		eq := reflect.DeepEqual(managedProduct, &game)
		So(eq, ShouldBeTrue)
		// Verify the managed product fields
		So(managedProduct.GetId(), ShouldEqual, gameId)
		So(managedProduct.GetDraftDocumentId(), ShouldEqual, gameDraftDocumentId)
		So(managedProduct.GetADGProductId(), ShouldEqual, gameProductId)
		So(managedProduct.GetLiveSubmissionId(), ShouldEqual, gameLiveId)
		So(managedProduct.GetWorkingSubmissionId(), ShouldEqual, gameWorkingSubmissionId)
		// ShouldEqual fails to properly compare these, so DeepEqual is required
		eq = reflect.DeepEqual(managedProduct.GetLastUpdated(), time.Time{})
		So(eq, ShouldBeTrue)
	})

	Convey("Verifying the game implements ADGSellable", t, func() {
		var adgSellable adg.Sellable = &game

		// Verify the conversion worked as expected
		eq := reflect.DeepEqual(adgSellable, &game)
		So(eq, ShouldBeTrue)
		// Verify the managed product fields
		So(adgSellable.GetSKU(), ShouldEqual, gameSku)
		So(adgSellable.GetTitle(), ShouldEqual, gameTitle)
		// ShouldEqual fails to properly compare these, so DeepEqual is required
		eq = reflect.DeepEqual(adgSellable.GetTaxInformation(), adg.TaxInformation{
			MultiplePurchases: false,
			InGameCurrency:    false,
			Durable:           true,
		})
		So(eq, ShouldBeTrue)
		So(adgSellable.GetFulfillmentMethod(), ShouldEqual, gameFulfillment)

		// Verify the ADG metadata when fulfilledByTwitch is true
		fulfilledByTwitch := true
		metadata := adgSellable.GetADGMetadata(fulfilledByTwitch)
		So(metadata.ContentType.ToString(), ShouldEqual, gameContentType)
		So(metadata.ProductLine.ToString(), ShouldEqual, gameProductLine)
		So(metadata.ProductCategory.ToString(), ShouldEqual, gameProductCategory)
		So(metadata.Subcategory.ToString(), ShouldEqual, gameSubCategoryFulfilled)
		So(metadata.ProductType.ToString(), ShouldEqual, gameProductType)

		// Verify the ADG metadata when fulfilledByTwitch is false
		fulfilledByTwitch = false
		metadata = adgSellable.GetADGMetadata(fulfilledByTwitch)
		So(metadata.ContentType.ToString(), ShouldEqual, gameContentType)
		So(metadata.ProductLine.ToString(), ShouldEqual, gameProductLine)
		So(metadata.ProductCategory.ToString(), ShouldEqual, gameProductCategory)
		So(metadata.Subcategory.ToString(), ShouldEqual, gameSubCategoryNotFulfilled)
		So(metadata.ProductType.ToString(), ShouldEqual, gameProductType)
	})
}

func shouldEqualBinaryInfo(actual interface{}, expected ...interface{}) string {

	actualBinaryInfo, ok := actual.(models.Game)
	if !ok {
		return "The actual BinaryInfo could not be converted properly"
	}

	convertedDataMap, ok := expected[0].(map[string]interface{})
	if !ok {
		return "The expected map could not be converted properly"
	}

	if convertedDataMap["requiresContinuousConnectivity"] != actualBinaryInfo.RequiresContinuousConnectivity {
		return fmt.Sprintf("BinaryInfo requiresContinuousConnectivity does not match. Wanted: %v but Got: %v", convertedDataMap["requiresContinuousConnectivity"], actualBinaryInfo.RequiresContinuousConnectivity)
	}

	if convertedDataMap["numberOfPlayersSupported"] != actualBinaryInfo.NumberOfPlayersSupported {
		return fmt.Sprintf("BinaryInfo numberOfPlayersSupported does not match. Wanted: %v but Got: %v", convertedDataMap["numberOfPlayersSupported"], actualBinaryInfo.NumberOfPlayersSupported)
	}

	if convertedDataMap["version"] != actualBinaryInfo.Version {
		return fmt.Sprintf("BinaryInfo Version does not match. Wanted: %v but Got: %v", convertedDataMap["version"], actualBinaryInfo.Version)
	}

	if convertedDataMap["supportedControllers"] != actualBinaryInfo.SupportedControllers {
		return fmt.Sprintf("BinaryInfo SupportedControllers does not match. Wanted: %v but Got: %v", convertedDataMap["supportedControllers"], actualBinaryInfo.SupportedControllers)
	}

	languageComparison := shouldEqualLanguages(actualBinaryInfo.LanguageSupport, convertedDataMap["languageSupport"])
	if languageComparison != "" {
		return fmt.Sprintf("BinaryInfo LanguageSupport does not match. %v", languageComparison)
	}

	expectedRequirementsInfo, ok := convertedDataMap["pcRequirementsInfo"].(map[string]interface{})
	if !ok {
		return "The expected RequirementsInfo could not be converted properly"
	}

	if expectedRequirementsInfo["otherRequirements"] != actualBinaryInfo.PCRequirementsInfo.OtherRequirements {
		return fmt.Sprintf("PCRequirements OtherRequirements does not match. Wanted: %v but Got: %v", expectedRequirementsInfo["otherRequirements"], actualBinaryInfo.PCRequirementsInfo.OtherRequirements)
	}

	pcComparison := shouldEqualPCRequirements(actualBinaryInfo.PCRequirementsInfo.MinimumRequirements, expectedRequirementsInfo["minimumRequirements"])
	if pcComparison != "" {
		return fmt.Sprintf("PCRequirements MinimumRequirements does not match. %v", pcComparison)
	}

	pcComparison = shouldEqualPCRequirements(actualBinaryInfo.PCRequirementsInfo.RecommendedRequirements, expectedRequirementsInfo["recommendedRequirements"])
	if pcComparison != "" {
		return fmt.Sprintf("PCRequirements RecommendedRequirements does not match. %v", pcComparison)
	}

	return ""
}

func shouldEqualLanguages(actual interface{}, expected ...interface{}) string {

	actualLanguages, ok := actual.([]models.Language)
	if !ok {
		return "The actual languages could not be converted properly"
	}

	expectedLanguageSupportEntry, ok := expected[0].([]interface{})
	if !ok {
		return "The expected languageSupport could not be converted properly"
	}

	if len(actualLanguages) != len(expectedLanguageSupportEntry) {
		return fmt.Sprintf("The number of expected languages does not match the actual number of languages supported. Expected: %v but Got: %v", len(expectedLanguageSupportEntry), len(actualLanguages))
	}

	for i, v := range expectedLanguageSupportEntry {
		expectedMap, ok := v.(map[string]interface{})
		if !ok {
			return "One of the expected language maps could not be converted properly"
		}

		if expectedMap["languageCode"] != actualLanguages[i].LanguageCode {
			return fmt.Sprintf("languageSupport languageCode does not match. Wanted: %v but Got: %v", expectedMap["languageCode"], actualLanguages[i].LanguageCode)
		}

		if expectedMap["instructions"] != actualLanguages[i].Instructions {
			return fmt.Sprintf("languageSupport instructions does not match. Wanted: %v but Got: %v", expectedMap["instructions"], actualLanguages[i].Instructions)
		}

		if expectedMap["subtitlesAndCaptions"] != actualLanguages[i].SubtitlesAndCaptions {
			return fmt.Sprintf("languageSupport subtitlesAndCaptions does not match. Wanted: %v but Got: %v", expectedMap["subtitlesAndCaptions"], actualLanguages[i].SubtitlesAndCaptions)
		}

		if expectedMap["audio"] != actualLanguages[i].Audio {
			return fmt.Sprintf("languageSupport audio does not match. Wanted: %v but Got: %v", expectedMap["audio"], actualLanguages[i].Audio)
		}
	}
	return ""
}

func shouldEqualPCRequirements(actual interface{}, expected ...interface{}) string {

	actualPCRequirements, ok := actual.(models.PCRequirements)
	if !ok {
		return "The actual PCRequirements could not be converted properly"
	}

	convertedDataMap, ok := expected[0].(map[string]interface{})
	if !ok {
		return "The expected map could not be converted properly"
	}

	if convertedDataMap["videoCard"] != actualPCRequirements.VideoCard {
		return fmt.Sprintf("ActualPCRequirements videoCard does not match. Wanted: %v but Got: %v", convertedDataMap["videoCard"], actualPCRequirements.VideoCard)
	}

	if convertedDataMap["ram"] != actualPCRequirements.RAM {
		return fmt.Sprintf("ActualPCRequirements ram does not match. Wanted: %v but Got: %v", convertedDataMap["ram"], actualPCRequirements.RAM)
	}

	if convertedDataMap["hardDriveSpace"] != actualPCRequirements.HardDriveSpace {
		return fmt.Sprintf("ActualPCRequirements hardDriveSpace does not match. Wanted: %v but Got: %v", convertedDataMap["hardDriveSpace"], actualPCRequirements.HardDriveSpace)
	}

	if convertedDataMap["processor"] != actualPCRequirements.Processor {
		return fmt.Sprintf("ActualPCRequirements processor does not match. Wanted: %v but Got: %v", convertedDataMap["processor"], actualPCRequirements.Processor)
	}

	if convertedDataMap["os"] != actualPCRequirements.OS {
		return fmt.Sprintf("ActualPCRequirements os does not match. Wanted: %v but Got: %v", convertedDataMap["os"], actualPCRequirements.OS)
	}

	if convertedDataMap["directX"] != actualPCRequirements.DirectX {
		return fmt.Sprintf("ActualPCRequirements directX does not match. Wanted: %v but Got: %v", convertedDataMap["directX"], actualPCRequirements.DirectX)
	}

	return ""
}

func shouldEqualContentRatingInfo(actual interface{}, expected ...interface{}) string {

	actualContentRatingInfo, ok := actual.(models.ContentRatingInfo)
	if !ok {
		return "The actual ContentRatingInfo could not be converted properly"
	}

	convertedDataMap, ok := expected[0].(map[string]interface{})
	if !ok {
		return "The expected map could not be converted properly"
	}

	if convertedDataMap["pegiRating"] != actualContentRatingInfo.PEGIRating {
		return fmt.Sprintf("ContentRatingInfo PEGIRating does not match. Wanted: %v but Got: %v", convertedDataMap["directX"], actualContentRatingInfo.PEGIRating)
	}

	expectedESRBEntry, ok := convertedDataMap["esrbRatingInfo"].(map[string]interface{})
	if !ok {
		return "The expected ESRBEntry could not be converted properly"
	}

	if expectedESRBEntry["ratingCategory"] != actualContentRatingInfo.ESRBRatingInfo.RatingCategory {
		return fmt.Sprintf("ContentRatingInfo ESRBRatingInfo RatingCategory does not match. Wanted: %v but Got: %v", expectedESRBEntry["ratingCategory"], actualContentRatingInfo.ESRBRatingInfo.RatingCategory)
	}

	return ""
}

func shouldEqualAvailabilityEntry(actual interface{}, expected ...interface{}) string {
	actualAvailabilityEntry, ok := actual.(models.AvailabilityEntry)
	if !ok {
		return "The actual AvailabilityEntry could not be converted properly"
	}

	convertedDataMap, ok := expected[0].(map[string]interface{})
	if !ok {
		return "The expected map could not be converted properly"
	}

	if convertedDataMap["CountryCode"] != actualAvailabilityEntry.CountryCode {
		return fmt.Sprintf("AvailabilityEntry CountryCode does not match. Wanted: %v but Got: %v", convertedDataMap["CountryCode"], actualAvailabilityEntry.CountryCode)
	}

	// Unfortunately, we can't cast directly to an int (or a string)
	expectedPriceFloat, ok := convertedDataMap["Price"].(float64)
	if !ok {
		return "The expected price could not be converted properly (to a float64)"
	}

	expectedPriceAmount := currency.Amount(expectedPriceFloat)

	if expectedPriceAmount != actualAvailabilityEntry.Price {
		return fmt.Sprintf("AvailabilityEntry Price does not match. Wanted: %v but Got: %v", expectedPriceFloat, actualAvailabilityEntry.Price)
	}

	if convertedDataMap["Available"] != actualAvailabilityEntry.Available {
		return fmt.Sprintf("AvailabilityEntry Available does not match. Wanted: %v but Got: %v", convertedDataMap["Available"], actualAvailabilityEntry.Available)
	}

	return ""
}

func shouldEqualLocale(actual interface{}, expected ...interface{}) string {

	actualLocale, ok := actual.(models.Locale)
	if !ok {
		return "The actual locale could not be converted properly"
	}

	convertedDataMap, ok := expected[0].(map[string]interface{})
	if !ok {
		return "The expected map could not be converted properly"
	}

	if convertedDataMap["locale"] != actualLocale.Locale {
		return fmt.Sprintf("Locale id does not match. Wanted: %v but Got: %v", convertedDataMap["id"], actualLocale.Locale)
	}

	if convertedDataMap["displayName"] != actualLocale.DisplayName {
		return fmt.Sprintf("Locale displayName does not match. Wanted: %v but Got: %v", convertedDataMap["displayName"], actualLocale.DisplayName)
	}

	if convertedDataMap["shortDescription"] != actualLocale.ShortDescription {
		return fmt.Sprintf("Locale shortDescription does not match. Wanted: %v but Got: %v", convertedDataMap["shortDescription"], actualLocale.ShortDescription)
	}

	if convertedDataMap["longDescription"] != actualLocale.LongDescription {
		return fmt.Sprintf("Locale longDescription does not match. Wanted: %v but Got: %v", convertedDataMap["longDescription"], actualLocale.LongDescription)
	}

	if convertedDataMap["gameEdition"] != actualLocale.GameEdition {
		return fmt.Sprintf("Locale gameEdition does not match. Wanted: %v but Got: %v", convertedDataMap["gameEdition"], actualLocale.GameEdition)
	}

	if convertedDataMap["gameEditionDetails"] != actualLocale.GameEditionDetails {
		return fmt.Sprintf("Locale gameEditionDetails does not match. Wanted: %v but Got: %v", convertedDataMap["gameEditionDetails"], actualLocale.GameEditionDetails)
	}

	if convertedDataMap["iconId"] != actualLocale.IconId {
		return fmt.Sprintf("Locale iconId does not match. Wanted: %v but Got: %v", convertedDataMap["iconId"], actualLocale.IconId)
	}

	if convertedDataMap["pageBackgroundId"] != actualLocale.PageBackgroundId {
		return fmt.Sprintf("Locale pageBackgroundId does not match. Wanted: %v but Got: %v", convertedDataMap["pageBackgroundId"], actualLocale.PageBackgroundId)
	}

	if convertedDataMap["eulaUrl"] != actualLocale.EulaUrl {
		return fmt.Sprintf("Locale eulaUrl does not match. Wanted: %v but Got: %v", convertedDataMap["eulaUrl"], actualLocale.EulaUrl)
	}

	if convertedDataMap["releaseNotesId"] != actualLocale.ReleaseNotesId {
		return fmt.Sprintf("Locale releaseNotesId does not match. Wanted: %v but Got: %v", convertedDataMap["releaseNotesId"], actualLocale.ReleaseNotesId)
	}

	listComparison := ShouldEqualStringLists(actualLocale.ProductFeatures, convertedDataMap["productFeatures"])
	if listComparison != "" {
		return fmt.Sprintf("Locale productFeatures does not match. %v", listComparison)
	}

	listComparison = ShouldEqualStringLists(actualLocale.Keywords, convertedDataMap["keywords"])
	if listComparison != "" {
		return fmt.Sprintf("Locale keywords does not match. %v", listComparison)
	}

	listComparison = ShouldEqualStringLists(actualLocale.ScreenshotIds, convertedDataMap["screenshotIds"])
	if listComparison != "" {
		return fmt.Sprintf("Locale screenshotIds does not match. %v", listComparison)
	}

	return ""
}

func ShouldEqualStringLists(actual interface{}, expected ...interface{}) string {
	actualList, ok := actual.([]string)
	if !ok {
		return "The actual list could not be converted properly"
	}

	convertedDataMap, ok := expected[0].([]interface{})
	if !ok {
		return "The expected list could not be converted properly"
	}

	if len(actualList) != len(convertedDataMap) {
		return fmt.Sprintf("List lengths do not match. Expected: %v but Got: %v", len(actualList), len(convertedDataMap))
	}

	for i, v := range convertedDataMap {
		if v != actualList[i] {
			return fmt.Sprintf("List elements do not match. Wanted: %v but Got: %v", v, actualList[i])
		}
	}
	return ""
}

func newTestMap() map[string]interface{} {
	var testMap map[string]interface{}
	err := json.Unmarshal([]byte(newGameDocumentString()), &testMap)
	if err != nil {
		panic(err)
	}
	return testMap
}

func newTestGame() models.Game {
	var game models.Game
	err := json.Unmarshal([]byte(newGameDocumentString()), &game)
	if err != nil {
		panic(err)
	}
	game.SetId(gameId)
	game.SetADGProductId(gameProductId)
	game.SetDraftDocumentId(gameDraftDocumentId)
	game.SetLiveSubmissionId(gameLiveId)
	game.SetWorkingSubmissionId(gameWorkingSubmissionId)
	game.SetLastUpdated(time.Time{})
	return game
}

func newGameDocumentString() string {
	var validGameDocumentFile string = os.Getenv("GOPATH") + "/src/code.justin.tv/commerce/tachanka/config/data/front_end_requests/valid_game_data_1.json"
	validJsonFile, err := ioutil.ReadFile(validGameDocumentFile)
	if err != nil {
		panic(err)
	}
	return string(validJsonFile)
}
