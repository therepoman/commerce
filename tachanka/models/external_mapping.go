package models

import (
	"errors"
	"time"
)

const (
	EM_ADGProductId ExternalMappingTypeEnum = "ADG_PRODUCT_ID"
	EM_ADGSku       ExternalMappingTypeEnum = "ADG_SKU"
	EM_ADGVendorId  ExternalMappingTypeEnum = "ADG_VENDOR_ID"
	EM_ADGDomainId  ExternalMappingTypeEnum = "ADG_DOMAIN_ID"
	EM_ADGAsin      ExternalMappingTypeEnum = "ADG_ASIN"
	EM_UNKNOWN      ExternalMappingTypeEnum = "NIL"
)

type (
	ExternalMappingTypeEnum string

	ExternalMapping struct {
		ParentId    string                  `dynamo:"parent-id,hash"`
		Type        ExternalMappingTypeEnum `dynamo:"type,range"`
		Value       string                  `dynamo:"value"`
		LastUpdated time.Time               `dynamo:"last_updated"`
	}
)

func NewExternalMapping(managedProductId string, externalMappingType ExternalMappingTypeEnum, value string) *ExternalMapping {
	return &ExternalMapping{
		ParentId:    managedProductId,
		Type:        externalMappingType,
		Value:       value,
		LastUpdated: time.Now(),
	}
}

func StringToExternalMappingType(s string) (ExternalMappingTypeEnum, error) {
	if s == string(EM_ADGProductId) {
		return EM_ADGProductId, nil
	} else if s == string(EM_ADGVendorId) {
		return EM_ADGVendorId, nil
	} else if s == string(EM_ADGDomainId) {
		return EM_ADGDomainId, nil
	} else if s == string(EM_ADGAsin) {
		return EM_ADGAsin, nil
	} else if s == string(EM_ADGSku) {
		return EM_ADGSku, nil
	}

	return EM_UNKNOWN, errors.New("Unable to determine ExternalProductType")
}
