package models

import (
	"errors"
	"time"
)

type ManagedProductType string

const (
	UnknownType   ManagedProductType = "UNKNOWN"
	DefaultLocale string             = "en-US"
)

func StringToManagedProductType(s string) (ManagedProductType, error) {
	switch s {
	case string(ModelType_Game):
		return ModelType_Game, nil
	case string(ModelType_InGameContent):
		return ModelType_InGameContent, nil
	case string(ModelType_Extension):
		return ModelType_Extension, nil
	case string(ModelType_ExtensionInAppPurchase):
		return ModelType_ExtensionInAppPurchase, nil
	default:
		return UnknownType, errors.New("Unable to determine ManagedProductType")
	}
}

type ManagedProduct interface {
	MetadataManager
	GetType() ManagedProductType
	SetData(string) error
	GetData() (string, error)
}

type MetadataManager interface {
	GetId() string
	SetId(string)
	GetDraftDocumentId() string
	SetDraftDocumentId(string)
	GetADGProductId() string
	SetADGProductId(string)
	GetLiveSubmissionId() string
	SetLiveSubmissionId(string)
	GetWorkingSubmissionId() string
	SetWorkingSubmissionId(string)
	GetLastUpdated() time.Time
	SetLastUpdated(time.Time)
}
