package models

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

const (
	SubmissionStatus_InProgress SubmissionStatus = "IN_PROGRESS"
	SubmissionStatus_Complete   SubmissionStatus = "COMPLETE"
	SubmissionStatus_Failed     SubmissionStatus = "FAILED"
)

type (
	SubmissionStatus string

	Submission struct {
		Id               string           `dynamo:"id,hash"`
		ManagedProductId string           `dynamo:"managed_product_id" index:"managed_product_id-index,hash"`
		DocumentId       string           `dynamo:"document"`
		Status           SubmissionStatus `dynamo:"status"`
		SubmissionSteps  []SubmissionStep `dynamo:"submission_steps"`
		LastUpdated      time.Time        `dynamo:"last_updated"`
		CreatedDate      time.Time        `dynamo:"created_date"`
	}
)

func NewSubmission(managedProductId string, documentId string) *Submission {
	return &Submission{
		Id:               uuid.NewV4().String(),
		ManagedProductId: managedProductId,
		DocumentId:       documentId,
		Status:           SubmissionStatus_InProgress,
	}
}
