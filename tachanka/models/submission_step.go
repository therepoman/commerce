package models

import (
	"errors"
	"time"

	uuid "github.com/satori/go.uuid"
)

const (
	SubmissionStepType_AdgCreateProduct  SubmissionStepType = "ADG_CREATE_PRODUCT"
	SubmissionStepType_AdgCreateRevision SubmissionStepType = "ADG_CREATE_REVISION"
	SubmissionStepType_AdgSave           SubmissionStepType = "ADG_SAVE"
	SubmissionStepType_AdgSubmit         SubmissionStepType = "ADG_SUBMIT"
	SubmissionStepType_Unknown           SubmissionStepType = "UNKNOWN"

	SubmissionStepStatus_Complete SubmissionStepStatus = "COMPLETE"
	SubmissionStepStatus_Running  SubmissionStepStatus = "RUNNING"
	SubmissionStepStatus_Waiting  SubmissionStepStatus = "WAITING"
	SubmissionStepStatus_Error    SubmissionStepStatus = "ERROR"
	SubmissionStepStatus_Unknown  SubmissionStepStatus = "UNKNOWN"
)

type (
	SubmissionStepType   string
	SubmissionStepStatus string

	SubmissionStep struct {
		Id          string               `dynamo:"id,hash"`
		Type        SubmissionStepType   `dynamo:"type"`
		Status      SubmissionStepStatus `dynamo:"status"`
		CreatedDate time.Time            `dynamo:"created_date"`
	}
)

func NewSubmissionStep(stepType SubmissionStepType, stepStatus SubmissionStepStatus) *SubmissionStep {
	return &SubmissionStep{
		Id:          uuid.NewV4().String(),
		Type:        stepType,
		Status:      stepStatus,
		CreatedDate: time.Now(),
	}
}

func StringToSubmissionStepStatus(s string) (SubmissionStepStatus, error) {
	if s == string(SubmissionStepStatus_Complete) {
		return SubmissionStepStatus_Complete, nil
	} else if s == string(SubmissionStepStatus_Running) {
		return SubmissionStepStatus_Running, nil
	} else if s == string(SubmissionStepStatus_Waiting) {
		return SubmissionStepStatus_Waiting, nil
	} else if s == string(SubmissionStepStatus_Error) {
		return SubmissionStepStatus_Error, nil
	}

	return SubmissionStepStatus_Unknown, errors.New("Unable to determine SubmissionStepStatus")
}
