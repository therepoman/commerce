package models

import (
	"encoding/json"

	"errors"

	"code.justin.tv/commerce/tachanka/models/adg"
	"code.justin.tv/commerce/tachanka/models/currency"
	"github.com/Sirupsen/logrus"
)

type ExtensionItemType string

const (
	CategoryInfo_Other                            string             = "twitch-extension-item-other"
	CategoryInfo_RemotelyAccessedDigitalGood      string             = "twitch-extension-item-remote-digital-game"
	CategoryInfo_RemotelyAccessedDigitalGame      string             = "twitch-extension-item-remote-digital-good"
	CategoryInfo_GeneralDigitalGood               string             = "twitch-extension-item-general-digital-good"
	CategoryInfo_StreamingAudio                   string             = "twitch-extension-item-streaming-audio"
	CategoryInfo_Services                         string             = "twitch-extension-item-services"
	CategoryInfo_Subscriptions                    string             = "twitch-extension-item-subscription"
	ExtensionItemType_Other                       ExtensionItemType  = "Other"
	ExtensionItemType_RemotelyAccessedDigitalGood ExtensionItemType  = "RemotelyAccessedDigitalGood"
	ExtensionItemType_RemotelyAccessedDigitalGame ExtensionItemType  = "RemotelyAccessedDigitalGame"
	ExtensionItemType_GeneralDigitalGood          ExtensionItemType  = "GeneralDigitalGood"
	ExtensionItemType_StreamingAudio              ExtensionItemType  = "StreamingAudio"
	ExtensionItemType_Services                    ExtensionItemType  = "Services"
	ExtensionItemType_Subscriptions               ExtensionItemType  = "Subscription"
	ModelType_ExtensionInAppPurchase              ManagedProductType = "EXTENSION_IAP"
	Extension_IAP_Fulfillment_Method              string             = "fulfilled_FuelLauncher_default"
)

var (
	TypeToCategoryInfoMapping = map[ExtensionItemType]string{
		ExtensionItemType_Other:                       CategoryInfo_Other,
		ExtensionItemType_RemotelyAccessedDigitalGood: CategoryInfo_RemotelyAccessedDigitalGood,
		ExtensionItemType_RemotelyAccessedDigitalGame: CategoryInfo_RemotelyAccessedDigitalGame,
		ExtensionItemType_GeneralDigitalGood:          CategoryInfo_GeneralDigitalGood,
		ExtensionItemType_StreamingAudio:              CategoryInfo_StreamingAudio,
		ExtensionItemType_Services:                    CategoryInfo_Services,
		ExtensionItemType_Subscriptions:               CategoryInfo_Subscriptions,
	}
)

type (
	ExtensionIAPListing struct {
		BoxshotId        string   `json:"boxShotId"`
		Locale           string   `json:"locale"`
		DisplayName      string   `json:"displayName"`
		ShortDescription string   `json:"shortDescription"`
		LongDescription  string   `json:"longDescription"`
		Keywords         []string `json:"keywords"`
	}

	ExtensionIAPCountry struct {
		CountryCode string          `json:"countryCode"`
		Available   bool            `json:"available"`
		Price       currency.Amount `json:"price"`
	}

	ExtensionIAP struct {
		ManagedProductMetadata
		SKU               string                `json:"sku"`
		MultiplePurchases bool                  `json:"multiplePurchases"`
		Durable           bool                  `json:"durable"`
		DefaultPrice      currency.Amount       `json:"defaultPrice"`
		AvailabilityInfo  []ExtensionIAPCountry `json:"availabilityInfo"`
		LocaleListings    []ExtensionIAPListing `json:"localeListings"`
		ItemType          ExtensionItemType     `json:"itemType"`
	}
)

func (extensionIAP *ExtensionIAP) GetType() ManagedProductType {
	return ModelType_ExtensionInAppPurchase
}

func (extensionIAP *ExtensionIAP) SetData(newData string) error {
	err := json.Unmarshal([]byte(newData), &extensionIAP)
	if err != nil {
		return err
	}
	return nil
}

func (extensionIAP *ExtensionIAP) GetData() (string, error) {
	iapData, err := json.Marshal(extensionIAP)
	if err != nil {
		return "", err
	}
	return string(iapData), nil
}

func (extensionIAP *ExtensionIAP) GetSKU() string {
	return extensionIAP.SKU
}

func (extensionIAP *ExtensionIAP) GetTitle() string {
	for _, locale := range extensionIAP.LocaleListings {
		if locale.Locale == DefaultLocale {
			return locale.DisplayName
		}
	}
	return ""
}

func (extensionIAP ExtensionIAP) GetADGMetadata(fulfilledByTwitch bool) adg.Metadata {
	var gameMetadata adg.Metadata
	gameMetadata.ContentType = adg.ContentType_Extension
	if extensionIAP.MultiplePurchases {
		gameMetadata.ProductLine = adg.ProductLine_Consumable
		gameMetadata.ProductType = adg.ProductType_Consumable
	} else {
		gameMetadata.ProductLine = adg.ProductLine_Entitlement
		gameMetadata.ProductType = adg.ProductType_Entitlement
	}
	gameMetadata.ProductCategory = extensionIAP.getExtensionIAPProductCategory()
	gameMetadata.Subcategory = adg.Subcategory(gameMetadata.ProductCategory + adg.Subcategory_Extensions)
	return gameMetadata
}

func (extension *ExtensionIAP) GetFulfillmentMethod() string {
	return Extension_IAP_Fulfillment_Method
}

func (extensionIAP *ExtensionIAP) GetTaxInformation() adg.TaxInformation {
	return adg.TaxInformation{
		MultiplePurchases: extensionIAP.MultiplePurchases,
		Durable:           extensionIAP.Durable,
		InGameCurrency:    false,
	}
}

func (extensionIAP *ExtensionIAP) PopulateADGSubmission(submission adg.ADGSubmission) (*adg.ADGSubmission, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":        "ExtensionIAP.PopulateADGSubmission",
		"MangedProductId": extensionIAP.Id,
	})
	log.Debug("Entering")

	// Product Info
	productInfo := &submission.Document.ProductInfo
	categoryId, ok := TypeToCategoryInfoMapping[extensionIAP.ItemType]
	if !ok {
		logrus.Warn("Unable to determine category info. Unknown item type: ", extensionIAP.ItemType)
		return nil, errors.New("Error creating submission. Unrecognized item type.")
	}
	productInfo.CategoryId = categoryId
	productInfo.RefinementIds = []string{categoryId}

	// Litsings
	listings := submission.Document.Listings
	if listings == nil {
		listings = make(map[string]adg.Listing)
	}
	for _, listing := range extensionIAP.LocaleListings {
		adgListing := adg.Listing{
			DisplayTitle:         listing.DisplayName,
			Keywords:             listing.Keywords,
			ShortDescription:     listing.ShortDescription,
			LongDescription:      listing.LongDescription,
			BoxShotAssetId:       listing.BoxshotId,
			IconAssetId:          listing.BoxshotId,
			ScreenshotAssetIds:   adg.DefaultScreenshotIds,
			GameEdition:          adg.NotApplicable,
			GameEditionDetails:   []string{adg.NotApplicable},
			Features:             []string{adg.NotApplicable},
			SupportedControllers: []string{adg.NotApplicable},
			PCRequirements:       adg.NewDefaultPCRequirements(),
		}

		listings[listing.Locale] = adgListing
	}
	submission.Document.Listings = listings

	// Offerings and Version For Country
	var offerings []adg.Offering
	var versionForCountry []string

	for _, country := range extensionIAP.AvailabilityInfo {
		if country.Available {
			versionForCountry = append(versionForCountry, country.CountryCode)
			offerings = append(offerings, adg.Offering{
				AnnounceDate:        adg.DefaultExtensionReleaseDate,
				ReleaseDate:         adg.DefaultExtensionReleaseDate,
				PreviousReleaseDate: adg.DefaultExtensionReleaseDate,
				PublicationDate:     adg.DefaultExtensionReleaseDate,
				Countries:           []string{country.CountryCode},
				Pricing: map[string]adg.ADGPrice{
					country.CountryCode: adg.ADGPrice{
						Currency: adg.DefaultCurrencyCode,
						Price:    float64(country.Price),
					}},
			})
		}
	}

	submission.Document.Offerings = offerings
	submission.Document.ProductInfo.VersionForCountry = versionForCountry

	log.WithField("Submission", submission).Debug("Exiting")

	return &submission, nil
}

func (extensionIAP ExtensionIAP) PopulateADGDocument(adgDocument adg.ADGDocument) adg.ADGDocument {
	adgDocument.FulfillmentMethod = Extension_IAP_Fulfillment_Method

	if adgDocument.ListingsByLocale == nil {
		logrus.Warn("AdgDocument listings by locale map empty")
		adgDocument.ListingsByLocale = make(map[string]adg.LocaleListing)
	}

	for _, listing := range extensionIAP.LocaleListings {
		docLocale, ok := adgDocument.ListingsByLocale[listing.Locale]
		if !ok {
			// This is gonna fail validation. RIP
			docLocale = adg.LocaleListing{}
		}
		docLocale.DisplayName = listing.DisplayName
		docLocale.Keywords = listing.Keywords
		docLocale.BoxshotId = listing.BoxshotId
		docLocale.ShortDescription = listing.ShortDescription
		docLocale.LongDescription = listing.LongDescription
		docLocale.ProductFeatures = []string{listing.DisplayName}
		adgDocument.ListingsByLocale[listing.Locale] = docLocale
	}

	pricesByCountry := make(map[string]adg.Price)
	for _, country := range extensionIAP.AvailabilityInfo {
		if country.Available {
			adgDocument.AvailabilityInfo.AvailableCountries = append(adgDocument.AvailabilityInfo.AvailableCountries, country.CountryCode)
			price := adg.Price{
				CurrencyCode: adg.DefaultCurrencyCode,
				Amount:       country.Price,
			}
			pricesByCountry[country.CountryCode] = price
		}
	}
	if adgDocument.PricingInfo.PricesByMarketplaceAndCountry == nil {
		logrus.Warn("AdgDocument prices by marketplace map empty")
		adgDocument.PricingInfo.PricesByMarketplaceAndCountry = make(map[string]adg.MarketplacePrices)
	}
	adgDocument.PricingInfo.PricesByMarketplaceAndCountry[adg.DefaultMarketplace] = adg.MarketplacePrices{
		DefaultListPrice: adg.Price{
			CurrencyCode: adg.DefaultCurrencyCode,
			Amount:       extensionIAP.DefaultPrice,
		},
		PricesByCountry: pricesByCountry,
	}

	// CategoryInfo
	categoryId, ok := TypeToCategoryInfoMapping[extensionIAP.ItemType]
	if ok {
		adgDocument.CategoryInfo.CategoryId = categoryId
		adgDocument.CategoryInfo.RefinementIds = []string{categoryId}
	} else {
		logrus.Warn("Unable to determine category info. Unknown item type: ", extensionIAP.ItemType)
	}

	return adgDocument
}

func (extensionIAP ExtensionIAP) getExtensionIAPProductCategory() adg.ProductCategory {
	switch extensionIAP.ItemType {
	case ExtensionItemType_RemotelyAccessedDigitalGood:
		return adg.ProductCategoryExt_10100
	case ExtensionItemType_RemotelyAccessedDigitalGame:
		return adg.ProductCategoryExt_10200
	case ExtensionItemType_GeneralDigitalGood:
		return adg.ProductCategoryExt_10300
	case ExtensionItemType_StreamingAudio:
		return adg.ProductCategoryExt_10400
	case ExtensionItemType_Services:
		return adg.ProductCategoryExt_10500
	case ExtensionItemType_Subscriptions:
		return adg.ProductCategoryExt_10600
	default:
		return adg.ProductCategoryExt_10000
	}
}

func NewExtensionIAP(metadata ManagedProductMetadata) *ExtensionIAP {
	defaultLocale := ExtensionIAPListing{
		Locale:   "en-US",
		Keywords: make([]string, 0),
	}

	sellableCountries := adg.GetAllSellableCountries()
	availabilityInfo := make([]ExtensionIAPCountry, len(sellableCountries))
	for i, countryCode := range sellableCountries {
		availabilityInfo[i] = ExtensionIAPCountry{
			Available:   false,
			CountryCode: countryCode,
			Price:       0,
		}
	}

	return &ExtensionIAP{
		AvailabilityInfo:       availabilityInfo,
		LocaleListings:         []ExtensionIAPListing{defaultLocale},
		ManagedProductMetadata: metadata,
		ItemType:               ExtensionItemType_GeneralDigitalGood,
	}
}
