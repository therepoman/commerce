package models

import (
	"encoding/json"

	"code.justin.tv/commerce/tachanka/models/adg"
	"github.com/Sirupsen/logrus"
)

const (
	ModelType_Extension          ManagedProductType = "EXTENSION"
	Extension_Fulfillment_Method string             = "fulfilled_FuelLauncher_default"
)

type Listing struct {
	Locale      string   `json:"locale"`
	DisplayName string   `json:"displayName"`
	Keywords    []string `json:"keywords"`
}

type Extension struct {
	ManagedProductMetadata
	Sku                  string    `json:"sku"`
	DeveloperName        string    `json:"developerName"`
	DeveloperWebsiteUrl  string    `json:"developerWebsiteUrl"`
	CustomerSupportUrl   string    `json:"customerSupportUrl"`
	CustomerSupportEmail string    `json:"customerSupportEmail"`
	LocaleListings       []Listing `json:"localeListings"`
}

func (extension *Extension) SetData(newData string) error {
	err := json.Unmarshal([]byte(newData), extension)
	if err != nil {
		return err
	}
	return nil
}

func (extension *Extension) GetData() (string, error) {
	extData, err := json.Marshal(extension)
	if err != nil {
		return "", err
	}
	return string(extData), nil
}

func (extension Extension) GetType() ManagedProductType {
	return ModelType_Extension
}

func (extension *Extension) GetSKU() string {
	return extension.Sku
}

func (extension *Extension) GetTitle() string {
	for _, locale := range extension.LocaleListings {
		if locale.Locale == DefaultLocale {
			return locale.DisplayName
		}
	}
	return ""
}

func (extension *Extension) GetFulfillmentMethod() string {
	return Extension_Fulfillment_Method
}

func (extension *Extension) GetTaxInformation() adg.TaxInformation {
	return adg.TaxInformation{
		MultiplePurchases: false,
		InGameCurrency:    false,
		Durable:           true,
	}
}

func NewExtension(metadata ManagedProductMetadata) *Extension {
	defaultListing := Listing{
		Locale:   "en-US",
		Keywords: make([]string, 0),
	}

	return &Extension{
		ManagedProductMetadata: metadata,
		LocaleListings:         []Listing{defaultListing},
	}
}

func (extension *Extension) PopulateADGSubmission(submission adg.ADGSubmission) (*adg.ADGSubmission, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":        "Extension.PopulateADGSubmission",
		"MangedProductId": extension.Id,
	})
	log.Debug("Entering")

	// Product Info
	productInfo := &submission.Document.ProductInfo
	productInfo.CustomerSupportEmailAddress = extension.CustomerSupportEmail
	productInfo.CustomerSupportUrl = extension.CustomerSupportUrl
	productInfo.DeveloperName = extension.DeveloperName
	productInfo.DeveloperSiteUrl = extension.DeveloperWebsiteUrl
	productInfo.DeveloperSiteUrlDuplicate = productInfo.DeveloperSiteUrl

	// Listings
	listings := submission.Document.Listings
	if listings == nil {
		listings = make(map[string]adg.Listing)
	}
	for _, listing := range extension.LocaleListings {
		adgListing := adg.Listing{
			DisplayTitle:         listing.DisplayName,
			Keywords:             listing.Keywords,
			ShortDescription:     adg.NotApplicable,
			LongDescription:      adg.NotApplicable,
			IconAssetId:          adg.DefaultIconId,
			ScreenshotAssetIds:   adg.DefaultScreenshotIds,
			GameEdition:          adg.NotApplicable,
			GameEditionDetails:   []string{adg.NotApplicable},
			Features:             []string{adg.NotApplicable},
			SupportedControllers: []string{adg.NotApplicable},
			PCRequirements:       adg.NewDefaultPCRequirements(),
		}

		listings[listing.Locale] = adgListing
	}
	submission.Document.Listings = listings

	log.WithField("Submission", submission).Debug("Exiting")
	return &submission, nil
}

func (extension *Extension) PopulateADGDocument(document adg.ADGDocument) adg.ADGDocument {
	// Developer
	document.DeveloperInfo = adg.DeveloperInfo{
		DeveloperName: extension.DeveloperName,
	}
	document.DeveloperWebsiteUrl = extension.DeveloperWebsiteUrl

	// Customer
	document.CustomerSupportEmail = extension.CustomerSupportEmail
	document.CustomerSupportUrl = extension.CustomerSupportUrl

	// Fulfillment method
	document.FulfillmentMethod = Extension_Fulfillment_Method

	// LocaleListings
	for _, listing := range extension.LocaleListings {
		key := listing.Locale

		// Create the map if it doesn't exist
		listingsByLocale := document.ListingsByLocale
		if listingsByLocale == nil {
			listingsByLocale = make(map[string]adg.LocaleListing)
			document.ListingsByLocale = listingsByLocale
		}

		// Create the Locale if it doesn't exist
		localeListing, present := listingsByLocale[key]
		if !present {
			localeListing = adg.LocaleListing{}
		}

		localeListing.DisplayName = listing.DisplayName
		localeListing.IconId = adg.DefaultIconId
		localeListing.ShortDescription = listing.DisplayName
		localeListing.LongDescription = listing.DisplayName
		localeListing.ProductFeatures = []string{listing.DisplayName}
		localeListing.Keywords = append(localeListing.Keywords, listing.Keywords...)

		// Add the default fields
		localeListing.BoxshotId = adg.DefaultBoxshotId
		localeListing.GameEditionBadgeId = adg.DefaultGameEditionBadgeId
		localeListing.PageBackgroundId = adg.DefaultPageBackgroundId
		localeListing.ProductFeatures = adg.DefaultProductFeatures
		localeListing.ReleaseNotesId = adg.DefaultReleaseNotesId
		localeListing.ScreenshotIds = adg.DefaultScreenshotIds
		localeListing.ThankYouPageBackgroundImageId = adg.DefaultThankYouPageBackgroundImageId
		localeListing.VideoIds = adg.DefaultVideoIds

		listingsByLocale[key] = localeListing
	}

	return document
}

func (extension Extension) GetADGMetadata(fulfilledByTwitch bool) adg.Metadata {
	var gameMetadata adg.Metadata
	gameMetadata.ContentType = adg.ContentType_Extension
	gameMetadata.ProductCategory = adg.ProductCategoryExt_10000
	gameMetadata.Subcategory = adg.Subcategory(gameMetadata.ProductCategory + adg.Subcategory_Extensions)
	gameMetadata.ProductLine = adg.ProductLine_Game
	gameMetadata.ProductType = adg.ProductType_Entitlement
	return gameMetadata
}
