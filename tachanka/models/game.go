package models

import (
	"encoding/json"

	"time"

	"code.justin.tv/commerce/tachanka/models/adg"
	"code.justin.tv/commerce/tachanka/models/currency"
	"github.com/Sirupsen/logrus"
)

const (
	ModelType_Game               ManagedProductType = "ADG_GAME"
	DefaultGameFulfillmentMethod string             = "fulfilled_FuelLauncher_default"
	DefaultGameCategoryInfoId    string             = "twitch_fuel_action_game"
	DefaultESRBRating            string             = "rating_pending"
	DefaultPEGIRating            string             = "to_be_announced"
	DefaultUSKRating             string             = "to_be_announced"
	DefaultGameVersion           string             = "1.0"
)

type (
	PCRequirements struct {
		VideoCard      string `json:"videoCard"`
		RAM            string `json:"ram"`
		HardDriveSpace string `json:"hardDriveSpace"`
		Processor      string `json:"processor"`
		OS             string `json:"os"`
		DirectX        string `json:"directX"`
	}

	AvailabilityEntry struct {
		CountryCode string          `json:"countryCode"`
		Price       currency.Amount `json:"price"`
		Available   bool            `json:"available"`
	}

	Language struct {
		LanguageCode         string `json:"languageCode"`
		Instructions         bool   `json:"instructions"`
		SubtitlesAndCaptions bool   `json:"subtitlesAndCaptions"`
		Audio                bool   `json:"audio"`
	}

	ESRBRatingInfo struct {
		RatingCategory string `json:"ratingCategory"`
	}

	ContentRatingInfo struct {
		ESRBRatingInfo ESRBRatingInfo `json:"esrbRatingInfo"`
		PEGIRating     string         `json:"pegiRating"`
		USKRating      string         `json:"uskRating"`
	}

	Locale struct {
		Locale                   string   `json:"locale"`
		DisplayName              string   `json:"displayName"`
		ShortDescription         string   `json:"shortDescription"`
		LongDescription          string   `json:"longDescription"`
		ProductFeatures          []string `json:"productFeatures"`
		Keywords                 []string `json:"keywords"`
		GameEdition              string   `json:"gameEdition"`
		GameEditionDetails       string   `json:"gameEditionDetails"`
		IconId                   string   `json:"iconId"`
		ScreenshotIds            []string `json:"screenshotIds"`
		PageBackgroundId         string   `json:"pageBackgroundId"`
		EulaUrl                  string   `json:"eulaUrl"`
		ReleaseNotesId           string   `json:"releaseNotesId"`
		ThankYouPageBackgroundId string   `json:"thankYouPageBackgroundId"`
		BoxShotId                string   `json:"boxShotId"`
	}

	PCRequirementsInfo struct {
		MinimumRequirements     PCRequirements `json:"minimumRequirements"`
		RecommendedRequirements PCRequirements `json:"recommendedRequirements"`
		OtherRequirements       string         `json:"otherRequirements"`
	}

	Game struct {
		ManagedProductMetadata
		PCRequirementsInfo             PCRequirementsInfo  `json:"pcRequirementsInfo"`
		RequiresContinuousConnectivity bool                `json:"requiresContinuousConnectivity"`
		NumberOfPlayersSupported       string              `json:"numberOfPlayersSupported"`
		LanguageSupport                []Language          `json:"languageSupport"`
		Version                        string              `json:"version"`
		SupportedControllers           string              `json:"supportedControllers"`
		CategoryInfoId                 string              `json:"categoryInfoId"`
		DeveloperName                  string              `json:"developerName"`
		ContentRatingInfo              ContentRatingInfo   `json:"contentRatingInfo"`
		DefaultPrice                   currency.Amount     `json:"defaultPrice"`
		AvailabilityInfo               []AvailabilityEntry `json:"availabilityInfo"`
		ReleaseDate                    string              `json:"releaseDate"`
		AnnounceDate                   string              `json:"announceDate"`
		PreviousReleaseDate            string              `json:"previousReleaseDate"`
		DeveloperWebsiteURL            string              `json:"developerWebsiteUrl"`
		BroadcasterVODUrls             []string            `json:"broadcasterVodUrls"`
		NewsAndReviewsURLs             []string            `json:"newsAndReviewsUrls"`
		CustomerSupportURL             string              `json:"customerSupportUrl"`
		CustomerSupportEmail           string              `json:"customerSupportEmail"`
		LocaleListings                 []Locale            `json:"localeListings"`
		FulfillmentMethod              string              `json:"fulfillmentMethod"`
		SKU                            string              `json:"sku"`
	}
)

func (game *Game) PopulateADGSubmission(submission adg.ADGSubmission) (*adg.ADGSubmission, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":        "Game.PopulateADGSubmission",
		"MangedProductId": game.Id,
	})
	log.Debug("Entering")

	// Product Info
	productInfo := &submission.Document.ProductInfo
	productInfo.NumberOfPlayersSupported = game.NumberOfPlayersSupported
	productInfo.AmazonMaturityRating = adg.DefaultAmazonRating
	productInfo.ESRBRating = []string{game.ContentRatingInfo.ESRBRatingInfo.RatingCategory}
	productInfo.PEGIRating = game.ContentRatingInfo.PEGIRating
	productInfo.USKRating = game.ContentRatingInfo.USKRating
	productInfo.DeveloperName = game.DeveloperName
	productInfo.DeveloperSiteUrl = game.DeveloperWebsiteURL
	productInfo.DeveloperSiteUrlDuplicate = productInfo.DeveloperSiteUrl
	productInfo.CustomerSupportUrl = game.CustomerSupportURL
	productInfo.CustomerSupportEmailAddress = game.CustomerSupportEmail
	productInfo.IsExtension = false
	productInfo.VideoUrls = game.BroadcasterVODUrls
	productInfo.GameVersion = game.Version
	productInfo.CategoryId = game.CategoryInfoId
	productInfo.ItemBadgeDetails = []string{adg.ItemBadgeDetails_InGameCurrenyNotAccepted, game.FulfillmentMethod}
	if game.RequiresContinuousConnectivity {
		productInfo.ItemBadgeDetails = append(productInfo.ItemBadgeDetails, "requires_online_connectivity")
	}
	for _, language := range game.LanguageSupport {
		if language.Audio {
			productInfo.SupportedLanguageAudio = append(productInfo.SupportedLanguageAudio, language.LanguageCode)
		}
		if language.Instructions {
			productInfo.SupportedLanguageInstructions = append(productInfo.SupportedLanguageInstructions, language.LanguageCode)
		}
		if language.SubtitlesAndCaptions {
			productInfo.SupportedLanguageSubtitles = append(productInfo.SupportedLanguageSubtitles, language.LanguageCode)
		}
	}

	// Listings
	listings := submission.Document.Listings
	for _, listing := range game.LocaleListings {
		adgListing := adg.Listing{
			DisplayTitle:                  listing.DisplayName,
			ShortDescription:              listing.ShortDescription,
			LongDescription:               listing.LongDescription,
			Features:                      listing.ProductFeatures,
			Keywords:                      listing.Keywords,
			GameEdition:                   listing.GameEdition,
			IconAssetId:                   listing.IconId,
			BoxShotAssetId:                listing.BoxShotId,
			PageBackgroundAssetId:         listing.PageBackgroundId,
			ThankYouPageBackgroundAssetId: listing.ThankYouPageBackgroundId,
			ReleaseNotesAssetId:           listing.ReleaseNotesId,
			ScreenshotAssetIds:            listing.ScreenshotIds,
			EulaUrl:                       listing.EulaUrl,
			SupportedControllers:          []string{game.SupportedControllers},
		}

		if len(listing.GameEditionDetails) > 0 {
			adgListing.GameEditionDetails = []string{listing.GameEditionDetails}
		}

		minimumRequirements := adg.Requirements{
			OS:             game.PCRequirementsInfo.MinimumRequirements.OS,
			Processor:      game.PCRequirementsInfo.MinimumRequirements.Processor,
			RAM:            game.PCRequirementsInfo.MinimumRequirements.RAM,
			HardDriveSpace: game.PCRequirementsInfo.MinimumRequirements.HardDriveSpace,
			VideoCard:      game.PCRequirementsInfo.MinimumRequirements.VideoCard,
			DirectX:        game.PCRequirementsInfo.MinimumRequirements.DirectX,
		}

		recommendedRequirements := adg.Requirements{
			OS:             game.PCRequirementsInfo.RecommendedRequirements.OS,
			Processor:      game.PCRequirementsInfo.RecommendedRequirements.Processor,
			RAM:            game.PCRequirementsInfo.RecommendedRequirements.RAM,
			HardDriveSpace: game.PCRequirementsInfo.RecommendedRequirements.HardDriveSpace,
			VideoCard:      game.PCRequirementsInfo.RecommendedRequirements.VideoCard,
			DirectX:        game.PCRequirementsInfo.RecommendedRequirements.DirectX,
		}

		adgListing.PCRequirements = &adg.PCRequirements{
			Minimum:           minimumRequirements,
			Recommended:       recommendedRequirements,
			OtherRequirements: game.PCRequirementsInfo.OtherRequirements,
		}
		listings[listing.Locale] = adgListing
	}
	submission.Document.Listings = listings

	// Offerings and Version For Country
	var offerings []adg.Offering
	var versionForCountry []string

	for _, country := range game.AvailabilityInfo {
		if country.Available {
			versionForCountry = append(versionForCountry, country.CountryCode)
			offerings = append(offerings, adg.Offering{
				AnnounceDate:        game.ReleaseDate,
				ReleaseDate:         game.ReleaseDate,
				PreviousReleaseDate: game.ReleaseDate,
				PublicationDate:     game.ReleaseDate,
				Countries:           []string{country.CountryCode},
				Pricing: map[string]adg.ADGPrice{
					country.CountryCode: adg.ADGPrice{
						Currency: adg.DefaultCurrencyCode,
						Price:    float64(country.Price),
					}},
			})
		}
	}

	submission.Document.Offerings = offerings
	submission.Document.ProductInfo.VersionForCountry = versionForCountry

	log.WithField("Submission", submission).Debug("Exiting")

	return &submission, nil
}

func (game *Game) PopulateADGDocument(partialDoc adg.ADGDocument) adg.ADGDocument {

	// Easy stuff: string -> string
	partialDoc.ReleaseDate = game.ReleaseDate
	partialDoc.AnnounceDate = game.AnnounceDate
	partialDoc.PreviousReleaseDate = game.PreviousReleaseDate
	partialDoc.DeveloperWebsiteUrl = game.DeveloperWebsiteURL
	partialDoc.CustomerSupportUrl = game.CustomerSupportURL
	partialDoc.CustomerSupportEmail = game.CustomerSupportEmail
	partialDoc.DefaultLocale = DefaultLocale
	partialDoc.FulfillmentMethod = game.FulfillmentMethod

	// Okay stuff: []string -> []string
	partialDoc.BroadcasterVodUrls = game.BroadcasterVODUrls
	partialDoc.NewsAndReviewsUrls = game.NewsAndReviewsURLs

	// Generated stuff: string -> object
	partialDoc.DeveloperInfo.DeveloperName = game.DeveloperName
	partialDoc.CategoryInfo.CategoryId = game.CategoryInfoId
	partialDoc.ContentRatingInfo = game.ContentRatingInfo.toADGContentRatingInfo()

	// DUMB STUFF I HATE
	partialDoc.BinaryInfo = game.generateADGBinaryInfo()
	partialDoc.ListingsByLocale = game.generateADGListings()

	// GRR PRIcES HATE PRIcES
	availableCountries, marketplacePrices := game.generateAvailabilityInfo()
	partialDoc.AvailabilityInfo = availableCountries
	partialDoc.PricingInfo.PricesByMarketplaceAndCountry["amazon.com"] = marketplacePrices

	return partialDoc
}

func (cri ContentRatingInfo) toADGContentRatingInfo() adg.ContentRatingInfo {
	return adg.ContentRatingInfo{
		EsrbRatingInfo: adg.EsrbRatingInfo{
			RatingCategory:     cri.ESRBRatingInfo.RatingCategory,
			ContentDescriptors: make([]string, 0),
		},
		EsrbRating:   cri.ESRBRatingInfo.RatingCategory,
		PegiRating:   cri.PEGIRating,
		AmazonRating: adg.DefaultAmazonRating,
		UskRating:    cri.USKRating,
	}
}

func (game Game) generateADGListings() map[string]adg.LocaleListing {
	locales := make(map[string]adg.LocaleListing)
	for _, listing := range game.LocaleListings {
		locales[listing.Locale] = adg.LocaleListing{
			DisplayName:        listing.DisplayName,
			ShortDescription:   listing.ShortDescription,
			LongDescription:    listing.LongDescription,
			ProductFeatures:    listing.ProductFeatures,
			Keywords:           listing.Keywords,
			GameEdition:        listing.GameEdition,
			GameEditionDetails: listing.GameEditionDetails,
			IconId:             listing.IconId,
			ScreenshotIds:      listing.ScreenshotIds,
			PageBackgroundId:   listing.PageBackgroundId,
			ReleaseNotesId:     adg.DefaultReleaseNotesId,
			VideoIds:           adg.DefaultVideoIds,
			EulaUrl:            listing.EulaUrl,
			ThankYouPageBackgroundImageId: listing.ThankYouPageBackgroundId,
			BoxshotId:                     listing.BoxShotId,
			GameEditionBadgeId:            adg.DefaultGameEditionBadgeId,
		}
	}
	return locales
}

func (game Game) generateADGBinaryInfo() adg.BinaryInfo {
	return adg.BinaryInfo{
		PCRequirementInfo: adg.PCRequirementsInfo{
			MinimumRequirements:     game.PCRequirementsInfo.MinimumRequirements.toADGRequirementsInfo(),
			RecommendedRequirements: game.PCRequirementsInfo.RecommendedRequirements.toADGRequirementsInfo(),
			OtherRequirements:       game.PCRequirementsInfo.OtherRequirements,
		},
		RequiresContinuousConnectivity: game.RequiresContinuousConnectivity,
		NumberOfPlayersSupported:       game.NumberOfPlayersSupported,
		LanguageSupport:                game.toADGLanguageSupport(),
		Version:                        game.Version,
		SupportedControllers:           game.SupportedControllers,
	}
}

func (game Game) generateAvailabilityInfo() (adg.AvailabilityInfo, adg.MarketplacePrices) {
	var availableCountries []string
	pricesByCountry := make(map[string]adg.Price)
	for _, country := range game.AvailabilityInfo {
		if country.Available {
			availableCountries = append(availableCountries, country.CountryCode)
			pricesByCountry[country.CountryCode] = adg.Price{
				CurrencyCode: "USD", // We need to actually include this long term but this works for now.
				Amount:       country.Price,
			}
		}
	}

	marketplacePrices := adg.MarketplacePrices{
		DefaultListPrice: adg.Price{
			CurrencyCode: "USD",
			Amount:       game.DefaultPrice,
		},
		PricesByCountry: pricesByCountry,
	}

	return adg.AvailabilityInfo{
		AvailableCountries: availableCountries,
	}, marketplacePrices
}

func (requirements PCRequirements) toADGRequirementsInfo() adg.RequirementsInfo {
	return adg.RequirementsInfo{
		VideoCard:      requirements.VideoCard,
		RAM:            requirements.RAM,
		HardDriveSpace: requirements.HardDriveSpace,
		Processor:      requirements.Processor,
		OS:             requirements.OS,
		DirectX:        requirements.DirectX,
	}
}

func (game Game) toADGLanguageSupport() adg.LanguageSupport {
	instructions := make([]string, 0)
	subtitles := make([]string, 0)
	audio := make([]string, 0)
	for _, language := range game.LanguageSupport {
		if language.Instructions {
			instructions = append(instructions, language.LanguageCode)
		}
		if language.Audio {
			audio = append(audio, language.LanguageCode)
		}
		if language.SubtitlesAndCaptions {
			subtitles = append(subtitles, language.LanguageCode)
		}
	}
	return adg.LanguageSupport{
		Instructions:         instructions,
		SubtitlesAndCaptions: subtitles,
		Audio:                audio,
	}
}

func (game *Game) GetSKU() string {
	return game.SKU
}

func (game *Game) GetFulfillmentMethod() string {
	return game.FulfillmentMethod
}

func (game *Game) GetTaxInformation() adg.TaxInformation {
	return adg.TaxInformation{
		MultiplePurchases: false,
		InGameCurrency:    false,
		Durable:           true,
	}
}

func (game *Game) GetTitle() string {
	for _, locale := range game.LocaleListings {
		if locale.Locale == DefaultLocale {
			return locale.DisplayName
		}
	}
	return ""
}

func (game *Game) SetData(newData string) error {
	err := json.Unmarshal([]byte(newData), &game)
	if err != nil {
		return err
	}
	return nil
}

func (game *Game) GetData() (string, error) {
	gameData, err := json.Marshal(game)
	if err != nil {
		return "", err
	}
	return string(gameData), nil
}

func (game *Game) GetType() ManagedProductType {
	return ModelType_Game
}

func NewGame(metadata ManagedProductMetadata) *Game {
	// Locale
	defaultLocale := Locale{
		Locale:          "en-US",
		Keywords:        make([]string, 0),
		ProductFeatures: make([]string, 0),
		ScreenshotIds:   make([]string, 0),
	}

	// Availability Info
	sellableCountries := adg.GetAllSellableCountries()
	availabilityInfo := make([]AvailabilityEntry, len(sellableCountries))
	for i, countryCode := range sellableCountries {
		availabilityInfo[i] = AvailabilityEntry{
			Available:   false,
			CountryCode: countryCode,
			Price:       0,
		}
	}

	// Default to same time yesterday
	defaultTime := time.Now().AddDate(0, 0, -1).UTC().Format(time.RFC3339)

	// Supported Languages
	allLanguages := adg.GetAllLanguages()
	languageSupport := make([]Language, len(allLanguages))
	for i, language := range allLanguages {
		languageSupport[i] = Language{
			LanguageCode:         language,
			Audio:                false,
			Instructions:         false,
			SubtitlesAndCaptions: false,
		}
	}

	// Ratings
	defaultESRBRatingInfo := ESRBRatingInfo{
		RatingCategory: DefaultESRBRating,
	}

	defaultContentRatingInfo := ContentRatingInfo{
		ESRBRatingInfo: defaultESRBRatingInfo,
		PEGIRating:     DefaultPEGIRating,
		USKRating:      DefaultUSKRating,
	}

	return &Game{
		ManagedProductMetadata: metadata,
		LocaleListings:         []Locale{defaultLocale},
		AvailabilityInfo:       availabilityInfo,
		FulfillmentMethod:      DefaultGameFulfillmentMethod,
		ContentRatingInfo:      defaultContentRatingInfo,
		CategoryInfoId:         DefaultGameCategoryInfoId,
		AnnounceDate:           defaultTime,
		ReleaseDate:            defaultTime,
		PreviousReleaseDate:    defaultTime,
		LanguageSupport:        languageSupport,
		Version:                DefaultGameVersion,
	}
}

func (game Game) GetADGMetadata(fulfilledByTwitch bool) adg.Metadata {
	var gameMetadata adg.Metadata
	gameMetadata.ContentType = adg.ContentType_Game
	gameMetadata.ProductLine = adg.ProductLine_Game
	gameMetadata.ProductCategory = adg.ProductCategory_1000
	gameMetadata.ProductType = adg.ProductType_Entitlement
	if fulfilledByTwitch {
		gameMetadata.Subcategory = adg.Subcategory(gameMetadata.ProductCategory) + adg.Subcategory_FulfilledByTwitch
	} else {
		gameMetadata.Subcategory = adg.Subcategory(gameMetadata.ProductCategory) + adg.Subcategory_FulfilledBy3P
	}
	return gameMetadata
}
