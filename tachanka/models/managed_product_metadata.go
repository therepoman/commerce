package models

import (
	"strconv"
	"time"
)

type ManagedProductMetadata struct {
	Id              string `json:"id"`
	DraftDocumentId string `json:"-"`

	LiveSubmissionId    *string `json:"-"`
	WorkingSubmissionId *string `json:"-"`
	ADGProductId        *string `json:"adgProductId"`

	LastUpdated ReadOnlyTime `json:"lastUpdated"`
}

type ReadOnlyTime struct{ time.Time }

// Don't allow unmarshalling to prevent lastUpdated from being set by callers
func (readOnlyTime ReadOnlyTime) UnmarshalJSON([]byte) error { return nil }

func (readOnlyTime *ReadOnlyTime) MarshalJSON() ([]byte, error) {
	if readOnlyTime == nil {
		return nil, nil
	}

	return []byte(strconv.FormatInt(readOnlyTime.Unix(), 10)), nil
}

func (mp *ManagedProductMetadata) GetLastUpdated() time.Time {
	return mp.LastUpdated.Time
}

func (mp *ManagedProductMetadata) SetLastUpdated(newTime time.Time) {
	mp.LastUpdated = ReadOnlyTime{newTime}
}

func (mp *ManagedProductMetadata) GetId() string {
	return mp.Id
}

func (mp *ManagedProductMetadata) SetId(id string) {
	mp.Id = id
}

func (mp *ManagedProductMetadata) GetDraftDocumentId() string {
	return mp.DraftDocumentId
}

func (mp *ManagedProductMetadata) SetDraftDocumentId(newDocId string) {
	mp.DraftDocumentId = newDocId
}

func (mp *ManagedProductMetadata) GetADGProductId() string {
	if mp.ADGProductId != nil {
		return *mp.ADGProductId
	}
	return ""
}

func (mp *ManagedProductMetadata) SetADGProductId(newADGProdId string) {
	mp.ADGProductId = &newADGProdId
}

func (mp *ManagedProductMetadata) GetLiveSubmissionId() string {
	if mp.LiveSubmissionId != nil {
		return *mp.LiveSubmissionId
	}
	return ""
}

func (mp *ManagedProductMetadata) SetLiveSubmissionId(newSubmissionId string) {
	mp.LiveSubmissionId = &newSubmissionId
}

func (mp *ManagedProductMetadata) GetWorkingSubmissionId() string {
	if mp.WorkingSubmissionId != nil {
		return *mp.WorkingSubmissionId
	}
	return ""
}

func (mp *ManagedProductMetadata) SetWorkingSubmissionId(newWorkingSubmissionId string) {
	mp.WorkingSubmissionId = &newWorkingSubmissionId
}
