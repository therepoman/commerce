# Tachanka

![Our lord and savior](/documentation/tachanka.jpg)

Code base for the Fuel Commerce Developer Portal

See full documentation at: [https://w.amazon.com/index.php/Respawn/Store/NewHire/Twitch#Go_Development](https://w.amazon.com/index.php/Respawn/Store/NewHire/Twitch#Go_Development)

For more on design and code structure of Tachanka refer to the [dev guide](https://git-aws.internal.justin.tv/commerce/tachanka/blob/adding_dev_documentation/documentation/dev_guide.md).

## Setting Up the Go Service for Tachanka

### Setting up your Github account

 - Go to your [git account settings page](https://git-aws.internal.justin.tv/settings/keys) and follow the instructions there to add a new SSH key to your account.
 - As someone on your team to add you to the [commerce repo on git](https://git-aws.internal.justin.tv/commerce).

### Setting up your workspace

 - Ensure you are on the VPN before you begin

 - Install go:

	    brew install go

 - Create a Go workspace folder in your home directory. Ex:

	    mkdir ~/ws-tachanka

 - create an alias for setting up go:
        
	    echo 'alias gohere='"'"'export GOPATH=`pwd`; export PATH=$PATH:$GOPATH/bin'"'"'' >> ~/.zshrc
	    cd ~/ws-tachanka
	    gohere

 - Fetch Tachanka using `go get` NOTE: This _will_ fail if you aren't on the VPN

	    go get code.justin.tv/commerce/tachanka
	    
 - NOTE: `go get` will fail with `package code.justin.tv/commerce/tachanka: no Go files in <$GOPATH>/src/code.justin.tv/commerce/tachanka`.
   This is fine, you can safely ignore this error, as it is complaining that there are no top level go files in the directory. As long as it makes a src directory and places the source code in it, you can move on
   
 - Directory structure will now look like this:

	    ~/ws-tachanka/src/code.justin.tv/commerce/tachanka

### Building Code

 - To build your code, run:

		make local

 - To run the tests in the code base:

		make test

### Testing Code

#### Mocks
- Use [Testify](https://github.com/stretchr/testify) to mock out behaviour in tests
- Use [Mockery](https://github.com/vektra/mockery) to autogenerate mock classes 

### Pull Request and Pushing Code to Master

 - Push your changes to a remote branch using:

		git push origin $STORY

 - Use Github to create a pull request: https://git-aws.internal.justin.tv/commerce/tachonka/compare/$STORY
 - Ask people on #fuel-devportal-crs to look over your pull request and approve it!
 - Make sure to get proper sign off before you merge
 - This might take multiple iterations until your team is satisfied ¯\\\_(ツ)\_/¯

### Setting up IntelliJ

 - Download the Go plugin for IntelliJ: [https://plugins.jetbrains.com/plugin/5047?pr=idea](https://plugins.jetbrains.com/plugin/5047?pr=idea)
 - Or go to **Intellij IDEA -> Preferences... -> Plugins -> Browse repositories... -> Search 'Go' -> Go Custom Languages -> Install**
 - Set up the Go SDK
   - Go to **File -> Project Structures -> SDK**
   - Click the `+` icon
   - If Go was installed with homebrew, add the Go SDK as `/usr/local/Cellar/go/<version>/libexec`
 - Open your Go workspace in IntelliJ
   - Create a new project
   - Select Go as the language
   - Choose the SDK you configured above
   - Set the project location as the Go src directory (`~/ws-Tachanka/src` using the example above)
 - (Optional) Configure GOPATH if not launching IntelliJ from an environment with GOPATH set
   - Preferences -> Appearance and Behavior -> Path Variables

### Terraform
 - Creating a new IAM user 
   - Before running terraform, you will need to create a new IAM user for both the `fuel-devportal@amazon.com` and `fuel-devportal+dev@amazon.com` accounts
   - Go to https://isengard.amazon.com/console-access
   - Select `admin` next to the `fuel-devportal@amazon.com` account
   - Under services, enter `IAM`
   - Select `Users` from the left menu, and then `Add User`
   - Enter your username, select `Programmatic access` as the access type, and then `Next: Permissions`
   - Select `Attach existing policies directly` and check the box next to `AdministratorAccess`
   - On the Review screen, ensure the username is correct and the AWS access type is `Programmatic access - with an access key`
   - Make note of the `Access key ID` and `Secret access key` on the final screen. You will need this later when you configure the aws cli.
   - repeat the same steps for both accounts
 - Setting up Terraform and configuring aws cli
    - Follow the instructions for setting up and installing terraform and the Twitch WTF wrapper: https://wiki.twitch.com/pages/viewpage.action?spaceKey=ENG&title=WTF. You should be using Terraform 0.10.7 or higher. Verify the version with `terraform --version`.
	- When you reach the section for configuring aws, *DO NOT EXPORT THE ENVIRONMENT VARIABLES*. Instead, use the access key id and secrect access key from the previous section. The `AWS_DEFAULT_REGION` is `us-west-2`
	- After running `aws configure`, cd to `~/.aws`. Open the `credentials` file. Enter the following, replacing the templated fields with your own secret and key
	```
	[default]
	aws_access_key_id = <fuel-devportal+dev@amazon.com key id>
	aws_secret_access_key = <fuel-devportal+dev@amazon.com secret key>

	[fuel-devportal-dev]
	aws_access_key_id = <fuel-devportal+dev@amazon.com key id>
	aws_secret_access_key = <fuel-devportal+dev@amazon.com secret key>

	[fuel-devportal-prod]
	aws_access_key_id = <fuel-devportal@amazon.com key id>
	aws_secret_access_key = <fuel-devportal@amazon.com secret key>
	```
 - Running terraform
	- cd to the root git directiory of tachanka
	- Development changes
		- Run `/scripts/terraform_wtf_development.sh -p -X` to generate the plan
		- Once the plan has completed, and the changes reviewed, run `/scripts/terraform_wtf_development.sh` to apply the changes to devo
	- Production changes
		- Run `/scripts/terraform_wtf_production.sh -p -X` to generate the plan
		- Once the plan has completed, and the changes reviewed, run `/scripts/terraform_wtf_production.sh` to apply the changes to production

### Troubleshooting

#### Brew Install: Agree to license for Xcode

If Brew install says Agree to license for Xcode Builds will fail! run

	sudo xcodebuild -license

#### Brew install: Unprocessable Entity

If Brew install says something like: 422 Unprocessable Entity (GitHub::Error) run

	brew update

#### Brew install: 'require': cannot load such file -- mach (LoadError)

If Brew install says something like (while running brew update) `/System/Library/Frameworks/Ruby.framework/Versions/2.0/usr/lib/ruby/2.0.0/rubygems/core_ext/kernel_require.rb:55:in 'require': cannot load such file -- mach (LoadError)`, try running

	sudo chown -R $(whoami):admin /usr/local

#### Intellij: Empty GOPATH

If Intellij says there is an empty GOPATH, click on Edit configuration and add your GOPATH into the global settings. Ex:

	/Users/<username>/ws-Tachanka

#### IntelliJ: Not a valid home for GO sdk


If Intellij says "the selected directory is not a valid home for GO sdk", try updating the version of IntelliJ and the Go plugin

#### Permission denied on push

```
[ryanresi@laptop] git push origin testbranch                                                                         ~GOPATH/src/code.justin.tv/commerce/tachanka
remote: Permission to commerce/tachanka.git denied to ryanresi.
fatal: unable to access 'https://git-aws.internal.justin.tv/commerce/tachanka.git/': The requested URL returned error: 403
```

You need to be added to the git repo permissions list. Ask someone on the team to add you.
