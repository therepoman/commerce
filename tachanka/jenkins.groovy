job {
    name 'commerce-tachanka'
    using 'TEMPLATE-autobuild'
    concurrentBuild true

    scm {
        git {
            remote {
                github 'commerce/tachanka', 'ssh', 'git-aws.internal.justin.tv'
                credentials 'git-aws-read-key'
            }
            clean true
        }
    }

    wrappers {
        sshAgent 'git-aws-read-key'
        preBuildCleanup()
        timestamps()
        credentialsBinding {
            file('COURIERD_PRIVATE_KEY', 'courierd')
            file('AWS_CONFIG_FILE', 'aws_config')
        }
    }

    steps {
        shell './scripts/build.sh'
        shell './scripts/push.sh'
        saveDeployArtifact 'commerce/tachanka-ebextensions', 'deploy/'
    }
}




freeStyleJob('commerce-tachanka-prod-deploy') {
    using 'TEMPLATE-deploy-aws'

    wrappers {
        credentialsBinding {
            file('COURIERD_PRIVATE_KEY', 'courierd')
            file('AWS_CONFIG_FILE', 'aws_config')
            string 'AWS_ACCESS_KEY', 'fuel-devportal-prod-tcs-access-key'
            string 'AWS_SECRET_KEY', 'fuel-devportal-prod-tcs-secret-key'
        }
    }

    steps {
        // download .ebextensions into deploy/.ebextensions
        downloadDeployArtifact 'commerce/tachanka-ebextensions'
        shell 'rm *.manifest*'
        // deploy server
        shell """set +x
                |export AWS_SECRET_ACCESS_KEY=\$AWS_SECRET_KEY
                |export AWS_ACCESS_KEY_ID=\$AWS_ACCESS_KEY
                |export PYTHONUNBUFFERED=1
                |set -x
                |export IMAGE=docker-registry.internal.justin.tv/commerce-tachanka:\$GIT_COMMIT
                |
                |cat <<EOF > Dockerrun.aws.json
                |{
                |  "AWSEBDockerrunVersion": "1",
                |  "Image": {
                |    "Name": "\$IMAGE",
                |    "Update": "false"
                |  },
                |  "Ports": [
                |    {
                |      "ContainerPort": "8000"
                |    }
                |  ],
                |  "Volumes": [
                |    {
                |      "ContainerDirectory": "/var/app",
                |      "HostDirectory": "/var/app"
                |    },
                |    {
                |      "ContainerDirectory": "/etc/ssl/certs/ca-bundle.crt",
                |      "HostDirectory": "/etc/ssl/certs/ca-bundle.crt"
                |    }
                |  ],
                |  "Logging": "/var/log/"
                |}
                |EOF
                | sed -i "s/tachanka/tachanka_prod/" .elasticbeanstalk/config.yml
                | eb labs cleanup-versions --num-to-leave 10 --older-than 5 --force -v --region us-west-2
                | eb deploy prod-commerce-tachanka-env""".stripMargin()
    }
}




freeStyleJob('commerce-tachanka-dev-deploy') {
    using 'TEMPLATE-deploy-aws'

    wrappers {
        credentialsBinding {
            file('COURIERD_PRIVATE_KEY', 'courierd')
            file('AWS_CONFIG_FILE', 'aws_config')
            string 'AWS_ACCESS_KEY', 'fuel-devportal-dev-tcs-access-key'
            string 'AWS_SECRET_KEY', 'fuel-devportal-dev-tcs-secret-key'
        }
    }

    steps {
        // download .ebextensions into deploy/.ebextensions
        downloadDeployArtifact 'commerce/tachanka-ebextensions'
        shell 'rm *.manifest*'
        // deploy server
        shell """set +x
                |export AWS_SECRET_ACCESS_KEY=\$AWS_SECRET_KEY
                |export AWS_ACCESS_KEY_ID=\$AWS_ACCESS_KEY
                |export PYTHONUNBUFFERED=1
                |set -x
                |export IMAGE=docker-registry.internal.justin.tv/commerce-tachanka:\$GIT_COMMIT
                |
                |cat <<EOF > Dockerrun.aws.json
                |{
                |  "AWSEBDockerrunVersion": "1",
                |  "Image": {
                |    "Name": "\$IMAGE",
                |    "Update": "false"
                |  },
                |  "Ports": [
                |    {
                |      "ContainerPort": "8000"
                |    }
                |  ],
                |  "Volumes": [
                |    {
                |      "ContainerDirectory": "/var/app",
                |      "HostDirectory": "/var/app"
                |    },
                |    {
                |      "ContainerDirectory": "/etc/ssl/certs/ca-bundle.crt",
                |      "HostDirectory": "/etc/ssl/certs/ca-bundle.crt"
                |    }
                |  ],
                |  "Logging": "/var/log/"
                |}
                |EOF
                | sed -i "s/tachanka/tachanka_dev/" .elasticbeanstalk/config.yml
                | eb labs cleanup-versions --num-to-leave 10 --older-than 5 --force -v --region us-west-2
                | eb deploy dev-commerce-tachanka-env""".stripMargin()
    }
}


freeStyleJob('commerce-tachanka-staging-deploy') {
    using 'TEMPLATE-deploy-aws'

    wrappers {
        credentialsBinding {
            file('COURIERD_PRIVATE_KEY', 'courierd')
            file('AWS_CONFIG_FILE', 'aws_config')
            string 'AWS_ACCESS_KEY', 'fuel-devportal-dev-tcs-access-key'
            string 'AWS_SECRET_KEY', 'fuel-devportal-dev-tcs-secret-key'
        }
    }

    steps {
        // download .ebextensions into deploy/.ebextensions
        downloadDeployArtifact 'commerce/tachanka-ebextensions'
        shell 'rm *.manifest*'
        // deploy server
        shell """set +x
                |export AWS_SECRET_ACCESS_KEY=\$AWS_SECRET_KEY
                |export AWS_ACCESS_KEY_ID=\$AWS_ACCESS_KEY
                |export PYTHONUNBUFFERED=1
                |set -x
                |export IMAGE=docker-registry.internal.justin.tv/commerce-tachanka:\$GIT_COMMIT
                |
                |cat <<EOF > Dockerrun.aws.json
                |{
                |  "AWSEBDockerrunVersion": "1",
                |  "Image": {
                |    "Name": "\$IMAGE",
                |    "Update": "false"
                |  },
                |  "Ports": [
                |    {
                |      "ContainerPort": "8000"
                |    }
                |  ],
                |  "Volumes": [
                |    {
                |      "ContainerDirectory": "/var/app",
                |      "HostDirectory": "/var/app"
                |    },
                |    {
                |      "ContainerDirectory": "/etc/ssl/certs/ca-bundle.crt",
                |      "HostDirectory": "/etc/ssl/certs/ca-bundle.crt"
                |    }
                |  ],
                |  "Logging": "/var/log/"
                |}
                |EOF
                | sed -i "s/tachanka/tachanka_staging/" .elasticbeanstalk/config.yml
                | eb labs cleanup-versions --num-to-leave 10 --older-than 5 --force -v --region us-west-2
                | eb deploy staging-commerce-tachanka-env""".stripMargin()
    }
}



