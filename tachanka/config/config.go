package config

import (
	"encoding/json"
	"fmt"
	"os"
)

type Configuration struct {
	LocalHostPort  string
	LocalDebugPort string

	// AWS Dynamo configuration
	DynamoRegion           string `valid:"required"`
	DynamoTablePrefix      string `valid:"required"`
	DynamoEndpointOverride string

	// TProx configuration
	TProxAccessRoleARN  string `valid:"required"`
	TProxServiceName    string `valid:"required"`
	TProxAWSRegion      string `valid:"required"`
	TProxSigV4AWSRegion string `valid:"required"`
}

const (
	LocalConfigFilePath                    = "/src/code.justin.tv/commerce/tachanka/config/data/%s.json"
	GlobalConfigFilePath                   = "/etc/tachanka/config/%s.json"
	MulliganGameSchemaBaseFilename         = "mulligan_respawn_game_schema"
	MulliganIGCSchemaBaseFilename          = "mulligan_respawn_igc_schema"
	MulliganExtensionSchemaBaseFilename    = "mulligan_respawn_extension_schema"
	MulliganExtensionIAPSchemaBaseFilename = "mulligan_respawn_extension_item_schema"
	EmptyDocumentDataJSON                  = "empty_document_data"
)

func GetCurrentEnvironment() string {
	env := os.Getenv("ENVIRONMENT")
	if env == "prod" {
		return "production"
	} else if env == "local" {
		return "local"
	} else if env == "staging" {
		return "staging"
	} else {
		return "development"
	}
}

func getConfigFilePath(baseFilename string) (string, error) {
	localFname := os.Getenv("GOPATH") + fmt.Sprintf(LocalConfigFilePath, baseFilename)
	if _, err := os.Stat(localFname); !os.IsNotExist(err) {
		return localFname, nil
	}
	globalFname := fmt.Sprintf(GlobalConfigFilePath, baseFilename)
	if _, err := os.Stat(globalFname); os.IsNotExist(err) {
		return "", err
	}
	return globalFname, nil
}

func GetMulliganGameSchemaFilePath() (string, error) {
	return getConfigFilePath(MulliganGameSchemaBaseFilename)
}

func GetMulliganIGCSchemaFilePath() (string, error) {
	return getConfigFilePath(MulliganIGCSchemaBaseFilename)
}

func GetMulliganExtensionSchemaFilePath() (string, error) {
	return getConfigFilePath(MulliganExtensionSchemaBaseFilename)
}

func GetMulliganExtensionIAPSchemaFilePath() (string, error) {
	return getConfigFilePath(MulliganExtensionIAPSchemaBaseFilename)
}

func GetEmptyDocumentDataFilePath() (string, error) {
	return getConfigFilePath(EmptyDocumentDataJSON)
}

func LoadEnvironmentConfig(environment string) (*Configuration, error) {
	configFilePath, err := getConfigFilePath(environment)
	if err != nil {
		return nil, err
	}
	return loadEnvironmentConfig(configFilePath)
}

func loadEnvironmentConfig(filepath string) (*Configuration, error) {
	file, err := os.Open(filepath)

	if err != nil {
		return nil, fmt.Errorf("Failed while opening config file: %v", err)
	}
	decoder := json.NewDecoder(file)
	configuration := &Configuration{}

	err = decoder.Decode(configuration)
	if err != nil {
		return nil, fmt.Errorf("Failed while parsing configuration file: %v", err)
	}

	return configuration, nil
}
