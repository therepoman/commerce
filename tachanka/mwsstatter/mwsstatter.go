package mwsstatter

import (
	log "github.com/Sirupsen/logrus"
	"net/http"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws/credentials"
	"goji.io"
	"golang.org/x/net/context"

	"code.justin.tv/commerce/AmazonMWSGoClient/mws"
)

const (
	unscopedClientID = ""
	hostSpecific     = false
	partitionID      = ""
	customerID       = ""
	metricsRegion    = "PDX" // PDX is us-west-2.

	minPeriod       = mws.PeriodFiveMinute
	maxPeriod       = mws.PeriodFiveMinute
	marketplace     = "us-west-2"
	TimeMetricType  = "Time"
	CountMetricType = "Count"
	serviceName     = "tachanka"
	environment     = "tachanka/NA"
	tachankaClient  = "/v1/TachankaClient/"

	// MetricTypeSuccesses is the number of successes for a provided API.
	MetricTypeSuccesses = "Success"
	// MetricTypeServerError is the number of server errors for a provided API.
	MetricTypeServerError = "Server_Error"
	// MetricTypeClientError is the number of client errors for a provided API.
	MetricTypeClientError = "Client_Error"
)

func StatusToMetricType(status int) string {
	var metricType = ""
	if status < 300 {
		// Anything below 300 is a success.
		metricType = MetricTypeSuccesses
	} else if status < 500 {
		// Anything between 300 and 500 is a client error
		metricType = MetricTypeClientError
	} else {
		// Anything above 500 is a server error
		metricType = MetricTypeServerError
	}

	return metricType
}

type StatusTrackingResponseWriter struct {
	http.ResponseWriter
	// HTTP status code
	Status int
}

func (w *StatusTrackingResponseWriter) WriteHeader(status int) {
	w.Status = status
	w.ResponseWriter.WriteHeader(status)
}

func NewMWSMetricsMiddleware(mwsClient mws.IAmazonMWSGoClient, isProd bool, hostname string, awsCredentials *credentials.Credentials) func(goji.Handler) goji.Handler {
	return func(h goji.Handler) goji.Handler {
		return goji.HandlerFunc(
			func(c context.Context, w http.ResponseWriter, r *http.Request) {
				startTime := time.Now()

				ww := &StatusTrackingResponseWriter{w, http.StatusOK}
				h.ServeHTTPC(c, ww, r)

				// Use the URL path to determine the metrics-friendly method name (the last part of the URL)
				splitPath := strings.Split(r.URL.Path, "/")
				methodName := splitPath[len(splitPath)-1]

				postUnscopedClientMetrics(mwsClient, isProd, methodName, awsCredentials, hostname, time.Since(startTime), ww.Status)
			},
		)
	}
}

// postUnscopedClientMetrics will post a batched metric for both latency and the call response to PMET
func postUnscopedClientMetrics(mwsClient mws.IAmazonMWSGoClient, isProd bool, methodName string, awsCredentials *credentials.Credentials, hostname string, latency time.Duration, httpStatus int) {
	// TODO: Consider preventing metric reporting for tests/ local?
	// Initialize MetricReport and add metrics
	metricReport := mws.NewMetricReport(isProd, hostname, minPeriod, maxPeriod, partitionID, environment, customerID)
	addStatusMetric(&metricReport, isProd, methodName, unscopedClientID, hostname, httpStatus)
	addLatencyMetric(&metricReport, isProd, methodName, unscopedClientID, hostname, latency)
	go postMetrics(mwsClient, metricReport, awsCredentials)
}

// addLatencyMetric will add the latency for a given method and client to the PMET request.
func addLatencyMetric(metricReport *mws.MetricReport, isProd bool, methodName string, clientName string, hostname string, latency time.Duration) {

	// Initialize the Latency metric object.
	latencyMetric := mws.NewMetric(isProd, hostname, hostSpecific, marketplace, serviceName, methodName, clientName, TimeMetricType)

	// Add the actual latency value to the metric.
	latencyMetric.AddValue(latency.Seconds() * 1000.0)
	latencyMetric.Unit = mws.UnitMilliseconds
	latencyMetric.Timestamp = time.Now().UTC()
	metricReport.AddMetric(latencyMetric)
}

// addStatusMetric will add the response (200, 400, 500) metrics for a given method and client to the PMET request.
func addStatusMetric(metricReport *mws.MetricReport, isProd bool, methodName string, clientName string, hostname string, httpStatus int) {

	// Initialize the status count metric object.
	metricType := StatusToMetricType(httpStatus)
	countMetric := mws.NewMetric(isProd, hostname, hostSpecific, marketplace, serviceName, methodName, clientName, metricType)

	// Add a count of 1 to the metric object
	countMetric.AddValue(1.0)
	countMetric.Unit = CountMetricType
	countMetric.Timestamp = time.Now().UTC()
	metricReport.AddMetric(countMetric)
}

// postMetrics will make the PMET call necessary for emitting the metrics associated with a given metricReport
func postMetrics(mwsClient mws.IAmazonMWSGoClient, metricReport mws.MetricReport, awsCredentials *credentials.Credentials) {
	// Wrap the MetricReport in a request object.
	req := mws.NewPutMetricsForAggregationRequest()
	req.AddMetricReport(metricReport)

	_, metricsErr := mwsClient.PutMetricsForAggregation(req, mws.Regions[metricsRegion], awsCredentials)
	if metricsErr != nil {
		log.Printf("Could not post MWS metric due to error: %s", metricsErr.Error())
	}
}
