package mwsstatter_test

import (
	"context"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/aws/credentials"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/AmazonMWSGoClient/mws"
	"code.justin.tv/commerce/tachanka/mocks"
	"code.justin.tv/commerce/tachanka/mwsstatter"
)

var testContext context.Context
var testAwsCredentials *credentials.Credentials = credentials.NewStaticCredentials("awsAccessKeyId", "awsSecretKey", "token")
var mwsResponse mws.PutMetricsForAggregationResponse = mws.PutMetricsForAggregationResponse{}
var testRequest *http.Request = getTestRequest()

const (
	hostName                 = "hostName"
	serviceName              = "serviceName"
	methodName               = "methodName"
	path                     = "/v2/code.justin.tv.commerce.tachanka.client.TachankaClient/methodName"
	serveHTTPC               = "ServeHTTPC"
	putMetricsForAggregation = "PutMetricsForAggregation"
)

func TestMWSMetricsMiddleware_SuccessfulNestedHandlerAndSuccessfulMetricsPublishing(t *testing.T) {
	handler := new(mocks.Handler)
	mwsClient := new(mocks.IAmazonMWSGoClient)

	testHttpResponseWriter := httptest.NewRecorder()
	middleware := mwsstatter.NewMWSMetricsMiddleware(mwsClient, false, hostName, testAwsCredentials)
	handlerWithMiddleware := middleware(handler)

	Convey("When the nested handler is successful and metrics publishing is successful for a success metric", t, func() {
		handler.On(serveHTTPC, testContext, mock.Anything, testRequest).Run(func(args mock.Arguments) {
			args.Get(1).(http.ResponseWriter).WriteHeader(200)
		})

		mwsClient.On(putMetricsForAggregation, mock.Anything, mock.Anything, mock.Anything).Return(&mwsResponse, nil)
		handlerWithMiddleware.ServeHTTPC(testContext, testHttpResponseWriter, testRequest)

		// Sleep for 500 ms to avoid issues where the previous step is "done" even though the go sub-routines
		// posting metrics aren't
		time.Sleep(500 * time.Millisecond)
		So(handler.AssertCalled(t, serveHTTPC, testContext, mock.Anything, testRequest), ShouldBeTrue)

		So(mwsClient.AssertNumberOfCalls(t, putMetricsForAggregation, 1), ShouldBeTrue)
		r1, ok := (mwsClient.Calls[0].Arguments[0]).(*mws.PutMetricsForAggregationRequest)
		So(ok, ShouldBeTrue)
		checkMetricRequest(r1, true)
	})
}

func TestMWSMetricsMiddleware_SuccessfulNestedHandlerAndUnsuccessfulMetricsPublishing(t *testing.T) {
	handler := new(mocks.Handler)
	mwsClient := new(mocks.IAmazonMWSGoClient)

	testHttpResponseWriter := httptest.NewRecorder()
	middleware := mwsstatter.NewMWSMetricsMiddleware(mwsClient, false, hostName, testAwsCredentials)
	handlerWithMiddleware := middleware(handler)

	Convey("When the nested handler is successful and metrics publishing fails", t, func() {
		handler.On(serveHTTPC, testContext, mock.Anything, testRequest).Run(func(args mock.Arguments) {
			args.Get(1).(http.ResponseWriter).WriteHeader(200)
		})

		mwsClient.On(putMetricsForAggregation, mock.Anything, mock.Anything, mock.Anything).Return(&mwsResponse, errors.New("Test metrics publishing error"))
		handlerWithMiddleware.ServeHTTPC(testContext, testHttpResponseWriter, testRequest)

		// Sleep for 500 ms to avoid issues where the previous step is "done" even though the go sub-routines
		// posting metrics aren't
		time.Sleep(500 * time.Millisecond)
		So(handler.AssertCalled(t, serveHTTPC, testContext, mock.Anything, testRequest), ShouldBeTrue)

		So(mwsClient.AssertNumberOfCalls(t, putMetricsForAggregation, 1), ShouldBeTrue)
		r1, ok := (mwsClient.Calls[0].Arguments[0]).(*mws.PutMetricsForAggregationRequest)
		So(ok, ShouldBeTrue)
		checkMetricRequest(r1, true)
	})
}

func TestMWSMetricsMiddleware_UnsuccessfulNestedHandler(t *testing.T) {
	handler := new(mocks.Handler)
	mwsClient := new(mocks.IAmazonMWSGoClient)

	testHttpResponseWriter := httptest.NewRecorder()
	middleware := mwsstatter.NewMWSMetricsMiddleware(mwsClient, false, hostName, testAwsCredentials)
	handlerWithMiddleware := middleware(handler)

	Convey("When the nested handler fails and metrics publishing is successful", t, func() {
		handler.On(serveHTTPC, mock.Anything, mock.Anything, mock.Anything).Run(func(args mock.Arguments) {
			args.Get(1).(http.ResponseWriter).WriteHeader(500)
		})

		mwsClient.On(putMetricsForAggregation, mock.Anything, mock.Anything, mock.Anything).Return(&mwsResponse, nil)
		handlerWithMiddleware.ServeHTTPC(testContext, testHttpResponseWriter, testRequest)

		// Sleep for 500 ms to avoid issues where the previous step is "done" even though the go sub-routines
		// posting metrics aren't
		time.Sleep(500 * time.Millisecond)
		So(handler.AssertCalled(t, serveHTTPC, testContext, mock.Anything, testRequest), ShouldBeTrue)

		So(mwsClient.AssertNumberOfCalls(t, putMetricsForAggregation, 1), ShouldBeTrue)
		r1, ok := (mwsClient.Calls[0].Arguments[0]).(*mws.PutMetricsForAggregationRequest)
		So(ok, ShouldBeTrue)
		checkMetricRequest(r1, false)
	})
}

func TestMWSMetrics_StatusToMetricType(t *testing.T) {
	Convey("Test Success Metric Type", t, func() {
		So(mwsstatter.StatusToMetricType(200), ShouldEqual, mwsstatter.MetricTypeSuccesses)
	})
	Convey("Test Client Error Metric Type", t, func() {
		So(mwsstatter.StatusToMetricType(400), ShouldEqual, mwsstatter.MetricTypeClientError)
	})
	Convey("Test Server Error Metric Type", t, func() {
		So(mwsstatter.StatusToMetricType(500), ShouldEqual, mwsstatter.MetricTypeServerError)
	})
}

func getTestRequest() *http.Request {
	request, _ := http.NewRequest("POST", path, strings.NewReader("body"))
	return request
}

func checkMetricRequest(req *mws.PutMetricsForAggregationRequest, isSuccessful bool) {
	// There is a single metric report for each metrics call
	So(req.MetricReports, ShouldHaveLength, 1)
	// A single scoped metric emits multiple PMET metrics: (ALL, ALL) and (methodName, ALL)
	// We batch latency and status metrics (so 4 metrics total)
	So(req.MetricReports[0].Metrics, ShouldHaveLength, 4)
	checkStatusMetricRequest(req.MetricReports[0].Metrics[0], isSuccessful)
	checkStatusMetricRequest(req.MetricReports[0].Metrics[1], isSuccessful)
	checkLatencyMetricRequest(req.MetricReports[0].Metrics[2])
	checkLatencyMetricRequest(req.MetricReports[0].Metrics[3])
}

func checkLatencyMetricRequest(metric mws.Metric) {
	So(metric.MetricName, ShouldEqual, mwsstatter.TimeMetricType)
	So(metric.Values, ShouldHaveLength, 1)
	So(metric.Unit, ShouldEqual, mws.UnitMilliseconds)
	So(metric.Values[0], ShouldBeGreaterThan, 0)
}

func checkStatusMetricRequest(metric mws.Metric, isSuccessful bool) {
	if isSuccessful {
		So(metric.MetricName, ShouldEqual, mwsstatter.MetricTypeSuccesses)
	} else {
		So(metric.MetricName, ShouldEqual, mwsstatter.MetricTypeServerError)
	}
	So(metric.Values, ShouldHaveLength, 1)
	So(metric.Unit, ShouldEqual, mwsstatter.CountMetricType)
	So(metric.Values[0], ShouldEqual, 1.0)
}
