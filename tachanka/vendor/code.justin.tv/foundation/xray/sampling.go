package xray

import (
	"math/rand"
	"time"
)

var randomSource = rand.New(rand.NewSource(time.Now().UnixNano()))

func sample(seg *segment) bool {
	sampling := config.Sampling()
	if sampling >= 1.0 {
		return true
	}
	if sampling <= 0.0 {
		return false
	}
	return randomSource.Float64() < sampling
}
