package rotate

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"
)

// RotationFrequency specifies how often the log file should be
// rotated. The default is hourly.
type RotationFrequency string

// The following represent the supported rotation frequencies. Default
// is hourly, which is what most Amazon tools expect.
const (
	RotateMinute     RotationFrequency = "2006-01-02-15-04"
	RotateHourly     RotationFrequency = "2006-01-02-15"
	RotateTwiceDaily RotationFrequency = "2006-01-02PM"
	RotateDaily      RotationFrequency = "2006-01-02"
	RotateNever      RotationFrequency = ""
)

const (
	defaultRotationFrequency = RotateHourly
	defaultLocationName      = "UTC"
	defaultFileHandleTimeout = 5 * time.Second
)

// TemporalWriter is an io.Writer that writes to an Amazon-style
// rotating log file.
type TemporalWriter interface {
	io.Writer
	TimedWrite(p []byte, t *time.Time) (n int, err error)
}

type timeoutFileHandle struct {
	file  *os.File
	timer *time.Timer
	wChan chan bool
}

// RotatingFile is an io.Writer that writes to the correct log file
// based on the current time and the desired rotation frequency. Do
// not create one of these yourself, use NewTimeRotatingFile.
type RotatingFile struct {
	freq        RotationFrequency
	loc         *time.Location
	path        string
	files       map[string]*timeoutFileHandle
	fileTimeout time.Duration
	fhChan      chan bool
	createCheck bool
}

// NewTimeRotatingFile is the only way to create a RotatingFile.
func NewTimeRotatingFile(path string, options ...func(*RotatingFile) error) (*RotatingFile, error) {
	absPath, err := filepath.Abs(path)
	if err != nil {
		return nil, fmt.Errorf("Failed to retrieve absolute path: %v", err)
	}
	fileInfo, err := os.Stat(absPath)
	if fileInfo != nil && fileInfo.IsDir() {
		return nil, errors.New("path must not be a directory")
	}
	loc, err := time.LoadLocation(defaultLocationName)
	if err != nil {
		return nil, fmt.Errorf("Failed to parse default location. %v", err)
	}
	// Construct default RotatingFile
	f := &RotatingFile{
		path:        absPath,
		freq:        defaultRotationFrequency,
		loc:         loc,
		files:       make(map[string]*timeoutFileHandle, 2),
		fileTimeout: time.Duration(defaultFileHandleTimeout),
		fhChan:      make(chan bool, 1), // Fetch on a single file handler at a time.
		createCheck: true,
	}
	// Apply constructor options
	for _, option := range options {
		err := option(f)
		if err != nil {
			return nil, fmt.Errorf("Error creating RotatingFile. Error while setting options: %v", err)
		}
	}
	// Make directory to the file path if it doesn't already exist.
	err = os.MkdirAll(filepath.Dir(absPath), os.ModeDir|os.ModeSetgid|0775)
	if err != nil {
		return nil, fmt.Errorf("Failed to create file directory: %v", err)
	}
	// Validate we can create files in the specified directory.
	if f.createCheck {
		_, err = f.fetchFileHandle(time.Now().In(f.loc).Format(string(f.freq)))
		if err != nil {
			return nil, fmt.Errorf("Failed to create/open file in the directory. Error while opening file handler: %v", err)
		}
	}
	return f, nil
}

// RotationFreq is a constructor option to set the file rotation
// frequency.
func RotationFreq(freq RotationFrequency) func(*RotatingFile) error {
	return func(f *RotatingFile) error {
		f.freq = freq
		return nil
	}
}

// TimeLocation is a constructor option to set the file rotation
// timezone.
func TimeLocation(locName string) func(*RotatingFile) error {
	return func(f *RotatingFile) error {
		loc, err := time.LoadLocation(locName)
		if err != nil {
			return fmt.Errorf("Failed to parse location %v. %v", locName, err)
		}
		f.loc = loc
		return nil
	}
}

// SkipCreateCheck is a constructor option to skip validating that we
// can create a file in the log directory. Useful only for unit tests.
func SkipCreateCheck() func(*RotatingFile) error {
	return func(f *RotatingFile) error {
		f.createCheck = false
		return nil
	}
}

// Write writes len(p) bytes from p to the current file based on the
// rotation frequency.
func (f *RotatingFile) Write(p []byte) (n int, err error) {
	cur := time.Now()
	return f.TimedWrite(p, &cur)
}

// TimedWrite writes len(p) bytes from p to the appropriate rotated
// file as calculated by time t. You'd use this instead of Write if
// you already have a time.Time object you want to use.
func (f *RotatingFile) TimedWrite(p []byte, t *time.Time) (n int, err error) {
	if t == nil {
		return 0, errors.New("Time must be specified.")
	}
	postfix := t.In(f.loc).Format(string(f.freq))
	file, err := f.fetchFileHandle(postfix)
	if err != nil {
		return 0, fmt.Errorf("Failed to fetch file handle. %v", err)
	}
	// Only write to each file with a single thread.
	file.wChan <- true
	defer func() { <-file.wChan }()
	return file.file.Write(p)
}

// Fetches the file handle for the specified postfix.
func (f *RotatingFile) fetchFileHandle(postfix string) (*timeoutFileHandle, error) {
	f.fhChan <- true
	defer func() { <-f.fhChan }()
	// Check if we currently have the file handler open. If we do reset the timeout and return it.
	if file, ok := f.files[postfix]; ok {
		file.timer.Reset(f.fileTimeout)
		return file, nil
	}
	// File handle isn't open. Open and store it.
	path := fmt.Sprintf("%v.%v", f.path, postfix)
	// TODO: Make the group and everyone permission octets configurable.
	file, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		return nil, fmt.Errorf("Failed to create new file: %v", err)
	}
	tfh := timeoutFileHandle{
		file:  file,
		timer: time.AfterFunc(f.fileTimeout, func() { f.fhTimeout(postfix) }),
		wChan: make(chan bool, 1),
	}
	f.files[postfix] = &tfh
	return &tfh, nil
}

// Close the file handler after it has timed out.
func (f *RotatingFile) fhTimeout(postfix string) {
	f.fhChan <- true
	defer func() { <-f.fhChan }()
	if file, ok := f.files[postfix]; ok {
		if file.timer.Stop() {
			// Timer was reset! Things may still be writing so reset the timer and don't close the file.
			file.timer.Reset(f.fileTimeout)
			return
		}
		// Time was already stopped. File is not being written to so close the file.
		file.file.Close()
	}
	delete(f.files, postfix)
}
