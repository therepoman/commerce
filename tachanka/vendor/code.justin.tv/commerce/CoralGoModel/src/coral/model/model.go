package model

import (
	"fmt"
	"github.com/pkg/errors"
	"reflect"
	"strings"
)

type Assembly interface {
	// Shape returns any shape that has been registered with the given name.
	// If no shape is registered under the given name an error is returned.
	Shape(name string) (Shape, error)
	// Op returns any operation that has been registered with the given name.
	// If no op is registered under the given name an error is returned.
	Op(name string) (Op, error)
	// RegisterShape adds a Shape of the given name to the Assembly.  An error is
	// returned if any field is not specified or if a shape for the given name has
	// already been registered.
	RegisterShape(name string, t reflect.Type, constructor func() interface{}) error
	// RegisterOp adds an operation of the given name to the Assembly.  All inputs
	// except for name are optional.  An error is returned if an operation for the given
	// name has already been registered or if any of the given shapes have not been
	// registered.
	RegisterOp(name string, input, output Shape, errs []Shape) error
	// Name of the Assembly
	Name() string
}

type Shape interface {
	// Assembly the shape is attached to.
	Assembly() Assembly
	// Name of the shape.
	Name() string
	// FullyQualifiedName of the shape: assembly.Name + "#" + shape.Name
	FullyQualifiedName() string
	// Type that the Shape represents.
	Type() reflect.Type
	// New generates an instance that is referred to by Type().
	New() interface{}
}

// Op defines an Operation.
type Op interface {
	// Assembly the operation is attached to.
	Assembly() Assembly
	// Name of the operation.
	Name() string
	// Input that the operation takes.  May be nil.
	Input() Shape
	// Output of the operation.  May be nil.
	Output() Shape
	// Errors that the operation can throw.  May be nil.
	Errors() []Shape
}

var assemblies map[string]Assembly = map[string]Assembly{}
var shapes map[reflect.Type]Shape = map[reflect.Type]Shape{}
var converters map[reflect.Type]func(reflect.Value) reflect.Value = map[reflect.Type]func(reflect.Value) reflect.Value{}

// LookupAssembly returns the Assembly structure for the given name.
// It will never return nil.
func LookupAssembly(asmName string) Assembly {
	asm := assemblies[asmName]
	if asm == nil {
		asm = &assembly{
			shapes: map[string]Shape{},
			ops:    map[string]Op{},
			name:   asmName,
		}
		assemblies[asmName] = asm
	}
	return asm
}

func GetShapeFromType(t reflect.Type) (Shape, error) {
	s, ok := shapes[t]
	if ok {
		return s, nil
	}
	return nil, errors.New("No Shape Registered for Type " + t.String())
}

func GetShapeFromFQN(defaultAsm, fqn string) (Shape, error) {
	index := strings.LastIndex(fqn, "#")
	asmName := defaultAsm
	shapeName := fqn
	if index >= 0 {
		asmName = fqn[:index]
		shapeName = fqn[index+1:]
	}
	return LookupAssembly(asmName).Shape(shapeName)
}

func (asm *assembly) Shape(name string) (Shape, error) {
	s, ok := asm.shapes[name]
	if !ok {
		return nil, errors.Errorf("No Shape registered with the name %s", name)
	}
	return s, nil
}

func (asm *assembly) Op(name string) (Op, error) {
	o, ok := asm.ops[name]
	if !ok {
		return nil, errors.Errorf("No Op registered with the name %s", name)
	}
	return o, nil
}

func (asm *assembly) RegisterShape(name string, interfaceType reflect.Type, constructor func() interface{}) error {
	_, ok := asm.shapes[name]
	interfaceType = interfaceType.Elem()
	if !ok {
		shape := &shape{
			assembly:    asm,
			name:        name,
			t:           interfaceType,
			constructor: constructor,
		}
		asm.shapes[name] = shape
		shapes[interfaceType] = shape
		implType := reflect.Indirect(reflect.ValueOf(constructor())).Type()
		shapes[implType] = shape
		return nil
	}
	return errors.Errorf("Shape already registered with the name %s", name)
}

func (asm *assembly) RegisterOp(name string, input, output Shape, errs []Shape) error {
	if name == "" {
		return errors.Errorf("Op must have a name")
	}
	if _, ok := asm.ops[name]; ok {
		return errors.Errorf("Op already registered with the name %s", name)
	}

	shapes := []Shape{input, output}
	shapes = append(shapes, errs...)
	if s := asm.isRegistered(shapes); s != nil {
		return errors.Errorf("Given a shape that is not registered: %s", s.Name())
	}
	asm.ops[name] = &op{name: name, assembly: asm, input: input, output: output, errs: errs}
	return nil
}

// isRegistered iterates over the given shapes and returns the first Shape it finds
// that is not registered with the assembly.
func (asm *assembly) isRegistered(shapes []Shape) Shape {
	for _, s := range shapes {
		if s == nil {
			continue
		}
		if _, ok := asm.shapes[s.Name()]; !ok {
			return s
		}
	}
	return nil
}

func RegisterConverter(t reflect.Type, fun func(reflect.Value) reflect.Value) {
	converters[t] = fun
}

func Convert(t reflect.Type, v reflect.Value) reflect.Value {
	return v.Convert(t)
}

var strType reflect.Type = reflect.TypeOf("")

func ErrorMessage(e interface{}) string {
	val := reflect.ValueOf(e)
	name := val.Type().String()
	//Try to call a "Message() string" function
	method := val.MethodByName("Message")
	if method.IsValid() {
		output := method.Call([]reflect.Value{})

		message := output[0]
		if message.Type().ConvertibleTo(strType) {
			messageStr, _ := message.Convert(strType).Interface().(string)
			v := fmt.Sprintf("%s: %s", name, messageStr)
			return v
		}
	}
	return name
}
