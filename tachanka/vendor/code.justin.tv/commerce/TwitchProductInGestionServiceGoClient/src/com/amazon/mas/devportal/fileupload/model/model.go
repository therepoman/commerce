package com_amazon_mas_devportal_fileupload_model

import (
	__model__ "code.justin.tv/commerce/CoralGoModel/src/coral/model"
	__reflect__ "reflect"
)

func init() {
	var val *bool
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *bool
		if f, ok := from.Interface().(*bool); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Boolean")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int64
		if f, ok := from.Interface().(*int64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Long")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int64
		if f, ok := from.Interface().(*int64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ItemId")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int64
		if f, ok := from.Interface().(*int64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to AssetIndex")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int64
		if f, ok := from.Interface().(*int64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to CasVersion")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *int64
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *int64
		if f, ok := from.Interface().(*int64); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to Size")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to SignedS3Url")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to FileType")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to AssetSubGroupName")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to FileType")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to UploadStatus")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to AssetTypeEnum")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ScanStatus")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to AssetVersion")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to UploadStatusEnum")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to AssetType")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to String")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to AssetGroupName")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to ScanStatus")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val *string
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to *string
		if f, ok := from.Interface().(*string); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to CasPutUrl")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

//An exception that represents the base for all exceptions.
type BaseException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _BaseException struct {
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_BaseException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_BaseException) Message() *string {
	return this.Ị_message
}
func (this *_BaseException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_BaseException) __type() {
}
func NewBaseException() BaseException {
	return &_BaseException{}
}
func init() {
	var val BaseException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("BaseException", t, func() interface{} {
		return NewBaseException()
	})
}

//Deprecated!!! A structure representing the details of an asset.
type Asset interface {
	__type()
	SetItemId(v *int64)
	ItemId() *int64
	SetGroupName(v *string)
	GroupName() *string
	SetSubGroupName(v *string)
	SubGroupName() *string
	SetType(v *string)
	Type() *string
	SetIndex(v *int64)
	Index() *int64
	SetVersion(v *string)
	Version() *string
	SetUploadStatus(v *string)
	UploadStatus() *string
	SetLastCompletedAssetUrl(v *string)
	LastCompletedAssetUrl() *string
}
type _Asset struct {
	Ị_itemId                *int64  `coral:"itemId" json:"itemId"`
	Ị_groupName             *string `coral:"groupName" json:"groupName"`
	Ị_subGroupName          *string `coral:"subGroupName" json:"subGroupName"`
	Ị_type                  *string `coral:"type" json:"type"`
	Ị_index                 *int64  `coral:"index" json:"index"`
	Ị_version               *string `coral:"version" json:"version"`
	Ị_uploadStatus          *string `coral:"uploadStatus" json:"uploadStatus"`
	Ị_lastCompletedAssetUrl *string `coral:"lastCompletedAssetUrl" json:"lastCompletedAssetUrl"`
}

func (this *_Asset) UploadStatus() *string {
	return this.Ị_uploadStatus
}
func (this *_Asset) SetUploadStatus(v *string) {
	this.Ị_uploadStatus = v
}
func (this *_Asset) LastCompletedAssetUrl() *string {
	return this.Ị_lastCompletedAssetUrl
}
func (this *_Asset) SetLastCompletedAssetUrl(v *string) {
	this.Ị_lastCompletedAssetUrl = v
}
func (this *_Asset) ItemId() *int64 {
	return this.Ị_itemId
}
func (this *_Asset) SetItemId(v *int64) {
	this.Ị_itemId = v
}
func (this *_Asset) GroupName() *string {
	return this.Ị_groupName
}
func (this *_Asset) SetGroupName(v *string) {
	this.Ị_groupName = v
}
func (this *_Asset) SubGroupName() *string {
	return this.Ị_subGroupName
}
func (this *_Asset) SetSubGroupName(v *string) {
	this.Ị_subGroupName = v
}
func (this *_Asset) Type() *string {
	return this.Ị_type
}
func (this *_Asset) SetType(v *string) {
	this.Ị_type = v
}
func (this *_Asset) Index() *int64 {
	return this.Ị_index
}
func (this *_Asset) SetIndex(v *int64) {
	this.Ị_index = v
}
func (this *_Asset) Version() *string {
	return this.Ị_version
}
func (this *_Asset) SetVersion(v *string) {
	this.Ị_version = v
}
func (this *_Asset) __type() {
}
func NewAsset() Asset {
	return &_Asset{}
}
func init() {
	var val Asset
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("Asset", t, func() interface{} {
		return NewAsset()
	})
}

//Request to handle SQS message from CAS with update on the Upload. Parameters:
//uploadId: Unique identifier for the upload
//assetType: AssetType enum which defines the allowable asset types to upload through FUSE
//assetVersion: The CAS version of the asset. Should be 0 as we don't allow overwriting of files in FUSE.
//scanStatus: Status provided by CAS on it's verification of the file
//overriddenAssetUrlForTesting: Optional.  Allows tester to pass in url in place of calling CAS to download asset
type HandleCasSqsMessageRequest interface {
	__type()
	SetOverriddenAssetUrlForTesting(v *string)
	OverriddenAssetUrlForTesting() *string
	SetUploadId(v *string)
	UploadId() *string
	SetAssetType(v *string)
	AssetType() *string
	SetAssetVersion(v *int64)
	AssetVersion() *int64
	SetScanStatus(v *string)
	ScanStatus() *string
}
type _HandleCasSqsMessageRequest struct {
	Ị_uploadId                     *string `coral:"uploadId" json:"uploadId"`
	Ị_assetType                    *string `coral:"assetType" json:"assetType"`
	Ị_assetVersion                 *int64  `coral:"assetVersion" json:"assetVersion"`
	Ị_scanStatus                   *string `coral:"scanStatus" json:"scanStatus"`
	Ị_overriddenAssetUrlForTesting *string `coral:"overriddenAssetUrlForTesting" json:"overriddenAssetUrlForTesting"`
}

func (this *_HandleCasSqsMessageRequest) UploadId() *string {
	return this.Ị_uploadId
}
func (this *_HandleCasSqsMessageRequest) SetUploadId(v *string) {
	this.Ị_uploadId = v
}
func (this *_HandleCasSqsMessageRequest) AssetType() *string {
	return this.Ị_assetType
}
func (this *_HandleCasSqsMessageRequest) SetAssetType(v *string) {
	this.Ị_assetType = v
}
func (this *_HandleCasSqsMessageRequest) AssetVersion() *int64 {
	return this.Ị_assetVersion
}
func (this *_HandleCasSqsMessageRequest) SetAssetVersion(v *int64) {
	this.Ị_assetVersion = v
}
func (this *_HandleCasSqsMessageRequest) ScanStatus() *string {
	return this.Ị_scanStatus
}
func (this *_HandleCasSqsMessageRequest) SetScanStatus(v *string) {
	this.Ị_scanStatus = v
}
func (this *_HandleCasSqsMessageRequest) OverriddenAssetUrlForTesting() *string {
	return this.Ị_overriddenAssetUrlForTesting
}
func (this *_HandleCasSqsMessageRequest) SetOverriddenAssetUrlForTesting(v *string) {
	this.Ị_overriddenAssetUrlForTesting = v
}
func (this *_HandleCasSqsMessageRequest) __type() {
}
func NewHandleCasSqsMessageRequest() HandleCasSqsMessageRequest {
	return &_HandleCasSqsMessageRequest{}
}
func init() {
	var val HandleCasSqsMessageRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("HandleCasSqsMessageRequest", t, func() interface{} {
		return NewHandleCasSqsMessageRequest()
	})
}

//Deprecated!!! The response corresponding to the request for a url used to upload an asset associated
//with an AppStore item.  The response includes the signed S3 URL and expiration of URL.
type GetAssetUploadUrlResponse interface {
	__type()
	SetAssetDetails(v Asset)
	AssetDetails() Asset
	SetSignedS3Url(v *string)
	SignedS3Url() *string
	SetUrlExpirationTime(v *string)
	UrlExpirationTime() *string
}
type _GetAssetUploadUrlResponse struct {
	Ị_assetDetails      Asset   `coral:"assetDetails" json:"assetDetails"`
	Ị_signedS3Url       *string `coral:"signedS3Url" json:"signedS3Url"`
	Ị_urlExpirationTime *string `coral:"urlExpirationTime" json:"urlExpirationTime"`
}

func (this *_GetAssetUploadUrlResponse) UrlExpirationTime() *string {
	return this.Ị_urlExpirationTime
}
func (this *_GetAssetUploadUrlResponse) SetUrlExpirationTime(v *string) {
	this.Ị_urlExpirationTime = v
}
func (this *_GetAssetUploadUrlResponse) AssetDetails() Asset {
	return this.Ị_assetDetails
}
func (this *_GetAssetUploadUrlResponse) SetAssetDetails(v Asset) {
	this.Ị_assetDetails = v
}
func (this *_GetAssetUploadUrlResponse) SignedS3Url() *string {
	return this.Ị_signedS3Url
}
func (this *_GetAssetUploadUrlResponse) SetSignedS3Url(v *string) {
	this.Ị_signedS3Url = v
}
func (this *_GetAssetUploadUrlResponse) __type() {
}
func NewGetAssetUploadUrlResponse() GetAssetUploadUrlResponse {
	return &_GetAssetUploadUrlResponse{}
}
func init() {
	var val GetAssetUploadUrlResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("GetAssetUploadUrlResponse", t, func() interface{} {
		return NewGetAssetUploadUrlResponse()
	})
}

//Deprecated!!! The response to the request to copy an a specific asset to an AppStore item.
//Returns if the details of the copied asset if successful.
type CopyAssetToItemResponse interface {
	__type()
	SetAssetDetails(v Asset)
	AssetDetails() Asset
}
type _CopyAssetToItemResponse struct {
	Ị_assetDetails Asset `coral:"assetDetails" json:"assetDetails"`
}

func (this *_CopyAssetToItemResponse) AssetDetails() Asset {
	return this.Ị_assetDetails
}
func (this *_CopyAssetToItemResponse) SetAssetDetails(v Asset) {
	this.Ị_assetDetails = v
}
func (this *_CopyAssetToItemResponse) __type() {
}
func NewCopyAssetToItemResponse() CopyAssetToItemResponse {
	return &_CopyAssetToItemResponse{}
}
func init() {
	var val CopyAssetToItemResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("CopyAssetToItemResponse", t, func() interface{} {
		return NewCopyAssetToItemResponse()
	})
}

//Deprecated!!! The response containing details about a specific asset.
type GetAssetDetailsResponse interface {
	__type()
	SetAssetDetails(v Asset)
	AssetDetails() Asset
}
type _GetAssetDetailsResponse struct {
	Ị_assetDetails Asset `coral:"assetDetails" json:"assetDetails"`
}

func (this *_GetAssetDetailsResponse) AssetDetails() Asset {
	return this.Ị_assetDetails
}
func (this *_GetAssetDetailsResponse) SetAssetDetails(v Asset) {
	this.Ị_assetDetails = v
}
func (this *_GetAssetDetailsResponse) __type() {
}
func NewGetAssetDetailsResponse() GetAssetDetailsResponse {
	return &_GetAssetDetailsResponse{}
}
func init() {
	var val GetAssetDetailsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("GetAssetDetailsResponse", t, func() interface{} {
		return NewGetAssetDetailsResponse()
	})
}

//Deprecated!!! Provides details about assets corresponding to a specific item and asset group.
type GetItemAssetGroupDetailsResponse interface {
	__type()
	SetAssetGroupDetails(v AssetGroup)
	AssetGroupDetails() AssetGroup
}
type _GetItemAssetGroupDetailsResponse struct {
	Ị_assetGroupDetails AssetGroup `coral:"assetGroupDetails" json:"assetGroupDetails"`
}

func (this *_GetItemAssetGroupDetailsResponse) AssetGroupDetails() AssetGroup {
	return this.Ị_assetGroupDetails
}
func (this *_GetItemAssetGroupDetailsResponse) SetAssetGroupDetails(v AssetGroup) {
	this.Ị_assetGroupDetails = v
}
func (this *_GetItemAssetGroupDetailsResponse) __type() {
}
func NewGetItemAssetGroupDetailsResponse() GetItemAssetGroupDetailsResponse {
	return &_GetItemAssetGroupDetailsResponse{}
}
func init() {
	var val GetItemAssetGroupDetailsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("GetItemAssetGroupDetailsResponse", t, func() interface{} {
		return NewGetItemAssetGroupDetailsResponse()
	})
}

//This exception is thrown whenever a dependency cannot handle
//a request from our service. Ex: The database is down.
type DependencyException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _DependencyException struct {
	InternalServiceException
	BaseException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_DependencyException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_DependencyException) Message() *string {
	return this.Ị_message
}
func (this *_DependencyException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_DependencyException) __type() {
}
func NewDependencyException() DependencyException {
	return &_DependencyException{}
}
func init() {
	var val DependencyException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("DependencyException", t, func() interface{} {
		return NewDependencyException()
	})
}

//Deprecated!!! Contains details about a specific asset group corresponding to an AppStore item.
type AssetGroup interface {
	__type()
	SetName(v *string)
	Name() *string
	SetSubGroupList(v []AssetSubGroup)
	SubGroupList() []AssetSubGroup
}
type _AssetGroup struct {
	Ị_name         *string         `coral:"name" json:"name"`
	Ị_subGroupList []AssetSubGroup `coral:"subGroupList" json:"subGroupList"`
}

func (this *_AssetGroup) Name() *string {
	return this.Ị_name
}
func (this *_AssetGroup) SetName(v *string) {
	this.Ị_name = v
}
func (this *_AssetGroup) SubGroupList() []AssetSubGroup {
	return this.Ị_subGroupList
}
func (this *_AssetGroup) SetSubGroupList(v []AssetSubGroup) {
	this.Ị_subGroupList = v
}
func (this *_AssetGroup) __type() {
}
func NewAssetGroup() AssetGroup {
	return &_AssetGroup{}
}
func init() {
	var val AssetGroup
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("AssetGroup", t, func() interface{} {
		return NewAssetGroup()
	})
}

//Deprecated!!! The request for copying a specific asset to a different AppStore item.
type CopyAssetToItemRequest interface {
	__type()
	SetFromItemId(v *int64)
	FromItemId() *int64
	SetAssetGroupName(v *string)
	AssetGroupName() *string
	SetAssetSubGroupName(v *string)
	AssetSubGroupName() *string
	SetAssetType(v *string)
	AssetType() *string
	SetAssetIndex(v *int64)
	AssetIndex() *int64
	SetToItemId(v *int64)
	ToItemId() *int64
}
type _CopyAssetToItemRequest struct {
	Ị_fromItemId        *int64  `coral:"fromItemId" json:"fromItemId"`
	Ị_assetGroupName    *string `coral:"assetGroupName" json:"assetGroupName"`
	Ị_assetSubGroupName *string `coral:"assetSubGroupName" json:"assetSubGroupName"`
	Ị_assetType         *string `coral:"assetType" json:"assetType"`
	Ị_assetIndex        *int64  `coral:"assetIndex" json:"assetIndex"`
	Ị_toItemId          *int64  `coral:"toItemId" json:"toItemId"`
}

func (this *_CopyAssetToItemRequest) AssetGroupName() *string {
	return this.Ị_assetGroupName
}
func (this *_CopyAssetToItemRequest) SetAssetGroupName(v *string) {
	this.Ị_assetGroupName = v
}
func (this *_CopyAssetToItemRequest) AssetSubGroupName() *string {
	return this.Ị_assetSubGroupName
}
func (this *_CopyAssetToItemRequest) SetAssetSubGroupName(v *string) {
	this.Ị_assetSubGroupName = v
}
func (this *_CopyAssetToItemRequest) AssetType() *string {
	return this.Ị_assetType
}
func (this *_CopyAssetToItemRequest) SetAssetType(v *string) {
	this.Ị_assetType = v
}
func (this *_CopyAssetToItemRequest) AssetIndex() *int64 {
	return this.Ị_assetIndex
}
func (this *_CopyAssetToItemRequest) SetAssetIndex(v *int64) {
	this.Ị_assetIndex = v
}
func (this *_CopyAssetToItemRequest) ToItemId() *int64 {
	return this.Ị_toItemId
}
func (this *_CopyAssetToItemRequest) SetToItemId(v *int64) {
	this.Ị_toItemId = v
}
func (this *_CopyAssetToItemRequest) FromItemId() *int64 {
	return this.Ị_fromItemId
}
func (this *_CopyAssetToItemRequest) SetFromItemId(v *int64) {
	this.Ị_fromItemId = v
}
func (this *_CopyAssetToItemRequest) __type() {
}
func NewCopyAssetToItemRequest() CopyAssetToItemRequest {
	return &_CopyAssetToItemRequest{}
}
func init() {
	var val CopyAssetToItemRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("CopyAssetToItemRequest", t, func() interface{} {
		return NewCopyAssetToItemRequest()
	})
}

//Deprecated!!! The request for getting a signed S3 Url to upload asset and associate it with
//an Amazon AppStore item.
type GetAssetUploadUrlRequest interface {
	__type()
	SetItemId(v *int64)
	ItemId() *int64
	SetAssetGroupName(v *string)
	AssetGroupName() *string
	SetAssetSubGroupName(v *string)
	AssetSubGroupName() *string
	SetAssetType(v *string)
	AssetType() *string
	SetAssetIndex(v *int64)
	AssetIndex() *int64
	SetFileType(v *string)
	FileType() *string
}
type _GetAssetUploadUrlRequest struct {
	Ị_assetGroupName    *string `coral:"assetGroupName" json:"assetGroupName"`
	Ị_assetSubGroupName *string `coral:"assetSubGroupName" json:"assetSubGroupName"`
	Ị_assetType         *string `coral:"assetType" json:"assetType"`
	Ị_assetIndex        *int64  `coral:"assetIndex" json:"assetIndex"`
	Ị_fileType          *string `coral:"fileType" json:"fileType"`
	Ị_itemId            *int64  `coral:"itemId" json:"itemId"`
}

func (this *_GetAssetUploadUrlRequest) AssetType() *string {
	return this.Ị_assetType
}
func (this *_GetAssetUploadUrlRequest) SetAssetType(v *string) {
	this.Ị_assetType = v
}
func (this *_GetAssetUploadUrlRequest) AssetIndex() *int64 {
	return this.Ị_assetIndex
}
func (this *_GetAssetUploadUrlRequest) SetAssetIndex(v *int64) {
	this.Ị_assetIndex = v
}
func (this *_GetAssetUploadUrlRequest) FileType() *string {
	return this.Ị_fileType
}
func (this *_GetAssetUploadUrlRequest) SetFileType(v *string) {
	this.Ị_fileType = v
}
func (this *_GetAssetUploadUrlRequest) ItemId() *int64 {
	return this.Ị_itemId
}
func (this *_GetAssetUploadUrlRequest) SetItemId(v *int64) {
	this.Ị_itemId = v
}
func (this *_GetAssetUploadUrlRequest) AssetGroupName() *string {
	return this.Ị_assetGroupName
}
func (this *_GetAssetUploadUrlRequest) SetAssetGroupName(v *string) {
	this.Ị_assetGroupName = v
}
func (this *_GetAssetUploadUrlRequest) AssetSubGroupName() *string {
	return this.Ị_assetSubGroupName
}
func (this *_GetAssetUploadUrlRequest) SetAssetSubGroupName(v *string) {
	this.Ị_assetSubGroupName = v
}
func (this *_GetAssetUploadUrlRequest) __type() {
}
func NewGetAssetUploadUrlRequest() GetAssetUploadUrlRequest {
	return &_GetAssetUploadUrlRequest{}
}
func init() {
	var val GetAssetUploadUrlRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("GetAssetUploadUrlRequest", t, func() interface{} {
		return NewGetAssetUploadUrlRequest()
	})
}

//Deprecated!!! The parameters detailing the uploaded asset and details about the file.
type ProcessSQSUploadCompleteMessageRequest interface {
	__type()
	SetAssetIndex(v *int64)
	AssetIndex() *int64
	SetAssetVersion(v *int64)
	AssetVersion() *int64
	SetSize(v *int64)
	Size() *int64
	SetChecksum(v *string)
	Checksum() *string
	SetItemId(v *int64)
	ItemId() *int64
	SetAssetGroupName(v *string)
	AssetGroupName() *string
	SetAssetSubGroupName(v *string)
	AssetSubGroupName() *string
	SetAssetType(v *string)
	AssetType() *string
}
type _ProcessSQSUploadCompleteMessageRequest struct {
	Ị_itemId            *int64  `coral:"itemId" json:"itemId"`
	Ị_assetGroupName    *string `coral:"assetGroupName" json:"assetGroupName"`
	Ị_assetSubGroupName *string `coral:"assetSubGroupName" json:"assetSubGroupName"`
	Ị_assetType         *string `coral:"assetType" json:"assetType"`
	Ị_assetIndex        *int64  `coral:"assetIndex" json:"assetIndex"`
	Ị_assetVersion      *int64  `coral:"assetVersion" json:"assetVersion"`
	Ị_size              *int64  `coral:"size" json:"size"`
	Ị_checksum          *string `coral:"checksum" json:"checksum"`
}

func (this *_ProcessSQSUploadCompleteMessageRequest) Checksum() *string {
	return this.Ị_checksum
}
func (this *_ProcessSQSUploadCompleteMessageRequest) SetChecksum(v *string) {
	this.Ị_checksum = v
}
func (this *_ProcessSQSUploadCompleteMessageRequest) ItemId() *int64 {
	return this.Ị_itemId
}
func (this *_ProcessSQSUploadCompleteMessageRequest) SetItemId(v *int64) {
	this.Ị_itemId = v
}
func (this *_ProcessSQSUploadCompleteMessageRequest) AssetGroupName() *string {
	return this.Ị_assetGroupName
}
func (this *_ProcessSQSUploadCompleteMessageRequest) SetAssetGroupName(v *string) {
	this.Ị_assetGroupName = v
}
func (this *_ProcessSQSUploadCompleteMessageRequest) AssetSubGroupName() *string {
	return this.Ị_assetSubGroupName
}
func (this *_ProcessSQSUploadCompleteMessageRequest) SetAssetSubGroupName(v *string) {
	this.Ị_assetSubGroupName = v
}
func (this *_ProcessSQSUploadCompleteMessageRequest) AssetType() *string {
	return this.Ị_assetType
}
func (this *_ProcessSQSUploadCompleteMessageRequest) SetAssetType(v *string) {
	this.Ị_assetType = v
}
func (this *_ProcessSQSUploadCompleteMessageRequest) AssetIndex() *int64 {
	return this.Ị_assetIndex
}
func (this *_ProcessSQSUploadCompleteMessageRequest) SetAssetIndex(v *int64) {
	this.Ị_assetIndex = v
}
func (this *_ProcessSQSUploadCompleteMessageRequest) AssetVersion() *int64 {
	return this.Ị_assetVersion
}
func (this *_ProcessSQSUploadCompleteMessageRequest) SetAssetVersion(v *int64) {
	this.Ị_assetVersion = v
}
func (this *_ProcessSQSUploadCompleteMessageRequest) Size() *int64 {
	return this.Ị_size
}
func (this *_ProcessSQSUploadCompleteMessageRequest) SetSize(v *int64) {
	this.Ị_size = v
}
func (this *_ProcessSQSUploadCompleteMessageRequest) __type() {
}
func NewProcessSQSUploadCompleteMessageRequest() ProcessSQSUploadCompleteMessageRequest {
	return &_ProcessSQSUploadCompleteMessageRequest{}
}
func init() {
	var val ProcessSQSUploadCompleteMessageRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("ProcessSQSUploadCompleteMessageRequest", t, func() interface{} {
		return NewProcessSQSUploadCompleteMessageRequest()
	})
}

//Deprecated!!! Provides details about assets corresponding to a specific item, asset group, and sub group.
type GetItemAssetSubGroupDetailsResponse interface {
	__type()
	SetAssetSubGroupDetails(v AssetSubGroup)
	AssetSubGroupDetails() AssetSubGroup
}
type _GetItemAssetSubGroupDetailsResponse struct {
	Ị_assetSubGroupDetails AssetSubGroup `coral:"assetSubGroupDetails" json:"assetSubGroupDetails"`
}

func (this *_GetItemAssetSubGroupDetailsResponse) AssetSubGroupDetails() AssetSubGroup {
	return this.Ị_assetSubGroupDetails
}
func (this *_GetItemAssetSubGroupDetailsResponse) SetAssetSubGroupDetails(v AssetSubGroup) {
	this.Ị_assetSubGroupDetails = v
}
func (this *_GetItemAssetSubGroupDetailsResponse) __type() {
}
func NewGetItemAssetSubGroupDetailsResponse() GetItemAssetSubGroupDetailsResponse {
	return &_GetItemAssetSubGroupDetailsResponse{}
}
func init() {
	var val GetItemAssetSubGroupDetailsResponse
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("GetItemAssetSubGroupDetailsResponse", t, func() interface{} {
		return NewGetItemAssetSubGroupDetailsResponse()
	})
}

//This exception is thrown when a dependent system throws a Retryable exception, so the API can be called again
//safely.  One common example would be to throw this on receiving an opt-lock failure retryable from DADS.
type RetryableDependencyException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _RetryableDependencyException struct {
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_RetryableDependencyException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_RetryableDependencyException) Message() *string {
	return this.Ị_message
}
func (this *_RetryableDependencyException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_RetryableDependencyException) __type() {
}
func NewRetryableDependencyException() RetryableDependencyException {
	return &_RetryableDependencyException{}
}
func init() {
	var val RetryableDependencyException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("RetryableDependencyException", t, func() interface{} {
		return NewRetryableDependencyException()
	})
}

//Deprecated!!! Contains details about a specific asset group and sub group corresponding
//to an AppStore item.
type AssetSubGroup interface {
	__type()
	SetName(v *string)
	Name() *string
	SetAssetList(v []Asset)
	AssetList() []Asset
}
type _AssetSubGroup struct {
	Ị_name      *string `coral:"name" json:"name"`
	Ị_assetList []Asset `coral:"assetList" json:"assetList"`
}

func (this *_AssetSubGroup) Name() *string {
	return this.Ị_name
}
func (this *_AssetSubGroup) SetName(v *string) {
	this.Ị_name = v
}
func (this *_AssetSubGroup) AssetList() []Asset {
	return this.Ị_assetList
}
func (this *_AssetSubGroup) SetAssetList(v []Asset) {
	this.Ị_assetList = v
}
func (this *_AssetSubGroup) __type() {
}
func NewAssetSubGroup() AssetSubGroup {
	return &_AssetSubGroup{}
}
func init() {
	var val AssetSubGroup
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("AssetSubGroup", t, func() interface{} {
		return NewAssetSubGroup()
	})
}

//Request to create a new upload. Parameters:
//assetType: Type of asset to be uploaded.
//fileType: MIME file type of asset to be uploaded.
//tenantId: Which tenant is calling this operation.
type CreateUploadRequest interface {
	__type()
	SetAssetType(v *string)
	AssetType() *string
	SetFileType(v *string)
	FileType() *string
	SetTenantId(v *string)
	TenantId() *string
}
type _CreateUploadRequest struct {
	Ị_assetType *string `coral:"assetType" json:"assetType"`
	Ị_fileType  *string `coral:"fileType" json:"fileType"`
	Ị_tenantId  *string `coral:"tenantId" json:"tenantId"`
}

func (this *_CreateUploadRequest) AssetType() *string {
	return this.Ị_assetType
}
func (this *_CreateUploadRequest) SetAssetType(v *string) {
	this.Ị_assetType = v
}
func (this *_CreateUploadRequest) FileType() *string {
	return this.Ị_fileType
}
func (this *_CreateUploadRequest) SetFileType(v *string) {
	this.Ị_fileType = v
}
func (this *_CreateUploadRequest) TenantId() *string {
	return this.Ị_tenantId
}
func (this *_CreateUploadRequest) SetTenantId(v *string) {
	this.Ị_tenantId = v
}
func (this *_CreateUploadRequest) __type() {
}
func NewCreateUploadRequest() CreateUploadRequest {
	return &_CreateUploadRequest{}
}
func init() {
	var val CreateUploadRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("CreateUploadRequest", t, func() interface{} {
		return NewCreateUploadRequest()
	})
}

//Indicates a client's request to a service is invalid. By throwing an
//InvalidRequestException the service is saying the caller is at fault.
//Thus, this exception will not affect the service's availability score.
type InvalidRequestException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _InvalidRequestException struct {
	BaseException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_InvalidRequestException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_InvalidRequestException) Message() *string {
	return this.Ị_message
}
func (this *_InvalidRequestException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_InvalidRequestException) __type() {
}
func NewInvalidRequestException() InvalidRequestException {
	return &_InvalidRequestException{}
}
func init() {
	var val InvalidRequestException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("InvalidRequestException", t, func() interface{} {
		return NewInvalidRequestException()
	})
}

//UploadResource is the resource returned by FUSE to show the state of an upload,
//along with all other related metadata. This is the only resource returned by FUSE 2.0.
type UploadResource interface {
	__type()
	SetFileSize(v *int64)
	FileSize() *int64
	SetSha1(v *string)
	Sha1() *string
	SetAssetType(v *string)
	AssetType() *string
	SetFileType(v *string)
	FileType() *string
	SetAssetServiceId(v *string)
	AssetServiceId() *string
	SetTenantId(v *string)
	TenantId() *string
	SetExternalAssetServiceId(v *string)
	ExternalAssetServiceId() *string
	SetUploadId(v *string)
	UploadId() *string
	SetCasPutUrl(v *string)
	CasPutUrl() *string
	SetUploadStatus(v *string)
	UploadStatus() *string
}
type _UploadResource struct {
	Ị_casPutUrl              *string `coral:"casPutUrl" json:"casPutUrl"`
	Ị_uploadStatus           *string `coral:"uploadStatus" json:"uploadStatus"`
	Ị_tenantId               *string `coral:"tenantId" json:"tenantId"`
	Ị_externalAssetServiceId *string `coral:"externalAssetServiceId" json:"externalAssetServiceId"`
	Ị_uploadId               *string `coral:"uploadId" json:"uploadId"`
	Ị_fileType               *string `coral:"fileType" json:"fileType"`
	Ị_assetServiceId         *string `coral:"assetServiceId" json:"assetServiceId"`
	Ị_fileSize               *int64  `coral:"fileSize" json:"fileSize"`
	Ị_sha1                   *string `coral:"sha1" json:"sha1"`
	Ị_assetType              *string `coral:"assetType" json:"assetType"`
}

func (this *_UploadResource) UploadStatus() *string {
	return this.Ị_uploadStatus
}
func (this *_UploadResource) SetUploadStatus(v *string) {
	this.Ị_uploadStatus = v
}
func (this *_UploadResource) TenantId() *string {
	return this.Ị_tenantId
}
func (this *_UploadResource) SetTenantId(v *string) {
	this.Ị_tenantId = v
}
func (this *_UploadResource) ExternalAssetServiceId() *string {
	return this.Ị_externalAssetServiceId
}
func (this *_UploadResource) SetExternalAssetServiceId(v *string) {
	this.Ị_externalAssetServiceId = v
}
func (this *_UploadResource) UploadId() *string {
	return this.Ị_uploadId
}
func (this *_UploadResource) SetUploadId(v *string) {
	this.Ị_uploadId = v
}
func (this *_UploadResource) CasPutUrl() *string {
	return this.Ị_casPutUrl
}
func (this *_UploadResource) SetCasPutUrl(v *string) {
	this.Ị_casPutUrl = v
}
func (this *_UploadResource) AssetServiceId() *string {
	return this.Ị_assetServiceId
}
func (this *_UploadResource) SetAssetServiceId(v *string) {
	this.Ị_assetServiceId = v
}
func (this *_UploadResource) FileSize() *int64 {
	return this.Ị_fileSize
}
func (this *_UploadResource) SetFileSize(v *int64) {
	this.Ị_fileSize = v
}
func (this *_UploadResource) Sha1() *string {
	return this.Ị_sha1
}
func (this *_UploadResource) SetSha1(v *string) {
	this.Ị_sha1 = v
}
func (this *_UploadResource) AssetType() *string {
	return this.Ị_assetType
}
func (this *_UploadResource) SetAssetType(v *string) {
	this.Ị_assetType = v
}
func (this *_UploadResource) FileType() *string {
	return this.Ị_fileType
}
func (this *_UploadResource) SetFileType(v *string) {
	this.Ị_fileType = v
}
func (this *_UploadResource) __type() {
}
func NewUploadResource() UploadResource {
	return &_UploadResource{}
}
func init() {
	var val UploadResource
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("UploadResource", t, func() interface{} {
		return NewUploadResource()
	})
}

//Request to get the upload resource corresponding to uploadId. Parameters:
//uploadId: Upload Resource to retrieve.
type GetUploadRequest interface {
	__type()
	SetUploadId(v *string)
	UploadId() *string
}
type _GetUploadRequest struct {
	Ị_uploadId *string `coral:"uploadId" json:"uploadId"`
}

func (this *_GetUploadRequest) UploadId() *string {
	return this.Ị_uploadId
}
func (this *_GetUploadRequest) SetUploadId(v *string) {
	this.Ị_uploadId = v
}
func (this *_GetUploadRequest) __type() {
}
func NewGetUploadRequest() GetUploadRequest {
	return &_GetUploadRequest{}
}
func init() {
	var val GetUploadRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("GetUploadRequest", t, func() interface{} {
		return NewGetUploadRequest()
	})
}

//Deprecated!!! The parameters detailing the completed processing of the asset by CAS.
type ProcessSQSCASCompleteMessageRequest interface {
	__type()
	SetScanStatus(v *string)
	ScanStatus() *string
	SetItemId(v *int64)
	ItemId() *int64
	SetAssetGroupName(v *string)
	AssetGroupName() *string
	SetAssetSubGroupName(v *string)
	AssetSubGroupName() *string
	SetAssetType(v *string)
	AssetType() *string
	SetAssetIndex(v *int64)
	AssetIndex() *int64
	SetAssetVersion(v *int64)
	AssetVersion() *int64
}
type _ProcessSQSCASCompleteMessageRequest struct {
	Ị_assetIndex        *int64  `coral:"assetIndex" json:"assetIndex"`
	Ị_assetVersion      *int64  `coral:"assetVersion" json:"assetVersion"`
	Ị_scanStatus        *string `coral:"scanStatus" json:"scanStatus"`
	Ị_itemId            *int64  `coral:"itemId" json:"itemId"`
	Ị_assetGroupName    *string `coral:"assetGroupName" json:"assetGroupName"`
	Ị_assetSubGroupName *string `coral:"assetSubGroupName" json:"assetSubGroupName"`
	Ị_assetType         *string `coral:"assetType" json:"assetType"`
}

func (this *_ProcessSQSCASCompleteMessageRequest) AssetGroupName() *string {
	return this.Ị_assetGroupName
}
func (this *_ProcessSQSCASCompleteMessageRequest) SetAssetGroupName(v *string) {
	this.Ị_assetGroupName = v
}
func (this *_ProcessSQSCASCompleteMessageRequest) AssetSubGroupName() *string {
	return this.Ị_assetSubGroupName
}
func (this *_ProcessSQSCASCompleteMessageRequest) SetAssetSubGroupName(v *string) {
	this.Ị_assetSubGroupName = v
}
func (this *_ProcessSQSCASCompleteMessageRequest) AssetType() *string {
	return this.Ị_assetType
}
func (this *_ProcessSQSCASCompleteMessageRequest) SetAssetType(v *string) {
	this.Ị_assetType = v
}
func (this *_ProcessSQSCASCompleteMessageRequest) AssetIndex() *int64 {
	return this.Ị_assetIndex
}
func (this *_ProcessSQSCASCompleteMessageRequest) SetAssetIndex(v *int64) {
	this.Ị_assetIndex = v
}
func (this *_ProcessSQSCASCompleteMessageRequest) AssetVersion() *int64 {
	return this.Ị_assetVersion
}
func (this *_ProcessSQSCASCompleteMessageRequest) SetAssetVersion(v *int64) {
	this.Ị_assetVersion = v
}
func (this *_ProcessSQSCASCompleteMessageRequest) ScanStatus() *string {
	return this.Ị_scanStatus
}
func (this *_ProcessSQSCASCompleteMessageRequest) SetScanStatus(v *string) {
	this.Ị_scanStatus = v
}
func (this *_ProcessSQSCASCompleteMessageRequest) ItemId() *int64 {
	return this.Ị_itemId
}
func (this *_ProcessSQSCASCompleteMessageRequest) SetItemId(v *int64) {
	this.Ị_itemId = v
}
func (this *_ProcessSQSCASCompleteMessageRequest) __type() {
}
func NewProcessSQSCASCompleteMessageRequest() ProcessSQSCASCompleteMessageRequest {
	return &_ProcessSQSCASCompleteMessageRequest{}
}
func init() {
	var val ProcessSQSCASCompleteMessageRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("ProcessSQSCASCompleteMessageRequest", t, func() interface{} {
		return NewProcessSQSCASCompleteMessageRequest()
	})
}

//Deprecated!!! A request defining a specific asset to fetch details about.
type GetAssetDetailsRequest interface {
	__type()
	SetItemId(v *int64)
	ItemId() *int64
	SetAssetGroupName(v *string)
	AssetGroupName() *string
	SetAssetSubGroupName(v *string)
	AssetSubGroupName() *string
	SetAssetType(v *string)
	AssetType() *string
	SetAssetIndex(v *int64)
	AssetIndex() *int64
}
type _GetAssetDetailsRequest struct {
	Ị_itemId            *int64  `coral:"itemId" json:"itemId"`
	Ị_assetGroupName    *string `coral:"assetGroupName" json:"assetGroupName"`
	Ị_assetSubGroupName *string `coral:"assetSubGroupName" json:"assetSubGroupName"`
	Ị_assetType         *string `coral:"assetType" json:"assetType"`
	Ị_assetIndex        *int64  `coral:"assetIndex" json:"assetIndex"`
}

func (this *_GetAssetDetailsRequest) ItemId() *int64 {
	return this.Ị_itemId
}
func (this *_GetAssetDetailsRequest) SetItemId(v *int64) {
	this.Ị_itemId = v
}
func (this *_GetAssetDetailsRequest) AssetGroupName() *string {
	return this.Ị_assetGroupName
}
func (this *_GetAssetDetailsRequest) SetAssetGroupName(v *string) {
	this.Ị_assetGroupName = v
}
func (this *_GetAssetDetailsRequest) AssetSubGroupName() *string {
	return this.Ị_assetSubGroupName
}
func (this *_GetAssetDetailsRequest) SetAssetSubGroupName(v *string) {
	this.Ị_assetSubGroupName = v
}
func (this *_GetAssetDetailsRequest) AssetType() *string {
	return this.Ị_assetType
}
func (this *_GetAssetDetailsRequest) SetAssetType(v *string) {
	this.Ị_assetType = v
}
func (this *_GetAssetDetailsRequest) AssetIndex() *int64 {
	return this.Ị_assetIndex
}
func (this *_GetAssetDetailsRequest) SetAssetIndex(v *int64) {
	this.Ị_assetIndex = v
}
func (this *_GetAssetDetailsRequest) __type() {
}
func NewGetAssetDetailsRequest() GetAssetDetailsRequest {
	return &_GetAssetDetailsRequest{}
}
func init() {
	var val GetAssetDetailsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("GetAssetDetailsRequest", t, func() interface{} {
		return NewGetAssetDetailsRequest()
	})
}

//Deprecated!!! A request defining the specific item and asset group to fetch details about.
type GetItemAssetGroupDetailsRequest interface {
	__type()
	SetItemId(v *int64)
	ItemId() *int64
	SetAssetGroupName(v *string)
	AssetGroupName() *string
}
type _GetItemAssetGroupDetailsRequest struct {
	Ị_itemId         *int64  `coral:"itemId" json:"itemId"`
	Ị_assetGroupName *string `coral:"assetGroupName" json:"assetGroupName"`
}

func (this *_GetItemAssetGroupDetailsRequest) ItemId() *int64 {
	return this.Ị_itemId
}
func (this *_GetItemAssetGroupDetailsRequest) SetItemId(v *int64) {
	this.Ị_itemId = v
}
func (this *_GetItemAssetGroupDetailsRequest) AssetGroupName() *string {
	return this.Ị_assetGroupName
}
func (this *_GetItemAssetGroupDetailsRequest) SetAssetGroupName(v *string) {
	this.Ị_assetGroupName = v
}
func (this *_GetItemAssetGroupDetailsRequest) __type() {
}
func NewGetItemAssetGroupDetailsRequest() GetItemAssetGroupDetailsRequest {
	return &_GetItemAssetGroupDetailsRequest{}
}
func init() {
	var val GetItemAssetGroupDetailsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("GetItemAssetGroupDetailsRequest", t, func() interface{} {
		return NewGetItemAssetGroupDetailsRequest()
	})
}

//Deprecated!!! A request defining the specific item, asset group and sub group to fetch details about.
type GetItemAssetSubGroupDetailsRequest interface {
	__type()
	SetItemId(v *int64)
	ItemId() *int64
	SetAssetGroupName(v *string)
	AssetGroupName() *string
	SetAssetSubGroupName(v *string)
	AssetSubGroupName() *string
}
type _GetItemAssetSubGroupDetailsRequest struct {
	Ị_assetSubGroupName *string `coral:"assetSubGroupName" json:"assetSubGroupName"`
	Ị_itemId            *int64  `coral:"itemId" json:"itemId"`
	Ị_assetGroupName    *string `coral:"assetGroupName" json:"assetGroupName"`
}

func (this *_GetItemAssetSubGroupDetailsRequest) ItemId() *int64 {
	return this.Ị_itemId
}
func (this *_GetItemAssetSubGroupDetailsRequest) SetItemId(v *int64) {
	this.Ị_itemId = v
}
func (this *_GetItemAssetSubGroupDetailsRequest) AssetGroupName() *string {
	return this.Ị_assetGroupName
}
func (this *_GetItemAssetSubGroupDetailsRequest) SetAssetGroupName(v *string) {
	this.Ị_assetGroupName = v
}
func (this *_GetItemAssetSubGroupDetailsRequest) AssetSubGroupName() *string {
	return this.Ị_assetSubGroupName
}
func (this *_GetItemAssetSubGroupDetailsRequest) SetAssetSubGroupName(v *string) {
	this.Ị_assetSubGroupName = v
}
func (this *_GetItemAssetSubGroupDetailsRequest) __type() {
}
func NewGetItemAssetSubGroupDetailsRequest() GetItemAssetSubGroupDetailsRequest {
	return &_GetItemAssetSubGroupDetailsRequest{}
}
func init() {
	var val GetItemAssetSubGroupDetailsRequest
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("GetItemAssetSubGroupDetailsRequest", t, func() interface{} {
		return NewGetItemAssetSubGroupDetailsRequest()
	})
}

//This exception is thrown if the service fails to handle a request.
//
//This exception is thrown if something wrong happens in our system. This
//normally is our fault, not the client.
type InternalServiceException interface {
	__type()
	error
	SetMessage(v *string)
	Message() *string
}
type _InternalServiceException struct {
	BaseException
	Ị_message *string `coral:"message" json:"message"`
}

func (this *_InternalServiceException) Error() string {
	return __model__.ErrorMessage(this)
}
func (this *_InternalServiceException) Message() *string {
	return this.Ị_message
}
func (this *_InternalServiceException) SetMessage(v *string) {
	this.Ị_message = v
}
func (this *_InternalServiceException) __type() {
}
func NewInternalServiceException() InternalServiceException {
	return &_InternalServiceException{}
}
func init() {
	var val InternalServiceException
	t := __reflect__.TypeOf(&val)
	__model__.LookupAssembly("com.amazon.mas.devportal.fileupload.model").RegisterShape("InternalServiceException", t, func() interface{} {
		return NewInternalServiceException()
	})
}
func init() {
	var val []Asset
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []Asset
		if f, ok := from.Interface().([]Asset); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to AssetList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}
func init() {
	var val []AssetSubGroup
	t := __reflect__.TypeOf(&val).Elem()
	fun := func(from __reflect__.Value) __reflect__.Value {
		var to []AssetSubGroup
		if f, ok := from.Interface().([]AssetSubGroup); ok {
			to = f
		} else {
			panic("Can't Convert from " + from.Type().Name() + " to AssetSubGroupList")
		}
		return __reflect__.ValueOf(to)
	}
	__model__.RegisterConverter(t, fun)
}

//Provides a service to support asset (aka file) upload for the Amazon AppStore.
//The service provides a signed S3 URL for the upload of an asset. After
//the file is uploaded, the service processes the file and associates
//it with an AppStore item.
type MASFileUploadService interface { //Deprecated!!! Processes an message indicating a file upload was completed.
	ProcessSQSUploadCompleteMessage(input ProcessSQSUploadCompleteMessageRequest) error //Processes an message indicating a file upload was completed.
	HandleCasSqsMessage(input HandleCasSqsMessageRequest) (UploadResource, error)       //Retrieves the upload resource for the given upload id.
	GetUpload(input GetUploadRequest) (UploadResource, error)                           //Starts the upload process and retrieves the PUT URL.
	CreateUpload(input CreateUploadRequest) (UploadResource, error)                     //Deprecated!!! Initiates the upload of an asset for an item in the AppStore. Returns a signed S3 URL
	//that can be used to upload the asset.
	GetAssetUploadUrl(input GetAssetUploadUrlRequest) (GetAssetUploadUrlResponse, error)                      //Deprecated!!! Provides details about all assets of a particular asset group for an AppStore item.
	GetItemAssetGroupDetails(input GetItemAssetGroupDetailsRequest) (GetItemAssetGroupDetailsResponse, error) //Deprecated!!! Provides details about all assets of a particular asset group and sub group for an
	//individual AppStore item.
	GetItemAssetSubGroupDetails(input GetItemAssetSubGroupDetailsRequest) (GetItemAssetSubGroupDetailsResponse, error) //Deprecated!!!  Processes an message indicating a file upload was completed and the file was verified to
	//meet requirements by CAS.
	ProcessSQSCASCompleteMessage(input ProcessSQSCASCompleteMessageRequest) error //Deprecated!!! Copies a specific asset already uploaded for an AppStore item to a different AppStore
	//item. This allows the same asset to be resused for multiple AppStore items.
	CopyAssetToItem(input CopyAssetToItemRequest) (CopyAssetToItemResponse, error) //Deprecated!!! Provides the status and details about an specific asset releated to an AppStore item.
	GetAssetDetails(input GetAssetDetailsRequest) (GetAssetDetailsResponse, error)
}
