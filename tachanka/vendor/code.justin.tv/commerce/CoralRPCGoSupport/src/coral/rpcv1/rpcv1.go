/* Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved. */
package rpcv1

import (
	"code.justin.tv/commerce/AAAGo/src/aaa"
	"code.justin.tv/commerce/GoAuthV4/src/authv4"
	"code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
)

type BasicAuth struct {
	Username, Password string
}

// Assert that we fulfill the interfaces
var _ codec.Codec = RPCv1{}
var _ codec.RoundTripper = RPCv1{}
var _ codec.Server = RPCv1{}

// https://w.amazon.com/?Coral/Protocols#RPCv1
type RPCv1 struct {
	Host          string         // required, host or host:port
	Path          string         // optional
	SignerV4      *authv4.Signer // optional
	AuthInfo      *BasicAuth     // optional
	SecurityToken string         // optional, see https://w.amazon.com/index.php/Coral/Specifications/HttpSecurityToken
	AAAClient     aaa.Client     // optional
}

// Represents a construction option.
type Option func(*RPCv1)

/*
   Example: rpc := rpcv1.New("foo.com:8080", rpcv1.SetSignerV4(signer))
*/
func New(host string, options ...Option) RPCv1 {

	rpcv1 := RPCv1{Host: host}

	for _, option := range options {
		option(&rpcv1)
	}

	return rpcv1
}

func SetPath(path string) Option {
	return (Option)(func(rpcv1 *RPCv1) {
		rpcv1.Path = path
	})
}

func SetSignerV4(signer *authv4.Signer) Option {
	return (Option)(func(rpcv1 *RPCv1) {
		rpcv1.SignerV4 = signer
	})
}

func SetBasicAuth(ba *BasicAuth) Option {
	return (Option)(func(rpcv1 *RPCv1) {
		rpcv1.AuthInfo = ba
	})
}

func SetSecurityToken(securityToken string) Option {
	return (Option)(func(rpcv1 *RPCv1) {
		rpcv1.SecurityToken = securityToken
	})
}

func SetAAAClient(aaaClient aaa.Client) Option {
	return (Option)(func(rpcv1 *RPCv1) {
		rpcv1.AAAClient = aaaClient
	})
}
