/* Copyright 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved. */
package rpcv1

import (
	"code.justin.tv/commerce/GoLog/src/log"
	"code.justin.tv/commerce/AAAGo/src/aaa"
	"code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	"code.justin.tv/commerce/CoralGoModel/src/coral/model"
	cjson "code.justin.tv/commerce/CoralRPCGoSupport/src/coral/rpc/encoding/json"
	"bufio"
	"bytes"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/pkg/errors"
)

const (
	headerAccept          = "Accept"
	headerAmznDate        = "X-Amz-Date"
	headerAmznToken       = "X-Amz-Security-Token"
	headerAmznTarget      = "X-Amz-Target"
	headerContentType     = "Content-Type"
	headerContentEncoding = "Content-Encoding"

	contentType     = "application/json"
	contentEncoding = "amz-1.0"
)

// Marshal supports codec.Codec.
func (c RPCv1) Marshal(obj interface{}) ([]byte, error) {
	return cjson.Marshal(obj)
}

// Unmarshal supports codec.Codec.
func (c RPCv1) Unmarshal(d []byte, obj interface{}) error {
	return cjson.Unmarshal(d, obj)
}

// IsSupported supports codec.Codec.
// POST http://0.0.0.0:8000/
//
// HTTP/1.1
// Content-Type: application/json; charset=UTF-8
// Content-Encoding: amz-1.0
// X-Amz-Date: Thu, 01 Mar 2012 22:07:13 GMT
// X-Amz-Target: com.amazon.coral.demo.WeatherService.GetWeather
//
// {"location": "foo"}
func (c RPCv1) IsSupported(g codec.Getter) bool {
	if !strings.Contains(g.Get(headerContentType), contentType) {
		return false
	}

	if g.Get(headerContentEncoding) != contentEncoding {
		return false
	}

	// We don't care about these values, but we care that they are present.
	assembly, service, op := GetServiceAndOp(g.Get(headerAmznTarget))
	if g.Get(headerAmznDate) == "" || assembly == "" || service == "" || op == "" {
		return false
	}

	return true
}

// GetServiceAndOp splits the given val based on the location of the last period character.
// If there is no period character, then empty strings are returned.
func GetServiceAndOp(val string) (assembly, service, operation string) {
	if i := strings.LastIndex(val, "."); i > 0 && i < len(val)-1 {
		service, operation = val[:i], val[i+1:]
	}
	if i := strings.LastIndex(service, "."); i > 0 && i < len(service)-1 {
		assembly, service = service[:i], service[i+1:]
		return
	}
	return "", "", ""
}

// UnmarshalRequest satisfies codec.Server.
func (c RPCv1) UnmarshalRequest(r *http.Request) (*codec.Request, error) {
	asmName, serviceName, opName := GetServiceAndOp(r.Header.Get(headerAmznTarget))
	if serviceName == "" || opName == "" {
		return nil, errors.New("Request is not supported by the RPCv1 codec")
	}

	defer r.Body.Close()
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}

	if c.SignerV4 != nil {
		// TODO: Check for and handle AuthV4 signing.
	}

	if c.AAAClient != nil {
		// TODO: Check for and handle AAA signing.
	}

	asm := model.LookupAssembly(asmName)
	op, err := asm.Op(opName)
	if err != nil {
		return nil, errors.Wrap(err, "Unable to retrieve operation "+opName)
	}

	// Create the base codec.Request, then create instances of the input
	// and output if they are defined for the operation.
	cr := &codec.Request{Service: codec.ShapeRef{AsmName: asmName, ShapeName: serviceName},
		Operation: codec.ShapeRef{AsmName: asmName, ShapeName: opName},
	}
	if input := op.Input(); input != nil {
		cr.Input = input.New()
		if err := c.Unmarshal(reqBody, &cr.Input); err != nil {
			return nil, err
		}
	}
	if output := op.Output(); output != nil {
		cr.Output = output.New()
	}
	return cr, nil
}

// MarshalResponse satisfies codec.Server
func (c RPCv1) MarshalResponse(w http.ResponseWriter, r *codec.Request) {
	if r == nil || r.Operation.ShapeName == "" || r.Service.ShapeName == "" {
		log.Fatal("MarshalResponse called without proper input.")
		http.Error(w, "Unable to process request", http.StatusInternalServerError)
		return
	}

	var body []byte
	var err error

	// Serialize r.Output into bytes if available.
	if r.Output != nil {
		body, err = c.Marshal(r.Output)
		if err != nil {
			http.Error(w, "Unable to process response", http.StatusInternalServerError)
			return
		}
	}

	// Set the appropriate response headers for the codec.
	w.Header().Set(headerContentType, contentType)
	w.Header().Set(headerContentEncoding, contentEncoding)

	// Encode the response if we are set up to do so.
	// TODO

	// Send the response.
	if body != nil {
		w.Write(body)
	} else {
		w.WriteHeader(http.StatusNoContent)
	}
}

// RoundTrip supports codec.RoundTripper.
func (c RPCv1) RoundTrip(r *codec.Request, rw io.ReadWriter) error {
	path := c.Path
	if path == "" {
		path = "/"
	}

	b, err := c.Marshal(r.Input)
	if err != nil {
		return err
	}
	body := bytes.NewBuffer(b)
	request, err := http.NewRequest("POST", path, body)
	if err != nil {
		return err
	}

	if c.Host != "" {
		request.Host = c.Host
		request.URL.Host = c.Host
	}

	request.Header.Set(headerAccept, "application/json, text/javascript")
	request.Header.Set(headerContentType, "application/json; charset=UTF-8;")
	request.Header.Set(headerContentEncoding, contentEncoding)
	request.Header.Set(headerAmznTarget, r.Service.AsmName+"."+r.Service.ShapeName+"."+r.Operation.ShapeName)

	if c.SecurityToken != "" {
		request.Header.Set(headerAmznToken, c.SecurityToken)
	}

	if c.AuthInfo != nil {
		request.SetBasicAuth(c.AuthInfo.Username, c.AuthInfo.Password)
	}

	// TODO: There is nothing CODEC specific about the SignV4 or AAA encoding/decoding of requests and
	//       responses.  This logic should be moved to CoralGoCodec so that it can be shared across CODECs.

	// v4 signing?
	if c.SignerV4 != nil {
		c.SignerV4.Sign(request)
	} else {
		// v4 signing will add this header
		request.Header.Set(headerAmznDate, time.Now().UTC().Format(time.RFC822))
	}

	var clientCxt *aaa.ClientContext
	if c.AAAClient != nil {
		clientCxt, err = c.AAAClient.EncodeRequest(r.Service.ShapeName, r.Operation.ShapeName, request)
		if err != nil {
			return err
		}
	}

	err = request.Write(rw)
	if err != nil {
		return err
	}

	resp, err := http.ReadResponse(bufio.NewReader(rw), request)
	if err != nil {
		return err
	}

	if c.AAAClient != nil {
		err = c.AAAClient.DecodeResponse(clientCxt, resp)
		if err != nil {
			return err
		}
	}

	defer resp.Body.Close()
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return errors.New(string(respBody))
	}

	// Odin returns empty body with 200 sometimes which breaks Unmarshal
	if len(respBody) == 0 {
		return nil
	}

	if r.Output == nil {
		return nil
	} else {
		return c.Unmarshal(respBody, r.Output)
	}
}
