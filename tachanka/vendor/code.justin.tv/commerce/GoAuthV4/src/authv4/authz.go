package authv4

/*
 This is an authenticator based on the concept of an authenticator that will reply with signing keys for credentials
*/

import (
	"errors"
	"fmt"
	"sync"
	"time"

	"net/http"

	"code.justin.tv/commerce/GoLRU/src/lru"
)

// Holds information about an authenticated client.
type AuthenticatedClient struct {
	accessKey    string
	created      time.Time
	lastUpdated  time.Time
	expires      time.Time
	accountId    string // IAM account#
	requestCount int64
	failureCount int64

	// don't expose this
	signingKey SigningKey
}

func (ac *AuthenticatedClient) AccessKey() string {
	return ac.accessKey
}

func (ac *AuthenticatedClient) AccountId() string {
	return ac.accountId
}

func (ac *AuthenticatedClient) Created() time.Time {
	return ac.created
}

func (ac *AuthenticatedClient) LastUpdated() time.Time {
	return ac.lastUpdated
}

func (ac *AuthenticatedClient) SetExpires(expires time.Time) {
	ac.expires = expires
}

func (ac *AuthenticatedClient) Expires() time.Time {
	return ac.expires
}

func (ac *AuthenticatedClient) RequestCount() int64 {
	return ac.requestCount
}

func (ac *AuthenticatedClient) FailureCount() int64 {
	return ac.failureCount
}

func NewAuthenticatedClient(accessKey, accountId string, signingKey SigningKey) *AuthenticatedClient {
	now := time.Now().UTC()
	expires := now.Add(time.Hour * 24) // max possible but can be adjusted ... the signingKey itself expires after a day
	return &AuthenticatedClient{accessKey, now, now, expires, accountId, 0, 0, signingKey}
}

// Implements authentication of a claim. Allows debug logging.
type Authenticator interface {
	Authenticate(claim *AuthV4Claim) (*AuthenticatedClient, error)
	SetDebugLogger(debugLogger DebugLogger)
	DebugLoggerEnable()
	DebugLoggerDisable()
	DebugLoggerIsEnabled() bool
}

type authnWhitelist map[string]bool // hash of accountId's in whitelist for quick tests

/*
   Implmements authorization.

   Applies an Authenticator to get an AuthenticatedClient.

   Holds a LRU cache of AuthenicatedClients.
*/
type Authorizer struct {
	lock sync.RWMutex // to protect internal volatile structures

	authn Authenticator

	cacheExpiration time.Duration // oldest to allow a cached authClient
	authClients     *lru.Cache    // cache of references to authenticated clients

	whitelistMap authnWhitelist // if not nil, then clients must be in whitelist

	log *Logger // public so we can control logging
}

/*
   Creates an Authorizer object used to validate AuthV4 HTTP requests.

   The specified authn is used to fetch the signing key for a particular credential and then signs the request.
   The signed request is compared to the signature sent in the request.

   If the whitelist is not nil, it should be array of strings of IAM accountIds allowed to connect.

*/
func NewAuthorizer(authn Authenticator, whitelist []string) *Authorizer {
	var whitelistMap authnWhitelist
	if whitelist != nil {
		whitelistMap = make(authnWhitelist, 1)
		for _, accountId := range whitelist {
			whitelistMap[accountId] = true
		}
	} else {
		whitelistMap = nil
	}

	return &Authorizer{
		sync.RWMutex{},
		authn,
		defaultCacheExpiration, lru.New(defaultMaxCachedClients),
		whitelistMap,
		NewLogger()}
}

func (azr *Authorizer) SetCacheExpiration(cacheExpiration time.Duration) {
	azr.cacheExpiration = cacheExpiration
}

func (azr *Authorizer) CacheExpiration() time.Duration {
	return azr.cacheExpiration
}

// Calling this purges current cache and creates a new one
func (azr *Authorizer) SetCacheSize(size int) {

	// LOCK WHOLE FUNCTION
	azr.lock.Lock()
	defer azr.lock.Unlock()

	azr.authClients = lru.New(size)
}

func (azr *Authorizer) CacheCapacity() int {

	// LOCK WHOLE FUNCTION
	azr.lock.Lock()
	defer azr.lock.Unlock()

	return azr.authClients.MaxEntries
}

func (azr *Authorizer) CacheSize() int {

	// LOCK WHOLE FUNCTION
	azr.lock.Lock()
	defer azr.lock.Unlock()

	return azr.authClients.Len()
}

func (azr *Authorizer) SetDebugLogger(debugLogger DebugLogger) {
	azr.log.SetDebugLogger(debugLogger)
	azr.authn.SetDebugLogger(debugLogger)
}

func (azr *Authorizer) DebugLoggerEnable() {
	azr.log.DebugLoggerEnable()
	azr.authn.DebugLoggerEnable()
}

func (azr *Authorizer) DebugLoggerDisable() {
	azr.log.DebugLoggerDisable()
	azr.authn.DebugLoggerDisable()
}

func (azr *Authorizer) DebugLoggerIsEnabled() bool {
	return azr.log.DebugLoggerIsEnabled() && azr.authn.DebugLoggerIsEnabled()
}

func (azr *Authorizer) debugLog(claim *AuthV4Claim, canonical_request, calcauthz, errMsg string) {
	if azr.DebugLoggerIsEnabled() {

		// from request
		azr.log.Debugf("AuthV4 Claim:\n%s\n", claim.String())

		// generate if not set
		if canonical_request == "" && claim != nil {
			canonical_request, _, _ = claim.GetStringToSign()
		}
		azr.log.Debugf("Canonical Request=%s\n", canonical_request)

		// calculated
		azr.log.Debugf("Calc Authz=%s\n", calcauthz)

		azr.log.Debugf("Errors=%s\n", errMsg)

	}
}

func (azr *Authorizer) authenticateClaim(claim *AuthV4Claim) (*AuthenticatedClient, error) {

	newac, err := azr.authn.Authenticate(claim)
	if err != nil {
		return newac, err
	}

	// START CRITICAL SECTION
	azr.lock.Lock()

	azr.authClients.Add(makeClientKey(claim), newac)

	azr.lock.Unlock()
	// END CRITICAL SECTION

	return newac, nil
}

func makeClientKey(claim *AuthV4Claim) string {
	return fmt.Sprintf("%s-%t", claim.AccessKey, claim.StripDefaultPorts)
}

func (azr *Authorizer) getAuthenticatedClient(claim *AuthV4Claim) (*AuthenticatedClient, error) {

	// START CRITCAL SECTION
	azr.lock.Lock()

	obj, present := azr.authClients.Get(makeClientKey(claim))

	azr.lock.Unlock()
	// END CRITCAL SECTION

	// if in cache and not expired, then return, else need to authenticate
	if present {
		ac := obj.(*AuthenticatedClient)

		// check expiration
		now := time.Now().UTC()
		age := now.Sub(ac.created)
		if age < azr.cacheExpiration && ac.expires.Sub(now) > 0 {
			return ac, nil
		} else {
			return azr.authenticateClaim(claim) // expired, re-do
		}

	} else {
		return azr.authenticateClaim(claim) // not present
	}

}

func (azr *Authorizer) inWhitelist(authclient *AuthenticatedClient) bool {
	// no whitelist?
	if azr.whitelistMap == nil {
		return true
	}
	return azr.whitelistMap[authclient.accountId]
}

func (azr *Authorizer) signClaim(authclient *AuthenticatedClient, claim *AuthV4Claim) (string, string, error) {

	// what we return
	var canonical_request, calcauthz string

	var useCache, sigMismatch bool

	/*
	   Use cached copy of signingKey first...

	   The cached copy may not work because :
	     - the 'day' has rolled over and the credential (key'd by UTC day) is no longer vaild
	     - the client has changed their secret key

	     These are both issues introduced by caching, hence, the policy is that
	     is if there is a failure using the cached signingKey, retry with a new sigingKey.
	*/
	useCache = true

	for { // the test on authz == calcauthz  breaks the loop

		// lookup signingKey
		signingKey := authclient.signingKey

		if !useCache || signingKey == nil {

			// re-do authentication ... all we need is new signing key
			authclient, err := azr.authn.Authenticate(claim)
			if err != nil {
				return "", "", err
			}

			// update cache with new authenticated client

			// START CRITICAL SECTION
			azr.lock.Lock()

			azr.authClients.Add(makeClientKey(claim), authclient)

			azr.lock.Unlock()
			// END CRITICAL SECTION

			signingKey = authclient.signingKey // reset local

			useCache = false
		}

		canonical_request, calcauthz, _, err := doSign(claim.AmzDate, claim.Day,
			claim.Region, claim.Service, claim.AccessKey,
			claim.CanonicalHeaders, claim.SortedHeaders,
			signingKey, SignableRequest{claim.Request, claim.StripDefaultPorts})
		// THIS CAN'T HAPPEN: the only possible error from this is due to ReadSeeker errors which do not happen on the server
		if err != nil { // future proof
			return canonical_request, calcauthz, err
		}

		// validate that the signatures match
		if calcauthz != claim.Authz {
			if !useCache {
				sigMismatch = true
				break // break out of loop, done now!
			} else { // cached signingKey may be invalid
				// clear signing key
				authclient.signingKey = SigningKey(nil)
				// do not use cache
				useCache = false
				continue // loop again but authenticate
			}
		} else { // good to go
			sigMismatch = false
			break // break out of loop, done now!
		}
	} // end for true

	if sigMismatch {
		errMsg := "Signatures do not match"
		return canonical_request, calcauthz, errors.New(errMsg)
	}

	return canonical_request, calcauthz, nil
}

/*
 Authenticate incoming requests to a server: return AuthenticatedClient, error
*/
func (azr *Authorizer) Authenticate(request *http.Request) (*AuthenticatedClient, error) {
	// Try authorizing with stripping default ports
	// The default AWSSDK Signers strip :80 and :443 off of the Host header
	// before signing
	client, err := azr.tryAuthenticate(request, true)

	if err != nil {
		// retry without stripping ports in case this was generated with older Go code
		client, err = azr.tryAuthenticate(request, false)
	}

	return client, err
}

func (azr *Authorizer) tryAuthenticate(request *http.Request, stripDefaultPorts bool) (*AuthenticatedClient, error) {
	// parse headers
	claim, err := NewAuthV4Claim(request, stripDefaultPorts)
	if err != nil {
		azr.debugLog(claim, "", "", err.Error())
		return nil, err
	}

	// look up client in cache by AcccessKey
	authclient, err := azr.getAuthenticatedClient(claim)
	if err != nil {
		azr.debugLog(claim, "", "", err.Error())
		return authclient, err
	}
	authclient.lastUpdated = claim.Created
	authclient.requestCount++

	// calc signatures
	canonical_request, calcauthz, err := azr.signClaim(authclient, claim)
	if err != nil {
		authclient.failureCount++
		azr.debugLog(claim, canonical_request, calcauthz, err.Error())
		return authclient, err
	}

	// is authenticated client in whitelist?
	if !azr.inWhitelist(authclient) {
		err = errors.New("Client account not in whitelist")
		authclient.failureCount++
		azr.debugLog(claim, canonical_request, calcauthz, err.Error())
		return authclient, err
	}

	azr.debugLog(claim, canonical_request, calcauthz, "")
	return authclient, nil
}
