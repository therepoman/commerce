package authv4

import (
	"errors"
	"strings"
	"time"

	"net/http"

	"encoding/json"
)

// Represents parsed AuthV4 http.Request.
type AuthV4Claim struct {
	Created          time.Time
	Request          *http.Request
	Authz            string
	Signature        string
	Credential       string
	AccessKey        string
	Day              SigningDay
	Region           string
	Service          string
	Headers          []string
	AmzDate          string
	CanonicalHeaders string
	SortedHeaders    []string

	StripDefaultPorts bool
}

// Convert to JSON String.
func (claim *AuthV4Claim) String() string {
	cjson, _ := json.Marshal(claim)
	return string(cjson)
}

func (claim *AuthV4Claim) GetStringToSign() (string, string, error) {
	canonical_request, str2sign, _, err := getStringToSign(
		claim.AmzDate,
		claim.Day,
		claim.Region,
		claim.Service,
		claim.AccessKey,
		claim.CanonicalHeaders,
		claim.SortedHeaders,
		SignableRequest{claim.Request, claim.StripDefaultPorts})
	return canonical_request, str2sign, err
}

// Parse AuthV4 http.Request and produce claim.
func NewAuthV4Claim(request *http.Request, stripPorts bool) (*AuthV4Claim, error) {

	var errMsg string

	claim := AuthV4Claim{}

	claim.Request = request

	claim.Created = time.Now().UTC()
	claim.StripDefaultPorts = stripPorts

	// parse what we can

	claim.Authz = request.Header.Get("Authorization")
	if claim.Authz == "" {
		errMsg = "Missing Authorization header"
		return &claim, errors.New(errMsg)
	}

	if !strings.HasPrefix(claim.Authz, authv4Algorithm) {
		errMsg = "Authorization header does not start with " + authv4Algorithm
		return &claim, errors.New(errMsg)
	}

	/*
	   should be a string of the form:

	    "AWS4-HMAC-SHA256 Credential=<credential>, SignedHeaders=<canonicalHeaders>, Signature=<signature>"

	*/
	authparts := make(map[string]string, 3)
	for _, part := range strings.Split(claim.Authz[len(authv4Algorithm):], ",") {
		keyval := strings.Split(part, "=")
		if len(keyval) != 2 {
			continue
		}
		authparts[strings.TrimSpace(keyval[0])] = strings.TrimSpace(keyval[1])
	}

	// parse/validate creds
	var present bool
	claim.Credential, present = authparts["Credential"]
	if !present {
		errMsg = "Authorization header missing Credential field"
		return &claim, errors.New(errMsg)
	}
	credparts := strings.Split(claim.Credential, "/")
	if len(credparts) != 5 || credparts[4] != "aws4_request" {
		errMsg = "Malformed credential: " + claim.Credential
		return &claim, errors.New(errMsg)
	}
	claim.AccessKey = credparts[0]
	claim.Day = SigningDay(credparts[1])
	claim.Region = credparts[2]
	claim.Service = credparts[3]

	signed_headers, present := authparts["SignedHeaders"]
	if !present {
		errMsg = "Authorization header missing SignedHeaders field"
		return &claim, errors.New(errMsg)
	}
	claim.Headers = strings.Split(signed_headers, ";")

	signature, present := authparts["Signature"]
	if !present {
		errMsg = "Authorization header missing Signature field"
		return &claim, errors.New(errMsg)
	}
	claim.Signature = signature

	claim.AmzDate = request.Header.Get("X-Amz-Date")
	if claim.AmzDate == "" {
		errMsg = "Missing X-Amz-Date header"
		return &claim, errors.New(errMsg)
	}

	// check time tolerance
	amzdate, err := time.Parse(iso8601BasicFmt, claim.AmzDate)
	if err != nil {
		errMsg = "Bad X-Amz-Date format"
		return &claim, errors.New(errMsg)
	}
	tdelta := amzdate.Sub(claim.Created)
	if tdelta > clockSkew || tdelta < -clockSkew {
		errMsg = "X-Amz-Date too different from current datetime"
		return &claim, errors.New(errMsg)
	}

	host := request.Host
	if stripPorts {
		host = stripDefaultPorts(host)
	}

	request.Header.Set("host", host) // ensure this this is set
	claim.CanonicalHeaders, claim.SortedHeaders = normalizeHeaders(claim.Headers)

	return &claim, nil
}

func stripDefaultPorts(s string) string {
	return strings.TrimSuffix(strings.TrimSuffix(s, ":443"), ":80")
}
