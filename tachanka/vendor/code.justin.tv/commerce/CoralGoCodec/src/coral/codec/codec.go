//The codec package defines how to read and write data from the wire
package codec

import (
	"io"
	"net/http"
)

// RoundTripper executes a send and receive of a request over the wire.
type RoundTripper interface {
	// Request is what to encode and send over the wire while conn is the
	// transport to write the request to and read the response from.
	RoundTrip(req *Request, conn io.ReadWriter) error
}

// Codec defines how to read and write data from the wire.
type Codec interface {
	// Marshal defines how to encode an object into the format used on the wire.
	Marshal(obj interface{}) ([]byte, error)
	// Unmarshal defines how to decode an object from the format used on the wire.
	Unmarshal(d []byte, obj interface{}) error
}

// Server defines methods that are required of the codec to be used on the server side.
type Server interface {
	// IsSupported returns true if the given http.Headers match what is expected for
	// a message encoded using this Codec.
	IsSupported(g Getter) bool
	// UnmarshalRequest reads an http.Request to produce a Request struct with
	// the Service, Operation, and Input decoded.
	UnmarshalRequest(r *http.Request) (*Request, error)
	// MarshalResponse writes the Output of the given Request to the given ResponseWriter
	// adding headers and encryption as necessary to support the CODEC and security.
	// If the given Output is nil, then a status 204 response is returned.
	MarshalResponse(w http.ResponseWriter, r *Request)
}

// Getter provides a read-only abstraction of http.Header.
type Getter interface {
	// Get mimics the http.Header.Get function.
	Get(string) string
}

type ShapeRef struct {
	AsmName   string
	ShapeName string
}

// Request is a generic wrapper that can represent any Coral request and subsequent
// response.
type Request struct {
	Service   ShapeRef
	Operation ShapeRef
	Input     interface{}
	Output    interface{}
}
