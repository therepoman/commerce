package goauthorization

import (
	"time"

	"github.com/satori/go.uuid"

	"code.justin.tv/common/jwt"
	"code.justin.tv/common/jwt/claim"
)

// Encoder defines the attributes of an encoder
type Encoder struct {
	Type      string
	Issuer    string
	Algorithm jwt.Algorithm
	Header    jwt.Header
}

// IEncoder implements the methods of an encoder
type IEncoder interface {
	Encode(TokenParams) *AuthorizationToken
}

// NewEncoder instantiates a new Encoder
func NewEncoder(alg, secretOrKeyPath, iss string) (*Encoder, error) {
	enc := &Encoder{
		Type:   alg,
		Issuer: iss,
	}

	switch alg {
	case "HS512":
		key := []byte(secretOrKeyPath)
		enc.Algorithm = jwt.HS512(key)
	case "RS256":
		key, err := jwt.ReadRSAPrivateKey(secretOrKeyPath)
		if err != nil {
			return enc, err
		}
		enc.Algorithm = jwt.RS256(key)
	case "ES256":
		key, err := jwt.ReadECDSAPrivateKey(secretOrKeyPath)
		if err != nil {
			return enc, err
		}
		enc.Algorithm = jwt.ES256(key)
	}
	enc.Header = jwt.NewHeader(enc.Algorithm)

	return enc, nil
}

// Encode constructs an authorization token with the given parameters
func (e *Encoder) Encode(p TokenParams) *AuthorizationToken {
	return &AuthorizationToken{
		Algorithm: e.Algorithm,
		Header:    e.Header,
		Claims: TokenClaims{
			Audience:         p.Aud,
			Expires:          claim.Exp(p.Exp),
			Issuer:           claim.Iss(e.Issuer),
			NotBefore:        claim.Nbf(p.Nbf),
			Sub:              claim.Sub{Sub: p.Sub},
			IssuedAt:         claim.Iat(time.Now()),
			JwtID:            uuid.NewV4().String(),
			Authorizations:   p.Claims,
			FirstPartyClient: p.Fpc,
			ClientID:         p.ClientID,
		},
	}
}
