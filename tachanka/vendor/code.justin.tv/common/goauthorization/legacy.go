package goauthorization

import (
	"crypto/rsa"
	"fmt"
	"net/http"
	"strings"

	"code.justin.tv/common/jwt"
)

var (
	publicKey  *rsa.PublicKey
	privateKey *rsa.PrivateKey
	audience   string
	issuer     string
)

// InitializeDecoder initializes necessary constants publicKey, audience, and issuer
func InitializeDecoder(publicKeyPath, aud string, iss string) error {
	key, err := jwt.ReadRSAPublicKey(publicKeyPath)
	if err != nil {
		return err
	}
	publicKey = key
	audience = aud
	issuer = iss

	return nil
}

// DecodeToken unmarshals a serialized token
func DecodeToken(serialized string) (*AuthorizationToken, error) {
	authToken := AuthorizationToken{Algorithm: jwt.RSAValidateOnly(jwt.RS256, publicKey)}
	authToken.Header = jwt.NewHeader(authToken.Algorithm)
	if err := authToken.Parse(serialized); err != nil {
		return nil, err
	}
	return &authToken, nil
}

// Parse uses jwt library to parse a serialized jwt string into an AuthorizationToken
func (t *AuthorizationToken) Parse(serialized string) (err error) {
	err = jwt.DecodeAndValidate(&t.Header, &t.Claims, t.Algorithm, []byte(serialized))
	if err != nil {
		return err
	}

	return
}

// Validate is the equivalent of ValidateTokens but on a single token
func (t *AuthorizationToken) Validate(capabilities CapabilityClaims) (err error) {
	validatedClaims := t.legacyValidateClaims()
	validatedCaps := t.validateCapabilities(capabilities)

	if err = validatedClaims; err != nil {
		return err
	}
	if err = validatedCaps; err != nil {
		return err
	}

	return err
}

func (t *AuthorizationToken) legacyValidateClaims() error {
	if !t.validAudience(audience) {
		return fmt.Errorf("Invalid audience: Expected: %v Got: %v", audience, t.Claims.Audience)
	}
	if string(t.Claims.Issuer) != issuer {
		return fmt.Errorf("Invalid issuer: Expected: %v, Got: %v", issuer, t.Claims.Issuer)
	}

	return nil
}

// ParseTokens extracts all jwt tokens from an http request and unmarshals them into an AuthorizationToken
func ParseTokens(r *http.Request) (*AuthorizationTokens, error) {
	jwtArr := strings.Fields(r.Header.Get("Twitch-Authorization"))
	if len(jwtArr) == 0 {
		return nil, fmt.Errorf("Request contains no JWTs")
	}

	jwts := jwtArr[len(jwtArr)-1]

	authTokens := AuthorizationTokens{}
	for _, val := range strings.Split(jwts, ":") {
		authToken, err := DecodeToken(val)
		if err != nil {
			return nil, err
		}
		authTokens = append(authTokens, *authToken)
	}

	return &authTokens, nil
}

// ParseToken does the same as ParseTokens, but with a single token
func ParseToken(r *http.Request) (*AuthorizationToken, error) {
	authToken, err := DecodeToken(r.Header.Get("Twitch-Authorization"))
	if err != nil {
		return nil, err
	}
	return authToken, nil
}
