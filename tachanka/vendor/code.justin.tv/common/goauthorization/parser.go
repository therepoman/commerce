package goauthorization

import "net/http"

// ParseToken extracts an authorization token from the "Twitch-Authorization"
// header
func (d *Decoder) ParseToken(r *http.Request) (*AuthorizationToken, error) {
	serialized := r.Header.Get("Twitch-Authorization")
	if serialized == "" {
		return nil, ErrNoAuthorizationToken
	}
	return d.Decode(serialized)
}

// ParseTokens extracts all jwt tokens from an http request and unmarshals them into an AuthorizationToken
// TODO - refactor once a service requests multiple tokens
// func (d *Decoder) ParseTokens(r *http.Request) (*AuthorizationTokens, error) {
// 	jwtArr := strings.Fields(r.Header.Get("Twitch-Authorization"))
// 	if len(jwtArr) == 0 {
// 		return nil, ErrNoAuthorizationToken
// 	}

// 	jwts := jwtArr[len(jwtArr)-1]

// 	authTokens := AuthorizationTokens{}
// 	for _, val := range strings.Split(jwts, ":") {
// 		authToken, err := d.Decode(val)
// 		if err != nil {
// 			return nil, err
// 		}
// 		authTokens = append(authTokens, *authToken)
// 	}

// 	return &authTokens, nil
// }
