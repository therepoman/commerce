package goauthorization

import (
	"fmt"
	"time"

	"code.justin.tv/common/jwt"
	"code.justin.tv/common/jwt/claim"
)

// AuthorizationTokens is an array of AuthorizationTokens
type AuthorizationTokens []AuthorizationToken

// AuthorizationToken contains all relevant information stored in a jwt token
type AuthorizationToken struct {
	Header    jwt.Header
	Algorithm jwt.Algorithm
	Claims    TokenClaims
}

// TokenClaims contains all standard jwt claims
type TokenClaims struct {
	Audience         []string         `json:"aud"`
	Expires          claim.Exp        `json:"exp"`
	Issuer           claim.Iss        `json:"iss"`
	NotBefore        claim.Nbf        `json:"nbf"`
	IssuedAt         claim.Iat        `json:"iat"`
	JwtID            string           `json:"jti"`
	Authorizations   CapabilityClaims `json:"capabilities"`
	FirstPartyClient bool             `json:"firstPartyClient"`
	ClientID         string           `json:"clientID,omitempty"`
	claim.Sub
}

// CapabilityClaims is a hash of CapabilityClaim
type CapabilityClaims map[string]CapabilityClaim

// CapValue is the value of any capability key, can be of any type
type CapValue interface{}

// CapabilityClaim is a hash of claim key to values
type CapabilityClaim map[string]CapValue

// TokenParams contains arguments to construct a valid AuthorizationToken
type TokenParams struct {
	Exp      time.Time
	Nbf      time.Time
	Aud      []string
	Sub      string
	Claims   CapabilityClaims
	Fpc      bool
	ClientID string
}

// Serialize encodes an auth token into a byte array
func (t *AuthorizationToken) Serialize() ([]byte, error) {
	return jwt.Encode(t.Header, t.Claims, t.Algorithm)
}

// String encodes an auth token into a string
func (t *AuthorizationToken) String() (string, error) {
	encoded, err := jwt.Encode(t.Header, t.Claims, t.Algorithm)
	if err != nil {
		return "", fmt.Errorf("%s: %q", ErrEncode, err)
	}

	return string(encoded), nil
}

// GetID returns token ID
func (t *AuthorizationToken) GetID() string {
	return t.Claims.JwtID
}

// GetSubject returns token subject
func (t *AuthorizationToken) GetSubject() string {
	return t.Claims.Sub.Sub
}

// GetIssuedAt returns token Iat
func (t *AuthorizationToken) GetIssuedAt() time.Time {
	return time.Time(t.Claims.IssuedAt)
}

// GetTokenClaims is a helper function that extracts and returns the capabilities from an AuthorizationToken
func (t *AuthorizationToken) GetTokenClaims(capName string) (CapabilityClaim, error) {
	var capClaim CapabilityClaim
	caps := t.Claims.Authorizations
	for key := range caps {
		if key == capName {
			capClaim = caps[key]
			return capClaim, nil
		}
	}

	return capClaim, fmt.Errorf("%s: %s", ErrMissingCapability, capName)
}

// IsFirstPartyClient returns token first party client state
func (t *AuthorizationToken) IsFirstPartyClient() bool {
	return t.Claims.FirstPartyClient
}

// GetClientID returns token client ID
func (t *AuthorizationToken) GetClientID() string {
	return t.Claims.ClientID
}
