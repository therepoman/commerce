package goauthorization

import (
	"net/http"

	"code.justin.tv/common/jwt"
)

// Decoder defines the attributes of a decoder
type Decoder struct {
	Type      string
	Audience  string
	Issuer    string
	Algorithm jwt.Algorithm
	Header    jwt.Header
}

// IDecoder implements the methods of a decoder
type IDecoder interface {
	Decode(string) (*AuthorizationToken, error)
	ParseToken(*http.Request) (*AuthorizationToken, error)
	// ParseTokens(*http.Request) (*AuthorizationTokens, error)
	Validate(*AuthorizationToken, CapabilityClaims) error
	// ValidateTokens(*AuthorizationTokens, CapabilityClaims) error
}

// NewDecoder instantiates a new Decoder
func NewDecoder(alg, secretOrKeyPath, aud, iss string) (*Decoder, error) {
	dec := &Decoder{
		Type:     alg,
		Audience: aud,
		Issuer:   iss,
	}

	switch alg {
	case "HS512":
		key := []byte(secretOrKeyPath)
		dec.Algorithm = jwt.HS512(key)
	case "RS256":
		key, err := jwt.ReadRSAPublicKey(secretOrKeyPath)
		if err != nil {
			return dec, err
		}
		dec.Algorithm = jwt.RSAValidateOnly(jwt.RS256, key)
	case "ES256":
		key, err := jwt.ReadECDSAPublicKey(secretOrKeyPath)
		if err != nil {
			return dec, err
		}
		dec.Algorithm = jwt.ECDSAValidateOnly(jwt.ES256, key)
	}
	dec.Header = jwt.NewHeader(dec.Algorithm)

	return dec, nil
}

// Decode unmarshals a serialized authorization token and validates basic
// claims like expiration and not before time
func (d *Decoder) Decode(serialized string) (*AuthorizationToken, error) {
	tkn := AuthorizationToken{Algorithm: d.Algorithm, Header: d.Header}
	err := jwt.DecodeAndValidate(&tkn.Header, &tkn.Claims, tkn.Algorithm, []byte(serialized))
	return &tkn, err
}
