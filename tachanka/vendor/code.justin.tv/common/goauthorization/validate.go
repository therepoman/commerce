package goauthorization

import (
	"fmt"
	"time"

	"code.justin.tv/common/jwt/claim"
)

type expValid struct {
	Exp claim.Exp
}

type nbfValid struct {
	Nbf claim.Nbf
}

// Validate validates the default claims and given capabilities
func (d *Decoder) Validate(t *AuthorizationToken, capabilities CapabilityClaims) error {
	validatedClaims := t.validateClaims(d.Audience, d.Issuer)
	validatedCaps := t.validateCapabilities(capabilities)

	var err error
	if err = validatedClaims; err != nil {
		return err
	}
	err = validatedCaps
	return err
}

// ValidateTokens takes an array of auth tokens and returns an error if none of them are valid
// TODO - refactor once a service requests multiple tokens
// func (d *Decoder) ValidateTokens(t *AuthorizationTokens, capabilities CapabilityClaims) error {
// 	var err error
// 	for _, tkn := range *t {
// 		if err = d.Validate(&tkn, capabilities); err == nil {
// 			return nil
// 		}
// 	}
// 	return err
// }

func (t *AuthorizationToken) validateClaims(aud, iss string) error {
	if !claim.Validate(expValid{Exp: t.Claims.Expires}) {
		return fmt.Errorf("%s: %v", ErrExpiredToken, time.Time(t.Claims.Expires))
	}
	if !claim.Validate(nbfValid{Nbf: t.Claims.NotBefore}) {
		return fmt.Errorf("%s: %v", ErrInvalidNbf, time.Time(t.Claims.NotBefore))
	}
	if !t.validAudience(aud) {
		return fmt.Errorf("%s: expected %v, got %v", ErrInvalidAudience, aud, t.Claims.Audience)
	}
	if string(t.Claims.Issuer) != iss {
		return fmt.Errorf("%s: expected %v, got %v", ErrInvalidIssuer, iss, t.Claims.Issuer)
	}

	return nil
}

func (t *AuthorizationToken) validateCapabilities(capabilities CapabilityClaims) error {
	for capName, capArgs := range capabilities {
		tokenAuthorizations := t.Claims.Authorizations[capName]
		if tokenAuthorizations == nil {
			return fmt.Errorf("%s: %s", ErrMissingCapability, capName)
		}
		for arg, argv := range capArgs {
			tokenArg := tokenAuthorizations[arg]

			var compare interface{}
			switch tokenArg.(type) {
			case float64:
				compare = int(tokenArg.(float64))
			default:
				compare = tokenArg
			}

			if compare != argv {
				return fmt.Errorf("%s: expected %v, got %v", ErrInvalidCapability, argv, tokenAuthorizations[arg])
			}
		}
	}

	return nil
}

func (t *AuthorizationToken) validAudience(audience string) bool {
	for _, v := range t.Claims.Audience {
		if v == audience {
			return true
		}
	}
	return false
}
