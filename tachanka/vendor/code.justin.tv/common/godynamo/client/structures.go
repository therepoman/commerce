package client

import (
	"time"
)

//BaseDynamoTableRecord is the basic record that should be extended to use the client.
//While the DynamoTableRecord interface can be implemented directly, this will handle all the timestamp functionality
//which tends to be common accross all DAOs.
type BaseDynamoTableRecord struct {
	LastUpdated time.Time
}

func (b *BaseDynamoTableRecord) GetTimestamp() time.Time {
	return b.LastUpdated
}

func (b *BaseDynamoTableRecord) UpdateWithCurrentTimestamp() {
	b.LastUpdated = time.Now()
}

func (b *BaseDynamoTableRecord) ApplyTimestamp(timestamp time.Time) {
	b.LastUpdated = timestamp
}

func (b *BaseDynamoTableRecord) Equals(other DynamoTableRecord) bool {
	return true
}
