package client

import (
	"errors"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"math/big"
	"strconv"
	"time"
	"unicode"
)

const DefaultString string = ""
const DefaultBoolean bool = false
const DefaultInt64 int64 = 0

func attributeMapContains(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) bool {
	_, exists := attributeMap[attributeName]
	return exists
}

func numericStringToBigInt(numString string) (*big.Int, error) {
	if len(numString) == 0 {
		return nil, errors.New("Cannot convert empty string to bigInt")
	}

	for _, char := range numString {
		if !unicode.IsNumber(char) {
			return nil, errors.New(fmt.Sprintf("String contained non-numeric rune: %c", char))
		}
	}

	bigInt := big.Int{}
	_, ok := bigInt.SetString(numString, 10)
	if !ok {
		return nil, errors.New(fmt.Sprintf("Failed converting string to bigInt: %s", numString))
	}
	return &bigInt, nil
}

func hasValidStringAttribute(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) bool {
	if !attributeMapContains(attributeMap, attributeName) {
		return false
	}

	attribute, _ := attributeMap[attributeName]

	if attribute.S == nil {
		return false
	} else {
		return true
	}
}

//StringFromAttributes extracts a string object from the passed dynamo attribute map at the provided attribute name.
//DefaultString is returned if any errors occur.
func StringFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) string {
	if !hasValidStringAttribute(attributeMap, attributeName) {
		return DefaultString
	}

	return *attributeMap[attributeName].S
}

//OptionalStringFromAttributes extracts a string object from the passed dynamo attribute map at the provided attribute name.
//Nil is returned if any errors occur, notably if the attribute is not in the map.
func OptionalStringFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) *string {
	if !hasValidStringAttribute(attributeMap, attributeName) {
		return nil
	}

	attr := StringFromAttributes(attributeMap, attributeName)
	return &attr
}

func hasValidBoolAttribute(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) bool {
	if !attributeMapContains(attributeMap, attributeName) {
		return false
	}

	attribute, _ := attributeMap[attributeName]

	if attribute.BOOL == nil {
		return false
	} else {
		return true
	}
}

//BoolFromAttributes extracts a boolean value from the passed dynamo attribute map at the provided attribute name.
//DefaultBoolean is returned if any errors occur.
func BoolFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) bool {
	if !hasValidBoolAttribute(attributeMap, attributeName) {
		return DefaultBoolean
	}

	return *attributeMap[attributeName].BOOL
}

//OptionalBoolFromAttributes extracts a boolean value from the passed dynamo attribute map at the provided attribute name.
//Nil is returned if any errors occur, notably if the attribute is not in the map.
func OptionalBoolFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) *bool {
	if !hasValidBoolAttribute(attributeMap, attributeName) {
		return nil
	}

	attr := BoolFromAttributes(attributeMap, attributeName)
	return &attr
}

func hasValidNumericAttribute(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) bool {
	if !attributeMapContains(attributeMap, attributeName) {
		return false
	}

	attribute, _ := attributeMap[attributeName]

	if attribute.N == nil {
		return false
	} else {
		return true
	}
}

//Int64FromAttributes extracts an int64 value from the passed dynamo attribute map at the provided attribute name.
//DefaultInt64 is returned if any errors occur.
func Int64FromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) int64 {
	var result int64
	if !hasValidNumericAttribute(attributeMap, attributeName) {
		return DefaultInt64
	} else {
		// We know the atttribute exists from the validation call.
		attribute, _ := attributeMap[attributeName]
		parsedResult, err := strconv.ParseInt(*attribute.N, 10, 64)

		if err != nil {
			return result // The unset int64 (defauts to 0)
		} else {
			return parsedResult
		}
	}
}

//OptionalInt64FromAttributes extracts an int64 value from the passed dynamo attribute map at the provided attribute name.
//Nil is returned if any errors occur, notably if the attribute is not in the map.
func OptionalInt64FromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) *int64 {
	if !hasValidNumericAttribute(attributeMap, attributeName) {
		return nil
	} else {
		// We know the atttribute exists from the validation call.
		attribute, _ := attributeMap[attributeName]
		parsedResult, err := strconv.ParseInt(*attribute.N, 10, 64)

		if err != nil {
			return nil
		} else {
			return &parsedResult
		}
	}
}

//BigIntFromAttributes extracts a big.Int object from the passed dynamo attribute map at the provided attribute name.
//An uninitiailzed big.Int object (aka var big.Int) is returned if any errors occur.
func BigIntFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) big.Int {
	var defaultResult big.Int
	if !hasValidNumericAttribute(attributeMap, attributeName) {
		return defaultResult
	} else {
		attribute, _ := attributeMap[attributeName]
		parsedResult, err := numericStringToBigInt(*attribute.N)

		if err != nil {
			return defaultResult
		} else {
			return *parsedResult
		}
	}
}

//OptionalBigIntFromAttributes extracts a big.Int object from the passed dynamo attribute map at the provided attribute name.
//Nil is returned if any errors occur, notably if the attribute is not in the map.
func OptionalBigIntFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) *big.Int {
	if !hasValidNumericAttribute(attributeMap, attributeName) {
		return nil
	} else {
		attribute, _ := attributeMap[attributeName]
		parsedResult, err := numericStringToBigInt(*attribute.N)

		if err != nil {
			return nil
		} else {
			return parsedResult
		}
	}
}

//TimeFromAttributesInSeconds extracts a time.Time object from the passed dynamo attribute map at the provided attribute name.
//It is assumed that the value in the map is in seconds. An uninitiailzed time.Time object (aka var time.Time) is returned if any errors occur.
func TimeFromAttributesInSeconds(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) time.Time {
	var defaultResult time.Time
	if !hasValidNumericAttribute(attributeMap, attributeName) {
		return defaultResult
	} else {
		attribute, _ := attributeMap[attributeName]
		unixResult, err := strconv.ParseInt(*attribute.N, 10, 64)
		if err != nil {
			return defaultResult
		} else {
			return time.Unix(unixResult, 0)
		}
	}
}

//OptionalTimeFromAttributesInSeconds extracts a time.Time object from the passed dynamo attribute map at the provided attribute name.
//It is assumed that the value in the map is in seconds. Nil is returned if any errors occur, notably if the attribute is not in the map.
func OptionalTimeFromAttributesInSeconds(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) *time.Time {
	if !hasValidNumericAttribute(attributeMap, attributeName) {
		return nil
	} else {
		attribute, _ := attributeMap[attributeName]
		unixResult, err := strconv.ParseInt(*attribute.N, 10, 64)
		if err != nil {
			return nil
		} else {
			returnTime := time.Unix(unixResult, 0)
			return &returnTime
		}
	}
}

//TimeFromAttributes extracts a time.Time object from the passed dynamo attribute map at the provided attribute name.
//It is assumed that the value in the map is in nanoseconds. An uninitiailzed time.Time object (aka var time.Time) is returned if any errors occur.
func TimeFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) time.Time {
	var defaultResult time.Time
	if !hasValidNumericAttribute(attributeMap, attributeName) {
		return defaultResult
	} else {
		attribute, _ := attributeMap[attributeName]
		unixNanoResult, err := strconv.ParseInt(*attribute.N, 10, 64)
		if err != nil {
			return defaultResult
		} else {
			return time.Unix(0, unixNanoResult)
		}
	}
}

//OptionalTimeFromAttributes extracts a time.Time object from the passed dynamo attribute map at the provided attribute name.
//It is assumed that the value in the map is in nanoseconds. Nil is returned if any errors occur, notably if the attribute is not in the map.
func OptionalTimeFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) *time.Time {
	if !hasValidNumericAttribute(attributeMap, attributeName) {
		return nil
	} else {
		attribute, _ := attributeMap[attributeName]
		unixNanoResult, err := strconv.ParseInt(*attribute.N, 10, 64)
		if err != nil {
			return nil
		} else {
			returnTime := time.Unix(0, unixNanoResult)
			return &returnTime
		}
	}
}

//TimeFromAttributes extracts a time.Duration object from the passed dynamo attribute map at the provided attribute name.
//It is assumed that the value in the map is in nanoseconds. An uninitiailzed time.Duration object (aka var time.Duration) is returned if any errors occur.
func DurationFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) time.Duration {
	var defaultResult time.Duration
	if !hasValidNumericAttribute(attributeMap, attributeName) {
		return defaultResult
	} else {
		attribute, _ := attributeMap[attributeName]
		unixNanoResult, err := strconv.ParseInt(*attribute.N, 10, 64)
		if err != nil {
			return defaultResult
		} else {
			return time.Duration(unixNanoResult)
		}
	}
}

//OptionalDurationFromAttributes extracts a time.Time object from the passed dynamo attribute map at the provided attribute name.
//It is assumed that the value in the map is in nanoseconds. Nil is returned if any errors occur, notably if the attribute is not in the map.
func OptionalDurationFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) *time.Duration {
	if !hasValidNumericAttribute(attributeMap, attributeName) {
		return nil
	} else {
		attribute, _ := attributeMap[attributeName]
		unixNanoResult, err := strconv.ParseInt(*attribute.N, 10, 64)
		if err != nil {
			return nil
		} else {
			returnDuration := time.Duration(unixNanoResult)
			return &returnDuration
		}
	}
}

//PopulateAttributesString populates the provided attribute map with attributeValue are attributeName.
func PopulateAttributesString(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue string) {
	if attributeValue != "" {
		attributeMap[attributeName] = &dynamodb.AttributeValue{S: aws.String(attributeValue)}
	}
}

//PopulateAttributesOptionalString populates the provided attribute map with attributeValue are attributeName.
func PopulateAttributesOptionalString(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue *string) {
	if attributeValue != nil && *attributeValue != "" {
		PopulateAttributesString(attributeMap, attributeName, *attributeValue)
	}
}

//PopulateAttributesBool populates the provided attribute map with attributeValue are attributeName.
func PopulateAttributesBool(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue bool) {
	attributeMap[attributeName] = &dynamodb.AttributeValue{BOOL: aws.Bool(attributeValue)}
}

//PopulateAttributesOptionalBool populates the provided attribute map with attributeValue are attributeName.
func PopulateAttributesOptionalBool(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue *bool) {
	if attributeValue != nil {
		PopulateAttributesBool(attributeMap, attributeName, *attributeValue)
	}
}

//PopulateAttributesInt64 populates the provided attribute map with attributeValue are attributeName.
func PopulateAttributesInt64(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue int64) {
	strVal := strconv.FormatInt(attributeValue, 10)
	attributeMap[attributeName] = &dynamodb.AttributeValue{N: aws.String(strVal)}
}

//PopulateAttributesOptionalInt64 populates the provided attribute map with attributeValue are attributeName.
func PopulateAttributesOptionalInt64(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue *int64) {
	if attributeValue != nil {
		PopulateAttributesInt64(attributeMap, attributeName, *attributeValue)
	}
}

//PopulateAttributesBigInt populates the provided attribute map with attributeValue are attributeName.
func PopulateAttributesBigInt(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue big.Int) {
	strVal := attributeValue.String()
	attributeMap[attributeName] = &dynamodb.AttributeValue{N: aws.String(strVal)}
}

//PopulateAttributesOptionalBigInt populates the provided attribute map with attributeValue are attributeName.
func PopulateAttributesOptionalBigInt(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue *big.Int) {
	if attributeValue != nil {
		PopulateAttributesBigInt(attributeMap, attributeName, *attributeValue)
	}
}

//PopulateAttributesTime populates the provided attribute map with attributeValue are attributeName.
func PopulateAttributesTime(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, time time.Time) {
	if time.UnixNano() > 0 {
		unixNanoStr := strconv.FormatInt(time.UnixNano(), 10)
		attributeMap[attributeName] = &dynamodb.AttributeValue{N: aws.String(unixNanoStr)}
	}
}

//PopulateAttributesOptionalTime populates the provided attribute map with attributeValue are attributeName.
func PopulateAttributesOptionalTime(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, time *time.Time) {
	if time != nil {
		PopulateAttributesTime(attributeMap, attributeName, *time)
	}
}
