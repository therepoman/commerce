package testing

import (
	"code.justin.tv/common/godynamo/client"
	"time"
)

func EqualsWithExpectedLastUpdated(record client.DynamoTableRecord, other client.DynamoTableRecord, lastUpdatedTime time.Time, lastUpdatedDelta time.Duration) bool {
	endTime := lastUpdatedTime.Add(lastUpdatedDelta)
	otherTimeEqual := (other.GetTimestamp().Equal(lastUpdatedTime) || other.GetTimestamp().Equal(endTime))
	otherTimeBetween := (other.GetTimestamp().After(lastUpdatedTime) && other.GetTimestamp().Before(endTime))
	return record.Equals(other) && (otherTimeEqual || otherTimeBetween)
}
