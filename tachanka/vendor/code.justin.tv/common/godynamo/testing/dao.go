package testing

import (
	"code.justin.tv/common/godynamo/client"

	"errors"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type ItemDao struct {
	table  client.DynamoTable
	client *client.DynamoClient
}

type IItemDao interface {
	Put(user *Item) error
	Get(ItemId string) (*Item, error)
	Delete(ItemId string) error
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *client.DynamoClientConfig
}

func (dao *ItemDao) DeleteTable() error {
	return dao.client.DeleteTable(dao.table)
}

func (dao *ItemDao) SetupTable() error {
	return dao.client.SetupTable(dao.table)
}

func (dao *ItemDao) GetClientConfig() *client.DynamoClientConfig {
	return dao.client.ClientConfig
}

func (dao *ItemDao) Put(item *Item) error {
	return dao.client.PutItem(item)
}

func (dao *ItemDao) Get(ItemId string) (*Item, error) {
	var itemResult *Item

	itemQuery := &Item{
		Id: ItemId,
	}

	result, err := dao.client.GetItem(itemQuery)
	if err != nil {
		return itemResult, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	itemResult, isItem := result.(*Item)
	if !isItem {
		msg := "Encountered an unexpected type while converting dynamo result to user"
		return itemResult, errors.New(msg)
	}

	return itemResult, nil
}

func (dao *ItemDao) Delete(ItemId string) error {
	deletionRecord := &Item{
		Id: ItemId,
	}
	return dao.client.DeleteItem(deletionRecord)
}

func (dao *ItemDao) QueryForItems(ItemId string) ([]*Item, error) {
	filter := client.QueryFilter{
		Names: map[string]*string{
			"#ID": aws.String("id"),
		},
		Values: map[string]*dynamodb.AttributeValue{
			":id": &dynamodb.AttributeValue{S: aws.String(string(ItemId))},
		},
		KeyConditionExpression: aws.String("#ID = :id"),
	}

	result, err := dao.client.QueryTable(&ItemTable{}, filter)
	if err != nil {
		return nil, err
	}

	// The query returns DynamoTableRecord types, which should be underlying Items.
	// Need to cast to check.
	itemResult := make([]*Item, len(result))
	for i, record := range result {
		var isItem bool
		itemResult[i], isItem = record.(*Item)
		if !isItem {
			msg := "Encountered an unexpected type while converting dynamo result to item record"
			return itemResult, errors.New(msg)
		}
	}

	return itemResult, nil
}

func (dao *ItemDao) ScanForItems(ItemId string) ([]*Item, error) {
	filter := client.ScanFilter{
		Names: map[string]*string{
			"#ID": aws.String("id"),
		},
		Values: map[string]*dynamodb.AttributeValue{
			":id": &dynamodb.AttributeValue{S: aws.String(string(ItemId))},
		},

		Expression: aws.String("#ID = :id"),
	}

	result, err := dao.client.ScanTable(&ItemTable{}, filter)
	if err != nil {
		return nil, err
	}

	// The scan returs DynamoTableRecord types, which should be underlying Items.
	// Need to cast to check.
	itemResult := make([]*Item, len(result))
	for i, record := range result {
		var isItem bool
		itemResult[i], isItem = record.(*Item)
		if !isItem {
			msg := "Encountered an unexpected type while converting dynamo result to item record"
			return itemResult, errors.New(msg)
		}
	}

	return itemResult, nil
}

func NewItemDao(client *client.DynamoClient) IItemDao {
	dao := &ItemDao{}
	dao.client = client
	dao.table = &ItemTable{}
	return dao
}

type ItemTable struct{}

type Item struct {
	client.BaseDynamoTableRecord
	Id        string
	Number    int64
	NewNumber int64
}

func (t *ItemTable) GetTableName() string {
	return "test-item-table"
}

func (t *ItemTable) NewCreateTableInput() *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(100),
			WriteCapacityUnits: aws.Int64(100),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("id"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("number"),
				AttributeType: aws.String("N"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("id"),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String("number"),
				KeyType:       aws.String("RANGE"),
			},
		},
	}
}

func (t *ItemTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (client.DynamoTableRecord, error) {
	item, err := t.convertAttributeMapToRecord(attributeMap)
	return item, err
}

func (t *ItemTable) convertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (*Item, error) {
	return &Item{
		Id:        client.StringFromAttributes(attributeMap, "id"),
		Number:    client.Int64FromAttributes(attributeMap, "number"),
		NewNumber: client.Int64FromAttributes(attributeMap, "newnumber"),
	}, nil
}

func (i *Item) GetTable() client.DynamoTable {
	return &ItemTable{}
}

func (i *Item) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	client.PopulateAttributesString(itemMap, "id", i.Id)
	client.PopulateAttributesInt64(itemMap, "number", i.Number)
	client.PopulateAttributesInt64(itemMap, "newnumber", i.NewNumber)
	client.PopulateAttributesTime(itemMap, "lastUpdated", i.LastUpdated)
	return itemMap
}

func (i *Item) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	client.PopulateAttributesString(keyMap, "id", i.Id)
	client.PopulateAttributesInt64(keyMap, "number", i.Number)
	return keyMap
}

func (i *Item) Equals(other client.DynamoTableRecord) bool {
	otherItem, isItem := other.(*Item)
	if !isItem {
		return false
	} else {
		return i.Id == otherItem.Id &&
			i.Number == otherItem.Number &&
			i.NewNumber == otherItem.NewNumber
	}
}
