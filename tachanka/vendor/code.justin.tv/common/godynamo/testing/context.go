package testing

import (
	"code.justin.tv/common/godynamo/client"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"math/rand"
	"time"
)

const (
	awsRegion                      = "us-east-1"
	localDynamoPort                = "9001"
	localDynamoEndpoint            = "http://localhost:" + localDynamoPort
	acceptableUpdateTimeDeltaLocal = "15s"
	acceptableUpdateTimeDeltaReal  = "30s"
	defaultWaitTimeout             = "20s"
)

type DynamoTestDao interface {
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *client.DynamoClientConfig
}

type DynamoTestContext struct {
	AcceptableUpdateTimeDelta time.Duration
	Suffix                    string
	TestCredentials           *credentials.Credentials
	TestEndpoint              string
	Daos                      []DynamoTestDao
	WaitTimeout               time.Duration
}

//NewDynamoTestContext creates a new dynamo test context. It assumes DynamoLocal is running.
func NewDynamoTestContext(packageUnderTest string) (*DynamoTestContext, error) {
	rand.Seed(time.Now().UTC().UnixNano())

	dynamoContext := &DynamoTestContext{
		Suffix: packageUnderTest + "-testContext",
	}

	var err error
	dynamoContext.WaitTimeout, err = time.ParseDuration(defaultWaitTimeout)
	if err != nil {
		return nil, err
	}

	// Setup dummy credentials and use and endpoint corresponding to the locally running dynamodb
	dynamoContext.TestCredentials = credentials.NewStaticCredentials("testId", "testSecret", "testToken")
	dynamoContext.TestEndpoint = localDynamoEndpoint
	dynamoContext.AcceptableUpdateTimeDelta, err = time.ParseDuration(acceptableUpdateTimeDeltaLocal)

	if err != nil {
		return nil, err
	}

	return dynamoContext, nil
}

//CLeanup cleans up the context. Notably, it deletes every table it is aware of.
func (c *DynamoTestContext) Cleanup() error {
	// Delete all tables created for testing
	for _, dao := range c.Daos {
		// First, set the wait timeouts back to normal to make sure they have enough time to get ready for deletion
		clientConfig := dao.GetClientConfig()
		clientConfig.TableExistenceWaitTimeoutOverride = c.WaitTimeout
		clientConfig.TableStatusWaitTimeoutOverride = c.WaitTimeout

		err := dao.DeleteTable()
		if err != nil {
			return err
		}
	}

	return nil
}

//CreateTestClientConfig creates the test config to run with DynamoLocal.
func (c *DynamoTestContext) CreateTestClientConfig(existenceWaitTimeout, statusWaitTimeout time.Duration, prefix string) *client.DynamoClientConfig {
	return &client.DynamoClientConfig{
		AwsRegion:                         awsRegion,
		TablePrefix:                       prefix,
		TableSuffix:                       c.Suffix,
		EndpointOverride:                  c.TestEndpoint,
		CredentialsOverride:               c.TestCredentials,
		TableExistenceWaitTimeoutOverride: existenceWaitTimeout,
		TableStatusWaitTimeoutOverride:    statusWaitTimeout,
	}
}

//CreateTestClient creates a test client built using the test configuration.
func (c *DynamoTestContext) CreateTestClient(existenceWaitTimeout, statusWaitTimeout time.Duration, prefix string) *client.DynamoClient {
	clientConfig := c.CreateTestClientConfig(existenceWaitTimeout, statusWaitTimeout, prefix)
	return client.NewClient(clientConfig)
}

func (c *DynamoTestContext) RegisterTestingDao(dao DynamoTestDao) error {
	err := dao.SetupTable()
	if err != nil {
		return err
	}

	c.Daos = append(c.Daos, dao)
	return nil
}
