'use strict';

const AWS = require('aws-sdk');
const http = require("http");
const request = require('request')
const SQS = new AWS.SQS({ apiVersion: '2012-11-05' });
const Lambda = new AWS.Lambda({ apiVersion: '2015-03-31' });

const UPDATE_SUBMISSION_URL = process.env.updateSubmissionUrl; // The URL for the update submission step status API stored in the updateSubmissionUrl environment variable
const QUEUE_URL = process.env.queueUrl; // The queue URL stored in the queueUrl environment variable
const FAIL_QUEUE_URL = process.env.failQueueUrl; // The queue url for messages that aren't processed correctly
const PROCESS_MESSAGE = 'process-message';
const EVENT_INVOCATION_TYPE = 'Event';
const CONTENT_TYPE = "content-type";
const JSON_CONTENT_TYPE = "application/json";
const MAX_MESSAGES_TO_POLL = 10;
const VISIBILITY_TIMEOUT_SECS = 10;

function invokePoller(functionName, message) {
    const payload = {
        operation: PROCESS_MESSAGE,
        message,
    };
    const params = {
        FunctionName: functionName,
        InvocationType: EVENT_INVOCATION_TYPE,
        Payload: new Buffer(JSON.stringify(payload)),
    };

    return new Promise((resolve, reject) => {
        Lambda.invoke(params, (err) => (err ? reject(err) : resolve()));
    });
}

function processMessage(data, callback) {
    var body = JSON.parse(data.Body);
    var msg = JSON.parse(body.Message);
    console.log('msg received: ', msg)
    var err = updateStatus(msg.ProductId, msg.Status, msg.ASIN);

    if (err) {
        var failQueueParams = {
            MessageBody: data.Body,
            QueueUrl: FAIL_QUEUE_URL
        };
        console.log('Sending message to Fail queue')
        SQS.sendMessage(failQueueParams, function(err, data) {
            if (err) {
                console.log('Error %s posting message %s to failure queue', err, data.Body);
            }
        });
    }

    const params = {
        QueueUrl: QUEUE_URL,
        ReceiptHandle: data.ReceiptHandle,
    };
    SQS.deleteMessage(params, (err) => callback(err, data));
}

function updateStatus(productId, status, ASIN) {
    console.log('Updating status for ProductID: %s to %s, with ASIN: %s', productId, status, ASIN);
    //  Making http Call to something
    var options = {
      method: 'post',
      json: true,
      url: UPDATE_SUBMISSION_URL,
      body: {
            product_id: productId,
            status: status,
            asin: ASIN
        },
      headers: {
        CONTENT_TYPE: JSON_CONTENT_TYPE,
      }
    }

    request(options, function (err, res, body) {
      if (err) {
        console.log('Error %s calling UpdateSubmissionStepStatus API for ProductID: %s to %s, with ASIN: %s:', err, productId, status, ASIN)
        return err;
      }
      console.log('Response from UpdateSubmissionStepStatus :', body)
    });
}

function poll(functionName, callback) {
    const params = {
        QueueUrl: QUEUE_URL,
        MaxNumberOfMessages: MAX_MESSAGES_TO_POLL,
        VisibilityTimeout: VISIBILITY_TIMEOUT_SECS, // http://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-visibility-timeout.html
    };
    // batch request messages
    SQS.receiveMessage(params, (err, data) => {
        if (err) {
            console.log('Error polling message from SQS ', err)
            return callback(err);
        }
        if ("Messages" in data) {
            // for each message, reinvoke the function
            const promises = data.Messages.map((message) => invokePoller(functionName, message));

            // complete when all invocations have been made
            Promise.all(promises).then(() => {
                const result = `Messages received: ${data.Messages.length}`;
                console.log(result);
                callback(null, result);
            });
        }
    });
}

exports.handler = (event, context, callback) => {
    try {
        if (event.operation === PROCESS_MESSAGE) {
            // invoked by poller
            processMessage(event.message, callback);
        } else {
            // invoked by schedule
            poll(context.functionName, callback);
        }
    } catch (err) {
        callback(err);
    }
};
