'use strict';

const AWS = require("aws-sdk");
const DOC = require('dynamodb-doc');
const docClient = new DOC.DynamoDB();

// Environment constants
const submissionStepsTableName = "dev_submission-steps";
const submissionTableName = 'dev_submissions';
const region = "us-west-2";

AWS.config.update({region: region});

function PopulateSubmissionSteps(submission) {
    submission['submission_steps'] = [];
    
    // Not all submissions have steps
    const concatenatedSubmissionStepIds = submission['submission-steps'];

    delete submission['submission-steps'];

    if (!concatenatedSubmissionStepIds) {
        return Promise.resolve(submission);
    }

    // Split the submission step ids up, and filter out any incorrectly formatted ones
    let submissionStepIds = concatenatedSubmissionStepIds.split(':').filter(id => id.length > 0);

    // Load all the submission steps and convert their date field
    const submissionStepLoadingPromises = [];
    submissionStepIds.forEach((element) => {
        submissionStepLoadingPromises.push(new Promise((resolve, reject) => {
            const params = {};
            params.TableName = submissionStepsTableName;
            params.Key = { id : element };
            docClient.getItem(params, (err, result) => {
                if (err) {
                    reject(err);
                }
                else {
                    result.Item.createdDate = new Date(result.Item.lastUpdated / 1000000).toISOString();
                    delete result.Item.lastUpdated;
                    resolve(result.Item);
                }
            });
                
        }));
    });   
 
    return Promise.all(submissionStepLoadingPromises).then((submissionSteps) => {
        submission['submission_steps'] = submission['submission_steps'].concat(submissionSteps);
        console.log('Migrating SubmissionStep:');
        submission['submission_steps'].forEach(submissionStep => console.log(JSON.stringify(submissionStep, undefined, 2)));
        return submission;
    });
}

function ConvertSubmissionFields(submission) {
    return new Promise((resolve, reject) => {
        // Managed Product Id
        if (submission.hasOwnProperty('managed-product-id')) {
            let managedProductId = submission['managed-product-id'];
            submission['managed_product_id'] = managedProductId;
            delete submission['managed-product-id'];
        }

        // Last Updated
        if (submission.hasOwnProperty('lastUpdated')) {
            let lastUpdated = submission['lastUpdated'];
            submission['last_updated'] = new Date(lastUpdated / 1000000).toISOString();
            delete submission['lastUpdated'];
        }

        // Created Date
        submission['created_date'] = submission['last_updated']; // Best I can do

        // Status
        submission['status'] = 'COMPLETE'; // Again, about as good as it gets
        
        resolve(submission);
    });

}

exports.handler = (event, context, callback) => {
    
    // Scan the submissions table to load all submissions that haven't been converted
    const promise = new Promise((resolve, reject) => {
        let params = {
            TableName: 'dev_submissions',
            FilterExpression: 'attribute_not_exists(managed_product_id)'
        };

        docClient.scan(params, (err, result) => {
            if (err) {
                console.log(err);
                reject("Failed to read submissions");
            }
            else {
                console.log("Found " + result.Items.length + " submissions to migrate: ");
                result.Items.forEach(submission => console.log(JSON.stringify(submission, undefined, 2)));
                resolve(result.Items);
            }
        });
    }).then(submissions => { // Populate the submission steps
        const populatePromises = [];
        submissions.forEach(submission => populatePromises.push(PopulateSubmissionSteps(submission)));
        
        return Promise.all(populatePromises);
    }).then(submissions => { // Convert the submission fields
        const convertPromises = [];
        submissions.forEach(submission => convertPromises.push(ConvertSubmissionFields(submission)));
        
        return Promise.all(convertPromises);        
    }).then((submissionsWithSteps) => { // Update the submissions in place
        let submissionPutPromises = [];

        submissionsWithSteps.forEach(submission => {
            console.log('Updating Submission:');
            console.log(JSON.stringify(submission, undefined, 2)
        );
            let params = {
                TableName: submissionTableName,
                Item: submission
            };

            submissionPutPromises = submissionPutPromises.push(docClient.putItem(params).promise());
        });

        return Promise.all(submissionPutPromises).then(result => {
            console.log('Migrated ' + submissionsWithSteps.length + ' submissions');
            callback(null, 'Migration Successful');
        });
    }).catch((error) => {
        callback(null, 'Encountered Error while migrating submissions' + error);
    });
};
