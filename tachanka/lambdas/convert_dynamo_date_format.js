'use strict';

const AWS = require("aws-sdk");
const DOC = require('dynamodb-doc');
const docClient = new DOC.DynamoDB();

// Environment constants
const tableName = process.env.tableName;
const dateFieldName = process.env.dateFieldName;
const updatedDateFieldName = process.env.updatedDateFieldName;
const region = "us-west-2";
const microsecondsInSecond = 1000000;

AWS.config.update({region: region});

function ConvertLastUpdateDateField(tableRecord) {
    return new Promise((resolve, reject) => {
        if (tableRecord.hasOwnProperty(dateFieldName)) {
            let oldDate = tableRecord[dateFieldName];
            tableRecord[updatedDateFieldName] = new Date(oldDate / microsecondsInSecond).toISOString();

            delete tableRecord[dateFieldName];
        }

        resolve(tableRecord);
    });

}

exports.handler = (event, context, callback) => {

    // Scan the table to load all records
    const promise = new Promise((resolve, reject) => {
        let params = {
            TableName: tableName,
        };

        docClient.scan(params, (err, result) => {
            if (err) {
                console.log(err);
                reject("Failed to read");
            }
            else {
                console.log("Found " + result.Items.length + " records to migrate: ");
                result.Items.forEach(tableRecord => console.log(JSON.stringify(tableRecord, undefined, 2)));
                resolve(result.Items);
            }
        });
    }).then(tableRecord => { // Convert the fields
        const convertPromises = [];
        tableRecord.forEach(tableRecord => convertPromises.push(ConvertLastUpdateDateField(tableRecord)));

        return Promise.all(convertPromises);
    }).then((updatedRecord) => { // Update the records in place
        let recordPutPromises = [];

        updatedRecord.forEach(tableRecord => {
            console.log('Updating record:');
            console.log(JSON.stringify(tableRecord, undefined, 2)
        );
            let params = {
                TableName: tableName,
                Item: tableRecord
            };

            recordPutPromises = recordPutPromises.push(docClient.putItem(params).promise());
        });

        return Promise.all(recordPutPromises).then(result => {
            console.log('Migrated ' + updatedRecord.length + ' records');
            callback(null, 'Migration Successful');
        });
    }).catch((error) => {
        callback(null, 'Encountered Error while migrating updatedRecord' + error);
    });
};
