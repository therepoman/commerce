package service_test

import (
	. "github.com/smartystreets/goconvey/convey"

	"encoding/json"
	"io/ioutil"
	"os"
	"testing"
	"time"

	"code.justin.tv/commerce/tachanka/mocks"
	"code.justin.tv/commerce/tachanka/models"
	"code.justin.tv/commerce/tachanka/service"
	"github.com/satori/go.uuid"
	"github.com/stretchr/testify/mock"
)

var (
	vendorId     = "testVendorId"
	domainId     = "testDomainId"
	adgProductId = "testADGProductId"
)

func TestADGSubmissionBuilderService(t *testing.T) {
	Convey("Given a new ADGSubmissionBuilderService and valid DPP", t, func() {
		mockManagedProductService := new(mocks.IManagedProductsService)
		submissionBuilderService := service.NewADGSubmissionBuilder(mockManagedProductService)

		dpp := &service.DevPortalProduct{
			Id:                uuid.NewV4().String(),
			ManagedProductIds: []string{},
			ADGDomainId:       &domainId,
			ADGVendorId:       &vendorId,
		}

		Convey("And given a valid game", func() {
			game := newADGSubmissionTestGame()
			dpp.ManagedProductIds = append(dpp.ManagedProductIds, game.Id)

			Convey("When we build a submission for that game", func() {
				doc, err := submissionBuilderService.Build(dpp, &game)

				Convey("It should work", func() {
					So(err, ShouldBeNil)
					So(doc, ShouldNotBeEmpty)
				})
			})

			Convey("And given a valid IGC", func() {
				igc := newADGSubmissionTestIGC()
				dpp.ManagedProductIds = append(dpp.ManagedProductIds, igc.Id)
				game.ManagedProductMetadata.ADGProductId = &adgProductId

				Convey("When we build a submission for that igc", func() {
					mockManagedProductService.On("GetManagedProducts", mock.Anything).Return([]models.ManagedProduct{&game}, nil).Once()
					doc, err := submissionBuilderService.Build(dpp, &igc)

					Convey("It should work", func() {
						So(err, ShouldBeNil)
						So(doc, ShouldNotBeEmpty)
					})
				})
			})
		})

		Convey("And given a valid extension", func() {
			extension := newADGSubmissionTestExtension()
			dpp.ManagedProductIds = append(dpp.ManagedProductIds, extension.Id)

			Convey("When we build a submission for that game", func() {
				doc, err := submissionBuilderService.Build(dpp, &extension)

				Convey("It should work", func() {
					So(err, ShouldBeNil)
					So(doc, ShouldNotBeEmpty)
				})
			})

			Convey("And given a valid extension iap", func() {
				iap := newADGSubmissionTestExtensionIAP()
				dpp.ManagedProductIds = append(dpp.ManagedProductIds, iap.Id)
				extension.ManagedProductMetadata.ADGProductId = &adgProductId

				Convey("When we build a submission for that igc", func() {
					mockManagedProductService.On("GetManagedProducts", mock.Anything).Return([]models.ManagedProduct{&extension}, nil).Once()
					doc, err := submissionBuilderService.Build(dpp, &iap)

					Convey("It should work", func() {
						So(err, ShouldBeNil)
						So(doc, ShouldNotBeEmpty)
					})
				})
			})
		})
	})
}

func newADGSubmissionTestGame() models.Game {
	var game models.Game
	err := json.Unmarshal([]byte(newDocumentString("valid_game_data_1.json")), &game)
	if err != nil {
		panic(err)
	}
	game.SetId(uuid.NewV4().String())
	game.SetLastUpdated(time.Now())
	return game
}

func newADGSubmissionTestIGC() models.InGameContent {
	var igc models.InGameContent
	err := json.Unmarshal([]byte(newDocumentString("valid_igc_data_1.json")), &igc)
	if err != nil {
		panic(err)
	}
	igc.SetId(uuid.NewV4().String())
	igc.SetLastUpdated(time.Now())
	return igc
}

func newADGSubmissionTestExtension() models.Extension {
	var extension models.Extension
	err := json.Unmarshal([]byte(newDocumentString("valid_extensions_data.json")), &extension)
	if err != nil {
		panic(err)
	}
	extension.SetId(uuid.NewV4().String())
	extension.SetLastUpdated(time.Now())
	return extension
}

func newADGSubmissionTestExtensionIAP() models.ExtensionIAP {
	var extensionIAP models.ExtensionIAP
	err := json.Unmarshal([]byte(newDocumentString("valid_extension_iap_data.json")), &extensionIAP)
	if err != nil {
		panic(err)
	}
	extensionIAP.SetId(uuid.NewV4().String())
	extensionIAP.SetLastUpdated(time.Now())
	return extensionIAP
}

func newADGSubmissionTestDocumentString(fileName string) string {
	var validDocumentFile string = os.Getenv("GOPATH") + "/src/code.justin.tv/commerce/tachanka/config/data/front_end_requests/" + fileName
	validJsonFile, err := ioutil.ReadFile(validDocumentFile)
	if err != nil {
		panic(err)
	}
	return string(validJsonFile)
}
