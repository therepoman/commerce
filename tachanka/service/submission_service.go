package service

import (
	"errors"

	"code.justin.tv/commerce/tachanka/models/adg"

	"github.com/Sirupsen/logrus"
	"github.com/satori/go.uuid"

	adgClient "code.justin.tv/commerce/tachanka/client/adg"
	"code.justin.tv/commerce/tachanka/dynamo"
	"code.justin.tv/commerce/tachanka/models"
)

type ISubmissionService interface {
	SubmitManagedProduct(dpp *DevPortalProduct, managedProduct models.ManagedProduct, tuid string) (*models.Submission, error)
	CreateSubmission(dpp *DevPortalProduct, managedProduct models.ManagedProduct, tuid string) (*models.Submission, error)
	AddSubmissionStep(submissionId string, stepType models.SubmissionStepType, stepStatus models.SubmissionStepStatus) (*models.Submission, error)
	GetSubmissionsByManagedProduct(managedProductId string) ([]models.Submission, error)
}

type SimpleSubmissionService struct {
	docDao                dynamo.IDocumentDao
	submissionDao         dynamo.SubmissionDao
	managedProductService IManagedProductsService
	adgClient             adgClient.ADGSubmissionServiceClient
	submissionBuilder     SubmissionBuilder
}

func NewSubmissionService(mpService IManagedProductsService, dDao dynamo.IDocumentDao, sDao dynamo.SubmissionDao, adgClient adgClient.ADGSubmissionServiceClient, submissionBuilder SubmissionBuilder) ISubmissionService {
	return SimpleSubmissionService{
		managedProductService: mpService,
		docDao:                dDao,
		submissionDao:         sDao,
		adgClient:             adgClient,
		submissionBuilder:     submissionBuilder,
	}
}

func (s SimpleSubmissionService) SubmitManagedProduct(dpp *DevPortalProduct, managedProduct models.ManagedProduct, tuid string) (*models.Submission, error) {
	return s.submit(dpp, managedProduct, tuid, true)
}

func (s SimpleSubmissionService) CreateSubmission(dpp *DevPortalProduct, managedProduct models.ManagedProduct, tuid string) (*models.Submission, error) {
	return s.submit(dpp, managedProduct, tuid, false)
}

func (s SimpleSubmissionService) submit(dpp *DevPortalProduct, managedProduct models.ManagedProduct, tuid string, submitToADG bool) (*models.Submission, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":              "SimpleSubmissionService.SubmitManagedProduct",
		"Dev Portal Product Id": dpp.Id,
		"Managed Product Id":    managedProduct.GetId(),
		"Tuid":                  tuid,
	})
	log.Debug("Entering")

	// We can only make new submissions if the previous is complete, or this is the first time we are submitting
	liveSubmissionId := managedProduct.GetLiveSubmissionId()
	if liveSubmissionId != "" {
		submissionInProgress, err := s.isSubmissionInProgress(liveSubmissionId)
		if err != nil {
			log.Warn("Error checking if submission is complete", err)
			return nil, err
		}

		if submissionInProgress {
			log.Warn("Submission in progress. Unable to create new submission")
			return nil, errors.New("Submission in progress. Unable to create new submission")
		}
	}

	doc, err := s.cloneValidDocument(managedProduct.GetDraftDocumentId())
	if err != nil {
		log.Warn("Error cloning document: ", err)
		return nil, err
	}

	// Create the submission
	submission := models.NewSubmission(managedProduct.GetId(), doc.DocumentId)
	err = s.UpdateSubmission(submission)
	if err != nil {
		return nil, err
	}

	if !submitToADG {
		log.Debug("Exiting due to not submitting to adg.")
		return submission, nil
	}

	adgSubmission, err := s.submissionBuilder.Build(dpp, managedProduct)
	if err != nil {
		log.WithError(err).Warn("Error building adg document")
		return nil, err
	}

	// We need to create a new product if we don't have and ADGProductId
	if len(managedProduct.GetADGProductId()) == 0 {
		log.Debug("Creeating ADG Product")
		err = s.createADGProduct(submission.Id, managedProduct, adgSubmission)
	} else {
		log.Debug("Submitting revision")
		err = s.createRevision(submission.Id, adgSubmission)
	}

	if err != nil {
		log.Warn("Error publishing submission to ADGSubmissionService ", err)
		return nil, err
	}

	_, err = s.AddSubmissionStep(submission.Id, models.SubmissionStepType_AdgSubmit, models.SubmissionStepStatus_Running)
	if err != nil {
		log.Warn("Error adding submission step", err)
	}

	log.Debug("Exiting")
	return submission, nil
}

func (s SimpleSubmissionService) createADGProduct(submissionId string, managedProduct models.ManagedProduct, adgSubmission *adg.ADGSubmission) error {
	log := logrus.WithFields(logrus.Fields{
		"Function":         "SimpleSubmissionService.createADGProduct",
		"SubmissionId":     submissionId,
		"ManagedProductId": managedProduct.GetId(),
	})
	log.Debug("Entering")

	if managedProduct.GetADGProductId() != "" {
		log.Warn("Error - attempting to create ADG Product for Managed Product with preexisting ADG Product.")
		return errors.New("Error - ADGProduct already exists")
	}

	_, err := s.AddSubmissionStep(submissionId, models.SubmissionStepType_AdgCreateProduct, models.SubmissionStepStatus_Running)
	if err != nil {
		log.Warn("Error adding create product submission step", err)
		return err
	}

	response, err := s.adgClient.CreateProduct(adgSubmission)
	if err != nil {
		log.Warn("Error creating ADG Product", err)
		s.AddSubmissionStep(submissionId, models.SubmissionStepType_AdgCreateProduct, models.SubmissionStepStatus_Error)
		return err
	}

	// Udpate the managed product with the adg product id
	log.WithField("ADGProductId", response.GetADGProductId()).Debug("Updating managed product with ADGProductID")

	managedProduct.SetADGProductId(response.GetADGProductId())
	err = s.managedProductService.UpdateManagedProduct(managedProduct)
	if err != nil {
		log.WithField("ADGProductId", response.GetADGProductId()).Error("Error updating managed product with ADGProductID", err)
		s.AddSubmissionStep(submissionId, models.SubmissionStepType_AdgCreateProduct, models.SubmissionStepStatus_Error)
		return err
	}

	_, err = s.AddSubmissionStep(submissionId, models.SubmissionStepType_AdgCreateProduct, models.SubmissionStepStatus_Complete)
	if err != nil {
		log.Warn("Error adding create product submission step", err)
	}

	return nil
}

func (s SimpleSubmissionService) createRevision(submissionId string, adgSubmission *adg.ADGSubmission) error {
	log := logrus.WithFields(logrus.Fields{
		"Function":     "SimpleSubmissionService.createRevision",
		"SubmissionId": submissionId,
	})
	log.Debug("Entering")

	_, err := s.AddSubmissionStep(submissionId, models.SubmissionStepType_AdgCreateRevision, models.SubmissionStepStatus_Running)
	if err != nil {
		log.Warn("Error adding create revision submission step", err)
		return err
	}

	_, err = s.adgClient.CreateRevision(adgSubmission)
	if err != nil {
		log.Warn("Error creating ADG Product", err)
		s.AddSubmissionStep(submissionId, models.SubmissionStepType_AdgCreateRevision, models.SubmissionStepStatus_Error)
		return err
	}

	_, err = s.AddSubmissionStep(submissionId, models.SubmissionStepType_AdgCreateRevision, models.SubmissionStepStatus_Complete)
	if err != nil {
		log.Warn("Error adding create revision submission step", err)
	}

	return nil
}

func (s SimpleSubmissionService) isSubmissionInProgress(submissionId string) (bool, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":      "SimpleSubmissionService.isSubmissionInProgress",
		"Submission Id": submissionId,
	})
	log.Debug("Entering")

	submission, err := s.submissionDao.Get(submissionId)
	if err != nil {
		log.Warn("Error reading submission.", err)

		// If we can't load a submission, assume one is in progress (safe assumption)
		return false, err
	}

	log.Debug("Exiting")
	return (submission != nil) && (submission.Status == models.SubmissionStatus_InProgress), nil
}

func (s SimpleSubmissionService) fetchDocument(mp *dynamo.ManagedProductDynamoEntry) (*dynamo.Document, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":           "SimpleSubmissionService.fetchDocument",
		"Managed Product Id": mp.Id,
	})
	log.Debug("Entering")
	// Fetch the document
	fetchedDoc, docErr := s.docDao.Get(*mp.DraftDocumentId)
	if docErr != nil {
		log.WithField("Document Id", *mp.DraftDocumentId).Warn("Error fetching Document", docErr)
		return nil, docErr
	}

	log.Debug("Exiting")
	return fetchedDoc, nil
}

func (s SimpleSubmissionService) cloneValidDocument(docId string) (*dynamo.Document, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":    "SimpleSubmissionService.cloneValidDocument",
		"Document Id": docId,
	})
	log.Debug("Entering")

	fetchedDoc, err := s.docDao.Get(docId)
	if err != nil {
		log.Warn("Error fetching document from DAO", err)
		return nil, err
	}

	// Save a copy of the document
	docCopyId := uuid.NewV4().String()
	fetchedDoc.DocumentId = docCopyId

	err = s.docDao.Put(fetchedDoc)
	if err != nil {
		log.Warn("Error saving updated document", err)
		return nil, err
	}
	log.Debug("Exiting")
	return fetchedDoc, nil
}

func (s SimpleSubmissionService) UpdateSubmission(submission *models.Submission) error {
	log := logrus.WithFields(logrus.Fields{
		"Function":      "SimpleSubmissionService.UpdateSubmission",
		"Submission Id": submission.Id,
	})

	err := s.submissionDao.Put(submission)
	if err != nil {
		log.WithField("Submission", submission.Id).Warn("Error saving submission", err)
	}

	return err
}

func (s SimpleSubmissionService) AddSubmissionStep(submissionId string, stepType models.SubmissionStepType, stepStatus models.SubmissionStepStatus) (*models.Submission, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":      "SimpleSubmissionService.AddSubmissionStep",
		"Submission Id": submissionId,
		"Step Type":     stepType,
		"Step Status":   stepStatus,
	})

	step := models.NewSubmissionStep(stepType, stepStatus)

	submission, err := s.submissionDao.AddStep(submissionId, step)
	if err != nil {
		log.Warn("Error adding submission step", err)
		return nil, err
	}

	// Only in progress submissions can have their status updated
	submissionStatus := DetermineSubmissionStatus(step)
	if submission.Status == models.SubmissionStatus_InProgress && submissionStatus != models.SubmissionStatus_InProgress {
		submission, err = s.submissionDao.UpdateStatus(submissionId, submissionStatus)
		if err != nil {
			log.Warn("Error adding submission step", err)
			return nil, err
		}
	}

	return submission, nil
}

func (s SimpleSubmissionService) GetSubmissionsByManagedProduct(managdProductId string) ([]models.Submission, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":          "SimpleSubmissionService.GetSubmissions",
		"ManagedProduct Id": managdProductId,
	})

	var results []models.Submission
	results, err := s.submissionDao.GetByManagedProduct(managdProductId)
	if err != nil {
		log.Warn("Error getting submissions by managed product id", err)
		return nil, err
	}

	return results, nil
}

func DetermineSubmissionStatus(step *models.SubmissionStep) models.SubmissionStatus {
	var status models.SubmissionStatus

	if step.Type == models.SubmissionStepType_AdgSubmit && step.Status == models.SubmissionStepStatus_Complete {
		// If the ADG submission is complete, then the submission is complete
		status = models.SubmissionStatus_Complete
	} else if step.Status == models.SubmissionStepStatus_Error {
		// Any failed steps cause the submission to fail
		status = models.SubmissionStatus_Failed
	} else {
		status = models.SubmissionStatus_InProgress
	}

	return status
}

func DetermineFulfilledByTwitchFromVendor(vendorId string) bool {
	var fulfilledByTwitch bool
	//FIXME: THIS IS HARD CODED DATA TO DETERMINE TAX AND LIABILITY JUNK. :feelsreidr:
	if vendorId == "UBIWD" || vendorId == "XTA5P" {
		fulfilledByTwitch = false
	} else {
		fulfilledByTwitch = true
	}
	return fulfilledByTwitch

}
