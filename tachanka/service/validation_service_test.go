package service_test

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"code.justin.tv/commerce/tachanka/mocks"
	"code.justin.tv/commerce/tachanka/models"
	"code.justin.tv/commerce/tachanka/service"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

var (
	testVendorId string = "testVendorId"
	testDomainId string = "testDomainId"
)

func TestValidationService(t *testing.T) {
	Convey("Given a new ValidationService", t, func() {
		mockManagedProductService := new(mocks.IManagedProductsService)
		dbs := service.NewADGDocumentBuilder(mockManagedProductService)
		cut := service.NewValidationService(dbs)

		subId := TestSubmissionId
		mockMPGame := &models.Game{
			ManagedProductMetadata: models.ManagedProductMetadata{
				Id:               TestManagedProductId,
				DraftDocumentId:  TestDocumentId,
				LiveSubmissionId: &subId,
				ADGProductId:     nil,
			},
		}

		Convey("With an invalid Dev Portal Product", func() {
			dpp := &service.DevPortalProduct{}

			Convey("Missing both vendor and domain ids", func() {
				validationErrors, err := cut.ValidateForSubmission(dpp, mockMPGame)

				So(err, ShouldBeNil)
				So(validationErrors, ShouldNotBeNil)
				So(len(validationErrors), ShouldBeGreaterThan, 0)
				So(validationErrors, ShouldContainErrorWithField, "ADGVendorId")
			})

			Convey("Missing vendor id", func() {
				dpp.ADGDomainId = &testDomainId
				validationErrors, err := cut.ValidateForSubmission(dpp, mockMPGame)

				So(err, ShouldBeNil)
				So(validationErrors, ShouldNotBeNil)
				So(len(validationErrors), ShouldBeGreaterThan, 0)
				So(validationErrors, ShouldContainErrorWithField, "ADGVendorId")
			})
		})

		Convey("With a valid Dev Portal Product", func() {
			dpp := &service.DevPortalProduct{
				ADGVendorId: &testVendorId,
			}

			Convey("Missing domain id", func() {
				validationErrors, err := cut.ValidateForSubmission(dpp, mockMPGame)

				So(err, ShouldBeNil)
				So(validationErrors, ShouldNotContainErrorWithField, "ADGDomainId")
			})

			Convey("And an invalid Document", func() {
				// There are 30+ validation errors here
				documentData := `{"availabilityInfo":"","releaseDate":"NEEVVVAAA"}`
				mockMPGame.SetData(documentData)

				validationErrors, err := cut.ValidateForSubmission(dpp, mockMPGame)

				So(err, ShouldBeNil)
				So(validationErrors, ShouldNotBeNil)
				So(len(validationErrors), ShouldBeGreaterThan, 1)
			})

			// Validate a valid game
			Convey("When we validate an error free Game Document", func() {
				documentData := NewGameDocumentString()
				mockMPGame.SetData(documentData)

				validationErrors, err := cut.ValidateForSubmission(dpp, mockMPGame)

				So(err, ShouldBeNil)
				So(validationErrors, ShouldBeEmpty)
				for _, validationError := range validationErrors {
					fmt.Println(fmt.Sprintf("%v \n", validationError))
				}
			})

			Convey("When we validate extensions", func() {
				mockMPExt := &models.Extension{
					ManagedProductMetadata: models.ManagedProductMetadata{
						Id:               TestManagedProductId,
						DraftDocumentId:  TestDocumentId,
						LiveSubmissionId: &subId,
						ADGProductId:     nil,
					},
				}

				documentData := NewExtDocumentString()
				mockMPExt.SetData(documentData)

				// Validate a valid extension
				Convey("When we validate an error free Extension Document", func() {
					validationErrors, err := cut.ValidateForSubmission(dpp, mockMPExt)
					So(err, ShouldBeNil)
					So(validationErrors, ShouldBeEmpty)
				})

				// Validate a valid extension iap
				Convey("When we validate an error free Extension IAP Document", func() {
					mockManagedProductService.On("GetManagedProducts", mock.Anything).Return([]models.ManagedProduct{mockMPExt}, nil).Once()

					metaData := models.ManagedProductMetadata{
						Id:               TestManagedProductId,
						DraftDocumentId:  TestDocumentId,
						LiveSubmissionId: &subId,
						ADGProductId:     nil,
					}
					mockMPExtIap := models.NewExtensionIAP(metaData)

					documentData := NewExtIAPDocumentString()
					mockMPExtIap.SetData(documentData)
					validationErrors, err := cut.ValidateForSubmission(dpp, mockMPExtIap)

					So(err, ShouldBeNil)
					So(validationErrors, ShouldBeEmpty)
				})
			})
		})
	})
}

func ShouldContainErrorWithField(actual interface{}, expected ...interface{}) string {
	if len(expected) != 1 {
		return "Must provide at least one, and only one, field"
	}

	if actual == nil {
		return "Actual value cannot be nil"
	}

	validationErrors, ok := actual.([]*service.ValidationError)
	if !ok {
		return "Actual needs to be of type []*ValidationError"
	}

	for _, value := range validationErrors {
		if value.Field == expected[0] {
			return ""
		}
	}

	return fmt.Sprintf("Expected (%v) to contain: '%v' (but it didn't)!", validationErrors, expected[0])
}

func ShouldNotContainErrorWithField(actual interface{}, expected ...interface{}) string {
	if len(expected) != 1 {
		return "Must provide at least one, and only one, field"
	}

	if actual == nil {
		return "Actual value cannot be nil"
	}

	result := ShouldContainErrorWithField(actual, expected)
	if result != "" {
		return ""
	}

	return fmt.Sprintf("Expected (%v) to not contain: '%v' (but it did)!", actual, expected[0])
}

func NewGameDocumentString() string {
	var validGameDocumentFile string = os.Getenv("GOPATH") + "/src/code.justin.tv/commerce/tachanka/config/data/front_end_requests/valid_game_data_1.json"
	validJsonFile, err := ioutil.ReadFile(validGameDocumentFile)
	if err != nil {
		panic(err)
	}
	return string(validJsonFile)
}

func NewExtDocumentString() string {
	var validExtensionsDocumentFile string = os.Getenv("GOPATH") + "/src/code.justin.tv/commerce/tachanka/config/data/front_end_requests/valid_extensions_data.json"
	validJsonFile, err := ioutil.ReadFile(validExtensionsDocumentFile)
	if err != nil {
		panic(err)
	}
	return string(validJsonFile)
}

func NewExtIAPDocumentString() string {
	var validExtensionsIAPDocumentFile string = os.Getenv("GOPATH") + "/src/code.justin.tv/commerce/tachanka/config/data/front_end_requests/valid_extension_iap_data.json"
	validJsonFile, err := ioutil.ReadFile(validExtensionsIAPDocumentFile)
	if err != nil {
		panic(err)
	}
	return string(validJsonFile)
}
