package service

import (
	"errors"

	"code.justin.tv/commerce/tachanka/models"
	"code.justin.tv/commerce/tachanka/models/adg"
	"github.com/Sirupsen/logrus"
)

type ADGDocumentBuilder struct {
	managedProductService IManagedProductsService
}

func NewADGDocumentBuilder(mps IManagedProductsService) DocumentBuilder {
	return &ADGDocumentBuilder{
		managedProductService: mps,
	}
}

type DocumentBuilder interface {
	BuildADGDocument(dpp *DevPortalProduct, product models.ManagedProduct) (*adg.ADGDocument, error)
}

func (s ADGDocumentBuilder) BuildADGDocument(dpp *DevPortalProduct, product models.ManagedProduct) (*adg.ADGDocument, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":          "ADGDocumentBuilder.BuildADGDocument",
		"ManagedProduct ID": product.GetId(),
	})
	log.Debug("Entering")

	var docBuilder func(*DevPortalProduct, models.ManagedProduct) (*adg.ADGDocument, error)

	switch product.GetType() {
	case models.ModelType_Game:
		docBuilder = s.buildGameADGDocument
	case models.ModelType_Extension:
		docBuilder = s.buildExtensionADGDocument
	case models.ModelType_ExtensionInAppPurchase:
		docBuilder = s.buildExtensionIAPADGDocument
	case models.ModelType_InGameContent:
		docBuilder = s.buildGameIGCADGDocument
	default:
		log.WithField("Type", product.GetType()).Warn("Unsupported type sent for ADG Document")
		return nil, errors.New("Unknown type for building a document")
	}

	log.Debug("Exiting")
	return docBuilder(dpp, product)
}

func (s ADGDocumentBuilder) buildGameADGDocument(dpp *DevPortalProduct, product models.ManagedProduct) (*adg.ADGDocument, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":          "ADGDocumentBuilder.buildGameADGDocument",
		"ManagedProduct ID": product.GetId(),
	})
	log.Debug("Entering")

	game, ok := product.(*models.Game)
	if !ok {
		log.Warn("Error casting Managed Product to InGameContent.")
		return nil, errors.New("Managed Product claimed to be an In Game Content but failed to cast!")
	}

	doc := adg.NewBaseGameDocument()
	doc = game.PopulateADGDocument(doc)

	log.Debug("Exiting")
	return &doc, nil
}

func (s ADGDocumentBuilder) buildGameIGCADGDocument(dpp *DevPortalProduct, product models.ManagedProduct) (*adg.ADGDocument, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":          "ADGDocumentBuilder.buildGameIGCADGDocument",
		"ManagedProduct ID": product.GetId(),
	})
	log.Debug("Entering")

	igc, ok := product.(*models.InGameContent)
	if !ok {
		log.Warn("Error casting Managed Product to InGameContent.")
		return nil, errors.New("Managed Product claimed to be an In Game Content but failed to cast!")
	}

	parent, err := s.findSellableParent(dpp, models.ModelType_Game)
	if err != nil {
		log.Warn("Error finding parent product")
		return nil, err
	}

	doc := adg.NewBaseIGCDocument()
	doc = parent.PopulateADGDocument(doc)
	doc = igc.PopulateADGDocument(doc)

	log.Debug("Exiting")
	return &doc, nil

}

func (s ADGDocumentBuilder) buildExtensionADGDocument(dpp *DevPortalProduct, product models.ManagedProduct) (*adg.ADGDocument, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":          "ADGDocumentBuilder.buildExtensionADGDocument",
		"ManagedProduct ID": product.GetId(),
	})
	log.Debug("Entering")

	extension, ok := product.(*models.Extension)
	if !ok {
		log.Warn("Error casting Managed Product to Extension.")
		return nil, errors.New("Managed Product claimed to be an extension but failed to cast!")
	}

	doc := adg.NewBaseExtensionDocument()
	doc = extension.PopulateADGDocument(doc)

	log.Debug("Exiting")
	return &doc, nil
}

func (s ADGDocumentBuilder) buildExtensionIAPADGDocument(dpp *DevPortalProduct, product models.ManagedProduct) (*adg.ADGDocument, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":          "ADGDocumentBuilder.buildExtensionIAPADGDocument",
		"ManagedProduct ID": product.GetId(),
	})
	log.Debug("Entering")

	iap, ok := product.(*models.ExtensionIAP)
	if !ok {
		log.Warn("Error casting Managed Product to Extension.")
		return nil, errors.New("Managed Product claimed to be an extension but failed to cast!")
	}

	parent, err := s.findSellableParent(dpp, models.ModelType_Extension)
	if err != nil {
		log.Warn("Error finding parent product")
		return nil, err
	}

	doc := adg.NewBaseExtensionIAPDocument()
	doc = parent.PopulateADGDocument(doc)
	doc = iap.PopulateADGDocument(doc)

	log.Debug("Exiting")
	return &doc, nil
}

func (s ADGDocumentBuilder) findSellableParent(dpp *DevPortalProduct, productType models.ManagedProductType) (adg.Sellable, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":            "ADGDocumentBuilder.buildExtensionADGDocument",
		"DevPortalProduct ID": dpp.Id,
		"Type":                productType,
	})
	log.Debug("Entering")

	mps, err := s.managedProductService.GetManagedProducts(dpp)
	if err != nil {
		log.WithError(err).Warn("Error fetching ManagedProducts while looking for parent")
		return nil, err
	}

	var possibleParents []models.ManagedProduct
	for _, mp := range mps {
		if mp.GetType() == productType {
			possibleParents = append(possibleParents, mp)
		}
	}
	if len(possibleParents) != 1 {
		log.WithField("Number of possible parents found", len(possibleParents)).Warn("Either too many, or too few parent products.")
		return nil, errors.New("Invalid number of parent products found")
	}

	parent := possibleParents[0]
	sellableParent, ok := parent.(adg.Sellable)
	if !ok {
		log.WithField("Unsellable Product Id", parent.GetId()).Warn("Parent product not sellable!")
		return nil, errors.New("Parent product not sellable")
	}

	log.Debug("Exiting")
	return sellableParent, nil
}
