package service

import (
	"encoding/json"
	"errors"

	pb "code.justin.tv/commerce/tachanka-client"
	"code.justin.tv/commerce/tachanka/models"
	utils "code.justin.tv/commerce/tachanka/utilities"
	"code.justin.tv/common/twirp"
	"github.com/Sirupsen/logrus"
)

type SimpleTwirpTranslator struct{}

func NewTwirpTranslator() ITwirpTranslator {
	return SimpleTwirpTranslator{}
}

type ITwirpTranslator interface {
	ConvertSubmissionStepToTwirp(*models.SubmissionStep) (*pb.SubmissionStep, error)
	ConvertSubmissionToTwirp(*models.Submission) (*pb.Submission, error)
	TranslateManagedProduct(*DevPortalProduct, models.ManagedProduct) (*pb.ManagedProduct, error)
	ConvertManagedProductTypeFromTwirp(pb.ManagedProductType) (models.ManagedProductType, error)
	ConvertManagedProductTypeToTwirp(models.ManagedProductType) (pb.ManagedProductType, error)
	ConvertServiceValidationErrorToAPIValidationError(*ValidationError) *pb.ValidationError
	ExtractVendorId(*pb.ManagedProduct) (string, error)
}

func (this SimpleTwirpTranslator) ConvertSubmissionStepToTwirp(submissionStep *models.SubmissionStep) (*pb.SubmissionStep, error) {
	// Convert the date to ISO 8601 string
	convertedDate, err := submissionStep.CreatedDate.MarshalText()
	if err != nil {
		logrus.Warn("Failed to marshal SubmissionStep CreatedDate", err)
		return nil, twirp.InternalErrorWith(err)
	}

	twirpSubmissionStep := &pb.SubmissionStep{
		Id:          submissionStep.Id,
		Type:        string(submissionStep.Type),
		Status:      string(submissionStep.Status),
		CreatedDate: string(convertedDate),
	}
	return twirpSubmissionStep, nil
}

func (this SimpleTwirpTranslator) ConvertSubmissionToTwirp(submission *models.Submission) (*pb.Submission, error) {
	// Convert dates to ISO 8601 string
	convertedCreateDate, err := submission.CreatedDate.MarshalText()
	if err != nil {
		logrus.Warn("Failed to marshal Submission CreatedDate", err)
		return nil, twirp.InternalErrorWith(err)
	}

	convertedLastUpdatedDate, err := submission.LastUpdated.MarshalText()
	if err != nil {
		logrus.Warn("Failed to marshal Submission LastUpdated Date", err)
		return nil, twirp.InternalErrorWith(err)
	}

	twirpSubmission := &pb.Submission{
		Id:          submission.Id,
		Status:      string(submission.Status),
		LastUpdated: string(convertedLastUpdatedDate),
		CreatedDate: string(convertedCreateDate),
	}

	// Convert the SubmissionSteps
	if submission.SubmissionSteps != nil && len(submission.SubmissionSteps) > 0 {
		twirpSubmissionSteps := make([]*pb.SubmissionStep, len(submission.SubmissionSteps))
		for i, step := range submission.SubmissionSteps {
			twirpSubmissionSteps[i], err = this.ConvertSubmissionStepToTwirp(&step)
			if err != nil {
				logrus.Warn("Failed to convert SubmissionStep", err)
				return nil, twirp.InternalErrorWith(err)
			}
		}

		twirpSubmission.SubmissionSteps = twirpSubmissionSteps
	}

	return twirpSubmission, nil
}

func (this SimpleTwirpTranslator) TranslateManagedProduct(dpp *DevPortalProduct, mp models.ManagedProduct) (*pb.ManagedProduct, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":           "SimpleTwirpTranslator.TranslateManagedProduct",
		"Managed Product Id": mp.GetId(),
		"Dev Portal Product": dpp.Id,
	})
	log.Debug("Entering")

	// @ryanlath - This makes me feel forever unclean - Only done to put the domain and vendor IDs in the response
	// TODO - Kill this with fire. Then make FE load the Dev Portal Product
	jsonBytes, err := json.Marshal(mp)
	if err != nil {
		log.Error("Error marshalling ManagedProduct to JSON", err)
		return nil, err
	}

	var managedProductMap map[string]interface{}
	err = json.Unmarshal(jsonBytes, &managedProductMap)
	if err != nil {
		log.Error("Error unmarshalling ManagedProduct bytes to map", err)
		return nil, err
	}

	managedProductMap["adgVendorId"] = dpp.ADGVendorId
	managedProductMap["adgDomainId"] = dpp.ADGDomainId

	jsonBytes, err = json.Marshal(managedProductMap)

	twirpSubmission := &pb.Submission{
		Id: mp.GetLiveSubmissionId(),
	}

	convertedType, err := this.ConvertManagedProductTypeToTwirp(mp.GetType())
	if err != nil {
		return nil, err
	}

	document := &pb.Document{
		Id:               mp.GetDraftDocumentId(),
		ManagedProductId: mp.GetId(),
		DocumentData:     string(jsonBytes),
	}

	pbmp := &pb.ManagedProduct{
		Id:         mp.GetId(),
		Type:       convertedType,
		Submission: twirpSubmission,
		Document:   document,
	}
	return pbmp, nil
}

// Converts a TWIRP ManagedProductType to a service ManagedProductType
func (this SimpleTwirpTranslator) ConvertManagedProductTypeFromTwirp(productType pb.ManagedProductType) (models.ManagedProductType, error) {
	switch productType {
	case pb.ManagedProductType_ADG_GAME:
		return models.ModelType_Game, nil
	case pb.ManagedProductType_ADG_IGC:
		return models.ModelType_InGameContent, nil
	case pb.ManagedProductType_ADG_EXTENSION:
		return models.ModelType_Extension, nil
	case pb.ManagedProductType_ADG_EXTENSION_IAP:
		return models.ModelType_ExtensionInAppPurchase, nil
	default:
		return models.UnknownType, errors.New("Unknown Product Type.")
	}
}

// Converts a service ManagedProductType to a TWIRP API ManagedProductType
func (this SimpleTwirpTranslator) ConvertManagedProductTypeToTwirp(productType models.ManagedProductType) (pb.ManagedProductType, error) {
	switch productType {
	case models.ModelType_Game:
		return pb.ManagedProductType_ADG_GAME, nil
	case models.ModelType_InGameContent:
		return pb.ManagedProductType_ADG_IGC, nil
	case models.ModelType_Extension:
		return pb.ManagedProductType_ADG_EXTENSION, nil
	case models.ModelType_ExtensionInAppPurchase:
		return pb.ManagedProductType_ADG_EXTENSION_IAP, nil
	default:
		return pb.ManagedProductType_UNSPECIFIED, errors.New("Unknown Product Type.")
	}
}

// Validation Conversion
func (this SimpleTwirpTranslator) ConvertServiceValidationErrorToAPIValidationError(validationError *ValidationError) *pb.ValidationError {
	apiValidationError := &pb.ValidationError{
		Field:       validationError.Field,
		Description: validationError.Description,
	}

	return apiValidationError
}

// Returns Vendor Id. Will also delete it off the managed product
// This method is intended to be used when creating managed products, and should eventually be replaced by direct updates
// to the Dev Portal product.
func (this SimpleTwirpTranslator) ExtractVendorId(managedProduct *pb.ManagedProduct) (string, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":           "SimpleTwirpTranslator.ExtractDomainAndVendorIds",
		"Managed Product Id": managedProduct.Id,
	})
	log.Debug("Entering")

	var managedProductJSONMap map[string]interface{}
	err := json.Unmarshal([]byte(managedProduct.Document.DocumentData), &managedProductJSONMap)
	if err != nil {
		log.Error("Error unmarshalling document data to map", err)
		return "", twirp.InvalidArgumentError("ManagedProduct", "Invalid JSON")
	}

	// Vendor Id
	vendorId, err := utils.GetAndRemoveStringFromMap("adgVendorId", managedProductJSONMap)
	if err != nil {
		log.Error("Unable to retrieve domain id from the ManagedProduct", err)
		return "", twirp.InvalidArgumentError("Vendor Id", "Invalid or missing")
	}

	return vendorId, nil
}
