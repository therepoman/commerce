package service_test

import (
	"testing"

	"code.justin.tv/commerce/tachanka/dynamo"
	"code.justin.tv/commerce/tachanka/mocks"
	"code.justin.tv/commerce/tachanka/service"
	"github.com/satori/go.uuid"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestSimpleDevPortalProductService(t *testing.T) {

	Convey("Given a new DevPortalProductService", t, func() {
		devPortalProductDao := new(mocks.IDevPortalOwnershipDao)

		externalMappingDao := new(mocks.ExternalMappingDao)

		cut := service.NewDevPortalProductService(
			devPortalProductDao,
			externalMappingDao,
		)

		Convey("When we request a non-existent Dev Portal Product", func() {
			dpp, err := cut.GetDevPortalProduct("GAFFO!")
			So(dpp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("When we request an existent Dev Portal Product with no Vendor or Domain", func() {
			dppId := uuid.NewV4().String()
			testMPs := []*dynamo.DevPortalOwnershipEntry{
				{
					DevPortalProductId: dppId,
					ManagedProductId:   "TestMPId1",
				},
				{
					DevPortalProductId: dppId,
					ManagedProductId:   "TestMPId2",
				},
			}
			devPortalProductDao.On("Get", mock.Anything).Return(testMPs, nil).Once()
			externalMappingDao.On("Get", mock.Anything, mock.Anything).Return(nil, nil).Times(len(testMPs) * 2)

			dpp, err := cut.GetDevPortalProduct(dppId)
			So(err, ShouldBeNil)
			So(dpp, ShouldNotBeNil)
		})
	})

}
