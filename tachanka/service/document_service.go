package service

import (
	"code.justin.tv/commerce/tachanka/dynamo"
	"github.com/satori/go.uuid"
)

type SimpleDocumentService struct {
	docDao            dynamo.IDocumentDao
	emptyDocumentData []byte
}

func NewDocumentService(dDao dynamo.IDocumentDao, mpDao dynamo.IManagedProductDao, sDao dynamo.SubmissionDao) IDocumentService {
	return SimpleDocumentService{
		docDao: dDao,
	}
}

type IDocumentService interface {
	CreateDocument(mpId string) (*dynamo.Document, error)
	GetDocument(documentId string) (*dynamo.Document, error)
	UpdateDocument(documentId string, data string) (*dynamo.Document, error)
}

func (s SimpleDocumentService) UpdateDocument(documentId string, data string) (*dynamo.Document, error) {
	// Get the document.
	fetchedDoc, err := s.docDao.Get(documentId)
	if err != nil {
		return nil, err
	}

	// Wipe out the old doc with the new one, cuz it's better
	fetchedDoc.DocumentData = data

	// Save the updated doc.
	err = s.docDao.Update(fetchedDoc)
	if err != nil {
		return nil, err
	}

	return fetchedDoc, nil
}

func (s SimpleDocumentService) GetDocument(documentId string) (*dynamo.Document, error) {
	fetchedDoc, err := s.docDao.Get(documentId)
	if err != nil {
		return nil, err
	} else {
		return fetchedDoc, nil
	}
}

func (s SimpleDocumentService) CreateDocument(mpId string) (*dynamo.Document, error) {
	document := &dynamo.Document{
		DocumentId:       uuid.NewV4().String(),
		ManagedProductId: mpId,
	}

	// Persist
	dynamoError := s.docDao.Put(document)
	if dynamoError != nil {
		return nil, dynamoError
	}

	return document, nil
}
