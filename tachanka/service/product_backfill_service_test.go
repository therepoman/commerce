package service_test

import (
	"testing"

	"errors"

	"code.justin.tv/commerce/tachanka/dynamo"
	"code.justin.tv/commerce/tachanka/mocks"
	"code.justin.tv/commerce/tachanka/models"
	"code.justin.tv/commerce/tachanka/service"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

const (
	TestDPPId              string                    = "testId"
	TestAdgVendorId        string                    = "testAdgVendorId"
	TestAdgDomainId        string                    = "testAdgDomainId"
	TestManagedProductType models.ManagedProductType = "ADG_GAME"
	TestDocument           string                    = "{}"
)

func TestSimpleProductBackfillService_BackfillManagedProduct(t *testing.T) {

	Convey("Given a new ProductBackfillService", t, func() {
		TestDPP := &service.DevPortalProduct{
			Id: TestDPPId,
		}

		externalMappingService := new(mocks.IExternalMappingService)
		managedProductService := new(mocks.IManagedProductsService)
		devPortalProductService := new(mocks.IDevPortalProductService)
		devPortalProductService.On("UpdateDevPortalProduct", mock.Anything).Return(nil)
		documentService := new(mocks.IDocumentService)
		submissionService := new(mocks.ISubmissionService)

		pbs := service.NewProductBackfillService(externalMappingService, managedProductService, devPortalProductService, documentService, submissionService)

		Convey("When we attempt to backfill with invalid JSON", func() {
			mp, err := pbs.BackfillManagedProduct(TestDPP, TestManagedProductType, TestAdgVendorId, TestAdgDomainId, TestADGProductId, TestTuid, "5$#Q%$T")
			Convey("We should get an error", func() {
				So(mp, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("when the product can't be created", func() {
			externalMappingService.On("PutExternalMapping", TestDPPId, models.EM_ADGVendorId, TestAdgVendorId).Return(&models.ExternalMapping{}, nil)
			managedProductService.On("CreateManagedProduct", mock.Anything, mock.Anything, TestManagedProductType).Return(nil, errors.New("test"))

			mp, err := pbs.BackfillManagedProduct(TestDPP, TestManagedProductType, TestAdgVendorId, TestAdgDomainId, TestADGProductId, TestTuid, TestDocument)
			So(mp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when the document can't be created", func() {
			externalMappingService.On("PutExternalMapping", TestDPPId, models.EM_ADGVendorId, TestAdgVendorId).Return(&models.ExternalMapping{}, nil)
			managedProductService.On("CreateManagedProduct", mock.Anything, mock.Anything, TestManagedProductType).Return(&models.Game{}, nil)
			documentService.On("CreateDocument", mock.Anything).Return(nil, errors.New("test")).Once()

			mp, err := pbs.BackfillManagedProduct(TestDPP, TestManagedProductType, TestAdgVendorId, TestAdgDomainId, TestADGProductId, TestTuid, TestDocument)
			So(mp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when the document can't be updated", func() {
			externalMappingService.On("PutExternalMapping", TestDPPId, models.EM_ADGVendorId, TestAdgVendorId).Return(&models.ExternalMapping{}, nil)
			managedProductService.On("CreateManagedProduct", mock.Anything, mock.Anything, TestManagedProductType).Return(&models.Game{}, nil)
			documentService.On("CreateDocument", mock.Anything).Return(&dynamo.Document{DocumentId: TestDocumentId}, nil)
			documentService.On("UpdateDocument", TestDocumentId, mock.Anything).Return(nil, errors.New("test")).Once()

			mp, err := pbs.BackfillManagedProduct(TestDPP, TestManagedProductType, TestAdgVendorId, TestAdgDomainId, TestADGProductId, TestTuid, TestDocument)
			So(mp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when the managed product can't be updated", func() {
			externalMappingService.On("PutExternalMapping", TestDPPId, models.EM_ADGVendorId, TestAdgVendorId).Return(&models.ExternalMapping{}, nil)
			managedProductService.On("CreateManagedProduct", mock.Anything, mock.Anything, TestManagedProductType).Return(&models.Game{}, nil)
			documentService.On("CreateDocument", mock.Anything).Return(&dynamo.Document{DocumentId: TestDocumentId}, nil)
			documentService.On("UpdateDocument", TestDocumentId, mock.Anything).Return(&dynamo.Document{}, nil)
			managedProductService.On("UpdateManagedProduct", mock.Anything).Return(errors.New("test")).Once()

			mp, err := pbs.BackfillManagedProduct(TestDPP, TestManagedProductType, TestAdgVendorId, TestAdgDomainId, TestADGProductId, TestTuid, TestDocument)
			So(mp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when the submission can't be created", func() {
			externalMappingService.On("PutExternalMapping", TestDPPId, models.EM_ADGVendorId, TestAdgVendorId).Return(&models.ExternalMapping{}, nil)
			managedProductService.On("CreateManagedProduct", mock.Anything, mock.Anything, TestManagedProductType).Return(&models.Game{}, nil)
			documentService.On("CreateDocument", mock.Anything).Return(&dynamo.Document{DocumentId: TestDocumentId}, nil)
			documentService.On("UpdateDocument", TestDocumentId, mock.Anything).Return(&dynamo.Document{}, nil)
			managedProductService.On("UpdateManagedProduct", mock.Anything).Return(nil)
			submissionService.On("CreateSubmission", mock.Anything, mock.Anything, TestTuid).Return(nil, errors.New("test")).Once()

			mp, err := pbs.BackfillManagedProduct(TestDPP, TestManagedProductType, TestAdgVendorId, TestAdgDomainId, TestADGProductId, TestTuid, TestDocument)
			So(mp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("when we submit a valid request", func() {
			externalMappingService.On("PutExternalMapping", TestDPPId, models.EM_ADGVendorId, TestAdgVendorId).Return(&models.ExternalMapping{}, nil)
			managedProductService.On("CreateManagedProduct", mock.Anything, mock.Anything, TestManagedProductType).Return(&models.Game{}, nil)
			documentService.On("CreateDocument", mock.Anything).Return(&dynamo.Document{DocumentId: TestDocumentId}, nil)
			documentService.On("UpdateDocument", TestDocumentId, mock.Anything).Return(&dynamo.Document{}, nil)
			managedProductService.On("UpdateManagedProduct", mock.Anything).Return(nil)
			submissionService.On("CreateSubmission", mock.Anything, mock.Anything, TestTuid).Return(&models.Submission{}, nil)
			managedProductService.On("GetManagedProduct", mock.Anything).Return(&models.Game{
				ManagedProductMetadata: models.ManagedProductMetadata{
					Id: "Not Nil",
				},
			}, nil)

			mp, err := pbs.BackfillManagedProduct(TestDPP, TestManagedProductType, TestAdgVendorId, TestAdgDomainId, TestADGProductId, TestTuid, TestDocument)
			So(err, ShouldBeNil)
			So(mp, ShouldNotBeNil)

		})
	})
}
