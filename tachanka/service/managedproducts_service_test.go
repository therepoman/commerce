package service_test

import (
	"code.justin.tv/commerce/tachanka/dynamo"

	. "github.com/smartystreets/goconvey/convey"

	"errors"
	"testing"

	"code.justin.tv/commerce/tachanka/mocks"
	"code.justin.tv/commerce/tachanka/models"
	"code.justin.tv/commerce/tachanka/service"
	"github.com/satori/go.uuid"
	"github.com/stretchr/testify/mock"
)

func TestSimpleManagedProductsService(t *testing.T) {
	Convey("Given a new ManagedProductsService", t, func() {
		managedProductDao := new(mocks.IManagedProductDao)
		documentDao := new(mocks.IDocumentDao)
		devPortalProductService := new(mocks.IDevPortalProductService)
		externalMappingDao := new(mocks.ExternalMappingDao)
		documentService := new(mocks.IDocumentService)
		managedProductService := service.NewManagedProductsService(documentService, managedProductDao, documentDao, devPortalProductService, externalMappingDao)

		Convey("When we request a nonexistent product", func() {
			managedProductDao.On("Get", mock.Anything).Return(nil, nil).Once()

			managedProduct, err := managedProductService.GetManagedProduct("Nonexistent")
			Convey("We should get nothing back", func() {
				So(managedProduct, ShouldBeNil)
				So(err, ShouldBeNil)
			})
		})

		Convey("When we request a valid product", func() {
			validMPId := uuid.NewV4().String()
			managedProductDao.On("Get", mock.Anything).Return(&dynamo.ManagedProductDynamoEntry{
				Id:                 validMPId,
				ManagedProductType: models.ModelType_Game,
				DraftDocumentId:    &validMPId,
			}, nil).Once()
			externalMappingDao.On("Get", mock.Anything, mock.Anything).Return(&models.ExternalMapping{
				ParentId: validMPId,
				Type:     models.EM_ADGProductId,
				Value:    "HECKIN GOOD GAME",
			}, nil).Once()
			documentService.On("GetDocument", mock.Anything).Return(&dynamo.Document{
				DocumentId:       "Some junky doc",
				DocumentData:     "",
				ManagedProductId: validMPId,
			}, nil).Once()
			managedProduct, err := managedProductService.GetManagedProduct(validMPId)
			Convey("We should get a valid product back", func() {
				So(managedProduct, ShouldNotBeNil)
				So(err, ShouldBeNil)
			})
		})

		Convey("When we encounter an error retrieving data", func() {
			errorMPId := uuid.NewV4().String()
			managedProductDao.On("Get", errorMPId).Return(nil, errors.New("Test Error")).Once()
			managedProduct, err := managedProductService.GetManagedProduct(errorMPId)
			Convey("We should get an error back", func() {
				So(managedProduct, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("and Given a number of ManagedProducts in various states", func() {
			docId1 := uuid.NewV4().String()
			mp1 := &dynamo.ManagedProductDynamoEntry{
				Id:                 uuid.NewV4().String(),
				ManagedProductType: models.ModelType_Game,
				DraftDocumentId:    &docId1,
			}
			managedProductDao.On("Get", mp1.Id).Return(mp1, nil).Once()
			externalMappingDao.On("Get", mp1.Id, mock.Anything).Return(nil, nil).Once()
			documentService.On("GetDocument", *mp1.DraftDocumentId).Return(&dynamo.Document{
				DocumentId:   *mp1.DraftDocumentId,
				DocumentData: "",
			}, nil).Once()

			docId2 := uuid.NewV4().String()
			mp2 := &dynamo.ManagedProductDynamoEntry{
				Id:                 uuid.NewV4().String(),
				ManagedProductType: models.ModelType_Game,
				DraftDocumentId:    &docId2,
			}
			managedProductDao.On("Get", mp2.Id).Return(mp2, nil).Once()
			externalMappingDao.On("Get", mp2.Id, mock.Anything).Return(&models.ExternalMapping{
				ParentId: mp2.Id,
				Type:     models.EM_ADGProductId,
				Value:    "ADG123",
			}, nil).Once()
			documentService.On("GetDocument", *mp2.DraftDocumentId).Return(&dynamo.Document{
				DocumentId:   *mp2.DraftDocumentId,
				DocumentData: "",
			}, nil).Once()

			docId3 := uuid.NewV4().String()
			mp3 := &dynamo.ManagedProductDynamoEntry{
				Id:                 uuid.NewV4().String(),
				ManagedProductType: models.ModelType_Game,
				DraftDocumentId:    &docId3,
			}
			externalMappingDao.On("Get", mp3.Id, mock.Anything).Return(nil, nil).Once()
			managedProductDao.On("Get", mp3.Id).Return(mp3, nil).Once()
			documentService.On("GetDocument", *mp3.DraftDocumentId).Return(&dynamo.Document{
				DocumentId:   *mp3.DraftDocumentId,
				DocumentData: "",
			}, nil).Once()

			docId4 := uuid.NewV4().String()
			mp4 := &dynamo.ManagedProductDynamoEntry{
				Id:                 uuid.NewV4().String(),
				ManagedProductType: models.ModelType_Game,
				DraftDocumentId:    &docId4,
			}
			externalMappingDao.On("Get", mp4.Id, mock.Anything).Return(nil, nil).Once()
			managedProductDao.On("Get", mp4.Id).Return(mp4, nil).Once()
			documentService.On("GetDocument", *mp4.DraftDocumentId).Return(&dynamo.Document{
				DocumentId:   *mp4.DraftDocumentId,
				DocumentData: "",
			}, nil).Once()

			dpp := &service.DevPortalProduct{
				Id:                uuid.NewV4().String(),
				ManagedProductIds: []string{mp1.Id, mp2.Id, mp3.Id, mp4.Id},
			}

			Convey("When we request all of them", func() {
				results, err := managedProductService.GetManagedProducts(dpp)

				Convey("We should get them all back", func() {
					So(results, ShouldNotBeNil)
					So(err, ShouldBeNil)
				})

			})

		})
	})
}
