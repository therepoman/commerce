package service

import (
	"fmt"

	"code.justin.tv/commerce/tachanka/dynamo"
	"code.justin.tv/commerce/tachanka/models"
	"code.justin.tv/commerce/tachanka/models/adg"
	"code.justin.tv/common/twirp"
	"github.com/Sirupsen/logrus"
	"github.com/pkg/errors"
)

type SimpleManagedProductsService struct {
	documentService         IDocumentService
	managedProductDao       dynamo.IManagedProductDao
	documentDao             dynamo.IDocumentDao
	devPortalProductService IDevPortalProductService
	externalMappingDao      dynamo.ExternalMappingDao
}

func NewManagedProductsService(docService IDocumentService, mDao dynamo.IManagedProductDao, docDao dynamo.IDocumentDao, dppService IDevPortalProductService, emDao dynamo.ExternalMappingDao) IManagedProductsService {
	return SimpleManagedProductsService{
		documentService:         docService,
		managedProductDao:       mDao,
		documentDao:             docDao,
		devPortalProductService: dppService,
		externalMappingDao:      emDao,
	}
}

type IManagedProductsService interface {
	CreateManagedProduct(dpp *DevPortalProduct, mpId string, pType models.ManagedProductType) (models.ManagedProduct, error)
	GetManagedProduct(mpid string) (models.ManagedProduct, error)
	GetManagedProducts(dpp *DevPortalProduct) ([]models.ManagedProduct, error)
	UpdateManagedProduct(mp models.ManagedProduct) error
	GetManagedProductFromDoc(doc *dynamo.Document) (models.ManagedProduct, error)
	GetADGMetadataFromManagedProduct(mp models.ManagedProduct, fulfilledByTwitch bool) (*ADGProductMetadata, error)
	GetProductTitleFromManagedProduct(mp models.ManagedProduct) (string, error)
	GetFuelStoreAvailabilityFromManagedProduct(mp models.ManagedProduct) (string, error)
}

type ADGProductMetadata struct {
	adg.Metadata
	SKU string
}

func (s SimpleManagedProductsService) CreateManagedProduct(dpp *DevPortalProduct, mpId string, pType models.ManagedProductType) (models.ManagedProduct, error) {
	log := logrus.WithFields(logrus.Fields{
		"Receiver":              "SimpleManagedProductsService",
		"Function":              "CreateManagedProduct",
		"Dev Portal Product Id": dpp.Id,
		"Managed Product Id":    mpId,
		"Product Type":          pType,
	})
	log.Debug("Entering")

	// Create the backing document.
	nDoc, err := s.documentService.CreateDocument(mpId)
	if err != nil {
		log.Warn("Error creating Document: ", err)
		return nil, twirp.InternalError("Error creating Document.")
	}

	// Create the managed product.
	metadata := models.ManagedProductMetadata{
		Id:              mpId,
		DraftDocumentId: nDoc.DocumentId,
	}

	var newMP models.ManagedProduct
	switch pType {
	case models.ModelType_Game:
		newMP = models.NewGame(metadata)
	case models.ModelType_Extension:
		newMP = models.NewExtension(metadata)
	case models.ModelType_ExtensionInAppPurchase:
		newMP = models.NewExtensionIAP(metadata)
	case models.ModelType_InGameContent:
		newMP = models.NewInGameContent(metadata)
	default:
		return nil, errors.New(fmt.Sprintf("Invalid Managed Product Type: %v", pType))
	}

	// Save the newly created ManagedProduct
	err = s.UpdateManagedProduct(newMP)
	if err != nil {
		return nil, err
	}

	// Associate this ManagedProduct with it's parent DPP
	dpp.ManagedProductIds = append(dpp.ManagedProductIds, mpId)
	err = s.devPortalProductService.UpdateDevPortalProduct(dpp)
	if err != nil {
		log.Warn("Error updating Dev Portal Product", err)
		return nil, err
	}

	log.Debug("Exiting")
	return newMP, nil
}

func (s SimpleManagedProductsService) GetManagedProduct(mpId string) (models.ManagedProduct, error) {
	dynamoEntry, err := s.managedProductDao.Get(mpId)
	if err != nil {
		return nil, err
	}
	// Make sure something actually exists
	if dynamoEntry == nil {
		return nil, nil
	}

	// Make sure that it was properly created
	if dynamoEntry.DraftDocumentId == nil {
		return nil, errors.New("No inner document present!")
	}
	fetchedDocument, err := s.documentService.GetDocument(*dynamoEntry.DraftDocumentId)
	if err != nil {
		return nil, err
	}

	var desiredMP models.ManagedProduct

	switch dynamoEntry.ManagedProductType {
	case models.ModelType_Game:
		desiredMP = models.NewGame(convertToMetadata(*dynamoEntry))
	case models.ModelType_InGameContent:
		desiredMP = models.NewInGameContent(convertToMetadata(*dynamoEntry))
	case models.ModelType_Extension:
		desiredMP = models.NewExtension(convertToMetadata(*dynamoEntry))
	case models.ModelType_ExtensionInAppPurchase:
		desiredMP = models.NewExtensionIAP(convertToMetadata(*dynamoEntry))
	default:
		return nil, errors.New("Unknown/unsupported type provided")
	}

	desiredMP.SetData(fetchedDocument.DocumentData)

	// ADG Product Ids should be serialized with the document
	if len(desiredMP.GetADGProductId()) == 0 {
		adgProductId, err := s.externalMappingDao.Get(mpId, models.EM_ADGProductId)
		if err != nil {
			return nil, err
		}

		if adgProductId != nil {
			desiredMP.SetADGProductId(adgProductId.Value)
		}
	}

	return desiredMP, nil
}

func convertToMetadata(entry dynamo.ManagedProductDynamoEntry) models.ManagedProductMetadata {
	metadata := models.ManagedProductMetadata{
		Id: entry.Id,
	}

	if entry.DraftDocumentId != nil {
		metadata.SetDraftDocumentId(*entry.DraftDocumentId)
	}

	if entry.WorkingSubmissionId != nil {
		metadata.SetWorkingSubmissionId(*entry.WorkingSubmissionId)
	}

	if entry.LiveSubmissionId != nil {
		metadata.SetLiveSubmissionId(*entry.LiveSubmissionId)
	}

	metadata.SetLastUpdated(entry.LastUpdated)

	return metadata
}

func (s SimpleManagedProductsService) GetManagedProducts(dpp *DevPortalProduct) ([]models.ManagedProduct, error) {
	managedProducts := make([]models.ManagedProduct, len(dpp.ManagedProductIds))
	for i, entry := range dpp.ManagedProductIds {
		current, mpErr := s.GetManagedProduct(entry)
		if mpErr != nil {
			return nil, mpErr
		}
		managedProducts[i] = current
	}

	return managedProducts, nil
}

func (s SimpleManagedProductsService) UpdateManagedProduct(mp models.ManagedProduct) error {
	documentId := mp.GetDraftDocumentId()
	if documentId == "" {
		return errors.New("Can't update a managed product with no Document Id")
	}
	dynamoTableEntry := &dynamo.ManagedProductDynamoEntry{
		Id:                 mp.GetId(),
		ManagedProductType: mp.GetType(),
		DraftDocumentId:    &documentId,
	}
	liveSubmission := mp.GetLiveSubmissionId()
	if liveSubmission != "" {
		dynamoTableEntry.LiveSubmissionId = &liveSubmission
	}
	workingSubmission := mp.GetWorkingSubmissionId()
	if workingSubmission != "" {
		dynamoTableEntry.WorkingSubmissionId = &workingSubmission
	}
	dynamoTableEntry.LastUpdated = mp.GetLastUpdated()
	err := s.managedProductDao.Update(dynamoTableEntry)
	if err != nil {
		return err
	}

	documentData, err := mp.GetData()
	if err != nil {
		return err
	}

	_, err = s.documentService.UpdateDocument(mp.GetDraftDocumentId(), documentData)
	if err != nil {
		return err
	}

	if mp.GetADGProductId() != "" {
		_, err = s.externalMappingDao.Update(&models.ExternalMapping{
			ParentId: mp.GetId(),
			Type:     models.EM_ADGProductId,
			Value:    mp.GetADGProductId(),
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func (s SimpleManagedProductsService) GetManagedProductFromDoc(doc *dynamo.Document) (models.ManagedProduct, error) {
	return s.GetManagedProduct(doc.ManagedProductId)
}

func (s SimpleManagedProductsService) GetADGMetadataFromManagedProduct(mp models.ManagedProduct, fulfilledByTwitch bool) (*ADGProductMetadata, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":           "SimpleManagedProductsService.GetADGMetadataFromManagedProduct",
		"Managed Product Id": mp.GetId(),
	})
	log.Debug("Entering")
	sellableProduct, ok := mp.(adg.Sellable)
	if !ok {
		log.Warn("Trying to create ADG Metadata for a type that doesn't implement ADGSellable")
		return nil, errors.New("Type is not sellable via ADG")
	}

	sku := sellableProduct.GetSKU()
	adgMetadata := sellableProduct.GetADGMetadata(fulfilledByTwitch)

	log.Debug("Exiting")
	return &ADGProductMetadata{
		adgMetadata,
		sku,
	}, nil
}

func (s SimpleManagedProductsService) GetProductTitleFromManagedProduct(mp models.ManagedProduct) (string, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":           "SimpleManagedProductsService.GetProductTitleFromManagedProduct",
		"Managed Product Id": mp.GetId(),
	})
	log.Debug("Entering")

	sellableProduct, ok := mp.(adg.Sellable)
	if !ok {
		log.Warn("Trying to create ADG Metadata for a type that doesn't implement ADGSellable")
		return "", errors.New("Type is not sellable via ADG")
	}

	title := sellableProduct.GetTitle()
	if title == "" {
		log.Warn("No valid title found for managed product!")
		return "", errors.New("No title found for managed product in default locale")
	}

	title = "{\"en_US\":\"" + title + "\"}"

	log.Debug("Exiting")
	return title, nil
}

func (s SimpleManagedProductsService) GetFuelStoreAvailabilityFromManagedProduct(mp models.ManagedProduct) (string, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":           "SimpleManagedProductsService.GetFuelStoreAvailabilityFromManagedProduct",
		"Managed Product Id": mp.GetId(),
	})
	log.Debug("Entering")

	sellableProduct, ok := mp.(adg.Sellable)
	if !ok {
		log.Warn("Trying to create ADG Metadata for a type that doesn't implement ADGSellable")
		return "", errors.New("Type is not sellable via ADG")
	}

	fulfillmentMethod := sellableProduct.GetFulfillmentMethod()
	if fulfillmentMethod == "" {
		log.Warn("No fulfillment method set for managed product")
		return "", errors.New("No valid fulfillment method set")
	}

	log.Debug("Exiting")
	return fulfillmentMethod, nil
}
