package service

import (
	"errors"

	"code.justin.tv/commerce/tachanka/dynamo"
	"code.justin.tv/commerce/tachanka/models"
	"github.com/Sirupsen/logrus"
	"github.com/satori/go.uuid"
)

type SimpleDevPortalProductService struct {
	devPortalOwnershipDao dynamo.IDevPortalOwnershipDao
	externalMappingDao    dynamo.ExternalMappingDao
}

func NewDevPortalProductService(dpoDao dynamo.IDevPortalOwnershipDao, emDao dynamo.ExternalMappingDao) IDevPortalProductService {
	return SimpleDevPortalProductService{
		devPortalOwnershipDao: dpoDao,
		externalMappingDao:    emDao,
	}
}

type IDevPortalProductService interface {
	GetDevPortalProduct(devPortalProductId string) (*DevPortalProduct, error)
	UpdateDevPortalProduct(product *DevPortalProduct) error
}

type DevPortalProduct struct {
	Id                string
	ADGVendorId       *string
	ADGDomainId       *string
	ManagedProductIds []string
}

func (s SimpleDevPortalProductService) GetDevPortalProduct(devPortalProductId string) (*DevPortalProduct, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":              "SimpleDevPortalProductService.GetDevPortalProduct",
		"Dev Portal Product Id": devPortalProductId,
	})
	log.Debug("Entering")
	dppuuid, err := uuid.FromString(devPortalProductId)
	if err != nil {
		log.Warn("Error parsing UUID from DPP ID: ", err)
		return nil, errors.New("Dev Portal Product Id is not a valid UUID")
	}
	devPortalProductId = dppuuid.String()

	ownershipEntries, err := s.devPortalOwnershipDao.Get(devPortalProductId)
	if err != nil {
		log.Warn("Error retrieving Dev Portal Ownership entries", err)
		return nil, err
	}
	// Now iterate through them and get all the managed product Ids.
	managedProductIds := make([]string, len(ownershipEntries))
	for i, entry := range ownershipEntries {
		managedProductIds[i] = entry.ManagedProductId
	}

	var adgVendorString *string
	adgVendorId, err := s.externalMappingDao.Get(devPortalProductId, models.EM_ADGVendorId)
	if err != nil {
		log.Warn("Error retrieving Vendor Id from External Mapping Table", err)
		return nil, err
	}
	if adgVendorId != nil {
		adgVendorString = &adgVendorId.Value
	}

	var adgDomainString *string
	adgDomainId, err := s.externalMappingDao.Get(devPortalProductId, models.EM_ADGDomainId)
	if err != nil {
		log.Warn("Error retrieving Domain ID from External Mapping Table", err)
		return nil, err
	}
	if adgDomainId != nil {
		adgDomainString = &adgDomainId.Value
	}

	log.Debug("Exiting")
	return &DevPortalProduct{
		Id:                devPortalProductId,
		ADGVendorId:       adgVendorString,
		ADGDomainId:       adgDomainString,
		ManagedProductIds: managedProductIds,
	}, nil

}

func (s SimpleDevPortalProductService) UpdateDevPortalProduct(product *DevPortalProduct) error {
	log := logrus.WithFields(logrus.Fields{
		"Function":              "SimpleDevPortalProductService.UpdateDevPortalProduct",
		"Dev Portal Product Id": product.Id,
	})
	log.Debug("Entering")
	for _, entry := range product.ManagedProductIds {
		err := s.devPortalOwnershipDao.Put(&dynamo.DevPortalOwnershipEntry{
			DevPortalProductId: product.Id,
			ManagedProductId:   entry,
		})
		if err != nil {
			log.Warn("Error Updating Dev Portal Ownership: ", err)
			return err
		}
	}

	if product.ADGVendorId != nil {
		err := s.externalMappingDao.Put(&models.ExternalMapping{
			ParentId: product.Id,
			Type:     models.EM_ADGVendorId,
			Value:    *product.ADGVendorId,
		})
		if err != nil {
			log.Warn("Error Updating External Mapping: ", err)
			return err
		}
	}

	if product.ADGDomainId != nil {
		err := s.externalMappingDao.Put(&models.ExternalMapping{
			ParentId: product.Id,
			Type:     models.EM_ADGDomainId,
			Value:    *product.ADGDomainId,
		})
		if err != nil {
			log.Warn("Error Updating External Mapping: ", err)
			return err
		}
	}

	log.Debug("Exiting")
	return nil
}
