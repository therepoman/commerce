package service_test

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	"code.justin.tv/commerce/tachanka/mocks"
	"code.justin.tv/commerce/tachanka/service"

	"code.justin.tv/commerce/tachanka/models"
)

const (
	ExternalMappingTestId    string                         = "TestParentId"
	ExternalMappingTestType  models.ExternalMappingTypeEnum = models.EM_ADGProductId
	ExternalMappingTestValue string                         = "TestMappingValue"
)

func TestExternalMappingService(t *testing.T) {

	mappingDao := new(mocks.ExternalMappingDao)
	externalMappingService := service.NewExternalMappingService(mappingDao)

	Convey("Given a new ExternalMappingService", t, func() {
		Convey("When we put a new mapping", func() {
			mappingDao.On("Put", mock.Anything).Return(nil)

			mapping, err := externalMappingService.PutExternalMapping(ExternalMappingTestId, ExternalMappingTestType, ExternalMappingTestValue)
			Convey("We should succeed", func() {
				So(mapping, ShouldNotBeNil)
				So(err, ShouldBeNil)
			})
		})

		Convey("When we try to get a mapping by value", func() {
			mapping := models.ExternalMapping{
				ParentId: ExternalMappingTestId,
				Type:     ExternalMappingTestType,
				Value:    ExternalMappingTestValue,
			}
			var daoResult []models.ExternalMapping
			daoResult = append(daoResult, mapping)
			mappingDao.On("GetByMappingValue", mock.Anything, mock.Anything).Return(&daoResult, nil)

			mappings, err := externalMappingService.GetExternalMappingByValue(ExternalMappingTestValue, ExternalMappingTestType)
			Convey("We should succeed", func() {
				So(mappings, ShouldNotBeNil)
				So(err, ShouldBeNil)
				So((*mappings)[0], ShouldNotBeNil)
				So((*mappings)[0].Type, ShouldEqual, mapping.Type)
				So((*mappings)[0].ParentId, ShouldEqual, mapping.ParentId)
			})
		})
	})
}
