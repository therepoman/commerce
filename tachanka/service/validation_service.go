package service

import (
	"bytes"

	"github.com/Sirupsen/logrus"
	"github.com/xeipuuv/gojsonschema"

	"encoding/json"
	"errors"

	"code.justin.tv/commerce/tachanka/config"
	"code.justin.tv/commerce/tachanka/models"
	"code.justin.tv/common/twirp"
)

type (
	IValidationService interface {
		ValidateDevPortalProduct(dpp *DevPortalProduct) []*ValidationError
		ValidateForSubmission(dpp *DevPortalProduct, product models.ManagedProduct) ([]*ValidationError, error)
		ValidateManagedProduct(dpp *DevPortalProduct, product models.ManagedProduct) ([]*ValidationError, error)
	}

	ValidationError struct {
		Field       string
		Description string
	}

	ValidationService struct {
		docSchemaLoaders map[models.ManagedProductType]gojsonschema.JSONLoader
		documentBuilder  DocumentBuilder
	}
)

var (
	filePathFunctions = map[models.ManagedProductType]func() (string, error){
		models.ModelType_Game:                   config.GetMulliganGameSchemaFilePath,
		models.ModelType_InGameContent:          config.GetMulliganIGCSchemaFilePath,
		models.ModelType_Extension:              config.GetMulliganExtensionSchemaFilePath,
		models.ModelType_ExtensionInAppPurchase: config.GetMulliganExtensionIAPSchemaFilePath,
	}
)

func NewValidationService(documentBuilder DocumentBuilder) IValidationService {
	docSchemaLoaders := make(map[models.ManagedProductType]gojsonschema.JSONLoader, len(filePathFunctions))
	for modelType, filePathFunc := range filePathFunctions {
		filePath, err := filePathFunc()
		if err != nil {
			logrus.Error("Unable to retrieve schema file path for ManagedProductType: "+modelType, err)
			panic(err)
		}

		docSchemaLoaders[modelType] = gojsonschema.NewReferenceLoader("file://" + filePath)
	}

	return ValidationService{
		docSchemaLoaders: docSchemaLoaders,
		documentBuilder:  documentBuilder,
	}
}

func (vs ValidationService) ValidateDevPortalProduct(dpp *DevPortalProduct) []*ValidationError {
	log := logrus.WithFields(logrus.Fields{
		"Function":              "ValidationService.validateDevPortalProduct",
		"Dev Portal Product Id": dpp.Id,
	})
	log.Debug("Entering")

	var validationErrors []*ValidationError

	if dpp.ADGVendorId == nil {
		log.Warn("Dev Portal Product has no associated Vendor Id")
		validationErrors = append(validationErrors, &ValidationError{
			Field:       "ADGVendorId",
			Description: "Dev Portal Product has no associated Vendor Id",
		})
	}

	log.Debug("Exiting")
	return validationErrors
}

func (vs ValidationService) ValidateForSubmission(dpp *DevPortalProduct, product models.ManagedProduct) ([]*ValidationError, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":              "ValidationService.ValidateForSubmission",
		"Dev Portal Product Id": dpp.Id,
		"Managed Product Id":    product.GetId(),
		"Type":                  product.GetType(),
	})
	log.Debug("Entering")

	var validationErrors []*ValidationError

	devPortalValidationResults := vs.ValidateDevPortalProduct(dpp)
	if len(devPortalValidationResults) > 0 {
		validationErrors = append(validationErrors, devPortalValidationResults...)
	}

	managedProductValidationResults, err := vs.ValidateManagedProduct(dpp, product)
	if err != nil {
		log.Warn("Error validating managed product")
		return nil, err
	}

	if len(managedProductValidationResults) > 0 {
		validationErrors = append(validationErrors, managedProductValidationResults...)
	}

	log.Debug("Exiting")

	return validationErrors, nil
}

func (vs ValidationService) ValidateManagedProduct(dpp *DevPortalProduct, product models.ManagedProduct) ([]*ValidationError, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":           "ValidationService.ValidateManagedProduct",
		"Managed Product Id": product.GetId(),
		"Type":               product.GetType(),
	})
	log.Debug("Entering")

	if product.GetType() == "" || product.GetType() == models.UnknownType {
		log.Error("Unable to validate a ManagedProduct without a ManagedProductType")
		return nil, twirp.InvalidArgumentError("ManagedProductType", "Missing value")
	}

	adgDoc, err := vs.documentBuilder.BuildADGDocument(dpp, product)
	if err != nil {
		log.WithError(err).Warn("Error building ADG Document")
		return nil, err
	}

	// Marshal it into json
	serializedDoc, err := json.Marshal(adgDoc)
	if err != nil {
		log.WithError(err).Error("Error marshalling the ADG Doc")
		return nil, errors.New("Unable to marshal ADG Document")
	}

	var prettyJSON bytes.Buffer
	err = json.Indent(&prettyJSON, serializedDoc, "", "    ")
	if err == nil {
		// I don't really care if there is an error pretty printing
		log.WithFields(logrus.Fields{"Document": prettyJSON.String()}).Info("ADG Document to be validated")
	}

	// Validate
	docSchemaLoader, ok := vs.docSchemaLoaders[product.GetType()]
	if !ok {
		log.Error("Unable to find schema loader for ManagedProductType: " + product.GetType())
		return nil, errors.New("Unable to validate ManagedProduct with unknown type")
	}

	documentLoader := gojsonschema.NewBytesLoader(serializedDoc)
	result, err := gojsonschema.Validate(docSchemaLoader, documentLoader)
	if err != nil {
		log.Error("Error Validating ADG json", err)
		return nil, err
	}

	if result.Valid() {
		log.Info("No errors found during validation")
		log.Debug("Exiting")

		return nil, nil
	}

	// Logs results and build slice of errors
	validationResultsLog := make(logrus.Fields)
	validationErrors := make([]*ValidationError, len(result.Errors()))
	for i, desc := range result.Errors() {
		validationResultsLog[desc.Field()] = desc.String()

		validationErrors[i] = &ValidationError{
			Field:       desc.Field(),
			Description: desc.Description(),
		}
	}
	log.WithFields(validationResultsLog).Error("Errors found during validation")
	log.Debug("Exiting")

	return validationErrors, nil
}
