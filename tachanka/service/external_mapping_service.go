package service

import (
	"code.justin.tv/commerce/tachanka/dynamo"
	"code.justin.tv/commerce/tachanka/models"
	"github.com/Sirupsen/logrus"
)

type SimpleExternalMappingService struct {
	externalMappingDao dynamo.ExternalMappingDao
}

func NewExternalMappingService(emDao dynamo.ExternalMappingDao) IExternalMappingService {
	return SimpleExternalMappingService{
		externalMappingDao: emDao,
	}
}

type IExternalMappingService interface {
	PutExternalMapping(parentId string, emType models.ExternalMappingTypeEnum, newValue string) (*models.ExternalMapping, error)
	GetExternalMappingByValue(value string, externalMappingType models.ExternalMappingTypeEnum) (*[]models.ExternalMapping, error)
}

func (s SimpleExternalMappingService) PutExternalMapping(parentId string, emType models.ExternalMappingTypeEnum, newValue string) (*models.ExternalMapping, error) {
	newMapping := &models.ExternalMapping{
		ParentId: parentId,
		Type:     emType,
		Value:    newValue,
	}
	err := s.externalMappingDao.Put(newMapping)
	if err != nil {
		return nil, err
	}
	return newMapping, nil
}

func (s SimpleExternalMappingService) GetExternalMappingByValue(value string, externalMappingType models.ExternalMappingTypeEnum) (*[]models.ExternalMapping, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function": "ExternalMappingService.GetExternalMappingByValue",
		"Value":    value,
	})

	// Fetch the key
	fetchedValues, err := s.externalMappingDao.GetByMappingValue(value, externalMappingType)
	if err != nil {
		log.Warn("Erorr fetching external mapping with value: ", err)
		return nil, err
	}
	return fetchedValues, nil
}
