package service

import (
	"code.justin.tv/commerce/tachanka/models"
	"encoding/json"
	"github.com/satori/go.uuid"
)

type IProductBackfillService interface {
	BackfillManagedProduct(dpp *DevPortalProduct, mpType models.ManagedProductType, adgVendorId string, adgDomainId string, adgProductId string, tuid string, adgDocument string) (models.ManagedProduct, error)
}

type SimpleProductBackfillService struct {
	externalMappingService  IExternalMappingService
	managedProductService   IManagedProductsService
	devPortalProductService IDevPortalProductService
	documentService         IDocumentService
	submissionService       ISubmissionService
}

func NewProductBackfillService(ems IExternalMappingService, mps IManagedProductsService, dpps IDevPortalProductService, ds IDocumentService, ss ISubmissionService) IProductBackfillService {
	return SimpleProductBackfillService{
		externalMappingService:  ems,
		managedProductService:   mps,
		devPortalProductService: dpps,
		documentService:         ds,
		submissionService:       ss,
	}
}

func (s SimpleProductBackfillService) BackfillManagedProduct(dpp *DevPortalProduct, mpType models.ManagedProductType, adgVendorId string, adgDomainId string, adgProductId string, tuid string, adgDocument string) (models.ManagedProduct, error) {
	mpId := uuid.NewV4().String()

	adgDocumentBytes := []byte(adgDocument)
	var adgDocumentData map[string]*json.RawMessage
	err := json.Unmarshal(adgDocumentBytes, &adgDocumentData)
	if err != nil {
		return nil, err
	}

	genericADGData := make(map[string]interface{})
	for k, v := range adgDocumentData {
		genericADGData[k] = v
	}

	stringADGData, err := json.Marshal(genericADGData)
	if err != nil {
		return nil, err
	}

	dpp.ADGVendorId = &adgVendorId
	dpp.ADGDomainId = &adgDomainId
	err = s.devPortalProductService.UpdateDevPortalProduct(dpp)
	if err != nil {
		return nil, err
	}

	// Create the product
	mp, err := s.managedProductService.CreateManagedProduct(dpp, mpId, mpType)
	if err != nil {
		return nil, err
	}

	// Create & populate the document
	document, err := s.documentService.CreateDocument(mpId)
	if err != nil {
		return nil, err
	}
	_, err = s.documentService.UpdateDocument(document.DocumentId, string(stringADGData))
	if err != nil {
		return nil, err
	}

	// Associate the document with the managed product
	mp.SetDraftDocumentId(document.DocumentId)
	if adgProductId != "" {
		mp.SetADGProductId(adgProductId)
	}
	err = s.managedProductService.UpdateManagedProduct(mp)
	if err != nil {
		return nil, err
	}

	// Create the submission
	_, err = s.submissionService.CreateSubmission(dpp, mp, tuid)
	if err != nil {
		return nil, err
	}

	return s.managedProductService.GetManagedProduct(mpId)
}
