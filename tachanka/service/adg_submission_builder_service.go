package service

import (
	"errors"

	"code.justin.tv/commerce/tachanka/models"
	"code.justin.tv/commerce/tachanka/models/adg"
	"github.com/Sirupsen/logrus"
)

type ADGSubmissionBuilder struct {
	managedProductService IManagedProductsService
}

func NewADGSubmissionBuilder(mps IManagedProductsService) SubmissionBuilder {
	return &ADGSubmissionBuilder{
		managedProductService: mps,
	}
}

type SubmissionBuilder interface {
	Build(dpp *DevPortalProduct, product models.ManagedProduct) (*adg.ADGSubmission, error)
}

func (s ADGSubmissionBuilder) Build(dpp *DevPortalProduct, product models.ManagedProduct) (*adg.ADGSubmission, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":            "ADGSubmissionBuilder.Build",
		"ManagedProduct ID":   product.GetId(),
		"DevPortalProduct ID": dpp.Id,
	})
	log.Debug("Entering")

	var docBuilder func(*DevPortalProduct, models.ManagedProduct) (*adg.ADGSubmission, error)

	switch product.GetType() {
	case models.ModelType_Game:
		docBuilder = s.buildGameSubmission
	case models.ModelType_Extension:
		docBuilder = s.buildExtensionSubmission
	case models.ModelType_ExtensionInAppPurchase:
		docBuilder = s.buildExtensionIAPSubmission
	case models.ModelType_InGameContent:
		docBuilder = s.buildGameIGCSubmission
	default:
		log.WithField("Type", product.GetType()).Warn("Unsupported type sent for ADG Submission")
		return nil, errors.New("Unknown type for building a document")
	}

	log.Debug("Exiting")
	return docBuilder(dpp, product)
}

func (s ADGSubmissionBuilder) addTypeAgnosticFields(submission *adg.ADGSubmission, dpp *DevPortalProduct, managedProduct models.ManagedProduct) (*adg.ADGSubmission, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":          "ADGDocumentBuilder.addTypeAgnosticFields",
		"ManagedProduct ID": managedProduct.GetId(),
	})
	log.WithFields(logrus.Fields{
		"DevPortalProduct": *dpp,
		"ManagedProduct":   managedProduct,
	}).Debug("Entering")

	adgProductMetadata, err := s.getADGMetadataFromManagedProduct(managedProduct, *dpp.ADGVendorId)
	if err != nil {
		log.Warn("Error fetching Metadata for managed product: ", err)
		return nil, err
	}

	// Vendor
	submission.Vendor.Id = *dpp.ADGVendorId

	// Product
	submission.Product.Id = managedProduct.GetADGProductId()
	submission.Product.Sku = adgProductMetadata.SKU
	submission.Product.ExternalProductId = submission.Product.Sku
	submission.Product.ProductLine = adgProductMetadata.ProductLine
	title, err := s.getProductTitleFromManagedProduct(managedProduct)
	if err != nil {
		log.Warn("Error fetching Title for managed product: ", err)
		return nil, err
	}
	submission.Product.Name = title

	// Options
	submission.Options.AutoSubmit = true

	// Document - TaxCategories
	submission.Document.TaxCategories.ProductCategory = adgProductMetadata.ProductCategory.ToString()
	submission.Document.TaxCategories.ProductSubCategory = adgProductMetadata.Subcategory.ToString()

	log.Debug("Exiting")
	return submission, nil
}

func (s ADGSubmissionBuilder) buildGameSubmission(dpp *DevPortalProduct, product models.ManagedProduct) (*adg.ADGSubmission, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":          "ADGDocumentBuilder.buildGameSubmission",
		"ManagedProduct ID": product.GetId(),
	})
	log.Debug("Entering")

	game, ok := product.(*models.Game)
	if !ok {
		log.Warn("Error casting Managed Product to Game.")
		return nil, errors.New("Managed Product claimed to be a Game but failed to cast!")
	}

	submission := adg.NewGameSubmission()
	submission, err := s.addTypeAgnosticFields(submission, dpp, product)
	if err != nil {
		log.Warn("Error adding type agnostic fields ", err)
		return nil, err
	}

	submission, err = game.PopulateADGSubmission(*submission)
	if err != nil {
		log.Warn("Error populating submission for game", err)
		return nil, err
	}

	log.WithField("Submission", *submission).Debug("Exiting")
	return submission, nil
}

func (s ADGSubmissionBuilder) buildGameIGCSubmission(dpp *DevPortalProduct, product models.ManagedProduct) (*adg.ADGSubmission, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":          "ADGSubmissionBuilder.buildGameIGCSubmission",
		"ManagedProduct ID": product.GetId(),
	})
	log.Debug("Entering")

	igc, ok := product.(*models.InGameContent)
	if !ok {
		log.Warn("Error casting Managed Product to InGameContent.")
		return nil, errors.New("Managed Product claimed to be an In Game Content but failed to cast!")
	}

	// IGC must always be nested under a parent submission
	parent, err := s.findSellableParent(dpp, models.ModelType_Game)
	if err != nil {
		log.Warn("Error finding parent product")
		return nil, err
	}

	submission := adg.NewIGCSubmission()
	submission, err = s.addTypeAgnosticFields(submission, dpp, product)
	if err != nil {
		log.Warn("Error adding type agnostic fields ", err)
		return nil, err
	}

	// Populate parent portions of the submission. Note - some of these will get overwritten by the IGC population
	submission, err = parent.PopulateADGSubmission(*submission)
	if err != nil {
		log.Warn("Error populating submission from parent game", err)
		return nil, err
	}

	// Fill in IGC specific fields
	submission, err = igc.PopulateADGSubmission(*submission)
	if err != nil {
		log.Warn("Error populating submission for in game content", err)
		return nil, err
	}

	// Relate to parent
	parentADGProductId := parent.GetADGProductId()
	if len(parentADGProductId) == 0 {
		log.WithField("Parent Game MangedProduct Id", parent.GetId()).Warn("Unable to create a submission for IGC when the parent doesn't have an ADG Product Id")
		return nil, errors.New("Parent game doesn't have an ADG Product Id")
	}
	submission.Document.Relationships = &adg.Relationships{
		Parent: adg.Parent{
			ADGProductId: parentADGProductId,
		},
	}

	log.WithField("Submission", *submission).Debug("Exiting")
	return submission, nil

}

func (s ADGSubmissionBuilder) buildExtensionSubmission(dpp *DevPortalProduct, product models.ManagedProduct) (*adg.ADGSubmission, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":          "ADGSubmissionBuilder.buildExtensionSubmission",
		"ManagedProduct ID": product.GetId(),
	})
	log.Debug("Entering")

	extension, ok := product.(*models.Extension)
	if !ok {
		log.Warn("Error casting Managed Product to Extension.")
		return nil, errors.New("Managed Product claimed to be an extension but failed to cast!")
	}

	submission := adg.NewExtensionSubmission()
	submission, err := s.addTypeAgnosticFields(submission, dpp, product)
	if err != nil {
		log.Warn("Error adding type agnostic fields ", err)
		return nil, err
	}

	submission, err = extension.PopulateADGSubmission(*submission)
	if err != nil {
		log.Warn("Error populating submission for extension", err)
		return nil, err
	}

	log.WithField("Submission", *submission).Debug("Exiting")
	return submission, nil
}

func (s ADGSubmissionBuilder) buildExtensionIAPSubmission(dpp *DevPortalProduct, product models.ManagedProduct) (*adg.ADGSubmission, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":          "ADGSubmissionBuilder.buildExtensionIAPSubmission",
		"ManagedProduct ID": product.GetId(),
	})
	log.Debug("Entering")

	iap, ok := product.(*models.ExtensionIAP)
	if !ok {
		log.Warn("Error casting Managed Product to Extension IAP.")
		return nil, errors.New("Managed Product claimed to be an extension iap but failed to cast!")
	}

	parent, err := s.findSellableParent(dpp, models.ModelType_Extension)
	if err != nil {
		log.Warn("Error finding parent product")
		return nil, err
	}

	submission := adg.NewExtensionIAPSubmission()
	submission, err = s.addTypeAgnosticFields(submission, dpp, product)
	if err != nil {
		log.Warn("Error adding type agnostic fields ", err)
		return nil, err
	}

	// Populate parent portions of the submission. Note - some of these will get overwritten by the IGC population
	submission, err = parent.PopulateADGSubmission(*submission)
	if err != nil {
		log.Warn("Error populating submission from parent extension", err)
		return nil, err
	}

	// Fill in IAP specific fields
	submission, err = iap.PopulateADGSubmission(*submission)
	if err != nil {
		log.Warn("Error populating submission for extension iap", err)
		return nil, err
	}

	// Relate to parent
	parentADGProductId := parent.GetADGProductId()
	if len(parentADGProductId) == 0 {
		log.WithField("Parent Extension MangedProduct Id", parent.GetId()).Warn("Unable to create a submission for IAP when the parent doesn't have an ADG Product Id")
		return nil, errors.New("Parent extension doesn't have an ADG Product Id")
	}
	submission.Document.Relationships = &adg.Relationships{
		Parent: adg.Parent{
			ADGProductId: parentADGProductId,
		},
	}

	log.WithField("Submission", *submission).Debug("Exiting")
	return submission, nil
}

func (s ADGSubmissionBuilder) findSellableParent(dpp *DevPortalProduct, productType models.ManagedProductType) (adg.Sellable, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":            "ADGSubmissionBuilder.buildExtensionADGDocument",
		"DevPortalProduct ID": dpp.Id,
		"Type":                productType,
	})
	log.Debug("Entering")

	mps, err := s.managedProductService.GetManagedProducts(dpp)
	if err != nil {
		log.WithError(err).Warn("Error fetching ManagedProducts while looking for parent")
		return nil, err
	}

	var possibleParents []models.ManagedProduct
	for _, mp := range mps {
		if mp.GetType() == productType {
			possibleParents = append(possibleParents, mp)
		}
	}
	if len(possibleParents) != 1 {
		log.WithField("Number of possible parents found", len(possibleParents)).Warn("Either too many, or too few parent products.")
		return nil, errors.New("Invalid number of parent products found")
	}

	parent := possibleParents[0]
	sellableParent, ok := parent.(adg.Sellable)
	if !ok {
		log.WithField("Unsellable Product Id", parent.GetId()).Warn("Parent product not sellable!")
		return nil, errors.New("Parent product not sellable")
	}

	log.Debug("Exiting")
	return sellableParent, nil
}

func (s ADGSubmissionBuilder) isFulfilledByTwitchFromVendor(vendorId string) bool {
	var fulfilledByTwitch bool
	//FIXME: THIS IS HARD CODED DATA TO DETERMINE TAX AND LIABILITY JUNK. :feelsreidr:
	if vendorId == "UBIWD" || vendorId == "XTA5P" {
		fulfilledByTwitch = false
	} else {
		fulfilledByTwitch = true
	}
	return fulfilledByTwitch

}

func (s ADGSubmissionBuilder) getProductTitleFromManagedProduct(mp models.ManagedProduct) (string, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":           "ADGSubmissionBuilder.getProductTitleFromManagedProduct",
		"Managed Product Id": mp.GetId(),
	})
	log.Debug("Entering")

	sellableProduct, ok := mp.(adg.Sellable)
	if !ok {
		log.Warn("Trying to create ADG Metadata for a type that doesn't implement ADGSellable")
		return "", errors.New("Type is not sellable via ADG")
	}

	title := sellableProduct.GetTitle()
	if title == "" {
		log.Warn("No valid title found for managed product!")
		return "", errors.New("No title found for managed product in default locale")
	}

	log.Debug("Exiting")
	return title, nil
}

func (s ADGSubmissionBuilder) getADGMetadataFromManagedProduct(mp models.ManagedProduct, vendorId string) (*ADGProductMetadata, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function":           "ADGSubmissionBuilder.getADGMetadataFromManagedProduct",
		"Vendor Id":          vendorId,
		"Managed Product Id": mp.GetId(),
	})
	log.Debug("Entering")
	sellableProduct, ok := mp.(adg.Sellable)
	if !ok {
		log.Warn("Trying to create ADG Metadata for a type that doesn't implement ADGSellable")
		return nil, errors.New("Type is not sellable via ADG")
	}

	sku := sellableProduct.GetSKU()
	adgMetadata := sellableProduct.GetADGMetadata(s.isFulfilledByTwitchFromVendor(vendorId))

	log.Debug("Exiting")
	return &ADGProductMetadata{
		adgMetadata,
		sku,
	}, nil
}
