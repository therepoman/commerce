package service_test

import (
	"encoding/json"
	"math"
	"testing"
	"time"

	"code.justin.tv/commerce/tachanka/models"
	"code.justin.tv/commerce/tachanka/service"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	ManagedProductId     string = "3c98031d-a5fc-455b-b67c-f1c5562fe5aa"
	DevPortProductId     string = "e9547ec7-ed5d-4482-9851-f7a4e8555108"
	DocumentId           string = "2a326089-b9f6-468c-9847-c3377358c24a"
	StaticTimeUnixTime   int64  = 946684800
	ADGDomainId          string = "TestADGDomainID"
	ADGVendorId          string = "TestADGVendorID"
	ADGProductId         string = "TestADGProductID"
	SupportedControllers string = "TestSupportedControllers"

	TestSubmissionStep_Id          string                      = "TestSubmissionStepId"
	TestSubmissionStep_Status      models.SubmissionStepStatus = models.SubmissionStepStatus_Complete
	TestSubmissionStep_Type        models.SubmissionStepType   = models.SubmissionStepType_AdgSubmit
	TestSubmissionStep_CreatedDate string                      = "2010-01-02T15:04:05Z"

	TestSubmission_Id               string                  = "TestSubmissionId"
	TestSubmission_ManagedProductId string                  = "TestSubmissionManagedProductId"
	TestSubmission_DocumentId       string                  = "TestSubmissionDocumentId"
	TestSubmission_Status           models.SubmissionStatus = models.SubmissionStatus_Complete
	TestSubmission_CreatedDate      string                  = "2006-01-02T15:04:05Z"
	TestSubmission_LastUpdated      string                  = "2018-01-02T15:04:05Z"
)

func SetupTests() service.ITwirpTranslator {
	return service.NewTwirpTranslator()
}

func TestTwirpTranslator(t *testing.T) {
	twirpTranslator := SetupTests()

	Convey("Given a valid Managed Product, and valid Dev Portal Product", t, func() {
		managedProduct := newValidManagedProduct()
		devPortalProduct := newValidDevPortalProduct()

		Convey("When we translate that Managed Product", func() {

			translatedManagedProduct, err := twirpTranslator.TranslateManagedProduct(devPortalProduct, managedProduct)
			So(err, ShouldBeNil)

			Convey("The TWIRP Managed Product should match the service Managed Product", func() {
				So(translatedManagedProduct.Id, ShouldEqual, managedProduct.GetId())
				So(translatedManagedProduct.Document.Id, ShouldEqual, managedProduct.GetDraftDocumentId())

				convertedEnum, err := twirpTranslator.ConvertManagedProductTypeToTwirp(managedProduct.GetType())
				So(err, ShouldBeNil)
				So(translatedManagedProduct.Type, ShouldEqual, convertedEnum)
			})

			Convey("The TWIRP DocumentData should match the Game", func() {

				// So(twirpDoc.Id, ShouldEqual, dynamoDoc.DocumentId)
				// So(twirpDoc.ManagedProductId, ShouldEqual, dynamoDoc.ManagedProductId)

				// In order to better compare elements, convert the DocumentData to a map for comparisons
				var documentDataMap map[string]interface{}
				err = json.Unmarshal([]byte(translatedManagedProduct.Document.DocumentData), &documentDataMap)
				So(err, ShouldBeNil)
				So(documentDataMap, ShouldNotBeNil)
				So(documentDataMap, ShouldNotBeEmpty)
				So(documentDataMap["adgVendorId"], ShouldEqual, *devPortalProduct.ADGVendorId)
				So(documentDataMap["adgProductId"], ShouldEqual, managedProduct.GetADGProductId())
				So(documentDataMap["adgDomainId"], ShouldEqual, *devPortalProduct.ADGDomainId)

				// Check if game fields are present
				game := managedProduct.(*models.Game)
				So(documentDataMap["supportedControllers"], ShouldEqual, game.SupportedControllers)

				// We can run into weird issues were the expected and actual are a few milliseconds apart
				// Account for this
				lastUpdatedTimeExpected := float64(managedProduct.GetLastUpdated().Unix())
				documentMapLastUpdated := documentDataMap["lastUpdated"].(float64)
				timeDifference := math.Abs(lastUpdatedTimeExpected - documentMapLastUpdated)
				allowedDifference := timeDifference < 10
				So(allowedDifference, ShouldBeTrue)
			})
		})
	})

	Convey("Given a valid submission", t, func() {
		submission := newValidTwirpTestSubmission()

		Convey("When we translate it", func() {
			twirpSubmission, err := twirpTranslator.ConvertSubmissionToTwirp(submission)

			Convey("the TWIRP submission should match the model one", func() {
				So(err, ShouldBeNil)
				So(twirpSubmission.Id, ShouldEqual, submission.Id)
				So(twirpSubmission.Status, ShouldEqual, submission.Status)
				So(twirpSubmission.LastUpdated, ShouldEqual, TestSubmission_LastUpdated)
				So(twirpSubmission.CreatedDate, ShouldEqual, TestSubmission_CreatedDate)
				So(len(twirpSubmission.SubmissionSteps), ShouldEqual, len(submission.SubmissionSteps))
			})

			Convey("the TWIRP submission step should match the model one", func() {
				twirpSubmissionStep := twirpSubmission.SubmissionSteps[0]
				modelSubmissionStep := submission.SubmissionSteps[0]

				So(twirpSubmissionStep.Id, ShouldEqual, modelSubmissionStep.Id)
				So(twirpSubmissionStep.Status, ShouldEqual, modelSubmissionStep.Status)
				So(twirpSubmissionStep.Type, ShouldEqual, modelSubmissionStep.Type)
				So(twirpSubmissionStep.CreatedDate, ShouldEqual, TestSubmissionStep_CreatedDate)
			})

			So(twirpSubmission.Status, ShouldEqual, submission.Status)
		})
	})
}

func newValidTwirpTestSubmission() *models.Submission {
	parsedCreatedDate, _ := time.Parse(time.RFC3339, TestSubmission_CreatedDate)
	parsedLastUpdated, _ := time.Parse(time.RFC3339, TestSubmission_LastUpdated)

	firstSubmissionStep := newValidTwirpTestSubmissionStep()
	secondSubmissionStep := newValidTwirpTestSubmissionStep()

	return &models.Submission{
		Id:               TestSubmission_Id,
		DocumentId:       TestSubmission_DocumentId,
		ManagedProductId: TestSubmission_ManagedProductId,
		Status:           TestSubmission_Status,
		SubmissionSteps:  []models.SubmissionStep{*firstSubmissionStep, *secondSubmissionStep},
		LastUpdated:      parsedLastUpdated,
		CreatedDate:      parsedCreatedDate,
	}
}

func newValidTwirpTestSubmissionStep() *models.SubmissionStep {
	parsedTime, _ := time.Parse(time.RFC3339, TestSubmissionStep_CreatedDate)

	return &models.SubmissionStep{
		Id:          TestSubmissionStep_Id,
		Status:      TestSubmissionStep_Status,
		Type:        TestSubmissionStep_Type,
		CreatedDate: parsedTime,
	}
}

func newValidDevPortalProduct() *service.DevPortalProduct {
	// Use to get the pointers
	tempADGDomainId := ADGDomainId
	tempADGVendorId := ADGVendorId

	return &service.DevPortalProduct{
		Id:          DevPortProductId,
		ADGDomainId: &tempADGDomainId,
		ADGVendorId: &tempADGVendorId,
	}
}

func newValidManagedProduct() models.ManagedProduct {
	tempProductId := ADGProductId

	metaData := models.ManagedProductMetadata{
		Id:              ManagedProductId,
		ADGProductId:    &tempProductId,
		DraftDocumentId: DocumentId,
		LastUpdated:     models.ReadOnlyTime{Time: time.Unix(StaticTimeUnixTime, 0)},
	}

	// TODO - Add more tests for things besides games
	managedProduct := models.NewGame(metaData)
	managedProduct.SupportedControllers = SupportedControllers // Rando field to test

	return managedProduct
}
