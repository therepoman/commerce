package service_test

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	adgClient "code.justin.tv/commerce/tachanka/client/adg"
	"code.justin.tv/commerce/tachanka/dynamo"
	"code.justin.tv/commerce/tachanka/mocks"
	"code.justin.tv/commerce/tachanka/models"
	"code.justin.tv/commerce/tachanka/models/adg"
	"code.justin.tv/commerce/tachanka/service"
)

const (
	TestDocumentId       string = "testDocumentId"
	TestManagedProductId string = "testManagedProductId"
	TestVendorId         string = "testVendorId"
	TestDomainId         string = "testDomainId"
	TestSubmissionId     string = "testSubmissionId"
	TestTuid             string = "1234567"
	TestADGProductId     string = "ADG123"
)

var (
	TestParentADGProductId string = "ParentADG123"
)

func newValidSubmission() *models.Submission {
	return &models.Submission{
		Id:               TestSubmissionId,
		ManagedProductId: TestManagedProductId,
		DocumentId:       TestDocumentId,
	}
}

func newValidMapping() *models.ExternalMapping {
	return &models.ExternalMapping{
		ParentId: TestSubmissionId,
		Type:     models.EM_ADGProductId,
		Value:    "BLAH",
	}
}

func newValidADGProductMetadata() *service.ADGProductMetadata {
	return &service.ADGProductMetadata{
		Metadata: adg.Metadata{
			ContentType:     adg.ContentType_Game,
			ProductLine:     adg.ProductLine_Entitlement,
			ProductCategory: adg.ProductCategory_1000,
			Subcategory:     adg.Subcategory_FulfilledBy3P,
			ProductType:     adg.ProductType_Entitlement,
		},
		SKU: "test SKU",
	}
}

func newValidGameDocument() *dynamo.Document {
	// Intentionally don't give this data, since this is passed in through the MP
	document := &dynamo.Document{
		DocumentId:       TestDocumentId,
		DocumentVersion:  "v1.0",
		ManagedProductId: TestManagedProductId,
	}
	return document
}

func TestSimpleSubmissionService(t *testing.T) {
	Convey("Given a new SimpleSubmisisonService", t, func() {
		managedProductService := new(mocks.IManagedProductsService)
		documentDao := new(mocks.IDocumentDao)
		submissionDao := new(mocks.SubmissionDao)
		adgSubmissionServiceClient := new(mocks.ADGSubmissionServiceClient)
		submissionBuilder := service.NewADGSubmissionBuilder(managedProductService)

		submissionService := service.NewSubmissionService(managedProductService, documentDao, submissionDao, adgSubmissionServiceClient, submissionBuilder)

		Convey("With several submissions for the same managed product", func() {
			submissionDao.On("GetByManagedProduct", TestManagedProductId).Return([]models.Submission{*newValidSubmission(), *newValidSubmission()}, nil).Once()

			Convey("When we query by managed product id, we should get back submissions for that id", func() {
				result, err := submissionService.GetSubmissionsByManagedProduct(TestManagedProductId)
				So(err, ShouldBeNil)
				So(result, ShouldNotBeEmpty)
				So(result, ShouldHaveLength, 2)
			})
		})
		Convey("When we submit a valid product", func() {
			managedProductService.On("GetADGMetadataFromManagedProduct", mock.Anything, mock.Anything).Return(newValidADGProductMetadata(), nil).Once()
			managedProductService.On("GetProductTitleFromManagedProduct", mock.Anything).Return("TestTitle", nil).Once()
			managedProductService.On("GetFuelStoreAvailabilityFromManagedProduct", mock.Anything).Return("TestFuelStoreAvailability", nil).Once()
			managedProductService.On("UpdateManagedProduct", mock.Anything).Return(nil).Once()

			documentDao.On("Put", mock.AnythingOfType("*dynamo.Document")).Return(nil).Once()
			document := newValidGameDocument()
			documentDao.On("Get", mock.Anything).Return(document, nil).Once()

			validSubmission := newValidSubmission()
			submissionDao.On("Put", mock.Anything).Return(nil)
			submissionDao.On("AddStep", mock.Anything, mock.Anything).Return(validSubmission, nil)

			domain := TestDomainId
			vendor := TestVendorId
			mockDPP := &service.DevPortalProduct{
				Id:                TestDPPId,
				ADGDomainId:       &domain,
				ADGVendorId:       &vendor,
				ManagedProductIds: []string{TestManagedProductId},
			}

			response := adgClient.ADGSubmissionServiceResponse{}
			response.Product.Id = TestADGProductId

			adgSubmissionServiceClient.On("CreateProduct", mock.Anything).Return(response, nil).Once()
			adgSubmissionServiceClient.On("CreateRevision", mock.Anything).Return(response, nil).Once()

			Convey("That is a game", func() {
				// Create a valid game
				mockMPGame := &models.Game{
					ManagedProductMetadata: models.ManagedProductMetadata{
						Id:              TestManagedProductId,
						DraftDocumentId: TestDocumentId,
						ADGProductId:    nil,
					},
				}

				documentData := NewGameDocumentString()
				mockMPGame.SetData(documentData)

				Convey("Without previous submissions", func() {
					mockMPGame.SetLiveSubmissionId("")
					submission, err := submissionService.SubmitManagedProduct(mockDPP, mockMPGame, TestTuid)

					Convey("We should succeed, and get an ADG Product Id back", func() {
						So(submission, ShouldNotBeNil)
						So(err, ShouldBeNil)
						So(mockMPGame.ADGProductId, ShouldNotBeNil)
					})
				})

				Convey("With previous submission(s)", func() {
					mockMPGame.SetLiveSubmissionId(TestSubmissionId)
					previousSubmission := newValidSubmission()

					Convey("in progress", func() {
						previousSubmission.Status = models.SubmissionStatus_InProgress
						submissionDao.On("Get", TestSubmissionId).Return(previousSubmission, nil).Once()

						submission, err := submissionService.SubmitManagedProduct(mockDPP, mockMPGame, TestTuid)

						Convey("We should fail and get an error back", func() {
							So(submission, ShouldBeNil)
							So(err, ShouldNotBeNil)
							So(mockMPGame.ADGProductId, ShouldBeNil)
						})
					})

					Convey("failed", func() {
						previousSubmission.Status = models.SubmissionStatus_Failed
						submissionDao.On("Get", TestSubmissionId).Return(previousSubmission, nil).Once()

						submission, err := submissionService.SubmitManagedProduct(mockDPP, mockMPGame, TestTuid)

						Convey("We should succeed, and get an ADG Product Id back", func() {
							So(submission, ShouldNotBeNil)
							So(err, ShouldBeNil)
							So(mockMPGame.ADGProductId, ShouldNotBeNil)
						})
					})

					Convey("complete", func() {
						previousSubmission.Status = models.SubmissionStatus_Failed
						submissionDao.On("Get", TestSubmissionId).Return(previousSubmission, nil).Once()

						submission, err := submissionService.SubmitManagedProduct(mockDPP, mockMPGame, TestTuid)

						Convey("We should succeed, and get an ADG Product Id back", func() {
							So(submission, ShouldNotBeNil)
							So(err, ShouldBeNil)
							So(mockMPGame.ADGProductId, ShouldNotBeNil)
						})
					})
				})
			})

			Convey("That is an extension", func() {
				// Create a valid extension
				subId := TestSubmissionId
				mockMPExt := &models.Extension{
					ManagedProductMetadata: models.ManagedProductMetadata{
						Id:               TestManagedProductId,
						DraftDocumentId:  TestDocumentId,
						LiveSubmissionId: &subId,
						ADGProductId:     nil,
					},
				}
				documentData := NewExtDocumentString()
				mockMPExt.SetData(documentData)

				submissionDao.On("Get", TestSubmissionId).Return(validSubmission, nil).Once()

				submission, err := submissionService.SubmitManagedProduct(mockDPP, mockMPExt, TestTuid)
				Convey("We should succeed, and get an ADG Product Id back", func() {
					So(submission, ShouldNotBeNil)
					So(err, ShouldBeNil)
					So(mockMPExt.ADGProductId, ShouldNotBeNil)
				})
			})

			Convey("That is an extension iap", func() {
				// Create a valid extension
				subId := TestSubmissionId
				mockMPExt := &models.Extension{
					ManagedProductMetadata: models.ManagedProductMetadata{
						Id:               TestManagedProductId,
						DraftDocumentId:  TestDocumentId,
						LiveSubmissionId: &subId,
						ADGProductId:     &TestParentADGProductId,
					},
				}
				documentData := NewExtDocumentString()
				mockMPExt.SetData(documentData)

				managedProductService.On("GetManagedProducts", mock.Anything).Return([]models.ManagedProduct{mockMPExt}, nil).Once()
				submissionDao.On("Get", TestSubmissionId).Return(validSubmission, nil).Once()

				// Create a valid extension iap
				mockMPExtIAP := &models.ExtensionIAP{
					ManagedProductMetadata: models.ManagedProductMetadata{
						Id:               TestManagedProductId,
						DraftDocumentId:  TestDocumentId,
						LiveSubmissionId: &subId,
						ADGProductId:     nil,
					},
				}
				documentData = NewExtIAPDocumentString()
				mockMPExtIAP.SetData(documentData)
				submission, err := submissionService.SubmitManagedProduct(mockDPP, mockMPExtIAP, TestTuid)

				Convey("We should succeed, and get an ADG Product Id back", func() {
					So(submission, ShouldNotBeNil)
					So(err, ShouldBeNil)
					So(mockMPExtIAP.ADGProductId, ShouldNotBeNil)
				})
			})

		})
	})
}
