package utilities

import (
	"errors"

	"github.com/Sirupsen/logrus"
)

// Given a map of strings to interfaces and a key, returns the value of the key as a string
// An error will be returned if the key doesn't exist, or the value isn't a string.
func GetAndRemoveStringFromMap(key string, theMap map[string]interface{}) (string, error) {
	log := logrus.WithFields(logrus.Fields{
		"Function": "getAndRemoveStringFromMap",
		"Key":      key,
		"Map":      theMap,
	})
	log.Debug("Entering")

	value, ok := theMap[key]
	if !ok {
		log.Error(key + " does not exist")
		return "", errors.New(key + " cannot be empty.")
	}
	delete(theMap, key)

	valueAsString, ok := value.(string)
	if !ok {
		log.Error(key + " is not a string")
		return "", errors.New(key + " must be a string.")
	}

	log.WithField("Value", valueAsString).Debug("Exiting")

	return valueAsString, nil
}
