package slog

import (
	"code.justin.tv/commerce/payday/Godeps/_workspace/src/github.com/stretchr/testify/assert"
	"testing"
	"strings"
	"bytes"
)

func TestDebug(t *testing.T) {
	var b bytes.Buffer
	setOutput(&b)

	Debug("TestA", "TestB")

	out := b.String()
	assert.True(t, strings.HasPrefix(out, "[DEBUG]"))
	assert.True(t, strings.HasSuffix(out, "TestATestB\n"))
}

func TestDebugln(t *testing.T) {
	var b bytes.Buffer
	setOutput(&b)

	Debugln("TestA", "TestB")

	out := b.String()
	assert.True(t, strings.HasPrefix(out, "[DEBUG]"))
	assert.True(t, strings.HasSuffix(out, "TestA TestB\n"))
}

func TestDebugf(t *testing.T) {
	var b bytes.Buffer
	setOutput(&b)

	Debugf("Test%d", 123)

	out := b.String()
	assert.True(t, strings.HasPrefix(out, "[DEBUG]"))
	assert.True(t, strings.HasSuffix(out, "Test123\n"))
}


func TestInfo(t *testing.T) {
	var b bytes.Buffer
	setOutput(&b)

	Info("TestA", "TestB")

	out := b.String()
	assert.True(t, strings.HasPrefix(out, "[INFO]"))
	assert.True(t, strings.HasSuffix(out, "TestATestB\n"))
}

func TestInfoln(t *testing.T) {
	var b bytes.Buffer
	setOutput(&b)

	Infoln("TestA", "TestB")

	out := b.String()
	assert.True(t, strings.HasPrefix(out, "[INFO]"))
	assert.True(t, strings.HasSuffix(out, "TestA TestB\n"))
}

func TestInfof(t *testing.T) {
	var b bytes.Buffer
	setOutput(&b)

	Infof("Test%d", 123)

	out := b.String()
	assert.True(t, strings.HasPrefix(out, "[INFO]"))
	assert.True(t, strings.HasSuffix(out, "Test123\n"))
}


func TestWarn(t *testing.T) {
	var b bytes.Buffer
	setOutput(&b)

	Warn("TestA", "TestB")

	out := b.String()
	assert.True(t, strings.HasPrefix(out, "[WARN]"))
	assert.True(t, strings.HasSuffix(out, "TestATestB\n"))
}

func TestWarnln(t *testing.T) {
	var b bytes.Buffer
	setOutput(&b)

	Warnln("TestA", "TestB")

	out := b.String()
	assert.True(t, strings.HasPrefix(out, "[WARN]"))
	assert.True(t, strings.HasSuffix(out, "TestA TestB\n"))
}

func TestWarnf(t *testing.T) {
	var b bytes.Buffer
	setOutput(&b)

	Warnf("Test%d", 123)

	out := b.String()
	assert.True(t, strings.HasPrefix(out, "[WARN]"))
	assert.True(t, strings.HasSuffix(out, "Test123\n"))
}


func TestError(t *testing.T) {
	var b bytes.Buffer
	setOutput(&b)

	Error("TestA", "TestB")

	out := b.String()
	assert.True(t, strings.HasPrefix(out, "[ERROR]"))
	assert.True(t, strings.HasSuffix(out, "TestATestB\n"))
}

func TestErrorln(t *testing.T) {
	var b bytes.Buffer
	setOutput(&b)

	Errorln("TestA", "TestB")

	out := b.String()
	assert.True(t, strings.HasPrefix(out, "[ERROR]"))
	assert.True(t, strings.HasSuffix(out, "TestA TestB\n"))
}

func TestErrorf(t *testing.T) {
	var b bytes.Buffer
	setOutput(&b)

	Errorf("Test%d", 123)

	out := b.String()
	assert.True(t, strings.HasPrefix(out, "[ERROR]"))
	assert.True(t, strings.HasSuffix(out, "Test123\n"))
}


func TestPanic(t *testing.T) {
	var b bytes.Buffer
	setOutput(&b)

	defer func() {
		if r := recover(); r != nil {
			out := b.String()

			assert.True(t, strings.HasPrefix(out, "[PANIC]"))
			assert.True(t, strings.HasSuffix(out, "TestATestB\n"))
		} else {
			t.Error("Expected a panic")
		}
	}()

	Panic("TestA", "TestB")
}

func TestPanicln(t *testing.T) {
	var b bytes.Buffer
	setOutput(&b)

	defer func() {
		if r := recover(); r != nil {
			out := b.String()

			assert.True(t, strings.HasPrefix(out, "[PANIC]"))
			assert.True(t, strings.HasSuffix(out, "TestA TestB\n"))
		} else {
			t.Error("Expected a panic")
		}
	}()

	Panicln("TestA", "TestB")
}

func TestPanicf(t *testing.T) {
	var b bytes.Buffer
	setOutput(&b)

	defer func() {
		if r := recover(); r != nil {
			out := b.String()

			assert.True(t, strings.HasPrefix(out, "[PANIC]"))
			assert.True(t, strings.HasSuffix(out, "Test123\n"))
		} else {
			t.Error("Expected a panic")
		}
	}()

	Panicf("Test%d", 123)
}


func TestFatal(t *testing.T) {
	setExitOnFatal(false)

	var b bytes.Buffer
	setOutput(&b)

	Fatal("TestA", "TestB")

	out := b.String()
	assert.True(t, strings.HasPrefix(out, "[FATAL]"))
	assert.True(t, strings.HasSuffix(out, "TestATestB\n"))
}

func TestFatalln(t *testing.T) {
	setExitOnFatal(false)

	var b bytes.Buffer
	setOutput(&b)

	Fatalln("TestA", "TestB")

	out := b.String()
	assert.True(t, strings.HasPrefix(out, "[FATAL]"))
	assert.True(t, strings.HasSuffix(out, "TestA TestB\n"))
}

func TestFatalf(t *testing.T) {
	setExitOnFatal(false)

	var b bytes.Buffer
	setOutput(&b)

	Fatalf("Test%d", 123)

	out := b.String()
	assert.True(t, strings.HasPrefix(out, "[FATAL]"))
	assert.True(t, strings.HasSuffix(out, "Test123\n"))
}