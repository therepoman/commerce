package slog

import (
	"log"
	"fmt"
	"os"
	"io"
)

type logLevel int

const (
	debugLevel logLevel = iota
	infoLevel
	warnLevel
	errorLevel
	panicLevel
	fatalLevel
)

const (
	debugPrefix = "[DEBUG] "
	infoPrefix = "[INFO] "
	warnPrefix = "[WARN] "
	errorPrefix = "[ERROR] "
	panicPrefix = "[PANIC] "
	fatalPrefix = "[FATAL] "
)

// Log date, time, and file name with line number
const defaultLoggerFlags = log.Ldate | log.Ltime | log.Lmicroseconds | log.Lshortfile

var logger = log.New(os.Stderr, getPrefix(infoLevel), defaultLoggerFlags)

var exitOnFatal = true

func init()  {
	// Setup the standard log which may be used by dependencies
	log.SetFlags(defaultLoggerFlags)
	log.SetPrefix(getPrefix(infoLevel))
}

// Output to a custom writer for testing
func setOutput(writer io.Writer)  {
	logger.SetOutput(writer)
}

// Don't call os.Exit on fatals for testing
func setExitOnFatal(shouldExit bool)  {
	exitOnFatal = shouldExit
}




func getPrefix(level logLevel) string {
	switch level {
	case debugLevel:
		return debugPrefix
	case infoLevel:
		return infoPrefix
	case warnLevel:
		return warnPrefix
	case errorLevel:
		return errorPrefix
	case panicLevel:
		return panicPrefix
	case fatalLevel:
		return fatalPrefix
	default:
		return infoPrefix
	}
}

func logAtLevel(level logLevel, msg string)  {
	logger.SetPrefix(getPrefix(level))

	err := logger.Output(3, msg) // logs the relevant file and line number (3 levels back)
	if err != nil {
		log.SetPrefix(getPrefix(errorLevel))
		log.Printf("something went wrong with the payday custom logger %v", err)
		log.SetPrefix(getPrefix(infoLevel))
	}

	if level == panicLevel {
		panic(msg)
	} else if level == fatalLevel && exitOnFatal {
		os.Exit(1)
	}
}

func Debug(v ...interface{}) {
	logAtLevel(debugLevel, fmt.Sprint(v...))
}

func Debugln(v ...interface{}) {
	logAtLevel(debugLevel, fmt.Sprintln(v...))
}

func Debugf(format string, v ...interface{}) {
	logAtLevel(debugLevel, fmt.Sprintf(format, v...))
}

func Info(v ...interface{}) {
	logAtLevel(infoLevel, fmt.Sprint(v...))
}

func Infoln(v ...interface{}) {
	logAtLevel(infoLevel, fmt.Sprintln(v...))
}

func Infof(format string, v ...interface{}) {
	logAtLevel(infoLevel, fmt.Sprintf(format, v...))
}

func Warn(v ...interface{}) {
	logAtLevel(warnLevel, fmt.Sprint(v...))
}

func Warnln(v ...interface{}) {
	logAtLevel(warnLevel, fmt.Sprintln(v...))
}

func Warnf(format string, v ...interface{}) {
	logAtLevel(warnLevel, fmt.Sprintf(format, v...))
}

func Error(v ...interface{}) {
	logAtLevel(errorLevel, fmt.Sprint(v...))
}

func Errorln(v ...interface{}) {
	logAtLevel(errorLevel, fmt.Sprintln(v...))
}

func Errorf(format string, v ...interface{}) {
	logAtLevel(errorLevel, fmt.Sprintf(format, v...))
}

func Panic(v ...interface{}) {
	logAtLevel(panicLevel, fmt.Sprint(v...))
}

func Panicln(v ...interface{}) {
	logAtLevel(panicLevel, fmt.Sprintln(v...))
}

func Panicf(format string, v ...interface{}) {
	logAtLevel(panicLevel, fmt.Sprintf(format, v...))
}

func Fatal(v ...interface{}) {
	logAtLevel(fatalLevel, fmt.Sprint(v...))
}

func Fatalln(v ...interface{}) {
	logAtLevel(fatalLevel, fmt.Sprintln(v...))
}

func Fatalf(format string, v ...interface{}) {
	logAtLevel(fatalLevel, fmt.Sprintf(format, v...))
}


