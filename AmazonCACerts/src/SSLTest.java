import java.io.*;
import java.net.*;
import javax.net.*;
import javax.net.ssl.*;


public class SSLTest {
    public static void main(String[] args) throws Exception {
    	
        String hostname;
        hostname = args[0];
        
        int sslPort = 443;
        String message = "HEAD / HTTP/1.0\n\n";
        if (args.length > 1) {
            sslPort = Integer.valueOf(args[1]);
            message = "\n";
        }
        
        System.out.println("Checking HOSTNAME : " + hostname);
        System.out.println("On Port : " + sslPort);
        
        SocketFactory socketFactory = SSLSocketFactory.getDefault();
        Socket socket = socketFactory.createSocket(hostname, sslPort);
        InputStream in = socket.getInputStream();
        OutputStream out = socket.getOutputStream();
        out.write(message.getBytes());
        byte[] response = new byte[2000];
        in.read(response);
        in.close();
        out.close();
        System.out.println(new String(response));
    }
}
