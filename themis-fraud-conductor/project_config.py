from conductor.config import (
    EnvironmentConfig,
    Project,
    AirflowConfig,
)

staging_airflow_config = AirflowConfig(mwaa_environment_name="commerce-ml-fraud-beta")
staging_env = EnvironmentConfig(
    account_id="911861225579",
    account_name="twitch-commerce-science-fraud-beta",
    default_region="us-west-2",
    airflow=staging_airflow_config,
)

project = Project(name="test-conductor", environments={"staging": staging_env})

production_airflow_config = AirflowConfig(mwaa_environment_name="commerce-ml-fraud-beta")
staging_env = EnvironmentConfig(
    account_id="911861225579",
    account_name="twitch-commerce-science-fraud-beta",
    default_region="us-west-2",
    airflow=staging_airflow_config,
)

project = Project(name="test-conductor-test-v2", environments={"staging": staging_env})
