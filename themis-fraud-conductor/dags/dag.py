import airflow
from project_config import project
from conductor.core import Conductor

default_args = {
    "start_date": airflow.utils.dates.days_ago(0)
}

c = Conductor(
    "my-dag",
    project=project,
    schedule_interval="@once",
    default_args=default_args,
)
dag = c.dag


def my_func(a, b):
    print(a,b)

c.operators.SageMakerProcessingOperator(
    task_id="call-my-function",
    entrypoint=my_func,
    a = "Hello",
    b = "World!",
)