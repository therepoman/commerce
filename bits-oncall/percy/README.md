# Percy

## DNS

Environment | URL
--- | ---
Production | main.us-west-2.prod.percy.twitch.a2z.com
Staging | main.us-west-2.beta.percy.twitch.a2z.com

## Deploying

Deploys for Percy are done via [clean-deploy](https://deploy.xarth.tv/#/commerce/percy) 

## Infrastructure Changes

Infrastructure is managed via Terraform. Terraform changes need to go through the proper PR process and then must be plan and applied to the respective environments. 

## Common Issues 
### How to rollback the Hype Train feature

There’s two ways to turn off the Hype Train feature: 
#### Option 1: Backfill whitelisted channels non-whitelisted. 
Run the cmd/backfill_whitelist/main.go script to backfill and set the whitelisted channel’s isWhitelisted field to false. 

This turns off Hype Train for all whitelisted channels. 

Run script via specifying the AWS_PROFILE, region, and environment: 

Example: 
AWS_PROFILE=twitch-percy-aws-devo go run cmd/backfill_whitelist/main.go -env=staging -region=us-west-2

#### Option 2:  Unsubscribe from Event bus
Go to the Event Bus Dashboard, search for the service named “Percy”. Click on “View Details” under the “Targets” column. Click on “Edit Subscriptions” for the eventbus-percy-production Target Name. Then turn off all or specific event bus events. 

Turning off all eventbus subscriptions would result in Hype Trains not being started. Turning off some would result in Hype Trains still potentially being able to be kicked off via the remaining event bus events. 

### How to whitelist a specific channel

If you need to whitelist a specific channel, run the cmd/backfill_whitelist/main.go script and specify the specific channel_id by modifying whitelist.json

Run the script via specifying the AWS_PROFILE, region, and env. 

Example: 
AWS_PROFILE=twitch-percy-aws-devo go run cmd/backfill_whitelist/main.go -env=staging -region=us-west-2

### How to fix Hype Train’s that are “stuck”

Hype train’s get “stuck” when a service such as pubsub or Destiny goes down. Percy also ends a Hype Train via kicking off a goroutine and ending the Hype Train via the backend. 

If, for some reason, you still need to end a Hype Train, then: 

Go Isengard & click into Percy’s production AWS console. 
Go to DynamoDB, click on the table for Hype Train’s (hype-train-production). 
Search for a Hype Train with the channel_id, the row with a “0” value as the Instance, is the most recent Hype Train. 
Verify that the Hype Train is still stuck by looking at the endedAt field. 
If required, manually force the Hype Train to end via updating the endedAt and endingReason fields. 

### What is the number of events & threshold for a Hype Train? 

The number of events and threshold for a Hype Train is stored within the AWS account for Percy. 

Go to Isenguard & search for the Percy account 
Go to DynamoDB
Select the “hype-trains-config-production” table 
Select “query” and search by the “channel_id”
If a row is returned, then the object’s “num_of_events_to_kickoff” and “min_points_to_count_towards_kickoff” are the two fields. 
If no result is returned, then the Hype Train is using the Global default values, which are 100 pts to kickoff threshold & 3 distinct events required to trigger a Hype Train. 

### Who participated in the Hype Train ? 

The participation info is stored via in our DynamoDB tables

First find the Hype Train row: 

Go to Isenguard & search for the Percy account 
Go to DynamoDB
Select the “hype-trains-production” table 
Search by the “channel_id”  for the channel. 
Instance with a value of “0” is the most recently executed Hype Train
Take a note fo the ID of the given Hype Train 

Search the Participants table by Hype Train’s ID

Go to Isenguard & search for the Percy account 
Go to DynamoDB
Select the “participants-production” table 
Search by the Hype Train’s ID found above
Search will return the list of participant’s for the Hype Train 

### Was a user entitled to an emote after the Hype Train’s completion? 

TODO 

## Explicit Purchase Celebrations

### StepFunction Failures

#### Manual refunds

If we fail to publish the celebration pubsub, we attempt to automatically refund the purchase order. In the event that the auto refund fails, engage the Money oncall in #payments (this can wait until business hours). They will need the `originId`, which you can find in the StepFunction input. This class of error should not be redriven.
  
#### Redriving

Celebrations use StepFunctions for both async workflows (purchase fulfillment and revocation). These StepFunctions are idempotent and can be redriven upon failure. For the purchase fulfillment SFN, only redrive if the failure happened after the pubsub step (see manual refund case above). 
