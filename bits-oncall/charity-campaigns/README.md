[Cloudwatch Charity Dashboard](https://us-west-2.console.aws.amazon.com/cloudwatch/home?region=us-west-2#dashboards:name=Charity)

# Configuring a Charity Campaign

### 1. Upload new assets for the Charity Cheermote
Note that any cheermote that has `isCharitable` set to `true` will automatically append `#charity` to the end of the given Bits message. Also note that when we are outside the time bounds of a given charity, we modify the current charity cheermote to be of `type: display_only` to preserve clips/vods.

### 2. Upload a new `production-charity-tags` yaml file in s3
A charity campaign should look like this when you upload it:
```yaml
- name: "Direct Relief"
  hashtag: "#charity"
  learn_more: "https://link.twitch.tv/blizzardofbits"
  start_date: "2018-12-12T08:00:00Z"
  end_date: "2018-12-28T08:00:00Z"
```
Note that the `start_date` and `end_date` are in UTC. We started the charity on 12/12/2018 at 00:00:00 PT. Also note that there are optional fields like `image_url` (which allows for an image to show up somewhere on the front-end when the charity is active) and `description` (which allows a non-localized description to show up somewhere on the front-end when the charity is active). Eventually, we will add `maxTotalLimit`, which is the total number of Bits before the charity should auto-shut-off.

# Turning on a Charity Campaign

### 1. Wait until just before the `start_date` of the Charity event that was configured
Once you reach the `start_date`, the backend should automatically turn the charity on based on `tags/poller.go`, which has a cache refresh interval of ~5 minutes.

### 2. Turn the `bits_charity_event` dynamic setting to `"on"`
Once both this dynamic setting and the charity are on, the Bits card will be taken over by the special treatment we use for Charity.

# Turning off a Charity Campaign

Charity events on the backend will automatically turn off when the current date is after the configured `end_date` in the `production-charity-tags.yaml` file in s3. That being said, it's possible that you want to turn off the Charity early for some other reason.

### 1. Go to Payday s3 in Isengard

### 2. Click on [bits-supported-tags](https://s3.console.aws.amazon.com/s3/buckets/bits-supported-tags/?region=us-west-2&tab=overview)

### 3. Download the latest `production-charity-tags` yaml file.

### 4. Make your change locally (like deleting the currently active charity campaign)

### 5. Save your yaml file as `production-charity-tags`. NO EXTENSION!
If you save your file as `production-charity-tags.yaml`, s3 will let it upload, but it won't be read by Payday. You'll need to save your file as a yaml file with no extension in order for it to be correctly read by Payday from s3. If you need to make an empty `production-charity-tags` file, you can do so using the following terminal command:
```
echo "" > production-charity-tags
```

### 6. Upload your `production-charity-tags` file to s3.

### 7. Wait ~5 minutes for the charity poller in payday to update and turn the charity off

### 8. Turn off the `bits_charity_event` dynamic setting 
Go to [where we configure dynamic settings](https://savant.internal.justin.tv/web/settings?application=Twilight) and find on page `bits_charity_event`. Click Edit and set the `Production (default)` value to `"off"`. Read more about how [Twilight does dynamic settings here](https://git.xarth.tv/pages/twilight/twilight-docs/docs/guides/dynamic-settings.html?search=dynamic)

## FAQ

### Is there a place I can test Charity without it being live?

Yes. [qa_bits_partner](https://twitch.tv/qa_bits_partner) is configured on the backend to always have a charity running. You can override the `bits_charity_event` setting in Twilight using your staff account

### What happens if I change the `bits_charity_event` dyanamic setting?

If there is a currently-active charity campaign, then flipping the dynamic setting will either take-over or stop taking over the Bits card in Twilight.

### How do I see the total cheered so far?

Other than cheering with #charity in a channel yourself, if you are on the Twitch network, you can go [view the total here](https://prod.payday.twitch.a2z.com/api/charity)
