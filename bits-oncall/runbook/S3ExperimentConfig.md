# S3 Experiment Config
Some of our services use an experiment config json file in S3 to determine the dial up percentage of an experiment.
This config can also contain overrides for specific channels or users to activate this experiment for. 

An example config file can look like this: 
```
{
  "dial_up_percentage": 50,
  "user_whitelist": ["1232454678"],
  "channel_whitelist": ["987654321"]
}
```

## Editing the S3 config file
To edit the config in S3, you first have to find the right experiment. Ask your team mates or look in the service code to determine which experiment you need to edit.
There should be a single folder for the experiments in the service. For example, in payday the path might look like this:   
`s3://payday-experiment-config/bits-badge-selection/experiment_config.json`

To edit the config, you will have to:
1. download the json file, 
2. edit it in your favorite editor and 
3. upload it back to the same path (overwriting the existing config)   

## Activating an experiment for a specific user or channel
Note that this is an OR combination, there currently is now way to limit the experiment to a user AND channel.

### Activate the experiment for a user across all channels

To add a specific user to the override, you will have to add their user id to the `user_whitelist`. 
If you have a user with the id `56428`, editing the above file would look as follows:   
```
{
  "dial_up_percentage": 50,
  "user_whitelist": ["1232454678", "56428"],
  "channel_whitelist": ["987654321"]
}
```

### Activate the experiment for a specific channel
All users in this channel will experience the experiment's behavior.
To activate the experiment for a channel, you will need to add the channel id to the `channel_whitelist`.
If you have a channel with the id `6742`, editing the above file will look as follows:   
```
{
  "dial_up_percentage": 50,
  "user_whitelist": ["1232454678"],
  "channel_whitelist": ["987654321", "6742"]
}
```

## Dialing up the percentage for everyone
To dial up the percentage of how many users will experience the experiment's behavior, you will have to edit the `dial_up_percentage`.  
This value ranges from `0` for zero percent of users to `100` for one hundred percent of users. 
If you wanted to dial up to 100 percent, the edited config from above will look as follows:   
```
{
  "dial_up_percentage": 100,
  "user_whitelist": ["1232454678"],
  "channel_whitelist": ["987654321"]
}
```

At this point you could remove the overrides for users and channels, since the experiment is active for everyone. 

_Don't forget to remove experiments once you are done with them!_
