# Pitfalls / things to look out for

* Add the action to the actions table first, add them to the poller last!
* Blacklist and Whitelist are not used at the same time, it's either, or. If Blacklist is active EVERY CHANNEL on twitch will have the cheermote active, except for channels on the blacklist. If the Blacklist is not active, then only channels on the whitelist will have the cheermote. 
* Check all animated and static images show up correctly for light AND dark theme (in the bits card)
* Check the timestamps, they are in unix time format, in nanoseconds, which makes this more fragile and easy to get wrong. (one digit off can mean a few months difference)
* whitelist: Add channels to BOTH `sponsored_cheermote_channel_statuses` and `sponsored_cheermote_eligible_channels`

# Steps for adding sponsored cheermotes

*Always follow them in this order, or things will break and the oncall will get paged!*
*Yes, we need tooling*
1. Upload the brand image and the assets
1. Add the action to the `actions` table
1. Add the campaign to the `sponsored_cheermote_campaigns`
1. Add Channels to campaign
   1. EITHER add the ones you don't want to the blacklist
   1. OR add the ones you want to the two tables `sponsored_cheermote_channel_statuses` and `sponsored_cheermote_eligible_channels`.
1. Update the poller (in S3), add the campaign id to the `campaigns` file in `bits-sponsored-cheermote-campaigns`


# Adding a New Sponsored Cheermote

## Uploading Assets
In S3 in the bits-assets bucks you’ll need to add the brand image and images for the cheermotes.
* in `bits-assets/sponsored-cheermotes/brand-images` make a new folder with the campaign id and add the brand image in there as an 85x45 png. You will need to add this folder and png name to the dynamo record in a few steps.
* in `bits-assets/sponsored-actions` upload the assets for the cheermote using the standard folder structure we use for actions

## Dynamo
In Dynamo you’ll need to do 2 main things (with a 3rd for testing possible)
* In `{environment}_actions` add an entry in the following format in this example (make sure to click the DynamoDB json checkbox because we use giant date formats):
```json
{
  "canonicalCasing": {
    "S": "OldSpice"
  },
  "createdTime": {
    "N": "1527797920000000000"
  },
  "lastUpdated": {
    "N": "1527797920000000000"
  },
  "prefix": {
    "S": "oldspice"
  },
  "startTime": {
    "N": "1527797920000000000"
  },
  "support100K": {
    "BOOL": false
  },
  "type": {
    "S": "sponsored"
  }
}
```
* In `{environment}_sponsored_cheermote_campaigns` add an entry for the campaign following the format in this example:
```json
{
  "brandImageURL": {
    "S": "https://d3aqoihi2n8ty8.cloudfront.net/sponsored-cheermotes/brand-images/old-spice-demo/oldspice.png"
  },
  "brandName": {
    "S": "Old Spice"
  },
  "campaignId": {
    "S": "old-spice-demo"
  },
  "enabled": {
    "BOOL": true
  },
  "endTime": {
    "N": "1627120000000000000"
  },
  "minSponsoredAmount": {
    "N": "100"
  },
  "prefix": {
    "S": "oldspice"
  },
  "sponsoredAmountThresholds": {
    "M": {
      "100": {
        "N": "0.1"
      }
    }
  },
  "startTime": {
    "N": "1527120000000000000"
  },
  "totalBits": {
    "N": "1000000"
  },
  "usedBits": {
    "N": "0"
  },
  "userLimit": {
    "N": "10000"
  }
}
```
* Make a new entry in `{environment}_sponsored_cheermote_channel_statuses`.
* Make a new entry in `{environment}_sponsored_cheermote_eligible_channels`.

## Update The Poller
Last step is to update the poller to pull down the sponsored cheermote campaign. In `bits-sponsored-cheermote-campaigns` there are yaml files (with no extension) for each environment named in the scheme `{environment}-campaigns`, add your campaign id here and you’re done.

## Checking your work

There is a small [script](https://git.xarth.tv/commerce/payday/pull/2235) you can use to check the sponsored cheermote campaigns you have created.
To use this script you can use the following command with the campaign id and the channel id this campaign is running on:
```
AWS_PROFILE=twitch-bits-aws go run cmd/validate_sponsored_cheermote_campaign/main.go -environment production -environmentPrefix prod -campaignID <YOUR_CAMPAIGN_ID> -channelID <CHANNEL_ID THIS IS RUNNING ON>
```
An example output would be something like this:
(In this case for a miller-lite campaign) 
```
ID: millerlite-timthetatman-2019
Enabled: false
Start: 2019-10-11 01:00:00 -0700 PDT
End: 2019-10-11 14:30:00 -0700 PDT
Duration: 13h30m0s
LastUpdated: 2019-10-11 14:30:00.783807514 -0700 PDT
Prefix: millerlite
Minimum Cheer: 1
Match: map[1:1]
Total Bits: 360000
Blacklist Size: 0
Campaign uses whitelists, checking whitelist for channel 36769016
Checking channel statuses...OK
Checking channel eligibility...OK
Checking brand image exists in S3...OK
Checking cheermote assets folder exists in S3...OK
Checking action...OK
Checking poller config file...OK
Complete!
```

## Remove a Campaign
When removing a campaign’s configuration, make sure the remove the campaign’s ID from the S3 poller file first before removing the DynamoDB entries. The poller is updated periodically on the Payday hosts, so wait for a few minutes for all hosts to pick up the new change and then delete the campaign from DynamoDB. Otherwise, Payday hosts will hammer Dynamo with a non-existent ID and causes throttling issues. https://jira.twitch.com/browse/INC-1073.

