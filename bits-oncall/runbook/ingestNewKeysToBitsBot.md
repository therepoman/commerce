## How to Ingest New Keys to Bits Bot

So you've got the task to ingest new keys so the people of Twitch can once again cheer 100 bits on us. Here's how you do that.

### Create New Key Batch

Go to [The Bits Bot Key Pool Admin Panel page](https://admin-panel.xarth.tv/keys/batches?pool_id=bits-bot), and at the bottom you'll see a form for `Generate Product Keys`. Fill out the form to generate 1600 keys, and click `Generate`.

Wait for the keys to be generated, which is basically waiting a minute and refreshing the page until the batch says `SUCCEEDED`. Click on the batch ID in the table, which will take open our download key batch page and download the keys to your computer.

### Format the Keys for Ingestion

So what you get is basically a CSV file with a key per line. You need to format this so that it can be added to a JSON payload. Open your favorite text editor (I'm going to provide instructions for VSCode) and open the file.

With the file open, you can use `Shift + Option` to edit multiple lines at once, which you should add `"` on both ends of the key and a `,` at the end. Then, remove the comma on the last key entry, since JSON doesn't like trailing commas fool.

### Call the IngestKeys API

To call Bits Bot, you'll need to tunnel to Payday's prod VPC, because that's where Bits Bot lives. To do this, in your terminal do

```
TC=payday-prod ssh -D 8000 jumpbox
```

Then, in your REST client (I use Insomnia), configure the proxy correctly in the application preferences (you'd be using the address `socks5h://localhost:8000` for the field `HTTPS Network Proxy` in Insomnia Preferences).

Then, create a request to `https://main.us-west-2.prod.bits-bot.twitch.a2z.com/twirp/code.justin.tv.commerce.bits_bot.BitsBot/IngestKeys` with the payload

```
{
  "keys": [
    // the key records you just edited
  ]
}
```

Then hit the send button. If it returns 200, then you're good to go and Bits Bot will work again.
