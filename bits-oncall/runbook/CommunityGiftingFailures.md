## Triaging Failures

[Link to original Doc](https://docs.google.com/document/d/1CBu9T_lIl8CSqAZ3unK2rCXymS9QNvbiQG1DVdVaW7o) 

### Lambda Logs

When a community gift failures, a fulfillment failure sns message is sent. This message contains some useful information for us to triage failures for a particular community gift such as the `sender_id`, `origin_id` and `channel_id`.

You can find the failures based on which state machine failed in the Lambda logs(twitch-subs-aws account)
- Parent state machine failure: [PublishMultiGiftFulfillmentFailure Lambda Logs](https://tiny.amazon.com/13i0avlwi/IsenLink)
- Child state machine failure: [PublishGiftFulfillmentFailure Lambda Logs](https://tiny.amazon.com/12jehbe6r/IsenLink)

### State machine

In order to triage community gifting failures, you’ll want to go to the actual state machines that failed.

#### Parent State Machine

To get to the failed parent state machine, you’ll need the following information:
- `sender_id`
- `product_id`
- `origin_id`

From the lambda logs, you can retrieve all of this information to generate a direct link to the failed parent state machine. 

`https://us-west-2.console.aws.amazon.com/states/home?region=us-west-2#/executions/details/arn:aws:states:us-west-2:522398581884:execution:production-MassMysteryGiftParentStateMachine:MMG`-`SenderID`-`ProductID`-`OriginID UUID`

For example, a lambda log that has this failure:
```
time="01/Oct/2018:18:54:41 +0000" level=info msg="PublishMultiGiftFulfillmentFailure request: {OriginID:amzn1.twitch.payments.order.301cb0d9-70ac-4bf8-8678-071a80e6ffc6 ProductID:1151436 GiftQuantity:5 SenderID:264068609}"
```

Can be linked to the following parent state machine:

https://us-west-2.console.aws.amazon.com/states/home?region=us-west-2#/executions/details/arn:aws:states:us-west-2:522398581884:execution:production-MassMysteryGiftParentStateMachine:MMG-264068609-1151436-301cb0d9-70ac-4bf8-8678-071a80e6ffc6

#### Child State Machine

For the child state machines, you’ll need all the information a parent state machine needs plus the `recipientId`

`https://us-west-2.console.aws.amazon.com/states/home?region=us-west-2#/executions/details/arn:aws:states:us-west-2:522398581884:execution:production-MassMysteryGiftParentStateMachine:MMG`-`SenderID`-`ProductID`-`OriginID UUID`-`RecipientID`

Example lambda log failure: 
```
time="07/Oct/2018:05:25:40 +0000" level=info msg="PublishGiftFulfillmentFailureRequest: {GiftIndex:2 OriginID:amzn1.twitch.payments.order.c49c63c0-b892-47cc-a283-dd9efe4c0226 ProductID:300969 RecipientID:151269137 SenderID:90031740}"
```

From here you can generate a link directly to the state machine, like so:

https://us-west-2.console.aws.amazon.com/states/home?region=us-west-2#/executions/details/arn:aws:states:us-west-2:522398581884:execution:production-MassMysteryGiftChildStateMachine:MMGC-90031740-300969-c49c63c0-b892-47cc-a283-dd9efe4c0226-151269137


Since it is possible you won’t find the child state machine due to the originally chosen recipient being ineligible, you’ll want to go back to the parent machine. You can do this by replacing the link with the parent link and removing the recipient id from the url. EX:
 
https://us-west-2.console.aws.amazon.com/states/home?region=us-west-2#/executions/details/arn:aws:states:us-west-2:522398581884:execution:production-MassMysteryGiftParentStateMachine:MMG-90031740-300969-c49c63c0-b892-47cc-a283-dd9efe4c0226

### Replaying Failures

#### Replay Parent State Machine

[Complete Replay Parent State Machine](https://tiny.amazon.com/xhxdl4bc/IsenLink)

There currently exist two state machines that can replay failed community gift runs. You’ll want to use these “complete-replay” state machines so you don’t hit a duplicate execution id error with the state machines.
 

If there is a large amount of failures(e.g. > 5 failures), you can trigger the complete replay parent state machine. When you trigger it, you’ll need to pass in a JSON input. ***It is crucial that you don’t change the gift_quantity and pass in the is_replay flag***, all other fields can come from the failed parent state machine. 
Example:
```
{
  "sender_id": "260489662",
  "recipient_ids": null,
  "product_id": "8817",
  "origin_id": "amzn1.twitch.payments.order.ffe2b025-9e1b-4111-8dac-7b771ffeb0cb",
  "gift_type": "mystery",
  "gift_quantity": 20,
  "benefit_start": "2018-09-19T07:03:56Z",
  "benefit_end": "2018-10-22T07:03:56Z",
  "channel_id": "93031467",
  "is_replay": true
}
```

This will run the complete replay child state machine and fulfill 20 gifts.

#### Replay Child State Machine

For 1 or a few child state machines failures you can run this: [Complete Replay Child State Machine](https://tiny.amazon.com/m8y0izz1/IsenLink) to replay the failed child step functions.

For a smaller amount of failures, you can trigger the complete replay child state machine. If you know that the recipient won’t fail again then you can pass in the same parameters that the failed child state machine had to this state machine to replay this one community gift.

**You can run [the script found here](https://git.xarth.tv/revenue/lambdwich/blob/master/scripts/replay-failed-step-function/replay-failed-step-function.go) to replay failed child state machines or specific child state machines**

`go run * -env=staging -startDate=2019-02-07T09:31:14-07:00`

Example:
```
{
  "sender_id": "260489662",
  "origin_id": "amzn1.twitch.payments.order.f486911b-3fcc-40e6-9014-670ebd47edd7",
  "product_id": "8817",
  "channel_id": "93031467",
  "gift_type": "mystery",
  "gift_index": 64,
  "gift_quantity": 1,
  "benefit_start": "2018-09-19T07:03:56Z",
  "benefit_end": "2018-10-22T07:03:56Z",
  "recipient_id": "59589174"
}
```

#### Replay Parent State Machine Script

For a large amount of failures, there exist a go main script that will execute n number of replay parent state machines based off a CSV input. 

[This script can be found here.](https://git.xarth.tv/revenue/lambdwich/blob/master/scripts/replay-parent-state-machine.go)
 
Details on how to run the script can be found in the source code for the script.

#### Replay Child State Machine Lambda Step

To replay a particular failed step in the child state machine, there exist a go main script that can take in a name of a failed step and a failed state machine arn and replay that failed step by calling the lambda function for that step directly. 
For example, if you have a child state machine that has a failed `EntitleActiveGifterBadge` step then you can run this script to rerun that failed lambda. 

[This script can be found here.](https://git.xarth.tv/revenue/lambdwich/blob/master/scripts/replay-failed-child-lambda.go)

Details on how to run the script can be found in the source code for the script.


### Refunding Failures
Currently only full failures are refundable, so only when `PublishMultiGiftFulfillmentFailure` gets triggered. 
Once you have the gifter and the `origin_id` you can call payment’s refund order API to refund. 
You can then check that the purchase order was successfully refunded from [admin-panel](https://admin-panel.internal.justin.tv/payments/purchase_orders). 
Just search for the gifter.

Ex request to production:
```
curl --request PUT \
  --url http://http://payments.internal.justin.tv/purchase_orders/{origin_id}/refund \
  --header 'content-type: application/json' \
  --data '{
    "user_id":"{gifter_id}"
}'
```


