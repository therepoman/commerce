# Fulton Manual Rollback Instructions

Since [Fulton](https://docs.fulton.twitch.a2z.com/docs) has CI/CD pipeline set up, Fulton deploys do not go through clean-deploy.
Should errors/panics happen where emergency manual rollback is required, do the following:
 - Go to pipeline dashboard (`https://pipelines.amazon.com/pipelines/<service-name>`) and see the status
 - Go to AWS prod account (w/ Admin role) --> CloudFormation
 - If the service stack is still updating
   - Stack Actions --> Cancel Update Stack ([Doc](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/using-cfn--stack-update-cancel.html))
     (This rolls back to the previous stack setting)
 - If the stack has finished updating, you need to revert the stack from the Pipeline Dashboard
   - Get on Amazon VPN (otherwise interactivity in Dashboard is limited)
   - Back at Dashboard, go to `Emergency Actions` and follow instructions there
   - You technically should be able to do it in CF, but you'd have to manually update the CF parameters which can be error prone. From my own testing, CF Change set does not keep a full history and we can't roll back using this.