# Fix DynamoDB Application Autoscaling in Terraform

So, someone handjammed something in DynamoDB for autoscaling rules and now it's messed up your terraform so you can't apply your plan without errors? Follow these steps to fix yourself up right good.

### Find the offending scaling policy

First, we need to look up the offending scaling policy on the table that doesn't match up with terraform. Do this by running

```bash
AWS_PROFILE=${profile-name} aws application-autoscaling describe-scaling-policies --service-namespace dynamodb --resource-id table/${table-name}
```

Look for the appropriate scaling dimension, which is normally either `dynamodb:table:WriteCapacityUnits` or `dynamodb:table:ReadCapacityUnits`. When you find the dimension, note the policy name, we're going to need this next.

### Second, delete the offending policy

Now that we got our policy that's no bueno, let's delete it. Don't worry, you're going to be fine deleting it because we'll just recreate it in a bit. Run this command to delete:

```bash
AWS_PROFILE=${profile-name} aws application-autoscaling delete-scaling-policy --policy-name ${offending-policy-name} --service-namespace dynamodb --resource-id table/${table-name} --scalable-dimension ${scalable-dimension}
``` 

This should take no time to run. 

### Rinse and Repeat

If there are more tables or policies that have borked up autoscaling policies, find and delete those using the steps above. We want to clean up all the bad policies before we move on to the next step.

### Plan / Apply Terraform

Just do your normal terraform plan / apply. It should recreate the autoscaling policies without errors.

### Notes

* If you need to deal with a global table, just remember to also clean up policies in the other regions by adding on `region=${other-region}` to these commands.
* if you're working on an index (e.g., GSI), remember that you replace `table` in the resource ID with the appropriate identifier for indexes (usually in the form `table/{table-name}/index/{index-name}`). You also need to change your scalable-dimension from `dynamodb:table:WriteCapacityUnits` to `dynamodb:index:WriteCapacityUnits`.
