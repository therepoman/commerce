# Bits General Runbook

This is the start of the general runbook. 
This is supposed to include general instructions that aren't service specific. Feel free to link or add other instructions here as well!

* [sqs Message Moving](sqsMessageMoving.md) - easily move messages between queues with on simple CLI call. 
* [S3 backend Experiment config](S3ExperimentConfig.md) - how to dial up backend experiment percentages or add users / channels to an experiment.
* [Add a new sponsored cheermote](sponsoredCheermote.md) - how to set up a sponsored cheermote
* [Entitle bit emote failures](EntitleBitsEmoteFailures.md) - how to resolve emote entitlement failures before enqueueing to SQS
