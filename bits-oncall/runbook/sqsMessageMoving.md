# Easily move sqs message between queues

You don't need to change permissions or the redrive policy for this. 
You can easily move messages using this tool on your cloud desktop: https://w.amazon.com/index.php/SQSMessageMoverTool

If you're in Payday, there is already a IAM user role material set you can use 
```com.amazon.credentials.isengard.021561903526.user/SQS-Message-Mover```

In other services, just create a new IAM user or follow the instructions on the page linked above for cross-account moves.

# If you can't get that to work

It's OK, it happens. [Here's a good guide to how to redrive SQS messages manually](https://docs.google.com/document/d/1JF_WwJWMYJLmh1FWiQFhrq5bE_nZfCmDi-GEJ7zB6vM/edit#heading=h.s70x8fraypbd).
