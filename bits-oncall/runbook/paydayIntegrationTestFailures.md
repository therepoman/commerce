# Payday Integration Test Failures

### TestPrismPrivatePubsubEvent / TestPrismPrivatePubsubEventCheckForDouble
Error: 
```
10:10:22     --- FAIL: TestPayday/TestPrismPrivatePubsubEvent (0.11s)
10:10:22         require.go:794: 
10:10:22             	Error Trace:	pubsub_test.go:211
10:10:22             	            				pubsub_test.go:47
10:10:22             	            				pubsub_test.go:257
10:10:22             	            				pubsub_test.go:28
10:10:22             	Error:      	Received unexpected error:
10:10:22             	            	ERR_BADAUTH
10:10:22             	Test:       	TestPayday/TestPrismPrivatePubsubEvent
10:10:22     --- FAIL: TestPayday/TestPrismPrivatePubsubEventCheckForDouble (0.12s)
10:10:22         require.go:794: 
10:10:22             	Error Trace:	pubsub_test.go:211
10:10:22             	            				pubsub_test.go:116
10:10:22             	            				pubsub_test.go:257
10:10:22             	            				pubsub_test.go:97
10:10:22             	Error:      	Received unexpected error:
10:10:22             	            	ERR_BADAUTH
10:10:22             	Test:       	TestPayday/TestPrismPrivatePubsubEventCheckForDouble
```
- this is likely due to an oauth token expired
- look for `commerce/payday/staging/qa_bits_tests_oauth_token` in [sandstorm dashboard](https://dashboard.internal.justin.tv/sandstorm/manage-secrets?nameFilter=commerce%2Fpayday%2Fstaging%2Fqa_bits_tests_oauth_token)
- update the secret with the new oauth token for `qa_bits_tests` (145903336)
  * oauth token can be retrieved from the cookie (key `auth-token`) after logging into the account on twitch.tv (use prod twitch site)
  * this acconut has 2FA (Seph owns it), you can also ask someone to disable 2FA for the next login on admin panel (https://admin-panel.internal.justin.tv/twitch/users/145903336).
- re-run the tests and they should pass now.
