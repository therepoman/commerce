## Resolving Emote Entitlement Failures before enqueueing for SQS

### Resolving the issue

Runbook for resolving emote entitlement failures before enqueueing SQS.
Pong paging would look like the example below, given a time frame and the alarm.
```
Emote Entitlement failed before enqueueing for SQS
A user should be entitled a bits emote but something failed before we got to the SQS queue for entitlements. Check the logs for a failure related to 'emote entitlement' to find the channel, user, and threshold and then manually entitle them to that emote slot

To see Carnaval monitor: http://carnaval.amazon.com/v1/viewObject.do?type=monitor&name=Twitch.Bits.p...
To see Carnaval history: http://carnaval.amazon.com/v1/viewHistory.do?type=monitor&name=Twitch.Bits....
To see Carnaval snapshot: http://carnaval.amazon.com/v1/viewSnapshot.do?timestamp=2020-06-16+23%3A56%...
------------------------------------------------
Monitor: Twitch.Bits.payday_emote_entitlement_failure (65536434)
Status: ALERT
Description: Emote Entitlement failed before enqueueing for SQS

Supporting agents in ALARM:
Agent: Alarm(CloudWatch, arn:aws:cloudwatch:us-west-2:021561903526:alarm:prod_emote_entitlement_failure__get_group_id_alarm, NONE)
name: arn:aws:cloudwatch:us-west-2:021561903526:alarm:prod_emote_entitlement_failure__get_group_id_alarm
node: NONE
source: CloudWatch
lastValue: Threshold Crossed: 1 datapoint [1.0 (16/06/20 23:51:00)] was greater than the threshold (0.0).
------------------------------------------------
```

To resolve this page. You need to query Cloudwatch for information needed to run the Entitlement script in Payday. 
Script uses these arguments and can be found here.
- `user_id`
- `channel_id`
- `threshold`

Query in Cloudwatch for the error log to find the necessary arguments:

```$xslt
time="2020-06-16T23:55:25Z" level=error msg="failed to get new group id for badge tier emote entitlement from dynamo" 
channelID=456164749 error=": RequestCanceled: request context canceled\ncaused by: context canceled" threshold=10000 
userID=476135480
```

In above example userID is `476135480`, channelID=`456164749` and threshold is `10000` which you will use to run the entitlement script.
[Script can be found here](https://git.xarth.tv/commerce/payday/tree/master/cmd/emote_entitlement_replay)

To run the script make sure you have aws credentials or aws profile for PayDay Production. Insert these variables to the script
and run it. This should entitle the emote for that specific user. 

To check that this script ran successfully go to PayDay's Dynamodb tables `prod_badge_tier_emote_groupids
` and query for that channel_id and look for it to exist and have these columns
- `channel_id`
- `threshold`
- `created`
- `group_id`
- `is_backfilled`

if so you successful resolved the issue. Don't forget to resolve the tt!
