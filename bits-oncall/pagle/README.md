# Runbook

I, _Michael Flores_, hereby allow the poor soul reading this to page me for any Pagle issues after some best effort analysis until I finish fleshing out this runbook.

---> page-flomic@amazon.com <----

## Alarms

All pagle alarms are maintained in [Pagle's terraform configuration](https://git.xarth.tv/commerce/pagle/tree/master/terraform/modules/pagle/alarms). These alarms are used to create corresponding alarms in [Carnival](https://carnaval.amazon.com/v1/unifiedSearch/v2018/simpleSearch.do?searchFormType=&searchString=pagle).

## Dashboards

| Dashboards |
| --- | 
| **[Availability](https://us-west-2.console.aws.amazon.com/cloudwatch/home?region=us-west-2#dashboards:name=T0-Availability)** |
| **[API](https://us-west-2.console.aws.amazon.com/cloudwatch/home?region=us-west-2#dashboards:name=T1-API)** |
| **[Async Workers](https://us-west-2.console.aws.amazon.com/cloudwatch/home?region=us-west-2#dashboards:name=T1-Workers)** |
| **[SFN](https://us-west-2.console.aws.amazon.com/cloudwatch/home?region=us-west-2#dashboards:name=T2-StepFunction)** |
| **[SQS](https://us-west-2.console.aws.amazon.com/cloudwatch/home?region=us-west-2#dashboards:name=T2-SQS)** |
| **[Logging](https://us-west-2.console.aws.amazon.com/cloudwatch/home?region=us-west-2#dashboards:name=T3-Logging)** |
