package pkgpath

import (
	vpkgpath "code.justin.tv/video/pkgpath"
)

// Main returns the import path of package main, the currently running
// command.  If the import path could not be determined, it returns "main" and
// false.
//
// GOMODULES support:
//
// This works by looking at debug.ReadBuildInfo() added in Go1.12. It does not work
// if the main package is ran/built with the filename.
//
//     go run ./cmd/app/*.go
//
// It is recommended to run/build the binary with the package path.
//
//     go run ./cmd/app
//
// GOPATH support:
//
// It does not work on binaries compiled on filesystems that use a separator
// other than '/'.  It does not correctly detect the import path if a segment
// of the path is "src".
//
// This works by looking at the file path where the main.init function is
// defined, searching for the rightmost "src" directory if the recorded file
// path is absolute.
//
// If the command has been compiled with a trimpath argument in gcflags, the
// available file path is relative. In this case, the directory of the file is
// assumed to be exactly the import path. When using the trimpath compiler
// flag, be sure to trim off the relevant GOPATH segment and the "src" path
// segment that follows it, like so (in the case of a single-element GOPATH):
//
//     go build -gcflags "-trimpath $(go env GOPATH)/src" ./...
//
// Deprecated: Use code.justin.tv/video/pkgpath instead.
func Main() (string, bool) {
	return vpkgpath.Main()
}
