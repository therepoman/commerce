package yimg

import (
	"fmt"
)

const channelOfflineImageFormat = "https://static-cdn.jtvnw.net/jtv_user_pictures/%s-channel_offline_image-{{.Uid}}-{{.Size}}.{{.Format}}"
const channelOfflineImageNewFormat = "https://static-cdn.jtvnw.net/jtv_user_pictures/{{.Uid}}-channel_offline_image-{{.Size}}.{{.Format}}"

var ChannelOfflineImageSizes = []string{"1920x1080"}
var ChannelOfflineImageRatio = 16.0 / 9.0

// ChannelOfflineImages converts database yaml into an Images map for the provided username
func ChannelOfflineImages(data []byte, username string) (Images, error) {
	return parse(fmt.Sprintf(channelOfflineImageFormat, username), channelOfflineImageNewFormat, data, ChannelOfflineImageSizes, bySize)
}

func ChannelOfflineImagesToString(images *Images) (*string, error) {
	return images.Marshal(&ChannelOfflineImageRatio)
}

func ChannelImageName(image Image, username string) (string, error) {
	if image.NewFormat {
		return fmt.Sprintf("%s-channel_offline_image.%s", image.Uid, image.Format), image.validate()
	} else {
		return fmt.Sprintf("%s-channel_offline_image-%s.%s", username, image.Uid, image.Format), image.validate()
	}
}
