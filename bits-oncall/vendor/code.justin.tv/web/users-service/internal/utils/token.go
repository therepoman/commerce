package utils

import (
	"crypto/rand"
	"encoding/json"
	"math/big"
)

const (
	tokenLength = 30
	alphabet    = "abcdefghijklmnopqrstuvwxyz0123456789"
)

var (
	alphabetLength = big.NewInt(int64(len(alphabet)))
)

// RestoreTokenUserInfo stores the original email and ID of the user
type RestoreTokenUserInfo struct {
	Email  string `json:"email"`
	UserID string `json:"user_id"`
}

func GenerateRestorationToken(userID, email string) (string, string, error) {
	token, err := randomToken()
	if err != nil {
		return "", "", err
	}
	r := &RestoreTokenUserInfo{
		Email:  email,
		UserID: userID,
	}
	b, err := json.Marshal(r)
	if err != nil {
		return "", "", err
	}
	return token, string(b), nil
}

func randomToken() (string, error) {
	var bytes = make([]byte, tokenLength)

	for i := range bytes {
		offset, err := rand.Int(rand.Reader, alphabetLength)
		if err != nil {
			return "", err
		}
		bytes[i] = alphabet[offset.Uint64()]
	}

	return string(bytes), nil
}
