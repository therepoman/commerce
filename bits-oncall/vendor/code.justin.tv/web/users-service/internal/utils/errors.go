package utils

import (
	"strings"

	"code.justin.tv/chat/golibs/errx"
	"github.com/afex/hystrix-go/hystrix"
)

func IsTimeoutErr(err error) bool {
	err = errx.Unwrap(err)
	if err == nil {
		return false
	}

	return strings.Contains(err.Error(), "i/o timeout")
}

func IsContextCanceled(err error) bool {
	err = errx.Unwrap(err)
	if err == nil {
		return false
	}

	return strings.Contains(err.Error(), "context canceled")
}

func IsHystrixErr(err error) bool {
	err = errx.Unwrap(err)
	if err == hystrix.ErrCircuitOpen || err == hystrix.ErrTimeout || err == hystrix.ErrMaxConcurrency {
		return true
	}
	return false
}

func IsKnownErr(err error) bool {
	return IsTimeoutErr(err) || IsContextCanceled(err) || IsHystrixErr(err)
}

func IsUniqueKeyViolation(err error) bool {
	err = errx.Unwrap(err)
	if err == nil {
		return false
	}
	return strings.Contains(err.Error(), "pq: duplicate key value violates unique constraint")
}
