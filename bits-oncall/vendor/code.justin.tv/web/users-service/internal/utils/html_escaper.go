package utils

import "strings"

var htmlEscaper = strings.NewReplacer(
	`<`, "&lt;",
	`>`, "&gt;",
	`\n`, "",
)

func EscapeString(s string) string {
	return htmlEscaper.Replace(s)
}
