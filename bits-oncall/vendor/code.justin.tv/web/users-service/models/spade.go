package models

import "time"

type LoginRenameEvent struct {
	UserID   string `json:"user_id"`
	OldLogin string `json:"old_login"`
	NewLogin string `json:"new_login"`
	Type     string `json:"rename_type"`
	Env      string `json:"environment"`
}

type DisplaynameChangeEvent struct {
	UserID             string `json:"user_id"`
	OldDisplayname     string `json:"old_displayname"`
	OldDisplaynameLang string `json:"old_displayname_lang"`
	NewDisplayname     string `json:"new_displayname"`
	NewDisplaynameLang string `json:"new_displayname_lang"`
	UserLanguage       string `json:"user_language"`
	Type               string `json:"type"`
	Env                string `json:"environment"`
}

type BanUserEvent struct {
	ReporterID    string `json:"reporter_id"`
	TargetID      string `json:"target_id"`
	Reason        string `json:"reason"`
	Duration      string `json:"duration"`
	IPBanDuration int64  `json:"ip_ban_duration"`
	Type          string `json:"type"`
	Env           string `json:"environment"`
}

type SignupEvent struct {
	UserID                string `json:"user_id"`
	Login                 string `json:"login"`
	IP                    string `json:"ip"`
	DeviceID              string `json:"device_id"`
	Env                   string `json:"environment"`
	NameValidationSkipped bool   `json:"name_validation_skipped"`
}

type UpdateChannelEvent struct {
	ChannelID string `json:"channel_id"`
	Channel   string `json:"channel"`

	OldDirectoryHidden *bool `json:"old_directory_hidden"`
	DirectoryHidden    *bool `json:"directory_hidden,omitempty"`

	OldStatus *string `json:"old_status"`
	Status    *string `json:"status"`

	OldGame *string `json:"old_game"`
	Game    *string `json:"game"`

	OldBroadcaster *string `json:"old_broadcaster"`
	Broadcaster   *string `json:"broadcaster,omitempty"`

	OldBroadcasterLanguage *string `json:"old_broadcaster_language"`
	BroadcasterLanguage    *string `json:"broadcaster_language"`

	OldBroadcasterSoftware *string `json:"old_broadcaster_software"`
	BroadcasterSoftware    *string `json:"broadcaster_software"`

	OldGameID *uint64 `json:"old_game_id"`
	GameID    *uint64 `json:"game_id"`

	OldMature *bool `json:"old_mature"`
	Mature    *bool `json:"mature"`

	OldTitle *string `json:"old_title"`
	Title    *string `json:"title"`

	OldViewsCount *uint64 `json:"old_views_count"`
	ViewsCount    *uint64 `json:"views_count"`

	OldLastBroadcastTime *time.Time `json:"old_last_broadcast_time"`
	LastBroadcastTime    *time.Time `json:"last_broadcast_time"`

	OldLastBroadcastID *string `json:"old_last_broadcast_id"`
	LastBroadcastID    *string `json:"last_broadcast_id"`

	OldLastLiveNotificationSent *time.Time `json:"old_last_live_notification_sent"`
	LastLiveNotificationSent     *time.Time `json:"last_live_notification_sent"`

	OldAbout *string `json:"old_about"`
	About    *string `json:"about"`

	OldRedirectChannel *string `json:"old_redirect_channel"`
	RedirectChannel    *string `json:"redirect_channel"`

	OldDisableChat *bool `json:"old_disable_chat"`
	DisableChat    *bool `json:"disable_chat"`

	OldPrimaryTeamID *uint64 `json:"old_primary_team_id"`
	PrimaryTeamID    *uint64 `json:"primary_team_id"`

	OldPrivacyOptionsEnabled *bool `json:"old_privacy_options_enabled"`
	PrivacyOptionsEnabled    *bool `json:"privacy_options_enabled"`

	OldPrivateVideo *bool `json:"old_private_video"`
	PrivateVideo    *bool `json:"private_video"`

	Env string `json:"environment"`
}

type SetUserImagesEvent struct {
	UserID                       string  `json:"user_id"`
	PreviousImage                string  `json:"previous_image"`
	ImageType                    string  `json:"image_type"`
	DefaultProfileImageID        *string `json:"default_profile_image_id"`
	DefaultProfileBannerID       *string `json:"default_profile_banner_id"`
	DefaultChannelOfflineImageID *string `json:"default_channel_offline_image_id"`
	Env                          string  `json:"env"`
}
