package models

import (
	"net/url"
	"time"

	validator "gopkg.in/validator.v2"
)

type DeleteChannelSocialMediaInput struct {
	Item DeleteChannelSocialMediaInputItem `json:"item" validate:"nonzero"`
}

type DeleteChannelSocialMediaInputItem struct {
	ID     string `json:"id" validate:"nonzero"`
	UserID string `json:"user_id" validate:"nonzero"`
}

type UpdateChannelSocialMediaInput struct {
	Item UpdateChannelSocialMediaInputItem `json:"item" validate:"nonzero"`
}

type UpdateChannelSocialMediaInputItem struct {
	URL    string `json:"url"`
	Title  string `json:"title"`
	ID     string `json:"id" validate:"nonzero"`
	UserID string `json:"user_id" validate:"nonzero"`
}

func (sm *UpdateChannelSocialMediaInput) Validate() error {
	if err := validator.Validate(sm); err != nil {
		return err
	}

	if len(sm.Item.URL) > 0 {
		if _, err := url.ParseRequestURI(sm.Item.URL); err != nil {
			return ErrInvalidURL
		}
	}

	return nil
}

type CreateChannelSocialMediaInput struct {
	UserID string                              `json:"user_id" validate:"nonzero"`
	Items  []CreateChannelSocialMediaInputItem `json:"items" validate:"nonzero"`
}

type CreateChannelSocialMediaInputItem struct {
	URL   string `json:"url" validate:"nonzero"`
	Title string `json:"title" validate:"nonzero"`
}

func (sm *CreateChannelSocialMediaInput) Validate() error {
	if err := validator.Validate(sm); err != nil {
		return err
	}

	for _, item := range sm.Items {
		if _, err := url.ParseRequestURI(item.URL); err != nil {
			return ErrInvalidURL
		}
	}

	return nil
}

type ChannelSocialMediaProperties struct {
	ID        string    `json:"id"`
	URL       string    `json:"url"`
	Title     string    `json:"title"`
	UserID    string    `json:"user_id"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type ChannelSocialMediaResults struct {
	Results []ChannelSocialMediaProperties `json:"results"`
	Error   string                         `json:"error"`
}

type ChannelSocialMediaResult struct {
	Result ChannelSocialMediaProperties `json:"results"`
}
