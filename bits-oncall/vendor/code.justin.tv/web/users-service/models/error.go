package models

import (
	"fmt"
	"net/http"
)

// CodedError is an error that implements api.ServiceError and client.Error
type CodedError struct {
	CodeValue       string
	ErrorValue      string
	StatusCodeValue int
	ShouldLogValue  bool
	SkipCircuitTrip bool
}

func (e CodedError) Code() string  { return e.CodeValue }
func (e CodedError) Error() string { return e.ErrorValue }
func (e CodedError) StatusCode() int {
	if e.StatusCodeValue == 0 {
		return http.StatusInternalServerError
	}
	return e.StatusCodeValue
}
func (e CodedError) ShouldLog() bool { return e.ShouldLogValue }
func (e CodedError) SkipTrip() bool  { return e.SkipCircuitTrip }

// ErrorResponse is used to encode API errors and decode them client side.
type ErrorResponse struct {
	Status     int    `json:"status"`
	Message    string `json:"message"`
	StatusText string `json:"error"`
	ErrorCode  string `json:"error_code"`
}

func (T ErrorResponse) Error() string {
	return fmt.Sprintf("Error (%d): %s", T.Status, T.Message)
}

func (T ErrorResponse) StatusCode() int {
	return T.Status
}
