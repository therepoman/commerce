package api

import (
	"encoding/base64"
	"strconv"
)

func CursorToInt(cursor string) (int, error) {
	if cursor == "" {
		return 0, nil
	}

	decodedBytes, err := base64.StdEncoding.DecodeString(cursor)
	if err != nil {
		return 0, err
	}
	return strconv.Atoi(string(decodedBytes))
}

func IntToCursorString(cursorInt int) string {
	cursorString := strconv.FormatInt(int64(cursorInt), 10)
	return base64.StdEncoding.EncodeToString([]byte(cursorString))
}
