package identifier

// ProcessIdentifier represents the identity of a running process.
type ProcessIdentifier struct {
	Service  string `json:"service"`
	Region   string `json:"region"`
	Stage    string `json:"stage"`
	Substage string `json:"substage"`
	Version  string `json:"version"`
	Machine  string `json:"machine"`
	LaunchID string `json:"launch_id"`
}
