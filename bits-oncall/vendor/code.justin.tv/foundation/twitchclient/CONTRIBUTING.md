## Contributing

`twitchclient` is a core library at Twitch and its quality must be held to a
high standard.

Bugs may cause critical systems to fail and hasty design decisions can lead to
massive migration efforts after the next iteration.

As a result, the process to contribute is stricter than most projects.

Before writing any code, please open a Github issue. If it's a feature request,
explain the feature that you're asking for and propose a design. If it's a bug,
explain the failure and show steps to reproduce. While a Github issue is not
mandatory, it may save you from having to re-think your changes after you
implement and test them.

When you're ready, open a pull request. Pull requests will be rejected unless
they're accompanied by tests which exercise the changes being made to the
library.

Exceptions are considered for changes which can't be tested (for example,
fixing a typo in `README.md`).

When your changes are merged in, if any changes have been made to production
code, a new release following [Semantic Versioning](http://semver.org/) will
be created.

## Suggestions

If you'd just like to leave feedback or put something on our radar, please open
a Github issue.
