package payments

import (
	"time"
)

type LocationData struct {
	CountryCode string `json:"country_code,omitempty"`
	City        string `json:"city,omitempty"`
	PostalCode  string `json:"postal_code,omitempty"`
	IsAnonymous bool   `json:"is_anonymous,omitempty"`
}

type PaymentSession struct {
	DeviceID             string        `json:"device_id,omitempty"`
	LocalStorageDeviceID string        `json:"localstorage_device_id,omitempty"`
	TabSessionID         string        `json:"tab_session_id,omitempty"`
	PageSessionID        string        `json:"page_session_id,omitempty"`
	CheckoutSessionID    string        `json:"checkout_session_id,omitempty"`
	SessionIPAddress     string        `json:"session_ip_address,omitempty"`
	LocationData         *LocationData `json:"location_data,omitempty"`
}

// For InitiatePurchase API
type InitiatePurchaseRequest struct {
	PaymentSession   *PaymentSession `json:"payment_session,omitempty"`
	Platform         string          `json:"platform"`
	IsGift           bool            `json:"is_gift,omitempty"`
	IsAnon           bool            `json:"is_anon,omitempty"`
	MysteryGiftCount *int            `json:"mystery_gift_count,omitempty"`
	RecipientID      string          `json:"recipient_id,omitempty"`
	WithSpecialPromo string          `json:"with_special_promo,omitempty"`
	IPCountryCode    *string         `json:"ip_country_code,omitempty"`
	TaxCountryCode   *string         `json:"tax_country_code,omitempty"`
	Reactivate       *ReactivateInfo `json:"reactivate,omitempty"`
}

// For CompletePurchase API
type CompletePurchaseRequest struct {
	PaymentSession   *PaymentSession   `json:"payment_session,omitempty"`
	PaymentProvider  string            `json:"payment_provider"`
	PaymentInfo      PaymentInfo       `json:"payment_info"`
	Platform         string            `json:"platform"`
	IsGift           bool              `json:"is_gift,omitempty"`
	MysteryGiftCount *int              `json:"mystery_gift_count,omitempty"`
	RecipientID      string            `json:"recipient_id,omitempty"`
	IsAnon           bool              `json:"is_anon,omitempty"`
	WithSpecialPromo string            `json:"with_special_promo,omitempty"`
	DeviceID         *string           `json:"device_id,omitempty"`
	TabSessionID     *string           `json:"tab_session_id,omitempty"`
	IPCountryCode    *string           `json:"ip_country_code,omitempty"`
	PriceInfo        *PriceInfo        `json:"price_info,omitempty"`
	Residence        *ResidenceInfo    `json:"residence,omitempty"`
	Reactivate       *ReactivateInfo   `json:"reactivate,omitempty"`
	ExtensionID      string            `json:"extension_id,omitempty"`
	TenantTracking   map[string]string `json:"tenant_tracking,omitempty"`
}

// For CompleteChanSubPurchase API
type CompleteChanSubPurchaseRequest struct {
	PaymentSession   *PaymentSession  `json:"payment_session,omitempty"`
	PaymentProvider  string           `json:"payment_provider"`
	Platform         string           `json:"platform"`
	IsGift           bool             `json:"is_gift,omitempty"`
	MysteryGiftCount *int             `json:"mystery_gift_count,omitempty"`
	RecipientID      string           `json:"recipient_id,omitempty"`
	IsAnon           bool             `json:"is_anon,omitempty"`
	WithSpecialPromo string           `json:"with_special_promo,omitempty"`
	DeviceID         *string          `json:"device_id,omitempty"`
	TabSessionID     *string          `json:"tab_session_id,omitempty"`
	IPCountryCode    *string          `json:"ip_country_code,omitempty"`
	Reactivate       *ReactivateInfo  `json:"reactivate,omitempty"`
	ExtensionID      string           `json:"extension_id,omitempty"`
	TenantTracking   string           `json:"tenant_tracking,omitempty"`
	BillingAuthInfo  *BillingAuthInfo `json:"billing_auth_info,omitempty"`

	// Passed in by Everdeen for the async PurchaseOffer flow
	PurchaseOrderOriginID string `json:"purchase_order_origin_id,omitempty"`

	// Passed in by Everdeen
	// PromoPrice and PromoDiscountPercent used for promotional Offer with finalized promotional price (discount included)
	// OriginalBasePrice used for calculating order adjustment and setting renewal price for recurly sub
	HasPromoDiscount     bool     `json:"has_promo_discount,omitempty"`
	PromoPrice           *int     `json:"promo_price,omitempty"`
	PromoDiscountPercent *float32 `json:"promo_discount_percent,omitempty"`
	OriginalBasePrice    *int     `json:"original_base_price,omitempty"`
	PromoName            *string  `json:"promo_name,omitempty"`
}

// For CancelPurchase API
type CancelPurchaseRequest struct {
	PaymentSession *PaymentSession `json:"payment_session,omitempty"`
	// Used to form the base_key for notifying Subscriptions Service
	GiftPurchaseProfileID string `json:"gift_purchase_profile_id,omitempty"`
	// The actual purchase profile to cancel, also helps form the upgrade_key for notifying Subscriptions Service
	PurchaseProfileID string `json:"purchase_profile_id,omitempty"`
	Reason            string `json:"reason,omitempty"`
}

type PaymentInfo struct {
	FirstName      string  `json:"first_name"`
	LastName       string  `json:"last_name"`
	Token          string  `json:"token"`
	Gateway        string  `json:"gateway"`
	State          string  `json:"state"`
	Country        string  `json:"country"`
	CardBINCountry *string `json:"card_bin_country,omitempty"`
}

type PriceInfo struct {
	ID       string `json:"id"`
	Currency string `json:"currency"`
}

type DiscountPolicy struct {
	DiscountType    string  `json:"discount_type"`
	DiscountPercent float32 `json:"discount_percent"`
	BillingPeriods  int32   `json:"billing_periods"`
}

type ResidenceInfo struct {
	CountryCode string  `json:"country_code"`
	ZipCode     *string `json:"zip_code,omitempty"`
}

// For GetPurchase API
type GetPurchaseResponse struct {
	PurchaseDetails
	// If there are no purchase details, show the compatible purchases if they exist (i.e. user is subscribed to a different tier)
	CompatiblePurchases []PurchaseDetails `json:"compatible_purchases,omitempty"`
	Error               string            `json:"error,omitempty"`
}

// For GetInitiatePurchase API
type GetInitiatePurchaseResponse struct {
	Recurly                     RecurlyParams `json:"recurly,omitempty"`
	XsollaV3                    XsollaParams  `json:"xsolla_v3,omitempty"`
	Zuora                       ZuoraParams   `json:"zuora,omitempty"`
	Error                       string        `json:"error,omitempty"`
	CanGift                     bool          `json:"can_gift,omitempty"`
	CanPurchasePromo            bool          `json:"can_purchase_promo,omitempty"`
	CanPurchase                 bool          `json:"can_purchase,omitempty"`
	PurchaseIneligibilityReason string        `json:"purchase_ineligibility_reason,omitempty"`
	// e.g. BLOCK_SPM
	CheckoutActions []string `json:"checkout_actions"`
}

type RecurlyParams struct {
	PublicKey           string              `json:"public_key"`
	BraintreeClientAuth string              `json:"braintree_client_authorization"`
	PayWithAmazonConfig PayWithAmazonConfig `json:"pay_with_amazon_config"`
}

type XsollaParams struct {
	URL     string `json:"url"` // to be deprecated in the future-- URL will be generated client-side
	Token   string `json:"token"`
	Sandbox bool   `json:"sandbox"`
}

type ZuoraParams struct {
	ExtAccountID  string `json:"ext_account_id"`
	Signature     string `json:"signature"`
	Token         string `json:"token"`
	PublicKey     string `json:"public_key"`
	TenantID      string `json:"tenant_id"`
	HostedPageID  string `json:"hosted_page_id"`
	HostedPageURL string `json:"hosted_page_url"`
}

type PayWithAmazonConfig struct {
	SellerID   string `json:"seller_id"`
	ClientID   string `json:"client_id"`
	Production bool   `json:"production"`
}

type BillingAuthInfo struct {
	RecurlyFraudSessionID           string `json:"recurly_fraud_session_id,omitempty"`
	ThreeDSecureActionResultTokenID string `json:"three_d_secure_action_result_token_id,omitempty"`
}

// For CompletePurchase API
type CompletePurchaseResponse struct {
	PurchaseDetails

	Error        string                       `json:"error,omitempty"`
	ErrorDetails CompletePurchaseErrorDetails `json:"error_details,omitempty"`
}

// For CompleteChanSubPurchase API
type CompleteChanSubPurchaseResponse struct {
	PurchaseDetails

	Error        string                       `json:"error,omitempty"`
	ErrorDetails CompletePurchaseErrorDetails `json:"error_details,omitempty"`
}

type CompletePurchaseErrorDetails struct {
	Country                   string `json:"country"`
	ThreeDSecureActionTokenID string `json:"three_d_secure_action_token_id,omitempty"`
}

// For GetProductChannelName API
type GetProductChannelNameResponse struct {
	ChannelName string `json:"channel_name"`
}

// For GetPaymentMethodEligibilityForUser API
type GetPaymentMethodEligibilityForUserRequest struct {
	TPLRName      string `json:"tplr_name"`
	IPCountryCode string `json:"ip_country_code"`
}

// For GetPaymentMethodEligibilityForUser API
type GetPaymentMethodEligibilityForUserResponse struct {
	EligiblePaymentMethods []EligiblePaymentMethod `json:"eligible_payment_methods"`
}

type EligiblePaymentMethod struct {
	Name               string `json:"name"`
	AvailabilityStatus string `json:"status"`
	PaymentGateway     string `json:"gateway"`
	PaymentGatewayCode string `json:"gateway_code"`
}

// For multiple API responses
type InvoiceTotal struct {
	Price    int    `json:"price"`
	Currency string `json:"currency"`
	Divisor  int    `json:"divisor"`
}

type PurchaseDetails struct {
	PaymentProvider string       `json:"payment_provider,omitempty"`
	State           string       `json:"state,omitempty"`
	InvoiceTotal    InvoiceTotal `json:"invoice_total,omitempty"`
	CancelDate      string       `json:"cancel_date,omitempty"`
	ProductType     string       `json:"product_type,omitempty"`
	ProductTier     string       `json:"product_tier"`
	OriginID        string       `json:"origin_id"`
}

// For ProcessReceipt API
type ProcessReceiptRequest struct {
	PaymentSession *PaymentSession `json:"payment_session,omitempty"`
	// Android: directly correlates to INAPP_DATA_SIGNATURE, iOS: base64 encoded receipt-data
	SignedReceipt string `json:"signed_receipt"`
	// Raw unsigned purchase data that Android provides. It is basically the unsigned version of signed_receipt
	AndroidIABReceipt string `json:"android_iab_receipt"`
	// The type of receipt. Currently supports "bits" and "sub"
	Type string `json:"type"`
	// The platform calling this API, currently only "android" or "ios"
	Platform string `json:"platform"`
	// Mobile device ID
	DeviceID string `json:"device_id"`
	// The channel category type when user made purchase
	ChannelCategory string `json:"channel_category"`
	// The value of the local price
	PurchasePrice *int `json:"purchase_price,omitempty"`
	// The currency unit of the local price
	PurchaseCurrency string `json:"purchase_currency,omitempty"`
	// Only used for iOS currently, this is the purchase transaction ID that iOS wants us to process inside the receipt
	TransactionID string `json:"transaction_id"`
	// Only used for iOS currently, this is the purchase external product ID that iOS wants us to process inside the receipt
	ExtProductID string `json:"ext_product_id"`
	// The ID of the twitch product to subscribe to.
	ProductID string `json:"product_id,omitempty"`
	// Locale used for event tracking.
	Locale string `json:"locale,omitempty"`
	// The type of gifted sub (community, single). Only valid for "sub" receipt type
	GiftType string `json:"gift_type,omitempty"`
	// The quantity of gifted subs
	Quantity int `json:"quantity,omitempty"`
	// The recipient of a gifted sub
	RecipientID string `json:"recipient_id,omitempty"`
	// The country code based on ip
	IpCountryCode string `json:"ip_country_code"`
}

type ProcessReceiptResponse struct {
	// The type of receipt. Currently supports "bits" and "sub"
	Type string `json:"type,omitempty"`
	// Current user Bits balance
	Balance int `json:"balance,omitempty"`
	// Entitlement status
	EntitlementStatus string `json:"entitlement_status,omitempty"`
	// Error
	Error string `json:"error,omitempty"`
}

type GetUnacknowledgedEventsRequest struct {
	UserID   string `json:"user_id"`
	Platform string `json:"platform"`
}

type GetUnacknowledgedEventsResponse struct {
	UnacknowledgedEvents []SubscriptionEvent `json:"unacknowledged_events"`
}

type SubscriptionEvent struct {
	ExtProductID string     `json:"ext_product_id"`
	Status       string     `json:"status"`
	ChannelName  string     `json:"channel_name"`
	EndDate      *time.Time `json:"end_date"`
}

type CancelSubscriptionRequest struct {
	PaymentSession        *PaymentSession `json:"payment_session,omitempty"`
	UserID                string          `json:"user_id"`
	Reason                string          `json:"reason"`
	CancellationDirective string          `json:"cancellation_directive"`
	BenefitsDirective     string          `json:"benefits_directive"`
	GiftOriginID          string          `json:"gift_origin_id"`
}

type CancelSubscriptionResponse struct {
	ID        string `json:"id"`
	WillRenew bool   `json:"will_renew"`
}

type CancelPurchaseOrderRequest struct {
	PaymentSession *PaymentSession `json:"payment_session,omitempty"`
	UserID         string          `json:"user_id"`
	Reason         string          `json:"reason"`
}

type CancelPurchaseOrderResponse struct {
	ID string `json:"id"`
}

type RefundPurchaseOrderRequest struct {
	PaymentSession *PaymentSession `json:"payment_session,omitempty"`
	UserID         string          `json:"user_id"`
}

type RefundPurchaseOrderResponse struct {
	ID string `json:"id"`
}

type RevokeUnacknowledgedPurchasesRequest struct {
	UnacknowledgedPurchases []UnacknowledgedPurchase `json:"unacknowledged_purchases"`
	Platform                string                   `json:"platform"`
}

type RevokeUnacknowledgedPurchasesResponse struct {
	UnacknowledgedPurchasesStatus []UnacknowledgedPurchaseStatus `json:"unacknowledged_purchases_status"`
}

type UnacknowledgedPurchase struct {
	OrderID   string `json:"order_id"`
	Token     string `json:"token"`
	ProductID string `json:"product_id"`
}

type UnacknowledgedPurchaseStatus struct {
	OrderID string `json:"order_id"`
	Status  string `json:"status"`
}

type UserResidenceRequest struct {
	PaymentSession *PaymentSession `json:"payment_session,omitempty"`
	UserID         string          `json:"user_id"`
	CountryCode    string          `json:"country_code"`
	ZipCode        string          `json:"zip_code,omitempty"`
}

type UserResidenceResponse struct {
	UserID      string `json:"user_id"`
	CountryCode string `json:"country_code"`
	ZipCode     string `json:"zip_code,omitempty"`
}

type ProductPricing struct {
	ProductID      string  `json:"product_id"`
	ProductName    string  `json:"product_name"`
	ProductPricing Pricing `json:"product_pricing"`
	TaxInclusive   bool    `json:"tax_inclusive"`
}

// All price is in cents
type Pricing struct {
	// Flo pricing ID
	Id string `json:"id"`
	// price in base price
	Price int `json:"price"`
	// currency of base price
	Currency string `json:"currency"`
	// price in localized currency
	LocalizedPrice int `json:"localized_price"`
	// localized currency
	LocalizedCurrency string `json:"localized_currency"`
	Tax               int    `json:"tax"`
	Total             int    `json:"total"`
	Exponent          int    `json:"exponent"`
}

type BitsPurchaseRequest struct {
	PaymentSession  *PaymentSession   `json:"payment_session,omitempty"`
	IpCountryCode   string            `json:"ip_country_code"`
	Quantity        int               `json:"quantity"`
	PricingID       string            `json:"pricing_id"`
	PaymentMethod   string            `json:"payment_method"`
	PaymentProvider string            `json:"payment_provider"`
	Token           string            `json:"token"`
	TenantTracking  map[string]string `json:"tenant_tracking,omitempty"`
}

// Other payments models
type PurchaseProfile struct {
	ID                   int        `json:"id"`
	State                string     `json:"state"`
	PaymentProvider      string     `json:"payment_provider"`
	ExtSubscriptionID    *string    `json:"ext_subscription_id"`
	ExtPurchaserID       *string    `json:"ext_purchaser_id"`
	TicketID             int        `json:"ticket_id"`
	PurchaserID          int        `json:"purchaser_id"`
	PurchaserName        *string    `json:"purchaser_name"`
	PurchaserEmail       string     `json:"purchaser_email"`
	CreatedOn            time.Time  `json:"created_on"`
	UpdatedOn            *time.Time `json:"updated_on"`
	WillRenew            bool       `json:"will_renew"`
	PurchaseDateTime     *time.Time `json:"purchase_datetime"`
	CancelDateTime       *time.Time `json:"cancel_datetime"`
	DoNotRenewDateTime   *time.Time `json:"do_not_renew_datetime"`
	IsPaying             bool       `json:"is_paying"`
	IsRecurring          bool       `json:"is_recurring"`
	PaidOn               *time.Time `json:"paid_on"`
	ExpiresOn            *time.Time `json:"expires_on"`
	IsRefundable         *bool      `json:"is_refundable"`
	IsExpired            *bool      `json:"is_expired"`
	LastPaymentDate      *time.Time `json:"last_payment_date"`
	IsGift               *bool      `json:"is_gift"`
	TicketProductOwnerID *int       `json:"ticket_product_owner_id"`
	ProductPrice         *int       `json:"product_price"`          // e.g. 499
	ProductPriceDivisor  *int       `json:"product_price_divisor"`  // e.g. 100
	ProductPriceCurrency *string    `json:"product_price_currency"` // e.g. "USD"
	ProductType          string     `json:"product_type"`
}

type PurchaseProfiles struct {
	PurchaseProfiles []PurchaseProfile `json:"purchase_profiles"`
}

type Subscription struct {
	ID                string     `json:"id"`
	UserID            string     `json:"user_id"`
	Platform          string     `json:"platform"`
	ProductID         string     `json:"product_id"`
	ExternalProductID string     `json:"ext_product_id"`
	IsRefundable      bool       `json:"is_refundable"`
	RenewalDate       *time.Time `json:"renewal_date"`
}

type GetSubscriptionsRequest struct {
	PaymentSession *PaymentSession `json:"payment_session,omitempty"`
	IDs            []string        `json:"ids"`
	Platform       string          `json:"platform,omitempty"`
	Page           *int            `json:"page,omitempty"`
	PerPage        *int            `json:"per_page,omitempty"`
}

type GetSubscriptionsResponse struct {
	Subscriptions []Subscription `json:"subscriptions"`
}

// Payment methods APIs
type GetPaymentMethodsResponse struct {
	PaymentMethods []PaymentMethod `json:"payment_methods"`
}

type GetXsollaMethodsResponse struct {
	PaymentMethods []PaymentMethod `json:"payment_methods"`
}

type PaymentMethod struct {
	ID                 string   `json:"id"`
	Provider           string   `json:"provider"`
	PaymentType        string   `json:"payment_type"`
	BillingInfoCountry *string  `json:"billing_info_country"`
	BillingInfoZip     *string  `json:"billing_info_zip"`
	BillingInfoState   *string  `json:"billing_info_state"`
	BillingEmail       *string  `json:"billing_email"`
	CardType           *string  `json:"card_type"`
	LastFour           *string  `json:"last_four"`
	ExpirationMonth    *int     `json:"expiration_month"`
	ExpirationYear     *int     `json:"expiration_year"`
	Enabled            *bool    `json:"enabled"`
	ExtMethodID        *string  `json:"ext_method_id"`
	PurchaseProfileIDs []string `json:"purchase_profile_ids"`
	PaymentScheme      string   `json:"payment_scheme"`
	SubscriptionIDs    []string `json:"subscription_ids"`
}

type RecurringPaymentDetail struct {
	WillRenew            bool       `json:"will_renew"`
	ExpiresAt            *time.Time `json:"expires_at"`
	TicketOwnerID        *int       `json:"ticket_owner_id"`
	TicketProductOwnerID *int       `json:"ticket_product_owner_id"`
	RenewalPrice         *int       `json:"renewal_price"`         // e.g. 499
	RenewalPriceDivisor  *int       `json:"renewal_price_divisor"` // e.g. 100
	RenewalCurrency      *string    `json:"renewal_currency"`      // e.g. "USD"
	ProductType          string     `json:"product_type"`
}

type GetPaymentMethodConfigsResponse struct {
	Recurly  RecurlyParams `json:"recurly,omitempty"`
	XsollaV3 XsollaParams  `json:"xsolla_v3,omitempty"`
	Zuora    ZuoraParams   `json:"zuora,omitempty"`
	Error    string        `json:"error,omitempty"`
}

type SetDefaultPaymentMethodRequest struct {
	PaymentSession *PaymentSession `json:"payment_session,omitempty"`
	// Twitch user ID
	UserID string `json:"user_id"`

	// "recurly" or "zuora"
	Provider string `json:"provider"`

	// If provider is recurly, then token is the Recurly token.
	// If provider is zuora, then token is the new payment method ID
	Token string `json:"token"`

	// If provider is recurly, we need gateway
	Gateway string `json:"gateway"`

	// If gateway is cc (credit card), then billing_info_country is required.
	BillingInfoCountry *string `json:"billing_info_country"`

	// Info required by payments engine and/or gateway to authenticate user
	BillingAuthInfo *BillingAuthInfo `json:"billing_auth_info,omitempty"`

	// Allows client to provide the last four digits of the account number the user entered.
	LastFour *string `json:"last_four,omitempty"`
}

type SetDefaultPaymentMethodResponse struct {
	PaymentSession *PaymentSession                     `json:"payment_session,omitempty"`
	PaymentMethod  *PaymentMethod                      `json:"payment_method"`
	UpdatedAt      *time.Time                          `json:"updated_at,omitempty"`
	ErrorCode      int                                 `json:"error_code,omitempty"`
	ErrorDetails   SetDefaultPaymentMethodErrorDetails `json:"error_details,omitempty"`
}

type SetDefaultPaymentMethodErrorDetails struct {
	ThreeDSecureActionTokenID string `json:"three_d_secure_action_token_id,omitempty"`
}

type DeleteDefaultPaymentMethodRequest struct {
	PaymentSession *PaymentSession `json:"payment_session,omitempty"`
	UserID         string          `json:"user_id"`
	// "recurly" or "zuora"
	Provider string `json:"provider"`
}

type DeleteDefaultPaymentMethodResponse struct {
	UpdatedAt *time.Time `json:"updated_at"`
}

type GetPaymentTransactionsRequest struct {
	PaymentSession      *PaymentSession `json:"payment_session,omitempty"`
	UserID              string          `json:"user_id"`
	SortKey             []string        `json:"sort_key,omitempty"`
	PurchasedBeforeDate *time.Time      `json:"purchased_before_date,omitempty"`
	PurchasedAfterDate  *time.Time      `json:"purchased_after_date,omitempty"`
	Limit               *int            `json:"limit,omitempty"`
	Offset              *int            `json:"offset,omitempty"`
	ProductType         string          `json:"product_type,omitempty"`
}

type GetPaymentTransactionsResponse struct {
	FirstPurchaseDate *time.Time `json:"first_purchase_date"`
	LastPurchaseDate  *time.Time `json:"last_purchase_date"`
	TransactionIDs    []string   `json:"transaction_ids"`
	Total             int        `json:"_total"`
}

type PaymentTransaction struct {
	ID                 string    `json:"id"`
	PurchaseOrderID    *string   `json:"purchase_order_id"`
	PurchasedAt        time.Time `json:"purchased_at"`
	ProductType        string    `json:"product_type"`
	ProductName        string    `json:"product_name"`
	ProductTier        *string   `json:"product_tier"`
	ProductOwnerID     *int      `json:"product_owner_id"`
	PaymentProvider    string    `json:"payment_provider"`
	PaymentType        *string   `json:"payment_type"`
	CardType           *string   `json:"card_type"`
	LastFour           *string   `json:"last_four"`
	GrossAmount        *int      `json:"gross_amount"`
	GrossAmountDivisor *int      `json:"gross_amount_divisor"`
	Currency           *string   `json:"currency"`
	Quantity           int       `json:"quantity"`
	IsGift             bool      `json:"is_gift"`
	RecipientUserID    *string   `json:"recipient_user_id"`
	BundleSize         int       `json:"bundle_size"`
	PaymentScheme      *string    `json:"payment_scheme,omitempty"`
}

// Update purchase order item fulfillment status
type UpdateOrderItemFulfillmentStatusRequest struct {
	PaymentSession  *PaymentSession `json:"payment_session,omitempty"`
	PurchaseOrderID string          `json:"purchase_order_id"`
	Status          string          `json:"status"`
	RecipientID     string          `json:"recipient_id"`
	NthItem         int             `json:"nth_item"`
	EntitlementID   string          `json:"entitlement_id"`
}

type UpdateOrderItemFulfillmentStatusResponse struct {
	ItemFulfillmentID string `json:"item_fulfillment_id"`
	Status            string `json:"status"`
}

// Admin Mass User Mass DNR
type AdminUserMassDNRRequest struct {
	PaymentSession *PaymentSession `json:"payment_session,omitempty"`
	UserID         string          `json:"user_id"`
}

type AdminUserMassDNRResponse struct{}

// Payment Notifications
type ProcessPaymentNotificationRequest struct {
	PaymentSession *PaymentSession `json:"payment_session,omitempty"`
	PaymentGateway string
	RequestBody    string
}

type ProcessPaymentNotificationResponse struct {
	ResponseBody map[string]interface{}
}

// Bits Payment Methods Response
type BitsPaymentMethodsResponse struct {
	PaymentMethods []string `json:"payment_methods"`
}

// Preview Bits Purchase
type PreviewBitsPurchaseRequest struct {
	PaymentSession  *PaymentSession `json:"payment_session,omitempty"`
	UserId          string          `json:"user_id"`
	ProductId       string          `json:"product_id"`
	IpCountryCode   string          `json:"ip_country_code"`
	PricingId       string          `json:"pricing_id"`
	PaymentMethod   string          `json:"payment_method"`
	PaymentProvider string          `json:"payment_provider"`
	Quantity        int             `json:"quantity"`
}

// Preview Bits Purchase Response. Moved the "ProductPricing" fields into this new object as a refactor.
type PreviewBitsPurchaseResponse struct {
	ProductName    string  `json:"product_name"`
	ProductPricing Pricing `json:"product_pricing"`
	TaxInclusive   bool    `json:"tax_inclusive"`
	// e.g. BLOCK_SPM
	CheckoutActions []string `json:"checkout_actions"`
}

type ReactivateInfo struct {
	Recurring bool `json:"recurring"`
}

// GetCheckoutActions
type GetCheckoutActionsRequest struct {
	PaymentSession   *PaymentSession `json:"payment_session,omitempty"`
	PurchaserID      string          `json:"purchaser_id"`
	ProductType      string          `json:"product_type"`
	PaymentProvider  string          `json:"payment_provider"`
	TransactionPrice int             `json:"transaction_price"`
	Currency         string          `json:"currency"`
	TPLRName         string          `json:"tplr_name"`
	Checkpoint       string          `json:"checkpoint"`
	ProductID        string          `json:"product_id"`
}

type GetCheckoutActionsResponse struct {
	Actions []string `json:"actions"`
}

// PaymentMethodPreauth model for Bits Autorefill
type GetPaymentInstrumentPreauthsRequest struct {
	PaymentInstrumentID       string `json:"payment_instrument_id"` // UUID
	PaymentInstrumentCategory string `json:"payment_instrument_category"`
	UserID                    string `json:"user_id"`
}

type GetPaymentInstrumentPreauthsResponse struct {
	Preauthorizations []PaymentInstrumentPreauthorization `json:"preauthorizations"`
	Error             string                              `json:"error"`
}

type PaymentInstrumentPreauthorization struct {
	PaymentInstrumentID string    `json:"payment_instrument_id"` // UUID
	CreatedAt           time.Time `json:"created_at"`
	GrossAmount         int       `json:"gross_amount"`
	Currency            string    `json:"currency"`
	IsActive            bool      `json:"is_active"`
}

type CreatePaymentInstrumentPreauthRequest struct {
	PaymentInstrumentID       string `json:"payment_instrument_id"`       // PaymentMethod.UUID
	PaymentInstrumentCategory string `json:"payment_instrument_category"` // TWITCH_PAYMENT_INSTRUMENT
	UserID                    string `json:"user_id"`
	GrossAmount               int    `json:"gross_amount"`
	Currency                  string `json:"currency"`
}

type CreatePaymentInstrumentPreauthResponse struct {
	Error string `json:"error"`
}
