// Package payments contains a client for the Rails payments-service service intended for use by Visage and possibly
// other internal twitch services. It also contains shared models for response objects.
package payments

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/twitchtv/twirp"

	"code.justin.tv/foundation/twitchclient"
)

const (
	defaultStatSampleRate = 0.1
	defaultTimingXactName = "payments-service-rails"
)

type Client interface {
	// For the checkout flow on web-client
	GetPurchase(ctx context.Context, userID string, productShortName string, reqOpts *twitchclient.ReqOpts) (*GetPurchaseResponse, error)
	GetInitiatePurchase(ctx context.Context, userID string, productShortName string, request InitiatePurchaseRequest, reqOpts *twitchclient.ReqOpts) (*GetInitiatePurchaseResponse, error)
	CompletePurchase(ctx context.Context, userID string, productShortName string, request CompletePurchaseRequest, reqOpts *twitchclient.ReqOpts) (*CompletePurchaseResponse, error)
	CompleteChanSubPurchase(ctx context.Context, userID string, productShortName string, request CompleteChanSubPurchaseRequest, reqOpts *twitchclient.ReqOpts) (*CompleteChanSubPurchaseResponse, error)
	CancelPurchase(ctx context.Context, userID string, productShortName string, request CancelPurchaseRequest, reqOpts *twitchclient.ReqOpts) error
	DoNotRenewPurchase(ctx context.Context, userID string, productShortName string, reason string, reqOpts *twitchclient.ReqOpts) error
	GetProductChannelName(ctx context.Context, productShortName string, reqOpts *twitchclient.ReqOpts) (*GetProductChannelNameResponse, error)
	GetPaymentMethodEligibilityForUser(ctx context.Context, userID string, request GetPaymentMethodEligibilityForUserRequest, reqOpts *twitchclient.ReqOpts) (*GetPaymentMethodEligibilityForUserResponse, error)

	// For PayPal Bits checkout
	GetUserResidence(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*UserResidenceResponse, error)
	SetUserResidence(ctx context.Context, userID string, request UserResidenceRequest, reqOpts *twitchclient.ReqOpts) (*UserResidenceResponse, error)
	GetBitsPaymentMethods(ctx context.Context, userID string, IpCountryCode string, reqOpts *twitchclient.ReqOpts) (*BitsPaymentMethodsResponse, error)
	PreviewBitsPurchase(ctx context.Context, request PreviewBitsPurchaseRequest, reqOpts *twitchclient.ReqOpts) (*PreviewBitsPurchaseResponse, error)
	PurchaseBits(ctx context.Context, userID string, productID string, request BitsPurchaseRequest, reqOpts *twitchclient.ReqOpts) error
	GetProductsPrice(ctx context.Context, userID string, ipCountryCode string, productIDs []string, reqOpts *twitchclient.ReqOpts) ([]ProductPricing, error)
	GetLocalizedPrices(ctx context.Context, countryCode string, productIDs []string, reqOpts *twitchclient.ReqOpts) ([]ProductPricing, error)

	// For Mobile purchases
	ProcessReceipt(ctx context.Context, userID string, request ProcessReceiptRequest, reqOpts *twitchclient.ReqOpts) (*ProcessReceiptResponse, error)
	GetUnacknowledgedEvents(ctx context.Context, request GetUnacknowledgedEventsRequest, reqOpts *twitchclient.ReqOpts) (*GetUnacknowledgedEventsResponse, error)
	CancelSubscription(ctx context.Context, id string, request CancelSubscriptionRequest, reqOpts *twitchclient.ReqOpts) (*CancelSubscriptionResponse, error)
	CancelPurchaseOrder(ctx context.Context, id string, request CancelPurchaseOrderRequest, reqOpts *twitchclient.ReqOpts) (*CancelPurchaseOrderResponse, error)
	RefundPurchaseOrder(ctx context.Context, id string, request RefundPurchaseOrderRequest, reqOpts *twitchclient.ReqOpts) (*RefundPurchaseOrderResponse, error)
	RevokeUnacknowledgedPurchases(ctx context.Context, userID string, request RevokeUnacknowledgedPurchasesRequest, reqOpts *twitchclient.ReqOpts) (*RevokeUnacknowledgedPurchasesResponse, error)

	// For payments management page
	GetPaymentMethods(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*GetPaymentMethodsResponse, error)
	GetPaymentMethodConfigs(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*GetPaymentMethodConfigsResponse, error)
	SetDefaultPaymentMethod(ctx context.Context, request SetDefaultPaymentMethodRequest, reqOpts *twitchclient.ReqOpts) (*SetDefaultPaymentMethodResponse, error)
	DeleteDefaultPaymentMethod(ctx context.Context, request DeleteDefaultPaymentMethodRequest, reqOpts *twitchclient.ReqOpts) (*DeleteDefaultPaymentMethodResponse, error)
	GetPaymentTransactionIDs(ctx context.Context, request GetPaymentTransactionsRequest, reqOpts *twitchclient.ReqOpts) (*GetPaymentTransactionsResponse, error)
	GetPaymentTransactionByID(ctx context.Context, id string, reqOpts *twitchclient.ReqOpts) (*PaymentTransaction, error)
	GetRecurringPaymentDetail(ctx context.Context, subscription_id string, reqOpts *twitchclient.ReqOpts) (*RecurringPaymentDetail, error)
	GetXsollaMethods(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*GetXsollaMethodsResponse, error)

	// Various Payments DB models
	GetPurchaseProfile(ctx context.Context, purchaseProfileID string, extraKeys []string, reqOpts *twitchclient.ReqOpts) (*PurchaseProfile, error)
	GetPurchaseProfilesByPurchasableIDs(ctx context.Context, purchasableIDs []string, reqOpts *twitchclient.ReqOpts) (*PurchaseProfiles, error)
	GetSubscriptions(ctx context.Context, request GetSubscriptionsRequest, reqOpts *twitchclient.ReqOpts) (*GetSubscriptionsResponse, error)

	// For Mass Mystery Gifting - order fulfillment updates
	UpdateOrderItemFulfillmentStatus(ctx context.Context, request UpdateOrderItemFulfillmentStatusRequest, reqOpts *twitchclient.ReqOpts) (*UpdateOrderItemFulfillmentStatusResponse, error)

	// Admin APIs
	AdminUserMassDNR(ctx context.Context, request AdminUserMassDNRRequest, reqOpts *twitchclient.ReqOpts) (*AdminUserMassDNRResponse, error)

	// Payment Notifications (for migration to bubbles)
	ProcessPaymentNotification(ctx context.Context, request ProcessPaymentNotificationRequest, reqOpts *twitchclient.ReqOpts) (*ProcessPaymentNotificationResponse, error)

	// Fraud
	GetCheckoutActions(ctx context.Context, request GetCheckoutActionsRequest, reqOpts *twitchclient.ReqOpts) (*GetCheckoutActionsResponse, error)

	// Bits Autorefill
	GetPaymentInstrumentPreauthorizations(ctx context.Context, request GetPaymentInstrumentPreauthsRequest, opts *twitchclient.ReqOpts) (*GetPaymentInstrumentPreauthsResponse, error)
	CreatePaymentInstrumentPreauthorization(ctx context.Context, request CreatePaymentInstrumentPreauthRequest, opts *twitchclient.ReqOpts) (*CreatePaymentInstrumentPreauthResponse, error)

	// Internal APIs
	InternalCompleteChanSubPurchase(ctx context.Context, userID string, ticketProductID string, request CompleteChanSubPurchaseRequest, reqOpts *twitchclient.ReqOpts) (*CompleteChanSubPurchaseResponse, error)
	InternalGetPaymentMethodEligibilityForUser(ctx context.Context, userID string, request GetPaymentMethodEligibilityForUserRequest, reqOpts *twitchclient.ReqOpts) (*GetPaymentMethodEligibilityForUserResponse, error)
}

type client struct {
	twitchclient.Client
}

// NewClient creates a new client for use in making service calls.
func NewClient(conf twitchclient.ClientConf) (Client, error) {
	if conf.TimingXactName == "" {
		conf.TimingXactName = defaultTimingXactName
	}
	twitchClient, err := twitchclient.NewClient(conf)
	return &client{twitchClient}, err
}

// cloneHeader mimics the http.Header.Clone() method introduced in golang 1.13
// Since some of our services are not running 1.13 we have to introduce this.
func cloneHeader(h http.Header) http.Header {
	if h == nil {
		return nil
	}

	// Find total number of values.
	nv := 0
	for _, vv := range h {
		nv += len(vv)
	}
	sv := make([]string, nv) // shared backing array for headers' values
	h2 := make(http.Header, len(h))
	for k, vv := range h {
		n := copy(sv, vv)
		h2[k] = sv[:n:n]
		sv = sv[n:]
	}
	return h2
}

func (c *client) GetPurchase(ctx context.Context, userID string, productShortName string, reqOpts *twitchclient.ReqOpts) (*GetPurchaseResponse, error) {
	apiURL := fmt.Sprintf("/users/%s/products/%s/purchase", userID, productShortName)

	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_purchase",
		StatSampleRate: defaultStatSampleRate,
	})

	var getPurchaseResponse GetPurchaseResponse
	_, err = c.DoJSON(ctx, &getPurchaseResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getPurchaseResponse, nil
}

func (c *client) GetInitiatePurchase(ctx context.Context, userID string, productShortName string, request InitiatePurchaseRequest, reqOpts *twitchclient.ReqOpts) (*GetInitiatePurchaseResponse, error) {
	query := url.Values{}
	query.Add("platform", request.Platform)
	query.Add("is_gift", strconv.FormatBool(request.IsGift))
	query.Add("recipient_id", request.RecipientID)
	query.Add("with_special_promo", request.WithSpecialPromo)
	query.Add("is_anon", strconv.FormatBool(request.IsAnon))

	if request.MysteryGiftCount != nil {
		query.Add("mystery_gift_count", strconv.Itoa(*request.MysteryGiftCount))
	}
	if request.TaxCountryCode != nil {
		query.Add("tax_country_code", *request.TaxCountryCode)
	}
	if request.IPCountryCode != nil {
		query.Add("ip_country_code", *request.IPCountryCode)
	}
	if request.Reactivate != nil {
		query.Add("reactivate_recurring", strconv.FormatBool(request.Reactivate.Recurring))
	}
	if request.PaymentSession != nil {
		query.Add("device_id", request.PaymentSession.DeviceID)
		query.Add("localstorage_device_id", request.PaymentSession.LocalStorageDeviceID)
		query.Add("tab_session_id", request.PaymentSession.TabSessionID)
		query.Add("page_session_id", request.PaymentSession.PageSessionID)
		query.Add("session_ip_address", request.PaymentSession.SessionIPAddress)
		query.Add("checkout_session_id", request.PaymentSession.CheckoutSessionID)
		if request.PaymentSession.LocationData != nil {
			query.Add("session_country_code", request.PaymentSession.LocationData.CountryCode)
			query.Add("session_city", request.PaymentSession.LocationData.City)
			query.Add("session_postal_code", request.PaymentSession.LocationData.PostalCode)
			query.Add("session_is_anonymous", strconv.FormatBool(request.PaymentSession.LocationData.IsAnonymous))
		}
	}

	apiURL := fmt.Sprintf("/users/%s/products/%s/purchase/initiate?%s", userID, productShortName, query.Encode())

	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_initiate_purchase",
		StatSampleRate: defaultStatSampleRate,
	})

	var getInitiatePurchaseResponse GetInitiatePurchaseResponse
	_, err = c.DoJSON(ctx, &getInitiatePurchaseResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getInitiatePurchaseResponse, nil
}

func (c *client) CompletePurchase(ctx context.Context, userID string, productShortName string, request CompletePurchaseRequest, reqOpts *twitchclient.ReqOpts) (*CompletePurchaseResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	apiURL := fmt.Sprintf("/users/%s/products/%s/purchase/complete", userID, productShortName)

	req, err := c.NewRequest("POST", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.complete_purchase",
		StatSampleRate: defaultStatSampleRate,
	})

	var completePurchaseResponse CompletePurchaseResponse
	_, err = c.DoJSON(ctx, &completePurchaseResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &completePurchaseResponse, nil
}

func (c *client) CompleteChanSubPurchase(ctx context.Context, userID string, productShortName string, request CompleteChanSubPurchaseRequest, reqOpts *twitchclient.ReqOpts) (*CompleteChanSubPurchaseResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	apiURL := fmt.Sprintf("/users/%s/products/%s/purchase/complete_chansub_purchase", userID, productShortName)

	req, err := c.NewRequest("POST", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.complete_chansub_purchase",
		StatSampleRate: defaultStatSampleRate,
	})

	var completeChanSubPurchaseResponse CompleteChanSubPurchaseResponse
	_, err = c.DoJSON(ctx, &completeChanSubPurchaseResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &completeChanSubPurchaseResponse, nil
}

func (c *client) InternalCompleteChanSubPurchase(ctx context.Context, userID string, ticketProductID string, request CompleteChanSubPurchaseRequest, reqOpts *twitchclient.ReqOpts) (*CompleteChanSubPurchaseResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	apiURL := fmt.Sprintf("/internal/users/%s/products/%s/purchase/complete_chansub_purchase", userID, ticketProductID)

	req, err := c.NewRequest("POST", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.complete_chansub_purchase",
		StatSampleRate: defaultStatSampleRate,
	})

	var completeChanSubPurchaseResponse CompleteChanSubPurchaseResponse
	_, err = c.DoJSON(ctx, &completeChanSubPurchaseResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &completeChanSubPurchaseResponse, nil
}

func (c *client) CancelPurchase(ctx context.Context, userID string, productShortName string, request CancelPurchaseRequest, reqOpts *twitchclient.ReqOpts) error {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return err
	}

	apiURL := fmt.Sprintf("/users/%s/products/%s/purchase/cancel", userID, productShortName)

	req, err := c.NewRequest("PUT", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.cancel_purchase",
		StatSampleRate: defaultStatSampleRate,
	})

	var cancelPurchaseResponse map[string]interface{}
	_, err = c.DoJSON(ctx, &cancelPurchaseResponse, req, combinedReqOpts)
	if err != nil {
		return err
	}

	return nil
}

func (c *client) DoNotRenewPurchase(ctx context.Context, userID string, productShortName string, reason string, reqOpts *twitchclient.ReqOpts) error {
	body := make(map[string]string)
	if reason != "" {
		body["reason"] = reason
	}

	jsonBody, err := json.Marshal(body)
	if err != nil {
		return err
	}

	apiURL := fmt.Sprintf("/users/%s/products/%s/purchase/do_not_renew", userID, productShortName)

	req, err := c.NewRequest("PUT", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.do_not_renew_purchase",
		StatSampleRate: defaultStatSampleRate,
	})

	var doNotRenewPurchaseResponse map[string]interface{}
	_, err = c.DoJSON(ctx, &doNotRenewPurchaseResponse, req, combinedReqOpts)
	if err != nil {
		return err
	}

	return nil
}

func (c *client) GetProductChannelName(ctx context.Context, productShortName string, reqOpts *twitchclient.ReqOpts) (*GetProductChannelNameResponse, error) {
	apiURL := fmt.Sprintf("/products/%s/channel_name", productShortName)
	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_product_channel_name",
		StatSampleRate: defaultStatSampleRate,
	})

	var getProductChannelNameResponse GetProductChannelNameResponse
	_, err = c.DoJSON(ctx, &getProductChannelNameResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getProductChannelNameResponse, nil
}

func (c *client) GetPaymentMethodEligibilityForUser(ctx context.Context, userID string, request GetPaymentMethodEligibilityForUserRequest, reqOpts *twitchclient.ReqOpts) (*GetPaymentMethodEligibilityForUserResponse, error) {
	query := url.Values{}
	query.Add("tplr_name", request.TPLRName)
	query.Add("ip_country_code", request.IPCountryCode)

	apiURL := fmt.Sprintf("/users/%s/eligible_payment_methods?%s", userID, query.Encode())
	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_payment_method_eligibility_for_user",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp GetPaymentMethodEligibilityForUserResponse
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) InternalGetPaymentMethodEligibilityForUser(ctx context.Context, userID string, request GetPaymentMethodEligibilityForUserRequest, reqOpts *twitchclient.ReqOpts) (*GetPaymentMethodEligibilityForUserResponse, error) {
	query := url.Values{}
	query.Add("tplr_name", request.TPLRName)
	query.Add("ip_country_code", request.IPCountryCode)

	apiURL := fmt.Sprintf("/internal/users/%s/eligible_payment_methods?%s", userID, query.Encode())
	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.internal_get_payment_method_eligibility_for_user",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp GetPaymentMethodEligibilityForUserResponse
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) ProcessReceipt(ctx context.Context, userID string, request ProcessReceiptRequest, reqOpts *twitchclient.ReqOpts) (*ProcessReceiptResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	apiURL := fmt.Sprintf("/users/%s/receipt", userID)
	req, err := c.NewRequest("POST", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.process_receipt",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp ProcessReceiptResponse
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) GetUnacknowledgedEvents(ctx context.Context, request GetUnacknowledgedEventsRequest, reqOpts *twitchclient.ReqOpts) (*GetUnacknowledgedEventsResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	apiURL := "/subscriptions/unacknowledged"
	req, err := c.NewRequest("GET", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.unacknowledged_events",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp GetUnacknowledgedEventsResponse
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) CancelSubscription(ctx context.Context, id string, request CancelSubscriptionRequest, reqOpts *twitchclient.ReqOpts) (*CancelSubscriptionResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	apiURL := fmt.Sprintf("/subscriptions/%s/cancel", id)
	req, err := c.NewRequest("PUT", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.cancel_subscriptions",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp CancelSubscriptionResponse
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) CancelPurchaseOrder(ctx context.Context, id string, request CancelPurchaseOrderRequest, reqOpts *twitchclient.ReqOpts) (*CancelPurchaseOrderResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	apiURL := fmt.Sprintf("/purchase_orders/%s/cancel", id)
	req, err := c.NewRequest("PUT", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.cancel_purchase_orders",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp CancelPurchaseOrderResponse
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) RefundPurchaseOrder(ctx context.Context, id string, request RefundPurchaseOrderRequest, reqOpts *twitchclient.ReqOpts) (*RefundPurchaseOrderResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	apiURL := fmt.Sprintf("/purchase_orders/%s/refund", id)
	req, err := c.NewRequest("PUT", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.refund_purchase_orders",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp RefundPurchaseOrderResponse
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) RevokeUnacknowledgedPurchases(ctx context.Context, userID string, request RevokeUnacknowledgedPurchasesRequest, reqOpts *twitchclient.ReqOpts) (*RevokeUnacknowledgedPurchasesResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	apiURL := fmt.Sprintf("/users/%s/receipt/revoke", userID)
	req, err := c.NewRequest("POST", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.revoke_unacknowledged_purchases",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp RevokeUnacknowledgedPurchasesResponse
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) GetUserResidence(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*UserResidenceResponse, error) {
	apiURL := fmt.Sprintf("/users/%s/residence", userID)
	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_user_residence",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp UserResidenceResponse
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) SetUserResidence(ctx context.Context, userID string, request UserResidenceRequest, reqOpts *twitchclient.ReqOpts) (*UserResidenceResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	apiURL := fmt.Sprintf("/users/%s/residence", userID)
	req, err := c.NewRequest("PUT", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.set_user_residence",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp UserResidenceResponse
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) GetBitsPaymentMethods(ctx context.Context, userID string, IpCountryCode string, reqOpts *twitchclient.ReqOpts) (*BitsPaymentMethodsResponse, error) {
	query := url.Values{}
	query.Set("ip_country_code", IpCountryCode)

	apiURL := fmt.Sprintf("/users/%s/bits_payment_methods?%s", userID, query.Encode())

	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_bits_payment_methods",
		StatSampleRate: defaultStatSampleRate,
	})

	var response BitsPaymentMethodsResponse
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func (c *client) PreviewBitsPurchase(ctx context.Context, request PreviewBitsPurchaseRequest, reqOpts *twitchclient.ReqOpts) (*PreviewBitsPurchaseResponse, error) {
	query := url.Values{}
	query.Set("ip_country_code", request.IpCountryCode)
	query.Set("pricing_id", request.PricingId)
	query.Set("payment_method", request.PaymentMethod)
	query.Set("payment_provider", request.PaymentProvider)
	query.Set("quantity", strconv.Itoa(request.Quantity))
	if request.PaymentSession != nil {
		query.Add("device_id", request.PaymentSession.DeviceID)
		query.Add("localstorage_device_id", request.PaymentSession.LocalStorageDeviceID)
		query.Add("tab_session_id", request.PaymentSession.TabSessionID)
		query.Add("page_session_id", request.PaymentSession.PageSessionID)
		query.Add("session_ip_address", request.PaymentSession.SessionIPAddress)
		query.Add("checkout_session_id", request.PaymentSession.CheckoutSessionID)
		if request.PaymentSession.LocationData != nil {
			query.Add("session_country_code", request.PaymentSession.LocationData.CountryCode)
			query.Add("session_city", request.PaymentSession.LocationData.City)
			query.Add("session_postal_code", request.PaymentSession.LocationData.PostalCode)
			query.Add("session_is_anonymous", strconv.FormatBool(request.PaymentSession.LocationData.IsAnonymous))
		}
	}

	apiURL := fmt.Sprintf("/users/%s/products/%s/preview_purchase?%s", request.UserId, request.ProductId, query.Encode())

	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.preview_bits_purchase",
		StatSampleRate: defaultStatSampleRate,
	})

	var response PreviewBitsPurchaseResponse
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func (c *client) PurchaseBits(ctx context.Context, userID string, productID string, request BitsPurchaseRequest, reqOpts *twitchclient.ReqOpts) error {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return err
	}

	apiURL := fmt.Sprintf("/users/%s/products/%s/bits_purchase", userID, productID)

	req, err := c.NewRequest("POST", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.bits_purchase",
		StatSampleRate: defaultStatSampleRate,
	})

	var response map[string]interface{}
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return err
	}

	return nil
}

func (c *client) GetProductsPrice(ctx context.Context, userID string, ipCountryCode string, productIDs []string, reqOpts *twitchclient.ReqOpts) ([]ProductPricing, error) {
	query := url.Values{}
	query.Set("ip_country_code", ipCountryCode)
	for i := range productIDs {
		query.Add("product_ids[]", productIDs[i])
	}

	apiURL := fmt.Sprintf("/users/%s/products_price?%s", userID, query.Encode())

	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_products_price",
		StatSampleRate: defaultStatSampleRate,
	})

	response := make([]ProductPricing, len(productIDs))
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (c *client) GetLocalizedPrices(ctx context.Context, countryCode string, productIDs []string, reqOpts *twitchclient.ReqOpts) ([]ProductPricing, error) {
	query := url.Values{}
	query.Set("country_code", countryCode)
	for i := range productIDs {
		query.Add("product_ids[]", productIDs[i])
	}

	apiURL := fmt.Sprintf("/localized_prices?%s", query.Encode())

	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_localized_prices",
		StatSampleRate: defaultStatSampleRate,
	})

	response := make([]ProductPricing, len(productIDs))
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (c *client) GetPaymentMethods(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*GetPaymentMethodsResponse, error) {
	query := url.Values{}
	query.Set("user_id", userID)

	apiURL := fmt.Sprintf("/payment_methods?%s", query.Encode())

	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_payment_methods",
		StatSampleRate: defaultStatSampleRate,
	})

	var response GetPaymentMethodsResponse
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func (c *client) GetPaymentMethodConfigs(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*GetPaymentMethodConfigsResponse, error) {
	query := url.Values{}
	query.Set("user_id", userID)

	apiURL := fmt.Sprintf("/payment_methods/configs?%s", query.Encode())

	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_payment_method_configs",
		StatSampleRate: defaultStatSampleRate,
	})

	var response GetPaymentMethodConfigsResponse
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func (c *client) SetDefaultPaymentMethod(ctx context.Context, request SetDefaultPaymentMethodRequest, reqOpts *twitchclient.ReqOpts) (*SetDefaultPaymentMethodResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("PUT", "/payment_methods", bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.set_default_payment_method",
		StatSampleRate: defaultStatSampleRate,
	})

	var res SetDefaultPaymentMethodResponse
	_, err = c.DoJSON(ctx, &res, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (c *client) DeleteDefaultPaymentMethod(ctx context.Context, request DeleteDefaultPaymentMethodRequest, reqOpts *twitchclient.ReqOpts) (*DeleteDefaultPaymentMethodResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("DELETE", "/payment_methods", bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}
	req.Header.Set("Content-Type", "application/json")

	combineReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.delete_default_payment_method",
		StatSampleRate: defaultStatSampleRate,
	})

	var res DeleteDefaultPaymentMethodResponse
	_, err = c.DoJSON(ctx, &res, req, combineReqOpts)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (c *client) GetPaymentTransactionIDs(ctx context.Context, request GetPaymentTransactionsRequest, reqOpts *twitchclient.ReqOpts) (*GetPaymentTransactionsResponse, error) {
	query := url.Values{}
	query.Set("user_id", request.UserID)
	query.Set("sort_key", strings.Join(request.SortKey, ","))

	if request.PurchasedBeforeDate != nil {
		query.Set("purchased_before_date", request.PurchasedBeforeDate.Format(time.RFC3339Nano))
	}

	if request.PurchasedAfterDate != nil {
		query.Set("purchased_after_date", request.PurchasedAfterDate.Format(time.RFC3339Nano))
	}

	if request.Limit != nil {
		query.Set("limit", strconv.Itoa(*request.Limit))
	}

	if request.Offset != nil {
		query.Set("offset", strconv.Itoa(*request.Offset))
	}

	if request.ProductType != "" {
		query.Set("product_type", request.ProductType)
	}

	apiURL := fmt.Sprintf("/transactions?%s", query.Encode())

	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_payment_transactions",
		StatSampleRate: defaultStatSampleRate,
	})

	var response GetPaymentTransactionsResponse
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func (c *client) GetPaymentTransactionByID(ctx context.Context, id string, reqOpts *twitchclient.ReqOpts) (*PaymentTransaction, error) {
	apiURL := fmt.Sprintf("/transactions/%s", id)
	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_payment_transaction_by_id",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp PaymentTransaction
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) GetRecurringPaymentDetail(ctx context.Context, subscription_id string, reqOpts *twitchclient.ReqOpts) (*RecurringPaymentDetail, error) {
	query := url.Values{}
	query.Set("subscription_id", subscription_id)

	apiURL := fmt.Sprintf("/payment_methods/recurring_payment_detail?%s", query.Encode())
	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_recurring_payment_detail",
		StatSampleRate: defaultStatSampleRate,
	})

	var response RecurringPaymentDetail
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func (c *client) GetXsollaMethods(ctx context.Context, user_id string, reqOpts *twitchclient.ReqOpts) (*GetXsollaMethodsResponse, error) {
	query := url.Values{}
	query.Set("user_id", user_id)

	apiURL := fmt.Sprintf("/payment_methods/xsolla?%s", query.Encode())

	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_xsolla_methods",
		StatSampleRate: defaultStatSampleRate,
	})

	var response GetXsollaMethodsResponse
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func (c *client) GetPurchaseProfile(ctx context.Context, purchaseProfileID string, extraKeys []string, reqOpts *twitchclient.ReqOpts) (*PurchaseProfile, error) {
	query := url.Values{}

	if len(extraKeys) > 0 {
		query.Set("extra_keys", strings.Join(extraKeys, ","))
	}

	apiURL := fmt.Sprintf("/purchase_profiles/%s?%s", purchaseProfileID, query.Encode())
	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_purchase_profile",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp PurchaseProfile
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) GetPurchaseProfilesByPurchasableIDs(ctx context.Context, purchasableIDs []string, reqOpts *twitchclient.ReqOpts) (*PurchaseProfiles, error) {
	type reqBody struct {
		PurchasableIDs []string `json:"purchasable_id"`
		FullDetails    bool     `json:"full_details"`
		PerPage        string   `json:"per_page"`
	}

	jsonBody, err := json.Marshal(reqBody{
		PurchasableIDs: purchasableIDs,
		FullDetails:    true,
		PerPage:        "all",
	})
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("POST", "/query/purchase_profiles", bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.query_purchase_profiles",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp PurchaseProfiles
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *client) GetSubscriptions(ctx context.Context, request GetSubscriptionsRequest, reqOpts *twitchclient.ReqOpts) (*GetSubscriptionsResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("POST", "/internal/subscriptions/query", bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_payments_subscriptions",
		StatSampleRate: defaultStatSampleRate,
	})

	var res GetSubscriptionsResponse
	_, err = c.DoJSON(ctx, &res, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (c *client) UpdateOrderItemFulfillmentStatus(ctx context.Context, request UpdateOrderItemFulfillmentStatusRequest, reqOpts *twitchclient.ReqOpts) (*UpdateOrderItemFulfillmentStatusResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest(
		"PUT",
		fmt.Sprintf("/purchase_orders/%s/item_status", request.PurchaseOrderID),
		bytes.NewBuffer(jsonBody),
	)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.update_order_line_fulfillment_status",
		StatSampleRate: defaultStatSampleRate,
	})

	var res UpdateOrderItemFulfillmentStatusResponse
	_, err = c.DoJSON(ctx, &res, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (c *client) AdminUserMassDNR(ctx context.Context, request AdminUserMassDNRRequest, reqOpts *twitchclient.ReqOpts) (*AdminUserMassDNRResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest(
		"POST",
		fmt.Sprintf("/admin/users/%s/mass_dnr", request.UserID),
		bytes.NewBuffer(jsonBody),
	)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.user_mass_dnr",
		StatSampleRate: defaultStatSampleRate,
	})

	var res AdminUserMassDNRResponse
	_, err = c.DoJSON(ctx, &res, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (c *client) ProcessPaymentNotification(ctx context.Context, request ProcessPaymentNotificationRequest, reqOpts *twitchclient.ReqOpts) (*ProcessPaymentNotificationResponse, error) {
	req, err := c.NewRequest(
		"POST",
		fmt.Sprintf("/notifications/%s", request.PaymentGateway),
		bytes.NewBuffer([]byte(request.RequestBody)),
	)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}

	req.Header.Set("Content-Type", "application/json")
	if strings.ToLower(request.PaymentGateway) == "recurly" {
		req.Header.Set("Content-Type", "application/xml")
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.process_payment_notification",
		StatSampleRate: defaultStatSampleRate,
	})

	// Payments service seems to return different JSON structure depending on which payment gateway is being used
	var res map[string]interface{}
	_, err = c.DoJSON(ctx, &res, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &ProcessPaymentNotificationResponse{
		ResponseBody: res,
	}, nil
}

func (c *client) GetCheckoutActions(ctx context.Context, request GetCheckoutActionsRequest, reqOpts *twitchclient.ReqOpts) (*GetCheckoutActionsResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest(
		"POST",
		fmt.Sprintf("/fraud/checkout_actions"),
		bytes.NewBuffer(jsonBody),
	)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}

	req.Header.Set("Content-Type", "application/json")
	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_checkout_actions",
		StatSampleRate: defaultStatSampleRate,
	})

	var res GetCheckoutActionsResponse
	_, err = c.DoJSON(ctx, &res, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (c *client) GetPaymentInstrumentPreauthorizations(ctx context.Context, request GetPaymentInstrumentPreauthsRequest, reqOpts *twitchclient.ReqOpts) (*GetPaymentInstrumentPreauthsResponse, error) {
	query := url.Values{}
	query.Set("payment_instrument_category", request.PaymentInstrumentCategory)

	apiURL := fmt.Sprintf("/users/%s/payment_instruments/%s/preauth?%s", request.UserID, request.PaymentInstrumentID, query.Encode())

	req, err := c.NewRequest("GET", apiURL, nil)
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.get_payment_instrument_preauthorizations",
		StatSampleRate: defaultStatSampleRate,
	})

	var response GetPaymentInstrumentPreauthsResponse
	_, err = c.DoJSON(ctx, &response, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func (c *client) CreatePaymentInstrumentPreauthorization(ctx context.Context, request CreatePaymentInstrumentPreauthRequest, reqOpts *twitchclient.ReqOpts) (*CreatePaymentInstrumentPreauthResponse, error) {
	jsonBody, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	apiURL := fmt.Sprintf("/users/%s/payment_instruments/%s/preauth", request.UserID, request.PaymentInstrumentID)
	req, err := c.NewRequest("POST", apiURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, err
	}

	if h, ok := twirp.HTTPRequestHeaders(ctx); ok {
		req.Header = cloneHeader(h)
	}
	req.Header.Set("Content-Type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payments_service.create_payment_instrument_preauthorization",
		StatSampleRate: defaultStatSampleRate,
	})

	var resp CreatePaymentInstrumentPreauthResponse
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}
