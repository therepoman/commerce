package iamgenerator

type stringSet map[string]interface{}

func newStringSet(values ...string) stringSet {
	ss := make(stringSet)
	ss.AddAll(values)
	return ss
}

func (ss stringSet) contains(value string) bool {
	_, ok := ss[value]
	return ok
}

func (ss stringSet) add(value string) {
	ss[value] = nil
}

func (ss stringSet) AddAll(values []string) {
	for _, value := range values {
		ss.add(value)
	}
}

func (ss stringSet) RemoveAll(values []string) {
	for _, value := range values {
		delete(ss, value)
	}
}

func (ss stringSet) Intersection(values []string) stringSet {
	intersection := newStringSet()
	for _, value := range values {
		if ss.contains(value) {
			intersection.add(value)
		}
	}
	return intersection
}

func (ss stringSet) ToSlice() []string {
	values := make([]string, 0, len(ss))
	for item := range ss {
		values = append(values, item)
	}
	return values
}
