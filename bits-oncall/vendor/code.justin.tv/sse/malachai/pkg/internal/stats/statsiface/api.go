package statsiface

import (
	"time"

	"code.justin.tv/sse/malachai/pkg/internal/stats"
)

// StatsAPI writes statistics to cloudwatch
type StatsAPI interface {
	Run()
	Stop(time.Duration) error
	WithSuccessStatter(string, []*stats.Dimension, stats.Func) error
}
