package api

import (
	"time"
)

// data structures for modeling bits chat badges APIs

type BadgeTier struct {
	ChannelId          string     `json:"channelId"`
	Threshold          int64      `json:"threshold"`
	Enabled            bool       `json:"enabled"`
	Title              string     `json:"title"`
	ImageURL1x         string     `json:"image_url_1x"`
	ImageURL2x         string     `json:"image_url_2x"`
	ImageURL4x         string     `json:"image_url_4x"`
	UnlockedUsersCount int64      `json:"unlocked_users_count"`
	LastUpdated        *time.Time `json:"last_updated"`
}

type BadgeTierSetting struct {
	Threshold   int64   `json:"threshold"`
	DeleteImage *bool   `json:"delete_image"`
	DeleteTitle *bool   `json:"delete_title"`
	ImageData1x *string `json:"image_data_1x"`
	ImageData2x *string `json:"image_data_2x"`
	ImageData4x *string `json:"image_data_4x"`
	Enabled     *bool   `json:"enabled"`
	Title       *string `json:"title"`
}

type GetBadgesResponse struct {
	Tiers []*BadgeTier `json:"tiers"`
}

type SetBadgesRequest struct {
	Tiers []*BadgeTierSetting `json:"tiers"`
}

type SetBadgeTiersAdminRequest struct {
	Tiers []*BadgeTierSetting `json:"tiers"`
}
