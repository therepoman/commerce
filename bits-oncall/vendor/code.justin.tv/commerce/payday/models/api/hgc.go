package api

import (
	"strings"
	"time"
)

type Milestone struct {
	Name           string `json:"name"`
	RewardType     string `json:"reward_type" yaml:"reward_type"`
	ImageURL       string `json:"image" yaml:"image"`
	Threshold      int64  `json:"threshold"`
	Description    string `json:"description" yaml:"description,omitempty"`
	RewardID       string `json:"reward_id" yaml:"reward_id"`
	ProductSKU     string `json:"product_sku,omitempty" yaml:"product_sku,omitempty"`
	FulfilledBy    string `json:"fulfilled_by,omitempty" yaml:"fulfilled_by,omitempty"`
	CanBeFulfilled bool   `json:"can_be_fulfilled,omitempty" yaml:"can_be_fulfilled,omitempty"`
}

type MilestoneStatus struct {
	Milestone
	Status string `json:"status"`
}

type Team struct {
	Milestones      []Milestone `json:"milestones"`
	Name            string      `json:"name"`
	Logo            string      `json:"logo"`
	Locale          string      `json:"locale"`
	ID              string      `json:"id"`
	CheeringEnabled bool        `json:"cheering_enabled" yaml:"cheering_enabled"`
}

type Tournament struct {
	Participants         []string    `json:"-"`
	Teams                []Team      `json:"teams" yaml:"teams,omitempty"`
	GlobalMilestones     []Milestone `json:"global_milestones" yaml:"global_milestones"`
	IndividualMilestones []Milestone `json:"individual_milestones" yaml:"individual_milestones"`
}

func (t Tournament) GetTeam(teamId string) (team Team, found bool) {
	for _, t := range t.Teams {
		if strings.EqualFold(teamId, t.ID) {
			team = t
			found = true
			return
		}
	}
	return
}

type GetProgressResponse struct {
	AggregateProgress  int64             `json:"aggregate_progress"`
	IndividualProgress int64             `json:"individual_progress"`
	AggregateRewards   []MilestoneStatus `json:"aggregate_milestones"`
	IndividualRewards  []MilestoneStatus `json:"individual_milestones"`
}

type GetTeamResponse struct {
	Team             Team  `json:"team"`
	PersonalProgress int64 `json:"individual_progress"`
}

type Reward struct {
	RewardID           string    `json:"reward_id"`
	RewardName         string    `json:"reward_name"`
	RewardPlatform     string    `json:"reward_platform"`
	TeamName           string    `json:"team_name,omitempty"`
	FulfillmentStatus  string    `json:"status"`
	Domain             string    `json:"domain"`
	MilestoneType      string    `json:"milestone_type"`
	MilestoneThreshold int64     `json:"milestone_threshold"`
	RewardType         string    `json:"reward_type"`
	LastUpdated        time.Time `json:"last_updated"`
}

type GetTournamentUserRewardsResponse struct {
	Rewards []Reward `json:"rewards"`
}
type AdminGetTournamentUserRewardsResponse struct {
	Rewards                        []Reward `json:"rewards"`
	HasLinkedBlizzardAccount       bool     `json:"has_linked_blizzard_account"`
	LinkedBlizzardAccountID        string   `json:"linked_blizzard_account_id"`
	LinkedBlizzardAccountBattleTag string   `json:"linked_blizzard_account_battle_tag"`
	LinkedBlizzardAccountRegion    string   `json:"linked_blizzard_account_region"`
}

type ClaimRewardRequest struct {
	UserID   string `json:"user_id"`
	RewardID string `json:"reward_id"`
}
