package api

import (
	"encoding/json"
	"fmt"
)

// constants that dictate user state for bits usage

const (
	NEWUSER  = "new"
	ACQUIRED = "acquired"
	SKIPPED  = "skipped"
	CHEERED  = "cheered"
)

// data structures for modeling bits user APIs

type GetBannedResponse struct {
	User        string `json:"user"`
	Banned      bool   `json:"banned"`
	BannedBy    string `json:"bannedBy"`
	Annotation  string `json:"annotation"`
	LastUpdated int64  `json:"lastUpdated"`
}

type SetBannedRequest struct {
	Banned     bool   `json:"banned"`
	BannedBy   string `json:"bannedBy"`
	Annotation string `json:"annotation"`
}

type GetUserInfoResponse struct {
	Eligible  bool   `json:"eligible"`
	UserState string `json:"userState"`
}

func ConvertJsonToGetBannedResponse(jsonResponse []byte) (*GetBannedResponse, error) {
	var response GetBannedResponse
	err := json.Unmarshal(jsonResponse, &response)
	if err != nil {
		return nil, fmt.Errorf("Could not convert json to GetBannedResponse, %v", err)
	}
	return &response, nil
}

func ConvertJsonToGetUserInfoResponse(jsonResponse []byte) (*GetUserInfoResponse, error) {
	var response GetUserInfoResponse
	err := json.Unmarshal(jsonResponse, &response)
	if err != nil {
		return nil, fmt.Errorf("Could not convert json to GetUserInfoResponse, %v", err)
	}
	return &response, nil
}
