package api

type Product struct {
	Id             string  `json:"id"`
	BitsAmount     int     `json:"bits_amount,omitempty"`
	Promo          bool    `json:"promo"` //deprecated
	PromoType      *string `json:"promo_type,omitempty"`
	PromoId        *string `json:"promo_id,omitempty"`
	LocalizedTitle *string `json:"localized_title,omitempty"`
	ProductType    *string `json:"product_type,omitempty"`
	MaxQuantity    int32   `json:"max_quantity"`
	PricingId      *string `json:"pricing_id,omitempty"`
}
