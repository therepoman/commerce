package dynamo

import "github.com/aws/aws-sdk-go/service/dynamodb"

type TableUpdate struct {
	EventId        string            `json:"eventID"`
	EventName      string            `json:"eventName"`
	EventVersion   string            `json:"eventVersion"`
	EventSource    string            `json:"eventSource"`
	AWSRegion      string            `json:"awsRegion"`
	EventSourceARN string            `json:"eventSourceARN"`
	DynamoDB       TableUpdateRecord `json:"dynamodb"`
}

type TableUpdateRecord struct {
	SequenceNumber string                              `json:"SequenceNumber"`
	SizeBytes      int32                               `json:"SizeBytes"`
	StreamViewType string                              `json:"StreamViewType"`
	NewImage       map[string]*dynamodb.AttributeValue `json:"NewImage,omitempty"`
	OldImage       map[string]*dynamodb.AttributeValue `json:"OldImage,omitempty"`
}
