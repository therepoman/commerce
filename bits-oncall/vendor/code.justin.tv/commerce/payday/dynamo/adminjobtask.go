package dynamo

import (
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type AdminJobTasksTable struct {
}

type AdminJobTask struct {
	JobID       string // HASH
	TaskKey     string // RANGE
	LastUpdated time.Time
}

type AdminJobTaskDao struct {
	BaseDao
}

type IAdminJobTaskDao interface {
	Get(jobID string, taskKey string) (*AdminJobTask, error)
	Update(newAdminJobTask *AdminJobTask) error
	Delete(jobID string, taskKey string) error
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *DynamoClientConfig
	GetLatestStreamArn() (string, error)
}

func NewAdminJobTaskDao(client *DynamoClient) IAdminJobTaskDao {
	dao := &AdminJobTaskDao{}
	dao.client = client
	dao.table = &AdminJobTasksTable{}
	return dao
}

func (t *AdminJobTasksTable) GetBaseTableName() BaseTableName {
	return "admin-job-tasks"
}

func (t *AdminJobTasksTable) GetNodeJsRecordKeySelector() NodeJsRecordKeySelector {
	return "record.dynamodb.Keys.jobId.S + '_' + record.dynamodb.Keys.taskKey.S"
}

func (t *AdminJobTasksTable) NewCreateTableInput(name FullTableName, read ReadCapacity, write WriteCapacity) *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		TableName: aws.String(string(name)),
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(int64(read)),
			WriteCapacityUnits: aws.Int64(int64(write)),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("jobId"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("taskKey"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("jobId"),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String("taskKey"),
				KeyType:       aws.String("RANGE"),
			},
		},
		StreamSpecification: &dynamodb.StreamSpecification{
			StreamEnabled:  aws.Bool(true),
			StreamViewType: aws.String(dynamodb.StreamViewTypeNewAndOldImages),
		},
	}
}

func (t *AdminJobTasksTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (DynamoTableRecord, error) {
	lastUpdated, err := TimeFromAttributes(attributeMap, "lastUpdated")
	if err != nil {
		return nil, err
	}

	return &AdminJobTask{
		JobID:       StringFromAttributes(attributeMap, "jobId"),
		TaskKey:     StringFromAttributes(attributeMap, "taskKey"),
		LastUpdated: lastUpdated,
	}, nil
}

func (t *AdminJobTask) GetTable() DynamoTable {
	return &AdminJobTasksTable{}
}

func (t *AdminJobTask) GetScanQueryTable() DynamoScanQueryTable {
	return &AdminJobTasksTable{}
}

func (t *AdminJobTask) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)

	PopulateAttributesString(itemMap, "jobId", t.JobID)
	PopulateAttributesString(itemMap, "taskKey", t.TaskKey)
	PopulateAttributesTime(itemMap, "lastUpdated", t.LastUpdated)

	return itemMap
}

func (t *AdminJobTask) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	keyMap["jobId"] = &dynamodb.AttributeValue{S: aws.String(t.JobID)}
	keyMap["taskKey"] = &dynamodb.AttributeValue{S: aws.String(t.TaskKey)}
	return keyMap
}

func (t *AdminJobTask) NewHashKeyEqualsExpression() string {
	return "jobId = :jobId"
}

func (t *AdminJobTask) NewHashKeyExpressionAttributeValues() map[string]*dynamodb.AttributeValue {
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(attributeMap, ":jobId", t.JobID)
	return attributeMap
}

func (t *AdminJobTask) UpdateWithCurrentTimestamp() {
	t.LastUpdated = time.Now()
}

func (t *AdminJobTask) GetTimestamp() time.Time {
	return t.LastUpdated
}

func (t *AdminJobTask) ApplyTimestamp(timestamp time.Time) {
	t.LastUpdated = timestamp
}

func (t *AdminJobTask) Equals(other DynamoTableRecord) bool {
	otherAdminJobTask, isAdminJobTask := other.(*AdminJobTask)
	if !isAdminJobTask {
		return false
	}

	return t.JobID == otherAdminJobTask.JobID &&
		t.TaskKey == otherAdminJobTask.TaskKey
}

func (t *AdminJobTask) EqualsWithExpectedLastUpdated(other DynamoTableRecord, lastUpdatedTime time.Time, lastUpdatedDelta time.Duration) bool {
	return t.Equals(other) && timeUtils.IsTimeInForwardDelta(other.GetTimestamp(), lastUpdatedTime, lastUpdatedDelta)
}

func (dao *AdminJobTaskDao) Update(newAdminJobTask *AdminJobTask) error {
	return dao.client.UpdateItem(newAdminJobTask)
}

func (dao *AdminJobTaskDao) Get(jobID string, taskKey string) (*AdminJobTask, error) {
	var adminJobTaskResult *AdminJobTask

	adminJobQuery := &AdminJobTask{
		JobID:   jobID,
		TaskKey: taskKey,
	}

	result, err := dao.client.GetItemWithConsistentRead(adminJobQuery)
	if err != nil {
		return nil, err
	}

	if result == nil {
		return nil, nil
	}

	adminJobTaskResult, isAdminJobTask := result.(*AdminJobTask)
	if !isAdminJobTask {
		msg := "Encountered an unexpected type while converting dynamo result to adminJobTask"
		log.Error(msg)
		return nil, errors.New(msg)
	}

	return adminJobTaskResult, nil
}

func (dao *AdminJobTaskDao) Delete(jobID string, taskKey string) error {
	deletionRecord := &AdminJobTask{
		JobID:   jobID,
		TaskKey: taskKey,
	}
	return dao.client.DeleteItem(deletionRecord)
}

func (t *AdminJobTasksTable) ConvertScanOutputToRecords(so *dynamodb.ScanOutput) ([]DynamoScanQueryTableRecord, error) {
	records := make([]DynamoScanQueryTableRecord, *so.Count)

	for idx, attributes := range so.Items {
		recordToAdd, err := t.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[idx] = scanQueryRecord
	}
	return records, nil
}

func (t *AdminJobTasksTable) ConvertQueryOutputToRecords(qo *dynamodb.QueryOutput) ([]DynamoScanQueryTableRecord, error) {
	records := make([]DynamoScanQueryTableRecord, *qo.Count)

	for idx, attributes := range qo.Items {
		recordToAdd, err := t.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[idx] = scanQueryRecord
	}
	return records, nil
}
