package dynamo

import (
	"math/big"
	"strconv"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/holds"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/utils/math"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

func attributeMapContains(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) bool {
	_, exists := attributeMap[attributeName]
	return exists
}

func StringFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) string {
	var result string
	attribute, exists := attributeMap[attributeName]
	if exists {
		result = *attribute.S
	}
	return result
}

func OptionalStringFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) *string {
	if !attributeMapContains(attributeMap, attributeName) {
		return nil
	}
	attr := StringFromAttributes(attributeMap, attributeName)
	return &attr
}

func BoolFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) bool {
	var result bool
	attribute, exists := attributeMap[attributeName]
	if exists {
		result = *attribute.BOOL
	}
	return result
}

func OptionalBoolFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) *bool {
	if !attributeMapContains(attributeMap, attributeName) {
		return nil
	}
	attr := BoolFromAttributes(attributeMap, attributeName)
	return &attr
}

func Int64FromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) (int64, error) {
	var result int64
	attribute, exists := attributeMap[attributeName]
	if exists {
		var err error
		result, err = strconv.ParseInt(*attribute.N, 10, 64)
		if err != nil {
			msg := "Error converting dynamo attribute to int64"
			log.WithField("attributeName", attributeName).Error(msg)
			return result, errors.Notef(err, msg)
		}
	}
	return result, nil
}

func BigIntFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) (big.Int, error) {
	var result *big.Int
	attribute, exists := attributeMap[attributeName]
	if exists {
		var err error
		result, err = math.NumericStringToBigInt(*attribute.N)
		if err != nil {
			msg := "Error converting dynamo attribute to bigInt"
			log.WithField("attributeName", attributeName).Error(msg)
			return big.Int{}, errors.Notef(err, msg)
		}
	}
	return *result, nil
}

func OptionalInt64FromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) (*int64, error) {
	if !attributeMapContains(attributeMap, attributeName) {
		return nil, nil
	}
	attr, err := Int64FromAttributes(attributeMap, attributeName)
	if err != nil {
		return nil, err
	}
	return &attr, nil
}

func Float64FromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) (float64, error) {
	var result float64
	attribute, exists := attributeMap[attributeName]
	if exists {
		var err error
		result, err = strconv.ParseFloat(*attribute.N, 64)
		if err != nil {
			msg := "Error converting dynamo attribute to float64"
			log.WithField("attributeName", attributeName).Error(msg)
			return result, errors.Notef(err, msg)
		}
	}
	return result, nil
}

func TimeFromAttributesInSeconds(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) (time.Time, error) {
	var result time.Time
	attribute, exists := attributeMap[attributeName]
	if exists {
		unixResult, err := strconv.ParseInt(*attribute.N, 10, 64)
		if err != nil {
			msg := "Error converting dynamo attribute to time"
			log.WithField("attributeName", attributeName).Error(msg)
			return result, errors.Notef(err, msg)
		}
		result = time.Unix(unixResult, 0)
	}
	return result, nil
}

func TimeFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) (time.Time, error) {
	var result time.Time
	attribute, exists := attributeMap[attributeName]
	if exists {
		unixNanoResult, err := strconv.ParseInt(*attribute.N, 10, 64)
		if err != nil {
			msg := "Error converting dynamo attribute to time"
			log.WithField("attributeName", attributeName).Error(msg)
			return result, errors.Notef(err, msg)
		}
		result = time.Unix(0, unixNanoResult)
	}
	return result, nil
}

func OptionalTimeFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) (*time.Time, error) {
	if !attributeMapContains(attributeMap, attributeName) {
		return nil, nil
	}
	attr, err := TimeFromAttributes(attributeMap, attributeName)
	if err != nil {
		return nil, err
	}
	return &attr, nil
}

func DurationFromAttribute(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) (time.Duration, error) {
	var result time.Duration
	attribute, exists := attributeMap[attributeName]
	if exists {
		unixNanoResult, err := strconv.ParseInt(*attribute.N, 10, 64)
		if err != nil {
			msg := "Error converting dynamo attribute to duration"
			log.WithField("attributeName", attributeName).Error(msg)
			return result, errors.Notef(err, msg)
		}
		result = time.Duration(unixNanoResult)
	}
	return result, nil
}

func MapInt64ToFloat64FromAttribute(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) (map[int64]float64, error) {
	result := make(map[int64]float64)
	attribute, exists := attributeMap[attributeName]
	if exists && attribute.M != nil {
		for key, value := range attribute.M {
			intKey, err := strconv.ParseInt(key, 10, 64)
			if err != nil {
				return nil, err
			}
			if value.N == nil {
				return nil, errors.New("Value in int map was empty")
			}
			floatValue, err := strconv.ParseFloat(*value.N, 10)
			if err != nil {
				return nil, err
			}
			result[intKey] = floatValue
		}
	}
	return result, nil
}

func MapStringToInt64FromAttribute(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) (map[string]int64, error) {
	result := make(map[string]int64)
	attribute, exists := attributeMap[attributeName]
	if exists && attribute.M != nil {
		for key, value := range attribute.M {
			if value.N == nil {
				return nil, errors.New("Value in map was empty")
			}
			int64Value, err := strconv.ParseInt(*value.N, 10, 64)
			if err != nil {
				return nil, err
			}

			result[key] = int64Value
		}
	}

	return result, nil
}

func MapStringToBoolFromAttribute(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) (map[string]bool, error) {
	result := make(map[string]bool)
	attribute, exists := attributeMap[attributeName]
	if exists && attribute.M != nil {
		for key, value := range attribute.M {
			if value.BOOL == nil {
				return nil, errors.New("Value in map was empty")
			}
			result[key] = *value.BOOL
		}
	}

	return result, nil
}

func PopulateAttributesString(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue string) {
	if attributeValue != "" {
		attributeMap[attributeName] = &dynamodb.AttributeValue{S: aws.String(attributeValue)}
	}
}

func PopulateAttributesOptionalString(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue *string) {
	if attributeValue != nil && *attributeValue != "" {
		attributeMap[attributeName] = &dynamodb.AttributeValue{S: aws.String(*attributeValue)}
	}
}

func PopulateAttributesMapInt64ToFloat64(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue map[int64]float64) {
	if attributeValue != nil {
		attributeValueMap := make(map[string]*dynamodb.AttributeValue)
		for key, value := range attributeValue {
			stringKey := strconv.FormatInt(key, 10)
			stringValue := strconv.FormatFloat(value, 'f', -1, 64)
			attributeValueMap[stringKey] = &dynamodb.AttributeValue{N: &stringValue}
		}

		attributeMap[attributeName] = &dynamodb.AttributeValue{M: attributeValueMap}
	}
}

func PopulateAttributesMapStringToInt64(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue map[string]int64) {
	if attributeValue != nil {
		attributeValueMap := make(map[string]*dynamodb.AttributeValue)
		for key, value := range attributeValue {
			stringValue := strconv.FormatInt(value, 10)
			attributeValueMap[key] = &dynamodb.AttributeValue{N: &stringValue}
		}

		attributeMap[attributeName] = &dynamodb.AttributeValue{M: attributeValueMap}
	}
}

func PopulateAttributesMapStringToBool(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue map[string]bool) {
	if attributeValue != nil {
		attributeValueMap := make(map[string]*dynamodb.AttributeValue)
		for key, value := range attributeValue {
			v := value
			attributeValueMap[key] = &dynamodb.AttributeValue{BOOL: &v}
		}

		attributeMap[attributeName] = &dynamodb.AttributeValue{M: attributeValueMap}
	}
}

func PopulateAttributesBool(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue bool) {
	attributeMap[attributeName] = &dynamodb.AttributeValue{BOOL: aws.Bool(attributeValue)}
}

func PopulateAttributesOptionalBool(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue *bool) {
	if attributeValue != nil {
		attributeMap[attributeName] = &dynamodb.AttributeValue{BOOL: aws.Bool(*attributeValue)}
	}
}

func PopulateAttributesInt64(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue int64) {
	strVal := strconv.FormatInt(attributeValue, 10)
	attributeMap[attributeName] = &dynamodb.AttributeValue{N: aws.String(strVal)}
}

func PopulateAttributesOptionalInt64(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue *int64) {
	if attributeValue != nil {
		strVal := strconv.FormatInt(*attributeValue, 10)
		attributeMap[attributeName] = &dynamodb.AttributeValue{N: aws.String(strVal)}
	}
}

func PopulateAttributesFloat64(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue float64) {
	strVal := strconv.FormatFloat(attributeValue, 'f', -1, 64)
	attributeMap[attributeName] = &dynamodb.AttributeValue{N: aws.String(strVal)}
}

func PopulateAttributesBigInt(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, attributeValue big.Int) {
	strVal := attributeValue.String()
	attributeMap[attributeName] = &dynamodb.AttributeValue{N: aws.String(strVal)}
}

func PopulateAttributesTime(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, time time.Time) {
	if time.UnixNano() > 0 {
		unixNanoStr := strconv.FormatInt(time.UnixNano(), 10)
		attributeMap[attributeName] = &dynamodb.AttributeValue{N: aws.String(unixNanoStr)}
	}
}

func PopulateAttributesOptionalTime(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, time *time.Time) {
	if time != nil && time.UnixNano() > 0 {
		unixNanoStr := strconv.FormatInt(time.UnixNano(), 10)
		attributeMap[attributeName] = &dynamodb.AttributeValue{N: aws.String(unixNanoStr)}
	}
}

func PopulateAttributesDuration(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, duration time.Duration) {
	unixNanoStr := strconv.FormatInt(duration.Nanoseconds(), 10)
	attributeMap[attributeName] = &dynamodb.AttributeValue{N: aws.String(unixNanoStr)}
}

func PopulateAttributeBenefactors(attributeMap map[string]*dynamodb.AttributeValue, attributeName string, benefactors []*holds.Benefactor) {
	benefactorValues := make([]*dynamodb.AttributeValue, 0, len(benefactors))
	for _, benefactor := range benefactors {
		benefactorValues = append(benefactorValues, &dynamodb.AttributeValue{
			M: map[string]*dynamodb.AttributeValue{
				holds.BenefactorIDKey: &dynamodb.AttributeValue{
					S: pointers.StringP(benefactor.ID),
				},
				holds.BenefactorNumberOfBitsKey: &dynamodb.AttributeValue{
					N: pointers.StringP(strconv.FormatInt(benefactor.NumberOfBits, 10)),
				},
				holds.BenefactorTransactionTypeKey: &dynamodb.AttributeValue{
					S: pointers.StringP(benefactor.TransactionType),
				},
			},
		})
	}

	attributeMap[attributeName] = &dynamodb.AttributeValue{
		L: benefactorValues,
	}
}

func NewPutItemInput(fullTableName FullTableName, record DynamoTableRecord) *dynamodb.PutItemInput {
	record.GetTable()
	itemMap := record.NewAttributeMap()
	return &dynamodb.PutItemInput{
		TableName: aws.String(string(fullTableName)),
		Item:      itemMap,
	}
}

func NewUpdateItemInput(fullTableName FullTableName, record DynamoTableRecord) *dynamodb.UpdateItemInput {
	itemMap := record.NewAttributeMap()
	key := record.NewItemKey()
	return &dynamodb.UpdateItemInput{
		TableName:        aws.String(string(fullTableName)),
		Key:              key,
		AttributeUpdates: AttributeMapToAttributeUpdateMap(key, itemMap),
	}
}

func NewGetItemInput(fullTableName FullTableName, record DynamoTableRecord, consistentRead *bool) *dynamodb.GetItemInput {
	return &dynamodb.GetItemInput{
		TableName:      aws.String(string(fullTableName)),
		Key:            record.NewItemKey(),
		ConsistentRead: consistentRead,
	}
}

func NewBatchGetItemInput(fullTableName FullTableName, keys []map[string]*dynamodb.AttributeValue, consistentRead *bool) *dynamodb.BatchGetItemInput {
	return &dynamodb.BatchGetItemInput{
		RequestItems: map[string]*dynamodb.KeysAndAttributes{
			string(fullTableName): {
				Keys:           keys,
				ConsistentRead: consistentRead,
			},
		},
	}
}

func NewDeleteItemInput(fullTableName FullTableName, record DynamoTableRecord) *dynamodb.DeleteItemInput {
	return &dynamodb.DeleteItemInput{
		TableName: aws.String(string(fullTableName)),
		Key:       record.NewItemKey(),
	}
}

func attributeMapToAttributeUpdateMapWithAction(key map[string]*dynamodb.AttributeValue, attributeMap map[string]*dynamodb.AttributeValue, action string) map[string]*dynamodb.AttributeValueUpdate {
	attributeUpdateMap := make(map[string]*dynamodb.AttributeValueUpdate)
	for attr, attrVal := range attributeMap {
		_, isKey := key[attr]
		if !isKey && attrVal != nil {
			attrUpdate := &dynamodb.AttributeValueUpdate{
				Action: aws.String(action),
				Value:  attrVal,
			}
			attributeUpdateMap[attr] = attrUpdate
		}
	}
	return attributeUpdateMap
}

func AttributeMapToAttributeUpdateMap(key map[string]*dynamodb.AttributeValue, attributeMap map[string]*dynamodb.AttributeValue) map[string]*dynamodb.AttributeValueUpdate {
	return attributeMapToAttributeUpdateMapWithAction(key, attributeMap, "PUT")
}

func AttributeMapToAttributeUpdateMapAdd(key map[string]*dynamodb.AttributeValue, attributeMap map[string]*dynamodb.AttributeValue) map[string]*dynamodb.AttributeValueUpdate {
	return attributeMapToAttributeUpdateMapWithAction(key, attributeMap, "ADD")
}

func BenefactorsFromAttributes(attributeMap map[string]*dynamodb.AttributeValue, attributeName string) []*holds.Benefactor {
	values, ok := attributeMap[attributeName]
	if !ok {
		return nil
	}

	benefactors := make([]*holds.Benefactor, 0, len(values.L))
	for _, v := range values.L {
		numberOfBits, err := strconv.ParseInt(*v.M[holds.BenefactorNumberOfBitsKey].N, 10, 64)
		if err != nil {
			log.WithField("number_of_bits", v.M[holds.BenefactorNumberOfBitsKey].N).WithError(err).Warn("cannot parse benefactor field")
			continue
		}
		benefactors = append(benefactors, &holds.Benefactor{
			ID:              *v.M[holds.BenefactorIDKey].S,
			NumberOfBits:    numberOfBits,
			TransactionType: *v.M[holds.BenefactorTransactionTypeKey].S,
		})
	}

	return benefactors
}
