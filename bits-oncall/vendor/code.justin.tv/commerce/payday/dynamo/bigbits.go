package dynamo

import (
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/utils/pointers"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const partitionKey = "big_bits_1"

type BigBitsTable struct {
}

type BigBit struct {
	Partition   string
	Timestamp   time.Time
	ChannelId   string
	ChannelName string
	UserId      string
	UserName    string
	Message     string
	BitsAmount  int64
	IsAnonymous bool
	LastUpdated time.Time
}

type BigBitDao struct {
	BaseDao
}

type IBigBitDao interface {
	Get(timestamp time.Time) (*BigBit, error)
	Update(bigBit *BigBit) error
	Delete(timestamp time.Time) error
	GetNewer(time time.Time) ([]*BigBit, error)
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *DynamoClientConfig
	GetLatestStreamArn() (string, error)
}

func NewBigBitDao(client *DynamoClient) IBigBitDao {
	dao := &BigBitDao{}
	dao.client = client
	dao.table = &BigBitsTable{}
	return dao
}

func (b *BigBitsTable) GetBaseTableName() BaseTableName {
	return "big-bits"
}

func (b *BigBitsTable) GetNodeJsRecordKeySelector() NodeJsRecordKeySelector {
	return "record.dynamodb.Keys.partition.S + '_' + record.dynamodb.Keys.timestamp.N"
}

func (b *BigBitsTable) NewCreateTableInput(name FullTableName, read ReadCapacity, write WriteCapacity) *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		TableName: aws.String(string(name)),
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(int64(read)),
			WriteCapacityUnits: aws.Int64(int64(write)),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("partition"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("timestamp"),
				AttributeType: aws.String("N"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("partition"),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String("timestamp"),
				KeyType:       aws.String("RANGE"),
			},
		},
		StreamSpecification: &dynamodb.StreamSpecification{
			StreamEnabled:  aws.Bool(true),
			StreamViewType: aws.String(dynamodb.StreamViewTypeNewAndOldImages),
		},
	}
}

func (b *BigBitsTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (DynamoTableRecord, error) {
	timestamp, err := TimeFromAttributes(attributeMap, "timestamp")
	if err != nil {
		return nil, err
	}

	bitsAmount, err := Int64FromAttributes(attributeMap, "bitsAmount")
	if err != nil {
		return nil, err
	}

	lastUpdated, err := TimeFromAttributes(attributeMap, "lastUpdated")
	if err != nil {
		return nil, err
	}

	return &BigBit{
		Partition:   partitionKey,
		Timestamp:   timestamp,
		ChannelId:   StringFromAttributes(attributeMap, "channelId"),
		ChannelName: StringFromAttributes(attributeMap, "channelName"),
		UserId:      StringFromAttributes(attributeMap, "userId"),
		UserName:    StringFromAttributes(attributeMap, "userName"),
		Message:     StringFromAttributes(attributeMap, "message"),
		IsAnonymous: BoolFromAttributes(attributeMap, "isAnonymous"),
		BitsAmount:  bitsAmount,
		LastUpdated: lastUpdated,
	}, nil
}

func (b *BigBit) GetTable() DynamoTable {
	return &BigBitsTable{}
}

func (b *BigBit) GetScanQueryTable() DynamoScanQueryTable {
	return &BigBitsTable{}
}

func (b *BigBit) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(itemMap, "partition", b.Partition)
	PopulateAttributesTime(itemMap, "timestamp", b.Timestamp)
	PopulateAttributesString(itemMap, "channelId", b.ChannelId)
	PopulateAttributesString(itemMap, "channelName", b.ChannelName)
	PopulateAttributesString(itemMap, "userId", b.UserId)
	PopulateAttributesString(itemMap, "userName", b.UserName)
	PopulateAttributesInt64(itemMap, "bitsAmount", b.BitsAmount)
	PopulateAttributesString(itemMap, "message", b.Message)
	PopulateAttributesBool(itemMap, "isAnonymous", b.IsAnonymous)
	PopulateAttributesTime(itemMap, "lastUpdated", b.LastUpdated)

	return itemMap
}

func (b *BigBit) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(keyMap, "partition", b.Partition)
	PopulateAttributesTime(keyMap, "timestamp", b.Timestamp)

	return keyMap
}

func (b *BigBit) NewHashKeyEqualsExpression() string {
	return "partition = :partition"
}

func (b *BigBit) NewHashKeyExpressionAttributeValues() map[string]*dynamodb.AttributeValue {
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(attributeMap, "partition", b.Partition)

	return attributeMap
}

func (b *BigBit) UpdateWithCurrentTimestamp() {
	b.LastUpdated = time.Now()
}

func (b *BigBit) GetTimestamp() time.Time {
	return b.LastUpdated
}

func (b *BigBit) ApplyTimestamp(timestamp time.Time) {
	b.LastUpdated = timestamp
}

func (b *BigBit) Equals(other DynamoTableRecord) bool {
	otherBigBit, isBigBit := other.(*BigBit)
	if !isBigBit {
		return false
	}

	return b.Partition == otherBigBit.Partition &&
		b.Timestamp == otherBigBit.Timestamp &&
		b.ChannelId == otherBigBit.ChannelId &&
		b.ChannelName == otherBigBit.ChannelName &&
		b.UserId == otherBigBit.UserId &&
		b.UserName == otherBigBit.UserName &&
		b.BitsAmount == otherBigBit.BitsAmount &&
		b.Message == otherBigBit.Message &&
		b.IsAnonymous == otherBigBit.IsAnonymous
}

func (b *BigBit) EqualsWithExpectedLastUpdated(other DynamoTableRecord, lastUpdatedTime time.Time, lastUpdatedDelta time.Duration) bool {
	return b.Equals(other) && timeUtils.IsTimeInForwardDelta(other.GetTimestamp(), lastUpdatedTime, lastUpdatedDelta)
}

func (b *BigBitsTable) ConvertScanOutputToRecords(so *dynamodb.ScanOutput) ([]DynamoScanQueryTableRecord, error) {
	records := make([]DynamoScanQueryTableRecord, *so.Count)

	for i, attributes := range so.Items {
		recordToAdd, err := b.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func (b *BigBitsTable) ConvertQueryOutputToRecords(qo *dynamodb.QueryOutput) ([]DynamoScanQueryTableRecord, error) {
	records := make([]DynamoScanQueryTableRecord, *qo.Count)

	for i, attributes := range qo.Items {
		recordToAdd, err := b.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func (dao *BigBitDao) Get(timestamp time.Time) (*BigBit, error) {
	var bigBitResult *BigBit

	bigBitQuery := &BigBit{
		Partition: partitionKey,
		Timestamp: timestamp,
	}

	result, err := dao.client.GetItem(bigBitQuery)
	if err != nil {
		return bigBitResult, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	bigBitResult, isBigBit := result.(*BigBit)
	if !isBigBit {
		msg := "Encountered an unexpected type while converting dynamo result to bigBit"
		log.Error(msg)
		return bigBitResult, errors.New(msg)
	}

	return bigBitResult, nil
}

func (dao *BigBitDao) GetNewer(time time.Time) ([]*BigBit, error) {
	result, _, err := dao.client.QueryTable(&BigBitsTable{}, getBigBitsQueryFilter(time))
	if err != nil {
		return nil, err
	}

	if len(result) == 0 {
		return nil, nil
	}

	bigBits := make([]*BigBit, len(result))
	for i, record := range result {
		bigBit, isBigBit := record.(*BigBit)
		if !isBigBit {
			msg := "Non-bigBit query result returned by query"
			log.Error(msg)
			return nil, errors.New(msg)
		}
		bigBits[i] = bigBit
	}

	return bigBits, nil
}

func getBigBitsQueryFilter(time time.Time) QueryFilter {
	filter := QueryFilter{
		Names: map[string]*string{
			"#ID":   aws.String("partition"),
			"#TIME": aws.String("timestamp"),
		},
		Values: map[string]*dynamodb.AttributeValue{
			":id":    {S: aws.String(partitionKey)},
			":start": {N: aws.String(strconv.FormatInt(time.UnixNano(), 10))},
		},
		KeyConditionExpression: aws.String("#ID = :id AND #TIME >= :start"),
		Limit:                  pointers.Int64P(20),
		Descending:             true,
	}
	return filter
}

func (dao *BigBitDao) Update(bigBit *BigBit) error {
	bigBit.Partition = partitionKey
	return dao.client.UpdateItem(bigBit)
}

func (dao *BigBitDao) Delete(timestamp time.Time) error {
	deletionRecord := &BigBit{
		Partition: partitionKey,
		Timestamp: timestamp,
	}

	return dao.client.DeleteItem(deletionRecord)
}
