package dynamo

type BaseDao struct {
	table  DynamoTable
	client *DynamoClient
}

func NewBaseDao(table DynamoTable, client *DynamoClient) BaseDao {
	return BaseDao{
		table:  table,
		client: client,
	}
}

func (dao *BaseDao) GetClient() *DynamoClient {
	return dao.client
}

func (dao *BaseDao) DeleteTable() error {
	return dao.client.DeleteTable(dao.table)
}

func (dao *BaseDao) SetupTable() error {
	return dao.client.SetupTable(dao.table)
}

func (dao *BaseDao) GetClientConfig() *DynamoClientConfig {
	return dao.client.ClientConfig
}

func (dao *BaseDao) GetLatestStreamArn() (string, error) {
	return dao.client.GetLatestStreamArn(dao.table)
}

type Dao interface {
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *DynamoClientConfig
	GetLatestStreamArn() (string, error)
}
