package dynamo

import (
	"fmt"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type HashtagTable struct {
}

type Hashtag struct {
	ChannelId   string
	Hashtag     string
	Selected    bool
	Total       int64
	LastUpdated time.Time
}

type HashtagDao struct {
	BaseDao
}

type IHashtagDao interface {
	Put(tag *Hashtag) error
	PutAll(tags []*Hashtag) error
	Get(channelId string, hashtag string) (*Hashtag, error)
	GetAll(channelId string) ([]*Hashtag, error)
	Delete(channelId string, hashtag string) error
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *DynamoClientConfig
	GetLatestStreamArn() (string, error)
	AddToTotal(record *Hashtag, amountToAdd int64) error
}

func (u *HashtagTable) GetBaseTableName() BaseTableName {
	return "hashtags"
}

func (u *HashtagTable) GetNodeJsRecordKeySelector() NodeJsRecordKeySelector {
	return "record.dynamodb.Keys.channelId.S + '_' + record.dynamodb.Keys.hashtag.S"
}

func (u *HashtagTable) NewCreateTableInput(name FullTableName, read ReadCapacity, write WriteCapacity) *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		TableName: aws.String(string(name)),
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(int64(read)),
			WriteCapacityUnits: aws.Int64(int64(write)),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("channelId"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("hashtag"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("channelId"),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String("hashtag"),
				KeyType:       aws.String("RANGE"),
			},
		},
		StreamSpecification: &dynamodb.StreamSpecification{
			StreamEnabled:  aws.Bool(true),
			StreamViewType: aws.String(dynamodb.StreamViewTypeNewAndOldImages),
		},
	}
}

func (u *HashtagTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (DynamoTableRecord, error) {
	channelIdAttributeVal := StringFromAttributes(attributeMap, "channelId")
	hashtagAttributeVal := StringFromAttributes(attributeMap, "hashtag")
	selectedAttributeVal := BoolFromAttributes(attributeMap, "selected")
	totalAttributeVal, err := Int64FromAttributes(attributeMap, "total")

	if err != nil {
		msg := "Error converting attribute map to Hashtag record"
		log.WithError(err).Error(msg)
		return nil, errors.Notef(err, msg)
	}

	return &Hashtag{
		ChannelId: channelIdAttributeVal,
		Hashtag:   hashtagAttributeVal,
		Selected:  selectedAttributeVal,
		Total:     totalAttributeVal,
	}, nil
}

func (u *Hashtag) GetTable() DynamoTable {
	return &HashtagTable{}
}

func (u *Hashtag) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(itemMap, "channelId", u.ChannelId)
	PopulateAttributesString(itemMap, "hashtag", u.Hashtag)
	PopulateAttributesBool(itemMap, "selected", u.Selected)
	PopulateAttributesInt64(itemMap, "total", u.Total)
	PopulateAttributesTime(itemMap, "lastUpdated", u.LastUpdated)
	return itemMap
}

func (u *Hashtag) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(keyMap, "channelId", u.ChannelId)
	PopulateAttributesString(keyMap, "hashtag", u.Hashtag)
	return keyMap
}

func (u *Hashtag) NewHashKeyEqualsExpression() string {
	return "channelId=:ch"
}

func (u *Hashtag) NewHashKeyExpressionAttributeValues() map[string]*dynamodb.AttributeValue {
	attributeValue := &dynamodb.AttributeValue{
		S: aws.String(u.ChannelId),
	}
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	attributeMap[":ch"] = attributeValue
	return attributeMap
}

func (u *Hashtag) UpdateWithCurrentTimestamp() {
	u.LastUpdated = time.Now()
}

func (u *Hashtag) GetTimestamp() time.Time {
	return u.LastUpdated
}

func (u *Hashtag) ApplyTimestamp(timestamp time.Time) {
	u.LastUpdated = timestamp
}

func (u *Hashtag) Equals(other DynamoTableRecord) bool {
	otherTag, isTier := other.(*Hashtag)
	if !isTier {
		return false
	}

	return u.ChannelId == otherTag.ChannelId &&
		u.Hashtag == otherTag.Hashtag &&
		u.Selected == otherTag.Selected &&
		u.Total == otherTag.Total
}

func (u *Hashtag) EqualsWithExpectedLastUpdated(other DynamoTableRecord, lastUpdatedTime time.Time, lastUpdatedDelta time.Duration) bool {
	return u.Equals(other) && timeUtils.IsTimeInForwardDelta(other.GetTimestamp(), lastUpdatedTime, lastUpdatedDelta)
}

func NewHashtagDao(client *DynamoClient) IHashtagDao {
	dao := &HashtagDao{}
	dao.client = client
	dao.table = &HashtagTable{}
	return dao
}

func (dao *HashtagDao) Put(tag *Hashtag) error {
	return dao.client.PutItem(tag)
}

func (dao *HashtagDao) PutAll(tags []*Hashtag) error {
	for _, tag := range tags {
		err := dao.Put(tag)
		if err != nil {
			msg := "Encountered an error saving one of multiple hashtags"
			log.Error(msg)
			return errors.Notef(err, msg)
		}
	}
	return nil
}

func (dao *HashtagDao) Get(channelId string, hashtag string) (*Hashtag, error) {
	var hashtagResult *Hashtag

	hashtagQuery := &Hashtag{
		ChannelId: channelId,
		Hashtag:   hashtag,
	}

	result, err := dao.client.GetItem(hashtagQuery)
	if err != nil {
		return hashtagResult, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	hashtagResult, isHashtag := result.(*Hashtag)
	if !isHashtag {
		msg := "Encountered an unexpected type while converting dynamo result to hashtag"
		log.Error(msg)
		return hashtagResult, errors.New(msg)
	}

	return hashtagResult, nil
}

func (dao *HashtagDao) GetAll(channelId string) ([]*Hashtag, error) {
	hashtagQuery := &Hashtag{
		ChannelId: channelId,
	}

	results, err := dao.client.QueryByHashKey(hashtagQuery)
	if err != nil {
		return nil, nil
	}

	hashtagResults := make([]*Hashtag, 0, len(results))
	for _, result := range results {
		hashtagResult, isHashtag := result.(*Hashtag)
		if !isHashtag {
			msg := "Encountered an unexpected type while converting dynamo result to hashtags"
			log.Error(msg)
			return nil, errors.New(msg)
		}
		hashtagResults = append(hashtagResults, hashtagResult)
	}

	return hashtagResults, nil
}

func (dao *HashtagDao) Delete(channelId string, hashtag string) error {
	deletionRecord := &Hashtag{
		ChannelId: channelId,
		Hashtag:   hashtag,
	}
	return dao.client.DeleteItem(deletionRecord)
}

func (dao *HashtagDao) AddToTotal(record *Hashtag, amountToAdd int64) error {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesString(keyMap, "channelId", record.ChannelId)
	PopulateAttributesString(keyMap, "hashtag", record.Hashtag)

	totalUpdate := make(map[string]*dynamodb.AttributeValue)
	PopulateAttributesInt64(totalUpdate, "total", amountToAdd)

	updateMap := AttributeMapToAttributeUpdateMapAdd(keyMap, totalUpdate)

	fullTableName := dao.client.GetFullTableName(record.GetTable())
	updateItemInput := &dynamodb.UpdateItemInput{
		TableName:        aws.String(string(fullTableName)),
		Key:              keyMap,
		AttributeUpdates: updateMap,
	}

	_, err := dao.client.Dynamo.UpdateItem(updateItemInput)
	if err != nil {
		msg := fmt.Sprintf("HashTag DAO Add: Encountered an error calling dynamo UpdateItem, %v", err)
		log.Error(msg)
		return errors.Notef(err, msg)
	}
	return nil
}
