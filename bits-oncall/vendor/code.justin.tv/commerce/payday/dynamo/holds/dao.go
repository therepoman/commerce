package holds

import (
	"context"
)

const (
	HashKey  = "user_id"
	RangeKey = "transaction_id"

	BenefactorIDKey              = "id"
	BenefactorTransactionTypeKey = "transaction_type"
	BenefactorNumberOfBitsKey    = "number_of_bits"

	CreateHoldTransactionType   = "CreateHold"
	FinalizeHoldTransactionType = "FinalizeHold"
	ReleaseHoldTransactionType  = "ReleaseHold"
)

type BalanceHold struct {
	UserID        string `dynamo:"user_id"`
	TransactionID string `dynamo:"transaction_id"` // hash key
	IsActive      bool   `dynamo:"is_active"`

	// transaction fields
	TransactionType       string  `dynamo:"transaction_type"`
	UserIDTransactionType string  `dynamo:"user_id-transaction_type"` // GSI hash key
	LastUpdated           int64   `dynamo:"last_updated"`
	OperatorTransactionID *string `dynamo:"operator_transaction_id"`
}

type BalanceHoldExecution struct {
	BalanceHold

	NumberOfBits         int64         `dynamo:"number_of_bits"`
	Benefactors          []*Benefactor `dynamo:"benefactors"`
	OperandTransactionID string        `dynamo:"operand_transaction_id"`
}

type Benefactor struct {
	ID              string `dynamo:"id"`
	TransactionType string `dynamo:"transaction_type"`
	NumberOfBits    int64  `dynamo:"number_of_bits"`
}

type DAO interface {
	Create(ctx context.Context, userID, transactionID string) (*BalanceHold, error)
	GetHold(ctx context.Context, userID, holdID string) (*BalanceHold, error)
	FinalizeHold(ctx context.Context, transactionID, userID, holdID string, benefactors []*Benefactor) error
	ReleaseHold(ctx context.Context, transactionID, userID, holdID string) error
	GetActive(ctx context.Context, userID string) ([]*BalanceHold, error)
	GetOperatorTransactionID(ctx context.Context, userID, holdID string) (string, error)
}
