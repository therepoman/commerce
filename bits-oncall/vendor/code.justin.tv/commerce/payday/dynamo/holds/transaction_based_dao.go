package holds

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/payday/config"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/gofrs/uuid"
	db "github.com/guregu/dynamo"
	"github.com/pkg/errors"
)

const (
	hashKey                     = "transaction_id"
	userIdTransactionTypeKey    = "user_id-transaction_type-index"
	userIdTransactionTypeIndex  = "user_id-transaction_type"
	userIdTransactionTypeFormat = "%s-%s"

	canExecuteCondition = "'user_id' = ? AND 'is_active' = ?"
)

type transactionBasedDAO struct {
	*db.Table
	session *db.DB
}

func NewTransactionBasedDAO(sess *session.Session, cfg config.Configuration) DAO {
	db := db.New(sess)
	table := db.Table(cfg.TransactionTableName)
	return &transactionBasedDAO{
		Table:   &table,
		session: db,
	}
}

func (d *transactionBasedDAO) Create(ctx context.Context, userID, transactionID string) (*BalanceHold, error) {
	hold := &BalanceHold{
		TransactionID:         transactionID,
		UserID:                userID,
		IsActive:              true,
		TransactionType:       CreateHoldTransactionType,
		UserIDTransactionType: getUserIDTransactionTypeKey(userID, CreateHoldTransactionType),
		LastUpdated:           time.Now().UnixNano(),
	}

	err := d.Put(hold).RunWithContext(ctx)
	if err != nil {
		return nil, err
	}

	return hold, nil
}

func (d *transactionBasedDAO) GetHold(ctx context.Context, userID, holdID string) (*BalanceHold, error) {
	var hold BalanceHold
	err := d.Get(hashKey, holdID).Filter("'user_id' = ?", userID).OneWithContext(ctx, &hold)
	if err != nil && err != db.ErrNotFound {
		return nil, err
	}

	return &hold, nil
}

func (d *transactionBasedDAO) GetActive(ctx context.Context, userID string) ([]*BalanceHold, error) {
	var holds []*BalanceHold
	err := d.Get(userIdTransactionTypeKey, getUserIDTransactionTypeKey(userID, CreateHoldTransactionType)).
		Index(userIdTransactionTypeIndex).
		Filter("'is_active' = ?", true).AllWithContext(ctx, holds)

	if err != nil {
		return nil, err
	}
	return holds, err
}

func (d *transactionBasedDAO) FinalizeHold(ctx context.Context, transactionID, userID, holdID string, benefactors []*Benefactor) error {
	var totalNumberOfBits int64
	for _, benefactor := range benefactors {
		totalNumberOfBits += benefactor.NumberOfBits
	}

	tx := d.session.WriteTx()
	tx.Update(d.Update(hashKey, holdID).
		Set("is_active", false).
		Set("last_updated", time.Now().UnixNano()).
		If(canExecuteCondition, userID, true))

	tx.Put(d.Put(&BalanceHoldExecution{
		BalanceHold: BalanceHold{
			UserID:                userID,
			TransactionID:         transactionID,
			TransactionType:       FinalizeHoldTransactionType,
			IsActive:              false,
			LastUpdated:           time.Now().UnixNano(),
			UserIDTransactionType: getUserIDTransactionTypeKey(userID, FinalizeHoldTransactionType),
		},
		OperandTransactionID: holdID,
		NumberOfBits:         totalNumberOfBits,
		Benefactors:          benefactors,
	}).If("attribute_not_exists('transaction_id')"))

	return tx.RunWithContext(ctx)
}

func (d *transactionBasedDAO) ReleaseHold(ctx context.Context, transactionID, userID, holdID string) error {
	tx := d.session.WriteTx()

	tx.Update(d.Update(hashKey, holdID).
		Set("is_active", false).
		Set("last_updated", time.Now().UnixNano()).
		If(canExecuteCondition, userID, true))

	tx.Put(d.Put(&BalanceHoldExecution{
		BalanceHold: BalanceHold{
			TransactionID:         transactionID,
			TransactionType:       ReleaseHoldTransactionType,
			UserID:                userID,
			IsActive:              false,
			LastUpdated:           time.Now().UnixNano(),
			UserIDTransactionType: getUserIDTransactionTypeKey(userID, ReleaseHoldTransactionType),
		},
		OperandTransactionID: holdID,
	}).If("attribute_not_exists('transaction_id')"))

	return tx.RunWithContext(ctx)
}

// GetOperatorTransactionID returns the operator transaction ID on a hold
// if the ID does not exist, it generates a new ID and saves it in the DB
func (d *transactionBasedDAO) GetOperatorTransactionID(ctx context.Context, userID, holdID string) (string, error) {
	hold, err := d.GetHold(ctx, userID, holdID)
	if err != nil {
		return "", err
	}
	if hold == nil {
		return "", errors.Errorf("cannot find hold with ID [%s]", holdID)
	}

	if hold.OperatorTransactionID != nil {
		return *hold.OperatorTransactionID, nil
	}

	operatorTransactionID := uuid.Must(uuid.NewV4()).String()
	err = d.Update(hashKey, holdID).
		Set("operator_transaction_id", operatorTransactionID).
		If("attribute_not_exists('operator_transaction_id')").
		RunWithContext(ctx)

	return operatorTransactionID, err
}

func getUserIDTransactionTypeKey(userID, transactionType string) string {
	return fmt.Sprintf(userIdTransactionTypeFormat, userID, transactionType)
}
