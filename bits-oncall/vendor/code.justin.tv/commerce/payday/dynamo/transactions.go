package dynamo

import (
	"fmt"
	"reflect"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/dynamo/holds"
	"code.justin.tv/commerce/payday/errors"
	"code.justin.tv/commerce/payday/utils/pointers"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const (
	transactionIdColumn         = "transaction_id" // Hash key
	numberOfBitsColumn          = "number_of_bits"
	tmiResponseColumn           = "tmi_response"
	tmiMessageColumn            = "tmi_message"
	rawMessageColumn            = "raw_message"
	channelIdColumn             = "channel_id"
	transactionTypeColumn       = "transaction_type"
	userIdColumn                = "user_id"
	lastUpdatedColumn           = "last_updated"
	notifyUserColumn            = "notify_user"
	notifiedUserColumn          = "notified_user"
	originColumn                = "origin"
	tableName                   = "transactions"
	clientAppColumn             = "client_app"
	bitsTypeColumn              = "bits_type"
	productIdColumn             = "product_id"
	platformColumn              = "platform"
	emoteTotalsColumn           = "emote_totals"
	benefactorsColumn           = "benefactors"              // for executions on holds
	operandTransactionIDColumn  = "operand_transaction_id"   // for executions on holds
	userIdTransactionTypeColumn = "user_id-transaction_type" // GSI for user - transaction type search

	userIdTransactionTypeColumnFormat = "%s-%s"
)

type TransactionId string

type TransactionsTable struct{}

type Transaction struct {
	TransactionId        TransactionId
	TmiResponse          *string
	TmiMessage           *string
	RawMessage           *string
	ChannelId            *string
	UserId               *string
	TransactionType      *string
	NumberOfBits         *int64
	BitsType             *string
	ProductId            *string
	Platform             *string
	LastUpdated          time.Time
	NotifyUser           *bool
	NotifiedUser         *bool
	Origin               *string
	ClientApp            *string
	EmoteTotals          map[string]int64
	ExpiresAt            *time.Time
	OperandTransactionID *string
	Benefactors          []*holds.Benefactor
}

type TransactionsDao struct {
	BaseDao
}

type ITransactionsDao interface {
	Update(user *Transaction) error
	Get(id TransactionId) (*Transaction, error)
	BatchGet(ids []TransactionId) ([]*Transaction, error)
	Delete(id TransactionId) error
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *DynamoClientConfig
	GetLatestStreamArn() (string, error)
}

func (t *TransactionsTable) GetBaseTableName() BaseTableName {
	return tableName
}

func (t *TransactionsTable) GetNodeJsRecordKeySelector() NodeJsRecordKeySelector {
	return "record.dynamodb.Keys.transaction_id.S"
}

func (t *TransactionsTable) NewCreateTableInput(name FullTableName, read ReadCapacity, write WriteCapacity) *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		TableName: aws.String(string(name)),
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(int64(read)),
			WriteCapacityUnits: aws.Int64(int64(write)),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String(transactionIdColumn),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String(transactionIdColumn),
				KeyType:       aws.String("HASH"),
			},
		},
		StreamSpecification: &dynamodb.StreamSpecification{
			StreamEnabled:  aws.Bool(true),
			StreamViewType: aws.String(dynamodb.StreamViewTypeNewAndOldImages),
		},
	}
}

func (t *TransactionsTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (DynamoTableRecord, error) {
	numberOfBits, err := OptionalInt64FromAttributes(attributeMap, numberOfBitsColumn)
	if err != nil {
		return nil, err
	}

	lastUpdated, err := TimeFromAttributes(attributeMap, lastUpdatedColumn)
	if err != nil {
		return nil, err
	}

	emoteTotals, err := MapStringToInt64FromAttribute(attributeMap, emoteTotalsColumn)
	if err != nil {
		return nil, err
	}

	return &Transaction{
		TransactionId:        TransactionId(StringFromAttributes(attributeMap, transactionIdColumn)),
		TransactionType:      OptionalStringFromAttributes(attributeMap, transactionTypeColumn),
		TmiResponse:          OptionalStringFromAttributes(attributeMap, tmiResponseColumn),
		TmiMessage:           OptionalStringFromAttributes(attributeMap, tmiMessageColumn),
		RawMessage:           OptionalStringFromAttributes(attributeMap, rawMessageColumn),
		ChannelId:            OptionalStringFromAttributes(attributeMap, channelIdColumn),
		UserId:               OptionalStringFromAttributes(attributeMap, userIdColumn),
		NotifyUser:           OptionalBoolFromAttributes(attributeMap, notifyUserColumn),
		NotifiedUser:         OptionalBoolFromAttributes(attributeMap, notifiedUserColumn),
		Origin:               OptionalStringFromAttributes(attributeMap, originColumn),
		NumberOfBits:         numberOfBits,
		LastUpdated:          lastUpdated,
		ClientApp:            OptionalStringFromAttributes(attributeMap, clientAppColumn),
		BitsType:             OptionalStringFromAttributes(attributeMap, bitsTypeColumn),
		ProductId:            OptionalStringFromAttributes(attributeMap, productIdColumn),
		Platform:             OptionalStringFromAttributes(attributeMap, platformColumn),
		EmoteTotals:          emoteTotals,
		OperandTransactionID: OptionalStringFromAttributes(attributeMap, operandTransactionIDColumn),
		Benefactors:          BenefactorsFromAttributes(attributeMap, benefactorsColumn),
	}, nil
}

func (t *Transaction) GetTable() DynamoTable {
	return &TransactionsTable{}
}

func (t *Transaction) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)

	PopulateAttributesTime(itemMap, lastUpdatedColumn, t.LastUpdated)

	// Required attributes
	PopulateAttributesString(itemMap, transactionIdColumn, string(t.TransactionId))

	// Optional attributes
	if t.TmiResponse != nil {
		PopulateAttributesString(itemMap, tmiResponseColumn, *t.TmiResponse)
	}

	if t.TmiMessage != nil {
		PopulateAttributesString(itemMap, tmiMessageColumn, *t.TmiMessage)
	}

	if t.RawMessage != nil {
		PopulateAttributesString(itemMap, rawMessageColumn, *t.RawMessage)
	}

	if t.ChannelId != nil {
		PopulateAttributesString(itemMap, channelIdColumn, *t.ChannelId)
	}

	if t.UserId != nil {
		PopulateAttributesString(itemMap, userIdColumn, *t.UserId)
	}

	if t.NumberOfBits != nil {
		PopulateAttributesInt64(itemMap, numberOfBitsColumn, *t.NumberOfBits)
	}

	if t.TransactionType != nil {
		PopulateAttributesString(itemMap, transactionTypeColumn, *t.TransactionType)
	}

	if t.NotifyUser != nil {
		PopulateAttributesBool(itemMap, notifyUserColumn, *t.NotifyUser)
	}

	if t.NotifiedUser != nil {
		PopulateAttributesBool(itemMap, notifiedUserColumn, *t.NotifiedUser)
	}

	if t.Origin != nil {
		PopulateAttributesString(itemMap, originColumn, *t.Origin)
	}

	if t.ClientApp != nil {
		PopulateAttributesString(itemMap, clientAppColumn, *t.ClientApp)
	}

	if !t.LastUpdated.IsZero() {
		PopulateAttributesTime(itemMap, lastUpdatedColumn, t.LastUpdated)
	}

	if t.ProductId != nil {
		PopulateAttributesString(itemMap, productIdColumn, *t.ProductId)
	}

	if t.BitsType != nil {
		PopulateAttributesString(itemMap, bitsTypeColumn, *t.BitsType)
	}

	if t.Platform != nil {
		PopulateAttributesString(itemMap, platformColumn, *t.Platform)
	}

	if len(t.EmoteTotals) > 0 {
		PopulateAttributesMapStringToInt64(itemMap, emoteTotalsColumn, t.EmoteTotals)
	}

	if t.UserId != nil && t.TransactionType != nil {
		PopulateAttributesString(itemMap, userIdTransactionTypeColumn, fmt.Sprintf(userIdTransactionTypeColumnFormat, *t.UserId, *t.TransactionType))
	}

	if t.OperandTransactionID != nil {
		PopulateAttributesString(itemMap, operandTransactionIDColumn, *t.OperandTransactionID)
	}

	if t.Benefactors != nil {
		PopulateAttributeBenefactors(itemMap, benefactorsColumn, t.Benefactors)
	}

	return itemMap
}

func (t *Transaction) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	keyMap[transactionIdColumn] = &dynamodb.AttributeValue{S: aws.String(string(t.TransactionId))}
	return keyMap
}

func (t *Transaction) NewHashKeyEqualsExpression() string {
	return fmt.Sprintf("%s = :%s", transactionIdColumn, transactionIdColumn)
}

func (t *Transaction) NewHashKeyExpressionAttributeValues() map[string]*dynamodb.AttributeValue {
	attributeValue := &dynamodb.AttributeValue{
		S: aws.String(string(t.TransactionId)),
	}

	attributeMap := make(map[string]*dynamodb.AttributeValue)
	attributeMap[fmt.Sprintf(":%s", transactionIdColumn)] = attributeValue
	return attributeMap
}

func (t *Transaction) UpdateWithCurrentTimestamp() {
	t.LastUpdated = time.Now()
}

func (t *Transaction) GetTimestamp() time.Time {
	return t.LastUpdated
}

func (t *Transaction) ApplyTimestamp(timestamp time.Time) {
	t.LastUpdated = timestamp
}

func (t *Transaction) Equals(other DynamoTableRecord) bool {
	otherTransaction, isTransaction := other.(*Transaction)
	if !isTransaction {
		return false
	}

	return t.TransactionId == otherTransaction.TransactionId &&
		pointers.StringPEquals(t.TmiResponse, otherTransaction.TmiResponse) &&
		pointers.StringPEquals(t.TmiMessage, otherTransaction.TmiMessage) &&
		pointers.StringPEquals(t.RawMessage, otherTransaction.RawMessage) &&
		pointers.StringPEquals(t.ChannelId, otherTransaction.ChannelId) &&
		pointers.StringPEquals(t.UserId, otherTransaction.UserId) &&
		pointers.Int64PEquals(t.NumberOfBits, otherTransaction.NumberOfBits) &&
		pointers.BoolPEquals(t.NotifyUser, otherTransaction.NotifyUser) &&
		pointers.BoolPEquals(t.NotifiedUser, otherTransaction.NotifiedUser) &&
		pointers.StringPEquals(t.Origin, otherTransaction.Origin) &&
		pointers.StringPEquals(t.ProductId, otherTransaction.ProductId) &&
		pointers.StringPEquals(t.BitsType, otherTransaction.BitsType) &&
		pointers.StringPEquals(t.Platform, otherTransaction.Platform) &&
		reflect.DeepEqual(t.EmoteTotals, otherTransaction.EmoteTotals) &&
		pointers.StringPEquals(t.OperandTransactionID, otherTransaction.OperandTransactionID) &&
		reflect.DeepEqual(t.Benefactors, otherTransaction.Benefactors)
}

func (t *Transaction) EqualsWithExpectedLastUpdated(other DynamoTableRecord, lastUpdatedTime time.Time, lastUpdatedDelta time.Duration) bool {
	return t.Equals(other) && timeUtils.IsTimeInForwardDelta(other.GetTimestamp(), lastUpdatedTime, lastUpdatedDelta)
}

func NewTransactionsDao(client *DynamoClient) ITransactionsDao {
	dao := &TransactionsDao{}
	dao.client = client
	dao.table = &TransactionsTable{}
	return dao
}

func (dao *TransactionsDao) Update(user *Transaction) error {
	return dao.client.UpdateItem(user)
}

func (dao *TransactionsDao) Get(id TransactionId) (*Transaction, error) {
	var transactionResult *Transaction

	transactionQuery := &Transaction{
		TransactionId: id,
	}

	result, err := dao.client.GetItemWithConsistentRead(transactionQuery)
	if err != nil {
		return transactionResult, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	transactionResult, isTransaction := result.(*Transaction)
	if !isTransaction {
		msg := "Encountered an unexpected type while converting dynamo result to transaction"
		log.Error(msg)
		return transactionResult, errors.New(msg)
	}

	return transactionResult, nil
}

func (dao *TransactionsDao) BatchGet(ids []TransactionId) ([]*Transaction, error) {
	if len(ids) == 0 {
		return nil, nil
	}

	dedupedIDs := make(map[TransactionId]bool)
	queryRecords := make([]DynamoTableRecord, 0, len(ids))
	for _, id := range ids {
		if !dedupedIDs[id] {
			queryRecords = append(queryRecords, &Transaction{
				TransactionId: id,
			})
			dedupedIDs[id] = true
		}
	}

	results, err := dao.client.BatchGetItem(queryRecords)
	if err != nil {
		return nil, err
	}

	transactions := make([]*Transaction, 0, len(results))
	for _, result := range results {
		transaction, isTransaction := result.(*Transaction)
		if !isTransaction {
			msg := "Encountered an unexpected type while converting dynamo result to transaction"
			log.Error(msg)
			return nil, errors.New(msg)
		}
		transactions = append(transactions, transaction)
	}

	return transactions, nil
}

func (dao *TransactionsDao) Delete(transaction TransactionId) error {
	deletionRecord := &Transaction{
		TransactionId: transaction,
	}
	return dao.client.DeleteItem(deletionRecord)
}
