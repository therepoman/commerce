package dynamo

import (
	"bytes"
	"fmt"
	"strconv"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/errors"
	cmd "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/commerce/payday/utils/pointers"
	timeUtils "code.justin.tv/commerce/payday/utils/time"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type ChannelId string

type ChannelsTable struct {
}

type Channel struct {
	Id                        ChannelId
	OptedOut                  *bool
	Onboarded                 *bool
	Annotation                *string
	SessionId                 *string
	MinBits                   *int64
	MinBitsEmote              *int64
	PinTopCheers              *bool
	PinRecentCheers           *bool
	RecentCheerMin            *int64
	RecentCheerTimeout        *int64
	CustomCheermoteImageSetId *string
	CustomCheermoteStatus     *string
	AmendmentState            *string
	LastOnboardedEventTime    *int64
	LastUpdated               time.Time
	LeaderboardEnabled        *bool
	LeaderboardTimePeriod     *string
	CheerBombEventOptOut      *bool
	OptedOutOfVoldemort       *bool
}

type ChannelDao struct {
	BaseDao
}

type IChannelDao interface {
	Update(user *Channel) error
	Get(id ChannelId) (*Channel, error)
	Delete(id ChannelId) error
	DeleteTable() error
	SetupTable() error
	GetClientConfig() *DynamoClientConfig
	GetLatestStreamArn() (string, error)
	GetAll() ([]*Channel, error)
	GetByImageSetId(imageSetId string) (*Channel, error)
	UpdateIfStatusIsNewer(channel Channel) error
}

func (u *ChannelsTable) GetBaseTableName() BaseTableName {
	return "channels"
}

func (u *ChannelsTable) GetNodeJsRecordKeySelector() NodeJsRecordKeySelector {
	return "record.dynamodb.Keys.id.S"
}

func (u *ChannelsTable) NewCreateTableInput(name FullTableName, read ReadCapacity, write WriteCapacity) *dynamodb.CreateTableInput {
	return &dynamodb.CreateTableInput{
		TableName: aws.String(string(name)),
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(int64(read)),
			WriteCapacityUnits: aws.Int64(int64(write)),
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("id"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("customCheermoteImageSetId"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("id"),
				KeyType:       aws.String("HASH"),
			},
		},
		GlobalSecondaryIndexes: []*dynamodb.GlobalSecondaryIndex{
			{
				IndexName: aws.String("customCheermoteImageSetId-index"),
				KeySchema: []*dynamodb.KeySchemaElement{
					{
						AttributeName: aws.String("customCheermoteImageSetId"),
						KeyType:       aws.String("HASH"),
					},
				},
				Projection: &dynamodb.Projection{
					ProjectionType: aws.String("ALL"),
				},
				ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
					ReadCapacityUnits:  aws.Int64(int64(read)),
					WriteCapacityUnits: aws.Int64(int64(write)),
				},
			},
		},
		StreamSpecification: &dynamodb.StreamSpecification{
			StreamEnabled:  aws.Bool(true),
			StreamViewType: aws.String(dynamodb.StreamViewTypeNewAndOldImages),
		},
	}
}

func (u *ChannelsTable) ConvertAttributeMapToRecord(attributeMap map[string]*dynamodb.AttributeValue) (DynamoTableRecord, error) {
	minBitsAttr, err := OptionalInt64FromAttributes(attributeMap, "minBits")
	if err != nil {
		return nil, err
	}

	minBitsEmoteAttr, err := OptionalInt64FromAttributes(attributeMap, "minBitsEmote")
	if err != nil {
		return nil, err
	}

	recentCheerMin, err := OptionalInt64FromAttributes(attributeMap, "recentCheerMin")
	if err != nil {
		return nil, err
	}

	recentCheerTimeout, err := OptionalInt64FromAttributes(attributeMap, "recentCheerTimeout")
	if err != nil {
		return nil, err
	}

	onboardedEventTime, err := OptionalInt64FromAttributes(attributeMap, "lastOnboardedEventTime")
	if err != nil {
		return nil, err
	}

	return &Channel{
		Id:                        ChannelId(StringFromAttributes(attributeMap, "id")),
		OptedOut:                  OptionalBoolFromAttributes(attributeMap, "optedOut"),
		Onboarded:                 OptionalBoolFromAttributes(attributeMap, "onboarded"),
		Annotation:                OptionalStringFromAttributes(attributeMap, "annotation"),
		SessionId:                 OptionalStringFromAttributes(attributeMap, "session"),
		MinBits:                   minBitsAttr,
		MinBitsEmote:              minBitsEmoteAttr,
		PinTopCheers:              OptionalBoolFromAttributes(attributeMap, "pinTopCheers"),
		PinRecentCheers:           OptionalBoolFromAttributes(attributeMap, "pinRecentCheers"),
		RecentCheerMin:            recentCheerMin,
		RecentCheerTimeout:        recentCheerTimeout,
		CustomCheermoteImageSetId: OptionalStringFromAttributes(attributeMap, "customCheermoteImageSetId"),
		CustomCheermoteStatus:     OptionalStringFromAttributes(attributeMap, "customCheermoteStatus"),
		AmendmentState:            OptionalStringFromAttributes(attributeMap, "amendmentState"),
		LastOnboardedEventTime:    onboardedEventTime,
		LeaderboardEnabled:        OptionalBoolFromAttributes(attributeMap, "leaderboardEnabled"),
		LeaderboardTimePeriod:     OptionalStringFromAttributes(attributeMap, "leaderboardTimePeriod"),
		CheerBombEventOptOut:      OptionalBoolFromAttributes(attributeMap, "cheerBombEventOptOut"),
		OptedOutOfVoldemort:       OptionalBoolFromAttributes(attributeMap, "optedOutOfVoldemort"),
	}, nil
}

func (u *Channel) GetTable() DynamoTable {
	return &ChannelsTable{}
}

func (u *Channel) NewAttributeMap() map[string]*dynamodb.AttributeValue {
	itemMap := make(map[string]*dynamodb.AttributeValue)

	// Required attributes
	PopulateAttributesString(itemMap, "id", string(u.Id))
	PopulateAttributesTime(itemMap, "lastUpdated", u.LastUpdated)

	// Optional attributes
	PopulateAttributesOptionalBool(itemMap, "optedOut", u.OptedOut)
	PopulateAttributesOptionalBool(itemMap, "onboarded", u.Onboarded)
	PopulateAttributesOptionalString(itemMap, "annotation", u.Annotation)
	PopulateAttributesOptionalInt64(itemMap, "minBits", u.MinBits)
	PopulateAttributesOptionalInt64(itemMap, "minBitsEmote", u.MinBitsEmote)
	PopulateAttributesOptionalBool(itemMap, "pinTopCheers", u.PinTopCheers)
	PopulateAttributesOptionalBool(itemMap, "pinRecentCheers", u.PinRecentCheers)
	PopulateAttributesOptionalInt64(itemMap, "recentCheerMin", u.RecentCheerMin)
	PopulateAttributesOptionalInt64(itemMap, "recentCheerTimeout", u.RecentCheerTimeout)
	PopulateAttributesOptionalString(itemMap, "session", u.SessionId)
	PopulateAttributesOptionalString(itemMap, "customCheermoteImageSetId", u.CustomCheermoteImageSetId)
	PopulateAttributesOptionalString(itemMap, "customCheermoteStatus", u.CustomCheermoteStatus)
	PopulateAttributesOptionalString(itemMap, "amendmentState", u.AmendmentState)
	PopulateAttributesOptionalInt64(itemMap, "lastOnboardedEventTime", u.LastOnboardedEventTime)
	PopulateAttributesOptionalBool(itemMap, "leaderboardEnabled", u.LeaderboardEnabled)
	PopulateAttributesOptionalString(itemMap, "leaderboardTimePeriod", u.LeaderboardTimePeriod)
	PopulateAttributesOptionalBool(itemMap, "cheerBombEventOptOut", u.CheerBombEventOptOut)
	PopulateAttributesOptionalBool(itemMap, "optedOutOfVoldemort", u.OptedOutOfVoldemort)

	return itemMap
}

func (u *Channel) NewItemKey() map[string]*dynamodb.AttributeValue {
	keyMap := make(map[string]*dynamodb.AttributeValue)
	keyMap["id"] = &dynamodb.AttributeValue{S: aws.String(string(u.Id))}
	return keyMap
}

func (u *Channel) NewHashKeyEqualsExpression() string {
	return "id = :id"
}

func (u *Channel) NewHashKeyExpressionAttributeValues() map[string]*dynamodb.AttributeValue {
	attributeValue := &dynamodb.AttributeValue{
		S: aws.String(string(u.Id)),
	}
	attributeMap := make(map[string]*dynamodb.AttributeValue)
	attributeMap[":id"] = attributeValue
	return attributeMap
}

func (u *Channel) UpdateWithCurrentTimestamp() {
	u.LastUpdated = time.Now()
}

func (u *Channel) GetTimestamp() time.Time {
	return u.LastUpdated
}

func (u *Channel) ApplyTimestamp(timestamp time.Time) {
	u.LastUpdated = timestamp
}

func (u *Channel) Equals(other DynamoTableRecord) bool {
	otherChannel, isChannel := other.(*Channel)
	if !isChannel {
		return false
	}

	return u.Id == otherChannel.Id &&
		pointers.BoolPEquals(u.OptedOut, otherChannel.OptedOut) &&
		pointers.BoolPEquals(u.Onboarded, otherChannel.Onboarded) &&
		pointers.StringPEquals(u.Annotation, otherChannel.Annotation) &&
		pointers.Int64PEquals(u.MinBits, otherChannel.MinBits) &&
		pointers.Int64PEquals(u.MinBitsEmote, otherChannel.MinBitsEmote) &&
		pointers.StringPEquals(u.SessionId, otherChannel.SessionId) &&
		pointers.BoolPEquals(u.PinTopCheers, otherChannel.PinTopCheers) &&
		pointers.BoolPEquals(u.PinRecentCheers, otherChannel.PinRecentCheers) &&
		pointers.Int64PEquals(u.RecentCheerMin, otherChannel.RecentCheerMin) &&
		pointers.Int64PEquals(u.RecentCheerTimeout, otherChannel.RecentCheerTimeout) &&
		pointers.StringPEquals(u.CustomCheermoteImageSetId, otherChannel.CustomCheermoteImageSetId) &&
		pointers.StringPEquals(u.CustomCheermoteStatus, otherChannel.CustomCheermoteStatus) &&
		pointers.StringPEquals(u.AmendmentState, otherChannel.AmendmentState) &&
		pointers.Int64PEquals(u.LastOnboardedEventTime, otherChannel.LastOnboardedEventTime) &&
		pointers.BoolPEquals(u.LeaderboardEnabled, otherChannel.LeaderboardEnabled) &&
		pointers.StringPEquals(u.LeaderboardTimePeriod, otherChannel.LeaderboardTimePeriod) &&
		pointers.BoolPEquals(u.CheerBombEventOptOut, otherChannel.CheerBombEventOptOut) &&
		pointers.BoolPEquals(u.OptedOutOfVoldemort, otherChannel.OptedOutOfVoldemort)

}

func (u *Channel) EqualsWithExpectedLastUpdated(other DynamoTableRecord, lastUpdatedTime time.Time, lastUpdatedDelta time.Duration) bool {
	return u.Equals(other) && timeUtils.IsTimeInForwardDelta(other.GetTimestamp(), lastUpdatedTime, lastUpdatedDelta)
}

func NewChannelDao(client *DynamoClient) IChannelDao {
	dao := &ChannelDao{BaseDao{
		client: client,
		table:  &ChannelsTable{},
	}}
	return dao
}

func (dao *ChannelDao) Update(user *Channel) error {
	return dao.client.UpdateItem(user)
}

func (dao *ChannelDao) Get(id ChannelId) (*Channel, error) {
	channelChan := make(chan *Channel, 1)
	fn := func() error {
		channel, err := dao.get(id)
		if err != nil {
			return err
		}
		channelChan <- channel
		return nil
	}

	err := hystrix.Do(cmd.GetChannelCommand, fn, nil)
	if err != nil {
		return nil, err
	}

	channel := <-channelChan
	return channel, nil
}

func (dao *ChannelDao) get(id ChannelId) (*Channel, error) {
	var channelResult *Channel

	channelQuery := &Channel{
		Id: id,
	}

	result, err := dao.client.GetItemWithConsistentRead(channelQuery)
	if err != nil {
		return channelResult, err
	}

	// Result will be nil if no item is found in dynamo
	if result == nil {
		return nil, nil
	}

	channelResult, isChannel := result.(*Channel)
	if !isChannel {
		msg := "Encountered an unexpected type while converting dynamo result to channel"
		log.Error(msg)
		return channelResult, errors.New(msg)
	}

	return channelResult, nil
}

func (dao *ChannelDao) GetAll() ([]*Channel, error) {
	filter := ScanFilter{
		Names: map[string]*string{
			"#ID": aws.String("TargetTwitchUserId"),
		},
		Values: map[string]*dynamodb.AttributeValue{
			":id": {S: aws.String(string("0"))},
		},

		Expression: aws.String("#ID <> :id"),
	}

	result, err := dao.client.ScanTable(&ChannelsTable{}, filter)
	if err != nil {
		return nil, err
	}

	// Need to type assert the slice of records
	channelResult := make([]*Channel, len(result))
	for i, record := range result {
		var isChannel bool
		channelResult[i], isChannel = record.(*Channel)
		if !isChannel {
			msg := "Encountered an unexpected type while converting dynamo result to channel record"
			log.Error(msg)
			return channelResult, errors.New(msg)
		}
	}

	return channelResult, nil
}

func (dao *ChannelDao) GetByImageSetId(imageSetId string) (*Channel, error) {
	channels := make(chan *Channel, 1)
	fn := func() error {
		image, err := dao.getByImageSetId(imageSetId)
		if err != nil {
			return err
		}
		channels <- image
		return nil
	}

	err := hystrix.Do(cmd.GetChannelByImageSetIdCommand, fn, nil)
	if err != nil {
		return nil, err
	}

	channel := <-channels
	return channel, nil
}

func (dao *ChannelDao) getByImageSetId(imageSetId string) (*Channel, error) {
	table := &ChannelsTable{}
	queryFilter := QueryFilter{
		Names: map[string]*string{
			"#ID": aws.String("customCheermoteImageSetId"),
		},
		Values: map[string]*dynamodb.AttributeValue{
			":id": {S: aws.String(string(imageSetId))},
		},
		KeyConditionExpression: aws.String("#ID = :id"),
		Limit:                  pointers.Int64P(1),
		Descending:             false,
		IndexName:              pointers.StringP("customCheermoteImageSetId-index"),
	}

	result, _, err := dao.client.QueryTable(table, queryFilter)
	if err != nil {
		return nil, err
	}

	if len(result) == 0 {
		return nil, nil
	}

	if len(result) > 1 {
		msg := "Error querying for channel by imageSetId. Found multiple results but only expected 1."
		log.WithFields(log.Fields{
			"imagesSetId": imageSetId,
			"numResults":  len(result),
		}).Error(msg)
		return nil, errors.New(msg)
	}

	record := result[0]
	channel, isChannel := record.(*Channel)
	if !isChannel {
		msg := "Non-channel query result for channel by imageSetId"
		log.WithField("imageSetId", imageSetId).Error(msg)
		return nil, errors.New(msg)
	}

	return channel, nil
}

func (dao *ChannelDao) Delete(channelId ChannelId) error {
	deletionRecord := &Channel{
		Id: channelId,
	}
	return dao.client.DeleteItem(deletionRecord)
}

func (u *ChannelsTable) ConvertScanOutputToRecords(so *dynamodb.ScanOutput) ([]DynamoScanQueryTableRecord, error) {
	records := make([]DynamoScanQueryTableRecord, *so.Count)

	for i, attributes := range so.Items {
		recordToAdd, err := u.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func (u *ChannelsTable) ConvertQueryOutputToRecords(qo *dynamodb.QueryOutput) ([]DynamoScanQueryTableRecord, error) {
	records := make([]DynamoScanQueryTableRecord, *qo.Count)

	for i, attributes := range qo.Items {
		recordToAdd, err := u.ConvertAttributeMapToRecord(attributes)
		if err != nil {
			return nil, err
		}

		scanQueryRecord, err := recordToScanQueryRecord(recordToAdd)
		if err != nil {
			return nil, err
		}

		records[i] = scanQueryRecord
	}
	return records, nil
}

func (u *ChannelDao) UpdateIfStatusIsNewer(channel Channel) error {
	statusLastUpdatedString := strconv.FormatInt(pointers.Int64OrDefault(channel.LastOnboardedEventTime, 0), 10)

	fullTableName := u.client.GetFullTableName(channel.GetTable())
	input := NewUpdateItemInput(fullTableName, &channel)
	input.ConditionExpression = aws.String("lastOnboardedEventTime < :val OR attribute_not_exists(lastOnboardedEventTime)")
	input.ExpressionAttributeValues = map[string]*dynamodb.AttributeValue{
		":val": {
			N: aws.String(statusLastUpdatedString),
		},
	}

	buffer := new(bytes.Buffer)
	counter := 0
	_, _ = fmt.Fprint(buffer, "SET")
	for k, v := range input.AttributeUpdates {
		key := fmt.Sprintf(":value%d", counter)
		_, _ = fmt.Fprintf(buffer, " %s = %s", k, key)
		input.ExpressionAttributeValues[key] = v.Value
		counter++
		if counter < len(input.AttributeUpdates) {
			_, _ = fmt.Fprint(buffer, ",")
		}
	}

	input.AttributeUpdates = nil
	input.UpdateExpression = aws.String(buffer.String())
	_, err := u.client.Dynamo.UpdateItem(input)
	return err
}

func (u *Channel) GetScanQueryTable() DynamoScanQueryTable {
	return &ChannelsTable{}
}
