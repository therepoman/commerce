package payday

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/url"
	"strconv"

	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/foundation/twitchclient"
)

const (
	defaultStatSampleRate = 1.0
	defaultTimingXactName = "payday"
)

type clientImpl struct {
	twitchclient.Client
}

type Client interface {
	GetChannelImages(ctx context.Context, channelId string, reqOpts *twitchclient.ReqOpts) (*api.GetChannelImagesResponse, error)
	GetBanned(ctx context.Context, userId string, reqOpts *twitchclient.ReqOpts) (*api.GetBannedResponse, error)
	SetBanned(ctx context.Context, userId string, request *api.SetBannedRequest, reqOpts *twitchclient.ReqOpts) error
	GetOptOut(ctx context.Context, userId string, reqOpts *twitchclient.ReqOpts) (*api.GetOptOutResponse, error)
	SetOptOut(ctx context.Context, userId string, request *api.SetOptOutRequest, reqOpts *twitchclient.ReqOpts) error
	AddBits(ctx context.Context, request *api.AddBitsRequest, reqOpts *twitchclient.ReqOpts) error
	RemoveBits(ctx context.Context, request *api.RemoveBitsRequest, reqOpts *twitchclient.ReqOpts) error
	GetUserEvents(ctx context.Context, userId string, cursor string, limit int64, reqOpts *twitchclient.ReqOpts) (*api.UserEventsResponse, error)
	GetActions(ctx context.Context, request api.GetActionsRequest, reqOpts *twitchclient.ReqOpts) (*api.GetActionsResponse, error)
	GetActionAsset(ctx context.Context, options api.GetActionAssetOptions, reqOpts *twitchclient.ReqOpts) (*api.GetActionAssetResponse, error)
	GetActionsInfo(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*api.GetActionsInfoResponse, error)
	GetWithholdingByAccountId(ctx context.Context, accountId string, reqOpts *twitchclient.ReqOpts) (*api.GetWithholdingByAccountIdResponse, error)
	GetWithholdingByPayoutEntityId(ctx context.Context, payoutEntityId string, reqOpts *twitchclient.ReqOpts) (*api.GetWithholdingByPayoutEntityIdResponse, error)
	GetTournament(ctx context.Context, userId string, reqOpts *twitchclient.ReqOpts) (*api.Tournament, error)
	GetTournamentTeamProgress(ctx context.Context, tournament string, teamId string, userId string, reqOpts *twitchclient.ReqOpts) (*api.GetProgressResponse, error)
	GetTournamentGlobalProgress(ctx context.Context, tournament string, userId string, reqOpts *twitchclient.ReqOpts) (*api.GetProgressResponse, error)
	GetTournamentUserLeaderboard(ctx context.Context, tournament string, userId string, reqOpts *twitchclient.ReqOpts) (*api.GetLeaderboardResponse, error)
	GetTournamentTeamLeaderboard(ctx context.Context, tournament string, teamId string, reqOpts *twitchclient.ReqOpts) (*api.GetLeaderboardResponse, error)
	GetTournamentTeamFanLeaderboard(ctx context.Context, tournament string, teamId string, userId string, reqOpts *twitchclient.ReqOpts) (*api.GetLeaderboardResponse, error)
	GetTournamentUserRewards(ctx context.Context, tournament string, userId string, reqOpts *twitchclient.ReqOpts) (*api.GetTournamentUserRewardsResponse, error)
	ClaimTournamentUserRewards(ctx context.Context, tournament string, request api.ClaimRewardRequest, reqOpts *twitchclient.ReqOpts) error
	GetBitsUsageLeaderboard(ctx context.Context, channelId string, options api.GetLeaderboardOptions, reqOpts *twitchclient.ReqOpts) (*api.GetLeaderboardResponse, error)
	GetBitsProducts(ctx context.Context, userId, country, locale, platform string, reqOpts *twitchclient.ReqOpts) ([]api.Product, error)
	GetChannelInfo(ctx context.Context, channelId string, reqOpts *twitchclient.ReqOpts) (*api.ChannelEligibleResponse, error)
	ListTags(ctx context.Context, first int, after string, reqOpts *twitchclient.ReqOpts) (*api.GetSupportedTagsResponse, error)
	GetBalance(ctx context.Context, userId string, channelId string, reqOpts *twitchclient.ReqOpts) (*api.GetBalanceResponse, error)
	GetPrices(ctx context.Context, request api.GetPricesRequest, reqOpts *twitchclient.ReqOpts) (*api.GetPricesResponse, error)
	GetUserInfo(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*api.GetUserInfoResponse, error)
	GetChannelSettings(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*api.GetSettingsResponse, error)
	UpdateChannelSettings(ctx context.Context, channelID string, params api.UpdateSettingsRequest, reqOpts *twitchclient.ReqOpts) error
	GetBadgeTiers(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*api.GetBadgesResponse, error)
	UploadCustomCheermotes(ctx context.Context, params *api.UploadCustomCheermoteRequest, reqOpts *twitchclient.ReqOpts) (*api.UploadCustomCheermoteResponse, error)
	GetCustomCheermotes(ctx context.Context, channelId string, reqOpts *twitchclient.ReqOpts) (*api.GetCustomCheermotesResponse, error)
	SetBadgeTiers(ctx context.Context, channelID string, params api.SetBadgesRequest, reqOpts *twitchclient.ReqOpts) (*api.GetBadgesResponse, error)
	GetBadgeTiersAdmin(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*api.GetBadgesResponse, error)
	SetBadgeTiersAdmin(ctx context.Context, channelID string, params api.SetBadgeTiersAdminRequest, reqOpts *twitchclient.ReqOpts) (*api.GetBadgesResponse, error)
	EntitleBits(ctx context.Context, request *api.EntitleBitsRequest, reqOpts *twitchclient.ReqOpts) (*api.EntitleBitsResponse, error)
}

func NewClient(conf twitchclient.ClientConf) (Client, error) {
	if conf.TimingXactName == "" {
		conf.TimingXactName = defaultTimingXactName
	}
	twitchClient, err := twitchclient.NewClient(conf)
	return &clientImpl{twitchClient}, err
}

func (c *clientImpl) ListTags(ctx context.Context, first int, after string, reqOpts *twitchclient.ReqOpts) (*api.GetSupportedTagsResponse, error) {
	req, err := c.Client.NewRequest("GET", "/api/tags", nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("content-type", "application/json")

	q := req.URL.Query()
	if first > 0 {
		q.Add("first", fmt.Sprintf("%d", first))
	}

	if after != "" {
		q.Add("after", after)
	}

	req.URL.RawQuery = q.Encode()

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.list_tags",
		StatSampleRate: defaultStatSampleRate,
	})

	getSupportedTagsResponse := new(api.GetSupportedTagsResponse)
	_, err = c.DoJSON(ctx, getSupportedTagsResponse, req, combinedReqOpts)

	if err != nil {
		return nil, err
	}

	return getSupportedTagsResponse, nil
}

func (c *clientImpl) GetBitsProducts(ctx context.Context, userID, country, locale, platform string, reqOpts *twitchclient.ReqOpts) ([]api.Product, error) {
	req, err := c.Client.NewRequest("GET", "/api/products", nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("content-type", "application/json")

	q := req.URL.Query()
	if userID != "" {
		q.Add("user_id", userID)
	}

	if country != "" {
		q.Add("country", country)
	}

	if locale != "" {
		q.Add("locale", locale)
	}

	if platform != "" {
		q.Add("platform", platform)
	}

	req.URL.RawQuery = q.Encode()

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_products",
		StatSampleRate: defaultStatSampleRate,
	})

	getProductsResponse := []api.Product{}
	_, err = c.DoJSON(ctx, &getProductsResponse, req, combinedReqOpts)

	if err != nil {
		return nil, err
	}

	return getProductsResponse, nil
}

func (c *clientImpl) GetChannelImages(ctx context.Context, channelId string, reqOpts *twitchclient.ReqOpts) (*api.GetChannelImagesResponse, error) {
	req, err := c.Client.NewRequest("GET", "/api/channels/"+channelId+"/images", nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("content-type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_channel_images",
		StatSampleRate: defaultStatSampleRate,
	})
	getChannelImagesResponse := &api.GetChannelImagesResponse{}
	_, err = c.DoJSON(ctx, getChannelImagesResponse, req, combinedReqOpts)

	if err != nil {
		return nil, err
	}

	return getChannelImagesResponse, nil
}

func (c *clientImpl) GetBitsUsageLeaderboard(ctx context.Context, channelId string, options api.GetLeaderboardOptions, reqOpts *twitchclient.ReqOpts) (*api.GetLeaderboardResponse, error) {
	req, err := c.Client.NewRequest("GET", fmt.Sprintf("/api/leaderboards/%s", channelId), nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("content-type", "application/json")

	q := req.URL.Query()
	if options.TopNum > 0 {
		q.Add("top_n", strconv.Itoa(options.TopNum))
	}

	if options.Range > 0 {
		q.Add("range", strconv.Itoa(options.Range))
	}

	if options.TwitchUserId != "" {
		q.Add("tuid", options.TwitchUserId)
	}

	req.URL.RawQuery = q.Encode()

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_channel_leaderboard",
		StatSampleRate: defaultStatSampleRate,
	})

	getLeaderboardResponse := new(api.GetLeaderboardResponse)
	_, err = c.DoJSON(ctx, getLeaderboardResponse, req, combinedReqOpts)

	if err != nil {
		return nil, err
	}

	return getLeaderboardResponse, nil
}

func (c *clientImpl) GetTournament(ctx context.Context, tournament string, reqOpts *twitchclient.ReqOpts) (*api.Tournament, error) {
	req, err := c.Client.NewRequest("GET", fmt.Sprintf("/api/esports/tournaments/%s", tournament), nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("content-type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_tournament",
		StatSampleRate: defaultStatSampleRate,
	})

	getTournamentResponse := new(api.Tournament)
	_, err = c.DoJSON(ctx, getTournamentResponse, req, combinedReqOpts)

	if err != nil {
		return nil, err
	}

	return getTournamentResponse, nil
}

func (c *clientImpl) GetTournamentGlobalProgress(ctx context.Context, tournament string, userId string, reqOpts *twitchclient.ReqOpts) (*api.GetProgressResponse, error) {
	req, err := c.Client.NewRequest("GET", fmt.Sprintf("/api/esports/tournaments/%s/progress", tournament), nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("content-type", "application/json")
	params := req.URL.Query()
	params.Add("tuid", userId)
	req.URL.RawQuery = params.Encode()

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_global_progress",
		StatSampleRate: defaultStatSampleRate,
	})

	getProgressResponse := new(api.GetProgressResponse)
	_, err = c.DoJSON(ctx, getProgressResponse, req, combinedReqOpts)

	if err != nil {
		return nil, err
	}

	return getProgressResponse, nil
}

func (c *clientImpl) GetTournamentTeamProgress(ctx context.Context, tournament string, teamId string, userId string, reqOpts *twitchclient.ReqOpts) (*api.GetProgressResponse, error) {
	req, err := c.Client.NewRequest("GET", fmt.Sprintf("/api/esports/tournaments/%s/progress/teams/%s", tournament, teamId), nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("content-type", "application/json")
	params := req.URL.Query()
	params.Add("tuid", userId)
	req.URL.RawQuery = params.Encode()

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_team_progress",
		StatSampleRate: defaultStatSampleRate,
	})

	getTeamResponse := new(api.GetProgressResponse)
	_, err = c.DoJSON(ctx, getTeamResponse, req, combinedReqOpts)

	if err != nil {
		return nil, err
	}

	return getTeamResponse, nil
}

func (c *clientImpl) GetTournamentUserLeaderboard(ctx context.Context, tournament string, userId string, reqOpts *twitchclient.ReqOpts) (*api.GetLeaderboardResponse, error) {
	req, err := c.Client.NewRequest("GET", fmt.Sprintf("/api/esports/tournaments/%s/leaderboards/user", tournament), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("content-type", "application/json")
	params := req.URL.Query()
	params.Add("tuid", userId)
	req.URL.RawQuery = params.Encode()

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_tournament_global_leaderboard",
		StatSampleRate: defaultStatSampleRate,
	})

	getLeaderboardResponse := &api.GetLeaderboardResponse{}
	_, err = c.DoJSON(ctx, getLeaderboardResponse, req, combinedReqOpts)

	if err != nil {
		return nil, err
	}

	return getLeaderboardResponse, nil
}

func (c *clientImpl) GetTournamentTeamLeaderboard(ctx context.Context, tournament string, teamId string, reqOpts *twitchclient.ReqOpts) (*api.GetLeaderboardResponse, error) {
	req, err := c.Client.NewRequest("GET", fmt.Sprintf("/api/esports/tournaments/%s/leaderboards/team", tournament), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("content-type", "application/json")
	params := req.URL.Query()
	params.Add("team", teamId)
	req.URL.RawQuery = params.Encode()

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_tournament_team_leaderboard",
		StatSampleRate: defaultStatSampleRate,
	})

	getLeaderboardResponse := &api.GetLeaderboardResponse{}
	_, err = c.DoJSON(ctx, getLeaderboardResponse, req, combinedReqOpts)

	if err != nil {
		return nil, err
	}

	return getLeaderboardResponse, nil
}

func (c *clientImpl) GetTournamentTeamFanLeaderboard(ctx context.Context, tournament string, teamId string, userId string, reqOpts *twitchclient.ReqOpts) (*api.GetLeaderboardResponse, error) {
	req, err := c.Client.NewRequest("GET", fmt.Sprintf("/api/esports/tournaments/%s/leaderboards/team/%s", tournament, teamId), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("content-type", "application/json")
	params := req.URL.Query()
	params.Add("tuid", userId)
	req.URL.RawQuery = params.Encode()

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_tournament_team_fan_leaderboard",
		StatSampleRate: defaultStatSampleRate,
	})

	getLeaderboardResponse := &api.GetLeaderboardResponse{}
	_, err = c.DoJSON(ctx, getLeaderboardResponse, req, combinedReqOpts)

	if err != nil {
		return nil, err
	}

	return getLeaderboardResponse, nil
}

func (c *clientImpl) GetTournamentUserRewards(ctx context.Context, tournament string, userId string, reqOpts *twitchclient.ReqOpts) (*api.GetTournamentUserRewardsResponse, error) {
	req, err := c.Client.NewRequest("GET", fmt.Sprintf("/api/esports/tournaments/%s/rewards", tournament), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("content-type", "application/json")
	params := req.URL.Query()
	params.Add("tuid", userId)
	req.URL.RawQuery = params.Encode()

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_tournament_team_user_rewards",
		StatSampleRate: defaultStatSampleRate,
	})

	getTournamentUserRewardsResponse := &api.GetTournamentUserRewardsResponse{}
	_, err = c.DoJSON(ctx, getTournamentUserRewardsResponse, req, combinedReqOpts)

	if err != nil {
		return nil, err
	}

	return getTournamentUserRewardsResponse, nil
}

func (c *clientImpl) ClaimTournamentUserRewards(ctx context.Context, tournament string, request api.ClaimRewardRequest, reqOpts *twitchclient.ReqOpts) error {
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)

	req, err := c.Client.NewRequest("POST", fmt.Sprintf("/api/esports/tournaments/%s/rewards/claim", tournament), requestJsonReader)
	if err != nil {
		return err
	}

	req.Header.Add("content-type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.claim_tournament_user_rewards",
		StatSampleRate: defaultStatSampleRate,
	})

	resp, err := c.Do(ctx, req, combinedReqOpts)

	if err != nil {
		return err
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != 200 {
		return twitchclient.HandleFailedResponse(resp)
	}

	return nil
}

func (c *clientImpl) GetBanned(ctx context.Context, userId string, reqOpts *twitchclient.ReqOpts) (*api.GetBannedResponse, error) {
	req, err := c.Client.NewRequest("GET", "/authed/admin/users/"+userId+"/banned", nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_banned",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	responseJson, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if len(responseJson) == 0 || resp.StatusCode == 404 {
		return nil, nil
	}

	if resp.StatusCode != 200 {
		return nil, twitchclient.HandleFailedResponse(resp)
	}

	response, err := api.ConvertJsonToGetBannedResponse(responseJson)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (c *clientImpl) SetBanned(ctx context.Context, userId string, request *api.SetBannedRequest, reqOpts *twitchclient.ReqOpts) error {
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)

	req, err := c.Client.NewRequest("PUT", "/authed/admin/users/"+userId+"/banned", requestJsonReader)
	if err != nil {
		return err
	}
	req.Header.Add("content-type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.set_banned",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != 200 {
		return twitchclient.HandleFailedResponse(resp)
	}

	return nil
}

func (c *clientImpl) GetOptOut(ctx context.Context, userId string, reqOpts *twitchclient.ReqOpts) (*api.GetOptOutResponse, error) {
	req, err := c.Client.NewRequest("GET", "/authed/admin/channels/"+userId+"/opt-out", nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_opt_out",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode == 404 {
		return nil, nil
	}

	if resp.StatusCode != 200 {
		return nil, twitchclient.HandleFailedResponse(resp)
	}

	responseJson, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	response, err := api.ConvertJsonToGetOptOutResponse(responseJson)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (c *clientImpl) SetOptOut(ctx context.Context, userId string, request *api.SetOptOutRequest, reqOpts *twitchclient.ReqOpts) error {
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)

	req, err := c.Client.NewRequest("PUT", "/authed/admin/channels/"+userId+"/opt-out", requestJsonReader)
	if err != nil {
		return err
	}
	req.Header.Add("content-type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.set_opt_out",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != 200 {
		return twitchclient.HandleFailedResponse(resp)
	}

	return nil
}

func (c *clientImpl) AddBits(ctx context.Context, request *api.AddBitsRequest, reqOpts *twitchclient.ReqOpts) error {
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)

	req, err := c.Client.NewRequest("POST", "/authed/admin/addBits", requestJsonReader)
	if err != nil {
		return err
	}
	req.Header.Add("content-type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.add_bits",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != 200 {
		return twitchclient.HandleFailedResponse(resp)
	}

	return nil
}

func (c *clientImpl) RemoveBits(ctx context.Context, request *api.RemoveBitsRequest, reqOpts *twitchclient.ReqOpts) error {
	requestJson, _ := json.Marshal(request)
	requestJsonReader := bytes.NewReader(requestJson)

	req, err := c.Client.NewRequest("POST", "/authed/admin/removeBits", requestJsonReader)
	if err != nil {
		return err
	}
	req.Header.Add("content-type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.remove_bits",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != 200 {
		return twitchclient.HandleFailedResponse(resp)
	}

	return nil
}

func (c *clientImpl) GetUserEvents(ctx context.Context, userId string, cursor string, limit int64, reqOpts *twitchclient.ReqOpts) (*api.UserEventsResponse, error) {
	queryParams := make(url.Values)
	queryParams["cursor"] = []string{cursor}
	queryParams["limit"] = []string{strconv.FormatInt(limit, 10)}
	queryStr := queryParams.Encode()

	url := fmt.Sprintf("/api/users/%s/events?%s", userId, queryStr)
	req, err := c.Client.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_user_events",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != 200 {
		return nil, twitchclient.HandleFailedResponse(resp)
	}

	responseJson, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	response, err := api.ConvertJsonToUserEventsResponse(responseJson)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (c *clientImpl) GetActions(ctx context.Context, request api.GetActionsRequest, reqOpts *twitchclient.ReqOpts) (*api.GetActionsResponse, error) {
	req, err := c.Client.NewRequest("GET", "/api/actions", nil)
	if err != nil {
		return nil, err
	}

	q := req.URL.Query()
	for _, prefix := range request.Prefixes {
		q.Add("prefixes", prefix)
	}

	if request.ChannelId != "" {
		q.Add("channel_id", request.ChannelId)
	}

	if request.UserId != "" {
		q.Add("user_id", request.UserId)
	}

	q.Add("include_sponsored", strconv.FormatBool(request.IncludeSponsored))
	q.Add("include_upper_tiers", strconv.FormatBool(request.IncludeUpperTiers))

	req.URL.RawQuery = q.Encode()

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_actions",
		StatSampleRate: defaultStatSampleRate,
	})

	var actions api.GetActionsResponse
	_, err = c.DoJSON(ctx, &actions, req, combinedReqOpts)

	if err != nil {
		return nil, err
	}

	return &actions, err
}

func (c *clientImpl) GetWithholdingByAccountId(ctx context.Context, accountId string, reqOpts *twitchclient.ReqOpts) (*api.GetWithholdingByAccountIdResponse, error) {
	req, err := c.Client.NewRequest("GET", "/authed/admin/withholding/"+accountId, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("content-type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_withholding",
		StatSampleRate: defaultStatSampleRate,
	})
	withholding := &api.GetWithholdingByAccountIdResponse{}
	resp, err := c.DoJSON(ctx, withholding, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != 200 {
		return nil, twitchclient.HandleFailedResponse(resp)
	}
	return withholding, nil
}

func (c *clientImpl) GetWithholdingByPayoutEntityId(ctx context.Context, payoutEntityId string, reqOpts *twitchclient.ReqOpts) (*api.GetWithholdingByPayoutEntityIdResponse, error) {
	req, err := c.Client.NewRequest("GET", "/authed/admin/withholding/account/"+payoutEntityId, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("content-type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_withholding_by_payout_id",
		StatSampleRate: defaultStatSampleRate,
	})
	withholding := &api.GetWithholdingByPayoutEntityIdResponse{}
	resp, err := c.DoJSON(ctx, withholding, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != 200 {
		return nil, twitchclient.HandleFailedResponse(resp)
	}
	return withholding, nil
}

func (c *clientImpl) GetChannelInfo(ctx context.Context, channelId string, reqOpts *twitchclient.ReqOpts) (*api.ChannelEligibleResponse, error) {
	req, err := c.Client.NewRequest("GET", fmt.Sprintf("/api/channels/%v", channelId), nil)

	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_channel_info",
		StatSampleRate: defaultStatSampleRate,
	})

	getChannelInfo := api.ChannelEligibleResponse{}
	_, err = c.DoJSON(ctx, &getChannelInfo, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getChannelInfo, nil
}

func (c *clientImpl) GetBalance(ctx context.Context, userId string, channelId string, reqOpts *twitchclient.ReqOpts) (*api.GetBalanceResponse, error) {
	req, err := c.Client.NewRequest("GET", fmt.Sprintf("/api/balance/%s", userId), nil)
	if err != nil {
		return nil, err
	}

	q := req.URL.Query()
	if channelId != "" {
		q.Add("channel", channelId)
	}

	req.URL.RawQuery = q.Encode()

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_balance",
		StatSampleRate: defaultStatSampleRate,
	})

	getBalanceResponse := api.GetBalanceResponse{}
	_, err = c.DoJSON(ctx, &getBalanceResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getBalanceResponse, nil
}

func (c *clientImpl) GetUserInfo(ctx context.Context, userID string, reqOpts *twitchclient.ReqOpts) (*api.GetUserInfoResponse, error) {
	req, err := c.Client.NewRequest("GET", fmt.Sprintf("/api/users/%s", userID), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_user_info",
		StatSampleRate: defaultStatSampleRate,
	})

	getUserInfoResponse := api.GetUserInfoResponse{}
	_, err = c.DoJSON(ctx, &getUserInfoResponse, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &getUserInfoResponse, nil
}

func (c *clientImpl) GetPrices(ctx context.Context, request api.GetPricesRequest, reqOpts *twitchclient.ReqOpts) (*api.GetPricesResponse, error) {
	query := url.Values{}
	if request.UserID != "" {
		query.Add("user_id", request.UserID)
	}

	if len(request.ASINs) > 0 {
		for _, asin := range request.ASINs {
			query.Add("asins", asin)
		}
	}

	if request.Platform != "" {
		query.Add("platform", request.Platform)
	}

	if request.CountryOfResidence != "" {
		query.Add("cor", request.CountryOfResidence)
	}

	req, err := c.NewRequest("GET", fmt.Sprintf("/api/prices?%s", query.Encode()), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_prices",
		StatSampleRate: defaultStatSampleRate,
	})

	resp := api.GetPricesResponse{}
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, err
}

func (c *clientImpl) GetChannelSettings(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*api.GetSettingsResponse, error) {
	if channelID == "" {
		return nil, errors.New("channel ID is required")
	}

	req, err := c.NewRequest("GET", fmt.Sprintf("/api/channels/%s/settings", channelID), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_channel_settings",
		StatSampleRate: defaultStatSampleRate,
	})

	resp := api.GetSettingsResponse{}
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, err
}

func (c *clientImpl) UpdateChannelSettings(ctx context.Context, channelID string, params api.UpdateSettingsRequest, reqOpts *twitchclient.ReqOpts) error {
	if channelID == "" {
		return errors.New("channel ID is required")
	}

	requestJSON, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := c.NewRequest("PUT", fmt.Sprintf("/api/channels/%s/settings", channelID), bytes.NewReader(requestJSON))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.update_channel_settings",
		StatSampleRate: defaultStatSampleRate,
	})

	httpResp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer func() {
		_ = httpResp.Body.Close()
	}()

	if httpResp.StatusCode != 200 {
		return twitchclient.HandleFailedResponse(httpResp)
	}

	return nil
}

func (c *clientImpl) GetBadgeTiers(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*api.GetBadgesResponse, error) {
	if channelID == "" {
		return nil, errors.New("channel ID is required")
	}

	req, err := c.NewRequest("GET", fmt.Sprintf("/api/badges/%s/tiers", channelID), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_badge_tiers",
		StatSampleRate: defaultStatSampleRate,
	})

	resp := api.GetBadgesResponse{}
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, err
}

func (c *clientImpl) SetBadgeTiers(ctx context.Context, channelID string, params api.SetBadgesRequest, reqOpts *twitchclient.ReqOpts) (*api.GetBadgesResponse, error) {
	if channelID == "" {
		return nil, errors.New("channel ID is required")
	}

	requestJSON, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("PUT", fmt.Sprintf("/api/badges/%s/tiers", channelID), bytes.NewReader(requestJSON))
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.set_badge_tiers",
		StatSampleRate: defaultStatSampleRate,
	})

	resp := api.GetBadgesResponse{}
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *clientImpl) GetBadgeTiersAdmin(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*api.GetBadgesResponse, error) {
	if channelID == "" {
		return nil, errors.New("channel ID is required")
	}

	req, err := c.NewRequest("GET", fmt.Sprintf("/authed/admin/badges/%s/tiers", channelID), nil)
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_badge_tiers",
		StatSampleRate: defaultStatSampleRate,
	})

	resp := api.GetBadgesResponse{}
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, err
}

func (c *clientImpl) SetBadgeTiersAdmin(ctx context.Context, channelID string, params api.SetBadgeTiersAdminRequest, reqOpts *twitchclient.ReqOpts) (*api.GetBadgesResponse, error) {
	if channelID == "" {
		return nil, errors.New("channel ID is required")
	}

	requestJSON, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("PATCH", fmt.Sprintf("/authed/admin/badges/%s/tiers", channelID), bytes.NewReader(requestJSON))
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.set_badge_tiers",
		StatSampleRate: defaultStatSampleRate,
	})

	resp := api.GetBadgesResponse{}
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *clientImpl) GetActionAsset(ctx context.Context, options api.GetActionAssetOptions, reqOpts *twitchclient.ReqOpts) (*api.GetActionAssetResponse, error) {
	if options.ChannelID == "" && options.Prefix == "" {
		return nil, errors.New("channel ID or prefix is required")
	}

	req, err := c.NewRequest("GET", "/api/actions/asset", nil)
	if err != nil {
		return nil, err
	}

	q := req.URL.Query()
	if options.Prefix != "" {
		q.Add("prefix", options.Prefix)
	} else if options.ChannelID != "" {
		q.Add("channel_id", options.ChannelID)
	}

	if options.Background != "" {
		q.Add("background", string(options.Background))
	}

	if options.Scale != "" {
		q.Add("scale", string(options.Scale))
	}

	if options.State != "" {
		q.Add("state", string(options.State))
	}

	if options.Bits > 0 {
		q.Add("bits", strconv.FormatInt(options.Bits, 10))
	}

	req.URL.RawQuery = q.Encode()

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_action_asset",
		StatSampleRate: defaultStatSampleRate,
	})

	resp := api.GetActionAssetResponse{}
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)

	return &resp, err
}

func (c *clientImpl) GetActionsInfo(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*api.GetActionsInfoResponse, error) {
	if channelID == "" {
		return nil, errors.New("channel ID is required")
	}

	req, err := c.NewRequest("GET", "/api/actions/info", nil)
	if err != nil {
		return nil, err
	}

	q := req.URL.Query()
	q.Add("channel_id", channelID)
	req.URL.RawQuery = q.Encode()

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_actions_options",
		StatSampleRate: defaultStatSampleRate,
	})

	resp := api.GetActionsInfoResponse{}
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)

	return &resp, err
}

func (c *clientImpl) UploadCustomCheermotes(ctx context.Context, params *api.UploadCustomCheermoteRequest, reqOpts *twitchclient.ReqOpts) (*api.UploadCustomCheermoteResponse, error) {
	requestJSON, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := c.NewRequest("POST", "/api/customcheermotes", bytes.NewReader(requestJSON))
	if err != nil {
		return nil, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.upload_custom_cheermotes",
		StatSampleRate: defaultStatSampleRate,
	})

	resp := api.UploadCustomCheermoteResponse{}
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)

	if err != nil {
		return nil, err
	}

	return &resp, err
}

func (c *clientImpl) GetCustomCheermotes(ctx context.Context, channelId string, reqOpts *twitchclient.ReqOpts) (*api.GetCustomCheermotesResponse, error) {
	req, err := c.Client.NewRequest("GET", "/api/channels/"+channelId+"/customcheermotes", nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("content-type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.get_custom_cheermotes",
		StatSampleRate: defaultStatSampleRate,
	})
	getCustomCheermotesResponse := &api.GetCustomCheermotesResponse{}
	_, err = c.DoJSON(ctx, getCustomCheermotesResponse, req, combinedReqOpts)

	if err != nil {
		return nil, err
	}

	return getCustomCheermotesResponse, nil
}

func (c *clientImpl) EntitleBits(ctx context.Context, request *api.EntitleBitsRequest, reqOpts *twitchclient.ReqOpts) (*api.EntitleBitsResponse, error) {
	requestJSON, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}
	requestJsonReader := bytes.NewReader(requestJSON)

	req, err := c.Client.NewRequest("POST", "/authed/entitleBits", requestJsonReader)
	if err != nil {
		return nil, err
	}
	req.Header.Add("content-type", "application/json")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.payday.entitle_bits",
		StatSampleRate: defaultStatSampleRate,
	})

	resp := api.EntitleBitsResponse{}
	_, err = c.DoJSON(ctx, &resp, req, combinedReqOpts)
	if err != nil {
		return nil, err
	}

	return &resp, err
}
