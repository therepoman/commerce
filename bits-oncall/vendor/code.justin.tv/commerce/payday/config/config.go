package config

import (
	"context"
	"encoding/json"
	"fmt"
	"net"
	"os"
	"sync"
	"time"

	"code.justin.tv/commerce/gogogadget/http"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/clients/sandstorm"
	"code.justin.tv/commerce/payday/file"
	"code.justin.tv/commerce/payday/models/api"
	valid "github.com/asaskevich/govalidator"
)

const (
	DevelopmentEnvironment = "development"
	StagingEnvironment     = "staging"
	ProductionEnvironment  = "production"

	EC2MetadataURL = "http://169.254.169.254/latest/meta-data/local-ipv4"
)

type Configuration struct {
	// AWS SQS configuration
	SQSRegion                              string `valid:"required"`
	RaincatcherSQSQueueURL                 string `valid:"url"`
	PubSubSQSQueueURL                      string `valid:"url"`
	DataScienceSQSQueueURL                 string `valid:"url"`
	BigBitsSQSQueueURL                     string `valid:"url"`
	HashTagSQSQueueURL                     string `valid:"url"`
	BitEventV1SQSQueueURL                  string `valid:"url"`
	BitCacheSQSQueueURL                    string `valid:"url"`
	ImageModerationSQSQueueURL             string `valid:"url"`
	ImageUpdatesSQSQueueURL                string `valid:"url"`
	HGCLeaderboardIngestionSQSQueueURL     string `valid:"url"`
	ChannelLeaderboardIngestionSQSQueueURL string `valid:"url"`
	PartnerOnboardingSQSQueueURL           string `valid:"url"`
	AdminJobSQSQueueURL                    string `valid:"url"`
	PantheonEventSQSQueueURL               string `valid:"url"`
	ExtensionsPubSubSQSQueueURL            string `valid:"url"`
	UserModerationSQSQueueURL              string `valid:"url"`
	ChannelSettingsSQSQueueURL             string `valid:"url"`
	RevenueSQSQueueURL                     string `valid:"url"`
	BadgeSQSQueueURL                       string `valid:"url"`
	AutoModSQSQueueURL                     string `valid:"url"`
	PrometheusSQSQueueURL                  string `valid:"url"`
	PushySQSQueueURL                       string `valid:"url"`
	PhantoSQSQueueURL                      string `valid:"url"`
	UserStateQueueURL                      string `valid:"url"`
	EventbusQueueURL                       string `valid:"url"`

	KMSRegion string `valid:"required"`

	// AWS Firehose configuration
	FirehoseRegion                  string `valid:"required"`
	TransactionDeliveryStreamName   string
	EmoteUsesDeliveryStreamName     string
	BalanceUpdateDeliveryStreamName string
	TransactionAuditStreamName      string

	// AWS Dynamo configuration
	DynamoRegion      string `valid:"required"`
	DynamoTablePrefix string `valid:"required"`

	// SWS portal configuration
	SWSHostAddress string `valid:"required"`
	SWSHostPort    uint16 `valid:"required"`

	// Middleware request validation
	JsonContentTypeEnforcement   bool
	OriginEnforcement            bool
	CartmanEnforcement           bool
	ForwardedProtocolEnforcement bool

	// Eligibility cache configuration
	PartnerCacheExpirationMinutes int64 `valid:"required"`
	PartnerCacheCleanupMinutes    int64 `valid:"required"`
	DefaultCacheExpirationMinutes int64 `valid:"required"`
	DefaultCacheCleanupMinutes    int64 `valid:"required"`

	// Payday Server Properties
	AdminPort      int64 `valid:"required"`
	PaydayRestPort int64 `valid:"required"`

	// DataScience configuration
	DataScienceEnabled      bool
	DataScienceSpadeEnabled bool
	DataScienceSpadeURL     string `valid:"url"`

	// Dynamo audit configuration
	AuditRecordsS3Bucket      string `valid:"required"`
	AuditLambdaMemorySizeMB   int64  `valid:"required"`
	AuditLambdaTimeoutSec     int64  `valid:"required"`
	AuditLambdaRoleName       string `valid:"required"`
	AuditLambdaCodeS3Bucket   string `valid:"required"`
	AuditLambdaEventBatchSize int64  `valid:"required"`

	// Pubsub
	PubsubHostAddress     string `valid:"url,required"`
	PubsubControlEndpoint string `valid:"url,required"`

	// TMI
	TMIHostAddress     string `valid:"url,required"`
	TMIFailureSNSTopic string

	// User Service
	UserServiceEndpoint string `valid:"url,required"`

	// Leaderboards Service
	PantheonServiceEndpoint string `valid:"url,required"`

	// Cloudwatch
	CloudwatchBufferSize               int     `valid:"required"`
	CloudwatchBatchSize                int     `valid:"required"`
	CloudwatchFlushIntervalMinutes     int     `valid:"required"`
	CloudwatchFlushCheckDelayMs        int     `valid:"required"`
	CloudwatchEmergencyFlushPercentage float64 `valid:"required"`

	// Integration Tests
	IntegrationTestsEndpoint      string `valid:"url"`
	IntegrationTestsAdminEndpoint string `valid:"url"`

	// Profiling
	ProfilingEnabled bool

	// Include Duplicate Post Endpoints (if cartman isn't around to rewrite HTTP verbs)
	IncludeDuplicatePostEndpoints bool

	// Chat Badges Service
	ChatBadgesHostAddress string `valid:"url,required"`

	// Supported Country Override
	SupportedCountryOverride []string

	ZumaHost          string      `valid:"required"`
	Redis             RedisConfig `valid:"required"`
	RedisEndpoint     string      `valid:"required"`
	RedisReadReplicas []string    `valid:"required"`

	// Kraken (Twitch public API)
	TwitchClientIdFilePath string

	// TODO: Remove this
	// Clips Client
	ClipsOauthTokenFilePath string

	PrefixesEndpoint string

	// Image Processing
	ImageModerationSNSTopic string

	// Custom cheermotes
	CustomCheermoteEnabled                bool
	CustomCheermoteChannelSlackWebhookURL string
	CustomCheermoteBucketName             string
	CustomCheermoteCloudfrontPrefix       string `valid:"url,required"`

	// MoneyPenny (Revenue Service)
	MoneyPennyEndpoint string

	// Ripley
	RipleyServiceEndpoint string `valid:"url,required"`

	// Custom badges
	CustomBadgesChannelSlackWebhookURL string

	// TMI Failures
	TMIFailuresSlackWebhookURL string

	// Summer Games Done Quick (SGDQ) 2017
	SGDQChannel string

	// Bits Purchase Information string
	BitsPurchaseURL string

	HGCConfig HGCConfig

	Environment string
	IpAddress   string

	// Identity Connections Host
	ConnectionsHost string

	MakoSNSTopic string

	TwitchFulfillmentServiceSNSTopic    string
	TwitchFulfillmentServiceSQSQueueURL string

	RespawnRoleARN            string
	CorMapping                map[string]string
	CheckoutPageMapping       map[string]string
	TaxExclusiveCountriesList map[string]bool

	MarkedChannels map[string]bool

	MobileConfig MobileConfig

	CheckoutBaseURL           string
	PaypalEnabledCountries    map[string]bool
	PaypalRestrictedCountries []string
	FloServiceEndpoint        string

	HashTagsBucket string
	HashTagsKey    string

	BitsUsageSNSTopic string

	S2S                           S2SConfig `valid:"required"`
	SandstormRole                 string
	SandtormSWSSecret             string `valid:"sandstorm"`
	SandtormAmazonRootCert        string `valid:"sandstorm"`
	SandstormCartmanRSAPublicKey  string `valid:"sandstorm"`
	SandstormCartmanECCPublicKey  string `valid:"sandstorm"`
	SandstormCartmanHMACSecretKey string `valid:"sandstorm"`
	SandstormClipsOauthToken      string `valid:"sandstorm"`
	SandstormTwitchClientId       string `valid:"sandstorm"`
	SandstormRollbarToken         string `valid:"sandstorm"`

	// Admin Jobs
	AdminJobsSNSTopic string

	// Channel Settings Topic
	ChannelSettingsSNSTopic string

	// Subs service client
	SubsServiceEndpoint string

	EventsBucket string
	EventsKey    string

	// Extensions
	FaladorURL                string `valid:"url"`
	ExtensionsValidatorUrl    string `valid:"url"`
	ExtensionBroadcasterShare float64
	ExtensionDeveloperShare   float64

	// Extension Revshare Whitelist
	ExtensionRevshareWhitelist map[string]bool

	// Pushy Service
	PushyServiceSNSTopic string

	// Revenue Service
	RevenueServiceSNSTopic string

	OWLEndpoint string `valid:"url"`

	UploadServiceURL          string `valid:"url"`
	UploadCallBackSNSTopic    string
	UploadCallBackSQSQueueURL string `valid:"url"`

	Translations BitsTranslations

	// Actions
	ActionsBucket string
	ActionsKey    string

	// Sponsored Cheermote Campaigns
	SponsoredCheermoteCampaignsBucket                         string
	SponsoredCheermoteCampaignsKey                            string
	SponsoredCheermoteCampaignPubsubDefaultRateLimitPerSecond int64

	// Football Campaign
	BitsFootballEventEnabled   bool
	BitsFootballEventWhitelist []string

	AutoModSNSTopic string

	// Prometheus Transaction Service
	PrometheusServiceEndpoint string `valid:"url"`

	EMSEndpoint string `valid:"url"`

	// Raincatcher Audit
	RaincatcherAuditSQSUrl string

	// Event Topics
	GiveBitsToBroadcasterEventSNSTopic string
	UseBitsOnExtensionEventSNSTopic    string

	// Enable middleware for Twirp APIs for Cartman business
	TwirpMiddlewareEnabled bool

	// Dynamo tables
	BadgeTierNotificationTableName             string
	VoldemotePackTableName                     string
	GemNotificationConfigTableName             string
	GemNotificationTableName                   string
	BitsEntitlementTableName                   string
	SinglePurchaseConsumptionsTableName        string
	PurchaseByMostRecentSuperPlatformTableName string
	TransactionTableName                       string
	HoldsTableName                             string

	SWSPingDelaySeconds      int
	PantheonPingDelaySeconds int

	BulkEntitleCampaignsBucket string
	BulkEntitleCampaignsKey    string

	PortentSNSTopic string

	AprilFoolsStartTime string
	AprilFoolsEndTime   string

	PetoziServiceEndpointURL string `valid:"url"`

	PercentRolloutBucket string
	PercentRolloutKey    string

	PachinkoURL string

	NitroServiceEndpoint string `valid:"url"`
}

// make sure that nothing is nil
// returns nil if everything is a-ok; otherwise returns an error containing a list of all fields that are unpopulated / invalid
func (c Configuration) Validate() error {
	manager := sandstorm.New(c.SandstormRole)
	valid.TagMap["filepath"] = valid.Validator(func(str string) bool {
		if _, err := os.Stat(str); os.IsNotExist(err) {
			return false
		}
		return true
	})

	valid.TagMap["sandstorm"] = func(str string) bool {
		secret, err := manager.Get(str)
		if err != nil {
			log.WithError(err).WithField("secret", str).Error("Error getting secret")
		}
		return err == nil && secret != nil
	}

	// TODO: write a verifier for AWS region (I think they're all in the form of: LOCALE-COMPASS_DIRECTION-INTEGER)

	result, err := valid.ValidateStruct(&c)
	if err != nil {
		return err
	}

	if !result {
		return fmt.Errorf("Error occurred while during validation of config file. Additional errors: %v", err)
	}

	return nil
}

type S2SConfig struct {
	Name                   string `valid:"required"`
	Enabled                bool   `valid:"required"`
	createBitsEventEnabled bool
	twirpEnabled           bool
	pachinkoEnabled        bool
}

func (c *S2SConfig) IsCreateBitsEventEnabled() bool {
	return c.Enabled && c.createBitsEventEnabled
}

func (c *S2SConfig) IsTwirpEnabled() bool {
	return c.Enabled && c.twirpEnabled
}

func (c *S2SConfig) IsPachinkoEnabled() bool {
	return c.Enabled && c.pachinkoEnabled
}

type MobileConfig struct {
	IOS     MobilePlatformConfig
	Android MobilePlatformConfig
}

type MobilePlatformConfig struct {
	IAPEnabledCountries []string
}

type HGCConfig struct {
	Actions  []api.Action
	Channels []string
}

type BitsTranslations struct {
	BitsPrimePromo     map[string]string
	BitsFirstTimePromo map[string]string
	BlackFriday2018    map[string]string
}

type RedisConfig struct {
	Cluster    string
	NonCluster string
	Lock       string
}

const configFileName = "%s.json"

var once = new(sync.Once)
var cfg *Configuration

var configFilePaths = []string{"config/", "../config/"}

func getConfigFileName(environment string) string {
	return fmt.Sprintf(configFileName, environment)
}

func getConfigFilePath(environment string) (string, error) {
	fileName := getConfigFileName(environment)
	return file.SearchFileInPaths(fileName, configFilePaths)
}

func IsSet() bool {
	return cfg != nil
}

func Get() Configuration {
	if !IsSet() {
		return Configuration{}
	}

	return *cfg
}

func Load(environment string) error {
	filePath, err := getConfigFilePath(environment)
	if err != nil {
		return err
	}

	configuration, err := loadConfig(filePath)
	if err != nil {
		return err
	}

	configuration.Environment = environment

	hostIp := "unknown"
	if environment != DevelopmentEnvironment {
		client := http.NewHttpUtilClientApi(time.Second, 3, nil)
		resp, _, err := client.HttpGet(context.Background(), EC2MetadataURL)
		if err != nil {
			log.WithError(err).Error("error making request to ec2 metadata service")
		} else {
			if len(resp) > 0 {
				hostIpVar := net.ParseIP(string(resp))
				if hostIpVar != nil {
					hostIp = hostIpVar.String()
				}
			}
		}
	}

	configuration.IpAddress = hostIp

	once.Do(func() {
		cfg = configuration
	})

	return nil
}

func Set(configuration Configuration) {
	once.Do(func() {
		cfg = &configuration
	})
}

func loadConfig(filepath string) (*Configuration, error) {
	file, err := os.Open(filepath)

	if err != nil {
		return nil, fmt.Errorf("Failed while opening config file: %v", err)
	}
	decoder := json.NewDecoder(file)
	configuration := &Configuration{}

	err = decoder.Decode(configuration)
	if err != nil {
		return nil, fmt.Errorf("Failed while parsing configuration file: %v", err)
	}

	return configuration, nil
}
