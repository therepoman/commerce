package pachinko

import (
	"context"
	"net/http"
	"time"

	log "code.justin.tv/commerce/logrus"
	rpc "code.justin.tv/commerce/pachinko/rpc"
	"code.justin.tv/commerce/payday/config"
	cmds "code.justin.tv/commerce/payday/hystrix"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/golang/protobuf/ptypes"
	"github.com/pkg/errors"
)

const (
	GROUP_ID = "BALANCE"
)

var (
	ErrAlreadyCommitted = errors.New("pachinko: transaction is already committed")
)

type PachinkoClient interface {
	GetBalance(ctx context.Context, userID string) (int, error)
	AddBits(ctx context.Context, transactionId string, userId string, bitsQuantity int) (updatedBalance int, err error)
	RemoveBits(ctx context.Context, transactionId string, userId string, bitsQuantity int) (updatedBalance int, err error)
	CheckHold(ctx context.Context, holdID string) (*rpc.Hold, error)
	CreateHold(ctx context.Context, transactionID, userID string, bitsQuantity int, expiresAt *time.Time) (int, error)
	FinalizeHold(ctx context.Context, transactionID, holdID, userID string) (int, error)
	ReleaseHold(ctx context.Context, transactionID, holdID, userID string) (int, error)
}

type pachinkoClient struct {
	client rpc.Pachinko
}

func NewClient(cfg config.Configuration) PachinkoClient {
	var httpClient *http.Client

	if cfg.S2S.IsPachinkoEnabled() {
		rt, err := caller.NewRoundTripper(cfg.S2S.Name, &caller.Config{}, log.New())
		if err != nil {
			log.Panicf("Failed to create s2s round tripper: %v", err)
		}

		httpClient = &http.Client{
			Transport: rt,
		}
	} else {
		httpClient = http.DefaultClient
	}

	return &pachinkoClient{
		rpc.NewPachinkoProtobufClient(cfg.PachinkoURL, httpClient),
	}
}

func (c *pachinkoClient) GetBalance(ctx context.Context, userID string) (int, error) {
	var balance int
	err := hystrix.Do(cmds.PachinkoGetBalanceCommand, func() error {
		resp, err := c.client.GetToken(ctx, &rpc.GetTokenReq{
			Domain:  rpc.Domain_BITS,
			OwnerId: userID,
			GroupId: GROUP_ID,
		})

		if err != nil {
			log.WithFields(log.Fields{
				"UserID": userID,
			}).WithError(err).Warn("failed to fetch bits balance from Pachinko")
		} else if resp == nil || resp.Token == nil {
			log.WithFields(log.Fields{
				"UserID": userID,
			}).WithError(err).Warn("no balance was returned, but should have returned a balance.")
		} else {
			balance = int(resp.Token.Quantity)
		}
		return err
	}, nil)

	return balance, err
}

func (c *pachinkoClient) AddBits(ctx context.Context, transactionId string, userId string, bitsQuantity int) (updatedBalance int, err error) {
	err = hystrix.Do(cmds.PachinkoAddTokensCommand, func() error {
		resp, err := c.client.AddTokens(ctx, &rpc.AddTokensReq{
			TransactionId: transactionId,
			Token: &rpc.Token{
				OwnerId:  userId,
				GroupId:  GROUP_ID,
				Domain:   rpc.Domain_BITS,
				Quantity: int64(bitsQuantity),
			},
		})

		if err != nil {
			log.WithFields(log.Fields{
				"EventId":      transactionId,
				"TwitchUserId": userId,
				"Quantity":     bitsQuantity,
			}).WithError(err).Warn("Error doing AddTokens in Pachinko call.")
		} else {
			updatedBalance = int(resp.Token.Quantity)
		}
		return err
	}, nil)
	return
}

func (c *pachinkoClient) RemoveBits(ctx context.Context, transactionId string, userId string, bitsQuantity int) (int, error) {
	return c.performTransaction(ctx, userId, &rpc.StartTransactionReq{
		TransactionId: transactionId,
		Operation:     rpc.Operation_CONSUME,
		Tokens: []*rpc.Token{
			{
				OwnerId:  userId,
				GroupId:  GROUP_ID,
				Domain:   rpc.Domain_BITS,
				Quantity: int64(bitsQuantity),
			},
		},
	})
}

func (c *pachinkoClient) CheckHold(ctx context.Context, holdID string) (*rpc.Hold, error) {
	var hold *rpc.Hold
	err := hystrix.Do(cmds.PachinkoCheckHoldCommand, func() error {
		resp, err := c.client.CheckHold(ctx, &rpc.CheckHoldReq{
			TransactionId: holdID,
		})

		if err != nil {
			log.WithFields(log.Fields{
				"HoldId": holdID,
			}).WithError(err).Warn("failed to check hold")
		} else {
			hold = resp.Hold
		}
		return err
	}, nil)

	return hold, err
}

func (c *pachinkoClient) CreateHold(ctx context.Context, transactionID, userID string, bitsQuantity int, expiresAt *time.Time) (int, error) {
	req := &rpc.StartTransactionReq{
		TransactionId: transactionID,
		Operation:     rpc.Operation_CREATE_HOLD,
		Tokens: []*rpc.Token{
			{
				OwnerId:  userID,
				GroupId:  GROUP_ID,
				Domain:   rpc.Domain_BITS,
				Quantity: int64(bitsQuantity),
			},
		},
	}
	if expiresAt != nil {
		protoExpiresAt, err := ptypes.TimestampProto(*expiresAt)
		if err != nil {
			log.WithFields(log.Fields{}).WithError(err).Error("could not parse timestamp to create hold")
		}
		req.ExpiresAt = protoExpiresAt
	}

	return c.performTransaction(ctx, userID, req)
}

func (c *pachinkoClient) FinalizeHold(ctx context.Context, transactionID, holdID, userID string) (int, error) {
	return c.performTransaction(ctx, userID, &rpc.StartTransactionReq{
		TransactionId:        transactionID,
		Operation:            rpc.Operation_FINALIZE_HOLD,
		OperandTransactionId: holdID,
	})
}

func (c *pachinkoClient) ReleaseHold(ctx context.Context, transactionID, holdID, userID string) (int, error) {
	return c.performTransaction(ctx, userID, &rpc.StartTransactionReq{
		TransactionId:        transactionID,
		Operation:            rpc.Operation_RELEASE_HOLD,
		OperandTransactionId: holdID,
	})
}

func (c *pachinkoClient) performTransaction(ctx context.Context, userID string, req *rpc.StartTransactionReq) (updatedBalance int, err error) {
	transactionId := req.TransactionId
	err = hystrix.Do(cmds.PachinkoStartTransactionCommand, func() error {
		resp, err := c.client.StartTransaction(ctx, req)

		if err != nil {
			log.WithFields(log.Fields{
				"EventId":      transactionId,
				"Operation":    req.Operation.String(),
				"TwitchUserId": userID,
			}).WithError(err).Warn("Error doing StartTransaction in Pachinko call.")
		} else if resp.ValidationError != nil {
			switch resp.ValidationError.Code {
			case rpc.ValidationErrorCode_TRANSACTION_COMMITTED:
				err = ErrAlreadyCommitted
			default:
				err = errors.Errorf("Validation error doing StartTransaction: %s", resp.ValidationError.Code.String())
			}
		} else if resp.TransactionId != req.TransactionId {
			log.WithFields(log.Fields{
				"RequestTransactionId":  transactionId,
				"PachinkoTransactionId": resp.TransactionId,
			}).Warn("Pachinko is returning a different transaction id than was sent it.")
			// If Pachinko sent us a different transaction id, I guess we should use it or should we error?
			transactionId = resp.TransactionId
		}

		return err
	}, nil)

	if err != nil {
		return
	}

	err = hystrix.Do(cmds.PachinkoCommitTransactionCommand, func() error {
		resp, err := c.client.CommitTransaction(ctx, &rpc.CommitTransactionReq{
			TransactionId: transactionId,
			Domain:        rpc.Domain_BITS,
		})

		if err != nil {
			log.WithFields(log.Fields{
				"EventId":      transactionId,
				"TwitchUserId": userID,
			}).WithError(err).Warn("Error doing CommitTransaction in Pachinko call.")
		} else {
			updatedBalance = int(resp.Tokens[0].Quantity)
		}
		return err
	}, nil)
	return
}
