package audit

import (
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/payday/file"
	"code.justin.tv/commerce/payday/iam"
	"code.justin.tv/commerce/payday/lambda"
	"code.justin.tv/commerce/payday/s3"
)

const lambdaFileSuffix = "_lambda_code.zip"

type TableAuditSetupConfig struct {
	AuditS3Bucket       string
	DynamoFullTableName string
	RecordKeySelector   string

	LambdaFunctionName string
	LambdaHandler      string
	LambdaFileName     string
	LambdaDescription  string
	LambdaMemorySizeMB int64
	LambdaTimeoutSec   int64
	LambdaRoleName     string
	LambdaCodeS3Bucket string

	EventBatchSize int64
	EventSource    string

	OverwriteExisting bool
}

type TableAuditSetup struct {
	auditSetup *AuditSetup
	config     *TableAuditSetupConfig
}

type AuditSetup struct {
	s3Client     s3.IS3Client
	lambdaClient *lambda.LambdaClient
	iamClient    *iam.IAMClient
}

func NewAuditSetup(s3Client s3.IS3Client, lambdaClient *lambda.LambdaClient, iamClient *iam.IAMClient) *AuditSetup {
	return &AuditSetup{
		s3Client:     s3Client,
		lambdaClient: lambdaClient,
		iamClient:    iamClient,
	}
}

func NewTableAuditSetup(auditSetup *AuditSetup, config *TableAuditSetupConfig) *TableAuditSetup {
	return &TableAuditSetup{
		auditSetup: auditSetup,
		config:     config,
	}
}

func (s *TableAuditSetup) Setup() error {
	err := s.UploadLambdaCode()
	if err != nil {
		log.WithError(err).Error("Error uploading lambda code during audit setup")
		return err
	}

	err = s.SetupLambdaFunction()
	if err != nil {
		log.WithError(err).Error("Error creating lambda function during audit setup")
		return err
	}

	err = s.SetupLambdaEventSource()
	if err != nil {
		log.WithError(err).Error("Error creating event source mapping during audit setup")
		return err
	}

	return nil
}

func (s *TableAuditSetup) UploadLambdaCode() error {
	err := s.ensureAuditRecordBucketExists()
	if err != nil {
		log.WithError(err).Error("Encountered an error making sure audit record bucket exists in S3")
		return err
	}

	err = s.ensureLambdaCodeBucketExists()
	if err != nil {
		log.WithError(err).Error("Encountered an error making sure lambda code bucket exists in S3")
		return err
	}

	zippedLambdaCode, err := s.generateZippedCodeFile()
	if err != nil {
		log.WithError(err).Error("Encountered an error generating zipped lambda code")
		return err
	}

	err = s.uploadLambdaCodeFile(zippedLambdaCode)
	if err != nil {
		log.WithError(err).Error("Encountered an error generating zipped lambda code")
		return err
	}

	return nil
}

func (s *TableAuditSetup) SetupLambdaFunction() error {
	if s.config.OverwriteExisting {
		err := s.ensureLambdaDoesNotExist()
		if err != nil {
			log.WithError(err).Error("Encountered an error ensuring lambda does not exist")
			return err
		}
	}

	err := s.ensureLambdaExists()
	if err != nil {
		log.WithError(err).Error("Encountered an error ensuring lambda exists")
		return err
	}
	return nil
}

func (s *TableAuditSetup) SetupLambdaEventSource() error {
	if s.config.OverwriteExisting {
		err := s.ensureEventSourceMappingDoesNotExist()
		if err != nil {
			log.WithError(err).Error("Encountered an error ensuring lambda event source mapping does not exist")
		}
	}
	err := s.ensureEventSourceMappingExists()
	if err != nil {
		log.WithError(err).Error("Encountered an error ensuring event source mapping exists")
		return err
	}
	return nil
}

func (s *TableAuditSetup) generateZippedCodeFile() ([]byte, error) {
	codeTemplate := &AuditLambdaTemplate{
		FullTableName:     s.config.DynamoFullTableName,
		S3Bucket:          s.config.AuditS3Bucket,
		RecordKeySelector: s.config.RecordKeySelector,
	}

	generatedCodeFile, err := codeTemplate.GenerateCodeFile()
	if err != nil {
		return nil, err
	}

	return file.ZipFile(s.config.LambdaFileName, generatedCodeFile)
}

func (s *TableAuditSetup) ensureAuditRecordBucketExists() error {
	return s.auditSetup.s3Client.EnsureBucketExists(s.config.AuditS3Bucket)
}

func (s *TableAuditSetup) ensureLambdaCodeBucketExists() error {
	return s.auditSetup.s3Client.EnsureBucketExists(s.config.LambdaCodeS3Bucket)
}

func (s *TableAuditSetup) uploadLambdaCodeFile(file []byte) error {
	return s.auditSetup.s3Client.UploadFileToBucket(s.config.LambdaCodeS3Bucket, s.getLambdaZipS3Key(), file)
}

func (s *TableAuditSetup) ensureLambdaDoesNotExist() error {
	return s.auditSetup.lambdaClient.EnsureLambdaDoesNotExist(s.config.LambdaFunctionName)
}

func (s *TableAuditSetup) ensureLambdaExists() error {
	req, err := s.generateCreateLambdaRequest()
	if err != nil {
		return err
	}
	return s.auditSetup.lambdaClient.EnsureLambdaExists(req)
}

func (s *TableAuditSetup) getLambdaZipS3Key() string {
	return s.config.LambdaFunctionName + lambdaFileSuffix
}

func (s *TableAuditSetup) getRoleArn() (string, error) {
	return s.auditSetup.iamClient.GetRoleArn(s.config.LambdaRoleName)
}

func (s *TableAuditSetup) generateCreateLambdaRequest() (*lambda.CreateLambdaRequest, error) {
	roleArn, err := s.getRoleArn()
	if err != nil {
		return nil, err
	}

	return &lambda.CreateLambdaRequest{
		FunctionName: s.config.LambdaFunctionName,
		Handler:      s.config.LambdaHandler,
		Description:  s.config.LambdaDescription,
		MemorySizeMB: s.config.LambdaMemorySizeMB,
		RoleArn:      roleArn,
		TimeoutSec:   s.config.LambdaTimeoutSec,
		CodeS3Bucket: s.config.LambdaCodeS3Bucket,
		CodeS3Key:    s.getLambdaZipS3Key(),
	}, nil
}

func (s *TableAuditSetup) ensureEventSourceMappingExists() error {
	req := &lambda.CreateEventSourceMappingRequest{
		BatchSize:      s.config.EventBatchSize,
		FunctionName:   s.config.LambdaFunctionName,
		EventSourceARN: s.config.EventSource,
	}
	return s.auditSetup.lambdaClient.EnsureEventSourceMappingExists(req)
}

func (s *TableAuditSetup) ensureEventSourceMappingDoesNotExist() error {
	return s.auditSetup.lambdaClient.EnsureEventSourceMappingDoesNotExist(s.config.LambdaFunctionName, s.config.EventSource)
}
