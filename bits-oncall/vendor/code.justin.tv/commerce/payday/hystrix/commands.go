package hystrix

import afex "github.com/afex/hystrix-go/hystrix"

const (
	//Commands
	UsersCommand                         = "get_user"
	GetRecentEventCommand                = "get_recent_event"
	GetTopEventCommand                   = "get_top_event"
	UpdateRecentEventCommand             = "update_recent_event"
	UpdateTransactionsCommand            = "update_transactions"
	GetTransactionsCommand               = "get_transactions"
	GetHashTagsCommand                   = "get_tags"
	GetBalanceCommand                    = "get_balance"
	GetChannelCommand                    = "get_channel"
	GetChannelRedisCommand               = "get_channel_redis"
	UpdateHashTagsCommand                = "update_tags"
	GetHighestBadgeCommand               = "get_highest_badge"
	GetADGPricesCommand                  = "get_prices_adg"
	GetFloPricesCommand                  = "get_prices_flo"
	GetPaymentServicePricesCommand       = "get_prices_payment_service"
	GetRecentByTransactionId             = "get_recent_by_transaction_id"
	GetImageCommand                      = "get_image"
	GetPartnerPrefixCommand              = "get_partner_prefix"
	GetImageByAssetIdCommand             = "get_image_by_asset_id"
	GetChannelByImageSetIdCommand        = "get_channel_by_image_set"
	UpdateImageCommand                   = "update_image"
	GetTaxInfo                           = "get_tax_info"
	GetUserPayoutType                    = "get_user_payout_type"
	GetPartnerCommand                    = "get_partner"
	GetPayoutEntityForChannelCommand     = "get_payout_entity_for_channel"
	GetRewardCommand                     = "get_reward"
	GetRewardByFulfillmentAccountCommand = "get_reward_by_fulfillment_account"
	UpdateRewardCommand                  = "update_reward"
	GetBucketedLeaderboardCounts         = "get_bucketed_leaderboard_counts"
	GetLeaderboardCommand                = "get_leaderboard"
	PublishEventCommand                  = "publish_event"
	ModerateEntryCommand                 = "moderate_entry"
	HealthCheckPantheonCommand           = "health_check_pantheon"
	GetLeaderboardBadgeHoldersCommand    = "get_leaderboard_badge_holders"
	GetNumberOfPubSubListenersCommand    = "pub_sub_listeners"

	PachinkoGetBalanceCommand        = "pachinko_get_balance"
	PachinkoAddTokensCommand         = "pachinko_add_tokens"
	PachinkoStartTransactionCommand  = "pachinko_start_transaction"
	PachinkoCommitTransactionCommand = "pachinko_commit_transaction"
	PachinkoCheckHoldCommand         = "pachinko_check_hold"

	// Extension Transactions DAO
	GetExtensionTransactionCommand = "get_extension_transaction_command"

	// OWL Commands
	OWLGetClientCommand = "owl_get_client"

	// Zuma Commands
	ZumaListModCommand        = "zuma_list_mods"
	ZumaGetModCommand         = "zuma_get_mod"
	ZumaEnforceMessageCommand = "zuma_enforce_message"

	// FABS Commands
	FABSUseBitsOnExtensionCommand    = "fabs:use_bits_on_extension"
	FABSGiveBitsToBroadcasterCommand = "fabs:give_bits_to_broadcaster"
	FABSGetBitsBalanceCommand        = "fabs:get_bits_balance"
	FABSGetTransactionsCommand       = "fabs:get_transactions"
	FABSAddBitsCommand               = "fabs:add_bits"
	FABSAdminAddBitsCommand          = "fabs:admin_add_bits"
	FABSAdminRemoveBitsCommand       = "fabs:admin_remove_bits"

	// TAILS Commands
	TAILSGetPayoutIdentityCommand         = "tails:get_payout_identity"
	TAILSGetLinkedTwitchAccountsCommand   = "tails:get_linked_twitch_accounts"
	TAILSGetTwitchUserNameCommand         = "tails:get_twitch_user_name"
	TAILSGetTwitchUserInfoCommand         = "tails:get_twitch_user_info"
	TAILSGetLinkedAmazonDirectedIdCommand = "tails:get_linked_amazon_directed_id"
	TAILSGetLinkedAmazonAccountCommand    = "tails:get_linked_amazon_account"
	TAILSUnlinkTwitchAccountCommand       = "tails:unlink_twitch_account"
	TAILSLinkTwitchAccountCommand         = "tails:link_twitch_account"

	// ADGDisco Commands
	ADGDiscoGetProductListDetails = "adgdisco:get_product_list_details"

	// Prometheus Commands
	PrometheusPublishEvent = "prometheus:publish_event"
	PrometheusSearch       = "prometheus:search"

	// cache operations
	AutomodCacheGet                = "automod_cache_get"
	AutomodCacheSet                = "automod_cache_set"
	AutomodCacheDel                = "automod_cache_del"
	CampaignsCacheGet              = "campaign_cache_get"
	CampaignsCacheSet              = "campaign_cache_set"
	CampaignsCacheDel              = "campaign_cache_del"
	CampaignsUserCacheGet          = "campaign_user_cache_get"
	CampaignsUserCacheSet          = "campaign_user_cache_set"
	CampaignsUserCacheDel          = "campaign_user_cache_del"
	CampaignsChannelCacheGet       = "campaign_channel_cache_get"
	CampaignsChannelCacheSet       = "campaign_channel_cache_set"
	CampaignsChannelCacheDel       = "campaign_channel_cache_del"
	CampaignsEligibleCacheGet      = "campaign_eligible_cache_get"
	CampaignsEligibleCacheSet      = "campaign_eligible_cache_get"
	CampaignsEligibleCacheDel      = "campaign_eligible_cache_get"
	ActionsCacheGet                = "actions_cache_get"
	ActionsCacheSet                = "actions_cache_set"
	ActionsCacheDel                = "actions_cache_del"
	BalanceCacheGet                = "balance_cache_get"
	BalanceCacheSet                = "balance_cache_set"
	BalanceCacheDel                = "balance_cache_del"
	EarnedBadgesCacheGet           = "earned_badges_cache_get"
	EarnedBadgesCacheSet           = "earned_badges_cache_set"
	FloPricesCacheGet              = "flo_prices_cache_get"
	FloPricesCacheSet              = "flo_prices_cache_set"
	FloPricesCacheDel              = "flo_prices_cache_del"
	ImagesCacheGet                 = "images_cache_get"
	ImagesCacheSet                 = "images_cache_set"
	ImagesCacheDel                 = "images_cache_del"
	PrefixCacheGet                 = "prefix_cache_get"
	PrefixCacheSet                 = "prefix_cache_set"
	PrefixCacheDel                 = "prefix_cache_del"
	ProductsCacheGet               = "products_cache_get"
	ProductsCacheSet               = "products_cache_set"
	ProductsCacheDel               = "products_cache_del"
	UserCacheGet                   = "user_cache_get"
	UserCacheSet                   = "user_cache_set"
	UserCacheDel                   = "user_cache_del"
	UserServiceCacheGet            = "user_service_cache_get"
	UserServiceCacheSet            = "user_service_cache_set"
	UserServiceCacheDel            = "user_service_cache_del"
	ChatBadgesCacheGet             = "chat_badges_cache_get"
	ChatBadgesCacheSet             = "chat_badges_cache_set"
	ChatBadgesCacheDel             = "chat_badges_cache_del"
	GemNotificationConfigCacheGet  = "gem_notification_config_cache_get"
	GemNotificationConfigCacheSet  = "gem_notification_config_cache_set"
	GemNotificationConfigCacheDel  = "gem_notification_config_cache_del"
	GemNotificationCacheGet        = "gem_notification_cache_get"
	GemNotificationCacheSet        = "gem_notification_cache_set"
	GemNotificationCacheDel        = "gem_notification_cache_del"
	ThrottleGet                    = "throttle_get"
	ThrottleSet                    = "throttle_set"
	ExperimentGet                  = "experiment_get"
	ExperimentSet                  = "experiment_set"
	PushyStatusCacheGet            = "pushy_status_cache_get"
	PushyStatusCacheSet            = "pushy_status_cache_set"
	TierNotificationsCacheGet      = "tier_notifications_cache_get"
	TierNotificationsCacheSet      = "tier_notifications_cache_set"
	TierNotificationsCacheDel      = "tier_notifications_cache_del"
	ProductsEntitlementsCacheGet   = "products_entitlements_cache_get"
	ProductsEntitlementsCacheSet   = "products_entitlements_cache_set"
	ProductsRecentPlatformCacheGet = "products_recent_platform_cache_get"
	ProductsRecentPlatformCacheSet = "products_recent_platform_cache_set"
	ProductsSinglePurchaseCacheGet = "products_single_purchase_cache_get"
	ProductsSinglePurchaseCacheSet = "products_single_purchase_cache_set"
	HoldsCacheGet                  = "hold_get"
	HoldsCacheSet                  = "hold_set"
	HoldsCacheDel                  = "hold_del"

	// Dynamo
	ActionsDBGet      = "actions_db_get"
	ActionsDBBatchGet = "actions_db_batch_get"
	ActionsDBGetAll   = "actions_db_get_all"
	ActionsDBUpdate   = "actions_db_update"
	ActionsDBDelete   = "actions_db_delete"

	//Timeouts
	TestTimeout = 30000

	// Concurrency
	DefaultMaxConcurrent = 100

	NitroGetPremiumStatusesCommand = "nitro_get_premium_status"
)

func init() {
	configureTimeout(UsersCommand, afex.DefaultTimeout)
	configureTimeout(GetRecentEventCommand, afex.DefaultTimeout)
	configureTimeout(GetTopEventCommand, afex.DefaultTimeout)
	configureTimeout(UpdateRecentEventCommand, 2000)
	configureTimeout(UpdateTransactionsCommand, afex.DefaultTimeout)
	configureTimeout(GetTransactionsCommand, afex.DefaultTimeout)
	configureTimeout(GetHashTagsCommand, afex.DefaultTimeout)
	configureTimeout(GetBalanceCommand, 8000)
	configure(GetChannelCommand, 3000, 600)
	configureTimeout(GetChannelRedisCommand, afex.DefaultTimeout)
	configureTimeout(UpdateHashTagsCommand, afex.DefaultTimeout)
	configureTimeout(GetHighestBadgeCommand, 2000)
	configureTimeout(GetADGPricesCommand, 4000)
	configureTimeout(GetPaymentServicePricesCommand, 4000)
	configureTimeout(GetRecentByTransactionId, afex.DefaultTimeout)
	configureTimeout(GetImageCommand, afex.DefaultTimeout)
	configureTimeout(GetImageByAssetIdCommand, afex.DefaultTimeout)
	configureTimeout(UpdateImageCommand, afex.DefaultTimeout)
	configureTimeout(GetTaxInfo, afex.DefaultTimeout)
	configureTimeout(GetPartnerPrefixCommand, afex.DefaultTimeout)
	configureTimeout(GetChannelByImageSetIdCommand, afex.DefaultTimeout)
	configureTimeout(GetUserPayoutType, 5000)
	configureTimeout(GetPartnerCommand, 5000)
	configureTimeout(GetPayoutEntityForChannelCommand, 2000)
	configureTimeout(GetRewardCommand, afex.DefaultTimeout)
	configureTimeout(UpdateRewardCommand, afex.DefaultTimeout)
	configureTimeout(PublishEventCommand, 3000)
	configureTimeout(GetLeaderboardCommand, afex.DefaultTimeout)
	configureTimeout(ModerateEntryCommand, afex.DefaultTimeout)
	configureTimeout(HealthCheckPantheonCommand, 4000)
	configureTimeout(GetNumberOfPubSubListenersCommand, 500)

	// Extension Transactions DAO Timeouts
	configureTimeout(GetExtensionTransactionCommand, afex.DefaultTimeout)

	// OWL Timeouts
	configureTimeout(OWLGetClientCommand, afex.DefaultTimeout)

	// Zuma Timeouts
	configureTimeout(ZumaListModCommand, afex.DefaultTimeout)
	configureTimeout(ZumaGetModCommand, afex.DefaultTimeout)
	configureTimeout(ZumaEnforceMessageCommand, afex.DefaultTimeout)

	// FABS Timeouts
	configureTimeout(FABSGiveBitsToBroadcasterCommand, 3000)
	configureTimeout(FABSGetBitsBalanceCommand, 2000)
	configureTimeout(FABSGetTransactionsCommand, 2000)
	configureTimeout(FABSAddBitsCommand, 3000)
	configureTimeout(FABSAdminAddBitsCommand, 3000)
	configureTimeout(FABSAdminRemoveBitsCommand, 3000)

	// TAILS Timeouts
	configureTimeout(TAILSGetPayoutIdentityCommand, 2000)
	configureTimeout(TAILSGetLinkedTwitchAccountsCommand, 2000)
	configureTimeout(TAILSGetTwitchUserNameCommand, 2000)
	configureTimeout(TAILSGetTwitchUserInfoCommand, 2000)
	configureTimeout(TAILSGetLinkedAmazonDirectedIdCommand, 2000)
	configureTimeout(TAILSGetLinkedAmazonAccountCommand, 2000)
	configureTimeout(TAILSUnlinkTwitchAccountCommand, 4000)
	configureTimeout(TAILSLinkTwitchAccountCommand, 4000)

	// ADGDisco Timeouts
	configureTimeout(ADGDiscoGetProductListDetails, 2000)

	// cache timeouts
	configureTimeout(AutomodCacheGet, 250)
	configureTimeout(AutomodCacheSet, 250)
	configureTimeout(AutomodCacheDel, 250)
	configureTimeout(CampaignsCacheGet, 250)
	configureTimeout(CampaignsCacheSet, 250)
	configureTimeout(CampaignsCacheDel, 250)
	configureTimeout(BalanceCacheGet, 250)
	configureTimeout(BalanceCacheSet, 250)
	configureTimeout(BalanceCacheDel, 250)
	configureTimeout(EarnedBadgesCacheGet, 250)
	configureTimeout(EarnedBadgesCacheSet, 250)
	configureTimeout(FloPricesCacheGet, 250)
	configureTimeout(FloPricesCacheSet, 250)
	configureTimeout(FloPricesCacheDel, 250)
	configureTimeout(ImagesCacheGet, 250)
	configureTimeout(ImagesCacheSet, 250)
	configureTimeout(ImagesCacheDel, 250)
	configureTimeout(PrefixCacheGet, 250)
	configureTimeout(PrefixCacheSet, 250)
	configureTimeout(PrefixCacheDel, 250)
	configureTimeout(ProductsCacheGet, 250)
	configureTimeout(ProductsCacheSet, 250)
	configureTimeout(ProductsCacheDel, 250)
	configureTimeout(CampaignsUserCacheGet, 250)
	configureTimeout(CampaignsUserCacheSet, 250)
	configureTimeout(CampaignsUserCacheDel, 250)
	configureTimeout(CampaignsChannelCacheGet, 250)
	configureTimeout(CampaignsChannelCacheSet, 250)
	configureTimeout(CampaignsChannelCacheDel, 250)
	configureTimeout(CampaignsEligibleCacheGet, 250)
	configureTimeout(CampaignsEligibleCacheSet, 250)
	configureTimeout(CampaignsEligibleCacheDel, 250)
	configureTimeout(ActionsCacheGet, 250)
	configureTimeout(ActionsCacheSet, 250)
	configureTimeout(ActionsCacheDel, 250)
	configureTimeout(UserCacheGet, 250)
	configureTimeout(UserCacheSet, 250)
	configureTimeout(UserCacheDel, 250)
	configureTimeout(UserServiceCacheGet, 250)
	configureTimeout(UserServiceCacheSet, 250)
	configureTimeout(UserServiceCacheDel, 250)
	configureTimeout(ChatBadgesCacheGet, 250)
	configureTimeout(ChatBadgesCacheSet, 250)
	configureTimeout(ChatBadgesCacheDel, 250)
	configureTimeout(GemNotificationConfigCacheGet, 250)
	configureTimeout(GemNotificationConfigCacheSet, 250)
	configureTimeout(GemNotificationConfigCacheDel, 250)
	configureTimeout(GemNotificationCacheGet, 250)
	configureTimeout(GemNotificationCacheSet, 250)
	configureTimeout(GemNotificationCacheDel, 250)
	configureTimeout(ThrottleGet, 250)
	configureTimeout(ThrottleSet, 250)
	configureTimeout(ExperimentGet, 250)
	configureTimeout(ExperimentSet, 250)
	configureTimeout(PushyStatusCacheGet, 250)
	configureTimeout(PushyStatusCacheSet, 250)
	configureTimeout(TierNotificationsCacheGet, 250)
	configureTimeout(TierNotificationsCacheSet, 250)
	configureTimeout(TierNotificationsCacheDel, 250)
	configureTimeout(HoldsCacheGet, 250)
	configureTimeout(HoldsCacheSet, 250)
	configureTimeout(HoldsCacheDel, 250)

	// Dynamo timeouts
	configureTimeout(ActionsDBGet, 500)
	configureTimeout(ActionsDBBatchGet, 500)
	configureTimeout(ActionsDBGetAll, 500)
	configureTimeout(ActionsDBUpdate, 500)
	configureTimeout(ActionsDBDelete, 500)
}

func configureTimeout(cmd string, timeout int) {
	configure(cmd, timeout, DefaultMaxConcurrent)
}

func configure(cmd string, timeout int, maxConcurrent int) {
	afex.ConfigureCommand(cmd, afex.CommandConfig{
		Timeout:               timeout,
		MaxConcurrentRequests: maxConcurrent,
		ErrorPercentThreshold: afex.DefaultErrorPercentThreshold,
	})
}

func ConfigureForTests() {
	configureTimeout(UsersCommand, TestTimeout)
	configureTimeout(GetRecentEventCommand, TestTimeout)
	configureTimeout(GetTopEventCommand, TestTimeout)
	configureTimeout(UpdateRecentEventCommand, TestTimeout)
	configureTimeout(UpdateTransactionsCommand, TestTimeout)
	configureTimeout(GetTransactionsCommand, TestTimeout)
	configureTimeout(GetHashTagsCommand, TestTimeout)
	configureTimeout(GetBalanceCommand, TestTimeout)
	configureTimeout(GetChannelCommand, TestTimeout)
	configureTimeout(GetChannelRedisCommand, TestTimeout)
	configureTimeout(UpdateHashTagsCommand, TestTimeout)
	configureTimeout(GetHighestBadgeCommand, TestTimeout)
	configureTimeout(GetADGPricesCommand, TestTimeout)
	configureTimeout(GetPaymentServicePricesCommand, TestTimeout)
	configureTimeout(GetRecentByTransactionId, TestTimeout)
	configureTimeout(GetImageCommand, TestTimeout)
	configureTimeout(GetImageByAssetIdCommand, TestTimeout)
	configureTimeout(UpdateImageCommand, TestTimeout)
	configureTimeout(GetTaxInfo, TestTimeout)
	configureTimeout(GetPartnerPrefixCommand, TestTimeout)
	configureTimeout(GetChannelByImageSetIdCommand, TestTimeout)
	configureTimeout(GetUserPayoutType, TestTimeout)
	configureTimeout(GetPartnerCommand, TestTimeout)
	configureTimeout(GetPayoutEntityForChannelCommand, TestTimeout)
	configureTimeout(GetRewardCommand, TestTimeout)
	configureTimeout(UpdateRewardCommand, TestTimeout)
	configureTimeout(PublishEventCommand, TestTimeout)
	configureTimeout(GetLeaderboardCommand, TestTimeout)
	configureTimeout(ModerateEntryCommand, TestTimeout)
	configureTimeout(HealthCheckPantheonCommand, TestTimeout)

	// Extension Transactions DAO Timeouts
	configureTimeout(GetExtensionTransactionCommand, TestTimeout)

	// OWL Timeouts
	configureTimeout(OWLGetClientCommand, TestTimeout)

	// Zuma Timeouts
	configureTimeout(ZumaListModCommand, TestTimeout)
	configureTimeout(ZumaGetModCommand, TestTimeout)
	configureTimeout(ZumaEnforceMessageCommand, TestTimeout)

	// FABS Timeouts
	configureTimeout(FABSGiveBitsToBroadcasterCommand, TestTimeout)
	configureTimeout(FABSGetBitsBalanceCommand, TestTimeout)
	configureTimeout(FABSGetTransactionsCommand, TestTimeout)
	configureTimeout(FABSAddBitsCommand, TestTimeout)
	configureTimeout(FABSAdminAddBitsCommand, TestTimeout)
	configureTimeout(FABSAdminRemoveBitsCommand, TestTimeout)

	// TAILS Timeouts
	configureTimeout(TAILSGetPayoutIdentityCommand, TestTimeout)
	configureTimeout(TAILSGetLinkedTwitchAccountsCommand, TestTimeout)
	configureTimeout(TAILSGetTwitchUserNameCommand, TestTimeout)
	configureTimeout(TAILSGetTwitchUserInfoCommand, TestTimeout)
	configureTimeout(TAILSGetLinkedAmazonDirectedIdCommand, TestTimeout)
	configureTimeout(TAILSGetLinkedAmazonAccountCommand, TestTimeout)
	configureTimeout(TAILSUnlinkTwitchAccountCommand, TestTimeout)
	configureTimeout(TAILSLinkTwitchAccountCommand, TestTimeout)

	// ADGDisco Timeouts
	configureTimeout(ADGDiscoGetProductListDetails, TestTimeout)
}
