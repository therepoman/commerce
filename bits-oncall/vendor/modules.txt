# code.justin.tv/amzn/CorleoneTwirp v2.0.8-0.20200828001643-dba03c302e7e+incompatible
code.justin.tv/amzn/CorleoneTwirp
# code.justin.tv/amzn/TwitchLogging v0.0.0-20190731182733-d8aae132db1f
code.justin.tv/amzn/TwitchLogging
# code.justin.tv/amzn/TwitchProcessIdentifier v0.0.0-20191004180637-dc817f563e55
code.justin.tv/amzn/TwitchProcessIdentifier
# code.justin.tv/amzn/TwitchTelemetry v0.0.0-20200617222855-06f01d84945d
code.justin.tv/amzn/TwitchTelemetry
# code.justin.tv/chat/golibs v3.5.0+incompatible
code.justin.tv/chat/golibs/errx
code.justin.tv/chat/golibs/internal/rollbar
# code.justin.tv/chat/timing v1.0.1
## explicit
code.justin.tv/chat/timing
# code.justin.tv/chat/zuma v1.3.9
code.justin.tv/chat/zuma/app/api
# code.justin.tv/commerce/gogogadget v0.0.0-20190624182939-1153b7d0706e
## explicit
code.justin.tv/commerce/gogogadget/http
code.justin.tv/commerce/gogogadget/pointers
# code.justin.tv/commerce/logrus v1.1.2
## explicit
code.justin.tv/commerce/logrus
# code.justin.tv/commerce/pachinko v0.0.0-20190712220425-aaed3a7737f0
## explicit
code.justin.tv/commerce/pachinko/rpc
# code.justin.tv/commerce/payday v0.0.0-20190711214020-95d630387701
## explicit
code.justin.tv/commerce/payday/audit
code.justin.tv/commerce/payday/client
code.justin.tv/commerce/payday/clients/pachinko
code.justin.tv/commerce/payday/clients/sandstorm
code.justin.tv/commerce/payday/config
code.justin.tv/commerce/payday/dynamo
code.justin.tv/commerce/payday/dynamo/holds
code.justin.tv/commerce/payday/errors
code.justin.tv/commerce/payday/file
code.justin.tv/commerce/payday/hystrix
code.justin.tv/commerce/payday/iam
code.justin.tv/commerce/payday/lambda
code.justin.tv/commerce/payday/models/api
code.justin.tv/commerce/payday/s3
code.justin.tv/commerce/payday/utils/close
code.justin.tv/commerce/payday/utils/math
code.justin.tv/commerce/payday/utils/pointers
code.justin.tv/commerce/payday/utils/strings
code.justin.tv/commerce/payday/utils/time
# code.justin.tv/common/golibs v1.0.7
code.justin.tv/common/golibs/pkgpath
# code.justin.tv/common/yimg v0.0.0-20190311202211-55ce6468b269
## explicit
code.justin.tv/common/yimg
# code.justin.tv/feeds/ctxlog v0.0.0-20170328220514-514fab8fb5b7
## explicit
# code.justin.tv/feeds/feeds-common v1.2.2
## explicit
code.justin.tv/feeds/feeds-common/entity
# code.justin.tv/foundation/twitchclient v4.11.0+incompatible
## explicit
code.justin.tv/foundation/twitchclient
# code.justin.tv/foundation/xray v0.0.0-20171002153549-42df0dc4ee6b
## explicit
# code.justin.tv/revenue/everdeen v0.26.17
## explicit
code.justin.tv/revenue/everdeen/rpc
# code.justin.tv/revenue/mulan v1.0.13
## explicit
code.justin.tv/revenue/mulan/rpc
# code.justin.tv/revenue/payments-service-go-client v0.1.43
## explicit
code.justin.tv/revenue/payments-service-go-client/client
# code.justin.tv/sse/malachai v0.6.0
## explicit
code.justin.tv/sse/malachai/pkg/backoff
code.justin.tv/sse/malachai/pkg/config
code.justin.tv/sse/malachai/pkg/internal/apierror
code.justin.tv/sse/malachai/pkg/internal/discovery
code.justin.tv/sse/malachai/pkg/internal/inventory
code.justin.tv/sse/malachai/pkg/internal/keystore
code.justin.tv/sse/malachai/pkg/internal/stats
code.justin.tv/sse/malachai/pkg/internal/stats/statsiface
code.justin.tv/sse/malachai/pkg/internal/stringutil
code.justin.tv/sse/malachai/pkg/jwtvalidation
code.justin.tv/sse/malachai/pkg/log
code.justin.tv/sse/malachai/pkg/registration
code.justin.tv/sse/malachai/pkg/registration/iamgenerator
code.justin.tv/sse/malachai/pkg/rorolepicker
code.justin.tv/sse/malachai/pkg/s2s
code.justin.tv/sse/malachai/pkg/s2s/caller
code.justin.tv/sse/malachai/pkg/s2s/internal
code.justin.tv/sse/malachai/pkg/signature
code.justin.tv/sse/malachai/pkg/signature/signatureiface
code.justin.tv/sse/malachai/pkg/sigutils
# code.justin.tv/sse/policy v0.0.1
## explicit
code.justin.tv/sse/policy
# code.justin.tv/systems/sandstorm v1.6.6
code.justin.tv/systems/sandstorm/closer
code.justin.tv/systems/sandstorm/inventory/heartbeat
code.justin.tv/systems/sandstorm/inventory/heartbeat/heartbeatiface
code.justin.tv/systems/sandstorm/logging
code.justin.tv/systems/sandstorm/manager
code.justin.tv/systems/sandstorm/pkg/atomic
code.justin.tv/systems/sandstorm/pkg/envelope
code.justin.tv/systems/sandstorm/pkg/envelope/envelopeiface
code.justin.tv/systems/sandstorm/pkg/policy
code.justin.tv/systems/sandstorm/pkg/secret
code.justin.tv/systems/sandstorm/pkg/stat
code.justin.tv/systems/sandstorm/pkg/stat/statiface
code.justin.tv/systems/sandstorm/policy
code.justin.tv/systems/sandstorm/queue
code.justin.tv/systems/sandstorm/resource
# code.justin.tv/video/pkgpath v1.0.1
code.justin.tv/video/pkgpath
# code.justin.tv/web/users-service v2.4.1-0.20190827222711-4c8533680844+incompatible
## explicit
code.justin.tv/web/users-service/client
code.justin.tv/web/users-service/client/usersclient_internal
code.justin.tv/web/users-service/internal/utils
code.justin.tv/web/users-service/models
# github.com/SermoDigital/jose v0.9.2-0.20161205224733-f6df55f235c2
## explicit
github.com/SermoDigital/jose
github.com/SermoDigital/jose/crypto
github.com/SermoDigital/jose/jws
github.com/SermoDigital/jose/jwt
# github.com/afex/hystrix-go v0.0.0-20180502004556-fa1af6a1f4f5
## explicit
github.com/afex/hystrix-go/hystrix
github.com/afex/hystrix-go/hystrix/metric_collector
github.com/afex/hystrix-go/hystrix/rolling
# github.com/asaskevich/govalidator v0.0.0-20200907205600-7a23bdc65eef
## explicit
github.com/asaskevich/govalidator
# github.com/aws/aws-sdk-go v1.34.0
## explicit
github.com/aws/aws-sdk-go/aws
github.com/aws/aws-sdk-go/aws/arn
github.com/aws/aws-sdk-go/aws/awserr
github.com/aws/aws-sdk-go/aws/awsutil
github.com/aws/aws-sdk-go/aws/client
github.com/aws/aws-sdk-go/aws/client/metadata
github.com/aws/aws-sdk-go/aws/corehandlers
github.com/aws/aws-sdk-go/aws/credentials
github.com/aws/aws-sdk-go/aws/credentials/ec2rolecreds
github.com/aws/aws-sdk-go/aws/credentials/endpointcreds
github.com/aws/aws-sdk-go/aws/credentials/processcreds
github.com/aws/aws-sdk-go/aws/credentials/stscreds
github.com/aws/aws-sdk-go/aws/crr
github.com/aws/aws-sdk-go/aws/csm
github.com/aws/aws-sdk-go/aws/defaults
github.com/aws/aws-sdk-go/aws/ec2metadata
github.com/aws/aws-sdk-go/aws/endpoints
github.com/aws/aws-sdk-go/aws/request
github.com/aws/aws-sdk-go/aws/session
github.com/aws/aws-sdk-go/aws/signer/v4
github.com/aws/aws-sdk-go/internal/context
github.com/aws/aws-sdk-go/internal/ini
github.com/aws/aws-sdk-go/internal/s3err
github.com/aws/aws-sdk-go/internal/sdkio
github.com/aws/aws-sdk-go/internal/sdkmath
github.com/aws/aws-sdk-go/internal/sdkrand
github.com/aws/aws-sdk-go/internal/sdkuri
github.com/aws/aws-sdk-go/internal/shareddefaults
github.com/aws/aws-sdk-go/internal/strings
github.com/aws/aws-sdk-go/internal/sync/singleflight
github.com/aws/aws-sdk-go/private/checksum
github.com/aws/aws-sdk-go/private/protocol
github.com/aws/aws-sdk-go/private/protocol/eventstream
github.com/aws/aws-sdk-go/private/protocol/eventstream/eventstreamapi
github.com/aws/aws-sdk-go/private/protocol/json/jsonutil
github.com/aws/aws-sdk-go/private/protocol/jsonrpc
github.com/aws/aws-sdk-go/private/protocol/query
github.com/aws/aws-sdk-go/private/protocol/query/queryutil
github.com/aws/aws-sdk-go/private/protocol/rest
github.com/aws/aws-sdk-go/private/protocol/restjson
github.com/aws/aws-sdk-go/private/protocol/restxml
github.com/aws/aws-sdk-go/private/protocol/xml/xmlutil
github.com/aws/aws-sdk-go/service/cloudwatch
github.com/aws/aws-sdk-go/service/cloudwatch/cloudwatchiface
github.com/aws/aws-sdk-go/service/dynamodb
github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute
github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface
github.com/aws/aws-sdk-go/service/dynamodb/expression
github.com/aws/aws-sdk-go/service/iam
github.com/aws/aws-sdk-go/service/iam/iamiface
github.com/aws/aws-sdk-go/service/kms
github.com/aws/aws-sdk-go/service/kms/kmsiface
github.com/aws/aws-sdk-go/service/lambda
github.com/aws/aws-sdk-go/service/lambda/lambdaiface
github.com/aws/aws-sdk-go/service/s3
github.com/aws/aws-sdk-go/service/s3/internal/arn
github.com/aws/aws-sdk-go/service/s3/s3iface
github.com/aws/aws-sdk-go/service/sfn
github.com/aws/aws-sdk-go/service/sns
github.com/aws/aws-sdk-go/service/sns/snsiface
github.com/aws/aws-sdk-go/service/sqs
github.com/aws/aws-sdk-go/service/sqs/sqsiface
github.com/aws/aws-sdk-go/service/sts
github.com/aws/aws-sdk-go/service/sts/stsiface
# github.com/cactus/go-statsd-client/statsd v0.0.0-20200728222731-a2baea3bbfc6
github.com/cactus/go-statsd-client/statsd
# github.com/cenkalti/backoff v2.2.1+incompatible
github.com/cenkalti/backoff
# github.com/cpuguy83/go-md2man/v2 v2.0.0
github.com/cpuguy83/go-md2man/v2/md2man
# github.com/go-errors/errors v1.1.1
## explicit
github.com/go-errors/errors
# github.com/go-pg/pg v8.0.7+incompatible
## explicit
github.com/go-pg/pg
github.com/go-pg/pg/internal
github.com/go-pg/pg/internal/iszero
github.com/go-pg/pg/internal/parser
github.com/go-pg/pg/internal/pool
github.com/go-pg/pg/internal/struct_filter
github.com/go-pg/pg/internal/tag
github.com/go-pg/pg/orm
github.com/go-pg/pg/types
# github.com/go-redis/redis v6.15.9+incompatible
## explicit
# github.com/gofrs/uuid v3.3.0+incompatible
## explicit
github.com/gofrs/uuid
# github.com/golang/protobuf v1.4.2
github.com/golang/protobuf/jsonpb
github.com/golang/protobuf/proto
github.com/golang/protobuf/ptypes
github.com/golang/protobuf/ptypes/any
github.com/golang/protobuf/ptypes/duration
github.com/golang/protobuf/ptypes/timestamp
github.com/golang/protobuf/ptypes/wrappers
# github.com/guregu/dynamo v1.2.1
## explicit
github.com/guregu/dynamo
github.com/guregu/dynamo/internal/exprs
# github.com/heroku/rollbar v0.6.0
## explicit
# github.com/jinzhu/inflection v1.0.0
github.com/jinzhu/inflection
# github.com/jmespath/go-jmespath v0.3.0
github.com/jmespath/go-jmespath
# github.com/konsorten/go-windows-terminal-sequences v1.0.3
github.com/konsorten/go-windows-terminal-sequences
# github.com/pkg/errors v0.9.1
## explicit
github.com/pkg/errors
# github.com/russross/blackfriday/v2 v2.0.1
github.com/russross/blackfriday/v2
# github.com/satori/go.uuid v1.2.0
## explicit
github.com/satori/go.uuid
# github.com/shurcooL/sanitized_anchor_name v1.0.0
github.com/shurcooL/sanitized_anchor_name
# github.com/twitchtv/twirp v7.1.0+incompatible
github.com/twitchtv/twirp
github.com/twitchtv/twirp/ctxsetters
github.com/twitchtv/twirp/internal/contextkeys
# github.com/urfave/cli v1.22.5
## explicit
github.com/urfave/cli
# golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
golang.org/x/crypto/pbkdf2
golang.org/x/crypto/ssh/terminal
# golang.org/x/net v0.0.0-20200625001655-4c5254603344
## explicit
golang.org/x/net/context
golang.org/x/net/internal/socks
golang.org/x/net/proxy
# golang.org/x/sync v0.0.0-20200625203802-6e8e738ad208
golang.org/x/sync/errgroup
golang.org/x/sync/syncmap
# golang.org/x/sys v0.0.0-20200602225109-6fdc65e7d980
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
golang.org/x/sys/windows
# google.golang.org/protobuf v1.25.0
google.golang.org/protobuf/encoding/protojson
google.golang.org/protobuf/encoding/prototext
google.golang.org/protobuf/encoding/protowire
google.golang.org/protobuf/internal/descfmt
google.golang.org/protobuf/internal/descopts
google.golang.org/protobuf/internal/detrand
google.golang.org/protobuf/internal/encoding/defval
google.golang.org/protobuf/internal/encoding/json
google.golang.org/protobuf/internal/encoding/messageset
google.golang.org/protobuf/internal/encoding/tag
google.golang.org/protobuf/internal/encoding/text
google.golang.org/protobuf/internal/errors
google.golang.org/protobuf/internal/fieldsort
google.golang.org/protobuf/internal/filedesc
google.golang.org/protobuf/internal/filetype
google.golang.org/protobuf/internal/flags
google.golang.org/protobuf/internal/genid
google.golang.org/protobuf/internal/impl
google.golang.org/protobuf/internal/mapsort
google.golang.org/protobuf/internal/pragma
google.golang.org/protobuf/internal/set
google.golang.org/protobuf/internal/strs
google.golang.org/protobuf/internal/version
google.golang.org/protobuf/proto
google.golang.org/protobuf/reflect/protoreflect
google.golang.org/protobuf/reflect/protoregistry
google.golang.org/protobuf/runtime/protoiface
google.golang.org/protobuf/runtime/protoimpl
google.golang.org/protobuf/types/known/anypb
google.golang.org/protobuf/types/known/durationpb
google.golang.org/protobuf/types/known/timestamppb
google.golang.org/protobuf/types/known/wrapperspb
# gopkg.in/validator.v2 v2.0.0-20191029180049-30e574a82075
gopkg.in/validator.v2
# gopkg.in/yaml.v2 v2.3.0
gopkg.in/yaml.v2
# mellium.im/sasl v0.2.1
## explicit
mellium.im/sasl
# gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.3 => github.com/DATA-DOG/go-sqlmock v1.3.3
# github.com/cactus/go-statsd-client v3.1.1+incompatible => github.com/cactus/go-statsd-client v0.0.0-20200728222731-a2baea3bbfc6
# github.com/cactus/go-statsd-client/statsd v3.1.1+incompatible => github.com/cactus/go-statsd-client/statsd v0.0.0-20200728222731-a2baea3bbfc6
