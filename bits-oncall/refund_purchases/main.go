package main

import (
	"context"
	"encoding/csv"
	"flag"
	"fmt"
	"net"
	"net/http"
	"os"

	"code.justin.tv/foundation/twitchclient"
	everdeentwirp "code.justin.tv/revenue/everdeen/rpc"
	mulantwirp "code.justin.tv/revenue/mulan/rpc"
	payments "code.justin.tv/revenue/payments-service-go-client/client"
	"golang.org/x/net/proxy"
)

const (
	EverdeenProduction = "https://prod.everdeen.twitch.a2z.com"
	EverdeenStaging    = "https://beta.everdeen.twitch.a2z.com"

	MulanProduction = "https://prod.mulan.twitch.a2z.com"
	MulanStaging    = "https://beta.mulan.twitch.a2z.com"

	PaymentsServiceProduction = "https://prod.payments-service.twitch.a2z.com"
	PaymentsServiceStaging    = "https://beta.payments-service.twitch.a2z.com"
)

func main() {
	ctx := context.Background()

	input := flag.String("input", string("./channel_allowlist.csv"), "input file")
	dryRun := flag.Bool("dryRun", true, "whether this a dry run (don't actually refund)")
	failuresOuputFile := flag.String("failuresOutputFile", string("./refund_failures.csv"), "output file for refunds that failed")
	flag.Parse()

	dialer, err := proxy.SOCKS5("tcp", "127.0.0.1:8000", nil, proxy.Direct)
	if err != nil {
		panic("can't connect to the proxy")
	}

	everdeenClient := everdeentwirp.NewEverdeenProtobufClient(EverdeenProduction, &http.Client{
		Transport: &http.Transport{
			DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
				return dialer.Dial(network, addr)
			},
		},
	})
	if everdeenClient == nil {
		panic("Can't create everdeen client")
	}

	mulanClient := mulantwirp.NewMulanProtobufClient(MulanProduction, &http.Client{
		Transport: &http.Transport{
			DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
				return dialer.Dial(network, addr)
			},
		},
	})
	if mulanClient == nil {
		panic("Cant create mulanClient")
	}

	paymentsServiceClient, err := payments.NewClient(twitchclient.ClientConf{
		Host: PaymentsServiceProduction,
	})

	if err != nil {
		panic("Can't create payments service client")
	}

	file, err := os.Open(*input)
	if err != nil {
		panic(fmt.Sprintf("Failed to open the File %v", *input))
	}

	reader := csv.NewReader(file)

	platforms := map[string]int{}
	count := 0
	failedRefunds := make([][]string, 0)
	for {
		fmt.Println()
		fmt.Println("---------------------------------------------")

		line, err := reader.Read()
		if err != nil {
			break
		}

		originID := line[0]
		senderID := line[1]
		platform := line[2]
		giftType := line[3]

		purchaseOrderReq := mulantwirp.GetPurchaseOrderPaymentsByIDsRequest{
			Ids: []string{originID},
		}

		purchaseOrderResp, err := mulanClient.GetPurchaseOrderPaymentsByIDs(ctx, &purchaseOrderReq)
		if err != nil {
			fmt.Printf("Failed to get Purchase Order details from Mulan, OriginID: %s, %+v\n", originID, err)
			continue
		}

		if purchaseOrderResp == nil {
			fmt.Printf("Failed to get Purchase Order details , OriginID: %s\n", originID)
			continue
		}

		orderDetails, ok := purchaseOrderResp.PurchaseOrderPayments[originID]
		if !ok {
			fmt.Printf("Failed to get Order Details from map for OriginID: %s\n", originID)
			continue
		}

		if orderDetails == nil || len(orderDetails.PurchaseOrderPayments) <= 0 || orderDetails.PurchaseOrderPayments[0] == nil {
			fmt.Printf("Failed to get order details for OriginID: %s\n", originID)
			continue
		}

		if _, ok := platforms[platform]; !ok {
			platforms[platform] = 1
		} else {
			platforms[platform] += 1
		}
		amount := purchaseOrderResp.PurchaseOrderPayments[originID].PurchaseOrderPayments[0].GrossAmount

		if amount < 0 {
			fmt.Printf("Order %s for user %s with gift type %s on platform %s has negative amount %d (already refunded), skipping this record\n", originID, senderID, giftType, platform, amount)
			continue
		}

		fmt.Printf("Refunding Gifts for User: %s OriginId: %s Amount: %d Gift Type: %s Platform: %s\n", senderID, originID, amount, giftType, platform)

		if *dryRun {
			fmt.Println("Skipping this order due to dry run")
			continue
		}

		// Refunding in the same exact way sub step functions do:
		// http://git.xarth.tv/revenue/lambdwich/blob/62ac99104d135dfc9075e3382438cc9a5ff7f584/app/functions/refund_purchase_order/main.go#L39-L44
		if giftType == "standard" && platform != "ios" {
			resp, err := paymentsServiceClient.RefundPurchaseOrder(ctx, originID, payments.RefundPurchaseOrderRequest{
				UserID: senderID,
			}, nil)

			if err != nil {
				fmt.Println()
				fmt.Printf("Failed to Refund, OriginID: %s\n", originID)
				fmt.Printf("%v\n", err)
				failedRefunds = append(failedRefunds, line)
				continue
			}

			fmt.Println(fmt.Sprintf("Refund Response: %+v", resp))
		} else {
			req := everdeentwirp.RefundOrderRequest{
				OriginId:          originID,
				RefundReason:      "refund-gifts",
				Amount:            amount,
				BenefitsDirective: everdeentwirp.BenefitsDirective_RETAIN_BENEFITS,
			}

			refundResp, err := everdeenClient.RefundOrder(ctx, &req)
			if err != nil {
				fmt.Println()
				fmt.Printf("Failed to Refund, OriginID: %s\n", originID)
				fmt.Printf("%v\n", err)
				failedRefunds = append(failedRefunds, line)
				continue
			}

			fmt.Println(fmt.Sprintf("Refund Response: %+v", refundResp))
		}

		count++
	}

	fmt.Printf("Completed processing %d records\n", count)
	fmt.Println()
	fmt.Println("Orders processed:")
	for paymentGateway, pCount := range platforms {
		fmt.Printf("%v %v\n", paymentGateway, pCount)
	}

	fmt.Println("---------------------------------------------")
	fmt.Println("Orders Failed to Process:")
	for _, line := range failedRefunds {
		fmt.Printf("%s,%s,%s,%s\n", line[0], line[1], line[2], line[3])
	}

	if failuresOuputFile != nil {
		refundFailuresOutputFile, err := os.Create(*failuresOuputFile)
		if err != nil {
			fmt.Printf("failed to open file for output, not saving output: %v", err)
			return
		}
		writer := csv.NewWriter(refundFailuresOutputFile)

		err = writer.WriteAll(failedRefunds)
		if err != nil {
			fmt.Printf("failed to write failed refunds to file: %v\n", err)
		}
		writer.Flush()
	}
}
