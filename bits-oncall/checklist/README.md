## Requirements checklist
> _These are __not__ sufficient for you to determine "Yes, I am ready to go oncall for Bits." These __are__ sufficient for you to determine "No, I am not ready to go oncall for Bits."_

### Account Access
#### Amazon Employee Account

#### [Rollbar](https://rollbar.com/Twitch/)
+ Can you access rollbar for [Payday](https://rollbar.com/Twitch/Payday/?environment=production)?
+ Can you access rollbar for [Pantheon](https://rollbar.com/Twitch/Pantheon/?environment=prod)?

#### [Midway](https://midway.amazon.com)  
+ Do you have a yubi key?

#### [Pager Duty](https://paging.amazon.com)
+ Have you set up pong paging?
+ Have you verified that you correctly receive a page?
  + From pong paging?
  + From page-[your amazon username]@amazon.com?
  + From a sev-2 ticket that is assigned to you?
+ Do you know how to access [Twitch pager duty](https://twitchoncall.pagerduty.com) 
  + Do you know how to look the oncall schedule of another team?
  + Do you know how to page another team?

#### [Oncall Rotation](https://oncall.corp.amazon.com/#/view/twitch-commerce/schedule)
+ Are you in the oncall rotation?

#### [TT (Trouble Ticketing)](https://tt.amazon.com)
+ Can you navigate to the [twitch-bits-oncall trouble ticket queue](https://tt.amazon.com/search?status=Assigned%3BResearching%3BWork+In+Progress%3BPending&assigned_group=Twitch-Commerce-Oncall&search=1)?
+ Do you know how to comment on and add attachments to tickets?
+ Do you know how to resolve a ticket?
+ Do you know how to stop being paged by a sev-2 ticket while it's still open?
+ Do you know how to reassign a ticket to another team (like Respawn or Twitch Prime)?

### VPN Access
#### Amazon VPN
+ Do you have cisco anyconnect installed?
+ Do you have [a gemalto token](https://w.amazon.com/index.php/Gemalto_Tokens)?

#### Twitch VPN
+ Do you have viscocity installed?
+ Have you [created a Twitch VPN account](https://dashboard.internal.justin.tv/ldaptools/edit)?

### AWS Knowledge
#### [Isengard](https://isengard.amazon.com)
+ Can you log into [the Payday AWS account](https://isengard.amazon.com/federate?account=021561903526&role=Admin)?
+ Can you log into [the Pantheon AWS account](https://isengard.amazon.com/federate?account=375121467453&role=Admin)?
  + In staging?
+ Can you log into [the FABS AWS account](https://isengard.amazon.com/federate?account=277306168768&role=Admin)?
  + In test? In gamma?
+ Can you log into [the Phanto AWS account](https://isengard.amazon.com/federate?account=748615074154&role=Admin)?
  + In staging?
+ Can you log into [the Fortuna AWS account](https://isengard.amazon.com/federate?account=180265471281&role=Admin)
  + In staging?
+ Do you know which resources live in which regions for Payday, Pantheon, Phanto and FABS?

#### S3 (Simple Storage Service)
+ Do you know how to upload assets for a global Cheermote?
+ Do you know how to replace an existing Bits asset?

#### SNS (Simple Notification Service)

#### SQS (Simple Queueing Service)
+ Do you know how to redrive messages from a Dead-Letter Queue?
+ Are you able determine whether any of our given Dead-Letter Queues can be cleared out?
+ Do you know how to dump encrypted messages?

#### Cloudwatch
+ Can you search the production Bits logs for panics?
+ Can you navigate to the Bits production metrics dashboards?

#### Cloudfront
+ Do you know how to invalidate the cache for a given sub-folder of the bits-assets s3 bucket?

#### Datapipelines
+ Are you able to tell whether the datapipelines are currently failing or succeeding?

#### DynamoDB
+ Are you able to find a given Bits transaction in the prod_transactions table?
+ Can you enable Bits for a given partnered Twitch user?
+ Do you know how to restore a backup?

### Other Regular Operations
#### Admin tool lookup
+ Do you have access to [the admin panel](https://admin-panel.internal.twitch.tv)? If not go [here](admin-access)
+ Do you know how to look up a given user's Bits transactions?
+ Do you know how to opt a channel out of Bits?
+ Do you know how to admin grant Bits to a user?
+ Do you nokw how to admin grant Bits to many users using the jobs tool?
#### Redshift
+ Do you know how to connect to our redshift DB and run queries?
  + For emote royalty payout configs?
  + For Bits transactions?
  + For Bits totals in a channel over a given time period?
#### SSHing into Production Boxes
+ Are you able to ssh into the Payday prod boxes?
+ Are you able to access FABS boxes to view logs?

### Additional Nice to Haves
#### Cloud Desktop
+ Do you have a cloud desktop set up?
