# Uploading Global Cheermotes
How to upload Global Bits Cheermotes

## Requirements
### Account Access
+ [Midway](https://git.xarth.tv/commerce/bits-oncall#midway)
+ [Isengard](https://git.xarth.tv/commerce/bits-oncall#isengard), specifically access to the Payday AWS account
  + S3 to upload assets and update prefixes
  + DynamoDB to upload metadata

### Repo Access
+ [Payday](https://git.xarth.tv/commerce/payday)
  + [cheermote_upload_verifier](https://git.xarth.tv/commerce/payday/tree/master/cmd/cheermote_upload_verifier) to verify all of the files in the folder of assets

### Data
+ A folder of all of the permutations of the assets (state, background, scale, tiers, etc) in the form required by the verifier
+ A prefix to use (like `RIPCheer`) with correct capitalization and no collisions in the namespace of existing cheermotes
+ A release date/time for when the cheermote should be useable

## Instructions
### Verify the folder of the cheermote assets is correctly formatted
+ Go to your GoWorkspace and then navigate to `GoWorkspace/code.justin.tv/commerce/payday/cmd/cheermotes/cheermote_upload_verifier`
+ Follow the README comment at the top of the script on how to run it
+ Run the script (this may require running from `/payday` rather than `/payday/cmd/cheermotes/cheermote_upload_verifier`)

### Upload the files
+ In Payday's S3, find the public `bits-assets` folder, where we keep the assets for all global cheermotes
+ Use the GUI in Isengard to upload the folder or use the aws-cli to do `aws s3 sync --dryrun ~/Desktop/muxy/ s3://bits-assets/actions/muxy`
+ Check that the folder uploaded in S3 successfully

### Upload the metadata
+ In Payday's DynamoDB, go to the `prod_actions` table
+ Select the Items tab
+ Click the `Create Item` button
+ Add in fields for the cheermote like the prefix, startTime, and canonicalCasing (see [here](https://git.xarth.tv/commerce/payday/blob/master/dynamo/actions/item.go) for the dynamo model for a cheermote)

OR

+ There's also a script in payday you can use instead of creating the item manually, but you'll have to enter the right data for the item in there. `/payday/cmd/cheermotes/create_db_entry`

### Update the yaml file that keeps track of which cheermotes to vend
+ In Payday's S3, go to the [production supported actions](https://s3-us-west-2.amazonaws.com/bits-supported-actions/production-actions) folder
+ Download the current production_prefixes yaml file
+ In your editor of choice, add a new line to that file corresponding to the prefix of the cheermote you want vended
+ Make sure your prefix is all lowercase in this yaml file
+ TODO, once we enforce priority by that yaml file: make sure that the order of your prefix in the list of prefixes is in the correct order for how you want the global cheermotes to be vended, since we will determine the value of the `priority` field based off of the order of this yaml file

### Wait for the poller to get the new prefixes and load the new metadata from dynamo
+ This value is configured in [the poller code](https://git.xarth.tv/commerce/payday/blob/master/actions/poller.go) of Payday
