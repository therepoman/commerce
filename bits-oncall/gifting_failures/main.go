package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"os"
	"time"

	"code.justin.tv/commerce/logrus"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/pkg/errors"
	"github.com/urfave/cli"
)

type ExecutionInput struct {
	OriginID string `json:"origin_id"`
	SenderID string `json:"sender_id"`
	Platform string `json:"platform"`
	GiftType string `json:"gift_type"`
}

type ExecutionFailureInfo struct {
	SenderID     string
	ExecutionArn string
	StepFailed   string
	Platform     string
	GiftType     string
}

var environment string
var stateMachine string
var lookBack time.Duration
var outputFile string

const (
	singleStateMachineArn    = "arn:aws:states:us-west-2:522398581884:stateMachine:production-StandardGiftStateMachine"
	mmgChildStateMachineArn  = "arn:aws:states:us-west-2:522398581884:stateMachine:production-MassMysteryGiftChildStateMachine"
	mmgParentStateMachineArn = "arn:aws:states:us-west-2:522398581884:stateMachine:production-MassMysteryGiftParentStateMachine"
)

func main() {
	app := cli.NewApp()
	app.Name = "gifting failures thing"
	app.Usage = "need purchase orders from failed step functions"

	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:        "environment, e",
			Usage:       "the environment you're publishing to. Can be either 'staging' or 'production'.",
			Value:       "staging",
			Destination: &environment,
		},
		&cli.StringFlag{
			Name:        "state-machine, s",
			Usage:       "state machine you're looking for. Can be either 'mmg-child', 'mmg-parent', or 'single'.",
			Value:       "single",
			Destination: &stateMachine,
		},
		&cli.DurationFlag{
			Name:        "look-back, l",
			Usage:       "time to look back for failures",
			Destination: &lookBack,
		},
		&cli.StringFlag{
			Name:        "output, o",
			Usage:       "output file for results",
			Value:       "output.csv",
			Destination: &outputFile,
		},
	}

	app.Action = giftingFailuresLookup
	err := app.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Fatal("script failed")
	}
}

func giftingFailuresLookup(c *cli.Context) error {
	startTime := time.Now()
	sess := session.Must(session.NewSession(&aws.Config{
		Region: aws.String("us-west-2"),
	}))

	client := sfn.New(sess)

	var stateMachineArn string
	switch stateMachine {
	case "single":
		stateMachineArn = singleStateMachineArn
	case "mmg-child":
		stateMachineArn = mmgChildStateMachineArn
	case "mmg-parent":
		stateMachineArn = mmgParentStateMachineArn
	default:
		return errors.New("invalid state machine to investigate")
	}

	lookToDate := time.Now().Add(-1 * lookBack)

	logrus.WithField("lookToDate", lookToDate).Info("looking up step function failures...")

	sfnFailures := make([]*sfn.ExecutionListItem, 0)
	err := client.ListExecutionsPages(&sfn.ListExecutionsInput{
		StateMachineArn: aws.String(stateMachineArn),
		StatusFilter:    aws.String("FAILED"),
		MaxResults:      aws.Int64(1000),
	}, func(page *sfn.ListExecutionsOutput, b bool) bool {
		sfnFailures = append(sfnFailures, page.Executions...)
		logrus.WithField("sfnFailures", len(sfnFailures)).Info("still looking...")
		t := page.Executions[len(page.Executions)-1].StartDate
		return t != nil && lookToDate.Before(*t)
	})

	if err != nil {
		return errors.Wrap(err, "failed to look up executions")
	}

	logrus.WithField("sfnFailures", len(sfnFailures)).Info("finished looking for sfn failures")

	originIDs := make(map[string][]ExecutionFailureInfo, 0)
	var stepFunctionFailureCount int
	for i, failure := range sfnFailures {
		if failure == nil || failure.StartDate == nil || failure.StartDate.Before(lookToDate) {
			continue
		}

		stepFunctionFailureCount += 1

		percentThrough := float64(i) / float64(len(sfnFailures)) * 100
		modPercentThrough := int(percentThrough) % 5
		if modPercentThrough >= 4 && modPercentThrough <= 6 {
			logrus.Infof("%2.2f%% through describe executions...", percentThrough)
		}

		resp, err := client.DescribeExecution(&sfn.DescribeExecutionInput{
			ExecutionArn: failure.ExecutionArn,
		})

		if err != nil {
			return errors.Wrap(err, "failed to describe execution failure")
		}

		if resp == nil {
			logrus.WithField("failure", failure.ExecutionArn).Info("describe execution returned nothing here")
		}

		var executionInput ExecutionInput
		err = json.Unmarshal([]byte(*resp.Input), &executionInput)
		if err != nil {
			return errors.Wrap(err, "failed to unmarshal execution input")
		}

		executionHistory := make([]*sfn.HistoryEvent, 0)
		err = client.GetExecutionHistoryPages(&sfn.GetExecutionHistoryInput{
			ExecutionArn: failure.ExecutionArn,
		}, func(page *sfn.GetExecutionHistoryOutput, b bool) bool {
			executionHistory = append(executionHistory, page.Events...)
			return page.NextToken != nil
		})

		if err != nil {
			return errors.Wrap(err, "failed to get execution history")
		}

		var stepFailure string
		var currTaskName string
		for _, item := range executionHistory {
			if item.Type != nil && *item.Type == "TaskStateEntered" {
				currTaskName = *item.StateEnteredEventDetails.Name
			}
			if item.Type != nil && *item.Type == "LambdaFunctionFailed" {
				stepFailure = currTaskName
				break
			}
		}

		originIDs[executionInput.OriginID] = append(originIDs[executionInput.OriginID], ExecutionFailureInfo{
			ExecutionArn: *failure.ExecutionArn,
			StepFailed:   stepFailure,
			SenderID:     executionInput.SenderID,
			Platform:     executionInput.Platform,
			GiftType:     executionInput.GiftType,
		})
	}

	file, err := os.Create(outputFile)
	if err != nil {
		return errors.Wrap(err, "failed to open file for output")
	}
	writer := csv.NewWriter(file)

	fmt.Println("failed step function purchase orders")
	stepFailureCounts := map[string][]string{}
	for originID, failedExecutions := range originIDs {
		fmt.Printf("%v (Count: %d)\n", originID, len(failedExecutions))
		for _, failedExecutionInfo := range failedExecutions {
			if _, ok := stepFailureCounts[failedExecutionInfo.StepFailed]; ok {
				stepFailureCounts[failedExecutionInfo.StepFailed] = append(stepFailureCounts[failedExecutionInfo.StepFailed], failedExecutionInfo.ExecutionArn)
			} else {
				stepFailureCounts[failedExecutionInfo.StepFailed] = []string{failedExecutionInfo.ExecutionArn}
			}
		}
		err := writer.Write([]string{originID, failedExecutions[0].SenderID, failedExecutions[0].Platform, failedExecutions[0].GiftType})
		if err != nil {
			return errors.Wrap(err, "failed to write output")
		}
	}
	writer.Flush()

	fmt.Println()
	fmt.Println("failed step counts")
	for step, failedExecutions := range stepFailureCounts {
		fmt.Printf("%v (Count: %d)\n", step, len(failedExecutions))
	}

	logrus.WithFields(logrus.Fields{
		"failedSfnForPeriod":  stepFunctionFailureCount,
		"purchaseOrdersFound": len(originIDs),
		"duration":            time.Since(startTime),
	}).Info("script finished")

	return nil
}
