### Transactions Table
Prism has a central `transactions` table that contains a lot of information about each transaction that goes through Prism, including things like which tenants failed, what values each tenant returned, what transformations were applied to before sending a message to chat, etc. It is very useful for debugging purposes and should be a good place to start when looking into problems with certain transactions.

In order to query for a specific transaction, you can use Prometheus's Search API to look up a user or channel's transactions and retrieve the `transaction_id` from the `event_id` field. Then, you can use DynamoDB's Query functionality with the `transaction_id` you retrieved to get the transaction entry.

An example input body for `/Search` in Prometheus is:

```
{
	"event_type": "Bits",
	"event_sub_type": "GiveBitsToBroadcaster",
	"sort_direction": "DESCENDING",
	"user_id": "1234",
	"channel_id": ""
}
```