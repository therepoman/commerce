# bits-oncall
A source of knowledge for Bits Oncalls.

## Table of Contents

* [Checklist](checklist) - Checklist, are you ready to be on call for the bits team?
* [Services](services) - Oncall and documentation information for services that we call, call one of our services.
* [Schedule](schedule) - Schedule of things you need to keep in mind while on call.
* [Scripts](scripts) - A collection of useful scripts for common oncall tasks.
* [Runbook](runbook) - General task instructions go here!
* [Bits Data Audit](https://docs.google.com/spreadsheets/d/1V3NcORWiUPIxvgkFnuiYSAGGguG1A_icwG5LUe_zwmE/edit) - Audit of all datastores used by bits and what the impact of their loss would be and how they're backed up.
* [Bits Oncall Notes](https://docs.google.com/document/d/1jYyrrS8wgafSMIp5-rXW8Qtz1yWZrv8uwcIzJIwBBwA/edit#)   - Notes for weekly oncalls.

## Bits Services
* Payday
  * [Dashboard](https://isengard.amazon.com/federate?account=021561903526&role=Admin&destination=cloudwatch%2Fhome%3Fregion%3Dus-west-2%23dashboards%3Aname%3DPayday)
  * [GraphQL Dependency Dashboard](https://grafana.internal.justin.tv/d/1lVBTXAiz/graphql-dependency-details?from=now-30m&to=now-1m&orgId=1&var-env=production&var-region=us-west-2&var-service=Payday&var-method=All&var-all_services=All)
  * [Rollbar](https://rollbar.com/Twitch/Payday/)
  * [Git](https://git.xarth.tv/commerce/payday)
* FABS  
  * [Dashboard](https://w.amazon.com/index.php/Twitch/FABS/Dashboard/Prod)
  * [FABS RTLA](https://rtla.amazon.com/explore.do?org=BITSFABS)
  * [Git/Service](https://code.amazon.com/packages/FuelAmazonBitsService/trees/mainline)
  * [Git/Model](https://code.amazon.com/packages/FuelAmazonBitsServiceModel/trees/sponsored-cheermotes)
* Pantheon
  * [Dashboard](https://isengard.amazon.com/federate?account=375121467453&role=Admin&destination=cloudwatch%2Fhome%3Fregion%3Dus-west-2%23dashboards%3Aname%3DPantheonDashboard)
  * [Rollbar](https://rollbar.com/Twitch/Pantheon/)
  * [Git](https://git.xarth.tv/commerce/pantheon)
* Perseus
  * [Git](https://code.amazon.com/packages/FuelAmazonBitsServicePerseusClient/trees/mainline)
* Phanto
  * [Dashboard](https://isengard.amazon.com/federate?account=748615074154&role=Admin&destination=cloudwatch%2Fhome%3Fregion%3Dus-west-2%23dashboards%3Aname%3DPhanto)
  * [Git](https://git.xarth.tv/commerce/phanto)
* Prometheus
  * [Dashboard](https://isengard.amazon.com/federate?account=859821054041&role=Admin&destination=cloudwatch%2Fhome%3Fregion%3Dus-west-2%23dashboards%3Aname%3DPrometheus)
  * [Git](https://git.xarth.tv/commerce/prometheus) 
* Pachinko
  * [Dashboard](https://isengard.amazon.com/federate?account=702056039310&role=Admin&destination=cloudwatch%2Fhome%3Fregion%3Dus-west-2%23dashboards%3Aname%3DPachinko)
  * [Git](https://git.xarth.tv/commerce/pachinko)
* Portent
  * [Dashboard](https://isengard.amazon.com/federate?account=272653928523&role=Admin&destination=cloudwatch%2Fhome%3Fregion%3Dus-west-2%23dashboards%3Aname%3DPortent)
  * [Git](https://git.xarth.tv/commerce/portent)
* Prism 
  * [Dashboard](https://isengard.amazon.com/federate?account=033842175527&role=Admin&destination=cloudwatch%2Fhome%3Fregion%3Dus-west-2%23dashboards%3Aname%3DPrism)
  * [Git](https://git.xarth.tv/commerce/prism)
* Fortuna
  * [Dashboard](https://grafana.internal.justin.tv/d/000001475/fortuna?refresh=10s&orgId=1)
  * [Git](https://git.xarth.tv/commerce/fortuna)
* Petozi
  * [Dashboard](https://isengard.amazon.com/federate?account=919883070396&role=Admin&destination=cloudwatch%2Fhome%3Fregion%3Dus-west-2%23dashboards%3Aname%3DPetozi)
  * [Git](https://git.xarth.tv/commerce/petozi)
* Pachter
  * [Dashboard](https://isengard.amazon.com/federate?account=458492755823&role=Admin&destination=cloudwatch%2Fhome%3Fregion%3Dus-west-2%23dashboards%3Aname%3DPachter)
  * [Git](https://git.xarth.tv/commerce/pachter)
* TARFFU (TwitchAmazonReputationForFraudUploader)
  * [Git](https://code.amazon.com/packages/TwitchAmazonReputationForFraudUploaderService/trees/mainline)
* TwitchTruexReporting (to be transitioned over to ads team at a later point)
  * [Git](https://code.amazon.com/packages/TwitchTruexReporting/trees/mainline)
* Pagle
  * [Git](https://git.xarth.tv/commerce/pagle)
* Poliwag
  * [Dashboard](https://tiny.amazon.com/1dzvfwc41/IsenLink)
  * [Git](https://git.xarth.tv/commerce/poliwag)
* Clippy
  * [Dashboard](https://tiny.amazon.com/69xvpgqx/IsenLink)
  * [Git](https://git.xarth.tv/subs/clippy)
* Personalization
  * [Dashboard](https://w.amazon.com/bin/view/Twitch/Fulton/TwitchPersonalization/Dashboard)
  * [Git](https://code.amazon.com/packages/TwitchPersonalization/trees/mainline)
  * [Pipeline](https://pipelines.amazon.com/pipelines/TwitchPersonalization)