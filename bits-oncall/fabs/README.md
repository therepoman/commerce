# Connecting to a FABS box
In order to be able to connect to the FABS boxes, you must:

1. Be on wpa2
2. Have the latest mwinit. 
  a. Learn how to [set it up here](https://w.amazon.com/index.php/NextGenMidway/UserGuide#Client_Environment_Setup_.28for_CLI_or_SSH.29)
  b. Learn how to [upgrade it here](https://w.amazon.com/index.php/NextGenMidway/UserGuide#How_to_upgrade_mwinit_on_Mac)
3. Go to [FABS apollo](https://apollo.amazon.com/environments/FuelAmazonBitsService/stages/Prod) and pick a host to ssh to from the collection of Auto Scaling Groups
4. ssh into your host `ssh [fabs-host-name]` where fabs-host-name looks like `fabs-prod-1a-i-d56bb745.us-east-1.amazon.com` but probably isn't that one specifically

# Read Logs
run `cd /apollo/env/FuelAmazonBitsService/var/output/logs`
run `tail -f` on an application.log.[today's date] file

# Read Previous logs via Timber

## Connecting to a Timber box
1. Go to [Timber apollo](https://apollo.amazon.com/environments/TimberFS/IAD/TwitchCommerce/) and pick a host to ssh to from the collection of Auto Scaling Groups
2. ssh into your host `ssh [timber-host-name]` where timber-host-name looks like `twitch-timber-c3-xlrg-97270b4b.us-east-1.amazon.com` but probably isn't that one specifically

## Reading logs
The logs on Timber boxes are always stored per service (FABS, Perseus, TAILS) then by year, by day, and then hourly. Each log group is gzipped, which means you can't simply `cat | grep` the logs. Note that the logs are stored based on pacific time.

run `find /onlinelogs/FuelAmazonBitsService/Prod/ -name application.log.\*.gz -print0 | xargs -0 zgrep "{searchTERM}"` with `{searchTERM}` replaced with your search term. This will find every application log file for fabs, and use those outputs as inputs for zgrep, which is like grep but for zipped files.

# Dumping DLQ'd messages on the ADG Order Queue
Messages sent to us by adg are encrypted so you'll need to run a special script if you want to read any of them. After connecting into a FABS box (see above) 

run `sudo -u twitches /apollo/env/FuelAmazonBitsService/bin/ADGMessageDumper`

## RTLA

* [FABS RTLA](https://rtla.amazon.com/explore.do?org=BITSFABS)

* [Onboarding Status](https://rtla-onboarding.corp.amazon.com/onboarding/BITSFABS)

* [RTLA Health Dashboard](https://w.amazon.com/index.php/Twitch/FABS/RTLA)

* [FABS Sushi Dashboard](https://sushi.amazon.com/app?app_name=com.amazon.FuelAmazonBitsService)