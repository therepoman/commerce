# Emote Royalty Config
## About
When a new cheermote is launched we need to configure royalty payouts in our redshift DB.
A given cheermote can produce revenue for multiple payout entities. 
A payout entity can either be a Twitch User ID or an arbitrary string (ex: Streamlabs).

## Process
- Add a new SQL update script to payday's migrations folder for posterity. The script should add additional rows to the emoteroyaltypayoutconfigs table.
- Get your script reviewed and merged into payday master.
- Run your script against the redshfit DB using your favorite SQL tool (ex: SQLWorkbench).

## EmoteRoyaltyPayoutConfigs Table
- EmoteCode: The emote code that can be typed in chat to produce this cheermote (all lowercase).
- TwitchUserID: A payout entity that is a Twitch User. If set OtherPayoutEntity should be null.
- OtherPayoutEntity: A payout entity that is not a Twitch User. If set TwitchUserID should be null
- RevShareCents: Amount of royalty cents received for each bits used. Standard is $0.01 per 100 bits. 
- EffectiveStartDate: Royalty revenue will not show up in reports for bits transactions before this date.
- EffectiveEndDate: Royalty revenue will not show up in reports for bits transactions after this date.