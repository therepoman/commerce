# Runbook

I, _Nick Evans_, hereby allow the poor soul reading this to page me for any Poliwag issues after some best effort analysis until I finish fleshing out this runbook.

---> page-nevans@amazon.com <----

## Alarms

All poliwag alarms are maintained in [Poliwag's terraform configuration](https://git.xarth.tv/commerce/poliwag/tree/master/terraform). These alarms are used to create corresponding alarms in [Carnival](https://carnaval.amazon.com/v1/unifiedSearch/v2018/simpleSearch.do?searchFormType=&searchString=poliwag).

## Dashboards

| Dashboards |
| --- | 
| **[Poliwag Service Dashboard](https://us-west-2.console.aws.amazon.com/cloudwatch/home?region=us-west-2#dashboards:name=Poliwag)** |
| **[Step Functions Dashboard](https://us-west-2.console.aws.amazon.com/cloudwatch/home?region=us-west-2#cw:dashboard=States)** |
| **[RDS Dashboard](https://us-west-2.console.aws.amazon.com/cloudwatch/home?region=us-west-2#cw:dashboard=RDS)** |
| **[Dynamo Dashboard](https://us-west-2.console.aws.amazon.com/cloudwatch/home?region=us-west-2#cw:dashboard=DynamoDB)** |

## Step Functions

**[Step Functions Dashboard](https://us-west-2.console.aws.amazon.com/cloudwatch/home?region=us-west-2#cw:dashboard=States)**

Poliwag makes use of [AWS Step Functions](https://docs.aws.amazon.com/step-functions/latest/dg/getting-started.html) for managing poll lifecycles. These step functions run their executions on [AWS Lambda](https://docs.aws.amazon.com/lambda/latest/dg/getting-started.html) functions.

### [Poll Complete](https://us-west-2.console.aws.amazon.com/states/home?region=us-west-2#/statemachines/view/arn:aws:states:us-west-2:246232734983:stateMachine:poll_complete_v1_state_machine)

Started when a poll is created via the CreatePoll API. 

This workflow waits `n` seconds to start, where `n` is `the duration of the poll`. If the poll is still active at this time, the workflow will query RDS one last time for poll and choice aggregations, set the poll to status `COMPLETED`, and emit a PollComplete pubsub. If the poll is not active (meaning the broadcaster terminated or archived the poll), the workflow will do nothing.

### [Poll Terminate](https://us-west-2.console.aws.amazon.com/states/home?region=us-west-2#/statemachines/view/arn:aws:states:us-west-2:246232734983:stateMachine:poll_terminate_v1_state_machine)

Started when a poll is terminated by a broadcaster via the TerminatePoll API. 

This workflow queries RDS one last time for poll and choice aggregations, sets the poll to status `TERMINATED`, and emits a PollTerminate pubsub.

### [Poll Archive](https://us-west-2.console.aws.amazon.com/states/home?region=us-west-2#/statemachines/view/arn:aws:states:us-west-2:246232734983:stateMachine:poll_archive_v1_state_machine)

Started when a poll is archived by a broadcaster via the ArchivePoll API.

This workflow queries RDS one last time for poll and choice aggregations, sets the poll to status `TERMINATED`, and emits a PollArchive pubsub.

### [Poll Moderate](https://us-west-2.console.aws.amazon.com/states/home?region=us-west-2#/statemachines/view/arn:aws:states:us-west-2:246232734983:stateMachine:poll_moderate_v1_state_machine)

Started when a poll is moderated via the ModeratePoll API.

This workflow queries RDS one last time for poll and choice aggregations, sets the poll to status `MODERATED`, and emits a PollModerated pubsub.

### An execution failed, what do?
All workflows have proper checks in place such that they can be safely restarted via [the Seph Newman patented failed step function redriver](https://git.xarth.tv/commerce/poliwag/tree/master/cmd/stepfn-redriver) or manually in the AWS console. As always, check service and step execution first to see what the actual error is before restarting.

## RDS
**[RDS Dashboard](https://us-west-2.console.aws.amazon.com/cloudwatch/home?region=us-west-2#cw:dashboard=RDS)**

Poliwag currently makes use of a [MySQL instance managed by Amazon Aurora](https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/Aurora.AuroraMySQL.Overview.html) to store and query vote instances. So every time a user votes, we save a new entry in MySQL.

### Connecting to the MySQL Database

First, ensure that you are in the sandstorm permission role [here](https://dashboard.internal.justin.tv/sandstorm/manage-roles?nameFilter=twitch-poliwag-prod&action=editrole&role=teleport-remote-poliwag-prod) 

Enable teleport bastion:
```
teleport-bastion enable
```

SSH into the Poliwag jumpbox using teleport bastion. This is our gateway to our VPC - RDS is not accessible publicly. 

```
TC=twitch-poliwag-prod ssh <jumpbox-private-dns-in-ec2-console>
```

Now that you are in the VPC, connect to the MySQL instance. You'll need to get the password using the [Sandstorm CLI](https://wiki.twitch.com/display/SSE/Sandstorm+CLI) for the `commerce/poliwag/production/aurora-master-password` secret. 

Ensure your ARN is in the [poliwag-prod role](https://dashboard.internal.justin.tv/sandstorm/manage-roles?nameFilter=poliwag&action=editrole&role=poliwag-prod) and run:
``` 
sandstorm get --role-arn arn:aws:iam::734326455073:role/sandstorm/production/templated/role/poliwag-prod commerce/poliwag/production/aurora-master-password
```

Now connect to MySQL and supply the password. 
```
mysql --host=<poliwag-db-production-cluster-endpoint-in-rds-console> --port=3306 --user=admin --password
```

And make sure you are using the poliwag database:

```mysql
use poliwag;
```

### RDS Tables
#### Votes 

```mysql
SELECT * FROM votes LIMIT 10;
```

## Dynamo
**[Dynamo Dashboard](https://us-west-2.console.aws.amazon.com/cloudwatch/home?region=us-west-2#cw:dashboard=DynamoDB)**

Dynamo provides quick, persistent storage for Polls, Choices, and Voters in Poliwag. All APIs interface directly with Dynamo - no APIs call RDS.

### Polls Table
Stores Poll level information, including voter totals, token totals, vote totals, and settings.

### Choice Table
Stores Choice level information, including voter totals, token totals, and vote totals.

### Poll Voters Table
Store Poll level information about a Voter in a poll, including total number of tokens spent, total number of votes, and subscriber status. 

### Choice Voters Table
Store Choice level information about a Voter for a choice, including total number of tokens spent for that choice and total number of votes for that choice.

