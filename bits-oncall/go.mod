module code.justin.tv/commerce/bits-oncall

go 1.15

require (
	code.justin.tv/chat/timing v1.0.1 // indirect
	code.justin.tv/commerce/gogogadget v0.0.0-20190624182939-1153b7d0706e // indirect
	code.justin.tv/commerce/logrus v1.1.2
	code.justin.tv/commerce/pachinko v0.0.0-20190712220425-aaed3a7737f0
	code.justin.tv/commerce/payday v0.0.0-20190711214020-95d630387701
	code.justin.tv/common/yimg v0.0.0-20190311202211-55ce6468b269 // indirect
	code.justin.tv/feeds/ctxlog v0.0.0-20170328220514-514fab8fb5b7 // indirect
	code.justin.tv/feeds/feeds-common v1.2.2 // indirect
	code.justin.tv/foundation/twitchclient v4.11.0+incompatible
	code.justin.tv/foundation/xray v0.0.0-20171002153549-42df0dc4ee6b // indirect
	code.justin.tv/revenue/everdeen v0.26.17
	code.justin.tv/revenue/mulan v1.0.13
	code.justin.tv/revenue/payments-service-go-client v0.1.43
	code.justin.tv/sse/malachai v0.6.0
	code.justin.tv/sse/policy v0.0.1 // indirect
	code.justin.tv/web/users-service v2.4.1-0.20190827222711-4c8533680844+incompatible
	github.com/SermoDigital/jose v0.9.2-0.20161205224733-f6df55f235c2 // indirect
	github.com/afex/hystrix-go v0.0.0-20180502004556-fa1af6a1f4f5 // indirect
	github.com/asaskevich/govalidator v0.0.0-20200907205600-7a23bdc65eef // indirect
	github.com/aws/aws-sdk-go v1.34.0
	github.com/go-errors/errors v1.1.1 // indirect
	github.com/go-pg/pg v8.0.7+incompatible
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	github.com/gofrs/uuid v3.3.0+incompatible
	github.com/guregu/dynamo v1.2.1 // indirect
	github.com/heroku/rollbar v0.6.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/satori/go.uuid v1.2.0
	github.com/urfave/cli v1.22.5
	golang.org/x/net v0.0.0-20200625001655-4c5254603344
	mellium.im/sasl v0.2.1 // indirect
)

replace gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.3 => github.com/DATA-DOG/go-sqlmock v1.3.3

replace github.com/cactus/go-statsd-client v3.1.1+incompatible => github.com/cactus/go-statsd-client v0.0.0-20200728222731-a2baea3bbfc6

replace github.com/cactus/go-statsd-client/statsd v3.1.1+incompatible => github.com/cactus/go-statsd-client/statsd v0.0.0-20200728222731-a2baea3bbfc6
