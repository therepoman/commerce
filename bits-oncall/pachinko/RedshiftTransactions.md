## Redshift Transactions Ingestion Failure

We have a data stream `transactions-to-redshift-production` delivering transactions ledger into Redshift from S3. This may occasionally fail (after retrying for two hours) and we would need to manually backfill the data.
***It is important to do this within 30 days*** of the creation of the S3 bucket, as that is the TTL for the S3 buckets (they are deleted for privacy reasons, regarding GDPR). 

### Manually COPY the Data to Redshift
- Errors logs can be found in cloudwatch logs under the filter "/aws/kinesisfirehose/transactions-to-redshift-production".
- If the error is retryable:
    * Find the failed manifest files in Pachinko's S3 bucket `pachinko-production-redshift/transactions/errors`. Manifest files are grouped by date and hour.
    * Find the failed Firehose stream in the AWS console under Kinesis/DataFirehose. On its detail page, you should find a Redshift COPY command, which looks similar to:
    ```
     COPY transactions FROM 's3://pachinko-production-redshift/<manifest>' CREDENTIALS 'aws_iam_role=arn:aws:iam::<aws-account-id>:role/<role-name>' MANIFEST json 'auto';
    ```
    * Fill in the COPY command manifest file path, along with the account ID (`702056039310`) and role name (use `firehose_delivery_role`)
    * Connect to the Redshift cluster and execute the COPY command. Redshift credentials can be found in the team's shared 1Password vault.
