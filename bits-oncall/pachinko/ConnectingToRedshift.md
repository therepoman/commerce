## Connecting to Redshift

Pachinko has several Lambda functions that are used to delete user information in Redis, Dynamo, and Redshift for privacy reasons. 
It stores it's transactions ledger in Redshift as a backup, which contains user information and must be cleaned up. To do this, we need programmatic access to Redshift from the Lambdas. 
 
### Redshift Client
There is a [Redshift client](https://git.xarth.tv/commerce/pachinko/tree/master/lambdas_backend/clients/redshift/client.go) in the `lambdas_backend` directory that creates a connection to the Redshift cluster for the environment it is in. 
It does this by accessing the SecretsManager to get username and password, database name, and host and post number. 
It uses the [gorm](https://gorm.io/docs/connecting_to_the_database.html) library to then open a connection to the cluster.
This client is then used in the `delete_transactions_by_owner_id` and `delete_transactions_by_group_id` lambdas to delete transactions records in Redshift.

### Ensuring your client can connect to the cluster
Pachinko's Redshift cluster needs to be Publicly Accessible so that the Kinesis Firehose data stream can access it. This data stream copies the transactions ledger from S3 (uploaded there from the Ledgerman lambda) into the cluster.
The Redshift cluster thus needs to be in the public subnet of the VPC, whereas the service itself is in the Private subnet. To allow a connection request from the lambda (in the private subnet) to the Redshift cluster (public subnet) we must configure the cluster's security group to allow the right traffic through.  
Connection requests from the private subnet will go through a NAT gateway to the public subnet, so we must create an [Inbound Rule](https://tiny.amazon.com/1atpzr3xs/IsenLink) in the `redshift-access` security group to allow those requests to access the cluster. This is the sg that is associated with the Redshift Cluster.
To do this, click `Edit Inbound Rules` for the sg. Add a rule of type Redshift (TCP, port 5439) and add all the NAT gateway IPs as Custom sources.  

While this allows the service/lambdas to connect to Redshift, the security group does not allow access for local development. There would need to be additional Inbound Rules created for this. 
