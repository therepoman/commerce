### So something has gone wrong with Pachter, huh?

For the time being I'm going to assume it's an issue with bits-spend (based on prior experience).

### There are messages in the bits-spend DLQ, what should I do?

The first thing you should try is resetting the bits spend order for that user.

Short version:

* If you preview the DLQ and you can tell the users are already unique then you can continue on this shorter path, if you have duplicate user id's jump to the long version.
* You can tell the user id because it will be appended to the string `bits-spend-order` put in the field group id, for example `bits-spend-order-434363685` for user 434363685.
* Go to Pachter's Lambda console and select [BitsSpendOrderResetter][pachter-spend-order-resetter-lambda]. We don't have any configs to pass, so click "_Configure test events_" and make the input just an empty json `{}`. Then click "_Test_" to run the Lambda. Don't worry about the input, it doesn't matter, all we want here is to run the Lambda.

Long (through) version:

1. Clone the pachter repo if you don't already have it, either way navigate to that folder.
2. Change your AWS creds to Pachter (production)'s.
3. Run the script in `scripts/bits-spend-order-dlq-retriever/main.go` (it takes no parameters, so just `go run` it) to get all the DLQ messages into a CSV file. **This will clear the DLQ**, so be careful no lose overwrite your file, it might be worth making a copy.
4. Dedupe the messages in CSV by user id, I would recommend opening in Excel or another spreadsheet editor and using the sort function on the user id column to accomplish this. Copy the CSV into another file and edit it, so that there will be a unique user in each line  <br><br>
   (for example)  
   If the content of CSV looks like:  
   transaction-id-1,user-1  
   transaction-id-2,user-1  
   transaction-id-3,user-2  <br><br>
   Then you want to get rid of either the first or the second line so that in the CSV, so that the user only occurs once.  
   transaction-id-1,user-1  
   transaction-id-3,user-2  
   Don't worry that you seem to be dropping transaction-id-2, the reset script is per user, not per transaction.  <br><br>
5. Once you made that copy file with unique user and his transaction id per line, then run the script in `scripts/bits-spend-order-resetter/main.go` like so:
`go run scripts/bits-spend-order-resetter/main.go -i copyFile.csv -e prod`. This will send the unique users and their transaction ids back into the DLQ
6. Go to Pachter's Lambda console and select [BitsSpendOrderResetter][pachter-spend-order-resetter-lambda]. We don't have any configs to pass, so click "_Configure test events_" and make the input just an empty json `{}`. Then click "_Test_" to run the Lambda. Don't worry about the input, it doesn't matter, all we want here is to run the Lambda.
7. The Lambda should then start pulling messages from the DLQ and start resetting/recalculating spend order for each user. Normally it succeeds unless there is an actual data issue (e.g. there is a missing transaction for the user in the bits-transactions RDS table or Payday's transactions/extension_transactions dynamo tables). If the Lambda times out, it is safe to rerun it to process the remaining records, but be mindful of the in flight number though as the lambda can't poll those and they may have been left orphaned by a previous run.

### I have bits spend items still failing even after a reset.

Ok, so this can have a couple different problems and we're going to check them each in order of frequency. First up you should be able to use the AWS console to preview and pull all the affected transaction ids (and user ids). We'll search following places for data on the transaction:

* Check Payday's [transactions][payday-transactions-dynamo], [extension_transactions dynamo][payday-extension-transaction-dynamo] and [in_flight_transactions][payday-in-flight-transaction-dynamo] tables to see if the record is present.
    * If not jump to [My transaction is missing from all of Payday's transaction tables, how do I fix this?](#mytransactionismissingfromallofpaydaystransactiontableshowdoifixthis)
* Check the Pachter [worker-audit table][pachter-worker-audit-dynamo] to see if it has an entry for `cheermote`,`transactions` and `spend`. This table records when a particular worker has finished processing a given message and it's very useful for debugging problems of this sort. If only spend is missing you are actually in pretty good shape, we just need to figure out why spend isn't processing.
* Check the [Pachter logs][pachter-worker-logs] for the transaction id that you're having trouble with, if it's `found no past entitlements for the remaining spending` then we definitely have an order of processing issue and we're probably missing an earlier entitlement, if it's another error I'd recommend looking at the worker code and seeing what the cause could be.
* Check the `bits-transactions` table in Pachter's RDS.
    * This one is a little more complicated, I'm sorry, about that, but the tiny bubbles mean we'll need to use the teleport bastion jumpbox to create an RDS proxy for us to hit Pachter's RDS instance for that. The most direct way to do that is to run this command `TC=twitch-pachter-prod ssh -L 5432:rds-instance-pachter-production.cyp8xvcbw7cz.us-west-2.rds.amazonaws.com:5432 jumpbox`, note you will need to enable teleport-bastion before this will work.
    * Next run a query like this, but with the user id you're debugging.  `SELECT transaction_id, transaction_type, quantity, datetime, cheermote_usages, message FROM bits_transactions WHERE transaction_id = '{transaction_id}'` to fetch it from RDS.

Now that you've found the transaction work on restoring it to Payday or Pachter RDS and then run the spend backfill script.
    
### My transaction is missing from all of Payday's transaction tables, how do I fix this?

You're going to have to recreate this mostly by hand, there is a script for inserting it into Payday's dynamo table, but we're going to first need to make a CSV by hand to hand to the admin job system using Datascience, Prism and Pachinko (depending on your needs).

* For most operations you can grab the necessary data from [Pachinko's ledgerman][pachinko-ledgerman].
* Cheering data you can pull [Prism's transactions dynamo table][prism-transaction-dynamo].
* For bits purchases you'll need to grab payment data from mode analytics' payments tables (for WATEB entitlements Pachinko should have everything you need).
* You will need to populate this data in a very particular CSV format, I'd recommend checking the script for that since it does get updated as we find gaps, it's located in payday at `cmd/transaction_backfill/main.go`.

### I have problems with Payday Raincatcher or Datascience DLQ.

* Check the dashboard widget to see what the transaction id's are [Project Andes Dashboard][andes-metrics]. If you click on the hamburger button (⠇) in the widget (_View in CloudWatch Logs Insights_) you can then export the results as a CSV and use that for scripts.
* Go to general troubleshooting.

### I need to connect to Pachter's RDS to debug something

* Make your AWS credentials the ones you have for Pachter production.
* Run this command to create a proxy for connecting to RDS `TC=twitch-pachter-prod ssh -L 5432:rds-instance-pachter-production.cyp8xvcbw7cz.us-west-2.rds.amazonaws.com:5432 jumpbox`
* Get the username & password from 1Password, it's in the shared vault for bits-team (if you don't have access, ask around most of the team should have access).
* In your PostgreSQL client (I highly recommend grabbing [Postico][postico]), configure as localhost:5432/pachter_production, the full URL (minus and creds) will look something like this `postgresql://localhost/PachterProduction`.

### Extra notes

#### Pachter RDS query to grab transactions in order: 

This is very useful if you want to run a tally of the user's transactions or just to view their history in general.

```SQL
SELECT transaction_id, transaction_type,    
  CASE    
    WHEN transaction_type = 'AddBits' or transaction_type = 'AdminAddBits' THEN quantity   
    ELSE quantity * -1    
  END AS quantity,    
  datetime, cheermote_usages, message    
FROM bits_transactions    
WHERE requesting_twitch_user_id = '{user_id}'   
ORDER BY datetime ASC    
```

#### Getting a list of transaction from Pachinko by id

This is really useful when you need to just look up the version of the transaction that ran through Pachinko.

```SQL
SELECT DISTINCT(transaction_id), operation, quantity, timestamp, owner_id FROM transactions WHERE domain_name = 'BITS' AND transaction_id IN (
  '{transaction-id-1}', 
  '{transaction-id-2}')
```

#### Getting a list of unique user ids from Pachinko's ledgerman

This is useful if you have a lot of transactions and you want to reduce it to a list of user ids for debugging.

```SQL
SELECT DISTINCT(owner_id) FROM transactions WHERE domain_name = 'BITS' AND transaction_id IN (
  '{transaction-id-1}', 
  '{transaction-id-2}')
```

[google-sheets]: https://docs.google.com/spreadsheets/u/0/?tgif=d "Google Sheets"

[andes-metrics]: https://isengard.amazon.com/federate?account=021561903526&role=Admin&destination=cloudwatch%2Fhome%3Fregion%3Dus-west-2%23dashboards%3Aname%3DAndes-Metrics "Project Andes Metrics Cloudwatch"
[payday-transactions-dynamo]: https://isengard.amazon.com/federate?account=021561903526&role=Admin&destination=dynamodb%2Fhome%3Fregion%3Dus-west-2%23tables%3Aselected%3Dprod_transactions%3Btab%3Ditems "Payday Transaction Dynamo Console"
[payday-extension-transaction-dynamo]: https://isengard.amazon.com/federate?account=021561903526&role=Admin&destination=dynamodb%2Fhome%3Fregion%3Dus-west-2%23tables%3Aselected%3Dprod_extension_transactions%3Btab%3Ditems "Payday Extension Transaction Dynamo Console"
[payday-in-flight-transaction-dynamo]: https://isengard.amazon.com/federate?account=021561903526&role=Admin&destination=dynamodb%2Fhome%3Fregion%3Dus-west-2%23tables%3Aselected%3Dprod_in_flight_transactions%3Btab%3Ditems "Payday In Flight Transaction Dynamo Console"
[payday-raincatcher]: https://isengard.amazon.com/federate?account=021561903526&role=Admin&destination=redshiftv2%2Fhome%3Fregion%3Dus-west-2%23query-editor%3A

[pachter-spend-order-resetter-lambda]: https://isengard.amazon.com/federate?account=458492755823&role=Admin&destination=lambda%2Fhome%3Fregion%3Dus-west-2%23%2Ffunctions%2FBitsSpendOrderResetter%3Ftab%3Dconfiguration "Pachter Bits Spend Order Resetter Lambda"
[pachter-worker-audit-dynamo]: https://isengard.amazon.com/federate?account=458492755823&role=Admin&destination=dynamodb%2Fhome%3Fregion%3Dus-west-2%23tables%3Aselected%3Dworker-audit-prod%3Btab%3Ditems "Pachter Worker Audit Dynamo"
[pachter-worker-logs]: https://isengard.amazon.com/federate?account=458492755823&role=Admin&destination=cloudwatch%2Fhome%3Fregion%3Dus-west-2%23logEventViewer%3Agroup%3Dpachter-ECSLogGroup "Pachter Worker Cloudwatch Log Group"

[pachinko-ledgerman]: https://isengard.amazon.com/federate?account=702056039310&role=Admin&destination=redshiftv2%2Fhome%3Fregion%3Dus-west-2%23query-editor%3A

[prism-transaction-dynamo]: https://isengard.amazon.com/federate?account=033842175527&role=Admin&destination=dynamodb%2Fhome%3Fregion%3Dus-west-2%23tables%3Aselected%3Dtransactions-prod%3Btab%3Ditems "Prism Transaction Dynamo Console"

[postico]: https://itmarketplace.corp.amazon.com/detail_pages/590 "Postico"