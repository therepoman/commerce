/*
 *      This script fetches the balance from payday and sets the Pachinko balance to match.
 *
 *      Before running this script, make sure:
 *      - Your AWS secrets are stored in a file on your computer under $home/.aws so that it looks like:
 *        (file: $home/.aws/credentials) (<- this is not a part of the file content)
 *        aws_access_key_id = {your_access_key_id}
 *        aws_secret_access_key = {your_secret_access_key}
 *      - Your AWS ARN is registered under "Allowed ARNs" for payday-production
 *
 *      Each entry on the input csv file should look like:
 *      {User ID#}
 *      ex) 7168163
 *          24182350
 *
 *      To run the script
 *      Arg #1 (required): Input file name
 *      Arg #2 (optional): "prod" for production environment. Default to dev. Anything other than "prod" results in dev operation.
 *      Arg #3 (optional): SOCKS proxy to use for Pachinko
 *      ex) go run scripts/pachinko-balance-backfill/pachinko_balance_backfill.go ~/Downloads/test.csv dev localhost:8800
 */

package main

import (
	"bytes"
	"context"
	"encoding/csv"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"

	pachinko "code.justin.tv/commerce/pachinko/rpc"
	pc "code.justin.tv/commerce/payday/clients/pachinko"
	apimodel "code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	"code.justin.tv/commerce/logrus"
	"golang.org/x/net/proxy"
)

const (
	S2S_NAME_PROD = "payday-production"
	S2S_NAME_DEV  = "payday-staging"

	PAYDAY_PROD = "https://prod.payday.twitch.a2z.com"
	PAYDAY_DEV  = "https://main.us-west-2.beta.payday.twitch.a2z.com"

	PACHINKO_PROD = "https://bits.prod.pachinko.internal.justin.tv"
	PACHINKO_DEV  = "https://bits.staging.pachinko.internal.justin.tv"

	PAYDAY_BALANCE_PATH = "/authed/admin/balance/" // + tuid on the end

	OUTPUT_FILE = "output.csv"
)

func isProd() bool {
	if len(os.Args) > 2 {
		if os.Args[2] == "prod" {
			return true
		}
	}
	return false
}

func getHttpClient(logger *logrus.Logger) (client *http.Client) {
	var callerName string

	if isProd() {
		callerName = S2S_NAME_PROD
	} else {
		callerName = S2S_NAME_DEV
	}

	s2sConfig := caller.Config{
		DisableStatsClient: true,
	}

	rt, err := caller.NewRoundTripper(callerName, &s2sConfig, logger)
	if err != nil {
		logrus.WithError(err).Panic("Failed to create s2s round tripper for Payday")
	}

	client = &http.Client{
		Transport: rt,
	}

	logger.WithFields(logrus.Fields{
		"s2s name":      callerName,
		"is production": isProd(),
	}).Printf("Payday client configured")
	return
}

func getPachinkoClient(logger *logrus.Logger) (client pachinko.Pachinko) {
	var callerName string
	var endpoint string

	if isProd() {
		callerName = S2S_NAME_PROD
		endpoint = PACHINKO_PROD
	} else {
		callerName = S2S_NAME_DEV
		endpoint = PACHINKO_DEV
	}

	s2sConfig := caller.Config{
		DisableStatsClient: true,
	}
	rt, err := caller.NewRoundTripper(callerName, &s2sConfig, logger)
	if err != nil || rt == nil {
		logrus.WithError(err).Panic("Failed to create s2s round tripper for Pachinko")
	}

	var proxyEndpoint string
	if len(os.Args) > 3 {
		proxyEndpoint = os.Args[3]
	}

	if len(proxyEndpoint) > 0 {
		dialer, err := proxy.SOCKS5("tcp", proxyEndpoint, nil, proxy.Direct)
		if err != nil {
			logrus.WithError(err).WithField("proxy endpoint", proxyEndpoint).Panic("Failed to create socks proxy")
		}
		rt.SetInnerRoundTripper(&http.Transport{
			Dial: dialer.Dial,
		})
	}

	httpClient := http.Client{
		Transport: rt,
	}

	client = pachinko.NewPachinkoProtobufClient(endpoint, &httpClient)

	logger.WithFields(logrus.Fields{
		"s2s-name":      callerName,
		"using-proxy":   len(proxyEndpoint) > 0,
		"proxy":         proxyEndpoint,
		"endpoint":      endpoint,
		"is-production": isProd(),
	}).Printf("Pachinko client configured")

	return
}

func getUserList(logger *logrus.Logger) [][]string {
	file, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		logrus.WithError(err).Panic("Failed to read file")
	}

	r := csv.NewReader(bytes.NewReader(file))
	input, err := r.ReadAll()
	if err != nil {
		logrus.WithError(err).Panic("Failed to read all the lines of the file")
	}

	logger.WithFields(logrus.Fields{
		"filename":        os.Args[1],
		"number of lines": len(input),
	}).Info("Successfully loaded file")
	return input
}

func getPaydayBalance(client *http.Client, tuid string) int64 {
	var endpoint string
	if isProd() {
		endpoint = PAYDAY_PROD
	} else {
		endpoint = PAYDAY_DEV
	}

	req, err := http.NewRequest("GET", endpoint+PAYDAY_BALANCE_PATH+tuid, nil)
	if err != nil {
		logrus.WithError(err).Panic("Failed when generating request object for payday balance")
	}

	res, err := client.Do(req)
	if err != nil {
		logrus.WithError(err).Panic("Failed when making request to payday for balance")
	}
	defer func() {
		_ = res.Body.Close()
	}()

	balanceResponse := apimodel.GetBalanceResponse{}

	err = json.NewDecoder(res.Body).Decode(&balanceResponse)
	if err != nil {
		logrus.WithError(err).Panic("Failed to decode payday balance response")
	}

	return int64(balanceResponse.Balance)
}

func getPachinkoBalance(client pachinko.Pachinko, tuid string) int64 {
	res, err := client.GetToken(context.Background(), &pachinko.GetTokenReq{
		Domain:  pachinko.Domain_BITS,
		OwnerId: tuid,
		GroupId: pc.GROUP_ID,
	})
	if err != nil {
		logrus.WithError(err).Panic("Failed to get Pachinko balance")
	}
	return res.Token.Quantity
}

func updatePachinkoBalance(client pachinko.Pachinko, tuid string, newBalance int64) int64 {
	res, err := client.OverrideBalance(context.Background(), &pachinko.OverrideBalanceReq{
		Token: &pachinko.Token{
			Domain:   pachinko.Domain_BITS,
			OwnerId:  tuid,
			GroupId:  pc.GROUP_ID,
			Quantity: newBalance,
		},
	})
	if err != nil {
		logrus.WithError(err).Panic("Failed to override Pachinko balance")
	}
	return res.Token.Quantity
}

func main() {
	logger := logrus.New()
	if len(os.Args) < 2 {
		logger.WithField("args", os.Args).Panic("Not enough arguments.")
	}

	// Setting up the CSV writer, I want to put this in a method, but those defer close/flush operations.
	var writer *csv.Writer
	if isProd() {
		file, err := os.Create(OUTPUT_FILE)
		if err != nil {
			logrus.WithError(err).Panic("Failed to create file")
		}
		defer func() {
			_ = file.Close()
		}()

		writer = csv.NewWriter(file)
		defer writer.Flush()

		err = writer.Write([]string{"twitch user id", "payday balance", "pachinko balance", "updated balance"})
		if err != nil {
			logrus.WithError(err).Panic("Failed write header line")
		}
	}

	updatedUsers := 0
	paydayClient := getHttpClient(logger)
	pachinkoClient := getPachinkoClient(logger)
	userIds := getUserList(logger)

	for _, user := range userIds {
		tuid := user[0]
		paydayBalance := getPaydayBalance(paydayClient, tuid)
		pachinkoBalance := getPachinkoBalance(pachinkoClient, tuid)

		if paydayBalance != pachinkoBalance {
			updatedBalance := updatePachinkoBalance(pachinkoClient, tuid, paydayBalance)

			if isProd() && writer != nil {
				err := writer.Write([]string{
					tuid,
					strconv.FormatInt(paydayBalance, 10),
					strconv.FormatInt(pachinkoBalance, 10),
					strconv.FormatInt(updatedBalance, 10),
				})
				if err != nil {
					logrus.WithError(err).Panic("Failed write line to file")
				}
			} else {
				logger.WithFields(logrus.Fields{
					"tuid":                      tuid,
					"payday-balance":            paydayBalance,
					"original-pachinko-balance": pachinkoBalance,
					"updated-pachinko-balance":  updatedBalance,
				}).Info("Balance updated.")
			}
			updatedUsers++
		}
	}

	if isProd() && writer != nil {
		logrus.WithFields(logrus.Fields{
			"count":  updatedUsers,
			"output": OUTPUT_FILE,
		}).Info("Updated users")
	} else {
		logrus.WithField("count", updatedUsers).Info("Updated users")
	}

}
