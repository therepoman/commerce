package main

import (
	"encoding/csv"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type PachterTransaction struct {
	transactionId string
	transactionType string
	quantity int
	datetime string
}

type PromethousTransaction struct {
	eventType string 
	eventSubType string 
	eventID string
	eventTime string
	bitsAmount int
}

const (
	pachterFileName = "pachter.csv"
	promethousFileName = "promethous.csv"
)

func main() {

	lines, err := ReadCsv( fmt.Sprintf(pachterFileName))
	if err != nil {
		panic(err)
	}

	pachterTransactions := make(map[string]PachterTransaction)

	// Loop through lines & turn into object
	for _, line := range lines {
		i, err := strconv.ParseInt(line[2], 10, 64)
		if err != nil {
			panic(err)
		}


		pachterTransaction := PachterTransaction{
			transactionId: strings.TrimSpace(line[0]),
			transactionType: strings.TrimSpace(line[1]),
			quantity: int(i),
			datetime: strings.TrimSpace(line[3]),
		}

		pachterTransactions[strings.TrimSpace(line[0])] = pachterTransaction
	}

	lines, err = ReadCsv( fmt.Sprintf(promethousFileName))
	if err != nil {
		panic(err)
	}

	promethousTransactions := make(map[string]PromethousTransaction)

	// Loop through lines & turn into object
	for _, line := range lines {
		if strings.TrimSpace(line[0]) != "Bits"{
			continue
		}

		i, err := strconv.ParseInt(line[11], 10, 64)
		if err != nil {
			panic(err)
		}

		promethousTransaction := PromethousTransaction{
			eventType: strings.TrimSpace(line[0]),
			eventSubType: strings.TrimSpace(line[1]),
			eventID: strings.TrimSpace(line[2]),
			eventTime: strings.TrimSpace(line[3]),
			bitsAmount: int(i),
		}

		promethousTransactions[strings.TrimSpace(line[2])] = promethousTransaction
	}

	fmt.Println("Looking for missing transactions in Pachter")
	for promethousTransactionId, promethousTransaction := range promethousTransactions {
		_, ok := pachterTransactions[promethousTransactionId]
		if !ok {
			fmt.Printf("Promethous transaction with id %s, type %s, and bit amount %d on %s is missing from pachter\n", promethousTransactionId, promethousTransaction.eventSubType, promethousTransaction.bitsAmount, promethousTransaction.eventTime)
			continue
		}
	}

	fmt.Println("\nLooking for different transactions between Pachter and Promethous")
	for pachterTransactionId, pachterTransaction := range pachterTransactions {
		res, ok := promethousTransactions[pachterTransactionId]
		if ok && res.bitsAmount != pachterTransaction.quantity {
			fmt.Printf("Pachter transaction with id %s and type %s has a bit quantity of %d, while Promethous has %d\n", pachterTransactionId, pachterTransaction.transactionType, pachterTransaction.quantity, res.bitsAmount)
		}
	}

	fmt.Println("\nLooking for missing transactions in Promethous")

	for pachterTransactionId, pachterTransaction := range pachterTransactions {
		_, ok := promethousTransactions[pachterTransactionId]
		if !ok {
			fmt.Printf("Pachter transaction with id %s, type %s, and bit amount %d on %s is missing from Promethous\n", pachterTransactionId, pachterTransaction.transactionType, pachterTransaction.quantity, pachterTransaction.datetime)
			continue
		}
	}

}

// ReadCsv accepts a file and returns its content as a multi-dimentional type
// with lines and each column. Only parses to string type.
func ReadCsv(filename string) ([][]string, error) {

	// Open CSV file
	f, err := os.Open(filename)
	if err != nil {
		return [][]string{}, err
	}
	defer f.Close()

	reader := csv.NewReader(f)
	// Skip first line with metadata
	_, _ = reader.Read()

	// Read File into a Variable
	lines, err := reader.ReadAll()
	if err != nil {
		return [][]string{}, err
	}

	return lines, nil
}


