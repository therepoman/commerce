How to debug spend order for a given user

1. Connect to Pachter RDS (look for instructions in `pachter.md` at the root of this repo)
2. Run this (with the user id filled in) 
```SQL
SELECT transaction_id, transaction_type,    
  CASE    
    WHEN transaction_type = 'AddBits' or transaction_type = 'AdminAddBits' THEN quantity   
    ELSE quantity * -1    
  END AS quantity,    
  datetime, cheermote_usages, message    
FROM bits_transactions    
WHERE requesting_twitch_user_id = '{user_id}'   
ORDER BY datetime ASC    
```
3. Export the result to csv, then save as `pachter.csv` in this folder
4. Go to admin panel (`https://admin-panel.xarth.tv/transactions/entries`)
5. Fill in the user id in the `user_id` search box. 
6. Click search
7. Click "Download"
8. Save file as `promethous.csv` in this folder
9. Run `go run main.go`
10. You'll see something like this: 
![example](example.png)