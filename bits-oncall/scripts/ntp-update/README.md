# Updating the NTP time on our hosts

Hosts drift, hopefully not too much. Here's a script to help align our hosts again.

1. Add the hosts you're syncing to the `hosts.txt` file
2. Make sure you have SSH access to Twitch hosts (Different than Amazon / Midway) and you are on the Twitch VPN
3. Run `python update-time.py`
4. Be patient. It takes a while.
