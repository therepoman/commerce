# Enabling Bits from A list of channels
To enable its from a list of channels the first step in the process is to install the enable bits script from payday.
```bash
#make sure that either your $GOBIN or the $GOPATH/bin directory has been added to your path
export PATH=$PATH:$GOPATH/bin

# cd into the bits-oncall directory
cd $GOPATH/code.justin.tv/commerce/bits-oncall

#install the script
go install ./...
```

You may want to consider adding the following environment variables
this will keep you from having to specify db passwords everytime you run the script

```bash
export RAINCATCHER_DB_PASSWORD="*****"
export RAINCATCHER_DB_USER="*****"
export RAINCATCHER_DB_ADDRESS="52.32.31.152:5439"
export RAINCATCHER_DB_DATABASE="prod"
```

You should be able to run the script now by running
```
go run ./scripts/enable-bits/main.go -e=production -dir="/Users/andymc/go-workspace/src/code.justin.tv/commerce/payday/" --user=$RAINCATCHER_DB_USER --password=$RAINCATCHER_DB_PASSWROD --payouts-disabled scripts/channels.csv
```
The output should look something like this if successful:
```
Changing working directory to /Users/andymc/go-workspace/src/code.justin.tv/commerce/payday
Enabling channel 108706200
Succesfully enabled all users
```

The script expects for the input file to be line separated list of user ids. For more help on script see below. 
 
## Help Menu

```
 2017-09-18 14:56:37 ☆  |2.3.0| a45e60c07255 in ~/go-workspace/src/code.justin.tv/commerce/bits-oncall
± |master ↑1 ?:1 ✗| → enable_bits -h
NAME:
   enable_bits - A new cli application

USAGE:
   enable_bits [global options] command [command options] [arguments...]

VERSION:
   v0.0.1

COMMANDS:
     help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --dir value, -d value          Your payday directory (default: "/Users/andymc/go-workspace/src/code.justin.tv/commerce/payday")
   --environment value, -e value  The payday environment to emulate (default: "staging")
   --payouts-disabled             Marks all the channels as not payable
   --password value               The password used to connect to redshift [$RAINCATCHER_DB_PASSWORD]
   --user value                   The user used to connect to redshift [$RAINCATCHER_DB_USER]
   --addr value                   The ip address and port used to connect to redshift (default: "52.26.9.208:5439") [$RAINCATCHER_DB_ADDRESS]
   --database value, --db value   The name of the redshift database (default: "dev") [$RAINCATCHER_DB_DATABASE]
   --help, -h                     show help
   --version, -v                  print the version
```
