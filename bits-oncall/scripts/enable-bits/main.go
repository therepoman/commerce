package main

import (
	"bufio"
	"fmt"
	"os"
	"os/user"
	"path/filepath"
	"time"

	"strings"

	"code.justin.tv/commerce/payday/config"
	"code.justin.tv/commerce/payday/dynamo"
	"code.justin.tv/commerce/payday/utils/pointers"
	"github.com/urfave/cli"
	"github.com/go-pg/pg"
)

const staging = "staging"

func main() {
	app := cli.NewApp()

	gopath := os.Getenv("GOPATH")
	paydayDir := fmt.Sprintf("%s/src/code.justin.tv/commerce/payday", gopath)

	app.Flags = []cli.Flag{

		cli.StringFlag{
			Name:  "dir, d",
			Usage: "Your payday directory",
			Value: paydayDir,
		},

		cli.StringFlag{
			Name:  "environment, e",
			Usage: "The payday environment to emulate",
			Value: staging,
		},
		cli.BoolFlag{
			Name:  "payouts-disabled",
			Usage: "Marks all the channels as not payable",
		},
		cli.StringFlag{
			Name:   "password",
			Usage:  "The password used to connect to redshift",
			EnvVar: "RAINCATCHER_DB_PASSWORD",
		},
		cli.StringFlag{
			Name:   "user",
			Usage:  "The user used to connect to redshift",
			EnvVar: "RAINCATCHER_DB_USER",
		},
		cli.StringFlag{
			Name:   "addr",
			Usage:  "The ip address and port used to connect to redshift",
			Value:  "52.26.9.208:5439",
			EnvVar: "RAINCATCHER_DB_ADDRESS",
		},
		cli.StringFlag{
			Name:   "database, db",
			Usage:  "The name of the redshift database",
			Value:  "dev",
			EnvVar: "RAINCATCHER_DB_DATABASE",
		},
	}

	app.Version = "v0.0.1"
	app.Action = action
	app.Run(os.Args)
}

type TwitchUsers struct {
	tableName          struct{} `sql:"twitchusers"`
	TwitchUserID       string   `sql:"twitchuserid"`
	EligibleForPayouts bool     `sql:"eligibleforpayouts,notnull"`
}

func action(ctx *cli.Context) {
	argument := ctx.Args().Get(0)
	absolutePath, err := filepath.Abs(argument)
	if err != nil {
		panic(err)
	}

	f, err := os.Open(absolutePath)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	paydayDir := ctx.String("dir")
	payday, err := os.Stat(paydayDir)
	if err != nil {
		panic(err)
	}

	payoutsDisabled := ctx.Bool("payouts-disabled")
	password := ctx.String("password")
	redshiftUser := ctx.String("user")
	redshiftInstance := ctx.String("addr")
	redshiftDBName := ctx.String("database")

	if payoutsDisabled && redshiftUser == "" {
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Enter redshift user: ")
		text, _ := reader.ReadString('\n')
		redshiftUser = strings.TrimSpace(text)
	}

	if payoutsDisabled && password == "" {
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Enter redshift password: ")
		text, _ := reader.ReadString('\n')
		password = strings.TrimSpace(text)
	}

	if !payday.IsDir() {
		panic(fmt.Sprintf("The specified directory %s is not a directory", paydayDir))
	}

	fmt.Printf("Changing working directory to %s\n", paydayDir)
	err = os.Chdir(paydayDir)
	if err != nil {
		panic(err)
	}

	environment := ctx.String("e")
	err = config.Load(environment)
	if err != nil {
		panic(err)
	}

	dynamoClient := dynamo.NewClient(&dynamo.DynamoClientConfig{
		AwsRegion:   config.Get().DynamoRegion,
		TablePrefix: config.Get().DynamoTablePrefix,
	})

	channelDao := dynamo.NewChannelDao(dynamoClient)

	scanner := bufio.NewScanner(f)
	now := time.Now()
	u, err := user.Current()
	if err != nil {
		panic(err)
	}
	annotation := fmt.Sprintf("Enabled by %s on %s", u.Username, now.Format(time.RFC822))

	var db *pg.DB
	if payoutsDisabled {
		db = pg.Connect(&pg.Options{
			User:     redshiftUser,
			Password: password,
			Addr:     redshiftInstance,
			Database: redshiftDBName,
		})
	}

	for scanner.Scan() {
		if err := scanner.Err(); err != nil {
			panic(err)
		}

		channelID := scanner.Text()

		fmt.Printf("Enabling channel %s\n", channelID)
		channel := dynamo.Channel{
			Id:         dynamo.ChannelId(channelID),
			Annotation: pointers.StringP(annotation),
			Onboarded:  pointers.BoolP(true),
		}

		err := channelDao.Update(&channel)
		if err != nil {
			panic(err)
		}

		if payoutsDisabled {
			newUser := TwitchUsers{
				TwitchUserID:       channelID,
				EligibleForPayouts: false,
			}

			err := db.Insert(&newUser)
			if err != nil {
				panic(err)
			}
		}
	}

	fmt.Println("Succesfully enabled all users")
}
