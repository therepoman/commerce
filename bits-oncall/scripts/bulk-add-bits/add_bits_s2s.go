/*
 *      Before running this script, make sure:
 *      - Your AWS secrets are stored in a file on your computer under $home/.aws so that it looks like:
 *        (file: $home/.aws/credentials) (<- this is not a part of the file content)
 *        aws_access_key_id = {your_access_key_id}
 *        aws_secret_access_key = {your_secret_access_key}
 *      - Your AWS ARN is registered under "Allowed ARNs" for payday-production
 *
 *      Each entry on the input csv file should look like:
 *      {User ID#},{Reason},{Number of Bits to give}
 *  ex) 192991162,Reimburse Bits,500
 *      205613736,This is a test,28
 *      Note that there is NO space around commas.
 *
 *      To run the script, firstly build it and then run it with some arguments.
 *      Arg #1 (required): Input file name
 *      Arg #2 (required): Your Twitch LDAP username (later I'd love to remove the necessity to provide this)
 *      Arg #3 (optional): "prod" for production environment. Default to dev. Anything other than "prod" results in dev operation.
 *  ex) 1) go build script.go
 *      2) ./script input.csv hiranoo [prod]
 */

package main

import (
	"bytes"
	"context"
	"encoding/csv"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"strconv"

	"code.justin.tv/commerce/logrus"
	payday "code.justin.tv/commerce/payday/client"
	"code.justin.tv/commerce/payday/models/api"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/gofrs/uuid"
	"golang.org/x/net/proxy"
)

const (
	CALLER_PROD = "payday-production"
	CALLER_DEV  = "payday-staging"

	PAYDAY_PROD = "https://prod.payday.twitch.a2z.com"
	PAYDAY_DEV  = "https://main.us-west-2.beta.payday.twitch.a2z.com"
)

func errorCheck(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	var callerName = CALLER_DEV
	var endpoint = PAYDAY_DEV
	var affectedUsers = 0
	logger := logrus.New()
	if len(os.Args) < 3 {
		logger.Error("Not enough arguments.")
		return
	}
	if len(os.Args) > 3 {
		if os.Args[3] == "prod" {
			callerName = CALLER_PROD
			endpoint = PAYDAY_PROD
		}
	}

	dialer, err := proxy.SOCKS5("tcp", "127.0.0.1:8000", nil, proxy.Direct)
	errorCheck(err)

	t, err := caller.NewWithCustomRoundTripper(callerName, &caller.Config{
		AWSConfigBase: &aws.Config{
			Region: aws.String("us-west-2"),
		},
	}, &http.Transport{
		DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
			return dialer.Dial(network, addr)
		},
	},
		logger)
	errorCheck(err)

	client, err := payday.NewClient(twitchclient.ClientConf{
		Host:          endpoint,
		BaseTransport: t,
	})
	errorCheck(err)

	file, err := ioutil.ReadFile(os.Args[1])
	errorCheck(err)

	r := csv.NewReader(bytes.NewReader(file))
	input, err := r.ReadAll()
	errorCheck(err)

	for _, record := range input {
		transactionId := uuid.Must(uuid.NewV4())
		eachUser := &api.AddBitsRequest{}
		eachUser.TypeOfBitsToAdd = "CB_0"
		eachUser.AdminUserId = os.Args[2]
		eachUser.EventId = transactionId.String()
		for i, field := range record {
			switch i {
			case 0:
				eachUser.TwitchUserId = field
			case 1:
				eachUser.AdminReason = field
			case 2:
				convertedAmount, _ := strconv.Atoi(field)
				eachUser.AmountOfBitsToAdd = int32(convertedAmount)
			default:
				logger.Error("Incorrect number of fields in an entry found.")
				return
			}
		}
		logrus.WithFields(logrus.Fields{
			"userID": eachUser.TwitchUserId,
			"amount": eachUser.AmountOfBitsToAdd,
		}).Info("processing user admin add request...")

		err := client.AddBits(context.Background(), eachUser, &twitchclient.ReqOpts{})
		if err != nil {
			logger.WithError(err).Errorf("Failed to add bits for user #: %s", eachUser.TwitchUserId)
			logger.Printf("# of affected users: %d", affectedUsers)
			return
		}
		affectedUsers++
		logger.Printf("Successfully added bits for user #: %s", eachUser.TwitchUserId)
	}
	logger.Printf("Completed. # of affected users: %d", affectedUsers)
}
