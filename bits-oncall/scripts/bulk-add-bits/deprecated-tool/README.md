1. make sure you have python installed (default on macs)
2. install dependencies `pip install -r requirements.txt`
3. copy usernames to entitle to a csv into this directory. You can name it whatever you want, but if you don't want to specify a name in the script name this file `twitch_rpg_fantasy_usernames`
	File needs to be a list of names with no trailing commas

4. run the `add_bits_twitch_rpg` script, and replace the `{}` with the correct value:
```
python add_bits_twitch_rpg.py -e production -a { your user ID } -j { JIRA ticket ID of the request } -s { The name of the survey in the Google sheet } -b { Number of bits to entitle in the survey } --lookup-userids
```
If you need help with the parameters, or a complete list of parameters, run `python add_bits_twitch_rpg.py -h`
