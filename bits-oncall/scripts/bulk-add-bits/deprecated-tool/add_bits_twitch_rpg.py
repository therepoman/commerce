#!/usr/bin/python

import json
import requests
import argparse
import sys
import uuid
from argparse import ArgumentParser
from twitch_rpg_generate import lookup_twitch_usernames_from_ids

def check_args(args):
    if args.admin_user is None:
        return False, 'Admin user cannot be empty. Use the flag -a or --admin-user'
    elif args.jira_ticket_id is None:
        return False, 'JIRA ticket ID cannot be empty. Use the flag -j or --jira-ticket-id'
    elif args.survey_name is None:
        return False, 'Survey name cannot be empty. Use the flag -s or --survey-name'
    elif args.bits is None:
        return False, 'Amount of bits used cannot be empty. Use the flag -b or --bits'
    return True, None

def main():
    parser = ArgumentParser(description='Process RPG requests with ease')
    parser.add_argument('-e', '--env', default='dev', help='Stage to run. Defaults to dev.')
    parser.add_argument('-a', '--admin-user', help='Admin user ID to run the script.')
    parser.add_argument('-j', '--jira-ticket-id', help='JIRA ticket ID of the RPG request.')
    parser.add_argument('-s', '--survey-name', help='RPG Survey name of the request.')
    parser.add_argument('-b', '--bits', type=int, help='RPG Survey bits to entitle.')
    parser.add_argument('-u', '--usernames', default='twitch_rpg_fantasy_usernames', help='Filename for list of usernames to entitle bits.')
    parser.add_argument('-o', '--output-userids', default='twitch_rpg_fantasy_userids', help='Filename for output list of userids to entitle bits.')
    parser.add_argument('-l', '--lookup-userids', action='store_true', help='Lookup the user IDs automatically.')

    args = parser.parse_args()
    valid_args, error_msg = check_args(args)
    if not valid_args:
        print 'Error checking arguments: {}'.format(error_msg)
        return 1

    user_check = raw_input('\nYou are running this script to add {} bits to your list of users. Continue? [y/n] '.format(str(args.bits)))
    if str(user_check).lower() == 'y':
        print
        user_ids = []
        users_not_found = []

        if args.lookup_userids:
            usernames = 'twitch_rpg_fantasy_usernames'
            if args.usernames:
                usernames = args.usernames
            output_ids = 'twitch_rpg_fantasy_userids'
            if args.output_userids:
                output_ids = args.output_userids
            user_ids, users_not_found = lookup_twitch_usernames_from_ids(usernames, output_ids)
        else:
            with open(args.output_userids, 'r') as f:
                for line in f:
                    user_ids.append(line.split()[0])

        url = "https://main.us-west-2.beta.payday.twitch.a2z.com/admin/addBits" #staging
        if args.env == 'production':
            print "Adding bits to users in Prod"
            url = "https://prod.payday.twitch.a2z.com/admin/addBits" #prod
        else:
            print "Adding bits to users in Staging"

        print "Number of user IDs found: {}".format(len(user_ids))
        count = 0
        user_count = len(user_ids)

        for user_id in user_ids:
            payload = { "twitchUserId": user_id,
                      "eventId": str(uuid.uuid4()),
                      "adminUserId": args.admin_user,
                      "typeOfBitsToAdd": 'CB_0',
                      "adminReason": 'https://twitchtv.atlassian.net/browse/' + args.jira_ticket_id + ' - ' + args.survey_name,
                      "amountOfBitsToAdd": args.bits}

            headers = {"Content-type": "application/json"}
            response = requests.post(url.format(user_id), headers=headers, json=payload)

            if response.status_code != 200:
                print "ERROR adding bits for {}".format(user_id)
                print "{}: {}".format(response.status_code, response.json())

            count += 1
            print "{}/{}: {}".format(count, user_count, user_id)

        if len(users_not_found) > 0:
            print "\nThe following users were not found"
            for user in users_not_found:
                print user
            print

        return 0
    else:
        print 'Aborting script, exit normally.'
        return 0

if __name__ == '__main__':
    main()
