import json
import requests
from collections import OrderedDict

def lookup_twitch_usernames_from_ids(usernames_file='twitch_rpg_fantasy_usernames', userids_file='twitch_rpg_fantasy_userids'):
    print "Looking up User IDS from list provided here: {}".format(usernames_file)
    url = "https://api.twitch.tv/kraken/channels/{}?client_id=1twsdagrrv4t3r26bwm2bje9ab8koq1"

    def file_len(fname):
        with open(fname) as f:
            for i, l in enumerate(f):
                pass
        return i + 1

    user_count = file_len(usernames_file)
    print user_count
    count = 0

    lookup = {}
    usernames_not_found = []
    count = 1
    with open(usernames_file, 'r') as f:
      for line in f:
        user_name = line.split()[0]
        response = requests.get(url.format(user_name))
        if '_id' in json.loads(response.text):
          user_id = json.loads(response.text)['_id']
          if user_id > 0:
            user_name = user_name.lower()
            lookup[user_name] = user_id
          print "{} / {}".format(count, user_count)
          count += 1
          continue

        print "{} not found".format(user_name)
        usernames_not_found.append(user_name)

    ordered_lookup = OrderedDict(sorted(lookup.items(), key=lambda t: t[0]))
    ordered_ids = []

    print "Outputting user IDs found here: {}".format(userids_file)
    with open(userids_file, 'w') as f:
      for k,v in ordered_lookup.iteritems():
        f.write('{}\n'.format(v))
        ordered_ids.append(str(v))

    return ordered_ids, usernames_not_found

if __name__ == '__main__':
    lookup_twitch_usernames_from_ids()
