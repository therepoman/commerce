# Creating test users in bulk

1. Run an ssh tunnel command so that user service prod is accessible from your laptop (ssh -L 9999:users-service.prod.us-west2.twitch.tv:80 <payday_staging_box>)
2. Configure the files consts as necessary
3. Run the script through intellij or the cli
