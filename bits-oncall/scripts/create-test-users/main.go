package main

import (
	"bufio"
	"context"
	"encoding/csv"
	"fmt"
	"os"
	"strings"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/web/users-service/client/usersclient_internal"
	"code.justin.tv/web/users-service/models"
	usersModels "code.justin.tv/web/users-service/models"
)

const (
	// Tunnel to user service prod through staging payday
	// ssh -L 9999:users-service.prod.us-west2.twitch.tv:80 <payday_staging_box>
	defaultUserServiceEndpoint = "http://localhost:9999"

	defaultUserPrefix = "qa_test_bits_user_"

	defaultFirstUserNum = 10002

	defaultNumUsers = 1000

	defaultFilePath = "/Users/seph/test_users.csv"

	delay = 2 * time.Second
)

func main() {
	// TODO: Add support for passing these in as CLI args

	userServiceEndpoint := defaultUserServiceEndpoint
	userPrefix := defaultUserPrefix
	firstUserNum := defaultFirstUserNum
	numUsers := defaultNumUsers
	filePath := defaultFilePath

	confirm(fmt.Sprintf("User service is being called through: %s", userServiceEndpoint))
	confirm(fmt.Sprintf("%d users will be generated", numUsers))
	confirm(fmt.Sprintf("First user: %s", fmt.Sprintf("%s%d", userPrefix, firstUserNum)))
	confirm(fmt.Sprintf("Resulting csv will be written to: %s", filePath))
	confirm("User generation will begin now")

	conf := twitchclient.ClientConf{
		Host: userServiceEndpoint,
	}
	userClient, err := usersclient_internal.NewClient(conf)
	if err != nil {
		panic(err)
	}

	usersFile, err := os.Create(filePath)
	if err != nil {
		panic(err)
	}

	usersCSVWriter := csv.NewWriter(usersFile)
	defer usersCSVWriter.Flush()

	for i := firstUserNum; i < firstUserNum+numUsers; i++ {
		time.Sleep(delay)

		login := fmt.Sprintf("%s%d", userPrefix, i)

		resp, err := userClient.CreateUser(context.Background(), &models.CreateUserProperties{
			Login: login,
			Email: fmt.Sprintf("%s@twitch.tv", login),
			Birthday: usersModels.Birthday{
				Day:   20,
				Month: time.April,
				Year:  1969,
			},
		}, nil)
		if err != nil {
			log.WithError(err).Errorf("Error calling user service create user: %s", login)
			continue
		}
		if resp == nil {
			log.Errorf("User service returned nil response: %s", login)
			continue
		}

		err = usersCSVWriter.Write([]string{*resp.Login, resp.ID})
		if err != nil {
			panic(err)
		}

		log.Infof("Created User: %s (%s)", login, resp.ID)
	}
}

func confirm(statement string) {
	log.Infoln(statement)
	reader := bufio.NewReader(os.Stdin)

	log.Infoln("Are you sure? (y/n)")
	text, err := reader.ReadString('\n')
	if err != nil {
		panic(err)
	}

	text = strings.TrimSpace(text)
	if !strings.EqualFold(text, "Y") {
		panic("you must be sure to proceed")
	}
}
