/*
 *      Before running this script, make sure:
 *      - Your AWS secrets are stored in a file on your computer under $home/.aws so that it looks like:
 *        (file: $home/.aws/credentials) (<- this is not a part of the file content)
 *        aws_access_key_id = {your_access_key_id}
 *        aws_secret_access_key = {your_secret_access_key}
 *      - Your AWS ARN is registered under "Allowed ARNs" for payday-production
 *
 *
 *      Each entry on the input csv file should look like:
 *      {User ID#},{Platform},{SKU},{Transaction ID#}
 *  ex) 7168163,ios,tv.twitch.ios.iap.bits.everyday.t20,260000433725863
 *      24182350,android,tv.twitch.android.iap.bits.everyday.t50,GPA.3367-6631-6009-88707
 *      Note that there is NO space around commas.
 *
 *      Reference payment postgresql query.
 *        SELECT po.user_id, po.platform, li.product_id, transaction_id
 *        FROM purchase_orders po INNER JOIN purchase_order_line_items li ON po.id = li.purchase_order_id
 *        WHERE po.state = 'failed' AND po.platform IN ('android', 'ios') AND po.created_at > '2018-05-11 21:50:00'
 *        GROUP BY po.user_id, po.platform, li.product_id;
 *
 *      To run the script, firstly build it and then run it with some arguments.
 *      Arg #1 (required): Input file name
 *      Arg #2 (optional): "prod" for production environment. Default to dev. Anything other than "prod" results in dev operation.
 *  ex) 1) go build script.go
 *      2) ./script input.csv [prod]
 */

package main

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"code.justin.tv/sse/malachai/pkg/s2s"
	"code.justin.tv/commerce/logrus"
)

const (
	CALLER_PROD = "payday-production"
	CALLER_DEV  = "payday-staging"

	PAYDAY_PROD = "https://prod.payday.twitch.a2z.com"
	PAYDAY_DEV  = "https://main.us-west-2.beta.payday.twitch.a2z.com"

	ENTITLE_PATH = "/authed/entitleBits"
)

type EntitleBitsInput struct {
	TwitchUserId  string `json:"user_id"`        // Required
	TransactionId string `json:"transaction_id"` // Required
	ProductId     string `json:"product_id"`     // Required
	Platform      string `json:"platform"`       // Required
}

func errorCheck(err error) {
	if err != nil {
		panic(err)
	}
}

func main(){
	var callerName = CALLER_DEV
	var endpoint = PAYDAY_DEV + ENTITLE_PATH
	var affectedUsers = 0
	logger := logrus.New()
	if len(os.Args) < 2 {
		logger.Error("Not enough arguments.")
		return
	}
	if len(os.Args) > 2 {
		if os.Args[2] == "prod" {
			callerName = CALLER_PROD
			endpoint = PAYDAY_PROD + ENTITLE_PATH
		}
	}
	cfg := &s2s.CallerConfig{
		CallerName: callerName,
	}

	t, err := s2s.NewRoundTripper(cfg, logger)
	errorCheck(err)

	client := http.Client{
		Transport: t,
	}

	file, err := ioutil.ReadFile(os.Args[1])
	errorCheck(err)
	logger.Printf("Successfully loaded file %s", os.Args[1])

	r := csv.NewReader(bytes.NewReader(file))
	input, err := r.ReadAll()
	errorCheck(err)

	logger.Printf("Running against %s", endpoint)

	for _, record := range input {
		eachUser := &EntitleBitsInput{}
		for i, field := range record {
			switch i {
			case 0:
				eachUser.TwitchUserId = field
			case 1:
				eachUser.Platform = field
			case 2:
				eachUser.ProductId = field
			case 3:
				eachUser.TransactionId = field
			default:
				logger.Error("Incorrect number of fields in an entry found.")
				return
			}
		}
		jsonData, err := json.Marshal(eachUser)
		if err != nil {
			logger.Error("Failed to convert request to json.")
			return
		}

		req, err := http.NewRequest("POST", endpoint, bytes.NewReader(jsonData))
		req.Header.Set("Content-Type", "application/json")
		res, err := client.Do(req)
		if err != nil || res.StatusCode != http.StatusOK {
			logger.Errorf("Failed to entitle bits for user #: %s", eachUser.TwitchUserId)
			if err != nil {
				logger.Error(err)
				return
			}
			logger.Printf("Error Code: %d", res.StatusCode)
			body, _ := ioutil.ReadAll(res.Body)
			logger.Print(string(body))
			logger.Printf("# of affected users: %d", affectedUsers)
			return
		}
		defer res.Body.Close()
		affectedUsers++
		logger.Printf("Successfully entitled bits for user #: %s", eachUser.TwitchUserId)
	}
	logger.Printf("Completed. # of affected users: %d", affectedUsers)
}
