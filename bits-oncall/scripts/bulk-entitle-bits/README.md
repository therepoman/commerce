## Bulk Entitle Bits Tool
A way to entitle bits to many users based on the entitle api (so that SKU's and idempotency can be maintained), 
this is primarily used to replay mobile bits entitlements which may have failed (without having to worry if they did).

>*As of January 2018, we use the jobs tool in active admin for adding Bits to many users accounts all at once.*

>*As a backup, there is a copy of the deprecated python script that we used to use as the primary tool located in `./deprecated_tool`*

The new tool is provided assuming you have access to the JTV network, access to Admin-Panel, and you have the ok of the Bits team to run a job.

1. Connect to the JTV network
2. Go to https://admin-panel.internal.twitch.tv/bits/jobs
3. Sign in via Single Sign On (and Two Factor Auth)
4. You'll be able to see past jobs that you have run as well as the UI for starting new jobs
5. Fill out the number of Bits to give each user, the annotation (a combination of JIRA ticket number, a hyphen `-`, and the title of the campaign), and the list of usernames to grant Bits to. *Note: if there are repeated usernames in the list, each unique username will only be granted the Bits once.*
6. Click start job and click confirm on the dialogue that pops up
7. If there's any errors with starting the job, a red banner at the top of the screen will appear with the corresponding error. Otherwise, a purple success banner will appear at the top of the screen alerting you to the fact that the job has been started.
8. Once the job has been started, it will finish asynchronously, even if you close out of the window. Refreshing the window will allow you to see the job progress as it goes along.
9. When the job has finished, any errors (e.g. from being unable to find the Twitch user id of the corresponding username) will be put into a zip file in the errors column.

