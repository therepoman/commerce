package main

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"io/ioutil"
	"os"
	"strconv"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/satori/go.uuid"
	"code.justin.tv/commerce/logrus"
	"io"
)

const (
	REGION             = "us-west-2"
	BITS_USAGE_STAGING = "arn:aws:sns:us-west-2:021561903526:bits-usage-staging"
	BITS_USAGE_PROD    = "arn:aws:sns:us-west-2:021561903526:bits-usage-prod"
)

type BitsUsageSNS struct {
	ChannelID   string           `json:"channel_id"`
	UserID      string           `json:"user_id"`
	EventID     string           `json:"event_id"`
	Message     string           `json:"message"`
	Amount      int32            `json:"amount"`
	EmoteTotals map[string]int64 `json:"emote_totals"`
}

func main() {
	snsClient := sns.New(session.New(), &aws.Config{Region: aws.String(REGION)})
	logger := logrus.New()
	snsTopic := BITS_USAGE_STAGING
	affectedUsers := 0
	if len(os.Args) < 2 {
		logger.Error("Specify a filename")
		return
	}
	if len(os.Args) > 2 {
		if os.Args[2] == "prod" {
			snsTopic = BITS_USAGE_PROD
		}
	}
	file, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		logger.Error("Failed to read the file")
		return
	}

	r := csv.NewReader(bytes.NewReader(file))

	for  {
		event, err := r.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			logger.Error("Failed to read line")
			logger.Printf("Affected # of users: %d", affectedUsers)
			return
		}
		transactionId := uuid.NewV4()
		var bitsEvent = BitsUsageSNS{}
		bitsEvent.EventID = transactionId.String()
		bitsEvent.UserID = event[0]
		bitsEvent.ChannelID = event[1]
		value, _ := strconv.Atoi(event[2])
		bitsEvent.Amount = int32(value)

		bitsEvent.EmoteTotals = map[string]int64{
			"cheer": int64(bitsEvent.Amount),
		}
		bitsEvent.Message = "backfill:" + bitsEvent.EventID

		snsJSON, err := json.Marshal(bitsEvent)
		if err != nil {
			logger.Error("Failed to marshal BitsEvent")
			logger.Printf("Affected # of users: %d", affectedUsers)
			return
		}

		pi := &sns.PublishInput{
			Message:  aws.String(string(snsJSON)),
			TopicArn: aws.String(snsTopic),
		}
		_, err = snsClient.Publish(pi)
		if err != nil {
			logger.Error("Failed to publish SNS message for user id: %s", bitsEvent.UserID)
			logger.Printf("Affected # of users: %d", affectedUsers)
			return
		}

		affectedUsers++
		logger.Printf("Successfully published BitsEvent for user id: %s", bitsEvent.UserID)
	}
	logger.Printf("Affected # of users: %d", affectedUsers)
}
