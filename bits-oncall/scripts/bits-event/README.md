## Publish BitsEvent SNS message to the SQS queue
This script(`fire_bits_event.go`) only cares about posting BitsEvent sns message for Fortuna to process.

Follow the instruction below to run it:

1. `cd ./bits_event`
2. `go build fire_bits_event.go`
3. `./fire_bits_event input.csv [prod/staging]`

* Arg #1 (required): The path to the input file name. The example above is when you have the file in this directory
* Arg #2 (optional): "staging" or "prod". Default to "staging" if not provided

The file should be a csv with contents that should follow the following format:
> {cheerer id},{channel id},{cheer amount}

Example:
```
100000219,7010591,2
100006513,90789230,1050
```

** Note that there is no space around comma

** The other script, `fix_wc_outage.go`, is a fork of the script that is specific to WC outage