/*
	This script is meant only for fixing the WC outage issue. Basically the same usage as the other script
    in this directory (fire_bits_event.go) except the format of the input file. The input file should only
	contain the list of user IDs like
	111111111
	222222222
	333333333
	in just one column.
	Usage:
	`go build fix_wc_outage.go`
	`./fix_wc_outage path_to_filename`
*/

package main

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"io/ioutil"
	"math/rand"
	"os"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/satori/go.uuid"
	"code.justin.tv/commerce/logrus"
	"io"
)

const (
	QA_BITS_PARTNER    = "116076154"
	REGION             = "us-west-2"
	BITS_USAGE_STAGING = "arn:aws:sns:us-west-2:021561903526:bits-usage-staging"
	BITS_USAGE_PROD    = "arn:aws:sns:us-west-2:021561903526:bits-usage-prod"
)

type BitsUsageSNS struct {
	ChannelID   string           `json:"channel_id"`
	UserID      string           `json:"user_id"`
	EventID     string           `json:"event_id"`
	Message     string           `json:"message"`
	Amount      int32            `json:"amount"`
	EmoteTotals map[string]int64 `json:"emote_totals"`
}

func main() {
	snsClient := sns.New(session.New(), &aws.Config{Region: aws.String(REGION)})
	logger := logrus.New()
	snsTopic := BITS_USAGE_STAGING
	affectedUsers := 0
	if len(os.Args) < 2 {
		logger.Error("Specify a filename")
		return
	}
	if len(os.Args) > 2 {
		if os.Args[2] == "prod" {
			snsTopic = BITS_USAGE_PROD
		}
	}
	file, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		logger.Error("Failed to read the file")
		return
	}

	r := csv.NewReader(bytes.NewReader(file))

	for {
		event, err := r.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			logger.Error("Failed to read line")
			logger.Printf("Affected # of users: %d", affectedUsers)
			return
		}
		var bitsEvent = BitsUsageSNS{}
		bitsEvent.UserID = event[0]
		bitsEvent.ChannelID = QA_BITS_PARTNER
		bitsEvent.Amount = 1 //This amount will make sure no BitsEvent from this script would trigger a cheerbomb

		bitsEvent.EmoteTotals = map[string]int64{
			"backfill": int64(bitsEvent.Amount),
		}

		// "Cheer" three times to give each user all tier 1 emotes as per PM's request
		for i := 0; i < 3; i++ {
			rand.Seed(time.Now().UnixNano())
			transactionId := uuid.NewV4()
			bitsEvent.EventID = transactionId.String()
			bitsEvent.Message = "cheer1 backfill:" + bitsEvent.EventID

			snsJSON, err := json.Marshal(bitsEvent)
			if err != nil {
				logger.Error("Failed to marshal BitsEvent")
				logger.Printf("Affected # of users: %d", affectedUsers)
				return
			}

			pi := &sns.PublishInput{
				Message:  aws.String(string(snsJSON)),
				TopicArn: aws.String(snsTopic),
			}

			_, err = snsClient.Publish(pi)
			if err != nil {
				logger.Error("Failed to publish SNS message for user id: %s", bitsEvent.UserID)
				logger.Printf("Affected # of users: %d", affectedUsers)
				return
			}
		}

		affectedUsers++
		logger.Printf("Successfully published BitsEvent three times for user id: %s", bitsEvent.UserID)
	}
	logger.Printf("Affected # of users: %d", affectedUsers)
}
