/**
 * This script goes through the published-events-new-processing-queue-dlq-prod DLQ for Pantheon, looks up each message
 * against DynamoDB and verify against the value field from Dynamo. If the value does not match, the message is deleted form
 * the DLQ.
 */

package main

import (
	"encoding/json"
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/sqs"
)

const (
	REGION = "us-west-2"

	PANTHEON_EVENTS_DLQ_URL = "https://sqs.us-west-2.amazonaws.com/375121467453/published-events-new-processing-queue-dlq-prod"
	EVENT_TABLE             = "events-prod"
)

type EventMessage struct {
	Domain      string `json:"domain"`
	ID          string `json:"id"`
	TimeOfEvent int64  `json:"time_of_event"`
	GroupingKey string `json:"grouping_key"`
	EntryKey    string `json:"entry_key"`
	EventValue  int64  `json:"event_value"`
}

type Event struct {
	LeaderboardID string `dynamodbav:"leaderboard_id"`
	EventID       string `dynamodbav:"event_id"`
	Value         int64  `dynamodbav:"value"`
	Timestamp     int64  `dynamodbav:"timestamp"`
}

func main() {
	sqsClient := sqs.New(session.New(), &aws.Config{Region: aws.String(REGION)})
	qURL := PANTHEON_EVENTS_DLQ_URL

	dynamoClient := dynamodb.New(session.New(), &aws.Config{Region: aws.String(REGION)})

	for {
		result, err := sqsClient.ReceiveMessage(&sqs.ReceiveMessageInput{
			AttributeNames: []*string{
				aws.String(sqs.MessageSystemAttributeNameSentTimestamp),
			},
			MessageAttributeNames: []*string{
				aws.String(sqs.QueueAttributeNameAll),
			},
			QueueUrl:            &qURL,
			MaxNumberOfMessages: aws.Int64(10),
			VisibilityTimeout:   aws.Int64(20), // 20 seconds
			WaitTimeSeconds:     aws.Int64(0),
		})

		if err != nil {
			fmt.Println(err)
			return
		}

		if len(result.Messages) == 0 {
			fmt.Println("No more messages on the queue")
			return
		}

		for _, message := range result.Messages {
			var eventMessage EventMessage
			json.Unmarshal([]byte(*message.Body), &eventMessage)

			leaderboardID := fmt.Sprintf("%s/%s/ALLTIME/0", eventMessage.Domain, eventMessage.GroupingKey)
			eventID := eventMessage.ID

			result, err := dynamoClient.GetItem(&dynamodb.GetItemInput{
				TableName: aws.String(EVENT_TABLE),
				Key: map[string]*dynamodb.AttributeValue{
					"leaderboard_id": {
						S: aws.String(leaderboardID),
					},
					"event_id": {
						S: aws.String(eventID),
					},
				},
			})
			if err != nil {
				fmt.Println(err)
				return
			}

			event := Event{}
			err = dynamodbattribute.UnmarshalMap(result.Item, &event)
			if err != nil {
				fmt.Println(err)
				return
			}

			// Check if values match between the message and the Dynamo entry, delete the message from the DLQ if it does not.
			if eventMessage.ID == event.EventID && eventMessage.EventValue != event.Value {
				fmt.Printf("values don't match for message: %s == %s && %d != %d\ndeleting this message...\n", event.EventID, eventMessage.ID, event.Value, eventMessage.EventValue)
				sqsClient.DeleteMessage(&sqs.DeleteMessageInput{
					ReceiptHandle: message.ReceiptHandle,
					QueueUrl:      &qURL,
				})
			}
		}
	}
}
