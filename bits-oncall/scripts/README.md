
## Table of Contents

* [bits-event](bits-event) - This script fires a bits-event to replay integrations like fortuna.
* [bulk-add-bits](bulk-add-bits) - Used to entitle a number of users to different bits amounts (use admin tool if they're all the same), requires S2S.
* [bulk-entitle-bits](bulk-entitle-bits) - Used to replay entitle bits events for PayPal and mobile IAP purchases, requires S2S.
* [enable-bits](enable-bits) - Script used to enable a number of broadcasters to bits.  
* [ntp-update](ntp-update) - NTP update script, used when servers get out of sync with Twitch NTP servers.