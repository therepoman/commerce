### Generating Bits Product Keys

#### Setting up a New Bit Type for Keys
Each bits code redemption promotion / grouping should ideally use a different bit type. This lets us track bit usage of these bits independently from other bit-types / purchases etc. These used to be stored in FABS, but now the canonical data for Bits Types/SKUs is stored in Petozi.

It is important to firstly make the Bits Type and then secondly make the Bits Product.

To create the new bit types:
1. Go to https://admin-panel.internal.justin.tv/bits/types/new
1. Fill out the form for a new bits type
    * You can look at how other bits types are set up at https://admin-panel.internal.justin.tv/bits/types
    * `Is Paid` and `Is Promotional` should both be false.
    * By convention we prefix key bit-types with `phanto.`
    * `EntitlementSourceType` should be set to `REDEEMED_CODE`

You also need to create a new product. This is done by:
1. Go to https://admin-panel.internal.justin.tv/bits/products/new
1. Fill out the form for a new bits product
    * You can look at how other bits types are set up at https://admin-panel.internal.justin.tv/bits/types   
    * The `Platform` should be `PHANTO`. 
    * If you try to do this part first, your Bits Type won't be populated in the Bits Type ID dropdown.
    * `Quantity` should be equal to the number of Bits included per redeemed code.
    * `Max Purchasable Quantity` should be `1`
    * `Pricing ID` should be `NO_PRICING_ID`
    * `Include a promotion` should be unchecked

#### Configuring the new Bit Type for Redshift
1. Login to Raincatcher Admin
    * if you cannot connect to Raincatcher Redshift DB, please ask another teammate to help you get access (pending doc on how to gain access to Redshift)
2. Run the following command:
```sql
INSERT INTO bitaccounttypes (bitaccounttype, totalpurchaseprice, costbasisperbit) VALUES ('sku', 0, 0)
```
Where `sku` is the SKU you configured in the FABS Dynamo Table, and the `totalpurchaseprice` and `costbasisperbit` are normally both `0`, but confirm with the requester what the cost basis should be. 

#### Configuring a New Key Pool for the New Bit Type
1. Navigate to the keys section of admin-panel: [Quick link](https://admin-panel.internal.justin.tv/keys/product_types)
    * If you don’t have permissions, you need to file a Jira request like this: [Quick link](https://jira.twitch.com/browse/PERMS-171)
1. Configure a new bits product: [Quick link](https://admin-panel.internal.justin.tv/keys/products?product_type=bits)
    1. Make sure the SKU is the same as the bit-type you created in the FABS Dynamo Table.
    1. Make sure the description makes sense since customers will see it on the Redeem page.
1. Click Create.
1. Create a key pool: [Quick link](https://admin-panel.internal.justin.tv/keys/pools)
    1. The PoolID should be some unique name to represent this particular promotion / key-group.
    1. Product type should be `bits` 
    1. SKU should be the SKU you configured above.
    1. The Handler should be the Twitch UserID that has access to download these keys. If you will be giving these keys to somebody else, use your staff account’s TUID. 
    1. Start and end dates control when the keys can be redeemed - ask the requester what these values should be. If the keys should be valid forever, set the start date to yesterday and the end date to some day in the year 3000.
1. Click Create.

#### Generating Key Batches and Downloading the Keys
1. Once the pool is created, click the corresponding “Manage Batches” link from the table.
1. Enter the number of keys you wish to generate
1. click Create. 
1. Continue refreshing the page until the Workflow Status is `SUCCEEDED`. 
1. To download the keys navigate to `https://www.twitch.tv/download-keys/{batch-id}` where {batch-id} is substituted with the batch-id displayed in the table. You must be logged in with the Twitch User that corresponds to the Key Pool’s handler. A CSV will be automatically downloaded in your browser containing the decrypted keys.