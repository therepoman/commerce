This is a collection of services outside the bits team and all dashboard and runbooks that we have for them.

Pagerduty links get destroyed login

# Winnebago #

Winnebago is the service that provides the mobile bits for ads experience (powered by Upsight), calls Payday/authed/entitleBits.

* Dashboard: https://grafana.internal.justin.tv/dashboard/db/rewarded-video-winnebago?orgId=1
* Runbook: http://docs.ads.internal.twitch.tv/runbooks/winnebago.html
* Slack: #rewarded-video
* Oncall: https://twitchoncall.pagerduty.com/schedules#P8YK82I `Ads and Promotions/Ad Data Engineering`

# Payments Service #

Payments Service does a bunch of things:
* Receipt validation and entitle bits for iOS, Android and PayPal, calls Payday/authed/entitleBits.
* Provides the functionality behind the checkout experience for PayPal and Amazon Pay.

* Dashboard: https://grafana.internal.justin.tv/dashboard/db/payments-service?refresh=30s&orgId=1
* Rollbar: https://rollbar.com/Twitch/Payments/
* Rollbar Event for Failed Entitles: https://rollbar.com/Twitch/Payments/items/1322/?item_page=0&item_count=100&#instances
* Slack: #payments
* Oncall: https://twitchoncall.pagerduty.com/schedules#PVIFVWB `Payments`

# Flo #

Flo is the generic pricing service for payments, it's called from GQL and Payday to get bits prices.

* Dashboard: https://grafana.internal.justin.tv/d/TzcWOrKmz/flo?orgId=1
* Slack: #payments
* Oncall: https://twitchoncall.pagerduty.com/schedules#PVIFVWB `Payments`

# Everdeen #

Everdeen is the golang replacement for the entitlement and receipt validation components of Payments Service.

* Dashboard: https://grafana.internal.justin.tv/d/64Gmer5iz/everdeen?orgId=1
* Slack: #payments
* Oncall: https://twitchoncall.pagerduty.com/schedules#PVIFVWB `Payments`

# MoneyPenny #

MoneyPenny is the service that holds information about enrollment of broadcaster into monetization, we use it to determine a broadcater's partner or affiliate status. Planned be replaced by the Go service [Ripley](https://git.xarth.tv/revenue/ripley).

* Dashboard: https://grafana.internal.justin.tv/d/000000900/moneypenny?orgId=1
* Slack: #payments
* Oncall: https://twitchoncall.pagerduty.com/schedules#PVIFVWB `Payments`

# Mobile #

Mobile team does not have an oncall, your best call is to bring it up in #mobile on slack and in the case of an emergency reach out to their manager (Ravi Rani/@ravirani)

* Slack: #mobile (also #bits-dev-mobile) 

# TMI #

TMI is the chat service, it is used in cheering to post the cheer to chat after deducting from fabs. 

* Dashboard: https://grafana.internal.justin.tv/dashboard/db/tmi-health?refresh=10s&orgId=1
* Slack logging of failures [#bits-tmi-failures](https://twitch.slack.com/messages/C5B4V9Y2C/team/U71CG5WN8/)
* Slack: #chat-and-messaging
* Oncall: https://twitchoncall.pagerduty.com/schedules#PK8BM4Z `Communications - Primary`

# Zuma #

Zuma is the moderation service provided by the chat and messaging team

* Dashboard: https://grafana.internal.justin.tv/dashboard/db/zuma-health?refresh=30s&orgId=1
* Slack: #chat-and-messaging
* Oncall (same as TMI): https://twitchoncall.pagerduty.com/schedules#PK8BM4Z `Communications - Primary`

# Subscriptions #

Subscriptions is called to generate emote prefixes (used in cheermotes).

* Dashboard: https://grafana.internal.justin.tv/dashboard/db/subscriptions?refresh=30s&orgId=1
* Slack: #subs
* Oncall: https://twitchoncall.pagerduty.com/schedules#PVZB493 `Commerce - Subs`

# Visage #

Visage is the api gateway layer for all twitch services. Visage has a change freeze every week between Friday 3:30 PM Pacific to Monday morning.

* Dashboard: https://grafana.internal.justin.tv/dashboard/db/visage?refresh=15m&orgId=1 (there are many more dashboards on Grafana, they all start with Visage)
* Rollbar: https://rollbar.com/Twitch/Visage/
* Slack: #api-infra
* Oncall: https://twitchoncall.pagerduty.com/schedules#PAXW43G `Edge Platform`

# Twilight #

Twilight is the UI for twitch.

* Slack: #twilight (also #site-production for major issues)
* Oncall: https://twitchoncall.pagerduty.com/schedules#PXCWK07

# Admin Panel #

Admin Panel is the service for support to help customers

* Rollbar: https://rollbar.com/Twitch/AdminPanel/
* Slack: #admin-services
* Oncall: https://twitchoncall.pagerduty.com/schedules#PWSNLKA

# BTS #

Balace tracker service is used by fabs to keep tracker of a user's balance as well as amount given to broadcaster

* Dashboard: https://w.amazon.com/index.php/PaystationExchange/Monad/Dashboard
* Oncall: https://oncall.amazon.com/mason-handler.fcgi/index.mhtml?name=gps-monad-oncall
* Ticket Queue: https://tt.amazon.com/search?status=Assigned%3BResearching%3BWork+In+Progress%3BPending&search=Search!&assigned_group=bts-support#

# SWS # 

SWS is the proxy that allows us to call ADG and FABS from the Twitch network.
* Dashboard for ADG Discovery: https://w.amazon.com/index.php/Online_Proxy_Fleet/SWS/Dashboard/Prod/ADGDiscoveryService
* Dashboard for FABS: https://w.amazon.com/index.php/Online_Proxy_Fleet/SWS/Dashboard/Prod/FuelAmazonBitsService
* Oncall: https://oncall.corp.amazon.com/#/view/autopilot-routing/schedule
* Create TT Ticket: https://tt.amazon.com/new?category=Retail+Systems&type=Subsidiary+Services&item=Subsidiary+Web+Services
* Create SIM: https://w.amazon.com/index.php/Website_Request_Handling/Need_Help#TT_or_SIM
