# Digital Assets team

Digital Assets team primary focus is emotes and badges, as well as some features involving them.
The only service currently owned by Digital Assets is [Mako][GitHub Mako] (which includes Paint).

If you require assistance from the Digital Assets team, you have the following options: 
* Using Slack, go into the room [`#digital-assets`][DA Slack] and ping the oncall (the current oncall is in the topic of the room).
* Using PagerDuty, please include descriptions and who to contact when coming online in any incidents.
  * Login into [`https://twitchoncall.pagerduty.com`][PagerDuty] using SSO and then create a new incident against either [`Digital Assets - Other Service`][PagerDuty: DA - Other] or [`Mako`][PagerDuty: DA - Mako] (as approriate).
    * Note: if you're not logged in, PagerDuty will always send you to the home page after login, and won't redirect back to the intended deep link.
  * Email [`digital-assets@twitchoncall.pagerduty.com`][Email PagerDuty] with a descriptive body and subject.

[GitHub Mako]: https://git.xarth.tv/commerce/mako
[DA Slack]: https://app.slack.com/client/T0266V6GF/CPU1HNTQQ
[PagerDuty]: https://twitchoncall.pagerduty.com
[PagerDuty: DA - Other]: https://twitchoncall.pagerduty.com/services/PKFD0B3
[PagerDuty: DA - Mako]: https://twitchoncall.pagerduty.com/services/POTVAT1
[Email PagerDuty]: mailto:digital-assets@twitchoncall.pagerduty.com?subject=Moments%20team%20looking%20for%20assistance