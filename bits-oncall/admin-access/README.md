# Getting access to active admin / admin panel

1. Get someone to goto [admin panel's permissions page](https://admin-panel.internal.twitch.tv/beef_cake/roles)
2. Use the roles tab to search for the new team member. See screenshot [below](#roles-tab)
3. Click the pencil next to their name to edit what roles they can use. See screenshot [below](#edit-user)
4. Add the `Bits` role using the search field, and click save. See screenshot [below](#search-menu)

The user you just modified should now have access to bits related features in active admin / admin-portal

## Roles Tab
![roles tab](https://s3-us-west-2.amazonaws.com/andymc-test/pictures/Screenshot+2017-10-03+12.56.00.png)
## Edit User
![edit user](https://s3-us-west-2.amazonaws.com/andymc-test/pictures/Screenshot+2017-10-03+13.06.45.png)
## Search Menu
![search menu](https://s3-us-west-2.amazonaws.com/andymc-test/pictures/Screenshot+2017-10-03+13.08.56.png) 
