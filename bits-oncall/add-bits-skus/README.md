# Adding new Bits SKU's

## Preface

First you'll need to make sure that you have the ASIN registered in ADG/horde and Zuora (via the Payments team). 
For mobile SKU's the mobile team should drive registering them. 
Adding them to our dynamo and redshift systems will only make them show up in the product catalog, be entitleable and show up in reporting. 

## Dynamo

In the FABS accounts [`twitchstore-aws-prod`](https://isengard.amazon.com/federate?account=277306168768&role=Admin) for prod 
and [`twitchstore-aws-test`](https://isengard.amazon.com/federate?account=256445009424&role=Admin) and staging you'll need to edit the dynamo directly.

In dynamo you need to add rows to the `bitsProductInformationBySkuAndPlatform` for the information about the new SKU 
([PROD](https://isengard.amazon.com/federate?account=277306168768&role=Admin&destination=dynamodb%2Fhome%3Fregion%3Dus-east-1%23tables%3Aselected%3DbitsProductInformationBySkuAndPlatform%3Btab%3Ditems), 
[STAGING](https://isengard.amazon.com/federate?account=256445009424&role=Admin&destination=dynamodb%2Fhome%3Fregion%3Dus-east-1%23tables%3Aselected%3DbitsProductInformationBySkuAndPlatform%3Btab%3Ditems)) 
and `bitAccountTypeConfiguration` ([PROD](https://isengard.amazon.com/federate?account=277306168768&role=Admin&destination=dynamodb%2Fhome%3Fregion%3Dus-east-1%23tables%3Aselected%3DbitAccountTypeConfiguration%3Btab%3Ditems), 
[STAGING](https://isengard.amazon.com/federate?account=256445009424&role=Admin&destination=dynamodb%2Fhome%3Fregion%3Dus-east-1%23tables%3Aselected%3DbitAccountTypeConfiguration%3Btab%3Ditems)) with information about bits usage priority, I would look at 

I would recommend looking at existing records for reference on the current schema.

## Redshift

This is very important and easy to forget, if you don't do it, payouts to affiliates and partners will be missing all bits used from the new SKU's.

You will need to be on wpa2 for this. Additionally you will need to setup 
[SQLWorkbench/J for Redshift](https://docs.aws.amazon.com/redshift/latest/mgmt/connecting-using-workbench.html), 
you can also use [Postico](https://itmarketplace.corp.amazon.com/detail_pages/590) for a more mac like client.

The username and password are stored in Odin as [com.amazon.twitch.seattle.bits.aws.raincatcher.prod.master for PROD](https://odin.amazon.com/#view/materialSet/com.amazon.twitch.seattle.bits.aws.raincatcher.prod.master) 
and [com.amazon.twitch.seattle.bits.aws.raincatcher.staging.master for STAGING](https://odin.amazon.com/#view/materialSet/com.amazon.twitch.seattle.bits.aws.raincatcher.staging.master).
You can find redshift IP's in the AWS Console under [Redshift/raincatcher-production](https://isengard.amazon.com/federate?account=021561903526&role=Admin&destination=redshift%2Fhome%3Fregion%3Dus-west-2%23cluster-details%3Acluster%3Draincatcher-production) and
[Redshift/raincatcher-staging](https://isengard.amazon.com/federate?account=021561903526&role=Admin&destination=redshift%2Fhome%3Fregion%3Dus-west-2%23cluster-details%3Acluster%3Draincatcher-staging).

Alright with all that out of the way you can now update the table it's called `bitaccounttypes` and has a row for each bit account type, you only need to provide the 3 columns of value.

## Twilight

This step does not have customer impact, but is still important for datascience. When adding a new bits amount, do not forget to add it in this file [src/features/bits/utils/tracking.ts](https://git.xarth.tv/twilight/twilight/blob/master/src/features/bits/utils/tracking.ts) like every other bits bundle. If you don't do it, datascience will not be sent when user clicks on the new bits bundle.

## Notes

There are examples of the SQL and DynamoDB JSON in this folder.  