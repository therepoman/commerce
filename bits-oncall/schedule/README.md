# Oncall Schedule
This is a list that can be used to determine what needs to be done per day of oncall. This is not a mandate, but more of a set of guideline to follow.
Oncall Schedule: https://oncall.corp.amazon.com/#/view/twitch-commerce/schedule

## Daily Tasks
+ Scan the [TT Queue](https://tt.amazon.com) for open tickets. Try to take action upon older tickets first. If the ticket needs information from another team, assign the ticket to their CTI. Resolving these tickets should be top priority.
+ Find tech debt stories from [This JIRA link](https://jira.twitch.com/issues/?filter=47369) and add them to the sprint to work on. We need to pay off our tech debt to improve system reliabitliy and performance.
* If there are not any tech debt stories or TTs to work on, feel free to pick up work in the sprint.

## Specific Day Tasks

### Tuesday
On Tuesdays, we have a weekly operational excellence meeting, where we review dashboards, TT's and [notes](https://docs.google.com/document/d/1jYyrrS8wgafSMIp5-rXW8Qtz1yWZrv8uwcIzJIwBBwA/edit)  

