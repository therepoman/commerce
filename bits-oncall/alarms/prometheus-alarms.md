## Prometheus Alarms

### Logscan
* [Sev 2 - Errors](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Prometheus.Prod.LogScanError&type=monitor)
* [Sev 2 - Panics](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Prometheus.Prod.LogScanPanic&type=monitor)
* [Sev 3 - Panics](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Prometheus.Prod.LogScanPanicWarn&type=monitor)

### Latency
* [Sev 3 - Publish Event p99 Latency](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Prometheus.Prod.PublishEvent.Latency.p99&type=monitor)

### SQS
* [Sev 3 - Event Ingestion DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Prometheus.Prod.EventIngestionDLQ&type=monitor)
