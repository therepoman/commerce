## Payday

### SQS
* Raincatcher
    * [DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.RaincatcherDLQ&type=monitor)
    * [Empty Receives](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.PaydayRaincatcherEmptyReceives&type=monitor)
    * [Messages Sent](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.PaydayRaincatcherMessageSent&type=monitor)
* Chat Badges
    * [DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.ChatBadgesDLQ&type=monitor)
    * [Empty Receives](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.PaydayChatBadgeEmptyReceives&type=monitor)    
* Datascience
    * [DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.DatascienceDLQ&type=monitor)
    * [Empty Receives](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.PaydayDatascienceEmptyReceives&type=monitor)    
* PubSub
    * [DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.PubsubDLQ&type=monitor)
    * [Empty Receives](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.PaydayPubsubEmptyReceives&type=monitor)
    * [Message Sent](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.PaydayPubSubMessageSent&type=monitor)
* Bit Events
    * [DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.PaydayBitEventsV1+DLQ&type=monitor)
    * [Empty Receives](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.PaydayBitEventsV1EmptyReceives&type=monitor)
* [Image Moderation DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.ImageModerationDLQ&type=monitor)
* [Image Update DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.ImageUpdateDLQ&type=monitor)
* [Image Job DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.ImageJobDLQ&type=monitor)
* [Payday DLQs](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.Deadleader_Not_Empty&type=monitor)
   * Supporting Monitors:
      - [Pantheon Events DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3a%09%0d%0apantheon_event_queue_deadletter_not_empty%2c+NONE)&type=agent)
      - [Automod Timeout DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3a%09%0d%0apayday_automod_timeout_queue_deadletter_not_empty%2c+NONE)&type=agent)
      - [Badge DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3a%09%0d%0apayday_badge_queue_deadletter_not_empty%2c+NONE)&type=agent)
      - [Admin Job DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3a%09%0d%0apayday_bits_admin_job_queue_deadletter_not_empty%2c+NONE)&type=agent)
      - [Bits on Extension DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3a%09%0d%0apayday_bits_on_extensions_pubsub_queue_deadletter_not_empty%2c+NONE)&type=agent)
      - [Channel Leaderboard DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3a%09%0d%0apayday_channel_leaderboard_queue_deadletter_not_empty%2c+NONE)&type=agent)
      - [New User Purchase DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3a%09%0d%0apayday_new_user_purchase_queue_deadletter_not_empty%2c+NONE)&type=agent)
      - [Partner Onboarding Automation DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3a%09%0d%0apayday_partner_onboarding_automation_queue_deadletter_not_empty%2c+NONE)&type=agent)
      - [Prometheus DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3a%09%0d%0apayday_prometheus_queue_deadletter_not_empty%2c+NONE)&type=agent)
      - [Pushy DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3a%09%0d%0apayday_pushy_queue_deadletter_not_empty%2c+NONE)&type=agent)
      - [Raincatcher Audit DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3a%09%0d%0apayday_raincatcher_audit_queue_deadletter_not_empty%2c+NONE)&type=agent)
      - [Redeem Key Code DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3a%09%0d%0apayday_redeem_key_code_queue_deadletter_not_empty%2c+NONE)&type=agent)
      - [Revenue DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3a%09%0d%0apayday_revenue_queue_deadletter_not_empty%2c+NONE)&type=agent)
      - [Settings Update DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3a%09%0d%0apayday_settings_update_queue_deadletter_not_empty%2c+NONE)&type=agent)
      - [Upload Service Callback DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3a%09%0d%0apayday_upload_service_callback_queue_deadletter_not_empty%2c+NONE)&type=agent)
      - [User Moderation DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3a%09%0d%0apayday_user_moderation_queue_deadletter_not_empty%2c+NONE)&type=agent)
      - [Hashtag DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3a+payday_hashtag_queue_deadletter_not_empty%2c+NONE)&type=agent)
      - [Image Moderation DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3a+payday_image_moderation_queue_deadletter_not_empty%2c+NONE)&type=agent)
      - [Bit Events V1 DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3apayday_bit_events_v1_queue_deadletter_not_empty%2c+NONE)&type=agent)
      - [Bits Cache DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3apayday_bits_cache_queue_deadletter_not_empty%2c+NONE)&type=agent)
      - [Datascience DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3apayday_datascience_queue_deadletter_not_empty%2c+NONE)&type=agent)
      - [Pubsub DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3apayday_pubsub_queue_deadletter_dlq_not_empty%2c+NONE)&type=agent)
      - [Raincatcher DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Alarm(CloudWatch%2c+arn%3aaws%3acloudwatch%3aus-west-2%3a021561903526%3aalarm%3apayday_raincatcher_queue_deadletter_not_empty%2c+NONE)&type=agent)

### Firehose
* [Raincatcher Redshift Success](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.RaincatcherRedshiftSuccess&type=monitor)
* [Raincatcher S3 Success](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.RaincatcherS3Success&type=monitor)

### Datapipelines
* [Failures](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Raincatcher.Production.DatapipelineFailures&type=monitor)
* [Dynamo Backups](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.DynamoBackups&type=monitor)
* [Failures - Sev 2](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Raincatcher.Production.DatapipelineFailuresHighwatermark&type=monitor)

### Status Code Range
* [5xx Volume](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.5xxVolume.High&type=monitor)

### Latency
* [High Severity Latency](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.HighSev.Latency&type=monitor)
* Core
    * [Create Bits Event](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.CreateBitsEventVisageLatency&type=monitor)
    * [Get Balance - Legacy](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.PaydayGetBalanceLatency&type=monitor)
    * [Get Balance - Visage](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.GetBalanceVisageLatency&type=monitor)
    * [Get Channel Info](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.GetChannelInfoVisageLatency&type=monitor)
    * [Get Actions](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.GetActionsLatency&type=monitor)
    * [Get User Info](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.GetUserInfoVisageLatency&type=monitor)
    * [Get Bits Usage Leaderboard](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.GetBitsUsageLeaderboardLatency&type=monitor)
* Products
    * [Get Products](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.PaydayGetProductsLatency&type=monitor)
    * [Get Prices](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.GetPricesVisageLatency&type=monitor)
    * [Entitle Bits](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.PaydayEntitleBitsLatency&type=monitor)
* Extensions
    * [Use Bits on Extension](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.UseBitsOnExtension.Latency&type=monitor)    
    * [Is Eligible to use Bits on Extension](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.IsEligibleToUseBitsOnExtension.Latency&type=monitor)
* Partner Settings Dashboard
    * [Start Custom Cheermote Upload](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.StartCustomCheermoteUploadLatency&type=monitor)
    * [Get Badge Tiers](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.GetBadgeTiersVisageLatency&type=monitor)
    * [Get Channel Settings](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.GetChannelSettingsVisageLatency&type=monitor)
    * [Set Badge Tiers](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.SetBadgeTiersVisageLatency&type=monitor)
    * [Update Channel Settings](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.UpdateChannelSettingsVisageLatency&type=monitor)
* [Get Supported Tags](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.GetSupportedTagsLatency&type=monitor)

# Extensions API
* [Use Bits on Extension - 5xx Count](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.UseBitsOnExtension.500Count&type=monitor)
* [Is Eligible to use Bits on Extension - 5xx Count](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.IsEligibleToUseBitsOnExtension.Latency&type=monitor)

### Elastic Beanstalk
* [Sev 2 - EB Health](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.EB.Health&type=monitor)

### Logscan
* [Sev 2 - Errors](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.LogscanError&type=monitor)
* [Sev 3 - Panics](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.LogscanPanic.Low&type=monitor)
* [Sev 2 - Panics](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.LogscanPanic.High&type=monitor)

### Order Alarms
* [Get Products - No Country Specified](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.GetProducts_BadRequest_NoCountry&type=monitor)
* [iOS Purchase Volume](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.iosPurchaseVolume&type=monitor)
* [Android Purchase Volume](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.androidPurchaseVolume&type=monitor)
* [Paypal Purchase Volume](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.paypalPurchaseVolume&type=monitor)
* [Amazon Purchase Volume](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.amazonPurchaseVolume&type=monitor)
* [ADG not returning pricing](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Payday.Prod.ADGNoPrice)

