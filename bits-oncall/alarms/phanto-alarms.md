## Phanto Alarms

### Logscan
* [Sev 2 - Errors](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Phanto.Prod.LogScanError&type=monitor)
* [Sev 2 - Panics](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Phanto.Prod.LogScanPanic&type=monitor)
* [Sev 3 - Panics](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Phanto.Prod.LogScanPanicWarn&type=monitor)

### SQS
* [Sev 3 - Key Generation Jobs DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Phanto.Prod.KeyGenerationDLQ&type=monitor)
