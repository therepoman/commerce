## Pantheon

### Logscan
* [Sev 3 - Errors](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Pantheon.Prod.LogscanError.Warning&type=monitor)
* [Sev 2 - Errors](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Pantheon.Prod.LogscanError.Alert&type=monitor)
* [Sev 3 - Panics](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Pantheon.Prod.LogscanPanic.Warning&type=monitor)
* [Sev 2 - Panics](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Pantheon.Prod.LogscanPanic.Alert&type=monitor)

### Elastic Beanstalk
* [EB Health Sev 2](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Pantheon.Prod.Health.Alert&type=monitor)

### SQS Metrics
* Events
    * [DLQ - Sev 3](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Pantheon.Prod.Event.DLQ.Warning&type=monitor)
    * [DLQ - Sev 2](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Pantheon.Prod.Event.DLQ.Alert&type=monitor)
    * [Empty Receives](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Pantheon.Prod.Event.Queue.Empty.Recieves&type=monitor)

### Latency
* [Get Leaderboard](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Pantheon.Prod.GetLeaderboard.Latency&type=monitor)
* [Get Leaderboards](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Pantheon.Prod.GetLeaderboards.Latency&type=monitor)
* [Publish Event](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Pantheon.Prod.PublishEvent.Latency&type=monitor)