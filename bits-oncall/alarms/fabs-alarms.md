## FABS

### SQS
* Bits Orders
    * [Sev 3 - DLQ](https://carnaval.amazon.com/v1/viewObject.do?type=monitor&name=Twitch.Commerce.BitsOrderDLQSize)
    * [Sev 2 - DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Commerce.BitsOrderDLQSize.HighWater&type=monitor)
    * [Empty Receives](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Commerce.BitsOrders.NumberOfEmptyRecieves&type=monitor)
* [TIMS Update Notification DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Commerce.TIMSUpdateNotificationQueueDLQNotEmtpy&type=monitor)
* [Perseus DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Commerce.BitsPerseusDLQSize&type=monitor)
* [Chargeback DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Commerce.BitsChargebackDLQSize&type=monitor)
* [Create Address Record DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Commerce.CreateAddressRecordNotificationQueueDLQNotEmpty&type=monitor)
* [Tails Tax Info Verification DLQ](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Commerce.TAILSTaxInformationVerificationNotificationQueueDLQSize&type=monitor)

### Dynamo
* [Recent Products write throttle](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.FABS.Prod.RecentProducts.Dynamo.Write.Throttle&type=monitor)
* [Recent Products write throttle](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.FABS.Prod.IntrumentId.Dynamo.Throttled.Read&type=monitor)
* [Instrument ID write throttled](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.FABS.Prod.RecentProducts.Dynamo.Read.Throttle&type=monitor)

# Infrastructure
* [Min Healthy Hosts alive](https://carnaval.amazon.com/v1/viewObject.do?name=Twitch.Commerce.FABS.currServicesUpWarn&type=monitor)

# MonitorPortal Alarms
* Mostly includes availability / latency alarms for APIs
* [61 Alarms in Search](https://monitorportal.amazon.com/search?component_type=Monitor&query=FuelAmazonBitsService)