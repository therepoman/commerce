### So you want to make a new service?

#### New Service Account Setup
- [ ] Add a new entry to the CTI where your resources will be grouped - [CTI Page](https://cti.amazon.com)
- [ ] Create new service accounts in Isengard in the format `twitch-${service-name}-aws-${stage}` where `stage` is either `prod` or `devo`
- [ ] File ticket to Systems to add VPC / Networking / CIDR block for new service accounts. [Sample JIRA](https://jira.twitch.com/browse/SYS-14041) - Do not clone, as you will not have edit access after you create the ticket. Just copy the fields into a new ticket.
- [ ] Create Private Link between your new VPCs and devtools Artifactory / Docker via [TPS](https://tps.internal.justin.tv)
- [ ] Create Private Link between your new VPCs and LDAP via [TPS](https://tps.internal.justin.tv)
- [ ] Create Private Link between your new VPCs and GHE via [TPS](https://tps.internal.justin.tv)
- [ ] Configure DNS entries in your service accounts for LDAP / dev tools services in Route53 (See Pachinko Route53)
- [ ] Add new DHCP options set to VPCs to utilize your Route53 DNS records (See Pachinko VPC DHCP Options)
- [ ] Configure DNS entries for your internal service endpoints [DNS management dashboard](https://dashboard.internal.justin.tv/dns)
- [ ] Request access to the wildcard internal.justin.tv cert / key [Sample JIRA](https://jira.twitch.com/browse/SYS-14066)
- [ ] Onboard your accounts with Capacity team (Just ping them the accounts in #capacity_management)
- [ ] Onboard your accounts with the Teleport Bastion [Wiki Guide](https://wiki.twitch.com/display/SEC/Teleport+Remote#TeleportRemote-Planning)

#### New Service Application Setup
- [ ] Onboard with S2S if needed. [S2S dashboard](https://dashboard.internal.justin.tv/s2s/services)
- [ ] Create Sandstorm access role / secrets if your service requires them. [Sandstorm Roles dasboard](https://dashboard.internal.justin.tv/sandstorm/manage-roles)
- [ ] Do `tcs aws init ${aws-profile}` to provision a Jenkins user / secrets for builds (need to do this for both devo and prod). [Tool Info](https://git.xarth.tv/dta/tcs)
- [ ] Add `scripts` folder with two scripts `docker_build.sh` and `docker_push.sh` to support Docker commands that are used in Jenkins builds / deploys (make sure to `chmod +x`)
- [ ] Add `jenkins.groovy` file to root of repo so Jenkins knows how to build / deploy your repo
- [ ] Add the Jenkins webhook to your repo (will not build for the first time until the `jenkins.groovy` is merged into master)
- [ ] Add `build.json` and `deploy.json` for Docker builds / deploys.
- [ ] Add `Dockerfile` for starting up your service in your container.

#### Metrics / Alarms
- [ ] Create Sev 2 log scan alarms for errors and panics
- [ ] Create Sev 2 log scan alarm for panics
- [ ] Create Sev 3 latency alarms for key APIs on p90
