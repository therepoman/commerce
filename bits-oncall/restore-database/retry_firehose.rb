# Retries ingestion for all Firehose manifest files given a prefix
# Requires the aws-sdk gem and the pg gem
# Redshift DB passwords can be found in the team's 1Password vault
require 'aws-sdk'
require 'pg'

# Payday IAM user info
ACCESS_KEY_ID = 'FILL ME IN!!!'
SECRET_ACCESS_KEY = 'FILL ME IN!!!'

# Redshift info
DB_NAME = 'prod'
DB_HOST = '52.32.31.152'
DB_PORT = '5439'
DB_USER = 'firehose'
DB_PW = 'FILL ME IN!!!'

BUCKET = 'payday-raincatcher-firehose'
PREFIX = 'transactions/errors/2019'

COPY_COMMAND = %Q(
	COPY transactioninfo 
	FROM 's3://payday-raincatcher-firehose/%s' 
	CREDENTIALS 'aws_iam_role=arn:aws:iam::021561903526:role/RaincatcherRedshiftRole'
	MANIFEST json 'auto' 
	ACCEPTINVCHARS;
)


s3 = Aws::S3::Resource.new(
	region: 'us-west-2',
	access_key_id: ACCESS_KEY_ID,
	secret_access_key: SECRET_ACCESS_KEY
	)

bucket = s3.bucket(BUCKET)
unless bucket.exists?
	puts "bucket does not exist"
	exit(1)
end
manifests = bucket.objects(prefix: PREFIX).collect(&:key)


conn = nil
begin
	conn = PG.connect dbname: DB_NAME, host: DB_HOST, port: DB_PORT, user: DB_USER, password: DB_PW
	puts conn.server_version
rescue PG::Error => e
	puts e.message
end


manifests.each do |manifest|
	query = COPY_COMMAND % manifest
	results = conn.exec(query)
	puts manifest
	puts results.inspect
end
