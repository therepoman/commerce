## Data restore plan

We own four (Payday, Pantheon, Phanto, FABS) services and each of them stores its data in its own database.
Here is the protocols to follow to restore those databases to get back to the normal operations in case those databases get corrupted,
lost, etc., for any reasons.

1. **Redshift cluster - Payday**

    Payday has a Redshift cluster named `raincatcher` on which all the business-valuable tables reside. This cluster is automatically backed up
    every 8 hours and old ones get deleted every 10 days. To restore this cluster:
    + Go to Payday's redshift console from Isengard. On the left navigation bar, select "Snapshots".
    + You'll see a list of snapshots (backups) in the last 10 days. Select one of those that you want to use to restore your data.
    + Click the "Actions" dropdown at the top and select "Restore From Snapshot".

    Official documentations on Redshift data restore can be found [here](https://docs.aws.amazon.com/redshift/latest/mgmt/working-with-snapshots.html).

2. **Dynamo DB - Payday, Pantheon, Phanto**

    All these services rely on dynamo DB as its primary data store.
    Each of the tables is automatically backed up at certain period (e.g. every 5 hours).
    To restore these tables:
    + Go to each service's dynamo DB console from Isengard. On the left navigation bar, select "Backups".
    + You'll see a list of backups for those two tables. Select the ones you want to restore data from.
    + Click "Restore backup" button at the top.

## Raincatcher Delivery Failure

We have four Firehose streams deliverying data into Raincatcher, occasionally those delivery streams might fail (after retrying for two hours) and we would need to manually backfill the data.

### Manually Backfill the Data
- First let the team and accounting know about the missing data in Raincatcher. Any report generated for the given month is likely to be inaccurate.
- Errors logs can be found in cloudwatch logs under the filter "/aws/kinesisfirehose/". Alternatively, Raincatcher has a table named `stl_load_errors` that also stores recent load errors.
- If the error is retryable:
    * Find the failed manifest files in Payday's S3 bucket `payday-raincatcher-firehose/<faild_stream>/errors`. Manifest files are grouped by date and hour.
    * Find the failed Firehose stream in the AWS console under Kinesis/DataFirehose. On its detail page, you should find a Redshift COPY command, which looks similar to:
    ```
    COPY transactioninfo
    FROM 's3://payday-raincatcher-firehose/<manifest>' 
    CREDENTIALS 'aws_iam_role=arn:aws:iam::<aws-account-id>:role/<role-name>' 
    MANIFEST json 'auto' ACCEPTINVCHARS;
    ```
    * Fill in the COPY command manifest file path, along with the account ID (`021561903526`) and role name (use `RaincatcherRedshiftRole`)
    * Connect to the Raincatcher cluster and execute the COPY command (see retry_firehose.rb for a ruby reference if writing a script). Redshift credentials can be found in the team's shared 1Password vault.
