# Gets all the failed transactions and saves them into CSV file
# Remember to change the CSV header based on the type of data
require 'aws-sdk'
require 'pg'
require 'csv'

# Payday IAM user info
ACCESS_KEY_ID = 'FILL ME IN!!!'
SECRET_ACCESS_KEY = 'FILL ME IN!!!'

# Redshift info
DB_NAME = 'prod'
DB_HOST = '52.32.31.152'
DB_PORT = '5439'
DB_USER = 'firehose'
DB_PW = 'FILL ME IN!!!'

BUCKET = 'payday-raincatcher-firehose'
PREFIX = 'emoteuses/errors/manifests/2019/06'

OUT_FILE_NAME = "emoteuses_missed_transactions.csv"


s3 = Aws::S3::Resource.new(
	region: 'us-west-2',
	access_key_id: ACCESS_KEY_ID,
	secret_access_key: SECRET_ACCESS_KEY
	)

bucket = s3.bucket(BUCKET)
unless bucket.exists?
	puts "bucket does not exist"
	exit(1)
end



csv = CSV.open("./out/#{OUT_FILE_NAME}", "w")
csv << [
	'transaction_id',
	'emotecode',
	'bitamount',
	'timeofevent',
]

bucket.objects(prefix: PREFIX).each do |obj|
	content = obj.get.body.string
	urls = JSON.parse(content)['entries'].map { |e| e['url'] }

	urls.each do |url|
		puts "processing #{url}"
		content = bucket.object(url.gsub("s3://payday-raincatcher-firehose/", "")).get.body.string

		# The content isn't really valid JSON, so doing some slow string manipulation here
		entries = JSON.parse("[%s]" % content.gsub(/\}\{\"transactionid\"/, "},{\"transactionid\""))

		entries.each do |e|
			csv << [
				e['transactionid'],
				e['emotecode'],
				e['bitamount'],
				e['timeofevent']
			]
		end
	end
end

csv.close
