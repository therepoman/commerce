# Current Logs:
Logs will be on individual boxes for two hours before being moved to our timber FS archive 
Identify host addresses from apollo.

* FABS: https://apollo.amazon.com/environments/FuelAmazonBitsService

1. Run `mwinit` and use yubikey
2. SSH into the host address
3. Once SSH’d into host, logs are located in: /apollo/env/[ENV_NAME]/var/output/logs/ 

## FABS RTLA
RTLA (RealTimeLogAnalysis) shows Errors (FAULT) and Warning (NON-FAULT) messages from logs. This makes it easier to research sev-2s and general failures without having to ssh into a FABS host or timber.
* https://rtla.amazon.com/explore.do?org=BITSFABS

# Archived Logs:
## If you haven’t already set up security bastions
You need to in order to ssh to prod amazon internal vms https://security-bastions.amazon.com/ 
(This will edit your .ssh/config file to add some regexs in order to do a double 
hop when you ssh to a prod machine)
* For macbook click OSX section
* Follow instructions
    * If problems: https://w.amazon.com/index.php/How_To_SSH_Security_Bastions

## If you have set up security bastions, 
Run `mwinit` and use yubikey
1. Then `ssh twitch-timber-c3-xlrg-i-a7c35514.us-east-1.amazon.com` to get to the timber box
    * https://apollo.amazo n.com/environments/TimberFS/IAD/TwitchCommerce/
2. Expand Auto scaling group and find the one host
3. ssh to host
4. On vm logs are in 
    * /onlinelogs/FuelAmazonBitsService 
    * Timezone in folder structure is PST
