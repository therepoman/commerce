---
title: "Change Requests"
date: 2020-06-22T10:27:43-07:00
description: ""
weight: 5
---


For large production changes such as feature launches, it is good practice to open a Twitch Change Request, and share it in [#site-production](https://twitch.slack.com/archives/C03SW9JBR).
This is not required for bug fixes or smaller production changes. 

A change request should clearly list what steps need to be taken both to turn the feature on, and to turn it back off. 
This information can assist the on-call if something goes wrong during the launch. 

## How to create a change request

Go to your [team’s JIRA project](https://jira.twitch.com/projects/MOMENTS), and click the [Create] button. 
For issue type, switch to “Change Request” and fill it out from there.
Here are some past examples from the old Central Commerce team:

- https://jira.twitch.com/browse/REWARD-2601


Once the change request is ready, review it with the stakeholders and make sure everyone is aligned on the details.
 
