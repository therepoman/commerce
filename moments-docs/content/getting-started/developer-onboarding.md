---
title: Developer Onboarding
description: "Onboarding guide for moments engineers"
date: 2020-05-01T08:33:08-07:00
weight: 2
---

Welcome new Moments developers! This guide is written to help you set up your local development environment and to smoothe the onboarding process.

Unless specified, if you need to be granted access to anything, please refer to your manager if you have trouble 

### General Resources
- [Twitch New Hire Guide](https://twitchpeople.xarth.tv/hc/en-us/categories/115000707347-Welcome-to-Twitch-#360000232703)
- [Twic](https://client.twic.ai/explore)

### Prerequisite Setup
Please install all of the following to set up your developer environment

- [Homebrew](https://brew.sh/)

- [Twitch Staff Account](https://wiki.twitch.com/display/NET/How+to%3A+Create+a+Staff+Twitch+Account)

- Request Twitch Partner Account
  - Create a Partner QA account. Be sure to follow the naming pattern shown in the [Google Sheets](https://docs.google.com/spreadsheets/d/1Nt1pN2f58vQ1HMgVwuIouZadXbzjxjypD3QTxVsbdCU/edit?ts=5cd495a0#gid=233295339)
  - Collect the necessary info listed in the Google Sheets and ping your manager to request the Twitch Partner account. 

- [Amazon VPN](https://it.amazon.com/en/help/articles/registering-your-vpn-token) - Used to view internal Amazon resources and gain temporary permissions to shared AWS profiles.

- [Twitch VPN](https://wiki.twitch.com/display/HD/Twitch+VPN) - Most twitch internal services are gated by VPN usage. When trying to access a domain or run a service locally, please make sure you are on the Twitch VPN.

- [Golang](https://golang.org/dl/) - Most of the backend services here are written in Go. You can also install it with `brew install go`. Afterwards, add this line in your `.bashrc` or `.zshrc` if it's not in there already.
  ```
  export GOPATH=$HOME/go
  ```
This allows you to check out projects using `go get code.justin.tv/<owner>/<repo>` which will clone the project to `$GOPATH/src/code.justin.tv/<owner>/<repo>`.

- [Redis](https://redis.io/download) - Some of the backend services will have a Redis cache layer before the database (such as [Pantheon](https://git.xarth.tv/commerce/pantheon)). You can install with `brew install redis`. This memcache is a separate process, so you need to start it with `redis-server` before you execute the code for the services. (It's recommended to dedicate a terminal window for it, makes shutting it down easier)

- Midway
    - Midway is an SSO auth system and the YubiKey is a hardware token. These are used to log into Amazon systems.
    - Get a YubiKey from IT
    - [Setup midway token](https://twitchcorpit.service-now.com/sp?id=kb_article_view&sys_kb_id=5ee0dfce1b64281094c985d7cc4bcb82)

- Git SSH
  - [Create SSH Key](https://help.github.com/en/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent) (use `"<insert-your-username>@twitch.tv"` for email)
  - [Add SSH Pub-Key to GitHub](https://help.github.com/en/github/authenticating-to-github/adding-a-new-ssh-key-to-your-github-account)
  - [Set your username and email for Git](https://help.github.com/articles/setting-your-username-in-git/)
  - Add your new SSH key to the [LDAP Dashboard](https://dashboard.internal.justin.tv/.)

- AWS Profile
  - Work with another engineer to add you to get access to twitch-subs-aws, twitch-subs-dev AWS accounts
    - Steps to add new engineer: “Manage Accounts” -> “Console Access Roles” and add users. Disable password requirement for new user
  - Go to [Isengard](https://isengard.amazon.com/console-access). Follow the directions to set up your YubiKey.
  - Check to see that the two *-subs*- accounts are in your “AWS Console Access”
  - Set-up tools to refresh your Isengard temporary credentials for development and testing
    - Install [isengard_credentials](https://git.xarth.tv/twitch/isengard_credentials)
    - Install Midway tool mwinit
    ```
      curl -O https://s3.amazonaws.com/com.amazon.aws.midway.software/mac/mwinit && chmod u+x mwinit && sudo mv mwinit /usr/local/bin/mwinit
    ```

- AWS CLI
  - Install AWS CLI
    ```
    brew install awscli
    ```
  - Configure the keys for AWS using the command line by following [these instructions](../aws-console). 
  
- [Docker](https://docs.docker.com/docker-for-mac/install/)

### LDAP Membership
Access to our Git repositories is managed through LDAP and synched to Github Enterprise.  Request Anran or Eddie to add you to the following groups:
- team-subs
- team-revenue
- team-commerce
- team-payday

Note: There is a synch lag between LDAP and GHE.  We think it's a few hours, but we're not 100% certain.  If you need immediate access, a hack is to create a new Team in one of the repos in GHE which will force it to synch with LDAP.

### Moments Github team
Request access tp the [moments team github](https://git.xarth.tv/orgs/commerce/teams/moments) 

### Teleport Bastion
Set up your Teleport Bastion environment by following the [instructions here](https://wiki.twitch.com/display/SEC/Teleport+Bastion)

### Services
These are the [services owned by the Moments team](https://catalog.xarth.tv/services?primary_owner_management_chain_ldaps=ebenhoeh&primary_owner_management_chain_ldaps=kotowe):
- [Prism](https://git.xarth.tv/commerce/prism) - Twitch chat preprocessor service. Determines if a chat message has a cheer in it.
- [Pachinko](https://git.xarth.tv/commerce/pachinko) - Consumable token service. Used to determine if a user has bits, channel points, or sub tokens to use.
- [Payday](https://git.xarth.tv/commerce/payday) - Twitch Bits service.
- [Phanto](https://git.xarth.tv/commerce/phanto) - Key service, used for fulfilling bits codes.
- [Pachter](https://git.xarth.tv/commerce/pachter) - Bits Accounting & Reporting service
- [Petozi](https://git.xarth.tv/commerce/petozi) - Bits Product Catalog & Metadata service
- [Prometheus](https://git.xarth.tv/commerce/prometheus) - Twitch Records Searching service. Used for looking up records for a users bits usage. 
- [Personalizaton](https://code.amazon.com/packages/TwitchPersonalization/trees/mainline) Personalization service.
- [Clippy](https://git.xarth.tv/subs/clippy) - Recommendation engine for sub gifting. 
- [bits-oncall](https://git.xarth.tv/commerce/bits-oncall) - On-call repo for Moments

*Pantheon and Fortuna are operationally owned by the Digital Assets team, Moments is responsible for any feature work.

These are services owned by other teams, but they contain code owned by Moments, or otherwise support the Moments systems. 
- [Twilight](https://git.xarth.tv/twilight/twilight) - Twitch Web Client, owned by Browser Clients
- [Mako](https://git.xarth.tv/commerce/mako) - Emote Service, owned by Digital Assets
- [Visage](https://git.xarth.tv/edge/visage) - Twitch API Gateway, owned by Edge team
- [GraphQL](https://git.xarth.tv/edge/graphql) - Twitch API Gateway, owned by Edge team
- [Materia](https://code.amazon.com/packages/Materia/trees/mainline) - Entitlement Service, core component of [Chevron](https://git.xarth.tv/pages/subs/chevron-docs/). Owned by Money (Purchase Platform).  
- [Subscriptions](https://git.xarth.tv/revenue/subscriptions) - Subscriptions Monolith Service
  - [Lambdwich](https://git.xarth.tv/revenue/lambdwich) - Lambda functions for Subscriptions service
  - [SubscriptionsLambdasLambda](https://code.amazon.com/packages/SubscriptionsLambdasLambda/trees/mainline) (also called Subs LamLam) - Fulton-based lambda for Subscriptions, primarily supports the Founders Badge feature.
  - [Workflows](https://git.xarth.tv/subs/workflows) - Step function definitions for subs service
  - [Subs glue jobs](https://git.xarth.tv/subs/db-export-terracode) - Terraform for exporting data in subs databases to [Tahoe](https://wiki.xarth.tv/display/DI/Tahoe)
- [Voyager](https://code.amazon.com/packages/TwitchVoyager/trees/mainline) - New Octane service supporting "core" subscription flows.
- [Chronobreak](https://git.xarth.tv/subs/chronobreak) - Cumulative Tenure Service
- [Valhalla](https://git.xarth.tv/revenue/valhalla) - Legacy Subscriptions code written in Ruby (deprecated)
- [Galleon](https://git.xarth.tv/subs/galleon) - iOS Sub Token service
- [Griphook](https://git.xarth.tv/subs/griphook) - Deferred payouts (supports iOS Sub Tokens and Multi-Month gifting)
### Handling CS Requests

Read more in the Operations > [Investigating CS Issues](../../docs/operations/cs-issues.html) section of the docs.

### Internal Tools
Here are some links to commonly used internal tools

- [Oncall](https://oncall.corp.amazon.com/)
- [Trouble Ticketing](https://tt.amazon.com/)
- [Isengard](https://isengard.amazon.com/) - Portal to manage AWS accounts
- [Browser Scripts](https://w.amazon.com/index.php/AWS_IT_Security/Isengard/Scripts#Isengard%20AWS%20Banner) - Script that shows a banner in AWS consoles to display thr `accountID`. Must be on WPA2 to access the link.
  - [Isenlink Script](https://improvement-ninjas.amazon.com/gmget.cgi/isenLink.user.js)
- [Grafana](https://grafana.xarth.tv) - Production traffic information (Subs/Box Office/Nioh are migrated here). Read more in [Monitoring](../../docs/operations/monitoring.html).
- [Deploy](https://deploy.xarth.tv/#/commerce) - Internal deployment tool for <strong>most</strong> services at Twitch
- [Admin Panel](https://git.xarth.tv/admin-services/admin-panel) - Internal tool to perform some manual functions. (May need permission to view certain categories)
    - [Admin Panel Access Process](https://wiki.xarth.tv/display/CHAOS/Admin+Panel+Access+Process)
- [Toolkit](https://git.xarth.tv/admin-services/admin-panel) - Internal tool to perform some manual functions. (May need permission to view certain categories)
- [Sandstorm](https://dashboard.internal.justin.tv/sandstorm/) - Used for secret management<br>
    - For first time setup, ping another developer to give you Sandstorm permissions
    - To give access: Find the subs role for [staging](https://dashboard.internal.justin.tv/sandstorm/manage-roles?action=editrole&role=subscriptions-staging) and add the new developer's Isengard ARN. 
- Slack - Most of our async communication happens via slack. Please ask your manager or team lead to invite you to our channels

### Convenient External Tools
Here are some links to commonly used external tools

- [Insomnia](https://insomnia.rest/) - Client that can make REST, GQL, and protobuf calls (protobuf can be installed via `brew install protobuf`, `which protoc` to see if it's installed)
- [VSCode](https://code.visualstudio.com/download) - Popular IDE amongst the team
- [IntelliJ](https://www.jetbrains.com/idea/) - The other major IDE used on the team (you can get a license via software.amazon.com).
- [Mode Analytics](https://modeanalytics.com/) - Query for Data Analytics, <strong>Note: This is useful for expensive SQL queries, there are no timeouts under Science role</strong>
- [LucidChart](https://lucidchart.com/) - Draw graphs, charts, processes

### Libraries and Frameworks
Here are some of the libraries and frameworks that we use, and some useful references

- Backend
  - [Twirp](https://github.com/twitchtv/twirp) - In-house RPC framework that generates automatic routing and serialization. Used to communicate between our backend services.
  - [Go by Example](https://gobyexample.com/) - Useful reference for new Go developers
  - [Terraform](https://www.terraform.io/docs/index.html) - Infrastructure as code
- Frontend
  - [React](https://reactjs.org/docs/hello-world.html)
  - [CoreUI](https://design.internal.justin.tv/) - Internal design framework
  - [TypeScript](https://www.typescriptlang.org/docs/home.html)
- Query Language
  - SQL
  - [GraphQL](https://www.howtographql.com/) - Useful reference for new GraphQL developers
