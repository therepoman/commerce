---
title: "Making a Pull Request"
description: "PR Process"
date: 2020-05-01T08:33:08-07:00
weight: 4
---

To Contribute a Pull Request to any Subscriptions Team owned service, please adhere to the following:

- Each Pull Request for Moments Services should be tagged with their associated JIRA task.
   - Please prefix each PR with Jira task number and a description. I.E SUBS-2050: "This is a description". This will automatically tag your JIRA ticket with your PR.
   - For an example [Jira ticket](https://jira.twitch.com/browse/SUBS-1723) and [Pull Request](https://git.xarth.tv/revenue/subscriptions/pull/808)

- Pull Requests must be approved by 1 or more members of the team.
- New APIs should have unit tests and integration tests
