---
title: "Glossary"
description: "Commonly used terms at Twitch"
date: 2020-05-01T08:33:08-07:00
weight: 10
---

- This document contains terminologies that are commonly used at Twitch. 
- Feel free to contribute and update definitions.
- If you don't find what you are looking for here, you could try the [wiki](https://wiki.xarth.tv/display/ENG/Glossary).

## Terminologies

- **Admin Panel** - Internal support tool used by customer support and engineers to address issues experienced on the site such as incorrect tenures, uploading/deleting badges, payment processing data and much more. Is in the process of being replaced by **Toolkit**.

- **Active Ticket** - An active ticket is ticket where it’s start date is less than now, and it’s end date is greater than now. The existence of an active ticket for a user means they have a subscription. 

- **Bits** - Explicitly not a digital currency, bits can be purchased by the user and cheered in a channel to give money to the streamer. 
  
- **Box office** - The Twitch video access token service

- **Breakage** - If a person has purchased bits but doesn’t cheer them, after a certain period of time, we assume that person will never cheer the bits. At that point, the bits are considered “broken”, and revenue can be realized at that time. For sub tokens, they will be considered “broken”, and the revenue will be realized, when the tokens expire. 

- **Bubbles** -  Payments serverless framework for async event processing

- **Cheer** - A user can **cheer** in a channel by using bits in a chat message.

- **Chronobreak** - A standalone generic tenure service. See repo [here](https://git.xarth.tv/subs/chronobreak)

- **Clean Deploy** - Deployment tool used by many twitch services to help deploy services to various environments.

- **COE** - Correction of Error. Usually written when some error or bug has impacted Production environments. 

- **Cumulative Tenure** - defines the total tenure of a user's subscription to a channel.

- **DADS** - Declining Ads Service - Ads team business logic service

- **Deferred Revenue** - For tax and accounting reasons, money accepted for a product (bits, sub tokens) isn’t counted towards revenue numbers until the    product is actually used (e.g., until the bits are cheered or the sub token is redeemed). 

- **Dep** - Golang dependency manager that is used by many golang services at Twitch/ May be deprecated in the future to be Go modules. See [here for more info](https://golang.github.io/dep/)

- **Destiny** - Service used to guarantee events are sent. See [design doc](https://docs.google.com/document/d/18kZU3L_3zdxLMrT1dmCi-bOfAN5zbChUImVBXSAUIX8/edit?usp=sharing)

- **DNR** - Do Not Renew

- **Emotes** - Small pictoral glyphs that fans can use in chat. 

- **Everdeen** - A service hosting APIs for processing purchase orders and generic payment notifications.   

- **Entitlement** - A thing (usually a user) owns a thing (item) for a reason (origin)

- **Future Dated Ticket** - A future dated ticket is a ticket that has a start date and end date after now. When a user wants to convert a nonrecurring subscriptions to a recurring subscription.

- **Gift Subscriptions** - The ability for a user to gift a Twitch Subscription to another user. 

- **GraphQL** - Twitch's GraphQL service for first party client. See [Github Repo](https://git.xarth.tv/edge/graphql)

- **Gringotts** - Service that takes in creator attributions, used to power creator dashboard and finance payout reports

- **Hallpass** -  Go Service that provides user permission information. (e.g. “Is Editor”)

- **HATS** - the Twitch Entitlement Service.

- **Lambdwich** - Repo that contains code for lambdas that are used by subscription services. 

- **Mako** - the Twitch emoticon service. Its endpoints allow retrieving a user's emoticon sets and selected smilies, entitling or disentitling an emoticon set to a user, creating new emoticons, and more. See [Github Repo](https://git.xarth.tv/commerce/mako)

- **Multi-month Subscription** - A multi-month subscription is a user who is not subscribed to the channel is committing to more than 1 month subscription.

- **Nitro** -  Go Service that provides Prime and Turbo status

- **Pachinko** - Bit’s generic balance tracker service

- **Patcher** - Bit’s async accounting service

- **Project Andes** - Bits long-term architecture initiative. 

- **Project Chevron** - Initiative to define a commerce-wide long-term architecture. 

- **Pushy** - Twitch's notifications system. Pushy manages all async notifications to users. See [Github Repo](https://git.xarth.tv/chat/pushy)

- **Reconcilliation** - Asynchronous Process of checking that the payout events that have been submitted can be traced back to their original transactions

- **RTB** - "Run The Business"

- **SiteDB** - Twitch's main database. Slowly getting abstracted out and used less. 

- **SUBtember** - Twitch's annual marketing campaign in September, which focuses on promoting subscriptions. 

- **Subscription Service** - This is the core service which provides APIs and workers for the majority of subscription's use cases like subscription products, ticket entitlements, streak tenure, gifting, first-time and resub chat notifications, subscriber badges, subscriber score, subscription emote prefix, pending emote approvals, mobile subs, gift settings, etc.

- **Stacked Subscription** - A stacked subscription is a user already has a subscription to the channel is committing to more months to that channel.

- **Streak Tenure** - Defines the total consecutive months a user has subscribed to a channel

- **Tokenization** - Commerce project to enable users to pay for a Subscription via a concept of a Token. See [design doc](https://docs.google.com/document/d/132xpJBmxyLPyYF-ZFl2bhUMkR3a5je1qdZV2zIr41QI/edit)

- **Token Payout Revenue** - The post-promotion, post-taxes & fees value of a token.

- **Twilight** - Main Twitch frontend repo, built in React. See [Github Repo](https://git.xarth.tv/twilight/twilight)

- **User Service** - Users Service provides property access and business logic for core "user" properties.

- **Valhalla** - This rails service primarily hosts subscription's Kraken third-party APIs, admin panel APIs and few other important subscription APIs like tenure calculation. [See Github repo](https://git.xarth.tv/revenue/valhalla)

- **Visage** - Twitch's API gateway (authentication, rate-limiting, service response aggregation). See repo [here](https://git.xarth.tv/edge/visage)

- **Vinyl** - Go Service that provides VOD metadata

 




