---
title: "Accessing the AWS Console"
description: "how to access our AWS accounts"
weight: 3
---

### How It Works ✨ (Optional Reading)

Essentially, every time you need to get some AWS creds, you do a midway auth which stores a cookie on your machine. 
You then use that auth to curl the Isengard service which gives you short lived AWS creds tied to a role. 
The curling is simply handled in a script so you don’t have to remember a gnarly command line. 
[Here’s the curl command][uglycurl] for those of you who like to know how the sausage gets made. 

[uglycurl]: https://w.amazon.com/index.php/AWS_IT_Security/Isengard/UserGuide#Use_Isengard_API_with_NextGen_.28YubiKey.29_Authentication

The ~/.aws/credentials file can accept a `credential_process` param that just points to the script and sets the account and secret environment variables that get used the same way as the hard coded ones you have in there now.


### What You Need To Install 🛠️
Make sure you have [mwinit](https://w.amazon.com/bin/view/NextGenMidway/UserGuide#Mac) installed (TL:DR; it’s a `brew` package).
Install the [isengard_credentials](https://git.xarth.tv/twitch/isengard_credentials) script and add it to your local path. 
Modify the relevant entries in your ~/.aws/credentials file to look like the following...

```bash
[twitch-subs-aws]
region = us-west-2
credential_process = isengard_credentials --account-id 522398581884 --role admin
[twitch-subs-dev]
region = us-west-2
credential_process = isengard_credentials --account-id 958836777662 --role admin
```

After you’ve successfully installed those things, you should be able to go about your console work normally. 
If something you regularly do isn’t working, you probably just have to add the `AWS_PROFILE=twitch-subs-aws AWS_SDK_LOAD_CONFIG=1` env variables to prefix the command. 
The `AWS_SDK_LOAD_CONFIG` flag is there to tell the aws-go-sdk to use the Isengard creds. Long term, we’ll add these in to the various makefiles to make life easier. 
If you’re making Terraform changes, make sure you’re using v0.11.14 or higher.


{{% notice note %}}
**Pro Tip**: You can use `mwinit --aea` to get Isengard creds while not on WPA2   
{{% /notice %}}

### It's Not Working 
In some instances (like s2s-sidecar tool) credential_process=isengard_credentials isn't working. 
In these cases you can still get the credentials from isengard and use them non-programatically with [this shell function](https://git.xarth.tv/gist/cthedark/8c8a73bcb27388f38841b03a85a3420a). 
You can copy/paste to your ~/.zshrc or any shell rc you use, then invoke it like `buildAWS twitch-subs-dev` and your [default] profile will be using the temporary cred from isengard.

```bash
➜  ~ buildAWS twitch-subs-dev
➜  ~ aws s3 ls
2018-06-06 15:56:05 958836777662.us-west-2.lambdwich-ghe-artifacts
2018-06-06 15:56:05 958836777662.us-west-2.lambdwich-pipeline-deploy-artifacts
... (all the s3 buckets in subs dev account)
```



