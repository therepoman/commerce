---
title: "ECS"
description: "ECS Informational"
date: 2020-05-01T08:33:08-07:00
weight: 1
---
## Intro

ECS is an AWS service used to schedule Docker containers. We use it to power a number of services and workers, namely:

- Chronobreak
- Subscriptions


There are two variants of ECS:

1. EC2
2. Fargate


EC2 variant requires that you manage a cluster of EC2 instances and ECS will manage scheduling containers on them.
Fargate allows you to let AWS manage the underlying compute and so you don't need to manage EC2 instances.


We currently use the EC2 variant and we have a single, shared cluster in both *twitch-subs-aws* and *twitch-subs-dev*.


### Sonar Cluster

*Ensure you're already federated with isengard for either the staging or production AWS account so the links work*

- [Sonar ECS Dashboard](https://us-west-2.console.aws.amazon.com/ecs/home?region=us-west-2#/clusters/sonar/services)
- [Sonar Autoscaling Group](https://us-west-2.console.aws.amazon.com/ec2/autoscaling/home?region=us-west-2#AutoScalingGroups:id=sonar;view=details)
- [Sonar Launch Configuration](https://us-west-2.console.aws.amazon.com/ec2/autoscaling/home?region=us-west-2#LaunchConfigurations)
- [Cluster Terraform](https://git.xarth.tv/subs/terracode/blob/master/cluster.tf#L12-L23)

#### Launch Configuration

These describe how each EC2 instance will be configured (instance type, security groups, disk, iam, etc...)

The following configurations are added onto a pre-baked ECS AMI (given by AWS):

- ldap: allows sshing into the instances with our LDAP groups.
- ulimit: increase the number of file descriptors so we can open as many files/sockets as we need.

The main components of the AMI are:

- Docker
- systemd
- ECS Agent (runs via Docker + systemd)

The agent always runs at startup and is responsible for taking commands from ECS (start container, stop container, pull docker image, etc...).


#### Autoscaling Group

The launch configuration by itself does nothing. The autoscaling group is responsible for creating new EC2 instances using the launch configuration. The group is autoscaled by CPU and memory usage alarms (via CloudWatch).

Each cluster can set a min/max number of EC2 instances and the group will auto scale within those requirements.


### ECS Task Definitions

[AWS UI](https://us-west-2.console.aws.amazon.com/ecs/home?region=us-west-2#/taskDefinitions)

At the lowest level you have a task definition that describes how to run a single task. A task can run multiple different containers but is usually tied to a single container.

Common task settings:

- Docker image
- Environment variables
- Command to execute
- IAM Role to assume
- Memory/CPU units to use.
- Ports
- Logging
- Volumes
- etc...

To use a task definition you would start a **task** which is an instantiation of a particular task definition.


#### ECS Services

A service is a higher-level abstraction around tasks that:

1. Manage starting tasks and keeping them running (starting new ones if previous ones fail).
2. Register new tasks with the load balancer while removing old tasks from it.
3. Allow autoscaling based on CloudWatch alarms.

## Subscriptions Service 

This is the main dashboard for the subscriptions ECS service.

![subs service ui](../../images/ecs/subs-service-selected.png)


The two highlighted areas are important to know.


#### Container Count

This determines how many containers ECS will try and run and how many are actually running. 

The desired count and the running count should be exactly the same, unless a deployment is currently running.

**When are they different?**

1. When a deployment is running there will be more *running* tasks than *desired* tasks.
2. When tasks fail to start or start crashing you'll see there aren't enough containers and ECS will continue to try and
   start more until the *desired* count is met.
   
   
Tasks not starting or crashing can happen for a number of reasons:

1. The container image/tag cannot be pulled. Either it doesn't exist or ECS doesn't have permission to pull from ECR (the container registry).
2. IAM permissions are missing.
3. Task configuration is incorrect.
4. Container is crashing/stopping. Panics in Go can cause this.

#### Task Definition

This is essentially the version that the service is running on and each version is a collection of configurations, docker containers, etc...

You're able to see which container tag is running (we use git shas as our tags) by clicking on the task definition link.

## Subscriptions Task Definitions

This is the main dashboard for the subscriptions task definition. 

![subs service ui](../../images/ecs/subs-service-task-def.png)

- **Task Role:** This is the IAM role that the task and all containers assume. If you want to give the container access to a DynamoDB table or other AWS resource you would attach the permissions to this role. This role is completely managed by Terraform.
- **Compatibilities:** Determines whether this task is EC2-based or Fargate.

---

#### Container Configuration

At the bottom of the task definition page you'll find the most important pieces to configure: containers.

![subs service ui](../../images/ecs/subs-service-task-def-imp.png)

- **Task Memory:** Defines the maximum memory the task can use. 
- **Task CPU:** Defines the maximum CPU the task can use. 1024 units equal to 1 CPU core.


Each container can then have further constraints on the task memory and cpu but cannot go over the total defined for the task.

Next, the container image is defined and the container has "Essential = true" which means if it stops, the entire task also stops.


Logging is done via the `awslogs` driver that sends logs to CloudWatch automatically.

Here's the logging configuration in the subscription service task definition:

```json
{
 "logConfiguration": {
    "logDriver": "awslogs",
    "options": {
      "awslogs-group": "subscriptions-service",
      "awslogs-region": "us-west-2",
      "awslogs-stream-prefix": "subscriptions-service"
    }
  }
}
```

All the logs are streamed to the `subscriptions-service` CloudWatch log group. Each container is a separate stream in the group.

## Viewing Logs 

The quickest way to view the logs is to go to the ECS service dashboard and there's a "Logs" tab.

![subs service ui](../../images/ecs/subs-service-logs.png)

This view aggregates all the logs from every container and it's nice way to see recent logs.

You can also go to the CloudWatch dashboard directly to have a bit more searching power.

## Logs via the CLI

Sometimes you want to stream logs in your terminal (either real-time or historical logs). This allows you to use common tooling like `grep` to search.

```bash
go get github.com/segmentio/cwlogs
```

**Production:**

```json
AWS_REGION=us-west-2 AWS_PROFILE=twitch-subs-aws cwlogs
```

**Staging:**

```json
AWS_REGION=us-west-2 AWS_PROFILE=twitch-subs-dev cwlogs
```

You may want to add these as aliases in your `~/.zshrc` or `~/.bashrc` or `~/.bash_profile`


```
alias logsprod='AWS_REGION=us-west-2 AWS_PROFILE=twitch-subs-aws cwlogs'
alias logsstage='AWS_REGION=us-west-2 AWS_PROFILE=twitch-subs-dev cwlogs'
```

Now you can stream logs:

```bash
logsprod fetch subscriptions-service -v -f --since=10m
```

- `-f` will stream the logs as they come in.
- `-v` will give verbose logs (shows the structured logging)
- `--since` accepts a relative time unit (e.g., `10m`, `1h`, `1d`)


You can also pipe the command to grep, for example.

```bash
logsprod fetch subscriptions-service -v -f --since=10m | grep ERR
```

This is a quick way to stream errors that are produced and is a great way to debug in production/staging.


## Troubleshooting
ECS is not very surprising with its behaviour. There are straightforward ways to debug issues and fix them.


#### `Running Count < Desired Count`

The main culprit is tasks being stopped and while ECS will continue to start more tasks, the service will likely
not have the desired number of containers running.

You'll want to view all the "Stopped" tasks to debug further.

![subs service ui](../../images/ecs/subs-service-task-stopped.png)

Clicking on a stopped task will usually tell you why it stopped.

![subs service ui](../../images/ecs/subs-service-task-stopped-detailed.png)


- *Status*: if ECS was unable to start the task there will be further details after "STOPPED". For example, if the container image cannot be pulled, the tag doesn't exist, etc...
- *Details*: This will show you the exit code. In this case the task was stopped manually and we just get an exit code. If a container **crashed** the reason of "essential container stopped" will be provided.


**How to debug?**

1. Make sure the container isn't panicing. Go panics will terminate the process and container if they happen in the main goroutine. These will be visible in the logs.
2. Ensure ECS is able to place tasks on the cluster. The "Events" tab in the ECS service dashboard will show you what ECS has tried to do. 
    - The cluster may be at full capacity to schedule more tasks. ECS will wait until the EC2 autoscaling group adds more instances.
    - There's an edge case where the cluster isn't at full capacity but its CPU/memory units are fully reserved. This happens if all the tasks are reserving all the available units but isn't actually using them. The EC2 autoscaling group **won't autoscale**. To fix we would need to lower the task sizes so they aren't reserving more than they use or manually tell the autoscaling group to add more instances.


