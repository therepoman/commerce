---
title: "Domain Knowledge (tickets)"
description: "Knowledge of Subs Product"
date: 2020-05-01T08:33:08-07:00
weight: 5
---

{{% notice info %}}
Some of this may become outdated with Project Octane - come back and review as appropriate. 
{{% /notice %}}

## Tickets

**What is multi-month subscription?**
A multi-month subscription is a user who is not subscribed to the channel is committing to more than 1 month subscription.
Example: Bobby doesn’t have a subscription to loltyler1, and uses 5 sub tokens to purchase a 5-month subscription.

**What is a stacked subscription?**
A stacked subscription is a user already has a subscription to the channel is committing to more months to that channel.
Example: Bobby already has a subscription to loltyler1, and uses 2 sub tokens to extend his current subscription by 2-months.

**What is a non-recurring subscription?**
A non-recurring subscription is a subscription that does not renew.

**What is a recurring subscription?**
A recurring subscription is a subscription that renews every month

**What is an active ticket?**
An active ticket is ticket where it’s start date is less than now, and it’s end date is greater than now. The existence of an active ticket for a user means they have a subscription. 

**What is a future dated ticket?**
A future dated ticket is a ticket that has a start date and end date after now. When a user wants to convert a nonrecurring subscriptions to a recurring subscription. Payments will call Subs to create a future dated ticket with an interval of 3 days (dunning period). An attempt to charge the user at the start of the dunning period will occur until successful or the dunning period fails. On success, the future dated ticket will be extended to a full month ticket and will be now considered an active ticket

## Ticket Products

See [ticket products and onboarding](../../operations/ticket-products-and-onboarding). 