+++
title = "Lunch & Learn"
date = 2020-05-01T08:24:03-07:00
weight = 2
chapter = true
+++

### Chapter 2

# Lunch & Learn

{{% notice info %}}
In the spirit of inclusivity, the term "Brown Bags" have been replaced by "Lunch and Learn". 
[Learn more](https://www.nbcnews.com/news/us-news/theres-more-seattle-brown-bag-racial-controversy-meets-eye-flna6C10836263) 
{{% /notice %}}

Helpful tips about common technologies in use at Twitch.  
