---
title: Database Recovery
description: "data recovery db"
date: 2020-05-01T08:33:08-07:00
weight: 4
---

## DynamoDB - Overview
As part of [SUBS-1270](https://jira.twitch.com/browse/SUBS-1270), I have enabled [Point-in-time Recovery](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/PointInTimeRecovery.html) for all of our DynamoDB tables.
This will automatically create backups of our DynamoDB table data, which can be restored to any point in the last 35 days. 
When you restore using Point-in-time Recovery and select a recovery point, DynamoDB restores your table data to the state based on the selected date and time (day:hour:minute:second) **to a new table**. 
Other table settings like indexes, provisioned read and write capacity, and encryptions settings will also be included in the new table. 
However, auto scaling policies, IAM policies, CloudWatch metrics/Alarms, and Time to Live settings **will need to be manually setup**. 
For you can view the full details for Point-in-time Recovery [in the developer guide](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/PointInTimeRecovery_Howitworks.html).

## DynamoDB - Restore time
The time it takes you to restore a table will vary based on multiple factors, and the point in time restore times are not always correlated directly to the size of the table.
Here are some of the considerations for restore times: 
1. You restore backups to a new table. It can take up to 20 minutes (even if the table is empty) to perform all the actions to create the new table and initiate the restore process.
2. For tables with even data distribution across your primary keys, the restore time is proportional to the largest single partition by item count and not the overall table size. For the largest partitions with billions of items, a restore could take less than 10 hours.
3. If your source table contains data with significant skew, the time to restore may increase.

This information was taken from [AWS Documenation](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/backuprestore_HowItWorks.html).

## DynamoDB - Restoring
Since you cannot restore the data to the orginal table (and have to create a separate table with a different name), we will need to make two changes in the event of large scale data problems:
1. Using the instructions in the [Point-in-time Recovery guide](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/PointInTimeRecovery.Tutorial.html), restore the table data to the correct point in time. This will create a new table to hold the restored data. 
2. Make changes in SUBS/Valhalla/Mako/Other API's to call the new table name.  

Although having to create a new table and then updating the other services is not ideal, it does allow us to compare the bad data on the original table with the restored data.


## Amazon RDS Aurora - Overview
As part of [SUBS-1270](https://jira.twitch.com/browse/SUBS-1270), I have enabled automatic backups of Amazon RDS Aurora for `subscriptions-production-cluster`. 
These backups will be retained for 35 days after creation. 
You can view more information about automated backups in the [AWS Documentation](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_WorkingWithAutomatedBackups.html).
Similarly to DynamoDB, you can't restore data to the existing DB instance, and will have to provide a new name for the recovered data. 
It appears that all security groups changes will have to be set on the new created DB instance based on the [tutorial here](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_Tutorials.RestoringFromSnapshot.html).

## Amazon RDS Aurora - Restoring
Since you cannot restore the data to the orginal table (and have to create a separate table with a different name), we will need to make three changes in the event of large scale data problems:
1. Using the instructions in the [AWS Documentation](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_RestoreFromSnapshot.html), create a new DB from the backup snapshot
2. Ensure that all security/configuration settings have been set correctly on the new DB instance
3. Make changes in SUBS/Valhalla/Mako/Other API's to call the new table name.  
