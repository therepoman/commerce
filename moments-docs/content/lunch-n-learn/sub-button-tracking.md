---
title: Sub Button Tracking
description: "sub-button-tracking"
date: 2020-05-01T08:33:08-07:00
weight: 3
---

TODO: Add general description of tracking

TODO: Add some links about tracking in twilight

TODO: Add some links to help making reports

## How to see front end spade logging
When a tracking event is fired, twilight logs the event in the javascript console. You can use this to see the fields sent with each tracking call.

1. Open the javascript console. Chrome: `Cmd + Option + i` will open the chrome dev tools click on the `Console` tab at the top of the dev tools window.
2. Right below the tabs there will be a dropdown labeled `Default levels`. Click that and change it to `Verbose` which will enable a lot of logging. The text on the dropdown should now say `All levels`.
3. In the search box to the left of the levels dropdown you can filter log statements. To see just spade tracking calls you can put `[spade]` into the box. To see the subscribe button events specifically, you can put `subscribe_button`.

You should now see tracking events appear when clicking the subscribe button.

## Event
The spade tracking event for the subscribe button is `subscribe_button`. In ace the backing tables are named after the event so if you want to query for these events they will be in the `subscribe_button` table. This is sent in the `event` field on the tracking payload. This event is used anywhere the user has a flow to checkout for a subscription. Currently that includes the on channel subscribe button, hosting subscribe button, emote picker subscribe button, viewer card gift a sub button. 

## Actions
The `subscribe_button` event has many actions that are recorded. An action is something the user did to interact with the subscribe button or balloon. This field is sent in the `action` field of the event properties. We are always adding more actions as we add features to the subscribe modal, but here is the current list as of Dec 20, 2018:

| Action | Description | Locations |
| ------ | ----------- | -------- |
| buy_custom_mystery_gift | When the user clicks on the checkout button for a custom sized community gift bundle. | Subscribe Balloon |
| buy_gift_sub | When the user clicks on the checkout button for a single recipient gift subscription. | Subscribe Balloon, Viewer Card |
| buy_gift_to_paid_upgrade | When the user clicks on the checkout button for a gift to paid upgrade subscription. | Subscribe Balloon |
| buy_mystery_gift | When the user clicks on the checkout button for a preset community gift bundle. | Subscribe Balloon |
| buy_continue_sub | When the user clicks on the checkout button for a DNR to resub subscription | Subscribe Balloon |
| buy_prime_to_paid | When the user clicks on the checkout button for a prime to tier 1 subscription | Subscribe Ballon |
| change_tier | When the user selects a different tier tab while looking at all of the subscription tiers. | Subscribe Balloon, Viewer Card |
| click_checkout_button | When the user clicks on the self sub checkout button. | Subscribe Balloon, Emote Picker |
| anonymous_gift_click | When the user checks or unchecks the anonymous gift setting. | Subscribe Balloon, Viewer Card |
| more_options_click | When the user clicks on the More Paid Subscription Options button which takes them to see all sub tiers for the channel. | Subscribe Balloon |
| gift_options_click | When the user clicks on the Gift a Subscription button within the sub balloon which takes them to the screen that lets them select community vs single gift. | Subscribe Balloon |
| back_top_level | When the user clicks on the back button inside the subscribe balloon. | Subscribe Balloon |
| close_subscribe_menu | When the user clicks on the sub button or outside of the balloon to close the sub balloon. | Subscribe Balloon |
| gift_a_sub | When the user clicks on the Gift a Sub button to go to the single gift recipient selection. Or when the user clicks on Gift A Sub in the chat Viewer Card | Subscribe Balloon, Viewer Card |
| open_subscribe_menu | When the user clicks on the sub button to open the subscribe balloon. | Subscribe Balloon |
| samus_spend_credit | When the user clicks the button to use their prime sub token. | Subscribe Balloon |

## Fields
Many of the fields we send with the event are added automatically by the twilight client while some of them are set specifically in our tracking call. The Controlled by subs team column will be checked if we set the field specifically and it will be unchecked if the field is sent by the twilight spade client.

| Field | Description | Type | Nullable | Controlled by subs team |
| ----- | ----------- | ---- | --------: | -----------------------: |
| action | The action taken by the user that triggered the event. See the table in the Actions section above. | string | | X |
| button_cta | The CTA text currently shown to the user. This will be the english equivalent of whatever text the user saw. Null if the action was not triggered from the subscribe button. | string | X | X |
| can_subscribe | A flag indicating if the currently logged in user can spend a prime sub token. | bool | | X |
| channel | The login name for the channel that the user is currently interacting with. | string | | X |
| channel_id | The twuid for the channel that the user is currently interacting with. | string | | X |
| checkout_button_tier | Shows the currently selected tier the user is checking out for. Only used on actions where the user is sent to checkout. Null if the action does not send the user to checkout. | string | X | X |
| has_sub_credit | A flag indicating if the currently logged in user has an available prime sub token. | bool | | X |
| host_channel | The login name for the channel that is currently hosting. This will only come from the hosted subscribe button that appears below the video when the current channel is hosting. Will be null if the sub button is not in the hosted panel. | string | X | X |
| host_channel_id | The tuid for the channel that is currently hosting. This will only come from the hosted subscribe button that appears below the video when the current channel is hosting. Will be null if the sub button is not in the hosted panel. | string | X | X |
| is_anonymous | A flag indicating if the anonymous gifting setting has been turned on. | bool | X | X |
| is_menu_open | A flag indicating if the sub balloon is currently open. | bool | | X |
| is_single_month | A flag indicating if the user has selected the single month DNR to resub option. Null if the user was never given the option. | bool | X | X |
| is_subscribed | A flag indicating if the currently logged in user is subscribed. Null if the user is not logged. | bool | X | X |
| is_subscribed_current_tier | The tier for the current user's subscription. Null if the user is not logged in or not subscribed. | string | X | X |
| mystery_gift_count | The number of community gifts selected by the user. Null if the action is not a community gift checkout action. | number | X | X |
| sub_location | The location within the page where the event comes from. Possible values are: `channel_page`, `emote_picker`, `viewer_card`, `squad_page`, `landing_page`. | string |  | X |
| sub_recipient | The login name for the selected sub gift recipient. Null if this is not a gift checkout action. | string | X | X |
| modal | A flag indicating if the event was within the sub balloon. | bool | | X |
| modal_level | The level or "page" within the sub modal. Possible values are: `top_page`, `second_page` and empty string. | string | | X |
| show_prime_content | A flag indicating that the user was shown prime content. This is usually only false if the user is not in an approved prime region. | bool | | X |
| show_resub | A flag indicating if the user has an active prime sub and is in the window to resub with a new prime token. Null if the current user is not logged in. | bool | X | X |
| vod_id | The id of the VOD the user is watching. Null if the user is not watching a VOD. | string | X | X |
| vod_type | The type of VOD the user is watching. Null if the user is not watching a VOD. | string | X | X |
| viewport_height | The height of the user's browser window. | number | | X |
| viewport_width | The width of the user's browser window. | number | | X |
| adblock | A flag for if adblocking was detected on the browser. | bool | | |
| app_version | The client application build id. | string | X | |
| batch_time | Spade events on the client are batched, this field shows the time for the batch. | number | | |
| client_app | The client application. eg: `twilight` | string | | |
| client_time | The current time on the client for the event. | number | | |
| device_id | The id for the device the client is running on. | string | | |
| domain | The domain for the url in the user's browser. | string | | |
| host | The host for the url location in the user's browser. | string | | |
| logged_in | A flag for if the current user is logged in. | bool | X | |
| login | The login name for the current user. | string | X | |
| platform | The platform that the event came from. eg: `web` | string | | |
| preferred_language | The preferred language for the current user. | string | | |
| received_language | The language the user is seeing on the page. | string | | |
| referrer_host | The host of the referring page, only used when the user navigates to twitch from outside of the site. | string | | |
| referrer_url | The url of the referring page. | string | | |
| tab_session_id | The session id for the tab that is running the twitch site. | string | | |
| url | The full url for the current twitch page. | string | | |
| user_id | The twuid for the currently logged in user. | number | X | |
