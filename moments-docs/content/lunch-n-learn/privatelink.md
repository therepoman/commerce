---
title: "PrivateLink/VPC Endpoints"
description: "Allow services to communicate with each other."
date: 2020-05-01T08:33:08-07:00
weight: 2
---
## Introduction

PrivateLink/VPC Endpoints is a way for services in AWS to communicate with each other across VPCs.


![pic](https://d1.awsstatic.com/product-marketing/PrivateLink/privatelink_how-it-works.a8ae3df6830296337b30a7c4e75d8eed403eb5d2.png)


This allows services to be exposed individually to whitelisted consumers. Moreover, PrivateLink is designed to scale beyond the limits of VPC Peering.


### Endpoint Services

Endpoint services are created by exposing a load balancer (currently only NLB - Network Load Balancers are supported).

Endpoints can then be created in another AWS account (by the consumer of the service) if they're properly whitelisted.

Endpoints expose an AWS provided DNS that routes traffic directly to the load balancer previously exposed.

```
# Example Endpoint DNS
vpce-05ad2de450c3cb65f-msufwnu5.vpce-svc-083aaf6987e72bd53.us-west-2.vpce.amazonaws.com 
```

This works great for HTTP traffic but doesn't work for SSL as the provided DNS would never match the SSL certificate.

#### Modern DNS

Twitch is moving towards a more decentralized DNS solution. Each AWS account will have zones for each of the services they host and all the services they consume.

Each service that is hosted in an AWS account will need a public Route53 zone that is delegated to us by Twitch (e.g., destiny.twitch.a2z.com). A public zone is needed so that we can perform SSL certificate validation via DNS. This zone isn't used beyond that.

Each service that is consumed within an AWS account (for example, Mako could consume the Subscriptions service) would require a private Route53 zone.


Let's take Subscriptions service as an example.

**Route53 Zones:**

| Type    | Zone                         |
|---------|------------------------------|
| Public  | subscriptions.twitch.a2z.com |
| Private | chronobreak.twitch.a2z.com   |
| Private | tmi.twitch.a2z.com           |
| Private | zuma.twitch.a2z.com          |

The public zone is used for SSL certificate validation via DNS.
Each of the private zones are used for every consumer we use so that we can have SSL-working DNS names.


## Adding PrivateLink to a Service


#### Creating a new Route53 Zone

The `privatelink-zone` module will a public Route53 zone using the new DNS standards set within Twitch. Namely, services should use the following format for DNS:


| Environment | DNS |
|---------|------------------------------|
| Production | main.us-west-2.prod.{service}.twitch.a2z.com |
| Canary | canary.us-west-2.prod.{service}.twitch.a2z.com   |
| Beta/Staging | main.us-west-2.beta.{service}.twitch.a2z.com |

```
module "privatelink-zone" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-zone"

  name        = "destiny"
  environment = "${var.environment}"
}
```


After the Terraform has been applied you'll need to grab the nameservers for each zone (in production and staging environments). Twitch needs to delegate the zone to you and this is done via an [IPSYS Jira ticket](https://jira.twitch.com/browse/IPSYS) with the System team.


You can open a ticket with the following template:

```
AWS Account: {production account number}
Zone: prod.{service}.twitch.a2z.com.
Nameservers: {4 nameservers}
TTL: 172800
Test Record Name: _test.prod.{service}.twitch.a2z.com.
Test Record Value: "Test record"

AWS Account: {staging account number}
Zone: beta.materia.twitch.a2z.com.
Nameservers: {4 nameservers}
TTL: 172800
Test Record Name: _test.beta.materia.twitch.a2z.com.
Test Record Value: "Test record"
```

Simply replace anything in the `{}` brackets and leave everything else the same. You can find the nameservers in the Route53 dashboard for each zone.


![route53 ui](../../images/route53.png)


After the zone is delegated you should be able to run the following:

```
dig -t TXT _test.prod.{service}.twitch.a2z.com
dig -t TXT _test.beta.{service}.twitch.a2z.com
```

### Creating SSL Certificate

Next you'll need to create a certificate that can be validated via DNS.

```
module "privatelink-cert" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-cert"

  name        = "destiny"
  environment = "${var.environment}"
  zone_id     = "${module.route53-zone.zone_id}"
}
```

This will create the certificate via ACM and will automatically create a record in the public Route53 zone to validate it. It may take up to 30 minutes for validation so the Terraform apply will wait until it's done.

### Creating PrivateLink Service Endpoint


```
module "privatelink" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink"

  name             = "destiny"
  region           = "${var.region}"
  environment      = "${var.environment}"
  alb_dns          = "${module.service.dns}"
  cert_arn         = "${module.privatelink-cert.arn}"
  vpc_id           = "${var.vpc_id}"
  subnets          = "${split(",", var.subnets)}"
  whitelisted_arns = []
}
```

This uses the certificate in the last step to attach to the load balancer. This will make the service available over PrivateLink. You just need to provide an existing ALB.


# Consuming a PrivateLink Service

Connecting to a service via PrivateLink is done entirely within a VPC, including DNS.


Steps:

1. Establish a VPC Endpoint connection with the PrivateLink service.
2. Create Route53 Private Zone for each connecting service and create a record pointing to the new
   connection.
3. Configure the VPC to use AWS DNS instead of Twitch DNS.


### Consuming a PrivateLink Service via Terraform

You just need to grab the PrivateLink service endpoint (e.g., `com.amazonaws.vpce.us-west-2.vpce-svc-083aaf6987e72bd53`) and the `privatelink-service` module will automatically handle the rest for you.

```
module "privatelink-destiny" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-service"

  name            = "destiny"
  endpoint        = "com.amazonaws.vpce.us-west-2.vpce-svc-083aaf6987e72bd53"
  environment     = "${var.environment}"
  security_groups = ["${var.security_group}"]
  subnets         = "${split(",", var.subnets)}"
  vpc_id          = "${var.vpc_id}"
}
```

DNS records within Route53 will be created that you can use to connect to the service over SSL.

![route53 ui](../../images/route53-2.png)

In the example above, you would use `https://main.us-west-2.beta.destiny.twitch.a2z.com` to communicate with the Destiny service (in the staging/beta environment).


## Configuring VPC DNS

The last step is to configure the VPC to resolve DNS within the VPC instead of depending on Twitch's DNS infrastructure.


```
module "vpc-endpoint" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//route53-resolver"

  security_group = "${var.security_group}"
  environment    = "${var.environment}"
  subnets        = "${split(",", var.subnets)}"
  vpc_id         = "${var.vpc_id}"

  ip_addresses = [
    "10.204.232.126",
    "10.204.233.37",
    "10.204.234.157",
    "10.204.233.199",
  ]
}
```

You'll need **4 ip addresses** that are used for Twitch's DNS. You can find them in the VPC -> DHCP dashboard.

![vpc dhcp ui](../../images/vpc.png)


Once the Route53 endpoints have been created you'll need to change which DHCP set is configured with the VPC.


![vpc dhcp ui](../../images/vpc-2.png)


In this example, the VPC is assigned to the labeled DHCP options set (`twitch-destiny-dev`). This options set uses the Twitch DNS which we want to change.

The unlabeled set should be the default set created by AWS that uses its own DNS servers. You'll want to keep note of the DHCP options set ID (`dopt-062d8b7e` in the above screenshot).

![vpc dhcp ui](../../images/vpc-3.png)

You'll want to go over to the VPC dashboard and select the VPC. Under the `Actions` dropdown you want to click on `Edit DHCP options set`. Select the DHCP ID from the previous step and hit save!


