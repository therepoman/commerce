---
title: "Moments Docs Portal"
date: 2020-05-01T08:17:01-07:00
---

# Moments Docs Portal

Welcome! If you a new developer on Moments, please have a look at the [Getting Started](getting-started) section.

For On-call issues, please take a look in the [Operations](operations) section for the service in question. 

To contribute to these docs, please have a look at [Contributing](getting-started/contributing).

For information about other commerce tribes, please see the [Commerce Wiki](https://wiki.twitch.com/display/COMRCE/Commerce+Home). 

{{% notice tip %}}
**Reminder:** Mindshare is the key to a successful team!
{{% /notice %}}
