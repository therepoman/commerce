---
title: "Octane ⛽"
description: "octane - the subs chevron migration"
---

Octane is the subscriptions chevron migration plan. The original plan may be found [here](https://docs.google.com/document/d/1_C0QCVM8HaRDEr-znU6v03MgrBPpLs8Wzn0t8nnRS2Q/edit?usp=sharing).


Most milestones in the Octane migration follows a similar pattern to one another: Create a new datastore, dual-write to the new datastore, backfill, switch over the primary datastore, then cleanup. Here’s a list of all the in-progress/completed milestones:
- [M1 - Emoticon flow improvement](https://docs.google.com/document/d/1WWTxvtHE5MKMu9iDCnlv6P9_IpPI53P8oVTsHyEGSj8/edit?usp=sharing)
- [M3](https://docs.google.com/document/d/1EDo7B1fDKJzTRYEZCLbJVc4Kqi6TiJAgArsWu91wq8U)
- [M4](https://docs.google.com/document/d/1QlipYkirLZhWr_-7wKg8NP4sL_ZJpYwSaPSonpNxROM/edit#)
- [M5](https://docs.google.com/document/d/1mkK_WGmuhnegWFt9BCJc5D7y0ucjXpi0CBhZ1jQb1nY)
- [M6](https://docs.google.com/document/d/1YmyD5dWwv4pdEPgTDC8ww_0cilM1jhclXfgeqFGMuRs)
- [M7](https://docs.google.com/document/d/1FMxJwhZ-fgxCSYyEUy_uCVPCWZ8GMI0YIey-dbB0i8U)
    - We're deprecating the Valhalla path for Channel Onboarding ([proposal doc](https://docs.google.com/document/d/1FMxJwhZ-fgxCSYyEUy_uCVPCWZ8GMI0YIey-dbB0i8U/edit#)). 
    - Instead, we're going to onboard new Channel Subscriptions via a Step Function, which calls into Chevron components (ProductCatalog, BenefitsService, Materia, etc). 
    - We will "dual-onboard" streamers until Milestone 8 of Octane. After Milestone 8, we will turn off and deprecate the old Valhalla code path. 
- Helpful backfill scripts for the migrations can be found here: [subs/octane-scripts](https://git.xarth.tv/subs/octane-scripts)
