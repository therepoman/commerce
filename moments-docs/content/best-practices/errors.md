+++
title = "Go: Errors"
date = 2020-10-22T08:24:38-07:00
weight = 4
chapter = false
+++

## Wrapping Errors

A common practice is to wrap errors as they propagate back to the caller:

```golang
func callFoo() error {
    if err := callBar(); err != nil {
        return errors.Wrap(err, "failed to call bar")
    }
}

func callBar() error {
    // ...
    return errors.Wrap(err, "failed in bar")
}
```

However this is not always a best practice. A large amount of wrapping involves repeating the same information resulting in errors that look like:

> failed Bar api: failed bar write: failed redis bar setting key: failed sitedb operation: context timeout

Instead the best practices are to selectively wrap errors and let the stack trace handle the rest:

1. Don't wrap every error. Propagating the original error up to the caller is usually the best choice.
2. Wrap errors when using 3rd-party libraries or the standard library. This ensures you establish stack traces within your application's code and starts an error that you can easily reference back to.
3. Never use `fmt.Errorf` or `errors.WithMessagef`. Only use `errors.New` equivalents.
4. Always try and return errors to the caller instead of logging it. And never do both. This isn't always possible when a goroutine is involved; it would be perfectly acceptable to log an error there.

**#4 Example: Double handling errors**

```golang
func foobar() error {
    // ...
    logrus.WithError(err).Error("failed because of x")
    return errors.Wrap(err, "failed because of x)
}
```

It's almost always better to simply return the original error:

```golang
func foobar() error {
    // ...
    return err
}
```

## Twirp Errors

With the release of Twirp Interceptors we can simplify how twirp errors are returned. Interceptors allow you to build middleware that mutate the request or response within Twirp. Subscriptions service, for example, has an interceptor that attaches the original request to errors before logging them.

Previously services would do something like:

```golang
return nil, twirp.InternalErrorWith(errors.Wrap(err, "Error getting template SKU")).
    WithMeta("GetExternalProductTemplateSKURequest", req.String())
```

This is not recommended for a few reasons:

1. Attaching a request to each error with `WithMeta` isn't needed with interceptors and clutters the metadata sent to Rollbar.
2. Wrapping at the Twirp level isn't needed and actually hurts error readability. The stack trace will let you know which line in the Twirp method that failed.

**The recommended style:**

```golang
return nil, twirp.InternalErrorWith(err)
```
