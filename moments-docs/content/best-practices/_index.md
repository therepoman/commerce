+++
title = "Best Practices"
date = 2020-05-01T08:24:38-07:00
weight = 5
chapter = true
+++

### Chapter 5

# Best Practices

This will likely be focused on Go best practices to try and level-up the team with how we write Go code.

{{% notice tip %}}
This section needs your help! If you see a page that is light on details or could be improved, please update it! 
[Contributing](../getting-started/contributing) to this doc portal is easy! 🙏
{{% /notice %}}
