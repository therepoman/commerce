# Moments Documentation Portal

[Portal Homepage](https://git.xarth.tv/pages/commerce/moments-docs/docs/)

This repo stores a number of docs related to Moments Commerce team. Information in this portal is mostly related to operations, new hire guides, project docs, and best practices. Want to dig in? Click the link above to go to the portal!

As members of this team, it is our responsibility to keep this portal up-to-date with useful information.

# Releasing

Run `make publish` locally.

# Making updates
See instructions [here](https://git.xarth.tv/pages/commerce/moments-docs/docs/getting-started/contributing)
