# boba
![alt text](https://git-aws.internal.justin.tv/commerce/boba/blob/master/boba-tea-fett-prints.jpg)

## Development

### Permissions
1. Add yourself as an IAM user to boba+dev@amazon.com with the `AdministratorAccess` policy.
2. Add yourself as an IAM user to boba+prod@amazon.com with `AdministratorAccess` policy.

### Named Profiles
Add the following named profiles to your `~/.aws/credentials` file:
```
[boba-prod]
aws_access_key_id = YOUR_PROD_ACCESS_KEY
aws_secret_access_key = YOUR_PROD_SECRET_KEY

[boba-dev]
aws_access_key_id = YOUR_DEV_ACCESS_KEY
aws_secret_access_key = YOUR_DEV_SECRET_KEY
```
