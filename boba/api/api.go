package api

import (
	"log"
	"net/http"

	"code.justin.tv/foundation/twitchserver"
	"goji.io"
	"goji.io/pat"
)

// Server struct
type Server struct {
	*goji.Mux
}

// NewServer defines handlers
func NewServer() (*Server, error) {
	server := twitchserver.NewServer()

	twitchserver.AddDefaultSignalHandlers()
	s := &Server{
		server,
	}

	// Add your own endpoints here. Endpoint implementation should be defined as functions on Server.
	// For example: s.HandleFuncC(pat.Get("/my/path"), s.myEndpoint)
	// https://godoc.org/goji.io

	s.HandleFunc(pat.Get("/"), s.everythingIsFine)
	s.HandleFunc(pat.Get("/health"), s.everythingIsFine)
	s.HandleFunc(pat.Get("/debug/running"), s.everythingIsFine)
	return s, nil
}

func (s *Server) everythingIsFine(w http.ResponseWriter, r *http.Request) {
	_, err := w.Write([]byte("OK"))
	if err != nil {
		log.Fatal(err)
	}
}
