# CoralLambdaGoSupport
This package provides the codec for Go clients to call Coral lambdas.

The `Config` and `bmg.json` files are purely for Brazil compatability on the Amazon side: https://code.amazon.com/packages/Twitch-Commerce-CoralLambdaGoSupport/trees/master

## Example
See `cmd/example.go` for an example of how to use this codec.
