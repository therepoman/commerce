// Package json is a Coral Marshaler / Unmarshaler for JSON
// Copyright 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
package json

import (
	"code.justin.tv/commerce/CoralGoModel/src/coral/model"
	"encoding/json"
	"fmt"
	"math"
	"math/big"
	"reflect"
	"runtime/debug"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
)

var (
	bigRatType  = reflect.TypeOf(new(big.Rat))
	boolPtrType = reflect.PtrTo(reflect.TypeOf(true))
	timeType    = reflect.TypeOf(&time.Time{})
)

//TODO: Put the code for converting a struct to map[string]interface{} into a common package?
func encodeStruct(val reflect.Value) map[string]interface{} {
	t := val.Type()
	m := make(map[string]interface{})
	if shape, err := model.GetShapeFromType(t); err == nil {
		m["__type"] = shape.FullyQualifiedName()
	}
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		if field.PkgPath != "" {
			continue
		}
		//TODO: This will break if we add other things to the coral tag
		name := field.Tag.Get("coral")
		if name == "" {
			continue
		}
		encodedVal := encode(val.Field(i))
		if encodedVal != nil {
			m[name] = encodedVal
		}
	}
	return m
}

func encodeTime(val reflect.Value) float64 {
	//Format: Seconds.Milliseconds
	timeVal, _ := val.Convert(timeType.Elem()).Interface().(time.Time)
	seconds := timeVal.Unix()
	ms := time.Duration(timeVal.Nanosecond()) * time.Nanosecond
	return float64(seconds) + ms.Seconds()
}

func encodeMap(val reflect.Value) interface{} {
	m := make(map[string]interface{}, 0)
	keys := val.MapKeys()
	if len(keys) == 0 {
		return nil
	}
	for _, key := range keys {
		value := val.MapIndex(key)
		// do Indirect so we can support map[*string]*string
		key = reflect.Indirect(key)
		value = reflect.Indirect(value)
		if str, ok := key.Interface().(string); ok {
			m[str] = encode(value)
		} else {
			panic(errors.Errorf("Unexpected key type %T, want string", key.Interface()))
		}
	}
	return m
}

func encodeSlice(val reflect.Value) interface{} {
	if val.Len() == 0 {
		return nil
	}
	slice := make([]interface{}, val.Len())
	sliceIndex := 0
	for i := 0; i < len(slice); i++ {
		encodedValue := encode(val.Index(i))
		if encodedValue != nil {
			slice[sliceIndex] = encodedValue
			sliceIndex++
		}
	}
	return slice
}

func encode(val reflect.Value) interface{} {
	val = reflect.Indirect(val)
	if !val.IsValid() {
		return nil
	}
	t := val.Type()
	if t.ConvertibleTo(timeType.Elem()) {
		return encodeTime(val)
	}
	switch t.Kind() {
	case reflect.Ptr:
		return encode(reflect.Indirect(val))
	case reflect.Interface:
		return encode(val.Elem())
	case reflect.Struct:
		return encodeStruct(val)
	case reflect.Array, reflect.Slice:
		return encodeSlice(val)
	case reflect.Map:
		return encodeMap(val)
	}
	return val.Interface()
}

func decodeMap(in reflect.Value, t reflect.Type, service string) reflect.Value {
	k := t.Kind()
	if k == reflect.Interface {
		return decodeMapIntoInterface(in, t, service)
	} else if k == reflect.Struct {
		shape, err := model.GetShapeFromType(t)
		if err != nil {
			panic(errors.Errorf("Error finding shape for concrete struct %+v: %s", t, err))
		}
		structType := reflect.ValueOf(shape.New()).Elem().Type()
		returnStruct := decodeMapIntoStruct(in, structType, service)
		// decodeMapIntoStruct **always** returns a pointer to struct,
		// regardless of initial type - we have to dereference it here.
		return returnStruct.Elem()
	} else if k == reflect.Ptr && t.Elem().Kind() == reflect.Struct {
		return decodeMapIntoStruct(in, t, service)
	} else if k == reflect.Map {
		return decodeMapIntoMap(in, t, service)
	}
	panic(errors.Errorf("Unknown Kind: %s, type was %s", k, t))
}

func decodeMapIntoStruct(in reflect.Value, t reflect.Type, service string) reflect.Value {
	var out reflect.Value
	var structT reflect.Type
	if t.Kind() == reflect.Ptr {
		out = reflect.New(t.Elem())
		structT = t.Elem()
	} else {
		out = reflect.New(t)
		structT = t
	}
	for i := 0; i < structT.NumField(); i++ {
		field := structT.Field(i)
		key := field.Tag.Get("coral")
		mapVal := in.MapIndex(reflect.ValueOf(key))
		if mapVal.IsValid() {
			//Key doesn't exist otherwise
			val := decode(mapVal, field.Type, service)
			if val.IsValid() {
				outField := out.Elem().Field(i)
				outField.Set(val)
			}
		}
	}
	return out
}

func decodeMapIntoInterface(in reflect.Value, t reflect.Type, service string) reflect.Value {
	var shape model.Shape
	var err error
	if v := in.MapIndex(reflect.ValueOf("__type")); v.IsValid() {
		var defaultAsm model.Assembly
		defaultShape, err := model.GetShapeFromType(t)
		if err == nil {
			defaultAsm = defaultShape.Assembly()
		} else {
			panic(errors.Errorf("Did not find default assembly for %s: %s", t, err))
		}
		if defaultAsm == nil {
			panic(errors.Errorf("Found shape for %s but it was not associated with an assembly", t.Name()))
		}
		if fqn, ok := v.Interface().(string); ok {
			// If we have been given the service name, then we can look up the assembly
			// corresponding to the __type in the service.  This is useful when an abstract
			// type is defined in one assembly, and the concrete type is defined in another
			// but both are part of the same service.
			if service != "" {
				asmName, _ := splitFqn(fqn)
				// LookupService never returns nil.
				s := model.LookupService(service)
				// Sometimes the type is concrete and isn't from the service (e.g. it
				// is an imported type).  The service assembly will not have the shape
				// in that instance, so test before changing defaultAsm.
				serviceAssembly := s.Assembly(asmName)
				if _, err = serviceAssembly.ShapeFromFQN(fqn); err == nil {
					defaultAsm = serviceAssembly
				}
			}
			shape, err = defaultAsm.ShapeFromFQN(fqn)
			if err != nil {
				// If we don't know the type, we need to error out even though
				// we could use the default type.
				err = errors.WithMessage(err, fmt.Sprintf("error for fqn %s", fqn))
				panic(err)
			}
		}
	}
	if shape == nil {
		shape, err = model.GetShapeFromType(t)
		if err != nil {
			panic(err)
		}
	}
	out := reflect.ValueOf(shape.New()).Elem()
	return decodeMapIntoStruct(in, out.Type(), service)
}

// splitFqn turns the given Fully-Qualified Name into the requisite assembly and
// shape parts.  If there is no split, then only the shapeName will have a value.
func splitFqn(fqn string) (asmName string, shapeName string) {
	index := strings.LastIndex(fqn, "#")
	shapeName = fqn
	if index >= 0 {
		asmName = fqn[:index]
		shapeName = fqn[index+1:]
	}
	return asmName, shapeName
}

func decodeMapIntoMap(in reflect.Value, t reflect.Type, service string) reflect.Value {
	out := reflect.MakeMap(t)
	outElemT := t.Elem()
	outKeyT := t.Key()
	for _, key := range in.MapKeys() {
		v := decode(in.MapIndex(key), outElemT, service)
		k := decode(key, outKeyT, service)
		out.SetMapIndex(k, v)
	}
	return out
}

func decodeSlice(in reflect.Value, t reflect.Type, service string) reflect.Value {
	size := in.Len()
	out := reflect.MakeSlice(t, size, size)
	elemT := t.Elem()
	for i := 0; i < size; i++ {
		val := decode(in.Index(i), elemT, service)
		out.Index(i).Set(val)
	}
	return out
}

func decodeTime(in reflect.Value, t reflect.Type) reflect.Value {
	fractionalSeconds := in.Float()
	sec, ms := math.Modf(fractionalSeconds)
	// This at first glance looks incorrect, but it is correct
	// ms is a fractional second. time.Second is the number of
	// nanoseconds in a second. Multiplying the two together
	// gives us the number of nanoseconds
	nsec := ms * float64(time.Second)
	timeVal := time.Unix(int64(sec), int64(nsec))
	return reflect.ValueOf(&timeVal).Convert(t)
}

func decode(in reflect.Value, t reflect.Type, service string) reflect.Value {
	in = reflect.Indirect(in)
	if in.IsValid() && in.Kind() == reflect.Interface {
		in = in.Elem()
	}
	if !in.IsValid() {
		return reflect.ValueOf(nil)
	}
	if t.ConvertibleTo(timeType) {
		return decodeTime(in, t)
	}

	switch in.Kind() {
	case reflect.Map:
		return decodeMap(in, t, service)
	case reflect.Array, reflect.Slice:
		return decodeSlice(in, t, service)
	default:
		if t.Kind() == reflect.Ptr {
			if in.Kind() == reflect.Float64 &&
				t.AssignableTo(bigRatType) {
				return reflect.ValueOf(new(big.Rat).SetFloat64(in.Float()))
			} else if in.Kind() == reflect.String &&
				t.AssignableTo(boolPtrType) {
				/*
					Sad but true, unfortunately, many of the coral
					serializers serialize bools as json strings
				*/
				val, err := strconv.ParseBool(in.String())
				if err != nil {
					// Serialization error is expected
					panic(err)
				}
				return ptrTo(reflect.ValueOf(val))
			} else {
				return ptrTo(in.Convert(t.Elem()))
			}
		}
		return in.Convert(t)
	}
}

func Marshal(obj interface{}) (b []byte, err error) {
	defer func() {
		if errObj := recover(); errObj != nil {
			err = errors.Errorf("Error: %v.\nStack Trace: %s", errObj, string(debug.Stack()))
		}
	}()
	if obj == nil {
		return []byte(""), nil
	}
	val := reflect.Indirect(reflect.ValueOf(obj))
	if val.IsValid() && val.Type().Kind() != reflect.Struct {
		return nil, errors.Errorf("Cannot marshal top-level obj that isn't a struct. Kind was %v", val.Type().Kind())
	}
	m := encode(val)
	return json.Marshal(m)
}

func Unmarshal(data []byte, obj interface{}, service string) (err error) {
	defer func() {
		if errObj := recover(); errObj != nil {
			err = errors.Errorf("Error: %v.\nStack Trace: %s", errObj, string(debug.Stack()))
		}
	}()

	var m map[string]interface{}
	err = json.Unmarshal(data, &m)
	if err != nil {
		err = errors.Wrap(err, "Failed call to json.Unmarshal")
		return
	}

	// For interfaces, this targets the concrete struct, for structs, this goes to struct
	// For pointers, this can target a interface or struct
	targetType := reflect.TypeOf(obj).Elem()

	decoded := decode(reflect.ValueOf(m), targetType, service)

	var ok bool
	if err, ok = decoded.Interface().(error); ok && (!decoded.Type().AssignableTo(targetType)) {
		return
	} else {
		reflect.ValueOf(obj).Elem().Set(decoded)
		return
	}
}

func ptrTo(v reflect.Value) reflect.Value {
	if v.CanAddr() {
		return v.Addr()
	} else {
		ptr := reflect.New(v.Type())
		reflect.Indirect(ptr).Set(v)
		return ptr
	}
}
