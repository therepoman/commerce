var request = require('request');

var slackWebhookUrl = "https://hooks.slack.com/services/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";

var SHIPIT_EMOTES = [":happydance:", ":citto:", ":dance:", ":ditto:", ":nyannyan:", ":rareming:", ":bananana:", ":givebits1000:", ":snoop:"]

exports.handler = function(event, context) {
  var data = handleGithubMessage(event);

  if (data != null) {
    var options = {
      method: 'post',
      body: data,
      json: true,
      url: slackWebhookUrl
    };
    request(options, context.done);
  }

  return; 
};

var handleGithubMessage = function(event) {
  var data

  if (event.action == "opened") {
    data = {
        text: "<!here>: A new pull request has been opened by " + event.pull_request.user.login + ": `" + event.pull_request.title + "`. " + event.pull_request.html_url
    }

    return data;
  } else if (event.action == "created") {
      var shipit_re = new RegExp(":\\+1:|LGTM")
      var match = shipit_re.exec(event.comment.body)

     if (match) {
         var emote = Math.floor(Math.random() * SHIPIT_EMOTES.length)
         data = {
             text: "<!here>: SHIP IT! " + SHIPIT_EMOTES[emote] + " " + event.pull_request.html_url
         }

         return data
     }
  }

  return null;
}
