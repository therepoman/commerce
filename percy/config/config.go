package config

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/splatter"
	"github.com/cactus/go-statsd-client/statsd"
	yaml "gopkg.in/yaml.v2"
)

const (
	LocalConfigFilePath            = "/src/code.justin.tv/commerce/percy/config/data/%s.yaml"
	GlobalConfigFilePath           = "/etc/percy/config/%s.yaml"
	EnvironmentEnvironmentVariable = "ENVIRONMENT"

	LambdaRootPathEnvironmentVariable = "LAMBDA_TASK_ROOT"
	LambdaConfigFilePath              = "/config/data/%s.yaml"

	CloudwatchSubstage = "primary"
	ServiceName        = "percy"

	Local      = Environment("local")
	Staging    = Environment("staging")
	Production = Environment("production")
	Default    = Local
)

type Config struct {
	Environment                          string          `yaml:"environment"`
	AWSRegion                            string          `yaml:"aws-region"`
	ConfigS3Endpoint                     string          `yaml:"s3-endpoint"`
	DynamoSuffix                         string          `yaml:"dynamo-suffix"`
	EventbusCelebrationsSQSUrl           string          `yaml:"eventbus-celebrations-sqs-url"`
	EventbusPercySQSUrl                  string          `yaml:"eventbus-percy-sqs-url"`
	EventbusMakoEventsSQSUrl             string          `yaml:"eventbus-mako-events-sqs-url"`
	EventbusPinataSQSUrl                 string          `yaml:"eventbus-pinata-events-sqs-url"`
	EventbusStreamUpSQSUrl               string          `yaml:"eventbus-stream-up-events-sqs-url"`
	EventsRedisUrl                       string          `yaml:"events-redis-url"`
	ExpiredHypeTrainSQS                  string          `yaml:"expired-hypetrain-sqs"`
	MaxWorkers                           int             `yaml:"sqs-workers"`
	CloudWatchMetricsEnabled             bool            `yaml:"cloudwatch-metrics-enabled"`
	ExpireHypeTrainWorker                SQSWorkerConfig `yaml:"expire-hype-train-worker"`
	PantheonNotificationWorker           SQSWorkerConfig `yaml:"pantheon-notification-worker"`
	ClueChannelBanWorker                 SQSWorkerConfig `yaml:"clue-channel-ban-worker"`
	ParticipationEventWorker             SQSWorkerConfig `yaml:"participation-event-worker"`
	CooldownExpirationEventWorker        SQSWorkerConfig `yaml:"cooldown-expiration-event-worker"`
	CelebrationsWorker                   SQSWorkerConfig `yaml:"celebrations-worker"`
	PaidBoostOpportunityStartWorker      SQSWorkerConfig `yaml:"paid-boost-opportunity-start-worker"`
	PaidBoostOpportunityCompletionWorker SQSWorkerConfig `yaml:"paid-boost-opportunity-completion-worker"`
	PaidBoostOfferIDs                    []string        `yaml:"paid-boost-offer-ids"`
	SpadeConf                            SpadeConfig     `yaml:"spade"`
	Endpoints                            Endpoints       `yaml:"endpoints"`
	S2SServiceName                       string          `yaml:"s2s-service-name"`
	HypeTrainDynamicTagID                string          `yaml:"hype-train-dynamic-tag-id"`
	ExperimentOverridesS3BucketName      string          `yaml:"experiment_overrides_s3"`
	SNSTopics                            SNSTopics       `yaml:"sns-topics"`
	SOCKS5Addr                           string          `yaml:"socks5-addr"`
	StateMachines                        StateMachines   `yaml:"state-machines"`
	ApproachingEnabled                   bool            `yaml:"approaching-enabled"`
	ApproachingDatascienceEnabled        bool            `yaml:"approaching-datascience-enabled"`
	ApproachingChannelOverrides          []string        `yaml:"approaching-channel-overrides"`
	PDMS                                 PDMS            `yaml:"pdms"`
	SandstormRole                        string          `yaml:"sandstorm-role"`
	HypeTrainLiveSlackHookSecretName     string          `yaml:"slack-hypetrain-live-url"`
}

type PDMS struct {
	LambdaARN      string `yaml:"lambda-arn"`
	CallerRole     string `yaml:"caller-role"`
	DryRun         bool   `yaml:"dryrun"`
	ReportDeletion bool   `yaml:"report-deletion"`
}

type SQSWorkerConfig struct {
	Name       string `yaml:"name"`
	NumWorkers int    `yaml:"num-workers"`
}

type SpadeConfig struct {
	Environment    string `yaml:"environment"`
	MaxConcurrency int    `yaml:"max-concurrency"`
}

type Environment string

var Environments = map[Environment]interface{}{Local: nil, Staging: nil, Production: nil}

type Endpoints struct {
	IntegrationTest string  `yaml:"integration-test"`
	Mako            string  `yaml:"mako"`
	Pantheon        string  `yaml:"pantheon"`
	Destiny         string  `yaml:"destiny"`
	DynamoDB        *string `yaml:"dynamodb,omitempty"`
	DynamoDBSink    *string `yaml:"dynamodb_sink,omitempty"`
	Pubsub          string  `yaml:"pubsub"`
	PubsubControl   string  `yaml:"pubsub_control"`
	UsersService    string  `yaml:"users-service"`
	Badges          string  `yaml:"badges"`
	Arbiter         string  `yaml:"arbiter"`
	Subscriptions   string  `yaml:"subscriptions"`
	Hallpass        string  `yaml:"hallpass"`
	TMI             string  `yaml:"tmi"`
	Ripley          string  `yaml:"ripley"`
	Mulan           string  `yaml:"mulan"`
	Dart            string  `yaml:"dart"`
	Everdeen        string  `yaml:"everdeen"`
	Personalization string  `yaml:"personalization"`
	Liveline        string  `yaml:"liveline"`
	Booster         string  `yaml:"booster"`
	Payday          string  `yaml:"payday"`
}

type SNSTopics struct {
	EventsHistoryService          string `yaml:"events-history-service"`
	ActivityFeed                  string `yaml:"activity-feed"`
	GringottsRevenueEventSNSTopic string `yaml:"gringotts-revenue-event"`
	CelebrationsActivityFeed      string `yaml:"celebrations-activity-feed"`
}

type StateMachines struct {
	CelebrationPurchaseFulfillment string `yaml:"celebration-purchase-fulfillment"`
	CelebrationPurchaseRevoke      string `yaml:"celebration-purchase-revoke"`
	PDMSUserDeletion               string `yaml:"pdms-user-deletion"`
	BoostOpportunityContribution   string `yaml:"boost-opportunity-contribution"`
}

func IsValidEnvironment(env Environment) bool {
	_, ok := Environments[env]
	return ok
}

func LoadConfig(env Environment) (*Config, error) {
	if !IsValidEnvironment(env) {
		log.Errorf("Invalid environment: %s. Falling back to local", env)
		env = Local
	}

	baseFileName := string(env)
	filePath, err := getConfigFilePath(baseFileName)
	if err != nil {
		return nil, err
	}
	return loadConfig(filePath)
}

func loadConfig(path string) (*Config, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer func() {
		err := file.Close()
		if err != nil {
			log.WithError(err).Error("Error closing config file")
		}
	}()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var cfg Config
	err = yaml.Unmarshal(fileBytes, &cfg)
	if err != nil {
		return nil, err
	}
	return &cfg, nil
}

func getConfigFilePath(baseFilename string) (string, error) {
	if lambdaRootPath := os.Getenv(LambdaRootPathEnvironmentVariable); lambdaRootPath != "" {
		lambdaFName := path.Join(lambdaRootPath, fmt.Sprintf(LambdaConfigFilePath, baseFilename))
		_, err := os.Stat(lambdaFName)
		return lambdaFName, err
	}

	localFname := os.Getenv("GOPATH") + fmt.Sprintf(LocalConfigFilePath, baseFilename)
	if _, err := os.Stat(localFname); !os.IsNotExist(err) {
		return localFname, nil
	}
	globalFname := fmt.Sprintf(GlobalConfigFilePath, baseFilename)
	if _, err := os.Stat(globalFname); os.IsNotExist(err) {
		return "", err
	}
	return globalFname, nil
}

func GetEnv() Environment {
	env := os.Getenv(EnvironmentEnvironmentVariable)
	if env != "" {
		log.Infof("Found ENVIRONMENT environment variable: %s", env)
	} else {
		args := os.Args
		if len(args) > 2 {
			log.Panic("Received too many CLI args")
		} else if len(args) == 2 {
			env = args[1]
			log.Infof("Using environment from CLI arg: %s", env)
		}
	}

	if !IsValidEnvironment(Environment(env)) {
		log.Errorf("Invalid environment: %s", env)
		log.Infof("Falling back to default environment: %s", string(Default))
		return Default
	}

	return Environment(env)
}

// TODO: to consistently send metrics from our lambdas, we need to either:
//   * use an unbuffered client
//   * set a shorter flush period and add a synchronous wait to the end of each execution > flush period
func NewBufferedStatter(cfg Config) (statsd.Statter, error) {
	statters := make([]statsd.Statter, 0)

	if cfg.CloudWatchMetricsEnabled {
		cloudwatchStatter := splatter.NewBufferedTelemetryCloudWatchStatter(&splatter.BufferedTelemetryConfig{
			AWSRegion:         cfg.AWSRegion,
			ServiceName:       ServiceName,
			Stage:             cfg.Environment,
			Substage:          CloudwatchSubstage,
			BufferSize:        100000,
			AggregationPeriod: time.Minute,
			FlushPeriod:       time.Second * 30,
		}, map[string]bool{})

		statters = append(statters, cloudwatchStatter)
	}

	return splatter.NewCompositeStatter(statters)
}
