package lambdahandlers

import (
	"context"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
)

type cleanHypeTrainConfigsDBLambdaHandler struct {
	HypeTrainConfigsDB percy.HypeTrainConfigsDB
}

func NewCleanHypeTrainConfigsDBLambdaHandler(hypeTrainConfigsDB percy.HypeTrainConfigsDB) *cleanHypeTrainConfigsDBLambdaHandler {
	return &cleanHypeTrainConfigsDBLambdaHandler{
		HypeTrainConfigsDB: hypeTrainConfigsDB,
	}
}

func (h *cleanHypeTrainConfigsDBLambdaHandler) Handle(ctx context.Context, input *percy.CleanHypeTrainConfigsRecordsRequest) (*percy.CleanHypeTrainConfigsRecordsResponse, error) {
	err := IsValidUserDeletionRequest(input)
	if err != nil {
		return nil, err
	}

	if input.IsDryRun {
		logrus.Info("not cleaning hype train config records for user IDs, was dry run")
	} else {
		err := h.HypeTrainConfigsDB.DeleteConfigRecords(ctx, input.UserIDs)
		if err != nil {
			return nil, err
		}
	}

	return &percy.CleanHypeTrainConfigsRecordsResponse{
		UserDeletionResponseData: percy.UserDeletionResponseData{
			UserIDs:        input.UserIDs,
			IsDryRun:       input.IsDryRun,
			ReportDeletion: input.ReportDeletion,
		},
	}, nil
}
