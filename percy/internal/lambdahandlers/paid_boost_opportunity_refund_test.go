package lambdahandlers

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/percy/internal/awsmodels"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestBoostOpportunityRefundHandler_Handle(t *testing.T) {
	Convey("Given a Lambda handler", t, func() {
		launchPad := internalfakes.FakeLaunchPad{}
		idempotency := internalfakes.FakeIdempotencyEnforcer{}

		h := boostOpportunityRefundHandler{
			launchPad:   &launchPad,
			idempotency: &idempotency,
		}

		Convey("we should error if", func() {
			Convey("the request fails validation", func() {
				err := h.Handle(context.Background(), awsmodels.BoostOpportunityFulfillmentInput{})
				So(err, ShouldNotBeNil)
			})

			Convey("idempotency enforcer errors", func() {
				idempotency.WithIdempotencyReturns(errors.New("ERROR"))

				err := h.Handle(context.Background(), awsmodels.BoostOpportunityFulfillmentInput{
					ChannelID:       "123",
					UserID:          "456",
					OriginID:        "abc",
					Units:           5,
					UnixTimeSeconds: time.Now().Unix(),
				})
				So(err, ShouldNotBeNil)
			})
		})

		Convey("succeeds when idempotency enforcer returns nil", func() {
			idempotency.WithIdempotencyReturns(nil)

			err := h.Handle(context.Background(), awsmodels.BoostOpportunityFulfillmentInput{
				ChannelID:       "123",
				UserID:          "456",
				OriginID:        "abc",
				Units:           5,
				UnixTimeSeconds: time.Now().Unix(),
			})

			So(err, ShouldBeNil)
		})
	})
}
