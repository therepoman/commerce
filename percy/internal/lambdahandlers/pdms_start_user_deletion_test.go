package lambdahandlers

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/percy/config"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/eventbus/schema/pkg/user"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/sfn"
	. "github.com/smartystreets/goconvey/convey"
)

func TestHandler_Handle(t *testing.T) {
	Convey("given a start user deletion handler", t, func() {
		pdmsClient := &internalfakes.FakePrivacy{}
		sfnClient := &internalfakes.FakeStateMachine{}
		cfg := &config.Config{
			StateMachines: config.StateMachines{
				PDMSUserDeletion: "foomachine",
			},
			PDMS: config.PDMS{
				DryRun:         false,
				ReportDeletion: true,
			},
		}

		h := NewStartDeletionLambdaHandler(cfg, pdmsClient, sfnClient)

		ctx := context.Background()
		userID := "123123123"

		Convey("returns nil", func() {
			callsToPDMS := 0

			Convey("when we successfully start the step function and promise deletion", func() {
				sfnClient.StartExecutionPDMSUserDeletionReturns(nil)
				pdmsClient.PromiseDeletionsReturns(nil)
				callsToPDMS = 1
			})

			Convey("when we have already started the step function and then promise deletion", func() {
				sfnClient.StartExecutionPDMSUserDeletionReturns(awserr.New(sfn.ErrCodeExecutionAlreadyExists, "WALRUS STRIKE", nil))
				pdmsClient.PromiseDeletionsReturns(nil)
				callsToPDMS = 1
			})

			Convey("when we successfully start the step function and don't promise deletion since it's a dry run", func() {
				h.Config.PDMS.DryRun = true
				sfnClient.StartExecutionPDMSUserDeletionReturns(nil)
				callsToPDMS = 0
			})

			Convey("when we successfully start the step function and don't promise deletion since it's disabled", func() {
				h.Config.PDMS.ReportDeletion = false
				sfnClient.StartExecutionPDMSUserDeletionReturns(nil)
				callsToPDMS = 0
			})

			err := h.Handle(ctx, nil, &user.Destroy{
				UserId: userID,
			})

			So(err, ShouldBeNil)
			So(pdmsClient.PromiseDeletionsCallCount(), ShouldEqual, callsToPDMS)
		})

		Convey("returns error", func() {
			Convey("when we fail to invoke the step function", func() {
				sfnClient.StartExecutionPDMSUserDeletionReturns(errors.New("WALRUS STRIKE"))
			})

			Convey("when we successfully start the step function and fail to promise deletion", func() {
				sfnClient.StartExecutionPDMSUserDeletionReturns(nil)
				pdmsClient.PromiseDeletionsReturns(errors.New("WALRUS STRIKE"))
			})

			Convey("when we have already started the step function and fail to promise deletion", func() {
				sfnClient.StartExecutionPDMSUserDeletionReturns(awserr.New(sfn.ErrCodeExecutionAlreadyExists, "WALRUS STRIKE", nil))
				pdmsClient.PromiseDeletionsReturns(errors.New("WALRUS STRIKE"))
			})

			err := h.Handle(ctx, nil, &user.Destroy{
				UserId: userID,
			})

			So(err, ShouldNotBeNil)
		})
	})
}
