package lambdahandlers

import (
	"context"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/schema/pkg/user"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/sfn"
)

const (
	promiseDeleteNumOfDays = 30 * 24 * time.Hour // this value may change depending on recommendations from PDMS
)

type startDeletionLambdaHandler struct {
	Privacy   percy.Privacy
	SFNClient percy.StateMachine
	Config    *config.Config
}

func NewStartDeletionLambdaHandler(cfg *config.Config, privacy percy.Privacy, sfnClient percy.StateMachine) *startDeletionLambdaHandler {
	return &startDeletionLambdaHandler{
		Privacy:   privacy,
		Config:    cfg,
		SFNClient: sfnClient,
	}
}

func (h *startDeletionLambdaHandler) Handle(ctx context.Context, header *eventbus.Header, event *user.Destroy) error {
	log.WithField("userID", event.UserId).Info("Starting User Destroy Step Function")
	err := h.SFNClient.StartExecutionPDMSUserDeletion(ctx, event.UserId, percy.StartUserDeletionRequest{
		UserDeletionRequestData: percy.UserDeletionRequestData{
			UserIDs:        []string{event.UserId},
			IsDryRun:       h.Config.PDMS.DryRun,
			ReportDeletion: h.Config.PDMS.ReportDeletion,
		},
	})
	if err != nil {
		var existingExecutionError bool
		if awsErr, ok := err.(awserr.Error); ok { //nolint
			if awsErr.Code() == sfn.ErrCodeExecutionAlreadyExists {
				existingExecutionError = true
			}
		}

		if existingExecutionError {
			log.WithField("userID", event.UserId).Info("found existing User Destroy SFN execution, skipping.")
		} else {
			log.WithField("userID", event.UserId).WithError(err).Error("error creating User Destroy SFN execution")
			return err
		}
	}
	timeOfDeletionPromise := time.Now().Add(promiseDeleteNumOfDays)
	log := log.WithFields(log.Fields{
		"userID":    event.UserId,
		"timestamp": timeOfDeletionPromise,
	})
	if h.Config.PDMS.DryRun {
		log.Info("Not sending to PDMS to promise deletion due to Dry Run")
		return nil
	} else if !h.Config.PDMS.ReportDeletion {
		log.Info("Not sending to PDMS to promise deletion due to reporting deletion being disabled")
		return nil
	} else {
		log.Info("Sending PromiseDeletion to PDMS")
		err = h.Privacy.PromiseDeletions(ctx, []string{event.UserId}, timeOfDeletionPromise)

		if err != nil {
			log.WithError(err).Error("error sending user PromiseDeletion to PDMS")
			return err
		}

		return nil
	}
}
