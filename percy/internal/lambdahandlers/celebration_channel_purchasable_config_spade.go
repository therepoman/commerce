package lambdahandlers

import (
	"context"
	"errors"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	percy "code.justin.tv/commerce/percy/internal"
	percy_dynamo "code.justin.tv/commerce/percy/internal/dynamodb"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

// The DynamoDBEventRecord struct that ships with github.com/aws/aws-lambda-go/events is incompatible with dynamodbattribute.UnmarshalMap
// Taken from: https://stackoverflow.com/a/50164289
type DynamoDBEvent struct {
	Records []DynamoDBEventRecord `json:"Records"`
}

type DynamoDBEventRecord struct {
	AWSRegion      string                       `json:"awsRegion"`
	Change         DynamoDBStreamRecord         `json:"dynamodb"`
	EventID        string                       `json:"eventID"`
	EventName      events.DynamoDBOperationType `json:"eventName"`
	EventSource    string                       `json:"eventSource"`
	EventVersion   string                       `json:"eventVersion"`
	EventSourceArn string                       `json:"eventSourceARN"`
	UserIdentity   *events.DynamoDBUserIdentity `json:"userIdentity,omitempty"`
}

type DynamoDBStreamRecord struct {
	ApproximateCreationDateTime events.SecondsEpochTime `json:"ApproximateCreationDateTime,omitempty"`
	// changed to map[string]*dynamodb.AttributeValue
	Keys map[string]*dynamodb.AttributeValue `json:"Keys,omitempty"`
	// changed to map[string]*dynamodb.AttributeValue
	NewImage map[string]*dynamodb.AttributeValue `json:"NewImage,omitempty"`
	// changed to map[string]*dynamodb.AttributeValue
	OldImage       map[string]*dynamodb.AttributeValue `json:"OldImage,omitempty"`
	SequenceNumber string                              `json:"SequenceNumber"`
	SizeBytes      int64                               `json:"SizeBytes"`
	StreamViewType string                              `json:"StreamViewType"`
}

type CelebrationsChannelPurchasableConfigSpadeHandler struct {
	Spade percy.Spade
}

// NewCelebrationsChannelPurchasableConfigSpadeHandler ...
func NewCelebrationsChannelPurchasableConfigSpadeHandler(spade percy.Spade) *CelebrationsChannelPurchasableConfigSpadeHandler {
	return &CelebrationsChannelPurchasableConfigSpadeHandler{
		Spade: spade,
	}
}

func (c *CelebrationsChannelPurchasableConfigSpadeHandler) Handle(ctx context.Context, e DynamoDBEvent) error {
	var spadeEvents []percy.CelebrationChannelPurchasableConfigChangeEventParams

	for _, record := range e.Records {
		event, err := dynamoRecordToSpadeEvent(record)
		if err != nil {
			return err
		}

		spadeEvents = append(spadeEvents, event)
	}

	err := c.Spade.SendCelebrationChannelPurchasableConfigChangeEvent(ctx, spadeEvents...)
	if err != nil {
		return err
	}

	return nil
}

func dynamoRecordToSpadeEvent(event DynamoDBEventRecord) (percy.CelebrationChannelPurchasableConfigChangeEventParams, error) {
	spadeEvent := percy.CelebrationChannelPurchasableConfigChangeEventParams{}

	if len(event.Change.OldImage) == 0 && len(event.Change.NewImage) == 0 {
		return spadeEvent, errors.New("encountered change with empty new and old images")
	}

	oldRecord, err := unmarshalEventRecord(event.Change.OldImage)
	if err != nil {
		return spadeEvent, err
	}

	newRecord, err := unmarshalEventRecord(event.Change.NewImage)
	if err != nil {
		return spadeEvent, err
	}

	spadeEvent.ServerTime = time.Now()
	spadeEvent.EventID = event.EventID
	spadeEvent.StoreType = "DynamoDB"
	spadeEvent.StateChangeType = toStateChangeType(event)

	if oldRecord != nil {
		spadeEvent.ChannelID = oldRecord.ChannelID

		for size, config := range oldRecord.PurchaseConfig {
			switch size {
			case percy.CelebrationSizeSmall:
				spadeEvent.SmallOfferIDOld = pointers.StringOrDefault(config.OfferID, "")
				spadeEvent.SmallIsDisabledOld = pointers.BoolOrDefault(config.IsDisabled, false)
			case percy.CelebrationSizeLarge:
				spadeEvent.LargeOfferIDOld = pointers.StringOrDefault(config.OfferID, "")
				spadeEvent.LargeIsDisabledOld = pointers.BoolOrDefault(config.IsDisabled, false)
			case percy.CelebrationSizeUnknown:
				fallthrough
			default:
				return spadeEvent, fmt.Errorf("unrecognized celebration size %s", size)
			}
		}
	}

	if newRecord != nil {
		if spadeEvent.ChannelID == "" {
			spadeEvent.ChannelID = newRecord.ChannelID
		}

		for size, config := range newRecord.PurchaseConfig {
			switch size {
			case percy.CelebrationSizeSmall:
				spadeEvent.SmallOfferIDNew = pointers.StringOrDefault(config.OfferID, "")
				spadeEvent.SmallIsDisabledNew = pointers.BoolOrDefault(config.IsDisabled, false)
			case percy.CelebrationSizeLarge:
				spadeEvent.LargeOfferIDNew = pointers.StringOrDefault(config.OfferID, "")
				spadeEvent.LargeIsDisabledNew = pointers.BoolOrDefault(config.IsDisabled, false)
			case percy.CelebrationSizeUnknown:
				fallthrough
			default:
				return spadeEvent, fmt.Errorf("unrecognized celebration size %s", size)
			}
		}
	}

	return spadeEvent, nil
}

func toStateChangeType(record DynamoDBEventRecord) string {
	switch record.EventName {
	case events.DynamoDBOperationTypeModify:
		return "update"
	case events.DynamoDBOperationTypeInsert:
		return "insert"
	case events.DynamoDBOperationTypeRemove:
		return "delete"
	}
	return ""
}

func unmarshalEventRecord(record map[string]*dynamodb.AttributeValue) (*percy_dynamo.CelebrationChannelPurchasableConfigRecord, error) {
	var config *percy_dynamo.CelebrationChannelPurchasableConfigRecord
	err := dynamodbattribute.UnmarshalMap(record, &config)
	if err != nil {
		return nil, err
	}

	return config, nil
}
