package lambdahandlers

import (
	"context"
	"errors"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestEmitCelebrationHandler_Handle(t *testing.T) {
	Convey("Given a Lambda handler", t, func() {
		engine := internalfakes.FakeCelebrationsEngine{}
		idempotency := internalfakes.FakeIdempotencyEnforcer{}

		h := EmitCelebrationHandler{
			engine:      &engine,
			idempotency: &idempotency,
		}

		Convey("we should error if", func() {
			Convey("the request fails validation", func() {
				err := h.Handle(context.Background(), percy.CelebrationsPurchaseFulfillmentReq{})
				So(err, ShouldNotBeNil)
			})

			Convey("engine#EmitCelebration returns an error", func() {
				idempotency.WithIdempotencyReturns(errors.New("ERROR"))

				err := h.Handle(context.Background(), percy.CelebrationsPurchaseFulfillmentReq{
					BenefactorUserID: "123",
					RecipientUserID:  "456",
					OriginID:         "abc",
					OfferID:          "some-id",
				})
				So(err, ShouldNotBeNil)
			})
		})

		Convey("we should call engine#EmitCelebration", func() {
			idempotency.WithIdempotencyReturns(nil)

			err := h.Handle(context.Background(), percy.CelebrationsPurchaseFulfillmentReq{
				BenefactorUserID: "123",
				RecipientUserID:  "456",
				OriginID:         "abc",
				OfferID:          "some-id",
			})
			So(err, ShouldBeNil)
		})
	})
}
