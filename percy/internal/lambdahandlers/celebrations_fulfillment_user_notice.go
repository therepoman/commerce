package lambdahandlers

import (
	"context"

	percy "code.justin.tv/commerce/percy/internal"
)

type UserNoticeCelebrationHandler struct {
	engine      percy.CelebrationsEngine
	idempotency percy.IdempotencyEnforcer
}

// NewUserNoticeCelebrationHandler ...
func NewUserNoticeCelebrationHandler(engine percy.CelebrationsEngine, idempotency percy.IdempotencyEnforcer) *UserNoticeCelebrationHandler {
	return &UserNoticeCelebrationHandler{
		engine:      engine,
		idempotency: idempotency,
	}
}

func (e *UserNoticeCelebrationHandler) Handle(ctx context.Context, input percy.CelebrationsPurchaseFulfillmentReq) error {
	err := ValidateFulfillmentRequest(input)
	if err != nil {
		return err
	}

	//nolint:gosimple
	return e.idempotency.WithIdempotency(ctx, percy.IdempotencyNamespaceCelebrationPurchaseFulfillment, input.OriginID, string(percy.CelebrationPurchaseFulfillmentUserNotice), func() error {
		return e.engine.SendFulfillmentUserNotice(ctx, percy.CelebrationsUserNoticeArgs{
			RecipientUserID:  input.RecipientUserID,
			BenefactorUserID: input.BenefactorUserID,
			OriginID:         input.OriginID,
			OfferID:          input.OfferID,
		})
	})
}
