package lambdahandlers

import (
	"context"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCelebrationsChannelPurchasableConfigSpadeHandler_Handle(t *testing.T) {
	Convey("Given a Lambda handler", t, func() {
		spade := internalfakes.FakeSpade{}

		h := CelebrationsChannelPurchasableConfigSpadeHandler{
			Spade: &spade,
		}

		Convey("we should error if", func() {
			Convey("both new and old records are nil", func() {
				err := h.Handle(context.Background(), DynamoDBEvent{
					Records: []DynamoDBEventRecord{
						{
							Change: DynamoDBStreamRecord{},
						},
					},
				})

				So(err, ShouldNotBeNil)
			})
		})

		Convey("we should set the base fields", func() {
			spade.SendCelebrationChannelPurchasableConfigChangeEventReturns(nil)

			err := h.Handle(context.Background(), DynamoDBEvent{
				Records: []DynamoDBEventRecord{
					{
						EventID:   "123",
						EventName: "MODIFY",
						Change: DynamoDBStreamRecord{
							NewImage: map[string]*dynamodb.AttributeValue{
								"channel_id": {
									S: aws.String("abc"),
								},
							},
						},
					},
				},
			})

			So(err, ShouldBeNil)
			_, events := spade.SendCelebrationChannelPurchasableConfigChangeEventArgsForCall(0)
			So(events, ShouldHaveLength, 1)
			So(events[0].EventID, ShouldEqual, "123")
			So(events[0].ServerTime.IsZero(), ShouldBeFalse)
			So(events[0].StoreType, ShouldEqual, "DynamoDB")
			So(events[0].StateChangeType, ShouldEqual, "update")
			So(events[0].ChannelID, ShouldEqual, "abc")
		})

		Convey("we should set the 'old' fields if the old record exists", func() {
			spade.SendCelebrationChannelPurchasableConfigChangeEventReturns(nil)

			err := h.Handle(context.Background(), DynamoDBEvent{
				Records: []DynamoDBEventRecord{
					{
						EventID:   "123",
						EventName: "MODIFY",
						Change: DynamoDBStreamRecord{
							OldImage: map[string]*dynamodb.AttributeValue{
								"channel_id": {
									S: aws.String("abc"),
								},
								"purchase_config": {
									M: map[string]*dynamodb.AttributeValue{
										string(percy.CelebrationSizeSmall): {
											M: map[string]*dynamodb.AttributeValue{
												"is_disabled": {
													BOOL: aws.Bool(true),
												},
												"offer_id": {
													S: aws.String("offer1"),
												},
											},
										},
										string(percy.CelebrationSizeLarge): {
											M: map[string]*dynamodb.AttributeValue{
												"is_disabled": {
													BOOL: aws.Bool(false),
												},
												"offer_id": {
													S: aws.String("offer2"),
												},
											},
										},
									},
								},
							},
						},
					},
				},
			})

			So(err, ShouldBeNil)
			_, events := spade.SendCelebrationChannelPurchasableConfigChangeEventArgsForCall(0)
			So(events, ShouldHaveLength, 1)
			So(events[0].SmallIsDisabledOld, ShouldBeTrue)
			So(events[0].MediumIsDisabledOld, ShouldBeFalse)
			So(events[0].LargeIsDisabledOld, ShouldBeFalse)
			So(events[0].SmallOfferIDOld, ShouldEqual, "offer1")
			So(events[0].LargeOfferIDOld, ShouldEqual, "offer2")
		})

		Convey("we should set the 'new' fields if the new record exists", func() {
			spade.SendCelebrationChannelPurchasableConfigChangeEventReturns(nil)

			err := h.Handle(context.Background(), DynamoDBEvent{
				Records: []DynamoDBEventRecord{
					{
						EventID:   "123",
						EventName: "MODIFY",
						Change: DynamoDBStreamRecord{
							NewImage: map[string]*dynamodb.AttributeValue{
								"channel_id": {
									S: aws.String("abc"),
								},
								"purchase_config": {
									M: map[string]*dynamodb.AttributeValue{
										string(percy.CelebrationSizeSmall): {
											M: map[string]*dynamodb.AttributeValue{
												"is_disabled": {
													BOOL: aws.Bool(true),
												},
												"offer_id": {
													S: aws.String("offer1"),
												},
											},
										},
										string(percy.CelebrationSizeLarge): {
											M: map[string]*dynamodb.AttributeValue{
												"is_disabled": {
													BOOL: aws.Bool(false),
												},
												"offer_id": {
													S: aws.String("offer2"),
												},
											},
										},
									},
								},
							},
						},
					},
				},
			})

			So(err, ShouldBeNil)
			_, events := spade.SendCelebrationChannelPurchasableConfigChangeEventArgsForCall(0)
			So(events, ShouldHaveLength, 1)
			So(events[0].SmallIsDisabledNew, ShouldBeTrue)
			So(events[0].LargeIsDisabledNew, ShouldBeFalse)
			So(events[0].SmallOfferIDNew, ShouldEqual, "offer1")
			So(events[0].LargeOfferIDNew, ShouldEqual, "offer2")
		})

		Convey("we should set the 'new' and 'old' fields if both exists", func() {
			spade.SendCelebrationChannelPurchasableConfigChangeEventReturns(nil)

			err := h.Handle(context.Background(), DynamoDBEvent{
				Records: []DynamoDBEventRecord{
					{
						EventID:   "123",
						EventName: "MODIFY",
						Change: DynamoDBStreamRecord{
							OldImage: map[string]*dynamodb.AttributeValue{
								"channel_id": {
									S: aws.String("abc"),
								},
								"purchase_config": {
									M: map[string]*dynamodb.AttributeValue{
										string(percy.CelebrationSizeSmall): {
											M: map[string]*dynamodb.AttributeValue{
												"is_disabled": {
													BOOL: aws.Bool(true),
												},
												"offer_id": {
													S: aws.String("offer1"),
												},
											},
										},
										string(percy.CelebrationSizeLarge): {
											M: map[string]*dynamodb.AttributeValue{
												"is_disabled": {
													BOOL: aws.Bool(false),
												},
												"offer_id": {
													S: aws.String("offer2"),
												},
											},
										},
									},
								},
							},
							NewImage: map[string]*dynamodb.AttributeValue{
								"channel_id": {
									S: aws.String("abc"),
								},
								"purchase_config": {
									M: map[string]*dynamodb.AttributeValue{
										string(percy.CelebrationSizeSmall): {
											M: map[string]*dynamodb.AttributeValue{
												"is_disabled": {
													BOOL: aws.Bool(true),
												},
												"offer_id": {
													S: aws.String("offer3"),
												},
											},
										},
										string(percy.CelebrationSizeLarge): {
											M: map[string]*dynamodb.AttributeValue{
												"is_disabled": {
													BOOL: aws.Bool(false),
												},
												"offer_id": {
													S: aws.String("offer4"),
												},
											},
										},
									},
								},
							},
						},
					},
				},
			})

			So(err, ShouldBeNil)
			_, events := spade.SendCelebrationChannelPurchasableConfigChangeEventArgsForCall(0)
			So(events, ShouldHaveLength, 1)

			So(events[0].SmallIsDisabledOld, ShouldBeTrue)
			So(events[0].LargeIsDisabledOld, ShouldBeFalse)
			So(events[0].SmallOfferIDOld, ShouldEqual, "offer1")
			So(events[0].LargeOfferIDOld, ShouldEqual, "offer2")

			So(events[0].SmallIsDisabledNew, ShouldBeTrue)
			So(events[0].LargeIsDisabledNew, ShouldBeFalse)
			So(events[0].SmallOfferIDNew, ShouldEqual, "offer3")
			So(events[0].LargeOfferIDNew, ShouldEqual, "offer4")
		})
	})
}
