package lambdahandlers

import (
	"context"
	"errors"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCleanBadgesDBLambdaHandler_Handle(t *testing.T) {
	Convey("given a clean badges DB lambda handler", t, func() {
		badgesDB := &internalfakes.FakeBadgesDB{}

		h := NewCleanBadgesDBLambdaHandler(badgesDB)

		ctx := context.Background()
		userID := "123123123"
		userIDs := []string{userID}
		channelID1 := "45454545"
		channelID2 := "232323232"
		userID1 := "6767657"
		userID2 := "78787878787"

		Convey("returns error", func() {
			var input *percy.CleanBadgesRecordsRequest

			Convey("when the input is invalid", func() {
				input = nil
			})

			Convey("when we fail to look up records by user ID", func() {
				input = &percy.CleanBadgesRecordsRequest{
					UserDeletionRequestData: percy.UserDeletionRequestData{
						UserIDs: userIDs,
					},
				}

				badgesDB.GetAllBadgeChannelsForUserIDReturns(nil, "", errors.New("WALRUS STRIKE"))
			})

			Convey("when we fail to look up records by channel ID", func() {
				input = &percy.CleanBadgesRecordsRequest{
					UserDeletionRequestData: percy.UserDeletionRequestData{
						UserIDs: userIDs,
					},
				}

				badgesDB.GetAllBadgeUsersForChannelIDReturns(nil, "", errors.New("WALRUS STRIKE"))
			})

			Convey("when we fail to delete records when it's a real run", func() {
				input = &percy.CleanBadgesRecordsRequest{
					UserDeletionRequestData: percy.UserDeletionRequestData{
						UserIDs: userIDs,
					},
				}

				badgesDB.GetAllBadgeUsersForChannelIDReturns([]string{userID1, userID2}, "", nil)
				badgesDB.GetAllBadgeChannelsForUserIDReturns([]string{channelID1, channelID2}, "", nil)

				badgesDB.DeleteBulkBadgesReturns(errors.New("WALRUS STRIKE"))
			})

			_, err := h.Handle(ctx, input)
			So(err, ShouldNotBeNil)
		})

		Convey("returns success", func() {
			var input *percy.CleanBadgesRecordsRequest
			var numberOfCalls int

			Convey("when it is a dry run", func() {
				input = &percy.CleanBadgesRecordsRequest{
					UserDeletionRequestData: percy.UserDeletionRequestData{
						UserIDs:  userIDs,
						IsDryRun: true,
					},
				}
				numberOfCalls = 0
			})

			Convey("when it's the real run", func() {
				input = &percy.CleanBadgesRecordsRequest{
					UserDeletionRequestData: percy.UserDeletionRequestData{
						UserIDs: userIDs,
					},
				}
				numberOfCalls = 1
			})

			output, err := h.Handle(ctx, input)
			So(err, ShouldBeNil)
			So(output, ShouldNotBeNil)
			So(output.UserIDs, ShouldResemble, input.UserIDs)
			So(badgesDB.DeleteBulkBadgesCallCount(), ShouldEqual, numberOfCalls)
		})
	})
}
