package lambdahandlers

import (
	"context"

	percy "code.justin.tv/commerce/percy/internal"
)

type PayoutCelebrationHandler struct {
	engine      percy.CelebrationsEngine
	idempotency percy.IdempotencyEnforcer
}

// NewPayoutCelebrationHandler ...
func NewPayoutCelebrationHandler(engine percy.CelebrationsEngine, idempotency percy.IdempotencyEnforcer) *PayoutCelebrationHandler {
	return &PayoutCelebrationHandler{
		engine:      engine,
		idempotency: idempotency,
	}
}

func (e *PayoutCelebrationHandler) Handle(ctx context.Context, input percy.CelebrationsPurchaseFulfillmentReq) error {
	err := ValidateFulfillmentRequest(input)
	if err != nil {
		return err
	}

	return e.idempotency.WithIdempotency(ctx, percy.IdempotencyNamespaceCelebrationPurchaseFulfillment, input.OriginID, string(percy.CelebrationPurchaseFulfillmentPostRevenue), func() error {
		return e.engine.Payout(ctx, percy.CelebrationsPayoutArgs{
			IsClawback:       false,
			RecipientUserID:  input.RecipientUserID,
			BenefactorUserID: input.BenefactorUserID,
			OriginID:         input.OriginID,
			OfferID:          input.OfferID,
		})
	})
}
