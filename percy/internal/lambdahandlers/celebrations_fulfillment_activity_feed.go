package lambdahandlers

import (
	"context"

	percy "code.justin.tv/commerce/percy/internal"
)

type ActivityFeedCelebrationHandler struct {
	engine      percy.CelebrationsEngine
	idempotency percy.IdempotencyEnforcer
}

// NewActivityFeedCelebrationHandler ...
func NewActivityFeedCelebrationHandler(engine percy.CelebrationsEngine, idempotency percy.IdempotencyEnforcer) *ActivityFeedCelebrationHandler {
	return &ActivityFeedCelebrationHandler{
		engine:      engine,
		idempotency: idempotency,
	}
}

func (e *ActivityFeedCelebrationHandler) Handle(ctx context.Context, input percy.CelebrationsPurchaseFulfillmentReq) error {
	err := ValidateFulfillmentRequest(input)
	if err != nil {
		return err
	}

	//nolint:gosimple
	return e.idempotency.WithIdempotency(ctx, percy.IdempotencyNamespaceCelebrationPurchaseFulfillment, input.OriginID, string(percy.CelebrationPurchaseFulfillmentActivityFeed), func() error {
		return e.engine.PublishToActivityFeed(ctx, percy.PublishCelebrationToActivityFeedArgs{
			RecipientUserID:  input.RecipientUserID,
			BenefactorUserID: input.BenefactorUserID,
			OriginID:         input.OriginID,
			OfferID:          input.OfferID,
		})
	})
}
