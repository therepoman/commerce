package lambdahandlers

import (
	"context"
	"errors"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestLambdaHandler_Handle(t *testing.T) {
	Convey("Given a PDMS delete user leaderboard entries handler", t, func() {
		pdmsClient := new(internalfakes.FakePrivacy)

		handler := NewReportDeletionLambdaHandler(pdmsClient)

		ctx := context.Background()
		userID := "123123123"

		Convey("returns error", func() {
			var input *percy.ReportDeletionRequest

			Convey("when the input is nil", func() {
				input = nil
			})

			Convey("when the input contains no user ID", func() {
				input = &percy.ReportDeletionRequest{}
			})

			Convey("when we get an error from PDMS when reporting deletion is enabled", func() {
				input = &percy.ReportDeletionRequest{
					UserDeletionRequestData: percy.UserDeletionRequestData{
						UserIDs:        []string{userID},
						ReportDeletion: true,
					},
				}
				pdmsClient.ReportDeletionsReturns(errors.New("WALRUS STRIKE"))
			})

			_, err := handler.Handle(ctx, input)

			So(err, ShouldNotBeNil)
		})

		Convey("returns success", func() {
			var input *percy.ReportDeletionRequest
			var numberOfCalls int

			Convey("when we successfully call Pantheon with valid input", func() {
				input = &percy.ReportDeletionRequest{
					UserDeletionRequestData: percy.UserDeletionRequestData{
						UserIDs:        []string{userID},
						ReportDeletion: true,
					},
				}
				pdmsClient.ReportDeletionsReturns(nil)
				numberOfCalls = 1
			})

			Convey("when it is a dry run", func() {
				input = &percy.ReportDeletionRequest{
					UserDeletionRequestData: percy.UserDeletionRequestData{
						UserIDs:  []string{userID},
						IsDryRun: true,
					},
				}
				numberOfCalls = 0
			})

			Convey("when reporting deletion is disabled and it's not a dry run", func() {
				input = &percy.ReportDeletionRequest{
					UserDeletionRequestData: percy.UserDeletionRequestData{
						UserIDs: []string{userID},
					},
				}
				numberOfCalls = 0
			})

			resp, err := handler.Handle(ctx, input)

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.UserIDs, ShouldResemble, []string{userID})
			So(pdmsClient.ReportDeletionsCallCount(), ShouldEqual, numberOfCalls)
		})
	})
}
