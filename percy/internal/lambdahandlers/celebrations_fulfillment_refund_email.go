package lambdahandlers

import (
	"context"

	percy "code.justin.tv/commerce/percy/internal"
)

type RefundEmailCelebrationHandler struct {
	engine      percy.CelebrationsEngine
	idempotency percy.IdempotencyEnforcer
}

// NewRefundEmailCelebrationHandler ...
func NewRefundEmailCelebrationHandler(engine percy.CelebrationsEngine, idempotency percy.IdempotencyEnforcer) *RefundEmailCelebrationHandler {
	return &RefundEmailCelebrationHandler{
		engine:      engine,
		idempotency: idempotency,
	}
}

func (e *RefundEmailCelebrationHandler) Handle(ctx context.Context, input percy.CelebrationsPurchaseFulfillmentReq) error {
	err := ValidateFulfillmentRequest(input)
	if err != nil {
		return err
	}

	return e.idempotency.WithIdempotency(ctx, percy.IdempotencyNamespaceCelebrationPurchaseFulfillment, input.OriginID, string(percy.CelebrationPurchaseFulfillmentRefundPurchaseEmail), func() error {
		return e.engine.SendRefundEmail(ctx, percy.CelebrationsRefundEmailArgs{
			RecipientUserID:  input.RecipientUserID,
			BenefactorUserID: input.BenefactorUserID,
		})
	})
}
