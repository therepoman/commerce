package lambdahandlers

import (
	"context"

	percy "code.justin.tv/commerce/percy/internal"
)

type EmitCelebrationHandler struct {
	engine      percy.CelebrationsEngine
	idempotency percy.IdempotencyEnforcer
}

// NewEmitCelebrationHandler ...
func NewEmitCelebrationHandler(engine percy.CelebrationsEngine, idempotency percy.IdempotencyEnforcer) *EmitCelebrationHandler {
	return &EmitCelebrationHandler{
		engine:      engine,
		idempotency: idempotency,
	}
}

func (e *EmitCelebrationHandler) Handle(ctx context.Context, input percy.CelebrationsPurchaseFulfillmentReq) error {
	err := ValidateFulfillmentRequest(input)
	if err != nil {
		return err
	}

	//nolint:gosimple
	return e.idempotency.WithIdempotency(ctx, percy.IdempotencyNamespaceCelebrationPurchaseFulfillment, input.OriginID, string(percy.CelebrationPurchaseFulfillmentEmitPubsub), func() error {
		return e.engine.EmitCelebration(ctx, percy.EmitCelebrationArgs{
			RecipientUserID:  input.RecipientUserID,
			BenefactorUserID: input.BenefactorUserID,
			OriginID:         input.OriginID,
			OfferID:          input.OfferID,
		})
	})
}
