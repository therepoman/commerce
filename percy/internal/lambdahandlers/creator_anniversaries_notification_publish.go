package lambdahandlers

import (
	"context"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

const (
	dartOnsiteNotificationCampaign = "creator_anniversaries_onsite"
	dartEmailNotificationCampaign  = "creator_anniversary_email"
)

type CreatorAnniversariesNotificationPublishHandler struct {
	engine percy.CreatorAnniversariesEngine
}

type Notification struct {
	daysAhead    int
	campaignType string
}

var NOTIFICTION_SCHEDULE = []Notification{
	{
		7, dartEmailNotificationCampaign,
	},
	{
		2, dartOnsiteNotificationCampaign,
	},
}

// NewCreatorAnniversariesNotificationPublishHandler ...
func NewCreatorAnniversariesNotificationPublishHandler(engine percy.CreatorAnniversariesEngine) *CreatorAnniversariesNotificationPublishHandler {
	return &CreatorAnniversariesNotificationPublishHandler{
		engine: engine,
	}
}

func (e *CreatorAnniversariesNotificationPublishHandler) Handle(ctx context.Context, input percy.CreatorAnniversariesNotificationPublishReq) ([]string, error) {
	err := IsValidCreatorAnniversaryNotificationPublishRequest(input)
	if err != nil {
		return nil, err
	}

	var date time.Time
	if input.Date == "" {
		date = time.Now()
	} else {
		date, err = time.Parse(time.RFC3339, input.Date)
		if err != nil {
			return nil, err
		}
	}

	notificationsToSend := make([]Notification, 0)
	for _, notificationData := range NOTIFICTION_SCHEDULE {
		sendNotification := notificationSendInExperimentDates(e, date.AddDate(0, 0, notificationData.daysAhead))
		if sendNotification {
			notificationsToSend = append(notificationsToSend, notificationData)
		}
	}

	channelIds := make([]string, 0)
	for _, notificationData := range notificationsToSend {
		sendDate := date.AddDate(0, 0, notificationData.daysAhead)
		anniversaries, err := e.engine.GetCreatorAnniversariesByDate(int(sendDate.Month()), sendDate.Day())
		if err != nil {
			return channelIds, err
		}

		for channelId, anniversary := range anniversaries {
			notificationExists, err := e.engine.ChannelHasSentAnniversaryNotification(ctx, channelId, notificationData.campaignType)
			if err != nil {
				return channelIds, err
			}
			if notificationExists {
				logrus.Infof("notification has already been sent for channel id: %s", channelId)
			} else {
				years := percy.CalculateAnniversaryYears(time.Now(), anniversary.Date)
				notificationTraceId, err := e.engine.PublishCreatorAnniversaryNotification(ctx, notificationData.campaignType, channelId, string(anniversary.CreatorCategory), years)
				if err != nil {
					return channelIds, err
				}

				err = e.engine.PutCreatorAnniversaryNotification(ctx, channelId, date, notificationTraceId, notificationData.campaignType)
				if err != nil {
					return channelIds, errors.Wrap(err, "could not save dart notification trace id to db")
				}
				channelIds = append(channelIds, channelId)
			}
		}
	}
	return channelIds, nil
}

func notificationSendInExperimentDates(e *CreatorAnniversariesNotificationPublishHandler, t time.Time) bool {
	return e.engine.IsCreatorAnniversariesExperimentEnabled(t)
}
