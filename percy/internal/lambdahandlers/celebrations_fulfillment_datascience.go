package lambdahandlers

import (
	"context"

	percy "code.justin.tv/commerce/percy/internal"
)

type DatascienceCelebrationHandler struct {
	engine      percy.CelebrationsEngine
	idempotency percy.IdempotencyEnforcer
}

// NewDatascienceCelebrationHandler ...
func NewDatascienceCelebrationHandler(engine percy.CelebrationsEngine, idempotency percy.IdempotencyEnforcer) *DatascienceCelebrationHandler {
	return &DatascienceCelebrationHandler{
		engine:      engine,
		idempotency: idempotency,
	}
}

func (e *DatascienceCelebrationHandler) Handle(ctx context.Context, input percy.CelebrationsPurchaseFulfillmentReq) error {
	err := ValidateFulfillmentRequest(input)
	if err != nil {
		return err
	}

	//nolint:gosimple
	return e.idempotency.WithIdempotency(ctx, percy.IdempotencyNamespaceCelebrationPurchaseFulfillment, input.OriginID, string(percy.CelebrationPurchaseFulfillmentDataScience), func() error {
		return e.engine.PublishDatascience(ctx, percy.CelebrationsDatascienceArgs{
			RecipientUserID:  input.RecipientUserID,
			BenefactorUserID: input.BenefactorUserID,
			OriginID:         input.OriginID,
			OfferID:          input.OfferID,
		})
	})
}
