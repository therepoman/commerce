package lambdahandlers

import (
	"context"
	"testing"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	. "github.com/smartystreets/goconvey/convey"
)

func ValidCreatorAnniversariesSevenDays() map[string]percy.CreatorAnniversary {
	validCreatorAnniversaryObject := make(map[string]percy.CreatorAnniversary)

	validCreatorAnniversaryObject["121212"] = percy.CreatorAnniversary{
		CreatorCategory: percy.AffiliateCategory,
		Date:            time.Date(2021, 2, 14, 2, 0, 0, 0, time.UTC),
	}
	validCreatorAnniversaryObject["1212123"] = percy.CreatorAnniversary{
		CreatorCategory: percy.PartnerCategory,
		Date:            time.Date(2021, 2, 14, 2, 0, 0, 0, time.UTC),
	}

	return validCreatorAnniversaryObject
}

func ValidCreatorAnniversariesTwoDays() map[string]percy.CreatorAnniversary {
	validCreatorAnniversaryObject := make(map[string]percy.CreatorAnniversary)

	validCreatorAnniversaryObject["6789998212"] = percy.CreatorAnniversary{
		CreatorCategory: percy.AffiliateCategory,
		Date:            time.Date(2021, 2, 19, 2, 0, 0, 0, time.UTC),
	}
	validCreatorAnniversaryObject["13571357"] = percy.CreatorAnniversary{
		CreatorCategory: percy.PartnerCategory,
		Date:            time.Date(2010, 2, 19, 23, 59, 59, 0, time.UTC),
	}
	validCreatorAnniversaryObject["53571357"] = percy.CreatorAnniversary{
		CreatorCategory: percy.PartnerCategory,
		Date:            time.Date(2011, 2, 19, 23, 59, 59, 0, time.UTC),
	}
	validCreatorAnniversaryObject["43571359"] = percy.CreatorAnniversary{
		CreatorCategory: percy.PartnerCategory,
		Date:            time.Date(2014, 2, 19, 23, 59, 59, 0, time.UTC),
	}
	validCreatorAnniversaryObject["33571351"] = percy.CreatorAnniversary{
		CreatorCategory: percy.PartnerCategory,
		Date:            time.Date(2019, 2, 19, 23, 59, 59, 0, time.UTC),
	}
	validCreatorAnniversaryObject["23571353"] = percy.CreatorAnniversary{
		CreatorCategory: percy.PartnerCategory,
		Date:            time.Date(2021, 2, 19, 23, 59, 59, 0, time.UTC),
	}
	return validCreatorAnniversaryObject
}

func TestCreatorAnniversariesPublishNotificationHandler_Handle(t *testing.T) {
	Convey("Given a lambda handler", t, func() {
		engine := internalfakes.FakeCreatorAnniversariesEngine{}
		engine.IsCreatorAnniversariesExperimentEnabledReturns(true)

		h := CreatorAnniversariesNotificationPublishHandler{
			engine: &engine,
		}

		Convey("the request should call GetCreatorAnniversariesByDate with the correct arguments if the date is empty", func() {
			res, err := h.Handle(context.Background(), percy.CreatorAnniversariesNotificationPublishReq{Date: ""})
			now := time.Now()
			currMonth, currDayPlus7 := engine.GetCreatorAnniversariesByDateArgsForCall(0)
			expectedDate := now.AddDate(0, 0, 7)
			So(currMonth, ShouldEqual, expectedDate.Month())
			So(currDayPlus7, ShouldEqual, expectedDate.Day())
			currMonth, currDayPlus2 := engine.GetCreatorAnniversariesByDateArgsForCall(1)
			expectedDate = now.AddDate(0, 0, 2)
			So(currMonth, ShouldEqual, expectedDate.Month())
			So(currDayPlus2, ShouldEqual, expectedDate.Day())
			So(res, ShouldNotBeNil)
			So(err, ShouldBeNil)
		})

		Convey("returns success if", func() {
			creatorAnniversariesSevenDays := ValidCreatorAnniversariesSevenDays()
			creatorAnniversariesTwoDays := ValidCreatorAnniversariesTwoDays()
			expectedResults := make([]string, 0)
			for channel := range creatorAnniversariesSevenDays {
				expectedResults = append(expectedResults, channel)
			}
			for channel := range creatorAnniversariesTwoDays {
				expectedResults = append(expectedResults, channel)
			}

			engine.GetCreatorAnniversariesByDateCalls(func(month int, day int) (map[string]percy.CreatorAnniversary, error) {
				if month == 10 && day == 19 {
					return creatorAnniversariesSevenDays, nil
				} else if month == 10 && day == 14 {
					return creatorAnniversariesTwoDays, nil
				}
				return map[string]percy.CreatorAnniversary{}, nil
			})

			Convey("the request receives a date on October 12th, 2019 in rfc3339 format", func() {
				res, err := h.Handle(context.Background(), percy.CreatorAnniversariesNotificationPublishReq{Date: "2019-10-12T07:20:50.52Z"})
				for _, channel := range res {
					So(res, ShouldContain, channel)
				}
				So(len(res), ShouldEqual, len(expectedResults))
				So(res, ShouldNotBeNil)
				So(err, ShouldBeNil)
			})

			Convey("the request will not publish any notifications if notificationSendInExperimentDates returns false", func() {
				engine.IsCreatorAnniversariesExperimentEnabledReturns(false)
				res, err := h.Handle(context.Background(), percy.CreatorAnniversariesNotificationPublishReq{Date: "2019-10-12T07:20:50.52Z"})
				So(len(res), ShouldEqual, 0)
				So(err, ShouldBeNil)
			})
		})

		Convey("we should error if", func() {
			Convey("the request date is", func() {
				Convey("in words with a time", func() {
					res, err := h.Handle(context.Background(), percy.CreatorAnniversariesNotificationPublishReq{
						Date: "November 19th 2000, 3:52:12 pm",
					})
					So(res, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})
				Convey("in words", func() {
					res, err := h.Handle(context.Background(), percy.CreatorAnniversariesNotificationPublishReq{
						Date: "July 15th 2021",
					})
					So(res, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})
				Convey("in MMDDYYYY", func() {
					res, err := h.Handle(context.Background(), percy.CreatorAnniversariesNotificationPublishReq{
						Date: "11192000",
					})
					So(res, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})
				Convey("in rfc3339 format with timezone conversion", func() {
					res, err := h.Handle(context.Background(), percy.CreatorAnniversariesNotificationPublishReq{Date: "2019-10-12T07:20:50.52Z+03:00"})
					for _, channel := range res {
						So(res, ShouldContain, channel)
					}
					So(res, ShouldBeNil)
					So(err, ShouldNotBeNil)
				})
			})
		})
	})
}
