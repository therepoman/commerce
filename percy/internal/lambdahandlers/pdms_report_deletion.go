package lambdahandlers

import (
	"context"
	"time"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	"github.com/pkg/errors"
)

type reportDeletionLambdaHandler struct {
	Privacy percy.Privacy
}

func NewReportDeletionLambdaHandler(privacy percy.Privacy) *reportDeletionLambdaHandler {
	return &reportDeletionLambdaHandler{
		Privacy: privacy,
	}
}

func (h *reportDeletionLambdaHandler) Handle(ctx context.Context, input *percy.ReportDeletionRequest) (*percy.ReportDeletionResponse, error) {
	err := IsValidUserDeletionRequest(input)
	if err != nil {
		return nil, err
	}

	timeOfDeletion := time.Now()

	if input.IsDryRun {
		logrus.Info("Dry Run, not reporting deletion to PDMS")
	} else if !input.ReportDeletion {
		logrus.Info("not reporting deletion to PDMS due to input has it disabled")
	} else {
		err = h.Privacy.ReportDeletions(ctx, input.UserIDs, timeOfDeletion)
		if err != nil {
			return nil, errors.Wrap(err, "failed to report deletion to PDMS")
		}
	}

	return &percy.ReportDeletionResponse{
		UserDeletionResponseData: percy.UserDeletionResponseData{
			UserIDs:        input.UserIDs,
			IsDryRun:       input.IsDryRun,
			ReportDeletion: input.ReportDeletion,
		},
		Timestamp: timeOfDeletion,
	}, nil
}
