package lambdahandlers

import (
	"context"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/awsmodels"
	"github.com/pkg/errors"
)

type boostOpportunityContributionHandler struct {
	launchPad   percy.LaunchPad
	idempotency percy.IdempotencyEnforcer
}

func NewBoostOpportunityContributionHandler(launchPad percy.LaunchPad, idempotency percy.IdempotencyEnforcer) *boostOpportunityContributionHandler {
	return &boostOpportunityContributionHandler{
		launchPad:   launchPad,
		idempotency: idempotency,
	}
}

func (h *boostOpportunityContributionHandler) Handle(ctx context.Context, input awsmodels.BoostOpportunityFulfillmentInput) error {
	if err := ValidatePaidBoostOpportunityFulfillmentInput(input); err != nil {
		return errors.Wrap(err, "boost opportunity fulfillment validation error")
	}

	return h.idempotency.WithIdempotency(ctx, percy.IdempotencyNameSpaceBoostOpportunityFulfillment, input.OriginID, percy.BoostOpportunityFulfillmentRecordContribution.String(), func() error {
		_, err := h.launchPad.AddBoostOpportunityContribution(ctx, percy.BoostOpportunityContribution{
			ChannelID: input.ChannelID,
			UserID:    input.UserID,
			OriginID:  input.OriginID,
			Units:     input.Units,
			Timestamp: time.Unix(input.UnixTimeSeconds, 0),
		})
		return err
	})
}
