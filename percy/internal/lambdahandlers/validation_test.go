package lambdahandlers

import (
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidation_ValidateFulfillmentRequest(t *testing.T) {
	Convey("Given a validation util", t, func() {
		Convey("we should error if", func() {
			Convey("benefactor_user_id is empty", func() {
				err := ValidateFulfillmentRequest(percy.CelebrationsPurchaseFulfillmentReq{
					RecipientUserID: "456",
					OriginID:        "abc",
					OfferID:         "some-id",
				})
				So(err, ShouldNotBeNil)
			})
			Convey("origin_id is empty", func() {
				err := ValidateFulfillmentRequest(percy.CelebrationsPurchaseFulfillmentReq{
					BenefactorUserID: "123",
					RecipientUserID:  "456",
					OfferID:          "some-id",
				})
				So(err, ShouldNotBeNil)
			})
			Convey("offer is empty", func() {
				err := ValidateFulfillmentRequest(percy.CelebrationsPurchaseFulfillmentReq{
					BenefactorUserID: "123",
					RecipientUserID:  "456",
					OriginID:         "abc",
				})
				So(err, ShouldNotBeNil)
			})
			Convey("recipient_user_id is empty", func() {
				err := ValidateFulfillmentRequest(percy.CelebrationsPurchaseFulfillmentReq{
					BenefactorUserID: "123",
					OriginID:         "abc",
					OfferID:          "some-id",
				})
				So(err, ShouldNotBeNil)
			})
		})

		Convey("we should return nil otherwise", func() {
			err := ValidateFulfillmentRequest(percy.CelebrationsPurchaseFulfillmentReq{
				BenefactorUserID: "123",
				RecipientUserID:  "456",
				OriginID:         "abc",
				OfferID:          "some-id",
			})
			So(err, ShouldBeNil)
		})
	})
}

func TestValidation_RevocationRequest(t *testing.T) {
	Convey("Given a validation util", t, func() {
		Convey("we should error if", func() {
			Convey("benefactor_user_id is empty", func() {
				err := ValidateRevocationRequest(percy.CelebrationsPurchaseRevocationReq{
					RecipientUserID: "456",
					OriginID:        "abc",
					OfferID:         "some-id",
				})
				So(err, ShouldNotBeNil)
			})
			Convey("origin_id is empty", func() {
				err := ValidateRevocationRequest(percy.CelebrationsPurchaseRevocationReq{
					BenefactorUserID: "123",
					RecipientUserID:  "456",
					OfferID:          "some-id",
				})
				So(err, ShouldNotBeNil)
			})
			Convey("offer_id is empty", func() {
				err := ValidateRevocationRequest(percy.CelebrationsPurchaseRevocationReq{
					BenefactorUserID: "123",
					RecipientUserID:  "456",
					OriginID:         "abc",
				})
				So(err, ShouldNotBeNil)
			})
			Convey("recipient_user_id is empty", func() {
				err := ValidateRevocationRequest(percy.CelebrationsPurchaseRevocationReq{
					BenefactorUserID: "123",
					OriginID:         "abc",
					OfferID:          "some-id",
				})
				So(err, ShouldNotBeNil)
			})
		})

		Convey("we should return nil otherwise", func() {
			err := ValidateRevocationRequest(percy.CelebrationsPurchaseRevocationReq{
				BenefactorUserID: "123",
				RecipientUserID:  "456",
				OriginID:         "abc",
				OfferID:          "some-id",
			})
			So(err, ShouldBeNil)
		})
	})
}

func TestIsValidUserDeletionRequest(t *testing.T) {
	Convey("given a validation function for PDMS step function input", t, func() {
		Convey("returns error when", func() {
			// a struct that implements the UserDeletionRequest interface
			var input *percy.CleanBadgesRecordsRequest

			Convey("input is nil", func() {
				input = nil
			})
			Convey("list of user IDs is empty", func() {
				input = &percy.CleanBadgesRecordsRequest{}
			})
			Convey("list of user IDs contains empty IDs", func() {
				input = &percy.CleanBadgesRecordsRequest{
					UserDeletionRequestData: percy.UserDeletionRequestData{
						UserIDs: []string{"123123123", ""},
					},
				}
			})

			err := IsValidUserDeletionRequest(input)
			So(err, ShouldNotBeNil)
		})

		Convey("returns nil when", func() {
			var input *percy.CleanBadgesRecordsRequest

			Convey("input is valid", func() {
				input = &percy.CleanBadgesRecordsRequest{
					UserDeletionRequestData: percy.UserDeletionRequestData{
						UserIDs: []string{"123123123", "456456456"},
					},
				}
			})

			err := IsValidUserDeletionRequest(input)
			So(err, ShouldBeNil)
		})
	})
}
