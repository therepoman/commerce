package lambdahandlers

import (
	"context"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/awsmodels"
	"github.com/pkg/errors"
)

type boostOpportunityDataScienceHandler struct {
	launchPad   percy.LaunchPad
	idempotency percy.IdempotencyEnforcer
}

func NewBoostOpportunityDataScienceHandler(launchPad percy.LaunchPad, idempotency percy.IdempotencyEnforcer) *boostOpportunityDataScienceHandler {
	return &boostOpportunityDataScienceHandler{
		launchPad:   launchPad,
		idempotency: idempotency,
	}
}

func (h *boostOpportunityDataScienceHandler) Handle(ctx context.Context, input awsmodels.BoostOpportunityFulfillmentInput) error {
	if err := ValidatePaidBoostOpportunityFulfillmentInput(input); err != nil {
		return errors.Wrap(err, "boost opportunity fulfillment validation error")
	}

	return h.idempotency.WithIdempotency(ctx, percy.IdempotencyNameSpaceBoostOpportunityFulfillment, input.OriginID, percy.BoostOpportunityFulfillmentDataScience.String(), func() error {
		return nil
	})
}
