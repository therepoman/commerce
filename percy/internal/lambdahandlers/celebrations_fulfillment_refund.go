package lambdahandlers

import (
	"context"

	percy "code.justin.tv/commerce/percy/internal"
)

type RefundCelebrationHandler struct {
	engine      percy.CelebrationsEngine
	idempotency percy.IdempotencyEnforcer
}

// NewRefundCelebrationHandler ...
func NewRefundCelebrationHandler(engine percy.CelebrationsEngine, idempotency percy.IdempotencyEnforcer) *RefundCelebrationHandler {
	return &RefundCelebrationHandler{
		engine:      engine,
		idempotency: idempotency,
	}
}

func (e *RefundCelebrationHandler) Handle(ctx context.Context, input percy.CelebrationsPurchaseFulfillmentReq) error {
	err := ValidateFulfillmentRequest(input)
	if err != nil {
		return err
	}

	//nolint:gosimple
	return e.idempotency.WithIdempotency(ctx, percy.IdempotencyNamespaceCelebrationPurchaseFulfillment, input.OriginID, string(percy.CelebrationPurchaseFulfillmentRefundPurchase), func() error {
		return e.engine.Refund(ctx, percy.CelebrationsRefundArgs{
			RecipientUserID:  input.RecipientUserID,
			BenefactorUserID: input.BenefactorUserID,
			OriginID:         input.OriginID,
			OfferID:          input.OfferID,
		})
	})
}
