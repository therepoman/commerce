package lambdahandlers

import (
	"context"
	"sync"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	"golang.org/x/sync/errgroup"
)

type cleanConductorsDBLambdaHandler struct {
	ConductorsDB percy.ConductorsDB
}

func NewCleanConductorsDBLambdaHandler(conductorsDB percy.ConductorsDB) *cleanConductorsDBLambdaHandler {
	return &cleanConductorsDBLambdaHandler{
		ConductorsDB: conductorsDB,
	}
}

func (h *cleanConductorsDBLambdaHandler) Handle(ctx context.Context, input *percy.CleanConductorsRecordsRequest) (*percy.CleanConductorsRecordsResponse, error) {
	err := IsValidUserDeletionRequest(input)
	if err != nil {
		return nil, err
	}

	conductorRecordUpdates := &sync.Map{}
	errs, egCtx := errgroup.WithContext(ctx)
	for _, userID := range input.UserIDs {
		u := userID

		if input.IsDryRun {
			logrus.Info("not initiating delete for channel record, was dry run")
		} else {
			errs.Go(func() error {
				return h.ConductorsDB.DeleteConductorsRecord(egCtx, u)
			})
		}

		errs.Go(func() error {
			cursor := ""
			for {
				channelIDs, nextCursor, err := h.ConductorsDB.GetConductorChannelsForBitsConductorID(egCtx, u, cursor)
				if err != nil {
					return err
				}

				for _, channelID := range channelIDs {
					record, ok := conductorRecordUpdates.Load(channelID)
					if !ok {
						conductorRecordUpdates.Store(channelID, percy.CleanConductorsInput{
							CleanBitsConductorID: true,
						})
					} else {
						castedRecord := record.(percy.CleanConductorsInput)
						castedRecord.CleanBitsConductorID = true
						conductorRecordUpdates.Store(channelID, castedRecord)
					}
				}

				if nextCursor == "" {
					break
				}
				cursor = nextCursor
			}
			return nil
		})

		errs.Go(func() error {
			cursor := ""
			for {
				channelIDs, nextCursor, err := h.ConductorsDB.GetConductorChannelsForSubsConductorID(egCtx, u, cursor)
				if err != nil {
					return err
				}

				for _, channelID := range channelIDs {
					record, ok := conductorRecordUpdates.Load(channelID)
					if !ok {
						conductorRecordUpdates.Store(channelID, percy.CleanConductorsInput{
							CleanSubsConductorID: true,
						})
					} else {
						castedRecord := record.(percy.CleanConductorsInput)
						castedRecord.CleanSubsConductorID = true
						conductorRecordUpdates.Store(channelID, castedRecord)
					}
				}

				if nextCursor == "" {
					break
				}
				cursor = nextCursor
			}
			return nil
		})
	}

	if err := errs.Wait(); err != nil {
		return nil, err
	}

	cleanConductorsInput := map[string]percy.CleanConductorsInput{}
	conductorRecordUpdates.Range(func(key, value interface{}) bool {
		channelID := key.(string)
		input := value.(percy.CleanConductorsInput)
		cleanConductorsInput[channelID] = input
		return true
	})

	if input.IsDryRun {
		logrus.Info("not cleaning conductor records for user IDs, was dry run")
	} else {
		err := h.ConductorsDB.CleanConductorsRecords(ctx, cleanConductorsInput)
		if err != nil {
			return nil, err
		}
	}

	return &percy.CleanConductorsRecordsResponse{
		UserDeletionResponseData: percy.UserDeletionResponseData{
			UserIDs:        input.UserIDs,
			IsDryRun:       input.IsDryRun,
			ReportDeletion: input.ReportDeletion,
		},
	}, nil
}
