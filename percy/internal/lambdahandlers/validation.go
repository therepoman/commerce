package lambdahandlers

import (
	"errors"
	"strings"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/awsmodels"
)

func ValidateFulfillmentRequest(req percy.CelebrationsPurchaseFulfillmentReq) error {
	if strings.TrimSpace(req.RecipientUserID) == "" {
		return errors.New("missing recipient_user_id in fulfillment req")
	}

	if strings.TrimSpace(req.BenefactorUserID) == "" {
		return errors.New("missing benefactor_user_id in fulfillment req")
	}

	if strings.TrimSpace(req.OriginID) == "" {
		return errors.New("missing origin_id in fulfillment req")
	}

	if strings.TrimSpace(req.OfferID) == "" {
		return errors.New("missing offer_id in fulfillment req")
	}

	return nil
}

func ValidateRevocationRequest(req percy.CelebrationsPurchaseRevocationReq) error {
	if strings.TrimSpace(req.RecipientUserID) == "" {
		return errors.New("missing recipient_user_id in revocation req")
	}

	if strings.TrimSpace(req.BenefactorUserID) == "" {
		return errors.New("missing benefactor_user_id in revocation req")
	}

	if strings.TrimSpace(req.OriginID) == "" {
		return errors.New("missing origin_id in revocation req")
	}

	if strings.TrimSpace(req.OfferID) == "" {
		return errors.New("missing offer_id in revocation req")
	}

	return nil
}

func IsValidUserDeletionRequest(input percy.UserDeletionRequest) error {
	req := input.GetUserDeletionRequestData()

	if req == nil {
		return errors.New("nil request")
	}

	if len(req.UserIDs) < 1 {
		return errors.New("no users provided")
	}

	for _, userID := range req.UserIDs {
		if userID == "" {
			return errors.New("user IDs cannot be blank")
		}
	}

	return nil
}

func IsValidCreatorAnniversaryNotificationPublishRequest(input percy.CreatorAnniversariesNotificationPublishReq) error {
	if input.Date != "" {
		_, err := time.Parse(time.RFC3339, input.Date)
		if err != nil {
			return err
		}
	}

	return nil
}

func ValidatePaidBoostOpportunityFulfillmentInput(input awsmodels.BoostOpportunityFulfillmentInput) error {
	if input.ChannelID == "" {
		return errors.New("channel ID must not be empty")
	}

	if input.UserID == "" {
		return errors.New("user ID must not be empty")
	}

	if input.OriginID == "" {
		return errors.New("origin ID must not be empty")
	}

	if input.Units <= 0 {
		return errors.New("units must be greater than 0")
	}

	return nil
}
