package lambdahandlers

import (
	"context"
	"errors"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCleanHypeTrainConfigsDBLambdaHandler_Handle(t *testing.T) {
	Convey("given a clean hype train configs DB lambda handler", t, func() {
		hypeTrainConfigsDB := &internalfakes.FakeHypeTrainConfigsDB{}

		h := NewCleanHypeTrainConfigsDBLambdaHandler(hypeTrainConfigsDB)

		ctx := context.Background()
		userID := "123123123"
		userIDs := []string{userID}

		Convey("returns error", func() {
			var input *percy.CleanHypeTrainConfigsRecordsRequest

			Convey("when the input is invalid", func() {
				input = nil
			})

			Convey("when we fail to delete user records when it's a real run", func() {
				input = &percy.CleanHypeTrainConfigsRecordsRequest{
					UserDeletionRequestData: percy.UserDeletionRequestData{
						UserIDs: userIDs,
					},
				}

				hypeTrainConfigsDB.DeleteConfigRecordsReturns(errors.New("WALRUS STRIKE"))
			})

			_, err := h.Handle(ctx, input)
			So(err, ShouldNotBeNil)
		})

		Convey("returns success", func() {
			var input *percy.CleanHypeTrainConfigsRecordsRequest
			var numberOfCalls int

			Convey("when it is a dry run", func() {
				input = &percy.CleanHypeTrainConfigsRecordsRequest{
					UserDeletionRequestData: percy.UserDeletionRequestData{
						UserIDs:  userIDs,
						IsDryRun: true,
					},
				}
				numberOfCalls = 0
			})

			Convey("when it's the real run", func() {
				input = &percy.CleanHypeTrainConfigsRecordsRequest{
					UserDeletionRequestData: percy.UserDeletionRequestData{
						UserIDs: userIDs,
					},
				}
				numberOfCalls = 1
			})

			output, err := h.Handle(ctx, input)
			So(err, ShouldBeNil)
			So(output, ShouldNotBeNil)
			So(output.UserIDs, ShouldResemble, input.UserIDs)
			So(hypeTrainConfigsDB.DeleteConfigRecordsCallCount(), ShouldEqual, numberOfCalls)
		})
	})
}
