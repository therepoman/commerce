package lambdahandlers

import (
	"context"
	"sync"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	"golang.org/x/sync/errgroup"
)

type cleanBadgesDBLambdaHandler struct {
	BadgesDB percy.BadgesDB
}

func NewCleanBadgesDBLambdaHandler(badgesDB percy.BadgesDB) *cleanBadgesDBLambdaHandler {
	return &cleanBadgesDBLambdaHandler{
		BadgesDB: badgesDB,
	}
}

func (h *cleanBadgesDBLambdaHandler) Handle(ctx context.Context, input *percy.CleanBadgesRecordsRequest) (*percy.CleanBadgesRecordsResponse, error) {
	err := IsValidUserDeletionRequest(input)
	if err != nil {
		return nil, err
	}

	recordsToDelete := map[string][]string{}
	errs, egCtx := errgroup.WithContext(ctx)
	mu := &sync.RWMutex{}
	for _, userID := range input.UserIDs {
		recordsToDelete[userID] = make([]string, 0)
		u := userID

		errs.Go(func() error {
			cursor := ""
			for {
				badgeChannels, nextCursor, err := h.BadgesDB.GetAllBadgeChannelsForUserID(egCtx, u, cursor)
				if err != nil {
					return err
				}

				mu.Lock()
				recordsToDelete[u] = append(recordsToDelete[u], badgeChannels...)
				mu.Unlock()

				if nextCursor == "" {
					break
				}
				cursor = nextCursor
			}
			return nil
		})

		errs.Go(func() error {
			cursor := ""
			for {
				badgeUsers, nextCursor, err := h.BadgesDB.GetAllBadgeUsersForChannelID(egCtx, u, cursor)
				if err != nil {
					return err
				}

				mu.Lock()
				for _, user := range badgeUsers {
					recordsToDelete[user] = append(recordsToDelete[user], u)
				}
				mu.Unlock()

				if nextCursor == "" {
					break
				}
				cursor = nextCursor
			}
			return nil
		})
	}

	if err := errs.Wait(); err != nil {
		return nil, err
	}

	if input.IsDryRun {
		logrus.Info("not deleting badge records, was dry run")
	} else {
		err = h.BadgesDB.DeleteBulkBadges(ctx, recordsToDelete)
		if err != nil {
			return nil, err
		}
	}

	return &percy.CleanBadgesRecordsResponse{
		UserDeletionResponseData: percy.UserDeletionResponseData{
			UserIDs:        input.UserIDs,
			IsDryRun:       input.IsDryRun,
			ReportDeletion: input.ReportDeletion,
		},
	}, nil
}
