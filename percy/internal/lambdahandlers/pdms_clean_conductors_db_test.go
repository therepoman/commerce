package lambdahandlers

import (
	"context"
	"errors"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCleanConductorsDBLambdaHandler_Handle(t *testing.T) {
	Convey("given a clean conductors DB lambda handler", t, func() {
		conductorsDB := &internalfakes.FakeConductorsDB{}

		h := NewCleanConductorsDBLambdaHandler(conductorsDB)

		ctx := context.Background()
		userID := "123123123"
		userIDs := []string{userID}
		channelID1 := "45454545"
		channelID2 := "232323232"
		userID1 := "6767657"
		userID2 := "78787878787"

		Convey("returns error", func() {
			var input *percy.CleanConductorsRecordsRequest

			Convey("when the input is invalid", func() {
				input = nil
			})

			Convey("when we fail to look up records by bits conductor ID", func() {
				input = &percy.CleanConductorsRecordsRequest{
					UserDeletionRequestData: percy.UserDeletionRequestData{
						UserIDs: userIDs,
					},
				}

				conductorsDB.GetConductorChannelsForBitsConductorIDReturns(nil, "", errors.New("WALRUS STRIKE"))
			})

			Convey("when we fail to look up records by subs conductor ID", func() {
				input = &percy.CleanConductorsRecordsRequest{
					UserDeletionRequestData: percy.UserDeletionRequestData{
						UserIDs: userIDs,
					},
				}

				conductorsDB.GetConductorChannelsForSubsConductorIDReturns(nil, "", errors.New("WALRUS STRIKE"))
			})

			Convey("when we fail to delete channel records when it's a real run", func() {
				input = &percy.CleanConductorsRecordsRequest{
					UserDeletionRequestData: percy.UserDeletionRequestData{
						UserIDs: userIDs,
					},
				}

				conductorsDB.DeleteConductorsRecordReturns(errors.New("WALRUS STRIKE"))
			})

			Convey("when we fail to clean conductor records when it's a real run", func() {
				input = &percy.CleanConductorsRecordsRequest{
					UserDeletionRequestData: percy.UserDeletionRequestData{
						UserIDs: userIDs,
					},
				}

				conductorsDB.GetConductorChannelsForBitsConductorIDReturns([]string{userID1, userID2}, "", nil)
				conductorsDB.GetConductorChannelsForSubsConductorIDReturns([]string{channelID1, channelID2}, "", nil)
				conductorsDB.CleanConductorsRecordsReturns(errors.New("WALRUS STRIKE"))
			})

			_, err := h.Handle(ctx, input)
			So(err, ShouldNotBeNil)
		})

		Convey("returns success", func() {
			var input *percy.CleanConductorsRecordsRequest
			var numberOfCalls int

			Convey("when it is a dry run", func() {
				input = &percy.CleanConductorsRecordsRequest{
					UserDeletionRequestData: percy.UserDeletionRequestData{
						UserIDs:  userIDs,
						IsDryRun: true,
					},
				}
				numberOfCalls = 0
			})

			Convey("when it's the real run", func() {
				input = &percy.CleanConductorsRecordsRequest{
					UserDeletionRequestData: percy.UserDeletionRequestData{
						UserIDs: userIDs,
					},
				}
				numberOfCalls = 1
			})

			output, err := h.Handle(ctx, input)
			So(err, ShouldBeNil)
			So(output, ShouldNotBeNil)
			So(output.UserIDs, ShouldResemble, input.UserIDs)
			So(conductorsDB.DeleteConductorsRecordCallCount(), ShouldEqual, numberOfCalls)
			So(conductorsDB.CleanConductorsRecordsCallCount(), ShouldEqual, numberOfCalls)
		})
	})
}
