package lambdahandlers

import (
	"context"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestDatascienceCelebrationHandler_Handle(t *testing.T) {
	Convey("Given a Lambda handler", t, func() {
		engine := &internalfakes.FakeCelebrationsEngine{}
		idempotency := internalfakes.FakeIdempotencyEnforcer{}

		h := DatascienceCelebrationHandler{
			engine:      engine,
			idempotency: &idempotency,
		}

		Convey("should succeed when we have a valid request and process the request successfully", func() {
			err := h.Handle(context.Background(), percy.CelebrationsPurchaseFulfillmentReq{
				BenefactorUserID: "123",
				RecipientUserID:  "456",
				OriginID:         "abc",
				OfferID:          "some-id",
			})
			So(err, ShouldBeNil)
		})

		Convey("should error when", func() {
			Convey("the request fails validation", func() {
				err := h.Handle(context.Background(), percy.CelebrationsPurchaseFulfillmentReq{})
				So(err, ShouldNotBeNil)
			})

			Convey("engine#PublishDatascience returns an error", func() {
				idempotency.WithIdempotencyReturns(errors.New("ERROR"))

				err := h.Handle(context.Background(), percy.CelebrationsPurchaseFulfillmentReq{
					BenefactorUserID: "123",
					RecipientUserID:  "456",
					OriginID:         "abc",
					OfferID:          "some-id",
				})
				So(err, ShouldNotBeNil)
			})
		})
	})
}
