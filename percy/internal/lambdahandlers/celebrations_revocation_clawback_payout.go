package lambdahandlers

import (
	"context"

	percy "code.justin.tv/commerce/percy/internal"
)

type ClawbackPayoutCelebrationHandler struct {
	engine      percy.CelebrationsEngine
	idempotency percy.IdempotencyEnforcer
}

// NewClawbackPayoutCelebrationHandler ...
func NewClawbackPayoutCelebrationHandler(engine percy.CelebrationsEngine, idempotency percy.IdempotencyEnforcer) *ClawbackPayoutCelebrationHandler {
	return &ClawbackPayoutCelebrationHandler{
		engine:      engine,
		idempotency: idempotency,
	}
}

func (e *ClawbackPayoutCelebrationHandler) Handle(ctx context.Context, input percy.CelebrationsPurchaseRevocationReq) error {
	err := ValidateRevocationRequest(input)
	if err != nil {
		return err
	}

	return e.idempotency.WithIdempotency(ctx, percy.IdempotencyNamespaceCelebrationRevocation, input.OriginID, string(percy.CelebrationRevocationClawbackPayout), func() error {
		return e.engine.ClawbackPayout(ctx, percy.CelebrationsPayoutArgs{
			IsClawback:       true,
			RecipientUserID:  input.RecipientUserID,
			BenefactorUserID: input.BenefactorUserID,
			OriginID:         input.OriginID,
			OfferID:          input.OfferID,
		})
	})
}
