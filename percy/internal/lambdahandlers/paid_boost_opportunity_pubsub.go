package lambdahandlers

import (
	"context"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/awsmodels"
	"github.com/pkg/errors"
)

type boostOpportunityPubsubHandler struct {
	launchPad   percy.LaunchPad
	idempotency percy.IdempotencyEnforcer
}

func NewBoostOpportunityPubsubHandler(launchPad percy.LaunchPad, idempotency percy.IdempotencyEnforcer) *boostOpportunityPubsubHandler {
	return &boostOpportunityPubsubHandler{
		launchPad:   launchPad,
		idempotency: idempotency,
	}
}

func (h *boostOpportunityPubsubHandler) Handle(ctx context.Context, input awsmodels.BoostOpportunityFulfillmentInput) error {
	if err := ValidatePaidBoostOpportunityFulfillmentInput(input); err != nil {
		return errors.Wrap(err, "boost opportunity fulfillment validation error")
	}

	return h.idempotency.WithIdempotency(ctx, percy.IdempotencyNameSpaceBoostOpportunityFulfillment, input.OriginID, percy.BoostOpportunityFulfillmentSendPubSub.String(), func() error {
		return nil
	})
}
