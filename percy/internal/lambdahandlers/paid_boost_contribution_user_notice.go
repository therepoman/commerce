package lambdahandlers

import (
	"context"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/awsmodels"
	"github.com/pkg/errors"
)

type PaidBoostOpportunityUserNoticeHandler struct {
	launchPad   percy.LaunchPad
	idempotency percy.IdempotencyEnforcer
}

// NewPaidBoostOpportunityUserNoticeHandler ...
func NewPaidBoostOpportunityUserNoticeHandler(launchPad percy.LaunchPad, idempotency percy.IdempotencyEnforcer) *PaidBoostOpportunityUserNoticeHandler {
	return &PaidBoostOpportunityUserNoticeHandler{
		launchPad:   launchPad,
		idempotency: idempotency,
	}
}

func (e *PaidBoostOpportunityUserNoticeHandler) Handle(ctx context.Context, input awsmodels.BoostOpportunityFulfillmentInput) error {
	if err := ValidatePaidBoostOpportunityFulfillmentInput(input); err != nil {
		return errors.Wrap(err, "boost opportunity fulfillment validation error")
	}

	//nolint:gosimple
	return e.idempotency.WithIdempotency(ctx, percy.IdempotencyNamespaceCelebrationPurchaseFulfillment, input.OriginID, string(percy.BoostOpportunityFulfillmentSendUserNotice), func() error {
		return e.launchPad.SendBoostOpportunityContributionUserNotice(ctx, percy.BoostOpportunityContribution{
			ChannelID: input.ChannelID,
			UserID:    input.UserID,
			OriginID:  input.OriginID,
			Units:     input.Units,
			Timestamp: time.Unix(input.UnixTimeSeconds, 0),
		})
	})
}
