package pubsub

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"testing"
	"time"

	makotwirp "code.justin.tv/commerce/mako/twirp"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/mako/makofakes"
	"code.justin.tv/commerce/percy/internal/miniexperiments/miniexperimentsfakes"
	"code.justin.tv/commerce/percy/internal/pubsub/pubsubfakes"
	"code.justin.tv/commerce/percy/internal/pubsub/rewards/rewardsfakes"
	"code.justin.tv/commerce/percy/internal/slack/slackfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPublisher_StartHypeTrain(t *testing.T) {
	Convey("given a publisher", t, func() {
		pubClient := &pubsubfakes.FakePubClient{}
		slacker := &slackfakes.FakeSlacker{}
		userProfileDB := &internalfakes.FakeUserProfileDB{}
		rewardDisplayInfoAttacher := &rewardsfakes.FakeDisplayInfoAttacher{}
		miniexperimentsClient := &miniexperimentsfakes.FakeClient{}

		publisher := Publisher{
			client:                       pubClient,
			slacker:                      slacker,
			userProfileDB:                userProfileDB,
			rewardsDisplayInfoAttacher:   rewardDisplayInfoAttacher,
			celebrationsExperimentClient: miniexperimentsClient,
			approachingExperimentClient:  miniexperimentsClient,
		}

		hypeTrain := tests.ValidHypeTrain()
		ctx := context.Background()

		Convey("returns an error when pubsub errors", func() {
			pubClient.PublishReturns(errors.New("WALRUS STRIKE"))

			err := publisher.StartHypeTrain(ctx, hypeTrain)
			So(err, ShouldNotBeNil)
		})

		Convey("returns nil when pubsub succeeds", func() {
			pubClient.PublishReturns(nil)

			err := publisher.StartHypeTrain(ctx, hypeTrain)
			So(err, ShouldBeNil)

			actualCtx, actualTopics, actualBody, _ := pubClient.PublishArgsForCall(0)
			So(actualCtx, ShouldEqual, ctx)
			So(actualTopics, ShouldHaveLength, 1)
			So(actualTopics[0], ShouldEqual, fmt.Sprintf(hypeTrainEventsTopicFormat, hypeTrain.ChannelID))

			var msg percy.HypeTrainStartMessage
			err = json.Unmarshal([]byte(actualBody), &msg)
			So(err, ShouldBeNil)
			So(msg.MessageType, ShouldEqual, hypeTrainStartMessageType)

			// since this is in a goroutine, we delay the check
			time.Sleep(10 * time.Millisecond)
			So(slacker.PostCallCount(), ShouldEqual, 1)
			So(miniexperimentsClient.GetTreatmentCallCount(), ShouldEqual, 2)
		})
	})
}

func TestPublisher_PostHypeTrainProgression(t *testing.T) {
	Convey("given a publisher", t, func() {
		pubClient := &pubsubfakes.FakePubClient{}
		slacker := &slackfakes.FakeSlacker{}
		userProfileDB := &internalfakes.FakeUserProfileDB{}
		rewardDisplayInfoAttacher := &rewardsfakes.FakeDisplayInfoAttacher{}
		miniexperimentsClient := &miniexperimentsfakes.FakeClient{}

		publisher := Publisher{
			client:                       pubClient,
			slacker:                      slacker,
			userProfileDB:                userProfileDB,
			rewardsDisplayInfoAttacher:   rewardDisplayInfoAttacher,
			celebrationsExperimentClient: miniexperimentsClient,
			approachingExperimentClient:  miniexperimentsClient,
		}

		hypeTrain := tests.ValidHypeTrain()
		ctx := context.Background()
		participation := tests.ValidParticipation()
		progress := hypeTrain.LevelProgress()

		Convey("returns an error when pubsub errors", func() {
			pubClient.PublishReturns(errors.New("WALRUS STRIKE"))

			err := publisher.PostHypeTrainProgression(ctx, participation, progress)
			So(err, ShouldNotBeNil)
		})

		Convey("returns nil when pubsub succeeds", func() {
			pubClient.PublishReturns(nil)

			err := publisher.PostHypeTrainProgression(ctx, participation, progress)
			So(err, ShouldBeNil)

			actualCtx, actualTopics, actualBody, _ := pubClient.PublishArgsForCall(0)
			So(actualCtx, ShouldEqual, ctx)
			So(actualTopics, ShouldHaveLength, 1)
			So(actualTopics[0], ShouldEqual, fmt.Sprintf(hypeTrainEventsTopicFormat, hypeTrain.ChannelID))

			var msg percy.HypeTrainProgressionMessage
			err = json.Unmarshal([]byte(actualBody), &msg)
			So(err, ShouldBeNil)
			So(msg.MessageType, ShouldEqual, hypeTrainProgressionMessageType)
		})
	})
}

func TestPublisher_LevelUpHypeTrain(t *testing.T) {
	Convey("given a publisher", t, func() {
		pubClient := &pubsubfakes.FakePubClient{}
		slacker := &slackfakes.FakeSlacker{}
		userProfileDB := &internalfakes.FakeUserProfileDB{}
		rewardDisplayInfoAttacher := &rewardsfakes.FakeDisplayInfoAttacher{}
		miniexperimentsClient := &miniexperimentsfakes.FakeClient{}

		publisher := Publisher{
			client:                       pubClient,
			slacker:                      slacker,
			userProfileDB:                userProfileDB,
			rewardsDisplayInfoAttacher:   rewardDisplayInfoAttacher,
			celebrationsExperimentClient: miniexperimentsClient,
			approachingExperimentClient:  miniexperimentsClient,
		}

		hypeTrain := tests.ValidHypeTrain()
		ctx := context.Background()
		progress := hypeTrain.LevelProgress()
		timeToExpire := time.Now().Add(5 * time.Minute)

		Convey("returns an error when pubsub errors", func() {
			pubClient.PublishReturns(errors.New("WALRUS STRIKE"))

			err := publisher.LevelUpHypeTrain(ctx, hypeTrain.ChannelID, timeToExpire, progress)
			So(err, ShouldNotBeNil)
		})

		Convey("returns nil when pubsub succeeds", func() {
			pubClient.PublishReturns(nil)

			err := publisher.LevelUpHypeTrain(ctx, hypeTrain.ChannelID, timeToExpire, progress)
			So(err, ShouldBeNil)

			actualCtx, actualTopics, actualBody, _ := pubClient.PublishArgsForCall(0)
			So(actualCtx, ShouldEqual, ctx)
			So(actualTopics, ShouldHaveLength, 1)
			So(actualTopics[0], ShouldEqual, fmt.Sprintf(hypeTrainEventsTopicFormat, hypeTrain.ChannelID))

			var msg percy.HypeTrainLevelUpMessage
			err = json.Unmarshal([]byte(actualBody), &msg)
			So(err, ShouldBeNil)
			So(msg.MessageType, ShouldEqual, hypeTrainLevelUpMessageType)
			So(msg.Data.TimeToExpire, ShouldEqual, timeToExpire.Unix()*1000)
		})
	})
}

func TestPublisher_EndHypeTrain(t *testing.T) {
	Convey("given a publisher", t, func() {
		pubClient := &pubsubfakes.FakePubClient{}
		slacker := &slackfakes.FakeSlacker{}
		userProfileDB := &internalfakes.FakeUserProfileDB{}
		rewardDisplayInfoAttacher := &rewardsfakes.FakeDisplayInfoAttacher{}
		miniexperimentsClient := &miniexperimentsfakes.FakeClient{}

		publisher := Publisher{
			client:                       pubClient,
			slacker:                      slacker,
			userProfileDB:                userProfileDB,
			rewardsDisplayInfoAttacher:   rewardDisplayInfoAttacher,
			celebrationsExperimentClient: miniexperimentsClient,
			approachingExperimentClient:  miniexperimentsClient,
		}

		hypeTrain := tests.ValidHypeTrain()
		ctx := context.Background()

		Convey("returns an error when pubsub errors", func() {
			pubClient.PublishReturns(errors.New("WALRUS STRIKE"))

			err := publisher.EndHypeTrain(ctx, hypeTrain, *hypeTrain.EndedAt, *hypeTrain.EndingReason)
			So(err, ShouldNotBeNil)
		})

		Convey("returns nil when pubsub succeeds", func() {
			pubClient.PublishReturns(nil)

			err := publisher.EndHypeTrain(ctx, hypeTrain, *hypeTrain.EndedAt, *hypeTrain.EndingReason)
			So(err, ShouldBeNil)

			actualCtx, actualTopics, actualBody, _ := pubClient.PublishArgsForCall(0)
			So(actualCtx, ShouldEqual, ctx)
			So(actualTopics, ShouldHaveLength, 1)
			So(actualTopics[0], ShouldEqual, fmt.Sprintf(hypeTrainEventsTopicFormat, hypeTrain.ChannelID))

			var msg percy.HypeTrainEndMessage
			err = json.Unmarshal([]byte(actualBody), &msg)
			So(err, ShouldBeNil)
			So(msg.MessageType, ShouldEqual, hypeTrainEndMessageType)
			timeEnded := *hypeTrain.EndedAt
			So(msg.Data.EndedAt, ShouldEqual, timeEnded.Unix()*1000)
		})
	})
}

func TestPublisher_CelebrationEmotesSelector(t *testing.T) {
	Convey("Given a celebration emotes selector", t, func() {
		subsDB := &internalfakes.FakeSubscriptionsDB{}
		emoteFetcher := &makofakes.FakeFetcher{}
		selector := celebrationEmotesSelector{
			subscriptionsDB: subsDB,
			emotesFetcher:   emoteFetcher,
		}

		Convey("with a subscription gifting celebration", func() {
			Convey("select subscription emotes for celebrations", func() {
				Convey("when there are enough channel subscription emotes", func() {
					subsDB.GetChannelEmoticonSetsReturns([]string{"1", "2", "3"}, nil)
					emoteFetcher.GetEmoteSetInformationReturns(map[string]*makotwirp.Emoticon{
						"1": {Id: "1", Code: "one"},
						"2": {Id: "2", Code: "two"},
						"3": {Id: "3", Code: "three"},
					}, nil)
				})

				Convey("when there aren't enough channel subscription emotes", func() {
					subsDB.GetChannelEmoticonSetsReturns([]string{"1"}, nil)
					emoteFetcher.GetEmoteSetInformationReturns(map[string]*makotwirp.Emoticon{
						"1": {Id: "1", Code: "one"},
					}, nil)
				})

				emotes := selector.SelectCelebrationEmotes(context.Background(), "123", percy.EventTypeSubscriptionGift)
				So(len(emotes), ShouldBeGreaterThanOrEqualTo, 3)
				So(emotes, ShouldContain, percy.Emote{ID: "1", Token: "one"})
			})

			Convey("fallback to default emotes", func() {
				Convey("when subscriptions DB fails to retrieve channel's subscription emotes", func() {
					subsDB.GetChannelEmoticonSetsReturns(nil, errors.New("can't get"))
				})

				Convey("when emote fetcher fails to retrieve emote details", func() {
					subsDB.GetChannelEmoticonSetsReturns([]string{"1", "2", "3"}, nil)
					emoteFetcher.GetEmoteSetInformationReturns(nil, errors.New("can't fetch"))
				})

				emotes := selector.SelectCelebrationEmotes(context.Background(), "123", percy.EventTypeSubscriptionGift)
				So(len(emotes), ShouldBeGreaterThanOrEqualTo, 3)
			})
		})

		Convey("with a subscription founder celebration", func() {
			Convey("select just founders badge emote", func() {
				emotes := selector.SelectCelebrationEmotes(context.Background(), "123", percy.EventTypeSubscriptionFounder)
				So(emotes, ShouldHaveLength, 1)
			})
		})
	})
}

func TestPublisher_HypeTrainApproaching(t *testing.T) {
	const (
		channelID = "bontaChannel"
		goal      = int64(5)
	)

	miniexperimentsClient := &miniexperimentsfakes.FakeClient{}

	Convey("given a publisher", t, func() {
		pubClient := &pubsubfakes.FakePubClient{}
		rewardsDisplayInfoFetcher := &rewardsfakes.FakeDisplayInfoFetcher{}
		publisher := Publisher{
			client:                      pubClient,
			approachingEnabled:          true,
			approachingExperimentClient: miniexperimentsClient,
			rewardsDisplayInfoFetcher:   rewardsDisplayInfoFetcher,
		}

		ctx := context.Background()

		Convey("returns an error when pubsub errors", func() {
			pubClient.PublishReturns(errors.New("ANIME BEARS GO ON STRIKE"))

			err := publisher.HypeTrainApproaching(ctx, channelID, goal, map[int]time.Duration{}, []percy.Reward{}, "", []string{})
			So(err, ShouldNotBeNil)
		})

		Convey("returns nil when pubsub succeeds", func() {
			pubClient.PublishReturns(nil)

			err := publisher.HypeTrainApproaching(ctx, channelID, goal, map[int]time.Duration{
				1: parseDuration("3m"),
				2: parseDuration("1m"),
			}, []percy.Reward{}, "", []string{})
			So(err, ShouldBeNil)

			actualCtx, actualTopics, actualBody, _ := pubClient.PublishArgsForCall(0)
			So(actualCtx, ShouldEqual, ctx)
			So(actualTopics, ShouldHaveLength, 1)
			So(actualTopics[0], ShouldEqual, fmt.Sprintf(hypeTrainEventsTopicFormat, "bontaChannel"))

			var msg percy.HypeTrainApproachingMessage
			err = json.Unmarshal([]byte(actualBody), &msg)
			So(err, ShouldBeNil)
			So(msg.MessageType, ShouldEqual, hypeTrainApproachingMessageType)
			So(msg.Data.ChannelID, ShouldEqual, channelID)
			So(msg.Data.Goal, ShouldEqual, goal)
			So(msg.Data.EventsRemainingDurations[2], ShouldEqual, int64(60))
			So(msg.Data.EventsRemainingDurations[1], ShouldEqual, int64(180))
		})
	})
}

func parseDuration(s string) time.Duration {
	duration, _ := time.ParseDuration(s)
	return duration
}
