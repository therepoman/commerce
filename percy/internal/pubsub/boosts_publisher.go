package pubsub

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/chat/pubsub-go-pubclient/client"
	percy "code.justin.tv/commerce/percy/internal"
	"github.com/pkg/errors"
)

const (
	boostOpportunityTopicFormat = "rocket-boost-channel-v1.%s"
)

const (
	boostOpportunityStartMessageType      = "boost-opportunity-start"
	boostOpportunityCompletionMessageType = "boost-opportunity-completion"
)

type BoostsPublisher struct {
	client client.PubClient
}

type BoostOpportunity struct {
	Id               string `json:"id"`
	ChannelID        string `json:"channel_id"`
	StartedAt        int64  `json:"started_at"`
	ExpiresAt        int64  `json:"expires_at"`
	RemainingSeconds int    `json:"remaining_seconds"`
	UnitsContributed int    `json:"units_contributed"`
}

type BoostOpportunityData struct {
	BoostOpportunity
	ImpressionsContributed int `json:"impressions_contributed"`
}

type BoostOpportunityStartMessage struct {
	MessageType string               `json:"type"`
	Data        BoostOpportunityData `json:"data"`
}

type BoostOpportunityCompletionMessage struct {
	MessageType string               `json:"type"`
	Data        BoostOpportunityData `json:"data"`
}

func NewBoostsPublisher(pubsubClient client.PubClient) *BoostsPublisher {
	return &BoostsPublisher{
		client: pubsubClient,
	}
}

func (bp *BoostsPublisher) PublishBoostOpportunityStart(ctx context.Context, opportunity percy.BoostOpportunity) error {
	topic := fmt.Sprintf(boostOpportunityTopicFormat, opportunity.ChannelID)

	body := BoostOpportunityStartMessage{
		MessageType: boostOpportunityStartMessageType,
		Data: BoostOpportunityData{
			BoostOpportunity: BoostOpportunity{
				Id:               opportunity.ID,
				ChannelID:        opportunity.ChannelID,
				StartedAt:        opportunity.StartedAt.Unix() * 1000,
				ExpiresAt:        opportunity.ExpiresAt.Unix() * 1000,
				RemainingSeconds: int(time.Until(opportunity.ExpiresAt).Seconds()),
				UnitsContributed: opportunity.UnitsContributed,
			},
			ImpressionsContributed: opportunity.Impressions(),
		},
	}

	bodyBytes, err := json.Marshal(&body)
	if err != nil {
		return errors.Wrap(err, "failed to marshal boost opportunity start pubsub data")
	}

	err = bp.client.Publish(ctx, []string{topic}, string(bodyBytes), nil)
	if err != nil {
		return errors.Wrap(err, "failed to publish boost opportunity start pubsub message")
	}

	return nil
}

func (bp *BoostsPublisher) PublishBoostOpportunityCompletion(ctx context.Context, opportunity percy.BoostOpportunity) error {
	topic := fmt.Sprintf(boostOpportunityTopicFormat, opportunity.ChannelID)

	body := BoostOpportunityCompletionMessage{
		MessageType: boostOpportunityCompletionMessageType,
		Data: BoostOpportunityData{
			BoostOpportunity: BoostOpportunity{
				Id:               opportunity.ID,
				ChannelID:        opportunity.ChannelID,
				StartedAt:        opportunity.StartedAt.Unix() * 1000,
				ExpiresAt:        opportunity.ExpiresAt.Unix() * 1000,
				RemainingSeconds: int(time.Until(opportunity.ExpiresAt).Seconds()),
				UnitsContributed: opportunity.UnitsContributed,
			},
			ImpressionsContributed: opportunity.Impressions(),
		},
	}

	bodyBytes, err := json.Marshal(&body)
	if err != nil {
		return errors.Wrap(err, "failed to marshal boost opportunity completion pubsub data")
	}

	err = bp.client.Publish(ctx, []string{topic}, string(bodyBytes), nil)
	if err != nil {
		return errors.Wrap(err, "failed to publish boost opportunity completion message")
	}

	return nil
}
