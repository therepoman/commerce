package pubsub

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	"code.justin.tv/commerce/percy/internal/pubsub/pubsubfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestBoostsPublisher_PublishBoostOpportunityStart(t *testing.T) {
	Convey("given a boosts publisher", t, func() {
		pubClient := &pubsubfakes.FakePubClient{}

		boostsPublisher := NewBoostsPublisher(pubClient)

		boostOpportunity := tests.ValidBoostOpportunity()
		ctx := context.Background()

		Convey("returns an error when pubsub errors", func() {
			pubClient.PublishReturns(errors.New("error in PublishBoostOpportunityStart"))

			err := boostsPublisher.PublishBoostOpportunityStart(ctx, boostOpportunity)
			So(err, ShouldNotBeNil)
		})

		Convey("returns nil when pubsub succeeds", func() {
			pubClient.PublishReturns(nil)

			err := boostsPublisher.PublishBoostOpportunityStart(ctx, boostOpportunity)
			So(err, ShouldBeNil)

			actualCtx, actualTopics, actualBody, _ := pubClient.PublishArgsForCall(0)
			So(actualCtx, ShouldEqual, ctx)
			So(actualTopics, ShouldHaveLength, 1)
			So(actualTopics[0], ShouldEqual, fmt.Sprintf(boostOpportunityTopicFormat, boostOpportunity.ChannelID))

			var msg BoostOpportunityStartMessage
			err = json.Unmarshal([]byte(actualBody), &msg)
			So(err, ShouldBeNil)
			So(msg.MessageType, ShouldEqual, boostOpportunityStartMessageType)
		})
	})
}

func TestBoostsPublisher_PublishBoostOpportunityCompletion(t *testing.T) {
	Convey("given a boosts publisher", t, func() {
		pubClient := &pubsubfakes.FakePubClient{}

		boostsPublisher := NewBoostsPublisher(pubClient)
		completedBoostOpportunity := tests.ValidCompletedBoostOpportunity()
		ctx := context.Background()

		Convey("returns an error when pubsub errors", func() {
			pubClient.PublishReturns(errors.New("error in PublishBoostOpportunityCompletion"))

			err := boostsPublisher.PublishBoostOpportunityCompletion(ctx, completedBoostOpportunity)
			So(err, ShouldNotBeNil)
		})

		Convey("returns nil when pubsub succeeds", func() {
			pubClient.PublishReturns(nil)

			err := boostsPublisher.PublishBoostOpportunityCompletion(ctx, completedBoostOpportunity)
			So(err, ShouldBeNil)

			actualCtx, actualTopics, actualBody, _ := pubClient.PublishArgsForCall(0)
			So(actualCtx, ShouldEqual, ctx)
			So(actualTopics, ShouldHaveLength, 1)
			So(actualTopics[0], ShouldEqual, fmt.Sprintf(boostOpportunityTopicFormat, completedBoostOpportunity.ChannelID))

			var msg BoostOpportunityCompletionMessage
			err = json.Unmarshal([]byte(actualBody), &msg)
			So(err, ShouldBeNil)
			So(msg.MessageType, ShouldEqual, boostOpportunityCompletionMessageType)
		})
	})
}
