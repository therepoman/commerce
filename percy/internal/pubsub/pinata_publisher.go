package pubsub

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"code.justin.tv/chat/pubsub-go-pubclient/client"
	"code.justin.tv/commerce/gogogadget/pointers"
)

const (
	bitsPinataTopicFormat       = "bits-pinata-channel-v1.%s"
	bitsPinataUpdateMessageType = "bits_pinata_update"
	bitsPinataDropMessageType   = "bits_pinata_drop"
	bitsPinataCheerMessageType  = "bits_pinata_cheer"
)

type PinataPublisher interface {
	PublishPinataDrop(ctx context.Context, pinataId, channelId string) error
	PublishPinataActivity(ctx context.Context, pinataId, channelId, userId string, bitsUsed int64, isPinataBroken bool) error
}

type PinataUpdates struct {
	Type    string         `json:"type"`
	Updates []PinataUpdate `json:"updates"`
}

type PinataUpdate struct {
	Type           string    `json:"type"`
	PinataId       string    `json:"pinata_id"`
	ChannelId      string    `json:"channel_id,omitempty"`
	UserId         *string   `json:"user_id,omitempty"`
	Time           time.Time `json:"time,omitempty"`
	BitsUsed       *int64    `json:"bits_used,omitempty"`
	IsPinataBroken *bool     `json:"is_pinata_broken,omitempty"`
}

type pinataPublisher struct {
	client client.PubClient
}

func NewPinataPublisher(pubsubClient client.PubClient) PinataPublisher {
	return &pinataPublisher{
		client: pubsubClient,
	}
}

func (p *pinataPublisher) PublishPinataDrop(ctx context.Context, pinataId, channelId string) error {
	return p.publish(ctx, PinataUpdate{
		Type:      bitsPinataDropMessageType,
		PinataId:  pinataId,
		ChannelId: channelId,
		Time:      time.Now(),
	})
}

func (p *pinataPublisher) PublishPinataActivity(ctx context.Context, pinataId, channelId, userId string, bitsUsed int64, isPinataBroken bool) error {
	return p.publish(ctx, PinataUpdate{
		Type:           bitsPinataCheerMessageType,
		PinataId:       pinataId,
		ChannelId:      channelId,
		UserId:         pointers.StringP(userId),
		Time:           time.Now(),
		BitsUsed:       pointers.Int64P(bitsUsed),
		IsPinataBroken: pointers.BoolP(isPinataBroken),
	})
}

func (p *pinataPublisher) publish(ctx context.Context, body PinataUpdate) error {
	// TODO: batch these on channel id, send once per half-second.

	// batching idea 1
	// push all updates into a channel
	// once a second grab everything from the channel and batch it
	// this won't work because we don't have exactly one percy instance

	// batching idea 2
	// push everything into a sorted set in redis by channel id
	// pull everything out of redis every 1/2 second and then remove sorted set
	// will require some for locking, hard to know when to stop reading redis entries

	// batching idea 3
	// ask fraser

	topic := fmt.Sprintf(bitsPinataTopicFormat, body.ChannelId)

	bodyBytes, err := json.Marshal(&PinataUpdates{
		Type:    bitsPinataUpdateMessageType,
		Updates: []PinataUpdate{body},
	})
	if err != nil {
		return errors.New("failed to marshal pinata pubsub data")
	}

	err = p.client.Publish(ctx, []string{topic}, string(bodyBytes), nil)
	if err != nil {
		return errors.New("failed to publish pinata pubsub message")
	}

	return nil
}
