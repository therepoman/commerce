package rewards

import (
	"context"
	"encoding/base64"
	"fmt"
	"strconv"
	"sync"

	"code.justin.tv/chat/badges/app/models"
	"code.justin.tv/commerce/logrus"
	makotwirp "code.justin.tv/commerce/mako/twirp"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/badges"
	"code.justin.tv/commerce/percy/internal/mako"
)

const (
	syncMapKeyFmt = "%s:%s"
)

type fetcher struct {
	badgesClient  percy.BadgesClient
	emotesFetcher mako.Fetcher
}

func NewDisplayInfoFetcher(client percy.BadgesClient, makoFetcher mako.Fetcher) *fetcher {
	return &fetcher{
		badgesClient:  client,
		emotesFetcher: makoFetcher,
	}
}

func syncMapKey(reward percy.Reward) string {
	return fmt.Sprintf(syncMapKeyFmt, reward.GroupID, reward.ID)
}

func (r *fetcher) DisplayInformation(ctx context.Context, channelID string, rewards []percy.Reward) map[string]percy.Reward {
	rewardData := make(map[string]percy.Reward)
	emoteRewards := make([]string, 0)
	badgeRewards := make([]percy.Badge, 0)
	for _, reward := range rewards {
		switch reward.Type {
		case percy.BadgeReward:
			badgeVersion, err := strconv.ParseInt(reward.ID, 10, 64)
			if err != nil {
				logrus.WithError(err).Error("malformed badge ID")
				continue
			}
			badgeRewards = append(badgeRewards, badges.BadgesServiceVersionToPercyBadge(badgeVersion))
			rewardData[syncMapKey(reward)] = reward
		case percy.EmoteReward:
			if len(reward.ID) == 0 {
				logrus.WithFields(logrus.Fields{
					"channelID": channelID,
					"rewards":   rewards,
				}).Warn("Found empty emote ID in reward list")
				continue
			}
			emoteRewards = append(emoteRewards, reward.ID)
			rewardData[syncMapKey(reward)] = reward
		case percy.UnknownRewards:
			fallthrough
		default:
			continue
		}
	}

	wg := sync.WaitGroup{}
	wg.Add(2)
	badgeDisplayInfo := make(map[percy.Badge]string)
	emoteDisplayInfo := make(map[string]*makotwirp.Emoticon)
	var badgesErr, emotesErr error
	go func() {
		badgeDisplayInfo, badgesErr = r.badgesClient.GetBadgeDisplayInfo(ctx, channelID, badgeRewards)
		wg.Done()
	}()
	go func() {
		emoteDisplayInfo, emotesErr = r.emotesFetcher.GetEmoteInformation(ctx, emoteRewards)
		wg.Done()
	}()

	wg.Wait()

	if badgesErr != nil {
		logrus.WithError(badgesErr).Error("failed to fetch badge assets")
	}

	if emotesErr != nil {
		logrus.WithError(emotesErr).Error("failed to fetch emote assets")
	}

	rewardsWithDisplayInfo := make(map[string]percy.Reward)
	for key, reward := range rewardData {
		switch reward.Type {
		case percy.BadgeReward:
			badgeVersion, err := strconv.ParseInt(reward.ID, 10, 64)
			if err != nil {
				logrus.WithError(err).Error("malformed badge ID")
				continue
			}
			badgeToGet := badges.BadgesServiceVersionToPercyBadge(badgeVersion)
			// From GQL ID creation
			reward.BadgeID = base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s;%s;%s", models.BadgeSetHypeTrain, reward.ID, channelID)))
			reward.ImageURL = badgeDisplayInfo[badgeToGet]

			rewardsWithDisplayInfo[key] = reward
		case percy.EmoteReward:
			emote := emoteDisplayInfo[reward.ID]
			if emote != nil {
				reward.SetID = emote.GroupId
				reward.Token = emote.Code
			}

			rewardsWithDisplayInfo[key] = reward
		case percy.UnknownRewards:
			fallthrough
		default:
			continue
		}
	}

	return rewardsWithDisplayInfo
}
