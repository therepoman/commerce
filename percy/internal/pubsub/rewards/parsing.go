package rewards

import (
	percy "code.justin.tv/commerce/percy/internal"
)

const CalloutEmoteGroupID = "CalloutEmoteGroupID"

func getRewardsFromStartData(data percy.HypeTrainStartData) []percy.Reward {
	rewards := map[percy.Reward]interface{}{}

	// Get rewards from difficulty settings
	for _, difficultySetting := range data.Config.LevelSettingsByDifficulty {
		for _, levelSetting := range difficultySetting {
			for _, reward := range levelSetting.Rewards {
				rewards[reward] = nil
			}
		}
	}

	// Get rewards from progress settings
	for _, reward := range data.Progress.Setting.Rewards {
		rewards[reward] = nil
	}

	// Get rewards from conductor list
	for _, source := range data.Config.ConductorRewards {
		for _, conductorRewards := range source {
			for _, reward := range conductorRewards {
				rewards[reward] = nil
			}
		}
	}

	// if there is a calloutEmote, add that since we need to fetch the token
	if data.Config.CalloutEmoteID != "" {
		reward := getCalloutEmoteAsReward(data.Config.CalloutEmoteID)
		rewards[reward] = nil
	}

	rewardsSet := make([]percy.Reward, len(rewards))
	i := 0
	for reward := range rewards {
		rewardsSet[i] = reward
		i++
	}

	return rewardsSet
}

func setRewardsForStartData(data percy.HypeTrainStartData, rewards map[string]percy.Reward) percy.HypeTrainStartData {
	dataWithDisplayInfo := data

	// Set rewards for difficulty settings
	for _, difficultySetting := range dataWithDisplayInfo.Config.LevelSettingsByDifficulty {
		for _, levelSetting := range difficultySetting {
			for i, reward := range levelSetting.Rewards {
				levelSetting.Rewards[i] = rewards[syncMapKey(reward)]
			}
		}
	}

	// Set rewards for progress
	for i, reward := range dataWithDisplayInfo.Progress.Setting.Rewards {
		dataWithDisplayInfo.Progress.Setting.Rewards[i] = rewards[syncMapKey(reward)]
	}

	// Set rewards for conductor rewards
	for _, source := range dataWithDisplayInfo.Config.ConductorRewards {
		for _, conductorRewards := range source {
			for i, reward := range conductorRewards {
				conductorRewards[i] = rewards[syncMapKey(reward)]
			}
		}
	}

	// if there is a calloutEmote, set the token
	if data.Config.CalloutEmoteID != "" {
		reward := getCalloutEmoteAsReward(data.Config.CalloutEmoteID)
		reward = rewards[syncMapKey(reward)]
		dataWithDisplayInfo.Config.CalloutEmoteToken = reward.Token
	}

	return dataWithDisplayInfo
}

func getRewardsFromProgress(progress percy.Progress) []percy.Reward {
	rewards := map[percy.Reward]interface{}{}

	for _, reward := range progress.Setting.Rewards {
		rewards[reward] = nil
	}

	rewardsSet := make([]percy.Reward, len(rewards))
	i := 0
	for reward := range rewards {
		rewardsSet[i] = reward
		i++
	}

	return rewardsSet
}

func setRewardsForProgress(progress percy.Progress, rewards map[string]percy.Reward) percy.Progress {
	progressWithDisplayInfo := progress

	// Set rewards for progress
	for i, reward := range progressWithDisplayInfo.Setting.Rewards {
		progressWithDisplayInfo.Setting.Rewards[i] = rewards[syncMapKey(reward)]
	}

	return progressWithDisplayInfo
}

func getRewardsFromEntitledParticipants(entitledParticipants percy.EntitledParticipants) []percy.Reward {
	rewards := map[percy.Reward]interface{}{}

	for _, entitledRewards := range entitledParticipants {
		for _, reward := range entitledRewards {
			rewards[reward] = nil
		}
	}

	rewardsSet := make([]percy.Reward, len(rewards))
	i := 0
	for reward := range rewards {
		rewardsSet[i] = reward
		i++
	}

	return rewardsSet
}

func setRewardsForEntitledParticipants(entitledParticipants percy.EntitledParticipants, rewards map[string]percy.Reward) percy.EntitledParticipants {
	entitledParticipantsWithProgress := entitledParticipants

	for _, entitledRewards := range entitledParticipantsWithProgress {
		for i, reward := range entitledRewards {
			if len(entitledRewards[i].ID) != 0 {
				entitledRewards[i] = rewards[syncMapKey(reward)]
			}
		}
	}

	return entitledParticipantsWithProgress
}

func getCalloutEmoteAsReward(calloutEmoteID string) percy.Reward {
	return percy.Reward{
		ID:      calloutEmoteID,
		GroupID: CalloutEmoteGroupID,
		Type:    percy.EmoteReward,
	}
}
