package rewards

import (
	"context"

	percy "code.justin.tv/commerce/percy/internal"
)

type attacher struct {
	fetcher DisplayInfoFetcher
}

func NewRewardsDisplayInfoAttacher(fetcher DisplayInfoFetcher) *attacher {
	return &attacher{fetcher: fetcher}
}

func (a *attacher) AttachStartDataRewardsDisplayInfo(ctx context.Context, data percy.HypeTrainStartData) percy.HypeTrainStartData {
	rewardsToFetch := getRewardsFromStartData(data)

	rewardsWithDisplayInfo := a.fetcher.DisplayInformation(ctx, data.ChannelID, rewardsToFetch)

	dataWithDisplayInfo := setRewardsForStartData(data, rewardsWithDisplayInfo)

	return dataWithDisplayInfo
}

func (a *attacher) AttachProgressRewardsDisplayInfo(ctx context.Context, channelID string, progress percy.Progress) percy.Progress {
	rewardsToFetch := getRewardsFromProgress(progress)

	rewardsWithDisplayInfo := a.fetcher.DisplayInformation(ctx, channelID, rewardsToFetch)

	progressWithDisplayInfo := setRewardsForProgress(progress, rewardsWithDisplayInfo)

	return progressWithDisplayInfo
}

func (a *attacher) AttachRewardsListDisplayInfo(ctx context.Context, channelID string, rewards []percy.Reward) []percy.Reward {
	rewardsWithDisplayInfoMap := a.fetcher.DisplayInformation(ctx, channelID, rewards)

	rewardsWithDisplayInfo := make([]percy.Reward, len(rewardsWithDisplayInfoMap))
	i := 0
	for _, reward := range rewardsWithDisplayInfoMap {
		rewardsWithDisplayInfo[i] = reward
		i++
	}

	return rewardsWithDisplayInfo
}

func (a *attacher) AttachEntitledParticipantsRewardsDisplayInfo(ctx context.Context, channelID string, entitledParticipants percy.EntitledParticipants) percy.EntitledParticipants {
	rewardsToFetch := getRewardsFromEntitledParticipants(entitledParticipants)

	rewardsWithDisplayInfo := a.fetcher.DisplayInformation(ctx, channelID, rewardsToFetch)

	return setRewardsForEntitledParticipants(entitledParticipants, rewardsWithDisplayInfo)
}
