package rewards

import (
	"fmt"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/tests"
	. "github.com/smartystreets/goconvey/convey"
)

func TestParsing_SetRewardsForProgress(t *testing.T) {
	Convey("given some parsing objects", t, func() {
		hypeTrain := tests.ValidHypeTrain()
		progress := hypeTrain.LevelProgress()

		rewardsWithData := make(map[string]percy.Reward)
		for _, reward := range progress.Setting.Rewards {
			if reward.Type == percy.EmoteReward {
				reward.SetID = "fooSet"
				reward.Token = fmt.Sprintf("foo%s", syncMapKey(reward))
			}
			rewardsWithData[syncMapKey(reward)] = reward
		}

		Convey("attaches rewards to progress", func() {
			progressWithRewards := setRewardsForProgress(progress, rewardsWithData)

			for _, reward := range progressWithRewards.Setting.Rewards {
				if reward.Type == percy.EmoteReward {
					So(reward.SetID, ShouldEqual, "fooSet")
					So(reward.Token, ShouldEqual, fmt.Sprintf("foo%s", syncMapKey(reward)))
				}
			}
		})
	})
}

func TestParsing_SetRewardsForStartData(t *testing.T) {
	Convey("given some parsing objects", t, func() {
		hypeTrain := tests.ValidHypeTrain()
		progress := hypeTrain.LevelProgress()

		hypeTrainStartData := percy.HypeTrainStartData{
			HypeTrain: hypeTrain,
			Progress:  progress,
		}

		rewards := getRewardsFromStartData(hypeTrainStartData)

		rewardsWithData := make(map[string]percy.Reward)
		for _, reward := range rewards {
			if reward.Type == percy.EmoteReward {
				reward.SetID = "fooSet"
				reward.Token = fmt.Sprintf("foo%s", syncMapKey(reward))
			}
			rewardsWithData[syncMapKey(reward)] = reward
		}

		Convey("attaches rewards to start data", func() {
			startDataWithRewards := setRewardsForStartData(hypeTrainStartData, rewardsWithData)

			for _, difficultySetting := range startDataWithRewards.Config.LevelSettingsByDifficulty {
				for _, levelSetting := range difficultySetting {
					for _, reward := range levelSetting.Rewards {
						if reward.Type == percy.EmoteReward {
							So(reward.SetID, ShouldEqual, "fooSet")
							So(reward.Token, ShouldEqual, fmt.Sprintf("foo%s", syncMapKey(reward)))
						}
					}
				}
			}

			// Get rewards from progress settings
			for _, reward := range startDataWithRewards.Progress.Setting.Rewards {
				if reward.Type == percy.EmoteReward {
					So(reward.SetID, ShouldEqual, "fooSet")
					So(reward.Token, ShouldEqual, fmt.Sprintf("foo%s", syncMapKey(reward)))
				}
			}

			// Get rewards from conductor list
			for _, source := range startDataWithRewards.Config.ConductorRewards {
				for _, conductorRewards := range source {
					for _, reward := range conductorRewards {
						if reward.Type == percy.EmoteReward {
							So(reward.SetID, ShouldEqual, "fooSet")
							So(reward.Token, ShouldEqual, fmt.Sprintf("foo%s", syncMapKey(reward)))
						}
					}
				}
			}

			// Get CalloutEmote
			reward := percy.Reward{
				ID:      hypeTrain.Config.CalloutEmoteID,
				GroupID: CalloutEmoteGroupID,
			}
			So(startDataWithRewards.Config.CalloutEmoteID, ShouldEqual, hypeTrain.Config.CalloutEmoteID)
			So(startDataWithRewards.Config.CalloutEmoteToken, ShouldEqual, fmt.Sprintf("foo%s", syncMapKey(reward)))
		})
	})
}

func TestParsing_SetRewardsForEntitledParticipants(t *testing.T) {
	Convey("given some parsing object", t, func() {
		participants := tests.ValidParticipants()
		rewards := []percy.Reward{
			{ID: "foo", Type: percy.BadgeReward, GroupID: "fooGroup"},
			{ID: "bar", Type: percy.EmoteReward, GroupID: "barGroup"},
		}
		entitledParticipants := percy.EntitledParticipants{}
		for _, participant := range participants {
			entitledParticipants[participant.User.ID()] = rewards
		}

		rewardsWithData := make(map[string]percy.Reward)
		for _, reward := range rewards {
			if reward.Type == percy.EmoteReward {
				reward.SetID = "fooSet"
				reward.Token = fmt.Sprintf("foo%s", syncMapKey(reward))
			} else if reward.Type == percy.BadgeReward {
				reward.BadgeID = "fooBadgeSet"
				reward.ImageURL = "fooURL"
			}
			rewardsWithData[syncMapKey(reward)] = reward
		}

		Convey("attaches data correctly", func() {
			entitledParticipantsWithRewards := setRewardsForEntitledParticipants(entitledParticipants, rewardsWithData)

			for _, entitledRewards := range entitledParticipantsWithRewards {
				for _, reward := range entitledRewards {
					if reward.Type == percy.EmoteReward {
						So(reward.SetID, ShouldEqual, "fooSet")
						So(reward.Token, ShouldEqual, fmt.Sprintf("foo%s", syncMapKey(reward)))
					} else if reward.Type == percy.BadgeReward {
						So(reward.BadgeID, ShouldEqual, "fooBadgeSet")
						So(reward.ImageURL, ShouldEqual, "fooURL")
					}
				}
			}
		})

		emptyRewards := []percy.Reward{
			{ID: "", Type: percy.EmoteReward, RewardLevel: 1},
		}
		entitledParticipantWithEmptyRewards := percy.EntitledParticipants{}
		for _, participant := range participants {
			entitledParticipantWithEmptyRewards[participant.User.ID()] = emptyRewards
		}
		rewardsWithDataFiltered := make(map[string]percy.Reward)

		Convey("attaches data correctly on empty reward", func() {
			entitledParticipantsWithRewards := setRewardsForEntitledParticipants(entitledParticipantWithEmptyRewards, rewardsWithDataFiltered)

			for _, entitledRewards := range entitledParticipantsWithRewards {
				for _, reward := range entitledRewards {
					So(reward.ID, ShouldEqual, "")
					So(reward.Type, ShouldEqual, percy.EmoteReward)
					So(reward.RewardLevel, ShouldEqual, 1)
				}
			}
		})
	})
}
