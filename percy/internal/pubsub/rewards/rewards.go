package rewards

import (
	"context"

	percy "code.justin.tv/commerce/percy/internal"
)

//go:generate counterfeiter . DisplayInfoAttacher

type DisplayInfoAttacher interface {
	AttachStartDataRewardsDisplayInfo(ctx context.Context, data percy.HypeTrainStartData) percy.HypeTrainStartData
	AttachProgressRewardsDisplayInfo(ctx context.Context, channelID string, progress percy.Progress) percy.Progress
	AttachRewardsListDisplayInfo(ctx context.Context, channelID string, rewards []percy.Reward) []percy.Reward
	AttachEntitledParticipantsRewardsDisplayInfo(ctx context.Context, channelID string, entitledParticipants percy.EntitledParticipants) percy.EntitledParticipants
}

//go:generate counterfeiter . DisplayInfoFetcher

type DisplayInfoFetcher interface {
	DisplayInformation(ctx context.Context, channelID string, rewards []percy.Reward) map[string]percy.Reward
}
