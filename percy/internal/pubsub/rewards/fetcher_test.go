package rewards

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/chat/badges/app/models"
	makotwirp "code.justin.tv/commerce/mako/twirp"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/mako/makofakes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestFetcher_DisplayInformation(t *testing.T) {
	Convey("given a reward display info fetcher", t, func() {
		badgesClient := &internalfakes.FakeBadgesClient{}
		emotesFetcher := &makofakes.FakeFetcher{}

		rewardsDisplayInfoAttacher := NewDisplayInfoFetcher(badgesClient, emotesFetcher)

		ctx := context.Background()
		channelID := "123123123"

		badgesReward := percy.Reward{
			ID:      "1",
			GroupID: models.BadgeSetHypeTrain,
			Type:    percy.BadgeReward,
		}

		badgeURL := "https://foo.com/img.png"
		badgesResp := map[percy.Badge]string{
			percy.CurrentConductorBadge: badgeURL,
		}

		emoteReward := percy.Reward{
			ID:   "foo",
			Type: percy.EmoteReward,
		}

		emoteCode := "fooBeans"
		emoteGroupID := "fooGroup"
		emotesResp := map[string]*makotwirp.Emoticon{
			emoteReward.ID: {
				Code:    emoteCode,
				GroupId: emoteGroupID,
			},
		}

		rewards := []percy.Reward{badgesReward, emoteReward}

		emoteRewardEmpty := percy.Reward{
			ID:          "",
			Type:        percy.EmoteReward,
			GroupID:     "",
			RewardLevel: 1,
		}

		rewardsEmpty := []percy.Reward{emoteRewardEmpty}

		Convey("when we return success", func() {
			Convey("with valid rewards", func() {
				emotesFetcher.GetEmoteInformationReturns(emotesResp, nil)
				badgesClient.GetBadgeDisplayInfoReturns(badgesResp, nil)

				actual := rewardsDisplayInfoAttacher.DisplayInformation(ctx, channelID, rewards)

				So(actual, ShouldHaveLength, 2)
				for _, res := range actual {
					if res.Type == percy.EmoteReward {
						So(res.SetID, ShouldEqual, emoteGroupID)
						So(res.Token, ShouldEqual, emoteCode)
					} else if res.Type == percy.BadgeReward {
						So(res.ImageURL, ShouldEqual, badgeURL)
					}
				}
			})
			Convey("with empty rewards", func() {
				emotesFetcher.GetEmoteInformationReturns(emotesResp, nil)
				badgesClient.GetBadgeDisplayInfoReturns(badgesResp, nil)

				actual := rewardsDisplayInfoAttacher.DisplayInformation(ctx, channelID, rewardsEmpty)

				So(actual, ShouldHaveLength, 0)
			})
		})

		Convey("when we return an error", func() {
			Convey("when both fail to call", func() {
				badgesClient.GetBadgeDisplayInfoReturns(nil, errors.New("WALRUS STRIKE"))
				emotesFetcher.GetEmoteInformationReturns(nil, errors.New("WALRUS STRIKE"))

				actual := rewardsDisplayInfoAttacher.DisplayInformation(ctx, channelID, rewards)
				for _, res := range actual {
					if res.Type == percy.EmoteReward {
						So(res.SetID, ShouldEqual, "")
						So(res.Token, ShouldEqual, "")
					} else if res.Type == percy.BadgeReward {
						So(res.ImageURL, ShouldEqual, "")
					}
				}
			})

			Convey("when badges fail to call", func() {
				badgesClient.GetBadgeDisplayInfoReturns(nil, errors.New("WALRUS STRIKE"))
				emotesFetcher.GetEmoteInformationReturns(emotesResp, nil)

				actual := rewardsDisplayInfoAttacher.DisplayInformation(ctx, channelID, rewards)
				for _, res := range actual {
					if res.Type == percy.EmoteReward {
						So(res.SetID, ShouldEqual, emoteGroupID)
						So(res.Token, ShouldEqual, emoteCode)
					} else if res.Type == percy.BadgeReward {
						So(res.ImageURL, ShouldEqual, "")
					}
				}
			})

			Convey("when mako fail to call", func() {
				badgesClient.GetBadgeDisplayInfoReturns(badgesResp, nil)
				emotesFetcher.GetEmoteInformationReturns(nil, errors.New("WALRUS STRIKE"))

				actual := rewardsDisplayInfoAttacher.DisplayInformation(ctx, channelID, rewards)
				for _, res := range actual {
					if res.Type == percy.EmoteReward {
						So(res.SetID, ShouldEqual, "")
						So(res.Token, ShouldEqual, "")
					} else if res.Type == percy.BadgeReward {
						So(res.ImageURL, ShouldEqual, badgeURL)
					}
				}
			})
		})
	})
}
