// Code generated by counterfeiter. DO NOT EDIT.
package rewardsfakes

import (
	"context"
	"sync"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/pubsub/rewards"
)

type FakeDisplayInfoAttacher struct {
	AttachEntitledParticipantsRewardsDisplayInfoStub        func(context.Context, string, percy.EntitledParticipants) percy.EntitledParticipants
	attachEntitledParticipantsRewardsDisplayInfoMutex       sync.RWMutex
	attachEntitledParticipantsRewardsDisplayInfoArgsForCall []struct {
		arg1 context.Context
		arg2 string
		arg3 percy.EntitledParticipants
	}
	attachEntitledParticipantsRewardsDisplayInfoReturns struct {
		result1 percy.EntitledParticipants
	}
	attachEntitledParticipantsRewardsDisplayInfoReturnsOnCall map[int]struct {
		result1 percy.EntitledParticipants
	}
	AttachProgressRewardsDisplayInfoStub        func(context.Context, string, percy.Progress) percy.Progress
	attachProgressRewardsDisplayInfoMutex       sync.RWMutex
	attachProgressRewardsDisplayInfoArgsForCall []struct {
		arg1 context.Context
		arg2 string
		arg3 percy.Progress
	}
	attachProgressRewardsDisplayInfoReturns struct {
		result1 percy.Progress
	}
	attachProgressRewardsDisplayInfoReturnsOnCall map[int]struct {
		result1 percy.Progress
	}
	AttachRewardsListDisplayInfoStub        func(context.Context, string, []percy.Reward) []percy.Reward
	attachRewardsListDisplayInfoMutex       sync.RWMutex
	attachRewardsListDisplayInfoArgsForCall []struct {
		arg1 context.Context
		arg2 string
		arg3 []percy.Reward
	}
	attachRewardsListDisplayInfoReturns struct {
		result1 []percy.Reward
	}
	attachRewardsListDisplayInfoReturnsOnCall map[int]struct {
		result1 []percy.Reward
	}
	AttachStartDataRewardsDisplayInfoStub        func(context.Context, percy.HypeTrainStartData) percy.HypeTrainStartData
	attachStartDataRewardsDisplayInfoMutex       sync.RWMutex
	attachStartDataRewardsDisplayInfoArgsForCall []struct {
		arg1 context.Context
		arg2 percy.HypeTrainStartData
	}
	attachStartDataRewardsDisplayInfoReturns struct {
		result1 percy.HypeTrainStartData
	}
	attachStartDataRewardsDisplayInfoReturnsOnCall map[int]struct {
		result1 percy.HypeTrainStartData
	}
	invocations      map[string][][]interface{}
	invocationsMutex sync.RWMutex
}

func (fake *FakeDisplayInfoAttacher) AttachEntitledParticipantsRewardsDisplayInfo(arg1 context.Context, arg2 string, arg3 percy.EntitledParticipants) percy.EntitledParticipants {
	fake.attachEntitledParticipantsRewardsDisplayInfoMutex.Lock()
	ret, specificReturn := fake.attachEntitledParticipantsRewardsDisplayInfoReturnsOnCall[len(fake.attachEntitledParticipantsRewardsDisplayInfoArgsForCall)]
	fake.attachEntitledParticipantsRewardsDisplayInfoArgsForCall = append(fake.attachEntitledParticipantsRewardsDisplayInfoArgsForCall, struct {
		arg1 context.Context
		arg2 string
		arg3 percy.EntitledParticipants
	}{arg1, arg2, arg3})
	stub := fake.AttachEntitledParticipantsRewardsDisplayInfoStub
	fakeReturns := fake.attachEntitledParticipantsRewardsDisplayInfoReturns
	fake.recordInvocation("AttachEntitledParticipantsRewardsDisplayInfo", []interface{}{arg1, arg2, arg3})
	fake.attachEntitledParticipantsRewardsDisplayInfoMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2, arg3)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakeDisplayInfoAttacher) AttachEntitledParticipantsRewardsDisplayInfoCallCount() int {
	fake.attachEntitledParticipantsRewardsDisplayInfoMutex.RLock()
	defer fake.attachEntitledParticipantsRewardsDisplayInfoMutex.RUnlock()
	return len(fake.attachEntitledParticipantsRewardsDisplayInfoArgsForCall)
}

func (fake *FakeDisplayInfoAttacher) AttachEntitledParticipantsRewardsDisplayInfoCalls(stub func(context.Context, string, percy.EntitledParticipants) percy.EntitledParticipants) {
	fake.attachEntitledParticipantsRewardsDisplayInfoMutex.Lock()
	defer fake.attachEntitledParticipantsRewardsDisplayInfoMutex.Unlock()
	fake.AttachEntitledParticipantsRewardsDisplayInfoStub = stub
}

func (fake *FakeDisplayInfoAttacher) AttachEntitledParticipantsRewardsDisplayInfoArgsForCall(i int) (context.Context, string, percy.EntitledParticipants) {
	fake.attachEntitledParticipantsRewardsDisplayInfoMutex.RLock()
	defer fake.attachEntitledParticipantsRewardsDisplayInfoMutex.RUnlock()
	argsForCall := fake.attachEntitledParticipantsRewardsDisplayInfoArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2, argsForCall.arg3
}

func (fake *FakeDisplayInfoAttacher) AttachEntitledParticipantsRewardsDisplayInfoReturns(result1 percy.EntitledParticipants) {
	fake.attachEntitledParticipantsRewardsDisplayInfoMutex.Lock()
	defer fake.attachEntitledParticipantsRewardsDisplayInfoMutex.Unlock()
	fake.AttachEntitledParticipantsRewardsDisplayInfoStub = nil
	fake.attachEntitledParticipantsRewardsDisplayInfoReturns = struct {
		result1 percy.EntitledParticipants
	}{result1}
}

func (fake *FakeDisplayInfoAttacher) AttachEntitledParticipantsRewardsDisplayInfoReturnsOnCall(i int, result1 percy.EntitledParticipants) {
	fake.attachEntitledParticipantsRewardsDisplayInfoMutex.Lock()
	defer fake.attachEntitledParticipantsRewardsDisplayInfoMutex.Unlock()
	fake.AttachEntitledParticipantsRewardsDisplayInfoStub = nil
	if fake.attachEntitledParticipantsRewardsDisplayInfoReturnsOnCall == nil {
		fake.attachEntitledParticipantsRewardsDisplayInfoReturnsOnCall = make(map[int]struct {
			result1 percy.EntitledParticipants
		})
	}
	fake.attachEntitledParticipantsRewardsDisplayInfoReturnsOnCall[i] = struct {
		result1 percy.EntitledParticipants
	}{result1}
}

func (fake *FakeDisplayInfoAttacher) AttachProgressRewardsDisplayInfo(arg1 context.Context, arg2 string, arg3 percy.Progress) percy.Progress {
	fake.attachProgressRewardsDisplayInfoMutex.Lock()
	ret, specificReturn := fake.attachProgressRewardsDisplayInfoReturnsOnCall[len(fake.attachProgressRewardsDisplayInfoArgsForCall)]
	fake.attachProgressRewardsDisplayInfoArgsForCall = append(fake.attachProgressRewardsDisplayInfoArgsForCall, struct {
		arg1 context.Context
		arg2 string
		arg3 percy.Progress
	}{arg1, arg2, arg3})
	stub := fake.AttachProgressRewardsDisplayInfoStub
	fakeReturns := fake.attachProgressRewardsDisplayInfoReturns
	fake.recordInvocation("AttachProgressRewardsDisplayInfo", []interface{}{arg1, arg2, arg3})
	fake.attachProgressRewardsDisplayInfoMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2, arg3)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakeDisplayInfoAttacher) AttachProgressRewardsDisplayInfoCallCount() int {
	fake.attachProgressRewardsDisplayInfoMutex.RLock()
	defer fake.attachProgressRewardsDisplayInfoMutex.RUnlock()
	return len(fake.attachProgressRewardsDisplayInfoArgsForCall)
}

func (fake *FakeDisplayInfoAttacher) AttachProgressRewardsDisplayInfoCalls(stub func(context.Context, string, percy.Progress) percy.Progress) {
	fake.attachProgressRewardsDisplayInfoMutex.Lock()
	defer fake.attachProgressRewardsDisplayInfoMutex.Unlock()
	fake.AttachProgressRewardsDisplayInfoStub = stub
}

func (fake *FakeDisplayInfoAttacher) AttachProgressRewardsDisplayInfoArgsForCall(i int) (context.Context, string, percy.Progress) {
	fake.attachProgressRewardsDisplayInfoMutex.RLock()
	defer fake.attachProgressRewardsDisplayInfoMutex.RUnlock()
	argsForCall := fake.attachProgressRewardsDisplayInfoArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2, argsForCall.arg3
}

func (fake *FakeDisplayInfoAttacher) AttachProgressRewardsDisplayInfoReturns(result1 percy.Progress) {
	fake.attachProgressRewardsDisplayInfoMutex.Lock()
	defer fake.attachProgressRewardsDisplayInfoMutex.Unlock()
	fake.AttachProgressRewardsDisplayInfoStub = nil
	fake.attachProgressRewardsDisplayInfoReturns = struct {
		result1 percy.Progress
	}{result1}
}

func (fake *FakeDisplayInfoAttacher) AttachProgressRewardsDisplayInfoReturnsOnCall(i int, result1 percy.Progress) {
	fake.attachProgressRewardsDisplayInfoMutex.Lock()
	defer fake.attachProgressRewardsDisplayInfoMutex.Unlock()
	fake.AttachProgressRewardsDisplayInfoStub = nil
	if fake.attachProgressRewardsDisplayInfoReturnsOnCall == nil {
		fake.attachProgressRewardsDisplayInfoReturnsOnCall = make(map[int]struct {
			result1 percy.Progress
		})
	}
	fake.attachProgressRewardsDisplayInfoReturnsOnCall[i] = struct {
		result1 percy.Progress
	}{result1}
}

func (fake *FakeDisplayInfoAttacher) AttachRewardsListDisplayInfo(arg1 context.Context, arg2 string, arg3 []percy.Reward) []percy.Reward {
	var arg3Copy []percy.Reward
	if arg3 != nil {
		arg3Copy = make([]percy.Reward, len(arg3))
		copy(arg3Copy, arg3)
	}
	fake.attachRewardsListDisplayInfoMutex.Lock()
	ret, specificReturn := fake.attachRewardsListDisplayInfoReturnsOnCall[len(fake.attachRewardsListDisplayInfoArgsForCall)]
	fake.attachRewardsListDisplayInfoArgsForCall = append(fake.attachRewardsListDisplayInfoArgsForCall, struct {
		arg1 context.Context
		arg2 string
		arg3 []percy.Reward
	}{arg1, arg2, arg3Copy})
	stub := fake.AttachRewardsListDisplayInfoStub
	fakeReturns := fake.attachRewardsListDisplayInfoReturns
	fake.recordInvocation("AttachRewardsListDisplayInfo", []interface{}{arg1, arg2, arg3Copy})
	fake.attachRewardsListDisplayInfoMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2, arg3)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakeDisplayInfoAttacher) AttachRewardsListDisplayInfoCallCount() int {
	fake.attachRewardsListDisplayInfoMutex.RLock()
	defer fake.attachRewardsListDisplayInfoMutex.RUnlock()
	return len(fake.attachRewardsListDisplayInfoArgsForCall)
}

func (fake *FakeDisplayInfoAttacher) AttachRewardsListDisplayInfoCalls(stub func(context.Context, string, []percy.Reward) []percy.Reward) {
	fake.attachRewardsListDisplayInfoMutex.Lock()
	defer fake.attachRewardsListDisplayInfoMutex.Unlock()
	fake.AttachRewardsListDisplayInfoStub = stub
}

func (fake *FakeDisplayInfoAttacher) AttachRewardsListDisplayInfoArgsForCall(i int) (context.Context, string, []percy.Reward) {
	fake.attachRewardsListDisplayInfoMutex.RLock()
	defer fake.attachRewardsListDisplayInfoMutex.RUnlock()
	argsForCall := fake.attachRewardsListDisplayInfoArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2, argsForCall.arg3
}

func (fake *FakeDisplayInfoAttacher) AttachRewardsListDisplayInfoReturns(result1 []percy.Reward) {
	fake.attachRewardsListDisplayInfoMutex.Lock()
	defer fake.attachRewardsListDisplayInfoMutex.Unlock()
	fake.AttachRewardsListDisplayInfoStub = nil
	fake.attachRewardsListDisplayInfoReturns = struct {
		result1 []percy.Reward
	}{result1}
}

func (fake *FakeDisplayInfoAttacher) AttachRewardsListDisplayInfoReturnsOnCall(i int, result1 []percy.Reward) {
	fake.attachRewardsListDisplayInfoMutex.Lock()
	defer fake.attachRewardsListDisplayInfoMutex.Unlock()
	fake.AttachRewardsListDisplayInfoStub = nil
	if fake.attachRewardsListDisplayInfoReturnsOnCall == nil {
		fake.attachRewardsListDisplayInfoReturnsOnCall = make(map[int]struct {
			result1 []percy.Reward
		})
	}
	fake.attachRewardsListDisplayInfoReturnsOnCall[i] = struct {
		result1 []percy.Reward
	}{result1}
}

func (fake *FakeDisplayInfoAttacher) AttachStartDataRewardsDisplayInfo(arg1 context.Context, arg2 percy.HypeTrainStartData) percy.HypeTrainStartData {
	fake.attachStartDataRewardsDisplayInfoMutex.Lock()
	ret, specificReturn := fake.attachStartDataRewardsDisplayInfoReturnsOnCall[len(fake.attachStartDataRewardsDisplayInfoArgsForCall)]
	fake.attachStartDataRewardsDisplayInfoArgsForCall = append(fake.attachStartDataRewardsDisplayInfoArgsForCall, struct {
		arg1 context.Context
		arg2 percy.HypeTrainStartData
	}{arg1, arg2})
	stub := fake.AttachStartDataRewardsDisplayInfoStub
	fakeReturns := fake.attachStartDataRewardsDisplayInfoReturns
	fake.recordInvocation("AttachStartDataRewardsDisplayInfo", []interface{}{arg1, arg2})
	fake.attachStartDataRewardsDisplayInfoMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakeDisplayInfoAttacher) AttachStartDataRewardsDisplayInfoCallCount() int {
	fake.attachStartDataRewardsDisplayInfoMutex.RLock()
	defer fake.attachStartDataRewardsDisplayInfoMutex.RUnlock()
	return len(fake.attachStartDataRewardsDisplayInfoArgsForCall)
}

func (fake *FakeDisplayInfoAttacher) AttachStartDataRewardsDisplayInfoCalls(stub func(context.Context, percy.HypeTrainStartData) percy.HypeTrainStartData) {
	fake.attachStartDataRewardsDisplayInfoMutex.Lock()
	defer fake.attachStartDataRewardsDisplayInfoMutex.Unlock()
	fake.AttachStartDataRewardsDisplayInfoStub = stub
}

func (fake *FakeDisplayInfoAttacher) AttachStartDataRewardsDisplayInfoArgsForCall(i int) (context.Context, percy.HypeTrainStartData) {
	fake.attachStartDataRewardsDisplayInfoMutex.RLock()
	defer fake.attachStartDataRewardsDisplayInfoMutex.RUnlock()
	argsForCall := fake.attachStartDataRewardsDisplayInfoArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2
}

func (fake *FakeDisplayInfoAttacher) AttachStartDataRewardsDisplayInfoReturns(result1 percy.HypeTrainStartData) {
	fake.attachStartDataRewardsDisplayInfoMutex.Lock()
	defer fake.attachStartDataRewardsDisplayInfoMutex.Unlock()
	fake.AttachStartDataRewardsDisplayInfoStub = nil
	fake.attachStartDataRewardsDisplayInfoReturns = struct {
		result1 percy.HypeTrainStartData
	}{result1}
}

func (fake *FakeDisplayInfoAttacher) AttachStartDataRewardsDisplayInfoReturnsOnCall(i int, result1 percy.HypeTrainStartData) {
	fake.attachStartDataRewardsDisplayInfoMutex.Lock()
	defer fake.attachStartDataRewardsDisplayInfoMutex.Unlock()
	fake.AttachStartDataRewardsDisplayInfoStub = nil
	if fake.attachStartDataRewardsDisplayInfoReturnsOnCall == nil {
		fake.attachStartDataRewardsDisplayInfoReturnsOnCall = make(map[int]struct {
			result1 percy.HypeTrainStartData
		})
	}
	fake.attachStartDataRewardsDisplayInfoReturnsOnCall[i] = struct {
		result1 percy.HypeTrainStartData
	}{result1}
}

func (fake *FakeDisplayInfoAttacher) Invocations() map[string][][]interface{} {
	fake.invocationsMutex.RLock()
	defer fake.invocationsMutex.RUnlock()
	fake.attachEntitledParticipantsRewardsDisplayInfoMutex.RLock()
	defer fake.attachEntitledParticipantsRewardsDisplayInfoMutex.RUnlock()
	fake.attachProgressRewardsDisplayInfoMutex.RLock()
	defer fake.attachProgressRewardsDisplayInfoMutex.RUnlock()
	fake.attachRewardsListDisplayInfoMutex.RLock()
	defer fake.attachRewardsListDisplayInfoMutex.RUnlock()
	fake.attachStartDataRewardsDisplayInfoMutex.RLock()
	defer fake.attachStartDataRewardsDisplayInfoMutex.RUnlock()
	copiedInvocations := map[string][][]interface{}{}
	for key, value := range fake.invocations {
		copiedInvocations[key] = value
	}
	return copiedInvocations
}

func (fake *FakeDisplayInfoAttacher) recordInvocation(key string, args []interface{}) {
	fake.invocationsMutex.Lock()
	defer fake.invocationsMutex.Unlock()
	if fake.invocations == nil {
		fake.invocations = map[string][][]interface{}{}
	}
	if fake.invocations[key] == nil {
		fake.invocations[key] = [][]interface{}{}
	}
	fake.invocations[key] = append(fake.invocations[key], args)
}

var _ rewards.DisplayInfoAttacher = new(FakeDisplayInfoAttacher)
