package pubsub

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"time"

	pubsub_control "code.justin.tv/chat/pubsub-control/client"
	"code.justin.tv/chat/pubsub-go-pubclient/client"
	"code.justin.tv/commerce/logrus"
	makotwirp "code.justin.tv/commerce/mako/twirp"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/mako"
	"code.justin.tv/commerce/percy/internal/miniexperiments"
	"code.justin.tv/commerce/percy/internal/miniexperiments/ht_approaching"
	"code.justin.tv/commerce/percy/internal/pubsub/rewards"
	"code.justin.tv/commerce/percy/internal/slack"
	"github.com/google/uuid"
	"github.com/pkg/errors"
)

const (
	hypeTrainEventsTopicFormat          = "hype-train-events-v1.%s"
	hypeTrainRewardsTopicModifierFormat = "rewards.%s"
	celebrationsTopicFormat             = "celebration-events-v1.%s"
)

const (
	hypeTrainApproachingMessageType        = "hype-train-approaching"
	hypeTrainStartMessageType              = "hype-train-start"
	hypeTrainProgressionMessageType        = "hype-train-progression"
	hypeTrainLevelUpMessageType            = "hype-train-level-up"
	hypeTrainEndMessageType                = "hype-train-end"
	hypeTrainRewardsMessageType            = "hype-train-rewards"
	hypeTrainConductorUpdateMessageType    = "hype-train-conductor-update"
	hypeTrainCooldownExpirationMessageType = "hype-train-cooldown-expiration"
	celebrationInitiatedMessageType        = "celebration-initiated"
)

type Publisher struct {
	client                       client.PubClient
	control                      pubsub_control.Client
	slacker                      slack.Slacker
	userProfileDB                percy.UserProfileDB
	rewardsDisplayInfoAttacher   rewards.DisplayInfoAttacher
	rewardsDisplayInfoFetcher    rewards.DisplayInfoFetcher
	celebrationEmotesSelector    celebrationEmotesSelector
	approachingEnabled           bool
	approachingOverrides         []string
	celebrationsExperimentClient miniexperiments.Client
	approachingExperimentClient  miniexperiments.Client
}

type celebrationEmotesSelector struct {
	subscriptionsDB percy.SubscriptionsDB
	emotesFetcher   mako.Fetcher
}

func NewPublisher(pubsubClient client.PubClient, control pubsub_control.Client, slackURL string,
	userProfileDB percy.UserProfileDB, rewardsDisplayInfoAttacher rewards.DisplayInfoAttacher, rewardsDisplayInfoFetcher rewards.DisplayInfoFetcher,
	subscriptionsDB percy.SubscriptionsDB, makoFetcher mako.Fetcher, celebrationsExperimentClient miniexperiments.Client,
	approachingExperimentClient miniexperiments.Client, approachingEnabled bool, approachingOverrides []string) *Publisher {

	slacker := slack.NewSlacker(slackURL)

	return &Publisher{
		client:                     pubsubClient,
		control:                    control,
		slacker:                    slacker,
		userProfileDB:              userProfileDB,
		rewardsDisplayInfoAttacher: rewardsDisplayInfoAttacher,
		rewardsDisplayInfoFetcher:  rewardsDisplayInfoFetcher,
		celebrationEmotesSelector: celebrationEmotesSelector{
			subscriptionsDB: subscriptionsDB,
			emotesFetcher:   makoFetcher,
		},
		approachingEnabled:           approachingEnabled,
		approachingOverrides:         approachingOverrides,
		celebrationsExperimentClient: celebrationsExperimentClient,
		approachingExperimentClient:  approachingExperimentClient,
	}
}

func (p *Publisher) HypeTrainApproaching(ctx context.Context, channelID string, goal int64, eventsRemainingDurations map[int]time.Duration, rewards []percy.Reward, creatorColor string, participants []string) error {
	if !p.approachingEnabled {
		return nil
	}

	if !p.approachingOverriddenChannel(channelID) {
		treatment, err := p.approachingExperimentClient.GetTreatment(channelID)
		if err != nil {
			return err
		} else if treatment == ht_approaching.TreatmentControl {
			return nil
		}
	}

	// fetch emote data from Mako and use that version of the data instead of ours.
	rewardsMap := p.rewardsDisplayInfoFetcher.DisplayInformation(ctx, channelID, rewards)
	if len(rewardsMap) != len(rewards) {
		logrus.Warn("We received few rewards back than are configured.")
	}

	updatedRewards := make([]percy.Reward, 0)
	for _, rewardItem := range rewardsMap {
		updatedRewards = append(updatedRewards, rewardItem)
	}

	topic := fmt.Sprintf(hypeTrainEventsTopicFormat, channelID)
	eventsRemainingSeconds := make(map[int]int64)
	for index, duration := range eventsRemainingDurations {
		eventsRemainingSeconds[index] = int64(duration.Seconds())
	}

	body := percy.HypeTrainApproachingMessage{
		MessageType: hypeTrainApproachingMessageType,
		Data: percy.HypeTrainApproachingData{
			ChannelID:                channelID,
			Goal:                     goal,
			EventsRemainingDurations: eventsRemainingSeconds,
			LevelOneRewards:          updatedRewards,
			CreatorColor:             creatorColor,
			Participants:             participants,
		},
	}

	bodyBytes, err := json.Marshal(&body)
	if err != nil {
		return errors.Wrap(err, "failed to marshal hype train approaching pubsub data")
	}

	err = p.client.Publish(ctx, []string{topic}, string(bodyBytes), nil)
	if err != nil {
		return errors.Wrap(err, "failed to publish hype train approaching message")
	}

	return nil
}

func (p *Publisher) approachingOverriddenChannel(channelID string) bool {
	for _, overrideChannelID := range p.approachingOverrides {
		if overrideChannelID == channelID {
			return true
		}
	}
	return false
}

func (p *Publisher) StartHypeTrain(ctx context.Context, hypeTrain percy.HypeTrain, kickOffParticipations ...percy.Participation) error {
	topic := fmt.Sprintf(hypeTrainEventsTopicFormat, hypeTrain.ChannelID)

	hypeTrainStartData := p.rewardsDisplayInfoAttacher.AttachStartDataRewardsDisplayInfo(ctx, percy.HypeTrainStartData{
		HypeTrain: hypeTrain,
		Progress:  hypeTrain.LevelProgress(),
	})

	for _, c := range hypeTrainStartData.Conductors {
		participantUserInfo, _ := p.userProfileDB.GetProfileByID(ctx, c.User.Id)

		c.User.Login = participantUserInfo.Login
		c.User.DisplayName = participantUserInfo.DisplayName
		c.User.ProfileImageURL = participantUserInfo.ProfileImageURL
	}

	body := percy.HypeTrainStartMessage{
		MessageType: hypeTrainStartMessageType,
		Data:        hypeTrainStartData,
	}

	bodyBytes, err := json.Marshal(&body)
	if err != nil {
		return errors.Wrap(err, "failed to marshal start hype train pubsub data")
	}

	err = p.client.Publish(ctx, []string{topic}, string(bodyBytes), nil)
	if err != nil {
		return errors.Wrap(err, "failed to publish start hype train message")
	}

	go p.postToSlack(hypeTrain)

	return nil
}

func (p *Publisher) PostHypeTrainProgression(ctx context.Context, participation percy.Participation, progress percy.Progress) error {
	topic := fmt.Sprintf(hypeTrainEventsTopicFormat, participation.ChannelID)

	participantUserInfo, _ := p.userProfileDB.GetProfileByID(ctx, participation.User.Id)

	progressWithDisplayInfo := p.rewardsDisplayInfoAttacher.AttachProgressRewardsDisplayInfo(ctx, participation.ChannelID, progress)

	body := percy.HypeTrainProgressionMessage{
		MessageType: hypeTrainProgressionMessageType,
		Data: percy.HypeTrainProgressionData{
			UserID:              participation.User.Id,
			UserLogin:           participantUserInfo.Login,
			UserDisplayName:     participantUserInfo.DisplayName,
			UserProfileImageURL: participantUserInfo.ProfileImageURL,
			SequenceID:          int(progress.Total),
			Source:              participation.Source,
			Action:              participation.Action,
			Quantity:            percy.ParticipationPoint(participation.Quantity),
			Progress:            progressWithDisplayInfo,
		},
	}

	bodyBytes, err := json.Marshal(&body)
	if err != nil {
		return errors.Wrap(err, "failed to marshal hype train progression pubsub data")
	}

	err = p.client.Publish(ctx, []string{topic}, string(bodyBytes), nil)
	if err != nil {
		return errors.Wrap(err, "failed to publish hype train progression message")
	}

	return nil
}

func (p *Publisher) LevelUpHypeTrain(ctx context.Context, channelID string, timeToExpire time.Time, progress percy.Progress) error {
	topic := fmt.Sprintf(hypeTrainEventsTopicFormat, channelID)

	progressWithDisplayInfo := p.rewardsDisplayInfoAttacher.AttachProgressRewardsDisplayInfo(ctx, channelID, progress)

	body := percy.HypeTrainLevelUpMessage{
		MessageType: hypeTrainLevelUpMessageType,
		Data: percy.HypeTrainLevelUpData{
			TimeToExpire: timeToExpire.Unix() * 1000,
			Progress:     progressWithDisplayInfo,
		},
	}

	bodyBytes, err := json.Marshal(&body)
	if err != nil {
		return errors.Wrap(err, "failed to marshal hype train level up pubsub data")
	}

	err = p.client.Publish(ctx, []string{topic}, string(bodyBytes), nil)
	if err != nil {
		return errors.Wrap(err, "failed to publish hype train level up message")
	}

	return nil
}

func (p *Publisher) EndHypeTrain(ctx context.Context, hypeTrain percy.HypeTrain, timeEnded time.Time, reason percy.EndingReason) error {
	channelID := hypeTrain.ChannelID
	topic := fmt.Sprintf(hypeTrainEventsTopicFormat, channelID)

	body := percy.HypeTrainEndMessage{
		MessageType: hypeTrainEndMessageType,
		Data: percy.HypeTrainEndData{
			EndedAt:      timeEnded.Unix() * 1000,
			EndingReason: reason,
		},
	}

	bodyBytes, err := json.Marshal(&body)
	if err != nil {
		return errors.Wrap(err, "failed to marshal hype train level up pubsub data")
	}

	err = p.client.Publish(ctx, []string{topic}, string(bodyBytes), nil)
	if err != nil {
		return errors.Wrap(err, "failed to publish hype train level up message")
	}

	return nil
}

// Need channelID, participants, rewards, completed_level
func (p *Publisher) ParticipantRewards(ctx context.Context, channelID string, entitledParticipants percy.EntitledParticipants) error {
	entitledParticipantsWithRewards := p.rewardsDisplayInfoAttacher.AttachEntitledParticipantsRewardsDisplayInfo(ctx, channelID, entitledParticipants)

	for userID, entitledRewards := range entitledParticipantsWithRewards {
		topic := fmt.Sprintf(hypeTrainEventsTopicFormat, fmt.Sprintf(hypeTrainRewardsTopicModifierFormat, userID))

		body := percy.HypeTrainRewardsMessage{
			MessageType: hypeTrainRewardsMessageType,
			Data: percy.HypeTrainRewardsData{
				ChannelID:      channelID,
				CompletedLevel: entitledRewards[0].RewardLevel,
				Rewards:        entitledRewards,
			},
		}
		bodyBytes, err := json.Marshal(&body)
		if err != nil {
			logrus.WithError(err).Error("failed to marshal participant rewards data")
			continue
		}

		err = p.client.Publish(ctx, []string{topic}, string(bodyBytes), nil)
		if err != nil {
			logrus.WithError(err).Error("failed to publish participant rewards message")
			continue
		}
	}

	return nil
}

func (p *Publisher) ConductorUpdate(ctx context.Context, channelID string, conductor percy.Conductor) error {
	topic := fmt.Sprintf(hypeTrainEventsTopicFormat, channelID)

	participantUserInfo, _ := p.userProfileDB.GetProfileByID(ctx, conductor.User.Id)

	conductor.User.Login = participantUserInfo.Login
	conductor.User.DisplayName = participantUserInfo.DisplayName
	conductor.User.ProfileImageURL = participantUserInfo.ProfileImageURL

	body := percy.HypeTrainConductorUpdateMessage{
		MessageType: hypeTrainConductorUpdateMessageType,
		Data: percy.HypeTrainConductorUpdateData{
			Conductor: conductor,
		},
	}

	bodyBytes, err := json.Marshal(&body)
	if err != nil {
		return errors.Wrap(err, "failed to marshal conductor update pubsub data")
	}

	err = p.client.Publish(ctx, []string{topic}, string(bodyBytes), nil)
	if err != nil {
		return errors.Wrap(err, "failed to publish conductor update message")
	}

	return nil
}

func (p *Publisher) CooldownExpired(ctx context.Context, channelID string) error {
	topic := fmt.Sprintf(hypeTrainEventsTopicFormat, channelID)

	body := percy.HypeTrainCooldownExpirationMessage{MessageType: hypeTrainCooldownExpirationMessageType}

	bodyBytes, err := json.Marshal(&body)
	if err != nil {
		return errors.Wrap(err, "failed to marshal cooldown expiration pubsub data")
	}

	err = p.client.Publish(ctx, []string{topic}, string(bodyBytes), nil)
	if err != nil {
		return errors.Wrap(err, "failed to publish cooldown expiration message")
	}

	return nil
}

func (p *Publisher) StartCelebration(ctx context.Context, channelID string, sender percy.User, amount int64, celebration percy.Celebration) error {
	topic := fmt.Sprintf(celebrationsTopicFormat, channelID)

	senderProfile, err := p.userProfileDB.GetProfileByID(ctx, sender.Id)
	if err != nil {
		return errors.Wrap(err, "failed to load sender information from users service")
	}
	chosenEmotes := p.celebrationEmotesSelector.SelectCelebrationEmotes(ctx, channelID, celebration.EventType)

	body := percy.CelebrationStartMessage{
		MessageType: celebrationInitiatedMessageType,
		Data: percy.CelebrationStartData{
			EventID:     uuid.New().String(),
			Celebration: percy.CelebrationToPubsubDate(celebration),
			Benefactor:  senderProfile.Login,
			Amount:      amount,
			Emotes:      chosenEmotes,
		},
	}

	bodyBytes, err := json.Marshal(&body)
	if err != nil {
		return errors.Wrap(err, "failed to marshal celebration start pubsub data")
	}

	err = p.client.Publish(ctx, []string{topic}, string(bodyBytes), nil)
	if err != nil {
		return errors.Wrap(err, "failed to publish celebration start pubsub message")
	}
	logrus.Infof("Celebration pubsub sent, channelID %+v, celebration %+v", channelID, celebration)
	return nil
}

func (p *Publisher) postToSlack(hypeTrain percy.HypeTrain) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	profile, err := p.userProfileDB.GetProfileByID(ctx, hypeTrain.ChannelID)
	if err != nil {
		logrus.WithError(err).Error("failed to get user login to post to slack")
		return
	}

	msgText := ":percy: *New Hype Train Alert *:percy:\n"
	msgText += fmt.Sprintf("https://twitch.tv/%s (%s)\n", profile.Login, profile.ID)
	msgText += fmt.Sprintf("Hype Train ID: %s\n", hypeTrain.ID)

	msg := slack.SlackMsg{
		Text: p.attachExperimentInfo(msgText, profile),
	}

	err = p.slacker.Post(msg)
	if err != nil {
		logrus.WithError(err).Warn("error posting to slack")
	}
}

func (p *Publisher) attachExperimentInfo(msgText string, profile percy.UserProfile) string {
	// Celebrations Experiment
	experimentName := p.celebrationsExperimentClient.GetExperimentName()
	treatment, err := p.celebrationsExperimentClient.GetTreatment(profile.ID)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"experimentName": experimentName,
			"treatment":      treatment,
		}).WithError(err).Warn("error getting treatment")
		treatment = "Unknown"
	}
	msgText += fmt.Sprintf("HT+Celeb Treatment: %s\n", treatment)

	// Approaching Experiment, note that approaching part has already passed.
	treatment, err = p.approachingExperimentClient.GetTreatment(profile.ID)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"experimentName": p.approachingExperimentClient.GetExperimentName(),
			"treatment":      treatment,
		}).WithError(err).Warn("error getting treatment")
		treatment = "Unknown"
	}
	msgText += fmt.Sprintf("HT Approaching Treatment: %s\n", treatment)

	return msgText
}

func (p *Publisher) GetNumberOfPubsubListeners(pubsubTopic, channelID string) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	combinedTopic := pubsubTopic + channelID

	numListeners, err := p.control.GetNumberListeners(ctx, combinedTopic, nil)
	if err != nil {
		return 0, err
	}

	return numListeners, nil
}

func (s *celebrationEmotesSelector) SelectCelebrationEmotes(ctx context.Context, channelID string, eventType percy.CelebrationEventType) []percy.Emote {
	switch eventType {
	case percy.EventTypeSubscriptionFounder:
		return s.selectFounderCelebrationEmotes()
	case percy.EventTypeCheer:
		fallthrough
	case percy.EventTypeExplicitPurchase:
		fallthrough
	case percy.EventTypeSubscriptionGift:
		fallthrough
	default:
		return s.selectDefaultCelebrationEmotes(ctx, channelID)
	}
}

func (s *celebrationEmotesSelector) selectDefaultCelebrationEmotes(ctx context.Context, channelID string) []percy.Emote {
	defaultEmotes := []percy.Emote{
		{
			ID:    "88",
			Token: "PogChamp",
		},
		{
			ID:    "9",
			Token: "<3",
		},
		{
			ID:    "81274",
			Token: "VoHiYo",
		},
	}

	emoteSetIds, err := s.subscriptionsDB.GetChannelEmoticonSets(ctx, channelID)
	if err != nil {
		logrus.WithField("channelID", channelID).WithError(err).Error("Failed to retrieve channel emote sets from subs")
	}

	emoteDetails, err := s.emotesFetcher.GetEmoteSetInformation(ctx, emoteSetIds)
	if err != nil {
		logrus.WithField("channelID", channelID).WithError(err).Error("Failed to get emote information")
	}

	emotes := make([]percy.Emote, 0, len(emoteDetails))
	for _, detail := range emoteDetails {
		if detail.State == makotwirp.EmoteState_active {
			emote := percy.Emote{
				ID:    detail.Id,
				Token: detail.Code,
			}

			emotes = append(emotes, emote)
		}
	}

	// We need at least 3 emotes for the animation, so will append
	// placeholders if we don't have enough
	index := 0
	for len(emotes) < 3 {
		emotes = append(emotes, defaultEmotes[index])
		index++
	}

	// randomly shuffle the emotes
	if len(emotes) > 3 {
		rand.Seed(time.Now().UnixNano())
		rand.Shuffle(len(emotes), func(i, j int) { emotes[i], emotes[j] = emotes[j], emotes[i] })
	}

	// return the chosen three lucky emotes
	return emotes[0:3]
}

func (s *celebrationEmotesSelector) selectFounderCelebrationEmotes() []percy.Emote {
	return []percy.Emote{
		{
			ID:    "301741365",
			Token: "FounderCeleb",
		},
	}
}
