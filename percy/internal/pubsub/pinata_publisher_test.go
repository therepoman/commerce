package pubsub

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	"code.justin.tv/commerce/percy/internal/pubsub/pubsubfakes"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPinataPublisher_PublishPinataDrop(t *testing.T) {
	Convey("given a pinata publisher", t, func() {
		pubClient := &pubsubfakes.FakePubClient{}
		pinataPublisher := NewPinataPublisher(pubClient)

		ctx := context.Background()

		testId := "bears-too-cool"
		testChannel := "265716088"

		Convey("returns an error when pubsub errors", func() {
			pubClient.PublishReturns(errors.New("error in PublishBoostOpportunityStart"))

			err := pinataPublisher.PublishPinataDrop(ctx, testId, testChannel)
			So(err, ShouldNotBeNil)
		})

		Convey("returns nil when pubsub succeeds", func() {
			pubClient.PublishReturns(nil)

			err := pinataPublisher.PublishPinataDrop(ctx, testId, testChannel)
			So(err, ShouldBeNil)

			actualCtx, actualTopics, actualBody, _ := pubClient.PublishArgsForCall(0)
			So(actualCtx, ShouldEqual, ctx)
			So(actualTopics, ShouldHaveLength, 1)
			So(actualTopics[0], ShouldEqual, fmt.Sprintf(bitsPinataTopicFormat, testChannel))

			var msgs PinataUpdates
			err = json.Unmarshal([]byte(actualBody), &msgs)
			So(err, ShouldBeNil)

			So(msgs.Updates, ShouldHaveLength, 1)
			msg := msgs.Updates[0]

			So(msg.Type, ShouldEqual, bitsPinataDropMessageType)
			So(msg.ChannelId, ShouldEqual, testChannel)
			So(msg.UserId, ShouldBeNil)
			So(msg.BitsUsed, ShouldBeNil)
			So(msg.IsPinataBroken, ShouldBeNil)
		})
	})
}

func TestPinataPublisher_PublishPinataActivity(t *testing.T) {
	Convey("given a pinata publisher", t, func() {
		pubClient := &pubsubfakes.FakePubClient{}
		pinataPublisher := NewPinataPublisher(pubClient)

		ctx := context.Background()

		testId := "bears-too-lame"
		testChannel := "265716088"
		testUser := "46093303"

		Convey("returns an error when pubsub errors", func() {
			pubClient.PublishReturns(errors.New("error in PublishBoostOpportunityStart"))

			err := pinataPublisher.PublishPinataActivity(ctx, testId, testChannel, testUser, int64(100), false)
			So(err, ShouldNotBeNil)
		})

		Convey("returns nil when pubsub succeeds", func() {
			pubClient.PublishReturns(nil)

			err := pinataPublisher.PublishPinataActivity(ctx, testId, testChannel, testUser, int64(100), false)
			So(err, ShouldBeNil)

			actualCtx, actualTopics, actualBody, _ := pubClient.PublishArgsForCall(0)
			So(actualCtx, ShouldEqual, ctx)
			So(actualTopics, ShouldHaveLength, 1)
			So(actualTopics[0], ShouldEqual, fmt.Sprintf(bitsPinataTopicFormat, testChannel))

			var msgs PinataUpdates
			err = json.Unmarshal([]byte(actualBody), &msgs)
			So(err, ShouldBeNil)

			So(msgs.Updates, ShouldHaveLength, 1)
			msg := msgs.Updates[0]

			So(msg.Type, ShouldEqual, bitsPinataCheerMessageType)
			So(msg.ChannelId, ShouldEqual, testChannel)
			So(*msg.UserId, ShouldEqual, testUser)
			So(*msg.BitsUsed, ShouldEqual, 100)
			So(*msg.IsPinataBroken, ShouldBeFalse)
		})
	})
}
