package percy_test

import (
	"context"
	"errors"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	. "github.com/smartystreets/goconvey/convey"
)

func TestEngine_GetBadges(t *testing.T) {
	Convey("Given an Engine", t, func() {
		badgeDB := &internalfakes.FakeBadgesDB{}

		engine := percy.NewEngine(percy.EngineConfig{
			BadgesDB: badgeDB,
		})

		ctx := context.Background()
		userID := "123123123"
		channelID := "321321321"

		Convey("when the db fails to return, we return an error", func() {
			badgeDB.GetBadgesReturns(nil, errors.New("WALRUS STRIKE"))

			_, err := engine.GetBadges(ctx, userID, channelID)

			So(err, ShouldNotBeNil)
		})

		Convey("when the db returns badges, return them and no error", func() {
			badgeDB.GetBadgesReturns([]percy.Badge{percy.FormerConductorBadge}, nil)

			badges, err := engine.GetBadges(ctx, userID, channelID)

			So(err, ShouldBeNil)
			So(badges, ShouldNotBeNil)
			So(badges, ShouldHaveLength, 1)
			So(badges[0], ShouldEqual, percy.FormerConductorBadge)
		})
	})
}

func TestEngine_SetBadges(t *testing.T) {
	Convey("Given an Engine", t, func() {
		badgeDB := &internalfakes.FakeBadgesDB{}

		engine := percy.NewEngine(percy.EngineConfig{
			BadgesDB: badgeDB,
		})

		ctx := context.Background()
		userID := "123123123"
		channelID := "321321321"
		badges := tests.ValidBadgesResponse()

		Convey("when the db fails to return, we return an error", func() {
			badgeDB.SetBadgesReturns(errors.New("WALRUS STRIKE"))

			err := engine.SetBadges(ctx, userID, channelID, badges)

			So(err, ShouldNotBeNil)
		})

		Convey("when the db returns badges, return them and no error", func() {
			badgeDB.SetBadgesReturns(nil)

			err := engine.SetBadges(ctx, userID, channelID, badges)

			So(err, ShouldBeNil)
		})
	})
}

func TestConductorBadgesHandler_UpdateCurrentConductorBadgeHolders(t *testing.T) {
	Convey("Given an Engine", t, func() {
		badgeDB := &internalfakes.FakeBadgesDB{}
		conductorDB := &internalfakes.FakeConductorsDB{}
		badgesClient := &internalfakes.FakeBadgesClient{}
		spade := &internalfakes.FakeSpade{}

		handler := percy.NewConductorBadgesHandler(conductorDB, badgeDB, badgesClient, spade)

		ctx := context.Background()
		hypeTrain := tests.ValidHypeTrain()

		Convey("when we succeed", func() {
			conductorDB.SetConductorsReturns(nil)
			badgeDB.SetBadgesReturns(nil)

			Convey("when we successfully entitle badges", func() {
				badgesClient.SetSelectedHypeTrainBadgeReturns(nil)
			})

			Convey("when we error entitling badges", func() {
				badgesClient.SetSelectedHypeTrainBadgeReturns(errors.New("WALRUS STRIKE"))
			})

			Convey("when the hype train has no conductor", func() {
				hypeTrain.Conductors = map[percy.ParticipationSource]percy.Conductor{}
			})

			err := handler.UpdateCurrentConductorBadgeHolders(ctx, hypeTrain)
			So(err, ShouldBeNil)
		})

		Convey("when we error", func() {
			Convey("when we fail to get conductors", func() {
				conductorDB.SetConductorsReturns(errors.New("WALRUS STRIKE"))
			})

			Convey("when we fail to set the badges", func() {
				conductorDB.SetConductorsReturns(nil)
				badgeDB.SetBulkBadgesReturns(errors.New("WALRUS STRIKE"))
			})

			err := handler.UpdateCurrentConductorBadgeHolders(ctx, hypeTrain)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestConductorBadgesHandler_RemoveCurrentConductorBadgeHolders(t *testing.T) {
	Convey("Given an Engine", t, func() {
		badgeDB := &internalfakes.FakeBadgesDB{}
		conductorDB := &internalfakes.FakeConductorsDB{}
		badgesClient := &internalfakes.FakeBadgesClient{}
		spade := &internalfakes.FakeSpade{}

		handler := percy.NewConductorBadgesHandler(conductorDB, badgeDB, badgesClient, spade)

		ctx := context.Background()
		hypeTrain := tests.ValidHypeTrain()
		conductors := make([]percy.Conductor, 0)
		for _, c := range hypeTrain.Conductors {
			conductors = append(conductors, c)
		}

		Convey("when we succeed", func() {
			conductorDB.SetConductorsReturns(nil)
			badgeDB.SetBulkBadgesReturns(nil)

			Convey("when there are no conductors set in the db", func() {
				conductorDB.GetConductorsReturns(nil, nil)
			})

			Convey("when we succeed to get conductors from the db", func() {
				conductorDB.GetConductorsReturns(conductors, nil)

				Convey("when we error getting badge for user", func() {
					badgesClient.GetSelectedHypeTrainBadgeReturns(nil, errors.New("WALRUS STRIKE"))
				})

				Convey("when we succeed at getting badge for user and it's not a hype train badge", func() {
					badgesClient.GetSelectedHypeTrainBadgeReturns(nil, nil)
				})

				Convey("when we succeed at getting badge for user and it's a former conductor hype train badge", func() {
					badge := percy.FormerConductorBadge
					badgesClient.GetSelectedHypeTrainBadgeReturns(&badge, nil)
				})

				Convey("when we succeed at getting badge for user and it's a current conductor hype train badge", func() {
					badge := percy.CurrentConductorBadge
					badgesClient.GetSelectedHypeTrainBadgeReturns(&badge, nil)

					Convey("when we fail to remove the badge", func() {
						badgesClient.RemoveSelectedHypeTrainBadgeReturns(errors.New("WALRUS STRIKE"))
					})

					Convey("when we succeed to remove the badge", func() {
						badgesClient.RemoveSelectedHypeTrainBadgeReturns(nil)
					})
				})
			})

			err := handler.RemoveCurrentConductorBadgeHolders(ctx, hypeTrain)
			So(err, ShouldBeNil)
		})

		Convey("when we error", func() {
			Convey("when we fail to fetch conductors from the db", func() {
				conductorDB.GetConductorsReturns(nil, errors.New("WALRUS STRIKE"))
			})

			Convey("when we fail to reset the conductors", func() {
				conductorDB.GetConductorsReturns(conductors, nil)
				conductorDB.SetConductorsReturns(errors.New("WALRUS STRIKE"))
			})

			Convey("when we fail to set the badges for the former conductors", func() {
				conductorDB.GetConductorsReturns(conductors, nil)
				conductorDB.SetConductorsReturns(nil)
				badgeDB.SetBulkBadgesReturns(errors.New("WALRUS STRIKE"))
			})

			err := handler.RemoveCurrentConductorBadgeHolders(ctx, hypeTrain)
			So(err, ShouldNotBeNil)
		})
	})
}
