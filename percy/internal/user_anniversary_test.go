package percy_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestTwitchiversaryEngine_IsUserAnniversary(t *testing.T) {
	Convey("given a Twitchiversary engine", t, func() {
		userProfileDB := &internalfakes.FakeUserProfileDB{}
		userAnniversaryAllowlistDB := &internalfakes.FakeUserAnniversaryAllowlistDB{}
		userAnniversaryCache := &internalfakes.FakeUserAnniversaryCache{}

		eng := percy.NewTwitchiversaryEngine(percy.TwitchiversaryEngineConfig{
			UserAnniversaryAllowlistDB: userAnniversaryAllowlistDB,
			UserProfileDB:              userProfileDB,
			UserAnniversaryCache:       userAnniversaryCache,
		})

		ctx := context.Background()
		userID := "123"
		userAnniversaryCache.GetCreationDateReturns(time.Time{}, nil)
		userAnniversaryCache.AddNewCreationDateReturns(nil)

		Convey("should return false if experiment flag is not enabled", func() {
			userAnniversaryAllowlistDB.IsUserAnniversaryOverrideReturns(false)
			userAnniversaryAllowlistDB.IsUserAnniversaryExperimentEnabledReturns(false)
			userProfileDB.GetProfileByIDReturns(percy.UserProfile{
				ID:              "",
				Login:           "",
				DisplayName:     "",
				IsStaff:         false,
				PrimaryColorHex: nil,
				ProfileImageURL: "",
				CreatedOn:       time.Time{},
			}, nil)
			result, year, err := eng.IsUserAnniversary(ctx, userID, time.Now().UTC())
			So(result, ShouldBeFalse)
			So(year, ShouldEqual, 0)
			So(err, ShouldBeNil)
		})
		userAnniversaryAllowlistDB.IsUserAnniversaryExperimentEnabledReturns(true)

		Convey("should return true if the channel is in the allowlist", func() {
			userAnniversaryAllowlistDB.IsUserAnniversaryOverrideReturns(true)
			userProfileDB.GetProfileByIDReturns(percy.UserProfile{
				ID:              "",
				Login:           "",
				DisplayName:     "",
				IsStaff:         false,
				PrimaryColorHex: nil,
				ProfileImageURL: "",
				CreatedOn:       time.Time{},
			}, nil)
			result, year, err := eng.IsUserAnniversary(ctx, userID, time.Now().UTC())
			So(result, ShouldBeTrue)
			So(year, ShouldNotBeNil)
			So(err, ShouldBeNil)
		})

		Convey("when not explicitly it allowlist", func() {
			userAnniversaryAllowlistDB.IsUserAnniversaryOverrideReturns(false)
			Convey("testing caching", func() {
				now := time.Date(2021, 6, 24, 23, 0, 0, 0, time.UTC)
				createdOn := time.Date(2017, 6, 24, 23, 0, 0, 0, time.UTC)
				Convey("when reading from cache returns a time", func() {
					userAnniversaryCache.GetCreationDateReturns(createdOn, nil)
					userProfileDB.GetProfileByIDReturns(percy.UserProfile{
						ID:              "",
						Login:           "",
						DisplayName:     "",
						IsStaff:         false,
						PrimaryColorHex: nil,
						ProfileImageURL: "",
						CreatedOn:       createdOn,
					}, nil)
					result, year, err := eng.IsUserAnniversary(ctx, userID, now)
					userProfileCount := userProfileDB.GetProfileByIDCallCount()

					So(userProfileCount, ShouldEqual, 0)
					So(result, ShouldBeTrue)
					So(year, ShouldEqual, 4)
					So(err, ShouldBeNil)
				})
				Convey("when reading from cache returns empty string", func() {
					userAnniversaryCache.GetCreationDateReturns(time.Time{}, nil)
					userProfileDB.GetProfileByIDReturns(percy.UserProfile{
						ID:              "",
						Login:           "",
						DisplayName:     "",
						IsStaff:         false,
						PrimaryColorHex: nil,
						ProfileImageURL: "",
						CreatedOn:       createdOn,
					}, nil)
					result, year, err := eng.IsUserAnniversary(ctx, userID, now)
					userProfileCount := userProfileDB.GetProfileByIDCallCount()

					So(userProfileCount, ShouldEqual, 1)
					So(result, ShouldBeTrue)
					So(year, ShouldEqual, 4)
					So(err, ShouldBeNil)
				})
				Convey("when reading from cache returns error", func() {
					userAnniversaryCache.GetCreationDateReturns(time.Time{}, errors.New("error"))
					userProfileDB.GetProfileByIDReturns(percy.UserProfile{
						ID:              "",
						Login:           "",
						DisplayName:     "",
						IsStaff:         false,
						PrimaryColorHex: nil,
						ProfileImageURL: "",
						CreatedOn:       createdOn,
					}, nil)
					result, year, err := eng.IsUserAnniversary(ctx, userID, now)
					userProfileCount := userProfileDB.GetProfileByIDCallCount()

					So(userProfileCount, ShouldEqual, 1)
					So(result, ShouldBeTrue)
					So(year, ShouldEqual, 4)
					So(err, ShouldBeNil)
				})
			})

			Convey("when date is today", func() {
				now := time.Now().UTC()
				Convey("should return true if it is the account creation anniversary", func() {
					createdOn := time.Date(now.Year()-1, now.Month(), now.Day(), now.Hour(), now.Minute(), 0, 0, time.UTC)
					userProfileDB.GetProfileByIDReturns(percy.UserProfile{
						ID:              "",
						Login:           "",
						DisplayName:     "",
						IsStaff:         false,
						PrimaryColorHex: nil,
						ProfileImageURL: "",
						CreatedOn:       createdOn,
					}, nil)
					result, year, err := eng.IsUserAnniversary(ctx, userID, now)
					So(result, ShouldBeTrue)
					So(year, ShouldEqual, 1)
					So(err, ShouldBeNil)
				})
				Convey("should return false if it is the account was created today", func() {
					createdOn := time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), 0, 0, time.UTC)
					userProfileDB.GetProfileByIDReturns(percy.UserProfile{
						ID:              "",
						Login:           "",
						DisplayName:     "",
						IsStaff:         false,
						PrimaryColorHex: nil,
						ProfileImageURL: "",
						CreatedOn:       createdOn,
					}, nil)
					result, year, err := eng.IsUserAnniversary(ctx, userID, now)
					So(result, ShouldBeFalse)
					So(year, ShouldEqual, 0)
					So(err, ShouldBeNil)
				})
				Convey("should return true if it is the start of account creation anniversary", func() {
					createdOn := time.Date(now.Year()-1, now.Month(), now.Day(), now.Hour(), now.Minute(), 0, 0, time.UTC)
					createdOn = createdOn.Add(time.Duration(5) * time.Hour)
					userProfileDB.GetProfileByIDReturns(percy.UserProfile{
						ID:              "",
						Login:           "",
						DisplayName:     "",
						IsStaff:         false,
						PrimaryColorHex: nil,
						ProfileImageURL: "",
						CreatedOn:       createdOn,
					}, nil)
					result, year, err := eng.IsUserAnniversary(ctx, userID, now)
					So(result, ShouldBeTrue)
					So(year, ShouldEqual, 1)
					So(err, ShouldBeNil)
				})
				Convey("should return true if it is the end of account creation anniversary", func() {
					createdOn := time.Date(now.Year()-1, now.Month(), now.Day(), now.Hour(), now.Minute(), 0, 0, time.UTC)
					createdOn = createdOn.Add(time.Duration(-29) * time.Hour)
					userProfileDB.GetProfileByIDReturns(percy.UserProfile{
						ID:              "",
						Login:           "",
						DisplayName:     "",
						IsStaff:         false,
						PrimaryColorHex: nil,
						ProfileImageURL: "",
						CreatedOn:       createdOn,
					}, nil)
					result, year, err := eng.IsUserAnniversary(ctx, userID, now)
					logrus.Info("created on:", createdOn)
					logrus.Info("now:", now)
					So(result, ShouldBeTrue)
					So(year, ShouldEqual, 1)
					So(err, ShouldBeNil)
				})
				Convey("should return false if it is before the start of account creation anniversary", func() {
					createdOn := time.Date(now.Year()-1, now.Month(), now.Day(), now.Hour(), now.Minute(), 0, 0, time.UTC)
					createdOn = createdOn.Add(time.Duration(7) * time.Hour)
					userProfileDB.GetProfileByIDReturns(percy.UserProfile{
						ID:              "",
						Login:           "",
						DisplayName:     "",
						IsStaff:         false,
						PrimaryColorHex: nil,
						ProfileImageURL: "",
						CreatedOn:       createdOn,
					}, nil)
					result, year, err := eng.IsUserAnniversary(ctx, userID, now)
					So(result, ShouldBeFalse)
					So(year, ShouldEqual, 0)
					So(err, ShouldBeNil)
				})
				Convey("should return false if it is after the end of account creation anniversary", func() {
					createdOn := time.Date(now.Year()-1, now.Month(), now.Day(), now.Hour(), now.Minute(), 0, 0, time.UTC)
					createdOn = createdOn.Add(time.Duration(-31) * time.Hour)
					userProfileDB.GetProfileByIDReturns(percy.UserProfile{
						ID:              "",
						Login:           "",
						DisplayName:     "",
						IsStaff:         false,
						PrimaryColorHex: nil,
						ProfileImageURL: "",
						CreatedOn:       createdOn,
					}, nil)
					result, year, err := eng.IsUserAnniversary(ctx, userID, now)
					So(result, ShouldBeFalse)
					So(year, ShouldEqual, 1)
					So(err, ShouldBeNil)
				})
			})
			Convey("when creation date leap day", func() {
				createdOn := time.Date(2016, 2, 29, 0, 0, 0, 0, time.UTC)

				Convey("should return true today is a leap day", func() {
					now := time.Date(2020, 2, 29, 0, 0, 0, 0, time.UTC)
					userProfileDB.GetProfileByIDReturns(percy.UserProfile{
						ID:              "",
						Login:           "",
						DisplayName:     "",
						IsStaff:         false,
						PrimaryColorHex: nil,
						ProfileImageURL: "",
						CreatedOn:       createdOn,
					}, nil)
					result, year, err := eng.IsUserAnniversary(ctx, userID, now)
					So(result, ShouldBeTrue)
					So(year, ShouldEqual, 4)
					So(err, ShouldBeNil)
				})
				Convey("should return true at end timeframe of leap day", func() {
					now := time.Date(2020, 3, 1, 0, 0, 0, 0, time.UTC)
					userProfileDB.GetProfileByIDReturns(percy.UserProfile{
						ID:              "",
						Login:           "",
						DisplayName:     "",
						IsStaff:         false,
						PrimaryColorHex: nil,
						ProfileImageURL: "",
						CreatedOn:       createdOn,
					}, nil)
					result, year, err := eng.IsUserAnniversary(ctx, userID, now)
					So(result, ShouldBeTrue)
					So(year, ShouldEqual, 4)
					So(err, ShouldBeNil)
				})
				Convey("should return true at start timeframe of leap day", func() {
					now := time.Date(2020, 2, 28, 23, 0, 0, 0, time.UTC)
					userProfileDB.GetProfileByIDReturns(percy.UserProfile{
						ID:              "",
						Login:           "",
						DisplayName:     "",
						IsStaff:         false,
						PrimaryColorHex: nil,
						ProfileImageURL: "",
						CreatedOn:       createdOn,
					}, nil)
					result, year, err := eng.IsUserAnniversary(ctx, userID, now)
					So(result, ShouldBeTrue)
					So(year, ShouldEqual, 4)
					So(err, ShouldBeNil)
				})
				Convey("should return true on the 1st on a non leap year", func() {
					now := time.Date(2021, 3, 1, 23, 0, 0, 0, time.UTC)
					userProfileDB.GetProfileByIDReturns(percy.UserProfile{
						ID:              "",
						Login:           "",
						DisplayName:     "",
						IsStaff:         false,
						PrimaryColorHex: nil,
						ProfileImageURL: "",
						CreatedOn:       createdOn,
					}, nil)
					result, year, err := eng.IsUserAnniversary(ctx, userID, now)
					So(result, ShouldBeTrue)
					So(year, ShouldEqual, 5)
					So(err, ShouldBeNil)
				})
				Convey("should return false on the 1st on a leap year", func() {
					now := time.Date(2020, 3, 1, 23, 0, 0, 0, time.UTC)
					userProfileDB.GetProfileByIDReturns(percy.UserProfile{
						ID:              "",
						Login:           "",
						DisplayName:     "",
						IsStaff:         false,
						PrimaryColorHex: nil,
						ProfileImageURL: "",
						CreatedOn:       createdOn,
					}, nil)
					result, year, err := eng.IsUserAnniversary(ctx, userID, now)
					So(result, ShouldBeFalse)
					So(year, ShouldEqual, 4)
					So(err, ShouldBeNil)
				})
			})
			Convey("when creation date on dec 31", func() {
				createdOn := time.Date(2016, 12, 31, 0, 0, 0, 0, time.UTC)
				Convey("should return true on the jan 1", func() {
					now := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
					userProfileDB.GetProfileByIDReturns(percy.UserProfile{
						ID:              "",
						Login:           "",
						DisplayName:     "",
						IsStaff:         false,
						PrimaryColorHex: nil,
						ProfileImageURL: "",
						CreatedOn:       createdOn,
					}, nil)
					result, year, err := eng.IsUserAnniversary(ctx, userID, now)
					So(result, ShouldBeTrue)
					So(year, ShouldEqual, 3)
					So(err, ShouldBeNil)
				})
				Convey("should return true late on the dec 30", func() {
					now := time.Date(2019, 12, 30, 23, 0, 0, 0, time.UTC)
					userProfileDB.GetProfileByIDReturns(percy.UserProfile{
						ID:              "",
						Login:           "",
						DisplayName:     "",
						IsStaff:         false,
						PrimaryColorHex: nil,
						ProfileImageURL: "",
						CreatedOn:       createdOn,
					}, nil)
					result, year, err := eng.IsUserAnniversary(ctx, userID, now)
					So(result, ShouldBeTrue)
					So(year, ShouldEqual, 3)
					So(err, ShouldBeNil)
				})
				Convey("should return false early on the dec 30", func() {
					now := time.Date(2019, 12, 30, 0, 0, 0, 0, time.UTC)
					userProfileDB.GetProfileByIDReturns(percy.UserProfile{
						ID:              "",
						Login:           "",
						DisplayName:     "",
						IsStaff:         false,
						PrimaryColorHex: nil,
						ProfileImageURL: "",
						CreatedOn:       createdOn,
					}, nil)
					result, year, err := eng.IsUserAnniversary(ctx, userID, now)
					So(result, ShouldBeFalse)
					So(year, ShouldEqual, 2)
					So(err, ShouldBeNil)
				})
			})
			Convey("when creation date on jan 1", func() {
				createdOn := time.Date(2016, 1, 1, 0, 0, 0, 0, time.UTC)
				Convey("should return true on the jan 1", func() {
					now := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
					userProfileDB.GetProfileByIDReturns(percy.UserProfile{
						ID:              "",
						Login:           "",
						DisplayName:     "",
						IsStaff:         false,
						PrimaryColorHex: nil,
						ProfileImageURL: "",
						CreatedOn:       createdOn,
					}, nil)
					result, year, err := eng.IsUserAnniversary(ctx, userID, now)
					So(result, ShouldBeTrue)
					So(year, ShouldEqual, 4)
					So(err, ShouldBeNil)
				})
				Convey("should return true late on the dec 31", func() {
					now := time.Date(2019, 12, 31, 23, 0, 0, 0, time.UTC)
					userProfileDB.GetProfileByIDReturns(percy.UserProfile{
						ID:              "",
						Login:           "",
						DisplayName:     "",
						IsStaff:         false,
						PrimaryColorHex: nil,
						ProfileImageURL: "",
						CreatedOn:       createdOn,
					}, nil)
					result, year, err := eng.IsUserAnniversary(ctx, userID, now)
					So(result, ShouldBeTrue)
					So(year, ShouldEqual, 4)
					So(err, ShouldBeNil)
				})
				Convey("should return false early on the dec 31", func() {
					now := time.Date(2019, 12, 31, 0, 0, 0, 0, time.UTC)
					userProfileDB.GetProfileByIDReturns(percy.UserProfile{
						ID:              "",
						Login:           "",
						DisplayName:     "",
						IsStaff:         false,
						PrimaryColorHex: nil,
						ProfileImageURL: "",
						CreatedOn:       createdOn,
					}, nil)
					result, year, err := eng.IsUserAnniversary(ctx, userID, now)
					So(result, ShouldBeFalse)
					So(year, ShouldEqual, 3)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}

func TestTwitchiversaryEngine_CanSendUserNotice(t *testing.T) {
	Convey("given a Twitchiversary engine", t, func() {
		userProfileDB := &internalfakes.FakeUserProfileDB{}
		userAnniversaryAllowlistDB := &internalfakes.FakeUserAnniversaryAllowlistDB{}
		userAnniversaryCache := &internalfakes.FakeUserAnniversaryCache{}
		userAnniversaryTokensDB := &internalfakes.FakeUserAnniversaryTokensDB{}

		eng := percy.NewTwitchiversaryEngine(percy.TwitchiversaryEngineConfig{
			UserAnniversaryAllowlistDB: userAnniversaryAllowlistDB,
			UserProfileDB:              userProfileDB,
			UserAnniversaryCache:       userAnniversaryCache,
			UserAnniversaryTokensDB:    userAnniversaryTokensDB,
		})

		ctx := context.Background()
		userID := "123"
		userAnniversaryAllowlistDB.IsUserAnniversaryExperimentEnabledReturns(false)

		Convey("should return false if it is not their anniversary", func() {
			userAnniversaryAllowlistDB.IsUserAnniversaryOverrideReturns(false)
			userProfileDB.GetProfileByIDReturns(percy.UserProfile{
				ID:              "",
				Login:           "",
				DisplayName:     "",
				IsStaff:         false,
				PrimaryColorHex: nil,
				ProfileImageURL: "",
				CreatedOn:       time.Time{},
			}, nil)
			userAnniversaryTokensDB.HasChatNotificationTokenReturns(false, nil)

			// first two return values are tested in isUserAnniversary
			_, _, result, err := eng.CanSendUserNotice(ctx, userID)
			So(result, ShouldBeFalse)
			So(err, ShouldBeNil)
		})

		Convey("if it is their anniversary", func() {
			userAnniversaryAllowlistDB.IsUserAnniversaryOverrideReturns(true)
			Convey("should return false if the token exists in the database", func() {
				userProfileDB.GetProfileByIDReturns(percy.UserProfile{
					ID:              "",
					Login:           "",
					DisplayName:     "",
					IsStaff:         false,
					PrimaryColorHex: nil,
					ProfileImageURL: "",
					CreatedOn:       time.Time{},
				}, nil)
				userAnniversaryTokensDB.HasChatNotificationTokenReturns(true, nil)

				// first two return values are tested in isUserAnniversary
				_, _, result, err := eng.CanSendUserNotice(ctx, userID)
				So(result, ShouldBeFalse)
				So(err, ShouldBeNil)
			})
			Convey("should return true if the token is not in the database", func() {
				userProfileDB.GetProfileByIDReturns(percy.UserProfile{
					ID:              "",
					Login:           "",
					DisplayName:     "",
					IsStaff:         false,
					PrimaryColorHex: nil,
					ProfileImageURL: "",
					CreatedOn:       time.Time{},
				}, nil)
				userAnniversaryTokensDB.HasChatNotificationTokenReturns(false, nil)

				// first two return values are tested in isUserAnniversary
				_, _, result, err := eng.CanSendUserNotice(ctx, userID)
				So(result, ShouldBeTrue)
				So(err, ShouldBeNil)
			})
			Convey("should return error if error in database", func() {
				userProfileDB.GetProfileByIDReturns(percy.UserProfile{
					ID:              "",
					Login:           "",
					DisplayName:     "",
					IsStaff:         false,
					PrimaryColorHex: nil,
					ProfileImageURL: "",
					CreatedOn:       time.Time{},
				}, nil)
				userAnniversaryTokensDB.HasChatNotificationTokenReturns(false, errors.New("oops"))

				// first two return values are tested in isUserAnniversary
				_, _, result, err := eng.CanSendUserNotice(ctx, userID)
				So(result, ShouldBeFalse)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
