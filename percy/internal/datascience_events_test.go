package percy_test

import (
	"context"
	"testing"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/miniexperiments/ht_approaching"
	"code.justin.tv/commerce/percy/internal/miniexperiments/miniexperimentsfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	. "github.com/smartystreets/goconvey/convey"
)

func TestDataScience_HypeTrainInitiators(t *testing.T) {
	Convey("Given a hypetrain ending", t, func() {
		participationDB := &internalfakes.FakeParticipationDB{}

		engine := percy.NewEngine(percy.EngineConfig{
			ParticipationDB: participationDB,
		})

		participationTime := time.Now().Add(-1 * time.Minute)
		validHypeTrain := tests.ValidOngoingHypeTrain()

		subsParticipation := percy.Participation{
			ChannelID: tests.ChannelID,
			Timestamp: participationTime,
			User:      percy.NewUser("1"),
			Quantity:  1,
			ParticipationIdentifier: percy.ParticipationIdentifier{
				Source: percy.SubsSource,
				Action: percy.Tier1SubAction,
			},
		}

		bitsParticipation := percy.Participation{
			ChannelID: tests.ChannelID,
			Timestamp: participationTime,
			User:      percy.NewUser("2"),
			Quantity:  1000,
			ParticipationIdentifier: percy.ParticipationIdentifier{
				Source: percy.BitsSource,
				Action: percy.CheerAction,
			},
		}

		partialBitsParticipation := percy.Participation{
			ChannelID: tests.ChannelID,
			Timestamp: participationTime,
			User:      percy.NewUser("3"),
			Quantity:  10,
			ParticipationIdentifier: percy.ParticipationIdentifier{
				Source: percy.BitsSource,
				Action: percy.CheerAction,
			},
		}

		Convey("when a user updates their settings", func() {
			participationDB.GetChannelParticipationsReturns([]percy.Participation{subsParticipation, bitsParticipation, partialBitsParticipation}, nil)

			initiators, err := engine.GetInitiators(context.Background(), validHypeTrain)
			So(err, ShouldBeNil)
			So(initiators["1"].Initiated, ShouldBeTrue)
			So(initiators["2"].Initiated, ShouldBeTrue)
			So(initiators["3"].Initiated, ShouldBeFalse)
		})
	})
}

func TestDataScience_HypeTrainApproaching(t *testing.T) {
	Convey("Given a hypetrain approaching", t, func() {
		approachingExperimentClient := &miniexperimentsfakes.FakeClient{}
		spade := &internalfakes.FakeSpade{}

		engine := percy.NewEngine(percy.EngineConfig{
			Spade:                       spade,
			ApproachingExperimentClient: approachingExperimentClient,
		})
		config := tests.ValidHypeTrainConfig()

		timeStamp := time.Now()
		otherTimeStamp := time.Now().Add(time.Hour)
		hypeTrainID := "hypeTrainID"
		userID := "userID"

		Convey("when ghost mode", func() {
			approachingExperimentClient.GetTreatmentReturns(ht_approaching.TreatmentControl, nil)
			engine.SendHypeTrainApproachingEvent(config, config.ChannelID, 2, hypeTrainID, userID, timeStamp, otherTimeStamp, false, true)

			So(spade.SendHypeTrainApproachingEventCallCount(), ShouldEqual, 1)
			_, params := spade.SendHypeTrainApproachingEventArgsForCall(0)

			So(params[0].ChannelID, ShouldEqual, config.ChannelID)
			So(params[0].RemainingEvents, ShouldEqual, 2)
			So(params[0].Ghostmode, ShouldBeTrue)
			So(params[0].ParticipantUserID, ShouldEqual, userID)
			So(params[0].ColorSelection, ShouldEqual, config.PrimaryHexColor)
			So(params[0].UsePersonalizedSettings, ShouldEqual, config.UsePersonalizedSettings)
			So(params[0].RateLimited, ShouldEqual, false)
			So(params[0].Difficulty, ShouldEqual, string(config.Difficulty))
			So(params[0].StartTime, ShouldEqual, timeStamp)
			So(params[0].EndTime, ShouldEqual, otherTimeStamp)
		})

		Convey("when not ghost mode", func() {
			approachingExperimentClient.GetTreatmentReturns(ht_approaching.TreatmentExperiment, nil)
			engine.SendHypeTrainApproachingEvent(config, config.ChannelID, 1, hypeTrainID, userID, timeStamp, otherTimeStamp, false, false)

			So(spade.SendHypeTrainApproachingEventCallCount(), ShouldEqual, 1)
			_, params := spade.SendHypeTrainApproachingEventArgsForCall(0)

			So(params[0].ChannelID, ShouldEqual, config.ChannelID)
			So(params[0].RemainingEvents, ShouldEqual, 1)
			So(params[0].Ghostmode, ShouldBeFalse)
			So(params[0].ParticipantUserID, ShouldEqual, userID)
			So(params[0].ColorSelection, ShouldEqual, config.PrimaryHexColor)
			So(params[0].UsePersonalizedSettings, ShouldEqual, config.UsePersonalizedSettings)
			So(params[0].RateLimited, ShouldEqual, false)
			So(params[0].Difficulty, ShouldEqual, string(config.Difficulty))
			So(params[0].StartTime, ShouldEqual, timeStamp)
			So(params[0].EndTime, ShouldEqual, otherTimeStamp)
		})
	})
}
