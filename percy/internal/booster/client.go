package booster

import (
	"context"
	"net/http"

	booster_twirp "code.justin.tv/amzn/TwitchPromotionsBoosterTwirp"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/wrapper"
	"code.justin.tv/foundation/twitchclient"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
)

const (
	boostUnitPriceInCents         = 99
	centsToMicroUSDConversionRate = 10000
)

type Booster struct {
	client booster_twirp.TwitchPromotionsBooster
}

func NewClient(host string, statter statsd.Statter, socksAddr string) *Booster {
	return &Booster{
		client: booster_twirp.NewTwitchPromotionsBoosterProtobufClient(host, twitchclient.NewHTTPClient(twitchclient.ClientConf{
			Stats:          statter,
			StatNamePrefix: "booster",
			RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
				wrapper.NewProxyRoundTripWrapper(socksAddr),
				wrapper.NewRetryRoundTripWrapper(statter, "booster", 1),
				wrapper.NewHystrixRoundTripWrapper("booster", 2000),
			},
			Transport: twitchclient.TransportConf{
				MaxIdleConnsPerHost: 200,
			},
		})),
	}
}

func (b *Booster) LaunchBoost(ctx context.Context, opportunity percy.BoostOpportunity) (string, error) {
	resp, err := b.client.CreateOrder(ctx, &booster_twirp.CreateOrderRequest{
		Order: &booster_twirp.Order{
			OrderType:            booster_twirp.OrderType_PAID_ORDER,
			MicroUsdValue:        int64(opportunity.UnitsContributed) * boostUnitPriceInCents * centsToMicroUSDConversionRate,
			PaymentTransactionId: opportunity.ID,
			PurchaserUserId:      opportunity.ChannelID,
			ChannelId:            opportunity.ChannelID,
			GoalMetric:           booster_twirp.GoalMetric_IMPRESSIONS,
			GoalTarget:           int64(opportunity.Impressions()),
			Quantity:             1,
		},
	},
	)
	if err != nil {
		return "", errors.Wrap(err, "failed to create booster order")
	}
	return resp.OrderId, nil
}
