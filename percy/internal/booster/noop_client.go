package booster

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	"github.com/google/uuid"
)

type NoopBooster struct{}

func NewNoopClient() *NoopBooster {
	return &NoopBooster{}
}

func (b *NoopBooster) LaunchBoost(_ context.Context, boostOpportunity percy.BoostOpportunity) (string, error) {
	logrus.WithFields(logrus.Fields{
		"boostOpportunity": fmt.Sprintf("%+v", boostOpportunity),
	}).Info("CreateOrder called in Booster noop client")
	return uuid.New().String(), nil
}
