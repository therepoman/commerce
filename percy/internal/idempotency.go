package percy

import (
	"context"
	"time"
)

type (
	IdempotencyState     string
	IdempotencyNamespace string
)

const (
	IdempotencyStateProcessing = IdempotencyState("processing")
	IdempotencyStateComplete   = IdempotencyState("complete")
	IdempotencyStateError      = IdempotencyState("task_error")

	IdempotencyNamespaceCelebrationPurchaseFulfillment = IdempotencyNamespace("CelebrationPurchaseFulfillment")
	IdempotencyNamespaceCelebrationRevocation          = IdempotencyNamespace("CelebrationPurchaseRevocation")
	IdempotencyNameSpaceBoostOpportunityFulfillment    = IdempotencyNamespace("BoostOpportunityFulfillment")
)

type IdempotentTask struct {
	Namespace     IdempotencyNamespace
	TransactionID string
	Task          string
	State         IdempotencyState
	LastUpdated   time.Time
}

type IdempotentTaskUpdate struct {
	State IdempotencyState
	Error error
}

//go:generate counterfeiter -generate

//counterfeiter:generate . IdempotentTaskDB
type IdempotentTaskDB interface {
	Get(ctx context.Context, namespace IdempotencyNamespace, transactionID, taskName string) (*IdempotentTask, error)
	Update(ctx context.Context, namespace IdempotencyNamespace, transactionID, taskName string, update IdempotentTaskUpdate) (*IdempotentTask, error)
}

//counterfeiter:generate . IdempotencyEnforcer
type IdempotencyEnforcer interface {
	WithIdempotency(ctx context.Context, namespace IdempotencyNamespace, transactionID, taskName string, processingFunc func() error) error
}
