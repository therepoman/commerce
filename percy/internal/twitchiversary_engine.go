package percy

import (
	"context"
	"time"

	"code.justin.tv/commerce/percy/internal/metrics"
	"github.com/cactus/go-statsd-client/statsd"
)

//go:generate counterfeiter . TwitchiversaryEngine

type TwitchiversaryEngine interface {
	IsUserAnniversary(ctx context.Context, channelID string, now time.Time) (bool, int64, error)
	CanSendUserNotice(ctx context.Context, userID string) (bool, int64, bool, error)
	AddChatNotificationToken(ctx context.Context, userID string, years int64) error
	DeleteChatNotificationToken(ctx context.Context, userID string, years int64) error
	SendUserAnniversaryNotice(ctx context.Context, req UserAnniversaryNoticeArgs) error
}

type twitchiversaryEngine struct {
	UserAnniversaryAllowlistDB UserAnniversaryAllowlistDB
	UserProfileDB              UserProfileDB
	Statter                    statsd.Statter
	UserAnniversaryCache       UserAnniversaryCache
	UserAnniversaryTokensDB    UserAnniversaryTokensDB
}

type TwitchiversaryEngineConfig struct {
	UserAnniversaryAllowlistDB UserAnniversaryAllowlistDB
	UserProfileDB              UserProfileDB
	Statter                    statsd.Statter
	UserAnniversaryCache       UserAnniversaryCache
	UserAnniversaryTokensDB    UserAnniversaryTokensDB
}

// NewTwitchiversaryEngine ...
func NewTwitchiversaryEngine(config TwitchiversaryEngineConfig) TwitchiversaryEngine {
	statter := config.Statter
	if statter == nil {
		statter = &metrics.NoopStatter{}
	}
	return &twitchiversaryEngine{
		UserAnniversaryAllowlistDB: config.UserAnniversaryAllowlistDB,
		UserProfileDB:              config.UserProfileDB,
		Statter:                    statter,
		UserAnniversaryCache:       config.UserAnniversaryCache,
		UserAnniversaryTokensDB:    config.UserAnniversaryTokensDB,
	}
}
