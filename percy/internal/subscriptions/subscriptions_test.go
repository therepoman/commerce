package subscriptions

import (
	"context"
	"testing"
	"time"

	"code.justin.tv/commerce/percy/internal/subscriptions/subscriptionsfakes"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSubscriptionsDB_GetGetChannelOnboardDate(t *testing.T) {
	Convey("Given a subscriptions DB", t, func() {
		client := &subscriptionsfakes.FakeSubscriptions{}

		db := SubscriptionsDB{
			client: client,
		}

		channelID := "12345"
		startedAt := time.Now()

		Convey("returns channel's onboard date", func() {
			Convey("when the channel' subscription to itself found", func() {
				client.GetUserChannelSubscriptionReturns(&substwirp.GetUserChannelSubscriptionResponse{
					Subscription: &substwirp.Subscription{
						BenefitStart: &timestamp.Timestamp{Seconds: startedAt.Unix()},
					},
				}, nil)
			})

			onboardedAt, err := db.GetChannelOnboardDate(context.Background(), channelID)
			So(err, ShouldBeNil)
			So(onboardedAt, ShouldNotBeNil)
			So(onboardedAt.Unix(), ShouldEqual, startedAt.Unix())
		})

		Convey("returns nothing", func() {
			Convey("when channel does not have a subscription to itself", func() {
				client.GetUserChannelSubscriptionReturns(&substwirp.GetUserChannelSubscriptionResponse{}, nil)
			})

			Convey("when the start date of the subscription is missing", func() {
				client.GetUserChannelSubscriptionReturns(&substwirp.GetUserChannelSubscriptionResponse{
					Subscription: &substwirp.Subscription{},
				}, nil)
			})

			onboardedAt, err := db.GetChannelOnboardDate(context.Background(), channelID)
			So(err, ShouldBeNil)
			So(onboardedAt, ShouldBeNil)
		})

		Convey("returns an error", func() {
			Convey("when given channel ID is blank", func() {
				channelID = ""
			})

			Convey("when subscription service returns an error", func() {
				client.GetUserChannelSubscriptionReturns(nil, errors.New("can't find it"))
			})

			Convey("when the start date of the subscription fails to parse", func() {
				client.GetUserChannelSubscriptionReturns(&substwirp.GetUserChannelSubscriptionResponse{
					Subscription: &substwirp.Subscription{
						BenefitStart: &timestamp.Timestamp{Seconds: -1, Nanos: -1},
					},
				}, nil)
			})

			_, err := db.GetChannelOnboardDate(context.Background(), channelID)
			So(err, ShouldNotBeNil)
		})
	})
}
