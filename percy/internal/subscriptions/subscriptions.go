package subscriptions

import (
	"context"
	"net/http"
	"time"

	"code.justin.tv/commerce/percy/internal/wrapper"
	"code.justin.tv/foundation/twitchclient"
	substwirp "code.justin.tv/revenue/subscriptions/twirp"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/golang/protobuf/ptypes"
	"github.com/pkg/errors"
)

const (
	serviceName    = "subscriptions"
	defaultRetry   = 1
	defaultTimeout = 1000

	getChannelProductsAPI      = "GetChannelProducts"
	getUserChannelSubscription = "GetUserChannelSubscription"
)

type SubscriptionsDB struct {
	client substwirp.Subscriptions
}

func NewSubscriptionsDB(host string, statter statsd.Statter, socksAddr string) (*SubscriptionsDB, error) {
	client := substwirp.NewSubscriptionsProtobufClient(host, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Stats:          statter,
		StatNamePrefix: serviceName,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(socksAddr),
			wrapper.NewRetryRoundTripWrapper(statter, serviceName, defaultRetry),
			wrapper.NewHystrixRoundTripWrapper(serviceName, defaultTimeout),
		},
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: 200,
		},
	}))

	return &SubscriptionsDB{
		client: client,
	}, nil
}

func (db *SubscriptionsDB) GetChannelEmoticonSets(ctx context.Context, channelID string) ([]string, error) {
	if len(channelID) == 0 {
		return nil, errors.New("empty channel ID")
	}

	reqCtx := wrapper.GenerateRequestContext(ctx, serviceName, getChannelProductsAPI)
	req := &substwirp.GetChannelProductsRequest{
		ChannelId: channelID,
	}

	resp, err := db.client.GetChannelProducts(reqCtx, req)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get channel products response")
	}

	emoticonSets := make([]string, 0, len(resp.Products))
	for _, product := range resp.Products {
		if product != nil {
			emoticonSets = append(emoticonSets, product.EmoticonSetId)
		}
	}

	return emoticonSets, nil
}

func (db *SubscriptionsDB) GetChannelOnboardDate(ctx context.Context, channelID string) (*time.Time, error) {
	if len(channelID) == 0 {
		return nil, errors.New("empty channel ID")
	}

	reqCtx := wrapper.GenerateRequestContext(ctx, serviceName, getUserChannelSubscription)
	resp, err := db.client.GetUserChannelSubscription(reqCtx, &substwirp.GetUserChannelSubscriptionRequest{
		OwnerId:   channelID,
		ChannelId: channelID,
	})
	if err != nil {
		return nil, err
	}

	// return nothing if there is no subscription product for this channel
	if resp.Subscription == nil {
		return nil, nil
	}

	// return nothing if there is no benefit start date
	if resp.Subscription.BenefitStart == nil {
		return nil, nil
	}

	startedAt, err := ptypes.Timestamp(resp.Subscription.BenefitStart)
	if err != nil {
		return nil, errors.Wrap(err, "failed to convert benefit start timestamp")
	}

	return &startedAt, nil
}
