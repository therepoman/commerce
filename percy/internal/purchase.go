package percy

import (
	"context"
	"time"
)

type (
	RevenueSource          string
	RevenueTransactionType string
)

const (
	RevenueSourceCelebration = RevenueSource("experimental")

	RevenueTransactionTypePurchase = RevenueTransactionType("celebration-purchased")
	RevenueTransactionTypeRefund   = RevenueTransactionType("celebration-refund")
)

type CreatorRevenueShareArgs struct {
	RecipientUserID string
	PayoutCents     int64
	PartnerType     PartnerType
	TransactionID   string
	TimeOfEvent     time.Time
	OfferID         string
	RevenueSource   RevenueSource
	TransactionType RevenueTransactionType
}

type PurchaseOrderPayment struct {
	GrossAmount int64
	Fees        int64
	Taxes       int64
	IsRefund    bool
}

//go:generate counterfeiter . Purchase

type Purchase interface {
	GetPurchaseOrderPayments(ctx context.Context, purchaseOriginID string) ([]PurchaseOrderPayment, error)
	PostCreatorRevenueShareForPurchase(ctx context.Context, args CreatorRevenueShareArgs) error
	RefundPurchaseOrder(ctx context.Context, purchaseOriginID string, refundReason string) error
}
