package percy

import (
	"context"
)

//go:generate counterfeiter . Leaderboard

type Leaderboard interface {
	Publish(ctx context.Context, leaderboardID LeaderboardID, participation Participation) error
	BatchPublish(ctx context.Context, channelID, hypeTrainID string, participations ...Participation) error

	// Top returns the top-ranking user from the leaderboard, returns nil if no user is tracked on the leaderboard.
	Top(ctx context.Context, leaderboardID LeaderboardID) (*User, error)

	// Moderate excludes a user from the leaderboard, until the user is unmoderated.
	Moderate(ctx context.Context, leaderboardID LeaderboardID, userID string) error

	// Unmoderate brings a user back onto the leaderboard, if the user was previously moderated from the leaderboard.
	Unmoderate(ctx context.Context, leaderboardID LeaderboardID, userID string) error
}

type LeaderboardID struct {
	ChannelID   string
	HypeTrainID string
	Source      ParticipationSource
}
