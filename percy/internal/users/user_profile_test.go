package users

import (
	"context"
	"testing"

	tmi "code.justin.tv/chat/tmi/client"
	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/percy/internal/users/usersfakes"
	riptwirp "code.justin.tv/revenue/ripley/rpc"
	"code.justin.tv/web/users-service/models"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestUserProfileDB_GetProfileByID(t *testing.T) {
	Convey("Given a user profile DB", t, func() {
		client := usersfakes.FakeUserServiceWrapper{}
		db := UserProfileDB{
			userService: &client,
		}

		ctx := context.Background()
		id := uuid.New().String()

		Convey("returns user's profile", func() {
			displayName := ""
			profileImageURL := ""

			Convey("when there's not profile pic / display info", func() {
				client.GetUserByIDReturns(&models.Properties{
					ID:    id,
					Login: pointers.StringP("someLogin"),
				}, nil)
			})

			Convey("when there's display info", func() {
				displayName = "SomeDisplayName"
				client.GetUserByIDReturns(&models.Properties{
					ID:          id,
					Login:       pointers.StringP("someLogin"),
					Displayname: pointers.StringP(displayName),
				}, nil)
			})

			Convey("when there's a profile pic", func() {
				profileImageURL = "50x50 hi"
				client.GetUserByIDReturns(&models.Properties{
					ID:              id,
					Login:           pointers.StringP("someLogin"),
					ProfileImageURL: pointers.StringP("%s hi"),
				}, nil)
			})

			profile, err := db.GetProfileByID(ctx, id)
			So(err, ShouldBeNil)
			So(profile.ID, ShouldEqual, id)
			So(profile.Login, ShouldEqual, "someLogin")
			So(profile.DisplayName, ShouldEqual, displayName)
			So(profile.ProfileImageURL, ShouldEqual, profileImageURL)
		})

		Convey("returns an error if", func() {
			Convey("user ID is blank", func() {
				id = ""
			})

			Convey("user service client errors", func() {
				client.GetUserByIDReturns(nil, errors.New("can't"))
			})

			Convey("user's login is missing", func() {
				client.GetUserByIDReturns(&models.Properties{
					ID:    id,
					Login: nil,
				}, nil)
			})

			_, err := db.GetProfileByID(ctx, id)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestUserProfileDB_IsChatBanned(t *testing.T) {
	Convey("Given a user profile DB", t, func() {
		client := usersfakes.FakeTmiWrapper{}
		db := UserProfileDB{
			tmi: &client,
		}

		ctx := context.Background()

		Convey("returns user's ban status", func() {
			client.GetBanStatusReturns(tmi.ChannelBannedUser{}, true, nil)

			banned, err := db.IsChatBanned(ctx, "123", "456")
			So(err, ShouldBeNil)
			So(banned, ShouldBeTrue)
		})

		Convey("returns an error if", func() {
			Convey("user ID is blank", func() {
				_, err := db.IsChatBanned(ctx, "", "456")
				So(err, ShouldNotBeNil)
			})

			Convey("channel ID is blank", func() {
				_, err := db.IsChatBanned(ctx, "123", "")
				So(err, ShouldNotBeNil)
			})

			Convey("tmi returns an error", func() {
				client.GetBanStatusReturns(tmi.ChannelBannedUser{}, false, errors.New("ERROR"))
				_, err := db.IsChatBanned(ctx, "123", "456")
				So(err, ShouldNotBeNil)
			})
		})
	})
}

func TestUserProfileDB_IsMonetized(t *testing.T) {
	Convey("Given a user profile DB", t, func() {
		client := usersfakes.FakeRipleyWrapper{}
		db := UserProfileDB{
			ripley: &client,
		}

		ctx := context.Background()

		Convey("returns true if user is a partner", func() {
			client.GetPayoutTypeReturns(&riptwirp.GetPayoutTypeResponse{
				PayoutType: &riptwirp.PayoutType{
					IsPartner: true,
				},
			}, nil)

			monetized, err := db.IsMonetized(ctx, "123")
			So(err, ShouldBeNil)
			So(monetized, ShouldBeTrue)
		})

		Convey("returns true if user is a affiliate", func() {
			client.GetPayoutTypeReturns(&riptwirp.GetPayoutTypeResponse{
				PayoutType: &riptwirp.PayoutType{
					IsAffiliate: true,
				},
			}, nil)

			monetized, err := db.IsMonetized(ctx, "123")
			So(err, ShouldBeNil)
			So(monetized, ShouldBeTrue)
		})

		Convey("returns false if", func() {
			Convey("payout type is nil", func() {
				client.GetPayoutTypeReturns(&riptwirp.GetPayoutTypeResponse{
					PayoutType: nil,
				}, nil)

				monetized, err := db.IsMonetized(ctx, "123")
				So(err, ShouldBeNil)
				So(monetized, ShouldBeFalse)
			})

			Convey("user is neither partner nor affiliate", func() {
				client.GetPayoutTypeReturns(&riptwirp.GetPayoutTypeResponse{
					PayoutType: &riptwirp.PayoutType{
						IsAffiliate: false,
						IsPartner:   false,
					},
				}, nil)

				monetized, err := db.IsMonetized(ctx, "123")
				So(err, ShouldBeNil)
				So(monetized, ShouldBeFalse)
			})
		})

		Convey("returns an error if", func() {
			Convey("channel ID is blank", func() {
				_, err := db.IsMonetized(ctx, "")
				So(err, ShouldNotBeNil)
			})

			Convey("ripley returns an error", func() {
				client.GetPayoutTypeReturns(&riptwirp.GetPayoutTypeResponse{}, errors.New("ERROR"))
				_, err := db.IsMonetized(ctx, "123")
				So(err, ShouldNotBeNil)
			})
		})
	})
}
