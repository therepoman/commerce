package users

import (
	"context"
	"fmt"
	"net/http"
	"time"

	tmiService "code.justin.tv/chat/tmi/client"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/wrapper"
	"code.justin.tv/foundation/twitchclient"
	ripleyService "code.justin.tv/revenue/ripley/rpc"
	"code.justin.tv/web/users-service/client/usersclient_internal"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
)

const (
	userServiceName   = "users-service"
	tmiServiceName    = "tmi"
	ripleyServiceName = "ripley"
	defaultRetry      = 1
	defaultTimeout    = 1000
	profileImageSize  = "50x50" // supported sizes: https://git.xarth.tv/common/yimg/blob/master/profile_image.go#L10

	// user service
	getUserByIDAPI = "getUserByID"

	// tmi
	getBanStatusAPI   = "getBanStatus"
	sendUserNoticeAPI = "sendUserNotice"

	// ripley
	getPayoutTypeAPI = "getPayoutType"
)

//go:generate counterfeiter -generate

//counterfeiter:generate . tmiWrapper
//counterfeiter:generate . ripleyWrapper
//counterfeiter:generate . userServiceWrapper

// Wrapped so that we can generate fakes
type tmiWrapper interface {
	tmiService.Client
}

type ripleyWrapper interface {
	ripleyService.Ripley
}

type userServiceWrapper interface {
	usersclient_internal.InternalClient
}

type UserProfileDB struct {
	userService userServiceWrapper
	tmi         tmiWrapper
	ripley      ripleyWrapper
}

func NewUserProfileDB(userServiceEndpoint, tmiEndpoint, ripleyEndpoint string, stats statsd.Statter, socksAddr string) (*UserProfileDB, error) {
	userService, err := usersclient_internal.NewClient(twitchclient.ClientConf{
		Stats: stats,
		Host:  userServiceEndpoint,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(socksAddr),
			wrapper.NewRetryRoundTripWrapper(stats, userServiceName, defaultRetry),
			wrapper.NewHystrixRoundTripWrapper(userServiceName, defaultTimeout),
		},
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: 200,
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to initialize userService")
	}

	tmi, err := tmiService.NewClient(twitchclient.ClientConf{
		Stats: stats,
		Host:  tmiEndpoint,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(socksAddr),
			wrapper.NewRetryRoundTripWrapper(stats, tmiServiceName, defaultRetry),
			wrapper.NewHystrixRoundTripWrapper(tmiServiceName, defaultTimeout),
		},
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: 200,
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to initialize tmi")
	}

	ripley := ripleyService.NewRipleyProtobufClient(ripleyEndpoint, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Stats:          stats,
		StatNamePrefix: ripleyServiceName,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(socksAddr),
			wrapper.NewRetryRoundTripWrapper(stats, ripleyServiceName, defaultRetry),
			wrapper.NewHystrixRoundTripWrapper(ripleyServiceName, defaultTimeout),
		},
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: 200,
		},
	}))

	return &UserProfileDB{
		userService: userService,
		tmi:         tmi,
		ripley:      ripley,
	}, nil
}

func (db *UserProfileDB) GetProfileByID(ctx context.Context, id string) (percy.UserProfile, error) {
	if len(id) == 0 {
		return percy.UserProfile{}, errors.New("empty user ID")
	}

	reqCtx := wrapper.GenerateRequestContext(ctx, userServiceName, getUserByIDAPI)
	resp, err := db.userService.GetUserByID(reqCtx, id, nil)
	if err != nil {
		return percy.UserProfile{}, errors.Wrap(err, "failed to get user profile response")
	}

	if resp.Login == nil {
		return percy.UserProfile{}, errors.New("user login is missing")
	}

	profileImage := ""
	if resp.ProfileImageURL != nil {
		profileImage = fmt.Sprintf(*resp.ProfileImageURL, profileImageSize)
	}

	displayName := ""
	if resp.Displayname != nil {
		displayName = *resp.Displayname
	}

	createdOn := time.Time{}
	if resp.CreatedOn != nil {
		createdOn = *resp.CreatedOn
	}

	return percy.UserProfile{
		ID:              id,
		Login:           *resp.Login,
		DisplayName:     displayName,
		IsStaff:         resp.Admin != nil && *resp.Admin,
		PrimaryColorHex: resp.PrimaryColorHex,
		ProfileImageURL: profileImage,
		CreatedOn:       createdOn,
	}, nil
}

func (db *UserProfileDB) IsChatBanned(ctx context.Context, userID, channelID string) (bool, error) {
	if userID == "" {
		return false, errors.New("empty user ID")
	}

	if channelID == "" {
		return false, errors.New("empty channel ID")
	}

	reqCtx := wrapper.GenerateRequestContext(ctx, tmiServiceName, getBanStatusAPI)
	_, banned, err := db.tmi.GetBanStatus(reqCtx, channelID, userID, nil, nil)
	if err != nil {
		return false, errors.Wrap(err, "error calling TMI to get channel ban status")
	}

	return banned, nil
}

func (db *UserProfileDB) IsMonetized(ctx context.Context, channelID string) (bool, error) {
	partnerType, err := db.GetPartnerType(ctx, channelID)
	if err != nil {
		return false, err
	}

	if partnerType == percy.PartnerTypeAffiliate || partnerType == percy.PartnerTypePartner {
		return true, nil
	}

	return false, nil
}

func (db *UserProfileDB) GetPartnerType(ctx context.Context, channelID string) (percy.PartnerType, error) {
	if channelID == "" {
		return "", errors.New("empty channel ID")
	}

	reqCtx := wrapper.GenerateRequestContext(ctx, ripleyServiceName, getPayoutTypeAPI)
	resp, err := db.ripley.GetPayoutType(reqCtx, &ripleyService.GetPayoutTypeRequest{
		ChannelId: channelID,
	})
	if err != nil {
		return "", err
	}

	if resp.PayoutType == nil {
		return percy.PartnerTypeUnknown, nil
	}

	if resp.PayoutType.IsPartner {
		return percy.PartnerTypePartner, nil
	}

	if resp.PayoutType.IsAffiliate {
		return percy.PartnerTypeAffiliate, nil
	}

	return percy.PartnerTypeUnknown, nil
}

func (db *UserProfileDB) SendUserNotice(ctx context.Context, args percy.UserNoticeParams) error {
	tmiMsgParams := make([]tmiService.UserNoticeMsgParam, 0)
	for _, msgParam := range args.MessageParams {
		tmiMsgParams = append(tmiMsgParams, tmiService.UserNoticeMsgParam{
			Key:   msgParam.Key,
			Value: msgParam.Value,
		})
	}

	tmiArgs := tmiService.SendUserNoticeParams{
		SenderUserID:      args.SenderUserID,
		TargetChannelID:   args.TargetChannelID,
		Body:              args.Body,
		MsgID:             args.MessageID,
		MsgParams:         tmiMsgParams,
		DefaultSystemBody: args.DefaultSystemBody,
	}

	reqCtx := wrapper.GenerateRequestContext(ctx, tmiServiceName, sendUserNoticeAPI)
	err := db.tmi.SendUserNotice(reqCtx, tmiArgs, nil)
	if err != nil {
		return errors.Wrap(err, "error calling TMI#SendUserNotice")
	}

	return nil
}
