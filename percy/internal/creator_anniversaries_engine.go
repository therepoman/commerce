package percy

import (
	"context"
	"strconv"
	"time"
)

//go:generate counterfeiter . CreatorAnniversariesEngine

type CreatorAnniversariesEngine interface {
	IsCreatorAnniversariesExperimentEnabled(t time.Time) bool
	GetCreatorAnniversariesByDate(month int, day int) (map[string]CreatorAnniversary, error)
	GetCreatorAnniversariesByChannelIds(channelIds []string) ([]CreatorAnniversaryWithYears, error)
	PublishCreatorAnniversaryNotification(ctx context.Context, notificationType string, channelId string, creatorCategory string, years int) (string, error)
	ChannelHasSentAnniversaryNotification(ctx context.Context, channelID string, campaignType string) (bool, error)
	PutCreatorAnniversaryNotification(ctx context.Context, channelID string, anniversaryDate time.Time, notificationTraceId string, campaignType string) error
}

type creatorAnniversariesEngine struct {
	CreatorAnniversariesDB  CreatorAnniversariesDB
	Notification            Notification
	NotificationIdempotency CreatorAnniversariesNotificationDB
}

type CreatorAnniversariesEngineConfig struct {
	CreatorAnniversariesDB  CreatorAnniversariesDB
	Notification            Notification
	NotificationIdempotency CreatorAnniversariesNotificationDB
}

// New CreatorAnniversaries Engine
func NewCreatorAnniversariesEngine(config CreatorAnniversariesEngineConfig) *creatorAnniversariesEngine {
	return &creatorAnniversariesEngine{
		CreatorAnniversariesDB:  config.CreatorAnniversariesDB,
		Notification:            config.Notification,
		NotificationIdempotency: config.NotificationIdempotency,
	}
}

func (e *creatorAnniversariesEngine) PublishCreatorAnniversaryNotification(ctx context.Context, notificationType string, channelId string, creatorCategory string, years int) (string, error) {
	return e.Notification.PublishCreatorAnniversaryNotification(ctx, PublishCreatorAnniversaryNotificationArgs{
		ChannelID:        channelId,
		CreatorType:      creatorCategory,
		NotificationType: notificationType,
		Years:            strconv.Itoa(years),
	})
}
