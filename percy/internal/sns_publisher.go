package percy

import (
	"context"

	"code.justin.tv/commerce/percy/models"
)

//go:generate counterfeiter . SNSPublisher

type SNSPublisher interface {
	PublishWebhookEvent(ctx context.Context, hypeTrain HypeTrain, lastParticipation Participation) error
	PublishActivityFeedEvent(ctx context.Context, train HypeTrain, eventType models.ActivityFeedEventType) error
	PublishRevenueShareEvent(ctx context.Context, args CreatorRevenueShareArgs) error
	PublishCelebrationActivityFeedEvent(ctx context.Context, event models.CelebrationPurchaseActivityFeedEvent) error
}
