package metrics

import (
	"time"

	"github.com/cactus/go-statsd-client/statsd"
)

type NoopStatter struct{}

func (s *NoopStatter) Inc(string, int64, float32) error {
	return nil
}

func (s *NoopStatter) Dec(string, int64, float32) error {
	return nil
}

func (s *NoopStatter) Gauge(string, int64, float32) error {
	return nil
}

func (s *NoopStatter) GaugeDelta(string, int64, float32) error {
	return nil
}

func (s *NoopStatter) Timing(string, int64, float32) error {
	return nil
}

func (s *NoopStatter) TimingDuration(string, time.Duration, float32) error {
	return nil
}

func (s *NoopStatter) Set(string, string, float32) error {
	return nil
}

func (s *NoopStatter) SetInt(string, int64, float32) error {
	return nil
}

func (s *NoopStatter) Raw(string, string, float32) error {
	return nil
}

func (s *NoopStatter) NewSubStatter(string) statsd.SubStatter {
	return nil
}

func (s *NoopStatter) SetPrefix(string) {
}

func (s *NoopStatter) Close() error {
	return nil
}
