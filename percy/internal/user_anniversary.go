package percy

import (
	"context"
	"math"
	"strconv"
	"time"

	"code.justin.tv/commerce/logrus"
	"github.com/pkg/errors"
)

//go:generate counterfeiter . UserAnniversaryAllowlistDB
type UserAnniversaryAllowlistDB interface {
	IsUserAnniversaryOverride(channelID string) bool
	IsUserAnniversaryExperimentEnabled() bool
}

//go:generate counterfeiter . UserAnniversaryCache
type UserAnniversaryCache interface {
	AddNewCreationDate(ctx context.Context, channelID string, createdOn time.Time) error
	GetCreationDate(ctx context.Context, channelID string) (time.Time, error)
}

//go:generate counterfeiter . UserAnniversaryTokensDB
type UserAnniversaryTokensDB interface {
	HasChatNotificationToken(ctx context.Context, userID string, years string) (bool, error)
	AddChatNotificationToken(ctx context.Context, userID string, years string) error
	DeleteChatNotificationToken(ctx context.Context, userID string, years string) error
}

type UserAnniversaryNoticeArgs struct {
	SenderID  string
	ChannelID string
	Message   string
	Years     int64
}

const (
	UserAnniversaryNoticeID                = "useranniversary"
	userAnniversaryNoticeYearsKey          = "years"
	userAnniversaryNoticeDefaultSystemBody = "user anniversary notification"
)

// IsUserAnniversary returns if it is the user's anniversary and anniversary years
func (t *twitchiversaryEngine) IsUserAnniversary(ctx context.Context, channelID string, now time.Time) (bool, int64, error) {
	// if user is only the override list, always return true
	if t.UserAnniversaryAllowlistDB.IsUserAnniversaryOverride(channelID) {
		return true, 5, nil
	}

	// if the experiment is not enabled, always return false
	if !t.UserAnniversaryAllowlistDB.IsUserAnniversaryExperimentEnabled() {
		return false, 0, nil
	}

	createdOn, err := t.UserAnniversaryCache.GetCreationDate(ctx, channelID)
	if err != nil {
		logrus.WithError(err).Error("Error accessing user service cache")
	}

	if createdOn.IsZero() {
		userProfile, err := t.UserProfileDB.GetProfileByID(ctx, channelID)
		if err != nil {
			return false, 0, err
		}

		createdOn = userProfile.CreatedOn

		go func() {
			ctx, cancel := context.WithTimeout(context.Background(), time.Second)
			defer cancel()

			if err := t.UserAnniversaryCache.AddNewCreationDate(ctx, channelID, createdOn); err != nil {
				// Prevent any cache failures from preventing a successful response.
				logrus.WithError(err).Error("Failed to write to the redis cache during IsUserAnniversary")
			}
		}()
	}

	anniversaryYears := int64(math.Round((now.Sub(createdOn).Hours() / 24.0) / 365.0))

	// do not celebrate on actual date of account creation
	if anniversaryYears == 0 {
		return false, 0, nil
	}

	year := createdOn.Year() + int(anniversaryYears)

	// time of the current user anniversary
	closestAnniveraryTime := time.Date(year, createdOn.Month(), createdOn.Day(), createdOn.Hour(), createdOn.Minute(), createdOn.Second(), 0, time.UTC)

	start := closestAnniveraryTime.Add(time.Duration(-6) * time.Hour)
	end := closestAnniveraryTime.Add(time.Duration(30) * time.Hour)

	// fixing rounding error
	if now.Before(start) {
		anniversaryYears--
	}

	if now.After(start) && now.Before(end) {
		return true, anniversaryYears, nil
	}

	return false, anniversaryYears, nil
}

// CanSendUserNotice returns if it is the user's anniversary, anniversary years, and if they can send a user notice,
func (t *twitchiversaryEngine) CanSendUserNotice(ctx context.Context, userID string) (bool, int64, bool, error) {
	isUserAnniversary, years, err := t.IsUserAnniversary(ctx, userID, time.Now())
	if err != nil {
		return isUserAnniversary, years, false, err
	}
	if !isUserAnniversary {
		return isUserAnniversary, years, false, nil
	}

	hasChatNotificationToken, err := t.UserAnniversaryTokensDB.HasChatNotificationToken(ctx, userID, strconv.FormatInt(years, 10))
	if err != nil {
		return isUserAnniversary, years, false, err
	}
	if hasChatNotificationToken {
		return isUserAnniversary, years, false, nil
	}

	return isUserAnniversary, years, true, nil
}

func (t *twitchiversaryEngine) AddChatNotificationToken(ctx context.Context, userID string, years int64) error {
	err := t.UserAnniversaryTokensDB.AddChatNotificationToken(ctx, userID, strconv.FormatInt(years, 10))
	return err
}

func (t *twitchiversaryEngine) DeleteChatNotificationToken(ctx context.Context, userID string, years int64) error {
	err := t.UserAnniversaryTokensDB.DeleteChatNotificationToken(ctx, userID, strconv.FormatInt(years, 10))
	return err
}

func (t *twitchiversaryEngine) SendUserAnniversaryNotice(ctx context.Context, req UserAnniversaryNoticeArgs) error {
	parsedSenderID, err := strconv.Atoi(req.SenderID)
	if err != nil {
		return errors.Wrap(err, "error parsing sender userID as int")
	}

	parsedChannelID, err := strconv.Atoi(req.ChannelID)
	if err != nil {
		return errors.Wrap(err, "error parsing channel userID as int")
	}

	err = t.UserProfileDB.SendUserNotice(ctx, UserNoticeParams{
		SenderUserID:    parsedSenderID,
		TargetChannelID: parsedChannelID,
		Body:            req.Message,
		MessageID:       UserAnniversaryNoticeID,
		MessageParams: []UserNoticeMessageParams{
			{
				Key:   userAnniversaryNoticeYearsKey,
				Value: strconv.FormatInt(req.Years, 10),
			},
		},
		DefaultSystemBody: userAnniversaryNoticeDefaultSystemBody,
	})

	if err != nil {
		return errors.Wrap(err, "error sending user notice for user anniversary")
	}

	return nil
}
