package purchase

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/percy/internal/purchase/purchasefakes"
	everdeentwirp "code.justin.tv/revenue/everdeen/rpc"
	mulantwirp "code.justin.tv/revenue/mulan/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPurchaseDB_GetPurchaseOrderPayments(t *testing.T) {
	Convey("Given a purchase DB", t, func() {
		mulan := purchasefakes.FakeMulanWrapper{}
		db := PurchaseDB{
			mulan: &mulan,
		}

		Convey("returns an error if", func() {
			Convey("purchaseOriginID is empty", func() {
				_, err := db.GetPurchaseOrderPayments(context.Background(), "")
				So(err, ShouldNotBeNil)
			})

			Convey("mulan returns an error", func() {
				mulan.GetPurchaseOrderPaymentsByIDsReturns(nil, errors.New("ERROR"))
				_, err := db.GetPurchaseOrderPayments(context.Background(), "123")
				So(err, ShouldNotBeNil)
			})

			Convey("purchaseOrderPayments is empty", func() {
				mulan.GetPurchaseOrderPaymentsByIDsReturns(&mulantwirp.GetPurchaseOrderPaymentsByIDsResponse{
					PurchaseOrderPayments: map[string]*mulantwirp.PurchaseOrderPayments{
						"123": nil,
					},
				}, nil)
				_, err := db.GetPurchaseOrderPayments(context.Background(), "123")
				So(err, ShouldNotBeNil)
			})

			Convey("purchaseOrderPayments.PurchaseOrderPayments has 0 len", func() {
				mulan.GetPurchaseOrderPaymentsByIDsReturns(&mulantwirp.GetPurchaseOrderPaymentsByIDsResponse{
					PurchaseOrderPayments: map[string]*mulantwirp.PurchaseOrderPayments{
						"123": {
							PurchaseOrderPayments: []*mulantwirp.PurchaseOrderPayment{},
						},
					},
				}, nil)
				_, err := db.GetPurchaseOrderPayments(context.Background(), "123")
				So(err, ShouldNotBeNil)
			})
		})

		Convey("returns nil if", func() {
			Convey("mulan returns a nil response", func() {
				mulan.GetPurchaseOrderPaymentsByIDsReturns(nil, nil)
				orders, err := db.GetPurchaseOrderPayments(context.Background(), "123")
				So(err, ShouldBeNil)
				So(orders, ShouldHaveLength, 0)
			})

			Convey("originID is not represented in response", func() {
				mulan.GetPurchaseOrderPaymentsByIDsReturns(&mulantwirp.GetPurchaseOrderPaymentsByIDsResponse{
					PurchaseOrderPayments: map[string]*mulantwirp.PurchaseOrderPayments{},
				}, nil)
				orders, err := db.GetPurchaseOrderPayments(context.Background(), "123")
				So(err, ShouldBeNil)
				So(orders, ShouldHaveLength, 0)
			})
		})

		Convey("returns list of purchase order payments", func() {
			mulan.GetPurchaseOrderPaymentsByIDsReturns(&mulantwirp.GetPurchaseOrderPaymentsByIDsResponse{
				PurchaseOrderPayments: map[string]*mulantwirp.PurchaseOrderPayments{
					"123": {
						PurchaseOrderPayments: []*mulantwirp.PurchaseOrderPayment{
							{},
						},
					},
				},
			}, nil)
			orders, err := db.GetPurchaseOrderPayments(context.Background(), "123")
			So(err, ShouldBeNil)
			So(orders, ShouldHaveLength, 1)
		})
	})
}

func TestPurchaseDB_RefundPurchaseOrder(t *testing.T) {
	Convey("Given a purchase DB", t, func() {
		mulan := purchasefakes.FakeMulanWrapper{}
		everdeen := purchasefakes.FakeEverdeenWrapper{}
		db := PurchaseDB{
			mulan:    &mulan,
			everdeen: &everdeen,
		}

		Convey("returns an error if", func() {
			Convey("mulan returns an error", func() {
				mulan.GetPurchaseOrderPaymentsByIDsReturns(nil, errors.New("ERROR"))
				err := db.RefundPurchaseOrder(context.Background(), "123", "foo")
				So(err, ShouldNotBeNil)
			})

			Convey("gross amount is 0", func() {
				mulan.GetPurchaseOrderPaymentsByIDsReturns(&mulantwirp.GetPurchaseOrderPaymentsByIDsResponse{
					PurchaseOrderPayments: map[string]*mulantwirp.PurchaseOrderPayments{
						"123": {
							PurchaseOrderPayments: []*mulantwirp.PurchaseOrderPayment{
								{
									GrossAmountUsd: 0,
								},
							},
						},
					},
				}, nil)
				err := db.RefundPurchaseOrder(context.Background(), "123", "foo")
				So(err, ShouldNotBeNil)
			})

			Convey("gross amount is less than 0", func() {
				mulan.GetPurchaseOrderPaymentsByIDsReturns(&mulantwirp.GetPurchaseOrderPaymentsByIDsResponse{
					PurchaseOrderPayments: map[string]*mulantwirp.PurchaseOrderPayments{
						"123": {
							PurchaseOrderPayments: []*mulantwirp.PurchaseOrderPayment{
								{
									GrossAmountUsd: -1,
								},
							},
						},
					},
				}, nil)
				err := db.RefundPurchaseOrder(context.Background(), "123", "foo")
				So(err, ShouldNotBeNil)
			})

			Convey("everdeen returns an error", func() {
				mulan.GetPurchaseOrderPaymentsByIDsReturns(&mulantwirp.GetPurchaseOrderPaymentsByIDsResponse{
					PurchaseOrderPayments: map[string]*mulantwirp.PurchaseOrderPayments{
						"123": {
							PurchaseOrderPayments: []*mulantwirp.PurchaseOrderPayment{
								{
									GrossAmountUsd: 1,
								},
							},
						},
					},
				}, nil)
				everdeen.RefundOrderReturns(nil, errors.New("ERROR"))
				err := db.RefundPurchaseOrder(context.Background(), "123", "foo")
				So(err, ShouldNotBeNil)
			})
		})

		Convey("returns nil if", func() {
			Convey("the purchase order has already been refunded", func() {
				mulan.GetPurchaseOrderPaymentsByIDsReturns(&mulantwirp.GetPurchaseOrderPaymentsByIDsResponse{
					PurchaseOrderPayments: map[string]*mulantwirp.PurchaseOrderPayments{
						"123": {
							PurchaseOrderPayments: []*mulantwirp.PurchaseOrderPayment{
								{
									GrossAmountUsd: 1,
									State:          mulantwirp.OrderState_name[int32(mulantwirp.OrderState_SUCCESS)],
								},
								{
									GrossAmountUsd: -1,
									State:          mulantwirp.OrderState_name[int32(mulantwirp.OrderState_REFUND_APPLIED)],
								},
							},
						},
					},
				}, nil)
				everdeen.RefundOrderReturns(&everdeentwirp.RefundOrderResponse{}, nil)
				err := db.RefundPurchaseOrder(context.Background(), "123", "foo")
				So(err, ShouldBeNil)
				So(everdeen.RefundOrderCallCount(), ShouldEqual, 0)
			})

			Convey("the refund is successful", func() {
				mulan.GetPurchaseOrderPaymentsByIDsReturns(&mulantwirp.GetPurchaseOrderPaymentsByIDsResponse{
					PurchaseOrderPayments: map[string]*mulantwirp.PurchaseOrderPayments{
						"123": {
							PurchaseOrderPayments: []*mulantwirp.PurchaseOrderPayment{
								{
									GrossAmountUsd: 1,
								},
							},
						},
					},
				}, nil)
				everdeen.RefundOrderReturns(&everdeentwirp.RefundOrderResponse{}, nil)
				err := db.RefundPurchaseOrder(context.Background(), "123", "foo")
				So(err, ShouldBeNil)
				So(everdeen.RefundOrderCallCount(), ShouldEqual, 1)
			})
		})
	})
}
