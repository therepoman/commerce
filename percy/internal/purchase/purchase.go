package purchase

import (
	"context"
	"fmt"
	"net/http"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/wrapper"
	"code.justin.tv/foundation/twitchclient"
	everdeenService "code.justin.tv/revenue/everdeen/rpc"
	mulanService "code.justin.tv/revenue/mulan/rpc"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
)

const (
	mulanServiceName    = "mulan"
	everdeenServiceName = "everdeen"
	defaultRetry        = 1
	defaultTimeout      = 1000

	// mulan
	getPurchaseOrderPaymentsByIDsAPI = "getPurchaseOrderPaymentsByIDs"

	// everdeen
	refundOrderAPI = "refundOrder"
)

//go:generate counterfeiter -generate

//counterfeiter:generate  . mulanWrapper
//counterfeiter:generate  . everdeenWrapper

// Wrapped so that we can generate fakes
type mulanWrapper interface {
	mulanService.Mulan
}

type everdeenWrapper interface {
	everdeenService.Everdeen
}

type PurchaseDB struct {
	mulan    mulanWrapper
	sns      percy.SNSPublisher
	everdeen everdeenWrapper
}

func NewPurchaseDB(mulanEndpoint, everdeenEndpoint string, sns percy.SNSPublisher, stats statsd.Statter, socksAddr string) (*PurchaseDB, error) {
	mulan := mulanService.NewMulanProtobufClient(mulanEndpoint, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Stats:          stats,
		StatNamePrefix: mulanServiceName,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(socksAddr),
			wrapper.NewRetryRoundTripWrapper(stats, mulanServiceName, defaultRetry),
			wrapper.NewHystrixRoundTripWrapper(mulanServiceName, defaultTimeout),
		},
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: 200,
		},
	}))

	everdeen := everdeenService.NewEverdeenProtobufClient(everdeenEndpoint, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Stats:          stats,
		StatNamePrefix: everdeenServiceName,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(socksAddr),
			wrapper.NewRetryRoundTripWrapper(stats, everdeenServiceName, defaultRetry),
			wrapper.NewHystrixRoundTripWrapper(everdeenServiceName, defaultTimeout),
		},
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: 200,
		},
	}))

	return &PurchaseDB{
		mulan:    mulan,
		sns:      sns,
		everdeen: everdeen,
	}, nil
}

func (db *PurchaseDB) GetPurchaseOrderPayments(ctx context.Context, purchaseOriginID string) ([]percy.PurchaseOrderPayment, error) {
	if purchaseOriginID == "" {
		return nil, errors.New("empty purchaseOriginID")
	}

	reqCtx := wrapper.GenerateRequestContext(ctx, mulanServiceName, getPurchaseOrderPaymentsByIDsAPI)
	resp, err := db.mulan.GetPurchaseOrderPaymentsByIDs(reqCtx, &mulanService.GetPurchaseOrderPaymentsByIDsRequest{
		Ids: []string{purchaseOriginID},
	})
	if err != nil {
		return nil, err
	}

	if resp == nil {
		return nil, nil
	}

	purchaseOrderPayments, ok := resp.PurchaseOrderPayments[purchaseOriginID]
	if !ok {
		return nil, nil
	}

	if purchaseOrderPayments == nil || len(purchaseOrderPayments.PurchaseOrderPayments) == 0 {
		return nil, fmt.Errorf("FetchPurchaseOrderPaymentFromMulan error: no purchase order payment found for payments origin id %v", purchaseOriginID)
	}

	convertedPurchaseOrderPayments := make([]percy.PurchaseOrderPayment, 0)
	for _, purchaseOrderPayment := range purchaseOrderPayments.PurchaseOrderPayments {
		if purchaseOrderPayment != nil {
			convertedPurchaseOrderPayments = append(convertedPurchaseOrderPayments, percy.PurchaseOrderPayment{
				GrossAmount: purchaseOrderPayment.GetGrossAmountUsd(),
				Fees:        purchaseOrderPayment.GetFeesUsd(),
				Taxes:       purchaseOrderPayment.GetTaxesUsd(),
				// The OrderState enum isn't used in the `GetPurchaseOrderPaymentsByIDsResponse` response, but its
				// string values match.
				IsRefund: purchaseOrderPayment.GetState() == mulanService.OrderState_name[int32(mulanService.OrderState_REFUND_APPLIED)],
			})
		}
	}

	return convertedPurchaseOrderPayments, nil
}

func (db *PurchaseDB) PostCreatorRevenueShareForPurchase(ctx context.Context, args percy.CreatorRevenueShareArgs) error {
	return db.sns.PublishRevenueShareEvent(ctx, args)
}

func (db *PurchaseDB) RefundPurchaseOrder(ctx context.Context, purchaseOriginID string, refundReason string) error {
	payments, err := db.GetPurchaseOrderPayments(ctx, purchaseOriginID)
	if err != nil {
		return errors.Wrap(err, "error getting purchase order payments in RefundPurchaseOrder")
	}

	var grossAmountUSD int64

	for _, payment := range payments {
		// We have ran into situations where the call to Everdeen below returned an error indicating an internal timeout,
		// when in actuality the refund was processed. Upon retry, we were erring due to the gross amount equaling zero. However,
		// once a refund has been processed, this is expected. If we hit this case, we can skip the call to Everdeen,
		// and proceed with the rest of the refund flow.
		if payment.IsRefund {
			return nil
		}

		grossAmountUSD += payment.GrossAmount
	}

	if grossAmountUSD <= 0 {
		return errors.New("encountered purchase order with a non positive gross amount USD")
	}

	reqCtx := wrapper.GenerateRequestContext(ctx, everdeenServiceName, refundOrderAPI)
	_, err = db.everdeen.RefundOrder(reqCtx, &everdeenService.RefundOrderRequest{
		OriginId:     purchaseOriginID,
		RefundReason: refundReason,
		Amount:       grossAmountUSD,
	})

	if err != nil {
		return errors.Wrap(err, "error refunding everdeen order")
	}

	return nil
}
