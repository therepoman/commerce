package percy

//go:generate counterfeiter . WhitelistDB

type WhitelistDB interface {
	HasCelebrations(channelID string) bool
	Close()
}

func (e *celebrationEngine) HasCelebrations(channelID string) bool {
	if channelID == "" {
		return false
	}

	return e.WhitelistDB.HasCelebrations(channelID)
}
