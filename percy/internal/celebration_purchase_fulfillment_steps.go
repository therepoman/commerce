package percy

type (
	CelebrationPurchaseFulfillmentStepName string
	CelebrationRevocationStepName          string
)

const (
	CelebrationPurchaseFulfillmentUserNotice          = CelebrationPurchaseFulfillmentStepName("USER_NOTICE")
	CelebrationPurchaseFulfillmentEmitPubsub          = CelebrationPurchaseFulfillmentStepName("EMIT_PUBSUB")
	CelebrationPurchaseFulfillmentPostRevenue         = CelebrationPurchaseFulfillmentStepName("POST_REVENUE")
	CelebrationPurchaseFulfillmentActivityFeed        = CelebrationPurchaseFulfillmentStepName("ACTIVITY_FEED")
	CelebrationPurchaseFulfillmentRefundPurchase      = CelebrationPurchaseFulfillmentStepName("REFUND_PURCHASE")
	CelebrationPurchaseFulfillmentRefundPurchaseEmail = CelebrationPurchaseFulfillmentStepName("REFUND_PURCHASE_EMAIL")
	CelebrationPurchaseFulfillmentDataScience         = CelebrationPurchaseFulfillmentStepName("DATA_SCIENCE")

	CelebrationRevocationClawbackPayout = CelebrationRevocationStepName("CLAWBACK_PAYOUT")
)
