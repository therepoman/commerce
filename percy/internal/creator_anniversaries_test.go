package percy_test

import (
	"errors"
	"fmt"
	"sort"
	"testing"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	. "github.com/smartystreets/goconvey/convey"
)

func ValidCreatorAnniversaryObject() map[string]percy.CreatorAnniversary {
	validCreatorAnniversaryObject := make(map[string]percy.CreatorAnniversary)
	validCreatorAnniversaryObject["12345678"] = percy.CreatorAnniversary{
		CreatorCategory: percy.PartnerCategory,
		Date:            time.Date(2021, 1, 1, 0, 0, 0, 0, time.UTC),
	}
	validCreatorAnniversaryObject["80808080"] = percy.CreatorAnniversary{
		CreatorCategory: percy.PartnerCategory,
		Date:            time.Date(2019, time.Now().AddDate(0, 1, 0).Month(), time.Now().Day(), 0, 0, 0, 0, time.UTC),
	}
	validCreatorAnniversaryObject["42042042"] = percy.CreatorAnniversary{
		CreatorCategory: percy.PartnerCategory,
		Date:            time.Date(2015, time.Now().Month(), time.Now().Day(), 0, 0, 0, 0, time.UTC),
	}
	validCreatorAnniversaryObject["29292929"] = percy.CreatorAnniversary{
		CreatorCategory: percy.PartnerCategory,
		Date:            time.Date(2018, time.Now().Month(), time.Now().Day(), 0, 0, 0, 0, time.UTC),
	}
	validCreatorAnniversaryObject["07777777"] = percy.CreatorAnniversary{
		CreatorCategory: percy.PartnerCategory,
		Date:            time.Date(2021, time.Now().Month(), time.Now().Day(), 0, 0, 0, 0, time.UTC),
	}
	validCreatorAnniversaryObject["42424242"] = percy.CreatorAnniversary{
		CreatorCategory: percy.PartnerCategory,
		Date:            time.Date(2021, 2, 1, 1, 0, 0, 0, time.UTC),
	}
	validCreatorAnniversaryObject["6789998212"] = percy.CreatorAnniversary{
		CreatorCategory: percy.AffiliateCategory,
		Date:            time.Date(2021, 2, 13, 2, 0, 0, 0, time.UTC),
	}
	validCreatorAnniversaryObject["13571357"] = percy.CreatorAnniversary{
		CreatorCategory: percy.PartnerCategory,
		Date:            time.Date(2010, 2, 13, 23, 59, 59, 0, time.UTC),
	}
	validCreatorAnniversaryObject["53571357"] = percy.CreatorAnniversary{
		CreatorCategory: percy.PartnerCategory,
		Date:            time.Date(2011, 2, 13, 23, 59, 59, 0, time.UTC),
	}
	validCreatorAnniversaryObject["43571359"] = percy.CreatorAnniversary{
		CreatorCategory: percy.PartnerCategory,
		Date:            time.Date(2014, 2, 13, 23, 59, 59, 0, time.UTC),
	}
	validCreatorAnniversaryObject["33571351"] = percy.CreatorAnniversary{
		CreatorCategory: percy.PartnerCategory,
		Date:            time.Date(2019, 2, 13, 23, 59, 59, 0, time.UTC),
	}
	validCreatorAnniversaryObject["23571353"] = percy.CreatorAnniversary{
		CreatorCategory: percy.PartnerCategory,
		Date:            time.Date(2021, 2, 13, 23, 59, 59, 0, time.UTC),
	}
	validCreatorAnniversaryObject["22922922"] = percy.CreatorAnniversary{
		CreatorCategory: percy.AffiliateCategory,
		Date:            time.Date(2013, 2, 29, 0, 0, 0, 0, time.UTC),
	}
	validCreatorAnniversaryObject["22922921"] = percy.CreatorAnniversary{
		CreatorCategory: percy.PartnerCategory,
		Date:            time.Date(2021, 2, 29, 23, 59, 59, 0, time.UTC),
	}
	return validCreatorAnniversaryObject
}

func TestPartnerAnniversariesEngine_GetCreatorAnniversariesByDate(t *testing.T) {
	Convey("Given a CreatorAnniversariesEngine", t, func() {
		anniversariesDB := &internalfakes.FakeCreatorAnniversariesDB{}
		engine := percy.NewCreatorAnniversariesEngine(percy.CreatorAnniversariesEngineConfig{
			CreatorAnniversariesDB: anniversariesDB,
		})

		Convey("should fail if db errors", func() {
			anniversariesDB.GetCreatorAnniversariesReturns(nil, errors.New("anniversariesDB error"))
			res, err := engine.GetCreatorAnniversariesByDate(1, 1)
			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err, ShouldResemble, errors.New("anniversariesDB error"))
		})

		Convey("when db returns an empty creator_anniversaries", func() {
			anniversariesDB.GetCreatorAnniversariesReturns(map[string]percy.CreatorAnniversary{}, nil)
			res, err := engine.GetCreatorAnniversariesByDate(1, 1)

			So(res, ShouldResemble, map[string]percy.CreatorAnniversary{})
			So(err, ShouldBeNil)
		})

		Convey("when db returns an creator_anniversaries object", func() {
			anniversaries := ValidCreatorAnniversaryObject()
			anniversariesDB.GetCreatorAnniversariesReturns(anniversaries, nil)

			Convey("should return all creator_anniversaries on january 1st", func() {
				res, err := engine.GetCreatorAnniversariesByDate(1, 1)

				tmp := make(map[string]percy.CreatorAnniversary)
				for c, a := range anniversaries {
					if int(a.Date.Month()) == 1 && a.Date.Day() == 1 {
						tmp[c] = a
					}
				}

				So(res, ShouldResemble, tmp)
				So(err, ShouldBeNil)
			})

			Convey("should return all creator_anniversaries on february 13th", func() {
				res, err := engine.GetCreatorAnniversariesByDate(2, 13)

				tmp := make(map[string]percy.CreatorAnniversary)
				for c, a := range anniversaries {
					if int(a.Date.Month()) == 2 && a.Date.Day() == 13 {
						tmp[c] = a
					}
				}

				So(res, ShouldResemble, tmp)
				So(err, ShouldBeNil)
			})

			Convey("should return all creator_anniversaries on february 29th", func() {
				res, err := engine.GetCreatorAnniversariesByDate(2, 29)

				tmp := make(map[string]percy.CreatorAnniversary)
				for c, a := range anniversaries {
					if int(a.Date.Month()) == 2 && a.Date.Day() == 29 {
						tmp[c] = a
					}
				}

				So(res, ShouldResemble, tmp)
				So(err, ShouldBeNil)
			})

			Convey("should return empty if there are no creator_anniversaries on a given day", func() {
				res, err := engine.GetCreatorAnniversariesByDate(3, 14)

				tmp := make(map[string]percy.CreatorAnniversary)
				for c, a := range anniversaries {
					if int(a.Date.Month()) == 3 && a.Date.Day() == 14 {
						fmt.Println("On march 14th")
						tmp[c] = a
					}
				}

				So(res, ShouldResemble, tmp)
				So(err, ShouldBeNil)
			})
		})
	})
}

func TestCreatorAnniversariesEngine_GetCreatorAnniversariesByChannelIds(t *testing.T) {
	Convey("Given a CreatorAnniversaries Engine", t, func() {
		anniversariesDB := &internalfakes.FakeCreatorAnniversariesDB{}
		engine := percy.NewCreatorAnniversariesEngine(percy.CreatorAnniversariesEngineConfig{
			CreatorAnniversariesDB: anniversariesDB,
		})
		anniversaries := ValidCreatorAnniversaryObject()
		anniversariesDB.GetCreatorAnniversariesReturns(anniversaries, nil)

		Convey("should return no anniversary if anniversary years is 0", func() {
			testChannelIdsEngineInput := []string{"07777777"}
			resp, err := engine.GetCreatorAnniversariesByChannelIds(testChannelIdsEngineInput)
			sort.Slice(resp, func(i, j int) bool {
				return resp[i].ChannelId < resp[j].ChannelId
			})
			firstElemInResp := resp[0]

			firstExpectedAnniversaryDate := time.Date(2021, time.Now().Month(), time.Now().Day(), 0, 0, 0, 0, time.UTC)

			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(len(resp), ShouldEqual, 1)

			So(firstElemInResp.ChannelId, ShouldEqual, "07777777")
			So(firstElemInResp.IsCreatorAnniversary, ShouldEqual, false)
			So(firstElemInResp.CreatorAnniversaryYears, ShouldEqual, 0)
			So(firstElemInResp.Date, ShouldResemble, firstExpectedAnniversaryDate)
		})

		Convey("should return (resp, nil) when there is one valid anniversary in input array", func() {
			testChannelIdsEngineInput := []string{"42042042", "80808080"}
			resp, err := engine.GetCreatorAnniversariesByChannelIds(testChannelIdsEngineInput)
			sort.Slice(resp, func(i, j int) bool {
				return resp[i].ChannelId < resp[j].ChannelId
			})
			firstElemInResp := resp[0]
			secondElemInResp := resp[1]

			firstExpectedAnniversaryDate := time.Date(2015, time.Now().Month(), time.Now().Day(), 0, 0, 0, 0, time.UTC)
			secondExpectedAnniversaryDate := time.Date(2019, time.Now().AddDate(0, 1, 0).Month(), time.Now().Day(), 0, 0, 0, 0, time.UTC)

			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(len(resp), ShouldEqual, 2)

			So(firstElemInResp.ChannelId, ShouldEqual, "42042042")
			So(firstElemInResp.IsCreatorAnniversary, ShouldEqual, true)
			So(firstElemInResp.CreatorAnniversaryYears, ShouldEqual, 6)
			So(firstElemInResp.Date, ShouldResemble, firstExpectedAnniversaryDate)

			So(secondElemInResp.ChannelId, ShouldEqual, "80808080")
			So(secondElemInResp.IsCreatorAnniversary, ShouldEqual, false)
			So(secondElemInResp.CreatorAnniversaryYears, ShouldEqual, 0)
			So(secondElemInResp.Date, ShouldResemble, secondExpectedAnniversaryDate)
		})

		Convey("should return (resp, nil) when there are multiple valid anniversaries in input array", func() {
			testChannelIdsEngineInput := []string{"29292929", "42042042", "80808080"}
			resp, err := engine.GetCreatorAnniversariesByChannelIds(testChannelIdsEngineInput)
			sort.Slice(resp, func(i, j int) bool {
				return resp[i].ChannelId < resp[j].ChannelId
			})
			firstElemInResp := resp[0]
			secondElemInResp := resp[1]
			thirdElemInResp := resp[2]

			firstExpectedAnniversaryDate := time.Date(2018, time.Now().Month(), time.Now().Day(), 0, 0, 0, 0, time.UTC)
			secondExpectedAnniversaryDate := time.Date(2015, time.Now().Month(), time.Now().Day(), 0, 0, 0, 0, time.UTC)
			thirdExpectedAnniversaryDate := time.Date(2019, time.Now().AddDate(0, 1, 0).Month(), time.Now().Day(), 0, 0, 0, 0, time.UTC)

			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(len(resp), ShouldEqual, 3)

			So(firstElemInResp.ChannelId, ShouldEqual, "29292929")
			So(firstElemInResp.IsCreatorAnniversary, ShouldEqual, true)
			So(firstElemInResp.CreatorAnniversaryYears, ShouldEqual, 3)
			So(firstElemInResp.Date, ShouldResemble, firstExpectedAnniversaryDate)

			So(secondElemInResp.ChannelId, ShouldEqual, "42042042")
			So(secondElemInResp.IsCreatorAnniversary, ShouldEqual, true)
			So(secondElemInResp.CreatorAnniversaryYears, ShouldEqual, 6)
			So(secondElemInResp.Date, ShouldResemble, secondExpectedAnniversaryDate)

			So(thirdElemInResp.ChannelId, ShouldEqual, "80808080")
			So(thirdElemInResp.IsCreatorAnniversary, ShouldEqual, false)
			So(thirdElemInResp.CreatorAnniversaryYears, ShouldEqual, 0)
			So(thirdElemInResp.Date, ShouldResemble, thirdExpectedAnniversaryDate)
		})

		Convey("should return (nil, nil) when input array is empty", func() {
			var testChannelIdsEngineInput []string
			resp, err := engine.GetCreatorAnniversariesByChannelIds(testChannelIdsEngineInput)

			So(resp, ShouldBeNil)
			So(err, ShouldBeNil)
		})

		Convey("should return (resp, nil) if one channelId in input", func() {
			testChannelIdsEngineInput := []string{"22922921"}
			resp, err := engine.GetCreatorAnniversariesByChannelIds(testChannelIdsEngineInput)
			firstElemInResp := resp[0]

			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(len(resp), ShouldEqual, 1)
			So(firstElemInResp.ChannelId, ShouldEqual, "22922921")
			So(firstElemInResp.IsCreatorAnniversary, ShouldEqual, false)
			So(firstElemInResp.CreatorAnniversaryYears, ShouldEqual, 0)
		})

		Convey("append should work when multiple diff channelIds", func() {
			testChannelIdsEngineInput := []string{"22922921", "29292929", "6789998212"}
			resp, err := engine.GetCreatorAnniversariesByChannelIds(testChannelIdsEngineInput)
			sort.Slice(resp, func(i, j int) bool {
				return resp[i].ChannelId < resp[j].ChannelId
			})
			firstElemInResp := resp[0]
			secondElemIResp := resp[1]
			thirdElemInResp := resp[2]

			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(len(resp), ShouldEqual, 3)

			So(firstElemInResp.ChannelId, ShouldEqual, "22922921")
			So(firstElemInResp.IsCreatorAnniversary, ShouldEqual, false)
			So(firstElemInResp.CreatorAnniversaryYears, ShouldEqual, 0)
			So(firstElemInResp.Date, ShouldResemble, time.Date(2021, 2, 29, 23, 59, 59, 0, time.UTC))

			So(secondElemIResp.ChannelId, ShouldEqual, "29292929")
			So(secondElemIResp.IsCreatorAnniversary, ShouldEqual, true)
			So(secondElemIResp.CreatorAnniversaryYears, ShouldEqual, 3)
			So(secondElemIResp.Date, ShouldResemble, time.Date(2018, time.Now().Month(), time.Now().Day(), 0, 0, 0, 0, time.UTC))

			So(thirdElemInResp.ChannelId, ShouldEqual, "6789998212")
			So(thirdElemInResp.IsCreatorAnniversary, ShouldEqual, false)
			So(thirdElemInResp.CreatorAnniversaryYears, ShouldEqual, 0)
			So(thirdElemInResp.Date, ShouldResemble, time.Date(2021, 2, 13, 2, 0, 0, 0, time.UTC))
		})

		Convey("should return (resp, nil) when channel_id doesn't exist in DB", func() {
			testChannelIdsEngineInput := []string{"channelIdNotInAllowlistDB"}
			resp, err := engine.GetCreatorAnniversariesByChannelIds(testChannelIdsEngineInput)
			firstElemInResp := resp[0]

			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(len(resp), ShouldEqual, 1)
			So(firstElemInResp.ChannelId, ShouldEqual, "channelIdNotInAllowlistDB")
			So(firstElemInResp.IsCreatorAnniversary, ShouldEqual, false)
			So(firstElemInResp.CreatorAnniversaryYears, ShouldEqual, 0)
		})

		Convey("should return (resp, nil) when input array contains duplicates", func() {
			testChannelIdsEngineInput := []string{"22922921", "22922921", "29292929"}
			resp, err := engine.GetCreatorAnniversariesByChannelIds(testChannelIdsEngineInput)
			sort.Slice(resp, func(i, j int) bool {
				return resp[i].ChannelId < resp[j].ChannelId
			})
			firstElemInResp := resp[0]
			secondElemIResp := resp[1]

			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(len(resp), ShouldEqual, 2)

			So(firstElemInResp.ChannelId, ShouldEqual, "22922921")
			So(firstElemInResp.IsCreatorAnniversary, ShouldEqual, false)
			So(firstElemInResp.CreatorAnniversaryYears, ShouldEqual, 0)
			So(firstElemInResp.Date, ShouldResemble, time.Date(2021, 2, 29, 23, 59, 59, 0, time.UTC))

			So(secondElemIResp.ChannelId, ShouldEqual, "29292929")
			So(secondElemIResp.IsCreatorAnniversary, ShouldEqual, true)
			So(secondElemIResp.CreatorAnniversaryYears, ShouldEqual, 3)
			So(secondElemIResp.Date, ShouldResemble, time.Date(2018, time.Now().Month(), time.Now().Day(), 0, 0, 0, 0, time.UTC))
		})

		Convey("should return (nil, err) when engine returns error", func() {
			anniversariesDB.GetCreatorAnniversariesReturns(nil, errors.New("something went wrong"))
			var testChannelIdsEngineInput []string
			resp, err := engine.GetCreatorAnniversariesByChannelIds(testChannelIdsEngineInput)
			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
			So(err, ShouldResemble, errors.New("error on calling CreatorAnniversariesDB.GetCreatorAnniversaries()"))
		})
	})
}

func TestIsCreatorAnniversary(t *testing.T) {
	Convey("when there is no creatorAnniversary", t, func() {
		testCurrentTime := time.Date(2021, 1, 22, 20, 0, 0, 0, time.UTC)
		testAnniversaryTime := time.Date(2019, 7, 22, 10, 0, 0, 0, time.UTC)
		actualOutput, err := percy.IsCreatorAnniversary(testCurrentTime, testAnniversaryTime)
		So(actualOutput, ShouldNotBeNil)
		So(err, ShouldBeNil)
		So(actualOutput, ShouldResemble, false)
	})

	Convey("when there is a creatorAnniversary", t, func() {
		testCurrentTime := time.Date(2021, 7, 22, 20, 0, 0, 0, time.UTC)
		testAnniversaryTime := time.Date(2019, 7, 22, 10, 0, 0, 0, time.UTC)
		actualOutput, err := percy.IsCreatorAnniversary(testCurrentTime, testAnniversaryTime)
		So(actualOutput, ShouldNotBeNil)
		So(err, ShouldBeNil)
		So(actualOutput, ShouldResemble, true)
	})
}

func TestCalculateAnniversaryYears(t *testing.T) {
	Convey("when anniversaryYears is 0", t, func() {
		testCurrentTime := time.Date(2021, 7, 22, 20, 0, 0, 0, time.UTC)
		testAnniversaryTime := time.Date(2021, 7, 22, 10, 0, 0, 0, time.UTC)
		actualOutput := percy.CalculateAnniversaryYears(testCurrentTime, testAnniversaryTime)
		So(actualOutput, ShouldNotBeNil)
		So(actualOutput, ShouldResemble, 0)
	})

	Convey("when anniversaryYears is more than zero", t, func() {
		testCurrentTime := time.Date(2021, 7, 22, 20, 0, 0, 0, time.UTC)
		testAnniversaryTime := time.Date(2019, 7, 22, 10, 0, 0, 0, time.UTC)
		actualOutput := percy.CalculateAnniversaryYears(testCurrentTime, testAnniversaryTime)
		So(actualOutput, ShouldNotBeNil)
		So(actualOutput, ShouldResemble, 2)
	})
}
