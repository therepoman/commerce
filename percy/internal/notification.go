package percy

import (
	"context"
)

//go:generate counterfeiter . Notification

type Notification interface {
	PublishCelebrationRefund(ctx context.Context, args PublishCelebrationRefundArgs) error
	PublishCreatorAnniversaryNotification(ctx context.Context, args PublishCreatorAnniversaryNotificationArgs) (string, error)
}

type PublishCelebrationRefundArgs struct {
	BenefactorUserID string
	RecipientUserID  string
	Celebration      string
}

type PublishCreatorAnniversaryNotificationArgs struct {
	ChannelID        string
	CreatorType      string
	NotificationType string
	Years            string
}
