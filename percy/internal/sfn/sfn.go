package sfn

import (
	"context"
	"encoding/json"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/awsmodels"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/aws/aws-sdk-go/service/sfn/sfniface"
)

//go:generate counterfeiter . awsClientWrapper

// Creating our own interface so that we can generate a fake with counterfeiter
type awsClientWrapper interface {
	sfniface.SFNAPI
}

type SFN struct {
	sfnClient                         awsClientWrapper
	celebrationPurchaseFulfillmentARN string
	celebrationPurchaseRevokeARN      string
	pdmsUserDeletionARN               string
	boostOpportunityContributionARN   string
}

func NewSFN(cfg config.Config) *SFN {
	awsCfg := aws.Config{Region: aws.String(cfg.AWSRegion)}
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Config: awsCfg,
	}))
	sfnClient := sfn.New(sess, &awsCfg)

	return &SFN{
		sfnClient:                         sfnClient,
		celebrationPurchaseFulfillmentARN: cfg.StateMachines.CelebrationPurchaseFulfillment,
		celebrationPurchaseRevokeARN:      cfg.StateMachines.CelebrationPurchaseRevoke,
		pdmsUserDeletionARN:               cfg.StateMachines.PDMSUserDeletion,
		boostOpportunityContributionARN:   cfg.StateMachines.BoostOpportunityContribution,
	}
}

func (s *SFN) StartExecutionCelebrationPurchaseFulfillment(ctx context.Context, req percy.CelebrationsPurchaseFulfillmentReq) error {
	return s.startExecution(s.celebrationPurchaseFulfillmentARN, req.OriginID, req)
}

func (s *SFN) StartExecutionCelebrationPurchaseRevoke(ctx context.Context, req percy.CelebrationsPurchaseRevocationReq) error {
	return s.startExecution(s.celebrationPurchaseRevokeARN, req.OriginID, req)
}

func (s *SFN) StartExecutionPDMSUserDeletion(ctx context.Context, executionName string, req percy.StartUserDeletionRequest) error {
	return s.startExecution(s.pdmsUserDeletionARN, executionName, req)
}

func (s *SFN) StartExecutionBoostOpportunityContribution(ctx context.Context, contribution percy.BoostOpportunityContribution) error {
	return s.startExecution(s.boostOpportunityContributionARN, contribution.OriginID, awsmodels.BoostOpportunityFulfillmentInput{
		ChannelID:       contribution.ChannelID,
		UserID:          contribution.UserID,
		OriginID:        contribution.OriginID,
		Units:           contribution.Units,
		UnixTimeSeconds: contribution.Timestamp.Unix(),
	})
}

func (s *SFN) startExecution(stateMachineARN string, executionName string, executionInput interface{}) error {
	inputJSON, err := json.Marshal(executionInput)
	if err != nil {
		return err
	}

	startExecInput := &sfn.StartExecutionInput{
		Input:           pointers.StringP(string(inputJSON)),
		Name:            pointers.StringP(executionName),
		StateMachineArn: pointers.StringP(stateMachineARN),
	}

	_, err = s.sfnClient.StartExecution(startExecInput)
	if err != nil {
		return err
	}

	return nil
}
