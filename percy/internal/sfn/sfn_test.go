package sfn

import (
	"errors"
	"testing"

	"code.justin.tv/commerce/percy/internal/sfn/sfnfakes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSFN_startExecution(t *testing.T) {
	Convey("given a SFN client", t, func() {
		sfnFake := &sfnfakes.FakeAwsClientWrapper{}
		sfnWrapperClient := SFN{
			sfnClient: sfnFake,
		}

		Convey("we should error when StartExecution errors", func() {
			sfnFake.StartExecutionReturns(nil, errors.New("error"))
			err := sfnWrapperClient.startExecution("foo", "bar", struct{}{})
			So(err, ShouldNotBeNil)
		})

		Convey("we should return nil when StartExecution succeeds", func() {
			sfnFake.StartExecutionReturns(nil, nil)
			err := sfnWrapperClient.startExecution("foo", "bar", struct{}{})
			So(err, ShouldBeNil)

			startExecutionInput := sfnFake.StartExecutionArgsForCall(0)

			So(*startExecutionInput.Input, ShouldEqual, "{}")
			So(*startExecutionInput.Name, ShouldEqual, "bar")
			So(*startExecutionInput.StateMachineArn, ShouldEqual, "foo")
		})
	})
}
