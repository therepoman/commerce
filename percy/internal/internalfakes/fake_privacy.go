// Code generated by counterfeiter. DO NOT EDIT.
package internalfakes

import (
	"context"
	"sync"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
)

type FakePrivacy struct {
	PromiseDeletionsStub        func(context.Context, []string, time.Time) error
	promiseDeletionsMutex       sync.RWMutex
	promiseDeletionsArgsForCall []struct {
		arg1 context.Context
		arg2 []string
		arg3 time.Time
	}
	promiseDeletionsReturns struct {
		result1 error
	}
	promiseDeletionsReturnsOnCall map[int]struct {
		result1 error
	}
	ReportDeletionsStub        func(context.Context, []string, time.Time) error
	reportDeletionsMutex       sync.RWMutex
	reportDeletionsArgsForCall []struct {
		arg1 context.Context
		arg2 []string
		arg3 time.Time
	}
	reportDeletionsReturns struct {
		result1 error
	}
	reportDeletionsReturnsOnCall map[int]struct {
		result1 error
	}
	invocations      map[string][][]interface{}
	invocationsMutex sync.RWMutex
}

func (fake *FakePrivacy) PromiseDeletions(arg1 context.Context, arg2 []string, arg3 time.Time) error {
	var arg2Copy []string
	if arg2 != nil {
		arg2Copy = make([]string, len(arg2))
		copy(arg2Copy, arg2)
	}
	fake.promiseDeletionsMutex.Lock()
	ret, specificReturn := fake.promiseDeletionsReturnsOnCall[len(fake.promiseDeletionsArgsForCall)]
	fake.promiseDeletionsArgsForCall = append(fake.promiseDeletionsArgsForCall, struct {
		arg1 context.Context
		arg2 []string
		arg3 time.Time
	}{arg1, arg2Copy, arg3})
	stub := fake.PromiseDeletionsStub
	fakeReturns := fake.promiseDeletionsReturns
	fake.recordInvocation("PromiseDeletions", []interface{}{arg1, arg2Copy, arg3})
	fake.promiseDeletionsMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2, arg3)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakePrivacy) PromiseDeletionsCallCount() int {
	fake.promiseDeletionsMutex.RLock()
	defer fake.promiseDeletionsMutex.RUnlock()
	return len(fake.promiseDeletionsArgsForCall)
}

func (fake *FakePrivacy) PromiseDeletionsCalls(stub func(context.Context, []string, time.Time) error) {
	fake.promiseDeletionsMutex.Lock()
	defer fake.promiseDeletionsMutex.Unlock()
	fake.PromiseDeletionsStub = stub
}

func (fake *FakePrivacy) PromiseDeletionsArgsForCall(i int) (context.Context, []string, time.Time) {
	fake.promiseDeletionsMutex.RLock()
	defer fake.promiseDeletionsMutex.RUnlock()
	argsForCall := fake.promiseDeletionsArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2, argsForCall.arg3
}

func (fake *FakePrivacy) PromiseDeletionsReturns(result1 error) {
	fake.promiseDeletionsMutex.Lock()
	defer fake.promiseDeletionsMutex.Unlock()
	fake.PromiseDeletionsStub = nil
	fake.promiseDeletionsReturns = struct {
		result1 error
	}{result1}
}

func (fake *FakePrivacy) PromiseDeletionsReturnsOnCall(i int, result1 error) {
	fake.promiseDeletionsMutex.Lock()
	defer fake.promiseDeletionsMutex.Unlock()
	fake.PromiseDeletionsStub = nil
	if fake.promiseDeletionsReturnsOnCall == nil {
		fake.promiseDeletionsReturnsOnCall = make(map[int]struct {
			result1 error
		})
	}
	fake.promiseDeletionsReturnsOnCall[i] = struct {
		result1 error
	}{result1}
}

func (fake *FakePrivacy) ReportDeletions(arg1 context.Context, arg2 []string, arg3 time.Time) error {
	var arg2Copy []string
	if arg2 != nil {
		arg2Copy = make([]string, len(arg2))
		copy(arg2Copy, arg2)
	}
	fake.reportDeletionsMutex.Lock()
	ret, specificReturn := fake.reportDeletionsReturnsOnCall[len(fake.reportDeletionsArgsForCall)]
	fake.reportDeletionsArgsForCall = append(fake.reportDeletionsArgsForCall, struct {
		arg1 context.Context
		arg2 []string
		arg3 time.Time
	}{arg1, arg2Copy, arg3})
	stub := fake.ReportDeletionsStub
	fakeReturns := fake.reportDeletionsReturns
	fake.recordInvocation("ReportDeletions", []interface{}{arg1, arg2Copy, arg3})
	fake.reportDeletionsMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2, arg3)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakePrivacy) ReportDeletionsCallCount() int {
	fake.reportDeletionsMutex.RLock()
	defer fake.reportDeletionsMutex.RUnlock()
	return len(fake.reportDeletionsArgsForCall)
}

func (fake *FakePrivacy) ReportDeletionsCalls(stub func(context.Context, []string, time.Time) error) {
	fake.reportDeletionsMutex.Lock()
	defer fake.reportDeletionsMutex.Unlock()
	fake.ReportDeletionsStub = stub
}

func (fake *FakePrivacy) ReportDeletionsArgsForCall(i int) (context.Context, []string, time.Time) {
	fake.reportDeletionsMutex.RLock()
	defer fake.reportDeletionsMutex.RUnlock()
	argsForCall := fake.reportDeletionsArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2, argsForCall.arg3
}

func (fake *FakePrivacy) ReportDeletionsReturns(result1 error) {
	fake.reportDeletionsMutex.Lock()
	defer fake.reportDeletionsMutex.Unlock()
	fake.ReportDeletionsStub = nil
	fake.reportDeletionsReturns = struct {
		result1 error
	}{result1}
}

func (fake *FakePrivacy) ReportDeletionsReturnsOnCall(i int, result1 error) {
	fake.reportDeletionsMutex.Lock()
	defer fake.reportDeletionsMutex.Unlock()
	fake.ReportDeletionsStub = nil
	if fake.reportDeletionsReturnsOnCall == nil {
		fake.reportDeletionsReturnsOnCall = make(map[int]struct {
			result1 error
		})
	}
	fake.reportDeletionsReturnsOnCall[i] = struct {
		result1 error
	}{result1}
}

func (fake *FakePrivacy) Invocations() map[string][][]interface{} {
	fake.invocationsMutex.RLock()
	defer fake.invocationsMutex.RUnlock()
	fake.promiseDeletionsMutex.RLock()
	defer fake.promiseDeletionsMutex.RUnlock()
	fake.reportDeletionsMutex.RLock()
	defer fake.reportDeletionsMutex.RUnlock()
	copiedInvocations := map[string][][]interface{}{}
	for key, value := range fake.invocations {
		copiedInvocations[key] = value
	}
	return copiedInvocations
}

func (fake *FakePrivacy) recordInvocation(key string, args []interface{}) {
	fake.invocationsMutex.Lock()
	defer fake.invocationsMutex.Unlock()
	if fake.invocations == nil {
		fake.invocations = map[string][][]interface{}{}
	}
	if fake.invocations[key] == nil {
		fake.invocations[key] = [][]interface{}{}
	}
	fake.invocations[key] = append(fake.invocations[key], args)
}

var _ percy.Privacy = new(FakePrivacy)
