// Code generated by counterfeiter. DO NOT EDIT.
package internalfakes

import (
	"context"
	"sync"

	percy "code.justin.tv/commerce/percy/internal"
)

type FakeLeaderboard struct {
	BatchPublishStub        func(context.Context, string, string, ...percy.Participation) error
	batchPublishMutex       sync.RWMutex
	batchPublishArgsForCall []struct {
		arg1 context.Context
		arg2 string
		arg3 string
		arg4 []percy.Participation
	}
	batchPublishReturns struct {
		result1 error
	}
	batchPublishReturnsOnCall map[int]struct {
		result1 error
	}
	ModerateStub        func(context.Context, percy.LeaderboardID, string) error
	moderateMutex       sync.RWMutex
	moderateArgsForCall []struct {
		arg1 context.Context
		arg2 percy.LeaderboardID
		arg3 string
	}
	moderateReturns struct {
		result1 error
	}
	moderateReturnsOnCall map[int]struct {
		result1 error
	}
	PublishStub        func(context.Context, percy.LeaderboardID, percy.Participation) error
	publishMutex       sync.RWMutex
	publishArgsForCall []struct {
		arg1 context.Context
		arg2 percy.LeaderboardID
		arg3 percy.Participation
	}
	publishReturns struct {
		result1 error
	}
	publishReturnsOnCall map[int]struct {
		result1 error
	}
	TopStub        func(context.Context, percy.LeaderboardID) (*percy.User, error)
	topMutex       sync.RWMutex
	topArgsForCall []struct {
		arg1 context.Context
		arg2 percy.LeaderboardID
	}
	topReturns struct {
		result1 *percy.User
		result2 error
	}
	topReturnsOnCall map[int]struct {
		result1 *percy.User
		result2 error
	}
	UnmoderateStub        func(context.Context, percy.LeaderboardID, string) error
	unmoderateMutex       sync.RWMutex
	unmoderateArgsForCall []struct {
		arg1 context.Context
		arg2 percy.LeaderboardID
		arg3 string
	}
	unmoderateReturns struct {
		result1 error
	}
	unmoderateReturnsOnCall map[int]struct {
		result1 error
	}
	invocations      map[string][][]interface{}
	invocationsMutex sync.RWMutex
}

func (fake *FakeLeaderboard) BatchPublish(arg1 context.Context, arg2 string, arg3 string, arg4 ...percy.Participation) error {
	fake.batchPublishMutex.Lock()
	ret, specificReturn := fake.batchPublishReturnsOnCall[len(fake.batchPublishArgsForCall)]
	fake.batchPublishArgsForCall = append(fake.batchPublishArgsForCall, struct {
		arg1 context.Context
		arg2 string
		arg3 string
		arg4 []percy.Participation
	}{arg1, arg2, arg3, arg4})
	stub := fake.BatchPublishStub
	fakeReturns := fake.batchPublishReturns
	fake.recordInvocation("BatchPublish", []interface{}{arg1, arg2, arg3, arg4})
	fake.batchPublishMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2, arg3, arg4...)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakeLeaderboard) BatchPublishCallCount() int {
	fake.batchPublishMutex.RLock()
	defer fake.batchPublishMutex.RUnlock()
	return len(fake.batchPublishArgsForCall)
}

func (fake *FakeLeaderboard) BatchPublishCalls(stub func(context.Context, string, string, ...percy.Participation) error) {
	fake.batchPublishMutex.Lock()
	defer fake.batchPublishMutex.Unlock()
	fake.BatchPublishStub = stub
}

func (fake *FakeLeaderboard) BatchPublishArgsForCall(i int) (context.Context, string, string, []percy.Participation) {
	fake.batchPublishMutex.RLock()
	defer fake.batchPublishMutex.RUnlock()
	argsForCall := fake.batchPublishArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2, argsForCall.arg3, argsForCall.arg4
}

func (fake *FakeLeaderboard) BatchPublishReturns(result1 error) {
	fake.batchPublishMutex.Lock()
	defer fake.batchPublishMutex.Unlock()
	fake.BatchPublishStub = nil
	fake.batchPublishReturns = struct {
		result1 error
	}{result1}
}

func (fake *FakeLeaderboard) BatchPublishReturnsOnCall(i int, result1 error) {
	fake.batchPublishMutex.Lock()
	defer fake.batchPublishMutex.Unlock()
	fake.BatchPublishStub = nil
	if fake.batchPublishReturnsOnCall == nil {
		fake.batchPublishReturnsOnCall = make(map[int]struct {
			result1 error
		})
	}
	fake.batchPublishReturnsOnCall[i] = struct {
		result1 error
	}{result1}
}

func (fake *FakeLeaderboard) Moderate(arg1 context.Context, arg2 percy.LeaderboardID, arg3 string) error {
	fake.moderateMutex.Lock()
	ret, specificReturn := fake.moderateReturnsOnCall[len(fake.moderateArgsForCall)]
	fake.moderateArgsForCall = append(fake.moderateArgsForCall, struct {
		arg1 context.Context
		arg2 percy.LeaderboardID
		arg3 string
	}{arg1, arg2, arg3})
	stub := fake.ModerateStub
	fakeReturns := fake.moderateReturns
	fake.recordInvocation("Moderate", []interface{}{arg1, arg2, arg3})
	fake.moderateMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2, arg3)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakeLeaderboard) ModerateCallCount() int {
	fake.moderateMutex.RLock()
	defer fake.moderateMutex.RUnlock()
	return len(fake.moderateArgsForCall)
}

func (fake *FakeLeaderboard) ModerateCalls(stub func(context.Context, percy.LeaderboardID, string) error) {
	fake.moderateMutex.Lock()
	defer fake.moderateMutex.Unlock()
	fake.ModerateStub = stub
}

func (fake *FakeLeaderboard) ModerateArgsForCall(i int) (context.Context, percy.LeaderboardID, string) {
	fake.moderateMutex.RLock()
	defer fake.moderateMutex.RUnlock()
	argsForCall := fake.moderateArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2, argsForCall.arg3
}

func (fake *FakeLeaderboard) ModerateReturns(result1 error) {
	fake.moderateMutex.Lock()
	defer fake.moderateMutex.Unlock()
	fake.ModerateStub = nil
	fake.moderateReturns = struct {
		result1 error
	}{result1}
}

func (fake *FakeLeaderboard) ModerateReturnsOnCall(i int, result1 error) {
	fake.moderateMutex.Lock()
	defer fake.moderateMutex.Unlock()
	fake.ModerateStub = nil
	if fake.moderateReturnsOnCall == nil {
		fake.moderateReturnsOnCall = make(map[int]struct {
			result1 error
		})
	}
	fake.moderateReturnsOnCall[i] = struct {
		result1 error
	}{result1}
}

func (fake *FakeLeaderboard) Publish(arg1 context.Context, arg2 percy.LeaderboardID, arg3 percy.Participation) error {
	fake.publishMutex.Lock()
	ret, specificReturn := fake.publishReturnsOnCall[len(fake.publishArgsForCall)]
	fake.publishArgsForCall = append(fake.publishArgsForCall, struct {
		arg1 context.Context
		arg2 percy.LeaderboardID
		arg3 percy.Participation
	}{arg1, arg2, arg3})
	stub := fake.PublishStub
	fakeReturns := fake.publishReturns
	fake.recordInvocation("Publish", []interface{}{arg1, arg2, arg3})
	fake.publishMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2, arg3)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakeLeaderboard) PublishCallCount() int {
	fake.publishMutex.RLock()
	defer fake.publishMutex.RUnlock()
	return len(fake.publishArgsForCall)
}

func (fake *FakeLeaderboard) PublishCalls(stub func(context.Context, percy.LeaderboardID, percy.Participation) error) {
	fake.publishMutex.Lock()
	defer fake.publishMutex.Unlock()
	fake.PublishStub = stub
}

func (fake *FakeLeaderboard) PublishArgsForCall(i int) (context.Context, percy.LeaderboardID, percy.Participation) {
	fake.publishMutex.RLock()
	defer fake.publishMutex.RUnlock()
	argsForCall := fake.publishArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2, argsForCall.arg3
}

func (fake *FakeLeaderboard) PublishReturns(result1 error) {
	fake.publishMutex.Lock()
	defer fake.publishMutex.Unlock()
	fake.PublishStub = nil
	fake.publishReturns = struct {
		result1 error
	}{result1}
}

func (fake *FakeLeaderboard) PublishReturnsOnCall(i int, result1 error) {
	fake.publishMutex.Lock()
	defer fake.publishMutex.Unlock()
	fake.PublishStub = nil
	if fake.publishReturnsOnCall == nil {
		fake.publishReturnsOnCall = make(map[int]struct {
			result1 error
		})
	}
	fake.publishReturnsOnCall[i] = struct {
		result1 error
	}{result1}
}

func (fake *FakeLeaderboard) Top(arg1 context.Context, arg2 percy.LeaderboardID) (*percy.User, error) {
	fake.topMutex.Lock()
	ret, specificReturn := fake.topReturnsOnCall[len(fake.topArgsForCall)]
	fake.topArgsForCall = append(fake.topArgsForCall, struct {
		arg1 context.Context
		arg2 percy.LeaderboardID
	}{arg1, arg2})
	stub := fake.TopStub
	fakeReturns := fake.topReturns
	fake.recordInvocation("Top", []interface{}{arg1, arg2})
	fake.topMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2)
	}
	if specificReturn {
		return ret.result1, ret.result2
	}
	return fakeReturns.result1, fakeReturns.result2
}

func (fake *FakeLeaderboard) TopCallCount() int {
	fake.topMutex.RLock()
	defer fake.topMutex.RUnlock()
	return len(fake.topArgsForCall)
}

func (fake *FakeLeaderboard) TopCalls(stub func(context.Context, percy.LeaderboardID) (*percy.User, error)) {
	fake.topMutex.Lock()
	defer fake.topMutex.Unlock()
	fake.TopStub = stub
}

func (fake *FakeLeaderboard) TopArgsForCall(i int) (context.Context, percy.LeaderboardID) {
	fake.topMutex.RLock()
	defer fake.topMutex.RUnlock()
	argsForCall := fake.topArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2
}

func (fake *FakeLeaderboard) TopReturns(result1 *percy.User, result2 error) {
	fake.topMutex.Lock()
	defer fake.topMutex.Unlock()
	fake.TopStub = nil
	fake.topReturns = struct {
		result1 *percy.User
		result2 error
	}{result1, result2}
}

func (fake *FakeLeaderboard) TopReturnsOnCall(i int, result1 *percy.User, result2 error) {
	fake.topMutex.Lock()
	defer fake.topMutex.Unlock()
	fake.TopStub = nil
	if fake.topReturnsOnCall == nil {
		fake.topReturnsOnCall = make(map[int]struct {
			result1 *percy.User
			result2 error
		})
	}
	fake.topReturnsOnCall[i] = struct {
		result1 *percy.User
		result2 error
	}{result1, result2}
}

func (fake *FakeLeaderboard) Unmoderate(arg1 context.Context, arg2 percy.LeaderboardID, arg3 string) error {
	fake.unmoderateMutex.Lock()
	ret, specificReturn := fake.unmoderateReturnsOnCall[len(fake.unmoderateArgsForCall)]
	fake.unmoderateArgsForCall = append(fake.unmoderateArgsForCall, struct {
		arg1 context.Context
		arg2 percy.LeaderboardID
		arg3 string
	}{arg1, arg2, arg3})
	stub := fake.UnmoderateStub
	fakeReturns := fake.unmoderateReturns
	fake.recordInvocation("Unmoderate", []interface{}{arg1, arg2, arg3})
	fake.unmoderateMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2, arg3)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakeLeaderboard) UnmoderateCallCount() int {
	fake.unmoderateMutex.RLock()
	defer fake.unmoderateMutex.RUnlock()
	return len(fake.unmoderateArgsForCall)
}

func (fake *FakeLeaderboard) UnmoderateCalls(stub func(context.Context, percy.LeaderboardID, string) error) {
	fake.unmoderateMutex.Lock()
	defer fake.unmoderateMutex.Unlock()
	fake.UnmoderateStub = stub
}

func (fake *FakeLeaderboard) UnmoderateArgsForCall(i int) (context.Context, percy.LeaderboardID, string) {
	fake.unmoderateMutex.RLock()
	defer fake.unmoderateMutex.RUnlock()
	argsForCall := fake.unmoderateArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2, argsForCall.arg3
}

func (fake *FakeLeaderboard) UnmoderateReturns(result1 error) {
	fake.unmoderateMutex.Lock()
	defer fake.unmoderateMutex.Unlock()
	fake.UnmoderateStub = nil
	fake.unmoderateReturns = struct {
		result1 error
	}{result1}
}

func (fake *FakeLeaderboard) UnmoderateReturnsOnCall(i int, result1 error) {
	fake.unmoderateMutex.Lock()
	defer fake.unmoderateMutex.Unlock()
	fake.UnmoderateStub = nil
	if fake.unmoderateReturnsOnCall == nil {
		fake.unmoderateReturnsOnCall = make(map[int]struct {
			result1 error
		})
	}
	fake.unmoderateReturnsOnCall[i] = struct {
		result1 error
	}{result1}
}

func (fake *FakeLeaderboard) Invocations() map[string][][]interface{} {
	fake.invocationsMutex.RLock()
	defer fake.invocationsMutex.RUnlock()
	fake.batchPublishMutex.RLock()
	defer fake.batchPublishMutex.RUnlock()
	fake.moderateMutex.RLock()
	defer fake.moderateMutex.RUnlock()
	fake.publishMutex.RLock()
	defer fake.publishMutex.RUnlock()
	fake.topMutex.RLock()
	defer fake.topMutex.RUnlock()
	fake.unmoderateMutex.RLock()
	defer fake.unmoderateMutex.RUnlock()
	copiedInvocations := map[string][][]interface{}{}
	for key, value := range fake.invocations {
		copiedInvocations[key] = value
	}
	return copiedInvocations
}

func (fake *FakeLeaderboard) recordInvocation(key string, args []interface{}) {
	fake.invocationsMutex.Lock()
	defer fake.invocationsMutex.Unlock()
	if fake.invocations == nil {
		fake.invocations = map[string][][]interface{}{}
	}
	if fake.invocations[key] == nil {
		fake.invocations[key] = [][]interface{}{}
	}
	fake.invocations[key] = append(fake.invocations[key], args)
}

var _ percy.Leaderboard = new(FakeLeaderboard)
