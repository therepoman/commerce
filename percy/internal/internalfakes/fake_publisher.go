// Code generated by counterfeiter. DO NOT EDIT.
package internalfakes

import (
	"context"
	"sync"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
)

type FakePublisher struct {
	ConductorUpdateStub        func(context.Context, string, percy.Conductor) error
	conductorUpdateMutex       sync.RWMutex
	conductorUpdateArgsForCall []struct {
		arg1 context.Context
		arg2 string
		arg3 percy.Conductor
	}
	conductorUpdateReturns struct {
		result1 error
	}
	conductorUpdateReturnsOnCall map[int]struct {
		result1 error
	}
	CooldownExpiredStub        func(context.Context, string) error
	cooldownExpiredMutex       sync.RWMutex
	cooldownExpiredArgsForCall []struct {
		arg1 context.Context
		arg2 string
	}
	cooldownExpiredReturns struct {
		result1 error
	}
	cooldownExpiredReturnsOnCall map[int]struct {
		result1 error
	}
	EndHypeTrainStub        func(context.Context, percy.HypeTrain, time.Time, percy.EndingReason) error
	endHypeTrainMutex       sync.RWMutex
	endHypeTrainArgsForCall []struct {
		arg1 context.Context
		arg2 percy.HypeTrain
		arg3 time.Time
		arg4 percy.EndingReason
	}
	endHypeTrainReturns struct {
		result1 error
	}
	endHypeTrainReturnsOnCall map[int]struct {
		result1 error
	}
	GetNumberOfPubsubListenersStub        func(string, string) (int, error)
	getNumberOfPubsubListenersMutex       sync.RWMutex
	getNumberOfPubsubListenersArgsForCall []struct {
		arg1 string
		arg2 string
	}
	getNumberOfPubsubListenersReturns struct {
		result1 int
		result2 error
	}
	getNumberOfPubsubListenersReturnsOnCall map[int]struct {
		result1 int
		result2 error
	}
	HypeTrainApproachingStub        func(context.Context, string, int64, map[int]time.Duration, []percy.Reward, string, []string) error
	hypeTrainApproachingMutex       sync.RWMutex
	hypeTrainApproachingArgsForCall []struct {
		arg1 context.Context
		arg2 string
		arg3 int64
		arg4 map[int]time.Duration
		arg5 []percy.Reward
		arg6 string
		arg7 []string
	}
	hypeTrainApproachingReturns struct {
		result1 error
	}
	hypeTrainApproachingReturnsOnCall map[int]struct {
		result1 error
	}
	LevelUpHypeTrainStub        func(context.Context, string, time.Time, percy.Progress) error
	levelUpHypeTrainMutex       sync.RWMutex
	levelUpHypeTrainArgsForCall []struct {
		arg1 context.Context
		arg2 string
		arg3 time.Time
		arg4 percy.Progress
	}
	levelUpHypeTrainReturns struct {
		result1 error
	}
	levelUpHypeTrainReturnsOnCall map[int]struct {
		result1 error
	}
	ParticipantRewardsStub        func(context.Context, string, percy.EntitledParticipants) error
	participantRewardsMutex       sync.RWMutex
	participantRewardsArgsForCall []struct {
		arg1 context.Context
		arg2 string
		arg3 percy.EntitledParticipants
	}
	participantRewardsReturns struct {
		result1 error
	}
	participantRewardsReturnsOnCall map[int]struct {
		result1 error
	}
	PostHypeTrainProgressionStub        func(context.Context, percy.Participation, percy.Progress) error
	postHypeTrainProgressionMutex       sync.RWMutex
	postHypeTrainProgressionArgsForCall []struct {
		arg1 context.Context
		arg2 percy.Participation
		arg3 percy.Progress
	}
	postHypeTrainProgressionReturns struct {
		result1 error
	}
	postHypeTrainProgressionReturnsOnCall map[int]struct {
		result1 error
	}
	StartCelebrationStub        func(context.Context, string, percy.User, int64, percy.Celebration) error
	startCelebrationMutex       sync.RWMutex
	startCelebrationArgsForCall []struct {
		arg1 context.Context
		arg2 string
		arg3 percy.User
		arg4 int64
		arg5 percy.Celebration
	}
	startCelebrationReturns struct {
		result1 error
	}
	startCelebrationReturnsOnCall map[int]struct {
		result1 error
	}
	StartHypeTrainStub        func(context.Context, percy.HypeTrain, ...percy.Participation) error
	startHypeTrainMutex       sync.RWMutex
	startHypeTrainArgsForCall []struct {
		arg1 context.Context
		arg2 percy.HypeTrain
		arg3 []percy.Participation
	}
	startHypeTrainReturns struct {
		result1 error
	}
	startHypeTrainReturnsOnCall map[int]struct {
		result1 error
	}
	invocations      map[string][][]interface{}
	invocationsMutex sync.RWMutex
}

func (fake *FakePublisher) ConductorUpdate(arg1 context.Context, arg2 string, arg3 percy.Conductor) error {
	fake.conductorUpdateMutex.Lock()
	ret, specificReturn := fake.conductorUpdateReturnsOnCall[len(fake.conductorUpdateArgsForCall)]
	fake.conductorUpdateArgsForCall = append(fake.conductorUpdateArgsForCall, struct {
		arg1 context.Context
		arg2 string
		arg3 percy.Conductor
	}{arg1, arg2, arg3})
	stub := fake.ConductorUpdateStub
	fakeReturns := fake.conductorUpdateReturns
	fake.recordInvocation("ConductorUpdate", []interface{}{arg1, arg2, arg3})
	fake.conductorUpdateMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2, arg3)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakePublisher) ConductorUpdateCallCount() int {
	fake.conductorUpdateMutex.RLock()
	defer fake.conductorUpdateMutex.RUnlock()
	return len(fake.conductorUpdateArgsForCall)
}

func (fake *FakePublisher) ConductorUpdateCalls(stub func(context.Context, string, percy.Conductor) error) {
	fake.conductorUpdateMutex.Lock()
	defer fake.conductorUpdateMutex.Unlock()
	fake.ConductorUpdateStub = stub
}

func (fake *FakePublisher) ConductorUpdateArgsForCall(i int) (context.Context, string, percy.Conductor) {
	fake.conductorUpdateMutex.RLock()
	defer fake.conductorUpdateMutex.RUnlock()
	argsForCall := fake.conductorUpdateArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2, argsForCall.arg3
}

func (fake *FakePublisher) ConductorUpdateReturns(result1 error) {
	fake.conductorUpdateMutex.Lock()
	defer fake.conductorUpdateMutex.Unlock()
	fake.ConductorUpdateStub = nil
	fake.conductorUpdateReturns = struct {
		result1 error
	}{result1}
}

func (fake *FakePublisher) ConductorUpdateReturnsOnCall(i int, result1 error) {
	fake.conductorUpdateMutex.Lock()
	defer fake.conductorUpdateMutex.Unlock()
	fake.ConductorUpdateStub = nil
	if fake.conductorUpdateReturnsOnCall == nil {
		fake.conductorUpdateReturnsOnCall = make(map[int]struct {
			result1 error
		})
	}
	fake.conductorUpdateReturnsOnCall[i] = struct {
		result1 error
	}{result1}
}

func (fake *FakePublisher) CooldownExpired(arg1 context.Context, arg2 string) error {
	fake.cooldownExpiredMutex.Lock()
	ret, specificReturn := fake.cooldownExpiredReturnsOnCall[len(fake.cooldownExpiredArgsForCall)]
	fake.cooldownExpiredArgsForCall = append(fake.cooldownExpiredArgsForCall, struct {
		arg1 context.Context
		arg2 string
	}{arg1, arg2})
	stub := fake.CooldownExpiredStub
	fakeReturns := fake.cooldownExpiredReturns
	fake.recordInvocation("CooldownExpired", []interface{}{arg1, arg2})
	fake.cooldownExpiredMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakePublisher) CooldownExpiredCallCount() int {
	fake.cooldownExpiredMutex.RLock()
	defer fake.cooldownExpiredMutex.RUnlock()
	return len(fake.cooldownExpiredArgsForCall)
}

func (fake *FakePublisher) CooldownExpiredCalls(stub func(context.Context, string) error) {
	fake.cooldownExpiredMutex.Lock()
	defer fake.cooldownExpiredMutex.Unlock()
	fake.CooldownExpiredStub = stub
}

func (fake *FakePublisher) CooldownExpiredArgsForCall(i int) (context.Context, string) {
	fake.cooldownExpiredMutex.RLock()
	defer fake.cooldownExpiredMutex.RUnlock()
	argsForCall := fake.cooldownExpiredArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2
}

func (fake *FakePublisher) CooldownExpiredReturns(result1 error) {
	fake.cooldownExpiredMutex.Lock()
	defer fake.cooldownExpiredMutex.Unlock()
	fake.CooldownExpiredStub = nil
	fake.cooldownExpiredReturns = struct {
		result1 error
	}{result1}
}

func (fake *FakePublisher) CooldownExpiredReturnsOnCall(i int, result1 error) {
	fake.cooldownExpiredMutex.Lock()
	defer fake.cooldownExpiredMutex.Unlock()
	fake.CooldownExpiredStub = nil
	if fake.cooldownExpiredReturnsOnCall == nil {
		fake.cooldownExpiredReturnsOnCall = make(map[int]struct {
			result1 error
		})
	}
	fake.cooldownExpiredReturnsOnCall[i] = struct {
		result1 error
	}{result1}
}

func (fake *FakePublisher) EndHypeTrain(arg1 context.Context, arg2 percy.HypeTrain, arg3 time.Time, arg4 percy.EndingReason) error {
	fake.endHypeTrainMutex.Lock()
	ret, specificReturn := fake.endHypeTrainReturnsOnCall[len(fake.endHypeTrainArgsForCall)]
	fake.endHypeTrainArgsForCall = append(fake.endHypeTrainArgsForCall, struct {
		arg1 context.Context
		arg2 percy.HypeTrain
		arg3 time.Time
		arg4 percy.EndingReason
	}{arg1, arg2, arg3, arg4})
	stub := fake.EndHypeTrainStub
	fakeReturns := fake.endHypeTrainReturns
	fake.recordInvocation("EndHypeTrain", []interface{}{arg1, arg2, arg3, arg4})
	fake.endHypeTrainMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2, arg3, arg4)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakePublisher) EndHypeTrainCallCount() int {
	fake.endHypeTrainMutex.RLock()
	defer fake.endHypeTrainMutex.RUnlock()
	return len(fake.endHypeTrainArgsForCall)
}

func (fake *FakePublisher) EndHypeTrainCalls(stub func(context.Context, percy.HypeTrain, time.Time, percy.EndingReason) error) {
	fake.endHypeTrainMutex.Lock()
	defer fake.endHypeTrainMutex.Unlock()
	fake.EndHypeTrainStub = stub
}

func (fake *FakePublisher) EndHypeTrainArgsForCall(i int) (context.Context, percy.HypeTrain, time.Time, percy.EndingReason) {
	fake.endHypeTrainMutex.RLock()
	defer fake.endHypeTrainMutex.RUnlock()
	argsForCall := fake.endHypeTrainArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2, argsForCall.arg3, argsForCall.arg4
}

func (fake *FakePublisher) EndHypeTrainReturns(result1 error) {
	fake.endHypeTrainMutex.Lock()
	defer fake.endHypeTrainMutex.Unlock()
	fake.EndHypeTrainStub = nil
	fake.endHypeTrainReturns = struct {
		result1 error
	}{result1}
}

func (fake *FakePublisher) EndHypeTrainReturnsOnCall(i int, result1 error) {
	fake.endHypeTrainMutex.Lock()
	defer fake.endHypeTrainMutex.Unlock()
	fake.EndHypeTrainStub = nil
	if fake.endHypeTrainReturnsOnCall == nil {
		fake.endHypeTrainReturnsOnCall = make(map[int]struct {
			result1 error
		})
	}
	fake.endHypeTrainReturnsOnCall[i] = struct {
		result1 error
	}{result1}
}

func (fake *FakePublisher) GetNumberOfPubsubListeners(arg1 string, arg2 string) (int, error) {
	fake.getNumberOfPubsubListenersMutex.Lock()
	ret, specificReturn := fake.getNumberOfPubsubListenersReturnsOnCall[len(fake.getNumberOfPubsubListenersArgsForCall)]
	fake.getNumberOfPubsubListenersArgsForCall = append(fake.getNumberOfPubsubListenersArgsForCall, struct {
		arg1 string
		arg2 string
	}{arg1, arg2})
	stub := fake.GetNumberOfPubsubListenersStub
	fakeReturns := fake.getNumberOfPubsubListenersReturns
	fake.recordInvocation("GetNumberOfPubsubListeners", []interface{}{arg1, arg2})
	fake.getNumberOfPubsubListenersMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2)
	}
	if specificReturn {
		return ret.result1, ret.result2
	}
	return fakeReturns.result1, fakeReturns.result2
}

func (fake *FakePublisher) GetNumberOfPubsubListenersCallCount() int {
	fake.getNumberOfPubsubListenersMutex.RLock()
	defer fake.getNumberOfPubsubListenersMutex.RUnlock()
	return len(fake.getNumberOfPubsubListenersArgsForCall)
}

func (fake *FakePublisher) GetNumberOfPubsubListenersCalls(stub func(string, string) (int, error)) {
	fake.getNumberOfPubsubListenersMutex.Lock()
	defer fake.getNumberOfPubsubListenersMutex.Unlock()
	fake.GetNumberOfPubsubListenersStub = stub
}

func (fake *FakePublisher) GetNumberOfPubsubListenersArgsForCall(i int) (string, string) {
	fake.getNumberOfPubsubListenersMutex.RLock()
	defer fake.getNumberOfPubsubListenersMutex.RUnlock()
	argsForCall := fake.getNumberOfPubsubListenersArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2
}

func (fake *FakePublisher) GetNumberOfPubsubListenersReturns(result1 int, result2 error) {
	fake.getNumberOfPubsubListenersMutex.Lock()
	defer fake.getNumberOfPubsubListenersMutex.Unlock()
	fake.GetNumberOfPubsubListenersStub = nil
	fake.getNumberOfPubsubListenersReturns = struct {
		result1 int
		result2 error
	}{result1, result2}
}

func (fake *FakePublisher) GetNumberOfPubsubListenersReturnsOnCall(i int, result1 int, result2 error) {
	fake.getNumberOfPubsubListenersMutex.Lock()
	defer fake.getNumberOfPubsubListenersMutex.Unlock()
	fake.GetNumberOfPubsubListenersStub = nil
	if fake.getNumberOfPubsubListenersReturnsOnCall == nil {
		fake.getNumberOfPubsubListenersReturnsOnCall = make(map[int]struct {
			result1 int
			result2 error
		})
	}
	fake.getNumberOfPubsubListenersReturnsOnCall[i] = struct {
		result1 int
		result2 error
	}{result1, result2}
}

func (fake *FakePublisher) HypeTrainApproaching(arg1 context.Context, arg2 string, arg3 int64, arg4 map[int]time.Duration, arg5 []percy.Reward, arg6 string, arg7 []string) error {
	var arg5Copy []percy.Reward
	if arg5 != nil {
		arg5Copy = make([]percy.Reward, len(arg5))
		copy(arg5Copy, arg5)
	}
	var arg7Copy []string
	if arg7 != nil {
		arg7Copy = make([]string, len(arg7))
		copy(arg7Copy, arg7)
	}
	fake.hypeTrainApproachingMutex.Lock()
	ret, specificReturn := fake.hypeTrainApproachingReturnsOnCall[len(fake.hypeTrainApproachingArgsForCall)]
	fake.hypeTrainApproachingArgsForCall = append(fake.hypeTrainApproachingArgsForCall, struct {
		arg1 context.Context
		arg2 string
		arg3 int64
		arg4 map[int]time.Duration
		arg5 []percy.Reward
		arg6 string
		arg7 []string
	}{arg1, arg2, arg3, arg4, arg5Copy, arg6, arg7Copy})
	stub := fake.HypeTrainApproachingStub
	fakeReturns := fake.hypeTrainApproachingReturns
	fake.recordInvocation("HypeTrainApproaching", []interface{}{arg1, arg2, arg3, arg4, arg5Copy, arg6, arg7Copy})
	fake.hypeTrainApproachingMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakePublisher) HypeTrainApproachingCallCount() int {
	fake.hypeTrainApproachingMutex.RLock()
	defer fake.hypeTrainApproachingMutex.RUnlock()
	return len(fake.hypeTrainApproachingArgsForCall)
}

func (fake *FakePublisher) HypeTrainApproachingCalls(stub func(context.Context, string, int64, map[int]time.Duration, []percy.Reward, string, []string) error) {
	fake.hypeTrainApproachingMutex.Lock()
	defer fake.hypeTrainApproachingMutex.Unlock()
	fake.HypeTrainApproachingStub = stub
}

func (fake *FakePublisher) HypeTrainApproachingArgsForCall(i int) (context.Context, string, int64, map[int]time.Duration, []percy.Reward, string, []string) {
	fake.hypeTrainApproachingMutex.RLock()
	defer fake.hypeTrainApproachingMutex.RUnlock()
	argsForCall := fake.hypeTrainApproachingArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2, argsForCall.arg3, argsForCall.arg4, argsForCall.arg5, argsForCall.arg6, argsForCall.arg7
}

func (fake *FakePublisher) HypeTrainApproachingReturns(result1 error) {
	fake.hypeTrainApproachingMutex.Lock()
	defer fake.hypeTrainApproachingMutex.Unlock()
	fake.HypeTrainApproachingStub = nil
	fake.hypeTrainApproachingReturns = struct {
		result1 error
	}{result1}
}

func (fake *FakePublisher) HypeTrainApproachingReturnsOnCall(i int, result1 error) {
	fake.hypeTrainApproachingMutex.Lock()
	defer fake.hypeTrainApproachingMutex.Unlock()
	fake.HypeTrainApproachingStub = nil
	if fake.hypeTrainApproachingReturnsOnCall == nil {
		fake.hypeTrainApproachingReturnsOnCall = make(map[int]struct {
			result1 error
		})
	}
	fake.hypeTrainApproachingReturnsOnCall[i] = struct {
		result1 error
	}{result1}
}

func (fake *FakePublisher) LevelUpHypeTrain(arg1 context.Context, arg2 string, arg3 time.Time, arg4 percy.Progress) error {
	fake.levelUpHypeTrainMutex.Lock()
	ret, specificReturn := fake.levelUpHypeTrainReturnsOnCall[len(fake.levelUpHypeTrainArgsForCall)]
	fake.levelUpHypeTrainArgsForCall = append(fake.levelUpHypeTrainArgsForCall, struct {
		arg1 context.Context
		arg2 string
		arg3 time.Time
		arg4 percy.Progress
	}{arg1, arg2, arg3, arg4})
	stub := fake.LevelUpHypeTrainStub
	fakeReturns := fake.levelUpHypeTrainReturns
	fake.recordInvocation("LevelUpHypeTrain", []interface{}{arg1, arg2, arg3, arg4})
	fake.levelUpHypeTrainMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2, arg3, arg4)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakePublisher) LevelUpHypeTrainCallCount() int {
	fake.levelUpHypeTrainMutex.RLock()
	defer fake.levelUpHypeTrainMutex.RUnlock()
	return len(fake.levelUpHypeTrainArgsForCall)
}

func (fake *FakePublisher) LevelUpHypeTrainCalls(stub func(context.Context, string, time.Time, percy.Progress) error) {
	fake.levelUpHypeTrainMutex.Lock()
	defer fake.levelUpHypeTrainMutex.Unlock()
	fake.LevelUpHypeTrainStub = stub
}

func (fake *FakePublisher) LevelUpHypeTrainArgsForCall(i int) (context.Context, string, time.Time, percy.Progress) {
	fake.levelUpHypeTrainMutex.RLock()
	defer fake.levelUpHypeTrainMutex.RUnlock()
	argsForCall := fake.levelUpHypeTrainArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2, argsForCall.arg3, argsForCall.arg4
}

func (fake *FakePublisher) LevelUpHypeTrainReturns(result1 error) {
	fake.levelUpHypeTrainMutex.Lock()
	defer fake.levelUpHypeTrainMutex.Unlock()
	fake.LevelUpHypeTrainStub = nil
	fake.levelUpHypeTrainReturns = struct {
		result1 error
	}{result1}
}

func (fake *FakePublisher) LevelUpHypeTrainReturnsOnCall(i int, result1 error) {
	fake.levelUpHypeTrainMutex.Lock()
	defer fake.levelUpHypeTrainMutex.Unlock()
	fake.LevelUpHypeTrainStub = nil
	if fake.levelUpHypeTrainReturnsOnCall == nil {
		fake.levelUpHypeTrainReturnsOnCall = make(map[int]struct {
			result1 error
		})
	}
	fake.levelUpHypeTrainReturnsOnCall[i] = struct {
		result1 error
	}{result1}
}

func (fake *FakePublisher) ParticipantRewards(arg1 context.Context, arg2 string, arg3 percy.EntitledParticipants) error {
	fake.participantRewardsMutex.Lock()
	ret, specificReturn := fake.participantRewardsReturnsOnCall[len(fake.participantRewardsArgsForCall)]
	fake.participantRewardsArgsForCall = append(fake.participantRewardsArgsForCall, struct {
		arg1 context.Context
		arg2 string
		arg3 percy.EntitledParticipants
	}{arg1, arg2, arg3})
	stub := fake.ParticipantRewardsStub
	fakeReturns := fake.participantRewardsReturns
	fake.recordInvocation("ParticipantRewards", []interface{}{arg1, arg2, arg3})
	fake.participantRewardsMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2, arg3)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakePublisher) ParticipantRewardsCallCount() int {
	fake.participantRewardsMutex.RLock()
	defer fake.participantRewardsMutex.RUnlock()
	return len(fake.participantRewardsArgsForCall)
}

func (fake *FakePublisher) ParticipantRewardsCalls(stub func(context.Context, string, percy.EntitledParticipants) error) {
	fake.participantRewardsMutex.Lock()
	defer fake.participantRewardsMutex.Unlock()
	fake.ParticipantRewardsStub = stub
}

func (fake *FakePublisher) ParticipantRewardsArgsForCall(i int) (context.Context, string, percy.EntitledParticipants) {
	fake.participantRewardsMutex.RLock()
	defer fake.participantRewardsMutex.RUnlock()
	argsForCall := fake.participantRewardsArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2, argsForCall.arg3
}

func (fake *FakePublisher) ParticipantRewardsReturns(result1 error) {
	fake.participantRewardsMutex.Lock()
	defer fake.participantRewardsMutex.Unlock()
	fake.ParticipantRewardsStub = nil
	fake.participantRewardsReturns = struct {
		result1 error
	}{result1}
}

func (fake *FakePublisher) ParticipantRewardsReturnsOnCall(i int, result1 error) {
	fake.participantRewardsMutex.Lock()
	defer fake.participantRewardsMutex.Unlock()
	fake.ParticipantRewardsStub = nil
	if fake.participantRewardsReturnsOnCall == nil {
		fake.participantRewardsReturnsOnCall = make(map[int]struct {
			result1 error
		})
	}
	fake.participantRewardsReturnsOnCall[i] = struct {
		result1 error
	}{result1}
}

func (fake *FakePublisher) PostHypeTrainProgression(arg1 context.Context, arg2 percy.Participation, arg3 percy.Progress) error {
	fake.postHypeTrainProgressionMutex.Lock()
	ret, specificReturn := fake.postHypeTrainProgressionReturnsOnCall[len(fake.postHypeTrainProgressionArgsForCall)]
	fake.postHypeTrainProgressionArgsForCall = append(fake.postHypeTrainProgressionArgsForCall, struct {
		arg1 context.Context
		arg2 percy.Participation
		arg3 percy.Progress
	}{arg1, arg2, arg3})
	stub := fake.PostHypeTrainProgressionStub
	fakeReturns := fake.postHypeTrainProgressionReturns
	fake.recordInvocation("PostHypeTrainProgression", []interface{}{arg1, arg2, arg3})
	fake.postHypeTrainProgressionMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2, arg3)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakePublisher) PostHypeTrainProgressionCallCount() int {
	fake.postHypeTrainProgressionMutex.RLock()
	defer fake.postHypeTrainProgressionMutex.RUnlock()
	return len(fake.postHypeTrainProgressionArgsForCall)
}

func (fake *FakePublisher) PostHypeTrainProgressionCalls(stub func(context.Context, percy.Participation, percy.Progress) error) {
	fake.postHypeTrainProgressionMutex.Lock()
	defer fake.postHypeTrainProgressionMutex.Unlock()
	fake.PostHypeTrainProgressionStub = stub
}

func (fake *FakePublisher) PostHypeTrainProgressionArgsForCall(i int) (context.Context, percy.Participation, percy.Progress) {
	fake.postHypeTrainProgressionMutex.RLock()
	defer fake.postHypeTrainProgressionMutex.RUnlock()
	argsForCall := fake.postHypeTrainProgressionArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2, argsForCall.arg3
}

func (fake *FakePublisher) PostHypeTrainProgressionReturns(result1 error) {
	fake.postHypeTrainProgressionMutex.Lock()
	defer fake.postHypeTrainProgressionMutex.Unlock()
	fake.PostHypeTrainProgressionStub = nil
	fake.postHypeTrainProgressionReturns = struct {
		result1 error
	}{result1}
}

func (fake *FakePublisher) PostHypeTrainProgressionReturnsOnCall(i int, result1 error) {
	fake.postHypeTrainProgressionMutex.Lock()
	defer fake.postHypeTrainProgressionMutex.Unlock()
	fake.PostHypeTrainProgressionStub = nil
	if fake.postHypeTrainProgressionReturnsOnCall == nil {
		fake.postHypeTrainProgressionReturnsOnCall = make(map[int]struct {
			result1 error
		})
	}
	fake.postHypeTrainProgressionReturnsOnCall[i] = struct {
		result1 error
	}{result1}
}

func (fake *FakePublisher) StartCelebration(arg1 context.Context, arg2 string, arg3 percy.User, arg4 int64, arg5 percy.Celebration) error {
	fake.startCelebrationMutex.Lock()
	ret, specificReturn := fake.startCelebrationReturnsOnCall[len(fake.startCelebrationArgsForCall)]
	fake.startCelebrationArgsForCall = append(fake.startCelebrationArgsForCall, struct {
		arg1 context.Context
		arg2 string
		arg3 percy.User
		arg4 int64
		arg5 percy.Celebration
	}{arg1, arg2, arg3, arg4, arg5})
	stub := fake.StartCelebrationStub
	fakeReturns := fake.startCelebrationReturns
	fake.recordInvocation("StartCelebration", []interface{}{arg1, arg2, arg3, arg4, arg5})
	fake.startCelebrationMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2, arg3, arg4, arg5)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakePublisher) StartCelebrationCallCount() int {
	fake.startCelebrationMutex.RLock()
	defer fake.startCelebrationMutex.RUnlock()
	return len(fake.startCelebrationArgsForCall)
}

func (fake *FakePublisher) StartCelebrationCalls(stub func(context.Context, string, percy.User, int64, percy.Celebration) error) {
	fake.startCelebrationMutex.Lock()
	defer fake.startCelebrationMutex.Unlock()
	fake.StartCelebrationStub = stub
}

func (fake *FakePublisher) StartCelebrationArgsForCall(i int) (context.Context, string, percy.User, int64, percy.Celebration) {
	fake.startCelebrationMutex.RLock()
	defer fake.startCelebrationMutex.RUnlock()
	argsForCall := fake.startCelebrationArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2, argsForCall.arg3, argsForCall.arg4, argsForCall.arg5
}

func (fake *FakePublisher) StartCelebrationReturns(result1 error) {
	fake.startCelebrationMutex.Lock()
	defer fake.startCelebrationMutex.Unlock()
	fake.StartCelebrationStub = nil
	fake.startCelebrationReturns = struct {
		result1 error
	}{result1}
}

func (fake *FakePublisher) StartCelebrationReturnsOnCall(i int, result1 error) {
	fake.startCelebrationMutex.Lock()
	defer fake.startCelebrationMutex.Unlock()
	fake.StartCelebrationStub = nil
	if fake.startCelebrationReturnsOnCall == nil {
		fake.startCelebrationReturnsOnCall = make(map[int]struct {
			result1 error
		})
	}
	fake.startCelebrationReturnsOnCall[i] = struct {
		result1 error
	}{result1}
}

func (fake *FakePublisher) StartHypeTrain(arg1 context.Context, arg2 percy.HypeTrain, arg3 ...percy.Participation) error {
	fake.startHypeTrainMutex.Lock()
	ret, specificReturn := fake.startHypeTrainReturnsOnCall[len(fake.startHypeTrainArgsForCall)]
	fake.startHypeTrainArgsForCall = append(fake.startHypeTrainArgsForCall, struct {
		arg1 context.Context
		arg2 percy.HypeTrain
		arg3 []percy.Participation
	}{arg1, arg2, arg3})
	stub := fake.StartHypeTrainStub
	fakeReturns := fake.startHypeTrainReturns
	fake.recordInvocation("StartHypeTrain", []interface{}{arg1, arg2, arg3})
	fake.startHypeTrainMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2, arg3...)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakePublisher) StartHypeTrainCallCount() int {
	fake.startHypeTrainMutex.RLock()
	defer fake.startHypeTrainMutex.RUnlock()
	return len(fake.startHypeTrainArgsForCall)
}

func (fake *FakePublisher) StartHypeTrainCalls(stub func(context.Context, percy.HypeTrain, ...percy.Participation) error) {
	fake.startHypeTrainMutex.Lock()
	defer fake.startHypeTrainMutex.Unlock()
	fake.StartHypeTrainStub = stub
}

func (fake *FakePublisher) StartHypeTrainArgsForCall(i int) (context.Context, percy.HypeTrain, []percy.Participation) {
	fake.startHypeTrainMutex.RLock()
	defer fake.startHypeTrainMutex.RUnlock()
	argsForCall := fake.startHypeTrainArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2, argsForCall.arg3
}

func (fake *FakePublisher) StartHypeTrainReturns(result1 error) {
	fake.startHypeTrainMutex.Lock()
	defer fake.startHypeTrainMutex.Unlock()
	fake.StartHypeTrainStub = nil
	fake.startHypeTrainReturns = struct {
		result1 error
	}{result1}
}

func (fake *FakePublisher) StartHypeTrainReturnsOnCall(i int, result1 error) {
	fake.startHypeTrainMutex.Lock()
	defer fake.startHypeTrainMutex.Unlock()
	fake.StartHypeTrainStub = nil
	if fake.startHypeTrainReturnsOnCall == nil {
		fake.startHypeTrainReturnsOnCall = make(map[int]struct {
			result1 error
		})
	}
	fake.startHypeTrainReturnsOnCall[i] = struct {
		result1 error
	}{result1}
}

func (fake *FakePublisher) Invocations() map[string][][]interface{} {
	fake.invocationsMutex.RLock()
	defer fake.invocationsMutex.RUnlock()
	fake.conductorUpdateMutex.RLock()
	defer fake.conductorUpdateMutex.RUnlock()
	fake.cooldownExpiredMutex.RLock()
	defer fake.cooldownExpiredMutex.RUnlock()
	fake.endHypeTrainMutex.RLock()
	defer fake.endHypeTrainMutex.RUnlock()
	fake.getNumberOfPubsubListenersMutex.RLock()
	defer fake.getNumberOfPubsubListenersMutex.RUnlock()
	fake.hypeTrainApproachingMutex.RLock()
	defer fake.hypeTrainApproachingMutex.RUnlock()
	fake.levelUpHypeTrainMutex.RLock()
	defer fake.levelUpHypeTrainMutex.RUnlock()
	fake.participantRewardsMutex.RLock()
	defer fake.participantRewardsMutex.RUnlock()
	fake.postHypeTrainProgressionMutex.RLock()
	defer fake.postHypeTrainProgressionMutex.RUnlock()
	fake.startCelebrationMutex.RLock()
	defer fake.startCelebrationMutex.RUnlock()
	fake.startHypeTrainMutex.RLock()
	defer fake.startHypeTrainMutex.RUnlock()
	copiedInvocations := map[string][][]interface{}{}
	for key, value := range fake.invocations {
		copiedInvocations[key] = value
	}
	return copiedInvocations
}

func (fake *FakePublisher) recordInvocation(key string, args []interface{}) {
	fake.invocationsMutex.Lock()
	defer fake.invocationsMutex.Unlock()
	if fake.invocations == nil {
		fake.invocations = map[string][][]interface{}{}
	}
	if fake.invocations[key] == nil {
		fake.invocations[key] = [][]interface{}{}
	}
	fake.invocations[key] = append(fake.invocations[key], args)
}

var _ percy.Publisher = new(FakePublisher)
