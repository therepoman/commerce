// Code generated by counterfeiter. DO NOT EDIT.
package internalfakes

import (
	"context"
	"sync"

	percy "code.justin.tv/commerce/percy/internal"
)

type FakeEntitlementDB struct {
	CreateEmoteEntitlementStub        func(context.Context, percy.HypeTrain, percy.EntitledParticipants) error
	createEmoteEntitlementMutex       sync.RWMutex
	createEmoteEntitlementArgsForCall []struct {
		arg1 context.Context
		arg2 percy.HypeTrain
		arg3 percy.EntitledParticipants
	}
	createEmoteEntitlementReturns struct {
		result1 error
	}
	createEmoteEntitlementReturnsOnCall map[int]struct {
		result1 error
	}
	ReadEmoteEntitlementsStub        func(context.Context, string) (map[string]string, error)
	readEmoteEntitlementsMutex       sync.RWMutex
	readEmoteEntitlementsArgsForCall []struct {
		arg1 context.Context
		arg2 string
	}
	readEmoteEntitlementsReturns struct {
		result1 map[string]string
		result2 error
	}
	readEmoteEntitlementsReturnsOnCall map[int]struct {
		result1 map[string]string
		result2 error
	}
	invocations      map[string][][]interface{}
	invocationsMutex sync.RWMutex
}

func (fake *FakeEntitlementDB) CreateEmoteEntitlement(arg1 context.Context, arg2 percy.HypeTrain, arg3 percy.EntitledParticipants) error {
	fake.createEmoteEntitlementMutex.Lock()
	ret, specificReturn := fake.createEmoteEntitlementReturnsOnCall[len(fake.createEmoteEntitlementArgsForCall)]
	fake.createEmoteEntitlementArgsForCall = append(fake.createEmoteEntitlementArgsForCall, struct {
		arg1 context.Context
		arg2 percy.HypeTrain
		arg3 percy.EntitledParticipants
	}{arg1, arg2, arg3})
	stub := fake.CreateEmoteEntitlementStub
	fakeReturns := fake.createEmoteEntitlementReturns
	fake.recordInvocation("CreateEmoteEntitlement", []interface{}{arg1, arg2, arg3})
	fake.createEmoteEntitlementMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2, arg3)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakeEntitlementDB) CreateEmoteEntitlementCallCount() int {
	fake.createEmoteEntitlementMutex.RLock()
	defer fake.createEmoteEntitlementMutex.RUnlock()
	return len(fake.createEmoteEntitlementArgsForCall)
}

func (fake *FakeEntitlementDB) CreateEmoteEntitlementCalls(stub func(context.Context, percy.HypeTrain, percy.EntitledParticipants) error) {
	fake.createEmoteEntitlementMutex.Lock()
	defer fake.createEmoteEntitlementMutex.Unlock()
	fake.CreateEmoteEntitlementStub = stub
}

func (fake *FakeEntitlementDB) CreateEmoteEntitlementArgsForCall(i int) (context.Context, percy.HypeTrain, percy.EntitledParticipants) {
	fake.createEmoteEntitlementMutex.RLock()
	defer fake.createEmoteEntitlementMutex.RUnlock()
	argsForCall := fake.createEmoteEntitlementArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2, argsForCall.arg3
}

func (fake *FakeEntitlementDB) CreateEmoteEntitlementReturns(result1 error) {
	fake.createEmoteEntitlementMutex.Lock()
	defer fake.createEmoteEntitlementMutex.Unlock()
	fake.CreateEmoteEntitlementStub = nil
	fake.createEmoteEntitlementReturns = struct {
		result1 error
	}{result1}
}

func (fake *FakeEntitlementDB) CreateEmoteEntitlementReturnsOnCall(i int, result1 error) {
	fake.createEmoteEntitlementMutex.Lock()
	defer fake.createEmoteEntitlementMutex.Unlock()
	fake.CreateEmoteEntitlementStub = nil
	if fake.createEmoteEntitlementReturnsOnCall == nil {
		fake.createEmoteEntitlementReturnsOnCall = make(map[int]struct {
			result1 error
		})
	}
	fake.createEmoteEntitlementReturnsOnCall[i] = struct {
		result1 error
	}{result1}
}

func (fake *FakeEntitlementDB) ReadEmoteEntitlements(arg1 context.Context, arg2 string) (map[string]string, error) {
	fake.readEmoteEntitlementsMutex.Lock()
	ret, specificReturn := fake.readEmoteEntitlementsReturnsOnCall[len(fake.readEmoteEntitlementsArgsForCall)]
	fake.readEmoteEntitlementsArgsForCall = append(fake.readEmoteEntitlementsArgsForCall, struct {
		arg1 context.Context
		arg2 string
	}{arg1, arg2})
	stub := fake.ReadEmoteEntitlementsStub
	fakeReturns := fake.readEmoteEntitlementsReturns
	fake.recordInvocation("ReadEmoteEntitlements", []interface{}{arg1, arg2})
	fake.readEmoteEntitlementsMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2)
	}
	if specificReturn {
		return ret.result1, ret.result2
	}
	return fakeReturns.result1, fakeReturns.result2
}

func (fake *FakeEntitlementDB) ReadEmoteEntitlementsCallCount() int {
	fake.readEmoteEntitlementsMutex.RLock()
	defer fake.readEmoteEntitlementsMutex.RUnlock()
	return len(fake.readEmoteEntitlementsArgsForCall)
}

func (fake *FakeEntitlementDB) ReadEmoteEntitlementsCalls(stub func(context.Context, string) (map[string]string, error)) {
	fake.readEmoteEntitlementsMutex.Lock()
	defer fake.readEmoteEntitlementsMutex.Unlock()
	fake.ReadEmoteEntitlementsStub = stub
}

func (fake *FakeEntitlementDB) ReadEmoteEntitlementsArgsForCall(i int) (context.Context, string) {
	fake.readEmoteEntitlementsMutex.RLock()
	defer fake.readEmoteEntitlementsMutex.RUnlock()
	argsForCall := fake.readEmoteEntitlementsArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2
}

func (fake *FakeEntitlementDB) ReadEmoteEntitlementsReturns(result1 map[string]string, result2 error) {
	fake.readEmoteEntitlementsMutex.Lock()
	defer fake.readEmoteEntitlementsMutex.Unlock()
	fake.ReadEmoteEntitlementsStub = nil
	fake.readEmoteEntitlementsReturns = struct {
		result1 map[string]string
		result2 error
	}{result1, result2}
}

func (fake *FakeEntitlementDB) ReadEmoteEntitlementsReturnsOnCall(i int, result1 map[string]string, result2 error) {
	fake.readEmoteEntitlementsMutex.Lock()
	defer fake.readEmoteEntitlementsMutex.Unlock()
	fake.ReadEmoteEntitlementsStub = nil
	if fake.readEmoteEntitlementsReturnsOnCall == nil {
		fake.readEmoteEntitlementsReturnsOnCall = make(map[int]struct {
			result1 map[string]string
			result2 error
		})
	}
	fake.readEmoteEntitlementsReturnsOnCall[i] = struct {
		result1 map[string]string
		result2 error
	}{result1, result2}
}

func (fake *FakeEntitlementDB) Invocations() map[string][][]interface{} {
	fake.invocationsMutex.RLock()
	defer fake.invocationsMutex.RUnlock()
	fake.createEmoteEntitlementMutex.RLock()
	defer fake.createEmoteEntitlementMutex.RUnlock()
	fake.readEmoteEntitlementsMutex.RLock()
	defer fake.readEmoteEntitlementsMutex.RUnlock()
	copiedInvocations := map[string][][]interface{}{}
	for key, value := range fake.invocations {
		copiedInvocations[key] = value
	}
	return copiedInvocations
}

func (fake *FakeEntitlementDB) recordInvocation(key string, args []interface{}) {
	fake.invocationsMutex.Lock()
	defer fake.invocationsMutex.Unlock()
	if fake.invocations == nil {
		fake.invocations = map[string][][]interface{}{}
	}
	if fake.invocations[key] == nil {
		fake.invocations[key] = [][]interface{}{}
	}
	fake.invocations[key] = append(fake.invocations[key], args)
}

var _ percy.EntitlementDB = new(FakeEntitlementDB)
