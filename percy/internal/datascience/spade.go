package datascience

import (
	"context"
	"net/http"
	"reflect"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/wrapper"
	"code.justin.tv/common/spade-client-go/spade"
	"code.justin.tv/foundation/twitchclient"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/sirupsen/logrus"
)

const (
	hype_train_paritipation                       = "hype_train_participation"
	hype_train_settings                           = "hype_train_settings"
	hype_train_progress_update                    = "hype_train_progress_update"
	hype_train_participants                       = "hype_train_participants"
	hype_train_special_callout                    = "hype_train_special_callout"
	hype_train_cooldown_ignored_start             = "hype_train_cooldown_ignored_start"
	reward_item_receive                           = "reward_item_receive"
	hype_train_badge_update                       = "hype_train_badge_update"
	hype_train_badge_display_selection            = "hype_train_badge_display_selection"
	hype_train_approaching                        = "hype_train_approaching"
	celebration_overlay_start                     = "celebrations_overlay_start"
	celebration_channel_purchasable_config_change = "state_change_celebrations_channel_purchasable_configs_production"
)

type Spade struct {
	client                  spade.Client
	environment             string
	approachingEventEnabled bool
}

func NewSpadeClient(environment string, maxConcurrency int, statter statsd.Statter, socksAddr string, approachingEventEnabled bool) (*Spade, error) {
	twitchClient := twitchclient.NewHTTPClient(twitchclient.ClientConf{
		TimingXactName: "Spade",
		Stats:          statter,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(socksAddr),
		},
	})

	sc, err := spade.NewClient(
		spade.InitHTTPClient(twitchClient),
		spade.InitMaxConcurrency(maxConcurrency),
	)
	if err != nil {
		return nil, err
	}

	return &Spade{client: sc, environment: environment, approachingEventEnabled: approachingEventEnabled}, nil
}

func (s *Spade) SendHypeTrainParticipation(ctx context.Context, events ...percy.HypeTrainParticipationEventParams) error {
	return s.sendToSpade(ctx, hype_train_paritipation, events)
}

func (s *Spade) SendHypeTrainSettingsEvent(ctx context.Context, events ...percy.HypeTrainSettingsEventParams) error {
	return s.sendToSpade(ctx, hype_train_settings, events)
}

func (s *Spade) SendHypeTrainProgressUpdateEvents(ctx context.Context, events ...percy.HypeTrainProgressUpdateEventParams) error {
	return s.sendToSpade(ctx, hype_train_progress_update, events)
}

func (s *Spade) SendHypeTrainParticipantsEvents(ctx context.Context, events ...percy.HypeTrainParticipantsEventParams) error {
	return s.sendToSpade(ctx, hype_train_participants, events)
}

func (s *Spade) SendHypeTrainSpecialCalloutEvents(ctx context.Context, events ...percy.HypeTrainSpecialCalloutEventParams) error {
	return s.sendToSpade(ctx, hype_train_special_callout, events)
}

func (s *Spade) SendHypeTrainCoolDownIgnoredStartEvents(ctx context.Context, events ...percy.HypeTrainCoolDownIgnoredStartEventParams) error {
	return s.sendToSpade(ctx, hype_train_cooldown_ignored_start, events)
}

func (s *Spade) SendRewardItemReceiveEvents(ctx context.Context, events ...percy.RewardItemReceiveEventParams) error {
	return s.sendToSpade(ctx, reward_item_receive, events)
}

func (s *Spade) SendBadgeUpdateEvents(ctx context.Context, events ...percy.HypeTrainBadgeUpdateEventParams) error {
	return s.sendToSpade(ctx, hype_train_badge_update, events)
}

func (s *Spade) SendBadgeDisplaySelectionEvents(ctx context.Context, events ...percy.HypeTrainBadgeDisplaySelectionEventParams) error {
	return s.sendToSpade(ctx, hype_train_badge_display_selection, events)
}

func (s *Spade) SendHypeTrainApproachingEvent(ctx context.Context, events ...percy.HypeTrainApproachingEventParams) error {
	if s.approachingEventEnabled {
		return s.sendToSpade(ctx, hype_train_approaching, events)
	}
	return nil
}

func (s *Spade) SendCelebrationOverlayInitiatedEvent(ctx context.Context, events ...percy.CelebrationOverlayInitiatedEventParams) error {
	return s.sendToSpade(ctx, celebration_overlay_start, events)
}

func (s *Spade) SendCelebrationChannelPurchasableConfigChangeEvent(ctx context.Context, events ...percy.CelebrationChannelPurchasableConfigChangeEventParams) error {
	return s.sendToSpade(ctx, celebration_channel_purchasable_config_change, events)
}

func (s *Spade) sendToSpade(ctx context.Context, eventName string, eventsSliceInterface interface{}) error {
	eventsValue := reflect.ValueOf(eventsSliceInterface)
	eventsCount := eventsValue.Len()
	events := make([]interface{}, eventsCount)
	for i := 0; i < eventsCount; i++ {
		events[i] = eventsValue.Index(i).Interface()
	}

	var spadeEvents []spade.Event
	for _, event := range events {
		spadeEvents = append(spadeEvents, spade.Event{
			Name:       eventName,
			Properties: event,
		})
	}

	if len(spadeEvents) > 0 {
		if err := s.client.TrackEvents(ctx, spadeEvents...); err != nil {
			logrus.WithFields(logrus.Fields{
				"Event Name":  eventName,
				"Environment": s.environment,
			}).WithError(err).Error("Spade TrackEvents failed")
			return err
		}
	}

	return nil
}
