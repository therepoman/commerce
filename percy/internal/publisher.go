package percy

import (
	"context"
	"encoding/json"
	"time"
)

//go:generate counterfeiter . Publisher

type Publisher interface {
	HypeTrainApproaching(ctx context.Context, channelID string, goal int64, eventsRemainingDurations map[int]time.Duration, rewards []Reward, creatorColor string, participants []string) error
	StartHypeTrain(ctx context.Context, hypeTrain HypeTrain, initialParticipations ...Participation) error
	PostHypeTrainProgression(ctx context.Context, participation Participation, progress Progress) error
	LevelUpHypeTrain(ctx context.Context, channelID string, timeToExpire time.Time, progress Progress) error
	EndHypeTrain(ctx context.Context, hypeTrain HypeTrain, timeEnded time.Time, reason EndingReason) error
	ParticipantRewards(ctx context.Context, channelID string, entitledParticipants EntitledParticipants) error
	ConductorUpdate(ctx context.Context, channelID string, conductor Conductor) error
	CooldownExpired(ctx context.Context, channelID string) error
	StartCelebration(ctx context.Context, channelID string, sender User, amount int64, celebration Celebration) error
	GetNumberOfPubsubListeners(pubsubTopic, channelID string) (int, error)
}

type HypeTrainStartMessage struct {
	MessageType string             `json:"type"`
	Data        HypeTrainStartData `json:"data"`
}

type HypeTrainStartData struct {
	HypeTrain
	Progress Progress `json:"progress"`
}

type HypeTrainProgressionMessage struct {
	MessageType string                   `json:"type"`
	Data        HypeTrainProgressionData `json:"data"`
}

type HypeTrainProgressionData struct {
	// This is the timestamp of the event. This way we can easily know what events are in order.
	UserID              string              `json:"user_id"`
	UserLogin           string              `json:"user_login,omitempty"`
	UserDisplayName     string              `json:"user_display_name,omitempty"`
	UserProfileImageURL string              `json:"user_profile_image_url,omitempty"`
	SequenceID          int                 `json:"sequence_id"`
	Action              ParticipationAction `json:"action"`
	Source              ParticipationSource `json:"source"`
	Quantity            ParticipationPoint  `json:"quantity"`
	Progress            Progress            `json:"progress"`
}

type HypeTrainLevelUpMessage struct {
	MessageType string               `json:"type"`
	Data        HypeTrainLevelUpData `json:"data"`
}

type HypeTrainLevelUpData struct {
	TimeToExpire int64    `json:"time_to_expire"`
	Progress     Progress `json:"progress"`
}

type HypeTrainEndMessage struct {
	MessageType string           `json:"type"`
	Data        HypeTrainEndData `json:"data"`
}

type HypeTrainEndData struct {
	EndedAt      int64        `json:"ended_at"`
	EndingReason EndingReason `json:"ending_reason"`
}

type HypeTrainRewardsMessage struct {
	MessageType string               `json:"type"`
	Data        HypeTrainRewardsData `json:"data"`
}

type HypeTrainRewardsData struct {
	ChannelID      string   `json:"channel_id"`
	CompletedLevel int      `json:"completed_level"`
	Rewards        []Reward `json:"rewards"`
}

type HypeTrainConductorUpdateMessage struct {
	MessageType string                       `json:"type"`
	Data        HypeTrainConductorUpdateData `json:"data"`
}

type HypeTrainConductorUpdateData struct {
	Conductor
}

type HypeTrainCooldownExpirationMessage struct {
	MessageType string `json:"type"`
}

type HypeTrainApproachingMessage struct {
	MessageType string                   `json:"type"`
	Data        HypeTrainApproachingData `json:"data"`
}

type HypeTrainApproachingData struct {
	ChannelID                string        `json:"channel_id"`
	Goal                     int64         `json:"goal"`
	EventsRemainingDurations map[int]int64 `json:"events_remaining_durations"`
	LevelOneRewards          []Reward      `json:"level_one_rewards"`
	CreatorColor             string        `json:"creator_color"`
	Participants             []string      `json:"participants"`
}

func (hypeTrain *HypeTrainStartData) MarshalJSON() ([]byte, error) {
	var endedAt *int64
	if hypeTrain.EndedAt != nil {
		endedAtUnix := hypeTrain.EndedAt.Unix() * 1000
		endedAt = &endedAtUnix
	}

	return json.Marshal(&struct {
		ChannelID      string                            `json:"channel_id"`
		ID             string                            `json:"id"`
		StartedAt      int64                             `json:"started_at"`
		ExpiresAt      int64                             `json:"expires_at"`
		UpdatedAt      int64                             `json:"updated_at"`
		EndedAt        *int64                            `json:"ended_at"`
		EndingReason   *EndingReason                     `json:"ending_reason"`
		Config         HypeTrainConfig                   `json:"config"`
		Participations ParticipationTotals               `json:"participations"`
		Conductors     map[ParticipationSource]Conductor `json:"conductors"`
		Progress       Progress                          `json:"progress"`
	}{
		ChannelID:      hypeTrain.ChannelID,
		ID:             hypeTrain.ID,
		StartedAt:      hypeTrain.StartedAt.Unix() * 1000,
		ExpiresAt:      hypeTrain.ExpiresAt.Unix() * 1000,
		UpdatedAt:      hypeTrain.UpdatedAt.Unix() * 1000,
		EndedAt:        endedAt,
		EndingReason:   hypeTrain.EndingReason,
		Config:         hypeTrain.Config,
		Participations: hypeTrain.Participations,
		Conductors:     hypeTrain.Conductors,
		Progress:       hypeTrain.Progress,
	})
}

func (hypeTrain *HypeTrainStartData) UnmarshalJSON(data []byte) error {
	type Alias HypeTrainStartData
	aux := &struct {
		StartedAt int64  `json:"started_at"`
		ExpiresAt int64  `json:"expires_at"`
		UpdatedAt int64  `json:"updated_at"`
		EndedAt   *int64 `json:"ended_at"`
		*Alias
	}{
		Alias: (*Alias)(hypeTrain),
	}
	if err := json.Unmarshal(data, &aux); err != nil {
		return err
	}

	hypeTrain.StartedAt = time.Unix(aux.StartedAt/1000, 0)
	hypeTrain.ExpiresAt = time.Unix(aux.ExpiresAt/1000, 0)
	hypeTrain.UpdatedAt = time.Unix(aux.UpdatedAt/1000, 0)
	if aux.EndedAt != nil {
		endedAtTime := time.Unix(*aux.EndedAt/1000, 0)
		hypeTrain.EndedAt = &endedAtTime
	}
	return nil
}

func (c *Conductor) MarshalJSON() ([]byte, error) {
	return json.Marshal(&struct {
		Source         ParticipationSource `json:"source"`
		User           User                `json:"user"`
		Participations ParticipationTotals `json:"participations"`
	}{
		Source:         c.Source,
		User:           c.User,
		Participations: c.Participations,
	})
}

type Emote struct {
	ID    string `json:"id"`
	Token string `json:"token"`
}

type CelebrationPubsubData struct {
	CelebrationID                   string               `json:"celebration_id"`
	Enabled                         bool                 `json:"enabled"`
	EventType                       CelebrationEventType `json:"event_type"`
	EventThreshold                  int64                `json:"event_threshold"`
	CelebrationEffect               CelebrationEffect    `json:"effect"`
	CelebrationArea                 CelebrationArea      `json:"area"`
	CelebrationIntensity            int64                `json:"intensity"`
	CelebrationDurationMilliseconds int64                `json:"duration_milliseconds"`
}

type CelebrationStartData struct {
	EventID         string                `json:"event_id"` // this is to differentiate events on the front end
	Celebration     CelebrationPubsubData `json:"celebration"`
	Benefactor      string                `json:"benefactor_name"`
	Amount          int64                 `json:"event_amount"`
	PrimaryColorHex *string               `json:"primary_color_hex"`
	Emotes          []Emote               `json:"emotes"`
}

type CelebrationStartMessage struct {
	MessageType string               `json:"type"`
	Data        CelebrationStartData `json:"data"`
}

func CelebrationToPubsubDate(celebration Celebration) CelebrationPubsubData {
	duration := int64(celebration.CelebrationDuration) / 1e6
	return CelebrationPubsubData{
		CelebrationID:                   celebration.CelebrationID,
		Enabled:                         celebration.Enabled,
		EventType:                       celebration.EventType,
		EventThreshold:                  celebration.EventThreshold,
		CelebrationEffect:               celebration.CelebrationEffect,
		CelebrationArea:                 celebration.CelebrationArea,
		CelebrationIntensity:            celebration.CelebrationIntensity,
		CelebrationDurationMilliseconds: duration,
	}
}
