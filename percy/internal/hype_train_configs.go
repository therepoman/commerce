package percy

import (
	"context"
	"encoding/json"
	"sort"
	"strings"
	"time"

	"code.justin.tv/commerce/logrus"
	"github.com/pkg/errors"
)

type (
	Difficulty         string
	ParticipationPoint int
)

const (
	DifficultyEasy      = Difficulty("EASY")
	DifficultyMedium    = Difficulty("MEDIUM")
	DifficultyHard      = Difficulty("HARD")
	DifficultySuperHard = Difficulty("SUPER HARD")
	DifficultyInsane    = Difficulty("INSANE")
	DifficultyUnknown   = Difficulty("UNKNOWN")
)

func (difficulty *Difficulty) Unknown() bool {
	return *difficulty == DifficultyUnknown
}

var ValidDifficulties = map[Difficulty]bool{
	DifficultyEasy:      true,
	DifficultyMedium:    true,
	DifficultyHard:      true,
	DifficultySuperHard: true,
	DifficultyInsane:    true,
}

type (
	DifficultySettings map[Difficulty][]LevelSetting
	ConductorRewards   map[ParticipationSource]map[ConductorType][]Reward
)

type ParticipationConversionRates map[ParticipationIdentifier]ParticipationPoint

func (p ParticipationConversionRates) MarshalJSON() ([]byte, error) {
	newMap := make(map[string]ParticipationPoint)

	for key, entry := range p {
		newKey := string(key.Source) + "." + string(key.Action)
		newMap[newKey] = entry
	}

	return json.Marshal(newMap)
}

func (p *ParticipationConversionRates) UnmarshalJSON(data []byte) error {
	convMap := make(map[string]ParticipationPoint)

	if err := json.Unmarshal(data, &convMap); err != nil {
		return err
	}

	newMap := make(ParticipationConversionRates)

	for key, entry := range convMap {
		sourceAndAction := strings.Split(key, ".")
		if len(sourceAndAction) != 2 {
			return errors.New("key needs to be SOURCE.ACTION")
		}
		newKey := ParticipationIdentifier{
			Source: ParticipationSource(sourceAndAction[0]),
			Action: ParticipationAction(sourceAndAction[1]),
		}
		newMap[newKey] = entry
	}

	*p = newMap

	return nil
}

type NotificationThresholds map[ParticipationIdentifier]int

func (n NotificationThresholds) MarshalJSON() ([]byte, error) {
	newMap := make(map[string]int)

	for key, entry := range n {
		newKey := string(key.Source) + "." + string(key.Action)
		newMap[newKey] = entry
	}

	return json.Marshal(newMap)
}

func (n *NotificationThresholds) UnmarshalJSON(data []byte) error {
	convMap := make(map[string]int)

	if err := json.Unmarshal(data, &convMap); err != nil {
		return err
	}

	newMap := make(NotificationThresholds)

	for key, entry := range convMap {
		sourceAndAction := strings.Split(key, ".")
		if len(sourceAndAction) != 2 {
			return errors.New("key needs to be SOURCE.ACTION")
		}
		newKey := ParticipationIdentifier{
			Source: ParticipationSource(sourceAndAction[0]),
			Action: ParticipationAction(sourceAndAction[1]),
		}
		newMap[newKey] = entry
	}

	*n = newMap

	return nil
}

type HypeTrainConfig struct {
	ChannelID     string `json:"channel_id"`
	Enabled       bool   `json:"is_enabled"`
	IsWhitelisted bool   `json:"is_whitelisted"`

	KickoffConfig    KickoffConfig `json:"kickoff"`
	CooldownDuration time.Duration `json:"cooldown_duration"`
	LevelDuration    time.Duration `json:"level_duration"`
	Difficulty       Difficulty    `json:"difficulty"`
	RewardEndDate    *time.Time    `json:"reward_end_date"`

	// Mapping participations to a participation point value.
	ParticipationConversionRates ParticipationConversionRates `json:"participation_conversion_rates"`

	// Participations that meets this threshold should trigger a special callout.
	NotificationThresholds NotificationThresholds `json:"notification_thresholds"`

	LevelSettingsByDifficulty DifficultySettings `json:"difficulty_settings"`
	ConductorRewards          ConductorRewards   `json:"conductor_rewards"`
	CalloutEmoteID            string             `json:"callout_emote_id"`
	CalloutEmoteToken         string             `json:"callout_emote_token"`
	UseCreatorColor           bool               `json:"use_creator_color"`
	PrimaryHexColor           string             `json:"primary_hex_color"`
	UsePersonalizedSettings   bool               `json:"use_personalized_settings"`

	HasConductorBadges bool `json:"has_conductor_badges"`
}

func (config *HypeTrainConfig) IsValid() bool {
	// ensure level settings exists for the selected difficulty and
	// the settings are sorted in ascending order with no gaps in between levels
	levelSettings := config.LevelSettings()
	if len(levelSettings) == 0 {
		return false
	}

	prevLevel := 0
	prevGoal := ParticipationPoint(0)
	for _, setting := range levelSettings {
		if setting.Level != prevLevel+1 || setting.Goal < prevGoal {
			return false
		}
		prevLevel = setting.Level
	}

	// check numerical and time fields
	if len(config.ChannelID) == 0 ||
		config.KickoffConfig.NumOfEvents < 1 ||
		config.KickoffConfig.MinPoints < 1 ||
		config.KickoffConfig.Duration.Seconds() < 1.0 ||
		config.LevelDuration.Seconds() < 1.0 ||
		len(config.Difficulty) == 0 ||
		len(config.ParticipationConversionRates) < 1 {

		return false
	}

	return true
}

// LevelSettings returns the level settings for the selected difficulty
func (config *HypeTrainConfig) LevelSettings() []LevelSetting {
	settings := config.LevelSettingsByDifficulty[config.Difficulty]
	sort.SliceStable(settings, func(i, j int) bool {
		return settings[i].Level < settings[j].Level
	})

	return settings
}

func (config *HypeTrainConfig) ShouldTriggerNotification(identifier ParticipationIdentifier, quantity int) bool {
	if threshold, present := config.NotificationThresholds[identifier]; present {
		return quantity >= threshold
	}

	return false
}

func (config *HypeTrainConfig) EquivalentParticipationPoint(identifier ParticipationIdentifier, quantity int) ParticipationPoint {
	if conversionRate, present := config.ParticipationConversionRates[identifier]; present {
		return ParticipationPoint(quantity) * conversionRate
	} else {
		logrus.WithFields(logrus.Fields{
			"channelID":  config.ChannelID,
			"identifier": identifier,
			"quantity":   quantity,
		}).Warnf("No conversion rate found for identifier in config: %+v", config)
	}

	return 0
}

// Remove information about difficulties that are not the currently selected difficulty
func (config *HypeTrainConfig) TrimDifficultySettings() HypeTrainConfig {
	for difficulty := range config.LevelSettingsByDifficulty {
		if difficulty != config.Difficulty {
			delete(config.LevelSettingsByDifficulty, difficulty)
		}
	}

	return *config
}

type KickoffConfig struct {
	// NumOfEvents is then number of events from unique users needed to kick off a new hype train.
	NumOfEvents int `json:"num_of_events"`

	// MinPoints determines what participations are eligible to count towards kickoff.
	MinPoints ParticipationPoint `json:"min_points"`

	// Duration is the period of time to check for events to determine kick off. e.g from (Now - Duration) to Now
	Duration time.Duration `json:"duration"`
}

type LevelSetting struct {
	Level   int                `json:"value"`
	Goal    ParticipationPoint `json:"goal"`
	Rewards []Reward           `json:"rewards"`
}

type Reward struct {
	Type        RewardType `json:"type"`
	ID          string     `json:"id"`
	GroupID     string     `json:"group_id"`
	RewardLevel int        `json:"reward_level"`
	BadgeID     string     `json:"badge_id,omitempty"`
	ImageURL    string     `json:"image_url,omitempty"`
	SetID       string     `json:"set_id,omitempty"`
	Token       string     `json:"token,omitempty"`
}

type RewardType string

const (
	EmoteReward    = RewardType("EMOTE")
	BadgeReward    = RewardType("BADGE")
	UnknownRewards = RewardType("UNKNOWN")
)

type ConductorType string

const (
	FormerConductor  = ConductorType("FORMER")
	CurrentConductor = ConductorType("CURRENT")

	minNumOfEventsToKickoff = 3
	maxNumOfEventsToKickoff = 25

	minCooldownDurationMinutes = 60  // 1 hour
	maxCooldownDurationMinutes = 480 // 8 hours
)

type HypeTrainConfigUpdate struct {
	IsWhitelisted           *bool
	Enabled                 *bool
	NumOfEventsToKickoff    *int
	Difficulty              *Difficulty
	CooldownDuration        *time.Duration
	CalloutEmoteID          *string
	UseCreatorColor         *bool
	UsePersonalizedSettings *bool
	KickoffDuration         *time.Duration
}

type HypeTrainConfigUnset struct {
	IsWhitelisted           bool
	Enabled                 bool
	NumOfEventsToKickoff    bool
	Difficulty              bool
	CooldownDuration        bool
	CalloutEmoteID          bool
	UseCreatorColor         bool
	UsePersonalizedSettings bool
	KickoffDuration         bool
}

func (update *HypeTrainConfigUpdate) IsValid() bool {
	if update.NumOfEventsToKickoff != nil &&
		(*update.NumOfEventsToKickoff < minNumOfEventsToKickoff ||
			*update.NumOfEventsToKickoff > maxNumOfEventsToKickoff) {
		return false
	}

	if update.Difficulty != nil {
		if _, valid := ValidDifficulties[*update.Difficulty]; !valid {
			return false
		}
	}

	if update.CooldownDuration != nil &&
		(update.CooldownDuration.Minutes() < minCooldownDurationMinutes ||
			update.CooldownDuration.Minutes() > maxCooldownDurationMinutes) {
		return false
	}

	return true
}

//go:generate counterfeiter . HypeTrainConfigsDB

type HypeTrainConfigsDB interface {
	UpdateConfig(ctx context.Context, channelID string, update HypeTrainConfigUpdate) (*HypeTrainConfig, error)
	GetConfig(ctx context.Context, channelID string) (*HypeTrainConfig, error)
	UnsetConfig(ctx context.Context, channelID string, configUpdate HypeTrainConfigUnset) (*HypeTrainConfig, error)
	DeleteConfigRecords(ctx context.Context, channelIDs []string) error
}

func (eng *engine) GetHypeTrainConfig(ctx context.Context, channelID string) (HypeTrainConfig, error) {
	if len(channelID) == 0 {
		return HypeTrainConfig{}, errors.New("channel ID is blank")
	}

	config, err := eng.ConfigsDB.GetConfig(ctx, channelID)
	if err != nil {
		return HypeTrainConfig{}, err
	}

	if config == nil {
		return HypeTrainConfig{}, errors.New("no hype train config found")
	}

	if !config.IsValid() {
		logrus.WithFields(logrus.Fields{
			"channelID": channelID,
			"config":    *config,
		}).Errorf("Found invalid hype train config from DB")
		return HypeTrainConfig{}, errors.Errorf("invalid hype train config")
	}

	return *config, nil
}

func (eng *engine) UpdateHypeTrainConfig(ctx context.Context, channelID string, update HypeTrainConfigUpdate) (HypeTrainConfig, error) {
	if len(channelID) == 0 {
		return HypeTrainConfig{}, errors.New("channel ID is blank")
	}

	if !update.IsValid() {
		return HypeTrainConfig{}, errors.New("invalid hype train config update")
	}

	updatedConfig, err := eng.ConfigsDB.UpdateConfig(ctx, channelID, update)
	if err != nil {
		return HypeTrainConfig{}, err
	}

	if updatedConfig == nil {
		return HypeTrainConfig{}, errors.New("no updated hype train config is found")
	}

	if !updatedConfig.IsValid() {
		return HypeTrainConfig{}, errors.New("hype train config is invalid after an update")
	}

	// Send spade event for config update
	// Send empty hypetrainID since there might be no current hype train
	go eng.SendHypeTrainSettingsEvent("", *updatedConfig, "UPDATE")

	go func() {
		ctx := context.Background()

		if update.CooldownDuration == nil {
			return
		}

		hypeTrain, err := eng.HypeTrainDB.GetMostRecentHypeTrain(ctx, channelID)
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"channelID": channelID,
			}).WithError(err).Error("Failed to get most recent hype train for calling Destiny")
			return
		}
		if hypeTrain == nil || hypeTrain.EndedAt != nil {
			return
		}

		err = eng.EventDB.UpsertHypeTrainCoolDownTimer(ctx, *hypeTrain)
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"channelID":   channelID,
				"hypeTrainID": hypeTrain.ID,
			}).WithError(err).Error("Failed to send cool down event to Destiny")
		}
	}()

	return *updatedConfig, nil
}

func (eng *engine) UnsetHypeTrainConfig(ctx context.Context, channelID string, update HypeTrainConfigUnset) (HypeTrainConfig, error) {
	if len(channelID) == 0 {
		return HypeTrainConfig{}, errors.New("channel ID is blank")
	}

	updatedConfig, err := eng.ConfigsDB.UnsetConfig(ctx, channelID, update)
	if err != nil {
		return HypeTrainConfig{}, err
	}

	if updatedConfig == nil {
		return HypeTrainConfig{}, errors.New("no updated hype train config is found")
	}

	if !updatedConfig.IsValid() {
		return HypeTrainConfig{}, errors.New("hype train config is invalid after an update")
	}

	// Send spade event for config update
	// Send empty hypetrainID since there might be no current hype train
	go eng.SendHypeTrainSettingsEvent("", *updatedConfig, "UPDATE")

	go func() {
		ctx := context.Background()

		if !update.CooldownDuration {
			return
		}

		hypeTrain, err := eng.HypeTrainDB.GetMostRecentHypeTrain(ctx, channelID)
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"channelID": channelID,
			}).WithError(err).Error("Failed to get most recent hype train for calling Destiny")
			return
		}
		if hypeTrain == nil || hypeTrain.EndedAt != nil {
			return
		}

		err = eng.EventDB.UpsertHypeTrainCoolDownTimer(ctx, *hypeTrain)
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"channelID":   channelID,
				"hypeTrainID": hypeTrain.ID,
			}).WithError(err).Error("Failed to send cool down event to Destiny")
		}
	}()

	return *updatedConfig, nil
}
