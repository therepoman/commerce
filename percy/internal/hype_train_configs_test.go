package percy_test

import (
	"context"
	"encoding/json"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	. "github.com/smartystreets/goconvey/convey"
)

func TestParticipationConversionRates_UnmarshalJSON(t *testing.T) {
	Convey("should work?", t, func() {
		configString := `{
      "BITS.CHEER" : 1,
      "BITS.POLL" : 1,
      "SUBS.TIER_2_SUB" : 1000,
      "SUBS.TIER_3_SUB" : 2500,
      "SUBS.TIER_3_GIFTED_SUB" : 2500,
      "SUBS.TIER_1_SUB" : 500,
      "BITS.EXTENSION" : 1,
      "SUBS.TIER_1_GIFTED_SUB" : 500,
      "SUBS.TIER_2_GIFTED_SUB" : 1000
}`

		var unmarshaled percy.ParticipationConversionRates

		err := json.Unmarshal([]byte(configString), &unmarshaled)

		So(err, ShouldBeNil)
		So(unmarshaled, ShouldHaveLength, 9)
		So(unmarshaled[percy.ParticipationIdentifier{
			Source: percy.BitsSource,
			Action: percy.CheerAction,
		}], ShouldEqual, 1)
	})
}

func TestHypeTrainConfig_IsValid(t *testing.T) {
	var config percy.HypeTrainConfig
	Convey("Returns true when", t, func() {
		Convey("given a valid config", func() {
			config = tests.ValidHypeTrainConfig()
		})

		So(config.IsValid(), ShouldBeTrue)
	})

	Convey("Returns false when", t, func() {
		config = tests.ValidHypeTrainConfig()

		var emptyDuration time.Duration

		Convey("channel ID is blank", func() {
			config.ChannelID = ""
		})

		Convey("number of events to kickoff is less than 1", func() {
			config.KickoffConfig.NumOfEvents = 0
		})

		Convey("minimum points that counts towards kickoff is less than 1", func() {
			config.KickoffConfig.MinPoints = 0
		})

		Convey("kickoff duration is 0", func() {
			config.KickoffConfig.Duration = emptyDuration
		})

		Convey("level duration is 0", func() {
			config.LevelDuration = emptyDuration
		})

		Convey("difficulty is unspecified", func() {
			config.Difficulty = ""
		})

		So(config.IsValid(), ShouldBeFalse)
	})
}

func TestEngine_GetHypeTrainConfig(t *testing.T) {
	Convey("Given an Engine", t, func() {
		configsDB := &internalfakes.FakeHypeTrainConfigsDB{}
		engine := percy.NewEngine(percy.EngineConfig{
			ConfigsDB: configsDB,
		})

		ctx := context.Background()
		channelID := "1234567890"

		validConfig := tests.ValidHypeTrainConfig()
		invalidConfig := tests.InvalidHypeTrainConfig()

		Convey("when configs DB retrieves a valid config", func() {
			configsDB.GetConfigReturns(&validConfig, nil)

			Convey("returns the valid config", func() {
				config, err := engine.GetHypeTrainConfig(ctx, channelID)

				So(err, ShouldBeNil)
				So(config, ShouldResemble, validConfig)
			})
		})

		Convey("returns an error when", func() {
			Convey("channel ID is blank", func() {
				channelID = ""
			})

			Convey("configs DB errors", func() {
				configsDB.GetConfigReturns(nil, errors.New("configs DB error"))
			})

			Convey("no config is found", func() {
				configsDB.GetConfigReturns(nil, nil)
			})

			Convey("the found config isn't valid", func() {
				configsDB.GetConfigReturns(&invalidConfig, nil)
			})

			_, err := engine.GetHypeTrainConfig(ctx, channelID)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestEngine_UpdateHypeTrainConfig(t *testing.T) {
	Convey("Given an Engine", t, func() {
		configsDB := &internalfakes.FakeHypeTrainConfigsDB{}
		hypeTrainDB := &internalfakes.FakeHypeTrainDB{}
		spade := &internalfakes.FakeSpade{}
		eventDB := &internalfakes.FakeEventDB{}

		engine := percy.NewEngine(percy.EngineConfig{
			ConfigsDB:   configsDB,
			HypeTrainDB: hypeTrainDB,
			Spade:       spade,
			EventDB:     eventDB,
		})

		ctx := context.Background()
		channelID := "1234567890"

		validConfig := tests.ValidHypeTrainConfig()
		invalidConfig := tests.InvalidHypeTrainConfig()

		difficulty := percy.DifficultyMedium
		update := percy.HypeTrainConfigUpdate{
			Enabled:                 pointers.BoolP(true),
			NumOfEventsToKickoff:    pointers.IntP(3),
			Difficulty:              &difficulty,
			CooldownDuration:        pointers.DurationP(2 * time.Hour),
			UseCreatorColor:         pointers.BoolP(true),
			UsePersonalizedSettings: pointers.BoolP(true),
			CalloutEmoteID:          pointers.StringP("123123123"),
		}

		Convey("updates config in configs DB", func() {
			configsDB.UpdateConfigReturns(&validConfig, nil)
			spade.SendHypeTrainSettingsEventReturns(nil)

			updatedConfig, err := engine.UpdateHypeTrainConfig(ctx, channelID, update)

			So(err, ShouldBeNil)
			So(configsDB.UpdateConfigCallCount(), ShouldEqual, 1)

			Convey("returns the updated config", func() {
				So(updatedConfig, ShouldResemble, validConfig)
			})
		})

		Convey("returns an error when", func() {
			Convey("channel ID is blank", func() {
				channelID = ""
			})

			Convey("hype train config update isn't valid", func() {
				Convey("number of events to kickoff is less than 3", func() {
					update.NumOfEventsToKickoff = pointers.IntP(2)
				})

				Convey("with unknown difficulty", func() {
					difficulty := percy.Difficulty("what?")
					update.Difficulty = &difficulty
				})

				Convey("cooldown duration less than 1 hour", func() {
					update.CooldownDuration = pointers.DurationP(59 * time.Second)
				})
			})

			Convey("configs DB errors", func() {
				configsDB.UpdateConfigReturns(nil, errors.New("configs DB error"))
			})

			Convey("no updated config is found", func() {
				configsDB.UpdateConfigReturns(nil, nil)
			})

			Convey("the updated config isn't valid", func() {
				configsDB.UpdateConfigReturns(&invalidConfig, nil)
			})

			_, err := engine.UpdateHypeTrainConfig(ctx, channelID, update)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestEngine_UnsetHypeTrainConfig(t *testing.T) {
	Convey("Given an Engine", t, func() {
		configsDB := &internalfakes.FakeHypeTrainConfigsDB{}
		hypeTrainDB := &internalfakes.FakeHypeTrainDB{}
		spade := &internalfakes.FakeSpade{}
		eventDB := &internalfakes.FakeEventDB{}

		engine := percy.NewEngine(percy.EngineConfig{
			ConfigsDB:   configsDB,
			HypeTrainDB: hypeTrainDB,
			Spade:       spade,
			EventDB:     eventDB,
		})

		ctx := context.Background()
		channelID := "1234567890"

		validConfig := tests.ValidHypeTrainConfig()
		invalidConfig := tests.InvalidHypeTrainConfig()

		update := percy.HypeTrainConfigUnset{
			Enabled:                 true,
			NumOfEventsToKickoff:    true,
			Difficulty:              true,
			CooldownDuration:        true,
			UseCreatorColor:         true,
			UsePersonalizedSettings: true,
			CalloutEmoteID:          true,
		}

		Convey("updates config in configs DB", func() {
			configsDB.UnsetConfigReturns(&validConfig, nil)
			spade.SendHypeTrainSettingsEventReturns(nil)

			updatedConfig, err := engine.UnsetHypeTrainConfig(ctx, channelID, update)

			So(err, ShouldBeNil)
			So(configsDB.UnsetConfigCallCount(), ShouldEqual, 1)

			Convey("returns the updated config", func() {
				So(updatedConfig, ShouldResemble, validConfig)
			})
		})

		Convey("returns an error when", func() {
			Convey("channel ID is blank", func() {
				channelID = ""
			})

			Convey("configs DB errors", func() {
				configsDB.UnsetConfigReturns(nil, errors.New("configs DB error"))
			})

			Convey("no updated config is found", func() {
				configsDB.UnsetConfigReturns(nil, nil)
			})

			Convey("the updated config isn't valid", func() {
				configsDB.UnsetConfigReturns(&invalidConfig, nil)
			})

			_, err := engine.UnsetHypeTrainConfig(ctx, channelID, update)
			So(err, ShouldNotBeNil)
		})
	})
}
