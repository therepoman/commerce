package arbiter

import (
	"context"
	"net/http"

	"code.justin.tv/amzn/TwitchArbiterTwirp"
	"code.justin.tv/commerce/percy/config"
	"code.justin.tv/commerce/percy/internal/wrapper"
	"code.justin.tv/foundation/twitchclient"
	"github.com/cactus/go-statsd-client/statsd"
)

const (
	serviceName         = "arbiter"
	defaultRetry        = 3
	defaultTimeout      = 2000
	applyContentTagsAPI = "applyContentTags"
)

type dynamicTagImpl struct {
	Arbiter        TwitchArbiterTwirp.Arbiter
	HypeTrainTagID string
}

func NewDynamicTagClient(cfg *config.Config, statter statsd.Statter) (*dynamicTagImpl, error) {
	s2sRoundTripperWrapper, err := wrapper.NewS2SRoundTripperWrapper(cfg.S2SServiceName)
	if err != nil {
		return nil, err
	}

	return &dynamicTagImpl{
		Arbiter: TwitchArbiterTwirp.NewArbiterProtobufClient(cfg.Endpoints.Arbiter, twitchclient.NewHTTPClient(twitchclient.ClientConf{
			Stats: statter,
			RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
				s2sRoundTripperWrapper,
				wrapper.NewProxyRoundTripWrapper(cfg.SOCKS5Addr),
				wrapper.NewRetryRoundTripWrapper(statter, serviceName, defaultRetry),
				wrapper.NewHystrixRoundTripWrapper(serviceName, defaultTimeout),
			},
		})),
		HypeTrainTagID: cfg.HypeTrainDynamicTagID,
	}, nil
}

func (dti dynamicTagImpl) ApplyTag(ctx context.Context, channelID string) error {
	reqCtx := wrapper.GenerateRequestContext(ctx, serviceName, applyContentTagsAPI)
	_, err := dti.Arbiter.ApplyContentTags(reqCtx, &TwitchArbiterTwirp.ApplyContentTagsRequest{
		ContentId:      channelID,
		ContentType:    TwitchArbiterTwirp.ContentType_LIVE_CHANNEL,
		TagIdsToAdd:    []string{dti.HypeTrainTagID},
		TagIdsToRemove: []string{},
	})
	return err
}

func (dti dynamicTagImpl) RemoveTag(ctx context.Context, channelID string) error {
	reqCtx := wrapper.GenerateRequestContext(ctx, serviceName, applyContentTagsAPI)
	_, err := dti.Arbiter.ApplyContentTags(reqCtx, &TwitchArbiterTwirp.ApplyContentTagsRequest{
		ContentId:      channelID,
		ContentType:    TwitchArbiterTwirp.ContentType_LIVE_CHANNEL,
		TagIdsToAdd:    []string{},
		TagIdsToRemove: []string{dti.HypeTrainTagID},
	})
	return err
}
