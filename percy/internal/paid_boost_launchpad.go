package percy

import (
	"context"
	"math/rand"
	"strconv"
	"time"

	"code.justin.tv/commerce/logrus"
	"github.com/google/uuid"
	"github.com/pkg/errors"
)

const (
	boostOpportunityContributionRefundReason = "Could not successfully process boost opportunity contribution"
)

//go:generate counterfeiter . LaunchPad
type LaunchPad interface {
	ChannelIsAllowed(channelID string) bool
	GetActiveBoostOpportunity(ctx context.Context, channelID string) (*BoostOpportunity, error)
	ScheduleBoostOpportunity(ctx context.Context, channelID string, startTime time.Time) error
	StartBoostOpportunity(ctx context.Context, channelID string) (BoostOpportunity, error)
	CompleteBoostOpportunity(ctx context.Context, channelID, opportunityID string) error
	AddBoostOpportunityContribution(ctx context.Context, contribution BoostOpportunityContribution) (BoostOpportunity, error)
	SendBoostOpportunityContributionUserNotice(ctx context.Context, contribution BoostOpportunityContribution) error
	IsUserAllowedToContribute(ctx context.Context, userID, channelID string, useConsistentReads bool) (bool, error)
	FulfillContribution(ctx context.Context, contribution BoostOpportunityContribution) error
	RefundContribution(ctx context.Context, contribution BoostOpportunityContribution) error
}

var _ LaunchPad = &launchPad{}

// Our backend struct, will house all business logic for paid boost
type launchPad struct {
	boostsDB        BoostsDB
	scheduler       BoostOpportunityScheduler
	publisher       BoostsPublisher
	boostsLauncher  BoostsLauncher
	configsDB       PaidBoostConfigsDB
	userProfileDB   UserProfileDB
	channelStatusDB ChannelStatusDB
	stateMachine    StateMachine

	// from Celebrations work, to issue refunds
	paymentsHandler Purchase

	// PacificLocation is in Pacific time and is used to determine kickoff daily limits
	pacificLocation *time.Location
}

//go:generate counterfeiter . PaidBoostConfigsDB
type PaidBoostConfigsDB interface {
	IsChannelAllowed(channelID string) bool
	GetBoostOpportunityConfig(channelID string) BoostOpportunityConfig
}

// TODO: Modify this as needed to successfully initialize client
type LaunchPadConfig struct {
	BoostsDB        BoostsDB
	ChannelStatusDB ChannelStatusDB
	ConfigsDB       PaidBoostConfigsDB
	BoostsLauncher  BoostsLauncher
	PaymentsHandler Purchase
	Publisher       BoostsPublisher
	Scheduler       BoostOpportunityScheduler
	StateMachine    StateMachine
	UserProfileDB   UserProfileDB
}

func NewPaidBoostLaunchPad(cfg LaunchPadConfig) LaunchPad {
	// Load a Pacific time location that is used to determine daily boost opportunity kickoff limits
	location, err := time.LoadLocation("America/Los_Angeles")
	if err != nil {
		// Fallback to "Local" which doesn't throw an error, this "should" be safe since we run all our services in us-west-2
		// We shouldn't need to be keeping this per-day logic past the initial experiment phase
		location, _ = time.LoadLocation("Local")
		logrus.WithError(err).Warn("Failed to load PST timezone data, falling back to 'Local'")
	}

	return &launchPad{
		boostsDB:        cfg.BoostsDB,
		scheduler:       cfg.Scheduler,
		publisher:       cfg.Publisher,
		boostsLauncher:  cfg.BoostsLauncher,
		paymentsHandler: cfg.PaymentsHandler,
		configsDB:       cfg.ConfigsDB,
		userProfileDB:   cfg.UserProfileDB,
		channelStatusDB: cfg.ChannelStatusDB,
		stateMachine:    cfg.StateMachine,
		pacificLocation: location,
	}
}

func (pad *launchPad) ChannelIsAllowed(channelID string) bool {
	return pad.configsDB.IsChannelAllowed(channelID)
}

// GetActiveBoostOpportunity returns an active paid boost opportunity, or nil if there isn't an active one
func (pad *launchPad) GetActiveBoostOpportunity(ctx context.Context, channelID string) (*BoostOpportunity, error) {
	// Get latest opportunity
	latestOpportunity, err := pad.boostsDB.GetLatestBoostOpportunity(ctx, channelID, BoostsDBReadOptions{
		ConsistentRead: false,
	})
	if err != nil {
		return nil, errors.Wrap(err, "error getting latest boost opportunity")
	}

	// channel has no opportunity
	if latestOpportunity == nil {
		return nil, nil
	}

	// Check if the opportunity is active
	isActive := time.Now().Before(latestOpportunity.ExpiresAt) && latestOpportunity.EndedAt == nil
	if !isActive {
		return nil, nil
	}

	return latestOpportunity, nil
}

/*
	ScheduleBoostOpportunity is called when Percy receives eventbus message that an eligible channel has gone live
	this schedules a boost opportunity to be kicked off for a channel some time in the future.
	Since the eventbus SQS worker calls ChannelIsAllowed before calling this handler, we can assume channel eligibility
*/
func (pad *launchPad) ScheduleBoostOpportunity(ctx context.Context, channelID string, startTime time.Time) error {
	logrus.WithFields(logrus.Fields{
		"channelID": channelID,
		"startTime": startTime,
	}).Info("ScheduleBoostOpportunity is called!")

	config := pad.configsDB.GetBoostOpportunityConfig(channelID)

	// No-op if:
	//   - there is already a boost opportunity going on
	//   - there had already been a boost opportunity kicked off in the same day (in PT)
	//   - there is still cooldown time left since the last boost opportunity from the previous day
	latestOpportunity, err := pad.boostsDB.GetLatestBoostOpportunity(ctx, channelID, BoostsDBReadOptions{
		ConsistentRead: true,
	})
	if err != nil {
		return errors.Wrap(err, "error getting latest boost opportunity")
	}

	if !config.SkipKickoffLimitCheck && latestOpportunity != nil {
		if time.Now().Before(latestOpportunity.ExpiresAt.Add(config.Cooldown)) {
			return nil
		}

		if latestOpportunity.ExpiresAt.In(pad.pacificLocation).YearDay() == time.Now().In(pad.pacificLocation).YearDay() {
			return nil
		}
	}

	// Check Stream online status, noop if stream is offline (in case eventbus message was false alarm)
	streamIsOnline, err := pad.channelStatusDB.ChannelIsStreaming(ctx, channelID)
	if err != nil {
		return errors.Wrap(err, "error checking stream online status")
	}
	if !streamIsOnline {
		return nil
	}

	// Schedule Boost Opportunity Kick off
	// Kick off time are randomly chosen between min to max minutes from now (i.e. somewhere between 30 and 90 minutes)
	kickoffDelayMinutes :=
		rand.Intn(
			int(config.KickoffDelayMax.Minutes()-config.KickoffDelayMin.Minutes()),
		) + int(config.KickoffDelayMin.Minutes())

	err = pad.scheduler.ScheduleBoostOpportunityStart(ctx, channelID, time.Now().Add(time.Duration(kickoffDelayMinutes)*time.Minute))
	if err != nil {
		return errors.Wrap(err, "error scheduling boost opportunity to start")
	}

	return nil
}

/*
	StartBoostOpportunity is called when Percy receives boost opportunity start event from Destiny
	this starts Boost Opportunity, notify users, and schedules completion event.
	The SQS worker calls ChannelIsAllowed before calling this handler, so we can assume channel eligibility
*/
func (pad *launchPad) StartBoostOpportunity(ctx context.Context, channelID string) (BoostOpportunity, error) {
	logrus.WithFields(logrus.Fields{
		"channelID": channelID,
	}).Info("StartBoostOpportunity is called!")

	config := pad.configsDB.GetBoostOpportunityConfig(channelID)

	// Check if Stream has ongoing Boost Opportunity (see most recent expire time, if one exists)
	latestOpportunity, err := pad.boostsDB.GetLatestBoostOpportunity(ctx, channelID, BoostsDBReadOptions{
		ConsistentRead: true,
	})
	if err != nil {
		return BoostOpportunity{}, errors.Wrap(err, "error getting latest boost opportunity")
	}
	if latestOpportunity != nil && time.Now().Before(latestOpportunity.ExpiresAt.Add(config.Cooldown)) {
		return BoostOpportunity{}, nil
	}

	// Check Stream Online Status, noop if stream is offline
	streamIsOnline, err := pad.channelStatusDB.ChannelIsStreaming(ctx, channelID)
	if err != nil {
		return BoostOpportunity{}, errors.Wrap(err, "error checking stream online status")
	}
	if !streamIsOnline {
		return BoostOpportunity{}, nil
	}

	// Create New Boost Opportunity ID
	boostOpportunityID := uuid.New().String()

	// Calculate Expire Time
	startAt := time.Now()
	expireAt := startAt.Add(config.Duration)

	// Schedule Boost Opportunity Completion
	err = pad.scheduler.ScheduleBoostOpportunityCompletion(ctx, channelID, boostOpportunityID, expireAt)
	if err != nil {
		return BoostOpportunity{}, errors.Wrap(err, "error scheduling boost opportunity completion")
	}

	// Create New Boost Opportunity
	boostOpportunity := BoostOpportunity{
		ID:        boostOpportunityID,
		ChannelID: channelID,
		StartedAt: startAt,
		ExpiresAt: expireAt,
	}
	err = pad.boostsDB.CreateBoostOpportunity(ctx, boostOpportunity)
	if err != nil {
		return BoostOpportunity{}, errors.Wrap(err, "error creating boost opportunity in BoostsDB")
	}

	// Send Boost Opportunity StartEvent to PubSub
	err = pad.publisher.PublishBoostOpportunityStart(ctx, boostOpportunity)
	if err != nil {
		return BoostOpportunity{}, errors.Wrap(err, "error publishing boost opportunity start to pubsub")
	}

	return boostOpportunity, nil
}

/*
	CompleteBoostOpportunity is called when Percy receives boost opportunity completion event from Destiny
	this ends the boost opportunity, and checks if Boost Opportunity was successful
	- if yes, notify users and start the boost w/ booster service
	- if no, notify users
*/
func (pad *launchPad) CompleteBoostOpportunity(ctx context.Context, channelID, opportunityID string) error {
	logrus.WithFields(logrus.Fields{
		"channelID":     channelID,
		"opportunityID": opportunityID,
	}).Info("CompleteBoostOpportunity is called!")

	// Check if Boost Opportunity exists, noop if:
	// - boost opportunity is nil
	// - opportunity id doesn't match
	// - it's been ended by a parallel worker
	latestOpportunity, err := pad.boostsDB.GetLatestBoostOpportunity(ctx, channelID, BoostsDBReadOptions{
		ConsistentRead: true,
	})
	if err != nil {
		return errors.Wrap(err, "error getting latest boost opportunity")
	}
	if latestOpportunity == nil || opportunityID != latestOpportunity.ID || latestOpportunity.EndedAt != nil {
		logrus.WithFields(logrus.Fields{
			"channelID":     channelID,
			"opportunityID": opportunityID,
		}).Info("(Test) CompleteBoostOpportunity noop!")
		return nil
	}

	var boostIdPtr *string
	if latestOpportunity.UnitsContributed > 0 {
		// Create Order in Booster Service
		// if this fails, Opportunity's EndedAt would stay nil (signals unfulfilled status), so retry is safe
		// Once we placed the order, even if the channel goes offline, we can expect booster service to fulfill later.
		boostID, err := pad.boostsLauncher.LaunchBoost(ctx, *latestOpportunity)
		if err != nil {
			return errors.Wrap(err, "error creating order in booster service")
		}
		boostIdPtr = &boostID
	}

	// Update Boost Opportunity to mark it as complete
	// On failure retry we would reorder boost. If this becomes a bigger product, we will add guardrails to prevent duplicate orders
	err = pad.boostsDB.EndLatestBoostOpportunity(ctx, channelID, time.Now(), boostIdPtr)
	if err != nil {
		return errors.Wrap(err, "error ending boost opportunity in BoostsDB")
	}

	// Notify users of Boost Opportunity status via PubSub
	// We fail open since frontend will have guardrail around not receiving completion pubsub
	go func() {
		ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
		defer cancel()

		err = pad.publisher.PublishBoostOpportunityCompletion(ctx, *latestOpportunity)
		if err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{
				"channelID":     channelID,
				"opportunityID": opportunityID,
			}).Error("error publishing boost opportunity completion to pubsub")
		}
	}()

	return nil
}

func (pad *launchPad) AddBoostOpportunityContribution(ctx context.Context, contribution BoostOpportunityContribution) (BoostOpportunity, error) {
	logrus.WithFields(logrus.Fields{
		"channelID": contribution.ChannelID,
		"originID":  contribution.OriginID,
		"userID":    contribution.UserID,
		"timestamp": contribution.Timestamp,
		"units":     contribution.Units,
	}).Info("AddBoostOpportunityContribution is called!")

	if contribution.ChannelID == "" {
		return BoostOpportunity{}, errors.New("channel ID must not be empty")
	}

	if contribution.UserID == "" {
		return BoostOpportunity{}, errors.New("user ID must not be empty")
	}

	if contribution.OriginID == "" {
		return BoostOpportunity{}, errors.New("origin ID must not be empty")
	}

	if contribution.Units <= 0 {
		return BoostOpportunity{}, errors.New("units must be greater than 0")
	}

	latestOpportunity, err := pad.boostsDB.GetLatestBoostOpportunity(ctx, contribution.ChannelID, BoostsDBReadOptions{
		ConsistentRead: true,
	})
	if err != nil {
		return BoostOpportunity{}, errors.Wrap(err, "error getting latest boost opportunity")
	}

	if latestOpportunity == nil {
		return BoostOpportunity{}, errors.New("no boost opportunity available to contribute to")
	}

	if !latestOpportunity.IsActive() {
		return BoostOpportunity{}, errors.New("boost opportunity is no longer active to contribute to")
	}

	// Check if this contribution has already been recorded
	existingContribution, err := pad.boostsDB.GetBoostOpportunityContribution(ctx, latestOpportunity.ID, contribution.OriginID, BoostsDBReadOptions{
		ConsistentRead: true,
	})
	if err != nil {
		return BoostOpportunity{}, errors.Wrap(err, "failed to check existing contribution")
	}

	// No-op if the contribution has already been recorded
	if existingContribution != nil {
		return *latestOpportunity, nil
	}

	return pad.boostsDB.AddBoostOpportunityContribution(ctx, latestOpportunity.ID, contribution)
}

func (pad *launchPad) IsUserAllowedToContribute(ctx context.Context, userID, channelID string, useConsistentReads bool) (bool, error) {
	if userID == "" {
		return false, errors.New("user ID must not be empty")
	}

	if channelID == "" {
		return false, errors.New("channel ID must not be empty")
	}

	// reject channels that are not in the allow list
	if !pad.configsDB.IsChannelAllowed(channelID) {
		return false, nil
	}

	boostOpportunity, err := pad.boostsDB.GetLatestBoostOpportunity(ctx, channelID, BoostsDBReadOptions{
		ConsistentRead: useConsistentReads,
	})
	if err != nil {
		// fall back to false on failed boost opportunity read
		logrus.WithError(err).WithFields(logrus.Fields{
			"channelID": channelID,
			"userID":    userID,
		}).Warn("Failed to get latest boost opportunity to decide if user can contribute to boost opportunity")
		//nolint:nilerr
		return false, nil
	}

	if boostOpportunity == nil || !boostOpportunity.IsActive() {
		return false, nil
	}

	// reject user who are banned in the channel
	isBanned, err := pad.userProfileDB.IsChatBanned(ctx, userID, channelID)
	if err != nil {
		// fall back to false on failed ban status check
		logrus.WithError(err).WithFields(logrus.Fields{
			"channelID": channelID,
			"userID":    userID,
		}).Warn("Failed to get user's channel ban status to decide if user can contribute to boost opportunity")
		//nolint:nilerr
		return false, nil
	}

	return !isBanned, nil
}

func (pad *launchPad) SendBoostOpportunityContributionUserNotice(ctx context.Context, contribution BoostOpportunityContribution) error {
	if contribution.ChannelID == "" {
		return errors.New("channel ID must not  be empty")
	}

	if contribution.UserID == "" {
		return errors.New("user ID must not  be empty")
	}

	if contribution.Units <= 0 {
		return errors.New("contribution units must be greater than 0")
	}

	channelIDInt, err := strconv.Atoi(contribution.ChannelID)
	if err != nil {
		return errors.Wrap(err, "failed to convert channel ID to int")
	}

	userIDInt, err := strconv.Atoi(contribution.UserID)
	if err != nil {
		return errors.Wrap(err, "failed to convert user ID to int")
	}

	return pad.userProfileDB.SendUserNotice(ctx, UserNoticeParams{
		TargetChannelID: channelIDInt,
		SenderUserID:    userIDInt,
		MessageID:       celebrationUserNoticeID,
		MessageParams: []UserNoticeMessageParams{
			// re-using the intensity field from the existing schema to encode impressions
			{
				Key:   celebrationUserNoticeIntensityKey,
				Value: strconv.Itoa(contribution.Impressions()),
			},
		},
		DefaultSystemBody: "contribution notification",
	})
}

func (pad *launchPad) FulfillContribution(ctx context.Context, contribution BoostOpportunityContribution) error {
	return pad.stateMachine.StartExecutionBoostOpportunityContribution(ctx, contribution)
}

func (pad *launchPad) RefundContribution(ctx context.Context, contribution BoostOpportunityContribution) error {
	if contribution.OriginID == "" {
		return errors.New("contribution origin ID must not be empty")
	}

	return pad.paymentsHandler.RefundPurchaseOrder(ctx, contribution.OriginID, boostOpportunityContributionRefundReason)
}
