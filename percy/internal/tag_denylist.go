package percy

var excludedChannelsSet map[string]struct{}

func initExcludedChannelSet() {
	excludedChannels := []string{
		"266397851", // qa_egg_partner
		"267885347", // qa_NicodemusRat_partner
		"265716088", // qa_bonta_partner
		"267893713", // qa_xanderwolf_partner
		"270954519", // qa_sokihalisan_partner
		"265946585", // qa_slaye_partner
		"267887052", // qa_walrus_partner
		"265912600", // qa_flomic_partner
		"402659066", // qa_frances_partner
		"408378849", // qa_misscoded_partner
		"408380203", // qa_weaver_partner
		"408380599", // qa_gummyace_partner
		"499158006", // qa_anarion_partner
		"531221714", // qa_hpokuan_partner
		"542568395", // qa_gr33nh3ad_partner
		"560433187", // qatuttle28affiliate
		"116076154", // qa_bits_partner
	}
	excludedChannelsSet = make(map[string]struct{})
	for _, excludedChannel := range excludedChannels {
		excludedChannelsSet[excludedChannel] = struct{}{}
	}
}

// Accounts to be excluded for HypeTrain Tags (such as Test Accounts)
func shouldNotTagChannel(channelID string) bool {
	if excludedChannelsSet == nil {
		initExcludedChannelSet()
	}
	_, exists := excludedChannelsSet[channelID]
	return exists
}
