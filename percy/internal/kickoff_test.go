package percy

import (
	"fmt"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

func TestHelpers_nearKickoff(t *testing.T) {
	const (
		easyKickoffThreshold   = 3
		mediumKickoffThreshold = 5
		insaneKickoffThreshold = 25
	)

	Convey("Given some helper function", t, func() {
		Convey("for easy hype trains should only trigger when at 2/3's", func() {
			So(nearKickoff(0, easyKickoffThreshold), ShouldBeFalse)
			So(nearKickoff(1, easyKickoffThreshold), ShouldBeFalse)
			So(nearKickoff(2, easyKickoffThreshold), ShouldBeTrue)
		})

		Convey("for 4 threshold hype trains should only trigger when at 2/3's", func() {
			So(nearKickoff(0, 4), ShouldBeFalse)
			So(nearKickoff(1, 4), ShouldBeFalse)
			So(nearKickoff(2, 4), ShouldBeFalse)
			So(nearKickoff(3, 4), ShouldBeTrue)
			So(nearKickoff(4, 4), ShouldBeTrue)
		})

		Convey("for medium hype trains should only trigger when at 1 event left", func() {
			So(nearKickoff(0, mediumKickoffThreshold), ShouldBeFalse)
			So(nearKickoff(1, mediumKickoffThreshold), ShouldBeFalse)
			So(nearKickoff(2, mediumKickoffThreshold), ShouldBeFalse)
			So(nearKickoff(3, mediumKickoffThreshold), ShouldBeFalse)
			So(nearKickoff(4, mediumKickoffThreshold), ShouldBeTrue)
			So(nearKickoff(5, mediumKickoffThreshold), ShouldBeTrue)
		})

		Convey("for insane hype trains should only trigger when at 1-2 events left", func() {
			So(nearKickoff(0, insaneKickoffThreshold), ShouldBeFalse)
			So(nearKickoff(5, insaneKickoffThreshold), ShouldBeFalse)
			So(nearKickoff(10, insaneKickoffThreshold), ShouldBeFalse)
			So(nearKickoff(20, insaneKickoffThreshold), ShouldBeFalse)
			So(nearKickoff(22, insaneKickoffThreshold), ShouldBeFalse)
			So(nearKickoff(23, insaneKickoffThreshold), ShouldBeTrue)
			So(nearKickoff(24, insaneKickoffThreshold), ShouldBeTrue)
		})

		Convey("if above threshold still works and doesn't error", func() {
			So(nearKickoff(5, easyKickoffThreshold), ShouldBeTrue)
			So(nearKickoff(7, mediumKickoffThreshold), ShouldBeTrue)
			So(nearKickoff(400000, insaneKickoffThreshold), ShouldBeTrue)
		})
	})
}

func TestHelpers_generateEventsExpirationMap(t *testing.T) {
	Convey("Given some hype train config", t, func() {
		config := HypeTrainConfig{
			KickoffConfig: KickoffConfig{
				Duration:    parseDuration("5m"),
				MinPoints:   1,
				NumOfEvents: 3,
			},
			ParticipationConversionRates: map[ParticipationIdentifier]ParticipationPoint{
				{Source: SubsSource, Action: Tier1SubAction}: ParticipationPoint(500),
			},
		}

		Convey("when we have no participations", func() {
			result, _ := generateEventsExpirationMap(config, []Participation{})
			So(result, ShouldBeEmpty)
		})

		Convey("when we have two participations and numEvents of three", func() {
			participations := []Participation{
				makeTestParticipant("user1", time.Now().Add(-3*time.Minute)),
				makeTestParticipant("user2", time.Now().Add(-1*time.Minute)),
			}

			actual, returnedParticipants := generateEventsExpirationMap(config, participations)
			So(actual, ShouldHaveLength, 1)
			So(returnedParticipants, ShouldHaveLength, 2)
			So(actual[1], ShouldHaveDurationBetween, parseDuration("119s"), parseDuration("120s"))
		})

		Convey("with one user who spammed a bunch of events over a range", func() {
			participations := []Participation{
				makeTestParticipant("user0", time.Now().Add(-4*time.Minute)),
				makeTestParticipant("user0", time.Now().Add(-1*time.Minute)),
				makeTestParticipant("user1", time.Now().Add(-3*time.Minute)),
				makeTestParticipant("user0", time.Now().Add(-1*time.Minute)),
			}

			expirationsMap, returnedParticipants := generateEventsExpirationMap(config, participations)
			So(expirationsMap, ShouldHaveLength, 1)
			So(returnedParticipants, ShouldHaveLength, 2)
			So(expirationsMap[1], ShouldHaveDurationBetween, parseDuration("1m59s"), parseDuration("2m"))
		})

		Convey("With a hype train config that is more difficult", func() {
			config.KickoffConfig.NumOfEvents = 6

			participations := []Participation{
				makeTestParticipant("user0", time.Now().Add(-4*time.Minute)),
				makeTestParticipant("user0", time.Now().Add(-1*time.Minute)),
				makeTestParticipant("user1", time.Now().Add(-3*time.Minute)),
				makeTestParticipant("user0", time.Now().Add(-1*time.Minute)),

				makeTestParticipant("user2", time.Now().Add(-1*time.Minute)),
				makeTestParticipant("user3", time.Now().Add(-2*time.Minute)),
			}

			expirationsMap, returnedParticipants := generateEventsExpirationMap(config, participations)
			So(expirationsMap, ShouldHaveLength, 2)
			So(returnedParticipants, ShouldHaveLength, 4)
			So(expirationsMap[2], ShouldHaveDurationBetween, parseDuration("1m59s"), parseDuration("2m"))
			So(expirationsMap[1], ShouldHaveDurationBetween, parseDuration("2m59s"), parseDuration("3m"))
		})

		Convey("numEvents is ten", func() {
			config.KickoffConfig.NumOfEvents = 10

			Convey("when we seven participations", func() {
				participations := []Participation{
					makeTestParticipant("user0", time.Now().Add(-1*time.Minute)),
					makeTestParticipant("user1", time.Now().Add(-3*time.Minute)),
					makeTestParticipant("user2", time.Now().Add(-1*time.Minute)),
					makeTestParticipant("user3", time.Now().Add(-4*time.Minute)),
					makeTestParticipant("user4", time.Now().Add(-2*time.Minute)),
					makeTestParticipant("user5", time.Now().Add(-1*time.Minute)),
					makeTestParticipant("user6", time.Now().Add(-1*time.Minute)),
				}

				actual, returnedParticipants := generateEventsExpirationMap(config, participations)
				So(actual, ShouldHaveLength, 0)
				So(returnedParticipants, ShouldHaveLength, 7)
			})

			Convey("when we eight participations", func() {
				participations := []Participation{
					makeTestParticipant("user0", time.Now().Add(-1*time.Minute)),
					makeTestParticipant("user1", time.Now().Add(-3*time.Minute)),
					makeTestParticipant("user2", time.Now().Add(-1*time.Minute)),
					makeTestParticipant("user3", time.Now().Add(-4*time.Minute)),
					makeTestParticipant("user4", time.Now().Add(-2*time.Minute)),
					makeTestParticipant("user5", time.Now().Add(-1*time.Minute)),
					makeTestParticipant("user6", time.Now().Add(-1*time.Minute)),
					makeTestParticipant("user7", time.Now().Add(-3*time.Minute)),
				}

				actual, returnedParticipants := generateEventsExpirationMap(config, participations)
				So(actual, ShouldHaveLength, 2)
				So(actual[1], ShouldBeGreaterThanOrEqualTo, actual[2])
				So(actual[2], ShouldHaveDurationBetween, parseDuration("59s"), parseDuration("60s"))
				So(actual[1], ShouldHaveDurationBetween, parseDuration("119s"), parseDuration("120s"))
				So(returnedParticipants, ShouldHaveLength, 8)
			})

			Convey("when we nine participations", func() {
				participations := []Participation{
					makeTestParticipant("user0", time.Now().Add(-1*time.Minute)),
					makeTestParticipant("user1", time.Now().Add(-3*time.Minute)),
					makeTestParticipant("user2", time.Now().Add(-1*time.Minute)),
					makeTestParticipant("user3", time.Now().Add(-4*time.Minute)),
					makeTestParticipant("user4", time.Now().Add(-2*time.Minute)),
					makeTestParticipant("user5", time.Now().Add(-1*time.Minute)),
					makeTestParticipant("user6", time.Now().Add(-1*time.Minute)),
					makeTestParticipant("user7", time.Now().Add(-3*time.Minute)),
					makeTestParticipant("user8", time.Now().Add(-3*time.Minute)),
				}

				actual, returnedParticipants := generateEventsExpirationMap(config, participations)
				So(actual, ShouldHaveLength, 1)
				So(actual[1], ShouldHaveDurationBetween, parseDuration("59s"), parseDuration("60s"))
				So(returnedParticipants, ShouldHaveLength, 9)
			})
		})
	})
}

func makeTestParticipant(userId string, timeStamp time.Time) Participation {
	return Participation{
		ChannelID: "foo",
		User:      NewUser(userId),
		ParticipationIdentifier: ParticipationIdentifier{
			Source: SubsSource,
			Action: Tier1SubAction,
		},
		Quantity:  1,
		Timestamp: timeStamp,
	}
}

func parseDuration(s string) time.Duration {
	duration, _ := time.ParseDuration(s)
	return duration
}

func ShouldHaveDurationBetween(actual interface{}, expected ...interface{}) string {
	actualDuration, firstOk := actual.(time.Duration)
	expectedLowerBound, secondOk := expected[0].(time.Duration)
	expectedUpperBound, thirdOk := expected[1].(time.Duration)

	if !firstOk || !secondOk || !thirdOk {
		return "types are wrong, should all be durations"
	}

	if expectedLowerBound >= expectedUpperBound {
		return "lower bound should be the first provided number"
	}

	if expectedLowerBound <= actualDuration && actualDuration <= expectedUpperBound {
		return ""
	}

	return fmt.Sprintf("duration (%s) is not between %s and %s", actualDuration, expectedLowerBound, expectedUpperBound)
}
