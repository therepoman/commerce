package badges

import (
	"context"
	"net/http"
	"strconv"

	"code.justin.tv/chat/badges/app/models"
	chat_badges "code.justin.tv/chat/badges/client"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/wrapper"
	"code.justin.tv/foundation/twitchclient"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
)

const (
	serviceName    = "badges"
	defaultRetry   = 1
	defaultTimeout = 1000
)

type Client struct {
	Badges chat_badges.Client
}

func NewClient(host string, statter statsd.Statter, socksAddr string) (*Client, error) {
	client, err := chat_badges.NewClient(twitchclient.ClientConf{
		Host:           host,
		Stats:          statter,
		StatNamePrefix: serviceName,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(socksAddr),
			wrapper.NewRetryRoundTripWrapper(statter, serviceName, defaultRetry),
			wrapper.NewHystrixRoundTripWrapper(serviceName, defaultTimeout),
		},
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: 200,
		},
	})
	if err != nil {
		return nil, err
	}

	return &Client{
		Badges: client,
	}, nil
}

func (c *Client) GetSelectedHypeTrainBadge(ctx context.Context, userID, channelID string) (*percy.Badge, error) {
	resp, err := c.Badges.GetSelectedChannelBadge(ctx, userID, channelID, &twitchclient.ReqOpts{})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get selected badge from badges service")
	}

	if resp == nil || resp.BadgeSetID != models.BadgeSetHypeTrain {
		return nil, nil
	}

	val, err := strconv.ParseInt(resp.BadgeSetVersion, 10, 64)
	if err != nil {
		return nil, errors.Wrap(err, "error converting hype train badge from badges service")
	}

	badge := BadgesServiceVersionToPercyBadge(val)
	if badge == percy.UnknownBadge {
		return nil, errors.Errorf("got unknown badge set in badges service for hype train: %s", resp.BadgeSetVersion)
	}

	return &badge, nil
}

func (c *Client) SetSelectedHypeTrainBadge(ctx context.Context, userID, channelID string, badge percy.Badge) error {
	badgeSetVersion := PercyBadgeToBadgesServiceVersion(badge)
	if badgeSetVersion < 1 {
		return errors.New("invalid badge to set in badges service")
	}

	val := strconv.FormatInt(badgeSetVersion, 10)
	isSet, err := c.Badges.SelectChannelBadge(ctx, userID, channelID, models.SelectBadgeParams{
		BadgeSetID:      models.BadgeSetHypeTrain,
		BadgeSetVersion: &val,
	}, &twitchclient.ReqOpts{})
	if err != nil {
		return errors.Wrap(err, "failed to select channel badge in badges service")
	}

	if !isSet {
		return errors.New("do not have permission to set badge in badges service")
	}

	return nil
}

func (c *Client) RemoveSelectedHypeTrainBadge(ctx context.Context, userID, channelID string) error {
	err := c.Badges.DeselectChannelBadge(ctx, userID, channelID, &twitchclient.ReqOpts{})
	if err != nil {
		return errors.Wrap(err, "failed to deselect channel badge in badges service")
	}

	return nil
}

func (c *Client) GetBadgeDisplayInfo(ctx context.Context, channelID string, badges []percy.Badge) (map[percy.Badge]string, error) {
	badgeIDs := make([]models.BadgeID, 0)
	for _, badge := range badges {
		version := PercyBadgeToBadgesServiceVersion(badge)
		if version > 0 {
			badgeIDs = append(badgeIDs, models.BadgeID{SetID: models.BadgeSetHypeTrain, Version: strconv.FormatInt(version, 10), UserID: channelID})
		}
	}
	resp, err := c.Badges.BulkGetDisplayInfo(ctx, badgeIDs, "EN", true, &twitchclient.ReqOpts{})
	if err != nil {
		return nil, errors.Wrap(err, "failed to fetch badge display info")
	}

	badgeDisplayInfo := map[percy.Badge]string{}
	for _, displayInfo := range resp {
		if displayInfo != nil {
			badgeVersion, err := strconv.ParseInt(displayInfo.ID.Version, 10, 64)
			if err != nil {
				return nil, errors.Errorf("got back non numeric badge version from badges service: %s", displayInfo.ID.Version)
			}
			badge := BadgesServiceVersionToPercyBadge(badgeVersion)
			if badge != percy.UnknownBadge && displayInfo.Data.ImageURL2x != nil {
				badgeDisplayInfo[badge] = *displayInfo.Data.ImageURL2x
			}
		}
	}

	return badgeDisplayInfo, nil
}

func BadgesServiceVersionToPercyBadge(badgeVersion int64) percy.Badge {
	switch badgeVersion {
	case 1:
		return percy.CurrentConductorBadge
	case 2:
		return percy.FormerConductorBadge
	default:
		return percy.UnknownBadge
	}
}

func PercyBadgeToBadgesServiceVersion(badge percy.Badge) int64 {
	switch badge {
	case percy.CurrentConductorBadge:
		return 1
	case percy.FormerConductorBadge:
		return 2
	case percy.UnknownBadge:
		fallthrough
	default:
		return 0
	}
}
