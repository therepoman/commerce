package badges_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/chat/badges/app/models"
	chat_badges "code.justin.tv/chat/badges/client"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/badges"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
)

func TestClient_GetSelectedHypeTrainBadge(t *testing.T) {
	Convey("given a badges client", t, func() {
		mockClient := &chat_badges.MockClient{}

		c := badges.Client{
			Badges: mockClient,
		}

		ctx := context.Background()
		userID := "123123123"
		channelID := "321321321"
		badge := &models.Badge{
			BadgeSetID:      models.BadgeSetHypeTrain,
			BadgeSetVersion: "1",
		}

		Convey("when we return a hype train badge", func() {
			mockClient.On("GetSelectedChannelBadge", ctx, userID, channelID, mock.Anything).Return(badge, nil).Once()

			resp, err := c.GetSelectedHypeTrainBadge(ctx, userID, channelID)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(*resp, ShouldEqual, percy.CurrentConductorBadge)
			mockClient.AssertExpectations(t)
		})

		Convey("when we return nil badge", func() {
			Convey("when there is no badge returned", func() {
				mockClient.On("GetSelectedChannelBadge", ctx, userID, channelID, mock.Anything).Return(nil, nil).Once()
			})

			Convey("when the badge returned is not a hype train badge", func() {
				badge.BadgeSetID = models.BadgeSetBits
				mockClient.On("GetSelectedChannelBadge", ctx, userID, channelID, mock.Anything).Return(badge, nil).Once()
			})

			resp, err := c.GetSelectedHypeTrainBadge(ctx, userID, channelID)
			So(err, ShouldBeNil)
			So(resp, ShouldBeNil)
			mockClient.AssertExpectations(t)
		})

		Convey("when we return an error", func() {
			Convey("when badges service returns an error", func() {
				mockClient.On("GetSelectedChannelBadge", ctx, userID, channelID, mock.Anything).Return(nil, errors.New("WALRUS STRIKE")).Once()
			})

			Convey("when badges service returns a badge set version we do not expect", func() {
				badge.BadgeSetVersion = "WALRUS"
				mockClient.On("GetSelectedChannelBadge", ctx, userID, channelID, mock.Anything).Return(badge, nil).Once()
			})

			Convey("when badges service returns a badge set version that is not a valid badge number", func() {
				badge.BadgeSetVersion = "420"
				mockClient.On("GetSelectedChannelBadge", ctx, userID, channelID, mock.Anything).Return(badge, nil).Once()
			})

			_, err := c.GetSelectedHypeTrainBadge(ctx, userID, channelID)
			So(err, ShouldNotBeNil)
			mockClient.AssertExpectations(t)
		})
	})
}

func TestClient_SetSelectedHypeTrainBadge(t *testing.T) {
	Convey("given a badges client", t, func() {
		mockClient := &chat_badges.MockClient{}

		c := badges.Client{
			Badges: mockClient,
		}

		ctx := context.Background()
		userID := "123123123"
		channelID := "321321321"
		badge := percy.CurrentConductorBadge

		Convey("when we return nil", func() {
			mockClient.On("SelectChannelBadge", ctx, userID, channelID, mock.Anything, mock.Anything).Return(true, nil).Once()

			err := c.SetSelectedHypeTrainBadge(ctx, userID, channelID, badge)
			So(err, ShouldBeNil)
			mockClient.AssertExpectations(t)
		})

		Convey("when we return an error", func() {
			Convey("when badges set version is invalid", func() {
				badge = percy.UnknownBadge
			})

			Convey("when badges service returns an error", func() {
				mockClient.On("SelectChannelBadge", ctx, userID, channelID, mock.Anything, mock.Anything).Return(false, errors.New("WALRUS STRIKE")).Once()
			})

			Convey("when badges service returns a that we did not have permissions to set the badge", func() {
				mockClient.On("SelectChannelBadge", ctx, userID, channelID, mock.Anything, mock.Anything).Return(false, nil).Once()
			})

			err := c.SetSelectedHypeTrainBadge(ctx, userID, channelID, badge)
			So(err, ShouldNotBeNil)
			mockClient.AssertExpectations(t)
		})
	})
}

func TestClient_RemoveSelectedHypeTrainBadge(t *testing.T) {
	Convey("given a badges client", t, func() {
		mockClient := &chat_badges.MockClient{}

		c := badges.Client{
			Badges: mockClient,
		}

		ctx := context.Background()
		userID := "123123123"
		channelID := "321321321"

		Convey("when we return nil", func() {
			mockClient.On("DeselectChannelBadge", ctx, userID, channelID, mock.Anything).Return(nil).Once()

			err := c.RemoveSelectedHypeTrainBadge(ctx, userID, channelID)
			So(err, ShouldBeNil)
			mockClient.AssertExpectations(t)
		})

		Convey("when we return an error", func() {
			mockClient.On("DeselectChannelBadge", ctx, userID, channelID, mock.Anything).Return(errors.New("WALRUS STRIKE")).Once()

			err := c.RemoveSelectedHypeTrainBadge(ctx, userID, channelID)
			So(err, ShouldNotBeNil)
			mockClient.AssertExpectations(t)
		})
	})
}
