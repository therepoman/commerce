package eventbus

import (
	"context"
	"errors"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/metrics"
	"code.justin.tv/commerce/percy/internal/tests"
	eventbus "code.justin.tv/eventbus/client"
	. "github.com/smartystreets/goconvey/convey"
)

func TestWorkerConfig_HandleBitsUse(t *testing.T) {
	Convey("Given a valid WorkerConfig", t, func() {
		engine := &internalfakes.FakeEngine{}

		config := WorkerConfig{
			Engine:   engine,
			Mux:      eventbus.NewMux(),
			QueueURL: "some_url",
			Statter:  &metrics.NoopStatter{},
		}

		ctx := context.Background()

		validHypeTrain := tests.ValidHypeTrain()
		validHypeTrainConfig := tests.ValidHypeTrainConfig()
		validEventHeader := tests.ValidEventHeader()
		validBitsUseEvent := tests.ValidBitsUseEvent()
		invalidBitsUseEvent := tests.InvalidBitsUseEvent()
		inDevelopmentBitsUseEvent := tests.InDevelopmentBitsUseEvent()

		Convey("when a valid Bits Use event comes in ", func() {
			engine.GetHypeTrainConfigReturns(validHypeTrainConfig, nil)
			engine.GetMostRecentHypeTrainReturns(&validHypeTrain, nil)

			err := config.HandleBitsUse(ctx, validEventHeader, validBitsUseEvent)
			So(err, ShouldBeNil)
		})

		Convey("when an invalid event header for a Bits Use event comes in ", func() {
			engine.GetHypeTrainConfigReturns(validHypeTrainConfig, nil)
			engine.GetMostRecentHypeTrainReturns(&validHypeTrain, nil)

			err := config.HandleBitsUse(ctx, nil, validBitsUseEvent)
			So(err, ShouldNotBeNil)
		})

		Convey("when an invalid event Bits use event comes in", func() {
			engine.GetHypeTrainConfigReturns(validHypeTrainConfig, nil)
			engine.GetMostRecentHypeTrainReturns(&validHypeTrain, nil)

			err := config.HandleBitsUse(ctx, nil, invalidBitsUseEvent)
			So(err, ShouldNotBeNil)
		})

		Convey("when a transaction involving an in development extension comes in", func() {
			engine.GetHypeTrainConfigReturns(percy.HypeTrainConfig{}, errors.New("we should never reach this"))
			engine.GetMostRecentHypeTrainReturns(nil, errors.New("we should never reach this"))

			err := config.HandleBitsUse(ctx, validEventHeader, inDevelopmentBitsUseEvent)
			So(err, ShouldBeNil)
		})
	})
}

func TestWorkerConfig_CheerCreate(t *testing.T) {
	Convey("Given a valid WorkerConfig", t, func() {
		engine := &internalfakes.FakeEngine{}

		config := WorkerConfig{
			Engine:   engine,
			Mux:      eventbus.NewMux(),
			QueueURL: "some_url",
			Statter:  &metrics.NoopStatter{},
		}

		ctx := context.Background()

		validHypeTrain := tests.ValidHypeTrain()
		validHypeTrainConfig := tests.ValidHypeTrainConfig()
		validEventHeader := tests.ValidEventHeader()
		validCheerCreateEvent := tests.ValidCheerCreateEvent()
		invalidCheerCreateEvent := tests.InvalidCheerCreateEvent()

		Convey("when a valid Bits Use event comes in ", func() {
			engine.GetHypeTrainConfigReturns(validHypeTrainConfig, nil)
			engine.GetMostRecentHypeTrainReturns(&validHypeTrain, nil)

			err := config.HandleCheerCreate(ctx, validEventHeader, validCheerCreateEvent)
			So(err, ShouldBeNil)
		})

		Convey("when an invalid event header for a Cheer Create event comes in ", func() {
			engine.GetHypeTrainConfigReturns(validHypeTrainConfig, nil)
			engine.GetMostRecentHypeTrainReturns(&validHypeTrain, nil)

			err := config.HandleCheerCreate(ctx, nil, validCheerCreateEvent)
			So(err, ShouldNotBeNil)
		})

		Convey("when an invalid event Cheer Create event comes in", func() {
			engine.GetHypeTrainConfigReturns(validHypeTrainConfig, nil)
			engine.GetMostRecentHypeTrainReturns(&validHypeTrain, nil)

			err := config.HandleCheerCreate(ctx, nil, invalidCheerCreateEvent)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestWorkerConfig_HandleUserGiftSubscription(t *testing.T) {
	Convey("Given a valid WorkerConfig", t, func() {
		engine := &internalfakes.FakeEngine{}

		config := WorkerConfig{
			Engine:   engine,
			Mux:      eventbus.NewMux(),
			QueueURL: "some_url",
			Statter:  &metrics.NoopStatter{},
		}

		ctx := context.Background()

		validHypeTrain := tests.ValidHypeTrain()
		validHypeTrainConfig := tests.ValidHypeTrainConfig()
		validEventHeader := tests.ValidEventHeader()
		validGiftSubscriptionEvent := tests.ValidUserGiftSubscriptionEvent()
		invalidGiftSubscriptionEvent := tests.InvalidUserGiftSubscriptionEvent()

		Convey("when a valid Gifted Subscription event comes in ", func() {
			engine.GetHypeTrainConfigReturns(validHypeTrainConfig, nil)
			engine.GetMostRecentHypeTrainReturns(&validHypeTrain, nil)

			err := config.HandleUserGiftSubscription(ctx, validEventHeader, validGiftSubscriptionEvent)
			So(err, ShouldBeNil)
		})

		Convey("when an invalid event header for a Gifted Subscription event comes in ", func() {
			engine.GetHypeTrainConfigReturns(validHypeTrainConfig, nil)
			engine.GetMostRecentHypeTrainReturns(&validHypeTrain, nil)

			err := config.HandleUserGiftSubscription(ctx, nil, validGiftSubscriptionEvent)
			So(err, ShouldNotBeNil)
		})

		Convey("when an invalid event Gifted Subscription event comes in", func() {
			engine.GetHypeTrainConfigReturns(validHypeTrainConfig, nil)
			engine.GetMostRecentHypeTrainReturns(&validHypeTrain, nil)

			err := config.HandleUserGiftSubscription(ctx, nil, invalidGiftSubscriptionEvent)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestWorkerConfig_HandleUserSubscriptionUserNotice(t *testing.T) {
	Convey("Given a valid WorkerConfig", t, func() {
		engine := &internalfakes.FakeEngine{}

		config := WorkerConfig{
			Engine:   engine,
			Mux:      eventbus.NewMux(),
			QueueURL: "some_url",
			Statter:  &metrics.NoopStatter{},
		}

		ctx := context.Background()

		validHypeTrain := tests.ValidHypeTrain()
		validHypeTrainConfig := tests.ValidHypeTrainConfig()
		validEventHeader := tests.ValidEventHeader()
		validUserNoticeEvent := tests.ValidUserSubscriptionUserNoticeEvent()
		invalidUserNoticeEvent := tests.InvalidUserSubscriptionUserNoticeEvent()

		Convey("when a valid User Subscription User Notice event comes in ", func() {
			engine.GetHypeTrainConfigReturns(validHypeTrainConfig, nil)
			engine.GetMostRecentHypeTrainReturns(&validHypeTrain, nil)

			err := config.HandleUserSubscriptionUserNotice(ctx, validEventHeader, validUserNoticeEvent)
			So(err, ShouldBeNil)
		})

		Convey("when an invalid event header for a User Subscription User Notice event comes in ", func() {
			engine.GetHypeTrainConfigReturns(validHypeTrainConfig, nil)
			engine.GetMostRecentHypeTrainReturns(&validHypeTrain, nil)

			err := config.HandleUserSubscriptionUserNotice(ctx, nil, validUserNoticeEvent)
			So(err, ShouldNotBeNil)
		})

		Convey("when an invalid event User Subscription User Notice event comes in", func() {
			engine.GetHypeTrainConfigReturns(validHypeTrainConfig, nil)
			engine.GetMostRecentHypeTrainReturns(&validHypeTrain, nil)

			err := config.HandleUserSubscriptionUserNotice(ctx, nil, invalidUserNoticeEvent)
			So(err, ShouldNotBeNil)
		})
	})
}
