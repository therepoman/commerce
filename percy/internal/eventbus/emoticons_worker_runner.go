package eventbus

import (
	"context"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	eventbus "code.justin.tv/eventbus/client"
	subscriber "code.justin.tv/eventbus/client/subscriber/sqsclient"
	"code.justin.tv/eventbus/schema/pkg/emoticon"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/sirupsen/logrus"
)

const (
	makoEventBusHandlerLatencyMetric = "mako-eventbus-handler-latency"
	makoEventBusHandlerSuccessMetric = "mako-eventbus-handler-success"
	makoEventBusHandlerFailureMetric = "mako-eventbus-handler-failure"
)

type EmoticonsWorkerConfig struct {
	Engine   percy.Engine
	Mux      *eventbus.Mux
	QueueURL string
	Statter  statsd.Statter
}

func NewEmoticonsWorkerRunner(config EmoticonsWorkerConfig, region string) (*subscriber.SQSClient, error) {
	config.registerHandlers()

	awsSession, err := session.NewSession(&aws.Config{
		Region: aws.String(region),
	})
	if err != nil {
		return nil, err
	}

	client, err := subscriber.New(subscriber.Config{
		Session:    awsSession,
		Dispatcher: config.Mux.Dispatcher(),
		QueueURL:   config.QueueURL,
		FineTuning: subscriber.FineTuning{
			DeliverChannelDepth:    0,
			AckChannelDepth:        0,
			ShutdownCancelRequests: time.Second * 1,
		},
		HookCallback: logErrorCallback,
	})
	if err != nil {
		return nil, err
	}

	return client, nil
}

func (w *EmoticonsWorkerConfig) HandleEmoticonDelete(ctx context.Context, h *eventbus.Header, event *emoticon.EmoticonDelete) error {
	start := time.Now()
	defer func() {
		_ = w.Statter.TimingDuration(makoEventBusHandlerLatencyMetric, time.Since(start), 1.0)
	}()

	if event == nil || event.GetEmoticon() == nil || !isEventValidDelete(event) {
		go func() {
			_ = w.Statter.Inc(makoEventBusHandlerSuccessMetric, 1, 1.0)
		}()
		return nil
	}

	channelID := event.GetEmoticon().GetOwnerId()

	hypeTrainConfig, err := w.Engine.GetHypeTrainConfig(ctx, channelID)
	if err != nil {
		logrus.WithError(err).Error("failed to fetch hype train config from emoticon delete event")
		go func() {
			_ = w.Statter.Inc(makoEventBusHandlerFailureMetric, 1, 1.0)
		}()
		return err
	}

	if hypeTrainConfig.CalloutEmoteID == event.GetEmoticon().GetId() {
		_, err := w.Engine.UnsetHypeTrainConfig(ctx, channelID, percy.HypeTrainConfigUnset{CalloutEmoteID: true})
		if err != nil {
			logrus.WithError(err).Error("failed to delete callout emote in hype train config from emoticon delete event")
			go func() {
				_ = w.Statter.Inc(makoEventBusHandlerFailureMetric, 1, 1.0)
			}()
			return err
		}
	}

	go func() {
		_ = w.Statter.Inc(makoEventBusHandlerSuccessMetric, 1, 1.0)
	}()

	return nil
}

func (w *EmoticonsWorkerConfig) registerHandlers() {
	emoticon.RegisterDeleteHandler(w.Mux, w.HandleEmoticonDelete)
}

func isEventValidDelete(event *emoticon.EmoticonDelete) bool {
	return event.GetState() == emoticon.State_STATE_MODERATED ||
		event.GetState() == emoticon.State_STATE_ARCHIVED ||
		event.GetState() == emoticon.State_STATE_INACTIVE
}
