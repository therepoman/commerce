package eventbus

import (
	"context"
	"errors"
	"strings"
	"time"

	"code.justin.tv/commerce/gogogadget/math"
	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	eventbus "code.justin.tv/eventbus/client"
	subscriber "code.justin.tv/eventbus/client/subscriber/sqsclient"
	"code.justin.tv/eventbus/schema/pkg/cheer"
	"code.justin.tv/eventbus/schema/pkg/entitlement_badge"
	"code.justin.tv/eventbus/schema/pkg/user_gift_subscription_user"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/cactus/go-statsd-client/statsd"
)

const (
	eventBusCelebrationHandlerLatencyMetric           = "eventbus-celebration-handler-latency"
	eventBusCelebrationHandlerSuccessMetric           = "eventbus-celebration-handler-success"
	eventBusCelebrationHandlerFailureMetric           = "eventbus-celebration-handler-failure"
	eventBusCelebrationHandlerValidationFailureMetric = "eventbus-celebration-handler-validation-failure"

	celebrationHandlerTimeout = 3 * time.Second

	FoundersBadgePrefix = "founder-"
)

type CelebrationsWorkerConfig struct {
	Engine   percy.CelebrationsEngine
	Mux      *eventbus.Mux
	QueueURL string
	Statter  statsd.Statter
}

func NewCelebrationsWorkerRunner(config CelebrationsWorkerConfig, region string) (*subscriber.SQSClient, error) {
	config.registerHandlers()

	awsSession, err := session.NewSession(&aws.Config{
		Region: aws.String(region),
	})
	if err != nil {
		return nil, err
	}

	client, err := subscriber.New(subscriber.Config{
		Session:    awsSession,
		Dispatcher: config.Mux.Dispatcher(),
		QueueURL:   config.QueueURL,
		FineTuning: subscriber.FineTuning{
			DeliverChannelDepth:    0,
			AckChannelDepth:        0,
			ShutdownCancelRequests: time.Second * 1,
		},
		HookCallback: logErrorCallback,
	})
	if err != nil {
		return nil, err
	}

	return client, nil
}

func (w *CelebrationsWorkerConfig) HandleCheerCreate(ctx context.Context, h *eventbus.Header, event *cheer.CheerCreate) error {
	start := time.Now()
	ctx, cancel := context.WithTimeout(ctx, celebrationHandlerTimeout)
	defer cancel()

	if err := validateCheerCreateEvent(h, event); err != nil {
		go func() {
			logrus.WithError(err).Errorf("Invalid cheer create event %+v", event)
			_ = w.Statter.Inc(eventBusCelebrationHandlerValidationFailureMetric, 1, 1.0)
		}()
		return err
	}
	// Get sender ID
	var sender percy.User
	switch event.GetFrom().(type) {
	case *cheer.CheerCreate_User:
		if len(event.GetUser().Id) == 0 {
			return errors.New("cheer create event missing sender information")
		}
		sender = percy.NewUser(event.GetUser().Id)
	case *cheer.CheerCreate_AnonymousUser:
		sender = percy.NewAnonymousCheerer()
	default:
		return errors.New("invalid From type in Cheer Create event")
	}

	// ToUserId has already been validated to not be empty string
	channelId := event.ToUserId

	// CheerCreate's `Bits` field is always negative.
	bitsCheered := math.Int64Abs(event.Bits)

	var err error
	if w.Engine.HasCelebrations(channelId) {
		err = w.Engine.StartHighestValueCelebration(ctx, channelId, sender, percy.EventTypeCheer, bitsCheered, event.BitsTransactionId)
	} else {
		logrus.Debugf("Channel not in experiment, not triggering cheer: %+v", event)
	}

	go func() {
		_ = w.Statter.TimingDuration(eventBusCelebrationHandlerLatencyMetric, time.Since(start), 1.0)
		if err == nil {
			_ = w.Statter.Inc(eventBusCelebrationHandlerSuccessMetric, 1, 1.0)
		} else {
			_ = w.Statter.Inc(eventBusCelebrationHandlerFailureMetric, 1, 1.0)
		}
	}()

	return err
}

func (w *CelebrationsWorkerConfig) HandleUserGiftSubscription(ctx context.Context, h *eventbus.Header, event *user_gift_subscription_user.Create) error {
	start := time.Now()
	ctx, cancel := context.WithTimeout(ctx, celebrationHandlerTimeout)
	defer cancel()

	if err := validateGiftSubscriptionEvent(h, event); err != nil {
		go func() {
			logrus.WithError(err).Errorf("invalid gift subscription event: %+v", event)
			_ = w.Statter.Inc(eventBusCelebrationHandlerValidationFailureMetric, 1, 1.0)
		}()
		return err
	}

	// ToUserId has already been validated to not be empty string
	channelId := event.ToUserId

	var err error
	if w.Engine.HasCelebrations(channelId) {
		err = w.Engine.StartHighestValueCelebration(ctx, channelId, percy.NewUser(event.FromUserId), percy.EventTypeSubscriptionGift, event.Quantity, event.OriginId)
	} else {
		logrus.Debugf("Channel not in experiment, not triggering sub gift: %+v", event)
	}

	go func() {
		_ = w.Statter.TimingDuration(eventBusCelebrationHandlerLatencyMetric, time.Since(start), 1.0)
		if err == nil {
			_ = w.Statter.Inc(eventBusCelebrationHandlerSuccessMetric, 1, 1.0)
		} else {
			_ = w.Statter.Inc(eventBusCelebrationHandlerFailureMetric, 1, 1.0)
		}
	}()

	return err
}

func (w *CelebrationsWorkerConfig) HandleEntitlementBadgeCreate(ctx context.Context, h *eventbus.Header, event *entitlement_badge.EntitlementBadgeCreate) error {
	start := time.Now()
	ctx, cancel := context.WithTimeout(ctx, celebrationHandlerTimeout)
	defer cancel()

	if err := validateEntitlementBadgeCreateEvent(h, event); err != nil {
		go func() {
			logrus.WithError(err).Errorf("invalid entitlement badge create event: %+v", event)
			_ = w.Statter.Inc(eventBusCelebrationHandlerValidationFailureMetric, 1, 1.0)
		}()
		return err
	}

	// we only react to founder badge entitlements right now
	if !isFoundersBadgeEntitlement(event) {
		return nil
	}

	// For how the entitlement is created originally, see https://code.amazon.com/packages/SubscriptionsLambdasLambda/blobs/b13597ea71015a875cb0179a674d665ab2a890e6/--/server/find_founders.go#L452
	channelID := event.BadgeOwnerUserId
	userID := event.OwnerUserId
	quantity := int64(1)

	logrus.WithFields(logrus.Fields{
		"channelID": channelID,
		"userID":    userID,
	}).Info("Received a founders badge event")

	var err error
	if w.Engine.HasFounderCelebrations(ctx, channelID) {
		err = w.Engine.StartHighestValueCelebration(ctx, channelID, percy.NewUser(userID), percy.EventTypeSubscriptionFounder, quantity, event.OriginId)
	} else {
		logrus.Debugf("Channel not in founders celebration experiment, not triggering a celebration: %+v", event)
	}

	go func() {
		_ = w.Statter.TimingDuration(eventBusCelebrationHandlerLatencyMetric, time.Since(start), 1.0)
		if err == nil {
			_ = w.Statter.Inc(eventBusCelebrationHandlerSuccessMetric, 1, 1.0)
		} else {
			_ = w.Statter.Inc(eventBusCelebrationHandlerFailureMetric, 1, 1.0)
		}
	}()

	return err
}

// Parse entitlement events based on the Source and ItemID field
// For how the entitlement is created originally, see https://code.amazon.com/packages/SubscriptionsLambdasLambda/blobs/b13597ea71015a875cb0179a674d665ab2a890e6/--/server/find_founders.go#L452
func isFoundersBadgeEntitlement(event *entitlement_badge.EntitlementBadgeCreate) bool {
	return event.Source == entitlement_badge.Source_SOURCE_SUBS && strings.HasPrefix(event.ItemId, FoundersBadgePrefix)
}

func validateEntitlementBadgeCreateEvent(h *eventbus.Header, event *entitlement_badge.Create) error {
	if h == nil {
		return errors.New("header is nil")
	}

	if event == nil {
		return errors.New("event is nil")
	}

	if len(event.BadgeOwnerUserId) == 0 {
		return errors.New("empty badge owner user ID")
	}

	if len(event.OwnerUserId) == 0 {
		return errors.New("empty user ID")
	}

	if len(event.ItemId) == 0 {
		return errors.New("empty item ID")
	}

	return nil
}

func (w *CelebrationsWorkerConfig) registerHandlers() {
	cheer.RegisterCreateHandler(w.Mux, w.HandleCheerCreate)
	user_gift_subscription_user.RegisterCreateHandler(w.Mux, w.HandleUserGiftSubscription)
	entitlement_badge.RegisterCreateHandler(w.Mux, w.HandleEntitlementBadgeCreate)
}
