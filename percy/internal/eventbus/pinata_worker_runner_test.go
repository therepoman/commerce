package eventbus

import (
	"context"
	"testing"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/metrics"
	"code.justin.tv/commerce/percy/internal/miniexperiments/c2_bits_pinata"
	"code.justin.tv/commerce/percy/internal/miniexperiments/miniexperimentsfakes"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/schema/pkg/cheer"
	"code.justin.tv/eventbus/schema/pkg/stream"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func TestBitsPinataWorkerConfig_HandleStreamCreate(t *testing.T) {
	Convey("Given a valid WorkerConfig", t, func() {
		bitsPinataDB := &internalfakes.FakeBitsPinataDB{}
		pinataExperimentClient := &miniexperimentsfakes.FakeClient{}
		liveline := &internalfakes.FakeChannelStatusDB{}
		ctx := context.Background()

		config := PinataWorkerConfig{
			PinataExperimentClient: pinataExperimentClient,
			Mux:                    eventbus.NewMux(),
			QueueURL:               "some_url",
			Statter:                &metrics.NoopStatter{},
			BitsPinataDB:           bitsPinataDB,
			Liveline:               liveline,
		}

		validHeader := &eventbus.Header{
			MessageID:   makeGofrsUUID(),
			EventType:   "",
			CreatedAt:   time.Time{},
			Environment: "",
		}

		validStreamCreate := &stream.Create{
			UserId:     "user-123",
			Id:         "stream-123",
			StartedAt:  timestamppb.Now(),
			StreamType: stream.StreamType_STREAM_TYPE_LIVE,
		}

		Convey("it returns an error", func() {
			Convey("when the header is nil", func() {
				err := config.HandleStreamCreate(ctx, nil, validStreamCreate)
				So(err, ShouldNotBeNil)
			})

			Convey("when the event is nil", func() {
				err := config.HandleStreamCreate(ctx, validHeader, nil)
				So(err, ShouldNotBeNil)
			})

			Convey("when the event user ID is empty", func() {
				invalidStreamCreate := &stream.Create{
					UserId: "",
					Id:     "stream-123",
				}
				err := config.HandleStreamCreate(ctx, validHeader, invalidStreamCreate)
				So(err, ShouldNotBeNil)
			})

			Convey("when the event stream ID is empty", func() {
				invalidStreamCreate := &stream.Create{
					UserId: "user-123",
					Id:     "",
				}
				err := config.HandleStreamCreate(ctx, validHeader, invalidStreamCreate)
				So(err, ShouldNotBeNil)
			})

			Convey("when the event started at timestamp is nil", func() {
				invalidStreamCreate := &stream.Create{
					UserId:    "user-123",
					Id:        "stream-123",
					StartedAt: nil,
				}
				err := config.HandleStreamCreate(ctx, validHeader, invalidStreamCreate)
				So(err, ShouldNotBeNil)
			})

			Convey("when liveline cannot check that the channel is streaming", func() {
				pinataExperimentClient.GetTreatmentReturns(c2_bits_pinata.TreatmentExperiment, nil)
				liveline.ChannelIsStreamingReturns(false, errors.New("error"))
				err := config.HandleStreamCreate(ctx, validHeader, validStreamCreate)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("it returns nil", func() {
			Convey("when the event stream type is not live", func() {
				rerunStreamCreate := &stream.Create{
					UserId:     "user-123",
					Id:         "stream-123",
					StreamType: stream.StreamType_STREAM_TYPE_RERUN,
					StartedAt:  timestamppb.Now(),
				}
				err := config.HandleStreamCreate(ctx, validHeader, rerunStreamCreate)
				So(err, ShouldBeNil)
			})

			Convey("when the channel is not live", func() {
				pinataExperimentClient.GetTreatmentReturns(c2_bits_pinata.TreatmentExperiment, nil)
				liveline.ChannelIsStreamingReturns(false, nil)
				err := config.HandleStreamCreate(ctx, validHeader, validStreamCreate)
				So(err, ShouldBeNil)
			})
		})
	})
}

func TestBitsPinataWorkerConfig_HandleCheerCreate(t *testing.T) {
	Convey("Given a valid WorkerConfig", t, func() {
		bitsPinataDB := &internalfakes.FakeBitsPinataDB{}
		pinataExperimentClient := &miniexperimentsfakes.FakeClient{}
		liveline := &internalfakes.FakeChannelStatusDB{}
		ctx := context.Background()

		config := PinataWorkerConfig{
			PinataExperimentClient: pinataExperimentClient,
			Mux:                    eventbus.NewMux(),
			QueueURL:               "some_url",
			Statter:                &metrics.NoopStatter{},
			BitsPinataDB:           bitsPinataDB,
			Liveline:               liveline,
		}

		validHeader := &eventbus.Header{
			MessageID:   makeGofrsUUID(),
			EventType:   "",
			CreatedAt:   time.Time{},
			Environment: "",
		}

		validCheerCreate := &cheer.CheerCreate{
			BitsTransactionId: "transaction-123",
			ToUserId:          "user-123",
			From: &cheer.CheerCreate_User{User: &cheer.User{
				Id: "user-234",
			}},
			PublicMessage: "",
			Bits:          -50,
		}

		Convey("it returns an error", func() {
			Convey("when the event is nil", func() {
				err := config.HandleCheerCreate(ctx, validHeader, nil)
				So(err, ShouldNotBeNil)
			})

			Convey("when the event to-user ID is empty", func() {
				invalidCheerCreate := &cheer.CheerCreate{
					BitsTransactionId: "transaction-123",
					ToUserId:          "",
					From: &cheer.CheerCreate_User{User: &cheer.User{
						Id: "user-234",
					}},
					PublicMessage: "",
					Bits:          -50,
				}
				err := config.HandleCheerCreate(ctx, validHeader, invalidCheerCreate)
				So(err, ShouldNotBeNil)
			})

			Convey("when the event from-user type is nil", func() {
				invalidCheerCreate := &cheer.CheerCreate{
					BitsTransactionId: "transaction-123",
					ToUserId:          "user-123",
					From:              nil,
					PublicMessage:     "",
					Bits:              -50,
				}
				err := config.HandleCheerCreate(ctx, validHeader, invalidCheerCreate)
				So(err, ShouldNotBeNil)
			})

			Convey("when the event from-user is nil", func() {
				liveline.ChannelIsStreamingReturns(true, nil)
				invalidCheerCreate := &cheer.CheerCreate{
					BitsTransactionId: "transaction-123",
					ToUserId:          "user-123",
					From:              &cheer.CheerCreate_User{User: nil},
					PublicMessage:     "",
					Bits:              -50,
				}
				err := config.HandleCheerCreate(ctx, validHeader, invalidCheerCreate)
				So(err, ShouldNotBeNil)
			})

			Convey("when the event from-user ID is empty", func() {
				liveline.ChannelIsStreamingReturns(true, nil)
				invalidCheerCreate := &cheer.CheerCreate{
					BitsTransactionId: "transaction-123",
					ToUserId:          "user-123",
					From: &cheer.CheerCreate_User{User: &cheer.User{
						Id: "",
					}},
					PublicMessage: "",
					Bits:          -50,
				}
				err := config.HandleCheerCreate(ctx, validHeader, invalidCheerCreate)
				So(err, ShouldNotBeNil)
			})

			Convey("when liveline cannot check that the channel is streaming", func() {
				liveline.ChannelIsStreamingReturns(false, errors.New("error"))
				err := config.HandleCheerCreate(ctx, validHeader, validCheerCreate)
				So(err, ShouldNotBeNil)
			})

			Convey("when the pinata DB cannot add bits to the pinata", func() {
				liveline.ChannelIsStreamingReturns(true, nil)
				bitsPinataDB.AddBitsPinataContributionReturns(&percy.BitsPinata{}, errors.New("error"))
				err := config.HandleCheerCreate(ctx, validHeader, validCheerCreate)
				So(err, ShouldNotBeNil)
			})

			Convey("when the pinata DB returns a pinata with an empty stream ID", func() {
				liveline.ChannelIsStreamingReturns(true, nil)
				bitsPinataDB.AddBitsPinataContributionReturns(&percy.BitsPinata{StreamID: ""}, nil)
				err := config.HandleCheerCreate(ctx, validHeader, validCheerCreate)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("it returns nil", func() {
			Convey("when the channel is not streaming", func() {
				liveline.ChannelIsStreamingReturns(false, nil)
				err := config.HandleCheerCreate(ctx, validHeader, validCheerCreate)
				So(err, ShouldBeNil)
			})

			Convey("when the cheer create event is from an anonymous user", func() {
				liveline.ChannelIsStreamingReturns(true, nil)
				anonCheerCreate := &cheer.CheerCreate{
					BitsTransactionId: "transaction-123",
					ToUserId:          "user-123",
					From:              &cheer.CheerCreate_AnonymousUser{},
					PublicMessage:     "",
					Bits:              -50,
				}
				err := config.HandleCheerCreate(ctx, validHeader, anonCheerCreate)
				So(err, ShouldBeNil)
			})

			Convey("when the pinata DB returns a nil pinata", func() {
				liveline.ChannelIsStreamingReturns(true, nil)
				bitsPinataDB.AddBitsPinataContributionReturns(nil, nil)
				err := config.HandleCheerCreate(ctx, validHeader, validCheerCreate)
				So(err, ShouldBeNil)
			})
		})
	})
}
