package eventbus

import (
	"context"
	"errors"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/metrics"
	"code.justin.tv/commerce/percy/internal/tests"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/schema/pkg/emoticon"
	. "github.com/smartystreets/goconvey/convey"
)

func TestEmoticonsWorkerConfig_HandleEmoticonDelete(t *testing.T) {
	Convey("given an emoticons eventbus worker runner config", t, func() {
		engine := &internalfakes.FakeEngine{}

		config := EmoticonsWorkerConfig{
			Engine:   engine,
			Mux:      eventbus.NewMux(),
			QueueURL: "some_url",
			Statter:  &metrics.NoopStatter{},
		}

		ctx := context.Background()

		validHypeTrainConfig := tests.ValidHypeTrainConfig()
		validEventHeader := tests.ValidEventHeader()
		validEvent := tests.ValidEmoticonDeleteEvent()

		Convey("returns nil", func() {
			Convey("when we successfully delete the callout emote record", func() {
				validHypeTrainConfig.CalloutEmoteID = validEvent.Emoticon.Id
				engine.GetHypeTrainConfigReturns(validHypeTrainConfig, nil)
				engine.UnsetHypeTrainConfigReturns(validHypeTrainConfig, nil)
			})

			Convey("when the emote ID doesn't match the one set", func() {
				validHypeTrainConfig.CalloutEmoteID = "somethingElse"
				engine.GetHypeTrainConfigReturns(validHypeTrainConfig, nil)
			})

			Convey("when the isn't valid for a delete event", func() {
				validEvent.State = emoticon.State_STATE_ACTIVE
			})

			Convey("when the emoticon record isn't there", func() {
				validEvent.Emoticon = nil
			})

			Convey("when the event is nil", func() {
				validEvent = nil
			})

			err := config.HandleEmoticonDelete(ctx, validEventHeader, validEvent)
			So(err, ShouldBeNil)
		})

		Convey("returns error", func() {
			Convey("when we fail to fetch hype train config", func() {
				engine.GetHypeTrainConfigReturns(percy.HypeTrainConfig{}, errors.New("WALRUS STRIKE"))
			})

			Convey("when we fail to update hype train config", func() {
				validHypeTrainConfig.CalloutEmoteID = validEvent.Emoticon.Id
				engine.GetHypeTrainConfigReturns(validHypeTrainConfig, nil)
				engine.UnsetHypeTrainConfigReturns(percy.HypeTrainConfig{}, errors.New("WALRUS STRIKE"))
			})

			err := config.HandleEmoticonDelete(ctx, validEventHeader, validEvent)
			So(err, ShouldNotBeNil)
		})
	})
}
