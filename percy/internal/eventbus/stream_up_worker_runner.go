package eventbus

import (
	"context"
	"fmt"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	eventbus "code.justin.tv/eventbus/client"
	subscriber "code.justin.tv/eventbus/client/subscriber/sqsclient"
	"code.justin.tv/eventbus/schema/pkg/stream"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
)

const (
	// TODO: this is a placeholder value, need confirmation from Product
	waitTimeToStartPaidBoostOpportunity = 1 * time.Hour

	eventBusStreamUpHandlerLatencyMetric = "eventbus-stream-up-handler-latency"
	eventBusStreamUpHandlerSuccessMetric = "eventbus-stream-up-handler-success"
	eventBusStreamUpHandlerFailureMetric = "eventbus-stream-up-handler-failure"

	streamUpHandlerTimeout = 3 * time.Second
)

type StreamUpWorkerConfig struct {
	LaunchPad percy.LaunchPad
	Mux       *eventbus.Mux
	QueueURL  string
	Statter   statsd.Statter
}

func NewStreamUpWorkerRunner(config StreamUpWorkerConfig, region string) (*subscriber.SQSClient, error) {
	config.registerHandlers()

	awsSession, err := session.NewSession(&aws.Config{
		Region: aws.String(region),
	})
	if err != nil {
		return nil, err
	}

	client, err := subscriber.New(subscriber.Config{
		Session:    awsSession,
		Dispatcher: config.Mux.Dispatcher(),
		QueueURL:   config.QueueURL,
		FineTuning: subscriber.FineTuning{
			DeliverChannelDepth:    0,
			AckChannelDepth:        0,
			ShutdownCancelRequests: time.Second * 1,
		},
		HookCallback: logErrorCallback,
	})
	if err != nil {
		return nil, err
	}

	return client, nil
}

func (w *StreamUpWorkerConfig) HandleStreamCreate(ctx context.Context, h *eventbus.Header, event *stream.Create) error {
	start := time.Now()
	ctx, cancel := context.WithTimeout(ctx, streamUpHandlerTimeout)
	defer cancel()

	if err := validateStreamCreateEvent(h, event); err != nil {
		go func() {
			_ = w.Statter.Inc(eventBusHandlerValidationFailureMetric, 1, 1.0)
		}()
		return errors.Wrap(err, "invalid stream create event")
	}

	// paid boost opportunity: only start if streamer is in allowlist
	channelID := event.UserId
	if w.LaunchPad.ChannelIsAllowed(channelID) {
		err := w.LaunchPad.ScheduleBoostOpportunity(ctx, channelID, time.Now().Add(waitTimeToStartPaidBoostOpportunity))

		go func() {
			_ = w.Statter.TimingDuration(eventBusStreamUpHandlerLatencyMetric, time.Since(start), 1.0)
			if err == nil {
				_ = w.Statter.Inc(eventBusStreamUpHandlerSuccessMetric, 1, 1.0)
			} else {
				_ = w.Statter.Inc(eventBusStreamUpHandlerFailureMetric, 1, 1.0)
			}
		}()

		if err != nil {
			return errors.Wrap(err, fmt.Sprintf("could not schedule boost opportunity with channel: %s", channelID))
		}
	}

	return nil
}

func validateStreamCreateEvent(h *eventbus.Header, event *stream.Create) error {
	if h == nil {
		return errors.New("Stream Create event header is nil")
	}
	if event == nil {
		return errors.New("Stream Create event is nil")
	}
	if event.UserId == "" {
		return errors.New("Stream Create event has empty user ID")
	}
	return nil
}

func (w *StreamUpWorkerConfig) registerHandlers() {
	stream.RegisterCreateHandler(w.Mux, w.HandleStreamCreate)
}
