package eventbus

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/gogogadget/math"
	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	eventbus "code.justin.tv/eventbus/client"
	subscriber "code.justin.tv/eventbus/client/subscriber/sqsclient"
	"code.justin.tv/eventbus/schema/pkg/bits_use"
	"code.justin.tv/eventbus/schema/pkg/cheer"
	"code.justin.tv/eventbus/schema/pkg/user_gift_subscription_user"
	"code.justin.tv/eventbus/schema/pkg/user_subscribe_user_notice"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/cactus/go-statsd-client/statsd"
)

const (
	eventBusHandlerLatencyMetric           = "eventbus-handler-latency"
	eventBusHandlerSuccessMetric           = "eventbus-handler-success"
	eventBusHandlerFailureMetric           = "eventbus-handler-failure"
	eventBusHandlerValidationFailureMetric = "eventbus-handler-validation-failure"

	eventBusHandlerTimeout = 3 * time.Second
)

type WorkerConfig struct {
	Engine   percy.Engine
	Mux      *eventbus.Mux
	QueueURL string
	Statter  statsd.Statter
}

// NewWorkerRunner registers all Handlers with their respective Event Bus Events.
// It also initiates all the SQS jobs to start polling for events.
func NewWorkerRunner(config WorkerConfig, region string) (*subscriber.SQSClient, error) {
	config.registerHandlers()

	awsSession, err := session.NewSession(&aws.Config{
		Region: aws.String(region),
	})
	if err != nil {
		return nil, err
	}

	client, err := subscriber.New(subscriber.Config{
		Session:    awsSession,
		Dispatcher: config.Mux.Dispatcher(),
		QueueURL:   config.QueueURL,
		FineTuning: subscriber.FineTuning{
			DeliverChannelDepth:    0,
			AckChannelDepth:        0,
			ShutdownCancelRequests: time.Second * 1,
		},
		HookCallback: logErrorCallback,
	})
	if err != nil {
		return nil, err
	}

	return client, nil
}

func logErrorCallback(event subscriber.HookEvent) {
	if err := event.Error(); err != nil {
		logrus.WithError(err).Error("Event bus handler encountered an error")
	}
}

func (w *WorkerConfig) handleParticipation(ctx context.Context, participation percy.Participation) error {
	ctx, cancel := context.WithTimeout(ctx, eventBusHandlerTimeout)
	defer cancel()

	start := time.Now()
	err := w.Engine.RecordParticipation(ctx, participation)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"channelID": participation.ChannelID,
			"userID":    participation.User.ID(),
			"source":    participation.Source,
			"action":    participation.Action,
			"quantity":  participation.Quantity,
		}).Error("Failed to handle participation event")
	}

	go func(err error) {
		_ = w.Statter.TimingDuration(eventBusHandlerLatencyMetric, time.Since(start), 1.0)
		if err != nil {
			_ = w.Statter.Inc(eventBusHandlerFailureMetric, 1, 1.0)
		} else {
			_ = w.Statter.Inc(eventBusHandlerSuccessMetric, 1, 1.0)
		}
	}(err)

	return err
}

func (w *WorkerConfig) HandleBitsUse(ctx context.Context, h *eventbus.Header, event *bits_use.Create) error {
	// Only process BitsUseCreate events for extensions that are not "in development"
	// InDevelopment SKU's are to put briefly test SKU's that don't consume bits,
	// but otherwise go through most of the process, they are a tool for extension developers
	// to test bits use without actually having to use bits AND to be able to test new SKU's
	// without exposing them on an existing extension to regular users.
	if isExtensionInDevelopment(event) {
		return nil
	}

	if err := validateBitsUseEvent(h, event); err != nil {
		go func() {
			logrus.WithError(err).Errorf("Invalid bits use event: %+v", event)
			_ = w.Statter.Inc(eventBusHandlerValidationFailureMetric, 1, 1.0)
		}()
		return err
	}

	var participationIdentifier percy.ParticipationIdentifier
	switch event.GetFrom().(type) {
	case *bits_use.BitsUseCreate_Extension:
		participationIdentifier = percy.ParticipationIdentifier{
			Source: percy.BitsSource,
			Action: percy.ExtensionAction,
		}
	case *bits_use.BitsUseCreate_Poll:
		participationIdentifier = percy.ParticipationIdentifier{
			Source: percy.BitsSource,
			Action: percy.PollAction,
		}
	default:
		return errors.New("Invalid From field on Bits Use event")
	}

	participation := percy.Participation{
		ChannelID:               event.ToUserId,
		Timestamp:               h.CreatedAt,
		User:                    percy.NewUser(event.FromUserId),
		Quantity:                int(math.Int64Abs(event.BitsAmount)), // bits use amount is negative on spendings
		ParticipationIdentifier: participationIdentifier,
		EventID:                 event.BitsTransactionId,
	}

	return w.handleParticipation(ctx, participation)
}

func validateBitsUseEvent(h *eventbus.Header, event *bits_use.Create) error {
	if h == nil {
		return errors.New("Bits Use event header is nil")
	}
	if event == nil {
		return errors.New("Event is nil")
	}
	if len(event.ToUserId) == 0 {
		return errors.New("Bits Use missing ToUserId")
	}
	if len(event.FromUserId) == 0 {
		return errors.New("Bits Use missing FromUserId")
	}
	if math.Int64Abs(event.BitsAmount) <= 0 {
		return errors.New("Bits Use amount is invalid")
	}
	if event.GetFrom() == nil {
		return errors.New("Bits Use is missing From")
	}
	return nil
}

func isExtensionInDevelopment(event *bits_use.Create) bool {
	// Only process BitsUseCreate events for extensions that are not "in development"
	from, ok := event.GetFrom().(*bits_use.BitsUseCreate_Extension)
	if ok && from.Extension != nil && from.Extension.InDevelopment {
		return true
	}
	return false
}

func (w *WorkerConfig) HandleCheerCreate(ctx context.Context, h *eventbus.Header, event *cheer.Create) error {
	if err := validateCheerCreateEvent(h, event); err != nil {
		go func() {
			logrus.WithError(err).Errorf("Invalid cheer create event: %+v", event)
			_ = w.Statter.Inc(eventBusHandlerValidationFailureMetric, 1, 1.0)
		}()
		return err
	}

	var user percy.User
	switch event.GetFrom().(type) {
	case *cheer.CheerCreate_User:
		if len(event.GetUser().Id) == 0 {
			return errors.New("Bits Use Create missing ToUserId")
		}
		user = percy.NewUser(event.GetUser().Id)
	case *cheer.CheerCreate_AnonymousUser:
		user = percy.NewAnonymousCheerer()
	default:
		return errors.New("Invalid From type in Cheer Create event")
	}

	participation := percy.Participation{
		ChannelID: event.ToUserId,
		Timestamp: h.CreatedAt,
		Quantity:  int(math.Int64Abs(event.Bits)),
		User:      user,
		ParticipationIdentifier: percy.ParticipationIdentifier{
			Source: percy.BitsSource,
			Action: percy.CheerAction,
		},
		EventID: event.BitsTransactionId,
	}

	return w.handleParticipation(ctx, participation)
}

func validateCheerCreateEvent(h *eventbus.Header, event *cheer.Create) error {
	if h == nil {
		return errors.New("Bits Cheer Create event header is nil")
	}
	if event == nil {
		return errors.New("Event is nil")
	}
	if len(event.ToUserId) == 0 {
		return errors.New("Bits Cheer Create missing ToUserId")
	}
	if event.Bits >= 0 {
		return errors.New("Bits Cheer Create bits amount is invalid")
	}
	if event.GetFrom() == nil {
		return errors.New("Bits Cheer Create is missing From")
	}
	return nil
}

func (w *WorkerConfig) HandleUserGiftSubscription(ctx context.Context, h *eventbus.Header, event *user_gift_subscription_user.Create) error {
	if err := validateGiftSubscriptionEvent(h, event); err != nil {
		go func() {
			logrus.WithError(err).Errorf("invalid gift subscription event: %+v", event)
			_ = w.Statter.Inc(eventBusHandlerValidationFailureMetric, 1, 1.0)
		}()
		return err
	}

	var user percy.User
	if event.IsAnonymous {
		user = percy.NewAnonymousGifter()
	} else {
		user = percy.NewUser(event.GetFromUserId())
	}

	var participationIdentifier percy.ParticipationIdentifier
	tier := event.GetTier()
	if tier.GetNumeral() == user_gift_subscription_user.SubTier_SUB_TIER_1 {
		participationIdentifier = percy.ParticipationIdentifier{
			Source: percy.SubsSource,
			Action: percy.Tier1GiftedSubAction,
		}
	} else if tier.GetNumeral() == user_gift_subscription_user.SubTier_SUB_TIER_2 {
		participationIdentifier = percy.ParticipationIdentifier{
			Source: percy.SubsSource,
			Action: percy.Tier2GiftedSubAction,
		}
	} else if tier.GetNumeral() == user_gift_subscription_user.SubTier_SUB_TIER_3 {
		participationIdentifier = percy.ParticipationIdentifier{
			Source: percy.SubsSource,
			Action: percy.Tier3GiftedSubAction,
		}
	} else {
		return errors.New("invalid tier for UserGiftSubscriptionUser event")
	}

	if event.SubscriptionNumberOfMonths <= 0 {
		event.SubscriptionNumberOfMonths = 1
	}

	participation := percy.Participation{
		ChannelID:               event.ToUserId,
		Timestamp:               h.CreatedAt,
		User:                    user,
		Quantity:                int(event.Quantity * event.SubscriptionNumberOfMonths),
		ParticipationIdentifier: participationIdentifier,
		EventID:                 event.OriginId, // UUID of purchase order table
	}

	return w.handleParticipation(ctx, participation)
}

func validateGiftSubscriptionEvent(h *eventbus.Header, event *user_gift_subscription_user.Create) error {
	if h == nil {
		return errors.New("Gift Subscription event header is nil")
	}
	if event == nil {
		return errors.New("Event is nil")
	}
	if len(event.FromUserId) == 0 {
		return errors.New("Gift Subscription missing FromUserId")
	}
	if len(event.ToUserId) == 0 {
		return errors.New("Gift Subscription missing ToUserId")
	}
	if event.Quantity <= 0 {
		return errors.New("Gift Subscription Quantity is invalid")
	}
	if len(event.ProductId) == 0 {
		return errors.New("Gift Subscription is missing ProductId")
	}
	return nil
}

func (w *WorkerConfig) HandleUserSubscriptionUserNotice(ctx context.Context, h *eventbus.Header, event *user_subscribe_user_notice.Create) error {
	if err := validateSubscriptionUserNoticeEvent(h, event); err != nil {
		go func() {
			logrus.WithError(err).Errorf("Invalid user subscription user notice event: %+v", event)
			_ = w.Statter.Inc(eventBusHandlerValidationFailureMetric, 1, 1.0)
		}()
		return err
	}

	// filter out user notices that we don't track for Hype Train
	userNoticeType := event.GetUserNotice()
	if _, ok := userNoticeType.(*user_subscribe_user_notice.UserSubscribeUserNoticeCreate_GiftedSubscription); ok {
		return nil
	} else if _, ok := userNoticeType.(*user_subscribe_user_notice.UserSubscribeUserNoticeCreate_PayItForward); ok {
		return nil
	} else if _, ok := userNoticeType.(*user_subscribe_user_notice.UserSubscribeUserNoticeCreate_DeprecatedPrimeToPaid); ok {
		return nil
	}

	quantity, err := getQuantityFromUserNotice(event)
	if err != nil {
		return err
	}

	participationIdentifier, err := getParticipationIdentifierFromUserNotice(event)
	if err != nil {
		return err
	}

	originID := ""
	if event.SubscriptionInfo != nil {
		originID = event.SubscriptionInfo.OriginId
	}

	participation := percy.Participation{
		ChannelID:               event.ToUserId,
		Timestamp:               h.CreatedAt,
		Quantity:                quantity,
		ParticipationIdentifier: participationIdentifier,
		User:                    percy.NewUser(event.GetFromUserId()),
		EventID:                 originID, // UUID of purchase order table
	}

	return w.handleParticipation(ctx, participation)
}

func validateSubscriptionUserNoticeEvent(h *eventbus.Header, event *user_subscribe_user_notice.Create) error {
	if h == nil {
		return errors.New("Subscription User Notice event header is nil")
	}
	if event == nil {
		return errors.New("Event is nil")
	}
	if len(event.FromUserId) == 0 {
		return errors.New("Subscription User Notice event is missing FromUserId")
	}
	if len(event.ToUserId) == 0 {
		return errors.New("Subscription User Notice event is missing ToUserId")
	}
	/*
		// TODO - uncomment this when subscriptionInfo is populated from SUBS-4968
		if event.SubscriptionInfo == nil {
			return errors.New("Subscription User Notice event is missing Subscription")
		}
	*/
	if event.UserNotice == nil {
		return errors.New("Subscription User Notice event is missing User Notice")
	}
	return nil
}

// Note: open to refactoring
func getQuantityFromUserNotice(event *user_subscribe_user_notice.Create) (int, error) {
	switch event.GetUserNotice().(type) {
	case *user_subscribe_user_notice.UserSubscribeUserNoticeCreate_Subscription,
		*user_subscribe_user_notice.UserSubscribeUserNoticeCreate_Resubscription,
		*user_subscribe_user_notice.UserSubscribeUserNoticeCreate_TierUpgrade,
		*user_subscribe_user_notice.UserSubscribeUserNoticeCreate_GiftUpgrade,
		*user_subscribe_user_notice.UserSubscribeUserNoticeCreate_PrimeUpgrade:
		return 1, nil
	case *user_subscribe_user_notice.UserSubscribeUserNoticeCreate_ExtendSubscription:
		quantity := event.GetExtendSubscription().GetQuantity()
		return int(quantity), nil
	default:
		return 0, errors.New("Invalid Subscription User Notice type")
	}
}

// Note: open to refactoring
func getParticipationIdentifierFromUserNotice(event *user_subscribe_user_notice.Create) (percy.ParticipationIdentifier, error) {
	var tier *user_subscribe_user_notice.Tier = nil
	userNotice := event.GetUserNotice()

	switch userNotice.(type) {
	case *user_subscribe_user_notice.UserSubscribeUserNoticeCreate_Subscription:
		tier = event.GetSubscription().GetTier()
	case *user_subscribe_user_notice.UserSubscribeUserNoticeCreate_Resubscription:
		tier = event.GetResubscription().GetTier()
	case *user_subscribe_user_notice.UserSubscribeUserNoticeCreate_TierUpgrade:
		tier = event.GetTierUpgrade().GetNewTier()
	case *user_subscribe_user_notice.UserSubscribeUserNoticeCreate_GiftUpgrade:
		tier = event.GetGiftUpgrade().GetPaidTier()
	case *user_subscribe_user_notice.UserSubscribeUserNoticeCreate_PayItForward:
		tier = event.GetPayItForward().GetTier()
	case *user_subscribe_user_notice.UserSubscribeUserNoticeCreate_ExtendSubscription:
		tier = event.GetExtendSubscription().GetTier()
	case *user_subscribe_user_notice.UserSubscribeUserNoticeCreate_PrimeUpgrade:
		tier = event.GetPrimeUpgrade().GetPaidTier()
	default:
		return percy.ParticipationIdentifier{}, errors.New("Invalid Subscription User Notice type")
	}

	// Map tier to Percy's ParticipationIdentifier
	if tier.GetNumeral() == user_subscribe_user_notice.SubTier_SUB_TIER_1 {
		return percy.ParticipationIdentifier{
			Source: percy.SubsSource,
			Action: percy.Tier1SubAction,
		}, nil
	} else if tier.GetNumeral() == user_subscribe_user_notice.SubTier_SUB_TIER_2 {
		return percy.ParticipationIdentifier{
			Source: percy.SubsSource,
			Action: percy.Tier2SubAction,
		}, nil
	} else if tier.GetNumeral() == user_subscribe_user_notice.SubTier_SUB_TIER_3 {
		return percy.ParticipationIdentifier{
			Source: percy.SubsSource,
			Action: percy.Tier3SubAction,
		}, nil
	} else if tier.GetNumeral() == user_subscribe_user_notice.SubTier_SUB_TIER_INVALID {
		logrus.Warnf("Invalid tier found in subscribe user notice event, fallback to Tier 1 Sub. Event: %+v", event)
		return percy.ParticipationIdentifier{
			Source: percy.SubsSource,
			Action: percy.Tier1SubAction,
		}, nil
	}
	return percy.ParticipationIdentifier{}, errors.New("Could not get Participation Identifier")
}

func (w *WorkerConfig) registerHandlers() {
	bits_use.RegisterCreateHandler(w.Mux, w.HandleBitsUse)
	cheer.RegisterCreateHandler(w.Mux, w.HandleCheerCreate)
	user_gift_subscription_user.RegisterCreateHandler(w.Mux, w.HandleUserGiftSubscription)
	user_subscribe_user_notice.RegisterCreateHandler(w.Mux, w.HandleUserSubscriptionUserNotice)
}
