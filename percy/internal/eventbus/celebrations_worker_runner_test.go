package eventbus

import (
	"context"
	"testing"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/metrics"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/schema/pkg/cheer"
	"code.justin.tv/eventbus/schema/pkg/entitlement_badge"
	"code.justin.tv/eventbus/schema/pkg/user_gift_subscription_user"
	"github.com/gofrs/uuid"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/stretchr/testify/assert"
)

// eventbus.Header needs a UUID from this specific library
func makeGofrsUUID() uuid.UUID {
	val, _ := uuid.NewV4()
	return val
}

func TestCelebrationsWorkerConfig_HandleCheerCreate(t *testing.T) {
	// Reusable values
	validHeader := &eventbus.Header{
		MessageID:   makeGofrsUUID(),
		EventType:   "",
		CreatedAt:   time.Time{},
		Environment: "",
	}

	validCheerCreate := &cheer.CheerCreate{
		BitsTransactionId: "1234-56",
		ToUserId:          "1234",
		From: &cheer.CheerCreate_User{User: &cheer.User{
			Id: "5656",
		}},
		PublicMessage: "",
		Bits:          -50,
	}

	tests := []struct {
		scenario string
		h        *eventbus.Header
		event    *cheer.CheerCreate
		test     func(t *testing.T, h *eventbus.Header, event *cheer.CheerCreate)
	}{
		{
			scenario: "rejects when header is nil",
			h:        nil,
			event:    validCheerCreate,
			test: func(t *testing.T, h *eventbus.Header, event *cheer.CheerCreate) {
				engine := &internalfakes.FakeCelebrationsEngine{}

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				err := worker.HandleCheerCreate(context.Background(), h, event)
				assert.Error(t, err)
				assert.Equal(t, 0, engine.StartHighestValueCelebrationCallCount())
			},
		},
		{
			scenario: "rejects when event is nil",
			h:        validHeader,
			event:    nil,
			test: func(t *testing.T, h *eventbus.Header, event *cheer.CheerCreate) {
				engine := &internalfakes.FakeCelebrationsEngine{}

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				err := worker.HandleCheerCreate(context.Background(), h, event)
				assert.Error(t, err)
				assert.Equal(t, 0, engine.StartHighestValueCelebrationCallCount())
			},
		},
		{
			scenario: "rejects when userID is missing",
			h:        validHeader,
			event: &cheer.CheerCreate{
				BitsTransactionId: "1234-56",
				ToUserId:          "",
				From: &cheer.CheerCreate_User{User: &cheer.User{
					Id: "5656",
				}},
				PublicMessage: "",
				Bits:          -50,
			},
			test: func(t *testing.T, h *eventbus.Header, event *cheer.CheerCreate) {
				engine := &internalfakes.FakeCelebrationsEngine{}

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				err := worker.HandleCheerCreate(context.Background(), h, event)
				assert.Error(t, err)
				assert.Equal(t, 0, engine.StartHighestValueCelebrationCallCount())
			},
		},
		{
			scenario: "rejects when bits sent are 0 or greater (cheer events are always negative)",
			h:        validHeader,
			event: &cheer.CheerCreate{
				BitsTransactionId: "1234-56",
				ToUserId:          "1234",
				From: &cheer.CheerCreate_User{User: &cheer.User{
					Id: "5656",
				}},
				PublicMessage: "",
				Bits:          0,
			},
			test: func(t *testing.T, h *eventbus.Header, event *cheer.CheerCreate) {
				engine := &internalfakes.FakeCelebrationsEngine{}

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				err := worker.HandleCheerCreate(context.Background(), h, event)
				assert.Error(t, err)
				assert.Equal(t, 0, engine.StartHighestValueCelebrationCallCount())
			},
		},
		{
			scenario: "rejects when from is nil",
			h:        validHeader,
			event: &cheer.CheerCreate{
				BitsTransactionId: "1234-56",
				ToUserId:          "1234",
				From:              nil,
				PublicMessage:     "",
				Bits:              -50,
			},
			test: func(t *testing.T, h *eventbus.Header, event *cheer.CheerCreate) {
				engine := &internalfakes.FakeCelebrationsEngine{}

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				err := worker.HandleCheerCreate(context.Background(), h, event)
				assert.Error(t, err)
				assert.Equal(t, 0, engine.StartHighestValueCelebrationCallCount())
			},
		},
		{
			scenario: "rejects when bits sent are 0 or greater (cheer events are always negative)",
			h:        validHeader,
			event: &cheer.CheerCreate{
				BitsTransactionId: "1234-56",
				ToUserId:          "1234",
				From: &cheer.CheerCreate_User{User: &cheer.User{
					Id: "5656",
				}},
				PublicMessage: "",
				Bits:          0,
			},
			test: func(t *testing.T, h *eventbus.Header, event *cheer.CheerCreate) {
				engine := &internalfakes.FakeCelebrationsEngine{}

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				err := worker.HandleCheerCreate(context.Background(), h, event)
				assert.Error(t, err)
				assert.Equal(t, 0, engine.StartHighestValueCelebrationCallCount())
			},
		},
		{
			scenario: "Passes when parameters are OK",
			h:        validHeader,
			event:    validCheerCreate,
			test: func(t *testing.T, h *eventbus.Header, event *cheer.CheerCreate) {
				engine := &internalfakes.FakeCelebrationsEngine{}

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				engine.GetCelebrationConfigReturns(percy.CelebrationConfig{
					ChannelID: "1234",
					Enabled:   true,
					Celebrations: map[string]percy.Celebration{
						"0": {
							CelebrationID:        "0",
							Enabled:              true,
							EventType:            percy.EventTypeCheer,
							EventThreshold:       50,
							CelebrationEffect:    "",
							CelebrationArea:      "",
							CelebrationIntensity: 0,
							CelebrationDuration:  0,
						},
					},
				}, nil)
				engine.HasCelebrationsReturns(true)
				err := worker.HandleCheerCreate(context.Background(), h, event)
				assert.NoError(t, err)
				assert.Equal(t, 1, engine.StartHighestValueCelebrationCallCount())
			},
		},
		{
			scenario: "Passes when parameters are OK and experiment is off",
			h:        validHeader,
			event:    validCheerCreate,
			test: func(t *testing.T, h *eventbus.Header, event *cheer.CheerCreate) {
				engine := &internalfakes.FakeCelebrationsEngine{}

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				engine.GetCelebrationConfigReturns(percy.CelebrationConfig{
					ChannelID: "1234",
					Enabled:   true,
					Celebrations: map[string]percy.Celebration{
						"0": {
							CelebrationID:        "0",
							Enabled:              true,
							EventType:            percy.EventTypeCheer,
							EventThreshold:       50,
							CelebrationEffect:    "",
							CelebrationArea:      "",
							CelebrationIntensity: 0,
							CelebrationDuration:  0,
						},
					},
				}, nil)
				engine.HasCelebrationsReturns(false)
				err := worker.HandleCheerCreate(context.Background(), h, event)
				assert.NoError(t, err)
				assert.Equal(t, 0, engine.StartHighestValueCelebrationCallCount())
			},
		},
	}

	for _, test := range tests {
		tc := test

		t.Run(test.scenario, func(t *testing.T) {
			tc.test(t, tc.h, tc.event)
		})
	}
}

func TestCelebrationsWorkerConfig_HandleUserGiftSubscription(t *testing.T) {
	// Reusable values
	validHeader := &eventbus.Header{
		MessageID:   makeGofrsUUID(),
		EventType:   "",
		CreatedAt:   time.Time{},
		Environment: "",
	}

	validEvent := &user_gift_subscription_user.Create{
		FromUserId:  "1234",
		ToUserId:    "5656",
		Quantity:    25,
		ProductId:   "7",
		IsAnonymous: false,
		Tier:        nil,
	}

	tests := []struct {
		scenario string
		h        *eventbus.Header
		event    *user_gift_subscription_user.Create
		test     func(t *testing.T, h *eventbus.Header, event *user_gift_subscription_user.Create)
	}{
		{
			scenario: "rejects when the header is nil",
			h:        nil,
			event:    validEvent,
			test: func(t *testing.T, h *eventbus.Header, event *user_gift_subscription_user.Create) {
				engine := &internalfakes.FakeCelebrationsEngine{}

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				err := worker.HandleUserGiftSubscription(context.Background(), h, event)
				assert.Error(t, err)
				assert.Equal(t, 0, engine.StartHighestValueCelebrationCallCount())
			},
		},
		{
			scenario: "rejects when the event is nil",
			h:        validHeader,
			event:    nil,
			test: func(t *testing.T, h *eventbus.Header, event *user_gift_subscription_user.Create) {
				engine := &internalfakes.FakeCelebrationsEngine{}

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				err := worker.HandleUserGiftSubscription(context.Background(), h, event)
				assert.Error(t, err)
				assert.Equal(t, 0, engine.StartHighestValueCelebrationCallCount())
			},
		},
		{
			scenario: "rejects when the from user ID is empty",
			h:        validHeader,
			event: &user_gift_subscription_user.Create{
				FromUserId:  "",
				ToUserId:    "5656",
				Quantity:    25,
				ProductId:   "7",
				IsAnonymous: false,
				Tier:        nil,
			},
			test: func(t *testing.T, h *eventbus.Header, event *user_gift_subscription_user.Create) {
				engine := &internalfakes.FakeCelebrationsEngine{}

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				err := worker.HandleUserGiftSubscription(context.Background(), h, event)
				assert.Error(t, err)
				assert.Equal(t, 0, engine.StartHighestValueCelebrationCallCount())
			},
		},
		{
			scenario: "rejects when the to user ID is empty",
			h:        validHeader,
			event: &user_gift_subscription_user.Create{
				FromUserId:  "1234",
				ToUserId:    "",
				Quantity:    25,
				ProductId:   "7",
				IsAnonymous: false,
				Tier:        nil,
			},
			test: func(t *testing.T, h *eventbus.Header, event *user_gift_subscription_user.Create) {
				engine := &internalfakes.FakeCelebrationsEngine{}

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				err := worker.HandleUserGiftSubscription(context.Background(), h, event)
				assert.Error(t, err)
				assert.Equal(t, 0, engine.StartHighestValueCelebrationCallCount())
			},
		},
		{
			scenario: "rejects when the quantity is 0 or fewer",
			h:        validHeader,
			event: &user_gift_subscription_user.Create{
				FromUserId:  "1234",
				ToUserId:    "5656",
				Quantity:    0,
				ProductId:   "7",
				IsAnonymous: false,
				Tier:        nil,
			},
			test: func(t *testing.T, h *eventbus.Header, event *user_gift_subscription_user.Create) {
				engine := &internalfakes.FakeCelebrationsEngine{}

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				err := worker.HandleUserGiftSubscription(context.Background(), h, event)
				assert.Error(t, err)
				assert.Equal(t, 0, engine.StartHighestValueCelebrationCallCount())
			},
		},
		{
			scenario: "Passes when parameters are OK",
			h:        validHeader,
			event:    validEvent,
			test: func(t *testing.T, h *eventbus.Header, event *user_gift_subscription_user.Create) {
				engine := &internalfakes.FakeCelebrationsEngine{}

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				engine.GetCelebrationConfigReturns(percy.CelebrationConfig{
					ChannelID: "5656",
					Enabled:   false,
					Celebrations: map[string]percy.Celebration{
						"0": {
							CelebrationID:        "0",
							Enabled:              true,
							EventType:            percy.EventTypeSubscriptionGift,
							EventThreshold:       10,
							CelebrationEffect:    "",
							CelebrationArea:      "",
							CelebrationIntensity: 0,
							CelebrationDuration:  0,
						},
					},
				}, nil)
				engine.HasCelebrationsReturns(true)
				err := worker.HandleUserGiftSubscription(context.Background(), h, event)
				assert.NoError(t, err)
				assert.Equal(t, 1, engine.StartHighestValueCelebrationCallCount())
			},
		},
		{
			scenario: "Passes when parameters are OK and experiment is off",
			h:        validHeader,
			event:    validEvent,
			test: func(t *testing.T, h *eventbus.Header, event *user_gift_subscription_user.Create) {
				engine := &internalfakes.FakeCelebrationsEngine{}

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				engine.GetCelebrationConfigReturns(percy.CelebrationConfig{
					ChannelID: "5656",
					Enabled:   false,
					Celebrations: map[string]percy.Celebration{
						"0": {
							CelebrationID:        "0",
							Enabled:              true,
							EventType:            percy.EventTypeSubscriptionGift,
							EventThreshold:       10,
							CelebrationEffect:    "",
							CelebrationArea:      "",
							CelebrationIntensity: 0,
							CelebrationDuration:  0,
						},
					},
				}, nil)
				engine.HasCelebrationsReturns(false)
				err := worker.HandleUserGiftSubscription(context.Background(), h, event)
				assert.NoError(t, err)
				assert.Equal(t, 0, engine.StartHighestValueCelebrationCallCount())
			},
		},
	}

	for _, test := range tests {
		tc := test

		t.Run(test.scenario, func(t *testing.T) {
			tc.test(t, tc.h, tc.event)
		})
	}
}

func TestCelebrationWorkerConfig_HandleEntitlementBadgeCreate(t *testing.T) {
	now := timestamp.Timestamp{Seconds: time.Now().Unix()}
	later := timestamp.Timestamp{Seconds: time.Now().Unix() + 10000}

	// Reusable values
	validHeader := &eventbus.Header{
		MessageID:   makeGofrsUUID(),
		EventType:   "",
		CreatedAt:   time.Time{},
		Environment: "",
	}

	validEvent := &entitlement_badge.Create{
		Id:               "1234",
		OwnerUserId:      "5656",
		Source:           entitlement_badge.Source_SOURCE_SUBS,
		ItemId:           "founder-1123",
		BadgeOwnerUserId: "1234567890",
		OriginId:         makeGofrsUUID().String(),
		StartsAt:         &now,
		EndsAt:           &later,
		Type:             "base",
	}

	tests := []struct {
		scenario string
		h        *eventbus.Header
		event    *entitlement_badge.Create
		test     func(t *testing.T, h *eventbus.Header, event *entitlement_badge.Create)
	}{
		{
			scenario: "rejects when the header is nil",
			h:        nil,
			event:    validEvent,
			test: func(t *testing.T, h *eventbus.Header, event *entitlement_badge.Create) {
				engine := &internalfakes.FakeCelebrationsEngine{}

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				err := worker.HandleEntitlementBadgeCreate(context.Background(), h, event)
				assert.Error(t, err)
				assert.Equal(t, 0, engine.StartHighestValueCelebrationCallCount())
			},
		},
		{
			scenario: "rejects when the event is nil",
			h:        validHeader,
			event:    nil,
			test: func(t *testing.T, h *eventbus.Header, event *entitlement_badge.Create) {
				engine := &internalfakes.FakeCelebrationsEngine{}

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				err := worker.HandleEntitlementBadgeCreate(context.Background(), h, event)
				assert.Error(t, err)
				assert.Equal(t, 0, engine.StartHighestValueCelebrationCallCount())
			},
		},
		{
			scenario: "rejects when the from it is not a founders badge entitlement",
			h:        validHeader,
			event: &entitlement_badge.Create{
				Id:               "1234",
				OwnerUserId:      "5656",
				Source:           entitlement_badge.Source_SOURCE_SUBS,
				ItemId:           "1123",
				BadgeOwnerUserId: "1234567890",
				OriginId:         makeGofrsUUID().String(),
				StartsAt:         &now,
				EndsAt:           &later,
				Type:             "base",
			},
			test: func(t *testing.T, h *eventbus.Header, event *entitlement_badge.Create) {
				engine := &internalfakes.FakeCelebrationsEngine{}

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				err := worker.HandleEntitlementBadgeCreate(context.Background(), h, event)
				assert.NoError(t, err)
				assert.Equal(t, 0, engine.StartHighestValueCelebrationCallCount())
			},
		},
		{
			scenario: "rejects when the to Item ID is empty",
			h:        validHeader,
			event: &entitlement_badge.Create{
				Id:               "1234",
				OwnerUserId:      "5656",
				Source:           entitlement_badge.Source_SOURCE_SUBS,
				ItemId:           "",
				BadgeOwnerUserId: "1234567890",
				OriginId:         makeGofrsUUID().String(),
				StartsAt:         &now,
				EndsAt:           &later,
				Type:             "base",
			},
			test: func(t *testing.T, h *eventbus.Header, event *entitlement_badge.Create) {
				engine := &internalfakes.FakeCelebrationsEngine{}

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				err := worker.HandleEntitlementBadgeCreate(context.Background(), h, event)
				assert.Error(t, err)
				assert.Equal(t, 0, engine.StartHighestValueCelebrationCallCount())
			},
		},
		{
			scenario: "rejects when the to Owner User ID is empty",
			h:        validHeader,
			event: &entitlement_badge.Create{
				Id:               "1234",
				OwnerUserId:      "",
				Source:           entitlement_badge.Source_SOURCE_SUBS,
				ItemId:           "founder-1123",
				BadgeOwnerUserId: "1234567890",
				OriginId:         makeGofrsUUID().String(),
				StartsAt:         &now,
				EndsAt:           &later,
				Type:             "base",
			},
			test: func(t *testing.T, h *eventbus.Header, event *entitlement_badge.Create) {
				engine := &internalfakes.FakeCelebrationsEngine{}

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				err := worker.HandleEntitlementBadgeCreate(context.Background(), h, event)
				assert.Error(t, err)
				assert.Equal(t, 0, engine.StartHighestValueCelebrationCallCount())
			},
		},
		{
			scenario: "rejects when the to Item ID is empty",
			h:        validHeader,
			event: &entitlement_badge.Create{
				Id:               "1234",
				OwnerUserId:      "12345",
				Source:           entitlement_badge.Source_SOURCE_SUBS,
				ItemId:           "",
				BadgeOwnerUserId: "1234567890",
				OriginId:         makeGofrsUUID().String(),
				StartsAt:         &now,
				EndsAt:           &later,
				Type:             "base",
			},
			test: func(t *testing.T, h *eventbus.Header, event *entitlement_badge.Create) {
				engine := &internalfakes.FakeCelebrationsEngine{}

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				err := worker.HandleEntitlementBadgeCreate(context.Background(), h, event)
				assert.Error(t, err)
				assert.Equal(t, 0, engine.StartHighestValueCelebrationCallCount())
			},
		},
		{
			scenario: "Passes when parameters are OK and experiment is on",
			h:        validHeader,
			event:    validEvent,
			test: func(t *testing.T, h *eventbus.Header, event *entitlement_badge.Create) {
				engine := &internalfakes.FakeCelebrationsEngine{}
				engine.HasFounderCelebrationsReturns(true)

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				err := worker.HandleEntitlementBadgeCreate(context.Background(), h, event)
				assert.NoError(t, err)
				assert.Equal(t, 1, engine.StartHighestValueCelebrationCallCount())
			},
		},
		{
			scenario: "Passes when parameters are OK and experiment is off",
			h:        validHeader,
			event:    validEvent,
			test: func(t *testing.T, h *eventbus.Header, event *entitlement_badge.Create) {
				engine := &internalfakes.FakeCelebrationsEngine{}
				engine.HasCelebrationsReturns(false)

				worker := CelebrationsWorkerConfig{
					Engine:   engine,
					Mux:      eventbus.NewMux(),
					QueueURL: "wow!",
					Statter:  &metrics.NoopStatter{},
				}
				err := worker.HandleEntitlementBadgeCreate(context.Background(), h, event)
				assert.NoError(t, err)
				assert.Equal(t, 0, engine.StartHighestValueCelebrationCallCount())
			},
		},
	}

	for _, test := range tests {
		tc := test

		t.Run(test.scenario, func(t *testing.T) {
			tc.test(t, tc.h, tc.event)
		})
	}
}
