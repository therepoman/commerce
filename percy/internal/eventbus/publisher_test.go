package eventbus

import (
	"context"
	"testing"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/schema/pkg/hype_train"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	dummyAction1   = "action1"
	dummyAction2   = "action2"
	dummyChannelID = "cool-channel"
	dummyID        = "hype-train-id"
)

var (
	now           = time.Now()
	baseHypeTrain = percy.HypeTrain{
		ChannelID: dummyChannelID,
		ID:        dummyID,
		StartedAt: now.Add(-10 * time.Minute),
		ExpiresAt: now.Add(10 * time.Minute),
		Config: percy.HypeTrainConfig{
			Difficulty:       "ezpz",
			CooldownDuration: 5 * time.Minute,
			ParticipationConversionRates: percy.ParticipationConversionRates{
				{
					Source: percy.BitsSource,
					Action: dummyAction1,
				}: 1,
				{
					Source: percy.BitsSource,
					Action: dummyAction2,
				}: 3,
				{
					Source: percy.SubsSource,
					Action: dummyAction1,
				}: 500,
			},
			LevelSettingsByDifficulty: percy.DifficultySettings{
				"ezpz": []percy.LevelSetting{
					{
						Level:   1,
						Goal:    10000,
						Rewards: []percy.Reward{},
					},
				},
			},
		},
	}
)

type NoopEventbusClient struct {
	called bool
	event  eventbus.Message
}

func (n *NoopEventbusClient) Publish(ctx context.Context, message eventbus.Message) error {
	n.called = true
	n.event = message
	return nil
}

func TestUpdateHypeTrain(t *testing.T) {
	ctx := context.Background()

	Convey("Given an eventbus publisher", t, func() {
		mockClient := &NoopEventbusClient{}
		p := &Publisher{
			client: mockClient,
		}

		Convey("When HypeTrainUpdate is called with conductor data", func() {
			hypeTrain := baseHypeTrain
			hypeTrain.Conductors = map[percy.ParticipationSource]percy.Conductor{
				percy.BitsSource: {
					Source: percy.BitsSource,
					User: percy.User{
						Id: "bloodseeker-spammer",
					},
					Participations: percy.ParticipationTotals{
						percy.ParticipationIdentifier{
							Source: percy.BitsSource,
							Action: dummyAction1,
						}: 11,
						percy.ParticipationIdentifier{
							Source: percy.BitsSource,
							Action: dummyAction2,
						}: 20,
					},
				},
				percy.SubsSource: {
					Source: percy.SubsSource,
					User: percy.User{
						Id: "injoker",
					},
					Participations: percy.ParticipationTotals{
						percy.ParticipationIdentifier{
							Source: percy.SubsSource,
							Action: dummyAction1,
						}: 8,
					},
				},
			}

			err := p.HypeTrainUpdate(ctx, hypeTrain, percy.Participation{
				ChannelID: dummyChannelID,
				Timestamp: now,
				User: percy.User{
					Id: "1k-mmr-riki-picker",
				},
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: dummyAction2,
				},
				Quantity: 5,
			})

			Convey("Then there should be no error", func() {
				So(err, ShouldBeNil)
			})

			Convey("Then the eventbus fields should all be set", func() {
				event := mockClient.event.(*hype_train.HypeTrainUpdate)

				So(event.Id, ShouldEqual, hypeTrain.ID)
				So(event.BroadcasterUserId, ShouldEqual, hypeTrain.ChannelID)
				So(event.ExpiresAt.GetValue().AsTime(), ShouldEqual, hypeTrain.ExpiresAt)
				So(event.StartedAt.AsTime(), ShouldEqual, hypeTrain.StartedAt)
				So(event.GetTotal().GetValue(), ShouldEqual, hypeTrain.LevelProgress().Total)
				So(event.GetGoal().GetValue(), ShouldEqual, hypeTrain.LevelProgress().Goal)
				So(event.GetLevel().GetValue(), ShouldEqual, 1)

				So(len(event.GetTopContributions()), ShouldEqual, 2)
				for _, cChange := range event.GetTopContributions() {
					c := cChange.GetValue()
					if c.Type == hype_train.ContributionType_CONTRIBUTION_TYPE_BITS {
						So(c.Total, ShouldEqual, 71) // 11*1 + 20*3
						So(c.Type, ShouldEqual, hype_train.ContributionType_CONTRIBUTION_TYPE_BITS)
						So(c.UserId, ShouldEqual, "bloodseeker-spammer")
					} else if c.Type == hype_train.ContributionType_CONTRIBUTION_TYPE_SUBSCRIPTION {
						So(c.Total, ShouldEqual, 4000) // 8 * 500
						So(c.Type, ShouldEqual, hype_train.ContributionType_CONTRIBUTION_TYPE_SUBSCRIPTION)
						So(c.UserId, ShouldEqual, "injoker")
					} else {
						// unexpected!
						t.Fail()
					}
				}

				lastContribution := event.GetLastContribution().GetValue()
				So(lastContribution, ShouldNotBeNil)
				So(lastContribution.GetTotal(), ShouldEqual, 15) // 5*3
				So(lastContribution.GetType(), ShouldEqual, hype_train.ContributionType_CONTRIBUTION_TYPE_BITS)
				So(lastContribution.GetUserId(), ShouldEqual, "1k-mmr-riki-picker")
			})
		})

		Convey("When HypeTrainUpdate is called without conductor data", func() {
			hypeTrain := baseHypeTrain

			err := p.HypeTrainUpdate(ctx, hypeTrain, percy.Participation{})

			Convey("Then an error is returned", func() {
				So(err, ShouldNotBeNil)
			})
		})
	})
}

func TestEndHypeTrain(t *testing.T) {
	ctx := context.Background()

	endTime := time.Now()

	cooldown := 5 * time.Minute
	mockConfigDB := &internalfakes.FakeHypeTrainConfigsDB{}
	mockConfigDB.GetConfigReturns(&percy.HypeTrainConfig{
		CooldownDuration: cooldown,
	}, nil)

	Convey("Given an eventbus publisher", t, func() {
		mockClient := &NoopEventbusClient{}
		p := &Publisher{
			client:             mockClient,
			hypeTrainConfigsDB: mockConfigDB,
		}

		Convey("When EndHypeTrain is called with conductor data", func() {
			hypeTrain := baseHypeTrain

			hypeTrain.Conductors = map[percy.ParticipationSource]percy.Conductor{
				percy.BitsSource: {
					Source: percy.BitsSource,
					User: percy.User{
						Id: "cool-user-1",
					},
					Participations: percy.ParticipationTotals{
						percy.ParticipationIdentifier{
							Source: percy.BitsSource,
							Action: dummyAction1,
						}: 12,
						percy.ParticipationIdentifier{
							Source: percy.BitsSource,
							Action: dummyAction2,
						}: 25,
					},
				},
				percy.SubsSource: {
					Source: percy.SubsSource,
					User: percy.User{
						Id: "cool-user-2",
					},
					Participations: percy.ParticipationTotals{
						percy.ParticipationIdentifier{
							Source: percy.SubsSource,
							Action: dummyAction1,
						}: 5,
					},
				},
			}

			err := p.EndHypeTrain(ctx, hypeTrain, endTime, percy.EndingReasonCompleted)

			Convey("Then there should be no error", func() {
				So(err, ShouldBeNil)
			})

			Convey("Then the event fields should all be set properly", func() {
				event := mockClient.event.(*hype_train.HypeTrainDelete)

				So(event.Id, ShouldEqual, hypeTrain.ID)
				So(event.BroadcasterUserId, ShouldEqual, hypeTrain.ChannelID)
				So(event.EndedAt.AsTime(), ShouldEqual, endTime)
				So(event.StartedAt.AsTime(), ShouldEqual, hypeTrain.StartedAt)
				So(event.Total, ShouldEqual, hypeTrain.LevelProgress().Total)
				So(event.Goal, ShouldEqual, hypeTrain.LevelProgress().Goal)
				So(event.Level, ShouldEqual, 1)
				So(event.CooldownEndsAt.AsTime(), ShouldEqual, event.EndedAt.AsTime().Add(hypeTrain.Config.CooldownDuration))

				So(len(event.TopContributions), ShouldEqual, 2)
				for _, c := range event.TopContributions {
					if c.Type == hype_train.ContributionType_CONTRIBUTION_TYPE_BITS {
						So(c.Total, ShouldEqual, 87) // 3*25 + 12
						So(c.Type, ShouldEqual, hype_train.ContributionType_CONTRIBUTION_TYPE_BITS)
						So(c.UserId, ShouldEqual, "cool-user-1")
					} else if c.Type == hype_train.ContributionType_CONTRIBUTION_TYPE_SUBSCRIPTION {
						So(c.Total, ShouldEqual, 2500)
						So(c.Type, ShouldEqual, hype_train.ContributionType_CONTRIBUTION_TYPE_SUBSCRIPTION)
						So(c.UserId, ShouldEqual, "cool-user-2")
					} else {
						// unexpected!
						t.Fail()
					}
				}
			})
		})
	})
}

func TestStartHypeTrain(t *testing.T) {
	ctx := context.Background()

	hypeTrain := baseHypeTrain

	kickoffParticipations := []percy.Participation{
		// Largest bits
		{
			ChannelID: "channel1",
			Timestamp: now.Add(-5 * time.Minute),
			User: percy.User{
				Id: "userA",
			},
			Quantity: 1234,
			ParticipationIdentifier: percy.ParticipationIdentifier{
				Source: percy.BitsSource,
				Action: dummyAction1,
			},
		},
		// Only subs
		{
			ChannelID: "channel1",
			Timestamp: now.Add(-6 * time.Minute),
			User: percy.User{
				Id: "userB",
			},
			Quantity: 3,
			ParticipationIdentifier: percy.ParticipationIdentifier{
				Source: percy.SubsSource,
				Action: dummyAction1,
			},
		},
		// Not largest bits (but newest)
		{
			ChannelID: "channel1",
			Timestamp: now.Add(-2 * time.Minute),
			User: percy.User{
				Id: "userC",
			},
			Quantity: 10,
			ParticipationIdentifier: percy.ParticipationIdentifier{
				Source: percy.BitsSource,
				Action: dummyAction1,
			},
		},
	}

	Convey("Given an eventbus publisher", t, func() {
		mockClient := &NoopEventbusClient{}
		p := &Publisher{
			client: mockClient,
		}

		Convey("When StartHypeTrain is called", func() {
			err := p.StartHypeTrain(ctx, hypeTrain, kickoffParticipations...)

			Convey("Then there should be no error", func() {
				So(err, ShouldBeNil)
			})

			Convey("Then Publish should be called on the eventbus client", func() {
				So(mockClient.called, ShouldBeTrue)
				So(mockClient.event, ShouldNotBeNil)
			})

			Convey("Then the emitted event should contain the right fields", func() {
				event := mockClient.event.(*hype_train.HypeTrainCreate)

				So(event.Id, ShouldEqual, hypeTrain.ID)
				So(event.BroadcasterUserId, ShouldEqual, hypeTrain.ChannelID)
				So(event.ExpiresAt.AsTime(), ShouldEqual, hypeTrain.ExpiresAt)
				So(event.StartedAt.AsTime(), ShouldEqual, hypeTrain.StartedAt)
				So(event.Total, ShouldEqual, hypeTrain.LevelProgress().Total)
				So(event.Goal, ShouldEqual, hypeTrain.LevelProgress().Goal)

				So(event.LastContribution.Total, ShouldEqual, 10)
				So(event.LastContribution.UserId, ShouldEqual, "userC")
				So(event.LastContribution.Type, ShouldEqual, hype_train.ContributionType_CONTRIBUTION_TYPE_BITS)

				So(len(event.TopContributions), ShouldEqual, 2)

				for _, c := range event.TopContributions {
					if c.Type == hype_train.ContributionType_CONTRIBUTION_TYPE_BITS {
						So(c.Total, ShouldEqual, 1234)
						So(c.Type, ShouldEqual, hype_train.ContributionType_CONTRIBUTION_TYPE_BITS)
						So(c.UserId, ShouldEqual, "userA")
					} else if c.Type == hype_train.ContributionType_CONTRIBUTION_TYPE_SUBSCRIPTION {
						So(c.Total, ShouldEqual, 1500)
						So(c.Type, ShouldEqual, hype_train.ContributionType_CONTRIBUTION_TYPE_SUBSCRIPTION)
						So(c.UserId, ShouldEqual, "userB")
					} else {
						// unexpected!
						t.Fail()
					}
				}
			})
		})
	})
}
