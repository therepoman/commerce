package eventbus

import (
	"context"
	"time"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/client/publisher"
	"code.justin.tv/eventbus/schema/pkg/eventbus/change"
	"code.justin.tv/eventbus/schema/pkg/hype_train"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/golang/protobuf/ptypes"
	"github.com/pkg/errors"
)

const (
	hypeTrainCreate = "HypeTrainCreate"
	hypeTrainUpdate = "HypeTrainUpdate"
	hypeTrainDelete = "HypeTrainDelete"
)

type EventbusClient interface {
	Publish(ctx context.Context, message eventbus.Message) error
}

type Publisher struct {
	client             EventbusClient
	hypeTrainConfigsDB percy.HypeTrainConfigsDB
}

func NewPublisher(region string, env string, hypeTrainConfigsDB percy.HypeTrainConfigsDB) (*Publisher, error) {
	var eventbusEnvironment publisher.Environment
	switch env {
	case "production", "prod":
		eventbusEnvironment = publisher.EnvProduction
	case "staging":
		eventbusEnvironment = publisher.EnvStaging
	case "development":
		fallthrough
	case "local":
		eventbusEnvironment = publisher.EnvDevelopment
	}
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{Region: aws.String(region)},
	}))
	c, err := publisher.New(publisher.Config{
		Session:     sess,
		Environment: eventbusEnvironment,
		EventTypes: []string{
			hypeTrainCreate,
			hypeTrainUpdate,
			hypeTrainDelete,
		},
	})
	if err != nil {
		return nil, err
	}
	return &Publisher{
		client:             c,
		hypeTrainConfigsDB: hypeTrainConfigsDB,
	}, err
}

func (p *Publisher) HypeTrainApproaching(ctx context.Context, channelID string, goal int64, eventsRemainingDurations map[int]time.Duration, rewards []percy.Reward, creatorColor string, participants []string) error {
	// not going to implement this into Event bus yet.
	return nil
}

func (p *Publisher) StartHypeTrain(ctx context.Context, hypeTrain percy.HypeTrain, kickoffParticipations ...percy.Participation) error {
	startedAt, err := ptypes.TimestampProto(hypeTrain.StartedAt)
	if err != nil {
		return errors.Wrap(err, "could not convert start time to protobuf timestamp type")
	}

	expiresAt, err := ptypes.TimestampProto(hypeTrain.ExpiresAt)
	if err != nil {
		return errors.Wrap(err, "could not convert expiration time to protobuf timestamp type")
	}

	if len(kickoffParticipations) == 0 {
		return errors.New("0 participations found for starting hype train")
	}

	topContributions, lastContribution := topAndLastContributions(hypeTrain.Config, kickoffParticipations)

	event := &hype_train.Create{
		Id:                hypeTrain.ID,
		BroadcasterUserId: hypeTrain.ChannelID,
		Total:             int64(hypeTrain.LevelProgress().Total),
		Goal:              int64(hypeTrain.LevelProgress().Goal),
		Progress:          int64(hypeTrain.LevelProgress().Progression),
		TopContributions:  topContributions,
		LastContribution:  lastContribution,
		StartedAt:         startedAt,
		ExpiresAt:         expiresAt,
	}
	return p.client.Publish(ctx, event)
}

func (p *Publisher) EndHypeTrain(ctx context.Context, hypeTrain percy.HypeTrain, timeEnded time.Time, reason percy.EndingReason) error {
	config, err := p.hypeTrainConfigsDB.GetConfig(ctx, hypeTrain.ChannelID)
	if err != nil {
		return errors.Wrap(err, "could not fetch config for hype train")
	}

	startedAt, err := ptypes.TimestampProto(hypeTrain.StartedAt)
	if err != nil {
		return errors.Wrap(err, "could not convert start time to protobuf timestamp type")
	}

	endedAt, err := ptypes.TimestampProto(timeEnded)
	if err != nil {
		return errors.Wrap(err, "could not convert expiration time to protobuf timestamp type")
	}

	cooldownEndsAt, err := ptypes.TimestampProto(timeEnded.Add(config.CooldownDuration))
	if err != nil {
		return errors.Wrap(err, "could not convert cooldown end time to protobuf timestamp type")
	}

	topContributions := contributionsFromConductors(hypeTrain)
	totalBits := int64(0)
	totalSubs := int64(0)

	for participationIdentifier, participationTotal := range hypeTrain.Participations {
		if participationIdentifier.Source == percy.SubsSource {
			totalSubs += int64(participationTotal)
		} else if participationIdentifier.Source == percy.BitsSource {
			totalBits += int64(participationTotal)
		}
	}

	event := &hype_train.HypeTrainDelete{
		Id:                hypeTrain.ID,
		BroadcasterUserId: hypeTrain.ChannelID,
		Total:             int64(hypeTrain.LevelProgress().Total),
		Goal:              int64(hypeTrain.LevelProgress().Goal),
		Progress:          int64(hypeTrain.LevelProgress().Progression),
		Level:             int64(hypeTrain.Level()),
		TopContributions:  topContributions,
		StartedAt:         startedAt,
		EndedAt:           endedAt,
		CooldownEndsAt:    cooldownEndsAt,
		TotalBits:         totalBits,
		TotalSubs:         totalSubs,
	}
	return p.client.Publish(ctx, event)
}

func (p *Publisher) HypeTrainUpdate(ctx context.Context, hypeTrain percy.HypeTrain, lastParticipation percy.Participation) error {
	startedAt, err := ptypes.TimestampProto(hypeTrain.StartedAt)
	if err != nil {
		return errors.Wrap(err, "could not convert start time to protobuf timestamp type")
	}

	expiresAt, err := ptypes.TimestampProto(hypeTrain.ExpiresAt)
	if err != nil {
		return errors.Wrap(err, "could not convert expiration time to protobuf timestamp type")
	}

	if len(hypeTrain.Conductors) == 0 {
		return errors.New("unexpected empty conductors map for hype train progression")
	}

	lastContributionProto := &hype_train.ContributionChange{
		Value: &hype_train.Contribution{
			UserId: lastParticipation.User.ID(),
			Type:   participationSourceProto(lastParticipation.ParticipationIdentifier.Source),
			Total:  int64(hypeTrain.Config.EquivalentParticipationPoint(lastParticipation.ParticipationIdentifier, lastParticipation.Quantity)),
		},
	}

	topContributions := contributionsFromConductors(hypeTrain)

	// Convert the *hype_train.Contribution structs into *hype_train.ContributionChange to satisfy the EventBus "update" style schema
	var topContributionsProto []*hype_train.ContributionChange
	for _, c := range topContributions {
		topContributionsProto = append(topContributionsProto, &hype_train.ContributionChange{
			Value: &hype_train.Contribution{
				UserId: c.GetUserId(),
				Type:   c.GetType(),
				Total:  c.GetTotal(),
			},
		})
	}

	event := &hype_train.Update{
		Id:                hypeTrain.ID,
		BroadcasterUserId: hypeTrain.ChannelID,
		Total: &change.Int64Change{
			Value: int64(hypeTrain.LevelProgress().Total),
		},
		Goal: &change.Int64Change{
			Value: int64(hypeTrain.LevelProgress().Goal),
		},
		Progress: &change.Int64Change{
			Updated: true,
			Value:   int64(hypeTrain.LevelProgress().Progression),
		},
		Level: &change.Int64Change{
			Value: int64(hypeTrain.Level()),
		},
		TopContributions: topContributionsProto,
		LastContribution: lastContributionProto,
		StartedAt:        startedAt,
		ExpiresAt: &change.TimestampChange{
			Value: expiresAt,
		},
	}
	return p.client.Publish(ctx, event)
}

//
// The following methods are currently no-ops to meet the percy.Publisher interface since no corresponding event is defined in EventBus
//
func (p *Publisher) PostHypeTrainProgression(ctx context.Context, participation percy.Participation, progress percy.Progress) error {
	return nil
}

func (p *Publisher) LevelUpHypeTrain(ctx context.Context, channelID string, timeToExpire time.Time, progress percy.Progress) error {
	return nil
}

func (p *Publisher) ParticipantRewards(ctx context.Context, channelID string, entitledParticipants percy.EntitledParticipants) error {
	return nil
}

func (p *Publisher) ConductorUpdate(ctx context.Context, channelID string, conductor percy.Conductor) error {
	return nil
}

func (p *Publisher) CooldownExpired(ctx context.Context, channelID string) error {
	return nil
}

func (p *Publisher) StartCelebration(ctx context.Context, channelID string, sender percy.User, amount int64, celebration percy.Celebration) error {
	return nil
}

func (p *Publisher) GetNumberOfPubsubListeners(pubsubTopic, channelID string) (int, error) {
	return 0, nil
}

func contributionsFromConductors(hypeTrain percy.HypeTrain) []*hype_train.Contribution {
	var topContributions []*hype_train.Contribution
	for source, conductor := range hypeTrain.Conductors {
		conductorTotal := int64(0)
		for participationIdentifier, quantity := range conductor.Participations {
			if participationIdentifier.Source == source {
				conductorTotal += int64(hypeTrain.Config.EquivalentParticipationPoint(participationIdentifier, quantity))
			}
		}

		contributionType := participationSourceProto(source)
		if contributionType == hype_train.ContributionType_CONTRIBUTION_TYPE_INVALID {
			logrus.WithField("source", source).Warn("unexpected participation source in eventbus publisher hype train conductor set")
			continue
		}

		topContributions = append(topContributions, &hype_train.Contribution{
			Type:   contributionType,
			UserId: conductor.User.ID(),
			Total:  conductorTotal,
		})
	}
	return topContributions
}

func topAndLastContributions(config percy.HypeTrainConfig, participations []percy.Participation) ([]*hype_train.Contribution, *hype_train.Contribution) {
	contributionMap := make(map[hype_train.ContributionType]map[string]int64)

	// Make the array so it is an empty array instead of nil in the zero-case
	var topContributions []*hype_train.Contribution
	var lastContribution *hype_train.Contribution
	var lastContributionTimestamp time.Time

	for _, p := range participations {
		source := participationSourceProto(p.Source)
		if source == hype_train.ContributionType_CONTRIBUTION_TYPE_INVALID {
			continue
		}

		if contributionMap[source] == nil {
			contributionMap[source] = make(map[string]int64)
		}

		contributionMap[source][p.User.ID()] += participationValue(config, p)

		if lastContribution == nil || p.Timestamp.After(lastContributionTimestamp) {
			lastContributionTimestamp = p.Timestamp
			lastContribution = &hype_train.Contribution{
				UserId: p.User.ID(),
				Total:  participationValue(config, p),
				Type:   source,
			}
		}
	}

	for source, contributors := range contributionMap {
		var topUserID string
		var topTotal int64
		for userID, total := range contributors {
			if total > topTotal {
				topUserID = userID
				topTotal = total
			}
		}
		topContributions = append(topContributions, &hype_train.Contribution{
			UserId: topUserID,
			Total:  topTotal,
			Type:   source,
		})
	}

	return topContributions, lastContribution
}

func participationSourceProto(source percy.ParticipationSource) hype_train.ContributionType {
	switch source {
	case percy.SubsSource:
		return hype_train.ContributionType_CONTRIBUTION_TYPE_SUBSCRIPTION
	case percy.BitsSource:
		return hype_train.ContributionType_CONTRIBUTION_TYPE_BITS
	case percy.AdminSource:
		fallthrough
	case percy.UnknownSource:
		fallthrough
	default:
		return hype_train.ContributionType_CONTRIBUTION_TYPE_INVALID
	}
}

func participationValue(config percy.HypeTrainConfig, p percy.Participation) int64 {
	return int64(config.EquivalentParticipationPoint(percy.ParticipationIdentifier{
		Source: p.Source,
		Action: p.Action,
	}, p.Quantity))
}
