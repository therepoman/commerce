package eventbus

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/metrics"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/schema/pkg/stream"
	"github.com/stretchr/testify/assert"
)

func TestStreamUpWorkerConfig_HandleStreamCreate(t *testing.T) {
	validHeader := &eventbus.Header{
		MessageID:   makeGofrsUUID(),
		EventType:   "",
		CreatedAt:   time.Time{},
		Environment: "",
	}

	// we only care about user id field
	validStreamCreate := &stream.Create{
		UserId: "some-user-id",
	}

	tests := []struct {
		scenario string
		h        *eventbus.Header
		event    *stream.Create
		test     func(t *testing.T, h *eventbus.Header, event *stream.Create)
	}{
		{
			scenario: "rejects when header is nil",
			h:        nil,
			event:    validStreamCreate,
			test: func(t *testing.T, h *eventbus.Header, event *stream.Create) {
				launchPadMock := &internalfakes.FakeLaunchPad{}

				worker := StreamUpWorkerConfig{
					LaunchPad: launchPadMock,
					Mux:       eventbus.NewMux(),
					QueueURL:  "some-queue-url",
					Statter:   &metrics.NoopStatter{},
				}

				err := worker.HandleStreamCreate(context.Background(), h, event)
				assert.Error(t, err)
				assert.Equal(t, 0, launchPadMock.ChannelIsAllowedCallCount())
				assert.Equal(t, 0, launchPadMock.ScheduleBoostOpportunityCallCount())
			},
		},
		{
			scenario: "rejects when event is nil",
			h:        validHeader,
			event:    nil,
			test: func(t *testing.T, h *eventbus.Header, event *stream.Create) {
				launchPadMock := &internalfakes.FakeLaunchPad{}

				worker := StreamUpWorkerConfig{
					LaunchPad: launchPadMock,
					Mux:       eventbus.NewMux(),
					QueueURL:  "some-queue-url",
					Statter:   &metrics.NoopStatter{},
				}

				err := worker.HandleStreamCreate(context.Background(), h, event)
				assert.Error(t, err)
				assert.Equal(t, 0, launchPadMock.ChannelIsAllowedCallCount())
				assert.Equal(t, 0, launchPadMock.ScheduleBoostOpportunityCallCount())
			},
		},
		{
			scenario: "noop when channel is not allowed",
			h:        validHeader,
			event:    validStreamCreate,
			test: func(t *testing.T, h *eventbus.Header, event *stream.Create) {
				launchPadMock := &internalfakes.FakeLaunchPad{}
				launchPadMock.ChannelIsAllowedReturns(false)

				worker := StreamUpWorkerConfig{
					LaunchPad: launchPadMock,
					Mux:       eventbus.NewMux(),
					QueueURL:  "some-queue-url",
					Statter:   &metrics.NoopStatter{},
				}

				err := worker.HandleStreamCreate(context.Background(), h, event)
				assert.NoError(t, err)
				assert.Equal(t, 1, launchPadMock.ChannelIsAllowedCallCount())
				assert.Equal(t, 0, launchPadMock.ScheduleBoostOpportunityCallCount())
			},
		},
		{
			scenario: "when channel is allowed and handler succeeds",
			h:        validHeader,
			event:    validStreamCreate,
			test: func(t *testing.T, h *eventbus.Header, event *stream.Create) {
				launchPadMock := &internalfakes.FakeLaunchPad{}
				launchPadMock.ChannelIsAllowedReturns(true)
				launchPadMock.ScheduleBoostOpportunityReturns(nil)

				worker := StreamUpWorkerConfig{
					LaunchPad: launchPadMock,
					Mux:       eventbus.NewMux(),
					QueueURL:  "some-queue-url",
					Statter:   &metrics.NoopStatter{},
				}

				err := worker.HandleStreamCreate(context.Background(), h, event)
				assert.NoError(t, err)
				assert.Equal(t, 1, launchPadMock.ChannelIsAllowedCallCount())
				assert.Equal(t, 1, launchPadMock.ScheduleBoostOpportunityCallCount())
			},
		},
		{
			scenario: "when channel is allowed but handler fails",
			h:        validHeader,
			event:    validStreamCreate,
			test: func(t *testing.T, h *eventbus.Header, event *stream.Create) {
				launchPadMock := &internalfakes.FakeLaunchPad{}
				launchPadMock.ChannelIsAllowedReturns(true)
				launchPadMock.ScheduleBoostOpportunityReturns(errors.New("handler failed"))

				worker := StreamUpWorkerConfig{
					LaunchPad: launchPadMock,
					Mux:       eventbus.NewMux(),
					QueueURL:  "some-queue-url",
					Statter:   &metrics.NoopStatter{},
				}

				err := worker.HandleStreamCreate(context.Background(), h, event)
				assert.Error(t, err)
				assert.Equal(t, 1, launchPadMock.ChannelIsAllowedCallCount())
				assert.Equal(t, 1, launchPadMock.ScheduleBoostOpportunityCallCount())
			},
		},
	}

	for _, test := range tests {
		tc := test

		t.Run(test.scenario, func(t *testing.T) {
			tc.test(t, tc.h, tc.event)
		})
	}
}
