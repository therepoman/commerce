package eventbus

import (
	"context"
	"encoding/json"
	"errors"
	"math/rand"
	"time"

	"code.justin.tv/commerce/gogogadget/random"
	log "code.justin.tv/commerce/logrus"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"code.justin.tv/commerce/percy/internal/miniexperiments"
	"code.justin.tv/commerce/percy/internal/miniexperiments/c2_bits_pinata"
	"code.justin.tv/commerce/percy/internal/pubsub"
	"code.justin.tv/commerce/percy/internal/sqs/models"
	eventbus "code.justin.tv/eventbus/client"
	subscriber "code.justin.tv/eventbus/client/subscriber/sqsclient"
	"code.justin.tv/eventbus/schema/pkg/cheer"
	"code.justin.tv/eventbus/schema/pkg/stream"
	"code.justin.tv/subs/destiny/destinytwirp"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/golang/protobuf/ptypes/timestamp"
)

const (
	eventBusPinataHandlerLatencyMetric = "eventbus-pinata-handler-latency"
	eventBusPinataHandlerSuccessMetric = "eventbus-pinata-handler-success"
	eventBusPinataHandlerFailureMetric = "eventbus-pinata-handler-failure"

	bitsPinataEndingDomain = "cheering2PointOh/bitsPinata/drop"

	pinataHandlerTimeout = 3 * time.Second
)

type PinataWorkerConfig struct {
	PinataExperimentClient miniexperiments.Client
	Liveline               percy.ChannelStatusDB
	Destiny                destinytwirp.Destiny
	Mux                    *eventbus.Mux
	QueueURL               string
	Statter                statsd.Statter
	BitsPinataDB           percy.BitsPinataDB
	Publisher              pubsub.PinataPublisher
	Payday                 paydayrpc.Payday
}

func NewPinataWorkerRunner(config PinataWorkerConfig, region string) (*subscriber.SQSClient, error) {
	config.registerHandlers()

	awsSession, err := session.NewSession(&aws.Config{
		Region: aws.String(region),
	})
	if err != nil {
		return nil, err
	}

	client, err := subscriber.New(subscriber.Config{
		Session:    awsSession,
		Dispatcher: config.Mux.Dispatcher(),
		QueueURL:   config.QueueURL,
		FineTuning: subscriber.FineTuning{
			DeliverChannelDepth:    0,
			AckChannelDepth:        0,
			ShutdownCancelRequests: time.Second * 1,
		},
		HookCallback: logErrorCallback,
	})
	if err != nil {
		return nil, err
	}

	return client, nil
}

func (w *PinataWorkerConfig) HandleStreamCreate(ctx context.Context, h *eventbus.Header, event *stream.StreamCreate) error {
	start := time.Now()
	ctx, cancel := context.WithTimeout(ctx, pinataHandlerTimeout)
	defer cancel()

	// Check event is valid
	err := w.validateStreamCreateEventForPinata(h, event)
	if err != nil {
		return err
	}

	// Check stream type
	if event.StreamType != stream.StreamType_STREAM_TYPE_LIVE {
		// in the case of non-live content we want to bail and not process the stream for pinata usages
		return nil
	}

	// Check experiment
	treatment, err := w.PinataExperimentClient.GetTreatment(event.GetUserId())
	if err != nil {
		return err
	}
	if treatment == c2_bits_pinata.TreatmentControl {
		// if we're in the Control group, we can skip doing any pinata logic.
		return nil
	}

	// Check up status
	channelIsStreaming, err := w.Liveline.ChannelIsStreaming(ctx, event.GetUserId())
	if err != nil {
		return err
	}
	if !channelIsStreaming {
		log.WithFields(log.Fields{
			"streamID":  event.GetId(),
			"channelID": event.GetUserId(),
			"startedAt": event.GetStartedAt(),
		}).Warn("Got stream up event, but stream is not live at time of processing")
		return nil
	}

	// Calculate when we should drop, somewhere between 15 minutes in and 2 hours in.
	minutesAfterStart := rand.Intn(105) + 15
	pinataDropTime := event.GetStartedAt().AsTime().Add(time.Duration(minutesAfterStart) * time.Minute)
	log.WithFields(log.Fields{
		"streamID":          event.GetId(),
		"channelID":         event.GetUserId(),
		"startedAt":         event.GetStartedAt(),
		"minutesAfterStart": minutesAfterStart,
		"dropTime":          pinataDropTime,
	}).Info("Pinata drop scheduled")

	// Define payload
	data := models.BitsPinataDropEvent{
		ChannelID: event.GetUserId(),
		StreamID:  event.GetId(),
	}
	payload, err := json.Marshal(data)
	if err != nil {
		return err
	}

	// Schedule with Destiny
	_, err = w.Destiny.WriteEvents(ctx, &destinytwirp.WriteEventsRequest{
		Events: []*destinytwirp.Event{
			{
				Id:      event.GetId(),
				Domain:  bitsPinataEndingDomain,
				Payload: payload,
				SendAt: &timestamp.Timestamp{
					Seconds: pinataDropTime.Unix(),
				},
				MaxRetries: 5,
			},
		},
	})
	if err != nil {
		return err
	}

	go func() {
		_ = w.Statter.TimingDuration(eventBusPinataHandlerLatencyMetric, time.Since(start), 1.0)
		if err == nil {
			_ = w.Statter.Inc(eventBusPinataHandlerSuccessMetric, 1, 1.0)
		} else {
			_ = w.Statter.Inc(eventBusPinataHandlerFailureMetric, 1, 1.0)
		}
	}()

	return nil
}

func (w *PinataWorkerConfig) HandleCheerCreate(ctx context.Context, h *eventbus.Header, event *cheer.CheerCreate) error {
	start := time.Now()
	ctx, cancel := context.WithTimeout(ctx, pinataHandlerTimeout)
	defer cancel()

	// validate event
	err := w.validateCheerCreateEventForPinata(event)
	if err != nil {
		return err
	}

	// check and retrieve user id.
	cheererUserId, err := getCheererUserId(event)
	if err != nil {
		return err
	} else if cheererUserId == nil {
		return nil // we don't process anon cheers
	}

	// check if channel is live
	channelIsStreaming, err := w.Liveline.ChannelIsStreaming(ctx, event.GetToUserId())
	if err != nil {
		return err
	}
	if !channelIsStreaming {
		return nil // we don't process cheers for offline channels.
	}

	// Transaction iterate on pinata.
	pinata, err := w.BitsPinataDB.AddBitsPinataContribution(ctx, event.GetToUserId(), int(event.GetBits()))
	if err != nil {
		return err
	}
	if pinata == nil {
		return nil // we don't process cheers when there isn't an active pinata.
	}
	if len(pinata.StreamID) == 0 {
		return errors.New("pinata DB returned pinata with empty stream ID")
	}

	// now that we updated the pinata dynamo entry, let's see if it's been breached?
	hasPinataBeenBreached := pinata.BitsContributed >= pinata.BitsToBreak

	// we'll use the stream id as the idempotency key so we can just retry this over and over
	if hasPinataBeenBreached {
		_, err := w.Payday.GrantPinataBonusBits(ctx, &paydayrpc.GrantPinataBonusBitsReq{
			TransactionId: pinata.StreamID,
			ChannelId:     pinata.ChannelID,
			PinataBonus:   w.getBonusBits(pinata.BitsToBreak),
		})
		if err != nil {
			return err
		}
	} else {
		// Define payload
		data := models.BitsPinataDropEvent{
			ChannelID: event.GetToUserId(),
			StreamID:  pinata.StreamID,
		}
		payload, err := json.Marshal(data)
		if err != nil {
			return err
		}

		// Update timer in Destiny (if pinata isn't already broken)
		pinataExpirationTime := time.Now().Add(dynamodb.BitsPinataContributionTimeout)
		_, err = w.Destiny.WriteEvents(ctx, &destinytwirp.WriteEventsRequest{
			Events: []*destinytwirp.Event{
				{
					Id:      pinata.StreamID,
					Domain:  bitsPinataEndingDomain,
					Payload: payload,
					SendAt: &timestamp.Timestamp{
						Seconds: pinataExpirationTime.Unix(),
					},
					MaxRetries: 5,
				},
			},
		})
		if err != nil {
			return err
		}
	}

	// Send pubsub to the front end.
	err = w.Publisher.PublishPinataActivity(ctx, pinata.PinataID, event.GetToUserId(), *cheererUserId, event.GetBits(), hasPinataBeenBreached)
	if err != nil {
		return err
	}

	go func() {
		_ = w.Statter.TimingDuration(eventBusPinataHandlerLatencyMetric, time.Since(start), 1.0)
		if err == nil {
			_ = w.Statter.Inc(eventBusPinataHandlerSuccessMetric, 1, 1.0)
		} else {
			_ = w.Statter.Inc(eventBusPinataHandlerFailureMetric, 1, 1.0)
		}
	}()
	return nil
}

// So this function generates a bonus amount given this table of bucketing:
//
// | Bonus  | Chance    |
// | ------ | --------- |
// | 5%     | 80%       |
// | 10%    | 9%        |
// | 25%    | 6%        |
// | 50%    | 4%        |
// | 100%   | 1%        |
//
// see https://docs.google.com/spreadsheets/d/1vxmjB-m_DYwKnWBub4x2hsz_AISTQElvrOqDnlI8XZM/edit#gid=0
// for reference on math reasoning.
func (w *PinataWorkerConfig) getBonusBits(bitsToBreak int64) int64 {
	bonusModifier := 0.05
	roll := random.Int(1, 100)

	if roll > 99 {
		_ = w.Statter.Inc("pinata-bonus-100-percent-rolled", 1, 1.0)
		bonusModifier = 1.00
	} else if roll > 95 {
		_ = w.Statter.Inc("pinata-bonus-50-percent-rolled", 1, 1.0)
		bonusModifier = 0.5
	} else if roll > 89 {
		_ = w.Statter.Inc("pinata-bonus-25-percent-rolled", 1, 1.0)
		bonusModifier = 0.25
	} else if roll > 80 {
		_ = w.Statter.Inc("pinata-bonus-10-percent-rolled", 1, 1.0)
		bonusModifier = 0.1
	} else {
		_ = w.Statter.Inc("pinata-bonus-5-percent-rolled", 1, 1.0)
	}

	return int64(bonusModifier * float64(bitsToBreak))
}

func (w *PinataWorkerConfig) validateCheerCreateEventForPinata(event *cheer.CheerCreate) error {
	if event == nil {
		return errors.New("cheer create event is nil")
	}
	if len(event.GetToUserId()) == 0 {
		return errors.New("cheer create event has empty to-user ID")
	}
	if event.GetFrom() == nil {
		return errors.New("cheer create event has nil from-user type")
	}
	return nil
}

func (w *PinataWorkerConfig) validateStreamCreateEventForPinata(h *eventbus.Header, event *stream.StreamCreate) error {
	if h == nil {
		return errors.New("stream create event header is nil")
	}
	if event == nil {
		return errors.New("stream create event is nil")
	}
	if len(event.GetUserId()) == 0 {
		return errors.New("stream create event has empty user ID")
	}
	if len(event.GetId()) == 0 {
		return errors.New("stream create event has empty stream ID")
	}
	if event.GetStartedAt() == nil {
		return errors.New("stream create event has nil started at timestamp")
	}
	return nil
}

// Returns user id for non-anon cheers, nil for anon cheers and an error if there was issue parsing the from user.
func getCheererUserId(event *cheer.CheerCreate) (*string, error) {
	// Check user who cheered
	switch event.GetFrom().(type) {
	case *cheer.CheerCreate_User:
		if event.GetUser() == nil || len(event.GetUser().Id) == 0 {
			log.WithFields(log.Fields{
				"transactionid": event.BitsTransactionId,
			}).Error("cheering event missing source user")
			return nil, errors.New("cheering event missing source user")
		}
		return &event.GetUser().Id, nil
	case *cheer.CheerCreate_AnonymousUser:
		return nil, nil
	default:
		log.WithFields(log.Fields{
			"transactionid": event.BitsTransactionId,
		}).Error("invalid From type in cheering event")
		return nil, errors.New("invalid From type in cheering event")
	}
}

func (w *PinataWorkerConfig) registerHandlers() {
	stream.RegisterCreateHandler(w.Mux, w.HandleStreamCreate)
	cheer.RegisterCreateHandler(w.Mux, w.HandleCheerCreate)
}
