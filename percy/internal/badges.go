package percy

import (
	"context"
	"time"

	"code.justin.tv/commerce/logrus"
)

type Badge string

const (
	UnknownBadge          = Badge("unknown")
	CurrentConductorBadge = Badge("current_conductor")
	FormerConductorBadge  = Badge("former_conductor")
)

//go:generate counterfeiter -generate

//counterfeiter:generate . BadgesDB

type BadgesDB interface {
	GetBadges(ctx context.Context, userID, channelID string) ([]Badge, error)
	SetBadges(ctx context.Context, userID, channelID string, badges []Badge) error
	SetBulkBadges(ctx context.Context, userIDs []string, channelID string, badges []Badge) error
	GetAllBadgeChannelsForUserID(ctx context.Context, userID string, cursor string) ([]string, string, error)
	GetAllBadgeUsersForChannelID(ctx context.Context, channelID string, cursor string) ([]string, string, error)
	DeleteBulkBadges(ctx context.Context, badgesToDelete map[string][]string) error
}

//counterfeiter:generate . BadgesClient

type BadgesClient interface {
	GetSelectedHypeTrainBadge(ctx context.Context, userID, channelID string) (*Badge, error)
	SetSelectedHypeTrainBadge(ctx context.Context, userID, channelID string, badge Badge) error
	RemoveSelectedHypeTrainBadge(ctx context.Context, userID, channelID string) error
	GetBadgeDisplayInfo(ctx context.Context, channelID string, badges []Badge) (map[Badge]string, error)
}

func (e *engine) GetBadges(ctx context.Context, userID, channelID string) ([]Badge, error) {
	return e.BadgesDB.GetBadges(ctx, userID, channelID)
}

func (e *engine) SetBadges(ctx context.Context, userID, channelID string, badges []Badge) error {
	return e.BadgesDB.SetBadges(ctx, userID, channelID, badges)
}

//go:generate counterfeiter . ConductorBadgesHandler

type ConductorBadgesHandler interface {
	UpdateCurrentConductorBadgeHolders(ctx context.Context, hypeTrain HypeTrain) error
	RemoveCurrentConductorBadgeHolders(ctx context.Context, hypeTrain HypeTrain) error
}

type conductorBadgesHandler struct {
	ConductorsDB ConductorsDB
	BadgesDB     BadgesDB
	BadgesClient BadgesClient
	Spade        Spade
}

func NewConductorBadgesHandler(conductorsDB ConductorsDB, badgesDB BadgesDB, client BadgesClient, spade Spade) *conductorBadgesHandler {
	return &conductorBadgesHandler{
		ConductorsDB: conductorsDB,
		BadgesDB:     badgesDB,
		BadgesClient: client,
		Spade:        spade,
	}
}

func (h *conductorBadgesHandler) UpdateCurrentConductorBadgeHolders(ctx context.Context, hypeTrain HypeTrain) error {
	// In some rare cases we can have no conductors
	// e.g. when all eligible users got moderated
	if len(hypeTrain.Conductors) == 0 {
		return nil
	}

	log := logrus.WithFields(logrus.Fields{
		"hypeTrainID": hypeTrain.ID,
		"channelID":   hypeTrain.ChannelID,
	})

	newConductorBadgeHolders := make([]Conductor, 0)
	newConductorBadgeHolderIDs := make([]string, 0)
	newConductorBadgeHolderIDsMap := make(map[string]interface{})
	for _, c := range hypeTrain.Conductors {
		if !c.User.IsAnonymous() {
			newConductorBadgeHolders = append(newConductorBadgeHolders, c)
			newConductorBadgeHolderIDsMap[c.User.ID()] = nil
		}
	}

	for key := range newConductorBadgeHolderIDsMap {
		newConductorBadgeHolderIDs = append(newConductorBadgeHolderIDs, key)
	}

	err := h.ConductorsDB.SetConductors(ctx, hypeTrain.ChannelID, newConductorBadgeHolders)
	if err != nil {
		log.WithError(err).Error("failed to set current conductors to conductor DB")
		return err
	}

	err = h.BadgesDB.SetBulkBadges(ctx, newConductorBadgeHolderIDs, hypeTrain.ChannelID, []Badge{CurrentConductorBadge})
	if err != nil {
		log.WithField("userIDs", newConductorBadgeHolderIDs).WithError(err).Error("failed to set badges for users in channel")
		return err
	}

	go func() {
		// Wait until the above DynamoDB write settles so that Badges service can get the correct available hype train badges for the user / channel
		time.Sleep(1 * time.Second)

		for _, c := range newConductorBadgeHolders {
			err = h.BadgesClient.SetSelectedHypeTrainBadge(context.Background(), c.User.ID(), hypeTrain.ChannelID, CurrentConductorBadge)
			// if we fail to auto equip the badge, we should know about it but it shouldn't fail the flow since
			// all that matters for them to equip the badge is that we have it marked in our database.
			if err != nil {
				log.WithField("userID", c.User.ID()).WithError(err).Error("failed to auto equip badge for user in channel")
			}
		}
	}()

	go func() {
		badgeUpdates := make([]HypeTrainBadgeUpdateEventParams, 0)
		for _, c := range newConductorBadgeHolderIDs {
			badgeUpdates = append(badgeUpdates, HypeTrainBadgeUpdateEventParams{
				ChannelID: hypeTrain.ChannelID,
				UserID:    c,
				TrainID:   hypeTrain.ID,
				BadgeName: CurrentConductorBadgeUpdate,
				Action:    EarnedBadge,
			})
		}

		err := h.Spade.SendBadgeUpdateEvents(context.Background(), badgeUpdates...)
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"trainID":   hypeTrain.ID,
				"channelID": hypeTrain.ChannelID,
				"badgeName": CurrentConductorBadgeUpdate,
				"action":    EarnedBadge,
			}).WithError(err).Error("failed to post datascience metrics for earning of conductor badge")
		}

		badgeDisplaySelections := make([]HypeTrainBadgeDisplaySelectionEventParams, 0)
		for _, c := range newConductorBadgeHolderIDs {
			badgeDisplaySelections = append(badgeDisplaySelections, HypeTrainBadgeDisplaySelectionEventParams{
				ChannelID: hypeTrain.ChannelID,
				UserID:    c,
				BadgeName: CurrentConductorBadgeUpdate,
				Action:    AutoEquipBadge,
			})
		}

		err = h.Spade.SendBadgeDisplaySelectionEvents(context.Background(), badgeDisplaySelections...)
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"channelID": hypeTrain.ChannelID,
				"badgeName": CurrentConductorBadgeUpdate,
				"action":    AutoEquipBadge,
			}).WithError(err).Error("failed to post datascience metrics for auto equip of conductor badge")
		}
	}()

	return nil
}

func (h *conductorBadgesHandler) RemoveCurrentConductorBadgeHolders(ctx context.Context, hypeTrain HypeTrain) error {
	log := logrus.WithFields(logrus.Fields{
		"hypeTrainID": hypeTrain.ID,
		"channelID":   hypeTrain.ChannelID,
	})

	currentConductors, err := h.ConductorsDB.GetConductors(ctx, hypeTrain.ChannelID)
	if err != nil {
		log.WithError(err).Error("failed to get current conductors from conductors DB")
		return err
	}

	// if there are no current conductors, then we're fine just return nil
	if len(currentConductors) == 0 {
		return nil
	}

	// reset current conductors
	err = h.ConductorsDB.SetConductors(ctx, hypeTrain.ChannelID, []Conductor{})
	if err != nil {
		log.WithError(err).Error("failed to set current conductors to conductor DB")
		return err
	}

	currentConductorIDs := make([]string, 0)
	currentConductorIDsMap := make(map[string]interface{})
	for _, c := range currentConductors {
		if !c.User.IsAnonymous() {
			currentConductorIDsMap[c.User.ID()] = nil
		}
	}

	for key := range currentConductorIDsMap {
		currentConductorIDs = append(currentConductorIDs, key)
	}

	// return early if we have no conductors
	if len(currentConductorIDs) == 0 {
		return nil
	}

	err = h.BadgesDB.SetBulkBadges(ctx, currentConductorIDs, hypeTrain.ChannelID, []Badge{FormerConductorBadge})
	if err != nil {
		log.WithField("userIDs", currentConductorIDs).WithError(err).Error("failed to set badges for users in channel")
		return err
	}

	go func() {
		for _, userID := range currentConductorIDs {
			badge, err := h.BadgesClient.GetSelectedHypeTrainBadge(context.Background(), userID, hypeTrain.ChannelID)
			if err != nil {
				log.WithField("userID", userID).WithError(err).Error("failed to fetch equipped channel badge from badges service")
			}

			if badge != nil && *badge == CurrentConductorBadge {
				err := h.BadgesClient.RemoveSelectedHypeTrainBadge(context.Background(), userID, hypeTrain.ChannelID)
				if err != nil {
					log.WithField("userID", userID).WithError(err).Error("failed to remove equipped channel badge in badges service")
				}
			}
		}
	}()

	go func() {
		badgeUpdates := make([]HypeTrainBadgeUpdateEventParams, 0)
		for _, c := range currentConductorIDs {
			badgeUpdates = append(badgeUpdates, HypeTrainBadgeUpdateEventParams{
				ChannelID: hypeTrain.ChannelID,
				UserID:    c,
				TrainID:   hypeTrain.ID,
				BadgeName: CurrentConductorBadgeUpdate,
				Action:    ExpiredBadge,
			}, HypeTrainBadgeUpdateEventParams{
				ChannelID: hypeTrain.ChannelID,
				UserID:    c,
				TrainID:   hypeTrain.ID,
				BadgeName: FormerConductorBadgeUpdate,
				Action:    EarnedBadge,
			})
		}

		err := h.Spade.SendBadgeUpdateEvents(context.Background(), badgeUpdates...)
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"trainID": hypeTrain.ID,
			}).WithError(err).Error("failed to post datascience metrics for expiration / earning of conductor badges")
		}

		badgeDisplaySelections := make([]HypeTrainBadgeDisplaySelectionEventParams, 0)
		for _, c := range currentConductorIDs {
			badgeDisplaySelections = append(badgeDisplaySelections, HypeTrainBadgeDisplaySelectionEventParams{
				ChannelID: hypeTrain.ChannelID,
				UserID:    c,
				BadgeName: CurrentConductorBadgeUpdate,
				Action:    RemoveBadge,
			})
		}

		err = h.Spade.SendBadgeDisplaySelectionEvents(context.Background(), badgeDisplaySelections...)
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"channelID": hypeTrain.ChannelID,
				"badgeName": CurrentConductorBadgeUpdate,
				"action":    RemoveBadge,
			}).WithError(err).Error("failed to post datascience metrics for removal of current conductor badge")
		}
	}()

	return nil
}
