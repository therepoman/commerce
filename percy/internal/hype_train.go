package percy

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/internal/miniexperiments/ht_approaching"
	"code.justin.tv/commerce/percy/models"
	"github.com/google/uuid"
	"github.com/pkg/errors"
)

type EndingReason string

const (
	EndingReasonCompleted = EndingReason("COMPLETED")
	EndingReasonExpired   = EndingReason("EXPIRED")
	EndingReasonError     = EndingReason("ERROR")

	hypeTrainKickedOffMetric            = "hype-train-kicked-off"
	hypeTrainLevelCompletedMetricFormat = "hype-train-level-%d-completed"
	hypeTrainKickoffLatencyMetric       = "hype-train-kickoff-latency"
	hypeTrainKickoffPointsMetric        = "hype-train-kickoff-points"
	hypeTrainCompletionPointsMetric     = "hype-train-completion-points"
	hypeTrainNumberOfParticipantsMetric = "hype-train-number-of-participants"
)

type Conductor struct {
	Source         ParticipationSource `json:"source"`
	User           User                `json:"user"`
	Participations ParticipationTotals `json:"participations"`
}

type HypeTrain struct {
	ChannelID      string                            `json:"channel_id"`
	ID             string                            `json:"id"`
	StartedAt      time.Time                         `json:"started_at"`
	ExpiresAt      time.Time                         `json:"expires_at"`
	UpdatedAt      time.Time                         `json:"updated_at"`
	EndedAt        *time.Time                        `json:"ended_at"`
	EndingReason   *EndingReason                     `json:"ending_reason"`
	Config         HypeTrainConfig                   `json:"config"`
	Participations ParticipationTotals               `json:"participations"`
	Conductors     map[ParticipationSource]Conductor `json:"conductors"`
}

type Progress struct {
	Setting LevelSetting `json:"level"`

	// Progression is the amount of participation points accumulated for the current level.
	// Does not include participation points used to complete previous levels.
	Progression ParticipationPoint `json:"value"`

	// Goal is the amount of additional participation points required to complete the current level.
	// Does not include participation points required to complete previous levels.
	Goal ParticipationPoint `json:"goal"`

	// Total participation points accumulated throughout the hype train progress, across all levels
	Total ParticipationPoint `json:"total"`

	// Number of seconds left before this hype train ends
	RemainingSeconds int `json:"remaining_seconds"`
}

func NewHypeTrain(config HypeTrainConfig, id string) HypeTrain {
	now := time.Now().UTC()

	return HypeTrain{
		ChannelID:      config.ChannelID,
		ID:             id,
		StartedAt:      now,
		ExpiresAt:      now.Add(config.LevelDuration),
		UpdatedAt:      now,
		Config:         config,
		Participations: make(ParticipationTotals),
		Conductors:     make(map[ParticipationSource]Conductor),
	}
}

func (hypeTrain *HypeTrain) IsValid() bool {
	if len(hypeTrain.ChannelID) == 0 ||
		len(hypeTrain.ID) == 0 ||
		!hypeTrain.Config.IsValid() {
		return false
	}
	return true
}

func (hypeTrain *HypeTrain) IsActive() bool {
	return hypeTrain.StartedAt.Before(time.Now()) &&
		hypeTrain.ExpiresAt.After(time.Now()) &&
		hypeTrain.EndedAt == nil
}

// ParticipationPoints returns the current participation points
// based on the cumulated participation totals.
func (hypeTrain *HypeTrain) ParticipationPoints() ParticipationPoint {
	points := ParticipationPoint(0)
	for identifier, quantity := range hypeTrain.Participations {
		points += hypeTrain.Config.EquivalentParticipationPoint(identifier, quantity)
	}

	return points
}

func (hypeTrain *HypeTrain) Level() int {
	totalPoints := hypeTrain.ParticipationPoints()
	settings := hypeTrain.Config.LevelSettings()
	level := 0

	for _, setting := range settings {
		level += 1
		if totalPoints < setting.Goal {
			break
		}
	}

	return level
}

func (hypeTrain *HypeTrain) CompletedLevel() int {
	totalPoints := hypeTrain.ParticipationPoints()
	settings := hypeTrain.Config.LevelSettings()
	level := 0

	for _, setting := range settings {
		if totalPoints < setting.Goal {
			break
		}
		level += 1
	}

	return level
}

func (hypeTrain *HypeTrain) LevelProgress() Progress {
	totalPoints := hypeTrain.ParticipationPoints()
	settings := hypeTrain.Config.LevelSettings()

	remainingSeconds := 0
	now := time.Now()
	if now.Before(hypeTrain.ExpiresAt) {
		remainingSeconds = int(hypeTrain.ExpiresAt.Sub(now).Seconds())
	}

	prevGoal := ParticipationPoint(0)
	for _, setting := range settings {
		if totalPoints < setting.Goal {
			return Progress{
				Setting:          setting,
				Progression:      totalPoints - prevGoal,
				Goal:             setting.Goal - prevGoal,
				Total:            totalPoints,
				RemainingSeconds: remainingSeconds,
			}
		}

		prevGoal = setting.Goal
	}

	// current total participation point is over the highest level's goal
	// note that it's possible to have Current >= Goal.
	finalLevel := settings[len(settings)-1]
	if len(settings) > 1 {
		prevGoal = settings[len(settings)-2].Goal
	} else {
		prevGoal = ParticipationPoint(0)
	}

	return Progress{
		Setting:          finalLevel,
		Progression:      totalPoints - prevGoal,
		Goal:             finalLevel.Goal - prevGoal,
		Total:            totalPoints,
		RemainingSeconds: remainingSeconds,
	}
}

// End marks a on-going hype train as ended with the current time and the provided ending reason.
func (hypeTrain *HypeTrain) End(reason EndingReason) error {
	if !hypeTrain.IsActive() {
		return errors.New("hype train is no longer active")
	}

	hypeTrain.EndedAt = pointers.TimeP(time.Now().UTC())
	hypeTrain.EndingReason = &reason
	return nil
}

func (hypeTrain *HypeTrain) AddParticipation(participation Participation) {
	if hypeTrain.Participations == nil {
		hypeTrain.Participations = make(ParticipationTotals)
	}

	hypeTrain.Participations[participation.ParticipationIdentifier] += participation.Quantity
}

func (hypeTrain *HypeTrain) SetParticipations(participations ParticipationTotals) {
	if hypeTrain.Participations == nil {
		hypeTrain.Participations = make(ParticipationTotals)
	}

	for identifier, quantity := range participations {
		hypeTrain.Participations[identifier] += quantity
	}
}

func (hypeTrain *HypeTrain) IsStalled() bool {
	// Consider a hype train stalled if it has not been marked as ended {levelDuration} after its expiration time
	return hypeTrain.EndedAt == nil && time.Now().After(hypeTrain.ExpiresAt.Add(hypeTrain.Config.LevelDuration))
}

//go:generate counterfeiter . HypeTrainDB

type HypeTrainDB interface {
	// GetMostRecentHypeTrain returns the most recent hype train for the channel.
	// If no hype train was ever executed, returns nil.
	GetMostRecentHypeTrain(ctx context.Context, channelID string) (*HypeTrain, error)

	UpsertHypeTrain(ctx context.Context, hypeTrain HypeTrain) error
	CreateHypeTrain(ctx context.Context, hypeTrain HypeTrain) error

	// Update current hype train conductor based on the conductor's participation source
	UpdateConductor(ctx context.Context, channelID string, hypeTrainID string, conductor Conductor) (HypeTrain, error)

	// Remove the current conductor based on the participation source
	RemoveConductor(ctx context.Context, channelID string, hypeTrainID, userID string, source ParticipationSource) (HypeTrain, error)

	// AddParticipation adds new participation to the hype train and returns the updated hype train
	AddParticipation(ctx context.Context, channelID, hypeTrainID string, participation Participation) (HypeTrain, error)

	// EndHypeTrain ends the currently ongoing hype train with a reason, and returns the ended hype train.
	// If there is no ongoing hype train in the channel, returns an error.
	EndHypeTrain(ctx context.Context, channelID, hypeTrainID string, reason EndingReason) (HypeTrain, error)

	// ExtendHypeTrain extends the currently ongoing hype train to expires at the new timestamp, and returns the extended hype train.
	// If there is no ongoing hype train in the channel, returns an error.
	ExtendHypeTrain(ctx context.Context, channelID, hypeTrainID string, expresAt time.Time) (HypeTrain, error)

	// Find hype trains that started within the provided timestamps in the params.
	QueryHypeTrainByStartTime(ctx context.Context, channelID string, params HypeTrainQueryParams) ([]HypeTrain, error)

	// Get hype train by the provided ID, returns nil if no hype train is found with the ID.
	GetHypeTrainByID(ctx context.Context, id string) (*HypeTrain, error)
}

type HypeTrainQueryParams struct {
	From *time.Time
	To   *time.Time
}

func (eng *engine) GetOngoingHypeTrain(ctx context.Context, channelID string) (*HypeTrain, *Approaching, error) {
	if len(channelID) == 0 {
		return nil, nil, errors.New("channel ID is blank")
	}

	hypeTrain, err := eng.GetMostRecentHypeTrain(ctx, channelID)
	if err != nil {
		return nil, nil, err
	}
	if hypeTrain == nil || !hypeTrain.IsActive() {
		approaching, err := eng.ApproachingDB.GetApproaching(ctx, channelID)
		if err != nil {
			return nil, nil, err
		}

		if approaching != nil && approaching.IsActive() {
			return nil, approaching, nil
		}

		return nil, nil, nil
	}

	return hypeTrain, nil, nil
}

func (eng *engine) GetMostRecentHypeTrain(ctx context.Context, channelID string) (*HypeTrain, error) {
	if len(channelID) == 0 {
		return nil, errors.New("channel ID is blank")
	}

	hypeTrain, err := eng.HypeTrainDB.GetMostRecentHypeTrain(ctx, channelID)
	if err != nil {
		return nil, err
	}

	if hypeTrain == nil {
		return nil, nil
	}

	if !hypeTrain.IsValid() {
		return nil, errors.New("invalid hype train")
	}

	return hypeTrain, nil
}

func (eng *engine) GetHypeTrainByID(ctx context.Context, hypeTrainID string) (*HypeTrain, error) {
	if len(hypeTrainID) == 0 {
		return nil, errors.New("hype train ID is blank")
	}

	hypeTrain, err := eng.HypeTrainDB.GetHypeTrainByID(ctx, hypeTrainID)
	if err != nil {
		return nil, err
	}

	if hypeTrain == nil {
		return nil, nil
	}

	if !hypeTrain.IsValid() {
		return nil, errors.New("invalid hype train")
	}

	return hypeTrain, nil
}

// Admin use only; Twitch staff forces a Hype Train to be initiated via scripts/Twirp API
func (eng *engine) UpsertHypeTrain(ctx context.Context, hypeTrain HypeTrain) error {
	if !hypeTrain.IsValid() {
		return errors.New("Invalid hype train")
	}

	if err := eng.HypeTrainDB.UpsertHypeTrain(ctx, hypeTrain); err != nil {
		return err
	}

	if err := eng.ScheduleHypeTrainEnding(ctx, hypeTrain); err != nil {
		return err
	}

	return nil
}

func (eng *engine) EndHypeTrain(ctx context.Context, hypeTrain HypeTrain) error {
	if len(hypeTrain.ChannelID) == 0 || len(hypeTrain.ID) == 0 {
		return errors.New("invalid channelID or hypeTrainID")
	}

	logger := log.WithFields(log.Fields{
		"channelID":   hypeTrain.ChannelID,
		"hypeTrainID": hypeTrain.ID,
	})
	endingReason := EndingReasonExpired
	var entitledParticipants EntitledParticipants
	var participants []Participant

	// Entitle users if the hypetrain completed a level
	if hypeTrain.CompletedLevel() > 0 {
		endingReason = EndingReasonCompleted

		var err error
		participants, err = eng.ParticipantDB.GetHypeTrainParticipants(ctx, hypeTrain.ID)
		if err != nil {
			return errors.Wrap(err, "failed to fetch participants")
		}

		entitledParticipants, err = eng.EntitleParticipants(ctx, hypeTrain, participants...)
		if err != nil {
			return errors.Wrap(err, "failed to entitle participants")
		}

		if hypeTrain.Config.HasConductorBadges {
			go func() {
				err = eng.ConductorBadgesHandler.UpdateCurrentConductorBadgeHolders(context.Background(), hypeTrain)
				if err != nil {
					logger.WithError(err).Error("Failed to update conductor badges on hype train end")
				}
			}()
		}

		// FIXME: matching a change in Mako which added a little delay before refreshing the emote picker upon entitlements
		// https://git-aws.internal.justin.tv/commerce/mako/pull/774
		// This is to combat an issue where Materia are reading staled data from GSI before replication completes
		go func() {
			time.Sleep(3 * time.Second)

			for i := 0; i < 3; i++ {
				err := eng.Publisher.ParticipantRewards(context.Background(), hypeTrain.ChannelID, entitledParticipants)
				if err != nil {
					logger.WithError(err).Error("Failed to publish notification of rewards to users")
				} else {
					break
				}
			}
		}()
	}

	// End hype train in DB if EndedAt hasn't been marked
	// If the train has already ended (likely from the previous retry), don't try to end it again in DB
	endedHypeTrain := hypeTrain
	var err error
	if endedHypeTrain.EndedAt == nil {
		endedHypeTrain, err = eng.HypeTrainDB.EndHypeTrain(ctx, hypeTrain.ChannelID, hypeTrain.ID, endingReason)
		if err != nil {
			return err
		}
	}

	err = eng.Publisher.EndHypeTrain(ctx, endedHypeTrain, *endedHypeTrain.EndedAt, endingReason)
	if err != nil {
		return err
	}

	go func() {
		_ = eng.Statter.Inc(fmt.Sprintf(hypeTrainLevelCompletedMetricFormat, endedHypeTrain.CompletedLevel()), 1, 1.0)
		_ = eng.Statter.Inc(hypeTrainCompletionPointsMetric, int64(endedHypeTrain.ParticipationPoints()), 1.0)
		_ = eng.Statter.Inc(hypeTrainNumberOfParticipantsMetric, int64(len(participants)), 1.0)
	}()

	// Send data science events for participants
	go eng.SendHypeTrainParticipantEvents(hypeTrain, entitledParticipants, participants...)

	// Publish end progression event to spade
	finalLevel := hypeTrain.Config.LevelSettings()[len(hypeTrain.Config.LevelSettings())-1]
	if hypeTrain.CompletedLevel() == finalLevel.Level {
		go eng.SendHypeTrainProgressEvents(hypeTrain, "END COMPLETED")
	} else {
		go eng.SendHypeTrainProgressEvents(hypeTrain, "END TIMED OUT")
	}

	// Remove the tag "Hype Train" from this channel
	go func() {
		err := eng.DynamicTag.RemoveTag(context.Background(), hypeTrain.ChannelID)
		if err != nil {
			logger.WithError(err).Error("Failed to remove a dynamic tag")
		}
	}()

	// Publish Hype Train "end" event to Activity Feed
	go func() {
		err := eng.SNSPublisher.PublishActivityFeedEvent(context.Background(), hypeTrain, models.HypeTrainEnded)
		if err != nil {
			logger.WithError(err).Error("Failed to send Hype Train end notification to Activity Feed")
		}
	}()

	if len(endedHypeTrain.Conductors) == 0 {
		logger.Warn("Hype train ended with no conductor")
	}

	return nil
}

/*
A Hype Train can be started if:

	- The corresponding HypeTrainConfig is enabled
	- There isn't an ongoing active Hype Train for this channel
	- It isn't within the cooldown period of a Hype Train
	- There isn't an active Bits Pinata for this channel
*/
func (eng *engine) CanStartHypeTrain(ctx context.Context, channelID string) (bool, error) {
	if len(channelID) == 0 {
		return false, errors.New("channelID is missing")
	}

	config, err := eng.GetHypeTrainConfig(ctx, channelID)
	if err != nil {
		return false, err
	}

	// is hype train disabled
	if !config.Enabled {
		return false, nil
	}
	hypeTrain, err := eng.HypeTrainDB.GetMostRecentHypeTrain(ctx, channelID)
	if err != nil {
		return false, err
	}

	// is bits pinata active
	bitsPinata, err := eng.BitsPinataDB.GetActiveBitsPinata(ctx, channelID)
	if err != nil {
		return false, errors.Wrap(err, "failed getting active bits pinata")
	}
	if bitsPinata != nil {
		return false, nil
	}

	if hypeTrain == nil {
		return true, nil
	}

	if !hypeTrain.IsValid() {
		return false, errors.New("Invalid hype train")
	}

	// is hype train ongoing
	if hypeTrain.IsActive() {
		return false, err
	}

	// is within cooldown period
	endedAt := hypeTrain.EndedAt
	if endedAt == nil {
		endedAt = &hypeTrain.ExpiresAt
	}

	nextAvailableTime := endedAt.Add(config.CooldownDuration)

	if time.Now().Before(nextAvailableTime) {
		return false, nil
	}

	return true, nil
}

func (eng *engine) kickOffHypeTrainApproaching(ctx context.Context, config HypeTrainConfig, approachingRecord *Approaching, participationUserId string, kickoffParticipations []Participation, now time.Time) error {
	goal := int64(config.KickoffConfig.NumOfEvents)
	contributions, participations := generateEventsExpirationMap(config, kickoffParticipations)
	if len(contributions) == 1 && contributions[1] < MinTimeRemainingForApproaching {
		// it's too close, we shouldn't send it.
		return nil
	} else if len(contributions) > 0 {
		levelOneRewards := config.LevelSettingsByDifficulty[config.Difficulty][0].Rewards

		var creatorColor string
		if config.UseCreatorColor {
			creatorColor = config.PrimaryHexColor
		}

		var participants []string
		for _, val := range participations {
			participants = append(participants, val.User.Id)
		}

		rateLimited := false
		if approachingRecord == nil {
			treatment, err := eng.ApproachingExperimentClient.GetTreatment(config.ChannelID)
			if err != nil {
				return err
			}

			approachingRecord = &Approaching{
				ChannelID:               config.ChannelID,
				Participants:            participants,
				Goal:                    goal,
				EventsRemainingDuration: contributions,
				CreatorColor:            creatorColor,
				ExpiresAt:               now.Add(contributions[1]),
				StartedAt:               now,
				UpdatedAt:               now,
				CreatedTime:             now,
				TimeShownPerHour:        contributions[1],
				NumberOfTimesShown:      1,
				ID:                      uuid.New().String(),
				RateLimited:             false,
				LevelOneRewards:         levelOneRewards,
				GhostMode:               treatment != ht_approaching.TreatmentExperiment,
			}
		} else {
			shouldBeRateLimited, newExpiresAt, newStartedAt, newTimeShown, startedOver := ApproachingRateLimitFunc(now,
				approachingRecord.StartedAt,
				approachingRecord.ExpiresAt,
				approachingRecord.TimeShownPerHour,
				contributions[1])

			rateLimited = shouldBeRateLimited

			approachingRecord.ExpiresAt = newExpiresAt
			approachingRecord.StartedAt = newStartedAt
			approachingRecord.UpdatedAt = now
			approachingRecord.TimeShownPerHour = newTimeShown
			approachingRecord.NumberOfTimesShown++
			approachingRecord.Participants = participants
			approachingRecord.EventsRemainingDuration = contributions
			approachingRecord.RateLimited = rateLimited

			if startedOver {
				approachingRecord.ID = uuid.New().String()
			}
		}

		if !rateLimited && !approachingRecord.GhostMode {
			err := eng.Publisher.HypeTrainApproaching(ctx, config.ChannelID, goal, contributions, levelOneRewards, creatorColor, participants)
			if err != nil {
				return err
			}
			log.WithFields(log.Fields{"channelID": config.ChannelID}).Info("Sent notification of nearby hype train!")
		}

		err := eng.UpdateHypeTrainApproaching(ctx, *approachingRecord)
		if err != nil {
			log.WithFields(log.Fields{"channelID": config.ChannelID}).Error("Error updating dynamo approaching hype train entry!")
			// Once we have verified this behavior we can come back to it and make it critical and return instead of logging
			// return nil, err
		}
		log.WithFields(log.Fields{"channelID": config.ChannelID}).Info("Updated dynamo approaching hype train entry!")

		go eng.SendHypeTrainSettingsEvent("", config, "TRAIN APPROACHING")
		go eng.SendHypeTrainApproachingEvent(config, config.ChannelID, int(goal)-numOfEligibleParticipants(config, kickoffParticipations), approachingRecord.ID, participationUserId, approachingRecord.StartedAt, approachingRecord.ExpiresAt, rateLimited, approachingRecord.GhostMode)

		return err
	}
	return nil
}

// Given the new participation event, check if we can start a new hype train
func (eng *engine) kickOffHypeTrain(ctx context.Context, config HypeTrainConfig, participation Participation, kickoffParticipations []Participation, hypeTrainID string) (*HypeTrain, error) {
	start := time.Now()

	// the config snapshot is saved with the hype train record at this point
	// do config overrides (kickoffs, user customizations etc) before this call
	hypeTrain := NewHypeTrain(config.TrimDifficultySettings(), hypeTrainID)
	logger := log.WithFields(log.Fields{
		"channelID":   hypeTrain.ChannelID,
		"hypeTrainID": hypeTrain.ID,
	})

	// aggregate kickoff participations into participants, and add all kickoff participations into the hype train
	participants := map[User]Participant{}
	for _, participation := range kickoffParticipations {
		hypeTrain.AddParticipation(participation)

		participant, ok := participants[participation.User]
		if !ok {
			participant = NewParticipant(hypeTrain.ID, participation)
		} else {
			participant.AddParticipation(participation.ParticipationIdentifier, participation.Quantity)
		}
		participants[participation.User] = participant
	}

	participantsToSave := make([]Participant, 0, len(participants))
	for _, participant := range participants {
		participantsToSave = append(participantsToSave, participant)
	}

	err := eng.ParticipantDB.PutHypeTrainParticipants(ctx, participantsToSave...)
	if err != nil {
		return nil, errors.Wrap(err, "failed to save kickoff participants")
	}

	// create hype train record in DB, at this point we consider the hype train created and started
	// all subsequent queries on this page will load this hype train while it is still active
	err = eng.HypeTrainDB.CreateHypeTrain(ctx, hypeTrain)
	if err != nil {
		return nil, err
	}

	// remove the current conductor badge from previous conductors
	if hypeTrain.Config.HasConductorBadges {
		go func() {
			err := eng.ConductorBadgesHandler.RemoveCurrentConductorBadgeHolders(context.Background(), hypeTrain)
			if err != nil {
				logger.WithError(err).Error("failed to remove current conductor badge holders")
			}
		}()
	}

	// Schedule the hype train ending event
	go func() {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		if err := eng.ScheduleHypeTrainEnding(ctx, hypeTrain); err != nil {
			logger.WithError(err).Error("Failed to schedule an ending event with Destiny on hype train start")
		}
	}()

	// Publish initial participations to leaderboard
	go func() {
		ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
		defer cancel()
		if err := eng.Leaderboard.BatchPublish(ctx, hypeTrain.ChannelID, hypeTrain.ID, kickoffParticipations...); err != nil {
			logger.WithError(err).Error("Failed to publish kickoff participations to leaderboard")
		}
	}()

	// Publish start to pubsub
	go func() {
		ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
		defer cancel()

		if err := eng.Publisher.StartHypeTrain(ctx, hypeTrain, kickoffParticipations...); err != nil {
			logger.WithError(err).Error("Failed to publish start hypetrain message to pubsub")
		}
	}()

	go func() {
		logger.Info("Kicked off a new  hype train")
		_ = eng.Statter.Inc(hypeTrainKickedOffMetric, 1, 1.0)
		_ = eng.Statter.Inc(hypeTrainKickoffPointsMetric, int64(hypeTrain.ParticipationPoints()), 1.0)
		_ = eng.Statter.TimingDuration(hypeTrainKickoffLatencyMetric, time.Since(start), 1.0)
	}()

	// Send spade event for participation
	go eng.SendHypeTrainParticipationEvent(hypeTrain.ID, participation)

	// Send spade event for config update
	go eng.SendHypeTrainSettingsEvent(hypeTrain.ID, hypeTrain.Config, "TRAIN START")

	// Publish start progression event to spade
	go eng.SendHypeTrainProgressEvents(hypeTrain, "START")

	// Add the tag "Hype Train" to this channel
	go func() {
		if shouldNotTagChannel(hypeTrain.ChannelID) {
			return
		}
		err := eng.DynamicTag.ApplyTag(context.Background(), hypeTrain.ChannelID)
		if err != nil {
			logger.WithError(err).Error("Failed to apply a dynamic tag")
		}
	}()

	// Publish Hype Train "start" event to Activity Feed
	go func() {
		err := eng.SNSPublisher.PublishActivityFeedEvent(context.Background(), hypeTrain, models.HypeTrainStarted)
		if err != nil {
			logger.WithError(err).Error("Failed to send Hype Train start notification to Activity Feed")
		}
	}()

	return &hypeTrain, nil
}
