package percy_test

import (
	"context"
	"errors"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCelebrationEngine_IsEligibleForCelebrationPurchase(t *testing.T) {
	Convey("given a celebrations engine", t, func() {
		configDB := &internalfakes.FakeCelebrationConfigsDB{}
		userProfileDB := &internalfakes.FakeUserProfileDB{}
		purchasableConfigDB := &internalfakes.FakeCelebrationChannelPurchasableConfigDB{}
		whitelistDB := &internalfakes.FakeWhitelistDB{}

		eng := percy.NewCelebrationEngine(percy.CelebrationEngineConfig{
			CelebrationConfigsDB:                  configDB,
			UserProfileDB:                         userProfileDB,
			CelebrationChannelPurchasableConfigDB: purchasableConfigDB,
			WhitelistDB:                           whitelistDB,
		})

		ctx := context.Background()
		userID := "123"
		channelID := "456"
		offerIDs := []string{
			"abc",
			"def",
		}
		args := percy.CelebrationsIsEligibleArgs{
			UserID:    userID,
			ChannelID: channelID,
			OfferIDs:  offerIDs,
			ReadDepth: percy.CelebrationsEligibilityReadDepthDeep,
		}

		Convey("should return not eligible if the channel is not in the experiment", func() {
			whitelistDB.HasCelebrationsReturns(false)

			isEligibleArgs := percy.CelebrationsIsEligibleArgs{
				UserID:    userID,
				ChannelID: channelID,
				OfferIDs:  offerIDs,
				ReadDepth: percy.CelebrationsEligibilityReadDepthShallow,
			}
			offers, err := eng.IsEligibleForCelebrationPurchase(ctx, isEligibleArgs)
			So(err, ShouldBeNil)
			So(offers, ShouldHaveLength, 2)
			So(offers[0].IsEligible, ShouldBeFalse)
			So(offers[1].IsEligible, ShouldBeFalse)
		})

		Convey("when in the experiment", func() {
			whitelistDB.HasCelebrationsReturns(true)

			Convey("should return eligible for a shallow read depth", func() {
				isEligibleArgs := percy.CelebrationsIsEligibleArgs{
					UserID:    userID,
					ChannelID: channelID,
					OfferIDs:  offerIDs,
					ReadDepth: percy.CelebrationsEligibilityReadDepthShallow,
				}
				offers, err := eng.IsEligibleForCelebrationPurchase(ctx, isEligibleArgs)
				So(err, ShouldBeNil)
				So(offers, ShouldHaveLength, 2)
				So(offers[0].IsEligible, ShouldBeTrue)
				So(offers[1].IsEligible, ShouldBeTrue)
			})

			Convey("should return eligible for a shallow read depth, even if channel == user", func() {
				isEligibleArgs := percy.CelebrationsIsEligibleArgs{
					UserID:    channelID,
					ChannelID: channelID,
					OfferIDs:  offerIDs,
					ReadDepth: percy.CelebrationsEligibilityReadDepthShallow,
				}
				offers, err := eng.IsEligibleForCelebrationPurchase(ctx, isEligibleArgs)
				So(err, ShouldBeNil)
				So(offers, ShouldHaveLength, 2)
				So(offers[0].IsEligible, ShouldBeTrue)
				So(offers[1].IsEligible, ShouldBeTrue)
			})

			Convey("should return not eligible if attempting to purchase celebration in own channel", func() {
				offers, err := eng.IsEligibleForCelebrationPurchase(ctx, percy.CelebrationsIsEligibleArgs{
					UserID:    channelID,
					ChannelID: channelID,
					OfferIDs:  offerIDs,
					ReadDepth: percy.CelebrationsEligibilityReadDepthDeep,
				})
				So(err, ShouldBeNil)
				So(offers, ShouldHaveLength, 2)
				So(offers[0].IsEligible, ShouldBeFalse)
				So(offers[1].IsEligible, ShouldBeFalse)
			})

			Convey("should return not eligible for a nil celebration config response", func() {
				configDB.GetCelebrationConfigReturns(nil, nil)
				userProfileDB.IsChatBannedReturns(false, nil)
				userProfileDB.IsMonetizedReturns(true, nil)
				purchasableConfigDB.GetConfigReturns(&percy.CelebrationChannelPurchasableConfig{
					ChannelID: channelID,
					PurchaseConfig: map[percy.CelebrationSize]percy.CelebrationPurchaseConfig{
						percy.CelebrationSizeSmall: {
							IsDisabled: true,
							OfferID:    "abc",
						},
						percy.CelebrationSizeLarge: {
							IsDisabled: false,
							OfferID:    "def",
						},
					},
				}, nil)

				offers, err := eng.IsEligibleForCelebrationPurchase(ctx, args)
				So(err, ShouldBeNil)
				So(offers, ShouldHaveLength, 2)
				So(offers[0].IsEligible, ShouldBeFalse)
				So(offers[1].IsEligible, ShouldBeFalse)
			})

			Convey("should return offer eligibility based on channel config", func() {
				configDB.GetCelebrationConfigReturns(&percy.CelebrationConfig{
					Enabled: true,
				}, nil)
				userProfileDB.IsChatBannedReturns(false, nil)
				userProfileDB.IsMonetizedReturns(true, nil)
				purchasableConfigDB.GetConfigReturns(&percy.CelebrationChannelPurchasableConfig{
					ChannelID: channelID,
					PurchaseConfig: map[percy.CelebrationSize]percy.CelebrationPurchaseConfig{
						percy.CelebrationSizeSmall: {
							IsDisabled: true,
							OfferID:    "ghi",
						},
						percy.CelebrationSizeLarge: {
							IsDisabled: false,
							OfferID:    "abc",
						},
					},
				}, nil)

				offers, err := eng.IsEligibleForCelebrationPurchase(ctx, args)
				So(err, ShouldBeNil)
				So(offers, ShouldHaveLength, 2)
				So(offers[0].IsEligible, ShouldBeTrue)
				So(offers[1].IsEligible, ShouldBeFalse)
			})

			Convey("should return all offers as ineligible", func() {
				Convey("when the user is chat banned", func() {
					configDB.GetCelebrationConfigReturns(&percy.CelebrationConfig{
						Enabled: true,
					}, nil)
					userProfileDB.IsChatBannedReturns(true, nil)
					userProfileDB.IsMonetizedReturns(false, nil)
				})

				Convey("when the channel is not monetized", func() {
					configDB.GetCelebrationConfigReturns(&percy.CelebrationConfig{
						Enabled: true,
					}, nil)
					userProfileDB.IsChatBannedReturns(false, nil)
					userProfileDB.IsMonetizedReturns(false, nil)
				})

				Convey("when celebrations are disabled on the channel", func() {
					configDB.GetCelebrationConfigReturns(&percy.CelebrationConfig{
						Enabled: false,
					}, nil)
					userProfileDB.IsChatBannedReturns(false, nil)
					userProfileDB.IsMonetizedReturns(true, nil)
				})

				offers, err := eng.IsEligibleForCelebrationPurchase(ctx, args)
				So(err, ShouldBeNil)
				So(offers, ShouldHaveLength, 2)
				So(offers[0].IsEligible, ShouldBeFalse)
				So(offers[1].IsEligible, ShouldBeFalse)
			})

			Convey("should error", func() {
				Convey("when the GetCelebrationConfigReturns returns an error", func() {
					configDB.GetCelebrationConfigReturns(nil, errors.New("ERROR"))
					userProfileDB.IsChatBannedReturns(false, nil)
					userProfileDB.IsMonetizedReturns(false, nil)
				})

				Convey("when the IsChatBannedReturns returns an error", func() {
					configDB.GetCelebrationConfigReturns(&percy.CelebrationConfig{}, nil)
					userProfileDB.IsChatBannedReturns(false, errors.New("ERROR"))
					userProfileDB.IsMonetizedReturns(false, nil)
				})

				Convey("when the IsMonetizedReturns returns an error", func() {
					configDB.GetCelebrationConfigReturns(&percy.CelebrationConfig{}, nil)
					userProfileDB.IsChatBannedReturns(false, nil)
					userProfileDB.IsMonetizedReturns(false, errors.New("ERROR"))
				})

				Convey("when the CelebrationChannelPurchasableConfigDB.GetConfig returns an error", func() {
					configDB.GetCelebrationConfigReturns(&percy.CelebrationConfig{
						Enabled: true,
					}, nil)
					userProfileDB.IsChatBannedReturns(false, nil)
					userProfileDB.IsMonetizedReturns(true, nil)
					purchasableConfigDB.GetConfigReturns(nil, errors.New("ERROR"))
				})

				_, err := eng.IsEligibleForCelebrationPurchase(ctx, args)
				So(err, ShouldNotBeNil)
			})
		})
	})
}

func TestCelebrationEngine_ClawbackPayout(t *testing.T) {
	Convey("given a celebrations engine", t, func() {
		userProfileDB := &internalfakes.FakeUserProfileDB{}
		purchaseDB := &internalfakes.FakePurchase{}
		idempotencyDB := &internalfakes.FakeIdempotentTaskDB{}

		eng := percy.NewCelebrationEngine(percy.CelebrationEngineConfig{
			UserProfileDB: userProfileDB,
			Purchase:      purchaseDB,
			IdempotencyDB: idempotencyDB,
		})

		payoutReq := percy.CelebrationsPayoutArgs{
			BenefactorUserID: "abc",
			OriginID:         "123",
			RecipientUserID:  "def",
			OfferID:          "some-offer",
			IsClawback:       true,
		}

		Convey("we should error if", func() {
			Convey("fetching task returns an error", func() {
				idempotencyDB.GetReturns(nil, errors.New("ERROR"))
				err := eng.ClawbackPayout(context.Background(), payoutReq)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("we should do nothing and return nil if", func() {
			purchaseDB.GetPurchaseOrderPaymentsReturns(nil, nil)

			Convey("payout task is nil", func() {
				idempotencyDB.GetReturns(nil, nil)
				err := eng.ClawbackPayout(context.Background(), payoutReq)
				So(err, ShouldBeNil)
				So(purchaseDB.GetPurchaseOrderPaymentsCallCount(), ShouldEqual, 0)
			})

			Convey("payout task is not in completion state", func() {
				idempotencyDB.GetReturns(&percy.IdempotentTask{State: percy.IdempotencyStateProcessing}, nil)
				err := eng.ClawbackPayout(context.Background(), payoutReq)
				So(err, ShouldBeNil)
				So(purchaseDB.GetPurchaseOrderPaymentsCallCount(), ShouldEqual, 0)
			})
		})

		Convey("we should progress through the revocation flow if", func() {
			purchaseDB.GetPurchaseOrderPaymentsReturns(nil, nil)

			Convey("payout task is not nil and in the  completion state", func() {
				idempotencyDB.GetReturns(&percy.IdempotentTask{State: percy.IdempotencyStateComplete}, nil)
				_ = eng.ClawbackPayout(context.Background(), payoutReq)
				So(purchaseDB.GetPurchaseOrderPaymentsCallCount(), ShouldEqual, 1)
			})
		})
	})
}

func TestCelebrationEngine_Payout(t *testing.T) {
	Convey("given a celebrations engine", t, func() {
		userProfileDB := &internalfakes.FakeUserProfileDB{}
		purchaseDB := &internalfakes.FakePurchase{}

		eng := percy.NewCelebrationEngine(percy.CelebrationEngineConfig{
			UserProfileDB: userProfileDB,
			Purchase:      purchaseDB,
		})

		payoutReq := percy.CelebrationsPayoutArgs{
			BenefactorUserID: "abc",
			OriginID:         "123",
			RecipientUserID:  "def",
			OfferID:          "some-offer",
			IsClawback:       false,
		}

		Convey("we should post revenue share to gringotts", func() {
			purchaseOrderPayments := []percy.PurchaseOrderPayment{
				{
					GrossAmount: 100,
					Fees:        10,
					Taxes:       10,
				},
			}

			purchaseDB.GetPurchaseOrderPaymentsReturns(purchaseOrderPayments, nil)
			userProfileDB.GetPartnerTypeReturns(percy.PartnerTypePartner, nil)
			purchaseDB.PostCreatorRevenueShareForPurchaseReturns(nil)

			Convey("when payout", func() {
				err := eng.Payout(context.Background(), payoutReq)
				So(err, ShouldBeNil)
				_, gringottsArgs := purchaseDB.PostCreatorRevenueShareForPurchaseArgsForCall(0)
				So(gringottsArgs.TransactionID, ShouldEqual, "123")
				So(gringottsArgs.PartnerType, ShouldEqual, percy.PartnerTypePartner)
				So(gringottsArgs.PayoutCents, ShouldEqual, 40)
				So(gringottsArgs.TransactionType, ShouldEqual, percy.RevenueTransactionTypePurchase)
				So(gringottsArgs.RevenueSource, ShouldEqual, percy.RevenueSourceCelebration)
			})

			Convey("when clawback", func() {
				err := eng.Payout(context.Background(), percy.CelebrationsPayoutArgs{
					BenefactorUserID: "abc",
					OriginID:         "123",
					RecipientUserID:  "def",
					OfferID:          "some-offer",
					IsClawback:       true,
				})
				So(err, ShouldBeNil)
				_, gringottsArgs := purchaseDB.PostCreatorRevenueShareForPurchaseArgsForCall(0)
				So(gringottsArgs.TransactionID, ShouldEqual, "123")
				So(gringottsArgs.PartnerType, ShouldEqual, percy.PartnerTypePartner)
				So(gringottsArgs.PayoutCents, ShouldEqual, -40)
				So(gringottsArgs.TransactionType, ShouldEqual, percy.RevenueTransactionTypeRefund)
				So(gringottsArgs.RevenueSource, ShouldEqual, percy.RevenueSourceCelebration)
			})
		})

		Convey("we should error if", func() {
			Convey("Purchase.GetPurchaseOrderPayments returns an error", func() {
				purchaseDB.GetPurchaseOrderPaymentsReturns(nil, errors.New("ERROR"))
				err := eng.Payout(context.Background(), payoutReq)
				So(err, ShouldNotBeNil)
			})

			Convey("GrossAmountUsd is 0", func() {
				purchaseOrderPayments := []percy.PurchaseOrderPayment{
					{
						GrossAmount: 0,
						Fees:        0,
						Taxes:       0,
					},
				}

				purchaseDB.GetPurchaseOrderPaymentsReturns(purchaseOrderPayments, nil)
				err := eng.Payout(context.Background(), payoutReq)
				So(err, ShouldNotBeNil)
			})

			Convey("GrossAmountUsd is less than 0", func() {
				purchaseOrderPayments := []percy.PurchaseOrderPayment{
					{
						GrossAmount: -1,
						Fees:        0,
						Taxes:       0,
					},
				}

				purchaseDB.GetPurchaseOrderPaymentsReturns(purchaseOrderPayments, nil)
				err := eng.Payout(context.Background(), payoutReq)
				So(err, ShouldNotBeNil)
			})

			Convey("purchaseCostBasisCents is less than or equal to 0", func() {
				purchaseOrderPayments := []percy.PurchaseOrderPayment{
					{
						GrossAmount: 0,
						Fees:        10,
						Taxes:       10,
					},
				}

				purchaseDB.GetPurchaseOrderPaymentsReturns(purchaseOrderPayments, nil)
				err := eng.Payout(context.Background(), payoutReq)
				So(err, ShouldNotBeNil)
			})

			Convey("UserProfileDB.GetPartnerType returns an error", func() {
				purchaseOrderPayments := []percy.PurchaseOrderPayment{
					{
						GrossAmount: 100,
						Fees:        10,
						Taxes:       10,
					},
				}

				purchaseDB.GetPurchaseOrderPaymentsReturns(purchaseOrderPayments, nil)
				userProfileDB.GetPartnerTypeReturns(percy.PartnerTypeUnknown, errors.New("ERROR"))
				err := eng.Payout(context.Background(), payoutReq)
				So(err, ShouldNotBeNil)
			})

			Convey("Purchase.PostCreatorRevenueShareForPurchase returns an error", func() {
				purchaseOrderPayments := []percy.PurchaseOrderPayment{
					{
						GrossAmount: 100,
						Fees:        10,
						Taxes:       10,
					},
				}

				purchaseDB.GetPurchaseOrderPaymentsReturns(purchaseOrderPayments, nil)
				userProfileDB.GetPartnerTypeReturns(percy.PartnerTypePartner, nil)
				purchaseDB.PostCreatorRevenueShareForPurchaseReturns(errors.New("ERROR"))
				err := eng.Payout(context.Background(), payoutReq)
				So(err, ShouldNotBeNil)
			})
		})
	})
}

func TestCelebrationEngine_EmitCelebration(t *testing.T) {
	Convey("given a celebrations engine", t, func() {
		configDB := internalfakes.FakeCelebrationChannelPurchasableConfigDB{}
		publisher := internalfakes.FakePublisher{}

		eng := percy.NewCelebrationEngine(percy.CelebrationEngineConfig{
			CelebrationChannelPurchasableConfigDB: &configDB,
			Publisher:                             &publisher,
		})

		args := percy.EmitCelebrationArgs{
			RecipientUserID:  "recipient",
			BenefactorUserID: "benefactor",
			OriginID:         "abc",
			OfferID:          "123",
		}

		Convey("we should error if", func() {
			Convey("config query returns an error", func() {
				configDB.GetConfigReturns(&percy.CelebrationChannelPurchasableConfig{}, errors.New("ERROR"))
				err := eng.EmitCelebration(context.Background(), args)
				So(err, ShouldNotBeNil)
			})

			Convey("config query returns a nil config", func() {
				configDB.GetConfigReturns(nil, nil)
				err := eng.EmitCelebration(context.Background(), args)
				So(err, ShouldNotBeNil)
			})

			Convey("configuration does not exist with the matching offerID", func() {
				config := map[percy.CelebrationSize]percy.CelebrationPurchaseConfig{
					percy.CelebrationSizeSmall: {
						IsDisabled: false,
						OfferID:    "whoops",
					},
				}

				configDB.GetConfigReturns(&percy.CelebrationChannelPurchasableConfig{
					PurchaseConfig: config,
				}, nil)
				err := eng.EmitCelebration(context.Background(), args)
				So(err, ShouldNotBeNil)
			})

			Convey("configuration exists for the offerID, but it is disabled", func() {
				config := map[percy.CelebrationSize]percy.CelebrationPurchaseConfig{
					percy.CelebrationSizeSmall: {
						IsDisabled: true,
						OfferID:    "123",
					},
				}

				configDB.GetConfigReturns(&percy.CelebrationChannelPurchasableConfig{
					PurchaseConfig: config,
				}, nil)
				err := eng.EmitCelebration(context.Background(), args)
				So(err, ShouldNotBeNil)
			})

			Convey("configuration exists for the offerID, but the mapped size is invalid", func() {
				config := map[percy.CelebrationSize]percy.CelebrationPurchaseConfig{
					"foobar": {
						IsDisabled: false,
						OfferID:    "123",
					},
				}

				configDB.GetConfigReturns(&percy.CelebrationChannelPurchasableConfig{
					PurchaseConfig: config,
				}, nil)
				err := eng.EmitCelebration(context.Background(), args)
				So(err, ShouldNotBeNil)
			})

			Convey("publisher#StartCelebration returns an error", func() {
				config := map[percy.CelebrationSize]percy.CelebrationPurchaseConfig{
					percy.CelebrationSizeSmall: {
						IsDisabled: false,
						OfferID:    "123",
					},
				}

				configDB.GetConfigReturns(&percy.CelebrationChannelPurchasableConfig{
					PurchaseConfig: config,
				}, nil)
				publisher.StartCelebrationReturns(errors.New("ERROR"))
				err := eng.EmitCelebration(context.Background(), args)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("we should start a celebration", func() {
			config := map[percy.CelebrationSize]percy.CelebrationPurchaseConfig{
				percy.CelebrationSizeSmall: {
					IsDisabled: false,
					OfferID:    "123",
				},
			}

			configDB.GetConfigReturns(&percy.CelebrationChannelPurchasableConfig{
				PurchaseConfig: config,
			}, nil)
			publisher.StartCelebrationReturns(nil)
			err := eng.EmitCelebration(context.Background(), args)
			So(err, ShouldBeNil)
			So(publisher.StartCelebrationCallCount(), ShouldEqual, 1)
		})
	})
}

func TestCelebrationEngine_SendFulfillmentUserNotice(t *testing.T) {
	Convey("given a celebrations engine", t, func() {
		userProfileDB := &internalfakes.FakeUserProfileDB{}
		purchasableConfigDB := &internalfakes.FakeCelebrationChannelPurchasableConfigDB{}

		eng := percy.NewCelebrationEngine(percy.CelebrationEngineConfig{
			UserProfileDB:                         userProfileDB,
			CelebrationChannelPurchasableConfigDB: purchasableConfigDB,
		})

		ctx := context.Background()
		userID := "123"
		channelID := "456"

		Convey("we should error if", func() {
			Convey("GetConfig returns an error", func() {
				purchasableConfigDB.GetConfigReturns(nil, errors.New("ERROR"))
				err := eng.SendFulfillmentUserNotice(ctx, percy.CelebrationsUserNoticeArgs{
					BenefactorUserID: userID,
					RecipientUserID:  channelID,
					OriginID:         "some-id",
					OfferID:          "abc",
				})
				So(err, ShouldNotBeNil)
			})

			Convey("tmi returns an error", func() {
				purchasableConfigDB.GetConfigReturns(&percy.CelebrationChannelPurchasableConfig{
					ChannelID: channelID,
					PurchaseConfig: map[percy.CelebrationSize]percy.CelebrationPurchaseConfig{
						percy.CelebrationSizeLarge: {
							IsDisabled: false,
							OfferID:    "abc",
						},
					},
				}, nil)
				userProfileDB.SendUserNoticeReturns(errors.New("ERROR"))

				err := eng.SendFulfillmentUserNotice(ctx, percy.CelebrationsUserNoticeArgs{
					BenefactorUserID: userID,
					RecipientUserID:  channelID,
					OriginID:         "some-id",
					OfferID:          "abc",
				})
				So(err, ShouldNotBeNil)
			})
		})

		Convey("we should return nil if sending the user notice succeeds", func() {
			purchasableConfigDB.GetConfigReturns(&percy.CelebrationChannelPurchasableConfig{
				ChannelID: channelID,
				PurchaseConfig: map[percy.CelebrationSize]percy.CelebrationPurchaseConfig{
					percy.CelebrationSizeLarge: {
						IsDisabled: false,
						OfferID:    "abc",
					},
				},
			}, nil)
			userProfileDB.SendUserNoticeReturns(nil)

			err := eng.SendFulfillmentUserNotice(ctx, percy.CelebrationsUserNoticeArgs{
				BenefactorUserID: userID,
				RecipientUserID:  channelID,
				OriginID:         "some-id",
				OfferID:          "abc",
			})
			So(err, ShouldBeNil)
			_, tmiArgs := userProfileDB.SendUserNoticeArgsForCall(0)
			So(tmiArgs.SenderUserID, ShouldEqual, 123)
			So(tmiArgs.TargetChannelID, ShouldEqual, 456)
			So(tmiArgs.MessageParams[0].Value, ShouldEqual, string(percy.CelebrationSizeLarge))
		})
	})
}

func TestCelebrationEngine_PublishDatascience(t *testing.T) {
	Convey("given a celebrations engine", t, func() {
		purchasableConfigDB := &internalfakes.FakeCelebrationChannelPurchasableConfigDB{}
		publisher := &internalfakes.FakePublisher{}
		purchaseDB := &internalfakes.FakePurchase{}
		spade := &internalfakes.FakeSpade{}

		eng := percy.NewCelebrationEngine(percy.CelebrationEngineConfig{
			CelebrationChannelPurchasableConfigDB: purchasableConfigDB,
			Publisher:                             publisher,
			Purchase:                              purchaseDB,
			Spade:                                 spade,
		})

		ctx := context.Background()
		userID := tests.UserID
		channelID := tests.ChannelID
		req := percy.CelebrationsDatascienceArgs{
			BenefactorUserID: userID,
			RecipientUserID:  channelID,
			OriginID:         "some-id",
			OfferID:          tests.OfferID,
		}

		Convey("success when all information is gathered.", func() {
			purchaseOrderPayments := []percy.PurchaseOrderPayment{
				{
					GrossAmount: 100,
					Fees:        10,
					Taxes:       10,
				},
			}

			purchaseDB.GetPurchaseOrderPaymentsReturns(purchaseOrderPayments, nil)
			publisher.GetNumberOfPubsubListenersReturns(10, nil)
			purchasableConfigDB.GetConfigReturns(tests.ValidCelebrationPurchasableConfig(), nil)
			err := eng.PublishDatascience(ctx, req)
			So(err, ShouldBeNil)
			So(spade.SendCelebrationOverlayInitiatedEventCallCount(), ShouldEqual, 1)
			_, args := spade.SendCelebrationOverlayInitiatedEventArgsForCall(0)
			So(args, ShouldHaveLength, 1)
			So(args[0].UserID, ShouldEqual, userID)
			So(args[0].ChannelID, ShouldEqual, channelID)
			So(args[0].TransactionID, ShouldEqual, "some-id")
			So(args[0].CelebrationID, ShouldEqual, tests.OfferID)
			So(args[0].NumPubsubListeners, ShouldEqual, 10)
			So(args[0].Intensity, ShouldEqual, percy.CelebrationSizeSmall)
			So(args[0].GrossAmount, ShouldEqual, 80)
			So(args[0].TransactionProduct, ShouldEqual, "EXPLICIT_PURCHASE")
		})

		Convey("returns error when", func() {
			Convey("when getting purchase config fails", func() {
				purchasableConfigDB.GetConfigReturns(nil, errors.New("WALRUS STRIKE"))
			})

			Convey("when the purchase config is nil", func() {
				purchasableConfigDB.GetConfigReturns(nil, nil)
			})

			Convey("when offer does not match any configuration", func() {
				req.OfferID = "someNonOffer"
			})

			Convey("when fetching the purchase order fails", func() {
				purchaseDB.GetPurchaseOrderPaymentsReturns(nil, errors.New("WALRUS STRIKE"))
			})

			Convey("when the gross amount fetched would turn out to be 0", func() {
				purchaseDB.GetPurchaseOrderPaymentsReturns([]percy.PurchaseOrderPayment{
					{
						GrossAmount: 0,
						Fees:        0,
						Taxes:       0,
					},
				}, nil)
			})

			Convey("when the purchase cost basis would be less than 0", func() {
				purchaseDB.GetPurchaseOrderPaymentsReturns([]percy.PurchaseOrderPayment{
					{
						GrossAmount: 0,
						Fees:        10,
						Taxes:       10,
					},
				}, nil)
			})

			Convey("when we fail to fetch num listeners from pub control", func() {
				publisher.GetNumberOfPubsubListenersReturns(0, errors.New("WALRUS STRIKE"))
			})

			Convey("when we fail to publish to datascience", func() {
				spade.SendCelebrationOverlayInitiatedEventReturns(errors.New("WALRUS STRIKE"))
			})

			err := eng.PublishDatascience(ctx, req)
			So(err, ShouldNotBeNil)
		})
	})
}
