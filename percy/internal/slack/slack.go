package slack

import (
	"bytes"
	"encoding/json"
	"net/http"
)

//go:generate counterfeiter . Slacker

type Slacker interface {
	Post(msg SlackMsg) error
	PostMsg(msgBody string) error
}

type slacker struct {
	HttpClient *http.Client
	WebhookURL string
}

func NewSlacker(webhookURL string) Slacker {
	return &slacker{
		HttpClient: http.DefaultClient,
		WebhookURL: webhookURL,
	}
}

type SlackMsg struct {
	Text        string            `json:"text"`
	UnfurlLinks bool              `json:"unfurl_links"`
	Parse       string            `json:"parse"`
	Attachments []SlackAttachment `json:"attachments"`
}

type SlackAttachment struct {
	Fallback string `json:"fallback"`
	Pretext  string `json:"pretext"`
	ImageURL string `json:"image_url"`
}

func (s *slacker) Post(msg SlackMsg) error {
	if s.WebhookURL == "" {
		return nil
	}

	requestJson, err := json.Marshal(&msg)
	if err != nil {
		return err
	}
	requestJsonReader := bytes.NewReader(requestJson)

	_, err = s.HttpClient.Post(s.WebhookURL, "application/json", requestJsonReader) //nolint
	if err != nil {
		return err
	}
	return nil
}

func (s *slacker) PostMsg(msgBody string) error {
	msg := SlackMsg{
		Text:        msgBody,
		UnfurlLinks: true,
		Parse:       "full",
	}
	return s.Post(msg)
}
