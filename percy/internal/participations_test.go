package percy_test

import (
	"context"
	"testing"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/miniexperiments/ht_approaching"
	"code.justin.tv/commerce/percy/internal/miniexperiments/miniexperimentsfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestEngine_RecordParticipation__OngoingHypeTrain(t *testing.T) {
	Convey("Given an engine", t, func() {
		configsDB := &internalfakes.FakeHypeTrainConfigsDB{}
		hypeTrainDB := &internalfakes.FakeHypeTrainDB{}
		participationDB := &internalfakes.FakeParticipationDB{}
		participantDB := &internalfakes.FakeParticipantDB{}
		entitlementsDB := &internalfakes.FakeEntitlementDB{}
		eventDB := &internalfakes.FakeEventDB{}
		leaderboard := &internalfakes.FakeLeaderboard{}
		publisher := &internalfakes.FakePublisher{}
		spade := &internalfakes.FakeSpade{}
		conductorBadgesHandler := &internalfakes.FakeConductorBadgesHandler{}
		snsPublisher := &internalfakes.FakeSNSPublisher{}

		engine := percy.NewEngine(percy.EngineConfig{
			ConfigsDB:              configsDB,
			HypeTrainDB:            hypeTrainDB,
			ParticipantDB:          participantDB,
			ParticipationDB:        participationDB,
			EntitlementDB:          entitlementsDB,
			EventDB:                eventDB,
			Leaderboard:            leaderboard,
			Publisher:              publisher,
			Spade:                  spade,
			ConductorBadgesHandler: conductorBadgesHandler,
			SNSPublisher:           snsPublisher,
		})

		ctx := context.Background()

		validOngoingHypeTrain := tests.ValidOngoingHypeTrain()
		validUpdatedHypeTrain := tests.ValidOngoingHypeTrain()
		participation := tests.ValidParticipation()
		participation.Timestamp = validOngoingHypeTrain.StartedAt.Add(5 * time.Second)

		Convey("when there is an ongoing hype train", func() {
			hypeTrainDB.GetMostRecentHypeTrainReturns(&validOngoingHypeTrain, nil)
			participationDB.RecordChannelParticipationReturns(nil)
			participantDB.RecordHypeTrainParticipationReturns(percy.NewParticipant(validOngoingHypeTrain.ID, participation), nil)
			participantDB.GetHypeTrainParticipantsReturns([]percy.Participant{}, nil)
			entitlementsDB.CreateEmoteEntitlementReturns(nil)
			hypeTrainDB.AddParticipationReturns(validUpdatedHypeTrain, nil)
			hypeTrainDB.ExtendHypeTrainReturns(validUpdatedHypeTrain, nil)
			eventDB.UpsertHypeTrainExpirationTimerReturns(nil)
			leaderboard.PublishReturns(nil)
			publisher.PostHypeTrainProgressionReturns(nil)
			spade.SendHypeTrainSpecialCalloutEventsReturns(nil)

			Convey("if the participation comes from the broadcaster", func() {
				participation.Source = percy.BitsSource
				participation.Action = percy.CheerAction
				participation.Quantity = int(validOngoingHypeTrain.LevelProgress().Goal) + 1
				participation.User = percy.User{
					Id: participation.ChannelID,
				}

				Convey("should not record new participation", func() {
					err := engine.RecordParticipation(ctx, participation)
					So(err, ShouldBeNil)
					So(participationDB.RecordChannelParticipationCallCount(), ShouldEqual, 0)
					So(participantDB.RecordHypeTrainParticipationCallCount(), ShouldEqual, 0)
					So(hypeTrainDB.AddParticipationCallCount(), ShouldEqual, 0)
				})
			})

			Convey("if the participation completes the current hype train level", func() {
				Convey("with an intermediate level completed", func() {
					// enough bits to complete the current level
					participation.Source = percy.BitsSource
					participation.Action = percy.CheerAction
					participation.Quantity = int(validOngoingHypeTrain.LevelProgress().Goal) + 1

					// updated hype train includes this participation
					validUpdatedHypeTrain.AddParticipation(participation)
					So(validUpdatedHypeTrain.CompletedLevel(), ShouldBeGreaterThan, validOngoingHypeTrain.CompletedLevel())

					publisher.LevelUpHypeTrainReturns(nil)

					Convey("extends the expiration timer on the hype train", func() {
						err := engine.RecordParticipation(ctx, participation)
						So(err, ShouldBeNil)
						So(hypeTrainDB.ExtendHypeTrainCallCount(), ShouldEqual, 1)

						Convey("publish participation to leaderboard", func() {
							// wait for async call
							time.Sleep(100 * time.Millisecond)
							So(leaderboard.PublishCallCount(), ShouldEqual, 1)
						})
					})

					Convey("returns an error when hype train DB fails to extend the expiration timer", func() {
						hypeTrainDB.ExtendHypeTrainReturns(percy.HypeTrain{}, errors.New("can't extend"))
						err := engine.RecordParticipation(ctx, participation)
						So(err, ShouldNotBeNil)
					})
				})

				Convey("with max level compeleted", func() {
					// set the hype train's status to be 1 bit away from completing its highest level
					levelSettings := validOngoingHypeTrain.Config.LevelSettings()
					finalLevel := levelSettings[len(levelSettings)-1]

					validOngoingHypeTrain.Participations = make(percy.ParticipationTotals)
					validOngoingHypeTrain.AddParticipation(percy.Participation{
						ChannelID: validOngoingHypeTrain.ChannelID,
						User:      percy.NewAnonymousCheerer(),
						ParticipationIdentifier: percy.ParticipationIdentifier{
							Source: percy.BitsSource,
							Action: percy.CheerAction,
						},
						Timestamp: time.Now(),
						Quantity:  int(finalLevel.Goal) - 1,
					})

					// updated hype train completes all the levels
					validUpdatedHypeTrain.AddParticipation(percy.Participation{
						ChannelID: validOngoingHypeTrain.ChannelID,
						User:      percy.NewAnonymousCheerer(),
						ParticipationIdentifier: percy.ParticipationIdentifier{
							Source: percy.BitsSource,
							Action: percy.CheerAction,
						},
						Timestamp: time.Now(),
						Quantity:  int(finalLevel.Goal) + 1,
					})

					Convey("does not extend the expiration timer on the hype train", func() {
						err := engine.RecordParticipation(ctx, participation)
						So(err, ShouldBeNil)
						So(hypeTrainDB.ExtendHypeTrainCallCount(), ShouldEqual, 0)
					})

					Convey("returns an error when", func() {
						Convey("fails to get current hype train", func() {
							hypeTrainDB.GetMostRecentHypeTrainReturns(nil, errors.New("no train"))
						})

						Convey("fails to record participation", func() {
							participationDB.RecordChannelParticipationReturns(errors.New("fails to record"))
						})

						Convey("fails to record participant", func() {
							hypeTrainDB.AddParticipationReturns(percy.HypeTrain{}, errors.New("can't add"))
						})

						err := engine.RecordParticipation(ctx, participation)
						So(err, ShouldNotBeNil)
					})
				})
			})

			Convey("if the participation does not complete the current hype train level", func() {
				Convey("just record new participation", func() {
					err := engine.RecordParticipation(ctx, participation)
					So(err, ShouldBeNil)
					So(participationDB.RecordChannelParticipationCallCount(), ShouldEqual, 1)
					So(participantDB.RecordHypeTrainParticipationCallCount(), ShouldEqual, 1)
					So(hypeTrainDB.AddParticipationCallCount(), ShouldEqual, 1)
				})

				Convey("returns an error when", func() {
					Convey("fails to get current hype train", func() {
						hypeTrainDB.GetMostRecentHypeTrainReturns(nil, errors.New("no train"))
					})

					Convey("fails to record participation", func() {
						participationDB.RecordChannelParticipationReturns(errors.New("fails to record"))
					})

					Convey("fails to record participant", func() {
						hypeTrainDB.AddParticipationReturns(percy.HypeTrain{}, errors.New("can't add"))
					})

					err := engine.RecordParticipation(ctx, participation)
					So(err, ShouldNotBeNil)
				})
			})
		})
	})
}

func TestEngine_RecordParticipation_KickoffNewHypetrain(t *testing.T) {
	Convey("Given an engine", t, func() {
		approachingDB := &internalfakes.FakeApproachingDB{}
		bitsPinataDB := &internalfakes.FakeBitsPinataDB{}
		configsDB := &internalfakes.FakeHypeTrainConfigsDB{}
		hypeTrainDB := &internalfakes.FakeHypeTrainDB{}
		participationDB := &internalfakes.FakeParticipationDB{}
		participantDB := &internalfakes.FakeParticipantDB{}
		eventDB := &internalfakes.FakeEventDB{}
		leaderboard := &internalfakes.FakeLeaderboard{}
		publisher := &internalfakes.FakePublisher{}
		spade := &internalfakes.FakeSpade{}
		conductorBadgeHandler := &internalfakes.FakeConductorBadgesHandler{}
		dynamicTag := &internalfakes.FakeDynamicTag{}
		snsPublisher := &internalfakes.FakeSNSPublisher{}
		approachingExperimentClient := &miniexperimentsfakes.FakeClient{}

		engine := percy.NewEngine(percy.EngineConfig{
			ApproachingDB:               approachingDB,
			BitsPinataDB:                bitsPinataDB,
			ConfigsDB:                   configsDB,
			HypeTrainDB:                 hypeTrainDB,
			ParticipantDB:               participantDB,
			ParticipationDB:             participationDB,
			Leaderboard:                 leaderboard,
			EventDB:                     eventDB,
			Publisher:                   publisher,
			Spade:                       spade,
			ConductorBadgesHandler:      conductorBadgeHandler,
			DynamicTag:                  dynamicTag,
			SNSPublisher:                snsPublisher,
			ApproachingExperimentClient: approachingExperimentClient,
		})

		ctx := context.Background()

		validConfig := tests.ValidHypeTrainConfig()
		validOngoingHypeTrain := tests.ValidOngoingHypeTrain()
		participation := tests.ValidParticipation()
		participation.Timestamp = validOngoingHypeTrain.StartedAt.Add(5 * time.Second)

		Convey("when there is no ongoing hype train, and a new one can be started", func() {
			validConfig.Enabled = true

			configsDB.GetConfigReturns(&validConfig, nil)

			hypeTrainDB.GetMostRecentHypeTrainReturns(nil, nil)
			participationDB.RecordChannelParticipationReturns(nil)
			participantDB.GetHypeTrainParticipantsReturns([]percy.Participant{}, nil)
			eventDB.UpsertHypeTrainExpirationTimerReturns(nil)
			publisher.StartHypeTrainReturns(nil)
			spade.SendHypeTrainSettingsEventReturns(nil)
			approachingExperimentClient.GetTreatmentReturns(ht_approaching.TreatmentControl, nil)
			bitsPinataDB.GetActiveBitsPinataReturns(nil, nil)

			Convey("if there are enough events to kickoff a new hype train", func() {
				// 3 participations from 3 different users
				participation1 := tests.ValidParticipation()
				participation2 := tests.ValidParticipation()
				participation3 := tests.ValidParticipation()
				participation1.User = percy.NewUser("1")
				participation2.User = percy.NewUser("2")
				participation3.User = percy.NewUser("3")

				Convey("kicks off a new hype train with kickoff participations", func() {
					participationDB.GetChannelParticipationsReturns([]percy.Participation{
						participation1, participation2, participation3,
					}, nil)
					participantDB.PutHypeTrainParticipantsReturns(nil)
					hypeTrainDB.CreateHypeTrainReturns(nil)
					conductorBadgeHandler.RemoveCurrentConductorBadgeHoldersReturns(nil)
					eventDB.UpsertHypeTrainExpirationTimerReturns(nil)

					err := engine.RecordParticipation(ctx, participation)
					So(err, ShouldBeNil)
					So(participationDB.RecordChannelParticipationCallCount(), ShouldEqual, 1)
					So(hypeTrainDB.CreateHypeTrainCallCount(), ShouldEqual, 1)
					So(participantDB.PutHypeTrainParticipantsCallCount(), ShouldEqual, 1)

					// hype train should have been initialized with the 3 kickoff participation events
					_, createdHypeTrain := hypeTrainDB.CreateHypeTrainArgsForCall(0)
					So(createdHypeTrain.ParticipationPoints(),
						ShouldEqual,
						createdHypeTrain.Config.EquivalentParticipationPoint(
							participation.ParticipationIdentifier,
							participation.Quantity)*3)

					time.Sleep(100 * time.Millisecond)
					So(leaderboard.BatchPublishCallCount(), ShouldEqual, 1)
				})
			})

			Convey("if there are enough events to be approaching", func() {
				approachingDB.GetApproachingReturns(nil, nil)
				approachingDB.SetApproachingReturns(nil)
				approachingExperimentClient.GetTreatmentReturns(ht_approaching.TreatmentExperiment, nil)

				participation1 := tests.ValidParticipation()
				participation2 := tests.ValidParticipation()
				participation1.User = percy.NewUser("1")
				participation2.User = percy.NewUser("2")

				participationDB.GetChannelParticipationsReturns([]percy.Participation{
					participation1, participation2,
				}, nil)
				participantDB.PutHypeTrainParticipantsReturns(nil)

				err := engine.RecordParticipation(ctx, participation)
				So(err, ShouldBeNil)

				So(publisher.HypeTrainApproachingCallCount(), ShouldEqual, 1)

				// wait a 100ms for async.
				time.Sleep(100 * time.Millisecond)
				So(spade.SendHypeTrainSettingsEventCallCount(), ShouldEqual, 1)
				So(spade.SendHypeTrainApproachingEventCallCount(), ShouldEqual, 1)
			})

			Convey("if there are enough events to be approaching, but the time frame is too tight", func() {
				approachingDB.GetApproachingReturns(nil, nil)

				participation := tests.ValidParticipation()
				participation.Timestamp = time.Now().Add(-percy.MinTimeRemainingForApproaching)
				participation.User = percy.NewUser("1")

				participationDB.GetChannelParticipationsReturns([]percy.Participation{
					participation,
				}, nil)
				participantDB.PutHypeTrainParticipantsReturns(nil)

				err := engine.RecordParticipation(ctx, participation)
				So(err, ShouldBeNil)

				So(publisher.HypeTrainApproachingCallCount(), ShouldEqual, 0)
			})

			Convey("if there aren't enough events to kickoff a new hype train", func() {
				configsDB.GetConfigReturns(&validConfig, nil)

				err := engine.RecordParticipation(ctx, participation)
				So(err, ShouldBeNil)
				So(participationDB.RecordChannelParticipationCallCount(), ShouldEqual, 1)
				So(hypeTrainDB.CreateHypeTrainCallCount(), ShouldEqual, 0)
			})

			Convey("if new event does not meet thresholds to count towards kickoff", func() {
				// 1-bit cheer
				participation.ParticipationIdentifier = percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				}
				participation.Quantity = 1

				err := engine.RecordParticipation(ctx, participation)
				So(err, ShouldBeNil)
				So(participationDB.RecordChannelParticipationCallCount(), ShouldEqual, 1)
				So(hypeTrainDB.CreateHypeTrainCallCount(), ShouldEqual, 0)
			})

			Convey("returns an error when", func() {
				Convey("fails to get the current hype train", func() {
					hypeTrainDB.GetMostRecentHypeTrainReturns(nil, errors.New("can't train"))
				})

				Convey("fails to record participation event", func() {
					participationDB.RecordChannelParticipationReturns(errors.New("can't record"))
				})

				err := engine.RecordParticipation(ctx, participation)
				So(err, ShouldNotBeNil)
			})
		})
	})
}

func TestEngine_RecordParticipation_ChannelInCooldown(t *testing.T) {
	Convey("Given an engine", t, func() {
		configsDB := &internalfakes.FakeHypeTrainConfigsDB{}
		hypeTrainDB := &internalfakes.FakeHypeTrainDB{}
		participationDB := &internalfakes.FakeParticipationDB{}
		participantDB := &internalfakes.FakeParticipantDB{}
		spade := &internalfakes.FakeSpade{}
		approachingDB := &internalfakes.FakeApproachingDB{}
		bitsPinataDB := &internalfakes.FakeBitsPinataDB{}
		engine := percy.NewEngine(percy.EngineConfig{
			ApproachingDB:   approachingDB,
			BitsPinataDB:    bitsPinataDB,
			ConfigsDB:       configsDB,
			HypeTrainDB:     hypeTrainDB,
			Spade:           spade,
			ParticipantDB:   participantDB,
			ParticipationDB: participationDB,
		})

		ctx := context.Background()

		validConfig := tests.ValidHypeTrainConfig()
		validOngoingHypeTrain := tests.ValidOngoingHypeTrain()
		validExpiredHypeTrain := tests.ValidExpiredHypeTrain()
		participation := tests.ValidParticipation()
		participation.Timestamp = validOngoingHypeTrain.StartedAt.Add(5 * time.Second)
		Convey("when channel has hype train enabled, but is in cooldown", func() {
			validConfig.Enabled = true
			validConfig.CooldownDuration = 9999 * time.Hour
			configsDB.GetConfigReturns(&validConfig, nil)
			hypeTrainDB.GetMostRecentHypeTrainReturns(&validExpiredHypeTrain, nil)
			participantDB.GetHypeTrainParticipantsReturns([]percy.Participant{}, nil)
			approachingDB.GetApproachingReturns(nil, nil)
			bitsPinataDB.GetActiveBitsPinataReturns(nil, nil)

			Convey("does not record participation", func() {
				err := engine.RecordParticipation(ctx, participation)
				So(err, ShouldBeNil)
				So(participationDB.RecordChannelParticipationCallCount(), ShouldEqual, 0)
			})

			Convey("returns an error when", func() {
				Convey("fails to retrieve configs", func() {
					configsDB.GetConfigReturns(nil, errors.New("can't get"))
				})

				Convey("fails to get current train", func() {
					hypeTrainDB.GetMostRecentHypeTrainReturns(nil, errors.New("can't train"))
				})

				err := engine.RecordParticipation(ctx, participation)
				So(err, ShouldNotBeNil)
			})
		})
	})
}

func TestEngine_RecordParticipation_HypeTrainDisabled(t *testing.T) {
	Convey("Given an engine", t, func() {
		configsDB := &internalfakes.FakeHypeTrainConfigsDB{}
		hypeTrainDB := &internalfakes.FakeHypeTrainDB{}
		participationDB := &internalfakes.FakeParticipationDB{}
		participantDB := &internalfakes.FakeParticipantDB{}
		approachingDB := &internalfakes.FakeApproachingDB{}
		bitsPinataDB := &internalfakes.FakeBitsPinataDB{}
		engine := percy.NewEngine(percy.EngineConfig{
			ApproachingDB:   approachingDB,
			BitsPinataDB:    bitsPinataDB,
			ConfigsDB:       configsDB,
			HypeTrainDB:     hypeTrainDB,
			ParticipantDB:   participantDB,
			ParticipationDB: participationDB,
		})

		ctx := context.Background()

		validConfig := tests.ValidHypeTrainConfig()
		validOngoingHypeTrain := tests.ValidOngoingHypeTrain()
		participation := tests.ValidParticipation()
		participation.Timestamp = validOngoingHypeTrain.StartedAt.Add(5 * time.Second)

		Convey("when channel has hype train disabled", func() {
			validConfig.Enabled = false
			configsDB.GetConfigReturns(&validConfig, nil)
			participantDB.GetHypeTrainParticipantsReturns([]percy.Participant{}, nil)
			approachingDB.GetApproachingReturns(nil, nil)
			bitsPinataDB.GetActiveBitsPinataReturns(nil, nil)

			Convey("does not record participation", func() {
				err := engine.RecordParticipation(ctx, participation)
				So(err, ShouldBeNil)
				So(participationDB.RecordChannelParticipationCallCount(), ShouldEqual, 0)
			})

			Convey("returns an error when", func() {
				Convey("fails to retrieve configs", func() {
					configsDB.GetConfigReturns(nil, errors.New("can't get"))
				})

				Convey("fails to get current train", func() {
					hypeTrainDB.GetMostRecentHypeTrainReturns(nil, errors.New("can't train"))
				})

				err := engine.RecordParticipation(ctx, participation)
				So(err, ShouldNotBeNil)
			})
		})
	})
}

func TestParticipants(t *testing.T) {
	Convey("Given a participants", t, func() {
		userID := uuid.New().String()
		channelID := uuid.New().String()

		participant := percy.NewParticipant(uuid.New().String(), percy.Participation{
			ChannelID: channelID,
			User:      percy.NewUser(userID),
			Timestamp: time.Now(),
			ParticipationIdentifier: percy.ParticipationIdentifier{
				Source: percy.BitsSource,
				Action: percy.CheerAction,
			},
			Quantity: 100,
		})

		Convey("returns participations", func() {
			quantity := participant.ParticipationQuantity(percy.ParticipationIdentifier{
				Source: percy.BitsSource,
				Action: percy.CheerAction,
			})

			So(quantity, ShouldEqual, 100)

			participations := participant.Participations()
			So(participations, ShouldHaveLength, 1)
			So(participations[0].Source, ShouldEqual, percy.BitsSource)
			So(participations[0].Action, ShouldEqual, percy.CheerAction)
			So(participations[0].Quantity, ShouldEqual, 100)
			So(participations[0].User.ID(), ShouldEqual, userID)
		})

		Convey("can add new participation events", func() {
			participant.AddParticipation(percy.ParticipationIdentifier{
				Source: percy.SubsSource,
				Action: percy.Tier1SubAction,
			}, 1)

			quantity := participant.ParticipationQuantity(percy.ParticipationIdentifier{
				Source: percy.SubsSource,
				Action: percy.Tier1SubAction,
			})

			So(quantity, ShouldEqual, 1)

			participations := participant.Participations()
			So(participations, ShouldHaveLength, 2)
		})

		Convey("groups participations of the same type and action", func() {
			participant.AddParticipation(percy.ParticipationIdentifier{
				Source: percy.BitsSource,
				Action: percy.CheerAction,
			}, 300)

			quantity := participant.ParticipationQuantity(percy.ParticipationIdentifier{
				Source: percy.BitsSource,
				Action: percy.CheerAction,
			})

			So(quantity, ShouldEqual, 400)

			participations := participant.Participations()
			So(participations, ShouldHaveLength, 1)
			So(participations[0].Source, ShouldEqual, percy.BitsSource)
			So(participations[0].Action, ShouldEqual, percy.CheerAction)
			So(participations[0].Quantity, ShouldEqual, 400)
		})
	})
}

func TestUser_NewUser(t *testing.T) {
	Convey("Creating a new user with an ID", t, func() {
		userID := uuid.New().String()

		user := percy.NewUser(userID)
		So(user.ID(), ShouldEqual, userID)
		So(user.IsAnonymous(), ShouldBeFalse)
	})

	Convey("Creating a new anonymous user", t, func() {
		user := percy.NewAnonymousCheerer()
		So(user.IsAnonymous(), ShouldBeTrue)
	})
}

func TestParticipationTotals_ParticipationsBySource(t *testing.T) {
	Convey("Given participation total", t, func() {
		totals := percy.ParticipationTotals{
			percy.ParticipationIdentifier{
				Source: percy.SubsSource,
				Action: percy.Tier1SubAction,
			}: 1,
			percy.ParticipationIdentifier{
				Source: percy.SubsSource,
				Action: percy.Tier1GiftedSubAction,
			}: 10,
			percy.ParticipationIdentifier{
				Source: percy.BitsSource,
				Action: percy.CheerAction,
			}: 100,
		}
		Convey("return the total for a specific source", func() {
			total := totals.ParticipationQuantitiesBySource(percy.SubsSource)
			So(total, ShouldEqual, 11)
		})
	})
}
