package percy

import "context"

//go:generate counterfeiter . DynamicTag

type DynamicTag interface {
	ApplyTag(ctx context.Context, channelID string) error
	RemoveTag(ctx context.Context, channelID string) error
}
