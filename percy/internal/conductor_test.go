package percy_test

import (
	"context"
	"errors"
	"testing"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/metrics"
	"code.justin.tv/commerce/percy/internal/tests"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func TestConductorUpdater_Update(t *testing.T) {
	Convey("Given a conductor updater", t, func() {
		hypeTrainDB := &internalfakes.FakeHypeTrainDB{}
		participantDB := &internalfakes.FakeParticipantDB{}
		leaderboard := &internalfakes.FakeLeaderboard{}
		pubsubPublisher := &internalfakes.FakePublisher{}
		eventbusPublisher := &internalfakes.FakeEventbusUpdatePublisher{}
		snsPublisher := &internalfakes.FakeSNSPublisher{}
		participationDB := &internalfakes.FakeParticipationDB{}
		statter := &metrics.NoopStatter{}
		updater := percy.NewConductorUpdater(hypeTrainDB, participantDB, leaderboard, pubsubPublisher, eventbusPublisher, snsPublisher, participationDB, statter)

		leaderboardID := percy.LeaderboardID{
			ChannelID:   "12345",
			HypeTrainID: uuid.New().String(),
			Source:      percy.SubsSource,
		}
		userID := "101010101"
		ctx := context.Background()

		testParticipations := []percy.Participation{
			{
				ChannelID: tests.ChannelID,
				Timestamp: time.Now(),
				User: percy.User{
					Id: tests.UserID,
				},
				Quantity: 100,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				},
			},
			{
				ChannelID: tests.ChannelID,
				Timestamp: time.Now(),
				User: percy.User{
					Id: tests.UserID,
				},
				Quantity: 30,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.ExtensionAction,
				},
			},
			{
				ChannelID: tests.ChannelID,
				Timestamp: time.Now(),
				User: percy.User{
					Id: tests.UserID,
				},
				Quantity: 20,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.PollAction,
				},
			},
			{
				ChannelID: tests.ChannelID,
				Timestamp: time.Now(),
				User: percy.User{
					Id: tests.UserID,
				},
				Quantity: 1,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.SubsSource,
					Action: percy.Tier1GiftedSubAction,
				},
			},
			{
				ChannelID: tests.ChannelID,
				Timestamp: time.Now(),
				User: percy.User{
					Id: tests.UserID,
				},
				Quantity: 1,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.SubsSource,
					Action: percy.Tier2GiftedSubAction,
				},
			},
			{
				ChannelID: tests.ChannelID,
				Timestamp: time.Now(),
				User: percy.User{
					Id: tests.UserID,
				},
				Quantity: 1,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.SubsSource,
					Action: percy.Tier3GiftedSubAction,
				},
			},
		}

		Convey("given new leaderboard leader", func() {
			ongoingHypeTrain := tests.ValidOngoingHypeTrain()
			ongoingHypeTrain.ChannelID = leaderboardID.ChannelID
			ongoingHypeTrain.ID = leaderboardID.HypeTrainID
			ongoingHypeTrain.Conductors = map[percy.ParticipationSource]percy.Conductor{
				percy.BitsSource: {
					Source: percy.BitsSource,
					User:   percy.NewUser(userID),
					Participations: percy.ParticipationTotals{
						percy.ParticipationIdentifier{
							Source: percy.BitsSource,
							Action: percy.CheerAction,
						}: 1,
					},
				},
			}

			participant := tests.ValidParticipant()

			hypeTrainDB.GetMostRecentHypeTrainReturns(&ongoingHypeTrain, nil)
			hypeTrainDB.UpdateConductorReturns(ongoingHypeTrain, nil)
			participantDB.GetHypeTrainParticipantReturns(&participant, nil)
			participationDB.GetChannelParticipationsReturns(testParticipations, nil)
			snsPublisher.PublishWebhookEventReturns(nil)
			eventbusPublisher.HypeTrainUpdateReturns(nil)

			Convey("retrieves participant details", func() {
				err := updater.Update(ctx, leaderboardID, userID)
				So(err, ShouldBeNil)
				So(participantDB.GetHypeTrainParticipantCallCount(), ShouldEqual, 1)

				_, calledHypeTrainID, calledUserID := participantDB.GetHypeTrainParticipantArgsForCall(0)
				So(calledHypeTrainID, ShouldEqual, leaderboardID.HypeTrainID)
				So(calledUserID, ShouldEqual, userID)

				Convey("updates conductor in hype train DB", func() {
					So(hypeTrainDB.UpdateConductorCallCount(), ShouldEqual, 1)
					pubsubPublisher.ConductorUpdateReturns(nil)

					_, calledChannelID, calledHypeTrainID, calledConductor := hypeTrainDB.UpdateConductorArgsForCall(0)
					So(calledChannelID, ShouldEqual, leaderboardID.ChannelID)
					So(calledHypeTrainID, ShouldEqual, leaderboardID.HypeTrainID)
					So(calledConductor, ShouldResemble, percy.Conductor{
						User:           percy.NewUser(userID),
						Source:         leaderboardID.Source,
						Participations: participant.ParticipationTotals,
					})

					Convey("publishes webhook and eventbus events", func() {
						time.Sleep(time.Second) // await goroutine
						So(snsPublisher.PublishWebhookEventCallCount(), ShouldEqual, 1)
						So(eventbusPublisher.HypeTrainUpdateCallCount(), ShouldEqual, 1)

						_, _, lastParticipationArg := snsPublisher.PublishWebhookEventArgsForCall(0)
						So(lastParticipationArg, ShouldResemble, testParticipations[0])

						_, _, lastParticipationArg = eventbusPublisher.HypeTrainUpdateArgsForCall(0)
						So(lastParticipationArg, ShouldResemble, testParticipations[0])
					})
				})
			})

			Convey("when the new leader has less participation quantity than the existing conductor", func() {
				ongoingHypeTrain.Conductors = map[percy.ParticipationSource]percy.Conductor{
					percy.BitsSource: {
						Source: percy.BitsSource,
						User:   percy.NewUser(userID),
						Participations: percy.ParticipationTotals{
							percy.ParticipationIdentifier{
								Source: percy.BitsSource,
								Action: percy.CheerAction,
							}: 1,
						},
					},
				}

				Convey("does not replace the existing conductor", func() {
					err := updater.Update(ctx, leaderboardID, userID)
					So(err, ShouldBeNil)
					So(hypeTrainDB.UpdateConductorCallCount(), ShouldEqual, 1)
				})
			})
		})

		Convey("returns an error when", func() {
			ongoingHypeTrain := tests.ValidOngoingHypeTrain()
			ongoingHypeTrain.ChannelID = leaderboardID.ChannelID
			ongoingHypeTrain.ID = leaderboardID.HypeTrainID

			participant := tests.ValidParticipant()

			// default mocks, to be overridden below
			hypeTrainDB.GetMostRecentHypeTrainReturns(&ongoingHypeTrain, nil)
			hypeTrainDB.UpdateConductorReturns(ongoingHypeTrain, nil)
			participantDB.GetHypeTrainParticipantReturns(&participant, nil)

			Convey("user ID is missing", func() {
				userID = ""
			})

			Convey("fails to get on-going hype train", func() {
				hypeTrainDB.GetMostRecentHypeTrainReturns(nil, errors.New("can't train"))
			})

			Convey("fails to get participant details", func() {
				participantDB.GetHypeTrainParticipantReturns(nil, errors.New("can't, just can't"))
			})

			Convey("no participant is found for the given user ID", func() {
				participantDB.GetHypeTrainParticipantReturns(nil, nil)
			})

			Convey("fails to update conductor on hype train", func() {
				hypeTrainDB.UpdateConductorReturns(percy.HypeTrain{}, errors.New("old conductor best conductor"))
			})

			err := updater.Update(ctx, leaderboardID, userID)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestConductorUpdater_Moderate(t *testing.T) {
	Convey("Given a conductor updater", t, func() {
		hypeTrainDB := &internalfakes.FakeHypeTrainDB{}
		participantDB := &internalfakes.FakeParticipantDB{}
		leaderboard := &internalfakes.FakeLeaderboard{}
		pubsubPublisher := &internalfakes.FakePublisher{}
		eventbusPublisher := &internalfakes.FakeEventbusUpdatePublisher{}
		snsPublisher := &internalfakes.FakeSNSPublisher{}
		participationDB := &internalfakes.FakeParticipationDB{}
		statter := &metrics.NoopStatter{}
		updater := percy.NewConductorUpdater(hypeTrainDB, participantDB, leaderboard, pubsubPublisher, eventbusPublisher, snsPublisher, participationDB, statter)

		ctx := context.Background()

		channelID := uuid.New().String()
		user := percy.NewUser(uuid.New().String())

		Convey("when there is an ongoing hype train", func() {
			train := tests.ValidOngoingHypeTrain()
			train.ChannelID = channelID
			hypeTrainDB.GetMostRecentHypeTrainReturns(&train, nil)

			Convey("and user has participated in the current hype train", func() {
				participant := tests.ValidParticipant()
				participant.User = user
				participantDB.GetHypeTrainParticipantReturns(&participant, nil)

				Convey("if user is the current Bits conductor", func() {
					train.Conductors[percy.BitsSource] = percy.Conductor{
						Participations: participant.ParticipationTotals,
						Source:         percy.BitsSource,
						User:           user,
					}

					err := updater.Moderate(ctx, channelID, user.ID())
					So(err, ShouldBeNil)
					So(leaderboard.ModerateCallCount(), ShouldEqual, len(participant.ParticipationTotals.Sources()))
					So(hypeTrainDB.RemoveConductorCallCount(), ShouldEqual, 1)
				})

				Convey("if user is the current Bits and Subs conductor", func() {
					train.Conductors[percy.BitsSource] = percy.Conductor{
						Participations: participant.ParticipationTotals,
						Source:         percy.BitsSource,
						User:           user,
					}
					train.Conductors[percy.SubsSource] = percy.Conductor{
						Participations: participant.ParticipationTotals,
						Source:         percy.SubsSource,
						User:           user,
					}

					err := updater.Moderate(ctx, channelID, user.ID())
					So(err, ShouldBeNil)
					So(leaderboard.ModerateCallCount(), ShouldEqual, len(participant.ParticipationTotals.Sources()))
					So(hypeTrainDB.RemoveConductorCallCount(), ShouldEqual, 2)
				})

				Convey("if user is not a current conductor", func() {
					train.Conductors[percy.BitsSource] = percy.Conductor{
						Participations: participant.ParticipationTotals,
						Source:         percy.BitsSource,
						User:           percy.NewUser(uuid.New().String()),
					}
					train.Conductors[percy.SubsSource] = percy.Conductor{
						Participations: participant.ParticipationTotals,
						Source:         percy.SubsSource,
						User:           percy.NewUser(uuid.New().String()),
					}

					err := updater.Moderate(ctx, channelID, user.ID())
					So(err, ShouldBeNil)
					So(leaderboard.ModerateCallCount(), ShouldEqual, len(participant.ParticipationTotals.Sources()))
					So(hypeTrainDB.RemoveConductorCallCount(), ShouldEqual, 0)
				})
			})
		})

		Convey("do nothing if", func() {
			Convey("when there is no ongoing hype train", func() {
				hypeTrainDB.GetMostRecentHypeTrainReturns(nil, nil)
			})

			Convey("when user has not participated to the current hype train", func() {
				train := tests.ValidOngoingHypeTrain()
				train.ChannelID = channelID
				hypeTrainDB.GetMostRecentHypeTrainReturns(&train, nil)
				participantDB.GetHypeTrainParticipantReturns(nil, nil)
			})

			err := updater.Moderate(ctx, channelID, user.ID())
			So(err, ShouldBeNil)
			So(leaderboard.ModerateCallCount(), ShouldEqual, 0)
			So(hypeTrainDB.RemoveConductorCallCount(), ShouldEqual, 0)
		})

		Convey("returns an error if", func() {
			channelID := uuid.New().String()
			userID := uuid.New().String()

			train := tests.ValidOngoingHypeTrain()
			train.ChannelID = channelID

			hypeTrainDB.GetMostRecentHypeTrainReturns(&train, nil)

			participant := tests.ValidParticipant()
			participant.User = user
			participantDB.GetHypeTrainParticipantReturns(&participant, nil)

			Convey("channel ID is blank", func() {
				channelID = ""
			})

			Convey("user ID is blank", func() {
				userID = ""
			})

			Convey("fails to get current hype train", func() {
				hypeTrainDB.GetMostRecentHypeTrainReturns(nil, errors.New("trains DB error"))
			})

			Convey("fail to get participant details for user", func() {
				participantDB.GetHypeTrainParticipantReturns(nil, errors.New("participants DB error"))
			})

			Convey("fail to moderate user on leaderboard", func() {
				leaderboard.ModerateReturns(errors.New("leaderboard error"))
			})

			Convey("fail to remove user as a conductor", func() {
				train.Conductors[percy.BitsSource] = percy.Conductor{
					Participations: participant.ParticipationTotals,
					Source:         percy.BitsSource,
					User:           percy.NewUser(userID),
				}

				hypeTrainDB.RemoveConductorReturns(percy.HypeTrain{}, errors.New("remove conductor error"))
			})

			err := updater.Moderate(ctx, channelID, userID)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestConductorUpdater_Unmoderate(t *testing.T) {
	Convey("Given a conductor updater", t, func() {
		hypeTrainDB := &internalfakes.FakeHypeTrainDB{}
		participantDB := &internalfakes.FakeParticipantDB{}
		leaderboard := &internalfakes.FakeLeaderboard{}
		pubsubPublisher := &internalfakes.FakePublisher{}
		eventbusPublisher := &internalfakes.FakeEventbusUpdatePublisher{}
		snsPublisher := &internalfakes.FakeSNSPublisher{}
		participationDB := &internalfakes.FakeParticipationDB{}
		statter := &metrics.NoopStatter{}

		updater := percy.NewConductorUpdater(hypeTrainDB, participantDB, leaderboard, pubsubPublisher, eventbusPublisher, snsPublisher, participationDB, statter)

		ctx := context.Background()

		channelID := uuid.New().String()
		user := percy.NewUser(uuid.New().String())

		Convey("when there is an hype train going on", func() {
			train := tests.ValidOngoingHypeTrain()
			train.ChannelID = channelID
			hypeTrainDB.GetMostRecentHypeTrainReturns(&train, nil)

			Convey("user has participated in the current hype train", func() {
				participant := percy.Participant{
					ChannelID:   channelID,
					HypeTrainID: train.ID,
					ParticipationTotals: map[percy.ParticipationIdentifier]int{
						{Source: percy.BitsSource, Action: percy.CheerAction}:          100,
						{Source: percy.SubsSource, Action: percy.Tier1GiftedSubAction}: 10,
					},
					User: user,
				}
				participantDB.GetHypeTrainParticipantReturns(&participant, nil)

				Convey("unmoderate user from all sources participated", func() {
					err := updater.Unmoderate(ctx, channelID, user.ID())
					So(err, ShouldBeNil)
					So(leaderboard.UnmoderateCallCount(), ShouldEqual, len(participant.ParticipationTotals.Sources()))
				})
			})
		})

		Convey("do nothing if", func() {
			Convey("when there is no ongoing hype train", func() {
				hypeTrainDB.GetMostRecentHypeTrainReturns(nil, nil)
			})

			Convey("when user has not participated to the current hype train", func() {
				train := tests.ValidOngoingHypeTrain()
				train.ChannelID = channelID
				hypeTrainDB.GetMostRecentHypeTrainReturns(&train, nil)
				participantDB.GetHypeTrainParticipantReturns(nil, nil)
			})

			err := updater.Unmoderate(ctx, channelID, user.ID())
			So(err, ShouldBeNil)
			So(leaderboard.UnmoderateCallCount(), ShouldEqual, 0)
		})

		Convey("returns an error if", func() {
			channelID := uuid.New().String()
			userID := uuid.New().String()

			train := tests.ValidOngoingHypeTrain()
			train.ChannelID = channelID

			hypeTrainDB.GetMostRecentHypeTrainReturns(&train, nil)

			participant := tests.ValidParticipant()
			participant.User = user
			participantDB.GetHypeTrainParticipantReturns(&participant, nil)

			Convey("channel ID is blank", func() {
				channelID = ""
			})

			Convey("user ID is blank", func() {
				userID = ""
			})

			Convey("fails to get current hype train", func() {
				hypeTrainDB.GetMostRecentHypeTrainReturns(nil, errors.New("trains DB error"))
			})

			Convey("fail to get participant details for user", func() {
				participantDB.GetHypeTrainParticipantReturns(nil, errors.New("participants DB error"))
			})

			Convey("fail to unmoderate user on leaderboard", func() {
				leaderboard.UnmoderateReturns(errors.New("leaderboard error"))
			})

			err := updater.Unmoderate(ctx, channelID, userID)
			So(err, ShouldNotBeNil)
		})
	})
}
