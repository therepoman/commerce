package destiny

import (
	"net/http"

	"code.justin.tv/commerce/percy/internal/wrapper"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/subs/destiny/destinytwirp"
	"github.com/cactus/go-statsd-client/statsd"
)

func NewClient(host string, statter statsd.Statter, socksAddr string) destinytwirp.Destiny {
	return destinytwirp.NewDestinyProtobufClient(host, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Stats:          statter,
		StatNamePrefix: "destiny",
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(socksAddr),
			wrapper.NewRetryRoundTripWrapper(statter, "destiny", 1),
			wrapper.NewHystrixRoundTripWrapper("destiny", 2000),
		},
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: 200,
		},
	}))
}
