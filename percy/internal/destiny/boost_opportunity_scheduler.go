package destiny

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/sqs/models"
	"code.justin.tv/commerce/percy/internal/wrapper"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/subs/destiny/destinytwirp"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/golang/protobuf/ptypes/timestamp"
)

// Ticket to create Domain in Destiny https://jira.xarth.tv/browse/SUBS-9430
const (
	BoostOpportunityStartDomain      = "paidBoostOpportunity/start/notification"
	BoostOpportunityCompletionDomain = "paidBoostOpportunity/expired/notification"
)

type BoostOpportunityScheduler struct {
	client destinytwirp.Destiny
}

var _ percy.BoostOpportunityScheduler = &BoostOpportunityScheduler{}

func NewBoostOpportunityScheduler(host string, statter statsd.Statter, socksAddr string) *BoostOpportunityScheduler {
	return &BoostOpportunityScheduler{
		client: destinytwirp.NewDestinyProtobufClient(host, twitchclient.NewHTTPClient(twitchclient.ClientConf{
			Stats:          statter,
			StatNamePrefix: serviceName,
			RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
				wrapper.NewProxyRoundTripWrapper(socksAddr),
				wrapper.NewRetryRoundTripWrapper(statter, serviceName, defaultRetry),
				wrapper.NewHystrixRoundTripWrapper(serviceName, defaultTimeout),
			},
			Transport: twitchclient.TransportConf{
				MaxIdleConnsPerHost: 200,
			},
		})),
	}
}

func (bos *BoostOpportunityScheduler) ScheduleBoostOpportunityStart(ctx context.Context, channelID string, startsAt time.Time) error {
	data := models.BoostOpportunityStartEvent{
		ChannelID: channelID,
	}

	payload, err := json.Marshal(data)
	if err != nil {
		return err
	}

	boostOpportunityStartEvent := destinytwirp.Event{
		Id:       "boost-opportunity-start-" + channelID,
		Domain:   BoostOpportunityStartDomain,
		Payload:  payload,
		Encoding: "json",
		SendAt: &timestamp.Timestamp{
			Seconds: startsAt.Unix(),
		},
		MaxRetries: 5,
	}

	coolDownReqCtx := wrapper.GenerateRequestContext(ctx, serviceName, writeEventsAPI)
	_, err = bos.client.WriteEvents(coolDownReqCtx, &destinytwirp.WriteEventsRequest{
		Events: []*destinytwirp.Event{&boostOpportunityStartEvent},
	})

	return err
}

func (bos *BoostOpportunityScheduler) ScheduleBoostOpportunityCompletion(ctx context.Context, channelID, boostOpportunityID string, endsAt time.Time) error {
	data := models.BoostOpportunityCompletionEvent{
		ChannelID:          channelID,
		BoostOpportunityID: boostOpportunityID,
	}

	payload, err := json.Marshal(data)
	if err != nil {
		return err
	}

	boostOpportunityCompletionEvent := destinytwirp.Event{
		Id:       "boost-opportunity-completion-" + channelID,
		Domain:   BoostOpportunityCompletionDomain,
		Payload:  payload,
		Encoding: "json",
		SendAt: &timestamp.Timestamp{
			Seconds: endsAt.Add(1 * time.Second).Unix(),
		},
		MaxRetries: 5,
	}

	coolDownReqCtx := wrapper.GenerateRequestContext(ctx, serviceName, writeEventsAPI)
	_, err = bos.client.WriteEvents(coolDownReqCtx, &destinytwirp.WriteEventsRequest{
		Events: []*destinytwirp.Event{&boostOpportunityCompletionEvent},
	})

	return err
}
