package destiny

const (
	serviceName    = "destiny"
	defaultRetry   = 1
	defaultTimeout = 2000
	writeEventsAPI = "writeEvents"
)
