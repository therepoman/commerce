package destiny

import (
	"context"
	"encoding/json"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/sqs/models"
	"code.justin.tv/commerce/percy/internal/wrapper"
	sns_models "code.justin.tv/commerce/percy/models"
	"code.justin.tv/subs/destiny/destinytwirp"
	"github.com/golang/protobuf/ptypes/timestamp"
)

const (
	HypeTrainEndingDomain        = "hypetrain/ending/notification"
	HypeTrainActivityFeedsDomain = "hypetrain/activityfeeds"
)

type EventDB struct {
	client destinytwirp.Destiny
}

var _ percy.EventDB = &EventDB{}

func NewEventDB(destiny destinytwirp.Destiny) *EventDB {
	return &EventDB{
		client: destiny,
	}
}

func (db *EventDB) UpsertHypeTrainExpirationTimer(ctx context.Context, hypeTrain percy.HypeTrain) error {
	data := models.ExpireHypeTrainEvent{
		ChannelID:   hypeTrain.ChannelID,
		HypeTrainID: hypeTrain.ID,
	}

	payload, err := json.Marshal(data)
	if err != nil {
		return err
	}

	// Delay expiration in Destiny for about 5 seconds, to prevent preemptively dismissing front-end UI before their counter counts down to 0.
	expirationEvent := destinytwirp.Event{
		Id:       hypeTrain.ChannelID,
		Domain:   HypeTrainEndingDomain,
		Payload:  payload,
		Encoding: "json",
		SendAt: &timestamp.Timestamp{
			Seconds: hypeTrain.ExpiresAt.Add(1 * time.Second).Unix(),
		},
		MaxRetries: 5,
	}

	expReqCtx := wrapper.GenerateRequestContext(ctx, serviceName, writeEventsAPI)
	_, err = db.client.WriteEvents(expReqCtx, &destinytwirp.WriteEventsRequest{
		Events: []*destinytwirp.Event{&expirationEvent},
	})

	if err != nil {
		return err
	}

	return nil
}

func (db *EventDB) UpsertHypeTrainCoolDownTimer(ctx context.Context, hypeTrain percy.HypeTrain) error {
	data := sns_models.ActivityFeedEvent{
		ChannelID:   hypeTrain.ChannelID,
		HypeTrainID: hypeTrain.ID,
		EventType:   string(sns_models.HypeTrainCoolDownExpired),
	}

	payload, err := json.Marshal(data)
	if err != nil {
		return err
	}

	endOfCoolDownEvent := destinytwirp.Event{
		Id:       "cool-down-ending-" + hypeTrain.ChannelID,
		Domain:   HypeTrainActivityFeedsDomain,
		Payload:  payload,
		Encoding: "json",
		SendAt: &timestamp.Timestamp{
			Seconds: hypeTrain.ExpiresAt.Add(hypeTrain.Config.CooldownDuration).Unix(),
		},
		MaxRetries: 5,
	}

	coolDownReqCtx := wrapper.GenerateRequestContext(ctx, serviceName, writeEventsAPI)
	_, err = db.client.WriteEvents(coolDownReqCtx, &destinytwirp.WriteEventsRequest{
		Events: []*destinytwirp.Event{&endOfCoolDownEvent},
	})

	return err
}
