package percy

import (
	"context"
	"crypto/md5"
	"encoding/binary"
	"io"
	"math/rand"
	"time"

	"code.justin.tv/commerce/logrus"
	"github.com/pkg/errors"
)

type SelectedReward struct {
	UserID      string
	RewardID    string
	Type        RewardType
	RewardLevel int
}

type EntitledParticipants map[string][]Reward

//go:generate counterfeiter . EntitlementDB

type EntitlementDB interface {
	CreateEmoteEntitlement(ctx context.Context, hypeTrain HypeTrain, entitledUsers EntitledParticipants) error
	ReadEmoteEntitlements(ctx context.Context, entitledUserID string) (map[string]string, error)
}

func (e *engine) EntitleParticipants(ctx context.Context, hypeTrain HypeTrain, participants ...Participant) (EntitledParticipants, error) {
	// Retrieve level rewards and shuffle order
	levelSettings := hypeTrain.Config.LevelSettings()
	curLevelIndex := hypeTrain.CompletedLevel() - 1

	if len(levelSettings[0].Rewards) == 0 {
		return nil, nil
	}

	// Double-check to make sure the hype train completed at least one level
	if curLevelIndex < 0 {
		return nil, nil
	}

	if err := shuffleRewards(hypeTrain.ID, levelSettings); err != nil {
		return nil, err
	}

	entitledParticipants := make(map[string][]Reward)

	// Add the streamer to the list of eligible users for an emote reward
	eligibleUsers := make([]User, 0, len(participants)+1)
	eligibleUsers = append(eligibleUsers, NewUser(hypeTrain.ChannelID))

	for _, participant := range participants {
		if len(participant.Rewards) > 0 {
			// do not entitle user reward again if we have already granted reward (likely from the previous retry)
			continue
		}
		if participant.ParticipationTotals.ParticipationPoints(hypeTrain.Config) >= int(hypeTrain.Config.KickoffConfig.MinPoints) {
			eligibleUsers = append(eligibleUsers, participant.User)
		}
	}

	logrus.WithFields(logrus.Fields{
		"channelID":   hypeTrain.ChannelID,
		"hypeTrainID": hypeTrain.ID,
	}).Infof("Granting %d participants rewards for completing a level %d hype train", len(eligibleUsers), hypeTrain.CompletedLevel())

	// Iterate over participants to find an emote entitlement the participant is eligible for
	for _, user := range eligibleUsers {
		// Get participant's prior hype train rewards
		entitledEmotes, err := e.EntitlementDB.ReadEmoteEntitlements(ctx, user.ID())
		if err != nil {
			return nil, err
		}

		selectedEmote := selectReward(user.ID(), entitledEmotes, levelSettings, curLevelIndex)

		rewardsToEntitleMap := map[Reward]interface{}{
			selectedEmote: nil,
		}

		for source, conductor := range hypeTrain.Conductors {
			if conductor.User.ID() == user.ID() {
				for _, reward := range hypeTrain.Config.ConductorRewards[source][CurrentConductor] {
					reward.RewardLevel = selectedEmote.RewardLevel
					rewardsToEntitleMap[reward] = nil
				}
			}
		}

		entitledParticipants[user.ID()] = []Reward{}
		for reward := range rewardsToEntitleMap {
			entitledParticipants[user.ID()] = append(entitledParticipants[user.ID()], reward)
		}
	}

	// Send reward events to spade
	go e.sendRewardItemReceivedEvents(hypeTrain, entitledParticipants)

	// Pass map to CreateEmoteEntitlements
	err := e.EntitlementDB.CreateEmoteEntitlement(ctx, hypeTrain, entitledParticipants)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create emote entitlements")
	}

	// Update all participants with the rewards they have received
	go func() {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		rewardedParticipants := make([]Participant, 0, len(participants))
		for _, participant := range participants {
			if rewards, ok := entitledParticipants[participant.User.ID()]; ok {
				participant.Rewards = rewards
				rewardedParticipants = append(rewardedParticipants, participant)
			}
		}

		err := e.ParticipantDB.PutHypeTrainParticipants(ctx, rewardedParticipants...)
		if err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{
				"hypeTrainID": hypeTrain.ID,
			}).Error("Failed to update hype train participant rewards")
		}
	}()

	return entitledParticipants, err
}

// Used to for CS/Support cases where we want to manually entitle a user hype train reward
// Intentionally not checking if the user has met the participation threshold to be eligible for a rewards
func (e *engine) AdminEntitleParticipant(ctx context.Context, hypeTrain HypeTrain, userID string) (*Reward, error) {
	// Retrieve level rewards and shuffle order
	levelSettings := hypeTrain.Config.LevelSettings()
	curLevelIndex := hypeTrain.CompletedLevel() - 1

	if len(levelSettings[0].Rewards) == 0 {
		return nil, nil
	}

	// Double-check to make sure the hype train completed at least one level
	if curLevelIndex < 0 {
		return nil, nil
	}

	if err := shuffleRewards(hypeTrain.ID, levelSettings); err != nil {
		return nil, err
	}

	entitledParticipants := make(map[string][]Reward)
	entitledEmotes, err := e.EntitlementDB.ReadEmoteEntitlements(ctx, userID)
	if err != nil {
		return nil, err
	}

	selectedEmote := selectReward(userID, entitledEmotes, levelSettings, curLevelIndex)
	if len(selectedEmote.ID) == 0 {
		return nil, nil
	}

	entitledParticipants[userID] = []Reward{selectedEmote}

	if err := e.EntitlementDB.CreateEmoteEntitlement(ctx, hypeTrain, entitledParticipants); err != nil {
		return nil, errors.Wrap(err, "failed to create emote entitlement")
	}

	return &selectedEmote, nil
}

// A user that has every emote will return an empty string
func selectReward(userID string, entitledRewards map[string]string, levelSettings []LevelSetting, curLevelIndex int) Reward {
	// Return the first emote entitlement for the hype train level if the user has no previous entitlements
	if len(entitledRewards) == 0 {
		return Reward{
			ID:          levelSettings[curLevelIndex].Rewards[0].ID,
			Type:        EmoteReward,
			RewardLevel: curLevelIndex + 1,
			GroupID:     "",
		}
	}

	for i := curLevelIndex; i >= 0; i-- {
		for j := 0; j < len(levelSettings[curLevelIndex].Rewards); j++ {
			_, ok := entitledRewards[levelSettings[i].Rewards[j].ID]
			if !ok {
				return Reward{
					ID:          levelSettings[i].Rewards[j].ID,
					Type:        EmoteReward,
					RewardLevel: i + 1,
					GroupID:     "",
				}
			}

		}
	}

	return Reward{
		ID:          "",
		Type:        EmoteReward,
		RewardLevel: curLevelIndex + 1,
		GroupID:     "",
	}
}

func shuffleRewards(id string, levels []LevelSetting) error {
	h := md5.New()
	_, err := io.WriteString(h, id)
	if err != nil {
		return err
	}
	var seed uint64 = binary.BigEndian.Uint64(h.Sum(nil))
	rand.Seed(int64(seed))

	for i := 0; i < len(levels); i++ {
		i := i
		rand.Shuffle(len(levels[i].Rewards), func(j, k int) {
			levels[i].Rewards[j], levels[i].Rewards[k] = levels[i].Rewards[k], levels[i].Rewards[j]
		})
	}

	return nil
}
