package redis

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net"
	"time"

	"code.justin.tv/chat/rediczar/redefault"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/go-redis/redis/v7"
	"golang.org/x/net/proxy"
)

const (
	// DefaultMaxConcurrentRequest is the number of maximum concurrent requests when it is not specified
	DefaultMaxConcurrentRequest = 1000
	// DefaultVolumeThreshold is the number of maximum volume threshold when it is not specified
	DefaultVolumeThreshold = 20
	// DefaultSleepWindow is the sleep window in milliseconds when it is not specified
	DefaultSleepWindow = 5000
	// DefaultErrorPercentThreshold is the error percent threshold when it is not specified
	DefaultErrorPercentThreshold = 20

	DefaultTimeout = 1000

	serviceName        = "redis"
	redisLatencyMetric = "redis-user-anniversary-latency"
	redisErrorMetric   = "redis-user-anniversary-error"
)

type UserAnniversaryCache struct {
	client  *redis.ClusterClient
	statter statsd.Statter
}

// userAnniversaryCache connects to a specific Redis instance.
// Cluster option should be set if using Redis clustering mode.
func NewUserAnniversaryCache(addr, ns string, statter statsd.Statter, socksAddr string) (UserAnniversaryCache, error) {
	hystrixConfig := hystrix.CommandConfig{
		Timeout:                DefaultTimeout,
		MaxConcurrentRequests:  DefaultMaxConcurrentRequest,
		RequestVolumeThreshold: DefaultVolumeThreshold,
		SleepWindow:            DefaultSleepWindow,
		ErrorPercentThreshold:  DefaultErrorPercentThreshold,
	}
	hystrix.ConfigureCommand(serviceName, hystrixConfig)

	// create redis client
	clustersOpts := redefault.ClusterOpts{
		ReadOnly:     true,
		PoolSize:     250,
		MaxRedirects: 3,
		ReadTimeout:  200 * time.Millisecond,
		WriteTimeout: 1 * time.Second,
	}

	if socksAddr != "" {
		dialer, err := proxy.SOCKS5("tcp", socksAddr, nil, proxy.Direct)
		if err != nil {
			log.Fatalf("can't connect to the proxy: %v", err)
		}
		clustersOpts.Dialer = func(ctx context.Context, network, addr string) (net.Conn, error) {
			return dialer.Dial(network, addr)
		}
	}

	client := redefault.NewClusterClient(addr, &clustersOpts)

	return UserAnniversaryCache{
		client:  client,
		statter: statter,
	}, nil
}

// Writes the creation date to the Redis cluster via Set command
func (u UserAnniversaryCache) AddNewCreationDate(ctx context.Context, channelID string, createdOn time.Time) error {
	createdOnString := createdOn.Format(time.RFC3339)

	start := time.Now()
	err := hystrix.Do(serviceName, func() (e error) {
		_, e = u.client.WithContext(ctx).Set(makeUserAnniversaryCacheKey(channelID), createdOnString, time.Hour*24).Result()
		return e
	}, nil)
	duration := time.Since(start)

	go func() {
		_ = u.statter.TimingDuration(redisLatencyMetric, duration, 1.0)
		if err != nil {
			_ = u.statter.Inc(redisErrorMetric, 1, 1.0)
		}
	}()

	return err
}

// Reads the creation date from the Redis cluster via Get command
// Returns empty string on cache miss
func (u UserAnniversaryCache) GetCreationDate(ctx context.Context, channelID string) (time.Time, error) {
	createdOnString := ""

	startTime := time.Now()
	err := hystrix.Do(serviceName, func() (e error) {
		createdOnString, e = u.client.WithContext(ctx).Get(makeUserAnniversaryCacheKey(channelID)).Result()
		return e
	}, nil)
	duration := time.Since(startTime)

	go func() {
		_ = u.statter.TimingDuration(redisLatencyMetric, duration, 1.0)
		if err != nil {
			_ = u.statter.Inc(redisErrorMetric, 1, 1.0)
		}
	}()

	if err != nil {
		// User not found (cache miss)
		if errors.Is(err, redis.Nil) {
			return time.Time{}, nil
		}
		return time.Time{}, err
	}

	createdOn, err := time.Parse(time.RFC3339, createdOnString)
	if err != nil {
		return time.Time{}, err
	}

	return createdOn, err
}

func makeUserAnniversaryCacheKey(id string) string {
	return fmt.Sprintf("userAnniversary:%s", id)
}
