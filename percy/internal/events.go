package percy

import (
	"context"
	"errors"

	"code.justin.tv/commerce/logrus"
)

//go:generate counterfeiter . EventDB

type EventDB interface {
	UpsertHypeTrainExpirationTimer(ctx context.Context, hypeTrain HypeTrain) error
	UpsertHypeTrainCoolDownTimer(ctx context.Context, hypeTrain HypeTrain) error
}

func (e *engine) ScheduleHypeTrainEnding(ctx context.Context, hypeTrain HypeTrain) error {
	if !hypeTrain.IsValid() {
		return errors.New("invalid hype train")
	}

	err := e.EventDB.UpsertHypeTrainExpirationTimer(ctx, hypeTrain)
	if err != nil {
		return err
	}

	// Notification for when the cool down period ends
	go func() {
		err := e.EventDB.UpsertHypeTrainCoolDownTimer(context.Background(), hypeTrain)
		// Ignore the error here - failure on this is not worth taking any action but just log
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"channelID":   hypeTrain.ChannelID,
				"hypeTrainID": hypeTrain.ID,
			}).WithError(err).Error("Failed to send cooldown event to Destiny")
		}
	}()

	return nil
}
