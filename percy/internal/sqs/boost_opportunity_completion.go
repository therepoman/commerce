package sqs

import (
	"context"
	"encoding/json"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/sqs/models"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/pkg/errors"
)

const (
	boostOpportunityCompletionTimeout = 3 * time.Second
)

type BoostOpportunityCompletionWorker struct {
	LaunchPad percy.LaunchPad
}

func NewBoostOpportunityCompletionWorker(launchPad percy.LaunchPad) *BoostOpportunityCompletionWorker {
	return &BoostOpportunityCompletionWorker{
		LaunchPad: launchPad,
	}
}

func (w *BoostOpportunityCompletionWorker) Handle(msg *sqs.Message) error {
	ctx, cancel := context.WithTimeout(context.Background(), boostOpportunityCompletionTimeout)
	defer cancel()

	event, err := parseBoostOpportunityCompletionEvent(msg)
	if err != nil {
		return err
	}

	if event.ChannelID == "" {
		return errors.New("received event with empty channelID")
	}

	if event.BoostOpportunityID == "" {
		return errors.New("received event with empty boost opportunity id")
	}

	return w.LaunchPad.CompleteBoostOpportunity(ctx, event.ChannelID, event.BoostOpportunityID)
}

func parseBoostOpportunityCompletionEvent(msg *sqs.Message) (*models.BoostOpportunityCompletionEvent, error) {
	var snsMsg models.SNSMessage
	err := json.Unmarshal([]byte(*msg.Body), &snsMsg)
	if err != nil {
		return nil, err
	}

	var event models.BoostOpportunityCompletionEvent
	err = json.Unmarshal([]byte(snsMsg.Message), &event)
	if err != nil {
		return nil, err
	}

	return &event, nil
}
