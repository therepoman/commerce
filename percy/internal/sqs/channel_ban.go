package sqs

import (
	"context"
	"encoding/json"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/sqs/models"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type channelBanWorker struct {
	updater percy.ConductorUpdater
}

func NewChannelBanWorker(updater percy.ConductorUpdater) *channelBanWorker {
	return &channelBanWorker{
		updater: updater,
	}
}

type channelBanEvent struct {
	Action    string
	ChannelID string    `json:"channel_id"`
	UserID    string    `json:"target_id"`
	ExpiresAt time.Time `json:"expires_at"`
}

const (
	// https://git-aws.internal.justin.tv/chat/tmi/blob/master/docs/subscribing_to_chat_events.md
	banAction   = "chat_ban"
	unbanAction = "chat_unban"
)

func (worker *channelBanWorker) Handle(msg *sqs.Message) error {
	ctx := context.TODO()

	event, err := parseChannelBanEvent(msg)
	if err != nil {
		return errors.Wrap(err, "failed to parse channel ban event")
	}

	if !event.ExpiresAt.IsZero() {
		// expiresAt indicates that this is a timeout event, which we don't need to handle
		return nil
	}

	var updaterError error
	switch event.Action {
	case banAction:
		updaterError = worker.updater.Moderate(ctx, event.ChannelID, event.UserID)
	case unbanAction:
		updaterError = worker.updater.Unmoderate(ctx, event.ChannelID, event.UserID)
	default:
		logrus.WithFields(logrus.Fields{
			"action":    event.Action,
			"channelID": event.ChannelID,
			"userID":    event.UserID,
		}).Warn("Unrecognized channel action")
	}

	if updaterError != nil {
		logrus.WithError(updaterError).WithFields(logrus.Fields{
			"action":    event.Action,
			"channelID": event.ChannelID,
			"userID":    event.UserID,
		}).Errorf("Failed to handle channel ban event")
		return updaterError
	}

	return nil
}

func parseChannelBanEvent(msg *sqs.Message) (channelBanEvent, error) {
	var snsMsg models.SNSMessage
	err := json.Unmarshal([]byte(*msg.Body), &snsMsg)
	if err != nil {
		return channelBanEvent{}, err
	}

	var event channelBanEvent
	err = json.Unmarshal([]byte(snsMsg.Message), &event)
	if err != nil {
		return channelBanEvent{}, err
	}

	action, ok := snsMsg.MessageAttributes["event"]
	if !ok {
		return channelBanEvent{}, errors.New("no 'event' field was found in the message")
	}

	event.Action = action.Value
	return event, nil
}
