package sqs

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/sqs/models"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/pkg/errors"
)

const (
	boostOpportunityStartTimeout = 3 * time.Second
)

type BoostOpportunityStartWorker struct {
	LaunchPad percy.LaunchPad
}

func NewBoostOpportunityStartWorker(launchPad percy.LaunchPad) *BoostOpportunityStartWorker {
	return &BoostOpportunityStartWorker{
		LaunchPad: launchPad,
	}
}

func (w *BoostOpportunityStartWorker) Handle(msg *sqs.Message) error {
	ctx, cancel := context.WithTimeout(context.Background(), boostOpportunityStartTimeout)
	defer cancel()

	event, err := parseBoostOpportunityStartEvent(msg)
	if err != nil {
		return err
	}

	if event.ChannelID == "" {
		return errors.New("received event with empty channelID")
	}

	if w.LaunchPad.ChannelIsAllowed(event.ChannelID) {
		_, err = w.LaunchPad.StartBoostOpportunity(ctx, event.ChannelID)
		if err != nil {
			return errors.Wrap(err, fmt.Sprintf("could not start boost opportunity with channel: %s", event.ChannelID))
		}
	}
	return nil
}

func parseBoostOpportunityStartEvent(msg *sqs.Message) (*models.BoostOpportunityStartEvent, error) {
	var snsMsg models.SNSMessage
	err := json.Unmarshal([]byte(*msg.Body), &snsMsg)
	if err != nil {
		return nil, err
	}

	var event models.BoostOpportunityStartEvent
	err = json.Unmarshal([]byte(snsMsg.Message), &event)
	if err != nil {
		return nil, err
	}

	return &event, nil
}
