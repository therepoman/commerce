package sqs_test

import (
	"encoding/json"
	"errors"
	"testing"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	sqs_worker "code.justin.tv/commerce/percy/internal/sqs"
	"code.justin.tv/commerce/percy/internal/sqs/models"
	"code.justin.tv/commerce/percy/internal/tests"
	sns_models "code.justin.tv/commerce/percy/models"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCooldownExpirationEventWorker_Handle(t *testing.T) {
	Convey("given a cooldown expiration event worker", t, func() {
		engine := &internalfakes.FakeEngine{}
		publisher := &internalfakes.FakePublisher{}

		worker := sqs_worker.NewCooldownExpirationEventWorker(engine, publisher)

		validHypeTrain := tests.ValidHypeTrain()

		validEvent := sns_models.ActivityFeedEvent{
			ChannelID:   "1234",
			HypeTrainID: validHypeTrain.ID,
			EventType:   string(sns_models.HypeTrainCoolDownExpired),
		}

		sqsMessage := createValidEvent(validEvent)

		Convey("it returns an error", func() {
			Convey("when we have an invalid event", func() {
				sqsMessage.Body = pointers.StringP("this is invalid")
			})

			Convey("when we fail to get most recent hype train", func() {
				engine.GetMostRecentHypeTrainReturns(nil, errors.New("WALRUS STRIKE"))
			})

			Convey("when we fail to publish to pubsub", func() {
				engine.GetMostRecentHypeTrainReturns(&validHypeTrain, nil)
				publisher.CooldownExpiredReturns(errors.New("WALRUS STRIKE"))
			})

			err := worker.Handle(sqsMessage)

			So(err, ShouldNotBeNil)
		})

		Convey("it returns nil", func() {
			publisherCallCount := 0

			Convey("When the event is not a cooldown expiration event", func() {
				sqsMessage = createValidEvent(sns_models.ActivityFeedEvent{
					ChannelID:   "1234",
					HypeTrainID: validHypeTrain.ID,
					EventType:   "walrus",
				})
			})

			Convey("when the most recent hype train does not match the hype train in the message", func() {
				engine.GetMostRecentHypeTrainReturns(&validHypeTrain, nil)
				sqsMessage = createValidEvent(sns_models.ActivityFeedEvent{
					ChannelID:   "1234",
					HypeTrainID: "walrus_train",
					EventType:   string(sns_models.HypeTrainCoolDownExpired),
				})
			})

			Convey("when the cooldown expiration pubsub successfully publishes", func() {
				engine.GetMostRecentHypeTrainReturns(&validHypeTrain, nil)
				publisher.CooldownExpiredReturns(nil)
				publisherCallCount = 1
			})

			err := worker.Handle(sqsMessage)

			So(err, ShouldBeNil)
			So(publisher.CooldownExpiredCallCount(), ShouldEqual, publisherCallCount)
		})
	})
}

func createValidEvent(validEvent sns_models.ActivityFeedEvent) *sqs.Message {
	eventJson, _ := json.Marshal(validEvent)
	snsMessage := models.SNSMessage{
		Message: string(eventJson),
	}
	snsJson, _ := json.Marshal(snsMessage)
	sqsBody := string(snsJson)

	return &sqs.Message{
		Body: &sqsBody,
	}
}
