package sqs

import (
	"encoding/json"
	"testing"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/pantheon"
	"code.justin.tv/commerce/percy/internal/sqs/models"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPantheonUpdateWorker(t *testing.T) {
	Convey("Given a sqs pool that receives a message", t, func() {
		updater := &internalfakes.FakeConductorUpdater{}
		worker := NewPantheonUpdateWorker(updater)
		updater.UpdateReturns(nil)

		notification := leaderboardUpdateNotification{
			Identifier: leaderboardUpdateNotificationIdentifier{
				Domain:      pantheon.Domain,
				GroupingKey: "123:456:SUBS_SOURCE",
			},
			Top: []*pantheonrpc.LeaderboardEntry{
				{
					Rank:      int64(1),
					Score:     int64(100),
					EntryKey:  uuid.New().String(),
					Moderated: false,
				},
			},
		}
		Convey("given a valid leaderboard notification", func() {
			msg := marshalMessage(notification)

			Convey("updates conductor for the given hype train", func() {
				err := worker.Handle(msg)
				So(err, ShouldBeNil)
				So(updater.UpdateCallCount(), ShouldEqual, 1)
			})
		})

		Convey("given a notification from a different domain", func() {
			notification.Identifier.Domain = "bits"
			msg := marshalMessage(notification)

			Convey("does not call updater", func() {
				err := worker.Handle(msg)
				So(err, ShouldBeNil)
				So(updater.UpdateCallCount(), ShouldEqual, 0)
			})
		})

		Convey("given a valid notification with no Top", func() {
			notification.Top = nil
			msg := marshalMessage(notification)

			Convey("does not call updater", func() {
				err := worker.Handle(msg)
				So(err, ShouldBeNil)
				So(updater.UpdateCallCount(), ShouldEqual, 0)
			})
		})

		Convey("returns an error if", func() {
			Convey("leaderboard Id isn't parsable", func() {
				notification.Identifier.GroupingKey = "not:right"
			})

			Convey("conductor updater errors", func() {
				updater.UpdateReturns(errors.New("can't update"))
			})

			msg := marshalMessage(notification)
			err := worker.Handle(msg)
			So(err, ShouldNotBeNil)
		})
	})
}

func marshalMessage(notification leaderboardUpdateNotification) *sqs.Message {
	notificationJson, _ := json.Marshal(notification)
	snsMessage := models.SNSMessage{
		Message: string(notificationJson),
	}
	snsJson, _ := json.Marshal(snsMessage)
	sqsBody := string(snsJson)

	return &sqs.Message{
		Body: &sqsBody,
	}
}
