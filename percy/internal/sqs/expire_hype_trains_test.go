package sqs_test

import (
	"encoding/json"
	"errors"
	"testing"

	"code.justin.tv/commerce/percy/internal/internalfakes"
	sqs_worker "code.justin.tv/commerce/percy/internal/sqs"
	"code.justin.tv/commerce/percy/internal/sqs/models"
	"code.justin.tv/commerce/percy/internal/tests"
	"github.com/aws/aws-sdk-go/service/sqs"
	. "github.com/smartystreets/goconvey/convey"
)

func TestExpireHypeTrainWorker(t *testing.T) {
	Convey("Given a sqs pool that receives a message", t, func() {
		engine := internalfakes.FakeEngine{}

		worker := sqs_worker.ExpireHypeTrainWorker{
			Engine: &engine,
		}

		validHypeTrain := tests.ValidHypeTrain()

		validEvent := models.ExpireHypeTrainEvent{
			ChannelID:   "1234",
			HypeTrainID: validHypeTrain.ID,
		}

		eventJson, _ := json.Marshal(validEvent)
		snsMessage := models.SNSMessage{
			Message: string(eventJson),
		}
		snsJson, _ := json.Marshal(snsMessage)
		sqsBody := string(snsJson)

		sqsMessage := &sqs.Message{
			Body: &sqsBody,
		}

		Convey("when there is a invalid hype train", func() {
			engine.GetMostRecentHypeTrainReturns(nil, errors.New("invalid hype train"))

			err := worker.Handle(sqsMessage)
			So(err, ShouldNotBeNil)
		})

		Convey("when there is a valid hype train", func() {
			engine.GetMostRecentHypeTrainReturns(&validHypeTrain, nil)

			Convey("when engine succeeds at ending the hype train", func() {
				err := worker.Handle(sqsMessage)
				So(err, ShouldBeNil)
			})

			Convey("when engine fails to end the hype train", func() {
				engine.EndHypeTrainReturns(errors.New("can't end"))
				err := worker.Handle(sqsMessage)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
