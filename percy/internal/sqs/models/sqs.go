package models

type ExpireHypeTrainEvent struct {
	ChannelID   string `json:"channel_id"`
	HypeTrainID string `json:"hypetrain_id"`
}

type SNSMessage struct {
	MessageID         string                         `json:"MessageID"`
	Message           string                         `json:"Message"`
	MessageAttributes map[string]SNSMessageAttribute `json:"MessageAttributes,optional"` //nolint //Apparently optional does not work on a map, but I'm too afraid to remove it
}

type SNSMessageAttribute struct {
	Type  string `json:"Type"`
	Value string `json:"Value"`
}

type BoostOpportunityEventType string

const (
	BoostOpportunityStart      = BoostOpportunityEventType("BOOST_OPPORTUNITY_START")
	BoostOpportunityCompletion = BoostOpportunityEventType("BOOST_OPPORTUNITY_COMPLETION")
)

type BoostOpportunityStartEvent struct {
	ChannelID string `json:"channel_id"`
}

type BoostOpportunityCompletionEvent struct {
	BoostOpportunityID string `json:"boost_opportunity_id"`
	ChannelID          string `json:"channel_id"`
}

type BitsPinataDropEvent struct {
	ChannelID string `json:"channel_id"`
	StreamID  string `json:"stream_id"`
}
