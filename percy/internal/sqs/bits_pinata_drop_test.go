package sqs_test

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	"code.justin.tv/commerce/gogogadget/pointers"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/miniexperiments/miniexperimentsfakes"
	sqs_worker "code.justin.tv/commerce/percy/internal/sqs"
	"code.justin.tv/commerce/percy/internal/sqs/models"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestBitsPinataDropWorker_Handle(t *testing.T) {
	Convey("given a bits pinata drop event worker", t, func() {
		engine := &internalfakes.FakeEngine{}
		pinata := &internalfakes.FakeBitsPinataDB{}
		liveline := &internalfakes.FakeChannelStatusDB{}
		pinataExperimentClient := &miniexperimentsfakes.FakeClient{}

		worker := sqs_worker.PinataDropWorker{
			Engine:                 engine,
			Pinata:                 pinata,
			Liveline:               liveline,
			PinataExperimentClient: pinataExperimentClient,
		}

		Convey("it returns an error", func() {
			Convey("when we have an invalid message", func() {
				invalidSNSJson := getSNSJson(`{`)
				invalidSQSMsg := &sqs.Message{
					Body: pointers.StringP(invalidSNSJson),
				}
				err := worker.Handle(invalidSQSMsg)
				So(err, ShouldNotBeNil)
			})

			Convey("when we have a valid message but an empty channelID", func() {
				validSNSJson := getBitsPinataDropEventJson("", "stream-123")
				validSQSMsg := &sqs.Message{
					Body: pointers.StringP(validSNSJson),
				}
				err := worker.Handle(validSQSMsg)
				So(err, ShouldNotBeNil)
			})

			Convey("when we have a valid message but an empty streamID", func() {
				validSNSJson := getBitsPinataDropEventJson("channel-123", "")
				validSQSMsg := &sqs.Message{
					Body: pointers.StringP(validSNSJson),
				}
				err := worker.Handle(validSQSMsg)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("it returns nil", func() {
			Convey("when the event is the right type", func() {
				validEventJson := getBitsPinataDropEventJson("channel-123", "stream-123")
				validSNSJson := getSNSJson(validEventJson)
				validSQSMsg := &sqs.Message{
					Body: pointers.StringP(validSNSJson),
				}
				err := worker.Handle(validSQSMsg)
				So(err, ShouldBeNil)
			})
		})
	})
}

func TestBitsPinataDropWorker_DropBitsPinata(t *testing.T) {
	Convey("given a bits pinata drop event worker", t, func() {
		engine := &internalfakes.FakeEngine{}
		pinata := &internalfakes.FakeBitsPinataDB{}
		liveline := &internalfakes.FakeChannelStatusDB{}
		pinataExperimentClient := &miniexperimentsfakes.FakeClient{}

		worker := sqs_worker.PinataDropWorker{
			Engine:                 engine,
			Pinata:                 pinata,
			Liveline:               liveline,
			PinataExperimentClient: pinataExperimentClient,
		}

		validStreamData := percy.StreamData{
			ChannelID:   "channel-123",
			IsStreaming: true,
			BroadcastID: "stream-123",
			ViewCount:   100,
		}

		Convey("it returns an error", func() {
			Convey("when liveline returns an error", func() {
				liveline.GetStreamDataReturns(percy.StreamData{}, errors.New("error"))
				ctx := context.Background()
				err := worker.DropBitsPinata(ctx, "channel-123", "stream-123")
				So(err, ShouldNotBeNil)
			})

			Convey("when getting bits pinata returns an error", func() {
				liveline.GetStreamDataReturns(validStreamData, nil)
				pinata.GetActiveBitsPinataReturns(nil, errors.New("error"))

				ctx := context.Background()
				err := worker.DropBitsPinata(ctx, "channel-123", "stream-123")
				So(err, ShouldNotBeNil)
			})

			Convey("when getting hype train returns an error", func() {
				liveline.GetStreamDataReturns(validStreamData, nil)
				pinata.GetActiveBitsPinataReturns(nil, nil)
				engine.GetOngoingHypeTrainReturns(nil, nil, errors.New("error"))

				ctx := context.Background()
				err := worker.DropBitsPinata(ctx, "channel-123", "stream-123")
				So(err, ShouldNotBeNil)
			})

			Convey("when creating a bits pinata returns an error", func() {
				liveline.GetStreamDataReturns(validStreamData, nil)
				pinata.GetActiveBitsPinataReturns(nil, nil)
				engine.GetOngoingHypeTrainReturns(nil, nil, nil)
				pinata.CreateBitsPinataReturns(errors.New("error"))

				ctx := context.Background()
				err := worker.DropBitsPinata(ctx, "channel-123", "stream-123")
				So(err, ShouldNotBeNil)
			})

			Convey("it returns nil", func() {
				Convey("when stream view count is less than minimum", func() {
					streamData := percy.StreamData{
						ChannelID:   "channel-123",
						IsStreaming: true,
						BroadcastID: "broadcast-123",
						ViewCount:   1,
					}
					liveline.GetStreamDataReturns(streamData, nil)
					pinata.GetActiveBitsPinataReturns(nil, nil)
					engine.GetOngoingHypeTrainReturns(nil, nil, nil)
					pinata.CreateBitsPinataReturns(nil)

					ctx := context.Background()
					err := worker.DropBitsPinata(ctx, "channel-123", "stream-123")
					So(err, ShouldBeNil)
				})

				Convey("when broadcast ID is empty", func() {
					streamData := percy.StreamData{
						ChannelID:   "channel-123",
						IsStreaming: true,
						BroadcastID: "",
						ViewCount:   100,
					}
					liveline.GetStreamDataReturns(streamData, nil)
					pinata.GetActiveBitsPinataReturns(nil, nil)
					engine.GetOngoingHypeTrainReturns(nil, nil, nil)
					pinata.CreateBitsPinataReturns(nil)

					ctx := context.Background()
					err := worker.DropBitsPinata(ctx, "channel-123", "stream-123")
					So(err, ShouldBeNil)
				})

				Convey("when liveline broadcast ID and event stream ID do not match", func() {
					streamData := percy.StreamData{
						ChannelID:   "channel-123",
						IsStreaming: true,
						BroadcastID: "stream-321",
						ViewCount:   100,
					}
					liveline.GetStreamDataReturns(streamData, nil)
					pinata.GetActiveBitsPinataReturns(nil, nil)
					engine.GetOngoingHypeTrainReturns(nil, nil, nil)
					pinata.CreateBitsPinataReturns(nil)

					ctx := context.Background()
					err := worker.DropBitsPinata(ctx, "channel-123", "stream-123")
					So(err, ShouldBeNil)
				})

				Convey("when stream is no longer live", func() {
					streamData := percy.StreamData{
						ChannelID:   "channel-123",
						IsStreaming: false,
						BroadcastID: "broadcast-123",
						ViewCount:   100,
					}
					liveline.GetStreamDataReturns(streamData, nil)
					pinata.GetActiveBitsPinataReturns(nil, nil)
					engine.GetOngoingHypeTrainReturns(nil, nil, nil)
					pinata.CreateBitsPinataReturns(nil)

					ctx := context.Background()
					err := worker.DropBitsPinata(ctx, "channel-123", "stream-123")
					So(err, ShouldBeNil)
				})
			})
		})
	})
}

func TestBitsPinataDropWorker_getBitsPinataGoal(t *testing.T) {
	Convey("given a bits pinata drop event worker", t, func() {
		engine := &internalfakes.FakeEngine{}
		pinata := &internalfakes.FakeBitsPinataDB{}
		liveline := &internalfakes.FakeChannelStatusDB{}
		pinataExperimentClient := &miniexperimentsfakes.FakeClient{}

		worker := sqs_worker.PinataDropWorker{
			Engine:                 engine,
			Pinata:                 pinata,
			Liveline:               liveline,
			PinataExperimentClient: pinataExperimentClient,
		}

		// Bits Pinata Goal Values
		viewerCountMin := [...]int{5, 10, 100, 4000}
		bitsGoalMin := [...]int{50, 100, 750, 750}
		bitsGoalMax := [...]int{500, 1000, 3000, 5000}

		Convey("it returns false", func() {
			Convey("when viewer count is less than the minimum", func() {
				isValid, _ := worker.GetBitsPinataGoal(0)
				So(isValid, ShouldBeFalse)
			})
		})

		// Test each tier
		for i := 0; i < len(viewerCountMin); i++ {
			ii := i
			testCase := fmt.Sprintf("it returns a tier %d value", ii+1)
			Convey(testCase, func() {
				isValid, value := worker.GetBitsPinataGoal(viewerCountMin[ii])
				So(isValid, ShouldBeTrue)
				So(value, ShouldBeBetweenOrEqual, bitsGoalMin[ii], bitsGoalMax[ii])

				if ii < len(viewerCountMin)-1 {
					isValid, value = worker.GetBitsPinataGoal(viewerCountMin[ii+1] - 1)
					So(isValid, ShouldBeTrue)
					So(value, ShouldBeBetweenOrEqual, bitsGoalMin[ii], bitsGoalMax[ii])
				}
			})
		}
	})
}

func getBitsPinataDropEventJson(channelID string, streamID string) string {
	validEvent := models.BitsPinataDropEvent{
		ChannelID: channelID,
		StreamID:  streamID,
	}
	validEventJson, err := json.Marshal(validEvent)
	So(err, ShouldBeNil)
	return string(validEventJson)
}
