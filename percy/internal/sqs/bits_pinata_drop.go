package sqs

import (
	"context"
	"encoding/json"
	"math/rand"
	"time"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/miniexperiments"
	"code.justin.tv/commerce/percy/internal/miniexperiments/c2_bits_pinata"
	"code.justin.tv/commerce/percy/internal/pubsub"
	"code.justin.tv/commerce/percy/internal/sqs/models"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

type PinataDropWorker struct {
	Engine                 percy.Engine
	Pinata                 percy.BitsPinataDB
	Liveline               percy.ChannelStatusDB
	PinataExperimentClient miniexperiments.Client
	Publisher              pubsub.PinataPublisher
}

const (
	expireBitsPinataDropWorkerTimeout = 15 * time.Second
)

func NewPinataDropWorker(engine percy.Engine, pinata percy.BitsPinataDB, liveline percy.ChannelStatusDB, experimentClient miniexperiments.Client, publisher pubsub.PinataPublisher) *PinataDropWorker {
	return &PinataDropWorker{
		Engine:                 engine,
		Pinata:                 pinata,
		Liveline:               liveline,
		PinataExperimentClient: experimentClient,
		Publisher:              publisher,
	}
}

func (w *PinataDropWorker) Handle(msg *sqs.Message) error {
	ctx, cancel := context.WithTimeout(context.Background(), expireBitsPinataDropWorkerTimeout)
	defer cancel()

	event, err := parseDropBitsPinataEvent(msg)
	if err != nil {
		return err
	}

	// Check experiment
	treatment, err := w.PinataExperimentClient.GetTreatment(event.ChannelID)
	if err != nil {
		return err
	}
	if treatment == c2_bits_pinata.TreatmentControl {
		// if we're in the Control group, we can skip doing any pinata logic.
		return nil
	}

	// Check IDs
	if event.ChannelID == "" {
		return errors.New("received pinata drop event with empty channelID")
	}
	if event.StreamID == "" {
		return errors.New("received pinata drop event with empty streamID")
	}

	// Drop Pinata
	err = w.DropBitsPinata(ctx, event.ChannelID, event.StreamID)
	return err
}

func (w *PinataDropWorker) DropBitsPinata(ctx context.Context, channelID string, streamID string) error {
	logrus.WithFields(logrus.Fields{
		"channelID": channelID,
	}).Info("DropBitsPinata is called!")

	// Get stream data
	streamData, err := w.Liveline.GetStreamData(ctx, channelID)
	if err != nil {
		return errors.Wrap(err, "DropBitsPinata called but couldn't get stream data")
	}

	// Check if stream is live
	if !streamData.IsStreaming {
		log.WithFields(log.Fields{
			"channelID": channelID,
		}).Info("DropBitsPinata called when stream isn't live")
		return nil
	}

	// Check broadcast ID
	if streamData.BroadcastID == "" {
		log.WithFields(log.Fields{
			"channelID": channelID,
		}).Info("DropBitsPinata called but broadcast ID is empty")
		return nil
	}
	if streamData.BroadcastID != streamID {
		log.WithFields(log.Fields{
			"channelID": channelID,
		}).Info("DropBitsPinata called but stream ID doesn't match broadcast ID")
		return nil
	}

	// Check if stream has ongoing Bits Pinata
	pinata, err := w.Pinata.GetActiveBitsPinata(ctx, channelID)
	if err != nil {
		return err
	}
	if pinata != nil {
		log.WithFields(log.Fields{
			"channelID": channelID,
		}).Info("DropBitsPinata called when a pinata is already active")
		return nil
	}

	// Check if channel has ongoing hype train or hype train approaching
	hypeTrain, approaching, err := w.Engine.GetOngoingHypeTrain(ctx, channelID)
	if err != nil {
		return err
	}
	if hypeTrain != nil {
		log.WithFields(log.Fields{
			"channelID": channelID,
		}).Warn("Dropping bits pinata called but hype train is active")
		return nil
	}
	if approaching != nil {
		log.WithFields(log.Fields{
			"channelID": channelID,
		}).Warn("Dropping bits pinata called but hype train approaching is active")
		return nil
	}

	// Calculate bits goal for number of viewers
	isValidViewCount, bitsPinataGoal := w.GetBitsPinataGoal(streamData.ViewCount)
	if !isValidViewCount {
		log.WithFields(log.Fields{
			"channelID":   channelID,
			"viewerCount": streamData.ViewCount,
		}).Info("DropBitsPinata called when view count does not meet requirements")
		return nil
	}

	// Call DB to create the Pinata
	newBitsPinata := percy.BitsPinata{
		ChannelID:        channelID,
		CreatedAt:        time.Now().UTC(),
		PinataID:         uuid.New().String(),
		TimerID:          uuid.New().String(),
		StreamID:         streamData.BroadcastID,
		BitsToBreak:      int64(bitsPinataGoal),
		BitsContributed:  0,
		LastContribution: time.Now().UTC(),
	}
	err = w.Pinata.CreateBitsPinata(ctx, newBitsPinata)
	if err != nil {
		return err
	}

	// Send pubsub to the front end.
	err = w.Publisher.PublishPinataDrop(ctx, newBitsPinata.PinataID, channelID)
	if err != nil {
		return err
	}

	return nil
}

func (w *PinataDropWorker) GetBitsPinataGoal(viewerCount int) (bool, int) {
	// Bits Pinata Goal Values
	viewerCountMin := [...]int{5, 10, 100, 4000}
	bitsGoalMin := [...]int{50, 100, 750, 750}
	bitsGoalMax := [...]int{500, 1000, 3000, 5000}

	// Check if view count meets tier 1 minimum
	if viewerCount < viewerCountMin[0] {
		return false, 0
	}

	// Iterate through viewer tier minimums
	for i := len(viewerCountMin) - 1; i >= 0; i-- {
		if viewerCount >= viewerCountMin[i] {
			return true, rand.Intn(bitsGoalMax[i]-bitsGoalMin[i]) + bitsGoalMin[i]
		}
	}

	// Unexpected viewCount value
	return false, 0
}

func parseDropBitsPinataEvent(msg *sqs.Message) (*models.BitsPinataDropEvent, error) {
	var snsMsg models.SNSMessage
	err := json.Unmarshal([]byte(*msg.Body), &snsMsg)
	if err != nil {
		return nil, err
	}

	var event models.BitsPinataDropEvent
	err = json.Unmarshal([]byte(snsMsg.Message), &event)
	if err != nil {
		return nil, err
	}

	return &event, nil
}
