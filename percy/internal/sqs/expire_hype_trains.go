package sqs

import (
	"context"
	"encoding/json"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/sqs/models"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/sirupsen/logrus"
)

const (
	expireHypeTrainWorkerTimeout = 15 * time.Second
)

type ExpireHypeTrainWorker struct {
	Engine percy.Engine
}

func (w *ExpireHypeTrainWorker) Handle(msg *sqs.Message) error {
	ctx, cancel := context.WithTimeout(context.Background(), expireHypeTrainWorkerTimeout)
	defer cancel()

	event, err := parseMessage(msg)
	if err != nil {
		return err
	}

	hypeTrain, err := w.Engine.GetMostRecentHypeTrain(ctx, event.ChannelID)
	if err != nil {
		return err
	}

	if hypeTrain == nil {
		logrus.Warnf("No hype train was found for channel %s to end", event.ChannelID)
		return nil
	}

	if hypeTrain.ID != event.HypeTrainID {
		logrus.Warnf("Current hype train ID %s does not match hype train ID in the message %s", hypeTrain.ID, event.HypeTrainID)
		return nil
	}

	if err = w.Engine.EndHypeTrain(ctx, *hypeTrain); err != nil {
		return err
	}

	return nil
}

func parseMessage(msg *sqs.Message) (*models.ExpireHypeTrainEvent, error) {
	var snsMsg models.SNSMessage
	err := json.Unmarshal([]byte(*msg.Body), &snsMsg)
	if err != nil {
		return nil, err
	}

	var event models.ExpireHypeTrainEvent
	err = json.Unmarshal([]byte(snsMsg.Message), &event)
	if err != nil {
		return nil, err
	}

	return &event, nil
}
