package sqs_test

import (
	"encoding/json"
	"testing"

	"code.justin.tv/commerce/gogogadget/pointers"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	sqs_worker "code.justin.tv/commerce/percy/internal/sqs"
	"code.justin.tv/commerce/percy/internal/sqs/models"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestBoostOpportunityStartWorker_Handle(t *testing.T) {
	Convey("given a boost opportunity starter event worker", t, func() {
		launchpad := internalfakes.FakeLaunchPad{}
		worker := sqs_worker.NewBoostOpportunityStartWorker(&launchpad)

		Convey("it returns an error", func() {
			Convey("when we have an invalid message", func() {
				invalidSNSJson := getSNSJson(`{`)
				invalidSQSMsg := &sqs.Message{
					Body: pointers.StringP(invalidSNSJson),
				}
				err := worker.Handle(invalidSQSMsg)
				So(err, ShouldNotBeNil)
			})

			Convey("when we have a valid message but empty channelID", func() {
				validSNSJson := getSNSJson(`{}`)
				validSQSMsg := &sqs.Message{
					Body: pointers.StringP(validSNSJson),
				}
				err := worker.Handle(validSQSMsg)
				So(err, ShouldNotBeNil)
			})

			Convey("when handler returns error", func() {
				launchpad.ChannelIsAllowedReturns(true)
				launchpad.StartBoostOpportunityReturns(
					percy.BoostOpportunity{},
					errors.New("cannot start boost opportunity"),
				)
				validEventJson := getBoostOpportunityStartEventJson("some-channel")
				validSNSJson := getSNSJson(validEventJson)
				validSQSMsg := &sqs.Message{
					Body: pointers.StringP(validSNSJson),
				}

				err := worker.Handle(validSQSMsg)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("It returns nil", func() {
			validEventJson := getBoostOpportunityStartEventJson("some-channel")
			validSNSJson := getSNSJson(validEventJson)
			validSQSMsg := &sqs.Message{
				Body: pointers.StringP(validSNSJson),
			}
			launchpad.StartBoostOpportunityReturns(
				percy.BoostOpportunity{},
				nil,
			)

			Convey("when channel is not in allowlist", func() {
				launchpad.ChannelIsAllowedReturns(false)
			})
			Convey("channel is in allowlist and handler succeeds", func() {
				launchpad.ChannelIsAllowedReturns(true)
			})

			err := worker.Handle(validSQSMsg)
			So(err, ShouldBeNil)
		})
	})
}

func getSNSJson(message string) string {
	validEventSNSMsg := models.SNSMessage{
		Message: message,
	}
	snsJson, err := json.Marshal(validEventSNSMsg)
	So(err, ShouldBeNil)
	return string(snsJson)
}

func getBoostOpportunityStartEventJson(channelID string) string {
	validEvent := models.BoostOpportunityStartEvent{
		ChannelID: channelID,
	}
	validEventJson, err := json.Marshal(validEvent)
	So(err, ShouldBeNil)
	return string(validEventJson)
}
