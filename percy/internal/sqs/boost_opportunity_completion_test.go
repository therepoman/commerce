package sqs_test

import (
	"encoding/json"
	"testing"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	sqs_worker "code.justin.tv/commerce/percy/internal/sqs"
	"code.justin.tv/commerce/percy/internal/sqs/models"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestBoostOpportunityCompletionWorker_Handle(t *testing.T) {
	Convey("given a boost opportunity completion event worker", t, func() {
		launchpad := internalfakes.FakeLaunchPad{}
		worker := sqs_worker.NewBoostOpportunityCompletionWorker(&launchpad)

		Convey("it returns an error", func() {
			Convey("when we have an invalid message", func() {
				invalidSNSJson := getSNSJson(`{`)
				invalidSQSMsg := &sqs.Message{
					Body: pointers.StringP(invalidSNSJson),
				}
				err := worker.Handle(invalidSQSMsg)
				So(err, ShouldNotBeNil)
			})

			Convey("when we have a valid message but empty channelID", func() {
				validSNSJson := getSNSJson(`{}`)
				validSQSMsg := &sqs.Message{
					Body: pointers.StringP(validSNSJson),
				}
				err := worker.Handle(validSQSMsg)
				So(err, ShouldNotBeNil)
			})

			Convey("when we have a valid message but empty boost opportunity id", func() {
				validSNSJson := getSNSJson(`{"channel_id":"some-channel"}`)
				validSQSMsg := &sqs.Message{
					Body: pointers.StringP(validSNSJson),
				}
				err := worker.Handle(validSQSMsg)
				So(err, ShouldNotBeNil)
			})

			Convey("when handler returns error", func() {
				launchpad.CompleteBoostOpportunityReturns(
					errors.New("cannot complete boost opportunity"),
				)
				validEventJson := getBoostOpportunityCompletionEventJson("some-channel", "some-boost-opportunity-id")
				validSNSJson := getSNSJson(validEventJson)
				validSQSMsg := &sqs.Message{
					Body: pointers.StringP(validSNSJson),
				}
				err := worker.Handle(validSQSMsg)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("It returns nil", func() {
			Convey("when event is the right type", func() {
				validEventJson := getBoostOpportunityCompletionEventJson("some-channel", "some-boost-opportunity-id")
				validSNSJson := getSNSJson(validEventJson)
				validSQSMsg := &sqs.Message{
					Body: pointers.StringP(validSNSJson),
				}
				Convey("and handler succeeds", func() {
					launchpad.CompleteBoostOpportunityReturns(nil)
					err := worker.Handle(validSQSMsg)
					So(err, ShouldBeNil)
				})
			})
		})
	})
}

func getBoostOpportunityCompletionEventJson(channelID, boostOpportunityID string) string {
	validEvent := models.BoostOpportunityCompletionEvent{
		ChannelID:          channelID,
		BoostOpportunityID: boostOpportunityID,
	}
	validEventJson, err := json.Marshal(validEvent)
	So(err, ShouldBeNil)
	return string(validEventJson)
}
