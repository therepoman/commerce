package sqs

import (
	"context"
	"encoding/json"

	percy "code.justin.tv/commerce/percy/internal"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/pkg/errors"
)

type participationWorker struct {
	engine percy.Engine
}

func NewParticipationWorker(engine percy.Engine) *participationWorker {
	return &participationWorker{
		engine: engine,
	}
}

func (worker *participationWorker) Handle(msg *sqs.Message) error {
	participation, err := parseParticipation(msg)
	if err != nil {
		return errors.New("failed to parse participation event")
	}

	return worker.engine.RecordParticipation(context.TODO(), participation)
}

func parseParticipation(msg *sqs.Message) (percy.Participation, error) {
	var participation percy.Participation
	err := json.Unmarshal([]byte(*msg.Body), &participation)
	if err != nil {
		return percy.Participation{}, errors.Wrap(err, "failed to unmarshal participation message")
	}
	return participation, nil
}
