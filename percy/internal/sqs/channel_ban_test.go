package sqs

import (
	"encoding/json"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/sqs/models"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestChannelBanWorker(t *testing.T) {
	Convey("Given a channel ban event worker", t, func() {
		updater := &internalfakes.FakeConductorUpdater{}
		worker := NewChannelBanWorker(updater)

		Convey("given a channel ban event", func() {
			event := channelBanMessage()

			Convey("calls updater to moderate user", func() {
				err := worker.Handle(event)
				So(err, ShouldBeNil)
				So(updater.ModerateCallCount(), ShouldEqual, 1)
			})

			Convey("returns an error when updater fails", func() {
				updater.ModerateReturns(errors.New("can't moderate"))
				err := worker.Handle(event)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("given a channel unban event", func() {
			event := channelUnbanMessage()

			Convey("calls updater to unmoderate user", func() {
				err := worker.Handle(event)
				So(err, ShouldBeNil)
				So(updater.UnmoderateCallCount(), ShouldEqual, 1)
			})

			Convey("returns an error when updater fails", func() {
				updater.UnmoderateReturns(errors.New("can't moderate"))
				err := worker.Handle(event)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("given a channel timeout event", func() {
			event := channelTimeoutMessage()

			Convey("should not call updater", func() {
				err := worker.Handle(event)
				So(err, ShouldBeNil)
				So(updater.ModerateCallCount(), ShouldEqual, 0)
				So(updater.UnmoderateCallCount(), ShouldEqual, 0)
			})
		})
	})
}

func channelBanMessage() *sqs.Message {
	event := channelBanEvent{
		ChannelID: uuid.New().String(),
		UserID:    uuid.New().String(),
	}
	eventJson, _ := json.Marshal(event)

	snsMessage := models.SNSMessage{
		MessageAttributes: map[string]models.SNSMessageAttribute{
			"event": {
				Type:  "String",
				Value: "chat_ban",
			},
		},
		Message: string(eventJson),
	}

	snsJson, _ := json.Marshal(snsMessage)
	return &sqs.Message{
		Body: pointers.StringP(string(snsJson)),
	}
}

func channelUnbanMessage() *sqs.Message {
	event := channelBanEvent{
		ChannelID: uuid.New().String(),
		UserID:    uuid.New().String(),
	}
	eventJson, _ := json.Marshal(event)

	snsMessage := models.SNSMessage{
		MessageAttributes: map[string]models.SNSMessageAttribute{
			"event": {
				Type:  "String",
				Value: "chat_unban",
			},
		},
		Message: string(eventJson),
	}

	snsJson, _ := json.Marshal(snsMessage)
	return &sqs.Message{
		Body: pointers.StringP(string(snsJson)),
	}
}

func channelTimeoutMessage() *sqs.Message {
	event := channelBanEvent{
		ChannelID: uuid.New().String(),
		UserID:    uuid.New().String(),
		ExpiresAt: time.Now(),
	}
	eventJson, _ := json.Marshal(event)

	snsMessage := models.SNSMessage{
		MessageAttributes: map[string]models.SNSMessageAttribute{
			"event": {
				Type:  "String",
				Value: "chat_ban",
			},
		},
		Message: string(eventJson),
	}

	snsJson, _ := json.Marshal(snsMessage)
	return &sqs.Message{
		Body: pointers.StringP(string(snsJson)),
	}
}
