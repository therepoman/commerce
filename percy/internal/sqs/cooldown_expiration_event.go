package sqs

import (
	"context"
	"encoding/json"
	"time"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/sqs/models"
	sns_models "code.justin.tv/commerce/percy/models"
	"github.com/aws/aws-sdk-go/service/sqs"
)

const (
	cooldownExpirationWorkerTimeout = 3 * time.Second
)

type cooldownExpirationEventWorker struct {
	engine    percy.Engine
	publisher percy.Publisher
}

func NewCooldownExpirationEventWorker(engine percy.Engine, publisher percy.Publisher) *cooldownExpirationEventWorker {
	return &cooldownExpirationEventWorker{
		engine:    engine,
		publisher: publisher,
	}
}

func (w *cooldownExpirationEventWorker) Handle(msg *sqs.Message) error {
	ctx, cancel := context.WithTimeout(context.Background(), cooldownExpirationWorkerTimeout)
	defer cancel()

	event, err := parseActivityFeedEvent(msg)
	if err != nil {
		return err
	}

	if event.EventType != string(sns_models.HypeTrainCoolDownExpired) {
		return nil
	}

	hypeTrain, err := w.engine.GetMostRecentHypeTrain(ctx, event.ChannelID)
	if err != nil {
		return err
	}

	if hypeTrain.ID != event.HypeTrainID {
		logrus.Warn("Destiny message had a different hype train ID than the most recent hype train")
		return nil
	}

	return w.publisher.CooldownExpired(ctx, event.ChannelID)
}

func parseActivityFeedEvent(msg *sqs.Message) (*sns_models.ActivityFeedEvent, error) {
	var snsMsg models.SNSMessage
	err := json.Unmarshal([]byte(*msg.Body), &snsMsg)
	if err != nil {
		return nil, err
	}

	var event sns_models.ActivityFeedEvent
	err = json.Unmarshal([]byte(snsMsg.Message), &event)
	if err != nil {
		return nil, err
	}

	return &event, nil
}
