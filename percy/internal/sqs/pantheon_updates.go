package sqs

import (
	"context"
	"encoding/json"
	"time"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/pantheon"
	"code.justin.tv/commerce/percy/internal/sqs/models"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

const (
	pantheonUpdateWorkerTimeout = 3 * time.Second
)

type leaderboardUpdateNotificationIdentifier struct {
	Domain              string    `json:"domain"`
	GroupingKey         string    `json:"grouping_key"`
	TimeAggregationUnit string    `json:"time_aggregation_unit"`
	TimeBucket          time.Time `json:"time_bucket"`
}

type leaderboardUpdateNotification struct {
	Identifier   leaderboardUpdateNotificationIdentifier `json:"identifier"`
	Top          []*pantheonrpc.LeaderboardEntry         `json:"top"`
	EntryContext *pantheonrpc.EntryContext               `json:"entry_context"`
}

type pantheonUpdateWorker struct {
	conductorUpdater percy.ConductorUpdater
}

func NewPantheonUpdateWorker(updater percy.ConductorUpdater) *pantheonUpdateWorker {
	return &pantheonUpdateWorker{
		conductorUpdater: updater,
	}
}

func (worker *pantheonUpdateWorker) Handle(msg *sqs.Message) error {
	ctx, cancel := context.WithTimeout(context.Background(), pantheonUpdateWorkerTimeout)
	defer cancel()

	notification, err := parseNotification(msg)
	if err != nil {
		return errors.Wrap(err, "failed to parse pantheon notification message")
	}

	if notification.Identifier.Domain != pantheon.Domain || len(notification.Top) == 0 {
		return nil
	}

	leaderboardID, err := pantheon.ParseLeaderboardID(notification.Identifier.GroupingKey)
	if err != nil {
		return errors.Wrap(err, "failed to parse leaderboard ID from pantheon notification")
	}
	userID := notification.Top[0].EntryKey

	err = worker.conductorUpdater.Update(ctx, leaderboardID, userID)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"channelID":   leaderboardID.ChannelID,
			"hypeTrainID": leaderboardID.HypeTrainID,
			"source":      leaderboardID.Source,
			"userID":      userID,
		}).Error("Failed to handle Pantheon leaderboard update")
		return err
	}

	return nil
}

func parseNotification(msg *sqs.Message) (leaderboardUpdateNotification, error) {
	var snsMsg models.SNSMessage
	var notification leaderboardUpdateNotification

	err := json.Unmarshal([]byte(*msg.Body), &snsMsg)
	if err != nil {
		return notification, err
	}

	err = json.Unmarshal([]byte(snsMsg.Message), &notification)
	if err != nil {
		return notification, err
	}

	return notification, nil
}
