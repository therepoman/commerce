package percy

import (
	"context"
	"errors"
	"time"
)

type (
	creatorCategory string
)

const (
	PartnerCategory   = creatorCategory("Partner")
	AffiliateCategory = creatorCategory("Affiliate")
)

type CreatorAnniversary struct {
	Date            time.Time
	CreatorCategory creatorCategory
}

type CreatorAnniversariesNotificationPublishReq struct {
	Date string `json:"time"`
}

type CreatorAnniversaryWithYears struct {
	Date                    time.Time
	ChannelId               string
	IsCreatorAnniversary    bool
	CreatorAnniversaryYears int
}

//go:generate counterfeiter . CreatorAnniversariesDB

type CreatorAnniversariesDB interface {
	GetCreatorAnniversaries() (map[string]CreatorAnniversary, error)
	IsCreatorAnniversariesExperimentEnabled(t time.Time) bool
}

//go:generate counterfeiter . CreatorAnniversariesNotificationDB

type CreatorAnniversariesNotificationDB interface {
	PutCreatorAnniversaryNotification(ctx context.Context, channelID string, anniversaryDate time.Time, notificationTraceId string, campaignType string) error
	ChannelHasSentAnniversaryNotification(ctx context.Context, channelID string, campaignType string) (bool, error)
}

func (e *creatorAnniversariesEngine) GetCreatorAnniversariesByDate(month int, day int) (map[string]CreatorAnniversary, error) {
	anniversaries, err := e.CreatorAnniversariesDB.GetCreatorAnniversaries()
	if err != nil {
		return nil, err
	}

	filteredAnniversaries := filterCreatorAnniversariesByDate(anniversaries, month, day)
	return filteredAnniversaries, nil
}

func (e *creatorAnniversariesEngine) IsCreatorAnniversariesExperimentEnabled(t time.Time) bool {
	return e.CreatorAnniversariesDB.IsCreatorAnniversariesExperimentEnabled(t)
}

func (e *creatorAnniversariesEngine) PutCreatorAnniversaryNotification(ctx context.Context, channelID string, anniversaryDate time.Time, notificationTraceId string, campaignType string) error {
	return e.NotificationIdempotency.PutCreatorAnniversaryNotification(ctx, channelID, anniversaryDate, notificationTraceId, campaignType)
}

func (e *creatorAnniversariesEngine) ChannelHasSentAnniversaryNotification(ctx context.Context, channelID string, campaignType string) (bool, error) {
	return e.NotificationIdempotency.ChannelHasSentAnniversaryNotification(ctx, channelID, campaignType)
}

func filterCreatorAnniversariesByDate(anniversaries map[string]CreatorAnniversary, month int, day int) map[string]CreatorAnniversary {
	filteredAnniversaries := make(map[string]CreatorAnniversary)
	for channelId, anniversary := range anniversaries {
		if int(anniversary.Date.Month()) == month && anniversary.Date.Day() == day {
			filteredAnniversaries[channelId] = anniversary
		}
	}
	return filteredAnniversaries
}

// GetCreatorAnniversariesByChannelIds takes a list of channelIds and returns a list of CreatorAnniversaryWithYears
func (e *creatorAnniversariesEngine) GetCreatorAnniversariesByChannelIds(channelIds []string) ([]CreatorAnniversaryWithYears, error) {
	var anniversariesWithYears []CreatorAnniversaryWithYears
	var filteredInput []string
	seen := make(map[string]bool)
	currentServerTime := time.Now().UTC()

	anniversaries, err := e.CreatorAnniversariesDB.GetCreatorAnniversaries()
	if err != nil {
		return nil, errors.New("error on calling CreatorAnniversariesDB.GetCreatorAnniversaries()")
	}

	if len(anniversaries) == 0 {
		return nil, nil
	}

	// Handle duplicates by filtering input
	for _, channelId := range channelIds {
		seen[channelId] = true
	}
	for channelId := range seen {
		filteredInput = append(filteredInput, channelId)
	}

	for _, channelId := range filteredInput {
		hasCreatorAnniversary := false
		creatorAnniversaryYears := 0
		anniversaryTime := time.Time{}
		anniversary, ok := anniversaries[channelId]
		if ok {
			anniversaryTime = anniversary.Date

			// Check if channelId has anniversary and calculate if yes
			isCreatorAnniversary, err := IsCreatorAnniversary(currentServerTime, anniversaryTime)
			if err != nil {
				return nil, err
			}
			if isCreatorAnniversary {
				creatorAnniversaryYears = CalculateAnniversaryYears(currentServerTime, anniversaryTime)
				if creatorAnniversaryYears >= 1 {
					hasCreatorAnniversary = true
				}
			}
		}

		// Create CreatorAnniversaryWithYears struct of channelId
		creatorAnniversary := CreatorAnniversaryWithYears{
			anniversaryTime,
			channelId,
			hasCreatorAnniversary,
			creatorAnniversaryYears,
		}

		anniversariesWithYears = append(anniversariesWithYears, creatorAnniversary)
	}
	return anniversariesWithYears, nil
}

// IsCreatorAnniversary checks if creator channel is currently in respective 48hr anniversary window
func IsCreatorAnniversary(currentTime time.Time, anniversaryTime time.Time) (bool, error) {
	yearsDifference := currentTime.Year() - anniversaryTime.Year()
	anniversaryTime = anniversaryTime.AddDate(yearsDifference, 0, 0)

	minus24Hours, err := time.ParseDuration("-24h")
	if err != nil {
		return false, errors.New("unable to parse duration -24h")
	}
	add24Hours, err := time.ParseDuration("24h")
	if err != nil {
		return false, errors.New("unable to parse duration 24h")
	}
	firstHalfAnniversaryWindow := anniversaryTime.Add(minus24Hours)
	secondHalfAnniversaryWindow := anniversaryTime.Add(add24Hours)
	if (currentTime.After(firstHalfAnniversaryWindow)) && (currentTime.Before(secondHalfAnniversaryWindow)) {
		return true, nil
	}
	return false, nil
}

// CalculateAnniversaryYears calculates the number of years of a creator anniversary
func CalculateAnniversaryYears(currentTime time.Time, anniversaryTime time.Time) int {
	years := currentTime.Year() - anniversaryTime.Year()
	return years
}
