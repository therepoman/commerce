package percy

import (
	"context"
	"sort"

	"code.justin.tv/commerce/logrus"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
)

const conductorModeratedMetric = "conductor-moderated"

type CleanConductorsInput struct {
	CleanBitsConductorID bool
	CleanSubsConductorID bool
}

//go:generate counterfeiter . ConductorsDB

type ConductorsDB interface {
	GetConductors(ctx context.Context, channelID string) ([]Conductor, error)
	SetConductors(ctx context.Context, channelID string, conductors []Conductor) error
	GetConductorChannelsForBitsConductorID(ctx context.Context, userID, cursor string) ([]string, string, error)
	GetConductorChannelsForSubsConductorID(ctx context.Context, userID, cursor string) ([]string, string, error)
	DeleteConductorsRecord(ctx context.Context, channelID string) error
	CleanConductorsRecords(ctx context.Context, input map[string]CleanConductorsInput) error
}

//go:generate counterfeiter . ConductorUpdater

type ConductorUpdater interface {
	// Update takes the new leaderboard leader and updates the conductor details for the corresponding hype train.
	Update(ctx context.Context, leaderboardID LeaderboardID, userID string) error

	// Moderate disabllows the user from becoming a hype train conductor in the given channe.
	Moderate(ctx context.Context, channelID, userID string) error

	// Unmoderate allows the user to become a hype train conductor in the given channel.
	Unmoderate(ctx context.Context, channelID, userID string) error
}

//go:generate counterfeiter . EventbusUpdatePublisher

type EventbusUpdatePublisher interface {
	HypeTrainUpdate(ctx context.Context, hypeTrain HypeTrain, lastParticipation Participation) error
}

type conductorUpdater struct {
	hypeTrainDB       HypeTrainDB
	participantDB     ParticipantDB
	leaderboard       Leaderboard
	pubsubPublisher   Publisher
	eventbusPublisher EventbusUpdatePublisher
	snsPublisher      SNSPublisher
	participationDB   ParticipationDB
	statter           statsd.Statter
}

func NewConductorUpdater(hypeTrainDB HypeTrainDB,
	participantDB ParticipantDB,
	leaderboard Leaderboard,
	pubsubPublisher Publisher,
	eventbusPublisher EventbusUpdatePublisher,
	snsPublisher SNSPublisher,
	participationDB ParticipationDB,
	statter statsd.Statter) *conductorUpdater {
	return &conductorUpdater{
		hypeTrainDB:       hypeTrainDB,
		participantDB:     participantDB,
		leaderboard:       leaderboard,
		pubsubPublisher:   pubsubPublisher,
		eventbusPublisher: eventbusPublisher,
		snsPublisher:      snsPublisher,
		participationDB:   participationDB,
		statter:           statter,
	}
}

func (updater *conductorUpdater) Update(ctx context.Context, leaderboardID LeaderboardID, userID string) error {
	hypeTrainID := leaderboardID.HypeTrainID
	if len(userID) == 0 {
		return errors.New("user ID is blank")
	}

	// check current hype train status for the channel
	mostRecentHypeTrain, err := updater.hypeTrainDB.GetMostRecentHypeTrain(ctx, leaderboardID.ChannelID)
	if err != nil {
		return errors.Wrap(err, "failed to retrieve the corresponding hype train")
	}

	if mostRecentHypeTrain == nil || mostRecentHypeTrain.ID != hypeTrainID {
		return nil
	}

	if !mostRecentHypeTrain.IsActive() {
		return nil
	}

	// check participation details for the new leaderboard leader
	participant, err := updater.participantDB.GetHypeTrainParticipant(ctx, hypeTrainID, userID)
	if err != nil {
		return errors.Wrap(err, "failed to retrieve participation details for hype train conductor")
	}

	if participant == nil {
		return errors.New("no participant was found for the given hype train and user ID")
	}

	if participant.User.IsAnonymous() {
		// Publish a Webhook event to notify the subscribers of this event
		go func() {
			updater.publishWebhookEvent(context.Background(), *mostRecentHypeTrain)
		}()

		return nil
	}

	// don't update unless the new conductor has more participation quantities than the existing conductor
	currentConductor, hasConductor := mostRecentHypeTrain.Conductors[leaderboardID.Source]
	if hasConductor &&
		participant.ParticipationTotals.ParticipationQuantitiesBySource(leaderboardID.Source) <
			currentConductor.Participations.ParticipationQuantitiesBySource(leaderboardID.Source) {

		// Publish a Webhook event to notify the subscribers of this event
		go func() {
			updater.publishWebhookEvent(context.Background(), *mostRecentHypeTrain)
		}()

		return nil
	}

	conductor := Conductor{
		User:           NewUser(userID),
		Participations: participant.ParticipationTotals,
		Source:         leaderboardID.Source,
	}

	updatedHypeTrain, err := updater.hypeTrainDB.UpdateConductor(ctx, leaderboardID.ChannelID, hypeTrainID, conductor)
	if err != nil {
		return errors.Wrap(err, "failed to update conductor in hype train DB")
	}

	err = updater.pubsubPublisher.ConductorUpdate(ctx, leaderboardID.ChannelID, conductor)
	if err != nil {
		return errors.Wrap(err, "failed to send conductor update via pubsub")
	}

	// Publish a Webhook event to notify the subscribers of this event
	go func() {
		updater.publishWebhookEvent(context.Background(), updatedHypeTrain)
	}()

	return nil
}

func (updater *conductorUpdater) Moderate(ctx context.Context, channelID, userID string) error {
	if len(channelID) == 0 {
		return errors.New("empty channel ID")
	}

	if len(userID) == 0 {
		return errors.New("empty user ID")
	}

	train, err := updater.hypeTrainDB.GetMostRecentHypeTrain(ctx, channelID)
	if err != nil {
		return errors.Wrap(err, "failed to get hype train")
	}

	// no-op if there is no hype train active
	if train == nil || !train.IsActive() {
		return nil
	}

	participant, err := updater.participantDB.GetHypeTrainParticipant(ctx, train.ID, userID)
	if err != nil {
		return errors.Wrap(err, "failed to get participation details for moderated user")
	}

	// no-op if user has not participated
	if participant == nil {
		return nil
	}

	for _, source := range participant.ParticipationTotals.Sources() {
		err := updater.leaderboard.Moderate(ctx, LeaderboardID{
			ChannelID:   channelID,
			HypeTrainID: train.ID,
			Source:      source,
		}, userID)
		if err != nil {
			return errors.Wrapf(err, "failed to moderate user from %s leaderboard", string(source))
		}
	}

	conductors := train.Conductors
	for source, conductor := range conductors {
		if conductor.User.ID() == userID {
			_ = updater.statter.Inc(conductorModeratedMetric, 1, 1.0)
			_, err := updater.hypeTrainDB.RemoveConductor(ctx, channelID, train.ID, userID, source)
			if err != nil {
				return errors.Wrap(err, "failed to remove moderated conductor")
			}
		}
	}

	return nil
}

func (updater *conductorUpdater) Unmoderate(ctx context.Context, channelID, userID string) error {
	if len(channelID) == 0 {
		return errors.New("empty channel ID")
	}

	if len(userID) == 0 {
		return errors.New("empty user ID")
	}

	train, err := updater.hypeTrainDB.GetMostRecentHypeTrain(ctx, channelID)
	if err != nil {
		return errors.Wrap(err, "failed to get hype train")
	}

	// no-op if there is no hype train active
	if train == nil || !train.IsActive() {
		return nil
	}

	participant, err := updater.participantDB.GetHypeTrainParticipant(ctx, train.ID, userID)
	if err != nil {
		return errors.Wrap(err, "failed to get participation details for moderated user")
	}

	// no-op if user has not participated
	if participant == nil {
		return nil
	}

	for _, source := range participant.ParticipationTotals.Sources() {
		err := updater.leaderboard.Unmoderate(ctx, LeaderboardID{
			ChannelID:   channelID,
			HypeTrainID: train.ID,
			Source:      source,
		}, userID)
		if err != nil {
			return errors.Wrapf(err, "failed to unmoderate user from %s leaderboard", string(source))
		}
	}

	return nil
}

func (updater *conductorUpdater) publishWebhookEvent(ctx context.Context, hypeTrain HypeTrain) {
	participations, err := updater.participationDB.GetChannelParticipations(ctx, hypeTrain.ChannelID, ParticipationQueryParams{
		StartTime: hypeTrain.StartedAt.Add(-1 * hypeTrain.Config.KickoffConfig.Duration),
		EndTime:   hypeTrain.ExpiresAt.Add(hypeTrain.Config.CooldownDuration),
	})
	if err != nil {
		logrus.WithError(err).Error("Failed to fetch participations for hype train webhook update publication")
		return
	}

	var lastParticipation Participation
	if len(participations) > 0 {
		sort.Slice(participations, func(i, j int) bool {
			return participations[i].Timestamp.After(participations[j].Timestamp)
		})
		lastParticipation = participations[0]
	}

	// EventBus --> Webhooks 2.0
	err = updater.eventbusPublisher.HypeTrainUpdate(ctx, hypeTrain, lastParticipation)
	if err != nil {
		logrus.WithError(err).Error("Failed to publish hype train update to eventbus")
	}

	// Event History Service (EHS) --> Webhooks 1.0 (WebSub)
	err = updater.snsPublisher.PublishWebhookEvent(context.Background(), hypeTrain, lastParticipation)
	if err != nil {
		logrus.WithError(err).Error("Failed to send progression to EHS")
	}
}
