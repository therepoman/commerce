package percy

import (
	"context"
)

//go:generate counterfeiter . ChannelStatusDB
type ChannelStatusDB interface {
	ChannelIsStreaming(ctx context.Context, channelID string) (bool, error)
	GetStreamData(ctx context.Context, channelID string) (StreamData, error)
}

type StreamData struct {
	ChannelID   string
	IsStreaming bool
	BroadcastID string
	ViewCount   int
}
