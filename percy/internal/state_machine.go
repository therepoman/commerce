package percy

import (
	"context"
)

//go:generate counterfeiter . StateMachine

type StateMachine interface {
	StartExecutionCelebrationPurchaseFulfillment(ctx context.Context, req CelebrationsPurchaseFulfillmentReq) error
	StartExecutionCelebrationPurchaseRevoke(ctx context.Context, req CelebrationsPurchaseRevocationReq) error
	StartExecutionPDMSUserDeletion(ctx context.Context, executionName string, req StartUserDeletionRequest) error
	StartExecutionBoostOpportunityContribution(ctx context.Context, contribution BoostOpportunityContribution) error
}
