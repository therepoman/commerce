package percy_test

import (
	"context"
	"testing"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetActiveBoostOpportunity(t *testing.T) {
	Convey("given a boost opportunity launchpad", t, func() {
		boostsDB := internalfakes.FakeBoostsDB{}
		launchpad := percy.NewPaidBoostLaunchPad(percy.LaunchPadConfig{
			BoostsDB: &boostsDB,
		})
		channelID := "12345"
		boostID := "abcde"

		Convey("it returns error when we fail to get latest boost opportunity from boost DB", func() {
			boostsDB.GetLatestBoostOpportunityReturns(nil, errors.New("some boostsDB error"))
			boostOpportunity, err := launchpad.GetActiveBoostOpportunity(context.Background(), channelID)
			So(boostOpportunity, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("it does not error", func() {
			Convey("and returns nil when", func() {
				Convey("opportunity does not exist", func() {
					boostsDB.GetLatestBoostOpportunityReturns(nil, nil)
				})
				Convey("latest opportunity is not active", func() {
					Convey("expiresAt is before now", func() {
						boostsDB.GetLatestBoostOpportunityReturns(&percy.BoostOpportunity{
							ChannelID: channelID,
							ID:        boostID,
							ExpiresAt: time.Now().Add(-1 * time.Second),
						}, nil)
					})
					Convey("expiresAt is after now but EndedAt is not nil", func() {
						someTime := time.Now()
						boostsDB.GetLatestBoostOpportunityReturns(&percy.BoostOpportunity{
							ChannelID: channelID,
							ID:        boostID,
							ExpiresAt: time.Now().Add(1 * time.Second),
							EndedAt:   &someTime,
						}, nil)
					})
				})
				boostOpportunity, err := launchpad.GetActiveBoostOpportunity(context.Background(), channelID)
				So(boostOpportunity, ShouldBeNil)
				So(err, ShouldBeNil)
			})
			Convey("latest opportunity is active", func() {
				boostsDB.GetLatestBoostOpportunityReturns(&percy.BoostOpportunity{
					ChannelID: channelID,
					ID:        boostID,
					ExpiresAt: time.Now().Add(1 * time.Second),
				}, nil)
				boostOpportunity, err := launchpad.GetActiveBoostOpportunity(context.Background(), channelID)
				So(err, ShouldBeNil)
				So(boostOpportunity, ShouldNotBeNil)
				So(boostOpportunity.ChannelID, ShouldEqual, channelID)
				So(boostOpportunity.ID, ShouldEqual, boostID)
			})
		})
		So(boostsDB.GetLatestBoostOpportunityCallCount(), ShouldEqual, 1)
	})
}

func TestScheduleBoostOpportunity(t *testing.T) {
	Convey("given a boost opportunity launchpad", t, func() {
		boostsDB := internalfakes.FakeBoostsDB{}
		channelStatusDB := internalfakes.FakeChannelStatusDB{}
		scheduler := internalfakes.FakeBoostOpportunityScheduler{}
		configsDB := internalfakes.FakePaidBoostConfigsDB{}

		launchpad := percy.NewPaidBoostLaunchPad(percy.LaunchPadConfig{
			BoostsDB:        &boostsDB,
			ChannelStatusDB: &channelStatusDB,
			Scheduler:       &scheduler,
			ConfigsDB:       &configsDB,
		})
		channelID := "12345"
		validStartTime := time.Now().Add(1 * time.Second)
		configsDB.GetBoostOpportunityConfigReturns(percy.DefaultBoostOpportunityConfig)

		getLatestBoostOpportunityExpectedCallCount := 0
		channelIsStreamingExepctedCallCount := 0
		scheduleBoostOpportunityStartCallCount := 0

		Convey("it returns error when", func() {
			Convey("we fail to get latest boost opportunity from boost DB", func() {
				boostsDB.GetLatestBoostOpportunityReturns(nil, errors.New("some boostsDB error"))
				getLatestBoostOpportunityExpectedCallCount = 1
			})
			Convey("we fail to get stream online status", func() {
				boostsDB.GetLatestBoostOpportunityReturns(nil, nil)
				getLatestBoostOpportunityExpectedCallCount = 1
				channelStatusDB.ChannelIsStreamingReturns(false, errors.New("some liveline error"))
				channelIsStreamingExepctedCallCount = 1
			})
			Convey("we fail to schedule boost opportunity kick off in Destiny", func() {
				boostsDB.GetLatestBoostOpportunityReturns(nil, nil)
				getLatestBoostOpportunityExpectedCallCount = 1
				channelStatusDB.ChannelIsStreamingReturns(true, nil)
				channelIsStreamingExepctedCallCount = 1
				scheduler.ScheduleBoostOpportunityStartReturns(errors.New("some scheduler error"))
				scheduleBoostOpportunityStartCallCount = 1
			})

			err := launchpad.ScheduleBoostOpportunity(context.Background(), channelID, validStartTime)
			So(err, ShouldNotBeNil)
		})

		Convey("it noops when", func() {
			Convey("latest opportunity is still active", func() {
				ongoingBoostOpportunity := tests.ValidBoostOpportunity()
				boostsDB.GetLatestBoostOpportunityReturns(&ongoingBoostOpportunity, nil)
				getLatestBoostOpportunityExpectedCallCount = 1
			})

			Convey("latest opportunity is within cool down period", func() {
				completeBoostOpportunity := tests.ValidCompletedBoostOpportunity()
				completeBoostOpportunity.ExpiresAt = time.Now().Add(-1 * percy.DefaultBoostOpportunityConfig.Cooldown).Add(1 * time.Second)
				boostsDB.GetLatestBoostOpportunityReturns(&completeBoostOpportunity, nil)
				getLatestBoostOpportunityExpectedCallCount = 1
			})

			Convey("stream is offline", func() {
				boostsDB.GetLatestBoostOpportunityReturns(nil, nil)
				getLatestBoostOpportunityExpectedCallCount = 1
				channelStatusDB.ChannelIsStreamingReturns(false, nil)
				channelIsStreamingExepctedCallCount = 1
			})

			err := launchpad.ScheduleBoostOpportunity(context.Background(), channelID, validStartTime)
			So(err, ShouldBeNil)
			So(scheduler.ScheduleBoostOpportunityStartCallCount(), ShouldEqual, 0)
		})

		Convey("it schedules a boost opportunity when", func() {
			Convey("latest boost opportunity is on the previous day and channel is not in cooldown time", func() {
				completeBoostOpportunity := tests.ValidCompletedBoostOpportunity()
				completeBoostOpportunity.ExpiresAt = time.Now().Add(-24 * time.Hour)
				boostsDB.GetLatestBoostOpportunityReturns(&completeBoostOpportunity, nil)
				getLatestBoostOpportunityExpectedCallCount = 1
			})

			Convey("channel did not have a previous boost opportunity", func() {
				boostsDB.GetLatestBoostOpportunityReturns(nil, nil)
				getLatestBoostOpportunityExpectedCallCount = 1
			})

			// stream is online
			channelStatusDB.ChannelIsStreamingReturns(true, nil)
			channelIsStreamingExepctedCallCount = 1

			// scheduled successfully
			scheduler.ScheduleBoostOpportunityStartReturns(nil)
			scheduleBoostOpportunityStartCallCount = 1

			err := launchpad.ScheduleBoostOpportunity(context.Background(), channelID, validStartTime)
			So(err, ShouldBeNil)
			So(scheduler.ScheduleBoostOpportunityStartCallCount(), ShouldEqual, 1)

			_, _, scheduledAt := scheduler.ScheduleBoostOpportunityStartArgsForCall(0)

			// Scheduled kickoff should be somewhere between kickoffDelayMin and kickoffDelayMax from now
			So(scheduledAt.Before(time.Now().Add(percy.DefaultBoostOpportunityConfig.KickoffDelayMax)), ShouldBeTrue)
			So(scheduledAt.After(time.Now().Add(percy.DefaultBoostOpportunityConfig.KickoffDelayMin-(1*time.Second))), ShouldBeTrue)
		})

		So(boostsDB.GetLatestBoostOpportunityCallCount(), ShouldEqual, getLatestBoostOpportunityExpectedCallCount)
		So(channelStatusDB.ChannelIsStreamingCallCount(), ShouldEqual, channelIsStreamingExepctedCallCount)
		So(scheduler.ScheduleBoostOpportunityStartCallCount(), ShouldEqual, scheduleBoostOpportunityStartCallCount)
	})
}

func TestStartBoostOpportunity(t *testing.T) {
	Convey("given a boost opportunity launchpad", t, func() {
		boostsDB := internalfakes.FakeBoostsDB{}
		channelStatusDB := internalfakes.FakeChannelStatusDB{}
		scheduler := internalfakes.FakeBoostOpportunityScheduler{}
		publisher := internalfakes.FakeBoostsPublisher{}
		configsDB := internalfakes.FakePaidBoostConfigsDB{}

		launchpad := percy.NewPaidBoostLaunchPad(percy.LaunchPadConfig{
			BoostsDB:        &boostsDB,
			ChannelStatusDB: &channelStatusDB,
			Scheduler:       &scheduler,
			Publisher:       &publisher,
			ConfigsDB:       &configsDB,
		})
		channelID := "12345"
		configsDB.GetBoostOpportunityConfigReturns(percy.DefaultBoostOpportunityConfig)

		getLatestBoostOpportunityExpectedCallCount := 0
		channelIsStreamingExepctedCallCount := 0
		scheduleBoostOpportunityCompletionCallCount := 0
		createBoostOpportunityExpectedCallCount := 0
		publishBoostOpportunityStartExpectedCallCount := 0

		Convey("it returns an error when", func() {
			Convey("we fail to get latest boost opportunity from boost DB", func() {
				boostsDB.GetLatestBoostOpportunityReturns(nil, errors.New("some boostsDB error"))
				getLatestBoostOpportunityExpectedCallCount = 1
			})
			Convey("we fail to get stream online status", func() {
				boostsDB.GetLatestBoostOpportunityReturns(nil, nil)
				getLatestBoostOpportunityExpectedCallCount = 1
				channelStatusDB.ChannelIsStreamingReturns(false, errors.New("some liveline error"))
				channelIsStreamingExepctedCallCount = 1
			})
			Convey("we fail to schedule boost opportunity completion", func() {
				boostsDB.GetLatestBoostOpportunityReturns(nil, nil)
				getLatestBoostOpportunityExpectedCallCount = 1
				channelStatusDB.ChannelIsStreamingReturns(true, nil)
				channelIsStreamingExepctedCallCount = 1
				scheduler.ScheduleBoostOpportunityCompletionReturns(errors.New("some scheduler error"))
				scheduleBoostOpportunityCompletionCallCount = 1
			})
			Convey("we fail to create boost opportunity in BoostsDB", func() {
				boostsDB.GetLatestBoostOpportunityReturns(nil, nil)
				getLatestBoostOpportunityExpectedCallCount = 1
				channelStatusDB.ChannelIsStreamingReturns(true, nil)
				channelIsStreamingExepctedCallCount = 1
				scheduler.ScheduleBoostOpportunityCompletionReturns(nil)
				scheduleBoostOpportunityCompletionCallCount = 1
				boostsDB.CreateBoostOpportunityReturns(errors.New("some boostsDB error"))
				createBoostOpportunityExpectedCallCount = 1
			})
			Convey("we fail to publish to PubSub", func() {
				boostsDB.GetLatestBoostOpportunityReturns(nil, nil)
				getLatestBoostOpportunityExpectedCallCount = 1
				channelStatusDB.ChannelIsStreamingReturns(true, nil)
				channelIsStreamingExepctedCallCount = 1
				scheduler.ScheduleBoostOpportunityCompletionReturns(nil)
				scheduleBoostOpportunityCompletionCallCount = 1
				boostsDB.CreateBoostOpportunityReturns(nil)
				createBoostOpportunityExpectedCallCount = 1
				publisher.PublishBoostOpportunityStartReturns(errors.New("some pubsub error"))
				publishBoostOpportunityStartExpectedCallCount = 1
			})
			emptyBoostOpportunity := percy.BoostOpportunity{}
			resp, err := launchpad.StartBoostOpportunity(context.Background(), channelID)
			So(err, ShouldNotBeNil)
			So(resp, ShouldResemble, emptyBoostOpportunity)
		})

		Convey("It noops when", func() {
			Convey("latest opportunity is within cool down period, this includes active opportunity", func() {
				completeBoostOpportunity := tests.ValidCompletedBoostOpportunity()
				completeBoostOpportunity.ExpiresAt = time.Now().Add(-1 * percy.DefaultBoostOpportunityConfig.Cooldown).Add(1 * time.Second)
				boostsDB.GetLatestBoostOpportunityReturns(&completeBoostOpportunity, nil)
				getLatestBoostOpportunityExpectedCallCount = 1
			})
			Convey("stream is not online", func() {
				boostsDB.GetLatestBoostOpportunityReturns(nil, nil)
				getLatestBoostOpportunityExpectedCallCount = 1
				channelStatusDB.ChannelIsStreamingReturns(false, nil)
				channelIsStreamingExepctedCallCount = 1
			})
			emptyBoostOpportunity := percy.BoostOpportunity{}
			resp, err := launchpad.StartBoostOpportunity(context.Background(), channelID)
			So(err, ShouldBeNil)
			So(resp, ShouldResemble, emptyBoostOpportunity)
		})

		Convey("It returns populated boost opportunity and nil error when", func() {
			Convey("latest boost opportunity is not on cooldown", func() {
				completeBoostOpportunity := tests.ValidCompletedBoostOpportunity()
				completeBoostOpportunity.ExpiresAt = time.Now().Add(-1 * percy.DefaultBoostOpportunityConfig.Cooldown)
				boostsDB.GetLatestBoostOpportunityReturns(&completeBoostOpportunity, nil)
				getLatestBoostOpportunityExpectedCallCount = 1
			})

			Convey("channel did not have a previous boost opportunity", func() {
				boostsDB.GetLatestBoostOpportunityReturns(nil, nil)
				getLatestBoostOpportunityExpectedCallCount = 1
			})

			// Channel is online
			channelStatusDB.ChannelIsStreamingReturns(true, nil)
			channelIsStreamingExepctedCallCount = 1

			// Scheduled completion event successfully
			scheduler.ScheduleBoostOpportunityStartReturns(nil)
			scheduleBoostOpportunityCompletionCallCount = 1

			// New Boost Opportunity successfully created
			boostsDB.CreateBoostOpportunityReturns(nil)
			createBoostOpportunityExpectedCallCount = 1

			// Published successfully
			publisher.PublishBoostOpportunityStartReturns(nil)
			publishBoostOpportunityStartExpectedCallCount = 1

			emptyBoostOpportunity := percy.BoostOpportunity{}
			resp, err := launchpad.StartBoostOpportunity(context.Background(), channelID)
			So(err, ShouldBeNil)
			So(resp, ShouldNotResemble, emptyBoostOpportunity)
		})

		So(boostsDB.GetLatestBoostOpportunityCallCount(), ShouldEqual, getLatestBoostOpportunityExpectedCallCount)
		So(channelStatusDB.ChannelIsStreamingCallCount(), ShouldEqual, channelIsStreamingExepctedCallCount)
		So(scheduler.ScheduleBoostOpportunityCompletionCallCount(), ShouldEqual, scheduleBoostOpportunityCompletionCallCount)
		So(boostsDB.CreateBoostOpportunityCallCount(), ShouldEqual, createBoostOpportunityExpectedCallCount)
		So(publisher.PublishBoostOpportunityStartCallCount(), ShouldEqual, publishBoostOpportunityStartExpectedCallCount)
	})
}

func TestCompleteBoostOpportunity(t *testing.T) {
	Convey("given a boost opportunity launchpad", t, func() {
		boostsDB := internalfakes.FakeBoostsDB{}
		booster := internalfakes.FakeBoostsLauncher{}
		publisher := internalfakes.FakeBoostsPublisher{}

		launchpad := percy.NewPaidBoostLaunchPad(percy.LaunchPadConfig{
			BoostsDB:       &boostsDB,
			BoostsLauncher: &booster,
			Publisher:      &publisher,
		})
		channelID := "12345"
		boostOpportunityID := "some-boost-opportunity-id"

		getLatestBoostOpportunityExpectedCallCount := 0
		launchBoostExpectedCallCount := 0
		publishBoostOpportunityCompletionExpectedCallCount := 0
		endLatestBoostOpportunityExpectedCallCount := 0

		boostID := "some-boost-id"
		checkBoostIdArgInEndLatestBoostOpportunity := false

		Convey("it returns error when", func() {
			Convey("we fail to query boost opportunity from boost DB", func() {
				boostsDB.GetLatestBoostOpportunityReturns(nil, errors.New("some boostsDB error"))
				getLatestBoostOpportunityExpectedCallCount = 1
			})
			Convey("if boost opportunity has contribution", func() {
				boostsDB.GetLatestBoostOpportunityReturns(&percy.BoostOpportunity{
					ChannelID:        channelID,
					ID:               boostOpportunityID,
					UnitsContributed: 1,
				}, nil)
				getLatestBoostOpportunityExpectedCallCount = 1

				Convey("we fail to create order in booster", func() {
					booster.LaunchBoostReturns(boostID, errors.New("some booster service error"))
					launchBoostExpectedCallCount = 1
				})
				Convey("we fail to update boost opportunity to mark it as complete", func() {
					booster.LaunchBoostReturns(boostID, nil)
					launchBoostExpectedCallCount = 1
					boostsDB.EndLatestBoostOpportunityReturns(errors.New("some boostsDB error"))
					endLatestBoostOpportunityExpectedCallCount = 1
					checkBoostIdArgInEndLatestBoostOpportunity = true
				})
			})
			Convey("if boost opportunity has no contribution", func() {
				boostsDB.GetLatestBoostOpportunityReturns(&percy.BoostOpportunity{
					ChannelID:        channelID,
					ID:               boostOpportunityID,
					UnitsContributed: 0,
				}, nil)
				getLatestBoostOpportunityExpectedCallCount = 1
				Convey("we fail to update boost opportunity to mark it as complete", func() {
					boostsDB.EndLatestBoostOpportunityReturns(errors.New("some boostsDB error"))
					endLatestBoostOpportunityExpectedCallCount = 1
				})
			})
			err := launchpad.CompleteBoostOpportunity(context.Background(), channelID, boostOpportunityID)
			So(err, ShouldNotBeNil)
		})

		Convey("it does not error when", func() {
			Convey("boost opportunity does not exist", func() {
				boostsDB.GetLatestBoostOpportunityReturns(nil, nil)
				getLatestBoostOpportunityExpectedCallCount = 1
			})
			Convey("opportunity ID does not match", func() {
				boostsDB.GetLatestBoostOpportunityReturns(&percy.BoostOpportunity{
					ChannelID: channelID,
					ID:        "not-the-opportunity-id",
				}, nil)
				getLatestBoostOpportunityExpectedCallCount = 1
			})
			Convey("boost opportunity ended already", func() {
				someTime := time.Now()
				boostsDB.GetLatestBoostOpportunityReturns(&percy.BoostOpportunity{
					ChannelID: channelID,
					ID:        boostOpportunityID,
					EndedAt:   &someTime,
				}, nil)
				getLatestBoostOpportunityExpectedCallCount = 1
			})
			Convey("if boost opportunity has contribution", func() {
				boostsDB.GetLatestBoostOpportunityReturns(&percy.BoostOpportunity{
					ChannelID:        channelID,
					ID:               boostOpportunityID,
					UnitsContributed: 1,
				}, nil)
				getLatestBoostOpportunityExpectedCallCount = 1

				checkBoostIdArgInEndLatestBoostOpportunity = true

				// order is created in booster
				booster.LaunchBoostReturns(boostID, nil)
				launchBoostExpectedCallCount = 1

				// boost opportunity is marked as complete successfully
				boostsDB.EndLatestBoostOpportunityReturns(nil)
				endLatestBoostOpportunityExpectedCallCount = 1

				Convey("notify via PubSub succeeded", func() {
					publisher.PublishBoostOpportunityCompletionReturns(nil)
					publishBoostOpportunityCompletionExpectedCallCount = 1
				})

				Convey("we fail to notify via PubSub", func() {
					publisher.PublishBoostOpportunityCompletionReturns(errors.New("some pubsub error"))
					publishBoostOpportunityCompletionExpectedCallCount = 1
				})
			})
			Convey("if boost opportunity has no contribution", func() {
				boostsDB.GetLatestBoostOpportunityReturns(&percy.BoostOpportunity{
					ChannelID:        channelID,
					ID:               boostOpportunityID,
					UnitsContributed: 0,
				}, nil)
				getLatestBoostOpportunityExpectedCallCount = 1

				// boost opportunity is marked as complete successfully
				boostsDB.EndLatestBoostOpportunityReturns(nil)
				endLatestBoostOpportunityExpectedCallCount = 1

				Convey("notify via PubSub succeeded", func() {
					publisher.PublishBoostOpportunityCompletionReturns(nil)
					publishBoostOpportunityCompletionExpectedCallCount = 1
				})

				Convey("we fail to notify via PubSub", func() {
					publisher.PublishBoostOpportunityCompletionReturns(errors.New("some pubsub error"))
					publishBoostOpportunityCompletionExpectedCallCount = 1
				})
			})
			err := launchpad.CompleteBoostOpportunity(context.Background(), channelID, boostOpportunityID)
			So(err, ShouldBeNil)
		})

		// sleep for goroutine to finish
		time.Sleep(10 * time.Millisecond)
		So(boostsDB.GetLatestBoostOpportunityCallCount(), ShouldEqual, getLatestBoostOpportunityExpectedCallCount)
		So(booster.LaunchBoostCallCount(), ShouldEqual, launchBoostExpectedCallCount)
		So(publisher.PublishBoostOpportunityCompletionCallCount(), ShouldEqual, publishBoostOpportunityCompletionExpectedCallCount)
		So(boostsDB.EndLatestBoostOpportunityCallCount(), ShouldEqual, endLatestBoostOpportunityExpectedCallCount)

		if checkBoostIdArgInEndLatestBoostOpportunity {
			_, _, _, boosterPtr := boostsDB.EndLatestBoostOpportunityArgsForCall(0)
			So(*boosterPtr, ShouldEqual, boostID)
		}
	})
}

func TestLaunchPad_AddBoostOpportunityContribution(t *testing.T) {
	Convey("given a boost opportunity launchpad", t, func() {
		boostOpportunity := tests.ValidBoostOpportunity()
		contribution := tests.ValidBoostOpportunityContribution()

		boostsDB := &internalfakes.FakeBoostsDB{}

		launchpad := percy.NewPaidBoostLaunchPad(percy.LaunchPadConfig{
			BoostsDB: boostsDB,
		})

		Convey("when there is an active boost opportunity", func() {
			boostsDB.GetLatestBoostOpportunityReturns(&boostOpportunity, nil)

			Convey("add the contribution to boosts DB", func() {
				boostsDB.GetBoostOpportunityContributionReturns(nil, nil)
				_, err := launchpad.AddBoostOpportunityContribution(context.Background(), contribution)
				So(err, ShouldBeNil)
				So(boostsDB.AddBoostOpportunityContributionCallCount(), ShouldEqual, 1)
			})

			Convey("no-ops if the contribution is already recorded", func() {
				boostsDB.GetBoostOpportunityContributionReturns(&contribution, nil)
				_, err := launchpad.AddBoostOpportunityContribution(context.Background(), contribution)
				So(err, ShouldBeNil)
				So(boostsDB.AddBoostOpportunityContributionCallCount(), ShouldBeZeroValue)
			})
		})

		Convey("returns an error when", func() {
			// happy paths
			boostsDB.GetLatestBoostOpportunityReturns(&boostOpportunity, nil)
			boostsDB.AddBoostOpportunityContributionReturns(boostOpportunity, errors.New("can't add"))

			Convey("contribution has no channel ID", func() {
				contribution.ChannelID = ""
			})

			Convey("contribution has no user ID", func() {
				contribution.UserID = ""
			})

			Convey("contribution has no origin ID", func() {
				contribution.OriginID = ""
			})

			Convey("contribution has no units", func() {
				contribution.Units = 0
			})

			Convey("failed to look up the latest boost opportunity", func() {
				boostsDB.GetLatestBoostOpportunityReturns(nil, errors.New("oops"))
			})

			Convey("there is no boost opportunity", func() {
				boostsDB.GetLatestBoostOpportunityReturns(nil, nil)
			})

			Convey("latest boost opportunity isn't active anymore", func() {
				endedAt := time.Now().Add(-5 * time.Minute)
				boostOpportunity.EndedAt = &endedAt
				boostsDB.GetLatestBoostOpportunityReturns(&boostOpportunity, nil)
			})

			Convey("fail to look up existing recorded contribution", func() {
				boostsDB.GetBoostOpportunityContributionReturns(nil, errors.New("uh-oh"))
			})

			Convey("fail to add the contribution to boost DB", func() {
				boostsDB.AddBoostOpportunityContributionReturns(boostOpportunity, errors.New("can't add"))
			})

			_, err := launchpad.AddBoostOpportunityContribution(context.Background(), contribution)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestLaunchPad_IsUserAllowedToContribute(t *testing.T) {
	Convey("given a launch pad", t, func() {
		userID := "12345"
		channelID := "67890"

		boostOpportunity := tests.ValidBoostOpportunity()

		boostsDB := &internalfakes.FakeBoostsDB{}
		configsDB := &internalfakes.FakePaidBoostConfigsDB{}
		userProfileDB := &internalfakes.FakeUserProfileDB{}

		launchPad := percy.NewPaidBoostLaunchPad(percy.LaunchPadConfig{
			BoostsDB:      boostsDB,
			ConfigsDB:     configsDB,
			UserProfileDB: userProfileDB,
		})

		Convey("returns true when", func() {
			Convey("user is allowed to contribute to the current active boost opportunity", func() {
				configsDB.IsChannelAllowedReturns(true)
				boostsDB.GetLatestBoostOpportunityReturns(&boostOpportunity, nil)
				userProfileDB.IsChatBannedReturns(false, nil)
			})

			isAllowed, err := launchPad.IsUserAllowedToContribute(context.Background(), userID, channelID, true)
			So(err, ShouldBeNil)
			So(isAllowed, ShouldBeTrue)
		})

		Convey("returns false when", func() {
			Convey("channel is not in the allow list to have boost opportunities", func() {
				configsDB.IsChannelAllowedReturns(false)
			})

			Convey("fail to retrieve active boost opportunity", func() {
				configsDB.IsChannelAllowedReturns(true)
				boostsDB.GetLatestBoostOpportunityReturns(nil, errors.New("woah?"))
			})

			Convey("there is no past boost opportunity", func() {
				configsDB.IsChannelAllowedReturns(true)
				boostsDB.GetLatestBoostOpportunityReturns(nil, nil)
			})

			Convey("the most recent boost opportunity is not active", func() {
				fiveSecondsAgo := time.Now().Add(-5 * time.Second)
				boostOpportunity.EndedAt = &fiveSecondsAgo

				configsDB.IsChannelAllowedReturns(true)
				boostsDB.GetLatestBoostOpportunityReturns(&boostOpportunity, nil)
			})

			Convey("fail to retrieve ban status", func() {
				configsDB.IsChannelAllowedReturns(true)
				boostsDB.GetLatestBoostOpportunityReturns(&boostOpportunity, nil)
				userProfileDB.IsChatBannedReturns(false, errors.New("I don't know"))
			})

			Convey("user is banned in the channel", func() {
				configsDB.IsChannelAllowedReturns(true)
				boostsDB.GetLatestBoostOpportunityReturns(&boostOpportunity, nil)
				userProfileDB.IsChatBannedReturns(true, nil)
			})

			isAllowed, err := launchPad.IsUserAllowedToContribute(context.Background(), userID, channelID, true)
			So(err, ShouldBeNil)
			So(isAllowed, ShouldBeFalse)
		})

		Convey("returns an error when", func() {
			Convey("user ID is empty", func() {
				userID = ""
			})

			Convey("channel ID is empty", func() {
				channelID = ""
			})

			_, err := launchPad.IsUserAllowedToContribute(context.Background(), userID, channelID, false)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestLaunchPad_SendBoostOpportunityContributionUserNotice(t *testing.T) {
	Convey("given a launchpad", t, func() {
		userID := "12345"
		channelID := "67890"
		units := 5

		userProfileDB := &internalfakes.FakeUserProfileDB{}

		launchPad := percy.NewPaidBoostLaunchPad(percy.LaunchPadConfig{
			UserProfileDB: userProfileDB,
		})

		Convey("returns an error when", func() {
			Convey("channel ID is empty", func() {
				channelID = ""
			})

			Convey("user ID is empty", func() {
				userID = ""
			})

			Convey("contribution unit is 0", func() {
				units = 0
			})

			Convey("user profile DB returns an error", func() {
				userProfileDB.SendUserNoticeReturns(errors.New("oops"))
			})

			contribution := percy.BoostOpportunityContribution{
				UserID:    userID,
				ChannelID: channelID,
				Timestamp: time.Now(),
				OriginID:  "origin.12345",
				Units:     units,
			}

			err := launchPad.SendBoostOpportunityContributionUserNotice(context.Background(), contribution)
			So(err, ShouldNotBeNil)
		})

		Convey("calls user profile DB to send a user notice", func() {
			contribution := percy.BoostOpportunityContribution{
				UserID:    userID,
				ChannelID: channelID,
				Timestamp: time.Now(),
				OriginID:  "origin.12345",
				Units:     units,
			}

			err := launchPad.SendBoostOpportunityContributionUserNotice(context.Background(), contribution)
			So(err, ShouldBeNil)
			So(userProfileDB.SendUserNoticeCallCount(), ShouldEqual, 1)
		})
	})
}
