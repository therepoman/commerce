package pdms

import (
	"context"
	"net/http"
	"time"

	pdms_service "code.justin.tv/amzn/PDMSLambdaTwirp"
	twirplambdatransport "code.justin.tv/amzn/TwirpGoLangAWSTransports/lambda"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/lambda"
	"github.com/golang/protobuf/ptypes"
	"github.com/pkg/errors"
	"github.com/twitchtv/twirp"
)

//go:generate counterfeiter . clientWrapper

// Creating our own interface so that we can generate a fake with counterfeiter
type clientWrapper interface {
	pdms_service.PDMSService
}

type Client struct {
	PDMS clientWrapper
}

func NewClient(cfg config.Config) *Client {
	// dedicating a session for just PDMS client
	// see https://wiki.twitch.com/display/SEC/PDMS-+Delete+pipeline+Service+Onboarding
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			Region:              aws.String("us-west-2"),
			STSRegionalEndpoint: endpoints.RegionalSTSEndpoint,
			// We want a long timeout for PDMS, as Lambda based services can take a while to warm up on cold start.
			HTTPClient: &http.Client{Timeout: 10 * time.Second},
		},
	}))
	// see https://wiki.twitch.com/display/SEC/PDMS-+Delete+pipeline+Service+Onboarding
	creds := stscreds.NewCredentials(sess, cfg.PDMS.CallerRole)
	pdmsLambdaCli := lambda.New(sess, &aws.Config{Credentials: creds})
	pdmsTransport := twirplambdatransport.NewClient(pdmsLambdaCli, cfg.PDMS.LambdaARN)
	pdmsClient := pdms_service.NewPDMSServiceProtobufClient("", pdmsTransport)

	return &Client{
		PDMS: pdmsClient,
	}
}

func (c *Client) PromiseDeletions(ctx context.Context, userIDs []string, timePromisedToDelete time.Time) error {
	timeOfDeletionPromiseTwirp, err := ptypes.TimestampProto(timePromisedToDelete)
	if err != nil {
		return err
	}

	for _, userID := range userIDs {
		_, err := c.PDMS.PromiseDeletion(ctx, &pdms_service.PromiseDeletionRequest{
			UserId:      userID,
			ServiceId:   percy.ServiceCatalogID,
			Timestamp:   timeOfDeletionPromiseTwirp,
			AutoResolve: false,
		})
		if err != nil {
			// PDMSClientWrapper's `promiseDeletion` function returns an HttpError wrapping a twirp error.
			if twirpError, ok := err.(twirp.Error); ok { //nolint
				if twirpError.Code() == twirp.AlreadyExists {
					logrus.Info("User has already been deleted in PDMS")
					return nil
				}
			}

			return err
		}
	}

	return nil
}

func (c *Client) ReportDeletions(ctx context.Context, userIDs []string, timeOfDeletion time.Time) error {
	timeOfDeletionTwirp, err := ptypes.TimestampProto(timeOfDeletion)
	if err != nil {
		return errors.Wrap(err, "failed to convert timestamp of deletion to ptypes timestamp")
	}

	for _, userID := range userIDs {
		_, err = c.PDMS.ReportDeletion(ctx, &pdms_service.ReportDeletionRequest{
			UserId:    userID,
			ServiceId: percy.ServiceCatalogID,
			Timestamp: timeOfDeletionTwirp,
		})
		if err != nil {
			return errors.Wrap(err, "failed to report deletion to PDMS")
		}
	}

	return nil
}
