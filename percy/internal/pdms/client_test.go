package pdms

import (
	"context"
	"errors"
	"testing"
	"time"

	pdms_service "code.justin.tv/amzn/PDMSLambdaTwirp"
	"code.justin.tv/commerce/percy/internal/pdms/pdmsfakes"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestClient_PromiseDeletions(t *testing.T) {
	Convey("given a PDMS client", t, func() {
		pdmsClient := &pdmsfakes.FakeClientWrapper{}

		client := &Client{
			PDMS: pdmsClient,
		}

		ctx := context.Background()
		userID := "123123123"
		userIDs := []string{userID}
		timePromised := time.Now()

		Convey("returns error", func() {
			Convey("when PDMS errors", func() {
				pdmsClient.PromiseDeletionReturns(nil, errors.New("WALRUS STRIKE"))
			})

			err := client.PromiseDeletions(ctx, userIDs, timePromised)

			So(err, ShouldNotBeNil)
		})

		Convey("returns success", func() {
			Convey("when PDMS returns already promised for user", func() {
				pdmsClient.PromiseDeletionReturns(nil, twirp.NewError(twirp.AlreadyExists, "WALRUS STRIKE"))
			})

			Convey("when PDMS call succeeds", func() {
				pdmsClient.PromiseDeletionReturns(&pdms_service.PromiseDeletionPayload{}, nil)
			})

			err := client.PromiseDeletions(ctx, userIDs, timePromised)

			So(err, ShouldBeNil)
		})
	})
}

func TestClient_ReportDeletions(t *testing.T) {
	Convey("given a PDMS client", t, func() {
		pdmsClient := &pdmsfakes.FakeClientWrapper{}

		client := &Client{
			PDMS: pdmsClient,
		}

		ctx := context.Background()
		userID := "123123123"
		userIDs := []string{userID}
		timePromised := time.Now()

		Convey("returns error", func() {
			Convey("when PDMS client returns an error", func() {
				pdmsClient.ReportDeletionReturns(nil, errors.New("WALRUS STRIKE"))
			})

			err := client.ReportDeletions(ctx, userIDs, timePromised)

			So(err, ShouldNotBeNil)
		})

		Convey("returns success", func() {
			Convey("when PDMS client succeeds", func() {
				pdmsClient.ReportDeletionReturns(&pdms_service.ReportDeletionPayload{}, nil)
			})

			err := client.ReportDeletions(ctx, userIDs, timePromised)

			So(err, ShouldBeNil)
		})
	})
}
