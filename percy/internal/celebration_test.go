package percy_test

import (
	"context"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/metrics"
	"github.com/stretchr/testify/assert"
)

func TestCelebrationEngine_StartHighestValueCelebration(t *testing.T) {
	tests := []struct {
		scenario string
		test     func(t *testing.T)
	}{
		{
			scenario: "does not attempt to publish when celebrations are disabled",
			test: func(t *testing.T) {
				fakeCelebDB := internalfakes.FakeCelebrationConfigsDB{}
				fakePublisher := internalfakes.FakePublisher{}
				fakeSpade := internalfakes.FakeSpade{}
				engine := percy.NewCelebrationEngine(percy.CelebrationEngineConfig{
					CelebrationConfigsDB: &fakeCelebDB,
					Statter:              &metrics.NoopStatter{},
					Publisher:            &fakePublisher,
					Spade:                &fakeSpade,
				})
				fakeCelebDB.GetCelebrationConfigReturns(&percy.CelebrationConfig{
					ChannelID: "123",
					Enabled:   false,
					Celebrations: map[string]percy.Celebration{
						"0": {
							CelebrationID:        "0",
							Enabled:              true,
							EventType:            percy.EventTypeSubscriptionGift,
							EventThreshold:       4,
							CelebrationEffect:    "",
							CelebrationArea:      "",
							CelebrationIntensity: 0,
							CelebrationDuration:  0,
						},
					},
				}, nil)
				err := engine.StartHighestValueCelebration(context.Background(), "1", percy.NewUser("2"), percy.EventTypeSubscriptionGift, 5, "id")
				assert.NoError(t, err)
				assert.Equal(t, 0, fakePublisher.StartCelebrationCallCount())
			},
		},
		{
			scenario: "does not attempt to publish when amount doesn't meet threshold",
			test: func(t *testing.T) {
				fakeCelebDB := internalfakes.FakeCelebrationConfigsDB{}
				fakePublisher := internalfakes.FakePublisher{}
				fakeSpade := internalfakes.FakeSpade{}
				engine := percy.NewCelebrationEngine(percy.CelebrationEngineConfig{
					CelebrationConfigsDB: &fakeCelebDB,
					Statter:              &metrics.NoopStatter{},
					Publisher:            &fakePublisher,
					Spade:                &fakeSpade,
				})
				fakeCelebDB.GetCelebrationConfigReturns(&percy.CelebrationConfig{
					ChannelID: "123",
					Enabled:   true,
					Celebrations: map[string]percy.Celebration{
						"0": {
							CelebrationID:        "0",
							Enabled:              true,
							EventType:            percy.EventTypeSubscriptionGift,
							EventThreshold:       100,
							CelebrationEffect:    "",
							CelebrationArea:      "",
							CelebrationIntensity: 0,
							CelebrationDuration:  0,
						},
					},
				}, nil)
				err := engine.StartHighestValueCelebration(context.Background(), "1", percy.NewUser("2"), percy.EventTypeSubscriptionGift, 5, "id")
				assert.NoError(t, err)
				assert.Equal(t, 0, fakePublisher.StartCelebrationCallCount())
			},
		},
		{
			scenario: "publishes the highest valued celebration for the given event type - sub gifting",
			test: func(t *testing.T) {
				fakeCelebDB := internalfakes.FakeCelebrationConfigsDB{}
				fakePublisher := internalfakes.FakePublisher{}
				fakeSpade := internalfakes.FakeSpade{}
				engine := percy.NewCelebrationEngine(percy.CelebrationEngineConfig{
					CelebrationConfigsDB: &fakeCelebDB,
					Statter:              &metrics.NoopStatter{},
					Publisher:            &fakePublisher,
					Spade:                &fakeSpade,
				})
				fakeCelebDB.GetCelebrationConfigReturns(&percy.CelebrationConfig{
					ChannelID: "123",
					Enabled:   true,
					Celebrations: map[string]percy.Celebration{
						"0": {
							CelebrationID:        "0",
							Enabled:              true,
							EventType:            percy.EventTypeSubscriptionGift,
							EventThreshold:       5,
							CelebrationEffect:    "",
							CelebrationArea:      "",
							CelebrationIntensity: 0,
							CelebrationDuration:  0,
						},
						"1": {
							CelebrationID:        "1",
							Enabled:              true,
							EventType:            percy.EventTypeSubscriptionGift,
							EventThreshold:       50,
							CelebrationEffect:    "",
							CelebrationArea:      "",
							CelebrationIntensity: 0,
							CelebrationDuration:  0,
						},
						"2": {
							CelebrationID:        "2",
							Enabled:              true,
							EventType:            percy.EventTypeSubscriptionGift,
							EventThreshold:       7,
							CelebrationEffect:    "",
							CelebrationArea:      "",
							CelebrationIntensity: 0,
							CelebrationDuration:  0,
						},
						"4": {
							CelebrationID:        "4",
							Enabled:              true,
							EventType:            percy.EventTypeCheer,
							EventThreshold:       50,
							CelebrationEffect:    "",
							CelebrationArea:      "",
							CelebrationIntensity: 0,
							CelebrationDuration:  0,
						},
					},
				}, nil)
				err := engine.StartHighestValueCelebration(context.Background(), "1", percy.NewUser("2"), percy.EventTypeSubscriptionGift, 50, "id")
				assert.NoError(t, err)
				assert.Equal(t, 1, fakePublisher.StartCelebrationCallCount())
				_, _, _, _, celebration := fakePublisher.StartCelebrationArgsForCall(0)
				assert.Equal(t, "1", celebration.CelebrationID)
			},
		},
		{
			scenario: "publishes the highest valued celebration for the given event type - cheering",
			test: func(t *testing.T) {
				fakeCelebDB := internalfakes.FakeCelebrationConfigsDB{}
				fakePublisher := internalfakes.FakePublisher{}
				fakeSpade := internalfakes.FakeSpade{}
				engine := percy.NewCelebrationEngine(percy.CelebrationEngineConfig{
					CelebrationConfigsDB: &fakeCelebDB,
					Statter:              &metrics.NoopStatter{},
					Publisher:            &fakePublisher,
					Spade:                &fakeSpade,
				})
				fakeCelebDB.GetCelebrationConfigReturns(&percy.CelebrationConfig{
					ChannelID: "123",
					Enabled:   true,
					Celebrations: map[string]percy.Celebration{
						"0": {
							CelebrationID:        "0",
							Enabled:              true,
							EventType:            percy.EventTypeSubscriptionGift,
							EventThreshold:       500,
							CelebrationEffect:    "",
							CelebrationArea:      "",
							CelebrationIntensity: 0,
							CelebrationDuration:  0,
						},
						"1": {
							CelebrationID:        "1",
							Enabled:              true,
							EventType:            percy.EventTypeCheer,
							EventThreshold:       500,
							CelebrationEffect:    "",
							CelebrationArea:      "",
							CelebrationIntensity: 0,
							CelebrationDuration:  0,
						},
						"2": {
							CelebrationID:        "2",
							Enabled:              true,
							EventType:            percy.EventTypeSubscriptionGift,
							EventThreshold:       7,
							CelebrationEffect:    "",
							CelebrationArea:      "",
							CelebrationIntensity: 0,
							CelebrationDuration:  0,
						},
						"4": {
							CelebrationID:        "4",
							Enabled:              true,
							EventType:            percy.EventTypeCheer,
							EventThreshold:       900,
							CelebrationEffect:    "",
							CelebrationArea:      "",
							CelebrationIntensity: 0,
							CelebrationDuration:  0,
						},
					},
				}, nil)
				err := engine.StartHighestValueCelebration(context.Background(), "1", percy.NewUser("2"), percy.EventTypeCheer, 700, "id")
				assert.NoError(t, err)
				assert.Equal(t, 1, fakePublisher.StartCelebrationCallCount())
				_, _, _, _, celebration := fakePublisher.StartCelebrationArgsForCall(0)
				assert.Equal(t, "1", celebration.CelebrationID)
			},
		},
		{
			scenario: "publishes celebration event - sub founder",
			test: func(t *testing.T) {
				fakeCelebDB := internalfakes.FakeCelebrationConfigsDB{}
				fakePublisher := internalfakes.FakePublisher{}
				fakeSpade := internalfakes.FakeSpade{}
				engine := percy.NewCelebrationEngine(percy.CelebrationEngineConfig{
					CelebrationConfigsDB: &fakeCelebDB,
					Statter:              &metrics.NoopStatter{},
					Publisher:            &fakePublisher,
					Spade:                &fakeSpade,
				})
				fakeCelebDB.GetCelebrationConfigReturns(&percy.CelebrationConfig{
					ChannelID:    "123",
					Enabled:      true,
					Celebrations: map[string]percy.Celebration{},
				}, nil)
				err := engine.StartHighestValueCelebration(context.Background(), "1", percy.NewUser("2"), percy.EventTypeSubscriptionFounder, 1, "id")
				assert.NoError(t, err)
				assert.Equal(t, 1, fakePublisher.StartCelebrationCallCount())
			},
		},
	}

	for _, test := range tests {
		tc := test
		t.Run(test.scenario, func(t *testing.T) {
			tc.test(t)
		})
	}
}
