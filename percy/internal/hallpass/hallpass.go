package hallpass

import (
	"context"
	"fmt"
	"net/http"

	"code.justin.tv/cb/hallpass/client/hallpass"
	"code.justin.tv/commerce/logrus"
	celebration_error "code.justin.tv/commerce/percy/internal/errors"
	"code.justin.tv/commerce/percy/internal/wrapper"
	"code.justin.tv/foundation/twitchclient"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
)

const (
	serviceName    = "hallpass"
	defaultRetry   = 1
	defaultTimeout = 1000

	getEditorStatusByIDAPI = "get_editor"
)

type EditorDB struct {
	client hallpass.Client
}

func NewEditorDB(endpoint string, stats statsd.Statter, socksAddr string) (*EditorDB, error) {
	client, err := hallpass.NewClient(twitchclient.ClientConf{
		Stats: stats,
		Host:  endpoint,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(socksAddr),
			wrapper.NewRetryRoundTripWrapper(stats, serviceName, defaultRetry),
			wrapper.NewHystrixRoundTripWrapper(serviceName, defaultTimeout),
		},
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: 200,
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to initialize hallpass client")
	}

	return &EditorDB{
		client: client,
	}, nil
}

func (db *EditorDB) GetEditorStatus(ctx context.Context, channelID, userID string) (bool, error) {
	if len(channelID) == 0 {
		return false, celebration_error.IllegalArgument.New("empty channel ID")
	}

	if len(userID) == 0 {
		return false, celebration_error.IllegalArgument.New("empty user ID")
	}

	reqCtx := wrapper.GenerateRequestContext(ctx, serviceName, getEditorStatusByIDAPI)
	resp, err := db.client.GetV1IsEditor(reqCtx, channelID, userID, nil)
	if err != nil {
		msg := fmt.Sprintf("error fetching editor status for user_id %s in channel_id %s", userID, channelID)
		return false, celebration_error.InternalError.Wrap(err, msg)
	}

	if resp == nil {
		msg := fmt.Sprintf("hallpass response was nil for user_id %s in channel_id %s", userID, channelID)
		logrus.WithFields(logrus.Fields{
			"ownedBy": channelID,
			"userID":  userID,
		}).Error(msg)
		return false, celebration_error.InternalError.New(msg)
	}

	// Channel editors are authorized
	if resp.IsEditor {
		return true, nil
	}

	return false, nil
}
