package percy

import (
	"context"
	"time"
)

//go:generate counterfeiter . SubscriptionsDB

type SubscriptionsDB interface {
	GetChannelEmoticonSets(ctx context.Context, id string) ([]string, error)
	GetChannelOnboardDate(ctx context.Context, channelID string) (*time.Time, error)
}
