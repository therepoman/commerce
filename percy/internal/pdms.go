package percy

import (
	"context"
	"time"
)

const (
	ServiceCatalogID = "659"
)

//go:generate counterfeiter . Privacy

type Privacy interface {
	PromiseDeletions(ctx context.Context, userIDs []string, timePromisedToDelete time.Time) error
	ReportDeletions(ctx context.Context, userIDs []string, timeOfDeletion time.Time) error
}

type UserDeletionRequest interface {
	GetUserDeletionRequestData() *UserDeletionRequestData
}

type UserDeletionRequestData struct {
	UserIDs        []string `json:"user_ids"`
	IsDryRun       bool     `json:"is_dry_run"`
	ReportDeletion bool     `json:"report_deletion"`
}

type UserDeletionResponseData struct {
	UserIDs        []string `json:"user_ids"`
	IsDryRun       bool     `json:"is_dry_run"`
	ReportDeletion bool     `json:"report_deletion"`
}

type StartUserDeletionRequest struct {
	UserDeletionRequestData
}

func (r *StartUserDeletionRequest) GetUserDeletionRequestData() *UserDeletionRequestData {
	if r == nil {
		return nil
	}

	return &r.UserDeletionRequestData
}

type CleanBadgesRecordsRequest struct {
	UserDeletionRequestData
}

func (r *CleanBadgesRecordsRequest) GetUserDeletionRequestData() *UserDeletionRequestData {
	if r == nil {
		return nil
	}

	return &r.UserDeletionRequestData
}

type CleanBadgesRecordsResponse struct {
	UserDeletionResponseData
}

type CleanConductorsRecordsRequest struct {
	UserDeletionRequestData
}

func (r *CleanConductorsRecordsRequest) GetUserDeletionRequestData() *UserDeletionRequestData {
	if r == nil {
		return nil
	}

	return &r.UserDeletionRequestData
}

type CleanConductorsRecordsResponse struct {
	UserDeletionResponseData
}

type CleanHypeTrainConfigsRecordsRequest struct {
	UserDeletionRequestData
}

func (r *CleanHypeTrainConfigsRecordsRequest) GetUserDeletionRequestData() *UserDeletionRequestData {
	if r == nil {
		return nil
	}

	return &r.UserDeletionRequestData
}

type CleanHypeTrainConfigsRecordsResponse struct {
	UserDeletionResponseData
}

type ReportDeletionRequest struct {
	UserDeletionRequestData
}

func (r *ReportDeletionRequest) GetUserDeletionRequestData() *UserDeletionRequestData {
	if r == nil {
		return nil
	}

	return &r.UserDeletionRequestData
}

type ReportDeletionResponse struct {
	UserDeletionResponseData
	Timestamp time.Time `json:"timestamp"`
}
