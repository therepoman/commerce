package idempotency

import (
	"context"
	"errors"
	"testing"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestEnforcer_WithIdempotency(t *testing.T) {
	Convey("Given an idempotency enforcer", t, func() {
		db := internalfakes.FakeIdempotentTaskDB{}
		enforcer := Enforcer{
			idempotencyDB: &db,
		}

		Convey("when fetching the previous state errors", func() {
			db.GetReturns(nil, errors.New("ERROR"))
			err := enforcer.WithIdempotency(context.Background(), percy.IdempotencyNamespaceCelebrationPurchaseFulfillment, "123", string(percy.CelebrationPurchaseFulfillmentActivityFeed), func() error {
				return nil
			})

			So(err, ShouldNotBeNil)
		})

		Convey("when there is no previous state", func() {
			Convey("we should call the processing func", func() {
				db.GetReturns(nil, nil)

				called := false

				err := enforcer.WithIdempotency(context.Background(), percy.IdempotencyNamespaceCelebrationPurchaseFulfillment, "123", string(percy.CelebrationPurchaseFulfillmentActivityFeed), func() error {
					called = true
					return nil
				})

				So(err, ShouldBeNil)
				So(called, ShouldBeTrue)
			})

			Convey("when the processing func errors, we should mark the task as erred", func() {
				db.GetReturns(nil, nil)

				err := enforcer.WithIdempotency(context.Background(), percy.IdempotencyNamespaceCelebrationPurchaseFulfillment, "123", string(percy.CelebrationPurchaseFulfillmentActivityFeed), func() error {
					return errors.New("ERROR")
				})

				So(err, ShouldNotBeNil)

				_, _, _, _, updateArgs0 := db.UpdateArgsForCall(0)
				So(updateArgs0.State, ShouldEqual, percy.IdempotencyStateProcessing)

				_, _, _, _, updateArgs1 := db.UpdateArgsForCall(1)
				So(updateArgs1.State, ShouldEqual, percy.IdempotencyStateError)
			})

			Convey("when the processing func succeeds, we should mark the task as complete", func() {
				db.GetReturns(nil, nil)

				err := enforcer.WithIdempotency(context.Background(), percy.IdempotencyNamespaceCelebrationPurchaseFulfillment, "123", string(percy.CelebrationPurchaseFulfillmentActivityFeed), func() error {
					return nil
				})

				So(err, ShouldBeNil)

				_, _, _, _, updateArgs0 := db.UpdateArgsForCall(0)
				So(updateArgs0.State, ShouldEqual, percy.IdempotencyStateProcessing)

				_, _, _, _, updateArgs1 := db.UpdateArgsForCall(1)
				So(updateArgs1.State, ShouldEqual, percy.IdempotencyStateComplete)
			})
		})

		Convey("when the previous state is processing", func() {
			Convey("when the last updated timestamp is older than 10s, we should call the processing func", func() {
				db.GetReturns(&percy.IdempotentTask{
					State:       percy.IdempotencyStateProcessing,
					LastUpdated: time.Now().Add(-15 * time.Second),
				}, nil)

				called := false

				err := enforcer.WithIdempotency(context.Background(), percy.IdempotencyNamespaceCelebrationPurchaseFulfillment, "123", string(percy.CelebrationPurchaseFulfillmentActivityFeed), func() error {
					called = true
					return nil
				})

				So(err, ShouldBeNil)
				So(called, ShouldBeTrue)
			})

			Convey("when the last updated timestamp is newer than 10s, we should not call the processing func", func() {
				db.GetReturns(&percy.IdempotentTask{
					State:       percy.IdempotencyStateProcessing,
					LastUpdated: time.Now(),
				}, nil)

				called := false

				err := enforcer.WithIdempotency(context.Background(), percy.IdempotencyNamespaceCelebrationPurchaseFulfillment, "123", string(percy.CelebrationPurchaseFulfillmentActivityFeed), func() error {
					called = true
					return nil
				})

				So(err, ShouldNotBeNil)
				So(called, ShouldBeFalse)
			})
		})

		Convey("when the previous state is erred, we should re-process", func() {
			db.GetReturns(&percy.IdempotentTask{
				State:       percy.IdempotencyStateError,
				LastUpdated: time.Now(),
			}, nil)
			db.UpdateReturns(nil, nil)

			called := false

			err := enforcer.WithIdempotency(context.Background(), percy.IdempotencyNamespaceCelebrationPurchaseFulfillment, "123", string(percy.CelebrationPurchaseFulfillmentActivityFeed), func() error {
				called = true
				return nil
			})

			So(err, ShouldBeNil)
			So(called, ShouldBeTrue)

			_, _, _, _, updateArgs0 := db.UpdateArgsForCall(0)
			So(updateArgs0.State, ShouldEqual, percy.IdempotencyStateProcessing)

			_, _, _, _, updateArgs1 := db.UpdateArgsForCall(1)
			So(updateArgs1.State, ShouldEqual, percy.IdempotencyStateComplete)
		})

		Convey("when the previous state is complete, we should return nil", func() {
			db.GetReturns(&percy.IdempotentTask{
				State:       percy.IdempotencyStateComplete,
				LastUpdated: time.Now(),
			}, nil)
			db.UpdateReturns(nil, nil)

			err := enforcer.WithIdempotency(context.Background(), percy.IdempotencyNamespaceCelebrationPurchaseFulfillment, "123", string(percy.CelebrationPurchaseFulfillmentActivityFeed), func() error {
				return nil
			})

			So(err, ShouldBeNil)
			So(db.UpdateCallCount(), ShouldEqual, 0)
		})

		Convey("when the previous state is unknown, we should return an error", func() {
			db.GetReturns(&percy.IdempotentTask{
				State: "FOO",
			}, nil)
			err := enforcer.WithIdempotency(context.Background(), percy.IdempotencyNamespaceCelebrationPurchaseFulfillment, "123", string(percy.CelebrationPurchaseFulfillmentActivityFeed), func() error {
				return nil
			})

			So(err, ShouldNotBeNil)
		})
	})
}
