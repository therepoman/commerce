package idempotency

import (
	"context"
	"fmt"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"github.com/pkg/errors"
)

const (
	// This assumes that the max execution duration for Lambdas using this utility is 10s (which is currently the case).
	// If we add longer running Lambdas, this will need to be configurable.
	lambdaTimeoutDuration = 10 * time.Second
)

type Enforcer struct {
	idempotencyDB percy.IdempotentTaskDB
}

func NewEnforcer(db percy.IdempotentTaskDB) (*Enforcer, error) {
	return &Enforcer{
		idempotencyDB: db,
	}, nil
}

func (e *Enforcer) WithIdempotency(ctx context.Context, namespace percy.IdempotencyNamespace, transactionID, taskName string, processingFunc func() error) error {
	shouldProcess, err := e.enforce(ctx, namespace, transactionID, taskName)
	if err != nil {
		return err
	}
	if !shouldProcess {
		return nil
	}

	err = processingFunc()

	if err != nil {
		markErr := e.markErred(ctx, namespace, transactionID, taskName, err)
		if markErr != nil {
			return fmt.Errorf("error marking task as erred in WithIdempotency: original %v markErr %w", err, markErr)
		}
		return err
	} else {
		markErr := e.markComplete(ctx, namespace, transactionID, taskName)
		if markErr != nil {
			return errors.Wrap(markErr, "error marking task as complete in WithIdempotency")
		}
	}

	return nil
}

func (e *Enforcer) enforce(ctx context.Context, namespace percy.IdempotencyNamespace, transactionID, taskName string) (shouldProcess bool, err error) {
	record, err := e.idempotencyDB.Get(ctx, namespace, transactionID, taskName)
	if err != nil {
		return false, err
	}

	if record != nil {
		switch record.State {
		case percy.IdempotencyStateProcessing:
			// If the Lambda is stuck in the processing state for 10 seconds or more, allow the task to be retried. We can
			// assume that the previous attempt timed out or hit a Lambda internal error before it had a chance to complete.
			//
			// A better solution might be to catch this class of error in the sfn and correct the dynamo state, but the engineering
			// effort for such a rare case is not worth.
			if record.LastUpdated.Before(time.Now().Add(-lambdaTimeoutDuration)) {
				break
			}

			return false, errors.New("conflicting transaction processing balance change")
		case percy.IdempotencyStateComplete:
			return false, nil
		case percy.IdempotencyStateError:
			break
		default:
			return false, fmt.Errorf("unhandled state while processing idempotency: %s", string(record.State))
		}
	}

	_, err = e.idempotencyDB.Update(ctx, namespace, transactionID, taskName, percy.IdempotentTaskUpdate{
		State: percy.IdempotencyStateProcessing,
	})
	if err != nil {
		return false, err
	}

	return true, nil
}

func (e *Enforcer) markComplete(ctx context.Context, namespace percy.IdempotencyNamespace, transactionID, taskName string) error {
	_, err := e.idempotencyDB.Update(ctx, namespace, transactionID, taskName, percy.IdempotentTaskUpdate{
		State: percy.IdempotencyStateComplete,
	})
	if err != nil {
		return err
	}

	return nil
}

func (e *Enforcer) markErred(ctx context.Context, namespace percy.IdempotencyNamespace, transactionID, taskName string, err error) error {
	if err == nil {
		return errors.New("markErred must be called with an error")
	}

	_, err = e.idempotencyDB.Update(ctx, namespace, transactionID, taskName, percy.IdempotentTaskUpdate{
		State: percy.IdempotencyStateError,
		Error: err,
	})

	if err != nil {
		return err
	}

	return nil
}
