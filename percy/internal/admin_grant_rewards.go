package percy

import (
	"context"
	"time"

	"github.com/pkg/errors"
)

type GrantHypeTrainRewardsResult struct {
	HypeTrain        *HypeTrain
	GrantedRewards   []Reward
	UserOwnedRewards []Reward
	Participant      Participant
}

func (eng *engine) AdminGrantHypeTrainRewardsByHypeTrainID(ctx context.Context, channelID, hypeTrainID, userID string) (GrantHypeTrainRewardsResult, error) {
	if len(channelID) == 0 {
		return GrantHypeTrainRewardsResult{}, errors.New("channel ID is blank")
	}

	if len(hypeTrainID) == 0 {
		return GrantHypeTrainRewardsResult{}, errors.New("hype train ID is blank")
	}

	if len(userID) == 0 {
		return GrantHypeTrainRewardsResult{}, errors.New("user ID is blank")
	}

	// find hype train given ID
	hypeTrain, err := eng.HypeTrainDB.GetHypeTrainByID(ctx, hypeTrainID)
	if err != nil {
		return GrantHypeTrainRewardsResult{}, errors.Wrap(err, "error retrieving hype train with the given ID")
	}

	if hypeTrain == nil {
		return GrantHypeTrainRewardsResult{}, nil
	}

	grantedRewards, participant, err := eng.adminGrantHypeTrainRewards(ctx, *hypeTrain, userID)
	if err != nil {
		return GrantHypeTrainRewardsResult{}, err
	}

	allUserOwnedRewards, err := eng.getAllOwnedRewards(ctx, channelID, userID)
	if err != nil {
		return GrantHypeTrainRewardsResult{}, errors.Wrap(err, "error retrieving all of user's emote rewards")
	}

	return GrantHypeTrainRewardsResult{
		HypeTrain:        hypeTrain,
		GrantedRewards:   grantedRewards,
		UserOwnedRewards: allUserOwnedRewards,
		Participant:      participant,
	}, nil
}

func (eng *engine) AdminGrantHypeTrainRewardsByParticipationTime(ctx context.Context, channelID, userID string, participatedAt time.Time) (GrantHypeTrainRewardsResult, error) {
	if len(channelID) == 0 {
		return GrantHypeTrainRewardsResult{}, errors.New("channel ID is blank")
	}

	if len(userID) == 0 {
		return GrantHypeTrainRewardsResult{}, errors.New("user ID is blank")
	}

	if participatedAt.IsZero() {
		return GrantHypeTrainRewardsResult{}, errors.New("participated at is zero")
	}

	// find the possible hype trains that the given participation could have contributed to.
	// we look 30 minutes past and forward to allow more margin of errors.
	from := participatedAt.Add(-30 * time.Minute)
	to := participatedAt.Add(30 * time.Minute)
	params := HypeTrainQueryParams{
		From: &from,
		To:   &to,
	}

	hypeTrains, err := eng.HypeTrainDB.QueryHypeTrainByStartTime(ctx, channelID, params)
	if err != nil {
		return GrantHypeTrainRewardsResult{}, errors.Wrapf(err, "error retrieving hype train with the given start time range (%v, %v) for channel %s", params.From, params.To, channelID)
	}

	if len(hypeTrains) == 0 {
		return GrantHypeTrainRewardsResult{}, nil
	}

	// we shouldn't be getting more than 1 results here
	hypeTrain := hypeTrains[0]
	grantedRewards, participant, err := eng.adminGrantHypeTrainRewards(ctx, hypeTrain, userID)
	if err != nil {
		return GrantHypeTrainRewardsResult{}, err
	}

	allUserOwnedRewards, err := eng.getAllOwnedRewards(ctx, channelID, userID)
	if err != nil {
		return GrantHypeTrainRewardsResult{}, errors.Wrap(err, "error retrieving all of user's emote rewards")
	}

	return GrantHypeTrainRewardsResult{
		HypeTrain:        &hypeTrain,
		GrantedRewards:   grantedRewards,
		UserOwnedRewards: allUserOwnedRewards,
		Participant:      participant,
	}, nil
}

func (eng *engine) adminGrantHypeTrainRewards(ctx context.Context, hypeTrain HypeTrain, userID string) ([]Reward, Participant, error) {
	// check if user already received emote
	participant, err := eng.ParticipantDB.GetHypeTrainParticipant(ctx, hypeTrain.ID, userID)
	if err != nil {
		return nil, Participant{}, errors.Wrap(err, "error retrieving participant info")
	}

	var rewards []Reward

	if participant == nil {
		// we don't have user's participation records
		// either user contributed slightly outside the hype train time (e.g a couple seconds too late),
		// or we failed to receive the event.
		// in this case, just grant emote rewards and don't check for conductor badges.
		reward, err := eng.AdminEntitleParticipant(ctx, hypeTrain, userID)
		if err != nil {
			return nil, Participant{}, errors.Wrap(err, "error granting user hype train participation reards")
		}

		if reward != nil && len(reward.ID) > 0 {
			rewards = append(rewards, *reward)
		}

		// record this user as a participant with Admin Action and Source
		// so we don't double entitle this user again on the next API call.
		participant := Participant{
			HypeTrainID: hypeTrain.ID,
			ChannelID:   hypeTrain.ChannelID,
			User:        NewUser(userID),
			Rewards:     rewards,
			ParticipationTotals: map[ParticipationIdentifier]int{
				{
					Source: AdminSource,
					Action: AdminAction,
				}: 1,
			},
		}

		err = eng.ParticipantDB.PutHypeTrainParticipants(ctx, participant)
		if err != nil {
			return nil, Participant{}, errors.Wrap(err, "error recording user as participant")
		}

		return rewards, participant, nil
	}

	// we have user's participation records
	// check if we have already granted user a emote reward, and verify with our entitlement DB
	// if we don't have record of any reward, try granting them again.
	shouldGrantRewards := false
	if len(participant.Rewards) != 0 {
		// check entitlement DB to ensure user has received the emote reward
		entitleRewards, err := eng.EntitlementDB.ReadEmoteEntitlements(ctx, userID)
		if err != nil {
			return nil, Participant{}, errors.Wrap(err, "error retrieving user's entitlements")
		}

		for _, reward := range participant.Rewards {
			if reward.Type != EmoteReward {
				continue
			}

			// if there is a reward that we think the user should have but the user doesn't, re-run the reward granting steps again.
			if _, ok := entitleRewards[reward.ID]; !ok {
				shouldGrantRewards = true
				break
			}
		}

		// user has the rewards that we recorded
		if !shouldGrantRewards {
			rewards = append(rewards, participant.Rewards...)
		}
	} else {
		shouldGrantRewards = true
	}

	if shouldGrantRewards {
		reward, err := eng.AdminEntitleParticipant(ctx, hypeTrain, userID)
		if err != nil {
			return nil, Participant{}, errors.Wrap(err, "error granting user hype train participation rewards")
		}

		if reward != nil {
			rewards = append(rewards, *reward)
		}

		// update participant record in DB
		participant.Rewards = rewards
		err = eng.ParticipantDB.PutHypeTrainParticipants(ctx, *participant)
		if err != nil {
			return nil, Participant{}, errors.Wrap(err, "error updating participant record after granting participation rewards")
		}
	}

	// check if user is eligible for a former conductor badge
	if isEligibleForConductor(*participant, hypeTrain) {
		currentBadges, err := eng.BadgesDB.GetBadges(ctx, userID, hypeTrain.ChannelID)
		if err != nil {
			return nil, Participant{}, errors.Wrap(err, "error checking user badges")
		}

		if len(currentBadges) == 0 {
			err := eng.BadgesDB.SetBadges(ctx, userID, hypeTrain.ChannelID, []Badge{FormerConductorBadge})
			if err != nil {
				return nil, Participant{}, errors.Wrap(err, "error granting former conductor badge")
			}

			rewards = append(rewards, Reward{
				Type:  BadgeReward,
				ID:    string(FormerConductorBadge),
				SetID: string(FormerConductorBadge),
			})
		} else {
			for _, badge := range currentBadges {
				rewards = append(rewards, Reward{
					Type:  BadgeReward,
					ID:    string(badge),
					SetID: string(badge),
				})
			}
		}
	}

	return rewards, *participant, nil
}

func isEligibleForConductor(participant Participant, hypeTrain HypeTrain) bool {
	for _, conductor := range hypeTrain.Conductors {
		if participant.ParticipationTotals.ParticipationQuantitiesBySource(conductor.Source) >
			conductor.Participations.ParticipationQuantitiesBySource(conductor.Source) {
			return true
		}
	}

	return false
}

func (eng *engine) getAllOwnedRewards(ctx context.Context, channelID string, userID string) ([]Reward, error) {
	ownedEmoteRewards, err := eng.EntitlementDB.ReadEmoteEntitlements(ctx, userID)
	if err != nil {
		return nil, err
	}

	ownedBadges, err := eng.BadgesDB.GetBadges(ctx, userID, channelID)
	if err != nil {
		return nil, err
	}

	allRewards := make([]Reward, 0, len(ownedEmoteRewards)+len(ownedEmoteRewards))
	for id := range ownedEmoteRewards {
		allRewards = append(allRewards, Reward{
			ID:   id,
			Type: EmoteReward,
		})
	}

	for _, badge := range ownedBadges {
		allRewards = append(allRewards, Reward{
			Type: BadgeReward,
			ID:   string(badge),
		})
	}

	return allRewards, nil
}
