package percy

import (
	"context"

	"github.com/pkg/errors"
)

type CelebrationUserSettings struct {
	IsOptedOut bool
}

type CelebrationUserSettingsUpdate struct {
	IsOptedOut *bool
}

var DefaultCelebrationUserSettings = CelebrationUserSettings{IsOptedOut: false}

//go:generate counterfeiter . CelebrationUserSettingsDB

type CelebrationUserSettingsDB interface {
	Get(ctx context.Context, userID string) (*CelebrationUserSettings, error)
	Update(ctx context.Context, userID string, update CelebrationUserSettingsUpdate) (*CelebrationUserSettings, error)
}

func (eng *celebrationEngine) GetCelebrationUserSettings(ctx context.Context, userID string) (CelebrationUserSettings, error) {
	userSettings, err := eng.CelebrationUserSettingsDB.Get(ctx, userID)
	if err != nil {
		return CelebrationUserSettings{}, errors.Wrap(err, "failed to fetch user settings")
	}

	if userSettings == nil {
		return DefaultCelebrationUserSettings, nil
	}

	return *userSettings, nil
}

func (eng *celebrationEngine) UpdateCelebrationUserSettings(ctx context.Context, userID string, update CelebrationUserSettingsUpdate) (CelebrationUserSettings, error) {
	userSettings, err := eng.CelebrationUserSettingsDB.Update(ctx, userID, update)
	if err != nil {
		return CelebrationUserSettings{}, errors.Wrap(err, "failed to update user settings")
	}

	if userSettings == nil {
		return CelebrationUserSettings{}, errors.New("no new user settings was returned")
	}

	return *userSettings, nil
}
