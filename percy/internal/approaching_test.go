package percy_test

import (
	"context"
	"testing"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"github.com/pkg/errors"
	. "github.com/smartystreets/goconvey/convey"
)

func TestEngine_UpdateHypeTrainApproaching(t *testing.T) {
	Convey("Given an Engine", t, func() {
		approachingDB := &internalfakes.FakeApproachingDB{}

		engine := percy.NewEngine(percy.EngineConfig{
			ApproachingDB: approachingDB,
		})

		ctx := context.Background()
		channelID := "1234567890"

		approaching := percy.Approaching{
			ChannelID:               channelID,
			ID:                      "asdfdasfasdfdsaf",
			RateLimited:             true,
			ExpiresAt:               time.Now(),
			StartedAt:               time.Now(),
			Participants:            []string{channelID},
			NumberOfTimesShown:      400,
			TimeShownPerHour:        time.Minute,
			CreatorColor:            "green",
			UpdatedAt:               time.Now(),
			Goal:                    20,
			EventsRemainingDuration: map[int]time.Duration{1: time.Second},
		}

		Convey("updates approaching in DB", func() {
			approachingDB.SetApproachingReturns(nil)

			err := engine.UpdateHypeTrainApproaching(ctx, approaching)

			So(err, ShouldBeNil)
		})

		Convey("rejects bad updates to approaching in DB (channel id)", func() {
			approachingCopy := approaching
			approachingCopy.ChannelID = ""
			err := engine.UpdateHypeTrainApproaching(ctx, approachingCopy)

			So(err, ShouldNotBeNil)
		})

		Convey("rejects bad updates to approaching in DB (hype train id)", func() {
			approachingCopy := approaching
			approachingCopy.ID = ""
			err := engine.UpdateHypeTrainApproaching(ctx, approachingCopy)

			So(err, ShouldNotBeNil)
		})

		Convey("updates approaching in DB pass through errors to top level", func() {
			approachingDB.SetApproachingReturns(errors.New("blah"))

			err := engine.UpdateHypeTrainApproaching(ctx, approaching)

			So(err, ShouldNotBeNil)
		})
	})
}

func TestEngine_GetHypeTrainApproaching(t *testing.T) {
	Convey("Given an Engine", t, func() {
		approachingDB := &internalfakes.FakeApproachingDB{}

		engine := percy.NewEngine(percy.EngineConfig{
			ApproachingDB: approachingDB,
		})

		ctx := context.Background()
		channelID := "1234567890"

		approaching := percy.Approaching{
			ChannelID:               channelID,
			ID:                      "asdfdasfasdfdsaf",
			RateLimited:             true,
			ExpiresAt:               time.Now(),
			StartedAt:               time.Now(),
			Participants:            []string{channelID},
			NumberOfTimesShown:      400,
			TimeShownPerHour:        time.Minute,
			CreatorColor:            "green",
			UpdatedAt:               time.Now(),
			Goal:                    20,
			EventsRemainingDuration: map[int]time.Duration{1: time.Second},
		}

		Convey("get approaching in DB", func() {
			approachingDB.GetApproachingReturns(&approaching, nil)

			entry, err := engine.GetHypeTrainApproaching(ctx, channelID)

			So(err, ShouldBeNil)
			So(entry, ShouldNotBeNil)
		})

		Convey("tells you when your channel id is empty", func() {
			entry, err := engine.GetHypeTrainApproaching(ctx, "")

			So(err, ShouldNotBeNil)
			So(entry, ShouldBeNil)
		})

		Convey("gives you an error when the db errors", func() {
			approachingDB.GetApproachingReturns(nil, errors.New("blah"))

			entry, err := engine.GetHypeTrainApproaching(ctx, channelID)

			So(entry, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})
	})
}
