package dart

import (
	"context"
	"errors"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dart/dartfakes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestNotificationDB_PublishCelebrationRefund(t *testing.T) {
	Convey("Given a notification DB", t, func() {
		dart := dartfakes.FakeDartWrapper{}
		db := NotificationDB{
			dartReceiver: &dart,
		}

		Convey("returns an error if", func() {
			Convey("dartReceiver.PublishNotification returns an error", func() {
				dart.PublishNotificationReturns(nil, errors.New("ERROR"))
				err := db.PublishCelebrationRefund(context.Background(), percy.PublishCelebrationRefundArgs{})
				So(err, ShouldNotBeNil)
			})
		})

		Convey("returns nil after publishing to dart", func() {
			dart.PublishNotificationReturns(nil, nil)
			err := db.PublishCelebrationRefund(context.Background(), percy.PublishCelebrationRefundArgs{})
			So(err, ShouldBeNil)
			So(dart.PublishNotificationCallCount(), ShouldEqual, 1)
		})
	})
}
