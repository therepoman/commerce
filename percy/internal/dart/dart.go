package dart

import (
	"context"
	"net/http"

	dart "code.justin.tv/amzn/TwitchDartReceiverTwirp"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/wrapper"
	"code.justin.tv/foundation/twitchclient"
	"github.com/cactus/go-statsd-client/statsd"
)

const (
	dartServiceName = "dart"
	defaultRetry    = 1
	defaultTimeout  = 1000

	// dart apis
	publishNotificationAPI = "publishNotification"

	// dart campaigns
	dartCelebrationRefundID = "celebration_explicit_purchase_refund"
)

//go:generate counterfeiter . dartWrapper
type dartWrapper interface {
	dart.Receiver
}

type NotificationDB struct {
	dartReceiver dartWrapper
}

func NewNotificationDB(dartEndpoint string, stats statsd.Statter, socksAddr string) (*NotificationDB, error) {
	dartReceiver := dart.NewReceiverProtobufClient(dartEndpoint, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Stats:          stats,
		StatNamePrefix: dartServiceName,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(socksAddr),
			wrapper.NewRetryRoundTripWrapper(stats, dartServiceName, defaultRetry),
			wrapper.NewHystrixRoundTripWrapper(dartServiceName, defaultTimeout),
		},
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: 200,
		},
	}))

	return &NotificationDB{
		dartReceiver: dartReceiver,
	}, nil
}

func (db *NotificationDB) PublishCelebrationRefund(ctx context.Context, args percy.PublishCelebrationRefundArgs) error {
	metadata := map[string]string{
		"user_id":     args.BenefactorUserID,
		"channel_id":  args.RecipientUserID,
		"celebration": args.Celebration,
	}

	reqCtx := wrapper.GenerateRequestContext(ctx, dartServiceName, publishNotificationAPI)
	_, err := db.dartReceiver.PublishNotification(reqCtx, &dart.PublishNotificationRequest{
		Recipient:                      &dart.PublishNotificationRequest_RecipientId{RecipientId: args.BenefactorUserID},
		NotificationType:               dartCelebrationRefundID,
		Metadata:                       metadata,
		DropNotificationBeforeDelivery: false,
	})

	return err
}

func (db *NotificationDB) PublishCreatorAnniversaryNotification(ctx context.Context, args percy.PublishCreatorAnniversaryNotificationArgs) (string, error) {
	metadata := map[string]string{
		"channel_id": args.ChannelID,
		"status":     args.CreatorType,
		"years":      args.Years,
	}

	req := &dart.PublishNotificationRequest{
		Recipient:        &dart.PublishNotificationRequest_RecipientId{RecipientId: args.ChannelID},
		Metadata:         metadata,
		NotificationType: args.NotificationType,
	}

	reqCtx := wrapper.GenerateRequestContext(ctx, dartServiceName, publishNotificationAPI)
	resp, err := db.dartReceiver.PublishNotification(reqCtx, req)
	if err != nil {
		return "", err
	}

	return resp.NotificationTraceId, nil
}
