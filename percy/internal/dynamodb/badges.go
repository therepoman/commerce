package dynamodb

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/gogogadget/pointers"
	percy "code.justin.tv/commerce/percy/internal"
	"github.com/aws/aws-dax-go/dax"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
)

const (
	badgesDBFormat               = "badges-%s"
	badgesDBChannelIDIndexFormat = "badges-channel-id-index-%s"

	badgesHashKey  = "user_id"
	badgesRangeKey = "channel_id"

	queryPageSize   = 100
	deleteBatchSize = 25
)

type badgesRecord struct {
	UserID    string   `dynamodbav:"user_id"`
	ChannelID string   `dynamodbav:"channel_id"`
	Badges    []string `dynamodbav:"badges"`
}

type BadgesDB struct {
	tableName          string
	channelIDIndexName string
	client             dynamodbiface.DynamoDBAPI
}

func NewBadgesDB(config DynamoDBConfig) (*BadgesDB, error) {
	if config.Endpoint == nil {
		// no endpoint is provided, use the DynamoDB client
		sess := session.Must(session.NewSessionWithOptions(session.Options{
			Config: aws.Config{Region: aws.String(config.Region)},
		}))

		return &BadgesDB{
			client:             dynamodb.New(sess),
			tableName:          fmt.Sprintf(badgesDBFormat, config.Environment),
			channelIDIndexName: fmt.Sprintf(badgesDBChannelIDIndexFormat, config.Environment),
		}, nil

	} else {
		// endpoint provided, use DAX client
		daxConfig := dax.DefaultConfig()
		daxConfig.HostPorts = []string{*config.Endpoint}
		daxConfig.Region = config.Region
		daxConfig.ReadRetries = daxReadRetries
		daxConfig.WriteRetries = daxWriteRetries
		daxConfig.RequestTimeout = daxTimeout

		daxClient, err := dax.New(daxConfig)
		if err != nil {
			return nil, err
		}

		return &BadgesDB{
			client:             daxClient,
			tableName:          fmt.Sprintf(badgesDBFormat, config.Environment),
			channelIDIndexName: fmt.Sprintf(badgesDBChannelIDIndexFormat, config.Environment),
		}, nil
	}
}

func (db *BadgesDB) GetBadges(ctx context.Context, userID, channelID string) ([]percy.Badge, error) {
	resp, err := db.client.GetItemWithContext(ctx, &dynamodb.GetItemInput{
		TableName: aws.String(db.tableName),
		Key: map[string]*dynamodb.AttributeValue{
			badgesHashKey:  {S: aws.String(userID)},
			badgesRangeKey: {S: aws.String(channelID)},
		},
	})
	if err != nil {
		return nil, err
	}
	if resp == nil || resp.Item == nil {
		return nil, nil
	}

	var record badgesRecord
	if err := dynamodbattribute.UnmarshalMap(resp.Item, &record); err != nil {
		return nil, errors.Wrapf(err, "error unmarshaling badges for user %s channel %s", userID, channelID)
	}

	badges := make([]percy.Badge, len(record.Badges))
	for i, entry := range record.Badges {
		badges[i] = convertDynamoBadgeEntry(entry)
	}

	return badges, nil
}

func (db *BadgesDB) GetAllBadgeChannelsForUserID(ctx context.Context, userID string, cursor string) ([]string, string, error) {
	if len(userID) == 0 {
		return nil, "", errors.New("empty user ID")
	}

	expr, err := expression.NewBuilder().WithKeyCondition(
		expression.KeyEqual(expression.Key(badgesHashKey), expression.Value(userID)),
	).Build()
	if err != nil {
		return nil, "", errors.Wrap(err, "failed to build query expression")
	}

	queryInput := &dynamodb.QueryInput{
		TableName:                 aws.String(db.tableName),
		Limit:                     aws.Int64(queryPageSize),
		KeyConditionExpression:    expr.KeyCondition(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	if len(cursor) != 0 {
		queryInput.ExclusiveStartKey = map[string]*dynamodb.AttributeValue{
			badgesHashKey: {
				S: aws.String(cursor),
			},
		}
	}

	resp, err := db.client.QueryWithContext(ctx, queryInput)
	if err != nil {
		return nil, "", errors.Wrapf(err, "failed to query badges for user ID %s", userID)
	}

	if resp == nil || len(resp.Items) == 0 {
		return nil, "", nil
	}

	var records []badgesRecord
	if err := dynamodbattribute.UnmarshalListOfMaps(resp.Items, &records); err != nil {
		return nil, "", errors.Wrapf(err, "failed to unmarshal badges records for user ID %s ", userID)
	}

	channels := make([]string, len(records))
	for i, record := range records {
		channels[i] = record.ChannelID
	}

	nextCursor := ""
	if len(resp.LastEvaluatedKey) != 0 {
		nextCursor = pointers.StringOrDefault(resp.LastEvaluatedKey[badgesHashKey].S, "")
	}

	return channels, nextCursor, nil
}

func (db *BadgesDB) GetAllBadgeUsersForChannelID(ctx context.Context, channelID string, cursor string) ([]string, string, error) {
	if len(channelID) == 0 {
		return nil, "", errors.New("empty channel ID")
	}

	expr, err := expression.NewBuilder().WithKeyCondition(
		expression.KeyEqual(expression.Key(badgesRangeKey), expression.Value(channelID)),
	).Build()
	if err != nil {
		return nil, "", errors.Wrap(err, "failed to build query expression")
	}

	queryInput := &dynamodb.QueryInput{
		TableName:                 aws.String(db.tableName),
		IndexName:                 aws.String(db.channelIDIndexName),
		Limit:                     aws.Int64(queryPageSize),
		KeyConditionExpression:    expr.KeyCondition(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	if len(cursor) != 0 {
		queryInput.ExclusiveStartKey = map[string]*dynamodb.AttributeValue{
			badgesRangeKey: {
				S: aws.String(cursor),
			},
		}
	}

	resp, err := db.client.QueryWithContext(ctx, queryInput)
	if err != nil {
		return nil, "", errors.Wrapf(err, "failed to query badges for channel ID %s", channelID)
	}

	if resp == nil || len(resp.Items) == 0 {
		return nil, "", nil
	}

	var records []badgesRecord
	if err := dynamodbattribute.UnmarshalListOfMaps(resp.Items, &records); err != nil {
		return nil, "", errors.Wrapf(err, "failed to unmarshal badges records for channel ID %s ", channelID)
	}

	users := make([]string, len(records))
	for i, record := range records {
		users[i] = record.UserID
	}

	nextCursor := ""
	if len(resp.LastEvaluatedKey) != 0 {
		nextCursor = pointers.StringOrDefault(resp.LastEvaluatedKey[badgesRangeKey].S, "")
	}

	return users, nextCursor, nil
}

func (db *BadgesDB) SetBadges(ctx context.Context, userID, channelID string, badges []percy.Badge) error {
	dynamoBadges := make([]string, len(badges))
	for i, badge := range badges {
		dynamoBadges[i] = convertToDynamoBadgeEntry(badge)
	}

	marshaledBadge, err := dynamodbattribute.MarshalMap(&badgesRecord{
		UserID:    userID,
		ChannelID: channelID,
		Badges:    dynamoBadges,
	})
	if err != nil {
		return errors.New("failed to marshal badge dynamoDB entry")
	}

	_, err = db.client.PutItemWithContext(ctx, &dynamodb.PutItemInput{
		TableName: aws.String(db.tableName),
		Item:      marshaledBadge,
	})

	return err
}

func (db *BadgesDB) SetBulkBadges(ctx context.Context, userIDs []string, channelID string, badges []percy.Badge) error {
	requestedItems := map[string][]*dynamodb.WriteRequest{
		db.tableName: {},
	}
	badgesAsString := make([]string, 0)
	for _, badge := range badges {
		badgesAsString = append(badgesAsString, string(badge))
	}

	for _, userID := range userIDs {
		putItemInput, err := dynamodbattribute.MarshalMap(&badgesRecord{
			UserID:    userID,
			ChannelID: channelID,
			Badges:    badgesAsString,
		})
		if err != nil {
			return errors.Wrap(err, "Failed to marshal badge dynamoDB entry for bulk update")
		}

		requestedItems[db.tableName] = append(requestedItems[db.tableName], &dynamodb.WriteRequest{
			PutRequest: &dynamodb.PutRequest{
				Item: putItemInput,
			},
		})
	}

	resp, err := db.client.BatchWriteItemWithContext(ctx, &dynamodb.BatchWriteItemInput{
		RequestItems: requestedItems,
	})
	if err != nil {
		return errors.Wrap(err, "Failed to batch write badge dynamoDB entries")
	}

	if len(resp.UnprocessedItems) > 0 {
		return errors.New("Failed to process all items while writing to badges dynamoDB")
	}

	return nil
}

func (db *BadgesDB) DeleteBulkBadges(ctx context.Context, badgesToDelete map[string][]string) error {
	if !isValidBatchDeleteInput(badgesToDelete) {
		return errors.New("invalid input to delete badges")
	}

	requestedItems := make([]*dynamodb.WriteRequest, 0)

	for userID, channelIDs := range badgesToDelete {
		for _, channelID := range channelIDs {
			deleteItemInput, err := dynamodbattribute.MarshalMap(&badgesRecord{
				UserID:    userID,
				ChannelID: channelID,
			})
			if err != nil {
				return errors.Wrap(err, "Failed to marshal badge dynamoDB entry for bulk delete")
			}

			requestedItems = append(requestedItems, &dynamodb.WriteRequest{
				DeleteRequest: &dynamodb.DeleteRequest{
					Key: deleteItemInput,
				},
			})
		}
	}

	errs, egCtx := errgroup.WithContext(ctx)

	for i := 0; i <= len(requestedItems)/deleteBatchSize; i++ {
		start := i * deleteBatchSize
		end := start + deleteBatchSize
		if end > len(requestedItems) {
			end = len(requestedItems)
		}

		if start == end {
			break
		}

		batch := requestedItems[start:end]

		errs.Go(func() error {
			resp, err := db.client.BatchWriteItemWithContext(egCtx, &dynamodb.BatchWriteItemInput{
				RequestItems: map[string][]*dynamodb.WriteRequest{
					db.tableName: batch,
				},
			})
			if err != nil {
				return errors.Wrap(err, "Failed to delete badge records")
			}

			if resp != nil && len(resp.UnprocessedItems) > 0 {
				return errors.New("Failed to process all items while deleting badge records")
			}

			return nil
		})
	}

	if err := errs.Wait(); err != nil {
		return err
	}

	return nil
}

func convertToDynamoBadgeEntry(badge percy.Badge) string {
	return string(badge)
}

func convertDynamoBadgeEntry(entry string) percy.Badge {
	switch entry {
	case "current_conductor":
		return percy.CurrentConductorBadge
	case "former_conductor":
		return percy.FormerConductorBadge
	default:
		return percy.UnknownBadge
	}
}

func isValidBatchDeleteInput(badgesToDelete map[string][]string) bool {
	if len(badgesToDelete) == 0 {
		return false
	}

	validPairs := map[string]interface{}{}
	for userID, channelIDs := range badgesToDelete {
		for _, channelID := range channelIDs {
			if _, exists := validPairs[userID+channelID]; exists {
				return false
			}
			validPairs[userID+channelID] = nil
		}
	}

	return true
}
