package dynamodb

import (
	"context"
	"fmt"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"github.com/aws/aws-dax-go/dax"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/pkg/errors"
)

type BitsPinataDB struct {
	tableName string
	client    dynamodbiface.DynamoDBAPI
}

const (
	bitsPinataDBFormat            = "bits-pinata-%s"
	bitsPinataDBPrimaryKey        = "channel_id"
	bitsPinataDBSortKey           = "created_at"
	BitsPinataContributionTimeout = time.Second * 210

	bitsPinataTTLDuration = time.Hour * 24 * 30 // data expires after 30 days
)

type bitsPinata struct {
	ChannelID        string    `dynamodbav:"channel_id"`
	CreatedAt        time.Time `dynamodbav:"created_at"`
	PinataID         string    `dynamodbav:"pinata_id"`
	TimerID          string    `dynamodbav:"timer_id"`
	StreamID         string    `dynamodbav:"stream_id"`
	BitsToBreak      int64     `dynamodbav:"bits_to_break"`
	BitsContributed  int64     `dynamodbav:"bits_contributed"`
	LastContribution time.Time `dynamodbav:"last_contribution"`
	TTL              int64     `dynamodbav:"ttl"`
}

func NewBitsPinataDB(config DynamoDBConfig) (*BitsPinataDB, error) {
	if config.Endpoint == nil {
		// no endpoint is provided, use the DynamoDB client
		sess := session.Must(session.NewSessionWithOptions(session.Options{
			Config: aws.Config{Region: aws.String(config.Region)},
		}))

		return &BitsPinataDB{
			client:    dynamodb.New(sess),
			tableName: fmt.Sprintf(bitsPinataDBFormat, config.Environment),
		}, nil

	} else {
		// endpoint provided, use DAX client
		daxConfig := dax.DefaultConfig()
		daxConfig.HostPorts = []string{*config.Endpoint}
		daxConfig.Region = config.Region
		daxConfig.ReadRetries = daxReadRetries
		daxConfig.WriteRetries = daxWriteRetries
		daxConfig.RequestTimeout = daxTimeout

		daxClient, err := dax.New(daxConfig)
		if err != nil {
			return nil, err
		}

		return &BitsPinataDB{
			client:    daxClient,
			tableName: fmt.Sprintf(bitsPinataDBFormat, config.Environment),
		}, nil
	}
}

func (db *BitsPinataDB) CreateBitsPinata(ctx context.Context, bitsPinata percy.BitsPinata) error {
	if len(bitsPinata.ChannelID) == 0 {
		return errors.New("channel ID is blank")
	}

	marshaledPinata, err := dynamodbattribute.MarshalMap(toDBBitsPinata(bitsPinata))
	if err != nil {
		return errors.Wrap(err, "failed to marshal bits pinata entry")
	}

	_, err = db.client.PutItemWithContext(ctx,
		&dynamodb.PutItemInput{
			TableName: aws.String(db.tableName),
			Item:      marshaledPinata,
		},
	)

	return err
}

// Given a channel_id, return the BitsPinata if it exists, nil otherwise
func (db *BitsPinataDB) GetActiveBitsPinata(ctx context.Context, channelID string) (*percy.BitsPinata, error) {
	if len(channelID) == 0 {
		return nil, errors.New("channel ID is blank")
	}

	expr, err := expression.NewBuilder().WithKeyCondition(
		expression.KeyEqual(expression.Key(bitsPinataDBPrimaryKey), expression.Value(channelID)),
	).Build()
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query expression")
	}
	resp, err := db.client.QueryWithContext(ctx, &dynamodb.QueryInput{
		TableName:                 aws.String(db.tableName),
		KeyConditionExpression:    expr.KeyCondition(),
		ExpressionAttributeValues: expr.Values(),
		ExpressionAttributeNames:  expr.Names(),
		ScanIndexForward:          aws.Bool(false),
		Limit:                     aws.Int64(1),
	})
	if err != nil {
		return nil, err
	}
	if resp == nil || len(resp.Items) == 0 {
		return nil, nil
	}

	var dbBitsPinata bitsPinata
	if err := dynamodbattribute.UnmarshalMap(resp.Items[0], &dbBitsPinata); err != nil {
		return nil, errors.Wrapf(err, "error unmarshaling bits pinata with channelID #{channelID}")
	}

	pinata := fromDBBitsPinata(dbBitsPinata)

	// Check if most recent pinata is still active
	expiresAt := time.Now().UTC().Add(-1 * BitsPinataContributionTimeout)
	if pinata.BitsContributed >= pinata.BitsToBreak || pinata.LastContribution.Before(expiresAt) {
		return nil, nil
	}

	return &pinata, nil
}

// Given a channel id and number of bits contributed, update the BitsPinata if it exists
func (db *BitsPinataDB) AddBitsPinataContribution(ctx context.Context, channelID string, bitsContributed int) (*percy.BitsPinata, error) {
	if len(channelID) == 0 {
		return nil, errors.New("channel ID is blank")
	}

	// Get active pinata for this channel
	pinata, err := db.GetActiveBitsPinata(ctx, channelID)
	if err != nil {
		return nil, err
	}
	if pinata == nil {
		return nil, nil
	}

	expr, err := expression.NewBuilder().WithUpdate(contributionUpdateExpression(bitsContributed)).Build()
	if err != nil {
		return nil, errors.Wrap(err, "building expression for update")
	}

	input := dynamodb.UpdateItemInput{
		TableName: aws.String(db.tableName),
		Key: map[string]*dynamodb.AttributeValue{
			bitsPinataDBPrimaryKey: {S: aws.String(pinata.ChannelID)},
			bitsPinataDBSortKey:    {S: aws.String(toDBTime(pinata.CreatedAt))},
		},
		UpdateExpression:          expr.Update(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ReturnValues:              aws.String(dynamodb.ReturnValueAllNew),
	}

	output, err := db.client.UpdateItemWithContext(ctx, &input)
	if err != nil {
		return nil, errors.Wrap(err, "update Bits Pinata with contribution")
	}
	if output == nil {
		return nil, errors.New("no output was received")
	}

	var dbBitsPinata bitsPinata
	if err := dynamodbattribute.UnmarshalMap(output.Attributes, &dbBitsPinata); err != nil {
		return nil, errors.Wrapf(err, "error unmarshaling bits pinata with channelID #{channelID}")
	}

	updatedPinata := fromDBBitsPinata(dbBitsPinata)

	return &updatedPinata, nil
}

func contributionUpdateExpression(bitsContributed int) expression.UpdateBuilder {
	return expression.Add(
		expression.Name("bits_contributed"),
		expression.Value(bitsContributed),
	).Set(
		expression.Name("last_contribution"),
		expression.Value(toDBTime(time.Now().UTC())),
	).Set(
		expression.Name("ttl"),
		expression.Value(time.Now().Add(ttlDuration).Unix()),
	)
}

func toDBTime(t time.Time) string {
	return t.Format(time.RFC3339Nano)
}

func fromDBBitsPinata(bitsPinata bitsPinata) percy.BitsPinata {
	return percy.BitsPinata{
		ChannelID:        bitsPinata.ChannelID,
		CreatedAt:        bitsPinata.CreatedAt,
		PinataID:         bitsPinata.PinataID,
		TimerID:          bitsPinata.TimerID,
		StreamID:         bitsPinata.StreamID,
		BitsToBreak:      bitsPinata.BitsToBreak,
		BitsContributed:  bitsPinata.BitsContributed,
		LastContribution: bitsPinata.LastContribution,
	}
}

func toDBBitsPinata(pinata percy.BitsPinata) bitsPinata {
	return bitsPinata{
		ChannelID:        pinata.ChannelID,
		CreatedAt:        pinata.CreatedAt,
		PinataID:         pinata.PinataID,
		TimerID:          pinata.TimerID,
		StreamID:         pinata.StreamID,
		BitsToBreak:      pinata.BitsToBreak,
		LastContribution: pinata.LastContribution,
		BitsContributed:  pinata.BitsContributed,
		TTL:              time.Now().Add(bitsPinataTTLDuration).Unix(),
	}
}
