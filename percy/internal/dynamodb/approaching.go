package dynamodb

import (
	"context"
	"fmt"
	"strconv"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"github.com/aws/aws-dax-go/dax"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/pkg/errors"
)

type ApproachingDB struct {
	tableName string
	client    dynamodbiface.DynamoDBAPI
}

const (
	approachingDBFormat     = "approaching-%s"
	approachingDBPrimaryKey = "channel_id"
)

type approaching struct {
	ChannelID    string    `dynamodbav:"channel_id"`
	ID           string    `dynamodbav:"id"`
	CreatedTime  time.Time `dynamodbav:"created_time"`
	StartedAt    time.Time `dynamodbav:"started_at"`
	ExpiresAt    time.Time `dynamodbav:"expires_at"`
	UpdatedAt    time.Time `dynamodbav:"updated_at"`
	Participants []string  `dynamodbav:"participations"`
	TTL          int64     `dynamodbav:"ttl"`
	Goal         int64     `dynamodbav:"goal"`
	// dynamodb doesn't support maps with int keys, so we must convert to string
	EventsRemainingTimestamp map[string]time.Time `dynamodbav:"events_remaining_time"`
	CreatorColor             string               `dynamodbav:"creator_color"`
	TimeShownPerHour         time.Duration        `dynamodbav:"time_shown_per_hour"`
	NumberOfTimesShown       int                  `dynamodbav:"number_of_times_shown"`
	RateLimited              bool                 `dynamodbav:"rate_limited"`
	LevelOneRewards          []reward             `dynamodbav:"level_one_rewards"`
	GhostMode                bool                 `dynamodbav:"ghost_mode"`
}

func NewApproachingDB(config DynamoDBConfig) (*ApproachingDB, error) {
	if config.Endpoint == nil {
		// no endpoint is provided, use the DynamoDB client
		sess := session.Must(session.NewSessionWithOptions(session.Options{
			Config: aws.Config{Region: aws.String(config.Region)},
		}))

		return &ApproachingDB{
			client:    dynamodb.New(sess),
			tableName: fmt.Sprintf(approachingDBFormat, config.Environment),
		}, nil

	} else {
		// endpoint provided, use DAX client
		daxConfig := dax.DefaultConfig()
		daxConfig.HostPorts = []string{*config.Endpoint}
		daxConfig.Region = config.Region
		daxConfig.ReadRetries = daxReadRetries
		daxConfig.WriteRetries = daxWriteRetries
		daxConfig.RequestTimeout = daxTimeout

		daxClient, err := dax.New(daxConfig)
		if err != nil {
			return nil, err
		}

		return &ApproachingDB{
			client:    daxClient,
			tableName: fmt.Sprintf(approachingDBFormat, config.Environment),
		}, nil
	}
}

// SetApproaching sets an approaching record for a channel
func (db *ApproachingDB) SetApproaching(ctx context.Context, approaching percy.Approaching) error {
	if len(approaching.ChannelID) == 0 {
		return errors.New("channel ID is blank")
	}

	internalApproaching := toInternalApproaching(approaching)
	internalApproaching.TTL = internalApproaching.UpdatedAt.Add(time.Minute * 60).Unix()

	marshaledApproaching, err := dynamodbattribute.MarshalMap(&internalApproaching)
	if err != nil {
		return errors.New("failed to marshal approaching dynamoDB entry")
	}

	_, err = db.client.PutItemWithContext(ctx, &dynamodb.PutItemInput{
		TableName: aws.String(db.tableName),
		Item:      marshaledApproaching,
	})

	return err
}

// GetApproaching gets whether or not a HypeTrain has any previous approaching context
func (db *ApproachingDB) GetApproaching(ctx context.Context, channelID string) (*percy.Approaching, error) {
	if len(channelID) == 0 {
		return nil, errors.New("channel ID is blank")
	}

	resp, err := db.client.GetItemWithContext(ctx, &dynamodb.GetItemInput{
		TableName: aws.String(db.tableName),
		Key: map[string]*dynamodb.AttributeValue{
			approachingDBPrimaryKey: {S: aws.String(channelID)},
		},
	})
	if err != nil {
		return nil, err
	}

	if resp == nil || resp.Item == nil {
		return nil, nil
	}

	var result approaching
	if err := dynamodbattribute.UnmarshalMap(resp.Item, &result); err != nil {
		return nil, errors.Wrapf(err, "error unmarshaling approaching for channel %s", channelID)
	}

	// dynamodb sla on ttl deletion is 48 hours so don't risk it
	if result.TTL != 0 && time.Now().After(time.Unix(result.TTL, 0)) {
		return nil, nil
	}

	return result.toExternalApproaching(), nil
}

func (a *approaching) toExternalApproaching() *percy.Approaching {
	return &percy.Approaching{
		ChannelID:               a.ChannelID,
		ID:                      a.ID,
		Participants:            a.Participants,
		Goal:                    a.Goal,
		CreatorColor:            a.CreatorColor,
		EventsRemainingDuration: a.ConvertEventsRemainingDurationFromDynamoDBFormat(),
		NumberOfTimesShown:      a.NumberOfTimesShown,
		RateLimited:             a.RateLimited,
		TimeShownPerHour:        a.TimeShownPerHour,
		UpdatedAt:               a.UpdatedAt,
		StartedAt:               a.StartedAt,
		ExpiresAt:               a.ExpiresAt,
		CreatedTime:             a.CreatedTime,
		LevelOneRewards:         fromDBRewards(a.LevelOneRewards),
		GhostMode:               a.GhostMode,
	}
}

func toInternalApproaching(a percy.Approaching) approaching {
	return approaching{
		ChannelID:                a.ChannelID,
		ID:                       a.ID,
		Participants:             a.Participants,
		Goal:                     a.Goal,
		CreatorColor:             a.CreatorColor,
		EventsRemainingTimestamp: ConvertEventsRemainingDurationToDynamoDBFormat(a.UpdatedAt, a.EventsRemainingDuration),
		NumberOfTimesShown:       a.NumberOfTimesShown,
		RateLimited:              a.RateLimited,
		TimeShownPerHour:         a.TimeShownPerHour,
		UpdatedAt:                a.UpdatedAt,
		StartedAt:                a.StartedAt,
		ExpiresAt:                a.ExpiresAt,
		CreatedTime:              a.CreatedTime,
		LevelOneRewards:          toDBRewards(a.LevelOneRewards),
		GhostMode:                a.GhostMode,
	}
}

// helper function to convert to the dynamo db format because DynamoDB will fail to serialize a map with an int for the key
// converts to a timestamp based on the updated at time
func ConvertEventsRemainingDurationToDynamoDBFormat(now time.Time, eventsRemainingDuration map[int]time.Duration) map[string]time.Time {
	conversion := map[string]time.Time{}

	for k, v := range eventsRemainingDuration {
		conversion[strconv.Itoa(k)] = now.Add(v)
	}

	return conversion
}

// helper function because DynamoDB will fail to serialize a map with an int for the key
func (a *approaching) ConvertEventsRemainingDurationFromDynamoDBFormat() map[int]time.Duration {
	conversion := map[int]time.Duration{}
	now := time.Now()

	for k, v := range a.EventsRemainingTimestamp {
		i, _ := strconv.Atoi(k)
		remainingDuration := v.Sub(now)
		if remainingDuration <= 0 {
			continue
		}
		conversion[i] = remainingDuration
	}

	return conversion
}
