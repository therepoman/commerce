package dynamodb

import (
	"context"
	"fmt"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"github.com/aws/aws-dax-go/dax"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/pkg/errors"
)

const (
	celebrationChannelPurchasableConfigDBFormat = "celebrations-channel-purchasable-configs-%s"

	celebrationChannelPurchasableConfigHashKey = "channel_id"
)

type CelebrationChannelPurchasableConfigRecord struct {
	ChannelID      string                                               `dynamodbav:"channel_id"`
	PurchaseConfig map[percy.CelebrationSize]*CelebrationPurchaseConfig `dynamodbav:"purchase_config"`
	LastUpdated    *time.Time                                           `dynamodbav:"last_updated,omitempty"`
}

type CelebrationPurchaseConfig struct {
	IsDisabled *bool   `dynamodbav:"is_disabled"`
	OfferID    *string `dynamodbav:"offer_id"`
}

type CelebrationChannelPurchasableConfigDB struct {
	tableName string
	client    dynamodbiface.DynamoDBAPI
}

func NewCelebrationChannelPurchasableConfigDB(config DynamoDBConfig) (*CelebrationChannelPurchasableConfigDB, error) {
	if config.Endpoint == nil {
		// no endpoint is provided, use the DynamoDB client
		sess := session.Must(session.NewSessionWithOptions(session.Options{
			Config: aws.Config{Region: aws.String(config.Region)},
		}))

		return &CelebrationChannelPurchasableConfigDB{
			client:    dynamodb.New(sess),
			tableName: fmt.Sprintf(celebrationChannelPurchasableConfigDBFormat, config.Environment),
		}, nil

	} else {
		// endpoint provided, use DAX client
		daxConfig := dax.DefaultConfig()
		daxConfig.HostPorts = []string{*config.Endpoint}
		daxConfig.Region = config.Region
		daxConfig.ReadRetries = daxReadRetries
		daxConfig.WriteRetries = daxWriteRetries
		daxConfig.RequestTimeout = daxTimeout

		daxClient, err := dax.New(daxConfig)
		if err != nil {
			return nil, err
		}

		return &CelebrationChannelPurchasableConfigDB{
			client:    daxClient,
			tableName: fmt.Sprintf(celebrationChannelPurchasableConfigDBFormat, config.Environment),
		}, nil
	}
}

func getChannelPurchasableConfigKey(channelID string) map[string]*dynamodb.AttributeValue {
	return map[string]*dynamodb.AttributeValue{
		celebrationChannelPurchasableConfigHashKey: {S: aws.String(channelID)},
	}
}

func (db *CelebrationChannelPurchasableConfigDB) GetConfig(ctx context.Context, channelID string) (*percy.CelebrationChannelPurchasableConfig, error) {
	if len(channelID) == 0 {
		return nil, errors.New("channel ID is blank")
	}

	configs, err := db.batchGetConfigs(ctx, getChannelPurchasableConfigKey(globalChannelID), getChannelPurchasableConfigKey(channelID))
	if err != nil {
		return nil, err
	}

	channelPurchasableConfig := mergeCelebrationChannelPurchasableConfigs(channelID, configs...)

	return &channelPurchasableConfig, nil
}

func (db *CelebrationChannelPurchasableConfigDB) batchGetConfigs(ctx context.Context, keys ...map[string]*dynamodb.AttributeValue) ([]CelebrationChannelPurchasableConfigRecord, error) {
	output, err := db.client.BatchGetItemWithContext(ctx, &dynamodb.BatchGetItemInput{
		RequestItems: map[string]*dynamodb.KeysAndAttributes{
			db.tableName: {
				Keys: keys,
			},
		},
	})
	if err != nil {
		return nil, err
	}

	if output == nil {
		return nil, errors.New("BatchGetItem request received nil output from DynamoDB")
	}

	var results []CelebrationChannelPurchasableConfigRecord
	err = dynamodbattribute.UnmarshalListOfMaps(output.Responses[db.tableName], &results)
	return results, err
}

func (db *CelebrationChannelPurchasableConfigDB) UpdateConfig(ctx context.Context, channelID string, update percy.CelebrationChannelPurchasableConfigUpdate) (*percy.CelebrationChannelPurchasableConfig, error) {
	if len(channelID) == 0 {
		return nil, errors.New("channel ID is blank")
	}

	configs, err := db.batchGetConfigs(ctx, getChannelPurchasableConfigKey(globalChannelID), getChannelPurchasableConfigKey(channelID))
	if err != nil {
		return nil, err
	}

	var globalConfig CelebrationChannelPurchasableConfigRecord
	var customConfig *CelebrationChannelPurchasableConfigRecord

	for i, config := range configs {
		switch config.ChannelID {
		case globalChannelID:
			globalConfig = configs[i]
		default:
			customConfig = &configs[i]
		}
	}

	updatedConfig, err := db.updateConfig(ctx, channelID, customConfig, update)
	if err != nil {
		return nil, err
	}

	updatedMergedConfig := mergeCelebrationChannelPurchasableConfigs(channelID, []CelebrationChannelPurchasableConfigRecord{globalConfig, updatedConfig}...)
	return &updatedMergedConfig, nil
}

func (db *CelebrationChannelPurchasableConfigDB) updateConfig(ctx context.Context, channelID string, config *CelebrationChannelPurchasableConfigRecord, update percy.CelebrationChannelPurchasableConfigUpdate) (CelebrationChannelPurchasableConfigRecord, error) {
	var channelConfig CelebrationChannelPurchasableConfigRecord

	if config != nil {
		channelConfig = *config
	} else {
		channelConfig = CelebrationChannelPurchasableConfigRecord{
			ChannelID:      channelID,
			PurchaseConfig: map[percy.CelebrationSize]*CelebrationPurchaseConfig{},
		}
	}

	for productToUpdate, updateConfig := range update.PurchaseConfig {
		purchaseConfig := channelConfig.PurchaseConfig[productToUpdate]
		if purchaseConfig == nil {
			purchaseConfig = &CelebrationPurchaseConfig{}
		}
		if updateConfig.IsDisabled != nil {
			purchaseConfig.IsDisabled = updateConfig.IsDisabled
		}
		if updateConfig.OfferID != nil {
			purchaseConfig.OfferID = updateConfig.OfferID
		}
		channelConfig.PurchaseConfig[productToUpdate] = purchaseConfig
	}

	marshaledCelebrationChannelPurchaseConfig, err := dynamodbattribute.MarshalMap(&channelConfig)
	if err != nil {
		return CelebrationChannelPurchasableConfigRecord{}, errors.New("failed to marshal celebration channel purchase config dynamoDB entry")
	}

	_, err = db.client.PutItemWithContext(ctx, &dynamodb.PutItemInput{
		TableName: aws.String(db.tableName),
		Item:      marshaledCelebrationChannelPurchaseConfig,
	})
	if err != nil {
		return CelebrationChannelPurchasableConfigRecord{}, err
	}

	return channelConfig, nil
}

func mergeCelebrationChannelPurchasableConfigs(channelID string, configs ...CelebrationChannelPurchasableConfigRecord) percy.CelebrationChannelPurchasableConfig {
	var custom, globalDefault *CelebrationChannelPurchasableConfigRecord

	for i, config := range configs {
		switch config.ChannelID {
		case globalChannelID:
			globalDefault = &configs[i]
		default:
			custom = &configs[i]
		}
	}

	channelConfig := percy.CelebrationChannelPurchasableConfig{
		ChannelID:      channelID,
		PurchaseConfig: map[percy.CelebrationSize]percy.CelebrationPurchaseConfig{},
	}

	for _, config := range []*CelebrationChannelPurchasableConfigRecord{globalDefault, custom} {
		if config == nil {
			continue
		}

		if config.PurchaseConfig != nil {
			for productID, purchaseConfig := range config.PurchaseConfig {
				channelPurchaseConfig := channelConfig.PurchaseConfig[productID]
				if purchaseConfig.IsDisabled != nil {
					channelPurchaseConfig.IsDisabled = *purchaseConfig.IsDisabled
				}
				if purchaseConfig.OfferID != nil {
					channelPurchaseConfig.OfferID = *purchaseConfig.OfferID
				}
				channelConfig.PurchaseConfig[productID] = channelPurchaseConfig
			}
		}
	}

	return channelConfig
}
