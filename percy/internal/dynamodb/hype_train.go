package dynamodb

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	"github.com/aws/aws-dax-go/dax"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/pkg/errors"
)

const (
	hypeTrainDBFormat          = "hype-train-%s"
	hypeTrainDBPrimaryKey      = "channel_id"
	hypeTrainSortKey           = "instance"
	hypeTrainIDIndexName       = "hype-train-id-index"
	hypeTrainIDIndexPrimaryKey = "id"
	mostRecentDelimiter        = "0"

	ttlDuration = time.Hour * 24 * 90 // data should be expired after 90 days
)

// condition to check if a hype train ongoing
var isActiveCondition = expression.Name("instance").AttributeExists().And(expression.Name("ended_at").AttributeNotExists())

type conductorsMap map[string]conductor

func (m conductorsMap) MarshalDynamoDBAttributeValue(av *dynamodb.AttributeValue) error {
	av.M = map[string]*dynamodb.AttributeValue{}

	for source, conductor := range m {
		dbConductor, err := dynamodbattribute.MarshalMap(conductor)
		if err != nil {
			return err
		}

		av.M[source] = &dynamodb.AttributeValue{
			M: dbConductor,
		}
	}

	return nil
}

type participationsMap map[string]int

func (m participationsMap) MarshalDynamoDBAttributeValue(av *dynamodb.AttributeValue) error {
	av.M = map[string]*dynamodb.AttributeValue{}

	for identifier, quantity := range m {
		av.M[identifier] = &dynamodb.AttributeValue{
			N: pointers.StringP(strconv.Itoa(quantity)),
		}
	}

	return nil
}

type hypeTrain struct {
	ChannelID      string            `dynamodbav:"channel_id"`
	Instance       string            `dynamodbav:"instance"`
	ID             string            `dynamodbav:"id"`
	StartedAt      time.Time         `dynamodbav:"started_at"`
	ExpiresAt      time.Time         `dynamodbav:"expires_at"`
	UpdatedAt      time.Time         `dynamodbav:"updated_at"`
	EndedAt        *time.Time        `dynamodbav:"ended_at,omitempty"`
	EndingReason   *string           `dynamodbav:"ending_reason,omitempty"`
	Conductors     conductorsMap     `dynamodbav:"conductors"`
	Participations participationsMap `dynamodbav:"participations"`
	Config         hypeTrainConfig   `dynamodbav:"config"`
	TTL            int64             `dynamodbav:"ttl"`
}

type conductor struct {
	Source         string         `dynamodbav:"source"`
	UserID         string         `dynamodbav:"user_id"`
	Participations map[string]int `dynamodbav:"participations"`
}

type HypeTrainDB struct {
	tableName string
	client    dynamodbiface.DynamoDBAPI
}

func NewHypeTrainDB(config DynamoDBConfig) (*HypeTrainDB, error) {
	if config.Endpoint == nil {
		// no endpoint is provided, use the DynamoDB client
		sess := session.Must(session.NewSessionWithOptions(session.Options{
			Config: aws.Config{Region: aws.String(config.Region)},
		}))

		return &HypeTrainDB{
			client:    dynamodb.New(sess),
			tableName: fmt.Sprintf(hypeTrainDBFormat, config.Environment),
		}, nil

	} else {
		// endpoint provided, use DAX client
		daxConfig := dax.DefaultConfig()
		daxConfig.HostPorts = []string{*config.Endpoint}
		daxConfig.Region = config.Region
		daxConfig.ReadRetries = daxReadRetries
		daxConfig.WriteRetries = daxWriteRetries
		daxConfig.RequestTimeout = daxTimeout

		daxClient, err := dax.New(daxConfig)
		if err != nil {
			return nil, err
		}

		return &HypeTrainDB{
			client:    daxClient,
			tableName: fmt.Sprintf(hypeTrainDBFormat, config.Environment),
		}, nil
	}
}

// Given a channel_id and an instance, return the HypeTrain if exists, nil otherwise.
func (db *HypeTrainDB) GetMostRecentHypeTrain(ctx context.Context, channelID string) (*percy.HypeTrain, error) {
	if len(channelID) == 0 {
		return nil, errors.New("channel ID is blank")
	}

	resp, err := db.client.GetItemWithContext(ctx, &dynamodb.GetItemInput{
		TableName: aws.String(db.tableName),
		Key: map[string]*dynamodb.AttributeValue{
			hypeTrainDBPrimaryKey: {S: aws.String(channelID)},
			hypeTrainSortKey:      {S: aws.String(mostRecentDelimiter)},
		},
	})
	if err != nil {
		return nil, err
	}
	if resp == nil || resp.Item == nil {
		return nil, nil
	}

	var hypeTrain hypeTrain
	if err := dynamodbattribute.UnmarshalMap(resp.Item, &hypeTrain); err != nil {
		return nil, errors.Wrapf(err, "error unmarshaling hype train for channel %s", channelID)
	}

	var endingReason *percy.EndingReason
	if hypeTrain.EndingReason != nil {
		// taking address of `reason` so that endingReason is a nil pointer instead of a pointer to ""
		reason := percy.EndingReason(*hypeTrain.EndingReason)
		endingReason = &reason
	}

	return &percy.HypeTrain{
		ChannelID:      hypeTrain.ChannelID,
		ID:             hypeTrain.ID,
		StartedAt:      hypeTrain.StartedAt,
		ExpiresAt:      hypeTrain.ExpiresAt,
		UpdatedAt:      hypeTrain.UpdatedAt,
		EndedAt:        hypeTrain.EndedAt,
		EndingReason:   endingReason,
		Conductors:     fromDBConductors(hypeTrain.Conductors),
		Participations: fromDBParticipations(hypeTrain.Participations),
		Config:         fromDBConfig(hypeTrain.Config),
	}, nil
}

func (db *HypeTrainDB) GetHypeTrainByID(ctx context.Context, id string) (*percy.HypeTrain, error) {
	if len(id) == 0 {
		return nil, errors.New("empty hype train ID")
	}

	expr, err := expression.NewBuilder().WithKeyCondition(
		expression.KeyEqual(expression.Key(hypeTrainIDIndexPrimaryKey), expression.Value(id)),
	).Build()
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query expression")
	}

	resp, err := db.client.QueryWithContext(ctx, &dynamodb.QueryInput{
		TableName:                 aws.String(db.tableName),
		IndexName:                 aws.String(hypeTrainIDIndexName),
		KeyConditionExpression:    expr.KeyCondition(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	})
	if err != nil {
		return nil, err
	}
	if resp == nil || len(resp.Items) == 0 {
		return nil, nil
	}

	var dbHypeTrain hypeTrain
	if err := dynamodbattribute.UnmarshalMap(resp.Items[0], &dbHypeTrain); err != nil {
		return nil, errors.Wrapf(err, "error unmarshaling hype train with ID %s", id)
	}

	train := fromDBHypeTrain(dbHypeTrain)
	return &train, nil
}

func (db *HypeTrainDB) QueryHypeTrainByStartTime(ctx context.Context, channelID string, params percy.HypeTrainQueryParams) ([]percy.HypeTrain, error) {
	var from, to string
	if params.From != nil {
		from = params.From.Format(time.RFC3339Nano)
	}

	if params.To != nil {
		to = params.To.Format(time.RFC3339Nano)
	}

	return db.QueryHypeTrainsBySortPrefix(ctx, channelID, from, to)
}

func (db *HypeTrainDB) QueryHypeTrainsBySortPrefix(ctx context.Context, channelID string, from string, to string) ([]percy.HypeTrain, error) {
	if len(channelID) == 0 {
		return nil, errors.New("blank channel ID")
	}

	var exprBuilder expression.Builder
	if len(from) != 0 && len(to) != 0 {
		// query for a specific range
		exprBuilder = expression.NewBuilder().WithKeyCondition(
			expression.KeyAnd(
				expression.KeyEqual(expression.Key(hypeTrainDBPrimaryKey), expression.Value(channelID)),
				expression.KeyBetween(expression.Key(hypeTrainSortKey), expression.Value(from), expression.Value(to)),
			),
		)
	} else if len(from) != 0 {
		// query for all hype trains after a start time
		exprBuilder = expression.NewBuilder().WithKeyCondition(
			expression.KeyAnd(
				expression.KeyEqual(expression.Key(hypeTrainDBPrimaryKey), expression.Value(channelID)),
				expression.KeyGreaterThanEqual(expression.Key(hypeTrainSortKey), expression.Value(from)),
			),
		)
	} else if len(to) != 0 {
		// query for all hype trains before a end time
		exprBuilder = expression.NewBuilder().WithKeyCondition(
			expression.KeyAnd(
				expression.KeyEqual(expression.Key(hypeTrainDBPrimaryKey), expression.Value(channelID)),
				expression.KeyLessThanEqual(expression.Key(hypeTrainSortKey), expression.Value(to)),
			),
		)
	} else {
		// EVERYTHING!
		exprBuilder = expression.NewBuilder().WithKeyCondition(
			expression.KeyEqual(expression.Key(hypeTrainDBPrimaryKey), expression.Value(channelID)),
		)
	}

	expr, err := exprBuilder.Build()
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query expression")
	}

	resp, err := db.client.QueryWithContext(ctx, &dynamodb.QueryInput{
		TableName:                 aws.String(db.tableName),
		KeyConditionExpression:    expr.KeyCondition(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	})
	if err != nil {
		return nil, err
	}
	if resp == nil || len(resp.Items) == 0 {
		return nil, nil
	}

	trains := make([]percy.HypeTrain, 0, len(resp.Items))
	for _, item := range resp.Items {
		var dbHypeTrain hypeTrain
		if err := dynamodbattribute.UnmarshalMap(item, &dbHypeTrain); err != nil {
			return nil, errors.Wrapf(err, "error unmarshaling hype train")
		}
		trains = append(trains, fromDBHypeTrain(dbHypeTrain))
	}

	return trains, nil
}

func (db *HypeTrainDB) CreateHypeTrain(ctx context.Context, hypeTrain percy.HypeTrain) error {
	marshaledTrain, err := dynamodbattribute.MarshalMap(toDBHypeTrain(hypeTrain, mostRecentDelimiter))
	if err != nil {
		return errors.Wrap(err, "failed to marshal hype train")
	}

	expr, err := expression.NewBuilder().WithCondition(noOngoingHypeTrainCondition()).Build()
	if err != nil {
		return errors.Wrap(err, "building expression for update")
	}

	_, err = db.client.PutItemWithContext(ctx,
		&dynamodb.PutItemInput{
			ConditionExpression:       expr.Condition(),
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
			TableName:                 aws.String(db.tableName),
			Item:                      marshaledTrain,
		},
	)

	if err != nil {
		if awsErr, ok := err.(awserr.Error); ok && awsErr.Code() == dynamodb.ErrCodeConditionalCheckFailedException { //nolint
			return errors.Wrap(err, "encountered conditional check failure while creating a new hype train, likely due to a different hype train was recently created")
		}

		return err
	}

	return nil
}

func (db *HypeTrainDB) UpsertHypeTrain(ctx context.Context, hypeTrain percy.HypeTrain) error {
	expr, err := expression.NewBuilder().
		WithUpdate(buildHypeTrainUpdateBuilder(hypeTrain)).
		Build()
	if err != nil {
		return errors.Wrap(err, "building expression for update")
	}

	input := dynamodb.UpdateItemInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		Key: map[string]*dynamodb.AttributeValue{
			hypeTrainDBPrimaryKey: {S: aws.String(hypeTrain.ChannelID)},
			hypeTrainSortKey:      {S: aws.String(mostRecentDelimiter)},
		},
		TableName:        aws.String(db.tableName),
		UpdateExpression: expr.Update(),
	}

	if _, err := db.client.UpdateItemWithContext(ctx, &input); err != nil {
		return errors.Wrap(err, "upsert HypeTrain")
	}

	return nil
}

func (db *HypeTrainDB) AddParticipation(ctx context.Context, channelID, hypeTrainID string, participation percy.Participation) (percy.HypeTrain, error) {
	if len(channelID) == 0 {
		return percy.HypeTrain{}, errors.New("channel ID is blank")
	}

	update := expression.Add(
		expression.Name(fmt.Sprintf("participations.%s", formatCounterAttributeName(participation.ParticipationIdentifier))),
		expression.Value(participation.Quantity),
	).Set(
		expression.Name("updated_at"),
		expression.Value(time.Now().UTC()),
	)

	expr, err := expression.NewBuilder().
		WithUpdate(update).
		WithCondition(isActiveCondition.
			And(expression.Name("id").Equal(expression.Value(hypeTrainID)))).Build()
	if err != nil {
		return percy.HypeTrain{}, errors.Wrap(err, "failed to build update expression")
	}

	return db.updateAndReturnNew(ctx, channelID, expr)
}

func (db *HypeTrainDB) EndHypeTrain(ctx context.Context, channelID, hypeTrainID string, reason percy.EndingReason) (percy.HypeTrain, error) {
	if len(channelID) == 0 {
		return percy.HypeTrain{}, errors.New("channel ID is blank")
	}

	update := expression.Set(
		expression.Name("ended_at"),
		expression.Value(time.Now().UTC()),
	).Set(
		expression.Name("ending_reason"),
		expression.Value(string(reason)),
	).Set(
		expression.Name("updated_at"),
		expression.Value(time.Now().UTC()),
	)

	expr, err := expression.NewBuilder().
		WithUpdate(update).
		WithCondition(isActiveCondition.
			And(expression.Name("id").Equal(expression.Value(hypeTrainID)))).Build()
	if err != nil {
		return percy.HypeTrain{}, errors.Wrap(err, "failed to build update expression")
	}

	updated, err := db.updateAndReturnNew(ctx, channelID, expr)
	if err != nil {
		return percy.HypeTrain{}, err
	}

	logrus.WithFields(logrus.Fields{
		"channelID":   channelID,
		"hypeTrainID": hypeTrainID,
	}).Infof("Hype train ended with level %d completed", updated.CompletedLevel())

	go func() {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		err := db.archiveHypeTrain(ctx, updated)
		if err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{
				"channelID":   updated.ChannelID,
				"hypeTrainID": updated.ID,
			}).Error("Failed to archive an ended hype train")
		}
	}()

	return updated, nil
}

func (db *HypeTrainDB) ExtendHypeTrain(ctx context.Context, channelID, hypeTrainID string, expiresAt time.Time) (percy.HypeTrain, error) {
	if len(channelID) == 0 {
		return percy.HypeTrain{}, errors.New("channel ID is blank")
	}

	if expiresAt.Before(time.Now()) {
		return percy.HypeTrain{}, errors.New("new expiration time is in the past")
	}

	update := expression.Set(
		expression.Name("expires_at"),
		expression.Value(expiresAt),
	).Set(
		expression.Name("updated_at"),
		expression.Value(time.Now().UTC()),
	)

	expr, err := expression.NewBuilder().
		WithUpdate(update).
		WithCondition(isActiveCondition.
			And(expression.Name("id").Equal(expression.Value(hypeTrainID)))).Build()
	if err != nil {
		return percy.HypeTrain{}, errors.Wrap(err, "failed to build update expression")
	}

	return db.updateAndReturnNew(ctx, channelID, expr)
}

func (db *HypeTrainDB) updateAndReturnNew(ctx context.Context, channelID string, expr expression.Expression) (percy.HypeTrain, error) {
	input := dynamodb.UpdateItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			hypeTrainDBPrimaryKey: {S: aws.String(channelID)},
			hypeTrainSortKey:      {S: aws.String(mostRecentDelimiter)},
		},
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ConditionExpression:       expr.Condition(),
		UpdateExpression:          expr.Update(),
		TableName:                 aws.String(db.tableName),
		ReturnValues:              aws.String(dynamodb.ReturnValueAllNew),
	}

	output, err := db.client.UpdateItemWithContext(ctx, &input)
	if err != nil {
		return percy.HypeTrain{}, err
	}

	if output == nil {
		return percy.HypeTrain{}, errors.New("no output was received")
	}

	var dbHypeTrain hypeTrain
	if err := dynamodbattribute.UnmarshalMap(output.Attributes, &dbHypeTrain); err != nil {
		return percy.HypeTrain{}, errors.Wrapf(err, "error unmarshaling hype train for channel %s", channelID)
	}

	return fromDBHypeTrain(dbHypeTrain), nil
}

func (db *HypeTrainDB) UpdateConductor(ctx context.Context, channelID string, hypeTrainID string, conductor percy.Conductor) (percy.HypeTrain, error) {
	if len(channelID) == 0 {
		return percy.HypeTrain{}, errors.New("empty channel ID")
	}

	if len(hypeTrainID) == 0 {
		return percy.HypeTrain{}, errors.New("empty user ID")
	}

	dbConductor := toDBConductor(conductor)

	update := expression.Set(
		expression.Name(fmt.Sprintf("conductors.%s", dbConductor.Source)),
		expression.Value(dbConductor),
	).Set(
		expression.Name("updated_at"),
		expression.Value(time.Now().UTC()),
	)

	condition := expression.Name("id").Equal(expression.Value(hypeTrainID))
	expr, err := expression.NewBuilder().WithUpdate(update).WithCondition(condition).Build()
	if err != nil {
		return percy.HypeTrain{}, errors.Wrap(err, "failed to build update expression")
	}

	return db.updateAndReturnNew(ctx, channelID, expr)
}

func (db *HypeTrainDB) RemoveConductor(ctx context.Context, channelID, hypeTrainID, userID string, source percy.ParticipationSource) (percy.HypeTrain, error) {
	if len(channelID) == 0 {
		return percy.HypeTrain{}, errors.New("empty channel ID")
	}

	if len(hypeTrainID) == 0 {
		return percy.HypeTrain{}, errors.New("empty user ID")
	}

	update := expression.Remove(
		expression.Name(fmt.Sprintf("conductors.%s", source)),
	).Set(
		expression.Name("updated_at"),
		expression.Value(time.Now().UTC()),
	)

	// ensure that hype train ID and conductor ID match
	condition := expression.Name("id").Equal(
		expression.Value(hypeTrainID),
	).And(
		expression.Name(
			fmt.Sprintf("conductors.%s.user_id", source),
		).Equal(
			expression.Value(userID),
		),
	)

	expr, err := expression.NewBuilder().WithUpdate(update).WithCondition(condition).Build()
	if err != nil {
		return percy.HypeTrain{}, errors.Wrap(err, "failed to build update expression")
	}

	return db.updateAndReturnNew(ctx, channelID, expr)
}

func buildHypeTrainUpdateBuilder(hypeTrain percy.HypeTrain) expression.UpdateBuilder {
	update := expression.Set(
		expression.Name("id"),
		expression.Value(hypeTrain.ID),
	).Set(
		expression.Name("started_at"),
		expression.Value(hypeTrain.StartedAt),
	).Set(
		expression.Name("expires_at"),
		expression.Value(hypeTrain.ExpiresAt),
	).Set(
		expression.Name("updated_at"),
		expression.Value(hypeTrain.UpdatedAt),
	).Set(
		expression.Name("conductors"),
		expression.Value(toDBConductors(hypeTrain.Conductors)),
	).Set(
		expression.Name("participations"),
		expression.Value(toDBParticipations(hypeTrain.Participations)),
	).Set(
		expression.Name("config"),
		expression.Value(toDBConfig(hypeTrain.Config, hypeTrain.ChannelID, customConfigType)),
	).Set(
		expression.Name("ttl"),
		expression.Value(time.Now().Add(ttlDuration).Unix()),
	)

	if hypeTrain.EndedAt != nil {
		update = update.Set(expression.Name("ended_at"), expression.Value(hypeTrain.EndedAt))
	}

	if hypeTrain.EndingReason != nil {
		update = update.Set(expression.Name("ending_reason"), expression.Value(hypeTrain.EndingReason))
	}

	return update
}

// archiveHypeTrain creates a duplicated entry of a hype train, using its ID as the range key
func (db *HypeTrainDB) archiveHypeTrain(ctx context.Context, hypeTrain percy.HypeTrain) error {
	marshaledTrain, err := dynamodbattribute.MarshalMap(toDBHypeTrain(hypeTrain, hypeTrain.StartedAt.UTC().Format(time.RFC3339Nano)))
	if err != nil {
		return errors.Wrap(err, "failed to marshal hype train")
	}

	_, err = db.client.PutItemWithContext(ctx,
		&dynamodb.PutItemInput{
			TableName: aws.String(db.tableName),
			Item:      marshaledTrain,
		},
	)

	return err
}

func toDBConductor(c percy.Conductor) conductor {
	participations := toDBParticipations(c.Participations)
	return conductor{
		UserID:         c.User.ID(),
		Source:         string(c.Source),
		Participations: participations,
	}
}

func toDBConductors(conductors map[percy.ParticipationSource]percy.Conductor) conductorsMap {
	dbConductors := make(conductorsMap)

	if len(conductors) == 0 {
		return dbConductors
	}

	for _, c := range conductors {
		dbConductors[string(c.Source)] = toDBConductor(c)
	}

	return dbConductors
}

func fromDBConductors(dbConductors map[string]conductor) map[percy.ParticipationSource]percy.Conductor {
	conductors := make(map[percy.ParticipationSource]percy.Conductor)

	if len(dbConductors) == 0 {
		return conductors
	}

	for _, c := range dbConductors {
		conductors[percy.ParticipationSource(c.Source)] = percy.Conductor{
			Source:         percy.ParticipationSource(c.Source),
			User:           percy.NewUser(c.UserID),
			Participations: fromDBParticipations(c.Participations),
		}
	}

	return conductors
}

func toDBParticipations(totals percy.ParticipationTotals) participationsMap {
	participations := make(participationsMap)

	if len(totals) == 0 {
		return participations
	}

	for identifier, total := range totals {
		participations[formatCounterAttributeName(identifier)] += total
	}

	return participations
}

func fromDBParticipations(participations map[string]int) percy.ParticipationTotals {
	totals := make(map[percy.ParticipationIdentifier]int)

	if len(participations) == 0 {
		return totals
	}

	for name, quantity := range participations {
		identifier, err := parseParticipationFromAttributeName(name)
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"attributeName": name,
				"quantity":      quantity,
			}).Warn("cannot parse attribute name into participation identifier")
			continue
		}

		totals[identifier] += quantity
	}

	return totals
}

func fromDBHypeTrain(hypeTrain hypeTrain) percy.HypeTrain {
	var endingReason *percy.EndingReason
	if hypeTrain.EndingReason != nil {
		// taking address of `reason` so that endingReason is a nil pointer instead of a pointer to ""
		reason := percy.EndingReason(*hypeTrain.EndingReason)
		endingReason = &reason
	}

	return percy.HypeTrain{
		ChannelID:      hypeTrain.ChannelID,
		ID:             hypeTrain.ID,
		StartedAt:      hypeTrain.StartedAt,
		ExpiresAt:      hypeTrain.ExpiresAt,
		UpdatedAt:      hypeTrain.UpdatedAt,
		EndedAt:        hypeTrain.EndedAt,
		EndingReason:   endingReason,
		Conductors:     fromDBConductors(hypeTrain.Conductors),
		Participations: fromDBParticipations(hypeTrain.Participations),
		Config:         fromDBConfig(hypeTrain.Config),
	}
}

func toDBHypeTrain(train percy.HypeTrain, instance string) hypeTrain {
	var endingReason *string
	if train.EndingReason != nil {
		endingReason = pointers.StringP(string(*train.EndingReason))
	}

	return hypeTrain{
		ChannelID:      train.ChannelID,
		Instance:       instance,
		ID:             train.ID,
		StartedAt:      train.StartedAt,
		ExpiresAt:      train.ExpiresAt,
		UpdatedAt:      train.UpdatedAt,
		EndedAt:        train.EndedAt,
		EndingReason:   endingReason,
		Conductors:     toDBConductors(train.Conductors),
		Config:         toDBConfig(train.Config, train.ChannelID, customConfigType),
		Participations: toDBParticipations(train.Participations),
		TTL:            time.Now().Add(ttlDuration).Unix(),
	}
}

// condition to check if there is no ongoing hype train
func noOngoingHypeTrainCondition() expression.ConditionBuilder {
	return expression.Name("instance").AttributeNotExists().Or(expression.Name("ended_at").AttributeExists()).Or(expression.Name("expires_at").LessThan(expression.Value(time.Now().UTC())))
}
