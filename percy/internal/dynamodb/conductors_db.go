package dynamodb

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/gogogadget/pointers"
	percy "code.justin.tv/commerce/percy/internal"
	"github.com/aws/aws-dax-go/dax"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
)

const (
	conductorsDBFormat                   = "conductors-%s"
	conductorsBitsConductorIDIndexFormat = "conductors-bits-conductor-id-index-%s"
	conductorsSubsConductorIDIndexFormat = "conductors-subs-conductor-id-index-%s"

	conductorsDBPrimaryKey                     = "channel_id"
	conductorsDBBitsConductorIDIndexPrimaryKey = "bits_conductor_id"
	conductorsDBSubsConductorIDIndexPrimaryKey = "subs_conductor_id"
)

type conductors struct {
	ChannelID       string     `dynamodbav:"channel_id"`
	BitsConductor   *conductor `dynamodbav:"bits_conductor"`
	BitsConductorID string     `dynamodbav:"bits_conductor_id,omitempty"`
	SubsConductor   *conductor `dynamodbav:"subs_conductor"`
	SubsConductorID string     `dynamodbav:"subs_conductor_id,omitempty"`
}

type ConductorsDB struct {
	tableName            string
	bitsConductorIDIndex string
	subsConductorIDIndex string
	client               dynamodbiface.DynamoDBAPI
}

func NewConductorsDB(config DynamoDBConfig) (*ConductorsDB, error) {
	if config.Endpoint == nil {
		// no endpoint is provided, use the DynamoDB client
		sess := session.Must(session.NewSessionWithOptions(session.Options{
			Config: aws.Config{Region: aws.String(config.Region)},
		}))

		return &ConductorsDB{
			client:               dynamodb.New(sess),
			tableName:            fmt.Sprintf(conductorsDBFormat, config.Environment),
			bitsConductorIDIndex: fmt.Sprintf(conductorsBitsConductorIDIndexFormat, config.Environment),
			subsConductorIDIndex: fmt.Sprintf(conductorsSubsConductorIDIndexFormat, config.Environment),
		}, nil

	} else {
		// endpoint provided, use DAX client
		daxConfig := dax.DefaultConfig()
		daxConfig.HostPorts = []string{*config.Endpoint}
		daxConfig.Region = config.Region
		daxConfig.ReadRetries = daxReadRetries
		daxConfig.WriteRetries = daxWriteRetries
		daxConfig.RequestTimeout = daxTimeout

		daxClient, err := dax.New(daxConfig)
		if err != nil {
			return nil, err
		}

		return &ConductorsDB{
			client:               daxClient,
			tableName:            fmt.Sprintf(conductorsDBFormat, config.Environment),
			bitsConductorIDIndex: fmt.Sprintf(conductorsBitsConductorIDIndexFormat, config.Environment),
			subsConductorIDIndex: fmt.Sprintf(conductorsSubsConductorIDIndexFormat, config.Environment),
		}, nil
	}
}

func (db *ConductorsDB) GetConductors(ctx context.Context, channelID string) ([]percy.Conductor, error) {
	if len(channelID) == 0 {
		return nil, errors.New("channel ID is blank")
	}

	resp, err := db.client.GetItemWithContext(ctx, &dynamodb.GetItemInput{
		TableName: aws.String(db.tableName),
		Key: map[string]*dynamodb.AttributeValue{
			conductorsDBPrimaryKey: {S: aws.String(channelID)},
		},
	})
	if err != nil {
		return nil, err
	}
	if resp == nil || resp.Item == nil {
		return nil, nil
	}

	var conds conductors
	if err := dynamodbattribute.UnmarshalMap(resp.Item, &conds); err != nil {
		return nil, errors.Wrapf(err, "error unmarshaling hype train for channel %s", channelID)
	}

	conductors := make([]percy.Conductor, 0)
	if conds.BitsConductorID != "" {
		conductors = append(conductors, percy.Conductor{
			Source: percy.BitsSource,
			User:   percy.NewUser(conds.BitsConductorID),
		})
	} else if conds.BitsConductor != nil {
		conductors = append(conductors, percy.Conductor{
			Source: percy.BitsSource,
			User:   percy.NewUser(conds.BitsConductor.UserID),
		})
	}

	if conds.SubsConductorID != "" {
		conductors = append(conductors, percy.Conductor{
			Source: percy.SubsSource,
			User:   percy.NewUser(conds.SubsConductorID),
		})
	} else if conds.SubsConductor != nil {
		conductors = append(conductors, percy.Conductor{
			Source: percy.SubsSource,
			User:   percy.NewUser(conds.SubsConductor.UserID),
		})
	}

	return conductors, nil
}

func (db *ConductorsDB) GetConductorChannelsForBitsConductorID(ctx context.Context, userID, cursor string) ([]string, string, error) {
	if len(userID) == 0 {
		return nil, "", errors.New("empty user ID")
	}

	expr, err := expression.NewBuilder().WithKeyCondition(
		expression.KeyEqual(expression.Key(conductorsDBBitsConductorIDIndexPrimaryKey), expression.Value(userID)),
	).Build()
	if err != nil {
		return nil, "", errors.Wrap(err, "failed to build query expression")
	}

	queryInput := &dynamodb.QueryInput{
		TableName:                 aws.String(db.tableName),
		IndexName:                 aws.String(db.bitsConductorIDIndex),
		Limit:                     aws.Int64(queryPageSize),
		KeyConditionExpression:    expr.KeyCondition(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	if len(cursor) != 0 {
		queryInput.ExclusiveStartKey = map[string]*dynamodb.AttributeValue{
			conductorsDBBitsConductorIDIndexPrimaryKey: {
				S: aws.String(cursor),
			},
		}
	}

	resp, err := db.client.QueryWithContext(ctx, queryInput)
	if err != nil {
		return nil, "", errors.Wrapf(err, "failed to query conductors by bits conductor ID for user ID %s", userID)
	}

	if resp == nil || len(resp.Items) == 0 {
		return nil, "", nil
	}

	var records []conductors
	if err := dynamodbattribute.UnmarshalListOfMaps(resp.Items, &records); err != nil {
		return nil, "", errors.Wrapf(err, "failed to unmarshal conductors by bits conductor ID records for user ID %s ", userID)
	}

	channels := make([]string, len(records))
	for i, record := range records {
		if record.BitsConductorID != "" {
			channels[i] = record.BitsConductorID
		}
	}

	nextCursor := ""
	if len(resp.LastEvaluatedKey) != 0 {
		nextCursor = pointers.StringOrDefault(resp.LastEvaluatedKey[conductorsDBBitsConductorIDIndexPrimaryKey].S, "")
	}

	return channels, nextCursor, nil
}

func (db *ConductorsDB) GetConductorChannelsForSubsConductorID(ctx context.Context, userID, cursor string) ([]string, string, error) {
	if len(userID) == 0 {
		return nil, "", errors.New("empty user ID")
	}

	expr, err := expression.NewBuilder().WithKeyCondition(
		expression.KeyEqual(expression.Key(conductorsDBSubsConductorIDIndexPrimaryKey), expression.Value(userID)),
	).Build()
	if err != nil {
		return nil, "", errors.Wrap(err, "failed to build query expression")
	}

	queryInput := &dynamodb.QueryInput{
		TableName:                 aws.String(db.tableName),
		IndexName:                 aws.String(db.subsConductorIDIndex),
		Limit:                     aws.Int64(queryPageSize),
		KeyConditionExpression:    expr.KeyCondition(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	if len(cursor) != 0 {
		queryInput.ExclusiveStartKey = map[string]*dynamodb.AttributeValue{
			conductorsDBSubsConductorIDIndexPrimaryKey: {
				S: aws.String(cursor),
			},
		}
	}

	resp, err := db.client.QueryWithContext(ctx, queryInput)
	if err != nil {
		return nil, "", errors.Wrapf(err, "failed to query conductors by subs conductor ID for user ID %s", userID)
	}

	if resp == nil || len(resp.Items) == 0 {
		return nil, "", nil
	}

	var records []conductors
	if err := dynamodbattribute.UnmarshalListOfMaps(resp.Items, &records); err != nil {
		return nil, "", errors.Wrapf(err, "failed to unmarshal conductors by subs conductor ID records for user ID %s ", userID)
	}

	channels := make([]string, len(records))
	for i, record := range records {
		if record.SubsConductorID != "" {
			channels[i] = record.SubsConductorID
		}
	}

	nextCursor := ""
	if len(resp.LastEvaluatedKey) != 0 {
		nextCursor = pointers.StringOrDefault(resp.LastEvaluatedKey[conductorsDBSubsConductorIDIndexPrimaryKey].S, "")
	}

	return channels, nextCursor, nil
}

func (db *ConductorsDB) SetConductors(ctx context.Context, channelID string, updatedConductors []percy.Conductor) error {
	if len(channelID) == 0 {
		return errors.New("channel ID is blank")
	}

	conductors := &conductors{
		ChannelID: channelID,
	}

	for _, conductor := range updatedConductors {
		if conductor.Source == percy.BitsSource {
			conductors.BitsConductorID = conductor.User.ID()
		} else if conductor.Source == percy.SubsSource {
			conductors.SubsConductorID = conductor.User.ID()
		}
	}

	conductorsEntry, err := dynamodbattribute.MarshalMap(conductors)
	if err != nil {
		return errors.New("failed to marshal conductors dynamodb db entry")
	}

	_, err = db.client.PutItemWithContext(ctx, &dynamodb.PutItemInput{
		TableName: aws.String(db.tableName),
		Item:      conductorsEntry,
	})

	return err
}

func (db *ConductorsDB) DeleteConductorsRecord(ctx context.Context, channelID string) error {
	if len(channelID) == 0 {
		return errors.New("empty channel ID")
	}

	_, err := db.client.DeleteItemWithContext(ctx, &dynamodb.DeleteItemInput{
		TableName: aws.String(db.tableName),
		Key: map[string]*dynamodb.AttributeValue{
			conductorsDBPrimaryKey: {S: aws.String(channelID)},
		},
	})

	return err
}

func (db *ConductorsDB) CleanConductorsRecords(ctx context.Context, input map[string]percy.CleanConductorsInput) error {
	requestedItems := make([]*dynamodb.TransactWriteItem, 0)

	for channelID, update := range input {
		updateExpr := expression.UpdateBuilder{}
		if update.CleanBitsConductorID {
			updateExpr = updateExpr.Remove(expression.Name(conductorsDBBitsConductorIDIndexPrimaryKey)).
				Remove(expression.Name("bits_conductor")) // legacy record name
		}
		if update.CleanSubsConductorID {
			updateExpr = updateExpr.Remove(expression.Name(conductorsDBSubsConductorIDIndexPrimaryKey)).
				Remove(expression.Name("subs_conductor")) // legacy record name
		}
		expr, err := expression.NewBuilder().WithUpdate(updateExpr).Build()
		if err != nil {
			return errors.Wrap(err, "failed to build query expression")
		}

		requestedItems = append(requestedItems, &dynamodb.TransactWriteItem{
			Update: &dynamodb.Update{
				TableName: aws.String(db.tableName),
				Key: map[string]*dynamodb.AttributeValue{
					conductorsDBPrimaryKey: {S: aws.String(channelID)},
				},
				UpdateExpression:          expr.Update(),
				ExpressionAttributeNames:  expr.Names(),
				ExpressionAttributeValues: expr.Values(),
			},
		})
	}

	errs, egCtx := errgroup.WithContext(ctx)

	for i := 0; i <= len(requestedItems)/deleteBatchSize; i++ {
		start := i * deleteBatchSize
		end := start + deleteBatchSize
		if end > len(requestedItems) {
			end = len(requestedItems)
		}

		if start == end {
			break
		}

		batch := requestedItems[start:end]

		errs.Go(func() error {
			_, err := db.client.TransactWriteItemsWithContext(egCtx, &dynamodb.TransactWriteItemsInput{
				TransactItems: batch,
			})
			if err != nil {
				return errors.Wrap(err, "Failed to clean conductors records")
			}

			return nil
		})
	}

	if err := errs.Wait(); err != nil {
		return err
	}

	return nil
}
