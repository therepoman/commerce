package dynamodb

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	"github.com/aws/aws-dax-go/dax"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
)

const (
	configsDBFormat = "hype-train-configs-%s"
	configsHashKey  = "channel_id"
	configsRangeKey = "type"

	daxWriteRetries = 1
	daxReadRetries  = 1
	daxTimeout      = 500 * time.Millisecond
)

type configType string

const (
	globalChannelID = "GLOBAL"

	defaultConfigType     = configType("DEFAULT")
	recommendedConfigType = configType("RECOMMENDED")
	customConfigType      = configType("CUSTOM")
)

type hypeTrainConfig struct {
	ChannelID string     `dynamodbav:"channel_id"`
	Type      configType `dynamodbav:"type"`

	Enabled                        *bool      `dynamodbav:"enabled,omitempty"`
	IsWhitelisted                  *bool      `dynamodbav:"is_whitelisted,omitempty"`
	NumOfEventsToKickoff           *int       `dynamodbav:"num_of_events_to_kickoff,omitempty"`
	MinPointsToCountTowardsKickoff *int       `dynamodbav:"min_points_to_count_towards_kickoff,omitempty"`
	KickoffDurationSeconds         *int       `dynamodbav:"kickoff_duration_seconds,omitempty"`
	CooldownDurationMinutes        *int       `dynamodbav:"cooldown_duration_minutes,omitempty"`
	LevelDurationSeconds           *int       `dynamodbav:"level_duration_seconds,omitempty"`
	Difficulty                     *string    `dynamodbav:"difficulty,omitempty"`
	RewardEndDate                  *time.Time `dynamodbav:"reward_end_date,omitempty"`

	ParticipationConversionRates *[]conversionRate          `dynamodbav:"participation_conversion_rates,omitempty"`
	NotificationThresholds       *[]notificationThreshold   `dynamodbav:"notification_thresholds,omitempty"`
	LevelSettings                *map[string][]levelSetting `dynamodbav:"level_settings,omitempty"`
	ConductorRewards             *[]conductorReward         `dynamodbav:"conductor_rewards,omitempty"`
	HasConductorBadges           *bool                      `dynamodbav:"has_conductor_badges,omitempty"`
	CalloutEmoteID               *string                    `dynamodbav:"callout_emote_id,omitempty"`
	UseCreatorColor              *bool                      `dynamodbav:"use_creator_color,omitempty"`
	PrimaryHexColor              *string                    `dynamodbav:"primary_hex_color,omitempty"`
	UsePersonalizedSettings      *bool                      `dynamodbav:"use_personalized_settings,omitempty"`

	LastUpdated *time.Time `dynamodbav:"last_updated,omitempty"`
}

type conversionRate struct {
	ParticipationSource string `dynamodbav:"participation_source"`
	ParticipationAction string `dynamodbav:"participation_action"`
	Rate                int    `dynamodbav:"rate"`
}

type notificationThreshold struct {
	ParticipationSource string `dynamodbav:"participation_source"`
	ParticipationAction string `dynamodbav:"participation_action"`
	Threshold           int    `dynamodbav:"threshold"`
}

type levelSetting struct {
	Level   int      `dynamodbav:"level"`
	Goal    int      `dynamodbav:"goal"`
	Rewards []reward `dynamodbav:"rewards,omitempty"`
}

type conductorReward struct {
	Participationsource string   `dynamodbav:"participation_source"`
	ConductorType       string   `dynamodbav:"conductor_type"`
	Rewards             []reward `dynamodbav:"rewards,omitempty"`
}

type reward struct {
	Type    string `dynamodbav:"type"`
	ID      string `dynamodbav:"id"`
	GroupID string `dynamodbav:"group_id"`
}

type HypeTrainConfigsDB struct {
	tableName string
	client    dynamodbiface.DynamoDBAPI
}

type DynamoDBConfig struct {
	Region      string
	Environment string
	Endpoint    *string
}

func NewHypeTrainConfigsDB(config DynamoDBConfig) (*HypeTrainConfigsDB, error) {
	if config.Endpoint == nil {
		// no endpoint is provided, use the DynamoDB client
		sess := session.Must(session.NewSessionWithOptions(session.Options{
			Config: aws.Config{Region: aws.String(config.Region)},
		}))

		return &HypeTrainConfigsDB{
			client:    dynamodb.New(sess),
			tableName: fmt.Sprintf(configsDBFormat, config.Environment),
		}, nil

	} else {
		// endpoint provided, use DAX client
		daxConfig := dax.DefaultConfig()
		daxConfig.HostPorts = []string{*config.Endpoint}
		daxConfig.Region = config.Region
		daxConfig.ReadRetries = daxReadRetries
		daxConfig.WriteRetries = daxWriteRetries
		daxConfig.RequestTimeout = daxTimeout

		daxClient, err := dax.New(daxConfig)
		if err != nil {
			return nil, err
		}

		return &HypeTrainConfigsDB{
			client:    daxClient,
			tableName: fmt.Sprintf(configsDBFormat, config.Environment),
		}, nil
	}
}

// UpdateCustomConfig updates the CUSTOM config record for a channel
func (db *HypeTrainConfigsDB) UpdateConfig(ctx context.Context, channelID string, configUpdate percy.HypeTrainConfigUpdate) (*percy.HypeTrainConfig, error) {
	if len(channelID) == 0 {
		return nil, errors.New("channel ID is blank")
	}

	updatedConfig, err := db.updateConfig(ctx, channelID, configUpdate, customConfigType)
	if err != nil {
		return nil, errors.Wrap(err, "failed to update hype train config for channel")
	}

	// load the DEFAULT and RECOMMENDED configurations and merge them
	configs, err := db.batchGetConfigs(ctx,
		getKey(globalChannelID, defaultConfigType),
		getKey(channelID, recommendedConfigType))
	if err != nil {
		return nil, err
	}

	channelConfig := mergeConfigs(channelID, append(configs, updatedConfig)...)
	return &channelConfig, nil
}

// GetConfig retrieves the global default, the recommended and the custom configs for a given channel
// and merges all configs into a consolidated channel config
// with priority: custom > recommended > default
func (db *HypeTrainConfigsDB) GetConfig(ctx context.Context, channelID string) (*percy.HypeTrainConfig, error) {
	if len(channelID) == 0 {
		return nil, errors.New("channel ID is blank")
	}

	configs, err := db.batchGetConfigs(ctx,
		getKey(globalChannelID, defaultConfigType),
		getKey(channelID, recommendedConfigType),
		getKey(channelID, customConfigType))
	if err != nil {
		return nil, err
	}

	channelConfig := mergeConfigs(channelID, configs...)

	return &channelConfig, nil
}

// PutGlobalDefaultConfig is for internal uses only
// Having this method for testing and scripting purposes
func (db *HypeTrainConfigsDB) PutGlobalDefaultConfig(ctx context.Context, config percy.HypeTrainConfig) error {
	marshalledConfig, err := dynamodbattribute.MarshalMap(toDBConfig(config, globalChannelID, defaultConfigType))
	if err != nil {
		return err
	}

	_, err = db.client.PutItem(
		&dynamodb.PutItemInput{
			TableName: aws.String(db.tableName),
			Item:      marshalledConfig,
		},
	)

	return err
}

// PutRecommendedConfig is for internal uses only
// Having this method for testing and scripting purposes
func (db *HypeTrainConfigsDB) UpdateRecommendedConfig(ctx context.Context, channelID string, configUpdate percy.HypeTrainConfigUpdate) error {
	_, err := db.updateConfig(ctx, channelID, configUpdate, recommendedConfigType)
	return err
}

// UnsetRecommendedConfig is for internal uses only
// Having this method for testing and scripting purposes
func (db *HypeTrainConfigsDB) UnsetRecommendedConfig(ctx context.Context, channelID string, configUpdate percy.HypeTrainConfigUnset) error {
	_, err := db.unsetConfig(ctx, channelID, configUpdate, recommendedConfigType)
	return err
}

func (db *HypeTrainConfigsDB) DeleteConfigRecords(ctx context.Context, channelIDs []string) error {
	requestedItems := make([]*dynamodb.WriteRequest, 0)

	for _, channelID := range channelIDs {
		requestedItems = append(requestedItems, &dynamodb.WriteRequest{
			DeleteRequest: &dynamodb.DeleteRequest{
				Key: getKey(channelID, customConfigType),
			},
		}, &dynamodb.WriteRequest{
			DeleteRequest: &dynamodb.DeleteRequest{
				Key: getKey(channelID, recommendedConfigType),
			},
		})
	}

	errs, egCtx := errgroup.WithContext(ctx)

	for i := 0; i <= len(requestedItems)/deleteBatchSize; i++ {
		start := i * deleteBatchSize
		end := start + deleteBatchSize
		if end > len(requestedItems) {
			end = len(requestedItems)
		}

		if start == end {
			break
		}

		batch := requestedItems[start:end]

		errs.Go(func() error {
			output, err := db.client.BatchWriteItemWithContext(egCtx, &dynamodb.BatchWriteItemInput{
				RequestItems: map[string][]*dynamodb.WriteRequest{
					db.tableName: batch,
				},
			})
			if err != nil {
				return errors.Wrap(err, "Failed to clean hype train config records")
			}

			if output != nil && len(output.UnprocessedItems) > 0 {
				return errors.New("Failed to process all items while deleting hype train config records")
			}

			return nil
		})
	}

	if err := errs.Wait(); err != nil {
		return err
	}

	return nil
}

func (db *HypeTrainConfigsDB) updateConfig(ctx context.Context, channelID string, configUpdate percy.HypeTrainConfigUpdate, configType configType) (hypeTrainConfig, error) {
	updateBuilder := expression.Set(expression.Name("last_updated"), expression.Value(time.Now()))

	if configUpdate.Enabled != nil {
		updateBuilder = updateBuilder.Set(expression.Name("enabled"), expression.Value(*configUpdate.Enabled))
	}

	if configUpdate.IsWhitelisted != nil {
		updateBuilder = updateBuilder.Set(expression.Name("is_whitelisted"), expression.Value(*configUpdate.IsWhitelisted))
	}

	if configUpdate.NumOfEventsToKickoff != nil {
		updateBuilder = updateBuilder.Set(expression.Name("num_of_events_to_kickoff"), expression.Value(*configUpdate.NumOfEventsToKickoff))
	}

	if configUpdate.Difficulty != nil {
		updateBuilder = updateBuilder.Set(expression.Name("difficulty"), expression.Value(*configUpdate.Difficulty))
	}

	if configUpdate.CooldownDuration != nil {
		updateBuilder = updateBuilder.Set(expression.Name("cooldown_duration_minutes"), expression.Value(int(configUpdate.CooldownDuration.Minutes())))
	}

	if configUpdate.KickoffDuration != nil {
		updateBuilder = updateBuilder.Set(expression.Name("kickoff_duration_seconds"), expression.Value(int(configUpdate.KickoffDuration.Seconds())))
	}

	if configUpdate.CalloutEmoteID != nil {
		updateBuilder = updateBuilder.Set(expression.Name("callout_emote_id"), expression.Value(*configUpdate.CalloutEmoteID))
	}

	if configUpdate.UseCreatorColor != nil {
		updateBuilder = updateBuilder.Set(expression.Name("use_creator_color"), expression.Value(*configUpdate.UseCreatorColor))
	}

	if configUpdate.UsePersonalizedSettings != nil {
		updateBuilder = updateBuilder.Set(expression.Name("use_personalized_settings"), expression.Value(*configUpdate.UsePersonalizedSettings))
	}

	expr, err := expression.NewBuilder().WithUpdate(updateBuilder).Build()
	if err != nil {
		return hypeTrainConfig{}, errors.Wrap(err, "failed to build update expression to update config")
	}

	input := dynamodb.UpdateItemInput{
		TableName:                 aws.String(db.tableName),
		Key:                       getKey(channelID, configType),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		UpdateExpression:          expr.Update(),
		ReturnValues:              aws.String(dynamodb.ReturnValueAllNew),
	}

	output, err := db.client.UpdateItemWithContext(ctx, &input)
	if err != nil {
		return hypeTrainConfig{}, err
	}

	var updatedConfig hypeTrainConfig
	err = dynamodbattribute.UnmarshalMap(output.Attributes, &updatedConfig)
	if err != nil {
		return updatedConfig, err
	}

	return updatedConfig, nil
}

func (db *HypeTrainConfigsDB) UnsetConfig(ctx context.Context, channelID string, configUpdate percy.HypeTrainConfigUnset) (*percy.HypeTrainConfig, error) {
	if len(channelID) == 0 {
		return nil, errors.New("channel ID is blank")
	}

	updatedConfig, err := db.unsetConfig(ctx, channelID, configUpdate, customConfigType)
	if err != nil {
		return nil, errors.Wrap(err, "failed to perform unset for hype train config for channel")
	}

	// load the DEFAULT and RECOMMENDED configurations and merge them
	configs, err := db.batchGetConfigs(ctx,
		getKey(globalChannelID, defaultConfigType),
		getKey(channelID, recommendedConfigType))
	if err != nil {
		return nil, err
	}

	channelConfig := mergeConfigs(channelID, append(configs, updatedConfig)...)
	return &channelConfig, nil
}

func (db *HypeTrainConfigsDB) unsetConfig(ctx context.Context, channelID string, configUpdate percy.HypeTrainConfigUnset, configType configType) (hypeTrainConfig, error) {
	updateBuilder := expression.Set(expression.Name("last_updated"), expression.Value(time.Now()))

	if configUpdate.Enabled {
		updateBuilder = updateBuilder.Remove(expression.Name("enabled"))
	}

	if configUpdate.IsWhitelisted {
		updateBuilder = updateBuilder.Remove(expression.Name("is_whitelisted"))
	}

	if configUpdate.NumOfEventsToKickoff {
		updateBuilder = updateBuilder.Remove(expression.Name("num_of_events_to_kickoff"))
	}

	if configUpdate.Difficulty {
		updateBuilder = updateBuilder.Remove(expression.Name("difficulty"))
	}

	if configUpdate.CooldownDuration {
		updateBuilder = updateBuilder.Remove(expression.Name("cooldown_duration_minutes"))
	}

	if configUpdate.CalloutEmoteID {
		updateBuilder = updateBuilder.Remove(expression.Name("callout_emote_id"))
	}

	if configUpdate.UseCreatorColor {
		updateBuilder = updateBuilder.Remove(expression.Name("use_creator_color"))
	}

	if configUpdate.KickoffDuration {
		updateBuilder = updateBuilder.Remove(expression.Name("kickoff_duration_seconds"))
	}

	expr, err := expression.NewBuilder().WithUpdate(updateBuilder).Build()
	if err != nil {
		return hypeTrainConfig{}, errors.Wrap(err, "failed to build update expression to update config")
	}

	input := dynamodb.UpdateItemInput{
		TableName:                 aws.String(db.tableName),
		Key:                       getKey(channelID, configType),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		UpdateExpression:          expr.Update(),
		ReturnValues:              aws.String(dynamodb.ReturnValueAllNew),
	}

	output, err := db.client.UpdateItemWithContext(ctx, &input)
	if err != nil {
		return hypeTrainConfig{}, err
	}

	var updatedConfig hypeTrainConfig
	err = dynamodbattribute.UnmarshalMap(output.Attributes, &updatedConfig)
	if err != nil {
		return updatedConfig, err
	}

	return updatedConfig, nil
}

func (db *HypeTrainConfigsDB) batchGetConfigs(ctx context.Context, keys ...map[string]*dynamodb.AttributeValue) ([]hypeTrainConfig, error) {
	output, err := db.client.BatchGetItemWithContext(ctx, &dynamodb.BatchGetItemInput{
		RequestItems: map[string]*dynamodb.KeysAndAttributes{
			db.tableName: {
				Keys: keys,
			},
		},
	})
	if err != nil {
		return nil, err
	}

	if output == nil {
		return nil, errors.New("BatchGetItem request received nil output from DynamoDB")
	}

	var results []hypeTrainConfig
	err = dynamodbattribute.UnmarshalListOfMaps(output.Responses[db.tableName], &results)
	return results, err
}

// mergeConfigs merges all configs into a consolidated channel config
// with priority: custom > recommended > default
func mergeConfigs(channelID string, configs ...hypeTrainConfig) percy.HypeTrainConfig {
	var custom, recommended, globalDefault *hypeTrainConfig

	for i, config := range configs {
		switch config.Type {
		case defaultConfigType:
			globalDefault = &configs[i]
		case recommendedConfigType:
			recommended = &configs[i]
		case customConfigType:
			custom = &configs[i]
		default:
			logrus.WithField("channelID", config.ChannelID).Warnf("Found unrecognized hype train configuration type [%s]", config.Type)
		}
	}

	channelConfig := percy.HypeTrainConfig{
		ChannelID:              channelID,
		ConductorRewards:       make(percy.ConductorRewards),
		NotificationThresholds: make(map[percy.ParticipationIdentifier]int),
	}

	for _, config := range []*hypeTrainConfig{globalDefault, recommended, custom} {
		if config == nil {
			continue
		}

		if config.Enabled != nil {
			channelConfig.Enabled = *config.Enabled
		}

		if config.IsWhitelisted != nil {
			channelConfig.IsWhitelisted = *config.IsWhitelisted
		}

		if config.NumOfEventsToKickoff != nil {
			channelConfig.KickoffConfig.NumOfEvents = *config.NumOfEventsToKickoff
		}

		if config.MinPointsToCountTowardsKickoff != nil {
			channelConfig.KickoffConfig.MinPoints = percy.ParticipationPoint(*config.MinPointsToCountTowardsKickoff)
		}

		if config.KickoffDurationSeconds != nil {
			channelConfig.KickoffConfig.Duration = time.Duration(*config.KickoffDurationSeconds) * time.Second
		}

		if config.CooldownDurationMinutes != nil {
			channelConfig.CooldownDuration = time.Duration(*config.CooldownDurationMinutes) * time.Minute
		}

		if config.LevelDurationSeconds != nil {
			channelConfig.LevelDuration = time.Duration(*config.LevelDurationSeconds) * time.Second
		}

		if config.Difficulty != nil {
			channelConfig.Difficulty = percy.Difficulty(*config.Difficulty)
		}

		if config.RewardEndDate != nil {
			channelConfig.RewardEndDate = config.RewardEndDate
		}

		if config.ConductorRewards != nil {
			channelConfig.ConductorRewards = fromDBConductorRewards(*config.ConductorRewards)
		}

		if config.HasConductorBadges != nil {
			channelConfig.HasConductorBadges = *config.HasConductorBadges
		}

		if config.CalloutEmoteID != nil {
			channelConfig.CalloutEmoteID = *config.CalloutEmoteID
		}

		if config.UseCreatorColor != nil {
			channelConfig.UseCreatorColor = *config.UseCreatorColor
		}

		if config.PrimaryHexColor != nil {
			channelConfig.PrimaryHexColor = *config.PrimaryHexColor
		}

		if config.UsePersonalizedSettings != nil {
			channelConfig.UsePersonalizedSettings = *config.UsePersonalizedSettings
		}

		if config.ParticipationConversionRates != nil {
			channelConfig.ParticipationConversionRates = make(map[percy.ParticipationIdentifier]percy.ParticipationPoint)
			for _, rate := range *config.ParticipationConversionRates {
				channelConfig.ParticipationConversionRates[percy.ParticipationIdentifier{
					Source: percy.ParticipationSource(rate.ParticipationSource),
					Action: percy.ParticipationAction(rate.ParticipationAction),
				}] = percy.ParticipationPoint(rate.Rate)
			}
		}

		if config.NotificationThresholds != nil {
			channelConfig.NotificationThresholds = make(map[percy.ParticipationIdentifier]int)
			for _, threshold := range *config.NotificationThresholds {
				channelConfig.NotificationThresholds[percy.ParticipationIdentifier{
					Source: percy.ParticipationSource(threshold.ParticipationSource),
					Action: percy.ParticipationAction(threshold.ParticipationAction),
				}] = threshold.Threshold
			}
		}

		if config.LevelSettings != nil {
			channelConfig.LevelSettingsByDifficulty = make(map[percy.Difficulty][]percy.LevelSetting)
			for difficulty, settings := range *config.LevelSettings {
				for _, setting := range settings {
					channelConfig.LevelSettingsByDifficulty[percy.Difficulty(difficulty)] =
						append(channelConfig.LevelSettingsByDifficulty[percy.Difficulty(difficulty)], percy.LevelSetting{
							Level:   setting.Level,
							Goal:    percy.ParticipationPoint(setting.Goal),
							Rewards: fromDBRewards(setting.Rewards),
						})
				}
			}
		}
	}

	return channelConfig
}

func getKey(channelID string, configType configType) map[string]*dynamodb.AttributeValue {
	return map[string]*dynamodb.AttributeValue{
		configsHashKey:  {S: aws.String(channelID)},
		configsRangeKey: {S: aws.String(string(configType))},
	}
}

func toDBConfig(config percy.HypeTrainConfig, channelID string, configType configType) hypeTrainConfig {
	participationConversionRates := make([]conversionRate, 0)
	for identifier, rate := range config.ParticipationConversionRates {
		participationConversionRates = append(participationConversionRates, conversionRate{
			ParticipationSource: string(identifier.Source),
			ParticipationAction: string(identifier.Action),
			Rate:                int(rate),
		})
	}

	notificationThresholds := make([]notificationThreshold, 0)
	for identifier, threshold := range config.NotificationThresholds {
		notificationThresholds = append(notificationThresholds, notificationThreshold{
			ParticipationSource: string(identifier.Source),
			ParticipationAction: string(identifier.Action),
			Threshold:           threshold,
		})
	}

	conductorRewards := make([]conductorReward, 0)
	for source, RewardsByKind := range config.ConductorRewards {
		for kind, rewards := range RewardsByKind {
			dbRewards := toDBRewards(rewards)
			conductorRewards = append(conductorRewards, conductorReward{
				Participationsource: string(source),
				ConductorType:       string(kind),
				Rewards:             dbRewards,
			})
		}
	}

	levelSettings := make(map[string][]levelSetting)
	for difficulty, levels := range config.LevelSettingsByDifficulty {
		levelSettings[string(difficulty)] = make([]levelSetting, 0, len(levels))
		for _, level := range levels {
			levelSettings[string(difficulty)] = append(levelSettings[string(difficulty)], levelSetting{
				Level:   level.Level,
				Goal:    int(level.Goal),
				Rewards: toDBRewards(level.Rewards),
			})
		}
	}

	return hypeTrainConfig{
		ChannelID: channelID,
		Type:      configType,

		Enabled:                        &config.Enabled,
		IsWhitelisted:                  &config.IsWhitelisted,
		Difficulty:                     pointers.StringP(string(config.Difficulty)),
		RewardEndDate:                  config.RewardEndDate,
		NumOfEventsToKickoff:           &config.KickoffConfig.NumOfEvents,
		MinPointsToCountTowardsKickoff: pointers.IntP(int(config.KickoffConfig.MinPoints)),
		KickoffDurationSeconds:         pointers.IntP(int(config.KickoffConfig.Duration.Seconds())),
		ParticipationConversionRates:   &participationConversionRates,

		CooldownDurationMinutes: pointers.IntP(int(config.CooldownDuration.Minutes())),
		LevelDurationSeconds:    pointers.IntP(int(config.LevelDuration.Seconds())),
		LevelSettings:           &levelSettings,
		NotificationThresholds:  &notificationThresholds,
		ConductorRewards:        &conductorRewards,
		HasConductorBadges:      &config.HasConductorBadges,
		CalloutEmoteID:          &config.CalloutEmoteID,
		UseCreatorColor:         &config.UseCreatorColor,
		PrimaryHexColor:         &config.PrimaryHexColor,
		UsePersonalizedSettings: &config.UsePersonalizedSettings,

		LastUpdated: pointers.TimeP(time.Now().UTC()),
	}
}

func toDBRewards(rewards []percy.Reward) []reward {
	dbRewards := make([]reward, 0, len(rewards))
	for _, r := range rewards {
		dbRewards = append(dbRewards, reward{
			ID:      r.ID,
			Type:    string(r.Type),
			GroupID: r.GroupID,
		})
	}

	return dbRewards
}

func fromDBConfig(config hypeTrainConfig) percy.HypeTrainConfig {
	config.Type = customConfigType
	return mergeConfigs(config.ChannelID, config)
}

func fromDBConductorRewards(dbConductorRewards []conductorReward) percy.ConductorRewards {
	if len(dbConductorRewards) == 0 {
		return nil
	}

	conductorRewards := make(percy.ConductorRewards)

	for _, reward := range dbConductorRewards {
		participationSource := percy.ParticipationSource(reward.Participationsource)
		_, ok := conductorRewards[participationSource]
		if !ok {
			conductorRewards[participationSource] = make(map[percy.ConductorType][]percy.Reward)
		}
		if len(reward.Rewards) > 0 {
			conductorRewards[participationSource][percy.ConductorType(reward.ConductorType)] = fromDBRewards(reward.Rewards)
		}
	}

	return conductorRewards
}

func fromDBRewards(dbRewards []reward) []percy.Reward {
	if len(dbRewards) == 0 {
		return make([]percy.Reward, 0)
	}

	rewards := make([]percy.Reward, 0, len(dbRewards))
	for _, reward := range dbRewards {
		rewards = append(rewards, percy.Reward{
			ID:      reward.ID,
			Type:    percy.RewardType(reward.Type),
			GroupID: reward.GroupID,
		})
	}

	return rewards
}
