package dynamodb

import (
	"context"
	"fmt"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"github.com/aws/aws-dax-go/dax"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/pkg/errors"
)

const (
	celebrationUserSettingsDBFormat   = "celebrations-user-settings-%s"
	celebrationUserSettingsPrimaryKey = "user_id"

	celebrationUserSettingsDaxWriteRetries = 1
	celebrationUserSettingsDaxReadRetries  = 1
	celebrationUserSettingsDaxTimeout      = 500 * time.Millisecond
)

type celebrationUserSettings struct {
	UserID     string `dynamodbav:"user_id"`
	IsOptedOut bool   `dynamodbav:"is_opted_out"`
}

type CelebrationUserSettingsDB struct {
	tableName string
	client    dynamodbiface.DynamoDBAPI
}

func NewCelebrationUserSettingsDB(config DynamoDBConfig) (*CelebrationUserSettingsDB, error) {
	if config.Endpoint == nil {
		// no endpoint is provided, use the DynamoDB client
		sess := session.Must(session.NewSessionWithOptions(session.Options{
			Config: aws.Config{Region: aws.String(config.Region)},
		}))

		return &CelebrationUserSettingsDB{
			client:    dynamodb.New(sess),
			tableName: fmt.Sprintf(celebrationUserSettingsDBFormat, config.Environment),
		}, nil

	}

	// endpoint provided, use DAX client
	daxConfig := dax.DefaultConfig()
	daxConfig.HostPorts = []string{*config.Endpoint}
	daxConfig.Region = config.Region
	daxConfig.ReadRetries = celebrationUserSettingsDaxReadRetries
	daxConfig.WriteRetries = celebrationUserSettingsDaxWriteRetries
	daxConfig.RequestTimeout = celebrationUserSettingsDaxTimeout

	daxClient, err := dax.New(daxConfig)
	if err != nil {
		return nil, err
	}

	return &CelebrationUserSettingsDB{
		client:    daxClient,
		tableName: fmt.Sprintf(celebrationUserSettingsDBFormat, config.Environment),
	}, nil
}

func (db *CelebrationUserSettingsDB) Get(ctx context.Context, userID string) (*percy.CelebrationUserSettings, error) {
	if len(userID) == 0 {
		return nil, errors.New("user ID is blank")
	}

	resp, err := db.client.GetItemWithContext(ctx, &dynamodb.GetItemInput{
		TableName: aws.String(db.tableName),
		Key: map[string]*dynamodb.AttributeValue{
			celebrationUserSettingsPrimaryKey: {S: aws.String(userID)},
		},
	})
	if err != nil {
		return nil, err
	}

	if resp == nil || resp.Item == nil {
		return nil, nil
	}

	var celebrationUserSettings celebrationUserSettings
	if err := dynamodbattribute.UnmarshalMap(resp.Item, &celebrationUserSettings); err != nil {
		return nil, errors.Wrapf(err, "error unmarshaling celebrationUserSettings for user %s", userID)
	}

	return fromDBCelebrationUserSettings(celebrationUserSettings), nil
}

func (db *CelebrationUserSettingsDB) Update(ctx context.Context, userID string, update percy.CelebrationUserSettingsUpdate) (*percy.CelebrationUserSettings, error) {
	if len(userID) == 0 {
		return nil, errors.New("user ID is blank")
	}

	updateBuilder := expression.Set(
		expression.Name("updated_at"),
		expression.Value(time.Now().UTC()),
	)

	if update.IsOptedOut != nil {
		updateBuilder = updateBuilder.Set(expression.Name("is_opted_out"), expression.Value(update.IsOptedOut))
	}

	expr, err := expression.NewBuilder().WithUpdate(updateBuilder).Build()
	if err != nil {
		return nil, errors.Wrap(err, "failed to build update expression to update celebration user settings")
	}

	input := dynamodb.UpdateItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			celebrationUserSettingsPrimaryKey: {S: aws.String(userID)},
		},
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ConditionExpression:       expr.Condition(),
		UpdateExpression:          expr.Update(),
		TableName:                 aws.String(db.tableName),
		ReturnValues:              aws.String(dynamodb.ReturnValueAllNew),
	}

	output, err := db.client.UpdateItemWithContext(ctx, &input)
	if err != nil {
		return nil, err
	}

	if output == nil {
		return nil, errors.New("no output was received")
	}

	var celebrationUserSettings celebrationUserSettings
	if err := dynamodbattribute.UnmarshalMap(output.Attributes, &celebrationUserSettings); err != nil {
		return nil, errors.Wrapf(err, "error unmarshaling celebrationUserSettings for user %s", userID)
	}

	return fromDBCelebrationUserSettings(celebrationUserSettings), nil
}

func fromDBCelebrationUserSettings(settings celebrationUserSettings) *percy.CelebrationUserSettings {
	return &percy.CelebrationUserSettings{
		IsOptedOut: settings.IsOptedOut,
	}
}
