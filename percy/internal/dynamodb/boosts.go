package dynamodb

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	"github.com/aws/aws-dax-go/dax"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/pkg/errors"
)

const (
	boostsDBTableNameFormat                      = "paid-boost-opportunity-%s"
	boostsDBTTLDuration                          = 30 * 24 * time.Hour
	boostsDBTablePK                              = "pk"
	boostsDBTableSK                              = "sk"
	boostsDBPKByChannelFormat                    = "channel:%s"
	boostsDBPKByBoostOpportunityFormat           = "boostOpportunity:%s"
	boostsDBArchivedBoostOpportunitySK           = "boostOpportunity:%s"
	boostsDBLatestBoostOpportunitySK             = "boostOpportunity:LATEST"
	boostsDBBoostOpportunityContributionSKFormat = "boostOpportunityContribution:%s"
)

type BoostsDB struct {
	tableName string
	client    dynamodbiface.DynamoDBAPI
}

var _ percy.BoostsDB = &BoostsDB{}

func NewBoostsDB(config DynamoDBConfig) (*BoostsDB, error) {
	if config.Endpoint == nil {
		// no endpoint is provided, use the DynamoDB client
		sess := session.Must(session.NewSessionWithOptions(session.Options{
			Config: aws.Config{Region: aws.String(config.Region)},
		}))

		return &BoostsDB{
			client:    dynamodb.New(sess),
			tableName: fmt.Sprintf(boostsDBTableNameFormat, config.Environment),
		}, nil

	} else {
		// endpoint provided, use DAX client
		daxConfig := dax.DefaultConfig()
		daxConfig.HostPorts = []string{*config.Endpoint}
		daxConfig.Region = config.Region
		daxConfig.ReadRetries = daxReadRetries
		daxConfig.WriteRetries = daxWriteRetries
		daxConfig.RequestTimeout = daxTimeout

		daxClient, err := dax.New(daxConfig)
		if err != nil {
			return nil, err
		}

		return &BoostsDB{
			client:    daxClient,
			tableName: fmt.Sprintf(boostsDBTableNameFormat, config.Environment),
		}, nil
	}
}

type dynamoBoostOpportunity struct {
	PK                 string     `dynamodbav:"pk"`
	SK                 string     `dynamodbav:"sk"`
	ItemType           string     `dynamodbav:"item_type"`
	BoostOpportunityID string     `dynamodbav:"boost_opportunity_id"`
	ChannelID          string     `dynamodbav:"channel_id"`
	StartedAt          time.Time  `dynamodbav:"started_at"`
	ExpiresAt          time.Time  `dynamodbav:"expires_at"`
	EndedAt            *time.Time `dynamodbav:"ended_at,omitempty"`
	UnitsContributed   int        `dynamodbav:"units_contributed"`
	OrderID            *string    `dynamodbav:"order_id,omitempty"`
	TTL                int64      `dynamodbav:"ttl"`
}

type dynamoBoostOpportunityContribution struct {
	PK                 string    `dynamodbav:"pk"`
	SK                 string    `dynamodbav:"sk"`
	ItemType           string    `dynamodbav:"item_type"`
	BoostOpportunityID string    `dynamodbav:"boost_opportunity_id"`
	ChannelID          string    `dynamodbav:"channel_id"`
	UserID             string    `dynamodbav:"user_id"`
	FulfillmentID      string    `dynamodbav:"fulfillment_id"`
	OriginID           string    `dynamodbav:"origin_id"`
	Timestamp          time.Time `dynamodbav:"timestamp"`
	Units              int       `dynamodbav:"units"`
	TTL                int64     `dynamodbav:"ttl"`
}

type boostItemType string

const (
	boostItemBoostOpportunity             boostItemType = "boostOpportunity"
	boostItemBoostOpportunityContribution boostItemType = "boostOpportunityContribution"
)

func (t boostItemType) String() string {
	return string(t)
}

func (db *BoostsDB) CreateBoostOpportunity(ctx context.Context, opportunity percy.BoostOpportunity) error {
	marshaledBoostOpportunity, err := dynamodbattribute.MarshalMap(toDynamoBoostOpportunity(opportunity))
	if err != nil {
		return errors.Wrap(err, "failed to marshal boost opportunity")
	}

	expr, err := expression.NewBuilder().WithCondition(noActiveBoostOpportunityCondition()).Build()
	if err != nil {
		return errors.Wrap(err, "failed to build expression")
	}

	_, err = db.client.PutItemWithContext(ctx,
		&dynamodb.PutItemInput{
			ConditionExpression:       expr.Condition(),
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
			TableName:                 aws.String(db.tableName),
			Item:                      marshaledBoostOpportunity,
		})

	return err
}

func (db *BoostsDB) GetLatestBoostOpportunity(ctx context.Context, channelID string, options percy.BoostsDBReadOptions) (*percy.BoostOpportunity, error) {
	if channelID == "" {
		return nil, errors.New("channel ID must not be blank")
	}

	resp, err := db.client.GetItemWithContext(ctx, &dynamodb.GetItemInput{
		TableName:      aws.String(db.tableName),
		Key:            makeLatestBoostOpportunityKey(channelID),
		ConsistentRead: aws.Bool(options.ConsistentRead),
	})
	if err != nil {
		return nil, err
	}

	if resp == nil || resp.Item == nil {
		return nil, nil
	}

	var dbBoostOpportunity dynamoBoostOpportunity
	if err := dynamodbattribute.UnmarshalMap(resp.Item, &dbBoostOpportunity); err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal DynamoDB boost opportunity")
	}

	boostOpportunity := toBoostOpportunity(dbBoostOpportunity)
	return &boostOpportunity, nil
}

// AddBoostOpportunityContribution records a new boost opportunity contribution by
// 1. add a new entry to DynamoDB to record the contribution transaction
// 2. update boost opportunity to increment impressions
// 3. make a strongly consistent read to return the updated boost opportunity
func (db *BoostsDB) AddBoostOpportunityContribution(ctx context.Context, boostOpportunityID string, contribution percy.BoostOpportunityContribution) (percy.BoostOpportunity, error) {
	if contribution.ChannelID == "" {
		return percy.BoostOpportunity{}, errors.New("channel ID must not be blank")
	}

	if contribution.Units == 0 {
		return percy.BoostOpportunity{}, errors.New("unit quantity contributed must be greater than 0")
	}

	// marshal boost opportunity contribution
	marshaledBoostOpportunityContribution, err := dynamodbattribute.MarshalMap(toDynamoBoostOpportunityContribution(boostOpportunityID, contribution))
	if err != nil {
		return percy.BoostOpportunity{}, errors.Wrap(err, "failed to marshal boost opportunity contributionmarshaledBoostOpportunityContribution")
	}

	// condition to ensure that this contribution has not been recorded yet
	contributionExpr, err := expression.NewBuilder().WithCondition(boostOpportunityContributionDoesNotExistCondition()).Build()
	if err != nil {
		return percy.BoostOpportunity{}, errors.Wrap(err, "failed to build expression for opportunity contribute")
	}

	// expression for adding impressions to boost opportunity
	opportunityUpdateExpr, err := expression.NewBuilder().
		WithUpdate(expression.Add(
			expression.Name("units_contributed"),
			expression.Value(contribution.Units))).
		WithCondition(boostOpportunityActiveCondition()).
		Build()
	if err != nil {
		return percy.BoostOpportunity{}, errors.Wrap(err, "failed to build expression for opportunity update")
	}

	writeItems := []*dynamodb.TransactWriteItem{
		{
			Put: &dynamodb.Put{
				TableName:                 aws.String(db.tableName),
				Item:                      marshaledBoostOpportunityContribution,
				ExpressionAttributeNames:  contributionExpr.Names(),
				ExpressionAttributeValues: contributionExpr.Values(),
				ConditionExpression:       contributionExpr.Condition(),
			},
		},
		{
			Update: &dynamodb.Update{
				TableName:                 aws.String(db.tableName),
				Key:                       makeLatestBoostOpportunityKey(contribution.ChannelID),
				ExpressionAttributeNames:  opportunityUpdateExpr.Names(),
				ExpressionAttributeValues: opportunityUpdateExpr.Values(),
				UpdateExpression:          opportunityUpdateExpr.Update(),
				ConditionExpression:       opportunityUpdateExpr.Condition(),
			},
		},
	}

	// transact write both changes
	if _, err := db.client.TransactWriteItemsWithContext(ctx, &dynamodb.TransactWriteItemsInput{
		TransactItems: writeItems,
	}); err != nil {
		// TODO: re-consider how we want to surface this error for the business logic
		return percy.BoostOpportunity{}, errors.Wrap(err, "failed to transact write boost opportunity contribution")
	}

	// returned the updated boost opportunity
	resp, err := db.client.GetItemWithContext(ctx, &dynamodb.GetItemInput{
		TableName:      aws.String(db.tableName),
		Key:            makeLatestBoostOpportunityKey(contribution.ChannelID),
		ConsistentRead: aws.Bool(true),
	})
	if err != nil {
		return percy.BoostOpportunity{}, errors.Wrap(err, "failed to read updated boost opportunity after update")
	}

	var dbBoostOpportunity dynamoBoostOpportunity
	if err := dynamodbattribute.UnmarshalMap(resp.Item, &dbBoostOpportunity); err != nil {
		return percy.BoostOpportunity{}, errors.Wrap(err, "failed to unmarshal DynamoDB boost opportunity")
	}

	return toBoostOpportunity(dbBoostOpportunity), nil
}

func (db *BoostsDB) GetBoostOpportunityContribution(ctx context.Context, opportunityID, originID string, ops percy.BoostsDBReadOptions) (*percy.BoostOpportunityContribution, error) {
	if opportunityID == "" {
		return nil, errors.New("boost opportunity ID must not be empty")
	}

	if originID == "" {
		return nil, errors.New("origin ID must not be empty")
	}

	resp, err := db.client.GetItemWithContext(ctx, &dynamodb.GetItemInput{
		TableName:      aws.String(db.tableName),
		Key:            makeBoostOpportunityContributionKey(opportunityID, originID),
		ConsistentRead: aws.Bool(ops.ConsistentRead),
	})
	if err != nil {
		return nil, err
	}

	if resp == nil || resp.Item == nil {
		return nil, nil
	}

	var dbContribution dynamoBoostOpportunityContribution
	if err := dynamodbattribute.UnmarshalMap(resp.Item, &dbContribution); err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal DynamoDB boost opportunity contribution")
	}

	contribution := toBoostOpportunityContribution(dbContribution)
	return &contribution, nil
}

func (db *BoostsDB) EndLatestBoostOpportunity(ctx context.Context, channelID string, endedAt time.Time, orderID *string) error {
	if channelID == "" {
		return errors.New("channel ID must not be empty")
	}

	if endedAt.IsZero() {
		return errors.New("ended time must not be zero")
	}

	update := expression.Set(
		expression.Name("ended_at"),
		expression.Value(endedAt.UTC()),
	)

	// order ID is optional if no contribution is made
	if orderID != nil {
		update = update.Set(
			expression.Name("order_id"),
			expression.Value(orderID),
		)
	}

	expr, err := expression.NewBuilder().WithUpdate(update).WithCondition(boostOpportunityActiveCondition()).Build()
	if err != nil {
		return errors.Wrap(err, "failed to build update expression to end boost opportunity")
	}

	resp, err := db.client.UpdateItemWithContext(ctx, &dynamodb.UpdateItemInput{
		TableName:                 aws.String(db.tableName),
		Key:                       makeLatestBoostOpportunityKey(channelID),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		UpdateExpression:          expr.Update(),
		ConditionExpression:       expr.Condition(),
		ReturnValues:              aws.String(dynamodb.ReturnValueAllNew),
	})
	if err != nil {
		return err
	}

	if resp == nil || resp.Attributes == nil {
		return errors.New("no update response received while updating boost opportunity")
	}

	// Make a copy of the boost opportunity and use its start time as part of the sort kye
	// Such that, in addition to the latest boost opportunity, we also have a history of all boost opportunities in the channel sorted by their start time
	go func() {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		var dbBoostOpportunity dynamoBoostOpportunity

		logger := logrus.WithFields(logrus.Fields{
			"channel_id":           dbBoostOpportunity.ChannelID,
			"boost_opportunity_id": dbBoostOpportunity.BoostOpportunityID,
		})

		if err := dynamodbattribute.UnmarshalMap(resp.Attributes, &dbBoostOpportunity); err != nil {
			logger.WithError(err).Error("Failed to unmarshal ended boost opportunity")
			return
		}

		dbBoostOpportunity.SK = fmt.Sprintf(boostsDBArchivedBoostOpportunitySK, dbBoostOpportunity.StartedAt.UTC().Format(time.RFC3339Nano))
		marshalledOpportunity, err := dynamodbattribute.MarshalMap(dbBoostOpportunity)
		if err != nil {
			logger.WithError(err).Error("Failed to marshal ended boost opportunity")
			return
		}

		if _, err = db.client.PutItemWithContext(ctx,
			&dynamodb.PutItemInput{
				TableName: aws.String(db.tableName),
				Item:      marshalledOpportunity,
			},
		); err != nil {
			logger.WithError(err).Error("Failed to archive ended boost opportunity")
			return
		}
	}()

	return nil
}

// toBoostOpportunity converts a DynamoDB BoostOpportunity model to a percy model
func toBoostOpportunity(opportunity dynamoBoostOpportunity) percy.BoostOpportunity {
	return percy.BoostOpportunity{
		ChannelID:        opportunity.ChannelID,
		ID:               opportunity.BoostOpportunityID,
		UnitsContributed: opportunity.UnitsContributed,
		StartedAt:        opportunity.StartedAt,
		ExpiresAt:        opportunity.ExpiresAt,
		EndedAt:          opportunity.EndedAt,
		OrderID:          opportunity.OrderID,
	}
}

// toDynamoBoostOpportunity converts a percy BoostOpportunity model to a DynamoDB model
func toDynamoBoostOpportunity(opportunity percy.BoostOpportunity) dynamoBoostOpportunity {
	return dynamoBoostOpportunity{
		PK:                 fmt.Sprintf(boostsDBPKByChannelFormat, opportunity.ChannelID),
		SK:                 boostsDBLatestBoostOpportunitySK,
		ChannelID:          opportunity.ChannelID,
		BoostOpportunityID: opportunity.ID,
		ItemType:           boostItemBoostOpportunity.String(),
		StartedAt:          opportunity.StartedAt.UTC(),
		ExpiresAt:          opportunity.ExpiresAt.UTC(),
		EndedAt:            opportunity.EndedAt,
		OrderID:            opportunity.OrderID,
		UnitsContributed:   opportunity.UnitsContributed,
		TTL:                time.Now().Add(boostsDBTTLDuration).Unix(),
	}
}

func toBoostOpportunityContribution(contribution dynamoBoostOpportunityContribution) percy.BoostOpportunityContribution {
	return percy.BoostOpportunityContribution{
		ChannelID: contribution.ChannelID,
		UserID:    contribution.UserID,
		OriginID:  contribution.OriginID,
		Units:     contribution.Units,
		Timestamp: contribution.Timestamp,
	}
}

// toDynamoBoostOpportunityContribution converts a percy BoostOpportunityContribution model to a DynamoDB model
func toDynamoBoostOpportunityContribution(boostOpportunityID string, contribution percy.BoostOpportunityContribution) dynamoBoostOpportunityContribution {
	return dynamoBoostOpportunityContribution{
		PK:                 fmt.Sprintf(boostsDBPKByBoostOpportunityFormat, boostOpportunityID),
		SK:                 fmt.Sprintf(boostsDBBoostOpportunityContributionSKFormat, contribution.OriginID),
		BoostOpportunityID: boostOpportunityID,
		ItemType:           boostItemBoostOpportunityContribution.String(),
		ChannelID:          contribution.ChannelID,
		UserID:             contribution.UserID,
		Units:              contribution.Units,
		OriginID:           contribution.OriginID,
		Timestamp:          contribution.Timestamp.UTC(),
		TTL:                time.Now().Add(boostsDBTTLDuration).Unix(),
	}
}

func makeLatestBoostOpportunityKey(channelID string) map[string]*dynamodb.AttributeValue {
	return map[string]*dynamodb.AttributeValue{
		boostsDBTablePK: {S: aws.String(fmt.Sprintf(boostsDBPKByChannelFormat, channelID))},
		boostsDBTableSK: {S: aws.String(boostsDBLatestBoostOpportunitySK)},
	}
}

func makeBoostOpportunityContributionKey(opportunityID, originID string) map[string]*dynamodb.AttributeValue {
	return map[string]*dynamodb.AttributeValue{
		boostsDBTablePK: {S: aws.String(fmt.Sprintf(boostsDBPKByBoostOpportunityFormat, opportunityID))},
		boostsDBTableSK: {S: aws.String(fmt.Sprintf(boostsDBBoostOpportunityContributionSKFormat, originID))},
	}
}

// condition to check if there is no active boost opportunity
func noActiveBoostOpportunityCondition() expression.ConditionBuilder {
	return expression.Name("ended_at").AttributeNotExists().
		Or(expression.Name("expires_at").LessThan(expression.Value(time.Now().UTC())))
}

// condition to ensure that a boost opportunity hasn't been "completed"
func boostOpportunityActiveCondition() expression.ConditionBuilder {
	return expression.Name("ended_at").AttributeNotExists()
}

// condition to ensure that the contribution has not been written yet
func boostOpportunityContributionDoesNotExistCondition() expression.ConditionBuilder {
	return expression.Name(boostsDBTablePK).AttributeNotExists().And(expression.Name(boostsDBTableSK).AttributeNotExists())
}
