package dynamodb

import (
	"context"
	"fmt"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"github.com/aws/aws-dax-go/dax"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/pkg/errors"
)

const (
	participationsDBFormat = "participations-%s"

	participationsHashKey  = "channel_id"
	participationsRangeKey = "timestamp"
)

type participationRecord struct {
	ChannelID string    `dynamodbav:"channel_id"`
	Timestamp time.Time `dynamodbav:"timestamp"`
	UserID    string    `dynamodbav:"user_id"`

	ParticipationSource string `dynamodbav:"participation_source"`
	ParticipationAction string `dynamodbav:"participation_action"`
	Quantity            int    `dynamodbav:"quantity"`
	EventID             string `dynamodbav:"event_id"`

	TTL int64 `dynamodbav:"ttl"`
}

type ParticipationsDB struct {
	tableName string
	client    dynamodbiface.DynamoDBAPI
}

func NewParticipationsDB(config DynamoDBConfig) (*ParticipationsDB, error) {
	if config.Endpoint == nil {
		// no endpoint is provided, use the DynamoDB client
		sess := session.Must(session.NewSessionWithOptions(session.Options{
			Config: aws.Config{Region: aws.String(config.Region)},
		}))

		return &ParticipationsDB{
			client:    dynamodb.New(sess),
			tableName: fmt.Sprintf(participationsDBFormat, config.Environment),
		}, nil

	} else {
		// endpoint provided, use DAX client
		daxConfig := dax.DefaultConfig()
		daxConfig.HostPorts = []string{*config.Endpoint}
		daxConfig.Region = config.Region
		daxConfig.ReadRetries = daxReadRetries
		daxConfig.WriteRetries = daxWriteRetries
		daxConfig.RequestTimeout = daxTimeout

		daxClient, err := dax.New(daxConfig)
		if err != nil {
			return nil, err
		}

		return &ParticipationsDB{
			client:    daxClient,
			tableName: fmt.Sprintf(participationsDBFormat, config.Environment),
		}, nil
	}
}

// RecordChannelParticipation writes to the participations table with a new channel participation.
func (db *ParticipationsDB) RecordChannelParticipation(ctx context.Context, participation percy.Participation) error {
	if len(participation.ChannelID) == 0 {
		return errors.New("channel ID is blank")
	}

	if participation.Timestamp.IsZero() {
		return errors.New("timestamp has zero value")
	}

	if participation.Quantity == 0 {
		return errors.New("quantity is 0")
	}

	input, err := db.getPutParticipationInput(participation)
	if err != nil {
		return err
	}

	_, err = db.client.PutItemWithContext(ctx, &input)
	return err
}

// GetChannelParticipations queries for all participation records in the participations table, limited by the query parameters.
func (db *ParticipationsDB) GetChannelParticipations(ctx context.Context, channelID string, params percy.ParticipationQueryParams) ([]percy.Participation, error) {
	if len(channelID) == 0 {
		return nil, errors.New("channel ID is blank")
	}

	if params.EndTime.Before(params.StartTime) {
		return nil, errors.New("end time is before start time")
	}

	expr, err := expression.NewBuilder().WithKeyCondition(expression.KeyAnd(
		expression.Key(participationsHashKey).Equal(expression.Value(channelID)),
		expression.Key(participationsRangeKey).Between(expression.Value(params.StartTime), expression.Value(params.EndTime)))).Build()
	if err != nil {
		return nil, errors.New("failed to build the query condition expression")
	}

	output, err := db.client.QueryWithContext(ctx, &dynamodb.QueryInput{
		TableName:                 aws.String(db.tableName),
		KeyConditionExpression:    expr.KeyCondition(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ConsistentRead:            aws.Bool(true),
	})
	if err != nil {
		return nil, err
	}

	if output == nil || len(output.Items) == 0 {
		return nil, nil
	}

	var unmarshaledParticipations []participationRecord
	err = dynamodbattribute.UnmarshalListOfMaps(output.Items, &unmarshaledParticipations)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal participations")
	}

	participations := make([]percy.Participation, 0, len(unmarshaledParticipations))
	for _, p := range unmarshaledParticipations {
		participations = append(participations, percy.Participation{
			ChannelID: p.ChannelID,
			Timestamp: p.Timestamp,
			User:      percy.NewUser(p.UserID),
			ParticipationIdentifier: percy.ParticipationIdentifier{
				Source: percy.ParticipationSource(p.ParticipationSource),
				Action: percy.ParticipationAction(p.ParticipationAction),
			},
			Quantity: p.Quantity,
		})
	}

	return participations, nil
}

func (db *ParticipationsDB) getPutParticipationInput(participation percy.Participation) (dynamodb.PutItemInput, error) {
	marshaledParticipation, err := dynamodbattribute.MarshalMap(participationRecord{
		ChannelID:           participation.ChannelID,
		Timestamp:           participation.Timestamp.UTC(),
		UserID:              participation.User.ID(),
		ParticipationSource: string(participation.Source),
		ParticipationAction: string(participation.Action),
		Quantity:            participation.Quantity,
		EventID:             participation.EventID,
		TTL:                 time.Now().Add(ttlDuration).Unix(),
	})
	if err != nil {
		return dynamodb.PutItemInput{}, errors.Wrap(err, "failed to marshal participation")
	}

	return dynamodb.PutItemInput{
		TableName: aws.String(db.tableName),
		Item:      marshaledParticipation,
	}, nil
}
