package dynamodb

import (
	"context"
	"fmt"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"github.com/aws/aws-dax-go/dax"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/google/uuid"
	"github.com/pkg/errors"
)

const (
	celebrationsDBFormat          = "celebrations-config-%s"
	celebrationsConfigsPrimaryKey = "channel_id"

	celebrationsDaxWriteRetries = 1
	celebrationsDaxReadRetries  = 1
	celebrationsDaxTimeout      = 500 * time.Millisecond
)

type celebrationConfig struct {
	ChannelID    string                  `dynamodbav:"channel_id"`
	Enabled      bool                    `dynamodbav:"enabled"`
	Celebrations *map[string]celebration `dynamodbav:"celebrations,omitempty"`
}

type celebration struct {
	ID                   string                     `dynamodbav:"id"`
	Enabled              bool                       `dynamodbav:"enabled"`
	EventType            percy.CelebrationEventType `dynamodbav:"event_type"`
	EventThreshold       int64                      `dynamodbav:"event_threshold"`
	CelebrationEffect    percy.CelebrationEffect    `dynamodbav:"effect"`
	CelebrationArea      percy.CelebrationArea      `dynamodbav:"area"`
	CelebrationIntensity int64                      `dynamodbav:"intensity"`
	CelebrationDuration  time.Duration              `dynamodbav:"duration"`
}

type CelebrationConfigsDB struct {
	tableName string
	client    dynamodbiface.DynamoDBAPI
}

func NewCelebrationsConfigDB(config DynamoDBConfig) (*CelebrationConfigsDB, error) {
	if config.Endpoint == nil {
		// no endpoint is provided, use the DynamoDB client
		sess := session.Must(session.NewSessionWithOptions(session.Options{
			Config: aws.Config{Region: aws.String(config.Region)},
		}))

		return &CelebrationConfigsDB{
			client:    dynamodb.New(sess),
			tableName: fmt.Sprintf(celebrationsDBFormat, config.Environment),
		}, nil

	}

	// endpoint provided, use DAX client
	daxConfig := dax.DefaultConfig()
	daxConfig.HostPorts = []string{*config.Endpoint}
	daxConfig.Region = config.Region
	daxConfig.ReadRetries = celebrationsDaxReadRetries
	daxConfig.WriteRetries = celebrationsDaxWriteRetries
	daxConfig.RequestTimeout = celebrationsDaxTimeout

	daxClient, err := dax.New(daxConfig)
	if err != nil {
		return nil, err
	}

	return &CelebrationConfigsDB{
		client:    daxClient,
		tableName: fmt.Sprintf(celebrationsDBFormat, config.Environment),
	}, nil
}

func (db *CelebrationConfigsDB) GetCelebrationConfig(ctx context.Context, channelID string) (*percy.CelebrationConfig, error) {
	if len(channelID) == 0 {
		return nil, errors.New("channel ID is blank")
	}

	resp, err := db.client.GetItemWithContext(ctx, &dynamodb.GetItemInput{
		TableName: aws.String(db.tableName),
		Key: map[string]*dynamodb.AttributeValue{
			celebrationsConfigsPrimaryKey: {S: aws.String(channelID)},
		},
	})
	if err != nil {
		return nil, err
	}

	if resp == nil || resp.Item == nil {
		return nil, nil
	}

	var celebrationConfig celebrationConfig
	if err := dynamodbattribute.UnmarshalMap(resp.Item, &celebrationConfig); err != nil {
		return nil, errors.Wrapf(err, "error unmarshaling celebrationConfig for channel %s", channelID)
	}

	var celebrations map[string]percy.Celebration
	if celebrationConfig.Celebrations != nil {
		celebrations = make(map[string]percy.Celebration)
		for key, celebration := range *celebrationConfig.Celebrations {
			celebrations[key] = percy.Celebration{
				CelebrationID:        celebration.ID,
				Enabled:              celebration.Enabled,
				EventType:            celebration.EventType,
				EventThreshold:       celebration.EventThreshold,
				CelebrationEffect:    celebration.CelebrationEffect,
				CelebrationArea:      celebration.CelebrationArea,
				CelebrationIntensity: celebration.CelebrationIntensity,
				CelebrationDuration:  celebration.CelebrationDuration,
			}
		}
	}

	return &percy.CelebrationConfig{
		ChannelID:    celebrationConfig.ChannelID,
		Enabled:      celebrationConfig.Enabled,
		Celebrations: celebrations,
	}, nil
}

func (db *CelebrationConfigsDB) UpdateCelebrationConfig(ctx context.Context, channelID string, isEnabled bool) (*percy.CelebrationConfig, error) {
	if len(channelID) == 0 {
		return nil, errors.New("channel ID is blank")
	}

	updateBuilder := expression.Set(expression.Name("enabled"), expression.Value(isEnabled)).Set(
		expression.Name("updated_at"),
		expression.Value(time.Now().UTC()),
	)

	expr, err := expression.NewBuilder().WithUpdate(updateBuilder).Build()
	if err != nil {
		return nil, errors.Wrap(err, "failed to build update expression to update config")
	}

	return db.updateConfigAndReturnNew(ctx, channelID, expr)
}

func (db *CelebrationConfigsDB) CreateCelebration(ctx context.Context, channelID string, args percy.CelebrationArgs) (*percy.Celebration, error) {
	if len(channelID) == 0 {
		return nil, errors.New("channel ID is blank")
	}
	if args.CelebrationArea == nil {
		return nil, errors.New("celebration area is blank")
	}
	if args.CelebrationDuration == nil {
		return nil, errors.New("duration is blank")
	}
	if args.CelebrationEffect == nil {
		return nil, errors.New("effect is blank")
	}
	if args.CelebrationIntensity == nil {
		return nil, errors.New("intensity is blank")
	}
	if args.Enabled == nil {
		return nil, errors.New("enabled is blank")
	}
	if args.EventThreshold == nil {
		return nil, errors.New("threshold is blank")
	}
	if args.EventType == nil {
		return nil, errors.New("type is blank")
	}

	celebrationID := uuid.New().String()
	celebration := celebration{
		ID:                   celebrationID,
		Enabled:              *args.Enabled,
		EventType:            *args.EventType,
		EventThreshold:       *args.EventThreshold,
		CelebrationEffect:    *args.CelebrationEffect,
		CelebrationArea:      *args.CelebrationArea,
		CelebrationIntensity: *args.CelebrationIntensity,
		CelebrationDuration:  *args.CelebrationDuration,
	}

	update := expression.Set(
		expression.Name(fmt.Sprintf("celebrations.%s", celebrationID)),
		expression.Value(celebration),
	).Set(
		expression.Name("updated_at"),
		expression.Value(time.Now().UTC()),
	)

	condition := expression.Name(fmt.Sprintf("celebrations.%s", celebrationID)).AttributeNotExists().And(
		expression.Name("celebrations").AttributeExists(),
	)

	expr, err := expression.NewBuilder().
		WithUpdate(update).WithCondition(condition).Build()
	if err != nil {
		return nil, errors.Wrap(err, "failed to build update expression")
	}

	config, err := db.updateConfigAndReturnNew(ctx, channelID, expr)
	if err != nil {
		return nil, err
	}

	newCelebration, exists := config.Celebrations[celebrationID]
	if !exists {
		return nil, errors.New(fmt.Sprintf("celebration with id %s does not exist after creation!", celebrationID))
	}

	return &newCelebration, nil
}

func (db *CelebrationConfigsDB) UpdateCelebration(ctx context.Context, channelID, celebrationID string, args percy.CelebrationArgs) (*percy.Celebration, error) {
	if len(channelID) == 0 {
		return nil, errors.New("channel ID is blank")
	}

	if len(celebrationID) == 0 {
		return nil, errors.New("celebration ID is blank")
	}

	updateBuilder := expression.Set(
		expression.Name(fmt.Sprintf("celebrations.%s.updated_at", celebrationID)),
		expression.Value(time.Now().UTC()),
	)

	if args.CelebrationArea != nil {
		if percy.ValidAreas[*args.CelebrationArea] {
			updateBuilder.Set(
				expression.Name(fmt.Sprintf("celebrations.%s.area", celebrationID)),
				expression.Value(*args.CelebrationArea),
			)
		} else {
			return nil, errors.New("Invalid celebration area")
		}
	}

	if args.CelebrationDuration != nil {
		updateBuilder.Set(
			expression.Name(fmt.Sprintf("celebrations.%s.duration", celebrationID)),
			expression.Value(*args.CelebrationDuration),
		)
	}

	if args.CelebrationEffect != nil {
		if percy.ValidEffects[*args.CelebrationEffect] {
			updateBuilder.Set(
				expression.Name(fmt.Sprintf("celebrations.%s.effect", celebrationID)),
				expression.Value(*args.CelebrationEffect),
			)
		} else {
			return nil, errors.New("Invalid celebration effect")
		}
	}

	if args.CelebrationIntensity != nil {
		updateBuilder.Set(
			expression.Name(fmt.Sprintf("celebrations.%s.intensity", celebrationID)),
			expression.Value(*args.CelebrationIntensity),
		)
	}

	if args.Enabled != nil {
		updateBuilder.Set(
			expression.Name(fmt.Sprintf("celebrations.%s.enabled", celebrationID)),
			expression.Value(*args.Enabled),
		)
	}

	if args.EventThreshold != nil {
		updateBuilder.Set(
			expression.Name(fmt.Sprintf("celebrations.%s.event_threshold", celebrationID)),
			expression.Value(*args.EventThreshold),
		)
	}

	if args.EventType != nil {
		if percy.ValidEventTypes[*args.EventType] {
			updateBuilder.Set(
				expression.Name(fmt.Sprintf("celebrations.%s.event_type", celebrationID)),
				expression.Value(*args.EventType),
			)
		} else {
			return nil, errors.New("Invalid celebration event type")
		}
	}

	condition := expression.Name(fmt.Sprintf("celebrations.%s", celebrationID)).AttributeExists().And(
		expression.Name("celebrations").AttributeExists(),
	)

	expr, err := expression.NewBuilder().
		WithUpdate(updateBuilder).WithCondition(condition).Build()
	if err != nil {
		return nil, errors.Wrap(err, "failed to build update expression")
	}

	config, err := db.updateConfigAndReturnNew(ctx, channelID, expr)
	if err != nil {
		return nil, err
	}

	celebration, exists := config.Celebrations[celebrationID]
	if !exists {
		return nil, errors.New(fmt.Sprintf("celebration with id %s does not exist after update!", celebrationID))
	}

	return &celebration, nil
}

func (db *CelebrationConfigsDB) DeleteCelebration(ctx context.Context, channelID, celebrationID string) error {
	if len(channelID) == 0 {
		return errors.New("channel ID is blank")
	}
	if len(celebrationID) == 0 {
		return errors.New("channel ID is blank")
	}

	update := expression.Remove(
		expression.Name(fmt.Sprintf("celebrations.%s", celebrationID)),
	)

	condition := expression.Name(fmt.Sprintf("celebrations.%s", celebrationID)).AttributeExists().And(
		expression.Name("celebrations").AttributeExists(),
	)

	expr, err := expression.NewBuilder().
		WithUpdate(update).
		WithCondition(condition).Build()
	if err != nil {
		return errors.Wrap(err, "failed to build update expression")
	}

	_, err = db.updateConfigAndReturnNew(ctx, channelID, expr)
	if err != nil {
		return err
	}

	return nil
}

func (db *CelebrationConfigsDB) GetDefaultCelebrationConfig(channelID string) *percy.CelebrationConfig {
	bitsCelebrationID := uuid.New().String()
	subsCelebrationID := uuid.New().String()
	return &percy.CelebrationConfig{
		ChannelID: channelID,
		Enabled:   true,
		Celebrations: map[string]percy.Celebration{
			bitsCelebrationID: {
				CelebrationID:        bitsCelebrationID,
				Enabled:              true,
				EventType:            percy.EventTypeCheer,
				EventThreshold:       1000,
				CelebrationEffect:    percy.EffectFireworks,
				CelebrationArea:      percy.AreaEverywhere,
				CelebrationIntensity: 14,
				CelebrationDuration:  time.Duration(5) * time.Second,
			},
			subsCelebrationID: {
				CelebrationID:        subsCelebrationID,
				Enabled:              true,
				EventType:            percy.EventTypeSubscriptionGift,
				EventThreshold:       5,
				CelebrationEffect:    percy.EffectFireworks,
				CelebrationArea:      percy.AreaEverywhere,
				CelebrationIntensity: 14,
				CelebrationDuration:  time.Duration(5) * time.Second,
			},
		},
	}
}

func (db *CelebrationConfigsDB) BackfillCelebrationConfig(ctx context.Context, channelID string) (*percy.CelebrationConfig, error) {
	if len(channelID) == 0 {
		return nil, errors.New("channel ID is blank")
	}

	defaultConfig := db.GetDefaultCelebrationConfig(channelID)

	updateBuilder := expression.Set(expression.Name("enabled"), expression.Value(defaultConfig.Enabled)).Set(
		expression.Name("updated_at"),
		expression.Value(time.Now().UTC()),
	).Set(expression.Name("celebrations"), expression.Value(fromCelebrationMap(defaultConfig.Celebrations)))

	expr, err := expression.NewBuilder().WithUpdate(updateBuilder).Build()
	if err != nil {
		return nil, errors.Wrap(err, "failed to build update expression to update config")
	}

	return db.updateConfigAndReturnNew(ctx, channelID, expr)
}

// ONLY FOR INTEGRATION TESTS
func (db *CelebrationConfigsDB) DeleteCelebrationConfig(ctx context.Context, channelID string) error {
	if len(channelID) == 0 {
		return errors.New("channel ID is blank")
	}

	_, err := db.client.DeleteItemWithContext(ctx, &dynamodb.DeleteItemInput{
		TableName: aws.String(db.tableName),
		Key: map[string]*dynamodb.AttributeValue{
			celebrationsConfigsPrimaryKey: {S: aws.String(channelID)},
		},
	})
	if err != nil {
		return err
	}

	return nil
}

func (db *CelebrationConfigsDB) updateConfigAndReturnNew(ctx context.Context, channelID string, expr expression.Expression) (*percy.CelebrationConfig, error) {
	input := dynamodb.UpdateItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			celebrationsConfigsPrimaryKey: {S: aws.String(channelID)},
		},
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ConditionExpression:       expr.Condition(),
		UpdateExpression:          expr.Update(),
		TableName:                 aws.String(db.tableName),
		ReturnValues:              aws.String(dynamodb.ReturnValueAllNew),
	}

	output, err := db.client.UpdateItemWithContext(ctx, &input)
	if err != nil {
		return nil, err
	}

	if output == nil {
		return nil, errors.New("no output was received")
	}

	var config celebrationConfig
	if err := dynamodbattribute.UnmarshalMap(output.Attributes, &config); err != nil {
		return nil, errors.Wrapf(err, "error unmarshaling celebration config for channel %s", channelID)
	}

	return fromDBCelebrationConfig(config), nil
}

func fromDBCelebrationConfig(dbConfig celebrationConfig) *percy.CelebrationConfig {
	var percyCelebrations map[string]percy.Celebration
	if dbConfig.Celebrations != nil {
		percyCelebrations = make(map[string]percy.Celebration)
		for key, celebration := range *dbConfig.Celebrations {
			percyCelebrations[key] = percy.Celebration{
				CelebrationID:        celebration.ID,
				Enabled:              celebration.Enabled,
				EventType:            celebration.EventType,
				EventThreshold:       celebration.EventThreshold,
				CelebrationEffect:    celebration.CelebrationEffect,
				CelebrationArea:      celebration.CelebrationArea,
				CelebrationIntensity: celebration.CelebrationIntensity,
				CelebrationDuration:  celebration.CelebrationDuration,
			}
		}
	}

	return &percy.CelebrationConfig{
		ChannelID:    dbConfig.ChannelID,
		Enabled:      dbConfig.Enabled,
		Celebrations: percyCelebrations,
	}
}

func fromCelebrationMap(percyCelebrations map[string]percy.Celebration) map[string]celebration {
	dynamoCelebrations := make(map[string]celebration)
	for key, percyCelebration := range percyCelebrations {
		cel := celebration{
			ID:                   key,
			Enabled:              percyCelebration.Enabled,
			EventType:            percyCelebration.EventType,
			EventThreshold:       percyCelebration.EventThreshold,
			CelebrationEffect:    percyCelebration.CelebrationEffect,
			CelebrationArea:      percyCelebration.CelebrationArea,
			CelebrationIntensity: percyCelebration.CelebrationIntensity,
			CelebrationDuration:  percyCelebration.CelebrationDuration,
		}

		dynamoCelebrations[key] = cel
	}

	return dynamoCelebrations
}
