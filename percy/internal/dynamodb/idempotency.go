package dynamodb

import (
	"context"
	"fmt"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"github.com/aws/aws-dax-go/dax"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/pkg/errors"
)

const (
	idempotencyTableNameFormat = "idempotency-%s"

	idempotencyHashKey  = "namespace.transaction_id"
	idempotencyRangeKey = "task_name"

	idempotencyHashKeyFormat = "%s.%s"
)

type idempotencyRecord struct {
	NamespaceTransactionID string                     `dynamodbav:"namespace.transaction_id"`
	Namespace              percy.IdempotencyNamespace `dynamodbav:"namespace"`
	TransactionID          string                     `dynamodbav:"transaction_id"`
	Task                   string                     `dynamodbav:"task_name"`
	State                  percy.IdempotencyState     `dynamodbav:"task_state"`
	LastUpdated            time.Time                  `dynamodbav:"last_updated"`
	Error                  string                     `dynamodbav:"task_error"`
}

type IdempotencyDB struct {
	tableName string
	client    dynamodbiface.DynamoDBAPI
}

func NewIdempotencyDB(config DynamoDBConfig) (*IdempotencyDB, error) {
	if config.Endpoint == nil {
		// no endpoint is provided, use the DynamoDB client
		sess := session.Must(session.NewSessionWithOptions(session.Options{
			Config: aws.Config{Region: aws.String(config.Region)},
		}))

		return &IdempotencyDB{
			client:    dynamodb.New(sess),
			tableName: fmt.Sprintf(idempotencyTableNameFormat, config.Environment),
		}, nil

	} else {
		// endpoint provided, use DAX client
		daxConfig := dax.DefaultConfig()
		daxConfig.HostPorts = []string{*config.Endpoint}
		daxConfig.Region = config.Region
		daxConfig.ReadRetries = daxReadRetries
		daxConfig.WriteRetries = daxWriteRetries
		daxConfig.RequestTimeout = daxTimeout

		daxClient, err := dax.New(daxConfig)
		if err != nil {
			return nil, err
		}

		return &IdempotencyDB{
			client:    daxClient,
			tableName: fmt.Sprintf(idempotencyTableNameFormat, config.Environment),
		}, nil
	}
}

func (db *IdempotencyDB) Update(ctx context.Context, namespace percy.IdempotencyNamespace, transactionID, taskName string, update percy.IdempotentTaskUpdate) (*percy.IdempotentTask, error) {
	if len(namespace) == 0 {
		return nil, errors.New("namespace is blank")
	}

	if len(transactionID) == 0 {
		return nil, errors.New("transactionID is blank")
	}

	if len(taskName) == 0 {
		return nil, errors.New("taskName is blank")
	}

	updateBuilder := expression.
		Set(expression.Name("last_updated"), expression.Value(time.Now().UTC())).
		Set(expression.Name("task_state"), expression.Value(update.State))

	if update.Error != nil {
		updateBuilder = updateBuilder.Set(expression.Name("task_error"), expression.Value(update.Error.Error()))
	}

	expr, err := expression.NewBuilder().WithUpdate(updateBuilder).Build()
	if err != nil {
		return nil, errors.Wrap(err, "failed to build update expression to update idempotent task")
	}

	input := dynamodb.UpdateItemInput{
		Key:                       makeKey(namespace, transactionID, taskName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ConditionExpression:       expr.Condition(),
		UpdateExpression:          expr.Update(),
		TableName:                 aws.String(db.tableName),
		ReturnValues:              aws.String(dynamodb.ReturnValueAllNew),
	}

	output, err := db.client.UpdateItemWithContext(ctx, &input)
	if err != nil {
		return nil, err
	}

	if output == nil {
		return nil, errors.New("no output was received")
	}

	var record idempotencyRecord
	if err := dynamodbattribute.UnmarshalMap(output.Attributes, &record); err != nil {
		return nil, errors.Wrapf(err, "error unmarshaling idempotent task for namespace %s transactionID %s task %s", namespace, transactionID, taskName)
	}

	convertedRecord := fromDBIdempotencyRecord(record)

	return &convertedRecord, nil
}

func (db *IdempotencyDB) Get(ctx context.Context, namespace percy.IdempotencyNamespace, transactionID, taskName string) (*percy.IdempotentTask, error) {
	if len(namespace) == 0 {
		return nil, errors.New("namespace is blank")
	}

	if len(transactionID) == 0 {
		return nil, errors.New("transactionID is blank")
	}

	if len(taskName) == 0 {
		return nil, errors.New("taskName is blank")
	}

	resp, err := db.client.GetItemWithContext(ctx, &dynamodb.GetItemInput{
		TableName:      aws.String(db.tableName),
		Key:            makeKey(namespace, transactionID, taskName),
		ConsistentRead: aws.Bool(true),
	})
	if err != nil {
		return nil, err
	}
	if resp == nil || resp.Item == nil {
		return nil, nil
	}

	var record idempotencyRecord
	if err := dynamodbattribute.UnmarshalMap(resp.Item, &record); err != nil {
		return nil, errors.Wrapf(err, "error unmarshaling idempotent task for namespace %s transactionID %s task %s", namespace, transactionID, taskName)
	}

	convertedRecord := fromDBIdempotencyRecord(record)
	return &convertedRecord, nil
}

func fromDBIdempotencyRecord(record idempotencyRecord) percy.IdempotentTask {
	return percy.IdempotentTask{
		TransactionID: record.TransactionID,
		Namespace:     record.Namespace,
		Task:          record.Task,
		State:         record.State,
		LastUpdated:   record.LastUpdated,
	}
}

func makeKey(namespace percy.IdempotencyNamespace, transactionID, taskName string) map[string]*dynamodb.AttributeValue {
	return map[string]*dynamodb.AttributeValue{
		idempotencyHashKey:  {S: aws.String(fmt.Sprintf(idempotencyHashKeyFormat, string(namespace), transactionID))},
		idempotencyRangeKey: {S: aws.String(taskName)},
	}
}
