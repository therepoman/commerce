package dynamodb

import (
	"context"
	"fmt"
	"time"

	"github.com/aws/aws-dax-go/dax"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/pkg/errors"
)

const (
	creatorAnniversariesTableNameFormat = "creator-anniversaries-notif-%s"
	creatorAnniversariesHashKey         = "channel_id_campaign_type"
	creatorTtlDuration                  = time.Hour * 24 * 30 // data should be expired after 30 days
)

type creatorAnniversariesRecord struct {
	ChannelIDCampaignKey string `dynamodbav:"channel_id_campaign_type"`
	AnniversaryDate      string `dynamodbav:"anniversary_date"`
	NotificationTraceId  string `dynamodbav:"notification_trace_id"`
	TTL                  int64  `dynamodbav:"ttl"`
}

type CreatorAnniversariesNotificationDB struct {
	tableName string
	client    dynamodbiface.DynamoDBAPI
}

func NewCreatorAnniversariesNotificationDB(config DynamoDBConfig) (*CreatorAnniversariesNotificationDB, error) {
	if config.Endpoint == nil {
		// no endpoint is provided, use the DynamoDB client
		sess := session.Must(session.NewSessionWithOptions(session.Options{
			Config: aws.Config{Region: aws.String(config.Region)},
		}))

		return &CreatorAnniversariesNotificationDB{
			client:    dynamodb.New(sess),
			tableName: fmt.Sprintf(creatorAnniversariesTableNameFormat, config.Environment),
		}, nil

	} else {
		// endpoint provided, use DAX client
		daxConfig := dax.DefaultConfig()
		daxConfig.HostPorts = []string{*config.Endpoint}
		daxConfig.Region = config.Region
		daxConfig.ReadRetries = daxReadRetries
		daxConfig.WriteRetries = daxWriteRetries
		daxConfig.RequestTimeout = daxTimeout

		daxClient, err := dax.New(daxConfig)
		if err != nil {
			return nil, err
		}

		return &CreatorAnniversariesNotificationDB{
			client:    daxClient,
			tableName: fmt.Sprintf(creatorAnniversariesTableNameFormat, config.Environment),
		}, nil
	}
}

func (db *CreatorAnniversariesNotificationDB) PutCreatorAnniversaryNotification(ctx context.Context, channelID string, anniversaryDate time.Time, notificationTraceId string, campaignType string) error {
	if channelID == "" {
		return errors.Errorf("no channel id associated with notification: %s", notificationTraceId)
	}
	if notificationTraceId == "" {
		return errors.Errorf("no notification trace id")
	}
	if campaignType == "" {
		return errors.Errorf("no campaignType")
	}
	key := makeChannelIDCampaignKey(channelID, campaignType)
	record := &creatorAnniversariesRecord{
		ChannelIDCampaignKey: key,
		AnniversaryDate:      anniversaryDate.String(),
		NotificationTraceId:  notificationTraceId,
		TTL:                  (time.Now().Add(creatorTtlDuration)).Unix(),
	}
	marshaledRecord, err := dynamodbattribute.MarshalMap(record)
	if err != nil {
		return errors.New("failed to marshal chat notification token dynamodb db entry")
	}

	_, err = db.client.PutItemWithContext(ctx, &dynamodb.PutItemInput{
		TableName: aws.String(db.tableName),
		Item:      marshaledRecord,
	})

	return err
}

func (db *CreatorAnniversariesNotificationDB) ChannelHasSentAnniversaryNotification(ctx context.Context, channelID string, campaignType string) (bool, error) {
	key := makeChannelIDCampaignKey(channelID, campaignType)
	resp, err := db.client.GetItemWithContext(ctx, &dynamodb.GetItemInput{
		TableName: aws.String(db.tableName),
		Key: map[string]*dynamodb.AttributeValue{
			creatorAnniversariesHashKey: {S: aws.String(key)},
		},
	})
	if err != nil {
		return false, err
	}

	if resp == nil || len(resp.Item) == 0 {
		return false, nil
	}

	return true, nil
}

func (db *CreatorAnniversariesNotificationDB) DeleteCreatorAnniversaryNotificationByChannelId(ctx context.Context, channelID string, campaignType string) error {
	if channelID == "" {
		return errors.New("channel id cannot be empty")
	}
	key := makeChannelIDCampaignKey(channelID, campaignType)
	_, err := db.client.DeleteItemWithContext(ctx, &dynamodb.DeleteItemInput{
		TableName: aws.String(db.tableName),
		Key: map[string]*dynamodb.AttributeValue{
			creatorAnniversariesHashKey: {S: aws.String(key)},
		},
	})

	return err
}

func makeChannelIDCampaignKey(channelID string, campaignType string) string {
	return fmt.Sprintf("%s_%s", channelID, campaignType)
}
