package dynamodb

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	"github.com/aws/aws-dax-go/dax"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/pkg/errors"
)

const (
	participantsDBFormat = "participants-%s"

	participantsHashKey  = "hype_train_id"
	participantsRangeKey = "user_id"

	participationCounterFormat    = "%s___%s___%s"
	participationCounterPrefix    = "participation"
	participationCounterDelimiter = "___"
)

type baseParticipantRecord struct {
	HypeTrainID string   `dynamodbav:"hype_train_id"`
	UserID      string   `dynamodbav:"user_id"`
	ChannelID   string   `dynamodbav:"channel_id"`
	Rewards     []reward `dynamodbav:"rewards"`
	TTL         int64    `dynamodbav:"ttl"`
}

type ParticipantsDB struct {
	tableName string
	client    dynamodbiface.DynamoDBAPI
}

func NewParticipantsDB(config DynamoDBConfig) (*ParticipantsDB, error) {
	if config.Endpoint == nil {
		// no endpoint is provided, use the DynamoDB client
		sess := session.Must(session.NewSessionWithOptions(session.Options{
			Config: aws.Config{Region: aws.String(config.Region)},
		}))

		return &ParticipantsDB{
			client:    dynamodb.New(sess),
			tableName: fmt.Sprintf(participantsDBFormat, config.Environment),
		}, nil

	} else {
		// endpoint provided, use DAX client
		daxConfig := dax.DefaultConfig()
		daxConfig.HostPorts = []string{*config.Endpoint}
		daxConfig.Region = config.Region
		daxConfig.ReadRetries = daxReadRetries
		daxConfig.WriteRetries = daxWriteRetries
		daxConfig.RequestTimeout = daxTimeout

		daxClient, err := dax.New(daxConfig)
		if err != nil {
			return nil, err
		}

		return &ParticipantsDB{
			client:    daxClient,
			tableName: fmt.Sprintf(participantsDBFormat, config.Environment),
		}, nil
	}
}

// PutHypeTrainParticipants writes all participants to the participants table
func (db *ParticipantsDB) PutHypeTrainParticipants(ctx context.Context, participants ...percy.Participant) error {
	if len(participants) == 0 {
		return errors.New("no participant")
	}

	for _, participant := range participants {
		if len(participant.HypeTrainID) == 0 {
			return errors.New("hype train ID is blank")
		}

		if len(participant.User.ID()) == 0 {
			return errors.New("user ID is blank")
		}
	}

	marshaledParticipants := make([]map[string]*dynamodb.AttributeValue, 0, len(participants))
	for _, participant := range participants {
		marshaled, err := marshalParticipant(participant)
		if err != nil {
			return errors.Wrap(err, "failed to marshal participant")
		}
		marshaledParticipants = append(marshaledParticipants, marshaled)
	}

	return db.batchWriteItems(marshaledParticipants...)
}

// RecordHypeTrainParticipation writes to the participants table to update the user's total participations in the participants table.
// User participation totals are aggregated based their participation source and type.
func (db *ParticipantsDB) RecordHypeTrainParticipation(ctx context.Context, hypeTrainID string, participation percy.Participation) (percy.Participant, error) {
	if len(hypeTrainID) == 0 {
		return percy.Participant{}, errors.New("hype train ID is blank")
	}

	if len(participation.ChannelID) == 0 {
		return percy.Participant{}, errors.New("participation channel ID is blank")
	}

	participantsInput, err := db.getUpdateParticipantsInput(hypeTrainID, participation)
	if err != nil {
		return percy.Participant{}, err
	}

	output, err := db.client.UpdateItem(&participantsInput)
	if err != nil {
		return percy.Participant{}, errors.Wrap(err, "failed to update participants total")
	}

	if output == nil {
		return percy.Participant{}, errors.New("failed to retrieve the updated participant record")
	}

	return unmarshalParticipant(output.Attributes)
}

// GetHypeTrainParticipant returns a single user's total participations towards a hype train instance.
func (db *ParticipantsDB) GetHypeTrainParticipant(ctx context.Context, hypeTrainID, userID string) (*percy.Participant, error) {
	if len(hypeTrainID) == 0 {
		return nil, errors.New("hype train ID is blank")
	}

	if len(userID) == 0 {
		return nil, errors.New("user ID is blank")
	}

	output, err := db.client.GetItemWithContext(ctx, &dynamodb.GetItemInput{
		TableName: aws.String(db.tableName),
		Key: map[string]*dynamodb.AttributeValue{
			participantsHashKey:  {S: aws.String(hypeTrainID)},
			participantsRangeKey: {S: aws.String(userID)},
		},
		ConsistentRead: aws.Bool(true),
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get participant")
	}

	if output == nil || len(output.Item) == 0 {
		return nil, nil
	}

	participant, err := unmarshalParticipant(output.Item)
	return &participant, err
}

// GetHypeTrainParticipants returns all hype train participants with their participation totals.
func (db *ParticipantsDB) GetHypeTrainParticipants(ctx context.Context, hypeTrainID string) ([]percy.Participant, error) {
	if len(hypeTrainID) == 0 {
		return nil, errors.New("hype train ID is blank")
	}

	keyCondition := expression.KeyEqual(expression.Key(participantsHashKey), expression.Value(hypeTrainID))
	expr, err := expression.NewBuilder().WithKeyCondition(keyCondition).Build()
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query key condition")
	}

	output, err := db.client.QueryWithContext(ctx, &dynamodb.QueryInput{
		TableName:                 aws.String(db.tableName),
		KeyConditionExpression:    expr.KeyCondition(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get participants")
	}

	if output == nil || len(output.Items) == 0 {
		return nil, nil
	}

	participants := make([]percy.Participant, 0, len(output.Items))
	for _, item := range output.Items {
		participant, err := unmarshalParticipant(item)
		if err != nil {
			logrus.WithField("hypeTrainID", hypeTrainID).WithError(err).Warn("Failed to parse DynamoDB participant")
			continue
		}

		participants = append(participants, participant)
	}

	return participants, nil
}

func (db *ParticipantsDB) batchWriteItems(items ...map[string]*dynamodb.AttributeValue) error {
	// DynamoDB batch write supports up to 25 requests per call.
	batchSize := 25

	if len(items) == 0 {
		return nil
	}

	// write items to DynamoDB in batches
	// TODO: could send requests in parallel
	for i := 0; i <= len(items)/batchSize; i++ {
		start := i * batchSize
		end := i*batchSize + batchSize
		if end > len(items) {
			end = len(items)
		}

		if start == end {
			break
		}

		batch := items[start:end]
		requests := make([]*dynamodb.WriteRequest, 0, len(batch))
		for _, item := range batch {
			requests = append(requests, &dynamodb.WriteRequest{
				PutRequest: &dynamodb.PutRequest{
					Item: item,
				},
			})
		}

		output, err := db.client.BatchWriteItem(&dynamodb.BatchWriteItemInput{
			RequestItems: map[string][]*dynamodb.WriteRequest{
				db.tableName: requests,
			},
		})
		if err != nil {
			return errors.Wrap(err, "failed to batch write items")
		}

		if len(output.UnprocessedItems) > 0 {
			return errors.Errorf("failed to batch write %d items", len(output.UnprocessedItems))
		}
	}

	return nil
}

func (db *ParticipantsDB) getUpdateParticipantsInput(hypeTrainID string, participations ...percy.Participation) (dynamodb.UpdateItemInput, error) {
	if len(participations) == 0 {
		return dynamodb.UpdateItemInput{}, errors.New("no participation")
	}

	updateBuilder := expression.Set(
		expression.Name("channel_id"),
		expression.Value(participations[0].ChannelID),
	).Set(
		expression.Name("ttl"),
		expression.Value(time.Now().Add(ttlDuration).Unix()),
	)
	// Add participation quantities to the aggregated counters based on the participation source and type.
	// If the counter field is missing, DynamoDB should initialize it to 0 and add to it.
	for _, participation := range participations {
		updateBuilder = updateBuilder.Add(expression.Name(
			formatCounterAttributeName(participation.ParticipationIdentifier)),
			expression.Value(participation.Quantity))
	}

	expr, err := expression.NewBuilder().WithUpdate(updateBuilder).Build()
	if err != nil {
		return dynamodb.UpdateItemInput{}, err
	}

	return dynamodb.UpdateItemInput{
		TableName: aws.String(db.tableName),
		Key: map[string]*dynamodb.AttributeValue{
			participantsHashKey:  {S: aws.String(hypeTrainID)},
			participantsRangeKey: {S: aws.String(participations[0].User.ID())},
		},
		UpdateExpression:          expr.Update(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ReturnValues:              aws.String(dynamodb.ReturnValueAllNew),
	}, nil
}

func marshalParticipant(participant percy.Participant) (map[string]*dynamodb.AttributeValue, error) {
	baseParticipantRecord := baseParticipantRecord{
		HypeTrainID: participant.HypeTrainID,
		ChannelID:   participant.ChannelID,
		UserID:      participant.User.ID(),
		Rewards:     toDBRewards(participant.Rewards),
		TTL:         time.Now().Add(ttlDuration).Unix(),
	}

	marshaledParticipant, err := dynamodbattribute.MarshalMap(baseParticipantRecord)
	if err != nil {
		return nil, err
	}

	for _, participations := range participant.Participations() {
		attributeName := formatCounterAttributeName(participations.ParticipationIdentifier)
		marshaledParticipant[attributeName] = &dynamodb.AttributeValue{
			N: aws.String(strconv.Itoa(participations.Quantity)),
		}
	}

	return marshaledParticipant, nil
}

func unmarshalParticipant(item map[string]*dynamodb.AttributeValue) (percy.Participant, error) {
	var unmarshalParticipant baseParticipantRecord
	err := dynamodbattribute.UnmarshalMap(item, &unmarshalParticipant)
	if err != nil {
		return percy.Participant{}, errors.Wrap(err, "failed to unmarshal participant item")
	}

	participant := percy.Participant{
		HypeTrainID:         unmarshalParticipant.HypeTrainID,
		ChannelID:           unmarshalParticipant.ChannelID,
		User:                percy.NewUser(unmarshalParticipant.UserID),
		ParticipationTotals: make(map[percy.ParticipationIdentifier]int),
		Rewards:             fromDBRewards(unmarshalParticipant.Rewards),
	}

	for attributeName, attributeValue := range item {
		if strings.HasPrefix(attributeName, participationCounterPrefix) {
			identifier, err := parseParticipationFromAttributeName(attributeName)
			if err != nil {
				logrus.WithFields(logrus.Fields{
					"hypeTrainID":   participant.HypeTrainID,
					"userID":        participant.User.ID(),
					"channelID":     participant.ChannelID,
					"attributeName": attributeName,
				}).Warn("failed to parse out participation source and type from DynamoDB attribute name")
				continue
			}

			quantity, err := strconv.Atoi(*attributeValue.N)
			if err != nil {
				logrus.WithFields(logrus.Fields{
					"hypeTrainID":    participant.HypeTrainID,
					"userID":         participant.User.ID(),
					"channelID":      participant.ChannelID,
					"attributeValue": attributeValue,
				}).Warn("failed to parse out participation quantity from DynamoDB attribute value")
				continue
			}

			participant.AddParticipation(identifier, quantity)
		}
	}

	return participant, nil
}

// formatCounterAttributeName formats participation source and action into a string.
// this string is to be used as the field name to count total participation quantity given the source type combination.
// e.g. participationSource: BITS, participationAction: CHEER             => "participation___BITS___CHEER"
//      participationSource: SUBS, participationAction: TIER_1_GIFTED_SUB => "participation___SUBS___TIER_1_GIFTED_SUB"
func formatCounterAttributeName(identifier percy.ParticipationIdentifier) string {
	return fmt.Sprintf(participationCounterFormat, participationCounterPrefix, identifier.Source, identifier.Action)
}

// parseParticipationFromAttributeName extracts the participation source and action information from the attribute name
// reversing the logic in formatParticipationCounterAttributeName.
// e.g. "participation___BITS___CHEER"             => participationSource: BITS, participationAction: CHEER
//      "participation___SUBS___TIER_1_GIFTED_SUB" => participationSource: SUBS, participationAction: TIER_1_GIFTED_SUB
func parseParticipationFromAttributeName(attributeName string) (percy.ParticipationIdentifier, error) {
	if !strings.HasPrefix(attributeName, participationCounterPrefix) {
		return percy.ParticipationIdentifier{}, errors.Errorf("attribute name (%s) does not start with the expected participation prefix (%s)", attributeName, participationCounterPrefix)
	}

	tokens := strings.Split(attributeName, participationCounterDelimiter)
	if len(tokens) != 3 {
		return percy.ParticipationIdentifier{}, errors.Errorf("attribute name (%s) does not have 3 parts after splitting with delimeter (%s)", attributeName, participationCounterDelimiter)
	}

	return percy.ParticipationIdentifier{
		Source: percy.ParticipationSource(tokens[1]),
		Action: percy.ParticipationAction(tokens[2]),
	}, nil
}
