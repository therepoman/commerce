package dynamodb

import (
	"context"
	"fmt"
	"time"

	"github.com/aws/aws-dax-go/dax"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/pkg/errors"
)

const (
	userAnniversaryTokensDBTableNameFormat = "user-anniversary-%s"
	userAnniversaryTokensDBTTLDuration     = 30 * 24 * time.Hour
	/* #nosec */
	userAnniversaryTokensDBTableKey = "user_id-anniversary_years"
)

type UserAnniversaryTokensDB struct {
	tableName string
	client    dynamodbiface.DynamoDBAPI
}

type chatNotificationToken struct {
	Key              string `dynamodbav:"user_id-anniversary_years"`
	UserID           string `dynamodbav:"user_id"`
	AnniversaryYears string `dynamodbav:"anniversary_years"`
	TTL              int64  `dynamodbav:"ttl"`
}

func NewUserAnniversaryTokensDB(config DynamoDBConfig) (*UserAnniversaryTokensDB, error) {
	if config.Endpoint == nil {
		// no endpoint is provided, use the DynamoDB client
		sess := session.Must(session.NewSessionWithOptions(session.Options{
			Config: aws.Config{Region: aws.String(config.Region)},
		}))

		return &UserAnniversaryTokensDB{
			client:    dynamodb.New(sess),
			tableName: fmt.Sprintf(userAnniversaryTokensDBTableNameFormat, config.Environment),
		}, nil

	} else {
		// endpoint provided, use DAX client
		daxConfig := dax.DefaultConfig()
		daxConfig.HostPorts = []string{*config.Endpoint}
		daxConfig.Region = config.Region
		daxConfig.ReadRetries = daxReadRetries
		daxConfig.WriteRetries = daxWriteRetries
		daxConfig.RequestTimeout = daxTimeout

		daxClient, err := dax.New(daxConfig)
		if err != nil {
			return nil, err
		}

		return &UserAnniversaryTokensDB{
			client:    daxClient,
			tableName: fmt.Sprintf(userAnniversaryTokensDBTableNameFormat, config.Environment),
		}, nil
	}
}

// HasChatNotificationToken returns true if the given user id + anniversary years exists in db
func (db *UserAnniversaryTokensDB) HasChatNotificationToken(ctx context.Context, userID string, years string) (bool, error) {
	if len(userID) == 0 {
		return false, errors.New("user ID is blank")
	}
	if len(years) == 0 {
		return false, errors.New("years is blank")
	}
	key := fmt.Sprintf("%s-%s", userID, years)

	resp, err := db.client.GetItemWithContext(ctx, &dynamodb.GetItemInput{
		TableName: aws.String(db.tableName),
		Key: map[string]*dynamodb.AttributeValue{
			userAnniversaryTokensDBTableKey: {S: aws.String(key)},
		},
	})
	if err != nil {
		return false, err
	}
	if resp == nil || len(resp.Item) == 0 {
		return false, nil
	}

	return true, nil
}

func (db *UserAnniversaryTokensDB) AddChatNotificationToken(ctx context.Context, userID string, years string) error {
	if len(userID) == 0 {
		return errors.New("user ID is blank")
	}
	if len(years) == 0 {
		return errors.New("years is blank")
	}
	key := fmt.Sprintf("%s-%s", userID, years)

	token := &chatNotificationToken{
		Key:              key,
		UserID:           userID,
		AnniversaryYears: years,
		TTL:              time.Now().Add(userAnniversaryTokensDBTTLDuration).Unix(),
	}

	tokenEntry, err := dynamodbattribute.MarshalMap(token)
	if err != nil {
		return errors.New("failed to marshal chat notification token dynamodb db entry")
	}

	_, err = db.client.PutItemWithContext(ctx, &dynamodb.PutItemInput{
		TableName: aws.String(db.tableName),
		Item:      tokenEntry,
	})

	return err
}

func (db *UserAnniversaryTokensDB) DeleteChatNotificationToken(ctx context.Context, userID string, years string) error {
	if len(userID) == 0 {
		return errors.New("user ID is blank")
	}
	if len(years) == 0 {
		return errors.New("years is blank")
	}
	key := fmt.Sprintf("%s-%s", userID, years)

	_, err := db.client.DeleteItemWithContext(ctx, &dynamodb.DeleteItemInput{
		TableName: aws.String(db.tableName),
		Key: map[string]*dynamodb.AttributeValue{
			userAnniversaryTokensDBTableKey: {S: aws.String(key)},
		},
	})

	return err
}
