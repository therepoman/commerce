package percy

import (
	"context"

	"code.justin.tv/commerce/percy/internal/metrics"
	"github.com/cactus/go-statsd-client/statsd"
)

//go:generate counterfeiter . CelebrationsEngine

type CelebrationsEngine interface {
	GetCelebrationConfig(ctx context.Context, channelID string, backfill bool) (CelebrationConfig, error)
	StartHighestValueCelebration(ctx context.Context, channelID string, sender User, eventType CelebrationEventType, bitsCheered int64, transactionID string) error
	UpdateCelebrationConfig(ctx context.Context, channelID string, enabled bool) (CelebrationConfig, error)
	CreateCelebration(ctx context.Context, channelID string, args CelebrationArgs) (Celebration, error)
	UpdateCelebration(ctx context.Context, channelID, celebrationID string, args CelebrationArgs) (Celebration, error)
	DeleteCelebration(ctx context.Context, channelID, celebrationID string) error

	Authorize(ctx context.Context, channelID, userID string) error
	HasCelebrations(channelID string) bool
	HasFounderCelebrations(ctx context.Context, channelID string) bool

	GetCelebrationUserSettings(ctx context.Context, userID string) (CelebrationUserSettings, error)
	UpdateCelebrationUserSettings(ctx context.Context, userID string, update CelebrationUserSettingsUpdate) (CelebrationUserSettings, error)

	// Explicit Purchase
	FulfillCelebrationPurchase(ctx context.Context, req CelebrationsPurchaseFulfillmentReq) error
	RevokeCelebrationPurchase(ctx context.Context, req CelebrationsPurchaseRevocationReq) error
	IsEligibleForCelebrationPurchase(ctx context.Context, args CelebrationsIsEligibleArgs) ([]CelebrationsPurchaseOfferEligibility, error)
	GetPurchasableCelebrationConfig(ctx context.Context, channelID string) (*CelebrationChannelPurchasableConfig, error)
	UpdatePurchasableCelebrationConfig(ctx context.Context, channelID string, update CelebrationChannelPurchasableConfigUpdate) (*CelebrationChannelPurchasableConfig, error)
	Payout(ctx context.Context, req CelebrationsPayoutArgs) error
	SendRefundEmail(ctx context.Context, req CelebrationsRefundEmailArgs) error
	EmitCelebration(ctx context.Context, req EmitCelebrationArgs) error
	PublishToActivityFeed(ctx context.Context, req PublishCelebrationToActivityFeedArgs) error
	Refund(ctx context.Context, req CelebrationsRefundArgs) error
	SendFulfillmentUserNotice(ctx context.Context, req CelebrationsUserNoticeArgs) error
	PublishDatascience(ctx context.Context, req CelebrationsDatascienceArgs) error
	ClawbackPayout(ctx context.Context, req CelebrationsPayoutArgs) error
}

type celebrationEngine struct {
	CelebrationConfigsDB                  CelebrationConfigsDB
	CelebrationUserSettingsDB             CelebrationUserSettingsDB
	CelebrationChannelPurchasableConfigDB CelebrationChannelPurchasableConfigDB
	IdempotencyDB                         IdempotentTaskDB
	Statter                               statsd.Statter
	Publisher                             Publisher
	UserProfileDB                         UserProfileDB
	Spade                                 Spade
	EditorDB                              EditorDB
	WhitelistDB                           WhitelistDB
	FoundersExperimentDB                  ExperimentDB
	StateMachine                          StateMachine
	Purchase                              Purchase
	Notification                          Notification
	SNS                                   SNSPublisher
}

type CelebrationEngineConfig struct {
	CelebrationConfigsDB                  CelebrationConfigsDB
	CelebrationUserSettingsDB             CelebrationUserSettingsDB
	CelebrationChannelPurchasableConfigDB CelebrationChannelPurchasableConfigDB
	IdempotencyDB                         IdempotentTaskDB
	Statter                               statsd.Statter
	Publisher                             Publisher
	UserProfileDB                         UserProfileDB
	Spade                                 Spade
	EditorDB                              EditorDB
	WhitelistDB                           WhitelistDB
	FoundersExperimentDB                  ExperimentDB
	StateMachine                          StateMachine
	Purchase                              Purchase
	Notification                          Notification
	SNS                                   SNSPublisher
}

// NewEngine ...
func NewCelebrationEngine(config CelebrationEngineConfig) *celebrationEngine {
	statter := config.Statter
	if statter == nil {
		statter = &metrics.NoopStatter{}
	}
	return &celebrationEngine{
		CelebrationConfigsDB:                  config.CelebrationConfigsDB,
		CelebrationUserSettingsDB:             config.CelebrationUserSettingsDB,
		CelebrationChannelPurchasableConfigDB: config.CelebrationChannelPurchasableConfigDB,
		IdempotencyDB:                         config.IdempotencyDB,
		Statter:                               statter,
		Publisher:                             config.Publisher,
		UserProfileDB:                         config.UserProfileDB,
		Spade:                                 config.Spade,
		EditorDB:                              config.EditorDB,
		WhitelistDB:                           config.WhitelistDB,
		FoundersExperimentDB:                  config.FoundersExperimentDB,
		StateMachine:                          config.StateMachine,
		Purchase:                              config.Purchase,
		Notification:                          config.Notification,
		SNS:                                   config.SNS,
	}
}
