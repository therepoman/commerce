package percy

import (
	"context"
)

//go:generate counterfeiter . PersonalizationClient

type PersonalizationClient interface {
	GetHypeTrainConfigOverrides(ctx context.Context, channelId string) (*PersonalizedHypeTrainConfig, error)
}

type PersonalizedHypeTrainConfig struct {
	NumOfEventsToKickoff    int
	Difficulty              Difficulty
	ShouldUseExistingConfig bool
}
