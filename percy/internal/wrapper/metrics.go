package wrapper

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/foundation/twitchclient"
)

const (
	metricPattern         = "service.%s.%s"
	defaultStatSampleRate = 1.0
	defaultServiceName    = "unknownservice"
	defaultAPIName        = "unknownapi"
)

func GenerateRequestContext(ctx context.Context, serviceName string, apiName string) context.Context {
	if serviceName == "" {
		logrus.Warn("Service name must not be empty when generating client request context")
		serviceName = defaultServiceName
	}

	if apiName == "" {
		logrus.Warn("API name must not be empty when generating client request context")
		apiName = defaultAPIName
	}

	reqOpts := twitchclient.ReqOpts{}
	reqOpts.StatName = fmt.Sprintf(metricPattern, serviceName, apiName)
	reqOpts.StatSampleRate = defaultStatSampleRate
	reqCtx := twitchclient.WithReqOpts(ctx, reqOpts)
	return reqCtx
}
