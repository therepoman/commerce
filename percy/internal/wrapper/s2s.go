package wrapper

import (
	"net/http"

	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

func NewS2SRoundTripperWrapper(s2sServiceName string) (func(http.RoundTripper) http.RoundTripper, error) {
	var cfg *caller.Config

	rt, err := caller.NewRoundTripper(s2sServiceName, cfg, logrus.New())
	if err != nil {
		return nil, errors.Wrap(err, "creating new caller.roundtripper")
	}

	return func(inner http.RoundTripper) (outer http.RoundTripper) {
		rt.SetInnerRoundTripper(inner)
		return rt
	}, nil
}
