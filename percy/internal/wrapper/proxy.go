package wrapper

import (
	"context"
	"log"
	"net"
	"net/http"

	"golang.org/x/net/proxy"
)

// NewProxyRoundTripWrapper returns a new ProxyRoundTripWrapper.
// This is used to connect to tinybubble services from local machine not used in staging and prod
func NewProxyRoundTripWrapper(socksAddr string) func(c http.RoundTripper) http.RoundTripper {
	if socksAddr == "" {
		return func(next http.RoundTripper) http.RoundTripper {
			return next
		}
	}

	dialer, err := proxy.SOCKS5("tcp", socksAddr, nil, proxy.Direct)
	if err != nil {
		log.Fatalf("can't connect to the proxy: %v", err)
	}

	return func(next http.RoundTripper) http.RoundTripper {
		return &http.Transport{
			DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
				return dialer.Dial(network, addr)
			},
		}
	}
}
