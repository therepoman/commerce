package percy

import (
	"context"
	"time"
)

type PartnerType string

const (
	PartnerTypeAffiliate = PartnerType("affiliate")
	PartnerTypePartner   = PartnerType("partner")
	PartnerTypeUnknown   = PartnerType("unknown")
)

type UserProfile struct {
	ID              string
	Login           string
	DisplayName     string
	IsStaff         bool
	PrimaryColorHex *string
	ProfileImageURL string
	CreatedOn       time.Time
}

type UserNoticeMessageParams struct {
	Key   string
	Value string
}

type UserNoticeParams struct {
	SenderUserID      int
	TargetChannelID   int
	Body              string
	MessageID         string
	MessageParams     []UserNoticeMessageParams
	DefaultSystemBody string
}

//go:generate counterfeiter . UserProfileDB

type UserProfileDB interface {
	GetProfileByID(ctx context.Context, id string) (UserProfile, error)
	IsChatBanned(ctx context.Context, userID, channelID string) (bool, error)
	IsMonetized(ctx context.Context, channelID string) (bool, error)
	GetPartnerType(ctx context.Context, channelID string) (PartnerType, error)
	SendUserNotice(ctx context.Context, args UserNoticeParams) error
}
