package percy

import (
	"context"
	"errors"
)

type CelebrationSize string

const (
	CelebrationSizeUnknown = CelebrationSize("UNKNOWN")
	CelebrationSizeSmall   = CelebrationSize("SMALL")
	CelebrationSizeLarge   = CelebrationSize("LARGE")
)

var CelebrationPurchasableConfigDuplicateOfferError = errors.New("celebration purchasable config can only contain an offer once")

var ValidCelebrationSizes = map[CelebrationSize]interface{}{
	CelebrationSizeSmall: nil,
	CelebrationSizeLarge: nil,
}

type CelebrationChannelPurchasableConfig struct {
	ChannelID      string
	PurchaseConfig map[CelebrationSize]CelebrationPurchaseConfig
}

type CelebrationPurchaseConfig struct {
	IsDisabled bool
	OfferID    string
}

type CelebrationChannelPurchasableConfigUpdate struct {
	PurchaseConfig map[CelebrationSize]CelebrationPurchaseConfigUpdate
}

type CelebrationPurchaseConfigUpdate struct {
	IsDisabled *bool
	OfferID    *string
}

//go:generate counterfeiter . CelebrationChannelPurchasableConfigDB

type CelebrationChannelPurchasableConfigDB interface {
	GetConfig(ctx context.Context, channelID string) (*CelebrationChannelPurchasableConfig, error)
	UpdateConfig(ctx context.Context, channelID string, update CelebrationChannelPurchasableConfigUpdate) (*CelebrationChannelPurchasableConfig, error)
}

func (eng *celebrationEngine) GetPurchasableCelebrationConfig(ctx context.Context, channelID string) (*CelebrationChannelPurchasableConfig, error) {
	if channelID == "" {
		return nil, errors.New("channel ID required")
	}

	return eng.CelebrationChannelPurchasableConfigDB.GetConfig(ctx, channelID)
}

func (eng *celebrationEngine) UpdatePurchasableCelebrationConfig(ctx context.Context, channelID string, update CelebrationChannelPurchasableConfigUpdate) (*CelebrationChannelPurchasableConfig, error) {
	if channelID == "" {
		return nil, errors.New("channel ID required")
	}

	currentConfig, err := eng.GetPurchasableCelebrationConfig(ctx, channelID)
	if err != nil {
		return nil, err
	}

	err = validateConfigUpdates(currentConfig, update)
	if err != nil {
		return nil, err
	}

	return eng.CelebrationChannelPurchasableConfigDB.UpdateConfig(ctx, channelID, update)
}

// Ensure that config does not have duplicate offerIDs. This is a little tricky because
//   a) users do not need to provide all of the intensities with each update
//   b) we have logic for filling out a user's config with defaults
// We must first fetch the current config (which includes defaults), apply the updates on top, and then
// check for the presence of duplicate offerIDs.
func validateConfigUpdates(currentConfig *CelebrationChannelPurchasableConfig, update CelebrationChannelPurchasableConfigUpdate) error {
	newConfig := map[CelebrationSize]string{}

	if currentConfig != nil {
		for size, sizeConfig := range currentConfig.PurchaseConfig {
			if !sizeConfig.IsDisabled {
				newConfig[size] = sizeConfig.OfferID
			}
		}
	}

	for size, update := range update.PurchaseConfig {
		if update.OfferID != nil && update.IsDisabled != nil && !*update.IsDisabled {
			newConfig[size] = *update.OfferID
		}
	}

	offers := map[string]bool{}

	for _, offerID := range newConfig {
		_, ok := offers[offerID]
		if ok {
			return CelebrationPurchasableConfigDuplicateOfferError
		}

		offers[offerID] = true
	}

	return nil
}
