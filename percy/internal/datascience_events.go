package percy

import (
	"context"
	"strconv"
	"time"

	"code.justin.tv/commerce/logrus"
)

//go:generate counterfeiter . Spade
type Spade interface {
	// HypeTrain
	SendHypeTrainParticipation(ctx context.Context, events ...HypeTrainParticipationEventParams) error
	SendHypeTrainSettingsEvent(ctx context.Context, events ...HypeTrainSettingsEventParams) error
	SendHypeTrainProgressUpdateEvents(ctx context.Context, events ...HypeTrainProgressUpdateEventParams) error
	SendHypeTrainParticipantsEvents(ctx context.Context, events ...HypeTrainParticipantsEventParams) error
	SendHypeTrainSpecialCalloutEvents(ctx context.Context, events ...HypeTrainSpecialCalloutEventParams) error
	SendHypeTrainCoolDownIgnoredStartEvents(ctx context.Context, events ...HypeTrainCoolDownIgnoredStartEventParams) error
	SendRewardItemReceiveEvents(ctx context.Context, events ...RewardItemReceiveEventParams) error
	SendBadgeUpdateEvents(ctx context.Context, events ...HypeTrainBadgeUpdateEventParams) error
	SendBadgeDisplaySelectionEvents(ctx context.Context, events ...HypeTrainBadgeDisplaySelectionEventParams) error
	SendHypeTrainApproachingEvent(ctx context.Context, events ...HypeTrainApproachingEventParams) error

	// Celebrations
	SendCelebrationOverlayInitiatedEvent(ctx context.Context, events ...CelebrationOverlayInitiatedEventParams) error
	SendCelebrationChannelPurchasableConfigChangeEvent(ctx context.Context, events ...CelebrationChannelPurchasableConfigChangeEventParams) error
}

// Spade event - hype_train_settings
// This event is triggered every time a
// streamer updates their hype train settings and whenever a train starts
type HypeTrainSettingsEventParams struct {
	ChannelID               string `json:"channel_id"`
	TrainID                 string `json:"train_id"`
	OptedOut                bool   `json:"opted_out"`
	SendType                string `json:"send_type"`
	Threshold               int    `json:"threshold"`
	Difficulty              string `json:"difficulty"`
	Cooldown                int    `json:"cooldown"`
	Ghostmode               bool   `json:"ghost_mode"`
	CalloutEmote            string `json:"emote_selection"`
	UseCreatorColor         bool   `json:"use_creator_color"`
	UsePersonalizedSettings bool   `json:"use_personalized_settings"`
}

// Spade event - hype_train_progress_update
// This event is triggered when a train starts and ends as well
// as each level progression.
type HypeTrainProgressUpdateEventParams struct {
	ChannelID        string `json:"channel_id"`
	TrainID          string `json:"train_id"`
	Status           string `json:"status"`
	CurrentLevelGoal int    `json:"current_level_goal"`
	Ghostmode        bool   `json:"ghost_mode"`
}

// Spade event - hype_train_participants
// This event is triggered when a hype train ends.
// For anonymous submit a record representing all
// anonymous activity, maybe used reserved id.
type HypeTrainParticipantsEventParams struct {
	ChannelID          string `json:"channel_id"`
	UserID             string `json:"user_id"`
	TrainID            string `json:"train_id"`
	MaxTrainLevel      int    `json:"max_train_level"`
	Initiator          bool   `json:"initiator"`
	ContributionPoints int    `json:"contribution_points"`
	Bits               int    `json:"bits"` // Includes Cheer, Polls, Extension
	Tier1Subs          int    `json:"tier1_subs"`
	Tier2Subs          int    `json:"tier2_subs"`
	Tier3Subs          int    `json:"tier3_subs"`
	Tier1Gifts         int    `json:"tier1_gifts"`
	Tier2Gifts         int    `json:"tier2_gifts"`
	Tier3Gifts         int    `json:"tier3_gifts"`
	Anonymous          bool   `json:"anonymous"`
	TrainEmoteLevel    int    `json:"train_emote_level"`
	EmoteID            string `json:"emote_id"`
	Ghostmode          bool   `json:"ghost_mode"`
}

// Spade event - hype_train_special_callout
// This event is triggered when a special callout is sent for a user.
type HypeTrainSpecialCalloutEventParams struct {
	ChannelID string `json:"channel_id"`
	UserID    string `json:"user_id"`
	TrainID   string `json:"train_id"`
	Product   string `json:"product"`
	Amount    int    `json:"amount"`
	Ghostmode bool   `json:"ghost_mode"`
}

// Spade event - hype_train_cooldown_ignored_start
// This event is triggered when a hype train would have
// started during a cooldown period
type HypeTrainCoolDownIgnoredStartEventParams struct {
	ChannelID string `json:"channel_id"`
	Ghostmode bool   `json:"ghost_mode"`
}

// Spade event - reward_item_receive
// This event is triggered when we entitle rewards
type RewardItemReceiveEventParams struct {
	ChannelID     string `json:"channel_id"`
	Context       string `json:"context"`
	ItemType      string `json:"item_type"`
	ItemValue     string `json:"item_value"`
	TransactionID string `json:"transaction_id"`
	UserID        int    `json:"user_id"`
	Ghostmode     bool   `json:"ghost_mode"`
}

// Spade event - hype_train_participation
// This event is triggered when a participation occurs
type HypeTrainParticipationEventParams struct {
	ChannelID string `json:"channel_id"`
	TrainID   string `json:"train_id"`
	Type      string `json:"event_type"`
	ID        string `json:"event_id"`
	Quantity  int    `json:"quantity"`
}

// Spade event - hype_train_approaching
// This event is triggered when we're close to a hype train
type HypeTrainApproachingEventParams struct {
	ChannelID               string    `json:"channel_id"`
	RemainingEvents         int       `json:"events_remaining_to_hype_train"`
	Threshold               int       `json:"threshold"`
	Ghostmode               bool      `json:"ghost_mode"`
	RateLimited             bool      `json:"rate_limited"`
	ParticipantUserID       string    `json:"paticipant_user_id"`
	Difficulty              string    `json:"difficulty"`
	Cooldown                int       `json:"cooldown"`
	OptedOut                bool      `json:"opted_out"`
	UsePersonalizedSettings bool      `json:"use_personalized_settings"`
	ColorSelection          string    `json:"color_selection"`
	HypeTrainID             string    `json:"train_id"`
	StartTime               time.Time `json:"start_time"`
	EndTime                 time.Time `json:"end_time"`
}

type BadgeUpdateName string

const (
	CurrentConductorBadgeUpdate = BadgeUpdateName("CURRENT_CONDUCTOR")
	FormerConductorBadgeUpdate  = BadgeUpdateName("FORMER_CONDUCTOR")
)

type BadgeUpdateAction string

const (
	AutoEquipBadge = BadgeUpdateAction("AUTO_EQUIP")
	HideBadge      = BadgeUpdateAction("HIDE")
	RemoveBadge    = BadgeUpdateAction("REMOVE")
	EarnedBadge    = BadgeUpdateAction("EARNED")
	ExpiredBadge   = BadgeUpdateAction("EXPIRED")
)

type HypeTrainBadgeUpdateEventParams struct {
	ChannelID string            `json:"channel_id"`
	UserID    string            `json:"user_id"`
	BadgeName BadgeUpdateName   `json:"badge_name"`
	Action    BadgeUpdateAction `json:"action"`
	TrainID   string            `json:"train_id"`
}

type HypeTrainBadgeDisplaySelectionEventParams struct {
	ChannelID string            `json:"channel_id"`
	UserID    string            `json:"user_id"`
	BadgeName BadgeUpdateName   `json:"badge_name"`
	Action    BadgeUpdateAction `json:"action"`
}

type initiator struct {
	TotalPoints ParticipationPoint
	Initiated   bool
}

type initiators map[string]initiator

func (eng *engine) SendHypeTrainParticipationEvent(hypeTrainID string, participation Participation) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	// e.g. BITS-CHEER, BITS-EXTENSION, BITS-POLL, SUBS-TIER_1_GIFTED_SUB etc.
	eventType := string(participation.ParticipationIdentifier.Source) + "-" + string(participation.ParticipationIdentifier.Action)

	if err := eng.Spade.SendHypeTrainParticipation(ctx, HypeTrainParticipationEventParams{
		ChannelID: participation.ChannelID,
		TrainID:   hypeTrainID,
		Type:      eventType,
		ID:        participation.EventID,
		Quantity:  participation.Quantity,
	}); err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"channelID": participation.ChannelID,
			"trainID":   hypeTrainID,
			"type":      eventType,
			"id":        participation.EventID,
		}).Error("Failed to publish participation event to spade")
	}
}

func (eng *engine) SendHypeTrainSettingsEvent(hypeTrainID string, hypeTrainConfig HypeTrainConfig, sendType string) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	if err := eng.Spade.SendHypeTrainSettingsEvent(ctx, HypeTrainSettingsEventParams{
		ChannelID:               hypeTrainConfig.ChannelID,
		TrainID:                 hypeTrainID,
		OptedOut:                !hypeTrainConfig.Enabled,
		SendType:                sendType,
		Threshold:               hypeTrainConfig.KickoffConfig.NumOfEvents,
		Difficulty:              string(hypeTrainConfig.Difficulty),
		Cooldown:                int(hypeTrainConfig.CooldownDuration / time.Hour),
		Ghostmode:               !hypeTrainConfig.IsWhitelisted,
		CalloutEmote:            hypeTrainConfig.CalloutEmoteID,
		UseCreatorColor:         hypeTrainConfig.UseCreatorColor,
		UsePersonalizedSettings: hypeTrainConfig.UsePersonalizedSettings,
	}); err != nil {
		logrus.WithError(err).WithField(
			"channelID", hypeTrainConfig.ChannelID,
		).Error("Failed to publish config event to spade on train start")
	}
}

func (eng *engine) SendHypeTrainProgressEvents(hypeTrain HypeTrain, status string) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	if err := eng.Spade.SendHypeTrainProgressUpdateEvents(ctx, HypeTrainProgressUpdateEventParams{
		ChannelID:        hypeTrain.ChannelID,
		TrainID:          hypeTrain.ID,
		Status:           status,
		CurrentLevelGoal: int(hypeTrain.LevelProgress().Goal),
		Ghostmode:        !hypeTrain.Config.IsWhitelisted,
	}); err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"channelID": hypeTrain.ChannelID,
			"train_id":  hypeTrain.ID,
		}).Error("Failed to publish progression update event to spade")
	}
}

func (eng *engine) SendHypeTrainParticipantEvents(hypeTrain HypeTrain, entitledParticipants EntitledParticipants, participants ...Participant) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	// func getInitiators
	hypeTrainInitiators, err := eng.GetInitiators(ctx, hypeTrain)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"channelID": hypeTrain.ChannelID,
			"train_id":  hypeTrain.ID,
		}).Error("Failed to find initiators")
	}

	participantEvents := make([]HypeTrainParticipantsEventParams, len(participants))
	for i, p := range participants {
		contributionPoints := p.ParticipationTotals.ParticipationPoints(hypeTrain.Config)
		bitsTotal := p.ParticipationQuantity(ParticipationIdentifier{Source: BitsSource, Action: CheerAction})

		userReward, ok := entitledParticipants[p.User.Id]
		if !ok {
			userReward = []Reward{
				{
					Type:        EmoteReward,
					ID:          "",
					GroupID:     "",
					RewardLevel: hypeTrain.CompletedLevel(),
				},
			}
		}

		_, initiated := hypeTrainInitiators[p.User.Id]

		participantEvents[i] = HypeTrainParticipantsEventParams{
			ChannelID:          p.ChannelID,
			UserID:             p.User.Id,
			TrainID:            p.HypeTrainID,
			MaxTrainLevel:      hypeTrain.CompletedLevel(),
			Initiator:          initiated,
			ContributionPoints: contributionPoints,
			Bits:               bitsTotal,
			Tier1Subs:          p.ParticipationQuantity(ParticipationIdentifier{Source: SubsSource, Action: Tier1SubAction}),
			Tier2Subs:          p.ParticipationQuantity(ParticipationIdentifier{Source: SubsSource, Action: Tier2SubAction}),
			Tier3Subs:          p.ParticipationQuantity(ParticipationIdentifier{Source: SubsSource, Action: Tier3SubAction}),
			Tier1Gifts:         p.ParticipationQuantity(ParticipationIdentifier{Source: SubsSource, Action: Tier1GiftedSubAction}),
			Tier2Gifts:         p.ParticipationQuantity(ParticipationIdentifier{Source: SubsSource, Action: Tier2GiftedSubAction}),
			Tier3Gifts:         p.ParticipationQuantity(ParticipationIdentifier{Source: SubsSource, Action: Tier3GiftedSubAction}),
			Anonymous:          p.User.IsAnonymous(),
			TrainEmoteLevel:    userReward[0].RewardLevel,
			EmoteID:            userReward[0].ID,
			Ghostmode:          !hypeTrain.Config.IsWhitelisted,
		}
	}

	if err := eng.Spade.SendHypeTrainParticipantsEvents(ctx, participantEvents...); err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"channelID": hypeTrain.ChannelID,
			"train_id":  hypeTrain.ID,
		}).Error("Failed to publish participant events to spade")
	}
}

func (eng *engine) SendHypeTrainCalloutEvent(hypeTrain HypeTrain, participation Participation) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	if err := eng.Spade.SendHypeTrainSpecialCalloutEvents(ctx, HypeTrainSpecialCalloutEventParams{
		ChannelID: participation.ChannelID,
		UserID:    participation.User.Id,
		TrainID:   hypeTrain.ID,
		Product:   string(participation.ParticipationIdentifier.Action),
		Amount:    participation.Quantity,
		Ghostmode: !hypeTrain.Config.IsWhitelisted,
	}); err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"channelID": hypeTrain.ChannelID,
			"train_id":  hypeTrain.ID,
		}).Error("Failed to publish special callout event to spade")
	}
}

func (eng *engine) SendHypeTrainApproachingEvent(config HypeTrainConfig, channelID string, remainingEvents int, hypeTrainID string, participantUserID string, startTime, endTime time.Time, rateLimited bool, ghostMode bool) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	if err := eng.Spade.SendHypeTrainApproachingEvent(ctx, HypeTrainApproachingEventParams{
		ChannelID:               config.ChannelID,
		RemainingEvents:         remainingEvents,
		Ghostmode:               ghostMode,
		Difficulty:              string(config.Difficulty),
		UsePersonalizedSettings: config.UsePersonalizedSettings,
		ColorSelection:          config.PrimaryHexColor,
		Threshold:               config.KickoffConfig.NumOfEvents,
		OptedOut:                config.Enabled,
		ParticipantUserID:       participantUserID,
		HypeTrainID:             hypeTrainID,
		Cooldown:                int(config.CooldownDuration.Hours()),
		StartTime:               startTime,
		EndTime:                 endTime,
		RateLimited:             rateLimited,
	}); err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"channelID": config.ChannelID,
		}).Error("Failed to publish hype train approachhing event to spade")
	}
}

func (eng *engine) sendRewardItemReceivedEvents(hypeTrain HypeTrain, entitledParticipants EntitledParticipants) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	rewardItemEvents := make([]RewardItemReceiveEventParams, 0)
	for userID, reward := range entitledParticipants {
		userIDInt, err := strconv.Atoi(userID)
		if err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{
				"userID": userID,
			}).Error("Failed to convert userID to int")
			continue
		}

		rewardItemEvents = append(rewardItemEvents, RewardItemReceiveEventParams{
			ChannelID:     hypeTrain.ChannelID,
			Context:       "hypetrain.recipient",
			ItemType:      string(reward[0].Type),
			ItemValue:     reward[0].ID,
			TransactionID: hypeTrain.ID,
			UserID:        userIDInt,
			Ghostmode:     !hypeTrain.Config.IsWhitelisted,
		})
	}

	if err := eng.Spade.SendRewardItemReceiveEvents(ctx, rewardItemEvents...); err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"channelID": hypeTrain.ChannelID,
			"train_id":  hypeTrain.ID,
		}).Error("Failed to publish reward events to spade")
	}
}

func (eng *engine) GetInitiators(ctx context.Context, hypeTrain HypeTrain) (initiators, error) {
	// Return all participations from the kickoff period
	params := ParticipationQueryParams{
		StartTime: hypeTrain.StartedAt.Add(-1 * hypeTrain.Config.KickoffConfig.Duration),
		EndTime:   hypeTrain.StartedAt,
	}

	participations, err := eng.ParticipationDB.GetChannelParticipations(ctx, hypeTrain.ChannelID, params)
	if err != nil {
		return nil, err
	}

	// Iterate over participations and mark the users who contributed enough to be an initiator
	initiators := make(map[string]initiator)
	for _, participation := range participations {
		var points ParticipationPoint
		if _, ok := initiators[participation.User.Id]; !ok {
			points = hypeTrain.Config.EquivalentParticipationPoint(participation.ParticipationIdentifier, participation.Quantity)
		} else {
			points = hypeTrain.Config.EquivalentParticipationPoint(participation.ParticipationIdentifier, participation.Quantity) + initiators[participation.User.Id].TotalPoints
		}

		initiators[participation.User.Id] = initiator{
			TotalPoints: points,
			Initiated:   points >= hypeTrain.Config.KickoffConfig.MinPoints,
		}
	}

	return initiators, nil
}
