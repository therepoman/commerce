package mako

import (
	"context"
	"net/http"

	makotwirp "code.justin.tv/commerce/mako/twirp"
	"code.justin.tv/commerce/percy/internal/wrapper"
	"code.justin.tv/foundation/twitchclient"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
)

const (
	getEmotesByIDsAPI         = "getEmotesByIDs"
	getEmotesByEmoticonIdsAPI = "getEmoticonsByGroups"
)

//go:generate counterfeiter . Fetcher

type Fetcher interface {
	GetEmoteInformation(ctx context.Context, emoteIDs []string) (map[string]*makotwirp.Emoticon, error)
	GetEmoteSetInformation(ctx context.Context, emoticonIDs []string) (map[string]*makotwirp.Emoticon, error)
}

type fetcher struct {
	client makotwirp.Mako
}

func NewFetcher(host string, statter statsd.Statter) *fetcher {
	return &fetcher{
		client: makotwirp.NewMakoProtobufClient(host, twitchclient.NewHTTPClient(twitchclient.ClientConf{
			Stats:          statter,
			StatNamePrefix: serviceName,
			RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
				wrapper.NewRetryRoundTripWrapper(statter, serviceName, defaultRetry),
				wrapper.NewHystrixRoundTripWrapper(serviceName, defaultTimeout),
			},
			Transport: twitchclient.TransportConf{
				MaxIdleConnsPerHost: 200,
			},
		})),
	}
}

func (f *fetcher) GetEmoteInformation(ctx context.Context, emoteIDs []string) (map[string]*makotwirp.Emoticon, error) {
	if len(emoteIDs) == 0 {
		return nil, nil
	}

	reqCtx := wrapper.GenerateRequestContext(ctx, serviceName, getEmotesByIDsAPI)
	resp, err := f.client.GetEmoticonsByEmoticonIds(reqCtx, &makotwirp.GetEmoticonsByEmoticonIdsRequest{
		EmoticonIds: emoteIDs,
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get emotes by IDs from Mako")
	}

	if resp == nil || len(resp.Emoticons) == 0 {
		return nil, nil
	}

	emoteMap := map[string]*makotwirp.Emoticon{}
	for _, emote := range resp.Emoticons {
		emoteMap[emote.Id] = emote
	}

	return emoteMap, nil
}

func (f *fetcher) GetEmoteSetInformation(ctx context.Context, emoteSetIDs []string) (map[string]*makotwirp.Emoticon, error) {
	if len(emoteSetIDs) == 0 {
		return nil, nil
	}

	reqCtx := wrapper.GenerateRequestContext(ctx, serviceName, getEmotesByEmoticonIdsAPI)
	resp, err := f.client.GetEmoticonsByGroups(reqCtx, &makotwirp.GetEmoticonsByGroupsRequest{
		EmoticonGroupKeys: emoteSetIDs,
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get emotes by IDs from Mako")
	}

	if resp == nil || len(resp.Groups) == 0 {
		return nil, nil
	}

	emoteMap := map[string]*makotwirp.Emoticon{}
	for _, emoteGroup := range resp.Groups {
		for _, emote := range emoteGroup.Emoticons {
			emoteMap[emote.Id] = emote
		}
	}

	return emoteMap, nil
}
