package mako

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"code.justin.tv/commerce/logrus"
	makotwirp "code.justin.tv/commerce/mako/twirp"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/wrapper"
	"code.justin.tv/foundation/twitchclient"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/hashicorp/go-multierror"
	"github.com/pkg/errors"
	"golang.org/x/time/rate"
)

const (
	serviceName    = "mako"
	originIDPrefix = "amzn.twitch.hypetrain."
	defaultRetry   = 1
	defaultTimeout = 1000

	// Rate limiter
	requestsSecond = 10
	numBuckets     = 5

	createEntitlementsAPI = "createEntitlements"
	getEntitlementsAPI    = "getEntitlements"

	maxBatchSize = 25

	hypeTrainChannelID = "477339272"
)

type EntitlementDB struct {
	client  makotwirp.Mako
	limiter *rate.Limiter
}

func NewEntitlementDB(host string, statter statsd.Statter, socksAddr string) *EntitlementDB {
	return &EntitlementDB{
		client: makotwirp.NewMakoProtobufClient(host, twitchclient.NewHTTPClient(twitchclient.ClientConf{
			Stats:          statter,
			StatNamePrefix: serviceName,
			RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
				wrapper.NewProxyRoundTripWrapper(socksAddr),
				wrapper.NewRetryRoundTripWrapper(statter, serviceName, defaultRetry),
				wrapper.NewHystrixRoundTripWrapper(serviceName, defaultTimeout),
			},
			Transport: twitchclient.TransportConf{
				MaxIdleConnsPerHost: 200,
			},
		})),
		limiter: rate.NewLimiter(rate.Limit(requestsSecond), numBuckets),
	}
}

// CreateEmoteEntitlement creates an entry in Materia for the entitled participant through Mako
func (db *EntitlementDB) CreateEmoteEntitlement(ctx context.Context, hypeTrain percy.HypeTrain, entitledUsers percy.EntitledParticipants) error {
	if len(hypeTrain.ID) == 0 {
		return fmt.Errorf("error on required parameters. hypeTrainID: %v", hypeTrain.ID)
	}

	now := time.Now()

	entitlements := make([]*makotwirp.EmoteEntitlement, 0)
	startTime, _ := ptypes.TimestampProto(now)

	entitlementEndDate := hypeTrain.Config.RewardEndDate
	// Around March 2020, we made a change to the hype train default config to make the first batch of emote permanent.
	// https://git-aws.internal.justin.tv/commerce/percy/pull/260
	// This change effectively makes all hype train before March 13th to have an "incorrect" copy of the config, which had a defined reward end date.
	// Adding in a check here for hype train start time and erase the reward end date for all hype trains before April 1st.
	// This affect admin tools that may be granting rewards for old hype trains.
	if hypeTrain.StartedAt.Before(time.Date(2020, time.April, 1, 0, 0, 0, 0, time.UTC)) {
		entitlementEndDate = nil
	}

	var endTime *timestamp.Timestamp
	var err error
	if entitlementEndDate != nil {
		endTime, err = ptypes.TimestampProto(*entitlementEndDate)
		if err != nil {
			return errors.Wrap(err, "unable to parse reward end date")
		}
	}

	for userID, rewards := range entitledUsers {
		for _, reward := range rewards {
			if len(userID) == 0 || len(reward.ID) == 0 || reward.Type != percy.EmoteReward {
				continue
			}

			entitlements = append(entitlements, &makotwirp.EmoteEntitlement{
				OwnerId:     userID,
				ItemOwnerId: hypeTrainChannelID,
				EmoteId:     reward.ID,
				OriginId:    originIDPrefix + hypeTrain.ChannelID + "." + hypeTrain.ID,
				Group:       makotwirp.EmoteGroup_HypeTrain,
				Start:       startTime,
				End: &makotwirp.InfiniteTime{
					Time:       endTime,
					IsInfinite: entitlementEndDate == nil,
				},
			})
		}
	}

	if len(entitlements) == 0 {
		logrus.WithFields(logrus.Fields{"hypeTrainID": hypeTrain.ID}).Info("No users to entitle")
		return nil
	}

	var result error

	// Chunk entitlements into requests of 25
	for i := 0; i < len(entitlements); i += maxBatchSize {
		// Rate limit to 10 TPS
		if err := db.limiter.Wait(ctx); err != nil {
			result = multierror.Append(result, err)
		}

		entitlementsToWrite := entitlements[i:Min(len(entitlements), i+maxBatchSize)]
		reqCtx := wrapper.GenerateRequestContext(ctx, serviceName, createEntitlementsAPI)
		if _, err := db.client.CreateEmoteEntitlements(reqCtx, &makotwirp.CreateEmoteEntitlementsRequest{
			Entitlements: entitlementsToWrite,
		}); err != nil {
			result = multierror.Append(result, err)
		}
	}

	return result
}

// ReadEmoteEntitlements will read entitlements from Mako
func (db *EntitlementDB) ReadEmoteEntitlements(ctx context.Context, entitledUserID string) (map[string]string, error) {
	if len(entitledUserID) == 0 {
		return nil, fmt.Errorf("Cannot ReadEmoteEntitlements with empty userID: %v", entitledUserID)
	}

	reqCtx := wrapper.GenerateRequestContext(ctx, serviceName, getEntitlementsAPI)
	resp, err := db.client.GetEmoteEntitlements(reqCtx, &makotwirp.GetEmoteEntitlementsRequest{
		OwnerId: entitledUserID,
		Group:   makotwirp.EmoteGroup_HypeTrain,
	})
	if err != nil {
		return nil, err
	}

	entitledEmotes := make(map[string]string)
	for _, entitlement := range resp.Entitlements {
		if !isActive(*entitlement) {
			continue
		}
		entitledEmotes[entitlement.EmoteId] = entitlement.EmoteId
	}

	return entitledEmotes, nil
}

func isActive(entitlement makotwirp.EmoteEntitlement) bool {
	now := time.Now()

	startsAt, err := ptypes.Timestamp(entitlement.Start)
	if err != nil {
		return false
	}

	if now.Before(startsAt) {
		return false
	}

	if entitlement.End.IsInfinite {
		return true
	}

	endsAt, err := ptypes.Timestamp(entitlement.End.Time)
	if err != nil {
		return false
	}

	return now.Before(endsAt)
}

// Min ...
func Min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
