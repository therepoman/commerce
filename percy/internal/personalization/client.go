package personalization

import (
	"context"
	"net/http"

	personalizationtwirp "code.justin.tv/amzn/TwitchPersonalizationTwirp"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/wrapper"
	"code.justin.tv/foundation/twitchclient"
	"github.com/cactus/go-statsd-client/statsd"
)

const (
	serviceName                    = "personalization"
	GetHypeTrainConfigOverridesAPI = "GetHypeTrainConfigOverrides"
	defaultRetry                   = 1
	defaultTimeout                 = 1000
)

type Client struct {
	client personalizationtwirp.TwitchPersonalization
}

func NewClient(host string, stats statsd.Statter, socksAddr string) *Client {
	return &Client{
		client: personalizationtwirp.NewTwitchPersonalizationProtobufClient(host, twitchclient.NewHTTPClient(twitchclient.ClientConf{
			Stats: stats,
			RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
				wrapper.NewProxyRoundTripWrapper(socksAddr),
				wrapper.NewRetryRoundTripWrapper(stats, serviceName, defaultRetry),
				wrapper.NewHystrixRoundTripWrapper(serviceName, defaultTimeout),
			},
			Transport: twitchclient.TransportConf{
				MaxIdleConnsPerHost: 200,
			},
		})),
	}
}

func (c *Client) GetHypeTrainConfigOverrides(ctx context.Context, channelId string) (*percy.PersonalizedHypeTrainConfig, error) {
	reqCtx := wrapper.GenerateRequestContext(ctx, serviceName, GetHypeTrainConfigOverridesAPI)
	req := &personalizationtwirp.GetHypeTrainConfigOverridesRequest{
		ChannelId: channelId,
	}
	result, err := c.client.GetHypeTrainConfigOverrides(reqCtx, req)
	if err != nil {
		return nil, err
	}

	var difficulty percy.Difficulty
	switch result.Difficulty {
	case personalizationtwirp.HypeTrainDifficultyType_EASY:
		difficulty = percy.DifficultyEasy
	case personalizationtwirp.HypeTrainDifficultyType_MEDIUM:
		difficulty = percy.DifficultyMedium
	case personalizationtwirp.HypeTrainDifficultyType_HARD:
		difficulty = percy.DifficultyHard
	case personalizationtwirp.HypeTrainDifficultyType_SUPER_HARD:
		difficulty = percy.DifficultySuperHard
	case personalizationtwirp.HypeTrainDifficultyType_INSANE:
		difficulty = percy.DifficultyInsane
	default:
		return &percy.PersonalizedHypeTrainConfig{
			ShouldUseExistingConfig: true,
		}, nil
	}

	return &percy.PersonalizedHypeTrainConfig{
		ShouldUseExistingConfig: result.ShouldUseExistingConfig,
		NumOfEventsToKickoff:    int(result.Threshold),
		Difficulty:              difficulty,
	}, nil
}
