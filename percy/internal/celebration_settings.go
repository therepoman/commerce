package percy

import (
	"context"
	"time"

	celebration_error "code.justin.tv/commerce/percy/internal/errors"
	"github.com/pkg/errors"
)

// EventType which will potentially trigger a celebrations
type CelebrationEventType string

// Effect is the animation that will play when for the celebration
type CelebrationEffect string

// Area on which the animation will play
type CelebrationArea string

const (
	EventTypeCheer               = CelebrationEventType("BITS_CHEER")
	EventTypeSubscriptionGift    = CelebrationEventType("SUBSCRIPTION_GIFT")
	EventTypeSubscriptionFounder = CelebrationEventType("SUBSCRIPTION_FOUNDER")
	EventTypeExplicitPurchase    = CelebrationEventType("EXPLICIT_PURCHASE")
)

var ValidEventTypes = map[CelebrationEventType]bool{
	EventTypeCheer:            true,
	EventTypeSubscriptionGift: true,
}

const (
	EffectFireworks     = CelebrationEffect("FIREWORKS")
	EffectRain          = CelebrationEffect("RAIN")
	EffectFlamethrowers = CelebrationEffect("FLAMETHROWERS")
)

var ValidEffects = map[CelebrationEffect]bool{
	EffectFireworks:     true,
	EffectRain:          true,
	EffectFlamethrowers: true,
}

const (
	AreaEverywhere    = CelebrationArea("EVERYWHERE")
	AreaVideoOnly     = CelebrationArea("VIDEO_ONLY")
	AreaVideoAndPanel = CelebrationArea("VIDEO_AND_PANEL")
)

var ValidAreas = map[CelebrationArea]bool{
	AreaEverywhere:    true,
	AreaVideoOnly:     true,
	AreaVideoAndPanel: true,
}

type Celebration struct {
	CelebrationID        string               `json:"celebration_id"`
	Enabled              bool                 `json:"enabled"`
	EventType            CelebrationEventType `json:"event_type"`
	EventThreshold       int64                `json:"event_threshold"`
	CelebrationEffect    CelebrationEffect    `json:"effect"`
	CelebrationArea      CelebrationArea      `json:"area"`
	CelebrationIntensity int64                `json:"intensity"`
	CelebrationDuration  time.Duration        `json:"duration"`
}

type CelebrationConfig struct {
	ChannelID string `json:"channel_id"`
	Enabled   bool   `json:"enabled"`

	// mapping from celebration id to celebration
	Celebrations map[string]Celebration `json:"celebrations"`
}

func (config *CelebrationConfig) GetCelebration(celebrationID string) Celebration {
	celebration := config.Celebrations[celebrationID]
	return celebration
}

type CelebrationArgs struct {
	Enabled              *bool
	EventType            *CelebrationEventType
	EventThreshold       *int64
	CelebrationEffect    *CelebrationEffect
	CelebrationArea      *CelebrationArea
	CelebrationIntensity *int64
	CelebrationDuration  *time.Duration
}

//go:generate counterfeiter . CelebrationConfigsDB

type CelebrationConfigsDB interface {
	// Configs
	GetCelebrationConfig(ctx context.Context, channelID string) (*CelebrationConfig, error)
	UpdateCelebrationConfig(ctx context.Context, channelID string, enabled bool) (*CelebrationConfig, error)
	BackfillCelebrationConfig(ctx context.Context, channelID string) (*CelebrationConfig, error)
	GetDefaultCelebrationConfig(channelID string) *CelebrationConfig

	// Celebrations
	CreateCelebration(ctx context.Context, channelID string, args CelebrationArgs) (*Celebration, error)
	UpdateCelebration(ctx context.Context, channelID, celebrationID string, args CelebrationArgs) (*Celebration, error)
	DeleteCelebration(ctx context.Context, channelID, celebrationID string) error

	// INTERNAL ONLY
	DeleteCelebrationConfig(ctx context.Context, channelID string) error
}

func (eng *celebrationEngine) GetCelebrationConfig(ctx context.Context, channelID string, backfill bool) (CelebrationConfig, error) {
	if len(channelID) == 0 {
		return CelebrationConfig{}, errors.New("channelID is blank")
	}

	config, err := eng.CelebrationConfigsDB.GetCelebrationConfig(ctx, channelID)
	if err != nil {
		return CelebrationConfig{}, err
	}

	// In the case of no config, we return the default config for the user
	if config == nil {
		if backfill {
			config, err = eng.CelebrationConfigsDB.BackfillCelebrationConfig(ctx, channelID)
			if err != nil {
				return CelebrationConfig{}, err
			}
		} else {
			return *eng.CelebrationConfigsDB.GetDefaultCelebrationConfig(channelID), nil
		}
	}

	return *config, nil
}

func (eng *celebrationEngine) UpdateCelebrationConfig(ctx context.Context, channelID string, enabled bool) (CelebrationConfig, error) {
	if len(channelID) == 0 {
		return CelebrationConfig{}, errors.New("channelID is blank")
	}

	config, err := eng.CelebrationConfigsDB.UpdateCelebrationConfig(ctx, channelID, enabled)
	if err != nil {
		return CelebrationConfig{}, err
	}

	return *config, nil
}

func (eng *celebrationEngine) CreateCelebration(ctx context.Context, channelID string, args CelebrationArgs) (Celebration, error) {
	if len(channelID) == 0 {
		return Celebration{}, errors.New("channelID is blank")
	}

	celebration, err := eng.CelebrationConfigsDB.CreateCelebration(ctx, channelID, args)
	if err != nil {
		return Celebration{}, err
	}

	return *celebration, nil
}

func (eng *celebrationEngine) UpdateCelebration(ctx context.Context, channelID, celebrationID string, args CelebrationArgs) (Celebration, error) {
	if len(channelID) == 0 {
		return Celebration{}, errors.New("channelID is blank")
	}

	if len(celebrationID) == 0 {
		return Celebration{}, errors.New("celebrationID is blank")
	}

	celebration, err := eng.CelebrationConfigsDB.UpdateCelebration(ctx, channelID, celebrationID, args)
	if err != nil {
		return Celebration{}, err
	}

	return *celebration, nil
}

func (eng *celebrationEngine) DeleteCelebration(ctx context.Context, channelID, celebrationID string) error {
	if len(channelID) == 0 {
		return errors.New("channelID is blank")
	}

	if len(celebrationID) == 0 {
		return errors.New("celebrationID is blank")
	}

	err := eng.CelebrationConfigsDB.DeleteCelebration(ctx, channelID, celebrationID)
	if err != nil {
		return err
	}

	return nil
}

func (eng *celebrationEngine) Authorize(ctx context.Context, channelID, userID string) error {
	// Owners can always edit their own channels
	if userID == channelID {
		return nil
	}

	isEditor, err := eng.EditorDB.GetEditorStatus(ctx, channelID, userID)
	if err != nil {
		return errors.Wrap(err, "failed to load editor information from hallpass")
	}

	if isEditor {
		return nil
	}

	profile, err := eng.UserProfileDB.GetProfileByID(ctx, userID)
	if err != nil {
		return errors.Wrap(err, "failed to load requester information from users service")
	}

	// Staff can edit celebrations as well
	if profile.IsStaff {
		return nil
	}

	return celebration_error.PermissionDenied.New("user %s does not have permission", userID)
}
