package percy

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/logrus"
)

const (
	celebrationTypeMetricFormat = "celebration-event-type-%s-initiated"

	subscriptionFounderCelebrationID = "subscription_founder_celebration"
)

// placeholder for founder celebration, this would be moved to our database later. https://jira.twitch.com/browse/MOMENTS-743
var subscriptionFounderCelebration = Celebration{
	CelebrationID:        subscriptionFounderCelebrationID,
	CelebrationArea:      AreaEverywhere,
	CelebrationEffect:    EffectFireworks,
	CelebrationDuration:  4000 * time.Millisecond,
	CelebrationIntensity: int64(50), // 50 should map to Wow, or more like a mid-Wow...
	Enabled:              true,
	EventThreshold:       int64(1),
	EventType:            EventTypeSubscriptionFounder,
}

func (eng *celebrationEngine) StartHighestValueCelebration(ctx context.Context, channelID string, sender User, eventType CelebrationEventType, amount int64, transactionID string) error {
	logger := logrus.WithFields(logrus.Fields{
		"channelID":     channelID,
		"senderID":      sender.Id,
		"eventType":     eventType,
		"amount":        amount,
		"transactionID": transactionID,
	})

	celebrationConfig, err := eng.GetCelebrationConfig(ctx, channelID, false)
	if err != nil {
		logger.WithError(err).Error("Failed to load celebration configuration")
		return err
	}
	if !celebrationConfig.Enabled {
		return nil
	}
	celebration, ok := findHighestValueCelebration(celebrationConfig, eventType, amount)
	if ok {
		err := eng.Publisher.StartCelebration(ctx, channelID, sender, amount, celebration)
		if err != nil {
			logger.WithField("celebrationID", celebration.CelebrationID).WithError(err).Error("Could not publish StartCelebration event to pubsub")
			return err
		}

		go func() {
			eng.SendCelebrationOverlayInitiatedEvent(channelID, sender.ID(), celebration.CelebrationID, string(eventType), transactionID)
			_ = eng.Statter.Inc(fmt.Sprintf(celebrationTypeMetricFormat, string(eventType)), 1, 1.0)
		}()
	}
	return nil
}

func findHighestValueCelebration(celebrationConfig CelebrationConfig, eventType CelebrationEventType, amount int64) (Celebration, bool) {
	var highestValueCelebration Celebration
	foundValidCelebration := false

	// For founders badge celebration experiment
	// inject the founders badge celebration into our set of available celebrations
	// Should remove this once we are out of experiment phase
	// TODO: https://jira.twitch.com/browse/MOMENTS-743
	celebrationConfig.Celebrations[subscriptionFounderCelebrationID] = subscriptionFounderCelebration

	for _, celebration := range celebrationConfig.Celebrations {
		if celebration.Enabled && celebration.EventType == eventType && amount >= celebration.EventThreshold {
			// In the event of multiple celebrations with the same threshold, this logic will keep the first-encountered.
			if celebration.EventThreshold == highestValueCelebration.EventThreshold {
				logrus.Warnf("Encountered two celebration configs with the same event threshold for the same event type."+
					" This should not be possible. Channel %v, Celebration IDs %v and %v",
					celebrationConfig.ChannelID, celebration.CelebrationID, highestValueCelebration.CelebrationID,
				)
			}
			if celebration.EventThreshold > highestValueCelebration.EventThreshold {
				highestValueCelebration = celebration
				foundValidCelebration = true
			}
		}
	}
	return highestValueCelebration, foundValidCelebration
}
