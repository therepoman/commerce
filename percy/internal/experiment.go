package percy

import (
	"context"
)

//go:generate counterfeiter . ExperimentDB

type ExperimentDB interface {
	HasFounderCelebrations(ctx context.Context, channelID string) bool
	Close()
}

func (e *celebrationEngine) HasFounderCelebrations(ctx context.Context, channelID string) bool {
	if channelID == "" {
		return false
	}

	return e.FoundersExperimentDB.HasFounderCelebrations(ctx, channelID)
}
