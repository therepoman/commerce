package percy

import "context"

//go:generate counterfeiter . EditorDB

type EditorDB interface {
	GetEditorStatus(ctx context.Context, channelID, userID string) (bool, error)
}
