package percy

import (
	"context"
	"time"

	"github.com/pkg/errors"
)

//go:generate counterfeiter . ApproachingDB

type ApproachingDB interface {
	SetApproaching(ctx context.Context, approaching Approaching) error
	GetApproaching(ctx context.Context, channelID string) (*Approaching, error)
}

const (
	ApproachingMaxTimeShown = 30 * time.Minute
)

func (eng *engine) GetHypeTrainApproaching(ctx context.Context, channelID string) (*Approaching, error) {
	if len(channelID) == 0 {
		return nil, errors.New("channel ID is blank")
	}

	approaching, err := eng.ApproachingDB.GetApproaching(ctx, channelID)

	return approaching, err
}

func (eng *engine) UpdateHypeTrainApproaching(ctx context.Context, approaching Approaching) error {
	if len(approaching.ChannelID) == 0 {
		return errors.New("channel ID is blank")
	}

	if len(approaching.ID) == 0 {
		return errors.New("approaching ID is blank")
	}

	err := eng.ApproachingDB.SetApproaching(ctx, approaching)
	if err != nil {
		return err
	}

	return nil
}

type Approaching struct {
	ChannelID               string
	ID                      string
	CreatedTime             time.Time
	StartedAt               time.Time
	ExpiresAt               time.Time
	UpdatedAt               time.Time
	Participants            []string
	Goal                    int64
	EventsRemainingDuration map[int]time.Duration
	CreatorColor            string
	TimeShownPerHour        time.Duration
	NumberOfTimesShown      int
	RateLimited             bool
	LevelOneRewards         []Reward
	GhostMode               bool
}

/*
 ApproachingRateLimitFunc is a helper function to help calculate the amount of time HypeTrain Approaching has shown.

 Returns:
    rateLimited - Whether or not the Approaching prompt should be shown altogether
    newStart - The updated start timestamp of the Approaching entry.
    newEnd - The updated end timestamp for the Approaching entry.
    newTotalTimeShown - The total time the Approaching entry will be shown for (including the future time to be shown)
    newShowInstance - Whether or not this Approaching instance is a new pop up or if it is a time extension
*/
func ApproachingRateLimitFunc(currentTime time.Time, lastStarted time.Time, lastEnded time.Time, timeShown time.Duration, durationToShow time.Duration) (rateLimited bool, newStart time.Time, newEnd time.Time, newTotalTimeShown time.Duration, newShowInstance bool) {
	if durationToShow+timeShown > ApproachingMaxTimeShown {
		remainder := durationToShow + timeShown - ApproachingMaxTimeShown
		durationToShow = capDurationAtZero(durationToShow - remainder)
	}

	if durationToShow <= 0 {
		return true, time.Time{}, time.Time{}, 0, false
	}

	var newTimeShown time.Duration
	newInstance := false

	newEnd = currentTime.Add(durationToShow)

	if currentTime.After(lastEnded) {
		newStart = currentTime
		newTimeShown = timeShown + capDurationAtZero(newEnd.Sub(newStart))
		newInstance = true

	} else {
		newStart = lastStarted
		newTimeShown = timeShown + capDurationAtZero(newEnd.Sub(lastEnded))
	}

	return false, newStart, newEnd, newTimeShown, newInstance
}

func capDurationAtZero(duration time.Duration) time.Duration {
	if duration < 0 {
		return 0
	} else {
		return duration
	}
}

func (a *Approaching) IsActive() bool {
	now := time.Now()

	if now.Before(a.ExpiresAt) && now.After(a.StartedAt) && !a.RateLimited && !a.GhostMode {
		return true
	}

	return false
}
