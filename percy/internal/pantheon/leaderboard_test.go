package pantheon

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/pantheon/pantheonfakes"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func TestLeaderboard_Publish(t *testing.T) {
	Convey("Given a leaderboard", t, func() {
		pantheon := &pantheonfakes.FakePantheon{}
		leaderboard := Leaderboard{
			client: pantheon,
		}

		channelID := uuid.New().String()
		hypeTrainID := uuid.New().String()
		userID := uuid.New().String()
		source := percy.BitsSource
		ctx := context.Background()

		Convey("can publish new participation", func() {
			pantheon.PublishEventReturns(&pantheonrpc.PublishEventResp{}, nil)

			id := percy.LeaderboardID{
				ChannelID:   uuid.New().String(),
				HypeTrainID: uuid.New().String(),
				Source:      percy.BitsSource,
			}

			participation := percy.Participation{
				ChannelID: id.ChannelID,
				User:      percy.NewUser(uuid.New().String()),
				Timestamp: time.Now(),
				Quantity:  10,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				},
			}

			err := leaderboard.Publish(ctx, id, participation)

			So(err, ShouldBeNil)
			So(pantheon.PublishEventCallCount(), ShouldEqual, 1)

			Convey("to the correct leaderboard by participation source", func() {
				_, req := pantheon.PublishEventArgsForCall(0)
				So(req.GroupingKey, ShouldEqual, formatGroupingKey(id))
			})
		})

		Convey("does not publish anonymous users", func() {
			pantheon.PublishEventReturns(&pantheonrpc.PublishEventResp{}, nil)

			id := percy.LeaderboardID{
				ChannelID:   uuid.New().String(),
				HypeTrainID: uuid.New().String(),
				Source:      percy.BitsSource,
			}

			participation := percy.Participation{
				ChannelID: id.ChannelID,
				User:      percy.NewAnonymousCheerer(),
				Timestamp: time.Now(),
				Quantity:  10,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				},
			}

			err := leaderboard.Publish(ctx, id, participation)
			So(err, ShouldBeNil)
			So(pantheon.PublishEventCallCount(), ShouldEqual, 0)
		})

		Convey("returns an error when", func() {
			Convey("channel ID is missing", func() {
				channelID = ""
			})

			Convey("hype train ID is missing", func() {
				hypeTrainID = ""
			})

			Convey("leaderboard source doesn't match the participation source", func() {
				source = percy.SubsSource
			})

			Convey("pantheon client returns an error", func() {
				pantheon.PublishEventReturns(nil, errors.New("pantheon error"))
			})

			id := percy.LeaderboardID{
				ChannelID:   channelID,
				HypeTrainID: hypeTrainID,
				Source:      source,
			}

			participation := percy.Participation{
				ChannelID: channelID,
				User:      percy.NewUser(userID),
				Timestamp: time.Now(),
				Quantity:  10,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				},
			}

			err := leaderboard.Publish(ctx, id, participation)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestLeaderboard_BatchPublish(t *testing.T) {
	Convey("Given a leaderboard", t, func() {
		pantheon := &pantheonfakes.FakePantheon{}
		leaderboard := Leaderboard{
			client: pantheon,
		}

		ctx := context.Background()

		channelID := uuid.New().String()
		hypeTrainID := uuid.New().String()
		id := percy.LeaderboardID{
			ChannelID:   channelID,
			HypeTrainID: hypeTrainID,
			Source:      percy.BitsSource,
		}

		user1 := percy.NewUser(uuid.New().String())
		user2 := percy.NewUser(uuid.New().String())
		participations := []percy.Participation{
			{
				ChannelID: id.ChannelID,
				User:      user1,
				Timestamp: time.Now(),
				Quantity:  10,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				},
			},
			{
				ChannelID: id.ChannelID,
				User:      user2,
				Timestamp: time.Now(),
				Quantity:  20,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				},
			},
			{
				ChannelID: id.ChannelID,
				User:      user1,
				Timestamp: time.Now(),
				Quantity:  100,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.PollAction,
				},
			},
			{
				ChannelID: id.ChannelID,
				User:      user1,
				Timestamp: time.Now(),
				Quantity:  10,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.SubsSource,
					Action: percy.Tier1GiftedSubAction,
				},
			},
			{
				ChannelID: id.ChannelID,
				User:      percy.NewAnonymousGifter(),
				Timestamp: time.Now(),
				Quantity:  10,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.SubsSource,
					Action: percy.Tier1GiftedSubAction,
				},
			},
		}

		Convey("can publish new participations", func() {
			pantheon.PublishEventReturns(&pantheonrpc.PublishEventResp{}, nil)

			err := leaderboard.BatchPublish(ctx, channelID, hypeTrainID, participations...)
			So(err, ShouldBeNil)

			Convey("grouped by source and user", func() {
				So(pantheon.PublishEventCallCount(), ShouldEqual, 3)
				_, req1 := pantheon.PublishEventArgsForCall(0)
				_, req2 := pantheon.PublishEventArgsForCall(1)
				_, req3 := pantheon.PublishEventArgsForCall(2)

				calledUserIDs := []string{req1.EntryKey, req2.EntryKey, req3.EntryKey}
				So(calledUserIDs, ShouldContain, user1.ID())
				So(calledUserIDs, ShouldContain, user2.ID())

				calledGroupingKeys := []string{req1.GroupingKey, req2.GroupingKey, req3.GroupingKey}
				Convey("1 call to the Subs leaderboard", func() {
					subsID := percy.LeaderboardID{
						ChannelID:   channelID,
						HypeTrainID: hypeTrainID,
						Source:      percy.SubsSource,
					}
					So(calledGroupingKeys, ShouldContain, formatGroupingKey(subsID))
				})

				Convey("2 calls to the Bits leaderboard", func() {
					bitsID := percy.LeaderboardID{
						ChannelID:   channelID,
						HypeTrainID: hypeTrainID,
						Source:      percy.BitsSource,
					}

					bitsCalls := []string{}
					for _, key := range calledGroupingKeys {
						if key == formatGroupingKey(bitsID) {
							bitsCalls = append(bitsCalls, key)
						}
					}

					So(bitsCalls, ShouldHaveLength, 2)
				})
			})
		})

		Convey("returns an error when", func() {
			Convey("channel ID is missing", func() {
				channelID = ""
			})

			Convey("hype train ID is missing", func() {
				hypeTrainID = ""
			})

			Convey("pantheon client returns an error", func() {
				pantheon.PublishEventReturns(nil, errors.New("pantheon error"))
			})

			err := leaderboard.BatchPublish(ctx, channelID, hypeTrainID, participations...)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestLeaderboard_Top(t *testing.T) {
	Convey("Given a leaderboard", t, func() {
		pantheon := &pantheonfakes.FakePantheon{}

		leaderboard := Leaderboard{
			client: pantheon,
		}

		userID := uuid.New().String()
		ctx := context.Background()

		Convey("can get leaderboard leader", func() {
			pantheon.GetLeaderboardReturns(&pantheonrpc.GetLeaderboardResp{
				Top: []*pantheonrpc.LeaderboardEntry{
					{
						Rank:      1,
						Score:     100,
						EntryKey:  userID,
						Moderated: false,
					},
				},
			}, nil)

			id := percy.LeaderboardID{
				ChannelID:   uuid.New().String(),
				HypeTrainID: uuid.New().String(),
				Source:      percy.BitsSource,
			}

			user, err := leaderboard.Top(ctx, id)
			So(err, ShouldBeNil)
			So(user.ID(), ShouldEqual, userID)

			Convey("from the correct leaderboard by participation source", func() {
				_, req := pantheon.GetLeaderboardArgsForCall(0)
				So(req.GroupingKey, ShouldEqual, formatGroupingKey(id))
			})
		})

		Convey("returns an error when", func() {
			channelID := uuid.New().String()
			hypeTrainID := uuid.New().String()

			Convey("channel ID is missing", func() {
				channelID = ""
			})

			Convey("hype train ID is missing", func() {
				hypeTrainID = ""
			})

			Convey("pantheon client returns an error", func() {
				pantheon.GetLeaderboardReturns(nil, errors.New("pantheon error"))
			})

			user, err := leaderboard.Top(ctx, percy.LeaderboardID{
				ChannelID:   channelID,
				HypeTrainID: hypeTrainID,
				Source:      percy.BitsSource,
			})
			So(err, ShouldNotBeNil)
			So(user, ShouldBeNil)
		})
	})
}

func TestLeaderboard_Moderate(t *testing.T) {
	Convey("Given a leaderboard", t, func() {
		pantheon := &pantheonfakes.FakePantheon{}

		leaderboard := Leaderboard{
			client: pantheon,
		}

		userID := uuid.New().String()
		ctx := context.Background()

		Convey("can moderate a user from a leaderboard", func() {
			pantheon.ModerateEntryReturns(&pantheonrpc.ModerateEntryResp{}, nil)

			id := percy.LeaderboardID{
				ChannelID:   uuid.New().String(),
				HypeTrainID: uuid.New().String(),
				Source:      percy.BitsSource,
			}
			err := leaderboard.Moderate(ctx, id, userID)
			So(err, ShouldBeNil)

			Convey("from the correct leaderboard by participation source", func() {
				_, req := pantheon.ModerateEntryArgsForCall(0)
				So(req.GroupingKey, ShouldEqual, formatGroupingKey(id))
				So(req.ModerationAction, ShouldEqual, pantheonrpc.ModerationAction_MODERATE)
			})
		})

		Convey("returns an error when", func() {
			channelID := uuid.New().String()
			hypeTrainID := uuid.New().String()
			userID := uuid.New().String()

			Convey("channel ID is missing", func() {
				channelID = ""
			})

			Convey("hype train ID is missing", func() {
				hypeTrainID = ""
			})

			Convey("user ID is missing", func() {
				userID = ""
			})

			Convey("pantheon client returns an error", func() {
				pantheon.ModerateEntryReturns(nil, errors.New("pantheon error"))
			})

			err := leaderboard.Moderate(ctx, percy.LeaderboardID{
				ChannelID:   channelID,
				HypeTrainID: hypeTrainID,
				Source:      percy.BitsSource,
			}, userID)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestLeaderboard_Unmoderate(t *testing.T) {
	Convey("Given a leaderboard", t, func() {
		pantheon := &pantheonfakes.FakePantheon{}

		leaderboard := Leaderboard{
			client: pantheon,
		}

		userID := uuid.New().String()
		ctx := context.Background()

		Convey("can unmoderate a user from a leaderboard", func() {
			pantheon.ModerateEntryReturns(&pantheonrpc.ModerateEntryResp{}, nil)

			id := percy.LeaderboardID{
				ChannelID:   uuid.New().String(),
				HypeTrainID: uuid.New().String(),
				Source:      percy.BitsSource,
			}

			err := leaderboard.Unmoderate(ctx, id, userID)
			So(err, ShouldBeNil)

			Convey("from the correct leaderboard by participation source", func() {
				_, req := pantheon.ModerateEntryArgsForCall(0)
				So(req.GroupingKey, ShouldContainSubstring, formatGroupingKey(id))
				So(req.ModerationAction, ShouldEqual, pantheonrpc.ModerationAction_UNMODERATE)
			})
		})

		Convey("returns an error when", func() {
			channelID := uuid.New().String()
			hypeTrainID := uuid.New().String()
			userID := uuid.New().String()

			Convey("channel ID is missing", func() {
				channelID = ""
			})

			Convey("hype train ID is missing", func() {
				hypeTrainID = ""
			})

			Convey("user ID is missing", func() {
				userID = ""
			})

			Convey("pantheon client returns an error", func() {
				pantheon.ModerateEntryReturns(nil, errors.New("pantheon error"))
			})

			err := leaderboard.Unmoderate(ctx, percy.LeaderboardID{
				ChannelID:   channelID,
				HypeTrainID: hypeTrainID,
				Source:      percy.BitsSource,
			}, userID)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestLeaderboardID_String(t *testing.T) {
	Convey("Given a leaderboard ID", t, func() {
		leaderboardID := percy.LeaderboardID{
			ChannelID:   uuid.New().String(),
			HypeTrainID: uuid.New().String(),
			Source:      percy.SubsSource,
		}

		id := formatGroupingKey(leaderboardID)

		Convey("should contain the channel ID", func() {
			So(id, ShouldContainSubstring, leaderboardID.ChannelID)
		})

		Convey("should contain the hype train ID", func() {
			So(id, ShouldContainSubstring, leaderboardID.HypeTrainID)
		})

		Convey("should contain the source", func() {
			So(id, ShouldContainSubstring, string(leaderboardID.Source))
		})
	})
}

func TestLeaderboardID_Parse(t *testing.T) {
	Convey("Given a leaderboard ID", t, func() {
		leaderboardID := percy.LeaderboardID{
			ChannelID:   uuid.New().String(),
			HypeTrainID: uuid.New().String(),
			Source:      percy.SubsSource,
		}

		Convey("can parse its string format", func() {
			parsed, err := ParseLeaderboardID(formatGroupingKey(leaderboardID))
			So(err, ShouldBeNil)
			So(parsed, ShouldResemble, leaderboardID)
		})
	})

	Convey("returns an error when", t, func() {
		var id string

		Convey("string is empty", func() {
			id = ""
		})

		Convey("string is missing parts", func() {
			id = "channel___train"
		})

		Convey("string has too many parts", func() {
			id = "channel___train___source___other"
		})

		_, err := ParseLeaderboardID(id)
		So(err, ShouldNotBeNil)
	})
}

func TestLeaderboard_AggregateParticpationsPerSourceAndUser(t *testing.T) {
	channelID := uuid.New().String()
	timestamp := time.Now()

	user1 := percy.NewUser(uuid.New().String())
	user2 := percy.NewUser(uuid.New().String())
	user3 := percy.NewUser(uuid.New().String())

	Convey("with participations from 3 different users", t, func() {
		participations := []percy.Participation{
			{
				ChannelID: channelID,
				Timestamp: timestamp,
				User:      user1,
				Quantity:  100,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				},
			},
			{
				ChannelID: channelID,
				Timestamp: timestamp,
				User:      user1,
				Quantity:  500,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				},
			},
			{
				ChannelID: channelID,
				Timestamp: timestamp,
				User:      user1,
				Quantity:  2,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.SubsSource,
					Action: percy.Tier1GiftedSubAction,
				},
			},
			{
				ChannelID: channelID,
				Timestamp: timestamp,
				User:      user2,
				Quantity:  3,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.SubsSource,
					Action: percy.Tier1GiftedSubAction,
				},
			},
			{
				ChannelID: channelID,
				Timestamp: timestamp,
				User:      user2,
				Quantity:  2,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.SubsSource,
					Action: percy.Tier2GiftedSubAction,
				},
			},
			{
				ChannelID: channelID,
				Timestamp: timestamp,
				User:      user3,
				Quantity:  1,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.SubsSource,
					Action: percy.Tier3SubAction,
				},
			},
		}
		Convey("should aggregate user participations within the same source", func() {
			entries := aggregateParticipationsPerSourceAndUser(participations...)

			So(entries, ShouldHaveLength, 4) // 2 entries for user 1, 1 entry for user 2, 1 entry for user 3
			So(entries, ShouldContain, batchPublishEntry{
				UserID:   user1.ID(),
				Source:   percy.BitsSource,
				Quantity: 600,
			})
			So(entries, ShouldContain, batchPublishEntry{
				UserID:   user1.ID(),
				Source:   percy.SubsSource,
				Quantity: 2,
			})
			So(entries, ShouldContain, batchPublishEntry{
				UserID:   user2.ID(),
				Source:   percy.SubsSource,
				Quantity: 5,
			})
			So(entries, ShouldContain, batchPublishEntry{
				UserID:   user3.ID(),
				Source:   percy.SubsSource,
				Quantity: 1,
			})
		})
	})
}
