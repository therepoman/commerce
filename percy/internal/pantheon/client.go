package pantheon

import (
	"net/http"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	"code.justin.tv/commerce/percy/internal/wrapper"
	"code.justin.tv/foundation/twitchclient"
	"github.com/cactus/go-statsd-client/statsd"
)

const (
	serviceName    = "pantheon"
	defaultRetry   = 3
	defaultTimeout = 2000
)

func NewPantheonClient(endpoint string, stats statsd.Statter, socksAddr string) pantheonrpc.Pantheon {
	return pantheonrpc.NewPantheonProtobufClient(endpoint, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Stats: stats,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(socksAddr),
			wrapper.NewRetryRoundTripWrapper(stats, serviceName, defaultRetry),
			wrapper.NewHystrixRoundTripWrapper(serviceName, defaultTimeout),
		},
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: 200,
		},
	}))
}
