package pantheon

import (
	"context"
	"fmt"
	"strings"
	"sync"
	"time"

	"code.justin.tv/commerce/pantheon/rpc/pantheonrpc"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/wrapper"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/google/uuid"
	"github.com/hashicorp/go-multierror"
	"github.com/pkg/errors"
)

const (
	Domain = "trains"

	concurrentBatchWorkers = 4

	leaderboardIDFormat          = "%s:%s:%s"
	leaderboardIDFormatDelimiter = ":"

	publishEventAPI   = "publishEvent"
	getLeaderboardAPI = "getLeaderboard"
	moderateEntryAPI  = "moderateEntry"
)

type Leaderboard struct {
	client pantheonrpc.Pantheon
}

func NewLeaderboard(endpoint string, stats statsd.Statter, socksAddr string) *Leaderboard {
	return &Leaderboard{
		client: NewPantheonClient(endpoint, stats, socksAddr),
	}
}

func (leaderboard *Leaderboard) Publish(ctx context.Context, leaderboardID percy.LeaderboardID, participation percy.Participation) error {
	if len(leaderboardID.ChannelID) == 0 {
		return errors.New("channel ID is missing")
	}

	if len(leaderboardID.HypeTrainID) == 0 {
		return errors.New("hype train ID is missing")
	}

	if leaderboardID.Source != participation.Source {
		return errors.New("leaderboardID source does not match the participation source")
	}

	if participation.User.IsAnonymous() {
		return nil
	}

	req := &pantheonrpc.PublishEventReq{
		Domain:      Domain,
		EventId:     uuid.New().String(), // TODO: use a transaction ID, will revisit once we wire up with event bus events
		EntryKey:    participation.User.ID(),
		EventValue:  int64(participation.Quantity),
		GroupingKey: formatGroupingKey(leaderboardID),
		TimeOfEvent: uint64(participation.Timestamp.UnixNano()),
	}
	ctx = wrapper.GenerateRequestContext(ctx, serviceName, publishEventAPI)
	if _, err := leaderboard.client.PublishEvent(ctx, req); err != nil {
		return err
	}

	return nil
}

type batchPublishEntry struct {
	UserID   string
	Source   percy.ParticipationSource
	Quantity int
}

// Pantheon doesn't have a batch publish event API, so here we group participations by users and spawn a few workers to send all participations over.
func (leaderboard *Leaderboard) BatchPublish(ctx context.Context, channelID, hypeTrainID string, participations ...percy.Participation) error {
	if len(participations) == 0 {
		return nil
	}

	if len(channelID) == 0 {
		return errors.New("channel ID is missing")
	}

	if len(hypeTrainID) == 0 {
		return errors.New("hype train ID is missing")
	}

	workChan := make(chan batchPublishEntry, len(participations))
	errChan := make(chan error, len(participations))

	// spawn goroutines to publish events in parallel
	wg := sync.WaitGroup{}
	wg.Add(concurrentBatchWorkers)
	reqCtx := wrapper.GenerateRequestContext(ctx, serviceName, publishEventAPI)
	for i := 0; i < concurrentBatchWorkers; i++ {
		go func() {
			for {
				entry, ok := <-workChan
				if !ok {
					wg.Done()
					return
				}

				leaderboardID := percy.LeaderboardID{
					ChannelID:   channelID,
					HypeTrainID: hypeTrainID,
					Source:      entry.Source,
				}

				_, err := leaderboard.client.PublishEvent(reqCtx, &pantheonrpc.PublishEventReq{
					Domain:      Domain,
					EventId:     uuid.New().String(), // TODO: use a transaction ID, will revisit once we wire up with event bus events
					EntryKey:    entry.UserID,
					EventValue:  int64(entry.Quantity),
					GroupingKey: formatGroupingKey(leaderboardID),
					TimeOfEvent: uint64(time.Now().UnixNano()),
				})
				if err != nil {
					errChan <- err
				}
			}
		}()
	}

	batchEntries := aggregateParticipationsPerSourceAndUser(participations...)
	for _, entry := range batchEntries {
		workChan <- entry
	}
	close(workChan)
	wg.Wait()

	var multierr *multierror.Error
	for len(errChan) > 0 {
		multierr = multierror.Append(multierr, <-errChan)
	}

	return multierr.ErrorOrNil()
}

func (leaderboard *Leaderboard) Top(ctx context.Context, leaderboardID percy.LeaderboardID) (*percy.User, error) {
	if len(leaderboardID.ChannelID) == 0 {
		return nil, errors.New("channel ID is missing")
	}

	if len(leaderboardID.HypeTrainID) == 0 {
		return nil, errors.New("hype train ID is missing")
	}

	reqCtx := wrapper.GenerateRequestContext(ctx, serviceName, getLeaderboardAPI)
	resp, err := leaderboard.client.GetLeaderboard(reqCtx, &pantheonrpc.GetLeaderboardReq{
		Domain:      Domain,
		GroupingKey: formatGroupingKey(leaderboardID),
		TimeUnit:    pantheonrpc.TimeUnit_ALLTIME,
		TopN:        1,
	})
	if err != nil {
		return nil, err
	}

	var user *percy.User
	if resp != nil && len(resp.Top) > 0 && resp.Top[0] != nil {
		top := percy.NewUser(resp.Top[0].EntryKey)
		user = &top
	}

	return user, err
}

func (leaderboard *Leaderboard) Moderate(ctx context.Context, leaderboardID percy.LeaderboardID, userID string) error {
	if len(leaderboardID.ChannelID) == 0 {
		return errors.New("channel ID is missing")
	}

	if len(leaderboardID.HypeTrainID) == 0 {
		return errors.New("hype train ID is missing")
	}

	if len(userID) == 0 {
		return errors.New("user ID is missing")
	}

	req := &pantheonrpc.ModerateEntryReq{
		Domain:           Domain,
		GroupingKey:      formatGroupingKey(leaderboardID),
		EntryKey:         userID,
		ModerationAction: pantheonrpc.ModerationAction_MODERATE,
	}
	reqCtx := wrapper.GenerateRequestContext(ctx, serviceName, moderateEntryAPI)
	if _, err := leaderboard.client.ModerateEntry(reqCtx, req); err != nil {
		return err
	}

	return nil
}

func (leaderboard *Leaderboard) Unmoderate(ctx context.Context, leaderboardID percy.LeaderboardID, userID string) error {
	if len(leaderboardID.ChannelID) == 0 {
		return errors.New("channel ID is missing")
	}

	if len(leaderboardID.HypeTrainID) == 0 {
		return errors.New("hype train ID is missing")
	}

	if len(userID) == 0 {
		return errors.New("user ID is missing")
	}

	req := &pantheonrpc.ModerateEntryReq{
		Domain:           Domain,
		GroupingKey:      formatGroupingKey(leaderboardID),
		EntryKey:         userID,
		ModerationAction: pantheonrpc.ModerationAction_UNMODERATE,
	}
	reqCtx := wrapper.GenerateRequestContext(ctx, serviceName, moderateEntryAPI)
	if _, err := leaderboard.client.ModerateEntry(reqCtx, req); err != nil {
		return err
	}

	return nil
}

func formatGroupingKey(id percy.LeaderboardID) string {
	return fmt.Sprintf(leaderboardIDFormat, id.ChannelID, id.HypeTrainID, string(id.Source))
}

func ParseLeaderboardID(id string) (percy.LeaderboardID, error) {
	tokens := strings.Split(id, leaderboardIDFormatDelimiter)
	if len(tokens) != 3 {
		return percy.LeaderboardID{}, errors.Errorf("invalid leaderboard ID %s", id)
	}

	return percy.LeaderboardID{
		ChannelID:   tokens[0],
		HypeTrainID: tokens[1],
		Source:      percy.ParticipationSource(tokens[2]),
	}, nil
}

func aggregateParticipationsPerSourceAndUser(participations ...percy.Participation) []batchPublishEntry {
	entries := make([]batchPublishEntry, 0)
	// group participations by source
	participationsBySource := make(map[percy.ParticipationSource][]percy.Participation)
	for _, participation := range participations {
		if participation.User.IsAnonymous() {
			continue
		}
		participationsBySource[participation.Source] = append(participationsBySource[participation.Source], participation)
	}

	// for each source, aggregate participation quantities by user ID and put into work chan
	for source, participationsOfSource := range participationsBySource {
		quantitiesByUserID := make(map[string]int)
		for _, participation := range participationsOfSource {
			userID := participation.User.ID()
			quantitiesByUserID[userID] = quantitiesByUserID[userID] + participation.Quantity
		}

		for userID, totalQuantity := range quantitiesByUserID {
			entries = append(entries, batchPublishEntry{
				UserID:   userID,
				Source:   source,
				Quantity: totalQuantity,
			})
		}
	}

	return entries
}
