package percy

import (
	"context"
	"time"
)

//go:generate counterfeiter . BitsPinataDB

type BitsPinataDB interface {
	CreateBitsPinata(ctx context.Context, bitsPinata BitsPinata) error
	GetActiveBitsPinata(ctx context.Context, channelID string) (*BitsPinata, error)
	AddBitsPinataContribution(ctx context.Context, channelID string, bitsContributed int) (*BitsPinata, error)
}

type BitsPinata struct {
	ChannelID        string    `json:"channel_id"`
	CreatedAt        time.Time `json:"created_at"`
	PinataID         string    `json:"pinata_id"`
	TimerID          string    `json:"timer_id"`
	StreamID         string    `json:"stream_id"`
	BitsToBreak      int64     `json:"bits_to_break"`
	BitsContributed  int64     `json:"bits_contributed"`
	LastContribution time.Time `json:"last_contribution"`
}
