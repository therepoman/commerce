package miniexperiments

/*
	For each miniexperiment, create a new sub-package and have a separate client implementation specific
 	for that experiment.
*/

//go:generate counterfeiter . Client

type Client interface {
	GetExperimentID() string
	GetExperimentName() string
	GetTreatment(userID string) (string, error)
	GetTreatmentGroups() []string
	Close() error
}
