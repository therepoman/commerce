package miniexperiments

type noopClient struct{}

func NewNoopClient() Client {
	return &noopClient{}
}

func (c *noopClient) GetExperimentName() string {
	return ""
}

func (c *noopClient) GetExperimentID() string {
	return ""
}

func (c *noopClient) GetTreatmentGroups() []string {
	return []string{}
}

func (c *noopClient) GetTreatment(string) (string, error) {
	return "", nil
}

func (c *noopClient) Close() error {
	return nil
}
