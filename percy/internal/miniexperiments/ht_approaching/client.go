package ht_approaching

import (
	"fmt"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/internal/miniexperiments"
	"code.justin.tv/discovery/experiments"
	"code.justin.tv/discovery/experiments/experiment"
)

const (
	experimentsPollingInterval = 1 * time.Minute
	experimentName             = "hype_train_approaching_2"
	experimentId               = "96771625-1381-489b-ac51-1cc43c50d67d"

	TreatmentControl    = "Control"
	TreatmentExperiment = "Experiment"
)

type experimentsClient struct {
	client experiments.Experiments
}

func NewClient() miniexperiments.Client {
	client, err := experiments.New(experiments.Config{
		Platform:        experiment.LegacyPlatform,
		PollingInterval: experimentsPollingInterval,
		ErrorHandler: func(err error) {
			logrus.WithError(err).Error("Experiments Client encountered an error")
		},
	})
	if err != nil {
		logrus.WithError(err).Panic("Failed to initialize Experiments Client")
	}
	registerDefaultGroup(client)
	return &experimentsClient{
		client: client,
	}
}

func registerDefaultGroup(client experiments.Experiments) {
	client.RegisterDefault(experiment.ChannelType, experimentName, experimentId, TreatmentControl)
}

func (c *experimentsClient) GetTreatment(channelID string) (string, error) {
	treatment, err := c.client.Treat(experiment.ChannelType, experimentName, experimentId, channelID)
	if err != nil {
		return "", err
	}

	switch treatment {
	case TreatmentControl:
		fallthrough
	case TreatmentExperiment:
		return treatment, nil
	default:
		return treatment, fmt.Errorf("Unexpected treatment found: %s", treatment)
	}
}

func (c *experimentsClient) GetExperimentID() string {
	return experimentId
}

func (c *experimentsClient) GetExperimentName() string {
	return experimentName
}

func (c *experimentsClient) GetTreatmentGroups() []string {
	return []string{
		TreatmentControl,
		TreatmentExperiment,
	}
}

func (c *experimentsClient) Close() error {
	return c.client.Close()
}
