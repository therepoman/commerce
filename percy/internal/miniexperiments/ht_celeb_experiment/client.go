package ht_celeb_experiment

import (
	"fmt"
	"time"

	"code.justin.tv/commerce/percy/internal/miniexperiments"
	"code.justin.tv/discovery/experiments"
	"code.justin.tv/discovery/experiments/experiment"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type experimentsClient struct {
	client experiments.Experiments
}

// ht_celeb_experiment under Miniexperiment
// https://minixperiment.di.xarth.tv/admin#/experiment/218acff9-67f7-459f-9f3e-d9b41238e761
const (
	experimentsPollingInterval = 1 * time.Minute

	experimentID      = "218acff9-67f7-459f-9f3e-d9b41238e761"
	experimentName    = "ht_celeb_experiment"
	treatmentControl  = "Control"
	treatmentVariantA = "VariantA"
	treatmentVariantB = "VariantB"
	treatmentVariantC = "VariantC"
)

func NewClient() miniexperiments.Client {
	client, err := experiments.New(experiments.Config{
		DisableSpadeTracking: true,
		Platform:             experiment.LegacyPlatform,
		PollingInterval:      experimentsPollingInterval,
		ErrorHandler: func(err error) {
			logrus.WithError(err).Error("Experiments Client encountered an error")
		},
	})
	if err != nil {
		logrus.WithError(err).Panic("Failed to initialize Experiments Client")
	}
	registerDefaultGroup(client)
	return &experimentsClient{
		client: client,
	}
}

func registerDefaultGroup(client experiments.Experiments) {
	client.RegisterDefault(experiment.ChannelType, experimentName, experimentID, treatmentControl)
}

func (c *experimentsClient) GetExperimentName() string {
	return experimentName
}

func (c *experimentsClient) GetExperimentID() string {
	return experimentID
}

func (c *experimentsClient) GetTreatmentGroups() []string {
	return []string{
		treatmentControl,
		treatmentVariantA,
		treatmentVariantB,
		treatmentVariantC,
	}
}

func (c *experimentsClient) GetTreatment(channelID string) (string, error) {
	treatment, err := c.client.Treat(experiment.ChannelType, experimentName, experimentID, channelID)
	if err != nil {
		return "", err
	}

	switch treatment {
	case treatmentControl,
		treatmentVariantA,
		treatmentVariantB,
		treatmentVariantC:
		return treatment, nil
	default:
		return treatment, errors.New(fmt.Sprintf("Unexpected treatment found: %s", treatment))
	}
}

func (c *experimentsClient) Close() error {
	return c.client.Close()
}
