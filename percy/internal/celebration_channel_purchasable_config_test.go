package percy_test

import (
	"context"
	"testing"

	"code.justin.tv/commerce/gogogadget/pointers"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	. "github.com/smartystreets/goconvey/convey"
)

type updatePurchasableCelebrationConfigTestCase struct {
	name          string
	currentConfig *percy.CelebrationChannelPurchasableConfig
	update        percy.CelebrationChannelPurchasableConfigUpdate
	shouldError   bool
}

func TestCelebrationChannelPurchasableConfig_UpdatePurchasableCelebrationConfig(t *testing.T) {
	Convey("given a celebration engine", t, func() {
		db := internalfakes.FakeCelebrationChannelPurchasableConfigDB{}

		engine := percy.NewCelebrationEngine(percy.CelebrationEngineConfig{
			CelebrationChannelPurchasableConfigDB: &db,
		})

		Convey("when channel is empty, we should return an error", func() {
			updatedConfig, err := engine.UpdatePurchasableCelebrationConfig(context.Background(), "", percy.CelebrationChannelPurchasableConfigUpdate{})
			So(updatedConfig, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		testCases := []updatePurchasableCelebrationConfigTestCase{
			{
				name:          "we should not error when current config is nil and no updates",
				currentConfig: nil,
				update: percy.CelebrationChannelPurchasableConfigUpdate{
					PurchaseConfig: nil,
				},
				shouldError: false,
			},
			{
				name:          "we should not error when current config is nil and no duplicate updates",
				currentConfig: nil,
				update: percy.CelebrationChannelPurchasableConfigUpdate{
					PurchaseConfig: map[percy.CelebrationSize]percy.CelebrationPurchaseConfigUpdate{
						percy.CelebrationSizeSmall: {
							IsDisabled: pointers.BoolP(false),
							OfferID:    pointers.StringP("123"),
						},
						percy.CelebrationSizeLarge: {
							IsDisabled: pointers.BoolP(false),
							OfferID:    pointers.StringP("abc"),
						},
					},
				},
				shouldError: false,
			},
			{
				name:          "we should error when current config is nil and there are duplicate updates",
				currentConfig: nil,
				update: percy.CelebrationChannelPurchasableConfigUpdate{
					PurchaseConfig: map[percy.CelebrationSize]percy.CelebrationPurchaseConfigUpdate{
						percy.CelebrationSizeSmall: {
							IsDisabled: pointers.BoolP(false),
							OfferID:    pointers.StringP("123"),
						},
						percy.CelebrationSizeLarge: {
							IsDisabled: pointers.BoolP(false),
							OfferID:    pointers.StringP("123"),
						},
					},
				},
				shouldError: true,
			},
			{
				name: "we should error when there are duplicates between current and updated config",
				currentConfig: &percy.CelebrationChannelPurchasableConfig{
					ChannelID: "somechannel",
					PurchaseConfig: map[percy.CelebrationSize]percy.CelebrationPurchaseConfig{
						percy.CelebrationSizeSmall: {
							IsDisabled: false,
							OfferID:    "abc",
						},
					},
				},
				update: percy.CelebrationChannelPurchasableConfigUpdate{
					PurchaseConfig: map[percy.CelebrationSize]percy.CelebrationPurchaseConfigUpdate{
						percy.CelebrationSizeLarge: {
							IsDisabled: pointers.BoolP(false),
							OfferID:    pointers.StringP("abc"),
						},
					},
				},
				shouldError: true,
			},
			{
				name: "we should not error when there are duplicates in the same slot",
				currentConfig: &percy.CelebrationChannelPurchasableConfig{
					ChannelID: "somechannel",
					PurchaseConfig: map[percy.CelebrationSize]percy.CelebrationPurchaseConfig{
						percy.CelebrationSizeSmall: {
							IsDisabled: false,
							OfferID:    "abc",
						},
					},
				},
				update: percy.CelebrationChannelPurchasableConfigUpdate{
					PurchaseConfig: map[percy.CelebrationSize]percy.CelebrationPurchaseConfigUpdate{
						percy.CelebrationSizeSmall: {
							IsDisabled: pointers.BoolP(false),
							OfferID:    pointers.StringP("abc"),
						},
					},
				},
				shouldError: false,
			},
			{
				name: "we should not error when the duplicate is in a disabled slot (current config)",
				currentConfig: &percy.CelebrationChannelPurchasableConfig{
					ChannelID: "somechannel",
					PurchaseConfig: map[percy.CelebrationSize]percy.CelebrationPurchaseConfig{
						percy.CelebrationSizeSmall: {
							IsDisabled: true,
							OfferID:    "abc",
						},
					},
				},
				update: percy.CelebrationChannelPurchasableConfigUpdate{
					PurchaseConfig: map[percy.CelebrationSize]percy.CelebrationPurchaseConfigUpdate{
						percy.CelebrationSizeLarge: {
							IsDisabled: pointers.BoolP(false),
							OfferID:    pointers.StringP("abc"),
						},
					},
				},
				shouldError: false,
			},
			{
				name: "we should not error when the duplicate is in a disabled slot (updated config)",
				currentConfig: &percy.CelebrationChannelPurchasableConfig{
					ChannelID: "somechannel",
					PurchaseConfig: map[percy.CelebrationSize]percy.CelebrationPurchaseConfig{
						percy.CelebrationSizeSmall: {
							IsDisabled: false,
							OfferID:    "abc",
						},
					},
				},
				update: percy.CelebrationChannelPurchasableConfigUpdate{
					PurchaseConfig: map[percy.CelebrationSize]percy.CelebrationPurchaseConfigUpdate{
						percy.CelebrationSizeLarge: {
							IsDisabled: pointers.BoolP(true),
							OfferID:    pointers.StringP("abc"),
						},
					},
				},
				shouldError: false,
			},
		}

		//nolint:scopelint
		for _, testCase := range testCases {
			Convey(testCase.name, func() {
				db.GetConfigReturns(testCase.currentConfig, nil)
				db.UpdateConfigReturns(nil, nil)

				_, err := engine.UpdatePurchasableCelebrationConfig(context.Background(), "123", testCase.update)
				So(err != nil, ShouldEqual, testCase.shouldError)
			})
		}
	})
}
