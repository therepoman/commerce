package percy

import (
	"fmt"
	"time"
)

const (
	// Maps to existing frontend logic.
	// TODO This would be much cleaner if we could either:
	//  * send an enum from the backend or
	//  * remove all effect configuration from the frontend, passing all necessary config in the pubsub
	// https://git.xarth.tv/twilight/twilight/blob/master/src/features/channel-celebrations/utils/utils.ts#L211-L221
	celebrationIntensityBucketSize = 100 / 7

	purchasableCelebrationDuration = 5 * time.Second
)

var purchasableCelebrationIntensity = map[CelebrationSize]int64{
	CelebrationSizeSmall: celebrationIntensityBucketSize * 2, // Noice
	CelebrationSizeLarge: celebrationIntensityBucketSize * 6, // OmegaPog
}

func MakePurchasableCelebration(id string, size CelebrationSize) (Celebration, error) {
	intensity, ok := purchasableCelebrationIntensity[size]
	if !ok {
		return Celebration{}, fmt.Errorf("unable to map size %s to Celebration", size)
	}

	return Celebration{
		CelebrationID:        id,
		Enabled:              true,
		EventType:            EventTypeExplicitPurchase,
		EventThreshold:       1,
		CelebrationEffect:    EffectFireworks,
		CelebrationArea:      AreaEverywhere,
		CelebrationIntensity: intensity,
		CelebrationDuration:  purchasableCelebrationDuration,
	}, nil
}
