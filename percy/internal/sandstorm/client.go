package sandstorm

import (
	"errors"
	"sync"
	"time"

	"code.justin.tv/commerce/percy/config"
	"code.justin.tv/systems/sandstorm/manager"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
)

type Client interface {
	GetSecret(secretName string) (*manager.Secret, error)
}

type client struct {
	sandstormManager *manager.Manager
	secretCache      *sync.Map
}

func New(cfg *config.Config) (Client, error) {
	awsConfig := aws.NewConfig().WithRegion(cfg.AWSRegion).WithSTSRegionalEndpoint(endpoints.RegionalSTSEndpoint)

	sess, err := session.NewSession(awsConfig)
	if err != nil {
		return nil, errors.New("failed to create aws session for sandstorm client")
	}

	stsClient := sts.New(sess)
	arp := &stscreds.AssumeRoleProvider{
		Duration:     900 * time.Second,
		ExpiryWindow: 10 * time.Second,
		RoleARN:      cfg.SandstormRole,
		Client:       stsClient,
	}

	creds := credentials.NewCredentials(arp)
	awsConfig.WithCredentials(creds)

	managerCfg := manager.Config{
		AWSConfig: awsConfig,
		TableName: "sandstorm-production",
		KeyID:     "alias/sandstorm-production",
	}

	sandstormManager := manager.New(managerCfg)

	return &client{
		sandstormManager: sandstormManager,
		secretCache:      new(sync.Map),
	}, nil
}

func (c *client) GetSecret(secretName string) (*manager.Secret, error) {
	cachedSecret, isCached := c.secretCache.Load(secretName)
	if isCached {
		secret, isCorrectType := cachedSecret.(*manager.Secret)
		if !isCorrectType {
			return nil, errors.New("incorrect secret type cached")
		}
		return secret, nil
	}

	secret, err := c.sandstormManager.Get(secretName)
	if err != nil {
		return nil, err
	}

	c.secretCache.Store(secretName, secret)
	return secret, nil
}
