package percy_test

import (
	"context"
	"errors"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCelebrationEngine_GetCelebrationUserSettings(t *testing.T) {
	Convey("given a celebrations engine", t, func() {
		userSettingsDB := &internalfakes.FakeCelebrationUserSettingsDB{}

		eng := percy.NewCelebrationEngine(percy.CelebrationEngineConfig{CelebrationUserSettingsDB: userSettingsDB})

		ctx := context.Background()
		userID := "123123123"

		Convey("should succeed", func() {
			var expected percy.CelebrationUserSettings

			Convey("when settings DB returns nil", func() {
				userSettingsDB.GetReturns(nil, nil)
				expected = percy.DefaultCelebrationUserSettings
			})

			Convey("when settings DB returns entry", func() {
				userSettings := tests.ValidCelebrationUserSettings()
				userSettingsDB.GetReturns(&userSettings, nil)
				expected = userSettings
			})

			settings, err := eng.GetCelebrationUserSettings(ctx, userID)
			So(err, ShouldBeNil)
			So(settings, ShouldResemble, expected)
		})

		Convey("should error", func() {
			Convey("when the DB returns error", func() {
				userSettingsDB.GetReturns(nil, errors.New("WALRUS STRIKE"))
			})

			_, err := eng.GetCelebrationUserSettings(ctx, userID)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestCelebrationEngine_UpdateCelebrationUserSettings(t *testing.T) {
	Convey("given a celebrations engine", t, func() {
		userSettingsDB := &internalfakes.FakeCelebrationUserSettingsDB{}

		eng := percy.NewCelebrationEngine(percy.CelebrationEngineConfig{CelebrationUserSettingsDB: userSettingsDB})

		ctx := context.Background()
		userID := "123123123"

		Convey("should succeed", func() {
			Convey("when settings DB returns entry", func() {
				userSettings := tests.ValidCelebrationUserSettings()
				userSettingsDB.UpdateReturns(&userSettings, nil)
			})

			settings, err := eng.UpdateCelebrationUserSettings(ctx, userID, percy.CelebrationUserSettingsUpdate{})
			So(err, ShouldBeNil)
			So(settings, ShouldResemble, tests.ValidCelebrationUserSettings())
		})

		Convey("should error", func() {
			Convey("when the DB returns error", func() {
				userSettingsDB.UpdateReturns(nil, errors.New("WALRUS STRIKE"))
			})

			Convey("when settings DB returns nil", func() {
				userSettingsDB.UpdateReturns(nil, nil)
			})

			_, err := eng.UpdateCelebrationUserSettings(ctx, userID, percy.CelebrationUserSettingsUpdate{})
			So(err, ShouldNotBeNil)
		})
	})
}
