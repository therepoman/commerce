package percy

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"code.justin.tv/commerce/logrus"
	"github.com/google/uuid"
	"github.com/pkg/errors"
)

const (
	AnonymousCheererID = "407665396"
	AnonymousGifterID  = "274598607"
)

type User struct {
	Id              string `json:"id"`
	Login           string `json:"login,omitempty"`
	DisplayName     string `json:"display_name,omitempty"`
	ProfileImageURL string `json:"profile_image_url,omitempty"`
}

func NewUser(id string) User {
	if len(id) == 0 {
		id = AnonymousCheererID
	}
	return User{
		Id: id,
	}
}

func NewAnonymousCheerer() User {
	return User{
		Id: AnonymousCheererID,
	}
}

func NewAnonymousGifter() User {
	return User{
		Id: AnonymousGifterID,
	}
}

func (user *User) ID() string {
	return user.Id
}

func (user *User) IsAnonymous() bool {
	return user.Id == AnonymousCheererID || user.Id == AnonymousGifterID
}

type ParticipationSource string

const (
	UnknownSource = ParticipationSource("UNKNOWN")
	SubsSource    = ParticipationSource("SUBS")
	BitsSource    = ParticipationSource("BITS")
	AdminSource   = ParticipationSource("ADMIN")
)

type ParticipationAction string

const (
	UnknownAction        = ParticipationAction("UNKNOWN")
	CheerAction          = ParticipationAction("CHEER")
	ExtensionAction      = ParticipationAction("EXTENSION")
	PollAction           = ParticipationAction("POLL")
	Tier1SubAction       = ParticipationAction("TIER_1_SUB")
	Tier2SubAction       = ParticipationAction("TIER_2_SUB")
	Tier3SubAction       = ParticipationAction("TIER_3_SUB")
	Tier1GiftedSubAction = ParticipationAction("TIER_1_GIFTED_SUB")
	Tier2GiftedSubAction = ParticipationAction("TIER_2_GIFTED_SUB")
	Tier3GiftedSubAction = ParticipationAction("TIER_3_GIFTED_SUB")
	AdminAction          = ParticipationAction("ADMIN")
)

type ParticipationIdentifier struct {
	Source ParticipationSource
	Action ParticipationAction
}

type ParticipationQueryParams struct {
	StartTime time.Time
	EndTime   time.Time
}

// Participation represents a single participation event in a channel
type Participation struct {
	ChannelID string    `json:"channel_id"`
	Timestamp time.Time `json:"timestamp"`
	User      User      `json:"user"`
	Quantity  int       `json:"quantity"`
	EventID   string    `json:"event_id"`
	ParticipationIdentifier
}

type ParticipationTotals map[ParticipationIdentifier]int

func (totals *ParticipationTotals) Sources() []ParticipationSource {
	sources := make(map[ParticipationSource]bool)
	for identifier := range *totals {
		sources[identifier.Source] = true
	}

	uniqueSources := make([]ParticipationSource, 0)
	for source := range sources {
		uniqueSources = append(uniqueSources, source)
	}

	return uniqueSources
}

func (totals *ParticipationTotals) ParticipationQuantitiesBySource(source ParticipationSource) int {
	total := 0
	for identifier, quantity := range *totals {
		if identifier.Source == source {
			total += quantity
		}
	}

	return total
}

func (totals *ParticipationTotals) ParticipationPoints(config HypeTrainConfig) int {
	total := 0
	for pi, quantity := range *totals {
		total += quantity * int(config.ParticipationConversionRates[pi])
	}

	return total
}

func (p ParticipationTotals) MarshalJSON() ([]byte, error) {
	newMap := make(map[string]int)

	for key, entry := range p {
		newKey := string(key.Source) + "." + string(key.Action)
		newMap[newKey] = entry
	}

	return json.Marshal(newMap)
}

func (p *ParticipationTotals) UnmarshalJSON(data []byte) error {
	convMap := make(map[string]int)

	if err := json.Unmarshal(data, &convMap); err != nil {
		return err
	}

	newMap := make(ParticipationTotals)

	for key, entry := range convMap {
		sourceAndAction := strings.Split(key, ".")
		if len(sourceAndAction) != 2 {
			return errors.New("key needs to be SOURCE.ACTION")
		}
		newKey := ParticipationIdentifier{
			Source: ParticipationSource(sourceAndAction[0]),
			Action: ParticipationAction(sourceAndAction[1]),
		}
		newMap[newKey] = entry
	}

	*p = newMap

	return nil
}

// Participant represents a user's summed participation events for a given hype train instance.
type Participant struct {
	ChannelID           string              `json:"channel_id"`
	HypeTrainID         string              `json:"id"`
	User                User                `json:"user"`
	ParticipationTotals ParticipationTotals `json:"participation_totals"`
	Rewards             []Reward            `json:"rewards"`
}

func NewParticipant(hypeTrainID string, participation Participation) Participant {
	participant := Participant{
		ChannelID:   participation.ChannelID,
		User:        participation.User,
		HypeTrainID: hypeTrainID,
	}

	participant.AddParticipation(participation.ParticipationIdentifier, participation.Quantity)
	return participant
}

func (p *Participant) AddParticipation(identifier ParticipationIdentifier, quantity int) {
	if p.ParticipationTotals == nil {
		p.ParticipationTotals = make(map[ParticipationIdentifier]int)
	}
	p.ParticipationTotals[identifier] += quantity
}

func (p *Participant) ParticipationQuantity(identifier ParticipationIdentifier) int {
	if p.ParticipationTotals == nil {
		p.ParticipationTotals = make(map[ParticipationIdentifier]int)
	}

	return p.ParticipationTotals[identifier]
}

// ParticipationQuantities returns the participations totals (aggregated by participation source and type).
func (p *Participant) Participations() []Participation {
	participations := make([]Participation, 0)
	for identifier, total := range p.ParticipationTotals {
		participations = append(participations, Participation{
			ChannelID:               p.ChannelID,
			User:                    p.User,
			ParticipationIdentifier: identifier,
			Quantity:                total,
			Timestamp:               time.Now().UTC(),
		})
	}

	return participations
}

//go:generate counterfeiter . ParticipationDB

type ParticipationDB interface {
	// RecordChannelParticipation records a new channel participation.
	// E.g. Used to record channel participations when no hype train is ongoing.
	RecordChannelParticipation(ctx context.Context, participation Participation) error

	// GetParticipation returns all participations for a channel within the given time period.
	// E.g. To get all participation events during the kickoff period.
	GetChannelParticipations(ctx context.Context, channelID string, params ParticipationQueryParams) ([]Participation, error)
}

//go:generate counterfeiter . ParticipantDB

type ParticipantDB interface {
	// PutHypeTrainParticipant saves all participants
	// E.g. Load all kickoff participants when a new hype train starts.
	PutHypeTrainParticipants(ctx context.Context, participants ...Participant) error

	// RecordHypeTrainParticipation updates the participant total with the new participation event.
	// E.g. Record participation events for participants that may or may not already exist in the DB.
	RecordHypeTrainParticipation(ctx context.Context, hypeTrainID string, participation Participation) (Participant, error)

	// GetHypeTrainParticipants returns a single participant for a given hype train instance, returns nil if the given user ID isn't found.
	GetHypeTrainParticipant(ctx context.Context, hypeTrainID, userID string) (*Participant, error)

	// GetHypeTrainParticipants returns all participants for a given hype train instance.
	// E.g. Query for all participants to reward participation rewards.
	GetHypeTrainParticipants(ctx context.Context, hypeTrainID string) ([]Participant, error)
}

func (eng *engine) RecordParticipation(ctx context.Context, participation Participation) error {
	if participation.ChannelID == participation.User.Id {
		return nil
	}

	if participation.Quantity == 0 {
		return nil
	}

	hypeTrain, _, err := eng.GetOngoingHypeTrain(ctx, participation.ChannelID)
	if err != nil {
		return errors.Wrap(err, "failed to retrieve the current ongoing hype train")
	}

	if hypeTrain != nil {
		return eng.handleParticipationForOngoingHypeTrain(ctx, *hypeTrain, participation)
	} else {
		return eng.handleParticipation(ctx, participation)
	}
}

// Handles a new participation when there is an ongoing hype train in the channel
func (eng *engine) handleParticipationForOngoingHypeTrain(ctx context.Context, hypeTrain HypeTrain, participation Participation) error {
	if participation.Timestamp.Before(hypeTrain.StartedAt) ||
		participation.Timestamp.After(hypeTrain.ExpiresAt) {
		return nil
	}

	if err := eng.ParticipationDB.RecordChannelParticipation(ctx, participation); err != nil {
		return errors.Wrap(err, "failed to record new channel participation")
	}

	if _, err := eng.ParticipantDB.RecordHypeTrainParticipation(ctx, hypeTrain.ID, participation); err != nil {
		return errors.Wrap(err, "failed to update participant")
	}

	updatedHypeTrain, err := eng.HypeTrainDB.AddParticipation(ctx, hypeTrain.ChannelID, hypeTrain.ID, participation)
	if err != nil {
		return errors.Wrap(err, "failed to update hype train participation total")
	}

	go func() {
		ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
		defer cancel()

		if err := eng.Publisher.PostHypeTrainProgression(ctx, participation, updatedHypeTrain.LevelProgress()); err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{
				"channelID":   hypeTrain.ChannelID,
				"hypeTrainID": hypeTrain.ID,
			}).Error("Failed to publish participation message to pubsub")
		}
	}()

	// Send data science event for participation
	go eng.SendHypeTrainParticipationEvent(hypeTrain.ID, participation)

	// Send data science event for special callouts
	if participation.Quantity > updatedHypeTrain.Config.NotificationThresholds[participation.ParticipationIdentifier] {
		go eng.SendHypeTrainCalloutEvent(updatedHypeTrain, participation)
	}

	// check if this participation event levels up the Hype Train
	if updatedHypeTrain.CompletedLevel() > hypeTrain.CompletedLevel() {
		if err := eng.handleHypeTrainLevelUp(ctx, updatedHypeTrain, hypeTrain); err != nil {
			return err
		}
	}

	go func() {
		ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
		defer cancel()
		err := eng.Leaderboard.Publish(ctx, LeaderboardID{
			ChannelID:   hypeTrain.ChannelID,
			HypeTrainID: hypeTrain.ID,
			Source:      participation.Source,
		}, participation)
		if err != nil {
			logrus.WithError(err).WithFields(
				logrus.Fields{
					"channelID":   hypeTrain.ChannelID,
					"hypeTrainID": hypeTrain.ID,
				}).Error("Failed to publish participation to leaderboard")
		}
	}()

	return err
}

func (eng *engine) handleHypeTrainLevelUp(ctx context.Context, updatedHypeTrain HypeTrain, prevHypeTrain HypeTrain) error {
	levelSettings := updatedHypeTrain.Config.LevelSettings()
	finalLevel := levelSettings[len(levelSettings)-1]

	// do not extend the hype train's timer if the final level is completed
	if updatedHypeTrain.CompletedLevel() < finalLevel.Level {
		extendTo := time.Now().UTC().Add(updatedHypeTrain.Config.LevelDuration)
		_, err := eng.HypeTrainDB.ExtendHypeTrain(ctx, updatedHypeTrain.ChannelID, updatedHypeTrain.ID, extendTo)
		if err != nil {
			return errors.Wrap(err, "failed to extend hype train expiration time after completing the current level")
		}
		updatedHypeTrain.ExpiresAt = extendTo
		if err := eng.ScheduleHypeTrainEnding(ctx, updatedHypeTrain); err != nil {
			return err
		}
	}

	// Send level up Pubsub to client
	go func() {
		ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
		defer cancel()

		if err := eng.Publisher.LevelUpHypeTrain(ctx, updatedHypeTrain.ChannelID, updatedHypeTrain.ExpiresAt, updatedHypeTrain.LevelProgress()); err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{
				"channelID":   updatedHypeTrain.ChannelID,
				"hypeTrainID": updatedHypeTrain.ID,
			}).Error("Failed to publish levelup message to pubsub")
		}

		logrus.WithFields(logrus.Fields{
			"channelID":   updatedHypeTrain.ChannelID,
			"hypeTrainID": updatedHypeTrain.ID,
		}).Infof("Hype train level %d completed", updatedHypeTrain.CompletedLevel())
	}()

	// Publish level up event to datascience, including levels that were skipped
	for level := prevHypeTrain.Level(); level < updatedHypeTrain.Level(); level++ {
		go eng.SendHypeTrainProgressEvents(updatedHypeTrain, fmt.Sprintf("LEVEL %d", level))
	}

	return nil
}

// Handles a new participation when there is no ongoing hype train in the channel
// Saves the participation to DynamoDB as well as Redis
// Attempts to kickoff a new hype train with the new event.
func (eng *engine) handleParticipation(ctx context.Context, participation Participation) error {
	// check if a hype train is allowed to start in the channel
	canStart, err := eng.CanStartHypeTrain(ctx, participation.ChannelID)
	if err != nil {
		return errors.Wrap(err, "failed to check whether a new hype train can be started in this channel")
	}

	if !canStart {
		return nil
	}

	config, err := eng.GetHypeTrainConfig(ctx, participation.ChannelID)
	if err != nil {
		return errors.Wrap(err, "failed to retrieve channel's hype train config")
	}

	// Write participation event to Dynamo Participation table & redis (if hype train is ongoing)
	if err := eng.ParticipationDB.RecordChannelParticipation(ctx, participation); err != nil {
		return errors.Wrap(err, "failed to record new channel participation")
	}

	// no-op if the participation isn't big enough to be counted as a kick off event.
	if !isEligibleForKickoff(config, participation) {
		return nil
	}

	// Get personalized settings for difficulty and numOfEvents from Personalization
	if config.UsePersonalizedSettings && eng.Personalization != nil {
		personalizedConfig, err := eng.Personalization.GetHypeTrainConfigOverrides(ctx, config.ChannelID)
		if err != nil {
			logrus.WithField("channelID", config.ChannelID).WithError(err).Warn("failed to get personalized settings for hype train")
		} else if !personalizedConfig.ShouldUseExistingConfig {
			config.Difficulty = personalizedConfig.Difficulty
			config.KickoffConfig.NumOfEvents = personalizedConfig.NumOfEventsToKickoff
		}
	}

	now := time.Now()
	kickoffParticipations, err := eng.ParticipationDB.GetChannelParticipations(ctx, config.ChannelID, ParticipationQueryParams{
		StartTime: now.Add(-1 * config.KickoffConfig.Duration),
		EndTime:   now,
	})
	if err != nil {
		return errors.Wrap(err, "failed to get kickoff participations for a new hype train")
	}

	// Get users theme color in the case when the setting is enabled.
	if config.UseCreatorColor && eng.UserProfileDB != nil {
		userProfile, err := eng.UserProfileDB.GetProfileByID(ctx, config.ChannelID)
		if err == nil && userProfile.PrimaryColorHex != nil {
			config.PrimaryHexColor = *userProfile.PrimaryColorHex
		}
	}

	approachingRecord, err := eng.GetHypeTrainApproaching(ctx, config.ChannelID)
	if err != nil {
		// we can ignore these errors for now, will put out a follow up PR to engage directly on error
		logrus.WithError(err).Error("Error attempting to pull Hype Train Approaching entry")
	}

	canKickoff, nearKickoff := canKickoffHypeTrain(config, kickoffParticipations)

	if nearKickoff {
		err = eng.kickOffHypeTrainApproaching(ctx, config, approachingRecord, participation.User.Id, kickoffParticipations, now)
		if err != nil {
			return errors.Wrap(err, "failed to kickoff a new hype train approach")
		}
	} else if canKickoff {
		// Source the Hype Train ID from the Approaching Record if possible
		hypeTrainID := uuid.New().String()
		if approachingRecord != nil && len(approachingRecord.ID) != 0 {
			hypeTrainID = approachingRecord.ID
		}
		// try to kickoff a hype train with the new participation event
		_, err = eng.kickOffHypeTrain(ctx, config, participation, kickoffParticipations, hypeTrainID)
		if err != nil {
			return errors.Wrap(err, "failed to kickoff a new hype train")
		}

	}

	return nil
}

func (eng *engine) GetParticipations(ctx context.Context, channelID string, params ParticipationQueryParams) ([]Participation, error) {
	return eng.ParticipationDB.GetChannelParticipations(ctx, channelID, params)
}
