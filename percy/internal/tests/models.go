package tests

import (
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/rpc"
	rpc_celebi "code.justin.tv/commerce/percy/rpc/celebrations"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/schema/pkg/bits_use"
	"code.justin.tv/eventbus/schema/pkg/cheer"
	"code.justin.tv/eventbus/schema/pkg/emoticon"
	"code.justin.tv/eventbus/schema/pkg/user_gift_subscription_user"
	"code.justin.tv/eventbus/schema/pkg/user_subscribe_user_notice"
	uuid2 "github.com/gofrs/uuid"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/wrappers"
	"github.com/google/uuid"
	"google.golang.org/protobuf/types/known/timestamppb"
)

const (
	ChannelID         = "1234567890"
	UserID            = "433099049"
	HypeTrainID       = "1234"
	BitsTransactionId = "1234567"
	OfferID           = "foo"
)

var (
	now                = time.Now()
	startedAt          = now.AddDate(0, 0, 0).Add(time.Minute * -3)
	expiresAt          = now.AddDate(0, 0, 0).Add(time.Minute * 5)
	updatedAt          = now.AddDate(0, 0, 0).Add(time.Minute * 5)
	endedAt            = now.AddDate(0, 0, 0).Add(time.Minute * 5)
	pTypesStartedAt, _ = ptypes.TimestampProto(startedAt)
	pTypesExpiresAt, _ = ptypes.TimestampProto(expiresAt)
	pTypesUpdatedAt, _ = ptypes.TimestampProto(updatedAt)
	pTypesEndedAt, _   = ptypes.TimestampProto(endedAt)

	level1Rewards = []percy.Reward{
		{
			Type:    percy.EmoteReward,
			ID:      "1A",
			GroupID: "1111",
		},
		{
			Type:    percy.EmoteReward,
			ID:      "1B",
			GroupID: "1111",
		},
		{
			Type:    percy.EmoteReward,
			ID:      "1C",
			GroupID: "1111",
		},
	}
	level2Rewards = []percy.Reward{
		{
			Type:    percy.EmoteReward,
			ID:      "2A",
			GroupID: "2222",
		},
		{
			Type:    percy.EmoteReward,
			ID:      "2B",
			GroupID: "2222",
		},
		{
			Type:    percy.EmoteReward,
			ID:      "2C",
			GroupID: "2222",
		},
	}
	level3Rewards = []percy.Reward{
		{
			Type:    percy.EmoteReward,
			ID:      "3A",
			GroupID: "3333",
		},
		{
			Type:    percy.EmoteReward,
			ID:      "3B",
			GroupID: "3333",
		},
		{
			Type:    percy.EmoteReward,
			ID:      "3C",
			GroupID: "3333",
		},
	}
)

func ValidHypeTrainConfig() percy.HypeTrainConfig {
	return percy.HypeTrainConfig{
		ChannelID:          ChannelID,
		CalloutEmoteID:     "1234567",
		Enabled:            true,
		IsWhitelisted:      true,
		HasConductorBadges: false,
		KickoffConfig: percy.KickoffConfig{
			NumOfEvents: 3,
			MinPoints:   100,
			Duration:    5 * time.Minute,
		},
		CooldownDuration: 2 * time.Hour,
		LevelDuration:    5 * time.Minute,
		Difficulty:       percy.DifficultyMedium,
		RewardEndDate:    nil,
		LevelSettingsByDifficulty: map[percy.Difficulty][]percy.LevelSetting{
			percy.DifficultyEasy: {
				{Level: 1, Goal: percy.ParticipationPoint(500), Rewards: level1Rewards},
				{Level: 2, Goal: percy.ParticipationPoint(1000), Rewards: level2Rewards},
				{Level: 3, Goal: percy.ParticipationPoint(1500), Rewards: level3Rewards},
			},
			percy.DifficultyMedium: {
				{Level: 1, Goal: percy.ParticipationPoint(1000), Rewards: level1Rewards},
				{Level: 2, Goal: percy.ParticipationPoint(2000), Rewards: level2Rewards},
				{Level: 3, Goal: percy.ParticipationPoint(3000), Rewards: level3Rewards},
			},
			percy.DifficultyHard: {
				{Level: 1, Goal: percy.ParticipationPoint(5000), Rewards: level1Rewards},
				{Level: 2, Goal: percy.ParticipationPoint(10000), Rewards: level2Rewards},
				{Level: 3, Goal: percy.ParticipationPoint(15000), Rewards: level3Rewards},
			},
		},
		ParticipationConversionRates: map[percy.ParticipationIdentifier]percy.ParticipationPoint{
			{Source: percy.BitsSource, Action: percy.CheerAction}:          percy.ParticipationPoint(1),
			{Source: percy.BitsSource, Action: percy.ExtensionAction}:      percy.ParticipationPoint(1),
			{Source: percy.BitsSource, Action: percy.PollAction}:           percy.ParticipationPoint(1),
			{Source: percy.SubsSource, Action: percy.Tier1SubAction}:       percy.ParticipationPoint(500),
			{Source: percy.SubsSource, Action: percy.Tier2SubAction}:       percy.ParticipationPoint(1000),
			{Source: percy.SubsSource, Action: percy.Tier3SubAction}:       percy.ParticipationPoint(2500),
			{Source: percy.SubsSource, Action: percy.Tier1GiftedSubAction}: percy.ParticipationPoint(500),
			{Source: percy.SubsSource, Action: percy.Tier2GiftedSubAction}: percy.ParticipationPoint(1000),
			{Source: percy.SubsSource, Action: percy.Tier3GiftedSubAction}: percy.ParticipationPoint(2500),
		},
		ConductorRewards: make(percy.ConductorRewards),
		NotificationThresholds: map[percy.ParticipationIdentifier]int{
			{Source: percy.BitsSource, Action: percy.CheerAction}:          1000,
			{Source: percy.BitsSource, Action: percy.ExtensionAction}:      1000,
			{Source: percy.BitsSource, Action: percy.PollAction}:           1000,
			{Source: percy.SubsSource, Action: percy.Tier1SubAction}:       10,
			{Source: percy.SubsSource, Action: percy.Tier2SubAction}:       5,
			{Source: percy.SubsSource, Action: percy.Tier3SubAction}:       1,
			{Source: percy.SubsSource, Action: percy.Tier1GiftedSubAction}: 10,
			{Source: percy.SubsSource, Action: percy.Tier2GiftedSubAction}: 5,
			{Source: percy.SubsSource, Action: percy.Tier3GiftedSubAction}: 1,
		},
	}
}

func InvalidHypeTrainConfig() percy.HypeTrainConfig {
	return percy.HypeTrainConfig{
		ChannelID: "",
		Enabled:   true,

		KickoffConfig: percy.KickoffConfig{
			NumOfEvents: 0,
			MinPoints:   0,
		},
	}
}

func ValidTwirpHypeTrainConfig() rpc.HypeTrainConfig {
	return rpc.HypeTrainConfig{
		Enabled: true,
		KickoffConfig: &rpc.KickoffConfig{
			NumOfEvents:     2,
			MinPoints:       100,
			DurationSeconds: 300,
		},
		Difficulty:              rpc.Difficulty_MEDIUM,
		CooldownDurationMinutes: 120,
		LevelDurationSeconds:    300,
		DifficultySettings: []*rpc.DifficultySetting{
			{
				Difficulty: rpc.Difficulty_EASY,
				LevelSettings: []*rpc.LevelSetting{
					{Level: 1, Goal: int64(500), Rewards: []*rpc.Reward{}},
					{Level: 2, Goal: int64(1000), Rewards: []*rpc.Reward{}},
					{Level: 3, Goal: int64(1500), Rewards: []*rpc.Reward{}},
				},
			},
			{
				Difficulty: rpc.Difficulty_MEDIUM,
				LevelSettings: []*rpc.LevelSetting{
					{Level: 1, Goal: int64(1000), Rewards: []*rpc.Reward{}},
					{Level: 2, Goal: int64(2000), Rewards: []*rpc.Reward{}},
					{Level: 3, Goal: int64(3000), Rewards: []*rpc.Reward{}},
				},
			},
			{
				Difficulty: rpc.Difficulty_HARD,
				LevelSettings: []*rpc.LevelSetting{
					{Level: 1, Goal: int64(5000), Rewards: []*rpc.Reward{}},
					{Level: 2, Goal: int64(10000), Rewards: []*rpc.Reward{}},
					{Level: 3, Goal: int64(15000), Rewards: []*rpc.Reward{}},
				},
			},
		},
		ParticipationConversionRates: []*rpc.ParticipationConversionRate{
			{Source: rpc.ParticipationSource_BITS, Action: rpc.ParticipationAction_CHEER, Rate: int64(1)},
			{Source: rpc.ParticipationSource_BITS, Action: rpc.ParticipationAction_EXTENSION, Rate: int64(1)},
			{Source: rpc.ParticipationSource_BITS, Action: rpc.ParticipationAction_POLL, Rate: int64(1)},
			{Source: rpc.ParticipationSource_SUBS, Action: rpc.ParticipationAction_TIER_1_SUB, Rate: int64(500)},
			{Source: rpc.ParticipationSource_SUBS, Action: rpc.ParticipationAction_TIER_2_SUB, Rate: int64(1000)},
			{Source: rpc.ParticipationSource_SUBS, Action: rpc.ParticipationAction_TIER_3_SUB, Rate: int64(2500)},
			{Source: rpc.ParticipationSource_SUBS, Action: rpc.ParticipationAction_TIER_1_GIFTED_SUB, Rate: int64(500)},
			{Source: rpc.ParticipationSource_SUBS, Action: rpc.ParticipationAction_TIER_2_GIFTED_SUB, Rate: int64(1000)},
			{Source: rpc.ParticipationSource_SUBS, Action: rpc.ParticipationAction_TIER_3_GIFTED_SUB, Rate: int64(2500)},
		},
		NotificationThresholds: []*rpc.NotificationThreshold{},
		ConductorRewards:       []*rpc.ConductorReward{},
	}
}

func ValidTwirpConductors() []*rpc.Conductor {
	return []*rpc.Conductor{
		{
			Source: rpc.ParticipationSource_BITS,
			UserId: UserID,
		},
		{
			Source: rpc.ParticipationSource_SUBS,
			UserId: UserID,
		},
	}
}

func ValidConductorRewards() percy.ConductorRewards {
	return percy.ConductorRewards{
		percy.SubsSource: {
			percy.FormerConductor:  ValidFormerConductorReward(),
			percy.CurrentConductor: ValidCurrentConductorReward(),
		},
		percy.BitsSource: {
			percy.FormerConductor:  ValidFormerConductorReward(),
			percy.CurrentConductor: ValidCurrentConductorReward(),
		},
	}
}

func ValidCurrentConductorReward() []percy.Reward {
	return []percy.Reward{
		{
			ID:   "1",
			Type: percy.BadgeReward,
		},
	}
}

func ValidFormerConductorReward() []percy.Reward {
	return []percy.Reward{
		{
			ID:   "2",
			Type: percy.BadgeReward,
		},
	}
}

func ValidConductors() map[percy.ParticipationSource]percy.Conductor {
	return map[percy.ParticipationSource]percy.Conductor{
		percy.BitsSource: {
			Source: percy.BitsSource,
			User:   percy.NewUser(UserID),
			Participations: map[percy.ParticipationIdentifier]int{
				{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				}: 100,
				{
					Source: percy.BitsSource,
					Action: percy.ExtensionAction,
				}: 30,
				{
					Source: percy.BitsSource,
					Action: percy.PollAction,
				}: 20,
			},
		},
		percy.SubsSource: {
			Source: percy.SubsSource,
			User:   percy.NewUser(UserID),
			Participations: map[percy.ParticipationIdentifier]int{
				{
					Source: percy.SubsSource,
					Action: percy.Tier1GiftedSubAction,
				}: 1,
				{
					Source: percy.SubsSource,
					Action: percy.Tier2GiftedSubAction,
				}: 1,
				{
					Source: percy.SubsSource,
					Action: percy.Tier3GiftedSubAction,
				}: 1,
			},
		},
	}
}

func ValidTwirpHypeTrain() rpc.HypeTrain {
	config := ValidTwirpHypeTrainConfig()
	conductors := ValidTwirpConductors()
	progress := rpc.Progress{
		Progression: 100,
		Setting:     config.DifficultySettings[0].LevelSettings[0],
	}

	return rpc.HypeTrain{
		ChannelId:     ChannelID,
		Id:            HypeTrainID,
		StartedAt:     pTypesStartedAt,
		ExpiresAt:     pTypesExpiresAt,
		UpdatedAt:     pTypesUpdatedAt,
		EndedAt:       pTypesEndedAt,
		EndingReason:  rpc.EndingReason_COMPLETED,
		Config:        &config,
		Conductors:    conductors,
		LevelProgress: &progress,
	}
}

func ValidOngoingHypeTrain() percy.HypeTrain {
	return percy.NewHypeTrain(ValidHypeTrainConfig(), uuid.New().String())
}

func ValidHypeTrain() percy.HypeTrain {
	endingReason := percy.EndingReasonCompleted
	return percy.HypeTrain{
		ChannelID:    ChannelID,
		ID:           HypeTrainID,
		StartedAt:    startedAt,
		ExpiresAt:    expiresAt,
		UpdatedAt:    updatedAt,
		EndedAt:      &endedAt,
		EndingReason: &endingReason,
		Config:       ValidHypeTrainConfig(),
		Conductors:   ValidConductors(),
		Participations: map[percy.ParticipationIdentifier]int{
			{
				Source: percy.BitsSource,
				Action: percy.CheerAction,
			}: 100,
			{
				Source: percy.BitsSource,
				Action: percy.ExtensionAction,
			}: 30,
			{
				Source: percy.BitsSource,
				Action: percy.PollAction,
			}: 20,
			{
				Source: percy.SubsSource,
				Action: percy.Tier1GiftedSubAction,
			}: 1,
			{
				Source: percy.SubsSource,
				Action: percy.Tier2GiftedSubAction,
			}: 1,
			{
				Source: percy.SubsSource,
				Action: percy.Tier3GiftedSubAction,
			}: 1,
		},
	}
}

func ValidExpiredHypeTrain() percy.HypeTrain {
	endingReason := percy.EndingReasonExpired
	return percy.HypeTrain{
		ChannelID:    ChannelID,
		ID:           HypeTrainID,
		StartedAt:    startedAt,
		ExpiresAt:    expiresAt,
		UpdatedAt:    updatedAt,
		EndedAt:      &endedAt,
		EndingReason: &endingReason,
		Config:       ValidHypeTrainConfig(),
		Conductors:   ValidConductors(),
	}
}

func InvalidHypeTrain() percy.HypeTrain {
	return percy.HypeTrain{
		ChannelID:    "",
		ID:           "",
		EndingReason: nil,
		Config:       InvalidHypeTrainConfig(),
		Conductors:   map[percy.ParticipationSource]percy.Conductor{},
	}
}

func ValidParticipant() percy.Participant {
	return percy.Participant{
		ChannelID:   ChannelID,
		HypeTrainID: HypeTrainID,
		ParticipationTotals: map[percy.ParticipationIdentifier]int{
			{Source: percy.BitsSource, Action: percy.CheerAction}:          100,
			{Source: percy.SubsSource, Action: percy.Tier1GiftedSubAction}: 10,
		},
		User: percy.NewUser(UserID),
	}
}

func ValidParticipation() percy.Participation {
	return percy.Participation{
		ChannelID: ChannelID,
		Timestamp: startedAt,
		User:      percy.NewUser(UserID),
		Quantity:  1,
		ParticipationIdentifier: percy.ParticipationIdentifier{
			Source: percy.SubsSource,
			Action: percy.Tier1SubAction,
		},
	}
}

func InvalidParticipation() percy.Participation {
	return percy.Participation{
		ChannelID: "",
		User:      percy.NewUser(""),
		Quantity:  -1,
		ParticipationIdentifier: percy.ParticipationIdentifier{
			Source: percy.UnknownSource,
			Action: percy.UnknownAction,
		},
	}
}

func ValidParticipants() []percy.Participant {
	return []percy.Participant{
		{
			ChannelID:   ValidHypeTrain().ChannelID,
			User:        percy.NewUser("1"),
			HypeTrainID: ValidHypeTrain().ID,
			ParticipationTotals: map[percy.ParticipationIdentifier]int{
				{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				}: 100,
				{
					Source: percy.SubsSource,
					Action: percy.Tier1SubAction,
				}: 1,
			},
		},
		{
			ChannelID:   ValidHypeTrain().ChannelID,
			User:        percy.NewUser("2"),
			HypeTrainID: ValidHypeTrain().ID,
			ParticipationTotals: map[percy.ParticipationIdentifier]int{
				{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				}: 100,
				{
					Source: percy.SubsSource,
					Action: percy.Tier1SubAction,
				}: 1,
			},
		},
	}
}

func ValidEventHeader() *eventbus.Header {
	return &eventbus.Header{
		MessageID: uuid2.UUID{},
		CreatedAt: time.Now(),
		EventType: "some_event_type",
	}
}

func ValidEmoticonDeleteEvent() *emoticon.Delete {
	return &emoticon.Delete{
		Emoticon: &emoticon.Emoticon{
			Id:      "walrusID",
			OwnerId: ChannelID,
			GroupId: "walrusGroup",
			Token:   "walrusStrike",
		},
		State: emoticon.State_STATE_ARCHIVED,
	}
}

func ValidBitsUseEvent() *bits_use.Create {
	return &bits_use.BitsUseCreate{
		BitsTransactionId: BitsTransactionId,
		ToUserId:          UserID,
		FromUserId:        "111111111",
		BitsAmount:        -100,
		From: &bits_use.BitsUseCreate_Extension{
			Extension: &bits_use.Extension{
				Id:        "12345",
				ProductId: "67890",
			},
		},
	}
}

func InvalidBitsUseEvent() *bits_use.Create {
	return &bits_use.BitsUseCreate{
		BitsTransactionId: "",
		ToUserId:          "",
		FromUserId:        "",
		BitsAmount:        100,
		From: &bits_use.BitsUseCreate_Extension{
			Extension: &bits_use.Extension{
				Id:        "",
				ProductId: "",
			},
		},
	}
}

func InDevelopmentBitsUseEvent() *bits_use.Create {
	return &bits_use.BitsUseCreate{
		BitsTransactionId: BitsTransactionId,
		ToUserId:          UserID,
		FromUserId:        "111111111",
		BitsAmount:        -100,
		From: &bits_use.BitsUseCreate_Extension{
			Extension: &bits_use.Extension{
				Id:            "12345",
				ProductId:     "67890",
				InDevelopment: true,
			},
		},
	}
}

func ValidCheerCreateEvent() *cheer.Create {
	return &cheer.CheerCreate{
		BitsTransactionId: BitsTransactionId,
		ToUserId:          ChannelID,
		From: &cheer.CheerCreate_User{
			User: &cheer.User{
				Id: UserID,
			},
		},
		PublicMessage: "nic needs a slack photo",
		Bits:          -100,
	}
}

func InvalidCheerCreateEvent() *cheer.Create {
	return &cheer.CheerCreate{
		BitsTransactionId: "",
		ToUserId:          "",
		From:              nil,
		PublicMessage:     "",
		Bits:              100,
	}
}

func ValidUserGiftSubscriptionEvent() *user_gift_subscription_user.Create {
	return &user_gift_subscription_user.UserGiftSubscriptionUserCreate{
		FromUserId: UserID,
		ToUserId:   ChannelID,
		Quantity:   5,
		OriginId:   "1234567",
		ProductId:  "8901234",
		Tier: &user_gift_subscription_user.Tier{
			Value: &user_gift_subscription_user.Tier_Numeral{
				Numeral: user_gift_subscription_user.SubTier_SUB_TIER_1,
			},
		},
		IsAnonymous:                false,
		SubscriptionNumberOfMonths: 1,
	}
}

func InvalidUserGiftSubscriptionEvent() *user_gift_subscription_user.Create {
	return &user_gift_subscription_user.UserGiftSubscriptionUserCreate{
		FromUserId:  UserID,
		ToUserId:    ChannelID,
		Quantity:    5,
		OriginId:    "1234567",
		ProductId:   "8901234",
		IsAnonymous: false,
	}
}

func ValidUserSubscriptionUserNoticeEvent() *user_subscribe_user_notice.Create {
	return &user_subscribe_user_notice.UserSubscribeUserNoticeCreate{
		FromUserId: UserID,
		ToUserId:   ChannelID,
		SubscriptionInfo: &user_subscribe_user_notice.Subscription{
			Id:              "1234",
			ProductId:       "56678",
			OriginId:        "901234",
			BenefitStartsAt: pTypesStartedAt,
			BenefitEndsAt:   pTypesEndedAt,
			UpdatedAt:       pTypesStartedAt,
			CreatedAt:       pTypesStartedAt,
			CreatedBy:       UserID,
		},
		UserNotice: &user_subscribe_user_notice.UserSubscribeUserNoticeCreate_Subscription{
			Subscription: &user_subscribe_user_notice.SubscriptionUserNotice{
				CumulativeTenureMonths: 2,
				CustomMessage:          "some_mesage",
				Tier: &user_subscribe_user_notice.Tier{
					Value: &user_subscribe_user_notice.Tier_Numeral{
						Numeral: user_subscribe_user_notice.SubTier_SUB_TIER_1,
					},
				},
				Streak: &user_subscribe_user_notice.TenureStreak{},
			},
		},
	}
}

func InvalidUserSubscriptionUserNoticeEvent() *user_subscribe_user_notice.Create {
	return &user_subscribe_user_notice.UserSubscribeUserNoticeCreate{
		FromUserId: "",
		ToUserId:   "",
		SubscriptionInfo: &user_subscribe_user_notice.Subscription{
			Id:              "",
			ProductId:       "",
			OriginId:        "",
			BenefitStartsAt: nil,
			BenefitEndsAt:   nil,
			UpdatedAt:       nil,
			CreatedAt:       nil,
			CreatedBy:       "",
		},
		UserNotice: &user_subscribe_user_notice.UserSubscribeUserNoticeCreate_Subscription{
			Subscription: &user_subscribe_user_notice.SubscriptionUserNotice{
				CumulativeTenureMonths: -1,
				CustomMessage:          "",
				Tier:                   &user_subscribe_user_notice.Tier{},
				Streak:                 &user_subscribe_user_notice.TenureStreak{},
			},
		},
	}
}

func ValidBadgesResponse() []percy.Badge {
	return []percy.Badge{
		percy.FormerConductorBadge,
	}
}

func ValidTwirpBadgeTypeResponse() rpc.BadgeType {
	return rpc.BadgeType_FORMER_CONDUCTOR
}

func ValidCelebrationConfig() percy.CelebrationConfig {
	return percy.CelebrationConfig{
		ChannelID: ChannelID,
		Enabled:   true,
		Celebrations: map[string]percy.Celebration{
			"id": {
				CelebrationID:        "id",
				Enabled:              true,
				EventType:            percy.EventTypeSubscriptionGift,
				EventThreshold:       1,
				CelebrationEffect:    percy.EffectFireworks,
				CelebrationArea:      percy.AreaEverywhere,
				CelebrationIntensity: 1,
				CelebrationDuration:  time.Duration(1),
			},
		},
	}
}

func ValidTwirpCelebrationConfig() rpc_celebi.CelebrationConfig {
	return rpc_celebi.CelebrationConfig{
		ChannelId: ChannelID,
		Enabled:   true,
		Celebrations: []*rpc_celebi.Celebration{
			{
				CelebrationId:  "hi",
				Enabled:        true,
				EventType:      rpc_celebi.EventType_SUBSCRIPTION_GIFT,
				EventThreshold: 1,
				Effect:         rpc_celebi.CelebrationEffect_FIREWORKS,
				Area:           rpc_celebi.CelebrationArea_EVERYWHERE,
				Intensity:      1,
				Duration:       ptypes.DurationProto(1),
			},
		},
	}
}

func CreatedValidCelebration() percy.Celebration {
	return percy.Celebration{
		CelebrationID:        "hi",
		Enabled:              true,
		EventType:            percy.EventTypeSubscriptionGift,
		EventThreshold:       1,
		CelebrationEffect:    percy.EffectFireworks,
		CelebrationArea:      percy.AreaEverywhere,
		CelebrationIntensity: 1,
		CelebrationDuration:  time.Duration(1),
	}
}

func CreatedValidTwirpCelebration() rpc_celebi.Celebration {
	return rpc_celebi.Celebration{
		CelebrationId:  "hi",
		Enabled:        true,
		EventType:      rpc_celebi.EventType_SUBSCRIPTION_GIFT,
		EventThreshold: 1,
		Effect:         rpc_celebi.CelebrationEffect_FIREWORKS,
		Area:           rpc_celebi.CelebrationArea_EVERYWHERE,
		Intensity:      1,
		Duration:       ptypes.DurationProto(1),
	}
}

func ValidCelebrationUserSettings() percy.CelebrationUserSettings {
	return percy.CelebrationUserSettings{
		IsOptedOut: true,
	}
}

func ValidTwirpCelebrationUserSettings() rpc_celebi.CelebrationUserSettings {
	return rpc_celebi.CelebrationUserSettings{
		IsOptedOut: true,
	}
}

func ValidCelebrationPurchasableConfig() *percy.CelebrationChannelPurchasableConfig {
	return &percy.CelebrationChannelPurchasableConfig{
		ChannelID: ChannelID,
		PurchaseConfig: map[percy.CelebrationSize]percy.CelebrationPurchaseConfig{
			percy.CelebrationSizeSmall: {
				IsDisabled: false,
				OfferID:    OfferID,
			},
		},
	}
}

func ValidTwirpCelebrationProductConfiguration() rpc_celebi.ProductConfiguration {
	return rpc_celebi.ProductConfiguration{
		Size:     rpc_celebi.CelebrationSize_SMALL,
		Disabled: false,
		OfferId:  OfferID,
	}
}

func ValidCelebrationProductConfigurationUpdate() percy.CelebrationChannelPurchasableConfigUpdate {
	return percy.CelebrationChannelPurchasableConfigUpdate{
		PurchaseConfig: map[percy.CelebrationSize]percy.CelebrationPurchaseConfigUpdate{
			percy.CelebrationSizeSmall: {
				IsDisabled: pointers.BoolP(false),
				OfferID:    pointers.StringP(OfferID),
			},
		},
	}
}

func ValidTwirpCelebrationProductConfigurationUpdate() []*rpc_celebi.ProductConfigurationUpdate {
	return []*rpc_celebi.ProductConfigurationUpdate{
		{
			Size:     rpc_celebi.CelebrationSize_SMALL,
			Disabled: &wrappers.BoolValue{Value: false},
			OfferId:  &wrappers.StringValue{Value: OfferID},
		},
	}
}

func ValidBoostOpportunity() percy.BoostOpportunity {
	return percy.BoostOpportunity{
		ChannelID:        ChannelID,
		ID:               uuid.New().String(),
		StartedAt:        time.Now().UTC(),
		ExpiresAt:        time.Now().Add(5 * time.Minute).UTC(),
		UnitsContributed: 0,
	}
}

func ValidCompletedBoostOpportunity() percy.BoostOpportunity {
	currentTime := time.Now()

	return percy.BoostOpportunity{
		ChannelID:        ChannelID,
		ID:               uuid.New().String(),
		StartedAt:        currentTime.Add(-5 * time.Minute).UTC(),
		ExpiresAt:        currentTime,
		EndedAt:          &currentTime,
		UnitsContributed: 10,
	}
}

func ValidTwirpPaidBoostOpportunity() rpc_celebi.PaidBoostOpportunity {
	return rpc_celebi.PaidBoostOpportunity{
		Id:                    ChannelID,
		StartedAt:             timestamppb.Now(),
		ExpiresAt:             timestamppb.New(time.Now().Add(time.Second * 10)),
		RemainingSeconds:      9,
		BoostUnitsContributed: 0,
	}
}

func ValidBoostOpportunityContribution() percy.BoostOpportunityContribution {
	return percy.BoostOpportunityContribution{
		ChannelID: ChannelID,
		UserID:    UserID,
		Units:     100,
		OriginID:  uuid.New().String(),
		Timestamp: time.Now().UTC(),
	}
}

func ValidApproaching() percy.Approaching {
	return percy.Approaching{
		ChannelID:               ChannelID,
		ExpiresAt:               time.Now().Add(time.Minute),
		StartedAt:               time.Now(),
		Participants:            []string{UserID},
		Goal:                    10,
		RateLimited:             false,
		ID:                      uuid.New().String(),
		UpdatedAt:               time.Now(),
		NumberOfTimesShown:      1,
		TimeShownPerHour:        time.Minute,
		CreatedTime:             time.Now(),
		CreatorColor:            "red",
		EventsRemainingDuration: map[int]time.Duration{1: 30 * time.Second},
		LevelOneRewards: []percy.Reward{{
			Type:    percy.EmoteReward,
			ID:      "1A",
			GroupID: "1111",
		}},
	}
}

func ValidTwirpApproaching() rpc.Approaching {
	return rpc.Approaching{
		Approaching:            true,
		Participants:           []string{UserID},
		Goal:                   10,
		CreatorColor:           "red",
		EventsRemainingSeconds: map[int64]int64{1: 100},
		LevelOneRewards: []*rpc.Reward{{
			Id:      "a",
			Type:    rpc.RewardType_BADGE,
			GroupId: "b",
		}},
	}
}

func ValidActiveBitsPinata(channelID string) percy.BitsPinata {
	return percy.BitsPinata{
		ChannelID:        channelID,
		CreatedAt:        time.Now().UTC().Add(time.Second * -20),
		PinataID:         uuid.New().String(),
		TimerID:          uuid.New().String(),
		StreamID:         uuid.New().String(),
		BitsToBreak:      1000,
		BitsContributed:  900,
		LastContribution: time.Now().UTC().Add(time.Second * -2),
	}
}

func ValidNewBitsPinata(channelID string) percy.BitsPinata {
	return percy.BitsPinata{
		ChannelID:        channelID,
		CreatedAt:        time.Now().UTC(),
		PinataID:         uuid.New().String(),
		TimerID:          uuid.New().String(),
		StreamID:         uuid.New().String(),
		BitsToBreak:      1000,
		BitsContributed:  0,
		LastContribution: time.Now().UTC(),
	}
}

func ValidExpiredBitsPinata(channelID string) percy.BitsPinata {
	return percy.BitsPinata{
		ChannelID:        channelID,
		CreatedAt:        time.Now().UTC().Add(time.Minute * -2),
		PinataID:         uuid.New().String(),
		TimerID:          uuid.New().String(),
		StreamID:         uuid.New().String(),
		BitsToBreak:      1000,
		BitsContributed:  100,
		LastContribution: time.Now().UTC().Add(time.Second * -220),
	}
}

func ValidBrokenBitsPinata(channelID string) percy.BitsPinata {
	return percy.BitsPinata{
		ChannelID:        channelID,
		CreatedAt:        time.Now().UTC().Add(time.Minute * -2),
		PinataID:         uuid.New().String(),
		TimerID:          uuid.New().String(),
		StreamID:         uuid.New().String(),
		BitsToBreak:      1000,
		BitsContributed:  1000,
		LastContribution: time.Now().UTC().Add(time.Minute * -1),
	}
}
