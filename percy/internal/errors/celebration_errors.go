package errors

import (
	celebi "code.justin.tv/commerce/percy/rpc/celebrations"
	celebi_error "code.justin.tv/commerce/percy/rpc/celebrations/errors"
	"github.com/joomcode/errorx"
	"github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

var (
	CelebiError = errorx.NewNamespace("CelebiError")

	CelebrationNotFound  = CelebiError.NewType("NotFound")
	PermissionDenied     = CelebiError.NewType("PermissionDenied")
	CelebrationCollision = CelebiError.NewType("Collision")
	OverLimit            = CelebiError.NewType("OverLimit")

	IllegalArgument = errorx.IllegalArgument
	InternalError   = errorx.InternalError
	IllegalState    = errorx.IllegalState
	NotImplemented  = errorx.NotImplemented
)

func CelebiErrorToTwirpError(err error) twirp.Error {
	switch errorx.TypeSwitch(err, CelebrationNotFound, PermissionDenied, CelebrationCollision) {
	case CelebrationNotFound:
		errorResponse := celebi_error.CelebiError{
			ErrorCode: celebi.CelebrationErrorCode_CELEBRATION_NOT_FOUND.String(),
		}
		return celebi_error.NewCelebiError(twirp.NewError(twirp.NotFound, err.Error()), errorResponse)
	case CelebrationCollision:
		errorResponse := celebi_error.CelebiError{
			ErrorCode: celebi.CelebrationErrorCode_CELEBRATION_ALREADY_EXISTS.String(),
		}
		return celebi_error.NewCelebiError(twirp.NewError(twirp.InvalidArgument, err.Error()), errorResponse)
	case PermissionDenied:
		errorResponse := celebi_error.CelebiError{
			ErrorCode: celebi.CelebrationErrorCode_PERMISSION_DENIED.String(),
		}
		return celebi_error.NewCelebiError(twirp.NewError(twirp.PermissionDenied, err.Error()), errorResponse)
	case OverLimit:
		errorResponse := celebi_error.CelebiError{
			ErrorCode: celebi.CelebrationErrorCode_OVER_CELEBRATION_LIMIT.String(),
		}
		return celebi_error.NewCelebiError(twirp.NewError(twirp.ResourceExhausted, err.Error()), errorResponse)
	case errorx.NotRecognisedType():
		fallthrough
	default:
		logrus.WithError(err).Error("returning twirp internal error from error handler")
		return twirp.InternalErrorWith(err)
	}
}
