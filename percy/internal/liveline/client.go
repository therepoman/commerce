package liveline

import (
	"context"
	"net/http"
	"strconv"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/wrapper"
	"code.justin.tv/discovery/liveline/proto/liveline"
	"code.justin.tv/foundation/twitchclient"
	"github.com/cactus/go-statsd-client/statsd"
)

type Client struct {
	client liveline.Liveline
}

var _ percy.ChannelStatusDB = &Client{}

func NewClient(host string, statter statsd.Statter, socksAddr string) *Client {
	return &Client{
		client: liveline.NewLivelineProtobufClient(host,
			twitchclient.NewHTTPClient(twitchclient.ClientConf{
				Stats:          statter,
				StatNamePrefix: "liveline",
				RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
					wrapper.NewProxyRoundTripWrapper(socksAddr),
					wrapper.NewRetryRoundTripWrapper(statter, "liveline", 1),
					wrapper.NewHystrixRoundTripWrapper("liveline", 2000),
				},
				Transport: twitchclient.TransportConf{
					MaxIdleConnsPerHost: 200,
				},
			})),
	}
}

func (l *Client) ChannelIsStreaming(ctx context.Context, channelID string) (bool, error) {
	resp, err := l.client.GetStreamsByChannelIDs(ctx, &liveline.StreamsByChannelIDsRequest{
		ChannelIds: []string{channelID},
	})
	if err != nil {
		return false, err
	}
	return isStreaming(resp, channelID), nil
}

func (l *Client) GetStreamData(ctx context.Context, channelID string) (percy.StreamData, error) {
	resp, err := l.client.GetStreamsByChannelIDs(ctx, &liveline.StreamsByChannelIDsRequest{
		ChannelIds: []string{channelID},
	})
	if err != nil {
		return percy.StreamData{}, err
	}

	// Check if offline
	if !isStreaming(resp, channelID) {
		return percy.StreamData{
			IsStreaming: false,
		}, nil
	}
	stream := resp.Streams[0]

	// Get viewcount
	viewCount := 0
	if stream.GetViewcountData() != nil {
		viewCount = int(stream.GetViewcountData().GetViewcount())
	}

	return percy.StreamData{
		ChannelID:   channelID,
		IsStreaming: true,
		BroadcastID: strconv.FormatInt(stream.GetBroadcastId(), 10),
		ViewCount:   viewCount,
	}, nil
}

func isStreaming(resp *liveline.StreamsResponse, channelID string) bool {
	isOffline := resp == nil || len(resp.Streams) == 0 || resp.Streams[0].ChannelId != channelID
	return !isOffline
}
