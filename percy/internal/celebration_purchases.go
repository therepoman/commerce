package percy

import (
	"context"
	"fmt"
	"math"
	"strconv"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/models"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
)

type CelebrationsNotEligibleReasonCode string

const (
	celebrationExperimentRevenueSplit = 0.5
	purchasableCelebrationEffectName  = "FIREWORKS" // TODO: Where should we get this string?
	celebrationRefundReason           = "celebration can't be fulfilled"
	celebrationUserNoticeID           = "celebrationpurchase"

	// TODO: Should get this from product catalog, but this will not be useful until:
	//  * our product descriptions are more meaningful
	//  * the descriptions are localized
	celebrationRefundEmailProductName = "Celebration"

	celebrationUserNoticeIntensityKey      = "intensity"
	celebrationUserNoticeEffectKey         = "effect"
	celebrationUserNoticeDefaultSystemBody = "celebration notification"

	celebrationPurchaseTransactionProduct = "EXPLICIT_PURCHASE"

	CelebrationsNotEligibleReasonCodeNone = CelebrationsNotEligibleReasonCode("NONE")
)

type CelebrationsPurchaseOfferEligibility struct {
	OfferId               string
	IsEligible            bool
	ShouldDisplay         bool
	NotEligibleReason     string
	NotEligibleReasonCode CelebrationsNotEligibleReasonCode
}

type CelebrationsPurchaseFulfillmentReq struct {
	RecipientUserID  string `json:"receipient_user_id"`
	BenefactorUserID string `json:"benefactor_user_id"`
	OriginID         string `json:"origin_id"`
	OfferID          string `json:"offer_id"`
}

type CelebrationsPurchaseRevocationReq struct {
	RecipientUserID  string `json:"receipient_user_id"`
	BenefactorUserID string `json:"benefactor_user_id"`
	OriginID         string `json:"origin_id"`
	OfferID          string `json:"offer_id"`
}

type CelebrationsEligibilityReadDepth string

const (
	CelebrationsEligibilityReadDepthShallow = CelebrationsEligibilityReadDepth("SHALLOW")
	CelebrationsEligibilityReadDepthDeep    = CelebrationsEligibilityReadDepth("DEEP")
)

type CelebrationsIsEligibleArgs struct {
	UserID    string
	ChannelID string
	OfferIDs  []string
	ReadDepth CelebrationsEligibilityReadDepth
}

type CelebrationsPayoutArgs struct {
	IsClawback       bool
	RecipientUserID  string
	BenefactorUserID string
	OriginID         string
	OfferID          string
}

type CelebrationsRefundEmailArgs struct {
	RecipientUserID  string
	BenefactorUserID string
}

type EmitCelebrationArgs struct {
	RecipientUserID  string
	BenefactorUserID string
	OriginID         string
	OfferID          string
}

type PublishCelebrationToActivityFeedArgs struct {
	RecipientUserID  string
	BenefactorUserID string
	OriginID         string
	OfferID          string
}

type CelebrationsDatascienceArgs struct {
	RecipientUserID  string
	BenefactorUserID string
	OriginID         string
	OfferID          string
}

type CelebrationsRefundArgs struct {
	RecipientUserID  string
	BenefactorUserID string
	OriginID         string
	OfferID          string
}

type CelebrationsUserNoticeArgs struct {
	RecipientUserID  string
	BenefactorUserID string
	OriginID         string
	OfferID          string
}

func (eng *celebrationEngine) FulfillCelebrationPurchase(ctx context.Context, req CelebrationsPurchaseFulfillmentReq) error {
	err := eng.StateMachine.StartExecutionCelebrationPurchaseFulfillment(ctx, req)
	if err != nil {
		msg := "error calling StartExecutionCelebrationPurchaseFulfillment in FulfillCelebrationPurchase"
		logrus.WithField("req", req).WithError(err).Error(msg)
		return errors.Wrap(err, msg)
	}

	return nil
}

func (eng *celebrationEngine) RevokeCelebrationPurchase(ctx context.Context, req CelebrationsPurchaseRevocationReq) error {
	err := eng.StateMachine.StartExecutionCelebrationPurchaseRevoke(ctx, req)
	if err != nil {
		msg := "error calling StartExecutionCelebrationPurchaseRevoke in RevokeCelebrationPurchase"
		logrus.WithField("req", req).WithError(err).Error(msg)
		return errors.Wrap(err, msg)
	}

	return nil
}

func (eng *celebrationEngine) IsEligibleForCelebrationPurchase(ctx context.Context, args CelebrationsIsEligibleArgs) ([]CelebrationsPurchaseOfferEligibility, error) {
	if !eng.WhitelistDB.HasCelebrations(args.ChannelID) {
		return makeIneligibleAll(args.OfferIDs, "channel is not included in the celebration experiment"), nil
	}

	// Use the Corleone read depth parameter to indicate request context
	// "Shallow" corresponds to the creator dashboard presentation in which all offers should be displayed as eligible
	if args.ReadDepth == CelebrationsEligibilityReadDepthShallow {
		return makeEligibleAll(args.OfferIDs), nil
	}

	if args.ChannelID == args.UserID {
		return makeIneligibleAll(args.OfferIDs, "cannot purchase celebration in own channel"), nil
	}

	channelCelebrationsEnabled := false
	userChatBanned := false
	channelMonetized := false

	g, gCtx := errgroup.WithContext(ctx)

	// Ensure channel has celebrations enabled
	g.Go(func() error {
		channelCelebrationConfig, err := eng.CelebrationConfigsDB.GetCelebrationConfig(gCtx, args.ChannelID)
		if err != nil {
			return err
		}
		if channelCelebrationConfig != nil && channelCelebrationConfig.Enabled {
			channelCelebrationsEnabled = true
		}

		return nil
	})

	// Ensure user is not chat banned
	g.Go(func() error {
		banned, err := eng.UserProfileDB.IsChatBanned(gCtx, args.UserID, args.ChannelID)
		if err != nil {
			return err
		}
		if banned {
			userChatBanned = true
		}

		return nil
	})

	// Ensure channel is monetized
	g.Go(func() error {
		monetized, err := eng.UserProfileDB.IsMonetized(gCtx, args.ChannelID)
		if err != nil {
			return err
		}
		if monetized {
			channelMonetized = true
		}

		return nil
	})

	err := g.Wait()
	if err != nil {
		msg := "error during parallel validation in IsEligibleForCelebrationPurchase"
		logrus.WithField("req", args).WithError(err).Error(msg)
		return nil, errors.Wrap(err, msg)
	}

	if userChatBanned {
		return makeIneligibleAll(args.OfferIDs, "user is chat banned"), nil
	}

	if !channelCelebrationsEnabled {
		return makeIneligibleAll(args.OfferIDs, "celebrations disabled on channel"), nil
	}

	if !channelMonetized {
		return makeIneligibleAll(args.OfferIDs, "channel is not monetized"), nil
	}

	offerEligibility, err := eng.getOfferEligibilityFromConfig(ctx, args)
	if err != nil {
		logrus.WithField("req", args).WithError(err).Error("error calling getOfferEligibilityFromConfig in IsEligibleForCelebrationPurchase")
		return nil, err
	}

	return offerEligibility, nil
}

func (eng *celebrationEngine) getOfferEligibilityFromConfig(ctx context.Context, args CelebrationsIsEligibleArgs) ([]CelebrationsPurchaseOfferEligibility, error) {
	var offerEligibility []CelebrationsPurchaseOfferEligibility

	config, err := eng.CelebrationChannelPurchasableConfigDB.GetConfig(ctx, args.ChannelID)
	if err != nil {
		return nil, errors.Wrap(err, "error calling CelebrationChannelPurchasableConfigDB#GetConfig in getOfferEligibilityFromConfig")
	}
	if config == nil {
		return nil, nil
	}

	for _, offerID := range args.OfferIDs {
		isEligible := false

		for _, channelConfiguredOffer := range config.PurchaseConfig {
			if channelConfiguredOffer.OfferID == offerID && !channelConfiguredOffer.IsDisabled {
				isEligible = true
				break
			}
		}

		if isEligible {
			offerEligibility = append(offerEligibility, makeEligibleOffer(offerID))
		} else {
			offerEligibility = append(offerEligibility, makeIneligibleOffer(offerID, "offer not enabled on channel"))
		}
	}

	return offerEligibility, nil
}

func makeIneligibleOffer(offerID string, reason string) CelebrationsPurchaseOfferEligibility {
	return CelebrationsPurchaseOfferEligibility{
		OfferId:    offerID,
		IsEligible: false,
		// Corleone + GQL don't gracefully handle offers where "ShouldDisplay" is "false"
		// We need to mark this as "true" so inelgible offers can be correctly displayed on the creator dashboard
		ShouldDisplay:     true,
		NotEligibleReason: reason,
	}
}

func makeEligibleOffer(offerID string) CelebrationsPurchaseOfferEligibility {
	return CelebrationsPurchaseOfferEligibility{
		OfferId:               offerID,
		IsEligible:            true,
		ShouldDisplay:         true,
		NotEligibleReasonCode: CelebrationsNotEligibleReasonCodeNone,
	}
}

func makeIneligibleAll(offerIDs []string, reason string) []CelebrationsPurchaseOfferEligibility {
	var ineligibleOffers []CelebrationsPurchaseOfferEligibility
	for _, offer := range offerIDs {
		ineligibleOffers = append(ineligibleOffers, makeIneligibleOffer(offer, reason))
	}

	return ineligibleOffers
}

func makeEligibleAll(offerIDs []string) []CelebrationsPurchaseOfferEligibility {
	var eligibleOffers []CelebrationsPurchaseOfferEligibility
	for _, offer := range offerIDs {
		eligibleOffers = append(eligibleOffers, makeEligibleOffer(offer))
	}

	return eligibleOffers
}

func (eng *celebrationEngine) SendRefundEmail(ctx context.Context, req CelebrationsRefundEmailArgs) error {
	return eng.Notification.PublishCelebrationRefund(ctx, PublishCelebrationRefundArgs{
		RecipientUserID:  req.RecipientUserID,
		BenefactorUserID: req.BenefactorUserID,
		Celebration:      celebrationRefundEmailProductName,
	})
}

func (eng *celebrationEngine) ClawbackPayout(ctx context.Context, req CelebrationsPayoutArgs) error {
	task, err := eng.IdempotencyDB.Get(ctx, IdempotencyNamespaceCelebrationPurchaseFulfillment, req.OriginID, string(CelebrationPurchaseFulfillmentPostRevenue))
	if err != nil {
		return err
	}

	// Should not be getting revocation requests for celebrations that were not paid out
	if !(task != nil && task.State == IdempotencyStateComplete) {
		logrus.WithField("origin_id", req.OriginID).Info("skipping revocation, payout never completed in original fulfillment")
		return nil
	}

	return eng.Payout(ctx, req)
}

func (eng *celebrationEngine) Payout(ctx context.Context, req CelebrationsPayoutArgs) error {
	purchaseOrderPayments, err := eng.Purchase.GetPurchaseOrderPayments(ctx, req.OriginID)
	if err != nil {
		return errors.Wrap(err, "error calling Purchase#GetPurchaseOrderPayments in Payout")
	}

	payoutCents, err := calculatePayoutCents(purchaseOrderPayments)
	if err != nil {
		return errors.Wrap(err, "error calling calculatePayoutCents in Payout")
	}

	transactionType := RevenueTransactionTypePurchase
	if req.IsClawback {
		transactionType = RevenueTransactionTypeRefund
		payoutCents = payoutCents * -1
	}

	partnerType, err := eng.UserProfileDB.GetPartnerType(ctx, req.RecipientUserID)
	if err != nil {
		return errors.Wrap(err, "error calling UserProfileDB#GetPartnerType in Payout")
	}

	return eng.Purchase.PostCreatorRevenueShareForPurchase(ctx, CreatorRevenueShareArgs{
		RecipientUserID: req.RecipientUserID,
		PayoutCents:     payoutCents,
		PartnerType:     partnerType,
		TransactionID:   req.OriginID,
		TimeOfEvent:     time.Now(),
		OfferID:         req.OfferID,
		RevenueSource:   RevenueSourceCelebration,
		TransactionType: transactionType,
	})
}

func (eng *celebrationEngine) EmitCelebration(ctx context.Context, req EmitCelebrationArgs) error {
	size, err := eng.getCelebrationSizeForOfferID(ctx, req.RecipientUserID, req.OfferID)
	if err != nil {
		return errors.Wrap(err, "error getting celebration size in EmitCelebration")
	}

	celebration, err := MakePurchasableCelebration(req.OriginID, size)
	if err != nil {
		return errors.Wrap(err, "error calling MakePurchasableCelebration in EmitCelebration")
	}

	return eng.Publisher.StartCelebration(ctx, req.RecipientUserID, User{Id: req.BenefactorUserID}, 1, celebration)
}

func (eng *celebrationEngine) PublishToActivityFeed(ctx context.Context, req PublishCelebrationToActivityFeedArgs) error {
	size, err := eng.getCelebrationSizeForOfferID(ctx, req.RecipientUserID, req.OfferID)
	if err != nil {
		return errors.Wrap(err, "error getting celebration size in PublishToActivityFeed")
	}

	return eng.SNS.PublishCelebrationActivityFeedEvent(ctx, models.CelebrationPurchaseActivityFeedEvent{
		ChannelID: req.RecipientUserID,
		UserID:    req.BenefactorUserID,
		Intensity: string(size),
		Effect:    purchasableCelebrationEffectName,
	})
}

func (eng *celebrationEngine) PublishDatascience(ctx context.Context, req CelebrationsDatascienceArgs) error {
	size, err := eng.getCelebrationSizeForOfferID(ctx, req.RecipientUserID, req.OfferID)
	if err != nil {
		return errors.Wrap(err, "error getting celebration size in Datascience")
	}

	purchaseOrderPayments, err := eng.Purchase.GetPurchaseOrderPayments(ctx, req.OriginID)
	if err != nil {
		return errors.Wrap(err, "error calling Purchase#GetPurchaseOrderPayments in Datascience")
	}

	purchaseCostBasis, err := calculatePurchaseCostBasis(purchaseOrderPayments)
	if err != nil {
		return errors.Wrap(err, "error calling calculatePurchaseCostBasis in Datascience")
	}

	numListeners, err := eng.Publisher.GetNumberOfPubsubListeners(CelebrationOverlayPubsubTopic, req.RecipientUserID)
	if err != nil {
		return errors.Wrap(err, "error calling pubsub control for num listeners in Datascience")
	}

	err = eng.Spade.SendCelebrationOverlayInitiatedEvent(ctx, CelebrationOverlayInitiatedEventParams{
		ServerTime:         time.Now().UTC().Format(time.RFC3339),
		ChannelID:          req.RecipientUserID,
		UserID:             req.BenefactorUserID,
		TransactionID:      req.OriginID,
		CelebrationID:      req.OfferID,
		TransactionProduct: celebrationPurchaseTransactionProduct,
		NumPubsubListeners: numListeners,
		Intensity:          size,
		GrossAmount:        purchaseCostBasis,
	})
	if err != nil {
		return errors.Wrap(err, "failed to publish event to Datascience")
	}

	return nil
}

func (eng *celebrationEngine) Refund(ctx context.Context, req CelebrationsRefundArgs) error {
	return eng.Purchase.RefundPurchaseOrder(ctx, req.OriginID, celebrationRefundReason)
}

func (eng *celebrationEngine) SendFulfillmentUserNotice(ctx context.Context, req CelebrationsUserNoticeArgs) error {
	size, err := eng.getCelebrationSizeForOfferID(ctx, req.RecipientUserID, req.OfferID)
	if err != nil {
		return errors.Wrap(err, "error getting celebration size in SendFulfillmentUserNotice")
	}

	parsedChannelID, err := strconv.Atoi(req.RecipientUserID)
	if err != nil {
		return errors.Wrap(err, "error parsing recipient userID as int")
	}

	parsedUserID, err := strconv.Atoi(req.BenefactorUserID)
	if err != nil {
		return errors.Wrap(err, "error parsing benefactor userID as int")
	}

	err = eng.UserProfileDB.SendUserNotice(ctx, UserNoticeParams{
		SenderUserID:    parsedUserID,
		TargetChannelID: parsedChannelID,
		MessageID:       celebrationUserNoticeID,
		MessageParams: []UserNoticeMessageParams{
			{
				Key:   celebrationUserNoticeIntensityKey,
				Value: string(size),
			},
			{
				Key:   celebrationUserNoticeEffectKey,
				Value: purchasableCelebrationEffectName,
			},
		},
		DefaultSystemBody: celebrationUserNoticeDefaultSystemBody,
	})

	if err != nil {
		return errors.Wrap(err, "error sending user notice for celebration")
	}

	return nil
}

func (eng *celebrationEngine) getCelebrationSizeForOfferID(ctx context.Context, channelID, offerID string) (CelebrationSize, error) {
	config, err := eng.CelebrationChannelPurchasableConfigDB.GetConfig(ctx, channelID)
	if err != nil {
		return "", errors.Wrap(err, "error calling CelebrationChannelPurchasableConfigDB#GetConfig")
	}
	if config == nil {
		return "", fmt.Errorf("GetConfig returned nil in EmitCelebration for channel<%s>", channelID)
	}

	var matchingSize CelebrationSize
	for size, config := range config.PurchaseConfig {
		if !config.IsDisabled && config.OfferID == offerID {
			matchingSize = size
			break
		}
	}

	if matchingSize == "" {
		return "", fmt.Errorf("could not find a purchasable configuration for the offer<%s> and channel<%s>", offerID, channelID)
	}

	return matchingSize, nil
}

// Mostly copied from https://git.xarth.tv/subs/griphook/blame/master/lambda/functions/calculate_creator_payout_v2/main.go#L59
func calculatePayoutCents(purchaseOrderPayments []PurchaseOrderPayment) (int64, error) {
	purchaseCostBasisCents, err := calculatePurchaseCostBasis(purchaseOrderPayments)
	if err != nil {
		return 0, err
	}

	// TODO: For the experiment, we are paying out a static 50% revenue split. To facilitate this,
	// we are only allowing in creators with the "standard" split (and if it changes mid-experiment,
	// there will be no recourse). For the full rollout, we will need to:
	//  * Query the each channel's contractual revenue split
	//  * Honor 0 rev split channels
	creatorPayoutCents := int64(math.Ceil(float64(purchaseCostBasisCents) * celebrationExperimentRevenueSplit))

	return creatorPayoutCents, nil
}

func calculatePurchaseCostBasis(purchaseOrderPayments []PurchaseOrderPayment) (int64, error) {
	var grossAmountUSD int64
	var feesUSD int64
	var taxesUSD int64

	// TODO: Will eventually need to support bundling/promos
	for _, purchaseOrderPayment := range purchaseOrderPayments {
		grossAmountUSD += purchaseOrderPayment.GrossAmount
		feesUSD += purchaseOrderPayment.Fees
		taxesUSD += purchaseOrderPayment.Taxes
	}

	if grossAmountUSD <= 0 {
		return 0, errors.New("encountered a purchase of <= $0 gross")
	}

	purchaseCostBasisCents := grossAmountUSD - feesUSD - taxesUSD

	if purchaseCostBasisCents <= 0 {
		return 0, errors.New("attempted to payout $0")
	}

	return purchaseCostBasisCents, nil
}
