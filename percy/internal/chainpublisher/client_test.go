package chainpublisher

import (
	"context"
	"errors"
	"testing"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	. "github.com/smartystreets/goconvey/convey"
)

func TestChainPublisherCollectErrors(t *testing.T) {
	ctx := context.Background()

	Convey("Given a chain publisher with multiple publishers in the chain", t, func() {
		mockPublisher1 := &internalfakes.FakePublisher{}
		mockPublisher2 := &internalfakes.FakePublisher{}
		mockPublisher3 := &internalfakes.FakePublisher{}

		chainPublisher := &Client{
			publishers: []percy.Publisher{mockPublisher1, mockPublisher2, mockPublisher3},
		}

		Convey("When none of the underlying publish calls return an error", func() {
			mockPublisher1.StartHypeTrainReturns(nil)
			mockPublisher2.StartHypeTrainReturns(nil)
			mockPublisher3.StartHypeTrainReturns(nil)

			hypeTrain := percy.HypeTrain{
				ChannelID: "cool-person",
			}
			err := chainPublisher.StartHypeTrain(ctx, hypeTrain)

			Convey("Then the publish call does not return an error", func() {
				So(err, ShouldBeNil)
			})

			Convey("Then each underlying publisher should have their method called", func() {
				So(mockPublisher1.StartHypeTrainCallCount(), ShouldEqual, 1)
				So(mockPublisher2.StartHypeTrainCallCount(), ShouldEqual, 1)
				So(mockPublisher3.StartHypeTrainCallCount(), ShouldEqual, 1)
			})
		})

		Convey("When one of the publishers returns an error", func() {
			mockPublisher1.EndHypeTrainReturns(nil)
			mockPublisher2.EndHypeTrainReturns(nil)
			mockPublisher3.EndHypeTrainReturns(errors.New("uh oh spaghettios"))

			err := chainPublisher.EndHypeTrain(ctx, percy.HypeTrain{}, time.Now(), percy.EndingReasonCompleted)

			Convey("Then the publish call does return an error", func() {
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "uh oh spaghettios")
			})

			Convey("Then each underlying publisher should have their method called", func() {
				So(mockPublisher1.EndHypeTrainCallCount(), ShouldEqual, 1)
				So(mockPublisher2.EndHypeTrainCallCount(), ShouldEqual, 1)
				So(mockPublisher3.EndHypeTrainCallCount(), ShouldEqual, 1)
			})
		})

		Convey("When all of the publishers return errors", func() {
			mockPublisher1.PostHypeTrainProgressionReturns(errors.New("this is not good"))
			mockPublisher2.PostHypeTrainProgressionReturns(errors.New("all hands on deck"))
			mockPublisher3.PostHypeTrainProgressionReturns(errors.New("everyone panic!"))

			err := chainPublisher.PostHypeTrainProgression(ctx, percy.Participation{}, percy.Progress{})

			Convey("Then the returned error contains all error strings rolled up", func() {
				So(err, ShouldNotBeNil)
				So(err.Error(), ShouldContainSubstring, "this is not good")
				So(err.Error(), ShouldContainSubstring, "all hands on deck")
				So(err.Error(), ShouldContainSubstring, "everyone panic!")
			})

			Convey("Then each underlying publisher should have their method called", func() {
				So(mockPublisher1.PostHypeTrainProgressionCallCount(), ShouldEqual, 1)
				So(mockPublisher2.PostHypeTrainProgressionCallCount(), ShouldEqual, 1)
				So(mockPublisher3.PostHypeTrainProgressionCallCount(), ShouldEqual, 1)
			})
		})
	})
}
