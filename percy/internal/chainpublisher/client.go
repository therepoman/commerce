package chainpublisher

import (
	"context"
	"errors"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/pubsub"
	"github.com/hashicorp/go-multierror"
)

// Client holds a series of percy.Publisher implementations and any publish call fans out to every publisher
type Client struct {
	publishers []percy.Publisher
}

func New(publishers ...percy.Publisher) *Client {
	return &Client{
		publishers: publishers,
	}
}

func (c *Client) HypeTrainApproaching(ctx context.Context, channelID string, goal int64, eventsRemainingDurations map[int]time.Duration, rewards []percy.Reward, creatorColor string, participants []string) error {
	return c.collectErrors(func(p percy.Publisher) error {
		return p.HypeTrainApproaching(ctx, channelID, goal, eventsRemainingDurations, rewards, creatorColor, participants)
	})
}

func (c *Client) StartHypeTrain(ctx context.Context, hypeTrain percy.HypeTrain, kickOffParticipations ...percy.Participation) error {
	return c.collectErrors(func(p percy.Publisher) error {
		return p.StartHypeTrain(ctx, hypeTrain, kickOffParticipations...)
	})
}

func (c *Client) PostHypeTrainProgression(ctx context.Context, participation percy.Participation, progress percy.Progress) error {
	return c.collectErrors(func(p percy.Publisher) error {
		return p.PostHypeTrainProgression(ctx, participation, progress)
	})
}

func (c *Client) LevelUpHypeTrain(ctx context.Context, channelID string, timeToExpire time.Time, progress percy.Progress) error {
	return c.collectErrors(func(p percy.Publisher) error {
		return p.LevelUpHypeTrain(ctx, channelID, timeToExpire, progress)
	})
}

func (c *Client) EndHypeTrain(ctx context.Context, hypeTrain percy.HypeTrain, timeEnded time.Time, reason percy.EndingReason) error {
	return c.collectErrors(func(p percy.Publisher) error {
		return p.EndHypeTrain(ctx, hypeTrain, timeEnded, reason)
	})
}

func (c *Client) ParticipantRewards(ctx context.Context, channelID string, entitledParticipants percy.EntitledParticipants) error {
	return c.collectErrors(func(p percy.Publisher) error {
		return p.ParticipantRewards(ctx, channelID, entitledParticipants)
	})
}

func (c *Client) ConductorUpdate(ctx context.Context, channelID string, conductor percy.Conductor) error {
	return c.collectErrors(func(p percy.Publisher) error {
		return p.ConductorUpdate(ctx, channelID, conductor)
	})
}

func (c *Client) CooldownExpired(ctx context.Context, channelID string) error {
	return c.collectErrors(func(p percy.Publisher) error {
		return p.CooldownExpired(ctx, channelID)
	})
}

func (c *Client) StartCelebration(ctx context.Context, channelID string, sender percy.User, amount int64, celebration percy.Celebration) error {
	return c.collectErrors(func(p percy.Publisher) error {
		return p.StartCelebration(ctx, channelID, sender, amount, celebration)
	})
}

// GetNumberOfPubsubListeners is required to meet the (leaky) Publisher abstraction (internal/publisher.go). It will choose
// the first pubsub client it finds in the list of publishers, and call its GetNumberOfPubsubListeners method, or returns an error
// if no such publisher in the chain
func (c *Client) GetNumberOfPubsubListeners(pubsubTopic, channelID string) (int, error) {
	for _, p := range c.publishers {
		if pubsubClient, ok := p.(*pubsub.Publisher); ok {
			return pubsubClient.GetNumberOfPubsubListeners(pubsubTopic, channelID)
		}
	}
	return 0, errors.New("no pubsub client in publisher chain")
}

func (c *Client) collectErrors(f func(p percy.Publisher) error) error {
	var multiErr error
	for _, p := range c.publishers {
		if p == nil {
			continue
		}

		err := f(p)
		if err != nil {
			multiErr = multierror.Append(multiErr, err)
		}
	}
	return multiErr
}
