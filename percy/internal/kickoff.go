package percy

import (
	"sort"
	"time"
)

const (
	// for low values of threshold such as 3, we don't want to trigger pending at a level that is too low a percentage.
	minPercentageForApproaching = 0.66
	// the maximum number of events we want left to trigger a hype train.
	maxMissingEventsForApproaching = 2
	// minimum time in a approaching hype train required before we send the message out.
	MinTimeRemainingForApproaching = 20 * time.Second
)

func canKickoffHypeTrain(config HypeTrainConfig, participations []Participation) (canKickedOff bool, nearbyToKickoff bool) {
	numberOfParticipants := numOfEligibleParticipants(config, participations)

	if numberOfParticipants >= config.KickoffConfig.NumOfEvents {
		return true, false
	} else if nearKickoff(numberOfParticipants, config.KickoffConfig.NumOfEvents) {
		return false, true
	}
	return false, false
}

func numOfEligibleParticipants(config HypeTrainConfig, participations []Participation) int {
	uniqueUseIDs := map[string]interface{}{}
	for _, participation := range participations {
		if isEligibleForKickoff(config, participation) {
			uniqueUseIDs[participation.User.ID()] = nil
		}
	}
	return len(uniqueUseIDs)
}

func nearKickoff(uniqueUserIDs int, numOfEventsToKickoff int) bool {
	minPercentUserRequirement := float64(numOfEventsToKickoff) * minPercentageForApproaching
	return float64(uniqueUserIDs) >= minPercentUserRequirement && numOfEventsToKickoff-uniqueUserIDs <= maxMissingEventsForApproaching
}

func isEligibleForKickoff(config HypeTrainConfig, participation Participation) bool {
	// broadcasters cannot kickoff hype trains in their own channels
	if participation.User.ID() == participation.ChannelID {
		return false
	}

	// check if participation is "big" enough
	points := config.EquivalentParticipationPoint(participation.ParticipationIdentifier, participation.Quantity)
	return points >= config.KickoffConfig.MinPoints
}

func generateEventsExpirationMap(config HypeTrainConfig, participations []Participation) (map[int]time.Duration, []Participation) {
	result := make(map[int]time.Duration)
	filteredParticipations := newestParticipationsByUser(config, participations)

	// do a check to make sure we're still valid
	minPercentUserRequirement := float64(config.KickoffConfig.NumOfEvents) * minPercentageForApproaching
	numOfFilteredUsers := float64(len(filteredParticipations))
	if numOfFilteredUsers >= minPercentUserRequirement {
		// grab the the front most entries (since we know they're sorted oldest to newest after filtering
		switch config.KickoffConfig.NumOfEvents - len(filteredParticipations) {
		case 2:
			result[2] = getDurationSince(config, filteredParticipations[0])
			result[1] = getDurationSince(config, filteredParticipations[1])
		case 1:
			result[1] = getDurationSince(config, filteredParticipations[0])
		}
	}

	return result, filteredParticipations
}

func getDurationSince(config HypeTrainConfig, participation Participation) time.Duration {
	timeSince := time.Since(participation.Timestamp)
	timeLeftInKickoff := config.KickoffConfig.Duration - timeSince
	return timeLeftInKickoff.Round(time.Second)
}

func newestParticipationsByUser(config HypeTrainConfig, participations []Participation) []Participation {
	// sort newest to oldest
	sort.Slice(participations, func(i, j int) bool {
		return participations[i].Timestamp.After(participations[j].Timestamp)
	})

	// filter to newest eligible event for each user, using the sort to make assumptions.
	uniqueUseIDs := map[string]interface{}{}
	filteredParticipations := make([]Participation, 0)
	for _, participation := range participations {
		if _, ok := uniqueUseIDs[participation.User.Id]; !ok && isEligibleForKickoff(config, participation) {
			filteredParticipations = append(filteredParticipations, participation)
			uniqueUseIDs[participation.User.Id] = nil
		}
	}

	// sort oldest to newest before returning
	sort.Slice(filteredParticipations, func(i, j int) bool {
		return filteredParticipations[i].Timestamp.Before(filteredParticipations[j].Timestamp)
	})

	return filteredParticipations
}
