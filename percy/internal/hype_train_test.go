package percy_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	. "github.com/smartystreets/goconvey/convey"
)

func TestHypeTrain_IsValid(t *testing.T) {
	var hypeTrain percy.HypeTrain
	Convey("Returns true when", t, func() {
		Convey("given a valid config", func() {
			hypeTrain = tests.ValidHypeTrain()
		})
		So(hypeTrain.IsValid(), ShouldBeTrue)
	})

	Convey("Returns false when", t, func() {
		hypeTrain = tests.ValidHypeTrain()

		var emptyDuration time.Duration

		Convey("channel ID is blank", func() {
			hypeTrain.ChannelID = ""
		})

		Convey("ID is blank", func() {
			hypeTrain.ID = ""
		})

		Convey("Config is not valid", func() {
			hypeTrain.Config.ChannelID = ""
			hypeTrain.Config.KickoffConfig.Duration = emptyDuration
		})

		So(hypeTrain.IsValid(), ShouldBeFalse)
	})
}

func TestHypeTrain_Progress(t *testing.T) {
	Convey("Give a hype train", t, func() {
		train := tests.ValidOngoingHypeTrain()
		levelSettings := train.Config.LevelSettings()
		firstLevel := levelSettings[0]
		secondLevel := levelSettings[1]
		lastLevel := levelSettings[len(levelSettings)-1]
		secondLastLevel := levelSettings[len(levelSettings)-2]

		Convey("returns the current hype train level", func() {
			var participationPointsToAdd int
			var expectedProgress percy.Progress
			var expectedParticipationPoints percy.ParticipationPoint

			Convey("when there is no participation point", func() {
				expectedProgress = percy.Progress{
					Setting:     firstLevel,
					Progression: percy.ParticipationPoint(0),
					Goal:        firstLevel.Goal,
					Total:       percy.ParticipationPoint(0),
				}
			})

			Convey("when at some participation points", func() {
				participationPointsToAdd = 1
				expectedProgress = percy.Progress{
					Setting:     firstLevel,
					Progression: percy.ParticipationPoint(1),
					Goal:        firstLevel.Goal,
					Total:       percy.ParticipationPoint(1),
				}
			})

			Convey("when there are enough participation points to level up the train", func() {
				participationPointsToAdd = int(firstLevel.Goal) + 1
				expectedProgress = percy.Progress{
					Setting:     secondLevel,
					Progression: percy.ParticipationPoint(1),
					Goal:        secondLevel.Goal - firstLevel.Goal,
					Total:       percy.ParticipationPoint(firstLevel.Goal + 1),
				}
			})

			Convey("when participation points exceeds the highest configured level", func() {
				participationPointsToAdd = int(lastLevel.Goal) + 1
				expectedProgress = percy.Progress{
					Setting:     lastLevel,
					Progression: percy.ParticipationPoint(lastLevel.Goal + 1 - secondLastLevel.Goal),
					Goal:        lastLevel.Goal - secondLastLevel.Goal,
					Total:       percy.ParticipationPoint(lastLevel.Goal + 1),
				}
			})

			expectedParticipationPoints = percy.ParticipationPoint(participationPointsToAdd)
			train.AddParticipation(percy.Participation{
				ChannelID: train.ChannelID,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				},
				Quantity:  participationPointsToAdd,
				Timestamp: time.Now(),
				User:      percy.NewAnonymousCheerer(),
			})

			So(train.Level(), ShouldEqual, expectedProgress.Setting.Level)
			So(train.LevelProgress().Goal, ShouldResemble, expectedProgress.Goal)
			So(train.LevelProgress().Progression, ShouldResemble, expectedProgress.Progression)
			So(train.LevelProgress().Setting, ShouldResemble, expectedProgress.Setting)
			So(train.LevelProgress().Total, ShouldResemble, expectedProgress.Total)
			So(train.LevelProgress().RemainingSeconds, ShouldBeGreaterThan, 0)
			So(train.ParticipationPoints(), ShouldEqual, expectedParticipationPoints)
		})
	})
}

func TestHypeTrain_Levels(t *testing.T) {
	Convey("Given a hype train", t, func() {
		train := tests.ValidOngoingHypeTrain()

		Convey("when there is no participation", func() {
			levelSettings := train.Config.LevelSettings()
			train.Participations = make(percy.ParticipationTotals)

			So(train.Level(), ShouldEqual, levelSettings[0].Level)
			So(train.CompletedLevel(), ShouldEqual, levelSettings[0].Level-1)
		})

		Convey("when there are enough participation to complete the first level", func() {
			levelSettings := train.Config.LevelSettings()
			train.Participations = make(percy.ParticipationTotals)
			train.AddParticipation(percy.Participation{
				ChannelID: train.ChannelID,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				},
				Quantity:  int(levelSettings[0].Goal) + 1,
				Timestamp: time.Now(),
				User:      percy.NewAnonymousCheerer(),
			})

			So(train.Level(), ShouldEqual, levelSettings[1].Level)
			So(train.CompletedLevel(), ShouldEqual, levelSettings[1].Level-1)
		})

		Convey("when there are enough particpation to complete all levels", func() {
			levelSettings := train.Config.LevelSettings()
			finalLevel := levelSettings[len(levelSettings)-1]
			train.Participations = make(percy.ParticipationTotals)
			train.AddParticipation(percy.Participation{
				ChannelID: train.ChannelID,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				},
				Quantity:  int(finalLevel.Goal) + 1,
				Timestamp: time.Now(),
				User:      percy.NewAnonymousCheerer(),
			})

			So(train.Level(), ShouldEqual, finalLevel.Level)
			So(train.CompletedLevel(), ShouldEqual, finalLevel.Level)
		})
	})
}

func TestHypeTrain_IsActive(t *testing.T) {
	Convey("Given a hype train", t, func() {
		train := tests.ValidOngoingHypeTrain()

		Convey("Returns true when", func() {
			Convey("train is ongoing", func() {
				So(train.IsActive(), ShouldBeTrue)
			})
		})

		Convey("Returns false when", func() {
			Convey("train has ended", func() {
				train.EndedAt = pointers.TimeP(time.Now())
			})

			Convey("train has not started", func() {
				train.StartedAt = time.Now().Add(5 * time.Minute)
				train.ExpiresAt = time.Now().Add(5 * time.Minute)
			})

			So(train.IsActive(), ShouldBeFalse)
		})
	})
}

func TestHypeTrain_End(t *testing.T) {
	Convey("Given a ongoing hype train", t, func() {
		train := tests.ValidOngoingHypeTrain()
		endingReason := percy.EndingReasonExpired
		Convey("sets ending timestamp and ending reason", func() {
			err := train.End(percy.EndingReasonExpired)
			So(err, ShouldBeNil)

			So(train.IsActive(), ShouldBeFalse)

			So(train.EndedAt, ShouldNotBeNil)
			So(train.EndedAt.After(time.Now().Add(-1*time.Second)), ShouldBeTrue)

			So(train.EndingReason, ShouldNotBeNil)
			So(*train.EndingReason, ShouldEqual, endingReason)
		})
	})

	Convey("Given an inactive hype train", t, func() {
		train := tests.ValidHypeTrain()
		endingReason := percy.EndingReasonCompleted
		train.EndedAt = pointers.TimeP(time.Now().Add(-5 * time.Minute))
		train.EndingReason = &endingReason

		Convey("returns an error", func() {
			err := train.End(percy.EndingReasonCompleted)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestHypeTrain_ParticipationPoints(t *testing.T) {
	Convey("Given a hype train", t, func() {
		train := tests.ValidOngoingHypeTrain()

		Convey("with no participation", func() {
			train.Participations = make(percy.ParticipationTotals)

			Convey("should have 0 points", func() {
				So(train.ParticipationPoints(), ShouldBeZeroValue)
			})
		})

		Convey("with some participation", func() {
			train.Participations = map[percy.ParticipationIdentifier]int{
				{Source: percy.BitsSource, Action: percy.CheerAction}:          100, // 100
				{Source: percy.BitsSource, Action: percy.ExtensionAction}:      200, // 200
				{Source: percy.SubsSource, Action: percy.Tier1GiftedSubAction}: 10,  // 5000
				{Source: percy.SubsSource, Action: percy.Tier2SubAction}:       1,   // 1000
			}

			Convey("should include points for recognized participations", func() {
				So(train.ParticipationPoints(), ShouldEqual, 6300)
			})
		})

		Convey("with unrecognized participation identifier", func() {
			train.Participations = map[percy.ParticipationIdentifier]int{
				{Source: percy.BitsSource, Action: percy.CheerAction}:                                  100, // 100
				{Source: percy.BitsSource, Action: percy.ExtensionAction}:                              200, // 200
				{Source: percy.SubsSource, Action: percy.Tier1GiftedSubAction}:                         10,  // 5000
				{Source: percy.SubsSource, Action: percy.Tier2SubAction}:                               1,   // 1000
				{Source: percy.ParticipationSource("huh?"), Action: percy.ParticipationAction("wow?")}: 125,
			}

			Convey("should ignore points for unrecognized participations", func() {
				So(train.ParticipationPoints(), ShouldEqual, 6300)
			})
		})
	})
}

func TestEngine_GetMostRecentHypeTrain(t *testing.T) {
	Convey("Given an Engine", t, func() {
		hypeTrainDB := &internalfakes.FakeHypeTrainDB{}
		engine := percy.NewEngine(percy.EngineConfig{
			HypeTrainDB: hypeTrainDB,
		})

		ctx := context.Background()
		channelID := "1234567890"

		validHypeTrain := tests.ValidHypeTrain()
		invalidHypeTrain := tests.InvalidHypeTrain()

		Convey("when hype train DB retrieves the most-rencet valid hype train", func() {
			hypeTrainDB.GetMostRecentHypeTrainReturns(&validHypeTrain, nil)

			Convey("returns the valid hypeTrain", func() {
				hypeTrain, err := engine.GetMostRecentHypeTrain(ctx, channelID)

				So(err, ShouldBeNil)
				So(hypeTrain, ShouldResemble, &validHypeTrain)
			})
		})

		Convey("when hype train DB retrives no hype train", func() {
			hypeTrainDB.GetMostRecentHypeTrainReturns(nil, nil)

			Convey("returns the valid hypeTrain", func() {
				hypeTrain, err := engine.GetMostRecentHypeTrain(ctx, channelID)

				So(err, ShouldBeNil)
				So(hypeTrain, ShouldBeNil)
			})
		})

		Convey("returns an error when", func() {
			Convey("channel ID is blank", func() {
				channelID = ""
			})

			Convey("hypeTrain DB errors", func() {
				hypeTrainDB.GetMostRecentHypeTrainReturns(nil, errors.New("hypeTrain DB error"))
			})

			Convey("the found hype train isn't valid", func() {
				hypeTrainDB.GetMostRecentHypeTrainReturns(&invalidHypeTrain, nil)
			})

			_, err := engine.GetMostRecentHypeTrain(ctx, channelID)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestEngine_UpsertHypeTrain(t *testing.T) {
	Convey("Given an Engine", t, func() {
		hypeTrainDB := &internalfakes.FakeHypeTrainDB{}
		eventDB := &internalfakes.FakeEventDB{}
		engine := percy.NewEngine(percy.EngineConfig{
			HypeTrainDB: hypeTrainDB,
			EventDB:     eventDB,
		})
		ctx := context.Background()

		validHypeTrain := tests.ValidHypeTrain()
		invalidHypeTrain := tests.InvalidHypeTrain()

		eventDB.UpsertHypeTrainExpirationTimerReturns(nil)

		Convey("when hype train DB upserts a valid change", func() {
			hypeTrainDB.UpsertHypeTrainReturns(nil)

			Convey("returns no error after upsert", func() {
				err := engine.UpsertHypeTrain(ctx, validHypeTrain)
				So(err, ShouldBeNil)
			})
		})

		Convey("returns an error when", func() {
			err := engine.UpsertHypeTrain(ctx, invalidHypeTrain)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestEngine_CanStartHypeTrain(t *testing.T) {
	Convey("Given an Engine", t, func() {
		hypeTrainDB := &internalfakes.FakeHypeTrainDB{}
		hypeTrainConfigDB := &internalfakes.FakeHypeTrainConfigsDB{}
		eventDB := &internalfakes.FakeEventDB{}
		bitsPinataDB := &internalfakes.FakeBitsPinataDB{}
		engine := percy.NewEngine(percy.EngineConfig{
			HypeTrainDB:  hypeTrainDB,
			ConfigsDB:    hypeTrainConfigDB,
			EventDB:      eventDB,
			BitsPinataDB: bitsPinataDB,
		})
		ctx := context.Background()

		validHypeTrain := tests.ValidHypeTrain()
		invalidHypeTrain := tests.InvalidHypeTrain()
		ongoingHypeTrain := tests.ValidOngoingHypeTrain()
		validHypeTrainConfig := tests.ValidHypeTrainConfig()
		activeBitsPinata := tests.ValidActiveBitsPinata("123")

		eventDB.UpsertHypeTrainExpirationTimerReturns(nil)

		Convey("when channel has hype train enabled", func() {
			hypeTrainConfigDB.GetConfigReturns(&validHypeTrainConfig, nil)

			Convey("returns true if", func() {
				Convey("the previous hype train ended outside of the cooldown period", func() {
					validHypeTrain.EndedAt = pointers.TimeP(time.Now().Add(-1000 * time.Hour))
					hypeTrainDB.GetMostRecentHypeTrainReturns(&validHypeTrain, nil)
				})

				Convey("returns true if no hype train was ever ran", func() {
					hypeTrainDB.GetMostRecentHypeTrainReturns(nil, nil)
				})

				Convey("there is no active bits pinata", func() {
					bitsPinataDB.GetActiveBitsPinataReturns(nil, nil)
				})

				canStart, err := engine.CanStartHypeTrain(ctx, validHypeTrain.ChannelID)
				So(err, ShouldBeNil)
				So(canStart, ShouldBeTrue)
			})

			Convey("returns false if", func() {
				Convey("returns false if a hype train is ongoing", func() {
					hypeTrainDB.GetMostRecentHypeTrainReturns(&ongoingHypeTrain, nil)
				})

				Convey("returns false if currently in cooldown", func() {
					validHypeTrain.EndedAt = pointers.TimeP(time.Now().Add(-1 * time.Second))
					hypeTrainDB.GetMostRecentHypeTrainReturns(&validHypeTrain, nil)
				})

				Convey("returns false if bits pinata is active", func() {
					bitsPinataDB.GetActiveBitsPinataReturns(&activeBitsPinata, nil)
				})

				canStart, err := engine.CanStartHypeTrain(ctx, validHypeTrain.ChannelID)
				So(err, ShouldBeNil)
				So(canStart, ShouldBeFalse)
			})
		})

		Convey("when channel has hype train disabled", func() {
			validHypeTrainConfig.Enabled = false
			hypeTrainConfigDB.GetConfigReturns(&validHypeTrainConfig, nil)

			canStart, err := engine.CanStartHypeTrain(ctx, validHypeTrain.ChannelID)
			So(err, ShouldBeNil)
			So(canStart, ShouldBeFalse)
		})

		Convey("returns an error when", func() {
			Convey("channel ID is blank", func() {
				validHypeTrain.ChannelID = ""
			})

			Convey("hypeTrain DB errors", func() {
				hypeTrainDB.GetMostRecentHypeTrainReturns(nil, errors.New("hypeTrain DB error"))
			})

			Convey("the found hype train isn't valid", func() {
				hypeTrainDB.GetMostRecentHypeTrainReturns(&invalidHypeTrain, nil)
			})

			Convey("bitsPinata DB errors", func() {
				bitsPinataDB.GetActiveBitsPinataReturns(nil, errors.New("bitsPinata DB error"))
			})

			_, err := engine.CanStartHypeTrain(ctx, validHypeTrain.ChannelID)
			So(err, ShouldNotBeNil)
		})
	})
}
