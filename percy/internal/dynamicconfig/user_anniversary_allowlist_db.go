package dynamicconfig

import (
	"context"
	"time"

	"code.justin.tv/commerce/dynamicconfig"
	"code.justin.tv/commerce/logrus"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
)

type UserAnniversaryAllowlistDB struct {
	dynamicConfig *dynamicconfig.DynamicConfig
	statter       statsd.Statter
	poller        *dynamicconfig.Poller
}

const (
	userAnniversaryOverridesFile        = "user_anniversary_allowlist.json"
	userAnniversaryExperimentEnabledKey = "experiment-enabled"
	userAnniversaryAllowlistKey         = "allowlist"
)

func NewUserAnniversaryAllowlistDB(region string, endpoint string, bucket string, statter statsd.Statter) (*UserAnniversaryAllowlistDB, error) {
	// 10 second timeout for initialization of the dynamic configuration
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	s3JSONSource, err := dynamicconfig.NewS3JSONSource(region, endpoint, bucket, userAnniversaryOverridesFile)
	if err != nil {
		err = errors.Wrap(err, "initializing s3 dynamic config source")
		return nil, err
	}
	dynamicConfig, err := dynamicconfig.NewDynamicConfig(ctx, s3JSONSource, map[string]dynamicconfig.Transform{
		userAnniversaryExperimentEnabledKey: transformExperimentEnabled,
		userAnniversaryAllowlistKey:         transformAllowlistKey,
	})
	if err != nil {
		_ = statter.Inc("dynconfig.startup.failure", 1, 1.0)
		return nil, errors.Wrap(err, "initializing dynamic config values")
	}

	_ = statter.Inc("dynconfig.startup.success", 1, 1.0)

	logrus.Info("initialized dynamic config values")

	poller := dynamicconfig.Poller{
		Reloader: dynamicConfig,
		Interval: experimentsPollingInterval,
		Timeout:  experimentsPollingInterval,
	}

	go poller.Poll()

	return &UserAnniversaryAllowlistDB{
		dynamicConfig: dynamicConfig,
		statter:       statter,
		poller:        &poller,
	}, nil
}

func (d *UserAnniversaryAllowlistDB) Close() {
	d.poller.Stop()
}

func transformExperimentEnabled(key string, value interface{}) (interface{}, error) {
	b, ok := value.(bool)
	if !ok {
		return nil, errors.Errorf("value of %s must be a string", key)
	}
	return b, nil
}

func transformAllowlistKey(key string, value interface{}) (interface{}, error) {
	ids, ok := value.([]interface{})
	if !ok {
		return nil, errors.Errorf("value of %s must be an array", key)
	}

	allowlist := make(map[string]bool)
	for _, id := range ids {
		allowlistedID, ok := id.(string)
		if !ok {
			return nil, errors.Errorf("value of %s must be a string", id)
		}
		allowlist[allowlistedID] = true
	}

	logrus.Infof("transformAllowlist: loaded %d allowlisted IDs", len(allowlist))
	return allowlist, nil
}

// If this function returns true, the user always has a user anniversary
func (d *UserAnniversaryAllowlistDB) IsUserAnniversaryOverride(channelID string) bool {
	// check allowlist
	if allowlist, ok := d.dynamicConfig.Get(userAnniversaryAllowlistKey).(map[string]bool); ok {
		if _, isInAllowlist := allowlist[channelID]; isInAllowlist {
			return true
		}
	}
	return false
}

func (d *UserAnniversaryAllowlistDB) IsUserAnniversaryExperimentEnabled() bool {
	if isEnabled, ok := d.dynamicConfig.Get(userAnniversaryExperimentEnabledKey).(bool); ok {
		return isEnabled
	}
	return false
}
