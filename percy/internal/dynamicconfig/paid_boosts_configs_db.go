package dynamicconfig

import (
	"context"
	"encoding/json"
	"time"

	"code.justin.tv/commerce/dynamicconfig"
	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ssm"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
)

const (
	paidBoostsConfigKey       = "/paid-boosts/config"
	paidBoostsPollingInterval = 1 * time.Minute
)

type PaidBoostsConfigsDB struct {
	dynamicConfig *dynamicconfig.DynamicConfig
	statter       statsd.Statter
	poller        *dynamicconfig.Poller
}

type paidBoostsConfigJsonData struct {
	PaidBoostsAllowlist    []string                    `json:"paid_boosts_allowlist"`
	ChannelConfigOverrides []channelConfigOverrideData `json:"channel_config_overrides"`
}

type channelConfigOverrideData struct {
	ChannelID                      string `json:"channel_id"`
	CooldownHours                  int    `json:"cooldown_hours"`
	DurationMinutes                int    `json:"duration_minutes"`
	KickoffDelayMinMinutes         int    `json:"kickoff_delay_min_minutes"`
	KickoffDelayMaxMinutes         int    `json:"kickoff_delay_max_minutes"`
	SkipKickoffLimitCheck          bool   `json:"skip_kickoff_limit_check"`
	LaunchNoopBoost                bool   `json:"launch_noop_boost"`
	FailContributionRecordStep     bool   `json:"fail_contribution_record_step"`
	FailContributionUserNoticeStep bool   `json:"fail_contribution_user_notice_step"`
}

// unmarshalled version of paidBoostsConfigJsonData that uses map for easier lookups
type paidBoostsConfigData struct {
	PaidBoostsAllowlist    map[string]interface{}
	ChannelConfigOverrides map[string]channelConfigOverrideData
}

func NewPaidBoostsConfigsDB(region string, statter statsd.Statter) (*PaidBoostsConfigsDB, error) {
	// 10 second timeout for initialization of the dynamic configuration
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	sess, err := session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			Region: aws.String(region),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "error creating aws session")
	}

	ssmClient := ssm.New(sess)
	configKeys := []string{paidBoostsConfigKey}
	ssmSource := dynamicconfig.NewSSMSource(configKeys, ssmClient)

	dynamicConfig, err := dynamicconfig.NewDynamicConfig(ctx, ssmSource, map[string]dynamicconfig.Transform{
		paidBoostsConfigKey: transformPaidBoostsConfig,
	})
	if err != nil {
		_ = statter.Inc("dynconfig.startup.failure", 1, 1.0)
		return nil, errors.Wrap(err, "initializing dynamic config values")
	}

	_ = statter.Inc("dynconfig.startup.success", 1, 1.0)
	poller := dynamicconfig.Poller{
		Reloader: dynamicConfig,
		Interval: paidBoostsPollingInterval,
		Timeout:  paidBoostsPollingInterval,
	}

	go poller.Poll()

	return &PaidBoostsConfigsDB{
		dynamicConfig: dynamicConfig,
		statter:       statter,
		poller:        &poller,
	}, nil
}

func transformPaidBoostsConfig(key string, value interface{}) (interface{}, error) {
	str, ok := value.(string)
	if !ok {
		return nil, errors.New("failed to cast paid boosts config to string")
	}

	var data paidBoostsConfigJsonData
	err := json.Unmarshal([]byte(str), &data)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal paid boosts config data as json")
	}

	channelAllowlist := make(map[string]interface{})
	for _, id := range data.PaidBoostsAllowlist {
		channelAllowlist[id] = nil
	}

	channelConfigOverrides := make(map[string]channelConfigOverrideData)
	for _, override := range data.ChannelConfigOverrides {
		channelConfigOverrides[override.ChannelID] = override
	}

	return paidBoostsConfigData{
		PaidBoostsAllowlist:    channelAllowlist,
		ChannelConfigOverrides: channelConfigOverrides,
	}, nil
}

func (db *PaidBoostsConfigsDB) IsChannelAllowed(channelID string) bool {
	configRaw := db.dynamicConfig.Get(paidBoostsConfigKey)
	config, ok := configRaw.(paidBoostsConfigData)
	if !ok {
		logrus.WithField("rawString", configRaw).Error("Failed to unmarshal paid boosts config data")
		return false
	}

	_, isInAllowList := config.PaidBoostsAllowlist[channelID]
	return isInAllowList
}

func (db *PaidBoostsConfigsDB) GetBoostOpportunityConfig(channelID string) percy.BoostOpportunityConfig {
	if data, ok := db.dynamicConfig.Get(paidBoostsConfigKey).(paidBoostsConfigData); ok {
		if config, hasOverride := data.ChannelConfigOverrides[channelID]; hasOverride {
			return percy.BoostOpportunityConfig{
				Cooldown:                       time.Duration(config.CooldownHours) * time.Hour,
				Duration:                       time.Duration(config.DurationMinutes) * time.Minute,
				KickoffDelayMin:                time.Duration(config.KickoffDelayMinMinutes) * time.Minute,
				KickoffDelayMax:                time.Duration(config.KickoffDelayMaxMinutes) * time.Minute,
				SkipKickoffLimitCheck:          config.SkipKickoffLimitCheck,
				LaunchNoopBoost:                config.LaunchNoopBoost,
				FailContributionRecordStep:     config.FailContributionRecordStep,
				FailContributionUserNoticeStep: config.FailContributionUserNoticeStep,
			}
		}
	}

	return percy.DefaultBoostOpportunityConfig
}
