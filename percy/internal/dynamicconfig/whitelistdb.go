package dynamicconfig

import (
	"context"
	"time"

	"code.justin.tv/commerce/dynamicconfig"
	"code.justin.tv/commerce/logrus"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
)

type WhitelistDB struct {
	dynamicConfig *dynamicconfig.DynamicConfig
	statter       statsd.Statter
	poller        *dynamicconfig.Poller
}

const (
	experimentOverridesFile    = "celebrations_whitelist.json"
	experimentsPollingInterval = 5 * time.Minute
)

func NewWhitelistDB(region string, endpoint string, bucket string, statter statsd.Statter) (*WhitelistDB, error) {
	// 10 second timeout for initialization of the dynamic configuration
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	s3JSONSource, err := dynamicconfig.NewS3JSONSource(region, endpoint, bucket, experimentOverridesFile)
	if err != nil {
		err = errors.Wrap(err, "initializing s3 dynamic config source")
		return nil, err
	}

	dynamicConfig, err := dynamicconfig.NewDynamicConfig(ctx, s3JSONSource, nil)
	if err != nil {
		_ = statter.Inc("dynconfig.startup.failure", 1, 1.0)
		return nil, errors.Wrap(err, "initializing dynamic config values")
	}

	_ = statter.Inc("dynconfig.startup.success", 1, 1.0)

	logrus.Info("initialized dynamic config values")

	poller := dynamicconfig.Poller{
		Reloader: dynamicConfig,
		Interval: experimentsPollingInterval,
		Timeout:  experimentsPollingInterval,
	}

	go poller.Poll()

	return &WhitelistDB{
		dynamicConfig: dynamicConfig,
		statter:       statter,
		poller:        &poller,
	}, nil
}

func (d *WhitelistDB) Close() {
	d.poller.Stop()
}

func (d *WhitelistDB) HasCelebrations(channelID string) bool {
	if whitelistMap := d.dynamicConfig.Get(channelID); whitelistMap != nil {
		if value, ok := whitelistMap.(bool); ok {
			return value
		}
	}

	return false
}
