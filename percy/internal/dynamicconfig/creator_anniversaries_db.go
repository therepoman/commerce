package dynamicconfig

import (
	"context"
	"time"

	"code.justin.tv/commerce/dynamicconfig"
	percy "code.justin.tv/commerce/percy/internal"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

const (
	anniversariesFile                   = "creator_anniversaries.json"
	anniversariesByChannelIdKey         = "partner_anniversaries_by_channel_id"
	anniversariesPartnerDateKey         = "partner_anniversary_date"
	anniversariesAffiliateDateKey       = "affiliate_anniversary_date"
	creatorAnniversariesExperimentStart = "experimentStartDate"
	creatorAnniversariesExperimentEnd   = "experimentEndDate"
)

type CreatorAnniversariesDB struct {
	dynamicConfig *dynamicconfig.DynamicConfig
}

func NewCreatorAnniversariesDB(region string, endpoint string, bucket string) (*CreatorAnniversariesDB, error) {
	// 10 second timeout for initialization of the dynamic configuration
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	s3JSONSource, err := dynamicconfig.NewS3JSONSource(region, endpoint, bucket, anniversariesFile)
	if err != nil {
		err = errors.Wrap(err, "initializing s3 dynamic config source")
		return nil, err
	}

	dynamicConfig, err := dynamicconfig.NewDynamicConfig(ctx, s3JSONSource, map[string]dynamicconfig.Transform{
		anniversariesByChannelIdKey: transformCreatorAnniversariesConfig,
	})
	if err != nil {
		return nil, errors.Wrap(err, "initializing dynamic config")
	}

	return &CreatorAnniversariesDB{
		dynamicConfig: dynamicConfig,
	}, nil
}

func transformCreatorAnniversariesConfig(key string, value interface{}) (interface{}, error) {
	creatorAnniversariesObject, ok := value.(map[string]interface{})
	if !ok {
		return nil, errors.New("failed to cast creator anniversary config to string")
	}

	anniversariesByChannelId := make(map[string]percy.CreatorAnniversary)
	for channelId, dateObject := range creatorAnniversariesObject {
		if dateMap, ok := dateObject.(map[string]interface{}); ok {
			partnerDate := dateMap[anniversariesPartnerDateKey].(string)
			affiliateDate := dateMap[anniversariesAffiliateDateKey].(string)

			// Set anniversary to the partner date if it exists. Else, use the affiliate date.
			if partnerDate != "" {
				date, err := time.Parse(time.RFC3339, partnerDate)
				if err != nil {
					return nil, err
				}
				anniversariesByChannelId[channelId] = percy.CreatorAnniversary{
					Date:            date,
					CreatorCategory: percy.PartnerCategory,
				}
			} else if partnerDate == "" && affiliateDate != "" {
				date, err := time.Parse(time.RFC3339, affiliateDate)
				if err != nil {
					return nil, err
				}
				anniversariesByChannelId[channelId] = percy.CreatorAnniversary{
					Date:            date,
					CreatorCategory: percy.AffiliateCategory,
				}
			}
		}
	}

	return anniversariesByChannelId, nil
}

// Get config settings for experiment
func (d *CreatorAnniversariesDB) IsCreatorAnniversariesExperimentEnabled(t time.Time) bool {
	experimentMap := struct {
		startTime time.Time
		endTime   time.Time
	}{}
	experimentStartInterface := d.dynamicConfig.Get(creatorAnniversariesExperimentStart)
	if experimentStartInterface != nil {
		logrus.Info(experimentStartInterface)
		if dateString, ok := experimentStartInterface.(string); ok {
			logrus.Info(dateString)
			experimentStart, err := time.Parse(time.RFC3339, dateString)
			logrus.Info(experimentStart)
			if err != nil {
				return false
			}
			experimentMap.startTime = experimentStart
		}
	}

	experimentEndInterface := d.dynamicConfig.Get(creatorAnniversariesExperimentEnd)
	if experimentEndInterface != nil {
		logrus.Info(experimentEndInterface)
		if dateString, ok := experimentEndInterface.(string); ok {
			logrus.Info(dateString)
			experimentEnd, err := time.Parse(time.RFC3339, dateString)
			logrus.Info(experimentEnd)
			if err != nil {
				return false
			}
			experimentMap.endTime = experimentEnd
		}
	}

	if t.After(experimentMap.startTime) && t.Before(experimentMap.endTime) {
		return true
	}
	return false
}

// Returns all Partner and Affiliate Anniversaries
func (d *CreatorAnniversariesDB) GetCreatorAnniversaries() (map[string]percy.CreatorAnniversary, error) {
	if creatorAnniversariesMap, ok := d.dynamicConfig.Get(anniversariesByChannelIdKey).(map[string]percy.CreatorAnniversary); ok {
		return creatorAnniversariesMap, nil
	}
	return nil, nil
}
