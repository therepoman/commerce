package dynamicconfig

import (
	"context"
	"strconv"
	"time"

	"code.justin.tv/commerce/dynamicconfig"
	percy "code.justin.tv/commerce/percy/internal"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

const (
	foundersCelebrationConfigKey             = "founders_celebration_config.json"
	foundersCelebrationConfigStartDateKey    = "starte-date"
	foundersCelebrationConfigWhitelistKey    = "whitelistedIDs"
	foundersCelebrationConfigPollingInterval = 5 * time.Minute
)

type foundersCelebrationsExperimentDB struct {
	subscriptionsDB percy.SubscriptionsDB
	dynamicConfig   *dynamicconfig.DynamicConfig
	statter         statsd.Statter
	poller          *dynamicconfig.Poller
}

func NewFoundersCelebrationExperimentDB(region string, endpoint string, bucket string, subscriptionsDB percy.SubscriptionsDB, statter statsd.Statter) (*foundersCelebrationsExperimentDB, error) {
	// 10 second timeout for initialization of the dynamic configuration
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	s3JSONSource, err := dynamicconfig.NewS3JSONSource(region, endpoint, bucket, foundersCelebrationConfigKey)
	if err != nil {
		return nil, errors.Wrap(err, "initializing s3 dynamic config source")
	}

	dynamicConfig, err := dynamicconfig.NewDynamicConfig(ctx, s3JSONSource, map[string]dynamicconfig.Transform{
		foundersCelebrationConfigStartDateKey: transformDate,
		foundersCelebrationConfigWhitelistKey: transformWhitelistIDs,
	})
	if err != nil {
		_ = statter.Inc("dynconfig.startup.failure", 1, 1.0)
		return nil, errors.Wrap(err, "initializing dynamic config values")
	}

	_ = statter.Inc("dynconfig.startup.success", 1, 1.0)

	logrus.Info("initialized dynamic config values")

	poller := dynamicconfig.Poller{
		Reloader: dynamicConfig,
		Interval: foundersCelebrationConfigPollingInterval,
		Timeout:  10 * time.Second,
	}

	go poller.Poll()

	return &foundersCelebrationsExperimentDB{
		subscriptionsDB: subscriptionsDB,
		dynamicConfig:   dynamicConfig,
		statter:         statter,
		poller:          &poller,
	}, nil
}

func (db *foundersCelebrationsExperimentDB) HasFounderCelebrations(ctx context.Context, channelID string) bool {
	if len(channelID) == 0 {
		return false
	}

	// check whitelist
	if whitelist, ok := db.dynamicConfig.Get(foundersCelebrationConfigWhitelistKey).(map[string]bool); ok {
		if _, isInWhitelist := whitelist[channelID]; isInWhitelist {
			return true
		}
	}

	channelIDInt, err := strconv.Atoi(channelID)
	if err != nil {
		logrus.WithError(err).Error("Failed to parse channel ID as an integer")
		return false
	}

	// reject all odd channel IDs
	if channelIDInt%2 != 0 {
		return false
	}

	// get channel's onboarding date and compare with launch date
	channelOnboardDate, err := db.subscriptionsDB.GetChannelOnboardDate(ctx, channelID)
	if err != nil {
		logrus.WithError(err).Error("Failed to get channel onboard date from subscriptions DB")
		return false
	}

	if channelOnboardDate == nil {
		return false
	}

	if startDate, ok := db.dynamicConfig.Get(foundersCelebrationConfigStartDateKey).(time.Time); ok {
		return channelOnboardDate.After(startDate)
	}

	return false
}

func (db *foundersCelebrationsExperimentDB) Close() {
	db.poller.Stop()
}

func transformDate(key string, value interface{}) (interface{}, error) {
	str, ok := value.(string)
	if !ok {
		return nil, errors.Errorf("value of %s must be a string", key)
	}
	t, err := time.Parse(time.RFC3339, str)
	if err != nil {
		err = errors.Wrapf(err, "value of %s must be valid rfc3339", key)
		return nil, err
	}

	logrus.Infof("transformDate: loaded %+v", t)
	return t, nil
}

func transformWhitelistIDs(key string, value interface{}) (interface{}, error) {
	ids, ok := value.([]interface{})
	if !ok {
		return nil, errors.Errorf("value of %s must be an array", key)
	}

	whitelist := make(map[string]bool)
	for _, id := range ids {
		whitelistedID, ok := id.(string)
		if !ok {
			return nil, errors.Errorf("value of %s must be a string", id)
		}
		whitelist[whitelistedID] = true
	}

	logrus.Infof("transformWhiteListIDs: loaded %d whitelisted IDs", len(whitelist))
	return whitelist, nil
}
