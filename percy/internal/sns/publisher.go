package sns

import (
	"context"
	"encoding/json"
	"time"

	"code.justin.tv/commerce/gogogadget/aws/sns"
	gringotts "code.justin.tv/commerce/gringotts/sns"
	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/models"
)

type Publisher struct {
	snsClient                       sns.Client
	webhookSNSTopic                 string
	activityFeedSNSTopic            string
	celebrationActivityFeedSNSTopic string
	gringottsRevenueEventSNSTopic   string
}

func NewPublisher(cfg config.Config, snsClient sns.Client) *Publisher {
	return &Publisher{
		snsClient:                       snsClient,
		webhookSNSTopic:                 cfg.SNSTopics.EventsHistoryService,
		activityFeedSNSTopic:            cfg.SNSTopics.ActivityFeed,
		gringottsRevenueEventSNSTopic:   cfg.SNSTopics.GringottsRevenueEventSNSTopic,
		celebrationActivityFeedSNSTopic: cfg.SNSTopics.CelebrationsActivityFeed,
	}
}

func (p *Publisher) PublishWebhookEvent(ctx context.Context, hypeTrain percy.HypeTrain, lastParticipation percy.Participation) error {
	var topContributions []models.Contribution
	for source, conductor := range hypeTrain.Conductors {
		conductorTotal := 0
		for participationIdentifier, quantity := range conductor.Participations {
			if participationIdentifier.Source == source {
				conductorTotal += int(hypeTrain.Config.EquivalentParticipationPoint(participationIdentifier, quantity))
			}
		}
		topContributions = append(topContributions, models.Contribution{
			Type:  string(source),
			User:  conductor.User.ID(),
			Total: conductorTotal,
		})
	}

	lastContribution := models.Contribution{
		Type:  string(lastParticipation.ParticipationIdentifier.Source),
		User:  lastParticipation.User.ID(),
		Total: int(hypeTrain.Config.EquivalentParticipationPoint(lastParticipation.ParticipationIdentifier, lastParticipation.Quantity)),
	}

	event := models.EHSEvent{
		BroadcasterID:    hypeTrain.ChannelID,
		HypeTrainID:      hypeTrain.ID,
		StartedAt:        hypeTrain.StartedAt.Truncate(time.Second),
		ExpiresAt:        hypeTrain.ExpiresAt.Truncate(time.Second),
		CoolDownEndTime:  hypeTrain.ExpiresAt.Add(hypeTrain.Config.CooldownDuration).Truncate(time.Second),
		Level:            hypeTrain.Level(),
		Goal:             int(hypeTrain.LevelProgress().Goal),
		Total:            int(hypeTrain.LevelProgress().Progression),
		TopContributions: topContributions,
		LastContribution: lastContribution,
	}
	eventJson, err := json.Marshal(&event)
	if err != nil {
		return err
	}

	return p.snsClient.Publish(ctx, p.webhookSNSTopic, string(eventJson))
}

func (p *Publisher) PublishActivityFeedEvent(ctx context.Context, hypeTrain percy.HypeTrain, eventType models.ActivityFeedEventType) error {
	activityFeedEvent := models.ActivityFeedEvent{
		ChannelID:   hypeTrain.ChannelID,
		HypeTrainID: hypeTrain.ID,
		EventType:   string(eventType),
	}

	if eventType == models.HypeTrainEnded {
		activityFeedEvent.CompletedLevel = hypeTrain.CompletedLevel()
		for participationIdentifier, participationTotal := range hypeTrain.Participations {
			if participationIdentifier.Source == percy.SubsSource {
				activityFeedEvent.TotalSubs += participationTotal
			} else if participationIdentifier.Source == percy.BitsSource {
				activityFeedEvent.TotalBits += participationTotal
			}
		}
	}

	activityFeedEventJson, err := json.Marshal(&activityFeedEvent)
	if err != nil {
		return err
	}

	return p.snsClient.Publish(ctx, p.activityFeedSNSTopic, string(activityFeedEventJson))
}

func (p *Publisher) PublishRevenueShareEvent(ctx context.Context, args percy.CreatorRevenueShareArgs) error {
	event := gringotts.RevenueEventMessage{
		ChannelId:       args.RecipientUserID,
		TimeOfEvent:     args.TimeOfEvent,
		TransactionId:   args.TransactionID,
		TransactionType: string(args.TransactionType),
		RevenueSource:   string(args.RevenueSource),
		Revenue:         float64(args.PayoutCents),
		ParticipantRole: string(args.PartnerType),
		SKU:             args.OfferID,
	}

	eventJson, err := json.Marshal(&event)
	if err != nil {
		return err
	}

	return p.snsClient.Publish(ctx, p.gringottsRevenueEventSNSTopic, string(eventJson))
}

func (p *Publisher) PublishCelebrationActivityFeedEvent(ctx context.Context, event models.CelebrationPurchaseActivityFeedEvent) error {
	eventJson, err := json.Marshal(&event)
	if err != nil {
		return err
	}

	return p.snsClient.Publish(ctx, p.celebrationActivityFeedSNSTopic, string(eventJson))
}
