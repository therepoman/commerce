package sns

import (
	"context"
	"errors"
	"testing"
	"time"

	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/sns/snsfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	"code.justin.tv/commerce/percy/models"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPublisher_PublishWebhook(t *testing.T) {
	Convey("Given SNS publisher", t, func() {
		snsClient := &snsfakes.FakeClient{}
		publisher := NewPublisher(config.Config{}, snsClient)
		ctx := context.Background()
		testParticipations := []percy.Participation{
			{
				ChannelID: tests.ChannelID,
				Timestamp: time.Now(),
				User: percy.User{
					Id: tests.UserID,
				},
				Quantity: 100,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				},
			},
			{
				ChannelID: tests.ChannelID,
				Timestamp: time.Now(),
				User: percy.User{
					Id: tests.UserID,
				},
				Quantity: 30,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.ExtensionAction,
				},
			},
			{
				ChannelID: tests.ChannelID,
				Timestamp: time.Now(),
				User: percy.User{
					Id: tests.UserID,
				},
				Quantity: 20,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.PollAction,
				},
			},
			{
				ChannelID: tests.ChannelID,
				Timestamp: time.Now(),
				User: percy.User{
					Id: tests.UserID,
				},
				Quantity: 1,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.SubsSource,
					Action: percy.Tier1GiftedSubAction,
				},
			},
			{
				ChannelID: tests.ChannelID,
				Timestamp: time.Now(),
				User: percy.User{
					Id: tests.UserID,
				},
				Quantity: 1,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.SubsSource,
					Action: percy.Tier2GiftedSubAction,
				},
			},
			{
				ChannelID: tests.ChannelID,
				Timestamp: time.Now(),
				User: percy.User{
					Id: tests.UserID,
				},
				Quantity: 1,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.SubsSource,
					Action: percy.Tier3GiftedSubAction,
				},
			},
		}
		testHypeTrain := tests.ValidHypeTrain()

		Convey("PublishWebhook returns error when", func() {
			Convey("SNS client fails", func() {
				snsClient.PublishReturns(errors.New("meh"))
			})

			err := publisher.PublishWebhookEvent(ctx, testHypeTrain, testParticipations[0])
			So(err, ShouldNotBeNil)
		})

		Convey("PublishWebhook succeeds", func() {
			snsClient.PublishReturns(nil)

			err := publisher.PublishWebhookEvent(ctx, testHypeTrain, testParticipations[0])
			So(err, ShouldBeNil)
		})
	})
}

func TestPublisher_PublishActivityFeed(t *testing.T) {
	Convey("Given SNS Publisher", t, func() {
		snsClient := &snsfakes.FakeClient{}
		publisher := NewPublisher(config.Config{}, snsClient)
		ctx := context.Background()
		testHypeTrain := tests.ValidHypeTrain()

		Convey("PublishActivityFeed returns error when", func() {
			Convey("SNS client fails", func() {
				snsClient.PublishReturns(errors.New("meh"))

				err := publisher.PublishActivityFeedEvent(ctx, testHypeTrain, models.HypeTrainStarted)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("PublishActivityFeed succeeds", func() {
			snsClient.PublishReturns(nil)

			err := publisher.PublishActivityFeedEvent(ctx, testHypeTrain, models.HypeTrainEnded)
			So(err, ShouldBeNil)
		})
	})
}

func TestPublisher_PublishRevenueShareEvent(t *testing.T) {
	Convey("Given SNS Publisher", t, func() {
		snsClient := &snsfakes.FakeClient{}
		publisher := NewPublisher(config.Config{}, snsClient)
		ctx := context.Background()
		testRevenueEvent := percy.CreatorRevenueShareArgs{}

		Convey("PublishRevenueShareEvent returns error when", func() {
			Convey("SNS client fails", func() {
				snsClient.PublishReturns(errors.New("error"))

				err := publisher.PublishRevenueShareEvent(ctx, testRevenueEvent)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("PublishRevenueShareEvent succeeds", func() {
			snsClient.PublishReturns(nil)

			err := publisher.PublishRevenueShareEvent(ctx, testRevenueEvent)
			So(err, ShouldBeNil)
		})
	})
}
