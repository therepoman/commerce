package percy

import (
	"context"
	"time"

	"code.justin.tv/commerce/percy/internal/metrics"
	"code.justin.tv/commerce/percy/internal/miniexperiments"
	"github.com/cactus/go-statsd-client/statsd"
	"golang.org/x/time/rate"
)

//go:generate counterfeiter . Engine

type Engine interface {
	GetHypeTrainConfig(ctx context.Context, channelID string) (HypeTrainConfig, error)
	UpdateHypeTrainConfig(ctx context.Context, channelID string, updates HypeTrainConfigUpdate) (HypeTrainConfig, error)
	UnsetHypeTrainConfig(ctx context.Context, channelID string, update HypeTrainConfigUnset) (HypeTrainConfig, error)

	GetOngoingHypeTrain(ctx context.Context, channelID string) (*HypeTrain, *Approaching, error)
	GetMostRecentHypeTrain(ctx context.Context, channelID string) (*HypeTrain, error)
	GetHypeTrainByID(ctx context.Context, hypeTrainID string) (*HypeTrain, error)

	UpsertHypeTrain(ctx context.Context, hypeTrain HypeTrain) error
	CanStartHypeTrain(ctx context.Context, channelID string) (bool, error)
	EndHypeTrain(ctx context.Context, hypeTrain HypeTrain) error

	RecordParticipation(ctx context.Context, participation Participation) error

	EntitleParticipants(ctx context.Context, hypeTrain HypeTrain, participants ...Participant) (EntitledParticipants, error)

	ScheduleHypeTrainEnding(ctx context.Context, hypeTrain HypeTrain) error

	GetBadges(ctx context.Context, userID, channelID string) ([]Badge, error)
	SetBadges(ctx context.Context, userID, channelID string, badges []Badge) error

	GetParticipations(ctx context.Context, channelID string, params ParticipationQueryParams) ([]Participation, error)

	AdminGrantHypeTrainRewardsByHypeTrainID(ctx context.Context, channelID, hypeTrainID, userID string) (GrantHypeTrainRewardsResult, error)
	AdminGrantHypeTrainRewardsByParticipationTime(ctx context.Context, channelID, userID string, participatedAt time.Time) (GrantHypeTrainRewardsResult, error)
}

type engine struct {
	ApproachingDB               ApproachingDB
	BitsPinataDB                BitsPinataDB
	ConfigsDB                   HypeTrainConfigsDB
	HypeTrainDB                 HypeTrainDB
	EntitlementDB               EntitlementDB
	ParticipantDB               ParticipantDB
	BadgesDB                    BadgesDB
	EventDB                     EventDB
	ConductorsDB                ConductorsDB
	Leaderboard                 Leaderboard
	Statter                     statsd.Statter
	Publisher                   Publisher
	Spade                       Spade
	ConductorBadgesHandler      ConductorBadgesHandler
	DynamicTag                  DynamicTag
	SNSPublisher                SNSPublisher
	ParticipationDB             ParticipationDB
	UserProfileDB               UserProfileDB
	Personalization             PersonalizationClient
	StalledHypeTrainRateLimiter *rate.Limiter
	BitsPinataExperimentClient  miniexperiments.Client
	ApproachingExperimentClient miniexperiments.Client
}

type EngineConfig struct {
	ApproachingDB               ApproachingDB
	BitsPinataDB                BitsPinataDB
	ConfigsDB                   HypeTrainConfigsDB
	HypeTrainDB                 HypeTrainDB
	EntitlementDB               EntitlementDB
	ParticipantDB               ParticipantDB
	BadgesDB                    BadgesDB
	EventDB                     EventDB
	ConductorsDB                ConductorsDB
	Leaderboard                 Leaderboard
	Statter                     statsd.Statter
	Publisher                   Publisher
	Spade                       Spade
	ConductorBadgesHandler      ConductorBadgesHandler
	DynamicTag                  DynamicTag
	SNSPublisher                SNSPublisher
	ParticipationDB             ParticipationDB
	UserProfileDB               UserProfileDB
	Personalization             PersonalizationClient
	BitsPinataExperimentClient  miniexperiments.Client
	ApproachingExperimentClient miniexperiments.Client
}

// NewEngine ...
func NewEngine(config EngineConfig) *engine {
	statter := config.Statter
	if statter == nil {
		statter = &metrics.NoopStatter{}
	}
	return &engine{
		ApproachingDB:          config.ApproachingDB,
		BitsPinataDB:           config.BitsPinataDB,
		ConfigsDB:              config.ConfigsDB,
		HypeTrainDB:            config.HypeTrainDB,
		EntitlementDB:          config.EntitlementDB,
		ParticipantDB:          config.ParticipantDB,
		BadgesDB:               config.BadgesDB,
		EventDB:                config.EventDB,
		ConductorsDB:           config.ConductorsDB,
		Leaderboard:            config.Leaderboard,
		Statter:                statter,
		Publisher:              config.Publisher,
		Spade:                  config.Spade,
		ConductorBadgesHandler: config.ConductorBadgesHandler,
		DynamicTag:             config.DynamicTag,
		SNSPublisher:           config.SNSPublisher,
		ParticipationDB:        config.ParticipationDB,
		UserProfileDB:          config.UserProfileDB,
		Personalization:        config.Personalization,
		// this rate limiter to prevent Percy from furiously trying to fix stalled hype train in a large dependency outage
		StalledHypeTrainRateLimiter: rate.NewLimiter(1, 1),
		ApproachingExperimentClient: config.ApproachingExperimentClient,
		BitsPinataExperimentClient:  config.BitsPinataExperimentClient,
	}
}
