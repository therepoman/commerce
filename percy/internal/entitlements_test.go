package percy_test

import (
	"context"
	"errors"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	. "github.com/smartystreets/goconvey/convey"
)

func TestEngine_EntitleParticipants(t *testing.T) {
	Convey("Given an Engine", t, func() {
		hypeTrainDB := &internalfakes.FakeHypeTrainDB{}
		participantDB := &internalfakes.FakeParticipantDB{}
		entitlementDB := &internalfakes.FakeEntitlementDB{}
		spade := &internalfakes.FakeSpade{}
		engine := percy.NewEngine(percy.EngineConfig{
			HypeTrainDB:   hypeTrainDB,
			ParticipantDB: participantDB,
			EntitlementDB: entitlementDB,
			Spade:         spade,
		})

		ctx := context.Background()

		validHypeTrain := tests.ValidHypeTrain()
		validParticipation := tests.ValidParticipation()
		validParticipation.Quantity = 3
		validHypeTrain.AddParticipation(validParticipation)

		validParticipants := tests.ValidParticipants()
		validEmoteEntitlements := map[string]string{"1A": "1A", "1B": "1B"}

		spade.SendRewardItemReceiveEventsReturns(nil)

		Convey("when there is a valid hype train, valid participants and an error getting their entitlements", func() {
			entitlementDB.ReadEmoteEntitlementsReturns(nil, errors.New("entitlementDB error"))

			_, err := engine.EntitleParticipants(ctx, validHypeTrain, validParticipants...)
			So(err, ShouldNotBeNil)
		})

		Convey("when there is a valid hype train, valid participants, multiple entitlements and create entitlements fails", func() {
			entitlementDB.ReadEmoteEntitlementsReturns(validEmoteEntitlements, nil)
			entitlementDB.CreateEmoteEntitlementReturns(errors.New("entitlementDB error"))

			_, err := engine.EntitleParticipants(ctx, validHypeTrain, validParticipants...)
			So(err, ShouldNotBeNil)
		})

		Convey("when there is a valid hype train, valid participants, no entitlements and create entitlements succeeds", func() {
			entitlementDB.ReadEmoteEntitlementsReturns(nil, nil)
			entitlementDB.CreateEmoteEntitlementReturns(nil)

			entitledParticipants, err := engine.EntitleParticipants(ctx, validHypeTrain, validParticipants...)
			So(err, ShouldBeNil)
			So(entitledParticipants, ShouldNotBeNil)
			So(entitledParticipants, ShouldHaveLength, validParticipation.Quantity)
			for _, rewards := range entitledParticipants {
				So(rewards, ShouldHaveLength, 1)
			}
		})

		Convey("when there is a valid hype train, valid participants, multiple valid entitlements and create entitlements succeeds", func() {
			entitlementDB.ReadEmoteEntitlementsReturns(validEmoteEntitlements, nil)
			entitlementDB.CreateEmoteEntitlementReturns(nil)

			entitledParticipants, err := engine.EntitleParticipants(ctx, validHypeTrain, validParticipants...)
			So(err, ShouldBeNil)
			So(entitledParticipants, ShouldNotBeNil)
			for _, rewards := range entitledParticipants {
				So(rewards, ShouldHaveLength, 1)
			}
		})

		Convey("when there is a valid hype train, valid participants, valid conductor rewards, multiple valid entitlements and create entitlements succeeds", func() {
			entitlementDB.ReadEmoteEntitlementsReturns(validEmoteEntitlements, nil)
			entitlementDB.CreateEmoteEntitlementReturns(nil)
			validHypeTrain.Config.ConductorRewards = tests.ValidConductorRewards()
			for _, conductor := range validHypeTrain.Conductors {
				validParticipants = append(validParticipants, percy.Participant{
					ChannelID:   validHypeTrain.ChannelID,
					User:        conductor.User,
					HypeTrainID: validHypeTrain.ID,
					ParticipationTotals: map[percy.ParticipationIdentifier]int{
						{
							Source: percy.BitsSource,
							Action: percy.CheerAction,
						}: 100,
						{
							Source: percy.SubsSource,
							Action: percy.Tier1SubAction,
						}: 1,
					},
				})
			}

			entitledParticipants, err := engine.EntitleParticipants(ctx, validHypeTrain, validParticipants...)
			So(err, ShouldBeNil)
			So(entitledParticipants, ShouldNotBeNil)
			for userID, rewards := range entitledParticipants {
				isConductor := false
				for _, conductor := range validHypeTrain.Conductors {
					if conductor.User.ID() == userID {
						isConductor = true
					}
				}
				if isConductor {
					So(rewards, ShouldHaveLength, 2)
				} else {
					So(rewards, ShouldHaveLength, 1)
				}
			}
		})
	})
}
