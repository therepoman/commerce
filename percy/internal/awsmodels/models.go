package awsmodels

// BoostOpportunityFulfillmentInput is used throughout opportunity purchases (Lambdas, SFNs etc)
type BoostOpportunityFulfillmentInput struct {
	ChannelID       string `json:"channel_id"`
	UserID          string `json:"user_id"`
	OriginID        string `json:"origin_id"`
	Units           int    `json:"units"`
	UnixTimeSeconds int64  `json:"unix_time_seconds"`
}
