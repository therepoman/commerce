package percy

import (
	"context"
	"time"

	"code.justin.tv/commerce/logrus"
)

const (
	CelebrationOverlayPubsubTopic = "celebration-events-v1."
)

// Spade event - celebration overlay initiated
// This event is triggered every time a
// celebration is started
type CelebrationOverlayInitiatedEventParams struct {
	ServerTime         string          `json:"server_time"`
	ChannelID          string          `json:"channel_id"`
	UserID             string          `json:"user_id"`
	CelebrationID      string          `json:"celebration_id"`
	NumPubsubListeners int             `json:"num_pubsub_listeners"`
	TransactionID      string          `json:"transaction_id"`
	TransactionProduct string          `json:"transaction_product"`
	Intensity          CelebrationSize `json:"intensity,omitempty"`
	GrossAmount        int64           `json:"gross_amount,omitempty"`
}

// Spade event - celebration overlay status
// This event is triggered every time a
// channel changes its celebration enabled status
type CelebrationOverlayStatusEventParams struct {
	ServerTime string `json:"server_time"`
	ChannelID  string `json:"channel_id"`
	SendType   string `json:"send_type"`
}

// Spade event - celebration overlay status
// This event is triggered every time a
// channel changes its celebration settings
type CelebrationOverlaySettingsEventParams struct {
	ServerTime    string `json:"server_time"`
	ChannelID     string `json:"channel_id"`
	CelebrationID string `json:"celebration_id"`
	SendType      string `json:"send_type"`
	Status        string `json:"status"`
	Prompt        string `json:"prompt"`
	Threshold     string `json:"threshold"`
	Effect        string `json:"effect"`
	Area          string `json:"area"`
	Duration      int    `json:"duration"`
	Intensity     int    `json:"intensity"`
}

type CelebrationChannelPurchasableConfigChangeEventParams struct {
	ServerTime          time.Time `json:"server_time"`
	ChannelID           string    `json:"channel_id"`
	EventID             string    `json:"event_id"`
	StateChangeType     string    `json:"state_change_type"`
	StoreType           string    `json:"store_type"`
	SmallIsDisabledOld  bool      `json:"small_is_disabled_old"`
	SmallIsDisabledNew  bool      `json:"small_is_disabled_new"`
	SmallOfferIDOld     string    `json:"small_offer_id_old"`
	SmallOfferIDNew     string    `json:"small_offer_id_new"`
	MediumIsDisabledOld bool      `json:"medium_is_disabled_old"`
	MediumIsDisabledNew bool      `json:"medium_is_disabled_new"`
	MediumOfferIDOld    string    `json:"medium_offer_id_old"`
	MediumOfferIDNew    string    `json:"medium_offer_id_new"`
	LargeIsDisabledOld  bool      `json:"large_is_disabled_old"`
	LargeIsDisabledNew  bool      `json:"large_is_disabled_new"`
	LargeOfferIDOld     string    `json:"large_offer_id_old"`
	LargeOfferIDNew     string    `json:"large_offer_id_new"`
}

func (eng *celebrationEngine) SendCelebrationOverlayInitiatedEvent(channelID, userID, celebrationID, eventType, transactionID string) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	numListeners, err := eng.Publisher.GetNumberOfPubsubListeners(CelebrationOverlayPubsubTopic, channelID)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"channelID":     channelID,
			"UserID":        userID,
			"type":          eventType,
			"TransactionID": transactionID,
		}).Error("Failed to get pubsub listeners")
	} else {
		if err := eng.Spade.SendCelebrationOverlayInitiatedEvent(ctx, CelebrationOverlayInitiatedEventParams{
			ServerTime:         time.Now().UTC().Format(time.RFC3339),
			ChannelID:          channelID,
			UserID:             userID,
			NumPubsubListeners: numListeners,
			CelebrationID:      celebrationID,
			TransactionID:      transactionID,
			TransactionProduct: eventType,
		}); err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{
				"channelID":     channelID,
				"UserID":        userID,
				"type":          eventType,
				"TransactionID": transactionID,
			}).Error("Failed to publish celebration intitiated event to spade")
		}
	}
}
