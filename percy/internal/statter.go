package percy

import "time"

type Statter interface {
	Inc(name string, value int64)
	Duration(name string, dur time.Duration)
}

type NoopStatter struct{}

func (s *NoopStatter) Inc(name string, value int64) error {
	return nil
}

func (s *NoopStatter) Duration(name string, dur time.Duration) error {
	return nil
}
