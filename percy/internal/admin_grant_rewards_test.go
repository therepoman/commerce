package percy_test

import (
	"context"
	"errors"
	"testing"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func TestEngine_AdminGrantHypeTrainRewardsByParticipationTime(t *testing.T) {
	Convey("Given an engine", t, func() {
		channelID := uuid.New().String()
		userID := uuid.New().String()
		participatedAt := time.Now()

		badgesDB := &internalfakes.FakeBadgesDB{}
		hypeTrainDB := &internalfakes.FakeHypeTrainDB{}
		participantDB := &internalfakes.FakeParticipantDB{}
		entitlementsDB := &internalfakes.FakeEntitlementDB{}

		engine := percy.NewEngine(percy.EngineConfig{
			BadgesDB:      badgesDB,
			HypeTrainDB:   hypeTrainDB,
			ParticipantDB: participantDB,
			EntitlementDB: entitlementsDB,
		})

		Convey("returns hype train and granted rewards", func() {
			validHypeTrain := tests.ValidHypeTrain()
			hypeTrainDB.QueryHypeTrainByStartTimeReturns([]percy.HypeTrain{validHypeTrain}, nil)
			entitlementsDB.ReadEmoteEntitlementsReturns(map[string]string{
				"12345": "12345",
			}, nil)
			entitlementsDB.CreateEmoteEntitlementReturns(nil)

			Convey("when participant info isn't found", func() {
				participantDB.GetHypeTrainParticipantReturns(nil, nil)

				result, err := engine.AdminGrantHypeTrainRewardsByParticipationTime(context.Background(), channelID, userID, participatedAt)
				So(err, ShouldBeNil)
				So(result.HypeTrain, ShouldNotBeNil)
				So(result.GrantedRewards, ShouldNotBeEmpty)
				So(result.UserOwnedRewards, ShouldNotBeEmpty)
				So(result.Participant.ParticipationTotals, ShouldNotBeEmpty)
			})

			Convey("when rewards were already granted to user", func() {
				participant := tests.ValidParticipant()
				participant.Rewards = []percy.Reward{
					{
						ID:   uuid.New().String(),
						Type: percy.EmoteReward,
					},
				}
				participantDB.GetHypeTrainParticipantReturns(&participant, nil)
				entitlementsDB.ReadEmoteEntitlementsReturns(map[string]string{
					participant.Rewards[0].ID: participant.Rewards[0].ID,
				}, nil)
				badgesDB.GetBadgesReturns([]percy.Badge{"current_conductor"}, nil)

				result, err := engine.AdminGrantHypeTrainRewardsByParticipationTime(context.Background(), channelID, userID, participatedAt)
				So(err, ShouldBeNil)
				So(result.HypeTrain, ShouldNotBeNil)
				So(result.GrantedRewards, ShouldHaveLength, 2)
				So(result.GrantedRewards, ShouldContain, participant.Rewards[0])
				So(result.UserOwnedRewards, ShouldNotBeEmpty)
				So(result.Participant.ParticipationTotals, ShouldNotBeEmpty)

				So(entitlementsDB.CreateEmoteEntitlementCallCount(), ShouldEqual, 0)
				So(badgesDB.SetBadgesCallCount(), ShouldEqual, 0)
			})

			Convey("when rewards were recorded in participant DB but do not exist in entitlement and badges DB", func() {
				participant := tests.ValidParticipant()
				participant.Rewards = []percy.Reward{
					{
						ID:   uuid.New().String(),
						Type: percy.EmoteReward,
					},
				}
				participantDB.GetHypeTrainParticipantReturns(&participant, nil)

				result, err := engine.AdminGrantHypeTrainRewardsByParticipationTime(context.Background(), channelID, userID, participatedAt)
				So(err, ShouldBeNil)
				So(result.HypeTrain, ShouldNotBeNil)
				So(result.GrantedRewards, ShouldNotBeEmpty)
				So(result.UserOwnedRewards, ShouldNotBeEmpty)

				So(entitlementsDB.CreateEmoteEntitlementCallCount(), ShouldEqual, 1)
				So(badgesDB.SetBadgesCallCount(), ShouldEqual, 1)
			})

			Convey("when no reward was granted to user previously", func() {
				participant := tests.ValidParticipant()
				participant.Rewards = []percy.Reward{}
				participantDB.GetHypeTrainParticipantReturns(&participant, nil)

				result, err := engine.AdminGrantHypeTrainRewardsByParticipationTime(context.Background(), channelID, userID, participatedAt)
				So(err, ShouldBeNil)
				So(result.HypeTrain, ShouldNotBeNil)
				So(result.GrantedRewards, ShouldNotBeEmpty)
				So(result.UserOwnedRewards, ShouldNotBeEmpty)

				So(entitlementsDB.CreateEmoteEntitlementCallCount(), ShouldEqual, 1)
				So(badgesDB.SetBadgesCallCount(), ShouldEqual, 1)
			})
		})

		Convey("returns nothing", func() {
			Convey("if no hype train is found", func() {
				hypeTrainDB.QueryHypeTrainByStartTimeReturns(nil, nil)

				result, err := engine.AdminGrantHypeTrainRewardsByParticipationTime(context.Background(), channelID, userID, participatedAt)
				So(err, ShouldBeNil)
				So(result.HypeTrain, ShouldBeNil)
				So(result.GrantedRewards, ShouldBeEmpty)
				So(result.UserOwnedRewards, ShouldBeEmpty)
			})
		})

		Convey("returns an error", func() {
			hypeTrain := tests.ValidHypeTrain()
			participant := tests.ValidParticipant()

			// happy path behaviors
			hypeTrainDB.QueryHypeTrainByStartTimeReturns([]percy.HypeTrain{hypeTrain}, nil)
			participantDB.GetHypeTrainParticipantReturns(&participant, nil)

			Convey("if channel ID is missing", func() {
				channelID = ""
			})

			Convey("if participated timestamp is zero", func() {
				var zeroTime time.Time
				participatedAt = zeroTime
			})

			Convey("if user ID is missing", func() {
				userID = ""
			})

			Convey("if hype train DB fails", func() {
				hypeTrainDB.QueryHypeTrainByStartTimeReturns(nil, errors.New("can't find"))
			})

			Convey("if participant DB fails to get participant", func() {
				participantDB.GetHypeTrainParticipantReturns(nil, errors.New("can't get"))
			})

			Convey("if participant DB fails to update participant", func() {
				participantDB.PutHypeTrainParticipantsReturns(errors.New("can't put"))
			})

			Convey("if entitlement DB fails to read entitlements", func() {
				entitlementsDB.ReadEmoteEntitlementsReturns(nil, errors.New("can't read"))
			})

			Convey("if entitlement DB fails to entitle", func() {
				entitlementsDB.CreateEmoteEntitlementReturns(errors.New("can't create"))
			})

			_, err := engine.AdminGrantHypeTrainRewardsByParticipationTime(context.Background(), channelID, userID, participatedAt)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestEngine_AdminGrantHypeTrainRewardsByHypeTrainID(t *testing.T) {
	Convey("Given an engine", t, func() {
		channelID := uuid.New().String()
		hypeTrainID := uuid.New().String()
		userID := uuid.New().String()

		badgesDB := &internalfakes.FakeBadgesDB{}
		hypeTrainDB := &internalfakes.FakeHypeTrainDB{}
		participantDB := &internalfakes.FakeParticipantDB{}
		entitlementsDB := &internalfakes.FakeEntitlementDB{}

		engine := percy.NewEngine(percy.EngineConfig{
			BadgesDB:      badgesDB,
			HypeTrainDB:   hypeTrainDB,
			ParticipantDB: participantDB,
			EntitlementDB: entitlementsDB,
		})

		Convey("returns hype train and granted rewards", func() {
			validHypeTrain := tests.ValidHypeTrain()
			hypeTrainDB.GetHypeTrainByIDReturns(&validHypeTrain, nil)
			entitlementsDB.ReadEmoteEntitlementsReturns(map[string]string{
				"12345": "12345",
			}, nil)
			entitlementsDB.CreateEmoteEntitlementReturns(nil)

			Convey("when participant info isn't found", func() {
				participantDB.GetHypeTrainParticipantReturns(nil, nil)

				result, err := engine.AdminGrantHypeTrainRewardsByHypeTrainID(context.Background(), channelID, userID, hypeTrainID)
				So(err, ShouldBeNil)
				So(result.HypeTrain, ShouldNotBeNil)
				So(result.GrantedRewards, ShouldNotBeEmpty)
				So(result.UserOwnedRewards, ShouldNotBeEmpty)
			})

			Convey("when rewards were already granted to user", func() {
				participant := tests.ValidParticipant()
				participant.Rewards = []percy.Reward{
					{
						ID:   uuid.New().String(),
						Type: percy.EmoteReward,
					},
				}
				participantDB.GetHypeTrainParticipantReturns(&participant, nil)
				entitlementsDB.ReadEmoteEntitlementsReturns(map[string]string{
					participant.Rewards[0].ID: participant.Rewards[0].ID,
				}, nil)
				badgesDB.GetBadgesReturns([]percy.Badge{"current_conductor"}, nil)

				result, err := engine.AdminGrantHypeTrainRewardsByHypeTrainID(context.Background(), channelID, userID, hypeTrainID)
				So(err, ShouldBeNil)
				So(result.HypeTrain, ShouldNotBeNil)
				So(result.GrantedRewards, ShouldNotBeEmpty)
				So(result.UserOwnedRewards, ShouldNotBeEmpty)

				So(entitlementsDB.CreateEmoteEntitlementCallCount(), ShouldEqual, 0)
				So(badgesDB.SetBadgesCallCount(), ShouldEqual, 0)
			})

			Convey("when rewards were recorded in participant DB but do not exist in entitlement and badges DB", func() {
				participant := tests.ValidParticipant()
				participant.Rewards = []percy.Reward{
					{
						ID:   uuid.New().String(),
						Type: percy.EmoteReward,
					},
				}
				participantDB.GetHypeTrainParticipantReturns(&participant, nil)

				result, err := engine.AdminGrantHypeTrainRewardsByHypeTrainID(context.Background(), channelID, userID, hypeTrainID)
				So(err, ShouldBeNil)
				So(result.HypeTrain, ShouldNotBeNil)
				So(result.GrantedRewards, ShouldNotBeEmpty)
				So(result.UserOwnedRewards, ShouldNotBeEmpty)

				So(entitlementsDB.CreateEmoteEntitlementCallCount(), ShouldEqual, 1)
				So(badgesDB.SetBadgesCallCount(), ShouldEqual, 1)
			})

			Convey("when no reward was granted to user previously", func() {
				participant := tests.ValidParticipant()
				participant.Rewards = []percy.Reward{}
				participantDB.GetHypeTrainParticipantReturns(&participant, nil)

				result, err := engine.AdminGrantHypeTrainRewardsByHypeTrainID(context.Background(), channelID, userID, hypeTrainID)
				So(err, ShouldBeNil)
				So(result.HypeTrain, ShouldNotBeNil)
				So(result.GrantedRewards, ShouldNotBeEmpty)
				So(result.UserOwnedRewards, ShouldNotBeEmpty)

				So(entitlementsDB.CreateEmoteEntitlementCallCount(), ShouldEqual, 1)
				So(badgesDB.SetBadgesCallCount(), ShouldEqual, 1)
			})
		})

		Convey("returns nothing", func() {
			Convey("if no hype train is found", func() {
				hypeTrainDB.GetHypeTrainByIDReturns(nil, nil)

				result, err := engine.AdminGrantHypeTrainRewardsByHypeTrainID(context.Background(), channelID, userID, hypeTrainID)
				So(err, ShouldBeNil)
				So(result.HypeTrain, ShouldBeNil)
				So(result.GrantedRewards, ShouldBeEmpty)
				So(result.UserOwnedRewards, ShouldBeEmpty)
			})
		})

		Convey("returns an error", func() {
			hypeTrain := tests.ValidHypeTrain()
			participant := tests.ValidParticipant()

			// happy path behaviors
			hypeTrainDB.GetHypeTrainByIDReturns(&hypeTrain, nil)
			participantDB.GetHypeTrainParticipantReturns(&participant, nil)

			Convey("if channel ID is missing", func() {
				channelID = ""
			})

			Convey("if hype train ID is missing", func() {
				hypeTrainID = ""
			})

			Convey("if user ID is missing", func() {
				userID = ""
			})

			Convey("if hype train DB fails", func() {
				hypeTrainDB.GetHypeTrainByIDReturns(nil, errors.New("can't get"))
			})

			Convey("if participant DB fails to get participant", func() {
				participantDB.GetHypeTrainParticipantReturns(nil, errors.New("can't get"))
			})

			Convey("if participant DB fails to update participant", func() {
				participantDB.PutHypeTrainParticipantsReturns(errors.New("can't put"))
			})

			Convey("if entitlement DB fails to read entitlements", func() {
				entitlementsDB.ReadEmoteEntitlementsReturns(nil, errors.New("can't read"))
			})

			Convey("if entitlement DB fails to entitle", func() {
				entitlementsDB.CreateEmoteEntitlementReturns(errors.New("can't create"))
			})

			_, err := engine.AdminGrantHypeTrainRewardsByHypeTrainID(context.Background(), channelID, hypeTrainID, userID)
			So(err, ShouldNotBeNil)
		})
	})
}
