package twirp_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	"code.justin.tv/commerce/percy/internal/twirp"
	"code.justin.tv/commerce/percy/internal/twirp/twirpfakes"
	"code.justin.tv/commerce/percy/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestServer_CanStartHypeTrain(t *testing.T) {
	Convey("Given a Twirp server", t, func() {
		engine := &internalfakes.FakeEngine{}
		converter := &twirpfakes.FakeConverter{}

		p := twirp.NewPercy(engine, converter)

		ctx := context.Background()

		req := &rpc.CanStartHypeTrainReq{
			ChannelId: tests.ChannelID,
		}

		config := tests.ValidHypeTrainConfig()

		Convey("when engine returns true for starting a hype train", func() {
			engine.GetHypeTrainConfigReturns(config, nil)
			engine.CanStartHypeTrainReturns(true, nil)

			Convey("returns true for a valid hype train", func() {
				resp, err := p.CanStartHypeTrain(ctx, req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.CanStartHypeTrain, ShouldBeTrue)
			})
		})

		Convey("returns an error when", func() {
			Convey("req is nil", func() {
				req = nil
			})

			Convey("channelID is missing", func() {
				req.ChannelId = ""
			})

			Convey("engine returns an error", func() {
				engine.CanStartHypeTrainReturns(false, errors.New("engine failure"))
			})

			_, err := p.CanStartHypeTrain(ctx, req)
			So(err, ShouldNotBeNil)
		})
	})
}
