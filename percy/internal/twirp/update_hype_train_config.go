package twirp

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/rpc"
	"github.com/twitchtv/twirp"
)

const (
	updateHypeTrainConfigTimeout = 1 * time.Second
)

func (p *Percy) UpdateHypeTrainConfig(ctx context.Context, req *rpc.UpdateHypeTrainConfigReq) (*rpc.UpdateHypeTrainConfigResp, error) {
	ctx, cancel := context.WithTimeout(ctx, updateHypeTrainConfigTimeout)
	defer cancel()

	if req == nil {
		return nil, errors.New("request is missing")
	}

	if len(req.ChannelId) == 0 {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	// only set parameters that have values on the update struct
	update := percy.HypeTrainConfigUpdate{}

	if req.Enabled != nil {
		update.Enabled = &req.Enabled.Value
	}

	if req.NumOfEventsToKickoff != nil {
		update.NumOfEventsToKickoff = pointers.IntP(int(req.NumOfEventsToKickoff.Value))
	}

	if req.Difficulty != rpc.Difficulty_UNKNOWN_DIFFICULTY {
		difficulty := p.converter.ToDifficulty(req.Difficulty)
		if !difficulty.Unknown() {
			update.Difficulty = &difficulty
		}
	}

	if req.CooldownDurationMinutes != nil {
		update.CooldownDuration = pointers.DurationP(time.Duration(req.CooldownDurationMinutes.Value) * time.Minute)
	}

	if req.CalloutEmoteId != nil {
		update.CalloutEmoteID = pointers.StringP(req.CalloutEmoteId.Value)
	}

	if req.UseCreatorColor != nil {
		update.UseCreatorColor = pointers.BoolP(req.UseCreatorColor.Value)
	}

	if req.UsePersonalizedSettings != nil {
		update.UsePersonalizedSettings = pointers.BoolP(req.UsePersonalizedSettings.Value)
	}

	updatedConfig, err := p.engine.UpdateHypeTrainConfig(ctx, req.ChannelId, update)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"channelID": req.ChannelId,
			"request":   req,
		}).Error("Failed to update hype train config")
		return nil, err
	}

	twirpConfig, err := p.converter.ToTwirpHypeTrainConfig(updatedConfig)
	if err != nil {
		return nil, err
	}

	return &rpc.UpdateHypeTrainConfigResp{
		ChannelId: req.ChannelId,
		Config:    &twirpConfig,
	}, nil
}
