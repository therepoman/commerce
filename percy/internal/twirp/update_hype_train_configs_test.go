package twirp_test

import (
	"context"
	"errors"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	"code.justin.tv/commerce/percy/internal/twirp"
	"code.justin.tv/commerce/percy/internal/twirp/twirpfakes"
	"code.justin.tv/commerce/percy/rpc"
	"github.com/golang/protobuf/ptypes/wrappers"
	. "github.com/smartystreets/goconvey/convey"
)

func TestServer_UpdateHypeTrainConfig(t *testing.T) {
	Convey("Give a Twirp server", t, func() {
		engine := &internalfakes.FakeEngine{}
		converter := &twirpfakes.FakeConverter{}

		p := twirp.NewPercy(engine, converter)

		ctx := context.Background()

		channelID := "1234567890"
		req := &rpc.UpdateHypeTrainConfigReq{
			ChannelId:               channelID,
			Enabled:                 &wrappers.BoolValue{Value: true},
			NumOfEventsToKickoff:    &wrappers.Int64Value{Value: 5},
			CooldownDurationMinutes: &wrappers.Int64Value{Value: 120},
			Difficulty:              rpc.Difficulty_MEDIUM,
		}

		Convey("updates hype train configs with provided parameters", func() {
			validConfig := tests.ValidTwirpHypeTrainConfig()
			engine.UpdateHypeTrainConfigReturns(tests.ValidHypeTrainConfig(), nil)
			converter.ToTwirpHypeTrainConfigReturns(validConfig, nil)

			resp, err := p.UpdateHypeTrainConfig(ctx, req)
			So(err, ShouldBeNil)
			So(resp.ChannelId, ShouldEqual, channelID)
			So(*resp.Config, ShouldResemble, validConfig)
		})

		Convey("ignores update to parameters", func() {
			Convey("when enabled is unset", func() {
				req.Enabled = nil
				engine.UpdateHypeTrainConfigReturns(percy.HypeTrainConfig{}, nil)

				_, err := p.UpdateHypeTrainConfig(ctx, req)
				So(err, ShouldBeNil)

				_, channelIDCalled, updateCalled := engine.UpdateHypeTrainConfigArgsForCall(0)
				So(channelIDCalled, ShouldEqual, channelID)
				So(updateCalled.Enabled, ShouldBeNil)
			})

			Convey("when number of events to kickoff is unset", func() {
				req.NumOfEventsToKickoff = nil
				engine.UpdateHypeTrainConfigReturns(percy.HypeTrainConfig{}, nil)

				_, err := p.UpdateHypeTrainConfig(ctx, req)
				So(err, ShouldBeNil)

				_, channelIDCalled, updateCalled := engine.UpdateHypeTrainConfigArgsForCall(0)
				So(channelIDCalled, ShouldEqual, channelID)
				So(updateCalled.NumOfEventsToKickoff, ShouldBeNil)
			})

			Convey("when difficulty is unknown", func() {
				req.Difficulty = rpc.Difficulty_UNKNOWN_DIFFICULTY
				engine.UpdateHypeTrainConfigReturns(percy.HypeTrainConfig{}, nil)

				_, err := p.UpdateHypeTrainConfig(ctx, req)
				So(err, ShouldBeNil)

				_, channelIDCalled, updateCalled := engine.UpdateHypeTrainConfigArgsForCall(0)
				So(channelIDCalled, ShouldEqual, channelID)
				So(updateCalled.Difficulty, ShouldBeNil)
			})

			Convey("when cooldown duration is unset", func() {
				req.CooldownDurationMinutes = nil
				engine.UpdateHypeTrainConfigReturns(percy.HypeTrainConfig{}, nil)

				_, err := p.UpdateHypeTrainConfig(ctx, req)
				So(err, ShouldBeNil)

				_, channelIDCalled, updateCalled := engine.UpdateHypeTrainConfigArgsForCall(0)
				So(channelIDCalled, ShouldEqual, channelID)
				So(updateCalled.CooldownDuration, ShouldBeNil)
			})
		})

		Convey("returns an error when", func() {
			Convey("request is nil", func() {
				req = nil
			})

			Convey("channel ID is blank", func() {
				req.ChannelId = ""
			})

			Convey("engine returns an error", func() {
				engine.UpdateHypeTrainConfigReturns(percy.HypeTrainConfig{}, errors.New("engine failure"))
			})

			_, err := p.UpdateHypeTrainConfig(ctx, req)
			So(err, ShouldNotBeNil)
		})
	})
}
