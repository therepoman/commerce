package twirp_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	"code.justin.tv/commerce/percy/internal/twirp"
	"code.justin.tv/commerce/percy/internal/twirp/twirpfakes"
	"code.justin.tv/commerce/percy/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestServer_GetOngoingHypeTrain(t *testing.T) {
	Convey("Given a Twirp server", t, func() {
		engine := &internalfakes.FakeEngine{}
		converter := &twirpfakes.FakeConverter{}

		p := twirp.NewPercy(engine, converter)

		ctx := context.Background()

		channelID := "1234567890"
		req := &rpc.GetOngoingHypeTrainReq{
			ChannelId: channelID,
		}

		hypeTrain := tests.ValidHypeTrain()
		twirpHypeTrain := tests.ValidTwirpHypeTrain()
		approaching := tests.ValidApproaching()
		twirpApproaching := tests.ValidTwirpApproaching()

		Convey("when engine returns a valid hype train", func() {
			engine.GetOngoingHypeTrainReturns(&hypeTrain, nil, nil)
			converter.ToTwirpHypeTrainReturns(twirpHypeTrain, nil)

			Convey("returns a valid twirp response", func() {
				resp, err := p.GetOngoingHypeTrain(ctx, req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.HypeTrain, ShouldResemble, &twirpHypeTrain)
				So(resp.HypeTrain.Config, ShouldResemble, twirpHypeTrain.Config)
				So(resp.HypeTrain.Conductors, ShouldResemble, twirpHypeTrain.Conductors)
				So(resp.HypeTrain.EndingReason, ShouldResemble, twirpHypeTrain.EndingReason)
			})
		})

		Convey("when there is no on-going hype train", func() {
			engine.GetOngoingHypeTrainReturns(nil, nil, nil)

			Convey("returns empty response", func() {
				resp, err := p.GetOngoingHypeTrain(ctx, req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.HypeTrain, ShouldBeNil)
				So(resp.Approaching, ShouldBeNil)
			})
		})

		Convey("when there is an approaching hype train", func() {
			engine.GetOngoingHypeTrainReturns(nil, &approaching, nil)
			converter.ToTwirpApproachingReturns(twirpApproaching)

			Convey("returns approaching response", func() {
				resp, err := p.GetOngoingHypeTrain(ctx, req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.HypeTrain, ShouldBeNil)
				So(resp.Approaching, ShouldNotBeNil)
			})
		})

		Convey("returns an error when", func() {
			Convey("req is nil", func() {
				req = nil
			})

			Convey("channelID is missing", func() {
				req.ChannelId = ""
			})

			Convey("engine returns an error", func() {
				engine.GetOngoingHypeTrainReturns(nil, nil, errors.New("engine failure"))
			})

			Convey("cannot be converted back to twirp format", func() {
				engine.GetOngoingHypeTrainReturns(&hypeTrain, nil, nil)
				converter.ToTwirpHypeTrainReturns(rpc.HypeTrain{}, errors.New("failed to convert"))
			})

			_, err := p.GetOngoingHypeTrain(ctx, req)
			So(err, ShouldNotBeNil)
		})
	})
}
