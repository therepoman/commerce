package twirp_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	"code.justin.tv/commerce/percy/internal/twirp"
	"code.justin.tv/commerce/percy/internal/twirp/twirpfakes"
	"code.justin.tv/commerce/percy/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestServer_UpsertHypeTrain(t *testing.T) {
	Convey("Given a Twirp server", t, func() {
		engine := &internalfakes.FakeEngine{}
		converter := &twirpfakes.FakeConverter{}
		p := twirp.NewPercy(engine, converter)
		ctx := context.Background()

		twirpHypeTrain := tests.ValidTwirpHypeTrain()
		req := &rpc.UpsertHypeTrainReq{
			HypeTrain: &twirpHypeTrain,
		}

		Convey("when engine returns a valid hype train", func() {
			engine.UpsertHypeTrainReturns(nil)

			Convey("returns no error in twirp response", func() {
				resp, err := p.UpsertHypeTrain(ctx, req)

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
			})
		})

		Convey("returns an error when", func() {
			Convey("req is nil", func() {
				req = nil
			})

			Convey("hype train is missing", func() {
				req.HypeTrain = nil
			})

			Convey("engine returns n error", func() {
				engine.UpsertHypeTrainReturns(errors.New("engine failure"))
			})

			_, err := p.UpsertHypeTrain(ctx, req)
			So(err, ShouldNotBeNil)
		})
	})
}
