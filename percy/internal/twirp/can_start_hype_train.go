package twirp

import (
	"context"
	"errors"

	"code.justin.tv/commerce/percy/rpc"
)

func (p *Percy) CanStartHypeTrain(ctx context.Context, req *rpc.CanStartHypeTrainReq) (*rpc.CanStartHypeTrainResp, error) {
	if req == nil {
		return nil, errors.New("request is missing")
	}

	if len(req.ChannelId) == 0 {
		return nil, errors.New("channel_id is missing in request")
	}

	canStartHypeTrain, err := p.engine.CanStartHypeTrain(ctx, req.ChannelId)
	if err != nil {
		return nil, err
	}

	return &rpc.CanStartHypeTrainResp{
		CanStartHypeTrain: canStartHypeTrain,
	}, nil
}
