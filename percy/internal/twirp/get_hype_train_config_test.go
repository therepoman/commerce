package twirp_test

import (
	"context"
	"errors"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	"code.justin.tv/commerce/percy/internal/twirp"
	"code.justin.tv/commerce/percy/internal/twirp/twirpfakes"
	rpc "code.justin.tv/commerce/percy/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestServer_GetHypeTrainConfig(t *testing.T) {
	Convey("Given a Twirp server", t, func() {
		engine := &internalfakes.FakeEngine{}
		converter := &twirpfakes.FakeConverter{}

		p := twirp.NewPercy(engine, converter)

		ctx := context.Background()

		channelID := "1234567890"
		req := &rpc.GetHypeTrainConfigReq{
			ChannelId: channelID,
		}

		config := tests.ValidHypeTrainConfig()
		twirpConfig := tests.ValidTwirpHypeTrainConfig()

		Convey("when engine returns a valid hype train config", func() {
			engine.GetHypeTrainConfigReturns(config, nil)
			converter.ToTwirpHypeTrainConfigReturns(twirpConfig, nil)

			Convey("returns a valid twirp response", func() {
				resp, err := p.GetHypeTrainConfig(ctx, req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.Config, ShouldResemble, &twirpConfig)
				So(resp.Config.KickoffConfig, ShouldResemble, twirpConfig.KickoffConfig)
			})
		})

		Convey("returns an error when", func() {
			Convey("req is nil", func() {
				req = nil
			})

			Convey("channelID is missing", func() {
				req.ChannelId = ""
			})

			Convey("engine returns an error", func() {
				engine.GetHypeTrainConfigReturns(percy.HypeTrainConfig{}, errors.New("engine failure"))
			})

			_, err := p.GetHypeTrainConfig(ctx, req)
			So(err, ShouldNotBeNil)
		})
	})
}
