package twirp

import (
	"context"
	"time"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/rpc"
	"github.com/golang/protobuf/ptypes"
	"github.com/pkg/errors"
	"github.com/twitchtv/twirp"
)

func (p *Percy) AdminGrantHypeTrainRewards(ctx context.Context, req *rpc.AdminGrantHypeTrainRewardsReq) (*rpc.AdminGrantHypeTrainRewardsResp, error) {
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	if req == nil {
		return nil, errors.New("request is missing")
	}

	if len(req.ChannelId) == 0 {
		return nil, errors.New("channel_id is missing in request")
	}

	if len(req.UserId) == 0 {
		return nil, errors.New("user_id is missing in request")
	}

	var result percy.GrantHypeTrainRewardsResult
	var err error

	switch req.HypeTrainIdentifier.(type) {
	case *rpc.AdminGrantHypeTrainRewardsReq_HypeTrainId:
		hypeTrainID := req.GetHypeTrainId()
		if len(hypeTrainID) == 0 {
			return nil, twirp.InvalidArgumentError("hype_train_id", "is missing")
		}

		result, err = p.engine.AdminGrantHypeTrainRewardsByHypeTrainID(ctx, req.ChannelId, hypeTrainID, req.UserId)
	case *rpc.AdminGrantHypeTrainRewardsReq_ParticipatedAt:
		participatedAtProto := req.GetParticipatedAt()
		if participatedAtProto == nil {
			return nil, twirp.InvalidArgumentError("participated_at", "timestamp is invalid")
		}

		participatedAt, timeErr := ptypes.Timestamp(participatedAtProto)
		if timeErr != nil {
			return nil, twirp.InvalidArgumentError("participated_at", "timestamp is invalid")
		}

		result, err = p.engine.AdminGrantHypeTrainRewardsByParticipationTime(ctx, req.ChannelId, req.UserId, participatedAt)
	}

	if err != nil {
		logrus.WithFields(logrus.Fields{
			"channelID": req.ChannelId,
			"userID":    req.UserId,
		}).WithError(err).Error("Failed to admin grant rewards")
		return nil, twirp.InternalErrorWith(err)
	}

	// convert the found hype train, use nil if no hype train is found
	var twirpHypeTrain *rpc.HypeTrain
	if result.HypeTrain != nil {
		converterHypeTrain, converterErr := p.converter.ToTwirpHypeTrain(*result.HypeTrain)
		if converterErr != nil {
			logrus.WithFields(logrus.Fields{
				"channelID": req.ChannelId,
				"userID":    req.UserId,
			}).WithError(err).Error("Failed to convert models after admin granting rewards")
			return nil, twirp.InternalErrorWith(errors.Wrap(err, "failed to convert response object"))
		}

		twirpHypeTrain = &converterHypeTrain
	}

	return &rpc.AdminGrantHypeTrainRewardsResp{
		ChannelId:           req.ChannelId,
		UserId:              req.UserId,
		HypeTrain:           twirpHypeTrain,
		GrantedRewards:      p.converter.ToTwirpRewards(result.GrantedRewards...),
		UserOwnedRewards:    p.converter.ToTwirpRewards(result.UserOwnedRewards...),
		ParticipationTotals: p.converter.ToTwirpParticipations(result.Participant.ParticipationTotals),
	}, nil
}
