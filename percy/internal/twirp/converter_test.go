package twirp_test

import (
	"math/rand"
	"testing"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/tests"
	"code.justin.tv/commerce/percy/internal/twirp"
	"code.justin.tv/commerce/percy/rpc"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func TestConverter_ToTwirpHypeTrainConfig(t *testing.T) {
	channelID := uuid.New().String()
	config := percy.HypeTrainConfig{
		ChannelID:     channelID,
		Enabled:       true,
		IsWhitelisted: true,

		KickoffConfig: percy.KickoffConfig{
			NumOfEvents: 2,
			MinPoints:   100,
			Duration:    5 * time.Minute,
		},
		CooldownDuration: 2 * time.Hour,
		LevelDuration:    5 * time.Minute,
		Difficulty:       percy.DifficultyMedium,
		LevelSettingsByDifficulty: map[percy.Difficulty][]percy.LevelSetting{
			percy.DifficultyEasy: {
				{Level: 1, Goal: percy.ParticipationPoint(500), Rewards: []percy.Reward{}},
				{Level: 2, Goal: percy.ParticipationPoint(1000), Rewards: []percy.Reward{}},
				{Level: 3, Goal: percy.ParticipationPoint(1500), Rewards: []percy.Reward{}},
			},
			percy.DifficultyMedium: {
				{Level: 1, Goal: percy.ParticipationPoint(1000), Rewards: []percy.Reward{}},
				{Level: 2, Goal: percy.ParticipationPoint(2000), Rewards: []percy.Reward{}},
				{Level: 3, Goal: percy.ParticipationPoint(3000), Rewards: []percy.Reward{}},
			},
			percy.DifficultyHard: {
				{Level: 1, Goal: percy.ParticipationPoint(5000), Rewards: []percy.Reward{}},
				{Level: 2, Goal: percy.ParticipationPoint(10000), Rewards: []percy.Reward{}},
				{Level: 3, Goal: percy.ParticipationPoint(15000), Rewards: []percy.Reward{}},
			},
		},
		ParticipationConversionRates: map[percy.ParticipationIdentifier]percy.ParticipationPoint{
			{Source: percy.BitsSource, Action: percy.CheerAction}:          percy.ParticipationPoint(1),
			{Source: percy.BitsSource, Action: percy.ExtensionAction}:      percy.ParticipationPoint(1),
			{Source: percy.BitsSource, Action: percy.PollAction}:           percy.ParticipationPoint(1),
			{Source: percy.SubsSource, Action: percy.Tier1SubAction}:       percy.ParticipationPoint(500),
			{Source: percy.SubsSource, Action: percy.Tier2SubAction}:       percy.ParticipationPoint(1000),
			{Source: percy.SubsSource, Action: percy.Tier3SubAction}:       percy.ParticipationPoint(2500),
			{Source: percy.SubsSource, Action: percy.Tier1GiftedSubAction}: percy.ParticipationPoint(500),
			{Source: percy.SubsSource, Action: percy.Tier2GiftedSubAction}: percy.ParticipationPoint(1000),
			{Source: percy.SubsSource, Action: percy.Tier3GiftedSubAction}: percy.ParticipationPoint(2500),
		},
		NotificationThresholds: map[percy.ParticipationIdentifier]int{
			{Source: percy.BitsSource, Action: percy.CheerAction}:          1000,
			{Source: percy.SubsSource, Action: percy.Tier1GiftedSubAction}: 10,
			{Source: percy.SubsSource, Action: percy.Tier2GiftedSubAction}: 5,
			{Source: percy.SubsSource, Action: percy.Tier3GiftedSubAction}: 1,
		},
		ConductorRewards: percy.ConductorRewards{
			percy.BitsSource: map[percy.ConductorType][]percy.Reward{
				percy.CurrentConductor: {},
				percy.FormerConductor:  {},
			},
			percy.SubsSource: map[percy.ConductorType][]percy.Reward{
				percy.CurrentConductor: {},
				percy.FormerConductor:  {},
			},
		},
		CalloutEmoteID:          "123123123",
		UseCreatorColor:         true,
		UsePersonalizedSettings: true,
	}

	Convey("Given a converter", t, func() {
		converter := twirp.NewConverter()

		Convey("converts a hype train config into twirp hype train config", func() {
			converted, err := converter.ToTwirpHypeTrainConfig(config)
			So(err, ShouldBeNil)
			So(converted.Enabled, ShouldEqual, config.Enabled)
			So(converted.IsWhitelisted, ShouldBeTrue)
			So(converted.KickoffConfig.NumOfEvents, ShouldResemble, int64(config.KickoffConfig.NumOfEvents))
			So(converted.KickoffConfig.MinPoints, ShouldResemble, int64(config.KickoffConfig.MinPoints))
			So(converted.KickoffConfig.DurationSeconds, ShouldResemble, int64(config.KickoffConfig.Duration.Seconds()))
			So(converted.LevelDurationSeconds, ShouldEqual, int64(config.LevelDuration.Seconds()))
			So(converted.Difficulty, ShouldEqual, rpc.Difficulty_MEDIUM)
			So(converted.ConductorRewards, ShouldHaveLength, 4)
			So(converted.NotificationThresholds, ShouldHaveLength, 4)
			So(converted.ParticipationConversionRates, ShouldHaveLength, len(config.ParticipationConversionRates))
			So(converted.CalloutEmoteId, ShouldEqual, config.CalloutEmoteID)
			So(converted.UseCreatorColor, ShouldEqual, config.UseCreatorColor)
			So(converted.UsePersonalizedSettings, ShouldEqual, config.UsePersonalizedSettings)
		})
	})
}

func TestConverter_ToTwirpHypeTrain(t *testing.T) {
	Convey("Given a converter", t, func() {
		converter := twirp.NewConverter()

		Convey("with an ended hype train", func() {
			endingReason := percy.EndingReasonCompleted
			hypeTrain := percy.HypeTrain{
				ChannelID:    uuid.New().String(),
				ID:           uuid.New().String(),
				StartedAt:    time.Now().Add(-7 * time.Minute),
				ExpiresAt:    time.Now().Add(-2 * time.Minute),
				UpdatedAt:    time.Now(),
				EndedAt:      pointers.TimeP(time.Now().Add(-3 * time.Minute)),
				EndingReason: &endingReason,
				Config:       tests.ValidHypeTrainConfig(),
				Conductors: map[percy.ParticipationSource]percy.Conductor{
					percy.BitsSource: {
						Source: percy.BitsSource,
						User:   percy.NewUser(uuid.New().String()),
					},
					percy.SubsSource: {
						Source: percy.SubsSource,
						User:   percy.NewUser(uuid.New().String()),
					},
				},
				Participations: map[percy.ParticipationIdentifier]int{
					{
						Source: percy.BitsSource,
						Action: percy.CheerAction,
					}: 100,
					{
						Source: percy.SubsSource,
						Action: percy.Tier1GiftedSubAction,
					}: 100,
				},
			}

			Convey("converts it into a twirp hype train", func() {
				converted, err := converter.ToTwirpHypeTrain(hypeTrain)
				So(err, ShouldBeNil)

				So(converted.Id, ShouldEqual, hypeTrain.ID)
				So(converted.StartedAt.Nanos, ShouldEqual, int64(hypeTrain.StartedAt.Nanosecond()))
				So(converted.ExpiresAt.Nanos, ShouldEqual, int64(hypeTrain.ExpiresAt.Nanosecond()))
				So(converted.EndedAt.Nanos, ShouldEqual, int64(hypeTrain.EndedAt.Nanosecond()))
				So(converted.EndingReason, ShouldEqual, rpc.EndingReason_COMPLETED)
				So(converted.Conductors, ShouldHaveLength, 2)
				So(converted.LevelProgress, ShouldNotBeNil)
				So(converted.LevelProgress.RemainingSeconds, ShouldEqual, 0)
				So(converted.Participations, ShouldHaveLength, 2)
				So(converted.BoostOpportunity, ShouldNotBeNil)
			})
		})

		Convey("with an ongoing hype train", func() {
			hypeTrain := percy.HypeTrain{
				ChannelID: uuid.New().String(),
				ID:        uuid.New().String(),
				StartedAt: time.Now().Add(-1 * time.Minute),
				ExpiresAt: time.Now().Add(4 * time.Minute),
				UpdatedAt: time.Now(),
				Config:    tests.ValidHypeTrainConfig(),
				Conductors: map[percy.ParticipationSource]percy.Conductor{
					percy.BitsSource: {
						Source: percy.BitsSource,
						User:   percy.NewUser(uuid.New().String()),
					},
					percy.SubsSource: {
						Source: percy.SubsSource,
						User:   percy.NewUser(uuid.New().String()),
					},
				},
			}

			Convey("converts it into a twirp hype train", func() {
				converted, err := converter.ToTwirpHypeTrain(hypeTrain)
				So(err, ShouldBeNil)

				So(converted.Id, ShouldEqual, hypeTrain.ID)
				So(converted.StartedAt.Nanos, ShouldEqual, int64(hypeTrain.StartedAt.Nanosecond()))
				So(converted.ExpiresAt.Nanos, ShouldEqual, int64(hypeTrain.ExpiresAt.Nanosecond()))
				So(converted.EndedAt, ShouldBeNil)
				So(converted.EndingReason, ShouldEqual, rpc.EndingReason_IN_PROGRESS)
				So(converted.Conductors, ShouldHaveLength, 2)
				So(converted.LevelProgress, ShouldNotBeNil)
				So(converted.LevelProgress.RemainingSeconds, ShouldBeBetween, 235, 245)
				So(converted.BoostOpportunity, ShouldNotBeNil)
			})
		})
	})
}

func TestConverter_ToTwirpConductor(t *testing.T) {
	Convey("Given a converter", t, func() {
		converter := twirp.NewConverter()

		Convey("converts conductor into a twirp conductor", func() {
			userID := uuid.New().String()
			conductor := percy.Conductor{
				Source: percy.SubsSource,
				User:   percy.NewUser(userID),
				Participations: map[percy.ParticipationIdentifier]int{
					{Source: percy.SubsSource, Action: percy.Tier1GiftedSubAction}: 10,
				},
			}

			twirpConductor := converter.ToTwirpConductor(conductor)
			So(twirpConductor.Source, ShouldEqual, rpc.ParticipationSource_SUBS)
			So(twirpConductor.UserId, ShouldEqual, conductor.User.ID())
			So(twirpConductor.Participations, ShouldHaveLength, 1)
		})
	})
}

func TestConverter_ToTwirpParticipation(t *testing.T) {
	Convey("Given a converter", t, func() {
		converter := twirp.NewConverter()

		Convey("flattens participation totals to a slice", func() {
			totals := map[percy.ParticipationIdentifier]int{
				{Source: percy.BitsSource, Action: percy.CheerAction}:          100,
				{Source: percy.BitsSource, Action: percy.ExtensionAction}:      200,
				{Source: percy.SubsSource, Action: percy.Tier1GiftedSubAction}: 10,
				{Source: percy.SubsSource, Action: percy.Tier2SubAction}:       1,
			}

			expected := []*rpc.Participation{
				{Source: rpc.ParticipationSource_BITS, Action: rpc.ParticipationAction_CHEER, Quantity: 100},
				{Source: rpc.ParticipationSource_BITS, Action: rpc.ParticipationAction_EXTENSION, Quantity: 200},
				{Source: rpc.ParticipationSource_SUBS, Action: rpc.ParticipationAction_TIER_1_GIFTED_SUB, Quantity: 10},
				{Source: rpc.ParticipationSource_SUBS, Action: rpc.ParticipationAction_TIER_2_SUB, Quantity: 1},
			}

			participations := converter.ToTwirpParticipations(totals)
			So(participations, ShouldHaveLength, 4)
			for _, i := range expected {
				So(participations, ShouldContain, i)
			}
		})
	})
}

func TestConverter_Difficulty(t *testing.T) {
	Convey("Given a converter", t, func() {
		converter := twirp.NewConverter()
		Convey("with a valid difficulty", func() {
			difficulty := []percy.Difficulty{
				percy.DifficultyEasy,
				percy.DifficultyMedium,
				percy.DifficultyHard,
			}[rand.Intn(3)] //nolint

			Convey("can convert to a twirp Difficulty", func() {
				twirpDifficulty := converter.ToTwirpDifficulty(difficulty)

				Convey("can convert back to the same Difficulty", func() {
					So(converter.ToDifficulty(twirpDifficulty), ShouldEqual, difficulty)
				})
			})
		})

		Convey("returns unknown for a unrecognized difficulty", func() {
			So(converter.ToDifficulty(rpc.Difficulty_UNKNOWN_DIFFICULTY), ShouldEqual, percy.DifficultyUnknown)
		})

		Convey("returns unknown twirp difficulty for unrecognized difficulty", func() {
			So(converter.ToTwirpDifficulty(percy.Difficulty("WHAT")), ShouldEqual, rpc.Difficulty_UNKNOWN_DIFFICULTY)
		})
	})
}
