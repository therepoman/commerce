package twirp

import (
	"net/http"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/rpc"
	rpc_bits_pinata "code.justin.tv/commerce/percy/rpc/bits_pinata"
	rpc_celebi "code.justin.tv/commerce/percy/rpc/celebrations"
	rpc_creator_anniversaries "code.justin.tv/commerce/percy/rpc/creator_anniversaries"
	rpc_twitchiversary "code.justin.tv/commerce/percy/rpc/twitchiversary"
	offerstenantrpc "code.justin.tv/revenue/offer-tenant-schema/rpc"
	"github.com/twitchtv/twirp"
)

type Percy struct {
	engine    percy.Engine
	converter Converter
}

// NewPercy ...
func NewPercy(engine percy.Engine, converter Converter) *Percy {
	return &Percy{
		engine:    engine,
		converter: converter,
	}
}

// NewHandler ...
func NewHandler(percy rpc.Percy, celebrations rpc_celebi.Celebi, twitchiversary rpc_twitchiversary.Twitchiversary, creator_anniversaries rpc_creator_anniversaries.CreatorAnniversaries, bits_pinata rpc_bits_pinata.Pinata, celebrationsOfferTenant offerstenantrpc.OffersTenant, hooks *twirp.ServerHooks) http.Handler {
	twirpHandler := rpc.NewPercyServer(percy, hooks)
	mux := http.NewServeMux()

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write([]byte("Percy!"))
	})

	mux.Handle(rpc.PercyPathPrefix, twirpHandler)

	celebrationsTwirpHandler := rpc_celebi.NewCelebiServer(celebrations, hooks)
	mux.Handle(rpc_celebi.CelebiPathPrefix, celebrationsTwirpHandler)

	celebrationsOfferTenantTwirpHandler := offerstenantrpc.NewOffersTenantServer(celebrationsOfferTenant, hooks)
	mux.Handle(offerstenantrpc.OffersTenantPathPrefix, celebrationsOfferTenantTwirpHandler)

	twitchiversaryTwirpHandler := rpc_twitchiversary.NewTwitchiversaryServer(twitchiversary, hooks)
	mux.Handle(rpc_twitchiversary.TwitchiversaryPathPrefix, twitchiversaryTwirpHandler)

	creatorAnniversariesTwirpHandler := rpc_creator_anniversaries.NewCreatorAnniversariesServer(creator_anniversaries, hooks)
	mux.Handle(rpc_creator_anniversaries.CreatorAnniversariesPathPrefix, creatorAnniversariesTwirpHandler)

	bitsPinataTwirpHandler := rpc_bits_pinata.NewPinataServer(bits_pinata, hooks)
	mux.Handle(rpc_bits_pinata.PinataPathPrefix, bitsPinataTwirpHandler)

	logrus.Info("(public) percy twirp handler: %{handler}s", rpc.PercyPathPrefix)
	logrus.Info("(public) percy twirp handler: %{handler}s", rpc_celebi.CelebiPathPrefix)
	logrus.Info("(public) percy twirp handler: %{handler}s", offerstenantrpc.OffersTenantPathPrefix)
	logrus.Info("(public) percy twirp handler: %{handler}s", rpc_twitchiversary.TwitchiversaryPathPrefix)
	logrus.Info("(public) percy twirp handler: %{handler}s", rpc_creator_anniversaries.CreatorAnniversariesPathPrefix)

	return mux
}
