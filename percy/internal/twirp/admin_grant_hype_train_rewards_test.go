package twirp_test

import (
	"context"
	"errors"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	"code.justin.tv/commerce/percy/internal/twirp"
	"code.justin.tv/commerce/percy/internal/twirp/twirpfakes"
	"code.justin.tv/commerce/percy/rpc"
	"github.com/golang/protobuf/ptypes"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func TestAdminGrantHypeTrainRewards(t *testing.T) {
	Convey("given a Twirp server", t, func() {
		engine := &internalfakes.FakeEngine{}
		converter := &twirpfakes.FakeConverter{}

		server := twirp.NewPercy(engine, converter)

		Convey("returns hype train and granted rewards", func() {
			req := rpc.AdminGrantHypeTrainRewardsReq{
				ChannelId: uuid.New().String(),
				UserId:    uuid.New().String(),
				HypeTrainIdentifier: &rpc.AdminGrantHypeTrainRewardsReq_ParticipatedAt{
					ParticipatedAt: ptypes.TimestampNow(),
				},
			}

			converter.ToTwirpParticipationsReturns([]*rpc.Participation{
				{
					Source:   rpc.ParticipationSource_BITS,
					Action:   rpc.ParticipationAction_CHEER,
					Quantity: 100,
				},
			})

			Convey("when hype train and rewards are found", func() {
				validHypeTrain := tests.ValidHypeTrain()
				rewards := []percy.Reward{
					{
						Type: percy.EmoteReward,
						ID:   uuid.New().String(),
					},
				}
				participant := tests.ValidParticipant()
				converter.ToTwirpRewardsReturns([]*rpc.Reward{
					{
						Id:   rewards[0].ID,
						Type: rpc.RewardType_EMOTE,
					},
				})

				result := percy.GrantHypeTrainRewardsResult{
					HypeTrain:        &validHypeTrain,
					GrantedRewards:   rewards,
					UserOwnedRewards: rewards,
					Participant:      participant,
				}

				Convey("given hype train ID", func() {
					engine.AdminGrantHypeTrainRewardsByHypeTrainIDReturns(result, nil)
					req.HypeTrainIdentifier = &rpc.AdminGrantHypeTrainRewardsReq_HypeTrainId{
						HypeTrainId: uuid.New().String(),
					}
				})

				Convey("given participation timestamp", func() {
					engine.AdminGrantHypeTrainRewardsByParticipationTimeReturns(result, nil)
				})

				resp, err := server.AdminGrantHypeTrainRewards(context.Background(), &req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.HypeTrain, ShouldNotBeNil)
				So(resp.GrantedRewards, ShouldNotBeEmpty)
				So(resp.UserOwnedRewards, ShouldNotBeEmpty)
				So(resp.ParticipationTotals, ShouldNotBeEmpty)
			})
		})

		Convey("returns nothing", func() {
			req := rpc.AdminGrantHypeTrainRewardsReq{
				ChannelId: uuid.New().String(),
				UserId:    uuid.New().String(),
				HypeTrainIdentifier: &rpc.AdminGrantHypeTrainRewardsReq_ParticipatedAt{
					ParticipatedAt: ptypes.TimestampNow(),
				},
			}

			Convey("when hype train and rewards are not found", func() {
				Convey("given hype train ID", func() {
					engine.AdminGrantHypeTrainRewardsByHypeTrainIDReturns(percy.GrantHypeTrainRewardsResult{}, nil)
					req.HypeTrainIdentifier = &rpc.AdminGrantHypeTrainRewardsReq_HypeTrainId{
						HypeTrainId: uuid.New().String(),
					}
				})

				Convey("given participation timestamp", func() {
					engine.AdminGrantHypeTrainRewardsByParticipationTimeReturns(percy.GrantHypeTrainRewardsResult{}, nil)
				})
			})

			resp, err := server.AdminGrantHypeTrainRewards(context.Background(), &req)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.HypeTrain, ShouldBeNil)
			So(resp.GrantedRewards, ShouldBeEmpty)
			So(resp.ParticipationTotals, ShouldBeEmpty)
		})

		Convey("returns an error", func() {
			// happy path should return no error
			req := rpc.AdminGrantHypeTrainRewardsReq{
				ChannelId: uuid.New().String(),
				UserId:    uuid.New().String(),
				HypeTrainIdentifier: &rpc.AdminGrantHypeTrainRewardsReq_ParticipatedAt{
					ParticipatedAt: ptypes.TimestampNow(),
				},
			}
			_, err := server.AdminGrantHypeTrainRewards(context.Background(), &req)
			So(err, ShouldBeNil)

			// sad paths
			Convey("when channel ID is missing", func() {
				req.ChannelId = ""
			})

			Convey("when user ID is missing", func() {
				req.UserId = ""
			})

			Convey("when hype train ID is provided but blank", func() {
				req.HypeTrainIdentifier = &rpc.AdminGrantHypeTrainRewardsReq_HypeTrainId{
					HypeTrainId: "",
				}
			})

			Convey("when participated at timestamp is provided but invalid", func() {
				req.HypeTrainIdentifier = &rpc.AdminGrantHypeTrainRewardsReq_ParticipatedAt{
					ParticipatedAt: nil,
				}
			})

			Convey("engine returns an error", func() {
				engine.AdminGrantHypeTrainRewardsByParticipationTimeReturns(percy.GrantHypeTrainRewardsResult{}, errors.New("huh?"))
			})

			_, err = server.AdminGrantHypeTrainRewards(context.Background(), &req)
			So(err, ShouldNotBeNil)
		})
	})
}
