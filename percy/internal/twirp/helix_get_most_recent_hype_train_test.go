package twirp_test

import (
	"context"
	"errors"
	"sort"
	"testing"
	"time"

	percy_internal "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	"code.justin.tv/commerce/percy/internal/twirp"
	"code.justin.tv/commerce/percy/internal/twirp/twirpfakes"
	"code.justin.tv/commerce/percy/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestServer_HelixGetMostRecentHypeTrain(t *testing.T) {
	Convey("Given a Twirp server", t, func() {
		engine := &internalfakes.FakeEngine{}
		converter := &twirpfakes.FakeConverter{}
		percy := twirp.NewPercy(engine, converter)
		ctx := context.Background()
		req := &rpc.HelixGetMostRecentHypeTrainReq{
			ChannelId: tests.ChannelID,
		}
		testHypeTrain := tests.ValidHypeTrain()
		testParticipations := []percy_internal.Participation{
			{
				ChannelID: tests.ChannelID,
				User: percy_internal.User{
					Id: tests.UserID,
				},
				Quantity: 100,
				ParticipationIdentifier: percy_internal.ParticipationIdentifier{
					Source: percy_internal.BitsSource,
					Action: percy_internal.CheerAction,
				},
				Timestamp: time.Now().Add(-6 * time.Minute),
			},
			{
				ChannelID: tests.ChannelID,
				User: percy_internal.User{
					Id: tests.UserID,
				},
				Quantity: 1,
				ParticipationIdentifier: percy_internal.ParticipationIdentifier{
					Source: percy_internal.SubsSource,
					Action: percy_internal.Tier1GiftedSubAction,
				},
				Timestamp: time.Now().Add(-5 * time.Minute),
			},
			{
				ChannelID: tests.ChannelID,
				User: percy_internal.User{
					Id: tests.UserID,
				},
				Quantity: 30,
				ParticipationIdentifier: percy_internal.ParticipationIdentifier{
					Source: percy_internal.BitsSource,
					Action: percy_internal.CheerAction,
				},
				Timestamp: time.Now().Add(-4 * time.Minute),
			},
			{
				ChannelID: tests.ChannelID,
				User: percy_internal.User{
					Id: tests.UserID,
				},
				Quantity: 20,
				ParticipationIdentifier: percy_internal.ParticipationIdentifier{
					Source: percy_internal.BitsSource,
					Action: percy_internal.CheerAction,
				},
				Timestamp: time.Now().Add(-3 * time.Minute),
			},
			{
				ChannelID: tests.ChannelID,
				User: percy_internal.User{
					Id: tests.UserID,
				},
				Quantity: 1,
				ParticipationIdentifier: percy_internal.ParticipationIdentifier{
					Source: percy_internal.SubsSource,
					Action: percy_internal.Tier2GiftedSubAction,
				},
				Timestamp: time.Now().Add(-2 * time.Minute),
			},
			{
				ChannelID: tests.ChannelID,
				User: percy_internal.User{
					Id: tests.UserID,
				},
				Quantity: 1,
				ParticipationIdentifier: percy_internal.ParticipationIdentifier{
					Source: percy_internal.SubsSource,
					Action: percy_internal.Tier3GiftedSubAction,
				},
				Timestamp: time.Now().Add(-1 * time.Minute),
			},
		}

		Convey("when engine returns a valid hype train", func() {
			Convey("when there is ongoing hype train", func() {
				engine.GetMostRecentHypeTrainReturns(&testHypeTrain, nil)
				engine.GetParticipationsReturns(testParticipations, nil)
			})

			resp, err := percy.HelixGetMostRecentHypeTrain(ctx, req)
			sort.Slice(resp.TopContributions, func(i, j int) bool {
				return resp.TopContributions[i].Type < resp.TopContributions[j].Type
			})

			So(err, ShouldBeNil)
			So(resp.HypeTrainId, ShouldEqual, tests.HypeTrainID)
			So(resp.Level, ShouldEqual, 3)
			So(resp.Goal, ShouldEqual, 1000)
			So(resp.Total, ShouldEqual, 2150) // 4150 - 2000
			So(resp.TopContributions, ShouldResemble, []*rpc.Contribution{
				{
					Type:  string(percy_internal.BitsSource),
					User:  tests.UserID,
					Total: 150,
				},
				{
					Type:  string(percy_internal.SubsSource),
					User:  tests.UserID,
					Total: 4000,
				},
			})
			So(resp.LastContribution, ShouldResemble, &rpc.Contribution{
				Type:  string(percy_internal.SubsSource),
				User:  tests.UserID,
				Total: 2500,
			})
		})

		Convey("when there has never been a hype train on this channel", func() {
			engine.GetMostRecentHypeTrainReturns(nil, nil)

			resp, err := percy.HelixGetMostRecentHypeTrain(ctx, req)
			So(err, ShouldBeNil)
			So(resp.Level, ShouldEqual, 0)
			So(resp.Goal, ShouldEqual, 0)
			So(resp.Total, ShouldEqual, 0)
			So(len(resp.TopContributions), ShouldEqual, 0)
			So(resp.LastContribution, ShouldBeNil)
		})

		Convey("returns an error when", func() {
			Convey("req is nil", func() {
				req = nil
			})

			Convey("ChannelId is missing", func() {
				req.ChannelId = ""
			})

			Convey("engine.GetMostRecentHypeTrain errors", func() {
				engine.GetMostRecentHypeTrainReturns(nil, errors.New("meh"))
			})

			Convey("hype train has bad StartedAt value", func() {
				engine.GetMostRecentHypeTrainReturns(&percy_internal.HypeTrain{
					StartedAt: time.Date(-1, 1, 1, 0, 0, 0, 0, time.UTC),
				}, nil)
			})

			Convey("hype train has bad ExpiresAt value", func() {
				engine.GetMostRecentHypeTrainReturns(&percy_internal.HypeTrain{
					StartedAt: time.Now(),
					ExpiresAt: time.Date(-1, 1, 1, 0, 0, 0, 0, time.UTC),
				}, nil)
			})

			Convey("hype train has bad cool down duration", func() {
				engine.GetMostRecentHypeTrainReturns(&percy_internal.HypeTrain{
					StartedAt: time.Date(1, 1, 1, 0, 0, 0, 0, time.UTC),
					ExpiresAt: time.Date(1, 1, 1, 0, 5, 0, 0, time.UTC),
					Config: percy_internal.HypeTrainConfig{
						CooldownDuration: -1 * time.Hour,
					},
				}, nil)
			})

			Convey("engine.GetParticipations errors", func() {
				engine.GetMostRecentHypeTrainReturns(&testHypeTrain, nil)
				engine.GetParticipationsReturns(nil, errors.New("meh"))
			})

			_, err := percy.HelixGetMostRecentHypeTrain(ctx, req)
			So(err, ShouldNotBeNil)
		})
	})
}
