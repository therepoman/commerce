package twirp_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	"code.justin.tv/commerce/percy/internal/twirp"
	"code.justin.tv/commerce/percy/internal/twirp/twirpfakes"
	"code.justin.tv/commerce/percy/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPercy_GetHypeTrainBadges(t *testing.T) {
	Convey("Given a Twrip Server", t, func() {
		engine := &internalfakes.FakeEngine{}
		converter := &twirpfakes.FakeConverter{}

		p := twirp.NewPercy(engine, converter)

		ctx := context.Background()
		req := &rpc.GetHypeTrainBadgesReq{
			UserId:    "123123123",
			ChannelId: "321321321",
		}

		badges := tests.ValidBadgesResponse()
		twirpBadgeType := tests.ValidTwirpBadgeTypeResponse()

		Convey("when engine returns valid badges", func() {
			engine.GetBadgesReturns(badges, nil)
			converter.ToTwirpBadgeTypeReturns(twirpBadgeType)

			Convey("returns valid twirp response", func() {
				resp, err := p.GetHypeTrainBadges(ctx, req)

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.Badges, ShouldNotBeNil)
				So(resp.Badges, ShouldHaveLength, 1)
				So(resp.Badges[0].BadgeType, ShouldEqual, twirpBadgeType)
			})
		})

		Convey("return an error when", func() {
			Convey("req is nil", func() {
				req = nil
			})

			Convey("userID is missing", func() {
				req.UserId = ""
			})

			Convey("channelID is missing", func() {
				req.ChannelId = ""
			})

			Convey("engine returns an error", func() {
				engine.GetBadgesReturns(nil, errors.New("WALRUS STRIKE"))
			})

			_, err := p.GetHypeTrainBadges(ctx, req)
			So(err, ShouldNotBeNil)
		})
	})
}
