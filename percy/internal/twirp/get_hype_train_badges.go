package twirp

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/rpc"
	"github.com/twitchtv/twirp"
)

const (
	getHypeTrainBadgesTimeout = 1 * time.Second
)

func (p *Percy) GetHypeTrainBadges(ctx context.Context, req *rpc.GetHypeTrainBadgesReq) (*rpc.GetHypeTrainBadgesResp, error) {
	ctx, cancel := context.WithTimeout(ctx, getHypeTrainBadgesTimeout)
	defer cancel()

	if req == nil {
		return nil, errors.New("request is missing")
	}

	if len(req.UserId) == 0 {
		return nil, twirp.RequiredArgumentError("user_id")
	}

	if len(req.ChannelId) == 0 {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	badges, err := p.engine.GetBadges(ctx, req.UserId, req.ChannelId)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"userID":    req.UserId,
			"channelID": req.ChannelId,
		}).Error("Failed to get hype train badges for user in channel")
		return nil, err
	}

	respBadges := make([]*rpc.HypeTrainBadge, len(badges))
	for i, badge := range badges {
		respBadges[i] = &rpc.HypeTrainBadge{
			BadgeType: p.converter.ToTwirpBadgeType(badge),
		}
	}

	return &rpc.GetHypeTrainBadgesResp{
		Badges: respBadges,
	}, nil
}
