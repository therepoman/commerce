package twirp

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/logrus"
	rpc "code.justin.tv/commerce/percy/rpc"
	"github.com/twitchtv/twirp"
)

const (
	getHypeTrainConfigTimeout = 1 * time.Second
)

func (p *Percy) GetHypeTrainConfig(ctx context.Context, req *rpc.GetHypeTrainConfigReq) (*rpc.GetHypeTrainConfigResp, error) {
	ctx, cancel := context.WithTimeout(ctx, getHypeTrainConfigTimeout)
	defer cancel()

	if req == nil {
		return nil, errors.New("request is missing")
	}

	if len(req.ChannelId) == 0 {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	config, err := p.engine.GetHypeTrainConfig(ctx, req.ChannelId)
	if err != nil {
		logrus.WithError(err).WithField("channelID", req.ChannelId).Error("Failed to get hype train config")
		return nil, err
	}

	twirpConfig, err := p.converter.ToTwirpHypeTrainConfig(config)
	if err != nil {
		return nil, err
	}

	return &rpc.GetHypeTrainConfigResp{
		ChannelId: req.ChannelId,
		Config:    &twirpConfig,
	}, nil
}
