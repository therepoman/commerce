package twirp

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/rpc"
	"github.com/twitchtv/twirp"
)

const (
	unsetHypeTrainConfigTimeout = 1 * time.Second
)

func (p *Percy) UnsetHypeTrainConfig(ctx context.Context, req *rpc.UnsetHypeTrainConfigReq) (*rpc.UnsetHypeTrainConfigResp, error) {
	ctx, cancel := context.WithTimeout(ctx, unsetHypeTrainConfigTimeout)
	defer cancel()

	if req == nil {
		return nil, errors.New("request is missing")
	}

	if len(req.ChannelId) == 0 {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	unset := percy.HypeTrainConfigUnset{}

	if req.Enabled != nil {
		unset.Enabled = req.Enabled.Value
	}

	if req.NumOfEventsToKickoff != nil {
		unset.NumOfEventsToKickoff = req.NumOfEventsToKickoff.Value
	}

	if req.Difficulty != nil {
		unset.Difficulty = req.Difficulty.Value
	}

	if req.CooldownDurationMinutes != nil {
		unset.CooldownDuration = req.CooldownDurationMinutes.Value
	}

	if req.CalloutEmoteId != nil {
		unset.CalloutEmoteID = req.CalloutEmoteId.Value
	}

	if req.UseCreatorColor != nil {
		unset.UseCreatorColor = req.UseCreatorColor.Value
	}

	if req.UsePersonalizedSettings != nil {
		unset.UsePersonalizedSettings = req.UsePersonalizedSettings.Value
	}

	updatedConfig, err := p.engine.UnsetHypeTrainConfig(ctx, req.ChannelId, unset)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"channelID": req.ChannelId,
			"unsetReq":  unset,
		}).Error("failed to unset values in config")
		return nil, err
	}

	twirpConfig, err := p.converter.ToTwirpHypeTrainConfig(updatedConfig)
	if err != nil {
		return nil, err
	}

	return &rpc.UnsetHypeTrainConfigResp{
		ChannelId: req.ChannelId,
		Config:    &twirpConfig,
	}, nil
}
