package twirp_celebrations

import (
	"context"
	"errors"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	rpc_celebi "code.justin.tv/commerce/percy/rpc/celebrations"
	"github.com/twitchtv/twirp"
)

const (
	updatePurchasableCelebrationConfigTimeout = 1 * time.Second
)

func (p *Celebrations) UpdatePurchasableCelebrationConfig(ctx context.Context, req *rpc_celebi.UpdatePurchasableCelebrationConfigReq) (*rpc_celebi.UpdatePurchasableCelebrationConfigResp, error) {
	ctx, cancel := context.WithTimeout(ctx, updatePurchasableCelebrationConfigTimeout)
	defer cancel()

	if req == nil {
		return nil, errors.New("request is missing")
	}

	if len(req.ChannelId) == 0 {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	// if there is nothing to update, we shouldn't bother calling any DDB APIs.
	if len(req.ProductConfigurationUpdates) < 1 {
		return nil, twirp.InvalidArgumentError("product_configuration_updates", "need at least 1 product configuration update")
	}

	// TODO: Validate that offer, if passed in, should be validated as valid offer ID to set. We will need to
	// Privatelink to Corleone to then call them to validate the offer.
	// JIRA: https://jira.twitch.com/browse/MOMENTS-1495 https://jira.twitch.com/browse/MOMENTS-1496
	for _, purchaseConfigUpdate := range req.ProductConfigurationUpdates {
		if purchaseConfigUpdate == nil {
			return nil, twirp.InvalidArgumentError("product_configuration_updates", "cannot pass nil update")
		}

		if _, ok := rpc_celebi.CelebrationSize_name[int32(purchaseConfigUpdate.Size)]; !ok && purchaseConfigUpdate.Size < 1 {
			return nil, twirp.InvalidArgumentError("size", "invalid size for update")
		}

		if purchaseConfigUpdate.OfferId == nil && purchaseConfigUpdate.Disabled == nil {
			return nil, twirp.InvalidArgumentError("product_configuration_updates", "nothing pass to update")
		}
	}

	celebrationPurchaseConfig, err := p.celebrationEngine.UpdatePurchasableCelebrationConfig(ctx, req.ChannelId, p.converter.ToCelebrationProductConfigurationUpdate(req.ProductConfigurationUpdates))
	if err != nil {
		if errors.Is(err, percy.CelebrationPurchasableConfigDuplicateOfferError) {
			return nil, twirp.InvalidArgumentError("product_configuration_updates", "cannot contain duplicate offers")
		}

		return nil, twirp.InternalErrorWith(err)
	}

	productConfigs := make([]*rpc_celebi.ProductConfiguration, 0)
	for size, purchaseConfig := range celebrationPurchaseConfig.PurchaseConfig {
		productConfig := p.converter.ToTwirpCelebrationProductConfiguration(size, purchaseConfig)
		productConfigs = append(productConfigs, &productConfig)
	}

	return &rpc_celebi.UpdatePurchasableCelebrationConfigResp{
		Configuration: &rpc_celebi.PurchasableCelebrationConfig{
			ProductConfiguration: productConfigs,
		},
	}, nil
}
