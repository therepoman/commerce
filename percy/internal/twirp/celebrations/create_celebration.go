package twirp_celebrations

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	celebration_error "code.justin.tv/commerce/percy/internal/errors"
	rpc_celebrations "code.justin.tv/commerce/percy/rpc/celebrations"
	"github.com/twitchtv/twirp"
)

const (
	createCelebrationTimeout = 1 * time.Second
)

func (p *Celebrations) CreateCelebration(ctx context.Context, req *rpc_celebrations.CreateCelebrationReq) (*rpc_celebrations.CreateCelebrationResp, error) {
	ctx, cancel := context.WithTimeout(ctx, createCelebrationTimeout)
	defer cancel()

	if req == nil {
		return nil, errors.New("request is missing")
	}

	if len(req.ChannelId) == 0 {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	if len(req.UserId) == 0 {
		return nil, twirp.RequiredArgumentError("user_id")
	}

	if !p.celebrationEngine.HasCelebrations(req.ChannelId) {
		return nil, errors.New("channel not in whitelist")
	}

	err := p.celebrationEngine.Authorize(ctx, req.ChannelId, req.UserId)
	if err != nil {
		return nil, celebration_error.CelebiErrorToTwirpError(err)
	}

	var args percy.CelebrationArgs
	if req.Area == rpc_celebrations.CelebrationArea_UNKNOWN_AREA {
		return nil, twirp.RequiredArgumentError("area")
	}

	area := percy.CelebrationArea(req.Area.String())
	args.CelebrationArea = &area

	if req.Duration == nil {
		return nil, twirp.RequiredArgumentError("duration")
	}

	duration := (time.Duration(req.Duration.Seconds) * time.Second) + (time.Duration(req.Duration.Nanos) * time.Nanosecond)
	if duration <= 0 {
		return nil, twirp.InvalidArgumentError("duration", "must be greater then 0")
	}
	args.CelebrationDuration = &duration

	if req.Effect == rpc_celebrations.CelebrationEffect_UNKNOWN_EFFECT {
		return nil, twirp.RequiredArgumentError("effect")
	}

	effect := percy.CelebrationEffect(req.Effect.String())
	args.CelebrationEffect = &effect

	args.Enabled = &req.Enabled

	if req.EventThreshold <= 0 {
		return nil, twirp.InvalidArgumentError("threshold", "must be greater then 0")
	}

	args.EventThreshold = &req.EventThreshold

	if req.EventType == rpc_celebrations.EventType_UNKNOWN_EVENT_TYPE {
		return nil, twirp.RequiredArgumentError("event_type")
	}

	if req.Intensity < 0 {
		return nil, twirp.InvalidArgumentError("intensity", "must be greater than or equal to 0")
	}

	if req.Intensity > 100 {
		return nil, twirp.InvalidArgumentError("intensity", "must be less than or equal to 100")
	}

	args.CelebrationIntensity = &req.Intensity

	event := percy.CelebrationEventType(req.EventType.String())
	args.EventType = &event

	existingConfig, err := p.celebrationEngine.GetCelebrationConfig(ctx, req.ChannelId, true)
	if err != nil {
		logrus.WithError(err).WithField("channelID", req.ChannelId).Error("Failed to get existing celebrations")
		return nil, err
	}

	if len(existingConfig.Celebrations) >= 20 {
		return nil, celebration_error.CelebiErrorToTwirpError(celebration_error.OverLimit.New("user has reached the max celebrations that can be created"))
	}

	for _, existingCelebration := range existingConfig.Celebrations {
		if existingCelebration.EventThreshold == *args.EventThreshold && existingCelebration.EventType == *args.EventType {
			return nil, celebration_error.CelebiErrorToTwirpError(celebration_error.CelebrationCollision.New("celebration with that threshold and event already exists"))
		}
	}

	celebration, err := p.celebrationEngine.CreateCelebration(ctx, req.ChannelId, args)
	if err != nil {
		logrus.WithError(err).WithField("channelID", req.ChannelId).Error("Failed to update celebration")
		return nil, celebration_error.CelebiErrorToTwirpError(err)
	}

	twirpCelebration := p.converter.ToTwirpCelebration(celebration)

	return &rpc_celebrations.CreateCelebrationResp{
		Celebration: &twirpCelebration,
	}, nil
}
