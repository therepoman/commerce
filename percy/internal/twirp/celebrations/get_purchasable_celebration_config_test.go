package twirp_celebrations_test

import (
	"context"
	"testing"

	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	celebrations_twirp "code.justin.tv/commerce/percy/internal/twirp/celebrations"
	"code.justin.tv/commerce/percy/internal/twirp/celebrations/celebrationsfakes"
	rpcCelebi "code.justin.tv/commerce/percy/rpc/celebrations"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCelebrations_GetPurchasableCelebrationConfig(t *testing.T) {
	Convey("given a celebrations API", t, func() {
		celebrationEngine := &internalfakes.FakeCelebrationsEngine{}
		converter := &celebrationsfakes.FakeCelebrationsConverter{}
		launchPad := &internalfakes.FakeLaunchPad{}

		p := celebrations_twirp.NewCelebrations(celebrationEngine, converter, launchPad, []string{"offer-1"})

		ctx := context.Background()
		channelID := tests.ChannelID

		req := &rpcCelebi.GetPurchasableCelebrationConfigReq{ChannelId: channelID}

		Convey("returns a product config per offer ID", func() {
			resp, err := p.GetPurchasableCelebrationConfig(ctx, req)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.Configuration, ShouldNotBeNil)
			So(resp.Configuration.ProductConfiguration, ShouldHaveLength, 1)
			So(resp.Configuration.ProductConfiguration[0].OfferId, ShouldEqual, "offer-1")
		})

		Convey("we return error", func() {
			Convey("when the request is nil", func() {
				req = nil
			})

			Convey("when the user ID is blank", func() {
				req = &rpcCelebi.GetPurchasableCelebrationConfigReq{}
			})

			_, err := p.GetPurchasableCelebrationConfig(ctx, req)
			So(err, ShouldNotBeNil)
		})
	})
}
