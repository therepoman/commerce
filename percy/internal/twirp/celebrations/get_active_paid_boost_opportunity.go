package twirp_celebrations

import (
	"context"
	"errors"
	"time"

	rpcCelebi "code.justin.tv/commerce/percy/rpc/celebrations"
	"github.com/twitchtv/twirp"
)

const (
	getActiveBoostOpportunityTimeout = 1 * time.Second
)

func (p *Celebrations) GetActivePaidBoostOpportunity(ctx context.Context, req *rpcCelebi.GetActivePaidBoostOpportunityReq) (*rpcCelebi.GetActivePaidBoostOpportunityResp, error) {
	ctx, cancel := context.WithTimeout(ctx, getActiveBoostOpportunityTimeout)
	defer cancel()

	if req == nil {
		return nil, errors.New("request is missing")
	}

	if len(req.ChannelId) == 0 {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	activeBoostOpportunity, err := p.launchPad.GetActiveBoostOpportunity(ctx, req.ChannelId)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	// no active boost
	if activeBoostOpportunity == nil {
		return &rpcCelebi.GetActivePaidBoostOpportunityResp{}, nil
	}

	twirpBoostOpportunity, err := p.converter.ToTwirpPaidBoostOpportunity(*activeBoostOpportunity)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	return &rpcCelebi.GetActivePaidBoostOpportunityResp{
		Opportunity: twirpBoostOpportunity,
	}, nil
}
