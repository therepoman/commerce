package twirp_celebrations_offertenant

import (
	"testing"

	"code.justin.tv/amzn/CorleoneTwirp"
	offerstenantrpc "code.justin.tv/revenue/offer-tenant-schema/rpc"
	. "github.com/smartystreets/goconvey/convey"
)

func TestValidation_ValidateFulfillmentRequest(t *testing.T) {
	Convey("Given a validation util", t, func() {
		Convey("we should error if", func() {
			Convey("owner_id is empty", func() {
				err := ValidateFulfillmentRequest(offerstenantrpc.FulfillmentRequest{
					OwnerId:  "",
					OriginId: "abc",
					Offer: &CorleoneTwirp.Offer{
						Id:       "some-id",
						TplrData: &CorleoneTwirp.TPLR{},
						TagBindings: map[string]string{
							"channel_id": "456",
						},
					},
				})
				So(err, ShouldNotBeNil)
			})
			Convey("origin_id is empty", func() {
				err := ValidateFulfillmentRequest(offerstenantrpc.FulfillmentRequest{
					OwnerId:  "123",
					OriginId: "",
					Offer: &CorleoneTwirp.Offer{
						Id:       "some-id",
						TplrData: &CorleoneTwirp.TPLR{},
						TagBindings: map[string]string{
							"channel_id": "456",
						},
					},
				})
				So(err, ShouldNotBeNil)
			})
			Convey("offer is empty", func() {
				err := ValidateFulfillmentRequest(offerstenantrpc.FulfillmentRequest{
					OwnerId:  "123",
					OriginId: "abc",
					Offer:    nil,
				})
				So(err, ShouldNotBeNil)
			})
			Convey("offer.id is empty", func() {
				err := ValidateFulfillmentRequest(offerstenantrpc.FulfillmentRequest{
					OwnerId:  "123",
					OriginId: "abc",
					Offer: &CorleoneTwirp.Offer{
						Id:       "",
						TplrData: &CorleoneTwirp.TPLR{},
						TagBindings: map[string]string{
							"channel_id": "456",
						},
					},
				})
				So(err, ShouldNotBeNil)
			})
			Convey("tplr_data is empty", func() {
				err := ValidateFulfillmentRequest(offerstenantrpc.FulfillmentRequest{
					OwnerId:  "123",
					OriginId: "abc",
					Offer: &CorleoneTwirp.Offer{
						Id:       "some-id",
						TplrData: nil,
						TagBindings: map[string]string{
							"channel_id": "456",
						},
					},
				})
				So(err, ShouldNotBeNil)
			})
			Convey("channel_id is empty", func() {
				err := ValidateFulfillmentRequest(offerstenantrpc.FulfillmentRequest{
					OwnerId:  "123",
					OriginId: "abc",
					Offer: &CorleoneTwirp.Offer{
						Id:       "some-id",
						TplrData: &CorleoneTwirp.TPLR{},
						TagBindings: map[string]string{
							"channel_id": "",
						},
					},
				})
				So(err, ShouldNotBeNil)
			})
		})

		Convey("we should return nil otherwise", func() {
			err := ValidateFulfillmentRequest(offerstenantrpc.FulfillmentRequest{
				OwnerId:  "123",
				OriginId: "abc",
				Offer: &CorleoneTwirp.Offer{
					Id:       "some-id",
					TplrData: &CorleoneTwirp.TPLR{},
					TagBindings: map[string]string{
						"channel_id": "456",
					},
				},
			})
			So(err, ShouldBeNil)
		})
	})
}

func TestValidation_RevocationRequest(t *testing.T) {
	Convey("Given a validation util", t, func() {
		Convey("we should error if", func() {
			Convey("owner_id is empty", func() {
				err := ValidateRevocationRequest(offerstenantrpc.RevocationRequest{
					OwnerId:  "",
					OriginId: "abc",
					Offer: &CorleoneTwirp.Offer{
						Id:       "some-id",
						TplrData: &CorleoneTwirp.TPLR{},
						TagBindings: map[string]string{
							"channel_id": "456",
						},
					},
				})
				So(err, ShouldNotBeNil)
			})
			Convey("origin_id is empty", func() {
				err := ValidateRevocationRequest(offerstenantrpc.RevocationRequest{
					OwnerId:  "123",
					OriginId: "",
					Offer: &CorleoneTwirp.Offer{
						Id:       "some-id",
						TplrData: &CorleoneTwirp.TPLR{},
						TagBindings: map[string]string{
							"channel_id": "456",
						},
					},
				})
				So(err, ShouldNotBeNil)
			})
			Convey("offer is empty", func() {
				err := ValidateRevocationRequest(offerstenantrpc.RevocationRequest{
					OwnerId:  "123",
					OriginId: "abc",
					Offer:    nil,
				})
				So(err, ShouldNotBeNil)
			})
			Convey("offer.id is empty", func() {
				err := ValidateRevocationRequest(offerstenantrpc.RevocationRequest{
					OwnerId:  "123",
					OriginId: "abc",
					Offer: &CorleoneTwirp.Offer{
						Id:       "",
						TplrData: &CorleoneTwirp.TPLR{},
						TagBindings: map[string]string{
							"channel_id": "456",
						},
					},
				})
				So(err, ShouldNotBeNil)
			})
			Convey("tplr_data is empty", func() {
				err := ValidateRevocationRequest(offerstenantrpc.RevocationRequest{
					OwnerId:  "123",
					OriginId: "abc",
					Offer: &CorleoneTwirp.Offer{
						Id:       "some-id",
						TplrData: nil,
						TagBindings: map[string]string{
							"channel_id": "456",
						},
					},
				})
				So(err, ShouldNotBeNil)
			})
			Convey("channel_id is empty", func() {
				err := ValidateRevocationRequest(offerstenantrpc.RevocationRequest{
					OwnerId:  "123",
					OriginId: "abc",
					Offer: &CorleoneTwirp.Offer{
						Id:       "some-id",
						TplrData: &CorleoneTwirp.TPLR{},
						TagBindings: map[string]string{
							"channel_id": "",
						},
					},
				})
				So(err, ShouldNotBeNil)
			})
		})

		Convey("we should return nil otherwise", func() {
			err := ValidateRevocationRequest(offerstenantrpc.RevocationRequest{
				OwnerId:  "123",
				OriginId: "abc",
				Offer: &CorleoneTwirp.Offer{
					Id:       "some-id",
					TplrData: &CorleoneTwirp.TPLR{},
					TagBindings: map[string]string{
						"channel_id": "456",
					},
				},
			})
			So(err, ShouldBeNil)
		})
	})
}
