package twirp_celebrations_offertenant

import (
	"strings"

	offerstenantrpc "code.justin.tv/revenue/offer-tenant-schema/rpc"
	"github.com/twitchtv/twirp"
)

func ValidateFulfillmentRequest(req offerstenantrpc.FulfillmentRequest) error {
	if strings.TrimSpace(req.OwnerId) == "" {
		return twirp.RequiredArgumentError("owner_id")
	}

	if strings.TrimSpace(req.OriginId) == "" {
		return twirp.RequiredArgumentError("origin_id")
	}

	if req.Offer == nil {
		return twirp.RequiredArgumentError("offer")
	}

	if req.Offer.Id == "" {
		return twirp.RequiredArgumentError("offer.id")
	}

	if req.Offer.TplrData == nil {
		return twirp.RequiredArgumentError("offer.tplrData")
	}

	if req.Offer.TagBindings["channel_id"] == "" {
		return twirp.RequiredArgumentError("offer.TagBindings['channel_id']")
	}

	return nil
}

func ValidateRevocationRequest(req offerstenantrpc.RevocationRequest) error {
	if strings.TrimSpace(req.OwnerId) == "" {
		return twirp.RequiredArgumentError("owner_id")
	}

	if strings.TrimSpace(req.OriginId) == "" {
		return twirp.RequiredArgumentError("origin_id")
	}

	if req.Offer == nil {
		return twirp.RequiredArgumentError("offer")
	}

	if req.Offer.Id == "" {
		return twirp.RequiredArgumentError("offer.id")
	}

	if req.Offer.TplrData == nil {
		return twirp.RequiredArgumentError("offer.tplrData")
	}

	if req.Offer.TagBindings["channel_id"] == "" {
		return twirp.RequiredArgumentError("offer.TagBindings['channel_id']")
	}

	return nil
}
