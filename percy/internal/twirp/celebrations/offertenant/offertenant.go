package twirp_celebrations_offertenant

import (
	percy "code.justin.tv/commerce/percy/internal"
	offerstenantrpc "code.justin.tv/revenue/offer-tenant-schema/rpc"
)

/*
 * The celebration offer tenant code is being re-used to handle paid boost opportunity fulfillment as well.
 * Most of the paid boost opportunity handling logic will be housed in percy.LaunchPad, but who knows ;)
 */

type OfferTenant struct {
	celebrationEngine percy.CelebrationsEngine
	launchPad         percy.LaunchPad
	converter         Converter
}

// NewOfferTenant...
func NewOfferTenant(celebrationEngine percy.CelebrationsEngine, launchPad percy.LaunchPad, converter Converter) offerstenantrpc.OffersTenant {
	return &OfferTenant{
		celebrationEngine: celebrationEngine,
		launchPad:         launchPad,
		converter:         converter,
	}
}
