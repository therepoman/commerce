package twirp_celebrations_offertenant

import (
	"context"
	"time"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	offerstenantrpc "code.justin.tv/revenue/offer-tenant-schema/rpc"
	"github.com/twitchtv/twirp"
)

func (o *OfferTenant) HandleFulfillment(ctx context.Context, req *offerstenantrpc.FulfillmentRequest) (*offerstenantrpc.FulfillmentResponse, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("req")
	}

	err := ValidateFulfillmentRequest(*req)
	if err != nil {
		logrus.WithError(err).Error("Invalid request for HandleFulfillment")
		return nil, err
	}

	err = o.launchPad.FulfillContribution(ctx, percy.BoostOpportunityContribution{
		ChannelID: req.GetOffer().GetTagBindings()["channel_id"],
		UserID:    req.GetOwnerId(),
		OriginID:  req.GetOriginId(),
		Units:     int(req.GetQuantity()),
		Timestamp: time.Now().UTC(),
	})

	if err != nil {
		logrus.WithError(err).Error("Failed to fulfill contribution")
		return nil, err
	}

	return &offerstenantrpc.FulfillmentResponse{
		FulfillmentId: req.OriginId,
	}, nil
}
