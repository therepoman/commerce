package twirp_celebrations_offertenant

import (
	"code.justin.tv/amzn/CorleoneTwirp"
	percy "code.justin.tv/commerce/percy/internal"
	offerstenantrpc "code.justin.tv/revenue/offer-tenant-schema/rpc"
)

type Converter interface {
	ToCelebrationsPurchaseFulfillmentRequest(req *offerstenantrpc.FulfillmentRequest) percy.CelebrationsPurchaseFulfillmentReq
	ToCelebrationsPurchaseRevocationRequest(req *offerstenantrpc.RevocationRequest) percy.CelebrationsPurchaseRevocationReq
	ToCelebrationsEligibilityReadDepth(depth CorleoneTwirp.ReadDepth) percy.CelebrationsEligibilityReadDepth
	ToTwirpOfferEligibilityNotEligibileReasonCode(reasonCode percy.CelebrationsNotEligibleReasonCode) CorleoneTwirp.NotEligibleReasonCode
	ToTwirpOfferEligibility(eligibility percy.CelebrationsPurchaseOfferEligibility) *offerstenantrpc.OfferEligibility
}

type converter struct{}

func NewConverter() *converter { return &converter{} }

func (c *converter) ToCelebrationsPurchaseFulfillmentRequest(req *offerstenantrpc.FulfillmentRequest) percy.CelebrationsPurchaseFulfillmentReq {
	if req == nil {
		return percy.CelebrationsPurchaseFulfillmentReq{}
	}

	return percy.CelebrationsPurchaseFulfillmentReq{
		RecipientUserID:  req.Offer.TagBindings["channel_id"],
		BenefactorUserID: req.OwnerId,
		OriginID:         req.OriginId,
		OfferID:          req.Offer.Id,
	}
}

func (c *converter) ToCelebrationsPurchaseRevocationRequest(req *offerstenantrpc.RevocationRequest) percy.CelebrationsPurchaseRevocationReq {
	if req == nil {
		return percy.CelebrationsPurchaseRevocationReq{}
	}

	return percy.CelebrationsPurchaseRevocationReq{
		RecipientUserID:  req.Offer.TagBindings["channel_id"],
		BenefactorUserID: req.OwnerId,
		OriginID:         req.OriginId,
		OfferID:          req.Offer.Id,
	}
}

func (c *converter) ToCelebrationsEligibilityReadDepth(depth CorleoneTwirp.ReadDepth) percy.CelebrationsEligibilityReadDepth {
	switch depth {
	case CorleoneTwirp.ReadDepth_READ_DEPTH_SHALLOW:
		return percy.CelebrationsEligibilityReadDepthShallow
	case CorleoneTwirp.ReadDepth_READ_DEPTH_DEEP:
		return percy.CelebrationsEligibilityReadDepthDeep
	case CorleoneTwirp.ReadDepth_READ_DEPTH_UNSPECIFIED:
		fallthrough
	default:
		return percy.CelebrationsEligibilityReadDepthDeep
	}
}

func (c *converter) ToTwirpOfferEligibilityNotEligibileReasonCode(reasonCode percy.CelebrationsNotEligibleReasonCode) CorleoneTwirp.NotEligibleReasonCode {
	switch reasonCode {
	case percy.CelebrationsNotEligibleReasonCodeNone:
		return CorleoneTwirp.NotEligibleReasonCode_NONE
	default:
		return CorleoneTwirp.NotEligibleReasonCode_OTHER
	}
}

func (c *converter) ToTwirpOfferEligibility(eligibility percy.CelebrationsPurchaseOfferEligibility) *offerstenantrpc.OfferEligibility {
	return &offerstenantrpc.OfferEligibility{
		OfferId:               eligibility.OfferId,
		IsEligible:            eligibility.IsEligible,
		ShouldDisplay:         eligibility.ShouldDisplay,
		NotEligibleReason:     eligibility.NotEligibleReason,
		NotEligibleReasonCode: c.ToTwirpOfferEligibilityNotEligibileReasonCode(eligibility.NotEligibleReasonCode),
	}
}
