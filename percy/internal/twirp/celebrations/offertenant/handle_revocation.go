package twirp_celebrations_offertenant

import (
	"context"

	offerstenantrpc "code.justin.tv/revenue/offer-tenant-schema/rpc"
)

// HandleRevocation is now used for the paid boost experiment and is no longer handling celebration revocations
// For paid boost, we don't pay out to creators and Twitch takes 100% of the purchase. Therefore, no claw back is needed  and this API should no-op
func (o *OfferTenant) HandleRevocation(ctx context.Context, req *offerstenantrpc.RevocationRequest) (*offerstenantrpc.RevocationResponse, error) {
	return &offerstenantrpc.RevocationResponse{
		RevocationId: req.OriginId,
	}, nil
}
