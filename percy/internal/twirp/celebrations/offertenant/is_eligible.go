package twirp_celebrations_offertenant

import (
	"context"
	"strings"

	"code.justin.tv/amzn/CorleoneTwirp"
	percy "code.justin.tv/commerce/percy/internal"
	offerstenantrpc "code.justin.tv/revenue/offer-tenant-schema/rpc"
	"github.com/twitchtv/twirp"
)

func (o *OfferTenant) IsEligible(ctx context.Context, req *offerstenantrpc.IsEligibleRequest) (*offerstenantrpc.IsEligibleResponse, error) {
	if req == nil {
		return nil, twirp.RequiredArgumentError("req")
	}

	if strings.TrimSpace(req.OwnerId) == "" {
		return nil, twirp.RequiredArgumentError("owner_id")
	}

	if len(req.Offer) == 0 {
		return nil, twirp.RequiredArgumentError("offer")
	}

	offerIDs := make([]string, 0)
	for _, offer := range req.Offer {
		if offer.TplrData == nil {
			return nil, twirp.RequiredArgumentError("offer.tplr_data")
		}

		if offer.TagBindings == nil {
			return nil, twirp.RequiredArgumentError("offer.tag_bindings")
		}

		if offer.TagBindings["channel_id"] == "" {
			return nil, twirp.RequiredArgumentError("offer.tag_bindings['channel_id']")
		}

		offerIDs = append(offerIDs, offer.Id)
	}

	userID := req.OwnerId
	// Peak the first offer, there should just be one but will all have the same channelID if there were multiple
	channelID := req.Offer[0].TagBindings["channel_id"]
	consistentRead := req.ReadDepth == CorleoneTwirp.ReadDepth_READ_DEPTH_DEEP

	isAllowed, err := o.launchPad.IsUserAllowedToContribute(ctx, userID, channelID, consistentRead)
	if err != nil {
		return nil, err
	}

	twirpOfferEligibility := make([]*offerstenantrpc.OfferEligibility, 0, len(offerIDs))
	for _, offerID := range offerIDs {
		notEligibleReason := ""
		notEligibleCode := percy.CelebrationsNotEligibleReasonCodeNone
		if !isAllowed {
			notEligibleReason = "purchase is not enabled/allowed"

			// We don't have any typed value other than None
			notEligibleCode = percy.CelebrationsNotEligibleReasonCode("Other")
		}

		eligibility := percy.CelebrationsPurchaseOfferEligibility{
			OfferId:               offerID,
			IsEligible:            isAllowed,
			ShouldDisplay:         true,
			NotEligibleReason:     notEligibleReason,
			NotEligibleReasonCode: notEligibleCode,
		}
		twirpOfferEligibility = append(twirpOfferEligibility, o.converter.ToTwirpOfferEligibility(eligibility))
	}

	return &offerstenantrpc.IsEligibleResponse{
		Eligibility: twirpOfferEligibility,
	}, nil
}
