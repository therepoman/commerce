package twirp_celebrations_test

import (
	"context"
	"errors"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	celebrations_twirp "code.justin.tv/commerce/percy/internal/twirp/celebrations"
	"code.justin.tv/commerce/percy/internal/twirp/celebrations/celebrationsfakes"
	celebrations_rpc "code.justin.tv/commerce/percy/rpc/celebrations"
	. "github.com/smartystreets/goconvey/convey"
)

func TestServer_GetCelebrationConfig(t *testing.T) {
	Convey("Given a Twirp server", t, func() {
		celebrationEngine := &internalfakes.FakeCelebrationsEngine{}
		converter := &celebrationsfakes.FakeCelebrationsConverter{}
		launchPad := &internalfakes.FakeLaunchPad{}

		p := celebrations_twirp.NewCelebrations(celebrationEngine, converter, launchPad, nil)

		ctx := context.Background()

		channelID := "1234567890"
		userID := "seph"
		req := &celebrations_rpc.GetCelebrationConfigReq{
			ChannelId: channelID,
			UserId:    userID,
		}

		config := tests.ValidCelebrationConfig()
		twirpConfig := tests.ValidTwirpCelebrationConfig()

		Convey("when engine returns a valid celebration config", func() {
			celebrationEngine.GetCelebrationConfigReturns(config, nil)
			converter.ToTwirpCelebrationConfigReturns(twirpConfig)
			celebrationEngine.HasCelebrationsReturns(true)

			Convey("returns a valid twirp response", func() {
				resp, err := p.GetCelebrationConfig(ctx, req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.Config, ShouldResemble, &twirpConfig)
			})
		})

		Convey("when engine returns a valid celebration config and not in whitelist", func() {
			celebrationEngine.GetCelebrationConfigReturns(config, nil)
			converter.ToTwirpCelebrationConfigReturns(twirpConfig)
			celebrationEngine.HasCelebrationsReturns(false)

			Convey("returns a valid twirp response", func() {
				resp, err := p.GetCelebrationConfig(ctx, req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.Config, ShouldBeNil)
			})
		})

		Convey("returns an error when", func() {
			celebrationEngine.HasCelebrationsReturns(true)

			Convey("req is nil", func() {
				req = nil
			})

			Convey("channelID is missing", func() {
				req.ChannelId = ""
			})

			Convey("engine returns an error", func() {
				celebrationEngine.HasCelebrationsReturns(true)
				celebrationEngine.AuthorizeReturns(nil)
				celebrationEngine.GetCelebrationConfigReturns(percy.CelebrationConfig{}, errors.New("engine failure"))
			})

			_, err := p.GetCelebrationConfig(ctx, req)
			So(err, ShouldNotBeNil)
		})
	})
}
