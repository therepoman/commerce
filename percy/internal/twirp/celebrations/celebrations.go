package twirp_celebrations

import (
	percy "code.justin.tv/commerce/percy/internal"
)

type Celebrations struct {
	celebrationEngine percy.CelebrationsEngine
	converter         CelebrationsConverter
	launchPad         percy.LaunchPad // paid boost
	paidBoostOfferIDs []string        // for paid boost offer discovery
}

// NewCelebrations...
func NewCelebrations(celebrationEngine percy.CelebrationsEngine, converter CelebrationsConverter, launchPad percy.LaunchPad, offerIDs []string) *Celebrations {
	return &Celebrations{
		celebrationEngine: celebrationEngine,
		converter:         converter,
		launchPad:         launchPad,
		paidBoostOfferIDs: offerIDs,
	}
}
