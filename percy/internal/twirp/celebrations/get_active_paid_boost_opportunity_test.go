package twirp_celebrations_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	celebrations_twirp "code.justin.tv/commerce/percy/internal/twirp/celebrations"
	"code.justin.tv/commerce/percy/internal/twirp/celebrations/celebrationsfakes"
	rpcCelebi "code.justin.tv/commerce/percy/rpc/celebrations"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestPaidBoosts_GetActivePaidBoostOpportunity(t *testing.T) {
	Convey("given a celebrations API", t, func() {
		celebrationEngine := &internalfakes.FakeCelebrationsEngine{}
		converter := &celebrationsfakes.FakeCelebrationsConverter{}
		launchPad := &internalfakes.FakeLaunchPad{}

		p := celebrations_twirp.NewCelebrations(celebrationEngine, converter, launchPad, nil)

		ctx := context.Background()
		channelID := tests.ChannelID

		req := &rpcCelebi.GetActivePaidBoostOpportunityReq{ChannelId: channelID}

		validBoostOpportunity := tests.ValidBoostOpportunity()
		validTwirpPaidBoostOpportunity := tests.ValidTwirpPaidBoostOpportunity()

		Convey("succeeds when we return an active boost opportunity for channel and have a valid request", func() {
			launchPad.GetActiveBoostOpportunityReturns(&validBoostOpportunity, nil)
			converter.ToTwirpPaidBoostOpportunityReturns(&validTwirpPaidBoostOpportunity, nil)

			resp, err := p.GetActivePaidBoostOpportunity(ctx, req)

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.Opportunity, ShouldNotBeNil)
		})

		Convey("succeeds when we don't find an active boost opportunity for the channel", func() {
			launchPad.GetActiveBoostOpportunityReturns(nil, nil)

			resp, err := p.GetActivePaidBoostOpportunity(ctx, req)

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.Opportunity, ShouldBeNil)
		})

		Convey("we return error", func() {
			Convey("when the request is nil", func() {
				req = nil

				_, err := p.GetActivePaidBoostOpportunity(ctx, req)

				So(err, ShouldNotBeNil)
				So(err, ShouldResemble, errors.New("request is missing"))
			})

			Convey("when the channel ID is blank", func() {
				req = &rpcCelebi.GetActivePaidBoostOpportunityReq{}

				_, err := p.GetActivePaidBoostOpportunity(ctx, req)

				So(err, ShouldNotBeNil)
				So(err, ShouldResemble, twirp.RequiredArgumentError("channel_id"))
			})

			Convey("when we fail to fetch the channel active boost opportunity", func() {
				launchPad.GetActiveBoostOpportunityReturns(nil, errors.New("failed to fetch boost opportunity"))

				_, err := p.GetActivePaidBoostOpportunity(ctx, req)

				So(err, ShouldNotBeNil)
				So(err, ShouldResemble, twirp.InternalErrorWith(errors.New("failed to fetch boost opportunity")))
			})
		})
	})
}
