package twirp_celebrations

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/logrus"
	celebration_error "code.justin.tv/commerce/percy/internal/errors"
	rpc_celebrations "code.justin.tv/commerce/percy/rpc/celebrations"
	"github.com/twitchtv/twirp"
)

const (
	deleteCelebrationTimeout = 1 * time.Second
)

func (p *Celebrations) DeleteCelebration(ctx context.Context, req *rpc_celebrations.DeleteCelebrationReq) (*rpc_celebrations.DeleteCelebrationResp, error) {
	ctx, cancel := context.WithTimeout(ctx, deleteCelebrationTimeout)
	defer cancel()

	if req == nil {
		return nil, errors.New("request is missing")
	}

	if len(req.ChannelId) == 0 {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	if len(req.UserId) == 0 {
		return nil, twirp.RequiredArgumentError("user_id")
	}

	if len(req.CelebrationId) == 0 {
		return nil, twirp.RequiredArgumentError("celebration_id")
	}

	if !p.celebrationEngine.HasCelebrations(req.ChannelId) {
		return nil, errors.New("channel not in whitelist")
	}

	err := p.celebrationEngine.Authorize(ctx, req.ChannelId, req.UserId)
	if err != nil {
		return nil, celebration_error.CelebiErrorToTwirpError(err)
	}

	existingConfig, err := p.celebrationEngine.GetCelebrationConfig(ctx, req.ChannelId, true)
	if err != nil {
		logrus.WithError(err).WithField("channelID", req.ChannelId).Error("Failed to get existing celebrations")
		return nil, celebration_error.CelebiErrorToTwirpError(err)
	}

	_, exists := existingConfig.Celebrations[req.CelebrationId]

	if !exists {
		return nil, celebration_error.CelebiErrorToTwirpError(celebration_error.CelebrationNotFound.New("celebration with id %s does not exist", req.CelebrationId))
	}

	err = p.celebrationEngine.DeleteCelebration(ctx, req.ChannelId, req.CelebrationId)
	if err != nil {
		logrus.WithError(err).WithField("channelID", req.ChannelId).Error("Failed to delete celebration")
		return nil, celebration_error.CelebiErrorToTwirpError(err)
	}

	return &rpc_celebrations.DeleteCelebrationResp{}, nil
}
