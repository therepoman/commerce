package twirp_celebrations

import (
	"context"
	"errors"

	"code.justin.tv/commerce/gogogadget/pointers"
	percy "code.justin.tv/commerce/percy/internal"
	rpcCelebi "code.justin.tv/commerce/percy/rpc/celebrations"
	"github.com/twitchtv/twirp"
)

func (p *Celebrations) UpdateCelebrationUserSettings(ctx context.Context, req *rpcCelebi.UpdateCelebrationUserSettingsReq) (*rpcCelebi.UpdateCelebrationUserSettingsResp, error) {
	ctx, cancel := context.WithTimeout(ctx, getCelebrationUserSettingsTimeout)
	defer cancel()

	if req == nil {
		return nil, errors.New("request is missing")
	}

	if len(req.UserId) == 0 {
		return nil, twirp.RequiredArgumentError("user_id")
	}

	update := percy.CelebrationUserSettingsUpdate{}

	if req.IsOptedOut != nil {
		update.IsOptedOut = pointers.BoolP(req.IsOptedOut.Value)
	}

	settings, err := p.celebrationEngine.UpdateCelebrationUserSettings(ctx, req.UserId, update)
	if err != nil {
		return nil, err
	}

	twirpSettings := p.converter.ToTwirpCelebrationUserSettings(settings)

	return &rpcCelebi.UpdateCelebrationUserSettingsResp{
		UserSettings: &twirpSettings,
	}, nil
}
