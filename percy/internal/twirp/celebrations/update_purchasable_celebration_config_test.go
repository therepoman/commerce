package twirp_celebrations_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	celebrations_twirp "code.justin.tv/commerce/percy/internal/twirp/celebrations"
	"code.justin.tv/commerce/percy/internal/twirp/celebrations/celebrationsfakes"
	rpcCelebi "code.justin.tv/commerce/percy/rpc/celebrations"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCelebrations_UpdatePurchasableCelebrationConfig(t *testing.T) {
	Convey("given a celebrations API", t, func() {
		celebrationEngine := &internalfakes.FakeCelebrationsEngine{}
		converter := &celebrationsfakes.FakeCelebrationsConverter{}
		launchPad := &internalfakes.FakeLaunchPad{}

		p := celebrations_twirp.NewCelebrations(celebrationEngine, converter, launchPad, nil)

		ctx := context.Background()
		channelID := tests.ChannelID

		req := &rpcCelebi.UpdatePurchasableCelebrationConfigReq{
			ChannelId:                   channelID,
			ProductConfigurationUpdates: tests.ValidTwirpCelebrationProductConfigurationUpdate(),
		}

		Convey("succeeds when we return purchasable config for channel and have a valid request", func() {
			converter.ToCelebrationProductConfigurationUpdateReturns(tests.ValidCelebrationProductConfigurationUpdate())
			celebrationEngine.UpdatePurchasableCelebrationConfigReturns(tests.ValidCelebrationPurchasableConfig(), nil)
			converter.ToTwirpCelebrationProductConfigurationReturns(tests.ValidTwirpCelebrationProductConfiguration())

			resp, err := p.UpdatePurchasableCelebrationConfig(ctx, req)

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.Configuration, ShouldNotBeNil)
			So(resp.Configuration.ProductConfiguration, ShouldHaveLength, 1)
			So(*resp.Configuration.ProductConfiguration[0], ShouldResemble, tests.ValidTwirpCelebrationProductConfiguration())
		})

		Convey("we return error", func() {
			Convey("when the request is nil", func() {
				req = nil
			})

			Convey("when the user ID is blank", func() {
				req = &rpcCelebi.UpdatePurchasableCelebrationConfigReq{}
			})

			Convey("when there are no updates in the req", func() {
				req = &rpcCelebi.UpdatePurchasableCelebrationConfigReq{
					ChannelId: channelID,
				}
			})

			Convey("when the items to update in the update list is nil", func() {
				req = &rpcCelebi.UpdatePurchasableCelebrationConfigReq{
					ChannelId: channelID,
					ProductConfigurationUpdates: []*rpcCelebi.ProductConfigurationUpdate{
						nil,
					},
				}
			})

			Convey("when an item to update has an unknown size", func() {
				req = &rpcCelebi.UpdatePurchasableCelebrationConfigReq{
					ChannelId: channelID,
					ProductConfigurationUpdates: []*rpcCelebi.ProductConfigurationUpdate{
						{
							Size: rpcCelebi.CelebrationSize_UNKNOWN,
						},
					},
				}
			})

			Convey("when an item to update has an invalid size", func() {
				req = &rpcCelebi.UpdatePurchasableCelebrationConfigReq{
					ChannelId: channelID,
					ProductConfigurationUpdates: []*rpcCelebi.ProductConfigurationUpdate{
						{
							Size: rpcCelebi.CelebrationSize(-10),
						},
					},
				}
			})

			Convey("when an item to update has no actual updates in it", func() {
				req = &rpcCelebi.UpdatePurchasableCelebrationConfigReq{
					ChannelId: channelID,
					ProductConfigurationUpdates: []*rpcCelebi.ProductConfigurationUpdate{
						{
							Size: rpcCelebi.CelebrationSize_SMALL,
						},
					},
				}
			})

			Convey("when we fail to fetch the the channel purchasable celebration config", func() {
				celebrationEngine.UpdatePurchasableCelebrationConfigReturns(nil, errors.New("WALRUS STRIKE"))
			})

			_, err := p.UpdatePurchasableCelebrationConfig(ctx, req)
			So(err, ShouldNotBeNil)
		})
	})
}
