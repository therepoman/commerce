package twirp_celebrations

import (
	"errors"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	rpc_celebi "code.justin.tv/commerce/percy/rpc/celebrations"
	"github.com/golang/protobuf/ptypes"
)

//go:generate counterfeiter . CelebrationsConverter

type CelebrationsConverter interface {
	ToTwirpCelebrationConfig(percy.CelebrationConfig) rpc_celebi.CelebrationConfig
	ToTwirpCelebration(percy.Celebration) rpc_celebi.Celebration
	ToTwirpEventType(percy.CelebrationEventType) rpc_celebi.EventType
	ToTwirpCelebrationEffect(percy.CelebrationEffect) rpc_celebi.CelebrationEffect
	ToTwirpCelebrationArea(percy.CelebrationArea) rpc_celebi.CelebrationArea
	ToTwirpCelebrationUserSettings(settings percy.CelebrationUserSettings) rpc_celebi.CelebrationUserSettings
	ToTwirpCelebrationSize(size percy.CelebrationSize) rpc_celebi.CelebrationSize
	ToCelebrationSize(size rpc_celebi.CelebrationSize) percy.CelebrationSize
	ToTwirpCelebrationProductConfiguration(size percy.CelebrationSize, config percy.CelebrationPurchaseConfig) rpc_celebi.ProductConfiguration
	ToCelebrationProductConfigurationUpdate(productConfigurationUpdates []*rpc_celebi.ProductConfigurationUpdate) percy.CelebrationChannelPurchasableConfigUpdate
	ToTwirpPaidBoostOpportunity(boostOpportunity percy.BoostOpportunity) (*rpc_celebi.PaidBoostOpportunity, error)
}

type celebrationsConverter struct{}

func NewCelebrationsConverter() *celebrationsConverter {
	return &celebrationsConverter{}
}

func (converter *celebrationsConverter) ToTwirpCelebrationConfig(config percy.CelebrationConfig) rpc_celebi.CelebrationConfig {
	var twirpCelebrations []*rpc_celebi.Celebration
	for _, celebration := range config.Celebrations {
		twirpCelebration := converter.ToTwirpCelebration(celebration)
		twirpCelebrations = append(twirpCelebrations, &twirpCelebration)
	}

	return rpc_celebi.CelebrationConfig{
		ChannelId:    config.ChannelID,
		Enabled:      config.Enabled,
		Celebrations: twirpCelebrations,
	}
}

func (converter *celebrationsConverter) ToTwirpCelebration(celebration percy.Celebration) rpc_celebi.Celebration {
	return rpc_celebi.Celebration{
		CelebrationId:  celebration.CelebrationID,
		Enabled:        celebration.Enabled,
		EventType:      converter.ToTwirpEventType(celebration.EventType),
		EventThreshold: celebration.EventThreshold,
		Effect:         converter.ToTwirpCelebrationEffect(celebration.CelebrationEffect),
		Area:           converter.ToTwirpCelebrationArea(celebration.CelebrationArea),
		Intensity:      celebration.CelebrationIntensity,
		Duration:       ptypes.DurationProto(celebration.CelebrationDuration),
	}
}

func (converter *celebrationsConverter) ToTwirpEventType(event percy.CelebrationEventType) rpc_celebi.EventType {
	var converted rpc_celebi.EventType
	switch event {
	case percy.EventTypeCheer:
		converted = rpc_celebi.EventType_BITS_CHEER
	case percy.EventTypeSubscriptionGift:
		converted = rpc_celebi.EventType_SUBSCRIPTION_GIFT
	case percy.EventTypeExplicitPurchase:
		fallthrough
	case percy.EventTypeSubscriptionFounder:
		fallthrough
	default:
		converted = rpc_celebi.EventType_UNKNOWN_EVENT_TYPE
	}

	return converted
}

func (converter *celebrationsConverter) ToTwirpCelebrationEffect(effect percy.CelebrationEffect) rpc_celebi.CelebrationEffect {
	var converted rpc_celebi.CelebrationEffect
	switch effect {
	case percy.EffectFireworks:
		converted = rpc_celebi.CelebrationEffect_FIREWORKS
	case percy.EffectRain:
		converted = rpc_celebi.CelebrationEffect_RAIN
	case percy.EffectFlamethrowers:
		converted = rpc_celebi.CelebrationEffect_FLAMETHROWERS
	default:
		converted = rpc_celebi.CelebrationEffect_UNKNOWN_EFFECT
	}

	return converted
}

func (converter *celebrationsConverter) ToTwirpCelebrationArea(area percy.CelebrationArea) rpc_celebi.CelebrationArea {
	var converted rpc_celebi.CelebrationArea
	switch area {
	case percy.AreaEverywhere:
		converted = rpc_celebi.CelebrationArea_EVERYWHERE
	case percy.AreaVideoAndPanel:
		converted = rpc_celebi.CelebrationArea_VIDEO_AND_PANEL
	case percy.AreaVideoOnly:
		converted = rpc_celebi.CelebrationArea_VIDEO_ONLY
	default:
		converted = rpc_celebi.CelebrationArea_UNKNOWN_AREA
	}

	return converted
}

func (converter *celebrationsConverter) ToTwirpCelebrationUserSettings(settings percy.CelebrationUserSettings) rpc_celebi.CelebrationUserSettings {
	return rpc_celebi.CelebrationUserSettings{
		IsOptedOut: settings.IsOptedOut,
	}
}

func (converter *celebrationsConverter) ToTwirpCelebrationSize(size percy.CelebrationSize) rpc_celebi.CelebrationSize {
	switch size {
	case percy.CelebrationSizeSmall:
		return rpc_celebi.CelebrationSize_SMALL
	case percy.CelebrationSizeLarge:
		return rpc_celebi.CelebrationSize_LARGE
	case percy.CelebrationSizeUnknown:
		fallthrough
	default:
		return rpc_celebi.CelebrationSize_UNKNOWN
	}
}

func (converter *celebrationsConverter) ToCelebrationSize(size rpc_celebi.CelebrationSize) percy.CelebrationSize {
	switch size {
	case rpc_celebi.CelebrationSize_SMALL:
		return percy.CelebrationSizeSmall
	case rpc_celebi.CelebrationSize_LARGE:
		return percy.CelebrationSizeLarge
	case rpc_celebi.CelebrationSize_UNKNOWN:
		fallthrough
	default:
		return percy.CelebrationSizeUnknown
	}
}

func (converter *celebrationsConverter) ToTwirpCelebrationProductConfiguration(size percy.CelebrationSize, config percy.CelebrationPurchaseConfig) rpc_celebi.ProductConfiguration {
	return rpc_celebi.ProductConfiguration{
		Size:     converter.ToTwirpCelebrationSize(size),
		OfferId:  config.OfferID,
		Disabled: config.IsDisabled,
	}
}

func (converter *celebrationsConverter) ToCelebrationProductConfigurationUpdate(productConfigurationUpdates []*rpc_celebi.ProductConfigurationUpdate) percy.CelebrationChannelPurchasableConfigUpdate {
	updates := percy.CelebrationChannelPurchasableConfigUpdate{
		PurchaseConfig: map[percy.CelebrationSize]percy.CelebrationPurchaseConfigUpdate{},
	}

	for _, productUpdate := range productConfigurationUpdates {
		if productUpdate != nil {
			u := percy.CelebrationPurchaseConfigUpdate{}
			if productUpdate.OfferId != nil {
				u.OfferID = &productUpdate.GetOfferId().Value
			}
			if productUpdate.Disabled != nil {
				u.IsDisabled = &productUpdate.GetDisabled().Value
			}
			updates.PurchaseConfig[converter.ToCelebrationSize(productUpdate.Size)] = u
		}
	}

	return updates
}

func (converter *celebrationsConverter) ToTwirpPaidBoostOpportunity(boostOpportunity percy.BoostOpportunity) (*rpc_celebi.PaidBoostOpportunity, error) {
	startedAt, err := ptypes.TimestampProto(boostOpportunity.StartedAt)
	if err != nil {
		return &rpc_celebi.PaidBoostOpportunity{}, errors.New("error converting startedAt")
	}

	expiresAt, err := ptypes.TimestampProto(boostOpportunity.ExpiresAt)
	if err != nil {
		return &rpc_celebi.PaidBoostOpportunity{}, errors.New("error converting expiresAt")
	}

	remainingSeconds := int64(time.Until(boostOpportunity.ExpiresAt).Seconds())

	return &rpc_celebi.PaidBoostOpportunity{
		Id:                     boostOpportunity.ID,
		StartedAt:              startedAt,
		ExpiresAt:              expiresAt,
		RemainingSeconds:       remainingSeconds,
		BoostUnitsContributed:  int64(boostOpportunity.UnitsContributed),
		ImpressionsContributed: int64(boostOpportunity.Impressions()),
	}, nil
}
