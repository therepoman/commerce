package twirp_celebrations

import (
	"context"
	"errors"
	"time"

	rpc_celebi "code.justin.tv/commerce/percy/rpc/celebrations"
	"github.com/twitchtv/twirp"
)

const (
	getPurchasableCelebrationConfigTimeout = 1 * time.Second
)

// We are re-purposing this API for paid boosts.
// Making this API an no-op for now to ensure that we aren't leaking old celebrations data
// Once the proper products and offers are set up for paid boost, we will make this API return that data

func (p *Celebrations) GetPurchasableCelebrationConfig(ctx context.Context, req *rpc_celebi.GetPurchasableCelebrationConfigReq) (*rpc_celebi.GetPurchasableCelebrationConfigResp, error) {
	// TODO: bring this ctx back
	_, cancel := context.WithTimeout(ctx, getPurchasableCelebrationConfigTimeout)
	defer cancel()

	if req == nil {
		return nil, errors.New("request is missing")
	}

	if len(req.ChannelId) == 0 {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	productConfigs := make([]*rpc_celebi.ProductConfiguration, 0)
	for _, offerID := range p.paidBoostOfferIDs {
		productConfigs = append(
			productConfigs, &rpc_celebi.ProductConfiguration{
				OfferId:  offerID,
				Size:     rpc_celebi.CelebrationSize_SMALL,
				Disabled: false,
			},
		)
	}

	return &rpc_celebi.GetPurchasableCelebrationConfigResp{
		Configuration: &rpc_celebi.PurchasableCelebrationConfig{
			ProductConfiguration: productConfigs,
		},
	}, nil
}
