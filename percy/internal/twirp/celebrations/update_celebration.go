package twirp_celebrations

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	celebration_error "code.justin.tv/commerce/percy/internal/errors"
	rpc_celebrations "code.justin.tv/commerce/percy/rpc/celebrations"
	"github.com/twitchtv/twirp"
)

const (
	updateCelebrationTimeout = 1 * time.Second
)

func (p *Celebrations) UpdateCelebration(ctx context.Context, req *rpc_celebrations.UpdateCelebrationReq) (*rpc_celebrations.UpdateCelebrationResp, error) {
	ctx, cancel := context.WithTimeout(ctx, updateCelebrationTimeout)
	defer cancel()

	if req == nil {
		return nil, errors.New("request is missing")
	}

	if len(req.ChannelId) == 0 {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	if len(req.UserId) == 0 {
		return nil, twirp.RequiredArgumentError("user_id")
	}

	if len(req.CelebrationId) == 0 {
		return nil, twirp.RequiredArgumentError("celebration_id")
	}

	if !p.celebrationEngine.HasCelebrations(req.ChannelId) {
		return nil, errors.New("channel not in whitelist")
	}

	err := p.celebrationEngine.Authorize(ctx, req.ChannelId, req.UserId)
	if err != nil {
		return nil, celebration_error.CelebiErrorToTwirpError(err)
	}

	var args percy.CelebrationArgs
	if req.Area != rpc_celebrations.CelebrationArea_UNKNOWN_AREA {
		area := percy.CelebrationArea(req.Area.String())
		args.CelebrationArea = &area
	}

	if req.Duration != nil {
		duration := (time.Duration(req.Duration.Seconds) * time.Second) + (time.Duration(req.Duration.Nanos) * time.Nanosecond)
		if duration <= 0 {
			return nil, twirp.InvalidArgumentError("duration_seconds", "must be greater then 0")
		}
		args.CelebrationDuration = &duration
	}

	if req.Effect != rpc_celebrations.CelebrationEffect_UNKNOWN_EFFECT {
		effect := percy.CelebrationEffect(req.Effect.String())
		args.CelebrationEffect = &effect
	}

	if req.Enabled != nil {
		args.Enabled = &req.Enabled.Value
	}

	if req.EventThreshold != nil {
		if req.EventThreshold.Value <= 0 {
			return nil, twirp.InvalidArgumentError("threshold", "must be greater then 0")
		}

		args.EventThreshold = &req.EventThreshold.Value
	}

	if req.EventType != rpc_celebrations.EventType_UNKNOWN_EVENT_TYPE {
		event := percy.CelebrationEventType(req.EventType.String())
		args.EventType = &event
	}

	if req.Intensity != nil {
		if req.Intensity.Value < 0 {
			return nil, twirp.InvalidArgumentError("intensity", "must be greater than or equal to 0")
		}

		if req.Intensity.Value > 100 {
			return nil, twirp.InvalidArgumentError("intensity", "must be less than or equal to 100")
		}

		args.CelebrationIntensity = &req.Intensity.Value
	}

	existingConfig, err := p.celebrationEngine.GetCelebrationConfig(ctx, req.ChannelId, true)
	if err != nil {
		logrus.WithError(err).WithField("channelID", req.ChannelId).Error("Failed to get existing celebrations")
		return nil, celebration_error.CelebiErrorToTwirpError(err)
	}

	_, exists := existingConfig.Celebrations[req.CelebrationId]

	if !exists {
		return nil, celebration_error.CelebiErrorToTwirpError(celebration_error.CelebrationNotFound.New("celebration with id %s does not exist", req.CelebrationId))
	}

	celebration, err := p.celebrationEngine.UpdateCelebration(ctx, req.ChannelId, req.CelebrationId, args)
	if err != nil {
		logrus.WithError(err).WithField("channelID", req.ChannelId).Error("Failed to update celebration")
		return nil, celebration_error.CelebiErrorToTwirpError(err)
	}

	twirpCelebration := p.converter.ToTwirpCelebration(celebration)

	return &rpc_celebrations.UpdateCelebrationResp{
		Celebration: &twirpCelebration,
	}, nil
}
