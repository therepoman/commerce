package twirp_celebrations

import (
	"context"
	"errors"
	"time"

	rpcCelebi "code.justin.tv/commerce/percy/rpc/celebrations"
	"github.com/twitchtv/twirp"
)

const (
	getCelebrationUserSettingsTimeout = 1 * time.Second
)

func (p *Celebrations) GetCelebrationUserSettings(ctx context.Context, req *rpcCelebi.GetCelebrationUserSettingsReq) (*rpcCelebi.GetCelebrationUserSettingsResp, error) {
	ctx, cancel := context.WithTimeout(ctx, getCelebrationUserSettingsTimeout)
	defer cancel()

	if req == nil {
		return nil, errors.New("request is missing")
	}

	if len(req.UserId) == 0 {
		return nil, twirp.RequiredArgumentError("user_id")
	}

	settings, err := p.celebrationEngine.GetCelebrationUserSettings(ctx, req.UserId)
	if err != nil {
		return nil, err
	}

	twirpUserSettings := p.converter.ToTwirpCelebrationUserSettings(settings)

	return &rpcCelebi.GetCelebrationUserSettingsResp{
		UserSettings: &twirpUserSettings,
	}, nil
}
