package twirp_celebrations_test

import (
	"context"
	"errors"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	celebrations_twirp "code.justin.tv/commerce/percy/internal/twirp/celebrations"
	"code.justin.tv/commerce/percy/internal/twirp/celebrations/celebrationsfakes"
	celebrations_rpc "code.justin.tv/commerce/percy/rpc/celebrations"
	"github.com/golang/protobuf/ptypes/duration"
	. "github.com/smartystreets/goconvey/convey"
)

func TestServer_CreateCelebration(t *testing.T) {
	Convey("Given a Twirp server", t, func() {
		celebrationEngine := &internalfakes.FakeCelebrationsEngine{}
		converter := &celebrationsfakes.FakeCelebrationsConverter{}
		launchPad := &internalfakes.FakeLaunchPad{}

		p := celebrations_twirp.NewCelebrations(celebrationEngine, converter, launchPad, nil)

		ctx := context.Background()

		channelID := "1234567890"
		userID := "1234567890"

		config := tests.ValidCelebrationConfig()
		updateCelebration := tests.CreatedValidCelebration()
		twirpConfig := tests.CreatedValidTwirpCelebration()

		req := &celebrations_rpc.CreateCelebrationReq{
			ChannelId:      channelID,
			UserId:         userID,
			Enabled:        true,
			EventType:      celebrations_rpc.EventType_SUBSCRIPTION_GIFT,
			EventThreshold: 5,
			Effect:         celebrations_rpc.CelebrationEffect_FIREWORKS,
			Area:           celebrations_rpc.CelebrationArea_EVERYWHERE,
			Intensity:      5,
			Duration: &duration.Duration{
				Seconds: 1,
			},
		}

		Convey("when engine req is valid", func() {
			celebrationEngine.CreateCelebrationReturns(updateCelebration, nil)
			celebrationEngine.GetCelebrationConfigReturns(config, nil)
			converter.ToTwirpCelebrationReturns(twirpConfig)
			celebrationEngine.HasCelebrationsReturns(true)

			Convey("returns a valid twirp response", func() {
				resp, err := p.CreateCelebration(ctx, req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.Celebration, ShouldResemble, &twirpConfig)
			})
		})

		Convey("returns an error when", func() {
			Convey("req is nil", func() {
				req = nil
			})

			Convey("channelID is missing", func() {
				req.ChannelId = ""
			})

			Convey("userID is missing", func() {
				req.UserId = ""
			})

			Convey("EventType is UNKNOWN", func() {
				req.EventType = 0
			})

			Convey("EventThreshold is invalid", func() {
				req.EventThreshold = 0
			})

			Convey("Effect is UNKNOWN", func() {
				req.Effect = 0
			})

			Convey("Area is UNKNOWN", func() {
				req.Area = 0
			})

			Convey("Intensity is invalid", func() {
				req.Intensity = -1
			})

			Convey("Duration is missing", func() {
				req.Duration = nil
			})

			Convey("DurationSeconds is invalid", func() {
				req.Duration = &duration.Duration{
					Seconds: 0,
				}
			})

			Convey("not in whitelist", func() {
				celebrationEngine.HasCelebrationsReturns(false)
			})

			Convey("auth fails", func() {
				celebrationEngine.HasCelebrationsReturns(true)
				celebrationEngine.AuthorizeReturns(errors.New("nope"))
			})

			Convey("engine returns an error", func() {
				celebrationEngine.HasCelebrationsReturns(true)
				celebrationEngine.AuthorizeReturns(nil)
				celebrationEngine.CreateCelebrationReturns(percy.Celebration{}, errors.New("engine failure"))
			})

			_, err := p.CreateCelebration(ctx, req)
			So(err, ShouldNotBeNil)
		})
	})
}
