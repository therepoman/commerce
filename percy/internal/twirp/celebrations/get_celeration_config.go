package twirp_celebrations

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/logrus"
	celebration_error "code.justin.tv/commerce/percy/internal/errors"
	rpc_celebi "code.justin.tv/commerce/percy/rpc/celebrations"
	"github.com/twitchtv/twirp"
)

const (
	getCelebrationConfigTimeout = 1 * time.Second
)

func (p *Celebrations) GetCelebrationConfig(ctx context.Context, req *rpc_celebi.GetCelebrationConfigReq) (*rpc_celebi.GetCelebrationConfigResp, error) {
	ctx, cancel := context.WithTimeout(ctx, getCelebrationConfigTimeout)
	defer cancel()

	if req == nil {
		return nil, errors.New("request is missing")
	}

	if len(req.ChannelId) == 0 {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	if !p.celebrationEngine.HasCelebrations(req.ChannelId) {
		return &rpc_celebi.GetCelebrationConfigResp{
			Config: nil,
		}, nil
	}

	config, err := p.celebrationEngine.GetCelebrationConfig(ctx, req.ChannelId, true)
	if err != nil {
		logrus.WithError(err).WithField("channelID", req.ChannelId).Error("Failed to get celebration config")
		return nil, celebration_error.CelebiErrorToTwirpError(err)
	}

	twirpConfig := p.converter.ToTwirpCelebrationConfig(config)

	return &rpc_celebi.GetCelebrationConfigResp{
		Config: &twirpConfig,
	}, nil
}
