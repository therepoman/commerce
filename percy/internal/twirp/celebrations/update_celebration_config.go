package twirp_celebrations

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/logrus"
	celebration_error "code.justin.tv/commerce/percy/internal/errors"
	rpc_celebrations "code.justin.tv/commerce/percy/rpc/celebrations"
	"github.com/twitchtv/twirp"
)

const (
	updateCelebrationConfigTimeout = 1 * time.Second
)

func (p *Celebrations) UpdateCelebrationConfig(ctx context.Context, req *rpc_celebrations.UpdateCelebrationConfigReq) (*rpc_celebrations.UpdateCelebrationConfigResp, error) {
	ctx, cancel := context.WithTimeout(ctx, updateCelebrationConfigTimeout)
	defer cancel()

	if req == nil {
		return nil, errors.New("request is missing")
	}

	if len(req.ChannelId) == 0 {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	if len(req.UserId) == 0 {
		return nil, twirp.RequiredArgumentError("user_id")
	}

	if !p.celebrationEngine.HasCelebrations(req.ChannelId) {
		return nil, errors.New("channel not in whitelist")
	}

	err := p.celebrationEngine.Authorize(ctx, req.ChannelId, req.UserId)
	if err != nil {
		return nil, celebration_error.CelebiErrorToTwirpError(err)
	}

	config, err := p.celebrationEngine.UpdateCelebrationConfig(ctx, req.ChannelId, req.Enabled)
	if err != nil {
		logrus.WithError(err).WithField("channelID", req.ChannelId).Error("Failed to update celebration config")
		return nil, err
	}

	twirpConfig := p.converter.ToTwirpCelebrationConfig(config)

	return &rpc_celebrations.UpdateCelebrationConfigResp{
		Config: &twirpConfig,
	}, nil
}
