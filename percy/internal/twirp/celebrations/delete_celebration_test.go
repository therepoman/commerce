package twirp_celebrations_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	celebrations_twirp "code.justin.tv/commerce/percy/internal/twirp/celebrations"
	"code.justin.tv/commerce/percy/internal/twirp/celebrations/celebrationsfakes"
	celebrations_rpc "code.justin.tv/commerce/percy/rpc/celebrations"
	. "github.com/smartystreets/goconvey/convey"
)

func TestServer_DeleteCelebration(t *testing.T) {
	Convey("Given a Twirp server", t, func() {
		celebrationEngine := &internalfakes.FakeCelebrationsEngine{}
		converter := &celebrationsfakes.FakeCelebrationsConverter{}
		launchPad := &internalfakes.FakeLaunchPad{}

		p := celebrations_twirp.NewCelebrations(celebrationEngine, converter, launchPad, nil)

		ctx := context.Background()

		channelID := "1234567890"
		userID := "seph"

		config := tests.ValidCelebrationConfig()

		req := &celebrations_rpc.DeleteCelebrationReq{
			ChannelId:     channelID,
			UserId:        userID,
			CelebrationId: "id",
		}

		Convey("when engine req is valid", func() {
			celebrationEngine.DeleteCelebrationReturns(nil)
			celebrationEngine.GetCelebrationConfigReturns(config, nil)
			celebrationEngine.HasCelebrationsReturns(true)

			Convey("returns a valid twirp response", func() {
				resp, err := p.DeleteCelebration(ctx, req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
			})
		})

		Convey("returns an error when", func() {
			Convey("req is nil", func() {
				req = nil
			})

			Convey("channelID is missing", func() {
				req.ChannelId = ""
			})

			Convey("userID is missing", func() {
				req.UserId = ""
			})

			Convey("celebrationID is missing", func() {
				req.CelebrationId = ""
			})

			Convey("celebrationID does not exist", func() {
				req.CelebrationId = "not_real"
			})

			Convey("not in whitelist", func() {
				celebrationEngine.HasCelebrationsReturns(false)
			})

			Convey("auth fails", func() {
				celebrationEngine.HasCelebrationsReturns(true)
				celebrationEngine.AuthorizeReturns(errors.New("nope"))
			})

			Convey("engine returns an error", func() {
				celebrationEngine.HasCelebrationsReturns(true)
				celebrationEngine.AuthorizeReturns(nil)
				celebrationEngine.DeleteCelebrationReturns(errors.New("engine failure"))
			})

			_, err := p.DeleteCelebration(ctx, req)
			So(err, ShouldNotBeNil)
		})
	})
}
