package twirp_celebrations_test

import (
	"context"
	"errors"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	celebrations_twirp "code.justin.tv/commerce/percy/internal/twirp/celebrations"
	"code.justin.tv/commerce/percy/internal/twirp/celebrations/celebrationsfakes"
	rpcCelebi "code.justin.tv/commerce/percy/rpc/celebrations"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCelebrations_GetCelebrationUserSettings(t *testing.T) {
	Convey("given a celebrations API", t, func() {
		celebrationEngine := &internalfakes.FakeCelebrationsEngine{}
		converter := &celebrationsfakes.FakeCelebrationsConverter{}
		launchPad := &internalfakes.FakeLaunchPad{}

		p := celebrations_twirp.NewCelebrations(celebrationEngine, converter, launchPad, nil)

		ctx := context.Background()
		userID := "123123123"

		req := &rpcCelebi.GetCelebrationUserSettingsReq{UserId: userID}

		Convey("succeeds when we return user settings and have a valid request", func() {
			celebrationEngine.GetCelebrationUserSettingsReturns(tests.ValidCelebrationUserSettings(), nil)
			converter.ToTwirpCelebrationUserSettingsReturns(tests.ValidTwirpCelebrationUserSettings())

			resp, err := p.GetCelebrationUserSettings(ctx, req)

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.UserSettings, ShouldNotBeNil)
			So(*resp.UserSettings, ShouldResemble, tests.ValidTwirpCelebrationUserSettings())
		})

		Convey("we return error", func() {
			Convey("when the request is nil", func() {
				req = nil
			})

			Convey("when the user ID is blank", func() {
				req = &rpcCelebi.GetCelebrationUserSettingsReq{}
			})

			Convey("when we fail to fetch the user settings", func() {
				celebrationEngine.GetCelebrationUserSettingsReturns(percy.CelebrationUserSettings{}, errors.New("WALRUS STRIKE"))
			})

			_, err := p.GetCelebrationUserSettings(ctx, req)
			So(err, ShouldNotBeNil)
		})
	})
}
