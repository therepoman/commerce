package twirp

import (
	"context"
	"errors"
	"sort"
	"time"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/rpc"
	"github.com/golang/protobuf/ptypes"
	"github.com/twitchtv/twirp"
)

const (
	helixGetHypeTrainTimeout = 1 * time.Second
)

// Gets either the ongoing Hype Train for the channel if there is one, or the last one
// if there is no ongoing Hype Train
func (p *Percy) HelixGetMostRecentHypeTrain(ctx context.Context, req *rpc.HelixGetMostRecentHypeTrainReq) (*rpc.HelixGetMostRecentHypeTrainResp, error) {
	ctx, cancel := context.WithTimeout(ctx, helixGetHypeTrainTimeout)
	defer cancel()

	if req == nil {
		return nil, errors.New("request is missing")
	}
	if len(req.ChannelId) == 0 {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	targetHypeTrain, err := p.engine.GetMostRecentHypeTrain(ctx, req.ChannelId)
	if err != nil {
		logrus.WithError(err).WithField("channelID", req.ChannelId).
			Error("Helix: Failed to get most recent Hype Train")
		return nil, err
	}
	if targetHypeTrain == nil {
		return &rpc.HelixGetMostRecentHypeTrainResp{}, nil
	}

	startedAt, err := ptypes.TimestampProto(targetHypeTrain.StartedAt)
	if err != nil {
		logrus.WithError(err).Error("Helix: Failed to convert started_at to timestamp")
		return nil, errors.New("invalid started_at value found")
	}
	expiresAt, err := ptypes.TimestampProto(targetHypeTrain.ExpiresAt)
	if err != nil {
		logrus.WithError(err).Error("Helix: Failed to convert expires_at to timestamp")
		return nil, errors.New("invalid expires_at value found")
	}
	coolDownEnd := targetHypeTrain.ExpiresAt.Add(targetHypeTrain.Config.CooldownDuration)
	coolDownEndTime, err := ptypes.TimestampProto(coolDownEnd)
	if err != nil {
		logrus.WithError(err).Error("Helix: Failed to convert cool_down_end_time to timestamp")
		return nil, errors.New("invalid cool down end value found")
	}

	var topContributions []*rpc.Contribution
	for _, conductor := range targetHypeTrain.Conductors {
		conductorTotal := int64(0)
		for sourceIdentifier, quantity := range conductor.Participations {
			if sourceIdentifier.Source == conductor.Source {
				participationPoint := targetHypeTrain.Config.EquivalentParticipationPoint(sourceIdentifier, quantity)
				conductorTotal += int64(participationPoint)
			}
		}

		topContributions = append(topContributions, &rpc.Contribution{
			Type:  string(conductor.Source),
			User:  conductor.User.ID(),
			Total: conductorTotal,
		})
	}

	lastContribution, err := p.getLastContribution(ctx, req.ChannelId, targetHypeTrain)
	if err != nil {
		logrus.WithError(err).Error("Helix: Failed to get the last contribution")
		return nil, errors.New("failed to get the last contribution")
	}

	return &rpc.HelixGetMostRecentHypeTrainResp{
		HypeTrainId:      targetHypeTrain.ID,
		StartedAt:        startedAt,
		ExpiresAt:        expiresAt,
		CooldownEndTime:  coolDownEndTime,
		Level:            int64(targetHypeTrain.Level()),
		Goal:             int64(targetHypeTrain.LevelProgress().Goal),
		Total:            int64(targetHypeTrain.LevelProgress().Progression),
		TopContributions: topContributions,
		LastContribution: lastContribution,
	}, nil
}

func (p *Percy) getLastContribution(ctx context.Context, channelID string, hypeTrain *percy.HypeTrain) (*rpc.Contribution, error) {
	participations, err := p.engine.GetParticipations(ctx, channelID, percy.ParticipationQueryParams{
		StartTime: hypeTrain.StartedAt.Add(-1 * hypeTrain.Config.KickoffConfig.Duration),
		EndTime:   hypeTrain.ExpiresAt.Add(hypeTrain.Config.CooldownDuration),
	})
	if err != nil {
		return nil, err
	}
	if len(participations) == 0 {
		return nil, nil
	}

	// sort into descending order by timestamp
	sort.Slice(participations, func(i, j int) bool {
		return participations[i].Timestamp.After(participations[j].Timestamp)
	})

	return &rpc.Contribution{
		Type:  string(participations[0].Source),
		User:  participations[0].User.ID(),
		Total: int64(hypeTrain.Config.EquivalentParticipationPoint(participations[0].ParticipationIdentifier, participations[0].Quantity)),
	}, nil
}
