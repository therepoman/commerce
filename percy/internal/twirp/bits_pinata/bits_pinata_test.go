package bits_pinata_twirp_test

import (
	"context"
	"fmt"
	"testing"

	"code.justin.tv/commerce/percy/internal/dynamodb"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	twirp "code.justin.tv/commerce/percy/internal/twirp/bits_pinata"
	rpc "code.justin.tv/commerce/percy/rpc/bits_pinata"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func TestBitsPinata_GetActiveBitsPinata(t *testing.T) {
	Convey("Given a Twirp server", t, func() {
		db := &internalfakes.FakeBitsPinataDB{}

		bp := twirp.NewBitsPinata(db)

		ctx := context.Background()

		Convey("when db returns an active bits pinata", func() {
			channelID := fmt.Sprintf("test_channel:%s", uuid.New())
			activePinata := tests.ValidActiveBitsPinata(channelID)
			db.GetActiveBitsPinataReturns(&activePinata, nil)

			req := &rpc.GetActiveBitsPinataReq{
				ChannelId: channelID,
			}

			resp, err := bp.GetActiveBitsPinata(ctx, req)

			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(resp.BitsPinata.ChannelId, ShouldEqual, activePinata.ChannelID)
			So(resp.BitsPinata.PinataId, ShouldEqual, activePinata.PinataID)
			So(resp.BitsPinata.SecondsLeft.GetSeconds(), ShouldBeBetweenOrEqual, 0, dynamodb.BitsPinataContributionTimeout)
		})

		Convey("when db returns a brand new active bits pinata", func() {
			channelID := fmt.Sprintf("test_channel:%s", uuid.New())
			activePinata := tests.ValidNewBitsPinata(channelID)
			db.GetActiveBitsPinataReturns(&activePinata, nil)

			req := &rpc.GetActiveBitsPinataReq{
				ChannelId: channelID,
			}

			resp, err := bp.GetActiveBitsPinata(ctx, req)

			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(resp.BitsPinata.ChannelId, ShouldEqual, activePinata.ChannelID)
			So(resp.BitsPinata.PinataId, ShouldEqual, activePinata.PinataID)
			So(resp.BitsPinata.SecondsLeft.GetSeconds(), ShouldBeBetweenOrEqual, 0, dynamodb.BitsPinataContributionTimeout)
		})

		Convey("when db returns an expired bits pinata", func() {
			channelID := fmt.Sprintf("test_channel:%s", uuid.New())
			expiredPinata := tests.ValidExpiredBitsPinata(channelID)
			db.GetActiveBitsPinataReturns(&expiredPinata, nil)

			req := &rpc.GetActiveBitsPinataReq{
				ChannelId: channelID,
			}

			resp, err := bp.GetActiveBitsPinata(ctx, req)

			So(resp, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})
	})
}
