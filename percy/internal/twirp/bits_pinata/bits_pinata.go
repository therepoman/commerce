package bits_pinata_twirp

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	rpc "code.justin.tv/commerce/percy/rpc/bits_pinata"
	"github.com/golang/protobuf/ptypes"
	"github.com/twitchtv/twirp"
)

const (
	getActiveBitsPinataTimeout = 1 * time.Second
)

type BitsPinata struct {
	BitsPinataDB percy.BitsPinataDB
}

func NewBitsPinata(bitsPinataDB percy.BitsPinataDB) *BitsPinata {
	return &BitsPinata{
		BitsPinataDB: bitsPinataDB,
	}
}

func toTwirpBitsPinata(bitsPinata percy.BitsPinata) (rpc.BitsPinata, error) {
	secondsLeft, err := getSecondsLeft(bitsPinata.LastContribution)
	if err != nil {
		return rpc.BitsPinata{}, err
	}

	return rpc.BitsPinata{
		ChannelId:   bitsPinata.ChannelID,
		PinataId:    bitsPinata.PinataID,
		SecondsLeft: ptypes.DurationProto(secondsLeft),
	}, nil
}

func getSecondsLeft(lastContribution time.Time) (time.Duration, error) {
	secondsLeft := dynamodb.BitsPinataContributionTimeout - time.Since(lastContribution)
	if secondsLeft <= 0 || secondsLeft > dynamodb.BitsPinataContributionTimeout {
		return 0, errors.New("unexpected lastContribution timestamp from DB")
	}
	return secondsLeft, nil
}

func (bp *BitsPinata) GetActiveBitsPinata(ctx context.Context, req *rpc.GetActiveBitsPinataReq) (*rpc.GetActiveBitsPinataResp, error) {
	ctx, cancel := context.WithTimeout(ctx, getActiveBitsPinataTimeout)
	defer cancel()

	if req == nil {
		return nil, errors.New("request is missing")
	}

	if len(req.ChannelId) == 0 {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	bitsPinata, err := bp.BitsPinataDB.GetActiveBitsPinata(ctx, req.ChannelId)
	if err != nil {
		logrus.WithError(err).WithField("channelID", req.ChannelId).Error("Failed to get active bits pinata")
		return nil, err
	}

	if bitsPinata == nil {
		return &rpc.GetActiveBitsPinataResp{
			BitsPinata: nil,
		}, nil
	}

	twirpBitsPinata, err := toTwirpBitsPinata(*bitsPinata)
	if err != nil {
		logrus.WithError(err).WithField("channelID", req.ChannelId).Error("Failed to convert to twirp bits pinata struct")
		return nil, err
	}

	return &rpc.GetActiveBitsPinataResp{
		BitsPinata: &twirpBitsPinata,
	}, nil
}
