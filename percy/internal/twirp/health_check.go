package twirp

import (
	"context"

	rpc "code.justin.tv/commerce/percy/rpc"
)

func (p *Percy) HealthCheck(ctx context.Context, req *rpc.HealthCheckReq) (*rpc.HealthCheckResp, error) {
	return &rpc.HealthCheckResp{
		Response: "https://www.youtube.com/watch?v=aA_41xCcptI",
	}, nil
}
