package twirp

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/logrus"
	rpc "code.justin.tv/commerce/percy/rpc"
	"github.com/twitchtv/twirp"
)

const (
	getOngoingHypeTrainTimeout = 1 * time.Second
)

func (p *Percy) GetOngoingHypeTrain(ctx context.Context, req *rpc.GetOngoingHypeTrainReq) (*rpc.GetOngoingHypeTrainResp, error) {
	ctx, cancel := context.WithTimeout(ctx, getOngoingHypeTrainTimeout)
	defer cancel()

	if req == nil {
		return nil, errors.New("request is missing")
	}

	if len(req.ChannelId) == 0 {
		return nil, twirp.RequiredArgumentError("channel_id")
	}

	hypeTrain, approaching, err := p.engine.GetOngoingHypeTrain(ctx, req.ChannelId)
	if err != nil {
		logrus.WithError(err).WithField("channelID", req.ChannelId).Error("Failed to get hype train")
		return nil, err
	}

	if hypeTrain != nil && !hypeTrain.Config.IsWhitelisted {
		return &rpc.GetOngoingHypeTrainResp{
			HypeTrain: nil,
		}, nil
	}

	if hypeTrain == nil {
		if approaching == nil {
			return &rpc.GetOngoingHypeTrainResp{
				HypeTrain: nil,
			}, nil
		}

		twirpApproaching := p.converter.ToTwirpApproaching(*approaching)
		return &rpc.GetOngoingHypeTrainResp{
			HypeTrain:   nil,
			Approaching: &twirpApproaching,
		}, nil
	}

	twirpHypeTrain, err := p.converter.ToTwirpHypeTrain(*hypeTrain)
	if err != nil {
		logrus.WithError(err).WithField("channelID", req.ChannelId).Error("Failed to convert to twirp hype train struct")
		return nil, err
	}

	return &rpc.GetOngoingHypeTrainResp{
		HypeTrain:   &twirpHypeTrain,
		Approaching: nil,
	}, nil
}
