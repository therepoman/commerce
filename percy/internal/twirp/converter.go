package twirp

import (
	"errors"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/rpc"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
)

//go:generate counterfeiter . Converter

type Converter interface {
	ToTwirpHypeTrainConfig(percy.HypeTrainConfig) (rpc.HypeTrainConfig, error)
	ToTwirpHypeTrain(percy.HypeTrain) (rpc.HypeTrain, error)
	ToTwirpDifficulty(percy.Difficulty) rpc.Difficulty
	ToTwirpConductor(conductor percy.Conductor) rpc.Conductor
	ToTwirpSource(source percy.ParticipationSource) rpc.ParticipationSource
	ToTwirpAction(action percy.ParticipationAction) rpc.ParticipationAction
	ToTwirpEndingReason(train percy.HypeTrain) rpc.EndingReason
	ToTwirpParticipations(participationTotals percy.ParticipationTotals) []*rpc.Participation
	ToTwirpDifficultySettings(settings percy.DifficultySettings) []*rpc.DifficultySetting
	ToTwirpLevelSettings(settings ...percy.LevelSetting) []*rpc.LevelSetting
	ToTwirpConductorRewards(rewards percy.ConductorRewards) []*rpc.ConductorReward
	ToTwirpParticipationConversionRates(converstionRates map[percy.ParticipationIdentifier]percy.ParticipationPoint) []*rpc.ParticipationConversionRate
	ToTwirpNotificationThresholds(thresholds map[percy.ParticipationIdentifier]int) []*rpc.NotificationThreshold
	ToTwirpRewards(rewards ...percy.Reward) []*rpc.Reward
	ToTwirpRewardType(rewardType percy.RewardType) rpc.RewardType
	ToTwirpConductorType(conductorType percy.ConductorType) rpc.ConductorType
	ToTwirpProgress(progress percy.Progress) *rpc.Progress
	ToTwirpBadgeType(badge percy.Badge) rpc.BadgeType
	ToTwirpApproaching(approaching percy.Approaching) rpc.Approaching
	ToDifficulty(rpc.Difficulty) percy.Difficulty
}

type converter struct{}

func NewConverter() *converter {
	return &converter{}
}

func (converter *converter) ToTwirpHypeTrain(hypeTrain percy.HypeTrain) (rpc.HypeTrain, error) {
	config, err := converter.ToTwirpHypeTrainConfig(hypeTrain.Config)
	if err != nil {
		return rpc.HypeTrain{}, errors.New("failed to get config")
	}

	conductors := make([]*rpc.Conductor, 0)
	for _, conductor := range hypeTrain.Conductors {
		twirpConductor := converter.ToTwirpConductor(conductor)
		conductors = append(conductors, &twirpConductor)
	}

	startedAt, err := ptypes.TimestampProto(hypeTrain.StartedAt)
	if err != nil {
		return rpc.HypeTrain{}, errors.New("error converting startedAt")
	}

	expiresAt, err := ptypes.TimestampProto(hypeTrain.ExpiresAt)
	if err != nil {
		return rpc.HypeTrain{}, errors.New("error converting expiresAt")
	}

	updatedAt, err := ptypes.TimestampProto(hypeTrain.UpdatedAt)
	if err != nil {
		return rpc.HypeTrain{}, errors.New("error converting updatedAt")
	}

	var endedAt *timestamp.Timestamp
	if hypeTrain.EndedAt != nil {
		endedAt, err = ptypes.TimestampProto(*hypeTrain.EndedAt)
		if err != nil {
			return rpc.HypeTrain{}, errors.New("error converting endedAt")
		}
	}

	return rpc.HypeTrain{
		ChannelId:      hypeTrain.ChannelID,
		Id:             hypeTrain.ID,
		StartedAt:      startedAt,
		ExpiresAt:      expiresAt,
		UpdatedAt:      updatedAt,
		EndedAt:        endedAt,
		EndingReason:   converter.ToTwirpEndingReason(hypeTrain),
		Config:         &config,
		Participations: converter.ToTwirpParticipations(hypeTrain.Participations),
		Conductors:     conductors,
		IsActive:       hypeTrain.IsActive(),
		LevelProgress:  converter.ToTwirpProgress(hypeTrain.LevelProgress()),
		// Stubbing out response to unblock GQL integration
		BoostOpportunity: &rpc.BoostOpportunity{
			HasOpportunity:   false,
			OpportunityState: rpc.BoostOpportunityState_BOOST_OPPORTUNITY_STATE_UNKNOWN,
		},
	}, nil
}

func (converter *converter) ToTwirpHypeTrainConfig(config percy.HypeTrainConfig) (rpc.HypeTrainConfig, error) {
	var rewardEndDate *timestamp.Timestamp
	var err error
	if config.RewardEndDate != nil {
		rewardEndDate, err = ptypes.TimestampProto(*config.RewardEndDate)
		if err != nil {
			return rpc.HypeTrainConfig{}, errors.New("error converting reward_end_date")
		}
	}

	return rpc.HypeTrainConfig{
		Enabled: config.Enabled,
		KickoffConfig: &rpc.KickoffConfig{
			NumOfEvents:     int64(config.KickoffConfig.NumOfEvents),
			MinPoints:       int64(config.KickoffConfig.MinPoints),
			DurationSeconds: int64(config.KickoffConfig.Duration.Seconds()),
		},
		CooldownDurationMinutes:      int64(config.CooldownDuration.Minutes()),
		LevelDurationSeconds:         int64(config.LevelDuration.Seconds()),
		Difficulty:                   converter.ToTwirpDifficulty(config.Difficulty),
		RewardEndDate:                rewardEndDate,
		DifficultySettings:           converter.ToTwirpDifficultySettings(config.LevelSettingsByDifficulty),
		ConductorRewards:             converter.ToTwirpConductorRewards(config.ConductorRewards),
		ParticipationConversionRates: converter.ToTwirpParticipationConversionRates(config.ParticipationConversionRates),
		NotificationThresholds:       converter.ToTwirpNotificationThresholds(config.NotificationThresholds),
		IsWhitelisted:                config.IsWhitelisted,
		UseCreatorColor:              config.UseCreatorColor,
		PrimaryHexColor:              config.PrimaryHexColor,
		CalloutEmoteId:               config.CalloutEmoteID,
		UsePersonalizedSettings:      config.UsePersonalizedSettings,
	}, nil
}

func (converter *converter) ToTwirpConductor(conductor percy.Conductor) rpc.Conductor {
	return rpc.Conductor{
		Source:         converter.ToTwirpSource(conductor.Source),
		UserId:         conductor.User.ID(),
		Participations: converter.ToTwirpParticipations(conductor.Participations),
	}
}

func (converter *converter) ToTwirpSource(source percy.ParticipationSource) rpc.ParticipationSource {
	switch source {
	case percy.BitsSource:
		return rpc.ParticipationSource_BITS
	case percy.SubsSource:
		return rpc.ParticipationSource_SUBS
	case percy.AdminSource:
		return rpc.ParticipationSource_ADMIN_SOURCE
	case percy.UnknownSource:
		fallthrough
	default:
		return rpc.ParticipationSource_UNKNOWN_SOURCE
	}
}

func (converter *converter) ToTwirpAction(action percy.ParticipationAction) rpc.ParticipationAction {
	switch action {
	case percy.CheerAction:
		return rpc.ParticipationAction_CHEER
	case percy.ExtensionAction:
		return rpc.ParticipationAction_EXTENSION
	case percy.PollAction:
		return rpc.ParticipationAction_POLL
	case percy.Tier1SubAction:
		return rpc.ParticipationAction_TIER_1_SUB
	case percy.Tier2SubAction:
		return rpc.ParticipationAction_TIER_2_SUB
	case percy.Tier3SubAction:
		return rpc.ParticipationAction_TIER_3_SUB
	case percy.Tier1GiftedSubAction:
		return rpc.ParticipationAction_TIER_1_GIFTED_SUB
	case percy.Tier2GiftedSubAction:
		return rpc.ParticipationAction_TIER_2_GIFTED_SUB
	case percy.Tier3GiftedSubAction:
		return rpc.ParticipationAction_TIER_3_GIFTED_SUB
	case percy.AdminAction:
		return rpc.ParticipationAction_ADMIN_ACTION
	case percy.UnknownAction:
		fallthrough
	default:
		return rpc.ParticipationAction_UNKNOWN_ACTION
	}
}

func (converter *converter) ToTwirpEndingReason(train percy.HypeTrain) rpc.EndingReason {
	if train.IsActive() {
		return rpc.EndingReason_IN_PROGRESS
	}

	if train.EndingReason == nil {
		logrus.WithFields(logrus.Fields{
			"hypeTrainID": train.ID,
		}).Warn("Found an inactive hype train with no ending reason")
		return rpc.EndingReason_UNKNOWN_REASON
	}

	switch *train.EndingReason {
	case percy.EndingReasonExpired:
		return rpc.EndingReason_EXPIRED
	case percy.EndingReasonCompleted:
		return rpc.EndingReason_COMPLETED
	case percy.EndingReasonError:
		fallthrough
	default:
		return rpc.EndingReason_UNKNOWN_REASON
	}
}

func (converter *converter) ToTwirpParticipations(participationTotals percy.ParticipationTotals) []*rpc.Participation {
	if len(participationTotals) == 0 {
		return nil
	}

	participations := make([]*rpc.Participation, 0, len(participationTotals))
	for identifier, quantity := range participationTotals {
		participations = append(participations, &rpc.Participation{
			Source:   converter.ToTwirpSource(identifier.Source),
			Action:   converter.ToTwirpAction(identifier.Action),
			Quantity: int64(quantity),
		})
	}

	return participations
}

func (converter *converter) ToDifficulty(difficulty rpc.Difficulty) percy.Difficulty {
	switch difficulty {
	case rpc.Difficulty_EASY:
		return percy.DifficultyEasy
	case rpc.Difficulty_MEDIUM:
		return percy.DifficultyMedium
	case rpc.Difficulty_HARD:
		return percy.DifficultyHard
	case rpc.Difficulty_SUPER_HARD:
		return percy.DifficultySuperHard
	case rpc.Difficulty_INSANE:
		return percy.DifficultyInsane
	case rpc.Difficulty_UNKNOWN_DIFFICULTY:
		fallthrough
	default:
		return percy.DifficultyUnknown
	}
}

func (converter *converter) ToTwirpDifficulty(difficulty percy.Difficulty) rpc.Difficulty {
	var converted rpc.Difficulty
	switch difficulty {
	case percy.DifficultyEasy:
		converted = rpc.Difficulty_EASY
	case percy.DifficultyMedium:
		converted = rpc.Difficulty_MEDIUM
	case percy.DifficultyHard:
		converted = rpc.Difficulty_HARD
	case percy.DifficultySuperHard:
		converted = rpc.Difficulty_SUPER_HARD
	case percy.DifficultyInsane:
		converted = rpc.Difficulty_INSANE
	case percy.DifficultyUnknown:
		fallthrough
	default:
		converted = rpc.Difficulty_UNKNOWN_DIFFICULTY
	}

	return converted
}

func (converter *converter) ToTwirpDifficultySettings(settings percy.DifficultySettings) []*rpc.DifficultySetting {
	twirpSettings := make([]*rpc.DifficultySetting, 0, len(settings))
	for difficulty, levelSetting := range settings {
		twirpSettings = append(twirpSettings, &rpc.DifficultySetting{
			Difficulty:    converter.ToTwirpDifficulty(difficulty),
			LevelSettings: converter.ToTwirpLevelSettings(levelSetting...),
		})
	}

	return twirpSettings
}

func (converter *converter) ToTwirpLevelSettings(settings ...percy.LevelSetting) []*rpc.LevelSetting {
	twirpSettings := make([]*rpc.LevelSetting, 0, len(settings))
	for _, setting := range settings {
		twirpSettings = append(twirpSettings, &rpc.LevelSetting{
			Level:   int64(setting.Level),
			Goal:    int64(setting.Goal),
			Rewards: converter.ToTwirpRewards(setting.Rewards...),
		})
	}

	return twirpSettings
}

func (converter *converter) ToTwirpRewards(rewards ...percy.Reward) []*rpc.Reward {
	twirpRewards := make([]*rpc.Reward, 0, len(rewards))
	for _, reward := range rewards {
		twirpRewards = append(twirpRewards, &rpc.Reward{
			Id:      reward.ID,
			GroupId: reward.GroupID,
			Type:    converter.ToTwirpRewardType(reward.Type),
		})
	}

	return twirpRewards
}

func (converter *converter) ToTwirpRewardType(rewardType percy.RewardType) rpc.RewardType {
	switch rewardType {
	case percy.BadgeReward:
		return rpc.RewardType_BADGE
	case percy.EmoteReward:
		return rpc.RewardType_EMOTE
	case percy.UnknownRewards:
		fallthrough
	default:
		return rpc.RewardType_UNKNOWN_REWARD_TYPE
	}
}

func (converter *converter) ToTwirpConductorType(conductorType percy.ConductorType) rpc.ConductorType {
	switch conductorType {
	case percy.FormerConductor:
		return rpc.ConductorType_FORMER
	case percy.CurrentConductor:
		return rpc.ConductorType_CURRENT
	default:
		return rpc.ConductorType_UNKNOWN_CONDUCTOR_TYPE
	}
}

func (converter *converter) ToTwirpConductorRewards(rewards percy.ConductorRewards) []*rpc.ConductorReward {
	var twirpRewards []*rpc.ConductorReward
	for source, rewardsByType := range rewards {
		for conductorType, rewards := range rewardsByType {
			twirpRewards = append(twirpRewards, &rpc.ConductorReward{
				Source:  converter.ToTwirpSource(source),
				Type:    converter.ToTwirpConductorType(conductorType),
				Rewards: converter.ToTwirpRewards(rewards...),
			})
		}
	}

	return twirpRewards
}

func (converter *converter) ToTwirpParticipationConversionRates(converstionRates map[percy.ParticipationIdentifier]percy.ParticipationPoint) []*rpc.ParticipationConversionRate {
	rates := make([]*rpc.ParticipationConversionRate, 0, len(converstionRates))
	for identifier, rate := range converstionRates {
		rates = append(rates, &rpc.ParticipationConversionRate{
			Source: converter.ToTwirpSource(identifier.Source),
			Action: converter.ToTwirpAction(identifier.Action),
			Rate:   int64(rate),
		})
	}

	return rates
}

func (converter *converter) ToTwirpNotificationThresholds(thresholds map[percy.ParticipationIdentifier]int) []*rpc.NotificationThreshold {
	twirpThresholds := make([]*rpc.NotificationThreshold, 0, len(thresholds))
	for identifier, quantity := range thresholds {
		twirpThresholds = append(twirpThresholds, &rpc.NotificationThreshold{
			Source:   converter.ToTwirpSource(identifier.Source),
			Action:   converter.ToTwirpAction(identifier.Action),
			Quantity: int64(quantity),
		})
	}

	return twirpThresholds
}

func (converter *converter) ToTwirpProgress(progress percy.Progress) *rpc.Progress {
	return &rpc.Progress{
		Setting:          converter.ToTwirpLevelSettings(progress.Setting)[0],
		Progression:      int64(progress.Progression),
		Goal:             int64(progress.Goal),
		Total:            int64(progress.Total),
		RemainingSeconds: int64(progress.RemainingSeconds),
	}
}

func (converter *converter) ToTwirpBadgeType(badge percy.Badge) rpc.BadgeType {
	switch badge {
	case percy.FormerConductorBadge:
		return rpc.BadgeType_FORMER_CONDUCTOR
	case percy.CurrentConductorBadge:
		return rpc.BadgeType_CURRENT_CONDUCTOR
	case percy.UnknownBadge:
		fallthrough
	default:
		return rpc.BadgeType_UNKNOWN_BADGE
	}
}

func (converter *converter) ToTwirpApproaching(approaching percy.Approaching) rpc.Approaching {
	eventsRemainingSeconds := map[int64]int64{}

	for k, v := range approaching.EventsRemainingDuration {
		eventsRemainingSeconds[int64(k)] = int64(v.Seconds())
	}
	return rpc.Approaching{
		Approaching:            true,
		CreatorColor:           approaching.CreatorColor,
		Goal:                   approaching.Goal,
		Participants:           approaching.Participants,
		EventsRemainingSeconds: eventsRemainingSeconds,
		LevelOneRewards:        converter.ToTwirpRewards(approaching.LevelOneRewards...),
	}
}
