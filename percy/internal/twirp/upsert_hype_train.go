package twirp

import (
	"context"
	"errors"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/rpc"
	"github.com/sirupsen/logrus"
)

func (p *Percy) UpsertHypeTrain(ctx context.Context, req *rpc.UpsertHypeTrainReq) (*rpc.UpsertHypeTrainResp, error) {
	if req == nil {
		return nil, errors.New("request is missing")
	}
	if req.HypeTrain == nil {
		return nil, errors.New("hype train is missing")
	}

	if err := validateHypeTrain(req.HypeTrain); err != nil {
		return nil, err
	}

	if err := p.engine.UpsertHypeTrain(ctx, percy.HypeTrain{}); err != nil {
		logrus.WithError(err).Error("error on upserting hype train instance")
		return nil, err
	}

	return &rpc.UpsertHypeTrainResp{}, nil
}

func validateHypeTrain(hypeTrain *rpc.HypeTrain) error {
	if len(hypeTrain.ChannelId) == 0 {
		return errors.New("channel_id is missing")
	}

	if hypeTrain.StartedAt == nil {
		return errors.New("started_at is missing")
	}

	if hypeTrain.ExpiresAt == nil {
		return errors.New("expires_at is missing")
	}

	if hypeTrain.UpdatedAt == nil {
		return errors.New("updated_at is missing")
	}

	if hypeTrain.Config == nil {
		return errors.New("config is missing")
	}

	if hypeTrain.Conductors == nil {
		return errors.New("conductors is missing")
	}
	return nil
}
