package twirp_twitchiversary

import (
	percy "code.justin.tv/commerce/percy/internal"
)

type Twitchiversary struct {
	twitchiversaryEngine percy.TwitchiversaryEngine
}

// NewTwitchiversary...
func NewTwitchiversary(twitchiversaryEngine percy.TwitchiversaryEngine) *Twitchiversary {
	return &Twitchiversary{
		twitchiversaryEngine: twitchiversaryEngine,
	}
}
