package twirp_twitchiversary

import (
	"context"
	"time"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	rpc_twitchiversary "code.justin.tv/commerce/percy/rpc/twitchiversary"
	"github.com/twitchtv/twirp"
)

const (
	isUserAnniversaryTimeout = 200 * time.Millisecond
)

// IsUserAnniversary returns if it is the user's anniversary and anniversary years
func (p *Twitchiversary) IsUserAnniversary(ctx context.Context, req *rpc_twitchiversary.IsUserAnniversaryReq) (*rpc_twitchiversary.IsUserAnniversaryResp, error) {
	ctx, cancel := context.WithTimeout(ctx, isUserAnniversaryTimeout)
	defer cancel()

	if req.UserId == "" {
		return nil, twirp.RequiredArgumentError("user_id")
	}

	isUserAnniversary, anniversaryYears, err := p.twitchiversaryEngine.IsUserAnniversary(ctx, req.UserId, time.Now())
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"userID": req.UserId,
		}).Error("Failed to get user anniversary")
		return nil, twirp.InternalErrorWith(err)
	}

	return &rpc_twitchiversary.IsUserAnniversaryResp{
		IsUserAnniversary: isUserAnniversary,
		AnniversaryYears:  anniversaryYears,
	}, nil
}

// GetUserAnniversaryInfo returns if it is the user's anniversary, anniverary years, and whether they can send a user notice
func (p *Twitchiversary) GetUserAnniversaryInfo(ctx context.Context, req *rpc_twitchiversary.GetUserAnniversaryInfoReq) (*rpc_twitchiversary.GetUserAnniversaryInfoResp, error) {
	ctx, cancel := context.WithTimeout(ctx, isUserAnniversaryTimeout)
	defer cancel()

	if req.UserId == "" {
		return nil, twirp.RequiredArgumentError("user_id")
	}

	if req.Authed {
		isUserAnniversary, anniversaryYears, canSendUserNotice, err := p.twitchiversaryEngine.CanSendUserNotice(ctx, req.UserId)
		if err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{
				"userID": req.UserId,
			}).Error("Failed to get user anniversary token")
			return nil, twirp.InternalErrorWith(err)
		}

		return &rpc_twitchiversary.GetUserAnniversaryInfoResp{
			IsUserAnniversary: isUserAnniversary,
			AnniversaryYears:  anniversaryYears,
			CanSendUserNotice: canSendUserNotice,
		}, nil
	}

	isUserAnniversary, anniversaryYears, err := p.twitchiversaryEngine.IsUserAnniversary(ctx, req.UserId, time.Now())
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"userID": req.UserId,
		}).Error("Failed to get user anniversary")
		return nil, twirp.InternalErrorWith(err)
	}

	return &rpc_twitchiversary.GetUserAnniversaryInfoResp{
		IsUserAnniversary: isUserAnniversary,
		AnniversaryYears:  anniversaryYears,
		CanSendUserNotice: false,
	}, nil
}

// SendDimsissUserNotice dimisses the callout and sends the user notice if the user chooses to
func (p *Twitchiversary) SendDimsissUserNotice(ctx context.Context, req *rpc_twitchiversary.SendDimsissUserNoticeReq) (*rpc_twitchiversary.SendDimsissUserNoticeResp, error) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	userID := req.UserId
	if userID == "" {
		return nil, twirp.RequiredArgumentError("user_id")
	}
	isUserAnniversary, anniversaryYears, canSendUserNotice, err := p.twitchiversaryEngine.CanSendUserNotice(ctx, userID)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"userID": req.UserId,
		}).Error("Failed to get user anniversary token")
		return nil, twirp.InternalErrorWith(err)
	}
	if !isUserAnniversary {
		return &rpc_twitchiversary.SendDimsissUserNoticeResp{
				IsUserAnniversary: isUserAnniversary,
				AnniversaryYears:  anniversaryYears,
				CanSendUserNotice: false,
			}, rpc_twitchiversary.NewClientError(
				twirp.NewError(twirp.FailedPrecondition, "not user anniversary"),
				rpc_twitchiversary.ErrCodeNotUserAnniversary)
	}
	if !canSendUserNotice {
		return &rpc_twitchiversary.SendDimsissUserNoticeResp{
				IsUserAnniversary: isUserAnniversary,
				AnniversaryYears:  anniversaryYears,
				CanSendUserNotice: false,
			}, rpc_twitchiversary.NewClientError(
				twirp.NewError(twirp.FailedPrecondition, "token already used"),
				rpc_twitchiversary.ErrCodeTokenAlreadyUsed)
	}

	if req.SendUserNotice {
		if req.ChannelId == "" {
			return nil, twirp.RequiredArgumentError("channel_id")
		}
		err := p.twitchiversaryEngine.SendUserAnniversaryNotice(ctx, percy.UserAnniversaryNoticeArgs{
			SenderID:  userID,
			ChannelID: req.ChannelId,
			Message:   req.Message,
			Years:     anniversaryYears,
		})
		if err != nil {
			return &rpc_twitchiversary.SendDimsissUserNoticeResp{
				IsUserAnniversary: isUserAnniversary,
				AnniversaryYears:  anniversaryYears,
				CanSendUserNotice: true,
			}, twirp.InternalErrorWith(err)
		}
	}

	err = p.twitchiversaryEngine.AddChatNotificationToken(ctx, userID, anniversaryYears)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"userID": req.UserId,
		}).Error("Failed to add user anniversary token")
		return &rpc_twitchiversary.SendDimsissUserNoticeResp{
			IsUserAnniversary: isUserAnniversary,
			AnniversaryYears:  anniversaryYears,
			CanSendUserNotice: false,
		}, twirp.InternalErrorWith(err)
	}

	return &rpc_twitchiversary.SendDimsissUserNoticeResp{
		IsUserAnniversary: isUserAnniversary,
		AnniversaryYears:  anniversaryYears,
		CanSendUserNotice: false,
	}, nil
}

func (p *Twitchiversary) DeleteUserAnniversaryToken(ctx context.Context, req *rpc_twitchiversary.DeleteUserAnniversaryTokenReq) (*rpc_twitchiversary.DeleteUserAnniversaryTokenResp, error) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	err := p.twitchiversaryEngine.DeleteChatNotificationToken(ctx, req.UserId, req.AnniversaryYears)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"userID": req.UserId,
		}).Error("Failed to delete user anniversary token")
		return nil, twirp.InternalErrorWith(err)
	}

	return &rpc_twitchiversary.DeleteUserAnniversaryTokenResp{}, nil
}
