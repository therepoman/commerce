package twirp_twitchiversary_test

import (
	"context"
	"errors"
	"testing"

	"code.justin.tv/commerce/percy/internal/internalfakes"
	twitchiversary_twirp "code.justin.tv/commerce/percy/internal/twirp/twitchiversary"
	twitchiversary_rpc "code.justin.tv/commerce/percy/rpc/twitchiversary"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/twitchtv/twirp"
)

func TestTwitchiversary_IsUserAnniversary(t *testing.T) {
	Convey("Given a Twirp server", t, func() {
		engine := &internalfakes.FakeTwitchiversaryEngine{}

		p := twitchiversary_twirp.NewTwitchiversary(engine)

		ctx := context.Background()

		userID := "1234567890"
		req := &twitchiversary_rpc.IsUserAnniversaryReq{
			UserId: userID,
		}

		Convey("when engine returns true", func() {
			engine.IsUserAnniversaryReturns(true, 10, nil)
			resp, err := p.IsUserAnniversary(ctx, req)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.IsUserAnniversary, ShouldBeTrue)
			So(resp.AnniversaryYears, ShouldEqual, 10)
		})
		Convey("when engine returns false", func() {
			engine.IsUserAnniversaryReturns(false, 0, nil)
			resp, err := p.IsUserAnniversary(ctx, req)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.IsUserAnniversary, ShouldBeFalse)
		})
		Convey("when engine returns error", func() {
			engine.IsUserAnniversaryReturns(false, 0, errors.New("user login is missing"))
			resp, err := p.IsUserAnniversary(ctx, req)

			So(err, ShouldNotBeNil)
			So(resp, ShouldBeNil)
			So(err, ShouldResemble, twirp.InternalErrorWith(errors.New("user login is missing")))
		})

		Convey("when user id is null", func() {
			userID := ""
			req := &twitchiversary_rpc.IsUserAnniversaryReq{
				UserId: userID,
			}
			resp, err := p.IsUserAnniversary(ctx, req)

			So(err, ShouldNotBeNil)
			So(resp, ShouldBeNil)
			So(err, ShouldResemble, twirp.RequiredArgumentError("user_id"))
		})
	})
}

func TestTwitchiversary_CanSendUserNotice(t *testing.T) {
	Convey("Given a Twirp server", t, func() {
		engine := &internalfakes.FakeTwitchiversaryEngine{}

		p := twitchiversary_twirp.NewTwitchiversary(engine)

		ctx := context.Background()

		userID := "1234567890"

		Convey("when user id is null", func() {
			req := &twitchiversary_rpc.GetUserAnniversaryInfoReq{
				UserId: "",
				Authed: false,
			}
			resp, err := p.GetUserAnniversaryInfo(ctx, req)
			So(err, ShouldNotBeNil)
			So(resp, ShouldBeNil)
			So(err, ShouldResemble, twirp.RequiredArgumentError("user_id"))
		})

		Convey("when not authed", func() {
			req := &twitchiversary_rpc.GetUserAnniversaryInfoReq{
				UserId: userID,
				Authed: false,
			}
			Convey("should return false", func() {
				engine.IsUserAnniversaryReturns(true, 10, nil)
				resp, err := p.GetUserAnniversaryInfo(ctx, req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.IsUserAnniversary, ShouldBeTrue)
				So(resp.AnniversaryYears, ShouldEqual, 10)
				So(resp.CanSendUserNotice, ShouldBeFalse)
			})
			Convey("when engine returns error", func() {
				engine.IsUserAnniversaryReturns(false, 0, errors.New("user login is missing"))
				resp, err := p.GetUserAnniversaryInfo(ctx, req)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
				So(err, ShouldResemble, twirp.InternalErrorWith(errors.New("user login is missing")))
			})
		})

		Convey("when authed", func() {
			req := &twitchiversary_rpc.GetUserAnniversaryInfoReq{
				UserId: userID,
				Authed: true,
			}
			Convey("when engine returns true", func() {
				engine.CanSendUserNoticeReturns(true, 10, true, nil)
				resp, err := p.GetUserAnniversaryInfo(ctx, req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.IsUserAnniversary, ShouldBeTrue)
				So(resp.AnniversaryYears, ShouldEqual, 10)
				So(resp.CanSendUserNotice, ShouldBeTrue)
			})
			Convey("when engine returns false", func() {
				engine.CanSendUserNoticeReturns(true, 10, false, nil)
				resp, err := p.GetUserAnniversaryInfo(ctx, req)
				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.IsUserAnniversary, ShouldBeTrue)
				So(resp.AnniversaryYears, ShouldEqual, 10)
				So(resp.CanSendUserNotice, ShouldBeFalse)
			})
			Convey("when engine returns error", func() {
				engine.CanSendUserNoticeReturns(true, 10, false, errors.New("oops"))
				resp, err := p.GetUserAnniversaryInfo(ctx, req)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
				So(err, ShouldResemble, twirp.InternalErrorWith(errors.New("oops")))
			})
		})
	})
}

func TestTwitchiversary_SendDimsissUserNotice(t *testing.T) {
	Convey("Given a Twirp server", t, func() {
		engine := &internalfakes.FakeTwitchiversaryEngine{}

		p := twitchiversary_twirp.NewTwitchiversary(engine)

		ctx := context.Background()

		userID := "1234567890"
		channelID := "0987654321"

		Convey("when user id is null", func() {
			req := &twitchiversary_rpc.SendDimsissUserNoticeReq{
				UserId:         "",
				ChannelId:      "",
				SendUserNotice: false,
				Message:        "hello",
			}
			resp, err := p.SendDimsissUserNotice(ctx, req)
			So(err, ShouldNotBeNil)
			So(resp, ShouldBeNil)
			So(err, ShouldResemble, twirp.RequiredArgumentError("user_id"))
		})

		Convey("when not allowed to send user notice", func() {
			req := &twitchiversary_rpc.SendDimsissUserNoticeReq{
				UserId:         userID,
				ChannelId:      channelID,
				SendUserNotice: true,
				Message:        "hello",
			}
			Convey("when engine returns error", func() {
				engine.CanSendUserNoticeReturns(true, 10, false, errors.New("oops"))
				resp, err := p.SendDimsissUserNotice(ctx, req)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
				So(err, ShouldResemble, twirp.InternalErrorWith(errors.New("oops")))
			})
			Convey("when engine returns not anniversary", func() {
				engine.CanSendUserNoticeReturns(false, 0, false, nil)
				resp, err := p.SendDimsissUserNotice(ctx, req)
				So(err, ShouldNotBeNil)
				So(resp.IsUserAnniversary, ShouldBeFalse)
				So(resp.AnniversaryYears, ShouldEqual, 0)
				So(resp.CanSendUserNotice, ShouldBeFalse)
				errorCode, parseErr := getClientError(err)
				So(parseErr, ShouldBeNil)
				So(errorCode, ShouldEqual, twitchiversary_rpc.ErrCodeNotUserAnniversary)
			})
			Convey("when engine returns cannot send user notice", func() {
				engine.CanSendUserNoticeReturns(true, 10, false, nil)
				resp, err := p.SendDimsissUserNotice(ctx, req)
				So(err, ShouldNotBeNil)
				So(resp.IsUserAnniversary, ShouldBeTrue)
				So(resp.AnniversaryYears, ShouldEqual, 10)
				So(resp.CanSendUserNotice, ShouldBeFalse)
				errorCode, parseErr := getClientError(err)
				So(parseErr, ShouldBeNil)
				So(errorCode, ShouldEqual, twitchiversary_rpc.ErrCodeTokenAlreadyUsed)
			})
		})

		Convey("when sending user notice", func() {
			req := &twitchiversary_rpc.SendDimsissUserNoticeReq{
				UserId:         userID,
				ChannelId:      channelID,
				SendUserNotice: true,
				Message:        "hello",
			}
			engine.CanSendUserNoticeReturns(true, 10, true, nil)

			Convey("when SendUserAnniversaryNotice returns error", func() {
				engine.SendUserAnniversaryNoticeReturns(errors.New("oops"))
				resp, err := p.SendDimsissUserNotice(ctx, req)
				So(err, ShouldNotBeNil)
				So(resp.IsUserAnniversary, ShouldBeTrue)
				So(resp.AnniversaryYears, ShouldEqual, 10)
				So(resp.CanSendUserNotice, ShouldBeTrue)
				So(err, ShouldResemble, twirp.InternalErrorWith(errors.New("oops")))
			})
			Convey("when AddChatNotificationToken returns error", func() {
				engine.SendUserAnniversaryNoticeReturns(nil)
				engine.AddChatNotificationTokenReturns(errors.New("oops"))
				resp, err := p.SendDimsissUserNotice(ctx, req)
				So(err, ShouldNotBeNil)
				So(resp.IsUserAnniversary, ShouldBeTrue)
				So(resp.AnniversaryYears, ShouldEqual, 10)
				So(resp.CanSendUserNotice, ShouldBeFalse)
				So(err, ShouldResemble, twirp.InternalErrorWith(errors.New("oops")))
			})
			Convey("when everything succeeds", func() {
				engine.SendUserAnniversaryNoticeReturns(nil)
				engine.AddChatNotificationTokenReturns(nil)
				resp, err := p.SendDimsissUserNotice(ctx, req)
				So(err, ShouldBeNil)
				So(resp.IsUserAnniversary, ShouldBeTrue)
				So(resp.AnniversaryYears, ShouldEqual, 10)
				So(resp.CanSendUserNotice, ShouldBeFalse)
			})
			Convey("when channel id is null", func() {
				req := &twitchiversary_rpc.SendDimsissUserNoticeReq{
					UserId:         userID,
					ChannelId:      "",
					SendUserNotice: true,
					Message:        "hello",
				}
				resp, err := p.SendDimsissUserNotice(ctx, req)
				So(err, ShouldNotBeNil)
				So(resp, ShouldBeNil)
				So(err, ShouldResemble, twirp.RequiredArgumentError("channel_id"))
			})
		})

		Convey("when not sending user notice", func() {
			req := &twitchiversary_rpc.SendDimsissUserNoticeReq{
				UserId:         userID,
				ChannelId:      "",
				SendUserNotice: false,
				Message:        "hello",
			}
			engine.CanSendUserNoticeReturns(true, 10, true, nil)

			Convey("when AddChatNotificationToken returns error", func() {
				engine.AddChatNotificationTokenReturns(errors.New("oops"))
				resp, err := p.SendDimsissUserNotice(ctx, req)
				So(err, ShouldNotBeNil)
				So(resp.IsUserAnniversary, ShouldBeTrue)
				So(resp.AnniversaryYears, ShouldEqual, 10)
				So(resp.CanSendUserNotice, ShouldBeFalse)
				So(err, ShouldResemble, twirp.InternalErrorWith(errors.New("oops")))
			})
			Convey("when everything succeeds", func() {
				engine.AddChatNotificationTokenReturns(nil)
				resp, err := p.SendDimsissUserNotice(ctx, req)
				So(err, ShouldBeNil)
				So(resp.IsUserAnniversary, ShouldBeTrue)
				So(resp.AnniversaryYears, ShouldEqual, 10)
				So(resp.CanSendUserNotice, ShouldBeFalse)
			})
		})
	})
}

func getClientError(err error) (string, error) {
	var clientError twitchiversary_rpc.ClientError
	parseErr := twitchiversary_rpc.ParseClientError(err, &clientError)
	if parseErr != nil {
		return "", parseErr
	}
	return clientError.ErrorCode, nil
}
