package creator_anniversaries_twirp

import (
	"context"
	"time"

	"code.justin.tv/commerce/logrus"
	percy "code.justin.tv/commerce/percy/internal"
	rpc_creator_anniversaries "code.justin.tv/commerce/percy/rpc/creator_anniversaries"
)

const (
	GetCreatorAnniversariesByChannelIdsTimeout = 250 * time.Millisecond
)

type CreatorAnniversaries struct {
	creatorAnniversariesEngine percy.CreatorAnniversariesEngine
}

// New Creator Anniversary...
func NewCreatorAnniversaries(creatorAnniversariesEngine percy.CreatorAnniversariesEngine) *CreatorAnniversaries {
	return &CreatorAnniversaries{
		creatorAnniversariesEngine: creatorAnniversariesEngine,
	}
}

func (p *CreatorAnniversaries) GetCreatorAnniversariesByChannelIds(ctx context.Context, req *rpc_creator_anniversaries.GetCreatorAnniversariesByChannelIdsReq) (*rpc_creator_anniversaries.GetCreatorAnniversariesByChannelIdsResp, error) {
	_, cancel := context.WithTimeout(ctx, GetCreatorAnniversariesByChannelIdsTimeout)
	defer cancel()

	if len(req.ChannelIds) < 1 {
		return nil, nil
	}

	var anniversaries []*rpc_creator_anniversaries.CreatorAnniversary
	creatorAnniversaries, err := p.creatorAnniversariesEngine.GetCreatorAnniversariesByChannelIds(req.ChannelIds)
	if err != nil {
		logrus.WithError(err).Error("error with calling creatorAnniversariesEngine.GetCreatorAnniversariesByChannelIds")
		return nil, err
	}

	for _, anniversary := range creatorAnniversaries {
		isCreatorAnniversary := anniversary.IsCreatorAnniversary
		anniversaryYears := anniversary.CreatorAnniversaryYears
		channelId := anniversary.ChannelId

		anniversary := &rpc_creator_anniversaries.CreatorAnniversary{
			ChannelId:            channelId,
			IsCreatorAnniversary: isCreatorAnniversary,
			AnniversaryYears:     int64(anniversaryYears),
		}

		anniversaries = append(anniversaries, anniversary)
	}

	response := &rpc_creator_anniversaries.GetCreatorAnniversariesByChannelIdsResp{
		CreatorAnniversaries: anniversaries,
	}

	return response, nil
}
