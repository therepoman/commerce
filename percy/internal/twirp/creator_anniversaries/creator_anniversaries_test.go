package creator_anniversaries_twirp_test

import (
	"context"
	"testing"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	twirp_creator_anniversaries "code.justin.tv/commerce/percy/internal/twirp/creator_anniversaries"
	rpc_creator_anniversaries "code.justin.tv/commerce/percy/rpc/creator_anniversaries"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCreatorAnniversaries_GetCreatorAnniversariesByChannelIds(t *testing.T) {
	Convey("Given a Twirp server", t, func() {
		engine := &internalfakes.FakeCreatorAnniversariesEngine{}

		p := twirp_creator_anniversaries.NewCreatorAnniversaries(engine)

		ctx := context.Background()

		Convey("when engine returns map of multiple items", func() {
			// Create mocked engine return data
			var testAnniversaries []percy.CreatorAnniversaryWithYears
			testAnniversaries = append(testAnniversaries, percy.CreatorAnniversaryWithYears{
				Date:                    time.Date(2018, 7, 1, 0, 0, 0, 0, time.UTC),
				ChannelId:               "12121212",
				IsCreatorAnniversary:    true,
				CreatorAnniversaryYears: 3,
			})
			testAnniversaries = append(testAnniversaries, percy.CreatorAnniversaryWithYears{
				Date:                    time.Date(2017, 7, 1, 0, 0, 0, 0, time.UTC),
				ChannelId:               "34343434",
				IsCreatorAnniversary:    true,
				CreatorAnniversaryYears: 4,
			})
			testAnniversaries = append(testAnniversaries, percy.CreatorAnniversaryWithYears{
				Date:                    time.Date(2019, 7, 1, 0, 0, 0, 0, time.UTC),
				ChannelId:               "56565656",
				IsCreatorAnniversary:    true,
				CreatorAnniversaryYears: 2,
			})
			// Create expected test output data
			testCreatorAnniversariesWithYears := []*rpc_creator_anniversaries.CreatorAnniversary{
				{
					ChannelId:            "12121212",
					IsCreatorAnniversary: true,
					AnniversaryYears:     3,
				},
				{
					ChannelId:            "34343434",
					IsCreatorAnniversary: true,
					AnniversaryYears:     4,
				},
				{
					ChannelId:            "56565656",
					IsCreatorAnniversary: true,
					AnniversaryYears:     2,
				},
			}
			testResp := &rpc_creator_anniversaries.GetCreatorAnniversariesByChannelIdsResp{
				CreatorAnniversaries: testCreatorAnniversariesWithYears,
			}

			engine.GetCreatorAnniversariesByChannelIdsReturns(testAnniversaries, nil)
			req := &rpc_creator_anniversaries.GetCreatorAnniversariesByChannelIdsReq{
				ChannelIds: []string{"12121212", "34343434", "56565656"},
			}
			resp, err := p.GetCreatorAnniversariesByChannelIds(ctx, req)

			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(resp, ShouldResemble, testResp)
		})

		Convey("when engine returns nil", func() {
			engine.GetCreatorAnniversariesByChannelIdsReturns(nil, nil)
			req := &rpc_creator_anniversaries.GetCreatorAnniversariesByChannelIdsReq{
				ChannelIds: []string{},
			}
			resp, err := p.GetCreatorAnniversariesByChannelIds(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldBeNil)
		})

		Convey("when input array is empty", func() {
			engine.GetCreatorAnniversariesByChannelIdsReturns(nil, nil)
			req := &rpc_creator_anniversaries.GetCreatorAnniversariesByChannelIdsReq{
				ChannelIds: []string{},
			}
			resp, err := p.GetCreatorAnniversariesByChannelIds(ctx, req)
			So(resp, ShouldBeNil)
			So(err, ShouldBeNil)
		})

		Convey("when input array contains one channelid", func() {
			// Create mocked engine return data
			var testAnniversaries []percy.CreatorAnniversaryWithYears
			testAnniversaries = append(testAnniversaries, percy.CreatorAnniversaryWithYears{
				Date:                    time.Date(2018, 7, 1, 0, 0, 0, 0, time.UTC),
				ChannelId:               "12121212",
				IsCreatorAnniversary:    true,
				CreatorAnniversaryYears: 3,
			})
			// Create expected test output data
			testCreatorAnniversariesWithYears := []*rpc_creator_anniversaries.CreatorAnniversary{
				{
					ChannelId:            "12121212",
					IsCreatorAnniversary: true,
					AnniversaryYears:     3,
				},
			}
			testResp := &rpc_creator_anniversaries.GetCreatorAnniversariesByChannelIdsResp{
				CreatorAnniversaries: testCreatorAnniversariesWithYears,
			}

			engine.GetCreatorAnniversariesByChannelIdsReturns(testAnniversaries, nil)
			req := &rpc_creator_anniversaries.GetCreatorAnniversariesByChannelIdsReq{
				ChannelIds: []string{"12121212"},
			}
			resp, err := p.GetCreatorAnniversariesByChannelIds(ctx, req)

			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(resp, ShouldResemble, testResp)
		})

		Convey("when input array has multiple different channelids", func() {
			// Create mocked engine return data
			var testAnniversaries []percy.CreatorAnniversaryWithYears
			testAnniversaries = append(testAnniversaries, percy.CreatorAnniversaryWithYears{
				Date:                    time.Date(2018, 7, 1, 0, 0, 0, 0, time.UTC),
				ChannelId:               "12121212",
				IsCreatorAnniversary:    true,
				CreatorAnniversaryYears: 3,
			})
			testAnniversaries = append(testAnniversaries, percy.CreatorAnniversaryWithYears{
				Date:                    time.Date(2017, 7, 1, 0, 0, 0, 0, time.UTC),
				ChannelId:               "34343434",
				IsCreatorAnniversary:    true,
				CreatorAnniversaryYears: 4,
			})
			testAnniversaries = append(testAnniversaries, percy.CreatorAnniversaryWithYears{
				Date:                    time.Date(2019, 7, 1, 0, 0, 0, 0, time.UTC),
				ChannelId:               "56565656",
				IsCreatorAnniversary:    true,
				CreatorAnniversaryYears: 2,
			})
			// Create expected test output data
			testCreatorAnniversariesWithYears := []*rpc_creator_anniversaries.CreatorAnniversary{
				{
					ChannelId:            "12121212",
					IsCreatorAnniversary: true,
					AnniversaryYears:     3,
				},
				{
					ChannelId:            "34343434",
					IsCreatorAnniversary: true,
					AnniversaryYears:     4,
				},
				{
					ChannelId:            "56565656",
					IsCreatorAnniversary: true,
					AnniversaryYears:     2,
				},
			}
			testResp := &rpc_creator_anniversaries.GetCreatorAnniversariesByChannelIdsResp{
				CreatorAnniversaries: testCreatorAnniversariesWithYears,
			}

			engine.GetCreatorAnniversariesByChannelIdsReturns(testAnniversaries, nil)
			req := &rpc_creator_anniversaries.GetCreatorAnniversariesByChannelIdsReq{
				ChannelIds: []string{"12121212", "34343434", "56565656"},
			}
			resp, err := p.GetCreatorAnniversariesByChannelIds(ctx, req)

			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(resp, ShouldResemble, testResp)
		})

		Convey("when input array has duplicate channelIds", func() {
			// Create mocked engine return data
			var testAnniversaries []percy.CreatorAnniversaryWithYears
			testAnniversaries = append(testAnniversaries, percy.CreatorAnniversaryWithYears{
				Date:                    time.Date(2018, 7, 1, 0, 0, 0, 0, time.UTC),
				ChannelId:               "12121212",
				IsCreatorAnniversary:    true,
				CreatorAnniversaryYears: 3,
			})
			testAnniversaries = append(testAnniversaries, percy.CreatorAnniversaryWithYears{
				Date:                    time.Date(2019, 7, 1, 0, 0, 0, 0, time.UTC),
				ChannelId:               "56565656",
				IsCreatorAnniversary:    true,
				CreatorAnniversaryYears: 2,
			})
			// Create expected test output data
			testCreatorAnniversariesWithYears := []*rpc_creator_anniversaries.CreatorAnniversary{
				{
					ChannelId:            "12121212",
					IsCreatorAnniversary: true,
					AnniversaryYears:     3,
				},
				{
					ChannelId:            "56565656",
					IsCreatorAnniversary: true,
					AnniversaryYears:     2,
				},
			}
			testResp := &rpc_creator_anniversaries.GetCreatorAnniversariesByChannelIdsResp{
				CreatorAnniversaries: testCreatorAnniversariesWithYears,
			}

			engine.GetCreatorAnniversariesByChannelIdsReturns(testAnniversaries, nil)
			req := &rpc_creator_anniversaries.GetCreatorAnniversariesByChannelIdsReq{
				ChannelIds: []string{"12121212", "12121212", "56565656"},
			}
			resp, err := p.GetCreatorAnniversariesByChannelIds(ctx, req)

			So(resp, ShouldNotBeNil)
			So(err, ShouldBeNil)
			So(resp, ShouldResemble, testResp)
		})
	})
}
