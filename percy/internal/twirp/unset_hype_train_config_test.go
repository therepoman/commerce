package twirp_test

import (
	"context"
	"errors"
	"testing"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/internalfakes"
	"code.justin.tv/commerce/percy/internal/tests"
	"code.justin.tv/commerce/percy/internal/twirp"
	"code.justin.tv/commerce/percy/internal/twirp/twirpfakes"
	"code.justin.tv/commerce/percy/rpc"
	"github.com/golang/protobuf/ptypes/wrappers"
	. "github.com/smartystreets/goconvey/convey"
)

func TestPercy_UnsetHypeTrainConfig(t *testing.T) {
	Convey("given a hype train API", t, func() {
		engine := &internalfakes.FakeEngine{}
		converter := &twirpfakes.FakeConverter{}

		p := twirp.NewPercy(engine, converter)

		ctx := context.Background()

		channelID := "1234567890"
		req := &rpc.UnsetHypeTrainConfigReq{
			ChannelId:               channelID,
			Enabled:                 &wrappers.BoolValue{Value: true},
			NumOfEventsToKickoff:    &wrappers.BoolValue{Value: true},
			CooldownDurationMinutes: &wrappers.BoolValue{Value: true},
			Difficulty:              &wrappers.BoolValue{Value: true},
			UseCreatorColor:         &wrappers.BoolValue{Value: true},
			UsePersonalizedSettings: &wrappers.BoolValue{Value: true},
			CalloutEmoteId:          &wrappers.BoolValue{Value: true},
		}

		updatedConfig := tests.ValidHypeTrainConfig()

		twirpUpdatedConfig := tests.ValidTwirpHypeTrainConfig()

		Convey("should return error", func() {
			Convey("when the request is nil", func() {
				req = nil
			})

			Convey("when the request has no channel ID", func() {
				req.ChannelId = ""
			})

			Convey("when the engine fails", func() {
				engine.UnsetHypeTrainConfigReturns(percy.HypeTrainConfig{}, errors.New("WALRUS STRIKE"))
			})

			Convey("when the converter fails", func() {
				engine.UnsetHypeTrainConfigReturns(updatedConfig, nil)
				converter.ToTwirpHypeTrainConfigReturns(rpc.HypeTrainConfig{}, errors.New("WALRUS STRIKE"))
			})

			_, err := p.UnsetHypeTrainConfig(ctx, req)
			So(err, ShouldNotBeNil)
		})

		Convey("should return updated config", func() {
			Convey("when all dependencies succeed", func() {
				engine.UnsetHypeTrainConfigReturns(updatedConfig, nil)
				converter.ToTwirpHypeTrainConfigReturns(twirpUpdatedConfig, nil)
			})

			resp, err := p.UnsetHypeTrainConfig(ctx, req)
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.ChannelId, ShouldEqual, channelID)
			So(*resp.Config, ShouldResemble, twirpUpdatedConfig)
		})
	})
}
