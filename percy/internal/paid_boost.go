package percy

import (
	"context"
	"errors"
	"time"
)

type BoostOpportunity struct {
	ID               string
	ChannelID        string
	StartedAt        time.Time
	ExpiresAt        time.Time
	EndedAt          *time.Time
	OrderID          *string
	UnitsContributed int
}

func (o *BoostOpportunity) IsActive() bool {
	now := time.Now()
	return o.StartedAt.Before(now) && o.ExpiresAt.After(now) && o.EndedAt == nil
}

func (o BoostOpportunity) Impressions() int {
	return o.UnitsContributed * 1000
}

// TODO: does this work well in practice? move orderID back into BoostOpportunity struct if not
type CompletedBoostOpportunity struct {
	BoostOpportunity
	OrderID string
}

type BoostOpportunityContribution struct {
	ChannelID string
	UserID    string
	OriginID  string // origin ID of the purchase?
	Timestamp time.Time

	Units int
}

type BoostOpportunityConfig struct {
	Cooldown        time.Duration
	Duration        time.Duration
	KickoffDelayMin time.Duration
	KickoffDelayMax time.Duration

	// Allow multiple boost opportunities, bypassing daily and cooldown limits
	SkipKickoffLimitCheck bool

	// To control whether a real boost is launched
	LaunchNoopBoost bool

	// For testing purposes
	FailContributionRecordStep     bool
	FailContributionUserNoticeStep bool
}

// TODO: these values are TBD
var DefaultBoostOpportunityConfig = BoostOpportunityConfig{
	Cooldown:              3 * time.Hour,
	Duration:              10 * time.Minute,
	KickoffDelayMin:       30 * time.Minute,
	KickoffDelayMax:       90 * time.Minute,
	SkipKickoffLimitCheck: false,

	// Remove or flip this to false for launch
	LaunchNoopBoost: true,

	FailContributionRecordStep:     false,
	FailContributionUserNoticeStep: false,
}

func (b BoostOpportunityContribution) Impressions() int {
	return b.Units * 1000
}

type BoostUnitContributionSize string

type BoostsDBReadOptions struct {
	ConsistentRead bool
}

//go:generate counterfeiter . BoostsDB
type BoostsDB interface {
	CreateBoostOpportunity(ctx context.Context, opportunity BoostOpportunity) error

	// GetLatestBoostOpportunity returns the most recent paid boost opportunity for channelID, returns nil if no paid boost opportunity is found
	GetLatestBoostOpportunity(ctx context.Context, channelID string, ops BoostsDBReadOptions) (*BoostOpportunity, error)

	// AddBoostOpportunityContribution adds a new contribution to an active existing paid boost opportunity
	// returns the updated boost opportunity
	// returns an error if no boost opportunity is currently active
	AddBoostOpportunityContribution(ctx context.Context, boostOpportunityID string, contribution BoostOpportunityContribution) (BoostOpportunity, error)

	GetBoostOpportunityContribution(ctx context.Context, boostOpportunityID, originID string, ops BoostsDBReadOptions) (*BoostOpportunityContribution, error)

	EndLatestBoostOpportunity(ctx context.Context, channelID string, endedAt time.Time, orderID *string) error
}

//go:generate counterfeiter . BoostOpportunityScheduler
type BoostOpportunityScheduler interface {
	// ScheduleBoostOpportunityStart schedules an event to start a new boost opportunity in the given channel
	ScheduleBoostOpportunityStart(ctx context.Context, channelID string, startsAt time.Time) error

	// ScheduleBoostOpportunityCompletion schedules an event to complete the provided boost opportunity in the given channel
	ScheduleBoostOpportunityCompletion(ctx context.Context, channelID, boostOpportunityID string, endsAt time.Time) error
}

//go:generate counterfeiter . BoostsPublisher
type BoostsPublisher interface {
	PublishBoostOpportunityStart(ctx context.Context, opportunity BoostOpportunity) error

	PublishBoostOpportunityCompletion(ctx context.Context, opportunity BoostOpportunity) error
}

// TODO: to be implemented by Dynamic config
type BoostExperimentDB interface {
	// IsAllowed checks if the given channel is in the paid boost experiment
	IsAllowed(ctx context.Context, channelID string) (bool, error)
}

//go:generate counterfeiter . BoostsLauncher
type BoostsLauncher interface {
	LaunchBoost(ctx context.Context, opportunity BoostOpportunity) (string, error)
}

type boostsLauncherSwitch struct {
	configsDB    PaidBoostConfigsDB
	launcher     BoostsLauncher
	noopLauncher BoostsLauncher
}

func NewBoostsLaunchSwitch(configsDB PaidBoostConfigsDB, launcher, noopLauncher BoostsLauncher) (*boostsLauncherSwitch, error) {
	if configsDB == nil {
		return nil, errors.New("configsDB must not be nil")
	}

	if launcher == nil {
		return nil, errors.New("launcher must not be nil")
	}

	if noopLauncher == nil {
		return nil, errors.New("noop launcher must not be nil")
	}

	return &boostsLauncherSwitch{
		configsDB:    configsDB,
		launcher:     launcher,
		noopLauncher: noopLauncher,
	}, nil
}

func (s *boostsLauncherSwitch) LaunchBoost(ctx context.Context, opportunity BoostOpportunity) (string, error) {
	if s.configsDB.GetBoostOpportunityConfig(opportunity.ChannelID).LaunchNoopBoost {
		return s.noopLauncher.LaunchBoost(ctx, opportunity)
	}

	return s.launcher.LaunchBoost(ctx, opportunity)
}

// TBD, We probably want to Pessimistic Locking for paid boost, low priority for now
type LocksDB interface {
	ObtainLock(ctx context.Context, key, owner string) (Lock, error)
}

type Lock interface {
	Release(ctx context.Context) error
}

type (
	BoostOpportunityFulfillmentStepName string
)

const (
	BoostOpportunityFulfillmentRecordContribution = BoostOpportunityFulfillmentStepName("CONTRIBUTION")
	BoostOpportunityFulfillmentSendUserNotice     = BoostOpportunityFulfillmentStepName("USER_NOTICE")
	BoostOpportunityFulfillmentSendPubSub         = BoostOpportunityFulfillmentStepName("PUBSUB")
	BoostOpportunityFulfillmentRefundPurchase     = BoostOpportunityFulfillmentStepName("REFUND")
	BoostOpportunityFulfillmentDataScience        = BoostOpportunityFulfillmentStepName("DATA_SCIENCE")
)

func (name BoostOpportunityFulfillmentStepName) String() string {
	return string(name)
}
