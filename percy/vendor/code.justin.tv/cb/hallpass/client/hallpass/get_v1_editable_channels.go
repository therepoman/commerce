package hallpass

import (
	"context"
	"fmt"
	"net/http"

	"code.justin.tv/cb/hallpass/view"
	"code.justin.tv/foundation/twitchclient"
)

func (c *client) GetV1EditableChannels(ctx context.Context, channelID string, reqOpts *twitchclient.ReqOpts) (*view.GetEditableChannelsResponse, error) {
	path := fmt.Sprintf("/v1/permissions/channels/%s/editable_channels", channelID)
	response := &view.GetEditableChannelsResponse{}

	mergedOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.cb-hallpass.v1.get_editable_channels",
		StatSampleRate: 0.1,
	})

	req, err := c.NewRequest(http.MethodGet, path, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")

	_, err = c.DoJSON(ctx, response, req, mergedOpts)
	if err != nil {
		return nil, err
	}

	return response, nil
}
