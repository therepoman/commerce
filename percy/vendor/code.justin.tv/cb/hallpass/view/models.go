package view

import "time"

// V1GetEditorsResponse contains the response for a get request for editors permissions
type V1GetEditorsResponse struct {
	Editors []string `json:"editors"`
}

// V2GetEditorsResponse contains the response for a get request for editors permissions for V2
type V2GetEditorsResponse struct {
	Editors []Editor `json:"editors"`
}

// Editor represents an editor
type Editor struct {
	EditorUserID string    `json:"editor_user_id"`
	CreatedOn    time.Time `json:"created_on"`
}

// GetEditableChannelsResponse contains a list of channel ids a channel can edit
type GetEditableChannelsResponse struct {
	Editable []string `json:"editable"`
}

// CreateEditorResponse contains the response for a post request for editor permissions
type CreateEditorResponse struct {
	GrantedTo string `json:"granted_to"`
}

// DeleteEditorResponse contains the response for a delete request for editor permissions
type DeleteEditorResponse struct {
	Status string `json:"status"`
}

// CreateEditorRequest is the POST request body for creating a new editor permission
type CreateEditorRequest struct {
	GrantedBy string `json:"granted_by"` // who made the request?
	GrantedTo string `json:"granted_to"` // who is the user that wll become an editor?
}

// GetIsEditorResponse contains the response body when checking is a user is an editor for a channel
type GetIsEditorResponse struct {
	IsEditor bool `json:"is_editor"`
}
