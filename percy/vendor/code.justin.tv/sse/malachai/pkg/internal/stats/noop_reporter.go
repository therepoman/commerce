package stats

import "time"

type NoopReporter struct {
}

func (r *NoopReporter) Report(metricName string, value float64, units string) {
}

func (r *NoopReporter) ReportDurationSample(metricName string, duration time.Duration) {
}
