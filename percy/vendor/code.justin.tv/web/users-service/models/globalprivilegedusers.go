package models

type GlobalPrivilegedUsers struct {
	Admins     []string `json:"admins,omitempty"`
	SubAdmins  []string `json:"subadmins,omitempty"`
	GlobalMods []string `json:"global_mods,omitempty"`
}
