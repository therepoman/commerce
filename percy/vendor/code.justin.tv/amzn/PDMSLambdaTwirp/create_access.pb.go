// Code generated by protoc-gen-go. DO NOT EDIT.
// source: code.justin.tv/amzn/PDMSLambdaTwirp/create_access.proto

package PDMSLambdaTwirp

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// Called by users-service to start a deletion request
type CreateAccessRequest struct {
	// TUID
	UserId string `protobuf:"bytes,1,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
	// what is the current email of the user
	Email string `protobuf:"bytes,2,opt,name=email,proto3" json:"email,omitempty"`
	// optional, defaults to current
	Timestamp *timestamp.Timestamp `protobuf:"bytes,3,opt,name=timestamp,proto3" json:"timestamp,omitempty"`
	// when was the user created
	UserCreatedAt *timestamp.Timestamp `protobuf:"bytes,4,opt,name=user_created_at,json=userCreatedAt,proto3" json:"user_created_at,omitempty"`
	// what is the current login of the user
	Login string `protobuf:"bytes,5,opt,name=login,proto3" json:"login,omitempty"`
	// what is the current phone number of the user
	PhoneNumber          string   `protobuf:"bytes,6,opt,name=phone_number,json=phoneNumber,proto3" json:"phone_number,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CreateAccessRequest) Reset()         { *m = CreateAccessRequest{} }
func (m *CreateAccessRequest) String() string { return proto.CompactTextString(m) }
func (*CreateAccessRequest) ProtoMessage()    {}
func (*CreateAccessRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_72bb30f61828f74c, []int{0}
}

func (m *CreateAccessRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateAccessRequest.Unmarshal(m, b)
}
func (m *CreateAccessRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateAccessRequest.Marshal(b, m, deterministic)
}
func (m *CreateAccessRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateAccessRequest.Merge(m, src)
}
func (m *CreateAccessRequest) XXX_Size() int {
	return xxx_messageInfo_CreateAccessRequest.Size(m)
}
func (m *CreateAccessRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateAccessRequest.DiscardUnknown(m)
}

var xxx_messageInfo_CreateAccessRequest proto.InternalMessageInfo

func (m *CreateAccessRequest) GetUserId() string {
	if m != nil {
		return m.UserId
	}
	return ""
}

func (m *CreateAccessRequest) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func (m *CreateAccessRequest) GetTimestamp() *timestamp.Timestamp {
	if m != nil {
		return m.Timestamp
	}
	return nil
}

func (m *CreateAccessRequest) GetUserCreatedAt() *timestamp.Timestamp {
	if m != nil {
		return m.UserCreatedAt
	}
	return nil
}

func (m *CreateAccessRequest) GetLogin() string {
	if m != nil {
		return m.Login
	}
	return ""
}

func (m *CreateAccessRequest) GetPhoneNumber() string {
	if m != nil {
		return m.PhoneNumber
	}
	return ""
}

type CreateAccessPayload struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CreateAccessPayload) Reset()         { *m = CreateAccessPayload{} }
func (m *CreateAccessPayload) String() string { return proto.CompactTextString(m) }
func (*CreateAccessPayload) ProtoMessage()    {}
func (*CreateAccessPayload) Descriptor() ([]byte, []int) {
	return fileDescriptor_72bb30f61828f74c, []int{1}
}

func (m *CreateAccessPayload) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateAccessPayload.Unmarshal(m, b)
}
func (m *CreateAccessPayload) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateAccessPayload.Marshal(b, m, deterministic)
}
func (m *CreateAccessPayload) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateAccessPayload.Merge(m, src)
}
func (m *CreateAccessPayload) XXX_Size() int {
	return xxx_messageInfo_CreateAccessPayload.Size(m)
}
func (m *CreateAccessPayload) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateAccessPayload.DiscardUnknown(m)
}

var xxx_messageInfo_CreateAccessPayload proto.InternalMessageInfo

func init() {
	proto.RegisterType((*CreateAccessRequest)(nil), "twitch.fulton.privacy.pdmsservice.CreateAccessRequest")
	proto.RegisterType((*CreateAccessPayload)(nil), "twitch.fulton.privacy.pdmsservice.CreateAccessPayload")
}

func init() {
	proto.RegisterFile("code.justin.tv/amzn/PDMSLambdaTwirp/create_access.proto", fileDescriptor_72bb30f61828f74c)
}

var fileDescriptor_72bb30f61828f74c = []byte{
	// 285 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x84, 0x4f, 0x4b, 0x4f, 0x32, 0x31,
	0x14, 0xcd, 0x7c, 0x9f, 0x60, 0x28, 0x1a, 0x93, 0x51, 0xe3, 0x84, 0x8d, 0xc0, 0x8a, 0x55, 0x27,
	0xd1, 0x85, 0x6e, 0x51, 0x37, 0x26, 0x6a, 0x08, 0xb2, 0x9f, 0x74, 0xda, 0x0b, 0xd4, 0x4c, 0x1f,
	0xb6, 0xb7, 0x10, 0xfc, 0xeb, 0x6e, 0xcc, 0xb4, 0x41, 0xe3, 0xca, 0xe5, 0x39, 0xf7, 0x9e, 0x17,
	0xb9, 0xe1, 0x46, 0x00, 0x7d, 0x0b, 0x1e, 0xa5, 0xa6, 0xb8, 0x29, 0x99, 0xfa, 0xd0, 0xe5, 0xec,
	0xe1, 0xf9, 0xf5, 0x89, 0xa9, 0x5a, 0xb0, 0xc5, 0x56, 0x3a, 0x5b, 0x72, 0x07, 0x0c, 0xa1, 0x62,
	0x9c, 0x83, 0xf7, 0xd4, 0x3a, 0x83, 0x26, 0x1f, 0xe1, 0x56, 0x22, 0x5f, 0xd3, 0x65, 0x68, 0xd0,
	0x68, 0x6a, 0x9d, 0xdc, 0x30, 0xbe, 0xa3, 0x56, 0x28, 0xef, 0xc1, 0x6d, 0x24, 0x87, 0xc1, 0xe5,
	0xca, 0x98, 0x55, 0x03, 0x65, 0x14, 0xd4, 0x61, 0x59, 0xa2, 0x54, 0xe0, 0x91, 0x29, 0x9b, 0x3c,
	0xc6, 0x9f, 0x19, 0x39, 0xbd, 0x8f, 0xde, 0xd3, 0x68, 0x3d, 0x87, 0xf7, 0x00, 0x1e, 0xf3, 0x0b,
	0x72, 0x18, 0x3c, 0xb8, 0x4a, 0x8a, 0x22, 0x1b, 0x66, 0x93, 0xde, 0xbc, 0xdb, 0xc2, 0x47, 0x91,
	0x9f, 0x91, 0x0e, 0x28, 0x26, 0x9b, 0xe2, 0x5f, 0xa4, 0x13, 0xc8, 0x6f, 0x49, 0xef, 0xdb, 0xb9,
	0xf8, 0x3f, 0xcc, 0x26, 0xfd, 0xab, 0x01, 0x4d, 0xd9, 0x74, 0x9f, 0x4d, 0x17, 0xfb, 0x8f, 0xf9,
	0xcf, 0x73, 0x7e, 0x47, 0x4e, 0x62, 0x50, 0x1a, 0x28, 0x2a, 0x86, 0xc5, 0xc1, 0x9f, 0xfa, 0xe3,
	0x56, 0x92, 0x6a, 0x8b, 0x29, 0xb6, 0x9d, 0x1a, 0xb3, 0x92, 0xba, 0xe8, 0xa4, 0x4e, 0x11, 0xe4,
	0x23, 0x72, 0x64, 0xd7, 0x46, 0x43, 0xa5, 0x83, 0xaa, 0xc1, 0x15, 0xdd, 0x78, 0xec, 0x47, 0xee,
	0x25, 0x52, 0xe3, 0xf3, 0xdf, 0xe3, 0x67, 0x6c, 0xd7, 0x18, 0x26, 0xea, 0x6e, 0x8c, 0xbc, 0xfe,
	0x0a, 0x00, 0x00, 0xff, 0xff, 0xe4, 0xaf, 0x31, 0x17, 0x9a, 0x01, 0x00, 0x00,
}
