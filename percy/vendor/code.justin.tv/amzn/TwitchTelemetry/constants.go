package telemetry

// TODO: Are these standardized names best kept here?
// or somewhere else?

// Standardized names for metric dimensions.
const (
	DimensionService  = "Service"
	DimensionStage    = "Stage"
	DimensionSubstage = "Substage"
	DimensionRegion   = "Region"

	DimensionProcessAddress = "ProcessAddress"

	DimensionOperation = "Operation"

	DimensionDependency = "Dependency"
)

func isStandardDimension(dimension string) bool {
	switch dimension {
	case DimensionService,
		DimensionStage,
		DimensionSubstage,
		DimensionRegion,
		DimensionProcessAddress,
		DimensionOperation,
		DimensionDependency:
		return true
	}
	return false
}

// Standardized names for metrics.
const (
	MetricDuration    = "Duration"
	MetricSuccess     = "Success"
	MetricRedirect    = "Redirect"
	MetricClientError = "ClientError"
	MetricServerError = "ServerError"
	MetricTwirpError  = "TwirpErrorCode"
	MetricHTTPError   = "HTTPErrorCode"

	MetricDependencyDuration    = "DependencyDuration"
	MetricDependencySuccess     = "DependencySuccess"
	MetricDependencyClientError = "DependencyClientError"
	MetricDependencyServerError = "DependencyServerError"

	MetricStdoutBytesWritten = "StdoutBytesWritten"
	MetricStdoutWaitTime     = "StdoutWaitTime"
)

// Standardized metric values.
const (
	MetricValueUnknownService      = "Unknown"
	MetricValueUnknownOperation    = "Unknown"
	MetricValueDependencySeparator = ":"
)

// Standardized names for units.  These are the same as the CloudWatch API.
const (
	UnitBytes   = "Bytes"
	UnitCount   = "Count"
	UnitSeconds = "Seconds"
)

const (
	// MaxDimensions is the maximum number of supported dimensions.
	MaxDimensions = 10
)

// AvailabilityCode represents how the outcome of an operation is interpreted with regards to service availability.
type AvailabilityCode int

// Availability codes used when recording the outcome of requests and dependency calls.
const (
	AvailabilityCodeSucccess    AvailabilityCode = 0
	AvailabilityCodeClientError AvailabilityCode = 1
	AvailabilityCodeServerError AvailabilityCode = 2
)
