package oleiface

import "io"

// OleAPI is the ole client
type OleAPI interface {
	// NewEncryptor should be called to implement
	NewEncryptor(encryptionContext map[string]string, w io.Writer) io.Writer

	// NewDecryptor wraps the reader for encrypted data and decrypts it
	NewDecryptor(r io.Reader) io.Reader
}
