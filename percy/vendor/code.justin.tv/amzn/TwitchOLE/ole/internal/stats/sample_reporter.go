package stats

// SampleReporterAPI what TwitchTelemetry.SampleReporter implements
type SampleReporterAPI interface {
	Report(string, float64, string)
}
