package ole

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/kms"
)

// KMSAPI describes the KMS API that the kms client needs
type KMSAPI interface {
	Decrypt(*kms.DecryptInput) (*kms.DecryptOutput, error)
	GenerateDataKey(*kms.GenerateDataKeyInput) (*kms.GenerateDataKeyOutput, error)
}

func toAWSEncryptionContext(ec map[string]string) map[string]*string {
	r := map[string]*string{}
	for k, v := range ec {
		r[k] = aws.String(v)
	}
	return r
}
