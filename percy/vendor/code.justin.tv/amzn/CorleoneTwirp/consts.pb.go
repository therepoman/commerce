// Code generated by protoc-gen-go. DO NOT EDIT.
// source: code.justin.tv/amzn/CorleoneTwirp/consts.proto

package CorleoneTwirp

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type CreditChargeModelType int32

const (
	CreditChargeModelType_NO_CREDIT_TYPE CreditChargeModelType = 0
	CreditChargeModelType_SUB_TOKEN      CreditChargeModelType = 1
)

var CreditChargeModelType_name = map[int32]string{
	0: "NO_CREDIT_TYPE",
	1: "SUB_TOKEN",
}

var CreditChargeModelType_value = map[string]int32{
	"NO_CREDIT_TYPE": 0,
	"SUB_TOKEN":      1,
}

func (x CreditChargeModelType) String() string {
	return proto.EnumName(CreditChargeModelType_name, int32(x))
}

func (CreditChargeModelType) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_b09ad0e31061295f, []int{0}
}

type CancellationPolicyType int32

const (
	CancellationPolicyType_NON_REFUNDABLE CancellationPolicyType = 0
	CancellationPolicyType_REFUNDABLE     CancellationPolicyType = 1
)

var CancellationPolicyType_name = map[int32]string{
	0: "NON_REFUNDABLE",
	1: "REFUNDABLE",
}

var CancellationPolicyType_value = map[string]int32{
	"NON_REFUNDABLE": 0,
	"REFUNDABLE":     1,
}

func (x CancellationPolicyType) String() string {
	return proto.EnumName(CancellationPolicyType_name, int32(x))
}

func (CancellationPolicyType) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_b09ad0e31061295f, []int{1}
}

type PlatformType int32

const (
	PlatformType_WEB     PlatformType = 0
	PlatformType_DESKTOP PlatformType = 1
	PlatformType_IOS     PlatformType = 2
	PlatformType_ANDROID PlatformType = 3
)

var PlatformType_name = map[int32]string{
	0: "WEB",
	1: "DESKTOP",
	2: "IOS",
	3: "ANDROID",
}

var PlatformType_value = map[string]int32{
	"WEB":     0,
	"DESKTOP": 1,
	"IOS":     2,
	"ANDROID": 3,
}

func (x PlatformType) String() string {
	return proto.EnumName(PlatformType_name, int32(x))
}

func (PlatformType) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_b09ad0e31061295f, []int{2}
}

type NotEligibleReasonCode int32

const (
	NotEligibleReasonCode_OTHER                    NotEligibleReasonCode = 0
	NotEligibleReasonCode_NONE                     NotEligibleReasonCode = 1
	NotEligibleReasonCode_TOKENS_MAX_TOKEN_BALANCE NotEligibleReasonCode = 2
	NotEligibleReasonCode_ALREADY_PURCHASED        NotEligibleReasonCode = 3
	NotEligibleReasonCode_COULD_NOT_VERIFY         NotEligibleReasonCode = 4
)

var NotEligibleReasonCode_name = map[int32]string{
	0: "OTHER",
	1: "NONE",
	2: "TOKENS_MAX_TOKEN_BALANCE",
	3: "ALREADY_PURCHASED",
	4: "COULD_NOT_VERIFY",
}

var NotEligibleReasonCode_value = map[string]int32{
	"OTHER":                    0,
	"NONE":                     1,
	"TOKENS_MAX_TOKEN_BALANCE": 2,
	"ALREADY_PURCHASED":        3,
	"COULD_NOT_VERIFY":         4,
}

func (x NotEligibleReasonCode) String() string {
	return proto.EnumName(NotEligibleReasonCode_name, int32(x))
}

func (NotEligibleReasonCode) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_b09ad0e31061295f, []int{3}
}

type GiftType int32

const (
	GiftType_UNKNOWN_GIFT_TYPE GiftType = 0
	GiftType_SINGLE_RECIPIENT  GiftType = 1
	GiftType_COMMUNITY         GiftType = 2
)

var GiftType_name = map[int32]string{
	0: "UNKNOWN_GIFT_TYPE",
	1: "SINGLE_RECIPIENT",
	2: "COMMUNITY",
}

var GiftType_value = map[string]int32{
	"UNKNOWN_GIFT_TYPE": 0,
	"SINGLE_RECIPIENT":  1,
	"COMMUNITY":         2,
}

func (x GiftType) String() string {
	return proto.EnumName(GiftType_name, int32(x))
}

func (GiftType) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_b09ad0e31061295f, []int{4}
}

type RenewalPolicyType int32

const (
	RenewalPolicyType_UNKNOWN_POLICY_TYPE RenewalPolicyType = 0
	RenewalPolicyType_NO_RENEW            RenewalPolicyType = 1
	RenewalPolicyType_AUTO_RENEW          RenewalPolicyType = 2
)

var RenewalPolicyType_name = map[int32]string{
	0: "UNKNOWN_POLICY_TYPE",
	1: "NO_RENEW",
	2: "AUTO_RENEW",
}

var RenewalPolicyType_value = map[string]int32{
	"UNKNOWN_POLICY_TYPE": 0,
	"NO_RENEW":            1,
	"AUTO_RENEW":          2,
}

func (x RenewalPolicyType) String() string {
	return proto.EnumName(RenewalPolicyType_name, int32(x))
}

func (RenewalPolicyType) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_b09ad0e31061295f, []int{5}
}

type IntervalUnit int32

const (
	IntervalUnit_UNKNOWN_INTERVAL_UNIT IntervalUnit = 0
	IntervalUnit_MINUTES               IntervalUnit = 1
	IntervalUnit_HOURS                 IntervalUnit = 2
	IntervalUnit_DAYS                  IntervalUnit = 3
	IntervalUnit_WEEKS                 IntervalUnit = 4
	IntervalUnit_MONTHS                IntervalUnit = 5
)

var IntervalUnit_name = map[int32]string{
	0: "UNKNOWN_INTERVAL_UNIT",
	1: "MINUTES",
	2: "HOURS",
	3: "DAYS",
	4: "WEEKS",
	5: "MONTHS",
}

var IntervalUnit_value = map[string]int32{
	"UNKNOWN_INTERVAL_UNIT": 0,
	"MINUTES":               1,
	"HOURS":                 2,
	"DAYS":                  3,
	"WEEKS":                 4,
	"MONTHS":                5,
}

func (x IntervalUnit) String() string {
	return proto.EnumName(IntervalUnit_name, int32(x))
}

func (IntervalUnit) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_b09ad0e31061295f, []int{6}
}

type PurchaseType int32

const (
	PurchaseType_DEFAULT_PURCHASE         PurchaseType = 0
	PurchaseType_UPGRADE_RECURRING_TIER   PurchaseType = 1
	PurchaseType_DOWNGRADE_RECURRING_TIER PurchaseType = 2
	PurchaseType_FUTURE_PURCHASE          PurchaseType = 3
)

var PurchaseType_name = map[int32]string{
	0: "DEFAULT_PURCHASE",
	1: "UPGRADE_RECURRING_TIER",
	2: "DOWNGRADE_RECURRING_TIER",
	3: "FUTURE_PURCHASE",
}

var PurchaseType_value = map[string]int32{
	"DEFAULT_PURCHASE":         0,
	"UPGRADE_RECURRING_TIER":   1,
	"DOWNGRADE_RECURRING_TIER": 2,
	"FUTURE_PURCHASE":          3,
}

func (x PurchaseType) String() string {
	return proto.EnumName(PurchaseType_name, int32(x))
}

func (PurchaseType) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_b09ad0e31061295f, []int{7}
}

type DiscountType int32

const (
	DiscountType_DISCOUNT_TYPE_NONE    DiscountType = 0
	DiscountType_DISCOUNT_TYPE_PERCENT DiscountType = 1
	DiscountType_DISCOUNT_TYPE_CENTS   DiscountType = 2
)

var DiscountType_name = map[int32]string{
	0: "DISCOUNT_TYPE_NONE",
	1: "DISCOUNT_TYPE_PERCENT",
	2: "DISCOUNT_TYPE_CENTS",
}

var DiscountType_value = map[string]int32{
	"DISCOUNT_TYPE_NONE":    0,
	"DISCOUNT_TYPE_PERCENT": 1,
	"DISCOUNT_TYPE_CENTS":   2,
}

func (x DiscountType) String() string {
	return proto.EnumName(DiscountType_name, int32(x))
}

func (DiscountType) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_b09ad0e31061295f, []int{8}
}

type EligibilityFilterType int32

const (
	EligibilityFilterType_ELIGIBILITY_FILTER_TYPE_NONE             EligibilityFilterType = 0
	EligibilityFilterType_ELIGIBILITY_FILTER_TYPE_FIRST_TIME_BUYER EligibilityFilterType = 1
)

var EligibilityFilterType_name = map[int32]string{
	0: "ELIGIBILITY_FILTER_TYPE_NONE",
	1: "ELIGIBILITY_FILTER_TYPE_FIRST_TIME_BUYER",
}

var EligibilityFilterType_value = map[string]int32{
	"ELIGIBILITY_FILTER_TYPE_NONE":             0,
	"ELIGIBILITY_FILTER_TYPE_FIRST_TIME_BUYER": 1,
}

func (x EligibilityFilterType) String() string {
	return proto.EnumName(EligibilityFilterType_name, int32(x))
}

func (EligibilityFilterType) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_b09ad0e31061295f, []int{9}
}

func init() {
	proto.RegisterEnum("consts.CreditChargeModelType", CreditChargeModelType_name, CreditChargeModelType_value)
	proto.RegisterEnum("consts.CancellationPolicyType", CancellationPolicyType_name, CancellationPolicyType_value)
	proto.RegisterEnum("consts.PlatformType", PlatformType_name, PlatformType_value)
	proto.RegisterEnum("consts.NotEligibleReasonCode", NotEligibleReasonCode_name, NotEligibleReasonCode_value)
	proto.RegisterEnum("consts.GiftType", GiftType_name, GiftType_value)
	proto.RegisterEnum("consts.RenewalPolicyType", RenewalPolicyType_name, RenewalPolicyType_value)
	proto.RegisterEnum("consts.IntervalUnit", IntervalUnit_name, IntervalUnit_value)
	proto.RegisterEnum("consts.PurchaseType", PurchaseType_name, PurchaseType_value)
	proto.RegisterEnum("consts.DiscountType", DiscountType_name, DiscountType_value)
	proto.RegisterEnum("consts.EligibilityFilterType", EligibilityFilterType_name, EligibilityFilterType_value)
}

func init() {
	proto.RegisterFile("code.justin.tv/amzn/CorleoneTwirp/consts.proto", fileDescriptor_b09ad0e31061295f)
}

var fileDescriptor_b09ad0e31061295f = []byte{
	// 638 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x84, 0x93, 0xcd, 0x4f, 0xdb, 0x4a,
	0x14, 0xc5, 0xf3, 0x05, 0x84, 0xfb, 0xf2, 0x78, 0xc3, 0x40, 0xe0, 0xbd, 0x27, 0x16, 0xad, 0xba,
	0xa9, 0xac, 0x2a, 0x2c, 0xba, 0x43, 0xdd, 0x38, 0xf6, 0x38, 0x99, 0xc6, 0x99, 0xb1, 0xc6, 0x33,
	0xa4, 0x66, 0x33, 0x32, 0xc9, 0x00, 0xae, 0x8c, 0x8d, 0x1c, 0x03, 0xa2, 0x7f, 0x7d, 0x65, 0x5b,
	0xe1, 0x43, 0x6a, 0xd5, 0xe5, 0x3d, 0xe3, 0x7b, 0xee, 0xb9, 0xbf, 0xf1, 0xc0, 0x68, 0x99, 0xaf,
	0xcc, 0xe8, 0xfb, 0xfd, 0xba, 0x4c, 0xb2, 0x51, 0xf9, 0x70, 0x1a, 0xdf, 0xfe, 0xc8, 0x4e, 0x9d,
	0xbc, 0x48, 0x4d, 0x9e, 0x19, 0xf9, 0x98, 0x14, 0x77, 0xa7, 0xcb, 0x3c, 0x5b, 0x97, 0xeb, 0xd1,
	0x5d, 0x91, 0x97, 0x39, 0xde, 0x6e, 0x2a, 0xeb, 0x0c, 0x86, 0x4e, 0x61, 0x56, 0x49, 0xe9, 0xdc,
	0xc4, 0xc5, 0xb5, 0x99, 0xe7, 0x2b, 0x93, 0xca, 0xa7, 0x3b, 0x83, 0x31, 0xec, 0x31, 0xae, 0x1d,
	0x41, 0x5c, 0x2a, 0xb5, 0x8c, 0x02, 0x82, 0x5a, 0xf8, 0x6f, 0xd8, 0x0d, 0xd5, 0x58, 0x4b, 0x3e,
	0x23, 0x0c, 0xb5, 0xad, 0x2f, 0x70, 0xe4, 0xc4, 0xd9, 0xd2, 0xa4, 0x69, 0x5c, 0x26, 0x79, 0x16,
	0xe4, 0x69, 0xb2, 0x7c, 0x7a, 0x69, 0x66, 0x5a, 0x10, 0x4f, 0x31, 0xd7, 0x1e, 0xfb, 0x55, 0xf3,
	0x1e, 0xc0, 0xab, 0xba, 0x6d, 0x9d, 0xc1, 0x20, 0x48, 0xe3, 0xf2, 0x2a, 0x2f, 0x6e, 0xeb, 0x9e,
	0x1d, 0xe8, 0x2e, 0xc8, 0x18, 0xb5, 0xf0, 0x5f, 0xb0, 0xe3, 0x92, 0x70, 0x26, 0x79, 0x80, 0xda,
	0x95, 0x4a, 0x79, 0x88, 0x3a, 0x95, 0x6a, 0x33, 0x57, 0x70, 0xea, 0xa2, 0xae, 0xf5, 0x08, 0x43,
	0x96, 0x97, 0x24, 0x4d, 0xae, 0x93, 0xcb, 0xd4, 0x08, 0x13, 0xaf, 0xf3, 0xcc, 0xc9, 0x57, 0x06,
	0xef, 0xc2, 0x16, 0x97, 0x53, 0x22, 0x50, 0x0b, 0xf7, 0xa1, 0xc7, 0x38, 0x23, 0xa8, 0x8d, 0x4f,
	0xe0, 0xdf, 0x3a, 0x72, 0xa8, 0xe7, 0xf6, 0xb7, 0x26, 0xbd, 0x1e, 0xdb, 0xbe, 0xcd, 0x1c, 0x82,
	0x3a, 0x78, 0x08, 0xfb, 0xb6, 0x2f, 0x88, 0xed, 0x46, 0x3a, 0x50, 0xc2, 0x99, 0xda, 0x21, 0x71,
	0x51, 0x17, 0x1f, 0x02, 0x72, 0xb8, 0xf2, 0x5d, 0xcd, 0xb8, 0xd4, 0xe7, 0x44, 0x50, 0x2f, 0x42,
	0x3d, 0xcb, 0x83, 0xfe, 0x24, 0xb9, 0x2a, 0xeb, 0xc0, 0x43, 0xd8, 0x57, 0x6c, 0xc6, 0xf8, 0x82,
	0xe9, 0x09, 0xf5, 0x9e, 0x21, 0x1d, 0x02, 0x0a, 0x29, 0x9b, 0xf8, 0x44, 0x0b, 0xe2, 0xd0, 0x80,
	0x12, 0x26, 0x51, 0xbb, 0x42, 0xe7, 0xf0, 0xf9, 0x5c, 0x31, 0x2a, 0x23, 0xd4, 0xb1, 0xbe, 0xc2,
	0xbe, 0x30, 0x99, 0x79, 0x8c, 0xd3, 0x57, 0xd4, 0x8e, 0xe1, 0x60, 0x63, 0x18, 0x70, 0x9f, 0x3a,
	0xd1, 0xc6, 0x72, 0x00, 0x7d, 0xc6, 0xb5, 0x20, 0x8c, 0x2c, 0x50, 0xbb, 0x02, 0x69, 0x2b, 0xb9,
	0xa9, 0x3b, 0xd6, 0x25, 0x0c, 0x68, 0x56, 0x9a, 0xe2, 0x21, 0x4e, 0x55, 0x96, 0x94, 0xf8, 0x3f,
	0x18, 0x6e, 0x6c, 0x28, 0x93, 0x44, 0x9c, 0xdb, 0xbe, 0xae, 0xe6, 0x36, 0x68, 0xe7, 0x94, 0x29,
	0x49, 0x42, 0xd4, 0xae, 0x58, 0x4d, 0xb9, 0x12, 0x15, 0xdc, 0x3e, 0xf4, 0x5c, 0x3b, 0x0a, 0x51,
	0xb7, 0x12, 0x17, 0x84, 0xcc, 0x42, 0xd4, 0xc3, 0x00, 0xdb, 0x73, 0xce, 0xe4, 0x34, 0x44, 0x5b,
	0xd6, 0x1a, 0x06, 0xc1, 0x7d, 0xb1, 0xbc, 0x89, 0xd7, 0xa6, 0x8e, 0x7a, 0x08, 0xc8, 0x25, 0x9e,
	0xad, 0x7c, 0xf9, 0x0c, 0x0d, 0xb5, 0xf0, 0xff, 0x70, 0xa4, 0x82, 0x89, 0xb0, 0xdd, 0x7a, 0x77,
	0x25, 0x04, 0x65, 0x13, 0x2d, 0x29, 0x11, 0xcd, 0x25, 0xb8, 0x7c, 0xc1, 0x7e, 0x79, 0xda, 0xc1,
	0x07, 0xf0, 0x8f, 0xa7, 0xa4, 0x12, 0xe4, 0xc5, 0xae, 0x6b, 0x5d, 0xc0, 0xc0, 0x4d, 0xd6, 0xcb,
	0xfc, 0x3e, 0x6b, 0x80, 0x1f, 0x01, 0x76, 0x69, 0xe8, 0x70, 0xc5, 0x1a, 0xd8, 0xba, 0xbe, 0xdf,
	0x56, 0xb5, 0xf0, 0x5b, 0x3d, 0x20, 0xc2, 0x69, 0xb0, 0x1f, 0xc3, 0xc1, 0xdb, 0xa3, 0x4a, 0x0f,
	0x51, 0xc7, 0xba, 0x86, 0x61, 0xf3, 0xfb, 0x24, 0x69, 0x52, 0x3e, 0x79, 0x49, 0x5a, 0x9a, 0xa2,
	0x1e, 0xf2, 0x0e, 0x4e, 0x88, 0x4f, 0x27, 0x74, 0x4c, 0x7d, 0x2a, 0x23, 0xed, 0x51, 0x5f, 0x12,
	0xf1, 0x66, 0xdc, 0x27, 0xf8, 0xf8, 0xbb, 0x2f, 0x3c, 0x2a, 0x42, 0xa9, 0x25, 0x9d, 0x13, 0x3d,
	0x56, 0x51, 0xb5, 0xf7, 0xf8, 0xc3, 0xc5, 0xfb, 0x3f, 0x3e, 0xcd, 0xcb, 0xed, 0xfa, 0x51, 0x7e,
	0xfe, 0x19, 0x00, 0x00, 0xff, 0xff, 0x33, 0xfb, 0xb5, 0x0b, 0xc6, 0x03, 0x00, 0x00,
}
