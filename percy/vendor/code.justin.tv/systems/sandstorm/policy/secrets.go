package policy

// SecretsManager represents the secrets interface we need for creating policies
// manager.API implements this interface
type SecretsManager interface {
	CrossEnvironmentSecretsSet(secretNames []string) (crossEnvSecrets map[string]struct{}, err error)
}
