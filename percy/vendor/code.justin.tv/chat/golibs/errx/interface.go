package errx

// Error wraps a native Golang error with metadata fields and stack trace.
// Error implements error and Stringer.
type Error interface {
	Error() string
	String() string
	Fields() Fields
}

// Fields is a map of arbitrary metadata.
type Fields map[string]interface{}
