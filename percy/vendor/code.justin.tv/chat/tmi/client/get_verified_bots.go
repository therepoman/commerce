package tmi

import (
	"context"

	"code.justin.tv/foundation/twitchclient"
)

type GetVerifiedBotsResponse struct {
	VerifiedBotIDs []string `json:"verified_bot_ids"`
}

type GetBotsResponse struct {
	VerifiedBotIDs []string `json:"verified_bot_ids"`
	KnownBotIDs    []string `json:"known_bot_ids"`
}

// GetVerified Bots just gets the Verified Bots in our service
func (c *clientImpl) GetVerifiedBots(ctx context.Context, reqOpts *twitchclient.ReqOpts) (GetVerifiedBotsResponse, error) {
	path := "/internal/bots"
	req, err := c.NewRequest("GET", path, nil)
	if err != nil {
		return GetVerifiedBotsResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.clue.get_verified_bots",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded GetVerifiedBotsResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return GetVerifiedBotsResponse{}, err
	}
	return decoded, nil
}

// GetBots returns the bots registered with the service
func (c *clientImpl) GetBots(ctx context.Context, reqOpts *twitchclient.ReqOpts) (GetBotsResponse, error) {
	path := "/internal/v2/bots"
	req, err := c.NewRequest("GET", path, nil)
	if err != nil {
		return GetBotsResponse{}, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.clue.get_bots",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded GetBotsResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return GetBotsResponse{}, err
	}
	return decoded, nil
}
