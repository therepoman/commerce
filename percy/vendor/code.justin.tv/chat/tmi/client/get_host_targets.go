package tmi

import (
	"net/url"
	"strings"

	"context"

	"code.justin.tv/foundation/twitchclient"
)

type HostTargetsResponse struct {
	Hosts []HostTargetEntity `json:"hosts"`
}

type HostTargetEntity struct {
	HostID   int `json:"host_id"`
	TargetID int `json:"target_id"`
}

func (c *clientImpl) GetHostTargets(ctx context.Context, channelIDs []string, reqOpts *twitchclient.ReqOpts) (*HostTargetsResponse, error) {
	if len(channelIDs) == 0 {
		return &HostTargetsResponse{}, nil
	}

	data := &url.Values{
		"host": []string{strings.Join(channelIDs, ",")},
	}
	req, err := c.NewRequest("POST", "/hosts", strings.NewReader(data.Encode()))
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.clue.user_properties",
		StatSampleRate: defaultStatSampleRate,
	})

	var decoded HostTargetsResponse
	if _, err := c.DoJSON(ctx, &decoded, req, combinedReqOpts); err != nil {
		return nil, err
	}
	return &decoded, nil
}
