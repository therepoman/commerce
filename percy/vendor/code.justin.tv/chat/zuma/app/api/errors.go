package api

const (
	// channel mods
	// used for TMI to return text such as 'that user is already a mod'
	ErrCodeTargetUserAlreadyMod = "target_user_already_mod"
	ErrCodeTargetUserNotMod     = "target_user_not_mod"

	// channel vips
	// used for TMI to return text such as 'that user is already a vip'
	ErrCodeMaxVIPsReached       = "max_vips_reached"
	ErrCodeVIPQuestIncomplete   = "vip_quest_incomplete"
	ErrCodeTargetUserAlreadyVIP = "target_user_already_vip"
	ErrCodeTargetUserNotVIP     = "target_user_not_vip"

	// general purpose
	ErrCodeRequestingUserNotPermitted = "requesting_user_not_permitted"
	ErrCodeRequestingUserNotFound     = "requesting_user_not_found"
	ErrCodeTargetUserNotFound         = "target_user_not_found"
	ErrCodeTargetUserStaff            = "target_user_staff"
	ErrCodeTargetUserSelf             = "target_user_self"
	ErrCodeTargetChannelNotFound      = "target_channel_not_found"
	ErrCodeTargetUserDisabled         = "target_user_disabled"

	// add mods
	ErrCodeTargetUserBanned = "target_user_banned"

	// user blocks
	ErrCodeTargetUserCannotBeBlocked = "target_cannot_be_blocked"

	// ritual tokens
	ErrCodeRitualTokenNotFound     = "ritual_token_not_found"
	ErrCodeRitualTokenNotAvailable = "ritual_token_not_available"

	// MaaP errors
	ErrCodeEnforcementGeneric       = "failed_enforcement_generic"
	ErrCodeEnforcementBanned        = "failed_enforcement_banned"
	ErrCodeEnforcementTimedOut      = "failed_enforcement_timed_out"
	ErrCodeEnforcementFollowersOnly = "failed_enforcement_followers_only"
	ErrCodeEnforcementSubsOnly      = "failed_enforcement_subs_only"
)

// ErrorResponse is used to give services more detailed error responses to
// errors caused by user input
type ErrorResponse struct {
	Err    string `json:"error"`
	Status int    `json:"status"`
}

func (e *ErrorResponse) GetErrorCode() int {
	return e.Status
}

func (e *ErrorResponse) Error() string {
	return e.Err
}

type ErrorCodeResponse struct {
	ErrorCode *string `json:"error_code"`
}
