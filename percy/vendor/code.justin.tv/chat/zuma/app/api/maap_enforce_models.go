package api

import (
	"time"
)

// Valid responses from an enforcement rule.
const (
	EnforcementReject = "reject" // reject a message
	EnforcementOK     = "ok"     // accept the message if we don't have an explicit rejection
	EnforcementError  = "error"  // attempting to check this message resulted in an error
)

const (
	AutoModEnforcementName             = "channel_community_sift"
	ChannelBlockedTermsEnforcementName = "channel_blocked_terms"
	BannedEnforcementName              = "banned"
	BlockedEnforcementName             = "blocked"
	DisposableEmailEnforcementName     = "disposable_email"
	EmoteOnlyEnforcementName           = "emote_only"
	EntropyEnforcementName             = "entropy"
	FollowersOnlyEnforcementName       = "followers_only"
	IPBlockedEnforcementName           = "ip_blocked"
	SafeLinksEnforcementName           = "safe_links"
	SpamEnforcementName                = "spam"
	SubsOnlyEnforcementName            = "subs_only"
	SuspendedEnforcementName           = "suspended"
	VerifiedOnlyEnforcementName        = "verified_only"
	// Deprecated: This is no longer used.
	ZalgoEnforcementName = "zalgo"
)

// EnforcementRuleset allows the user to specify which enforcement rules to run
type EnforcementRuleset struct {
	// AutoMod checks the message text against the containers AutoMod settings (TODO: if there is no container, default to AutoMod Lvl 1)
	AutoMod bool `json:"automod"`

	// ChannelBlockedTerms checks whether the text contains any channel blocked terms. Requires use of ChannelBlockedTerms in enforcement configuration.
	ChannelBlockedTerms bool `json:"channel_blocked_terms"`

	// Banned checks whether the sending user is banned in the target container (skips if no container)
	Banned bool `json:"banned"`

	// IsBlocked checks whether the sending user is blocked in the target container (skips if no container)
	Blocked bool `json:"blocked"`

	// DisposableEmail checks whether the sending user has a disposable email
	DisposableEmail bool `json:"disposable_email"`

	// Entropy checks whether the message text contains little informational context (i.e. is all punctuation, the same character, etc.)
	Entropy bool `json:"entropy"`

	// IPBlocked checks whether the messages is rejected based on the blocked status of the IP address of the sender
	IPBlocked bool `json:"ip_blocked"`

	// SafeLinks checks whether any links include in the message text are unsafe for users/malicious
	SafeLinks bool `json:"safe_links"`

	// Spam checks whether the message text is highly likely to be spam
	Spam bool `json:"spam"`

	// Suspended checks whether or not the sending user has a DMCA or TOS violation, or whether their account was deleted
	Suspended bool `json:"suspended"`

	// EmotesOnly checks whether or not the message consists solely of emotes
	// TODO (MES-3161): Move to Clue.
	EmotesOnly bool `json:"emotes_only"`
	// FollowersOnly checks whether followers_only mode has been enabled, and if so, whether the user can legally send a message
	// TODO (MES-3161): Move to Clue.
	FollowersOnly bool `json:"followers_only"`
	// SubscribersOnly checks whether subscribers_only mode is on, and if so, if the user is a subscriber to this channel (or a mod)
	// TODO (MES-3161): Move to Clue.
	SubscribersOnly bool `json:"subs_only"`
	// VerifiedOnly checks whether verified_only mode has been enabled, and if so, whether the user has a verified email address
	// TODO (MES-3161): Move to Clue.
	VerifiedOnly bool `json:"verified_only"`

	// Deprecated: This is no longer used. Zalgo is always sanitized.
	Zalgo bool `json:"zalgo"`
}

// EnforcementConfiguration provides a series of knobs and bells that allow you to slightly tune the behavior
// of the enforcement engine
type EnforcementConfiguration struct {
	// CheckAllyClassification tells Zuma if AutoMod should engage Ally, our new classification vendor, when enforcing AutoMod levels
	CheckAllyClassification *bool `json:"check_ally_classification"`

	// DefaultAutoModLevel sets the automod level in the absence of a channel or other container
	DefaultAutoModLevel *int `json:"default_automod_level"`

	// two letter language code for sift to use. By default we use "*" if this is not set
	DefaultAutoModLanguageCode *string `json:"default_automod_language_code"`

	// TrackingRequestID will be set in any enforcement tracking calls. TMI would use msg_id as the TrackingRequestID
	TrackingRequestID *string `json:"tracking_request_id"`

	// SpamModel is the model we want to use for distinguish spam checking
	SpamModel *string `json:"spam_model"`
}

// ConvertToRulesList converts an EnforcementRuleset into a list of strings
// the strings will correspond to each active rule
func (e EnforcementRuleset) ConvertToRulesList() []string {
	var rules []string
	if e.AutoMod {
		rules = append(rules, AutoModEnforcementName)
	}

	if e.ChannelBlockedTerms {
		rules = append(rules, ChannelBlockedTermsEnforcementName)
	}

	if e.Banned {
		rules = append(rules, BannedEnforcementName)
	}

	if e.Blocked {
		rules = append(rules, BlockedEnforcementName)
	}

	if e.DisposableEmail {
		rules = append(rules, DisposableEmailEnforcementName)
	}

	if e.EmotesOnly {
		rules = append(rules, EmoteOnlyEnforcementName)
	}

	if e.Entropy {
		rules = append(rules, EntropyEnforcementName)
	}

	if e.FollowersOnly {
		rules = append(rules, FollowersOnlyEnforcementName)
	}

	if e.IPBlocked {
		rules = append(rules, IPBlockedEnforcementName)
	}

	if e.SafeLinks {
		rules = append(rules, SafeLinksEnforcementName)
	}

	if e.Spam {
		rules = append(rules, SpamEnforcementName)
	}

	if e.SubscribersOnly {
		rules = append(rules, SubsOnlyEnforcementName)
	}

	if e.Suspended {
		rules = append(rules, SuspendedEnforcementName)
	}

	if e.VerifiedOnly {
		rules = append(rules, VerifiedOnlyEnforcementName)
	}

	return rules
}

// ExtractProperties contains all the information related to a user extraction
type ExtractProperties struct {
	Sender  MessageSender    `json:"sender"`
	Content MessageContentV2 `json:"content"`
}

// GenericEnforcementResponse contains the information about whether a message was accepted
type GenericEnforcementResponse struct {
	Passed bool `json:"passed"`
}

// AutoModResponse contains metadata when checking a message with AutoMod.
type AutoModResponse struct {
	GenericEnforcementResponse
	Reason              string          `json:"reason"`
	IsClean             bool            `json:"is_clean"`
	ContainsBannedWords bool            `json:"contains_banned_words"`
	FailureReasons      []FailureReason `json:"failure_reasons,omitempty"`
	// AllFailureReasons are the reasons that a token would fail under the strictest level of AutoMod
	// If a container/channel does not have strict AutoMod enforcement enabled, it is possible
	// to have many tokens in AllFailureReasons but none in FailureReasons
	AllFailureReasons []FailureReason `json:"all_failure_reasons"`
	Topics            map[string]int  `json:"topics"`
	DetectedLanguage  string          `json:"detected_language"`
	NormalizedText    string          `json:"normalized_text"`
}

// FailureReason is a reason that a message failed sift
type FailureReason struct {
	NormalizedText string         `json:"normalized_text"`
	Topics         map[string]int `json:"topics"`
	StartPosition  int            `json:"start_position"`
	EndPosition    int            `json:"end_position"`
}

// ChannelBlockedTermsResponse is a simple presentation of the response from our AutoMod Endpoint
type ChannelBlockedTermsResponse struct {
	GenericEnforcementResponse
	// ContainsModEditableBlockedTerms indicates the message contained one or more mod editable blocked term fragments.
	ContainsModEditableBlockedTerms bool
	// ContainsPrivateBlockedTerms indicates the message contained one or more private blocked term fragments.
	ContainsPrivateBlockedTerms bool
}

// BannedResponse contains metadata about enforcing bans and timeouts.
type BannedResponse struct {
	GenericEnforcementResponse
	// EndTime indicates when a ban expires (i.e. a timeout). Nil if the ban is permanent.
	EndTime *time.Time `json:"end_time,omitempty"`
}

// FollowersOnlyResponse contains metadata about enforcing followers-only mode.
type FollowersOnlyResponse struct {
	GenericEnforcementResponse
	Exempt                bool           `json:"exempt"` // is this user exempt from FollowersOnly for some reason (sub, mod, etc.)
	FollowingDuration     time.Duration  `json:"following_duration"`
	RequiredFollowingTime *time.Duration `json:"minimum_following_duration"`
}

// SpamResponse contains metadata when checking a message with the spam classifier.
type SpamResponse struct {
	GenericEnforcementResponse
	FlagThreshold float64 `json:"flag_threshold"`
	DropThreshold float64 `json:"drop_threshold"`
	Score         float64 `json:"score"`
}

// EnforceProperties contains all the information related to enforcing rules on the message
type EnforceProperties struct {
	AllPassed           bool                         `json:"all_passed"`
	AutoMod             *AutoModResponse             `json:"automod,omitempty"`
	Banned              *BannedResponse              `json:"banned,omitempty"`
	Blocked             *GenericEnforcementResponse  `json:"blocked,omitempty"`
	ChannelBlockedTerms *ChannelBlockedTermsResponse `json:"channel_blocked_terms,omitempty"`
	DisposableEmail     *GenericEnforcementResponse  `json:"disposable_email,omitempty"`
	EmoteOnly           *GenericEnforcementResponse  `json:"emote_only,omitempty"`
	Entropy             *GenericEnforcementResponse  `json:"entropy,omitempty"`
	FollowersOnly       *FollowersOnlyResponse       `json:"followers_only,omitempty"`
	IPBlocked           *GenericEnforcementResponse  `json:"ipblock,omitempty"`
	SafeLinks           *GenericEnforcementResponse  `json:"safe_links,omitempty"`
	Spam                *SpamResponse                `json:"spam,omitempty"`
	SubscribersOnly     *GenericEnforcementResponse  `json:"subscribers_only,omitempty"`
	Suspended           *GenericEnforcementResponse  `json:"suspended,omitempty"`
	VerifiedOnly        *GenericEnforcementResponse  `json:"verified_only,omitempty"`

	// Deprecated: This is no longer used.
	Zalgo *GenericEnforcementResponse `json:"zalgo,omitempty"`
}
