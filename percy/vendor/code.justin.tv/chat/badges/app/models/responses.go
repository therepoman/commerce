package models

import "time"

// GetBadgeDisplayResponse is the response for GetGlobalBadgeDisplayInfo and GetChannelBadgeDisplayInfo
type GetBadgeDisplayResponse struct {
	BadgeSets map[string]BadgeSet `json:"badge_sets"`
}

// GetAvailableBadgesResponse is the response for AvailableGlobalUserBadges and AvailableChannelUserBadges
type GetAvailableBadgesResponse struct {
	AvailableBadges []Badge `json:"available_badges"`
}

// GetAvailableBitsBadgeResponse is the response for GetAvailableBitsBadge
type GetAvailableBitsBadgeResponse struct {
	BitsBadge *Badge `json:"bits_badge"`
}

// SelectBadgeResponse is an empty struct, should probably replace this (TODO)
type SelectBadgeResponse struct{}

// GrantBadgeResponse is an empty struct, should probably replace this (TODO)
type GrantBadgeResponse struct{}

// UserBadgesResponse is the response for the UserBadges endpoint
// Fields are null if not requested, or if user has no badge in slot
type UserBadgesResponse struct {
	GlobalAuthority  *Badge `json:"global_authority"`
	ChannelAuthority *Badge `json:"channel_authority"`
	Subscriber       *Badge `json:"subscriber"`
	GlobalSelected   *Badge `json:"global_selected"`
	ChannelSelected  *Badge `json:"channel_selected"`
	Temporary        *Badge `json:"temporary"`
}

// ClearCacheResponse is the response for the InvalidateSubBadgeCache endpoint
type ClearCacheResponse struct {
	Cleared bool `json:"cleared"`
}

// UploadImagesResponse is the response for the UploadImages endpoints
// Versions is a map of badge_set_version to ImageURLs
type UploadImagesResponse struct {
	Versions map[string]ImageURLs `json:"versions"`
}

// GetSamusOfferBadgeInfoResponse is the response for the GetSamusOfferBadgeInfo
// and BulkGetSamusOfferBadgeInfo endpoints
// BadgeFamilies is a map of badgeSetID to badgeSet
type GetSamusOfferBadgeInfoResponse struct {
	BadgeFamilies map[string]BadgeSet `json:"badge_families"`
}

// SamusOfferBadgeInfo contains the BadgeSetID, ImageID, BadgeTitle, and the string of the ClickURL
type SamusOfferBadgeInfo struct {
	BadgeSetID string `json:"badge_set_id"`
	BadgeTitle string `json:"title"`
	ImageID    string `json:"image_id"`
}

// ImageURLs contains the URLs for all 3 sizes of a specific badge version
type ImageURLs struct {
	ImageURL1x string `json:"image_url_1x"`
	ImageURL2x string `json:"image_url_2x"`
	ImageURL4x string `json:"image_url_4x"`
}

// BulkRequestResponse is the response from a bulk update/remove endpoint
type BulkRequestResponse struct {
	Error        string  `json:"error"`
	ProcessedIDs []int64 `json:"processed_ids"`
}

// BulkDisplayResponse ...
type BulkDisplayResponse struct {
	Badges []*DisplayInfo `json:"badges"`
}

// GetClipChampBadgeResponse is the response from the endpoint that gets a user's
// clip champ badge for a channel.
type GetClipChampBadgeResponse struct {
	ClipChampBadge *Badge `json:"clip_champ_badge"`
}

// BitsUploadImagesResponse is the response for the UploadBitsImage endpoint
// Versions is a map of badge_set_version to ImageURLs
type BitsUploadImagesResponse struct {
	Versions map[string]BitsBadgeUploadImageResponse `json:"versions"`
}

// Response for UploadBitsImage logic that includes image URLS, badge title and the time this badge was last updated
type BitsBadgeUploadImageResponse struct {
	ImageURL1x  string     `json:"image_url_1x"`
	ImageURL2x  string     `json:"image_url_2x"`
	ImageURL4x  string     `json:"image_url_4x"`
	Title       string     `json:"title"`
	LastUpdated *time.Time `json:"last_updated"`
}

type GrantSubGifterBadgeResponse struct {
	OldSubGifterBadge *Badge `json:"old_sub_gifter_badge"`
}
