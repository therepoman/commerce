package models

const SNSUserSelectedBadgeChangeEventName = "user_selected_badge_change"

// UserSelectedBadgeChangeSNSEvent is published when a user changes their selected badge
type UserSelectedBadgeChangeSNSEvent struct {
	UserID string `json:"user_id"`
	// ChannelID is empty if the badge is selected globally
	ChannelID  string `json:"channel_id"`
	BadgeSetID string `json:"badge_set_id"`
}
