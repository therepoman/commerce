package workerqueue

import (
	"time"

	telemetry "code.justin.tv/amzn/TwitchTelemetry"

	"github.com/cactus/go-statsd-client/statsd"
)

// reporterWithDimensions creates a reporter with additional dimensions. Dimensions in dims take precedent
// Copied from chat/telemetryext
func reporterWithDimensions(base telemetry.SampleReporter, dims map[string]string) telemetry.SampleReporter {
	allDims := make(map[string]string, len(base.Dimensions)+len(dims))

	for k, v := range base.Dimensions {
		allDims[k] = v
	}

	for k, v := range dims {
		allDims[k] = v
	}

	base.Dimensions = allDims

	return base
}

// metricsReporter is a simple metrics-reporting interface which specifically allows using
// TwitchTelemetry for metrics
type metricsReporter interface {
	Report(metricName string, value float64, units string)
	ReportDurationSample(metricName string, duration time.Duration)
}

// inc calls Inc on the provided StatSender with the provided metric/count/rate if the sender is not nil
func inc(stats statsd.StatSender, metric string, count int64, rate float32) {
	if stats == nil {
		return
	}
	stats.Inc(metric, count, rate)
}

// timing calls TimingDuration on the provided StatSender with the provided metric/duration/rate if the sender is not nil
func timing(stats statsd.StatSender, metric string, duration time.Duration, rate float32) {
	if stats == nil {
		return
	}
	stats.TimingDuration(metric, duration, rate)
}

// report calls Report on the provided MetricsReporter with the provided metric/count if the reporter is not nil
func report(reporter metricsReporter, metric string, count int) {
	if reporter == nil {
		return
	}
	reporter.Report(metric, float64(count), telemetry.UnitCount)
}

// report calls ReportDurationSample on the provided MetricsReporter with the provided metric/duration if the reporter is not nil
func reportTiming(reporter metricsReporter, metric string, duration time.Duration) {
	if reporter == nil {
		return
	}
	reporter.ReportDurationSample(metric, duration)
}
