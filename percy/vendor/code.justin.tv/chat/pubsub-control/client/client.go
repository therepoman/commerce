package pubsub

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"code.justin.tv/chat/golibs/errx"
	"code.justin.tv/foundation/twitchclient"
)

const (
	defaultStatSampleRate = 0.1
	defaultTimingXactName = "pubsub_control"
)

// Client is the interface for the PubsubControl go client
type Client interface {
	GetNumberListeners(ctx context.Context, topic string, reqOpts *twitchclient.ReqOpts) (int, error)
	ForceUnlisten(ctx context.Context, userIDs, topics []string, reqOpts *twitchclient.ReqOpts) error
	AuthListen(topics []string, oauthToken string, reqOpts *twitchclient.ReqOpts) (AuthorizeListenResponses, error)
	IsPrivileged(oauthToken string, reqOpts *twitchclient.ReqOpts) (bool, error)
}

type client struct {
	twitchclient.Client
}

// NewClient creates a new PubsubControl go client
func NewClient(conf twitchclient.ClientConf) (Client, error) {
	if conf.TimingXactName == "" {
		conf.TimingXactName = defaultTimingXactName
	}

	twitchClient, err := twitchclient.NewClient(conf)
	return &client{twitchClient}, err
}

// GetNumberListenersResponse is the response from the GET /internal/num_listeners endpoint
type GetNumberListenersResponse struct {
	NumberListeners int `json:"num_listeners"`
}

func (c *client) GetNumberListeners(ctx context.Context, topic string, reqOpts *twitchclient.ReqOpts) (int, error) {
	req, err := c.NewRequest("GET", fmt.Sprintf("/internal/num_listeners?topic=%s", topic), nil)
	if err != nil {
		return 0, err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.pubsub_control.get_number_listeners",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return 0, err
	}
	defer closeResponse(resp)

	if resp.StatusCode >= 400 {
		return 0, fmt.Errorf("error response code from pubsub-control %d", resp.StatusCode)
	}

	data := GetNumberListenersResponse{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return 0, fmt.Errorf("can't read response from pubsub-control: %s", err)
	}

	return data.NumberListeners, nil
}

// ForceUnlistenParams is the request body for the POST /internal/force_unlisten endpoint
type ForceUnlistenParams struct {
	UserIDs []string `json:"user_ids"`
	Topics  []string `json:"topics"`
}

func (c *client) ForceUnlisten(ctx context.Context, userIDs, topics []string, reqOpts *twitchclient.ReqOpts) error {
	if len(userIDs) == 0 || len(topics) == 0 {
		return errx.New("must provide at least one user ID and topic")
	}

	params := ForceUnlistenParams{
		UserIDs: userIDs,
		Topics:  topics,
	}
	bodyBytes, err := json.Marshal(params)
	if err != nil {
		return errx.New(err)
	}

	req, err := c.NewRequest("POST", "/internal/force_unlisten", bytes.NewReader(bodyBytes))
	if err != nil {
		return err
	}

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.pubsub_control.force_unlisten",
		StatSampleRate: defaultStatSampleRate,
	})
	resp, err := c.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return err
	}
	defer closeResponse(resp)

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("error response code from pubsub-control %d", resp.StatusCode)
	}
	return nil
}

type AuthorizeListenResponses struct {
	Responses []AuthorizeListenResponse `json:"responses"`
}

type AuthorizeListenResponse struct {
	Topic      string `json:"topic"`
	UserID     string `json:"user_id"`
	Authorized bool   `json:"authorized"`
	Error      string `json:"error"`
}

func (c *client) AuthListen(topics []string, oauthToken string, reqOpts *twitchclient.ReqOpts) (AuthorizeListenResponses, error) {
	params := buildAuthListenParams(topics)
	paramsJSON, err := json.Marshal(params)
	if err != nil {
		return AuthorizeListenResponses{}, err
	}

	req, err := c.Client.NewRequest("POST", "/private/authorize_listen", bytes.NewReader(paramsJSON))
	if err != nil {
		return AuthorizeListenResponses{}, err
	}
	req.Header.Add("Authorization", fmt.Sprintf("OAuth %s", oauthToken))
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Content-Length", strconv.Itoa(len(paramsJSON)))

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.pubsub_control.auth_listen",
		StatSampleRate: defaultStatSampleRate,
	})

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	resp, err := c.Client.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return AuthorizeListenResponses{}, err
	}
	defer closeResponse(resp)

	if resp.StatusCode != http.StatusOK {
		err := fmt.Errorf("unexpected http status %d", resp.StatusCode)
		return AuthorizeListenResponses{}, errx.New(err, errx.Fields{"client": "control"})
	}

	var authResponse AuthorizeListenResponses
	if err := json.NewDecoder(resp.Body).Decode(&authResponse); err != nil {
		return AuthorizeListenResponses{}, err
	}
	return authResponse, nil
}

type privilegedResponse struct {
	Authorized bool `json:"authorized"`
}

func (c *client) IsPrivileged(oauthToken string, reqOpts *twitchclient.ReqOpts) (bool, error) {
	params := privilegedParams{OauthToken: oauthToken}
	paramsJSON, err := json.Marshal(params)
	if err != nil {
		return false, err
	}

	req, err := c.Client.NewRequest("POST", "/private/privileged_client", bytes.NewReader(paramsJSON))
	if err != nil {
		return false, err
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Content-Length", strconv.Itoa(len(paramsJSON)))

	combinedReqOpts := twitchclient.MergeReqOpts(reqOpts, twitchclient.ReqOpts{
		StatName:       "service.pubsub_control.is_privileged",
		StatSampleRate: defaultStatSampleRate,
	})

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	resp, err := c.Client.Do(ctx, req, combinedReqOpts)
	if err != nil {
		return false, err
	}
	defer closeResponse(resp)

	if resp.StatusCode != http.StatusOK {
		err := fmt.Errorf("unexpected http status %d", resp.StatusCode)
		return false, errx.New(err, errx.Fields{"client": "control"})
	}
	privResponse := privilegedResponse{}
	err = json.NewDecoder(resp.Body).Decode(&privResponse)
	if err != nil {
		return false, err
	}

	return privResponse.Authorized, err
}

func buildAuthListenParams(topics []string) authorizeListenParams {
	return authorizeListenParams{Topics: topics}
}

type authorizeListenParams struct {
	Topics []string `json:"topics"`
}

type privilegedParams struct {
	OauthToken string `json:"oauth_token"`
}

func closeResponse(resp *http.Response) {
	if err := resp.Body.Close(); err != nil {
		log.Printf("error closing response body: %v", err)
	}
}
