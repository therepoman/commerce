module code.justin.tv/chat/ratecache

go 1.13

require (
	github.com/stretchr/testify v1.3.0
	golang.org/x/time v0.0.0-20190308202827-9d24e82272b4
)
