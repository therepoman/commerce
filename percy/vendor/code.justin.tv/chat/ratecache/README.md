# ratecache

Cache of rate limiters. This is useful for rate limiting creating new TCP connections by address.

[Examples](examples/)
