package experiments

import (
	"code.justin.tv/hygienic/spade"
)

var _ Tracking = noopTrackingClient{}

type noopTrackingClient struct{}

func (t noopTrackingClient) QueueEvents(_ ...spade.Event) {}

func (t noopTrackingClient) Close() error { return nil }
