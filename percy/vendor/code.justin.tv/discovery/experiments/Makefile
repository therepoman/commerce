.PHONY: all fmt lint integration_test mocks test test_assignments update upload_overrides install_overrides_management

# Run gofmt and goimports to fix formatting issues
fmt:
	find . -iname '*.go' -not -path '*/*vendor/*' -print0 | xargs -0 gofmt -s -w
	find . -iname '*.go' -not -path '*/*vendor/*' -print0 | xargs -0 goimports -w

# Run linters on the code
lint:
	gometalinter --vendor --dupl-threshold=200 --min-confidence=.3 --tests --deadline=90s --disable-all -Egolint -Etest -Eineffassign -Etestify -Eunconvert -Estaticcheck -Egoconst -Egocyclo -Eerrcheck -Egofmt -Evet -Edupl -Einterfacer -Estructcheck -Evetshadow -Egosimple -Egoimports -Evarcheck -Emisspell -Ealigncheck -Etest ./...

# Generate mocks
# We create the mocks in package to avoid import cycles, and to avoid having to export types just
# to make the mocks work.
mocks:
	mockery -dir . -name Provider -testonly -inpkg
	mockery -dir . -name minixperimentClient -testonly -inpkg
	mockery -dir . -name Tracking -testonly -inpkg
	mockery -dir ./clients -name S3Client -output ./mocks -outpkg mocks
	mockery -dir ./overridepolling -name FetchAdminIDsClient -output ./mocks -outpkg mocks

# Run all tests, including integration tests
test:
	go test -race -cover -tags=integration -timeout 30s -v ./...

# Run unit tests
unit_test:
	go test -race -cover ./...

# Generate the test-assignments.json file that is read by the unit tests that verify that package Experiments assigns
# requests into groups in the same way that minixperiment-client-js does.
test_assignments:
	node ./gen-test-groups/gen-test-groups.js

# Update dependencies using glide
update:
	glide update --strip-vendor
	glide-vc --use-lock-file --no-tests --only-code

# upload local overrides file to s3
upload_overrides:
	@if [[ ( "$(S3_BUCKET)" = "" ) || "$(LOCAL_OVERRIDES_FILE)" == "" ]]; then\
		echo "Need to set both S3_BUCKET and LOCAL_OVERRIDES_FILE environment variables";\
	else\
		go run cmd/validate_global_overrides/main.go -file $(LOCAL_OVERRIDES_FILE) &&\
		./cmd/scripts/gen_diff.sh &&\
		aws s3 cp $(LOCAL_OVERRIDES_FILE) s3://$(S3_BUCKET)/$(S3_KEY);\
	fi

install_overrides_management:
	go install ./cmd/overrides-management