package mwsclient

import (
	"bytes"
	"compress/gzip"
	"context"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/url"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/aws/client/metadata"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/aws/signer/v4"
)

const (
	serviceName = "monitor-api"
)

type PutMetricDataForAggregationInput struct {
	// Required.
	MetricReports []*MetricReport `json:"MetricReports"`
}

type MetricReport struct {
	// Required. Metadata specifies parameters used by the PMET aggregation
	// pipeline.
	Metadata *MetricMetadata `json:"Metadata"`
	// Required. The namespace "Amazon/Schema/Service" indicates that this
	// data is for the amazon service schema and it will be validated as such.
	//
	// Possible values are "Amazon/Schema/Service" and
	// "Amazon/Schema/Network".
	Namespace string    `json:"Namespace"`
	Metrics   []*Metric `json:"Metrics"`
}

type MetricMetadata struct {
	// Optional. An identifier of the customer making the requests.
	CustomerID string `json:"CustomerId"`
	// Optional. The name of the hosts sending the data.
	Host string `json:"Host"`
	// Required. Used by PMET to distribute the load. Normally should be set
	// to the name of the host sending the data. Do NOT set this to a constant
	// value without contacting the monitoring team.
	PartitionID string `json:"PartitionId"`
	// Optional. The name of the apollo environment associated with the
	// metrics. If there is more than one environment split them into seperate
	// "MetricReports". Note: this value will not be aggregated to any
	// dimension of your final metric, we simply use this to decide how to
	// partition your metrics through the aggregation pipeline.
	Environment string `json:"Environment"`
	// Optional. The apollo stage associated with the metrics. Note: this
	// value will not be aggregated to any dimension of your final metric, we
	// simply use this to decide how to partition your metrics through the
	// aggregation pipeline.
	//
	// Possible values are "Alpha", "Beta", "Gamma", and "Prod", though they can be customized.
	Stage string `json:"Stage"`
	// Required. Legacy field representing MinPeriod. No longer matters, though this needs to be set to  "OneMinute",
	// "FiveMinute", or "OneHour" (all periods are now available by default)
	MinPeriod string `json:"MinPeriod"`
	// Required. Legacy field representing MaxPeriod. No longer matters, though this needs to be set to  "OneMinute",
	// "FiveMinute", or "OneHour" (all periods are now available by default) The value must be at least MinPeriod.
	MaxPeriod string `json:"MaxPeriod"`
}

type Metric struct {
	// Required.
	Dimensions isSchemaDimensions `json:"Dimensions"`
	// Required.
	MetricName string `json:"MetricName"`
	// Required.
	Timestamp time.Time `json:"Timestamp"`
	// Required.
	Unit string `json:"Unit"`
	// Optional. The values of the metric.
	//
	// Only non-negative numeric values are allowed.
	Values []float64 `json:"Values,omitempty"`
	// Optional. The weight values associated with the Values, equivalent to
	// 'n' (SampleCount) in PutMetricData API. Defaults to 1.
	Weights []float64 `json:"Weights,omitempty"`
	// Optional. This is only recommended to be set when specifying Values and
	// optionally Weights in your request. AggregationType specifies the type
	// of Aggregation that the server uses for the metric prior to publishing
	// it to the data-store. "Simple" calculates a Simple Distribution
	// allowing for Min, Max, Avg, Sum & SampleCount to be retrieved. "Dist"
	// and "SEH1" calculate a complete distribution internally from the
	// samples provided in your request (allowing other statistics. Eg.
	// Percentiles).
	//
	// Possible values are "Simple", "Dist" (default), and "SEH1".
	AggregationType string `json:"AggregationType,omitempty"`
	// Optional. A "Simple" or "SEH1" distribution pre-calculated by the client.
	//
	// Clear the AggregationType when setting this field, as the aggregation has already been done.
	Distribution *Distribution `json:"Distribution,omitempty"`
}

// Distribution describes a Sparse Exponential Histogram (when the SEH1 field is set) or a
// Simple distribution of data points.
//
// https://w.amazon.com/index.php/Monitoring/Documentation/MonitoringWebService/PutMetricDataForAggregation#Distribution_Input
// https://w.amazon.com/index.php/MonitoringTeam/MetricAgent/SEHAggregation
// https://w.amazon.com/index.php/MonitoringTeam/MonitoringExperience/Fridge/SEH_And_Extended_Stats
type Distribution struct {
	// Required
	SampleCount int `json:"SampleCount"`
	// Required
	Minimum float64 `json:"Minimum"`
	// Required
	Maximum float64 `json:"Maximum"`
	// Required
	Sum float64 `json:"Sum"`
	// Optional
	SEH1 map[string]int `json:"SEH1,omitempty"`
}

// isSchemaDimensions ensures that the Dimensions provided correspond to a
// schema known to MWS.
//
// See
// https://w.amazon.com/index.php/Monitoring/Documentation/MonitorWebservice/Reference/MetricSchemas#Schema_and_Dimension_Listing
// for a complete list of schemas.
//
// TODO: Add a "namespace() string" method to check against the MetricReport's Namespace.
type isSchemaDimensions interface {
	isSchemaDimensions()
}

type ServiceSchemaDimensions struct {
	DataSet     string `json:"DataSet"`
	Marketplace string `json:"Marketplace"`
	HostGroup   string `json:"HostGroup"`
	Host        string `json:"Host"`
	ServiceName string `json:"ServiceName"`
	MethodName  string `json:"MethodName"`
	Client      string `json:"Client"`
	MetricClass string `json:"MetricClass"`
	Instance    string `json:"Instance"`
}

func (*ServiceSchemaDimensions) isSchemaDimensions() {}

type PutMetricDataForAggregationOutput struct {
	XMLName xml.Name `xml:"PutMetricDataForAggregationResponse"`

	NumberOfAttempted int `xml:"NumberOfAttempted"`
	NumberOfCommitted int `xml:"NumberOfCommitted"`
}

type MWS struct {
	*client.Client
}

// https://w.amazon.com/index.php/MonitoringWebService/Docs/MonitoringWebServiceEndpoints
var endpts = map[string]endpoints.ResolvedEndpoint{
	"cn-north-1":     endpoints.ResolvedEndpoint{URL: "https://monitor-api-public.cn-north-1.amazonaws.com.cn"},
	"ap-south-1":     endpoints.ResolvedEndpoint{URL: "https://monitor-api-public.ap-south-1.amazonaws.com"},
	"us-east-2":      endpoints.ResolvedEndpoint{URL: "https://monitor-api-public.us-east-2.amazonaws.com"},
	"us-iso-east-1":  endpoints.ResolvedEndpoint{URL: "https://monitor-api-public.us-iso-east-1.c2s.ic.gov"},
	"eu-west-1":      endpoints.ResolvedEndpoint{URL: "https://monitor-api-public-dub.amazon.com"},
	"eu-central-1":   endpoints.ResolvedEndpoint{URL: "https://monitor-api-public.eu-central-1.amazonaws.com"},
	"sa-east-1":      endpoints.ResolvedEndpoint{URL: "https://monitor-api-public-gru.amazon.com"},
	"us-east-1":      endpoints.ResolvedEndpoint{URL: "https://monitor-api-public-iad.amazon.com"},
	"ap-northeast-2": endpoints.ResolvedEndpoint{URL: "https://monitor-api-public.ap-northeast-2.amazonaws.com"},
	"ap-northeast-1": endpoints.ResolvedEndpoint{URL: "https://monitor-api-public-nrt.amazon.com"},
	"eu-west-2":      endpoints.ResolvedEndpoint{URL: "https://monitor-api-public.eu-west-2.amazonaws.com"},
	"us-gov-west-1":  endpoints.ResolvedEndpoint{URL: "https://monitor-api-public-pdt.amazon.com", SigningRegion: "us-west-2"},
	"us-west-2":      endpoints.ResolvedEndpoint{URL: "https://monitor-api-public-pdx.amazon.com"},
	"cn-amazon":      endpoints.ResolvedEndpoint{URL: "https://monitor-api-public-pek.amazon.com"},
	"us-seattle":     endpoints.ResolvedEndpoint{URL: "https://monitor-api-public-sea.amazon.com"},
	"us-west-1":      endpoints.ResolvedEndpoint{URL: "https://monitor-api-public-sfo.amazon.com"},
	"ap-southeast-1": endpoints.ResolvedEndpoint{URL: "https://monitor-api-public-sin.amazon.com"},
	"ap-southeast-2": endpoints.ResolvedEndpoint{URL: "https://monitor-api-public-syd.amazon.com"},
	"us-titan":       endpoints.ResolvedEndpoint{URL: "https://monitor-api-public-titan.amazon.com", SigningRegion: "us-seattle"},
	"ca-central-1":   endpoints.ResolvedEndpoint{URL: "https://monitor-api-public.ca-central-1.amazonaws.com"},
	"cn-northwest-1": endpoints.ResolvedEndpoint{URL: "https://monitor-api-public.cn-northwest-1.amazonaws.com.cn"},
	"us-isob-east-1": endpoints.ResolvedEndpoint{URL: "https://monitor-api-public.us-isob-east-1.sc2s.sgov.gov"},
	"eu-west-3":      endpoints.ResolvedEndpoint{URL: "https://monitor-api-public.eu-west-3.amazonaws.com"},
	"eu-south-1":     endpoints.ResolvedEndpoint{URL: "https://monitor-api-public.eu-south-1.amazonaws.com"},
	"ap-northeast-3": endpoints.ResolvedEndpoint{URL: "https://monitor-api-public.ap-northeast-3.amazonaws.com"},
}

func resolveEndpoint(service, region string, opts ...func(*endpoints.Options)) (endpoints.ResolvedEndpoint, error) {
	if service != serviceName {
		return endpoints.ResolvedEndpoint{}, fmt.Errorf("unknown service %q", service)
	}
	override, ok := endpts[region]
	if !ok {
		return endpoints.ResolvedEndpoint{}, fmt.Errorf("unknown region %q", region)
	}

	ret := endpoints.ResolvedEndpoint{
		URL:           override.URL,
		SigningRegion: region,
		SigningName:   service,
		SigningMethod: "v4",
	}

	if override.SigningRegion != "" {
		ret.SigningRegion = override.SigningRegion
	}
	if override.SigningMethod != "" {
		ret.SigningMethod = override.SigningMethod
	}
	if override.SigningName != "" {
		ret.SigningName = override.SigningName
	}
	return ret, nil
}

func New(p client.ConfigProvider, cfgs ...*aws.Config) *MWS {
	cfgs = append([]*aws.Config{
		aws.NewConfig().WithEndpointResolver(endpoints.ResolverFunc(resolveEndpoint)),
	}, cfgs...)
	cc := p.ClientConfig(serviceName, cfgs...)

	info := metadata.ClientInfo{
		ServiceName:   serviceName,
		SigningName:   cc.SigningName,
		SigningRegion: cc.SigningRegion,
		Endpoint:      cc.Endpoint,
		APIVersion:    "2007-07-07",
	}

	cl := client.New(*cc.Config, info, cc.Handlers)
	cl.Handlers.Sign.PushBackNamed(v4.SignRequestHandler)
	cl.Handlers.Build.PushBack(buildZipJSON)
	cl.Handlers.UnmarshalMeta.PushBack(unmarshalMetaXML)
	cl.Handlers.UnmarshalError.PushBack(unmarshalErrorXML)

	c := &MWS{
		Client: cl,
	}
	return c
}

// PutMetricDataForAggregation provides a mechanism for clients to publish
// metrics for real time aggregation with data for the same metrics published
// from other sources. The metrics are currently published to CloudWatch, the
// data is stored in CloudWatch backed datastores, alarms are also evaluated
// against CloudWatch. Once the data is put and aggregated, it will be
// available for "Get" via all the available methods (GetMetricData,
// GetMetricDataBulk, GetGraph).
//
// Most metrics are by default aggregated into 1-minute, 5-minute and 1-hour
// datapoints.
//
// https://w.amazon.com/index.php/Monitoring/Documentation/MonitoringWebService/PutMetricDataForAggregation
func (c *MWS) PutMetricDataForAggregation(ctx context.Context, input *PutMetricDataForAggregationInput, opts ...request.Option) (*PutMetricDataForAggregationOutput, error) {
	const action = "PutMetricDataForAggregation"
	q := make(url.Values)
	q.Set("Version", c.ClientInfo.APIVersion)
	q.Set("Action", action)
	op := &request.Operation{
		Name:       action,
		HTTPMethod: "POST",
		HTTPPath: (&url.URL{
			Path:     "/",
			RawQuery: q.Encode(),
		}).String(),
	}

	output := new(PutMetricDataForAggregationOutput)
	req := c.NewRequest(op, input, output)
	req.SetContext(ctx)
	req.ApplyOptions(opts...)
	err := req.Send()

	if err != nil {
		return nil, err
	}
	return output, nil
}

func buildZipJSON(req *request.Request) {
	var buf []byte
	var err error
	if req.ParamsFilled() {
		buf, err = json.Marshal(req.Params)
		if err != nil {
			req.Error = awserr.New(request.ErrCodeSerialization, "failed encoding JSON RPC request", err)
			return
		}
	} else {
		buf = []byte("{}")
	}

	var w bytes.Buffer
	gw := gzip.NewWriter(&w)
	_, err = gw.Write(buf)
	if err != nil {
		req.Error = awserr.New(request.ErrCodeSerialization, "failed encoding JSON GZIP RPC request", err)
		return
	}
	err = gw.Close()
	if err != nil {
		req.Error = awserr.New(request.ErrCodeSerialization, "failed encoding JSON GZIP RPC request", err)
		return
	}

	req.SetBufferBody(w.Bytes())

	req.HTTPRequest.Header.Add("Content-Type", "application/x-gzip")
}

// unmarshalMetaXML is used as an UnmarshalMeta handler. The
// request.Request.Send method of aws-sdk-go calls this function to decode all
// HTTP responses, after the Send handlers have succeeded.
func unmarshalMetaXML(req *request.Request) {
	defer req.HTTPResponse.Body.Close()
	bodyBytes, err := ioutil.ReadAll(req.HTTPResponse.Body)
	if err != nil {
		req.Error = awserr.New(request.ErrCodeSerialization, "failed reading XML RPC meta response", err)
		return
	}

	err = xml.Unmarshal(bodyBytes, req.Data)
	if err != nil {
		req.Error = awserr.New(request.ErrCodeSerialization, "failed reading XML RPC response", err)
		req.HTTPResponse.Body = ioutil.NopCloser(bytes.NewReader(bodyBytes))
		return
	}
}

// unmarshalErrorXML is used as an UnmarshalError handler. The
// request.Request.Send method of aws-sdk-go calls this function to decode
// responses that the UnmarshalMeta or ValidateResponse handlers have
// determined to be errors.
func unmarshalErrorXML(req *request.Request) {
	defer req.HTTPResponse.Body.Close()
	bodyBytes, err := ioutil.ReadAll(req.HTTPResponse.Body)
	if err != nil {
		req.Error = awserr.New(request.ErrCodeSerialization, "failed reading XML RPC error response", err)
		return
	}

	var v errorResponse
	err = xml.Unmarshal(bodyBytes, &v)
	if err != nil {
		req.Error = awserr.New(request.ErrCodeSerialization, "failed reading XML RPC meta response", err)
		return
	}

	if len(v.Errors) == 0 {
		req.Error = awserr.New(request.ErrCodeSerialization, "invalid XML RPC response", nil)
		return
	}

	code, msg := v.Errors[0].Code, v.Errors[0].Message
	if code == "" {
		code, msg = request.ErrCodeSerialization, "missing error code"
	}
	req.Error = awserr.NewRequestFailure(awserr.New(code, msg, nil), req.HTTPResponse.StatusCode, v.RequestID)
}

type errorResponse struct {
	XMLName   xml.Name
	RequestID string        `xml:"RequestID"`
	Errors    []*errorValue `xml:"Errors"`
}

type errorValue struct {
	Code    string `xml:"Error>Code"`
	Message string `xml:"Error>Message"`
}
