// Code generated by protoc-gen-go. DO NOT EDIT.
// source: destinytwirp/destiny.proto

package destinytwirp

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import timestamp "github.com/golang/protobuf/ptypes/timestamp"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type Event struct {
	Id                   string               `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Domain               string               `protobuf:"bytes,2,opt,name=domain,proto3" json:"domain,omitempty"`
	Payload              []byte               `protobuf:"bytes,3,opt,name=payload,proto3" json:"payload,omitempty"`
	Encoding             string               `protobuf:"bytes,4,opt,name=encoding,proto3" json:"encoding,omitempty"`
	MaxRetries           int32                `protobuf:"varint,5,opt,name=max_retries,json=maxRetries,proto3" json:"max_retries,omitempty"`
	SendAt               *timestamp.Timestamp `protobuf:"bytes,6,opt,name=send_at,json=sendAt,proto3" json:"send_at,omitempty"`
	XXX_NoUnkeyedLiteral struct{}             `json:"-"`
	XXX_unrecognized     []byte               `json:"-"`
	XXX_sizecache        int32                `json:"-"`
}

func (m *Event) Reset()         { *m = Event{} }
func (m *Event) String() string { return proto.CompactTextString(m) }
func (*Event) ProtoMessage()    {}
func (*Event) Descriptor() ([]byte, []int) {
	return fileDescriptor_destiny_3fe877a89764a876, []int{0}
}
func (m *Event) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Event.Unmarshal(m, b)
}
func (m *Event) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Event.Marshal(b, m, deterministic)
}
func (dst *Event) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Event.Merge(dst, src)
}
func (m *Event) XXX_Size() int {
	return xxx_messageInfo_Event.Size(m)
}
func (m *Event) XXX_DiscardUnknown() {
	xxx_messageInfo_Event.DiscardUnknown(m)
}

var xxx_messageInfo_Event proto.InternalMessageInfo

func (m *Event) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *Event) GetDomain() string {
	if m != nil {
		return m.Domain
	}
	return ""
}

func (m *Event) GetPayload() []byte {
	if m != nil {
		return m.Payload
	}
	return nil
}

func (m *Event) GetEncoding() string {
	if m != nil {
		return m.Encoding
	}
	return ""
}

func (m *Event) GetMaxRetries() int32 {
	if m != nil {
		return m.MaxRetries
	}
	return 0
}

func (m *Event) GetSendAt() *timestamp.Timestamp {
	if m != nil {
		return m.SendAt
	}
	return nil
}

type WriteEventsRequest struct {
	Events               []*Event `protobuf:"bytes,1,rep,name=events,proto3" json:"events,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *WriteEventsRequest) Reset()         { *m = WriteEventsRequest{} }
func (m *WriteEventsRequest) String() string { return proto.CompactTextString(m) }
func (*WriteEventsRequest) ProtoMessage()    {}
func (*WriteEventsRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_destiny_3fe877a89764a876, []int{1}
}
func (m *WriteEventsRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_WriteEventsRequest.Unmarshal(m, b)
}
func (m *WriteEventsRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_WriteEventsRequest.Marshal(b, m, deterministic)
}
func (dst *WriteEventsRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_WriteEventsRequest.Merge(dst, src)
}
func (m *WriteEventsRequest) XXX_Size() int {
	return xxx_messageInfo_WriteEventsRequest.Size(m)
}
func (m *WriteEventsRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_WriteEventsRequest.DiscardUnknown(m)
}

var xxx_messageInfo_WriteEventsRequest proto.InternalMessageInfo

func (m *WriteEventsRequest) GetEvents() []*Event {
	if m != nil {
		return m.Events
	}
	return nil
}

type WriteEventsResponse struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *WriteEventsResponse) Reset()         { *m = WriteEventsResponse{} }
func (m *WriteEventsResponse) String() string { return proto.CompactTextString(m) }
func (*WriteEventsResponse) ProtoMessage()    {}
func (*WriteEventsResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_destiny_3fe877a89764a876, []int{2}
}
func (m *WriteEventsResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_WriteEventsResponse.Unmarshal(m, b)
}
func (m *WriteEventsResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_WriteEventsResponse.Marshal(b, m, deterministic)
}
func (dst *WriteEventsResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_WriteEventsResponse.Merge(dst, src)
}
func (m *WriteEventsResponse) XXX_Size() int {
	return xxx_messageInfo_WriteEventsResponse.Size(m)
}
func (m *WriteEventsResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_WriteEventsResponse.DiscardUnknown(m)
}

var xxx_messageInfo_WriteEventsResponse proto.InternalMessageInfo

func init() {
	proto.RegisterType((*Event)(nil), "subs.Event")
	proto.RegisterType((*WriteEventsRequest)(nil), "subs.WriteEventsRequest")
	proto.RegisterType((*WriteEventsResponse)(nil), "subs.WriteEventsResponse")
}

func init() { proto.RegisterFile("destinytwirp/destiny.proto", fileDescriptor_destiny_3fe877a89764a876) }

var fileDescriptor_destiny_3fe877a89764a876 = []byte{
	// 297 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x6c, 0x91, 0x3f, 0x4f, 0xc3, 0x30,
	0x10, 0xc5, 0xe5, 0xfe, 0x49, 0xe1, 0x52, 0x75, 0x30, 0x02, 0x99, 0x2c, 0x8d, 0xca, 0x92, 0xc9,
	0x95, 0xda, 0x89, 0x91, 0x0a, 0x46, 0x16, 0x0b, 0x09, 0x89, 0xa5, 0x72, 0xf1, 0x51, 0x59, 0x6a,
	0xec, 0x10, 0x3b, 0xd0, 0x7e, 0x36, 0xbe, 0x1c, 0xaa, 0x9d, 0xa0, 0x22, 0x18, 0xdf, 0xbb, 0xdf,
	0x9d, 0xdf, 0x9d, 0x21, 0x53, 0xe8, 0xbc, 0x36, 0x07, 0xff, 0xa9, 0xeb, 0x6a, 0xde, 0x0a, 0x5e,
	0xd5, 0xd6, 0x5b, 0x3a, 0x70, 0xcd, 0xc6, 0x65, 0xd3, 0xad, 0xb5, 0xdb, 0x1d, 0xce, 0x83, 0xb7,
	0x69, 0xde, 0xe6, 0x5e, 0x97, 0xe8, 0xbc, 0x2c, 0xab, 0x88, 0xcd, 0xbe, 0x08, 0x0c, 0x1f, 0x3e,
	0xd0, 0x78, 0x3a, 0x81, 0x9e, 0x56, 0x8c, 0xe4, 0xa4, 0x38, 0x17, 0x3d, 0xad, 0xe8, 0x15, 0x24,
	0xca, 0x96, 0x52, 0x1b, 0xd6, 0x0b, 0x5e, 0xab, 0x28, 0x83, 0x51, 0x25, 0x0f, 0x3b, 0x2b, 0x15,
	0xeb, 0xe7, 0xa4, 0x18, 0x8b, 0x4e, 0xd2, 0x0c, 0xce, 0xd0, 0xbc, 0x5a, 0xa5, 0xcd, 0x96, 0x0d,
	0x42, 0xcf, 0x8f, 0xa6, 0x53, 0x48, 0x4b, 0xb9, 0x5f, 0xd7, 0xe8, 0x6b, 0x8d, 0x8e, 0x0d, 0x73,
	0x52, 0x0c, 0x05, 0x94, 0x72, 0x2f, 0xa2, 0x43, 0x97, 0x30, 0x72, 0x68, 0xd4, 0x5a, 0x7a, 0x96,
	0xe4, 0xa4, 0x48, 0x17, 0x19, 0x8f, 0xd9, 0x79, 0x97, 0x9d, 0x3f, 0x75, 0xd9, 0x45, 0x72, 0x44,
	0xef, 0xfc, 0xec, 0x16, 0xe8, 0x73, 0xad, 0x3d, 0x86, 0x0d, 0x9c, 0xc0, 0xf7, 0x06, 0x9d, 0xa7,
	0x37, 0x90, 0x60, 0x30, 0x18, 0xc9, 0xfb, 0x45, 0xba, 0x48, 0xf9, 0xf1, 0x16, 0x3c, 0x40, 0xa2,
	0x2d, 0xcd, 0x2e, 0xe1, 0xe2, 0x57, 0xab, 0xab, 0xac, 0x71, 0xb8, 0x78, 0x84, 0xd1, 0x7d, 0xbc,
	0x23, 0x5d, 0x41, 0x7a, 0x42, 0x50, 0x16, 0xa7, 0xfc, 0x7d, 0x2f, 0xbb, 0xfe, 0xa7, 0x12, 0xc7,
	0xad, 0x26, 0x2f, 0xe3, 0xd3, 0x3f, 0xda, 0x24, 0x61, 0x99, 0xe5, 0x77, 0x00, 0x00, 0x00, 0xff,
	0xff, 0x26, 0xae, 0xa4, 0x52, 0xba, 0x01, 0x00, 0x00,
}
