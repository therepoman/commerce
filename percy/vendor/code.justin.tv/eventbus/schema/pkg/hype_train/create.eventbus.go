// NOTE this is a generated file! do not edit!

package hype_train

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	CreateEventType = "HypeTrainCreate"
)

type Create = HypeTrainCreate

type HypeTrainCreateHandler func(context.Context, *eventbus.Header, *HypeTrainCreate) error

func (h HypeTrainCreateHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &HypeTrainCreate{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterHypeTrainCreateHandler(mux *eventbus.Mux, f HypeTrainCreateHandler) {
	mux.RegisterHandler(CreateEventType, f.Handler())
}

func RegisterCreateHandler(mux *eventbus.Mux, f HypeTrainCreateHandler) {
	RegisterHypeTrainCreateHandler(mux, f)
}

func (*HypeTrainCreate) EventBusName() string {
	return CreateEventType
}
