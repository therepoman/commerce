// NOTE this is a generated file! do not edit!

package hype_train

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	DeleteEventType = "HypeTrainDelete"
)

type Delete = HypeTrainDelete

type HypeTrainDeleteHandler func(context.Context, *eventbus.Header, *HypeTrainDelete) error

func (h HypeTrainDeleteHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &HypeTrainDelete{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterHypeTrainDeleteHandler(mux *eventbus.Mux, f HypeTrainDeleteHandler) {
	mux.RegisterHandler(DeleteEventType, f.Handler())
}

func RegisterDeleteHandler(mux *eventbus.Mux, f HypeTrainDeleteHandler) {
	RegisterHypeTrainDeleteHandler(mux, f)
}

func (*HypeTrainDelete) EventBusName() string {
	return DeleteEventType
}
