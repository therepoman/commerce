// NOTE this is a generated file! do not edit!

package hype_train

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	UpdateEventType = "HypeTrainUpdate"
)

type Update = HypeTrainUpdate

type HypeTrainUpdateHandler func(context.Context, *eventbus.Header, *HypeTrainUpdate) error

func (h HypeTrainUpdateHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &HypeTrainUpdate{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterHypeTrainUpdateHandler(mux *eventbus.Mux, f HypeTrainUpdateHandler) {
	mux.RegisterHandler(UpdateEventType, f.Handler())
}

func RegisterUpdateHandler(mux *eventbus.Mux, f HypeTrainUpdateHandler) {
	RegisterHypeTrainUpdateHandler(mux, f)
}

func (*HypeTrainUpdate) EventBusName() string {
	return UpdateEventType
}
