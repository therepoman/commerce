// NOTE this is a generated file! do not edit!

package stream

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	DeleteEventType = "StreamDelete"
)

type Delete = StreamDelete

type StreamDeleteHandler func(context.Context, *eventbus.Header, *StreamDelete) error

func (h StreamDeleteHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &StreamDelete{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterStreamDeleteHandler(mux *eventbus.Mux, f StreamDeleteHandler) {
	mux.RegisterHandler(DeleteEventType, f.Handler())
}

func RegisterDeleteHandler(mux *eventbus.Mux, f StreamDeleteHandler) {
	RegisterStreamDeleteHandler(mux, f)
}

func (*StreamDelete) EventBusName() string {
	return DeleteEventType
}
