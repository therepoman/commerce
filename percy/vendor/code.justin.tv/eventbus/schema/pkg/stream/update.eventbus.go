// NOTE this is a generated file! do not edit!

package stream

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	UpdateEventType = "StreamUpdate"
)

type Update = StreamUpdate

type StreamUpdateHandler func(context.Context, *eventbus.Header, *StreamUpdate) error

func (h StreamUpdateHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &StreamUpdate{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterStreamUpdateHandler(mux *eventbus.Mux, f StreamUpdateHandler) {
	mux.RegisterHandler(UpdateEventType, f.Handler())
}

func RegisterUpdateHandler(mux *eventbus.Mux, f StreamUpdateHandler) {
	RegisterStreamUpdateHandler(mux, f)
}

func (*StreamUpdate) EventBusName() string {
	return UpdateEventType
}
