// NOTE this is a generated file! do not edit!

package stream

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	CreateEventType = "StreamCreate"
)

type Create = StreamCreate

type StreamCreateHandler func(context.Context, *eventbus.Header, *StreamCreate) error

func (h StreamCreateHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &StreamCreate{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterStreamCreateHandler(mux *eventbus.Mux, f StreamCreateHandler) {
	mux.RegisterHandler(CreateEventType, f.Handler())
}

func RegisterCreateHandler(mux *eventbus.Mux, f StreamCreateHandler) {
	RegisterStreamCreateHandler(mux, f)
}

func (*StreamCreate) EventBusName() string {
	return CreateEventType
}
