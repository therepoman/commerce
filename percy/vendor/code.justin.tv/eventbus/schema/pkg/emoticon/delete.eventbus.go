// NOTE this is a generated file! do not edit!

package emoticon

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	DeleteEventType = "EmoticonDelete"
)

type Delete = EmoticonDelete

type EmoticonDeleteHandler func(context.Context, *eventbus.Header, *EmoticonDelete) error

func (h EmoticonDeleteHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &EmoticonDelete{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterEmoticonDeleteHandler(mux *eventbus.Mux, f EmoticonDeleteHandler) {
	mux.RegisterHandler(DeleteEventType, f.Handler())
}

func RegisterDeleteHandler(mux *eventbus.Mux, f EmoticonDeleteHandler) {
	RegisterEmoticonDeleteHandler(mux, f)
}

func (*EmoticonDelete) EventBusName() string {
	return DeleteEventType
}
