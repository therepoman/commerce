// NOTE this is a generated file! do not edit!

package emoticon

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	CreateEventType = "EmoticonCreate"
)

type Create = EmoticonCreate

type EmoticonCreateHandler func(context.Context, *eventbus.Header, *EmoticonCreate) error

func (h EmoticonCreateHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &EmoticonCreate{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterEmoticonCreateHandler(mux *eventbus.Mux, f EmoticonCreateHandler) {
	mux.RegisterHandler(CreateEventType, f.Handler())
}

func RegisterCreateHandler(mux *eventbus.Mux, f EmoticonCreateHandler) {
	RegisterEmoticonCreateHandler(mux, f)
}

func (*EmoticonCreate) EventBusName() string {
	return CreateEventType
}
