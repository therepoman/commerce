// NOTE this is a generated file! do not edit!

package entitlement_badge

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	CreateEventType = "EntitlementBadgeCreate"
)

type Create = EntitlementBadgeCreate

type EntitlementBadgeCreateHandler func(context.Context, *eventbus.Header, *EntitlementBadgeCreate) error

func (h EntitlementBadgeCreateHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &EntitlementBadgeCreate{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterEntitlementBadgeCreateHandler(mux *eventbus.Mux, f EntitlementBadgeCreateHandler) {
	mux.RegisterHandler(CreateEventType, f.Handler())
}

func RegisterCreateHandler(mux *eventbus.Mux, f EntitlementBadgeCreateHandler) {
	RegisterEntitlementBadgeCreateHandler(mux, f)
}

func (*EntitlementBadgeCreate) EventBusName() string {
	return CreateEventType
}
