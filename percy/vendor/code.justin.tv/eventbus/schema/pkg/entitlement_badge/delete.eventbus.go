// NOTE this is a generated file! do not edit!

package entitlement_badge

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	DeleteEventType = "EntitlementBadgeDelete"
)

type Delete = EntitlementBadgeDelete

type EntitlementBadgeDeleteHandler func(context.Context, *eventbus.Header, *EntitlementBadgeDelete) error

func (h EntitlementBadgeDeleteHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &EntitlementBadgeDelete{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterEntitlementBadgeDeleteHandler(mux *eventbus.Mux, f EntitlementBadgeDeleteHandler) {
	mux.RegisterHandler(DeleteEventType, f.Handler())
}

func RegisterDeleteHandler(mux *eventbus.Mux, f EntitlementBadgeDeleteHandler) {
	RegisterEntitlementBadgeDeleteHandler(mux, f)
}

func (*EntitlementBadgeDelete) EventBusName() string {
	return DeleteEventType
}
