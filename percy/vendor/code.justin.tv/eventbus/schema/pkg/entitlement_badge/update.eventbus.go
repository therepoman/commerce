// NOTE this is a generated file! do not edit!

package entitlement_badge

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	UpdateEventType = "EntitlementBadgeUpdate"
)

type Update = EntitlementBadgeUpdate

type EntitlementBadgeUpdateHandler func(context.Context, *eventbus.Header, *EntitlementBadgeUpdate) error

func (h EntitlementBadgeUpdateHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &EntitlementBadgeUpdate{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterEntitlementBadgeUpdateHandler(mux *eventbus.Mux, f EntitlementBadgeUpdateHandler) {
	mux.RegisterHandler(UpdateEventType, f.Handler())
}

func RegisterUpdateHandler(mux *eventbus.Mux, f EntitlementBadgeUpdateHandler) {
	RegisterEntitlementBadgeUpdateHandler(mux, f)
}

func (*EntitlementBadgeUpdate) EventBusName() string {
	return UpdateEventType
}
