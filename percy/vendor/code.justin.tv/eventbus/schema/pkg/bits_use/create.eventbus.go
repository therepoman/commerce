// NOTE this is a generated file! do not edit!

package bits_use

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	CreateEventType = "BitsUseCreate"
)

type Create = BitsUseCreate

type BitsUseCreateHandler func(context.Context, *eventbus.Header, *BitsUseCreate) error

func (h BitsUseCreateHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &BitsUseCreate{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterBitsUseCreateHandler(mux *eventbus.Mux, f BitsUseCreateHandler) {
	mux.RegisterHandler(CreateEventType, f.Handler())
}

func RegisterCreateHandler(mux *eventbus.Mux, f BitsUseCreateHandler) {
	RegisterBitsUseCreateHandler(mux, f)
}

func (*BitsUseCreate) EventBusName() string {
	return CreateEventType
}
