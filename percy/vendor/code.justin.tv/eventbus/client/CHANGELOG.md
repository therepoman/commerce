# Changelog
All notable changes to the EventBus Client will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.7.0] - 2020-03-10

### Changed
- Publisher and subscriber client constructors now check the installed EventBus CloudFormation version (using the added package below)
  - If CFN >= v1.3.0: do _not_ perform `sns:Publish` and `sqs:ReceiveMessage` API calls under an assumed role
  - If CFN < v1.3.0: perform `sns:Publish` and `sqs:ReceiveMessage` API calls using an assumed role, as is done by default in previous client versions (now legacy behavior)

### Added
- `lowlevel/cfnversion` package; able to lookup the version of the installed EventBus CloudFormation for CFN version v1.3.0 and higher

## [1.6.0] - 2020-01-23

### Added
- New `RetryCountFromContext` method added which reflects the SQS `ApproximateReceiveCount`

## [1.5.0] - 2020-01-21

### Changed
- `testhelpers.MakePayload` can default time&UUID values when omitted.
- Refactored `encryption` package to not break Amazon build systems with a dependency cycle between this repository and the `eventbus/schema` repository
  * Move `authorization.Provider`, `authorization.Encrypter` and `authorization.Decrypter` into `encryption` package
  * `encryption` package `Encrypt<TYPE>` and `Decrypt<TYPE>` functions no longer know about `authorization` protobuf types; instead, they accept and return raw `[]byte`
  * These interfaces now expect `map[string]string` instead of `authorization.Context` (which is just a type alias)

## [1.4.0] - 2020-01-08

### Changed
- Publisher client now creates and stores an authorized field client (see `Added` below) upon initialization
  * Sane defaults are used but can be overriden
- Publisher client embeds the configured `environment` into event header on calls to `Publish`

### Added
- New authorized field client for encrypting and decrypting event fields containing PII
- String `environment` has been added to the EventBus wire header format

## [1.3.0] - 2020-01-07

### Changed
- The context provided to event handlers will respect the SQS message visibility timeout provided to the client
- Expose the low-level SNS marshaling for other integrations and testing usage.

### Added
- Testing helpers added for unmarshaling and for creating SNS payloads.

## [1.2.0] - 2019-10-31

### Added
- Provide a low-level publisher package for people who want to do custom/bespoke eventbus.

### Changed
- STS regional endpoints are used for both Publisher and Subscriber clients by default

## [1.1.0] - 2019-08-26

### Added
- Publisher attaches [a few SNS attributes](docs/message_attributes.md) for potential use in the future
- Publisher sends no-op message(s) at `publisher.New` time to ensure the publisher has permissions to publish to the topic(s)
- Mux/Dispatcher changes:
  * User can override the behavior when no-op messages are received.
  * User can override how messages for unregistered handlers are handled.

### Changed
- Cleaned up mock publisher to be easier to make use of.
- Publisher fetches event stream routes over HTTPS (formerly http)

## [1.0.0] - 2019-07-30

First semver-adhering release of eventbus client.

### Changed
- Client assumes roles for both publishing and subscribing

### Removed
- Removed many old deprecated items, such as dispatch packaged

[1.4.1]: https://git-aws.internal.justin.tv/eventbus/client/compare/v1.4.0...v1.4.1
[1.4.0]: https://git-aws.internal.justin.tv/eventbus/client/compare/v1.3.0...v1.4.0
[1.3.0]: https://git-aws.internal.justin.tv/eventbus/client/compare/v1.2.0...v1.3.0
[1.2.0]: https://git-aws.internal.justin.tv/eventbus/client/compare/v1.1.0...v1.2.0
[1.1.0]: https://git-aws.internal.justin.tv/eventbus/client/compare/v1.0.0...v1.1.0
[1.0.0]: https://git-aws.internal.justin.tv/eventbus/client/compare/v0.2.0...v1.0.0
