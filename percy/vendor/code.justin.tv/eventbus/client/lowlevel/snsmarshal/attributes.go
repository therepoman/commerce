package snsmarshal

import (
	"github.com/aws/aws-sdk-go/aws"
)

const (
	AttrEventType = "eventbus-event-type"
	AttrRandE6    = "eventbus-rand-e6"
	AttrIsNoop    = "eventbus-is-noop"
)

// Data Types defined at
// https://docs.aws.amazon.com/sns/latest/dg/sns-message-attributes.html#SNSMessageAttributes.DataTypes
var (
	DataTypeString = aws.String("String")
	DataTypeNumber = aws.String("Number")
)
