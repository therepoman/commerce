// Package snsmarshal implements the codec specific to marshaling on SNS.
//
// Because SNS uses JSON (utf-8 safe) strings as payload, we cannot simply
// take the bytes from the 'wire' package and throw them in SNS, as any high
// bytes and unprintable characters would end up needing escaping at minimal.
// While one could use unicode glyphs a la `\u205`, this is somewhat inaccurate
// of a representation of byte 205, and also turns each high byte from 1 to 5
// bytes.
//
// For that reason, we have chosen the very common base64 encoding instead,
// for the systems that decode data from SNS (including SQS since it contains
// the SNS payload in the SQS message)
package snsmarshal

import (
	"encoding/base64"
	"io"
)

func EncodeToString(input []byte) string {
	return base64.RawURLEncoding.EncodeToString(input)
}

func DecodeString(input string) ([]byte, error) {
	return base64.RawURLEncoding.DecodeString(input)
}

func NewDecoder(input io.Reader) io.Reader {
	return base64.NewDecoder(base64.RawURLEncoding, input)
}
