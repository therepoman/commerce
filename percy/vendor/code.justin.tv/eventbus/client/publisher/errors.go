package publisher

import "errors"

var (
	errUnregistered = errors.New("event not registered for publishing")
)
