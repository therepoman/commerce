Event Bus Go Client
===================

Client support for the Twitch Event Bus

```bash
go get code.justin.tv/eventbus/client
```

This is a simple getting started document. Narrative documentation and examples are available in the [eventbus docs site](https://git-aws.internal.justin.tv/pages/eventbus/docs) and our [godoc][] has the full API parameter docs.

## Publisher

Publishing to Event Bus requires an aws session with credentials to talk to the SNS topic:

```go
p, err := publisher.New(publisher.Config{
	Session: awsSession,
	Environment: publisher.EnvProd,
	EventTypes: []string{"ClockUpdate", "FooCreate"},
})
```

then, to publish a message, send the initialized struct

```go
message := &clock.ClockUpdate{
	Time: &timestamp.Timestamp{Seconds: 123},
}

err := p.Publish(ctx, message)
// handle error
```

See the [publisher godoc][doc-pub] for more complete configuration docs and examples


## Subscribers

There are multiple ways to receive messages from the event bus, and all use the 
same dispatch logic.

Registering handlers is similar to using a mux in `net/http`,
or registering handlers in Twirp.

```go
import (
	"code.justin.tv/eventbus/schema/pkg/clock" // This repo is where event schemas' generated code lives
	eventbus "code.justin.tv/eventbus/client" // This repo holds client (pub and sub) impls
)

// The handler is called for each incoming message.
func tickHandler(ctx context.Context, h *eventbus.Header, tick *clock.Update) error {
	log.Infof("Got clock tick: %d", tick.Time.Seconds)
	return nil
}

func main() {
	mux := eventbus.NewMux()
	// pass your handler func. Inline funcs are allowed too.
	clock.RegisterUpdateHandler(mux, tickHandler)
}
```

Now, connect the mux to a subscriber implementation. 

Here's an example using the [SQS poller][doc-sqs] implementation:

```go
func main() {
	// setup mux as before
	
	client, err := sqsclient.New(sqsclient.Config{
		Session: session.Must(session.NewSession()),
		Dispatcher: mux.Dispatcher(),
		QueueURL: "<SQS queue URL from your infrastructure stack or AWS console>",
	})
}
```

You can also [build a lambda][doc-lambda-example] with the same mux/handler code:

```go
func main() {
	// setup mux as before

	lambda.Start(lambdafunc.NewSQS(mux.Dispatcher()))
}
```

**Subscriber Implementations**

* [Amazon SQS][doc-sqs] - Run your own client that polls messages from a durable queue
* [Lambda Function Invocation][doc-lambda] - run your processing code as a lambda

For information on how to select one of the above subscriber implementations, read
our [subscriber docs][].

[subscriber docs]: https://git-aws.internal.justin.tv/pages/eventbus/docs/subscribers/#handle-events-in-your-application

[godoc]: https://godoc.internal.justin.tv/code.justin.tv/eventbus/client
[doc-pub]: https://godoc.internal.justin.tv/code.justin.tv/eventbus/client/publisher
[doc-lambda]: https://godoc.internal.justin.tv/code.justin.tv/eventbus/client/subscriber/lambdafunc
[doc-lambda-example]: https://godoc.internal.justin.tv/code.justin.tv/eventbus/client/subscriber/lambdafunc#example-package
[doc-sqs]: https://godoc.internal.justin.tv/code.justin.tv/eventbus/client/subscriber/sqsclient
