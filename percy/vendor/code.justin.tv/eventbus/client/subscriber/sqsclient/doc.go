/*
Package sqsclient polls an SQS queue to dispatch event bus messages.

The SQS Client, like the other subscriber implementations, will call a provided
Dispatcher with Event Bus messages to be processed. How this differs from the
SNS and Lambda invocation implementations is that this is a 'poll'
implementation, so there's significantly more tuning the user of the library
will have to know and manage.

Concurrency

The SQS implementation uses long-polling to pull some messages off the queue
with at least MinPollers concurrency. If the poller frequently returns with
the maximum number of messages very quickly, it will start to increase the
number of polling goroutines until the processing system is saturated.

Messages are then sent to your dispatcher with no more than DeliverConcurrency
simultaneous goroutines dispatching at once.

Acks and Visibility

An important tuning parameter for your message processing is the "visibility
timeout". The visibility timeout should be set to something longer than what
99th percentile of processing takes. Read more on visibility here:

https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-visibility-timeout.html

If your dispatcher returns with no error, then it is presumed that the
processing work succeeded, and the message is deleted off the queue for you.
If your dispatch function returns any error value, then no deletion is
performed. Eventually, the message will show up again (re-delivery) in the
queue (perhaps on a different worker machine).

*/
package sqsclient
