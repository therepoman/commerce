package splatter

import (
	"time"

	"github.com/cactus/go-statsd-client/statsd"

	log "github.com/sirupsen/logrus"
)

// LogStatter is a debugging tool for logging  metrics that are being sent to a statsd.Statter interface.
// This can be swapped in for a real statter or added to a composite statter to aid testing of metrics recording
type LogStatter struct {
	prefix string
}

// NewLogStatter returns a new LogStatter which will log all called metrics to log.Debug
func NewLogStatter(prefix string) (statsd.Statter, error) {
	return &LogStatter{
		prefix: prefix,
	}, nil
}

// Inc increments a statsd count type.
// stat is a string name for the metric.
// value is the integer value
// rate is the sample rate (0.0 to 1.0)
func (s *LogStatter) Inc(stat string, value int64, rate float32) error {
	log.WithFields(log.Fields{
		"prefix": s.prefix,
		"stat":   stat,
		"value":  value,
		"rate":   rate,
	}).Debug("LogStatter::Inc")
	return nil
}

// Dec decrements a statsd count type.
// stat is a string name for the metric.
// value is the integer value.
// rate is the sample rate (0.0 to 1.0)
func (s *LogStatter) Dec(stat string, value int64, rate float32) error {
	log.WithFields(log.Fields{
		"prefix": s.prefix,
		"stat":   stat,
		"value":  value,
		"rate":   rate,
	}).Debug("LogStatter::Dec")
	return nil
}

// Gauge submits/updates a statsd gauge type.
// stat is a string name for the metric.
// value is the integer value.
// rate is the sample rate (0.0 to 1.0)
func (s *LogStatter) Gauge(stat string, value int64, rate float32) error {
	log.WithFields(log.Fields{
		"prefix": s.prefix,
		"stat":   stat,
		"value":  value,
		"rate":   rate,
	}).Debug("LogStatter::Gauge")
	return nil
}

// GaugeDelta submits a delta to a statsd gauge.
// stat is the string name for the metric.
// value is the (positive or negative) change.
// rate is the sample rate (0.0 to 1.0).
func (s *LogStatter) GaugeDelta(stat string, value int64, rate float32) error {
	log.WithFields(log.Fields{
		"prefix": s.prefix,
		"stat":   stat,
		"value":  value,
		"rate":   rate,
	}).Debug("LogStatter::GaugeDelta")
	return nil
}

// Timing submits a statsd timing type.
// stat is a string name for the metric.
// delta is the time duration value in milliseconds
// rate is the sample rate (0.0 to 1.0).
func (s *LogStatter) Timing(stat string, delta int64, rate float32) error {
	log.WithFields(log.Fields{
		"prefix": s.prefix,
		"stat":   stat,
		"delta":  delta,
		"rate":   rate,
	}).Debug("LogStatter::Timing")
	return nil
}

// TimingDuration submits a statsd timing type.
// stat is a string name for the metric.
// delta is the timing value as time.Duration
// rate is the sample rate (0.0 to 1.0).
func (s *LogStatter) TimingDuration(stat string, delta time.Duration, rate float32) error {
	log.WithFields(log.Fields{
		"prefix": s.prefix,
		"stat":   stat,
		"delta":  delta,
		"rate":   rate,
	}).Debug("LogStatter::TimingDuration")
	return nil
}

// Set submits a stats set type
// stat is a string name for the metric.
// value is the string value
// rate is the sample rate (0.0 to 1.0).
func (s *LogStatter) Set(stat string, value string, rate float32) error {
	log.WithFields(log.Fields{
		"prefix": s.prefix,
		"stat":   stat,
		"value":  value,
		"rate":   rate,
	}).Debug("LogStatter::Set")
	return nil
}

// SetInt submits a number as a stats set type.
// stat is a string name for the metric.
// value is the integer value
// rate is the sample rate (0.0 to 1.0).
func (s *LogStatter) SetInt(stat string, value int64, rate float32) error {
	log.WithFields(log.Fields{
		"prefix": s.prefix,
		"stat":   stat,
		"value":  value,
		"rate":   rate,
	}).Debug("LogStatter::SetInt")
	return nil
}

// Raw submits a preformatted value.
// stat is the string name for the metric.
// value is a preformatted "raw" value string.
// rate is the sample rate (0.0 to 1.0).
func (s *LogStatter) Raw(stat string, value string, rate float32) error {
	log.WithFields(log.Fields{
		"prefix": s.prefix,
		"stat":   stat,
		"value":  value,
		"rate":   rate,
	}).Debug("LogStatter::Raw")
	return nil
}

// SetSamplerFunc sets a sampler function to something other than the default
// sampler is a function that determines whether the metric is
// to be accepted, or discarded.
// An example use case is for submitted pre-sampled metrics.
func (s *LogStatter) SetSamplerFunc(sampler statsd.SamplerFunc) {}

// SetPrefix sets/updates the statsd client prefix.
// Note: Does not change the prefix of any SubStatters.
func (s *LogStatter) SetPrefix(prefix string) {
	s.prefix = prefix
}

// NewSubStatter returns a SubStatter with appended prefix
func (s *LogStatter) NewSubStatter(prefix string) statsd.SubStatter {
	var c *LogStatter
	if s != nil {
		c = &LogStatter{
			prefix: joinPathComponents(s.prefix, prefix),
		}
	}
	return c
}

// Close closes the connection and cleans up.
func (s *LogStatter) Close() error {
	// No-op. No shutdown work required for the LogStatter
	return nil
}
