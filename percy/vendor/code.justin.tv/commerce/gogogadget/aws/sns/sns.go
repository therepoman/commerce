package sns

import (
	"context"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
)

const (
	defaultRegion = "us-west-2"
)

type Client interface {
	Publish(ctx context.Context, topicARN string, message string) error
}

type SDKClientWrapper struct {
	SDKClient *sns.SNS
}

func NewDefaultConfig() *aws.Config {
	return &aws.Config{
		Region: aws.String(defaultRegion),
	}
}

func NewDefaultClient() Client {
	return &SDKClientWrapper{
		SDKClient: sns.New(session.New(), NewDefaultConfig()),
	}
}

func (c *SDKClientWrapper) Publish(ctx context.Context, topicARN string, message string) error {
	publishInput := &sns.PublishInput{
		TopicArn: aws.String(topicARN),
		Message:  aws.String(message),
	}
	_, err := c.SDKClient.PublishWithContext(ctx, publishInput)
	if err != nil {
		return err
	}
	return nil
}
