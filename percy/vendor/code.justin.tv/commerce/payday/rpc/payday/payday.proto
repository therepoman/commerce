syntax = "proto3";

import "google/protobuf/timestamp.proto";
package code.justin.tv.commerce.paydayrpc;
option go_package = "paydayrpc";

service Payday {
    // Health Check API
    rpc HealthCheck (HealthCheckReq) returns (HealthCheckResp) {};

    rpc GetBalance (GetBalanceReq) returns (GetBalanceResp) {};
    rpc GetBitsToBroadcaster (GetBitsToBroadcasterReq) returns (GetBitsToBroadcasterResp) {};

    // Bits in Extensions API's
    rpc IsEligibleToUseBitsOnExtension (IsEligibleToUseBitsOnExtensionReq) returns (IsEligibleToUseBitsOnExtensionResp) {};
    rpc UseBitsOnExtension (UseBitsOnExtensionReq) returns (UseBitsOnExtensionResp) {};
    rpc GetExtensionTransactions (GetExtensionTransactionsReq) returns (GetExtensionTransactionsResp) {}

    // Sponsored Cheermote API's
    rpc GetUserBitsForCampaign (GetUserBitsForCampaignReq) returns (GetUserBitsForCampaignResp) {};
    rpc GetUserCampaignStatuses (GetUserCampaignStatusesReq) returns (GetUserCampaignStatusesResp) {};
    rpc SetUserCampaignStatus (SetUserCampaignStatusReq) returns (SetUserCampaignStatusResp) {};

    // Bits Products APIs
    rpc CheckBitsProductsEligibility (CheckBitsProductsEligibilityReq) returns (CheckBitsProductsEligibilityResp);

    // Bits Bulk Grant APIs
    rpc GetBitsBulkGrantCampaigns (GetBitsBulkGrantCampaignsReq) returns (GetBitsBulkGrantCampaignsResp);

    // User settings APIs
    rpc GetUserSettings (GetUserSettingsReq) returns (GetUserSettingsResp);
    rpc SetUserSettings (SetUserSettingsReq) returns (SetUserSettingsResp);

    // Badge Tier Notifications
    rpc GetBadgeTierNotification (GetBadgeTierNotificationReq) returns (GetBadgeTierNotificationResp);
    rpc SetBadgeTierNotificationState (SetBadgeTierNotificationStateReq) returns (SetBadgeTierNotificationStateResp);

    rpc GetHypedChannels(GetHypedChannelsReq) returns (GetHypedChannelsResp);

    // Badge Selection
    rpc GetUserBitsBadgesAvailableForChannel(GetUserBitsBadgesAvailableForChannelReq) returns (GetUserBitsBadgesAvailableForChannelResp);

    // Bits Badge Tier Emote Rewards
    rpc GetBitsTierSummaryForEmoteGroupID(GetBitsTierSummaryForEmoteGroupIDReq) returns (GetBitsTierSummaryForEmoteGroupIDResp);
    rpc GetBitsBadgeTierEmotes(GetBitsBadgeTierEmotesReq) returns (GetBitsBadgeTierEmotesResp);
    rpc AdminGrantUnlockedBadgeTierRewards(AdminGrantUnlockedBadgeTierRewardsReq) returns (AdminGrantUnlockedBadgeTierRewardsResp);
    rpc AssignEmoteToBitsTier(AssignEmoteToBitsTierReq) returns (AssignEmoteToBitsTierResp);
    rpc GetBitsTierEmoteGroups(GetBitsTierEmoteGroupsReq) returns (GetBitsTierEmoteGroupsResp);

    // Voldemote Management
    rpc ListVoldemotes(ListVoldemotesReq) returns (ListVoldemotesResp);
    rpc CreateOrUpdateVoldemote(CreateOrUpdateVoldemoteReq) returns (CreateOrUpdateVoldemoteResp);
    rpc RemoveVoldemote(RemoveVoldemoteReq) returns (RemoveVoldemoteResp);
    rpc ModerateVoldemote(ModerateVoldemoteReq) returns (ModerateVoldemoteResp);
    rpc CreateOrUpdateVoldemotePack(CreateOrUpdateVoldemotePackReq) returns (CreateOrUpdateVoldemotePackResp);
    rpc ListVoldemotePacksForBroadcaster(ListVoldemotePacksForBroadcasterReq) returns (ListVoldemotePacksForBroadcasterResp);
    rpc ListVoldemotePacksForViewer(ListVoldemotePacksForViewerReq) returns (ListVoldemotePacksForViewerResp);

    // Holds API
    rpc CheckHold(CheckHoldReq) returns (CheckHoldResp);
    rpc CreateHold(CreateHoldReq) returns (CreateHoldResp);
    rpc ReleaseHold(ReleaseHoldReq) returns (ReleaseHoldResp);
    rpc FinalizeHold(FinalizeHoldReq) returns (FinalizeHoldResp);

    // Transactions
    rpc GetTransactionsByID (GetTransactionsByIDReq) returns (GetTransactionsByIDResp);
    rpc SearchTransactions (SearchTransactionsReq) returns (SearchTransactionsResp);

    // Bits in Polls API
    rpc UseBitsOnPoll (UseBitsOnPollReq) returns (UseBitsOnPollResp) {};

    // Bits Auto Refill Settings APIs
    rpc GetAutoRefillSettings (GetAutoRefillSettingsReq) returns (GetAutoRefillSettingsResp) {};
    rpc SetAutoRefillSettings (SetAutoRefillSettingsReq) returns (SetAutoRefillSettingsResp) {};

    // Actions API V2
    rpc GetGlobalCheers (GetGlobalCheersReq) returns (GetCheersResp) {};
    rpc GetChannelCheers (GetChannelCheersReq) returns (GetCheersResp) {};

    // Cheermote Management APIs
    rpc UpdateCheermoteTier (UpdateCheermoteTierReq) returns (UpdateCheermoteTierResp);
    rpc DeleteCheermoteTier (DeleteCheermoteTierReq) returns (DeleteCheermoteTierResp);

    // Screenshot experiment APIs
    rpc GetCheerScreenshotsForChannel(GetCheerScreenshotsForChannelReq) returns (GetCheerScreenshotsForChannelResp);
    rpc GetCheerScreenshotFromId(GetCheerScreenshotFromIdReq) returns (GetCheerScreenshotFromIdResp);

    // Pinata experiment APIs
    rpc GrantPinataBonusBits (GrantPinataBonusBitsReq) returns (GrantPinataBonusBitsResp) {};
    rpc MicroCheer (MicroCheerReq) returns (MicroCheerResp) {};
}

message HealthCheckReq {}

message HealthCheckResp {
    string message = 1;
}

message GetBalanceReq {
    string userId = 1;
}

message GetBalanceResp {
    int32 balance = 1;
}

message GetBitsToBroadcasterReq {
    string userId = 1;
    string channelId = 2;
}

message GetBitsToBroadcasterResp {
    int32 bitsToBroadcaster = 1;
}

message UseBitsOnExtensionReq {
    string tuid = 1;
    string transactionid = 2;
    string ipAddress = 3;
    string zipCode = 4;
    string countryCode = 5;
    string city = 6;
}

message UseBitsOnExtensionResp {
    int32 balance = 1;
}

message IsEligibleToUseBitsOnExtensionReq {
    string tuid = 1;
    string extensionClientId = 2;
    string sKU = 3;
    string transactionId = 4;
    string channelId = 5;
}

message IsEligibleToUseBitsOnExtensionResp {
    bool promptRequired = 1;
    int32 bitsRequired = 2;
    string transactionId = 3;
    int32 balance = 4;
    bool sufficientBalance = 5;
}

message GetExtensionTransactionsReq {
    string tuid = 1;
    string extensionClientId = 2;
    string channelId = 3;
    string cursor = 4;
}

message ExtensionTransaction {
    string tuid = 1;
    string transactionId = 2;
    string extensionClientId = 3;
    string channelId = 4;
    string sKU = 5;
    int32 bitsCost = 6;
    bool inDevelopment = 7;
    bool broadcast = 8;
    google.protobuf.Timestamp createTime = 9;
    string status = 10;
}

message GetExtensionTransactionsResp {
    repeated ExtensionTransaction transactions = 1;
    string cursor = 2;
}

message GetUserBitsForCampaignReq {
    string user_id = 1;
    string campaign_id = 2;
}

message GetUserBitsForCampaignResp {
    int64 bits_used = 1;
}

message GetUserCampaignStatusesReq {
    string user_id = 1;
}

message UserCampaignStatus {
    string campaign_id = 1;
    bool opted_in = 2;
    string brand_name = 3;
    string brand_image_url = 4;
    map<int64, double> sponsored_amount_thresholds = 5;
    google.protobuf.Timestamp start_time = 6;
    google.protobuf.Timestamp end_time = 7;
}

message GetUserCampaignStatusesResp {
    repeated UserCampaignStatus campaign_statuses = 1;
}

message SetUserCampaignStatusReq {
    string user_id = 1;
    string campaign_id = 2;
    bool opted_in = 3;
}

message SetUserCampaignStatusResp {
}

message CheckBitsProductsEligibilityReq {
    string user_id = 1;
    string product_id = 2;
    int32 quantity = 3;
    string country_code = 4;
    string locale = 5;
    string platform = 6;
}

message CheckBitsProductsEligibilityResp {
    bool isEligible = 1;
    string ineligibleCode = 2;
}

message GetBitsBulkGrantCampaignsReq {

}

message GetBitsBulkGrantCampaignsResp {
    repeated BitsBulkGrantCampaign bulk_grant_campaigns = 1;
}

message BitsBulkGrantCampaign {
    string id = 1;
    string name = 2;
    string bit_type = 3;
    int32 limit_per_user = 4;
    int32 bit_amount = 5;
    string admin_reason = 6;
    string pushy_notification_origin = 7;
}

message GetUserSettingsReq {
    string user_id = 1;
}

message GetUserSettingsResp {
    bool skipped_first_cheer_tutorial = 1;
    bool abandoned_first_cheer_tutorial = 2;
}

message SetUserSettingsReq {
    string user_id = 1;
    bool skipped_first_cheer_tutorial = 2;
    bool abandoned_first_cheer_tutorial = 3;
}

message SetUserSettingsResp {
}

message GetBadgeTierNotificationReq {
    string user_id = 1;
    string channel_id = 2;
}

message GetBadgeTierNotificationResp {
    string notification_state = 1;
    int64 threshold = 2;
    string notification_id = 3;
    bool can_share = 4;
    repeated string emote_ids = 5;
}

message SetBadgeTierNotificationStateReq {
    string notification_id = 1;
    string notification_state = 2;
    string message = 3;
    string user_id = 4;
}

message SetBadgeTierNotificationStateResp {
}

// Badge Selection

message GetUserBitsBadgesAvailableForChannelReq {
    string user_id = 1; // required
    string channel_id = 2; // required
}

message Badge {
    string badge_set_id = 1; // required
    string badge_set_version = 2; // required
}

message GetUserBitsBadgesAvailableForChannelResp {
    repeated Badge available_badges = 1;
}

message GetBitsTierSummaryForEmoteGroupIDReq {
    string group_id = 1; //required
    string user_id = 2; //optional
}

message GetBitsTierSummaryForEmoteGroupIDResp {
    string group_id = 1;
    int64 badge_tier_threshold = 2 [deprecated=true];
    string emote_channel_id = 3;
    bool is_emote_unlocked_for_user = 4 [deprecated=true];
    int64 number_of_bits_until_unlock = 5 [deprecated=true];
    EmoteBitsBadgeTierSummary badge_tier_summary = 6;
}

message EmoteBitsBadgeTierSummary {
    int64 badge_tier_threshold = 1;
    bool is_emote_unlocked_for_user = 2;
    int64 number_of_bits_until_unlock = 3;
}

message GetBitsBadgeTierEmotesReq {
    string channel_id = 1;
    string user_id = 2;
    BitsBadgeTierEmoteFilter filter = 3; //Default to all
}

enum BitsBadgeTierEmoteFilter {
    ALL = 0;
    UNLOCKED = 1;
    LOCKED = 2;
    LOCKED_PREVIEW = 3;
    HIGHEST_UNLOCKED_AND_NEXT = 4;
}

message GetBitsBadgeTierEmotesResp {
    repeated BitsBadgeTierEmote emotes = 1;
}

message BitsBadgeTierEmote {
    string emote_id = 1;
    string emote_code = 2;
    string emote_channel_id = 3;
    string emote_group_id = 4;
    EmoteBitsBadgeTierSummary badge_tier_summary = 5;
}

message AdminGrantUnlockedBadgeTierRewardsReq {
    string user_id = 1;
    string channel_id = 2;
}

message AdminGrantUnlockedBadgeTierRewardsResp {
    string user_id = 1;
    string channel_id = 2;
    int64 total_bits_cheered = 3;
}

message GetHypedChannelsReq {
    google.protobuf.Timestamp start_time = 1; // required
    google.protobuf.Timestamp end_time = 2; // required

}

message HypedChannel {
    string channel_id = 1;
    int64 total_bits_cheered = 2;
}

message GetHypedChannelsResp {
    repeated HypedChannel channel = 1;
}

enum VoldemoteState {
    ACTIVE = 0;
    INACTIVE = 1;
    MODERATED = 2;
    PENDING = 3;
    ARCHIVED = 4;
}

message ImageSet {
    string image1x = 1;
    string image2x = 2;
    string image4x = 3;
}

message Voldemote {
    string id = 1;
    string text_code = 2;
    string description = 3;
    VoldemoteState state = 4;
    ImageSet image_set = 5;
    string owner_id = 6;
    bool is_global = 7;
}

message ListVoldemotesReq {
    string channel_id = 1;
}

message ListVoldemotesResp {
    repeated Voldemote voldemotes = 1;
}

message CreateOrUpdateVoldemoteReq {
    string channel_id = 1;
    string suffix = 2;
    string description = 3;
    ImageSet image_set = 4;
    string voldemote_id = 5; // if provided, updates an existing voldemote
    string user_id = 6; // tuid of logged in user, must match channel_id
}

message CreateOrUpdateVoldemoteResp {
    Voldemote voldemote = 1;
}

message ModerateVoldemoteReq {
    string channel_id = 1;
    string voldemote_id = 2;
    string reason = 3;
}

message ModerateVoldemoteResp {}

message VoldemotePack {
    string voldemote_pack_id = 1;
    repeated VoldemotePackSlot slots = 2;
    string voldemote_pack_owner_id = 3;
}

message VoldemotePackSlot {
    int32 slot_index = 1;
    Voldemote voldemote = 2;
}

message ListVoldemotePacksForBroadcasterReq {
    string channel_id = 1;
    string user_id = 2; // tuid of logged in user, must match channel_id
}

message ListVoldemotePacksForBroadcasterResp {
    repeated VoldemotePack voldemote_packs = 1;
}

message VoldemoteSlotUpdate {
    string voldemote_id = 1;
    int32 slot_index = 2;
}

message CreateOrUpdateVoldemotePackReq {
    string channel_id = 1;
    string voldemote_pack_id = 2; // If provided, does an update
    repeated VoldemoteSlotUpdate slots = 3;
    string user_id = 4; // tuid of logged in user, must match channel_id
}

message CreateOrUpdateVoldemotePackResp {
    VoldemotePack voldemote_pack = 1;
}

message RemoveVoldemoteReq {
    string channel_id = 1;
    string voldemote_id = 2;
    string user_id = 3; // tuid of logged in user, must match channel_id
}

message RemoveVoldemoteResp {
}

message ListVoldemotePacksForViewerReq {
    string channel_id = 1;
    string user_id = 2;
}

message ListVoldemotePacksForViewerResp {
    repeated VoldemotePack voldemote_packs = 1;
}

message GetGemNotificationsReq {
    string channel_id = 1;
    string user_id = 2;
}

message GetGemNotificationsResp {
    repeated GemNotification gem_notifications = 1;
}

message GemNotification {
    string id = 1;
    google.protobuf.Timestamp created_at = 2;
    bool   checked     = 3;
    string title       = 4;
    string description = 5;
    bool   active      = 6;
    repeated string channel_ids = 7;
}

message CheckGemNotificationReq {
    string notification_id = 1;
    string user_id = 2;
}

message CheckGemNotificationResp {
    GemNotification checked_gem_notification = 1;
}

message CreateGemNotificationReq {
    string id          = 1;
    bool   active      = 2;
    string title       = 3;
    string description = 4;
    repeated string channel_ids = 5;
}

message CreateGemNotificationResp {
    GemNotification new_gem_notification = 1;
}

message UpdateGemNotificationReq {
    string          id          = 1;
    bool            active      = 2;
    string          title       = 3;
    string          description = 4;
    repeated string channel_ids = 5;
}

message UpdateGemNotificationResp {
    GemNotification updated_gem_notification = 1;
}

message GetAllGemNotificationConfigsReq {}

message GetAllGemNotificationConfigsResp {
    repeated GemNotification gem_notifications = 1;
}

message CheckHoldReq {
    string hold_id = 1;
    string user_id = 2;
}

message CheckHoldResp {
    Hold hold = 1;
}

message Hold {
    string hold_id = 1;
    string user_id = 2;
    int64 amount = 3;
    google.protobuf.Timestamp expires_at = 4;
    bool is_released = 5;
    bool is_finalized = 6;
    bool has_expired = 7;
}

message CreateHoldReq {
    string hold_id = 1;
    string user_id = 2;
    int64 amount = 3;
    google.protobuf.Timestamp expires_at = 4;
}

message CreateHoldResp {
    Hold hold = 1;
}

message ReleaseHoldReq {
    string hold_id = 1;
    string user_id = 2;
}

message ReleaseHoldResp {
    Hold hold = 1;
}

message FinalizeHoldReq {
    string hold_id = 1;
    string user_id = 2;
    reserved 3; // We are going to deprecate Benefactor and Recipient for a new structure, so don't use the old numbers
    repeated Recipient recipients = 4;
    // Finalize Hold takes a bits_usage_attributions Map which requires you to assign both the
    // Bits that count towards Rewards (leaderboard rank/emotes/etc) as well as the actual Broadcaster payout amounts
    map<string, BitsUsageAttribution> bits_usage_attributions = 5;
    TransactionType transaction_type = 6;
    // `ignore_payout` marks that we should not process the payout in Pachter/Gringotts as well as
    // in the validation step only sum  `reward_attribution` instead of `bits_to_user_amount`
    bool ignore_payout = 7;
}

message Benefactor {
    string id = 1;
    TransactionType transaction_type = 2;
    int64 amount = 3;
}

message Recipient {
    string id = 1;
    TransactionType transaction_type = 2;
    int64 amount = 3;
    int64 reward_amount = 4;
    int64 total_bits_to_broadcaster = 5;
}

// This is intended as a struct with inputs only (vs *WithTotal below which outputs the total # of bits used with broadcaster prior, used in Rewards.)
message BitsUsageAttribution {
    reserved 1;
    // `reward_attribution` marks how many Bits went to a Channel's reward systems
    // (ie: Bits in Extensions might have 100 Bits used, all of which count towards Badges/Emotes/Leaderboards for the Channel they were used in,
    // but some of those Bits might give a Revenue split to other channels/creators.
    int64 reward_attribution = 2;
    // `bits_revenue_attribution` marks how much Bits Revenue the transaction earns for a mapped broadcaster
    int64 bits_revenue_attribution = 3;
}

// This is
message BitsUsageAttributionWithTotal {
    reserved 1;
    // `reward_attribution` marks how many Bits went to a Channel's reward systems
    // (ie: Bits in Extensions might have 100 Bits used, all of which count towards Badges/Emotes/Leaderboards for the Channel they were used in,
    // but some of those Bits might give a Revenue split to other channels/creators.
    int64 reward_attribution = 2;
    // this is the total number of bits used with the broadcaster prior to this transaction
    int64 total_bits_with_broadcaster = 3;
    // `bits_revenue_attribution` marks how much Bits Revenue the transaction earns for a mapped broadcaster
    int64 bits_revenue_attribution = 4;
}

message FinalizeHoldResp {
    Hold hold = 1;
}

enum TransactionType {
    GiveBitsToBroadcaster = 0;
    UseBitsOnExtension = 1;

    AddBits = 2;
    AdminAddBits = 3;
    AdminRemoveBits = 4;

    CreateHold = 5;
    FinalizeHold = 6;
    ReleaseHold = 7;

    UseBitsInExtensionChallenge = 8;
    GiveBitsToBroadcasterChallenge = 9;

    UseBitsOnPoll = 10;
}

message GetTransactionsByIDReq {
    repeated string transaction_ids = 1;
}

message GetTransactionsByIDResp {
    repeated Transaction transactions = 1;
}

enum SearchTransactionType {
    ONSITE = 0; // First party generated transactions i.e. cheering, bits on polls, add bits, admin add bits, etc
    EXTENSION = 1;
}

message SearchTransactionsReq {
    oneof user_filter {
        string user_id = 1;
        string channel_id = 2;
    }
    google.protobuf.Timestamp event_after = 3;
    google.protobuf.Timestamp event_before = 4;
    SearchTransactionType search_type = 5;
    string cursor = 6;
}

message SearchTransactionsResp {
    repeated Transaction transactions = 1;
    string cursor = 2;
}

message Transaction {
    string transaction_id = 1;
    string channel_id = 2;
    string user_id = 3;
    int64 number_of_bits = 4;
    google.protobuf.Timestamp last_updated = 5;

    // transaction fields
    string raw_message = 6;
    string transaction_type = 7;
    map<string, int64> emote_totals = 8;

    // for entitlements only
    string bits_type = 9;
    string platform = 10;

    // for extension transactions and Phanto redemptions
    string product_id = 11;

    // extension transaction fields, available if the transaction is a extension transaction
    string extension_client_id = 12;

    // for holds
    string operand_transaction_id = 13;
    repeated Benefactor benefactors = 14; // TODO: deprecate benefactor for recipients
    repeated Recipient recipients = 15;

    int64 number_of_bits_sponsored = 16;

    google.protobuf.Timestamp time_of_event = 17;

    string admin_id = 18;
    string admin_reason = 19;
    string purchase_price = 20;
    string purchase_currency = 21;
    string tracking_id = 22;
    int64 sku_quantity = 23;

    int64 total_bits_after_transaction = 24;
    int64 total_bits_to_broadcaster = 25;

    string zip_code = 26;
    string city = 27;
    string country_code = 28;
    string ip_address = 29;

    string partner_type = 30;
    string client_id = 31;
    string num_chatters = 32;
    bool is_anonymous = 33;
    map<string, int64> sponsored_amounts = 34;

    string poll_id = 35;
    string poll_choice_id = 36;

    bool inFlight = 37;

    // for entitlement tracking
    string origin_order_id = 38;

    map<string, BitsUsageAttributionWithTotal> bits_usage_attributions = 39;
    bool payout_ignored = 40;
}

message UseBitsOnPollReq {
    string vote_id = 1;
    string user_id = 2;
    string channel_id = 3;
    int32 bits_amount = 4;
    string poll_id = 5;
    string choice_id = 6;
}

message UseBitsOnPollResp {
    int32 balance = 1;
}

message GetAutoRefillSettingsReq {
    string user_id = 1;
}

message GetAutoRefillSettingsResp {
    AutoRefillSettings settings = 1;
}

message SetAutoRefillSettingsReq {
    string user_id = 1;
    AutoRefillSettings settings = 2;
}

message SetAutoRefillSettingsResp {
    // The changed auto refill profile settings
    AutoRefillSettings newSettings = 1;
}

message AutoRefillSettings {
    //optional in the set call
    string id = 1;
    string offer_id = 2;
    int32 threshold = 3;
    bool enabled = 4;
    bool is_email_notification_enabled = 5;
    string	charge_instrument_category = 6;
    string charge_instrument_id = 7;
    //ignored in the set call
    string created_at = 8;
    //ignored in the set call currently, hardcoded as bits_bundle
    string tpl = 9;
    // quantity and sku (bitsProduct.id) will be retrieved from Petozi using the offerID on save. not needed for set.
    int32 quantity = 10;
    string sku = 11;
}

enum CheerType {
    UnknownCheerType = 0;
    Default = 1;
    GlobalFirstParty = 2;
    GlobalThirdParty = 3;
    ChannelCustom = 4;
    Sponsored = 5;
    DisplayOnly = 6;
    Charity = 7;
    Anonymous = 8;
}

message CheerTier {
    int32 min_bits = 1;
    bool show_in_bits_card = 2;
}

message CheerCampaign {
    bool is_enabled = 1;
    string id = 2;
    int64 total_bits = 3;
    int64 used_bits = 4;
    int64 min_bits_to_be_sponsored = 5;
    map<int64, double> sponsored_amount_thresholds = 6;
    int64 user_limit = 7;
    string brand_name = 8;
    string brand_image_url = 9;
    int64 user_used_bits = 10;
}

message Cheer {
    string prefix = 1;
    CheerType type = 2;
    repeated CheerTier tiers = 3;
    CheerCampaign campaign = 4;
}

message DisplayType {
    string type = 1;
    string extension = 2;
}

message CheerGroup {
    string template = 1;
    repeated Cheer cheers = 2;
}

message GetGlobalCheersReq {}

message GetCheersResp {
    repeated string scales = 1;
    repeated DisplayType display_types = 2;
    repeated string backgrounds = 3;
    map<int32, string> cheer_tier_colors = 4;
    repeated CheerType cheer_type_display_order = 5;
    repeated CheerGroup cheer_groups = 6;
}

message GetChannelCheersReq {
    string channel_id = 1;
    string user_id = 2;
}

enum CheermoteTier {
    Unknown = 0;
    Tier1 = 1;
    Tier100 = 2;
    Tier1000 = 3;
    Tier5000 = 4;
    Tier10000 = 5;
}

message EmoteImageAsset {
    string image1x_id = 1;
    string image2x_id = 2;
    string image4x_id = 3;
    EmoteImageAssetType assetType = 4;
}

enum EmoteImageAssetType {
    STATIC = 0;
    ANIMATED = 1;
}

message UpdateCheermoteTierReq {
    // The owner of the cheermotes being updated
    string channel_id = 1;
    // The user taking this action
    string requesting_user_id = 2;
    CheermoteTier tier = 3;
    repeated EmoteImageAsset image_assets = 4;
}

message UpdateCheermoteTierResp {
}

message DeleteCheermoteTierReq {
    string user_id = 1;
    string requesting_user_id = 2;
    CheermoteTier tier = 3;
}

message DeleteCheermoteTierResp {
    google.protobuf.Timestamp deleted_at = 1;
}

message AssignEmoteToBitsTierReq {
    // Channel that owns the bits tier
    string channel_id = 1;
    // User making the request
    string requesting_user_id = 2;
    string emote_id = 3;
    int64 bits_tier_threshold = 4;
}

message AssignEmoteToBitsTierResp {
    string channel_id = 1;
    string emote_id = 2;
    int64 bits_tier_threshold = 3;
}

message GetCheerScreenshotsForChannelReq {
    string channel_id = 1;
}    

message CheerImage {
    string image_id = 1;
    string image_url = 2;
}

message GetCheerScreenshotsForChannelResp {
    repeated CheerImage images = 1;
}

message GetCheerScreenshotFromIdReq {
    string image_id = 1;
}

message GetCheerScreenshotFromIdResp {
    string image_url = 1;
}

message GrantPinataBonusBitsReq {
    string transaction_id = 1;
    string channel_id = 2;
    int64 pinata_bonus = 3;
}
message GrantPinataBonusBitsResp {
}

message GetBitsTierEmoteGroupsReq {
    string channel_id = 1; // Channel whose bits tier emote groups are requested
}

message GetBitsTierEmoteGroupsResp {
    repeated BitsTierEmoteGroup emote_groups = 1;
}

message BitsTierEmoteGroup {
    int64 bits_tier_threshold = 1;
    string emote_group_id = 2;
}

enum MicroCheerType {
    UNKNOWN = 0;
    PINATA = 1;
}

message MicroCheerReq {
    string transaction_id = 1;
    string channel_id = 2;
    string user_id = 3;
    int64 quantity = 4;
    MicroCheerType type = 5;
}

message MicroCheerResp {
}
