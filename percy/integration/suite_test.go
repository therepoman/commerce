// +build integration dynamodb internal

package integration

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type PercyIntegTestSuite struct {
	suite.Suite
}

func TestPercyIntegSuite(t *testing.T) {
	suite.Run(t, new(PercyIntegTestSuite))
}
