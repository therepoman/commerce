// +build integration dynamodb

package integration

import (
	"context"
	"time"

	"code.justin.tv/commerce/percy/internal/dynamodb"
	. "github.com/smartystreets/goconvey/convey"
)

func (s *PercyIntegTestSuite) TestUserAnniversaryTokensDB() {
	cfg := loadConfig()

	Convey("Given a dynamodb implementation of userAnniversaryTokensDB", s.T(), func() {
		db, err := dynamodb.NewUserAnniversaryTokensDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
			Endpoint:    cfg.Endpoints.DynamoDB,
		})
		So(err, ShouldBeNil)

		Convey("returns false when there isn't an token", func() {
			userID := "1"
			years := "1"

			result, err := db.HasChatNotificationToken(context.Background(), userID, years)
			So(err, ShouldBeNil)
			So(result, ShouldBeFalse)
		})

		Convey("returns true after an token is added", func() {
			userID := "2"
			years := "2"

			err := db.AddChatNotificationToken(context.Background(), userID, years)
			So(err, ShouldBeNil)
			time.Sleep(time.Millisecond * 10)

			result, err := db.HasChatNotificationToken(context.Background(), userID, years)
			So(err, ShouldBeNil)
			So(result, ShouldBeTrue)

			db.DeleteChatNotificationToken(context.Background(), userID, years)
		})

		Convey("returns false after an token is deleted", func() {
			userID := "3"
			years := "3"

			db.AddChatNotificationToken(context.Background(), userID, years)
			time.Sleep(time.Millisecond * 10)

			err := db.DeleteChatNotificationToken(context.Background(), userID, years)
			So(err, ShouldBeNil)
			time.Sleep(time.Millisecond * 10)

			result, err := db.HasChatNotificationToken(context.Background(), userID, years)
			So(err, ShouldBeNil)
			So(result, ShouldBeFalse)

		})
	})
}
