// +build integration dynamodb

package integration

import (
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"code.justin.tv/commerce/percy/internal/tests"
	"context"
	"fmt"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func (s *PercyIntegTestSuite) TestBitsPinata() {
	cfg := loadConfig()

	Convey("Given a bits pinata dynamoDB db", s.T(), func() {
		db, err := dynamodb.NewBitsPinataDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
		})
		So(err, ShouldBeNil)

		ctx := context.Background()

		channelID := fmt.Sprintf("test_channel:%s", uuid.New())

		Convey("attempts to get an active bits pinata entry", func() {
			Convey("when there is no entry", func() {
				pinata, err := db.GetActiveBitsPinata(ctx, channelID)
				So(err, ShouldBeNil)
				So(pinata, ShouldBeNil)
			})

			Convey("when there is an entry that is inactive due to bits contributed", func() {
				// Populate db with pinata
				err := db.CreateBitsPinata(ctx, tests.ValidBrokenBitsPinata(channelID))
				So(err, ShouldBeNil)

				// Check there is no active pinata
				pinata, err := db.GetActiveBitsPinata(ctx, channelID)
				So(err, ShouldBeNil)
				So(pinata, ShouldBeNil)
			})

			Convey("when there is an entry that is inactive due to timeout", func() {
				// Populate db with pinata
				err := db.CreateBitsPinata(ctx, tests.ValidExpiredBitsPinata(channelID))
				So(err, ShouldBeNil)

				// Check there is no active pinata
				pinata, err := db.GetActiveBitsPinata(ctx, channelID)
				So(err, ShouldBeNil)
				So(pinata, ShouldBeNil)
			})

			Convey("when there is an active entry and an inactive entry", func() {
				// Populate db with these pinatas
				activePinata := tests.ValidActiveBitsPinata(channelID)
				err := db.CreateBitsPinata(ctx, activePinata)
				So(err, ShouldBeNil)
				err = db.CreateBitsPinata(ctx, tests.ValidExpiredBitsPinata(channelID))
				So(err, ShouldBeNil)

				// Check active pinata is returned
				channelPinata, err := db.GetActiveBitsPinata(ctx, channelID)
				So(err, ShouldBeNil)
				So(channelPinata, ShouldNotBeNil)
				So(*channelPinata, ShouldResemble, activePinata)
			})

			Convey("when there is brand new active entry", func() {
				// Populate db with new pinata
				channelID := fmt.Sprintf("test_channel:%s", uuid.New())
				activePinata := tests.ValidNewBitsPinata(channelID)
				err := db.CreateBitsPinata(ctx, activePinata)
				So(err, ShouldBeNil)

				// Check pinata is returned
				channelPinata, err := db.GetActiveBitsPinata(ctx, channelID)
				So(err, ShouldBeNil)
				So(channelPinata, ShouldNotBeNil)
				So(*channelPinata, ShouldResemble, activePinata)
			})
		})
	})
}

func (s *PercyIntegTestSuite) TestBitsPinata_AddContribution() {
	cfg := loadConfig()

	Convey("Given a bits pinata dynamoDB db", s.T(), func() {
		db, err := dynamodb.NewBitsPinataDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
		})
		So(err, ShouldBeNil)

		ctx := context.Background()

		channelID := fmt.Sprintf("test_channel:%s", uuid.New())

		Convey("attempts to add bits to a bits pinata", func() {
			Convey("when there is no entry", func() {
				pinata, err := db.AddBitsPinataContribution(ctx, channelID, 100)
				So(err, ShouldBeNil)
				So(pinata, ShouldBeNil)
			})

			Convey("when there is an active entry and an inactive entry", func() {
				// Populate db with these pinatas
				activePinata := tests.ValidActiveBitsPinata(channelID)
				err := db.CreateBitsPinata(ctx, activePinata)
				So(err, ShouldBeNil)
				err = db.CreateBitsPinata(ctx, tests.ValidExpiredBitsPinata(channelID))
				So(err, ShouldBeNil)

				// Contribute bits toward active pinata
				updatedPinata, err := db.AddBitsPinataContribution(ctx, channelID, 10)

				// Check that active pinata was updated properly
				So(err, ShouldBeNil)
				So(updatedPinata, ShouldNotBeNil)
				So(updatedPinata.BitsContributed == (activePinata.BitsContributed + 10), ShouldBeTrue)
				So(updatedPinata.LastContribution.After(activePinata.LastContribution), ShouldBeTrue)
			})

			Convey("when there is an active entry and the pinata is popped", func() {
				// Populate db with active pinata
				newPinata := tests.ValidNewBitsPinata(channelID)
				err := db.CreateBitsPinata(ctx, newPinata)
				So(err, ShouldBeNil)

				// Contribute enough bits to pinata to pop it
				updatedPinata, err := db.AddBitsPinataContribution(ctx, channelID, int(newPinata.BitsToBreak) + 100)
				So(err, ShouldBeNil)
				So(updatedPinata.BitsContributed, ShouldBeGreaterThan, updatedPinata.BitsToBreak)

				// Check that pinata is no longer active
				activePinata, err := db.GetActiveBitsPinata(ctx, channelID)
				So(err, ShouldBeNil)
				So(activePinata, ShouldBeNil)
			})
		})
	})
}
