// +build integration dynamodb

package integration

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/gogogadget/pointers"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func (s *PercyIntegTestSuite) TestCelebrationUserSettingsDB() {
	cfg := loadConfig()

	userID := fmt.Sprintf("test_user:%s", uuid.New())

	Convey("given a celebration user settings dynamoDB db", s.T(), func() {
		db, err := dynamodb.NewCelebrationUserSettingsDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
			Endpoint:    cfg.Endpoints.DynamoDB,
		})
		So(err, ShouldBeNil)

		ctx := context.Background()

		Convey("loads nil settings when not in DB", func() {
			settings, err := db.Get(ctx, userID)
			So(err, ShouldBeNil)
			So(settings, ShouldBeNil)

			Convey("updates settings", func() {
				settings, err := db.Update(ctx, userID, percy.CelebrationUserSettingsUpdate{
					IsOptedOut: pointers.BoolP(true),
				})

				So(err, ShouldBeNil)
				So(settings, ShouldNotBeNil)
				So(settings.IsOptedOut, ShouldBeTrue)

				Convey("fetching settings reflects update", func() {
					settings, err := db.Get(ctx, userID)
					So(err, ShouldBeNil)
					So(settings, ShouldNotBeNil)
					So(settings.IsOptedOut, ShouldBeTrue)
				})
			})
		})
	})
}
