// +build integration dynamodb internal

package integration

import (
	"net/http"

	offerstenantrpc "code.justin.tv/revenue/offer-tenant-schema/rpc"

	"code.justin.tv/commerce/percy/config"
	rpc "code.justin.tv/commerce/percy/rpc"
	rpc_celebi "code.justin.tv/commerce/percy/rpc/celebrations"
	"github.com/sirupsen/logrus"
)

func loadConfig() config.Config {
	environment := config.GetEnv()
	if len(environment) == 0 {
		environment = config.Staging
	}

	logrus.Infof("Loading config file for environment: %s", environment)
	cfg, err := config.LoadConfig(config.Environment(environment))
	if err != nil {
		logrus.WithError(err).Panicf("Could not load config file for environment: %s", environment)
	}

	if cfg == nil {
		logrus.WithError(err).Panicf("Could not find a config file for environment: %s", environment)
	}

	return *cfg
}

func NewTwirpClient() rpc.Percy {
	cfg := loadConfig()

	return rpc.NewPercyProtobufClient(cfg.Endpoints.IntegrationTest, http.DefaultClient)
}

func NewCelebiTwirpClient() rpc_celebi.Celebi {
	cfg := loadConfig()

	return rpc_celebi.NewCelebiProtobufClient(cfg.Endpoints.IntegrationTest, http.DefaultClient)
}

func NewCelebiOfferTenant() offerstenantrpc.OffersTenant {
	cfg := loadConfig()

	return offerstenantrpc.NewOffersTenantProtobufClient(cfg.Endpoints.IntegrationTest, http.DefaultClient)
}
