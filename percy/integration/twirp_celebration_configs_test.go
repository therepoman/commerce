// +build integration

package integration

import (
	"context"
	"math/rand"

	"code.justin.tv/commerce/percy/internal/dynamodb"
	rpc_celebi "code.justin.tv/commerce/percy/rpc/celebrations"

	"github.com/golang/protobuf/ptypes/duration"
	"github.com/golang/protobuf/ptypes/wrappers"
	. "github.com/smartystreets/goconvey/convey"
)

func (s *PercyIntegTestSuite) TestCelebrationConfigsAPIs() {
	celebiClient := NewCelebiTwirpClient()
	ctx := context.Background()
	channelID := "116076154" // qa_bits_partner

	// the number of hardcoded default celebrations we have
	expectedDefaultConfigsCount := 2

	Convey("Given a Celebi client", s.T(), func() {
		err := cleanupDynamo(ctx, channelID)
		So(err, ShouldBeNil)

		Convey("can retrieve a channel's celebi config with defaults", func() {
			resp, err := celebiClient.GetCelebrationConfig(ctx, &rpc_celebi.GetCelebrationConfigReq{
				ChannelId: channelID,
				UserId:    channelID,
			})

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.Config, ShouldNotBeNil)
			So(resp.Config.Enabled, ShouldBeTrue)
			So(resp.Config.ChannelId, ShouldEqual, channelID)
			So(len(resp.Config.Celebrations), ShouldEqual, expectedDefaultConfigsCount)
		})

		Convey("after updating the channel's celebration config", func() {
			// do a get request first to backfill default configurations
			_, err := celebiClient.GetCelebrationConfig(ctx, &rpc_celebi.GetCelebrationConfigReq{
				ChannelId: channelID,
				UserId:    channelID,
			})
			So(err, ShouldBeNil)

			req := rpc_celebi.UpdateCelebrationConfigReq{
				ChannelId: channelID,
				UserId:    channelID,
				Enabled:   false,
			}

			resp, err := celebiClient.UpdateCelebrationConfig(ctx, &req)

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.Config, ShouldNotBeNil)

			So(resp.Config.Enabled, ShouldEqual, req.Enabled)
			So(len(resp.Config.Celebrations), ShouldEqual, expectedDefaultConfigsCount)

			Convey("can retrieve the updated celebration config", func() {
				resp, err := celebiClient.GetCelebrationConfig(ctx, &rpc_celebi.GetCelebrationConfigReq{
					ChannelId: channelID,
					UserId:    channelID,
				})

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.Config, ShouldNotBeNil)

				So(resp.Config.Enabled, ShouldEqual, req.Enabled)
				So(len(resp.Config.Celebrations), ShouldEqual, expectedDefaultConfigsCount)

				Convey("can create a celebration", func() {
					createReq := &rpc_celebi.CreateCelebrationReq{
						ChannelId: channelID,
						UserId:    channelID,
						Enabled: []bool{
							true,
							false,
						}[rand.Intn(2)],
						EventType: []rpc_celebi.EventType{
							rpc_celebi.EventType_BITS_CHEER,
							rpc_celebi.EventType_SUBSCRIPTION_GIFT,
						}[rand.Intn(2)],
						Effect: []rpc_celebi.CelebrationEffect{
							rpc_celebi.CelebrationEffect_FIREWORKS,
							rpc_celebi.CelebrationEffect_RAIN,
							rpc_celebi.CelebrationEffect_FLAMETHROWERS,
						}[rand.Intn(3)],
						Area: []rpc_celebi.CelebrationArea{
							rpc_celebi.CelebrationArea_EVERYWHERE,
							rpc_celebi.CelebrationArea_VIDEO_ONLY,
							rpc_celebi.CelebrationArea_VIDEO_AND_PANEL,
						}[rand.Intn(3)],
						EventThreshold: 1 + rand.Int63n(420),
						Intensity:      1 + rand.Int63n(10),
						Duration: &duration.Duration{
							Seconds: 1 + rand.Int63n(10),
						},
					}

					resp, err := celebiClient.CreateCelebration(ctx, createReq)

					So(err, ShouldBeNil)
					So(resp, ShouldNotBeNil)
					So(resp.Celebration, ShouldNotBeNil)

					So(resp.Celebration.Intensity, ShouldEqual, createReq.Intensity)
					So(resp.Celebration.Enabled, ShouldEqual, createReq.Enabled)
					So(resp.Celebration.Area, ShouldEqual, createReq.Area)

					Convey("can retrieve the updated celebration config", func() {
						resp, err := celebiClient.GetCelebrationConfig(ctx, &rpc_celebi.GetCelebrationConfigReq{
							ChannelId: channelID,
							UserId:    channelID,
						})

						So(err, ShouldBeNil)
						So(resp, ShouldNotBeNil)
						So(resp.Config, ShouldNotBeNil)

						So(resp.Config.Enabled, ShouldEqual, req.Enabled)
						So(len(resp.Config.Celebrations), ShouldEqual, expectedDefaultConfigsCount+1)

						celebrationID := resp.Config.Celebrations[0].CelebrationId

						Convey("can update a celebration", func() {
							updateReq := &rpc_celebi.UpdateCelebrationReq{
								ChannelId:     channelID,
								UserId:        channelID,
								CelebrationId: celebrationID,
								Enabled: &wrappers.BoolValue{
									Value: []bool{
										true,
										false,
									}[rand.Intn(2)],
								},
								EventType: []rpc_celebi.EventType{
									rpc_celebi.EventType_BITS_CHEER,
									rpc_celebi.EventType_SUBSCRIPTION_GIFT,
								}[rand.Intn(2)],
								Effect: []rpc_celebi.CelebrationEffect{
									rpc_celebi.CelebrationEffect_FIREWORKS,
									rpc_celebi.CelebrationEffect_RAIN,
									rpc_celebi.CelebrationEffect_FLAMETHROWERS,
								}[rand.Intn(3)],
								Area: []rpc_celebi.CelebrationArea{
									rpc_celebi.CelebrationArea_EVERYWHERE,
									rpc_celebi.CelebrationArea_VIDEO_ONLY,
									rpc_celebi.CelebrationArea_VIDEO_AND_PANEL,
								}[rand.Intn(3)],
								EventThreshold: &wrappers.Int64Value{Value: 1 + rand.Int63n(420)},
								Intensity:      &wrappers.Int64Value{Value: 1 + rand.Int63n(10)},
								Duration: &duration.Duration{
									Seconds: 1 + rand.Int63n(10),
								},
							}
							resp, err := celebiClient.UpdateCelebration(ctx, updateReq)

							So(err, ShouldBeNil)
							So(resp, ShouldNotBeNil)
							So(resp.Celebration, ShouldNotBeNil)

							So(resp.Celebration.Intensity, ShouldEqual, updateReq.Intensity.Value)
							So(resp.Celebration.Enabled, ShouldEqual, updateReq.Enabled.Value)
							So(resp.Celebration.Area, ShouldEqual, updateReq.Area)
							So(resp.Celebration.EventThreshold, ShouldEqual, updateReq.EventThreshold.Value)
							So(resp.Celebration.EventType, ShouldEqual, updateReq.EventType)

							Convey("can delete a celebration", func() {
								resp, err := celebiClient.DeleteCelebration(ctx, &rpc_celebi.DeleteCelebrationReq{
									ChannelId:     channelID,
									UserId:        channelID,
									CelebrationId: celebrationID,
								})

								So(err, ShouldBeNil)
								So(resp, ShouldNotBeNil)

								Convey("can retrieve the updated celebration config", func() {
									resp, err := celebiClient.GetCelebrationConfig(ctx, &rpc_celebi.GetCelebrationConfigReq{
										ChannelId: channelID,
										UserId:    channelID,
									})

									So(err, ShouldBeNil)
									So(resp, ShouldNotBeNil)
									So(resp.Config, ShouldNotBeNil)

									So(resp.Config.Enabled, ShouldEqual, req.Enabled)
									So(len(resp.Config.Celebrations), ShouldEqual, expectedDefaultConfigsCount)
								})
							})
						})
					})
				})
			})
		})
	})
}

func cleanupDynamo(ctx context.Context, channelID string) error {
	cfg := loadConfig()
	db, err := dynamodb.NewCelebrationsConfigDB(dynamodb.DynamoDBConfig{
		Region:      cfg.AWSRegion,
		Environment: cfg.DynamoSuffix,
		Endpoint:    cfg.Endpoints.DynamoDB,
	})
	if err != nil {
		return err
	}

	return db.DeleteCelebrationConfig(ctx, channelID)
}
