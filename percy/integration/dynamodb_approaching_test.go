// +build integration dynamodb

package integration

import (
	"context"
	"fmt"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func (s *PercyIntegTestSuite) TestHypeTrainApproachingDB() {
	cfg := loadConfig()

	channelID := fmt.Sprintf("test_channel:%s", uuid.New())
	timestamp := time.Now()
	otherTimestamp := timestamp.Add(time.Hour)

	Convey("Given a hype train approaching dynamoDB db", s.T(), func() {
		db, err := dynamodb.NewApproachingDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
			Endpoint:    cfg.Endpoints.DynamoDB,
		})
		So(err, ShouldBeNil)

		ctx := context.Background()

		Convey("attempts to get an approaching entry", func() {
			Convey("when there is no entry", func() {
				approaching, err := db.GetApproaching(ctx, channelID)
				So(err, ShouldBeNil)
				So(approaching, ShouldBeNil)
			})

			Convey("updates should take", func() {
				approaching := percy.Approaching{
					ChannelID:               channelID,
					ID:                      uuid.New().String(),
					RateLimited:             true,
					ExpiresAt:               otherTimestamp,
					StartedAt:               timestamp,
					CreatedTime:             time.Now(),
					Participants:            []string{channelID},
					NumberOfTimesShown:      400,
					TimeShownPerHour:        time.Minute,
					CreatorColor:            "green",
					UpdatedAt:               time.Now(),
					Goal:                    20,
					EventsRemainingDuration: map[int]time.Duration{1: 100 * time.Second},
				}

				err := db.SetApproaching(ctx, approaching)
				So(err, ShouldBeNil)

				channelApproaching, err := db.GetApproaching(ctx, channelID)
				So(err, ShouldBeNil)
				So(channelApproaching, ShouldNotBeNil)
				So(channelApproaching.ID, ShouldEqual, approaching.ID)
				So(channelApproaching.ExpiresAt, ShouldEqual, approaching.ExpiresAt)
				So(channelApproaching.Participants[0], ShouldEqual, approaching.Participants[0])
				// The way that the event works is that EventsRemainingDuration is calculated dynamically,
				// so as long as we're within 90% of 100 seconds we should be okay
				So(channelApproaching.EventsRemainingDuration[1], ShouldBeGreaterThan, approaching.EventsRemainingDuration[1]*9/10)
			})
		})
	})
}
