// +build integration

package integration

import (
	"context"
	"math/rand"

	"code.justin.tv/commerce/percy/rpc"
	"github.com/golang/protobuf/ptypes/wrappers"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func (s *PercyIntegTestSuite) TestHypeTrainConfigsAPIs() {
	client := NewTwirpClient()
	ctx := context.Background()
	channelID := "test_channel:" + uuid.New().String()

	Convey("Given a Percy client", s.T(), func() {
		Convey("can retrieve a channel's hype train config", func() {
			resp, err := client.GetHypeTrainConfig(ctx, &rpc.GetHypeTrainConfigReq{
				ChannelId: channelID,
			})

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.ChannelId, ShouldEqual, channelID)
			So(resp.Config, ShouldNotBeNil)
		})

		Convey("after updating the channel's hype train config", func() {
			req := rpc.UpdateHypeTrainConfigReq{
				ChannelId: channelID,
				Enabled: &wrappers.BoolValue{
					Value: []bool{
						true,
						false,
					}[rand.Intn(2)],
				},
				Difficulty: []rpc.Difficulty{
					rpc.Difficulty_EASY,
					rpc.Difficulty_MEDIUM,
					rpc.Difficulty_HARD,
				}[rand.Intn(3)],
				CooldownDurationMinutes: &wrappers.Int64Value{Value: 60 + rand.Int63n(420)},
				NumOfEventsToKickoff:    &wrappers.Int64Value{Value: 3 + rand.Int63n(10)},
				CalloutEmoteId:          &wrappers.StringValue{Value: "123123123"},
				UseCreatorColor:         &wrappers.BoolValue{Value: true},
				UsePersonalizedSettings: &wrappers.BoolValue{Value: true},
			}

			resp, err := client.UpdateHypeTrainConfig(ctx, &req)

			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.Config, ShouldNotBeNil)

			So(resp.Config.Enabled, ShouldEqual, req.Enabled.Value)
			So(resp.Config.Difficulty, ShouldEqual, req.Difficulty)
			So(resp.Config.CooldownDurationMinutes, ShouldEqual, req.CooldownDurationMinutes.Value)
			So(resp.Config.KickoffConfig.NumOfEvents, ShouldEqual, req.NumOfEventsToKickoff.Value)
			So(resp.Config.CalloutEmoteId, ShouldEqual, req.CalloutEmoteId.Value)
			So(resp.Config.UseCreatorColor, ShouldEqual, req.UseCreatorColor.Value)
			So(resp.Config.UsePersonalizedSettings, ShouldEqual, req.UsePersonalizedSettings.Value)

			Convey("can retrieve the updated hype train config", func() {
				resp, err := client.GetHypeTrainConfig(ctx, &rpc.GetHypeTrainConfigReq{
					ChannelId: channelID,
				})

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.Config, ShouldNotBeNil)

				So(resp.Config.Enabled, ShouldEqual, req.Enabled.Value)
				So(resp.Config.Difficulty, ShouldEqual, req.Difficulty)
				So(resp.Config.CooldownDurationMinutes, ShouldEqual, req.CooldownDurationMinutes.Value)
				So(resp.Config.KickoffConfig.NumOfEvents, ShouldEqual, req.NumOfEventsToKickoff.Value)
				So(resp.Config.CalloutEmoteId, ShouldEqual, req.CalloutEmoteId.Value)
				So(resp.Config.UseCreatorColor, ShouldEqual, req.UseCreatorColor.Value)
				So(resp.Config.UsePersonalizedSettings, ShouldEqual, req.UsePersonalizedSettings.Value)
			})
		})
	})
}
