// +build integration dynamodb

package integration

import (
	"context"
	"fmt"
	"sort"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func (s *PercyIntegTestSuite) TestParticipationsDB() {
	cfg := loadConfig()

	userID1 := "test_user:1"
	userID2 := "test_user:2"
	userID3 := "test_user:3"
	userID4 := "test_user:4"

	Convey("Given a participations DB", s.T(), func() {
		channelID := fmt.Sprintf("test_channel:%s", uuid.New())
		db, err := dynamodb.NewParticipationsDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
			Endpoint:    cfg.Endpoints.DynamoDB,
		})
		So(err, ShouldBeNil)

		ctx := context.Background()

		Convey("records channel participation events", func() {
			participation1 := percy.Participation{
				ChannelID: channelID,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				},
				Quantity:  100,
				User:      percy.NewUser(userID1),
				Timestamp: time.Date(2019, time.October, 15, 0, 0, 0, 0, time.UTC),
			}

			participation2 := percy.Participation{
				ChannelID: channelID,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.SubsSource,
					Action: percy.Tier1SubAction,
				},
				Quantity:  1,
				User:      percy.NewUser(userID2),
				Timestamp: time.Date(2019, time.October, 15, 1, 0, 0, 0, time.UTC),
			}

			participation3 := percy.Participation{
				ChannelID: channelID,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.SubsSource,
					Action: percy.Tier1GiftedSubAction,
				},
				Quantity:  10,
				User:      percy.NewUser(userID3),
				Timestamp: time.Date(2019, time.October, 15, 5, 0, 0, 0, time.UTC),
			}

			participation4 := percy.Participation{
				ChannelID: channelID,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				},
				Quantity:  500,
				User:      percy.NewUser(userID4),
				Timestamp: time.Date(2019, time.October, 15, 6, 0, 0, 0, time.UTC),
			}

			Convey("retrieves participation events", func() {
				err := db.RecordChannelParticipation(ctx, participation1)
				So(err, ShouldBeNil)

				err = db.RecordChannelParticipation(ctx, participation2)
				So(err, ShouldBeNil)

				err = db.RecordChannelParticipation(ctx, participation3)
				So(err, ShouldBeNil)

				err = db.RecordChannelParticipation(ctx, participation4)
				So(err, ShouldBeNil)

				Convey("should include all participations within the given start and end time", func() {
					// exclude participation 1 and 4
					participations, err := db.GetChannelParticipations(ctx, channelID, percy.ParticipationQueryParams{
						StartTime: participation2.Timestamp,
						EndTime:   participation3.Timestamp,
					})
					So(err, ShouldBeNil)
					So(participations, ShouldHaveLength, 2)

					sort.SliceStable(participations, func(i, j int) bool {
						return participations[i].Timestamp.Before(participations[j].Timestamp)
					})

					So(participations[0], ShouldResemble, participation2)
					So(participations[1], ShouldResemble, participation3)
				})
			})
		})
	})
}
