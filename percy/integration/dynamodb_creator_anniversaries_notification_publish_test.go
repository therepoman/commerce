// +build integration dynamodb

package integration

import (
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"context"
	. "github.com/smartystreets/goconvey/convey"
	"time"
)

const (
	dartOnsiteNotificationCampaign = "creator-anniversaries-test"
	dartEmailNotificationCampaign  = "creator-anniversaries-test-email"
)

func (s *PercyIntegTestSuite) TestCreatorAnniversariesNotificationDB() {
	cfg := loadConfig()

	Convey("Given a creatorAnniversariesNotificationDB dynamodb", s.T(), func() {
		ctx := context.Background()

		db, err := dynamodb.NewCreatorAnniversariesNotificationDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
			Endpoint:    cfg.Endpoints.DynamoDB,
		})
		So(db, ShouldNotBeNil)
		So(err, ShouldBeNil)

		Convey("should succeed when adding notifications", func() {
			channelID := "1234"
			notificationTraceId := "2"

			ninjaID := "4242"
			ninjaNotificationId := "3"

			err := db.PutCreatorAnniversaryNotification(ctx, channelID, time.Now(), notificationTraceId, dartEmailNotificationCampaign)
			So(err, ShouldBeNil)

			err = db.PutCreatorAnniversaryNotification(ctx, ninjaID, time.Now(), ninjaNotificationId, dartOnsiteNotificationCampaign)
			So(err, ShouldBeNil)
			time.Sleep(time.Millisecond * 15)

			Convey("should succeed when checking for those notifications", func() {
				resp, err := db.ChannelHasSentAnniversaryNotification(ctx, channelID, dartEmailNotificationCampaign)
				So(resp, ShouldBeTrue)
				So(err, ShouldBeNil)

				resp, err = db.ChannelHasSentAnniversaryNotification(ctx, ninjaID, dartOnsiteNotificationCampaign)
				So(resp, ShouldBeTrue)
				So(err, ShouldBeNil)
			})

			Convey("should return false when checking for those notifications with the wrong campaign type", func() {
				resp, err := db.ChannelHasSentAnniversaryNotification(ctx, channelID, dartOnsiteNotificationCampaign)
				So(resp, ShouldBeFalse)
				So(err, ShouldBeNil)

				resp, err = db.ChannelHasSentAnniversaryNotification(ctx, ninjaID, dartEmailNotificationCampaign)
				So(resp, ShouldBeFalse)
				So(err, ShouldBeNil)

				resp, err = db.ChannelHasSentAnniversaryNotification(ctx, ninjaID, "DOES_NOT_EXIST")
				So(resp, ShouldBeFalse)
				So(err, ShouldBeNil)
			})


			Convey("then successfully delete all notifications", func() {
				err = db.DeleteCreatorAnniversaryNotificationByChannelId(ctx, channelID, dartEmailNotificationCampaign)
				So(err, ShouldBeNil)

				err = db.DeleteCreatorAnniversaryNotificationByChannelId(ctx, ninjaID, dartOnsiteNotificationCampaign)
				So(err, ShouldBeNil)
				time.Sleep(time.Millisecond * 15)

				resp, err := db.ChannelHasSentAnniversaryNotification(ctx, channelID, dartEmailNotificationCampaign)
				So(resp, ShouldBeFalse)
				So(err, ShouldBeNil)

				resp, err = db.ChannelHasSentAnniversaryNotification(ctx, ninjaID, dartOnsiteNotificationCampaign)
				So(resp, ShouldBeFalse)
				So(err, ShouldBeNil)
			})
		})
	})
}
