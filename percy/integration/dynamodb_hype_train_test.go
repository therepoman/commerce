// +build integration dynamodb

package integration

import (
	"context"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"code.justin.tv/commerce/percy/internal/tests"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func (s *PercyIntegTestSuite) TestHypeTrainDB() {
	cfg := loadConfig()

	Convey("Given a hype train dynamoDB db", s.T(), func() {
		db, err := dynamodb.NewHypeTrainDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
			Endpoint:    cfg.Endpoints.DynamoDB,
		})
		So(err, ShouldBeNil)

		ctx := context.Background()
		channelID := uuid.New().String()
		validHypeTrain := tests.ValidOngoingHypeTrain()
		validHypeTrain.ChannelID = channelID
		validHypeTrain.Config.ChannelID = channelID

		Convey("returns nil when there isn't a Hype Train for a channel_id & instance", func() {
			hypeTrain, err := db.GetMostRecentHypeTrain(ctx, "some_channel_id")
			So(err, ShouldBeNil)
			So(hypeTrain, ShouldBeNil)
		})

		Convey("returns hype train for a valid channel_id and instance", func() {
			err := db.UpsertHypeTrain(ctx, validHypeTrain)
			So(err, ShouldBeNil)

			hypeTrain, err := db.GetMostRecentHypeTrain(ctx, validHypeTrain.ChannelID)
			So(err, ShouldBeNil)
			So(hypeTrain, ShouldNotBeNil)
			So(hypeTrain.IsValid(), ShouldBeTrue)
			So(*hypeTrain, ShouldResemble, validHypeTrain)
		})
	})
}

func (s *PercyIntegTestSuite) TestHypeTrainDB_UpdateConductor() {
	cfg := loadConfig()

	Convey("Given a hype train dynamoDB db", s.T(), func() {
		db, err := dynamodb.NewHypeTrainDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
		})
		So(err, ShouldBeNil)

		ctx := context.Background()

		hypeTrain := tests.ValidOngoingHypeTrain()
		hypeTrain.ChannelID = uuid.New().String()
		hypeTrain.Config.ChannelID = uuid.New().String()
		hypeTrain.Conductors = make(map[percy.ParticipationSource]percy.Conductor)

		Convey("with a valid hype train with no conductor", func() {
			err := db.UpsertHypeTrain(ctx, hypeTrain)

			So(err, ShouldBeNil)
			So(hypeTrain.Conductors, ShouldBeEmpty)

			Convey("can add a BITS conductor", func() {
				bitsConductor := percy.Conductor{
					User:   percy.NewUser(uuid.New().String()),
					Source: percy.BitsSource,
					Participations: map[percy.ParticipationIdentifier]int{
						{
							Source: percy.BitsSource,
							Action: percy.CheerAction,
						}: 100,
					},
				}

				hypeTrain, err := db.UpdateConductor(ctx, hypeTrain.ChannelID, hypeTrain.ID, bitsConductor)
				So(err, ShouldBeNil)
				So(hypeTrain.Conductors, ShouldHaveLength, 1)
				So(hypeTrain.Conductors[percy.BitsSource], ShouldResemble, bitsConductor)

				Convey("can add a SUBS conductor", func() {
					subsConductor := percy.Conductor{
						User:   percy.NewUser(uuid.New().String()),
						Source: percy.SubsSource,
						Participations: map[percy.ParticipationIdentifier]int{
							{
								Source: percy.SubsSource,
								Action: percy.Tier1GiftedSubAction,
							}: 10,
						},
					}

					hypeTrain, err := db.UpdateConductor(ctx, hypeTrain.ChannelID, hypeTrain.ID, subsConductor)
					So(err, ShouldBeNil)
					So(hypeTrain.Conductors, ShouldHaveLength, 2)
					So(hypeTrain.Conductors[percy.SubsSource], ShouldResemble, subsConductor)
				})

				Convey("can override BITS conductor", func() {
					newBitsConductor := percy.Conductor{
						User:   percy.NewUser(uuid.New().String()),
						Source: percy.BitsSource,
						Participations: map[percy.ParticipationIdentifier]int{
							{
								Source: percy.BitsSource,
								Action: percy.PollAction,
							}: 1000,
						},
					}

					hypeTrain, err := db.UpdateConductor(ctx, hypeTrain.ChannelID, hypeTrain.ID, newBitsConductor)
					So(err, ShouldBeNil)
					So(hypeTrain.Conductors, ShouldHaveLength, 1)
					So(hypeTrain.Conductors[percy.BitsSource], ShouldResemble, newBitsConductor)
				})
			})

			Convey("when hype train ID mismatches", func() {
				differentHypeTrainID := uuid.New().String()
				bitsConductor := percy.Conductor{
					User:   percy.NewUser(uuid.New().String()),
					Source: percy.BitsSource,
					Participations: map[percy.ParticipationIdentifier]int{
						{
							Source: percy.BitsSource,
							Action: percy.CheerAction,
						}: 100,
					},
				}

				Convey("should fail the update", func() {
					_, err := db.UpdateConductor(ctx, hypeTrain.ChannelID, differentHypeTrainID, bitsConductor)
					So(err, ShouldNotBeNil)
				})
			})
		})
	})
}

func (s *PercyIntegTestSuite) TestHypeTrainDB_RemoveConductor() {
	cfg := loadConfig()

	Convey("Given a hype train dynamoDB db", s.T(), func() {
		db, err := dynamodb.NewHypeTrainDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
		})
		So(err, ShouldBeNil)

		ctx := context.Background()

		userID := uuid.New().String()
		hypeTrain := tests.ValidOngoingHypeTrain()
		hypeTrain.ChannelID = uuid.New().String()
		hypeTrain.Config.ChannelID = uuid.New().String()
		hypeTrain.Conductors = map[percy.ParticipationSource]percy.Conductor{
			percy.SubsSource: {
				User:   percy.NewUser(userID),
				Source: percy.SubsSource,
			},
			percy.BitsSource: {
				User:   percy.NewUser(uuid.New().String()),
				Source: percy.BitsSource,
			},
		}

		Convey("with a valid hype train that has a conductor", func() {
			err := db.UpsertHypeTrain(ctx, hypeTrain)
			So(err, ShouldBeNil)

			Convey("can remove the conductor based on the source", func() {
				hypeTrain, err := db.RemoveConductor(ctx, hypeTrain.ChannelID, hypeTrain.ID, userID, percy.SubsSource)
				So(err, ShouldBeNil)
				So(hypeTrain.Conductors, ShouldHaveLength, 1)
				_, ok := hypeTrain.Conductors[percy.SubsSource]
				So(ok, ShouldBeFalse)
			})

			Convey("when hype train ID mismatches", func() {
				differentHypeTrainID := uuid.New().String()

				Convey("should fail the update", func() {
					_, err := db.RemoveConductor(ctx, hypeTrain.ChannelID, differentHypeTrainID, userID, percy.BitsSource)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("when user ID mismatches", func() {
				differentUserID := uuid.New().String()

				Convey("should fail the update", func() {
					_, err := db.RemoveConductor(ctx, hypeTrain.ChannelID, hypeTrain.ID, differentUserID, percy.BitsSource)
					So(err, ShouldNotBeNil)
				})
			})
		})
	})
}

func (s *PercyIntegTestSuite) TestHypeTrainDB_CreateHypeTrain() {
	cfg := loadConfig()

	Convey("Given a hype train db", s.T(), func() {
		db, err := dynamodb.NewHypeTrainDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
		})
		So(err, ShouldBeNil)

		ctx := context.Background()

		Convey("create a new ongoing hype train", func() {
			config := tests.ValidHypeTrainConfig()
			config.ChannelID = uuid.New().String()
			hypeTrain := percy.NewHypeTrain(config, uuid.New().String())

			err := db.CreateHypeTrain(ctx, hypeTrain)
			So(err, ShouldBeNil)

			ongoingHypeTrain, err := db.GetMostRecentHypeTrain(ctx, config.ChannelID)
			So(err, ShouldBeNil)
			So(ongoingHypeTrain, ShouldNotBeNil)
			So(ongoingHypeTrain.ID, ShouldEqual, hypeTrain.ID)

			Convey("returns an error if there is already an ongoing hype train", func() {
				newHypeTrain := percy.NewHypeTrain(config, uuid.New().String())
				err := db.CreateHypeTrain(ctx, newHypeTrain)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("given a expired hype train", func() {
			config := tests.ValidHypeTrainConfig()
			config.ChannelID = uuid.New().String()
			hypeTrain := percy.NewHypeTrain(config, uuid.New().String())
			hypeTrain.StartedAt = time.Now().UTC().Add(-60 * time.Minute)
			hypeTrain.ExpiresAt = time.Now().UTC().Add(-55 * time.Minute)
			hypeTrain.UpdatedAt = time.Now().UTC().Add(-55 * time.Minute)

			err := db.CreateHypeTrain(ctx, hypeTrain)
			So(err, ShouldBeNil)

			Convey("creates an new ongoing hype train", func() {
				newHypeTrain := percy.NewHypeTrain(config, uuid.New().String())
				err = db.CreateHypeTrain(ctx, newHypeTrain)
				So(err, ShouldBeNil)
			})
		})
	})
}

func (s *PercyIntegTestSuite) TestHypeTrainDB_AddParticipation() {
	cfg := loadConfig()

	Convey("Give a hype train db", s.T(), func() {
		db, err := dynamodb.NewHypeTrainDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
		})
		So(err, ShouldBeNil)

		ctx := context.Background()

		Convey("with an ongoing hype train record", func() {
			channelID := uuid.New().String()
			validHypeTrain := tests.ValidOngoingHypeTrain()
			validHypeTrain.ID = uuid.New().String()
			validHypeTrain.ChannelID = channelID
			validHypeTrain.Config.ChannelID = channelID
			validHypeTrain.Participations = make(percy.ParticipationTotals)

			err := db.UpsertHypeTrain(ctx, validHypeTrain)
			So(err, ShouldBeNil)

			Convey("can add hype train participation", func() {
				cheer1 := percy.Participation{
					ChannelID: validHypeTrain.ChannelID,
					ParticipationIdentifier: percy.ParticipationIdentifier{
						Source: percy.BitsSource,
						Action: percy.CheerAction,
					},
					Quantity:  100,
					Timestamp: time.Now(),
					User:      percy.NewUser("123"),
				}

				updatedHypeTrain, err := db.AddParticipation(ctx, validHypeTrain.ChannelID, validHypeTrain.ID, cheer1)
				So(err, ShouldBeNil)
				So(updatedHypeTrain.Participations[percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				}], ShouldEqual, cheer1.Quantity)

				Convey("can add more participation of the same identifier", func() {
					cheer2 := percy.Participation{
						ChannelID: validHypeTrain.ChannelID,
						ParticipationIdentifier: percy.ParticipationIdentifier{
							Source: percy.BitsSource,
							Action: percy.CheerAction,
						},
						Quantity:  500,
						Timestamp: time.Now(),
						User:      percy.NewUser("123"),
					}

					updatedHypeTrain, err := db.AddParticipation(ctx, validHypeTrain.ChannelID, validHypeTrain.ID, cheer2)
					So(err, ShouldBeNil)
					So(updatedHypeTrain.Participations[percy.ParticipationIdentifier{
						Source: percy.BitsSource,
						Action: percy.CheerAction,
					}], ShouldEqual, cheer1.Quantity+cheer2.Quantity)
				})
			})

			Convey("returns an error when hype train ID mismatches", func() {
				cheer := percy.Participation{
					ChannelID: validHypeTrain.ChannelID,
					ParticipationIdentifier: percy.ParticipationIdentifier{
						Source: percy.BitsSource,
						Action: percy.CheerAction,
					},
					Quantity:  100,
					Timestamp: time.Now(),
					User:      percy.NewUser("123"),
				}

				_, err := db.AddParticipation(ctx, validHypeTrain.ChannelID, uuid.New().String(), cheer)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("with an ended hype train", func() {
			channelID := uuid.New().String()
			validHypeTrain := tests.ValidOngoingHypeTrain()
			validHypeTrain.ID = uuid.New().String()
			validHypeTrain.ChannelID = channelID
			validHypeTrain.Config.ChannelID = channelID
			validHypeTrain.Participations = make(percy.ParticipationTotals)
			validHypeTrain.EndedAt = pointers.TimeP(time.Now())

			endingReason := percy.EndingReasonExpired
			validHypeTrain.EndingReason = &endingReason

			err := db.UpsertHypeTrain(ctx, validHypeTrain)
			So(err, ShouldBeNil)

			Convey("returns an error", func() {
				cheer1 := percy.Participation{
					ChannelID: validHypeTrain.ChannelID,
					ParticipationIdentifier: percy.ParticipationIdentifier{
						Source: percy.BitsSource,
						Action: percy.CheerAction,
					},
					Quantity:  100,
					Timestamp: time.Now(),
					User:      percy.NewUser("123"),
				}

				_, err := db.AddParticipation(ctx, validHypeTrain.ChannelID, validHypeTrain.ID, cheer1)
				So(err, ShouldNotBeNil)
			})

		})
	})
}

func (s *PercyIntegTestSuite) TestHypeTrainDB_EndHypeTrain() {
	cfg := loadConfig()

	Convey("Given a hype train DB", s.T(), func() {
		db, err := dynamodb.NewHypeTrainDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
		})
		So(err, ShouldBeNil)

		ctx := context.Background()

		Convey("with an ongoing hype train record", func() {
			channelID := uuid.New().String()
			validHypeTrain := tests.ValidOngoingHypeTrain()
			validHypeTrain.ID = uuid.New().String()
			validHypeTrain.ChannelID = channelID
			validHypeTrain.Config.ChannelID = channelID
			validHypeTrain.Participations = make(percy.ParticipationTotals)

			err := db.UpsertHypeTrain(ctx, validHypeTrain)
			So(err, ShouldBeNil)

			Convey("ends the hype train with a reason", func() {
				beforeEnd := time.Now()

				endedTrain, err := db.EndHypeTrain(ctx, channelID, validHypeTrain.ID, percy.EndingReasonExpired)

				So(err, ShouldBeNil)
				So(endedTrain, ShouldNotBeNil)
				So(endedTrain.IsActive(), ShouldBeFalse)
				So(endedTrain.EndedAt, ShouldNotBeNil)
				So(*endedTrain.EndedAt, ShouldHappenOnOrBetween, beforeEnd, time.Now())
				So(*endedTrain.EndingReason, ShouldEqual, percy.EndingReasonExpired)
			})

			Convey("returns an error when hype train ID mismatches", func() {
				_, err := db.EndHypeTrain(ctx, channelID, uuid.New().String(), percy.EndingReasonExpired)
				So(err, ShouldNotBeNil)
			})
		})

		Convey("with an already-ended hype train", func() {
			channelID := uuid.New().String()
			validHypeTrain := tests.ValidOngoingHypeTrain()
			validHypeTrain.ID = uuid.New().String()
			validHypeTrain.ChannelID = channelID
			validHypeTrain.Config.ChannelID = channelID
			validHypeTrain.Participations = make(percy.ParticipationTotals)
			validHypeTrain.EndedAt = pointers.TimeP(time.Now())

			endingReason := percy.EndingReasonExpired
			validHypeTrain.EndingReason = &endingReason

			err := db.UpsertHypeTrain(ctx, validHypeTrain)
			So(err, ShouldBeNil)

			Convey("returns an error", func() {
				_, err := db.EndHypeTrain(ctx, channelID, validHypeTrain.ID, percy.EndingReasonExpired)
				So(err, ShouldNotBeNil)
			})
		})
	})
}

func (s *PercyIntegTestSuite) TestHypeTrainDB_ExtendHypeTrain() {
	cfg := loadConfig()

	Convey("Given a hype train DB", s.T(), func() {
		db, err := dynamodb.NewHypeTrainDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
		})
		So(err, ShouldBeNil)

		ctx := context.Background()

		Convey("with an ongoing hype train record", func() {
			channelID := uuid.New().String()
			validHypeTrain := tests.ValidOngoingHypeTrain()
			validHypeTrain.ID = uuid.New().String()
			validHypeTrain.ChannelID = channelID
			validHypeTrain.Config.ChannelID = channelID
			validHypeTrain.Participations = make(percy.ParticipationTotals)
			validHypeTrain.ExpiresAt = time.Now().Add(1 * time.Minute)

			err := db.UpsertHypeTrain(ctx, validHypeTrain)
			So(err, ShouldBeNil)

			Convey("extends the hype train's remaining time", func() {
				expiresAt := time.Now().Add(5 * time.Minute)
				extendedTrain, err := db.ExtendHypeTrain(ctx, channelID, validHypeTrain.ID, expiresAt)

				So(err, ShouldBeNil)
				So(extendedTrain, ShouldNotBeNil)
				So(extendedTrain.IsActive(), ShouldBeTrue)
				So(extendedTrain.ExpiresAt, ShouldEqual, expiresAt)
			})

			Convey("returns an error when hype train ID mismatches", func() {
				_, err := db.ExtendHypeTrain(ctx, channelID, uuid.New().String(), time.Now().Add(5*time.Minute))
				So(err, ShouldNotBeNil)
			})

			Convey("returns an error when expiration time is in the past", func() {
				_, err := db.ExtendHypeTrain(ctx, channelID, uuid.New().String(), time.Now().Add(-5*time.Minute))
				So(err, ShouldNotBeNil)
			})
		})

		Convey("with an already-ended hype train", func() {
			channelID := uuid.New().String()
			validHypeTrain := tests.ValidOngoingHypeTrain()
			validHypeTrain.ID = uuid.New().String()
			validHypeTrain.ChannelID = channelID
			validHypeTrain.Config.ChannelID = channelID
			validHypeTrain.Participations = make(percy.ParticipationTotals)
			validHypeTrain.EndedAt = pointers.TimeP(time.Now())

			endingReason := percy.EndingReasonExpired
			validHypeTrain.EndingReason = &endingReason

			err := db.UpsertHypeTrain(ctx, validHypeTrain)
			So(err, ShouldBeNil)

			Convey("returns an error", func() {
				_, err := db.ExtendHypeTrain(ctx, channelID, validHypeTrain.ID, time.Now().Add(5*time.Minute))
				So(err, ShouldNotBeNil)
			})
		})
	})
}
