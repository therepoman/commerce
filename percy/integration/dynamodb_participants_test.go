// +build integration dynamodb

package integration

import (
	"context"
	"fmt"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func (s *PercyIntegTestSuite) TestParticipantsDB() {
	cfg := loadConfig()
	userID1 := "test_user:1"
	userID2 := "test_user:2"
	userID3 := "test_user:3"
	userID4 := "test_user:4"

	Convey("Given a participations DB", s.T(), func() {
		channelID := fmt.Sprintf("test_channel:%s", uuid.New())
		hypeTrainID := fmt.Sprintf("test_hype_train:%s", uuid.New())

		db, err := dynamodb.NewParticipantsDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
			Endpoint:    cfg.Endpoints.DynamoDB,
		})
		So(err, ShouldBeNil)

		ctx := context.Background()

		Convey("records participants", func() {
			input1 := percy.Participant{
				ChannelID:   channelID,
				HypeTrainID: hypeTrainID,
				User:        percy.NewUser(userID1),
				ParticipationTotals: map[percy.ParticipationIdentifier]int{
					{
						Source: percy.BitsSource,
						Action: percy.CheerAction,
					}: 100,
					{
						Source: percy.SubsSource,
						Action: percy.Tier1SubAction,
					}: 1,
				},
				Rewards: []percy.Reward{
					{
						ID:   "123",
						Type: percy.EmoteReward,
					},
				},
			}

			input2 := percy.Participant{
				ChannelID:   channelID,
				HypeTrainID: hypeTrainID,
				User:        percy.NewUser(userID2),
				ParticipationTotals: map[percy.ParticipationIdentifier]int{
					{
						Source: percy.SubsSource,
						Action: percy.Tier1GiftedSubAction,
					}: 10,
				},
				Rewards: []percy.Reward{},
			}

			err := db.PutHypeTrainParticipants(ctx, input1, input2)
			So(err, ShouldBeNil)

			// get read a specific hype train participant
			participant1, err := db.GetHypeTrainParticipant(ctx, hypeTrainID, userID1)
			So(err, ShouldBeNil)
			So(participant1, ShouldNotBeNil)
			So(*participant1, ShouldResemble, input1)

			// get read all hype train participants
			participants, err := db.GetHypeTrainParticipants(ctx, hypeTrainID)
			So(err, ShouldBeNil)
			So(participants, ShouldHaveLength, 2)
		})

		Convey("records a large list of participants", func() {
			participants := make([]percy.Participant, 0, 30)
			for i := 0; i < 30; i++ {
				p := percy.Participant{
					ChannelID:   channelID,
					HypeTrainID: hypeTrainID,
					User:        percy.NewUser(fmt.Sprintf("test_user:%d", i)),
				}
				p.AddParticipation(percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				}, 100)
				participants = append(participants, p)
			}

			err := db.PutHypeTrainParticipants(ctx, participants...)
			So(err, ShouldBeNil)

			savedParticipants, err := db.GetHypeTrainParticipants(ctx, hypeTrainID)
			So(err, ShouldBeNil)
			So(savedParticipants, ShouldHaveLength, 30)
		})

		Convey("records new participation event as a new participant", func() {
			participation := percy.Participation{
				ChannelID: channelID,
				User:      percy.NewUser(userID3),
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				},
				Quantity:  100,
				Timestamp: time.Now().UTC(),
			}

			// record a new participation event, from a new user
			participant, err := db.RecordHypeTrainParticipation(ctx, hypeTrainID, participation)
			So(err, ShouldBeNil)
			So(participant.ChannelID, ShouldEqual, participation.ChannelID)
			So(participant.HypeTrainID, ShouldEqual, hypeTrainID)
			So(participant.User.ID(), ShouldEqual, participation.User.ID())
			So(participant.ParticipationQuantity(participation.ParticipationIdentifier), ShouldEqual, participation.Quantity)

			// after recording the new event, can retrieve the participant record
			savedParticipant, err := db.GetHypeTrainParticipant(ctx, hypeTrainID, userID3)
			So(err, ShouldBeNil)
			So(savedParticipant, ShouldNotBeNil)
			So(*savedParticipant, ShouldResemble, participant)
		})

		Convey("records additional participations for an existing participant", func() {
			participant := percy.Participant{
				ChannelID:   channelID,
				HypeTrainID: hypeTrainID,
				User:        percy.NewUser(userID4),
				ParticipationTotals: map[percy.ParticipationIdentifier]int{
					{
						Source: percy.BitsSource,
						Action: percy.CheerAction,
					}: 100,
					{
						Source: percy.SubsSource,
						Action: percy.Tier1SubAction,
					}: 1,
				},
			}

			err := db.PutHypeTrainParticipants(ctx, participant)
			So(err, ShouldBeNil)

			participation := percy.Participation{
				ChannelID: channelID,
				User:      percy.NewUser(userID4),
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				},
				Quantity:  50,
				Timestamp: time.Now(),
			}

			expected := percy.Participant{
				ChannelID:   channelID,
				HypeTrainID: hypeTrainID,
				User:        percy.NewUser(userID4),
				ParticipationTotals: map[percy.ParticipationIdentifier]int{
					{
						Source: percy.BitsSource,
						Action: percy.CheerAction,
					}: 150,
					{
						Source: percy.SubsSource,
						Action: percy.Tier1SubAction,
					}: 1,
				},
				Rewards: []percy.Reward{},
			}

			updatedParticipant, err := db.RecordHypeTrainParticipation(ctx, hypeTrainID, participation)
			So(err, ShouldBeNil)
			So(updatedParticipant, ShouldResemble, expected)
		})
	})
}
