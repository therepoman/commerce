// +build integration dynamodb

package integration

import (
	"context"
	"errors"
	"fmt"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func (s *PercyIntegTestSuite) TestIdempotencyDB() {
	cfg := loadConfig()

	Convey("given a idempotency dynamoDB db", s.T(), func() {
		db, err := dynamodb.NewIdempotencyDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
			Endpoint:    cfg.Endpoints.DynamoDB,
		})
		So(err, ShouldBeNil)

		ctx := context.Background()

		Convey("loads nil output when not in DB", func() {
			transactionID := fmt.Sprintf("test_tx:%s", uuid.New())
			task, err := db.Get(ctx, percy.IdempotencyNamespaceCelebrationPurchaseFulfillment, transactionID, string(percy.CelebrationPurchaseFulfillmentEmitPubsub))
			So(err, ShouldBeNil)
			So(task, ShouldBeNil)

			Convey("updates idempotent task", func() {
				task, err := db.Update(ctx, percy.IdempotencyNamespaceCelebrationPurchaseFulfillment, transactionID, string(percy.CelebrationPurchaseFulfillmentEmitPubsub), percy.IdempotentTaskUpdate{
					State: percy.IdempotencyStateProcessing,
				})

				So(err, ShouldBeNil)
				So(task, ShouldNotBeNil)
				So(task.State, ShouldEqual, percy.IdempotencyStateProcessing)

				Convey("fetching the task reflects update", func() {
					task, err := db.Get(ctx, percy.IdempotencyNamespaceCelebrationPurchaseFulfillment, transactionID, string(percy.CelebrationPurchaseFulfillmentEmitPubsub))
					So(err, ShouldBeNil)
					So(task, ShouldNotBeNil)
					So(task.State, ShouldEqual, percy.IdempotencyStateProcessing)
				})
			})
		})

		Convey("handles error states", func() {
			task, err := db.Update(ctx, percy.IdempotencyNamespaceCelebrationPurchaseFulfillment, fmt.Sprintf("test_tx:%s", uuid.New()), string(percy.CelebrationPurchaseFulfillmentEmitPubsub), percy.IdempotentTaskUpdate{
				State: percy.IdempotencyStateError,
				Error: errors.New("ERROR"),
			})

			So(err, ShouldBeNil)
			So(task, ShouldNotBeNil)
			So(task.State, ShouldEqual, percy.IdempotencyStateError)
		})
	})
}
