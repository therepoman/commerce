// +build integration dynamodb

package integration

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/gogogadget/pointers"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func (s *PercyIntegTestSuite) TestCelebrationChannelPurchasableConfigDB() {
	cfg := loadConfig()

	userID := fmt.Sprintf("test_user:%s", uuid.New())

	Convey("given a celebration channel purchasable config dynamoDB db", s.T(), func() {
		db, err := dynamodb.NewCelebrationChannelPurchasableConfigDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
			Endpoint:    cfg.Endpoints.DynamoDB,
		})
		So(err, ShouldBeNil)

		ctx := context.Background()

		Convey("loads default config when no custom settings are in DB", func() {
			config, err := db.GetConfig(ctx, userID)
			So(err, ShouldBeNil)
			So(config, ShouldNotBeNil)
			So(config.PurchaseConfig[percy.CelebrationSizeSmall].IsDisabled, ShouldBeFalse)

			Convey("updates config", func() {
				config, err := db.UpdateConfig(ctx, userID, percy.CelebrationChannelPurchasableConfigUpdate{
					PurchaseConfig: map[percy.CelebrationSize]percy.CelebrationPurchaseConfigUpdate{
						percy.CelebrationSizeSmall: {
							IsDisabled: pointers.BoolP(true),
						},
					},
				})

				So(err, ShouldBeNil)
				So(config, ShouldNotBeNil)
				So(config.PurchaseConfig[percy.CelebrationSizeSmall].IsDisabled, ShouldBeTrue)

				Convey("fetching config reflects update", func() {
					config, err := db.GetConfig(ctx, userID)
					So(err, ShouldBeNil)
					So(config, ShouldNotBeNil)
					So(config.PurchaseConfig[percy.CelebrationSizeSmall].IsDisabled, ShouldBeTrue)
				})
			})
		})
	})
}
