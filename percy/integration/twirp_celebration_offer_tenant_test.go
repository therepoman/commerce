// +build integration

package integration

import (
	"context"
	"fmt"

	"code.justin.tv/amzn/CorleoneTwirp"
	offerstenantrpc "code.justin.tv/revenue/offer-tenant-schema/rpc"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

const (
	originIDFormat = "integration-%s"
	testChannelID  = "116076154" // qa_bits_partner
)

func (s *PercyIntegTestSuite) TestCelebrationOfferTenant_HandleFulfillment() {
	tenant := NewCelebiOfferTenant()

	Convey("Given a Celebrations OfferTenant", s.T(), func() {
		Convey("when request is nil, we should error", func() {
			_, err := tenant.HandleFulfillment(context.Background(), nil)
			So(err, ShouldNotBeNil)
		})

		Convey("when ownerID is blank, we should error", func() {
			id := uuid.New().String()

			_, err := tenant.HandleFulfillment(context.Background(), &offerstenantrpc.FulfillmentRequest{
				OwnerId:  "",
				OriginId: fmt.Sprintf(originIDFormat, id),
				Offer: &CorleoneTwirp.Offer{
					Id:       "some-id",
					TplrData: &CorleoneTwirp.TPLR{},
					TagBindings: map[string]string{
						"channel_id": testChannelID,
					},
				},
			})
			So(err, ShouldNotBeNil)
		})

		Convey("when originID is blank, we should error", func() {
			_, err := tenant.HandleFulfillment(context.Background(), &offerstenantrpc.FulfillmentRequest{
				OwnerId:  "someOwner",
				OriginId: "",
				Offer: &CorleoneTwirp.Offer{
					Id:       "some-id",
					TplrData: &CorleoneTwirp.TPLR{},
					TagBindings: map[string]string{
						"channel_id": testChannelID,
					},
				},
			})
			So(err, ShouldNotBeNil)
		})

		Convey("when offer is nil, we should error", func() {
			id := uuid.New().String()

			_, err := tenant.HandleFulfillment(context.Background(), &offerstenantrpc.FulfillmentRequest{
				OwnerId:  "someOwner",
				OriginId: fmt.Sprintf(originIDFormat, id),
				Offer:    nil,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("when tplr data is nil, we should error", func() {
			id := uuid.New().String()

			_, err := tenant.HandleFulfillment(context.Background(), &offerstenantrpc.FulfillmentRequest{
				OwnerId:  "someOwner",
				OriginId: fmt.Sprintf(originIDFormat, id),
				Offer: &CorleoneTwirp.Offer{
					Id:       "some-id",
					TplrData: nil,
					TagBindings: map[string]string{
						"channel_id": testChannelID,
					},
				},
			})
			So(err, ShouldNotBeNil)
		})

		Convey("when channel_id is empty, we should error", func() {
			id := uuid.New().String()

			_, err := tenant.HandleFulfillment(context.Background(), &offerstenantrpc.FulfillmentRequest{
				OwnerId:  "someOwner",
				OriginId: fmt.Sprintf(originIDFormat, id),
				Offer: &CorleoneTwirp.Offer{
					Id:       "some-id",
					TplrData: &CorleoneTwirp.TPLR{},
					TagBindings: map[string]string{
						"channel_id": "",
					},
				},
			})
			So(err, ShouldNotBeNil)
		})

		Convey("when the request is valid, we should fulfill the purchase, and return a FulfillmentResponse", func() {
			id := uuid.New().String()
			originID := fmt.Sprintf(originIDFormat, id)

			resp, err := tenant.HandleFulfillment(context.Background(), &offerstenantrpc.FulfillmentRequest{
				OwnerId:  "someOwner",
				OriginId: originID,
				Offer: &CorleoneTwirp.Offer{
					Id:       "some-id",
					TplrData: &CorleoneTwirp.TPLR{},
					TagBindings: map[string]string{
						"channel_id": testChannelID,
					},
				},
			})
			So(err, ShouldBeNil)
			So(resp, ShouldNotBeNil)
			So(resp.FulfillmentId, ShouldEqual, originID)
		})
	})
}

func (s *PercyIntegTestSuite) TestCelebrationOfferTenant_HandleRevocation() {
	tenant := NewCelebiOfferTenant()

	Convey("Given a Celebrations OfferTenant", s.T(), func() {
		Convey("HandleRevocation succeeds as a no-op endpoint", func() {
			_, err := tenant.HandleRevocation(context.Background(), &offerstenantrpc.RevocationRequest{
				OwnerId: "12345",
			})
			So(err, ShouldBeNil)
		})
	})
}

func (s *PercyIntegTestSuite) TestCelebrationOfferTenant_IsEligible() {
	tenant := NewCelebiOfferTenant()

	Convey("Given a Celebrations OfferTenant", s.T(), func() {
		Convey("when request is nil, we should error", func() {
			_, err := tenant.IsEligible(context.Background(), nil)
			So(err, ShouldNotBeNil)
		})

		Convey("when ownerID is blank, we should error", func() {
			_, err := tenant.IsEligible(context.Background(), &offerstenantrpc.IsEligibleRequest{
				OwnerId: "",
				Offer: []*CorleoneTwirp.Offer{
					{
						Id:       "some-id",
						TplrData: &CorleoneTwirp.TPLR{},
						TagBindings: map[string]string{
							"channel_id": testChannelID,
						},
					},
				},
			})
			So(err, ShouldNotBeNil)
		})

		Convey("when offer is nil, we should error", func() {
			_, err := tenant.IsEligible(context.Background(), &offerstenantrpc.IsEligibleRequest{
				OwnerId: "someOwner",
				Offer:   nil,
			})
			So(err, ShouldNotBeNil)
		})

		Convey("when offer has length of 0, we should error", func() {
			_, err := tenant.IsEligible(context.Background(), &offerstenantrpc.IsEligibleRequest{
				OwnerId: "someOwner",
				Offer:   []*CorleoneTwirp.Offer{},
			})
			So(err, ShouldNotBeNil)
		})

		Convey("when tplr data is nil, we should error", func() {
			_, err := tenant.IsEligible(context.Background(), &offerstenantrpc.IsEligibleRequest{
				OwnerId: "someOwner",
				Offer: []*CorleoneTwirp.Offer{
					{
						Id:       "some-id",
						TplrData: nil,
						TagBindings: map[string]string{
							"channel_id": testChannelID,
						},
					},
				},
			})
			So(err, ShouldNotBeNil)
		})

		Convey("when tag bindings is nil, we should error", func() {
			_, err := tenant.IsEligible(context.Background(), &offerstenantrpc.IsEligibleRequest{
				OwnerId: "someOwner",
				Offer: []*CorleoneTwirp.Offer{
					{
						Id:          "some-id",
						TplrData:    &CorleoneTwirp.TPLR{},
						TagBindings: nil,
					},
				},
			})
			So(err, ShouldNotBeNil)
		})

		Convey("when channel_id is blank, we should error", func() {
			_, err := tenant.IsEligible(context.Background(), &offerstenantrpc.IsEligibleRequest{
				OwnerId: "someOwner",
				Offer: []*CorleoneTwirp.Offer{
					{
						Id:       "some-id",
						TplrData: &CorleoneTwirp.TPLR{},
						TagBindings: map[string]string{
							"channel_id": "",
						},
					},
				},
			})
			So(err, ShouldNotBeNil)
		})

		// TODO: We need real users/purchases to test the happy path. We can create the requisite
		// Percy config at runtime, but we will likely need test fixtures for the users/channels
	})
}
