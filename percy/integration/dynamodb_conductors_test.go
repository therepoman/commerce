// +build integration dynamodb

package integration

import (
	"context"
	"fmt"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"code.justin.tv/commerce/percy/internal/tests"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func (s *PercyIntegTestSuite) TestConductorsDB() {
	cfg := loadConfig()

	channelID := fmt.Sprintf("test_channel:%s", uuid.New())
	validConductors := tests.ValidConductors()
	conductors := make([]percy.Conductor, 0)
	for _, c := range validConductors {
		conductors = append(conductors, c)
	}

	Convey("given a conductors dynamoDB db", s.T(), func() {
		db, err := dynamodb.NewConductorsDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
			Endpoint:    cfg.Endpoints.DynamoDB,
		})
		So(err, ShouldBeNil)

		ctx := context.Background()

		Convey("loads nil when there's no conductors", func() {
			actual, err := db.GetConductors(ctx, channelID)

			So(err, ShouldBeNil)
			So(actual, ShouldBeNil)

			Convey("sets conductors appropriately", func() {
				err := db.SetConductors(ctx, channelID, conductors)

				So(err, ShouldBeNil)

				Convey("new conductors are returned", func() {
					// pause briefly to avoid reading from a stale replica
					time.Sleep(100 * time.Millisecond)

					actual, err := db.GetConductors(ctx, channelID)

					So(err, ShouldBeNil)
					So(actual, ShouldNotBeNil)
					So(actual, ShouldHaveLength, 2)
				})
			})
		})
	})
}
