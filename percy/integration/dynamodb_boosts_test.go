// +build integration dynamodb

package integration

import (
	"context"
	"time"

	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"code.justin.tv/commerce/percy/internal/tests"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func (s *PercyIntegTestSuite) TestBoostsDB() {
	cfg := loadConfig()

	Convey("Given a dynamodb implementation of boostsDB", s.T(), func() {
		db, err := dynamodb.NewBoostsDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
			Endpoint:    cfg.Endpoints.DynamoDBSink,
		})
		So(err, ShouldBeNil)

		validOpportunity := tests.ValidBoostOpportunity()
		// user a different channel ID each time
		validOpportunity.ChannelID = uuid.New().String()

		contribution := tests.ValidBoostOpportunityContribution()
		contribution.ChannelID = validOpportunity.ChannelID

		Convey("returns nil when there isn't an active boost opportunity", func() {
			opportunity, err := db.GetLatestBoostOpportunity(context.Background(), validOpportunity.ChannelID, percy.BoostsDBReadOptions{})
			So(err, ShouldBeNil)
			So(opportunity, ShouldBeNil)
		})

		Convey("after an opportunity is created", func() {
			err := db.CreateBoostOpportunity(context.Background(), validOpportunity)
			So(err, ShouldBeNil)

			Convey("returns the newly created opportunity", func() {
				opportunity, err := db.GetLatestBoostOpportunity(context.Background(), validOpportunity.ChannelID, percy.BoostsDBReadOptions{ConsistentRead: true})
				So(err, ShouldBeNil)
				So(opportunity, ShouldNotBeNil)
				So(*opportunity, ShouldResemble, validOpportunity)
			})

			Convey("can add contributions to it", func() {
				opportunity, err := db.AddBoostOpportunityContribution(context.Background(), validOpportunity.ID, contribution)
				So(err, ShouldBeNil)
				So(opportunity.UnitsContributed, ShouldEqual, contribution.Units)

				Convey("can retrieve recorded contribution", func() {
					recordedContribution, err := db.GetBoostOpportunityContribution(context.Background(), opportunity.ID, contribution.OriginID, percy.BoostsDBReadOptions{ConsistentRead: true})
					So(err, ShouldBeNil)
					So(recordedContribution, ShouldNotBeNil)
					So(*recordedContribution, ShouldResemble, contribution)
				})

				Convey("cannot add the same contribution multiple times", func() {
					_, err := db.AddBoostOpportunityContribution(context.Background(), validOpportunity.ID, contribution)
					So(err, ShouldNotBeNil)

					// boost opportunity should still just have impressions form the first contribution
					opportunity, err := db.GetLatestBoostOpportunity(context.Background(), validOpportunity.ChannelID, percy.BoostsDBReadOptions{ConsistentRead: true})
					So(err, ShouldBeNil)
					So(opportunity.UnitsContributed, ShouldResemble, contribution.Units)
				})
			})

			Convey("can end it with a timestamp and order ID", func() {
				boostOpportunity, err := db.GetLatestBoostOpportunity(context.Background(), validOpportunity.ChannelID, percy.BoostsDBReadOptions{ConsistentRead: true})
				So(err, ShouldBeNil)
				So(boostOpportunity, ShouldNotBeNil)

				orderID := "order-id"
				err = db.EndLatestBoostOpportunity(context.Background(), validOpportunity.ChannelID, time.Now(), &orderID)
				So(err, ShouldBeNil)

				Convey("the channel should have no active boost opportunity now", func() {
					opportunity, err := db.GetLatestBoostOpportunity(context.Background(), validOpportunity.ChannelID, percy.BoostsDBReadOptions{ConsistentRead: true})
					So(err, ShouldBeNil)
					So(opportunity.EndedAt, ShouldNotBeNil)
					So(opportunity.OrderID, ShouldNotBeNil)
					So(*opportunity.OrderID, ShouldEqual, orderID)
				})
			})
		})
	})
}
