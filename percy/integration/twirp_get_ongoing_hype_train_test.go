// +build integration

package integration

import (
	"context"

	"code.justin.tv/commerce/percy/internal/dynamodb"
	"code.justin.tv/commerce/percy/internal/tests"
	"code.justin.tv/commerce/percy/rpc"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func (s *PercyIntegTestSuite) TestGetOngoingHypeTrainAPI() {
	cfg := loadConfig()
	client := NewTwirpClient()

	Convey("Given a percy client", s.T(), func() {
		ctx := context.Background()
		channelID := "test_channel:" + uuid.New().String()
		hypeTrainDB, _ := dynamodb.NewHypeTrainDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
			Endpoint:    cfg.Endpoints.DynamoDB,
		})

		Convey("for a channel that has an active hype train", func() {
			ongoingHypeTrain := tests.ValidOngoingHypeTrain()
			ongoingHypeTrain.ChannelID = channelID
			ongoingHypeTrain.ID = uuid.New().String()

			err := hypeTrainDB.UpsertHypeTrain(ctx, ongoingHypeTrain)
			So(err, ShouldBeNil)

			Convey("it should return the hype train", func() {
				resp, err := client.GetOngoingHypeTrain(ctx, &rpc.GetOngoingHypeTrainReq{
					ChannelId: channelID,
				})

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.HypeTrain, ShouldNotBeNil)
				So(resp.HypeTrain.ChannelId, ShouldEqual, channelID)
				So(resp.HypeTrain.Id, ShouldEqual, ongoingHypeTrain.ID)
			})
		})

		Convey("for a channel that has no past hype train", func() {
			channelID := "test_channel:" + uuid.New().String()
			Convey("it should return no hype train", func() {
				resp, err := client.GetOngoingHypeTrain(ctx, &rpc.GetOngoingHypeTrainReq{
					ChannelId: channelID,
				})

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.HypeTrain, ShouldBeNil)
			})

		})

		Convey("for a channel that has a past hype train", func() {
			expiredHypeTrain := tests.ValidExpiredHypeTrain()
			expiredHypeTrain.ChannelID = channelID
			expiredHypeTrain.ID = uuid.New().String()

			err := hypeTrainDB.UpsertHypeTrain(ctx, expiredHypeTrain)
			So(err, ShouldBeNil)

			Convey("it should return the hype train", func() {
				resp, err := client.GetOngoingHypeTrain(ctx, &rpc.GetOngoingHypeTrainReq{
					ChannelId: channelID,
				})

				So(err, ShouldBeNil)
				So(resp, ShouldNotBeNil)
				So(resp.HypeTrain, ShouldBeNil)
			})
		})
	})
}
