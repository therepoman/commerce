// +build integration dynamodb

package integration

import (
	"context"
	"fmt"
	"time"

	"code.justin.tv/commerce/gogogadget/pointers"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func (s *PercyIntegTestSuite) TestHypeTrainConfigsDB() {
	cfg := loadConfig()

	channelID := fmt.Sprintf("test_channel:%s", uuid.New())

	easyDifficulty := percy.DifficultyEasy
	mediumDifficulty := percy.DifficultyMedium

	Convey("Given a hype train configs dynamoDB db", s.T(), func() {
		db, err := dynamodb.NewHypeTrainConfigsDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
			Endpoint:    cfg.Endpoints.DynamoDB,
		})
		So(err, ShouldBeNil)

		ctx := context.Background()

		Convey("loads a valid channel config", func() {
			Convey("when there is a global default config", func() {
				// Assuming a global default configuration is already been put into the staging database
				channelConfig, err := db.GetConfig(ctx, channelID)
				So(err, ShouldBeNil)
				So(channelConfig, ShouldNotBeNil)
				So(channelConfig.IsValid(), ShouldBeTrue)
			})

			Convey("when there is a recommended config", func() {
				recommendedConfig := percy.HypeTrainConfigUpdate{
					Enabled:              pointers.BoolP(true),
					NumOfEventsToKickoff: pointers.IntP(2),
					Difficulty:           &easyDifficulty,
				}

				err := db.UpdateRecommendedConfig(ctx, channelID, recommendedConfig)
				So(err, ShouldBeNil)

				channelConfig, err := db.GetConfig(ctx, channelID)
				So(err, ShouldBeNil)
				So(channelConfig, ShouldNotBeNil)
				So(channelConfig.IsValid(), ShouldBeTrue)
				So(channelConfig.Enabled, ShouldEqual, *recommendedConfig.Enabled)
				So(channelConfig.Difficulty, ShouldEqual, *recommendedConfig.Difficulty)
			})

			Convey("when there is a custom config", func() {
				configUpdate := percy.HypeTrainConfigUpdate{
					NumOfEventsToKickoff:    pointers.IntP(20),
					Difficulty:              &mediumDifficulty,
					CooldownDuration:        pointers.DurationP(2 * time.Hour),
					CalloutEmoteID:          pointers.StringP("32123124"),
					UseCreatorColor:         pointers.BoolP(true),
					UsePersonalizedSettings: pointers.BoolP(true),
				}

				updatedConfig, err := db.UpdateConfig(ctx, channelID, configUpdate)
				So(err, ShouldBeNil)
				So(updatedConfig, ShouldNotBeNil)
				So(updatedConfig.IsValid(), ShouldBeTrue)

				channelConfig, err := db.GetConfig(ctx, channelID)
				So(err, ShouldBeNil)
				So(channelConfig, ShouldNotBeNil)
				So(channelConfig.IsValid(), ShouldBeTrue)
				So(channelConfig.KickoffConfig.NumOfEvents, ShouldEqual, *configUpdate.NumOfEventsToKickoff)
				So(channelConfig.Difficulty, ShouldEqual, *configUpdate.Difficulty)
				So(channelConfig.CooldownDuration, ShouldEqual, *configUpdate.CooldownDuration)
				So(channelConfig.CalloutEmoteID, ShouldEqual, *configUpdate.CalloutEmoteID)
				So(channelConfig.UseCreatorColor, ShouldEqual, *configUpdate.UseCreatorColor)
				So(channelConfig.UsePersonalizedSettings, ShouldEqual, *configUpdate.UsePersonalizedSettings)

				Convey("When we unset a field from the custom config", func() {
					configUnset := percy.HypeTrainConfigUnset{CalloutEmoteID: true}

					updatedConfig, err := db.UnsetConfig(ctx, channelID, configUnset)
					So(err, ShouldBeNil)
					So(updatedConfig, ShouldNotBeNil)
					So(updatedConfig.IsValid(), ShouldBeTrue)

					channelConfig, err := db.GetConfig(ctx, channelID)
					So(err, ShouldBeNil)
					So(channelConfig, ShouldNotBeNil)
					So(channelConfig.KickoffConfig.NumOfEvents, ShouldEqual, *configUpdate.NumOfEventsToKickoff)
					So(channelConfig.Difficulty, ShouldEqual, *configUpdate.Difficulty)
					So(channelConfig.CooldownDuration, ShouldEqual, *configUpdate.CooldownDuration)
					So(channelConfig.UseCreatorColor, ShouldEqual, *configUpdate.UseCreatorColor)
					So(channelConfig.UsePersonalizedSettings, ShouldEqual, *configUpdate.UsePersonalizedSettings)
					So(channelConfig.CalloutEmoteID, ShouldEqual, "81273") // This is komodohype, the default
				})
			})
		})
	})
}
