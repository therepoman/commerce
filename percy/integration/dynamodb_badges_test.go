// +build integration dynamodb

package integration

import (
	"context"
	"fmt"

	"code.justin.tv/commerce/percy/internal/dynamodb"
	"code.justin.tv/commerce/percy/internal/tests"
	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func (s *PercyIntegTestSuite) TestBadgesDB() {
	cfg := loadConfig()

	userID := fmt.Sprintf("test_user:%s", uuid.New())
	channelID := fmt.Sprintf("test_channel:%s", uuid.New())
	badges := tests.ValidBadgesResponse()

	Convey("given a badges dynamoDB db", s.T(), func() {
		db, err := dynamodb.NewBadgesDB(dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
			Endpoint:    cfg.Endpoints.DynamoDB,
		})
		So(err, ShouldBeNil)

		ctx := context.Background()

		Convey("loads nil when there's no badges", func() {
			actual, err := db.GetBadges(ctx, userID, channelID)

			So(err, ShouldBeNil)
			So(actual, ShouldHaveLength, 0)

			Convey("sets badge appropriately", func() {
				err := db.SetBadges(ctx, userID, channelID, badges)

				So(err, ShouldBeNil)

				Convey("new badge is returned", func() {
					actual, err := db.GetBadges(ctx, userID, channelID)

					So(err, ShouldBeNil)
					So(actual, ShouldHaveLength, 1)
					So(actual[0], ShouldEqual, badges[0])
				})
			})
		})

		Convey("sets bulk badges just ok", func() {
			userID1 := fmt.Sprintf("test_user:%s", uuid.New())
			userID2 := fmt.Sprintf("test_user:%s", uuid.New())
			userIDs := []string{userID1, userID2}
			channelID := fmt.Sprintf("test_channel:%s", uuid.New())

			for _, userID := range userIDs {
				actual, err := db.GetBadges(ctx, userID, channelID)

				So(err, ShouldBeNil)
				So(actual, ShouldHaveLength, 0)
			}

			Convey("sets badge appropriately", func() {
				err := db.SetBulkBadges(ctx, userIDs, channelID, badges)

				So(err, ShouldBeNil)

				Convey("new badge is returned", func() {
					for _, userID := range userIDs {
						actual, err := db.GetBadges(ctx, userID, channelID)

						So(err, ShouldBeNil)
						So(actual, ShouldHaveLength, 1)
						So(actual[0], ShouldEqual, badges[0])
					}
				})
			})
		})
	})
}
