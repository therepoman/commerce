// +build integration internal

package integration

import (
	"code.justin.tv/commerce/percy/internal/tests"
	"context"
	"time"

	"code.justin.tv/chat/pubsub-go-pubclient/client"
	gogogadget_sns "code.justin.tv/commerce/gogogadget/aws/sns"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/arbiter"
	"code.justin.tv/commerce/percy/internal/badges"
	"code.justin.tv/commerce/percy/internal/chainpublisher"
	"code.justin.tv/commerce/percy/internal/datascience"
	"code.justin.tv/commerce/percy/internal/destiny"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"code.justin.tv/commerce/percy/internal/eventbus"
	"code.justin.tv/commerce/percy/internal/mako"
	"code.justin.tv/commerce/percy/internal/metrics"
	"code.justin.tv/commerce/percy/internal/miniexperiments"
	"code.justin.tv/commerce/percy/internal/miniexperiments/ht_approaching"
	"code.justin.tv/commerce/percy/internal/pantheon"
	"code.justin.tv/commerce/percy/internal/personalization"
	"code.justin.tv/commerce/percy/internal/pubsub"
	"code.justin.tv/commerce/percy/internal/pubsub/rewards"
	"code.justin.tv/commerce/percy/internal/sandstorm"
	"code.justin.tv/commerce/percy/internal/sns"
	"code.justin.tv/commerce/percy/internal/users"
	"code.justin.tv/foundation/twitchclient"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
)

func (s *PercyIntegTestSuite) TestNewParticipation_Kickoff() {
	cfg := loadConfig()

	channelID := uuid.New().String()
	userID1 := uuid.New().String()
	userID2 := uuid.New().String()
	userID3 := uuid.New().String()

	Convey("given an engine", s.T(), func() {
		statter := &metrics.NoopStatter{}

		sandstormClient, err := sandstorm.New(&cfg)
		So(err, ShouldBeNil)

		hypeTrainLiveSlackWebHookSecret, err := sandstormClient.GetSecret(cfg.HypeTrainLiveSlackHookSecretName)
		So(err, ShouldBeNil)

		HypetrainLiveSlackWebhookURL := string(hypeTrainLiveSlackWebHookSecret.Plaintext)

		dynamoDBConfig := dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
			Endpoint:    cfg.Endpoints.DynamoDB,
		}

		hypeTrainConfigsDB, err := dynamodb.NewHypeTrainConfigsDB(dynamoDBConfig)
		So(err, ShouldBeNil)

		hypeTrainDB, err := dynamodb.NewHypeTrainDB(dynamoDBConfig)
		So(err, ShouldBeNil)

		participationDB, err := dynamodb.NewParticipationsDB(dynamoDBConfig)
		So(err, ShouldBeNil)

		participantDB, err := dynamodb.NewParticipantsDB(dynamoDBConfig)
		So(err, ShouldBeNil)

		badgesDB, err := dynamodb.NewBadgesDB(dynamoDBConfig)
		So(err, ShouldBeNil)

		conductorsDB, err := dynamodb.NewConductorsDB(dynamoDBConfig)
		So(err, ShouldBeNil)

		approachingDB, err := dynamodb.NewApproachingDB(dynamoDBConfig)
		So(err, ShouldBeNil)

		bitsPinataDB, err := dynamodb.NewBitsPinataDB(dynamoDBConfig)
		So(err, ShouldBeNil)

		spade, err := datascience.NewSpadeClient(cfg.SpadeConf.Environment, cfg.SpadeConf.MaxConcurrency, statter, cfg.SOCKS5Addr, false)
		So(err, ShouldBeNil)

		eventDB := destiny.NewEventDB(destiny.NewClient(cfg.Endpoints.Destiny, &metrics.NoopStatter{}, cfg.SOCKS5Addr))

		entitlementDB := mako.NewEntitlementDB(cfg.Endpoints.Mako, statter, cfg.SOCKS5Addr)

		userProfileDB, err := users.NewUserProfileDB(cfg.Endpoints.UsersService, cfg.Endpoints.TMI, cfg.Endpoints.Ripley, statter, cfg.SOCKS5Addr)
		So(err, ShouldBeNil)

		leaderboard := pantheon.NewLeaderboard(cfg.Endpoints.Pantheon, statter, cfg.SOCKS5Addr)

		badgesClient, err := badges.NewClient(cfg.Endpoints.Badges, &metrics.NoopStatter{}, cfg.SOCKS5Addr)
		So(err, ShouldBeNil)

		emoteFetcher := mako.NewFetcher(cfg.Endpoints.Mako, &metrics.NoopStatter{})

		rewardsDisplayInfoFetcher := rewards.NewDisplayInfoFetcher(badgesClient, emoteFetcher)

		rewardsDisplayInfoAttacher := rewards.NewRewardsDisplayInfoAttacher(rewardsDisplayInfoFetcher)

		pubConfig := twitchclient.ClientConf{
			Host: cfg.Endpoints.Pubsub,
		}
		pubsubClient, err := client.NewPubClient(pubConfig)
		So(err, ShouldBeNil)

		miniexperimentsClient := miniexperiments.NewNoopClient()
		pubsubPublisher := pubsub.NewPublisher(
			pubsubClient, nil, HypetrainLiveSlackWebhookURL, userProfileDB, rewardsDisplayInfoAttacher, rewardsDisplayInfoFetcher,
			nil, nil, miniexperimentsClient, miniexperimentsClient,
			false, []string{})

		eventbusPublisher, err := eventbus.NewPublisher(cfg.AWSRegion, cfg.Environment, hypeTrainConfigsDB)
		So(err, ShouldBeNil)

		chainPublisher := chainpublisher.New(pubsubPublisher, eventbusPublisher)

		conductorBadgesHandler := percy.NewConductorBadgesHandler(conductorsDB, badgesDB, badgesClient, spade)
		dynamicTagClient, err := arbiter.NewDynamicTagClient(&cfg, &metrics.NoopStatter{})
		if err != nil {
			logrus.WithError(err).Fatal("could not initialize arbiter client")
		}

		snsPublisher := sns.NewPublisher(cfg, gogogadget_sns.NewDefaultClient())

		personalizationClient := personalization.NewClient(cfg.Endpoints.Personalization, statter, cfg.SOCKS5Addr)

		approachingMiniexperimentsClient := ht_approaching.NewClient()

		engine := percy.NewEngine(percy.EngineConfig{
			ApproachingDB:               approachingDB,
			BitsPinataDB:                bitsPinataDB,
			ConfigsDB:                   hypeTrainConfigsDB,
			HypeTrainDB:                 hypeTrainDB,
			ParticipantDB:               participantDB,
			ParticipationDB:             participationDB,
			EventDB:                     eventDB,
			EntitlementDB:               entitlementDB,
			UserProfileDB:               userProfileDB,
			Statter:                     statter,
			Leaderboard:                 leaderboard,
			Publisher:                   chainPublisher,
			Spade:                       spade,
			ConductorBadgesHandler:      conductorBadgesHandler,
			ConductorsDB:                conductorsDB,
			BadgesDB:                    badgesDB,
			DynamicTag:                  dynamicTagClient,
			SNSPublisher:                snsPublisher,
			Personalization:             personalizationClient,
			ApproachingExperimentClient: approachingMiniexperimentsClient,
		})

		Convey("with just 1 supporting event", func() {
			err := engine.RecordParticipation(context.Background(), percy.Participation{
				ChannelID: channelID,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				},
				Quantity:  100,
				Timestamp: time.Now(),
				User:      percy.NewUser(userID1),
			})
			So(err, ShouldBeNil)

			Convey("should not start a hype train yet", func() {
				// make sure to add approaching back here if you're uncommenting the tests below post HTA launch
				train, _, err := engine.GetOngoingHypeTrain(context.Background(), channelID)
				So(err, ShouldBeNil)
				So(train, ShouldBeNil)
				// This should be uncommented once we remove the feature flag gating on this product,
				// but for now the experiment flag being respected means that it won't appear for test users
				//So(approaching, ShouldNotBeNil)

				Convey("with 2 supporting events", func() {
					err = engine.RecordParticipation(context.Background(), percy.Participation{
						ChannelID: channelID,
						ParticipationIdentifier: percy.ParticipationIdentifier{
							Source: percy.SubsSource,
							Action: percy.Tier1GiftedSubAction,
						},
						Quantity:  10,
						Timestamp: time.Now(),
						User:      percy.NewUser(userID2),
					})
					So(err, ShouldBeNil)

					Convey("should not have started a hype train", func() {
						// make sure to add approaching back here if you're uncommenting the tests below post HTA launch
						train, _, err = engine.GetOngoingHypeTrain(context.Background(), channelID)
						So(err, ShouldBeNil)
						So(train, ShouldBeNil)
						// This should be uncommented once we remove the feature flag gating on this product,
						// but for now the experiment flag being respected means that it won't appear for test users
						//So(approaching, ShouldNotBeNil)

						Convey("with 3 supporting events", func() {
							err = engine.RecordParticipation(context.Background(), percy.Participation{
								ChannelID: channelID,
								ParticipationIdentifier: percy.ParticipationIdentifier{
									Source: percy.BitsSource,
									Action: percy.CheerAction,
								},
								Quantity:  100,
								Timestamp: time.Now(),
								User:      percy.NewUser(userID3),
							})
							So(err, ShouldBeNil)

							Convey("should start a hype train with all 3 kickoff participation events", func() {
								// make sure to add approaching back here if you're uncommenting the tests below post HTA launch
								train, _, err = engine.GetOngoingHypeTrain(context.Background(), channelID)
								So(err, ShouldBeNil)
								So(train, ShouldNotBeNil)
								So(train.ParticipationPoints(), ShouldEqual, percy.ParticipationPoint(5200))
								// This should be uncommented once we remove the feature flag gating on this product,
								// but for now the experiment flag being respected means that it won't appear for test users
								//So(approaching, ShouldBeNil)
							})
						})
					})
				})
			})
		})
	})
}

func (s *PercyIntegTestSuite) TestNewParticipation_KickoffWithActivePinata() {
	cfg := loadConfig()

	channelID := uuid.New().String()
	userID1 := uuid.New().String()
	userID2 := uuid.New().String()
	userID3 := uuid.New().String()

	Convey("given an engine", s.T(), func() {
		statter := &metrics.NoopStatter{}

		sandstormClient, err := sandstorm.New(&cfg)
		So(err, ShouldBeNil)

		hypeTrainLiveSlackWebHookSecret, err := sandstormClient.GetSecret(cfg.HypeTrainLiveSlackHookSecretName)
		So(err, ShouldBeNil)

		HypetrainLiveSlackWebhookURL := string(hypeTrainLiveSlackWebHookSecret.Plaintext)

		dynamoDBConfig := dynamodb.DynamoDBConfig{
			Region:      cfg.AWSRegion,
			Environment: cfg.DynamoSuffix,
			Endpoint:    cfg.Endpoints.DynamoDB,
		}

		hypeTrainConfigsDB, err := dynamodb.NewHypeTrainConfigsDB(dynamoDBConfig)
		So(err, ShouldBeNil)

		hypeTrainDB, err := dynamodb.NewHypeTrainDB(dynamoDBConfig)
		So(err, ShouldBeNil)

		participationDB, err := dynamodb.NewParticipationsDB(dynamoDBConfig)
		So(err, ShouldBeNil)

		participantDB, err := dynamodb.NewParticipantsDB(dynamoDBConfig)
		So(err, ShouldBeNil)

		badgesDB, err := dynamodb.NewBadgesDB(dynamoDBConfig)
		So(err, ShouldBeNil)

		conductorsDB, err := dynamodb.NewConductorsDB(dynamoDBConfig)
		So(err, ShouldBeNil)

		approachingDB, err := dynamodb.NewApproachingDB(dynamoDBConfig)
		So(err, ShouldBeNil)

		bitsPinataDB, err := dynamodb.NewBitsPinataDB(dynamoDBConfig)
		So(err, ShouldBeNil)

		spade, err := datascience.NewSpadeClient(cfg.SpadeConf.Environment, cfg.SpadeConf.MaxConcurrency, statter, cfg.SOCKS5Addr, false)
		So(err, ShouldBeNil)

		eventDB := destiny.NewEventDB(destiny.NewClient(cfg.Endpoints.Destiny, &metrics.NoopStatter{}, cfg.SOCKS5Addr))

		entitlementDB := mako.NewEntitlementDB(cfg.Endpoints.Mako, statter, cfg.SOCKS5Addr)

		userProfileDB, err := users.NewUserProfileDB(cfg.Endpoints.UsersService, cfg.Endpoints.TMI, cfg.Endpoints.Ripley, statter, cfg.SOCKS5Addr)
		So(err, ShouldBeNil)

		leaderboard := pantheon.NewLeaderboard(cfg.Endpoints.Pantheon, statter, cfg.SOCKS5Addr)

		badgesClient, err := badges.NewClient(cfg.Endpoints.Badges, &metrics.NoopStatter{}, cfg.SOCKS5Addr)
		So(err, ShouldBeNil)

		emoteFetcher := mako.NewFetcher(cfg.Endpoints.Mako, &metrics.NoopStatter{})

		rewardsDisplayInfoFetcher := rewards.NewDisplayInfoFetcher(badgesClient, emoteFetcher)

		rewardsDisplayInfoAttacher := rewards.NewRewardsDisplayInfoAttacher(rewardsDisplayInfoFetcher)

		pubConfig := twitchclient.ClientConf{
			Host: cfg.Endpoints.Pubsub,
		}
		pubsubClient, err := client.NewPubClient(pubConfig)
		So(err, ShouldBeNil)

		miniexperimentsClient := miniexperiments.NewNoopClient()
		pubsubPublisher := pubsub.NewPublisher(
			pubsubClient, nil, HypetrainLiveSlackWebhookURL, userProfileDB, rewardsDisplayInfoAttacher, rewardsDisplayInfoFetcher,
			nil, nil, miniexperimentsClient, miniexperimentsClient,
			false, []string{})

		eventbusPublisher, err := eventbus.NewPublisher(cfg.AWSRegion, cfg.Environment, hypeTrainConfigsDB)
		So(err, ShouldBeNil)

		chainPublisher := chainpublisher.New(pubsubPublisher, eventbusPublisher)

		conductorBadgesHandler := percy.NewConductorBadgesHandler(conductorsDB, badgesDB, badgesClient, spade)
		dynamicTagClient, err := arbiter.NewDynamicTagClient(&cfg, &metrics.NoopStatter{})
		if err != nil {
			logrus.WithError(err).Fatal("could not initialize arbiter client")
		}

		snsPublisher := sns.NewPublisher(cfg, gogogadget_sns.NewDefaultClient())

		personalizationClient := personalization.NewClient(cfg.Endpoints.Personalization, statter, cfg.SOCKS5Addr)

		approachingMiniexperimentsClient := ht_approaching.NewClient()

		ctx := context.Background()
		bitsPinataDB.CreateBitsPinata(ctx, tests.ValidActiveBitsPinata(channelID))

		engine := percy.NewEngine(percy.EngineConfig{
			ApproachingDB:               approachingDB,
			BitsPinataDB:                bitsPinataDB,
			ConfigsDB:                   hypeTrainConfigsDB,
			HypeTrainDB:                 hypeTrainDB,
			ParticipantDB:               participantDB,
			ParticipationDB:             participationDB,
			EventDB:                     eventDB,
			EntitlementDB:               entitlementDB,
			UserProfileDB:               userProfileDB,
			Statter:                     statter,
			Leaderboard:                 leaderboard,
			Publisher:                   chainPublisher,
			Spade:                       spade,
			ConductorBadgesHandler:      conductorBadgesHandler,
			ConductorsDB:                conductorsDB,
			BadgesDB:                    badgesDB,
			DynamicTag:                  dynamicTagClient,
			SNSPublisher:                snsPublisher,
			Personalization:             personalizationClient,
			ApproachingExperimentClient: approachingMiniexperimentsClient,
		})

		Convey("with 3 supporting events during a bits pinata", func() {
			err := bitsPinataDB.CreateBitsPinata(ctx, tests.ValidActiveBitsPinata(channelID))
			So(err, ShouldBeNil)

			err = engine.RecordParticipation(context.Background(), percy.Participation{
				ChannelID: channelID,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				},
				Quantity:  100,
				Timestamp: time.Now(),
				User:      percy.NewUser(userID1),
			})
			So(err, ShouldBeNil)

			err = engine.RecordParticipation(context.Background(), percy.Participation{
				ChannelID: channelID,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.SubsSource,
					Action: percy.Tier1GiftedSubAction,
				},
				Quantity:  10,
				Timestamp: time.Now(),
				User:      percy.NewUser(userID2),
			})
			So(err, ShouldBeNil)

			err = engine.RecordParticipation(context.Background(), percy.Participation{
				ChannelID: channelID,
				ParticipationIdentifier: percy.ParticipationIdentifier{
					Source: percy.BitsSource,
					Action: percy.CheerAction,
				},
				Quantity:  100,
				Timestamp: time.Now(),
				User:      percy.NewUser(userID3),
			})
			So(err, ShouldBeNil)

			train, _, err := engine.GetOngoingHypeTrain(context.Background(), channelID)
			So(err, ShouldBeNil)
			So(train, ShouldBeNil)
		})
	})
}
