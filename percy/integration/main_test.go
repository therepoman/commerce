// +build integration dynamodb

package integration

import (
	"context"
	"errors"
	"fmt"
	"math/rand"
	"os"
	"testing"
	"time"
)

const (
	numAttempts = 1
	retryDelay  = time.Second * 5
)

func TestMain(m *testing.M) {
	_, cancel := context.WithCancel(context.Background())
	defer cancel()

	rand.Seed(time.Now().UTC().UnixNano())

	var exitCode int
	for i := 1; i <= numAttempts; i++ {
		fmt.Printf("\n\nBeginning Test attempt %d....\n\n", i)
		err := runTest(m)
		if err == nil {
			exitCode = 0
			break
		} else {
			exitCode = 1

			if i < numAttempts {
				fmt.Println("Tests failed sleeping 5s before retrying...")
				time.Sleep(retryDelay)
			}
		}
	}
	os.Exit(exitCode)
}

func runTest(m *testing.M) (err error) {
	exitCode := m.Run()
	if exitCode == 0 {
		return nil
	} else {
		return errors.New("Test attempt failed")
	}
}
