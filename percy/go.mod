module code.justin.tv/commerce/percy

go 1.16

require (
	code.justin.tv/amzn/CorleoneTwirp v2.0.7+incompatible
	code.justin.tv/amzn/PDMSLambdaTwirp v0.0.0-20210127223959-88d0a40bba85
	code.justin.tv/amzn/TwirpGoLangAWSTransports v0.0.0-20200311200501-30801aea0958
	code.justin.tv/amzn/TwitchArbiterTwirp v0.0.0-20200226165627-dd41e2d064bd
	code.justin.tv/amzn/TwitchDartReceiverTwirp v0.0.0-20210204230056-e3536128a10f
	code.justin.tv/amzn/TwitchPersonalizationTwirp v0.0.0-20201019223827-6f91f0057ea5
	code.justin.tv/amzn/TwitchPromotionsBoosterTwirp v0.0.0-20210624231601-16387db1940f
	code.justin.tv/amzn/TwitchTelemetryMWSMetricsSender v0.0.0-20190731182749-aa2dfbb7fe29 // indirect
	code.justin.tv/cb/hallpass v0.0.0-20181217205108-ff409aeba736
	code.justin.tv/chat/badges v1.8.0
	code.justin.tv/chat/pubsub-control v0.0.0-20180906231336-827131bb48fc
	code.justin.tv/chat/pubsub-go-pubclient v1.0.0
	code.justin.tv/chat/rediczar v1.12.0
	code.justin.tv/chat/tmi v1.17.0
	code.justin.tv/chat/zuma v1.6.1 // indirect
	code.justin.tv/commerce/dynamicconfig v1.1.2
	code.justin.tv/commerce/gogogadget v0.0.0-20200403181836-bbb6f1ea0223
	code.justin.tv/commerce/gringotts v0.2.3
	code.justin.tv/commerce/logrus v1.1.2
	code.justin.tv/commerce/mako v1.1.9
	code.justin.tv/commerce/pantheon v0.2.1-0.20191016231555-0143097070b4
	code.justin.tv/commerce/payday v1.0.2-0.20210820001021-699c2b9a7e8e
	code.justin.tv/commerce/splatter v1.5.2
	code.justin.tv/common/spade-client-go v2.1.2-0.20191022164217-445273a0e877+incompatible
	code.justin.tv/discovery/experiments v6.1.0+incompatible
	code.justin.tv/discovery/liveline v0.0.0-20210622185600-f96f1b165336
	code.justin.tv/eventbus/client v1.7.1
	code.justin.tv/eventbus/schema v0.0.0-20210701175231-583d8e100570
	code.justin.tv/foundation/twitchclient v4.11.0+incompatible
	code.justin.tv/hygienic/spade v1.6.0 // indirect
	code.justin.tv/revenue/everdeen v0.26.8
	code.justin.tv/revenue/mulan v1.0.13
	code.justin.tv/revenue/offer-tenant-schema v2.0.7+incompatible
	code.justin.tv/revenue/ripley v1.1.1
	code.justin.tv/revenue/subscriptions v0.0.0-20200716215303-a1f42052c8fa
	code.justin.tv/sse/malachai v0.5.9
	code.justin.tv/subs/destiny v0.0.0-20191112010136-c5a5fb3fba5f
	code.justin.tv/systems/sandstorm v1.6.6
	code.justin.tv/web/users-service v2.12.0+incompatible
	github.com/SermoDigital/jose v0.9.2-0.20180104203859-803625baeddc // indirect
	github.com/afex/hystrix-go v0.0.0-20180502004556-fa1af6a1f4f5
	github.com/aws/aws-dax-go v1.1.6
	github.com/aws/aws-lambda-go v1.19.0
	github.com/aws/aws-sdk-go v1.33.5
	github.com/cactus/go-statsd-client/statsd v0.0.0-20200423205355-cb0885a1018c
	github.com/go-redis/redis/v7 v7.0.0-beta.4
	github.com/gofrs/uuid v3.3.0+incompatible
	github.com/golang/protobuf v1.4.2
	github.com/google/go-cmp v0.5.4 // indirect
	github.com/google/uuid v1.1.1
	github.com/hashicorp/go-multierror v1.0.0
	github.com/joomcode/errorx v0.8.0
	github.com/pkg/errors v0.9.1
	github.com/segmentio/events/v2 v2.4.0
	github.com/sirupsen/logrus v1.6.0
	github.com/smartystreets/goconvey v1.6.4
	github.com/stretchr/testify v1.6.1
	github.com/twitchtv/twirp v5.12.0+incompatible
	github.com/urfave/cli v1.22.1
	github.com/xeipuuv/gojsonschema v1.2.0 // indirect
	golang.org/x/net v0.0.0-20201021035429-f5854403a974
	golang.org/x/sync v0.0.0-20201020160332-67f06af15bc9
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/protobuf v1.25.0
	gopkg.in/yaml.v2 v2.2.8
)

replace (
	github.com/cactus/go-statsd-client v3.1.1+incompatible => github.com/cactus/go-statsd-client v0.0.0-20190805010426-5089fcbbe532
	gopkg.in/DATA-DOG/go-sqlmock.v1 => github.com/DATA-DOG/go-sqlmock v1.3.3
)
