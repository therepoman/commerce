package rpcTwitchiversary

// These error codes are meant to be returned within a rpcTwitchiversary.ClientError type.

const (
	ErrCodeNotUserAnniversary = "NOT_USER_ANNIVERSARY"
	ErrCodeTokenAlreadyUsed   = "TOKEN_ALREADY_USED"
)

type ClientError struct {
	ErrorCode string `json:"error_code"`
}
