package errors

import (
	"encoding/json"

	"github.com/twitchtv/twirp"
)

const (
	celebiErrorMetaKey = "celebi_error"
)

// NewCelebiError returns a new ClientError that wraps a twirp.Error and stores
// an error code enum.
func NewCelebiError(twErr twirp.Error, celebiError CelebiError) twirp.Error {
	b, err := json.Marshal(&celebiError)
	if err != nil {
		return twErr
	}

	return twErr.WithMeta(celebiErrorMetaKey, string(b))
}

// ParseClientError takes an error, and if it is a CelebiError returns the CelebiError.
func ParseCelebiError(err error) (*CelebiError, error) {
	twErr, ok := err.(twirp.Error) //nolint
	if !ok {
		return nil, err
	}

	clientErr := twErr.Meta(celebiErrorMetaKey)
	if clientErr == "" {
		return nil, err
	}

	var celebiError CelebiError
	jsonErr := json.Unmarshal([]byte(clientErr), &celebiError)

	return &celebiError, jsonErr
}
