package models

import "time"

type EHSEvent struct {
	BroadcasterID    string         `json:"broadcaster_id"`
	HypeTrainID      string         `json:"id"`
	StartedAt        time.Time      `json:"started_at"`
	ExpiresAt        time.Time      `json:"expires_at"`
	CoolDownEndTime  time.Time      `json:"cooldown_end_time"`
	Level            int            `json:"level"`
	Goal             int            `json:"goal"`
	Total            int            `json:"total"`
	TopContributions []Contribution `json:"top_contributions"`
	LastContribution Contribution   `json:"last_contribution"`
}

type Contribution struct {
	Type  string `json:"type"`
	User  string `json:"user"`
	Total int    `json:"total"`
}

type ActivityFeedEventType string

const (
	HypeTrainStarted         = ActivityFeedEventType("HYPE_TRAIN_STARTED")
	HypeTrainEnded           = ActivityFeedEventType("HYPE_TRAIN_ENDED")
	HypeTrainCoolDownExpired = ActivityFeedEventType("HYPE_TRAIN_COOL_DOWN_EXPIRED")
)

type ActivityFeedEvent struct {
	ChannelID      string `json:"channel_id"`
	HypeTrainID    string `json:"hypetrain_id"`
	EventType      string `json:"event_type"`
	CompletedLevel int    `json:"completed_level,omitempty"`
	TotalSubs      int    `json:"total_subs,omitempty"`
	TotalBits      int    `json:"total_bits,omitempty"`
}

type CelebrationPurchaseActivityFeedEvent struct {
	ChannelID string `json:"channel_id"`
	UserID    string `json:"user_id"`
	Intensity string `json:"intensity"`
	Effect    string `json:"effect"`
}
