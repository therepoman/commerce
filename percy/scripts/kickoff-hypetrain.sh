#!/bin/bash

# This is a "brute-force" way to kick off a hype train, this basically sends 5 consecutive participation events to a channel to kick it off.
# ./scripts/kickoff-hypetrain.sh {channelID} {staging | production} {aws profile}
channel=$1
env=$2
profile=$3

# AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247451 -quantity=100
# AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247453 -quantity=100
# AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247454 -quantity=100
# AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247450 -quantity=100
# AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247455 -quantity=100

# AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247450 -quantity=300
# AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247451 -quantity=300
# AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247453 -quantity=300
# AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247454 -quantity=300
# AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247455 -quantity=300

# Animation that completes the entire hypetrain but pauses at 95% for 10s each
# kickoff

testtime=0
sleeptime=10
waittime=20

level1=1600
level2=1800
level3=2100
level4=2300
level5=3000


AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247451 -quantity=100
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247453 -quantity=100
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247454 -quantity=100

sleep $sleeptime

#level1
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247451 -quantity=300
sleep $testtime
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247453 -quantity=300
sleep $testtime
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247452 -quantity=300
sleep $testtime
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247453 -quantity=320

sleep $waittime
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247455 -quantity=200
sleep $sleeptime

#level2
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247451 -quantity=500
sleep $testtime
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247454 -quantity=500
sleep $testtime
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247453 -quantity=300
sleep $testtime
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247454 -quantity=290

sleep $waittime
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247455 -quantity=290
sleep $sleeptime

#level3
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247451 -quantity=500
sleep $testtime
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247454 -quantity=450
sleep $testtime
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247451 -quantity=450
sleep $testtime
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247454 -quantity=395

sleep $waittime
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247455 -quantity=205
sleep $sleeptime

#level4
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247453 -quantity=500
sleep $testtime
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247454 -quantity=535
sleep $testtime
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247451 -quantity=500
sleep $testtime
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247454 -quantity=550

sleep $waittime
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247455 -quantity=215
sleep $sleeptime

#level5
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247451 -quantity=600
sleep $testtime
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247452 -quantity=850
sleep $testtime
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247453 -quantity=600
sleep $testtime
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247454 -quantity=750

sleep $waittime
AWS_PROFILE=$3 go run cmd/send_participation_event/main.go -env=$2 -channel=$channel -source=BITS -action=CHEER -user=134247455 -quantity=100
sleep $sleeptime
