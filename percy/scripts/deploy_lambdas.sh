#!/bin/bash -e

# Flags
version=$1

# User Config
s3bucket="percy-$ENVIRONMENT_FULL-lambdas"
region="us-west-2"
lambdadir=./cmd/lambdas

version="${version//\/}"

deploy(){
   name=$1
   functionname=default-${name}

   aws lambda --region us-west-2 update-function-code \
    --function-name $functionname \
    --s3-bucket $s3bucket \
    --s3-key $name/$version/$name.zip \
    --publish
}

deployall() {
    for path in $lambdadir/*; do
        name=$(basename $path)
        echo "-> [$name] building lambda..."

        deploy $name
    done
}

deployall