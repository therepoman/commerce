module "percy" {
  source = "./percy"
  commit = "latest"

  name           = "percy"
  cluster        = "percy-cluster"
  security_group = var.security_group
  region         = var.region
  backup_region  = var.backup_region
  vpc_id         = var.vpc_id
  environment    = var.environment
  subnets        = var.subnets
  account_id     = var.account_id

  sandstorm_role = var.sandstorm_role

  providers = {
    aws        = aws
    aws.backup = aws.backup
  }
}

