resource "aws_s3_bucket" "terraform-state-storage-s3" {
  bucket = "twitch-percy-${var.environment}-terracode"
  region = var.region

  versioning {
    enabled = false
  }

  lifecycle {
    prevent_destroy = true
  }

  tags = {
    Name        = "S3 Remote Terraform State Store"
    Service     = "percy"
    Environment = var.environment
  }
}

resource "aws_dynamodb_table" "lock-table" {
  name         = "percy-${var.environment}-terracode-lock"
  hash_key     = "LockID"
  billing_mode = "PAY_PER_REQUEST"

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name        = "DynamoDB Terraform State Lock Table"
    Environment = var.environment
  }
}

