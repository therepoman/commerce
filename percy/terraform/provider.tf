provider "aws" {
  region     = var.region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  profile    = var.aws_profile
}

provider "aws" {
  alias   = "backup"
  region  = var.backup_region
  profile = var.aws_profile
}

# terraform {
#   backend "s3" {
#     encrypt = true
#     region  = "us-west-2"
#   }
# }
