region = "us-west-2"

backup_region = "us-east-2"

environment = "staging"

aws_profile = "twitch-percy-aws-devo"

vpc_id = "vpc-02246c08021e18b2b"

security_group = "sg-079b3da4b49717a81"

subnets = "subnet-0d4b2b66ef8ffc36d,subnet-045e72be86dc6b6e6,subnet-01da699669bf60552"

account_id = "999515204624"

sandstorm_role = "percy-staging"
