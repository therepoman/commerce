locals {
  badges_table_name = "badges-${var.environment}"
  badges_channel_id_index_name = "badges-channel-id-index-${var.environment}"
  badges_hash_key   = "user_id"
  badges_range_key  = "channel_id"

  badges_table_min_read = {
    production = 50
    staging    = 10
  }

  badges_table_max_read = {
    production = 20000
    staging    = 20000
  }

  badges_table_min_write = {
    production = 50
    staging    = 10
  }

  badges_table_max_write = {
    production = 20000
    staging    = 20000
  }
}

resource "aws_dynamodb_table" "badges_primary" {
  name             = local.badges_table_name
  billing_mode     = "PAY_PER_REQUEST"
  hash_key         = local.badges_hash_key
  range_key        = local.badges_range_key
  read_capacity    = local.badges_table_min_read[var.environment]
  write_capacity   = local.badges_table_min_write[var.environment]
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "user_id"
    type = "S"
  }

  attribute {
    name = "channel_id"
    type = "S"
  }

  global_secondary_index {
    hash_key = local.badges_range_key
    range_key = local.badges_hash_key
    name = local.badges_channel_id_index_name
    projection_type = "ALL"
  }

  point_in_time_recovery {
    enabled = "true"
  }

  lifecycle {
    prevent_destroy = true

    ignore_changes = [
      read_capacity,
      write_capacity,
    ]
  }
}

module "badges_primary_autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"
  kind       = "table"
  table_name = aws_dynamodb_table.badges_primary.name
  min_read   = local.badges_table_min_read[var.environment]
  max_read   = local.badges_table_max_read[var.environment]
  min_write  = local.badges_table_min_write[var.environment]
  max_write  = local.badges_table_max_write[var.environment]
}

resource "aws_dynamodb_table" "badges_replica" {
  name             = local.badges_table_name
  billing_mode     = "PAY_PER_REQUEST"
  hash_key         = local.badges_hash_key
  range_key        = local.badges_range_key
  read_capacity    = local.badges_table_min_read[var.environment]
  write_capacity   = local.badges_table_min_write[var.environment]
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "user_id"
    type = "S"
  }

  attribute {
    name = "channel_id"
    type = "S"
  }

  global_secondary_index {
    hash_key = local.badges_range_key
    range_key = local.badges_hash_key
    name = local.badges_channel_id_index_name
    projection_type = "ALL"
  }

  point_in_time_recovery {
    enabled = "true"
  }

  lifecycle {
    ignore_changes = [
      read_capacity,
      write_capacity,
    ]
  }

  provider = aws.backup
}

module "badges_replica_autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"
  kind       = "table"
  table_name = aws_dynamodb_table.badges_replica.name
  min_read   = local.badges_table_min_read[var.environment]
  max_read   = local.badges_table_max_read[var.environment]
  min_write  = local.badges_table_min_write[var.environment]
  max_write  = local.badges_table_max_write[var.environment]

  providers = {
    aws = aws.backup
  }
}

resource "aws_dynamodb_global_table" "badges_global_table" {
  depends_on = [aws_dynamodb_table.badges_primary, aws_dynamodb_table.badges_replica]
  name       = local.badges_table_name

  replica {
    region_name = var.region
  }

  replica {
    region_name = var.backup_region
  }
}
