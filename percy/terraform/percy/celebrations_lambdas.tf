module "celebrations_fulfillment_emit_lambda" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  name            = "celebrations_fulfillment_emit"
  handler         = "main"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]

  env_vars = {
    ENVIRONMENT = var.environment
  }
}

module "celebrations_fulfillment_user_notice_lambda" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  name            = "celebrations_fulfillment_user_notice"
  handler         = "main"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]

  env_vars = {
    ENVIRONMENT = var.environment
  }
}

module "celebrations_fulfillment_activity_feed_lambda" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  name            = "celebrations_fulfillment_activity_feed"
  handler         = "main"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]

  env_vars = {
    ENVIRONMENT = var.environment
  }
}

module "celebrations_fulfillment_payout_lambda" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  name            = "celebrations_fulfillment_payout"
  handler         = "main"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]

  env_vars = {
    ENVIRONMENT = var.environment
  }
}

module "celebrations_fulfillment_refund_lambda" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  name            = "celebrations_fulfillment_refund"
  handler         = "main"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]

  env_vars = {
    ENVIRONMENT = var.environment
  }
}

module "celebrations_fulfillment_refund_email_lambda" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  name            = "celebrations_fulfillment_refund_email"
  handler         = "main"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]

  env_vars = {
    ENVIRONMENT = var.environment
  }
}

module "celebrations_fulfillment_datascience_lambda" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  name            = "celebrations_fulfillment_datascience"
  handler         = "main"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]

  env_vars = {
    ENVIRONMENT = var.environment
  }
}

module "celebrations_revocation_clawback_payout_lambda" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  name            = "celebrations_revocation_clawback_payout"
  handler         = "main"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]

  env_vars = {
    ENVIRONMENT = var.environment
  }
}

module "celebrations_channel_purchasable_config_spade_lambda" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  name            = "celebrations_channel_purchasable_config_spade"
  handler         = "main"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]

  env_vars = {
    ENVIRONMENT = var.environment
  }
}

resource "aws_lambda_event_source_mapping" "stream" {
  event_source_arn  = aws_dynamodb_table.celebrations_channel_purchasable_configs_primary.stream_arn
  function_name     = module.celebrations_channel_purchasable_config_spade_lambda.lambda_arn
  starting_position = "TRIM_HORIZON"
}
