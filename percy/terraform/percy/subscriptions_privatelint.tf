locals {
  subscriptions-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-028c4edba1d80b104"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-0da600ed5479b2bbc"
  }

  subscriptions-prefix = {
    staging    = "beta"
    production = "prod"
  }

  subscriptions_domain_name = "subscriptions.twitch.a2z.com"
}

resource "aws_route53_zone" "subscriptions" {
  name = local.subscriptions_domain_name

  vpc {
    vpc_id = var.vpc_id
  }

  tags = {
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_vpc_endpoint" "subscriptions" {
  service_name        = local.subscriptions-vpce[var.environment]
  security_group_ids  = [var.security_group]
  subnet_ids          = split(",", var.subnets)
  vpc_id              = var.vpc_id
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = false

  tags = {
    Domain      = "main.us-west-2.${local.subscriptions-prefix[var.environment]}.subscriptions.twitch.a2z.com"
    Name        = "subscriptions"
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_route53_record" "subscriptions-endpoint-record" {
  zone_id = aws_route53_zone.subscriptions.zone_id
  name    = "main.us-west-2.${local.subscriptions-prefix[var.environment]}.subscriptions.twitch.a2z.com"
  type    = "CNAME"
  ttl     = "300"
  records = [lookup(aws_vpc_endpoint.subscriptions.dns_entry[0], "dns_name")]
}
