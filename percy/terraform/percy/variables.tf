variable "environment" {
  type        = string
  description = "e.g., production/staging"
}

variable "commit" {
  type        = string
  description = "git sha commit of the service"
}

variable "security_group" {
  type        = string
  description = "Security group to attach to the ECS services"
}

variable "region" {
  type        = string
  description = "AWS region (e.g., us-west-2)"
}

variable "backup_region" {
  type        = string
  description = "AWS region for backups (e.g., us-west-2)"
}

variable "vpc_id" {
  type        = string
  description = "Private network to create resources in"
}

variable "name" {
  type        = string
  default     = "Percy"
  description = "Optional namespace to group resources with"
}

variable "subnets" {
  type = string
}

variable "cluster" {
  type        = string
  description = "ECS cluster to run services on"
}

variable "min_count" {
  default     = 3
  description = "Minimum number of Chronobreak containers"
}

variable "max_count" {
  default     = 300
  description = "Maximum number of Chronobreak containers"
}

variable "account_id" {}

provider "aws" {}

provider "aws" {
  alias = "backup"
}

variable "sandstorm_role" {}
