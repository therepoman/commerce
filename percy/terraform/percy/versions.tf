terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 2.70.0"
    }
    http = {
      source = "hashicorp/http"
    }
  }
  required_version = ">= 0.14"
}
