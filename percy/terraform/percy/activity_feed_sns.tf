// Destiny will be sending an arbitrary event (e.g. cooldown period expiration) to this SNS topic and Sauron
// will be subscribing to it, so the event shows up on the activity feed.
locals {
  sauron_account = "989470033077" # Sauron has just one AWS account for both staging and prod
}

resource "aws_sns_topic" "hypetrain-activity-feeds" {
  name = "hypetrain-activity-feeds"

  delivery_policy = <<EOF
{
  "http": {
    "defaultHealthyRetryPolicy": {
      "minDelayTarget": 20,
      "maxDelayTarget": 20,
      "numRetries": 3,
      "numMaxDelayRetries": 0,
      "numNoDelayRetries": 0,
      "numMinDelayRetries": 0,
      "backoffFunction": "linear"
    },
    "disableSubscriptionOverrides": false
  }
}
EOF
}

resource "aws_sns_topic_policy" "hypetrain-activity-feeds" {
  arn = aws_sns_topic.hypetrain-activity-feeds.arn

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Id": "subscribers",
  "Statement": [
    {
      "Sid": "root-account",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${local.percy_account[var.environment]}:root"
        ]
      },
      "Action": [
        "SNS:RemovePermission",
        "SNS:SetTopicAttributes",
        "SNS:DeleteTopic",
        "SNS:ListSubscriptionsByTopic",
        "SNS:GetTopicAttributes",
        "SNS:Receive",
        "SNS:AddPermission",
        "SNS:Subscribe",
        "SNS:Publish"
      ],
      "Resource": "${aws_sns_topic.hypetrain-activity-feeds.arn}"
    },
    {
      "Sid": "subscriptions",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${local.destiny_account[var.environment]}:root"
        ]
      },
      "Action": "SNS:Publish",
      "Resource": "${aws_sns_topic.hypetrain-activity-feeds.arn}"
    },
    {
      "Sid": "sauron-subscription",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${local.sauron_account}:root"
        ]
      },
      "Action": [
        "SNS:Receive",
        "SNS:Subscribe"
      ],
      "Resource": "${aws_sns_topic.hypetrain-activity-feeds.arn}"
    },
    {
      "Sid": "percy-app",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${local.percy_account[var.environment]}:role/${module.service.service_role}"
        ]
      },
      "Action": [
        "SNS:Publish"
      ],
      "Resource": "${aws_sns_topic.hypetrain-activity-feeds.arn}"
    },
    {
      "Sid": "percy-app-canary",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${local.percy_account[var.environment]}:role/${module.service.canary_role}"
        ]
      },
      "Action": [
        "SNS:Publish"
      ],
      "Resource": "${aws_sns_topic.hypetrain-activity-feeds.arn}"
    }
  ]
}
EOF
}

resource "aws_sns_topic" "celebrations-activity-feeds" {
  name = "celebrations-activity-feeds"

  delivery_policy = <<EOF
{
  "http": {
    "defaultHealthyRetryPolicy": {
      "minDelayTarget": 20,
      "maxDelayTarget": 20,
      "numRetries": 3,
      "numMaxDelayRetries": 0,
      "numNoDelayRetries": 0,
      "numMinDelayRetries": 0,
      "backoffFunction": "linear"
    },
    "disableSubscriptionOverrides": false
  }
}
EOF
}

resource "aws_sns_topic_policy" "celebrations-activity-feeds" {
  arn = aws_sns_topic.celebrations-activity-feeds.arn

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Id": "subscribers",
  "Statement": [
    {
      "Sid": "root-account",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${local.percy_account[var.environment]}:root"
        ]
      },
      "Action": [
        "SNS:RemovePermission",
        "SNS:SetTopicAttributes",
        "SNS:DeleteTopic",
        "SNS:ListSubscriptionsByTopic",
        "SNS:GetTopicAttributes",
        "SNS:Receive",
        "SNS:AddPermission",
        "SNS:Subscribe",
        "SNS:Publish"
      ],
      "Resource": "${aws_sns_topic.celebrations-activity-feeds.arn}"
    },
    {
      "Sid": "sauron-subscription",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${local.sauron_account}:root"
        ]
      },
      "Action": [
        "SNS:Receive",
        "SNS:Subscribe"
      ],
      "Resource": "${aws_sns_topic.celebrations-activity-feeds.arn}"
    },
    {
      "Sid": "percy-app",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${local.percy_account[var.environment]}:role/${module.service.service_role}"
        ]
      },
      "Action": [
        "SNS:Publish"
      ],
      "Resource": "${aws_sns_topic.celebrations-activity-feeds.arn}"
    },
    {
      "Sid": "percy-app-canary",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${local.percy_account[var.environment]}:role/${module.service.canary_role}"
        ]
      },
      "Action": [
        "SNS:Publish"
      ],
      "Resource": "${aws_sns_topic.celebrations-activity-feeds.arn}"
    }
  ]
}
EOF
}
