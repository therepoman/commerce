locals {
  idempotency_table_name = "idempotency-${var.environment}"
  idempotency_hash_key   = "namespace.transaction_id"
  idempotency_range_key  = "task_name"
}

resource "aws_dynamodb_table" "idempotency_primary" {
  name             = local.idempotency_table_name
  hash_key         = local.idempotency_hash_key
  range_key        = local.idempotency_range_key
  stream_enabled   = true
  billing_mode     = "PAY_PER_REQUEST"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute{
    name = "namespace.transaction_id"
    type = "S"
  }
  attribute{
    name = "task_name"
    type = "S"
  }

  point_in_time_recovery {
    enabled = "true"
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_dynamodb_table" "idempotency_replica" {
  name             = local.idempotency_table_name
  hash_key         = local.idempotency_hash_key
  range_key        = local.idempotency_range_key
  stream_enabled   = true
  billing_mode     = "PAY_PER_REQUEST"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute{
    name = "namespace.transaction_id"
    type = "S"
  }

  attribute{
    name = "task_name"
    type = "S"
  }

  point_in_time_recovery {
    enabled = "true"
  }

  provider = aws.backup
}

resource "aws_dynamodb_global_table" "idempotency_global_table" {
  depends_on = [aws_dynamodb_table.idempotency_primary, aws_dynamodb_table.idempotency_replica]
  name       = local.idempotency_table_name

  replica {
    region_name = var.region
  }

  replica {
    region_name = var.backup_region
  }
}
