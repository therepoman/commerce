locals {
  pubsub-control-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-08c288cf362092729"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-0113de1ff3c8bddf5"
  }

  pubsub-control-prefix = {
    staging    = "darklaunch"
    production = "prod"
  }

  pubsub-control_domain_name = "pubsub-control.twitch.a2z.com"
}

resource "aws_route53_zone" "pubsub-control" {
  name = local.pubsub-control_domain_name

  vpc {
    vpc_id = var.vpc_id
  }

  tags = {
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_vpc_endpoint" "pubsub-control" {
  service_name        = local.pubsub-control-vpce[var.environment]
  security_group_ids  = [var.security_group]
  subnet_ids          = split(",", var.subnets)
  vpc_id              = var.vpc_id
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = false

  tags = {
    Domain      = "${local.pubsub-control-prefix[var.environment]}.${local.pubsub-control_domain_name}"
    Name        = "pubsub-control"
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_route53_record" "pubsub-control-endpoint-record" {
  zone_id = aws_route53_zone.pubsub-control.zone_id
  name    = "${local.pubsub-control-prefix[var.environment]}.${local.pubsub-control_domain_name}"
  type    = "CNAME"
  ttl     = "300"
  records = [lookup(aws_vpc_endpoint.pubsub-control.dns_entry[0], "dns_name")]
}
