module "boost_opportunity_contribution_lambda" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  name            = "boost_opportunity_contribution"
  handler         = "main"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]

  env_vars = {
    ENVIRONMENT = var.environment
  }
}