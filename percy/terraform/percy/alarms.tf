/*
  API Alarms
*/

// GetCreatorAnniversariesByChannelIds
resource "aws_cloudwatch_metric_alarm" "get_creator_anniversaries_by_channel_ids_500_errors" {
  count                     = 1
  alarm_name                = "${var.environment}-GetCreatorAnniversariesByChannelIds-500-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.GetCreatorAnniversariesByChannelIds.500"
  namespace                 = var.name
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "2"
  unit                      = "Count"
  alarm_description         = "GetCreatorAnniversariesByChannelIds API errors"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_creator_anniversaries_by_channel_ids_latency_p90" {
  count                     = 1
  alarm_name                = "${var.environment}-GetCreatorAnniversariesByChannelIds-latency-p90"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetCreatorAnniversariesByChannelIds.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.2"
  unit                      = "Seconds"
  alarm_description         = "GetCreatorAnniversariesByChannelIds API latency p90"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_creator_anniversaries_by_channel_ids_latency_p95" {
  count                     = 1
  alarm_name                = "${var.environment}-GetCreatorAnniversariesByChannelIds-latency-p95"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetCreatorAnniversariesByChannelIds.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p95"
  threshold                 = "0.23"
  unit                      = "Seconds"
  alarm_description         = "GetCreatorAnniversariesByChannelIds API latency p95"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_creator_anniversaries_by_channel_ids_latency_p99" {
  count                     = 1
  alarm_name                = "${var.environment}-GetCreatorAnniversariesByChannelIds-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetCreatorAnniversariesByChannelIds.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.25"
  unit                      = "Seconds"
  alarm_description         = "GetCreatorAnniversariesByChannelIds API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

// GetHypeTrainConfig
// TODO - do we need an alarm for non-500?
resource "aws_cloudwatch_metric_alarm" "get_hype_train_config_errors" {
  count                     = 1
  alarm_name                = "${var.environment}-GetHypeTrainConfig-500-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.GetHypeTrainConfig.500"
  namespace                 = var.name
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "GetHypeTrainConfig API 500 errors"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_hype_train_config_latency_p90" {
  count                     = 1
  alarm_name                = "${var.environment}-GetHypeTrainConfig-latency-p90"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetHypeTrainConfig.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.6"
  unit                      = "Seconds"
  alarm_description         = "GetHypeTrainConfig API latency p90"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_hype_train_config_latency_p99" {
  count                     = 1
  alarm_name                = "${var.environment}-GetHypeTrainConfig-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetHypeTrainConfig.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.8"
  unit                      = "Seconds"
  alarm_description         = "GetHypeTrainConfig API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

// UpdateHypeTrainConfig
// TODO - do we need an alarm for non-500?

resource "aws_cloudwatch_metric_alarm" "update_hype_train_config_errors" {
  count                     = 1
  alarm_name                = "${var.environment}-UpdateHypeTrainConfig-500-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.UpdateHypeTrainConfig.500"
  namespace                 = var.name
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "UpdateHypeTrainConfig API 500 errors"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "update_hype_train_config_latency_p90" {
  count                     = 1
  alarm_name                = "${var.environment}-UpdateHypeTrainConfig-latency-p90"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.UpdateHypeTrainConfig.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.6"
  unit                      = "Seconds"
  alarm_description         = "UpdateHypeTrainConfig API latency p90"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "update_hype_train_config_latency_p99" {
  count                     = 1
  alarm_name                = "${var.environment}-UpdateHypeTrainConfig-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.UpdateHypeTrainConfig.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.8"
  unit                      = "Seconds"
  alarm_description         = "UpdateHypeTrainConfig API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

// GetOngoingHypeTrain
// TODO - do we need an alarm for non-500?

resource "aws_cloudwatch_metric_alarm" "get_ongoing_hype_train_errors" {
  count                     = 1
  alarm_name                = "${var.environment}-GetOngoingHypeTrain-500-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.GetOngoingHypeTrain.500"
  namespace                 = var.name
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1000"
  unit                      = "Count"
  alarm_description         = "GetOngoingHypeTrain API 500 errors"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_ongoing_hype_train_latency_p90" {
  count                     = 1
  alarm_name                = "${var.environment}-GetOngoingHypeTrain-latency-p90"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetOngoingHypeTrain.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.3"
  unit                      = "Seconds"
  alarm_description         = "GetOngoingHypeTrain API latency p90"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_ongoing_hype_train_latency_p99" {
  count                     = 1
  alarm_name                = "${var.environment}-GetOngoingHypeTrain-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetOngoingHypeTrain.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "GetOngoingHypeTrain API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

// UpsertHypeTrain
resource "aws_cloudwatch_metric_alarm" "upsert_hype_train_errors" {
  count                     = 1
  alarm_name                = "${var.environment}-UpsertHypeTrain-500-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.UpsertHypeTrain.500"
  namespace                 = var.name
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  unit                      = "Count"
  alarm_description         = "UpsertHypeTrain API 500 errors"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "upsert_hype_train_latency_p90" {
  count                     = 1
  alarm_name                = "${var.environment}-UpsertHypeTrain-latency-p90"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.UpsertHypeTrain.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.6"
  unit                      = "Seconds"
  alarm_description         = "UpsertHypeTrain API latency p90"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "upsert_hype_train_latency_p99" {
  count                     = 1
  alarm_name                = "${var.environment}-UpsertHypeTrain-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.UpsertHypeTrain.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.8"
  unit                      = "Seconds"
  alarm_description         = "UpsertHypeTrain API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

// CanStartHypeTrain
// TODO - do we need an alarm for non-500?
resource "aws_cloudwatch_metric_alarm" "can_start_hype_train_errors" {
  count                     = 1
  alarm_name                = "${var.environment}-CanStartHypeTrain-500-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.CanStartHypeTrain.500"
  namespace                 = var.name
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  unit                      = "Count"
  alarm_description         = "CanStartHypeTrain API 500 errors"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "can_start_hype_train_latency_p50" {
  count                     = 1
  alarm_name                = "${var.environment}-CanStartHypeTrain-latency-p50"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.CanStartHypeTrain.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p50"
  threshold                 = "0.4"
  unit                      = "Seconds"
  alarm_description         = "CanStartHypeTrain API latency p50"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "can_start_hype_train_latency_p90" {
  count                     = 1
  alarm_name                = "${var.environment}-CanStartHypeTrain-latency-p90"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.CanStartHypeTrain.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.6"
  unit                      = "Seconds"
  alarm_description         = "CanStartHypeTrain API latency p90"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "can_start_hype_train_latency_p99" {
  count                     = 1
  alarm_name                = "${var.environment}-CanStartHypeTrain-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.CanStartHypeTrain.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.8"
  unit                      = "Seconds"
  alarm_description         = "CanStartHypeTrain API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

/*
  Dynamo Alarms
*/
resource "aws_cloudwatch_metric_alarm" "hype_train_throttled_read_events" {
  count                     = 1
  alarm_name                = "${var.environment}-hype-train-read-throttles"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "ReadThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "50"
  unit                      = "Count"
  alarm_description         = "Hype Train DynamoDB table reads are being throttled"
  insufficient_data_actions = []

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    TableName = "hype-train-${var.environment}"
  }
}

resource "aws_cloudwatch_metric_alarm" "hype_train_throttled_write_events" {
  count                     = 1
  alarm_name                = "${var.environment}-hype-train-write-throttles"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "WriteThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "50"
  unit                      = "Count"
  alarm_description         = "Hype Train DynamoDB table writes are being throttled"
  insufficient_data_actions = []

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    TableName = "hype-train-${var.environment}"
  }
}

/*
  Logscan Alarms
*/

// Errors
resource "aws_cloudwatch_metric_alarm" "percy_error_alert" {
  count                     = 1
  alarm_name                = "${var.environment}-percy-logscan-error-alert"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "7"
  metric_name               = "percy-logscan-errors"
  namespace                 = var.name
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "50"
  alarm_description         = "High volume of error logs"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]
}

// Panics
resource "aws_cloudwatch_metric_alarm" "percy_panic_alert" {
  count                     = 1
  alarm_name                = "${var.environment}-percy-logscan-panic-alert"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  metric_name               = "percy-logscan-panics"
  namespace                 = var.name
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "1"
  alarm_description         = "Low volume of panic logs"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]
}

/*
SQS Alarms
*/
resource "aws_cloudwatch_metric_alarm" "channel_ban_events_dlq_count_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-sqs-channel-ban-events-dlq-count-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "Channel Ban Events DLQ has at least 1 message"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    QueueName = "channel-ban-events-${var.environment}-deadletter"
  }
}

resource "aws_cloudwatch_metric_alarm" "eventbus_percy_dlq_count" {
  count                     = 1
  alarm_name                = "${var.environment}-sqs-eventbus-percy-dlq-count"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "20"
  unit                      = "Count"
  alarm_description         = "Eventbus Events DLQ count is high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    QueueName = "eventbus-percy-${var.environment}-deadletter"
  }
}

resource "aws_cloudwatch_metric_alarm" "eventbus_percy_dlq_count_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-sqs-eventbus-percy-dlq-count-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "Eventbus Events DLQ has at least 1 message"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    QueueName = "eventbus-percy-${var.environment}-deadletter"
  }
}

resource "aws_cloudwatch_metric_alarm" "expired_hypetrain_notifications_dlq_count" {
  count                     = 1
  alarm_name                = "${var.environment}-sqs-expired-hypetrain-notifications-dlq-count"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "20"
  unit                      = "Count"
  alarm_description         = "Expired Hypetrain Notification Events DLQ count is high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    QueueName = "expired-hypetrain-notifications-${var.environment}-DLQ"
  }
}

resource "aws_cloudwatch_metric_alarm" "expired_hypetrain_notifications_dlq_count_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-sqs-expired-hypetrain-notifications-dlq-count-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "Expired Hypetrain Notification Events DLQ has at least 1 message"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    QueueName = "expired-hypetrain-notifications-${var.environment}-DLQ"
  }
}

resource "aws_cloudwatch_metric_alarm" "pantheon_notifications_dlq_count" {
  count                     = 1
  alarm_name                = "${var.environment}-sqs-pantheon-notifications-dlq-count"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "20"
  unit                      = "Count"
  alarm_description         = "Pantheon Notifications Events DLQ count is high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    QueueName = "pantheon-notifications-${var.environment}-deadletter"
  }
}

resource "aws_cloudwatch_metric_alarm" "pantheon_notifications_dlq_count_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-sqs-pantheon-notifications-dlq-count-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "Pantheon Notifications Events DLQ has at least 1 message"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    QueueName = "pantheon-notifications-${var.environment}-deadletter"
  }
}

resource "aws_cloudwatch_metric_alarm" "expired_hypetrain_notifications_volume_drop" {
  count                     = 1
  alarm_name                = "${var.environment}-expired-hypetrain-notifications-volume-drop"
  comparison_operator       = "LessThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "NumberOfMessagesReceived"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "0"
  unit                      = "Count"
  alarm_description         = "Drop in expired hype train notifications received from Destiny"
  insufficient_data_actions = []
  treat_missing_data        = "breaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    QueueName = "expired-hypetrain-notifications-${var.environment}"
  }
}

resource "aws_cloudwatch_metric_alarm" "pdms_dlq_count_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-sqs-pdms-dlq-count-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "PDMS DLQ has at least 1 message"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    QueueName = "eventbus-percy-user-deletion-queue-dlq"
  }
}

// Glue job failure alarm
resource "aws_cloudwatch_metric_alarm" "glue_job_dbexport_error" {
  count                     = 1
  alarm_name                = "${var.environment}-glue-job-dbexport-error"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "[LowSev] DB export Glue job failure"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    QueueName = "glue-job-dbexport-error-${var.environment}"
  }
}

// Paid Boost Opportunity Stream up event DLQ alarm
resource "aws_cloudwatch_metric_alarm" "eventbus_stream_up_queue_dlq_low_count" {
  count                     = 1
  alarm_name                = "${var.environment}-sqs-eventbus-stream-up-queue-dlq-low-count"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "Eventbus Stream Up Events DLQ not empty"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    QueueName = "eventbus-percy-stream-up-queue-${var.environment}-dlq"
  }
}

resource "aws_cloudwatch_metric_alarm" "eventbus_stream_up_queue_dlq_high_count" {
  count                     = 1
  alarm_name                = "${var.environment}-sqs-eventbus-stream-up-queue-dlq-high-count"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "Eventbus Stream Up Events DLQ high count alarm"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    QueueName = "eventbus-percy-stream-up-queue-${var.environment}-dlq"
  }
}

// Paid Boost Opportunity Kickoff DLQ alarm
resource "aws_cloudwatch_metric_alarm" "boost_opportunity_kickoff_notifications_dlq_low_count" {
  count                     = 1
  alarm_name                = "${var.environment}-sqs-boost-opportunity-kickoff-notifications-dlq-low-count"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "Boost Opportunity Kickoff Notification Events DLQ not empty"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    QueueName = "boost-opportunity-kickoff-notifications-${var.environment}-deadletter"
  }
}

resource "aws_cloudwatch_metric_alarm" "boost_opportunity_kickoff_notifications_dlq_high_count" {
  count                     = 1
  alarm_name                = "${var.environment}-sqs-boost-opportunity-kickoff-notifications-dlq-high-count"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "Boost Opportunity Kickoff Notification Events DLQ high count alarm"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    QueueName = "boost-opportunity-kickoff-notifications-${var.environment}-deadletter"
  }
}

// Paid Boost Opportunity Expired DLQ alarm
resource "aws_cloudwatch_metric_alarm" "boost_opportunity_expired_notifications_dlq_low_count" {
  count                     = 1
  alarm_name                = "${var.environment}-sqs-boost-opportunity-expired-notifications-dlq-low-count"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "Boost Opportunity Expired Notification Events DLQ not empty"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    QueueName = "boost-opportunity-expired-notifications-${var.environment}-deadletter"
  }
}

resource "aws_cloudwatch_metric_alarm" "boost_opportunity_expired_notifications_dlq_high_count" {
  count                     = 1
  alarm_name                = "${var.environment}-sqs-boost-opportunity-expired-notifications-dlq-high-count"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  datapoints_to_alarm       = "1"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "Boost Opportunity Expired Notification Events DLQ high count alarm"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    QueueName = "boost-opportunity-expired-notifications-${var.environment}-deadletter"
  }
}

// GetActivePaidBoostOpportunity Alarms
module "get_active_paid_boost_opportunity_availability_drop" {
  source                     = "./modules/alarms/api_availability"
  api_name                   = "GetActivePaidBoostOpportunity"
  low_watermark              = "0.95"
  requests_metric_name_total = "counter.twirp.GetActivePaidBoostOpportunity.requests"
  requests_metric_name_5xx   = "counter.twirp.status_codes.GetActivePaidBoostOpportunity.500"
  pager_duty_sns_arn         = aws_sns_topic.pager-duty-sns.arn
  environment                = var.environment
}

resource "aws_cloudwatch_metric_alarm" "get_active_paid_boost_opportunity_p99" {
  count                     = 1
  alarm_name                = "${var.environment}-GetActivePaidBoostOpportunity-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetActivePaidBoostOpportunity.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "1"
  unit                      = "Seconds"
  alarm_description         = "GetActivePaidBoostOpportunity API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

// Active Paid Boost Opportunity IsEligible Alarms
module "is_eligible_availability_drop" {
  source                     = "./modules/alarms/api_availability"
  api_name                   = "IsEligible"
  low_watermark              = "0.95"
  requests_metric_name_total = "counter.twirp.IsEligible.requests"
  requests_metric_name_5xx   = "counter.twirp.status_codes.IsEligible.500"
  pager_duty_sns_arn         = aws_sns_topic.pager-duty-sns.arn
  environment                = var.environment
}

resource "aws_cloudwatch_metric_alarm" "is_eligible_p99" {
  count                     = 1
  alarm_name                = "${var.environment}-IsEligible-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.IsEligible.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "1"
  unit                      = "Seconds"
  alarm_description         = "IsEligible API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

// Active Paid Boost Opportunity HandleFulfillment Alarms
module "handle_fulfillment_availability_drop" {
  source                     = "./modules/alarms/api_availability"
  api_name                   = "HandleFulfillment"
  low_watermark              = "0.95"
  requests_metric_name_total = "counter.twirp.HandleFulfillment.requests"
  requests_metric_name_5xx   = "counter.twirp.status_codes.HandleFulfillment.500"
  pager_duty_sns_arn         = aws_sns_topic.pager-duty-sns.arn
  environment                = var.environment
}

resource "aws_cloudwatch_metric_alarm" "handle_fulfillment_p99" {
  count                     = 1
  alarm_name                = "${var.environment}-HandleFulfillment-latency-p99"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.HandleFulfillment.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "1"
  unit                      = "Seconds"
  alarm_description         = "HandleFulfillment API latency p99"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

// Paid Boost Opportunity DB alarms
resource "aws_cloudwatch_metric_alarm" "paid_boost_opportunity_db_throttled_read_events" {
  count                     = 1
  alarm_name                = "${var.environment}-paid-boost-opportunity-db-read-throttles"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "ReadThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "Paid Boost Opportunity DynamoDB table reads are being throttled"
  insufficient_data_actions = []

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    TableName = "paid-boost-opportunity-${var.environment}"
  }
}

resource "aws_cloudwatch_metric_alarm" "paid_boost_opportunity_db_throttled_write_events" {
  count                     = 1
  alarm_name                = "${var.environment}-paid-boost-opportunity-db-write-throttles"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "WriteThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "Paid Boost Opportunity DynamoDB table writes are being throttled"
  insufficient_data_actions = []

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    TableName = "paid-boost-opportunity-${var.environment}"
  }
}

// Paid Boost Purchase Step Function failure
module "paid_boost_opportunity_purchase_sfn_failed" {
  source             = "./modules/alarms/sfn"
  state_machine_name = aws_sfn_state_machine.paid_boost_purchases.name
  state_machine_arn  = aws_sfn_state_machine.paid_boost_purchases.arn
  environment        = var.environment
  pager_duty_sns_arn = aws_sns_topic.pager-duty-sns.arn
}

// CREATOR ANNIVERSARIES BACKEND
resource "aws_cloudwatch_metric_alarm" "creator_anniversaries_throttled_read_events" {
  count                     = 1
  alarm_name                = "${var.environment}-creator-anniversaries-read-throttles"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "ReadThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "Creator Anniversaries Notification DynamoDB table reads are being throttled"
  insufficient_data_actions = []

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    TableName = "creator-anniversaries-notif-${var.environment}"
  }
}

resource "aws_cloudwatch_metric_alarm" "creator_anniversaries_throttled_write_events" {
  count                     = 1
  alarm_name                = "${var.environment}-creator-anniversaries-write-throttles"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "WriteThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "Creator Anniversaries Notification DynamoDB table reads are being throttled"
  insufficient_data_actions = []

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    TableName = "creator-anniversaries-notif-${var.environment}"
  }
}

resource "aws_cloudwatch_metric_alarm" "creator_anniversaries_notification_publish_errors" {
  alarm_name                = "${var.environment}-creator-anniversaries-notification-publish-errors"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "2"
  metric_name               = "Errors"
  namespace                 = "AWS/Lambda"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "Alarms when Creator Anniversaries Notification Publish Lambda fails"
  insufficient_data_actions = []

  actions_enabled = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions   = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions      = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    FunctionName = "creator_anniversaries_notification_publish"
  }
}

