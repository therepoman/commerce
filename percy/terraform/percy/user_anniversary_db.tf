locals {
  user_anniversary_table_name = "user-anniversary-${var.environment}"
  user_anniversary_hash_key   = "user_id-anniversary_years"
}

resource "aws_dynamodb_table" "user_anniversary_primary" {
  name             = local.user_anniversary_table_name
  hash_key         = local.user_anniversary_hash_key
  stream_enabled   = true
  billing_mode     = "PAY_PER_REQUEST"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute{
    name = "user_id-anniversary_years"
    type = "S"
  }

  point_in_time_recovery {
    enabled = "true"
  }

  ttl {
    attribute_name = "ttl"
    enabled        = true
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_dynamodb_table" "user_anniversary_replica" {
  name             = local.user_anniversary_table_name
  hash_key         = local.user_anniversary_hash_key
  stream_enabled   = true
  billing_mode     = "PAY_PER_REQUEST"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute{
    name = "user_id-anniversary_years"
    type = "S"
  }

  ttl {
    attribute_name = "ttl"
    enabled        = true
  }

  point_in_time_recovery {
    enabled = "true"
  }

  provider = aws.backup
}

resource "aws_dynamodb_global_table" "user_anniversary_global_table" {
  depends_on = [aws_dynamodb_table.user_anniversary_primary, aws_dynamodb_table.user_anniversary_replica]
  name       = local.user_anniversary_table_name

  replica {
    region_name = var.region
  }

  replica {
    region_name = var.backup_region
  }
}
