locals {
  everdeen_vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-05fd4d8d40ca27ab4"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-0e3f15bfd3281c3ab"
  }

  everdeen_prefix = {
    staging    = "beta"
    production = "prod"
  }

  everdeen_domain_name = "everdeen.twitch.a2z.com"
}

resource "aws_route53_zone" "everdeen" {
  name = local.everdeen_domain_name

  vpc {
    vpc_id = var.vpc_id
  }

  tags = {
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_vpc_endpoint" "everdeen" {
  service_name        = local.everdeen_vpce[var.environment]
  security_group_ids  = [var.security_group]
  subnet_ids          = split(",", var.subnets)
  vpc_id              = var.vpc_id
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = false

  tags = {
    Domain      = local.everdeen_domain_name
    Name        = var.name
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_route53_record" "everdeen-record" {
  zone_id = aws_route53_zone.everdeen.zone_id
  name    = "${local.everdeen_prefix[var.environment]}.${local.everdeen_domain_name}"
  type    = "CNAME"
  ttl     = "300"
  records = [lookup(aws_vpc_endpoint.everdeen.dns_entry[0], "dns_name")]
}
