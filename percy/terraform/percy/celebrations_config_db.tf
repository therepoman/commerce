locals {
  celebrations_configs_table_name = "celebrations-config-${var.environment}"
  celebrations_configs_hash_key   = "channel_id"
}

resource "aws_dynamodb_table" "celebrations_configs_primary" {
  name             = local.celebrations_configs_table_name
  hash_key         = local.celebrations_configs_hash_key
  stream_enabled   = true
  billing_mode     = "PAY_PER_REQUEST"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "channel_id"
    type = "S"
  }

  point_in_time_recovery {
    enabled = "true"
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_dynamodb_table" "celebrations_configs_replica" {
  name             = local.celebrations_configs_table_name
  hash_key         = local.celebrations_configs_hash_key
  stream_enabled   = true
  billing_mode     = "PAY_PER_REQUEST"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "channel_id"
    type = "S"
  }

  point_in_time_recovery {
    enabled = "true"
  }

  provider = aws.backup
}

resource "aws_dynamodb_global_table" "celebrations_configs_global_table" {
  depends_on = [aws_dynamodb_table.celebrations_configs_primary, aws_dynamodb_table.celebrations_configs_replica]
  name       = local.celebrations_configs_table_name

  replica {
    region_name = var.region
  }

  replica {
    region_name = var.backup_region
  }
}
