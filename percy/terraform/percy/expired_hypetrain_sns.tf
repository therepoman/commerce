// This SNS topic is used by the Destiny service whenever a hype train expires.
// Percy is responsible for writing events to Destiny.
locals {
  percy_account = {
    staging    = "999515204624"
    production = "958761666246"
  }

  destiny_account = {
    staging    = "423302830879"
    production = "389947071127"
  }
}

resource "aws_sns_topic" "expired-hypetrain-notifications" {
  name = "expired-hypetrain-notifications"

  delivery_policy = <<EOF
{
  "http": {
    "defaultHealthyRetryPolicy": {
      "minDelayTarget": 20,
      "maxDelayTarget": 20,
      "numRetries": 3,
      "numMaxDelayRetries": 0,
      "numNoDelayRetries": 0,
      "numMinDelayRetries": 0,
      "backoffFunction": "linear"
    },
    "disableSubscriptionOverrides": false
  }
}
EOF
}

resource "aws_sns_topic_policy" "expired-hypetrain-notifications" {
  arn = aws_sns_topic.expired-hypetrain-notifications.arn

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Id": "subscribers",
  "Statement": [
    {
      "Sid": "root-account",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${local.percy_account[var.environment]}:root"
        ]
      },
      "Action": [
        "SNS:RemovePermission",
        "SNS:SetTopicAttributes",
        "SNS:DeleteTopic",
        "SNS:ListSubscriptionsByTopic",
        "SNS:GetTopicAttributes",
        "SNS:Receive",
        "SNS:AddPermission",
        "SNS:Subscribe"
      ],
      "Resource": "${aws_sns_topic.expired-hypetrain-notifications.arn}"
    },
    {
      "Sid": "subscriptions",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${local.destiny_account[var.environment]}:root"
        ]
      },
      "Action": "SNS:Publish",
      "Resource": "${aws_sns_topic.expired-hypetrain-notifications.arn}"
    }
  ]
}
EOF
}

resource "aws_sqs_queue" "expired-hypetrain-notifications-dlq" {
  name                      = "expired-hypetrain-notifications-${var.environment}-DLQ"
  message_retention_seconds = 1209600
}

resource "aws_sqs_queue" "expired-hypetrain-notifications" {
  name                       = "expired-hypetrain-notifications-${var.environment}"
  receive_wait_time_seconds  = 20
  visibility_timeout_seconds = 20
  redrive_policy             = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.expired-hypetrain-notifications-dlq.arn}\",\"maxReceiveCount\":4}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Id" : "arn:aws:sqs:us-west-2:${local.percy_account[var.environment]}:expired-hypetrain-notifications-${var.environment}/SQSPolicy",
  "Statement": [
    {
      "Sid": "publisher",
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": "SQS:SendMessage",
      "Resource": "arn:aws:sqs:us-west-2:${local.percy_account[var.environment]}:expired-hypetrain-notifications-${var.environment}",
      "Condition": {
        "ForAnyValue:ArnEquals": {
          "aws:SourceArn": "${aws_sns_topic.expired-hypetrain-notifications.arn}"
        }
      }
    },
    {
      "Sid": "consumer",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${local.percy_account[var.environment]}:root"
        ]
      },
      "Action": [
        "sqs:SendMessage",
        "sqs:ReceiveMessage",
        "sqs:ChangeMessageVisibility",
        "sqs:DeleteMessage",
        "sqs:GetQueueAttributes",
        "sqs:GetQueueUrl"
      ],
      "Resource": "arn:aws:sqs:us-west-2:${local.percy_account[var.environment]}:expired-hypetrain-notifications-${var.environment}"
    }
  ]
}
EOF
}

resource "aws_sns_topic_subscription" "expired_hypetrain_notification_sqs_target" {
  topic_arn = aws_sns_topic.expired-hypetrain-notifications.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.expired-hypetrain-notifications.arn
}

resource "aws_iam_policy" "sqs_policy" {
  name        = "${aws_sqs_queue.expired-hypetrain-notifications.name}-sqs-policy"
  description = "Policy to allow access to sqs for expired hypetrain notifications"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "sqs:*"
            ],
            "Effect": "Allow",
            "Resource": "arn:aws:sqs:*:${local.percy_account[var.environment]}:*"
        }
    ]
}
EOF
}

resource "aws_iam_policy_attachment" "sqs_policy_attach" {
  name       = "${aws_sqs_queue.expired-hypetrain-notifications.name}-sqs-attachment"
  roles      = [module.service.service_role, module.service.canary_role]
  policy_arn = aws_iam_policy.sqs_policy.arn
}
