locals {
  badges-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-05cfe1ccfde6ecb1b"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-03a967196cfcb83ac"
  }

  badges-prefix = {
    staging    = "staging"
    production = "prod"
  }

  badges_domain_name = "badges.twitch.a2z.com"
}

resource "aws_route53_zone" "badges" {
  name = local.badges_domain_name

  vpc {
    vpc_id = var.vpc_id
  }

  tags = {
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_vpc_endpoint" "badges" {
  service_name        = local.badges-vpce[var.environment]
  security_group_ids  = [var.security_group]
  subnet_ids          = split(",", var.subnets)
  vpc_id              = var.vpc_id
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = false

  tags = {
    Domain      = "${local.badges-prefix[var.environment]}.${local.badges_domain_name}"
    Name        = "badges"
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_route53_record" "badges-endpoint-record" {
  zone_id = aws_route53_zone.badges.zone_id
  name    = "${local.badges-prefix[var.environment]}.${local.badges_domain_name}"
  type    = "CNAME"
  ttl     = "300"
  records = [lookup(aws_vpc_endpoint.badges.dns_entry[0], "dns_name")]
}
