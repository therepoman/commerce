locals {
  celebrations_channel_purchasable_configs_glue_name = "cep-configs-${var.environment}"
  celebrations_channel_purchasable_configs_table_name = "celebrations-channel-purchasable-configs-${var.environment}"
  celebrations_channel_purchasable_configs_hash_key   = "channel_id"
}

resource "aws_dynamodb_table" "celebrations_channel_purchasable_configs_primary" {
  name             = local.celebrations_channel_purchasable_configs_table_name
  hash_key         = local.celebrations_channel_purchasable_configs_hash_key
  stream_enabled   = true
  billing_mode     = "PAY_PER_REQUEST"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "channel_id"
    type = "S"
  }

  point_in_time_recovery {
    enabled = "true"
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_dynamodb_table" "celebrations_channel_purchasable_configs_replica" {
  name             = local.celebrations_channel_purchasable_configs_table_name
  hash_key         = local.celebrations_channel_purchasable_configs_hash_key
  stream_enabled   = true
  billing_mode     = "PAY_PER_REQUEST"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "channel_id"
    type = "S"
  }

  point_in_time_recovery {
    enabled = "true"
  }

  provider = aws.backup
}

resource "aws_sns_topic" "celebrations_channel_purchasable_configs_db_export_glue_errors" {
  name = "${local.celebrations_channel_purchasable_configs_glue_name}-dbexport-error"
}

module "celebrations_channel_purchasable_configs_glue_dynamo" {
  source = "git::ssh://git@git.xarth.tv/dp/db-s3-glue.git?ref=926a2ddddee44e1d528d7ac241c5dec1f1cddeca"
  database_type = "dynamodb"
  job_name = "${local.celebrations_channel_purchasable_configs_glue_name}-dbexport-job"
  cluster_name = local.celebrations_channel_purchasable_configs_table_name
  table_config = {
    (local.celebrations_channel_purchasable_configs_table_name) = <<EOF
      {
        "worker_count": 1,
        "read_ratio": 0.2,
        "version": 1,
        "spark_optimization": true,
        "schema": [
          {"name": "channel_id", "type": "string", "sensitivity": "userid" },
          {"name": "purchase_config", "type": "string"},
          {"name": "small_is_disabled", "type": "boolean", "is_derived": true},
          {"name": "small_offer_id", "type": "string", "is_derived": true},
          {"name": "medium_is_disabled", "type": "boolean", "is_derived": true},
          {"name": "medium_offer_id", "type": "string", "is_derived": true},
          {"name": "large_is_disabled", "type": "boolean", "is_derived": true},
          {"name": "large_offer_id", "type": "string", "is_derived": true}
        ],
        "output_fields": ["channel_id", "small_is_disabled", "small_offer_id", "medium_is_disabled", "medium_offer_id", "large_is_disabled", "large_offer_id" ]
      }
EOF
  }

  spark_cleaning_code = <<CODE
return (df
    .withColumn('small_is_disabled', F.get_json_object(df.purchase_config, "$.SMALL.is_disabled") == "true")
    .withColumn('medium_is_disabled', F.get_json_object(df.purchase_config, "$.MEDIUM.is_disabled") == "true")
    .withColumn('large_is_disabled', F.get_json_object(df.purchase_config, "$.LARGE.is_disabled") == "true")
    .withColumn('small_offer_id', F.get_json_object(df.purchase_config, "$.SMALL.offer_id"))
    .withColumn('medium_offer_id', F.get_json_object(df.purchase_config, "$.MEDIUM.offer_id"))
    .withColumn('large_offer_id', F.get_json_object(df.purchase_config, "$.LARGE.offer_id"))
)
CODE

  dynamodb_splits_count      = "1"
  create_s3_output_bucket    = 1
  s3_output_bucket           = "${local.celebrations_channel_purchasable_configs_glue_name}-dbexport-job-output-bucket"
  s3_output_key              = "${local.celebrations_channel_purchasable_configs_glue_name}-dbexport-job-output-key"
  create_s3_script_bucket    = 1
  s3_script_bucket           = "${local.celebrations_channel_purchasable_configs_glue_name}-dbexport-job-script-bucket"
  error_sns_topic_name       = aws_sns_topic.celebrations_channel_purchasable_configs_db_export_glue_errors.name
  account_number             = var.account_id
  vpc_id                     = ""
  subnet_id                  = ""
  availability_zone          = ""
  rds_subnet_group           = ""
  cluster_username           = ""
  db_password_parameter_name = ""
  db_password_key_id         = ""
  api_key_parameter_name     = aws_ssm_parameter.tahoe_api_password.name
  api_key_kms_key_id         = aws_kms_alias.tahoe_api.target_key_id
  tahoe_producer_name        = "percy${var.environment}dbexport"
  tahoe_producer_role_arn    = "arn:aws:iam::331582574546:role/producer-percy${var.environment}dbexport"
}

output "s3_kms_key" {
  value = module.celebrations_channel_purchasable_configs_glue_dynamo.s3_kms_key
}

output "s3_output_bucket" {
  value = module.celebrations_channel_purchasable_configs_glue_dynamo.s3_output_bucket
}

output "glue_role" {
  value = module.celebrations_channel_purchasable_configs_glue_dynamo.glue_role
}