locals {
  users-service-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-05dd975d62ccc04fe"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-044e493c84b7dc984"
  }

  users-service-prefix = {
    staging    = "dev"
    production = "prod"
  }

  users-service-domain = "users-service.twitch.a2z.com"
}

resource "aws_route53_zone" "users-service" {
  name = local.users-service-domain

  vpc {
    vpc_id = var.vpc_id
  }

  tags = {
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_vpc_endpoint" "users-service" {
  service_name        = local.users-service-vpce[var.environment]
  security_group_ids  = [var.security_group]
  subnet_ids          = split(",", var.subnets)
  vpc_id              = var.vpc_id
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = false

  tags = {
    Domain      = local.users-service-domain
    Name        = "users-service"
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_route53_record" "users-service" {
  zone_id = aws_route53_zone.users-service.zone_id
  name    = "${local.users-service-prefix[var.environment]}.${local.users-service-domain}"
  type    = "CNAME"
  ttl     = "300"
  records = [lookup(aws_vpc_endpoint.users-service.dns_entry[0], "dns_name")]
}
