locals {
  pubsub-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-0a80bc39593294cc4"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-044f01799c2129051"
  }

  prefix = {
    staging    = "darklaunch"
    production = "prod"
  }

  domain_name = "pubsub-broker.twitch.a2z.com"
}

resource "aws_route53_zone" "main" {
  name = local.domain_name

  vpc {
    vpc_id = var.vpc_id
  }

  tags = {
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_vpc_endpoint" "endpoint" {
  service_name        = local.pubsub-vpce[var.environment]
  security_group_ids  = [var.security_group]
  subnet_ids          = split(",", var.subnets)
  vpc_id              = var.vpc_id
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = false

  tags = {
    Domain      = local.domain_name
    Name        = var.name
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_route53_record" "endpoint-record" {
  zone_id = aws_route53_zone.main.zone_id
  name    = "${local.prefix[var.environment]}.${local.domain_name}"
  type    = "CNAME"
  ttl     = "300"
  records = [lookup(aws_vpc_endpoint.endpoint.dns_entry[0], "dns_name")]
}
