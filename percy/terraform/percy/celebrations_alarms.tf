# Alarms for GetCelebrationConfig
resource "aws_cloudwatch_metric_alarm" "get_celebration_config_errors_critical" {
  count                     = 1
  alarm_name                = "${var.environment}-GetCelebrationConfig-500-errors-critical"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.GetCelebrationConfig.500"
  namespace                 = var.name
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "GetCelebrationConfig API 500 errors is critically high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_celebration_config_errors_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-GetCelebrationConfig-500-errors-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.GetCelebrationConfig.500"
  namespace                 = var.name
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  unit                      = "Count"
  alarm_description         = "GetCelebrationConfig API 500 errors is higher than usual"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_celebration_config_p99_critical" {
  count                     = 1
  alarm_name                = "${var.environment}-GetCelebrationConfig-latency-p99-critical"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetCelebrationConfig.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.8"
  unit                      = "Seconds"
  alarm_description         = "GetCelebrationConfig API latency p99 is critically high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_celebration_config_p99_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-GetCelebrationConfig-latency-p99-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetCelebrationConfig.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "GetCelebrationConfig API latency p99 is higher than usual"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_celebration_config_p90_critical" {
  count                     = 1
  alarm_name                = "${var.environment}-GetCelebrationConfig-latency-p90-critical"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetCelebrationConfig.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "GetCelebrationConfig API latency p90 is critically high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "get_celebration_config_p90_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-GetCelebrationConfig-latency-p90-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.GetCelebrationConfig.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.3"
  unit                      = "Seconds"
  alarm_description         = "GetCelebrationConfig API latency p90 is higher than usual"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

# Alarms for UpdateCelebrationConfig
resource "aws_cloudwatch_metric_alarm" "update_celebration_config_errors_critical" {
  count                     = 1
  alarm_name                = "${var.environment}-UpdateCelebrationConfig-500-errors-critical"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.UpdateCelebrationConfig.500"
  namespace                 = var.name
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "UpdateCelebrationConfig API 500 errors is critically high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "update_celebration_config_errors_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-UpdateCelebrationConfig-500-errors-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.UpdateCelebrationConfig.500"
  namespace                 = var.name
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  unit                      = "Count"
  alarm_description         = "UpdateCelebrationConfig API 500 errors is higher than usual"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "update_celebration_config_p99_critical" {
  count                     = 1
  alarm_name                = "${var.environment}-UpdateCelebrationConfig-latency-p99-critical"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.UpdateCelebrationConfig.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.8"
  unit                      = "Seconds"
  alarm_description         = "UpdateCelebrationConfig API latency p99 is critically high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "update_celebration_config_p99_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-UpdateCelebrationConfig-latency-p99-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.UpdateCelebrationConfig.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "UpdateCelebrationConfig API latency p99 is higher than usual"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "update_celebration_config_p90_critical" {
  count                     = 1
  alarm_name                = "${var.environment}-UpdateCelebrationConfig-latency-p90-critical"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.UpdateCelebrationConfig.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "UpdateCelebrationConfig API latency p90 is critically high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "update_celebration_config_p90_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-UpdateCelebrationConfig-latency-p90-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.UpdateCelebrationConfig.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.3"
  unit                      = "Seconds"
  alarm_description         = "UpdateCelebrationConfig API latency p90 is higher than usual"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

# Alarms for CreateCelebration
resource "aws_cloudwatch_metric_alarm" "create_celebration_errors_critical" {
  count                     = 1
  alarm_name                = "${var.environment}-CreateCelebration-500-errors-critical"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.CreateCelebration.500"
  namespace                 = var.name
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "CreateCelebration API 500 errors is critically high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "create_celebration_errors_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-CreateCelebration-500-errors-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.CreateCelebration.500"
  namespace                 = var.name
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  unit                      = "Count"
  alarm_description         = "CreateCelebration API 500 errors is higher than usual"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "create_celebration_p99_critical" {
  count                     = 1
  alarm_name                = "${var.environment}-CreateCelebration-latency-p99-critical"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.CreateCelebration.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.8"
  unit                      = "Seconds"
  alarm_description         = "CreateCelebration API latency p99 is critically high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "create_celebration_p99_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-CreateCelebration-latency-p99-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.CreateCelebration.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "CreateCelebration API latency p99 is higher than usual"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "create_celebration_p90_critical" {
  count                     = 1
  alarm_name                = "${var.environment}-CreateCelebration-latency-p90-critical"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.CreateCelebration.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "CreateCelebration API latency p90 is critically high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "create_celebration_p90_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-CreateCelebration-latency-p90-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.CreateCelebration.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.3"
  unit                      = "Seconds"
  alarm_description         = "CreateCelebration API latency p90 is higher than usual"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

# Alarms for UpdateCelebration
resource "aws_cloudwatch_metric_alarm" "update_celebration_errors_critical" {
  count                     = 1
  alarm_name                = "${var.environment}-UpdateCelebration-500-errors-critical"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.UpdateCelebration.500"
  namespace                 = var.name
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "UpdateCelebration API 500 errors is critically high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "update_celebration_errors_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-UpdateCelebration-500-errors-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.UpdateCelebration.500"
  namespace                 = var.name
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  unit                      = "Count"
  alarm_description         = "UpdateCelebration API 500 errors is higher than usual"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "update_celebration_p99_critical" {
  count                     = 1
  alarm_name                = "${var.environment}-UpdateCelebration-latency-p99-critical"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.UpdateCelebration.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.8"
  unit                      = "Seconds"
  alarm_description         = "UpdateCelebration API latency p99 is critically high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "update_celebration_p99_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-UpdateCelebration-latency-p99-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.UpdateCelebration.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "UpdateCelebration API latency p99 is higher than usual"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "update_celebration_p90_critical" {
  count                     = 1
  alarm_name                = "${var.environment}-UpdateCelebration-latency-p90-critical"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.UpdateCelebration.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "UpdateCelebration API latency p90 is critically high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "update_celebration_p90_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-UpdateCelebration-latency-p90-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.UpdateCelebration.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.3"
  unit                      = "Seconds"
  alarm_description         = "UpdateCelebration API latency p90 is higher than usual"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

# Alarms for DeleteCelebration
resource "aws_cloudwatch_metric_alarm" "delete_celebration_errors_critical" {
  count                     = 1
  alarm_name                = "${var.environment}-DeleteCelebration-500-errors-critical"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.DeleteCelebration.500"
  namespace                 = var.name
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "DeleteCelebration API 500 errors is critically high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "delete_celebration_errors_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-DeleteCelebration-500-errors-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "counter.twirp.status_codes.DeleteCelebration.500"
  namespace                 = var.name
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "5"
  unit                      = "Count"
  alarm_description         = "DeleteCelebration API 500 errors is higher than usual"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "delete_celebration_p99_critical" {
  count                     = 1
  alarm_name                = "${var.environment}-DeleteCelebration-latency-p99-critical"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.DeleteCelebration.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.8"
  unit                      = "Seconds"
  alarm_description         = "DeleteCelebration API latency p99 is critically high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "delete_celebration_p99_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-DeleteCelebration-latency-p99-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.DeleteCelebration.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "DeleteCelebration API latency p99 is higher than usual"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "delete_celebration_p90_critical" {
  count                     = 1
  alarm_name                = "${var.environment}-DeleteCelebration-latency-p90-critical"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.DeleteCelebration.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "DeleteCelebration API latency p90 is critically high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "delete_celebration_p90_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-DeleteCelebration-latency-p90-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "timing.twirp.DeleteCelebration.response"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.3"
  unit                      = "Seconds"
  alarm_description         = "DeleteCelebration API latency p90 is higher than usual"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

# Dynamo Table Alarms
resource "aws_cloudwatch_metric_alarm" "celebrations_config_throttled_read_events_high" {
  count                     = 1
  alarm_name                = "${var.environment}-celebrations-config-read-throttles-high"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "ReadThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "50"
  unit                      = "Count"
  alarm_description         = "Celebrations Config DynamoDB table reads are being throttled"
  insufficient_data_actions = []

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    TableName = "celebrations-config-${var.environment}"
  }
}

resource "aws_cloudwatch_metric_alarm" "celebrations_config_throttled_read_events_above_zero" {
  count                     = 1
  alarm_name                = "${var.environment}-celebrations-config-read-throttles-above-zero"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "ReadThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "Celebrations Config DynamoDB table reads are being throttled"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    TableName = "celebrations-config-${var.environment}"
  }
}

resource "aws_cloudwatch_metric_alarm" "celebrations_config_throttled_write_events_high" {
  count                     = 1
  alarm_name                = "${var.environment}-celebrations-config-write-throttles-high"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "WriteThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "50"
  unit                      = "Count"
  alarm_description         = "Celebrations Config DynamoDB table writes are being throttled"
  insufficient_data_actions = []

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    TableName = "celebrations-config-${var.environment}"
  }
}

resource "aws_cloudwatch_metric_alarm" "celebrations_config_throttled_write_events_above_zero" {
  count                     = 1
  alarm_name                = "${var.environment}-celebrations-config-write-throttles-above-zero"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "WriteThrottleEvents"
  namespace                 = "AWS/DynamoDB"
  period                    = "60"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "Celebrations Config DynamoDB table writes are being throttled"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    TableName = "celebrations-config-${var.environment}"
  }
}

# SQS Alarms

resource "aws_cloudwatch_metric_alarm" "celebration_events_dlq_count_high" {
  count                     = 1
  alarm_name                = "${var.environment}-sqs-celebration-events-dlq-count-high"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "10"
  unit                      = "Count"
  alarm_description         = "Celebration Events DLQ count is high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    QueueName = "celebrations-eventbus-${var.environment}-deadletter"
  }
}

resource "aws_cloudwatch_metric_alarm" "celebration_events_dlq_count_above_zero" {
  count                     = 1
  alarm_name                = "${var.environment}-sqs-celebration-events-dlq-count-above-zero"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "Celebration Events DLQ count is high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    QueueName = "celebrations-eventbus-${var.environment}-deadletter"
  }
}

resource "aws_cloudwatch_metric_alarm" "celebration_events_unprocessed_queue_size_high" {
  count                     = 1
  alarm_name                = "${var.environment}-sqs-celebration-events-unprocessed-queue-size"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "5"
  datapoints_to_alarm       = "5"
  metric_name               = "ApproximateNumberOfMessagesVisible"
  namespace                 = "AWS/SQS"
  period                    = "300"
  statistic                 = "Maximum"
  threshold                 = "100"
  unit                      = "Count"
  alarm_description         = "Celebration Events Queue has high number of unprocessed events"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    QueueName = "celebrations-eventbus-${var.environment}"
  }
}

# SQS worker
resource "aws_cloudwatch_metric_alarm" "celebration_events_worker_errors_high" {
  count                     = 1
  alarm_name                = "${var.environment}-celebration-events-worker-errors-critical"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "eventbus-celebration-handler-failure"
  namespace                 = var.name
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "100"
  unit                      = "Count"
  alarm_description         = "Celebration events worker error count is critically high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "celebration_events_worker_errors_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-celebration-events-worker-errors-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "eventbus-celebration-handler-failure"
  namespace                 = var.name
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "50"
  unit                      = "Count"
  alarm_description         = "Celebration events worker error count is higher than usual"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "celebration_events_worker_latency_p99_critical" {
  count                     = 1
  alarm_name                = "${var.environment}-celebration-events-worker-latency-p99-critical"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "eventbus-celebration-handler-latency"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.8"
  unit                      = "Seconds"
  alarm_description         = "Celebration events worker latency p99 is critically high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "celebration_events_worker_latency_p99_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-celebration-events-worker-latency-p99-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "eventbus-celebration-handler-latency"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p99"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "Celebration events worker latency p99 is is higher than usual"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "celebration_events_worker_latency_p90_critical" {
  count                     = 1
  alarm_name                = "${var.environment}-celebration-events-worker-latency-p90-critical"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "eventbus-celebration-handler-latency"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.5"
  unit                      = "Seconds"
  alarm_description         = "Celebration events worker latency p90 is critically high"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(aws_sns_topic.pager-duty-sns.arn) > 0 ? true : false
  alarm_actions             = [aws_sns_topic.pager-duty-sns.arn]
  ok_actions                = [aws_sns_topic.pager-duty-sns.arn]

  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

resource "aws_cloudwatch_metric_alarm" "celebration_events_worker_latency_p90_warning" {
  count                     = 1
  alarm_name                = "${var.environment}-celebration-events-worker-latency-p90-warning"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "3"
  datapoints_to_alarm       = "3"
  metric_name               = "eventbus-celebration-handler-latency"
  namespace                 = var.name
  period                    = "300"
  extended_statistic        = "p90"
  threshold                 = "0.3"
  unit                      = "Seconds"
  alarm_description         = "Celebration events worker latency p90 is is higher than usual"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"
  
  dimensions = {
    Region   = var.region
    Service  = var.name
    Stage    = var.environment
    Substage = "primary"
  }
}

// OfferTenant
module "CelebrationsOfferTenant_IsEligible_availability" {
  source                     = "./modules/alarms/api_availability"
  api_name                   = "OfferTenant#IsEligible"
  environment                = var.environment
  low_watermark              = "0.98"
  requests_metric_name_5xx   = "counter.twirp.status_codes.IsEligible.500"
  requests_metric_name_total = "counter.twirp.IsEligible.requests"
  pager_duty_sns_arn = aws_sns_topic.pager-duty-sns.arn
}

module "CelebrationsOfferTenant_HandleFulfillment_availability" {
  source                     = "./modules/alarms/api_availability"
  api_name                   = "OfferTenant#HandleFulfillment"
  environment                = var.environment
  low_watermark              = "0.995"
  traffic_low_watermark      = "5"
  requests_metric_name_5xx   = "counter.twirp.status_codes.HandleFulfillment.500"
  requests_metric_name_total = "counter.twirp.HandleFulfillment.requests"
  pager_duty_sns_arn = aws_sns_topic.pager-duty-sns.arn
}

module "CelebrationsOfferTenant_HandleRevocation_availability" {
  source                     = "./modules/alarms/api_availability"
  api_name                   = "OfferTenant#HandleRevocation"
  environment                = var.environment
  low_watermark              = "0.995"
  traffic_low_watermark      = "5"
  requests_metric_name_5xx   = "counter.twirp.status_codes.HandleRevocation.500"
  requests_metric_name_total = "counter.twirp.HandleRevocation.requests"
  pager_duty_sns_arn = aws_sns_topic.pager-duty-sns.arn
}

// SFNs
module "sfn_celebration_purchase_alarm" {
  source = "./modules/alarms/sfn"

  environment        = var.environment
  state_machine_name = aws_sfn_state_machine.celebration_purchases.name
  state_machine_arn  = aws_sfn_state_machine.celebration_purchases.id
  pager_duty_sns_arn = aws_sns_topic.pager-duty-sns.arn
}

module "sfn_celebration_revoke_alarm" {
  source = "./modules/alarms/sfn"

  environment        = var.environment
  state_machine_name = aws_sfn_state_machine.celebration_revoke.name
  state_machine_arn  = aws_sfn_state_machine.celebration_revoke.id
  pager_duty_sns_arn = aws_sns_topic.pager-duty-sns.arn
}
