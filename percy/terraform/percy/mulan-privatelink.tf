locals {
  mulan_vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-0a8e72c4b8f792c52"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-024bde07637f35efc"
  }

  mulan_prefix = {
    staging    = "beta"
    production = "prod"
  }

  mulan_domain_name = "mulan.twitch.a2z.com"
}

resource "aws_route53_zone" "mulan" {
  name = local.mulan_domain_name

  vpc {
    vpc_id = var.vpc_id
  }

  tags = {
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_vpc_endpoint" "mulan" {
  service_name        = local.mulan_vpce[var.environment]
  security_group_ids  = [var.security_group]
  subnet_ids          = split(",", var.subnets)
  vpc_id              = var.vpc_id
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = false

  tags = {
    Domain      = local.mulan_domain_name
    Name        = var.name
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_route53_record" "mulan-record" {
  zone_id = aws_route53_zone.mulan.zone_id
  name    = "${local.mulan_prefix[var.environment]}.${local.mulan_domain_name}"
  type    = "CNAME"
  ttl     = "300"
  records = [lookup(aws_vpc_endpoint.mulan.dns_entry[0], "dns_name")]
}
