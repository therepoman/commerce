locals {
  product_catalog_vpc_endpoint_service = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-01a69bfd7bd7630a3"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-0e5ccaaa99e0cc6ef"
  }

  product_catalog_endpoint_stage = {
    staging    = "beta"
    production = "prod"
  }
}

module "product_catalog_privatelink" {
  source                    = "git::git+ssh://git@git.xarth.tv/terraform-modules/fulton-cli-privatelinks.git?ref=705fce199c446205ef100f2b71f998e80426d576"
  vpc_endpoint_service_name = local.product_catalog_vpc_endpoint_service[var.environment]
  service_dns_entry         = "us-west-2.${local.product_catalog_endpoint_stage[var.environment]}.twitchproductcatalog.s.twitch.a2z.com"
  vpc_id                    = var.vpc_id
  subnet_ids                = split(",", var.subnets)
  service_name              = "productcatalog"
  security_group            = var.security_group
}
