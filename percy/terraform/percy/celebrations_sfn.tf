resource "aws_iam_role" "step_functions_role" {
  name = "percy_${var.environment}_step_functions_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "states.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "step_functions_policy" {
  name        = "percy-${var.environment}-StepFunctionsPolicy"
  description = "Step function access to invoke Percy Lambda functions"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "lambda:InvokeFunction",
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "step-functions-role-policy-attach" {
  role       = aws_iam_role.step_functions_role.name
  policy_arn = aws_iam_policy.step_functions_policy.arn
}

resource "aws_sfn_state_machine" "celebration_purchases" {
  name     = "percy-${var.environment}-CelebrationPurchases"
  role_arn = aws_iam_role.step_functions_role.arn

  definition = <<EOF
  {
    "Comment": "Handles Purchases of Celebrations",
    "StartAt": "PublishUserNotice",
    "States": {
      "WaitABit": {
        "Type": "Wait",
        "Seconds": 5,
        "Next": "EmitCelebrationPubsub"
      },
      "FailState": {
        "Comment": "Failed execution",
        "Type": "Fail"
      },
      "SuccessState": {
        "Type": "Succeed"
      },
      "PublishUserNotice": ${local.user_notice_step},
      "EmitCelebrationPubsub": ${local.emit_pubsub_step},
      "PostRevenueShare": ${local.post_revenue_step},
      "Datascience": ${local.datascience_step},
      "PublishActivityFeed": ${local.activity_feed_step},
      "RefundPurchase": ${local.refund_purchase_step},
      "RefundPurchaseEmail": ${local.refund_purchase_email_step}
    }
  }
EOF
}

resource "aws_sfn_state_machine" "celebration_revoke" {
  name     = "percy-${var.environment}-CelebrationRevocations"
  role_arn = aws_iam_role.step_functions_role.arn

  definition = <<EOF
  {
    "Comment": "Handles Revocations of Celebrations",
    "StartAt": "ClawbackRevenueShare",
    "States": {
      "FailState": {
        "Comment": "Failed execution",
        "Type": "Fail"
      },
      "SuccessState": {
        "Type": "Succeed"
      },
      "ClawbackRevenueShare": ${local.clawback_revenue_step}
    }
  }
EOF
}

locals {
  default_retry = <<EOF
    [
      {
        "ErrorEquals": [
          "States.ALL"
        ],
        "IntervalSeconds": 5,
        "MaxAttempts": 3,
        "BackoffRate": 3.0
      }
    ]
EOF

  user_notice_step = <<EOF
  {
    "Comment": "Publishes Celebration Purchase User Notice to TMI ",
    "Type": "Task",
    "Next": "WaitABit",
    "Resource": "${module.celebrations_fulfillment_user_notice_lambda.lambda_arn}",
    "ResultPath": "$.userNotice",
    "Catch" : [{
      "ErrorEquals": ["States.ALL"],
      "Next": "WaitABit",
      "ResultPath": "$.userNoticeError"
    }],
    "Retry": ${local.default_retry}
  }
EOF

  emit_pubsub_step = <<EOF
  {
    "Comment": "Publishes Celebration Pubsub",
    "Type": "Task",
    "Next": "PublishActivityFeed",
    "Resource": "${module.celebrations_fulfillment_emit_lambda.lambda_arn}",
    "ResultPath": "$.emitPubsub",
    "Catch" : [{
      "ErrorEquals": ["States.ALL"],
      "Next": "RefundPurchase",
      "ResultPath": "$.emitPubsubError"
    }],
    "Retry": [
      {
        "ErrorEquals": [
          "States.ALL"
        ],
        "IntervalSeconds": 2,
        "MaxAttempts": 3,
        "BackoffRate": 3.0
      }
    ]
  }
EOF

  post_revenue_step = <<EOF
  {
    "Comment": "Publishes revenue to Gringotts",
    "Type": "Task",
    "Next": "Datascience",
    "Resource": "${module.celebrations_fulfillment_payout_lambda.lambda_arn}",
    "ResultPath": "$.postRevenue",
    "Catch" : [{
      "ErrorEquals": ["States.ALL"],
      "Next": "FailState",
      "ResultPath": "$.postRevenueError"
    }],
    "Retry": ${local.default_retry}
  }
EOF

  activity_feed_step = <<EOF
  {
    "Comment": "Publishes Celebration Purchase Activity Feed Item to Sauron",
    "Type": "Task",
    "Next": "PostRevenueShare",
    "Resource": "${module.celebrations_fulfillment_activity_feed_lambda.lambda_arn}",
    "ResultPath": "$.activityFeed",
    "Catch" : [{
      "ErrorEquals": ["States.ALL"],
      "Next": "FailState",
      "ResultPath": "$.activityFeedError"
    }],
    "Retry": ${local.default_retry}
  }
EOF

  datascience_step = <<EOF
  {
    "Comment": "Publishes Celebration Purchase Datascience",
    "Type": "Task",
    "Next": "SuccessState",
    "Resource": "${module.celebrations_fulfillment_datascience_lambda.lambda_arn}",
    "ResultPath": "$.datascience",
    "Catch" : [{
      "ErrorEquals": ["States.ALL"],
      "Next": "FailState",
      "ResultPath": "$.datascienceError"
    }],
    "Retry": ${local.default_retry}
  }
EOF

  refund_purchase_step = <<EOF
  {
    "Comment": "Refunds purchase on failure to publish user notice or emit the celebration",
    "Type": "Task",
    "Next": "RefundPurchaseEmail",
    "Resource": "${module.celebrations_fulfillment_refund_lambda.lambda_arn}",
    "ResultPath": "$.refundPurchase",
    "Catch" : [{
      "ErrorEquals": ["States.ALL"],
      "Next": "FailState",
      "ResultPath": "$.refundPurchaseError"
    }],
    "Retry": ${local.default_retry}
  }
EOF

  refund_purchase_email_step = <<EOF
  {
    "Comment": "sends a refund email to the user upon successful refund of purchase",
    "Type": "Task",
    "Next": "SuccessState",
    "Resource": "${module.celebrations_fulfillment_refund_email_lambda.lambda_arn}",
    "ResultPath": "$.refundPurchaseEmail",
    "Catch" : [{
      "ErrorEquals": ["States.ALL"],
      "Next": "FailState",
      "ResultPath": "$.refundPurchaseEmailError"
    }],
    "Retry": ${local.default_retry}
  }
EOF

  clawback_revenue_step = <<EOF
  {
    "Comment": "Claws back revenue from Gringotts",
    "Type": "Task",
    "Next": "SuccessState",
    "Resource": "${module.celebrations_revocation_clawback_payout_lambda.lambda_arn}",
    "ResultPath": "$.clawbackRevenue",
    "Catch" : [{
      "ErrorEquals": ["States.ALL"],
      "Next": "FailState",
      "ResultPath": "$.clawbackRevenueError"
    }],
    "Retry": ${local.default_retry}
  }
EOF
}
