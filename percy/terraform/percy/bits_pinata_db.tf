locals {
  bits_pinata_table_name = "bits-pinata-${var.environment}"
  bits_pinata_hash_key = "channel_id"
  bits_pinata_range_key = "created_at"

  bits_pinata_ttl_attribute_name = "ttl"
}

resource "aws_dynamodb_table" "bits_pinata_primary" {
  name             = local.bits_pinata_table_name
  billing_mode     = "PAY_PER_REQUEST"
  hash_key         = local.bits_pinata_hash_key
  range_key        = local.bits_pinata_range_key
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "channel_id"
    type = "S"
  }

  attribute {
    name = "created_at"
    type = "S"
  }

  ttl {
    enabled        = true
    attribute_name = local.bits_pinata_ttl_attribute_name
  }

  replica {
    region_name = var.backup_region
  }

  point_in_time_recovery {
    enabled = "true"
  }

  lifecycle {
    prevent_destroy = true
  }
}


