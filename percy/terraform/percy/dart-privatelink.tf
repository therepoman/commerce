locals {
  twitchdartreceiver = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-00cc5f5ddd42126a0"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-0ce6ec756b1a5acba"
  }

  dart_dns = {
    production = "us-west-2.prod.twitchdartreceiver.s.twitch.a2z.com"
    staging    = "us-west-2.beta.twitchdartreceiver.s.twitch.a2z.com"
  }
}

module "twitchdartreceiver-privatelink" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=0afb9b2c9d94728d8c759e2656920fc55edc6299"

  name            = "twitchdartreceiver"
  dns             = local.dart_dns[var.environment]
  endpoint        = local.twitchdartreceiver[var.environment]
  security_groups = [var.security_group]
  subnets         = split(",", var.subnets)
  vpc_id          = var.vpc_id
}
