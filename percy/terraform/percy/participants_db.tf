locals {
  participants_table_name = "participants-${var.environment}"
  participants_hash_key   = "hype_train_id"
  participants_range_key  = "user_id"

  participants_table_min_read = {
    production = 50
    staging    = 10
  }

  participants_table_max_read = {
    production = 20000
    staging    = 20000
  }

  participants_table_min_write = {
    production = 50
    staging    = 10
  }

  participants_table_max_write = {
    production = 20000
    staging    = 20000
  }
}

resource "aws_dynamodb_table" "participants_primary" {
  name             = local.participants_table_name
  billing_mode     = "PAY_PER_REQUEST"
  hash_key         = local.participants_hash_key
  range_key        = local.participants_range_key
  read_capacity    = local.participants_table_min_read[var.environment]
  write_capacity   = local.participants_table_min_write[var.environment]
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "hype_train_id"
    type = "S"
  }

  attribute {
    name = "user_id"
    type = "S"
  }

  ttl {
    enabled        = true
    attribute_name = local.ttl_attribute_name
  }

  point_in_time_recovery {
    enabled = "true"
  }

  lifecycle {
    prevent_destroy = true

    ignore_changes = [
      read_capacity,
      write_capacity,
    ]
  }
}

module "participants_primary_autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"
  kind       = "table"
  table_name = aws_dynamodb_table.participants_primary.name
  min_read   = local.participants_table_min_read[var.environment]
  max_read   = local.participants_table_max_read[var.environment]
  min_write  = local.participants_table_min_write[var.environment]
  max_write  = local.participants_table_max_write[var.environment]
}

resource "aws_dynamodb_table" "participants_replica" {
  name             = local.participants_table_name
  hash_key         = local.participants_hash_key
  range_key        = local.participants_range_key
  read_capacity    = local.participants_table_min_read[var.environment]
  write_capacity   = local.participants_table_min_write[var.environment]
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "hype_train_id"
    type = "S"
  }

  attribute {
    name = "user_id"
    type = "S"
  }

  ttl {
    enabled        = true
    attribute_name = local.ttl_attribute_name
  }

  point_in_time_recovery {
    enabled = "true"
  }

  lifecycle {
    ignore_changes = [
      read_capacity,
      write_capacity,
    ]
  }

  provider = aws.backup
}

module "participants_replica_autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"
  kind       = "table"
  table_name = aws_dynamodb_table.participants_replica.name
  min_read   = local.participants_table_min_read[var.environment]
  max_read   = local.participants_table_max_read[var.environment]
  min_write  = local.participants_table_min_write[var.environment]
  max_write  = local.participants_table_max_write[var.environment]

  providers = {
    aws = aws.backup
  }
}

resource "aws_dynamodb_global_table" "participants_global_table" {
  depends_on = [aws_dynamodb_table.participants_primary, aws_dynamodb_table.participants_replica]
  name       = local.participants_table_name

  replica {
    region_name = var.region
  }

  replica {
    region_name = var.backup_region
  }
}
