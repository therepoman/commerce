data "http" "eventbus_cloudformation" {
  url = "https://eventbus-setup.s3-us-west-2.amazonaws.com/cloudformation.yaml"
}

resource "aws_cloudformation_stack" "eventbus" {
  name          = "EventBus"
  capabilities  = ["CAPABILITY_NAMED_IAM"]
  template_body = data.http.eventbus_cloudformation.body
}

resource "aws_iam_role_policy_attachment" "eventbus_access_attach" {
  role       = module.service.service_role
  policy_arn = aws_cloudformation_stack.eventbus.outputs["EventBusAccessPolicyARN"]
}

resource "aws_iam_role_policy_attachment" "eventbus_access_attach_canary" {
  role       = module.service.canary_role
  policy_arn = aws_cloudformation_stack.eventbus.outputs["EventBusAccessPolicyARN"]
}
