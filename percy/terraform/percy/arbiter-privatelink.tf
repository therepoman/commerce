locals {
  arbiter_vpc_endponit_service = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-0a08eb338daea02ef"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-0cca907dc4744634d"
  }

  arbiter_env_name = {
    staging    = "beta"
    production = "prod"
  }
}

module "arbiter" {
  source                    = "git::git+ssh://git@git.xarth.tv/terraform-modules/fulton-cli-privatelinks.git?ref=705fce199c446205ef100f2b71f998e80426d576"
  vpc_endpoint_service_name = local.arbiter_vpc_endponit_service[var.environment]
  service_dns_entry         = "us-west-2.${local.arbiter_env_name[var.environment]}.twitcharbiter.s.twitch.a2z.com"
  vpc_id                    = var.vpc_id
  subnet_ids                = split(",", var.subnets)
  service_name              = "arbiter"
  security_group            = var.security_group
}
