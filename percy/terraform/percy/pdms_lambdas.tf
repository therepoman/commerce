module "pdms_lambda_role" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda-role?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"
  name   = "percy-pdms"
}

resource aws_iam_role_policy_attachment "pdms_lambda_dynamodb_access" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
  role       = module.pdms_lambda_role.name
}

resource aws_iam_role_policy_attachment "pdms_lambda_sfn_access" {
  policy_arn = "arn:aws:iam::aws:policy/AWSStepFunctionsFullAccess"
  role       = module.pdms_lambda_role.name
}

resource "aws_iam_role_policy_attachment" "pdms_lambda_sqs_access" {
  role       = module.pdms_lambda_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
}

// PDMS lambda role, required to report deletion
// see https://wiki.twitch.com/display/SEC/PDMS-+Delete+pipeline+Service+Onboarding
locals {
  pdms_lambda_caller_role = {
    staging = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Action": "sts:AssumeRole",
      "Resource": [
        "arn:aws:iam::895799599216:role/PDMSLambda-CallerRole-18451FI19HSXT"
      ]
    }
  ]
}
EOF

    production = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Action": "sts:AssumeRole",
      "Resource": [
        "arn:aws:iam::125704643346:role/PDMSLambda-CallerRole-13IIND444YKVR"
      ]
    }
  ]
}
EOF
  }
}

resource "aws_iam_policy" "pdms_lambda_caller_role" {
  name        = "pdms_lambda_caller_role"
  description = "IAM Policy required to assume the PDMS caller role"
  policy      = lookup(local.pdms_lambda_caller_role, var.environment)
}

resource "aws_iam_role_policy_attachment" "pdms_lambda_caller_role_attachment" {
  role       = module.pdms_lambda_role.name
  policy_arn = aws_iam_policy.pdms_lambda_caller_role.arn
}

resource "aws_iam_role_policy_attachment" "pdms_lambda_eventbus_access_attachment" {
  role       = module.pdms_lambda_role.name
  policy_arn = aws_cloudformation_stack.eventbus.outputs["EventBusAccessPolicyARN"]
}

module "pdms_clean_badges_db_lambda" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  name            = "pdms_clean_badges_db"
  handler         = "main"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.pdms_lambda_role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]
  timeout         = "300"

  env_vars = {
    ENVIRONMENT = var.environment
  }
}

module "pdms_clean_conductors_db_lambda" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  name            = "pdms_clean_conductors_db"
  handler         = "main"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.pdms_lambda_role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]
  timeout         = "300"

  env_vars = {
    ENVIRONMENT = var.environment
  }
}

module "pdms_clean_hype_train_configs_db_lambda" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  name            = "pdms_clean_hype_train_configs_db"
  handler         = "main"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.pdms_lambda_role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]
  timeout         = "300"

  env_vars = {
    ENVIRONMENT = var.environment
  }
}

module "pdms_report_deletion_lambda" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  name            = "pdms_report_deletion"
  handler         = "main"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.pdms_lambda_role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]
  timeout         = "30"

  env_vars = {
    ENVIRONMENT = var.environment
  }
}

module "pdms_start_user_deletion_lambda" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  name            = "pdms_start_user_deletion"
  handler         = "main"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.pdms_lambda_role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]
  timeout         = "30"

  env_vars = {
    ENVIRONMENT = var.environment
  }
}

resource "aws_lambda_event_source_mapping" "pdms_start_user_deletion_queue_event_source" {
  event_source_arn = aws_sqs_queue.eventbus_user_deletion_queue.arn
  function_name    = "default-pdms_start_user_deletion"
  batch_size       = 1
}
