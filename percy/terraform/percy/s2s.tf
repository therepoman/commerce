resource aws_iam_policy "malachai_assume_role_policy" {
  name        = "malachai-assume-role-policy"
  description = "policy necessary for S2S"

  policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Action": "sts:AssumeRole",
    "Resource": "arn:aws:iam::180116294062:role/malachai/*"
  }
}
EOT
}
