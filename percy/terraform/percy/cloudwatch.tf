resource "aws_cloudwatch_event_rule" "creator_anniversaries_notification_publish_event" {
  name        = "cron_start_anniversaries_notification_publish_${var.environment}"
  description = "Starts the lambda to publish creator anniversaries notifications"
  is_enabled  = false

  // This is in GMT time, so we need to set it 7 hours ahead
  schedule_expression = "cron(0 18 * * ? *)"
}

resource "aws_cloudwatch_event_target" "creator_anniversaries_notification_publish_target" {
  rule = aws_cloudwatch_event_rule.creator_anniversaries_notification_publish_event.name
  arn  = module.creator_anniversaries_notification_publish_lambda.arn
}
