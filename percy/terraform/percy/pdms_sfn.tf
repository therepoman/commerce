resource "aws_sfn_state_machine" "pdms_user_deletion_v1_sfn_state_machine" {
  name     = "pdms_user_deletion_v1_state_machine"
  role_arn = aws_iam_role.step_functions_role.arn

  definition = <<EOF
  {
    "Comment": "Handles Eventbus UserDestroy event",
    "StartAt": "ParallelDeletes",
    "States": {
      "ParallelDeletes": {
        "Type": "Parallel",
        "Next": "ReportDeletion",
        "ResultPath": "$.parallelOutput",
        "Branches": [
          ${local.pdms_clean_badges_db_step},
          ${local.pdms_clean_conductors_db_step},
          ${local.pdms_clean_hype_train_configs_db_step}
        ],
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "FailState"
        }]
      },
      "ReportDeletion": {
        "Type": "Task",
        "End": true,
        "Resource": "${module.pdms_report_deletion_lambda.lambda_arn}",
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "FailState"
        }],
        "Retry": ${local.default_retry}
      },
      "FailState": {
        "Comment": "Failed execution",
        "Type": "Fail"
      }
    }
  }
EOF
}

locals {
  pdms_clean_badges_db_step = <<EOF
  {
    "StartAt": "CleanBadgesDB",
    "States": {
      "CleanBadgesDB": {
        "Comment": "Clean records from badges DB",
        "Type": "Task",
        "Resource": "${module.pdms_clean_badges_db_lambda.lambda_arn}",
        "ResultPath": "$.cleanBadgesDB",
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "CleanBadgesDBFailState"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "CleanBadgesDBFailState": {
        "Comment": "Failed to clean records claimed in badges DB for user IDs",
        "Type": "Fail"
      }
    }
  }
EOF

  pdms_clean_conductors_db_step = <<EOF
  {
    "StartAt": "CleanConductorsDB",
    "States": {
      "CleanConductorsDB": {
        "Comment": "Clean records from conductors DB",
        "Type": "Task",
        "Resource": "${module.pdms_clean_conductors_db_lambda.lambda_arn}",
        "ResultPath": "$.cleanConductorsDB",
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "CleanConductorsDBFailState"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "CleanConductorsDBFailState": {
        "Comment": "Failed to clean records claimed in conductors DB for user IDs",
        "Type": "Fail"
      }
    }
  }
EOF

  pdms_clean_hype_train_configs_db_step = <<EOF
  {
    "StartAt": "CleanHypeTrainConfigsDB",
    "States": {
      "CleanHypeTrainConfigsDB": {
        "Comment": "Clean records from hype train configs DB",
        "Type": "Task",
        "Resource": "${module.pdms_clean_hype_train_configs_db_lambda.lambda_arn}",
        "ResultPath": "$.cleanHypeTrainConfigsDB",
        "Catch" : [{
          "ErrorEquals": ["States.ALL"],
          "Next": "CleanHypeTrainConfigsDBFailState"
        }],
        "End": true,
        "Retry": ${local.default_retry}
      },
      "CleanHypeTrainConfigsDBFailState": {
        "Comment": "Failed to clean records claimed in hype train configs DB for user IDs",
        "Type": "Fail"
      }
    }
  }
EOF
}