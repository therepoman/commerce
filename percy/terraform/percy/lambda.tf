module "lambda-role" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda-role?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"
  name   = "percy"
}

module "lambda-bucket" {
  source      = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda-s3-bucket?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"
  name        = "percy"
  environment = var.environment
  role_arn    = module.lambda-role.arn
}

resource "aws_iam_role_policy_attachment" "lambda_dynamodb_access" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
  role       = module.lambda-role.name
}

resource "aws_iam_role_policy_attachment" "lambda_sns_access" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSNSFullAccess"
  role       = module.lambda-role.name
}

resource "aws_iam_role_policy_attachment" "lambda_s3_access" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
  role       = module.lambda-role.name
}

resource "aws_iam_role_policy_attachment" "sandstorm_policy_lambda_access" {
  policy_arn = aws_iam_policy.sandstorm_policy.arn
  role       = module.lambda-role.name
}

resource "aws_iam_role_policy_attachment" "lambda_ssm_access" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMFullAccess"
  role       = module.lambda-role.name
}