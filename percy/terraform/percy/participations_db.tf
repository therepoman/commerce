locals {
  participations_table_name = "participations-${var.environment}"
  participations_hash_key   = "channel_id"
  participations_range_key  = "timestamp"

  participations_attributes = [

  ]

  participations_table_min_read = {
    production = 1000
    staging    = 10
  }

  participations_table_max_read = {
    production = 20000
    staging    = 20000
  }

  participations_table_min_write = {
    production = 300
    staging    = 10
  }

  participations_table_max_write = {
    production = 20000
    staging    = 20000
  }
}

resource "aws_dynamodb_table" "participations_primary" {
  name             = local.participations_table_name
  billing_mode     = "PAY_PER_REQUEST"
  hash_key         = local.participations_hash_key
  range_key        = local.participations_range_key
  read_capacity    = local.participations_table_min_read[var.environment]
  write_capacity   = local.participations_table_min_write[var.environment]
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "channel_id"
    type = "S"
  }

  attribute {
    name = "timestamp"
    type = "S"
  }

  point_in_time_recovery {
    enabled = "true"
  }

  ttl {
    enabled        = true
    attribute_name = local.ttl_attribute_name
  }

  lifecycle {
    prevent_destroy = true

    ignore_changes = [
      read_capacity,
      write_capacity,
    ]
  }
}

module "participations_primary_autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"
  kind       = "table"
  table_name = aws_dynamodb_table.participations_primary.name
  min_read   = local.participations_table_min_read[var.environment]
  max_read   = local.participations_table_max_read[var.environment]
  min_write  = local.participations_table_min_write[var.environment]
  max_write  = local.participations_table_max_write[var.environment]
}

resource "aws_dynamodb_table" "participations_replica" {
  name             = local.participations_table_name
  hash_key         = local.participations_hash_key
  range_key        = local.participations_range_key
  read_capacity    = local.participations_table_min_read[var.environment]
  write_capacity   = local.participations_table_min_write[var.environment]
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "channel_id"
    type = "S"
  }

  attribute {
    name = "timestamp"
    type = "S"
  }

  ttl {
    enabled        = true
    attribute_name = local.ttl_attribute_name
  }

  point_in_time_recovery {
    enabled = "true"
  }

  lifecycle {
    ignore_changes = [
      read_capacity,
      write_capacity,
    ]
  }

  provider = aws.backup
}

module "participations_replica_autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"
  kind       = "table"
  table_name = aws_dynamodb_table.participations_replica.name
  min_read   = local.participations_table_min_read[var.environment]
  max_read   = local.participations_table_max_read[var.environment]
  min_write  = local.participations_table_min_write[var.environment]
  max_write  = local.participations_table_max_write[var.environment]

  providers = {
    aws = aws.backup
  }
}

resource "aws_dynamodb_global_table" "participations_global_table" {
  depends_on = [aws_dynamodb_table.participations_primary, aws_dynamodb_table.participations_replica]
  name       = local.participations_table_name

  replica {
    region_name = var.region
  }

  replica {
    region_name = var.backup_region
  }
}
