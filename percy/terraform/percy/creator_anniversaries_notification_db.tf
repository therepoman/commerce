locals {
  creator_anniversaries_table_name = "creator-anniversaries-notif-${var.environment}"
  creator_anniversaries_hash_key = "channel_id_campaign_type"
  creator_anniversaries_ttl_key = "ttl"
  creator_anniversaries_table_min_read = {
    production = 150
    staging = 10
  }

  creator_anniversaries_table_min_write = {
    production = 350
    staging = 10
  }
}

resource "aws_dynamodb_table" "creator_anniversaries_primary" {
  name = local.creator_anniversaries_table_name
  hash_key = local.creator_anniversaries_hash_key
  read_capacity = local.creator_anniversaries_table_min_read[var.environment]
  write_capacity = local.creator_anniversaries_table_min_write[var.environment]
  stream_enabled = true
  billing_mode = "PAY_PER_REQUEST"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "channel_id_campaign_type"
    type = "S"
  }
  ttl {
    attribute_name = local.creator_anniversaries_ttl_key
    enabled = true
  }

  point_in_time_recovery {
    enabled = "true"
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_dynamodb_table" "creator_anniversaries_replica" {
  name = local.creator_anniversaries_table_name
  hash_key = local.creator_anniversaries_hash_key
  read_capacity = local.creator_anniversaries_table_min_read[var.environment]
  write_capacity = local.creator_anniversaries_table_min_write[var.environment]
  stream_enabled = true
  billing_mode = "PAY_PER_REQUEST"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "channel_id_campaign_type"
    type = "S"
  }
  ttl {
    attribute_name = local.creator_anniversaries_ttl_key
    enabled = true
  }

  point_in_time_recovery {
    enabled = "true"
  }

  provider = aws.backup
}

resource "aws_dynamodb_global_table" "creator_anniversaries_global_table" {
  depends_on = [
    aws_dynamodb_table.creator_anniversaries_primary,
    aws_dynamodb_table.creator_anniversaries_replica]
  name = local.creator_anniversaries_table_name

  replica {
    region_name = var.region
  }

  replica {
    region_name = var.backup_region
  }
}
