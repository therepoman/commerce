// This SNS topic is used by the Destiny service whenever a paid boost opportunity should be kicked off.
// Percy is responsible for writing events to Destiny.

resource "aws_sns_topic" "boost-opportunity-kickoff-notifications" {
  name = "boost-opportunity-kickoff-notifications"

  delivery_policy = <<EOF
{
  "http": {
    "defaultHealthyRetryPolicy": {
      "minDelayTarget": 20,
      "maxDelayTarget": 20,
      "numRetries": 3,
      "numMaxDelayRetries": 0,
      "numNoDelayRetries": 0,
      "numMinDelayRetries": 0,
      "backoffFunction": "linear"
    },
    "disableSubscriptionOverrides": false
  }
}
EOF
}

resource "aws_sns_topic_policy" "boost-opportunity-kickoff-notifications" {
  arn = aws_sns_topic.boost-opportunity-kickoff-notifications.arn

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Id": "subscribers",
  "Statement": [
    {
      "Sid": "root-account",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${var.account_id}:root"
        ]
      },
      "Action": [
        "SNS:RemovePermission",
        "SNS:SetTopicAttributes",
        "SNS:DeleteTopic",
        "SNS:ListSubscriptionsByTopic",
        "SNS:GetTopicAttributes",
        "SNS:Receive",
        "SNS:AddPermission",
        "SNS:Subscribe"
      ],
      "Resource": "${aws_sns_topic.boost-opportunity-kickoff-notifications.arn}"
    },
    {
      "Sid": "subscriptions",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${local.destiny_account[var.environment]}:root"
        ]
      },
      "Action": "SNS:Publish",
      "Resource": "${aws_sns_topic.boost-opportunity-kickoff-notifications.arn}"
    }
  ]
}
EOF
}