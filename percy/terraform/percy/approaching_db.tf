locals {
  approaching_table_name = "approaching-${var.environment}"
  approaching_hash_key   = "channel_id"
  approaching_table_min_read = {
    production = 200
    staging    = 10
  }

  approaching_table_max_read = {
    production = 20000
    staging    = 20000
  }

  approaching_table_min_write = {
    production = 150
    staging    = 10
  }

  approaching_table_max_write = {
    production = 20000
    staging    = 20000
  }
}

resource "aws_dynamodb_table" "approaching_primary" {
  name             = local.approaching_table_name
  billing_mode     = "PAY_PER_REQUEST"
  hash_key         = local.approaching_hash_key
  read_capacity    = local.approaching_table_min_read[var.environment]
  write_capacity   = local.approaching_table_min_write[var.environment]
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "channel_id"
    type = "S"
  }

  ttl {
    enabled        = true
    attribute_name = local.ttl_attribute_name
  }

  point_in_time_recovery {
    enabled = "true"
  }

  lifecycle {
    prevent_destroy = true

    ignore_changes = [
      read_capacity,
      write_capacity,
    ]
  }
}

resource "aws_dynamodb_table" "approaching_replica" {
  name             = local.approaching_table_name
  billing_mode     = "PAY_PER_REQUEST"
  hash_key         = local.approaching_hash_key
  read_capacity    = local.approaching_table_min_read[var.environment]
  write_capacity   = local.approaching_table_min_write[var.environment]
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "channel_id"
    type = "S"
  }

  ttl {
    enabled        = true
    attribute_name = local.ttl_attribute_name
  }

  point_in_time_recovery {
    enabled = "true"
  }

  lifecycle {
    ignore_changes = [
      read_capacity,
      write_capacity,
    ]
  }

  provider = aws.backup
}

resource "aws_dynamodb_global_table" "approaching_global_table" {
  depends_on = [aws_dynamodb_table.approaching_primary, aws_dynamodb_table.approaching_replica]
  name       = local.approaching_table_name

  replica {
    region_name = var.region
  }

  replica {
    region_name = var.backup_region
  }
}
