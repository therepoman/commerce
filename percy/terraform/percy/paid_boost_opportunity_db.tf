locals {
  paid_boost_opportunity_table_name = "paid-boost-opportunity-${var.environment}"
  paid_boost_opportunity_hash_key   = "pk" 
  paid_boost_opportunity_range_key  = "sk"
}

resource "aws_dynamodb_table" "paid_boost_opportunity_primary" {
  name             = local.paid_boost_opportunity_table_name
  hash_key         = local.paid_boost_opportunity_hash_key
  range_key        = local.paid_boost_opportunity_range_key
  stream_enabled   = true
  billing_mode     = "PAY_PER_REQUEST"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "pk"
    type = "S"
  }

  attribute {
    name = "sk"
    type = "S"
  }

  ttl {
    attribute_name = local.ttl_attribute_name
    enabled        = true
  }

  point_in_time_recovery {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_dynamodb_table" "paid_boost_opportunity_replica" {
  name             = local.paid_boost_opportunity_table_name
  hash_key         = local.paid_boost_opportunity_hash_key
  range_key        = local.paid_boost_opportunity_range_key
  stream_enabled   = true
  billing_mode     = "PAY_PER_REQUEST"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "pk"
    type = "S"
  }

  attribute {
    name = "sk"
    type = "S"
  }

  ttl {
    attribute_name = "ttl"
    enabled        = true
  }

  point_in_time_recovery {
    enabled = true
  }

  provider = aws.backup
}

resource "aws_dynamodb_global_table" "paid_boost_opportunity_global_table" {
  depends_on = [aws_dynamodb_table.paid_boost_opportunity_primary, aws_dynamodb_table.paid_boost_opportunity_replica]
  name       = local.paid_boost_opportunity_table_name

  replica {
    region_name = var.region
  }

  replica {
    region_name = var.backup_region
  }
}