locals {
  hallpass-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-05c9a9682cec9fa80"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-051cf257eb25dd6d5"
  }

  hallpass-prefix = {
    staging    = "staging"
    production = "prod"
  }

  hallpass_domain_name = "hallpass.cb.twitch.a2z.com"
}

resource "aws_route53_zone" "hallpass" {
  name = local.hallpass_domain_name

  vpc {
    vpc_id = var.vpc_id
  }

  tags = {
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_vpc_endpoint" "hallpass" {
  service_name        = local.hallpass-vpce[var.environment]
  security_group_ids  = [var.security_group]
  subnet_ids          = split(",", var.subnets)
  vpc_id              = var.vpc_id
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = false

  tags = {
    Domain      = "${local.hallpass-prefix[var.environment]}.${local.hallpass_domain_name}"
    Name        = "hallpass"
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_route53_record" "hallpass-endpoint-record" {
  zone_id = aws_route53_zone.hallpass.zone_id
  name    = "${local.hallpass-prefix[var.environment]}.${local.hallpass_domain_name}"
  type    = "CNAME"
  ttl     = "300"
  records = [lookup(aws_vpc_endpoint.hallpass.dns_entry[0], "dns_name")]
}
