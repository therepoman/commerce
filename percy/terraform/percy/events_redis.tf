locals {
  instance = {
    production = "cache.r5.large"
    staging    = "cache.m3.medium"
  }

  node_groups = {
    production = 4
    staging    = 1
  }

  replicas = {
    production = 1
    staging    = 1
  }
}

module "events-redis" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//redis?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  name               = "events-redis"
  replicas_per_group = local.replicas[var.environment]
  node_groups        = local.node_groups[var.environment]

  instance_type   = local.instance[var.environment]
  subnets         = var.subnets
  security_groups = var.security_group
  environment     = var.environment
}
