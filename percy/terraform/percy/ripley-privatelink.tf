locals {
  ripley_vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-0302ab0bc9133e1a4"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-0e9e8a2896b1d5e89"
  }

  ripley_prefix = {
    staging    = "beta"
    production = "prod"
  }

  ripley_domain_name = "ripley.twitch.a2z.com"
}

resource "aws_route53_zone" "ripley" {
  name = local.ripley_domain_name

  vpc {
    vpc_id = var.vpc_id
  }

  tags = {
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_vpc_endpoint" "ripley" {
  service_name        = local.ripley_vpce[var.environment]
  security_group_ids  = [var.security_group]
  subnet_ids          = split(",", var.subnets)
  vpc_id              = var.vpc_id
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = false

  tags = {
    Domain      = local.ripley_domain_name
    Name        = var.name
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_route53_record" "ripley-record" {
  zone_id = aws_route53_zone.ripley.zone_id
  name    = "${local.ripley_prefix[var.environment]}.${local.ripley_domain_name}"
  type    = "CNAME"
  ttl     = "300"
  records = [lookup(aws_vpc_endpoint.ripley.dns_entry[0], "dns_name")]
}
