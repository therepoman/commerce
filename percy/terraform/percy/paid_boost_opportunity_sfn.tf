resource "aws_sfn_state_machine" "paid_boost_purchases" {
  name     = "percy-${var.environment}-BoostOpportunityContributions"
  role_arn = aws_iam_role.step_functions_role.arn

  definition = <<EOF
  {
    "Comment": "Handles Boost Opportunity Contributions ",
    "StartAt": "RecordContribution",
    "States": {
      "FailState": {
        "Comment": "Failed execution",
        "Type": "Fail"
      },
      "SuccessState": {
        "Type": "Succeed"
      },
      "RecordContribution": ${local.boost_contribution_record_contribution_step},
      "PublishContributionUserNotice": ${local.boost_contribution_user_notice_step},
      "EmitContributionPubsub": ${local.boost_contribution_emit_pubsub_step},
      "Datascience": ${local.boost_contribution_datascience_step},
      "RefundPurchase": ${local.boost_contribution_refund_purchase_step}
    }
  }
EOF
}

locals {
  boost_contribution_record_contribution_step = <<EOF
  {
    "Comment": "Count the contribution towards the active boost opportunity ",
    "Type": "Task",
    "Next": "PublishContributionUserNotice",
    "Resource": "${module.boost_opportunity_contribution_lambda.lambda_arn}",
    "ResultPath": "$.recordContribution",
    "Catch" : [{
      "ErrorEquals": ["States.ALL"],
      "Next": "RefundPurchase",
      "ResultPath": "$.recordContributionError"
    }],
    "Retry": ${local.default_retry}
  }
EOF

  boost_contribution_user_notice_step = <<EOF
  {
    "Comment": "Publishes Boost opportunity contritbution User Notice to TMI",
    "Type": "Task",
    "Next": "EmitContributionPubsub",
    "Resource": "${module.celebrations_fulfillment_user_notice_lambda.lambda_arn}",
    "ResultPath": "$.userNotice",
    "Catch" : [{
      "ErrorEquals": ["States.ALL"],
      "Next": "RefundPurchase",
      "ResultPath": "$.userNoticeError"
    }],
    "Retry": ${local.default_retry}
  }
EOF

  boost_contribution_emit_pubsub_step = <<EOF
  {
    "Comment": "Publishes contribution Pubsub",
    "Type": "Task",
    "Next": "Datascience",
    "Resource": "${module.celebrations_fulfillment_emit_lambda.lambda_arn}",
    "ResultPath": "$.emitPubsub",
    "Catch" : [{
      "ErrorEquals": ["States.ALL"],
      "Next": "RefundPurchase",
      "ResultPath": "$.emitPubsubError"
    }],
    "Retry": [
      {
        "ErrorEquals": [
          "States.ALL"
        ],
        "IntervalSeconds": 2,
        "MaxAttempts": 3,
        "BackoffRate": 3.0
      }
    ]
  }
EOF

  boost_contribution_datascience_step = <<EOF
  {
    "Comment": "Publishes Contritbuion Datascience",
    "Type": "Task",
    "Next": "SuccessState",
    "Resource": "${module.celebrations_fulfillment_datascience_lambda.lambda_arn}",
    "ResultPath": "$.datascience",
    "Catch" : [{
      "ErrorEquals": ["States.ALL"],
      "Next": "FailState",
      "ResultPath": "$.datascienceError"
    }],
    "Retry": ${local.default_retry}
  }
EOF

  boost_contribution_refund_purchase_step = <<EOF
  {
    "Comment": "Refunds purchase on failure to fulfill a boost opportunity contribution",
    "Type": "Task",
    "Next": "SuccessState",
    "Resource": "${module.celebrations_fulfillment_refund_lambda.lambda_arn}",
    "ResultPath": "$.refundPurchase",
    "Catch" : [{
      "ErrorEquals": ["States.ALL"],
      "Next": "FailState",
      "ResultPath": "$.refundPurchaseError"
    }],
    "Retry": ${local.default_retry}
  }
EOF
}
