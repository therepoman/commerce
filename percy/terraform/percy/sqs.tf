locals {
  pantheon_sns_arn = {
    staging    = "arn:aws:sns:us-west-2:192960249206:pantheon-staging-events"
    production = "arn:aws:sns:us-west-2:375121467453:pantheon-prod-events"
  }

  clue_sns_arn = {
    staging    = "arn:aws:sns:us-west-2:603200399373:clue_staging_channel_ban_events"
    production = "arn:aws:sns:us-west-2:603200399373:clue_production_channel_ban_events"
  }
}

# Event bus queue
resource "aws_sqs_queue" "eventbus" {
  name                       = "eventbus-percy-${var.environment}"
  policy                     = aws_cloudformation_stack.eventbus.outputs["EventBusSQSPolicyDocument"]
  kms_master_key_id          = aws_cloudformation_stack.eventbus.outputs["EventBusKMSMasterKeyARN"]
  redrive_policy             = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.percy_deadletter.arn}\",\"maxReceiveCount\":10}"
  visibility_timeout_seconds = "30"
}

resource "aws_sqs_queue" "percy_deadletter" {
  name              = "eventbus-percy-${var.environment}-deadletter"
  kms_master_key_id = aws_cloudformation_stack.eventbus.outputs["EventBusKMSMasterKeyARN"]
}

# Celebrations Event bus queue
resource "aws_sqs_queue" "celebrations-eventbus" {
  name                       = "celebrations-eventbus-${var.environment}"
  policy                     = aws_cloudformation_stack.eventbus.outputs["EventBusSQSPolicyDocument"]
  kms_master_key_id          = aws_cloudformation_stack.eventbus.outputs["EventBusKMSMasterKeyARN"]
  redrive_policy             = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.celebrations_deadletter.arn}\",\"maxReceiveCount\":5}"
  visibility_timeout_seconds = "5"
}

resource "aws_sqs_queue" "celebrations_deadletter" {
  name              = "celebrations-eventbus-${var.environment}-deadletter"
  kms_master_key_id = aws_cloudformation_stack.eventbus.outputs["EventBusKMSMasterKeyARN"]
}

# Pantheon notification queue
module "pantheon_notification_queue" {
  source                     = "./modules/sqs"
  allowed_sns_topic          = local.pantheon_sns_arn[var.environment]
  queue_name                 = "pantheon-notifications-${var.environment}"
  visibility_timeout_seconds = "5"
}

resource "aws_sns_topic_subscription" "pantheon_queue_subscription" {
  topic_arn = local.pantheon_sns_arn[var.environment]
  protocol  = "sqs"
  endpoint  = module.pantheon_notification_queue.queue_arn
}

# Channel Ban events queue
module "channel_ban_events_queue" {
  source                     = "./modules/sqs"
  allowed_sns_topic          = local.clue_sns_arn[var.environment]
  queue_name                 = "channel-ban-events-${var.environment}"
  visibility_timeout_seconds = "5"
}

resource "aws_sns_topic_subscription" "channel_ban_events_subscription" {
  topic_arn = local.clue_sns_arn[var.environment]
  protocol  = "sqs"
  endpoint  = module.channel_ban_events_queue.queue_arn
}

# Test participation events queue
module "test_participation_events_queue" {
  source                     = "./modules/sqs"
  queue_name                 = "test-participation-events-${var.environment}"
  visibility_timeout_seconds = "5"
}

module "hype_train_activity_feed_queue" {
  source                     = "./modules/sqs"
  allowed_sns_topic          = aws_sns_topic.hypetrain-activity-feeds.arn
  queue_name                 = "hypetrain-activity-feed-${var.environment}"
  visibility_timeout_seconds = "5"
}

resource "aws_sns_topic_subscription" "hype_train_activity_feed_subscription" {
  topic_arn = aws_sns_topic.hypetrain-activity-feeds.arn
  protocol  = "sqs"
  endpoint  = module.hype_train_activity_feed_queue.queue_arn
}

resource "aws_sqs_queue" "eventbus_stream_up_queue" {
  name                       = "eventbus-percy-stream-up-queue-${var.environment}"
  policy                     = aws_cloudformation_stack.eventbus.outputs["EventBusSQSPolicyDocument"]
  kms_master_key_id          = aws_cloudformation_stack.eventbus.outputs["EventBusKMSMasterKeyARN"]
  redrive_policy             = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.eventbus_stream_up_queue_dlq.arn}\",\"maxReceiveCount\":5}"
  message_retention_seconds  = 1209600
  visibility_timeout_seconds = 60
}

resource "aws_sqs_queue" "eventbus_stream_up_queue_dlq" {
  name                      = "eventbus-percy-stream-up-queue-${var.environment}-dlq"
  message_retention_seconds = 1209600
  kms_master_key_id         = aws_cloudformation_stack.eventbus.outputs["EventBusKMSMasterKeyARN"]
}

module "boost-opportunity-kickoff-notifications" {
  source                     = "./modules/sqs"
  allowed_sns_topic          = aws_sns_topic.boost-opportunity-kickoff-notifications.arn
  queue_name                 = "boost-opportunity-kickoff-notifications-${var.environment}"
  visibility_timeout_seconds = "5"
}

resource "aws_sns_topic_subscription" "boost-opportunity-kickoff-notifications-sqs-subscription" {
  topic_arn = aws_sns_topic.boost-opportunity-kickoff-notifications.arn
  protocol  = "sqs"
  endpoint  = module.boost-opportunity-kickoff-notifications.queue_arn
}

module "boost-opportunity-expired-notifications" {
  source                     = "./modules/sqs"
  allowed_sns_topic          = aws_sns_topic.boost-opportunity-expired-notifications.arn
  queue_name                 = "boost-opportunity-expired-notifications-${var.environment}"
  visibility_timeout_seconds = "5"
}

resource "aws_sns_topic_subscription" "boost-opportunity-expired-notifications-sqs-subscription" {
  topic_arn = aws_sns_topic.boost-opportunity-expired-notifications.arn
  protocol  = "sqs"
  endpoint  = module.boost-opportunity-expired-notifications.queue_arn
}

# Mako Emoticons event bus queue
resource "aws_sqs_queue" "mako_eventbus" {
  name                       = "mako-eventbus-percy-${var.environment}"
  policy                     = aws_cloudformation_stack.eventbus.outputs["EventBusSQSPolicyDocument"]
  kms_master_key_id          = aws_cloudformation_stack.eventbus.outputs["EventBusKMSMasterKeyARN"]
  redrive_policy             = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.mako_eventbus_deadletter.arn}\",\"maxReceiveCount\":5}"
  visibility_timeout_seconds = "5"
}

resource "aws_sqs_queue" "mako_eventbus_deadletter" {
  name              = "mako-eventbus-percy-${var.environment}-deadletter"
  kms_master_key_id = aws_cloudformation_stack.eventbus.outputs["EventBusKMSMasterKeyARN"]
}

resource "aws_sqs_queue" "glue_job_dbexport_error" {
  name = "glue-job-dbexport-error-${var.environment}"
}

resource "aws_sns_topic_subscription" "glue_job_dbexport_error_subscription" {
  topic_arn = aws_sns_topic.celebrations_channel_purchasable_configs_db_export_glue_errors.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.glue_job_dbexport_error.arn
}

resource "aws_sqs_queue_policy" "glue_job_dbexport_error_queue_policy" {
  queue_url = aws_sqs_queue.glue_job_dbexport_error.id
  policy    = data.aws_iam_policy_document.glue_job_dbexport_error_queue_policy_doc.json
}

data "aws_iam_policy_document" "glue_job_dbexport_error_queue_policy_doc" {
  statement {
    sid = "${aws_sqs_queue.glue_job_dbexport_error.name}-Sid"

    actions = ["SQS:SendMessage"]

    principals {
      identifiers = ["*"]
      type        = "AWS"
    }

    resources = [aws_sqs_queue.glue_job_dbexport_error.arn]

    condition {
      test     = "ArnEquals"
      variable = "aws:SourceArn"

      values = [
        aws_sns_topic.celebrations_channel_purchasable_configs_db_export_glue_errors.arn
      ]
    }
  }
}

# Pinata Event bus queue
resource "aws_sqs_queue" "pinata-eventbus" {
  name                       = "pinata-eventbus-${var.environment}"
  policy                     = aws_cloudformation_stack.eventbus.outputs["EventBusSQSPolicyDocument"]
  kms_master_key_id          = aws_cloudformation_stack.eventbus.outputs["EventBusKMSMasterKeyARN"]
  redrive_policy             = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.pinata_deadletter.arn}\",\"maxReceiveCount\":5}"
  visibility_timeout_seconds = "5"
}

resource "aws_sqs_queue" "pinata_deadletter" {
  name              = "pinata-eventbus-${var.environment}-deadletter"
  kms_master_key_id = aws_cloudformation_stack.eventbus.outputs["EventBusKMSMasterKeyARN"]
}