locals {
  celebrations_user_settings_table_name = "celebrations-user-settings-${var.environment}"
  celebrations_user_settings_hash_key   = "user_id"
}

resource "aws_dynamodb_table" "celebrations_user_settings_primary" {
  name             = local.celebrations_user_settings_table_name
  hash_key         = local.celebrations_user_settings_hash_key
  stream_enabled   = true
  billing_mode     = "PAY_PER_REQUEST"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "user_id"
    type = "S"
  }

  point_in_time_recovery {
    enabled = "true"
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_dynamodb_table" "celebrations_user_settings_replica" {
  name             = local.celebrations_user_settings_table_name
  hash_key         = local.celebrations_user_settings_hash_key
  stream_enabled   = true
  billing_mode     = "PAY_PER_REQUEST"
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "user_id"
    type = "S"
  }

  point_in_time_recovery {
    enabled = "true"
  }

  provider = aws.backup
}

resource "aws_dynamodb_global_table" "celebrations_user_settings_global_table" {
  depends_on = [aws_dynamodb_table.celebrations_user_settings_primary, aws_dynamodb_table.celebrations_user_settings_replica]
  name       = local.celebrations_user_settings_table_name

  replica {
    region_name = var.region
  }

  replica {
    region_name = var.backup_region
  }
}
