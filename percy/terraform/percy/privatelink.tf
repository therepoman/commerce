locals {
  materia-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-07bed25bc3a973c25"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-08049722d443d6257"
  }

  destiny-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-04dbf412798031779"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-0677d899a11f66ba4"
  }

  pantheon-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-0e024e12c03495b3e"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-03caeabab7289eff3"
  }

  twitchpersonalization-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-0fae49be5957f8db3"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-0637d9166777dc9da"
  }

  destiny-arn = {
    staging    = "arn:aws:iam::423302830879:root"
    production = "arn:aws:iam::389947071127:root"
  }

  users-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-05dd975d62ccc04fe"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-044e493c84b7dc984"
  }

  arbiter-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-0a08eb338daea02ef"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-0cca907dc4744634d"
  }

  payday-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-019e5066db9348615"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-07a49906f8e043689"
  }

  chat-arn = "arn:aws:iam::603200399373:root"

  admin-panel-arn = {
    staging    = "arn:aws:iam::219087926005:root"
    production = "arn:aws:iam::196915980276:root"
  }

  corleone-arn = {
    staging    = "arn:aws:iam::261425071853:root"
    production = "arn:aws:iam::020901929508:root"
  }

  everdeen-arn = {
    staging    = "arn:aws:iam::589506732331:root"
    production = "arn:aws:iam::316550374861:root"
  }

  gql-arn = {
    staging    = "arn:aws:iam::645130450452:root"
    production = "arn:aws:iam::787149559823:root"
  }

  // Service DNS
  payday_dns = {
    staging    = "main.us-west-2.beta.payday.twitch.a2z.com"
    production = "prod.payday.twitch.a2z.com"
  }
}

module "privatelink" {
  source      = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink?ref=e80ce001454204ae42037e5aef41040ad766f2e0"
  name        = "percy"
  region      = var.region
  environment = var.environment
  alb_dns     = module.service.dns
  cert_arn    = module.privatelink-cert.arn
  vpc_id      = var.vpc_id
  subnets     = split(",", var.subnets)

  whitelisted_arns = [
    local.destiny-arn[var.environment],
    local.chat-arn,
    local.admin-panel-arn[var.environment],
    local.corleone-arn[var.environment],
    local.everdeen-arn[var.environment],
    local.gql-arn[var.environment],
  ]
}

module "privatelink-zone" {
  source      = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-zone?ref=e80ce001454204ae42037e5aef41040ad766f2e0"
  name        = "percy"
  environment = var.environment
}

module "privatelink-cert" {
  source      = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-cert?ref=3e62ec768a05899922e2fef44ef400c22c28f234"
  name        = "percy"
  environment = var.environment
  zone_id     = module.privatelink-zone.zone_id
  subject_alternative_names = ["vpce"]

  alb_dns = module.service.dns
}

module "privatelink-materia" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-service?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name            = "materia"
  endpoint        = local.materia-vpce[var.environment]
  environment     = var.environment
  security_groups = [var.security_group]
  subnets         = split(",", var.subnets)
  vpc_id          = var.vpc_id
}

module "privatelink-destiny" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-service?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name            = "destiny"
  endpoint        = local.destiny-vpce[var.environment]
  environment     = var.environment
  region          = var.region
  security_groups = [var.security_group]
  subnets         = split(",", var.subnets)
  vpc_id          = var.vpc_id
}

module "privatelink-pantheon" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-service?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name            = "pantheon"
  endpoint        = local.pantheon-vpce[var.environment]
  environment     = var.environment
  region          = var.region
  security_groups = [var.security_group]
  subnets         = split(",", var.subnets)
  vpc_id          = var.vpc_id
}

module "privatelink-twitchpersonalization" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-service?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name            = "twitchpersonalization"
  domain          = "s.twitch.a2z.com"
  endpoint        = local.twitchpersonalization-vpce[var.environment]
  environment     = var.environment
  region          = var.region
  security_groups = [var.security_group]
  subnets         = split(",", var.subnets)
  vpc_id          = var.vpc_id
}

module "privatelink-ldap" {
  source          = "git::git+ssh://git@git.xarth.tv/subs/terracode//vpc-endpoint?ref=e80ce001454204ae42037e5aef41040ad766f2e0"
  name            = "ldap-a2z"
  endpoint        = "com.amazonaws.vpce.us-west-2.vpce-svc-0437151f68c61b808"
  security_groups = [var.security_group]
  vpc_id          = var.vpc_id
  subnets         = split(",", var.subnets)
  dns             = "ldap.twitch.a2z.com"
}

module "privatelink-payday" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//privatelink-service?ref=e80ce001454204ae42037e5aef41040ad766f2e0"

  name            = "payday"
  dns             = local.payday_dns[var.environment]
  domain          = "twitch.a2z.com"
  endpoint        = local.payday-vpce[var.environment]
  environment     = var.environment
  security_groups = [var.security_group]
  subnets         = split(",", var.subnets)
  vpc_id          = var.vpc_id
  zone            = "payday.twitch.a2z.com"
}
