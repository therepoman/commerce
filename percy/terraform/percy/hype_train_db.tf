locals {
  hype_train_table_name = "hype-train-${var.environment}"
  hype_train_hash_key   = "channel_id"
  hype_train_range_key  = "instance"

  hype_train_id_index_name     = "hype-train-id-index"
  hype_train_id_index_hash_key = "id"

  ttl_attribute_name = "ttl"

  hype_train_table_min_read = {
    production = 200
    staging    = 10
  }

  hype_train_table_max_read = {
    production = 20000
    staging    = 20000
  }

  hype_train_table_min_write = {
    production = 150
    staging    = 10
  }

  hype_train_table_max_write = {
    production = 20000
    staging    = 20000
  }
}

resource "aws_dynamodb_table" "hype_train_primary" {
  name             = local.hype_train_table_name
  billing_mode     = "PAY_PER_REQUEST"
  hash_key         = local.hype_train_hash_key
  range_key        = local.hype_train_range_key
  read_capacity    = local.hype_train_table_min_read[var.environment]
  write_capacity   = local.hype_train_table_min_write[var.environment]
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "channel_id"
    type = "S"
  }

  attribute {
    name = "instance"
    type = "S"
  }

  attribute {
    name = "id"
    type = "S"
  }

  global_secondary_index {
    name            = local.hype_train_id_index_name
    hash_key        = local.hype_train_id_index_hash_key
    projection_type = "ALL"
  }

  ttl {
    enabled        = true
    attribute_name = local.ttl_attribute_name
  }

  point_in_time_recovery {
    enabled = "true"
  }

  lifecycle {
    prevent_destroy = true

    ignore_changes = [
      read_capacity,
      write_capacity,
    ]
  }
}

module "hype_train_primary_autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"
  kind       = "table"
  table_name = aws_dynamodb_table.hype_train_primary.name
  min_read   = local.hype_train_table_min_read[var.environment]
  max_read   = local.hype_train_table_max_read[var.environment]
  min_write  = local.hype_train_table_min_write[var.environment]
  max_write  = local.hype_train_table_max_write[var.environment]
}

module "hype_train_primary_id_index_autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"
  kind       = "index"
  table_name = aws_dynamodb_table.hype_train_primary.name
  index_name = local.hype_train_id_index_name
  min_read   = local.hype_train_table_min_read[var.environment]
  max_read   = local.hype_train_table_max_read[var.environment]
  min_write  = local.hype_train_table_min_write[var.environment]
  max_write  = local.hype_train_table_max_write[var.environment]
}

resource "aws_dynamodb_table" "hype_train_replica" {
  name             = local.hype_train_table_name
  hash_key         = local.hype_train_hash_key
  range_key        = local.hype_train_range_key
  read_capacity    = local.hype_train_table_min_read[var.environment]
  write_capacity   = local.hype_train_table_min_write[var.environment]
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "channel_id"
    type = "S"
  }
  attribute {
    name = "instance"
    type = "S"
  }
  attribute {
    name = "id"
    type = "S"
  }

  global_secondary_index {
    name            = local.hype_train_id_index_name
    hash_key        = local.hype_train_id_index_hash_key
    read_capacity   = local.hype_train_table_min_read[var.environment]
    write_capacity  = local.hype_train_table_min_write[var.environment]
    projection_type = "ALL"
  }

  ttl {
    enabled        = true
    attribute_name = local.ttl_attribute_name
  }

  point_in_time_recovery {
    enabled = "true"
  }

  lifecycle {
    ignore_changes = [
      read_capacity,
      write_capacity,

    ]
  }

  provider = aws.backup
}

module "hype_train_replica_autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"
  kind       = "table"
  table_name = aws_dynamodb_table.hype_train_replica.name
  min_read   = local.hype_train_table_min_read[var.environment]
  max_read   = local.hype_train_table_max_read[var.environment]
  min_write  = local.hype_train_table_min_write[var.environment]
  max_write  = local.hype_train_table_max_write[var.environment]

  providers = {
    aws = aws.backup
  }
}

module "hype_train_replica_id_index_autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"
  kind       = "index"
  table_name = aws_dynamodb_table.hype_train_replica.name
  index_name = local.hype_train_id_index_name
  min_read   = local.hype_train_table_min_read[var.environment]
  max_read   = local.hype_train_table_max_read[var.environment]
  min_write  = local.hype_train_table_min_write[var.environment]
  max_write  = local.hype_train_table_max_write[var.environment]

  providers = {
    aws = aws.backup
  }
}

resource "aws_dynamodb_global_table" "hype_train_global_table" {
  depends_on = [aws_dynamodb_table.hype_train_primary, aws_dynamodb_table.hype_train_replica]
  name       = local.hype_train_table_name

  replica {
    region_name = var.region
  }

  replica {
    region_name = var.backup_region
  }
}
