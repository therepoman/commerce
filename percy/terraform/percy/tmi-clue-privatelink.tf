locals {
  tmi-vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-0ce468f29514248e6"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-0bc8ad3dc47e31c8f"
  }

  tmi-prefix = {
    staging    = "staging"
    production = "prod"
  }

  tmi-domain = "clue.twitch.a2z.com"
}

resource "aws_route53_zone" "tmi" {
  name = local.tmi-domain

  vpc {
    vpc_id = var.vpc_id
  }

  tags = {
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_vpc_endpoint" "tmi" {
  service_name        = local.tmi-vpce[var.environment]
  security_group_ids  = [var.security_group]
  subnet_ids          = split(",", var.subnets)
  vpc_id              = var.vpc_id
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = false

  tags = {
    Domain      = local.tmi-domain
    Name        = "tmi-clue"
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_route53_record" "tmi" {
  zone_id = aws_route53_zone.tmi.zone_id
  name    = "${local.tmi-prefix[var.environment]}.${local.tmi-domain}"
  type    = "CNAME"
  ttl     = "300"
  records = [lookup(aws_vpc_endpoint.tmi.dns_entry[0], "dns_name")]
}
