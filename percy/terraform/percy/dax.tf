locals {
  dax_cluster_size = {
    production = 6
    staging    = 1
  }

  dax_node_type = {
    production = "dax.r4.large"
    staging    = "dax.t2.small"
  }
}

resource "aws_iam_role_policy" "dax_access_policy" {
  name = "percy-dax-access-policy"
  role = aws_iam_role.dax_iam_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
        {
            "Sid": "DaxAccessPolicy",
            "Effect": "Allow",
            "Action": [
                "dynamodb:*"
            ],
            "Resource": [
                "arn:aws:dynamodb:*:*:table/*"
            ]
        }
  ]
}
EOF
}

resource "aws_iam_role" "dax_iam_role" {
  name = "percy-dax-iam-role"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "dax.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
EOF
}

resource "aws_dax_parameter_group" "dax_param_group" {
  name = "percy-dax-param-group"

  parameters {
    name  = "query-ttl-millis"
    value = "300000"
  }

  parameters {
    name  = "record-ttl-millis"
    value = "300000"
  }
}

resource "aws_dax_subnet_group" "dax_subnet_group" {
  name = "percy-dax-subnet-group"

  subnet_ids = split(",", var.subnets)
}

resource "aws_dax_cluster" "dax_cluster" {
  cluster_name         = "events-${var.environment}"
  iam_role_arn         = aws_iam_role.dax_iam_role.arn
  node_type            = local.dax_node_type[var.environment]
  replication_factor   = local.dax_cluster_size[var.environment]
  security_group_ids   = [var.security_group]
  subnet_group_name    = aws_dax_subnet_group.dax_subnet_group.id
  parameter_group_name = aws_dax_parameter_group.dax_param_group.name

  server_side_encryption {
    enabled = true
  }
}

resource "aws_dax_cluster" "experimental_features_cluster" {
  cluster_name         = "the-sink-${var.environment}"
  iam_role_arn         = aws_iam_role.dax_iam_role.arn
  node_type            = local.dax_node_type[var.environment]
  replication_factor   = local.dax_cluster_size[var.environment]
  security_group_ids   = [var.security_group]
  subnet_group_name    = aws_dax_subnet_group.dax_subnet_group.id
  parameter_group_name = aws_dax_parameter_group.dax_param_group.name

  server_side_encryption {
    enabled = true
  }
}
