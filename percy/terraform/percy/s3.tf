resource "aws_s3_bucket" "experiment_overrides" {
  bucket = "percy-${var.environment}-experiment-overrides"
}

resource "aws_s3_bucket" "partner_anniversaries_s3_bucket" {
  bucket = "percy-${var.environment}-partner-anniversaries-s3-bucket"
}
