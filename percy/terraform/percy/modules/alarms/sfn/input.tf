variable "state_machine_name" {
  type = string
}

variable "state_machine_arn" {
  type = string
}

variable "environment" {
  type = string
}

variable "pager_duty_sns_arn" {
  type    = string
  default = ""
  description = "ARN of the SNS topic set up for Pager Duty alarming"
}
