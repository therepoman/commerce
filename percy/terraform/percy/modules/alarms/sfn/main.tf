resource "aws_cloudwatch_metric_alarm" "state_function_executions_failed_alarm" {
  alarm_name                = "percy-${var.environment}-${var.state_machine_name}-executions-failed-alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  namespace                 = "AWS/States"
  metric_name               = "ExecutionsFailed"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  unit                      = "Count"
  alarm_description         = "${var.state_machine_name} executions failed"
  insufficient_data_actions = []
  treat_missing_data        = "notBreaching"

  actions_enabled           = length(var.pager_duty_sns_arn) > 0 ? true : false
  alarm_actions             = [var.pager_duty_sns_arn]
  ok_actions                = [var.pager_duty_sns_arn]

  dimensions = {
    StateMachineArn = var.state_machine_arn
  }
}
