locals {
  instances = {
    production = 6
    staging    = 1
  }
}

module "cluster" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//cluster?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  name           = "percy-cluster"
  min            = local.instances[var.environment]
  vpc_id         = var.vpc_id
  instance_type  = "c5n.4xlarge"
  environment    = var.environment
  subnets        = var.subnets
  security_group = var.security_group
}


# Requirement from Twitch Security (https://wiki.xarth.tv/display/SEC/AWS+Systems+Manager+Campaign)
resource "aws_iam_role_policy_attachment" "ecs_ssm_policy_attachment" {
  role       = module.cluster.ecs_instance_role_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}