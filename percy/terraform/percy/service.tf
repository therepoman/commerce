locals {
  counts = {
    production = 30
    staging    = 5
  }

  cpu = {
    production = 2048
    staging    = 2048
  }

  memory = {
    production = 2048
    staging    = 2048
  }

  # rollbar_token = "fb765c4062db43debbc9b358828cb3d8"
}

module "service" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//twitch-service?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  # Percy
  name   = lower(var.name)
  repo   = "commerce/percy"
  port   = 3000
  cpu    = local.cpu[var.environment]
  memory = local.memory[var.environment]

  canary_cpu    = local.cpu[var.environment]
  canary_memory = local.memory[var.environment]

  counts = {
    min = local.counts[var.environment]
    max = 500
  }

  env_vars = [
    {
      name  = "ENVIRONMENT"
      value = var.environment
    },
  ]

  cluster                  = var.cluster
  security_group           = var.security_group
  region                   = var.region
  vpc_id                   = var.vpc_id
  environment              = var.environment
  subnets                  = var.subnets
  dns                      = module.privatelink-cert.dns
  secondary_dns            = "vpce.${module.privatelink-cert.dns}"
  debug_enabled            = false
  fargate_platform_version = ""

  cpu_scale_up_threshold = 40
}

// Sandstorm policy enables sandstorm by granting the ability to assume a specifc role
resource "aws_iam_policy" "sandstorm_policy" {
  name = "sandstorm-assume-role-policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Resource": [
        "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/${var.sandstorm_role}"
      ],
      "Effect": "Allow"
    }
  ]
}
EOF
}

// Grant the Instance Profile Sandstorm access
resource "aws_iam_role_policy_attachment" "sandstorm_policy_attachment" {
  policy_arn = aws_iam_policy.sandstorm_policy.arn
  role       = module.service.service_role
}

// Grant the Instance Profile Sandstorm access
resource "aws_iam_role_policy_attachment" "sandstorm_policy_canary_attachment" {
  policy_arn = aws_iam_policy.sandstorm_policy.arn
  role       = module.service.canary_role
}

resource "aws_iam_role_policy_attachment" "dynamodb_access" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
  role       = module.service.service_role
}

resource "aws_iam_role_policy_attachment" "dynamodb_canary_access" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
  role       = module.service.canary_role
}

resource "aws_iam_role_policy_attachment" "percy_s2s_policy_attachment" {
  policy_arn = aws_iam_policy.malachai_assume_role_policy.arn
  role       = module.service.service_role
}

resource "aws_iam_role_policy_attachment" "percy_canary_s2s_policy_attachment" {
  policy_arn = aws_iam_policy.malachai_assume_role_policy.arn
  role       = module.service.canary_role
}

resource "aws_iam_role_policy_attachment" "s3_access" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
  role       = module.service.service_role
}

resource "aws_iam_role_policy_attachment" "s3_canary_access" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
  role       = module.service.canary_role
}

resource "aws_iam_role_policy_attachment" "sfn_access" {
  policy_arn = "arn:aws:iam::aws:policy/AWSStepFunctionsFullAccess"
  role       = module.service.service_role
}

resource "aws_iam_role_policy_attachment" "sfn_canary_access" {
  policy_arn = "arn:aws:iam::aws:policy/AWSStepFunctionsFullAccess"
  role       = module.service.canary_role
}

resource "aws_iam_role_policy_attachment" "ssm_access" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMFullAccess"
  role       = module.service.service_role
}

resource "aws_iam_role_policy_attachment" "ssm_canary_access" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMFullAccess"
  role       = module.service.canary_role
}
