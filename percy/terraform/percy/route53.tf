locals {
  ldap_name = {
    staging    = "ldap.internal.justin.tv"
    production = "ldap.internal.justin.tv"
  }

  ldap_value = {
    staging    = "vpce-0e5fff1353218a709-q6236o53.vpce-svc-02673a2089b1caf6d.us-west-2.vpce.amazonaws.com"
    production = "vpce-03d1291f6117dc6cb-oq9tv069.vpce-svc-02673a2089b1caf6d.us-west-2.vpce.amazonaws.com"
  }

  pantheon_name = {
    staging    = "pantheon-staging.internal.justin.tv"
    production = "pantheon-prod.internal.justin.tv"
  }

  pantheon_value = {
    staging    = "staging-commerce-pantheon-alb-env.hgizn9e8id.us-west-2.elasticbeanstalk.com"
    production = "prod-commerce-pantheon-alb-env.sj5q4jvtba.us-west-2.elasticbeanstalk.com"
  }
}

resource "aws_route53_zone" "internal_justin_tv" {
  name = "internal.justin.tv"

  vpc {
    vpc_id = var.vpc_id
  }

  tags = {
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_route53_record" "ldap" {
  zone_id = aws_route53_zone.internal_justin_tv.zone_id
  name    = local.ldap_name[var.environment]
  type    = "CNAME"
  ttl     = "300"
  records = [local.ldap_value[var.environment]]
}

resource "aws_route53_record" "pantheon" {
  zone_id = aws_route53_zone.internal_justin_tv.zone_id
  name    = local.pantheon_name[var.environment]
  type    = "CNAME"
  ttl     = "300"
  records = [local.pantheon_value[var.environment]]
}
