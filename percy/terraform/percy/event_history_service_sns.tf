locals {
  ehs_account = {
    staging    = "025081377436"
    production = "534808279643"
  }
}

resource "aws_sns_topic" "event-history-service" {
  name = "event-history-service"

  delivery_policy = <<EOF
{
  "http": {
    "defaultHealthyRetryPolicy": {
      "minDelayTarget": 20,
      "maxDelayTarget": 20,
      "numRetries": 3,
      "numMaxDelayRetries": 0,
      "numNoDelayRetries": 0,
      "numMinDelayRetries": 0,
      "backoffFunction": "linear"
    },
    "disableSubscriptionOverrides": false
  }
}
EOF
}

resource "aws_sns_topic_policy" "event-history-service" {
  arn = aws_sns_topic.event-history-service.arn

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Id": "subscribers",
  "Statement": [
    {
      "Sid": "root-account",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${local.percy_account[var.environment]}:root"
        ]
      },
      "Action": [
        "SNS:RemovePermission",
        "SNS:SetTopicAttributes",
        "SNS:DeleteTopic",
        "SNS:ListSubscriptionsByTopic",
        "SNS:GetTopicAttributes",
        "SNS:Receive",
        "SNS:AddPermission",
        "SNS:Subscribe",
        "SNS:Publish"
      ],
      "Resource": "${aws_sns_topic.event-history-service.arn}"
    },
    {
      "Sid": "percy-app",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${local.percy_account[var.environment]}:role/${module.service.service_role}"
        ]
      },
      "Action": [
        "SNS:Publish"
      ],
      "Resource": "${aws_sns_topic.event-history-service.arn}"
    },
    {
      "Sid": "percy-app-canary",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${local.percy_account[var.environment]}:role/${module.service.canary_role}"
        ]
      },
      "Action": [
        "SNS:Publish"
      ],
      "Resource": "${aws_sns_topic.event-history-service.arn}"
    },
    {
      "Sid": "subscriptions",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${local.ehs_account[var.environment]}:root"
        ]
      },
      "Action": [
        "SNS:Subscribe",
        "SNS:Receive"
      ],
      "Resource": "${aws_sns_topic.event-history-service.arn}"
    }
  ]
}
EOF
}
