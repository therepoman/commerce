locals {
  service_name        = "percy"
  canary_service_name = "percy-canary"
}

# Production logscan filters
resource "aws_cloudwatch_log_metric_filter" "logscan_errors" {
  log_group_name = local.service_name
  name           = "${local.service_name}-logscan-errors-filter"
  pattern        = "level=error"

  metric_transformation {
    name          = "${local.service_name}-logscan-errors"
    namespace     = local.service_name
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "logscan_panics" {
  log_group_name = local.service_name
  name           = "${local.service_name}-logscan-panics-filter"
  pattern        = "?\"panic:\" ?panic.go"

  metric_transformation {
    name          = "${local.service_name}-logscan-panics"
    namespace     = local.service_name
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "logscan_warnings" {
  log_group_name = local.service_name
  name           = "${local.service_name}-logscan-warnings-filter"
  pattern        = "level=warn"

  metric_transformation {
    name          = "${local.service_name}-logscan-warnings"
    namespace     = local.service_name
    value         = "1"
    default_value = "0"
  }
}

# Canary logscan filters
resource "aws_cloudwatch_log_metric_filter" "canary_logscan_errors" {
  log_group_name = local.canary_service_name
  name           = "${local.canary_service_name}-logscan-errors-filter"
  pattern        = "level=error"

  metric_transformation {
    name          = "${local.canary_service_name}-logscan-errors"
    namespace     = local.service_name
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "canary_logscan_panics" {
  log_group_name = local.canary_service_name
  name           = "${local.canary_service_name}-logscan-panics-filter"
  pattern        = "?\"panic:\" ?panic.go"

  metric_transformation {
    name          = "${local.canary_service_name}-logscan-panics"
    namespace     = local.service_name
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "canary_logscan_warnings" {
  log_group_name = local.canary_service_name
  name           = "${local.canary_service_name}-logscan-warnings-filter"
  pattern        = "level=warn"

  metric_transformation {
    name          = "${local.canary_service_name}-logscan-warnings"
    namespace     = local.service_name
    value         = "1"
    default_value = "0"
  }
}
