// Jenkins user
resource "aws_iam_user" "tcs_user" {
  name = "percy-${var.environment}-tcs-user"
}

resource "aws_iam_user_policy" "tcs_user_user_policy" {
  name = "jenkins"
  user = aws_iam_user.tcs_user.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Sid": "VisualEditor0",
          "Effect": "Allow",
          "Action": [
            "s3:*",
            "ecr:*",
            "ecs:*",
            "iam:*",
            "logs:*",
            "lambda:*"
          ],
          "Resource": "*"
      }
  ]
}
EOF
}
