module "creator_anniversaries_notification_publish_lambda" {
  source = "git::git+ssh://git@git.xarth.tv/subs/terracode//lambda?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"

  name            = "creator_anniversaries_notification_publish"
  handler         = "main"
  tag             = "latest"
  environment     = var.environment
  bucket          = module.lambda-bucket.bucket
  role_arn        = module.lambda-role.arn
  subnet_ids      = split(",", var.subnets)
  security_groups = [var.security_group]
  memory_size     = 8192
  timeout         = 900

  env_vars = {
    ENVIRONMENT = var.environment
  }
}
