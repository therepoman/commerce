resource "aws_kms_key" "tahoe_api" {
  description = "dynamodb export job tahoe key"
}

resource "aws_kms_alias" "tahoe_api" {
  name          = "alias/${var.environment}/tahoe"
  target_key_id = aws_kms_key.tahoe_api.key_id
}

resource "aws_ssm_parameter" "tahoe_api_password" {
  name        = "/tahoe/petozi${var.environment}/password"
  description = "${var.environment}'s password for the tahoe api"
  type        = "SecureString"
  value       = "CHANGE_ME"
  key_id      = aws_kms_key.tahoe_api.key_id

  lifecycle {
    ignore_changes = [value]
  }

  tags = {
    environment = var.environment
  }
}