locals {
  booster_vpc_endpoint_service = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-04d04e9679eeca99a"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-0d598b6ebd5b379cc"
  }

  booster_endpoint_stage = {
    staging    = "beta"
    production = "prod"
  }
}

module "booster_privatelink" {
  source                    = "git::git+ssh://git@git.xarth.tv/terraform-modules/fulton-cli-privatelinks.git?ref=705fce199c446205ef100f2b71f998e80426d576"
  vpc_endpoint_service_name = local.booster_vpc_endpoint_service[var.environment]
  service_dns_entry         = "us-west-2.${local.booster_endpoint_stage[var.environment]}.twitchpromotionsbooster.s.twitch.a2z.com"
  vpc_id                    = var.vpc_id
  subnet_ids                = split(",", var.subnets)
  service_name              = "booster"
  security_group            = var.security_group
}
