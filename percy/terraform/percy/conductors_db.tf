locals {
  conductors_table_name = "conductors-${var.environment}"
  conductors_bits_conductor_id_index_name = "conductors-bits-conductor-id-index-${var.environment}"
  conductors_subs_conductor_id_index_name = "conductors-subs-conductor-id-index-${var.environment}"
  conductors_hash_key   = "channel_id"
  conductors_bits_conductor_id_index_hash_key = "bits_conductor_id"
  conductors_subs_conductor_id_index_hash_key = "subs_conductor_id"

  conductors_table_min_read = {
    production = 50
    staging    = 10
  }

  conductors_table_max_read = {
    production = 20000
    staging    = 20000
  }

  conductors_table_min_write = {
    production = 50
    staging    = 10
  }

  conductors_table_max_write = {
    production = 20000
    staging    = 20000
  }
}

resource "aws_dynamodb_table" "conductors_primary" {
  name             = local.conductors_table_name
  billing_mode     = "PAY_PER_REQUEST"
  hash_key         = local.conductors_hash_key
  read_capacity    = local.conductors_table_min_read[var.environment]
  write_capacity   = local.conductors_table_min_write[var.environment]
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "channel_id"
    type = "S"
  }

  attribute {
    name = local.conductors_bits_conductor_id_index_hash_key
    type = "S"
  }

  attribute {
    name = local.conductors_subs_conductor_id_index_hash_key
    type = "S"
  }

  global_secondary_index {
    hash_key = local.conductors_bits_conductor_id_index_hash_key
    range_key = local.conductors_hash_key
    name = local.conductors_bits_conductor_id_index_name
    projection_type = "ALL"
  }

  global_secondary_index {
    hash_key = local.conductors_subs_conductor_id_index_hash_key
    range_key = local.conductors_hash_key
    name = local.conductors_subs_conductor_id_index_name
    projection_type = "ALL"
  }

  point_in_time_recovery {
    enabled = "true"
  }

  lifecycle {
    prevent_destroy = true

    ignore_changes = [
      read_capacity,
      write_capacity,
    ]
  }
}

module "conductors_primary_autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"
  kind       = "table"
  table_name = aws_dynamodb_table.conductors_primary.name
  min_read   = local.conductors_table_min_read[var.environment]
  max_read   = local.conductors_table_max_read[var.environment]
  min_write  = local.conductors_table_min_write[var.environment]
  max_write  = local.conductors_table_max_write[var.environment]
}

resource "aws_dynamodb_table" "conductors_replica" {
  name             = local.conductors_table_name
  billing_mode     = "PAY_PER_REQUEST"
  hash_key         = local.conductors_hash_key
  read_capacity    = local.conductors_table_min_read[var.environment]
  write_capacity   = local.conductors_table_min_write[var.environment]
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  attribute {
    name = "channel_id"
    type = "S"
  }

  attribute {
    name = local.conductors_bits_conductor_id_index_hash_key
    type = "S"
  }

  attribute {
    name = local.conductors_subs_conductor_id_index_hash_key
    type = "S"
  }

  global_secondary_index {
    hash_key = local.conductors_bits_conductor_id_index_hash_key
    range_key = local.conductors_hash_key
    name = local.conductors_bits_conductor_id_index_name
    projection_type = "ALL"
  }

  global_secondary_index {
    hash_key = local.conductors_subs_conductor_id_index_hash_key
    range_key = local.conductors_hash_key
    name = local.conductors_subs_conductor_id_index_name
    projection_type = "ALL"
  }

  point_in_time_recovery {
    enabled = "true"
  }

  lifecycle {
    ignore_changes = [
      read_capacity,
      write_capacity,
    ]
  }

  provider = aws.backup
}

module "conductors_replica_autoscaling" {
  source     = "git::git+ssh://git@git.xarth.tv/subs/terracode//dynamo-autoscaling?ref=0621ec0bd0c777d151a2bfbd549b9ea26bb1f73d"
  kind       = "table"
  table_name = aws_dynamodb_table.conductors_replica.name
  min_read   = local.conductors_table_min_read[var.environment]
  max_read   = local.conductors_table_max_read[var.environment]
  min_write  = local.conductors_table_min_write[var.environment]
  max_write  = local.conductors_table_max_write[var.environment]

  providers = {
    aws = aws.backup
  }
}

resource "aws_dynamodb_global_table" "conductors_global_table" {
  depends_on = [aws_dynamodb_table.conductors_primary, aws_dynamodb_table.conductors_replica]
  name       = local.conductors_table_name

  replica {
    region_name = var.region
  }

  replica {
    region_name = var.backup_region
  }
}
