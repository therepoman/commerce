locals {

  liveline_vpce = {
    staging    = "com.amazonaws.vpce.us-west-2.vpce-svc-0e4e8f90d47bca00d"
    production = "com.amazonaws.vpce.us-west-2.vpce-svc-09e5395f949c1bf56"
  }

  liveline_prefix = {
    staging    = "staging"
    production = "production"
  }

  liveline_domain_name = "liveline.twitch.a2z.com"
}

resource "aws_route53_zone" "liveline_route53_zone" {
  name = local.liveline_domain_name

  vpc {
    vpc_id = var.vpc_id
  }

  tags = {
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_vpc_endpoint" "liveline_endpoint" {
  service_name        = local.liveline_vpce[var.environment]
  security_group_ids  = [var.security_group]
  subnet_ids          = split(",", var.subnets)
  vpc_id              = var.vpc_id
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = false

  tags = {
    Domain      = local.liveline_domain_name
    Name        = var.name
    Environment = var.environment
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_route53_record" "liveline_route53_record" {
  zone_id = aws_route53_zone.liveline_route53_zone.zone_id
  name    = "main.${local.liveline_prefix[var.environment]}.${local.liveline_domain_name}"
  type    = "CNAME"
  ttl     = "300"
  records = [lookup(aws_vpc_endpoint.liveline_endpoint.dns_entry[0], "dns_name")]
}
