bucket = "twitch-percy-staging-terracode"

key = "staging.tfstate"

profile = "twitch-percy-dev"

dynamodb_table = "percy-staging-terracode-lock"

sandstorm_role = "percy-staging"
