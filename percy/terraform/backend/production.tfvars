bucket = "twitch-percy-production-terracode"

key = "production.tfstate"

profile = "twitch-percy-prod"

dynamodb_table = "percy-production-terracode-lock"

sandstorm_role = "percy-prod"
