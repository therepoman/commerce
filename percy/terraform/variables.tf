variable "aws_access_key" {
  default = ""
}

variable "aws_secret_key" {
  default = ""
}

variable "aws_profile" {
}

variable "vpc_id" {
}

variable "region" {
}

variable "backup_region" {
}

variable "security_group" {
}

variable "environment" {
}

variable "subnets" {
}

variable "account_id" {}

variable "sandstorm_role" {}

# provider "aws" {
#   region  = "${var.region}"
#   profile = "${var.aws_profile}"
# }
# provider "aws" {
#   alias   = "backup"
#   region  = "${var.backup_region}"
#   profile = "${var.aws_profile}"
# }
