region = "us-west-2"

backup_region = "us-east-2"

environment = "production"

aws_profile = "twitch-percy-aws-prod"

vpc_id = "vpc-0b3b598b81954ab79"

security_group = "sg-062afd30bfaba22d6"

subnets = "subnet-02426c7f532714c03,subnet-036067fa062137a21,subnet-0141571a61b5c992f"

account_id = "958761666246"

sandstorm_role = "percy-prod"
