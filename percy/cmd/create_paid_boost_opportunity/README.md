# README

This script can be used to manually create a new paid boost opportunity for a given channel.

### Usage

Creating a new opportunity for channel 478863946
```
AWS_PROFILE=percy-devo go run main.go -channel=478863946 -duration=3000
```
Keep in mind this writes directly to the DynamoDB table, and the DAX cluster fronting it is not updated. You might have to wait for a few miutes before the service picks up this change.

If you have connectivity to the DAX cluster (e.g. via a jumpbox proxy), you can specify the DAX flag so the write will go through DAX.

```
AWS_PROFILE=percy-devo go run main.go -channel=478863946 -duration=3000 -dax=true
```