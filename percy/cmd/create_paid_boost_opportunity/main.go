package main

import (
	"context"
	"flag"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"github.com/google/uuid"
)

func main() {
	region := flag.String("region", "us-west-2", "AWS account region")
	env := flag.String("env", string(config.Staging), "environment (staging or production)")
	useDax := flag.Bool("dax", false, "Whether we should call through DAX (if you have connnectivity to VPN)")
	channelID := flag.String("channel", "", "Channel to start the boost opportunity for")
	durationSeconds := flag.Int("duration", 300, "Duration of the boost opportunity in seconds")
	flag.Parse()

	if channelID == nil || *channelID == "" {
		logrus.Error("Channel must be specified")
		return
	}

	cfg, err := config.LoadConfig(config.Environment(*env))
	if err != nil {
		logrus.WithError(err).Panic(err, "Failed to load percy config")
	}

	ddbConfig := dynamodb.DynamoDBConfig{
		Region:      *region,
		Environment: *env,
	}

	if *useDax && cfg.Endpoints.DynamoDB != nil {
		ddbConfig.Endpoint = cfg.Endpoints.DynamoDB
	}

	db, err := dynamodb.NewBoostsDB(ddbConfig)
	if err != nil {
		logrus.WithError(err).Panic("Failed to initiated hype train configs DB")
	}

	opportunity := percy.BoostOpportunity{
		ID:        uuid.New().String(),
		ChannelID: *channelID,
		StartedAt: time.Now().UTC(),
		ExpiresAt: time.Now().Add(time.Duration(*durationSeconds) * time.Second).UTC(),
	}

	logrus.Infof("Creating new boost opportunity for channel %s, expires in %d seconds", *channelID, *durationSeconds)

	err = db.CreateBoostOpportunity(context.Background(), opportunity)
	if err != nil {
		logrus.WithError(err).Panic("Failed to seed the DynamoDB table with a new boost opportunity")
	}
}
