package main

import (
	"bufio"
	"context"
	"encoding/csv"
	"errors"
	"flag"
	"io"
	"os"

	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"github.com/sirupsen/logrus"
	"golang.org/x/time/rate"
)

func main() {
	region := flag.String("region", "us-west-2", "AWS account region")
	env := flag.String("env", string(config.Staging), "environment (staging or prod)")
	input := flag.String("input", string("./input.csv"), "input file")
	flag.Parse()

	configsDB, err := dynamodb.NewHypeTrainConfigsDB(dynamodb.DynamoDBConfig{
		Region:      *region,
		Environment: *env,
	})
	if err != nil {
		logrus.WithError(err).Panic("Failed to initiated hype train config DB")
	}

	rateLimiter := rate.NewLimiter(rate.Limit(10), 10)

	inputFile, err := os.Open(*input)
	if err != nil {
		logrus.WithError(err).Panic("Failed to open input file")
	}

	inputReader := csv.NewReader(bufio.NewReader(inputFile))
	count := 0
	for {
		row, err := inputReader.Read()
		if errors.Is(err, io.EOF) {
			break
		}

		if err != nil {
			logrus.WithError(err).Panic("Filed to read channel setting")
		}

		if len(row) != 1 {
			logrus.Panicf("invalid row found %+v", row)
		}

		channelID := row[0]

		err = rateLimiter.Wait(context.Background())
		if err != nil {
			logrus.WithError(err).Panic("failed to rate limit")
		}

		_, err = configsDB.UnsetConfig(context.Background(), channelID, percy.HypeTrainConfigUnset{
			CalloutEmoteID: true,
		})
		if err != nil {
			logrus.WithError(err).Panic("failed to unset channel's custom config")
		}

		count += 1
		if count%10 == 0 {
			logrus.Infof("backfilled %d channels", count)
		}
	}

	logrus.Infof("Done, updated %d channels", count)
}
