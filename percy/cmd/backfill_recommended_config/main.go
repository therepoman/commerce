package main

import (
	"bufio"
	"context"
	"encoding/csv"
	"errors"
	"flag"
	"io"
	"os"

	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"github.com/sirupsen/logrus"
	"golang.org/x/time/rate"
)

type setting struct {
	Threshold  int
	Difficulty percy.Difficulty
}

// Based on content of percentile_settings.csv
// Mapping percentile group to a threshold and difficulty setting
// Difficulty number in the percentile_settings maps to:
// 		1 -> EASY
// 		2 -> MEDIUM
// 		3 -> HARD
// 		4 -> SUPER HARD
// 		5 -> INSANE
var settings = map[string]setting{
	"1":  {20, percy.DifficultySuperHard},
	"2":  {9, percy.DifficultySuperHard},
	"3":  {7, percy.DifficultySuperHard},
	"4":  {6, percy.DifficultySuperHard},
	"5":  {6, percy.DifficultySuperHard},
	"6":  {5, percy.DifficultySuperHard},
	"7":  {4, percy.DifficultyHard},
	"8":  {4, percy.DifficultyHard},
	"9":  {4, percy.DifficultyHard},
	"10": {4, percy.DifficultyMedium},
	"11": {3, percy.DifficultyMedium},
	"12": {3, percy.DifficultyMedium},
	"13": {3, percy.DifficultyMedium},
}

func main() {
	region := flag.String("region", "us-west-2", "AWS account region")
	env := flag.String("env", string(config.Staging), "environment (staging or prod)")
	input := flag.String("input", string("./input.csv"), "input file")
	flag.Parse()

	configsDB, err := dynamodb.NewHypeTrainConfigsDB(dynamodb.DynamoDBConfig{
		Region:      *region,
		Environment: *env,
	})
	if err != nil {
		logrus.WithError(err).Panic("Failed to initiated hype train config DB")
	}

	rateLimiter := rate.NewLimiter(rate.Limit(100), 100)

	inputFile, err := os.Open(*input)
	if err != nil {
		logrus.WithError(err).Panic("Failed to open input file")
	}

	inputReader := csv.NewReader(bufio.NewReader(inputFile))
	count := 0
	for {
		row, err := inputReader.Read()
		if errors.Is(err, io.EOF) {
			break
		}

		if err != nil {
			logrus.WithError(err).Panic("Filed to read channel setting")
		}

		if len(row) != 2 {
			logrus.Panicf("invalid row found %+v", row)
		}

		setting, ok := settings[row[0]]
		if !ok {
			logrus.Warnf("could not find setting for group %s", row[0])
			continue
		}

		channelID := row[1]

		err = rateLimiter.Wait(context.Background())
		if err != nil {
			logrus.WithError(err).Panic("failed to rate limit")
		}
		err = configsDB.UpdateRecommendedConfig(context.Background(), channelID, percy.HypeTrainConfigUpdate{
			NumOfEventsToKickoff: &setting.Threshold,
			Difficulty:           &setting.Difficulty,
		})

		if err != nil {
			logrus.WithError(err).Panic("failed to update channel's recommended config")
		}

		count += 1
		if count%1000 == 0 {
			logrus.Infof("backfilled %d channels", count)
		}
	}

	logrus.Info("Done")
}
