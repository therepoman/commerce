package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"path"
	"runtime"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
)

var isWhitelisted = true

func main() {
	region := flag.String("region", "us-west-2", "AWS account region")
	env := flag.String("env", string(config.Staging), "environment (staging or prod)")
	flag.Parse()

	db, err := dynamodb.NewHypeTrainConfigsDB(dynamodb.DynamoDBConfig{
		Region:      *region,
		Environment: *env,
	})
	if err != nil {
		logrus.WithError(err).Panic("Failed to initiated hype train configs DB")
	}

	whitelistIDs, _ := loadWhitelistChannelIDs()

	for _, id := range whitelistIDs {
		update := percy.HypeTrainConfigUpdate{
			IsWhitelisted: &isWhitelisted,
		}

		err = db.UpdateRecommendedConfig(context.Background(), id, update)
		if err != nil {
			logrus.WithError(err).Panic("Failed to seed the DynamoDB table with the recommended config")
		}
	}
}

func loadWhitelistChannelIDs() ([]string, error) {
	_, filename, _, _ := runtime.Caller(1)
	dir := path.Dir(filename)
	fileName := fmt.Sprintf("%s/whitelist.json", dir)

	raw, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil, err
	}

	var ccSlice []string
	err = json.Unmarshal(raw, &ccSlice)
	if err != nil {
		return nil, err
	}

	return ccSlice, nil
}
