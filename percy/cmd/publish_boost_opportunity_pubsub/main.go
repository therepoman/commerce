package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"

	"code.justin.tv/chat/pubsub-go-pubclient/client"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/pubsub"
	"code.justin.tv/commerce/percy/internal/wrapper"
	"code.justin.tv/foundation/twitchclient"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/urfave/cli"
)

var (
	environment string
	channelID   string
	messageType string
)

func main() {
	app := cli.NewApp()
	app.Name = "Boosts publisher"
	app.Usage = "Sends test boost opportunity pubsub"

	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:        "environment, e",
			Usage:       "the environment you're publishing to. Can be either 'local', 'staging', or 'production'.",
			Value:       "local",
			Destination: &environment,
		},
		&cli.StringFlag{
			Name:        "channel-id, c",
			Usage:       "the channel ID for the boost opportunity.",
			Destination: &channelID,
		},
		cli.StringFlag{
			Name:        "type, t",
			Value:       "start",
			Usage:       "The pubsub Message type to publish",
			Destination: &messageType,
		},
	}

	app.Action = publishBoostOpportunityToPubsub
	err := app.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Fatal("script failed")
	}
}

func publishBoostOpportunityToPubsub(c *cli.Context) error {
	cfg, err := config.LoadConfig(config.Environment(environment))
	if err != nil {
		return errors.Wrap(err, "failed to create percy config")
	}

	pubsubClient, err := client.NewPubClient(twitchclient.ClientConf{
		Host: cfg.Endpoints.Pubsub,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(cfg.SOCKS5Addr),
		},
	})
	if err != nil {
		logrus.WithError(err).Panic("failed to init pubsub client")
	}

	publisher := pubsub.NewBoostsPublisher(pubsubClient)

	switch messageType {
	case "boost-opportunity-start":
		return sendBoostOpportunityStartMessage(*publisher)
	case "boost-opportunity-completion":
		return sendBoostOpportunityCompletionMessage(*publisher)
	default:
		logrus.WithField("messageType", messageType).Fatal("invalid message type")
		return nil
	}
}

func sendBoostOpportunityStartMessage(publisher pubsub.BoostsPublisher) error {
	boostOpportunity := percy.BoostOpportunity{
		ChannelID:        channelID,
		ID:               uuid.New().String(),
		StartedAt:        time.Now().UTC(),
		ExpiresAt:        time.Now().Add(5 * time.Minute).UTC(),
		UnitsContributed: 0,
	}

	err := publisher.PublishBoostOpportunityStart(context.Background(), boostOpportunity)
	if err != nil {
		return errors.Wrap(err, "failed to publish message to SNS")
	}

	logrus.Info("successfully published boost opportunity start message")

	err = printJSON(boostOpportunity)
	if err != nil {
		return errors.Wrap(err, "failed to print JSON")
	}

	return nil
}

func sendBoostOpportunityCompletionMessage(publisher pubsub.BoostsPublisher) error {
	currentTime := time.Now()

	boostOpportunity := percy.BoostOpportunity{
		ChannelID:        channelID,
		ID:               uuid.New().String(),
		StartedAt:        currentTime.Add(-5 * time.Minute).UTC(),
		ExpiresAt:        currentTime,
		EndedAt:          &currentTime,
		UnitsContributed: 10,
	}

	err := publisher.PublishBoostOpportunityCompletion(context.Background(), boostOpportunity)
	if err != nil {
		return errors.Wrap(err, "failed to publish message to SNS")
	}

	logrus.Info("successfully published boost opportunity completion message")

	err = printJSON(boostOpportunity)
	if err != nil {
		return errors.Wrap(err, "failed to print JSON")
	}

	return nil
}

func printJSON(opportunity percy.BoostOpportunity) error {
	messageJSON, err := json.Marshal(&opportunity)
	if err != nil {
		return errors.Wrap(err, "failed to marshal JSON for boost opportunity")
	}

	var prettyJSON bytes.Buffer
	err = json.Indent(&prettyJSON, messageJSON, "", "  ")
	if err != nil {
		return errors.Wrap(err, "could not format JSON indents")
	}

	fmt.Println(prettyJSON.String())

	return nil
}
