package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/config"
	"code.justin.tv/commerce/percy/models"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/pkg/errors"
	"github.com/urfave/cli"
)

var (
	environment string
	userID      string
	intensity   string
	channelID   string
	isDryRun    bool
)

func main() {
	app := cli.NewApp()
	app.Name = "Activity Feed SNS tester"
	app.Usage = "Sends test SNS to Activity Feed for celebration purchase events"

	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:        "environment, e",
			Usage:       "the environment you're publishing to. Can be either 'staging' or 'production'.",
			Value:       "staging",
			Destination: &environment,
		},
		&cli.StringFlag{
			Name:        "user-id, u",
			Usage:       "the user ID the celebration purchase is from.",
			Destination: &userID,
		},
		&cli.StringFlag{
			Name:        "channel-id, c",
			Usage:       "the channel ID the celebration purchase was made in.",
			Destination: &channelID,
		},
		&cli.StringFlag{
			Name:        "intensity, i",
			Usage:       "the intensity of the celebration purchased. Should be 'SMALL', 'MEDIUM', or 'LARGE'.",
			Destination: &intensity,
		},
		&cli.BoolFlag{
			Name:        "is-dry-run",
			Usage:       "denotes if this is a dry run, just so we don't send bad messages",
			Destination: &isDryRun,
		},
	}

	app.Action = testSNSPublish
	err := app.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Fatal("script failed")
	}
}

func testSNSPublish(c *cli.Context) error {
	cfg, err := config.LoadConfig(config.Environment(environment))
	if err != nil {
		return errors.Wrap(err, "failed to create percy config")
	}

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-2"),
	})
	if err != nil {
		return errors.Wrap(err, "failed to create AWS session")
	}
	snsClient := sns.New(sess)

	message := models.CelebrationPurchaseActivityFeedEvent{
		ChannelID: channelID,
		UserID:    userID,
		Effect:    "FIREWORKS",
		Intensity: "SMALL",
	}

	messageJSON, err := json.Marshal(&message)
	if err != nil {
		return errors.Wrap(err, "failed to marshal revenue event message")
	}

	if !isDryRun {
		_, err = snsClient.Publish(&sns.PublishInput{
			TopicArn: aws.String(cfg.SNSTopics.CelebrationsActivityFeed),
			Message:  aws.String(string(messageJSON)),
		})
		if err != nil {
			return errors.Wrap(err, "failed to publish message to SNS")
		}

		logrus.Infof("successfully published event t %s", cfg.SNSTopics.CelebrationsActivityFeed)
	} else {
		logrus.Infof("would be sending this message to topic %s", cfg.SNSTopics.CelebrationsActivityFeed)
	}

	var prettyJSON bytes.Buffer
	err = json.Indent(&prettyJSON, messageJSON, "", "  ")
	if err != nil {
		return errors.Wrap(err, "could not format JSON indents")
	}

	fmt.Println(prettyJSON.String())

	return nil
}
