package main

import (
	"context"
	"flag"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/config"
	"code.justin.tv/commerce/percy/internal/dynamodb"
)

func main() {
	region := flag.String("region", "us-west-2", "AWS account region")
	env := flag.String("env", string(config.Staging), "environment (staging or production)")
	channelID := flag.String("channel", "", "channel ID")
	from := flag.String("from", "", "start timestamp of the range to query for (in RFC3339Nano format or a partial prefix)")
	to := flag.String("to", "", "end timestamp of the range to query for (in RFC3339Nano format or a partial prefix)")
	flag.Parse()

	hypeTrainDB, err := dynamodb.NewHypeTrainDB(dynamodb.DynamoDBConfig{
		Region:      *region,
		Environment: *env,
	})
	if err != nil {
		logrus.WithError(err).Panic("Failed to initiated hype train configs DB")
	}

	trains, err := hypeTrainDB.QueryHypeTrainsBySortPrefix(context.Background(), *channelID, *from, *to)
	if err != nil {
		logrus.WithError(err).Panic("Failed to query for hype trains")
	}

	logrus.Info("Hype Trains Results:")
	logrus.Info("ID\t\tStartTime\t\tEndTime\t\tCompleted Level\t\tTotal Points")
	for _, train := range trains {
		logrus.Infof("%s\t\t%10s\t\t%s\t\t%d\t\t%d", train.ID, train.StartedAt.Format(time.UnixDate), train.EndedAt.Format(time.UnixDate), train.CompletedLevel(), int(train.ParticipationPoints()))
	}

	logrus.Info("Done")
}
