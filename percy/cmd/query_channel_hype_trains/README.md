# Query Channel Hype Trains Script

This script is used to query for hype train instances for a specific channel over a period of time. The input can either specific `from` and `to` timestamp, or just one of them. The timestamp can be specificied fully in RFC3339Nano format (which is the default format AWS SDK uses to serialize `time.Time` structs), or just a partial prefix of it (e.g. `2020-01` for Jan of 2020).


For hype trains within the given time range
```
AWS_PROFILE={your_prod_aws_profile} go run cmd/query_channel_hype_trains/main.go -channel=1234567 -from=2020-01-08T10:30:00Z -to=2020-01-09T02:00:00Z -env=production
```

For hype trains after Jan 10th of 2020
```
AWS_PROFILE={your_prod_aws_profile} go run cmd/query_channel_hype_trains/main.go -channel=1234567 -from=2020-01-10 -env=production
```

For hype trains before Jan 20th of 2020
```
AWS_PROFILE={your_prod_aws_profile} go run cmd/query_channel_hype_trains/main.go -channel=1234567 -to=2020-01-20 -env=production
```
