package main

import (
	"context"
	"errors"
	"math/rand"
	"net/http"
	"os"
	"time"

	pubsub_control "code.justin.tv/chat/pubsub-control/client"
	"code.justin.tv/chat/pubsub-go-pubclient/client"
	gogogadget_sns "code.justin.tv/commerce/gogogadget/aws/sns"
	sqsWorker "code.justin.tv/commerce/gogogadget/aws/sqs/worker"
	"code.justin.tv/commerce/logrus"
	paydayrpc "code.justin.tv/commerce/payday/rpc/payday"
	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/arbiter"
	"code.justin.tv/commerce/percy/internal/badges"
	"code.justin.tv/commerce/percy/internal/booster"
	"code.justin.tv/commerce/percy/internal/chainpublisher"
	"code.justin.tv/commerce/percy/internal/dart"
	"code.justin.tv/commerce/percy/internal/datascience"
	"code.justin.tv/commerce/percy/internal/destiny"
	"code.justin.tv/commerce/percy/internal/dynamicconfig"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"code.justin.tv/commerce/percy/internal/eventbus"
	"code.justin.tv/commerce/percy/internal/hallpass"
	"code.justin.tv/commerce/percy/internal/liveline"
	"code.justin.tv/commerce/percy/internal/mako"
	"code.justin.tv/commerce/percy/internal/miniexperiments"
	"code.justin.tv/commerce/percy/internal/miniexperiments/c2_bits_pinata"
	"code.justin.tv/commerce/percy/internal/miniexperiments/ht_approaching"
	"code.justin.tv/commerce/percy/internal/miniexperiments/ht_celeb_experiment"
	"code.justin.tv/commerce/percy/internal/pantheon"
	"code.justin.tv/commerce/percy/internal/personalization"
	"code.justin.tv/commerce/percy/internal/pubsub"
	"code.justin.tv/commerce/percy/internal/pubsub/rewards"
	"code.justin.tv/commerce/percy/internal/purchase"
	"code.justin.tv/commerce/percy/internal/redis"
	"code.justin.tv/commerce/percy/internal/sandstorm"
	"code.justin.tv/commerce/percy/internal/sfn"
	"code.justin.tv/commerce/percy/internal/sns"
	percy_sqs "code.justin.tv/commerce/percy/internal/sqs"
	"code.justin.tv/commerce/percy/internal/subscriptions"
	percy_twirp "code.justin.tv/commerce/percy/internal/twirp"
	bits_pinata_twirp "code.justin.tv/commerce/percy/internal/twirp/bits_pinata"
	celebrations_twirp "code.justin.tv/commerce/percy/internal/twirp/celebrations"
	celebrations_offertenant_twirp "code.justin.tv/commerce/percy/internal/twirp/celebrations/offertenant"
	creator_anniversaries_twirp "code.justin.tv/commerce/percy/internal/twirp/creator_anniversaries"
	twitchiversary_twirp "code.justin.tv/commerce/percy/internal/twirp/twitchiversary"
	"code.justin.tv/commerce/percy/internal/users"
	"code.justin.tv/commerce/splatter"
	eventbus_client "code.justin.tv/eventbus/client"
	"code.justin.tv/foundation/twitchclient"
	"code.justin.tv/subs/destiny/destinytwirp"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/cactus/go-statsd-client/statsd"
	events "github.com/segmentio/events/v2"
	"github.com/twitchtv/twirp"
)

const (
	DefaultNumberOfConnections = 100
)

type Clients struct {
	statter                         statsd.Statter
	hypeTrainConfigsDB              *dynamodb.HypeTrainConfigsDB
	hypeTrainDB                     *dynamodb.HypeTrainDB
	participationDB                 *dynamodb.ParticipationsDB
	badgesDB                        *dynamodb.BadgesDB
	participantDB                   *dynamodb.ParticipantsDB
	conductorsDB                    *dynamodb.ConductorsDB
	approachingDB                   *dynamodb.ApproachingDB
	userAnniversaryTokensDB         *dynamodb.UserAnniversaryTokensDB
	bitsPinataDB                    *dynamodb.BitsPinataDB
	spade                           *datascience.Spade
	entitlementDB                   *mako.EntitlementDB
	pubsubClient                    client.PubClient
	badgesClient                    *badges.Client
	pubsubPublisher                 *pubsub.Publisher
	eventbusPublisher               *eventbus.Publisher
	userProfileDB                   *users.UserProfileDB
	editorDB                        *hallpass.EditorDB
	whitelistDB                     *dynamicconfig.WhitelistDB
	userAnniversaryAllowlistDB      *dynamicconfig.UserAnniversaryAllowlistDB
	creatorAnniversariesDB          *dynamicconfig.CreatorAnniversariesDB
	userAnniversaryCache            redis.UserAnniversaryCache
	foundersCelebrationExperimentDB percy.ExperimentDB
	snsPublisher                    *sns.Publisher
	sfn                             *sfn.SFN
	purchaseDB                      *purchase.PurchaseDB
	notificationDB                  *dart.NotificationDB
	personalization                 *personalization.Client
	celebrationsExperimentClient    miniexperiments.Client
	approachingExperimentClient     miniexperiments.Client
	channelStatusDB                 *liveline.Client
	destiny                         destinytwirp.Destiny

	// For Paid Boost Experiment
	booster                    percy.BoostsLauncher
	paidBoostConfigsDB         *dynamicconfig.PaidBoostsConfigsDB
	boostsDB                   *dynamodb.BoostsDB
	boostsOpportunityScheduler *destiny.BoostOpportunityScheduler
	boostsPublisher            *pubsub.BoostsPublisher

	// For Bits Pinata
	payday                 paydayrpc.Payday
	pinataExperimentClient miniexperiments.Client
	pinataPublisher        pubsub.PinataPublisher
}

func InitializeClients(cfg config.Config) *Clients {
	statter, err := config.NewBufferedStatter(cfg)
	if err != nil {
		logrus.WithError(err).Panic("Error loading statter")
	}

	sandstormClient, err := sandstorm.New(&cfg)
	if err != nil {
		logrus.WithError(err).Panic("failed to initiate sandstorm client")
	}

	hypeTrainLiveSlackWebHookSecret, err := sandstormClient.GetSecret(cfg.HypeTrainLiveSlackHookSecretName)
	if err != nil {
		logrus.WithError(err).Panic("failed to get secrets from sandstorm client")
	}
	if hypeTrainLiveSlackWebHookSecret == nil {
		logrus.WithError(err).Panic("no secrets found in sandstorm")
	}
	HypetrainLiveSlackWebhookURL := string(hypeTrainLiveSlackWebHookSecret.Plaintext)

	dynamodbConfig := dynamodb.DynamoDBConfig{
		Region:      cfg.AWSRegion,
		Environment: cfg.DynamoSuffix,
		Endpoint:    cfg.Endpoints.DynamoDB,
	}

	hypeTrainConfigsDB, err := dynamodb.NewHypeTrainConfigsDB(dynamodbConfig)
	if err != nil {
		logrus.WithError(err).Panic("failed to initiate configs DB")
	}
	hypeTrainDB, err := dynamodb.NewHypeTrainDB(dynamodbConfig)
	if err != nil {
		logrus.WithError(err).Panic("failed to initiate hype train DB")
	}

	participationDB, err := dynamodb.NewParticipationsDB(dynamodbConfig)
	if err != nil {
		logrus.WithError(err).Panic("failed to initiate participation DB")
	}

	badgesDB, err := dynamodb.NewBadgesDB(dynamodbConfig)
	if err != nil {
		logrus.WithError(err).Panic("failed to initiate badges DB")
	}

	participantDB, err := dynamodb.NewParticipantsDB(dynamodbConfig)
	if err != nil {
		logrus.WithError(err).Panic("failed to initiate participant DB")
	}

	conductorsDB, err := dynamodb.NewConductorsDB(dynamodbConfig)
	if err != nil {
		logrus.WithError(err).Panic("failed to initiate conductors DB")
	}

	approachingDB, err := dynamodb.NewApproachingDB(dynamodbConfig)
	if err != nil {
		logrus.WithError(err).Panic("failed to initiate approaching DB")
	}

	userAnniversaryTokensDB, err := dynamodb.NewUserAnniversaryTokensDB(dynamodbConfig)
	if err != nil {
		logrus.WithError(err).Panic("failed to initiate user anniversary tokens DB")
	}

	bitsPinataDB, err := dynamodb.NewBitsPinataDB(dynamodb.DynamoDBConfig{
		Region:      cfg.AWSRegion,
		Environment: cfg.DynamoSuffix,
	})
	if err != nil {
		logrus.WithError(err).Panic("failed to initiate bits pinata DB")
	}

	spade, err := datascience.NewSpadeClient(cfg.SpadeConf.Environment, cfg.SpadeConf.MaxConcurrency, statter, cfg.SOCKS5Addr, cfg.ApproachingDatascienceEnabled)
	if err != nil {
		logrus.WithError(err).Panic("failed to initiate spade client")
	}

	entitlementDB := mako.NewEntitlementDB(cfg.Endpoints.Mako, statter, cfg.SOCKS5Addr)

	pubConfig := twitchclient.ClientConf{
		Host: cfg.Endpoints.Pubsub,
	}
	pubsubClient, err := client.NewPubClient(pubConfig)
	if err != nil {
		logrus.WithError(err).Panic("failed to initiate pubsub client")
	}

	eventbusPublisher, err := eventbus.NewPublisher(cfg.AWSRegion, cfg.Environment, hypeTrainConfigsDB)
	if err != nil {
		logrus.WithError(err).Panic("failed to initiate eventbus publisher client")
	}

	clientHttpTransport := twitchclient.TransportConf{
		MaxIdleConnsPerHost: DefaultNumberOfConnections,
	}

	pubSubControlClientCfg := twitchclient.ClientConf{
		Host:      cfg.Endpoints.PubsubControl,
		Stats:     statter,
		Transport: clientHttpTransport,
	}

	control, err := pubsub_control.NewClient(pubSubControlClientCfg)
	if err != nil {
		logrus.WithError(err).Panic("failed to initiate pubsub control client")
	}

	badgesClient, err := badges.NewClient(cfg.Endpoints.Badges, statter, cfg.SOCKS5Addr)
	if err != nil {
		logrus.WithError(err).Panic("Failed to initialize badges client")
	}

	userProfileDB, err := users.NewUserProfileDB(cfg.Endpoints.UsersService, cfg.Endpoints.TMI, cfg.Endpoints.Ripley, statter, cfg.SOCKS5Addr)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize users profile DB")
	}

	subscriptionsDB, err := subscriptions.NewSubscriptionsDB(cfg.Endpoints.Subscriptions, statter, cfg.SOCKS5Addr)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize subscriptions DB")
	}

	editorDB, err := hallpass.NewEditorDB(cfg.Endpoints.Hallpass, statter, cfg.SOCKS5Addr)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize editor DB")
	}

	whitelistDB, err := dynamicconfig.NewWhitelistDB(cfg.AWSRegion, cfg.ConfigS3Endpoint, cfg.ExperimentOverridesS3BucketName, statter)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize whitelist DB")
	}

	userAnniversaryAllowlistDB, err := dynamicconfig.NewUserAnniversaryAllowlistDB(cfg.AWSRegion, cfg.ConfigS3Endpoint, cfg.ExperimentOverridesS3BucketName, statter)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize Twitchiversary Allowlist DB")
	}
	userAnniversaryCache, err := redis.NewUserAnniversaryCache(cfg.EventsRedisUrl, "", statter, cfg.SOCKS5Addr)
	if err != nil {
		logrus.WithError(err).Panic("failed to initiate User Service Cache Redis client")
	}

	creatorAnniversariesDB, err := dynamicconfig.NewCreatorAnniversariesDB(cfg.AWSRegion, cfg.ConfigS3Endpoint, cfg.ExperimentOverridesS3BucketName)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize Creator Anniversaries DB")
	}

	foundersCelebrationExperimentDB, err := dynamicconfig.NewFoundersCelebrationExperimentDB(cfg.AWSRegion, cfg.ConfigS3Endpoint, cfg.ExperimentOverridesS3BucketName, subscriptionsDB, statter)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize founders celebration experiment DB")
	}

	notificationDB, err := dart.NewNotificationDB(cfg.Endpoints.Dart, statter, cfg.SOCKS5Addr)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize notification DB")
	}

	// Publisher
	makoFetcher := mako.NewFetcher(cfg.Endpoints.Mako, statter)
	rewardsDisplayInfoFetcher := rewards.NewDisplayInfoFetcher(badgesClient, makoFetcher)
	rewardsDisplayInfoAttacher := rewards.NewRewardsDisplayInfoAttacher(rewardsDisplayInfoFetcher)
	celebrationsMiniexperimentsClient := ht_celeb_experiment.NewClient()
	approachingMiniexperimentsClient := ht_approaching.NewClient()
	publisher := pubsub.NewPublisher(
		pubsubClient, control, HypetrainLiveSlackWebhookURL, userProfileDB, rewardsDisplayInfoAttacher, rewardsDisplayInfoFetcher, subscriptionsDB, makoFetcher,
		celebrationsMiniexperimentsClient, approachingMiniexperimentsClient, cfg.ApproachingEnabled, cfg.ApproachingChannelOverrides)
	snsPublisher := sns.NewPublisher(cfg, gogogadget_sns.NewDefaultClient())
	sfn := sfn.NewSFN(cfg)
	personalizationClient := personalization.NewClient(cfg.Endpoints.Personalization, statter, cfg.SOCKS5Addr)

	purchaseDB, err := purchase.NewPurchaseDB(cfg.Endpoints.Mulan, cfg.Endpoints.Everdeen, snsPublisher, statter, cfg.SOCKS5Addr)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize purchase DB")
	}

	destinyClient := destiny.NewClient(cfg.Endpoints.Destiny, statter, cfg.SOCKS5Addr)
	channelStatusDB := liveline.NewClient(cfg.Endpoints.Liveline, statter, cfg.SOCKS5Addr)

	// For Paid Boost Experiment
	boostDynamoDBConfig := dynamodb.DynamoDBConfig{
		Region:      cfg.AWSRegion,
		Environment: cfg.DynamoSuffix,
		Endpoint:    cfg.Endpoints.DynamoDBSink,
	}

	boostsDB, err := dynamodb.NewBoostsDB(boostDynamoDBConfig)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize Boosts DB")
	}
	paidBoostsConfigsDB, err := dynamicconfig.NewPaidBoostsConfigsDB(cfg.AWSRegion, statter)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize Paid Boosts Configs DB")
	}
	boostsOpportunityScheduler := destiny.NewBoostOpportunityScheduler(cfg.Endpoints.Destiny, statter, cfg.SOCKS5Addr)

	noopBoosterClient := booster.NewNoopClient()
	boosterClient := booster.NewClient(cfg.Endpoints.Booster, statter, cfg.SOCKS5Addr)
	boosterSwitch, err := percy.NewBoostsLaunchSwitch(paidBoostsConfigsDB, boosterClient, noopBoosterClient)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize boost launcher switch")
	}

	boostsPublisher := pubsub.NewBoostsPublisher(pubsubClient)

	// For Bits Pinata
	pinataMiniExperimentClient := c2_bits_pinata.NewClient()
	pinataPublisher := pubsub.NewPinataPublisher(pubsubClient)
	payday := paydayrpc.NewPaydayProtobufClient(cfg.Endpoints.Payday, twitchclient.NewHTTPClient(twitchclient.ClientConf{
		Stats: statter,
		Transport: twitchclient.TransportConf{
			MaxIdleConnsPerHost: 200,
		},
	}))

	return &Clients{
		statter:                         statter,
		hypeTrainConfigsDB:              hypeTrainConfigsDB,
		hypeTrainDB:                     hypeTrainDB,
		participationDB:                 participationDB,
		badgesDB:                        badgesDB,
		participantDB:                   participantDB,
		conductorsDB:                    conductorsDB,
		spade:                           spade,
		entitlementDB:                   entitlementDB,
		approachingDB:                   approachingDB,
		userAnniversaryTokensDB:         userAnniversaryTokensDB,
		bitsPinataDB:                    bitsPinataDB,
		pubsubClient:                    pubsubClient,
		badgesClient:                    badgesClient,
		pubsubPublisher:                 publisher,
		eventbusPublisher:               eventbusPublisher,
		userProfileDB:                   userProfileDB,
		editorDB:                        editorDB,
		whitelistDB:                     whitelistDB,
		userAnniversaryAllowlistDB:      userAnniversaryAllowlistDB,
		userAnniversaryCache:            userAnniversaryCache,
		creatorAnniversariesDB:          creatorAnniversariesDB,
		foundersCelebrationExperimentDB: foundersCelebrationExperimentDB,
		snsPublisher:                    snsPublisher,
		sfn:                             sfn,
		purchaseDB:                      purchaseDB,
		notificationDB:                  notificationDB,
		personalization:                 personalizationClient,
		celebrationsExperimentClient:    celebrationsMiniexperimentsClient,
		approachingExperimentClient:     approachingMiniexperimentsClient,
		pinataExperimentClient:          pinataMiniExperimentClient,
		destiny:                         destinyClient,
		channelStatusDB:                 channelStatusDB,
		boostsDB:                        boostsDB,
		paidBoostConfigsDB:              paidBoostsConfigsDB,
		boostsOpportunityScheduler:      boostsOpportunityScheduler,
		booster:                         boosterSwitch,
		boostsPublisher:                 boostsPublisher,
		pinataPublisher:                 pinataPublisher,
		payday:                          payday,
	}
}

func makeEngine(cfg *config.Config, clients *Clients) (percy.Engine, percy.EngineConfig, error) {
	if cfg == nil {
		return nil, percy.EngineConfig{}, errors.New("received nil config")
	}

	eventDB := destiny.NewEventDB(clients.destiny)

	leaderboard := pantheon.NewLeaderboard(cfg.Endpoints.Pantheon, clients.statter, cfg.SOCKS5Addr)

	conductorBadgesHandler := percy.NewConductorBadgesHandler(clients.conductorsDB, clients.badgesDB, clients.badgesClient, clients.spade)

	dynamicTagClient, err := arbiter.NewDynamicTagClient(cfg, clients.statter)
	if err != nil {
		return nil, percy.EngineConfig{}, err
	}

	chainPublisher := chainpublisher.New(clients.pubsubPublisher, clients.eventbusPublisher)

	engineConfig := percy.EngineConfig{
		ApproachingDB:               clients.approachingDB,
		BitsPinataDB:                clients.bitsPinataDB,
		ConfigsDB:                   clients.hypeTrainConfigsDB,
		HypeTrainDB:                 clients.hypeTrainDB,
		EntitlementDB:               clients.entitlementDB,
		ParticipantDB:               clients.participantDB,
		ParticipationDB:             clients.participationDB,
		BadgesDB:                    clients.badgesDB,
		EventDB:                     eventDB,
		ConductorsDB:                clients.conductorsDB,
		Leaderboard:                 leaderboard,
		Statter:                     clients.statter,
		Publisher:                   chainPublisher,
		Spade:                       clients.spade,
		ConductorBadgesHandler:      conductorBadgesHandler,
		DynamicTag:                  dynamicTagClient,
		SNSPublisher:                clients.snsPublisher,
		UserProfileDB:               clients.userProfileDB,
		Personalization:             clients.personalization,
		ApproachingExperimentClient: clients.approachingExperimentClient,
		BitsPinataExperimentClient:  clients.pinataExperimentClient,
	}
	engine := percy.NewEngine(engineConfig)

	return engine, engineConfig, nil
}

func makeCelebrationEngine(cfg *config.Config, clients *Clients) (percy.CelebrationsEngine, percy.CelebrationEngineConfig, error) {
	if cfg == nil {
		return nil, percy.CelebrationEngineConfig{}, errors.New("received nil config")
	}

	dynamodbConfig := dynamodb.DynamoDBConfig{
		Region:      cfg.AWSRegion,
		Environment: cfg.DynamoSuffix,
		Endpoint:    cfg.Endpoints.DynamoDB,
	}

	celebrationConfigsDB, err := dynamodb.NewCelebrationsConfigDB(dynamodbConfig)
	if err != nil {
		return nil, percy.CelebrationEngineConfig{}, errors.New("failed to initiate celebration configs DB")
	}

	celebrationUserSettingsDB, err := dynamodb.NewCelebrationUserSettingsDB(dynamodbConfig)
	if err != nil {
		return nil, percy.CelebrationEngineConfig{}, errors.New("failed to initiate celebration user settings DB")
	}

	celebrationChannelPurchasableConfigDB, err := dynamodb.NewCelebrationChannelPurchasableConfigDB(dynamodbConfig)
	if err != nil {
		return nil, percy.CelebrationEngineConfig{}, errors.New("failed to initiate celebration channel purchasable config DB")
	}

	idempotencyDB, err := dynamodb.NewIdempotencyDB(dynamodbConfig)
	if err != nil {
		return nil, percy.CelebrationEngineConfig{}, errors.New("failed to initiate celebration purchase fulfillment step DB")
	}

	celebrationEngineConfig := percy.CelebrationEngineConfig{
		CelebrationConfigsDB:                  celebrationConfigsDB,
		CelebrationUserSettingsDB:             celebrationUserSettingsDB,
		CelebrationChannelPurchasableConfigDB: celebrationChannelPurchasableConfigDB,
		IdempotencyDB:                         idempotencyDB,
		Statter:                               clients.statter,
		Publisher:                             clients.pubsubPublisher,
		UserProfileDB:                         clients.userProfileDB,
		EditorDB:                              clients.editorDB,
		WhitelistDB:                           clients.whitelistDB,
		FoundersExperimentDB:                  clients.foundersCelebrationExperimentDB,
		Spade:                                 clients.spade,
		StateMachine:                          clients.sfn,
		Purchase:                              clients.purchaseDB,
		Notification:                          clients.notificationDB,
		SNS:                                   clients.snsPublisher,
	}

	celebrationEngine := percy.NewCelebrationEngine(celebrationEngineConfig)

	return celebrationEngine, celebrationEngineConfig, nil
}

func makeTwitchiversaryEngine(clients *Clients) (percy.TwitchiversaryEngine, error) {
	twitchiversaryConfig := percy.TwitchiversaryEngineConfig{
		UserAnniversaryAllowlistDB: clients.userAnniversaryAllowlistDB,
		UserProfileDB:              clients.userProfileDB,
		UserAnniversaryCache:       clients.userAnniversaryCache,
		UserAnniversaryTokensDB:    clients.userAnniversaryTokensDB,
	}

	twitchiversary := percy.NewTwitchiversaryEngine(twitchiversaryConfig)

	return twitchiversary, nil
}

func makeCreatorAnniversariesEngine(clients *Clients) (percy.CreatorAnniversariesEngine, error) {
	creatorAnniversariesConfig := percy.CreatorAnniversariesEngineConfig{
		CreatorAnniversariesDB: clients.creatorAnniversariesDB,
	}
	if creatorAnniversariesConfig.CreatorAnniversariesDB == nil {
		return nil, errors.New("creatorAnniversariesDB is nil")
	}

	creatorAnniversaries := percy.NewCreatorAnniversariesEngine(creatorAnniversariesConfig)

	return creatorAnniversaries, nil
}

func makePaidBoostLaunchPad(clients *Clients) percy.LaunchPad {
	return percy.NewPaidBoostLaunchPad(percy.LaunchPadConfig{
		BoostsDB:        clients.boostsDB,
		ChannelStatusDB: clients.channelStatusDB,
		ConfigsDB:       clients.paidBoostConfigsDB,
		PaymentsHandler: clients.purchaseDB,
		Scheduler:       clients.boostsOpportunityScheduler,
		StateMachine:    clients.sfn,
		UserProfileDB:   clients.userProfileDB,
		Publisher:       clients.boostsPublisher,
		BoostsLauncher:  clients.booster,
	})
}

func main() {
	rand.Seed(time.Now().UnixNano())

	env := config.GetEnv()
	cfg, err := config.LoadConfig(env)

	if err != nil || cfg == nil {
		logrus.WithError(err).Panic("Error loading config")
	}

	ctx, cancel := events.WithSignals(context.Background(), os.Interrupt, os.Kill)
	defer cancel()

	logrus.Infof("Loaded config: %s", cfg.Environment)

	clients := InitializeClients(*cfg)

	// New Engine
	engine, engineConfig, err := makeEngine(cfg, clients)
	if err != nil {
		logrus.WithError(err).Panic("Error loading engine")
	}

	// New celebration engine
	celebrationEngine, _, err := makeCelebrationEngine(cfg, clients)
	if err != nil {
		logrus.WithError(err).Panic("Error loading celebration engine")
	}

	// New twitchiversary engine
	twitchiversaryEngine, err := makeTwitchiversaryEngine(clients)
	if err != nil {
		logrus.WithError(err).Panic("Error loading twitchiversary engine")
	}

	// New creatorAnniversaries engine
	creatorAnniversariesEngine, err := makeCreatorAnniversariesEngine(clients)
	if err != nil {
		logrus.WithError(err).Panic("Error loading creatorAnniversaries engine")
	}

	// New Paid Boost LaunchPad
	launchPad := makePaidBoostLaunchPad(clients)

	twirp := makeTwirp(
		engine,
		celebrationEngine,
		twitchiversaryEngine,
		creatorAnniversariesEngine,
		clients.bitsPinataDB,
		launchPad,
		cfg.PaidBoostOfferIDs,
		clients.statter)

	defer func() {
		logrus.Info("Initiated shutdown process of miniexperiments client")
		err := clients.celebrationsExperimentClient.Close()
		if err != nil {
			logrus.WithError(err).Error("Error shutting down celebrations miniexperiments client")
		}

		err = clients.approachingExperimentClient.Close()
		if err != nil {
			logrus.WithError(err).Error("Error shutting down approaching miniexperiments client")
		}

		err = clients.pinataExperimentClient.Close()
		if err != nil {
			logrus.WithError(err).Error("Error shutting down pinata miniexperiments client")
		}

		logrus.Info("Initiated shutdown process of whitelist client")
		clients.whitelistDB.Close()
		clients.foundersCelebrationExperimentDB.Close()
	}()

	if config.GetEnv() != config.Local {
		workerConfig := eventbus.WorkerConfig{
			Engine:   engine,
			Mux:      eventbus_client.NewMux(),
			QueueURL: cfg.EventbusPercySQSUrl,
			Statter:  clients.statter,
		}

		jobRunner, err := eventbus.NewWorkerRunner(workerConfig, cfg.AWSRegion)
		if err != nil {
			logrus.WithError(err).Panic("Error start sqs runner")
		}
		defer func() {
			logrus.Info("Initiated shutdown process of SQS Event Bus workers")
			if err := jobRunner.Shutdown(); err != nil {
				logrus.WithError(err).Warn("Error shutting down sqs runner")
			}
		}()

		celebrationWorkerConfig := eventbus.CelebrationsWorkerConfig{
			Engine:   celebrationEngine,
			Mux:      eventbus_client.NewMux(),
			QueueURL: cfg.EventbusCelebrationsSQSUrl,
			Statter:  clients.statter,
		}

		celebrationWorkerJobRunner, err := eventbus.NewCelebrationsWorkerRunner(celebrationWorkerConfig, cfg.AWSRegion)
		if err != nil {
			logrus.WithError(err).Panic("Error starting sqs runner")
		}
		defer func() {
			logrus.Info("Initiated shutdown process of SQS Event Bus workers")
			if err := celebrationWorkerJobRunner.Shutdown(); err != nil {
				logrus.WithError(err).Warn("Error shutting down sqs runner")
			}
		}()

		emoticonWorkerConfig := eventbus.EmoticonsWorkerConfig{
			Engine:   engine,
			Mux:      eventbus_client.NewMux(),
			QueueURL: cfg.EventbusMakoEventsSQSUrl,
			Statter:  clients.statter,
		}

		emoticonWorkerJobRunner, err := eventbus.NewEmoticonsWorkerRunner(emoticonWorkerConfig, cfg.AWSRegion)
		if err != nil {
			logrus.WithError(err).Panic("Error start sqs runner")
		}
		defer func() {
			logrus.Info("Initiated shutdown process of SQS Emoticon Event Bus workers")
			if err := emoticonWorkerJobRunner.Shutdown(); err != nil {
				logrus.WithError(err).Warn("Error shutting down emoticon sqs runner")
			}
		}()

		pinataWorkerConfig := eventbus.PinataWorkerConfig{
			PinataExperimentClient: clients.pinataExperimentClient,
			Liveline:               clients.channelStatusDB,
			Destiny:                clients.destiny,
			Mux:                    eventbus_client.NewMux(),
			QueueURL:               cfg.EventbusPinataSQSUrl,
			Statter:                clients.statter,
			Publisher:              clients.pinataPublisher,
		}

		pinataWorkerJobRunner, err := eventbus.NewPinataWorkerRunner(pinataWorkerConfig, cfg.AWSRegion)
		if err != nil {
			logrus.WithError(err).Panic("Error starting Pinata event bus runner")
		}
		defer func() {
			logrus.Info("Initiated shutdown process of Event Bus Pinata worker")
			if err := pinataWorkerJobRunner.Shutdown(); err != nil {
				logrus.WithError(err).Warn("Error shutting down Event Bus Pinata runner")
			}
		}()

		// For scheduling Paid Boost Opportunity
		streamUpWorkerConfig := eventbus.StreamUpWorkerConfig{
			LaunchPad: launchPad,
			Mux:       eventbus_client.NewMux(),
			QueueURL:  cfg.EventbusStreamUpSQSUrl,
			Statter:   clients.statter,
		}

		streamUpWorkerJobRunner, err := eventbus.NewStreamUpWorkerRunner(streamUpWorkerConfig, cfg.AWSRegion)
		if err != nil {
			logrus.WithError(err).Panic("Error starting event bus Stream Up worker job runner")
		}
		defer func() {
			logrus.Info("Initiated shutdown process of event bus Stream Up worker job runner")
			if err := streamUpWorkerJobRunner.Shutdown(); err != nil {
				logrus.WithError(err).Warn("Error shutting down event bus Stream Up worker job runner")
			}
		}()
	}

	makeSQSWorkers(*cfg, clients.statter, engine, engineConfig.Leaderboard, launchPad, clients)

	go func() {
		logrus.Info("listening on '%{port}s'", ":3000")
		if err := http.ListenAndServe(":3000", twirp); err != nil {
			logrus.Info("%{error}s", err)
		}
	}()

	<-ctx.Done()
}

func makeTwirp(
	engine percy.Engine,
	celebrationEngine percy.CelebrationsEngine,
	twitchiversaryEngine percy.TwitchiversaryEngine,
	creatorAnniversariesEngine percy.CreatorAnniversariesEngine,
	bitsPinataDB percy.BitsPinataDB,
	launchPad percy.LaunchPad,
	paidBoostOfferIDs []string,
	statter statsd.Statter) http.Handler {

	twirpStatsHook := twirp.ChainHooks(splatter.NewStatsdServerHooks(statter))
	// twirp converter
	converter := percy_twirp.NewConverter()
	celebrationsConvertor := celebrations_twirp.NewCelebrationsConverter()

	percy := percy_twirp.NewPercy(engine, converter)
	celebrations := celebrations_twirp.NewCelebrations(celebrationEngine, celebrationsConvertor, launchPad, paidBoostOfferIDs)
	twitchiversary := twitchiversary_twirp.NewTwitchiversary(twitchiversaryEngine)
	creator_anniversaries := creator_anniversaries_twirp.NewCreatorAnniversaries(creatorAnniversariesEngine)
	bitsPinata := bits_pinata_twirp.NewBitsPinata(bitsPinataDB)
	offerTenantConverter := celebrations_offertenant_twirp.NewConverter()

	// Note we are re-using the Celebrations offer tenant for paid boost
	// Passing in the paid boost struct/logic
	celebrationsOfferTenant := celebrations_offertenant_twirp.NewOfferTenant(celebrationEngine, launchPad, offerTenantConverter)

	handler := percy_twirp.NewHandler(percy, celebrations, twitchiversary, creator_anniversaries, bitsPinata, celebrationsOfferTenant, twirpStatsHook)

	logrus.Info("Initiated shutdown process")

	return handler
}

func makeSQSWorkers(cfg config.Config, statter statsd.Statter, engine percy.Engine, leaderboard percy.Leaderboard, launchpad percy.LaunchPad, clients *Clients) {
	conductorUpdater := percy.NewConductorUpdater(
		clients.hypeTrainDB,
		clients.participantDB,
		leaderboard,
		clients.pubsubPublisher,
		clients.eventbusPublisher,
		clients.snsPublisher,
		clients.participationDB,
		statter)

	pantheonNotificationWorker := percy_sqs.NewPantheonUpdateWorker(conductorUpdater)

	channelBanEventsWorker := percy_sqs.NewChannelBanWorker(conductorUpdater)

	expireHypeTrainWorker := &percy_sqs.ExpireHypeTrainWorker{
		Engine: engine,
	}

	participationWorker := percy_sqs.NewParticipationWorker(engine)

	cooldownExpirationEventWorker := percy_sqs.NewCooldownExpirationEventWorker(engine, clients.pubsubPublisher)

	boostOpportunityStartWorker := percy_sqs.NewBoostOpportunityStartWorker(launchpad)
	boostOpportunityEndWorker := percy_sqs.NewBoostOpportunityCompletionWorker(launchpad)

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(cfg.AWSRegion),
	})
	if err != nil {
		logrus.WithError(err).Panic("error creating session")
	}

	sqs := sqs.New(sess)

	// Destiny events worker
	sqsWorker.NewSQSWorkerManager(
		cfg.ExpireHypeTrainWorker.Name,
		cfg.ExpireHypeTrainWorker.NumWorkers,
		1,
		sqs,
		expireHypeTrainWorker,
		statter)

	// Pantheon events worker
	sqsWorker.NewSQSWorkerManager(
		cfg.PantheonNotificationWorker.Name,
		cfg.PantheonNotificationWorker.NumWorkers,
		1,
		sqs,
		pantheonNotificationWorker,
		statter)

	// Clue channel ban events worker
	sqsWorker.NewSQSWorkerManager(
		cfg.ClueChannelBanWorker.Name,
		cfg.ClueChannelBanWorker.NumWorkers,
		1,
		sqs,
		channelBanEventsWorker,
		statter)

	// Participation worker for testing purposes
	sqsWorker.NewSQSWorkerManager(
		cfg.ParticipationEventWorker.Name,
		cfg.ParticipationEventWorker.NumWorkers,
		1,
		sqs,
		participationWorker,
		statter)

	sqsWorker.NewSQSWorkerManager(
		cfg.CooldownExpirationEventWorker.Name,
		cfg.CooldownExpirationEventWorker.NumWorkers,
		1,
		sqs,
		cooldownExpirationEventWorker,
		statter)

	/*
		Paid Boost SQS Workers:
		- Destiny Calls back to start Paid Boost Opportunity
		- Destiny Calls back to end Paid Boost Opportunity
	*/
	sqsWorker.NewSQSWorkerManager(
		cfg.PaidBoostOpportunityStartWorker.Name,
		cfg.PaidBoostOpportunityStartWorker.NumWorkers,
		1,
		sqs,
		boostOpportunityStartWorker,
		statter)

	sqsWorker.NewSQSWorkerManager(
		cfg.PaidBoostOpportunityCompletionWorker.Name,
		cfg.PaidBoostOpportunityCompletionWorker.NumWorkers,
		1,
		sqs,
		boostOpportunityEndWorker,
		statter)
}
