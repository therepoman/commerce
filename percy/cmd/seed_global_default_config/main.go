// This scripts seeds the specified DynamoDB table with a global default Hype Train config
package main

import (
	"context"
	"flag"
	"time"

	"code.justin.tv/chat/badges/app/models"
	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"github.com/sirupsen/logrus"
)

var (
	enabled                        = true
	isWhitelisted                  = true
	numOfEventsToKickoff           = 3
	kickoffDuration                = 5 * time.Minute
	minPointsToCountTowardsKickoff = 100
	levelDuration                  = 5 * time.Minute
	cooldownDuration               = 120 * time.Minute
	difficulty                     = percy.DifficultyEasy

	level1Rewards = []percy.Reward{
		{Type: percy.EmoteReward, ID: "emotesv2_62199faa2ca34ea8a0f3567990a72a14"}, // HypeHeh
		{Type: percy.EmoteReward, ID: "emotesv2_69a7806c6837428f82475e99677d2f78"}, // HypeDoh
		{Type: percy.EmoteReward, ID: "emotesv2_a964a0cbae9348e6bd981bc714eec71d"}, // HypeYum
		{Type: percy.EmoteReward, ID: "emotesv2_680c3aae688947d8b6067cff1a8bcdbe"}, // HypeShame
		{Type: percy.EmoteReward, ID: "emotesv2_6a99bc2baae743099b23ed6ab07bc5c4"}, // HypeHide
	}
	level1RewardsWithSpecial = append(level1Rewards, percy.Reward{Type: percy.EmoteReward, ID: "emotesv2_d20a5e514e534288a1104b92c4f87834"}) // HypeWow

	level2Rewards = []percy.Reward{
		{Type: percy.EmoteReward, ID: "emotesv2_ea658eb2e9d54833a4518c6dcc196dc6"}, // HypeTongue
		{Type: percy.EmoteReward, ID: "emotesv2_811afb48bceb4ccdbd3281c602d3e3cb"}, // HypePurr
		{Type: percy.EmoteReward, ID: "emotesv2_994d515930a14e5396fd36d45e785d48"}, // HypeOoh
		{Type: percy.EmoteReward, ID: "emotesv2_f045d9aa07d54961ab2ba77174305278"}, // HypeBeard
		{Type: percy.EmoteReward, ID: "emotesv2_23f63a570f724822bb976f36572a0785"}, // HypeEyes
	}
	level2RewardsWithSpecial = append(level2Rewards, percy.Reward{Type: percy.EmoteReward, ID: "emotesv2_50e775355dbe4992a086f24ffaa73676"}) // HypeHay

	level3Rewards = []percy.Reward{
		{Type: percy.EmoteReward, ID: "emotesv2_fa2dad1f526b4c0a843d2cc4d12a7e06"}, // HypeYesPlease
		{Type: percy.EmoteReward, ID: "emotesv2_22683be90477418fbc8e76e0cd91a4bd"}, // HypeDerp
		{Type: percy.EmoteReward, ID: "emotesv2_164b5a252ea94201b7fcfcb7113fe621"}, // HypeJudge
		{Type: percy.EmoteReward, ID: "emotesv2_5ade9654471d406994040073d80c78ac"}, // HypeEars
		{Type: percy.EmoteReward, ID: "emotesv2_031719611d64458fb76982679a2d492a"}, // HypeCozy
	}
	level3RewardsWithSpecial = append(level3Rewards, percy.Reward{Type: percy.EmoteReward, ID: "emotesv2_d8271fc8f0264fdc9b1ac79051f75349"}) // HypeYas

	level4Rewards = []percy.Reward{
		{Type: percy.EmoteReward, ID: "emotesv2_2a3cd0373fe349cf853c058f10fae0be"}, // HypeWant
		{Type: percy.EmoteReward, ID: "emotesv2_661e2889e5b0420a8bb0766dd6cf8010"}, // HypeStahp
		{Type: percy.EmoteReward, ID: "emotesv2_0f5d26b991a44ffbb88188495a8dd689"}, // HypeYawn
		{Type: percy.EmoteReward, ID: "emotesv2_19e3d6baefa5477caeaa238bf1b31fb1"}, // HypeCreep
		{Type: percy.EmoteReward, ID: "emotesv2_dc24652ada1e4c84a5e3ceebae4de709"}, // HypeDisguise
	}
	level4RewardsWithSpecial = append(level4Rewards, percy.Reward{Type: percy.EmoteReward, ID: "emotesv2_f35caa0f5f3243b88cfbd85a3c9e69ff"}) // HypeAttack

	level5Rewards = []percy.Reward{
		{Type: percy.EmoteReward, ID: "emotesv2_a05d626acce9485d83fdfb02b6553826"}, // HypeScream
		{Type: percy.EmoteReward, ID: "emotesv2_07dfbc3be2af4edea09217f6f9292b40"}, // HypeSquawk
		{Type: percy.EmoteReward, ID: "emotesv2_e0d949b6afb94b01b608fb3ad3e08348"}, // HypeSus
		{Type: percy.EmoteReward, ID: "emotesv2_be2e7ac3e077421da3526633fbbb9176"}, // HypeHeyFriends
		{Type: percy.EmoteReward, ID: "emotesv2_ebc2e7675cdd4f4f9871557cfed4b28e"}, // HypeMine
	}
	level5RewardsWithSpecial = append(level5Rewards, percy.Reward{Type: percy.EmoteReward, ID: "emotesv2_d4a50cfaa51f46e99e5228ce8ef953c4"}) // HypeShy

	levelSettingsByDifficulty = map[percy.Difficulty][]percy.LevelSetting{
		percy.DifficultyEasy: {
			{Level: 1, Goal: percy.ParticipationPoint(1600), Rewards: level1Rewards},
			{Level: 2, Goal: percy.ParticipationPoint(3400), Rewards: level2Rewards},
			{Level: 3, Goal: percy.ParticipationPoint(5500), Rewards: level3Rewards},
			{Level: 4, Goal: percy.ParticipationPoint(7800), Rewards: level4Rewards},
			{Level: 5, Goal: percy.ParticipationPoint(10800), Rewards: level5Rewards},
		},
		percy.DifficultyMedium: {
			{Level: 1, Goal: percy.ParticipationPoint(2000), Rewards: level1Rewards},
			{Level: 2, Goal: percy.ParticipationPoint(4500), Rewards: level2Rewards},
			{Level: 3, Goal: percy.ParticipationPoint(7600), Rewards: level3Rewards},
			{Level: 4, Goal: percy.ParticipationPoint(11500), Rewards: level4Rewards},
			{Level: 5, Goal: percy.ParticipationPoint(17000), Rewards: level5Rewards},
		},
		percy.DifficultyHard: {
			{Level: 1, Goal: percy.ParticipationPoint(3000), Rewards: level1Rewards},
			{Level: 2, Goal: percy.ParticipationPoint(7000), Rewards: level2Rewards},
			{Level: 3, Goal: percy.ParticipationPoint(12300), Rewards: level3Rewards},
			{Level: 4, Goal: percy.ParticipationPoint(19100), Rewards: level4Rewards},
			{Level: 5, Goal: percy.ParticipationPoint(29000), Rewards: level5Rewards},
		},
		percy.DifficultySuperHard: {
			{Level: 1, Goal: percy.ParticipationPoint(5000), Rewards: level1RewardsWithSpecial},
			{Level: 2, Goal: percy.ParticipationPoint(12500), Rewards: level2RewardsWithSpecial},
			{Level: 3, Goal: percy.ParticipationPoint(23100), Rewards: level3RewardsWithSpecial},
			{Level: 4, Goal: percy.ParticipationPoint(37700), Rewards: level4RewardsWithSpecial},
			{Level: 5, Goal: percy.ParticipationPoint(60000), Rewards: level5RewardsWithSpecial},
		},
		percy.DifficultyInsane: {
			{Level: 1, Goal: percy.ParticipationPoint(10000), Rewards: level1RewardsWithSpecial},
			{Level: 2, Goal: percy.ParticipationPoint(25000), Rewards: level2RewardsWithSpecial},
			{Level: 3, Goal: percy.ParticipationPoint(45000), Rewards: level3RewardsWithSpecial},
			{Level: 4, Goal: percy.ParticipationPoint(70000), Rewards: level4RewardsWithSpecial},
			{Level: 5, Goal: percy.ParticipationPoint(105000), Rewards: level5RewardsWithSpecial},
		},
	}

	participationConversionRate = map[percy.ParticipationIdentifier]percy.ParticipationPoint{
		{Source: percy.BitsSource, Action: percy.CheerAction}:          percy.ParticipationPoint(1),
		{Source: percy.BitsSource, Action: percy.ExtensionAction}:      percy.ParticipationPoint(1),
		{Source: percy.BitsSource, Action: percy.PollAction}:           percy.ParticipationPoint(1),
		{Source: percy.SubsSource, Action: percy.Tier1SubAction}:       percy.ParticipationPoint(500),
		{Source: percy.SubsSource, Action: percy.Tier2SubAction}:       percy.ParticipationPoint(1000),
		{Source: percy.SubsSource, Action: percy.Tier3SubAction}:       percy.ParticipationPoint(2500),
		{Source: percy.SubsSource, Action: percy.Tier1GiftedSubAction}: percy.ParticipationPoint(500),
		{Source: percy.SubsSource, Action: percy.Tier2GiftedSubAction}: percy.ParticipationPoint(1000),
		{Source: percy.SubsSource, Action: percy.Tier3GiftedSubAction}: percy.ParticipationPoint(2500),
	}

	notificationThresholds = map[percy.ParticipationIdentifier]int{
		{Source: percy.BitsSource, Action: percy.CheerAction}:          1000,
		{Source: percy.BitsSource, Action: percy.ExtensionAction}:      1000,
		{Source: percy.BitsSource, Action: percy.PollAction}:           1000,
		{Source: percy.SubsSource, Action: percy.Tier1SubAction}:       5,
		{Source: percy.SubsSource, Action: percy.Tier2SubAction}:       5,
		{Source: percy.SubsSource, Action: percy.Tier3SubAction}:       5,
		{Source: percy.SubsSource, Action: percy.Tier1GiftedSubAction}: 5,
		{Source: percy.SubsSource, Action: percy.Tier2GiftedSubAction}: 5,
		{Source: percy.SubsSource, Action: percy.Tier3GiftedSubAction}: 5,
	}

	currentConductorBadge = percy.Reward{
		ID:      "1",
		Type:    percy.BadgeReward,
		GroupID: models.BadgeSetHypeTrain,
	}

	formerConductorBadge = percy.Reward{
		ID:      "2",
		Type:    percy.BadgeReward,
		GroupID: models.BadgeSetHypeTrain,
	}

	conductorRewards = percy.ConductorRewards{
		percy.BitsSource: map[percy.ConductorType][]percy.Reward{
			percy.CurrentConductor: {currentConductorBadge},
			percy.FormerConductor:  {formerConductorBadge},
		},
		percy.SubsSource: map[percy.ConductorType][]percy.Reward{
			percy.CurrentConductor: {currentConductorBadge},
			percy.FormerConductor:  {formerConductorBadge},
		},
	}
)

func main() {
	region := flag.String("region", "us-west-2", "AWS account region")
	env := flag.String("env", string(config.Staging), "environment (staging or production)")
	flag.Parse()

	db, err := dynamodb.NewHypeTrainConfigsDB(dynamodb.DynamoDBConfig{
		Region:      *region,
		Environment: *env,
	})
	if err != nil {
		logrus.WithError(err).Panic("Failed to initiated hype train configs DB")
	}

	defaultConfig := percy.HypeTrainConfig{
		Enabled:       enabled,
		IsWhitelisted: isWhitelisted,
		KickoffConfig: percy.KickoffConfig{
			NumOfEvents: numOfEventsToKickoff,
			Duration:    kickoffDuration,
			MinPoints:   percy.ParticipationPoint(minPointsToCountTowardsKickoff),
		},

		LevelDuration:    levelDuration,
		CooldownDuration: cooldownDuration,
		Difficulty:       difficulty,
		RewardEndDate:    nil,

		LevelSettingsByDifficulty:    levelSettingsByDifficulty,
		ParticipationConversionRates: participationConversionRate,
		NotificationThresholds:       notificationThresholds,
		ConductorRewards:             conductorRewards,
		HasConductorBadges:           true,
		CalloutEmoteID:               "81273", // this is the emote ID for komodohype
		UseCreatorColor:              true,
		UsePersonalizedSettings:      false,
	}

	err = db.PutGlobalDefaultConfig(context.Background(), defaultConfig)
	if err != nil {
		logrus.WithError(err).Panic("Failed to seed the DynamoDB table with the global default config")
	}
}
