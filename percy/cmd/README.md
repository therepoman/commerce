# Pery Scripts
:steam_locomotive: 

## How to run

To run one of the Percy scripts, simply specify the `AWS_PROFILE` `region` and `env` you'd like to run the script in. 
This assumes that you already have AWS credentials set up for the `twitch-percy-aws-devo`  and `twitch-percy-aws-prod` accounts. 

example: 
```AWS_PROFILE=twitch-percy-aws-devo go run cmd/seed_global_hype_train_config/main.go -env=staging -region=us-west-2```
