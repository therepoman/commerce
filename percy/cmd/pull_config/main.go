package main

import (
	"context"
	"flag"
	"fmt"
	"time"

	"code.justin.tv/commerce/percy/config"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"github.com/sirupsen/logrus"
	"golang.org/x/time/rate"
)

var ids = []string{
	"131862486",
	"39249330",
	"117974920",
	"105022773",
	"77862597",
	"21095024",
	"58836975",
	"23844396",
	"85521892",
	"23735582",
	"37455669",
	"30656206",
	"23179441",
	"165785",
	"37530941",
	"53728390",
	"137347549",
	"190708734",
	"21080562",
	"135675015",
	"2005586",
	"61339182",
	"38770961",
}

func main() {
	region := flag.String("region", "us-west-2", "AWS account region")
	env := flag.String("env", string(config.Staging), "environment (staging or prod)")
	flag.Parse()

	rateLimiter := rate.NewLimiter(20, 20)

	configsDB, err := dynamodb.NewHypeTrainConfigsDB(dynamodb.DynamoDBConfig{
		Region:      *region,
		Environment: *env,
	})
	if err != nil {
		logrus.WithError(err).Panic("Failed to initiated hype train config DB")
	}

	fmt.Printf("channelID, \tdifficulty, \teventsThreshold, \tcooldownHours\n")
	for _, id := range ids {
		err := rateLimiter.Wait(context.Background())
		if err != nil {
			logrus.WithError(err).Error("Failed to rate limit")
			continue
		}

		config, err := configsDB.GetConfig(context.Background(), id)
		if err != nil {
			logrus.WithError(err).Panic("failed to get config")
		}

		fmt.Printf("%s, \t%s, \t%d, \t%d\n", id, config.Difficulty, config.KickoffConfig.NumOfEvents, config.CooldownDuration/time.Hour)
	}

	logrus.Info("complete")
}
