package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"

	tmi "code.justin.tv/chat/tmi/client"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/config"
	"code.justin.tv/commerce/percy/internal/metrics"
	"code.justin.tv/commerce/percy/internal/wrapper"
	"code.justin.tv/foundation/twitchclient"
	"github.com/pkg/errors"
	"github.com/urfave/cli"
)

var (
	environment string
	userID      string
	intensity   string
	channelID   string
	isDryRun    bool
)

func main() {
	app := cli.NewApp()
	app.Name = "Celebration Purcahse User Notice tester"
	app.Usage = "Sends test Celebration Purcahse User Notice to TMI"

	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:        "environment, e",
			Usage:       "the environment you're publishing to. Can be either 'staging' or 'production'.",
			Value:       "staging",
			Destination: &environment,
		},
		&cli.StringFlag{
			Name:        "user-id, u",
			Usage:       "the user ID the celebration purchase is from.",
			Destination: &userID,
		},
		&cli.StringFlag{
			Name:        "channel-id, c",
			Usage:       "the channel ID the celebration purchase was made in.",
			Destination: &channelID,
		},
		&cli.StringFlag{
			Name:        "intensity, i",
			Usage:       "the intensity of the celebration purchased. Should be 'SMALL', 'MEDIUM', or 'LARGE'.",
			Destination: &intensity,
		},
		&cli.BoolFlag{
			Name:        "is-dry-run",
			Usage:       "denotes if this is a dry run, just so we don't send bad messages",
			Destination: &isDryRun,
		},
	}

	app.Action = publishUserNotice
	err := app.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Fatal("script failed")
	}
}

func publishUserNotice(c *cli.Context) error {
	cfg, err := config.LoadConfig(config.Environment(environment))
	if err != nil {
		return errors.Wrap(err, "failed to create percy config")
	}

	tmiClient, err := tmi.NewClient(twitchclient.ClientConf{
		Stats: &metrics.NoopStatter{},
		Host:  cfg.Endpoints.TMI,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper("127.0.0.1:8080"),
			wrapper.NewRetryRoundTripWrapper(&metrics.NoopStatter{}, "tmi", 1),
			wrapper.NewHystrixRoundTripWrapper("tmi", 1000),
		},
	})
	if err != nil {
		return errors.Wrap(err, "couldn't make TMI client")
	}

	parsedChannelID, err := strconv.Atoi(channelID)
	if err != nil {
		return errors.Wrap(err, "error parsing recipient userID as int")
	}

	parsedUserID, err := strconv.Atoi(userID)
	if err != nil {
		return errors.Wrap(err, "error parsing benefactor userID as int")
	}

	message := tmi.SendUserNoticeParams{
		SenderUserID:    parsedUserID,
		TargetChannelID: parsedChannelID,
		MsgID:           "celebrationpurchase",
		MsgParams: []tmi.UserNoticeMsgParam{
			{
				Key:   "intensity",
				Value: intensity,
			},
			{
				Key:   "effect",
				Value: "FIREWORKS",
			},
		},
		DefaultSystemBody: "celebration notification",
	}

	messageJSON, err := json.Marshal(&message)
	if err != nil {
		return errors.Wrap(err, "failed to marshal user notice event")
	}

	if !isDryRun {
		err = tmiClient.SendUserNotice(context.Background(), message, nil)
		if err != nil {
			return errors.Wrap(err, "failed to publish user notice to TMI")
		}

		logrus.Infof("successfully published event to %s", cfg.Endpoints.TMI)
	} else {
		logrus.Infof("would be sending this event to %s", cfg.Endpoints.TMI)
	}

	var prettyJSON bytes.Buffer
	err = json.Indent(&prettyJSON, messageJSON, "", "  ")
	if err != nil {
		return errors.Wrap(err, "could not format JSON indents")
	}

	fmt.Println(prettyJSON.String())

	return nil
}
