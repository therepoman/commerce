package main

import (
	"context"
	"errors"
	"time"

	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/destiny"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"code.justin.tv/commerce/percy/internal/mako"
	"code.justin.tv/commerce/percy/internal/metrics"
	"code.justin.tv/commerce/percy/internal/tests"
	"github.com/sirupsen/logrus"
)

const channelId = "111112"

func main() {
	env := config.GetEnv()
	cfg, err := config.LoadConfig(env)

	if err != nil || cfg == nil {
		logrus.WithError(err).Panic("Error loading config")
	}

	engine, err := makeEngine(cfg)
	if err != nil {
		logrus.WithError(err).Panic("Error loading engine")
	}
	ctx := context.Background()

	validHypeTrain := percy.HypeTrain{
		ChannelID:      channelId,
		ID:             "1234",
		StartedAt:      time.Now(),
		ExpiresAt:      time.Now().Add(1 * time.Minute),
		UpdatedAt:      time.Now(),
		Config:         tests.ValidHypeTrainConfig(),
		Conductors:     tests.ValidConductors(),
		Participations: make(percy.ParticipationTotals),
	}

	validParticipation := percy.Participation{
		ChannelID: channelId,
		Timestamp: time.Now().Add(30 * time.Second),
		User:      percy.NewUser("1234567"),
		Quantity:  1,
		ParticipationIdentifier: percy.ParticipationIdentifier{
			Source: percy.SubsSource,
			Action: percy.Tier1SubAction,
		},
	}

	logrus.Infof("%v %v\n", validHypeTrain, validParticipation)

	hypeTrain, err := engine.GetMostRecentHypeTrain(ctx, channelId)
	if err != nil {
		logrus.WithError(err).Panic("failed to get most recent hypetrain")
	}

	_, err = engine.EntitleParticipants(ctx, *hypeTrain)
	if err != nil {
		logrus.WithError(err).Panic("failed to entitle participants")
	}
}

func makeEngine(cfg *config.Config) (percy.Engine, error) {
	if cfg == nil {
		return nil, errors.New("received nil config")
	}

	dynamodbConfig := dynamodb.DynamoDBConfig{
		Region:      cfg.AWSRegion,
		Environment: cfg.DynamoSuffix,
		Endpoint:    cfg.Endpoints.DynamoDB,
	}

	hypeTrainConfigsDB, err := dynamodb.NewHypeTrainConfigsDB(dynamodbConfig)
	if err != nil {
		return nil, errors.New("failed to initiate configs DB")
	}

	hypeTrainDB, err := dynamodb.NewHypeTrainDB(dynamodbConfig)
	if err != nil {
		return nil, errors.New("failed to initiate hype train DB")
	}

	participationDB, err := dynamodb.NewParticipationsDB(dynamodbConfig)
	if err != nil {
		return nil, errors.New("failed to initiate participation DB")
	}

	participantDB, err := dynamodb.NewParticipantsDB(dynamodbConfig)
	if err != nil {
		return nil, errors.New("failed to initiate participant DB")
	}

	approachingDB, err := dynamodb.NewApproachingDB(dynamodbConfig)
	if err != nil {
		return nil, errors.New("failed to initiate approaching DB")
	}

	entitlementDB := mako.NewEntitlementDB(cfg.Endpoints.Mako, &metrics.NoopStatter{}, cfg.SOCKS5Addr)

	eventDB := destiny.NewEventDB(destiny.NewClient(cfg.Endpoints.Destiny, &metrics.NoopStatter{}, cfg.SOCKS5Addr))

	engine := percy.NewEngine(percy.EngineConfig{
		ApproachingDB:   approachingDB,
		ConfigsDB:       hypeTrainConfigsDB,
		HypeTrainDB:     hypeTrainDB,
		ParticipantDB:   participantDB,
		ParticipationDB: participationDB,
		EventDB:         eventDB,
		EntitlementDB:   entitlementDB,
	})

	return engine, nil
}
