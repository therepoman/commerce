package main

import (
	"math/rand"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"code.justin.tv/commerce/percy/internal/idempotency"
	"code.justin.tv/commerce/percy/internal/lambdahandlers"
	"github.com/aws/aws-lambda-go/lambda"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	env := config.GetEnv()
	cfg, err := config.LoadConfig(env)

	if err != nil || cfg == nil {
		logrus.WithError(err).Panic("Error loading config")
	}

	dynamoConfig := dynamodb.DynamoDBConfig{
		Region:      cfg.AWSRegion,
		Environment: cfg.DynamoSuffix,
		Endpoint:    cfg.Endpoints.DynamoDBSink,
	}

	idempotencyDB, err := dynamodb.NewIdempotencyDB(dynamoConfig)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize idempotency db")
	}

	boostsDB, err := dynamodb.NewBoostsDB(dynamoConfig)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize boosts DB")
	}

	launchPad := percy.NewPaidBoostLaunchPad(percy.LaunchPadConfig{
		BoostsDB: boostsDB,
	})

	enforcer, err := idempotency.NewEnforcer(idempotencyDB)
	if err != nil {
		logrus.WithError(err).Panic("failed to init idempotency enforcer")
	}

	handler := lambdahandlers.NewBoostOpportunityContributionHandler(launchPad, enforcer)

	lambda.Start(handler.Handle)
}
