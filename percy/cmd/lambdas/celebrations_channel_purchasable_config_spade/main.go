package main

import (
	"math/rand"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/config"
	"code.justin.tv/commerce/percy/internal/datascience"
	"code.justin.tv/commerce/percy/internal/lambdahandlers"
	"github.com/aws/aws-lambda-go/lambda"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	env := config.GetEnv()
	cfg, err := config.LoadConfig(env)

	if err != nil || cfg == nil {
		logrus.WithError(err).Panic("Error loading config")
	}

	statter, err := config.NewBufferedStatter(*cfg)
	if err != nil {
		logrus.WithError(err).Panic("Error loading statter")
	}

	spade, err := datascience.NewSpadeClient(cfg.SpadeConf.Environment, cfg.SpadeConf.MaxConcurrency, statter, cfg.SOCKS5Addr, false)
	if err != nil {
		logrus.WithError(err).Panic("failed to initiate spade client")
	}

	handler := lambdahandlers.NewCelebrationsChannelPurchasableConfigSpadeHandler(spade)

	lambda.Start(handler.Handle)
}
