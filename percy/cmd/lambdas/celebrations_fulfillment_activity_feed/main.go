package main

import (
	"math/rand"
	"time"

	gogogadget_sns "code.justin.tv/commerce/gogogadget/aws/sns"
	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"code.justin.tv/commerce/percy/internal/idempotency"
	"code.justin.tv/commerce/percy/internal/lambdahandlers"
	"code.justin.tv/commerce/percy/internal/sns"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/sirupsen/logrus"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	env := config.GetEnv()
	cfg, err := config.LoadConfig(env)

	if err != nil || cfg == nil {
		logrus.WithError(err).Panic("Error loading config")
	}

	snsPublisher := sns.NewPublisher(*cfg, gogogadget_sns.NewDefaultClient())

	dynamoConfig := dynamodb.DynamoDBConfig{
		Region:      cfg.AWSRegion,
		Environment: cfg.DynamoSuffix,
		Endpoint:    cfg.Endpoints.DynamoDB,
	}

	celebrationChannelPurchasableConfigDB, err := dynamodb.NewCelebrationChannelPurchasableConfigDB(dynamoConfig)
	if err != nil {
		logrus.WithError(err).Panic("failed to init celebration channel purchasable config DB")
	}

	idempotencyDB, err := dynamodb.NewIdempotencyDB(dynamoConfig)
	if err != nil {
		logrus.WithError(err).Panic("failed to init idempotency db")
	}

	engine := percy.NewCelebrationEngine(percy.CelebrationEngineConfig{
		SNS:                                   snsPublisher,
		CelebrationChannelPurchasableConfigDB: celebrationChannelPurchasableConfigDB,
		IdempotencyDB:                         idempotencyDB,
	})

	enforcer, err := idempotency.NewEnforcer(idempotencyDB)
	if err != nil {
		logrus.WithError(err).Panic("failed to init idempotency enforcer")
	}

	handler := lambdahandlers.NewActivityFeedCelebrationHandler(engine, enforcer)

	lambda.Start(handler.Handle)
}
