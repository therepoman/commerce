package main

import (
	"math/rand"
	"time"

	gogogadget_sns "code.justin.tv/commerce/gogogadget/aws/sns"
	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"code.justin.tv/commerce/percy/internal/idempotency"
	"code.justin.tv/commerce/percy/internal/lambdahandlers"
	"code.justin.tv/commerce/percy/internal/purchase"
	"code.justin.tv/commerce/percy/internal/sns"
	"code.justin.tv/commerce/percy/internal/users"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/sirupsen/logrus"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	env := config.GetEnv()
	cfg, err := config.LoadConfig(env)

	if err != nil || cfg == nil {
		logrus.WithError(err).Panic("Error loading config")
	}

	statter, err := config.NewBufferedStatter(*cfg)
	if err != nil {
		logrus.WithError(err).Panic("Error loading statter")
	}

	snsPublisher := sns.NewPublisher(*cfg, gogogadget_sns.NewDefaultClient())

	purchaseDB, err := purchase.NewPurchaseDB(cfg.Endpoints.Mulan, cfg.Endpoints.Everdeen, snsPublisher, statter, cfg.SOCKS5Addr)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize purchase DB")
	}

	userProfileDB, err := users.NewUserProfileDB(cfg.Endpoints.UsersService, cfg.Endpoints.TMI, cfg.Endpoints.Ripley, statter, cfg.SOCKS5Addr)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize users profile DB")
	}

	idempotencyDB, err := dynamodb.NewIdempotencyDB(dynamodb.DynamoDBConfig{
		Region:      cfg.AWSRegion,
		Environment: cfg.DynamoSuffix,
		Endpoint:    cfg.Endpoints.DynamoDB,
	})
	if err != nil {
		logrus.WithError(err).Panic("failed to init idempotency db")
	}

	engine := percy.NewCelebrationEngine(percy.CelebrationEngineConfig{
		Purchase:      purchaseDB,
		UserProfileDB: userProfileDB,
		IdempotencyDB: idempotencyDB,
	})

	enforcer, err := idempotency.NewEnforcer(idempotencyDB)
	if err != nil {
		logrus.WithError(err).Panic("failed to init idempotency enforcer")
	}

	handler := lambdahandlers.NewPayoutCelebrationHandler(engine, enforcer)

	lambda.Start(handler.Handle)
}
