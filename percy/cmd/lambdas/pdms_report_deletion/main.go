package main

import (
	"context"
	"os"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/lambdahandlers"
	"code.justin.tv/commerce/percy/internal/pdms"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/urfave/cli"
)

var (
	environment string
	dryrun      bool
	local       bool
)

func main() {
	app := cli.NewApp()
	app.Name = "ReportDeletion"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production', and 'staging'. Defaults to 'staging' if unset.",
			EnvVar:      config.EnvironmentEnvironmentVariable,
			Destination: &environment,
		},
		cli.StringSliceFlag{
			Name:  "userID",
			Usage: "Runtime user IDs to report deletion for. Only works if local is set to true.",
		},
		cli.BoolFlag{
			Name:        "dryrun",
			Usage:       "Pass in this flag to not actually report to PDMS deletions",
			Destination: &dryrun,
		},
		cli.BoolFlag{
			Name:        "local",
			Usage:       "Pass in this flag to run the handler directly instead of through lambda.Start()",
			Destination: &local,
		},
	}

	app.Action = createBackend

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func createBackend(c *cli.Context) {
	cfg, err := config.LoadConfig(config.Environment(environment))
	if err != nil || cfg == nil {
		log.WithError(err).Panic("error loading config")
	}

	pdmsClient := pdms.NewClient(*cfg)

	handler := lambdahandlers.NewReportDeletionLambdaHandler(pdmsClient)

	if local {
		ctx, cancel := context.WithTimeout(context.Background(), time.Duration(2)*time.Minute)
		defer cancel()
		output, err := handler.Handle(ctx, &percy.ReportDeletionRequest{
			UserDeletionRequestData: percy.UserDeletionRequestData{
				UserIDs:  c.StringSlice("userIDs"),
				IsDryRun: dryrun,
			},
		})
		if err != nil {
			log.WithError(err).Error("Lambda Handler Execution Failed")
		}
		log.WithField("output", output).Info("Execution Output")
	} else {
		lambda.Start(handler.Handle)
	}
}
