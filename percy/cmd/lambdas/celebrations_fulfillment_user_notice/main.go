package main

import (
	"math/rand"
	"time"

	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"code.justin.tv/commerce/percy/internal/idempotency"
	"code.justin.tv/commerce/percy/internal/lambdahandlers"
	"code.justin.tv/commerce/percy/internal/users"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/sirupsen/logrus"
)

// Note that this handler is re-purposed to serve paid boost and calls into the paid boost Launch Pad struct now instead of celebrations engine

func main() {
	rand.Seed(time.Now().UnixNano())

	env := config.GetEnv()
	cfg, err := config.LoadConfig(env)

	if err != nil || cfg == nil {
		logrus.WithError(err).Panic("Error loading config")
	}

	statter, err := config.NewBufferedStatter(*cfg)
	if err != nil {
		logrus.WithError(err).Panic("Error loading statter")
	}

	dynamoConfig := dynamodb.DynamoDBConfig{
		Region:      cfg.AWSRegion,
		Environment: cfg.DynamoSuffix,
		Endpoint:    cfg.Endpoints.DynamoDB,
	}

	userProfileDB, err := users.NewUserProfileDB(cfg.Endpoints.UsersService, cfg.Endpoints.TMI, cfg.Endpoints.Ripley, statter, cfg.SOCKS5Addr)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize users profile DB")
	}

	idempotencyDB, err := dynamodb.NewIdempotencyDB(dynamoConfig)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize idempotency db")
	}

	launchPad := percy.NewPaidBoostLaunchPad(percy.LaunchPadConfig{
		UserProfileDB: userProfileDB,
	})

	enforcer, err := idempotency.NewEnforcer(idempotencyDB)
	if err != nil {
		logrus.WithError(err).Panic("failed to init idempotency enforcer")
	}

	handler := lambdahandlers.NewPaidBoostOpportunityUserNoticeHandler(launchPad, enforcer)

	lambda.Start(handler.Handle)
}
