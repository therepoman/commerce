package main

import (
	"math/rand"
	"time"

	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dart"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"code.justin.tv/commerce/percy/internal/idempotency"
	"code.justin.tv/commerce/percy/internal/lambdahandlers"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/sirupsen/logrus"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	env := config.GetEnv()
	cfg, err := config.LoadConfig(env)

	if err != nil || cfg == nil {
		logrus.WithError(err).Panic("Error loading config")
	}

	statter, err := config.NewBufferedStatter(*cfg)
	if err != nil {
		logrus.WithError(err).Panic("Error loading statter")
	}

	notificationDB, err := dart.NewNotificationDB(cfg.Endpoints.Dart, statter, cfg.SOCKS5Addr)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize notification DB")
	}

	idempotencyDB, err := dynamodb.NewIdempotencyDB(dynamodb.DynamoDBConfig{
		Region:      cfg.AWSRegion,
		Environment: cfg.DynamoSuffix,
		Endpoint:    cfg.Endpoints.DynamoDB,
	})
	if err != nil {
		logrus.WithError(err).Panic("failed to init idempotency db")
	}

	engine := percy.NewCelebrationEngine(percy.CelebrationEngineConfig{
		Notification: notificationDB,
	})

	enforcer, err := idempotency.NewEnforcer(idempotencyDB)
	if err != nil {
		logrus.WithError(err).Panic("failed to init idempotency enforcer")
	}

	handler := lambdahandlers.NewRefundEmailCelebrationHandler(engine, enforcer)

	lambda.Start(handler.Handle)
}
