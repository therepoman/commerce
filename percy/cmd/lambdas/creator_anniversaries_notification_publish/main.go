package main

import (
	"os"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dart"
	"code.justin.tv/commerce/percy/internal/dynamicconfig"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"code.justin.tv/commerce/percy/internal/lambdahandlers"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
)

var (
	environment string
	local       bool
)

func main() {
	app := cli.NewApp()
	app.Name = "CreatorAnniversariesNotificationPublish"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production', and 'staging'. Defaults to 'staging' if unset.",
			EnvVar:      config.EnvironmentEnvironmentVariable,
			Destination: &environment,
		},
		cli.BoolFlag{
			Name:        "local",
			Usage:       "Pass in this flag to run the handler directly instead of through lambda.Start()",
			Destination: &local,
		},
	}

	app.Action = createBackend

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func createBackend(c *cli.Context) {
	cfg, err := config.LoadConfig(config.Environment(environment))
	if err != nil || cfg == nil {
		logrus.WithError(err).Panic("Error loading config")
	}

	statter, err := config.NewBufferedStatter(*cfg)
	if err != nil {
		logrus.WithError(err).Panic("Error loading statter")
	}

	notificationDB, err := dart.NewNotificationDB(cfg.Endpoints.Dart, statter, cfg.SOCKS5Addr)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize notification DB")
	}

	anniversariesDB, err := dynamicconfig.NewCreatorAnniversariesDB(cfg.AWSRegion, cfg.ConfigS3Endpoint, cfg.ExperimentOverridesS3BucketName)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize creator_anniversaries DB")
	}

	notificationIdempotencyDB, err := dynamodb.NewCreatorAnniversariesNotificationDB(dynamodb.DynamoDBConfig{
		Region:      cfg.AWSRegion,
		Environment: cfg.DynamoSuffix,
		Endpoint:    cfg.Endpoints.DynamoDB,
	})

	engine := percy.NewCreatorAnniversariesEngine(percy.CreatorAnniversariesEngineConfig{
		CreatorAnniversariesDB:  anniversariesDB,
		Notification:            notificationDB,
		NotificationIdempotency: notificationIdempotencyDB,
	})
	if err != nil {
		log.WithError(err).Panic("failed to initialize creator anniversaries engine")
	}

	handler := lambdahandlers.NewCreatorAnniversariesNotificationPublishHandler(engine)

	lambda.Start(handler.Handle)
}
