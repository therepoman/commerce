package main

import (
	"context"
	"os"
	"time"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/config"
	"code.justin.tv/commerce/percy/internal/lambdahandlers"
	"code.justin.tv/commerce/percy/internal/pdms"
	"code.justin.tv/commerce/percy/internal/sfn"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/client/subscriber/lambdafunc"
	"code.justin.tv/eventbus/schema/pkg/user"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/urfave/cli"
)

var (
	environment string
	userID      string
	dryrun      bool
	local       bool
)

func main() {
	app := cli.NewApp()
	app.Name = "StartDeletion"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'production', and 'staging'. Defaults to 'staging' if unset.",
			EnvVar:      config.EnvironmentEnvironmentVariable,
			Destination: &environment,
		},
		cli.StringFlag{
			Name:        "userID, u",
			Usage:       "The user ID to start deletion on; only used for local execution",
			Destination: &userID,
		},
		cli.BoolFlag{
			Name:        "dryrun",
			Usage:       "Pass in this flag to not actually report to PDMS deletions",
			Destination: &dryrun,
		},
		cli.BoolFlag{
			Name:        "local",
			Usage:       "Pass in this flag to run the handler directly instead of through lambda.Start()",
			Destination: &local,
		},
	}

	app.Action = createBackend

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func createBackend(c *cli.Context) {
	cfg, err := config.LoadConfig(config.Environment(environment))
	if err != nil || cfg == nil {
		log.WithError(err).Panic("error loading config")
	}

	pdmsClient := pdms.NewClient(*cfg)

	sfnClient := sfn.NewSFN(*cfg)

	handler := lambdahandlers.NewStartDeletionLambdaHandler(cfg, pdmsClient, sfnClient)

	if local {
		ctx, cancel := context.WithTimeout(context.Background(), time.Duration(2)*time.Minute)
		defer cancel()
		err := handler.Handle(ctx, &eventbus.Header{}, &user.Destroy{
			UserId: userID,
		})
		if err != nil {
			log.WithError(err).Error("Lambda Handler Execution Failed")
		}
		log.Info("Execution Complete")
	} else {
		mux := eventbus.NewMux()

		user.RegisterDestroyHandler(mux, handler.Handle)

		lambda.Start(lambdafunc.NewSQS(mux.Dispatcher()))
	}
}
