package main

import (
	"context"
	"fmt"
	"os"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/booster"
	"code.justin.tv/commerce/percy/internal/metrics"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/urfave/cli"
)

var (
	environment string
	channelID   string
)

func main() {
	app := cli.NewApp()
	app.Name = "Booster Client tester"
	app.Usage = "creates test order with TwitchPromotionsBooster service"

	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:        "environment, e",
			Usage:       "the environment you're publishing to. Can be either 'local', 'staging', or 'production'.",
			Value:       "local",
			Destination: &environment,
		},
		&cli.StringFlag{
			Name:        "channel-id, c",
			Usage:       "the channel ID for the order.",
			Destination: &channelID,
		},
	}

	app.Action = testBoosterCreateOrder
	err := app.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Fatal("script failed")
	}
}

func testBoosterCreateOrder(c *cli.Context) error {
	cfg, err := config.LoadConfig(config.Environment(environment))
	if err != nil {
		return errors.Wrap(err, "failed to create percy config")
	}

	boosterClient := booster.NewClient(cfg.Endpoints.Booster, &metrics.NoopStatter{}, cfg.SOCKS5Addr)

	orderID, err := boosterClient.LaunchBoost(context.Background(), percy.BoostOpportunity{
		ID:               uuid.New().String(),
		ChannelID:        channelID,
		StartedAt:        time.Now(),
		ExpiresAt:        time.Now().Add(time.Minute * 10),
		EndedAt:          nil,
		UnitsContributed: 1,
	})
	if err != nil {
		return errors.Wrap(err, "failed to create booster order")
	}

	fmt.Println("order id: ", orderID)

	return nil
}
