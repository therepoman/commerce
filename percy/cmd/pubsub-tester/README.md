
## Hype Train Pubsub Testing Utility
Use this guy to like fire pubsubs and stuff you know, jeez.

### Start Hype Train

#### Make the message
```bash
go run cmd/pubsub-tester/main.go -t make-hype-train-start-message -o cmd/pubsub-tester/start-hype-train-w-progress.json -c 108707191
```

#### Send the message you made
```bash
go run cmd/pubsub-tester/main.go -t hype-train-start -m cmd/pubsub-tester/start-hype-train-w-progress.json
```

### Hype Train Progression

#### Make the message
```bash
go run cmd/pubsub-tester/main.go -t make-hype-train-progression-message -o cmd/pubsub-tester/hype-train-progression-data.json -c 108707191 -s BITS -a CHEER -q 100 -p 100
```

#### Send the message you made
```bash
go run cmd/pubsub-tester/main.go -t hype-train-progression -m cmd/pubsub-tester/hype-train-progression-data.json
```

### Hype Train Level Up

#### Make the message
```bash
go run cmd/pubsub-tester/main.go -t make-hype-train-level-up-message -o cmd/pubsub-tester/hype-train-level-up-data.json -c 108707191 -p 1000 -l 2
```

#### Send the message you made
```bash
go run cmd/pubsub-tester/main.go -t hype-train-level-up -m cmd/pubsub-tester/hype-train-level-up-data.json -c 108707191
```

### End Hype Train

#### Make the message
```bash
go run cmd/pubsub-tester/main.go -t make-hype-train-end-message -o cmd/pubsub-tester/hype-train-end-data.json -r COMPLETE
```

#### Send the message you made
```bash
go run cmd/pubsub-tester/main.go -t hype-train-end -m cmd/pubsub-tester/hype-train-end-data.json -c 108707191
```

### Participant Reward

#### Make the message
```bash
go run cmd/pubsub-tester/main.go -t make-participant-rewards-message -o cmd/pubsub-tester/participant-rewards.json -u 230449069 -R 111111 -Q EMOTE
```

#### Send the message you made
```bash
go run cmd/pubsub-tester/main.go -t participant-rewards -m cmd/pubsub-tester/participant-rewards.json -c 108707191
```

### Conductor Update

#### Make the message
```bash
go run cmd/pubsub-tester/main.go -t make-conductor-update-message -o cmd/pubsub-tester/conductor-update.json -c 108707191 -s BITS -a CHEER -V 10000
```

#### Send the message you made
```bash
go run cmd/pubsub-tester/main.go -t conductor-update -m cmd/pubsub-tester/conductor-update.json
```