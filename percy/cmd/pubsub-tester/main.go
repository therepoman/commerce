package main

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"os"
	"time"

	"code.justin.tv/chat/pubsub-go-pubclient/client"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/badges"
	"code.justin.tv/commerce/percy/internal/mako"
	"code.justin.tv/commerce/percy/internal/metrics"
	"code.justin.tv/commerce/percy/internal/miniexperiments"
	"code.justin.tv/commerce/percy/internal/pubsub"
	"code.justin.tv/commerce/percy/internal/pubsub/rewards"
	"code.justin.tv/commerce/percy/internal/tests"
	"code.justin.tv/commerce/percy/internal/users"
	"code.justin.tv/foundation/twitchclient"
	"github.com/urfave/cli"
)

var (
	environment            string
	messageType            string
	messageContentFilename string
	outputFileName         string
	channelID              string
	userID                 string
	rewardID               string
	rewardType             string
	progressionSource      string
	progressionAction      string
	progressionQuantity    int
	progressionTotal       int
	progressionValue       int
	level                  int
	endReason              string
	goal                   int
	totalGoal              int
)

func main() {
	app := cli.NewApp()
	app.Name = "Percy Pubsub Publisher"
	app.Usage = "Publish Pubsub messages that Percy would publish, for testing."
	app.Action = publishToPubsub
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "the Pubsub environment to use. Can only be 'production' or 'staging'.",
			Destination: &environment,
		},
		cli.StringFlag{
			Name:        "type, t",
			Value:       "start",
			Usage:       "The Pubsub Message type to publish",
			Destination: &messageType,
		},
		cli.StringFlag{
			Name:        "message, m",
			Usage:       "The `FILE` that contains the Pubsub Message content to publish",
			Destination: &messageContentFilename,
		},
		cli.StringFlag{
			Name:        "output-file, o",
			Usage:       "the `FILE` that you want to output the program to",
			Destination: &outputFileName,
		},
		cli.StringFlag{
			Name:        "channel-id, c",
			Usage:       "the channel to post the message to",
			Destination: &channelID,
		},
		cli.StringFlag{
			Name:        "participation-source, s",
			Usage:       "the participation source for a progression event.",
			Destination: &progressionSource,
		},
		cli.StringFlag{
			Name:        "participation-action, a",
			Usage:       "the participation action for a progression event.",
			Destination: &progressionAction,
		},
		cli.IntFlag{
			Name:        "participation-quantity, q",
			Usage:       "the participation quantity for a progression event.",
			Destination: &progressionQuantity,
		},
		cli.IntFlag{
			Name:        "participation-total, p",
			Usage:       "the participation total for a progression event.",
			Destination: &progressionTotal,
		},
		cli.IntFlag{
			Name:        "participation-value, V",
			Usage:       "the participation aggregated value for a progression event.",
			Destination: &progressionValue,
		},
		cli.IntFlag{
			Name:        "level, l",
			Usage:       "the level of the hype train",
			Destination: &level,
		},
		cli.StringFlag{
			Name:        "end-reason, r",
			Usage:       "the ending reason for the hype train",
			Destination: &endReason,
		},
		cli.IntFlag{
			Name:        "goal, g",
			Usage:       "the goal of the hype train",
			Destination: &goal,
		},
		cli.IntFlag{
			Name:        "level-goal, T",
			Usage:       "the level goal of the hype train",
			Destination: &totalGoal,
		},
		cli.StringFlag{
			Name:        "user id, u",
			Usage:       "the userID to post the message to",
			Destination: &userID,
		},
		cli.StringFlag{
			Name:        "reward id, R",
			Usage:       "the ID of the reward (i.e. emoteID)",
			Destination: &rewardID,
		},
		cli.StringFlag{
			Name:        "reward type, Q",
			Usage:       "the type of reward (i.e. emote)",
			Destination: &rewardType,
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		logrus.Fatal(err)
	}
}

func publishToPubsub(c *cli.Context) error {
	cfg, err := config.LoadConfig(config.Environment(environment))
	if err != nil {
		logrus.WithError(err).Fatal("could not load config for environment")
	}

	userProfileDB, err := users.NewUserProfileDB(cfg.Endpoints.UsersService, cfg.Endpoints.TMI, cfg.Endpoints.Ripley, &metrics.NoopStatter{}, cfg.SOCKS5Addr)
	if err != nil {
		logrus.WithError(err).Fatal("could not initlaize user profile DB")
	}

	makoFetcher := mako.NewFetcher(cfg.Endpoints.Mako, &metrics.NoopStatter{})

	badgesClient, err := badges.NewClient(cfg.Endpoints.Badges, &metrics.NoopStatter{}, cfg.SOCKS5Addr)
	if err != nil {
		logrus.WithError(err).Fatal("could not initlaize badges client")
	}

	rewardsDisplayInfoFetcher := rewards.NewDisplayInfoFetcher(badgesClient, makoFetcher)

	rewardsDisplayInfoAttacher := rewards.NewRewardsDisplayInfoAttacher(rewardsDisplayInfoFetcher)

	pubConfig := twitchclient.ClientConf{
		Host: cfg.Endpoints.Pubsub,
	}
	pubsubClient, _ := client.NewPubClient(pubConfig)

	miniexperimentsClient := miniexperiments.NewNoopClient()
	publisher := pubsub.NewPublisher(pubsubClient, nil, "", userProfileDB, rewardsDisplayInfoAttacher, rewardsDisplayInfoFetcher, nil, nil, miniexperimentsClient, miniexperimentsClient, true, []string{})

	switch messageType {
	case "make-hype-train-start-message":
		return makeHypeTrainStartMessage()
	case "hype-train-start":
		return sendStartHypeTrainMessage(publisher)
	case "make-hype-train-progression-message":
		return makeHypeTrainProgressionMessage()
	case "hype-train-progression":
		return sendHypeTrainProgressionMessage(publisher)
	case "make-hype-train-level-up-message":
		return makeHypeTrainLevelUpMessage()
	case "hype-train-level-up":
		return sendHypeTrainLevelUpMessage(publisher)
	case "make-hype-train-end-message":
		return makeHypeTrainEndMessage()
	case "hype-train-end":
		return sendHypeTrainEndMessage(publisher)
	case "make-participant-rewards-message":
		return makeParticipantRewardsMessage()
	case "participant-rewards":
		return sendParticipantRewardsMessage(publisher)
	case "make-conductor-update-message":
		return makeHypeTrainConductorUpdateMessage()
	case "conductor-update":
		return sendHypeTrainConductorUpdateMessage(publisher)
	default:
		logrus.WithField("messgaeType", messageType).Fatal("invalid message type")
		return nil
	}
}

func sendStartHypeTrainMessage(publisher percy.Publisher) error {
	file, err := os.Open(messageContentFilename)
	if err != nil {
		logrus.WithError(err).Fatal("could not open file of message content")
	}

	decoder := json.NewDecoder(file)
	var msg percy.HypeTrainStartData

	err = decoder.Decode(&msg)
	if err != nil {
		logrus.WithError(err).Fatal("could not parse file for message")
	}

	err = publisher.StartHypeTrain(context.Background(), msg.HypeTrain)
	if err != nil {
		logrus.WithError(err).Error("could not publish start hype train message to pubsub")
	}

	logrus.Info("successfully published start hype train message")
	return nil
}

func makeHypeTrainStartMessage() error {
	hypeTrain := tests.ValidHypeTrain()

	if channelID != "" {
		hypeTrain.ChannelID = channelID
		hypeTrain.Config.ChannelID = channelID
	}

	now := time.Now()
	hypeTrain.StartedAt = now.Add(-5 * time.Minute)
	hypeTrain.UpdatedAt = now.Add(-1 * time.Minute)
	hypeTrain.ExpiresAt = now.Add(1 * time.Minute)
	endedAt := now.Add(5 * time.Minute)
	hypeTrain.EndedAt = &endedAt

	hypeTrainData := percy.HypeTrainStartData{
		HypeTrain: hypeTrain,
		Progress:  hypeTrain.LevelProgress(),
	}

	hypeTrainBytes, err := json.Marshal(&hypeTrainData)
	if err != nil {
		logrus.WithError(err).Fatal("could not marshal JSON")
	}

	var prettyJSON bytes.Buffer
	err = json.Indent(&prettyJSON, hypeTrainBytes, "", "  ")
	if err != nil {
		logrus.WithError(err).Fatal("could not format JSON indents")
	}

	err = ioutil.WriteFile(outputFileName, prettyJSON.Bytes(), 0o644)
	if err != nil {
		logrus.WithError(err).Fatal("could not write JSON to file")
	}

	return nil
}

type hypeTrainProgressionJSONData struct {
	Progress      percy.Progress      `json:"progress"`
	Participation percy.Participation `json:"participation"`
}

func sendHypeTrainProgressionMessage(publisher percy.Publisher) error {
	file, err := os.Open(messageContentFilename)
	if err != nil {
		logrus.WithError(err).Fatal("could not open file of message content")
	}

	decoder := json.NewDecoder(file)
	var hypeTrainProgressionData hypeTrainProgressionJSONData

	err = decoder.Decode(&hypeTrainProgressionData)
	if err != nil {
		logrus.WithError(err).Fatal("could not parse file for message")
	}

	err = publisher.PostHypeTrainProgression(context.Background(), hypeTrainProgressionData.Participation, hypeTrainProgressionData.Progress)
	if err != nil {
		logrus.WithError(err).Error("could not publish hype train progression message to pubsub")
	}

	logrus.Info("successfully published hype train progression message")
	return nil
}

func makeHypeTrainProgressionMessage() error {
	hypeTrain := tests.ValidHypeTrain()

	if channelID != "" {
		hypeTrain.ChannelID = channelID
		hypeTrain.Config.ChannelID = channelID
	}

	hypeTrainProgress := hypeTrain.LevelProgress()
	hypeTrainProgress.Total = percy.ParticipationPoint(progressionTotal)
	if progressionValue > 0 {
		hypeTrainProgress.Progression = percy.ParticipationPoint(progressionValue)
	} else {
		hypeTrainProgress.Progression = percy.ParticipationPoint(progressionTotal)
	}
	if level > 0 {
		hypeTrainProgress.Setting.Level = level
	}
	if goal > 0 {
		hypeTrainProgress.Goal = percy.ParticipationPoint(goal)
	}
	if totalGoal > 0 {
		hypeTrainProgress.Setting.Goal = percy.ParticipationPoint(totalGoal)
	}

	hypeTrainParticipation := percy.Participation{
		ChannelID: channelID,
		Timestamp: time.Now(),
		Quantity:  progressionQuantity,
		User:      percy.User{Id: "159222450"},
		ParticipationIdentifier: percy.ParticipationIdentifier{
			Action: percy.ParticipationAction(progressionAction),
			Source: percy.ParticipationSource(progressionSource),
		},
	}

	hypeTrainData := hypeTrainProgressionJSONData{
		Progress:      hypeTrainProgress,
		Participation: hypeTrainParticipation,
	}

	hypeTrainBytes, err := json.Marshal(&hypeTrainData)
	if err != nil {
		logrus.WithError(err).Fatal("could not marshal JSON")
	}

	var prettyJSON bytes.Buffer
	err = json.Indent(&prettyJSON, hypeTrainBytes, "", "  ")
	if err != nil {
		logrus.WithError(err).Fatal("could not format JSON indents")
	}

	err = ioutil.WriteFile(outputFileName, prettyJSON.Bytes(), 0o644)
	if err != nil {
		logrus.WithError(err).Fatal("could not write JSON to file")
	}

	return nil
}

func sendHypeTrainLevelUpMessage(publisher percy.Publisher) error {
	file, err := os.Open(messageContentFilename)
	if err != nil {
		logrus.WithError(err).Fatal("could not open file of message content")
	}

	decoder := json.NewDecoder(file)
	var hypeTrainProgress percy.Progress

	err = decoder.Decode(&hypeTrainProgress)
	if err != nil {
		logrus.WithError(err).Fatal("could not parse file for message")
	}

	newTimeToExpire := time.Now().Add(5 * time.Minute)

	err = publisher.LevelUpHypeTrain(context.Background(), channelID, newTimeToExpire, hypeTrainProgress)
	if err != nil {
		logrus.WithError(err).Error("could not publish level up hype train message to pubsub")
	}

	logrus.Info("successfully published level up hype train message")

	return nil
}

func makeHypeTrainLevelUpMessage() error {
	hypeTrain := tests.ValidHypeTrain()

	if channelID != "" {
		hypeTrain.ChannelID = channelID
		hypeTrain.Config.ChannelID = channelID
	}

	hypeTrainProgress := hypeTrain.LevelProgress()
	hypeTrainProgress.Total = percy.ParticipationPoint(progressionTotal)
	hypeTrainProgress.Progression = 0

	if level > 0 {
		hypeTrainProgress.Setting.Level = level
	}
	if goal > 0 {
		hypeTrainProgress.Goal = percy.ParticipationPoint(goal)
	}
	if totalGoal > 0 {
		hypeTrainProgress.Setting.Goal = percy.ParticipationPoint(totalGoal)
	}

	hypeTrainBytes, err := json.Marshal(&hypeTrainProgress)
	if err != nil {
		logrus.WithError(err).Fatal("could not marshal JSON")
	}

	var prettyJSON bytes.Buffer
	err = json.Indent(&prettyJSON, hypeTrainBytes, "", "  ")
	if err != nil {
		logrus.WithError(err).Fatal("could not format JSON indents")
	}

	err = ioutil.WriteFile(outputFileName, prettyJSON.Bytes(), 0o644)
	if err != nil {
		logrus.WithError(err).Fatal("could not write JSON to file")
	}

	return nil
}

type hypeTrainEndData struct {
	EndedAt   time.Time `json:"ended_at"`
	EndReason string    `json:"end_reason"`
}

func sendHypeTrainEndMessage(publisher percy.Publisher) error {
	file, err := os.Open(messageContentFilename)
	if err != nil {
		logrus.WithError(err).Fatal("could not open file of message content")
	}

	decoder := json.NewDecoder(file)
	var endData hypeTrainEndData

	err = decoder.Decode(&endData)
	if err != nil {
		logrus.WithError(err).Fatal("could not parse file for message")
	}

	hypeTrain := percy.HypeTrain{
		ChannelID: channelID,
	}

	err = publisher.EndHypeTrain(context.Background(), hypeTrain, endData.EndedAt, percy.EndingReason(endData.EndReason))
	if err != nil {
		logrus.WithError(err).Error("could not publish end hype train message to pubsub")
	}

	logrus.Info("successfully published end hype train message")

	return nil
}

func makeHypeTrainEndMessage() error {
	hypeTrainBytes, err := json.Marshal(&hypeTrainEndData{
		EndedAt:   time.Now(),
		EndReason: endReason,
	})
	if err != nil {
		logrus.WithError(err).Fatal("could not marshal JSON")
	}

	var prettyJSON bytes.Buffer
	err = json.Indent(&prettyJSON, hypeTrainBytes, "", "  ")
	if err != nil {
		logrus.WithError(err).Fatal("could not format JSON indents")
	}

	err = ioutil.WriteFile(outputFileName, prettyJSON.Bytes(), 0o644)
	if err != nil {
		logrus.WithError(err).Fatal("could not write JSON to file")
	}

	return nil
}

type participantRewardData struct {
	EntitledParticipants percy.EntitledParticipants `json:"entitled_participants"`
}

func makeParticipantRewardsMessage() error {
	participantRewardBytes, err := json.Marshal(&participantRewardData{
		EntitledParticipants: map[string][]percy.Reward{
			userID: {
				percy.Reward{
					Type:        percy.RewardType(rewardType),
					ID:          rewardID,
					GroupID:     "",
					RewardLevel: 3,
				},
			},
		},
	})
	if err != nil {
		logrus.WithError(err).Fatal("could not marshal json")
	}

	var prettyJSON bytes.Buffer
	err = json.Indent(&prettyJSON, participantRewardBytes, "", "  ")
	if err != nil {
		logrus.WithError(err).Fatal("could not format JSON indents")
	}

	err = ioutil.WriteFile(outputFileName, prettyJSON.Bytes(), 0o644)
	if err != nil {
		logrus.WithError(err).Fatal("could not write JSON to file")
	}

	return nil
}

func sendParticipantRewardsMessage(publisher percy.Publisher) error {
	file, err := os.Open(messageContentFilename)
	if err != nil {
		logrus.WithError(err).Fatal("could not open file of message content")
	}

	decoder := json.NewDecoder(file)
	var participantRewardData participantRewardData

	err = decoder.Decode(&participantRewardData)
	if err != nil {
		logrus.WithError(err).Fatal("could not parse file for message")
	}

	err = publisher.ParticipantRewards(context.Background(), channelID, participantRewardData.EntitledParticipants)
	if err != nil {
		logrus.WithError(err).Error("could not publish participant rewards to pubsub")
	}

	logrus.Info("successfully published participant rewards message")

	return nil
}

type hypeTrainConductorUpdateJSONData struct {
	Conductor percy.Conductor `json:"conductor"`
	ChannelID string          `json:"channel_id"`
}

func makeHypeTrainConductorUpdateMessage() error {
	participant := tests.ValidParticipant()

	if channelID != "" {
		participant.ChannelID = channelID
	}
	if progressionSource != "" && progressionAction != "" || progressionValue != 0 {
		participationIdentifier := percy.ParticipationIdentifier{
			Source: percy.ParticipationSource(progressionSource),
			Action: percy.ParticipationAction(progressionAction),
		}

		participant.ParticipationTotals[participationIdentifier] = progressionValue
	}

	conductor := percy.Conductor{
		Source:         percy.ParticipationSource(progressionSource),
		User:           participant.User,
		Participations: participant.ParticipationTotals,
	}

	conductorData := hypeTrainConductorUpdateJSONData{
		Conductor: conductor,
		ChannelID: participant.ChannelID,
	}

	conductorBytes, err := json.Marshal(&conductorData)
	if err != nil {
		logrus.WithError(err).Fatal("could not marshal json")
	}

	var prettyJSON bytes.Buffer
	err = json.Indent(&prettyJSON, conductorBytes, "", "")
	if err != nil {
		logrus.WithError(err).Fatal("could not format JSON indents")
	}

	err = ioutil.WriteFile(outputFileName, prettyJSON.Bytes(), 0o644)
	if err != nil {
		logrus.WithError(err).Fatal("could not write JSON to file")
	}

	return nil
}

func sendHypeTrainConductorUpdateMessage(publisher percy.Publisher) error {
	file, err := os.Open(messageContentFilename)
	if err != nil {
		logrus.WithError(err).Fatal("could not open file of message content")
	}

	decode := json.NewDecoder(file)
	var msg hypeTrainConductorUpdateJSONData

	err = decode.Decode(&msg)
	if err != nil {
		logrus.WithError(err).Fatal("could not parse file for message")
	}

	err = publisher.ConductorUpdate(context.Background(), msg.ChannelID, msg.Conductor)
	if err != nil {
		logrus.WithError(err).Panic("could not publish hype train conductor update to pubsub")
	}

	logrus.Info("successfully published hype train progression message")
	return nil
}
