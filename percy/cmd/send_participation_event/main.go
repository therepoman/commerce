package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"time"

	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/sirupsen/logrus"
)

const (
	stagingURL    = "https://sqs.us-west-2.amazonaws.com/999515204624/test-participation-events-staging"
	productionURL = "https://sqs.us-west-2.amazonaws.com/958761666246/test-participation-events-production"
)

func main() {
	region := flag.String("region", "us-west-2", "AWS account region")
	env := flag.String("env", string(config.Staging), "environment (staging or prod)")
	channelID := flag.String("channel", "123456789", "channel ID to send the event to")
	userID := flag.String("user", "987654321", "ID of the supporting user")
	source := flag.String("source", "BITS", "source of the participation event (SUBS or BITS)")
	action := flag.String("action", "CHEER", "action type of the participation event")
	quantity := flag.Int("quantity", 1, "quantity of the participation event")
	flag.Parse()

	var queueURL string
	if *env == string(config.Staging) {
		queueURL = stagingURL
	} else if *env == string(config.Production) {
		queueURL = productionURL
	} else {
		logrus.Panicf("Invalid environment %s", *env)
	}

	sess := session.Must(session.NewSession(&aws.Config{
		Region: aws.String(*region),
	}))

	client := sqs.New(sess)

	participation := percy.Participation{
		ChannelID: *channelID,
		User:      percy.NewUser(*userID),
		ParticipationIdentifier: percy.ParticipationIdentifier{
			Source: percy.ParticipationSource(*source),
			Action: percy.ParticipationAction(*action),
		},
		Quantity:  *quantity,
		Timestamp: time.Now(),
	}

	participationJSON, err := json.Marshal(participation)
	if err != nil {
		logrus.WithError(err).Panic("Failed to marshal participation")
	}

	fmt.Println(string(participationJSON))

	_, err = client.SendMessage(&sqs.SendMessageInput{
		QueueUrl:    aws.String(queueURL),
		MessageBody: aws.String(string(participationJSON)),
	})

	if err != nil {
		logrus.WithError(err).Panic("Failed to send message to SQS")
	}

	logrus.Info("Message successfully sent to SQS")
}
