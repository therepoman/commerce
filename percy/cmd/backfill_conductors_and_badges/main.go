package main

import (
	"bufio"
	"context"
	"encoding/csv"
	"flag"
	"fmt"
	"os"
	"path"
	"strings"
	"sync"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"code.justin.tv/sse/malachai/pkg/backoff"
	"github.com/pkg/errors"
)

const (
	initialTPS  = 10
	finalTPS    = 250
	rampUpTime  = 10 * time.Minute
	rampUpSteps = 20

	hypeTrainDBRegion  = "us-east-2" // we only read, so we read from replica table
	conductorsDBRegion = "us-west-2"
	badgesDBRegion     = "us-west-2"

	retryTimes = 3
	retryWait  = 1 * time.Second

	waitGroupsPerSecond = 10
)

// for progress file
const (
	progressFieldUserID = iota
	progressFieldChannelID
	progressFieldHypeTrainID
	progressFieldBadgeType
	progressFieldNumOfFields
	progressFilename = "backfilled_users_%s%s.csv"
)

// for failed trains
const (
	failedTrainID = iota
	failedTrainNumOfFields
	failedTrainFilename = "failed_train_%s%s.csv"
)

var (
	env    *string
	dryrun *bool
	input  *string
)

type backfillHandler struct {
	hypeTrainDB  *dynamodb.HypeTrainDB
	conductorsDB *dynamodb.ConductorsDB
	badgesDB     *dynamodb.BadgesDB

	totalCount   int
	currentCount int
	skippedCount int

	// for rate limiting
	startTime           time.Time
	initialTPS          int
	finalTPS            int
	rampUpTime          time.Duration
	rampUpSteps         int
	waitGroupsPerSecond int

	// go channels to prevent concurrent writes on progress file and failed trains file
	progressChan    chan progressRow
	failedTrainChan chan string
	fileWritersWG   sync.WaitGroup
}

type progressRow struct {
	userId    string
	channelId string
	trainId   string
	badgeType string
}

func main() {
	env = flag.String("env", string(config.Staging), "environment (staging or production)")
	dryrun = flag.Bool("dryrun", true, "dryrun (true/false)")
	input = flag.String("input", string("./train_list.csv"), "input file")
	flag.Parse()

	logrus.WithFields(logrus.Fields{
		"env":    *env,
		"dryrun": *dryrun,
		"input":  *input,
	}).Info("input parameters")

	inputFile, err := os.Open(*input)
	if err != nil {
		logrus.WithError(err).Panic("failed to open input file")
	}
	inputReader := csv.NewReader(bufio.NewReader(inputFile))
	rows, err := inputReader.ReadAll()
	if err != nil {
		logrus.WithError(err).Panic("failed to get rows from input file")
	}
	var trainIDs []string
	for _, row := range rows {
		trainIDs = append(trainIDs, row[0])
	}

	handler := backfillHandler{
		initialTPS:          initialTPS,
		finalTPS:            finalTPS,
		rampUpTime:          rampUpTime,
		rampUpSteps:         rampUpSteps,
		waitGroupsPerSecond: waitGroupsPerSecond,
		progressChan:        make(chan progressRow),
		failedTrainChan:     make(chan string),
	}

	err = handler.initializeDBs()
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize DBs")
	}

	handler.initializeSaveFileWorkers()
	handler.runBackfill(trainIDs)
}

func (b *backfillHandler) runBackfill(trainIDs []string) {
	b.startTime = time.Now()
	b.totalCount = len(trainIDs)
	b.currentCount = 0
	b.skippedCount = 0
	defer func() {
		logrus.WithFields(logrus.Fields{
			"env":           *env,
			"dryrun":        *dryrun,
			"totalCount":    b.totalCount,
			"finishedCount": b.currentCount,
			"skippedCount":  b.skippedCount,
			"timeTook":      time.Since(b.startTime),
		}).Info("Backfill Statistics")

		// notify file workers that backfill is finished
		close(b.progressChan)
		close(b.failedTrainChan)
		b.fileWritersWG.Wait()
	}()

	for {
		var wg sync.WaitGroup
		numOfGoroutines := b.getCurrentTPS(b.startTime) / waitGroupsPerSecond
		timer := time.NewTimer(time.Second / waitGroupsPerSecond)
		for i := 1; i <= numOfGoroutines && b.currentCount < len(trainIDs); i++ {
			trainId := trainIDs[b.currentCount]
			wg.Add(1)
			go func() {
				err := backoff.ConstantRetry(func() (err error) {
					return b.backfillConductorsAndBadges(trainId)
				}, retryTimes, retryWait)
				if err != nil {
					logrus.WithField("trainId", trainId).WithError(err).Warn("failed to backfill conductors and badges")
					b.failedTrainChan <- trainId
				}
				wg.Done()
			}()
			b.currentCount++
			if b.currentCount%100 == 0 {
				logrus.Infof("started backfilling %d HypeTrains", b.currentCount)
			}
		}
		// can only proceed if both all goroutines have finished and time allotted has been used up.
		wg.Wait()
		<-timer.C

		if b.currentCount >= len(trainIDs) {
			return
		}
	}
}

/*
since trains may come in out of order for the same channel,
for each train, we need to also check the most recent train

for each train:
	if train succeeded (gone past level 1):
		if train is most recent train:
			update conductor table from most recent train.
		  	grant conductors current conductor badge
		else:
			for each participation source (bits, subs)
				if conductor is not the same as most recent train
					grant former conductor badge
				(this check is needed b/c we don't want to override a current conductor's badge)
	else:
		do nothing (if the train didn't succeed, then it would not have granted any badges, so no actions)
*/
func (b *backfillHandler) backfillConductorsAndBadges(trainId string) error {
	ctx := context.Background()

	train, err := b.hypeTrainDB.GetHypeTrainByID(ctx, trainId)
	if err != nil {
		return errors.Wrap(err, "failed to get HypeTrain by ID")
	}

	if train != nil && train.EndingReason != nil && *train.EndingReason == percy.EndingReasonCompleted {
		logrus.WithField("trainId", trainId).Info("hypetrain was completed, backfilling")
		mostRecentTrain, err := b.hypeTrainDB.GetMostRecentHypeTrain(ctx, train.ChannelID)
		if err != nil {
			return errors.Wrap(err, "failed to get most recent HypeTrain")
		}

		log := logrus.WithFields(logrus.Fields{
			"trainId":   train.ID,
			"channelId": train.ChannelID,
			"dryrun":    *dryrun,
		})

		var badgeType string
		if train.ID == mostRecentTrain.ID {
			var conductors []percy.Conductor
			var conductorIDs []string
			for _, participationSource := range []percy.ParticipationSource{
				percy.SubsSource,
				percy.BitsSource,
			} {
				conductor := train.Conductors[participationSource].User
				conductors = append(conductors, percy.Conductor{
					User: conductor,
				})
				if conductor.ID() != "" && !inSlice(conductor.ID(), conductorIDs) {
					conductorIDs = append(conductorIDs, conductor.ID())
				}
			}

			log = log.WithField("conductorIDs", conductorIDs)
			log.Info("adding conductors to conductors table from most recent train")
			badgeType = string(percy.CurrentConductorBadge)

			if !*dryrun {
				err = b.conductorsDB.SetConductors(ctx, train.ChannelID, conductors)
				if err != nil {
					return errors.Wrap(err, "failed to add conductors to conductors table")
				}
			}

			if len(conductorIDs) > 0 {
				log.Info("giving participants current conductor badge")
				if !*dryrun {
					err = b.badgesDB.SetBulkBadges(ctx, conductorIDs, train.ChannelID, []percy.Badge{
						percy.CurrentConductorBadge,
					})
					if err != nil {
						return errors.Wrap(err, "failed to give current conductor badge to participant")
					}
				}

				for _, fixedConductor := range conductorIDs {
					b.progressChan <- progressRow{
						userId:    fixedConductor,
						channelId: train.ChannelID,
						trainId:   train.ID,
						badgeType: badgeType,
					}
				}
			}

		} else {
			for _, participationSource := range []percy.ParticipationSource{
				percy.SubsSource,
				percy.BitsSource,
			} {
				log = log.WithField("participationSource", participationSource)

				conductorByParticipation, ok := train.Conductors[participationSource]
				if !ok {
					log.Info("no conductor")
					continue
				}
				conductorIDByParticipation := conductorByParticipation.User.ID()

				conductorByParticipationMostRecent, ok := mostRecentTrain.Conductors[participationSource]
				var conductorIDByParticipationMostRecent string
				if !ok {
					log.Info("no conductor in most recent hype train")
					conductorIDByParticipationMostRecent = ""
				} else {
					conductorIDByParticipationMostRecent = conductorByParticipationMostRecent.User.ID()
				}

				if conductorIDByParticipation != conductorIDByParticipationMostRecent {
					log.Info("giving participants former conductor badge")
					badgeType = string(percy.FormerConductorBadge)

					if !*dryrun {
						err = b.badgesDB.SetBadges(ctx, conductorIDByParticipation, train.ChannelID, []percy.Badge{
							percy.FormerConductorBadge,
						})
						if err != nil {
							return errors.Wrap(err, "failed to give former conductor badge to participant")
						}
					}

					b.progressChan <- progressRow{
						userId:    conductorIDByParticipation,
						channelId: train.ChannelID,
						trainId:   train.ID,
						badgeType: badgeType,
					}
				}
			}
		}
	} else {
		logrus.WithField("trainId", trainId).Info("hypetrain did not complete, skipping")
		b.skippedCount++
	}
	return nil
}

func (b *backfillHandler) initializeDBs() error {
	hypeTrainDB, err := dynamodb.NewHypeTrainDB(dynamodb.DynamoDBConfig{
		Region:      hypeTrainDBRegion,
		Environment: *env,
	})
	if err != nil {
		return errors.Wrap(err, "Failed to initialize HypeTrainDB")
	}

	conductorsDB, err := dynamodb.NewConductorsDB(dynamodb.DynamoDBConfig{
		Region:      conductorsDBRegion,
		Environment: *env,
	})
	if err != nil {
		return errors.Wrap(err, "Failed to initialize ConductorsDB")
	}

	badgesDB, err := dynamodb.NewBadgesDB(dynamodb.DynamoDBConfig{
		Region:      badgesDBRegion,
		Environment: *env,
	})
	if err != nil {
		return errors.Wrap(err, "Failed to initialize BadgesDB")
	}

	b.hypeTrainDB = hypeTrainDB
	b.conductorsDB = conductorsDB
	b.badgesDB = badgesDB
	return nil
}

func (b *backfillHandler) initializeSaveFileWorkers() {
	var dryrunString string
	if *dryrun {
		dryrunString = "_dryrun"
	}

	b.fileWritersWG.Add(1)
	go func() {
		outputName := fmt.Sprintf(progressFilename, *env, dryrunString)
		shouldAddHeaderRow := !progressFileExists(outputName)
		f, err := os.OpenFile(outputName, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0o666)
		if err != nil {
			logrus.WithError(err).Panic("Failed to open progress writer")
		}
		if shouldAddHeaderRow {
			headers := []string{
				"userId",
				"channelId",
				"trainId",
				"badgeType",
			}
			_, err = f.WriteString(strings.Join(headers[:], ",") + "\n")
			if err != nil {
				logrus.WithError(err).Panic("Failed to open progress writer")
			}
		}

		for {
			progress, ok := <-b.progressChan
			if ok {
				err = saveProgress(progress.userId, progress.channelId, progress.trainId, progress.badgeType, f)
				if err != nil {
					logrus.WithError(err).WithField("progress", fmt.Sprintf("%+v", progress)).
						Panic("Failed to save progress")
				}
			} else {
				_ = f.Close()
				b.fileWritersWG.Done()
				return
			}
		}
	}()

	b.fileWritersWG.Add(1)
	go func() {
		var f *os.File
		var err error
		for {
			failedTrainId, ok := <-b.failedTrainChan
			if ok {
				// only open the file if we get a failed hypetrain
				if f == nil {
					outputName := fmt.Sprintf(failedTrainFilename, *env, dryrunString)
					f, err = os.OpenFile(outputName, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0o666)
					if err != nil {
						logrus.WithError(err).Panic("Failed to open failed train file writer")
					}
				}
				err = saveFailedTrain(failedTrainId, f)
				if err != nil {
					logrus.WithError(err).WithField("failedTrainId", failedTrainId).
						Panic("Failed to save failed train id")
				}
			} else {
				_ = f.Close()
				b.fileWritersWG.Done()
				return
			}
		}
	}()
}

// based on startTime it returns the appropriate currentTPS for goroutine control
func (b *backfillHandler) getCurrentTPS(startTime time.Time) int {
	timeDiff := time.Since(startTime)
	if timeDiff > b.rampUpTime {
		return b.finalTPS
	}
	timePerStep := time.Duration(float64(b.rampUpTime) / float64(b.rampUpSteps))
	stepsTaken := int(timeDiff / timePerStep)
	stepSize := b.finalTPS / b.rampUpSteps
	return b.initialTPS + stepsTaken*stepSize
}

func saveProgress(userId, channelId, trainId, badgeType string, f *os.File) error {
	var progress [progressFieldNumOfFields]string
	progress[progressFieldUserID] = userId
	progress[progressFieldChannelID] = channelId
	progress[progressFieldHypeTrainID] = trainId
	progress[progressFieldBadgeType] = badgeType

	_, err := f.WriteString(strings.Join(progress[:], ",") + "\n")
	if err != nil {
		return errors.Wrap(err, "error writing data row to progress file")
	}
	return nil
}

func saveFailedTrain(trainId string, f *os.File) error {
	var progress [failedTrainNumOfFields]string
	progress[failedTrainID] = trainId

	_, err := f.WriteString(strings.Join(progress[:], ",") + "\n")
	if err != nil {
		return errors.Wrap(err, "error writing data row to failed train file")
	}
	return nil
}

func progressFileExists(progressFileName string) bool {
	dir, err := os.Getwd()
	if err != nil {
		logrus.WithError(err).Panic("error getting working directory")
	}
	dest := path.Join(dir, progressFileName)
	info, err := os.Stat(dest)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func inSlice(str string, list []string) bool {
	for _, item := range list {
		if str == item {
			return true
		}
	}
	return false
}
