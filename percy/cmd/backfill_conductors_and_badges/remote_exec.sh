NAME=backfill_conductors_and_badges
FILE=test.csv
COMMAND="./$NAME --input=$FILE --env=staging --dryrun"
TC_NAME=twitch-percy-aws-devo
DIR=backfill
PROFILE=backfill

function main() {
  echo -------------------------------
  echo "Welcome to Remote Execution CLI"
  echo "1. Start Remote Execution"
  echo "2. Check Remote Process"
  echo "3. Clean up Remote Environment"
  read -p "What do you want to do (1,2,3,exit)? " ans

  if [ $ans == 1 ]; then
    echo "Starting Remote Execution..."
    start_remote_execution
  elif [ $ans == 2 ]; then
    echo "Checking Remote Process..."
    check_remote_proces
  elif [ $ans == 3 ]; then
    echo "Prepping Cleanup..."
    cleanup
  else
    echo "exiting..."
  fi
  echo -------------------------------
}

function start_remote_execution() {
  echo "Bulding..."
  GOOS=linux go build
  echo "To be executed: $COMMAND"

  read -p "Upload Assets (y/n)? " ans

  if [ $ans == y ]; then
    TC=$TC_NAME ssh jumpbox "mkdir -p $DIR/logs"
    TC=$TC_NAME scp ./$NAME "jumpbox:$DIR"
    TC=$TC_NAME scp ./$FILE "jumpbox:$DIR"
  fi

  TIMESTAMP=$(date '+%s')
  LOGNAME="BACKFILL_${TIMESTAMP}.log"
  COMMAND='$(AWS_PROFILE='$PROFILE' nohup '$COMMAND' > logs/'$LOGNAME' 2>&1 &)'
  echo $COMMAND
  echo -------------------------------
  TC=$TC_NAME ssh jumpbox "cd $DIR\
    && echo \"\"\
    && echo \"logs will be stored in $DIR/logs/$LOGNAME , to download:\"\
    && echo \"TC=$TC_NAME scp \"jumpbox:$DIR/logs/$LOGNAME\" $LOGNAME\"\
    && echo \"\"\
    && $COMMAND\
    & exit"
  echo "Script is now remotely executing, rerun this script and select 2 to check progress"
}

function check_remote_proces() {
  echo -------------------------------
  TC=$TC_NAME ssh jumpbox "\
    echo ------------------------------- ;\
    ps aux | head -1 ;\
    ps aux | grep [backfill]_conductors_and_badges"
}


function cleanup() {
  echo -------------------------------
  read -p "YOU ARE ABOUT TO CLEAN UP THE REMOTE ENVIRONMENT, ARE YOU SURE (y/n)? " ans
  if [ $ans == 'y' ]; then
    echo 'deleting...'
    TC=$TC_NAME ssh jumpbox "rm -rf $DIR"
  else
    echo 'aborted'
  fi
}

main