# Instructions to run in jumpbox

## Set up AWS Creds in Jumpbox
By default jumpbox does not have access to Dynamo, and we should not mess w/ Jumpbox settings.
The way around this is to create your own creds and have your execution use that.

AWS console → IAM → Users → your user, and create a new access key, we'll only use this for this backfill so you should delete it after the backfill is finished.

Inside jumpbox run `aws configure --profile backfill`.
it will prompt you to enter the creds, the creds are stored in `~/.aws/credentials`

## Run remote execution script
change the settings at the top of `remote_exec.sh` accordingly

- `NAME`: name of executable, don't change this
- `FILE`: name of input csv 
- `COMMAND`: the actual command to be executed, change the flags accordingly
- `TC_NAME`: the name of the jumpbox you're teleporting into
- `DIR`: where your script/assets will be stored on the jumpbox inside your user folder `~/`
- `PROFILE`: name of your AWS profile that you just set in the jumpbox (default: `backfill`)

`./remote_exec.sh` to run

## After finish
- run cleanup script (option 3)
- deactivate/remove creds in AWS console
- remove `~/.aws` folder in jumpbox
