package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	pubsub_control "code.justin.tv/chat/pubsub-control/client"
	"code.justin.tv/chat/pubsub-go-pubclient/client"
	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/badges"
	"code.justin.tv/commerce/percy/internal/mako"
	"code.justin.tv/commerce/percy/internal/metrics"
	"code.justin.tv/commerce/percy/internal/miniexperiments"
	"code.justin.tv/commerce/percy/internal/pubsub"
	"code.justin.tv/commerce/percy/internal/pubsub/rewards"
	"code.justin.tv/commerce/percy/internal/sandstorm"
	"code.justin.tv/commerce/percy/internal/subscriptions"
	"code.justin.tv/commerce/percy/internal/users"
	"code.justin.tv/commerce/percy/internal/wrapper"
	"code.justin.tv/foundation/twitchclient"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"github.com/urfave/cli"
)

var (
	environment string
	userID      string
	intensity   string
	channelID   string
	isDryRun    bool
)

func main() {
	app := cli.NewApp()
	app.Name = "Celebration publisher"
	app.Usage = "Sends test celebration pubsub"

	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:        "environment, e",
			Usage:       "the environment you're publishing to. Can be either 'staging' or 'production'.",
			Value:       "staging",
			Destination: &environment,
		},
		&cli.StringFlag{
			Name:        "user-id, u",
			Usage:       "the user ID the celebration purchase is from.",
			Destination: &userID,
		},
		&cli.StringFlag{
			Name:        "channel-id, c",
			Usage:       "the channel ID the celebration purchase was made in.",
			Destination: &channelID,
		},
		&cli.StringFlag{
			Name:        "intensity, i",
			Usage:       "the intensity of the celebration purchased. Should be 'SMALL', 'MEDIUM', or 'LARGE'.",
			Destination: &intensity,
		},
		&cli.BoolFlag{
			Name:        "is-dry-run",
			Usage:       "denotes if this is a dry run, just so we don't send bad messages",
			Destination: &isDryRun,
		},
	}

	app.Action = publishCelebration
	err := app.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Fatal("script failed")
	}
}

func publishCelebration(c *cli.Context) error {
	cfg, err := config.LoadConfig(config.Environment(environment))
	if err != nil {
		return errors.Wrap(err, "failed to create percy config")
	}

	statter := &metrics.NoopStatter{}

	sandstormClient, err := sandstorm.New(cfg)
	if err != nil {
		logrus.WithError(err).Panic("failed to initiate sandstorm client")
	}

	hypeTrainLiveSlackWebHookSecret, err := sandstormClient.GetSecret(cfg.HypeTrainLiveSlackHookSecretName)
	if err != nil || hypeTrainLiveSlackWebHookSecret == nil {
		logrus.WithError(err).Panic("failed to get secrets from sandstorm client")
	}
	HypetrainLiveSlackWebhookURL := string(hypeTrainLiveSlackWebHookSecret.Plaintext)

	userProfileDB, err := users.NewUserProfileDB(cfg.Endpoints.UsersService, cfg.Endpoints.TMI, cfg.Endpoints.Ripley, statter, cfg.SOCKS5Addr)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize users profile DB")
	}

	pubsubClient, err := client.NewPubClient(twitchclient.ClientConf{
		Host: cfg.Endpoints.Pubsub,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(cfg.SOCKS5Addr),
		},
	})
	if err != nil {
		logrus.WithError(err).Panic("failed to init pubsub client")
	}

	control, err := pubsub_control.NewClient(twitchclient.ClientConf{
		Host: cfg.Endpoints.PubsubControl,
		RoundTripperWrappers: []func(http.RoundTripper) http.RoundTripper{
			wrapper.NewProxyRoundTripWrapper(cfg.SOCKS5Addr),
		},
		Stats: statter,
	})
	if err != nil {
		logrus.WithError(err).Panic("failed to init pubsub control client")
	}

	badgesClient, err := badges.NewClient(cfg.Endpoints.Badges, statter, cfg.SOCKS5Addr)
	if err != nil {
		logrus.WithError(err).Panic("Failed to initialize badges client")
	}

	subscriptionsDB, err := subscriptions.NewSubscriptionsDB(cfg.Endpoints.Subscriptions, statter, cfg.SOCKS5Addr)
	if err != nil {
		logrus.WithError(err).Panic("failed to initialize subscriptions DB")
	}

	makoFetcher := mako.NewFetcher(cfg.Endpoints.Mako, statter)
	rewardsDisplayInfoFetcher := rewards.NewDisplayInfoFetcher(badgesClient, makoFetcher)
	rewardsDisplayInfoAttacher := rewards.NewRewardsDisplayInfoAttacher(rewardsDisplayInfoFetcher)
	miniexperimentsClient := miniexperiments.NewNoopClient()

	// TODO: leaky abstraction. Shouldn't need [rewardsDisplayInfoAttacher, subscriptionsDB, makoFetcher] to init the pubsub client.
	publisher := pubsub.NewPublisher(pubsubClient, control, HypetrainLiveSlackWebhookURL, userProfileDB, rewardsDisplayInfoAttacher, rewardsDisplayInfoFetcher, subscriptionsDB, makoFetcher, miniexperimentsClient, miniexperimentsClient, false, []string{})

	celebration, err := percy.MakePurchasableCelebration(fmt.Sprintf("testing-celebration.%s", uuid.Must(uuid.NewV4()).String()), percy.CelebrationSize(intensity))
	if err != nil {
		return errors.Wrap(err, "failed to make celebration struct")
	}

	if !isDryRun {
		err := publisher.StartCelebration(context.Background(), channelID, percy.User{Id: userID}, 1, celebration)
		if err != nil {
			return errors.Wrap(err, "failed to publish message to SNS")
		}

		logrus.Infof("successfully published event to %s", cfg.Endpoints.Pubsub)
	} else {
		logrus.Infof("would be sending this message to topic %s", cfg.Endpoints.Pubsub)
	}

	messageJSON, err := json.Marshal(&celebration)
	if err != nil {
		return errors.Wrap(err, "failed to marshal JSON for celebration")
	}

	var prettyJSON bytes.Buffer
	err = json.Indent(&prettyJSON, messageJSON, "", "  ")
	if err != nil {
		return errors.Wrap(err, "could not format JSON indents")
	}

	fmt.Println(prettyJSON.String())

	return nil
}
