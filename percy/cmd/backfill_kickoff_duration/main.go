package main

import (
	"bufio"
	"context"
	"encoding/csv"
	"errors"
	"flag"
	"io"
	"os"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"golang.org/x/time/rate"
)

var kickoffDuration = 10 * 60 * time.Second

// Example command:
// go run main.go --dryRun=false --unset=false --env=prod

func main() {
	region := flag.String("region", "us-west-2", "AWS account region")
	env := flag.String("env", string(config.Staging), "environment (staging or prod)")
	input := flag.String("input", string("./channel_allowlist.csv"), "input file")
	unset := flag.Bool("unset", false, "Set this field to true to remove kickoff duration from recommended config")
	dryRun := flag.Bool("dryRun", true, "Set this field to true to actually perform the operation, otherwise just log")
	flag.Parse()

	logrus.Infof("Running against %s in %s. Dry Run: %t. Unset: %t", *env, *region, *dryRun, *unset)
	db, err := dynamodb.NewHypeTrainConfigsDB(dynamodb.DynamoDBConfig{
		Region:      *region,
		Environment: *env,
	})
	if err != nil {
		logrus.WithError(err).Panic("Failed to initiated hype train configs DB")
	}

	rateLimiter := rate.NewLimiter(rate.Limit(100), 100)

	inputFile, err := os.Open(*input)
	if err != nil {
		logrus.WithError(err).Panic("Failed to open input file")
	}

	inputReader := csv.NewReader(bufio.NewReader(inputFile))
	count := 0
	for {
		channelID, err := inputReader.Read()
		if errors.Is(err, io.EOF) {
			break
		}

		if err != nil {
			logrus.WithError(err).Panic("Failed to read channel from file")
		}

		err = rateLimiter.Wait(context.Background())
		if err != nil {
			logrus.WithError(err).Panic("failed to rate limit")
		}
		if *unset {
			if !*dryRun {
				logrus.Infof("Removing kickoff duration for channel %s", channelID[0])
				err = db.UnsetRecommendedConfig(context.Background(), channelID[0], percy.HypeTrainConfigUnset{
					KickoffDuration: true,
				})
			} else {
				logrus.Infof("Will remove kickoff duration for channel %s", channelID[0])
			}
		} else {
			if !*dryRun {
				logrus.Infof("Updating kickoff duration for channel %s to %d seconds", channelID[0], kickoffDuration/time.Second)
				err = db.UpdateRecommendedConfig(context.Background(), channelID[0], percy.HypeTrainConfigUpdate{
					KickoffDuration: &kickoffDuration,
				})
			} else {
				logrus.Infof("Will update kickoff duration for channel %s to %d seconds", channelID[0], kickoffDuration/time.Second)
			}
		}

		if err != nil {
			logrus.WithError(err).Panic("failed to update channel's recommended config")
		}

		count += 1
		if count%100 == 0 {
			logrus.Infof("backfilled %d channels", count)
		}
	}
	logrus.Infof("Finished! Backfilled %d channels", count)
}
