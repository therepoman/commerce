package main

import (
	"bufio"
	"context"
	"encoding/csv"
	"errors"
	"flag"
	"io"
	"os"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/percy/config"
	percy "code.justin.tv/commerce/percy/internal"
	"code.justin.tv/commerce/percy/internal/dynamodb"
	"code.justin.tv/commerce/percy/internal/mako"
	"code.justin.tv/commerce/percy/internal/metrics"
	"golang.org/x/time/rate"
)

func main() {
	region := flag.String("region", "us-west-2", "AWS account region")
	env := flag.String("env", string(config.Staging), "environment (staging or production)")
	input := flag.String("input", string(",/input.csv"), "input file")
	flag.Parse()

	cfg, err := config.LoadConfig(config.Environment(*env))
	if err != nil || cfg == nil {
		logrus.WithError(err).Panic("Error loading config")
	}

	hypeTrainDB, err := dynamodb.NewHypeTrainDB(dynamodb.DynamoDBConfig{
		Region:      *region,
		Environment: *env,
	})
	if err != nil {
		logrus.WithError(err).Panic("Failed to initiated hype train configs DB")
	}

	engine := percy.NewEngine(percy.EngineConfig{
		HypeTrainDB:   hypeTrainDB,
		EntitlementDB: mako.NewEntitlementDB(cfg.Endpoints.Mako, &metrics.NoopStatter{}, cfg.SOCKS5Addr),
	})

	rateLimiter := rate.NewLimiter(rate.Limit(10), 10)

	inputFile, err := os.Open(*input)
	if err != nil {
		logrus.WithError(err).Panic("Failed to open input file")
	}

	inputReader := csv.NewReader(bufio.NewReader(inputFile))
	count := 0
	for {
		err = rateLimiter.Wait(context.Background())
		if err != nil {
			logrus.WithError(err).Panic("failed to rate limit")
		}

		row, err := inputReader.Read()
		if errors.Is(err, io.EOF) {
			break
		}

		if err != nil {
			logrus.WithError(err).Error("Filed to read input line")
			continue
		}

		// skip header
		if count == 0 {
			count += 1
			continue
		}

		if len(row) != 2 {
			logrus.Errorf("invalid row found %+v", row)
			continue
		}

		hypeTrainID := row[0]
		userID := row[1]

		hypeTrain, err := hypeTrainDB.GetHypeTrainByID(context.Background(), hypeTrainID)
		if err != nil {
			logrus.WithError(err).Error("failed to get hype train")
			continue
		}

		if hypeTrain == nil {
			logrus.Errorf("no hype train record found wiht ID %s", hypeTrainID)
			continue
		}

		reward, err := engine.AdminEntitleParticipant(context.Background(), *hypeTrain, userID)
		if err != nil {
			logrus.WithError(err).Error("failed to entitle user a hype train reward")
			continue
		}

		logrus.Infof("Entitled user %s a %s reward (%s)", userID, reward.Type, reward.ID)
	}

	logrus.Info("Done")
}
