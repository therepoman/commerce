# Grant Hype Train Rewards Script

Use this script to grant hype train rewards to specific users. 

Given a list of tuples (hype train ID, userID), the script fetches the hype train instance and grants emote rewards (based on the completed level of the hype train) to the specified user. The script runs the same reward selection logic as the service workflow, except that it doesn't validate the user has made enough contribution to be eligible for rewards. This is intentional for cases where we need to grant user rewards who contributed just slightly outside of hype train's time window (e.g. due to UI/UX bugs).

The script can be executed as
```
AWS_PROFILE={your_prod_aws_profile} go run cmd/grant_hype_train_reward/main.go -env=production -input=cmd/grant_hype_train_reward/input.csv
```

The input file should be a csv file where each line is in the format of {hype_train_id},{user_id}. The first line is treated as the header and not processed.
