# percy
:steam_locomotive: All Aboard the Hype Train!

## Endpoints

| Environment | Endpoint |
| --- | --- |
| Staging | main.us-west-2.beta.percy.twitch.a2z.com |
| Prod | main.us-west-2.prod.percy.twitch.a2z.com |

## Teleport Bastion Jumpbox
| Environment | Command |
| --- | --- |
| Staging | `TC=twitch-percy-aws-devo ssh jumpbox` |
| Prod | `TC=twitch-percy-aws-prod ssh jumpbox` |

## AWS Accounts
The percy AWS accounts are maintained in [isengard](https://isengard.amazon.com). 

| Environment | Account | Admin Console Login |
| --- | --- | --- |
| Dev / Staging | [twitch-percy-aws-devo@amazon.com](https://isengard.amazon.com/account/999515204624) | [Admin](https://isengard.amazon.com/federate?account=999515204624&role=Admin) |
| Prod | [twitch-percy-aws-prod@amazon.com](https://isengard.amazon.com/account/958761666246) | [Admin](https://isengard.amazon.com/federate?account=958761666246&role=Admin) |

## AWS Credentials
To run percy locally and/or perform terraform updates, you will need an IAM user with credentials for the appropriate AWS account. This can be done easily with an internal Amazon tool called ADA. Before you can install this, make sure you are on the Amazon network (WPA2).

First, install [Toolbox](https://w.amazon.com/index.php/BuilderToolbox/GettingStarted):
```bash	
/usr/bin/curl --negotiate -fLSsu: 'https://drive.corp.amazon.com/view/BuilderToolbox/toolbox-install.sh' -o /tmp/toolbox-install.sh && /bin/bash /tmp/toolbox-install.sh	
```

Then, install ADA:
```bash
toolbox install ada
```

Finally, add the following to your `~/.aws/credentials` file using the editor of your choice:

```text
[twitch-percy-aws-devo]
region=us-west-2
credential_process=ada credentials print --provider=isengard --account=999515204624 --role=Admin

[twitch-percy-aws-prod]
region=us-west-2
credential_process=ada credentials print --provider=isengard --account=958761666246 --role=LimitedAdmin
```

When trying to run Percy locally, you'll be prompted to sign in via the equivalent of running `mwinit --aea` if you haven't done so recently.

## Terraform

Place all your terraform in the `terraform/percy` module. Do not place it anywhere else.

### Planning

```bash
make plan env=staging
make plan env=production
```

### Applying

```bash
make apply env=staging
# Only apply to production when your PR has been approved and is ready to be merged in.
make apply env=production
```

## Twirp
[Twirp](https://git-aws.internal.justin.tv/common/twirp) is a Twitch internal RPC system for service to service communication.

To add a new API, define the requests and responses in the `rpc/percy-service.proto` file and then run:
```sh
make generate_twirp
```

## Dependency Management
Run make vendor to pull in new dependencies.
To require dependencies use a specific version range modify the `go.sum` file.


## Mocks
[Counterfeiter](https://github.com/maxbrunsfeld/counterfeiter) is used to generate mocks for golang interfaces.

To generate a mock for an interface, add a directive to the source file:
```
//go:generate counterfeiter . MyInterface
```
Generate all mocks:
```bash
make generate_mocks
```

## Testing

### unit test
```sh
make unit_test
```

### integration tests (staging)
Requires AWS credentials, and inside the [Amazon VPC](https://aws.amazon.com/vpc/).
```sh
make go_integration_tests_staging
```
If you try to run this locally, you might see a lot of tests fail due of `ServiceUnavailable: No Routes found` because you're not inside the VPC.

### integration tests (local)
Requires AWS credentials, make sure `AWS_PROFILE` is set to the correct profile
- spin up the server in one terminal
```sh
make run
```
- then in another terminal
```sh
make go_integration_tests_local
```
If there are any failed tests, chances are the test is trying to access something inside the Amazon VPC. Determine if the failed test impacts the feature/bug you're working on to decide whether you need this one to pass locally.

### Testing executing a hype train (production)
Requires AWS credentials for the role `hype-train-test-publisher` configured in `.aws/credentials`. I recommend using the [Isengard script](https://wiki.xarth.tv/pages/viewpage.action?pageId=228372569) and this config:
```
[percy-aws-prod-tester] 
credential_process = /usr/local/bin/isengard_credentials --account-id 958761666246 --role hype-train-test-publisher
region=us-west-2
```

then you can run the following command to start a hype train and run through all the levels

```sh
./scripts/kickoff-hypetrain.sh {channelID} production percy-aws-prod-tester
```

## Deployment

Follow the instruction below to deploy your change:

1. Make a branch for your change.
2. In Twitch network, go to [clean deploy tool](https://clean-deploy.internal.justin.tv/#/commerce/percy)
3. Beside each of the branches, there is a dropdown. Choose the environment you want to deploy your change to, fill out the reason, and hit "Ship it".
