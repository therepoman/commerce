package log

import (
	"bytes"
	"code.justin.tv/commerce/GoLog/src/log/level"
	"errors"
	"strings"
	"testing"
	"time"
)

type TestWriter struct {
	buffer  bytes.Buffer
	flushed bool
	r       chan bool
}

func NewTestWriter() *TestWriter {
	writer := TestWriter{
		bytes.Buffer{},
		false,
		make(chan bool, 1),
	}
	return &writer
}

func (t *TestWriter) Write(v []byte) (n int, err error) {
	n, err = t.buffer.Write(v)
	t.r <- true
	return n, err
}

func (t *TestWriter) Flush() error {
	t.flushed = true
	return nil
}

func (t *TestWriter) String() string {
	return t.buffer.String()
}

const (
	defaultTimeout = 1 * time.Second
	defaultAppName = "Tester"
	testString1    = "Test String 1"
)

func Test_NewAmznLogger_NilWriter(t *testing.T) {
	_, err := NewAmznLogger(nil, defaultAppName)
	if err == nil {
		t.Fatalf("Expected an error, however none was returned!")
	}
}

func Test_NewAmznLogger_OptionsError(t *testing.T) {
	errorOption := func(*AmznLogger) error {
		return errors.New("Expected")
	}
	_, err := NewAmznLogger(NewTestWriter(), defaultAppName, errorOption)
	if err == nil {
		t.Fatalf("Expected an error, however none was returned!")
	}
}

func Test_NewAmznLogger_NoOptionsError(t *testing.T) {
	option := func(*AmznLogger) error { return nil }
	_, err := NewAmznLogger(NewTestWriter(), defaultAppName, option)
	if err != nil {
		t.Fatalf("Unexpected error returned. Error: %v", err)
	}
}

func Test_LogLevel(t *testing.T) {
	level := level.Fatal
	l, err := NewAmznLogger(NewTestWriter(), defaultAppName, LogLevel(level))
	if err != nil {
		t.Fatalf("Unexpected error returned. Error: %v", err)
	}
	if l.defaultLevel != level {
		t.Fatalf("Expected log level: %v. Actual log level was: %v.", level, l.defaultLevel)
	}
}

func Test_DateFormat(t *testing.T) {
	format := "this is a test format"
	l, err := NewAmznLogger(NewTestWriter(), defaultAppName, DateFormat(format))
	if err != nil {
		t.Fatalf("Unexpected error returned. Error: %v", err)
	}
	if l.dateFormat != format {
		t.Fatalf("Expected dateFormat: %v. Acutal dateFormat was: %v.", format, l.dateFormat)
	}
}

func Test_BufferSize_NegativeSize(t *testing.T) {
	size := -1
	_, err := NewAmznLogger(NewTestWriter(), defaultAppName, BufferSize(size))
	if err == nil {
		t.Fatalf("Expected an error, however none was returned!")
	}
}

func Test_BufferSize_Zero(t *testing.T) {
	size := 0
	l, err := NewAmznLogger(NewTestWriter(), defaultAppName, BufferSize(size))
	if err != nil {
		t.Fatalf("Unexpected error returned. Error: %v.", err)
	}
	if cap(l.writeChan) != size {
		t.Fatalf("Expected writeChan size: %v. Actual writeChan size: %v.", size, l.writeChan)
	}
}

func Test_BufferSize_Valid(t *testing.T) {
	size := 150
	l, err := NewAmznLogger(NewTestWriter(), defaultAppName, BufferSize(size))
	if err != nil {
		t.Fatalf("Unexpected error returned. Error: %v.", err)
	}
	if cap(l.writeChan) != size {
		t.Fatalf("Expected writeChan size: %v. Actual writeChan size: %v.", size, l.writeChan)
	}
}

func Test_PackagePrefix_Empty(t *testing.T) {
	prefix := ""
	_, err := NewAmznLogger(NewTestWriter(), defaultAppName, LoggerPackagePrefix(prefix))
	if err == nil {
		t.Fatalf("Expected an error, however none was returned!")
	}
}

func Test_PackagePrefix_Valid(t *testing.T) {
	prefix := "testPrefix"
	l, err := NewAmznLogger(NewTestWriter(), defaultAppName, LoggerPackagePrefix(prefix))
	if err != nil {
		t.Fatalf("Unexpected error returned. Error: %v.", err)
	}
	if l.prefix != prefix {
		t.Fatalf("Expected prefix: %v. Acutal prefix: %v.", prefix, l.prefix)
	}
}

func Test_WriteChannelProcessor_Flush(t *testing.T) {
	w := NewTestWriter()
	l, err := NewAmznLogger(w, defaultAppName)
	if err != nil {
		t.Fatalf("Unexpected error returned. Error: %v.", err)
	}
	l.stopChan <- true
	timeout := time.After(defaultTimeout)
	<-timeout
	if w.flushed != true {
		t.Fatalf("Writer was not flushed properly")
	}
}

func Test_WriteChannelProcessor_WriterWrite(t *testing.T) {
	w := NewTestWriter()
	l, err := NewAmznLogger(w, defaultAppName)
	if err != nil {
		t.Fatalf("Unexpected error returned. error: %v.", err)
	}
	timeout := time.After(defaultTimeout)
	l.Fatal(testString1)
	select {
	case <-w.r:
		result := w.String()
		if !strings.Contains(result, testString1) {
			t.Fatalf("Unexpected log output. expected log to contain: %v. acutal log: %v.", testString1, result)
		}
	case <-timeout:
		t.Fatalf("Logger timed out!")
	}
}

func Test_WriteChannelProcessor_Close(t *testing.T) {
	w := NewTestWriter()
	l, err := NewAmznLogger(w, defaultAppName)
	if err != nil {
		t.Fatalf("Unexpected error returned. error: %v.", err)
	}
	// Close multiple times to ensure that no error is caused by doing so.
	l.Close()
	l.Close()
	// Pause. Wait for logger to exit. Should take much less than 10 ms.
	time.Sleep(10 * time.Millisecond)
	if l.Running() {
		t.Fatal("Logger is still running even after being stopped.")
	}
}

func Test_LogLevels_Less(t *testing.T) {
	level := level.Error
	w := NewTestWriter()
	l, err := NewAmznLogger(w, defaultAppName, LogLevel(level))
	if err != nil {
		t.Fatalf("Unexpected error returned. error: %v.", err)
	}
	timeout := time.After(defaultTimeout)
	l.Trace(testString1)
	select {
	case <-w.r:
		result := w.String()
		t.Fatalf("Unexpected log output. Expected log to contain: %v. Actual log: %v.", "", result)
	case <-timeout:
	}
}

func Test_LogLevels_Equal(t *testing.T) {
	level := level.Error
	w := NewTestWriter()
	l, err := NewAmznLogger(w, defaultAppName, LogLevel(level))
	if err != nil {
		t.Fatalf("Unexpected error returned. error: %v.", err)
	}
	timeout := time.After(defaultTimeout)
	l.Error(testString1)
	select {
	case <-w.r:
		result := w.String()
		if !strings.Contains(result, testString1) {
			t.Fatalf("Unexpected log output. expected log to contain: %v. acutal log: %v.", testString1, result)
		}
	case <-timeout:
		t.Fatalf("Logger timed out!")
	}
}

func Test_LogLevels_Greater(t *testing.T) {
	level := level.Error
	w := NewTestWriter()
	l, err := NewAmznLogger(w, defaultAppName, LogLevel(level))
	if err != nil {
		t.Fatalf("Unexpected error returned. error: %v.", err)
	}
	timeout := time.After(defaultTimeout)
	l.Fatal(testString1)
	select {
	case <-w.r:
		result := w.String()
		if !strings.Contains(result, testString1) {
			t.Fatalf("Unexpected log output. expected log to contain: %v. acutal log: %v.", testString1, result)
		}
	case <-timeout:
		t.Fatalf("Logger timed out!")
	}
}
