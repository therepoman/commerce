#!/bin/bash

set -x


PUBLISH_PATH=$(pwd)/docs/public
BIN=$(pwd)/_bin
HUGO=$(command -v hugo)

# check if hugo is installed - install locally if not
if [[ -z "$HUGO" ]]; then
    echo "downloading hugo"
    mkdir -p ${BIN}
	wget -O ${BIN}/hugo.tar.gz https://github.com/gohugoio/hugo/releases/download/v0.69.2/hugo_0.69.2_Linux-64bit.tar.gz
	tar -xzf ${BIN}/hugo.tar.gz -C ${BIN}
	HUGO=${BIN}/hugo
fi

echo "using: $HUGO"
echo "publish path: $PUBLISH_PATH"

rm -rf ${PUBLISH_PATH}
git worktree prune
git worktree add -B gh-pages ${PUBLISH_PATH} origin/gh-pages

(cd docs && ${HUGO} -d ${PUBLISH_PATH})


pushd ${PUBLISH_PATH}

pwd

# publish only if something has changed.
echo "publishing..."
git add -A && \
git -c user.email=root@jenkins-og.xarth.tv -c user.name='Jenkins' \
commit -m "Publishing to gh-pages" && \
git push git@git.xarth.tv:commerce/load-testing.git gh-pages

popd

echo "done"
exit 0
