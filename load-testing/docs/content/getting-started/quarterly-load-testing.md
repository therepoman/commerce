---
title: "Quarterly Load Testing"
date: 2020-04-20T16:16:06-07:00
weight: 3
---


1. Each tribe should identify their **top 2-3 critical services**.
    - _Rule of thumb:_ If the service being down would qualify as a Sev 1 or Sev 2, it's probably critical.
2. **Implement repeatable load tests** for the 3-5 most critical APIs in each of those services.
    - Repeatable in this case means: it's checked into this repository, and is easy to run on demand.
      Especially if the test requires multiple hosts to generate the desired load, there should be clearly documented
      steps or automation built around running the test.
    - Load tests between tribes should be consistent:
        - Same framework
        - Same process for documenting
        - Same process for notifying downstreams
3. Run the [load tests](../load-test-requirements) and **document the findings in this portal**.
4. **Repeat** the above process **once per quarter**.
    - Try to include one new service each quarter, until all services owned by the tribe have repeatable load tests.
    - This will be a manual process at first - as this process matures, Commerce could invest further and automate this process.


