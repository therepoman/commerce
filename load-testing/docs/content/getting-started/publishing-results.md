---
title: "Publishing results"
date: 2021-04-14T16:16:06-07:00
weight: 7
---

To publish results to this portal:

1. [Onboard your service](../onboarding)
2. [Run your load tests](../piledriver)
3. Write a summary of the results under `docs/content/services/<your service>`
4. Create a Pull Request in the [load-testing GHE repo](https://git.xarth.tv/commerce/load-testing)
5. Merge your changes
6. Verify the `master` build has succeeded in the [Jenkins portal](https://jenkins-og.xarth.tv/job/commerce/job/load-testing/job/master/)
7. From the root of this repo, run `make publish` to update this site with your merged changes.  


