---
title: "Piledriver"
date: 2020-04-27T22:17:20-07:00
weight: 6
---

Piledriver is the tool we use to load test our service. This repository contains a fork of the original [piledriver][og],
modified to work in a multi-service environment. Piledriver itself is built on top of [Nebbiolo][n], a plan-based load test driver.


[og]: https://git.xarth.tv/commerce/piledriver
[n]: https://git.xarth.tv/commerce/nebbiolo


# PILEDRIVER
Ad Hoc Distributed Load Testing

![Zangeif Piledrives!](../../images/zangief.gif?raw=true "Zangief Piledrives!")

## Why you would want to use this
* You want to make sure your service is operationally ready.
* The service you want to test is network isolated and you need a way to call it.
* You want to generate so much load that your laptop shouldn't be considered as an option to run it.

## Fetching Piledrive is Easy
I install this via `go get` like so:
```bash
go get code.justin.tv/commerce/piledriver
```

## Things you'll need to know before we start PILEDRIVING
This repository uses [Nebbiolo](https://git.xarth.tv/commerce/nebbiolo), which is a plan based load testing framework made by and for the commerce organization. You'll want to read up on how to use their TOML file plans to run load tests with Piledriver.

You will also need to have setup your SSH key on your box. This will allow your local machine to SSH into the instances created by Piledriver to start, stop, set file descriptors, and get results.

The instructions in this section assume that you've already onboarded your service for load-testing. If you haven't please refer to the onboarding section on the right to get your service ready for load tests using this framework before continuing here.

## Time to PILEDRIVE your Service!
First, confirm that there is a directory in the `load-tests` folder that matches the service you're load testing. For example, if you want to load test `commerce/payday`, confirm that there is a directory `commerce/payday` and cd into it. If there isn't an existing directory for your service, you'll need to go through the steps in the onboarding section.

```bash
cd load-tests/commerce/payday # this is an example, cd into the directory you want to run load tests for
```

Now, begin by exporting these environment variables specific to your service:
```bash
export AWS_PROFILE=<your services aws profile>
export TC=<your services teleport bastion name>
export USERNAME=$(whoami)
```

You will need to run some terraform in the account you wish to load test. This will setup a launch template and an ASG attached to your VPC.
To support this multi-service, multi-user environment, you need to run `make init` to generate a user-specific `.tfvars` file.

```bash
make init NUM_HOSTS=<integer> USERNAME=<optional>
```
This will generate a user-specific .tfvars file based on the service's .tfvars template.

Plan and apply the terraform changes with:
```bash
make plan
make apply
```

You will need to wait for terraform to finish creating your resources. This should not take but only a few minutes.

Once complete, check that you can access the hosts:
```bash
make list_hosts
```

If you plan on running load tests with 10's of thousands of RPS, we recommend running the following command to increase the number of availabile file descriptors on your machine
```bash
make set_file_descriptors
```
 Now you will want to make your load test plan. Create the load test plan file your service's directory (see [recommended layout](../onboarding#recommended-layout)). See the [Nebbiolo](https://git.xarth.tv/commerce/nebbiolo) repository for example usages. Your traffic plan will be copied to every host, so if you desire a total of `X` TPS, the value configured in your `<plan-name>.toml` should be `X/Num Hosts`

First, prepare you load test hosts for testing. This step needs to be run any time you change your test files. You should see that it is SCPing some files to all the hosts in your ASG.
```bash
make prep
```

You are ready to start load testing now! To start, simply run this command:
```bash
make start LOADTEST_FILES="<test-name>.toml <data-file>.toml <data-file>.toml"
```
You should see that it is starting the command to run the load test on each instance.

{{% notice note %}}
The plan files need to be specified via environment variable passed on the command line. The files need to be in a quoted,
space-separated list, and must not include  path information. All of your service's test files and the files in `datasets/`
are copied over and available as inputs to your load test.
{{% /notice %}}

If you update your plan/data files after running `make prep`, you can copy over only the diffs by the sync command:
```bash
make sync
```

If you need to stop the load test from running, simply run:
```bash
make stop
```

When your loadtest is complete, run this to download the result files:
```bash
make get_results OUTPUT_DIR=results/<test-name>/<YYYY-MM>
```

To aggregate the results and generate a markdown file for the doc portal, run this:
```bash
make publish_results RESULTS_DIR=results/<test-name>/<YYYY-MM>
```

## You're done PILEDRIVING, now CLEAN UP

To remove all your resources you made while running Piledrive, simply run the following commands:

```bash
make destroy
```

It should ask you to confirm that you want to destroy the resources you created, which you will just type `yes` and watch the resources disappear.
