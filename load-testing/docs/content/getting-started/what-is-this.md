---
title: "What Is This"
date: 2020-04-20T16:22:13-07:00
weight: 2
---


Twitch has seen a massive influx of traffic recently, driven in part by Stay-at-Home orders issued to fight the spread
of COVID-19, as well as launches of new popular games (VALORANT). Memberships-owned services have seen [between 20% to
100% more traffic](traffic) the week of April 6th, vs the week of March 23rd.
This has caused us to look closely at our services and infrastructure, and ask ourselves: _**“how far can we scale? How much headroom do we have?”**_
At this time, the answer to both of these questions is unfortunately “We don’t really know”, which is not an acceptable long-term answer.

[traffic]: https://docs.google.com/document/d/1eBKnfY8xOFj7keWGZRe5ObunA26cDN7Cou71FtnhirU/edit?usp=sharing


### Why can’t we answer these questions?
There are two major factors that impede our ability to answer the questions about how much room we have left to scale. Those are:

1. **Load testing is currently only performed on an ad-hoc basis.**
    - In practice, load tests are performed only just before the service launches, or occasionally when a new feature
      is launched on a service that dramatically changes the traffic patterns it sees.
    - While helpful, the _results of these load tests lose validity over time_, due to various small changes in code and
      infrastructure related to releasing small features, bug fixes, and maintenance.
2. **The results of load-tests are either not documented, or not organized in a way that is easy to find later.**
    - Often the results of these load tests are buried in Google docs that only one or two engineers know about,
      making it difficult for other engineers to know the history of load tests against a given service.


### Solution: Regular Load Tests with Discoverable Results

We can solve both of these problems by 1) encouraging tribes to load test their services on a regular basis (see [Quarterly Load Testing](../quarterly-load-testing)), and 2)
providing a centralized mechanism for reporting the results of the load tests, so that anyone in Commerce can easily
find the latest results (this repository/docs portal).

