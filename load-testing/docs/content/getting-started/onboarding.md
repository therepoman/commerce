---
title: "Onboarding"
date: 2020-04-17T21:08:36-07:00
weight: 5
---

{{% notice note %}}
Please install [Hugo](#hugo) before onboarding a new service: `brew install hugo`
{{% /notice %}}

Adding a new service to this portal is easy! Services in this portal are organized by their GHE folder path.
Markdown files live in `docs/content/services/` and everything else lives in `load-tests/`. The onboarding script
will create the correct structure for you, and generate a Makefile and terraform vars file as a starting point.

For example, if I wanted to onboard `commerce/payday` service, I'd run the following:

```bash
$ cd $GOPATH/src/code.justin.tv/commerce/load-testing
$ ./scripts/onboard.sh commerce/payday
```


Edit the newly created files. In the new `load-tests/` subdirectory:
- In ***.tfvars**: update the AWS Profile, VPC ID, Subnets, and Security Group to be specific to your service. Leave `_USERNAME_` and `_NUM_HOSTS_` as they are expected later by Piledriver.
- In **Makefile**: check that `TF_VARS` and `REPO` were set correctly.  Look at `load-tests/revenue/subscriptions/Makefile` for a working example.

In the `docs/content/services/` subdirectory:
- In **_index.md**: update the section at the top to include any relevant [tags](https://learn.netlify.app/en/cont/tags/),
    as well as relevant links to your service (dashboard, runbook, etc).

Add your test .toml files to the new `load-tests/` subdirectory, and use the Makefile to run the tests (see [Piledriver](../piledriver) for details).

### Recommended Layout

Ultimately we'd like the folder structure to look like this for each service:

```
|-- docs/
|  |-- content/
|     |-- services/
|        |-- commerce/
|           |-- payday/
|              |-- _index.md
|              |-- results-test-name-2020-04.md
|              |-- results-test-name-2020-08.md
|-- load-tests/
|  |-- commerce/
|     |-- payday/
|        |-- Makefile
|        |-- test-name.toml
|        |-- prod.tfvars
|        |-- results/
|           |-- test-name/
|              |-- 2020-04/
|                 |-- 10.0.0.1.txt
|                 |-- 10.0.0.2.txt
|              |-- 2020-08/
|                 |-- 10.0.0.1.txt
|                 |-- 10.0.0.2.txt
```

Raw results of previous load tests should be checked into folders under the `results/` sub-directory for each service.
Piledriver will automatically download these for you, you just need to specify the `OUTPUT_DIR`.

To make the results available via the web, use Piledriver's `publish_results` feature to automatically aggregate the results
and generate a markdown results file. Make sure to check these files into this repository as well.


### Hugo
This site is generated using [Hugo](https://github.com/gohugoio/hugo). To run a local copy of the site,
install hugo with `brew install hugo`, or download it from here: https://gohugo.io/getting-started/installing/

With hugo installed, adding a page for a new service is simple. Hugo is required to run the onboarding script, as it does this part for you. From within the `docs/` directory, run:

```bash
hugo new services/<org>/<repo>/_index.md
```

To see your changes on your local machine, run the following command from the `/docs` folder:

```bash
hugo serve
```

Any changes to the files under the `docs/` folder will trigger Jenkins to rebuild the [live version of this site](https://git.xarth.tv/pages/commerce/load-testing/)
when it is merged to master.
