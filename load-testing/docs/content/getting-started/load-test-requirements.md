---
title: "Load Test Requirements"
date: 2020-04-20T16:14:46-07:00
weight: 4
---



{{% notice note %}}
_Every service is different. This section provides a baseline - start from here and modify to suit your service's use case._
{{% /notice %}}



1. Identify the type of load test to perform. Eventually all services should implement both Ramp and a series of Burst tests (see the table below).
2. Inform stakeholders of load test (when, expected TPS, who to contact if things go south - use CM format, notify #site-production, etc)
    - Do this both well before (1wk) and immediately before the load test is to begin
    - Run load test according to the target plan
3. Closely monitor dashboards during load tests, including rollbar and other logs/error tracking
4. Document findings on service-specific page of this documentation portal
    - Include screenshots of metrics graphs as appropriate




### Types of tests
Definitions used in the table below:

- `Prod` traffic: average 1 week smoothed top of the daily curve
- `Peak` traffic: maximum 5 minute average TPS seen in that last 3 months
- `Spike` traffic: `Peak` - `Prod`


| Type      | Target Load & Traffic Pattern |
| --------- | ----------------------------- |
| Ramp      | `Prod` -> 2x `Prod` linear ramp over 60m, then hold for 30m |
| Burst1    | `Prod` -> `Prod` + .75 * `Spike`  in 5 minutes (linear ramp), then a 10 minute hold period at `Prod` + .75 * `Spike`. If good, go to Burst2 after cooling down 15m |
| Burst2    | `Prod` -> `Prod` + `Spike` in 5 minutes (linear ramp), then a 10 minute hold period at `Prod` + `Spike`. If good, go to Burst3 after cooling down 15m |
| Burst3    | `Prod` -> `Prod` + 1.5 * `Spike` in 5 minutes (linear ramp), then a 10 minute hold period at `Prod` + 1.5 * `Spike` |

For example, for a service with `Prod` traffic of 100k TPS, and `Peak` traffic of 170k TPS, this would be the load test plan:

| Type      | Target Load & Traffic Pattern |
| --------- | ----------------------------- |
| Ramp      | 100k -> 200k TPS over 1 hour, then 30 minute hold at 200k |
| Burst1    | 100k -> 152k TPS over 5 minutes, then 10 minute hold at 152k |
| Burst2    | 100k -> 170k TPS over 5 minutes, then 10 minute hold at 170k |
| Burst3    | 100k -> 205k TPS over 5 minutes, then 10 minute hold at 205k |

### Notifying others

When you run your load test, you need to notify any downstream dependencies whose services may see increased traffic.
It's good practice to notify at least 2 business days before your test, as well as just before you start. Ping the
on-call person for the downstream dependency to let them know.

At the very least, please post in [#commerce-oncall](https://twitch.slack.com/archives/C3SH458KB) when you run your load test. Include the following information:
- Service you are testing
- When you plan to start and how long the test will run
- The amount of load you plan to send to the service


Depending upon the service you are testing and the load you're planning to send, you might consider posting in [#site-production](https://twitch.slack.com/archives/C03SW9JBR) as well.

