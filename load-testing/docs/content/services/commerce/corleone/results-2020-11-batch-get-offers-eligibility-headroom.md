---
title: "Results 2020 11 Batch Get Offers Eligibility Headroom"
date: 2020-11-20T13:21:45-08:00
hidden: true
---

### Notes

Peak TPS when blended with prod traffic: *8950*

Almost all of the 5xx errors were due to channels in the `channel_ids_with_tickets.toml` test data not actually having associated tickets.
The erred channels were removed from the test data, and we created a JIRA to return a 4xx in this case rather than a 5xx.

See attached csv for errors we encountered: https://twitch.slack.com/archives/CQHUWDJ9M/p1605906555113300?thread_ts=1605831402.111000&cid=CQHUWDJ9M

### Host 10.0.32.137

```
Requests      [total, rate]            8022159, 2078.87
Duration      [total, attack, wait]    1h4m20.502297614s, 1h4m18.898511162s, 1.603786452s
Latencies     [mean, 50, 95, 99, max]  71.645505ms, 55.201647ms, 186.361758ms, 291.714884ms, 3.577605665s
Bytes In      [total, mean]            102534100341, 12781.36
Bytes Out     [total, mean]            9945299421, 1239.73
Success       [ratio]                  99.28%
Status Codes  [code:count]             200:7964106  400:31  500:58022  
Error Set:
500 Internal Server Error
400 Bad Request
```

