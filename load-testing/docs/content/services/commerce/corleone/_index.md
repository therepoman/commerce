---
title: "Corleone"
tags: ["memberships", "payments", "money"]
---


### Links

- [Code](https://code.amazon.com/packages/Corleone/trees/mainline)
- [Grafana Dashboard](https://grafana.xarth.tv/d/1lVBTXAiz/graphql-dependency-details?viewPanel=477&orgId=1&from=now-12h&to=now-1m&var-env=production&var-region=us-west-2&var-service=Corleone&var-method=All&var-all_services=All)

###  Running 

These tests are orchestrated via the `Makefile` using [piledriver][pl].

[pl]: ../../../getting-started/piledriver

{{% notice tip %}}
You may want to export the following environment variables - otherwise you'll need to include these on all of the Piledriver
makefile commands.
{{% /notice %}}

```bash
export AWS_PROFILE=twitch-corleone-prod
export USERNAME=$(whoami)
export TC=teleport-remote-corleone-prod-us-west-2
```

Next, follow the [piledriver][pl] instructions, or use this as the TL;DR:

```bash
$ cd load-tests/services/commerce/corleone
$ make init NUM_HOSTS=1
$ make plan
$ make apply
$ make list_hosts
$ make set_file_descriptors
$ make prep                     # from here and above only need to be done once
$ make start LOADTEST_FILES="<test-name>.toml <data-file>.toml <data-file>.toml"
$ make check_status
$ make stop                     # optional - if needed
$ make get_results OUTPUT_DIR=results/<YYYY-MM>/<test-name>
$ make publish_results RESULTS_DIR=results/<YYYY-MM>/<test-name>
```

### Note
It is important to be mindful of the kind of load a plan will generate on Corleone and how Corleone would behave. Previously, load tests would use the same static tags and so always go through the shortest path through Corleone, without exercising the caching layers in depth. Realistically, requests vary in static tags (related: [INC-1483](https://docs.google.com/document/d/1QMkYUB5nMTcA0CqqztDNKCvmSzeOxvBolaG4xfcoRJo)) and so test plans have been updated to choose from the known set of [tiers](https://git.xarth.tv/commerce/load-testing/blob/master/load-tests/commerce/corleone/BatchGetOffersEligibility_ramp.toml#L22) and [intervals](https://git.xarth.tv/commerce/load-testing/blob/master/load-tests/commerce/corleone/BatchGetOffersEligibility_ramp.toml#L23).

This, of course, calls for cache metrics.

### Results

{{% children showhidden="true" %}}
