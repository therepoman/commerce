---
title: "Results 2020 08 Batched Baseline"
date: 2020-08-31T18:18:49-07:00
hidden: true
---

```
make start LOADTEST_FILES="BatchGetOffersEligibility_ramp.toml GetOffers_ramp.toml ticket_product_ids_10k.toml user_ids_10k.toml channel_ids_with_tickets.toml tiers.toml intervals.toml subtember_staff_beta_user_ids.toml"
```
!["Corleone Dashboard"](../../../../images/commerce/corleone/08-28-2020/corleone-dashboard-batched.png)

### Host 10.0.182.134

```
Requests      [total, rate]            14300550, 1960.63
Duration      [total, attack, wait]    2h1m34.16446389s, 2h1m33.855153548s, 309.310342ms
Latencies     [mean, 50, 95, 99, max]  29.388547ms, 10.546766ms, 72.540565ms, 222.051522ms, 55.554003929s
Bytes In      [total, mean]            143562692939, 10038.96
Bytes Out     [total, mean]            6541480910, 457.43
Success       [ratio]                  99.93%
Status Codes  [code:count]             0:157  200:14290414  500:9979  
Error Set:
500 Internal Server Error
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffers": net/http: timeout awaiting response headers
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/BatchGetOffersEligibility": net/http: timeout awaiting response headers
```

### Host 10.0.63.113

```
Requests      [total, rate]            14300550, 1960.77
Duration      [total, attack, wait]    2h1m33.544339865s, 2h1m33.341008641s, 203.331224ms
Latencies     [mean, 50, 95, 99, max]  29.634019ms, 10.711441ms, 72.800939ms, 222.580134ms, 56.053804909s
Bytes In      [total, mean]            143560959976, 10038.84
Bytes Out     [total, mean]            6541475964, 457.43
Success       [ratio]                  99.93%
Status Codes  [code:count]             0:144  200:14289997  500:10409  
Error Set:
500 Internal Server Error
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffers": net/http: timeout awaiting response headers
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/BatchGetOffersEligibility": net/http: timeout awaiting response headers
```

