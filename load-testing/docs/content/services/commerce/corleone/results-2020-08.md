---
title: "Results 2020 08 (pre-batching)"
date: 2020-08-31T18:18:49-07:00
hidden: true
---

# GetOffersEligibility
```
make start LOADTEST_FILES="GetOffersEligibility_ramp.toml GetOffers_ramp.toml ticket_product_ids_10k.toml user_ids_10k.toml channel_ids_with_tickets.toml tiers.toml intervals.toml subtember_staff_beta_user_ids.toml"
```
!["Corleone Dashboard"](../../../../images/commerce/corleone/08-28-2020/corleone-dashboard.png)
