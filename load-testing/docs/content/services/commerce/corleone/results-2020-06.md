---
title: "Results 2020 06"
date: 2020-06-24T15:04:58-07:00
hidden: true
---


### Host 10.0.171.245

```
Requests      [total, rate]            55048446, 7425.97
Duration      [total, attack, wait]    2h3m33.124629661s, 2h3m32.961596678s, 163.032983ms
Latencies     [mean, 50, 95, 99, max]  107.421337ms, 30.724813ms, 536.596849ms, 714.080343ms, 4.354100796s
Bytes In      [total, mean]            224181237926, 4072.44
Bytes Out     [total, mean]            9883415201, 179.54
Success       [ratio]                  73.27%
Status Codes  [code:count]             0:14715938  200:40332060  500:448  
Error Set:
500 Internal Server Error
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffersEligibility": dial tcp 0.0.0.0:0->10.0.175.53:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffers": dial tcp 0.0.0.0:0->10.0.175.53:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffersEligibility": dial tcp: lookup us-west-2.prod.corleone.s.twitch.a2z.com on 10.0.0.2:53: dial udp 10.0.0.2:53: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffers": dial tcp: lookup us-west-2.prod.corleone.s.twitch.a2z.com on 10.0.0.2:53: dial udp 10.0.0.2:53: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffersEligibility": dial tcp: lookup us-west-2.prod.corleone.s.twitch.a2z.com on 10.0.0.2:53: no such host
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffers": dial tcp: lookup us-west-2.prod.corleone.s.twitch.a2z.com on 10.0.0.2:53: no such host
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffersEligibility": dial tcp 0.0.0.0:0->10.0.34.208:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffers": dial tcp 0.0.0.0:0->10.0.34.208:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffersEligibility": dial tcp 0.0.0.0:0->10.0.121.97:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffers": dial tcp 0.0.0.0:0->10.0.121.97:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffers": dial tcp 0.0.0.0:0->10.0.47.17:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffersEligibility": dial tcp 0.0.0.0:0->10.0.47.17:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffersEligibility": dial tcp 0.0.0.0:0->10.0.182.117:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffers": dial tcp 0.0.0.0:0->10.0.182.117:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffersEligibility": dial tcp 0.0.0.0:0->10.0.126.47:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffers": dial tcp 0.0.0.0:0->10.0.126.47:443: socket: too many open files
```

### Host 10.0.43.190

```
Requests      [total, rate]            55048446, 7418.24
Duration      [total, attack, wait]    2h3m40.756062129s, 2h3m40.68730078s, 68.761349ms
Latencies     [mean, 50, 95, 99, max]  107.18005ms, 30.410767ms, 535.728571ms, 713.126543ms, 4.411213871s
Bytes In      [total, mean]            225005603231, 4087.41
Bytes Out     [total, mean]            9923589733, 180.27
Success       [ratio]                  73.56%
Status Codes  [code:count]             0:14551638  200:40496340  500:468  
Error Set:
500 Internal Server Error
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffersEligibility": dial tcp 0.0.0.0:0->10.0.121.97:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffersEligibility": dial tcp: lookup us-west-2.prod.corleone.s.twitch.a2z.com on 10.0.0.2:53: dial udp 10.0.0.2:53: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffers": dial tcp: lookup us-west-2.prod.corleone.s.twitch.a2z.com on 10.0.0.2:53: dial udp 10.0.0.2:53: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffersEligibility": dial tcp 0.0.0.0:0->10.0.47.17:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffers": dial tcp 0.0.0.0:0->10.0.47.17:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffers": dial tcp 0.0.0.0:0->10.0.34.208:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffersEligibility": dial tcp 0.0.0.0:0->10.0.34.208:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffersEligibility": dial tcp: lookup us-west-2.prod.corleone.s.twitch.a2z.com on 10.0.0.2:53: no such host
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffers": dial tcp: lookup us-west-2.prod.corleone.s.twitch.a2z.com on 10.0.0.2:53: no such host
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffers": dial tcp 0.0.0.0:0->10.0.182.117:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffersEligibility": dial tcp 0.0.0.0:0->10.0.182.117:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffersEligibility": dial tcp 0.0.0.0:0->10.0.126.47:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffers": dial tcp 0.0.0.0:0->10.0.126.47:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffers": dial tcp 0.0.0.0:0->10.0.175.53:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffersEligibility": dial tcp 0.0.0.0:0->10.0.175.53:443: socket: too many open files
Post "https://us-west-2.prod.corleone.s.twitch.a2z.com/twirp/twitch.payments.corleone.Corleone/GetOffers": dial tcp 0.0.0.0:0->10.0.121.97:443: socket: too many open files
```

