---
title: "Results: GetViewablePoll 2020-04"
date: 2020-04-30T17:57:32-07:00
hidden: true
---


### Host 10.206.158.20

```
Requests      [total, rate]            6879240, 2547.31
Duration      [total, attack, wait]    45m0.59568831s, 45m0.589082213s, 6.606097ms
Latencies     [mean, 50, 95, 99, max]  4.20218ms, 3.105337ms, 7.881737ms, 9.681127ms, 354.125289ms
Bytes In      [total, mean]            14154860, 2.06
Bytes Out     [total, mean]            364143213, 52.93
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:6879240  
Error Set:
```

