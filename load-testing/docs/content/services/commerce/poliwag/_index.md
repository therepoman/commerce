---
title: "Poliwag"
tags: ["moments", "polls"]
---


### Links

- [Code](https://git.xarth.tv/commerce/poliwag)
- [Runbook](https://git.xarth.tv/commerce/bits-oncall/tree/master/poliwag)
- [Dashboard](https://isengard.amazon.com/federate?account=246232734983&role=admin&destination=cloudwatch%2Fhome%3Fregion%3Dus-west-2%23dashboards%3Aname%3DPoliwag%3Bstart%3DP7D)

### Running
```
make start LOADTEST_FILES="get-viewable-poll.toml channel_ids_5k.toml user_ids_50k.toml"
```

### Results

{{% children showhidden="true" %}}
