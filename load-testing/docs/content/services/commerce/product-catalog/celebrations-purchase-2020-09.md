---
title: "Results, Celebrations Purchase + GetProducts ramp, 2020 09"
date: 2020-09-30T16:48:00:00-07:00
hidden: true
---
First full run:
```
Requests      [total, rate]            28385673, 11673.94
Duration      [total, attack, wait]    40m31.542928915s, 40m31.541015599s, 1.913316ms
Latencies     [mean, 50, 95, 99, max]  13.356581ms, 2.916247ms, 41.045386ms, 134.654428ms, 10.057203488s
Bytes In      [total, mean]            61014797505, 2149.49
Bytes Out     [total, mean]            5826834663, 205.27
Success       [ratio]                  97.41%
Status Codes  [code:count]             0:710613  200:27651383  500:23677
Error Set:
500 Internal Server Error
```
Second full run:
```
Requests      [total, rate]            34385673, 14285.03
Duration      [total, attack, wait]    40m7.114352946s, 40m7.111608319s, 2.744627ms
Latencies     [mean, 50, 95, 99, max]  15.596542ms, 3.551307ms, 58.172341ms, 94.762692ms, 1.041982669s
Bytes In      [total, mean]            68738887981, 1999.06
Bytes Out     [total, mean]            6663650740, 193.79
Success       [ratio]                  90.65%
Status Codes  [code:count]             0:3188477  200:31170251  500:26945
Error Set:
500 Internal Server Error
```
