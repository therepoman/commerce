---
title: "Results GetEmoteSetDetails_burst3 2021 06"
date: 2021-06-22T15:16:21-07:00
hidden: true
---


### Host 10.201.96.141

```
Requests      [total, rate]            4116903, 4543.61
Duration      [total, attack, wait]    15m6.103989733s, 15m6.086248962s, 17.740771ms
Latencies     [mean, 50, 95, 99, max]  10.864781ms, 9.385068ms, 18.189734ms, 33.174012ms, 3.033974679s
Bytes In      [total, mean]            151311021348, 36753.60
Bytes Out     [total, mean]            250087465, 60.75
Success       [ratio]                  100.00%
Status Codes  [code:count]             0:1  200:4116901  500:1  
Error Set:
500 Internal Server Error
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": EOF
```

### Host 10.201.97.85

```
Requests      [total, rate]            4116903, 4525.06
Duration      [total, attack, wait]    15m9.82240062s, 15m9.80062291s, 21.77771ms
Latencies     [mean, 50, 95, 99, max]  18.491309ms, 9.078145ms, 17.636822ms, 37.572192ms, 7.613954651s
Bytes In      [total, mean]            151241790881, 36736.79
Bytes Out     [total, mean]            250089816, 60.75
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:4116899  500:4  
Error Set:
500 Internal Server Error
```

### Host 10.201.98.122

```
Requests      [total, rate]            4116903, 4533.68
Duration      [total, attack, wait]    15m8.086014546s, 15m8.070117604s, 15.896942ms
Latencies     [mean, 50, 95, 99, max]  14.158064ms, 9.334057ms, 17.752316ms, 36.84746ms, 3.370869961s
Bytes In      [total, mean]            151328089139, 36757.75
Bytes Out     [total, mean]            250088163, 60.75
Success       [ratio]                  100.00%
Status Codes  [code:count]             0:2  200:4116897  500:4  
Error Set:
500 Internal Server Error
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": EOF
```

