---
title: "Results GetEmoteSetDetails_burst1 2021 06"
date: 2021-06-22T15:16:15-07:00
hidden: true
---


### Host 10.201.96.141

```
Requests      [total, rate]            2058528, 2281.18
Duration      [total, attack, wait]    15m2.410015556s, 15m2.397293142s, 12.722414ms
Latencies     [mean, 50, 95, 99, max]  9.857637ms, 9.159074ms, 15.578785ms, 24.5832ms, 452.019709ms
Bytes In      [total, mean]            75649954922, 36749.54
Bytes Out     [total, mean]            125049850, 60.75
Success       [ratio]                  100.00%
Status Codes  [code:count]             0:1  200:2058525  500:2  
Error Set:
500 Internal Server Error
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": EOF
```

### Host 10.201.97.85

```
Requests      [total, rate]            2058528, 2280.81
Duration      [total, attack, wait]    15m2.560146304s, 15m2.541921979s, 18.224325ms
Latencies     [mean, 50, 95, 99, max]  9.589112ms, 8.901057ms, 15.351138ms, 24.335001ms, 452.040253ms
Bytes In      [total, mean]            75620076071, 36735.02
Bytes Out     [total, mean]            125048273, 60.75
Success       [ratio]                  100.00%
Status Codes  [code:count]             0:1  200:2058521  500:6  
Error Set:
500 Internal Server Error
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": EOF
```

### Host 10.201.98.122

```
Requests      [total, rate]            2058528, 2280.42
Duration      [total, attack, wait]    15m2.721246535s, 15m2.698228555s, 23.01798ms
Latencies     [mean, 50, 95, 99, max]  9.780892ms, 9.116763ms, 15.503993ms, 24.370462ms, 451.425362ms
Bytes In      [total, mean]            75660014101, 36754.43
Bytes Out     [total, mean]            125051109, 60.75
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2058526  500:2  
Error Set:
500 Internal Server Error
```

