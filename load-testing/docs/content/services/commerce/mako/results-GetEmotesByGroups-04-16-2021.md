---
title: "Results GetEmotesByGroups ramp and burst 04-16-2021"
date: 2021-04-16T13:03:44-07:00
hidden: true
---

### Purpose 

The purpose of the load test was to ensure we are properly scaled to handle launch of AnimatedEmotes. 

### Test parameters
These load tests were for `Mako.GetEmotesByGroups` in production. 

The list of groupIds were pulled from Mode with a query like:
``` 
SELECT groupid FROM emotesdb.prod_emotes_v4 WHERE emotesgroup = 'Subscriptions' LIMIT 8000;
```

#### Ramp load test

```
make start LOADTEST_FILES="GetEmotesByGroups_ramp.toml emote_group_ids_BTER_2k.toml emote_group_ids_Subs_8k.toml"
```

This load test was run from 0 -> 30 TPS over 60 mins, then at a consistent 30TPS for 30 mins. 
30 TPS is ~4x the traffic we see from current levels of GetEmotesByGroups. 

#### Burst load test

```
make start LOADTEST_FILES="GetEmotesByGroups_burst.toml emote_group_ids_BTER_2k.toml emote_group_ids_Subs_8k.toml"
```

This load test was run from 0 -> 30 TPS over 5 mins, then at a consistent 30TPS for 10 mins. 
30 TPS is ~4x the traffic we see from current levels of GetEmotesByGroups. 

### Dependencies
The only downstream dependency for this API is DynamoDB for the `prod_emote_groups` table. This table has auto scaling enabled. 
Current traffic levels to this table are well below the minimum 100 RCU.  


## Results

### Success Rate

API success rate was 99.9% for both tests. There were only a few failed API requests due to context timeouts. 

Ramp:

!["GetEmotesByGroups_APIRequests"](../../../../images/commerce/mako/GetEmotesByGroupsRampAPIRequests-04-16-2021.png)

Burst:

!["GetEmotesByGroups_APIRequests"](../../../../images/commerce/mako/GetEmotesByGroupsBurstAPIRequests-04-16-2021.png)


### Latency

Overall latency did not increase throughout either test. 

Ramp:

!["GetEmotesByGroups_APIRequests"](../../../../images/commerce/mako/GetEmotesByGroupsRampLatency-04-16-2021.png)

Burst:

!["GetEmotesByGroups_APIRequests"](../../../../images/commerce/mako/GetEmotesByGroupsBurstLatency-04-16-2021.png)

### Dynamo Read Capacity

Dynamo capacity increased as the tests went on. During the burst load test auto scaling was a bit delayed. It took about 10 mins for it to auto scale. 
This could potentially cause throttled reads if there was an even larger amount of traffic, but and we did not experience any throttled reads during either test. 

Ramp:

!["GetEmotesByGroups_APIRequests"](../../../../images/commerce/mako/GetEmotesByGroupsRampDDBReadCapacity-04-16-2021.png)

Burst:

!["GetEmotesByGroups_APIRequests"](../../../../images/commerce/mako/GetEmotesByGroupsBurstDDBReadCapacity-04-16-2021.png)

### Conclusion
Our API is are well scaled to handle launch and increase traffic to 4x the normal load. 
If we see any issues with this API we can simply scale up the `prod_emote_groups` table RCU, though auto scaling will eventually kick in and scale for us.    

### Raw Results 

#### Ramp load test results from Host 10.201.96.119

```
Requests      [total, rate]            107712, 20.00
Duration      [total, attack, wait]    1h29m44.760041498s, 1h29m44.748637001s, 11.404497ms
Latencies     [mean, 50, 95, 99, max]  15.127436ms, 13.517754ms, 24.168351ms, 45.384278ms, 1.002624233s
Bytes In      [total, mean]            424096431, 3937.32
Bytes Out     [total, mean]            20116796, 186.76
Success       [ratio]                  99.99%
Status Codes  [code:count]             200:107706  500:6  
Error Set:
500 Internal Server Error
```

#### Burst load test results from  Host 10.201.96.119

```
Requests      [total, rate]            22476, 25.34
Duration      [total, attack, wait]    14m46.832650776s, 14m46.817578621s, 15.072155ms
Latencies     [mean, 50, 95, 99, max]  14.976234ms, 13.172621ms, 23.773026ms, 45.363593ms, 2.662389316s
Bytes In      [total, mean]            87795024, 3906.17
Bytes Out     [total, mean]            4197392, 186.75
Success       [ratio]                  99.98%
Status Codes  [code:count]             200:22471  500:5  
Error Set:
500 Internal Server Error
```


