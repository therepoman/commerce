---
title: "Results GetEmoteSetDetails_burst3 2020 06"
date: 2020-06-16T16:30:44-07:00
hidden: true
---


### Host 10.201.96.39

```
Requests      [total, rate]            7485003, 7678.49
Duration      [total, attack, wait]    16m14.94014371s, 16m14.801329327s, 138.814383ms
Latencies     [mean, 50, 95, 99, max]  79.720525ms, 5.673491ms, 27.166185ms, 217.191116ms, 33.687034087s
Bytes In      [total, mean]            75901926826, 10140.53
Bytes Out     [total, mean]            227697243, 30.42
Success       [ratio]                  100.00%
Status Codes  [code:count]             0:3  200:7484951  500:49
Error Set:
500 Internal Server Error
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": net/http: timeout awaiting response headers
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": dial tcp 0.0.0.0:0->10.201.97.192:443: bind: address already in use
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": dial tcp 0.0.0.0:0->10.201.97.192:443: i/o timeout
```

