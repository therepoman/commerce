---
title: "Results GetEmoteSetDetails_ramp 2020 06"
date: 2020-06-16T11:47:21-07:00
hidden: true
---


### Host 10.201.96.39

```
Requests      [total, rate]            50148321, 7995.31
Duration      [total, attack, wait]    1h44m32.487877345s, 1h44m32.215505911s, 272.371434ms
Latencies     [mean, 50, 95, 99, max]  664.258612ms, 6.174582ms, 81.18445ms, 31.492532412s, 1m43.56427635s
Bytes In      [total, mean]            505140675050, 10072.93
Bytes Out     [total, mean]            1515612949, 30.22
Success       [ratio]                  99.35%
Status Codes  [code:count]             0:326654  200:49821385  500:282
Error Set:
500 Internal Server Error
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": net/http: timeout awaiting response headers
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": dial tcp 0.0.0.0:0->10.201.97.39:443: bind: address already in use
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": dial tcp 0.0.0.0:0->10.201.96.120:443: bind: address already in use
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": dial tcp 0.0.0.0:0->10.201.97.39:443: i/o timeout
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": dial tcp 0.0.0.0:0->10.201.97.192:443: i/o timeout
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": dial tcp 0.0.0.0:0->10.201.98.252:443: bind: address already in use
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": dial tcp 0.0.0.0:0->10.201.98.191:443: i/o timeout
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": dial tcp 0.0.0.0:0->10.201.96.233:443: i/o timeout
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": dial tcp 0.0.0.0:0->10.201.96.120:443: i/o timeout
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": dial tcp 0.0.0.0:0->10.201.98.252:443: i/o timeout
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": dial tcp 0.0.0.0:0->10.201.98.191:443: bind: address already in use
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": dial tcp 0.0.0.0:0->10.201.97.192:443: bind: address already in use
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": dial tcp: i/o timeout
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": EOF
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": http: server closed idle connection
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": dial tcp 0.0.0.0:0->10.201.96.233:443: bind: address already in use
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": net/http: TLS handshake timeout
```

