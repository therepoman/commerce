---
title: "Results GetAvailableFollowerEmotes_burst1 2021 06"
date: 2021-06-21T18:57:05-07:00
hidden: true
---


### Host 10.201.96.167

```
Requests      [total, rate]            3742503, 4161.17
Duration      [total, attack, wait]    14m59.388409889s, 14m59.386738629s, 1.67126ms
Latencies     [mean, 50, 95, 99, max]  1.355452ms, 1.309297ms, 2.196701ms, 2.551606ms, 218.638135ms
Bytes In      [total, mean]            903197645, 241.34
Bytes Out     [total, mean]            124999536, 33.40
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:3742503  
Error Set:
```

