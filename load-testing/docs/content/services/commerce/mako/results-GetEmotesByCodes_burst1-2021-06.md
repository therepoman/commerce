---
title: "Results GetEmotesByCodes_burst1 2021 06"
date: 2021-06-16T15:30:52-07:00
hidden: true
---


### Host 10.201.97.60

```
Requests      [total, rate]            2021103, 2244.57
Duration      [total, attack, wait]    15m0.451095827s, 15m0.442946949s, 8.148878ms
Latencies     [mean, 50, 95, 99, max]  2.935164ms, 1.890709ms, 8.167227ms, 10.363851ms, 252.773168ms
Bytes In      [total, mean]            4051623, 2.00
Bytes Out     [total, mean]            325705146, 161.15
Success       [ratio]                  100.00%
Status Codes  [code:count]             0:2  200:2021079  500:22  
Error Set:
500 Internal Server Error
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmotesByCodes": EOF
```

