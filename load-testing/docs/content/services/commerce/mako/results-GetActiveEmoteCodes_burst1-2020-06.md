---
title: "Results GetActiveEmoteCodes_burst1 2020 06"
date: 2020-06-16T16:30:58-07:00
hidden: true
---


### Host 10.201.96.39

```
Requests      [total, rate]            1122753, 1225.51
Duration      [total, attack, wait]    15m16.269223511s, 15m16.150891007s, 118.332504ms
Latencies     [mean, 50, 95, 99, max]  6.897327ms, 5.449323ms, 10.473751ms, 18.600883ms, 779.136017ms
Bytes In      [total, mean]            12847223427, 11442.61
Bytes Out     [total, mean]            34154483, 30.42
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1122745  500:8
Error Set:
500 Internal Server Error
```

