---
title: "Results GetEmotesByCodes_burst2 2021 06"
date: 2021-06-16T15:30:58-07:00
hidden: true
---


### Host 10.201.97.60

```
Requests      [total, rate]            2694753, 2993.50
Duration      [total, attack, wait]    15m0.206929443s, 15m0.200544988s, 6.384455ms
Latencies     [mean, 50, 95, 99, max]  2.909836ms, 1.891264ms, 8.081817ms, 10.154135ms, 252.01653ms
Bytes In      [total, mean]            5403310, 2.01
Bytes Out     [total, mean]            434248359, 161.15
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2694749  500:4  
Error Set:
500 Internal Server Error
```

