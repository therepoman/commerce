---
title: "Mako"
tags: ["emotes", "digital assets", "chat", "materia"]
---


### Links

- [Code](https://git.xarth.tv/commerce/mako)
- [Runbook](https://docs.google.com/document/d/1gYxLWV0mWDnurGl2d1WLgdJJaLwFhvvrDiTRJ6lgts4/edit#heading=h.ymtrjjqc2jso)
- [Cloudwatch Dashboards](https://isengard.amazon.com/federate?account=671452935478&role=admin&destination=cloudwatch%2Fhome%3Fregion%3Dus-west-2%23dashboards%3Aname%3DMako%3Bstart%3DP7D)

### Running

These tests are orchestrated via the `Makefile` using [piledriver][pl].

[pl]: ../../../getting-started/piledriver

{{% notice tip %}}
You may want to export the following environment variables - otherwise you'll need to include these on all of the Piledriver
makefile commands.
{{% /notice %}}

```bash
export AWS_PROFILE=mako-prod
export USERNAME=$(whoami)
export TC=mako-prod
```

Next, follow the [piledriver][pl] instructions, or use this as the TL;DR:

```bash
$ cd load-tests/services/commerce/mako
$ make init NUM_HOSTS=1
$ make plan
$ make apply
$ make list_hosts
$ make set_file_descriptors
$ make prep                     # from here and above only need to be done once
$ make start LOADTEST_FILES="<test-name>.toml <data-file>.toml <data-file>.toml"
$ make check_status
$ make stop                     # optional - if needed
$ make get_results OUTPUT_DIR=results/<test-name>/<YYYY-MM>
$ make publish_results RESULTS_DIR=results/<test-name>/<YYYY-MM>
```

For quick reference, here are the tests that are implemented:

```bash
$ make start LOADTEST_FILES="GetEmotesByCodes_ramp.toml user_ids_50k.toml emote_codes.toml"
$ make start LOADTEST_FILES="GetEmotesByCodes_burst1.toml user_ids_50k.toml emote_codes.toml"
# Repeat ^ for burst2 and burst3
$ make start LOADTEST_FILES="GetEmoteSetDetails_ramp.toml user_ids_50k.toml"
$ make start LOADTEST_FILES="GetEmoteSetDetails_burst1.toml user_ids_50k.toml"
# Repeat ^ for burst2 and burst3
```

Make sure to commit all result files to this repo.


### Results

{{% children showhidden="true" %}}
