---
title: "Results GetAvailableFollowerEmotes_burst3 2021 06"
date: 2021-06-21T18:57:15-07:00
hidden: true
---


### Host 10.201.96.167

```
Requests      [total, rate]            7485003, 8104.22
Duration      [total, attack, wait]    15m23.594605061s, 15m23.593259137s, 1.345924ms
Latencies     [mean, 50, 95, 99, max]  24.252146ms, 1.231802ms, 2.175264ms, 6.383367ms, 24.604275817s
Bytes In      [total, mean]            1805190566, 241.17
Bytes Out     [total, mean]            249998398, 33.40
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:7485003  
Error Set:
```

