---
title: "Results GetEmoteSetDetails_burst2 2020 06"
date: 2020-06-16T16:30:39-07:00
hidden: true
---


### Host 10.201.96.39

```
Requests      [total, rate]            2994003, 3246.96
Duration      [total, attack, wait]    15m22.147955427s, 15m22.095519864s, 52.435563ms
Latencies     [mean, 50, 95, 99, max]  6.941543ms, 5.370172ms, 10.413777ms, 20.619926ms, 1.239150144s
Bytes In      [total, mean]            30356296027, 10139.03
Bytes Out     [total, mean]            91081225, 30.42
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2993986  500:17
Error Set:
500 Internal Server Error
```

