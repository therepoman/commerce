---
title: "Results GetActiveEmoteCodes_burst2 2020 06"
date: 2020-06-16T16:30:55-07:00
hidden: true
---


### Host 10.201.96.39

```
Requests      [total, rate]            1497003, 1630.19
Duration      [total, attack, wait]    15m18.363326376s, 15m18.30030058s, 63.025796ms
Latencies     [mean, 50, 95, 99, max]  6.85096ms, 5.390861ms, 10.7028ms, 19.095629ms, 786.578015ms
Bytes In      [total, mean]            17135198683, 11446.34
Bytes Out     [total, mean]            45538550, 30.42
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1496993  500:10
Error Set:
500 Internal Server Error
```

