---
title: "Results GetEmotesByCodes_burst3 2021 06"
date: 2021-06-16T15:31:03-07:00
hidden: true
---


### Host 10.201.97.60

```
Requests      [total, rate]            4116903, 4569.79
Duration      [total, attack, wait]    15m0.9106744s, 15m0.895411543s, 15.262857ms
Latencies     [mean, 50, 95, 99, max]  3.329505ms, 1.869547ms, 8.235242ms, 11.624015ms, 253.403586ms
Bytes In      [total, mean]            8252662, 2.00
Bytes Out     [total, mean]            663402515, 161.14
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:4116872  500:31  
Error Set:
500 Internal Server Error
```

