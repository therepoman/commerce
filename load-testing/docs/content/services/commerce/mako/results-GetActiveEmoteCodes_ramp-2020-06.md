---
title: "Results GetActiveEmoteCodes_ramp 2020 06"
date: 2020-06-16T16:31:01-07:00
hidden: true
---


### Host 10.201.96.39

```
Requests      [total, rate]            10746036, 1982.45
Duration      [total, attack, wait]    1h30m20.75373198s, 1h30m20.596259698s, 157.472282ms
Latencies     [mean, 50, 95, 99, max]  7.157944ms, 5.428103ms, 11.852332ms, 21.636864ms, 1.681798706s
Bytes In      [total, mean]            122964434258, 11442.77
Bytes Out     [total, mean]            326901623, 30.42
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:10745941  500:95
Error Set:
500 Internal Server Error
```

