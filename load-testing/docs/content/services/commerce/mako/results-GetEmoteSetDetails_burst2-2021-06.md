---
title: "Results GetEmoteSetDetails_burst2 2021 06"
date: 2021-06-22T15:16:18-07:00
hidden: true
---


### Host 10.201.96.141

```
Requests      [total, rate]            2744898, 3039.43
Duration      [total, attack, wait]    15m3.109704245s, 15m3.095315713s, 14.388532ms
Latencies     [mean, 50, 95, 99, max]  9.889642ms, 9.158922ms, 15.602749ms, 25.479419ms, 459.207754ms
Bytes In      [total, mean]            100920409545, 36766.54
Bytes Out     [total, mean]            166744536, 60.75
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2744896  500:2  
Error Set:
500 Internal Server Error
```

### Host 10.201.97.85

```
Requests      [total, rate]            2744898, 3038.69
Duration      [total, attack, wait]    15m3.328142256s, 15m3.315666612s, 12.475644ms
Latencies     [mean, 50, 95, 99, max]  9.627871ms, 8.910954ms, 15.37388ms, 25.228555ms, 280.83973ms
Bytes In      [total, mean]            100870780323, 36748.46
Bytes Out     [total, mean]            166743526, 60.75
Success       [ratio]                  100.00%
Status Codes  [code:count]             0:1  200:2744897  
Error Set:
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": EOF
```

### Host 10.201.98.122

```
Requests      [total, rate]            2744898, 3039.71
Duration      [total, attack, wait]    15m3.031423851s, 15m3.01421432s, 17.209531ms
Latencies     [mean, 50, 95, 99, max]  9.673995ms, 8.973505ms, 15.412674ms, 25.230912ms, 278.217069ms
Bytes In      [total, mean]            100886129554, 36754.05
Bytes Out     [total, mean]            166744362, 60.75
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2744898  
Error Set:
```

