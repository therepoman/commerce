---
title: "Results GetAvailableFollowerEmotes_ramp4 2021 06"
date: 2021-06-21T18:56:56-07:00
hidden: true
---


### Host 10.201.96.167

```
Requests      [total, rate]            35820059, 6634.17
Duration      [total, attack, wait]    1h29m59.331442329s, 1h29m59.329874449s, 1.56788ms
Latencies     [mean, 50, 95, 99, max]  1.926034ms, 1.256921ms, 2.178791ms, 4.084935ms, 2.444123721s
Bytes In      [total, mean]            7765165838, 216.78
Bytes Out     [total, mean]            1196384622, 33.40
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:35820059  
Error Set:
```

