---
title: "Results GetEmoteSetDetails_ramp 2021 06"
date: 2021-06-22T15:16:08-07:00
hidden: true
---


### Host 10.201.96.141

```
Requests      [total, rate]            17671824, 3270.22
Duration      [total, attack, wait]    1h30m3.874874863s, 1h30m3.857943485s, 16.931378ms
Latencies     [mean, 50, 95, 99, max]  10.310574ms, 9.313294ms, 16.503195ms, 28.708353ms, 1.194998656s
Bytes In      [total, mean]            649419185342, 36748.85
Bytes Out     [total, mean]            1073506627, 60.75
Success       [ratio]                  99.99%
Status Codes  [code:count]             0:3  200:17670821  500:1000  
Error Set:
500 Internal Server Error
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": EOF
```

### Host 10.201.97.85

```
Requests      [total, rate]            17671824, 3270.01
Duration      [total, attack, wait]    1h30m4.265203778s, 1h30m4.211350154s, 53.853624ms
Latencies     [mean, 50, 95, 99, max]  9.92994ms, 9.059697ms, 15.960674ms, 27.565717ms, 640.575954ms
Bytes In      [total, mean]            649542031972, 36755.80
Bytes Out     [total, mean]            1073502233, 60.75
Success       [ratio]                  99.99%
Status Codes  [code:count]             200:17670717  500:1107  
Error Set:
500 Internal Server Error
```

### Host 10.201.98.122

```
Requests      [total, rate]            17671824, 3270.38
Duration      [total, attack, wait]    1h30m3.611328242s, 1h30m3.601683341s, 9.644901ms
Latencies     [mean, 50, 95, 99, max]  10.059078ms, 9.210137ms, 16.082029ms, 27.635534ms, 618.70444ms
Bytes In      [total, mean]            649544823155, 36755.96
Bytes Out     [total, mean]            1073507616, 60.75
Success       [ratio]                  99.99%
Status Codes  [code:count]             0:2  200:17670744  500:1078  
Error Set:
500 Internal Server Error
Post "https://main.us-west-2.prod.mako.twitch.a2z.com/twirp/code.justin.tv.commerce.mako.Mako/GetEmoteSetDetails": EOF
```

