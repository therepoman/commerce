---
title: "Results GetActiveEmoteCodes_burst3 2020 06"
date: 2020-06-16T16:30:52-07:00
hidden: true
---


### Host 10.201.96.39

```
Requests      [total, rate]            2245503, 2429.52
Duration      [total, attack, wait]    15m24.460216074s, 15m24.256598153s, 203.617921ms
Latencies     [mean, 50, 95, 99, max]  7.722168ms, 5.710026ms, 14.247491ms, 24.038952ms, 1.677815043s
Bytes In      [total, mean]            25691755979, 11441.43
Bytes Out     [total, mean]            68309494, 30.42
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2245479  500:24
Error Set:
500 Internal Server Error
```

