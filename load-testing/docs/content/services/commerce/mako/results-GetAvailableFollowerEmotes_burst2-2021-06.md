---
title: "Results GetAvailableFollowerEmotes_burst2 2021 06"
date: 2021-06-21T18:57:10-07:00
hidden: true
---


### Host 10.201.96.167

```
Requests      [total, rate]            4940103, 5493.57
Duration      [total, attack, wait]    14m59.252397071s, 14m59.251192691s, 1.20438ms
Latencies     [mean, 50, 95, 99, max]  1.3206ms, 1.278399ms, 2.18618ms, 2.541567ms, 211.246521ms
Bytes In      [total, mean]            1192359233, 241.36
Bytes Out     [total, mean]            164999358, 33.40
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:4940103  
Error Set:
```

