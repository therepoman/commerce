---
title: "Results GetEmotesByCodes_ramp 2021 06"
date: 2021-06-16T15:31:09-07:00
hidden: true
---


### Host 10.201.97.60

```
Requests      [total, rate]            19702836, 3648.17
Duration      [total, attack, wait]    1h30m0.759007968s, 1h30m0.751630769s, 7.377199ms
Latencies     [mean, 50, 95, 99, max]  2.9217ms, 1.903981ms, 8.055434ms, 10.088047ms, 838.577992ms
Bytes In      [total, mean]            39490500, 2.00
Bytes Out     [total, mean]            3175057591, 161.15
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:19702686  500:150  
Error Set:
500 Internal Server Error
```

