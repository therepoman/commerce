---
title: "Results GetAvailableFollowerEmotes_ramp1 2021 06"
date: 2021-06-21T18:56:19-07:00
hidden: true
---


### Host 10.201.96.167

```
Requests      [total, rate]            36180, 6.75
Duration      [total, attack, wait]    1h29m17.069569398s, 1h29m17.065262229s, 4.307169ms
Latencies     [mean, 50, 95, 99, max]  4.260508ms, 4.447769ms, 7.171543ms, 11.80351ms, 231.527442ms
Bytes In      [total, mean]            7870524, 217.54
Bytes Out     [total, mean]            1208439, 33.40
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:36180  
Error Set:
```

