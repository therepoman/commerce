---
title: "Results GetEmoteSetDetails_burst1 2020 06"
date: 2020-06-16T16:30:37-07:00
hidden: true
---


### Host 10.201.96.39

```
Requests      [total, rate]            2245503, 2432.02
Duration      [total, attack, wait]    15m23.707044992s, 15m23.306894967s, 400.150025ms
Latencies     [mean, 50, 95, 99, max]  7.735248ms, 5.7776ms, 14.473059ms, 24.55009ms, 1.218517076s
Bytes In      [total, mean]            22767472561, 10139.14
Bytes Out     [total, mean]            68310356, 30.42
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2245484  500:19
Error Set:
500 Internal Server Error
```

