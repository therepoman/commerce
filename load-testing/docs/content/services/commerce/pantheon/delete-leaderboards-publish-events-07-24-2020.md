---
title: "DeleteLeaderboards and PublishEvent Load Test: 07-24-2020"
date: 2020-05-06T14:01:37-07:00
hidden: true
---

### Background and Context
As part of the [Privacy initiative](https://wiki.twitch.com/display/TTV/Privacy) to conform to [GDPR standards](https://gdpr-info.eu/) we are under the obligation to delete all information we store about a user if requested by them. 
To do this in Pantheon we proposed exposing a `DeleteLeaderboards` API that would kick off a step function that accepts a `groupingKey` (a userID) and would `SCAN` Redis to find leaderboards for that user. (We cannot not simply `DEL` leaderboards as we don't know the keys, which are in the format `{domain}/{groupingKey}/{timePeriod}/{timeBucket}`.)
We'd then delete all matching leaderboards across all time periods. 

Since Pantheon is read-heavy and its writes are async, we decided to scan against the primary node which is only used for writes. Scanning against the read-replicas was also giving weird results (duplicate records, missing records) when compared to subsequent identical scans.

This load test describes the impact of scanning the Redis write node while receiving regular prod traffic for `PublishEvent`.  

### Test parameters
The load test was run in Pantheon staging. The `PublishEvent` API was run at 30 TPS for 40 mins. At the start of the load test 900 executions (15 TPS for 1 min) of the `DeleteLeaderboards` step function were executed. This closely simulates the traffic we'd see in production. 

#### DeleteLeaderboards
This API kicks off a step function that contains a single Lambda. This lambda will perform up to 200 scans, with each scan iterating over 100 items. If there are more items to scan it will return a `PagingKey` and another execution of this Lambda will start after a 30 second wait in the step function definition. 
The wait was added, as well as having lower scan count and loop iterations, to alleviate some of the load on Redis.    

#### PublishEvents
This API writes new leaderboard entries in Redis. We used random tuids to ensure that new entries were always being added and not failing due to duplicate entries. 

## Results

#### DeleteLeaderboards
Executions of the step functions were taking upwards of 1 hour when all 900 were running simultaneously. When run alone, a single execution will complete in less than 20 seconds (in staging). 
In production the size of the Redis cluster is 15x larger than staging, so it could potentially take 15x longer to perform the scans. 

#### Latency of PublishEvent
The `ingest-time` metric used to track the majority of the PublishEvent API saw high latencies of 20-35 seconds. This is a huge increase from regular traffic which takes milliseconds.

!["PublishEvent latency"](../../../../images/commerce/pantheon/07-24-2020/publish-event-latency.png) 

#### CPU Utilization
Redis Engine CPU Utilization was hitting around 75% for the entirety of the load test, while Redis CPU Utilization rose to 6%.

#### DLQs
We had 3 messages appear in the PublishEvent DLQ due to failing to obtain a redis lock. This is a good indication that there was high contention to write to Redis.

```
msg="Error occurred in SQS workers" error="error obtaining an ingestion lock for the event"
msg="error obtaining an ingestion lock for the event" error="failed to obtain redis lock"
```

### Conclusion
Performing all of the scans simultaneously is not going to scale for us. Currently the [UserDestroy](https://wiki.twitch.com/display/SEC/The+Deletion+Flow#TheDeletionFlow-UserDestroyEvent:) event from PDMS batches up requests and releases them once a day. Our tenants of Pantheon will receive these requests and call the `DeleteLeaderboards` API to perform the user deletion. 
If we receive all these requests at once we will see impact to our write latency, as well as possible failures due to contention (messages ending up ion DLQs). 
Possible solutions to this would be throttle the number of scans being performed at a single time, or working to avoid scans altogether. 

## Raw Results
These results aren't too important. The `PublishEvent` API simply sends a message to SQS and then returns successfully. The `DeleteLeadeboards` API starts a step function execution and returns successfully. 
Thus the status codes reported here aren't a good indication of how the test actually went. 

### Host 10.201.181.129

```
Requests      [total, rate]            72900, 30.40
Duration      [total, attack, wait]    39m57.792581234s, 39m57.78575135s, 6.829884ms
Latencies     [mean, 50, 95, 99, max]  11.407301ms, 7.07519ms, 38.49265ms, 76.55432ms, 816.36885ms
Bytes In      [total, mean]            145800, 2.00
Bytes Out     [total, mean]            12460488, 170.93
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:72900  
Error Set:

```
