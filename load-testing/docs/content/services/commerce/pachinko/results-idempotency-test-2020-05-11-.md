---
title: "Results Idempotency Test 2020 05 11 "
date: 2020-05-11T17:52:13-07:00
hidden: true
---


### Host 10.205.0.68

```
Requests      [total, rate]            30000, 100.13
Duration      [total, attack, wait]    4m59.628711971s, 4m59.622492379s, 6.219592ms
Latencies     [mean, 50, 95, 99, max]  5.664166ms, 5.372174ms, 7.079569ms, 12.62716ms, 188.973351ms
Bytes In      [total, mean]            4019965, 134.00
Bytes Out     [total, mean]            4950000, 165.00
Success       [ratio]                  99.98%
Status Codes  [code:count]             200:29995  500:5  
Error Set:
500 Internal Server Error
```

