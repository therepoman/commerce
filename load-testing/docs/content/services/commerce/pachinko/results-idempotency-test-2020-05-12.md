---
title: "Results Idempotency Test 2020 05 12"
date: 2020-05-12T10:09:03-07:00
hidden: true
---


### Host 10.205.0.68

```
Requests      [total, rate]            300000, 997.81
Duration      [total, attack, wait]    5m0.662574664s, 5m0.657743096s, 4.831568ms
Latencies     [mean, 50, 95, 99, max]  5.63599ms, 5.219643ms, 6.907738ms, 13.80702ms, 501.976509ms
Bytes In      [total, mean]            40150583, 133.84
Bytes Out     [total, mean]            49500000, 165.00
Success       [ratio]                  99.98%
Status Codes  [code:count]             200:299937  500:63  
Error Set:
500 Internal Server Error
```

