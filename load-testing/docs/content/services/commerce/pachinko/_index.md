---
title: "Pachinko"
tags: ["moments", "bits", "community points", "sub tokens", "wallet", "free subs", "bounty board"]
---


### Links

- [Code](https://git.xarth.tv/commerce/pachinko)
- [Runbook](https://git.xarth.tv/pages/commerce/pachinko-docs)
- etc...


###  Running 

### Add Tokens Idempotency Test

First, you'll need to generate requests by running

```bash
python3 generate_data_for_idempotency_test.py -n 1000 -o requests-1k.toml 
```

This will create requests that we can validate later. Next, start the load test by running

```bash
make start LOADTEST_FILES="add-tokens-idempotency-1k.toml requests-1k.toml"
```

Then, after the load test is complete, run the following command to validate the resulsts were idempotent

```bash
python3 validate_results.py -r requests-1k.toml
```

If nothing is outputted during this script, that means we were all good.

### Results
{{% children showhidden="true" %}}
