---
title: "Prism"
tags: ["moments", "bits"]
---

### Setup
To be able to run this load test, you need to ensure the users sending messages have enough Bits entitled to their account.
You can do this via the [Admin Panel](https://admin-panel.internal.justin.tv/bits/jobs).
Create a Job to entitle Bits for the users found in the `user_ids_10k.toml` file. 
The amount of Bits to entitle depends on the number of users and the TPS of the test. For the test run on 4/21/2020, 20,000 Bits we're entitled to the 10,000 users. 

Contact the Moments team if you need access. 

### Links

- [Code](https://git.xarth.tv/commerce/prism)
- [Runbook](https://git.xarth.tv/commerce/bits-oncall)


###  Running

```bash
make start LOADTEST_FILES="process-message.toml user_ids_10k.toml"
```

### Results

{{% children showhidden="true" %}}
