---
title: "Results GetUserProductSubscriptionsRamp 2020 02"
date: 2021-02-22T22:15:23-08:00
hidden: true
---


### Host 10.207.200.102

```
Requests      [total, rate]            21492037, 3978.80
Duration      [total, attack, wait]    1h30m1.645063553s, 1h30m1.633929411s, 11.134142ms
Latencies     [mean, 50, 95, 99, max]  8.941996ms, 8.674999ms, 11.465403ms, 17.197854ms, 513.342437ms
Bytes In      [total, mean]            43483667, 2.02
Bytes Out     [total, mean]            1351502289, 62.88
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:21491540  400:62  500:435  
Error Set:
400 Bad Request
500 Internal Server Error
```

### Host 10.207.200.146

```
Requests      [total, rate]            21492037, 3977.87
Duration      [total, attack, wait]    1h30m2.910509147s, 1h30m2.894422437s, 16.08671ms
Latencies     [mean, 50, 95, 99, max]  9.143534ms, 8.852335ms, 11.636802ms, 17.417264ms, 519.929977ms
Bytes In      [total, mean]            43462824, 2.02
Bytes Out     [total, mean]            1351517730, 62.88
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:21491541  400:71  500:425  
Error Set:
400 Bad Request
500 Internal Server Error
```

### Host 10.207.200.239

```
Requests      [total, rate]            21492037, 3978.41
Duration      [total, attack, wait]    1h30m2.18373428s, 1h30m2.170605581s, 13.128699ms
Latencies     [mean, 50, 95, 99, max]  9.181902ms, 8.891445ms, 11.66707ms, 17.479007ms, 515.36887ms
Bytes In      [total, mean]            43464435, 2.02
Bytes Out     [total, mean]            1351510537, 62.88
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:21491560  400:69  500:408  
Error Set:
400 Bad Request
500 Internal Server Error
```

### Host 10.207.200.79

```
Requests      [total, rate]            21492037, 3978.60
Duration      [total, attack, wait]    1h30m1.920142279s, 1h30m1.911793721s, 8.348558ms
Latencies     [mean, 50, 95, 99, max]  8.943084ms, 8.680068ms, 11.469689ms, 17.179817ms, 510.215055ms
Bytes In      [total, mean]            43470232, 2.02
Bytes Out     [total, mean]            1351513074, 62.88
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:21491574  400:66  500:397  
Error Set:
400 Bad Request
500 Internal Server Error
```

### Host 10.207.201.87

```
Requests      [total, rate]            21492037, 3978.55
Duration      [total, attack, wait]    1h30m1.99572348s, 1h30m1.982823971s, 12.899509ms
Latencies     [mean, 50, 95, 99, max]  8.913836ms, 8.647845ms, 11.473933ms, 17.191497ms, 525.13523ms
Bytes In      [total, mean]            43446407, 2.02
Bytes Out     [total, mean]            1351517145, 62.88
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:21491578  400:58  500:401  
Error Set:
400 Bad Request
500 Internal Server Error
```

