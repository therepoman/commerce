---
title: "Results GetUserChannelSubscription 2020 02"
date: 2021-02-22T11:29:55-08:00
hidden: true
---


### Host 10.207.200.102

```
Requests      [total, rate]            748503, 831.68
Duration      [total, attack, wait]    15m0.003849649s, 14m59.987754946s, 16.094703ms
Latencies     [mean, 50, 95, 99, max]  5.127671ms, 3.956478ms, 10.204015ms, 13.072627ms, 925.515753ms
Bytes In      [total, mean]            1497365, 2.00
Bytes Out     [total, mean]            47126263, 62.96
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:748501  400:1  500:1  
Error Set:
400 Bad Request
500 Internal Server Error
```

### Host 10.207.200.146

```
Requests      [total, rate]            748503, 831.67
Duration      [total, attack, wait]    15m0.000435846s, 14m59.994896169s, 5.539677ms
Latencies     [mean, 50, 95, 99, max]  5.321786ms, 4.242908ms, 10.391204ms, 13.355146ms, 502.35855ms
Bytes In      [total, mean]            1497616, 2.00
Bytes Out     [total, mean]            47127209, 62.96
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:748499  400:3  500:1  
Error Set:
400 Bad Request
500 Internal Server Error
```

### Host 10.207.200.239

```
Requests      [total, rate]            748503, 831.67
Duration      [total, attack, wait]    15m0.009145884s, 15m0.004218039s, 4.927845ms
Latencies     [mean, 50, 95, 99, max]  5.300918ms, 4.250344ms, 10.375606ms, 13.32128ms, 502.15396ms
Bytes In      [total, mean]            1497834, 2.00
Bytes Out     [total, mean]            47126824, 62.96
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:748501  400:1  500:1  
Error Set:
500 Internal Server Error
400 Bad Request
```

### Host 10.207.200.79

```
Requests      [total, rate]            748503, 831.88
Duration      [total, attack, wait]    14m59.780328162s, 14m59.772944596s, 7.383566ms
Latencies     [mean, 50, 95, 99, max]  5.077588ms, 3.916313ms, 10.130645ms, 12.98948ms, 912.406015ms
Bytes In      [total, mean]            1497621, 2.00
Bytes Out     [total, mean]            47125949, 62.96
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:748500  400:2  500:1  
Error Set:
400 Bad Request
500 Internal Server Error
```

### Host 10.207.201.87

```
Requests      [total, rate]            748503, 831.21
Duration      [total, attack, wait]    15m0.504744934s, 15m0.49940819s, 5.336744ms
Latencies     [mean, 50, 95, 99, max]  5.123982ms, 3.949269ms, 10.246553ms, 13.14143ms, 870.311172ms
Bytes In      [total, mean]            1497650, 2.00
Bytes Out     [total, mean]            47126007, 62.96
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:748501  400:2  
Error Set:
400 Bad Request
```

