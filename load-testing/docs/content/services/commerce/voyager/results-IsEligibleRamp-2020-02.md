---
title: "Results IsEligibleRamp 2020 02"
date: 2021-02-24T15:45:38-08:00
hidden: true
---


### Host 10.207.200.102

```
Requests      [total, rate]            4298436, 795.20
Duration      [total, attack, wait]    1h30m5.476493122s, 1h30m5.446483004s, 30.010118ms
Latencies     [mean, 50, 95, 99, max]  27.734096ms, 26.984076ms, 34.599369ms, 46.790044ms, 1.798762521s
Bytes In      [total, mean]            3950047357, 918.95
Bytes Out     [total, mean]            20243424665, 4709.49
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:4298418  400:18  
Error Set:
400 Bad Request
```

### Host 10.207.200.146

```
Requests      [total, rate]            4298436, 795.02
Duration      [total, attack, wait]    1h30m6.767762543s, 1h30m6.734565526s, 33.197017ms
Latencies     [mean, 50, 95, 99, max]  28.041822ms, 27.284243ms, 34.890222ms, 47.11937ms, 1.828212007s
Bytes In      [total, mean]            3950048997, 918.95
Bytes Out     [total, mean]            20243423637, 4709.49
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:4298422  400:14  
Error Set:
400 Bad Request
```

### Host 10.207.200.239

```
Requests      [total, rate]            4298436, 795.20
Duration      [total, attack, wait]    1h30m5.498282047s, 1h30m5.46208097s, 36.201077ms
Latencies     [mean, 50, 95, 99, max]  27.989905ms, 27.234658ms, 34.846065ms, 47.047596ms, 1.074750764s
Bytes In      [total, mean]            3950049847, 918.95
Bytes Out     [total, mean]            20243426742, 4709.49
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:4298421  400:15  
Error Set:
400 Bad Request
```

### Host 10.207.200.79

```
Requests      [total, rate]            4298436, 795.19
Duration      [total, attack, wait]    1h30m5.542175522s, 1h30m5.518069137s, 24.106385ms
Latencies     [mean, 50, 95, 99, max]  27.780251ms, 27.034748ms, 34.638449ms, 46.884947ms, 1.699693477s
Bytes In      [total, mean]            3950059172, 918.95
Bytes Out     [total, mean]            20243425308, 4709.49
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:4298428  400:8  
Error Set:
400 Bad Request
```

### Host 10.207.201.87

```
Requests      [total, rate]            4298436, 795.17
Duration      [total, attack, wait]    1h30m5.707372207s, 1h30m5.678725893s, 28.646314ms
Latencies     [mean, 50, 95, 99, max]  27.843026ms, 27.092446ms, 34.69001ms, 46.991917ms, 1.076730545s
Bytes In      [total, mean]            3950048359, 918.95
Bytes Out     [total, mean]            20243428139, 4709.49
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:4298423  400:13  
Error Set:
400 Bad Request
```

