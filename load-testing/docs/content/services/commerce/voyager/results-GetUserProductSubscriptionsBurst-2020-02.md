---
title: "Results GetUserProductSubscriptionsBurst 2020 02"
date: 2021-02-22T19:28:25-08:00
hidden: true
---


### Host 10.207.200.102

```
Requests      [total, rate]            748503, 831.01
Duration      [total, attack, wait]    15m0.729244315s, 15m0.718530201s, 10.714114ms
Latencies     [mean, 50, 95, 99, max]  8.701342ms, 8.359206ms, 11.206944ms, 18.042339ms, 354.860651ms
Bytes In      [total, mean]            1508407, 2.02
Bytes Out     [total, mean]            47069172, 62.88
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:748501  400:2  
Error Set:
400 Bad Request
```

### Host 10.207.200.146

```
Requests      [total, rate]            748503, 830.96
Duration      [total, attack, wait]    15m0.779050379s, 15m0.771210331s, 7.840048ms
Latencies     [mean, 50, 95, 99, max]  8.931408ms, 8.579052ms, 11.447266ms, 18.406105ms, 333.023205ms
Bytes In      [total, mean]            1510706, 2.02
Bytes Out     [total, mean]            47067599, 62.88
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:748501  400:2  
Error Set:
400 Bad Request
```

### Host 10.207.200.239

```
Requests      [total, rate]            748503, 831.09
Duration      [total, attack, wait]    15m0.644595164s, 15m0.633047007s, 11.548157ms
Latencies     [mean, 50, 95, 99, max]  8.935705ms, 8.594342ms, 11.44382ms, 18.105362ms, 347.785764ms
Bytes In      [total, mean]            1510387, 2.02
Bytes Out     [total, mean]            47069120, 62.88
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:748502  400:1  
Error Set:
400 Bad Request
```

### Host 10.207.200.79

```
Requests      [total, rate]            748503, 831.07
Duration      [total, attack, wait]    15m0.66412298s, 15m0.649901578s, 14.221402ms
Latencies     [mean, 50, 95, 99, max]  8.805001ms, 8.469064ms, 11.315634ms, 17.923525ms, 358.330783ms
Bytes In      [total, mean]            1508753, 2.02
Bytes Out     [total, mean]            47069793, 62.89
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:748501  400:2  
Error Set:
400 Bad Request
```

### Host 10.207.201.87

```
Requests      [total, rate]            748503, 831.25
Duration      [total, attack, wait]    15m0.457331918s, 15m0.449759243s, 7.572675ms
Latencies     [mean, 50, 95, 99, max]  8.819888ms, 8.484915ms, 11.396197ms, 17.98329ms, 345.431302ms
Bytes In      [total, mean]            1508391, 2.02
Bytes Out     [total, mean]            47067460, 62.88
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:748502  400:1  
Error Set:
400 Bad Request
```

