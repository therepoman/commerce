---
title: "Results GetUserChannelSubscriptionRamp 2020 02"
date: 2021-02-22T19:00:23-08:00
hidden: true
---


### Host 10.207.200.102

```
Requests      [total, rate]            17910036, 3315.63
Duration      [total, attack, wait]    1h30m1.702366306s, 1h30m1.694481488s, 7.884818ms
Latencies     [mean, 50, 95, 99, max]  4.962101ms, 3.905467ms, 9.671701ms, 12.213262ms, 502.634292ms
Bytes In      [total, mean]            35836018, 2.00
Bytes Out     [total, mean]            1127633810, 62.96
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:17909964  400:58  500:14  
Error Set:
400 Bad Request
500 Internal Server Error
```

### Host 10.207.200.146

```
Requests      [total, rate]            17910036, 3315.95
Duration      [total, attack, wait]    1h30m1.193441139s, 1h30m1.183716398s, 9.724741ms
Latencies     [mean, 50, 95, 99, max]  5.211543ms, 4.265348ms, 9.923091ms, 12.481985ms, 3.351624908s
Bytes In      [total, mean]            35840067, 2.00
Bytes Out     [total, mean]            1127638780, 62.96
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:17909953  400:65  500:18  
Error Set:
400 Bad Request
500 Internal Server Error
```

### Host 10.207.200.239

```
Requests      [total, rate]            17910036, 3316.08
Duration      [total, attack, wait]    1h30m0.96717382s, 1h30m0.961149646s, 6.024174ms
Latencies     [mean, 50, 95, 99, max]  5.223663ms, 4.272591ms, 9.937989ms, 12.504655ms, 3.33955519s
Bytes In      [total, mean]            35839458, 2.00
Bytes Out     [total, mean]            1127637826, 62.96
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:17909959  400:60  500:17  
Error Set:
400 Bad Request
500 Internal Server Error
```

### Host 10.207.200.79

```
Requests      [total, rate]            17910036, 3315.61
Duration      [total, attack, wait]    1h30m1.744608262s, 1h30m1.736119252s, 8.48901ms
Latencies     [mean, 50, 95, 99, max]  4.985091ms, 3.91926ms, 9.692772ms, 12.250304ms, 503.364036ms
Bytes In      [total, mean]            35834249, 2.00
Bytes Out     [total, mean]            1127632335, 62.96
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:17909972  400:52  500:12  
Error Set:
400 Bad Request
500 Internal Server Error
```

### Host 10.207.201.87

```
Requests      [total, rate]            17910036, 3315.71
Duration      [total, attack, wait]    1h30m1.574399937s, 1h30m1.563963171s, 10.436766ms
Latencies     [mean, 50, 95, 99, max]  5.04752ms, 3.991558ms, 9.798004ms, 12.347227ms, 504.077853ms
Bytes In      [total, mean]            35836924, 2.00
Bytes Out     [total, mean]            1127637960, 62.96
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:17909972  400:53  500:11  
Error Set:
400 Bad Request
500 Internal Server Error
```

