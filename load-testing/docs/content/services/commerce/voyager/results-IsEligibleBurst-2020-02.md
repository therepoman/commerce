---
title: "Results IsEligibleBurst 2020 02"
date: 2021-02-24T16:06:31-08:00
hidden: true
---


### Host 10.207.200.102

```
Requests      [total, rate]            898203, 991.31
Duration      [total, attack, wait]    15m6.102923475s, 15m6.072980106s, 29.943369ms
Latencies     [mean, 50, 95, 99, max]  27.871487ms, 27.023729ms, 34.664003ms, 48.294842ms, 1.05116071s
Bytes In      [total, mean]            825411983, 918.96
Bytes Out     [total, mean]            4230074160, 4709.49
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:898198  400:5  
Error Set:
400 Bad Request
```

### Host 10.207.200.146

```
Requests      [total, rate]            898203, 991.80
Duration      [total, attack, wait]    15m5.658839854s, 15m5.627656139s, 31.183715ms
Latencies     [mean, 50, 95, 99, max]  27.956785ms, 27.103379ms, 34.735604ms, 48.09863ms, 1.053317546s
Bytes In      [total, mean]            825412603, 918.96
Bytes Out     [total, mean]            4230075073, 4709.49
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:898200  400:3  
Error Set:
400 Bad Request
```

### Host 10.207.200.239

```
Requests      [total, rate]            898203, 991.55
Duration      [total, attack, wait]    15m5.894449962s, 15m5.856535802s, 37.91416ms
Latencies     [mean, 50, 95, 99, max]  28.05301ms, 27.208731ms, 34.796701ms, 48.104127ms, 1.755347927s
Bytes In      [total, mean]            825412330, 918.96
Bytes Out     [total, mean]            4230074558, 4709.49
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:898202  400:1  
Error Set:
400 Bad Request
```

### Host 10.207.200.79

```
Requests      [total, rate]            898203, 990.90
Duration      [total, attack, wait]    15m6.491882104s, 15m6.451435468s, 40.446636ms
Latencies     [mean, 50, 95, 99, max]  27.782181ms, 26.938463ms, 34.571221ms, 47.871047ms, 1.333671541s
Bytes In      [total, mean]            825410782, 918.96
Bytes Out     [total, mean]            4230074210, 4709.49
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:898200  400:3  
Error Set:
400 Bad Request
```

### Host 10.207.201.87

```
Requests      [total, rate]            898203, 991.82
Duration      [total, attack, wait]    15m5.643537215s, 15m5.612396434s, 31.140781ms
Latencies     [mean, 50, 95, 99, max]  27.937043ms, 27.100229ms, 34.704781ms, 48.156672ms, 1.742894995s
Bytes In      [total, mean]            825408347, 918.96
Bytes Out     [total, mean]            4230073650, 4709.49
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:898199  400:4  
Error Set:
400 Bad Request
```

