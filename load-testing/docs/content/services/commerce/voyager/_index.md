---
title: "Voyager"
tags: ["memberships"]
---



### Links

- [Runbook](https://git.xarth.tv/pages/subs/docs/operations/voyager/)
- [Dashboard](https://grafana.xarth.tv/d/daISYFMMz/voyager?orgId=1&refresh=1m&from=now-1h&to=now&var-AWSAccount=voyager-prod-us-west-2%20940853426594&var-Stage=prod&var-Substage=primary&var-LatencyStat=p99.9&var-API=GetUserChannelSubscription&var-API=GetUserProductSubscriptions&var-API=HandleFulfillment&var-DynamoTable=All)


### Running the tests

These tests are orchestrated via the `Makefile` using [piledriver][pl].

[pl]: ../../../getting-started/piledriver

Get set up by enabling teleport-bastion and exporting the following environment variables.
If you don't export these variables, you'll need to provide them as command-line args
for every subsequent make command.


```bash
$ teleport-bastion enable      # the connections to the load testing hosts are tunneled through the jumpbox.
$ teleport-bastion login
$ export AWS_PROFILE=twitch-voyager-prod
$ export USERNAME=$(whoami)
$ export TC=voyager-prod
```

First, update the test plan files with the desired TPS. Keep in mind that every host will run this file,
so if you're using multiple hosts, divide the total TPS you want by the number of hosts to get the TPS value you should
include in the test.


Next, follow the [piledriver][pl] instructions, or use this as the TL;DR:

```bash
$ cd load-tests/services/revenue/subscriptions
$ make init NUM_HOSTS = 5
$ make plan
$ make apply
$ make list_hosts
$ make set_file_descriptors
$ make prep                     # from here and above only need to be done once
$ make start LOADTEST_FILES="<test-name>.toml <data-file>.toml <data-file>.toml"
$ make check_status
$ make stop                     # optional - if needed
$ make get_results OUTPUT_DIR=results/<YYYY-MM>/<test-name>
$ make publish_results OUTPUT_DIR=results/<YYYY-MM>/<test-name>
$ make destroy                  # don't forget to clean up the resources when you're done.
``` 


For quick reference, here are the tests that are implemented:
```bash
$ make start LOADTEST_FILES="GetUserChannelSubscription_burst.toml user_ids_300k.toml channel_ids_60k.toml"
$ make start LOADTEST_FILES="GetUserChannelSubscription_ramp.toml user_ids_300k.toml channel_ids_60k.toml"
$ make start LOADTEST_FILES="GetUserProductSubscriptions_burst.toml user_ids_300k.toml ticket_product_ids_10k.toml"
$ make start LOADTEST_FILES="GetUserProductSubscriptions_ramp.toml user_ids_300k.toml ticket_product_ids_10k.toml"
$ make start LOADTEST_FILES="IsEligible_burst.toml user_ids_300k.toml channel_ids_50k.toml"
$ make start LOADTEST_FILES="IsEligible_ramp.toml user_ids_300k.toml channel_ids_50k.toml"
```


Make sure to commit all result files to this repo.

### Results

#### February 2021

| Endpoint                         | Peak RPS Tested | Type                                   | Success Rate |
|----------------------------------|-----------------|----------------------------------------|--------------|
| GetUserChannelSubscription       | 55k             | Burst                                  | 99.98%       |
| GetUserChannelSubscription       | 55K             | Ramp                                   | 100%         |
| GetUserProductSubscriptions      | 43K             | Burst                                  | 99.76%       |
| GetUserProductSubscriptions      | 43K             | Ramp                                   | 99.98%       |
| IsEligible                       | 10.45k          | Burst                                  | 99.98%       |
| IsEligible                       | 10.45k          | Ramp                                   | 99.98%       |



{{% children showhidden="true" %}}
