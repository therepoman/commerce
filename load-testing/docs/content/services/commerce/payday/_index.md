---
title: "Payday"
tags: ["moments"]
---


### Links

- [Code](https://git.xarth.tv/commerce/payday)
- [Runbook](https://git.xarth.tv/commerce/bits-oncall)



###  Running

```bash
make start LOADTEST_FILES="payday.toml channel_ids_60k.toml user_ids_50k.toml"
```

### Results

{{% children showhidden="true" %}}
