---
title: "Results: 2020-04"
date: 2020-05-02T10:47:05-07:00
hidden: true
---



The Payday Load test targeted four apis: GetBalance, GetGlobalCheers, GetChannelCheers and GetChannelnfo. 
GetBalance was load tested at 4787 TPS, GetGlobalCheers at 2847 TPS, GetChannelCheers at 2334 TPS and GetChannelInfo at 21067 TPS. 
The apis were all load test simultaneously to simulate 2x load on the apis together, with the load test taking 30 minutes to ramp up traffic and then maintaining that traffic load for an additional 30 minutes.


## Latency and Error Rates
Latency and error rates were unaffected by the load test.

!["GetChannelInfo latency and errors"](../../../../images/commerce/payday/2020-04/get-channel-info.png)
!["GetBalance latency and errors"](../../../../images/commerce/payday/2020-04/get-balance.png)
!["GetGlobalCheers latency and errors"](../../../../images/commerce/payday/2020-04/get-global-cheers.png)
!["GetChannelCheers latency and errors"](../../../../images/commerce/payday/2020-04/get-channel-cheers.png)



## CPU
CPU did increase from ~16 percent to topping out at 23 percent, but still not a problem.

!["ECS Cluster CPU Utilization"](../../../../images/commerce/payday/2020-04/ecs-cluster-cpu.png)

## Redis
Redis connections were unaffected from the load test and though CPU did increase it was still to an acceptable degree.

!["Redis cluster CPU Utilization"](../../../../images/commerce/payday/2020-04/redis-cpu.png)
!["Redis cluster Connections"](../../../../images/commerce/payday/2020-04/redis-connections.png)

## Conclusion
Payday does not require any additional operational work for handle 2x its load at this time. 
