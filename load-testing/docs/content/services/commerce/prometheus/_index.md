---
title: "Prometheus"
tags: ["moments", "bits"]
---


### Links

- [Code](https://git.xarth.tv/commerce/prometheus)
- [Runbook](https://git.xarth.tv/commerce/bits-oncall)


###  Running

```
make start LOADTEST_FILES="search_ramp.toml user_ids_10k.toml"
```

### Results

{{% children showhidden="true" %}}
