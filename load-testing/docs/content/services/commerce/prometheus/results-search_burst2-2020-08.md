---
title: "Results Search_ramp2 2020 08"
date: 2020-08-10T18:23:42-07:00
hidden: true
---


### Host 10.204.2.206

```
Requests      [total, rate]            100305, 111.88
Duration      [total, attack, wait]    14m56.561351345s, 14m56.551250178s, 10.101167ms
Latencies     [mean, 50, 95, 99, max]  11.712995ms, 10.659093ms, 17.288424ms, 32.810375ms, 1.042919821s
Bytes In      [total, mean]            185403791, 1848.40
Bytes Out     [total, mean]            30191805, 301.00
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:100305  
Error Set:
```
