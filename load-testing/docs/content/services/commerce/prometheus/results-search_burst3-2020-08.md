---
title: "Results Search_burst3 2020 08"
date: 2020-08-10T19:08:47-07:00
hidden: true
---


### Host 10.204.2.206

```
Requests      [total, rate]            422829, 201.46
Duration      [total, attack, wait]    34m58.821972594s, 34m58.811887016s, 10.085578ms
Latencies     [mean, 50, 95, 99, max]  12.236599ms, 10.522864ms, 20.085337ms, 35.704214ms, 6.650752322s
Bytes In      [total, mean]            780477695, 1845.85
Bytes Out     [total, mean]            127271529, 301.00
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:422829  
Error Set:
```

