---
title: "Results Search_ramp 2020 08"
date: 2020-08-10T17:02:53-07:00
hidden: true
---


### Host 10.204.2.245

```
Requests      [total, rate]            111132, 20.64
Duration      [total, attack, wait]    1h29m44.401810238s, 1h29m44.388620061s, 13.190177ms
Latencies     [mean, 50, 95, 99, max]  12.547928ms, 11.566896ms, 17.047457ms, 33.900639ms, 447.920193ms
Bytes In      [total, mean]            205853053, 1852.33
Bytes Out     [total, mean]            33450732, 301.00
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:111132  
Error Set:
```

