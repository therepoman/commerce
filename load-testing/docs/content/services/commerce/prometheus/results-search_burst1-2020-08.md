---
title: "Results Search_burst1 2020 08"
date: 2020-08-10T17:23:24-07:00
hidden: true
---


### Host 10.204.2.245

```
Requests      [total, rate]            92817, 103.57
Duration      [total, attack, wait]    14m56.187103355s, 14m56.177038953s, 10.064402ms
Latencies     [mean, 50, 95, 99, max]  11.613156ms, 10.596834ms, 16.864766ms, 32.695213ms, 177.379654ms
Bytes In      [total, mean]            171169869, 1844.17
Bytes Out     [total, mean]            27937917, 301.00
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:92817  
Error Set:
```

