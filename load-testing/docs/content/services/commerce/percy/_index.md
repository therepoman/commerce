---
title: "Percy"
tags: ["moments", "hype train"]
---


### Links

- [Code](https://git.xarth.tv/commerce/percy)
- [Runbook](https://git.xarth.tv/commerce/bits-oncall)



###  Running

```bash
make start LOADTEST_FILES="get-creator-anniversaries.toml creator-anniversaries-data.toml"
```

### Results

{{% children showhidden="true" %}}
