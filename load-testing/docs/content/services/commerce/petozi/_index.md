---
title: "Petozi"
tags: ["moments"]
---


### Links

- [Code](https://git.xarth.tv/commerce/petozi)
- [Runbook](https://git.xarth.tv/commerce/bits-oncall)


###  Running the tests

```bash
make start LOADTEST_FILES="petozi.toml"
```

### Results

{{% children showhidden="true" %}}
