---
title: "Results: 2020-04"
date: 2020-05-02T10:47:05-07:00
hidden: true
---

The Petozi load was run at 1113 TPS to simulate a 2x increase in traffic with Petozi running at average load. We took 30 minutes to ramp up from 0 to 1113 TPS and then maintained that traffic for an additional 30 minutes.

## Latency
Petozi did see an increase in latency, but the latency was negligible before and is still negligible so this is not a concern.

| Metric     | 2x Traffic |
| ---------- | ---------- |
| p50        | 4.194211ms |
| p95        | 5.782242ms |
| p99        | 6.430108ms |
| max        | 134.35953ms|


!["GetBitsProducts latency"](../../../../images/commerce/petozi/2020-04/get-bits-products-latency.png)

## Errors
Petozi had no errors as a result of the load test.

!["Logscan error metrics"](../../../../images/commerce/petozi/2020-04/logscan-errors.png)
!["GetBitsProducts status codes"](../../../../images/commerce/petozi/2020-04/get-bits-products-status-codes.png)


## CPU
Petozi’s CPU Utilization increased from 2% to 10% at 10sx traffic, but was still well below any threshold that would cause concern.

!["ECS CPU Utilization"](../../../../images/commerce/petozi/2020-04/ecs-cpu.png)


## Redis
Redis was unaffected by the load test.

!["Redis Latency"](../../../../images/commerce/petozi/2020-04/redis-latency.png)
!["Redis CPU Utilization and connections"](../../../../images/commerce/petozi/2020-04/redis-cpu-connections.png)


## Conclusion
Petozi can handle 2x load with absolutely no problems and require no additional operation work.
