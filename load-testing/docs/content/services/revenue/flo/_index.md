---
title: "Flo"
tags: ["money"]
---


### Links

- [Code](https://git.xarth.tv/revenue/flo)
- [Runbook](https://git.xarth.tv/pages/subs/docs/docs/operations/subs.html)
- [Dashboard](https://grafana.xarth.tv/d/TzcWOrKmz/flo)


### Traffic summary (2020-06-15):

| API | Prod | Peak | Spike |
|-----|------|------|-------|
|GetPreviewPrices| 7k | 18k | 11k |
|GetCheckoutPrices| 2 | 4 | 2 |
|GetCheckoutPriceForPaymentProvider| 20 | 33 | 11 |
|GetTaxBreakdown| 2 | 4 | 2 |
|GetChargeModelsByIDs| 25 | 53 | 28 | 

### Running

#### Setup
```bash
export AWS_PROFILE=twitch-revenue-aws
export USERNAME=$(whoami)

cd load-tests/services/revenue/flo
make init NUM_HOSTS=1
make plan
make apply
make list_hosts
make set_file_descriptors
make prep
```

```bash
#TEMPLATE FOR RUNNING TEST
make start LOADTEST_FILES="<test-name>.toml <data-file>.toml <data-file>.toml"
make check_status
make stop                     # optional - if needed
make get_results OUTPUT_DIR=results/<YYYY-MM>/<test-name>
make publish_results OUTPUT_DIR=results/<YYYY-MM>/<test-name>
```

#### Test for GetCheckoutPrice
```bash
make start LOADTEST_FILES="GetCheckoutPrices_burst1.toml pricing_ids.toml user_ids_300k.toml"
make get_results OUTPUT_DIR=results/2020-06/GetCheckoutPrices_burst1
```

#### Test for GetTaxBreakdown
```bash
make start LOADTEST_FILES="GetTaxBreakdown_burst1.toml"
make get_results OUTPUT_DIR=results/2020-06/GetTaxBreakdown_burst1
```

#### Tests for GetChargeModelsByIDS
```bash
make start LOADTEST_FILES="GetChargeModelsByIDs_ramp.toml charge_model_ids.toml"
make get_results OUTPUT_DIR=results/2020-06/GetChargeModelsByIDs_ramp
make start LOADTEST_FILES="GetChargeModelsByIDs_burst1.toml charge_model_ids.toml"
make get_results OUTPUT_DIR=results/2020-06/GetChargeModelsByIDs_burst1
make start LOADTEST_FILES="GetChargeModelsByIDs_burst2.toml charge_model_ids.toml"
make get_results OUTPUT_DIR=results/2020-06/GetChargeModelsByIDs_burst2
make start LOADTEST_FILES="GetChargeModelsByIDs_burst3.toml charge_model_ids.toml"
make get_results OUTPUT_DIR=results/2020-06/GetChargeModelsByIDs_burst3
```

#### Tests for GetChargeModelsByIDS
```bash
make start LOADTEST_FILES="GetChargeModelsByIDs_ramp.toml charge_model_ids.toml"
make get_results OUTPUT_DIR=results/2020-06/GetChargeModelsByIDs_ramp
make start LOADTEST_FILES="GetChargeModelsByIDs_burst1.toml charge_model_ids.toml"
make get_results OUTPUT_DIR=results/2020-06/GetChargeModelsByIDs_burst1
make start LOADTEST_FILES="GetChargeModelsByIDs_burst2.toml charge_model_ids.toml"
make get_results OUTPUT_DIR=results/2020-06/GetChargeModelsByIDs_burst2
make start LOADTEST_FILES="GetChargeModelsByIDs_burst3.toml charge_model_ids.toml"
make get_results OUTPUT_DIR=results/2020-06/GetChargeModelsByIDs_burst3
```

#### Tests for GetCheckoutPriceForPaymentProvider
```bash
make start LOADTEST_FILES="GetCheckoutPriceForPaymentProvider_ramp.toml pricing_ids.toml buyers_70k.toml country_codes.toml"
make get_results OUTPUT_DIR=results/2020-06/GetCheckoutPriceForPaymentProvider_ramp
make start LOADTEST_FILES="GetCheckoutPriceForPaymentProvider_burst1.toml pricing_ids.toml buyers_70k.toml country_codes.toml"
make get_results OUTPUT_DIR=results/2020-06/GetCheckoutPriceForPaymentProvider_burst1
make start LOADTEST_FILES="GetCheckoutPriceForPaymentProvider_burst2.toml pricing_ids.toml buyers_70k.toml country_codes.toml"
make get_results OUTPUT_DIR=results/2020-06/GetCheckoutPriceForPaymentProvider_burst2
make start LOADTEST_FILES="GetCheckoutPriceForPaymentProvider_burst3.toml pricing_ids.toml buyers_70k.toml country_codes.toml"
make get_results OUTPUT_DIR=results/2020-06/GetCheckoutPriceForPaymentProvider_burst3
```

#### Tests for GetPreviewPrice

we need to make a change for this API since it has high traffic.
The test plan is accounted traffic for 20 client machines.
```bash
# Re-run terraform with bigger fleets
make init NUM_HOSTS=20
make plan
make apply
make list_hosts
make set_file_descriptors
make prep
```
```bash
make start LOADTEST_FILES="GetPreviewPrices_ramp.toml pricing_ids.toml user_ids_300k.toml country_codes.toml"
make get_results OUTPUT_DIR=results/2020-06/GetPreviewPrices_ramp
make start LOADTEST_FILES="GetPreviewPrices_burst1.toml pricing_ids.toml user_ids_300k.toml country_codes.toml"
make get_results OUTPUT_DIR=results/2020-06/GetPreviewPrices_burst1
make start LOADTEST_FILES="GetPreviewPrices_burst2.toml pricing_ids.toml user_ids_300k.toml country_codes.toml"
make get_results OUTPUT_DIR=results/2020-06/GetPreviewPrices_burst2
make start LOADTEST_FILES="GetPreviewPrices_burst3.toml pricing_ids.toml user_ids_300k.toml country_codes.toml"
make get_results OUTPUT_DIR=results/2020-06/GetPreviewPrices_burst3
```

#### Publish Result
```bash
export DATE="2020-06"
make publish_results RESULTS_DIR=results/$DATE/GetCheckoutPrices_burst1 OUTPUT_DIR=results/$DATE/GetCheckoutPrices_burst1
make publish_results RESULTS_DIR=results/$DATE/GetTaxBreakdown_burst1 OUTPUT_DIR=results/$DATE/GetTaxBreakdown_burst1
make publish_results RESULTS_DIR=results/$DATE/GetChargeModelsByIDs_ramp OUTPUT_DIR=results/$DATE/GetChargeModelsByIDs_ramp
make publish_results RESULTS_DIR=results/$DATE/GetChargeModelsByIDs_burst1 OUTPUT_DIR=results/$DATE/GetChargeModelsByIDs_burst1
make publish_results RESULTS_DIR=results/$DATE/GetChargeModelsByIDs_burst2 OUTPUT_DIR=results/$DATE/GetChargeModelsByIDs_burst2
make publish_results RESULTS_DIR=results/$DATE/GetChargeModelsByIDs_burst3 OUTPUT_DIR=results/$DATE/GetChargeModelsByIDs_burst3
make publish_results RESULTS_DIR=results/$DATE/GetCheckoutPriceForPaymentProvider_ramp OUTPUT_DIR=results/$DATE/GetCheckoutPriceForPaymentProvider_ramp
make publish_results RESULTS_DIR=results/$DATE/GetCheckoutPriceForPaymentProvider_burst1 OUTPUT_DIR=results/$DATE/GetCheckoutPriceForPaymentProvider_burst1
make publish_results RESULTS_DIR=results/$DATE/GetCheckoutPriceForPaymentProvider_burst1 OUTPUT_DIR=results/$DATE/GetCheckoutPriceForPaymentProvider_burst2
make publish_results RESULTS_DIR=results/$DATE/GetCheckoutPriceForPaymentProvider_burst3 OUTPUT_DIR=results/$DATE/GetCheckoutPriceForPaymentProvider_burst3
make publish_results RESULTS_DIR=results/$DATE/GetPreviewPrices_ramp OUTPUT_DIR=results/$DATE/GetPreviewPrices_ramp
make publish_results RESULTS_DIR=results/$DATE/GetPreviewPrices_burst1 OUTPUT_DIR=results/$DATE/GetPreviewPrices_burst1
make publish_results RESULTS_DIR=results/$DATE/GetPreviewPrices_burst2 OUTPUT_DIR=results/$DATE/GetPreviewPrices_burst2
make publish_results RESULTS_DIR=results/$DATE/GetPreviewPrices_burst3 OUTPUT_DIR=results/$DATE/GetPreviewPrices_burst3
```

### Results

We are running load test against live production traffic, so plan load + curre

{{% children showhidden="true" %}}
