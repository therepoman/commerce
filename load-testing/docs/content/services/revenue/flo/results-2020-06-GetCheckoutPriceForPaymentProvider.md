---
title: "Results 2020 06 GetCheckoutPriceForPaymentProvider"
date: 2020-06-24T20:11:16-07:00
hidden: true
---

Current traffic summary

| Prod | Peak | Spike |
|-----|------|------|
| 20 | 33 | 11 |

## Test parameters

| Type | Plan |
|-----|------|
| Ramp | 40 |
| Burst1 | 29 |
| Burst2 | 33 |
| Burst3 | 37 |

## Dependencies
- Payments RDS
- Sakura service

## Conclusion
We do not observe any increased latency or error rate during all load tests.  
Flo GetCheckoutPriceForPaymentProvider can handle 2x the traffic that it normally gets without any additional work.


### Burst 1

---

#### Request Rate
[!["RequestRate"](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_burst1/RequestRate.png)](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_burst1/RequestRate.png)

#### Latency
[!["Latency"](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_burst1/Latency.png)](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_burst1/Latency.png)

#### Status Code
[!["StatusCode"](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_burst1/StatusCode.png)](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_burst1/StatusCode.png)

### Burst 2

---

#### Request Rate
[!["RequestRate"](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_burst2/RequestRate.png)](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_burst2/RequestRate.png)

#### Latency
[!["Latency"](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_burst2/Latency.png)](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_burst2/Latency.png)

#### Status Code
[!["StatusCode"](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_burst2/StatusCode.png)](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_burst2/StatusCode.png)

### Burst 3

---

#### Request Rate
[!["RequestRate"](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_burst3/RequestRate.png)](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_burst3/RequestRate.png)

#### Latency
[!["Latency"](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_burst3/Latency.png)](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_burst3/Latency.png)

#### Status Code
[!["StatusCode"](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_burst3/StatusCode.png)](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_burst3/StatusCode.png)

### Ramp

---

#### Request Rate

[!["RequestRate"](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_ramp/RequestRate.png)](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_ramp/RequestRate.png)

#### Latency
[!["Latency"](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_ramp/Latency.png)](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_ramp/Latency.png)

#### Status Code
[!["StatusCode"](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_ramp/StatusCode.png)](../../../../images/revenue/flo/2020-06/GetCheckoutPriceForPaymentProvider_ramp/StatusCode.png)


## Raw Results

### Ramp

```
Requests      [total, rate]            143352, 26.57
Duration      [total, attack, wait]    1h29m54.867379443s, 1h29m54.819669437s, 47.710006ms
Latencies     [mean, 50, 95, 99, max]  36.500862ms, 32.17881ms, 53.886421ms, 189.816872ms, 826.491743ms
Bytes In      [total, mean]            41513862, 289.59
Bytes Out     [total, mean]            24759834, 172.72
Success       [ratio]                  99.74%
Status Codes  [code:count]             200:142983  412:154  500:215  
Error Set:
500 Internal Server Error
412 Precondition Failed
```

### Burst 1

```
Requests      [total, rate]            21711, 24.65
Duration      [total, attack, wait]    14m40.833092768s, 14m40.832281748s, 811.02µs
Latencies     [mean, 50, 95, 99, max]  1.156285ms, 1.174307ms, 1.464692ms, 1.624469ms, 22.287817ms
Bytes In      [total, mean]            1432926, 66.00
Bytes Out     [total, mean]            3765330, 173.43
Success       [ratio]                  0.00%
Status Codes  [code:count]             400:21711  
Error Set:
400 Bad Request
```

### Burst 3

```
Requests      [total, rate]            27702, 31.07
Duration      [total, attack, wait]    14m51.596083439s, 14m51.573776108s, 22.307331ms
Latencies     [mean, 50, 95, 99, max]  36.746553ms, 32.56537ms, 55.42377ms, 185.028511ms, 793.272101ms
Bytes In      [total, mean]            8020706, 289.54
Bytes Out     [total, mean]            4784760, 172.72
Success       [ratio]                  99.74%
Status Codes  [code:count]             200:27629  412:30  500:43  
Error Set:
500 Internal Server Error
412 Precondition Failed
```
