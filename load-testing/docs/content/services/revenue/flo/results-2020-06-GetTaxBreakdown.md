---
title: "Results 2020 06 GetTaxBreakdown"
date: 2020-06-24T20:11:15-07:00
hidden: true
---


Current traffic summary

| Prod | Peak | Spike |
|-----|------|------|
| 2 | 4 | 2 |

## Test parameters

| Type | Plan |
|-----|------|
| Burst | 8 |

## Dependencies
- Redis
- Vertex Tax server

## Conclusion
We do not observe any increased latency or error rate during all load tests.  
Flo GetTaxBreakdown can handle 4x the traffic that it normally gets without any additional work.

## Results

#### Request Rate
[!["RequestRate"](../../../../images/revenue/flo/2020-06/GetTaxBreakdown_burst1/RequestRate.png)](../../../../images/revenue/flo/2020-06/GetTaxBreakdown_burst1/RequestRate.png)

#### Latency
[!["Latency"](../../../../images/revenue/flo/2020-06/GetTaxBreakdown_burst1/Latency.png)](../../../../images/revenue/flo/2020-06/GetTaxBreakdown_burst1/Latency.png)

#### Status Code
[!["StatusCode"](../../../../images/revenue/flo/2020-06/GetTaxBreakdown_burst1/StatusCode.png)](../../../../images/revenue/flo/2020-06/GetTaxBreakdown_burst1/StatusCode.png)

## Raw Results

### Host 10.201.38.144

```
Requests      [total, rate]            6009, 7.08
Duration      [total, attack, wait]    14m9.001882834s, 14m8.998916549s, 2.966285ms
Latencies     [mean, 50, 95, 99, max]  3.966065ms, 2.602401ms, 14.242092ms, 16.866637ms, 100.733762ms
Bytes In      [total, mean]            482163, 80.24
Bytes Out     [total, mean]            1516268, 252.33
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:6009  
Error Set:
```

