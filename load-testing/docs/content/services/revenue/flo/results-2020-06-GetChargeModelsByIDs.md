---
title: "Results 2020 06 GetChargeModelsByIDs"
date: 2020-06-24T20:14:24-07:00
hidden: true
---

Current traffic summary

| Prod | Peak | Spike |
|-----|------|------|
| 25 | 53 | 28 |

## Test parameters

| Type | Plan |
|-----|------|
| Ramp | 50 |
| Burst1 | 46 |
| Burst2 | 53 |
| Burst3 | 67 |

## Dependencies
- DynamoDB

## Conclusion
We do not observe any increased latency or error rate during all load tests.  
Flo GetChargeModelsByIDs can handle 2x the traffic that it normally gets without any additional work.

## Results

### Burst 1

---

#### Request Rate
[!["RequestRate"](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_burst1/RequestRate.png)](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_burst1/RequestRate.png)

#### Latency
[!["Latency"](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_burst1/Latency.png)](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_burst1/Latency.png)

#### Status Code
[!["StatusCode"](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_burst1/StatusCode.png)](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_burst1/StatusCode.png)

### Burst 2

---

#### Request Rate
[!["RequestRate"](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_burst2/RequestRate.png)](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_burst2/RequestRate.png)

#### Latency
[!["Latency"](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_burst2/Latency.png)](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_burst2/Latency.png)

#### Status Code
[!["StatusCode"](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_burst2/StatusCode.png)](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_burst2/StatusCode.png)

### Burst 3

---

#### Request Rate
[!["RequestRate"](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_burst3/RequestRate.png)](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_burst3/RequestRate.png)

#### Latency
[!["Latency"](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_burst3/Latency.png)](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_burst3/Latency.png)

#### Status Code
[!["StatusCode"](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_burst3/StatusCode.png)](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_burst3/StatusCode.png)

### Ramp

---

#### Request Rate
[!["RequestRate"](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_ramp/RequestRate.png)](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_ramp/RequestRate.png)

#### Latency
[!["Latency"](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_ramp/Latency.png)](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_ramp/Latency.png)

#### Status Code
[!["StatusCode"](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_ramp/StatusCode.png)](../../../../images/revenue/flo/2020-06/GetChargeModelsByIDs_ramp/StatusCode.png)

## Raw Results

### Ramp

```
Requests      [total, rate]            180036, 33.41
Duration      [total, attack, wait]    1h29m48.436955254s, 1h29m48.435078976s, 1.876278ms
Latencies     [mean, 50, 95, 99, max]  1.995724ms, 2.03431ms, 2.788093ms, 3.380434ms, 75.359671ms
Bytes In      [total, mean]            3600720, 20.00
Bytes Out     [total, mean]            10082016, 56.00
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:180036  
Error Set:
```

### Burst 1

```
Requests      [total, rate]            34440, 38.83
Duration      [total, attack, wait]    14m47.056561125s, 14m47.054071012s, 2.490113ms
Latencies     [mean, 50, 95, 99, max]  2.186083ms, 2.235801ms, 3.045811ms, 3.504664ms, 29.058625ms
Bytes In      [total, mean]            769394, 22.34
Bytes Out     [total, mean]            1928640, 56.00
Success       [ratio]                  98.02%
Status Codes  [code:count]             200:33757  502:683  
Error Set:
502 Bad Gateway
```


### Burst 2

```
Requests      [total, rate]            39675, 44.64
Duration      [total, attack, wait]    14m48.722477008s, 14m48.720700411s, 1.776597ms
Latencies     [mean, 50, 95, 99, max]  1.882103ms, 1.828185ms, 2.662874ms, 3.244016ms, 29.788054ms
Bytes In      [total, mean]            793500, 20.00
Bytes Out     [total, mean]            2221800, 56.00
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:39675  
Error Set:
```

### Burst 3

```
Requests      [total, rate]            50154, 56.32
Duration      [total, attack, wait]    14m50.460546342s, 14m50.458266806s, 2.279536ms
Latencies     [mean, 50, 95, 99, max]  1.996908ms, 2.03676ms, 2.784628ms, 3.428005ms, 43.730333ms
Bytes In      [total, mean]            1003080, 20.00
Bytes Out     [total, mean]            2808624, 56.00
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:50154  
Error Set:
```


