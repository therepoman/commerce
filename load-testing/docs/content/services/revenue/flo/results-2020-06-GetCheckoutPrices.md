---
title: "Results 2020 06 GetCheckoutPrices"
date: 2020-06-24T20:12:11-07:00
hidden: true
---

Current traffic summary

| Prod | Peak | Spike |
|-----|------|------|
| 2 | 4 | 2 |

## Test parameters

| Type | Plan |
|-----|------|
| Burst | 8 |

## Dependencies
- DynamoDB
- Redis
- Vertex Tax server

## Conclusion
We do not observe any increased latency or error rate during all load tests.  
Flo GetCheckoutPrices can handle 4x the traffic that it normally gets without any additional work.


## Results

#### Request Rate
[!["RequestRate"](../../../../images/revenue/flo/2020-06/GetCheckoutPrices_burst1/RequestRate.png)](../../../../images/revenue/flo/2020-06/GetCheckoutPrices_burst1/RequestRate.png)

#### Latency
[!["Latency"](../../../../images/revenue/flo/2020-06/GetCheckoutPrices_burst1/Latency.png)](../../../../images/revenue/flo/2020-06/GetCheckoutPrices_burst1/Latency.png)

#### Status Code
[!["StatusCode"](../../../../images/revenue/flo/2020-06/GetCheckoutPrices_burst1/StatusCode.png)](../../../../images/revenue/flo/2020-06/GetCheckoutPrices_burst1/StatusCode.png)


## Raw Results

### Host 10.201.38.144

```
Requests      [total, rate]            6009, 7.07
Duration      [total, attack, wait]    14m9.544910015s, 14m9.542408668s, 2.501347ms
Latencies     [mean, 50, 95, 99, max]  5.147527ms, 2.077759ms, 16.297783ms, 21.840333ms, 227.97388ms
Bytes In      [total, mean]            1721661, 286.51
Bytes Out     [total, mean]            1516751, 252.41
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:6009  
Error Set:
```

