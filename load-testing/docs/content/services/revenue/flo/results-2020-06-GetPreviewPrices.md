---
title: "Results 2020 06 GetPreviewPrices"
date: 2020-06-24T20:11:16-07:00
hidden: true
---

Current traffic summary

| Prod | Peak | Spike |
|-----|------|------|
| 7k | 18k | 11k |

## Test parameters

| Type | Plan |
|-----|------|
| Ramp | 14000 |
| Burst1 | 15240 |
| Burst2 | 18000 |
| Burst3 | 23500 |

## Dependencies
- Payments RDS
- Redis
- DynamoDB

## Conclusion
Flo GetPreviewPrices can handle traffic around 1.9x of current traffic.  
Additional investigation and redesign/optimization needed to make this API achieve 2x of the current traffic.

## Results

With ramp test, we reach Flo GetPreviewPrices capacity.
There is no need to do other test types since they have higher rate than ramp test.
At around 13.3k rps (1.9x normal traffic), we start to see some 5xx error due to slow database query
and high number of database connection.


### Request Rate
[!["RequestRate"](../../../../images/revenue/flo/2020-06/GetPreviewPrices_ramp/RequestRate.png)](../../../../images/revenue/flo/2020-06/GetPreviewPrices_ramp/RequestRate.png)

### Latency
Latency increase after database struggle
[!["Latency"](../../../../images/revenue/flo/2020-06/GetPreviewPrices_ramp/Latency.png)](../../../../images/revenue/flo/2020-06/GetPreviewPrices_ramp/Latency.png)

### Status Code
[!["StatusCode"](../../../../images/revenue/flo/2020-06/GetPreviewPrices_ramp/StatusCode.png)](../../../../images/revenue/flo/2020-06/GetPreviewPrices_ramp/StatusCode.png)

### Availability
Availability drops from error
[!["Availability"](../../../../images/revenue/flo/2020-06/GetPreviewPrices_ramp/Availability.png)](../../../../images/revenue/flo/2020-06/GetPreviewPrices_ramp/Availability.png)

### Database Connection
Database connection spike during test.
[!["DBConnection"](../../../../images/revenue/flo/2020-06/GetPreviewPrices_ramp/DBConnection.png)](../../../../images/revenue/flo/2020-06/GetPreviewPrices_ramp/DBConnection.png)

### Redis Connection
[!["RedisConnection"](../../../../images/revenue/flo/2020-06/GetPreviewPrices_ramp/RedisConnection.png)](../../../../images/revenue/flo/2020-06/GetPreviewPrices_ramp/RedisConnection.png)


### Host 10.201.38.144

```
Abort
```

