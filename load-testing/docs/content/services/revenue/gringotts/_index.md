---
title: "Gringotts"
tags: ["money"]
---


### Links

- [Catalog](https://catalog.xarth.tv/services/326/details)
- [Pipelines](https://pipelines.amazon.com/pipelines/TwitchGringotts)
- [Dashboard](https://grafana.xarth.tv/d/hG4TojtMz/gringotts)


### Traffic summary (2021-09-01):

| API | Prod | Peak | Spike |
|-----|------|------|-------|
|GetRevenue| 8 | 25 | 17 |

### Running

#### Setup
Gringotts load-test is running using [Fulton load testing framework](https://docs.fulton.twitch.a2z.com/docs/load_testing.html).
The load testing Infrastructure is setup using CDK. Instruction for using a load testing tool is in Fulton document.
For production, Fulton load test tool will have access to `twitchgringotts-infra-pr-fargateworkerloadtestreq-4cgfnpfkmypn` S3 bucket.

#### Test for GetRevenue
```bash
#RAMP testing
twitch-load-testing new-test --request-provider-url s3://twitchgringotts-infra-pr-fargateworkerloadtestreq-4cgfnpfkmypn/gringottsLoadTest.sh --series-file ramp.json --lambda-name TwitchLoadTesting-Fargate-prod-us-west-2 --segment-grace-period -1s --worker-env "STAGE=prod"
#BUST1 testing
twitch-load-testing new-test --request-provider-url s3://twitchgringotts-infra-pr-fargateworkerloadtestreq-4cgfnpfkmypn/gringottsLoadTest.sh --series-file burst1.json --lambda-name TwitchLoadTesting-Fargate-prod-us-west-2 --segment-grace-period -1s --worker-env "STAGE=prod"
#BURST2 testing
twitch-load-testing new-test --request-provider-url s3://twitchgringotts-infra-pr-fargateworkerloadtestreq-4cgfnpfkmypn/gringottsLoadTest.sh --series-file burst2.json --lambda-name TwitchLoadTesting-Fargate-prod-us-west-2 --segment-grace-period -1s --worker-env "STAGE=prod"
#BURST3 testing
twitch-load-testing new-test --request-provider-url s3://twitchgringotts-infra-pr-fargateworkerloadtestreq-4cgfnpfkmypn/gringottsLoadTest.sh --series-file burst3.json --lambda-name TwitchLoadTesting-Fargate-prod-us-west-2 --segment-grace-period -1s --worker-env "STAGE=prod"
```

### Results

Fulton load testing tool will generate result by using cloudwatch.

{{% children showhidden="true" %}}
