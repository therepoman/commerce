---
title: "Results 2020-09-01 GetRevenue"
date: 2021-09-02T14:27:52-07:00
hidden: true
---

Current traffic summary

| Prod | Peak | Spike |
|-----|------|------|
| 8 | 25 | 17 |

## Test parameters

| Type | Plan |
|-----|------|
| Ramp | 16 |
| Burst1 | 21 |
| Burst2 | 25 |
| Burst3 | 34 |

## Dependencies
- DynamoDB
- Moneypenny service

## Conclusion
Gringotts GetRevenue can handle traffic around 4.5x of current traffic.

## Results
### Ramp
tiny 5xx error near the end of the test is due to moneypenny error.

#### Request Rate
[!["RequestRate"](../../../../images/revenue/gringotts/2021-09/ramp_request_rate.png)](../../../../images/revenue/gringotts/2021-09/ramp_request_rate.png)

#### P99 Latency
[!["Latency"](../../../../images/revenue/gringotts/2021-09/ramp_latency_p99.png)](../../../../images/revenue/gringotts/2021-09/ramp_latency_p99.png)

#### P90 Latency
[!["Latency"](../../../../images/revenue/gringotts/2021-09/ramp_latency_p90.png)](../../../../images/revenue/gringotts/2021-09/ramp_latency_p90.png)

### Burst1

#### Request Rate
[!["RequestRate"](../../../../images/revenue/gringotts/2021-09/burst1_request_rate.png)](../../../../images/revenue/gringotts/2021-09/burst1_request_rate.png)

#### P99 Latency
[!["Latency"](../../../../images/revenue/gringotts/2021-09/burst1_latency_p99.png)](../../../../images/revenue/gringotts/2021-09/burst1_latency_p99.png)

#### P90 Latency
[!["Latency"](../../../../images/revenue/gringotts/2021-09/burst1_latency_p90.png)](../../../../images/revenue/gringotts/2021-09/burst1_latency_p90.png)

### Burst2

#### Request Rate
[!["RequestRate"](../../../../images/revenue/gringotts/2021-09/burst2_request_rate.png)](../../../../images/revenue/gringotts/2021-09/burst2_request_rate.png)

#### P99 Latency
[!["Latency"](../../../../images/revenue/gringotts/2021-09/burst2_latency_p99.png)](../../../../images/revenue/gringotts/2021-09/burst2_latency_p99.png)

#### P90 Latency
[!["Latency"](../../../../images/revenue/gringotts/2021-09/burst2_latency_p90.png)](../../../../images/revenue/gringotts/2021-09/burst2_latency_p90.png)

### Burst3

#### Request Rate
[!["RequestRate"](../../../../images/revenue/gringotts/2021-09/burst3_request_rate.png)](../../../../images/revenue/gringotts/2021-09/burst3_request_rate.png)

#### P99 Latency
[!["Latency"](../../../../images/revenue/gringotts/2021-09/burst3_latency_p99.png)](../../../../images/revenue/gringotts/2021-09/burst3_latency_p99.png)

#### P90 Latency
[!["Latency"](../../../../images/revenue/gringotts/2021-09/burst3_latency_p90.png)](../../../../images/revenue/gringotts/2021-09/burst3_latency_p90.png)
