---
title: "Results GetUserPaymentGatewaysForRegion 2020 06_burst3"
date: 2020-06-29T17:17:18-07:00
hidden: true
---


### Host 10.0.166.177

```
Requests      [total, rate]            56178, 62.80
Duration      [total, attack, wait]    14m54.513623483s, 14m54.493051376s, 20.572107ms
Latencies     [mean, 50, 95, 99, max]  16.130817ms, 15.598322ms, 19.097593ms, 26.075786ms, 1.803652403s
Bytes In      [total, mean]            4048066, 72.06
Bytes Out     [total, mean]            5383969, 95.84
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:56178  
Error Set:
```

