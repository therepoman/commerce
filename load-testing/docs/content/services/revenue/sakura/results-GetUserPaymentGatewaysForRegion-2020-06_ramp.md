---
title: "Results GetUserPaymentGatewaysForRegion 2020 06_ramp"
date: 2020-06-29T15:33:13-07:00
hidden: true
---


### Host 10.0.166.177

```
Requests      [total, rate]            107712, 20.00
Duration      [total, attack, wait]    1h29m44.75990639s, 1h29m44.742877304s, 17.029086ms
Latencies     [mean, 50, 95, 99, max]  16.295154ms, 15.770047ms, 19.494304ms, 27.26106ms, 605.369347ms
Bytes In      [total, mean]            7797879, 72.40
Bytes Out     [total, mean]            10322400, 95.83
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:107707  500:5  
Error Set:
500 Internal Server Error
```

