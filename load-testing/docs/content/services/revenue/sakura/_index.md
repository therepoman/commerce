---
title: "Sakura"
tags: ["money", "fulton"]
---


### Links

- [Code](https://code.amazon.com/packages/TwitchPaymentsSakura/trees/mainline)
- [Runbook](https://docs.google.com/document/d/1wANu9aXmJWsFBP45fUPWSdOj9fqh4C8QeXvF-s1tKH4/edit#heading=h.clwrbmym7jkn)
- [Dashboard](https://tiny.amazon.com/13pr1p12o/IsenLink)


### Results

{{% children showhidden="true" %}}

## Traffic Limits
* 06/29/2020
    * Max prod traffic: 60 total rps
    * Max spike traffic: 130 total rps for 10 minutes
    * Max increase rate: 20 rps per second

## Endpoints to Test
marked as essential in Jarvis https://jarvis.xarth.tv/availability/service/2020/w26?classification=Essential&service_id=718

1. GetUserPaymentGatewaysForRegion
    * Traffic patterns
        * prod 30 rps
	    * peak 80 rps
	    * spike 50 rps
	* Info
	    * one user id, product type, platform, and country code per request
        * no cache
        * queries dynamodb TwitchPaymentsSakura-PDX-prod-PaymentGatewayAvailabilityTable-18JIFZ2ZVIISY table
         
         ```
           curl -s -X POST -d '{"user_id": "184650462", "platform": "WEB", "product_type": "BITS", "tax_country_code": "US" }' \
           -H 'Content-Type: application/json; charset=UTF-8' \
           https://us-west-2.prod.twitchpaymentssakura.s.twitch.a2z.com/twirp/twitch.twitchpaymentssakura.TwitchPaymentsSakura/GetUserPaymentGatewaysForRegion
         ```
          
###  Running Tests
1. First set up your aws command line to point to `twitch-payments+sakura-prod-us-west-2` and export $AWS_PROFILE and $USERNAME
    ```
    export AWS_PROFILE=twitch-payments+sakura-prod-us-west-2
    export USERNAME=$(whoami)
    export TC=teleport-remote-twitch-paymentssakura-prod-us-west-2
    ```
2. Run `make init`
3. Run `make plan`
4. Run `make apply` if everything looks good
5. Run `make prep` to copy over all of mulan's load test plans and all datasets
6. Run tests
    
    To run ramp test
    ```bash
    $ make start LOADTEST_FILES="GetUserPaymentGatewaysForRegion_ramp.toml user_ids_10k.toml platforms.toml product_types.toml country_codes.toml"
    ```
    To run burst1 test
    ```bash
    $ make start LOADTEST_FILES="GetUserPaymentGatewaysForRegion_burst1.toml user_ids_10k.toml platforms.toml product_types.toml country_codes.toml"
    ```
    To run burst2 test
    ```bash
    $ make start LOADTEST_FILES="GetUserPaymentGatewaysForRegion_burst2.toml user_ids_10k.toml platforms.toml product_types.toml country_codes.toml"
    ```
    To run burst3
    ```bash
    $ make start LOADTEST_FILES="GetUserPaymentGatewaysForRegion_burst3.toml user_ids_10k.toml platforms.toml product_types.toml country_codes.toml"
    ```
7. Run `make check_status` to check if the test is still running
    a. If you need to prematurely stop the tests, run `make stop`
8. Run `make get_results OUTPUT_DIR=results/<test-name>/<YYYY-MM>`
9. Run `make publish_results RESULTS_DIR=results/<test-name>/<YYYY-MM>`
10. Run `make destroy` to clean up the instances you spun up
