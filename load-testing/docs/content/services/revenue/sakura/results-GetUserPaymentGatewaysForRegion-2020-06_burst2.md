---
title: "Results GetUserPaymentGatewaysForRegion 2020 06_burst2"
date: 2020-06-29T16:45:23-07:00
hidden: true
---


### Host 10.0.166.177

```
Requests      [total, rate]            37503, 42.08
Duration      [total, attack, wait]    14m51.209321895s, 14m51.194759184s, 14.562711ms
Latencies     [mean, 50, 95, 99, max]  15.968796ms, 15.423758ms, 19.111227ms, 26.605ms, 236.928851ms
Bytes In      [total, mean]            2733864, 72.90
Bytes Out     [total, mean]            3593697, 95.82
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:37503  
Error Set:
```

