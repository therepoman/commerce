---
title: "Results GetUserPaymentGatewaysForRegion 2020 06_burst1"
date: 2020-06-29T16:16:49-07:00
hidden: true
---


### Host 10.0.166.177

```
Requests      [total, rate]            28452, 31.96
Duration      [total, attack, wait]    14m50.328513591s, 14m50.310361397s, 18.152194ms
Latencies     [mean, 50, 95, 99, max]  16.366459ms, 15.804012ms, 19.445945ms, 26.026861ms, 4.0673836s
Bytes In      [total, mean]            2037430, 71.61
Bytes Out     [total, mean]            2726683, 95.83
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:28452  
Error Set:
```

