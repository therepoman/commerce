---
title: "Results GetPurchaseOrdersByIDs 2020 05_burst2"
date: 2020-05-20T10:45:40-07:00
hidden: true
---


### Host 10.201.38.203

```
Requests      [total, rate]            2020953, 2242.79
Duration      [total, attack, wait]    15m1.099801263s, 15m1.088480994s, 11.320269ms
Latencies     [mean, 50, 95, 99, max]  7.781142ms, 6.984017ms, 11.858167ms, 15.236672ms, 659.31168ms
Bytes In      [total, mean]            2653156495, 1312.82
Bytes Out     [total, mean]            580013511, 287.00
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2020953  
Error Set:
```
## Mulan
### Request Rate
!["RequestRate"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/burst2/RequestRate.png)

### Latency
!["Latency"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/burst2/Latency.png)

### Request Rate Status Codes
!["RequestRateStatusCode"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/burst2/RequestRateStatusCode.png)

### CPU Utilization
!["CPUUtilization"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/burst2/CPUUtilization.png)

## Dependencies
### Payments DB Connections
!["DBConnections"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/burst2/DBConnections.png)

### Redis
!["RedisCacheHitMiss"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/burst2/RedisCacheHitMiss.png)

!["RedisPoolClientMetrics"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/burst2/RedisPoolClientMetrics.png)
