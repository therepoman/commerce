---
title: "Results GetPurchaseProfilesByTicketIDs 2020 06_burst2"
date: 2020-06-01T10:45:46-07:00
hidden: true
---


### Host 10.201.38.161

```
Requests      [total, rate]            2020953, 2218.11
Duration      [total, attack, wait]    15m11.182136104s, 15m11.115866622s, 66.269482ms
Latencies     [mean, 50, 95, 99, max]  8.218708ms, 5.324201ms, 19.83786ms, 75.356804ms, 424.461062ms
Bytes In      [total, mean]            963615165, 476.81
Bytes Out     [total, mean]            76727128, 37.97
Success       [ratio]                  99.99%
Status Codes  [code:count]             200:2020775  400:178  
Error Set:
400 Bad Request
```

## Mulan
### Request Rate
!["RequestRate"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-06/burst2/RequestRate.png)

### Latency
!["Latency"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-06/burst2/Latency.png)

### Request Rate Status Codes
!["RequestRateStatusCode"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-06/burst2/RequestRateStatusCode.png)

### CPU Utilization
!["CPUUtilization"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-06/burst2/CPUUtilization.png)

## Dependencies
### Payments DB Connections
!["DBConnections"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-06/burst2/DBConnections.png)

### Redis
!["RedisCacheHitMiss"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-06/burst2/RedisCacheHitMiss.png)

!["RedisPoolClientMetrics"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-06/burst2/RedisPoolClientMetrics.png)
