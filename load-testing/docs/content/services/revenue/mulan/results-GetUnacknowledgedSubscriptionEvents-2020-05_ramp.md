---
title: "Results GetUnacknowledgedSubscriptionEvents 2020 05_ramp"
date: 2020-05-06T11:35:21-07:00
hidden: true
---


### Host 10.201.37.211

```
Requests      [total, rate]            2507436, 464.40
Duration      [total, attack, wait]    1h29m59.332999709s, 1h29m59.309613734s, 23.385975ms
Latencies     [mean, 50, 95, 99, max]  3.157219ms, 2.985245ms, 4.457854ms, 8.485382ms, 283.209579ms
Bytes In      [total, mean]            5497097, 2.19
Bytes Out     [total, mean]            81291514, 32.42
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2507430  400:6  
Error Set:
400 Bad Request
```
## Mulan

### Request Rate
!["GetUnacknowledgedSubscriptionEventsRequestRate"](../../../../images/revenue/mulan/GetUnacknowledgedSubscriptionEvents/2020-05/ramp/RequestRate.png)

### Latency
!["GetUnacknowledgedSubscriptionEventsLatencyP99"](../../../../images/revenue/mulan/GetUnacknowledgedSubscriptionEvents/2020-05/ramp/Latency_P99.png)
!["GetUnacknowledgedSubscriptionEventsLatencyMax"](../../../../images/revenue/mulan/GetUnacknowledgedSubscriptionEvents/2020-05/ramp/Latency_max.png)

### Request Rate Status Codes
!["GetUnacknowledgedSubscriptionEventsRequestRateStatusCode"](../../../../images/revenue/mulan/GetUnacknowledgedSubscriptionEvents/2020-05/ramp/RequestRate-StatusCode.png)

## Dependencies
### Payments DB read-only CPU Utilization
!["GetUnacknowledgedSubscriptionEventsDBCPUUtilization"](../../../../images/revenue/mulan/GetUnacknowledgedSubscriptionEvents/2020-05/ramp/payments-production-read-only-CPUUtilization.png)