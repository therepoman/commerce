---
title: "Mulan"
date: 2020-04-30T11:34:03-07:00
tags: ["money"]
---


## Links

- [Code](https://git.xarth.tv/revenue/mulan)
- [Runbook](https://docs.google.com/document/d/1L588fsV8ITyk45d37a1r9QBeH30prOg1iQiGmubORpA/edit#)
- [Dashboard](https://grafana.xarth.tv/d/000001731/mulan?orgId=1)


## Results

{{% children showhidden="true" %}}

## Traffic Limits
* 06/05/2020
    * Max prod traffic: 8000 total rps
    * Max spike traffic: 7345 total rps for 10 minutes
    * Max increase rate: 1469 rps per second

## Endpoints to Test
marked as essential in JARVIS https://jarvis.xarth.tv/availability/service/2020/m04?service_id=351&classification=Essential
1. GetPurchaseOrdersByIDs
    * Traffic patterns
        * prod 1.3k rps
	    * peak 4k rps
	    * spike 2.7k rps
	* Info
	    * 3-5 purchase order ids in any request https://grafana.internal.justin.tv/d/000001731/mulan?panelId=49&fullscreen&orgId=1&from=now-7d&to=now
        * cached with 5 second TTL
        * 800 millisecond context timeout
        * queries paymentsdb master
            ```
            curl --request POST \
              --url https://prod.mulan.twitch.a2z.com//twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseOrdersByIDs \
              --header 'content-type: application/json' \
              --data '{"ids": ["amzn1.twitch.payments.order.000c3ac2-3288-4586-a5b2-56572cdb003d"]}'
          ```

2. GetUnacknowledgedSubscriptionEvents
	* Traffic patterns
	    * prod 700 rps
	    * peak 1.7k rps
	    * spike 1k rps
    * Info
        * oner user id and one platform per request
        * 800 millisecond context timeout
        * queries payments db replica
        * no cache
            ```
            curl --request POST \
              --url https://prod.mulan.twitch.a2z.com//twirp/code.justin.tv.revenue.mulan.Mulan/GetUnacknowledgedSubscriptionEvents \
              --header 'content-type: application/json' \
              --data '{"user_id": "123456789"}'
          ```

increased load caused outage on 2/27/2020 https://docs.google.com/document/d/1QBQ-WhfywwvqOcUWKDxe5iaUyGgn_hwLtLk0DNVgdYc/edit

3. GetPurchaseProfilesByTicketIds
	* Traffic patterns
	    * prod 1.3k rps
	    * peak 4k rps
	    * spike 2.7k
    * Info
        * multiple ticket ids per request (no data on how many per on production)
        * 800 millisecond context timeout
        * 5 second TTL in cache
        * hits paymentsdb master
            ```
            curl --request POST \
              --url https://prod.mulan.twitch.a2z.com//twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs \
              --header 'content-type: application/json' \
              --data '{"ticket_ids": ["123456"]}'
          ```
4. GetSubscriptionsByIds
	* Traffic patterns
	    * prod 740 rps
	    * peak 2k rps
	    * spike 1.26k
    * Info
        * multiple subscription ids per request (no data on how many per on production)
        * 800 millisecond context timeout
        * hits payments db master
        * no cache
            ```
            curl --request POST \
              --url https://prod.mulan.twitch.a2z.com//twirp/code.justin.tv.revenue.mulan.Mulan/GetSubscriptionsByIDs \
              --header 'content-type: application/json' \
              --data '{"ids": ["b7556d2c-e12f-46f1-b408-348d3905ebc2"]}'
          ```

##  Running the tests
1. First set up your aws command line to point to `twitch-revenue-aws` and export $AWS_PROFILE and $USERNAME
    ```
    export AWS_PROFILE=twitch-revenue-aws
    export USERNAME=$(whoami)
    ```
2. Run `make plan`
3. Run `make apply` if everything looks good
4. Run `make prep` to copy over all of mulan's load test plans and all datasets
5. Run tests

    To run all ramp tests concurrently
    ```bash
    $ make start LOADTEST_FILES="*_ramp.toml subscription_uuids.toml purchase_profile_ticket_ids.toml purchase_order_ids.toml user_ids_300k.toml"
    ```
    To run all burst1 tests concurrently
    ```bash
    $ make start LOADTEST_FILES="*_burst1.toml subscription_uuids.toml purchase_profile_ticket_ids.toml purchase_order_ids.toml user_ids_300k.toml"
    ```
    To run all burst2 tests concurrently
    ```bash
    $ make start LOADTEST_FILES="*_burst2.toml subscription_uuids.toml purchase_profile_ticket_ids.toml purchase_order_ids.toml user_ids_300k.toml"
    ```
    To run all burst3 tests concurrently
    ```bash
    $ make start LOADTEST_FILES="*_burst3.toml subscription_uuids.toml purchase_profile_ticket_ids.toml purchase_order_ids.toml user_ids_300k.toml"
    ```

    To specify the endpoints you want to test concurrently, list out the test files and data files needed
    ```bash
    $ make start LOADTEST_FILES="GetPurchaseOrdersByIDs_ramp.toml purchase_order_ids.toml"
    ```
    ```bash
    $ make start LOADTEST_FILES="GetPurchaseOrdersByIDs_ramp.toml GetPurchaseProfilesByTicketIDs_ramp.toml purchase_order_ids.toml purchase_profile_ticket_ids.toml"
    ```
6. Run `make check_status` to check if the test is still running
    a. If you need to prematurely stop the tests, run `make stop`
7. Run `make get_results OUTPUT_DIR=results/<test-name>/<YYYY-MM>`
8. Run `make publish_results RESULTS_DIR=results/<test-name>/<YYYY-MM>`
9. Run `make destroy` to clean up the instances you spun up

### Dashboard/Graphs to monitor
* Mulan Grafana https://grafana.xarth.tv/d/000001731/mulan?from=now-1h&to=now
    * Request Rate https://grafana.xarth.tv/d/000001731/mulan?panelId=1&fullscreen&from=now-1h&to=now
    * Request Rate Status Code https://grafana.xarth.tv/d/000001731/mulan?panelId=11&fullscreen&from=now-1h&to=now
    * Latency https://grafana.xarth.tv/d/000001731/mulan?panelId=3&fullscreen&from=now-1h&to=now
    * CPU Utilization https://grafana.xarth.tv/d/000001731/mulan?panelId=4&fullscreen&from=now-1h&to=now
    * DB Connections https://grafana.xarth.tv/d/000001731/mulan?panelId=26&fullscreen&from=now-1h&to=now
    * Redis Cache Hit/Miss: https://grafana.xarth.tv/d/000001731/mulan?panelId=45&fullscreen&from=now-1h&to=now
    * Redis Cache Pool Client Metrics: https://grafana.xarth.tv/d/000001731/mulan?panelId=50&fullscreen&from=now-1h&to=now

* Payments DB
    * Payments DB dashboard in Cloudwatch (twitch-revenue-aws account) https://us-west-2.console.aws.amazon.com/cloudwatch/home?region=us-west-2#dashboards:name=PaymentsDB

* Redis
    * PaymentsCache dashboard in Cloudwatch (twitch-revenue-aws account) https://us-west-2.console.aws.amazon.com/cloudwatch/home?region=us-west-2#dashboards:name=PaymentsCache

* Flo (may be affected by load tests)
    * https://grafana.xarth.tv/d/TzcWOrKmz/flo?orgId=1&from=now-1h&to=now&refresh=5s
