---
title: "Results All 2020 06_burst1"
date: 2020-06-05T10:17:13-07:00
hidden: true
---


### Host 10.201.38.161

```
Requests      [total, rate]            4300296, 4613.80
Duration      [total, attack, wait]    15m32.187563916s, 15m32.051737647s, 135.826269ms
Latencies     [mean, 50, 95, 99, max]  165.494262ms, 165.846337ms, 345.751687ms, 431.475834ms, 3.996267707s
Bytes In      [total, mean]            3121861612, 725.96
Bytes Out     [total, mean]            570136876, 132.58
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:4300290  400:2  504:4  
Error Set:
400 Bad Request
504 GATEWAY_TIMEOUT
```

