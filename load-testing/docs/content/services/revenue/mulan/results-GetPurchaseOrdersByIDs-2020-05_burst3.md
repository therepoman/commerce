---
title: "Results GetPurchaseOrdersByIDs 2020 05_burst3"
date: 2020-05-20T11:16:59-07:00
hidden: true
---


### Host 10.201.38.203

```
Requests      [total, rate]            3031503, 3346.39
Duration      [total, attack, wait]    15m5.915242348s, 15m5.903131941s, 12.110407ms
Latencies     [mean, 50, 95, 99, max]  7.701803ms, 6.058837ms, 11.428387ms, 16.599866ms, 1.252543413s
Bytes In      [total, mean]            2021409410, 666.80
Bytes Out     [total, mean]            833544801, 274.96
Success       [ratio]                  99.97%
Status Codes  [code:count]             0:420  200:3030481  500:602  
Error Set:
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseOrdersByIDs: dial tcp 0.0.0.0:0->10.201.37.118:443: socket: too many open files
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseOrdersByIDs: dial tcp: lookup prod.mulan.twitch.a2z.com on 10.201.36.2:53: no such host
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseOrdersByIDs: dial tcp: lookup prod.mulan.twitch.a2z.com on 10.201.36.2:53: dial udp 10.201.36.2:53: socket: too many open files
500 Internal Server Error
```
## Mulan
### Request Rate
!["RequestRate"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/burst3/RequestRate.png)

### Latency
!["Latency"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/burst3/Latency.png)

### Request Rate Status Codes
!["RequestRateStatusCode"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/burst3/RequestRateStatusCode.png)

### CPU Utilization
!["CPUUtilization"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/burst3/CPUUtilization.png)

## Dependencies
### Payments DB Connections
!["DBConnections"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/burst3/DBConnections.png)

### Redis
!["RedisCacheHitMiss"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/burst3/RedisCacheHitMiss.png)

!["RedisPoolClientMetrics"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/burst3/RedisPoolClientMetrics.png)

