---
title: "Results GetPurchaseProfilesByTicketIDs 2020 05_ramp"
date: 2020-05-18T12:02:25-07:00
hidden: true
---


### Host 10.201.38.217

```
Requests      [total, rate]            4656636, 861.16
Duration      [total, attack, wait]    1h30m7.432885577s, 1h30m7.401315427s, 31.57015ms
Latencies     [mean, 50, 95, 99, max]  8.04461ms, 4.967876ms, 21.028014ms, 75.634308ms, 1.644752108s
Bytes In      [total, mean]            2189612339, 470.21
Bytes Out     [total, mean]            176433974, 37.89
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:4656630  500:6  
Error Set:
500 Internal Server Error
```

## Mulan
### Request Rate
!["GetPurchaseProfilesByTicketIDsRequestRate"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-05/ramp/RequestRate.png)

### Latency
!["GetPurchaseProfilesByTicketIDsLatency"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-05/ramp/Latency.png)

### Request Rate Status Codes
!["GetPurchaseProfilesByTicketIDsRequestRateStatusCode"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-05/ramp/RequestRateStatusCode.png)

## Dependencies
### Payments DB Connections
!["GetPurchaseProfilesByTicketIDsDBConnections"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-05/ramp/DBConnections.png)

### Redis
!["GetPurchaseProfilesByTicketIDsRedisCacheHitMiss"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-05/ramp/RedisCacheHitMiss.png)

!["GetPurchaseProfilesByTicketIDsRedisPoolClientMetrics"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-05/ramp/RedisPoolClientMetrics.png)
