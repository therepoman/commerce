---
title: "Results GetUnacknowledgedSubscriptionEvents 2020 05_burst1"
date: 2020-05-20T15:16:25-07:00
hidden: true
---


### Host 10.201.38.203

```
Requests      [total, rate]            561453, 620.20
Duration      [total, attack, wait]    15m5.278548802s, 15m5.270390709s, 8.158093ms
Latencies     [mean, 50, 95, 99, max]  3.129835ms, 2.67748ms, 4.592505ms, 8.366972ms, 6.041824053s
Bytes In      [total, mean]            1231633, 2.19
Bytes Out     [total, mean]            18202929, 32.42
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:561451  400:2  
Error Set:
400 Bad Request
```

### Request Rate
!["RequestRate"](../../../../images/revenue/mulan/GetUnacknowledgedSubscriptionEvents/2020-05/burst1/RequestRate.png)

### Latency
!["Latency"](../../../../images/revenue/mulan/GetUnacknowledgedSubscriptionEvents/2020-05/burst1/Latency.png)

### Request Rate Status Codes
!["RequestRateStatusCode"](../../../../images/revenue/mulan/GetUnacknowledgedSubscriptionEvents/2020-05/burst1/RequestRateStatusCode.png)

### CPU Utilization
!["CPUUtilization"](../../../../images/revenue/mulan/GetUnacknowledgedSubscriptionEvents/2020-05/burst1/CPUUtilization.png)
