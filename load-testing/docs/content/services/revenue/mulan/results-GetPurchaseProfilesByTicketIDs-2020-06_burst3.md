---
title: "Results GetPurchaseProfilesByTicketIDs 2020 06_burst3"
date: 2020-06-01T11:16:22-07:00
hidden: true
---


### Host 10.201.38.161

```
Requests      [total, rate]            3031503, 3217.50
Duration      [total, attack, wait]    15m42.276258265s, 15m42.191203659s, 85.054606ms
Latencies     [mean, 50, 95, 99, max]  25.781583ms, 5.850404ms, 31.263032ms, 89.733582ms, 5.278271709s
Bytes In      [total, mean]            1454292935, 479.73
Bytes Out     [total, mean]            115125096, 37.98
Success       [ratio]                  99.99%
Status Codes  [code:count]             0:85  200:3031224  400:193  504:1  
Error Set:
400 Bad Request
504 GATEWAY_TIMEOUT
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: EOF
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:46256->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:50474->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:43922->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:53772->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:57410->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:47216->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:54318->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:41140->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:41293->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:41305->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:34921->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:43457->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:59339->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:55735->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:41509->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:57183->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:35817->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:43325->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:38467->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:43999->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:60219->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:46859->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:49673->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:51529->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:33789->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:57959->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:42507->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:35169->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:42973->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:51027->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:50074->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:41510->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:55704->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:41163->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:36087->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:54121->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:48257->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:37213->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:50193->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:54667->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:53041->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:47303->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:52933->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:55005->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:57795->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:45339->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:49719->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:48159->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:58061->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:43105->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:48817->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:35727->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:41501->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:36051->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:57887->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:40581->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:45045->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:54045->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:49826->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:58511->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: read tcp 10.201.38.161:38553->10.201.38.126:443: read: connection reset by peer
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: net/http: timeout awaiting response headers
```

## Mulan
### Request Rate
!["RequestRate"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-06/burst3/RequestRate.png)

### Latency
!["Latency"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-06/burst3/Latency.png)

### Request Rate Status Codes
!["RequestRateStatusCode"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-06/burst3/RequestRateStatusCode.png)

### CPU Utilization
!["CPUUtilization"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-06/burst3/CPUUtilization.png)

## Dependencies
### Payments DB Connections
!["DBConnections"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-06/burst3/DBConnections.png)

### Redis
!["RedisCacheHitMiss"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-06/burst3/RedisCacheHitMiss.png)

!["RedisPoolClientMetrics"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-06/burst3/RedisPoolClientMetrics.png)
