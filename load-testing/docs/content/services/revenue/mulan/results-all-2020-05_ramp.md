---
title: "Results All 2020 05_ramp"
date: 2020-05-19T11:34:04-07:00
hidden: true
---


### Host 10.201.38.203

```
Requests      [total, rate]            14471424, 2676.45
Duration      [total, attack, wait]    1h30m6.978716679s, 1h30m6.944039951s, 34.676728ms
Latencies     [mean, 50, 95, 99, max]  7.14724ms, 5.43738ms, 12.189306ms, 49.755684ms, 2.026343446s
Bytes In      [total, mean]            9898081239, 683.97
Bytes Out     [total, mean]            1816795314, 125.54
Success       [ratio]                  99.99%
Status Codes  [code:count]             0:344  200:14470237  400:7  500:836  
Error Set:
400 Bad Request
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseOrdersByIDs: dial tcp 0.0.0.0:0->10.201.36.141:443: socket: too many open files
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetUnacknowledgedSubscriptionEvents: dial tcp 0.0.0.0:0->10.201.36.141:443: socket: too many open files
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/all: dial tcp: lookup prod.mulan.twitch.a2z.com on 10.201.36.2:53: dial udp 10.201.36.2:53: socket: too many open files
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseProfilesByTicketIDs: dial tcp: lookup prod.mulan.twitch.a2z.com on 10.201.36.2:53: dial udp 10.201.36.2:53: socket: too many open files
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetPurchaseOrdersByIDs: dial tcp: lookup prod.mulan.twitch.a2z.com on 10.201.36.2:53: dial udp 10.201.36.2:53: socket: too many open files
Post https://prod.mulan.twitch.a2z.com/twirp/code.justin.tv.revenue.mulan.Mulan/GetUnacknowledgedSubscriptionEvents: dial tcp: lookup prod.mulan.twitch.a2z.com on 10.201.36.2:53: dial udp 10.201.36.2:53: socket: too many open files
500 Internal Server Error
```

## Graphs

### Request Rate
!["allRequestRate"](../../../../images/revenue/mulan/all/2020-05/ramp/RequestRate.png)

### Latency
!["allLatency"](../../../../images/revenue/mulan/all/2020-05/ramp/Latency.png)

### Request Rate Status Codes
!["allRequestRateStatusCode"](../../../../images/revenue/mulan/all/2020-05/ramp/RequestRateStatusCode.png)

### Payments DB Connections
!["allDBConnections"](../../../../images/revenue/mulan/all/2020-05/ramp/DBConnections.png)

### Redis
!["allRedisCacheHitMiss"](../../../../images/revenue/mulan/all/2020-05/ramp/RedisCacheHitMiss.png)

!["allRedisPoolClientMetrics"](../../../../images/revenue/mulan/all/2020-05/ramp/RedisPoolClientMetrics.png)
