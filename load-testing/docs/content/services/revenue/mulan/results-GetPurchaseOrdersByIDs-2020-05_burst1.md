---
title: "Results GetPurchaseOrdersByIDs 2020 05_burst1"
date: 2020-05-20T10:15:45-07:00
hidden: true
---


### Host 10.201.38.203

```
Requests      [total, rate]            1515753, 1682.49
Duration      [total, attack, wait]    15m0.909711459s, 15m0.899330703s, 10.380756ms
Latencies     [mean, 50, 95, 99, max]  6.990696ms, 6.395917ms, 11.688675ms, 13.487287ms, 602.143482ms
Bytes In      [total, mean]            2000444049, 1319.77
Bytes Out     [total, mean]            435021111, 287.00
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1515753  
Error Set:
```
## Mulan
### Request Rate
!["RequestRate"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/burst1/RequestRate.png)

### Latency
!["Latency"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/burst1/Latency.png)

### Request Rate Status Codes
!["RequestRateStatusCode"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/burst1/RequestRateStatusCode.png)

### CPU Utilization
!["CPUUtilization"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/burst1/CPUUtilization.png)

## Dependencies
### Payments DB Connections
!["DBConnections"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/burst1/DBConnections.png)

### Redis
!["RedisCacheHitMiss"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/burst1/RedisCacheHitMiss.png)

!["RedisPoolClientMetrics"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/burst1/RedisPoolClientMetrics.png)
