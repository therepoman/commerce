---
title: "Results GetSubscriptionsByIDs 2020 05_burst3"
date: 2020-05-21T16:19:13-07:00
hidden: true
---


### Host 10.201.38.203

```
Requests      [total, rate]            1414671, 1570.69
Duration      [total, attack, wait]    15m0.683998967s, 15m0.670278045s, 13.720922ms
Latencies     [mean, 50, 95, 99, max]  7.985781ms, 6.325653ms, 12.678417ms, 16.489994ms, 1.410470515s
Bytes In      [total, mean]            822101055, 581.13
Bytes Out     [total, mean]            118832364, 84.00
Success       [ratio]                  99.99%
Status Codes  [code:count]             200:1414549  500:122  
Error Set:
500 Internal Server Error
```

## Mulan
### Request Rate
!["RequestRate"](../../../../images/revenue/mulan/GetSubscriptionsByIDs/2020-05/burst3/RequestRate.png)

### Latency
!["Latency"](../../../../images/revenue/mulan/GetSubscriptionsByIDs/2020-05/burst3/Latency.png)

### Request Rate Status Codes
!["RequestRateStatusCode"](../../../../images/revenue/mulan/GetSubscriptionsByIDs/2020-05/burst3/RequestRateStatusCode.png)

### CPU Utilization
!["CPUUtilization"](../../../../images/revenue/mulan/GetSubscriptionsByIDs/2020-05/burst3/CPUUtilization.png)

## Dependencies
### Payments DB Connections
!["DBConnections"](../../../../images/revenue/mulan/GetSubscriptionsByIDs/2020-05/burst3/DBConnections.png)
