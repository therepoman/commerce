---
title: "Results GetPurchaseOrdersByIDs 2020 05_ramp"
date: 2020-05-06T17:34:33-07:00
hidden: true
---


### Host 10.201.37.211

```
Requests      [total, rate]            4656636, 862.37
Duration      [total, attack, wait]    1h29m59.797417986s, 1h29m59.794736967s, 2.681019ms
Latencies     [mean, 50, 95, 99, max]  4.015718ms, 2.975981ms, 10.891124ms, 11.84846ms, 472.149864ms
Bytes In      [total, mean]            6183389027, 1327.87
Bytes Out     [total, mean]            1336454532, 287.00
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:4656636  
Error Set:
```

## Mulan
### Request Rate
!["GetPurchaseOrdersByIDsRequestRate"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/ramp/RequestRate.png)

### Latency
!["GetPurchaseOrdersByIDsLatencyMax"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/ramp/Latency_max.png)

### Request Rate Status Codes
!["GetPurchaseOrdersByIDsRequestRateStatusCode"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/ramp/RequestRateStatusCodes.png)

## Dependencies
### Payments DB read-only CPU Utilization and Connection count
!["GetPurchaseOrdersByIDsDBCPUUtilization"](../../../../images/revenue/mulan/GetPurchaseOrdersByIDs/2020-05/ramp/DB_CPUUtilization_Connections.png)