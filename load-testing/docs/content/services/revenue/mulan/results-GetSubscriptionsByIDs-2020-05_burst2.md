---
title: "Results GetSubscriptionsByIDs 2020 05_burst2"
date: 2020-05-21T15:47:03-07:00
hidden: true
---


### Host 10.201.38.203

```
Requests      [total, rate]            943113, 1044.78
Duration      [total, attack, wait]    15m2.698002176s, 15m2.687828592s, 10.173584ms
Latencies     [mean, 50, 95, 99, max]  7.678001ms, 6.137324ms, 12.457231ms, 14.806571ms, 633.050637ms
Bytes In      [total, mean]            548103582, 581.16
Bytes Out     [total, mean]            79221492, 84.00
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:943113  
Error Set:
```

## Mulan
### Request Rate
!["RequestRate"](../../../../images/revenue/mulan/GetSubscriptionsByIDs/2020-05/burst2/RequestRate.png)

### Latency
!["Latency"](../../../../images/revenue/mulan/GetSubscriptionsByIDs/2020-05/burst2/Latency.png)

### Request Rate Status Codes
!["RequestRateStatusCode"](../../../../images/revenue/mulan/GetSubscriptionsByIDs/2020-05/burst2/RequestRateStatusCode.png)

### CPU Utilization
!["CPUUtilization"](../../../../images/revenue/mulan/GetSubscriptionsByIDs/2020-05/burst2/CPUUtilization.png)

## Dependencies
### Payments DB Connections
!["DBConnections"](../../../../images/revenue/mulan/GetSubscriptionsByIDs/2020-05/burst2/DBConnections.png)
