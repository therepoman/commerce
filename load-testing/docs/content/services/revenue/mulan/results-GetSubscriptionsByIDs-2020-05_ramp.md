---
title: "Results GetSubscriptionsByIDs 2020 05_ramp"
date: 2020-05-18T16:36:37-07:00
hidden: true
---


### Host 10.201.38.217

```
Requests      [total, rate]            2650716, 490.86
Duration      [total, attack, wait]    1h30m0.128296418s, 1h30m0.120579567s, 7.716851ms
Latencies     [mean, 50, 95, 99, max]  7.887156ms, 6.408161ms, 12.613019ms, 15.095532ms, 744.127071ms
Bytes In      [total, mean]            1541873507, 581.68
Bytes Out     [total, mean]            222660144, 84.00
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2650716  
Error Set:
```

## Mulan
### Request Rate
!["GetSubscriptionsByIDsRequestRate"](../../../../images/revenue/mulan/GetSubscriptionsByIDs/2020-05/ramp/RequestRate.png)

### Latency
!["GetSubscriptionsByIDsLatency"](../../../../images/revenue/mulan/GetSubscriptionsByIDs/2020-05/ramp/Latency.png)

### Request Rate Status Codes
!["GetSubscriptionsByIDsRequestRateStatusCode"](../../../../images/revenue/mulan/GetSubscriptionsByIDs/2020-05/ramp/RequestRateStatusCode.png)

## Dependencies
### Payments DB Connections
!["GetSubscriptionsByIDsDBConnections"](../../../../images/revenue/mulan/GetSubscriptionsByIDs/2020-05/ramp/DBConnections.png)

