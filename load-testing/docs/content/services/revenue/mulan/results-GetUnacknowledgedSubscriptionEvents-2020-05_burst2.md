---
title: "Results GetUnacknowledgedSubscriptionEvents 2020 05_burst2"
date: 2020-05-20T15:48:18-07:00
hidden: true
---


### Host 10.201.38.203

```
Requests      [total, rate]            748503, 831.93
Duration      [total, attack, wait]    14m59.722701631s, 14m59.720235551s, 2.46608ms
Latencies     [mean, 50, 95, 99, max]  3.124949ms, 2.703997ms, 4.662759ms, 8.21963ms, 387.103819ms
Bytes In      [total, mean]            1637029, 2.19
Bytes Out     [total, mean]            24267269, 32.42
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:748501  400:2  
Error Set:
400 Bad Request
```

### Request Rate
!["RequestRate"](../../../../images/revenue/mulan/GetUnacknowledgedSubscriptionEvents/2020-05/burst2/RequestRate.png)

### Latency
!["Latency"](../../../../images/revenue/mulan/GetUnacknowledgedSubscriptionEvents/2020-05/burst2/Latency.png)

### Request Rate Status Codes
!["RequestRateStatusCode"](../../../../images/revenue/mulan/GetUnacknowledgedSubscriptionEvents/2020-05/burst2/RequestRateStatusCode.png)

### CPU Utilization
!["CPUUtilization"](../../../../images/revenue/mulan/GetUnacknowledgedSubscriptionEvents/2020-05/burst2/CPUUtilization.png)
