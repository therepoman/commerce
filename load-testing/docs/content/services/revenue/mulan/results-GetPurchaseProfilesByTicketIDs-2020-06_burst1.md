---
title: "Results GetPurchaseProfilesByTicketIDs 2020 06_burst1"
date: 2020-06-01T10:16:35-07:00
hidden: true
---


### Host 10.201.38.161

```
Requests      [total, rate]            1515753, 1666.95
Duration      [total, attack, wait]    15m9.450949053s, 15m9.297843983s, 153.10507ms
Latencies     [mean, 50, 95, 99, max]  7.958902ms, 5.018295ms, 18.271982ms, 74.647394ms, 369.747872ms
Bytes In      [total, mean]            720052236, 475.05
Bytes Out     [total, mean]            57496898, 37.93
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1515753  
Error Set:
```

## Mulan
### Request Rate
!["RequestRate"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-06/burst1/RequestRate.png)

### Latency
!["Latency"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-06/burst1/Latency.png)

### Request Rate Status Codes
!["RequestRateStatusCode"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-06/burst1/RequestRateStatusCode.png)

### CPU Utilization
!["CPUUtilization"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-06/burst1/CPUUtilization.png)

## Dependencies
### Payments DB Connections
!["DBConnections"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-06/burst1/DBConnections.png)

### Redis
!["RedisCacheHitMiss"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-06/burst1/RedisCacheHitMiss.png)

!["RedisPoolClientMetrics"](../../../../images/revenue/mulan/GetPurchaseProfilesByTicketIDs/2020-06/burst1/RedisPoolClientMetrics.png)
