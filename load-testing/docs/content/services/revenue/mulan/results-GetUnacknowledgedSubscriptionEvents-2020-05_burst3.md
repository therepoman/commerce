---
title: "Results GetUnacknowledgedSubscriptionEvents 2020 05_burst3"
date: 2020-05-20T16:18:42-07:00
hidden: true
---


### Host 10.201.38.203

```
Requests      [total, rate]            1122753, 1247.58
Duration      [total, attack, wait]    14m59.95074918s, 14m59.946282166s, 4.467014ms
Latencies     [mean, 50, 95, 99, max]  2.964357ms, 2.590829ms, 4.269407ms, 8.089809ms, 283.989771ms
Bytes In      [total, mean]            2455223, 2.19
Bytes Out     [total, mean]            36399697, 32.42
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1122751  400:2  
Error Set:
400 Bad Request
```

### Request Rate
!["RequestRate"](../../../../images/revenue/mulan/GetUnacknowledgedSubscriptionEvents/2020-05/burst3/RequestRate.png)

### Latency
!["Latency"](../../../../images/revenue/mulan/GetUnacknowledgedSubscriptionEvents/2020-05/burst3/Latency.png)

### Request Rate Status Codes
!["RequestRateStatusCode"](../../../../images/revenue/mulan/GetUnacknowledgedSubscriptionEvents/2020-05/burst3/RequestRateStatusCode.png)

### CPU Utilization
!["CPUUtilization"](../../../../images/revenue/mulan/GetUnacknowledgedSubscriptionEvents/2020-05/burst3/CPUUtilization.png)
