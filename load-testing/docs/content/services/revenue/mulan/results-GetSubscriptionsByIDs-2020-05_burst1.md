---
title: "Results GetSubscriptionsByIDs 2020 05_burst1"
date: 2020-05-21T15:16:42-07:00
hidden: true
---


### Host 10.201.38.203

```
Requests      [total, rate]            707337, 783.80
Duration      [total, attack, wait]    15m2.45663946s, 15m2.44508958s, 11.54988ms
Latencies     [mean, 50, 95, 99, max]  7.859391ms, 6.331125ms, 12.565849ms, 15.22548ms, 698.027441ms
Bytes In      [total, mean]            411060664, 581.14
Bytes Out     [total, mean]            59416308, 84.00
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:707337  
Error Set:
```
## Mulan
### Request Rate
!["RequestRate"](../../../../images/revenue/mulan/GetSubscriptionsByIDs/2020-05/burst1/RequestRate.png)

### Latency
!["Latency"](../../../../images/revenue/mulan/GetSubscriptionsByIDs/2020-05/burst1/Latency.png)

### Request Rate Status Codes
!["RequestRateStatusCode"](../../../../images/revenue/mulan/GetSubscriptionsByIDs/2020-05/burst1/RequestRateStatusCode.png)

### CPU Utilization
!["CPUUtilization"](../../../../images/revenue/mulan/GetSubscriptionsByIDs/2020-05/burst1/CPUUtilization.png)

## Dependencies
### Payments DB Connections
!["DBConnections"](../../../../images/revenue/mulan/GetSubscriptionsByIDs/2020-05/burst1/DBConnections.png)
