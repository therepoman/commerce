---
title: "Results GetUserChannelSubscriptionWithPaymentsData_burst2 2020 05"
date: 2020-05-21T11:01:24-07:00
hidden: true
---


### Host 10.202.80.145

```
Requests      [total, rate, throughput]         3353278, 3678.51, 3678.47
Duration      [total, attack, wait]             15m12s, 15m12s, 8.691ms
Latencies     [min, mean, 50, 90, 95, 99, max]  497.523µs, 4.387ms, 1.739ms, 3.077ms, 16.392ms, 72.452ms, 352.47ms
Bytes In      [total, mean]                     6706556, 2.00
Bytes Out     [total, mean]                     216340061, 64.52
Success       [ratio]                           100.00%
Status Codes  [code:count]                      200:3353278  
Error Set:
```

