---
title: "Results GetUserProductSubscriptions_burst3 2020 05"
date: 2020-05-20T15:34:30-07:00
hidden: true
---


### Host 10.202.82.42

```
Requests      [total, rate, throughput]         1919910, 2106.54, 2106.39
Duration      [total, attack, wait]             15m11s, 15m11s, 67.091ms
Latencies     [min, mean, 50, 90, 95, 99, max]  671.956µs, 5.235ms, 1.743ms, 3.655ms, 31.031ms, 76.326ms, 368.961ms
Bytes In      [total, mean]                     38398200, 20.00
Bytes Out     [total, mean]                     174437390, 90.86
Success       [ratio]                           100.00%
Status Codes  [code:count]                      200:1919910  
Error Set:
```

