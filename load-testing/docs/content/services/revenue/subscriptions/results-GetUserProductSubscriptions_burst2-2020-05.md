---
title: "Results GetUserProductSubscriptions_burst2 2020 05"
date: 2020-05-20T15:34:23-07:00
hidden: true
---


### Host 10.202.82.42

```
Requests      [total, rate, throughput]         1279953, 1407.90, 1407.80
Duration      [total, attack, wait]             15m9s, 15m9s, 62.199ms
Latencies     [min, mean, 50, 90, 95, 99, max]  663.034µs, 5.37ms, 1.707ms, 4.084ms, 33.225ms, 76.581ms, 401.562ms
Bytes In      [total, mean]                     25599060, 20.00
Bytes Out     [total, mean]                     116291552, 90.86
Success       [ratio]                           100.00%
Status Codes  [code:count]                      200:1279953  
Error Set:
```

