---
title: "Results GetUserProductSubscriptions_burst1 2020 05"
date: 2020-05-20T15:34:05-07:00
hidden: true
---


### Host 10.202.82.42

```
Requests      [total, rate, throughput]         960330, 1058.68, 1058.68
Duration      [total, attack, wait]             15m7s, 15m7s, 2.334ms
Latencies     [min, mean, 50, 90, 95, 99, max]  670.711µs, 4.797ms, 1.839ms, 3.193ms, 23.11ms, 74.343ms, 274.103ms
Bytes In      [total, mean]                     19206600, 20.00
Bytes Out     [total, mean]                     87248979, 90.85
Success       [ratio]                           100.00%
Status Codes  [code:count]                      200:960330  
Error Set:
```

