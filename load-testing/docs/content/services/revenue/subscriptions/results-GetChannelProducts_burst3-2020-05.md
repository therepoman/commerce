---
title: "Results GetChannelProducts_burst3 2020 05"
date: 2020-05-18T12:59:54-07:00
hidden: true
---


### Host 10.202.80.243

```
Requests      [total, rate]            2582403, 2832.94
Duration      [total, attack, wait]    15m11.601498964s, 15m11.562384175s, 39.114789ms
Latencies     [mean, 50, 95, 99, max]  5.065706ms, 1.833597ms, 24.555181ms, 75.827518ms, 3.42572189s
Bytes In      [total, mean]            1043870296, 404.22
Bytes Out     [total, mean]            161440590, 62.52
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2582403  
Error Set:
```

