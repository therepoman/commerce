---
title: "Results GetUserChannelSubscriptionWithPaymentsData_burst3 2020 05"
date: 2020-05-21T11:35:14-07:00
hidden: true
---


### Host 10.202.80.145

```
Requests      [total, rate, throughput]         5029923, 5505.22, 5504.79
Duration      [total, attack, wait]             15m14s, 15m14s, 70.603ms
Latencies     [min, mean, 50, 90, 95, 99, max]  490.509µs, 4.533ms, 1.516ms, 3.129ms, 23.556ms, 74.268ms, 352.28ms
Bytes In      [total, mean]                     10059846, 2.00
Bytes Out     [total, mean]                     324511241, 64.52
Success       [ratio]                           100.00%
Status Codes  [code:count]                      200:5029923  
Error Set:
```

