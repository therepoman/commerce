---
title: "Results GetSubscriberBadge_burst3 2020 05"
date: 2020-05-15T15:40:57-07:00
hidden: true
---


### Host 10.202.82.75

```
Requests      [total, rate]            1425903, 1567.79
Duration      [total, attack, wait]    15m9.547401108s, 15m9.495954741s, 51.446367ms
Latencies     [mean, 50, 95, 99, max]  4.906444ms, 2.055838ms, 19.216244ms, 75.24739ms, 423.825512ms
Bytes In      [total, mean]            29943963, 21.00
Bytes Out     [total, mean]            91752634, 64.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1425903  
Error Set:
```

