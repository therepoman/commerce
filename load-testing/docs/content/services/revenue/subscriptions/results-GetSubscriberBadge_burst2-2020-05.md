---
title: "Results GetSubscriberBadge_burst2 2020 05"
date: 2020-05-15T15:11:09-07:00
hidden: true
---


### Host 10.202.82.75

```
Requests      [total, rate]            950613, 1046.93
Duration      [total, attack, wait]    15m8.073738303s, 15m7.998384424s, 75.353879ms
Latencies     [mean, 50, 95, 99, max]  5.550943ms, 2.197749ms, 26.802658ms, 78.745691ms, 410.371853ms
Bytes In      [total, mean]            19962873, 21.00
Bytes Out     [total, mean]            61170352, 64.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:950613  
Error Set:
```

