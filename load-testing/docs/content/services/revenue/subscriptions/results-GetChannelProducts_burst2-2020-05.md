---
title: "Results GetChannelProducts_burst2 2020 05"
date: 2020-05-18T12:26:53-07:00
hidden: true
---


### Host 10.202.81.197

```
Requests      [total, rate]            1721553, 1888.39
Duration      [total, attack, wait]    15m11.749899498s, 15m11.653599902s, 96.299596ms
Latencies     [mean, 50, 95, 99, max]  5.33973ms, 1.917022ms, 26.622156ms, 77.135634ms, 346.175094ms
Bytes In      [total, mean]            695700281, 404.11
Bytes Out     [total, mean]            107625093, 62.52
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1721553  
Error Set:
```

