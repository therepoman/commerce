---
title: "Subscriptions"
tags: ["memberships"]
---



### Links

- [Old load test repo](https://git.xarth.tv/revenue/dutchcrunch)
- [Runbook](https://git.xarth.tv/pages/subs/docs/docs/operations/subs.html)
- [Dashboard](https://grafana.xarth.tv/d/6VdbUQqWk/subscriptions-api-deep-dive?orgId=1&from=now-1h&to=now&refresh=5s&var-AWSAccount=twitch-subs-aws%20522398581884&var-Stage=production&var-Substage=primary&var-API=GetUserChannelSubscriptionWithPaymentsData&var-Dependency=sitedb_read)


### Running the tests

These tests are orchestrated via the `Makefile` using [piledriver][pl].

[pl]: ../../../getting-started/piledriver

Get set up by enabling teleport-bastion and exporting the following environment variables. 
If you don't export these variables, you'll need to provide them as command-line args 
for every subsequent make command. 


```bash
$ teleport-bastion enable      # the connections to the load testing hosts are tunneled through the jumpbox.
$ teleport-bastion login
$ export AWS_PROFILE=twitch-subs-aws
$ export USERNAME=$(whoami)
$ export TC=subs-prod
```

First, update the test plan files with the desired TPS. Keep in mind that every host will run this file,
so if you're using multiple hosts, divide the total TPS you want by the number of hosts to get the TPS value you should
include in the test.


Next, follow the [piledriver][pl] instructions, or use this as the TL;DR:

```bash
$ cd load-tests/services/revenue/subscriptions
$ make init NUM_HOSTS=10
$ make plan
$ make apply
$ make list_hosts
$ make set_file_descriptors
$ make prep                     # from here and above only need to be done once
$ make start LOADTEST_FILES="<test-name>.toml <data-file>.toml <data-file>.toml"
$ make check_status
$ make stop                     # optional - if needed
$ make get_results OUTPUT_DIR=results/<YYYY-MM>/<test-name>
$ make publish_results OUTPUT_DIR=results/<YYYY-MM>/<test-name>
$ make destroy                  # don't forget to clean up the resources when you're done.
``` 

make init NUM_HOSTS=10
make plan
make apply
make list_hosts
make set_file_descriptors
make prep
make start LOADTEST_FILES="GetUserProductSubscriptions_ramp.toml user_ids_300k.toml ticket_product_ids_10k.toml"

For quick reference, here are the tests that are implemented:
```bash
$ make start LOADTEST_FILES="smol.toml channel_ids_5k.toml user_ids_50k.toml"
// TODO: add others here
```


Make sure to commit all result files to this repo.

### Results

#### February 2021

| Endpoint                         | Peak RPS Tested | Type                                   | Success Rate |
|----------------------------------|-----------------|----------------------------------------|--------------|
| GetChannelProducts               | 35k             | Burst (increased traffic over 5 mins)  | 100%         |
| GetChannelProducts               | 43.3k           | Ramp (increased traffic over 60 mins)  | 99.4%        |
| GetUserChannelSubscription       | 27.7k           | Burst                                  | 99.98%       |
| GetUserChannelSubscription       | 35k             | Ramp                                   | 100%         |
| GetUserProductSubscriptions      | 47k             | Burst                                  | 99.76%       |
| GetUserProductSubscriptions      | 66.95k          | Ramp                                   | 99.98%       |
| GetSubscriberBadge               | 19k             | Burst                                  | 99.9%        |
| GetSubscriberBadge               | N/A             | Ramp                                   | N/A          |

#### May 2020

| Endpoint                                        | Peak RPS Tested | Type                                   | Success Rate |
|-------------------------------------------------|-----------------|----------------------------------------|--------------|
| GetChannelProducts                              | 16.1k           | Burst (increased traffic over 5 mins)  | 100%         |
| GetChannelProducts                              | 27.5k           | Ramp (increased traffic over 60 mins)  | 99.85%       |
| GetUserChannelSubscription                      | 22.1k           | Burst                                  | 100%         |
| GetUserChannelSubscription                      | 28.5k           | Ramp                                   | 100%         |
| GetUserChannelSubscriptionWithPaymentsData      | 22.1k           | Burst                                  | 100%         |
| GetUserChannelSubscriptionWithPaymentsData      | 28.5k           | Ramp                                   | 100%         |
| GetUserProductSubscriptions                     | 30.3k           | Burst                                  | 100%         |
| GetUserProductSubscriptions                     | 47.6k           | Ramp                                   | 99.57%       |
| GetSubscriberBadge                              | 9.5k            | Burst                                  | 100%         |
| GetSubscriberBadge                              | 17.7k           | Ramp                                   | 100%         |

{{% children showhidden="true" %}}
