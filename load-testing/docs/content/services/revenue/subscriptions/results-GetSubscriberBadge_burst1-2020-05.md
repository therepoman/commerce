---
title: "Results GetSubscriberBadge_burst1 2020 05"
date: 2020-05-15T14:41:46-07:00
hidden: true
---


### Host 10.202.82.75

```
Requests      [total, rate]            713322, 786.97
Duration      [total, attack, wait]    15m6.448002655s, 15m6.414904157s, 33.098498ms
Latencies     [mean, 50, 95, 99, max]  5.426852ms, 2.160395ms, 25.153979ms, 77.939705ms, 316.761204ms
Bytes In      [total, mean]            14979762, 21.00
Bytes Out     [total, mean]            45900893, 64.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:713322  
Error Set:
```

