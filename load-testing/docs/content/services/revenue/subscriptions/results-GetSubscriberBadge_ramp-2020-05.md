---
title: "Results GetSubscriberBadge_ramp 2020 05"
date: 2020-05-15T17:28:53-07:00
hidden: true
---


### Host 10.202.82.198

```
Requests      [total, rate]            19629396, 3627.03
Duration      [total, attack, wait]    1h30m12.059309909s, 1h30m11.976507421s, 82.802488ms
Latencies     [mean, 50, 95, 99, max]  4.941817ms, 2.112527ms, 16.193824ms, 74.844837ms, 3.351317773s
Bytes In      [total, mean]            412217316, 21.00
Bytes Out     [total, mean]            1263117334, 64.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:19629396  
Error Set:
```

