---
title: "Results GetUserChannelSubscription_burst3 2020 05"
date: 2020-05-21T16:29:21-07:00
hidden: true
---


### Host 10.202.80.95

```
Requests      [total, rate, throughput]         2919149, 3202.80, 3202.56
Duration      [total, attack, wait]             15m12s, 15m11s, 67.302ms
Latencies     [min, mean, 50, 90, 95, 99, max]  530.376µs, 4.518ms, 1.642ms, 3.143ms, 20.651ms, 73.964ms, 3.323s
Bytes In      [total, mean]                     5838298, 2.00
Bytes Out     [total, mean]                     188331898, 64.52
Success       [ratio]                           100.00%
Status Codes  [code:count]                      200:2919149  
Error Set:
```

