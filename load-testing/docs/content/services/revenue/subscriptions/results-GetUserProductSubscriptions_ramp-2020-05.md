---
title: "Results GetUserProductSubscriptions_ramp 2020 05"
date: 2020-05-20T17:56:08-07:00
hidden: true
---


### Host 10.202.80.59

```
Requests      [total, rate, throughput]         54135583, 9018.44, 8715.24
Duration      [total, attack, wait]             1h43m0s, 1h40m0s, 3m2s
Latencies     [min, mean, 50, 90, 95, 99, max]  629.04µs, 554.944ms, 1.68ms, 4.237ms, 28.904ms, 101.773ms, 3m25s
Bytes In      [total, mean]                     1078085640, 19.91
Bytes Out     [total, mean]                     4897467495, 90.47
Success       [ratio]                           99.57%
Status Codes  [code:count]                      0:231301  200:53904282  
Error Set:
Post https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetUserProductSubscriptions: dial tcp 0.0.0.0:0->10.202.82.172:443: bind: address already in use
Post https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetUserProductSubscriptions: net/http: request canceled while waiting for connection (Client.Timeout exceeded while awaiting headers)
Post https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetUserProductSubscriptions: net/http: request canceled (Client.Timeout exceeded while awaiting headers)
Post https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetUserProductSubscriptions: dial tcp 0.0.0.0:0->10.202.82.113:443: bind: address already in use (Client.Timeout exceeded while awaiting headers)
Post https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetUserProductSubscriptions: http: server closed idle connection (Client.Timeout exceeded while awaiting headers)
Post https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetUserProductSubscriptions: EOF
Post https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetUserProductSubscriptions: dial tcp 0.0.0.0:0->10.202.82.19:443: bind: address already in use
Post https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetUserProductSubscriptions: write tcp 10.202.80.59:53056->10.202.82.19:443: write: broken pipe
Post https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetUserProductSubscriptions: dial tcp 0.0.0.0:0->10.202.82.86:443: bind: address already in use
Post https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetUserProductSubscriptions: http: server closed idle connection
Post https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetUserProductSubscriptions: dial tcp 0.0.0.0:0->10.202.80.100:443: bind: address already in use
Post https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetUserProductSubscriptions: write tcp 10.202.80.59:45585->10.202.80.80:443: write: broken pipe
Post https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetUserProductSubscriptions: write tcp 10.202.80.59:51953->10.202.80.80:443: write: broken pipe
Post https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetUserProductSubscriptions: dial tcp 0.0.0.0:0->10.202.81.91:443: bind: address already in use
Post https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetUserProductSubscriptions: dial tcp 0.0.0.0:0->10.202.81.95:443: bind: address already in use
```

