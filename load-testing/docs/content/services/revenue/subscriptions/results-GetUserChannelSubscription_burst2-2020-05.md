---
title: "Results GetUserChannelSubscription_burst2 2020 05"
date: 2020-05-21T16:29:26-07:00
hidden: true
---


### Host 10.202.80.95

```
Requests      [total, rate, throughput]         1946103, 2137.84, 2137.68
Duration      [total, attack, wait]             15m10s, 15m10s, 68.09ms
Latencies     [min, mean, 50, 90, 95, 99, max]  519.117µs, 4.696ms, 1.643ms, 3.199ms, 23.529ms, 75.084ms, 355.361ms
Bytes In      [total, mean]                     3892206, 2.00
Bytes Out     [total, mean]                     125554321, 64.52
Success       [ratio]                           100.00%
Status Codes  [code:count]                      200:1946103  
Error Set:
```

