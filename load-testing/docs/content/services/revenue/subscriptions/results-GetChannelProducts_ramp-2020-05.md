---
title: "Results GetChannelProducts_ramp 2020 05"
date: 2020-05-18T15:02:06-07:00
hidden: true
---


### Host 10.202.82.141

```
Requests      [total, rate]            32857767, 5631.39
Duration      [total, attack, wait]    1h37m14.82997028s, 1h37m14.748339814s, 81.630466ms
Latencies     [mean, 50, 95, 99, max]  478.52574ms, 1.727091ms, 49.199861ms, 24.362024773s, 1m29.341523305s
Bytes In      [total, mean]            13264790885, 403.70
Bytes Out     [total, mean]            2051127066, 62.42
Success       [ratio]                  99.85%
Status Codes  [code:count]             0:48262  200:32809505  
Error Set:
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.81.95:443: bind: address already in use
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.81.157:443: i/o timeout
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.81.221:443: i/o timeout
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.81.232:443: i/o timeout
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.81.39:443: i/o timeout
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.81.91:443: i/o timeout
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.81.237:443: i/o timeout
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.81.91:443: bind: address already in use
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.81.176:443: i/o timeout
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": net/http: timeout awaiting response headers
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.81.95:443: i/o timeout
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": EOF
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.81.157:443: bind: address already in use
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.82.172:443: bind: address already in use
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.80.211:443: bind: address already in use
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.80.230:443: bind: address already in use
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.80.230:443: i/o timeout
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.82.19:443: bind: address already in use
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.82.205:443: i/o timeout
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.82.86:443: i/o timeout
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.82.248:443: i/o timeout
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.82.208:443: i/o timeout
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.82.19:443: i/o timeout
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.82.113:443: i/o timeout
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.82.189:443: i/o timeout
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": dial tcp 0.0.0.0:0->10.202.82.172:443: i/o timeout
Post "https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetChannelProducts": net/http: TLS handshake timeout
```

