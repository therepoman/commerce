---
title: "Results GetUserChannelSubscriptionWithPaymentsData_burst1 2020 05"
date: 2020-05-21T10:31:27-07:00
hidden: true
---


### Host 10.202.80.145

```
Requests      [total, rate, throughput]         2514962, 2761.67, 2761.47
Duration      [total, attack, wait]             15m11s, 15m11s, 68.177ms
Latencies     [min, mean, 50, 90, 95, 99, max]  498.3µs, 4.541ms, 1.66ms, 3.122ms, 20.567ms, 74.082ms, 1.69s
Bytes In      [total, mean]                     5029924, 2.00
Bytes Out     [total, mean]                     162254258, 64.52
Success       [ratio]                           100.00%
Status Codes  [code:count]                      200:2514962  
Error Set:
```

