---
title: "Results GetUserChannelSubscription_ramp 2020 05"
date: 2020-05-26T11:48:01-07:00
hidden: true
---


### Host 10.202.80.32

```
Requests      [total, rate, throughput]         23283044, 4301.10, 4301.02
Duration      [total, attack, wait]             1h30m0s, 1h30m0s, 90.375ms
Latencies     [min, mean, 50, 90, 95, 99, max]  518.139µs, 4.626ms, 1.58ms, 3.173ms, 23.233ms, 74.712ms, 3.338s
Bytes In      [total, mean]                     46566088, 2.00
Bytes Out     [total, mean]                     1502133023, 64.52
Success       [ratio]                           100.00%
Status Codes  [code:count]                      200:23283044  
Error Set:
```

