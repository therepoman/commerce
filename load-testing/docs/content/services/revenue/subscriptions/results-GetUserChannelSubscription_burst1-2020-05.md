---
title: "Results GetUserChannelSubscription_burst1 2020 05"
date: 2020-05-21T16:29:32-07:00
hidden: true
---


### Host 10.202.80.95

```
Requests      [total, rate, throughput]         1459653, 1604.02, 1603.97
Duration      [total, attack, wait]             15m10s, 15m10s, 27.962ms
Latencies     [min, mean, 50, 90, 95, 99, max]  533.972µs, 5.084ms, 1.811ms, 3.712ms, 27.267ms, 76.456ms, 366.359ms
Bytes In      [total, mean]                     2919306, 2.00
Bytes Out     [total, mean]                     94171456, 64.52
Success       [ratio]                           100.00%
Status Codes  [code:count]                      200:1459653  
Error Set:
```

