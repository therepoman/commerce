---
title: "Results BatchGetPayoutType 2020 07_burst2"
date: 2020-07-02T10:49:42-07:00
hidden: true
---


### Host 10.201.36.102

```
Requests      [total, rate]            1247007, 1386.40
Duration      [total, attack, wait]    14m59.458629179s, 14m59.457822816s, 806.363µs
Latencies     [mean, 50, 95, 99, max]  1.484084ms, 1.469948ms, 2.292721ms, 2.440488ms, 1.676901594s
Bytes In      [total, mean]            96367093, 77.28
Bytes Out     [total, mean]            47820182, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1247007  
Error Set:
```

### Host 10.201.36.154

```
Requests      [total, rate]            1247007, 1386.50
Duration      [total, attack, wait]    14m59.394915041s, 14m59.393181516s, 1.733525ms
Latencies     [mean, 50, 95, 99, max]  1.349116ms, 1.268194ms, 1.723059ms, 2.305287ms, 872.290786ms
Bytes In      [total, mean]            96345229, 77.26
Bytes Out     [total, mean]            47820963, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1247007  
Error Set:
```

### Host 10.201.37.116

```
Requests      [total, rate]            1247007, 1386.94
Duration      [total, attack, wait]    14m59.105802627s, 14m59.104535543s, 1.267084ms
Latencies     [mean, 50, 95, 99, max]  1.028526ms, 906.866µs, 1.63822ms, 1.809765ms, 853.472943ms
Bytes In      [total, mean]            96365624, 77.28
Bytes Out     [total, mean]            47821226, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1247007  
Error Set:
```

### Host 10.201.37.153

```
Requests      [total, rate]            1247007, 1387.26
Duration      [total, attack, wait]    14m58.903278991s, 14m58.902231536s, 1.047455ms
Latencies     [mean, 50, 95, 99, max]  1.064172ms, 928.56µs, 1.649905ms, 1.890941ms, 1.680053284s
Bytes In      [total, mean]            96368703, 77.28
Bytes Out     [total, mean]            47820772, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1247007  
Error Set:
```

### Host 10.201.38.10

```
Requests      [total, rate]            1247007, 1387.24
Duration      [total, attack, wait]    14m58.913084318s, 14m58.912208958s, 875.36µs
Latencies     [mean, 50, 95, 99, max]  1.252308ms, 1.011498ms, 2.151179ms, 2.311336ms, 836.840228ms
Bytes In      [total, mean]            96335967, 77.25
Bytes Out     [total, mean]            47821371, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1247007  
Error Set:
```

### Host 10.201.38.203

```
Requests      [total, rate]            1247007, 1386.44
Duration      [total, attack, wait]    14m59.434406729s, 14m59.433543643s, 863.086µs
Latencies     [mean, 50, 95, 99, max]  1.345123ms, 1.243552ms, 2.279443ms, 2.429266ms, 644.769597ms
Bytes In      [total, mean]            96357603, 77.27
Bytes Out     [total, mean]            47819833, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1247007  
Error Set:
```

### Host 10.201.38.27

```
Requests      [total, rate]            1247007, 1386.65
Duration      [total, attack, wait]    14m59.298436554s, 14m59.297561371s, 875.183µs
Latencies     [mean, 50, 95, 99, max]  1.110158ms, 939.356µs, 1.723113ms, 2.239955ms, 1.721174978s
Bytes In      [total, mean]            96360838, 77.27
Bytes Out     [total, mean]            47819219, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1247007  
Error Set:
```

