---
title: "Results GetPayoutType 2020 06_ramp"
date: 2020-06-30T12:32:46-07:00
hidden: true
---


### Host 10.201.36.102

```
Requests      [total, rate]            2213748, 410.08
Duration      [total, attack, wait]    1h29m58.283348605s, 1h29m58.282121281s, 1.227324ms
Latencies     [mean, 50, 95, 99, max]  1.295065ms, 1.285662ms, 1.757699ms, 2.36697ms, 849.915763ms
Bytes In      [total, mean]            135366017, 61.15
Bytes Out     [total, mean]            73825163, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2213748  
Error Set:
```

### Host 10.201.36.154

```
Requests      [total, rate]            2213748, 410.08
Duration      [total, attack, wait]    1h29m58.272938941s, 1h29m58.271755119s, 1.183822ms
Latencies     [mean, 50, 95, 99, max]  1.278421ms, 1.266424ms, 1.713826ms, 2.024924ms, 1.478423846s
Bytes In      [total, mean]            135336285, 61.13
Bytes Out     [total, mean]            73825115, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2213748  
Error Set:
```

### Host 10.201.37.116

```
Requests      [total, rate]            2213748, 410.01
Duration      [total, attack, wait]    1h29m59.319292756s, 1h29m59.31847169s, 821.066µs
Latencies     [mean, 50, 95, 99, max]  1.235738ms, 1.108819ms, 1.961322ms, 2.132893ms, 1.671634207s
Bytes In      [total, mean]            135394587, 61.16
Bytes Out     [total, mean]            73822665, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2213748  
Error Set:
```

### Host 10.201.37.153

```
Requests      [total, rate]            2213748, 410.09
Duration      [total, attack, wait]    1h29m58.228219181s, 1h29m58.227399309s, 819.872µs
Latencies     [mean, 50, 95, 99, max]  1.072053ms, 957.649µs, 1.667921ms, 1.952058ms, 829.892111ms
Bytes In      [total, mean]            135389849, 61.16
Bytes Out     [total, mean]            73823066, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2213748  
Error Set:
```

### Host 10.201.38.10

```
Requests      [total, rate]            2213748, 410.09
Duration      [total, attack, wait]    1h29m58.242613078s, 1h29m58.241662474s, 950.604µs
Latencies     [mean, 50, 95, 99, max]  1.167938ms, 1.018969ms, 1.81455ms, 2.336094ms, 421.603308ms
Bytes In      [total, mean]            135401054, 61.16
Bytes Out     [total, mean]            73823566, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2213748  
Error Set:
```

### Host 10.201.38.203

```
Requests      [total, rate]            2213748, 410.07
Duration      [total, attack, wait]    1h29m58.425888007s, 1h29m58.424682606s, 1.205401ms
Latencies     [mean, 50, 95, 99, max]  1.159333ms, 992.557µs, 1.963545ms, 2.327031ms, 848.138561ms
Bytes In      [total, mean]            135356035, 61.14
Bytes Out     [total, mean]            73823581, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2213748  
Error Set:
```

### Host 10.201.38.27

```
Requests      [total, rate]            2213748, 410.08
Duration      [total, attack, wait]    1h29m58.294324302s, 1h29m58.293266143s, 1.058159ms
Latencies     [mean, 50, 95, 99, max]  1.322225ms, 1.155534ms, 2.230293ms, 2.425766ms, 1.666363064s
Bytes In      [total, mean]            135402414, 61.16
Bytes Out     [total, mean]            73824807, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2213748  
Error Set:
```

