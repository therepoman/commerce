---
title: "Results BatchGetPayoutType 2020 06_ramp"
date: 2020-06-30T16:30:56-07:00
hidden: true
---


### Host 10.201.36.102

```
Requests      [total, rate]            11086416, 2053.12
Duration      [total, attack, wait]    1h29m59.797260222s, 1h29m59.796141758s, 1.118464ms
Latencies     [mean, 50, 95, 99, max]  1.335898ms, 1.315473ms, 1.829397ms, 2.425934ms, 1.483285059s
Bytes In      [total, mean]            856528247, 77.26
Bytes Out     [total, mean]            425145096, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:11086416  
Error Set:
```

### Host 10.201.36.154

```
Requests      [total, rate]            11086416, 2053.25
Duration      [total, attack, wait]    1h29m59.444097095s, 1h29m59.443113563s, 983.532µs
Latencies     [mean, 50, 95, 99, max]  1.424318ms, 1.392474ms, 2.18001ms, 2.503764ms, 1.494187726s
Bytes In      [total, mean]            856658295, 77.27
Bytes Out     [total, mean]            425143600, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:11086416  
Error Set:
```

### Host 10.201.37.116

```
Requests      [total, rate]            11086416, 2053.28
Duration      [total, attack, wait]    1h29m59.376926863s, 1h29m59.375700853s, 1.22601ms
Latencies     [mean, 50, 95, 99, max]  1.211105ms, 1.056616ms, 1.992107ms, 2.299474ms, 1.459498711s
Bytes In      [total, mean]            856467454, 77.25
Bytes Out     [total, mean]            425143051, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:11086416  
Error Set:
```

### Host 10.201.37.153

```
Requests      [total, rate]            11086416, 2053.29
Duration      [total, attack, wait]    1h29m59.354848822s, 1h29m59.352950834s, 1.897988ms
Latencies     [mean, 50, 95, 99, max]  1.31822ms, 1.159843ms, 2.064584ms, 2.3801ms, 852.639716ms
Bytes In      [total, mean]            856610653, 77.27
Bytes Out     [total, mean]            425142789, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:11086416  
Error Set:
```

### Host 10.201.38.10

```
Requests      [total, rate]            11086416, 2053.18
Duration      [total, attack, wait]    1h29m59.635152872s, 1h29m59.634188834s, 964.038µs
Latencies     [mean, 50, 95, 99, max]  1.33484ms, 1.086881ms, 2.28767ms, 2.54726ms, 854.738094ms
Bytes In      [total, mean]            856715151, 77.28
Bytes Out     [total, mean]            425142131, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:11086416  
Error Set:
```

### Host 10.201.38.203

```
Requests      [total, rate]            11086416, 2053.38
Duration      [total, attack, wait]    1h29m59.117396682s, 1h29m59.116070942s, 1.32574ms
Latencies     [mean, 50, 95, 99, max]  1.36529ms, 1.220856ms, 2.314676ms, 2.59675ms, 858.58357ms
Bytes In      [total, mean]            856539991, 77.26
Bytes Out     [total, mean]            425140713, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:11086416  
Error Set:
```

### Host 10.201.38.27

```
Requests      [total, rate]            11086416, 2053.18
Duration      [total, attack, wait]    1h29m59.631604726s, 1h29m59.629730115s, 1.874611ms
Latencies     [mean, 50, 95, 99, max]  1.436346ms, 1.360638ms, 2.37024ms, 2.662285ms, 869.998474ms
Bytes In      [total, mean]            856780622, 77.28
Bytes Out     [total, mean]            425142479, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:11086416  
Error Set:
```

