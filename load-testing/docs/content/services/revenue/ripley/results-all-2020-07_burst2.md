---
title: "Results All 2020 07_burst2"
date: 2020-07-02T15:47:06-07:00
hidden: true
---


### Host 10.201.36.102

```
Requests      [total, rate]            1496262, 1664.51
Duration      [total, attack, wait]    14m58.924136064s, 14m58.922585284s, 1.55078ms
Latencies     [mean, 50, 95, 99, max]  1.333028ms, 1.287524ms, 1.789107ms, 2.362014ms, 1.667869107s
Bytes In      [total, mean]            111587239, 74.58
Bytes Out     [total, mean]            56131595, 37.51
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1496262  
Error Set:
```

### Host 10.201.36.154

```
Requests      [total, rate]            1496262, 1664.52
Duration      [total, attack, wait]    14m58.915569735s, 14m58.914410017s, 1.159718ms
Latencies     [mean, 50, 95, 99, max]  1.336387ms, 1.254911ms, 1.701875ms, 1.936929ms, 650.755909ms
Bytes In      [total, mean]            111559081, 74.56
Bytes Out     [total, mean]            56133374, 37.52
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1496262  
Error Set:
```

### Host 10.201.37.116

```
Requests      [total, rate]            1496262, 1664.05
Duration      [total, attack, wait]    14m59.172178074s, 14m59.171309787s, 868.287µs
Latencies     [mean, 50, 95, 99, max]  1.180108ms, 1.083407ms, 1.92077ms, 2.06877ms, 3.351319756s
Bytes In      [total, mean]            111640432, 74.61
Bytes Out     [total, mean]            56132115, 37.51
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1496262  
Error Set:
```

### Host 10.201.37.153

```
Requests      [total, rate]            1496262, 1664.02
Duration      [total, attack, wait]    14m59.183991574s, 14m59.182852476s, 1.139098ms
Latencies     [mean, 50, 95, 99, max]  1.090074ms, 937.663µs, 1.75281ms, 1.973964ms, 3.336986336s
Bytes In      [total, mean]            111579988, 74.57
Bytes Out     [total, mean]            56132008, 37.51
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1496262  
Error Set:
```

### Host 10.201.38.10

```
Requests      [total, rate]            1496262, 1663.73
Duration      [total, attack, wait]    14m59.345202696s, 14m59.344247376s, 955.32µs
Latencies     [mean, 50, 95, 99, max]  1.317951ms, 1.129941ms, 2.218085ms, 2.399307ms, 835.358998ms
Bytes In      [total, mean]            111589601, 74.58
Bytes Out     [total, mean]            56132021, 37.51
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1496262  
Error Set:
```

### Host 10.201.38.203

```
Requests      [total, rate]            1496262, 1655.50
Duration      [total, attack, wait]    15m3.816969388s, 15m3.815459941s, 1.509447ms
Latencies     [mean, 50, 95, 99, max]  1.428518ms, 1.430948ms, 2.255937ms, 2.405236ms, 6.847033595s
Bytes In      [total, mean]            111607774, 74.59
Bytes Out     [total, mean]            56133081, 37.52
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1496262  
Error Set:
```

### Host 10.201.38.27

```
Requests      [total, rate]            1496262, 1659.15
Duration      [total, attack, wait]    15m1.826418975s, 15m1.825174314s, 1.244661ms
Latencies     [mean, 50, 95, 99, max]  1.110336ms, 990.284µs, 1.617304ms, 2.144821ms, 3.125429141s
Bytes In      [total, mean]            111613654, 74.59
Bytes Out     [total, mean]            56131744, 37.51
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1496262  
Error Set:
```

