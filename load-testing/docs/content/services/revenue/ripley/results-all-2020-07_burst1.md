---
title: "Results All 2020 07_burst1"
date: 2020-07-02T15:16:18-07:00
hidden: true
---


### Host 10.201.36.102

```
Requests      [total, rate]            1122906, 1248.90
Duration      [total, attack, wait]    14m59.1179322s, 14m59.116469881s, 1.462319ms
Latencies     [mean, 50, 95, 99, max]  1.361501ms, 1.334354ms, 2.050003ms, 2.370326ms, 650.152043ms
Bytes In      [total, mean]            83780823, 74.61
Bytes Out     [total, mean]            42126518, 37.52
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1122906  
Error Set:
```

### Host 10.201.36.154

```
Requests      [total, rate]            1122906, 1249.33
Duration      [total, attack, wait]    14m58.804514984s, 14m58.803103248s, 1.411736ms
Latencies     [mean, 50, 95, 99, max]  1.382386ms, 1.392179ms, 2.150375ms, 2.318121ms, 857.366233ms
Bytes In      [total, mean]            83760133, 74.59
Bytes Out     [total, mean]            42124564, 37.51
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1122906  
Error Set:
```

### Host 10.201.37.116

```
Requests      [total, rate]            1122906, 1249.36
Duration      [total, attack, wait]    14m58.783015767s, 14m58.782188839s, 826.928µs
Latencies     [mean, 50, 95, 99, max]  1.324084ms, 1.240926ms, 1.981323ms, 2.116585ms, 1.736879973s
Bytes In      [total, mean]            83718001, 74.55
Bytes Out     [total, mean]            42125067, 37.51
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1122906  
Error Set:
```

### Host 10.201.37.153

```
Requests      [total, rate]            1122906, 1249.04
Duration      [total, attack, wait]    14m59.014697364s, 14m59.013038536s, 1.658828ms
Latencies     [mean, 50, 95, 99, max]  1.127474ms, 963.259µs, 1.824793ms, 2.050012ms, 3.326001353s
Bytes In      [total, mean]            83766951, 74.60
Bytes Out     [total, mean]            42126219, 37.52
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1122906  
Error Set:
```

### Host 10.201.38.10

```
Requests      [total, rate]            1122906, 1249.26
Duration      [total, attack, wait]    14m58.858823877s, 14m58.857468569s, 1.355308ms
Latencies     [mean, 50, 95, 99, max]  1.230722ms, 1.023283ms, 2.069403ms, 2.283754ms, 634.79709ms
Bytes In      [total, mean]            83745710, 74.58
Bytes Out     [total, mean]            42125548, 37.51
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1122906  
Error Set:
```

### Host 10.201.38.203

```
Requests      [total, rate]            1122906, 1244.30
Duration      [total, attack, wait]    15m2.441424484s, 15m2.440570788s, 853.696µs
Latencies     [mean, 50, 95, 99, max]  1.13475ms, 935.088µs, 1.945897ms, 2.295203ms, 3.404995608s
Bytes In      [total, mean]            83771831, 74.60
Bytes Out     [total, mean]            42125675, 37.51
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1122906  
Error Set:
```

### Host 10.201.38.27

```
Requests      [total, rate]            1122906, 1246.20
Duration      [total, attack, wait]    15m1.065209907s, 15m1.063658291s, 1.551616ms
Latencies     [mean, 50, 95, 99, max]  1.542094ms, 1.557008ms, 2.340678ms, 2.473122ms, 3.129136433s
Bytes In      [total, mean]            83727911, 74.56
Bytes Out     [total, mean]            42126068, 37.52
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1122906  
Error Set:
```

