---
title: "Results All 2020 07_burst3"
date: 2020-07-02T16:16:19-07:00
hidden: true
---


### Host 10.201.36.102

```
Requests      [total, rate]            2245506, 2497.44
Duration      [total, attack, wait]    14m59.12498134s, 14m59.1236488s, 1.33254ms
Latencies     [mean, 50, 95, 99, max]  1.550122ms, 1.520256ms, 2.31497ms, 2.492618ms, 1.685187921s
Bytes In      [total, mean]            167438870, 74.57
Bytes Out     [total, mean]            84239349, 37.51
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2245506  
Error Set:
```

### Host 10.201.36.154

```
Requests      [total, rate]            2245506, 2497.02
Duration      [total, attack, wait]    14m59.276785882s, 14m59.275634284s, 1.151598ms
Latencies     [mean, 50, 95, 99, max]  1.309096ms, 1.278561ms, 1.926715ms, 2.30209ms, 1.467633372s
Bytes In      [total, mean]            167486838, 74.59
Bytes Out     [total, mean]            84238658, 37.51
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2245506  
Error Set:
```

### Host 10.201.37.116

```
Requests      [total, rate]            2245506, 2496.47
Duration      [total, attack, wait]    14m59.473101444s, 14m59.471373648s, 1.727796ms
Latencies     [mean, 50, 95, 99, max]  1.11555ms, 950.222µs, 1.888119ms, 2.045424ms, 642.862081ms
Bytes In      [total, mean]            167454238, 74.57
Bytes Out     [total, mean]            84238434, 37.51
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2245506  
Error Set:
```

### Host 10.201.37.153

```
Requests      [total, rate]            2245506, 2434.50
Duration      [total, attack, wait]    15m22.369241619s, 15m22.368299827s, 941.792µs
Latencies     [mean, 50, 95, 99, max]  1.16985ms, 1.014012ms, 1.88902ms, 2.05315ms, 26.820813774s
Bytes In      [total, mean]            167459139, 74.58
Bytes Out     [total, mean]            84239087, 37.51
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2245506  
Error Set:
```

### Host 10.201.38.10

```
Requests      [total, rate]            2245506, 2496.88
Duration      [total, attack, wait]    14m59.326164554s, 14m59.324762165s, 1.402389ms
Latencies     [mean, 50, 95, 99, max]  1.198523ms, 1.011459ms, 2.005233ms, 2.268324ms, 836.423515ms
Bytes In      [total, mean]            167470990, 74.58
Bytes Out     [total, mean]            84239688, 37.51
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2245506  
Error Set:
```

### Host 10.201.38.203

```
Requests      [total, rate]            2245506, 2496.96
Duration      [total, attack, wait]    14m59.2977662s, 14m59.296188253s, 1.577947ms
Latencies     [mean, 50, 95, 99, max]  1.305473ms, 1.212036ms, 2.199172ms, 2.367994ms, 1.658431338s
Bytes In      [total, mean]            167469308, 74.58
Bytes Out     [total, mean]            84240031, 37.51
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2245506  
Error Set:
```

### Host 10.201.38.27

```
Requests      [total, rate]            2245506, 2496.67
Duration      [total, attack, wait]    14m59.400490757s, 14m59.399116388s, 1.374369ms
Latencies     [mean, 50, 95, 99, max]  1.359074ms, 1.280538ms, 2.250917ms, 2.389932ms, 1.667001155s
Bytes In      [total, mean]            167427960, 74.56
Bytes Out     [total, mean]            84236919, 37.51
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2245506  
Error Set:
```

