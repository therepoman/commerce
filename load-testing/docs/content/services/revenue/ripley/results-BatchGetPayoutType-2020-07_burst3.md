---
title: "Results BatchGetPayoutType 2020 07_burst3"
date: 2020-07-02T11:21:25-07:00
hidden: true
---


### Host 10.201.36.102

```
Requests      [total, rate]            1871253, 2079.70
Duration      [total, attack, wait]    14m59.770551437s, 14m59.768911017s, 1.64042ms
Latencies     [mean, 50, 95, 99, max]  1.353645ms, 1.301912ms, 1.891442ms, 2.351842ms, 847.840245ms
Bytes In      [total, mean]            144593801, 77.27
Bytes Out     [total, mean]            71759631, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1871253  
Error Set:
```

### Host 10.201.36.154

```
Requests      [total, rate]            1871253, 2079.04
Duration      [total, attack, wait]    15m0.058965885s, 15m0.057678783s, 1.287102ms
Latencies     [mean, 50, 95, 99, max]  1.378506ms, 1.388554ms, 2.223565ms, 2.383501ms, 856.913796ms
Bytes In      [total, mean]            144599405, 77.27
Bytes Out     [total, mean]            71759540, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1871253  
Error Set:
```

### Host 10.201.37.116

```
Requests      [total, rate]            1871253, 2081.35
Duration      [total, attack, wait]    14m59.059691528s, 14m59.057730295s, 1.961233ms
Latencies     [mean, 50, 95, 99, max]  1.179933ms, 1.064814ms, 1.896142ms, 2.059399ms, 1.688734084s
Bytes In      [total, mean]            144564753, 77.26
Bytes Out     [total, mean]            71758475, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1871253  
Error Set:
```

### Host 10.201.37.153

```
Requests      [total, rate]            1871253, 2080.54
Duration      [total, attack, wait]    14m59.408675858s, 14m59.407826555s, 849.303µs
Latencies     [mean, 50, 95, 99, max]  1.294768ms, 1.226654ms, 1.974412ms, 2.124188ms, 630.711324ms
Bytes In      [total, mean]            144558686, 77.25
Bytes Out     [total, mean]            71759963, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1871253  
Error Set:
```

### Host 10.201.38.10

```
Requests      [total, rate]            1871253, 2079.96
Duration      [total, attack, wait]    14m59.657907754s, 14m59.6567441s, 1.163654ms
Latencies     [mean, 50, 95, 99, max]  1.209784ms, 976.24µs, 2.114167ms, 2.388152ms, 835.799002ms
Bytes In      [total, mean]            144587076, 77.27
Bytes Out     [total, mean]            71758519, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1871253  
Error Set:
```

### Host 10.201.38.203

```
Requests      [total, rate]            1871253, 2080.84
Duration      [total, attack, wait]    14m59.281075806s, 14m59.279527415s, 1.548391ms
Latencies     [mean, 50, 95, 99, max]  1.411029ms, 1.403794ms, 2.263869ms, 2.410033ms, 1.724487798s
Bytes In      [total, mean]            144606312, 77.28
Bytes Out     [total, mean]            71758375, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1871253  
Error Set:
```

### Host 10.201.38.27

```
Requests      [total, rate]            1871253, 2080.42
Duration      [total, attack, wait]    14m59.459343374s, 14m59.458315903s, 1.027471ms
Latencies     [mean, 50, 95, 99, max]  1.332852ms, 1.212161ms, 2.239568ms, 2.408315ms, 829.676636ms
Bytes In      [total, mean]            144664256, 77.31
Bytes Out     [total, mean]            71759289, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1871253  
Error Set:
```

