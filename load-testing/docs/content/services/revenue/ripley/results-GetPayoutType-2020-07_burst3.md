---
title: "Results GetPayoutType 2020 07_burst3"
date: 2020-07-01T16:17:42-07:00
hidden: true
---


### Host 10.201.36.102

```
Requests      [total, rate]            374253, 416.68
Duration      [total, attack, wait]    14m58.169748518s, 14m58.168838763s, 909.755µs
Latencies     [mean, 50, 95, 99, max]  1.287934ms, 1.248463ms, 1.71337ms, 2.377201ms, 860.124275ms
Bytes In      [total, mean]            22899637, 61.19
Bytes Out     [total, mean]            12480628, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:374253  
Error Set:
```

### Host 10.201.36.154

```
Requests      [total, rate]            374253, 416.53
Duration      [total, attack, wait]    14m58.495503559s, 14m58.493785797s, 1.717762ms
Latencies     [mean, 50, 95, 99, max]  1.255336ms, 1.216263ms, 1.643207ms, 1.760423ms, 416.594684ms
Bytes In      [total, mean]            22894298, 61.17
Bytes Out     [total, mean]            12481401, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:374253  
Error Set:
```

### Host 10.201.37.116

```
Requests      [total, rate]            374253, 416.74
Duration      [total, attack, wait]    14m58.049562921s, 14m58.047863025s, 1.699896ms
Latencies     [mean, 50, 95, 99, max]  1.182548ms, 982.81µs, 1.787492ms, 2.018519ms, 826.440243ms
Bytes In      [total, mean]            22869089, 61.11
Bytes Out     [total, mean]            12480429, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:374253  
Error Set:
```

### Host 10.201.37.153

```
Requests      [total, rate]            374253, 416.73
Duration      [total, attack, wait]    14m58.065829673s, 14m58.064957795s, 871.878µs
Latencies     [mean, 50, 95, 99, max]  1.102231ms, 942.628µs, 1.69033ms, 1.828483ms, 423.688806ms
Bytes In      [total, mean]            22910345, 61.22
Bytes Out     [total, mean]            12480340, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:374253  
Error Set:
```

### Host 10.201.38.10

```
Requests      [total, rate]            374253, 416.75
Duration      [total, attack, wait]    14m58.034849225s, 14m58.03348771s, 1.361515ms
Latencies     [mean, 50, 95, 99, max]  1.057487ms, 947.037µs, 1.505798ms, 1.687186ms, 413.870785ms
Bytes In      [total, mean]            22893994, 61.17
Bytes Out     [total, mean]            12480403, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:374253  
Error Set:
```

### Host 10.201.38.203

```
Requests      [total, rate]            374253, 416.75
Duration      [total, attack, wait]    14m58.02819185s, 14m58.027378874s, 812.976µs
Latencies     [mean, 50, 95, 99, max]  1.085552ms, 874.86µs, 1.698236ms, 2.296891ms, 416.032558ms
Bytes In      [total, mean]            22889177, 61.16
Bytes Out     [total, mean]            12480599, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:374253  
Error Set:
```

### Host 10.201.38.27

```
Requests      [total, rate]            374253, 416.77
Duration      [total, attack, wait]    14m57.994546853s, 14m57.993152393s, 1.39446ms
Latencies     [mean, 50, 95, 99, max]  1.051703ms, 883.784µs, 1.628703ms, 1.738642ms, 415.192301ms
Bytes In      [total, mean]            22890640, 61.16
Bytes Out     [total, mean]            12480558, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:374253  
Error Set:
```

