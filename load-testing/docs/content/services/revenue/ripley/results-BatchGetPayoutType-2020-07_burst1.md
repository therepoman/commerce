---
title: "Results BatchGetPayoutType 2020 07_burst1"
date: 2020-07-02T10:16:40-07:00
hidden: true
---


### Host 10.201.36.102

```
Requests      [total, rate]            935703, 1040.64
Duration      [total, attack, wait]    14m59.1648683s, 14m59.163721982s, 1.146318ms
Latencies     [mean, 50, 95, 99, max]  1.279481ms, 1.249666ms, 1.698141ms, 1.947298ms, 428.644281ms
Bytes In      [total, mean]            72283549, 77.25
Bytes Out     [total, mean]            35881904, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:935703  
Error Set:
```

### Host 10.201.36.154

```
Requests      [total, rate]            935703, 1040.72
Duration      [total, attack, wait]    14m59.097082999s, 14m59.095333136s, 1.749863ms
Latencies     [mean, 50, 95, 99, max]  1.326194ms, 1.260175ms, 1.723585ms, 2.264356ms, 859.134514ms
Bytes In      [total, mean]            72312081, 77.28
Bytes Out     [total, mean]            35882359, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:935703  
Error Set:
```

### Host 10.201.37.116

```
Requests      [total, rate]            935703, 1040.53
Duration      [total, attack, wait]    14m59.253064878s, 14m59.252107117s, 957.761µs
Latencies     [mean, 50, 95, 99, max]  1.235715ms, 1.156239ms, 1.940767ms, 2.067582ms, 1.663829087s
Bytes In      [total, mean]            72301088, 77.27
Bytes Out     [total, mean]            35882382, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:935703  
Error Set:
```

### Host 10.201.37.153

```
Requests      [total, rate]            935703, 1039.78
Duration      [total, attack, wait]    14m59.904316688s, 14m59.903516332s, 800.356µs
Latencies     [mean, 50, 95, 99, max]  957.709µs, 853.501µs, 1.357042ms, 1.694899ms, 860.575159ms
Bytes In      [total, mean]            72291742, 77.26
Bytes Out     [total, mean]            35882886, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:935703  
Error Set:
```

### Host 10.201.38.10

```
Requests      [total, rate]            935703, 1040.39
Duration      [total, attack, wait]    14m59.382103694s, 14m59.381111979s, 991.715µs
Latencies     [mean, 50, 95, 99, max]  1.444182ms, 1.494514ms, 2.282208ms, 2.427127ms, 837.528151ms
Bytes In      [total, mean]            72283651, 77.25
Bytes Out     [total, mean]            35882536, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:935703  
Error Set:
```

### Host 10.201.38.203

```
Requests      [total, rate]            935703, 1041.01
Duration      [total, attack, wait]    14m58.844143051s, 14m58.842635231s, 1.50782ms
Latencies     [mean, 50, 95, 99, max]  1.071528ms, 900.163µs, 1.639456ms, 2.176032ms, 1.691644147s
Bytes In      [total, mean]            72338655, 77.31
Bytes Out     [total, mean]            35882454, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:935703  
Error Set:
```

### Host 10.201.38.27

```
Requests      [total, rate]            935703, 1039.06
Duration      [total, attack, wait]    15m0.52658058s, 15m0.525162469s, 1.418111ms
Latencies     [mean, 50, 95, 99, max]  1.093163ms, 942.172µs, 1.638029ms, 2.208465ms, 1.671016914s
Bytes In      [total, mean]            72318113, 77.29
Bytes Out     [total, mean]            35883501, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:935703  
Error Set:
```

