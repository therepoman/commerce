---
title: "Results GetPayoutType 2020 07_burst2"
date: 2020-07-01T15:48:59-07:00
hidden: true
---


### Host 10.201.36.102

```
Requests      [total, rate]            249255, 277.76
Duration      [total, attack, wait]    14m57.388869269s, 14m57.388177326s, 691.943µs
Latencies     [mean, 50, 95, 99, max]  1.248025ms, 1.248509ms, 1.675667ms, 1.882996ms, 649.825402ms
Bytes In      [total, mean]            15243018, 61.15
Bytes Out     [total, mean]            8312243, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:249255  
Error Set:
```

### Host 10.201.36.154

```
Requests      [total, rate]            249255, 277.75
Duration      [total, attack, wait]    14m57.400468702s, 14m57.399642196s, 826.506µs
Latencies     [mean, 50, 95, 99, max]  1.352998ms, 1.386365ms, 2.192708ms, 2.345639ms, 420.564648ms
Bytes In      [total, mean]            15249018, 61.18
Bytes Out     [total, mean]            8312297, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:249255  
Error Set:
```

### Host 10.201.37.116

```
Requests      [total, rate]            249255, 277.76
Duration      [total, attack, wait]    14m57.365862303s, 14m57.364925879s, 936.424µs
Latencies     [mean, 50, 95, 99, max]  1.201331ms, 1.050578ms, 1.892914ms, 2.058323ms, 413.972387ms
Bytes In      [total, mean]            15225544, 61.08
Bytes Out     [total, mean]            8312019, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:249255  
Error Set:
```

### Host 10.201.37.153

```
Requests      [total, rate]            249255, 277.76
Duration      [total, attack, wait]    14m57.372266393s, 14m57.370385604s, 1.880789ms
Latencies     [mean, 50, 95, 99, max]  1.311813ms, 1.242938ms, 1.997191ms, 2.12268ms, 418.388547ms
Bytes In      [total, mean]            15248234, 61.18
Bytes Out     [total, mean]            8312222, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:249255  
Error Set:
```

### Host 10.201.38.10

```
Requests      [total, rate]            249255, 277.68
Duration      [total, attack, wait]    14m57.629786594s, 14m57.627665104s, 2.12149ms
Latencies     [mean, 50, 95, 99, max]  1.427186ms, 1.456869ms, 2.192911ms, 2.326528ms, 416.07059ms
Bytes In      [total, mean]            15258858, 61.22
Bytes Out     [total, mean]            8312268, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:249255  
Error Set:
```

### Host 10.201.38.203

```
Requests      [total, rate]            249255, 277.70
Duration      [total, attack, wait]    14m57.573607068s, 14m57.572299838s, 1.30723ms
Latencies     [mean, 50, 95, 99, max]  1.473971ms, 1.420585ms, 2.21814ms, 2.351667ms, 212.794237ms
Bytes In      [total, mean]            15234525, 61.12
Bytes Out     [total, mean]            8312010, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:249255  
Error Set:
```

### Host 10.201.38.27

```
Requests      [total, rate]            249255, 277.77
Duration      [total, attack, wait]    14m57.35955068s, 14m57.358190572s, 1.360108ms
Latencies     [mean, 50, 95, 99, max]  1.07447ms, 976.316µs, 1.421892ms, 1.652591ms, 218.898462ms
Bytes In      [total, mean]            15239748, 61.14
Bytes Out     [total, mean]            8312527, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:249255  
Error Set:
```

