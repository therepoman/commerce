---
title: "Results GetPayoutType 2020 07_burst1"
date: 2020-07-01T15:18:58-07:00
hidden: true
---


### Host 10.201.36.102

```
Requests      [total, rate]            187203, 208.73
Duration      [total, attack, wait]    14m56.882313483s, 14m56.880885428s, 1.428055ms
Latencies     [mean, 50, 95, 99, max]  1.195437ms, 1.220812ms, 1.607589ms, 1.737411ms, 209.605003ms
Bytes In      [total, mean]            11458882, 61.21
Bytes Out     [total, mean]            6242605, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:187203  
Error Set:
```

### Host 10.201.36.154

```
Requests      [total, rate]            187203, 208.73
Duration      [total, attack, wait]    14m56.886736536s, 14m56.885582665s, 1.153871ms
Latencies     [mean, 50, 95, 99, max]  1.277214ms, 1.225788ms, 1.65508ms, 1.758852ms, 210.237725ms
Bytes In      [total, mean]            11437811, 61.10
Bytes Out     [total, mean]            6243063, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:187203  
Error Set:
```

### Host 10.201.37.116

```
Requests      [total, rate]            187203, 208.73
Duration      [total, attack, wait]    14m56.873250157s, 14m56.87164589s, 1.604267ms
Latencies     [mean, 50, 95, 99, max]  1.1511ms, 982.776µs, 1.754678ms, 1.881871ms, 212.238595ms
Bytes In      [total, mean]            11443766, 61.13
Bytes Out     [total, mean]            6243091, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:187203  
Error Set:
```

### Host 10.201.37.153

```
Requests      [total, rate]            187203, 208.72
Duration      [total, attack, wait]    14m56.91326784s, 14m56.912173512s, 1.094328ms
Latencies     [mean, 50, 95, 99, max]  1.266337ms, 1.198519ms, 1.941373ms, 2.068716ms, 207.23799ms
Bytes In      [total, mean]            11439719, 61.11
Bytes Out     [total, mean]            6242826, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:187203  
Error Set:
```

### Host 10.201.38.10

```
Requests      [total, rate]            187203, 208.73
Duration      [total, attack, wait]    14m56.854620411s, 14m56.853569069s, 1.051342ms
Latencies     [mean, 50, 95, 99, max]  1.132357ms, 1.0006ms, 1.698669ms, 1.976009ms, 209.313225ms
Bytes In      [total, mean]            11437198, 61.10
Bytes Out     [total, mean]            6243103, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:187203  
Error Set:
```

### Host 10.201.38.203

```
Requests      [total, rate]            187203, 208.74
Duration      [total, attack, wait]    14m56.837463894s, 14m56.836699394s, 764.5µs
Latencies     [mean, 50, 95, 99, max]  1.037563ms, 910.878µs, 1.580795ms, 1.704154ms, 210.134447ms
Bytes In      [total, mean]            11458770, 61.21
Bytes Out     [total, mean]            6242889, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:187203  
Error Set:
```

### Host 10.201.38.27

```
Requests      [total, rate]            187203, 208.73
Duration      [total, attack, wait]    14m56.85543897s, 14m56.854303425s, 1.135545ms
Latencies     [mean, 50, 95, 99, max]  1.103362ms, 978.407µs, 1.662043ms, 1.864404ms, 216.434982ms
Bytes In      [total, mean]            11445280, 61.14
Bytes Out     [total, mean]            6242500, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:187203  
Error Set:
```

