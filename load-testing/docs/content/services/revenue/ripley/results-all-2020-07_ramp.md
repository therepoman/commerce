---
title: "Results All 2020 07_ramp"
date: 2020-07-01T11:36:10-07:00
hidden: true
---


### Host 10.201.36.102

```
Requests      [total, rate]            13300164, 2463.36
Duration      [total, attack, wait]    1h29m59.195263137s, 1h29m59.194468496s, 794.641µs
Latencies     [mean, 50, 95, 99, max]  1.296584ms, 1.276384ms, 1.862244ms, 2.402386ms, 1.676873521s
Bytes In      [total, mean]            992011390, 74.59
Bytes Out     [total, mean]            498964015, 37.52
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:13300164  
Error Set:
```

### Host 10.201.36.154

```
Requests      [total, rate]            13300164, 2462.97
Duration      [total, attack, wait]    1h30m0.050249463s, 1h30m0.048610133s, 1.63933ms
Latencies     [mean, 50, 95, 99, max]  1.495958ms, 1.469313ms, 2.2368ms, 2.415069ms, 858.601344ms
Bytes In      [total, mean]            992062581, 74.59
Bytes Out     [total, mean]            498967908, 37.52
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:13300164  
Error Set:
```

### Host 10.201.37.116

```
Requests      [total, rate]            13300164, 2463.26
Duration      [total, attack, wait]    1h29m59.425745645s, 1h29m59.423726273s, 2.019372ms
Latencies     [mean, 50, 95, 99, max]  1.307534ms, 1.226345ms, 1.982137ms, 2.146273ms, 856.235615ms
Bytes In      [total, mean]            991895216, 74.58
Bytes Out     [total, mean]            498964742, 37.52
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:13300164  
Error Set:
```

### Host 10.201.37.153

```
Requests      [total, rate]            13300164, 2463.01
Duration      [total, attack, wait]    1h29m59.957307794s, 1h29m59.956173106s, 1.134688ms
Latencies     [mean, 50, 95, 99, max]  1.177433ms, 998.793µs, 1.816545ms, 2.047664ms, 838.085237ms
Bytes In      [total, mean]            991958059, 74.58
Bytes Out     [total, mean]            498966178, 37.52
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:13300164  
Error Set:
```

### Host 10.201.38.10

```
Requests      [total, rate]            13300164, 2463.41
Duration      [total, attack, wait]    1h29m59.096807751s, 1h29m59.095387799s, 1.419952ms
Latencies     [mean, 50, 95, 99, max]  1.332287ms, 1.11894ms, 2.290286ms, 2.495093ms, 843.995898ms
Bytes In      [total, mean]            991952909, 74.58
Bytes Out     [total, mean]            498963181, 37.52
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:13300164  
Error Set:
```

### Host 10.201.38.203

```
Requests      [total, rate]            13300164, 2463.31
Duration      [total, attack, wait]    1h29m59.304316221s, 1h29m59.302108874s, 2.207347ms
Latencies     [mean, 50, 95, 99, max]  1.54213ms, 1.556728ms, 2.289419ms, 2.450428ms, 667.135289ms
Bytes In      [total, mean]            991927973, 74.58
Bytes Out     [total, mean]            498964996, 37.52
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:13300164  
Error Set:
```

### Host 10.201.38.27

```
Requests      [total, rate]            13300164, 2463.02
Duration      [total, attack, wait]    1h29m59.949281231s, 1h29m59.948046324s, 1.234907ms
Latencies     [mean, 50, 95, 99, max]  1.370019ms, 1.266698ms, 2.299622ms, 2.513423ms, 1.687907441s
Bytes In      [total, mean]            991953757, 74.58
Bytes Out     [total, mean]            498965205, 37.52
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:13300164  
Error Set:
```

