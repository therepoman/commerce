---
title: "Ripley"
tags: ["money"]
---


### Links

- [Code](https://git.xarth.tv/revenue/ripley)
- [Runbook](https://docs.google.com/document/d/1zD5jL-B2FsCXbL3HqqgmR6ePQmSLspcwIkcDJAecQow/edit)
- [Dashboard](https://grafana.xarth.tv/d/000001593/ripley?orgId=1)


### Results

{{% children showhidden="true" %}}


## Traffic Limits
* 07/02/2020
    * All load tests passed with 100.00% success
    * Max prod traffic: 51996+ total rps 
    * Max spike traffic: 39999+ total rps for 10 minutes
    * Max increase rate: 2800+ rps per second

## Endpoints to Test
marked as essential in Jarvis https://jarvis.xarth.tv/availability/service/2020/w26?service_id=352&classification=Essential

1. GetPayoutType
    * Traffic patterns
        * Prod: 4332 rps
        * Peak: 6666 rps
        * Spike: 2334 rps
	* Info
	    * one channel id per request
        * in memory cache
        * queries dynamodb OnboardingStatusEvents table
            ```
           curl --request POST \
             --url https://prod.ripley.twitch.a2z.com/twirp/code.justin.tv.revenue.ripley.Ripley/GetPayoutType \
             --header 'client-id: ' \
             --header 'content-type: application/json' \
             --data '{
           	"channel_id":"95452353"
           }'
          ```

2. BatchGetPayoutType
	* Traffic patterns
	    * Prod: 21666 rps
        * Peak: 33333 rps
        * Spike: 11667 rps
    * Info
        * multiple channel ids per request, no data on how many
        * in memory cache
        * queries dynamodb OnboardingStatusEvents table
            ```
            curl --request POST \
              --url https://prod.ripley.twitch.a2z.com/twirp/code.justin.tv.revenue.ripley.Ripley/BatchGetPayoutType \
              --header 'client-id: ' \
              --header 'content-type: application/json' \
              --data '{
            	"channel_ids":[""]
            }'
          ```
          
###  Running Tests
1. First set up your aws command line to point to `twitch-revenue-aws` and export $AWS_PROFILE and $USERNAME
    ```
    export AWS_PROFILE=twitch-revenue-aws
    export USERNAME=$(whoami)
    ```
2. Run `make init NUM_HOSTS=7`
3. Run `make plan`
4. Run `make apply` if everything looks good
5. Run `make prep` to copy over all of mulan's load test plans and all datasets
6. Run tests
    
    To run individual test
    ```bash
    $ make start LOADTEST_FILES="GetPayoutType_ramp.toml channel_ids.toml"
    ```
    To run all ramp tests concurrently
    ```bash
    $ make start LOADTEST_FILES="*_ramp.toml channel_ids.toml"
    ```
    To run all burst1 tests concurrently
    ```bash
    $ make start LOADTEST_FILES="*_burst1.toml channel_ids.toml"
    ```
    To run all burst2 tests concurrently
    ```bash
    $ make start LOADTEST_FILES="*_burst2.toml channel_ids.toml"
    ```
    To run all burst3 tests concurrently
    ```bash
    $ make start LOADTEST_FILES="*_burst3.toml channel_ids.toml"
    ```
7. Run `make check_status` to check if the test is still running
    a. If you need to prematurely stop the tests, run `make stop`
8. Run `make get_results OUTPUT_DIR=results/<test-name>/<YYYY-MM>`
9. Run `make publish_results RESULTS_DIR=results/<test-name>/<YYYY-MM>`
10. Run `make destroy` to clean up the instances you spun up

### Dashboard/Graphs to monitor
* Ripley Grafana https://grafana.xarth.tv/d/000001593/ripley?orgId=1&from=now-1h&to=now
