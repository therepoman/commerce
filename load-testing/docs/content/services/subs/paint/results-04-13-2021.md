---
title: "Results: 04-13-2021"
date: 2021-04-13T17:55:51-07:00
hidden: true
---

### Test parameters
The load test for AnimateEmote in production. 

```
make start LOADTEST_FILES="AnimateEmote.toml prod-emote-list.toml"
```

#### AnimateEmote
The tests were run for 1 hour mins at 6TPS, which is roughly 6 times the CreateEmoticon API in Mako. This was to simulate creators testing out each animation type when AnimatedEmotes launches.

The emote list was created from existing prod emotes. To ensure we did not use actual emotes in this test, we copied the emotes to the same bucket and renamed them for the load test using the aws CLI.

```"aws s3 cp s3://prod-emoticon-uploads/emoticons/{some_key} s3://prod-emoticon-uploads/emoticons/{some_key}-loadtest"```

The purpose of the load test was to ensure we are properly scaled to handle launch of AnimatedEmotes. 

There are no downstream dependencies that needed scaling as the API simply kicks off a step function. The step function has two lambdas:
1) The first fetches the emote from S3, generates the small/medium/large animated emotes using the given operation, and uploads the results back to S3. 
2) The second simply fires off a pubsub message with the URLs of the three generated emotes and a corresponding id. 

We only need to ensure our Lambda concurrency limits are high enough to handle this TPS.

## Results

### Success Rate

API success rate was 100%. There were no failed API requests. 

!["AnimateEmote_APIRequests"](../../../../images/subs/paint/AnimateEmoteAPIRequests-04-13-2021.png)


There were 21600 successful step function executions and 0 failures. 

!["AnimateEmote_successRate"](../../../../images/subs/paint/AnimateEmoteStepFunctionRate-04-13-2021.png)


### Latency

#### S3 Latency

S3 latency was consistent throughout the test and was not a major contributing factor to overall latency. 

!["AnimateEmote_successRate"](../../../../images/subs/paint/AnimateEmoteS3Latency-04-13-2021.png)

#### Template Latency:

Template latency was consistent throughout the test. The templates that take the longest to complete are SLIDEIN, SLIDEOUT, and SPIN. 
This is because the number of operations Imagemagick has to complete is higher than other templates. These gifs result in larger files with more frames. 

!["AnimateEmote_successRate"](../../../../images/subs/paint/AnimateEmoteTemplateLatency-04-13-2021.png)

There were improvements checked in for this load test that parallelized generating and optimizing the different gif sizes. 
You can see in the image below that the first set of latencies, without the parallelization, is significantly higher than the second set of latencies which is from this load test.
This confirms our optimizations improved overall latency, which also reducing file size. 
Optimizations can be found in [this Pull Request](https://git.xarth.tv/subs/paint/pull/161). 

!["AnimateEmote_successRate"](../../../../images/subs/paint/AnimateEmoteTemplateLatencyComparison-04-13-2021.png)

### Lambda Concurrency

We hit 44 total concurrent lambda executions during the test. The current reserved capacity is 200 for each lambda in the AnimateEmote step function. 
We would need to see 18x the traffic of the CreateEmoticon API in Mako for us to hit the concurrency limit. 

!["AnimateEmote_lambdaconcurrency"](../../../../images/subs/paint/AnimateEmoteLambdaConcurrency-04-13-2021.png)

### Conclusion
Our API, step function, and Lambdas are well scaled to handle launch. Our latencies and output file sizes have improved since this was initially implemented.   

### Raw Results

#### Host 10.201.96.239

```
Requests      [total, rate]            21600, 6.01
Duration      [total, attack, wait]    59m51.745331086s, 59m51.664003861s, 81.327225ms
Latencies     [mean, 50, 95, 99, max]  85.684309ms, 76.697272ms, 129.490319ms, 241.165162ms, 4.745672381s
Bytes In      [total, mean]            7257272, 335.98
Bytes Out     [total, mean]            2273627, 105.26
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:21600  
Error Set:
```

