---
title: "Results: 03-18-2021"
date: 2021-T14:01:37-07:00
hidden: true
---

### Test parameters
The load tests for AnimateEmote in staging. 

#### AnimateEmote
The tests were run for 30 mins at 6TPS, which is roughly 6 times the CreateEmoticon emote in Mako. This was to simulate creators testing out each animation type when AnimatedEmotes launches.

The emote-list.toml file was created by querying the staging-emoticon-uploads/emoticons s3 bucket for any emote that was 4x size. 

The purpose of the load test was see if batch uploads to S3, or using `dispose: background` instead of `dispose: previous` during the animation would improve lambda execution time. 

## Results

### Latency

#### AnimateEmote Latency:
Latencies for uploading to S3 were consistent for both load tests. No improvements were made using the batch upload method compared to the current synchronous upload for all three emote sizes. 

Master branch:
!["AnimateEmote_latency_normal"](../../../../images/subs/paint/AnimateEmoteUploadAndAnimateLatencyNormal.png)

With changes:
!["AnimateEmote_latency_normal"](../../../../images/subs/paint/AnimateEmoteUploadAndAnimateLatencyWithChanges.png)

### Conclusion
We can see a small decrease in execution time for the animations when using `dispose: background`. It is not significant but still an improvement. 
There is no noticeable improvement to using batch uploads to s3 for this particular case. 

