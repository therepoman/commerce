---
title: "Paint"
tags: ["subs", "Paint", "AnimatedEmotes", "emotes", "animate"]
---


### Links

- [Code](https://git.xarth.tv/subs/paint)


###  Running

#### AnimateEmote

The emote list is created from existing prod emotes. To ensure we do not use actual emotes in this test, we will copy the emotes to the same bucket and rename them for the load test using a Makefile command.
You MUST generate a new list using this command as files  under `s3://prod-emoticon-uploads/emoticons/` have a lifecycle rule of 7 days.

```make data```

`make data` will generate a csv of emotes at `prod-emote-intermediate-list.csv`, 
then use that list to copy those files into `s3://prod-emoticon-uploads/emoticons/` but with loadtest appended to the file name.
The emotes in `prod-emote-list.csv` will then need to be copied over manually to `prod-emote-list.toml`.

To run the load test, use the following command. 

```
make start LOADTEST_FILES="AnimateEmote.toml prod-emote-list.toml"
```

### Results
{{% children showhidden="true" %}}

