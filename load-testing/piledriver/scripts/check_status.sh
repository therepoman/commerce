#!/bin/bash

loadTestIPs=($($(dirname "${BASH_SOURCE[0]}")/list-hosts.sh))

for i in "${loadTestIPs[@]}"
do
    (TC=$TC ssh -t "${USERNAME}@${i}" ./check-load-test-status.sh && echo -e "\033[92mLoad test still running on instance: ${i} \033[0m") || echo -e "\033[31mLoad test no longer running on instance: ${i} \033[0m"
done