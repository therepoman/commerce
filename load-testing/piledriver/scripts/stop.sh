#!/bin/bash

loadTestIPs=($($(dirname "${BASH_SOURCE[0]}")/list-hosts.sh))

for i in "${loadTestIPs[@]}"
do
    TC=$TC ssh -t "${USERNAME}@${i}" ./stop-load-test.sh
    retValue=$?
    if [ $retValue == 0 ]
    then
      echo -e "\033[92mSuccessfully stopped load test on instance: ${i} \033[0m"
    elif [ $retValue == 126 ]
    then
      echo -e "\033[31mLoad test was not running on instance: ${i} \033[0m"
    else
      echo -e "\033[31mFailed to stop load test on instance: ${i} \033[0m"
    fi
done