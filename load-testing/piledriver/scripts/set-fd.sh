#!/bin/bash

loadTestIPs=($($(dirname "${BASH_SOURCE[0]}")/list-hosts.sh))

for i in "${loadTestIPs[@]}"
do
    echo "Setting file descriptors on ${i}..."
    TC=$TC ssh -t "${USERNAME}@${i}" "echo '*  soft   nofile 80000' | sudo tee /etc/security/limits.conf"
    TC=$TC ssh -t "${USERNAME}@${i}" "echo '*  hard   nofile 80000' | sudo tee -a /etc/security/limits.conf"
done