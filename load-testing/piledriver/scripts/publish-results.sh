#!/bin/bash

# This script post-processes the result files into a .md file that can be put in the docs section
# Make sure Hugo is installed to generate the documentation.
if [[ -z "$(command -v hugo)" ]]; then
    echo "ERROR: hugo is not installed, please run 'brew install hugo' and try again"
    exit 1
fi
if [[ -z "$1" ]]; then
    echo "ERROR: you must specify the repository path to publish to"
    exit 1
fi

pwd

baseDir=${GOPATH}/src/code.justin.tv/commerce/load-testing
resultsDir=${RESULTS_DIR:-loadtest-results}
repo=$1
docfilename=$(echo $resultsDir | sed -e 's|/|-|g')


# to support re-generating the page, delete it if it exists (otherwise Hugo will complain)
resultFile=$(find "$baseDir/docs" -name "$docfilename.md")
if [[ -f "$resultFile" ]]; then
    rm "$resultFile"
fi

# Generate the page
(cd $baseDir/docs && hugo new --kind testresults "services/$repo/$docfilename.md")

# Dump the content of the results into the page.
resultFile=$(find "$baseDir/docs" -name "$docfilename.md")

echo "" >> $resultFile

for file in $(ls $resultsDir)
do
    server="${file%.*}"
    echo "### Host $server" >> $resultFile
    echo "" >> $resultFile
    echo "\`\`\`" >> $resultFile
    cat "$resultsDir/$file" >> $resultFile
    echo "\`\`\`" >> $resultFile
    echo "" >> $resultFile
done

