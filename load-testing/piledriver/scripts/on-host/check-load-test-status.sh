#!/bin/bash

# Check that process is running before killing
if [[ $(ps wx | grep "attack" | grep -v grep | cut -f 2 -d " ") ]]; then
    exit 0
else
    exit 1
fi
