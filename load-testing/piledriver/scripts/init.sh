#!/bin/bash

if [[ -z "$USERNAME" ]]; then
    USERNAME=$(whoami)
    echo "Defaulting to user $USERNAME"
fi
if [[ -z "$NUM_HOSTS" ]]; then
    NUM_HOSTS=1
    echo "Defaulting to $NUM_HOSTS hosts"
fi

outdir="_run"

rm -rf $outdir
mkdir $outdir

for arg in $@
do
    if [[ -f "$arg" ]]; then
        cat "$arg" | \
        sed -e "s|_USERNAME_|$USERNAME|g" \
            -e "s|_NUM_HOSTS_|$NUM_HOSTS|g" \
         > $outdir/$arg
    else
        echo "$arg is not a file"
    fi
done


