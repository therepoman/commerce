#!/bin/bash

loadTestIPs=($($(dirname "${BASH_SOURCE[0]}")/list-hosts.sh))

for i in "${loadTestIPs[@]}"
do
    (TC=$TC ssh -t "${USERNAME}@${i}" nohup ./run-load-test.sh "${LOADTEST_FILES}" && echo -e "\033[92mSuccessfully started load test on instance: ${i} \033[0m") || echo -e "\033[31mFailed to start load test on instance: ${i} \033[0m"
done