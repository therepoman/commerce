#!/bin/bash

# support for copying all .toml files from the service's directory.
if [[ $# -lt 1 ]] || [[ ! -d "$1" ]]; then
    echo "ERROR: you need to specify the absolute path to your service's load-test folder"
    exit 1
fi

loadTestIPs=($($(dirname "${BASH_SOURCE[0]}")/list-hosts.sh))

for i in "${loadTestIPs[@]}"
do
    (echo "rsyncing to ${i}..." \
    && TC=$TC rsync -P -c $1/*.toml "${USERNAME}@${i}:." \
    && TC=$TC rsync -P -c $GOPATH/src/code.justin.tv/commerce/load-testing/datasets/*.* "${USERNAME}@${i}:." \
    && echo -e "\033[92mSuccessfully synced all load test plan files to instance: ${i} \033[0m") || echo -e "\033[31mFailed to sync all load test plan files to instance: ${i} \033[0m"
done
