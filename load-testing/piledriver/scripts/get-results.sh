#!/bin/bash

outputDir=${OUTPUT_DIR:-loadtest-results}
loadTestIPs=($($(dirname "${BASH_SOURCE[0]}")/list-hosts.sh))

mkdir -p $outputDir

# TODO: write a new script that post-processes the result files into a .md file that can be put in the docs section

for i in "${loadTestIPs[@]}"
do
    (echo "Getting results from to ${i}..." \
    && TC=$TC scp "${USERNAME}@${i}:test.out" "${outputDir}/${i}.txt" \
    && echo -e "\033[92mSuccessfully copied load test results from instance: ${i} \033[0m") || echo -e "\033[31mFailed to load test results from instance: ${i} \033[0m"
done