#!/bin/bash

loadTestIPs=($($(dirname "${BASH_SOURCE[0]}")/list-hosts.sh))

for i in "${loadTestIPs[@]}"
do
    TC=$TC ssh -n ${USERNAME}@${i} 'tail -f test.out' &
done
