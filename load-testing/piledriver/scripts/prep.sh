#!/bin/bash

# support for copying all .toml files from the service's directory.
if [[ $# -lt 1 ]] || [[ ! -d "$1" ]]; then
    echo "ERROR: you need to specify the absolute path to your service's load-test folder"
    exit 1
fi

loadTestIPs=($($(dirname "${BASH_SOURCE[0]}")/list-hosts.sh))

for i in "${loadTestIPs[@]}"
do
    (echo "COPYing to ${i}..." \
    && echo "copying test plans" \
    && tar -C $1 -czf - *.toml | TC=$TC ssh "${USERNAME}@${i}" "tar zxf -"  \
    && echo "copying binary" \
    && tar -C $GOPATH/src/code.justin.tv/commerce/nebbiolo -czf - n | TC=$TC ssh "${USERNAME}@${i}" "tar zxf - && mv n attack" \
    && echo "copying datasets" \
    && tar -C $GOPATH/src/code.justin.tv/commerce/load-testing/datasets -czf - . | TC=$TC ssh "${USERNAME}@${i}" "tar zxf -" \
    && echo "copying scripts" \
    && tar -C $GOPATH/src/code.justin.tv/commerce/load-testing/piledriver/scripts/on-host -czf - . | TC=$TC ssh "${USERNAME}@${i}" "tar zxf -" \
    && echo -e "\033[92mSuccessfully copied all load test files to instance: ${i} \033[0m") || echo -e "\033[31mFailed to copy all load test files to instance: ${i} \033[0m"
done
