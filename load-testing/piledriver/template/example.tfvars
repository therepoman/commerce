aws_profile = "your aws profile name goes here - this is typically the AWS account name"

max_hosts = _NUM_HOSTS_

min_hosts = _NUM_HOSTS_

username = "_USERNAME_"

public_key_path = "~/.ssh/id_rsa.pub"

vpc_id = "your vpc ID goes here"

subnets = "your list of subnets goes here"

security_group = "your security group goes here"
