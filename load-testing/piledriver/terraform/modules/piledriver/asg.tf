resource aws_autoscaling_group "main" {
  name = "${var.username}-load-testing"

  min_size         = "${var.min_hosts}"
  desired_capacity = "${var.min_hosts}"
  max_size         = "${var.max_hosts}"

  vpc_zone_identifier = ["${split(",", var.subnets)}"]

  launch_template {
    id      = "${aws_launch_template.main.id}"
    version = "$Latest"
  }

  tag {
    key                 = "Name"
    propagate_at_launch = true
    value               = "${var.username}-load-testing"
  }
}
