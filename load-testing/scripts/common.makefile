# -------------------------------------
PILEDRIVER=$(GOPATH)/src/code.justin.tv/commerce/load-testing/piledriver
RUNDIR=$(CURDIR)/_run


# OPTIONAL: Specify NUM_HOSTS and/or USERNAME as command line arguments when executing this.
init: $(TF_VARS)
	$(PILEDRIVER)/scripts/init.sh $^
	@cd $(PILEDRIVER)/terraform && terraform init
.PHONY: init


plan: $(RUNDIR)/$(TF_VARS)
	@cd $(PILEDRIVER)/terraform && terraform plan \
		-var-file=$^ \
		-out loadtest.plan
.PHONY: plan

apply:
	@cd $(PILEDRIVER)/terraform && terraform apply loadtest.plan
.PHONY: apply

destroy: $(RUNDIR)/$(TF_VARS)
	@cd $(PILEDRIVER)/terraform && terraform destroy -var-file=$^
.PHONY: destroy

set_file_descriptors:
	$(PILEDRIVER)/scripts/set-fd.sh

list_hosts:
	$(PILEDRIVER)/scripts/list-hosts.sh

# Specify OUTPUT_DIR as a command line argument when executing this.
get_results:
	$(PILEDRIVER)/scripts/get-results.sh

# Specify RESULTS_DIR as a command line argument when executing this.
publish_results:
	$(PILEDRIVER)/scripts/publish-results.sh $(REPO)

build_nebbiolo:
	go get code.justin.tv/commerce/nebbiolo || true
	cd $(GOPATH)/src/code.justin.tv/commerce/nebbiolo; \
		env GOOS=linux GOARCH=amd64 go build -o n cmd/nebbiolo/main.go

# Specify LOADTEST_FILES as a command line argument when executing this.
start:
	$(PILEDRIVER)/scripts/start.sh

stop:
	$(PILEDRIVER)/scripts/stop.sh

prep: build_nebbiolo
	$(PILEDRIVER)/scripts/prep.sh $(CURDIR)

check_status:
	$(PILEDRIVER)/scripts/check_status.sh

sync:
	$(PILEDRIVER)/scripts/sync.sh $(CURDIR)

# Tail output files
# To stop tailing, use fg to bring one process to foreground and ctrl+c to terminate that process
# (or use ps aux | grep ssh to get pid and kill it this way)
tail:
	$(PILEDRIVER)/scripts/tail.sh
