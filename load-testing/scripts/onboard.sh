#!/bin/bash

# check the environment and parameters
if [[ -z "$(command -v hugo)" ]]; then
    echo "ERROR: Please install hugo with 'brew install hugo' and try again"
    exit 1
fi
if [[ -z "$1" ]]; then
    echo "ERROR: repository name is required"
    exit 1
fi


set -e

# Create the files and folders
repo=$1
mkdir -p load-tests/$repo
(cd docs && hugo new --kind service "services/$repo/_index.md")

# Copy over and update the template files.
tfvarsfile=$(echo "$repo" | sed -e 's|/|-|g')
cat piledriver/template/Makefile | sed -e "s|_REPO_|$repo|g" -e "s|_TFVARS_FILE_|$tfvarsfile.tfvars|g" > load-tests/$repo/Makefile
cat piledriver/template/example.tfvars > load-tests/$repo/$tfvarsfile.tfvars

ls -1 load-tests/$repo/*

echo "Done"