#!/bin/bash

function usage()
{
    echo "USAGE: ./`basename $0` [options] <input file> [<input file>...]"
    echo "  This script generates a .toml suitable for use with https://git.xarth.tv/commerce/nebbiolo"
    echo "  It requires one or more input files - currently it only supports one string variable per line"
    echo "OPTIONS:"
    echo "  -o <filename>   - Set the name of the output file"
    echo "  -n <name>       - The to give this data set"
    exit 1
}

output="data.toml"
name="ids"
while getopts "o:n:h" opt; do
    case $opt in
        o) output=$OPTARG ;;
        n) name=$OPTARG ;;
        h) usage ;;
        *) usage ;;
    esac
done
shift $((OPTIND - 1))


echo "[data]" > $output
echo "$name = [" >> $output

for arg in $@
do
    if [[ -f "$arg" ]]; then
        while read -r line
        do
            echo "  \"$line\"," >> $output
        done < "$arg"
    fi
done

echo "]" >> $output

echo "Done - wrote to $output"
