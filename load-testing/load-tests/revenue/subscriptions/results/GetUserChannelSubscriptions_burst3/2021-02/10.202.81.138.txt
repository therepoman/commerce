Requests      [total, rate]            2691003, 4439.32
Duration      [total, attack, wait]    10m6.186281151s, 10m6.173919008s, 12.362143ms
Latencies     [mean, 50, 95, 99, max]  3.117786ms, 2.241265ms, 4.529805ms, 30.391183ms, 377.416771ms
Bytes In      [total, mean]            56513803, 21.00
Bytes Out     [total, mean]            172121289, 63.96
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2690996  400:7  
Error Set:
400 Bad Request
