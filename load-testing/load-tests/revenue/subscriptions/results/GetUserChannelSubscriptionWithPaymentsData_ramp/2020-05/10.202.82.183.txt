Requests      [total, rate, throughput]         29848985, 5515.10, 5515.01
Duration      [total, attack, wait]             1h30m0s, 1h30m0s, 97.398ms
Latencies     [min, mean, 50, 90, 95, 99, max]  475.409µs, 4.191ms, 1.606ms, 3.019ms, 15.134ms, 70.853ms, 404.432ms
Bytes In      [total, mean]                     59697968, 2.00
Bytes Out     [total, mean]                     1925740021, 64.52
Success       [ratio]                           100.00%
Status Codes  [code:count]                      0:1  200:29848984  
Error Set:
Post https://main.us-west-2.prod.subscriptions.twitch.a2z.com/twirp/code.justin.tv.revenue.subscriptions.Subscriptions/GetUserChannelSubscriptionWithPaymentsData: EOF
