aws_profile = "twitch-subs-aws"

max_hosts = _NUM_HOSTS_

username = "_USERNAME_"

public_key_path = "~/.ssh/id_rsa.pub"

vpc_id = "vpc-cc8ee3aa"

subnets = "subnet-3cf74a5a,subnet-955685dd,subnet-7c3a7c27"

security_group = "sg-1f3ac262"
