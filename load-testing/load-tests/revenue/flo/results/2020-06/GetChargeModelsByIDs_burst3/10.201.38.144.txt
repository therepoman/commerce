Requests      [total, rate]            50154, 56.32
Duration      [total, attack, wait]    14m50.460546342s, 14m50.458266806s, 2.279536ms
Latencies     [mean, 50, 95, 99, max]  1.996908ms, 2.03676ms, 2.784628ms, 3.428005ms, 43.730333ms
Bytes In      [total, mean]            1003080, 20.00
Bytes Out     [total, mean]            2808624, 56.00
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:50154  
Error Set:
