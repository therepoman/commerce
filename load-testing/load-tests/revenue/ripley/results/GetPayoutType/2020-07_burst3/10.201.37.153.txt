Requests      [total, rate]            374253, 416.73
Duration      [total, attack, wait]    14m58.065829673s, 14m58.064957795s, 871.878µs
Latencies     [mean, 50, 95, 99, max]  1.102231ms, 942.628µs, 1.69033ms, 1.828483ms, 423.688806ms
Bytes In      [total, mean]            22910345, 61.22
Bytes Out     [total, mean]            12480340, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:374253  
Error Set:
