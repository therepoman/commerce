Requests      [total, rate]            374253, 416.77
Duration      [total, attack, wait]    14m57.994546853s, 14m57.993152393s, 1.39446ms
Latencies     [mean, 50, 95, 99, max]  1.051703ms, 883.784µs, 1.628703ms, 1.738642ms, 415.192301ms
Bytes In      [total, mean]            22890640, 61.16
Bytes Out     [total, mean]            12480558, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:374253  
Error Set:
