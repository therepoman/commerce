Requests      [total, rate]            187203, 208.73
Duration      [total, attack, wait]    14m56.854620411s, 14m56.853569069s, 1.051342ms
Latencies     [mean, 50, 95, 99, max]  1.132357ms, 1.0006ms, 1.698669ms, 1.976009ms, 209.313225ms
Bytes In      [total, mean]            11437198, 61.10
Bytes Out     [total, mean]            6243103, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:187203  
Error Set:
