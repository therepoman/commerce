Requests      [total, rate]            187203, 208.73
Duration      [total, attack, wait]    14m56.886736536s, 14m56.885582665s, 1.153871ms
Latencies     [mean, 50, 95, 99, max]  1.277214ms, 1.225788ms, 1.65508ms, 1.758852ms, 210.237725ms
Bytes In      [total, mean]            11437811, 61.10
Bytes Out     [total, mean]            6243063, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:187203  
Error Set:
