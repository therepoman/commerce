Requests      [total, rate]            2213748, 410.09
Duration      [total, attack, wait]    1h29m58.242613078s, 1h29m58.241662474s, 950.604µs
Latencies     [mean, 50, 95, 99, max]  1.167938ms, 1.018969ms, 1.81455ms, 2.336094ms, 421.603308ms
Bytes In      [total, mean]            135401054, 61.16
Bytes Out     [total, mean]            73823566, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2213748  
Error Set:
