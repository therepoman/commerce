Requests      [total, rate]            2213748, 410.08
Duration      [total, attack, wait]    1h29m58.294324302s, 1h29m58.293266143s, 1.058159ms
Latencies     [mean, 50, 95, 99, max]  1.322225ms, 1.155534ms, 2.230293ms, 2.425766ms, 1.666363064s
Bytes In      [total, mean]            135402414, 61.16
Bytes Out     [total, mean]            73824807, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2213748  
Error Set:
