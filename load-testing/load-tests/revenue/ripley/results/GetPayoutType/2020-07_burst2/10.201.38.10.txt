Requests      [total, rate]            249255, 277.68
Duration      [total, attack, wait]    14m57.629786594s, 14m57.627665104s, 2.12149ms
Latencies     [mean, 50, 95, 99, max]  1.427186ms, 1.456869ms, 2.192911ms, 2.326528ms, 416.07059ms
Bytes In      [total, mean]            15258858, 61.22
Bytes Out     [total, mean]            8312268, 33.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:249255  
Error Set:
