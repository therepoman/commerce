Requests      [total, rate]            1247007, 1386.40
Duration      [total, attack, wait]    14m59.458629179s, 14m59.457822816s, 806.363µs
Latencies     [mean, 50, 95, 99, max]  1.484084ms, 1.469948ms, 2.292721ms, 2.440488ms, 1.676901594s
Bytes In      [total, mean]            96367093, 77.28
Bytes Out     [total, mean]            47820182, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1247007  
Error Set:
