Requests      [total, rate]            1247007, 1386.44
Duration      [total, attack, wait]    14m59.434406729s, 14m59.433543643s, 863.086µs
Latencies     [mean, 50, 95, 99, max]  1.345123ms, 1.243552ms, 2.279443ms, 2.429266ms, 644.769597ms
Bytes In      [total, mean]            96357603, 77.27
Bytes Out     [total, mean]            47819833, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1247007  
Error Set:
