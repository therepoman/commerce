Requests      [total, rate]            935703, 1040.72
Duration      [total, attack, wait]    14m59.097082999s, 14m59.095333136s, 1.749863ms
Latencies     [mean, 50, 95, 99, max]  1.326194ms, 1.260175ms, 1.723585ms, 2.264356ms, 859.134514ms
Bytes In      [total, mean]            72312081, 77.28
Bytes Out     [total, mean]            35882359, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:935703  
Error Set:
