Requests      [total, rate]            935703, 1040.39
Duration      [total, attack, wait]    14m59.382103694s, 14m59.381111979s, 991.715µs
Latencies     [mean, 50, 95, 99, max]  1.444182ms, 1.494514ms, 2.282208ms, 2.427127ms, 837.528151ms
Bytes In      [total, mean]            72283651, 77.25
Bytes Out     [total, mean]            35882536, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:935703  
Error Set:
