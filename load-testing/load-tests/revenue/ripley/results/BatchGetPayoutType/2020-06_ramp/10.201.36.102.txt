Requests      [total, rate]            11086416, 2053.12
Duration      [total, attack, wait]    1h29m59.797260222s, 1h29m59.796141758s, 1.118464ms
Latencies     [mean, 50, 95, 99, max]  1.335898ms, 1.315473ms, 1.829397ms, 2.425934ms, 1.483285059s
Bytes In      [total, mean]            856528247, 77.26
Bytes Out     [total, mean]            425145096, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:11086416  
Error Set:
