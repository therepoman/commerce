Requests      [total, rate]            1871253, 2081.35
Duration      [total, attack, wait]    14m59.059691528s, 14m59.057730295s, 1.961233ms
Latencies     [mean, 50, 95, 99, max]  1.179933ms, 1.064814ms, 1.896142ms, 2.059399ms, 1.688734084s
Bytes In      [total, mean]            144564753, 77.26
Bytes Out     [total, mean]            71758475, 38.35
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1871253  
Error Set:
