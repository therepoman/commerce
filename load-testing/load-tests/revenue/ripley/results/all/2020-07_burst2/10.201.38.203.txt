Requests      [total, rate]            1496262, 1655.50
Duration      [total, attack, wait]    15m3.816969388s, 15m3.815459941s, 1.509447ms
Latencies     [mean, 50, 95, 99, max]  1.428518ms, 1.430948ms, 2.255937ms, 2.405236ms, 6.847033595s
Bytes In      [total, mean]            111607774, 74.59
Bytes Out     [total, mean]            56133081, 37.52
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1496262  
Error Set:
