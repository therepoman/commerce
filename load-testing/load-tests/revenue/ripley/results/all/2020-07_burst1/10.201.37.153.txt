Requests      [total, rate]            1122906, 1249.04
Duration      [total, attack, wait]    14m59.014697364s, 14m59.013038536s, 1.658828ms
Latencies     [mean, 50, 95, 99, max]  1.127474ms, 963.259µs, 1.824793ms, 2.050012ms, 3.326001353s
Bytes In      [total, mean]            83766951, 74.60
Bytes Out     [total, mean]            42126219, 37.52
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1122906  
Error Set:
