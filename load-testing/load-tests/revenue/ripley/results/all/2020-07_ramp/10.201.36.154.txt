Requests      [total, rate]            13300164, 2462.97
Duration      [total, attack, wait]    1h30m0.050249463s, 1h30m0.048610133s, 1.63933ms
Latencies     [mean, 50, 95, 99, max]  1.495958ms, 1.469313ms, 2.2368ms, 2.415069ms, 858.601344ms
Bytes In      [total, mean]            992062581, 74.59
Bytes Out     [total, mean]            498967908, 37.52
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:13300164  
Error Set:
