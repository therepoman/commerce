Requests      [total, rate]            2245506, 2496.88
Duration      [total, attack, wait]    14m59.326164554s, 14m59.324762165s, 1.402389ms
Latencies     [mean, 50, 95, 99, max]  1.198523ms, 1.011459ms, 2.005233ms, 2.268324ms, 836.423515ms
Bytes In      [total, mean]            167470990, 74.58
Bytes Out     [total, mean]            84239688, 37.51
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2245506  
Error Set:
