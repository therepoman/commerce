Requests      [total, rate]            2245506, 2496.47
Duration      [total, attack, wait]    14m59.473101444s, 14m59.471373648s, 1.727796ms
Latencies     [mean, 50, 95, 99, max]  1.11555ms, 950.222µs, 1.888119ms, 2.045424ms, 642.862081ms
Bytes In      [total, mean]            167454238, 74.57
Bytes Out     [total, mean]            84238434, 37.51
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2245506  
Error Set:
