Requests      [total, rate]            2020953, 2242.79
Duration      [total, attack, wait]    15m1.099801263s, 15m1.088480994s, 11.320269ms
Latencies     [mean, 50, 95, 99, max]  7.781142ms, 6.984017ms, 11.858167ms, 15.236672ms, 659.31168ms
Bytes In      [total, mean]            2653156495, 1312.82
Bytes Out     [total, mean]            580013511, 287.00
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2020953  
Error Set:
