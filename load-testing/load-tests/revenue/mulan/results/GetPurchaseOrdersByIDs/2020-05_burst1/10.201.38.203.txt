Requests      [total, rate]            1515753, 1682.49
Duration      [total, attack, wait]    15m0.909711459s, 15m0.899330703s, 10.380756ms
Latencies     [mean, 50, 95, 99, max]  6.990696ms, 6.395917ms, 11.688675ms, 13.487287ms, 602.143482ms
Bytes In      [total, mean]            2000444049, 1319.77
Bytes Out     [total, mean]            435021111, 287.00
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1515753  
Error Set:
