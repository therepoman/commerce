Requests      [total, rate]            4656636, 862.37
Duration      [total, attack, wait]    1h29m59.797417986s, 1h29m59.794736967s, 2.681019ms
Latencies     [mean, 50, 95, 99, max]  4.015718ms, 2.975981ms, 10.891124ms, 11.84846ms, 472.149864ms
Bytes In      [total, mean]            6183389027, 1327.87
Bytes Out     [total, mean]            1336454532, 287.00
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:4656636  
Error Set:
