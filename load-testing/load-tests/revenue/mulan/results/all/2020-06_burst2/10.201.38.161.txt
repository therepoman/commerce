Requests      [total, rate]            5733522, 6298.20
Duration      [total, attack, wait]    15m10.369617589s, 15m10.342328313s, 27.289276ms
Latencies     [mean, 50, 95, 99, max]  6.917136ms, 4.805423ms, 13.506961ms, 56.08759ms, 931.97893ms
Bytes In      [total, mean]            4166166832, 726.63
Bytes Out     [total, mean]            760229547, 132.59
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:5733307  400:211  504:4  
Error Set:
400 Bad Request
504 GATEWAY_TIMEOUT
