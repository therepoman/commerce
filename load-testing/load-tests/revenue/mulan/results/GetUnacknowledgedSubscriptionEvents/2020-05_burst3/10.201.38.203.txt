Requests      [total, rate]            1122753, 1247.58
Duration      [total, attack, wait]    14m59.95074918s, 14m59.946282166s, 4.467014ms
Latencies     [mean, 50, 95, 99, max]  2.964357ms, 2.590829ms, 4.269407ms, 8.089809ms, 283.989771ms
Bytes In      [total, mean]            2455223, 2.19
Bytes Out     [total, mean]            36399697, 32.42
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:1122751  400:2  
Error Set:
400 Bad Request
