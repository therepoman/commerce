aws_profile = "twitch-revenue-aws"

max_hosts = _NUM_HOSTS_

username = "_USERNAME_"

public_key_path = "~/.ssh/id_rsa.pub"

vpc_id = "vpc-9276a0f5"

subnets = "subnet-b29e95c4,subnet-29a5de71,subnet-29a9594e"

security_group = "sg-50c7c629"
