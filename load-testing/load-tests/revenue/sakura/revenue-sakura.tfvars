aws_profile = "twitch-payments+sakura-prod-us-west-2"

max_hosts = _NUM_HOSTS_

min_hosts = _NUM_HOSTS_

username = "_USERNAME_"

public_key_path = "~/.ssh/id_rsa.pub"

vpc_id = "vpc-06230e277d265e912"

subnets = "subnet-0563f8f6a606b80cc,subnet-0563f8f6a606b80cc,subnet-0563f8f6a606b80cc"

security_group = "sg-063e2894d14d8f726"
