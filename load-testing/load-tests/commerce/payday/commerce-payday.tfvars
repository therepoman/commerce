aws_profile = "payday"

min_hosts = _NUM_HOSTS_

max_hosts = _NUM_HOSTS_

username = "_USERNAME_"

public_key_path = "~/.ssh/id_rsa.pub"

vpc_id = "vpc-4cfeb629"

subnets = "subnet-31bc5e55,subnet-30393e47,subnet-6eadf037"

security_group = "sg-0ddddcad182ea3fa0"
