aws_profile = "twitch-pantheon-aws-prod"

max_hosts = _NUM_HOSTS_

min_hosts = _NUM_HOSTS_

username = "_USERNAME_"

public_key_path = "~/.ssh/id_rsa.pub"

vpc_id = "vpc-50635a37"

subnets = "subnet-20cb377b,subnet-e5154082,subnet-892d41c0"

security_group = "sg-8d1985f6"
