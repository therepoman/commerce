aws_profile = "petozi-prod"

max_hosts = _NUM_HOSTS_

username = "_USERNAME_"

public_key_path = "~/.ssh/id_rsa.pub"

vpc_id = "vpc-030de45023a90732b"

subnets = "subnet-004cbdf07e3dd181b,subnet-0855a8718f597c21a,subnet-0d40d1f39d08f9fd4"

security_group = "sg-0a873be57dede2eaa"
