from argparse import ArgumentParser
import uuid
import toml
import string
import random
import json
from io import StringIO


def generate_uuid():
    return str(uuid.uuid4())


def generate_user_id():
    return ''.join(random.choices(string.digits, k=20))


def generate_requests(n):
    num = 0
    while num < n:
        request = {
            'transaction_id': generate_uuid(),
            'token': {
                'domain': 'STICKER',
                'owner_id': generate_user_id(),
                'group_id': 'BALANCE',
                'quantity': 10
            }
        }
        io = StringIO()
        json.dump(request, io)
        yield io.getvalue()
        num += 1

def main():
    parser = ArgumentParser(description='Generate Requests for Pachinko Idempotency Load Test')
    parser.add_argument('-n', '--num', type=int, default=10000, help='the number of requests to generate')
    parser.add_argument('-o', '--output', default='requests.toml', help='the output file for the requests')

    args = parser.parse_args()

    requests = generate_requests(args.num)

    output = {
        'data': {
            'request': requests
        }
    }

    file = open(args.output, 'w+')
    output_toml = toml.dump(output, file)

    file.close()
    print('wrote ' + str(args.num) + ' requests to ' + args.output)

main()