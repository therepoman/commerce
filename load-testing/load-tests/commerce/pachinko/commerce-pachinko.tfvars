aws_profile = "pachinko-prod"

max_hosts = _NUM_HOSTS_

username = "_USERNAME_"

public_key_path = "~/.ssh/id_rsa.pub"

vpc_id = "vpc-03f9d62c97421b8e4"

# You typically want to use the private subnets for most services
subnets = "subnet-004ad255c07c50231,subnet-0c13ee46ece68593e,subnet-0d53e17f2c3973a6a"

security_group = "sg-017d2040cc44d995d"
