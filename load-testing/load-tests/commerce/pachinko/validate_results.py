from argparse import ArgumentParser
import toml
import json
import requests


def main():
    parser = ArgumentParser(description='Validate Requests for Pachinko Idempotency Load Test')
    parser.add_argument('-r', '--requests', default='requests.toml',
                        help='the input requests file that you want to validate')

    args = parser.parse_args()

    requests_data = toml.load(args.requests)

    for data in requests_data['data']['request']:
        req = json.loads(data)
        raw_resp = requests.post('https://sticker.prod.pachinko.twitch.a2z.com/twirp/code.justin.tv.commerce.pachinko.Pachinko/GetToken',
                             json={'domain': 'STICKER', 'owner_id': req['token']['owner_id'], 'group_id': 'BALANCE'},
                            proxies=dict(https='socks5://localhost:8080'))

        resp = json.loads(raw_resp.text)

        if int(resp['token']['quantity']) is not int(req['token']['quantity']):
            print('request ' + req['transaction_id'] + " somehow didn't work; expected " +
                  str(req['token']['quantity']) + " but got " + str(resp['token']['quantity']))


main()
