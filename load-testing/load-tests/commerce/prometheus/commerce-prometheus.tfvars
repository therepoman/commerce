aws_profile = "twitch-prometheus-prod"

max_hosts = _NUM_HOSTS_

min_hosts = _NUM_HOSTS_

username = "_USERNAME_"

public_key_path = "~/.ssh/id_rsa.pub"

vpc_id = "vpc-0e98258b8e57188ef"

subnets = "subnet-0845265a09a62b017,subnet-0b67c3fa1554c0bb5,subnet-0bafd9874c580fc12"

security_group = "sg-058656a898e0f0360"
