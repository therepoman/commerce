aws_profile = "twitch-percy-aws-prod"

max_hosts = _NUM_HOSTS_

min_hosts = _NUM_HOSTS_

username = "_USERNAME_"

public_key_path = "~/.ssh/id_rsa.pub"

vpc_id = "vpc-0b3b598b81954ab79"

subnets = "subnet-02426c7f532714c03,subnet-036067fa062137a21,subnet-0141571a61b5c992f"

security_group = "sg-062afd30bfaba22d6"
