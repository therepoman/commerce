-plan flag is deprecated; please use positional plan arguments instead of the -plan flag:
  $ nebbiolo  Percy-GetOngoingHypeTrain-LoadTest.toml []
Requests      [total, rate]            5382018, 1495.35
Duration      [total, attack, wait]    59m59.183092711s, 59m59.180516139s, 2.576572ms
Latencies     [mean, 50, 95, 99, max]  1.832019ms, 1.74881ms, 2.802208ms, 5.543916ms, 644.233993ms
Bytes In      [total, mean]            11604096, 2.16
Bytes Out     [total, mean]            175003497, 32.52
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:5382018  
Error Set:
