Requests      [total, rate]            13768452, 3715.38
Duration      [total, attack, wait]    1h1m45.973225363s, 1h1m45.797056448s, 176.168915ms
Latencies     [mean, 50, 95, 99, max]  28.557181ms, 16.019785ms, 70.517262ms, 178.493683ms, 8.375659661s
Bytes In      [total, mean]            43518497343, 3160.74
Bytes Out     [total, mean]            3972380588, 288.51
Success       [ratio]                  99.96%
Status Codes  [code:count]             200:13762588  500:5864  
Error Set:
500 Internal Server Error
