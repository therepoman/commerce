Requests      [total, rate]            13768452, 3711.98
Duration      [total, attack, wait]    1h1m49.310539233s, 1h1m49.188597927s, 121.941306ms
Latencies     [mean, 50, 95, 99, max]  28.528143ms, 16.191446ms, 70.455724ms, 173.599762ms, 10.772710497s
Bytes In      [total, mean]            43516924193, 3160.63
Bytes Out     [total, mean]            3972379601, 288.51
Success       [ratio]                  99.96%
Status Codes  [code:count]             200:13762424  500:6028  
Error Set:
500 Internal Server Error
