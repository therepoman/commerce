aws_profile = "twitch-corleone-prod"

min_hosts = _NUM_HOSTS_

max_hosts = _NUM_HOSTS_

username = "_USERNAME_"

public_key_path = "~/.ssh/id_rsa.pub"

vpc_id = "vpc-085272e807f316a86"

subnets = "subnet-0159c166164d4a97f,subnet-0e061d5e8a31a05e1,subnet-0717936c49483f129"

security_group = "sg-05051c9ddb0024e25"
