aws_profile = "twitch-product-catalog-prod"

max_hosts = _NUM_HOSTS_

min_hosts = _NUM_HOSTS_

username = "_USERNAME_"

public_key_path = "~/.ssh/id_rsa.pub"

vpc_id = "vpc-0e18ce938a508a6e7"

subnets = "subnet-0b6cd42ba61f4e71f,subnet-072e12ed32a192fe3,subnet-0280051a032cfdd5a"

security_group = "sg-07bea339faeaa86e3"
