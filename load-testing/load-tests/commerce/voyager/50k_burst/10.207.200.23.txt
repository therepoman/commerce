Requests      [total, rate]            3742503, 4122.89
Duration      [total, attack, wait]    15m7.746293901s, 15m7.737370708s, 8.923193ms
Latencies     [mean, 50, 95, 99, max]  23.610693ms, 8.71686ms, 24.086903ms, 502.479088ms, 1.237970571s
Bytes In      [total, mean]            40280251, 10.76
Bytes Out     [total, mean]            235345593, 62.88
Success       [ratio]                  97.60%
Status Codes  [code:count]             200:3652597  400:7  500:89899  
Error Set:
400 Bad Request
500 Internal Server Error
