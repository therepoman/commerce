Requests      [total, rate]            3742503, 4130.63
Duration      [total, attack, wait]    15m6.047013834s, 15m6.035827413s, 11.186421ms
Latencies     [mean, 50, 95, 99, max]  23.99737ms, 8.986852ms, 25.000836ms, 502.784295ms, 1.244639873s
Bytes In      [total, mean]            42133002, 11.26
Bytes Out     [total, mean]            235346279, 62.88
Success       [ratio]                  97.46%
Status Codes  [code:count]             200:3647446  400:11  500:95046  
Error Set:
400 Bad Request
500 Internal Server Error
