Requests      [total, rate]            21492037, 3978.41
Duration      [total, attack, wait]    1h30m2.18373428s, 1h30m2.170605581s, 13.128699ms
Latencies     [mean, 50, 95, 99, max]  9.181902ms, 8.891445ms, 11.66707ms, 17.479007ms, 515.36887ms
Bytes In      [total, mean]            43464435, 2.02
Bytes Out     [total, mean]            1351510537, 62.88
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:21491560  400:69  500:408  
Error Set:
400 Bad Request
500 Internal Server Error
