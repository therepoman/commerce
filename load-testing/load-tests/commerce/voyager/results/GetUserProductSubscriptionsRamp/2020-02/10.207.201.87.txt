Requests      [total, rate]            21492037, 3978.55
Duration      [total, attack, wait]    1h30m1.99572348s, 1h30m1.982823971s, 12.899509ms
Latencies     [mean, 50, 95, 99, max]  8.913836ms, 8.647845ms, 11.473933ms, 17.191497ms, 525.13523ms
Bytes In      [total, mean]            43446407, 2.02
Bytes Out     [total, mean]            1351517145, 62.88
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:21491578  400:58  500:401  
Error Set:
400 Bad Request
500 Internal Server Error
