aws_profile = "twitch-voyager-prod"

max_hosts = _NUM_HOSTS_

min_hosts = _NUM_HOSTS_

username = "_USERNAME_"

public_key_path = "~/.ssh/id_rsa.pub"

vpc_id = "vpc-07a967eb944ad3bad"

subnets = "subnet-081587a16d05344da,subnet-05cd36bde3ac08297,subnet-0292f83e5c0c66952"

security_group = "sg-0510523b5e3141459"

