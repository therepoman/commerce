Requests      [total, rate]            358236, 66.42
Duration      [total, attack, wait]    1h29m53.333046497s, 1h29m53.331840341s, 1.206156ms
Latencies     [mean, 50, 95, 99, max]  2.102823ms, 1.392171ms, 5.315862ms, 7.762388ms, 268.156002ms
Bytes In      [total, mean]            78040831, 217.85
Bytes Out     [total, mean]            11965346, 33.40
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:358236  
Error Set:
