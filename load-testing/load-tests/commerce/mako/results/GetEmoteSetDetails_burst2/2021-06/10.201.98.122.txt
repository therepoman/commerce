Requests      [total, rate]            2744898, 3039.71
Duration      [total, attack, wait]    15m3.031423851s, 15m3.01421432s, 17.209531ms
Latencies     [mean, 50, 95, 99, max]  9.673995ms, 8.973505ms, 15.412674ms, 25.230912ms, 278.217069ms
Bytes In      [total, mean]            100886129554, 36754.05
Bytes Out     [total, mean]            166744362, 60.75
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:2744898  
Error Set:
