Requests      [total, rate]            3582036, 663.50
Duration      [total, attack, wait]    1h29m58.731302985s, 1h29m58.728818985s, 2.484ms
Latencies     [mean, 50, 95, 99, max]  1.399567ms, 1.278874ms, 2.281991ms, 4.94636ms, 230.467961ms
Bytes In      [total, mean]            777665418, 217.10
Bytes Out     [total, mean]            119641820, 33.40
Success       [ratio]                  100.00%
Status Codes  [code:count]             200:3582036  
Error Set:
