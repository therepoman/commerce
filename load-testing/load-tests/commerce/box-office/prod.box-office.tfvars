# NOTE: This is used as input to the setup script - the values in the {CURLY_BRACES} will be replaced with user input

aws_profile = "twitch-box-office-aws"

max_hosts = _NUM_HOSTS_

username = "_USERNAME_"

public_key_path = "~/.ssh/id_rsa.pub"

vpc_id = "vpc-8b9ce9f2"

subnets = "subnet-90835be9,subnet-30cc377b,subnet-28a92d72"

security_group = "sg-b6ebecc9"
