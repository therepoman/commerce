aws_profile = "poliwag-prod"

max_hosts = _NUM_HOSTS_

username = "_USERNAME_"

public_key_path = "~/.ssh/id_rsa.pub"

vpc_id = "vpc-01105b4df584d9924"

subnets = "subnet-093d5373d3cb6ff2b,subnet-002c7aa781e0ca7c5,subnet-08d636b9f9cf7f3ac"

security_group = "sg-0d6bbf791e17318a6"