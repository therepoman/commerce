aws_profile = "mako-prod"

max_hosts = _NUM_HOSTS_

min_hosts = _NUM_HOSTS_

username = "_USERNAME_"

public_key_path = "~/.ssh/id_rsa.pub"

vpc_id = "vpc-b8e471df"

subnets = "subnet-b808c8f1,subnet-195df37e,subnet-6557793d"

security_group = "sg-082f5a79bc656028d"
