aws_profile = "mako-devo"

max_hosts = _NUM_HOSTS_

min_hosts = _NUM_HOSTS_

username = "_USERNAME_"

public_key_path = "~/.ssh/id_rsa.pub"

vpc_id = "vpc-b0f164d7"

subnets = "subnet-f9fd3db0,subnet-c25e709a,subnet-8d49e7ea"

security_group = "sg-013da1e1644952cb0"
