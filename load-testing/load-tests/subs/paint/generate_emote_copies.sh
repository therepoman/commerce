#!/bin/zsh

emote=$1

aws s3 cp s3://prod-emoticon-uploads/emoticons/"$emote" s3://prod-emoticon-uploads/emoticons/"$emote"-loadtest
echo "$emote"-loadtest >> prod-emote-list.csv
