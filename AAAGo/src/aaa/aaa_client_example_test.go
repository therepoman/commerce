package aaa_test

import (
	"code.justin.tv/commerce/AAAGo/src/aaa"
	"io"
	"net/http"
)

var httpClient *http.Client
var aaaClient aaa.Client
var body io.Reader

func Example_AAAClientFlow() {
	// Create a standard http.Request.
	req, err := http.NewRequest("GET", "http://SomeCoolService.amazon.com:8888/getSomethingCool", body)
	if err != nil {
		//return nil, err
		return
	}

	// Encode the request using your aaa.Client. Encoding requires the service name, the operation name, and the request to encode.
	// The http.Request is mutated by this operation.
	// A client context is returned that is required for decoding the response.
	clientCxt, err := aaaClient.EncodeRequest("SomeCoolService", "somethingCool", req)
	if err != nil {
		//return nil, err
		return
	}

	// Send the request using a http.Client as normal.
	resp, err := httpClient.Do(req)
	if err != nil {
		//return nil, err
		return
	}

	// Decode the response using your aaa.Client. Decoding requires the client context that was returned while encoding the request.
	// The http.Response is mutated by this operation.
	err = aaaClient.DecodeResponse(clientCxt, resp)
	if err != nil {
		//return nil, err
		return
	}

	// The http.Response is now available for use as normal.
	//return resp, nil
}
