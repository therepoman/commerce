package msgpack

import (
	"code.justin.tv/commerce/AAAGo/src/aaa/internal/msgpack/code"
	"bytes"
	"encoding/binary"
	"io"
	"math"
)

type MsgPackEncoder interface {
	EncodeMsgPack(*Encoder) error
}

type Encoder struct {
	w io.Writer
}

func Encode(v MsgPackEncoder) ([]byte, error) {
	buff := &bytes.Buffer{}
	e := &Encoder{
		w: buff,
	}
	if err := v.EncodeMsgPack(e); err != nil {
		return nil, err
	}
	return buff.Bytes(), nil
}

func (e *Encoder) writeByte(b byte) error {
	_, err := e.w.Write([]byte{b})
	return err
}

func (e *Encoder) writeCodeBytes(c byte, b []byte) error {
	if err := e.writeByte(c); err != nil {
		return err
	}
	_, err := e.w.Write(b)
	return err
}

func (e *Encoder) EncodeNil() error {
	return e.writeByte(code.Nil)
}

func (e *Encoder) EncodeBool(v bool) error {
	if v {
		return e.writeByte(code.BoolTrue)
	}
	return e.writeByte(code.BoolFalse)
}

func (e *Encoder) EncodeUint8(v uint8) error {
	return e.writeCodeBytes(code.Uint8, []byte{byte(v)})
}

func (e *Encoder) EncodeUint16(v uint16) error {
	return e.writeCodeBytes(code.Uint16, uint16Bytes(v))
}

func (e *Encoder) EncodeUint32(v uint32) error {
	return e.writeCodeBytes(code.Uint32, uint32Bytes(v))
}

func (e *Encoder) EncodeUint64(v uint64) error {
	return e.writeCodeBytes(code.Uint64, uint64Bytes(v))
}

func (e *Encoder) EncodeInt8(v int8) error {
	return e.writeCodeBytes(code.Int8, []byte{byte(v)})
}

func (e *Encoder) EncodeInt16(v int16) error {
	return e.writeCodeBytes(code.Int16, uint16Bytes(uint16(v)))
}

func (e *Encoder) EncodeString(v string) error {
	return e.EncodeBytes([]byte(v))
}

func (e *Encoder) EncodeBytes(v []byte) error {
	if err := e.encodeRawLen(uint32(len(v))); err != nil {
		return err
	}
	_, err := e.w.Write(v)
	return err
}

func (e *Encoder) encodeRawLen(l uint32) error {
	if l < 32 {
		return e.writeByte(code.FixRawLow | byte(l))
	} else if l < math.MaxUint16 {
		return e.writeCodeBytes(code.Raw16, uint16Bytes(uint16(l)))
	}
	return e.writeCodeBytes(code.Raw32, uint32Bytes(uint32(l)))
}

func (e *Encoder) EncodeArrayLen(l uint32) error {
	if l < 16 {
		return e.writeByte(code.FixArrayLow | byte(l))
	} else if l < math.MaxUint16 {
		e.writeCodeBytes(code.Array16, uint16Bytes(uint16(l)))
	}
	return e.writeCodeBytes(code.Array32, uint32Bytes(uint32(l)))
}

func uint16Bytes(v uint16) []byte {
	bs := make([]byte, 2)
	binary.BigEndian.PutUint16(bs, v)
	return bs
}

func uint32Bytes(v uint32) []byte {
	bs := make([]byte, 4)
	binary.BigEndian.PutUint32(bs, v)
	return bs
}

func uint64Bytes(v uint64) []byte {
	bs := make([]byte, 8)
	binary.BigEndian.PutUint64(bs, v)
	return bs
}
