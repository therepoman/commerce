package msgpack

func (e *Encoder) EncodeOuterArrayStart(numFields int) error {
	// With AAASecurityDaemon, there's always an extra empty field in the outer array.
	if err := e.EncodeArrayLen(uint32(numFields + 1)); err != nil {
		return err
	}
	return e.EncodeArrayLen(0)
}

func (e *Encoder) EncodeUInt8Field(index uint32, v uint8) error {
	if err := e.EncodeFieldHeader(index); err != nil {
		return err
	}
	return e.EncodeUint8(v)
}

func (e *Encoder) EncodeUInt16Field(index uint32, v uint16) error {
	if err := e.EncodeFieldHeader(index); err != nil {
		return err
	}
	return e.EncodeUint16(v)
}

func (e *Encoder) EncodeUInt32Field(index uint32, v uint32) error {
	if err := e.EncodeFieldHeader(index); err != nil {
		return err
	}
	return e.EncodeUint32(v)
}

func (e *Encoder) EncodeUInt64Field(index uint32, v uint64) error {
	if err := e.EncodeFieldHeader(index); err != nil {
		return err
	}
	return e.EncodeUint64(v)
}

func (e *Encoder) EncodeInt8Field(index uint32, v int8) error {
	if err := e.EncodeFieldHeader(index); err != nil {
		return err
	}
	return e.EncodeInt8(v)
}

func (e *Encoder) EncodeInt16Field(index uint32, v int16) error {
	if err := e.EncodeFieldHeader(index); err != nil {
		return err
	}
	return e.EncodeInt16(v)
}

func (e *Encoder) EncodeStringField(index uint32, v string) error {
	if err := e.EncodeFieldHeader(index); err != nil {
		return err
	}
	return e.EncodeString(v)
}

func (e *Encoder) EncodeBytesField(index uint32, v []byte) error {
	if err := e.EncodeFieldHeader(index); err != nil {
		return err
	}
	return e.EncodeBytes(v)
}

func (e *Encoder) EncodeSubField(index uint32, val MsgPackEncoder) error {
	if err := e.EncodeFieldHeader(index); err != nil {
		return err
	}
	return val.EncodeMsgPack(e)
}

func (e *Encoder) EncodeFieldHeader(index uint32) error {
	if err := e.EncodeArrayLen(2); err != nil {
		return err
	}
	return e.EncodeUint32(index)
}
