# sonic
Sonic is a Go Service that fronts information from the TAILS amazon service.

## Setting up the Go service

### Setting up your Github account

 - Go to your account settings page (https://git.xarth.tv/settings/keys) and follow the instructions there to add a new SSH key to your account.

### Setting up your workspace

 - Install go:

		brew install go

 - Create a Go workspace folder in your home directory. Ex:

		mkdir /Users/<username>/<GoWorkspace>

 - Set your environment variable for GOPATH (in your .bashrc, .zshrc, or etc.)

		export GOPATH=/Users/<username>/<GoWorkspace>

 - Append GOPATH/bin to PATH environment variable

		export PATH=$PATH:$GOPATH/bin

 - To check if Go installed correctly, run go version from commandline. This version should be more than 1.9

 - Clone down sonic source within the new gopath. You'll need to be on the twitch VPN for this to work.

		go get code.justin.tv/commerce/sonic

 - Directory structure will now look like this:

		/Users/<username>/GoWorkspace/src/code.justin.tv/commerce/sonic

### Setup dependencies

Sonic uses <a href="https://github.com/golang/dep" />Dep</a> for its dependency management. While installation instructions can be found there; its can be resumed to running

		brew install dep
		brew upgrade dep

Next, you'll want to create AWS credentials for yourself to be able to run sonic locally. These can be found in the twitch-sonic and twitch-sonic-dev AWS accounts. You'll need to add these to your AWS configuration. Notably, when sonic is run locally, it will use the credentials under the twitch-sonic-dev profile.

Finally, you'll want to pull down the dependencies needed to run sonic. Do so by running:

		dep ensure

### Making a code change

 - Create a new git branch : git checkout -b <branch name>

 - git push origin <branch name>

 - Click compare and pull request on the git website for your package

 - Make changes and commit to the branch. Do not use commit --amend --no-edit. Push the commits

 - When you get a +1 to your pull request, do "Squash and merge your changes"

### Building, Compiling, Testing, Running

 - Prior to running anything, you'll first want to run:

		make lint_setup
		make mock_setup

This will setup the necessary dependencies to run the linter and to generate mocks (if you have to).

- To run locally, run:

		make local

- Prior to committing, you'll want to run:

		make release

- To only run the tests, run:

		make test

- If you modify the twirp-model and need to update the client, run:

		make generate-twirp

- If you need to create a new mock, add a mockery call to the "mocks" target and run:

		make mocks

### Adding new dependecies

Typically you can just add an import and run:

		dep ensure

To have it get picked up. Alternatively, it can be added to the <a href="https://git.xarth.tv/commerce/sonic/blob/master/Gopkg.toml">Gopkg.toml</a> file and the running:

		dep ensure

### Deployment

#### Code changes

Sonic uses CodePipeline to deploy changes to its stages. You can find the relevant pipeline under "CodePipeline" in the twitch-sonic AWS account. It requires manual approvals to promote versions to OneBox and Prod.

**WARNING:** The CodePipeline will not deploy any push from github because it consumes from a mirror 
repo in AWS CodeCommit. As of today (6/22/21) the mirror repo is outdate for more than 2 years and 
the service is not able to get deployed automatically. Currently there is no plan to fix this 
because there are no urgent changes into the service code and this service might get deprecated in 
the near future and might change ownership to another PG team.


#### Infrastructure changes

Sonic uses CloudFormation to manage its AWS infrastrucuture. The associated templates for each stage can be found <a href="https://git.xarth.tv/commerce/sonic/tree/master/cloudformation">here</a>. For the most part, unless you're adding new AWS features to sonic, you'll never really need to change the template, and can instead simply go into CloudFormation for the associated AWS account and use the parameters to modify infrastructure (to scale up the service, for instance).

### Public Visibility

Sonic is exposed through GQL. The full <a href="https://git.xarth.tv/edge/visage/commit/79fec93317f65eb47c5c79f2143c393394715908#diff-75b25ff2b1c157c7afb71030f5fb42b0">onboarding PR</a>.

Details for relevant APIs are below. Each of these can be tested from the <a href="https://api.twitch.tv/gql/explore">Prod GQL explorer</a> or the <a href="http://localhost:8000/gql/explore">Local GQL explorer</a> (You'll need to run Visage locally to get the local explorer running).

Each API requires an OAuth token for the user you are trying to operate on. You can retrieve one by logging into twitch.tv for the account, going to the inventory page, opening "Developer Tools" and inspecting the "Application" tab. Your OAuth token will have the name "auth-token".

To provide this OAuth token to the GQL explorer, you can do so using the <a href="https://www.google.com/search?source=hp&ei=hxThWs-VKpbgjwPz3ZyoDA&q=ModHeader&oq=ModHeader&gs_l=psy-ab.3..0l3j0i10k1j0l6.448.448.0.1330.2.1.0.0.0.0.55.55.1.1.0....0...1.2.64.psy-ab..1.1.54.0...0.I07JWcrOLWY">ModHeader</a> Chrome plugin. You'll want to create a request header with "Authorization" as the key, and "OAuth \<token\>" as the value.

#### HasLinkedAccount

<a href="https://git.xarth.tv/edge/visage/blob/master/gql/schema/types/account_connection_set.graphql">Schema</a>
<br>
<a href="https://git.xarth.tv/edge/visage/blob/master/gql/resolvers/account_connection_set.go">Resolver</a>

Query:
<pre>
query {
  user(id:&lttuid to query&gt) {
    accountConnections {
      hasConnectedAmazon
    }
  }
}
</pre>

#### UnlinkAmazonAccount

<a href="https://git.xarth.tv/edge/visage/blob/master/gql/schema/mutations/unlink_amazon_connection.graphql">Schema</a>
<br>
<a href="https://git.xarth.tv/edge/visage/blob/master/gql/resolvers/mutation_unlink_amazon_connection.go">Resolver</a>

Query:
<pre>
mutation {
  unlinkAmazonConnection(input:{userID:&lttuid to unlink&gt}) {
    isSuccess
  }
}
</pre>

### Cross-network boundary

Sonic reaches TAILS APIs through SWS, as such in order to call the appropriate service, the IP which is making the call must be whitelisted here: https://code.amazon.com/packages/SubsidiaryWebServicesPortal/blobs/cd4a15ed57fafb80afd231bc1ed990539b2571f3/--/configuration/brazil-config/app/SubsidiaryWebServicesPortal.cfg

Twitch laptops on the VPN should already be whitelisted, so there shouldn't be any action needed to call locally. But if sonic infrastructure changes and gets new NAT gateways, then the new IP ranges will need to be added to the config in order for the calls to go through the network boundary.

### Scaling

Sonic was load test up to 500 TPS on its HasLinkedAmazonAccount API:

<p align="center"><img src="./loadTest.png" width="640"/></p>

### Service Dashboards

Amazon wiki [Service dashboard](https://w.amazon.com/bin/view/TAILS/Dashboard#Sonic_Metrics)

GraphQL [Client Dashboard](https://grafana.xarth.tv/d/1lVBTXAiz/graphql-dependency-details?orgId=1&var-env=production&var-region=us-west-2&var-service=Sonic&var-method=All&var-all_services=All)


### Certificate Rotation

Follow [this wiki](https://w.amazon.com/bin/view/Samus/Tech/Boosters/Runbook/TwitchCertificates/) to rotate the SSL certificate.
Make sure to replace all the service hosts as a final step!
