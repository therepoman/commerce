package api

import (
	"context"

	svc "code.justin.tv/commerce/TwitchAmazonIdentityLinkingServiceGoClient/src/tv/justin/tails"
	"code.justin.tv/commerce/sonic/config"
	"code.justin.tv/commerce/sonic/middleware"
	st "code.justin.tv/commerce/sonic/sonic-twirp"
	tw "code.justin.tv/common/twirp"
	"time"
)

const (
	// TODO: Grab directly from context
	HasLinkedAmazonAccountAPIName = "HasLinkedAmazonAccount"
	UnlinkTwitchAccountAPIName    = "UnlinkTwitchAccount"
	ClientString                  = ""
	TAILSHasLinkedLatency         = "TAILS:GetLinkedAmazonDirectedId:Latency"
	TAILSHasLinkedSuccess         = "TAILS:GetLinkedAmazonDirectedId:Success"
	TAILSUnlinkLatency            = "TAILS:UnlinkTwitchAccount:Latency"
	TAILSUnlinkSuccess            = "TAILS:UnlinkTwitchAccount:Success"
	TAILSHasLinkedAccountCount    = "TAILS:HasLinkedAccount:Count"
)

type SonicService struct {
	serviceConfig *config.SonicConfiguration
}

type TailsClient interface {
	GetLinkedAmazonDirectedId(svc.GetLinkedAmazonDirectedIdRequest) (svc.GetLinkedAmazonDirectedIdResponse, error)
	UnlinkTwitchAccount(svc.UnlinkTwitchAccountRequest) (svc.UnlinkTwitchAccountResponse, error)
}

func NewSonicService(serviceConfig *config.SonicConfiguration) SonicService {
	return SonicService{serviceConfig}
}

func (theServer *SonicService) HasLinkedAmazonAccount(c context.Context, request *st.HasLinkedAmazonAccountRequest) (*st.HasLinkedAmazonAccountResponse, error) {
	return theServer.CallHasLinkedAmazonAccount(theServer.createTailsServiceClient(), c, request)
}

func (theServer *SonicService) CallHasLinkedAmazonAccount(tailsClient TailsClient, c context.Context, request *st.HasLinkedAmazonAccountRequest) (*st.HasLinkedAmazonAccountResponse, error) {
	svcRequest := svc.NewGetLinkedAmazonDirectedIdRequest()
	requestTuid := request.GetTuid()
	svcRequest.SetTwitchUserId(&requestTuid)

	if !isUserAuthenticated(c, requestTuid) {
		return nil, tw.NewError(tw.Unauthenticated, "User is not authorized to get linking information for provided user.")
	}

	apiMetrics := middleware.GetMetricReportFromContext(c)
	requestStart := time.Now()
	svcResponse, err := tailsClient.GetLinkedAmazonDirectedId(svcRequest)
	requestTime := time.Now().Sub(requestStart)
	apiMetrics.PostLatencyMetricWithName(HasLinkedAmazonAccountAPIName, ClientString, TAILSHasLinkedLatency, requestTime)

	if err != nil {
		apiMetrics.PostCountMetric(HasLinkedAmazonAccountAPIName, ClientString, 0.0, TAILSHasLinkedSuccess)
		logger := middleware.GetLoggerFromContext(c)
		logger.Error("Error calling TAILS during fetch of amazon directed id for tuid ", requestTuid, ". Error : ", err)
		return nil, tw.NewError(tw.Internal, "Error calling Amazon linking service during account retrieval.")
	} else {
		apiMetrics.PostCountMetric(HasLinkedAmazonAccountAPIName, ClientString, 1.0, TAILSHasLinkedSuccess)
		hasLinkedAccount := false // defaults to false.

		if svcResponse.HasLinkedAccount() != nil {
			hasLinkedAccount = *svcResponse.HasLinkedAccount()
		}
		if hasLinkedAccount {
			apiMetrics.PostCountMetric(HasLinkedAmazonAccountAPIName, ClientString, 1.0, TAILSHasLinkedAccountCount)
		} else {
			apiMetrics.PostCountMetric(HasLinkedAmazonAccountAPIName, ClientString, 0.0, TAILSHasLinkedAccountCount)
		}

		response := st.HasLinkedAmazonAccountResponse{hasLinkedAccount}
		return &response, nil
	}
}

func (theServer *SonicService) UnlinkTwitchAccount(c context.Context, request *st.UnlinkTwitchAccountRequest) (*st.UnlinkTwitchAccountResponse, error) {
	return theServer.CallUnlinkTwitchAccount(theServer.createTailsServiceClient(), c, request)
}

func (theServer *SonicService) CallUnlinkTwitchAccount(tailsClient TailsClient, c context.Context, request *st.UnlinkTwitchAccountRequest) (*st.UnlinkTwitchAccountResponse, error) {
	svcRequest := svc.NewUnlinkTwitchAccountRequest()
	requestTuid := request.GetTuid()
	svcRequest.SetTwitchUserID(&requestTuid)

	if !isUserAuthenticated(c, requestTuid) {
		return nil, tw.NewError(tw.Unauthenticated, "User is not authorized to unlink the provided user.")
	}

	apiMetrics := middleware.GetMetricReportFromContext(c)
	requestStart := time.Now()
	svcResponse, err := tailsClient.UnlinkTwitchAccount(svcRequest)
	requestTime := time.Now().Sub(requestStart)
	apiMetrics.PostLatencyMetricWithName(UnlinkTwitchAccountAPIName, ClientString, TAILSUnlinkLatency, requestTime)

	if err != nil {
		apiMetrics.PostCountMetric(UnlinkTwitchAccountAPIName, ClientString, 0.0, TAILSUnlinkSuccess)
		logger := middleware.GetLoggerFromContext(c)
		logger.Error("Error calling TAILS during unlink of tuid ", requestTuid, ". Error : ", err)
		return nil, tw.NewError(tw.Internal, "Error calling Amazon linking service during account unlinking.")
	} else {
		apiMetrics.PostCountMetric(UnlinkTwitchAccountAPIName, ClientString, 1.0, TAILSUnlinkSuccess)
		success := false // defaults to false

		if svcResponse.Success() != nil {
			success = *svcResponse.Success()
		}

		return &st.UnlinkTwitchAccountResponse{success}, nil
	}
}

func (theServer *SonicService) createTailsServiceClient() *svc.TwitchAmazonIdentityLinkingServiceClient {
	return svc.NewTwitchAmazonIdentityLinkingServiceClient(theServer.serviceConfig.GetSwsDialer(), theServer.serviceConfig.GetSwsCodec())
}

func isUserAuthenticated(c context.Context, expectedUserId string) bool {
	verifiedUserId := middleware.GetVerifiedTwitchUserIdFromContext(c)
	return verifiedUserId == expectedUserId
}
