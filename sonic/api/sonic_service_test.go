package api

import (
	"context"
	"errors"
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	svc "code.justin.tv/commerce/TwitchAmazonIdentityLinkingServiceGoClient/src/tv/justin/tails"
	"code.justin.tv/commerce/sonic/contextkeys"
	"code.justin.tv/commerce/sonic/mocks"
	st "code.justin.tv/commerce/sonic/sonic-twirp"
	"code.justin.tv/commerce/sonic/sonictesting"
)

const (
	Tuid = "123654897"
)

func TestUnlinkTwitchAccountSuccess(t *testing.T) {
	service := &SonicService{}
	mockTailsClient := &mocks.TailsClient{}

	serviceRequest := st.UnlinkTwitchAccountRequest{
		Tuid,
	}

	ctx := context.WithValue(context.Background(), contextkeys.UserIDCtxKey, Tuid)
	mockMetricReport := &mocks.MetricsReporter{}
	ctx = context.WithValue(ctx, contextkeys.RequestMetricsReportKey, mockMetricReport)
	mockMetricReport.On(
		"PostLatencyMetricWithName",
		UnlinkTwitchAccountAPIName,
		ClientString,
		TAILSUnlinkLatency,
		mock.AnythingOfType("time.Duration")).Return()
	mockMetricReport.On(
		"PostCountMetric",
		HasLinkedAmazonAccountAPIName,
		ClientString,
		1.0,
		TAILSUnlinkSuccess).Return()

	success := true
	tailsResponse := svc.NewUnlinkTwitchAccountResponse()
	tailsResponse.SetSuccess(&success)

	mockTailsClient.On("UnlinkTwitchAccount", mock.MatchedBy(func(input svc.UnlinkTwitchAccountRequest) bool {
		return *input.TwitchUserID() == Tuid
	})).Return(tailsResponse, nil)

	// Check when there is a linked account.
	serviceResponse, serviceErr := service.CallUnlinkTwitchAccount(mockTailsClient, ctx, &serviceRequest)
	assert.Nil(t, serviceErr)
	assert.Equal(t, true, serviceResponse.GetSuccess())
	mockTailsClient.AssertNumberOfCalls(t, "UnlinkTwitchAccount", 1)
	mockMetricReport.AssertNumberOfCalls(t, "PostLatencyMetricWithName", 1)
	mockMetricReport.AssertNumberOfCalls(t, "PostCountMetric", 1)

	// Check when there isn't a linked account.
	success = false
	serviceResponse, serviceErr = service.CallUnlinkTwitchAccount(mockTailsClient, ctx, &serviceRequest)
	assert.Nil(t, serviceErr)
	assert.Equal(t, false, serviceResponse.GetSuccess())
	mockTailsClient.AssertNumberOfCalls(t, "UnlinkTwitchAccount", 2)
	mockMetricReport.AssertNumberOfCalls(t, "PostLatencyMetricWithName", 2)
	mockMetricReport.AssertNumberOfCalls(t, "PostCountMetric", 2)
}

func TestHasLinkedAccountSuccess(t *testing.T) {
	service := &SonicService{}
	mockTailsClient := &mocks.TailsClient{}

	serviceRequest := st.HasLinkedAmazonAccountRequest{
		Tuid,
	}

	ctx := context.WithValue(context.Background(), contextkeys.UserIDCtxKey, Tuid)
	mockMetricReport := &mocks.MetricsReporter{}
	ctx = context.WithValue(ctx, contextkeys.RequestMetricsReportKey, mockMetricReport)
	mockMetricReport.On(
		"PostLatencyMetricWithName",
		HasLinkedAmazonAccountAPIName,
		ClientString,
		TAILSHasLinkedLatency,
		mock.AnythingOfType("time.Duration")).Return()
	mockMetricReport.On(
		"PostCountMetric",
		HasLinkedAmazonAccountAPIName,
		ClientString,
		1.0,
		TAILSHasLinkedSuccess).Return()

	hasLinked := true
	tailsResponse := svc.NewGetLinkedAmazonDirectedIdResponse()
	tailsResponse.SetHasLinkedAccount(&hasLinked)

	mockTailsClient.On("GetLinkedAmazonDirectedId", mock.MatchedBy(func(input svc.GetLinkedAmazonDirectedIdRequest) bool {
		return *input.TwitchUserId() == Tuid
	})).Return(tailsResponse, nil)

	// Check when there is a linked account.
	mockMetricReport.On(
		"PostCountMetric",
		HasLinkedAmazonAccountAPIName,
		ClientString,
		1.0,
		TAILSHasLinkedAccountCount).Return()
	serviceResponse, serviceErr := service.CallHasLinkedAmazonAccount(mockTailsClient, ctx, &serviceRequest)
	assert.Nil(t, serviceErr)
	assert.Equal(t, true, serviceResponse.GetHasLinkedAccount())
	mockTailsClient.AssertNumberOfCalls(t, "GetLinkedAmazonDirectedId", 1)
	mockMetricReport.AssertNumberOfCalls(t, "PostLatencyMetricWithName", 1)
	// Both a success metric and a linked count metric is emitted
	mockMetricReport.AssertNumberOfCalls(t, "PostCountMetric", 2)

	// Check when there isn't a linked account.
	mockMetricReport.On(
		"PostCountMetric",
		HasLinkedAmazonAccountAPIName,
		ClientString,
		0.0,
		TAILSHasLinkedAccountCount).Return()
	hasLinked = false
	serviceResponse, serviceErr = service.CallHasLinkedAmazonAccount(mockTailsClient, ctx, &serviceRequest)
	assert.Nil(t, serviceErr)
	assert.Equal(t, false, serviceResponse.GetHasLinkedAccount())
	mockTailsClient.AssertNumberOfCalls(t, "GetLinkedAmazonDirectedId", 2)
	mockMetricReport.AssertNumberOfCalls(t, "PostLatencyMetricWithName", 2)
	// Both a success metric and a linked count metric is emitted
	mockMetricReport.AssertNumberOfCalls(t, "PostCountMetric", 4)
}

func TestUnlinkTwitchAccountError(t *testing.T) {
	service := &SonicService{}
	mockTailsClient := &mocks.TailsClient{}

	serviceRequest := st.UnlinkTwitchAccountRequest{
		Tuid,
	}

	recorder := sonictesting.CallRecorder{}
	writer := sonictesting.NewMockWriter(&recorder)
	log.SetOutput(writer)

	ctx := context.WithValue(context.Background(), contextkeys.UserIDCtxKey, Tuid)
	ctx = context.WithValue(ctx, contextkeys.RequestLogKey, log.WithFields(log.Fields{}))
	mockMetricReport := &mocks.MetricsReporter{}
	ctx = context.WithValue(ctx, contextkeys.RequestMetricsReportKey, mockMetricReport)
	mockMetricReport.On(
		"PostLatencyMetricWithName",
		UnlinkTwitchAccountAPIName,
		ClientString,
		TAILSUnlinkLatency,
		mock.AnythingOfType("time.Duration")).Return()
	mockMetricReport.On(
		"PostCountMetric",
		HasLinkedAmazonAccountAPIName,
		ClientString,
		0.0,
		TAILSUnlinkSuccess).Return()

	mockTailsClient.On("UnlinkTwitchAccount", mock.MatchedBy(func(input svc.UnlinkTwitchAccountRequest) bool {
		return *input.TwitchUserID() == Tuid
	})).Return(nil, errors.New("test error"))

	// Check that errors are handled correctly.
	serviceResponse, serviceErr := service.CallUnlinkTwitchAccount(mockTailsClient, ctx, &serviceRequest)
	assert.NotNil(t, serviceErr)
	assert.Nil(t, serviceResponse)
	assert.Equal(t, true, recorder.WasCalled())
	mockTailsClient.AssertNumberOfCalls(t, "UnlinkTwitchAccount", 1)
	mockMetricReport.AssertNumberOfCalls(t, "PostLatencyMetricWithName", 1)
	mockMetricReport.AssertNumberOfCalls(t, "PostCountMetric", 1)
}

func TestHasLinkedAccountError(t *testing.T) {
	service := &SonicService{}
	mockTailsClient := &mocks.TailsClient{}

	serviceRequest := st.HasLinkedAmazonAccountRequest{
		Tuid,
	}

	recorder := sonictesting.CallRecorder{}
	writer := sonictesting.NewMockWriter(&recorder)
	log.SetOutput(writer)

	ctx := context.WithValue(context.Background(), contextkeys.UserIDCtxKey, Tuid)
	ctx = context.WithValue(ctx, contextkeys.RequestLogKey, log.WithFields(log.Fields{}))
	mockMetricReport := &mocks.MetricsReporter{}
	ctx = context.WithValue(ctx, contextkeys.RequestMetricsReportKey, mockMetricReport)
	mockMetricReport.On(
		"PostLatencyMetricWithName",
		HasLinkedAmazonAccountAPIName,
		ClientString,
		TAILSHasLinkedLatency,
		mock.AnythingOfType("time.Duration")).Return()
	mockMetricReport.On(
		"PostCountMetric",
		HasLinkedAmazonAccountAPIName,
		ClientString,
		0.0,
		TAILSHasLinkedSuccess).Return()

	mockTailsClient.On("GetLinkedAmazonDirectedId", mock.MatchedBy(func(input svc.GetLinkedAmazonDirectedIdRequest) bool {
		return *input.TwitchUserId() == Tuid
	})).Return(nil, errors.New("test error"))

	// Check that errors are handled correctly.
	serviceResponse, serviceErr := service.CallHasLinkedAmazonAccount(mockTailsClient, ctx, &serviceRequest)
	assert.NotNil(t, serviceErr)
	assert.Nil(t, serviceResponse)
	assert.Equal(t, true, recorder.WasCalled())
	mockTailsClient.AssertNumberOfCalls(t, "GetLinkedAmazonDirectedId", 1)
	mockMetricReport.AssertNumberOfCalls(t, "PostLatencyMetricWithName", 1)
	mockMetricReport.AssertNumberOfCalls(t, "PostCountMetric", 1)
}

func TestUnlinkTwitchAccountNotAuthed(t *testing.T) {
	service := &SonicService{}
	mockTailsClient := &mocks.TailsClient{}

	serviceRequest := st.UnlinkTwitchAccountRequest{
		Tuid,
	}

	ctx := context.WithValue(context.Background(), contextkeys.UserIDCtxKey, "jank tuid")
	mockMetricReport := &mocks.MetricsReporter{}
	ctx = context.WithValue(ctx, contextkeys.RequestMetricsReportKey, mockMetricReport)

	// Check when a user is not authed an error occurs.
	serviceResponse, serviceErr := service.CallUnlinkTwitchAccount(mockTailsClient, ctx, &serviceRequest)
	assert.NotNil(t, serviceErr)
	assert.Nil(t, serviceResponse)
	mockTailsClient.AssertNumberOfCalls(t, "UnlinkTwitchAccount", 0)
}

func TestHasLinkedAccountNotAuthed(t *testing.T) {
	service := &SonicService{}
	mockTailsClient := &mocks.TailsClient{}

	serviceRequest := st.HasLinkedAmazonAccountRequest{
		Tuid,
	}

	ctx := context.WithValue(context.Background(), contextkeys.UserIDCtxKey, "jank tuid")
	mockMetricReport := &mocks.MetricsReporter{}
	ctx = context.WithValue(ctx, contextkeys.RequestMetricsReportKey, mockMetricReport)

	// Check when a user is not authed an error occurs.
	serviceResponse, serviceErr := service.CallHasLinkedAmazonAccount(mockTailsClient, ctx, &serviceRequest)
	assert.NotNil(t, serviceErr)
	assert.Nil(t, serviceResponse)
	mockTailsClient.AssertNumberOfCalls(t, "GetLinkedAmazonDirectedId", 0)
}
