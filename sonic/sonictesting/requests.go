package sonictesting

import (
	"net/http"
)

type EmptyResponseWriter struct {
}

func (r *EmptyResponseWriter) Header() http.Header {
	return nil
}

func (r *EmptyResponseWriter) Write(body []byte) (int, error) {
	return len(body), nil
}

func (r *EmptyResponseWriter) WriteHeader(status int) {
}
