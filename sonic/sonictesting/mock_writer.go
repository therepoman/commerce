package sonictesting

type MockWriter struct {
	recorder *CallRecorder
}

type CallRecorder struct {
	called bool
}

func NewMockWriter(r *CallRecorder) *MockWriter {
	return &MockWriter{
		recorder: r,
	}
}

func (writer MockWriter) Write(p []byte) (n int, err error) {
	writer.recorder.RecordCall()
	return len(p), nil
}

func (recorder *CallRecorder) RecordCall() {
	recorder.called = true
}

func (recorder *CallRecorder) WasCalled() bool {
	return recorder.called
}

func (recorder *CallRecorder) Reset() {
	recorder.called = false
}
