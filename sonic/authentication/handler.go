package authentication

import (
	"errors"
	"net/http"

	"code.justin.tv/commerce/sonic/config"
	"code.justin.tv/common/goauthorization"
)

type AuthenticationHandler interface {
	ParseToken(r *http.Request) (AuthToken, error)
	ValidateToken(token AuthToken) error
}

type AuthToken interface {
	GetSubject() string
}

type CartmanAuthenticationHandler struct {
	sonicConfig *config.SonicConfiguration
}

func NewCartmanAuthenticationHandler(config *config.SonicConfiguration) *CartmanAuthenticationHandler {
	return &CartmanAuthenticationHandler{
		sonicConfig: config,
	}
}

func (handler *CartmanAuthenticationHandler) ParseToken(r *http.Request) (AuthToken, error) {
	return handler.sonicConfig.GetAuthDecoder().ParseToken(r)
}

func (handler *CartmanAuthenticationHandler) ValidateToken(token AuthToken) error {
	authorizationToken, ok := token.(*goauthorization.AuthorizationToken)
	if ok {
		return handler.sonicConfig.GetAuthDecoder().Validate(authorizationToken, nil)
	} else {
		return errors.New("Invalid Authorization Token.")
	}
}
