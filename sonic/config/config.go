package config

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"os"
	"time"

	"code.justin.tv/commerce/CoralGoClient/src/coral/dialer"
	"code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	"code.justin.tv/commerce/CoralRPCGoSupport/src/coral/rpcv1"
	"code.justin.tv/common/goauthorization"
	"code.justin.tv/systems/sandstorm/manager"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/defaults"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"

	"github.com/sirupsen/logrus"
)

const (
	// Environment represents the environment variable for determining prod, staging, local, etc.
	Environment = "ENVIRONMENT"
	prod        = "production"
	staging     = "staging"
	local       = "local"
	onebox      = "onebox"

	serviceName          = "sonic"
	amazonRootCertSecret = "commerce/AmazonCACerts/production/amazon-root-ca.pem"

	authDecoderAlgorithm = "ES256"
	authDecoderAudience  = "code.justin.tv/web/cartman"
	authDecoderIssuer    = "code.justin.tv/web/cartman"

	stagingAwsRegion = "us-west-2"
	prodAwsRegion    = "us-west-2"

	stagingSwsHostname = "sws-na.amazonpmi.com"
	prodSwsHostname    = "sws-na.amazon.com"

	stagingSwsPort = 443
	prodSwsPort    = 443

	stagingSwsSecret = "commerce/sws/staging/cert"
	prodSwsSecret    = "commerce/sws/production/cert"

	stagingIdentitySecret = "identity/cartman/staging/ecc_public_key"
	prodIdentitySecret    = "identity/cartman/production/ecc_public_key"

	stagingSandstormArn = "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/sonic-staging-role"
	prodSandstormArn    = "arn:aws:iam::734326455073:role/sandstorm/production/templated/role/sonic-prod-role"
)

type SonicConfiguration struct {
	environment               string
	awsRegion                 string
	awsServiceRoleCredentials *credentials.Credentials
	sandstormAwsConfig        *aws.Config
	authDecoder               goauthorization.Decoder
	swsCodec                  codec.RoundTripper
	swsDialer                 dialer.Dialer
}

func (this SonicConfiguration) GetSwsCodec() codec.RoundTripper {
	return this.swsCodec
}

func (this SonicConfiguration) GetSwsDialer() dialer.Dialer {
	return this.swsDialer
}

func (this SonicConfiguration) GetAuthDecoder() goauthorization.Decoder {
	return this.authDecoder
}

func (this SonicConfiguration) GetAwsRegion() string {
	return this.awsRegion
}

func (this SonicConfiguration) GetSandstormAwsCredentials() *credentials.Credentials {
	return this.awsServiceRoleCredentials
}

// GetIsProd determines if the environment is prod
func (this *SonicConfiguration) IsProd() bool {
	return prod == this.environment || this.IsOnebox()
}

// GetIsStaging determines if the environment is staging
func (this *SonicConfiguration) IsStaging() bool {
	return staging == this.environment
}

func (this *SonicConfiguration) IsOnebox() bool {
	return onebox == this.environment
}

// GetIsLocal determines if the environment is local
func (this *SonicConfiguration) IsLocal() bool {
	return local == this.environment
}

func (this *SonicConfiguration) Fetch() error {
	this.fetchAwsRegion()
	this.fetchAwsServiceRoleCredentials()
	this.fetchSandstormAwsConfig()
	this.fetchSwsCodec()
	dialerError := this.fetchSwsDialer()

	if dialerError != nil {
		return dialerError
	}

	decoderError := this.fetchAuthorizationDecoder()
	return decoderError
}

func (this *SonicConfiguration) fetchAuthorizationDecoder() error {
	// Fetch the sandstorm config if its not already been retrieved.
	if this.sandstormAwsConfig == nil {
		this.fetchSandstormAwsConfig()
	}

	sandstormManager := manager.New(manager.Config{
		AWSConfig:   this.sandstormAwsConfig,
		ServiceName: serviceName,
	})

	cartmanPublicKey, sandstormError := sandstormManager.Get(this.fetchIdentitySecret())
	if sandstormError != nil {
		return sandstormError
	}

	decoder, decoderError := goauthorization.NewDecoder(authDecoderAlgorithm, authDecoderAudience, authDecoderIssuer, cartmanPublicKey.Plaintext)
	if decoderError != nil {
		return decoderError
	}

	this.authDecoder = decoder
	return nil
}

func (this *SonicConfiguration) fetchSwsDialer() error {
	// Fetch the sandstorm config if its not already been retrieved.
	if this.sandstormAwsConfig == nil {
		this.fetchSandstormAwsConfig()
	}

	sandstormManager := manager.New(manager.Config{
		AWSConfig:   this.sandstormAwsConfig,
		ServiceName: serviceName,
	})

	swsSecret, sandstormError := sandstormManager.Get(this.fetchSwsCertSecret())

	if sandstormError != nil {
		return sandstormError
	}

	clientCert, certError := tls.X509KeyPair(swsSecret.Plaintext, swsSecret.Plaintext)

	if certError != nil {
		return certError
	}

	rootCert, rootCertError := sandstormManager.Get(amazonRootCertSecret)

	if rootCertError != nil {
		return rootCertError
	}

	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(rootCert.Plaintext)

	tlsconfig := &tls.Config{
		RootCAs:            caCertPool,
		Certificates:       []tls.Certificate{clientCert},
		InsecureSkipVerify: false,
	}

	swsHost, swsPort := this.fetchSwsParams()
	dialer, dialerError := dialer.TLS(swsHost, swsPort, tlsconfig)

	if dialerError != nil {
		return dialerError
	}

	this.swsDialer = dialer
	return nil
}

func (this *SonicConfiguration) fetchSwsCodec() {
	swsHost, _ := this.fetchSwsParams()
	swsCodec := rpcv1.New(swsHost)
	this.swsCodec = swsCodec
}

func (this *SonicConfiguration) fetchAwsServiceRoleCredentials() {
	this.awsServiceRoleCredentials = defaults.CredChain(defaults.Config(), defaults.Handlers())
}

func (this *SonicConfiguration) fetchSandstormAwsConfig() {
	if this.awsRegion == "" {
		this.fetchAwsRegion()
	}

	stsEndpoint := fmt.Sprintf("sts.%s.amazonaws.com", this.awsRegion)
	awsConfig := aws.Config{Region: aws.String(this.awsRegion)}
	stsClientConfig := aws.Config{Region: aws.String(this.awsRegion), Endpoint: aws.String(stsEndpoint)}
	stsclient := sts.New(session.New(&stsClientConfig))

	arp := &stscreds.AssumeRoleProvider{
		Duration:     900 * time.Second,
		ExpiryWindow: 10 * time.Second,
		RoleARN:      this.fetchSandstormRoleArn(),
		Client:       stsclient,
	}

	creds := credentials.NewCredentials(arp)
	awsConfig.WithCredentials(creds)

	this.sandstormAwsConfig = &awsConfig
}

func (this *SonicConfiguration) fetchIdentitySecret() string {
	if this.IsProd() {
		return prodIdentitySecret
	} else {
		return stagingIdentitySecret
	}
}

func (this *SonicConfiguration) fetchSwsCertSecret() string {
	if this.IsProd() {
		return prodSwsSecret
	} else {
		return stagingSwsSecret
	}
}

func (this *SonicConfiguration) fetchSwsParams() (string, uint16) {
	if this.IsProd() {
		return prodSwsHostname, prodSwsPort
	} else {
		return stagingSwsHostname, stagingSwsPort
	}
}

func (this *SonicConfiguration) fetchSandstormRoleArn() string {
	if this.IsProd() {
		return prodSandstormArn
	} else {
		return stagingSandstormArn
	}
}

func (this *SonicConfiguration) fetchAwsRegion() {
	if this.IsProd() {
		this.awsRegion = prodAwsRegion
	} else {
		this.awsRegion = stagingAwsRegion
	}
}

func GetCurrentConfiguration() SonicConfiguration {
	configuration := SonicConfiguration{environment: getCurrentEnvironment()}
	configError := configuration.Fetch()

	if configError != nil {
		logrus.Error("Error getting current configuration.", configError)
		panic(configError)
	}

	return configuration
}

func getCurrentEnvironment() string {
	env := os.Getenv(Environment)
	if env == prod {
		return prod
	} else if env == onebox {
		return onebox
	} else if env == staging {
		return staging
	} else {
		return local
	}
}
