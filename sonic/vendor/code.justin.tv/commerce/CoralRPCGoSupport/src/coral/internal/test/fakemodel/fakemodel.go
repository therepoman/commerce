// Package fakemodel provides fake Coral models for testing
// Copyright 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
package fakemodel

import (
	"code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	"code.justin.tv/commerce/CoralGoModel/src/coral/model"
	"reflect"
)

const (
	serviceName          = "TopListsService"
	topListsAssemblyName = "com.amazon.toplistsservice"
)

func init() {
	// Register our fake shapes
	var val1 FakeInput
	t := reflect.TypeOf(&val1)
	asm := model.LookupService(serviceName).Assembly("rpcv1_test")
	asm.RegisterShape("FakeInput", t, func() interface{} {
		return NewFakeInput()
	})
	var val2 FakeRule
	t = reflect.TypeOf(&val2)
	asm.RegisterShape("FakeRule", t, func() interface{} {
		return NewFakeRule()
	})
	var val3 FakeOutput
	t = reflect.TypeOf(&val3)
	asm.RegisterShape("FakeOutput", t, func() interface{} {
		return NewFakeOutput()
	})
	fakeInput, _ := asm.Shape("FakeInput")
	fakeOutput, _ := asm.Shape("FakeOutput")
	asm.RegisterOp("IsAuthorized", fakeInput, fakeOutput, nil)

	// Register FindTopListInput with Coral
	var val FindTopListInput
	t = reflect.TypeOf(&val)
	model.LookupService(serviceName).Assembly(topListsAssemblyName).RegisterShape("FindTopListInput", t, func() interface{} {
		return NewFindTopListInput()
	})

	// Register FindTopListOutput with Coral
	var tlo FindTopListOutput
	t = reflect.TypeOf(&tlo)
	model.LookupService(serviceName).Assembly(topListsAssemblyName).RegisterShape("FindTopListOutput", t, func() interface{} {
		return NewFindTopListOutput()
	})

	// Register FindTopListInput and FindTopListOutput with the FindTopList operation
	asm = model.LookupService(serviceName).Assembly(topListsAssemblyName)
	input, _ := asm.Shape("FindTopListInput")
	output, _ := asm.Shape("FindTopListOutput")
	asm.RegisterOp("FindTopList", input, output, nil)
}

var (
	// TestService is a stub shape ref for serviceName
	TestService = codec.ShapeRef{
		AsmName:   "rpcv1_test",
		ShapeName: serviceName,
	}
	// TestOperation is a stub shape ref for serviceName.IsAuthorized
	TestOperation = codec.ShapeRef{
		AsmName:   "rpcv1_test",
		ShapeName: "IsAuthorized",
	}
)

// FakeInput  is a dummy implementation of a Coral input model
type FakeInput interface {
	SetUser(v string)
	SetRules(v []FakeRule)
	User() string
	Rules() []FakeRule
}

type _FakeInput struct {
	I_user  string     `coral:"user"`
	I_rules []FakeRule `coral:"rules"`
}

func (f *_FakeInput) SetUser(u string) {
	f.I_user = u
}

func (f *_FakeInput) SetRules(r []FakeRule) {
	f.I_rules = r
}

func (f *_FakeInput) User() string {
	return f.I_user
}

func (f *_FakeInput) Rules() []FakeRule {
	return f.I_rules
}

func NewFakeInput() FakeInput {
	return &_FakeInput{}
}

// FakeRule is a dummy implementation of a Coral model
type FakeRule interface {
	SetSource(string)
	SetIdentifier(string)
}

type _FakeRule struct {
	I_source     string `coral:"source"`
	I_identifier string `coral:"identifier"`
}

func (f *_FakeRule) SetSource(s string) {
	f.I_source = s
}

func (f *_FakeRule) SetIdentifier(i string) {
	f.I_identifier = i
}

func (f *_FakeRule) Source() string {
	return f.I_source
}

func (f *_FakeRule) Identifier() string {
	return f.I_identifier
}

func NewFakeRule() FakeRule {
	return &_FakeRule{}
}

// FakeOutput is a dummy implementation of a Coral Output model
type FakeOutput interface {
	SetOutput(v string)
	Output() string
}

type _FakeOutput struct {
	I_output string `coral:"output"`
}

func (f *_FakeOutput) SetOutput(s string) {
	f.I_output = s
}

func (f *_FakeOutput) Output() string {
	return f.I_output
}

func NewFakeOutput() FakeOutput {
	return &_FakeOutput{}
}

type FindTopListInput interface {
	__type()
	Name() *string
	SetName(v *string)
}
type _FindTopListInput struct {
	Ị_name *string `coral:"name"`
}

func (this *_FindTopListInput) Name() *string {
	return this.Ị_name
}
func (this *_FindTopListInput) SetName(v *string) {
	this.Ị_name = v
}
func (this *_FindTopListInput) __type() {
}
func NewFindTopListInput() FindTopListInput {
	return &_FindTopListInput{}
}

// Test Output
type FindTopListOutput interface {
	__type()
	Name() *string
	SetName(v *string)
}
type _FindTopListOutput struct {
	Ị_name *string `coral:"name"`
}

func (this *_FindTopListOutput) Name() *string {
	return this.Ị_name
}
func (this *_FindTopListOutput) SetName(v *string) {
	this.Ị_name = v
}
func (this *_FindTopListOutput) __type() {
}
func NewFindTopListOutput() FindTopListOutput {
	return &_FindTopListOutput{}
}
