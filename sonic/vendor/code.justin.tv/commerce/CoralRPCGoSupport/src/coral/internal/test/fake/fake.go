// Package fake provides dummy structs and interface implementations for things like AAA or AWS Signers
/* Copyright 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.*/
package fake

import (
	"code.justin.tv/commerce/AAAGo/src/aaa"
	"code.justin.tv/commerce/GoAuthV4/src/authv4"
	"bytes"
	"net/http"
)

var (
	// Signer is an initialized authv4 signer
	Signer = authv4.NewSigner(
		"us-east-1",
		"fakeService",
		"none",
		"none",
		[]string{},
	)
)

// AAA provides a dummy implementation of AAA
type AAA struct {
	AuthResult *aaa.AuthorizationResult
	AuthErr    error
	EncodeErr  error
	DecodeErr  error
}

// SanityCheck satisfies aaa.Support.
func (f *AAA) SanityCheck() error { return nil }

// EncodeRequest satisfies aaa.Support.
func (f *AAA) EncodeRequest(service, operation string, r *http.Request) (*aaa.ClientContext, error) {
	if f.EncodeErr == nil {
		return &aaa.ClientContext{}, nil
	}
	return nil, f.EncodeErr
}

// DecodeResponse satisfies aaa.Support.
func (f *AAA) DecodeResponse(clientCxt *aaa.ClientContext, resp *http.Response) error {
	return f.DecodeErr
}

// DecodeRequest satisfies aaa.Support.
func (f *AAA) DecodeRequest(r *http.Request) (*aaa.ServiceContext, error) {
	if f.DecodeErr == nil {
		return &aaa.ServiceContext{}, nil
	}
	return nil, f.DecodeErr
}

// AuthorizeRequest satisfies aaa.Support.
func (f *AAA) AuthorizeRequest(ctx *aaa.ServiceContext) (*aaa.AuthorizationResult, error) {
	return f.AuthResult, f.AuthErr
}

// EncodeResponse satisfies aaa.Support.
func (f *AAA) EncodeResponse(ctx *aaa.ServiceContext, h http.Header, body []byte) ([]byte, error) {
	return body, f.EncodeErr
}

// Pipe satisfies io.ReadWriter
type Pipe struct {
	ReadBuf  *bytes.Buffer
	WriteBuf bytes.Buffer
}

// Read satisfies io.Reader.
func (f *Pipe) Read(p []byte) (n int, err error) {
	return f.ReadBuf.Read(p)
}

// Write satisfies io.Writer.
func (f *Pipe) Write(p []byte) (n int, err error) {
	return f.WriteBuf.Write(p)
}
