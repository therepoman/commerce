// Copyright 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
package rpcv0

import (
	"code.justin.tv/commerce/AAAGo/src/aaa"
	"bufio"
	"bytes"
	"code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	"fmt"
	"github.com/pkg/errors"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

// RoundTrip executes a send and receive of a request over the wire.
// - Request is what to encode and send over the wire
// - conn is the transport to write the request to and read the response from.
func (c RPCv0) RoundTrip(r *codec.Request, rw io.ReadWriter) error {
	opName := fmt.Sprintf("%s#%s", r.Operation.AsmName, r.Operation.ShapeName)
	svcName := fmt.Sprintf("%s#%s", r.Service.AsmName, r.Service.ShapeName)
	b, err := c.MarshalInput(r.Input, opName, svcName)

	if err != nil {
		return errors.Wrap(err, "could not marshal input in RoundTrip")
	}

	body := bytes.NewBuffer(b)
	request, err := http.NewRequest(http.MethodPost, c.Path, body)
	if err != nil {
		return err
	}

	if c.Host != "" {
		request.Host = c.Host
		request.URL.Host = c.Host
	}

	request.Header.Set(headerAccept, "application/json, text/javascript")
	request.Header.Set(headerContentType, "application/json; charset=UTF-8;")

	if c.SignerV4 != nil {
		c.SignerV4.Sign(request)
	} else {
		request.Header.Set(headerAmznDate, time.Now().UTC().Format(time.RFC822))
	}

	var clientCxt *aaa.ClientContext
	if c.AAAClient != nil {
		clientCxt, err = c.AAAClient.EncodeRequest(r.Service.ShapeName, r.Operation.ShapeName, request)
		if err != nil {
			return errors.Wrap(err, "there was an error using AAA to encode the requset")
		}
	}

	err = request.Write(rw)
	if err != nil {
		return err
	}

	resp, err := http.ReadResponse(bufio.NewReader(rw), request)
	if err != nil {
		return errors.Wrap(err, "could not create a response")
	}

	if c.AAAClient != nil {
		err = c.AAAClient.DecodeResponse(clientCxt, resp)
		if err != nil {
			return errors.Wrap(err, "could not AAA decode the request")
		}
	}

	defer resp.Body.Close()
	respbody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return errors.Wrap(err, "could not read response body")
	}

	if len(respbody) == 0 {
		return nil
	}

	// TODO: what status code is returned for Coral Modeled errors?
	// If an error occurs in the service due to the input containing bad input at the application level
	// and those errors are mapped successfully to a Coral model what is the response code?
	if resp.StatusCode != http.StatusOK {
		return errors.New(string(respbody))
	}

	if r.Output == nil {
		return nil
	}

	return c.UnmarshalOutput(respbody, r.Output)
}
