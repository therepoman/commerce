// Copyright 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
package rpcv0

import (
	cjson "code.justin.tv/commerce/CoralRPCGoSupport/src/coral/rpc/encoding/json"
	"encoding/json"

	"github.com/pkg/errors"
)

// rpcInput is the on-the-wire JSON format that is expected.
//
// Input parsing is delayed until we know what struct we will Unmarshal into.
type rpcInput struct {
	Operation string
	Service   string
	Input     json.RawMessage
}

// rpcOutput is the on-the-wire JSON format. It is assumed that some other
// Marshaling process has Marshaled the output as the Output field is just
// raw bytes
type rpcOutput struct {
	Output  json.RawMessage
	Version string
}

// Marshal and Unmarshal satisfy the coral/codec interface

// Marshal defines how to encode an object into the format used on the wire.
func (c RPCv0) Marshal(obj interface{}) ([]byte, error) {
	return cjson.Marshal(obj)
}

// MarshalOutput marshals the coral model and wraps the resulting []byte in
// a rpcOutput
func (c RPCv0) MarshalOutput(obj interface{}) ([]byte, error) {
	out, err := cjson.Marshal(obj)

	if err != nil {
		return nil, errors.Wrap(err, "could not marshal coral obj")
	}

	b, err := json.Marshal(
		&rpcOutput{Output: out, Version: "1.0"},
	)

	if err != nil {
		return nil, errors.Wrap(err, "could not marshal rpcOutput")
	}

	return b, nil
}

// MarshalInput marshals the Coral model and then wraps the resulting []byte
// in rpcInput
func (c RPCv0) MarshalInput(obj interface{}, operation, service string) ([]byte, error) {
	in, err := c.Marshal(obj)

	if err != nil {
		return nil, errors.Wrap(err, "could not marshal coral obj")
	}

	b, err := json.Marshal(
		&rpcInput{Operation: operation, Service: service, Input: in},
	)

	if err != nil {
		return nil, errors.Wrap(err, "could not marshal rpcInput")
	}

	return b, nil
}

// Unmarshal defines how to decode an object from the format used on the wire.
func (c RPCv0) Unmarshal(d []byte, obj interface{}) error {
	return cjson.Unmarshal(d, obj)
}

// UnmarshalOutput unmarshals an object formatted as rpcOutput
func (c RPCv0) UnmarshalOutput(data []byte, obj interface{}) error {
	out := &rpcOutput{}
	err := json.Unmarshal(data, out)

	if err != nil {
		return errors.Wrap(err, "could not unmarshal data into rpcOutput")
	}

	err = cjson.Unmarshal(out.Output, obj)

	if err != nil {
		return errors.Wrap(err, "could not unmarshal data into coral obj")
	}

	return nil
}

// UnmarshalInput unmarshals an object formatted as rpcInput
func (c RPCv0) UnmarshalInput(data []byte, obj interface{}) error {
	in := &rpcInput{}
	err := json.Unmarshal(data, in)

	if err != nil {
		return errors.Wrap(err, "could not unmarshal data into rpcInput")
	}

	return c.Unmarshal(in.Input, obj)
}
