/* Copyright 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved. */
package sd

import (
	"code.justin.tv/commerce/GoLog/src/log"
	"code.justin.tv/commerce/AAAGo/src/aaa"
	"bytes"
	"github.com/pkg/errors"
	"io"
	"io/ioutil"
	"net/http"
)

// EncodeRequest satisfies aaa.Client
func (sd *Client) EncodeRequest(service, operation string, r *http.Request) (clientCxt *aaa.ClientContext, err error) {
	var payload []byte

	if r.Body != nil {
		defer r.Body.Close()
		payload, err = ioutil.ReadAll(r.Body)
		if err != nil {
			err = errors.Errorf("Failed to read request body. %v", err)
			return
		}
	} else {
		payload = []byte{}
	}
	log.Tracef("http.Header pre EncodeRequest: %+v", r.Header)
	in := &encodeRequestInput{
		payload: payload,
		rpc: &rpcInfo{
			service:   service,
			operation: operation,
		},
		httpRequest: &httpRequestInfo{
			verb: r.Method,
			uri:  r.URL.String(),
		},
		headers:     headers(r.Header),
		messageType: httpRequest,
	}

	retries := 0
	for clientCxt, err = sd.encodeRequest(service, operation, in, r); err != nil && retries < maxRetries; retries++ {
		log.Debug("Retrying call to encodeRequest due to error", err)
		clientCxt, err = sd.encodeRequest(service, operation, in, r)
	}
	if err == nil {
		log.Tracef("http.Header post EncodeRequest: %+v", r.Header)
	}
	return
}

// encodeRequest establishes a connection with the Security Daemon, sends it an encode request, reads the
// response, builds a ClientContext based on the response, and updates the given request with the
// required AAA headers and optionally encrypted body.
func (sd *Client) encodeRequest(service, operation string, in *encodeRequestInput, r *http.Request) (clientCxt *aaa.ClientContext, err error) {
	reqBuff, err := buildReq(&actionEncodeRequest, in)
	if err != nil {
		return
	}

	c, err := sd.Connect()
	if err != nil {
		return
	}

	// Clean up.  If there isn't an error, then return the connection to the pool
	defer func() {
		if err == nil {
			log.Trace("Releasing connection from encodeReqeust", c)
			sd.Release(c)
		} else {
			log.Trace("Closing connection from encodeReqeust", c)
			c.Close()
		}
	}()

	n, err := io.Copy((c), bytes.NewReader(reqBuff))
	log.Trace("Copied", n, "bytes to the socket")
	if err != nil {
		err = errors.Errorf("Failed to write encodeRequest to socket: %v", err)
		return
	}

	out := new(encodeRequestOutput)
	_, _, err = readResp((c), out)
	if err != nil {
		err = errors.Errorf("Failed to read encodeRequestResponse from socket: %v", err)
		return
	}

	clientCxt = &aaa.ClientContext{
		Service:   service,
		Operation: operation,
	}
	if out.key != nil {
		clientCxt.KeyId = string(out.key.id)
		clientCxt.KeyVersion = out.key.version
	}

	r.Header.Add(aaa.AaaAuthHeader, string(out.authHeader))
	if out.dateHeader != "" && r.Header.Get(aaa.AaaDateHeader) != "" {
		r.Header.Add(aaa.AaaDateHeader, out.dateHeader)
	}
	r.Body = ioutil.NopCloser(bytes.NewBuffer(out.payload))
	r.ContentLength = int64(len(out.payload))

	return
}

// DecodeRequest satisfies aaa.Client
func (sd *Client) DecodeResponse(clientCxt *aaa.ClientContext, r *http.Response) (err error) {
	var payload []byte

	if r.Body != nil {
		defer r.Body.Close()
		payload, err = ioutil.ReadAll(r.Body)
		if err != nil {
			err = errors.Errorf("Failed to read request body. %v", err)
			return
		}
	} else {
		payload = []byte{}
	}
	log.Tracef("http.Header pre DecodeResponse: %+v", r.Header)

	in := &decodeResponseInput{
		payload: payload,
		rpc: &rpcInfo{
			service:   clientCxt.Service,
			operation: clientCxt.Operation,
		},
		httpResponse: &httpResponseInfo{
			statusCode: int16(r.StatusCode),
		},
		key: &keyInfo{
			id:      clientCxt.KeyId,
			version: clientCxt.KeyVersion,
		},
		headers:     headers(r.Header),
		messageType: httpRequest,
	}

	retries := 0
	for err = sd.decodeResponse(in, clientCxt, r); err != nil && retries < maxRetries; retries++ {
		log.Debug("Retrying call to decodeResponse due to error", err)
		err = sd.decodeResponse(in, clientCxt, r)
	}
	if err == nil {
		log.Tracef("http.Header post DecodeResponse: %+v", r.Header)
	}
	return
}

// decodeResponse establishes a connection with the Security Daemon, sends it a decode request with
// the given decodeRequestInput
func (sd *Client) decodeResponse(in *decodeResponseInput, clientCxt *aaa.ClientContext, r *http.Response) (err error) {
	reqBuff, err := buildReq(&actionDecodeResponse, in)
	if err != nil {
		return
	}

	c, err := sd.Connect()
	if err != nil {
		return
	}

	// Clean up.  If there isn't an error, then return the connection to the pool
	defer func() {
		if err == nil {
			log.Trace("Releasing connection from decodeResponse", c)
			sd.Release(c)
		} else {
			log.Trace("Closing connection from decodeResponse", c)
			c.Close()
		}
	}()

	n, err := io.Copy((c), bytes.NewReader(reqBuff))
	log.Trace("Copied", n, "bytes to the socket")
	if err != err {
		err = errors.Errorf("Failed to write decodeResponse to socket: %v", err)
		return err
	}

	out := new(decodeResponseOutput)
	_, sl, err := readResp((c), out)
	if err != nil {
		err = errors.Errorf("Failed to read decodeResponseResponse from socket: %v", err)
		return
	}

	if sl.exceptionInfo != nil && sl.exceptionInfo.code > 0 {
		err = errors.Errorf("Exception from SecurityDaemon when decoding response: %+v", sl.exceptionInfo)
		return
	}

	r.Body = ioutil.NopCloser(bytes.NewBuffer(out.payload))
	return
}
