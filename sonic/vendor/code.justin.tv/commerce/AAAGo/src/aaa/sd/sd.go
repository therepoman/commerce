package sd

import (
	"code.justin.tv/commerce/GoLog/src/log"
	"code.justin.tv/commerce/GoApolloEnvironmentInfo/src/apollo"
	"github.com/pkg/errors"
	"io/ioutil"
	"net"
	"os/exec"
	"os/user"
	"path/filepath"
	"strings"
	"sync"
	"time"
)

const (
	dialTimeout = 1 * time.Second
	maxRetries  = 2
)

type Client struct {
	sync.Mutex
	socketPath string
	pool       []net.Conn
}

// Connect returns a socket connection with the Security Daemon.  A connection is
// returned from the connection pool if available, otherwise a new connection is created.
func (sd *Client) Connect() (net.Conn, error) {
	sd.Lock()
	defer sd.Unlock()
	numPooled := len(sd.pool)
	log.Trace("Number of pooled connections", numPooled)
	if numPooled > 0 {
		conn := sd.pool[0]
		copy(sd.pool, sd.pool[1:])
		sd.pool = sd.pool[:numPooled-1]
		log.Trace("Using pooled connection", conn)
		return conn, nil
	}
	c, err := net.DialTimeout("unix", sd.socketPath, dialTimeout)
	if err != nil {
		return nil, errors.Errorf("Failed to establish connection to AAASecurityDaemon with path %v: %v", sd.socketPath, err)
	}
	log.Trace("Created new connection", c)
	return c, nil
}

// Release puts the given connection back into the pool if it is not null.
// Connections that have caused an error should not be released.
func (sd *Client) Release(conn net.Conn) {
	if conn == nil {
		return
	}
	sd.Lock()
	defer sd.Unlock()
	log.Trace("Returning connection to the pool", conn)
	sd.pool = append(sd.pool, conn)
}

func NewSDClient(options ...func(*Client) error) (*Client, error) {
	sd := &Client{pool: make([]net.Conn, 0, 5)}
	for _, option := range options {
		err := option(sd)
		if err != nil {
			log.Errorf("Error creating AAASecurityDaemon client. Error while setting options: %+v", err)
			return nil, errors.Errorf("Error creating AAASecurityDaemon client. Error while setting options: %v", err)
		}
	}
	if sd.socketPath == "" {
		log.Error("Error creating AAASecurityDaemon client. The socketPath must be specified.")
		return nil, errors.Errorf("Error creating AAASecurityDaemon client. The socketPath must be specified.")
	}
	return sd, nil
}

// StaticSocketPath is a constructor option that allows you to specify what path to use
// for the socket.
func StaticSocketPath(path string) func(*Client) error {
	return func(sd *Client) error {
		sd.socketPath = path
		return nil
	}
}

// SocketPathAutodiscovery is a constructor option that tells the client to attempt to determine the
// socket path to use from the current Apollo environment.  If no environment is found, then an error
// is returned which causes initialization of the client to fail.
func SocketPathAutodiscovery() func(*Client) error {
	return func(sd *Client) error {
		root, err := getEnvRoot()
		if err != nil {
			return errors.Errorf("Unable to determine root: %v", err)
		}
		log.Debug("Using root", root)
		beSockPath := filepath.Join(root, "var", "state", "AAA", "be.sock.txt")
		log.Debug("Reading socket file at", beSockPath)
		buff, err := ioutil.ReadFile(beSockPath)
		if err != nil {
			errors.Errorf("Unable to read the be.sock.txt file located at %v: %v", beSockPath, err)
		}
		sd.socketPath = string(buff)
		log.Debug("Set socketPath to", sd.socketPath)
		return nil
	}
}

func getEnvRoot() (string, error) {
	// First attempt to pull the environment root from Apollo.
	if env, err := apollo.CurEnvironment().PrimaryEnvironment(); err == nil {
		// If we have an environment, then we need to be able to discover the root.
		if root, err := env.Root(); err != nil {
			return "", errors.Errorf("Unable to determine root of primary Apollo environment: %v", err)
		} else {
			return root, nil
		}
	} else {
		log.Info("Unable to discover primary Apollo environment")
	}

	// Next try to pull the environment root for a desktop
	return getWorkspaceEnvRoot()

	// TODO: Attempt to pull the environmnet root for TOD.
}

func getWorkspaceEnvRoot() (string, error) {
	workspace, err := execBrazilPath("workspace-root")
	if err != nil {
		return "", errors.Wrap(err, "Unable to determine workspace-root")
	}

	packageName, err := execBrazilPath("package-name")
	if err != nil {
		return "", errors.Wrap(err, "Unable to determine package-name")
	}

	usr, err := user.Current()
	if err != nil {
		return "", errors.Wrap(err, "Unable to determine current user")
	}

	root := strings.TrimSpace(workspace) + "/AAA/" + strings.TrimSpace(packageName) + "-" + usr.Username
	return root, nil
}

func execBrazilPath(recipe string) (string, error) {
	cmd := exec.Command("/apollo/env/SDETools/bin/brazil-path", recipe)
	log.Debug("Executing", cmd.Path, recipe)
	output, err := cmd.Output()
	if err != nil {
		return "", errors.Wrap(err, "Unable to read from stdout")
	}
	return string(output), nil
}

func (sd *Client) SanityCheck() error {
	c, err := sd.Connect()
	if c != nil {
		c.Close()
	}
	return err
}
