package sd

import (
	"code.justin.tv/commerce/GoLog/src/log"
	"aaa/internal/msgpack"
	"encoding/binary"
	"github.com/pkg/errors"
	"net/http"
)

type messageType int8

const (
	httpRequest messageType = 0
)

type rpcInfo struct {
	service   string
	operation string
}

func (rpc *rpcInfo) EncodeMsgPack(e *msgpack.Encoder) error {
	if err := e.EncodeOuterArrayStart(2); err != nil {
		return err
	}
	if err := e.EncodeStringField(1, rpc.service); err != nil {
		return err
	}
	return e.EncodeStringField(2, rpc.operation)
}

func (rpc *rpcInfo) DecodeMsgPack(d *msgpack.Decoder) error {
	outerLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode rpcInfo as outer array cannot be decoded: %v", err)
	}
	requiredFieldLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode rpcInfo as requiredFields array cannot be decoded: %v", err)
	}
	if requiredFieldLen != 0 {
		return errors.Errorf("Cannot decode rpcInfo as the requiredFields array length is %v. Expected is %v.", requiredFieldLen, 0)
	}

	for i := uint32(1); i < outerLen; i++ {
		innerLen, err := d.DecodeArrayLen()
		if err != nil {
			return errors.Errorf("Cannot decode rpcInfo as the inner array at entry %v cannot be decoded: %v", i, err)
		}
		if innerLen != 2 {
			return errors.Errorf("Cannot decode rpcInfo as the inner array at entry %v has a length of %v. Expected is %v.", i, innerLen, 0)
		}
		index, err := d.DecodeUint()
		if err != nil {
			return errors.Errorf("Cannot decode rpcInfo as the index in inner array entry %v cannot be decoded: %v", i, err)
		}
		switch index {
		case 1:
			rpc.service, err = d.DecodeString()
			if err != nil {
				return errors.Errorf("Cannot decode rpcInfo as the string in index %v cannot be decoded: %v", i, err)
			}
		case 2:
			rpc.operation, err = d.DecodeString()
			if err != nil {
				return errors.Errorf("Cannot decode rpcInfo as the string in index %v cannot be decoded: %v", i, err)
			}
		default:
			continue // OK, don't want to break if AAA adds extra fields.
		}
	}
	return nil
}

type httpRequestInfo struct {
	verb string
	uri  string
}

func (reqInfo *httpRequestInfo) EncodeMsgPack(e *msgpack.Encoder) error {
	if err := e.EncodeOuterArrayStart(2); err != nil {
		return err
	}
	if err := e.EncodeStringField(1, reqInfo.verb); err != nil {
		return err
	}
	return e.EncodeStringField(2, reqInfo.uri)
}

type httpResponseInfo struct {
	statusCode int16
}

func (respInfo *httpResponseInfo) EncodeMsgPack(e *msgpack.Encoder) error {
	e.EncodeOuterArrayStart(1)
	return e.EncodeInt16Field(1, respInfo.statusCode)
}

type headers http.Header

func (h headers) EncodeMsgPack(e *msgpack.Encoder) error {
	if err := e.EncodeArrayLen(uint32(len(h))); err != nil {
		return err
	}

	for k, v := range h {
		if err := e.EncodeOuterArrayStart(2); err != nil {
			return err
		}
		if err := e.EncodeStringField(1, k); err != nil {
			return err
		}
		if err := e.EncodeStringField(2, v[0]); err != nil {
			return err
		}
	}
	return nil
}

type keyInfo struct {
	version uint64
	id      string
}

func (ki *keyInfo) EncodeMsgPack(e *msgpack.Encoder) error {
	if err := e.EncodeOuterArrayStart(2); err != nil {
		return err
	}
	if err := e.EncodeUInt64Field(1, ki.version); err != nil {
		return err
	}
	return e.EncodeStringField(2, ki.id)
}

func (ki *keyInfo) DecodeMsgPack(d *msgpack.Decoder) error {
	outerLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode keyInfo as outer array cannot be decoded: %v", err)
	}
	requiredFieldLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode keyInfo as requiredFields array cannot be decoded: %v", err)
	}
	if requiredFieldLen != 0 {
		return errors.Errorf("Cannot decode keyInfo as the requiredFields array length is %v. Expected is %v.", requiredFieldLen, 0)
	}

	for i := uint32(1); i < outerLen; i++ {
		innerLen, err := d.DecodeArrayLen()
		if err != nil {
			return errors.Errorf("Cannot decode keyInfo as the inner array at entry %v cannot be decoded: %v", i, err)
		}
		if innerLen != 2 {
			return errors.Errorf("Cannot decode keyInfo as the inner array at entry %v has a length of %v. Expected is %v.", i, innerLen, 0)
		}
		index, err := d.DecodeUint()
		if err != nil {
			return errors.Errorf("Cannot decode keyInfo as the index in inner array entry %v cannot be decoded: %v", i, err)
		}
		switch index {
		case 1:
			ki.version, err = d.DecodeUint()
			if err != nil {
				return errors.Errorf("Cannot decode keyInfo as the uint64 in index %v cannot be decoded: %v", i, err)
			}
		case 2:
			ki.id, err = d.DecodeString()
			if err != nil {
				return errors.Errorf("Cannot decode keyInfo as the string in index %v cannot be decoded: %v", i, err)
			}
		default:
			continue // OK, don't want to break if AAA adds extra fields.
		}
	}
	return nil
}

type authInfo struct {
	signed    bool
	encrypted bool
}

func (ki *authInfo) EncodeMsgPack(e *msgpack.Encoder) error {
	if err := e.EncodeOuterArrayStart(2); err != nil {
		return err
	}
	if err := e.EncodeBoolField(1, ki.signed); err != nil {
		return err
	}
	return e.EncodeBoolField(2, ki.encrypted)
}

func (ki *authInfo) DecodeMsgPack(d *msgpack.Decoder) error {
	log.Trace("Decoding authInfo")
	outerLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode authInfo as outer array cannot be decoded: %v", err)
	}
	requiredFieldLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode authInfo as requiredFields array cannot be decoded: %v", err)
	}
	if requiredFieldLen != 0 {
		return errors.Errorf("Cannot decode authInfo as the requiredFields array length is %v. Expected is %v.", requiredFieldLen, 0)
	}

	log.Trace("authInfo outerLen", outerLen)
	for i := uint32(1); i < outerLen; i++ {
		innerLen, err := d.DecodeArrayLen()
		if err != nil {
			return errors.Errorf("Cannot decode authInfo as the inner array at entry %v cannot be decoded: %v", i, err)
		}
		if innerLen != 2 {
			return errors.Errorf("Cannot decode authInfo as the inner array at entry %v has a length of %v. Expected is %v.", i, innerLen, 0)
		}
		index, err := d.DecodeUint()
		if err != nil {
			return errors.Errorf("Cannot decode authInfo as the index in inner array entry %v cannot be decoded: %v", i, err)
		}
		log.Trace("authInfo index", index)
		switch index {
		case 1:
			ki.signed, err = d.DecodeBool()
			if err != nil {
				return errors.Errorf("Cannot decode authInfo as the bool in index %v cannot be decoded: %v", i, err)
			}
		case 2:
			ki.encrypted, err = d.DecodeBool()
			if err != nil {
				return errors.Errorf("Cannot decode authInfo as the bool in index %v cannot be decoded: %v", i, err)
			}
		default:
			continue // OK, don't want to break if AAA adds extra fields.
		}
	}
	return nil
}

type encodeRequestInput struct {
	payload     []byte
	rpc         *rpcInfo
	httpRequest *httpRequestInfo
	headers     headers
	messageType messageType
}

func (in *encodeRequestInput) EncodeMsgPack(e *msgpack.Encoder) error {
	if err := e.EncodeOuterArrayStart(5); err != nil {
		return err
	}
	if err := e.EncodeBytesField(1, in.payload); err != nil {
		return err
	}
	if err := e.EncodeSubField(2, in.rpc); err != nil {
		return err
	}
	if err := e.EncodeSubField(3, in.httpRequest); err != nil {
		return err
	}
	if err := e.EncodeSubField(4, in.headers); err != nil {
		return err
	}
	return e.EncodeUInt8Field(5, uint8(in.messageType))
}

type encodeRequestOutput struct {
	payload         []byte
	key             *keyInfo
	hasRelationship bool
	authHeader      string
	dateHeader      string
}

func (out *encodeRequestOutput) DecodeMsgPack(d *msgpack.Decoder) error {
	outerLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode encodeRequestOutput as outer array cannot be decoded: %v", err)
	}
	requiredFieldLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode encodeRequestOutput as requiredFields array cannot be decoded: %v", err)
	}
	if requiredFieldLen != 0 {
		return errors.Errorf("Cannot decode encodeRequestOutput as the requiredFields array length is %v. Expected is %v.", requiredFieldLen, 0)
	}

	for i := uint32(1); i < outerLen; i++ {
		innerLen, err := d.DecodeArrayLen()
		if err != nil {
			return errors.Errorf("Cannot decode encodeRequestOutput as the inner array at entry %v cannot be decoded: %v", i, err)
		}
		if innerLen != 2 {
			return errors.Errorf("Cannot decode encodeRequestOutput as the inner array at entry %v has a length of %v. Expected is %v.", i, innerLen, 2)
		}
		index, err := d.DecodeUint()
		if err != nil {
			return errors.Errorf("Cannot decode encodeRequestOutput as the index in inner array entry %v cannot be decoded: %v", i, err)
		}
		switch index {
		case 1:
			out.payload, err = d.DecodeBytes()
			if err != nil {
				return errors.Errorf("Cannot decode encodeRequestOutput as the []byte in index %v cannot be decoded: %v", i, err)
			}
		case 2:
			ki := new(keyInfo)
			err = ki.DecodeMsgPack(d)
			if err != nil {
				return errors.Errorf("Cannot decode encodeRequestOutput as the keyInfo in index %v cannot be decoded: %v", i, err)
			}
			out.key = ki
		case 3:
			out.hasRelationship, err = d.DecodeBool()
			if err != nil {
				return errors.Errorf("Cannot decode encodeRequestOutput as the bool in index %v cannot be decoded: %v", i, err)
			}
		case 4:
			out.authHeader, _ = d.DecodeString()
			if err != nil {
				return errors.Errorf("Cannot decode encodeRequestOutput as the string in index %v cannot be decoded: %v", i, err)
			}
		case 5:
			out.dateHeader, _ = d.DecodeString()
			if err != nil {
				return errors.Errorf("Cannot decode encodeRequestOutput as the string in index %v cannot be decoded: %v", i, err)
			}
		default:
			continue // OK, don't want to break if AAA adds extra fields.
		}
	}
	return nil
}

type decodeRequestInput struct {
	payload            []byte
	httpRequest        *httpRequestInfo
	headers            headers
	replayTimeOverride uint64
	messageType        messageType
}

func (in *decodeRequestInput) EncodeMsgPack(e *msgpack.Encoder) error {
	if err := e.EncodeOuterArrayStart(5); err != nil {
		return err
	}
	if err := e.EncodeBytesField(1, in.payload); err != nil {
		return err
	}
	if err := e.EncodeSubField(2, in.httpRequest); err != nil {
		return err
	}
	if err := e.EncodeSubField(3, in.headers); err != nil {
		return err
	}
	if err := e.EncodeUInt64Field(4, in.replayTimeOverride); err != nil {
		return err
	}
	return e.EncodeUInt8Field(5, uint8(in.messageType))
}

type decodeRequestOutput struct {
	payload                  []byte
	remoteIdentityId         []byte
	remoteIdentityName       []byte
	remoteIdentityClientName []byte
	auth                     *authInfo
	rpc                      *rpcInfo
	key                      *keyInfo
}

func (out *decodeRequestOutput) DecodeMsgPack(d *msgpack.Decoder) error {
	log.Trace("decoding request output")
	outerLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode decodeRequestOutput as outer array cannot be decoded: %v", err)
	}
	requiredFieldLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode decodeRequestOutput as requiredFields array cannot be decoded: %v", err)
	}
	if requiredFieldLen != 0 {
		return errors.Errorf("Cannot decode decodeRequestOutput as the requiredFields array length is %v. Expected is %v.", requiredFieldLen, 0)
	}
	log.Trace("decodeRequestOutput outerLen", outerLen)

	for i := uint32(1); i < outerLen; i++ {
		innerLen, err := d.DecodeArrayLen()
		if err != nil {
			return errors.Errorf("Cannot decode decodeRequestOutput as the inner array at entry %v cannot be decoded: %v", i, err)
		}
		if innerLen != 2 {
			return errors.Errorf("Cannot decode decodeRequestOutput as the inner array at entry %v has a length of %v. Expected is %v.", i, innerLen, 2)
		}
		index, err := d.DecodeUint()
		if err != nil {
			return errors.Errorf("Cannot decode decodeRequestOutput as the index in inner array entry %v cannot be decoded: %v", i, err)
		}
		log.Trace("Decoding index", index)
		switch index {
		case 1:
			out.payload, err = d.DecodeBytes()
			if err != nil {
				return errors.Errorf("Cannot decode decodeRequestOutput as the []byte in index %v cannot be decoded: %v", i, err)
			}
		case 2:
			out.remoteIdentityId, err = d.DecodeBytes()
			if err != nil {
				return errors.Errorf("Cannot decode decodeRequestOutput as the []byte in index %v cannot be decoded: %v", i, err)
			}
		case 3:
			out.remoteIdentityName, err = d.DecodeBytes()
			if err != nil {
				return errors.Errorf("Cannot decode decodeRequestOutput as the []byte in index %v cannot be decoded: %v", i, err)
			}
		case 4:
			out.remoteIdentityClientName, err = d.DecodeBytes()
			if err != nil {
				return errors.Errorf("Cannot decode decodeRequestOutput as the []byte in index %v cannot be decoded: %v", i, err)
			}
		case 5:
			ai := new(authInfo)
			err = ai.DecodeMsgPack(d)
			if err != nil {
				return errors.Errorf("Cannot decode decodeRequestOutput as the authInfo in index %v cannot be decoded: %v", i, err)
			}
			out.auth = ai
		case 6:
			rpc := new(rpcInfo)
			err = rpc.DecodeMsgPack(d)
			if err != nil {
				return errors.Errorf("Cannot decode decodeRequestOutput as the rpcInfo in index %v cannot be decoded: %v", i, err)
			}
			out.rpc = rpc
		case 7:
			ki := new(keyInfo)
			err = ki.DecodeMsgPack(d)
			if err != nil {
				return errors.Errorf("Cannot decode decodeRequestOutput as the keyInfo in index %v cannot be decoded: %v", i, err)
			}
			out.key = ki
		default:
			continue // OK, don't want to break if AAA adds extra fields.
		}
	}
	return nil
}

type encodeResponseInput struct {
	payload          []byte
	rpc              *rpcInfo
	httpResponse     *httpResponseInfo
	remoteIdentityId []byte
	key              *keyInfo
	headers          headers
	messageType      messageType
}

func (in *encodeResponseInput) EncodeMsgPack(e *msgpack.Encoder) error {
	if err := e.EncodeOuterArrayStart(7); err != nil {
		return err
	}

	if err := e.EncodeBytesField(1, in.payload); err != nil {
		return err
	}
	if err := e.EncodeSubField(2, in.rpc); err != nil {
		return err
	}
	if err := e.EncodeSubField(3, in.httpResponse); err != nil {
		return err
	}
	if err := e.EncodeBytesField(4, in.remoteIdentityId); err != nil {
		return err
	}
	if err := e.EncodeSubField(5, in.key); err != nil {
		return err
	}
	if err := e.EncodeSubField(6, in.headers); err != nil {
		return err
	}
	return e.EncodeUInt8Field(7, uint8(in.messageType))
}

type encodeResponseOutput struct {
	payload    []byte
	authHeader string
}

func (out *encodeResponseOutput) DecodeMsgPack(d *msgpack.Decoder) error {
	outerLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode encodeResponseOutput as outer array cannot be decoded: %v", err)
	}
	requiredFieldLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode encodeResponseOutput as requiredFields array cannot be decoded: %v", err)
	}
	if requiredFieldLen != 0 {
		return errors.Errorf("Cannot decode encodeResponseOutput as the requiredFields array length is %v. Expected is %v.", requiredFieldLen, 0)
	}

	for i := uint32(1); i < outerLen; i++ {
		innerLen, err := d.DecodeArrayLen()
		if err != nil {
			return errors.Errorf("Cannot decode encodeResponseOutput as the inner array at entry %v cannot be decoded: %v", i, err)
		}
		if innerLen != 2 {
			return errors.Errorf("Cannot decode encodeResponseOutput as the inner array at entry %v has a length of %v. Expected is %v.", i, innerLen, 2)
		}
		index, err := d.DecodeUint()
		if err != nil {
			return errors.Errorf("Cannot decode encodeResponseOutput as the index in inner array entry %v cannot be decoded: %v", i, err)
		}
		switch index {
		case 1:
			out.payload, err = d.DecodeBytes()
			if err != nil {
				return errors.Errorf("Cannot decode encodeResponseOutput as the []byte in index %v cannot be decoded: %v", i, err)
			}
		case 2:
			out.authHeader, _ = d.DecodeString()
			if err != nil {
				return errors.Errorf("Cannot decode encodeResponseOutput as the string in index %v cannot be decoded: %v", i, err)
			}
		default:
			continue // OK, don't want to break if AAA adds extra fields.
		}
	}
	return nil
}

type decodeResponseInput struct {
	payload      []byte
	rpc          *rpcInfo
	httpResponse *httpResponseInfo
	key          *keyInfo
	headers      headers
	messageType  messageType
}

func (in *decodeResponseInput) EncodeMsgPack(e *msgpack.Encoder) error {
	if err := e.EncodeOuterArrayStart(6); err != nil {
		return err
	}

	if err := e.EncodeBytesField(1, in.payload); err != nil {
		return err
	}
	if err := e.EncodeSubField(2, in.rpc); err != nil {
		return err
	}
	if err := e.EncodeSubField(3, in.httpResponse); err != nil {
		return err
	}
	if err := e.EncodeSubField(4, in.key); err != nil {
		return err
	}
	if err := e.EncodeSubField(5, in.headers); err != nil {
		return err
	}
	return e.EncodeUInt8Field(6, uint8(in.messageType))
}

type decodeResponseOutput struct {
	payload []byte
}

func (out *decodeResponseOutput) DecodeMsgPack(d *msgpack.Decoder) error {
	outerLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode decodeResponseOutput as outer array cannot be decoded: %v", err)
	}
	requiredFieldLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode decodeResponseOutput as requiredFields array cannot be decoded: %v", err)
	}
	if requiredFieldLen != 0 {
		return errors.Errorf("Cannot decode decodeResponseOutput as the requiredFields array length is %v. Expected is %v.", requiredFieldLen, 0)
	}

	for i := uint32(1); i < outerLen; i++ {
		innerLen, err := d.DecodeArrayLen()
		if err != nil {
			return errors.Errorf("Cannot decode decodeResponseOutput as the inner array at entry %v cannot be decoded: %v", i, err)
		}
		if innerLen != 2 {
			return errors.Errorf("Cannot decode decodeResponseOutput as the inner array at entry %v has a length of %v. Expected is %v.", i, innerLen, 2)
		}
		index, err := d.DecodeUint()
		if err != nil {
			return errors.Errorf("Cannot decode decodeResponseOutput as the index in inner array entry %v cannot be decoded: %v", i, err)
		}
		switch index {
		case 1:
			out.payload, err = d.DecodeBytes()
			if err != nil {
				return errors.Errorf("Cannot decode decodeResponseOutput as the []byte in index %v cannot be decoded: %v", i, err)
			}
		default:
			continue // OK, don't want to break if AAA adds extra fields.
		}
	}
	return nil
}

type authorizeRequestInput struct {
	rpc              *rpcInfo
	remoteIdentityId []byte
	auth             *authInfo
}

func (in *authorizeRequestInput) EncodeMsgPack(e *msgpack.Encoder) error {
	if err := e.EncodeOuterArrayStart(3); err != nil {
		return err
	}

	if err := e.EncodeSubField(1, in.rpc); err != nil {
		return err
	}
	if err := e.EncodeBytesField(2, in.remoteIdentityId); err != nil {
		return err
	}
	return e.EncodeSubField(3, in.auth)
}

type authorizeRequestOutput struct {
	// Spec says that the code should be an int8, but it's actually encoded as a raw byte.
	authCode byte
	message  []byte
}

func (out *authorizeRequestOutput) DecodeMsgPack(d *msgpack.Decoder) error {
	outerLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode authorizeRequestOutput as outer array cannot be decoded: %v", err)
	}
	requiredFieldLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode authorizeRequestOutput as requiredFields array cannot be decoded: %v", err)
	}
	if requiredFieldLen != 0 {
		return errors.Errorf("Cannot decode authorizeRequestOutput as the requiredFields array length is %v. Expected is %v.", requiredFieldLen, 0)
	}

	for i := uint32(1); i < outerLen; i++ {
		innerLen, err := d.DecodeArrayLen()
		if err != nil {
			return errors.Errorf("Cannot decode authorizeRequestOutput as the inner array at entry %v cannot be decoded: %v", i, err)
		}
		if innerLen != 2 {
			return errors.Errorf("Cannot decode authorizeRequestOutput as the inner array at entry %v has a length of %v. Expected is %v.", i, innerLen, 2)
		}
		index, err := d.DecodeUint()
		if err != nil {
			return errors.Errorf("Cannot decode authorizeRequestOutput as the index in inner array entry %v cannot be decoded: %v", i, err)
		}
		log.Trace("Reading index", index)
		switch index {
		case 1:
			out.authCode, err = d.DecodeByte()
			if err != nil {
				return errors.Errorf("Cannot decode authorizeRequestOutput as the int8 in index %v cannot be decoded: %v", i, err)
			}
		case 2:
			out.message, err = d.DecodeBytes()
			if err != nil {
				return errors.Errorf("Cannot decode authorizeRequestOutput as the []byte in index %v cannot be decoded: %v", i, err)
			}
		default:
			continue // OK, don't want to break if AAA adds extra fields.
		}
	}
	return nil
}

func uint16Bytes(v uint16) []byte {
	bs := make([]byte, 2)
	binary.BigEndian.PutUint16(bs, v)
	return bs
}

func uint32Bytes(v uint32) []byte {
	bs := make([]byte, 4)
	binary.BigEndian.PutUint32(bs, v)
	return bs
}
