/* Copyright 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved. */
package sd

import (
	"code.justin.tv/commerce/GoLog/src/log"
	"code.justin.tv/commerce/AAAGo/src/aaa"
	"bytes"
	"github.com/pkg/errors"
	"io"
	"io/ioutil"
	"net/http"
	"runtime/debug"
)

// DecodeRequest satisfies aaa.Server
func (sd *Client) DecodeRequest(r *http.Request) (serverCxt *aaa.ServiceContext, err error) {
	var payload []byte

	if r.Body != nil {
		defer r.Body.Close()
		payload, err = ioutil.ReadAll(r.Body)
		if err != nil {
			err = errors.Wrap(err, "Failed to read request body")
			return
		}
	} else {
		payload = []byte{}
	}
	log.Tracef("http.Header pre DecodeRequest: %+v", r.Header)
	in := &decodeRequestInput{
		payload: payload,
		httpRequest: &httpRequestInfo{
			verb: r.Method,
			uri:  r.URL.String(),
		},
		headers:            headers(r.Header),
		replayTimeOverride: 0,
		messageType:        httpRequest,
	}

	retries := 0
	for serverCxt, err = sd.decodeRequest(in, r); err != nil && retries < maxRetries; retries++ {
		log.Debug("Retrying call to decodeRequest due to error", err)
		serverCxt, err = sd.decodeRequest(in, r)
	}
	if err == nil {
		log.Tracef("http.Header post DecodeRequest: %+v", r.Header)
	}
	return
}

func (sd *Client) decodeRequest(in *decodeRequestInput, r *http.Request) (serverCxt *aaa.ServiceContext, err error) {
	defer func() {
		if rec := recover(); rec != nil {
			log.Fatalf("Panicked while decoding request: %+v\n%s", rec, string(debug.Stack()))
		}
	}()
	reqBuff, err := buildReq(&actionDecodeRequest, in)
	if err != nil {
		return
	}

	c, err := sd.Connect()
	if err != nil {
		return
	}

	// Clean up.  If there isn't an error, then return the connection to the pool
	defer func() {
		if err == nil {
			log.Trace("Releasing connection from decodeRequest", c)
			sd.Release(c)
		} else {
			log.Trace("Closing connection from decodeRequest", c)
			c.Close()
		}
	}()

	n, err := io.Copy((c), bytes.NewReader(reqBuff))
	log.Trace("Copied", n, "bytes to the socket")
	if err != nil {
		err = errors.Wrap(err, "Failed to write decodeRequest to socket")
		return
	}

	out := new(decodeRequestOutput)
	_, _, err = readResp((c), out)
	if err != nil {
		err = errors.Wrap(err, "Failed to read decodeRequestOutput from socket")
		return
	}

	serverCxt = &aaa.ServiceContext{}
	if out.auth != nil {
		serverCxt.Signed = out.auth.signed
		serverCxt.Encrypted = out.auth.encrypted
	}
	if out.remoteIdentityId != nil {
		serverCxt.RemoteIdentity = string(out.remoteIdentityId)
	}
	if out.key != nil {
		serverCxt.KeyId = string(out.key.id)
		serverCxt.KeyVersion = out.key.version
	}

	r.Body = ioutil.NopCloser(bytes.NewBuffer(out.payload))
	r.ContentLength = int64(len(out.payload))

	return
}

// AuthorizeRequest satisfies aaa.Server
func (sd *Client) AuthorizeRequest(ctx *aaa.ServiceContext) (result *aaa.AuthorizationResult, err error) {
	in := &authorizeRequestInput{
		rpc:              &rpcInfo{service: ctx.Service, operation: ctx.Operation},
		remoteIdentityId: []byte(ctx.RemoteIdentity),
		auth:             &authInfo{signed: ctx.Signed, encrypted: ctx.Encrypted},
	}
	log.Tracef("AuthorizeRequest input: %#v", in)

	retries := 0
	for result, err = sd.authorizeRequest(in); err != nil && retries < maxRetries; retries++ {
		log.Debug("Retrying call to authorizeRequest due to error", err)
		result, err = sd.authorizeRequest(in)
	}
	return
}

func (sd *Client) authorizeRequest(in *authorizeRequestInput) (*aaa.AuthorizationResult, error) {
	defer func() {
		if rec := recover(); rec != nil {
			log.Fatalf("Panicked while authorizing request: %+v\n%s", rec, string(debug.Stack()))
		}
	}()
	reqBuff, err := buildReq(&actionAuthRequest, in)
	if err != nil {
		return nil, err
	}

	c, err := sd.Connect()
	if err != nil {
		return nil, err
	}

	// Clean up.  If there isn't an error, then return the connection to the pool
	defer func() {
		if err == nil {
			log.Trace("Releasing connection from authorizeRequest", c)
			sd.Release(c)
		} else {
			log.Trace("Closing connection from authorizeRequest", c)
			c.Close()
		}
	}()

	n, err := io.Copy((c), bytes.NewReader(reqBuff))
	log.Trace("Copied", n, "bytes to the socket")
	if err != nil {
		err = errors.Wrap(err, "Failed to write authorizeRequest to socket")
		return nil, err
	}

	out := new(authorizeRequestOutput)
	_, _, err = readResp((c), out)
	if err != nil {
		err = errors.Wrap(err, "Failed to read authorizeRequestOutput from socket")
		return nil, err
	}
	code := aaa.AuthFromByteCode(out.authCode)
	authorized := code == aaa.AuthorizationCodePassThrough || code == aaa.AuthorizationCodeAuthorized
	msg := string(out.message)
	log.Trace("Found code", code, "and authorized", authorized, "with message", msg)

	return &aaa.AuthorizationResult{AuthorizationCode: code, Authorized: authorized, ErrorMessage: msg}, nil
}

// EncodeResponse satisfies aaa.Server
func (sd *Client) EncodeResponse(ctx *aaa.ServiceContext, h http.Header, payload []byte) (encoded []byte, err error) {
	in := &encodeResponseInput{
		payload:          payload,
		rpc:              &rpcInfo{service: ctx.Service, operation: ctx.Operation},
		httpResponse:     &httpResponseInfo{statusCode: int16(200)},
		remoteIdentityId: []byte(ctx.RemoteIdentity),
		key:              &keyInfo{id: ctx.KeyId, version: ctx.KeyVersion},
		headers:          headers(h),
		messageType:      httpRequest,
	}
	log.Tracef("EncodeResponse input: %#v", in)

	retries := 0
	for encoded, err = sd.encodeResponse(in, h, payload); err != nil && retries < maxRetries; retries++ {
		log.Debug("Retrying call to encodeResponse due to error", err)
		encoded, err = sd.encodeResponse(in, h, payload)
	}
	return
}

func (sd *Client) encodeResponse(in *encodeResponseInput, h http.Header, payload []byte) ([]byte, error) {
	defer func() {
		if rec := recover(); rec != nil {
			log.Fatalf("Panicked while encoding response: %+v\n%s", rec, string(debug.Stack()))
		}
	}()

	reqBuff, err := buildReq(&actionEncodeResponse, in)
	if err != nil {
		return nil, err
	}

	c, err := sd.Connect()
	if err != nil {
		return nil, err
	}

	// Clean up.  If there isn't an error, then return the connection to the pool
	defer func() {
		if err == nil {
			log.Trace("Releasing connection from encodeResponse", c)
			sd.Release(c)
		} else {
			log.Trace("Closing connection from encodeResponse", c)
			c.Close()
		}
	}()

	n, err := io.Copy((c), bytes.NewReader(reqBuff))
	log.Trace("Copied", n, "bytes to the socket")
	if err != nil {
		err = errors.Wrap(err, "Failed to write encodeResponse to socket")
		return nil, err
	}

	out := new(encodeResponseOutput)
	_, _, err = readResp((c), out)
	if err != nil {
		err = errors.Wrap(err, "Failed to read encodeResponseOutput from socket")
		return nil, err
	}
	h.Add(aaa.AaaAuthHeader, string(out.authHeader))
	log.Tracef("Encoded response headers %s \nand payload %#v", out.authHeader, out.payload)
	return out.payload, nil
}
