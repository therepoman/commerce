package code

// Old msgpack spec codes: https://github.com/msgpack/msgpack/blob/master/spec-old.md

const (
	PostFixNumMax = 0x7f
	NegFixNum     = 0xe0

	Nil = 0xc0

	BoolTrue  = 0xc3
	BoolFalse = 0xc2

	Uint8  = 0xcc
	Uint16 = 0xcd
	Uint32 = 0xce
	Uint64 = 0xcf

	Int8  = 0xd0
	Int16 = 0xd1
	Int32 = 0xd2
	Int64 = 0xd3

	Float32 = 0xca
	Float64 = 0xcb

	FixRawLow  = 0xa0
	FixRawHigh = 0xbf
	FixRawMask = 0x1f
	Raw16      = 0xda
	Raw32      = 0xdb

	FixArrayLow  = 0x90
	FixArrayHigh = 0x9f
	FixArrayMask = 0x0f
	Array16      = 0xdc
	Array32      = 0xdd

	FixMap     = 0x80
	FixMapMask = 0xf0
	Map16      = 0xde
	Map32      = 0xdf
)
