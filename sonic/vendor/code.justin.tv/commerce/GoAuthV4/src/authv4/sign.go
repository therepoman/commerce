/*
   AWS AuthV4 library for Go.

   Includes a Signer object for Go clients to sign http requests ( see: http://docs.aws.amazon.com/general/latest/gr/signature-version-4.html ).
   The Signer passes the Signature Version 4 Test Suite ( see http://docs.aws.amazon.com/general/latest/gr/signature-v4-test-suite.html ).

   Includes an Authorizer object for Go servers to use to authorize client AuthV4 connections via ARPS.

*/
package authv4

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"sort"
	"strings"
	"sync"
	"time"

	"path/filepath"

	"encoding/hex"

	"net/http"
	"net/url"

	"crypto/hmac"
	"crypto/sha256"
)

type SigningDay string

// Return date as formated string and day string
func NewSigningDay(date time.Time) (string, SigningDay) {
	day := date.UTC().Format(iso8601BasicFmt)
	return day, SigningDay(day[:8])
}

type SigningKey []byte

// Utility to calculate a signing key
func GetSigningKey(day SigningDay, secretKey, region, service string) SigningKey {

	awskey := "AWS4" + secretKey
	hashcode := hmac.New(sha256.New, []byte(awskey))
	hashcode.Write([]byte(day))
	kdate := hashcode.Sum(nil)

	hashcode = hmac.New(sha256.New, kdate)
	hashcode.Write([]byte(region))
	kregion := hashcode.Sum(nil)

	hashcode = hmac.New(sha256.New, kregion)
	hashcode.Write([]byte(service))
	kservice := hashcode.Sum(nil)

	hashcode = hmac.New(sha256.New, kservice)
	hashcode.Write([]byte("aws4_request"))
	ksigned := hashcode.Sum(nil)

	return ksigned
}

// map date string to signing key for that day
type signingKeyCache map[SigningDay]SigningKey

// returns dedup'd sorted headers + Host added if needed
func normalizeHeaders(headers []string) (string, []string) {

	// dedup headers
	headermap := map[string]bool{"host": true} // host is always required ... add the rest
	for _, h := range headers {
		headermap[strings.ToLower(h)] = true
	}
	var headers_sorted []string
	for key, _ := range headermap {
		headers_sorted = append(headers_sorted, key)
	}
	sort.Strings(headers_sorted)

	return strings.Join(headers_sorted, ";"), headers_sorted
}

// Object to sign http.Request with AuthV4.
type Signer struct {
	lock sync.RWMutex // to protect internal volatile structures

	region    string // the aws region (e.g., us-east-1) NOTE: must agree with server!
	service   string // the name of the service (e.g., redshift) NOTE: must agree with server!
	accessKey string // account access key
	secretKey string // account secret key

	sortedHeaders    []string // sorted list of headers to sign
	canonicalHeaders string   // sorted and ; delimited

	signingKeys signingKeyCache

	log *Logger
}

// Anything that implements this interface can be signed
type Signable interface {
	ReqHeaders() http.Header
	ReqURL() *url.URL
	ReqBody() io.Reader
	SetReqBody(io.ReadCloser)
	ReqMethod() string
}

type SignableRequest struct{ *http.Request }

func (hr SignableRequest) ReqBody() io.Reader {
	return hr.Body
}

// Todo, make ReqBody abstract this away by returning a copy, and doing so
// efficiently with seekable streams
func (hr SignableRequest) SetReqBody(r io.ReadCloser) {
	hr.Body = r
}

func (hr SignableRequest) ReqHeaders() http.Header {
	return hr.Header
}
func (hr SignableRequest) ReqURL() *url.URL {
	return hr.URL
}

func (hr SignableRequest) ReqMethod() string {
	return hr.Method
}

/*
 Create a signer for the specified aws region and service for a particular accessKey/secretKey.

 The signer always signs the "Host" header. Extra headers to sign should be listed in extraHeaders. Some services require specific headers.

*/
func NewSigner(region, service, accessKey, secretKey string, extraHeaders []string) *Signer {

	headers_listed, headers_sorted := normalizeHeaders(extraHeaders)

	return &Signer{
		sync.RWMutex{},
		strings.ToLower(region),
		strings.ToLower(service),
		accessKey,
		secretKey,
		headers_sorted,
		headers_listed,
		make(signingKeyCache, 1),
		NewLogger()}
}

func (signer *Signer) SetDebugLogger(debugLogger DebugLogger) {
	signer.log.SetDebugLogger(debugLogger)
}

func (signer *Signer) DebugLoggerEnable() {
	signer.log.DebugLoggerEnable()
}

func (signer *Signer) DebugLoggerDisable() {
	signer.log.DebugLoggerDisable()
}

func (signer *Signer) DebugLoggerIsEnabled() bool {
	return signer.log.DebugLoggerIsEnabled()
}

func (signer *Signer) debugLog(canonical_request, authz string) {
	if signer.DebugLoggerIsEnabled() {
		signer.log.Debugf("Canonical Request=%s\n", canonical_request)
		signer.log.Debugf("Authz=%s\n", authz)
	}
}

func (signer *Signer) GetSigningKey(today SigningDay) SigningKey {

	// Lock WHOLE function
	signer.lock.Lock()
	defer signer.lock.Unlock()

	ksigned, present := signer.signingKeys[today]

	if !present {
		ksigned = GetSigningKey(today, signer.secretKey, signer.region, signer.service)
		signer.signingKeys[today] = ksigned
	}

	return ksigned
}

// slightly modified from https://golang.org/src/net/url/url.go
func shouldEscape(c byte) bool {
	// 2.3 Unreserved characters
	if ('A' <= c && c <= 'Z') || ('a' <= c && c <= 'z') || ('0' <= c && c <= '9') || c == '-' || c == '.' || c == '_' || c == '~' {
		return false
	}

	// not path seperator
	if c == '/' {
		return false
	}

	return true
}

// slightly modified from https://golang.org/src/net/url/url.go
func escapePath(s string) string {
	// '/' is left alone in path components

	hexCount := 0
	for i := 0; i < len(s); i++ {
		c := s[i]
		if shouldEscape(c) {
			hexCount++
		}
	}

	if hexCount == 0 {
		return s
	}

	t := make([]byte, len(s)+2*hexCount)
	j := 0
	for i := 0; i < len(s); i++ {
		switch c := s[i]; {
		case shouldEscape(c):
			t[j] = '%'
			t[j+1] = "0123456789ABCDEF"[c>>4]
			t[j+2] = "0123456789ABCDEF"[c&15]
			j += 3
		default:
			t[j] = s[i]
			j++
		}
	}
	return string(t)
}

func normalizePath(p string) string {
	// If the path is empty, then return the URL version of an empty path.
	if p == "" {
		return "/"
	}

	// this was very much tuned to the canonical expectations in the test framework, some of which
	// were odd, for example //foo// -> /foo/ rather than /foo which just seems wrong.

	if strings.Contains(p, "./") || strings.Contains(p, "..") {
		p, _ = filepath.Abs(p)
	}

	if strings.Contains(p, "//") {
		p = strings.Replace(p, "//", "/", -1)
	}

	return escapePath(p)
}

func sha256Digest(b []byte) string {
	hash := sha256.Sum256(b)
	return hex.EncodeToString(hash[:])
}

func canonicalizeQuery(signable Signable) string {

	// arrange query parameters for signing
	canonical_query := ""
	u := signable.ReqURL()
	if u.RawQuery != "" {
		// sort the queries, and for repeated params, sort the values
		values := u.Query()
		for _, value := range values {
			sort.Strings(value) // ensure values are sorted
		}
		canonical_query = values.Encode()
	}

	return canonical_query
}

func canonicalizeHeaders(sortedHeaders []string, signable Signable) []string {

	// arrange headers for signing
	// NOTE: host (the minimum required header) may not be set, so set it
	if signable.ReqHeaders().Get("host") == "" {
		signable.ReqHeaders().Set("host", signable.ReqURL().Host)
	}
	var canonical_headers []string
	for _, h := range sortedHeaders {
		values, present := signable.ReqHeaders()[http.CanonicalHeaderKey(h)]
		if present {
			// multiple values must be sorted
			sort.Strings(values)
			canonical_headers = append(canonical_headers, h+":"+strings.Join(values, ","))
		}
	}
	return canonical_headers
}

// return canonical_request, string to sign, and payload_signature
func getStringToSign(nowDate string, nowDay SigningDay,
	region, service, accessKey string,
	canonicalHeaders string, sortedHeaders []string,
	signable Signable) (string, string, string, error) {

	// hash payload (if we have a ReadSeeker, use it)
	var payload_signature string

	reqBody := signable.ReqBody()

	if reqBody == nil {

		payload_signature = sha256Digest(nil)

	} else {

		// see if the reader can Seek() (e.g., a file), else we need to copy into memory

		// avoids reading request data ALL into ram at once (we hope)
		if seeker, ok := reqBody.(io.ReadSeeker); ok {

			hasher := sha256.New()
			_, err := io.Copy(hasher, seeker)
			if err != nil {
				return "", "", "", err
			}
			_, err = seeker.Seek(0, 0)

			if err != nil {
				return "", "", "", err
			}

			hash := hasher.Sum(nil)
			payload_signature = hex.EncodeToString(hash[:])

		} else {
			payload, _ := ioutil.ReadAll(reqBody)

			// Reset the request body back to the beginning by recreating it
			body := ioutil.NopCloser(bytes.NewReader(payload))
			signable.SetReqBody(body)
			payload_signature = sha256Digest(payload)
		}

	}

	canonical_query := canonicalizeQuery(signable)

	canonical_headers := canonicalizeHeaders(sortedHeaders, signable)

	canonical_request := (signable.ReqMethod() + "\n" +
		normalizePath(signable.ReqURL().Path) + "\n" +
		canonical_query + "\n" +
		strings.Join(canonical_headers, "\n") + "\n\n" +
		canonicalHeaders + "\n" +
		payload_signature)

	canonical_request_signature := sha256Digest([]byte(canonical_request))

	str2sign := fmt.Sprintf("AWS4-HMAC-SHA256\n%s\n%s/%s/%s/aws4_request\n%s",
		nowDate, nowDay, region, service, canonical_request_signature)

	return canonical_request, str2sign, payload_signature, nil
}

// core signing routine: return canonical_request, final "Authorization" string and payload signature
func doSign(nowDate string, nowDay SigningDay,
	region, service, accessKey string,
	canonicalHeaders string, sortedHeaders []string,
	signingKey SigningKey,
	signable Signable) (string, string, string, error) {

	canonical_request, str2sign, payload_signature, err := getStringToSign(nowDate, nowDay, region, service, accessKey, canonicalHeaders, sortedHeaders, signable)
	if err != nil {
		return "", "", "", err
	}

	hashcode := hmac.New(sha256.New, signingKey)
	hashcode.Write([]byte(str2sign))
	signature := hex.EncodeToString(hashcode.Sum(nil))

	credential := fmt.Sprintf("%s/%s/%s/%s/aws4_request", accessKey, nowDay, region, service)

	authz := ("AWS4-HMAC-SHA256 Credential=" + credential +
		", SignedHeaders=" + canonicalHeaders +
		", Signature=" + signature)

	return canonical_request, authz, payload_signature, nil
}

// used as entry point for test harness because it returns the key and authz which we use with Validate().
func (signer *Signer) sign(now time.Time, signable Signable) (SigningKey, string, error) {

	// format dates
	nowDate, nowDay := NewSigningDay(now)

	signingkey := signer.GetSigningKey(nowDay)

	// set date so we can sign it if directed to in extraHeaders
	signable.ReqHeaders().Set("X-Amz-Date", nowDate)

	canonical_request, authz, payload_signature, err := doSign(nowDate, nowDay,
		signer.region, signer.service, signer.accessKey,
		signer.canonicalHeaders, signer.sortedHeaders,
		signingkey, signable)

	if err != nil {
		return signingkey, authz, err
	}

	// set headers with signing info
	signable.ReqHeaders().Set("Authorization", authz)
	signable.ReqHeaders().Set("X-Amz-Content-SHA256", payload_signature)

	signer.debugLog(canonical_request, authz)

	return signingkey, authz, err
}

// Sign request with AWS AuthV4 headers.
func (signer *Signer) SignReq(signable Signable) error {
	_, _, err := signer.sign(time.Now(), signable)
	return err
}

// Sign an http.Request with AWS AuthV4 headers
func (signer *Signer) Sign(req *http.Request) error {
	signable := SignableRequest{req}
	_, _, err := signer.sign(time.Now(), signable)
	return err
}
