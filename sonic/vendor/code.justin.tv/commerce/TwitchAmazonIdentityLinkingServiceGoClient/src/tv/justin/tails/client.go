package tv_justin_tails

import (
	__client__ "code.justin.tv/commerce/CoralGoClient/src/coral/client"
	__codec__ "code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	__dialer__ "code.justin.tv/commerce/CoralGoClient/src/coral/dialer"
)

//A service that manages links between Amazon and Twitch Accounts.
type TwitchAmazonIdentityLinkingServiceClient struct {
	C __client__.Client
}

//Creates a new TwitchAmazonIdentityLinkingServiceClient
func NewTwitchAmazonIdentityLinkingServiceClient(dialer __dialer__.Dialer, codec __codec__.RoundTripper) (service *TwitchAmazonIdentityLinkingServiceClient) {
	return &TwitchAmazonIdentityLinkingServiceClient{__client__.NewClient("tv.justin.tails", "TwitchAmazonIdentityLinkingService", dialer, codec)}
}
func (this *TwitchAmazonIdentityLinkingServiceClient) CheckLinkIntegrity(input CheckLinkIntegrityRequest) (CheckLinkIntegrityResponse, error) {
	var output CheckLinkIntegrityResponse
	err := this.C.Call("tv.justin.tails", "CheckLinkIntegrity", input, &output)
	return output, err
}

//Gets the Tax and Address information for a payout entity id from TIMS and Address Service.
func (this *TwitchAmazonIdentityLinkingServiceClient) GetTaxInfoForPayoutEntity(input GetTaxInfoForPayoutEntityRequest) (GetTaxInfoForPayoutEntityResponse, error) {
	var output GetTaxInfoForPayoutEntityResponse
	err := this.C.Call("tv.justin.tails", "GetTaxInfoForPayoutEntity", input, &output)
	return output, err
}

//Link a twitch account to an amazon account.
func (this *TwitchAmazonIdentityLinkingServiceClient) LinkAccounts(input LinkAccountsRequest) (LinkAccountsResponse, error) {
	var output LinkAccountsResponse
	err := this.C.Call("tv.justin.tails", "LinkAccounts", input, &output)
	return output, err
}

//Get linked Amazon Account from TwitchUserID.
func (this *TwitchAmazonIdentityLinkingServiceClient) GetLinkedAmazonAccount(input GetLinkedAmazonAccountRequest) (GetLinkedAmazonAccountResponse, error) {
	var output GetLinkedAmazonAccountResponse
	err := this.C.Call("tv.justin.tails", "GetLinkedAmazonAccount", input, &output)
	return output, err
}

//Retrieves the Twitch login name and user ID for a user, given the OAuth code and state
func (this *TwitchAmazonIdentityLinkingServiceClient) GetTwitchUserInfo(input GetTwitchUserInfoRequest) (GetTwitchUserInfoResponse, error) {
	var output GetTwitchUserInfoResponse
	err := this.C.Call("tv.justin.tails", "GetTwitchUserInfo", input, &output)
	return output, err
}

//Get twitch linked accounts from amazon account.
//Optionally includes Twitch username and avatar (default is to include).
func (this *TwitchAmazonIdentityLinkingServiceClient) GetLinkedTwitchAccounts(input GetLinkedTwitchAccountsRequest) (GetLinkedTwitchAccountsResponse, error) {
	var output GetLinkedTwitchAccountsResponse
	err := this.C.Call("tv.justin.tails", "GetLinkedTwitchAccounts", input, &output)
	return output, err
}

//Used to lookup select TIMS information for a partners account
func (this *TwitchAmazonIdentityLinkingServiceClient) GetPayoutIdentity(input GetPayoutIdentityRequest) (GetPayoutIdentityResponse, error) {
	var output GetPayoutIdentityResponse
	err := this.C.Call("tv.justin.tails", "GetPayoutIdentity", input, &output)
	return output, err
}
func (this *TwitchAmazonIdentityLinkingServiceClient) SubmitDeskTicket(input SubmitDeskTicketRequest) (SubmitDeskTicketResponse, error) {
	var output SubmitDeskTicketResponse
	err := this.C.Call("tv.justin.tails", "SubmitDeskTicket", input, &output)
	return output, err
}

//Gets the tax withholding rate for a payout entity id from TIMS and Address Service.
func (this *TwitchAmazonIdentityLinkingServiceClient) GetTaxWithholdingRateForPayoutEntity(input GetTaxWithholdingRateForPayoutEntityRequest) (GetTaxWithholdingRateForPayoutEntityResponse, error) {
	var output GetTaxWithholdingRateForPayoutEntityResponse
	err := this.C.Call("tv.justin.tails", "GetTaxWithholdingRateForPayoutEntity", input, &output)
	return output, err
}

//Unlink a twitch account from its linked amazon account.
func (this *TwitchAmazonIdentityLinkingServiceClient) UnlinkTwitchAccount(input UnlinkTwitchAccountRequest) (UnlinkTwitchAccountResponse, error) {
	var output UnlinkTwitchAccountResponse
	err := this.C.Call("tv.justin.tails", "UnlinkTwitchAccount", input, &output)
	return output, err
}

//Unlink a twitch account from its linked amazon account.
func (this *TwitchAmazonIdentityLinkingServiceClient) UnlinkAmazonAccount(input UnlinkAmazonAccountRequest) (UnlinkAmazonAccountResponse, error) {
	var output UnlinkAmazonAccountResponse
	err := this.C.Call("tv.justin.tails", "UnlinkAmazonAccount", input, &output)
	return output, err
}

//Get linked Amazon DirectedId from TwitchUserID.
//Optionally includes Amazon accounts COR (default is to not include).
func (this *TwitchAmazonIdentityLinkingServiceClient) GetLinkedAmazonDirectedId(input GetLinkedAmazonDirectedIdRequest) (GetLinkedAmazonDirectedIdResponse, error) {
	var output GetLinkedAmazonDirectedIdResponse
	err := this.C.Call("tv.justin.tails", "GetLinkedAmazonDirectedId", input, &output)
	return output, err
}

//Retrieves the Twitch user name given the Twitch user ID
func (this *TwitchAmazonIdentityLinkingServiceClient) GetTwitchUserName(input GetTwitchUserNameRequest) (GetTwitchUserNameResponse, error) {
	var output GetTwitchUserNameResponse
	err := this.C.Call("tv.justin.tails", "GetTwitchUserName", input, &output)
	return output, err
}
