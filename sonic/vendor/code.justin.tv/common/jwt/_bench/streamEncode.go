package jwt

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"

	"code.justin.tv/common/jwt"
)

//Err represents an annotated error from another package.
type Err struct {
	Err  error
	Desc string
}

func (j Err) Error() string { return fmt.Sprintf("jwt: %s %s", j.Err, j.Desc) }

type trimSpaceWriter struct {
	w io.Writer
}

func (t *trimSpaceWriter) Write(b []byte) (n int, err error) {
	return t.w.Write(bytes.TrimSpace(b))
}

func newTrimSpaceWriter(w io.Writer) io.Writer {
	return &trimSpaceWriter{
		w: w,
	}
}

func streamEncode(header, claims interface{}, a jwt.Algorithm, w io.Writer) (err error) {
	var prefix bytes.Buffer
	dup := io.MultiWriter(&prefix, w)

	//Write the encoded base64 into both signer and output
	b64 := base64.NewEncoder(base64.URLEncoding, dup)

	//write json into the base64 encoder
	j := json.NewEncoder(newTrimSpaceWriter(b64))

	//Write header in base64 to both
	if err = j.Encode(header); err != nil {
		err = Err{err, "in writing base64 header"}
		return
	}

	// flush the blocks for the first base64 segment (header)
	if err = b64.Close(); err != nil {
		err = Err{err, "in closing base64 writer"}
		return
	}

	//Write the dot to both
	if _, err = io.WriteString(dup, "."); err != nil {
		err = Err{err, "in writing first dot separator"}
		return
	}

	//Write the claims in base64 to both
	if err = j.Encode(claims); err != nil {
		err = Err{err, "in encoding claims"}
		return
	}

	//flush claims base64
	if err = b64.Close(); err != nil {
		err = Err{err, "in flushing claims base64"}
		return
	}

	//Write the second dot separating the signature
	//to (just) the output
	if _, err = io.WriteString(w, "."); err != nil {
		err = Err{err, "in writing second dot separator"}
		return
	}

	//Write final signature to just output
	e2 := base64.NewEncoder(base64.URLEncoding, w)

	var sigBytes []byte
	if sigBytes, err = a.Sign(prefix.Bytes()); err != nil {
		err = Err{err, "in computing jwt signature"}
		return
	}

	if _, err = e2.Write(sigBytes); err != nil {
		err = Err{err, "in writing signature b64"}
		return
	}

	//flush the final signature
	if err = e2.Close(); err != nil {
		err = Err{err, "in flushing signature to b64"}
		return
	}

	return

}
