package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sync"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/iam"
	"github.com/aws/aws-sdk-go/service/sts"
	"github.com/hashicorp/hcl"
)

var iamInstance *iam.IAM
var once sync.Once

const (
	scopeLocal       = "Local"
	confFileLocation = "./removeUnattachedPolicyConf.hcl"
)

type config struct {
	Region     string `hcl:"region"`
	RoleARN    string `hcl:"roleArn"`
	PathPrefix string `hcl:"policyPathPrefix"`
}

type policyInfo struct {
	policyArn        *string
	policyName       *string
	attachedEntities *int64
}

type unattachedTemplatedPoliciesArn []string

var forceRun *bool
var deleteLimit *int

func main() {

	initCommandLineFlags()

	unattachedPolicies, err := getAllUnattachedPolicies()
	if err != nil {
		log.Fatal(err)
	}

	policyProcessed := 0
	fmt.Printf("This run would delete first %d policies.\n", *deleteLimit)
	for _, policy := range unattachedPolicies {
		if deleteLimit != nil && *deleteLimit > 0 {
			fmt.Printf("Attempting to delete %s\n", *policy.policyName)

			if *forceRun {
				err := deletePolicy(*policy.policyArn)
				if err != nil {
					log.Printf("failed to delete policy %s, err: %s", *policy.policyArn, err.Error())
					continue
				}
				fmt.Printf("deleted %s\n", *policy.policyArn)
			}

			policyProcessed++
			*deleteLimit--
		}

	}

	fmt.Printf("Total policy processed: %d.\n", policyProcessed)

}

func initCommandLineFlags() {
	forceRun = flag.Bool("forceRun", false, "Set this flag to delete policies.")
	deleteLimit = flag.Int("limit", 1, "Number of policies to be deleted.")
	flag.Parse()
}

func deletePolicy(policyArn string) error {
	iamService := getIAMInstance()
	fmt.Printf("Deleting %s\n", policyArn)
	params := &iam.DeletePolicyInput{
		PolicyArn: aws.String(policyArn),
	}

	_, err := iamService.DeletePolicy(params)
	if err != nil {
		return err
	}

	return nil
}

func getAllUnattachedPolicies() ([]*policyInfo, error) {

	inputParams := listPoliciesInput()
	allPolicyArns, err := getPolicyInfo(inputParams)
	if err != nil {
		return nil, err
	}

	var unattachedPolicies []*policyInfo
	for _, policy := range allPolicyArns {
		if *policy.attachedEntities == 0 {
			unattachedPolicies = append(unattachedPolicies, policy)
		}
	}

	return unattachedPolicies, nil
}

func getPolicyInfo(inputParams *iam.ListPoliciesInput) ([]*policyInfo, error) {

	iamService := getIAMInstance()

	var allPolicies []*policyInfo
	for {
		listPoliciesOutput, err := iamService.ListPolicies(inputParams)
		if err != nil {
			return allPolicies, err
		}
		policies := listPoliciesOutput.Policies
		for _, policy := range policies {
			allPolicies = append(allPolicies, &policyInfo{
				policyArn:        policy.Arn,
				policyName:       policy.PolicyName,
				attachedEntities: policy.AttachmentCount,
			})
		}

		if listPoliciesOutput.IsTruncated != nil && *listPoliciesOutput.IsTruncated {
			inputParams.Marker = listPoliciesOutput.Marker
		} else {
			break
		}
	}

	return allPolicies, nil
}

func listPoliciesInput() *iam.ListPoliciesInput {

	conf := getConfig()

	return &iam.ListPoliciesInput{
		PathPrefix:   aws.String(conf.PathPrefix),
		OnlyAttached: aws.Bool(false),
		Scope:        aws.String(scopeLocal),
	}
}

func getIAMInstance() *iam.IAM {

	once.Do(func() {
		sess := session.New(getAWSCredentialConfig())
		iamInstance = iam.New(sess)
	})
	return iamInstance
}

func getAWSCredentialConfig() *aws.Config {

	config := getConfig()

	awsConfig := &aws.Config{Region: aws.String(config.Region)}
	sess := session.New(awsConfig)
	stsclient := sts.New(sess)

	arp := &stscreds.AssumeRoleProvider{
		Duration:     900 * time.Second,
		ExpiryWindow: 10 * time.Second,
		RoleARN:      config.RoleARN,
		Client:       stsclient,
	}
	credentials := credentials.NewCredentials(arp)
	awsConfig.WithCredentials(credentials)
	return awsConfig

}

func getConfig() *config {

	confFile := configFilePath()

	confText, err := ioutil.ReadFile(confFile)
	if err != nil {
		return nil
	}

	conf := new(config)
	err = hcl.Decode(conf, string(confText))
	if err != nil {
		return nil
	}

	return conf
}

func configFilePath() string {
	confFile := os.Getenv("PurgePolicyConfig")
	if len(confFile) == 0 {
		confFile = confFileLocation
	}
	return confFile
}
