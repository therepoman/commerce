package policy

// RoleOwnerTable takes a tableName suffix and returns name of the
// DynamoDB role->slice of owners table
func RoleOwnerTable(tableName string) string {
	return tableName + "_role_owners"
}
