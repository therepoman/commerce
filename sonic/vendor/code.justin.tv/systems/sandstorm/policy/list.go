package policy

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/iam"
)

// List lists ALL roles (names only), even roles a user doesn't have access to modify
func (pg *IAMPolicyGenerator) List() (roles []string, err error) {
	roles = []string{}

	getMoreRoles := true
	var marker *string
	for getMoreRoles {
		input := &iam.ListRolesInput{
			PathPrefix: aws.String(pg.PathPrefix(IAMRolePathSuffix)),
			Marker:     marker,
			MaxItems:   aws.Int64(1000),
		}
		var listRolesOutput *iam.ListRolesOutput
		listRolesOutput, err = pg.IAM.ListRoles(input)
		if err != nil {
			return
		}
		getMoreRoles = aws.BoolValue(listRolesOutput.IsTruncated)
		marker = listRolesOutput.Marker

		for _, v := range listRolesOutput.Roles {
			roles = append(roles, aws.StringValue(v.RoleName))
		}
	}
	return
}
