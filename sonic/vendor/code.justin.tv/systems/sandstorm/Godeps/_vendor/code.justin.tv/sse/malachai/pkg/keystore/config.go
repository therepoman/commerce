package keystore

import (
	"code.justin.tv/sse/malachai/pkg/config"
)

// Config is passed to New on Storer creation
type Config struct {
	Environment string
	RoleArn     string
	Region      string
	TableName   string
}

// FillDefaults fills the config with default values if they don't exist
func (c *Config) FillDefaults() (err error) {
	resources, err := config.GetResources(c.Environment)
	if err != nil {
		return
	}

	if c.Region == "" {
		c.Region = resources.Region
	}

	if c.TableName == "" {
		c.TableName = resources.DiscoveryTable
	}
	return
}
