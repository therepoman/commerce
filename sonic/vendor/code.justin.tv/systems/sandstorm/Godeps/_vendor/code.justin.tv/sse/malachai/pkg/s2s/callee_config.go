package s2s

import (
	"errors"
	"fmt"
	"time"

	"github.com/cactus/go-statsd-client/statsd"

	"code.justin.tv/sse/malachai/pkg/capabilities"
	"code.justin.tv/sse/malachai/pkg/config"
	"code.justin.tv/sse/malachai/pkg/keystore"
	"code.justin.tv/sse/malachai/pkg/loggers"
	"code.justin.tv/sse/malachai/pkg/logging"
	"code.justin.tv/sse/malachai/pkg/registration"
	"code.justin.tv/sse/malachai/pkg/signature"
)

// Options for QueueUnavailablePolicy
const (
	// Passthrough requests if we can't access updates
	// Reject requests if we can't access updates
	QueueUnavailablePolicyPassthrough = "passthrough"
	QueueUnavailablePolicyReject      = "reject"
)

const statsdHost = "statsd.internal.justin.tv:8125"

// CalleeConfig holds configuration options for the callee
type CalleeConfig struct {
	// Required
	//
	// CalleeName is the human-readable name of the service. This is used to
	// discover internal configuration options.
	CalleeName string

	// Required
	//
	// Absolute path to where the boltdb file should be created. BoltDB is used
	// to persist state across process restarts and to cache caller pubkeys.
	BoltDBFileName string

	// Optional
	//
	// QueueUnavailablePolicy determines how the sdk handles problems with
	// the sqs queue for notifying on capability changes / pubkey revocations.
	// Defaults to `passthrough`
	//
	// QueueUnavailableRejectThreshold is the amount of time the sdk waits
	// until requests start getting rejected. Only applies when the sqs queue
	// is unreachable and QueueUnavailablePolicy is set to `reject`.
	QueueUnavailablePolicy          string
	QueueUnavailableRejectThreshold time.Duration

	// optional path to s2s logging directory. if empty, defaults to
	// /var/log/malachai.
	LogDir string

	// for internal use
	//
	// Environment should always be set to production or left blank
	Environment            string
	RequestValidatorConfig *signature.RequestValidatorConfig
	KeystoreConfig         *keystore.Config
	CapabilitiesConfig     *capabilities.Config
	RegistrationConfig     *registration.Config
	FirehoseConfig         *loggers.FirehoseLoggerConfig

	calleeCapConfig *calleeCapabilitiesConfig
	eventsConfig    *eventsConfig
	inventoryConfig *inventoryConfig
	roleArn         string
	calleeID        string
}

func (cfg *CalleeConfig) statsd() (statter statsd.Statter, err error) {
	return newStatter(cfg.Environment, cfg.CalleeName)
}

// FillDefaults fills in default configuration options. This is called in
// the middleware constructors, so the client does not need to call this
// manually.
func (cfg *CalleeConfig) FillDefaults() (err error) {
	if cfg.Environment == "" {
		cfg.Environment = "production"
	}
	resources, err := config.GetResources(cfg.Environment)
	if err != nil {
		return
	}

	if cfg.FirehoseConfig == nil {
		cfg.FirehoseConfig = new(loggers.FirehoseLoggerConfig)
	}
	if cfg.LogDir != "" {
		cfg.FirehoseConfig.LogDir = cfg.LogDir
	}
	cfg.FirehoseConfig.Environment = cfg.Environment

	if cfg.RegistrationConfig == nil {
		cfg.RegistrationConfig = new(registration.Config)
	}
	cfg.RegistrationConfig.Environment = cfg.Environment
	cfg.RegistrationConfig.RoleArn = resources.AuxiliaryRoleArn

	// callee only needs Get() which doesn't actually log
	reg, err := registration.New(cfg.RegistrationConfig, logging.NewNoOpLogger())
	if err != nil {
		return
	}

	callee, err := reg.Get(cfg.CalleeName)
	if err != nil {
		return
	}
	cfg.calleeID = callee.ID

	if cfg.eventsConfig == nil {
		cfg.eventsConfig = new(eventsConfig)
	}
	cfg.eventsConfig.Environment = cfg.Environment
	cfg.eventsConfig.ServiceID = callee.ID
	cfg.eventsConfig.RoleArn = callee.RoleArn
	cfg.eventsConfig.CalleeCapabilitiesTopicARN = callee.CapabilitiesTopicArn

	if cfg.KeystoreConfig == nil {
		cfg.KeystoreConfig = new(keystore.Config)
	}
	cfg.KeystoreConfig.Environment = cfg.Environment
	cfg.KeystoreConfig.RoleArn = callee.RoleArn
	err = cfg.KeystoreConfig.FillDefaults()
	if err != nil {
		return
	}

	if cfg.CapabilitiesConfig == nil {
		cfg.CapabilitiesConfig = new(capabilities.Config)
	}
	cfg.CapabilitiesConfig.Environment = cfg.Environment
	cfg.CapabilitiesConfig.RoleArn = callee.RoleArn
	err = cfg.CapabilitiesConfig.FillDefaults()
	if err != nil {
		return
	}

	if cfg.calleeCapConfig == nil {
		cfg.calleeCapConfig = new(calleeCapabilitiesConfig)
	}
	cfg.calleeCapConfig.roleArn = callee.RoleArn
	cfg.calleeCapConfig.callee = callee.ID
	cfg.calleeCapConfig.FillDefaults()

	cfg.eventsConfig.RoleArn = callee.RoleArn
	err = cfg.eventsConfig.FillDefaults()
	if err != nil {
		return
	}

	if cfg.inventoryConfig == nil {
		cfg.inventoryConfig = new(inventoryConfig)
	}
	cfg.inventoryConfig.Environment = cfg.Environment
	cfg.inventoryConfig.RoleArn = callee.RoleArn
	err = cfg.inventoryConfig.fillDefaults()
	if err != nil {
		return
	}

	if cfg.QueueUnavailablePolicy == "" {
		cfg.QueueUnavailablePolicy = QueueUnavailablePolicyPassthrough
	}

	err = cfg.validate()
	return
}

func (cfg *CalleeConfig) validate() (err error) {
	switch cfg.QueueUnavailablePolicy {
	case QueueUnavailablePolicyPassthrough:
	case QueueUnavailablePolicyReject:
		if cfg.QueueUnavailableRejectThreshold <= 0 {
			err = errors.New("reject requires a QueueUnavailableRejectThreshold greater than 0")
			return
		}
	default:
		err = fmt.Errorf("invalid QueueUnavailablePolicy: '%s' must be one of 'passthrough' or 'reject'", cfg.QueueUnavailablePolicy)
		return
	}
	return
}
