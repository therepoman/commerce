package s2s

import (
	"context"
	"encoding/json"
	"errors"
	"time"

	"github.com/boltdb/bolt"
	"github.com/sirupsen/logrus"

	"code.justin.tv/sse/malachai/pkg/capabilities"
)

// ErrCapabilitiesCacheExpired is returned when the local capabilities cache
// is expired and the events client hasn't retrieved an update to refresh the
// cache.
var ErrCapabilitiesCacheExpired = errors.New("local capabilities cache has expired")

const (
	boltCapabilitiesCacheKey   = "cache"
	boltCapabilitiesBucketName = "capabilities"
	defaultCapabilitiesTTL     = 24 * time.Hour
)

type calleeCapabilitiesCache struct {
	Cache         map[string]*capabilities.Capabilities
	LastRetrieved time.Time
}

type calleeCapabilitiesClient struct {
	cache         *calleeCapabilitiesCache
	capabilities  capabilities.Manager
	invalidations chan struct{}
	log           logrus.FieldLogger

	cfg *calleeCapabilitiesConfig
	db  *bolt.DB
}

func newCalleeCapabilitiesClient(cfg *CalleeConfig, db *bolt.DB, logger logrus.FieldLogger, invalidations chan struct{}) (capClient *calleeCapabilitiesClient, err error) {
	capabilities, err := capabilities.New(cfg.CapabilitiesConfig)
	if err != nil {
		return
	}

	capClient = &calleeCapabilitiesClient{
		capabilities:  capabilities,
		invalidations: invalidations,
		log:           logger,
		cfg:           cfg.calleeCapConfig,
		db:            db,
	}
	err = capClient.loadFromBolt()
	if err != nil {
		return
	}
	return
}

type calleeCapabilitiesConfig struct {
	callee          string
	ttl             time.Duration
	capabilitiesTTL time.Duration
	roleArn         string
}

// FillDefaults fills default values
func (cfg *calleeCapabilitiesConfig) FillDefaults() {
	if cfg.ttl == 0 {
		cfg.ttl = 1 * time.Hour
	}

	if cfg.capabilitiesTTL == 0 {
		cfg.capabilitiesTTL = defaultCapabilitiesTTL
	}
}

// loadFromBolt should be called after struct is created
func (ccc *calleeCapabilitiesClient) loadFromBolt() (err error) {
	err = ccc.db.Update(func(tx *bolt.Tx) (err error) {
		_, err = tx.CreateBucketIfNotExists([]byte(boltCapabilitiesBucketName))
		return
	})
	if err != nil {
		return
	}

	err = ccc.db.View(func(tx *bolt.Tx) (err error) {
		b := tx.Bucket([]byte(boltCapabilitiesBucketName))
		value := b.Get([]byte(boltCapabilitiesCacheKey))
		ccc.cache = new(calleeCapabilitiesCache)
		ccc.cache.Cache = make(map[string]*capabilities.Capabilities)

		if value == nil {
			return
		}

		err = json.Unmarshal(value, ccc.cache)
		return
	})
	return
}

func (ccc *calleeCapabilitiesClient) get(caller string) (c *capabilities.Capabilities, err error) {
	if time.Now().UTC().Sub(ccc.cache.LastRetrieved) > ccc.cfg.capabilitiesTTL {
		err = ErrCapabilitiesCacheExpired
		return
	}

	c, ok := ccc.cache.Cache[caller]
	if !ok {
		err = capabilities.ErrCapabilitiesNotFound
	}
	return
}

func (ccc *calleeCapabilitiesClient) updateCache(ctx context.Context) (err error) {

	caps, err := ccc.capabilities.GetForCallee(ctx, ccc.cfg.callee)
	if err != nil {
		return
	}

	ccc.cache = &calleeCapabilitiesCache{
		Cache:         make(map[string]*capabilities.Capabilities),
		LastRetrieved: time.Now().UTC(),
	}
	for _, c := range caps {
		ccc.cache.Cache[c.Caller] = c
	}

	err = ccc.flushCache()
	return
}

func (ccc *calleeCapabilitiesClient) flushCache() (err error) {
	value, err := json.Marshal(ccc.cache)
	if err != nil {
		return
	}

	err = ccc.db.Update(func(tx *bolt.Tx) (err error) {
		b := tx.Bucket([]byte(boltCapabilitiesBucketName))
		err = b.Put([]byte(boltCapabilitiesCacheKey), value)
		return
	})

	return
}

func (ccc *calleeCapabilitiesClient) start(ctx context.Context) {
	timer := time.NewTimer(ccc.cfg.ttl)
	for {
		err := ccc.updateCache(ctx)
		if err != nil {
			ccc.log.Errorf("error while updating capability cache: %s", err.Error())
		}

		select {
		case <-ctx.Done():
			ccc.log.Debug("exiting because context was cancelled")
			return
		case <-ccc.invalidations:
			ccc.log.Debug("updating cache due to invalidation")
		case <-timer.C:
			timer.Reset(ccc.cfg.ttl)
			ccc.log.Debug("updating cache due to invalidation timeout")
		}
	}
}
