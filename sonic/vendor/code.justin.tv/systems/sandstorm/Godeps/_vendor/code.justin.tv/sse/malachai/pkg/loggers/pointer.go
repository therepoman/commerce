package loggers

import (
	"encoding/binary"
	"fmt"
	"path/filepath"
	"strings"

	"github.com/boltdb/bolt"
)

const (
	boltDBBucketName     = "firehoseLogCursor"
	lastProcessedKeyName = "lastProcessedFilePath"

	//offset info about the current file
	fileOffsetKeyName = "fileOffset"
)

func getFromBoltDB(db *bolt.DB, key []byte) (value []byte, err error) {
	err = db.View(func(tx *bolt.Tx) (err error) {
		b := tx.Bucket([]byte(boltDBBucketName))
		if b == nil {
			return fmt.Errorf("boltdb bucket not initialized, name: %s", boltDBBucketName)
		}
		value = b.Get(key)
		return
	})
	return
}

func recordInBoltDB(db *bolt.DB, key, value []byte) (err error) {
	if len(value) == 0 {
		err = fmt.Errorf("cannot store empty value for key: %s", key)
		return
	}
	err = db.Update(func(tx *bolt.Tx) (err error) {
		b := tx.Bucket([]byte(boltDBBucketName))
		if b == nil {
			return fmt.Errorf("boltdb bucket not initialized, name: %s", boltDBBucketName)
		}
		err = b.Put(key, value)
		return
	})
	return
}

func recordLastProcessedFile(db *bolt.DB, absPath string) (err error) {
	return recordInBoltDB(db, []byte(lastProcessedKeyName), []byte(absPath))
}

func lastProcessedFilePath(db *bolt.DB) (lastProcessedFilePath string, err error) {
	value, err := getFromBoltDB(db, []byte(lastProcessedKeyName))
	if err != nil || value == nil {
		return
	}
	lastProcessedFilePath = string(value)
	return
}

func recordFileOffset(db *bolt.DB, offset int64) (err error) {
	return recordInBoltDB(db, []byte(fileOffsetKeyName), int64toBytes(offset))
}

func fileOffset(db *bolt.DB) (fileOffset int64, err error) {
	value, err := getFromBoltDB(db, []byte(fileOffsetKeyName))
	if err != nil || value == nil {
		return
	}
	fileOffset = bytesToInt64(value)
	return
}

func initBoltDBPointerBucket(db *bolt.DB) (err error) {
	err = db.Update(func(tx *bolt.Tx) (err error) {
		_, err = tx.CreateBucketIfNotExists([]byte(boltDBBucketName))
		if err != nil {
			return
		}
		return
	})

	if err != nil {
		return
	}
	err = recordFileOffset(db, int64(0))
	return
}

// compares log file names lexicographically and returns if the current file is
// newer than the last processed file (lumberjack puts the timestamp in the file
// name suffix)
func compareLogFiles(current, lastProcessed string) (newer bool) {
	return strings.Compare(filepath.Base(current), filepath.Base(lastProcessed)) == 1
}

func bytesToInt64(b []byte) (r int64) {
	r = int64(binary.LittleEndian.Uint64(b))
	return
}

func int64toBytes(i int64) (b []byte) {
	b = make([]byte, 8)
	binary.LittleEndian.PutUint64(b, uint64(i))
	return
}
