package s2s

import (
	"context"
	"io"
	"net/http"

	"github.com/sirupsen/logrus"

	"code.justin.tv/sse/malachai/pkg/loggers"
	"code.justin.tv/sse/malachai/pkg/signature"
)

// ContextKey key type used in the request context.
type ContextKey string

// CallerIDContextKey is used to retrieve the caller's service id from the request context.
var CallerIDContextKey = ContextKey("s2s-caller-id")

func newRequestVerifier(validator signature.RequestValidator, logger logrus.FieldLogger) (fn func(http.Handler) http.Handler) {
	fn = func(inner http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			seeker, err := WrapRequestBodyWithSeeker(r)
			if err != nil {
				logger.Error("error wrapping body with seeker: " + err.Error())
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			caller, err := validator.ValidateRequest(r)
			if err != nil {
				logger.Errorf("failed to validate request: " + err.Error())
				http.Error(w, err.Error(), http.StatusUnauthorized)
				return
			}

			logger = logger.WithFields(logrus.Fields{
				loggers.LogFieldCallerName: caller,
			})
			logger.Debug("successfully validated request")
			_, err = seeker.Seek(0, io.SeekStart)
			if err != nil {
				logger.Error("couldn't seek to beginning of body: " + err.Error())

				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			ctx := context.WithValue(r.Context(), CallerIDContextKey, caller)
			inner.ServeHTTP(w, r.WithContext(ctx))
		})
	}
	return
}
