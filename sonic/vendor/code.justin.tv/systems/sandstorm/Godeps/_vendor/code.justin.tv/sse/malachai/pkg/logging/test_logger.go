package logging

import (
	"testing"

	"github.com/sirupsen/logrus"
)

type testLogWriter struct {
	t *testing.T
}

// Write implements io.Writer
func (tlw *testLogWriter) Write(p []byte) (n int, err error) {
	tlw.t.Log(string(p))
	n = len(p)
	return
}

// NewTestLogger creates a logrus.Logger with the test log set as the output
func NewTestLogger(t *testing.T) (l logrus.FieldLogger) {
	logger := logrus.New()
	logger.Level = logrus.DebugLevel
	logger.Out = &testLogWriter{t}
	l = logger
	return
}

type benchmarkLogWriter struct {
	b *testing.B
}

// Write implements io.Writer
func (tlw *benchmarkLogWriter) Write(p []byte) (n int, err error) {
	tlw.b.Log(string(p))
	n = len(p)
	return
}

// NewBenchmarkLogger creates a logrus.FieldLogger with the benchmark log set
// as the output
func NewBenchmarkLogger(b *testing.B) (l logrus.FieldLogger) {
	logger := logrus.New()
	logger.Level = logrus.DebugLevel
	logger.Out = &benchmarkLogWriter{b}
	l = logger
	return
}
