package s2s

import (
	"bytes"
	"context"
	"encoding/json"
	"time"

	"code.justin.tv/sse/malachai/pkg/jwtvalidation"
)

type snsMessage struct {
	Type              string                         `json:"Type"`
	MessageID         string                         `json:"MessageId"`
	TopicArn          string                         `json:"TopicArn"`
	Message           string                         `json:"Message"`
	Timestamp         time.Time                      `json:"Timestamp"`
	SignatureVersion  string                         `json:"SignatureVersion"`
	Signature         string                         `json:"Signature"`
	SigningCertURL    string                         `json:"SigningCertURL"`
	UnsubscribeURL    string                         `json:"UnsubscribeURL"`
	MessageAttributes map[string]snsMessageAttribute `json:"MessageAttributes"`
}

type snsMessageAttribute struct {
	Type  string
	Value string
}

func (e *eventsClient) handlePubkeyEvent(ctx context.Context, message string) (err error) {
	caller := new(jwtvalidation.SigningEntity)
	err = json.NewDecoder(bytes.NewBufferString(message)).Decode(caller)
	if err != nil {
		return
	}

	select {
	case <-ctx.Done():
		err = ctx.Err()
	case e.pubkeys <- caller:
	}
	return
}

type capabilitiesEventHandledContextKey struct{}

func (e *eventsClient) handleCapabilitiesEvent(ctx context.Context, message string) (err error) {
	if ctx.Value(capabilitiesEventHandledContextKey{}) != nil {
		// we already pulled new capabilities, dont do it again here
		return
	}
	select {
	case <-ctx.Done():
		err = ctx.Err()
	case e.capabilities <- struct{}{}:
	}
	return
}
