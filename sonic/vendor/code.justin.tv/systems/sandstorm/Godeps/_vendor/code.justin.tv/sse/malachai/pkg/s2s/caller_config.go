package s2s

import (
	"github.com/cactus/go-statsd-client/statsd"

	"code.justin.tv/sse/malachai/pkg/config"
	"code.justin.tv/sse/malachai/pkg/registration"
)

// CallerConfig holds config options
type CallerConfig struct {
	// Required
	//
	// CallerName is the human-readable name of the service. This is used to
	// discover internal configuration options.
	CallerName string

	// optional config to disable secret rotations. if this is set to true,
	// sqs queues will not be created on startup. only disable for short usecases
	// such as lambda, ecs tasks, etc.
	DisableSecretRotationListener bool

	// for internal use only
	Region                    string
	Environment               string
	SandstormKMSKeyID         string
	SandstormSecretsTableName string
	SandstormTopicArn         string
	RegistrationConfig        *registration.Config
	roleArn                   string
}

// FillDefaults fills in default configuration options. This is called in
// the roundtripper constructor, so the client does not need to call this
// manually.
func (cfg *CallerConfig) FillDefaults() (err error) {
	if cfg.Environment == "" {
		cfg.Environment = "production"
	}

	resources, err := config.GetResources(cfg.Environment)
	if err != nil {
		return
	}

	if cfg.Region == "" {
		cfg.Region = resources.Region
	}

	if cfg.SandstormKMSKeyID == "" {
		cfg.SandstormKMSKeyID = resources.SandstormKMSKeyID
	}

	if cfg.SandstormSecretsTableName == "" {
		cfg.SandstormSecretsTableName = resources.SandstormSecretsTableName
	}

	if cfg.SandstormTopicArn == "" {
		cfg.SandstormTopicArn = resources.SandstormTopicArn
	}

	if cfg.RegistrationConfig == nil {
		cfg.RegistrationConfig = new(registration.Config)
	}

	cfg.RegistrationConfig.Environment = cfg.Environment
	cfg.RegistrationConfig.RoleArn = resources.AuxiliaryRoleArn
	err = cfg.RegistrationConfig.FillDefaults()
	if err != nil {
		return
	}
	return
}

func (cfg *CallerConfig) statsd(serviceName string) (statter statsd.Statter, err error) {
	return newStatter(cfg.Environment, serviceName)
}
