package s2s

import (
	"context"
	"net/http"

	"code.justin.tv/sse/malachai/pkg/capabilities"
	"code.justin.tv/sse/malachai/pkg/jwtvalidation"
)

// CapabilitiesContextKey is the key for the value in the request context
// containing the list of capabilities whitelisted for the caller.
var CapabilitiesContextKey = ContextKey("s2s-caller-capabilities")

// CalleeNameContextKey is the key for the value in the request context
// containing the name of the callee service.
var CalleeNameContextKey = ContextKey("s2s-callee-name")

// newCapabilitiesInjector returns middleware that injects the caller's
// capabilities (as a capabilities.Capabilities object) to the request context
func newCapabilitiesInjector(mgr *calleeCapabilitiesClient) (fn func(http.Handler) http.Handler) {
	fn = func(inner http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			var (
				caller *jwtvalidation.SigningEntity
				ok     bool
			)
			if caller, ok = r.Context().Value(CallerIDContextKey).(*jwtvalidation.SigningEntity); !ok {
				http.Error(w, "caller identity not set in request context", http.StatusInternalServerError)
				return
			}

			caps, err := mgr.get(caller.Caller)
			if err != nil {
				if err == capabilities.ErrCapabilitiesNotFound {
					http.Error(w, "required capabilities not found for caller", http.StatusUnauthorized)
					return
				}
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				return
			}

			ctx := context.WithValue(r.Context(), CapabilitiesContextKey, caps)
			ctx = context.WithValue(ctx, CalleeNameContextKey, mgr.cfg.callee)

			inner.ServeHTTP(w, r.WithContext(ctx))
		})
	}
	return
}
