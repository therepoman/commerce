package keystore

import (
	"fmt"

	"code.justin.tv/sse/malachai/pkg/discovery"
	"code.justin.tv/sse/malachai/pkg/jwtvalidation"

	"github.com/boltdb/bolt"
)

// defaults for boltdb
const (
	BoltDBBucketName = "pubkeys"
)

type boltReader struct {
	db *bolt.DB
}

// Get returns the value of a key in boltdb
func (br *boltReader) Get(caller *jwtvalidation.SigningEntity) (value []byte, err error) {
	err = br.db.View(func(tx *bolt.Tx) (err error) {
		b := tx.Bucket([]byte(BoltDBBucketName))
		value = b.Get([]byte(caller.EncodeToString()))
		if value == nil {
			err = discovery.ErrPubkeyNotFound
		}
		return
	})
	return
}

// Delete is called on invalidation, removes a specified public key from cache
func (br *boltReader) Delete(caller *jwtvalidation.SigningEntity) (err error) {
	err = br.db.Update(func(tx *bolt.Tx) error {
		return tx.Bucket([]byte(BoltDBBucketName)).Delete([]byte(caller.EncodeToString()))
	})
	return
}

func (br *boltReader) Put(caller *jwtvalidation.SigningEntity, value []byte) (err error) {
	err = br.db.Update(func(tx *bolt.Tx) (err error) {
		b := tx.Bucket([]byte(BoltDBBucketName))
		err = b.Put([]byte(caller.EncodeToString()), value)
		return
	})
	return
}

func newBoltReader(db *bolt.DB) (reader *boltReader, err error) {
	err = db.Update(func(tx *bolt.Tx) (err error) {
		_, err = tx.CreateBucketIfNotExists([]byte(BoltDBBucketName))
		if err != nil {
			err = fmt.Errorf("create bucket: %s", err)
			return
		}
		return
	})
	if err != nil {
		return
	}

	reader = &boltReader{
		db: db,
	}
	return
}
