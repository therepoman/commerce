package s2s

import (
	"errors"
	"time"

	"code.justin.tv/sse/malachai/pkg/config"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

const (
	sdkVersion = "1.0"
)

type inventoryConfig struct {
	Environment string
	Region      string
	RoleArn     string
	TableName   string
}

type inventoryClient struct {
	db     dynamodbiface.DynamoDBAPI
	config *inventoryConfig
}

//sdkVersionInfo stores information about s2s version being used.
type sdkVersionInfo struct {
	ServiceID   string                     `dynamodbav:"service_id"`
	ServiceName string                     `dynamodbav:"service_name"`
	InstanceID  string                     `dynamodbav:"instance_id"`
	Version     string                     `dynamodbav:"version"`
	CreatedAt   dynamodbattribute.UnixTime `dynamodbav:"created_at"`
	UpdatedAt   dynamodbattribute.UnixTime `dynamodbav:"updated_at"`
}

func (ic *inventoryConfig) fillDefaults() (err error) {
	if ic.Environment == "" {
		return errors.New("inventory config environment not set")
	}

	res, err := config.GetResources(ic.Environment)
	if err != nil {
		return
	}
	if ic.Region == "" {
		ic.Region = res.Region
	}
	if ic.TableName == "" {
		ic.TableName = res.InventoryTableName
	}
	return
}

//newInventoryClient return a new client which can be used to update dynamodb with sdk version info
func newInventoryClient(cfg *inventoryConfig) (client *inventoryClient, err error) {
	err = cfg.fillDefaults()
	if err != nil {
		return
	}
	sess := session.New(config.AWSConfig(cfg.Region, cfg.RoleArn))
	ddb := dynamodb.New(sess)
	client = &inventoryClient{
		db:     ddb,
		config: cfg,
	}
	return
}

// putInventory Udpates sdk version info in dynamodb
func (client *inventoryClient) putInventory(svi *sdkVersionInfo) (err error) {

	currentTime := dynamodbattribute.UnixTime(time.Now())
	svi.UpdatedAt = currentTime

	err = client.updateItem(svi)
	if err != nil {
		if err.(awserr.Error).Code() == dynamodb.ErrCodeConditionalCheckFailedException {
			// Record does not exist. Insert a new record.
			svi.CreatedAt = currentTime
			var item map[string]*dynamodb.AttributeValue
			item, err = dynamodbattribute.MarshalMap(&svi)
			if err != nil {
				return
			}
			putInput := &dynamodb.PutItemInput{
				Item:      item,
				TableName: aws.String(client.config.TableName),
			}
			_, err = client.db.PutItem(putInput)
			return
		}
	}
	return
}

func (client *inventoryClient) updateItem(svi *sdkVersionInfo) (err error) {
	updateAtAV := &dynamodb.AttributeValue{}
	err = svi.UpdatedAt.MarshalDynamoDBAttributeValue(updateAtAV)
	if err != nil {
		return
	}

	input := &dynamodb.UpdateItemInput{
		ExpressionAttributeNames: map[string]*string{
			"#uat": aws.String("updated_at"),
			"#sid": aws.String("service_id"),
			"#iid": aws.String("instance_id"),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":uat": updateAtAV,
			":sid": {
				S: aws.String(svi.ServiceID),
			},
			":iid": {
				S: aws.String(svi.InstanceID),
			},
		},
		Key: map[string]*dynamodb.AttributeValue{
			"service_id": {
				S: aws.String(svi.ServiceID),
			},
			"instance_id": {
				S: aws.String(svi.InstanceID),
			},
		},
		TableName:           aws.String(client.config.TableName),
		UpdateExpression:    aws.String("SET #uat= :uat"),
		ConditionExpression: aws.String("#sid = :sid AND #iid = :iid"),
	}

	_, err = client.db.UpdateItem(input)
	return
}
