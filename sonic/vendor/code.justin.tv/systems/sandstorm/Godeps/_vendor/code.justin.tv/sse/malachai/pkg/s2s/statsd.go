package s2s

import (
	"fmt"
	"time"

	"code.justin.tv/sse/malachai/pkg/config"

	"github.com/cactus/go-statsd-client/statsd"
)

func newStatter(environment, serviceName string) (statter statsd.Statter, err error) {
	if environment == "testing" {
		statter, err = statsd.NewNoopClient()
		return
	}

	resources := config.GetResourcesForProduction()
	prefix := fmt.Sprintf("%s.%s.%s.%s", "malachai", environment, resources.Region, serviceName)

	statter, err = statsd.NewBufferedClient(statsdHost, prefix, time.Second, 0)
	return
}
