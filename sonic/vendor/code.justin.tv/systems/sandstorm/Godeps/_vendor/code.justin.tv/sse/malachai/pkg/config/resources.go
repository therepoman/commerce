package config

import (
	"errors"
)

// Resources represents all the resources we need for a deployment
type Resources struct {
	AuditTableName      string `default:"malachai-audit-production"`
	MalachaiAdminGroups []string
	// list of groups that can create private services
	MalachaiAutomationGroups      []string
	CapabilitiesTable             string `default:"malachai-capabilities-production"`
	CapabilitiesTableArn          string `default:"arn:aws:dynamodb:us-west-2:180116294062:table/malachai-capabilities-production"`
	CapabilitiesLambdaRoleArn     string
	MalachaiGQLUrl                string
	Region                        string `default:"us-west-2"`
	ServicesTable                 string `default:"malachai-services-production"`
	ServicesTableArn              string `default:"arn:aws:dynamodb:us-west-2:180116294062:table/malachai-services-production"`
	DiscoveryTable                string `default:"malachai-discovery-production"`
	DiscoveryTableArn             string `default:"arn:aws:dynamodb:us-west-2:180116294062:table/malachai-discovery-production"`
	GuardianEndpoint              string
	CalleeCapabilitiesTopicARN    string
	MalachaiWhitelistRoleName     string
	PubkeyInvalidationTopicARN    string `default:"arn:aws:sns:us-west-2:180116294062:pubkey-invalidations-production"`
	TestUserArn                   string `default:"arn:aws:iam::289423984879:user/sse-admin-testing"`
	SandstormKMSKeyID             string `default:"alias/sandstorm-production"`
	SandstormSecretsTableName     string `default:"sandstorm-production"`
	SandstormSecretsTableArn      string `default:"arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-production"`
	SandstormSecretsAuditTableArn string `default:"arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-production_audit"`
	SandstormAuxPolicyArn         string `default:"arn:aws:iam::734326455073:policy/sandstorm/production/aux_policy/sandstorm-agent-production-aux"`
	SandstormRoleOwnerTableName   string `default:"sandstorm-production_role_owners"`
	SandstormNamespaceTableArn    string `default:"arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-production_namespaces"`
	SandstormTopicArn             string
	SandstormRoleArn              string `default:"arn:aws:iam::734326455073:role/terraform/production/role/malachai00049650eec2b974926bf9d0b3"`
	Environment                   string `default:"production"`
	RootUserArn                   string `default:"arn:aws:iam::180116294062:root"`
	RoleArn                       string
	AuditTableReadWriteRoleArn    string
	AuditTableReadRoleArn         string
	MalachaiAccountID             string `default:"180116294062"`
	FirehoseDeliveryStreamName    string
	FirehoseLogsS3BucketName      string
	AuxiliaryRoleArn              string `default:"arn:aws:iam::180116294062:role/malachai/auxiliary/malachai-services-read-only-production"`
	FirehoseLoggerRoleArn         string `default:"arn:aws:iam::180116294062:role/malachai/auxiliary/malachai-services-firehose-logger-production"`
	CORSAllowedHeaders            []string
	CORSAllowedOrigins            []string
	CORSMaxAge                    string
	InventoryTableName            string `default:"malachai-inventory-production"`
	InventoryTableArn             string `default:"arn:aws:dynamodb:us-west-2:180116294062:table/malachai-inventory-production"`
}

// GetResourcesForEnvironment returns resource names for env
func GetResourcesForEnvironment(env string) *Resources {
	return &Resources{
		MalachaiAdminGroups:           []string{"team-sse"},
		MalachaiAutomationGroups:      []string{"team-sse-automation"},
		AuditTableName:                "malachai-audit-table-" + env,
		CapabilitiesTable:             "malachai-capabilities-" + env,
		Region:                        "us-west-2",
		ServicesTable:                 "malachai-services-" + env,
		DiscoveryTable:                "malachai-discovery-" + env,
		SandstormKMSKeyID:             "alias/sandstorm-" + env,
		SandstormSecretsTableName:     "sandstorm-" + env,
		InventoryTableName:            "malachai-inventory-" + env,
		Environment:                   env,
		SandstormSecretsTableArn:      "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-" + env,
		SandstormSecretsAuditTableArn: "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-" + env + "_audit",
		SandstormNamespaceTableArn:    "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-" + env + "_namespaces",
		SandstormRoleOwnerTableName:   "sandstorm-" + env + "_role_owners",
		SandstormAuxPolicyArn:         "arn:aws:iam::734326455073:policy/sandstorm/" + env + "/aux_policy/sandstorm-agent-" + env + "-aux",
		RoleArn:                       "arn:aws:iam::289423984879:role/malachai-admin-role-" + env,
		FirehoseDeliveryStreamName:    "firehose-to-s3-logs-bucket-delivery-stream-" + env,
		FirehoseLogsS3BucketName:      "malachai-firehose-logs-" + env,
		MalachaiWhitelistRoleName:     "malachai-services-read-only-" + env,
		CORSAllowedHeaders: []string{
			"Authorization",
			"Content-Type",
		},
		CORSMaxAge: "5h",
	}
}

// GetResourcesForTesting returns resources for testing
func GetResourcesForTesting() (res *Resources) {
	res = GetResourcesForEnvironment("testing")
	res.CalleeCapabilitiesTopicARN = "arn:aws:sns:us-west-2:289423984879:capabilities-callee"
	res.CapabilitiesLambdaRoleArn = "arn:aws:iam::289423984879:role/malachai-capabilities-lambda-testing"
	res.MalachaiGQLUrl = "https://malachai-api-staging.internal.justin.tv"
	res.PubkeyInvalidationTopicARN = "arn:aws:sns:us-west-2:289423984879:pubkey-invalidations-testing"
	res.CapabilitiesTableArn = "arn:aws:dynamodb:us-west-2:289423984879:table/malachai-capabilities-testing"
	res.ServicesTableArn = "arn:aws:dynamodb:us-west-2:289423984879:table/malachai-services-testing"
	res.DiscoveryTableArn = "arn:aws:dynamodb:us-west-2:289423984879:table/malachai-discovery-testing"
	res.TestUserArn = "arn:aws:iam::289423984879:user/sse-admin-testing"
	res.SandstormRoleArn = "arn:aws:iam::734326455073:role/terraform/testing/role/malachai00049650eec2b974926bf9d0b5"
	res.SandstormTopicArn = "arn:aws:sns:us-west-2:734326455073:sandstorm-testing"
	res.RootUserArn = "arn:aws:iam::289423984879:root"
	res.AuditTableReadWriteRoleArn = "arn:aws:iam::289423984879:role/malachai-audit-table-admin-testing"
	res.AuditTableReadRoleArn = "arn:aws:iam::289423984879:role/malachai-audit-table-read-testing"
	res.MalachaiAccountID = "289423984879"
	res.AuxiliaryRoleArn = "arn:aws:iam::289423984879:role/malachai/auxiliary/malachai-services-read-only-testing"
	res.FirehoseLoggerRoleArn = "arn:aws:iam::289423984879:role/malachai/auxiliary/malachai-services-firehose-logger-testing"
	res.GuardianEndpoint = "https://guardian-staging.dev.us-west2.justin.tv"
	res.CORSAllowedOrigins = []string{
		"https://dashboard-v2-staging.internal.justin.tv",
	}
	res.InventoryTableArn = "arn:aws:dynamodb:us-west-2:289423984879:table/malachai-inventory-testing"
	return
}

// GetResourcesForProduction returns resources for production
// The management service needs to set the CalleeCapabilitiesTopicARN, which is
// unique to each callee service.
func GetResourcesForProduction() (res *Resources) {
	res = GetResourcesForEnvironment("production")
	res.AuditTableReadWriteRoleArn = "arn:aws:iam::180116294062:role/malachai-audit-table-admin-production"
	res.AuditTableReadRoleArn = "arn:aws:iam::180116294062:role/malachai-audit-table-read-production"
	res.CalleeCapabilitiesTopicARN = "arn:aws:sns:us-west-2:180116294062:capabilities-callee"
	res.MalachaiGQLUrl = "https://malachai-api.internal.justin.tv"
	res.PubkeyInvalidationTopicARN = "arn:aws:sns:us-west-2:180116294062:pubkey-invalidations-production"
	res.CapabilitiesTableArn = "arn:aws:dynamodb:us-west-2:180116294062:table/malachai-capabilities-production"
	res.CapabilitiesLambdaRoleArn = "arn:aws:iam::180116294062:role/malachai-capabilities-lambda-production"
	res.SandstormRoleArn = "arn:aws:iam::734326455073:role/terraform/production/role/malachai00049650eec2b974926bf9d0b3"
	res.SandstormTopicArn = "arn:aws:sns:us-west-2:734326455073:sandstorm-production"
	res.ServicesTableArn = "arn:aws:dynamodb:us-west-2:180116294062:table/malachai-services-production"
	res.DiscoveryTableArn = "arn:aws:dynamodb:us-west-2:180116294062:table/malachai-discovery-production"
	res.RootUserArn = "arn:aws:iam::180116294062:root"
	res.MalachaiAccountID = "180116294062"
	res.AuxiliaryRoleArn = "arn:aws:iam::180116294062:role/malachai/auxiliary/malachai-services-read-only-production"
	res.RoleArn = "arn:aws:iam::180116294062:role/malachai-admin-role-production"
	res.FirehoseLoggerRoleArn = "arn:aws:iam::180116294062:role/malachai/auxiliary/malachai-services-firehose-logger-production"
	res.GuardianEndpoint = "https://guardian.internal.justin.tv"
	res.CORSAllowedOrigins = []string{
		"https://dashboard-v2.internal.justin.tv",
	}
	res.InventoryTableArn = "arn:aws:dynamodb:us-west-2:180116294062:table/malachai-inventory-production"
	return
}

// GetResources returns the resources for an environment string
func GetResources(environment string) (res *Resources, err error) {
	switch environment {
	case "testing":
		res = GetResourcesForTesting()
	case "production":
		res = GetResourcesForProduction()
	default:
		err = errors.New("environment must be 'testing' or 'production'")
	}
	return
}
