package ccg

import (
	"net/http"
	"net/url"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"code.justin.tv/sse/malachai/pkg/s2s"
)

// S2SConfig holds configuration options for the s2s-enabled changelog client
type S2SConfig struct {
	CallerName string

	// defaults to changelog-s2s.internal.justin.tv
	Host string

	// optional config to disable secret rotations. if this is set to true,
	// sqs queues will not be created on startup. only disable for short usecases
	// such as lambda, ecs tasks, etc.
	DisableSecretRotationListener bool
}

func (cfg *S2SConfig) fillDefaults() {
	if cfg.Host == "" {
		cfg.Host = "changelog-s2s.internal.justin.tv"
	}
}

func (cfg *S2SConfig) validate() (err error) {
	cfg.fillDefaults()

	if cfg.CallerName == "" {
		err = errors.New("changelog: required config value CallerName is empty")
		return
	}
	return
}

// NewS2SClient constructs an s2s-enabled changelog client
func NewS2SClient(cfg *S2SConfig, logger logrus.FieldLogger) (client *Client, err error) {
	if cfg == nil {
		err = errors.New("changelog: config is required")
		return
	}

	err = cfg.validate()
	if err != nil {
		return
	}

	roundTripper, err := s2s.NewRoundTripper(&s2s.CallerConfig{
		CallerName:                    cfg.CallerName,
		DisableSecretRotationListener: cfg.DisableSecretRotationListener,
	}, logger)
	if err != nil {
		return
	}

	endpoint := &url.URL{
		Scheme: "https",
		Host:   cfg.Host,
		Path:   "/api/events",
	}

	client = &Client{
		Endpoint: endpoint.String(),
		HTTPClient: &http.Client{
			Transport: roundTripper,
		},
	}
	return
}
