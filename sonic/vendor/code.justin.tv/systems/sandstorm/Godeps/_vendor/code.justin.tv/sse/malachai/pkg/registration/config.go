package registration

import (
	"code.justin.tv/sse/malachai/pkg/config"
)

// dynamodb names
const (
	serviceIDKey     = "service_id"
	serviceNameKey   = "service_name"
	serviceByIDIndex = "services-by-id"
)

const iamPathPrefix = "malachai/registration/"

// Config holds options for service registration
type Config struct {
	Region      string
	RoleArn     string
	TableName   string
	Environment string // testing or production
	RootUserArn string

	MalachaiAccountID string

	CapabilitiesTableArn        string
	ServiceRegistrationTableArn string
	DiscoveryTableArn           string
	InventoryTableArn           string
	InventoryTableName          string
	PubkeyInvalidationTopicArn  string

	//Sandstorm policy generator
	SandstormKMSKeyID             string
	SandstormSecretsTableName     string
	SandstormRoleArn              string
	SandstormAuxPolicyArn         string
	SandstormSecretsTableArn      string
	SandstormSecretsAuditTableArn string
	SandstormNamespaceTableArn    string
	SandstormRoleOwnerTableName   string
	SandstormTopicArn             string
}

// FillDefaults fills empty values with defaults
func (c *Config) FillDefaults() (err error) {
	if c.Environment == "" {
		c.Environment = "production"
	}

	resources, err := config.GetResources(c.Environment)
	if err != nil {
		return
	}

	if c.Region == "" {
		c.Region = resources.Region
	}

	if c.TableName == "" {
		c.TableName = resources.ServicesTable
	}

	if c.MalachaiAccountID == "" {
		c.MalachaiAccountID = resources.MalachaiAccountID
	}

	if c.CapabilitiesTableArn == "" {
		c.CapabilitiesTableArn = resources.CapabilitiesTableArn
	}

	if c.ServiceRegistrationTableArn == "" {
		c.ServiceRegistrationTableArn = resources.ServicesTableArn
	}

	if c.DiscoveryTableArn == "" {
		c.DiscoveryTableArn = resources.DiscoveryTableArn
	}

	if c.PubkeyInvalidationTopicArn == "" {
		c.PubkeyInvalidationTopicArn = resources.PubkeyInvalidationTopicARN
	}

	if c.RootUserArn == "" {
		c.RootUserArn = resources.RootUserArn
	}

	if c.SandstormAuxPolicyArn == "" {
		c.SandstormAuxPolicyArn = resources.SandstormAuxPolicyArn
	}

	if c.SandstormRoleOwnerTableName == "" {
		c.SandstormRoleOwnerTableName = resources.SandstormRoleOwnerTableName
	}

	if c.SandstormSecretsTableArn == "" {
		c.SandstormSecretsTableArn = resources.SandstormSecretsTableArn
	}

	if c.SandstormSecretsTableName == "" {
		c.SandstormSecretsTableName = resources.SandstormSecretsTableName
	}

	if c.SandstormKMSKeyID == "" {
		c.SandstormKMSKeyID = resources.SandstormKMSKeyID
	}

	if c.SandstormRoleArn == "" {
		c.SandstormRoleArn = resources.SandstormRoleArn
	}

	if c.SandstormSecretsAuditTableArn == "" {
		c.SandstormSecretsAuditTableArn = resources.SandstormSecretsAuditTableArn
	}

	if c.SandstormNamespaceTableArn == "" {
		c.SandstormNamespaceTableArn = resources.SandstormNamespaceTableArn
	}

	if c.InventoryTableArn == "" {
		c.InventoryTableArn = resources.InventoryTableArn
	}

	if c.InventoryTableName == "" {
		c.InventoryTableName = resources.InventoryTableName
	}

	return
}
