package s2s

import (
	"crypto"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"time"

	"code.justin.tv/sse/malachai/pkg/jwtvalidation"
	"code.justin.tv/sse/malachai/pkg/signature"
	"code.justin.tv/systems/sandstorm/manager"

	joseCrypto "github.com/SermoDigital/jose/crypto"
	"github.com/cenkalti/backoff"
)

var defaultSigningMethod = joseCrypto.SigningMethodRS256

func newPrivateKeyStorer(mgr manager.API, sandstormSecretName string) (keyStorer signature.PrivateKeyStorer) {
	return func() (key crypto.Signer, fingerprint string, err error) {
		var secret *manager.Secret
		err = backoff.Retry(func() (err error) {
			secret, err = mgr.Get(sandstormSecretName)
			if err != nil {
				return
			}

			if secret == nil {
				err = errors.New("signing key plaintext not found in sandstorm")
				return
			}

			return
		}, backoff.WithMaxTries(backoff.NewConstantBackOff(time.Second), 10))
		if err != nil {
			return
		}

		block, _ := pem.Decode(secret.Plaintext)
		if block == nil {
			err = errors.New("private key loaded from sandstorm is not valid pem encoded block")
			return
		}

		privateKey, err := x509.ParsePKCS1PrivateKey(block.Bytes)
		if err != nil {
			return
		}

		fingerprint, err = jwtvalidation.CalculatePubkeyFingerprintFromPrivateKey(privateKey)
		if err != nil {
			return
		}

		key = privateKey
		return
	}
}
