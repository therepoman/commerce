package s2s

import (
	"fmt"
	"net/http"
	"strings"

	"code.justin.tv/sse/malachai/pkg/capabilities"
	"code.justin.tv/sse/malachai/pkg/jwtvalidation"
)

// newCapabilitiesAuthorizer returns middleware that authorizes the caller's
// capabilities (as a capabilities.Capabilities object)
func newCapabilitiesAuthorizer() (fn func(http.Handler) http.Handler) {
	fn = func(inner http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			var (
				caps *capabilities.Capabilities
				ok   bool
			)

			if caps, ok = r.Context().Value(CapabilitiesContextKey).(*capabilities.Capabilities); !ok {
				http.Error(w, "caller capabilities are not set in request context", http.StatusInternalServerError)
				return
			}
			err := isRequestAuthorized(r, caps)
			if err != nil {
				http.Error(w, err.Error(), http.StatusUnauthorized)
				return
			}
			inner.ServeHTTP(w, r.WithContext(r.Context()))
		})
	}
	return
}
func isRequestAuthorized(r *http.Request, caps *capabilities.Capabilities) (err error) {
	requestedCap := buildRequestedCapabilitiesFromHTTPRequest(r)
	authorized := caps.Contains(requestedCap)
	if !authorized {
		caller := r.Context().Value(CallerIDContextKey).(*jwtvalidation.SigningEntity).Caller
		callee := r.Context().Value(CalleeNameContextKey)
		err = fmt.Errorf("caller %s does not have authorization to perform capability %s on callee %s", caller, requestedCap, callee)
	}
	return
}

func buildRequestedCapabilitiesFromHTTPRequest(r *http.Request) (cap string) {
	//Remove leading/trailing whitespaces and slashes from URL.Path
	urlPath := strings.Trim(r.URL.Path, " ")
	if !strings.HasPrefix(urlPath, "/") {
		urlPath = fmt.Sprintf("/%s", urlPath)
	}

	cap = fmt.Sprintf("%s %s", r.Method, urlPath)
	return
}
