package logging

import "github.com/sirupsen/logrus"

type noOpWriter struct {
}

// Write implements io.Writer
func (nol *noOpWriter) Write(p []byte) (n int, err error) {
	return
}

// NewNoOpLogger creates a noop fieldlogger
func NewNoOpLogger() (l logrus.FieldLogger) {
	logger := logrus.New()
	logger.Out = &noOpWriter{}
	l = logger
	return
}
