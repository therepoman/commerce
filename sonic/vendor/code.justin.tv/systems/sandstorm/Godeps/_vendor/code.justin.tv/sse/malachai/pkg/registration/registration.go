package registration

import (
	"context"
	"errors"
	"net/http"
	"path"
	"time"

	"code.justin.tv/sse/malachai/pkg/config"
	"code.justin.tv/sse/malachai/pkg/logging"
	"code.justin.tv/systems/sandstorm/manager"
	"code.justin.tv/systems/sandstorm/policy"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/iam"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sns/snsiface"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
)

// errors
var (
	ErrServiceNameAlreadyRegistered = errors.New("service with same name already registered")
)

const (
	ownerTeamSSE = "team-sse"
)

type registrar struct {
	db      dynamodbiface.DynamoDBAPI
	sns     snsiface.SNSAPI
	cfg     *Config
	ig      IAMGenerator
	log     logrus.FieldLogger
	secrets manager.API
}

//UpdateServiceRequest input to update an existing malachai service.
type UpdateServiceRequest struct {
	ServiceName     string
	UpdatePolicy    bool
	OwnerLDAPGroups *[]string
	AllowedArns     *[]string
	UpdatedByUser   string
}

//UpdateServiceByIDRequest input to update an existing malachai service.
type UpdateServiceByIDRequest struct {
	ServiceID       string
	UpdatePolicy    bool
	OwnerLDAPGroups *[]string
	AllowedArns     *[]string
	UpdatedByUser   string
}

// Registrar registers services and provides lookups
type Registrar interface {
	Get(name string) (svc *Service, err error)
	GetServiceByID(serviceID string) (svc *Service, err error)
	GetServicesWithContext(ctx context.Context) (output *GetServicesOutput, err error)
	Deregister(serviceName string) (err error)
	DeregisterByID(serviceID string) (svc *Service, err error)
	Register(request *Request) (svc *Service, err error)
	UpdateService(request *UpdateServiceRequest) (svc *Service, err error)
	UpdateServiceByID(request *UpdateServiceByIDRequest) (svc *Service, err error)
}

// New initializes a new registration client. Pass in a nil or empty Config to
// use default values.
func New(cfg *Config, logger logrus.FieldLogger) (reg Registrar, err error) {
	return newRegistrar(cfg, logger)
}

func newRegistrar(cfg *Config, logger logrus.FieldLogger) (reg *registrar, err error) {
	if cfg == nil {
		cfg = new(Config)
	}

	err = cfg.FillDefaults()
	if err != nil {
		return
	}

	sess := session.New(config.AWSConfig(cfg.Region, cfg.RoleArn))

	sandstormSess := session.New(config.AWSConfig(
		cfg.Region, cfg.RoleArn, cfg.SandstormRoleArn))

	if err != nil {
		return
	}

	spg := &policy.IAMPolicyGenerator{
		AuxPolicyArn:                 cfg.SandstormAuxPolicyArn,
		DynamoDBSecretsTableArn:      cfg.SandstormSecretsTableArn,
		DynamoDBSecretsAuditTableArn: cfg.SandstormSecretsAuditTableArn,
		DynamoDBNamespaceTableArn:    cfg.SandstormNamespaceTableArn,
		RoleOwnerTableName:           cfg.SandstormRoleOwnerTableName,
		IAM:                          iam.New(sandstormSess),
		DynamoDB:                     dynamodb.New(sandstormSess),
		Logger:                       logging.NewNoOpLogger(),
	}

	ig, err := newIAMGenerator(spg, iam.New(sess), sess, cfg)
	if err != nil {
		return
	}

	reg = &registrar{
		cfg: cfg,
		db:  dynamodb.New(sess),
		sns: sns.New(sess),
		ig:  ig,
		log: logger,
		secrets: manager.New(manager.Config{
			AWSConfig: config.AWSConfig(cfg.Region, cfg.SandstormRoleArn),
			KeyID:     cfg.SandstormKMSKeyID,
			TableName: cfg.SandstormSecretsTableName,
		}),
	}
	return
}

// Request describes a service registration request
type Request struct {
	Name            string
	OwnerLDAPGroups []string
	AssumeRoleArns  []string

	// if Private is set to true, event will be broadcasted to a separate
	// changelog category
	Private bool

	// user that created service
	CreatedByUser string
}

func (req *Request) validate() (err error) {
	if req.Name == "" {
		return errors.New("name is required")
	}

	return nil
}

func sandstormSecretName(environment, serviceID string) (keyName string) {
	// sse/malachai/<environment>/signing_keys/<caller_service_id>
	return path.Join("sse/malachai/", environment, "signing_keys", serviceID)
}

func (reg *registrar) Register(request *Request) (svc *Service, err error) {
	err = request.validate()
	if err != nil {
		return
	}
	serviceExist, err := reg.serviceAlreadyRegistered(request.Name)
	if err != nil {
		return
	}

	if serviceExist {
		reg.log.Errorf("service registration failed, service with name '%s' already exists", request.Name)
		err = ErrServiceNameAlreadyRegistered
		return
	}

	reg.log.Debugf("registering a new service: %s", request.Name)

	svc = new(Service)
	svc.ID = uuid.NewV4().String()
	svc.CreatedAt = time.Now().UTC()
	svc.Name = request.Name
	svc.OwnerLDAPGroups = request.OwnerLDAPGroups
	svc.AllowedArns = request.AssumeRoleArns
	svc.SandstormSecretName = sandstormSecretName(reg.cfg.Environment, svc.ID)
	svc.LastUpdatedByUser = request.CreatedByUser
	svc.Private = request.Private

	svc.CapabilitiesTopicArn, err = reg.createSNSTopic(svc.getCapabilitiesTopic())
	if err != nil {
		return
	}

	role, err := reg.ig.RegisterServiceRole(&RegisterServiceRoleRequest{
		Service:        svc,
		AssumeRoleArns: request.AssumeRoleArns,
		env:            reg.cfg.Environment,
	})
	if err != nil {
		reg.log.Errorf("failed to register service with name: '%s', err: %s", svc.Name, err.Error())
		return
	}
	reg.log.Debugf("successfully registered service role for service with name '%s'", svc.Name)

	svc.RoleArn = role.RoleArn
	// TODO these will be removed
	svc.CalleeRoleArn = role.RoleArn
	svc.CallerRoleArn = role.RoleArn

	svc.SandstormRoleArn = role.SandstormRoleArn
	svc.SandstormRoleName = role.SandstormRoleName

	item, err := dynamodbattribute.MarshalMap(&svc)
	if err != nil {
		return
	}

	putInput := &dynamodb.PutItemInput{
		Item:                item,
		TableName:           aws.String(reg.cfg.TableName),
		ConditionExpression: aws.String("attribute_not_exists(service_name)"),
	}
	_, err = reg.db.PutItem(putInput)
	if err != nil {
		reg.log.Errorf("failed to put item with name: '%s' in services table. err: %s", svc.Name, err.Error())
		if aErr, ok := err.(awserr.RequestFailure); ok {
			if aErr.StatusCode() == http.StatusBadRequest {
				err = ErrServiceNameAlreadyRegistered
				//This should never happen, we ensured that the service was not present in the DB before proceeding.
				reg.log.Error("unexpected error putting service registration to dynamodb, err: " + err.Error())
			}
		}
	}
	if err == nil {
		reg.log.Debugf("service registration for service with name '%s' complete", request.Name)
	}
	return
}

func (reg *registrar) createSNSTopic(topicName string) (topicArn string, err error) {
	params := &sns.CreateTopicInput{
		Name: aws.String(topicName),
	}

	output, err := reg.sns.CreateTopic(params)
	if err != nil {
		reg.log.Errorf("failed to create SNS topic named '%s', err: %s", topicName, err.Error())
		return
	}

	topicArn = aws.StringValue(output.TopicArn)
	reg.log.Infof("successfully created SNS topic named '%s' with arn: '%s'", topicName, topicArn)
	return
}

func (reg *registrar) deleteSNSTopic(topicArn string) (err error) {

	_, err = reg.sns.DeleteTopic(&sns.DeleteTopicInput{
		TopicArn: aws.String(topicArn),
	})
	return
}

func (reg *registrar) serviceAlreadyRegistered(serviceName string) (exist bool, err error) {

	exist = false

	getItemInput := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"service_name": {
				S: aws.String(serviceName),
			},
		},
		TableName: aws.String(reg.cfg.TableName),
	}

	resp, err := reg.db.GetItem(getItemInput)
	if err != nil {
		reg.log.Error("failed to get service details from dynamodb: " + err.Error())
		return
	}
	if len(resp.Item) > 0 {
		exist = true
	}
	return
}

func (reg *registrar) Deregister(serviceName string) (err error) {
	svc, err := reg.Get(serviceName)
	if err != nil {
		if err == ErrServiceDoesNotExist {
			reg.log.Warnf("deregistering service failed. Service with name '%s' does not exist", serviceName)
			err = nil
		} else {
			reg.log.Warn("failed to get service details from dynamodb. err: " + err.Error())
		}
		return
	}

	err = reg.deregister(svc)
	return
}

func (reg *registrar) DeregisterByID(serviceID string) (svc *Service, err error) {
	svc, err = reg.GetServiceByID(serviceID)
	if err != nil {
		return
	}

	err = reg.deregister(svc)
	return
}

func (reg *registrar) deregister(svc *Service) (err error) {
	if svc.CapabilitiesTopicArn != "" {
		err = reg.deleteSNSTopic(svc.CapabilitiesTopicArn)
		if err != nil {
			reg.log.Warnf("failed to delete SNS topic with arn '%s'. err: %s", svc.CapabilitiesTopicArn, err.Error())
			return
		}
	}

	err = reg.ig.DeregisterServiceRole(svc)
	if err != nil {
		reg.log.Warnf("failed to deregister service with name '%s', err: %s", svc.Name, err.Error())
		return
	}
	reg.log.Infof("successfully deregistered service with name '%s'", svc.Name)

	input := &dynamodb.DeleteItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"service_name": {S: aws.String(svc.Name)},
		},
		TableName: aws.String(reg.cfg.TableName),
	}
	_, err = reg.db.DeleteItem(input)
	if err != nil {
		reg.log.Errorf("failed to remove service with name '%s' from service table, err: %s", svc.Name, err.Error())
	} else {
		reg.log.Infof("successfully removed service with name '%s' from service table", svc.Name)
	}

	err = reg.secrets.Delete(svc.SandstormSecretName)
	if err != nil {
		reg.log.Errorf("failed to delete secret for service id '%s'", svc.ID)
		err = nil
	}
	return
}

func (reg *registrar) UpdateServiceByID(req *UpdateServiceByIDRequest) (svc *Service, err error) {
	svc, err = reg.GetServiceByID(req.ServiceID)
	if err != nil {
		return
	}

	err = reg.updateService(svc, &updateServiceRequest{
		AllowedArns:     req.AllowedArns,
		OwnerLDAPGroups: req.OwnerLDAPGroups,
		UpdatePolicy:    req.UpdatePolicy,
		UpdatedByUser:   req.UpdatedByUser,
	})
	return
}

// UpdateService updates the auto generated policy attached to service
// role with the newer version of policy.
// This detaches/deletes the old policies after attaching the new policy.
func (reg *registrar) UpdateService(req *UpdateServiceRequest) (svc *Service, err error) {
	svc, err = reg.Get(req.ServiceName)
	if err != nil {
		return
	}

	err = reg.updateService(svc, &updateServiceRequest{
		AllowedArns:     req.AllowedArns,
		OwnerLDAPGroups: req.OwnerLDAPGroups,
		UpdatePolicy:    req.UpdatePolicy,
		UpdatedByUser:   req.UpdatedByUser,
	})
	return
}

//updateServiceRequest input to update an existing malachai service.
type updateServiceRequest struct {
	UpdatePolicy    bool
	OwnerLDAPGroups *[]string
	AllowedArns     *[]string
	UpdatedByUser   string
}

func (reg *registrar) updateService(svc *Service, req *updateServiceRequest) (err error) {
	if req.UpdatePolicy {
		err = reg.ig.UpdateServiceRolePolicy(&UpdateServiceRolePolicyRequest{
			Service: svc,
			env:     reg.cfg.Environment,
		})
		if err != nil {
			reg.log.Errorf("failed to update service role policies, service with name '%s', err: %s", svc.Name, err.Error())
			return
		}
		reg.log.Infof("successfully updated service role policies for service with name '%s'", svc.Name)
	}

	if req.AllowedArns == nil && req.OwnerLDAPGroups == nil {
		// No changes
		return
	}

	if req.AllowedArns != nil {
		svc.AllowedArns = *req.AllowedArns
		err = reg.ig.UpdateAssumeRolePolicy(svc, *req.AllowedArns)
		if err != nil {
			reg.log.Errorf("failed to update assume role policy for service with name '%s', err: %s", svc.Name, err.Error())
			return
		}
	}

	if req.OwnerLDAPGroups != nil {
		svc.OwnerLDAPGroups = *req.OwnerLDAPGroups
	}

	svc.UpdatedAt = time.Now().UTC()
	svc.LastUpdatedByUser = req.UpdatedByUser

	item, err := dynamodbattribute.MarshalMap(svc)
	if err != nil {
		return
	}

	putInput := &dynamodb.PutItemInput{
		Item:                item,
		TableName:           aws.String(reg.cfg.TableName),
		ConditionExpression: aws.String("attribute_exists(service_name)"),
	}
	_, err = reg.db.PutItem(putInput)
	if err != nil {
		//This should never happen, we ensured that the service was not present in the DB before proceeding.
		reg.log.Errorf("failed to update service, err: %s", err.Error())
		return
	}
	reg.log.Infof("service registration for '%s' complete.", svc.Name)
	return
}
