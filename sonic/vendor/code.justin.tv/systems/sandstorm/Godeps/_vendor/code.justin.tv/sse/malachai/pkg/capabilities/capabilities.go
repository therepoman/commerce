package capabilities

import (
	"errors"
	"time"

	"code.justin.tv/sse/malachai/pkg/internal/gql"

	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	graphql "github.com/neelance/graphql-go"
)

// dynamodb key names
const (
	CapabilitiesKeyCaller   = "caller"
	CapabilitiesKeyCallee   = "callee"
	capabilitiesIndexCaller = "by-caller"
)

const (
	//CapabilityAllowAll allows all caps for caller callee map
	CapabilityAllowAll = "*"
)

// errors
var (
	ErrCapabilitiesNotFound = errors.New("capabilities not found")
)

// StringSet is a set of strings
type StringSet map[string]bool

// Capabilities holds callee to caller capabilities map
type Capabilities struct {
	Capabilities      StringSet `dynamodbav:"capabilities,stringset" valid:"required"`
	Callee            string    `dynamodbav:"callee" valid:"required"`
	Caller            string    `dynamodbav:"caller" valid:"required"`
	UpdatedAt         time.Time `dynamodbav:"updated_at,unixtime"`
	LastUpdatedByUser string    `dynamodbav:"last_updated_by_user"`
}

// Key is the composite key for Capabilities
type Key struct {
	Caller string `dynamodbav:"caller"`
	Callee string `dynamodbav:"callee"`
}

// ToID outputs a graphql ID for the capability
func (k *Key) ToID() graphql.ID {
	return gql.MarshalGQLKey(k.Caller, k.Callee)
}

// UnmarshalID unmarshals a graphql id into a composite key
func (k *Key) UnmarshalID(id graphql.ID) (err error) {
	keys, err := gql.UnmarshalGQLKey(id, 2)
	k.Caller = keys[0]
	k.Callee = keys[0]
	return
}

// Cursor returns the rows cursor for pagination
func (c *Capabilities) Cursor() graphql.ID {
	key := &Key{
		Caller: c.Caller,
		Callee: c.Callee,
	}
	return key.ToID()
}

// ContainsAll returns true if c.Capabilities is a superset of capabilities
func (c *Capabilities) ContainsAll(capabilities []string) (containsAll bool) {
	for _, item := range capabilities {
		if !c.Contains(item) {
			return
		}
	}
	containsAll = true
	return
}

// Contains true if this capabilities set contains capability
func (c *Capabilities) Contains(capability string) (hasCapability bool) {
	if _, hasCapability = c.Capabilities[CapabilityAllowAll]; hasCapability {
		return
	}
	_, hasCapability = c.Capabilities[capability]
	return
}

// Equal returns true if capabilities have not changed
func (c *Capabilities) Equal(other *Capabilities) (equal bool) {
	switch {
	case c.Caller != other.Caller:
		return
	case c.Callee != other.Callee:
		return
	case len(c.Capabilities) != len(other.Capabilities):
		return
	}

	for item := range c.Capabilities {
		if !other.Contains(item) {
			return
		}
	}

	equal = true
	return
}

// Marshal as an input for dynamodb
func (c *Capabilities) Marshal() (attributeValues map[string]*dynamodb.AttributeValue, err error) {
	attributeValues, err = dynamodbattribute.MarshalMap(c)
	return
}

// ToStringSlice converts the capabilities to a slice of strings
func (c *Capabilities) ToStringSlice() (caps []string) {
	caps = make([]string, 0, len(c.Capabilities))
	for cap := range c.Capabilities {
		caps = append(caps, cap)
	}
	return
}
