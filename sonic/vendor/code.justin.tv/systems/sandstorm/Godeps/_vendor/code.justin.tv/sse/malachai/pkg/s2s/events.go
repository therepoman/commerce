package s2s

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"code.justin.tv/sse/malachai/pkg/config"
	"code.justin.tv/sse/malachai/pkg/jwtvalidation"
	"code.justin.tv/sse/policy"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sns/snsiface"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/boltdb/bolt"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
)

const (
	boltEventsClientStateKey = "client_state"
	boltEventsBucketName     = "events"

	sqsBatchSize   = 1
	sqsPollTimeout = 20
)

type eventsConfig struct {
	Environment                string
	RoleArn                    string
	Region                     string
	CalleeCapabilitiesTopicARN string
	PubkeyInvalidationTopicARN string
	ServiceID                  string
}

type queueState int

// FillDefaults fill sthe defaults
func (ec *eventsConfig) FillDefaults() (err error) {
	resources, err := config.GetResources(ec.Environment)
	if err != nil {
		return
	}

	if ec.Region == "" {
		ec.Region = "us-west-2"
	}

	if ec.PubkeyInvalidationTopicARN == "" {
		ec.PubkeyInvalidationTopicARN = resources.PubkeyInvalidationTopicARN
	}

	if ec.Region == "" {
		ec.Region = resources.Region
	}

	if ec.CalleeCapabilitiesTopicARN == "" {
		ec.CalleeCapabilitiesTopicARN = resources.CalleeCapabilitiesTopicARN
	}

	if ec.PubkeyInvalidationTopicARN == "" {
		ec.PubkeyInvalidationTopicARN = resources.PubkeyInvalidationTopicARN
	}

	return
}

func newEventsClient(cfg *eventsConfig, db *bolt.DB, logger logrus.FieldLogger) (client *eventsClient, err error) {
	err = cfg.FillDefaults()
	if err != nil {
		return
	}

	logger.Debugf("starting events client with roleArn: '%s'", cfg.RoleArn)

	sess := session.New(config.AWSConfig(cfg.Region, cfg.RoleArn))
	client = &eventsClient{
		pubkeys:      make(chan *jwtvalidation.SigningEntity),
		capabilities: make(chan struct{}),
		health:       new(Health),
		log:          logger,
		sns:          sns.New(sess),
		sqs:          sqs.New(sess),
		cfg:          cfg,
		db:           db,
	}

	err = client.loadState()
	if err != nil {
		return
	}
	return
}

type eventsClient struct {
	pubkeys      chan *jwtvalidation.SigningEntity
	capabilities chan struct{}

	health *Health
	log    logrus.FieldLogger
	sns    snsiface.SNSAPI
	sqs    sqsiface.SQSAPI
	cfg    *eventsConfig
	db     *bolt.DB
	state  *eventsClientState
}

type eventsClientState struct {
	QueueARN                               string
	QueueURL                               string
	CalleeCapabilitiesTopicSubscriptionARN string
	PubkeyInvalidationTopicSubscriptionARN string
}

func (e *eventsClient) loadState() (err error) {
	err = e.initState()
	if err != nil {
		return
	}

	err = e.db.View(func(tx *bolt.Tx) (err error) {
		b := tx.Bucket([]byte(boltEventsBucketName))
		value := b.Get([]byte(boltEventsClientStateKey))
		e.state = new(eventsClientState)

		if value == nil {
			return
		}

		err = json.Unmarshal(value, e.state)
		return
	})
	return
}

func (e *eventsClient) initState() (err error) {
	err = e.db.Update(func(tx *bolt.Tx) (err error) {
		_, err = tx.CreateBucketIfNotExists([]byte(boltEventsBucketName))
		return
	})
	return
}

func (e *eventsClient) writeState() (err error) {
	value, err := json.Marshal(e.state)
	if err != nil {
		return
	}

	err = e.db.Update(func(tx *bolt.Tx) (err error) {
		b := tx.Bucket([]byte(boltEventsBucketName))
		err = b.Put([]byte(boltEventsClientStateKey), value)
		return
	})

	return
}

func (e *eventsClient) setupQueue(ctx context.Context) (err error) {
	if e.state.QueueURL == "" {
		err = e.createQueue()
		if err != nil {
			e.log.Debugf("error creating queue: %s", err.Error())
			return
		}

		err = e.getQueueARN()
		if err != nil {
			e.log.Debugf("error getting queueARN: %s", err.Error())
			return
		}

		err = e.giveSNSPermission(ctx)
		if err != nil {
			e.log.Debugf("error giving sns: %s", err.Error())
			return
		}

		err = e.subscribeQueue()
		if err != nil {
			e.log.Debugf("error subscribing queue: %s", err.Error())
			return
		}

		err = e.writeState()
		if err != nil {
			return
		}
	}

	return
}

func (e *eventsClient) recreateQueue(ctx context.Context) (err error) {
	err = e.invalidateQueue()
	if err != nil {
		return
	}

	for {
		err = e.setupQueue(ctx)
		if err != nil {
			e.log.Error("error while recreating queue: " + err.Error())

			t := time.NewTimer(10 * time.Second)
			select {
			case <-ctx.Done():
				e.log.Debug("exiting recreating due to context done")
				return
			case <-t.C:
				continue
			}
		}
	}
}

func (e *eventsClient) invalidateQueue() (err error) {
	e.state.QueueARN = ""
	e.state.QueueURL = ""
	err = e.writeState()
	return
}

func (e *eventsClient) createQueue() (err error) {
	uid := uuid.NewV4().String()
	queueName := e.cfg.ServiceID + "_" + uid

	input := &sqs.CreateQueueInput{
		QueueName: aws.String(queueName),
	}

	output, err := e.sqs.CreateQueue(input)
	if err != nil {
		return
	}

	e.state.QueueURL = aws.StringValue(output.QueueUrl)
	return
}

func (e *eventsClient) getQueueARN() (err error) {
	input := &sqs.GetQueueAttributesInput{
		QueueUrl:       aws.String(e.state.QueueURL),
		AttributeNames: []*string{aws.String("QueueArn")},
	}

	output, err := e.sqs.GetQueueAttributes(input)
	if err != nil {
		return
	}

	e.state.QueueARN = aws.StringValue(output.Attributes["QueueArn"])
	return
}

func (e *eventsClient) giveSNSPermission(ctx context.Context) (err error) {
	topicArns := []string{e.cfg.PubkeyInvalidationTopicARN}
	if e.cfg.CalleeCapabilitiesTopicARN != "" {
		topicArns = append(topicArns, e.cfg.CalleeCapabilitiesTopicARN)
	}

	document := &policy.IAMPolicyDocument{
		Version: policy.DocumentVersion,
		Statement: []policy.IAMPolicyStatement{
			{
				Sid:    "allow callee sns topic",
				Effect: "Allow",
				Principal: map[string][]string{
					"AWS": {"*"},
				},
				Action:   "SQS:SendMessage",
				Resource: e.state.QueueARN,
				Condition: policy.IAMStatementCondition{
					"StringEquals": map[string]interface{}{
						"aws:SourceArn": topicArns,
					},
				},
			},
		},
	}

	str, err := json.Marshal(document)
	if err != nil {
		return
	}

	setParams := &sqs.SetQueueAttributesInput{
		Attributes: map[string]*string{
			"Policy": aws.String(string(str)),
		},
		QueueUrl: aws.String(e.state.QueueURL),
	}

	_, err = e.sqs.SetQueueAttributesWithContext(ctx, setParams)
	return
}

func (e *eventsClient) subscribeQueue() (err error) {
	output, err := e.sns.Subscribe(&sns.SubscribeInput{
		Endpoint: aws.String(e.state.QueueARN),
		Protocol: aws.String("sqs"),
		TopicArn: aws.String(e.cfg.PubkeyInvalidationTopicARN),
	})
	if err != nil {
		e.log.Debugf("error subbing to PubkeyInvalidationTopicARN: ", err.Error())
		return
	}
	e.state.PubkeyInvalidationTopicSubscriptionARN = aws.StringValue(output.SubscriptionArn)

	if e.cfg.CalleeCapabilitiesTopicARN != "" {
		output, err = e.sns.Subscribe(&sns.SubscribeInput{
			Endpoint: aws.String(e.state.QueueARN),
			Protocol: aws.String("sqs"),
			TopicArn: aws.String(e.cfg.CalleeCapabilitiesTopicARN),
		})
		if err != nil {
			e.log.Debugf("error subbing to caps: ", err.Error())
			return
		}
		e.state.CalleeCapabilitiesTopicSubscriptionARN = aws.StringValue(output.SubscriptionArn)
	}
	return
}

// event types
const (
	PubkeyInvalidationEventType        = "pubkey_invalidation"
	InvalidateAllCapabilitiesEventType = "invalidate_all_capabilities"
)

// EventTypeMessageAttributeName is the key of the sqs message attribute map
// that identifies the type of message that was sent
const EventTypeMessageAttributeName = "TWITCH.MALACHAI.EVENTS.EventType"

func (e *eventsClient) updateLoop(ctx context.Context) (err error) {
	err = e.pollForUpdates(ctx)
	if err != nil {
		select {
		case <-ctx.Done():
			return
		default:
		}

		e.log.Error("error while updating loop: " + err.Error())
		e.markQueueDown()

		if queueDoesNotExistError(err) {
			iqErr := e.recreateQueue(ctx)
			if iqErr != nil {
				e.log.Errorf("error while recreating queue: %s", iqErr.Error())
			}
		}
	} else {
		e.markQueueUp()
	}
	return
}

func (e *eventsClient) pollForUpdates(ctx context.Context) (err error) {
	e.log.Debug("running queue update loop")
	input := &sqs.ReceiveMessageInput{
		MaxNumberOfMessages: aws.Int64(sqsBatchSize),
		QueueUrl:            aws.String(e.state.QueueURL),
		WaitTimeSeconds:     aws.Int64(sqsPollTimeout),
	}
	output, err := e.sqs.ReceiveMessageWithContext(ctx, input)
	if err != nil {
		return
	}

	if len(output.Messages) > 0 {
		var f int
		for _, message := range output.Messages {
			var msg snsMessage
			err = json.Unmarshal([]byte(aws.StringValue(message.Body)), &msg)
			av, ok := msg.MessageAttributes[EventTypeMessageAttributeName]
			if !ok {
				// TODO: send message with missing event type to dlq
				e.log.Error("message missing event type")
				continue
			}

			e.log.Debugf("received event: %s", av.Value)

			switch av.Value {
			case PubkeyInvalidationEventType:
				err = e.handlePubkeyEvent(ctx, msg.Message)
				if err != nil {
					if err == ctx.Err() {
						return
					}
					// TODO: send message to dlq
					e.log.Error("error handling pubkey event: " + err.Error())
					continue
				}
			case InvalidateAllCapabilitiesEventType:
				err = e.handleCapabilitiesEvent(ctx, msg.Message)
				if err != nil {
					if err == ctx.Err() {
						return
					}
					// TODO: send message to dlq
					e.log.Error("error handling capabilities event: " + err.Error())
					continue
				}
				f++
				ctx = context.WithValue(ctx, capabilitiesEventHandledContextKey{}, struct{}{})
			default:
				// TODO: send messages with unhandled type to dead letter queue
				e.log.Error("invalid event type: " + av.Value)
				continue
			}

			_, err = e.sqs.DeleteMessageWithContext(ctx, &sqs.DeleteMessageInput{
				QueueUrl:      aws.String(e.state.QueueURL),
				ReceiptHandle: message.ReceiptHandle,
			})
			if err != nil {
				return
			}
		}
	} else {
		e.log.Debug("received no events. polling.")
	}
	return
}

func (e *eventsClient) start(ctx context.Context) (err error) {
	err = e.setupQueue(ctx)
	if err != nil {
		e.log.Debugf("setupQueue failed: %s", err.Error())
		return
	}

	go e.main(ctx)
	return
}

func (e *eventsClient) main(ctx context.Context) {
	for {
		updateErr := e.updateLoop(ctx)
		if updateErr == nil {
			continue
		}

		e.log.Errorf("error updating loop: %s", updateErr.Error())

		timer := time.NewTimer(10 * time.Second)
		select {
		case <-ctx.Done():
			return
		case <-timer.C:
		}
	}
}

// Health represents the health of the events client
type Health struct {
	QueueDownSince time.Time
}

func (e *eventsClient) markQueueDown() {
	e.log.Debug("marking queue as down")
	e.health.QueueDownSince = time.Now().UTC()
}

func (e *eventsClient) markQueueUp() {
	e.log.Debug("marking queue as up")
	e.health.QueueDownSince = time.Time{}
}

func (e *eventsClient) setHealth(h *Health) {
	e.health = h
}

func (e *eventsClient) Health() (h *Health) {
	return e.health
}

func queueDoesNotExistError(err error) bool {
	awsErr, ok := err.(awserr.RequestFailure)
	if !ok {
		return false
	}

	return awsErr.StatusCode() == http.StatusNotFound
}
