package s2s

import (
	"io"
	"net/http"
	"time"

	"github.com/cactus/go-statsd-client/statsd"
	"github.com/sirupsen/logrus"

	"code.justin.tv/sse/malachai/pkg/config"
	"code.justin.tv/sse/malachai/pkg/loggers"
	"code.justin.tv/sse/malachai/pkg/registration"
	"code.justin.tv/sse/malachai/pkg/signature"
	"code.justin.tv/systems/sandstorm/manager"
	"code.justin.tv/systems/sandstorm/queue"
)

const (
	metricNameRequestSigned             = "request.signed"
	metricNameRequestSignatureAttempted = "request.signature.attempted"
	metricNameRequestSignatureSuccess   = "request.signature.success"
	metricNameRequestSignatureFailure   = "request.signature.failure"
)

// RoundTripper implements http.RoundTripper. It signs requests using the
// configured caller service's private key, retrieved from sandstorm.
type RoundTripper struct {
	inner     http.RoundTripper
	signer    *signature.Signer
	sandstorm manager.API
	statter   statsd.Statter
	logger    logrus.FieldLogger
}

// NewRoundTripper returns an http.RoundTripper, which wraps the default http.RoundTripper
// with a request signer
func NewRoundTripper(cfg *CallerConfig, logger logrus.FieldLogger) (rt *RoundTripper, err error) {
	rt, err = NewWithCustomRoundTripper(cfg, http.DefaultTransport, logger)
	return
}

// NewWithCustomRoundTripper wraps the signing RoundTripper with the provided
// http.RoundTripper
func NewWithCustomRoundTripper(cfg *CallerConfig, inner http.RoundTripper, logger logrus.FieldLogger) (rt *RoundTripper, err error) {
	err = cfg.FillDefaults()
	if err != nil {
		return
	}

	logger.Debugf("config options: %#v", cfg)
	logger.Debugf("configuring registrar with: %#v", cfg.RegistrationConfig)
	reg, err := registration.New(cfg.RegistrationConfig, logger)
	if err != nil {
		return
	}

	logger.Debugf("retrieving service registration for caller name '%s'", cfg.CallerName)
	caller, err := reg.Get(cfg.CallerName)
	if err != nil {
		return
	}
	logger.Debugf("service registered is: %#v", caller)

	fhlFields := logrus.Fields{
		loggers.LogFieldServiceID:   caller.ID,
		loggers.LogFieldServiceName: caller.Name,
	}

	inventoryClient, err := newInventoryClient(&inventoryConfig{
		Environment: cfg.Environment,
		RoleArn:     caller.RoleArn,
	})
	if err != nil {
		return
	}

	instanceID := newInstanceID()
	err = inventoryClient.putInventory(&sdkVersionInfo{
		ServiceID:   caller.ID,
		ServiceName: caller.Name,
		InstanceID:  instanceID,
		Version:     sdkVersion,
	})
	if err != nil {
		return
	}

	logger.Debugf("configuring sandstorm manager using caller role '%s', sandstorm role '%s'", caller.RoleArn, caller.SandstormRoleArn)
	sandstorm := manager.New(manager.Config{
		AWSConfig: config.AWSConfig(
			cfg.Region,
			cfg.roleArn,
			caller.RoleArn,
			caller.SandstormRoleArn),
		KeyID:     cfg.SandstormKMSKeyID,
		TableName: cfg.SandstormSecretsTableName,
		Queue: queue.Config{
			TopicArn: cfg.SandstormTopicArn,
		},
		InstanceID: instanceID,
	})

	if !cfg.DisableSecretRotationListener {
		logger.WithFields(fhlFields).Debug("listening for sandstorm updates")
		err = sandstorm.ListenForUpdates()
		if err != nil {
			logger.Error("error starting sandstorm queue listener: " + err.Error())
			return
		}
	}

	statter, err := cfg.statsd(cfg.CallerName)
	if err != nil {
		logger.Errorf("failed to initialize statsd client")
		return
	}

	logger.Debug("configuring roundtripper")
	signingRoundTripper := &RoundTripper{
		inner: inner,
		signer: &signature.Signer{
			CallerID:   caller.ID,
			Method:     defaultSigningMethod,
			PrivateKey: newPrivateKeyStorer(sandstorm, caller.SandstormSecretName),
		},
		sandstorm: sandstorm,
		logger:    logger,
		statter:   statter,
	}

	rt = signingRoundTripper
	return
}

// Close cleans up the sandstorm notification pipeline
func (rt *RoundTripper) Close() (err error) {
	rt.logger.WithField(loggers.LogFieldServiceID, rt.signer.CallerID).Infof("stopped listening for sandstorm updates")
	return rt.sandstorm.StopListeningForUpdates()
}

// RoundTrip implements http.RoundTripper
func (rt *RoundTripper) RoundTrip(req *http.Request) (resp *http.Response, err error) {
	fhlFields := logrus.Fields{
		loggers.LogFieldServiceID: rt.signer.CallerID,
		loggers.LogFieldURLPath:   req.URL.String(),
		loggers.LogFieldHostname:  req.Host,
	}

	startTime := time.Now()
	if req.Body == nil {
		rt.logger.WithFields(fhlFields).Debug("signing an empty body request")

		sErr := rt.statter.Inc(metricNameRequestSignatureAttempted, 1, 1.0)
		err = rt.signer.SignRequest(req)
		if err != nil {
			rt.logger.WithFields(fhlFields).Error("failed to sign request: " + err.Error())
			sErr := rt.statter.Inc(metricNameRequestSignatureFailure, 1, 1.0)
			if sErr != nil {
				rt.logger.WithFields(fhlFields).Error("failed to send stat:" + sErr.Error())
			}
			return
		}
		sErr = rt.statter.Inc(metricNameRequestSignatureSuccess, 1, 1.0)
		if sErr != nil {
			rt.logger.WithFields(fhlFields).Error("failed to send stat: " + sErr.Error())
		}
		sErr = rt.statter.TimingDuration(metricNameRequestSigned, time.Now().Sub(startTime), 1.0)
		if sErr != nil {
			rt.logger.WithFields(fhlFields).Error("failed to send stat: " + sErr.Error())
		}
		resp, err = rt.inner.RoundTrip(req)
		if err != nil {
			rt.logger.WithFields(fhlFields).Error("failed to send request: " + err.Error())
		}
		return
	}

	seeker, err := WrapRequestBodyWithSeeker(req)
	if err != nil {
		rt.logger.WithFields(fhlFields).Error("failure wrapping request body: " + err.Error())
		return
	}

	rt.logger.WithFields(fhlFields).Debugf("signing a request with body, content-length: %d", req.ContentLength)
	err = rt.signer.SignRequest(req)
	if err != nil {
		rt.logger.WithFields(fhlFields).Error("failed to sign a request, err: " + err.Error())
		sErr := rt.statter.Inc(metricNameRequestSignatureFailure, 1, 1.0)
		if sErr != nil {
			rt.logger.WithFields(fhlFields).Error("failed to send stat: " + sErr.Error())
		}
		return
	}

	sErr := rt.statter.Inc(metricNameRequestSignatureSuccess, 1, 1.0)
	if sErr != nil {
		rt.logger.WithFields(fhlFields).Error("failed to send stat: " + sErr.Error())
	}

	sErr = rt.statter.TimingDuration(metricNameRequestSigned, time.Now().Sub(startTime), 1.0)
	if sErr != nil {
		rt.logger.WithFields(fhlFields).Error("failed to send stat: " + sErr.Error())
	}
	_, err = seeker.Seek(0, io.SeekStart)
	if err != nil {
		rt.logger.WithFields(fhlFields).Error("failed to build signed request: " + err.Error())
		return
	}
	resp, err = rt.inner.RoundTrip(req)
	if err != nil {
		rt.logger.WithFields(fhlFields).Error("failed to send request: " + err.Error())
	}
	return
}
