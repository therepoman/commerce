package jwtvalidation

import (
	"errors"
	"strings"
)

// Errors
var (
	ErrInvalidSigningEntityString = errors.New("signing entities should be of the form CALLEE:FINGERPRINT")
)

// SigningEntity represents an entity who signed a request
type SigningEntity struct {
	Caller      string `json:"caller"`
	Fingerprint string `json:"fingerprint"`
}

// EncodeToString converts the signing entity to string for use in jwt header
func (s *SigningEntity) EncodeToString() (encoded string) {
	return s.Caller + ":" + s.Fingerprint
}

// DecodeSigningEntityString decodes a SigningEntity from string
func DecodeSigningEntityString(encoded string) (s *SigningEntity, err error) {
	delimIndex := strings.Index(encoded, ":")
	if delimIndex < 0 {
		err = ErrInvalidSigningEntityString
		return
	}

	s = new(SigningEntity)
	s.Caller = encoded[:delimIndex]
	s.Fingerprint = encoded[delimIndex+1:]
	return
}
