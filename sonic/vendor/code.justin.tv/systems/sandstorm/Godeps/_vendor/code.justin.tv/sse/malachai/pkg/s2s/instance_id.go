package s2s

import uuid "github.com/satori/go.uuid"

func newInstanceID() string {
	return uuid.NewV4().String()
}
