package signature

import (
	"bytes"
	"encoding/base64"
	"errors"
	"fmt"
	"net/http"
	"time"

	"code.justin.tv/sse/malachai/pkg/jwtvalidation"
	"code.justin.tv/sse/malachai/pkg/keystore"

	"github.com/SermoDigital/jose/jws"
	"github.com/SermoDigital/jose/jwt"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/sirupsen/logrus"
)

const (
	metricNameRequestValidated                    = "request.validated"
	metricNameRequestValidationAttempted          = "request.validation.attempted"
	metricNameRequestValidationSuccess            = "request.validation.success"
	metricNameRequestValidationFailure            = "request.validation.failure"
	metricNameRequestValidationFailureExpired     = "request.validation.failure.expired"
	metricNameRequestValidationFailureNBF         = "request.validation.failure.nbf"
	metricNameRequestValidationFailureNoSignature = "request.validation.failure.no_signature"
)

// Validation errors
var (
	ErrCanonicalHashMismatch  = errors.New("sent canonical hash does not match request")
	ErrMissingSignatureHeader = errors.New("missing signature header")
)

// UnauthorizedRequestError is returned when the is invalid JWT
type UnauthorizedRequestError struct{}

// StatusCode returns the HTTP status code that should be served
func (te *UnauthorizedRequestError) StatusCode() int {
	return http.StatusUnauthorized
}

// TokenNotYetValidError is returned when a token is sent before its nbf time
type TokenNotYetValidError struct {
	UnauthorizedRequestError
	CheckTime time.Time
	NbfTime   time.Time
	NbfLeeway time.Duration
}

func (e *TokenNotYetValidError) Error() string {
	return fmt.Sprintf(
		"token is not yet valid: check time<%d> nbf<%d with %s leeway>",
		e.CheckTime.Unix(), e.NbfTime.Unix(), (e.NbfLeeway / time.Second).String())
}

// TokenExpiredError is returned when a token is sent before its nbf time
type TokenExpiredError struct {
	UnauthorizedRequestError
	CheckTime time.Time
	ExpTime   time.Time
	ExpLeeway time.Duration
}

func (e *TokenExpiredError) Error() string {
	return fmt.Sprintf(
		"token is expired: check time<%d> exp<%d with %s leeway>",
		e.CheckTime.Unix(), e.ExpTime.Unix(), (e.ExpLeeway / time.Second).String())
}

var requiredClaims = []string{
	"exp",
	"jti",
	CanonicalRequestHashClaim,
	CanonicalRequestHeadersClaim,
	CanonicalRequestHashAlgClaim,
}

// RequestValidatorConfig configures the RequestValidator
type RequestValidatorConfig struct {
	ExpLeeway time.Duration
	NbfLeeway time.Duration
}

// RequestValidator validates requests
type RequestValidator interface {
	ValidateRequest(r *http.Request) (caller *jwtvalidation.SigningEntity, err error)
	ValidateRequestWithHashedBody(r *http.Request, hashedBody []byte) (caller *jwtvalidation.SigningEntity, err error)
}

// RequestJWTValidator validates signatures
type RequestJWTValidator struct {
	*jwtvalidation.BaseValidator
	cfg      RequestValidatorConfig
	keystore keystore.Storer
	logger   logrus.FieldLogger
	statter  statsd.Statter
}

// NewRequestJWTValidator configures a new request validator
func NewRequestJWTValidator(cfg *RequestValidatorConfig, pubkeystore keystore.Storer, logger logrus.FieldLogger, statter statsd.Statter) (validator *RequestJWTValidator) {
	if cfg == nil {
		cfg = new(RequestValidatorConfig)
	}

	return &RequestJWTValidator{
		BaseValidator: &jwtvalidation.BaseValidator{
			Now: func() time.Time {
				return time.Now().UTC()
			},
			RequiredClaims: requiredClaims,
		},
		cfg:      *cfg,
		keystore: pubkeystore,
		logger:   logger,
		statter:  statter,
	}
}

// ValidateRequestWithHashedBody validates a request signature using a hash
// of the body of the body itself
func (s *RequestJWTValidator) ValidateRequestWithHashedBody(r *http.Request, hashedBody []byte) (caller *jwtvalidation.SigningEntity, err error) {
	sig := []byte(r.Header.Get(SignatureHeader))
	if len(sig) == 0 {
		err = ErrMissingSignatureHeader
		sErr := s.statter.Inc(metricNameRequestValidationFailureNoSignature, 1, 1.0)
		if sErr != nil {
			//no op for now
		}
		return
	}
	sErr := s.statter.Inc(metricNameRequestValidationAttempted, 1, 1.0)
	if sErr != nil {
		s.logger.Errorf("failed to update %s stats, err: %s", metricNameRequestValidationAttempted, err.Error())
	}
	startTime := time.Now()
	requestJWT, err := jws.ParseJWT(sig)
	if err != nil {
		return
	}

	kid, err := jwtvalidation.GetKID(requestJWT)
	if err != nil {
		return
	}

	claimedCaller, err := jwtvalidation.DecodeSigningEntityString(kid)
	if err != nil {
		return
	}

	publicKey, err := s.keystore.GetRSAPublicKey(claimedCaller)
	if err != nil {
		return
	}

	validator := jws.NewValidator(
		jws.Claims{
			CanonicalRequestHashAlgClaim: CanonicalRequestHashAlg,
		},
		s.cfg.ExpLeeway,
		s.cfg.NbfLeeway,
		s.getJWTClaimsValidator(r, hashedBody),
	)

	alg, err := jwtvalidation.GetAlg(requestJWT)
	if err != nil {
		return
	}
	var validationTime time.Duration
	var metricName string
	err = requestJWT.Validate(publicKey, jws.GetSigningMethod(alg), validator)
	if err != nil {
		validationTime = time.Now().Sub(startTime)
		if err == jwt.ErrTokenNotYetValid {
			metricName = metricNameRequestValidationFailureNBF
			nbf, _ := requestJWT.Claims().NotBefore()
			err = &TokenNotYetValidError{
				CheckTime: time.Now(),
				NbfTime:   nbf,
				NbfLeeway: s.cfg.NbfLeeway,
			}
		} else if err == jwt.ErrTokenIsExpired {
			metricName = metricNameRequestValidationFailureExpired
			exp, _ := requestJWT.Claims().Expiration()
			err = &TokenExpiredError{
				CheckTime: time.Now(),
				ExpTime:   exp,
				ExpLeeway: s.cfg.ExpLeeway,
			}
		} else {
			metricName = metricNameRequestValidationFailure

		}
		sErr := s.statter.Inc(metricName, 1, 1.0)
		if sErr != nil {
			s.logger.Errorf("failed to update %s stats, err: %s", metricName, err.Error())
		}
		//No Sampling for now.
		sErr = s.statter.TimingDuration(metricNameRequestValidated, validationTime, 1.0)
		if sErr != nil {
			s.logger.Errorf("failed to update %s stats, err: %s", metricNameRequestValidated, err.Error())
		}
		return
	}

	validationTime = time.Now().Sub(startTime)
	//No Sampling for now.
	sErr = s.statter.TimingDuration(metricNameRequestValidated, validationTime, 1.0)
	if sErr != nil {
		s.logger.Errorf("failed to update %s stats, err: %s", metricNameRequestValidated, err.Error())
	}
	sErr = s.statter.Inc(metricNameRequestValidationSuccess, 1, 1.0)
	if sErr != nil {
		s.logger.Errorf("failed to update %s stats, err: %s", metricNameRequestValidationSuccess, err.Error())
	}

	caller = claimedCaller
	return
}

// ValidateRequest returns an error if request signature is not valid
func (s *RequestJWTValidator) ValidateRequest(r *http.Request) (caller *jwtvalidation.SigningEntity, err error) {
	return s.ValidateRequestWithHashedBody(r, nil)
}

func (s *RequestJWTValidator) getJWTClaimsValidator(r *http.Request, hashedBody []byte) func(c jws.Claims) error {
	return func(c jws.Claims) (err error) {
		err = s.ValidateRequiredClaims(c)
		if err != nil {
			return
		}

		err = s.validateHash(r, hashedBody, c)
		return
	}
}

func (s *RequestJWTValidator) validateHash(r *http.Request, hashedBody []byte, c jws.Claims) (err error) {
	claimedCanonicalHashEncoded, ok := c[CanonicalRequestHashClaim].(string)
	if !ok {
		err = jwtvalidation.ClaimWrongType{
			FieldName: CanonicalRequestHashClaim,
			TypeName:  "string",
		}
		return
	}

	claimedCanonicalHash, err := base64.StdEncoding.DecodeString(claimedCanonicalHashEncoded)
	if err != nil {
		return
	}

	var headersList []string
	switch t := c[CanonicalRequestHeadersClaim].(type) {
	default:
		// this catches everything else
		err = jwtvalidation.ClaimWrongType{
			FieldName: CanonicalRequestHeadersClaim,
			TypeName:  "[]string",
		}
		return
	case []interface{}:
		// when len(list) > 0 with strings
		headersList = make([]string, len(t))
		for i, v := range t {
			var ok bool
			headersList[i], ok = v.(string)
			if !ok {
				err = jwtvalidation.ClaimWrongType{
					FieldName: CanonicalRequestHeadersClaim,
					TypeName:  "[]string",
				}
				return
			}
		}
	case []string:
		// when len(list) == 0
	}

	requestHash, err := generateCanonicalRequestHash(r, hashedBody, headersList)
	if err != nil {
		return
	}

	if bytes.Compare(requestHash, claimedCanonicalHash) != 0 {
		err = ErrCanonicalHashMismatch
	}
	return
}
