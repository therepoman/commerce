package s2s

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/boltdb/bolt"
	"github.com/sirupsen/logrus"

	"code.justin.tv/sse/malachai/pkg/keystore"
	"code.justin.tv/sse/malachai/pkg/loggers"
	"code.justin.tv/sse/malachai/pkg/signature"
)

const boltDBOpenTimeout = 2 * time.Second

type middlewareFunc func(http.Handler) http.Handler

func chainMiddlewares(inner http.Handler, mws ...middlewareFunc) (handler http.Handler) {
	if len(mws) == 0 {
		return inner
	}
	return mws[0](chainMiddlewares(inner, mws[1:len(mws)]...))
}

func middlewareHandlerFunc(mws ...middlewareFunc) (handler func(http.Handler) http.Handler, err error) {
	handler = func(inner http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			chainMiddlewares(inner, mws...).ServeHTTP(w, r)
		})
	}
	return
}

// NewRequestValidator returns an http.middleware that validates a request
// signature and sets the "s2s-caller-id" key in the request context.
func NewRequestValidator(ctx context.Context, cfg *CalleeConfig) (fn func(http.Handler) http.Handler, err error) {
	err = cfg.FillDefaults()
	if err != nil {
		return
	}

	db, err := bolt.Open(cfg.BoltDBFileName, 0640, &bolt.Options{Timeout: boltDBOpenTimeout})
	if err != nil {
		return
	}
	defer func() {
		if err != nil {
			dbCloseErr := db.Close()
			if err == nil {
				// we care more about the actual err
				err = dbCloseErr
			}
		}
	}()

	fhl, err := loggers.NewFirehoseLogger(cfg.FirehoseConfig, db)
	if err != nil {
		return
	}
	defer func() {
		if err != nil {
			fhl.Close()
		}
	}()

	fhlFields := logrus.Fields{
		loggers.LogFieldServiceName: cfg.CalleeName,
		loggers.LogFieldCalleeName:  cfg.CalleeName,
	}
	fhl.WithFields(fhlFields).Debugf("setting up request validator with %#v", cfg)

	eventsClient, err := newEventsClient(cfg.eventsConfig, db, fhl)
	if err != nil {
		return
	}
	err = eventsClient.start(ctx)
	if err != nil {
		return
	}

	mws, err := newRequestValidator(ctx, cfg, db, fhl, eventsClient)
	if err != nil {
		fhl.WithFields(fhlFields).Errorf("error initializing new request validator: %s", err.Error())
	}
	fn, err = middlewareHandlerFunc(mws...)
	return
}

func newRequestValidator(ctx context.Context, cfg *CalleeConfig, db *bolt.DB, logger logrus.FieldLogger, events *eventsClient) (mws []middlewareFunc, err error) {
	logger = logger.WithFields(logrus.Fields{
		loggers.LogFieldServiceName: cfg.CalleeName,
		loggers.LogFieldCalleeName:  cfg.CalleeName,
	})
	pubkeyStore, err := keystore.New(cfg.KeystoreConfig, db)
	if err != nil {
		return
	}

	go startPubkeyInvalidationManager(ctx, pubkeyStore, events.pubkeys, logger)

	statter, err := cfg.statsd()
	if err != nil {
		logger.Errorf("failed to initialize statsd, err: %s", err.Error())
	}
	instanceID := newInstanceID()
	inventoryClient, err := newInventoryClient(cfg.inventoryConfig)
	if err != nil {
		return
	}

	err = inventoryClient.putInventory(&sdkVersionInfo{
		ServiceID:   cfg.calleeID,
		ServiceName: cfg.CalleeName,
		InstanceID:  instanceID,
		Version:     sdkVersion,
	})
	if err != nil {
		return
	}

	validator := signature.NewRequestJWTValidator(cfg.RequestValidatorConfig, pubkeyStore, logger, statter)

	hcm := healthCheckMiddleware(events, cfg, logger)
	rv := newRequestVerifier(validator, logger)

	mws = append(mws, hcm, rv)
	return
}

// NewCapabilitiesInjector chains the request validator and capabilities
// injector middlewares
func NewCapabilitiesInjector(ctx context.Context, cfg *CalleeConfig) (fn func(http.Handler) http.Handler, err error) {
	mws, err := newCapabilitiesInjectorMiddleware(ctx, cfg)
	if err != nil {
		return
	}
	fn, err = middlewareHandlerFunc(mws...)
	return
}

//NewCapabilitiesAuthorizer returns a http handler with default capabilities authorizer
func NewCapabilitiesAuthorizer(ctx context.Context, cfg *CalleeConfig) (fn func(http.Handler) http.Handler, err error) {
	mws, err := newCapabilitiesInjectorMiddleware(ctx, cfg)
	if err != nil {
		return
	}
	mws = append(mws, newCapabilitiesAuthorizer())
	fn, err = middlewareHandlerFunc(mws...)
	return
}

// returns capablities middleware with authorizer if param withCapAuthorizer is set, middleware with capabilities injector
// is returned otherwise.
func newCapabilitiesInjectorMiddleware(ctx context.Context, cfg *CalleeConfig) (mws []middlewareFunc, err error) {
	err = cfg.FillDefaults()
	if err != nil {
		return
	}

	db, err := bolt.Open(cfg.BoltDBFileName, 0640, &bolt.Options{Timeout: boltDBOpenTimeout})
	if err != nil {
		return
	}
	defer func() {
		if err != nil {
			dbCloseErr := db.Close()
			if err == nil {
				// we care more about the actual err
				err = dbCloseErr
			}
		}
	}()

	fhl, err := loggers.NewFirehoseLogger(cfg.FirehoseConfig, db)
	if err != nil {
		return
	}
	defer func() {
		if err != nil {
			fhl.Close()
		}
	}()

	eventsClient, err := newEventsClient(cfg.eventsConfig, db, fhl)
	if err != nil {
		return
	}

	capClient, err := newCalleeCapabilitiesClient(cfg, db, fhl, eventsClient.capabilities)
	if err != nil {
		return
	}

	err = eventsClient.start(ctx)
	if err != nil {
		return
	}

	go capClient.start(ctx)

	ci := newCapabilitiesInjector(capClient)

	mws, err = newRequestValidator(ctx, cfg, db, fhl, eventsClient)
	if err != nil {
		return
	}
	mws = append(mws, ci)
	return
}

// healthCheckMiddleware takes the events client and serves a 4XX 5XX errors
// based on the health of the client. for example, sometimes we want to reject
// all requests if we can't update our cache from SQS
func healthCheckMiddleware(e *eventsClient, cfg *CalleeConfig, logger logrus.FieldLogger) (fn func(http.Handler) http.Handler) {
	logger = logger.WithFields(logrus.Fields{
		loggers.LogFieldServiceName: cfg.CalleeName,
		loggers.LogFieldCalleeName:  cfg.CalleeName,
	})

	fn = func(inner http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			now := time.Now()
			down := e.Health().QueueDownSince
			if !down.IsZero() && now.Sub(down) > cfg.QueueUnavailableRejectThreshold {
				msg := fmt.Sprintf(
					"request rejected by QueueUnavailablePolicy 'reject': unable to update cache since %s",
					down.String(),
				)
				logger.Warn(msg)
				if cfg.QueueUnavailablePolicy == QueueUnavailablePolicyReject {
					w.WriteHeader(http.StatusServiceUnavailable)
					_, err := w.Write([]byte(msg))
					if err != nil {
						logger.Errorf("error while writing response: %s", err.Error())
					}
					return
				}
			}
			inner.ServeHTTP(w, r)
		})
	}
	return
}
