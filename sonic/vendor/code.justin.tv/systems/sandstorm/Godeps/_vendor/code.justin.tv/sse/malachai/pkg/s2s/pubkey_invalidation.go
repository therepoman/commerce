package s2s

import (
	"context"

	"github.com/sirupsen/logrus"

	"code.justin.tv/sse/malachai/pkg/jwtvalidation"
	"code.justin.tv/sse/malachai/pkg/keystore"
)

func startPubkeyInvalidationManager(ctx context.Context, store keystore.Storer, invalidations chan *jwtvalidation.SigningEntity, log logrus.FieldLogger) {
	for {
		select {
		case <-ctx.Done():
			log.Debug("pubkey invalidation manager context cancelled")
			return
		case entity := <-invalidations:
			err := store.InvalidateRSAPublicKey(entity)
			if err != nil {
				log.Errorf("InvalidateRSAPublicKey: %s", err.Error())
			}
		}
	}
}
