package gql

import (
	"encoding/base64"
	"errors"
	"strings"

	graphql "github.com/neelance/graphql-go"
)

const gqlIDDelim = ":"

// NilID is an empty graphql id
const NilID graphql.ID = ""

// Errors
var (
	ErrInvalidGQLID = errors.New("invalid gql id")
)

// Time implements the gql schema
type Time string

// PageInfo implements relay
type PageInfo struct {
	StartCursorValue *graphql.ID
	EndCursorValue   *graphql.ID
	HasNextPageValue bool
}

// StartCursor implements relay
func (p *PageInfo) StartCursor() *graphql.ID {
	return p.StartCursorValue
}

// EndCursor implements relay
func (p *PageInfo) EndCursor() *graphql.ID {
	return p.EndCursorValue
}

// HasNextPage implements relay
func (p *PageInfo) HasNextPage() bool {
	return p.HasNextPageValue
}

// MarshalGQLKey converts list of composite keys into a graphql ID
func MarshalGQLKey(keys ...string) (id graphql.ID) {
	bs := []byte(strings.Join(keys, gqlIDDelim))
	id = graphql.ID(base64.RawURLEncoding.EncodeToString(bs))
	return
}

// UnmarshalGQLKey converts a graphql to a list of composite keys
func UnmarshalGQLKey(id graphql.ID, length int) (keys []string, err error) {
	raw, err := base64.RawURLEncoding.DecodeString(string(id))
	if err != nil {
		return
	}
	keys = strings.SplitN(string(raw), gqlIDDelim, length)
	if len(keys) != length {
		err = ErrInvalidGQLID
		return
	}
	return
}
