package ccg

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"
)

const (
	_ = iota
	// SevCritical maps to integer value 1
	SevCritical
	// SevError maps to integer value 2
	SevError
	// SevWarning maps to integer value 3
	SevWarning
	// SevNotification maps to integer value 3
	SevNotification
	// SevInfo maps to integer value 5
	SevInfo
)

var debug = false

// Client represents the Changelog client configuration.  One or both
// of the fields can be left empty and these will be filled in with
// default values in client.SendEvent
type Client struct {
	// Endpoint is the full URL to the Changelog server.
	// If left nil, this will be replaced with the default endpoint
	Endpoint string

	// HTTPClient is a pointer to a http.Client. If left nil, this
	// will be replaced with http.DefaultClient
	HTTPClient *http.Client
}

// timestampedEvent is an Event that has a Timestamp added and
// Criticality as int. This is the struct used for marshalling to
// JSON.
type timestampedEvent struct {
	AdditionalData string `json:"additional_data"`
	Category       string `json:"category"`
	Command        string `json:"command"`
	Criticality    int    `json:"criticality"`
	Description    string `json:"description"`
	Source         string `json:"source"`
	Target         string `json:"target"`
	Timestamp      int64  `json:"unix_timestamp"`
	Username       string `json:"username"`
}

// Event is an event to send to the Changelog server. The current Unix
// timestamp will be added.
type Event struct {
	// Additional event data
	AdditionalData string

	// Event category
	Category string

	// The command that was executed
	Command string

	// Criticality of the event. This can either be an integer
	// (preferably from constants SevCritical, SevError,
	// SevWarning, SevNotification or SevInfo) or a string. If a
	// string is supplied, this will be converted with
	// SeverityFromString.
	Criticality interface{}

	// Description of the event.
	Description string

	// The hostname of the machine that issued the command
	Source string

	// The name of the machine (or cluster, or system) being targeted
	Target string

	// The username of the user who issued the command
	Username string
}

// SeverityFromString converts a severity string (critical, error,
// warning, notification, info) into an integer expected by the
// changelog server.
//
// Defaults to SevInfo
func SeverityFromString(severity string) int {
	switch strings.ToLower(severity) {
	case "critical":
		return SevCritical
	case "error":
		return SevError
	case "warning":
		return SevWarning
	case "notification":
		return SevNotification
	case "info":
		return SevInfo
	default:
		return SevInfo
	}
}

func defaultClient() *Client {
	client := &Client{
		Endpoint:   "https://changelog.internal.justin.tv/api/events",
		HTTPClient: http.DefaultClient,
	}
	return client
}

func (e *Event) eventToTimestamped() *timestampedEvent {
	out := &timestampedEvent{
		AdditionalData: e.AdditionalData,
		Category:       e.Category,
		Command:        e.Command,
		Description:    e.Description,
		Source:         e.Source,
		Target:         e.Target,
		Timestamp:      time.Now().Unix(),
		Username:       e.Username,
	}
	sevint, ok := e.Criticality.(int)
	if ok {
		out.Criticality = sevint
	}
	sevstr, ok := e.Criticality.(string)
	if ok {
		out.Criticality = SeverityFromString(sevstr)
	}

	return out
}

// Send the event to the server.
// If not supplied, the Endpoint and/or HTTPClient will be replaced with defaults.
//
// In addition the event will be timestamped with the current unix
// timestamp (seconds since epoch)
func (c *Client) SendEvent(event *Event) error {
	tse := event.eventToTimestamped()

	jsonBody, err := json.Marshal(tse)
	if err != nil {
		return err
	}
	if debug {
		log.Printf("[DEBUG] Struct: %+v\n", tse)
		log.Printf("[DEBUG] JSON:   %s\n", jsonBody)
	}
	defClient := defaultClient()
	if c.Endpoint == "" {
		c.Endpoint = defClient.Endpoint
	}
	if c.HTTPClient == nil {
		c.HTTPClient = defClient.HTTPClient
	}

	req, err := http.NewRequest("POST", c.Endpoint, bytes.NewBuffer(jsonBody))
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("User-Agent", "Twitch Changelog Go Client")
	if debug {
		log.Printf("[DEBUG] About to call %s to %s\n", req.Method, req.URL)
	}
	resp, err := c.HTTPClient.Do(req)

	if err != nil {
		return err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	if debug {
		log.Printf("[DEBUG] Status code: %d, Body:\n%s\n", resp.StatusCode, string(body))
	}
	switch resp.StatusCode {
	case 200, 201:
		break
	default:
		return fmt.Errorf("Server error %d: %s", resp.StatusCode, string(body))
	}

	return nil
}
