package keystore

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/boltdb/bolt"

	"code.justin.tv/sse/malachai/pkg/config"
	"code.justin.tv/sse/malachai/pkg/discovery"
	"code.justin.tv/sse/malachai/pkg/jwtvalidation"
)

// Store of keys
type Store struct {
	dynamo discovery.Discoverer
	bolt   *boltReader

	cfg *Config
}

// Storer of keys
type Storer interface {
	InvalidateRSAPublicKey(caller *jwtvalidation.SigningEntity) error
	GetRSAPublicKey(caller *jwtvalidation.SigningEntity) (*rsa.PublicKey, error)
}

// New creates a new Storer
func New(cfg *Config, db *bolt.DB) (store *Store, err error) {
	err = cfg.FillDefaults()
	if err != nil {
		return
	}

	boltdb, err := newBoltReader(db)
	if err != nil {
		return
	}

	store = &Store{
		dynamo: &discovery.Client{
			DB:        dynamodb.New(session.New(config.AWSConfig(cfg.Region, cfg.RoleArn))),
			TableName: cfg.TableName,
		},
		bolt: boltdb,
		cfg:  cfg,
	}
	return
}

// File Errors
var (
	ErrKeystoreHasInvalidPubkey = errors.New("keystore returned invalid pubkey")
	ErrInvalidPubkey            = errors.New("invalid public key loaded")
)

// ParseRSAPubkey converts bytes into an *rsa.PublicKey
func ParseRSAPubkey(bs []byte) (publicKey *rsa.PublicKey, err error) {
	pkixEncoded, _ := pem.Decode(bs)
	if pkixEncoded == nil {
		err = ErrInvalidPubkey
		return
	}

	i, err := x509.ParsePKIXPublicKey(pkixEncoded.Bytes)
	if err != nil {
		err = ErrInvalidPubkey
		return
	}

	var ok bool
	publicKey, ok = i.(*rsa.PublicKey)
	if !ok {
		err = ErrKeystoreHasInvalidPubkey
		return
	}
	return
}

// GetRSAPublicKey returns an rsa.PublicKey from store
func (s *Store) GetRSAPublicKey(caller *jwtvalidation.SigningEntity) (publicKey *rsa.PublicKey, err error) {
	bs, err := s.get(caller)
	if err != nil {
		return
	}

	publicKey, err = ParseRSAPubkey(bs)
	return
}

// Get retrieves a key from bolt. if the key does not exist, pulls from dynamo and
// fills into bolt.
func (s *Store) get(caller *jwtvalidation.SigningEntity) (value []byte, err error) {
	value, err = s.bolt.Get(caller)
	if err != discovery.ErrPubkeyNotFound {
		return
	}

	value, err = s.dynamo.Get(caller)
	if err != nil {
		return
	}

	err = s.bolt.Put(caller, value)
	if err != nil {
		return
	}
	return
}

// InvalidateRSAPublicKey removes a pubkey from boltdb
func (s *Store) InvalidateRSAPublicKey(caller *jwtvalidation.SigningEntity) (err error) {
	return s.bolt.Delete(caller)
}
