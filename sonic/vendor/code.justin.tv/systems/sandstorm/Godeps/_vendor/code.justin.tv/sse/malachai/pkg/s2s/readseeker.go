package s2s

import (
	"bytes"
	"io"
	"io/ioutil"
	"net/http"
)

// WrapRequestBodyWithSeeker copies the request body into a buffer and creates
// a seekable reader. It closes the original request body.
func WrapRequestBodyWithSeeker(r *http.Request) (seeker io.Seeker, err error) {
	bs, err := ioutil.ReadAll(r.Body)
	defer func() {
		closeErr := r.Body.Close()
		if err == nil {
			err = closeErr
		}
	}()
	if err != nil {
		return
	}
	br := bytes.NewReader(bs)

	r.Body = ioutil.NopCloser(br)

	seeker = br
	return
}
