package bininfo

import "code.justin.tv/common/golibs/pkgpath"

var svcname string

func init() {
	svcname, _ = pkgpath.Main()
}

// SvcName returns the qualified name for the binary which is
// currently running
func SvcName() string {
	return svcname
}
