package apiserver

import (
	"encoding/json"
	"net/http"
	"strings"

	"github.com/sirupsen/logrus"
)

func jsonResponse(response interface{}, w http.ResponseWriter) error {
	return jsonResponseStatus(response, w, http.StatusOK)
}

func jsonResponseStatus(response interface{}, w http.ResponseWriter, statusCode int) error {
	w.Header().Set("Content-Type", contentType)
	w.WriteHeader(statusCode)
	encoder := json.NewEncoder(w)
	return encoder.Encode(response)
}

func logEntry(logger logrus.FieldLogger, r *http.Request) (entry *logrus.Entry) {
	entry = logger.WithFields(logrus.Fields{
		"method": r.Method,
		"path":   r.URL.Path,
	})

	user, err := getGuardianUser(r)
	if err == nil && user != nil {
		entry = entry.WithField("user", user.CN)
	}

	return
}

// cleanInputString cleans an input string of common whitespace / ctrl
// characters. currently, this is only \u0000 which is commonly added by copying
// and pasting from amazon workspaces
func cleanInputString(dirty string) (cleaned string) {
	cleaned = strings.Trim(dirty, "\u0000")
	return
}

// cleanInputStrings cleans all strings in a dirty slice and filters empty
// inputs
func cleanInputStrings(dirty []string) (cleaned []string) {
	cleaned = make([]string, 0, len(dirty))
	for _, dirtyString := range dirty {
		cleanedString := cleanInputString(dirtyString)
		if len(cleanedString) == 0 {
			continue
		}
		cleaned = append(cleaned, cleanedString)
	}
	return
}
