package apiserver

import (
	"net/http"

	"github.com/sirupsen/logrus"

	"code.justin.tv/systems/changelog-clients/go/ccg"
	"code.justin.tv/systems/sandstorm/internal/stat"
)

type changelogClient struct {
	*ccg.Client
	logger   logrus.FieldLogger
	statter  stat.Statter
	category string
}

const defaultChangelogCategory = "sandstorm-api-server"

// ChangelogConfig holds configuration options for changelog
type ChangelogConfig struct {
	Enabled        bool   `hcl:"enabled"`
	Category       string `hcl:"category"`
	Host           string `hcl:"host"`
	LogDir         string `hcl:"log_dir"`
	BoltDBFilePath string `hcl:"bolt_db_file_path"`

	CallerName string `hcl:"caller_name"`
}

func configureChangelog(config ChangelogConfig, logger logrus.FieldLogger, statter stat.Statter) (client *changelogClient, err error) {
	if !config.Enabled {
		logger.Info("changelog disabled")
		return
	}

	authedClient, err := ccg.NewS2SClient(&ccg.S2SConfig{
		CallerName: config.CallerName,
		Host:       config.Host,
	}, logger)
	if err != nil {
		return
	}

	client = &changelogClient{
		Client:   authedClient,
		logger:   logger,
		statter:  statter,
		category: defaultChangelogCategory,
	}
	logger.Debug("changelog enabled")

	if config.Category != "" {
		client.category = config.Category
	}
	return
}

func (c *changelogClient) logInfoChangeEvent(r *http.Request, target string, description string, username string, command string, additionalData string) {
	c.logChangeEvent(r, target, description, username, command, additionalData, ccg.SevInfo)
}

func (c *changelogClient) logWarnChangeEvent(r *http.Request, target string, description string, username string, command string, additionalData string) {
	c.logChangeEvent(r, target, description, username, command, additionalData, ccg.SevWarning)
}

func (c *changelogClient) logChangeEvent(r *http.Request, target string, description string, username string, command string, additionalData string, severity int) {
	if c.Client == nil {
		return
	}

	changeEvent := &ccg.Event{
		Category:       c.category,
		Criticality:    severity,
		Target:         target,
		Description:    description,
		Username:       username,
		Command:        command,
		AdditionalData: additionalData,
	}

	err := c.statter.WithCloudWatchMetrics("changelog.sendevent", func() (err error) {
		return c.SendEvent(changeEvent)
	})

	if err != nil {
		logEntry(c.logger, r).Errorf("couldn't send changelog entry: %s", err)
	}
}
