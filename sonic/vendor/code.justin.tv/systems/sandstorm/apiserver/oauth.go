package apiserver

import (
	"net/http"
	"strings"

	"code.justin.tv/systems/guardian/guardian"
	"code.justin.tv/systems/sandstorm/manager"
	"golang.org/x/oauth2"
)

var callbackState = `{"blueprint": "sandstorm"}`

var defaultOAuthConfig = OAuthConfig{
	AuthURL:             "https://guardian.prod.us-west2.twitch.tv/authorize",
	TokenURL:            "https://guardian.prod.us-west2.twitch.tv/oauth2/token",
	CheckTokenURL:       "https://guardian.prod.us-west2.twitch.tv/oauth2/check_token",
	Scopes:              []string{},
	CrossEnvAdminGroups: []string{"team-security"},
}

// OAuthConfig stores the client id and secret along with
// authorization and token URLs and OAuth scopes.
type OAuthConfig struct {
	Secret              string `hcl:"secret"`
	Scopes              []string
	ClientID            string              `hcl:"client_id"`
	AuthURL             string              `hcl:"auth_url"`
	TokenURL            string              `hcl:"token_url"`
	AdminGroups         []string            `hcl:"admin_groups"`
	adminSet            map[string]struct{} //set created from AdminGroups
	CheckTokenURL       string              `hcl:"checktoken_url"`
	CrossEnvAdminGroups []string            `hcl:"cross_env_admin_groups"`
}

// AuthorizeRedirect Handles redirection when no access token
func (oac OAuthConfig) AuthorizeRedirect(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, oac.oAuth2Config().AuthCodeURL(callbackState, oauth2.AccessTypeOffline), http.StatusFound)
	return
}

func (oac *OAuthConfig) oAuth2Config() (oauth2Config *oauth2.Config) {
	oauth2Config = &oauth2.Config{
		ClientID:     oac.ClientID,
		ClientSecret: oac.Secret,
		Scopes:       oac.Scopes,
		Endpoint: oauth2.Endpoint{
			AuthURL:  oac.AuthURL,
			TokenURL: oac.TokenURL,
		},
	}
	return
}

func (oac *OAuthConfig) getCrossEnvAdminSet() (crossEnvAdminSet map[string]struct{}) {
	crossEnvAdminSet = make(map[string]struct{})
	for _, k := range oac.CrossEnvAdminGroups {
		crossEnvAdminSet[k] = struct{}{}
	}
	return
}

func mergeOAuthConfig(provided *OAuthConfig) {
	if provided.AuthURL == "" {
		provided.AuthURL = defaultOAuthConfig.AuthURL
	}
	if provided.TokenURL == "" {
		provided.TokenURL = defaultOAuthConfig.TokenURL
	}
	if len(provided.Scopes) == 0 {
		provided.Scopes = defaultOAuthConfig.Scopes
	}
	if provided.CheckTokenURL == "" {
		provided.CheckTokenURL = defaultOAuthConfig.CheckTokenURL
	}

	if len(provided.AdminGroups) > 0 {
		set := make(map[string]struct{})
		for _, group := range provided.AdminGroups {
			set[group] = struct{}{}
		}
		provided.adminSet = set

	}

	if len(provided.CrossEnvAdminGroups) == 0 {
		provided.CrossEnvAdminGroups = defaultOAuthConfig.CrossEnvAdminGroups
	}
}

func ldapGroupToDynamoNamespace(group string) string {
	group = strings.TrimSuffix(group, "-admin")
	group = strings.TrimSuffix(group, "-sudo")
	group = strings.TrimPrefix(group, "team-")
	return group
}

// ldapGroupsToDynamoNamespaces takes ldap groups and maps them to their equivalent dynamo name
func ldapGroupsToDynamoNamespaces(groups []string) []string {
	grpSet := make(map[string]struct{}) //use a set incase user in multiple equivalent groups ("team-bar" and "team-bar-sudo" are just "bar")

	for _, group := range groups {
		group = ldapGroupToDynamoNamespace(group)
		grpSet[group] = struct{}{}
	}

	// remove "infra" from set
	delete(grpSet, "infra")

	var dynamoNamespaces []string

	for key := range grpSet {
		dynamoNamespaces = append(dynamoNamespaces, key)
	}

	return dynamoNamespaces
}

type oauthAuthorizer struct {
	adminSet         map[string]struct{}
	crossEnvAdminSet map[string]struct{}
}

func (a *oauthAuthorizer) IsCrossEnvAdmin(user *guardian.User) (ok bool) {
	for _, group := range user.Groups {
		_, ok = a.crossEnvAdminSet[group]
		if ok {
			return
		}
	}
	return
}

// IsAdmin verifies if a user is an admin
func (a *oauthAuthorizer) IsAdmin(user *guardian.User) bool {
	if a.adminSet == nil || len(a.adminSet) == 0 {
		return false
	}

	for _, group := range user.Groups {
		if _, ok := a.adminSet[group]; ok {
			return true
		}
	}
	return false
}

// IsAllowed checks to see if the user is allowed access to the secret based
// on its name
func (a *oauthAuthorizer) IsAllowed(secretName string, user *guardian.User) error {
	//Allow all users in admin groups
	if a.IsAdmin(user) {
		return nil
	}

	namespace := manager.SecretNameToNamespace(secretName)
	if !namespaceInGroups(namespace, ldapGroupsToDynamoNamespaces(user.Groups)) {
		return ForbiddenError{User: user, Keys: []string{secretName}}
	}
	return nil
}

// CheckKeys takes a slice of keys and verifies a user is allowed access
func (a *oauthAuthorizer) CheckKeys(keys []string, user *guardian.User) error {
	var FErrors = ForbiddenError{User: user}
	var err error
	// if empty keys passed, allow
	if len(keys) == 0 {
		return nil
	}
	for _, key := range keys {
		err = a.IsAllowed(key, user)
		if err != nil {
			// Only really expecting a ForbiddenError type, but being safe
			if Ferr, ok := err.(ForbiddenError); ok {
				FErrors.AddKeys(Ferr.Keys)
			} else {
				// Something new, going to return immediately
				return err
			}
		}
	}
	if len(FErrors.Keys) != 0 {
		return FErrors
	}
	return nil
}

// OAuthAuthorizer provides methods for authorizing a user
type OAuthAuthorizer interface {
	IsAdmin(user *guardian.User) (ok bool)
	IsCrossEnvAdmin(user *guardian.User) (ok bool)
	CheckKeys(keys []string, user *guardian.User) error
	IsAllowed(secretName string, user *guardian.User) error
}

func namespaceInGroups(namespace string, groups []string) bool {
	for _, group := range groups { // MS:using a set seems overkill for a users groups (like 6 at most?)
		if namespace == group {
			return true
		}
	}
	return false
}
