package apiserver

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	asiimov "code.justin.tv/systems/guardian/middleware"
	"code.justin.tv/systems/sandstorm/internal/stat"
	"code.justin.tv/systems/sandstorm/manager"

	"golang.org/x/oauth2"

	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/iam"
	"github.com/aws/aws-sdk-go/service/iam/iamiface"
	"github.com/gorilla/mux"
	negronilogrus "github.com/meatballhat/negroni-logrus"
	"github.com/rs/cors"
	"github.com/sirupsen/logrus"
	"github.com/urfave/negroni"
)

const contentType = "application/vnd.api+json"

// Service implements the sandstorm api service
type Service struct {
	*negroni.Negroni

	oauthMiddleware negroni.Handler

	router  *mux.Router
	config  *Config
	clients *clients
}

// clients contains dependencies required by Service to make calls to external services
type clients struct {
	logger          logrus.FieldLogger
	changelog       *changelogClient
	mgr             manager.API
	oauthAuthorizer OAuthAuthorizer
	ddb             dynamodbiface.DynamoDBAPI
	iam             iamiface.IAMAPI
	statter         stat.Statter
}

func (svc *Service) logEntry(r *http.Request) (entry *logrus.Entry) {
	return logEntry(svc.clients.logger, r)
}

// SubService implements a sub-service attached to the main service
type SubService interface {
	http.Handler

	// AttachHandlers attaches endpoints to the subservice
	AttachHandlers()
}

func (svc *Service) setup() (err error) {
	if lgr, ok := svc.clients.logger.(*logrus.Logger); ok {
		svc.Negroni.Use(negronilogrus.NewMiddlewareFromLogger(lgr, "web"))
	}

	// add statter ad middleware
	svc.Negroni.Use(svc.clients.statter)
	svc.Negroni.Use(buildCORS(svc.config.CORS))

	svc.router.HandleFunc("/status", svc.status).Methods("GET")
	svc.router.HandleFunc("/favicon.ico", favicon).Methods("GET")

	svc.router.HandleFunc("/oauth2/authorize", svc.config.OAuth.AuthorizeRedirect).Methods("GET")
	svc.router.HandleFunc("/oauth2/callback", svc.oAuth2Callback).Methods("GET")

	svc.setOAuthMiddleware()

	// attach subservices
	svc.AddSubService([]string{"/secrets", "/generate-plaintext"}, &SecretService{svc.newSubService()})

	pgSvc, err := buildIAMPolicyGenerator(svc.newSubService())
	if err != nil {
		return
	}
	svc.AddSubService([]string{"/roles", "/users", "/groups"}, pgSvc)

	svc.Negroni.UseHandler(svc.router)
	return
}

func newService(config *Config, clients *clients) *Service {
	// set up router
	return &Service{
		Negroni: negroni.New(),
		router:  mux.NewRouter().UseEncodedPath(),
		config:  config,
		clients: clients,
	}
}

// New creates a new service
func New(config *Config) (svc *Service, err error) {
	// set up dependencies
	logger := SetupLogging(config.Syslog)

	awsConfig := config.buildAWSConfig()
	sess := session.New(awsConfig)
	ddb := dynamodb.New(sess)
	iamClient := iam.New(sess)

	statter := stat.New(config.Environment, stscreds.NewCredentials(sess, config.CloudWatchRoleARN), logger)

	changelog, err := configureChangelog(config.Changelog, logger, statter)
	if err != nil {
		err = fmt.Errorf("error configuring changelog: %s", err.Error())
		return
	}

	svc = newService(config, &clients{
		logger:    logger,
		statter:   statter,
		changelog: changelog,
		mgr:       buildManager(config, logger),
		ddb:       ddb,
		iam:       iamClient,
		oauthAuthorizer: &oauthAuthorizer{
			adminSet:         config.OAuth.adminSet,
			crossEnvAdminSet: config.OAuth.getCrossEnvAdminSet(),
		},
	})
	err = svc.setup()
	if err != nil {
		err = fmt.Errorf("error setting up apiserver: %s", err.Error())
		return
	}
	return
}

func (svc *Service) newSubService() *Service {

	router := mux.NewRouter().UseEncodedPath()

	return &Service{
		Negroni: negroni.New(negroni.Wrap(router)),
		router:  router,
		config:  svc.config,
		clients: svc.clients,
	}
}

// AddSubService attaches a subservice to apiserver.Service. It calls
// AttachHandlers() as well as Validate(). Any errors from Validate() are
// returned as-is.
func (svc *Service) AddSubService(pathPrefixes []string, service SubService) {
	service.AttachHandlers()
	for _, prefix := range pathPrefixes {
		svc.router.PathPrefix(prefix).Handler(negroni.New(
			svc.oauthMiddleware,
			negroni.Wrap(service),
		))
	}
	return
}

func buildCORS(config CORSConfig) *cors.Cors {
	return cors.New(cors.Options{
		AllowedOrigins: config.AllowedOrigins,
		AllowedMethods: config.AllowedMethods,
		AllowedHeaders: config.AllowedHeaders,
		Debug:          config.Debug,
	})
}

func (svc *Service) setOAuthMiddleware() {
	asi := asiimov.New(svc.clients.logger, svc.config.OAuth.oAuth2Config())
	asi.CheckTokenURL = svc.config.OAuth.CheckTokenURL
	// allow any user with valid oauth creds,so no gating necessary
	asi.VerifyAccess = asi.DefaultVerifyAccess
	svc.oauthMiddleware = negroni.HandlerFunc(func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {

		user, err := asi.HandleGuardianRequest(r)
		if err == nil && user == nil {
			// no token passed
			svc.apiErrorStatus(w, r, errors.New("no OAuth token provided"), http.StatusUnauthorized)
			return

		}
		if err != nil {
			svc.apiErrorStatus(w, r, fmt.Errorf("OAuthRequired returned unauthorized: %s", err), http.StatusUnauthorized)
			return
		}
		if user == nil {
			svc.clients.logger.WithFields(logrus.Fields{
				"path": r.URL.Path,
			}).Warn("OAuth request failed - no authorization header present")
			svc.apiErrorStatus(w, r, errors.New("no authorization header present"), http.StatusUnauthorized)
			return
		}

		next(w, setGuardianUser(r, user))
	})
}

// GET /oauth2/callback
func (svc *Service) oAuth2Callback(w http.ResponseWriter, r *http.Request) {
	// validate state
	state := r.FormValue("state")
	if state != callbackState {
		svc.apiErrorStatus(w, r, errors.New("oauth2 state mismatch"), http.StatusUnauthorized)
		return
	}

	code := r.FormValue("code")

	token, err := svc.config.OAuth.oAuth2Config().Exchange(oauth2.NoContext, code)
	if err != nil {
		svc.apiErrorStatus(w, r, fmt.Errorf("error getting access token: %s", err.Error()), http.StatusInternalServerError)
		return
	}

	resp, err := json.MarshalIndent(token, "", " ")
	if err != nil {
		svc.apiErrorStatus(w, r, err, http.StatusInternalServerError)
	}
	w.Header().Set("Content-Length", strconv.Itoa(len(resp)))
	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(resp)
	if err != nil {
		svc.clients.logger.Println("unable to write to http.ResponseWriter")
	}

	return
}

func (svc *Service) status(w http.ResponseWriter, r *http.Request) {
	// TODO: coalesce results of both checks
	entry := svc.logEntry(r)
	err := svc.clients.mgr.CheckTable()
	if err != nil {
		entry.Warnf("error checking DynamoDB table: %s", err.Error())
		svc.apiError(errors.New("error checking DynamoDB table"), w, r)
		return
	}
	err = svc.clients.mgr.CheckKMS()
	if err != nil {
		entry.Warnf("error checking KMS key: %s", err.Error())
		svc.apiError(errors.New("error checking KMS"), w, r)
		return
	}

	err = jsonResponse(map[string]string{"status": "ok"}, w)
	if err != nil {
		svc.clients.logger.Errorf("error encoding JSON error response: %s", err)
	}

}

// it's a secret but don't worry about it
const (
	dududu = `
AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAABILAAASCwAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATKAAAEygCABHnB0ALokC
AC+KAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFWCsABNe
qjkncLbPGGSvcQBJnRAEYKoAADCRAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
A1OiAAAATwNAhMKYWpzT/yxyt/oSXKi9BlSjMglWpQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAJSoQAAP5UWY6HT1HSz4f9JjMj/O4DA/xdjraIARpgYCILDAAAhfgAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAqgwAvj8gAClejN2ai0+ODu+X/YKLX/1me1f9Kkcz9MXy+xwhV
ojk40vsAACqDAAAAAAAAAAAAAAAAAAAGWgAqf74AFV+mMluaztJ0seD/Y6fb/2Oo3f9kqd3/
Z6nc/2Gh1f9Ch8LbHGmuQTB5uQAAAAAAAAAAAAAAAAAXZakADV2kJVSWy8d2tOL/XKHX/1Wa
0/9WnNP/Wp7V/16d0/9fm9D/WpbM/yJtr5EAAAAAAlKcAAATTgAjh8YAAFCZFEKHwJaUyOz/
h8Ps/3Oy4f9oq93/V5zU/1OY0f9Rls//UZbQ/02Szf80fr7iDWCmLhBhpwAreLQAF2eoLkSH
v79gmcz8Zp/R/2Wh1P9kpdj/X6PY/1ab0/9Jjsr/OX6+/yxwtP8scLX/K3O1+w9gpVYUY6kA
AFCXDTd8t7JIg7//Onu8/1OY0f95wO3/eb/t/3zC7/9/xfH/fsTw/3e96/9Nks3/KGyx/yVq
sP8XY6jGCVygJg1jpCY2erjgW5XM/3a55/9bodf/c7jo/3S56f9oreD/Yqjc/12j2P9VmtL/
TZLM/1SZ0v8zd7n/IGet/A5go2oAVpkNKXe1qk6PyP94teP/eL3r/3vB7v+AxvH/fsTx/3vB
7v96v+3/esDu/3i+7P9dotj/N3y9/x5nq+8NX6JLG3GtAA9kohwoebSHPojB10uTy/diqt3/
d77r/3rA7f97we7/ecDt/3W96v9lrt/9OIG+7Bxpq70PYaNfBV2dCAAAAAAAQIMAADB0Agtf
nxocb6xFKny1bzaGvo88i8KgPo3DpzuKwZ4zg7yGI3WwXwhdnjAAVpYLBW3IAABKfQAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAABMAAAAEAQAACQIAAAEBAAAHAAAAAAAAAAAAAAAAAAAA
AAAAAAAA//8AAPx/AAD8PwAA+B8AAPgPAAD4BwAA8AMAAOABAADAAQAAgAEAAAAAAAAAAAAA
AAAAAIAAAADAAwAA/j8AAA==`
)

func favicon(w http.ResponseWriter, r *http.Request) {
	data, err := base64.StdEncoding.DecodeString(dududu)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "image/x-icon")
	reader := bytes.NewReader(data)
	_, err = reader.WriteTo(w)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func buildManager(config *Config, logger logrus.FieldLogger) *manager.Manager {
	mgrConfig := manager.Config{
		Environment: config.Environment,
	}

	mgrConfig.AWSConfig = config.buildAWSConfig()
	return manager.New(mgrConfig)
}
