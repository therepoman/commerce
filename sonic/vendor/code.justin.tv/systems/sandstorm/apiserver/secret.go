package apiserver

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strconv"

	"code.justin.tv/systems/sandstorm/cryptorand"
	"code.justin.tv/systems/sandstorm/manager"
	"github.com/gorilla/mux"
	validator "gopkg.in/validator.v2"
)

// SecretService provides http.Handlers for secrets management
type SecretService struct {
	*Service
}

// AttachHandlers attaches http.Handlers for secrets service to router
func (ss *SecretService) AttachHandlers() {
	ss.router.HandleFunc("/secrets", ss.getSecretList).Methods("GET")
	ss.router.HandleFunc("/secrets", ss.postSecret).Methods("POST")
	ss.router.HandleFunc("/secrets/{secret}/versions", ss.getSecretVersions).Methods("GET")
	ss.router.HandleFunc("/secrets/{secret}", ss.patchSecret).Methods("PATCH")
	ss.router.HandleFunc("/secrets/{secret}", ss.deleteSecret).Methods("DELETE")
	ss.router.HandleFunc("/secrets/{secret}", ss.getSecret).Methods("GET")
	ss.router.HandleFunc("/secrets/{secret}/copy", ss.copySecret).Methods("PUT")
	ss.router.HandleFunc("/secrets/{secret}/revert", ss.revertSecret).Methods("PUT")
	ss.router.HandleFunc("/generate-plaintext", ss.postGeneratePlaintext).Methods("POST")
}

func getSecretName(vars map[string]string) (string, error) {
	return url.QueryUnescape(vars["secret"])
}

// GET /secrets
func (ss *SecretService) getSecretList(w http.ResponseWriter, r *http.Request) {
	user, err := getGuardianUser(r)
	if err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}
	entry := ss.logEntry(r)
	entry.Info("Listing secrets")

	var secrets []*manager.Secret

	if ss.clients.oauthAuthorizer.IsAdmin(user) {
		secrets, err = ss.clients.mgr.List()
		if err != nil {
			ss.apiError(err, w, r)
			return
		}
	} else {
		namespaces := ldapGroupsToDynamoNamespaces(user.Groups)

		var namespaceSecrets []*manager.Secret

		for _, namespace := range namespaces {
			namespaceSecrets, err = ss.clients.mgr.ListNamespace(namespace)

			if err != nil {
				ss.apiError(err, w, r)
				return
			}
			secrets = append(secrets, namespaceSecrets...)
		}

	}

	err = secretArrayToJSON(secrets, w)
	if err != nil {
		ss.apiError(err, w, r)
		return
	}
}

// GET /secrets/{secret}
func (ss *SecretService) getSecret(w http.ResponseWriter, r *http.Request) {
	user, err := getGuardianUser(r)
	if err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}
	vars := mux.Vars(r)
	secretName, err := getSecretName(vars)
	if err != nil {
		ss.apiError(err, w, r)
		return
	}

	entry := ss.logEntry(r).WithField("secret", secretName)
	entry.Info("Fetching secret")

	// convert to GET
	err = ss.clients.oauthAuthorizer.IsAllowed(secretName, user)
	if err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	secret, err := ss.clients.mgr.GetEncrypted(secretName)
	if err != nil {
		ss.apiError(err, w, r)
		return
	}
	if secret == nil {
		ss.apiErrorStatus(w, r, errors.New("secret not found"), http.StatusNotFound)
		return
	}
	err = secretSingleToJSON(secret, w)
	if err != nil {
		ss.apiError(err, w, r)
	}
}

// GET /secrets/{secret}/versions
type getSecretVersionsQueryParams struct {
	Limit     int `validate:"min=1,max=100"`
	OffsetKey int `validate:"min=0"`
}

func (ss *SecretService) getSecretVersions(w http.ResponseWriter, r *http.Request) {
	user, err := getGuardianUser(r)
	if err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}
	vars := mux.Vars(r)
	secretName, err := getSecretName(vars)
	if err != nil {
		ss.apiError(err, w, r)
		return
	}

	entry := ss.logEntry(r).WithField("secret", secretName)
	entry.Info("Fetching versions")

	// convert to GET
	err = ss.clients.oauthAuthorizer.IsAllowed(secretName, user)
	if err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	queryParams := getSecretVersionsQueryParams{
		Limit:     10,
		OffsetKey: 0,
	}
	if v := r.FormValue("limit"); v != "" {
		vInt, err := strconv.Atoi(v)
		if err != nil {
			ss.apiError(err, w, r)
			return
		}
		queryParams.Limit = vInt
	}
	if v := r.FormValue("offset_key"); v != "" {
		vInt, err := strconv.Atoi(v)
		if err != nil {
			ss.apiError(err, w, r)
			return
		}
		queryParams.OffsetKey = vInt
	}
	if err := validator.Validate(queryParams); err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusBadRequest)
	}

	versions, err := ss.clients.mgr.GetVersionsEncrypted(secretName, int64(queryParams.Limit), int64(queryParams.OffsetKey))
	if err != nil {
		ss.apiError(err, w, r)
		return
	}
	err = secretVersionsPageToJSON(versions, w)
	if err != nil {
		ss.apiError(err, w, r)
	}
}

// Validates the POST request to /secrets Endpoint and ensure that all the correct params are present in the request.
func (ss *SecretService) validatePostRequest(r *http.Request, secret *manager.Secret) (int, error) {
	if secret.Plaintext == nil {
		return http.StatusUnprocessableEntity, errors.New("field 'plaintext' must be specified")
	}

	existingSecret, err := ss.clients.mgr.Exist(secret.Name)
	if err != nil {
		return getErrorMessageAndStatusCodeByError(err, r)
	}
	if existingSecret {
		return http.StatusConflict, errors.New("secret already exists, update (PATCH) instead")
	}
	return http.StatusOK, nil
}

func parseSecret(secret *manager.Secret) error {
	if secret == nil {
		return errors.New("missing required secret parameter")
	}
	if secret.Name == "" {
		return errors.New("field 'name' cannot be empty")
	}
	return nil
}

type putSecretRequest struct {
	Data         SecretElement                `json:"data"`
	Autogenerate manager.FillPlaintextRequest `json:"autogenerate"`
}

func (r *putSecretRequest) validate() (err error) {
	if r.Autogenerate.Length > 0 {
		if len(r.Data.Attributes.Plaintext) > 0 {
			err = errors.New("autogenerate cannot be sent with a plaintext value")
			return
		}
		err = r.Autogenerate.Validate()
		if err != nil {
			return err
		}
	}
	return
}

// Creates a new secret
// POST /secrets request
func (ss *SecretService) postSecret(w http.ResponseWriter, r *http.Request) {
	user, err := getGuardianUser(r)
	if err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	request := putSecretRequest{}
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		// XXX we should catch base64.CorruptInputError and
		// return 400 or 422
		ss.apiError(err, w, r)
		return
	}

	if err := request.validate(); err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusBadRequest)
		return
	}

	secret := request.Data.Attributes
	err = parseSecret(secret)
	if err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusUnprocessableEntity)
		return
	}

	if secret.CrossEnv && !ss.clients.oauthAuthorizer.IsCrossEnvAdmin(user) {
		err = errors.New("cross_env can only be editted by cross env admins")
		ss.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	err = ss.clients.oauthAuthorizer.IsAllowed(secret.Name, user)
	if err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	if request.Autogenerate.Length > 0 {
		if err := secret.FillPlaintext(&request.Autogenerate); err != nil {
			ss.apiError(err, w, r)
			return
		}
	}

	httpStatus, err := ss.validatePostRequest(r, secret)
	if err != nil {
		ss.apiErrorStatus(w, r, err, httpStatus)
		return
	}

	entry := ss.logEntry(r).WithField("secret", secret.Name)
	entry.Infof("Attempting to create a secret", r.Method)
	secret.ActionUser = user.CN
	err = ss.clients.mgr.Post(secret)
	if err != nil {
		ss.apiError(err, w, r)
		return
	}

	// Retrieve the secret from Datastore so that we return updated
	// information back to the caller.
	secret, err = ss.clients.mgr.GetEncrypted(secret.Name)
	if err != nil {
		ss.apiError(err, w, r)
		return
	}
	if secret == nil {
		ss.apiErrorStatus(w, r, errors.New("secret not found"), http.StatusNotFound)
		return
	}
	err = secretSingleToJSON(secret, w)
	if err != nil {
		ss.apiError(err, w, r)
		return
	}

}

// Updates a secret
// /PATCH /secrets/:secret_name
func (ss *SecretService) patchSecret(w http.ResponseWriter, r *http.Request) {
	user, err := getGuardianUser(r)
	if err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)

	request := &manager.PatchInput{}
	if err := json.NewDecoder(r.Body).Decode(request); err != nil {
		// XXX we should catch base64.CorruptInputError and
		// return 400 or 422
		ss.apiError(err, w, r)
		return
	}

	request.Name, err = getSecretName(vars)
	if err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusBadRequest)
		return
	}

	if err := request.Validate(); err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusBadRequest)
		return
	}

	err = ss.clients.oauthAuthorizer.IsAllowed(request.Name, user)
	if err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	if request.CrossEnv != nil && *request.CrossEnv && !ss.clients.oauthAuthorizer.IsCrossEnvAdmin(user) {
		err = errors.New("cross_env can only be editted by cross env admins")
		ss.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}
	request.ActionUser = &user.CN
	entry := ss.logEntry(r).WithField("secret", request.Name)
	entry.Infof("Attempting to update a secret", r.Method)
	err = ss.clients.mgr.Patch(request)
	if err != nil {
		switch err {
		case manager.ErrSecretDoesNotExist:
			ss.apiErrorStatus(w, r, err, http.StatusNotFound)
		default:
			ss.apiError(err, w, r)
		}
		return
	}

	// Retrieve the secret from Datastore so that we return updated
	// information back to the caller.
	secret, err := ss.clients.mgr.GetEncrypted(request.Name)
	if err != nil {
		ss.apiError(err, w, r)
		return
	}
	if secret == nil {
		ss.apiError(errors.New("secret not found"), w, r)
		return
	}
	err = secretSingleToJSON(secret, w)
	if err != nil {
		ss.apiError(err, w, r)
		return
	}

}

// Delete secret /secrets/{secret}
func (ss *SecretService) deleteSecret(w http.ResponseWriter, r *http.Request) {
	user, err := getGuardianUser(r)
	if err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}
	vars := mux.Vars(r)

	name, err := getSecretName(vars)
	if err != nil {
		ss.apiError(err, w, r)
		return
	}

	err = ss.clients.oauthAuthorizer.IsAllowed(name, user)
	if err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	entry := ss.logEntry(r).WithField("secret", name)
	entry.Info("Deleting secret")

	_, err = ss.clients.mgr.DeleteSecret(&manager.DeleteSecretInput{
		Name:       name,
		ActionUser: user.CN,
	})
	if err != nil {
		switch err := err.(type) {
		case manager.InvalidRequestError:
			ss.apiErrorStatus(w, r, err, http.StatusBadRequest)
		default:
			ss.apiError(err, w, r)
		}
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// PUT /secrets/{secret}/copy?destination=<dest>
// Copies a secret from {secret} to <dest>
func (ss *SecretService) copySecret(w http.ResponseWriter, r *http.Request) {
	user, err := getGuardianUser(r)
	if err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	source, err := getSecretName(vars)
	err = ss.clients.oauthAuthorizer.IsAllowed(source, user)
	if err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	destination := r.FormValue("destination")
	if destination == "" {
		ss.apiError(fmt.Errorf("no destination specified"), w, r)
		return
	}
	err = ss.clients.oauthAuthorizer.IsAllowed(destination, user)
	if err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	entry := ss.logEntry(r).WithField("secret", source).WithField("destination", destination)
	entry.Info("Copying secret")

	_, err = ss.clients.mgr.CopySecret(&manager.CopySecretInput{
		Source:      source,
		Destination: destination,
		ActionUser:  user.CN,
	})

	if err != nil {
		switch err := err.(type) {
		case manager.InvalidRequestError:
			ss.apiErrorStatus(w, r, err, http.StatusBadRequest)
		default:
			ss.apiError(err, w, r)
		}
		return
	}
}

type putRevertSecretRequest struct {
	Version int64 `json:"version"`
}

func (ss *SecretService) revertSecret(w http.ResponseWriter, r *http.Request) {
	user, err := getGuardianUser(r)
	if err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	secretName, err := getSecretName(vars)
	if err := ss.clients.oauthAuthorizer.IsAllowed(secretName, user); err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	var body putRevertSecretRequest
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusBadRequest)
		return
	}

	version := body.Version
	if version <= 0 {
		ss.apiErrorStatus(w, r, errors.New("version must be greater than 0"), http.StatusBadRequest)
		return
	}

	entry := ss.logEntry(r).
		WithField("secret", secretName).
		WithField("version", version)
	entry.Info("reverting secret")

	_, err = ss.clients.mgr.RevertSecret(&manager.RevertSecretInput{
		Name:       secretName,
		Version:    version,
		ActionUser: user.CN,
	})

	if err != nil {
		switch err := err.(type) {
		case manager.InvalidRequestError:
			ss.apiErrorStatus(w, r, err, http.StatusBadRequest)
		default:
			if err == manager.ErrorSecretVersionDoesNotExist || err == manager.ErrSecretDoesNotExist {
				ss.apiErrorStatus(w, r, err, http.StatusBadRequest)
			} else {
				ss.apiError(err, w, r)
			}
		}
		return
	}
}

type postGeneratePlaintextRequest struct {
	cryptorand.GeneratePlaintextRequest
}

func (r postGeneratePlaintextRequest) validate() error {
	if err := r.GeneratePlaintextRequest.Validate(); err != nil {
		return err
	}
	if r.Length < manager.PlaintextLengthMin {
		return errors.New("plaintext length is less than minumum length")
	}
	if r.Length > manager.PlaintextLengthMax {
		return errors.New("plaintext length is greater than maximum length")
	}
	return nil
}

type postGeneratePlaintextResponse struct {
	Plaintext string `json:"plaintext"`
}

// postGeneratePlaintext returns cryptographically generated plaintext with
// various requirements such as at least one uppercase letter.
func (ss *SecretService) postGeneratePlaintext(w http.ResponseWriter, r *http.Request) {
	request := &postGeneratePlaintextRequest{}

	if err := json.NewDecoder(r.Body).Decode(request); err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusBadRequest)
		return
	}

	if err := request.validate(); err != nil {
		ss.apiErrorStatus(w, r, err, http.StatusBadRequest)
	}

	plaintext, err := cryptorand.GeneratePlaintext(&request.GeneratePlaintextRequest)
	if err != nil {
		ss.apiError(err, w, r)
		return
	}

	response := postGeneratePlaintextResponse{
		Plaintext: string(plaintext),
	}
	if err := jsonResponse(response, w); err != nil {
		ss.apiError(err, w, r)
	}
}

// SecretArray is a helper type for outputting an array of Secrets in
// JSON.
type SecretArray struct {
	Data []SecretElement `json:"data"`
}

// SecretSingle is a helper type for outputting a single Secret in
// JSON
type SecretSingle struct {
	Data SecretElement `json:"data"`
}

// SecretVersionsPage is a helper type for outputing a page of versions of a
// secret in JSON.
type SecretVersionsPage struct {
	Data      []SecretElement `json:"data"`
	Limit     int64           `json:"limit"`
	NextKey   int64           `json:"next_key"`
	OffsetKey int64           `json:"offset_key"`
}

// SecretElement represents a secret for JSONAPI output.
type SecretElement struct {
	Type       string          `json:"type"`
	Attributes *manager.Secret `json:"attributes"`
	ID         string          `json:"id"`
}

// XXX coalesce this with secretSingleToJSON to one function that
// takes an interface
func secretArrayToJSON(secrets []*manager.Secret, w http.ResponseWriter) error {
	result := SecretArray{
		Data: secretArrayToSecretElementArray(secrets),
	}
	return jsonResponse(result, w)
}

// Converts a secret struct to a JSONAPI spec element
func secretToSecretElement(secret *manager.Secret) SecretElement {
	return SecretElement{
		ID:         secret.Name,
		Type:       "Secret",
		Attributes: secret,
	}
}

func secretArrayToSecretElementArray(secrets []*manager.Secret) []SecretElement {
	result := make([]SecretElement, len(secrets))
	for nSecret, secret := range secrets {
		result[nSecret] = secretToSecretElement(secret)
	}
	return result
}

func secretSingleToJSON(secret *manager.Secret, w http.ResponseWriter) error {
	result := SecretSingle{Data: secretToSecretElement(secret)}
	return jsonResponse(result, w)
}

func secretVersionsPageToJSON(versions *manager.VersionsPage, w http.ResponseWriter) error {
	result := SecretVersionsPage{
		Data:      secretArrayToSecretElementArray(versions.Secrets),
		OffsetKey: versions.OffsetKey,
		Limit:     versions.Limit,
		NextKey:   versions.NextKey,
	}
	return jsonResponse(result, w)
}
