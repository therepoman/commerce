package apiserver

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"code.justin.tv/systems/guardian/guardian"
	"github.com/aws/aws-sdk-go/aws/awserr"
)

// ErrorWithClassName is used when we want to provide the end user with a
// unique class name to compare to.
type ErrorWithClassName interface {
	GetClassName() string
}

// APIErrorer provides context needed to return the correct api error response
type APIErrorer interface {
	// Must satisfy base error interface
	error
	// HTTP response code
	Code() int
}

// APIError is a generic implementation of APIErrorer
type APIError struct {
	msg  error
	code int
}

// Error for interface adherence
func (ae APIError) Error() string {
	return ae.msg.Error()
}

// Code represets the http status code of the error
func (ae APIError) Code() int {
	return ae.code
}

// apiError handles API Responses where an error has been raised internally
func (svc *Service) apiError(err error, w http.ResponseWriter, r *http.Request) {
	errorStatus, err := getErrorMessageAndStatusCodeByError(err, r)
	svc.apiErrorStatus(w, r, err, errorStatus)
}

func getErrorMessageAndStatusCodeByError(err error, r *http.Request) (int, error) {
	var errorStatus int
	switch newErr := err.(type) {
	case awserr.RequestFailure:
		// remove struct from error and return string to error
		err = errors.New(newErr.Message())
		errorStatus = newErr.StatusCode()
	case awserr.Error:
		// remove struct from error and return string to error
		err = errors.New(newErr.Message())
		errorStatus = http.StatusInternalServerError
	case APIErrorer:
		errorStatus = newErr.Code()
	default:
		errorStatus = http.StatusInternalServerError
	}
	return errorStatus, err
}

// JSONError is a type representing a JSONAPI error.
type JSONError struct {
	Class  string `json:"class"`
	Detail string `json:"detail"`
	Status string `json:"status"`
	Source struct {
		Pointer string `json:"pointer"`
	} `json:"source"`
}

// JSONErrors represents a list of JSONError objects. Any error response should
// return this.
type JSONErrors struct {
	Errors []JSONError `json:"errors"`
}

// apiErrorStatus returns a JSON formatted error response in the body
// of http.Error for the specified statusCode.
// This also logs the error.
func (svc *Service) apiErrorStatus(w http.ResponseWriter, r *http.Request, responseError error, statusCode int) {
	entry := svc.logEntry(r).WithField("status", statusCode)
	entry.Warnf("error: %s", responseError.Error())

	className := "Error"

	if v, ok := responseError.(ErrorWithClassName); ok {
		className = v.GetClassName()
	}

	jError := JSONError{
		Class:  className,
		Detail: responseError.Error(),
		Status: strconv.Itoa(statusCode),
	}
	jError.Source.Pointer = "data/attributes/name"

	result := JSONErrors{
		Errors: []JSONError{jError},
	}

	err := jsonResponseStatus(result, w, statusCode)
	if err != nil {
		entry := svc.logEntry(r).WithField("status", statusCode)
		entry.Warnf("error encoding JSON error response: %s", err)
	}
}

// ForbiddenError is returned when attempting to create a policy containing
// keys and/or namespaces the user does not have access to view.
type ForbiddenError struct {
	Keys []string       `json:"keys"`
	User *guardian.User `json:"user"`
}

func (fe ForbiddenError) String() string { return fe.Error() }

func (fe ForbiddenError) Error() string {
	return fmt.Sprintf("User %s does not have access to keys %s. User's groups: %v", fe.User.CN, fe.Keys, fe.User.Groups)
}

// Code allows for adherence to APIErrorer
func (fe ForbiddenError) Code() int { return http.StatusForbidden }

// AddKeys is a helper to add key to Keys slice
func (fe *ForbiddenError) AddKeys(keys []string) {
	fe.Keys = append(fe.Keys, keys...)
}

// IsForbiddenError checks to see if error is of type ForbiddenError
func IsForbiddenError(err error) bool {
	if _, ok := err.(*ForbiddenError); ok {
		return true
	}
	return false
}

// ForbiddenNamespace is returned when a user doesn't have access to a particular namespace
type ForbiddenNamespace struct {
	Namespace string
	User      *guardian.User
}

func (fn ForbiddenNamespace) Error() string {
	return fmt.Sprintf("User %s does not have access to namespace %s. User's groups: %v", fn.User.CN, fn.Namespace, fn.User.Groups)
}

// Code allows for adherence to APIErrorer
func (fn ForbiddenNamespace) Code() int { return http.StatusForbidden }

// InvalidLDAPGroupsError is returned when a user's ldap groups
// aren't allowed to access the resource
type InvalidLDAPGroupsError struct {
	User   *guardian.User
	Groups []string
}

// Error returns the error string
func (ilg InvalidLDAPGroupsError) Error() string {
	return fmt.Sprintf("User %s is not a member of an administrative group. User's groups: %s, Owner groups %s", ilg.User.CN, ilg.User.Groups, ilg.Groups)
}

// Code is the status code associated with the error
func (ilg InvalidLDAPGroupsError) Code() int { return http.StatusForbidden }
