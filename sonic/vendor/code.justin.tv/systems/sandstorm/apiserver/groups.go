package apiserver

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"

	"code.justin.tv/systems/sandstorm/manager"
	"code.justin.tv/systems/sandstorm/policy"

	"github.com/gorilla/mux"
)

// DeleteGroup deletes an IAM Group
// Route: DELETE /groups/{groupName}
func (pg *IAMPolicyGenerator) DeleteGroup(w http.ResponseWriter, r *http.Request) {
	user, err := getGuardianUser(r)
	if err != nil {
		pg.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	groupName, err := url.QueryUnescape(vars["group"])
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	err = pg.IAMPolicyGenerator.DeleteGroup(groupName, pg.NewAuthorizedUser(user))
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	// HTTP status 204 so no entity returned
	w.WriteHeader(204)

	if pg.config.Changelog.Enabled {
		description := fmt.Sprintf("%s %s", r.Method, groupName)
		command := "deleted IAM group " + groupName
		additionalData := fmt.Sprintf("username=%s,groupName=%s", user.CN, groupName)
		pg.clients.changelog.logInfoChangeEvent(r, groupName, description, user.CN, command, additionalData)
	}
}

// CreateGroupRequest contains the JSON element expected from a POST /groups
type CreateGroupRequest struct {
	RoleName string `json:"RoleName,omitempty"`
}

// CreateGroup Creates an IAM Group that mirrors an IAM Role/Policy pair
//POST /groups
func (pg *IAMPolicyGenerator) CreateGroup(w http.ResponseWriter, r *http.Request) {
	user, err := getGuardianUser(r)
	if err != nil {
		pg.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	request := &CreateGroupRequest{}
	err = unmarshalJSONRequest(r, request)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	if request.RoleName == "" {
		pg.apiError(manager.InvalidRequestError{Message: errors.New("roleName field empty in request")}, w, r)
		return
	}

	req := policy.CreateGroupRequest{
		Auth:     pg.NewAuthorizedUser(user),
		RoleName: request.RoleName,
	}

	// Create group
	// NOTE: if the name doesn't adhere to sandstorm regex, will add
	// `sandstorm-` prefix to roleName for groupName
	group, err := pg.IAMPolicyGenerator.CreateGroup(req)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	err = jsonResponse(group, w)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	if pg.config.Changelog.Enabled {
		description := fmt.Sprintf("%s %s", r.Method, request.RoleName)
		command := "created IAM group " + request.RoleName
		additionalData := fmt.Sprintf("username=%s,groupName=%s", user.CN, request.RoleName)
		pg.clients.changelog.logInfoChangeEvent(r, request.RoleName, description, user.CN, command, additionalData)
	}

}

// PatchGroup Update a aux policy attachecd to an IAM Group
// Route: PATCH /groups/{groupName}
func (pg *IAMPolicyGenerator) PatchGroup(w http.ResponseWriter, r *http.Request) {
	user, err := getGuardianUser(r)
	if err != nil {
		pg.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	groupName, err := url.QueryUnescape(vars["group"])
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	// patchGroupRequest holds groups name and write_access flag to update the groups.
	type patchGroupRequest struct {
		Name        string `json:"name,omitempty"`
		WriteAccess bool   `json:"write_access"`
	}

	request := &patchGroupRequest{}
	err = unmarshalJSONRequest(r, request)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	if !pg.IAMPolicyGenerator.CheckGroupExistence(groupName) {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	err = pg.IAMPolicyGenerator.PatchGroup(groupName, request.WriteAccess)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	group, err := pg.IAMPolicyGenerator.GetGroup(groupName)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}
	err = jsonResponse(group, w)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	if pg.config.Changelog.Enabled {
		description := fmt.Sprintf("%s %s", r.Method, groupName)
		command := "updated IAM group " + groupName
		additionalData := fmt.Sprintf("username=%s,groupName=%s", user.CN, groupName)
		pg.clients.changelog.logInfoChangeEvent(r, groupName, description, user.CN, command, additionalData)
	}
}

// ListGroups returns List of IAM groups that adhere to sandstorm naming conventions
// Route: GET /groups
func (pg *IAMPolicyGenerator) ListGroups(w http.ResponseWriter, r *http.Request) {
	_, err := getGuardianUser(r)
	if err != nil {
		pg.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	groups, err := pg.IAMPolicyGenerator.ListGroups()
	if err != nil {
		pg.apiError(err, w, r)
		return
	}
	err = jsonResponse(groups, w)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}
}

// GetGroup returns information about a particular IAM group
// Route: GET /groups/{groupName}
func (pg *IAMPolicyGenerator) GetGroup(w http.ResponseWriter, r *http.Request) {
	_, err := getGuardianUser(r)
	if err != nil {
		pg.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	groupName, err := url.QueryUnescape(vars["group"])
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	group, err := pg.IAMPolicyGenerator.GetGroup(groupName)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}
	err = jsonResponse(group, w)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}
}

// GetUser returns a particular users info. Must adhere to sandstorm regex
// Route: GET /users/{user}
func (pg *IAMPolicyGenerator) GetUser(w http.ResponseWriter, r *http.Request) {
	_, err := getGuardianUser(r)
	if err != nil {
		pg.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	userName, err := url.QueryUnescape(vars["user"])
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	// verify user adhere to sandstorm regex
	if !policy.VerifySandstormNamespace(userName) {
		pg.apiError(manager.InvalidRequestError{Message: fmt.Errorf("Users must adhere to sandstorm-namespace")}, w, r)
		return
	}
	user, err := pg.IAMPolicyGenerator.GetUser(userName)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	err = jsonResponse(user, w)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

}

// ListUsers returns all IAM Users that adhere to the sandstorm regex
// Route GET /users
// TODO: add pagination
func (pg *IAMPolicyGenerator) ListUsers(w http.ResponseWriter, r *http.Request) {
	_, err := getGuardianUser(r)
	if err != nil {
		pg.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	users, err := pg.IAMPolicyGenerator.ListUsers()
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	err = jsonResponse(users, w)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

}

// AddUserToGroupReq represent req.Body of AddUserToGroup
type AddUserToGroupReq struct {
	UserNames []string
}

// AddUsersToGroup attempts to add a slice of users to an iam group, and returns 204 on success
// NOTE: AWS will return 204 regardless if users already part of group
func (pg *IAMPolicyGenerator) AddUsersToGroup(w http.ResponseWriter, r *http.Request) {
	user, err := getGuardianUser(r)
	if err != nil {
		pg.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	groupName, err := url.QueryUnescape(vars["group"])
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	request := &AddUserToGroupReq{}
	err = unmarshalJSONRequest(r, request)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	if groupName == "" {
		pg.apiError(
			manager.InvalidRequestError{Message: fmt.Errorf("GroupName is required in req.Body")},
			w,
			r,
		)
		return
	}
	if len(request.UserNames) == 0 {
		pg.apiError(
			manager.InvalidRequestError{Message: fmt.Errorf("UserNames are required in req.Body")},
			w,
			r,
		)
		return
	}

	// Verify Username adhere to sandstorm-regexs
	for _, userName := range request.UserNames {
		if !policy.VerifySandstormNamespace(userName) {
			pg.apiError(
				manager.InvalidRequestError{Message: fmt.Errorf("A Username doesn't adhere to sandstorm namespace: %s", userName)},
				w,
				r,
			)
			return
		}
	}

	input := policy.AddUsersToGroupInput{
		Users:     request.UserNames,
		GroupName: groupName,
		Auth:      pg.NewAuthorizedUser(user),
	}
	err = pg.IAMPolicyGenerator.AddUsersToGroup(input)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}
	// HTTP status 204 so no entity returned
	w.WriteHeader(204)

	if pg.config.Changelog.Enabled {
		description := fmt.Sprintf("%s %s", r.Method, groupName)
		command := "added users to IAM group " + groupName
		additionalData := fmt.Sprintf("username=%s,groupName=%s,userNames=%v", user.CN, groupName, request.UserNames)
		pg.clients.changelog.logInfoChangeEvent(r, groupName, description, user.CN, command, additionalData)
	}

}

// RemoveUsersFromGroupRequest take the same input as AddUserToGroupReq
type RemoveUsersFromGroupRequest AddUserToGroupReq

// RemoveUsersFromGroup does just that
func (pg *IAMPolicyGenerator) RemoveUsersFromGroup(w http.ResponseWriter, r *http.Request) {
	user, err := getGuardianUser(r)
	if err != nil {
		pg.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	groupName, err := url.QueryUnescape(vars["group"])
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	request := &RemoveUsersFromGroupRequest{}
	err = unmarshalJSONRequest(r, request)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	if groupName == "" {
		pg.apiError(
			manager.InvalidRequestError{Message: errors.New("GroupName is required in req.Body")},
			w,
			r,
		)
		return
	}
	if len(request.UserNames) == 0 {
		pg.apiError(
			manager.InvalidRequestError{Message: errors.New("UserNames are required in req.Body")},
			w,
			r,
		)
		return
	}

	// Verify Username adhere to sandstorm-regexs
	for _, userName := range request.UserNames {
		if !policy.VerifySandstormNamespace(userName) {
			pg.apiError(
				manager.InvalidRequestError{Message: fmt.Errorf("a username doesn't adhere to sandstorm namespace: %s", userName)},
				w,
				r,
			)
			return
		}
	}

	input := policy.RemoveUsersFromGroupInput{
		Users:     request.UserNames,
		GroupName: groupName,
		Auth:      pg.NewAuthorizedUser(user),
	}
	err = pg.IAMPolicyGenerator.RemoveUsersFromGroup(input)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}
	// HTTP status 204 so no entity returned
	w.WriteHeader(204)

	if pg.config.Changelog.Enabled {
		description := fmt.Sprintf("%s %s", r.Method, groupName)
		command := "removed users from IAM group " + groupName
		additionalData := fmt.Sprintf("username=%s,groupName=%s,userNames=%v", user.CN, groupName, request.UserNames)
		pg.clients.changelog.logInfoChangeEvent(r, groupName, description, user.CN, command, additionalData)
	}

}
