package apiserver

import (
	"context"
	"fmt"
	"net/http"

	"code.justin.tv/systems/guardian/guardian"
)

type contextKey string

const guardianUserContextKey = contextKey("guardianUser")

// getGuardianUser returns a value for this package from the request values.
func getGuardianUser(r *http.Request) (*guardian.User, error) {

	v := r.Context().Value(guardianUserContextKey)
	user, ok := v.(*guardian.User)
	if !ok {
		return nil, fmt.Errorf("no guardianUser found in context")
	}
	return user, nil
}

// setGuardianUser sets a value for this package in the request values.
func setGuardianUser(r *http.Request, val *guardian.User) *http.Request {
	ctx := context.WithValue(r.Context(), guardianUserContextKey, val)
	return r.WithContext(ctx)
}
