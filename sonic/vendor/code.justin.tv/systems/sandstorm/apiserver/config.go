package apiserver

import (
	"io/ioutil"
	"time"

	"code.justin.tv/systems/sandstorm/resource"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
	"github.com/hashicorp/hcl"
)

// CORSConfig is the HCL config struct for CORS headers.
type CORSConfig struct {
	AllowedOrigins []string `hcl:"allowed_origins"`
	AllowedMethods []string `hcl:"allowed_methods"`
	AllowedHeaders []string `hcl:"allowed_headers"`
	Debug          bool     `hcl:"debug"`
}

// Config is the configuration structure which the HCL config file is
// parsed into.
type Config struct {
	Sandstorm struct {
		Profile           string
		DDBStatsdHostPort string `hcl:"ddb_statsd_host_port"`
		DDBStatsdPrefix   string `hcl:"ddb_statsd_prefix"`
		RoleArn           string `hcl:"role_arn"`
	}
	OAuth OAuthConfig
	HTTP  struct {
		Listen string
	}

	Environment string `hcl:"environment"`

	CORS CORSConfig

	Syslog *SyslogConfig

	Changelog       ChangelogConfig
	PolicyGenerator struct {
		Environment string
	} `hcl:"policy_generator"`

	CloudWatchRoleARN string
}

func (c *Config) buildAWSConfig() *aws.Config {
	awsConfig := &aws.Config{Region: aws.String("us-west-2")}
	sess := session.New(awsConfig)
	if c.Sandstorm.RoleArn != "" {
		stsclient := sts.New(sess)

		arp := &stscreds.AssumeRoleProvider{
			Duration:     900 * time.Second,
			ExpiryWindow: 10 * time.Second,
			RoleARN:      c.Sandstorm.RoleArn,
			Client:       stsclient,
		}

		// Create credentials and config using our Assumed Role that we
		// will use to access the main account with
		credentials := credentials.NewCredentials(arp)
		awsConfig.WithCredentials(credentials)
	}
	return awsConfig
}

// MergeConfigForEnvironment merges aws resource config with provided config object.
// aws resource config are stored in and are fetched using the environment name,
func (c *Config) mergeConfigForEnvironment() (err error) {
	res, err := resource.GetConfigForEnvironment(c.Environment)
	if err != nil {
		return
	}

	if c.Sandstorm.RoleArn == "" {
		c.Sandstorm.RoleArn = res.RoleArn
	}

	if c.PolicyGenerator.Environment == "" {
		c.PolicyGenerator.Environment = c.Environment
	}

	if c.CloudWatchRoleARN == "" {
		c.CloudWatchRoleARN = res.CloudwatchRoleArn
	}
	return
}

// LoadConfig loads an hcl configuration file at the provided path
func LoadConfig(path string) (cfg *Config, err error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return
	}

	cfg = new(Config)
	err = hcl.Decode(cfg, string(b))
	if err != nil {
		return
	}

	err = cfg.mergeConfigForEnvironment()
	if err != nil {
		return
	}

	mergeOAuthConfig(&cfg.OAuth)
	return
}
