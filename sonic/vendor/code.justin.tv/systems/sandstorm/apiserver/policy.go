package apiserver

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"

	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/gorilla/mux"

	"code.justin.tv/systems/guardian/guardian"
	"code.justin.tv/systems/sandstorm/manager"
	"code.justin.tv/systems/sandstorm/policy"
	"code.justin.tv/systems/sandstorm/policy/policyiface"
)

var (
	// ErrNoKeys is returned when a request contains no secret keys
	ErrNoKeys = manager.InvalidRequestError{Message: errors.New("no keys passed to authorize against")}
)

func buildIAMPolicyGenerator(svc *Service) (*IAMPolicyGenerator, error) {
	defConfig, err := policy.GetDefaultConfigForEnvironment(svc.config.Environment)
	if err != nil {
		return nil, err
	}
	pg := &policy.IAMPolicyGenerator{
		AuxPolicyArn:                 defConfig.AuxPolicyArn,
		RWAuxPolicyArn:               defConfig.RWAuxPolicyArn,
		DynamoDBSecretsTableArn:      defConfig.DynamoDBSecretsTableArn,
		DynamoDBSecretsAuditTableArn: defConfig.DynamoDBSecretsAuditTableArn,
		DynamoDBNamespaceTableArn:    defConfig.DynamoDBNamespaceTableArn,
		RoleOwnerTableName:           defConfig.RoleOwnerTableName,
		IAM:                          svc.clients.iam,
		DynamoDB:                     svc.clients.ddb,
		Logger:                       svc.clients.logger,
		AccountID:                    defConfig.AccountID,
	}
	pg.SetPathPrefixEnvironment(svc.config.PolicyGenerator.Environment)

	return &IAMPolicyGenerator{
		Service:            svc,
		IAMPolicyGenerator: pg,
	}, nil
}

// IAMPolicyGenerator implements routes for api server
type IAMPolicyGenerator struct {
	*Service
	IAMPolicyGenerator policyiface.IAMPolicyGeneratorAPI
}

// AttachHandlers attaches service handlers to router
func (pg *IAMPolicyGenerator) AttachHandlers() {
	pg.router.HandleFunc("/roles", pg.ListRoles).Methods("GET")
	pg.router.HandleFunc("/roles", pg.CreateRole).Methods("POST")
	pg.router.HandleFunc("/roles/{role}", pg.UpdateRole).Methods("POST")
	pg.router.HandleFunc("/roles/{role}", pg.GetRole).Methods("GET")
	pg.router.HandleFunc("/roles/{role}", pg.DeleteRole).Methods("DELETE")
	pg.router.HandleFunc("/users", pg.ListUsers).Methods("GET")
	pg.router.HandleFunc("/users/{user}", pg.GetUser).Methods("GET")

	pg.router.HandleFunc("/groups", pg.ListGroups).Methods("GET")
	pg.router.HandleFunc("/groups", pg.CreateGroup).Methods("POST")
	pg.router.HandleFunc("/groups/{group}", pg.GetGroup).Methods("GET")
	pg.router.HandleFunc("/groups/{group}", pg.DeleteGroup).Methods("DELETE")
	pg.router.HandleFunc("/groups/{group}", pg.PatchGroup).Methods("PATCH")
	pg.router.HandleFunc("/groups/{group}/addUsers", pg.AddUsersToGroup).Methods("POST")
	pg.router.HandleFunc("/groups/{group}/removeUsers", pg.RemoveUsersFromGroup).Methods("POST")
}

// AuthorizedUser represents an authorized user
type AuthorizedUser struct {
	*guardian.User
	authorizer OAuthAuthorizer
}

// NewAuthorizedUser converts a guardian.User into a user with added context
// for authorization decisions by the policy generator.
func (pg *IAMPolicyGenerator) NewAuthorizedUser(user *guardian.User) (au AuthorizedUser) {
	return AuthorizedUser{
		User:       user,
		authorizer: pg.clients.oauthAuthorizer,
	}
}

// AuthorizeSecretKeys is used in sandstorm/policy calls to verify the user has access to given keys
func (user AuthorizedUser) AuthorizeSecretKeys(keys []string) (err error) {
	// if no keys passed, raise 422
	if len(keys) == 0 {
		return ErrNoKeys
	}
	err = user.authorizer.CheckKeys(keys, user.User)
	if err != nil {
		return err
	}
	return nil
}

// AuthorizeLDAPGroups verifies that the user is a member of a single group
// in the given groups. Errors when user isn't a member of any (or isn't an admin)
func (user AuthorizedUser) AuthorizeLDAPGroups(ldapGroups []string) error {
	// if admin, let um in
	if user.IsAdmin() {
		return nil
	}

	groupSet := make(map[string]struct{})
	for _, ldapGroup := range ldapGroups {
		groupSet[ldapGroup] = struct{}{}
	}

	var ok = false
	for _, group := range user.Groups {
		if _, ok = groupSet[group]; ok {
			return nil
		}
	}
	return InvalidLDAPGroupsError{User: user.User, Groups: ldapGroups}
}

// IsAdmin returns true if given slice contains an AdminGroup
func (user AuthorizedUser) IsAdmin() bool {

	return user.authorizer.IsAdmin(user.User)
}

// AuthorizeNamespace validates that a user has access to a namespace
func (user AuthorizedUser) AuthorizeNamespace(dynamoNamespace string) error {
	if !namespaceInGroups(dynamoNamespace, ldapGroupsToDynamoNamespaces(user.Groups)) {
		return ForbiddenNamespace{User: user.User, Namespace: dynamoNamespace}
	}
	return nil
}

// helper method to unmarshal json body into `v`
func unmarshalJSONRequest(r *http.Request, v interface{}) error {
	if r.Body == nil || r.ContentLength == 0 {
		return errors.New("unable to marshal nil request body")
	}
	if r.ContentLength < 0 {
		return errors.New("unable to marshal request body with unknown length")
	}

	var err error
	b := make([]byte, r.ContentLength)
	n, err := io.ReadFull(r.Body, b)
	if err != nil {
		return err
	}
	if int64(n) != r.ContentLength {
		return fmt.Errorf("%d bytes read from body, expected %d", n, r.ContentLength)
	}

	err = json.Unmarshal(b, v)
	if err != nil {
		return err
	}
	return nil
}

// CreateRole creates an IAM role and associated policy document
// POST /roles/
func (pg *IAMPolicyGenerator) CreateRole(w http.ResponseWriter, r *http.Request) {
	user, err := getGuardianUser(r)
	if err != nil {
		pg.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	request := new(policy.CreateRoleRequest)

	err = unmarshalJSONRequest(r, request)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	err = r.Body.Close()
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	request.SecretKeys = cleanInputStrings(request.SecretKeys)
	request.AllowedArns = cleanInputStrings(request.AllowedArns)

	// Validate input
	switch {
	case request.Name == "":
		pg.apiError(manager.InvalidRequestError{Message: errors.New("name field empty in request")}, w, r)
		return
	case len(request.Owners) == 0:
		pg.apiError(manager.InvalidRequestError{Message: errors.New("owners field empty in request")}, w, r)
		return
	case len(request.AllowedArns) == 0:
		pg.apiError(manager.InvalidRequestError{Message: errors.New("allowedARNs field empty in request")}, w, r)
		return
	case len(request.SecretKeys) == 0:
		pg.apiError(manager.InvalidRequestError{Message: errors.New("SecretKeys field empty in request")}, w, r)
		return
	}

	response, err := pg.IAMPolicyGenerator.Create(
		request,
		pg.NewAuthorizedUser(user),
		pg.clients.mgr,
	)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	err = jsonResponseStatus(response, w, http.StatusCreated)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	roleName := request.Name

	if pg.config.Changelog.Enabled {
		description := fmt.Sprintf("%s %s", r.Method, roleName)
		command := "created IAM role/policy " + roleName
		additionalData := fmt.Sprintf("username=%s,roleName=%s", user.CN, roleName)
		pg.clients.changelog.logInfoChangeEvent(r, roleName, description, user.CN, command, additionalData)
	}
	return
}

// UpdateRole updates IAM role and policy
// POST /roles/{role}
func (pg *IAMPolicyGenerator) UpdateRole(w http.ResponseWriter, r *http.Request) {
	user, err := getGuardianUser(r)
	if err != nil {
		pg.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	roleName, err := url.QueryUnescape(vars["role"])
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	request := new(policy.PutRoleRequest)
	err = unmarshalJSONRequest(r, request)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}
	err = r.Body.Close()
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	request.RoleName = roleName

	request.SecretKeys = cleanInputStrings(request.SecretKeys)
	request.AllowedArns = cleanInputStrings(request.AllowedArns)

	response, err := pg.IAMPolicyGenerator.Put(request, pg.NewAuthorizedUser(user), pg.clients.mgr)

	if err != nil {
		if awsErr, ok := err.(awserr.Error); ok {
			if awsErr.Code() == "AccessDenied" {
				pg.apiErrorStatus(w, r, err, http.StatusNotFound)
				return
			}
		}

		pg.apiError(err, w, r)
		return
	}

	err = jsonResponse(response, w)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	if pg.config.Changelog.Enabled {
		description := fmt.Sprintf("%s %s", r.Method, roleName)
		command := "updated IAM role/policy " + roleName
		additionalData := fmt.Sprintf("username=%s,roleName=%s", user.CN, roleName)
		pg.clients.changelog.logInfoChangeEvent(r, roleName, description, user.CN, command, additionalData)
	}

	return
}

// ListRoles returns IAM roles and policies
// GET /roles
func (pg *IAMPolicyGenerator) ListRoles(w http.ResponseWriter, r *http.Request) {
	_, err := getGuardianUser(r)
	if err != nil {
		pg.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	roles, err := pg.IAMPolicyGenerator.List()
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	err = jsonResponse(roles, w)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}
	return
}

// GetRole returns an IAM role and policy
// GET /roles/{role}
func (pg *IAMPolicyGenerator) GetRole(w http.ResponseWriter, r *http.Request) {
	_, err := getGuardianUser(r)
	if err != nil {
		pg.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	roleName, err := url.QueryUnescape(vars["role"])
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	role, err := pg.IAMPolicyGenerator.Get(roleName)
	if err != nil {
		if awsErr, ok := err.(awserr.Error); ok {
			if awsErr.Code() == "AccessDenied" {
				pg.apiErrorStatus(w, r, err, http.StatusNotFound)
				return
			}
		}
		pg.apiError(err, w, r)
		return
	}

	err = jsonResponse(role, w)
	if err != nil {
		pg.apiError(err, w, r)
		return
	}
	return
}

// DeleteRole removes an IAM role/policy
// DELETE /roles/{role}
func (pg *IAMPolicyGenerator) DeleteRole(w http.ResponseWriter, r *http.Request) {
	user, err := getGuardianUser(r)
	if err != nil {
		pg.apiErrorStatus(w, r, err, http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	roleName, err := url.QueryUnescape(vars["role"])
	if err != nil {
		pg.apiError(err, w, r)
		return
	}

	err = pg.IAMPolicyGenerator.Delete(roleName, pg.NewAuthorizedUser(user))

	if err != nil {
		switch newErr := err.(type) {
		case awserr.Error:

			if newErr.Code() == "AccessDenied" {
				pg.apiErrorStatus(w, r, newErr, http.StatusNotFound)
				return
			}
			pg.apiError(newErr, w, r)
			return
		case policy.InvalidRoleOwnerError:
			if newErr == policy.RoleOwnerDoesNotExist {
				// ignore it
				err = nil
			} else {
				// MS: dislike that I can't goto default or fallthrough in an if/else
				// but w/e =(
				pg.apiError(newErr, w, r)
				return
			}
		default:
			pg.apiError(newErr, w, r)
			return
		}
	}
	w.WriteHeader(http.StatusNoContent)
	if pg.config.Changelog.Enabled {
		description := fmt.Sprintf("%s %s", r.Method, roleName)
		command := "deleted IAM role/policy " + roleName
		additionalData := fmt.Sprintf("username=%s,roleName=%s", user.CN, roleName)
		pg.clients.changelog.logInfoChangeEvent(r, roleName, description, user.CN, command, additionalData)
	}
	return
}
