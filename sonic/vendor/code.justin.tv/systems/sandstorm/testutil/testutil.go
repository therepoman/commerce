package testutil

import (
	"fmt"
	"io/ioutil"

	"code.justin.tv/systems/sandstorm/resource"

	"github.com/hashicorp/hcl"
	uuid "github.com/satori/go.uuid"
)

// Config for testing
type Config struct {
	Sandstorm struct {
		Profile               string
		TableName             string
		KeyID                 string
		DDBStatsdHostPort     string `hcl:"ddb_statsd_host_port"`
		DDBStatsdPrefix       string `hcl:"ddb_statsd_prefix"`
		RoleArn               string
		Region                string
		InventoryAdminRoleARN string
		InventoryRoleARN      string
		InventoryStatusURL    string `hcl:"inventory_status_url"`
		ServiceName           string `hcl:"service_name"`
		TopicArn              string
	}
	Changelog struct {
		Enabled bool `hcl:"enabled"`
	}
	Environment string `hcl:"environment"`
}

// GetRandomSecretName gives a random secret for usage in tests.
// Please delete after test.
func GetRandomSecretName() string {
	return GetRandomSecretNameWithPrefix("system/testService/testing")
}

// GetRandomSecretNameWithPrefix gives a random secret for usage in tests.
// Please delete after test.
func GetRandomSecretNameWithPrefix(prefix string) string {
	return fmt.Sprintf("%s/%s", prefix, uuid.NewV4().String())
}

// GetRandomGroupName gives a random group name for usage in tests.
// Please delete after test.
func GetRandomGroupName() string {
	return fmt.Sprintf("sandstorm-api-test-group-%s", uuid.NewV4().String())
}

// LoadTestConfig gets configuration used for tests
func LoadTestConfig() (*Config, error) {
	return LoadTestConfigFromFile("../test.hcl")
}

// LoadTestConfigFromFile gets configuration used for tests from filename
func LoadTestConfigFromFile(filename string) (*Config, error) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	var out Config
	err = hcl.Decode(&out, string(b))
	if err := hcl.Decode(&out, string(b)); err != nil {
		return nil, err
	}
	err = mergeConfigForEnvironment(&out)
	if err != nil {
		return nil, err
	}
	return &out, nil
}

// mergeConfigForEnvironment merges aws resource config with provided config object.
// aws resource config are stored in and are fetched using the environment name,
func mergeConfigForEnvironment(cfg *Config) (err error) {
	res, err := resource.GetConfigForEnvironment(cfg.Environment)
	if err != nil {
		return
	}
	//Region needed to build aws config in inventory/heartbeat/client_integration_test.go
	cfg.Sandstorm.Region = res.AwsRegion

	// RoleArn requied to correctly provide AWSConfig in manager::createTestManager
	cfg.Sandstorm.RoleArn = res.RoleArn
	cfg.Sandstorm.InventoryAdminRoleARN = res.InventoryAdminRoleARN
	cfg.Sandstorm.ServiceName = "sandstorm-manager-testing"
	return
}
