package agent

import (
	"bufio"

	"code.justin.tv/systems/sandstorm/manager"

	"bytes"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"os/exec"
	"syscall"
	templater "text/template"
	"time"

	log "github.com/sirupsen/logrus"
	homedir "github.com/mitchellh/go-homedir"
)

func init() {
	// Give a truly random seed so that a cluster doesn't all random splay to the
	// same value.
	// From: http://stackoverflow.com/a/12321192/2051788
	rand.Seed(time.Now().UnixNano())
}

const (
	statusStartup = "Startup"
	statusRestart = "Restart Required"
	statusFail    = "Failure"
	statusSuccess = "Success"
)

type tmplFunc func(s string) (value string, err error)

// Template is the structure of a secret templating rule as found
// in a configuration file
type Template struct {
	Source        string
	Content       string
	Destination   string
	UserID        int
	GroupID       int
	FileMode      os.FileMode
	Command       string
	CommandStatus *CommandStatus
	Secrets       map[string]*manager.Secret
	Dirty         bool             `json:"-"`
	Error         string           `json:"error,omitempty"`
	FileComparer  FileStatComparer `json:"-"`
}

// FileStatComparer compares file mode, owner and groups, returns an error if mismatch
type FileStatComparer func(new, old *Template) bool

// CommandStatus holds status of a template command
type CommandStatus struct {
	Status   string
	ExitCode int
	Stdout   string
	Stderr   string
}

// NewTemplate initializes and returns a template object
func NewTemplate(source string, destination string, command string) *Template {
	return &Template{
		Source:      source,
		Destination: destination,
		Command:     command,
		UserID:      DefaultUserID,
		GroupID:     DefaultGroupID,
		FileMode:    os.FileMode(DefaultFilePermissions),
		Secrets:     map[string]*manager.Secret{},
		Dirty:       true,
		CommandStatus: &CommandStatus{
			Status: statusStartup,
		},
	}
}

// Initialize handles loading a template's content and other state regarding a run
func (t *Template) Initialize() error {
	source, err := homedir.Expand(t.Source)
	if err != nil {
		err = nil
		source = t.Source
	}

	contentBytes, err := ioutil.ReadFile(source)
	if err != nil {
		return fmt.Errorf(
			"For template source '%s', error: %s",
			t.Source,
			err.Error(),
		)
	}

	t.Content = string(contentBytes)
	return t.parseSecrets()
}

// UpsertSecret adds a secret for the template to track, or overwrites the
// existing one that matches the secret name.
func (t *Template) UpsertSecret(secret *manager.Secret) {
	oldSecret, alreadyExists := t.Secrets[secret.Name]

	// simply add the secret if we haven't seen it before
	if !alreadyExists {
		t.Secrets[secret.Name] = secret
		return
	}

	// When a template is first initialized, the value of it's
	// secret Plaintexts is empty. If this is the case, and we are
	// initializing these values, don't mark as dirty
	switch {
	case secret.UpdatedAt != oldSecret.UpdatedAt:
		t.Dirty = true
		fallthrough
	case len(oldSecret.Plaintext) == 0:
		t.Secrets[secret.Name] = secret
	}
}

// SetFileMode sets file permissions based on os
func (t *Template) SetFileMode(mode uint64) {
	t.FileMode = os.FileMode(mode)
}

// ContainsSecrets checks to see if the template contains one or more secret in
// the provided list.
func (t *Template) ContainsSecrets(secrets []*manager.Secret) bool {
	for _, secret := range secrets {
		if _, secretExists := t.Secrets[secret.Name]; secretExists {
			return true
		}
	}

	return false
}

// Output populates the template's content with secrets and write the result
// to file at the template's destination
func (t *Template) Output() error {

	log.Infof("Outputting template for source: %s", t.Source)

	goTmpl := templater.New(t.Source).Funcs(t.outputMap(nil))
	goTmpl, err := goTmpl.Parse(t.Content)
	if err != nil {
		return err
	}

	destination, err := homedir.Expand(t.Destination)
	if err != nil {
		err = nil
		destination = t.Destination
	}

	f, err := createFile(destination, t.FileMode)
	if err != nil {
		return err
	}

	err = os.Chown(destination, t.UserID, t.GroupID)
	if err != nil {
		return err
	}

	defer handleFileClose(f)

	w := bufio.NewWriter(f)
	err = goTmpl.Execute(w, nil)
	if err != nil {
		return err
	}

	return w.Flush()
}

// AttemptCommand waits a random time between 0-splay and then attempts to
// gracefully execute the templates restart command
func (t *Template) AttemptCommand(splay int) error {

	t.CommandStatus.Status = statusRestart

	if splay > 0 {
		randSplay := rand.Int63n(int64(splay))
		log.Infof("waiting %d due to splay window before running restart command: %s", randSplay, t.Command)
		time.Sleep(time.Duration(randSplay) * time.Second)
	}

	stdout, stderr, exitCode, err := execCommand(t.Command)
	t.CommandStatus.Stdout = stdout
	t.CommandStatus.ExitCode = exitCode
	t.CommandStatus.Stderr = stderr
	if err != nil {
		t.CommandStatus.Status = statusFail
		return fmt.Errorf("command '%s' failed with error: %s: %s", t.Command, err.Error(), stderr)
	}

	t.CommandStatus.Status = statusSuccess
	log.Debugf("Successful command with output: \n %s", string(stdout))
	return nil
}

// outputMap handles formatting a template to ouput with populated secrets
func (t *Template) outputMap(agent *Agent) templater.FuncMap {
	return templater.FuncMap{
		"key": func(secretName string) (value string, err error) {
			secret, exists := t.Secrets[secretName]
			if !exists {
				err = fmt.Errorf("no secret %s for template %s", secretName, t.Source)
				return
			}

			value = string(secret.Plaintext)
			return
		},
	}
}

// inputMap is used for parsing templates into the Agent
func (t *Template) inputMap() templater.FuncMap {
	return templater.FuncMap{
		"key": func(key string) (value string, err error) {
			t.UpsertSecret(&manager.Secret{Name: key})
			return
		},
	}
}

func (t *Template) parseSecrets() error {
	tmpl, err := templater.New(t.Source).Funcs(t.inputMap()).Parse(t.Content)
	if err != nil {
		return err
	}

	// Use nil because the lookup func handles the lookup for you
	return tmpl.Execute(ioutil.Discard, nil)
}

// Stat returns syscall.Stat_t for template destination file. If there's an
// error, return nil
func Stat(t *Template) *syscall.Stat_t {
	stat := new(syscall.Stat_t)
	err := syscall.Stat(t.Destination, stat)
	if err != nil {
		log.Error(err.Error())
		return nil
	}
	return stat
}

// CompareFileStat compares desired file ownership and mode with file ownership
// and mode of file on disk, returns true if equal, false if not.
func CompareFileStat(new, old *Template) bool {
	if !compareOwner(new, old) || !compareGroup(new, old) || !compareFileMode(new, old) {
		return false
	}
	return true
}

// compareOwner compares the desired owner to the owner of the output file on
// disk. Returns false if mismatch, true if equal.
func compareOwner(new, old *Template) bool {
	stat := Stat(new)
	if stat == nil {
		return false
	}

	if uint32(new.UserID) == stat.Uid {
		return true
	}
	log.Errorf("template %s owned by %d, wants %d", new.Destination, stat.Uid, uint32(new.UserID))
	return false
}

// compareGroup compares group IDs of templates, returns false if not equal,
// true if equal.
func compareGroup(new, old *Template) bool {
	stat := Stat(new)
	if stat == nil {
		return false
	}

	if uint32(new.GroupID) == stat.Gid {
		return true
	}
	log.Errorf("template %s has group %d, wants %d", new.Destination, old.GroupID, new.GroupID)
	return false
}

// compareFileMode compares desired file mode of template to file mode of
// file on disk. Returns false if not equal, true if equal.
func compareFileMode(new, old *Template) bool {
	info, err := os.Stat(old.Destination)
	if err != nil {
		return false
	}
	if info.Mode() == new.FileMode {
		return true
	}
	log.Errorf("template %s has mode %d, wants %d", new.Destination, info.Mode(), uint32(new.FileMode))
	return false
}

// CompareFileStat calls t.FileComparer if available, otherwise calls
// CompareFileStat to compare ownership and file mode
func (t *Template) CompareFileStat(template *Template) bool {
	if t.FileComparer == nil {
		return CompareFileStat(t, template)
	}
	return t.FileComparer(t, template)
}

// IsEqual does a deep equals check to determine whether two templates
// are equivalent
func (t *Template) IsEqual(template *Template) bool {

	if template.Source != t.Source {
		return false
	}

	if template.Destination != t.Destination {
		return false
	}

	if template.Content != t.Content {
		return false
	}

	if template.Command != t.Command {
		return false
	}

	if !t.CompareFileStat(template) {
		return false
	}

	tKeys := map[string]bool{}
	for key := range t.Secrets {
		tKeys[key] = true
	}

	if len(tKeys) != len(template.Secrets) {
		return false
	}

	for key := range template.Secrets {
		if _, ok := tKeys[key]; !ok {
			return false
		}
	}

	return true
}

// copies secrets from template to t, and returns set of secrets in t not in templates
func (t *Template) synchronizeSecrets(template *Template, newSecretsSet map[string]bool) {
	for secretName := range t.Secrets {
		// Mark secret as new secret
		if _, ok := template.Secrets[secretName]; !ok {
			newSecretsSet[secretName] = true
		} else {
			// Copy old secret from memory to new set of templates
			t.Secrets[secretName] = template.Secrets[secretName]
		}
	}
}

// execCommand if one is specified and return stdout, stderr, exitCode, and errors.
func execCommand(command string) (stdout string, stderr string, exitCode int, err error) {
	if command == "none" || command == "" {
		return "", "", 0, nil
	}

	var cmd *exec.Cmd

	cmd = exec.Command("bash", "-c", command)
	var outBuffer bytes.Buffer
	var stderrBuffer bytes.Buffer
	cmd.Stdout = &outBuffer
	cmd.Stderr = &stderrBuffer
	err = cmd.Run()
	if exiterr, ok := err.(*exec.ExitError); ok {
		if status, ok := exiterr.Sys().(syscall.WaitStatus); ok {
			exitCode = status.ExitStatus()
		}
	}
	stdout = outBuffer.String()
	stderr = stderrBuffer.String()
	return
}

// copyTemplate returns a deep copy of a template that can
// be modified without affecting the original
func copyTemplate(template *Template) *Template {
	newTemplate := NewTemplate(template.Source, template.Destination, template.Command)
	newTemplate.Content = template.Content
	newTemplate.FileMode = template.FileMode
	newTemplate.UserID = template.UserID
	newTemplate.GroupID = template.GroupID
	newTemplate.CommandStatus = template.CommandStatus

	for _, secret := range template.Secrets {
		copySecret := &manager.Secret{}
		*copySecret = *secret
		newTemplate.UpsertSecret(copySecret)
	}

	return newTemplate
}
