package agent

// loadQueueConfig loads queue config data from file,
import (
	"errors"
	"os"

	"code.justin.tv/systems/sandstorm/queue"
	"github.com/go-ini/ini"
)

const (
	queueArnKeyName = "QueueArn"
	queueURLKeyName = "QueueURL"
)

func loadQueueConfig(queueConfigPath string) (queueArn string, queueURL string, err error) {
	fh, err := ini.Load(queueConfigPath)
	if err != nil {
		return "", "", errors.New("error loading agent queue config: " + err.Error())
	}

	section := fh.Section(sectionHeader)
	arn, err := section.GetKey(queueArnKeyName)
	if err != nil {
		return "", "", errors.New("error reading queueArn from config file: " + err.Error())
	}
	url, err := section.GetKey(queueURLKeyName)
	if err != nil {
		return "", "", errors.New("error reading queueURL from config file: " + err.Error())
	}
	return arn.String(), url.String(), nil
}

// writeQueueConfig writes the queue arn and queue url to a file on disk
// located at config.QueueConfigPath()
func writeQueueConfig(cfg queue.Config) (err error) {
	f, err := os.Create(cfg.QueueConfigFullPath())
	if err != nil {
		return
	}

	defer func() {
		closeErr := f.Close()
		if err != nil {
			err = errors.New("error closing queue config file: " + closeErr.Error())
		}
	}()

	iniFile := ini.Empty()
	section, err := iniFile.NewSection(sectionHeader)
	if err != nil {
		return
	}

	_, err = section.NewKey(queueArnKeyName, cfg.QueueArn)
	if err != nil {
		return
	}
	_, err = section.NewKey(queueURLKeyName, cfg.QueueURL)
	if err != nil {
		return
	}

	_, err = iniFile.WriteTo(f)
	if err != nil {
		return
	}

	err = f.Sync()
	return
}
