package agent

import (
	"encoding/json"
	"net/http"
	"time"

	"code.justin.tv/systems/sandstorm/manager"
	log "github.com/sirupsen/logrus"

	"code.justin.tv/common/golibs/bininfo"
	"github.com/gorilla/mux"
)

// HealthProfile contains vital statistics for the current running agent
type HealthProfile struct {
	RecentError string

	// error not caused by user, i.e. aws services being unavailable
	SystemError *bool `json:"system_error,omitempty"`

	LastUpdate            []*manager.Secret
	LastSucessfulUpdate   []*manager.Secret
	ApproxMessagesInQueue int64
	QueueARN              string
	QueueURL              string
	TemplateStatus        *StateFile
	SecretStatus          []*manager.Secret
	GitCommit             string `json:"git_commit"`
	Timestamp             int64
	BlockedSecrets        []BlockedSecret `json:"blocked_secrets"`
}

// BlockedSecret contains info about blocked secrets
type BlockedSecret struct {
	Secret       string `json:"secret"`
	ErrorMessage string `json:"error"`
}

// VersionInfo contains info about the current running version
type VersionInfo struct {
	GitCommit string `json:"git_commit"`
}

// BuildRouter creates a new http server router with the endpoints we care about
func (a *Agent) BuildRouter() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/health", a.HealthCheck).Name("healthcheck")
	r.HandleFunc("/version", a.versionInfo).Name("version")
	r.NotFoundHandler = http.HandlerFunc(a.HealthCheck)
	return r
}

func (a *Agent) versionInfo(w http.ResponseWriter, r *http.Request) {
	version := &VersionInfo{
		GitCommit: bininfo.Revision(),
	}
	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err := encoder.Encode(version)

	if err != nil {
		a.handleHTTPError(w, err)
	}
}

func (a *Agent) buildHealthProfile() (*HealthProfile, error) {
	var errorMsg string
	var systemError *bool
	approxMessages, err := a.Queue.ApproximateNumberOfMessages()
	if err != nil {
		if isSystemError(err) {
			a.setProxyUse(true)
		}
		log.Error(err)
		errorMsg = err.Error()
		se := true
		systemError = &se
		err = nil
	}

	if a.State.RecentError != nil {
		errorMsg = a.State.RecentError.Error()
		se := true
		systemError = &se
	}

	if re, ok := a.State.RecentError.(RecentError); ok {
		se := re.SystemError()
		systemError = &se
	}

	profile := &HealthProfile{
		RecentError:           errorMsg,
		SystemError:           systemError,
		LastUpdate:            secretStats(a.State.LastUpdate),
		LastSucessfulUpdate:   secretStats(a.State.LastSuccessfulUpdate),
		ApproxMessagesInQueue: approxMessages,
		QueueARN:              a.Queue.Config.QueueArn,
		QueueURL:              a.Queue.Config.QueueURL,
		TemplateStatus:        newStateFile(a.State.Templates),
		GitCommit:             bininfo.Revision(),
		SecretStatus:          secretStats(a.GetAllSecrets()),
		Timestamp:             time.Now().Unix(),
		BlockedSecrets:        make([]BlockedSecret, 0, len(a.State.BlockedSecrets)),
	}

	for k, v := range a.State.BlockedSecrets {
		bs := BlockedSecret{
			Secret:       k,
			ErrorMessage: v,
		}
		profile.BlockedSecrets = append(profile.BlockedSecrets, bs)
	}

	return profile, nil
}

// HealthCheck provides the current status of the agent
// and the outcome of the most recent action it performed
func (a *Agent) HealthCheck(w http.ResponseWriter, r *http.Request) {
	profile, err := a.buildHealthProfile()
	if err != nil {
		log.Errorf("error building health profile: %s", err.Error())
		a.handleHTTPError(w, err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json, err := json.MarshalIndent(profile, "", " ")
	if err != nil {
		a.handleHTTPError(w, err)
		return
	}
	_, err = w.Write(json)
	if err != nil {
		log.Errorf("Health check error: %s", err.Error())
		return
	}
}

func (a *Agent) handleHTTPError(w http.ResponseWriter, err error) {
	log.Errorf("Health check error: %s", err.Error())
	http.Error(w, err.Error(), http.StatusInternalServerError)
}

// takes an array of secrets and converts them into a consumable format that avoids
// presenting any sensitive data
func secretStats(secrets []*manager.Secret) []*manager.Secret {
	stats := make([]*manager.Secret, len(secrets))

	for i, secret := range secrets {
		copy := &manager.Secret{}
		*copy = *secret
		copy.Plaintext = nil

		stats[i] = copy
	}

	return stats
}
