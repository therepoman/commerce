package cryptorand

const (
	// ByteSetAlphaLower are all lowercase letters
	ByteSetAlphaLower = "abcdefghijklmnopqrstuvwxyz"
	// ByteSetAlphaUpper are all uppercase letters
	ByteSetAlphaUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	// ByteSetDigits are all numbers
	ByteSetDigits = "0123456789"
	// ByteSetSymbols are symbols usable in secrets
	ByteSetSymbols = "!#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"
)
