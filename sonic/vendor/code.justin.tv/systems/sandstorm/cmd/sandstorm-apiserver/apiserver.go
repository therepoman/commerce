package main

import (
	"flag"
	"log"
	"os"
	"net/http"

	"code.justin.tv/systems/sandstorm/apiserver"
)

var configPath string

func init() {
	flag.StringVar(&configPath, "config", os.Getenv("SANDSTORM_CONFIG"), "Path to config file")
	flag.Parse()
}

func main() {

	cfg, err := apiserver.LoadConfig(configPath)
	if err != nil {
		log.Fatalln(err)
	}
	svc, err := apiserver.New(cfg)
	if err != nil {
		log.Fatalln(err)
	}
	log.Fatalln(http.ListenAndServe(cfg.HTTP.Listen, svc))
}
