package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"time"

	"code.justin.tv/common/golibs/bininfo"
	"code.justin.tv/systems/sandstorm/manager"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
	"github.com/codegangsta/cli"
)

// XXX move this to a build script later, hardcoded manually since
// manta doesn't support access to tags
const version = "v1.0.3"

var environment, roleARN, profile string
var class int
var doNotBroadcast, noNewLine bool

func buildManager() (*manager.Manager, error) {
	awsConfig := &aws.Config{Region: aws.String("us-west-2")}

	svc := sts.New(session.New(awsConfig))
	callerIdentity, err := svc.GetCallerIdentity(
		&sts.GetCallerIdentityInput{},
	)
	if err != nil {
		return nil, err
	}
	callerArn := aws.StringValue(callerIdentity.Arn)
	if roleARN != "" {
		sess, err := session.NewSessionWithOptions(session.Options{
			Config:  *awsConfig,
			Profile: profile,
		})
		if err != nil {
			return nil, err
		}

		stsclient := sts.New(sess)
		arp := &stscreds.AssumeRoleProvider{
			Duration:     900 * time.Second,
			ExpiryWindow: 10 * time.Second,
			RoleARN:      roleARN,
			Client:       stsclient,
		}

		credentials := credentials.NewCredentials(arp)
		awsConfig.WithCredentials(credentials)
	}

	return manager.New(manager.Config{
		AWSConfig:   awsConfig,
		ServiceName: "sandstorm-cli",
		ActionUser:  callerArn,
		Environment: environment,
	}), nil
}

func getSecretName(c *cli.Context) (string, error) {
	if !c.Args().Present() {
		return "", cli.NewExitError("Missing secret name", 1)
	}
	return c.Args().First(), nil
}

func sandstormGet(c *cli.Context) error {
	if !c.Args().Present() {
		return cli.NewExitError("Missing secret name", 1)
	}

	secretName := c.Args().First()
	version := c.Int64("version")
	mgr, err := buildManager()
	if err != nil {
		return cli.NewExitError(err.Error(), 1)
	}

	var (
		secret *manager.Secret
	)
	if version == 0 {
		secret, err = mgr.Get(secretName)
	} else {
		secret, err = mgr.GetVersion(secretName, version)
	}
	if err != nil {
		return cli.NewExitError(err.Error(), 1)
	}

	if secret == nil {
		return cli.NewExitError(fmt.Sprintf("Secret '%s' not found.", secretName), 1)
	}

	if noNewLine {
		fmt.Print(string(secret.Plaintext))
	} else {
		fmt.Println(string(secret.Plaintext))
	}

	err = mgr.CleanUp()
	if err != nil {
		_, err = os.Stderr.WriteString(fmt.Sprintf("Error during cleanup: %s", err.Error()))
	}

	if err != nil {
		return cli.NewExitError(fmt.Sprintf("Error when writing to stderr: %s", err), 1)
	}

	return nil
}

func sandstormGetVersions(c *cli.Context) error {
	secretName, err := getSecretName(c)
	if err != nil {
		return err
	}

	mgr, err := buildManager()
	if err != nil {
		return cli.NewExitError(err.Error(), 1)
	}

	versions, err := mgr.GetVersionsEncrypted(secretName, 10, 0)

	if err != nil {
		return cli.NewExitError(err.Error(), 1)
	}

	if len(versions.Secrets) == 0 {
		return cli.NewExitError(fmt.Sprintf("Secret '%s' not found.", secretName), 1)
	}

	for _, secret := range versions.Secrets {
		fmt.Printf("%d\n", secret.UpdatedAt)
	}

	return nil
}

func sandstormGetEncrypted(c *cli.Context) error {
	if !c.Args().Present() {
		return cli.NewExitError("Missing secret name", 1)
	}

	secretName := c.Args().First()

	mgr, err := buildManager()
	if err != nil {
		return cli.NewExitError(err.Error(), 1)
	}

	secret, err := mgr.GetEncrypted(secretName)
	if err != nil {
		return cli.NewExitError(err.Error(), 1)
	}
	if secret == nil {
		return cli.NewExitError(fmt.Sprintf("Secret '%s' not found.", secretName), 1)
	}

	printSecret(secret)
	return nil
}

// @deprecated use create/update instead
func sandstormPut(c *cli.Context) error {

	if !c.Args().Present() {
		return cli.NewExitError("Missing secret name", 1)
	}

	secretName := c.Args().First()
	secretValue := c.Args().Get(1)
	var bytes []byte
	var err error
	if secretValue == "" {
		if !stdinIsTTY() {
			_, err := os.Stderr.WriteString("Listening on STDIN for secret value.\n")
			if err != nil {
				return cli.NewExitError(err.Error(), 1)
			}
		}
		bytes, err = ioutil.ReadAll(os.Stdin)
		if err != nil {
			return cli.NewExitError(err.Error(), 1)
		}
	} else {
		bytes = []byte(secretValue)
	}

	mgr, err := buildManager()
	if err != nil {
		return cli.NewExitError(err.Error(), 1)
	}

	if class != 0 && (class < 1 || class > 4) {
		return cli.NewExitError("arg: class can only take values 1 to 4", 1)
	}

	secret := &manager.Secret{
		Name:           secretName,
		Plaintext:      bytes,
		DoNotBroadcast: doNotBroadcast,
		// default these values and update if sent below
		Class: manager.ClassDefault,
	}

	if class != 0 {
		secret.Class = manager.SecretClass(class)
	}

	err = mgr.Put(secret)
	if err != nil {
		return cli.NewExitError(err.Error(), 1)
	}

	fmt.Printf("Put secret %s\n", secretName)
	return nil
}

func sandstormRevert(c *cli.Context) error {
	secretName := c.Args().Get(0)
	version, err := strconv.Atoi(c.Args().Get(1))
	if err != nil {
		return cli.NewExitError(fmt.Sprintf("Version id must be an integer, got %v.", version), 1)
	}
	mgr, err := buildManager()
	if err != nil {
		return cli.NewExitError(err.Error(), 1)
	}

	return mgr.Revert(secretName, int64(version))
}

func sandstormUpdate(c *cli.Context) error {
	return sandstormCreateOrUpdate(c, false)
}

func sandstormCreate(c *cli.Context) error {
	return sandstormCreateOrUpdate(c, true)
}

func sandstormCreateOrUpdate(c *cli.Context, createSecret bool) error {
	if !c.Args().Present() {
		return cli.NewExitError("Missing secret name", 1)
	}

	secretName := c.Args().First()
	secretValue := c.Args().Get(1)
	autogenerate := c.Int("autogenerate")

	if createSecret && secretValue == "" && autogenerate == 0 {
		return cli.NewExitError("Missing secret value", 1)
	}

	if secretValue != "" && autogenerate > 0 {
		return cli.NewExitError("--autogenerate and [secretValue] cannot be sent together.", 1)
	}

	if class != 0 && (class < 1 || class > 4) {
		return cli.NewExitError("arg: 'class' can only take values 1 to 4", 1)
	}
	mgr, err := buildManager()
	if err != nil {
		return cli.NewExitError(err.Error(), 1)
	}

	secret := &manager.Secret{
		Name:           secretName,
		Plaintext:      []byte(secretValue),
		DoNotBroadcast: doNotBroadcast,
		// default these values and update if sent below
		Class: manager.ClassDefault,
	}

	if class != 0 {
		secret.Class = manager.SecretClass(class)
	}

	if autogenerate > 0 {
		err := secret.FillPlaintext(&manager.FillPlaintextRequest{
			Length: autogenerate,
		})
		if err != nil {
			return cli.NewExitError(err.Error(), 1)
		}
	}

	var actionVerb string
	if createSecret {
		actionVerb = "Created"
		err = mgr.Post(secret)
	} else {
		actionVerb = "Updated"

		patchInput := &manager.PatchInput{
			Name:           secret.Name,
			Plaintext:      secret.Plaintext,
			DoNotBroadcast: &doNotBroadcast,
		}

		if class != 0 {
			cls := manager.SecretClass(class)
			patchInput.Class = &cls
		}

		err = mgr.Patch(patchInput)
	}

	if err != nil {
		return cli.NewExitError(err.Error(), 1)
	}

	fmt.Printf("%s secret %s\n", actionVerb, secretName)
	return nil
}

func sandstormCopy(c *cli.Context) (err error) {
	if len(c.Args()) < 2 {
		return cli.NewExitError("Requires two secret names", 1)
	}

	oldSecretName := c.Args().First()
	newSecretName := c.Args().Get(1)

	mgr, err := buildManager()
	if err != nil {
		return cli.NewExitError(err.Error(), 1)
	}

	err = mgr.Copy(oldSecretName, newSecretName)
	if err != nil {
		return cli.NewExitError(err.Error(), 1)
	}
	fmt.Printf("Copied secret %s to %s\n", oldSecretName, newSecretName)
	return nil
}

func stdinIsTTY() bool {
	stat, err := os.Stdin.Stat()
	if err != nil {
		return false
	}
	return (stat.Mode() & os.ModeCharDevice) == 0
}

func printSecret(secret *manager.Secret) {
	jsonSecret, err := json.MarshalIndent(secret, "", "	")
	if err != nil {
		fmt.Println(err.Error())
	}

	fmt.Println(string(jsonSecret))
}

func getGlobalFlags() []cli.Flag {
	return []cli.Flag{
		cli.StringFlag{
			Name:        "role-arn, r",
			Usage:       "Use `ARN` for cross-account access",
			EnvVar:      "SANDSTORM_ROLE_ARN",
			Destination: &roleARN,
		},
		cli.StringFlag{
			Name:        "profile, p",
			Usage:       "Use `profile` for easy multi-account access using AWSCLI profiles",
			EnvVar:      "SANDSTORM_AWS_PROFILE",
			Destination: &profile,
		},
		cli.StringFlag{
			Name:        "environment, e",
			Usage:       "Use `environment` to use a separate sandstorm stack. Usually for sandstorm devs.",
			EnvVar:      "SANDSTORM_ENVIRONMENT",
			Destination: &environment,
		},
		cli.StringFlag{
			Name:  "table-name, t",
			Value: "sandstorm-production",
			Usage: "DEPRECIATED - use environment",
		},
		cli.StringFlag{
			Name:  "inventory-role",
			Usage: "DEPRECIATED - use environment",
		},
		cli.StringFlag{
			Name:  "inventory-url",
			Usage: "DEPRECIATED - use environment",
		},
	}
}

func getMasterKeyFlag() cli.StringFlag {
	return cli.StringFlag{
		Name:  "master-key, k",
		Value: "alias/sandstorm-production",
		Usage: "DEPRECIATED - use environment",
	}
}

func getClassFlag() cli.IntFlag {
	return cli.IntFlag{
		Name:        "class, c",
		Usage:       "Classification `LEVEL` of the secret. acceptable values: 1 -> 4, where 4 means Customer classification data.",
		Destination: &class,
	}
}

func getAutogenerateFlag() cli.Flag {
	return cli.IntFlag{
		Name:  "autogenerate",
		Usage: "Autogenerate a new plaintext secret.",
		Value: 0,
	}
}

func getDNBFlag() cli.BoolFlag {
	return cli.BoolFlag{
		Name:        "doNotBroadcast, dnb",
		Usage:       "Do not broadcast this update to agents",
		Destination: &doNotBroadcast,
	}
}

func getPutFlags() []cli.Flag {
	return append(getGlobalFlags(), []cli.Flag{getMasterKeyFlag(), getClassFlag(), getDNBFlag()}...)
}

func getRevertFlags() []cli.Flag {
	return getGlobalFlags()
}

func getUpdateFlags() []cli.Flag {
	return getFlagsForCreateOrUpdate()
}

func getCreateFlags() []cli.Flag {
	return getFlagsForCreateOrUpdate()
}

func getFlagsForCreateOrUpdate() []cli.Flag {
	return append(getGlobalFlags(), []cli.Flag{getMasterKeyFlag(), getClassFlag(), getDNBFlag(), getAutogenerateFlag()}...)
}

func getGetFlags() []cli.Flag {
	newLineFlag := cli.BoolFlag{
		Name:        "n",
		Usage:       "Don't print trailing newline",
		Destination: &noNewLine,
	}
	version := cli.Int64Flag{
		Name:  "version",
		Usage: "Get a specific version from get-versions",
		Value: 0,
	}
	return append(getGlobalFlags(), []cli.Flag{newLineFlag, version}...)
}

func getGetVersionsFlags() []cli.Flag {
	return getGlobalFlags()
}

func getGetEncryptedFlags() []cli.Flag {
	return getGlobalFlags()
}

func getCopyFlags() []cli.Flag {
	return append(getGlobalFlags(), getMasterKeyFlag())
}

func getCliSubCommands() []cli.Command {

	return []cli.Command{
		cli.Command{
			Action:    sandstormGet,
			Flags:     getGetFlags(),
			ArgsUsage: "secretName",
			Name:      "get",
			Usage:     "Fetches a secret from the Sandstorm table and outputs the plaintext secret to stdout.",
		},
		cli.Command{
			Action:    sandstormGetVersions,
			Flags:     getGetVersionsFlags(),
			ArgsUsage: "secretName",
			Name:      "get-versions",
			Usage:     "Fetches versions of a secret from the Sandstorm table and prints each timestamp to stdout.",
		},
		cli.Command{
			Action:    sandstormGetEncrypted,
			Flags:     getGetEncryptedFlags(),
			Name:      "get-encrypted",
			ArgsUsage: "secretName",
			Usage:     "Fetches a secret from the Sandstorm and prints it out to stdout.",
		},
		cli.Command{
			Action:    sandstormPut,
			Flags:     getPutFlags(),
			ArgsUsage: "secretName [secretValue]",
			Name:      "put",
			Usage:     "Deprecated use update/create instead. Reads a plaintext from standard input and stores it encrypted in the Sandstorm table",
		},
		cli.Command{
			Action:    sandstormRevert,
			Flags:     getRevertFlags(),
			ArgsUsage: "secretName version",
			Name:      "revert",
			Usage:     "Revert a secret to a previous version and don't create a new entry in the audit table. See get-versions for version ids.",
		},
		cli.Command{
			Action:    sandstormUpdate,
			Flags:     getUpdateFlags(),
			Name:      "update",
			ArgsUsage: "secretName [secretValue]",
			Usage:     "Updates an existing secret in sandstorm",
		},
		cli.Command{
			Action:    sandstormCreate,
			Flags:     getCreateFlags(),
			Name:      "create",
			ArgsUsage: "secretName [secretValue]",
			Usage:     "Create a new secret in sandstorm",
		},
		cli.Command{
			Action:    sandstormCopy,
			Flags:     getCopyFlags(),
			ArgsUsage: "sourceSecretName destinationSecretName",
			Name:      "copy",
			Usage:     "Copies an encrypted secret from Sandstorm to another secret name",
		},
	}
}

func main() {
	app := cli.NewApp()
	app.Name = "sandstorm"
	app.Usage = "command-line tool for interacting with Sandstorm secrets"
	app.Version = fmt.Sprintf("%s", bininfo.Revision())

	app.Commands = getCliSubCommands()

	err := app.Run(os.Args)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error starting app: %s\n", err.Error())
		os.Exit(1)
	}
}
