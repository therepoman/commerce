package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"path"
	"strconv"
	"syscall"

	"github.com/docopt/docopt-go"
	homedir "github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"

	"code.justin.tv/systems/sandstorm/agent"
)

const usage = `Sandstorm Agent

Usage:
	sandstorm-agent watch <config-dir> [--debug] [--pid=<path>]
	sandstorm-agent run   <config-dir> [--debug] [--template=<path> ...]  [--retry]
	sandstorm-agent simulate   <config-dir>
	sandstorm-agent status [<config-dir>]
	sandstorm-agent reload [--pid=<path>]
	sandstorm-agent -h | --help

Options:
	-h --help
	--debug            Turns on debug level logging
	--pid=<path>       Folder to place PID file in [default: /var/run/]
	--template=<path>  Run only on a specific template source (can be specified multiple times)
`

const (
	defaultDir = "/etc/sandstorm-agent/"
)

func main() {
	args, err := docopt.Parse(usage, nil, true, "", false)
	if err != nil {
		log.Fatalf("Aborting, CLI input error: %s.", err)
	}

	if v, ok := args["run"].(bool); ok && v {
		runCMD(args)
	}

	if v, ok := args["watch"].(bool); ok && v {
		watchCMD(args)
	}

	if v, ok := args["simulate"].(bool); ok && v {
		simulateCMD(args)
	}

	if v, ok := args["status"].(bool); ok && v {
		statusCMD(args)
	}

	if v, ok := args["reload"].(bool); ok && v {
		reloadCMD(args)
	}
}

// runCMD performs a single sandstorm run and then exits
func runCMD(args map[string]interface{}) {

	a, err := agentFromArgs(args, true, false)
	if err != nil {
		cmdError(args, a, err, true)
	}

	err = a.Run()
	if err != nil {
		cmdError(args, a, err, true)
	}
}

// watchCMD runs sandstorm and then polls for additional secret changes and handles
// them accordingly
func watchCMD(args map[string]interface{}) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	pidPathArg, ok := args["--pid"].(string)
	if !ok {
		log.Fatalln("CLI PID configuration error")
	}

	expandedPidPathArg, err := homedir.Expand(pidPathArg)
	if err != nil {
		err = nil
		expandedPidPathArg = pidPathArg
	}

	pidPath := path.Join(expandedPidPathArg, "sandstorm-agent.pid")
	err = agent.CreatePIDFile(pidPath)
	if err != nil {
		log.Fatalf("PID creation error: %s", err.Error())
	}

	// Create channel to send signal along to handler
	exitChannel := make(chan os.Signal, 1)
	signal.Notify(exitChannel, syscall.SIGINT, syscall.SIGKILL, syscall.SIGTERM)
	// Handler goroutine blocks until it receives a signal. Once it receives a signal
	// it sanitizes before gracefully exitings
	go agent.ExitSignalHandler(exitChannel, pidPath)

	a, err := agentFromArgs(args, false, false)
	if err != nil {
		cmdError(args, a, err, true)
	}

	HUPChannel := make(chan os.Signal, 1)
	signal.Notify(HUPChannel, syscall.SIGHUP)
	go a.HUPSignalHandler(HUPChannel, pidPath)

	// Load queues, watch for changes
	go a.Watch(ctx)

	port := ":" + strconv.Itoa(a.Port)
	router := a.BuildRouter()
	log.Printf("---- Listening On %s ----", port)
	log.Fatal(http.ListenAndServe(port, router))
}

// simulteCMD templates out all tempaltes defined in conf.d with simulated values
// users can define simulated values in sandstorm_simulate.json
func simulateCMD(args map[string]interface{}) {

	a, err := agentFromArgs(args, true, true)
	if err != nil {
		cmdError(args, a, err, true)
	}

	err = a.Simulate()
	if err != nil {
		cmdError(args, a, err, true)
	}
}

// statusCMD is convenience command that hits the healthcheck endpoint for users
func statusCMD(args map[string]interface{}) {
	a, err := agentFromArgs(args, true, false)
	if err != nil {
		cmdError(args, a, err, true)
	}
	healthURL, err := url.Parse(fmt.Sprintf("http://localhost:%d/health", a.Port))
	resp, err := http.Get(healthURL.String())
	if err != nil {
		cmdError(args, a, err, true)
	}

	profileResponse := &agent.HealthProfile{}
	dec := json.NewDecoder(resp.Body)
	err = dec.Decode(&profileResponse)
	if err != nil {
		cmdError(args, a, err, true)
	}

	enc := json.NewEncoder(os.Stdout)
	enc.SetIndent("", " ")
	err = enc.Encode(profileResponse)
	if err != nil {
		cmdError(args, a, err, true)
	}
}

func reloadCMD(args map[string]interface{}) {
	var pidPath string
	if v, ok := args["--pid"].(string); ok {
		pidPath = fmt.Sprintf("%s/%s", v, "sandstorm-agent.pid")
	}

	pidFile, err := os.Open(pidPath)
	var pid int
	_, err = fmt.Fscanln(pidFile, &pid)
	if err != nil {
		cmdError(args, nil, err, true)
	}

	sandstormProcess, err := os.FindProcess(pid)
	if err != nil {
		cmdError(args, nil, err, true)
	}

	err = sandstormProcess.Signal(syscall.SIGHUP)
	if err != nil {
		cmdError(args, nil, err, true)
	}
}

// agentFromArgs configurs and then builds a new Sandstorm Agent from CLI
// args
func agentFromArgs(args map[string]interface{}, isRun bool, issimulate bool) (*agent.Agent, error) {
	var configDir string

	if v, ok := args["--debug"].(bool); ok {
		if v {
			log.SetLevel(log.DebugLevel)
		}
	}

	if v, ok := args["<config-dir>"].(string); ok {
		if expandedConfigDir, expandErr := homedir.Expand(v); expandErr == nil {
			configDir = expandedConfigDir
		}
	} else {
		configDir = defaultDir
		log.Printf("No config path specified, defaulting to %s", defaultDir)
	}

	// If using a relative path, expand it to be absolute relative to this file
	if !path.IsAbs(configDir) {
		dir, err := os.Getwd()
		if err != nil {
			return nil, fmt.Errorf("Error parsing '<config-dir>': %s", err.Error())
		}

		configDir = path.Join(dir, configDir)
	}

	if issimulate {
		a, err := agent.BuildSimulatorAgent(configDir)
		if err != nil {
			return nil, err
		}
		return a, nil
	}

	// Load the most basic config from file
	agent, err := agent.Build(configDir, true, isRun)
	if err != nil {
		return nil, err
	}

	templateArg, ok := args["--template"].([]string)
	if !ok {
		return nil, errors.New("Error parsing CLI template whitelist argument")
	}
	for i, v := range templateArg {
		expanded, err := homedir.Expand(v)
		if err == nil {
			templateArg[i] = expanded
		}
	}

	agent.TemplateWhitelist = templateArg

	if v, ok := args["--retry"].(bool); ok {
		if !v {
			agent.MaxWaitTime = 0
			log.Print("Retries disabled, please use --retry to enable retries")
		}
	}

	return agent, nil
}

func cmdError(args map[string]interface{}, a *agent.Agent, err error, exit bool) {
	log.WithFields(log.Fields{
		"command": args,
		"error":   err.Error(),
		"agent":   a,
	}).Error("Error executing sandstorm command.")

	if exit {
		os.Exit(1)
	}
}
