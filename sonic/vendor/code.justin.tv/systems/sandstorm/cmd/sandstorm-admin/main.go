package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"code.justin.tv/systems/sandstorm/manager"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
	"github.com/docopt/docopt-go"
)

const usage = `sandstorm-admin contains admin commands to manage sandstorm


Usage:
  sandstorm-admin create-table <tablename> [--provisioned-write=<iops>] [--provisioned-read=<iops>]
  sandstorm-admin check-table <tablename>
  sandstorm-admin check-key <keyid>
  sandstorm-admin create-secret <tablename> <kmsID> <secretname> <plaintext> [--class=<1..4>] [--doNotBroadcast]
  sandstorm-admin update-secret <tablename> <kmsID> <secretname> [--plaintext=plaintext] [--class=<1..4>] [--doNotBroadcast]
  sandstorm-admin delete-secret <tablename> <secretname>
  sandstorm-admin list-secrets <tablename>
  sandstorm-admin list-namespaces <tablename>
  sandstorm-admin list-namespace <tablename> <namespace>
  sandstorm-admin get-secret-encrypted <tablename> <secretname>
  sandstorm-admin get-secret <tablename> <secretname>
  sandstorm-admin get-secret-versions <tablename> <secretname>
  sandstorm-admin copy-secret <tablename> <keyid> <source> <destination>
  sandstorm-admin revert-secret <tablename> <secretname> <secretversion>

create-table creates the specified table with the optional amount of provisioned IOPS.
check-table verifies that the table is queryable.
check-key checks that the KMS key is useable.
create-secret uploads a new encrypted version of <plaintext> to Sandstorm named <secretname>
update-secret updates class and do_not_broadcast of an existing secret. Note: 'do_not_broadcast' will be set to false if the option is not provided.
delete-secret will delete the secret <secretname> from table <tablename>
list-secrets will list secrets in table <tablename>
list-namespaces lists namespaces in table <tablename>
list-namespace lists secrets under namespace <namespace> in table <tablename>
get-secret-versions lists all UpdatedAt values for a secret

Options:
  -h --help

Configuration files:
  ~/.aws/credentials

Environment Variables:
  AWS_REGION            AWS region to use
  AWS_ACCESS_KEY_ID     AWS access key (if not set in instance role or credentials file)
  AWS_SECRET_ACCESS_KEY AWS secret
  AWS_ROLE_ARN          AWS role to use
`

func getManagerConfig(args map[string]interface{}) manager.Config {
	cfg := manager.Config{}
	if val, ok := args["<tablename>"].(string); ok {
		cfg.TableName = val
	}

	awsConfig := aws.Config{Region: aws.String("us-west-2")}
	svc := sts.New(session.New(&awsConfig))
	callerIdentity, err := svc.GetCallerIdentity(
		&sts.GetCallerIdentityInput{},
	)
	if err != nil {
		log.Fatal(err)
	}

	cfg.ActionUser = aws.StringValue(callerIdentity.Arn)
	// configuration parameters in environment
	if val, ok := os.LookupEnv("AWS_ROLE_ARN"); ok {
		updateConfigWithRoleArn(&cfg, val)
	}
	return cfg
}

func updateConfigWithRoleArn(config *manager.Config, roleARN string) {
	awsConfig := aws.Config{Region: aws.String("us-west-2")}
	stsclient := sts.New(session.New(&awsConfig))
	arp := &stscreds.AssumeRoleProvider{
		Duration:     900 * time.Second,
		ExpiryWindow: 10 * time.Second,
		RoleARN:      roleARN,
		Client:       stsclient,
	}
	credentials := credentials.NewCredentials(arp)
	awsConfig.WithCredentials(credentials)
	config.AWSConfig = &awsConfig
}

func createTable(args map[string]interface{}) {
	var provisionedRead int64
	var provisionedWrite int64

	provisionedRead = 1
	provisionedWrite = 1

	if val, ok := args["--provisioned-read"].(string); ok {
		i, err := strconv.ParseInt(val, 10, 64)
		if err != nil {
			log.Fatal(err)
		}
		provisionedRead = i
	}

	if val, ok := args["--provisioned-write"].(string); ok {
		i, err := strconv.ParseInt(val, 10, 64)
		if err != nil {
			log.Fatal(err)
		}
		provisionedWrite = i
	}

	config := getManagerConfig(args)
	config.ProvisionedThroughputRead = provisionedRead
	config.ProvisionedThroughputWrite = provisionedWrite

	m := manager.New(config)
	err := m.CreateTable()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Created tables prefixed with %s with provisioned throughput %d/%d (r/w)\n", config.TableName, provisionedRead, provisionedWrite)
}

func checkTable(args map[string]interface{}) {
	config := getManagerConfig(args)
	m := manager.New(config)
	err := m.CheckTable()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Table %s is ok\n", config.TableName)
}

func checkKey(args map[string]interface{}) {
	var keyID string
	if val, ok := args["<keyid>"].(string); ok {
		keyID = val
	} else {
		log.Fatal("can't parse <keyid>")
	}
	config := getManagerConfig(args)
	config.KeyID = keyID
	m := manager.New(config)
	err := m.CheckKMS()
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Key %s is ok\n", keyID)
}

func parseSecretClass(class string) manager.SecretClass {
	intClass, err := strconv.Atoi(class)
	if err != nil {
		log.Fatal("can't parse --class, --class must be a number")
	}
	if intClass < 1 || intClass > 4 {
		log.Fatal("--class can only take values 1 to 4")
	}
	return manager.SecretClass(intClass)
}

func parseArgKey(args map[string]interface{}, key string) string {
	if val, ok := args[key].(string); ok {
		return val
	}
	log.Fatalf("can't parse %s", key)
	return ""
}

func parseSecretName(args map[string]interface{}) string {
	return parseArgKey(args, "<secretname>")
}

func parseSecretVersion(args map[string]interface{}) int64 {
	raw := parseArgKey(args, "<secretversion>")
	val, err := strconv.ParseInt(raw, 10, 64)
	if err != nil {
		log.Fatal(err)
	}
	return val
}

func parseArgsRequiredForSecrets(args map[string]interface{}, kmsID *string, secretName *string, class *manager.SecretClass, doNotBroadcast *bool) {
	if val, ok := args["<kmsID>"].(string); ok {
		*kmsID = val
	} else {
		log.Fatal("can't parse <kmsID>")
	}

	*secretName = parseSecretName(args)

	if val, ok := args["--class"].(string); ok {
		*class = parseSecretClass(val)
	}

	if val, ok := args["--doNotBroadcast"].(bool); ok {
		*doNotBroadcast = val
	}
	return
}

func parseArgsRequiredForCreateSecret(args map[string]interface{}, kmsID *string, plainText *string, secretName *string, class *manager.SecretClass, doNotBroadcast *bool) {
	parseArgsRequiredForSecrets(args, kmsID, secretName, class, doNotBroadcast)
	if val, ok := args["<plaintext>"].(string); ok {
		*plainText = val
	} else {
		log.Fatal("can't parse <plaintext>")
	}
	return
}

func parseArgsRequiredForUpdateSecret(args map[string]interface{}, kmsID *string, plainText *string, secretName *string, class *manager.SecretClass, doNotBroadcast *bool) {
	parseArgsRequiredForSecrets(args, kmsID, secretName, class, doNotBroadcast)
	if val, ok := args["--plaintext"].(string); ok {
		*plainText = val
	}
	return
}

func createSecret(args map[string]interface{}) {
	var kmsID, plainText, secretName string
	var class manager.SecretClass
	doNotBroadcast := false

	parseArgsRequiredForCreateSecret(args, &kmsID, &plainText, &secretName, &class, &doNotBroadcast)

	config := getManagerConfig(args)
	log.Printf("Attempting to create secret '%s' in table '%s' with key '%s'\n", secretName, config.TableName, kmsID)

	config.KeyID = kmsID
	m := manager.New(config)

	secret := &manager.Secret{
		Name:           secretName,
		Plaintext:      []byte(plainText),
		DoNotBroadcast: doNotBroadcast,
		Class:          manager.ClassDefault,
		ActionUser:     m.Config.ActionUser,
	}
	if class != 0 {
		secret.Class = class
	}

	err := m.Post(secret)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Created secret '%s' in table '%s' with key '%s'\n", secretName, config.TableName, kmsID)
}

func updateSecret(args map[string]interface{}) {
	var kmsID, secretName, plainText string
	doNotBroadcast := false
	var class manager.SecretClass
	parseArgsRequiredForUpdateSecret(args, &kmsID, &plainText, &secretName, &class, &doNotBroadcast)

	config := getManagerConfig(args)
	config.KeyID = kmsID
	log.Printf("Attempting to update secret '%s' in table '%s' with key '%s'\n", secretName, config.TableName, kmsID)

	m := manager.New(config)

	secret := &manager.PatchInput{
		Name:           secretName,
		DoNotBroadcast: &doNotBroadcast,
		Plaintext:      []byte(plainText),
		ActionUser:     &m.Config.ActionUser,
	}

	if class != 0 {
		secret.Class = &class
	}

	err := m.Patch(secret)
	if err != nil {
		switch err {
		case manager.ErrSecretDoesNotExist:
			err = fmt.Errorf("Secret %s not found. Please use 'POST' to create a secret' (but it didn't)", secret.Name)
		}
		log.Fatal(err)
		return
	}

	log.Printf("Updated secret '%s' in table '%s'\n", secretName, config.TableName)
}

func deleteSecret(args map[string]interface{}) {
	secretName := parseSecretName(args)

	config := getManagerConfig(args)
	m := manager.New(config)

	err := m.Delete(secretName)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Deleted secret '%s' in table '%s'\n", secretName, config.TableName)
}

func listSecrets(args map[string]interface{}) {
	config := getManagerConfig(args)
	m := manager.New(config)

	secrets, err := m.List()
	if err != nil {
		log.Fatal(err)
	}

	for _, secret := range secrets {
		fmt.Printf("%s - %s\n", secret.Name, time.Unix(secret.UpdatedAt, 0).Format("2006-01-02 15:04:05"))
	}
}

func listNamespaces(args map[string]interface{}) {
	config := getManagerConfig(args)
	m := manager.New(config)

	namespaces, err := m.ListNamespaces()
	if err != nil {
		log.Fatal(err)
	}

	for _, namespace := range namespaces {
		fmt.Println(namespace)
	}
}

func listNamespace(args map[string]interface{}) {
	var namespace string

	if val, ok := args["<namespace>"].(string); ok {
		namespace = val
	} else {
		log.Fatal("can't parse <namespace>")
	}

	config := getManagerConfig(args)
	m := manager.New(config)

	secrets, err := m.ListNamespace(namespace)
	if err != nil {
		log.Fatal(err)
	}

	for _, secret := range secrets {
		fmt.Printf("%s - %s\n", secret.Name, time.Unix(secret.UpdatedAt, 0).Format("2006-01-02 15:04:05"))
	}
}

func getSecretEncrypted(args map[string]interface{}) {
	secretName := parseSecretName(args)
	config := getManagerConfig(args)
	m := manager.New(config)

	secret, err := m.GetEncrypted(secretName)
	if err != nil {
		log.Fatal(err)
	}
	if secret == nil {
		log.Fatalf("Secret: '%s' not found.", secretName)
	}
	printSecret(secret)
}

func getSecret(args map[string]interface{}) {
	secretName := parseSecretName(args)
	config := getManagerConfig(args)
	m := manager.New(config)

	secret, err := m.Get(secretName)
	if err != nil {
		log.Fatal(err)
	}
	if secret == nil {
		log.Fatalf("Secret: '%s' not found.", secretName)
	}
	fmt.Printf("%s", string(secret.Plaintext))
}

func getSecretVersions(args map[string]interface{}) {
	secretName := parseSecretName(args)
	m := manager.New(getManagerConfig(args))
	versions, err := m.GetVersionsEncrypted(secretName, 10, 0)
	if err != nil {
		fmt.Printf("\nError while getting versions\n: %#v", err)
		log.Fatal(err)
	}
	for _, secret := range versions.Secrets {
		fmt.Printf("%d\n", secret.UpdatedAt)
	}
}

func printSecret(secret *manager.Secret) {

	jsonSecret, err := json.MarshalIndent(secret, "", "	")
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(string(jsonSecret))
}

func copySecret(args map[string]interface{}) {
	var keyID, sourceName, destName string

	if val, ok := args["<keyid>"].(string); ok {
		keyID = val
	} else {
		log.Fatal("can't parse <keyid>")
	}

	if val, ok := args["<source>"].(string); ok {
		sourceName = val
	} else {
		log.Fatal("can't parse <source>")
	}

	if val, ok := args["<destination>"].(string); ok {
		destName = val
	} else {
		log.Fatal("can't parse <destination>")
	}

	config := getManagerConfig(args)
	config.KeyID = keyID
	m := manager.New(config)
	err := m.Copy(sourceName, destName)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Copied %s to %s\n", sourceName, destName)
}

func revertSecret(args map[string]interface{}) {
	secretName := parseSecretName(args)
	secretVersion := parseSecretVersion(args)
	m := manager.New(getManagerConfig(args))
	if err := m.Revert(secretName, secretVersion); err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Secret %s reverted to version %d\n", secretName, secretVersion)
}

func main() {
	args, err := docopt.Parse(usage, nil, true, "", false)

	if err != nil {
		log.Fatal(err)
	}

	if v, ok := args["create-table"].(bool); ok && v {
		createTable(args)
	}

	if v, ok := args["check-table"].(bool); ok && v {
		checkTable(args)
	}

	if v, ok := args["check-key"].(bool); ok && v {
		checkKey(args)
	}

	if v, ok := args["create-secret"].(bool); ok && v {
		createSecret(args)
	}

	if v, ok := args["update-secret"].(bool); ok && v {
		updateSecret(args)
	}

	if v, ok := args["delete-secret"].(bool); ok && v {
		deleteSecret(args)
	}

	if v, ok := args["list-secrets"].(bool); ok && v {
		listSecrets(args)
	}

	if v, ok := args["list-namespaces"].(bool); ok && v {
		listNamespaces(args)
	}

	if v, ok := args["list-namespace"].(bool); ok && v {
		listNamespace(args)
	}

	if v, ok := args["get-secret-encrypted"].(bool); ok && v {
		getSecretEncrypted(args)
	}

	if v, ok := args["get-secret"].(bool); ok && v {
		getSecret(args)
	}

	if v, ok := args["get-secret-versions"].(bool); ok && v {
		getSecretVersions(args)
	}

	if v, ok := args["copy-secret"].(bool); ok && v {
		copySecret(args)
	}

	if v, ok := args["revert-secret"].(bool); ok && v {
		revertSecret(args)
	}
}
