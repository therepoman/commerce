package consumedsecrets

import (
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

// Client is the struct that checks in client get requests.
type Client struct {
	DynamoDB  dynamodbiface.DynamoDBAPI
	TableName string
}

// API is the interface that Client implements
type API interface {
	IterateBySecret(secret string, cb IterationHandler) error
	IterateByHost(host string, cb IterationHandler) error
}

// Config is a struct for configuring the inventory logger
type Config struct {
	TableName string
}

// New creates a new client
func New(config *Config, sess *session.Session) (client *Client) {
	ddb := dynamodb.New(sess)
	client = &Client{
		DynamoDB:  ddb,
		TableName: config.TableName,
	}
	return
}

// IterationHandler is a function signature that handles a single ConsumedSecret
// when querying dynamo
type IterationHandler func(cs *ConsumedSecret) (err error)
