package consumedsecrets

//ConsumedSecret represents a secret consumed by a agent
type ConsumedSecret struct {
	Secret            string `dynamodbav:"secret"`
	Host              string `dynamodbav:"host"`
	FetchedAt         int64  `dynamodbav:"fetched_at"`
	FQDN              string `dynamodbav:"fqdn"`
	HeartbeatReceived int64  `dynamodbav:"heartbeat_received"`
	Service           string `dynamodbav:"service"`
	UpdatedAt         int64  `dynamodbav:"updated_at"`
	InstanceID        string `dynamodbav:"instance_id"`
	ManagerID         string `dynamodbav:"manager_id"`
}
