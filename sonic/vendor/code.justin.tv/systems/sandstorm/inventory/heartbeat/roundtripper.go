package heartbeat

import (
	"bytes"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/aws/aws-sdk-go/aws/signer/v4"
)

// sigV4Roundtripper signs a request using SigV4 before sending the request
type sigV4Roundtripper struct {
	inner  http.RoundTripper
	signer *v4.Signer
	region string
}

// RoundTrip implement RoundTripper
func (s *sigV4Roundtripper) RoundTrip(req *http.Request) (res *http.Response, err error) {
	seekableBody, err := wrapRequestBodyWithSeeker(req)
	if err != nil {
		return
	}

	_, err = s.signer.Sign(req, seekableBody, "execute-api", s.region, time.Now().UTC())
	if err != nil {
		return
	}

	res, err = s.inner.RoundTrip(req)
	return
}

func wrapRequestBodyWithSeeker(r *http.Request) (seekableBody io.ReadSeeker, err error) {
	if r.Body == nil {
		return
	}

	bs, err := ioutil.ReadAll(r.Body)
	defer func() {
		closeErr := r.Body.Close()
		if err == nil {
			err = closeErr
		}
	}()
	if err != nil {
		return
	}
	seekableBody = bytes.NewReader(bs)
	return
}
