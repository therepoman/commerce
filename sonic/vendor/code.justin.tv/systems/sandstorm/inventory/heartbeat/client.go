package heartbeat

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"sync"
	"time"

	"code.justin.tv/systems/sandstorm/logging"
	"code.justin.tv/systems/sandstorm/resource"

	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/signer/v4"
)

const endpointHeartbeat = "heartbeat"
const httpTimeout = 30
const defaultHost = "UnknownHost"
const defaultService = "UnknownService"
const httpHeaderContentType = "Content-Type"
const headerContentTypeJSON = "application/json"

var defaultInterval = time.Hour

//Config for inventory client
type Config struct {
	Interval       time.Duration
	URL            string
	Service        string
	Host           string
	FQDN           string
	Region         string
	InstanceID     string
	ManagerVersion string
	Environment    string
}

// HTTPClient so that we can mock http.client in tests.
type HTTPClient interface {
	Do(req *http.Request) (*http.Response, error)
}

//Client for inventory
type Client struct {
	httpClient     HTTPClient
	Config         *Config
	heartbeatState *heartbeatState
	Logger         logging.Logger
	running        bool
	cancel         chan struct{}
	cancelConfirm  chan struct{}
	flushChan      chan struct{}
	mutex          sync.Mutex
}

// API is the interface that Client implements
type API interface {
	UpdateHeartbeat(secret *Secret)
	Start()
	FlushHeartbeat(ctx context.Context)
	SendHeartbeat()
	Stop() error
}

// New condfigure a new http client, the client will send the secret status to the
// inventory server periodically.
// Once the oject is created the caller must call Start to start sending
// heartbeatState to inventory
func New(credentials *credentials.Credentials, config *Config, logger logging.Logger) (client *Client) {
	config, err := buildConfig(config)
	if err != nil {
		log.Fatal(err.Error())
	}
	if logger == nil {
		logger = &logging.NoopLogger{}
	}

	client = &Client{
		httpClient: &http.Client{
			Transport: &sigV4Roundtripper{
				inner:  http.DefaultTransport,
				signer: v4.NewSigner(credentials),
				region: config.Region,
			},
		},
		heartbeatState: newHeartbeatState(),
		Logger:         logger,
		Config:         config,
		cancel:         make(chan struct{}),
		cancelConfirm:  make(chan struct{}),
		flushChan:      make(chan struct{}, 1),
	}

	return
}

func defaultConfig() (config *Config, err error) {

	defaultEnvironment := "production"
	res, err := resource.GetConfigForEnvironment(defaultEnvironment)
	if err != nil {
		return
	}

	config = &Config{
		Region:      res.AwsRegion,
		URL:         res.InventoryStatusURL,
		Interval:    defaultInterval,
		Host:        hostName(),
		FQDN:        getFqdn(),
		Service:     serviceName(),
		Environment: res.Environment,
	}
	return
}

func getDefaultConfigByEnvironment(env string) (cfg *Config, err error) {
	cfg, err = defaultConfig()
	if err != nil {
		return
	}
	if env == "" || env == "production" {
		return
	}
	res, err := resource.GetConfigForEnvironment(env)
	if err != nil {
		return
	}

	cfg.URL = res.InventoryStatusURL
	cfg.Region = res.AwsRegion
	return
}

func buildConfig(providedConfig *Config) (config *Config, err error) {
	if providedConfig == nil {
		config, err = defaultConfig()
		return
	}

	config = providedConfig
	defaultConfig, err := getDefaultConfigByEnvironment(config.Environment)
	if err != nil {
		return
	}
	if config.URL == "" {
		config.URL = defaultConfig.URL
	}
	if config.Interval == 0 {
		config.Interval = defaultConfig.Interval
	}
	if config.Service == "" {
		config.Service = defaultConfig.Service
	}
	if config.Host == "" {
		config.Host = defaultConfig.Host
	}
	if config.FQDN == "" {
		config.FQDN = defaultConfig.FQDN
	}
	if config.Region == "" {
		config.Region = defaultConfig.Region
	}
	return
}

// UpdateHeartbeat updates the current state of heartbeats to send to
// the inventory server.
func (client *Client) UpdateHeartbeat(secret *Secret) {
	if secret == nil {
		return
	}
	client.Logger.Debugf("updating heartbeat for secret: %s", secret.Name)
	secret.FetchedAt = time.Now().Unix()

	client.mutex.Lock()
	client.heartbeatState.secretsToReport[secret.Name] = secret
	client.mutex.Unlock()

	return
}

// Start sends the current state of heartbeats to inventory server
func (client *Client) Start() {
	client.running = true

	defer func() {
		client.running = false
	}()

	client.Logger.Debugf(
		"reporting secret status to inventory at '%s' every %d seconds",
		client.Config.URL,
		client.Config.Interval/time.Second,
	)
	for {
		timer := time.NewTimer(client.Config.Interval)
		select {
		case <-client.cancel:
			client.Logger.Infof("Flushing one last time due to cancel request")
			client.SendHeartbeat()
			client.sendCancelConfirm()
			return
		case <-client.flushChan:
			client.SendHeartbeat()
		case <-timer.C:
			// send the heartbeats to server and sleep for specified interval
			client.SendHeartbeat()
			client.Logger.Debugf("Sleeping for %v seconds", client.Config.Interval)
		}
	}
}

func (client *Client) sendCancelConfirm() {
	timer := time.NewTimer(client.Config.Interval)
	select {
	case client.cancelConfirm <- struct{}{}:
		return
	case <-timer.C:
		client.Logger.Warnf("timeout when sending cancel confirmation")
		return
	}
}

// Stop stops the reporter goroutine
func (client *Client) Stop() (err error) {
	if !client.running {
		return
	}

	timer := time.NewTimer(10 * time.Second)
	err = client.sendStopRequest(timer)
	if err != nil {
		return
	}
	err = client.receiveCancelConfirm(timer)
	return
}

func (client *Client) sendStopRequest(timer *time.Timer) (err error) {
	select {
	case client.cancel <- struct{}{}:
		return
	case <-timer.C:
		err = errors.New("could not stop reporter gorountine")
		return
	}
}

func (client *Client) receiveCancelConfirm(timer *time.Timer) (err error) {
	select {
	case <-client.cancelConfirm:
		return
	case <-timer.C:
		err = errors.New("timeout when receiving cancel confirmation")
		return
	}
}

// FlushHeartbeat to manually flush the heartbeat
func (client *Client) FlushHeartbeat(ctx context.Context) {
	select {
	case client.flushChan <- struct{}{}:
	case <-ctx.Done():
	}
	return
}

// SendHeartbeat to manually flush the heartbeat. usually called in Start goroutine.
func (client *Client) SendHeartbeat() {
	if len(client.heartbeatState.secretsToReport) == 0 {
		client.Logger.Debugln("Nothing to report.")
		return
	}
	err := client.putHeartbeat()
	if err != nil {
		client.Logger.Debugf("failed to send heartbeat to inventory: err: %s", err.Error())
	}
	return
}

//Send the current heart state to inventory server
func (client *Client) putHeartbeat() (err error) {

	client.Logger.Debugf("Reporting heartbeat for %d secrets to %s. ", len(client.heartbeatState.secretsToReport), client.Config.URL)
	req, err := client.buildPutHeartbeatRequest()
	if err != nil {
		err = fmt.Errorf("Failed to build heartbeat request. Error: %s", err.Error())
		return
	}

	resp, err := client.httpClient.Do(req)

	if err != nil {
		err = fmt.Errorf("error sending request to inventory %s. err: %s", client.Config.URL, err.Error())
		return
	}

	defer func() {
		closeErr := resp.Body.Close()
		if closeErr != nil && err == nil {
			err = closeErr
		}
		return
	}()

	switch resp.StatusCode {
	case http.StatusOK:
		client.Logger.Debugf("Done reporting.")
	default:
		var body []byte
		body, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			return
		}
		err = fmt.Errorf("unexpected status code %d returned: %s", resp.StatusCode, string(body))
	}
	return
}

func (client *Client) buildPutHeartbeatRequest() (req *http.Request, err error) {

	hb := &heartbeat{}
	hb.Service = client.Config.Service
	hb.Host = client.Config.Host
	hb.FQDN = client.Config.FQDN
	hb.InstanceID = client.Config.InstanceID
	hb.ManagerVersion = client.Config.ManagerVersion

	//Build secrets to be send over to inventory
	var secrets []*Secret

	// build an inventory.heartbeats to send to the servers.
	for _, secret := range client.heartbeatState.secretsToReport {
		secrets = append(secrets, secret)
	}
	hb.Secrets = secrets

	bs := bytes.NewBuffer(nil)

	err = json.NewEncoder(bs).Encode(hb)
	if err != nil {
		return
	}

	reqBody := bytes.NewReader(bs.Bytes())

	putHeartbeatEndpoint := fmt.Sprintf("%s/%s", client.Config.URL, endpointHeartbeat)
	req, err = http.NewRequest("PUT", putHeartbeatEndpoint, reqBody)

	req.Header.Set(httpHeaderContentType, headerContentTypeJSON)
	if err != nil {
		return
	}

	return
}

//serviceName return processName. if non found return unknownService
func serviceName() (service string) {
	service = os.Args[0]
	if service == "" {
		service = defaultService
	}
	return
}

//hostName return hostname of the machine. if non found return unknownHost
func hostName() (host string) {
	host, err := os.Hostname()
	if err != nil {
		host = defaultHost
	}
	return
}

func getFqdn() (fqdn string) {
	var out bytes.Buffer
	cmd := exec.Command("hostname", "-f")
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		fqdn = "Unknown FQDN"
		return
	}
	fqdn = out.String()
	fqdn = strings.TrimSpace(fqdn)
	return
}
