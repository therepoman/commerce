package heartbeat

//Secret a heartbeat of a secret
type Secret struct {
	Name      string `json:"name,required"`
	UpdatedAt int64  `json:"updated_at,required"`
	FetchedAt int64  `json:"fetched_at"`
}

//heartbeatState to be reported to inventory
type heartbeatState struct {
	secretsToReport map[string]*Secret
}

func newHeartbeatState() *heartbeatState {
	return &heartbeatState{
		secretsToReport: make(map[string]*Secret),
	}
}

//DynamoHeartbeat stores the secrets heartBeat to be reported to inventory
type DynamoHeartbeat struct {
	Service           string `dynamodbav:"service"`
	UpdatedAt         int64  `dynamodbav:"updated_at"`
	HeartbeatReceived int64  `dynamodbav:"heartbeat_received"`
	FetchedAt         int64  `dynamodbav:"fetched_at"`
	ExpiresAt         int64  `dynamodbav:"expires_at"`
	Secret            string `dynamodbav:"secret"`
	FQDN              string `dynamodbav:"fqdn"`
	Host              string `dynamodbav:"host"`
}

// heartbeat represents the heartbeat that a sandstorm manager will send to
// inventory manager service.
type heartbeat struct {
	// Secrets is an array that represents the secrets that a host is retrieving
	Secrets []*Secret `json:"secrets"`

	// Hostname is the name of the host that sent the heartbeat.
	Host string `json:"host,required"`
	FQDN string `json:"fqdn,required"`

	//Name of the service who pulled the secrets.
	Service string `json:"service,required"`

	//InstanceID uniquely represting the manager instance
	InstanceID string `json:"instance_id"`

	//ManagerVersion version of manager sdk being used.
	ManagerVersion string `json:"manager_version"`
}
