package manager

import (
	"context"
	"log"
	"strconv"
	"time"

	"code.justin.tv/systems/sandstorm/internal/stat"
	"code.justin.tv/systems/sandstorm/inventory/heartbeat"
	"code.justin.tv/systems/sandstorm/logging"
	"code.justin.tv/systems/sandstorm/queue"
	"code.justin.tv/systems/sandstorm/resource"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/kms"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sts"
	uuid "github.com/satori/go.uuid"
)

// API describes the manager interface
type API interface {
	AuditTableName() string
	CheckKMS() error
	CheckTable() error
	Copy(source, destination string) error
	CreateTable() error
	Decrypt(secret *Secret) error
	Delete(secretName string) error
	Exist(secretName string) (bool, error)
	CrossEnvironmentSecretsSet(secretNames []string) (crossEnvSecrets map[string]struct{}, err error)
	FlushHeartbeat(ctx context.Context)
	Get(secretName string) (*Secret, error)
	GetEncrypted(secretName string) (*Secret, error)
	GetVersion(secretName string, version int64) (*Secret, error)
	GetVersionsEncrypted(secretName string, limit int64, offsetKey int64) (*VersionsPage, error)
	List() ([]*Secret, error)
	ListenForUpdates() error
	ListNamespace(namespace string) ([]*Secret, error)
	ListNamespaces() ([]string, error)
	NamespaceTableName() string
	Patch(patchInput *PatchInput) error
	Post(secret *Secret) error
	Put(secret *Secret) error
	Revert(name string, version int64) error
	RegisterSecretUpdateCallback(secretName string, callback func(input SecretChangeset))
	Seal(secret *Secret) error
	StopListeningForUpdates() error
	TableName() string
	CleanUp() error

	//
	DeleteSecret(input *DeleteSecretInput) (output *DeleteSecretOutput, err error)
	RevertSecret(input *RevertSecretInput) (output *RevertSecretOutput, err error)
	CopySecret(input *CopySecretInput) (output *CopySecretOutput, err error)
}

// InputValidationError represents error if input was invalid
type InputValidationError struct {
	err string
}

func (e *InputValidationError) Error() string {
	return e.err
}

const version = "1.0"

const (
	dynamoDBKeyTombstone       = "tombstone"
	dynamoDBKeyActionUser      = "action_user"
	dynamoDBKeyServiceName     = "service_name"
	dynamoDBKeyPreviousVersion = "previous_version"
)

const (
	attempted = "attempted"
	success   = "success"
	failure   = "failure"
	cached    = "cached"
	notFound  = "not_found"
)

// Manager is the struct that a caller interacts with for managing
// secrets.
type Manager struct {
	Envelope                Enveloper
	DynamoDB                dynamodbiface.DynamoDBAPI
	Config                  Config
	Logger                  logging.Logger
	statter                 stat.Statter
	inventoryClient         heartbeat.API
	queue                   queue.Queuer
	cache                   *cache
	stopListeningForUpdates chan struct{}
	secretCallbacks         map[string][]func(SecretChangeset)
}

// Config is a struct for configuring the manager
// XXX need to be able to provide an aws.Config for STS credentials,
// region etc
type Config struct {
	// AWS config structure. If left nil, will be replaced by a
	// default config with region us-west-2.
	AWSConfig *aws.Config
	// KMS KeyId - this is either a key alias prefixed by 'alias/',
	// a key ARN or a key UUID
	KeyID string
	// DynamoDB Table name
	TableName string
	// Provisioned Read and Write throughput when creating the
	// table, not used in normal operations. If left to 0 these
	// will be replaced by 1 by default.
	ProvisionedThroughputRead  int64
	ProvisionedThroughputWrite int64
	Logger                     logging.Logger

	Host               string
	InventoryRoleARN   string
	CloudWatchRoleARN  string
	InventoryStatusURL string
	InventoryInterval  time.Duration
	ServiceName        string

	Queue      queue.Config
	InstanceID string

	//User performing the action
	ActionUser string

	// Environment to configure manager for
	Environment string
}

const unknownServiceName = "unknown-service"

// DefaultConfig returns a config struct with some sane defaults that
// is merged with the provided config in New
func DefaultConfig() Config {
	defEnv := "production"

	// gets a predefined config values for production env.
	// This call with arg "" or "production" should not return error.
	res, err := resource.GetConfigForEnvironment(defEnv)
	if err != nil {
		log.Fatal(err.Error())
	}

	return Config{
		Environment:                defEnv,
		ProvisionedThroughputRead:  1,
		ProvisionedThroughputWrite: 1,
		AWSConfig:                  &aws.Config{Region: aws.String(res.AwsRegion)},
		Logger:                     &logging.NoopLogger{},
		InventoryRoleARN:           res.InventoryRoleARN,
		CloudWatchRoleARN:          res.CloudwatchRoleArn,
		KeyID:                      res.KMSKeyID,
		TableName:                  res.TableName,
		Queue: queue.Config{
			Environment: defEnv,
		},
	}
}

func getDefaultConfigByEnvironment(env string) (cfg Config) {

	cfg = DefaultConfig()
	if env == "" || env == "production" {
		return
	}

	// fetchs config values for the specified env stored in s3.
	res, err := resource.GetConfigForEnvironment(env)
	if err != nil {
		log.Fatal(err.Error())
	}

	cfg.InventoryRoleARN = res.InventoryRoleARN
	cfg.CloudWatchRoleARN = res.CloudwatchRoleArn
	cfg.KeyID = res.KMSKeyID
	cfg.TableName = res.TableName
	cfg.Logger = &logging.NoopLogger{}
	cfg.Queue = queue.Config{
		Environment: env,
	}

	return
}

func mergeConfig(provided *Config, defConfig Config) {
	if provided.AWSConfig == nil {
		provided.AWSConfig = defConfig.AWSConfig
	}
	if provided.ProvisionedThroughputRead == 0 {
		provided.ProvisionedThroughputRead = defConfig.ProvisionedThroughputRead
	}
	if provided.ProvisionedThroughputWrite == 0 {
		provided.ProvisionedThroughputWrite = defConfig.ProvisionedThroughputWrite
	}
	if provided.Logger == nil {
		provided.Logger = defConfig.Logger
	}
	if provided.InventoryRoleARN == "" {
		provided.InventoryRoleARN = defConfig.InventoryRoleARN
	}
	if provided.CloudWatchRoleARN == "" {
		provided.CloudWatchRoleARN = defConfig.CloudWatchRoleARN
	}
	if provided.InstanceID == "" {
		provided.InstanceID = uuid.NewV4().String()
	}
	if provided.KeyID == "" {
		provided.KeyID = defConfig.KeyID
	}
	if provided.TableName == "" {
		provided.TableName = defConfig.TableName
	}
	if provided.ActionUser == "" {
		provided.ActionUser = getCallerIdentity(provided.AWSConfig)
	}
	if provided.Queue == (queue.Config{}) {
		provided.Queue = defConfig.Queue
	}
}

// New creates a Manager from config. Merges configuration with
// DefaultConfig()
func New(config Config) *Manager {
	defConfig := getDefaultConfigByEnvironment(config.Environment)
	mergeConfig(&config, defConfig)

	session := session.New(config.AWSConfig)

	mgr := &Manager{
		DynamoDB: dynamodb.New(session),
		Config:   config,
		Envelope: &Envelope{
			KMS: kms.New(session),
		},
		Logger:  config.Logger,
		statter: stat.New(config.Environment, stscreds.NewCredentials(session, config.CloudWatchRoleARN), config.Logger),
		cache:   &cache{},
		queue:   queue.New(sqs.New(session), sns.New(session), config.Queue, config.Logger),
		stopListeningForUpdates: make(chan struct{}),
		secretCallbacks:         make(map[string][]func(input SecretChangeset)),
	}

	mgr.inventoryClient = heartbeat.New(
		stscreds.NewCredentials(session, config.InventoryRoleARN),
		&heartbeat.Config{
			Interval:       config.InventoryInterval,
			Service:        config.ServiceName,
			Region:         *config.AWSConfig.Region,
			Host:           config.Host,
			InstanceID:     config.InstanceID,
			ManagerVersion: version,
			Environment:    config.Environment,
		},
		config.Logger,
	)
	go mgr.inventoryClient.Start()

	return mgr
}

func (mgr *Manager) newCache() {
	mgr.cache = &cache{
		secretMap: make(map[string]*Secret),
	}
}

// ListenForUpdates manager listens on the sandstorm queue for secret updates.
// It also configures the manager to cache secret plaintext in memory. When an
// update event is received for a secret, that secret is expunged from the in
// memory cache, so the next Get call will fetch that secret from DynamoDB.
func (mgr *Manager) ListenForUpdates() (err error) {
	mgr.newCache()
	err = mgr.queue.Setup()
	if err != nil {
		return
	}

	go func() {
		for {
			select {
			case <-mgr.stopListeningForUpdates:
				return
			default:
				err := mgr.listenForSecretUpdates()
				if err != nil {
					mgr.Logger.Errorf("Failed to listen for secret update. err: %s", err.Error())
				}
			}
		}
	}()
	return
}

func (mgr *Manager) listenForSecretUpdates() (err error) {
	errChan := make(chan error)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go func() {
		queueSecret, err := mgr.queue.PollForSecret(ctx)
		if err != nil {
			errChan <- err
			return
		}
		if queueSecret != nil {
			secretName := queueSecret.Name.S
			mgr.cache.Delete(secretName)
			// This should always be int, if for some reason conversion fails,
			// send the error over to chan and continue with updatedAt = 0
			updatedAt, err := strconv.ParseInt(queueSecret.UpdatedAt.S, 10, 64)
			if err != nil {
				errChan <- err
			}
			mgr.triggerSecretUpdateCallbacks(secretName, updatedAt)
		}
		errChan <- nil
		return
	}()

	select {
	case <-mgr.stopListeningForUpdates:
		err = <-errChan
	case err = <-errChan:
	}
	return
}

// StopListeningForUpdates manager stops listening to secret update queue and
// deletes the queue
func (mgr *Manager) StopListeningForUpdates() (err error) {
	mgr.cache = &cache{}
	close(mgr.stopListeningForUpdates)
	return mgr.queue.Delete()
}

// CleanUp cleans exits any go routines spawned
func (mgr *Manager) CleanUp() (err error) {
	inventoryStopErr := mgr.inventoryClient.Stop()
	if inventoryStopErr != nil {
		mgr.Logger.Debugf("error stopping inventory client: %s", inventoryStopErr.Error())
	}
	return
}

// FlushHeartbeat sends a signal to immediately report heartbeat
func (mgr *Manager) FlushHeartbeat(ctx context.Context) {
	mgr.inventoryClient.FlushHeartbeat(ctx)
}

//return arn of the caller identity, can be role or user
func getCallerIdentity(awsConfig *aws.Config) (callerArn string) {
	svc := sts.New(session.New(awsConfig))
	input := &sts.GetCallerIdentityInput{}

	result, err := svc.GetCallerIdentity(input)
	if err != nil {
		//Do not fail if we cant get the caller identity, its only for changleog purpose.
		callerArn = "unknown-user-arn"
		err = nil
		return
	}
	callerArn = aws.StringValue(result.Arn)
	return
}
