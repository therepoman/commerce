package manager

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
)

const (
	sandstormKMSCloudwatchNamespace = "SandstormKMS"
)

func (m *Manager) putKMSAttemptedMetric(
	kmsMethod string,
	secret *Secret,
) {
	baseDimensions := []*cloudwatch.Dimension{
		&cloudwatch.Dimension{
			Name:  aws.String("KMSKeyARN"),
			Value: aws.String(secret.KeyARN),
		},
	}

	bySecretNameDimensions := append(baseDimensions, &cloudwatch.Dimension{
		Name:  aws.String("SecretName"),
		Value: aws.String(secret.Name),
	})

	metricName := fmt.Sprintf("%sAttempted", kmsMethod)

	m.statter.IncStatWithDimensionsAndNamespace(metricName, sandstormKMSCloudwatchNamespace, baseDimensions)
	m.statter.IncStatWithDimensionsAndNamespace(metricName, sandstormKMSCloudwatchNamespace, bySecretNameDimensions)
}

func (m *Manager) putKMSErrorMetric(
	kmsMethod string,
	secret *Secret,
	awsErr awserr.Error,
) {
	baseDimensions := []*cloudwatch.Dimension{
		&cloudwatch.Dimension{
			Name:  aws.String("AWSError"),
			Value: aws.String(awsErr.Code()),
		},
		&cloudwatch.Dimension{
			Name:  aws.String("KMSKeyARN"),
			Value: aws.String(secret.KeyARN),
		},
	}

	bySecretNameDimensions := append(baseDimensions, &cloudwatch.Dimension{
		Name:  aws.String("SecretName"),
		Value: aws.String(secret.Name),
	})

	metricName := fmt.Sprintf("%sError", kmsMethod)

	m.statter.IncStatWithDimensionsAndNamespace(metricName, sandstormKMSCloudwatchNamespace, baseDimensions)
	m.statter.IncStatWithDimensionsAndNamespace(metricName, sandstormKMSCloudwatchNamespace, bySecretNameDimensions)
}
