package callback

import (
	"time"

	"code.justin.tv/systems/sandstorm/closer"
	"code.justin.tv/systems/sandstorm/internal/secret"
	"code.justin.tv/systems/sandstorm/manager"
)

const (
	defaultDuration = 30 * time.Second
)

// Dweller holds info about callbacks and timers.
type Dweller struct {
	// Duration - wait for duration of time for more secret updates
	// before executing callback functions.
	// Execute callback iff no secret updates are recieved for `duration` time.
	Duration time.Duration

	Closer *closer.Closer

	executionChan []chan struct{}
	timer         *time.Timer
}

func (dw *Dweller) duration() time.Duration {
	if dw.Duration == 0*time.Second {
		dw.Duration = defaultDuration
	}
	return dw.Duration
}

func (dw *Dweller) closer() *closer.Closer {
	if dw.Closer == nil {
		dw.Closer = closer.New()
	}
	return dw.Closer
}

// Dwell wait for some time to wait for other secret update notification
// before running the innner callback funtion.
func (dw *Dweller) Dwell(inner func(input manager.SecretChangeset)) func(input manager.SecretChangeset) {

	if dw.timer == nil {
		dw.timer = time.NewTimer(dw.duration())
		go dw.startTimer()
	}

	executionChan := make(chan struct{})

	updates := &secret.Changeset{
		SecretInfo: make(map[string]int64),
	}

	dw.executionChan = append(dw.executionChan, executionChan)

	dwellFn := func(input manager.SecretChangeset) {

		secretChangeset, ok := input.(*secret.Changeset)
		if !ok {
			return
		}
		for secretName, updatedAt := range secretChangeset.SecretInfo {
			updates.SecretInfo[secretName] = updatedAt
		}
		_ = dw.timer.Reset(dw.Duration)
	}

	// wait for timer to expire, and call inner callback function
	go func() {
		for {
			select {
			case <-executionChan:
				// only execute the cb if the secret update notification was received.
				if updates.SecretsHaveChanged() {
					copyInput := updates

					// Start with new secretChangeset state
					updates = &secret.Changeset{
						SecretInfo: make(map[string]int64),
					}
					inner(copyInput)
				}
			case <-dw.closer().Done():
				return
			}
		}
	}()

	return dwellFn
}

// run a goroutine to keep track of timer.
func (dw *Dweller) startTimer() {
	for {
		select {
		case <-dw.timer.C:
			for _, callbackChan := range dw.executionChan {
				select {
				// notify each callback chan about the timer expiration.
				case callbackChan <- struct{}{}:
				case <-dw.closer().Done():
					return
				}
			}
		case <-dw.closer().Done():
			return
		}
	}
}
