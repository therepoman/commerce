package queue

import (
	"log"
	"path/filepath"
	"strings"

	"code.justin.tv/systems/sandstorm/resource"

	uuid "github.com/satori/go.uuid"
)

const (
	maxQueuePrefixLength   = 42
	maxNamePrefixLength    = 20
	defaultQueueConfigPath = "/var/run/sandstorm/"
	defaultQueueFileName   = "queue_arn"
	defaultQueueNamePrefix = "sandstorm"
	defaultRegion          = "us-west-2"
)

//Config for Queue
type Config struct {
	Environment     string
	QueueArn        string
	QueueURL        string
	TopicArn        string
	QueueConfigPath string
	QueueFileName   string
	QueueNamePrefix string
	WaitTimeSeconds int
}

func truncatePrefix(prefix string, maxLength int) (truncated string) {
	truncated = prefix
	if len(truncated) > maxLength {
		truncated = truncated[0:maxLength]
	}
	return
}

// buildQueueIdentifier returns a newly generated UUID and a FQDN
// Queuenames have a maximum length of 80 chars and and we prefix with 'sandstorm'
// and suffix wth a UUID. Hostnames are truncated down to 43-len(prefix) chars to fit
// name length
func buildQueueIdentifier(namePrefix, hostname string) (name string) {
	namePrefix = truncatePrefix(namePrefix, maxNamePrefixLength)
	hostname = truncatePrefix(hostname, maxQueuePrefixLength-len(namePrefix))

	uid := uuid.NewV4().String()
	name = strings.Join([]string{namePrefix, hostname, uid}, "-")
	name = strings.Replace(name, ".", "_", -1)
	return
}

// DefaultConfig creates a config with default values
func DefaultConfig() Config {

	res, err := resource.GetConfigForEnvironment("production")
	if err != nil {
		log.Fatal(err.Error())
	}
	return Config{
		TopicArn:        res.TopicArn,
		QueueConfigPath: defaultQueueConfigPath,
		QueueFileName:   defaultQueueFileName,
		QueueNamePrefix: defaultQueueNamePrefix,
	}
}

func getDefaultConfigByEnvironment(env string) (cfg Config, err error) {
	cfg = DefaultConfig()
	if env == "" || env == "production" {
		return
	}

	var res *resource.Config
	res, err = resource.GetConfigForEnvironment(env)
	if err != nil {
		return
	}
	cfg.TopicArn = res.TopicArn
	return
}

// replaces nil values in provided config with defaultConfig
func mergeConfig(provided *Config, defaultConfig Config) {
	if provided.QueueConfigPath == "" {
		provided.QueueConfigPath = defaultConfig.QueueConfigPath
	}

	if provided.QueueFileName == "" {
		provided.QueueFileName = defaultConfig.QueueFileName
	}

	if provided.QueueNamePrefix == "" {
		provided.QueueNamePrefix = defaultConfig.QueueNamePrefix
	}

	if provided.TopicArn == "" {
		provided.TopicArn = defaultConfig.TopicArn
	}
}

// QueueConfigFullPath returns the queue config
func (cfg *Config) QueueConfigFullPath() string {
	return filepath.Join(cfg.QueueConfigPath, cfg.QueueFileName)
}
