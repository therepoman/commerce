package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"strings"

	"code.justin.tv/systems/changelog-clients/go/ccg"

	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodbstreams"
	"github.com/kelseyhightower/envconfig"
	"github.com/sirupsen/logrus"
)

// eventRecord is parsed from the lambda event
type eventRecord struct {
	EventVersion string                       `json:"eventVersion"`
	EventSource  string                       `json:"eventSource"`
	AwsRegion    string                       `json:"awsRegion"`
	EventName    string                       `json:"eventName"`
	DynamoDB     dynamodbstreams.StreamRecord `json:"dynamodb"`
}

type event struct {
	Records []*eventRecord
}

//event type constants
const (
	eventTypeInsert = "INSERT"
	eventTypeModify = "MODIFY"
	eventTypeRemove = "REMOVE"
)

const changelogUsernameLengthLimit = 30

type dynamoDBAudit struct {
	Name            string `dynamodbav:"name" json:"-"`
	Tombstone       bool   `dynamodbav:"tombstone" json:"-"`
	ActionUser      string `dynamodbav:"action_user" json:"-"`
	PreviousVersion int64  `dynamodbav:"previous_version" json:"-"`
	UpdatedAt       int64  `dynamodbav:"updated_at" json:"-"`

	DoNotBroadcast bool `dynamodbav:"do_not_broadcast" json:"do_not_broadcast"`
	Class          int  `dynamodbav:"class" json:""`
	CrossEnv       bool `dynamodbav:"cross_env" json:"cross_env"`
}

type envConfig struct {
	ChangelogEndpoint string `default:"https://changelog.internal.justin.tv" envconfig:"changelog_endpoint"`
	Environment       string `default:"production" required:"true"`
}

type eventHandler struct {
	config *envConfig
	client *ccg.Client
	logger logrus.FieldLogger
}

const (
	commandSecretMetadataUpdated = "secret metadata updated"
	commandSecretReverted        = "secret reverted"
	commandSecretDeleted         = "secret deleted"
	commandSecretCreatedUpdated  = "secret created/updated"
)

func getCommand(eventName string, record, oldRecord *dynamoDBAudit) (command string, err error) {
	if eventName == eventTypeInsert {
		command = commandSecretCreatedUpdated
	} else if eventName == eventTypeModify {
		if record.Tombstone {
			command = commandSecretDeleted
		} else if record.PreviousVersion != 0 && oldRecord.PreviousVersion != record.PreviousVersion {
			command = commandSecretReverted
		} else {
			command = commandSecretMetadataUpdated
		}
	} else {
		err = fmt.Errorf("unexpected event name: %s recieved on secret: %s", eventName, record.Name)
	}
	return
}

func dbImageAsJSON(image map[string]*dynamodb.AttributeValue) (res string, err error) {
	img := new(dynamoDBAudit)
	err = dynamodbattribute.UnmarshalMap(image, img)
	if err != nil {
		return
	}

	resBytes, err := json.Marshal(img)
	if err != nil {
		return
	}

	res = string(resBytes)
	return
}

func (handler *eventHandler) changedFieldsInfo(er *eventRecord, printOldImage bool) (fieldInfo string, err error) {

	var newImage string
	newImage, err = dbImageAsJSON(er.DynamoDB.NewImage)
	if err != nil {
		return
	}
	fieldInfo = fmt.Sprintf("secret metadata fields: %v", newImage)

	if printOldImage == true {
		var oldImage string
		oldImage, err = dbImageAsJSON(er.DynamoDB.OldImage)
		if err != nil {
			return
		}
		fieldInfo = fmt.Sprintf("%s, old metadata: %s", fieldInfo, oldImage)
	}
	return
}

func (handler *eventHandler) handleEventRecord(er *eventRecord) (err error) {
	if er == nil {
		handler.logger.Error("empty event record received")
		return
	}
	// we do not expect remove on audit table.
	if er.EventName == eventTypeRemove {
		err = fmt.Errorf("unexpected event %s, on audit table", er.EventName)
		return
	}

	r := new(dynamoDBAudit)
	err = dynamodbattribute.UnmarshalMap(er.DynamoDB.NewImage, r)
	if err != nil {
		return
	}

	oldRecord := new(dynamoDBAudit)
	err = dynamodbattribute.UnmarshalMap(er.DynamoDB.OldImage, oldRecord)
	if err != nil {
		return
	}
	handler.logger.Infof("processing event %s on secret: %s", er.EventName, r.Name)

	command, err := getCommand(er.EventName, r, oldRecord)
	if err != nil {
		err = fmt.Errorf("invalid eventName %s for secret:%s. err: %s", er.EventName, r.Name, err.Error())
		return
	}
	username := r.ActionUser
	isAWSUserArn := strings.HasPrefix(username, "arn:aws:sts::")
	if isAWSUserArn {
		username = ""
	}

	//Remove prefix arn:aws:sts:: from string if present.
	if len(username) > changelogUsernameLengthLimit {
		username = username[:changelogUsernameLengthLimit]
	}

	var printOldImage bool
	var fieldInfo string
	if command == commandSecretDeleted || command == commandSecretMetadataUpdated {
		printOldImage = true
	}
	if command != commandSecretDeleted {
		fieldInfo, err = handler.changedFieldsInfo(er, printOldImage)
		if err != nil {
			err = fmt.Errorf("failed to get changed field info from record, err: %s", err.Error())
			return
		}
	}

	err = handler.sendEventToChangelog(command, username, fieldInfo, r)
	return
}

func (handler *eventHandler) sendEventToChangelog(command, username, changedFieldInfo string, r *dynamoDBAudit) (err error) {
	changelogEvent := &ccg.Event{
		AdditionalData: fmt.Sprintf("username=%s, secret=%s, updated_at(version)=%d. %s", r.ActionUser, r.Name, r.UpdatedAt, changedFieldInfo),
		Category:       fmt.Sprintf("sandstorm-%s", handler.config.Environment),
		Command:        command,
		Criticality:    ccg.SevInfo,
		Description:    fmt.Sprintf("%s: '%s'", command, r.Name),
		Target:         r.Name,
	}
	if username != "" {
		changelogEvent.Username = username
	}
	handler.logger.Infof("Sending event to changelog server %s with category: %s, description: %s, message: %s", handler.client.Endpoint, changelogEvent.Category, changelogEvent.Description, changelogEvent.AdditionalData)
	err = handler.client.SendEvent(changelogEvent)
	return
}

func (handler *eventHandler) handleEvent(e *event) (err error) {

	if e == nil {
		err = errors.New("empty event received")
		return
	}

	errs := []string{}
	for _, record := range e.Records {
		err := handler.handleEventRecord(record)
		if err != nil {
			errs = append(errs, err.Error())
		}
	}
	if len(errs) > 0 {
		err = errors.New("errors: " + strings.Join(errs, ", "))
		return
	}
	handler.logger.Infof("finished processing %d records", len(e.Records))
	return
}

func buildEventHandler(cfg *envConfig, lgr logrus.FieldLogger) (handler *eventHandler) {
	if lgr == nil {
		lgr = logrus.New()
		lgr.(*logrus.Logger).Level = logrus.InfoLevel
		lgr.(*logrus.Logger).Out = os.Stderr
	}

	handler = &eventHandler{
		config: cfg,
		client: &ccg.Client{Endpoint: cfg.ChangelogEndpoint},
		logger: lgr,
	}

	return
}

func main() {

	e := new(event)

	bio := bufio.NewReader(os.Stdin)
	err := json.NewDecoder(bio).Decode(e)
	if err != nil {
		log.Fatal(err)
	}

	cfg := new(envConfig)
	err = envconfig.Process("sandstorm", cfg)
	if err != nil {
		log.Fatal(err)
	}

	handler := buildEventHandler(cfg, nil)
	err = handler.handleEvent(e)
	if err != nil {
		handler.logger.Error(err.Error())
		log.Fatal(err)
	}
	return
}
