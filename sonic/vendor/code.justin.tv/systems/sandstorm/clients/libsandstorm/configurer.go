package main

import (
	"os"
	"time"

	"code.justin.tv/systems/sandstorm/manager"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
)

type configurerKey struct {
	Environment string
	Region      string
	RoleARN     string
}

type configurer struct {
	clients map[configurerKey]manager.API
}

func (c *configurer) getManager(cfg managerConfigurer) manager.API {
	key := configurerKey{
		Environment: cfg.GetEnvironment(),
		Region:      cfg.GetRegion(),
		RoleARN:     cfg.GetRoleARN(),
	}

	if m, ok := c.clients[key]; ok {
		return m
	}

	m := manager.New(manager.Config{
		Environment: cfg.GetEnvironment(),
		AWSConfig: &aws.Config{
			Region: aws.String(cfg.GetRegion()),
			Credentials: credentials.NewCredentials(&stscreds.AssumeRoleProvider{
				Duration:     900 * time.Second,
				ExpiryWindow: 10 * time.Second,
				RoleARN:      cfg.GetRoleARN(),
				Client: sts.New(session.New(&aws.Config{
					Region: aws.String(cfg.GetRegion()),
				})),
			}),
		},
	})

	c.clients[key] = m
	return m
}

func (c *configurer) updateEnvironment(cfg managerConfigurer) error {
	for _, EnvSetting := range []struct {
		Key   string
		Value string
	}{
		{"AWS_ACCESS_KEY_ID", cfg.GetAccessKeyID()},
		{"AWS_SECRET_ACCESS_KEY", cfg.GetSecretAccessKey()},
		{"AWS_SECURITY_TOKEN", cfg.GetSecurityToken()},
		{"AWS_SESSION_TOKEN", cfg.GetSessionToken()},
	} {
		if len(EnvSetting.Value) > 0 {
			if err := os.Setenv(EnvSetting.Key, EnvSetting.Value); err != nil {
				return err
			}
		}
	}
	return nil
}

func (c *configurer) WithManager(
	cfg managerConfigurer,
	fn func(m manager.API) (interface{}, error),
) (interface{}, error) {
	if err := c.updateEnvironment(cfg); err != nil {
		return nil, err
	}
	return fn(c.getManager(cfg))
}
