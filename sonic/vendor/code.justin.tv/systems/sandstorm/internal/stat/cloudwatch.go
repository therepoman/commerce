package stat

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"code.justin.tv/systems/sandstorm/logging"
	"code.justin.tv/systems/sandstorm/resource"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
	"github.com/aws/aws-sdk-go/service/cloudwatch/cloudwatchiface"
)

// Func is the method call expected in this package
type Func func() error

const (
	defaultCloudWatchNamespace = "Sandstorm"
)

// Statter reports metrics to cloudwatch
type Statter struct {
	CloudWatch  cloudwatchiface.CloudWatchAPI
	Logger      logging.Logger
	Environment string
}

// New initialize cloudwatch statter
func New(env string, credentials *credentials.Credentials, logger logging.Logger) (client Statter) {

	if logger == nil {
		logger = &logging.NoopLogger{}
	}

	if env == "" {
		env = "production"
	}
	// fetchs config values for the specified env stored in s3.
	res, err := resource.GetConfigForEnvironment(env)
	if err != nil {
		log.Fatal(err.Error())
	}

	client = Statter{
		Logger: logger,
		CloudWatch: cloudwatch.New(session.New(&aws.Config{
			Credentials: credentials,
			Region:      aws.String(res.AwsRegion),
		})),
		Environment: env,
	}
	return
}

// incStat increments the metrics metricName
func (s *Statter) incStat(metricName string) {
	s.incStatWithDimensions(metricName, s.getDefaultMetricDimension())
	return
}

// incStatWithDimensions ...
func (s *Statter) incStatWithDimensions(metricName string, dimensions []*cloudwatch.Dimension) {

	s.IncStatWithDimensionsAndNamespace(metricName, defaultCloudWatchNamespace, dimensions)
	return
}

// IncStatWithDimensionsAndNamespace report stats with dimension and namespace to cloudwatch
func (s *Statter) IncStatWithDimensionsAndNamespace(metricName string, namespace string, dimensions []*cloudwatch.Dimension) {

	metricDatum := &cloudwatch.MetricDatum{
		Dimensions:        s.getDefaultMetricDimension(),
		MetricName:        aws.String(metricName),
		StorageResolution: aws.Int64(1),
		Unit:              aws.String(cloudwatch.StandardUnitCount),
		Value:             aws.Float64(1.0),
	}

	if dimensions != nil {
		metricDatum.Dimensions = dimensions
	}

	s.putMetricData([]*cloudwatch.MetricDatum{metricDatum}, namespace)
	return
}

func (s *Statter) timingDurationStat(metricName string, startTime time.Time) {
	s.timingDurationStatHelper(metricName, time.Now().Sub(startTime))
	return
}

// WithCloudWatchMetrics increments couter stats "<metricName>.success" if fn returns
// no error, increments couter stats "<metricName>.failure" otherwise.
// It will also report timer stat "<metricName>.time" on success
func (s *Statter) WithCloudWatchMetrics(metricName string, fn Func) (err error) {
	startTime := time.Now()
	err = fn()
	if err != nil {
		s.incStat(buildMetricName(metricName, "failure"))
		return
	}
	s.incStat(buildMetricName(metricName, "success"))
	s.timingDurationStat(buildMetricName(metricName, "time"), startTime)
	return
}

/*########################
##### Helper functions ###
##########################*/

// to help test the mock, we are passing duration instead of passing start time.
func (s *Statter) timingDurationStatHelper(metricName string, duration time.Duration) {

	timeElapsedInMilli := duration.Nanoseconds() / int64(time.Millisecond)
	metricDatum := []*cloudwatch.MetricDatum{
		&cloudwatch.MetricDatum{
			Dimensions:        s.getDefaultMetricDimension(),
			MetricName:        aws.String(metricName),
			StorageResolution: aws.Int64(1),
			Unit:              aws.String(cloudwatch.StandardUnitMilliseconds),
			Value:             aws.Float64(float64(timeElapsedInMilli)),
		},
	}

	s.putMetricData(metricDatum, defaultCloudWatchNamespace)
	return

}

func (s *Statter) putMetricData(metricDatum []*cloudwatch.MetricDatum, namespace string) {

	_, err := s.CloudWatch.PutMetricData(&cloudwatch.PutMetricDataInput{
		Namespace:  aws.String(namespace),
		MetricData: metricDatum,
	})

	if err != nil {
		s.Logger.Printf("error: failed to send stat to cloudwatch, err: %s", err.Error())
	}
	return
}

func (s *Statter) getDefaultMetricDimension() (dimension []*cloudwatch.Dimension) {
	dimension = []*cloudwatch.Dimension{
		&cloudwatch.Dimension{
			Name:  aws.String("Environment"),
			Value: aws.String(s.Environment),
		},
	}
	return
}

// buildMetricName concats  metricname and action using a '.'
func buildMetricName(metricName, action string) (name string) {
	return fmt.Sprintf("%s.%s", metricName, action)
}

//####### Helpers to attch stat as middleware to apiserver #####
type mwResponseWriter struct {
	http.ResponseWriter
	status int
}

func newMwResponseWriter(w http.ResponseWriter) *mwResponseWriter {
	// Default the status code to 200
	return &mwResponseWriter{ResponseWriter: w, status: http.StatusOK}
}

func (w *mwResponseWriter) statusCode() int {
	return w.status
}

func (w *mwResponseWriter) WriteHeader(statusCode int) {
	// Store the status code
	w.status = statusCode

	// Write the status code onward.
	w.ResponseWriter.WriteHeader(statusCode)
}

func (s Statter) ServeHTTP(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	startTime := time.Now()
	mwrw := newMwResponseWriter(w)
	next.ServeHTTP(mwrw, r)
	statusCode := mwrw.statusCode()
	metricName := fmt.Sprintf("apiserver.%s.%s", r.Method, r.URL.Path)
	metricSuffix := "success"
	if statusCode >= 400 {
		metricSuffix = "failure"
	}
	s.incStat(buildMetricName(metricName, metricSuffix))
	s.timingDurationStat(buildMetricName(metricName, "time"), startTime)
}
