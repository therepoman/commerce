package contextkeys

const (
	UserIDCtxKey            = "VerifiedTwitchUserId"
	RequestLogKey           = "RequestLog"
	RequestMetricsReportKey = "RequestMetricsReport"
)
