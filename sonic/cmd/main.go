package main

import (
	"net/http"

	"code.justin.tv/commerce/sonic/api"
	"code.justin.tv/commerce/sonic/authentication"
	"code.justin.tv/commerce/sonic/config"
	"code.justin.tv/commerce/sonic/middleware"
	st "code.justin.tv/commerce/sonic/sonic-twirp"

	"github.com/sirupsen/logrus"

	"goji.io"
	"goji.io/pat"
)

type SonicHealthService struct {
	serviceConfig *config.SonicConfiguration
}

func NewSonicHealthService(serviceConfig *config.SonicConfiguration) SonicHealthService {
	return SonicHealthService{serviceConfig}
}

func (service *SonicHealthService) HandleHealth(w http.ResponseWriter, r *http.Request) {
	safeWrite(w, "OK\n")
	return
}

func (service *SonicHealthService) HandleStatus(w http.ResponseWriter, r *http.Request) {
	// Note the host configuration
	if service.serviceConfig.IsProd() {
		safeWrite(w, "Configuration : PROD\n")
	} else if service.serviceConfig.IsOnebox() {
		safeWrite(w, "Configuration : ONEBOX\n")
	} else if service.serviceConfig.IsStaging() {
		safeWrite(w, "Configuration : STAGING\n")
	} else if service.serviceConfig.IsLocal() {
		safeWrite(w, "Configuration : LOCAL\n")
	} else {
		safeWrite(w, "Configuration : UNKNOWN\n")
	}
	return
}

func main() {
	configuration := config.GetCurrentConfiguration()
	sonicService := api.NewSonicService(&configuration)
	soniceHealthService := NewSonicHealthService(&configuration)

	requestContextMiddleware := middleware.NewRequestContextMiddleware(&configuration)

	twirpHandler := st.NewSonicServer(&sonicService, nil, nil)
	mux := goji.NewMux()

	mux.Use(requestContextMiddleware.GetHandler())
	mux.Use(middleware.NewAuthMiddleware(authentication.NewCartmanAuthenticationHandler(&configuration)))

	mux.Handle(pat.Post(st.SonicPathPrefix+"*"), twirpHandler)
	mux.HandleFunc(pat.Get("/Health"), soniceHealthService.HandleHealth)
	mux.HandleFunc(pat.Get("/Status"), soniceHealthService.HandleStatus)

	server := &http.Server{
		Addr:    ":8080",
		Handler: mux,
	}

	serverLogger := logrus.New()
	serverLogger.Fatal(server.ListenAndServe())
}

func safeWrite(w http.ResponseWriter, str string) {
	_, err := w.Write([]byte(str))
	if err != nil {
		panic(err)
	}
}
