import argparse
import base64
import boto3
from botocore.exceptions import ClientError
import json
import logging
import re
import shutil
import os
from time import sleep

DEV_TEMPLATE_PATH = 'cloudformation/dev-pipeline-template.json'
PROD_TEMPLATE_PATH = 'cloudformation/prod-pipeline-template.json'
LAMBDA_FILE_PATH = 'cloudformation/lambda'


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def main():
    logging.basicConfig()

    args = parse_args()
    dev_session = get_session('dev', args.dev_profile_name, args.dev_odin_material_set)
    prod_session = get_session('prod', args.prod_profile_name, args.prod_odin_material_set)
    validator = ArgsValidator(args, dev_session, prod_session)
    if not validator.is_valid():
        return

    bootstrapper = ServiceBootstrapper(
        service_name=args.service_name,
        code_commit_branch=args.code_commit_branch,
        code_commit_repository=args.code_commit_repository,
        dev_vpc_id=args.dev_vpc_id,
        dev_vpc_security_group_id=args.dev_vpc_security_group_id,
        dev_vpc_subnet=args.dev_vpc_subnet,
        prod_vpc_id=args.prod_vpc_id,
        prod_vpc_security_group_id=args.prod_vpc_security_group_id,
        prod_vpc_subnet=args.prod_vpc_subnet,
        dev_service_role=args.dev_service_role_arn,
        prod_service_role=args.prod_service_role_arn,
        dev_session=dev_session,
        prod_session=prod_session
    )
    bootstrapper.bootstrap(args.skip_dev_bootstrap, args.skip_prod_bootstrap)


def parse_args():
    parser = argparse.ArgumentParser(description='Bootstrap a service using cloud formation.')
    parser.add_argument('--service-name', help='The name of the service.  This will be used to create stack and resource names.')
    parser.add_argument('--code-commit-branch', help='Branch name for code commit')
    parser.add_argument('--code-commit-repository', help='Repository name for code commit')
    parser.add_argument('--prod-vpc-id', help='Prod VPC id')
    parser.add_argument('--prod-vpc-security-group-id', help='Prod VPC security group id')
    parser.add_argument('--prod-vpc-subnet', nargs='+', help='Prod VPC subnet.  Can specify multiple separated by spaces.')
    parser.add_argument('--dev-vpc-id', help='Dev VPC id')
    parser.add_argument('--dev-vpc-security-group-id', help='Dev VPC security group id')
    parser.add_argument('--dev-vpc-subnet', nargs='+', help='Dev VPC subnet.  Can specify multiple separated by spaces.')

    parser.add_argument('--dev-service-role-arn', help='ARN of the IAM Role the DEV service will use to call AWS.')
    parser.add_argument('--prod-service-role-arn', help='ARN of the IAM Role the PROD service will use to call AWS.')

    parser.add_argument('--dev-profile-name', help='Profile name for dev account credentials, usually stored in ~/.aws/credentials')
    parser.add_argument('--dev-odin-material-set', help='Odin material set name for the dev account credentials.')
    parser.add_argument('--prod-profile-name', help='Profile name for prod account credentials, usually stored in ~/.aws/credentials')
    parser.add_argument('--prod-odin-material-set', help='Odin material set name for the prod account credentials.')

    parser.add_argument('--skip-dev-bootstrap', action='store_true', help='Skip creating the stack in the dev account using a bootstrap template.')
    parser.add_argument('--skip-prod-bootstrap', action='store_true', help='Skip creating the stack in the prod account using a bootstrap template.')

    return parser.parse_args()


class ArgsValidator:
    def __init__(self, args, dev_session, prod_session):
        self.args = args
        self.dev_session = dev_session
        self.prod_session = prod_session

    def is_valid(self):
        is_valid = True

        if not self.args.service_name:
            is_valid = False
            print('Missing --service-name parameter.')

        if not self.args.code_commit_branch:
            is_valid = False
            print('Missing --code-commit-branch parameter.')

        if not self.args.code_commit_repository:
            is_valid = False
            print('Missing --code-commit-repository parameter.')

        if not self.args.dev_service_role_arn:
            is_valid = False
            print('Missing --dev-service-role-arn parameter.')

        if not self.args.prod_service_role_arn:
            is_valid = False
            print('Missing --prod-service-role-arn parameter.')

        is_valid = self.is_vpc_valid(
            self.prod_session, self.args.prod_vpc_id, self.args.prod_vpc_security_group_id, self.args.prod_vpc_subnet,
            '--prod-vpc-id', '--prod-vpc-security-group-id', '--prod-vpc-subnet') and is_valid

        is_valid = self.is_vpc_valid(
            self.dev_session, self.args.dev_vpc_id, self.args.dev_vpc_security_group_id, self.args.dev_vpc_subnet,
            '--dev-vpc-id', '--dev-vpc-security-group-id', '--dev-vpc-subnet') and is_valid

        return is_valid

    def is_vpc_valid(self, session, vpc_id, security_group_id, subnets, vpc_id_arg, security_group_id_arg, subnet_arg):
        is_valid = True

        vpcs = set()
        if not vpc_id:
            is_valid = False
            print('Missing {} parameter.'.format(vpc_id_arg))
            for vpc in session.client('ec2').describe_vpcs()['Vpcs']:
                print('    {}'.format(vpc['VpcId']))
                vpcs.add(vpc['VpcId'])

        security_groups_by_vpc = {}
        if not security_group_id:
            is_valid = False
            print('Missing {} parameter.'.format(security_group_id_arg))
            for security_group in session.client('ec2').describe_security_groups()['SecurityGroups']:
                print('    {}: {} {}: {}'.format(security_group['GroupId'], security_group['VpcId'],
                    security_group['GroupName'], security_group['Description']))
                if security_group['VpcId'] not in security_groups_by_vpc:
                    security_groups_by_vpc[security_group['VpcId']] = []
                security_groups_by_vpc[security_group['VpcId']].append(security_group['GroupId'])

        subnets_by_vpc = {}
        if not subnets:
            is_valid = False
            print('Missing {} parameter.  More than one subnet can be used.'.format(subnet_arg))
            for subnet in session.client('ec2').describe_subnets()['Subnets']:
                print('    {}: {} {} {}'.format(subnet['SubnetId'], subnet['VpcId'], subnet['AvailabilityZone'],
                    ('MapPublicIpOnLaunch' if subnet['MapPublicIpOnLaunch'] else '')))
                if not subnet['MapPublicIpOnLaunch']:
                    if subnet['VpcId'] not in subnets_by_vpc:
                        subnets_by_vpc[subnet['VpcId']] = []
                    subnets_by_vpc[subnet['VpcId']].append(subnet['SubnetId'])

        examples = []
        for vpc in vpcs:
            subnets = subnets_by_vpc.get(vpc, [])
            if subnets:
                for security_group in security_groups_by_vpc.get(vpc, []):
                    examples.append('{} {} {} {} {} {}'.format(
                        vpc_id_arg, vpc, security_group_id_arg, security_group, subnet_arg, ' '.join(subnets)))

        if examples:
            print('Example:')
            for example in examples:
                print('    {}'.format(example))

        return is_valid


class ServiceBootstrapper:
    def __init__(self, service_name, code_commit_branch, code_commit_repository,
        dev_vpc_id, dev_vpc_security_group_id, dev_vpc_subnet,
        prod_vpc_id, prod_vpc_security_group_id, prod_vpc_subnet,
        dev_service_role, prod_service_role,
        dev_session, prod_session):
        self.service_name = service_name
        self.code_commit_branch = code_commit_branch
        self.code_commit_repository = code_commit_repository
        self.dev_vpc_id = dev_vpc_id
        self.dev_vpc_security_group_id = dev_vpc_security_group_id
        self.dev_vpc_subnet = dev_vpc_subnet
        self.prod_vpc_id = prod_vpc_id
        self.prod_vpc_security_group_id = prod_vpc_security_group_id
        self.prod_vpc_subnet = prod_vpc_subnet
        self.dev_session = dev_session
        self.prod_session = prod_session
        self.cloudformationdev = CloudFormationFacade(self.dev_session)
        self.cloudformationprod = CloudFormationFacade(self.prod_session)
        self.dev_account = self.dev_session.client('sts').get_caller_identity()['Account']
        self.prod_account = self.prod_session.client('sts').get_caller_identity()['Account']
        self.dev_service_role = dev_service_role
        self.prod_service_role = prod_service_role
        self.lambda_s3_bucket = '{0}-lambda-{1}'.format(self.service_name, self.prod_account)

    def bootstrap(self, skip_dev_bootstrap, skip_prod_bootstrap):
        self.upload_lambda_functions()

        if skip_dev_bootstrap:
            dev_stack = self.cloudformationdev.describe_stack(self.service_name)
        else:
            dev_stack = self.bootstrap_dev()
        self.dev_stack_outputs = { entry['OutputKey']: entry['OutputValue'] for entry in dev_stack['Outputs'] }

        if skip_prod_bootstrap:
            prod_stack = self.cloudformationprod.describe_stack(self.service_name)
        else:
            prod_stack = self.bootstrap_prod()
        self.prod_stack_outputs = { entry['OutputKey']: entry['OutputValue'] for entry in prod_stack['Outputs'] }

        self.update_dev()
        self.update_prod()

    def bootstrap_dev(self):
        with open(DEV_TEMPLATE_PATH, 'r') as f:
            template = json.loads(f.read())
        self.process_template_for_bootstrap(template)
        parameters = [
            {'ParameterKey': 'ServiceName', 'ParameterValue': self.service_name},
            {'ParameterKey': 'ServiceRoleArn', 'ParameterValue': self.dev_service_role},
            {'ParameterKey': 'ProdAWSAccountID', 'ParameterValue': self.prod_account},
            {'ParameterKey': 'ProdAWSAccountCodePipelineRoleArn', 'ParameterValue': 'placeholder'},
            {'ParameterKey': 'ProdS3ArtifactBucketArn', 'ParameterValue': 'placeholder'},
            {'ParameterKey': 'ProdCodePipelineArtifactKeyArn', 'ParameterValue': 'placeholder'},
            {'ParameterKey': 'VpcId', 'ParameterValue': self.dev_vpc_id},
            {'ParameterKey': 'TwitchSecurityGroupId', 'ParameterValue': self.dev_vpc_security_group_id},
            {'ParameterKey': 'VpcSubnetIds', 'ParameterValue': ','.join(self.dev_vpc_subnet)},
        ]
        logger.info('Creating stack {} in account {} with parameters {}'.format(self.service_name, self.dev_account,
            [ {param['ParameterKey']: param['ParameterValue']} for param in parameters]))
        return self.cloudformationdev.create_stack(self.service_name, json.dumps(template, indent=1, sort_keys=True), parameters)

    def update_dev(self):
        with open(DEV_TEMPLATE_PATH, 'r') as f:
            template = json.loads(f.read())
        parameters = [
            {'ParameterKey': 'ServiceName', 'ParameterValue': self.service_name},
            {'ParameterKey': 'ServiceRoleArn', 'ParameterValue': self.dev_service_role},
            {'ParameterKey': 'ProdAWSAccountID', 'ParameterValue': self.prod_account},
            {'ParameterKey': 'ProdAWSAccountCodePipelineRoleArn', 'ParameterValue': self.prod_stack_outputs['ProdAWSAccountCodePipelineRoleArn']},
            {'ParameterKey': 'ProdS3ArtifactBucketArn', 'ParameterValue': self.prod_stack_outputs['ProdS3ArtifactBucketArn']},
            {'ParameterKey': 'ProdCodePipelineArtifactKeyArn', 'ParameterValue': self.prod_stack_outputs['ProdCodePipelineArtifactKeyArn']},
            {'ParameterKey': 'VpcId', 'ParameterValue': self.dev_vpc_id},
            {'ParameterKey': 'TwitchSecurityGroupId', 'ParameterValue': self.dev_vpc_security_group_id},
            {'ParameterKey': 'VpcSubnetIds', 'ParameterValue': ','.join(self.dev_vpc_subnet)},
        ]
        logger.info('Updating stack {} in account {} with parameters {}'.format(self.service_name, self.dev_account,
            [ {param['ParameterKey']: param['ParameterValue']} for param in parameters]))
        return self.cloudformationdev.update_stack(self.service_name, json.dumps(template, indent=1, sort_keys=True), parameters)

    def bootstrap_prod(self):
        with open(PROD_TEMPLATE_PATH, 'r') as f:
            template = json.loads(f.read())
        self.process_template_for_bootstrap(template)
        parameters = [
            {'ParameterKey': 'ServiceName', 'ParameterValue': self.service_name},
            {'ParameterKey': 'ServiceRoleArn', 'ParameterValue': self.prod_service_role},
            {'ParameterKey': 'DevAWSAccountID', 'ParameterValue': self.dev_account},
            {'ParameterKey': 'DevAWSAccountECSRoleArn', 'ParameterValue': self.dev_stack_outputs['DevAWSAccountECSRoleArn']},
            {'ParameterKey': 'DevECSClusterArn', 'ParameterValue': 'placeholder'},
            {'ParameterKey': 'DevECSServiceArn', 'ParameterValue': 'placeholder'},
            {'ParameterKey': 'DevECSALB', 'ParameterValue': self.dev_stack_outputs['ECSALB']},
            {'ParameterKey': 'CodeCommitBranchName', 'ParameterValue': self.code_commit_branch},
            {'ParameterKey': 'CodeCommitRepositoryName', 'ParameterValue': self.code_commit_repository},
            {'ParameterKey': 'VpcId', 'ParameterValue': self.prod_vpc_id},
            {'ParameterKey': 'TwitchSecurityGroupId', 'ParameterValue': self.prod_vpc_security_group_id},
            {'ParameterKey': 'VpcSubnetIds', 'ParameterValue': ','.join(self.prod_vpc_subnet)},
            {'ParameterKey': 'LambdaS3Bucket', 'ParameterValue': self.lambda_s3_bucket},
        ]
        s3_object = self.get_s3_bucket().put_object(Key="prod-pipeline-template.json", Body=json.dumps(template, indent=1, sort_keys=True))
        logger.info('Creating stack {} in account {} with parameters {}'.format(self.service_name, self.prod_account,
            [ {param['ParameterKey']: param['ParameterValue']} for param in parameters]))
        return self.cloudformationprod.create_stack(self.service_name, s3_object=s3_object, parameters=parameters)

    def update_prod(self):
        with open(PROD_TEMPLATE_PATH, 'r') as f:
            template = json.loads(f.read())
        parameters = [
            {'ParameterKey': 'ServiceName', 'ParameterValue': self.service_name},
            {'ParameterKey': 'ServiceRoleArn', 'ParameterValue': self.prod_service_role},
            {'ParameterKey': 'DevAWSAccountID', 'ParameterValue': self.dev_account},
            {'ParameterKey': 'DevAWSAccountECSRoleArn', 'ParameterValue': self.dev_stack_outputs['DevAWSAccountECSRoleArn']},
            {'ParameterKey': 'DevECSClusterArn', 'ParameterValue': self.dev_stack_outputs['DevECSClusterArn']},
            {'ParameterKey': 'DevECSServiceArn', 'ParameterValue': self.dev_stack_outputs['DevECSServiceArn']},
            {'ParameterKey': 'DevECSALB', 'ParameterValue': self.dev_stack_outputs['ECSALB']},
            {'ParameterKey': 'CodeCommitBranchName', 'ParameterValue': self.code_commit_branch},
            {'ParameterKey': 'CodeCommitRepositoryName', 'ParameterValue': self.code_commit_repository},
            {'ParameterKey': 'VpcId', 'ParameterValue': self.prod_vpc_id},
            {'ParameterKey': 'TwitchSecurityGroupId', 'ParameterValue': self.prod_vpc_security_group_id},
            {'ParameterKey': 'VpcSubnetIds', 'ParameterValue': ','.join(self.prod_vpc_subnet)},
            {'ParameterKey': 'LambdaS3Bucket', 'ParameterValue': self.lambda_s3_bucket},
        ]
        s3_object = self.get_s3_bucket().put_object(Key="prod-pipeline-template.json", Body=json.dumps(template, indent=1, sort_keys=True))
        logger.info('Updating stack {} in account {} with parameters {}'.format(self.service_name, self.prod_account,
            [ {param['ParameterKey']: param['ParameterValue']} for param in parameters]))
        self.cloudformationprod.update_stack(self.service_name, s3_object=s3_object, parameters=parameters)

    def process_template_for_bootstrap(self, template):
        for resource in template['Resources'].values():
            bootstrap_config = resource.get('Metadata', {}).get('BootstrapTemplate', {})
            for replace_entry in bootstrap_config.get('Replace', []):
                parent, key = self.traverse_path(resource, replace_entry['Path'])
                parent[key] = replace_entry['Value']
            for delete_path in bootstrap_config.get('Delete', []):
                parent, key = self.traverse_path(resource, delete_path)
                del parent[key]

    # Traverses a path to find a target value inside of a nested structure.
    # Returns the parent structure of the target and the key needed to access or change the target.
    def traverse_path(self, root, path_string):
        path = path_string.split('/')
        parent = root
        child_key = None
        for path_element in path:
            # Advance the parent pointer except for the first iteration.
            if child_key != None:
                parent = parent[child_key]
            # If the parent is a list, interpret the path element as an index.
            # This handles paths like .../mylist/0/...
            if isinstance(parent, (list)):
                child_key = int(path_element)
            else:
                # This matches "key1" where key1 is a dictionary key, "key1[key2]" where key2 is a list index,
                # or "key1[key2=value]" where key2=value is used to find the correct dictionary in the list.
                match = re.match(r"([^\[]+)(?:\[([^=]+)(?:=(.*))?\])?", path_element)
                key1, key2, value = match.group(1, 2, 3)
                if isinstance(parent[key1], (list)) and key2 != None:
                    # The child is a list and a key is present, advance parent to the list.
                    parent = parent[key1]
                    if value != None:
                        # Search through the list contents for an object that has the matching property.
                        for index, child in enumerate(parent):
                            if child.get(key2) == value:
                                # Found a matching object.
                                child_key = index
                                break
                        else:
                            raise Exception('Unable to find {} in path {}'.format(path_element, path_string))
                    else:
                        # No value is present, use the key as an index.
                        child_key = int(key2)
                else:
                    # The child is either not a list or there's no key for the list.
                    child_key = key1
        return parent, child_key

    def upload_lambda_functions(self):
        filename = 'lambda'
        shutil.make_archive(filename, 'zip', LAMBDA_FILE_PATH)
        self.get_s3_bucket().upload_file(filename + '.zip', 'functions')
        os.remove(filename + '.zip')

    def get_s3_bucket(self):
        s3 = self.prod_session.resource('s3')
        bucket = s3.Bucket(self.lambda_s3_bucket)
        # Check if bucket exists and create it if not
        try:
            s3.meta.client.head_bucket(Bucket=self.lambda_s3_bucket)
        except ClientError as e:
            error_code = int(e.response['Error']['Code'])
            if error_code == 404:
                bucket.create(CreateBucketConfiguration={'LocationConstraint': self.prod_session.region_name})
                bucket.wait_until_exists()
        return bucket


class CloudFormationFacade:
    def __init__(self, session):
        self.client = session.client('cloudformation')

    def describe_stack(self, stack_name):
        return self.client.describe_stacks(StackName=stack_name)['Stacks'][0]

    def create_stack(self, stack_name, template=None, parameters=[], s3_object=None):
        request = {
            "StackName": stack_name,
            "Parameters": parameters,
            "Capabilities": ['CAPABILITY_IAM']
        }
        if template:
            request["TemplateBody"] = template
        if s3_object:
            request["TemplateURL"] = 'http://{}.s3.amazonaws.com/{}'.format(s3_object.bucket_name, s3_object.key)
        self.client.create_stack(**request)
        return self.wait_for_stack_complete('create', stack_name)

    def update_stack(self, stack_name, template=None, parameters=[], s3_object=None):
        try:
            request = {
                "StackName": stack_name,
                "Parameters": parameters,
                "Capabilities": ['CAPABILITY_IAM']
            }
            if template:
                request["TemplateBody"] = template
            if s3_object:
                request["TemplateURL"] = 'http://{}.s3.amazonaws.com/{}'.format(s3_object.bucket_name, s3_object.key)
            self.client.update_stack(**request)
        except ClientError as e:
            if e.response['Error']['Code'] == 'ValidationError' and 'No updates' in e.response['Error']['Message']:
                logger.info('No updates needed.')
                return
            raise e

        self.wait_for_stack_complete('update', stack_name)

    def wait_for_stack_complete(self, action, stack_name):
        last_status = None
        while True:
            stack = self.describe_stack(stack_name)
            status = stack['StackStatus']
            if status != last_status:
                logger.info('Stack {} status: {}'.format(stack_name, status))
                last_status = status
            if 'IN_PROGRESS' not in status:
                if 'FAILED' in status or 'ROLLBACK' in status:
                    raise RuntimeError('Failed to {} stack {}'.format(action, stack_name))
                break
            sleep(2)
        return stack


def get_session(name, profile, material):
    if profile:
        return boto3.Session(profile_name=profile)

    if material:
        # http.client is only required for odin and doesn't exist on all hosts.
        import http.client

        response = http_get('localhost:2009', '/query?ContentType=JSON&Operation=retrieve&material.materialName={}&material.materialType=Principal'.format(material))
        principal = base64.b64decode(json.loads(response.decode('utf-8'))['material']['materialData']).decode('utf-8')

        response = http_get('localhost:2009', '/query?ContentType=JSON&Operation=retrieve&material.materialName={}&material.materialType=Credential'.format(material))
        credential = base64.b64decode(json.loads(response.decode('utf-8'))['material']['materialData']).decode('utf-8')

        return boto3.Session(aws_access_key_id=principal, aws_secret_access_key=credential)

    raise RuntimeError('Missing {} profile or material set'.format(name))


def http_get(host, request):
    conn = http.client.HTTPConnection(host)
    try:
        conn.request('GET', request)
        response = conn.getresponse()
        if response.status < 200 or response.status >= 300:
            logger.error('Odin response: %s %s', response.status, response.reason)
            logger.error('Odin response body: %s', response.read())
            raise RuntimeError('Odin response status: {}'.format(response.status))
        return response.read()
    finally:
        conn.close()


if __name__ == "__main__":
    main()
