## How to create a new service stack
* The script needs python with boto3.  If using Odin, it also needs the http.client module.  For amazon managed linux, this can be installed from brazil with:
```
brazil-bootstrap --environmentRoot ~/python --environmentType runtime --flavor DEV.STD.PTHREAD --packages Python-default,Boto3-1.x
PATH=~/python/bin:$PATH
```
* Obtain credentials or odin material sets with sufficient permissions on both accounts for creating all of the resources.
* Collect the required parameters.  This can be done by running the script with only credentials.  It will list the available vpc items and print example parameters to copy/paste.
```
python cloudformation/create-service.py --dev-odin-material-set com.amazon.credentials.isengard.1234.user/my-user --prod-odin-material-set com.amazon.credentials.isengard.12345.user/my-user
python cloudformation/create-service.py --dev-profile-name myservicedev --prod-profile-name myservice
```
* Use the AWS Console to create the service stacks using dev-service-template.json and prod-service-template.json.  Copy the service role arn from each of the stacks, expand the Outputs section to see it.  These are used as parameters for the script.
* To create the service from scratch, run it without --skip* parameters.  It will first create both stacks using modified bootstrap templates to work around circular dependencies, then update them with the full template to connect everything properly.
```
python cloudformation/create-service.py \
--service-name test1234 \
--code-commit-branch master \
--code-commit-repository falador \
--dev-vpc-id vpc-1234 --dev-vpc-security-group-id sg-1234 --dev-vpc-subnet subnet-1234 subnet-1234 subnet-1234 \
--prod-vpc-id vpc-1234 --prod-vpc-security-group-id sg-1234 --prod-vpc-subnet subnet-1234 subnet-1234 subnet-1234 \
--dev-service-role-arn arn:aws:iam::12345:role/stackname-service-ServiceRole-12345 \
--prod-service-role-arn arn:aws:iam::12345:role/stackname-service-ServiceRole-12345 \
--dev-odin-material-set com.amazon.credentials.isengard.1234.user/my-user \
--prod-odin-material-set com.amazon.credentials.isengard.12345.user/my-user
```
* If using profiles instead of odin:
```
python cloudformation/create-service.py \
--service-name Test1234 \
--code-commit-branch master \
--code-commit-repository falador \
--prod-vpc-id vpc-8a05aef3 \
--prod-vpc-security-group-id sg-0d0df672 \
--prod-vpc-subnet subnet-fd0fd2a7 subnet-d311feaa subnet-388d8370 \
--dev-service-role-arn arn:aws:iam::12345:role/stackname-service-ServiceRole-12345 \
--prod-service-role-arn arn:aws:iam::12345:role/stackname-service-ServiceRole-12345 \
--dev-profile-name myservicedev \
--prod-profile-name myservice
```

## How to update a service stack
* Once the service is live it would be best to manually upload the template using the AWS console since it will give a summary of what is changing.
* If no sanity checks or confirmation prompts are needed, add the following to the above create command.  It will skip the create calls with bootstrap versions of the templates and do an update to each stack.
```
--skip-dev-bootstrap \
--skip-prod-bootstrap \
```

## How the bootstrap works around circular dependencies
The script loads the actual templates and then processes them to remove anything that prevents the template from being created.

The Metadata section is used to define what changes need to be made to resources.
Metadata/BootstrapTemplate/Delete can be used to remove optional values or structures.
Metadata/BootstrapTemplate/Replace can be used to replace values or structures with any placeholder that is valid and has no real purpose besides filling a required field.

The placeholder will only be there temporarily.
Once the bootstrap templates have created the resources, the script will update the stacks without doing the BootstrapTemplate processing so that they have the correct values.

CloudFormation ignores custom Metadata and just stores it as is, so it's safe to manually load these templates directly into CloudFormation.
```
"MyResource": {
    "Metadata": {
        "BootstrapTemplate": {
            "Delete": [
                "Properties/Property1[Name=B]"
            ],
            "Replace": [
                {"Path": "Properties/Property2", "Value": "PlaceholderValue"}
            ]
        }
    },
    "Properties": {
        "Property1": [
            {"Name": "A", "Key1": "AcceptableValue"},
            {"Name": "B", "Key1": "OptionalReferenceToNonExistentItem"},
        ]
        "Property2": "RequiredReferenceToNonExistentItem"
    }
}
```
is processed into:
```
"MyResource": {
    "Metadata": {
        "BootstrapTemplate": {
            "Delete": [
                "Properties/Property1[Name=B]"
            ],
            "Replace": [
                {"Path": "Properties/Property2", "Value": "PlaceholderValue"}
            ]
        }
    },
    "Properties": {
        "Property1": [
            {"Name": "A", "Key1": "AcceptableValue"},
        ]
        "Property2": "PlaceholderValue"
    }
}
```

