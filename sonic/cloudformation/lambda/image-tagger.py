from boto3.session import Session

import boto3
import botocore
import json
import tempfile
import traceback
import zipfile

ecr = boto3.client('ecr')
code_pipeline = boto3.client('codepipeline')


def get_image_def_from_s3(s3, artifact):
    bucket = artifact['location']['s3Location']['bucketName']
    key = artifact['location']['s3Location']['objectKey']

    with tempfile.NamedTemporaryFile() as tmp_file:
        s3.download_file(bucket, key, tmp_file.name)
        with zipfile.ZipFile(tmp_file.name, 'r') as zip:
            return json.loads(zip.read('imagedefinitions.json'))[0]


def setup_s3_client(job_data):
    key_id = job_data['artifactCredentials']['accessKeyId']
    key_secret = job_data['artifactCredentials']['secretAccessKey']
    session_token = job_data['artifactCredentials']['sessionToken']

    session = Session(aws_access_key_id=key_id,
                      aws_secret_access_key=key_secret,
                      aws_session_token=session_token)
    return session.client('s3', config=botocore.client.Config(signature_version='s3v4'))


def put_job_success(job, message):
    print('Putting job success')
    print(message)
    code_pipeline.put_job_success_result(jobId=job)


def put_job_failure(job, message):
    print('Putting job failure')
    print(message)
    code_pipeline.put_job_failure_result(jobId=job, failureDetails={
                                         'message': message, 'type': 'JobFailed'})


def tag_image(image_def, tag):
    repository_name = image_def['repositoryName']
    image_tag = image_def['imageTag']

    try:
        response = ecr.batch_get_image(
            repositoryName=repository_name,
            imageIds=[
                {
                    'imageTag': image_tag
                }
            ]
        )

        if len(response['failures']) > 0:
            failure_code = response['failures'][0]['failureCode']
            failure_reason = response['failures'][0]['failureReason']
            raise Exception('{0}: {1}'.format(failure_code, failure_reason))

        if len(response['images']) != 1:
            raise Exception('Too many images found for image definition "{0}"'.format(
                json.dumps(image_def)))

        image_manifest = response['images'][0]['imageManifest']
        ecr.put_image(
            repositoryName=repository_name,
            imageManifest=image_manifest,
            imageTag=tag
        )

    except botocore.exceptions.ClientError as e:
        raise Exception('Error updating image tag for image definiton "{0}"'.format(
            json.dumps(image_def)), e)


def lambda_handler(event, context):
    try:
        # Extract the Job ID
        job_id = event['CodePipeline.job']['id']

        # Extract the Job Data
        job_data = event['CodePipeline.job']['data']

        # Extract the tag param
        tag = job_data['actionConfiguration']['configuration']['UserParameters']

        # Get the artifact passed to the function
        artifact = job_data['inputArtifacts'][0]

        # Get S3 client to access artifact with
        s3 = setup_s3_client(job_data)

        # Get the image definitions from S3
        image_def = get_image_def_from_s3(s3, artifact)

        # Update tag on image
        tag_image(image_def, tag)

        # Post job success to CodePipeline
        put_job_success(job_id, 'Image tag update complete')

    except Exception as e:
        # If any other exceptions which we didn't expect are raised
        # then fail the job and log the exception message.
        print('Function failed due to exception.')
        print(e)
        traceback.print_exc()
        put_job_failure(job_id, 'Function exception: ' + str(e))

    print('Function complete.')
    return "Complete."
