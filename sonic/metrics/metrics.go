package metrics

import (
	"time"

	"code.justin.tv/commerce/AmazonMWSGoClient/mws"
	"code.justin.tv/commerce/sonic/config"
)

const (
	hostSpecific  = false
	serviceName   = "Sonic"
	environment   = "Sonic/NA"
	hostname      = "AWS-ELB"
	partitionID   = ""
	customerID    = ""
	metricsRegion = "PDX" // PDX is us-west-2.

	minPeriod = mws.PeriodOneMinute
	maxPeriod = mws.PeriodOneHour

	metricTypeTime = "Time"

	// Exported metrics types that can be used for counts.
	// MetricTypeSuccesses is the number of successes for a provided API.
	MetricTypeSuccesses = "Success"
	// MetricTypeServerError the number of server errors for a provided API.
	MetricTypeServerError = "Server_Error"
	// MetricTypeClientError the number of client errors for a provided API.
	MetricTypeClientError = "Client_Error"
)

type MetricsReporter interface {
	Publish() error
	PostStatusMetric(methodName string, clientString string, status int)
	PostCountMetric(methodName string, clientString string, value float64, metricType string)
	PostLatencyMetric(methodName string, clientString string, latency time.Duration)
	PostLatencyMetricWithName(methodName string, clientString string, metricName string, latency time.Duration)
}

type SonicMetricReport struct {
	report       *mws.MetricReport
	reportConfig *config.SonicConfiguration
}

func statusToMetricType(status int) string {
	var metricType = ""
	if status < 300 {
		// Anything below 300 is a success.
		metricType = MetricTypeSuccesses
	} else if status < 500 {
		// Anything between 300 and 500 is a client error of some sort.
		metricType = MetricTypeClientError
	} else {
		// Anything above 500 is a server error.
		metricType = MetricTypeServerError
	}

	return metricType
}

func (this *SonicMetricReport) Publish() error {
	// Don't publish metrics outside of staging/ prod
	// Technically don't need to check OneBox, since this is covered by Prod, but this is safer in case it's ever changed
	if this.reportConfig.IsStaging() || this.reportConfig.IsOnebox() || this.reportConfig.IsProd() {
		req := mws.NewPutMetricsForAggregationRequest()
		req.AddMetricReport(*this.report)

		_, metricsErr := mws.PutMetricsForAggregation(req, mws.Regions[metricsRegion], this.reportConfig.GetSandstormAwsCredentials())
		return metricsErr
	}
	return nil
}

func (this *SonicMetricReport) PostStatusMetric(methodName string, clientString string, status int) {
	this.PostCountMetric(methodName, clientString, 1.0, statusToMetricType(status))
}

func (this *SonicMetricReport) PostCountMetric(methodName string, clientString string, value float64, metricType string) {
	// Initialize the metric
	countMetric := mws.NewMetric(this.reportConfig.IsProd(), hostname, hostSpecific, this.reportConfig.GetAwsRegion(), serviceName, methodName, clientString, metricType)

	// Add the success value.
	countMetric.AddValue(value)
	countMetric.Unit = "Count"
	countMetric.Timestamp = time.Now().UTC()

	this.report.AddMetric(countMetric)
}

func (this *SonicMetricReport) PostLatencyMetric(methodName string, clientString string, latency time.Duration) {
	this.PostLatencyMetricWithName(methodName, clientString, metricTypeTime, latency)
}

func (this *SonicMetricReport) PostLatencyMetricWithName(methodName string, clientString string, metricName string, latency time.Duration) {
	this.postDecimalMetricWithMetricNameAndUnit(methodName, clientString, latency.Seconds()*1000.0, metricName, mws.UnitMilliseconds)
}

func (this *SonicMetricReport) postDecimalMetricWithMetricNameAndUnit(methodName string, clientString string, decimal float64, metricName string, unit string) {
	// Initialize the metric object.
	decimalMetric := mws.NewMetric(this.reportConfig.IsProd(), hostname, hostSpecific, this.reportConfig.GetAwsRegion(), serviceName, methodName, clientString, metricName)

	// Add the actual decimal value to the metric.
	decimalMetric.AddValue(decimal)
	decimalMetric.Unit = unit
	decimalMetric.Timestamp = time.Now().UTC()

	this.report.AddMetric(decimalMetric)
}

func NewSonicMetricReport(sonicConfig *config.SonicConfiguration) MetricsReporter {
	metricReport := mws.NewMetricReport(sonicConfig.IsProd(), hostname, minPeriod, maxPeriod, partitionID, environment, customerID)
	sonicReport := &SonicMetricReport{}
	sonicReport.report = &metricReport
	sonicReport.reportConfig = sonicConfig
	return sonicReport
}
