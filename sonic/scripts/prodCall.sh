if [ -z "$1" ]
  then
    echo "No authorization token was provided. Call getCreds to retrieve one."
    exit 1
fi

if [ -z "$2" ]
  then
    echo "No tuid was supplied."
    exit 1
fi

curl --request "POST" \
     --location "http://internal-sonic-ECSALB-139866694.us-west-2.elb.amazonaws.com/twirp/code.justin.tv.commerce.sonic.Sonic/HasLinkedAmazonAccount" \
     --header "Content-Type:application/json" \
     --header "Twitch-Authorization:$1" \
     --data '{"tuid": "'$2'"}' \
     --verbose
