if [ -z "$1" ]
  then
    echo "No OAuth token was supplied. You need an OAuth token in order to retrieve credentials. You can retrieve one from the Twitch website."
    exit 1
fi

curl "https://cartman-dev.twitch.a2z.com/authorization_token?capabilities=cartman%3A%3Aauthenticate_user&key=ecc.key" -H "Authorization: OAuth $1"
