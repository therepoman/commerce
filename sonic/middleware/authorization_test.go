package middleware

import (
	"context"
	"errors"
	"net/http"
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	//
	"code.justin.tv/commerce/sonic/contextkeys"
	"code.justin.tv/commerce/sonic/mocks"
	"code.justin.tv/commerce/sonic/sonictesting"
)

const (
	Tuid = "123487352"
)

type VerifiedTuidRecorder struct {
	recordedTuid *string
}

func (v *VerifiedTuidRecorder) GetRecordedTuid() *string {
	return v.recordedTuid
}

func (v *VerifiedTuidRecorder) SetRecordedTuid(s *string) {
	v.recordedTuid = s
}

func (v *VerifiedTuidRecorder) Reset() {
	v.recordedTuid = nil
}

type VerifiedTuidRecordingHandler struct {
	recorder *VerifiedTuidRecorder
}

func (h VerifiedTuidRecordingHandler) GetRecordedTuid() *string {
	return h.recorder.GetRecordedTuid()
}

func (h VerifiedTuidRecordingHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	verifiedTuid := GetVerifiedTwitchUserIdFromContext(r.Context())
	h.recorder.SetRecordedTuid(&verifiedTuid)
}

func TestGetVerifiedTwitchUserIdFromContext(t *testing.T) {
	ctx := context.WithValue(context.Background(), contextkeys.UserIDCtxKey, Tuid)
	ctx2 := context.WithValue(context.Background(), contextkeys.UserIDCtxKey, true)
	ctx3 := context.Background()

	assert.Equal(t, Tuid, GetVerifiedTwitchUserIdFromContext(ctx))
	assert.Equal(t, "", GetVerifiedTwitchUserIdFromContext(ctx2))
	assert.Equal(t, "", GetVerifiedTwitchUserIdFromContext(ctx3))
}

func TestAuthMiddlewareParseError(t *testing.T) {
	mockAuthenticationHandler := &mocks.AuthenticationHandler{}
	tuidRecorder := VerifiedTuidRecorder{}
	httpHandler := VerifiedTuidRecordingHandler{&tuidRecorder}
	responseWriter := sonictesting.EmptyResponseWriter{}
	emptyRequest, _ := http.NewRequest("POST", "junk", nil)

	logRecorder := sonictesting.CallRecorder{}
	writer := sonictesting.NewMockWriter(&logRecorder)
	log.SetOutput(writer)
	ctx := context.WithValue(context.Background(), contextkeys.RequestLogKey, log.WithFields(log.Fields{}))

	mockAuthenticationHandler.On("ParseToken", mock.Anything).Return(nil, errors.New("test parsing error"))

	emptyRequest = emptyRequest.WithContext(ctx)

	authMiddleware := NewAuthMiddleware(mockAuthenticationHandler)
	handler := authMiddleware(httpHandler)

	handler.ServeHTTP(&responseWriter, emptyRequest)
	recordedTuid := httpHandler.GetRecordedTuid()
	assert.Equal(t, "", *recordedTuid)
	mockAuthenticationHandler.AssertNumberOfCalls(t, "ParseToken", 1)
}

func TestAuthMiddlewareValidationError(t *testing.T) {
	mockAuthenticationHandler := &mocks.AuthenticationHandler{}
	mockAuthenticationToken := &mocks.AuthToken{}
	tuidRecorder := VerifiedTuidRecorder{}
	httpHandler := VerifiedTuidRecordingHandler{&tuidRecorder}
	responseWriter := sonictesting.EmptyResponseWriter{}
	emptyRequest, _ := http.NewRequest("POST", "junk", nil)

	logRecorder := sonictesting.CallRecorder{}
	writer := sonictesting.NewMockWriter(&logRecorder)
	log.SetOutput(writer)
	ctx := context.WithValue(context.Background(), contextkeys.RequestLogKey, log.WithFields(log.Fields{}))

	mockAuthenticationHandler.On("ParseToken", mock.Anything).Return(mockAuthenticationToken, nil)
	mockAuthenticationHandler.On("ValidateToken", mock.Anything).Return(errors.New("test auth validation error"))

	emptyRequest = emptyRequest.WithContext(ctx)

	authMiddleware := NewAuthMiddleware(mockAuthenticationHandler)
	handler := authMiddleware(httpHandler)

	handler.ServeHTTP(&responseWriter, emptyRequest)
	recordedTuid := httpHandler.GetRecordedTuid()
	assert.Equal(t, "", *recordedTuid)
	assert.Equal(t, true, logRecorder.WasCalled())
	mockAuthenticationHandler.AssertNumberOfCalls(t, "ParseToken", 1)
	mockAuthenticationHandler.AssertNumberOfCalls(t, "ValidateToken", 1)
}

func TestAuthMiddlewareSuccess(t *testing.T) {
	mockAuthenticationHandler := &mocks.AuthenticationHandler{}
	mockAuthenticationToken := &mocks.AuthToken{}
	tuidRecorder := VerifiedTuidRecorder{}
	httpHandler := VerifiedTuidRecordingHandler{&tuidRecorder}
	responseWriter := sonictesting.EmptyResponseWriter{}
	emptyRequest, _ := http.NewRequest("POST", "junk", nil)

	logRecorder := sonictesting.CallRecorder{}
	writer := sonictesting.NewMockWriter(&logRecorder)
	log.SetOutput(writer)
	ctx := context.WithValue(context.Background(), contextkeys.RequestLogKey, log.WithFields(log.Fields{}))

	mockAuthenticationHandler.On("ParseToken", mock.Anything).Return(mockAuthenticationToken, nil)
	mockAuthenticationHandler.On("ValidateToken", mock.Anything).Return(nil)
	mockAuthenticationToken.On("GetSubject", mock.Anything).Return(Tuid)

	emptyRequest = emptyRequest.WithContext(ctx)

	authMiddleware := NewAuthMiddleware(mockAuthenticationHandler)
	handler := authMiddleware(httpHandler)

	handler.ServeHTTP(&responseWriter, emptyRequest)
	recordedTuid := httpHandler.GetRecordedTuid()
	assert.Equal(t, Tuid, *recordedTuid)
	mockAuthenticationHandler.AssertNumberOfCalls(t, "ParseToken", 1)
	mockAuthenticationHandler.AssertNumberOfCalls(t, "ValidateToken", 1)
}
