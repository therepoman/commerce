package middleware

import (
	"context"
	"net/http"
	"strings"
	"time"

	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"

	"code.justin.tv/commerce/sonic/config"
	"code.justin.tv/commerce/sonic/contextkeys"
	"code.justin.tv/commerce/sonic/metrics"
)

type StatusTrackingResponseWriter struct {
	http.ResponseWriter
	// HTTP status code
	Status int
}

// WriteHeader is overridden to capture the status code.
func (w *StatusTrackingResponseWriter) WriteHeader(status int) {
	w.Status = status
	w.ResponseWriter.WriteHeader(status)
}

type RequestContextMiddleware struct {
	configuration *config.SonicConfiguration
}

const (
	UnknownRequestId  = "Unknown"
	UnknownMethodName = "UnknownMethod"
)

func NewRequestContextMiddleware(c *config.SonicConfiguration) *RequestContextMiddleware {
	return &RequestContextMiddleware{c}
}

func (m *RequestContextMiddleware) GetHandler() func(http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {
				requestStart := time.Now()

				// Use the URL path to determine the method name (the last part of the URL)
				splitPath := strings.Split(r.URL.Path, "/")
				methodName := splitPath[len(splitPath)-1]

				updatedContext := initializeRequestContext(m.configuration, r.Context(), methodName)
				updatedRequest := r.WithContext(updatedContext)
				statusTrackingWriter := &StatusTrackingResponseWriter{w, http.StatusOK}

				h.ServeHTTP(statusTrackingWriter, updatedRequest)

				requestTime := time.Now().Sub(requestStart)
				sonicMetricReport := GetMetricReportFromContext(updatedContext)
				sonicMetricReport.PostStatusMetric(methodName, "", statusTrackingWriter.Status)
				sonicMetricReport.PostLatencyMetric(methodName, "", requestTime)
				requestLogger := GetLoggerFromContext(updatedContext)
				go reportMetrics(sonicMetricReport, requestLogger)
			},
		)
	}
}

func reportMetrics(report metrics.MetricsReporter, logger *log.Entry) {
	publishError := report.Publish()
	if publishError != nil {
		logger.Error("Error publishing metrics. ", publishError)
	}
}

func GetMetricReportFromContext(ctx context.Context) metrics.MetricsReporter {
	metricReport, ok := ctx.Value(contextkeys.RequestMetricsReportKey).(metrics.MetricsReporter)
	if ok {
		return metricReport
	} else {
		logger := log.WithFields(log.Fields{
			"request.id": UnknownRequestId,
			"method":     UnknownMethodName,
		})
		logger.Error("Error retrieving metric report from context. Returning re-created report.")
		recreatedConfig := config.GetCurrentConfiguration()
		return metrics.NewSonicMetricReport(&recreatedConfig)
	}
}

func GetLoggerFromContext(ctx context.Context) *log.Entry {
	logger, ok := ctx.Value(contextkeys.RequestLogKey).(*log.Entry)
	if ok {
		return logger
	} else {
		logger = log.WithFields(log.Fields{
			"request.id": UnknownRequestId,
			"method":     UnknownMethodName,
		})
		logger.Error("Error getting logger from context. Returning anonymous logger.")
		return logger
	}
}

func initializeRequestContext(config *config.SonicConfiguration, ctx context.Context, methodName string) context.Context {
	requestID := uuid.NewV4().String()

	metricReport := metrics.NewSonicMetricReport(config)
	loggerFields := log.Fields{
		"request.id": requestID,
		"method":     methodName,
	}

	updatedContext := context.WithValue(ctx, contextkeys.RequestLogKey, log.WithFields(loggerFields))
	updatedContext = context.WithValue(updatedContext, contextkeys.RequestMetricsReportKey, metricReport)

	return updatedContext
}
