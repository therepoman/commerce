package middleware

import (
	"context"
	"net/http"

	"code.justin.tv/commerce/sonic/authentication"
	"code.justin.tv/commerce/sonic/contextkeys"
)

func NewAuthMiddleware(handler authentication.AuthenticationHandler) func(h http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			logger := GetLoggerFromContext(r.Context())
			token, parseError := handler.ParseToken(r)

			if parseError != nil {
				h.ServeHTTP(w, r) // token could not be parsed, so we don't know which tuid is trying to call us.
				return
			}

			validationError := handler.ValidateToken(token)
			if validationError != nil {
				logger.Error("Auth validation failed.", validationError)
				h.ServeHTTP(w, r) // token could not be validated, so we can't ensure which tuid is trying to call us.
				return
			}

			userID := token.GetSubject()
			ctx := context.WithValue(r.Context(), contextkeys.UserIDCtxKey, userID)

			h.ServeHTTP(w, r.WithContext(ctx))
			return
		})
	}
}

func GetVerifiedTwitchUserIdFromContext(ctx context.Context) string {
	userID, ok := ctx.Value(contextkeys.UserIDCtxKey).(string)
	if ok {
		return userID
	} else {
		return ""
	}
}
