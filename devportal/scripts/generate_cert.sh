DIR="ssl"
mkdir -p "$DIR"
openssl req -nodes -x509 -newkey rsa:2048 -keyout "$DIR/server.key" -out "$DIR/server.crt" -days 1001 -reqexts SAN -extensions SAN -config "$DIR/local.cnf"

if [[ "$OSTYPE" == "darwin"* ]]; then
  echo "Installing cert..."
  sudo security add-trusted-cert -d -p ssl -r trustRoot -k "/Library/Keychains/System.keychain" "$DIR/server.crt"
else
  echo "Please install and trust cert at $DIR/server.crt"
fi
