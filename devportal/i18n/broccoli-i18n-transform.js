var Filter = require('broccoli-filter');
var path = require('path');

/**
  Converts JSON file into JS function to be used by the Ember App
  It's been suggested to leave the function as JSON.parse("{JSON_Stringified}") because
  Javascript can parse a JSON string faster than the browser can parse an object literal.
**/

I18nTransform.prototype = Object.create(Filter.prototype);
I18nTransform.prototype.constructor = I18nTransform;
function I18nTransform(inputNode) {
  Filter.call(this, inputNode);
}

I18nTransform.prototype.extensions = ['json'];
I18nTransform.prototype.targetExtension = 'js';

function escapeQuotes(str) {
  return str.replace(/\\([\s\S])|(")/g,"\\$1$2");
}

I18nTransform.prototype.processString = function(content, relativePath) {
  var locale = path.basename(relativePath, '.json');
  content = JSON.stringify(JSON.parse(content)); // Confirming this is a valid JSON object
  
  content = escapeQuotes(content);
  var jsonFuncStr = `function getTranslations() {return {"${locale}": JSON.parse("${content}")};}`;
  
  try {
    // Confirming the JS function is well formed
    eval(`(${jsonFuncStr})`);
  } catch (e) {
    var errorMsg = `Error validating the JS file for this locale. Most likely some character(s) in the translations is not being escaped correctly. \n${e}`;
    throw new Error(errorMsg);
  }

  return jsonFuncStr;
};

module.exports = I18nTransform;
