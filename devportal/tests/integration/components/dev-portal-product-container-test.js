/* global server */
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import startMirage from '../../helpers/start-mirage';

moduleForComponent('dev-portal-product-container', 'Integration | Component | dev-portal-product container', {
  integration: true,
  beforeEach() {
    startMirage(this.container);
  },
  afterEach() {
    window.server.shutdown();
  }
});

test('it lists the dev portal product names', function(assert) {
  const model = server.createList('dev-portal-product', 5);

  this.set('model', model);
  this.render(hbs`{{dev-portal-product-container devPortalProducts=model}}`);

  const text = this.$().text().trim();
  model.forEach(function(devPortalProduct) {
    assert.ok(text.indexOf(devPortalProduct.title) !== -1, `${devPortalProduct.title} appears in text`);
  });
});
