/* global server */
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import testSelector from 'ember-test-selectors';
import startMirage from '../../helpers/start-mirage';

moduleForComponent('igc-container', 'Integration | Component | igc container', {
  integration: true,
  beforeEach() {
    startMirage(this.container);
  },
  afterEach() {
    window.server.shutdown();
  }
});

test('it lists the in-game content for the product', function(assert) {
  const igcList = server.createList('in-game-content', 5);

  this.set('igcList', igcList);
  this.render(hbs`{{igc-container igc=igcList}}`);

  let titles = this.$(testSelector('igc-item-title')).text();

  igcList.forEach((igc) => {
    assert.ok(titles.indexOf(igc.title) !== -1);
  });
});
