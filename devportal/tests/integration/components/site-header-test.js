import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('site-header', 'Integration | Component | site header', {
  integration: true
});

test('it renders', function(assert) {

  this.render(hbs`{{site-header}}`);

  assert.ok(this.$().text(), 'There was some sort of text here.');

});
