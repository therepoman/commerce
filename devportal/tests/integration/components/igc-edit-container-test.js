/* global server */
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import testSelector from 'ember-test-selectors';
import startMirage from '../../helpers/start-mirage';

moduleForComponent('igc-edit-container', 'Integration | Component | igc edit container', {
  integration: true,
  beforeEach() {
    startMirage(this.container);
  },
  afterEach() {
    window.server.shutdown();
  }
});

test('changing the title input updates the igc document', function(assert) {
  const igc = server.create('in-game-content');

  this.set('igc', igc);
  this.render(hbs`{{igc-edit-container igcItem=igc}}`);

  let title = this.$(testSelector('igc-input', 'title'));

  let empty = '';
  title.val(empty);
  title.trigger('change');
  assert.equal(igc.title, empty);

  let newValue = 'new value';
  title.val(newValue);
  title.trigger('change');
  assert.equal(igc.title, newValue);
});

test('changing the SKU input updates the igc document', function(assert) {
  const igc = server.create('in-game-content');

  this.set('igc', igc);
  this.render(hbs`{{igc-edit-container igcItem=igc}}`);

  let sku = this.$(testSelector('igc-input', 'sku'));

  let empty = '';
  sku.val(empty);
  sku.trigger('change');
  assert.equal(igc.sku, empty);

  let newValue = 'new value';
  sku.val(newValue);
  sku.trigger('change');
  assert.equal(igc.sku, newValue);
});

test('it displays the image file name next to the image', function(assert) {
  const igc = server.create('in-game-content', { imageUrl: '//test/image/foo.png' });

  this.set('igc', igc);
  this.render(hbs`{{igc-edit-container igcItem=igc}}`);

  let imageFileName = this.$('.igc-image__name').text();

  assert.equal(imageFileName, 'foo.png');
});

/*
  Can't use selectDate on Pikaday interactor in integration tests
  due to bug. See https://github.com/edgycircle/ember-pikaday/issues/127
  or more information.
test('changing the publish date updates the igc document', function(assert) {
  const igc = server.create('in-game-content');

  this.set('igc', igc);
  this.render(hbs`{{igc-edit-container igcItem=igc}}`);

  let pikadayInput = openDatepicker(this.$('.pika-input'));
  let expectedDate = new Date(1989, 3, 28);

  pikadayInput.selectDate(expectedDate);
  assert.equal(igc.publishDate, igc.publishDate);
});*/

test('the user can switch the free radio button status', function(assert) {
  const igc = server.create('in-game-content');

  this.set('igc', igc);
  this.render(hbs`{{igc-edit-container igcItem=igc}}`);

  let free = this.$('#free');
  let notFree = this.$('#notFree');

  free.click();
  assert.equal(free.prop('checked'), true);
  assert.equal(notFree.prop('checked'), false);

  notFree.click();
  assert.equal(free.prop('checked'), false);
  assert.equal(notFree.prop('checked'), true);
});

test('clicking a free radio button updates the igc document', function(assert) {
  const igc = server.create('in-game-content');

  this.set('igc', igc);
  this.render(hbs`{{igc-edit-container igcItem=igc}}`);

  let free = this.$('#free');
  let notFree = this.$('#notFree');

  free.click();
  assert.equal(igc.isFreeProduct, true);

  notFree.click();
  assert.equal(igc.isFreeProduct, false);
});

test('changing the basePrice input updates the igcItem', function(assert) {
  const igc = server.create('in-game-content');

  this.set('igc', igc);
  this.render(hbs`{{igc-edit-container igcItem=igc}}`);

  let basePrice = this.$(testSelector('igc-input', 'basePrice'));

  let empty = '';
  basePrice.val(empty);
  basePrice.trigger('change');
  assert.equal(igc.basePrice, empty);

  let newValue = 'new value';
  basePrice.val(newValue);
  basePrice.trigger('change');
  assert.equal(igc.basePrice, newValue);
});
