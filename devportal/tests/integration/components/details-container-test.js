/* global server */
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import testSelector from 'ember-test-selectors';
import startMirage from '../../helpers/start-mirage';

moduleForComponent('details-container', 'Integration | Component | details container', {
  integration: true,
  beforeEach() {
    startMirage(this.container);
  },
  afterEach() {
    window.server.shutdown();
  }
});

test('it lists minimum specs', function(assert) {
  const doc = server.create('commerce-document');

  this.set('doc', doc);
  this.render(hbs`{{details-container document=doc}}`);

  for (let label in doc.minimumSpecs) {
    let input = this.$(testSelector('min-specs', label));
    assert.equal(input.val(), doc.minimumSpecs[label]);
  }
});

test('it lists recommended specs', function(assert) {
  const doc = server.create('commerce-document');

  this.set('doc', doc);
  this.render(hbs`{{details-container document=doc}}`);

  for (let label in doc.recommendedSpecs) {
    let input = this.$(testSelector('rec-specs', label));
    assert.equal(input.val(), doc.recommendedSpecs[label]);
  }
});

test('it displays short and long descriptions', function(assert) {
  const doc = server.create('commerce-document');

  this.set('doc', doc);
  this.render(hbs`{{details-container document=doc}}`);

  let shortDescInput = this.$(testSelector('short-desc'));
  let longDescInput = this.$(testSelector('long-desc'));

  assert.equal(shortDescInput.val(), doc.shortDesc);
  assert.equal(longDescInput.val(), doc.longDesc);
});
