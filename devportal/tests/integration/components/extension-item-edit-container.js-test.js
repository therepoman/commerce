import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('extension-item-edit-container.js', 'Integration | Component | extension item edit container.js', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{extension-item-edit-container.js}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#extension-item-edit-container.js}}
      template block text
    {{/extension-item-edit-container.js}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
