/* global server */
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import testSelector from 'ember-test-selectors';
import startMirage from '../../helpers/start-mirage';

moduleForComponent('distribution-container', 'Integration | Component | distribution container', {
  integration: true,
  beforeEach() {
    startMirage(this.container);
  },
  afterEach() {
    window.server.shutdown();
  }
});

test('it renders with the correct region radio button checked', function(assert) {
  const doc = server.create('commerce-document');

  this.set('doc', doc);
  this.render(hbs`{{distribution-container document=doc}}`);

  let allRegions = this.$('#allRegions');
  let selectRegions = this.$('#selectRegions');

  assert.equal(allRegions.prop('checked'), doc.isAvailableInAllRegions);
  assert.equal(selectRegions.prop('checked'), !doc.isAvailableInAllRegions);
});

test('the user can switch the region radio button status', function(assert) {
  const doc = server.create('commerce-document');

  this.set('doc', doc);
  this.render(hbs`{{distribution-container document=doc}}`);

  let allRegions = this.$('#allRegions');
  let selectRegions = this.$('#selectRegions');

  allRegions.click();
  assert.equal(allRegions.prop('checked'), true);
  assert.equal(selectRegions.prop('checked'), false);

  selectRegions.click();
  assert.equal(allRegions.prop('checked'), false);
  assert.equal(selectRegions.prop('checked'), true);
});

test('clicking a region radio button updates the document', function(assert) {
  const doc = server.create('commerce-document');

  this.set('doc', doc);
  this.render(hbs`{{distribution-container document=doc}}`);

  let allRegions = this.$('#allRegions');
  let selectRegions = this.$('#selectRegions');

  allRegions.click();
  assert.equal(doc.isAvailableInAllRegions, true);

  selectRegions.click();
  assert.equal(doc.isAvailableInAllRegions, false);
});

test('clicking the AllRegions radio button disables the region checkboxes', function(assert) {
  const doc = server.create('commerce-document');

  this.set('doc', doc);
  this.render(hbs`{{distribution-container document=doc}}`);

  let allRegions = this.$('#allRegions');
  allRegions.click();

  let regionCheckboxes = this.$(testSelector('checkbox-region'));
  regionCheckboxes.each((index, checkbox) => {
    assert.equal(checkbox.disabled, true);
  });
});

test('clicking the region checkboxes updates the document', function(assert) {
  const doc = server.create('commerce-document', { isAvailableInAllRegions: false });

  this.set('doc', doc);
  this.render(hbs`{{distribution-container document=doc}}`);

  let regionCheckboxes = this.$(testSelector('checkbox-region'));
  regionCheckboxes.each((index, checkbox) => {
    let initialValue = doc.regions[checkbox.id].isAvailable;
    checkbox.click();
    assert.equal(doc.regions[checkbox.id].isAvailable, !initialValue);
  });
});

/*
  Can't use selectDate on Pikaday interactor in integration tests
  due to bug. See https://github.com/edgycircle/ember-pikaday/issues/127
  or more information.
test('changing the release date updates the document', function(assert) {
  const doc = server.create('commerce-document');

  this.set('doc', doc);
  this.render(hbs`{{distribution-container document=doc}}`);

  let pikadayInput = openDatepicker(this.$('.pika-input'));
  let expectedDate = new Date(1989, 3, 28);

  pikadayInput.selectDate(expectedDate);
  assert.equal(doc.releaseDate, doc.releaseDate);
});*/

test('the user can switch the forSale radio button status', function(assert) {
  const doc = server.create('commerce-document');

  this.set('doc', doc);
  this.render(hbs`{{distribution-container document=doc}}`);

  let forSale = this.$('#forSale');
  let notForSale = this.$('#notForSale');

  forSale.click();
  assert.equal(forSale.prop('checked'), true);
  assert.equal(notForSale.prop('checked'), false);

  notForSale.click();
  assert.equal(forSale.prop('checked'), false);
  assert.equal(notForSale.prop('checked'), true);
});

test('clicking a forSale radio button updates the document', function(assert) {
  const doc = server.create('commerce-document');

  this.set('doc', doc);
  this.render(hbs`{{distribution-container document=doc}}`);

  let forSale = this.$('#forSale');
  let notForSale = this.$('#notForSale');

  forSale.click();
  assert.equal(doc.isForSaleOnTwitch, true);

  notForSale.click();
  assert.equal(doc.isForSaleOnTwitch, false);
});

test('changing the SKU input updates the document', function(assert) {
  const doc = server.create('commerce-document');

  this.set('doc', doc);
  this.render(hbs`{{distribution-container document=doc}}`);

  let sku = this.$(testSelector('input', 'sku'));

  let empty = '';
  sku.val(empty);
  sku.trigger('change');
  assert.equal(doc.sku, empty);

  let newValue = 'new value';
  sku.val(newValue);
  sku.trigger('change');
  assert.equal(doc.sku, newValue);
});

test('the user can switch the free radio button status', function(assert) {
  const doc = server.create('commerce-document');

  this.set('doc', doc);
  this.render(hbs`{{distribution-container document=doc}}`);

  let free = this.$('#free');
  let notFree = this.$('#notFree');

  free.click();
  assert.equal(free.prop('checked'), true);
  assert.equal(notFree.prop('checked'), false);

  notFree.click();
  assert.equal(free.prop('checked'), false);
  assert.equal(notFree.prop('checked'), true);
});

test('clicking a free radio button updates the document', function(assert) {
  const doc = server.create('commerce-document');

  this.set('doc', doc);
  this.render(hbs`{{distribution-container document=doc}}`);

  let free = this.$('#free');
  let notFree = this.$('#notFree');

  free.click();
  assert.equal(doc.isFreeProduct, true);

  notFree.click();
  assert.equal(doc.isFreeProduct, false);
});

test('changing the basePrice input updates the document', function(assert) {
  const doc = server.create('commerce-document');

  this.set('doc', doc);
  this.render(hbs`{{distribution-container document=doc}}`);

  let basePrice = this.$(testSelector('input', 'basePrice'));

  let empty = '';
  basePrice.val(empty);
  basePrice.trigger('change');
  assert.equal(doc.basePrice, empty);

  let newValue = 'new value';
  basePrice.val(newValue);
  basePrice.trigger('change');
  assert.equal(doc.basePrice, newValue);
});
