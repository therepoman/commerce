import { moduleFor, test } from 'ember-qunit';
import sinonTest from 'ember-sinon-qunit/test-support/test';

moduleFor('authenticator:twitch', 'Unit | Authenticator | Twitch', {
});

// Replace this with your real tests.
test('it exists', function(assert) {
  let authenticator = this.subject();
  assert.ok(authenticator);
});

sinonTest('Authenticator just passes through data', function(assert){
  let authenticator = this.subject();
  return authenticator.restore('api_key').then((value) => {
    assert.equal(value, 'api_key', 'data in, data out');
  });
});
