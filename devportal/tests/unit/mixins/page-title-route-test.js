import EmberObject from '@ember/object';
import PageTitleRouteMixin from 'devportal-client/mixins/page-title-route';
import { module, test } from 'qunit';

module('Unit | Mixin | page title route');

// Replace this with your real tests.
test('it works', function(assert) {
  let PageTitleRouteObject = EmberObject.extend(PageTitleRouteMixin);
  let subject = PageTitleRouteObject.create();
  assert.ok(subject);
});
