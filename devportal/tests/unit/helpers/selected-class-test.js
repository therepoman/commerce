import { selectedClass } from 'devportal-client/helpers/selected-class';
import { module, test } from 'qunit';

module('Unit | Helper | selected class');

test('it returns selected class string', function(assert) {
  let params = [0, 0];
  let result = selectedClass(params);
  assert.equal('selected', result);
});

test('it returns empty string', function(assert) {
  let params = [0, 1];
  let result = selectedClass(params);
  assert.notEqual('selected', result);
});
