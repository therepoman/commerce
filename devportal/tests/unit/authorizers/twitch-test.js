import { moduleFor, test } from 'ember-qunit';
import sinonTest from 'ember-sinon-qunit/test-support/test';
import ENV from 'devportal-client/config/environment';

moduleFor('authorizer:twitch', 'Unit | Authorizer | Twitch', {
});

// Replace this with your real tests.
test('it exists', function(assert) {
  let authorizer = this.subject();
  assert.ok(authorizer);
});

sinonTest('Twitch Authorizer always adds client-id to header block', function(assert){
  let authorizer = this.subject();

  let block = this.spy();
  authorizer.authorize({}, block);
  assert.ok(block.calledOnce, 'Block only called once when no api token');
  assert.ok(block.calledWithExactly('Client-ID', ENV.CLIENT_ID));
});

sinonTest('Twitch Authorizer adds twitch-api-token when api_token was in the cookie', function(assert){
  let authorizer = this.subject();

  let block = this.spy();
  authorizer.authorize({ 'api_token': 'test-api-token' }, block);
  assert.ok(block.calledTwice, 'Block called twice');
  assert.ok(block.getCall(0).calledWithExactly('Client-ID', ENV.CLIENT_ID));
  assert.ok(block.getCall(1).calledWithExactly('twitch-api-token', 'test-api-token'));
});
