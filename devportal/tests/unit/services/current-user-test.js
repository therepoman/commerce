import { resolve } from 'rsvp';
import EmberObject from '@ember/object';
import { moduleFor, test } from 'ember-qunit';
import sinonTest from 'ember-sinon-qunit/test-support/test';

moduleFor('service:current-user', 'Unit | Service | current-user', {
});

// Replace this with your real tests.
test('it exists', function(assert) {
  let service = this.subject();
  assert.ok(service);
});

sinonTest('load does nothing if session not authenticated', function(){
  let sessionService = EmberObject.create({isAuthenticated:false});
  let storeService = EmberObject.create({
    queryRecord(){}
  });
  let storeMock = this.mock(storeService);
  storeMock.expects('queryRecord').never();
  let service = this.subject({
    session:sessionService
  });
  service.load();
  storeMock.verify();
});

sinonTest('load queries store for a session and stores it as activeSession', function(assert){
  let sessionService = EmberObject.create({isAuthenticated:true});
  let storeService = EmberObject.create({
    queryRecord(){}
  });
  let storeMock = this.mock(storeService);
  let pretendSession = {pretendSession:1};

  storeMock
    .expects('queryRecord')
    .once()
    .withArgs('session', {})
    .returns(resolve(pretendSession));

  let service = this.subject({
    session:sessionService,
    store:storeService
  });

  return service.load().then(() => {
    storeMock.verify();
    assert.equal(service.get('activeSession'), pretendSession, 'Active Session was set');
  });
});
