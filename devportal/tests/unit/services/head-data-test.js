import { moduleFor, test } from 'ember-qunit';

moduleFor('service:head-data', 'Unit | Service | head data', {
  // Specify the other units that are required for this test.
  // needs: ['service:foo']
});

// Replace this with your real tests.
test('it exists', function(assert) {
  let service = this.subject();
  assert.ok(service);
});

test('setTitle sets title', function(assert) {
  let service = this.subject();
  service.setTitle('The Title');
  assert.equal(service.get('title'), 'The Title', 'Title was set');
});

test('setTitle sets title with suffix', function(assert) {
  let service = this.subject();
  service.setTitle('The Title', true);
  assert.equal(service.get('title'), 'The Title - Twitch Developers', 'Title was set with Suffix');
});
