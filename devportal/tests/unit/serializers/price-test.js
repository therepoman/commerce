import { moduleForModel, test } from 'ember-qunit';

moduleForModel('price', 'Unit | Serializer | price', {
  needs: [
    'serializer:price',
    'model:managed-product',
    'validator:presence',
    'validator:number'
  ]
});

// Replace this with your real tests.
test('it serializes records', function(assert) {
  let record = this.subject();

  let serializedRecord = record.serialize();

  assert.ok(serializedRecord);
});
