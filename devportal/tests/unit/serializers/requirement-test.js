import { moduleForModel, test } from 'ember-qunit';

moduleForModel('requirement', 'Unit | Serializer | requirement', {
  needs: [
    'serializer:requirement',
    'model:managed-product',
    'validator:presence'
  ]
});

// Replace this with your real tests.
test('it serializes records', function(assert) {
  let record = this.subject();

  let serializedRecord = record.serialize();

  assert.ok(serializedRecord);
});
