import { moduleForModel, test } from 'ember-qunit';

moduleForModel('listing', 'Unit | Serializer | listing', {
  needs: [
    'serializer:listing',
    'model:managed-product',
    'validator:presence',
    'validator:length'
  ]
});

// Replace this with your real tests.
test('it serializes records', function(assert) {
  let record = this.subject();

  let serializedRecord = record.serialize();

  assert.ok(serializedRecord);
});
