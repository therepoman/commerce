import { moduleForModel, test } from 'ember-qunit';

moduleForModel('rating-usk', 'Unit | Serializer | rating usk', {
  // Specify the other units that are required for this test.
  needs: ['serializer:rating-usk']
});

// Replace this with your real tests.
test('it serializes records', function(assert) {
  let record = this.subject();

  let serializedRecord = record.serialize();

  assert.ok(serializedRecord);
});
