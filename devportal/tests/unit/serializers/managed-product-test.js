import { run } from '@ember/runloop';
import { A } from '@ember/array';
import Service from '@ember/service';
import { moduleForModel, test } from 'ember-qunit';
import Pretender from 'pretender';

let server;

const countriesStub = Service.extend({
  getAllCountryInfo: function() {
    return A([
      { iso2Code: 'GB', isoCurrencyCode: 'GBP', name: 'United Kingdom' },
      { iso2Code: 'US', isoCurrencyCode: 'USD', name: 'United States of America' },
      { iso2Code: 'YE', isoCurrencyCode: 'YER', name: 'Yemen' },
      { iso2Code: 'ZM', isoCurrencyCode: 'ZMK', name: 'Zambia' },
      { iso2Code: 'ZW', isoCurrencyCode: 'ZWL', name: 'Zimbabwe' }
    ]);
  }
});

moduleForModel('managed-product', 'Unit | Serializer | managed product', {
  needs: [
    'serializer:managed-product',
    'model:listing',
    'model:price',
    'model:country-price',
    'model:requirement',
    'serializer:listing',
    'serializer:price',
    'serializer:country-price',
    'serializer:requirement',
    'validator:presence',
    'validator:date',
    'validator:format'
  ],

  beforeEach() {
    server = new Pretender(function() {
      this.get('/managed-products/0', function() {
        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(managedProductJson)
        ];
      });
    });

    this.register('service:countries', countriesStub);
    this.inject.service('countries', { as: 'countries' });
  },
  afterEach() {
    server.shutdown();
  }
});

const managedProductJson = {
  'managedProduct': {
    'id': 0,
    'type': 'ADG_GAME',
    'binaryInfo': {
      'pcRequirementsInfo': {
        'minimumRequirements': {
          'videoCard': 'Intel HD300',
          'ram': '2GB',
          'hardDriveSpace': '1GB',
          'processor': 'Dual Core 2 GHz',
          'os': 'Windows 7+',
          'directX': 'DirectX 10'
        },
        'recommendedRequirements': {
          'videoCard': 'Intel Iris',
          'ram': '4GB',
          'hardDriveSpace': '1GB',
          'processor': 'Intel i3',
          'os': 'Windows 7+',
          'directX': 'DirectX 10'
        },
        'otherRequirements': 'None'
      },
      'requiresContinuousConnectivity': 'true',
      'numberOfPlayersSupported': '1',
      'languageSupport': {
        'instructions': ['english'],
        'subtitlesAndCaptions': ['english', 'japanese'],
        'audio': ['english']
      },
      'version': '1.0.1',
      'supportedControllers': 'keyboard, mouse, and gamepad'
    },
    'developerInfo': {
      'developerName': 'developer'
    },
    'contentRatingInfo': {
      'esrbRating': {
        'ratingCategory': 'teen',
        'contentDescriptors': ['blood', 'adult language']
      },
      'pegiRating': 'ages_12_and_over',
      'amazonRating': 'rating_pending'
    },
    'pricingInfo': {
      'pricesByMarketplaceAndCountry': {
        'amazon.com': {
          'defaultListPrice': {
            'currencyCode': 'USD',
            'amount': 7.99
          },
          'pricesByCountry': {
            'US': {
              'currencyCode': 'USD',
              'amount': 7.99
            },
            'GB': {
              'currencyCode': 'USD',
              'amount': 15.99
            },
            'YE': {
              'currencyCode': 'USD',
              'amount': 7.99
            },
            'ZM': {
              'currencyCode': 'USD',
              'amount': 7.99
            },
            'ZW': {
              'currencyCode': 'USD',
              'amount': 7.99
            }
          }
        }
      }
    },
    'availabilityInfo': {
      'availableCountries': ['GB', 'US']
    },
    'publishOnTwitchDate': '2017-01-01T12:00:00.000Z',
    'releaseDate': '2017-01-01T12:00:00.000Z',
    'announceDate': '2017-01-01T12:00:00.000Z',
    'previousReleaseDate': '2017-01-01T12:00:00.000Z',
    'developerWebsiteUrl': 'http://developer.com/',
    'broadcasterVodUrls': ['http://developer.com/'],
    'newsAndReviewsUrls': ['http://developer.com/'],
    'customerSupportUrl': 'http://developer.com/',
    'customerSupportEmail': 'help@developer.com',
    'defaultLocale': 'en-US',
    'listingsByLocale': {
      'en-US': {
        'displayName': 'Game',
        'shortDescription': 'short game description',
        'longDescription': 'long game description',
        'productFeatures': ['feature 1', 'feature 2'],
        'keywords': ['game', 'test'],
        'gameEdition': 'Standard',
        'gameEditionDetails': 'edition details',
        'iconId': 'iconId',
        'screenshotIds': ['screenshotId1', 'screenshotId2'],
        'videoIds': ['videoId'],
        'boxshotId': 'boxshotId',
        'pageBackgroundId': 'pageBackgroundId',
        'gameEditionBadgeId': 'gameEditionBadgeId',
        'releaseNotesId': 'releaseNotesId',
        'eulaUrl': 'http://developer.com/eula',
        'thankYouPageBackgroundImageId': 'thankYouPageBackgroundImageId'
      }
    }
  }
};

test('it normalizes binaryInfo', function(assert) {
  return run(() => {
    this.store().findRecord('managed-product', 0).then((managedProduct) => {
      let minimumRequirements = managedProduct.get('requirements').findBy('requirementType', 'minimumRequirements');
      let recommendedRequirements = managedProduct.get('requirements').findBy('requirementType', 'recommendedRequirements');
      let otherRequirements = managedProduct.get('otherRequirements');
      let requiresContinuousConnectivity = managedProduct.get('requiresContinuousConnectivity');
      let numberOfPlayersSupported = managedProduct.get('numberOfPlayersSupported');
      let languageSupport = managedProduct.get('languageSupport');
      let version = managedProduct.get('version');
      let supportedControllers = managedProduct.get('supportedControllers');

      let expectedMinimumRequirements = this.store().createRecord('requirement', {
        requirementType: 'minimumRequirements',
        videoCard: 'Intel HD300',
        ram: '2GB',
        hardDriveSpace: '1GB',
        processor: 'Dual Core 2 GHz',
        os: 'Windows 7+',
        directX: 'DirectX 10',
        managedProduct: managedProduct
      });

      let expectedRecommendedRequirements = this.store().createRecord('requirement', {
        requirementType: 'recommendedRequirements',
        videoCard: 'Intel Iris',
        ram: '4GB',
        hardDriveSpace: '1GB',
        processor: 'Intel i3',
        os: 'Windows 7+',
        directX: 'DirectX 10',
        managedProduct: managedProduct
      });

      assert.deepEqual(minimumRequirements.toJSON(), expectedMinimumRequirements.toJSON(), 'minimumRequirements did not normalize correctly');
      assert.deepEqual(recommendedRequirements.toJSON(), expectedRecommendedRequirements.toJSON(), 'recommendedRequirements did not normalize correctly');
      assert.equal(otherRequirements, 'None', 'otherRequirements did not normalize correctly');
      assert.equal(requiresContinuousConnectivity, true, 'requiresContinuousConnectivity did not normalize correctly');
      assert.equal(numberOfPlayersSupported, '1', 'numberOfPlayersSupported did not normalize correctly');
      assert.deepEqual(languageSupport, {
        instructions: ['english'],
        subtitlesAndCaptions: ['english', 'japanese'],
        audio: ['english']
      }, 'languageSupport did not normalize correctly');
      assert.equal(version, '1.0.1', 'version did not normalize correctly');
      assert.equal(supportedControllers, 'keyboard, mouse, and gamepad', 'supportedControllers did not normalize correctly');
    });
  });
});

test('it normalizes developerInfo', function(assert) {
  return run(() => {
    this.store().findRecord('managed-product', 0).then((managedProduct) => {
      let developerName = managedProduct.get('developerName');
      assert.equal(developerName, 'developer', 'developerName did not normalize properly');
    });
  });
});

test('it normalizes contentRatingInfo', function(assert) {
  return run(() => {
    this.store().findRecord('managed-product', 0).then((managedProduct) => {
      let contentRatings = managedProduct.get('contentRatings');
      assert.deepEqual(contentRatings, {
        esrbRating: { ratingCategory: 'teen', contentDescriptors: ['blood', 'adult language'] },
        pegiRating: { ratingCategory: 'ages_12_and_over', contentDescriptors: [] },
        amazonRating: { ratingCategory: 'rating_pending', contentDescriptors: [] }
      }, 'contentRatings did not normalize properly');
    });
  });
});

test('it normalizes pricingInfo', function(assert) {
  return run(() => {
    this.store().findRecord('managed-product', 0).then((managedProduct) => {
      let basePrice = managedProduct.get('basePrice');
      let pricesByCountry = managedProduct.get('pricesByCountry');

      assert.deepEqual(basePrice, {
        currencyCode: 'USD',
        amount: 7.99
      }, 'basePrice did not normalize properly');
      assert.deepEqual(pricesByCountry, [
        { countryCode: 'GB', currencyCode: 'USD', countryName: 'United Kingdom of Great Britain and Northern Ireland', amount: 15.99 },
        { countryCode: 'US', currencyCode: 'USD', countryName: 'United States of America', amount: 7.99 },
        { countryCode: 'YE', currencyCode: 'USD', countryName: 'Yemen', amount: 7.99 },
        { countryCode: 'ZM', currencyCode: 'USD', countryName: 'Zambia', amount: 7.99 },
        { countryCode: 'ZW', currencyCode: 'USD', countryName: 'Zimbabwe', amount: 7.99 }
      ], 'contentRatings did not normalize properly');
    });
  });
});

test('it normalizes availabilityInfo', function(assert) {
  return run(() => {
    this.store().findRecord('managed-product', 0).then((managedProduct) => {
      let countries = managedProduct.get('countries');

      assert.deepEqual(countries, [
        { countryCode: 'GB', countryName: 'United Kingdom of Great Britain and Northern Ireland', available: true },
        { countryCode: 'US', countryName: 'United States of America', available: true },
        { countryCode: 'YE', countryName: 'Yemen', available: false },
        { countryCode: 'ZM', countryName: 'Zambia', available: false },
        { countryCode: 'ZW', countryName: 'Zimbabwe', available: false }
      ], 'countries did not normalize properly');
    });
  });
});

test('it normalizes listingsByLocale', function(assert) {
  return run(() => {
    this.store().findRecord('managed-product', 0).then((managedProduct) => {
      let listings = managedProduct.get('listings');

      assert.deepEqual(listings, [{
        localeCode: 'en-US',
        displayName: 'Game',
        shortDescription: 'short game description',
        longDescription: 'long game description',
        productFeatures: ['feature 1', 'feature 2'],
        keywords: ['game', 'test'],
        gameEdition: 'Standard',
        gameEditionDetails: 'edition details',
        iconId: 'iconId',
        screenshotIds: ['screenshotId1', 'screenshotId2'],
        videoIds: ['videoId'],
        boxshotId: 'boxshotId',
        pageBackgroundId: 'pageBackgroundId',
        gameEditionBadgeId: 'gameEditionBadgeId',
        releaseNotesId: 'releaseNotesId',
        eulaUrl: 'http://developer.com/eula',
        thankYouPageBackgroundImageId: 'thankYouPageBackgroundImageId'
      }], 'listings did not normalize properly');
    });
  });
});

test('it serializes managed-product models', function(assert) {
  return run(() => {
    let managedProduct = this.subject();
    let testDate = new Date('2017-01-01T12:00Z');

    managedProduct.set('id', 0);
    managedProduct.set('type', 'ADG_GAME');
    managedProduct.set('minimumRequirements', {
      videoCard: 'Intel HD300',
      ram: '2GB',
      hardDriveSpace: '1GB',
      processor: 'Dual Core 2 GHz',
      os: 'Windows 7+',
      directX: 'DirectX 10'
    });
    managedProduct.set('recommendedRequirements', {
      videoCard: 'Intel Iris',
      ram: '4GB',
      hardDriveSpace: '1GB',
      processor: 'Intel i3',
      os: 'Windows 7+',
      directX: 'DirectX 10'
    });
    managedProduct.set('otherRequirements', 'None');
    managedProduct.set('requiresContinuousConnectivity', true);
    managedProduct.set('numberOfPlayersSupported', '1');
    managedProduct.set('languageSupport', {
      instructions: ['english'],
      subtitlesAndCaptions: ['english', 'japanese'],
      audio: ['english']
    });
    managedProduct.set('version', '1.0.1');
    managedProduct.set('supportedControllers', 'keyboard, mouse, and gamepad');
    managedProduct.set('developerName', 'developer');
    managedProduct.set('contentRatings', {
      esrbRating: { ratingCategory: 'teen', contentDescriptors: ['blood', 'adult language'] },
      pegiRating: { ratingCategory: 'ages_12_and_over', contentDescriptors: [] },
      amazonRating: { ratingCategory: 'rating_pending', contentDescriptors: [] }
    });
    managedProduct.set('basePrice', {
      currencyCode: 'USD',
      amount: 7.99
    });
    managedProduct.set('pricesByCountry', [
      { countryCode: 'GB', currencyCode: 'USD', countryName: 'United Kingdom of Great Britain and Northern Ireland', amount: 15.99 },
      { countryCode: 'US', currencyCode: 'USD', countryName: 'United States of America', amount: 7.99 },
      { countryCode: 'YE', currencyCode: 'USD', countryName: 'Yemen', amount: 7.99 },
      { countryCode: 'ZM', currencyCode: 'USD', countryName: 'Zambia', amount: 7.99 },
      { countryCode: 'ZW', currencyCode: 'USD', countryName: 'Zimbabwe', amount: 7.99 }
    ]);
    managedProduct.set('countries' ,[
      { countryCode: 'GB', countryName: 'United Kingdom of Great Britain and Northern Ireland', available: true },
      { countryCode: 'US', countryName: 'United States of America', available: true },
      { countryCode: 'YE', countryName: 'Yemen', available: false },
      { countryCode: 'ZM', countryName: 'Zambia', available: false },
      { countryCode: 'ZW', countryName: 'Zimbabwe', available: false }
    ]);
    managedProduct.set('publishOnTwitchDate', testDate);
    managedProduct.set('releaseDate', testDate);
    managedProduct.set('announceDate', testDate);
    managedProduct.set('previousReleaseDate', testDate);
    managedProduct.set('developerWebsiteUrl', 'http://developer.com/');
    managedProduct.set('broadcasterVodUrls', ['http://developer.com/']);
    managedProduct.set('newsAndReviewsUrls', ['http://developer.com/']);
    managedProduct.set('customerSupportUrl', 'http://developer.com/');
    managedProduct.set('customerSupportEmail', 'help@developer.com');
    managedProduct.set('defaultLocale', 'en-US');
    managedProduct.set('listings', [{
      localeCode: 'en-US',
      displayName: 'Game',
      shortDescription: 'short game description',
      longDescription: 'long game description',
      productFeatures: ['feature 1', 'feature 2'],
      keywords: ['game', 'test'],
      gameEdition: 'Standard',
      gameEditionDetails: 'edition details',
      iconId: 'iconId',
      screenshotIds: ['screenshotId1', 'screenshotId2'],
      videoIds: ['videoId'],
      boxshotId: 'boxshotId',
      pageBackgroundId: 'pageBackgroundId',
      gameEditionBadgeId: 'gameEditionBadgeId',
      releaseNotesId: 'releaseNotesId',
      eulaUrl: 'http://developer.com/eula',
      thankYouPageBackgroundImageId: 'thankYouPageBackgroundImageId'
    }]);

    let serializedmanagedProduct = managedProduct.serialize();
    serializedmanagedProduct.id = 0;
    assert.deepEqual(serializedmanagedProduct, managedProductJson['managed-product'],
      'the document did not serialize correctly');
  });
});
