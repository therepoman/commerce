import { moduleForModel, test } from 'ember-qunit';

moduleForModel('commerce/country', 'Unit | Serializer | commerce/country', {
  // Specify the other units that are required for this test.
  needs: ['serializer:commerce/country']
});

// Replace this with your real tests.
test('it serializes records', function(assert) {
  let record = this.subject();

  let serializedRecord = record.serialize();

  assert.ok(serializedRecord);
});
