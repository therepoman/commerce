import { moduleFor, test } from 'ember-qunit';
import sinonTest from 'ember-sinon-qunit/test-support/test';

moduleFor('session-store:application', 'Unit | Session Store | application', {

});

test('it exists', function(assert) {
  let sessionStore = this.subject();
  assert.ok(sessionStore);
});

sinonTest('it read the api_token cookie when restoring', function() {
  let sessionStore = this.subject();
  let mock = this.mock(sessionStore);
  mock.expects('_read').once().withExactArgs('api_token');

  sessionStore.restore();
  mock.verify();
});

sinonTest('it restores as not authenticated when no cookie exists', function(assert) {
  assert.expect(1);
  let sessionStore = this.subject();
  let stub = this.stub(sessionStore, '_read');
  stub.onCall().returns({});

  return sessionStore.restore()
    .then((authObj) => {
      assert.notOk(authObj.authenticated, 'No Authentication Information Present');
    });
});

sinonTest('it restores as authenticated when cookie defined', function(assert) {
  assert.expect(1);
  let sessionStore = this.subject();
  let stub = this.stub(sessionStore, '_read');
  stub.returns('fake_api_token');

  return sessionStore.restore()
    .then((authObj) => {
      assert.equal(authObj.authenticated.api_token, 'fake_api_token', 'Authentication Information Present');
    });
});

test('persist doesnt really do anything', function(assert){
  let sessionStore = this.subject();
  let promise = sessionStore.persist('Some Data');
  assert.equal(promise._state, 1, 'Promise was immediately resolved');
  assert.equal(sessionStore._lastData, 'Some Data', 'It set last data, at least');

});
