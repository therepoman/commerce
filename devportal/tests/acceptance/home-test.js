/* global server */
import { test } from 'qunit';
import testSelector from 'ember-test-selectors';
import startMirage from '../helpers/start-mirage';
import moduleForAcceptance from 'devportal-client/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | home', {
  beforeEach() {
    startMirage(this.container);
  },
  afterEach() {
    window.server.shutdown();
  }
});

test('visiting /games', function(assert) {
  visit('/games');

  andThen(() => {
    assert.equal(currentURL(), '/games');
  });
});

test('should transition to commerce for first dev portal product', function(assert) {
  const devPortalProducts = server.createList('dev-portal-product', 5);
  const devPortalProductId = devPortalProducts[0].id;

  visit('/games');

  click(testSelector('edit-commerce-button'));

  andThen(() => {
    assert.equal(currentURL(), `/games/${devPortalProductId}/commerce/distribution`);
  });
});

test('should transition to commerce for third dev portal product', function(assert) {
  let done = assert.async();
  const devPortalProducts = server.createList('dev-portal-product', 5);
  const devPortalProductId = devPortalProducts[2].id;

  visit('/games');

  andThen(() => {
    click(testSelector('game-nav-item', devPortalProductId));
    click(testSelector('edit-commerce-button'));

    andThen(() => {
      assert.equal(currentURL(), `/games/${devPortalProductId}/commerce/distribution`);
      done();
    });
  });
});
