#!/bin/bash

set -e

source common.sh

function create_s3_bucket() {
  echo "+ Creating S3 bucket"
  aws s3 mb s3://$S3BUCKET || true
}

function save_github_token() {
  if ! [[ -f .github.apitoken ]]; then
    echo "+ Creating github API access token. This operation may require you to enter your Github password"
    echo '{
      "client_id": "8abe836aa0288500b2e6",
      "client_secret": "51119cecf8a1a2f7612fffeb0dd199245fe9bb0d",
      "scopes": ["public_repo","write:repo_hook"],
      "note": "'$USER' github-import lambda access",
      "note_url": "https://git-aws.internal.justin.tv/vod/github-import"
    }' | http post https://git-aws.internal.justin.tv/api/v3/authorizations -b -a $USER | jq -r '.token' > .github.apitoken
  else
    echo "+ Github API access token already exists at .github.apitoken"
  fi
  export GITHUB_APITOKEN=$(cat .github.apitoken)
}

function package_lambda() {
  echo "+ Packaging Lambda function as CloudFormation template to output.yaml"
  aws cloudformation package --template-file github-to-codecommit.yml --output-template-file output.yaml --s3-bucket $S3BUCKET
  sed -i '' \
    -e "s|__SUBNET__|$SUBNET|" \
    -e "s|__SECURITY_GROUP__|$SECURITY_GROUP|" \
    -e "s|__GITHUB_USER__|$USER|" \
    -e "s|__GITHUB_PASS__|$GITHUB_APITOKEN|" \
    output.yaml
}

function deploy_lambda() {
  echo "+ Deploying Lambda function with CloudFormation using output.yaml"
  aws cloudformation deploy --template-file output.yaml --stack-name github-import --capabilities CAPABILITY_NAMED_IAM
}

cat <<EOF

In order to access both git-aws.internal.justin.tv and AWS CodeCommit,
the lambda function requires VPC and Internet access. If your AWS account is
setup such that you have a private (internal) Subnet with a NAT Gateway as
default route, enter the ID of this subnet and its corresponding security group.

EOF

echo -n "Enter Subnet Id: " && read SUBNET
echo -n "Enter Security Group Id: " && read SECURITY_GROUP

create_s3_bucket
save_github_token
package_lambda
deploy_lambda
save_github_iam_creds
