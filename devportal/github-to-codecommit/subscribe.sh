#!/bin/bash

set -e

source common.sh

repo=$1

function subscribe_github_repo() {
  repository=$1

  echo "+ Subscribing ${repository} to github-import SNS topic..."
  echo "+ Enter your Github password when prompted."

  echo '{
    "name": "amazonsns",
    "active": true,
    "config": {
      "aws_key": "'$(cat .github.creds | jq -r ".AccessKey.AccessKeyId")'",
      "aws_secret": "'$(cat .github.creds | jq -r ".AccessKey.SecretAccessKey")'",
      "sns_topic": "'$(get_cfn_resource SNSTopic)'",
      "sns_region": "'$(aws configure get region)'"
    }
  }' | http post https://git-aws.internal.justin.tv/api/v3/repos/${repository}/hooks -b -a $USER
}

function usage() {
  echo "USAGE:"
  echo "$0 <github_org>/<github_repo>"
  echo
  echo "EXAMPLE:"
  echo "$0 vod/github-import"
  exit 0
}

if [[ "$repo" == "" ]]; then
  usage
fi

save_github_iam_creds
subscribe_github_repo $repo
