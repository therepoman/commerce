import subprocess
import sys
import os
import errno
import shutil
import boto3
import botocore
import json
from pprint import pprint

env = os.environ.copy()
env["GIT_EXEC_PATH"] = "/tmp/usr/libexec/git-core"
env["HOME"] = "/tmp"
env["PATH"] = env["PATH"] + ":/var/task"
env["PYTHONPATH"] = "/tmp/python/lib/python2.7/site-packages"

class GitException(Exception):
    pass

def mkdir_p(target):
    try:
        os.makedirs(target)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(target):
            pass
        else:
            raise

def get_repo_url(name, description):
    client = boto3.client('codecommit')
    repo = None
    try:
        print("> finding repo in codecommit")
        repo = client.get_repository(repositoryName=name)
    except botocore.exceptions.ClientError as e:
        if e.response["Error"]["Code"] == "RepositoryDoesNotExistException":
            print("> repo not found in codecommit, creating new repo")
            repo = client.create_repository(repositoryName=name, repositoryDescription=description)
        else:
            raise e
    return repo["repositoryMetadata"]["cloneUrlHttp"]

def git(cmd):
    try:
        cmds = []
        if isinstance(cmd, list):
            cmds = cmd
        else:
            cmds = cmd.split()
        print("+ " + " ".join(cmds))
        print(subprocess.check_output(["/tmp/usr/bin/git"] + cmds, stderr=subprocess.STDOUT, env=env))
    except subprocess.CalledProcessError as e:
        print(e.output)
        raise GitException()

def run(cmd):
    try:
        print("+ " + cmd)
        print(subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT, env=env))
    except subprocess.CalledProcessError as e:
        print(e.output)

def run_quiet(cmd):
    try:
        print("+ " + cmd)
        print(subprocess.check_output(cmd, shell=True, env=env))
    except subprocess.CalledProcessError as e:
        print(e.output)

def install_dependencies():
    if os.path.isfile("/tmp/usr/bin/git") == False:
        run("tar -zxf /var/task/git.tgz -C /tmp")
    else:
        print("> git binary already extracted")

    if os.path.isdir("/tmp/python") == False:
        run("mkdir -p /tmp/python")
        run_quiet("tar -zxf /var/task/awscli.tgz -C /tmp/python")
    else:
        print("> awscli already extracted")

def clone_repo(org, repo, target_repo_url):
    repo_params = {
        "user" : env["GITHUB_USER"],
        "password" : env["GITHUB_PASSWORD"],
        "org" : org,
        "repo" : repo,
    }

    src_repo_url = "https://%(user)s:%(password)s@git-aws.internal.justin.tv/%(org)s/%(repo)s" % repo_params

    if len(os.listdir(".")) == 0:
        git(["clone",src_repo_url,"."])
        git("remote add codecommit " + target_repo_url)
    else:
        print("> repo already cloned")

def delete_branch(branch):
    try:
        git(["branch","-D",branch])
    except GitException: # branch may not exist, resulting in error
        pass

def handler(event, context):
    sns_message_json = event["Records"][0]["Sns"]["Message"]
    sns_message = json.loads(sns_message_json)
    github_repo = sns_message["repository"]
    branch = sns_message["ref"].split("/")[2]
    org, repo = github_repo["full_name"].split("/")
    description = github_repo["description"] or "No description"
    target_repo_url = get_repo_url(repo, description)
    target_dir = "/tmp/" + repo
    mkdir_p(target_dir)
    os.chdir(target_dir)

    install_dependencies()

    git(["config","--global","credential.helper","!aws codecommit credential-helper $@"])
    git(["config","--global","credential.UseHttpPath","true"])

    clone_repo(org, repo, target_repo_url)

    if sns_message["deleted"]:
        git("push --delete codecommit " + branch)
        delete_branch(branch)
        return {}

    git("remote update --prune")
    git("checkout origin/master")
    delete_branch(branch)
    git(["checkout","-b",branch,"origin/"+branch])
    push_cmds = ["push","-u","codecommit",branch]
    if sns_message["forced"] == True:
        push_cmds += ["--force"]
    git(push_cmds)

    return {}

