function check_os() {
  [[ $(uname) == "Darwin" ]] ||
    (echo "This script is designed for OSX. Specifically, the brew and sed commands. Exiting..." && exit 1)
}

function install_deps() {
  echo "+ Verifying dependencies"
  which jq || brew install jq
  which http || brew install httpie
  which aws || brew install awscli
}

function check_aws_version() {
  currentver=$(aws --version 2>&1 | awk '{print $1}' | cut -d/ -f2)
  requiredver="1.11.28"
  if [ "$(printf "$requiredver\n$currentver" | sort -V | head -n1)" == "$currentver" ] && [ "$currentver" != "$requiredver" ]; then
    echo "aws-cli needs to be at least version 1.11.28, installed is $version"
    exit 1
  else
    echo "+ aws-cli version is $currentver"
  fi
}

function account_id() {
  aws iam get-user | jq -r '.User.Arn' | cut -d: -f5 | tr -d '\n'
}

function save_github_iam_creds() {
  S3_GITHUB_CREDS="s3://${S3BUCKET}/github.creds"
  if aws s3 ls $S3_GITHUB_CREDS >/dev/null ; then
    echo "+ Copying github IAM credentials from $S3_GITHUB_CREDS"
    aws s3 cp $S3_GITHUB_CREDS .github.creds >/dev/null
    iam_access_key=$(aws iam list-access-keys --user-name github-to-codecommit | jq -r '.AccessKeyMetadata[0].AccessKeyId')
    local_access_key=$(cat .github.creds | jq -r '.AccessKey.AccessKeyId')
    if [[ "$local_access_key" != "$iam_access_key" ]]; then
      echo "WARNING: The github IAM access key saved in S3 is different to registered access key. Perhaps S3 file is stale?"
      echo "Consider deleting both the file in S3 and the IAM access keys by running these commands:"
      echo "  aws s3 rm $S3_GITHUB_CREDS"
      if [[ "$iam_access_key" != "null" ]]; then
        echo "  aws iam delete-access-key --user-name github-to-codecommit --access-key-id $iam_access_key"
      fi
      echo "You will have to resubscribe any repos that have already been subscribed using this access key!"
      exit 1
    fi
  else
    echo "+ Saving github IAM credentials to $S3_GITHUB_CREDS"
    aws iam create-access-key --user-name github-to-codecommit > .github.creds
    aws s3 cp .github.creds $S3_GITHUB_CREDS
  fi
}

function get_cfn_resource() {
  logical_resource=$1
  aws cloudformation describe-stack-resource --stack-name github-import --logical-resource-id $logical_resource --output json | jq -r '.StackResourceDetail.PhysicalResourceId'
}

S3BUCKET="github-to-codecommit-$(account_id)"

check_os
install_deps
check_aws_version
