import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  keyForModel(modelName) {
    return 'managedProduct';
  }
});
