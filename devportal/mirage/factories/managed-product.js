import moment from 'moment';
import _number from 'lodash/number';
import _collection from 'lodash/collection';
import { v4 } from 'uuid';
import { Factory, faker, trait } from 'ember-cli-mirage';

const VALID_LANGUAGES = [
  'english',
  'spanish',
  'italian'
];

const VALID_LOCALES = [
  'en-US',
  'en-GB',
  'de-DE',
  'es-ES',
  'fr-FR',
  'it-IT'
];

function generatePcRequirements() {
  return {
    os: faker.lorem.word(),
    processor: faker.lorem.word(),
    ram: faker.lorem.word(),
    hardDriveSpace: faker.lorem.word(),
    videoCard: faker.lorem.word(),
    directX: faker.lorem.word()
  };
}

function generateLanguageSupport() {
  return VALID_LANGUAGES.map((language) => {
    return {
      languageCode: language,
      instructions: faker.random.boolean(),
      subtitlesAndCaptions: faker.random.boolean(),
      audio: faker.random.boolean()
    };
  });
}

function generateAvailabilityInfo() {
  let n = _number.random(1, 10);
  let availabilityInfo = [];
  for (let x = 0; x < n; x++) {
    availabilityInfo.push({
      countryCode: faker.address.countryCode(),
      available: faker.random.boolean(),
      price: _number.random(1, 10)
    });
  }
  return availabilityInfo;
}

function generateLocaleListings() {
  return _collection.sampleSize(VALID_LOCALES, _number.random(1, 6))
  .map((locale) => {
    return {
      locale: locale,
      displayName: faker.commerce.productName(),
      shortDescription: faker.lorem.sentence(),
      longDescription: faker.lorem.paragraph(),
      keywords: faker.lorem.words().split(' '),
      productFeatures: faker.lorem.words().split(' '),
      gameEdition: faker.lorem.word(),
      gameEditionDetails: faker.lorem.words(),
      iconId: faker.lorem.word(),
      boxShotId: faker.lorem.word(),
      screenshotIds: faker.lorem.words(),
      pageBackgroundId: faker.lorem.word(),
      thankYouPageBackgroundId: faker.lorem.word(),
      eulaUrl: faker.internet.url(),
      releaseNotesId: faker.lorem.word()
    };
  });
}

function generateRating() {
  return {
    ratingCategory: 'rating_pending',
    contentDescriptors: faker.lorem.words().split(' ')
  };
}

export default Factory.extend({
  id: v4,
  devPortalProductId: v4,
  lastUpdated: moment(faker.date.recent()).format('X'),
  sku: faker.commerce.product,
  type: 'ADG_GAME',
  adgVendorId: v4,
  adgDomainId: v4,
  fulfillmentMethod: 'fulfilled_FuelLauncher_default',
  pcRequirementsInfo() {
    return {
      minimumRequirements: generatePcRequirements(),
      recommendedRequirements: generatePcRequirements(),
      otherRequirements: faker.lorem.words()
    };
  },
  requiresContinuousConnectivity: faker.random.boolean,
  numberOfPlayersSupported() { return _number.random(1, 8); },
  languageSupport: generateLanguageSupport,
  version: faker.system.semver,
  supportedControllers: faker.lorem.words,
  categoryInfoId: faker.lorem.word,
  developerName() { return faker.company.companyName(0); },
  developerWebsiteUrl: faker.internet.url,
  defaultPrice() { return _number.random(1, 10); },
  availabilityInfo: generateAvailabilityInfo,
  releaseDate: faker.date.recent,
  announceDate: faker.date.recent,
  previousReleaseDate: faker.date.recent,
  broadcasterVodUrls: faker.lorem.words().split(' '),
  newsAndReviewsUrls: faker.lorem.words().split(' '),
  customerSupportUrl: faker.internet.url,
  customerSupportEmail: faker.internet.email,
  localeListings: generateLocaleListings,
  contentRatingInfo() {
    return {
      esrbRatingInfo: generateRating(),
      pegiRating: 'rating_pending',
      uskRating: 'to_be_announced',
      amazonRating: 'rating_pending'
    };
  }
});
