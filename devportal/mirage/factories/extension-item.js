import moment from 'moment';
import _number from 'lodash/number';
import _collection from 'lodash/collection';
import { v4 } from 'uuid';
import { Factory, faker } from 'ember-cli-mirage';

const VALID_LOCALES = [
  'en-US',
  'en-GB',
  'de-DE',
  'es-ES',
  'fr-FR',
  'it-IT'
];

const ITEM_TYPES = [
  'general_digital_good',
  'remotely_accessed_digital_good',
  'remotely_accessed_digital_game',
  'streaming_audio',
  'services',
  'subscriptions',
  'other'
];

export default Factory.extend({
  id: v4,
  devPortalProductId: v4,
  type: 'ADG_EXTENSION_IAP',
  sku: faker.commerce.product,
  lastUpdated: moment(faker.date.recent()).format('X'),
  adgVendorId: v4,
  adgDomainId: v4,
  itemType() { return _collection.sample(ITEM_TYPES); },
  defaultPrice() { return _number.random(1, 10); },

  availabilityInfo() {
    let n = _number.random(1, 10);
    let countries = [];
    for (let x = 0; x < n; x++) {
      countries.push({
        countryCode: faker.address.countryCode(),
        available: faker.random.boolean(),
        price: _number.random(1, 10)
      });
    }
    return countries;
  },

  localeListings() {
    return _collection.sampleSize(VALID_LOCALES, _number.random(1, 6))
      .map((locale) => {
        return {
          locale: locale,
          displayName: faker.commerce.productName(),
          iconId: faker.lorem.word(),
          shortDescription: faker.lorem.sentence(),
          longDescription: faker.lorem.paragraph(),
          keywords: faker.lorem.words().split(' ')
        };
    });
  }
});
