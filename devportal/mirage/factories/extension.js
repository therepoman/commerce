import moment from 'moment';
import _number from 'lodash/number';
import _collection from 'lodash/collection';
import { v4 } from 'uuid';
import { Factory, faker } from 'ember-cli-mirage';

const VALID_LOCALES = [
  'en-US',
  'en-GB',
  'de-DE',
  'es-ES',
  'fr-FR',
  'it-IT'
];

export default Factory.extend({
  id: v4,
  devPortalProductId: v4,
  type: 'ADG_EXTENSION',
  sku: faker.commerce.product,
  lastUpdated: moment(faker.date.recent()).format('X'),
  adgVendorId: v4,
  adgDomainId: v4,
  developerName() { return faker.company.companyName(0); },
  developerWebsiteUrl: faker.internet.url,
  customerSupportUrl: faker.internet.url,
  customerSupportEmail: faker.internet.email,

  localeListings() {
    return _collection.sampleSize(VALID_LOCALES, _number.random(1, 6))
      .map((locale) => {
        return {
          locale: locale,
          displayName: faker.commerce.productName(),
          keywords: faker.lorem.words().split(' ')
        };
    });
  }
});
