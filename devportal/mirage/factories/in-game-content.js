import moment from 'moment';
import _number from 'lodash/number';
import _collection from 'lodash/collection';
import { v4 } from 'uuid';
import { Factory, faker } from 'ember-cli-mirage';

const VALID_LOCALES = [
  'en-US',
  'en-GB',
  'de-DE',
  'es-ES',
  'fr-FR',
  'it-IT'
];

export default Factory.extend({
  id: v4,
  devPortalProductId: v4,
  adgVendorId: v4,
  adgDomainId: v4,
  adgProductId: v4,
  lastUpdated: moment(faker.date.recent()).format('X'),
  type: 'ADG_IGC',
  sku: faker.commerce.product,
  multiplePurchases: faker.random.boolean,
  durable: faker.random.boolean,
  inGameCurrency: faker.random.boolean,
  fulfillmentMethod: 'fulfilled_FuelLauncher_default',
  categoryInfoId: 'game-microtransactions-for-currency',
  defaultPrice() { return _number.random(1, 10); },
  releaseDate: faker.date.recent,

  availabilityInfo() {
    let n = _number.random(1, 10);
    let countries = [];
    for (let x = 0; x < n; x++) {
      countries.push({
        countryCode: faker.address.countryCode(),
        available: faker.random.boolean(),
        price: _number.random(1, 10)
      });
    }
    return countries;
  },

  localeListings() {
    return _collection.sampleSize(VALID_LOCALES, _number.random(1, 6))
      .map((locale) => {
        return {
          locale: locale,
          displayName: faker.commerce.productName(),
          iconId: faker.lorem.word(),
          shortDescription: faker.lorem.sentence(),
          longDescription: faker.lorem.paragraph(),
          gameEdition: faker.lorem.word(),
          gameEditionDetails: faker.lorem.words(),
          productFeatures: faker.lorem.words().split(' '),
          keywords: faker.lorem.words().split(' '),
          thankYouPageBackgroundId: faker.lorem.word(),
        };
    });
  }
});
