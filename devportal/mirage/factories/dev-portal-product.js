import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  id: function(i) {
    return i;
  },

  title: function() {
    return faker.commerce.productName();
  }
});
