export default function(server) {
  let dpps = server.createList('dev-portal-product', 2);
  server.createList('extension', 1, {
    devPortalProductId: dpps[0].id
  });
  server.createList('extension-item', 10, {
    devPortalProductId: dpps[0].id
  });
  server.createList('managed-product', 1, {
    devPortalProductId: dpps[1].id
  });
  server.createList('in-game-content', 10, {
    devPortalProductId: dpps[1].id
  });
}
