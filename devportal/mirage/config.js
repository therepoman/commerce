import moment from 'moment';

export default function() {
  this.urlPrefix = 'https://api.twitch.tv';
  this.namespace = '/v5';

  this.get('/devportal/products/', (schema, request) => {
    return schema.devPortalProducts.all();
  });

  this.get('/commerce/product/:id/managed-products', (schema, request) => {
    let id = request.params.id;
    let type = request.queryParams.type;

    if (type === 'ADG_EXTENSION') {
      return schema.extensions.where({ devPortalProductId: id });
    }

    if (type === 'ADG_EXTENSION_IAP') {
      return schema.extensionItems.where({ devPortalProductId: id });
    }

    if (type === 'ADG_IGC') {
      return schema.inGameContents.where({ devPortalProductId: id });
    }

    return schema.managedProducts.where({ devPortalProductId: id });
  });

  this.post('/commerce/product/:dpp_id/managed-products', (schema, request) => {
    let body = JSON.parse(request.requestBody).managedProduct;
    let type = body.type;

    if (type === 'ADG_EXTENSION') {
      return schema.extensions.create(body);
    }

    if (type === 'ADG_EXTENSION_IAP') {
      return schema.extensionItems.create(body);
    }

    if (type === 'ADG_IGC') {
      return schema.inGameContents.create(body);
    }

    return schema.managedProducts.create(body);
  });

  this.get('/commerce/product/:dpp_id/managed-products/:mp_id', (schema, request) => {
    let id = request.params.mp_id;

    if (schema.extensions.find(id)) { return schema.extensions.find(id); }
    if (schema.extensionItems.find(id)) { return schema.extensionItems.find(id); }
    if (schema.inGameContents.find(id)) { return schema.inGameContents.find(id); }
    if (schema.managedProducts.find(id)) { return schema.managedProducts.find(id); }
  });

  this.put('/commerce/product/:dpp_id/managed-products/:mp_id', (schema, request) => {
    let id = request.params.mp_id;
    let body = JSON.parse(request.requestBody).managedProduct;
    body.lastUpdated = moment().format('X');

    if (schema.extensions.find(id)) {
      let extension = schema.extensions.find(id);
      extension.update(body);
      return extension;
    }
    if (schema.extensionItems.find(id)) {
      let extensionItem = schema.extensionItems.find(id);
      extensionItem.update(body);
      return extensionItem;
    }
    if (schema.inGameContents.find(id)) {
      let inGameContent = schema.inGameContents.find(id);
      inGameContent.update(body);
      return inGameContent;
    }
    if (schema.managedProducts.find(id)) {
      let managedProduct = schema.managedProducts.find(id);
      managedProduct.update(body);
      return managedProduct;
    }
  });

  this.post('/commerce/product/:dpp_id/managed-products/:mp_id/submit',
    { message: '' }, 200);

  this.passthrough('https://api.twitch.tv/kraken/**');
}

