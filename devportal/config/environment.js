/* eslint-env node */

module.exports = function(environment) {
  const deployTarget = process.env.DEPLOY_TARGET;

  let ENV = {
    host: 'https://api.twitch.tv',
    modulePrefix: 'devportal-client',
    environment: environment,
    rootURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    },
    'ember-simple-auth': {
      baseURL: 'https://api.twitch.tv'
    }
  };

  if (environment === 'development') {
    ENV['ember-cli-mirage'] = {
      enabled: false
    };

    ENV.CLIENT_ID = '6z4wh200jgxac8mg6monrrrtnf4srn';
    ENV.OAUTH_URI = 'https://id.twitch.tv/oauth2/authorize';
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.CLIENT_ID = 'client-id-during-devportal-testing';
  }

  if (environment === 'production') {
    ENV.GOOGLE_SITE_VERIFICATION = '';
    ENV.GOOGLE_PUBLISHER_TAG = '';

    if (deployTarget === 'production') {
      ENV.CLIENT_ID = '69yoru7dj0kdtv6tbkbfei5imcxap8';
      ENV.OAUTH_URI = 'https://id.twitch.tv/oauth2/authorize';
    }

    if (deployTarget === 'staging') {
      ENV.host = 'https://visage.staging.us-west2.justin.tv',
      ENV.CLIENT_ID = 'qok1ow7dad63go3p1h0k551p3h1qxp';
      ENV.OAUTH_URI = 'https://id.twitch.tv/oauth2/authorize';
    }
  }

  return ENV;
};
