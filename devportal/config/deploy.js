/* eslint-env node */
'use strict';

module.exports = function(deployTarget) {
  let ENV = {
    build: {},

    's3': {
      region: 'us-west-2'
    },

    's3-index': {
      region: 'us-west-2',
      allowOverwrite: true
    },

    'revision-data': {
      type: 'file-hash',
      scm: null
    }
  };

  if (deployTarget === 'development') {
    ENV.build.environment = 'development';
    ENV['s3'].bucket = 'devportal2-dev.justin.tv';
    ENV['s3-index'].bucket = 'devportal2-dev.justin.tv';
  }

  if (deployTarget === 'staging') {
    ENV.build.environment = 'production';
    ENV['s3'].bucket = 'devportal2-staging.justin.tv';
    ENV['s3-index'].bucket = 'devportal2-staging.justin.tv';
  }

  if (deployTarget === 'production') {
    ENV.build.environment = 'production';
    ENV['s3'].bucket = 'devportal2.justin.tv';
    ENV['s3-index'].bucket = 'devportal2.justin.tv';
  }

  // Note: if you need to build some configuration asynchronously, you can return
  // a promise that resolves with the ENV object instead of returning the
  // ENV object synchronously.
  return ENV;
};
