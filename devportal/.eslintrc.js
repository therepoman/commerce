module.exports = {
  root: true,
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module'
  },
  plugins: ['ember-rules'],
  extends: [
    'eslint:recommended',
    'plugin:ember/recommended'
  ],
  env: {
    'browser': true
  },
  rules: {
    // Rules removed from eslint:recommended
    'indent': [0, 2],
    // Rules added beyond eslint:recommended
    'arrow-spacing': [2, { 'before': true, 'after': true }],
    'camelcase': [2, { properties: 'never' }],
    'comma-dangle': 2,
    'curly': [2, 'all'],
    'eol-last': 2,
    'eqeqeq': 2,
    'linebreak-style': 2,
    'no-caller': 2,
    'no-case-declarations': 2,
    'no-eq-null': 2,
    'no-eval': 2,
    'no-multi-str': 2,
    'no-new': 2,
    'no-var': 2,
    'operator-linebreak': [2, 'after'],
    'prefer-spread': 2,
    'semi': 2,
    'space-in-parens': [2, 'never'],
    'space-unary-ops': [2, { 'words': false, 'nonwords': false }],
    'quotes': ['error', 'single', { 'allowTemplateLiterals': true }],

    // Ember Rules
    'ember-rules/no-function-prototype-extension-calls': 1,

    // eslint-ember-plugin
    'ember/local-modules': 'off',
    'ember/use-ember-get-and-set': 'off',
    'ember/no-empty-attrs': 'off',
    'ember/named-functions-in-promises': 'off',
    'ember/no-old-shims': 'error',
    'ember/order-in-models': 'off',
    'ember/closure-actions': 'off'
  }
};
