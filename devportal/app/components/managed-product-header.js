import { later } from '@ember/runloop';
import Component from '@ember/component';

export default Component.extend({
  saving: false,
  submittedSuccessfully: false,
  showSubmissionMessageBrick: false,

  actions: {
    saveProduct(product) {
      this.set('saving', true);
      this.sendAction('save', product, this.resetSavingFlag.bind(this), this.resetSavingFlag.bind(this));
    },
    submitProduct(product) {
      if (product.get('validations.isValid')) {
        this.set('saving', true);
        this.sendAction('submit', product, this.handleSubmitSuccess.bind(this), this.handleSubmitFailure.bind(this));
      }
    }
  },

  resetSavingFlag() {
    this.set('saving', false);
  },

  handleSubmitSuccess() {
    this.set('submittedSuccessfully', true);
    this.set('showSubmissionMessageBrick', true);
    this.set('submissionResponse', 'Submitted successfully!');
    this.set('saving', false);
    later(this.hideSubmissionMessageBrick.bind(this), 5000);
  },

  handleSubmitFailure(error) {
    this.set('showSubmittedMessage', true);
    this.set('submissionResponse', 'Submission failed: ' + error.payload.message);
    this.set('saving', false);
    later(this.hideSubmissionMessageBrick.bind(this), 5000);
  },

  hideSubmissionMessageBrick() {
    this.set('showSubmissionMessageBrick', false);
  }
});
