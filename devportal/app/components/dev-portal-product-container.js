import Component from '@ember/component';

export default Component.extend({
  classNames: ['content'],

  init() {
    this._super(...arguments);

    // if there are no dev portal products, don't select one
    let devPortalProducts = this.get('dpps');
    if (devPortalProducts.length === 0) { return; }

    // set selected dev portal products to first in list
    this.set('selectedIndex', 0);
    this.set('selectedDevPortalProduct', devPortalProducts.get('firstObject'));
  },

  actions: {
    selectDevPortalProduct(devPortalProduct, index) {
      this.set('selectedIndex', index);
      this.set('selectedDevPortalProduct', devPortalProduct);
    }
  }
});
