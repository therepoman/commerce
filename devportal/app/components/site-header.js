import Component from '@ember/component';
import { inject as injectService } from '@ember/service';

export default Component.extend({
  drawerToggled: false,

  session: injectService(),
  currentUser: injectService(),

  actions: {
    logout() {
      this.get('session').invalidate();
    }
  }
});
