import Component from '@ember/component';
import { later } from '@ember/runloop';
import { isArray } from '@ember/array';

const VALID_LOCALES = [
  'en-US',
  'en-GB',
  'de-DE',
  'es-ES',
  'fr-FR',
  'it-IT',
  'en-CA',
  'fr-CA'
];

export default Component.extend({
  saving: false,
  submittedSuccessfully: false,
  showSubmissionMessageBrick: false,

  init() {
    this._super(...arguments);
    this.updateRemainingLocales();
  },

  actions: {
    addLocaleCard() {
      let newLocale = this.get('newLocale');
      let localeListings = this.get('extension.localeListings');
      localeListings.createFragment({
        locale: newLocale
      });
      this.updateRemainingLocales();
    },
    removeLocaleCard(locale) {
      let localeListings = this.get('extension.localeListings');
      let listingToDelete = localeListings.find(function(listing) {
        return listing.get('locale') === locale;
      });
      localeListings.removeObject(listingToDelete);
      this.updateRemainingLocales();
    },
    copyToAllListings(key, value) {
      let localeListings = this.get('extension.localeListings');

      localeListings.forEach(function(listing) {
        if (isArray(value)) {
          value = value.toArray();
        }
        listing.set(key, value);
      });
    },
    saveExtension(extension) {
      this.set('saving', true);
      this.sendAction('save', extension, this.resetSavingFlag.bind(this), this.resetSavingFlag.bind(this));
    },
    submitExtension(extension) {
      if (extension.get('validations.isValid')) {
        this.set('saving', true);
        this.sendAction('submit', extension, this.handleSubmitSuccess.bind(this), this.handleSubmitFailure.bind(this));
      }
    }
  },

  updateRemainingLocales() {
    let existingLocales = this.get('extension.localeListings').mapBy('locale');
    let remainingLocales = VALID_LOCALES.filter(function(locale) {
      return !existingLocales.includes(locale);
    });
    this.set('remainingLocales', remainingLocales);
    this.set('newLocale', remainingLocales ? remainingLocales[0] : '');
  },

  resetSavingFlag() {
    this.set('saving', false);
  },

  handleSubmitSuccess() {
    this.set('submittedSuccessfully', true);
    this.set('showSubmissionMessageBrick', true);
    this.set('submissionResponse', 'Submitted successfully!');
    this.set('saving', false);
    later(this.hideSubmissionMessageBrick.bind(this), 5000);
  },

  handleSubmitFailure(error) {
    this.set('showSubmittedMessage', true);
    this.set('submissionResponse', 'Submission failed: ' + error.payload.message);
    this.set('saving', false);
    later(this.hideSubmissionMessageBrick.bind(this), 5000);
  },

  hideSubmissionMessageBrick() {
    this.set('showSubmissionMessageBrick', false);
  }
});
