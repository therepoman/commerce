import Component from '@ember/component';
import { isArray } from '@ember/array';

const VALID_LOCALES = [
  'en-US',
  'en-GB',
  'de-DE',
  'es-ES',
  'fr-FR',
  'it-IT',
  'en-CA',
  'fr-CA'
];

export default Component.extend({

  init() {
    this._super(...arguments);
    this.updateRemainingLocales();
  },

  actions: {
    addLocaleCard() {
      let newLocale = this.get('newLocale');
      let localeListings = this.get('managedProduct.localeListings');
      localeListings.createFragment({
        locale: newLocale
      });
      this.updateRemainingLocales();
    },
    removeLocaleCard(locale) {
      let localeListings = this.get('managedProduct.localeListings');
      let listingToDelete = localeListings.find(function(listing) {
        return listing.get('locale') === locale;
      });
      localeListings.removeObject(listingToDelete);
      this.updateRemainingLocales();
    },
    copyToAllListings(key, value) {
      let localeListings = this.get('managedProduct.localeListings');

      localeListings.forEach(function(listing) {
        if (isArray(value)) {
          value = value.toArray();
        }
        listing.set(key, value);
      });
    }
  },

  updateRemainingLocales() {
    let existingLocales = this.get('managedProduct.localeListings').mapBy('locale');
    let remainingLocales = VALID_LOCALES.filter(function(locale) {
      return !existingLocales.includes(locale);
    });
    this.set('remainingLocales', remainingLocales);
    this.set('newLocale', remainingLocales ? remainingLocales[0] : '');
  }
});
