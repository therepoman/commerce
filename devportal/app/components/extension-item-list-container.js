import Component from '@ember/component';

export default Component.extend({
  actions: {
    addItemBtnHandler() {
      this.sendAction('createExtensionItem');
    }
  }
});
