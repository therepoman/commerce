import Component from '@ember/component';

export default Component.extend({
  actions: {
    trimValue() {
      let parentModel = this.get('parentModel');
      let fieldName = this.get('fieldName');
      let currentValue = parentModel.get(fieldName);
      if (!this.get('number')) {
        currentValue = currentValue.trim();
      }
      parentModel.set(fieldName, currentValue);
    }
  }
});
