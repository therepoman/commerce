import Component from '@ember/component';

export default Component.extend({
  tagName: 'nav',
  classNames: ['sidebar-nav', 'flex__item']
});
