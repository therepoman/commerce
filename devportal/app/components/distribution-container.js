import Component from '@ember/component';

export default Component.extend({
  init() {
    this._super(...arguments);
    this.set('oldDefaultPrice', this.get('managedProduct.defaultPrice'));
  },

  actions: {
    setReleaseDate(date) {
      let managedProduct = this.get('managedProduct');
      managedProduct.set('releaseDate', date);
      managedProduct.set('announceDate', date);
      managedProduct.set('previousReleaseDate', date);
    },
    setPriceForAllCountries() {
      let newAmount = this.get('managedProduct.defaultPrice');
      let countries = this.get('managedProduct.availabilityInfo');
      countries.forEach((country) => {
        let countryPrice = country.get('price');
        let oldDefaultPrice = this.get('oldDefaultPrice');
        if (countryPrice === oldDefaultPrice) {
          country.set('price', newAmount);
        }
      });
      this.set('oldDefaultPrice', newAmount);
    }
  }
});
