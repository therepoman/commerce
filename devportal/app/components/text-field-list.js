import Component from '@ember/component';

export default Component.extend({
  actions: {
    appendToList(newItem) {
      if (newItem) {
        this.get('list').unshiftObject(newItem.trim());
        this.set('newItem', '');
      }
    },
    removeFromList(index) {
      this.get('list').removeAt(index);
    }
  }
});
