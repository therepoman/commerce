import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  filter: '',

  filteredCountries: computed('filter', 'countries', function() {
    let filter = this.get('filter');

    return this.get('countries').filter((country) => {
      return country.get('countryCode').startsWith(filter.toUpperCase());
    });
  }),

  actions: {
    setAvailableInFilteredCountries() {
      this.get('filteredCountries').setEach('available', true);
    },
    unsetAvailableInFilteredCountries() {
      this.get('filteredCountries').setEach('available', false);
    }
  }
});
