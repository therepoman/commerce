import Component from '@ember/component';

const SUPPORTED_LANGUAGES = [
  'arabic',
  'czech',
  'danish',
  'dutch',
  'egyptian',
  'english',
  'farsi',
  'finnish',
  'french',
  'french_canadian',
  'german',
  'greek',
  'hebrew',
  'hindi',
  'hungarian',
  'indonesian',
  'italian',
  'japanese',
  'korean',
  'latin',
  //'none',
  'norwegian',
  'persian',
  'polish',
  'portuguese',
  'portuguese_brazilian',
  'russian',
  'simplified_chinese',
  'slavic',
  'spanish',
  'swedish',
  'thai',
  'traditional_chinese',
  'turkish',
  'ukranian',
  //'unknown',
  'vietnamese'
];

export default Component.extend({

  languages: SUPPORTED_LANGUAGES,

  actions: {
    appendToList(list, fieldName, newItem) {
      if (newItem) {
        list.unshiftObject(newItem);
        this.set(fieldName, '');
      }
    },
    removeFromList(list, index) {
      list.removeAt(index);
    },
    toggleSupportedLanguage(languages, language) {
      if (languages.includes(language)) {
        languages.removeObject(language);
      } else {
        languages.pushObject(language);
      }
    }
  }
});
