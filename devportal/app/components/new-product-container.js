import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  createRequestSent: false,

  invalidVendorId: computed('adgVendorId', function() {
    return !this.get('adgVendorId');
  }),

  actions: {
    createNewProduct(adgVendorId) {
      if (adgVendorId) {
        this.set('createRequestSent', true);
        this.sendAction('createNewProduct', adgVendorId);
      }
    }
  }
});
