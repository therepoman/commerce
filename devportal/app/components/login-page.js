import Component from '@ember/component';
import ENV from 'devportal-client/config/environment';

export default Component.extend({

  actions: {
    authenticateWithTwitch() {
      let oauthURI = ENV.OAUTH_URI;
      let clientId = ENV.CLIENT_ID;
      let redirectURI = `${window.location.origin}/oauth-callback`;
      let scope = 'user_read';
      window.location.replace(`${oauthURI}` +
                              `?response_type=token` +
                              `&client_id=${clientId}` +
                              `&redirect_uri=${redirectURI}` +
                              `&scope=${scope}`);
    }
  }
});
