import Router from '@ember/routing/router';
import config from './config/environment';
import { on } from '@ember/object/evented';

const r = Router.extend({
  location: config.locationType,
  rootURL: config.rootURL,
  updatePageTitle: on('didTransition', function () {
    this.send('updatePageTitle');
  })
});

r.map(function() {
  this.route('login');
  this.route('oauth-callback');
  this.route('products');
  this.route('product', { path: 'products/:dev_portal_product_id' }, function() {
    this.route('commerce', function() {
      this.route('new');
      this.route('managed-product', { path: '/:managed_product_id' }, function() {
        this.route('details');
        this.route('distribution');
        this.route('listing');
      });
      this.route('igc', function() {
        this.route('edit', { path: '/:igc_id' });
      });
    });
    this.route('extensions', function() {
      this.route('new');
      this.route('extension', { path: '/:extension_id' });
      this.route('extension-items');
      this.route('extension-item-edit', { path: '/extension-items/:extension_item_id'});
    });
  });
});

export default r;
