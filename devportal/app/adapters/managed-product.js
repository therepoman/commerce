import RESTAdapter from 'ember-data/adapters/rest';
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';
import ENV from 'devportal-client/config/environment';

export default RESTAdapter.extend(DataAdapterMixin, {

  host: ENV.host,
  authorizer: 'authorizer:oauth2',

  pathForType() {
    return 'managed-products';
  }
});
