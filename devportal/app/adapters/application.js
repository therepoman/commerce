import RESTAdapter from 'ember-data/adapters/rest';
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';
import ENV from 'devportal-client/config/environment';

export default RESTAdapter.extend(DataAdapterMixin, {
  authorizer: 'authorizer:oauth2',
  host: ENV.host,
  headers: {
    'Client-ID': ENV.CLIENT_ID,
    'Accept': 'application/vnd.twitchtv.v3+json'
  }
});
