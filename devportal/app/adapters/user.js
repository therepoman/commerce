import KrakenAdapter from './kraken';

export default KrakenAdapter.extend({
  pathForType() {
    return 'user';
  }
});
