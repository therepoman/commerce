import $ from 'jquery';
import { Promise as EmberPromise } from 'rsvp';
import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({
  namespace: 'v5/devportal',

  pathForType() {
    return 'products';
  },

  findRecord(store, type, id) {
    let _this = this;
    return new EmberPromise(function(resolve, reject) {
      _this.findAll(store, type).then(function(records) {
        let matchingRecords = $.grep(records.devPortalProducts, (dpp) => {
          return dpp.id === id;
        });

        if (matchingRecords) {
          resolve({ devPortalProduct: matchingRecords[0] });
        }

        reject();
      }, function(error) {
        reject(error);
      });
    });
  }
});
