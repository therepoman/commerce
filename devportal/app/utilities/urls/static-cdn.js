export const BASE_URL = 'https://static-cdn.jtvnw.net';

// Badges
export const BADGES_URL = `${ BASE_URL }/badges/v1`;
export const BADGES_DEFAULT_SUBSCRIBER_18X18_URL = `${ BADGES_URL }/19dd8673-124d-4f44-830c-b0f4f9d78635/1`;
export const BADGES_DEFAULT_SUBSCRIBER_36X36_URL = `${ BADGES_URL }/19dd8673-124d-4f44-830c-b0f4f9d78635/2`;
export const BADGES_DEFAULT_SUBSCRIBER_72X72_URL = `${ BADGES_URL }/19dd8673-124d-4f44-830c-b0f4f9d78635/3`;

// Bits
export const BASE_BITS_URL = 'https://d3aqoihi2n8ty8.cloudfront.net';
export const BITS_URL = `${ BASE_BITS_URL }/cheer`;
export const BITS_DARK_URL = `${ BITS_URL }/dark`;
export const BITS_LIGHT_URL = `${ BITS_URL }/light`;
export const BITS_DARK_ANIMATED_URL = `${ BITS_DARK_URL }/animated`;
export const BITS_LIGHT_ANIMATED_URL = `${ BITS_LIGHT_URL }/animated`;
export const BITS_DARK_ANIMATED_PROMO_URL = `${ BITS_DARK_ANIMATED_URL }/promo`;
export const BITS_LIGHT_ANIMATED_PROMO_URL = `${ BITS_LIGHT_ANIMATED_URL }/promo`;
export const BITS_DARK_ANIMATED_PROMO_INTRO_URL = `${ BITS_DARK_ANIMATED_PROMO_URL }/intro.gif`;
export const BITS_LIGHT_ANIMATED_PROMO_INTRO_URL = `${ BITS_LIGHT_ANIMATED_PROMO_URL }/intro.gif`;

// Emoticons
export const EMOTICONS_URL = `${ BASE_URL }/emoticons`;
export const EMOTICONS_V1_URL = `${ EMOTICONS_URL }/v1`;

// JTV user pictures
export const JTV_USER_PICTURES_URL = `${ BASE_URL }/jtv_user_pictures`;
export const JTV_USER_PICTURES_404_USER_70X70_URL = `${ JTV_USER_PICTURES_URL }/xarth/404_user_70x70.png`;
export const JTV_USER_PICTURES_404_USER_150X150_URL = `${ JTV_USER_PICTURES_URL }/xarth/404_user_150x150.png`;

// Previews TTV
export const PREVIEWS_TTV_URL = `${ BASE_URL }/previews-ttv`;

// TTV boxart
export const TTV_BOXART_URL = `${ BASE_URL }/ttv-boxart`;
export const TTV_BOXART_404_URL = `${ TTV_BOXART_URL }/404_boxart.png`;

// TTV logoart
export const TTV_LOGOART_URL = `${ BASE_URL }/ttv-logoart`;
