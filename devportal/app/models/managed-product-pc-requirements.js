import attr from 'ember-data/attr';
import Fragment from 'ember-data-model-fragments/fragment';
import { validator, buildValidations } from 'ember-cp-validations';

const Validations = buildValidations({
  os: validator('presence', {
    presence: true,
    ignoreBlank: true,
    description: 'OS'
  }),
  processor: validator('presence', {
    presence: true,
    ignoreBlank: true,
    description: 'Processor'
  }),
  ram: validator('presence', {
    presence: true,
    ignoreBlank: true,
    description: 'RAM'
  }),
  hardDriveSpace: validator('presence', {
    presence: true,
    ignoreBlank: true,
    description: 'Hard Drive Space'
  }),
  videoCard: validator('presence', {
    presence: true,
    ignoreBlank: true,
    description: 'Video Card'
  }),
  directX: validator('presence', {
    presence: true,
    ignoreBlank: true,
    description: 'DirectX'
  })
});

export default Fragment.extend(Validations, {
  os: attr('string'),
  processor: attr('string'),
  ram: attr('string'),
  hardDriveSpace: attr('string'),
  videoCard: attr('string'),
  directX: attr('string')
});
