import attr from 'ember-data/attr';
import { computed } from '@ember/object';
import Fragment from 'ember-data-model-fragments/fragment';
import { validator, buildValidations } from 'ember-cp-validations';

const Validations = buildValidations({
  priceAsNumber: {
    description: 'Price',
    validators: [
      validator('presence', true),
      validator('number', {
        positive: true
      })
    ]
  }
});

export default Fragment.extend(Validations, {
  countryCode: attr('string'),
  available: attr('boolean', { defaultValue: true }),
  price: attr('number', { defaultValue: 0 }),
  priceAsNumber: computed('price', {
    get: function () {
      let returnValue = parseFloat(this.get('price'));
      return isNaN(returnValue) ? 0 : returnValue;
    },
    set: function (key, value) {
      let valueToSet = parseFloat(value);
      if (isNaN(valueToSet)) {
        valueToSet = 0;
      }
      this.set('price', valueToSet);
      return valueToSet;
    }
  })
});
