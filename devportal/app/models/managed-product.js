import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { computed } from '@ember/object';
import { fragment, fragmentArray, array } from 'ember-data-model-fragments/attributes';
import { validator, buildValidations } from 'ember-cp-validations';

const DEFAULT_LOCALE = 'en-US';

const Validations = buildValidations({
  sku: validator('presence', {
    presence: true,
    ignoreBlank: true,
    description: 'SKU'
  }),
  developerName: validator('presence', {
    presence: true,
    ignoreBlank: true,
    description: 'Developer Name'
  }),
  developerWebsiteUrl: {
    description: 'Developer Website URL',
    validators: [
      validator('presence', true),
      validator('format', { type: 'url' })
    ]
  },
  customerSupportUrl: {
    description: 'Customer Support URL',
    validators: [
      validator('presence', true),
      validator('format', { type: 'url' })
    ]
  },
  customerSupportEmail: {
    description: 'Customer Support Email',
    validators: [
      validator('presence', true),
      validator('format', { type: 'email' })
    ]
  },
  defaultPriceAsNumber: {
    description: 'Default Price',
    validators: [
      validator('presence', true),
      validator('number', {
        positive: true
      })
    ]
  },
  releaseDate: {
    description: 'Release Date',
    validators: [
      validator('presence', true),
      validator('date', {
        format: 'YYYY-MM-DDTHH:mm:ssZ'
      })
    ]
  },
  announceDate: {
    description: 'Announce Date',
    validators: [
      validator('presence', true),
      validator('date', {
        format: 'YYYY-MM-DDTHH:mm:ssZ'
      })
    ]
  },
  previousReleaseDate: {
    description: 'Previous Release Date',
    validators: [
      validator('presence', true),
      validator('date', {
        format: 'YYYY-MM-DDTHH:mm:ssZ'
      })
    ]
  },
  minimumPcRequirements: validator('belongs-to'),
  recommendedPcRequirements: validator('belongs-to'),
  availabilityInfo: validator('has-many'),
  localeListings: validator('has-many')
});

export default Model.extend(Validations, {
  devPortalProductId: attr('string'),
  adgVendorId: attr('string'),
  adgDomainId: attr('string'),
  adgProductId: attr('string'),
  lastUpdated: attr('string'),
  type: attr('string'),
  sku: attr('string'),
  fulfillmentMethod: attr('string', { defaultValue: 'fulfilled_FuelLauncher_default' }),
  minimumPcRequirements: fragment('managed-product-pc-requirements'),
  recommendedPcRequirements: fragment('managed-product-pc-requirements'),
  otherPcRequirements: attr('string'),
  requiresContinuousConnectivity: attr('boolean', { defaultValue: false }),
  numberOfPlayersSupported: attr('string'),
  languageSupport: fragmentArray('managed-product-language-support', { defaultValue: [] }),
  version: attr('string'),
  supportedControllers: attr('string'),
  categoryInfoId: attr('string', { defaultValue: 'twitch_fuel_action_game' }),
  developerName: attr('string'),
  developerWebsiteUrl: attr('string'),
  defaultPrice: attr('number', { defaultValue: 0 }),
  availabilityInfo: fragmentArray('managed-product-availability-info', { defaultValue: [] }),
  releaseDate: attr('date', { defaultValue() { return new Date(); } }),
  announceDate: attr('date', { defaultValue() { return new Date(); } }),
  previousReleaseDate: attr('date', { defaultValue() { return new Date(); } }),
  broadcasterVodUrls: array('string'),
  newsAndReviewsUrls: array('string'),
  customerSupportUrl: attr('string'),
  customerSupportEmail: attr('string'),
  localeListings: fragmentArray('managed-product-listing', { defaultValue: [] }),
  esrbRating: fragment('managed-product-rating'),
  pegiRating: attr('string', { defaultValue: 'to_be_announced' }),
  uskRating: attr('string', { defaultValue: 'to_be_announced' }),
  amazonRating: attr('string', { defaultValue: 'rating_pending' }),

  title: computed('localeListings.@each.displayName', 'sku', function() {
    let localeListings = this.get('localeListings');
    let defaultListing = localeListings.findBy('locale', DEFAULT_LOCALE);

    if (defaultListing && defaultListing.get('displayName')) {
      return defaultListing.get('displayName');
    }

    if (localeListings.get('length') !== 0 && localeListings.get('firstObject').get('displayName')) {
      return localeListings.get('firstObject').get('displayName');
    }

    if (this.get('sku')) {
      return this.get('sku');
    }

    return 'New managed product';
  }),

  defaultPriceAsNumber: computed('defaultPrice', {
    get: function () {
      let returnValue = parseFloat(this.get('defaultPrice'));
      return isNaN(returnValue) ? 0 : returnValue;
    },
    set: function (key, value) {
      let valueToSet = parseFloat(value);
      if (isNaN(valueToSet)) {
        valueToSet = 0;
      }
      this.set('defaultPrice', valueToSet);
      return valueToSet;
    }
  })
});
