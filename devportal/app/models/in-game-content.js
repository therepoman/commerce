import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { computed } from '@ember/object';
import { fragmentArray } from 'ember-data-model-fragments/attributes';
import { validator, buildValidations } from 'ember-cp-validations';

const DEFAULT_LOCALE = 'en-US';

const Validations = buildValidations({
  sku: validator('presence', {
    presence: true,
    ignoreBlank: true,
    description: 'SKU'
  }),
  defaultPriceAsNumber: {
    description: 'Default Price',
    validators: [
      validator('presence', true),
      validator('number', {
        positive: true
      })
    ]
  },
  releaseDate: {
    description: 'Release Date',
    validators: [
      validator('presence', true),
      validator('date', {
        format: 'YYYY-MM-DDTHH:mm:ssZ'
      })
    ]
  },
  availabilityInfo: validator('has-many'),
  localeListings: validator('has-many')
});

export default Model.extend(Validations, {
  devPortalProductId: attr('string'),
  adgVendorId: attr('string'),
  adgDomainId: attr('string'),
  domainIdOverride: attr('string'),
  adgProductId: attr('string'),
  lastUpdated: attr('string'),
  type: attr('string', { defaultValue: 'ADG_IGC' }),
  sku: attr('string'),
  multiplePurchases: attr('boolean', { defaultValue: false }),
  durable: attr('boolean', { defaultValue: false }),
  inGameCurrency: attr('boolean', { defaultValue: false }),
  fulfillmentMethod: attr('string', { defaultValue: 'fulfilled_FuelLauncher_default' }),
  categoryInfoId: attr('string', { defaultValue: 'game-microtransactions-for-currency' }),
  defaultPrice: attr('number', { defaultValue: 0 }),
  availabilityInfo: fragmentArray('in-game-content-availability-info', { defaultValue: [] }),
  releaseDate: attr('date', { defaultValue() { return new Date(); } }),
  localeListings: fragmentArray('in-game-content-listing', { defaultValue: [] }),

  title: computed('localeListings.@each.displayName', 'sku', function() {
    let localeListings = this.get('localeListings');
    let defaultListing = localeListings.findBy('locale', DEFAULT_LOCALE);

    if (defaultListing && defaultListing.get('displayName')) {
      return defaultListing.get('displayName');
    }

    if (localeListings.get('length') !== 0 && localeListings.get('firstObject').get('displayName')) {
      return localeListings.get('firstObject').get('displayName');
    }

    if (this.get('sku')) {
      return this.get('sku');
    }

    return 'New in-game content';
  }),

  defaultPriceAsNumber: computed('defaultPrice', {
    get: function () {
      let returnValue = parseFloat(this.get('defaultPrice'));
      return isNaN(returnValue) ? 0 : returnValue;
    },
    set: function (key, value) {
      let valueToSet = parseFloat(value);
      if (isNaN(valueToSet)) {
        valueToSet = 0;
      }
      this.set('defaultPrice', valueToSet);
      return valueToSet;
    }
  })
});
