import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { computed } from '@ember/object';
import {
  JTV_USER_PICTURES_404_USER_70X70_URL
} from 'devportal-client/utilities/urls/static-cdn';

const USER_TYPE_STAFF = 'staff';
// const USER_TYPE_USER = 'user';

export default Model.extend({
  bio: attr('string'),
  displayName: attr('string'),
  email: attr('string'),
  logo: attr('string', { defaultValue: JTV_USER_PICTURES_404_USER_70X70_URL }),
  name: attr('string'),
  notifications: attr(), // eslint-disable-line ember/no-empty-attrs
  partnered: attr('boolean'),
  type: attr('string'),

  staff: computed.equal('type', USER_TYPE_STAFF)
});
