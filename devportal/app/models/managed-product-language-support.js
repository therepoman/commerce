import attr from 'ember-data/attr';
import Fragment from 'ember-data-model-fragments/fragment';

export default Fragment.extend({
  languageCode: attr('string'),
  instructions: attr('boolean', { defaultValue: false }),
  subtitlesAndCaptions: attr('boolean', { defaultValue: false }),
  audio: attr('boolean', { defaultValue: false })
});
