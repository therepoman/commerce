import attr from 'ember-data/attr';
import Fragment from 'ember-data-model-fragments/fragment';
import { array } from 'ember-data-model-fragments/attributes';
import { validator, buildValidations } from 'ember-cp-validations';

const Validations = buildValidations({
  displayName: validator('presence', {
    presence: true,
    ignoreBlank: true,
    description: 'Display Name'
  }),
  shortDescription: {
    description: 'Short Description',
    validators: [
      validator('presence', true),
      validator('length', { max: 1600 })
    ]
  },
  longDescription: {
    description: 'Long Description',
    validators: [
      validator('presence', true),
      validator('length', { max: 4000 })
    ]
  },
  iconId: validator('presence', {
    presence: true,
    ignoreBlank: true,
    description: 'Icon Asset ID'
  }),
  boxShotId: validator('presence', {
    presence: true,
    ignoreBlank: true,
    description: 'Box Shot Asset ID'
  }),
  pageBackgroundId: validator('presence', {
    presence: true,
    ignoreBlank: true,
    description: 'Page Background Asset ID'
  }),
  thankYouPageBackgroundId: validator('presence', {
    presence: true,
    ignoreBlank: true,
    description: '"Thank You" Page Background Asset ID'
  }),
  eulaUrl: validator('format', {
    type: 'url',
    allowBlank: 'true',
    description: 'EULA URL'
  })
});

export default Fragment.extend(Validations, {
  locale: attr('string'),
  displayName: attr('string'),
  shortDescription: attr('string'),
  longDescription: attr('string'),
  productFeatures: array('string'),
  keywords: array('string'),
  gameEdition: attr('string'),
  gameEditionDetails: attr('string'),
  iconId: attr('string'),
  boxShotId: attr('string'),
  screenshotIds: array('string'),
  pageBackgroundId: attr('string'),
  thankYouPageBackgroundId: attr('string'),
  eulaUrl: attr('string'),
  releaseNotesId: attr('string')
});
