import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { computed } from '@ember/object';
import { fragmentArray } from 'ember-data-model-fragments/attributes';
import { validator, buildValidations } from 'ember-cp-validations';

const DEFAULT_LOCALE = 'en-US';

const TYPES = [
  'other',
  'remotely_accessed_digital_good',
  'remotely_accessed_digital_game',
  'general_digital_good',
  'streaming_audio',
  'services',
  'subscriptions'
];

const Validations = buildValidations({
  sku: validator('presence', {
    presence: true,
    ignoreBlank: true,
    description: 'SKU'
  }),
  defaultPriceAsNumber: {
    description: 'Default Price',
    validators: [
      validator('presence', true),
      validator('number', {
        positive: true
      })
    ]
  },
  availabilityInfo: validator('has-many'),
  localeListings: validator('has-many')
});

export default Model.extend(Validations, {
  devPortalProductId: attr('string'),
  sku: attr('string'),
  lastUpdated: attr('string'),
  type: attr('string', { defaultValue: 'ADG_EXTENSION_IAP' }),
  adgVendorId: attr('string'),
  adgDomainId: attr('string'),
  adgProductId: attr('string'),
  itemType: attr('string', { defaultValue: 'GeneralDigitalGood' }),
  multiplePurchases: attr('boolean', { defaultValue: false }),
  durable: attr('boolean', { defaultValue: false }),
  defaultPrice: attr('number', { defaultValue: 0 }),
  availabilityInfo: fragmentArray('extension-item-availability-info', { defaultValue: [] }),
  localeListings: fragmentArray('extension-item-listing', { defaultValue: [] }),

  title: computed('localeListings.@each.displayName', 'sku', function() {
    let localeListings = this.get('localeListings');
    let defaultListing = localeListings.findBy('locale', DEFAULT_LOCALE);

    if (defaultListing && defaultListing.get('displayName')) {
      return defaultListing.get('displayName');
    }

    if (localeListings.get('length') !== 0 && localeListings.get('firstObject').get('displayName')) {
      return localeListings.get('firstObject').get('displayName');
    }

    if (this.get('sku')) {
      return this.get('sku');
    }

    return 'New extension item';
  }),

  defaultPriceAsNumber: computed('defaultPrice', {
    get: function () {
      let returnValue = parseFloat(this.get('defaultPrice'));
      return isNaN(returnValue) ? 0 : returnValue;
    },
    set: function (key, value) {
      let valueToSet = parseFloat(value);
      if (isNaN(valueToSet)) {
        valueToSet = 0;
      }
      this.set('defaultPrice', valueToSet);
      return valueToSet;
    }
  })
});
