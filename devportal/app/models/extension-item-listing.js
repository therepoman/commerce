import attr from 'ember-data/attr';
import Fragment from 'ember-data-model-fragments/fragment';
import { array } from 'ember-data-model-fragments/attributes';
import { validator, buildValidations } from 'ember-cp-validations';

const Validations = buildValidations({
  displayName: validator('presence', {
    presence: true,
    ignoreBlank: true,
    description: 'Display Name'
  }),
  boxShotId: validator('presence', {
    presence: true,
    ignoreBlank: true,
    description: 'Box Shot ID'
  }),
  shortDescription: {
    description: 'Short Description',
    validators: [
      validator('presence', true),
      validator('length', { max: 1600 })
    ]
  },
  longDescription: {
    description: 'Long Description',
    validators: [
      validator('presence', true),
      validator('length', { max: 4000 })
    ]
  }
});

export default Fragment.extend(Validations, {
  locale: attr('string'),
  displayName: attr('string'),
  boxShotId: attr('string'),
  shortDescription: attr('string'),
  longDescription: attr('string'),
  keywords: array('string')
});
