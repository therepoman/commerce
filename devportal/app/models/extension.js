import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { computed } from '@ember/object';
import { fragmentArray } from 'ember-data-model-fragments/attributes';
import { validator, buildValidations } from 'ember-cp-validations';

const DEFAULT_LOCALE = 'en-US';

const Validations = buildValidations({
  sku: validator('presence', {
    presence: true,
    ignoreBlank: true,
    description: 'SKU'
  }),
  developerName: validator('presence', {
    presence: true,
    ignoreBlank: true,
    description: 'Developer Name'
  }),
  developerWebsiteUrl: {
    description: 'Developer Website URL',
    validators: [
      validator('presence', true),
      validator('format', { type: 'url' })
    ]
  },
  customerSupportUrl: {
    description: 'Customer Support URL',
    validators: [
      validator('presence', true),
      validator('format', { type: 'url' })
    ]
  },
  customerSupportEmail: {
    description: 'Customer Support Email',
    validators: [
      validator('presence', true),
      validator('format', { type: 'email' })
    ]
  },
  localeListings: validator('has-many')
});

export default Model.extend(Validations, {
  devPortalProductId: attr('string'),
  sku: attr('string'),
  type: attr('string', { defaultValue: 'ADG_EXTENSION' }),
  adgVendorId: attr('string'),
  adgDomainId: attr('string'),
  adgProductId: attr('string'),
  developerName: attr('string'),
  developerWebsiteUrl: attr('string'),
  customerSupportUrl: attr('string'),
  customerSupportEmail: attr('string'),
  lastUpdated: attr('string'),
  localeListings: fragmentArray('extension-listing', { defaultValue: [] }),

  title: computed('localeListings.@each.displayName', 'sku', function() {
    let localeListings = this.get('localeListings');
    let defaultListing = localeListings.findBy('locale', DEFAULT_LOCALE);

    if (defaultListing && defaultListing.get('displayName')) {
      return defaultListing.get('displayName');
    }

    if (localeListings.get('length') !== 0 && localeListings.get('firstObject').get('displayName')) {
      return localeListings.get('firstObject').get('displayName');
    }

    if (this.get('sku')) {
      return this.get('sku');
    }

    return 'New extension';
  })
});
