import attr from 'ember-data/attr';
import Fragment from 'ember-data-model-fragments/fragment';
import { array } from 'ember-data-model-fragments/attributes';
import { validator, buildValidations } from 'ember-cp-validations';

const Validations = buildValidations({
  displayName: validator('presence', {
    presence: true,
    ignoreBlank: true,
    description: 'Display name'
  })
});

export default Fragment.extend(Validations, {
  locale: attr('string'),
  displayName: attr('string'),
  keywords: array('string')
});
