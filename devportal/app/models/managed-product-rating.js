import attr from 'ember-data/attr';
import Fragment from 'ember-data-model-fragments/fragment';
import { array } from 'ember-data-model-fragments/attributes';

const RATING_CATEGORIES = {
  esrb: [
    'rating_pending',
    'early_childhood',
    'everyone',
    'everyone_10_plus',
    'teen',
    'mature',
    'adults_only'
  ],
  pegi: [
    'to_be_announced',
    'ages_3_and_over',
    'ages_7_and_over',
    'ages_12_and_over',
    'ages_16_and_over',
    'ages_18_and_over',
    'unknown'
  ],
  usk: [
    'to_be_announced',
    'educational',
    'infotainment',
    'checked_by_legal_department',
    'not_checked',
    'ages_12_and_over',
    'ages_16_and_over',
    'ages_6_and_over',
    'without_age_limitation',
    'ages_18_and_over',
    'cannot_publicize',
    'unknown'
  ],
  amazon: [ 'rating_pending' ]
};

export default Fragment.extend({
  ratingCategory: attr('string', { defaultValue: 'rating_pending' }),
  contentDescriptors: array('string')
});
