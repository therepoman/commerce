import Route from '@ember/routing/route';
import PageTitleRouteMixin from 'devportal-client/mixins/page-title-route';

Route.reopen(PageTitleRouteMixin);
