import Service from '@ember/service';
import { inject as injectService } from '@ember/service';

export default Service.extend({
  session: injectService(),
  store: injectService(),
  user: null,

  load() {
    if (this.get('session.isAuthenticated')) {
      return this.get('store').queryRecord('user', {})
        .then(this.set.bind(this, 'user'));
    }
  }
});
