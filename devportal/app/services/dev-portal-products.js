import Service from '@ember/service';
import { inject as injectService } from '@ember/service';

export default Service.extend({
  session: injectService(),
  store: injectService(),
  products: null,
  currentProduct: null,

  load(productId) {
    if (this.get('session.isAuthenticated')) {
      if (this.get('products')) {
        this.setCurrentProduct(productId);
      } else {
        return this.get('store').findAll('dev-portal-product').then(function(products) {
          this.set('products', products);
          this.setCurrentProduct(productId);
        }.bind(this));
      }
    }
  },

  setCurrentProduct(productId) {
    if (productId) {
      let currentProduct = this.get('products').findBy('id', productId);
      this.set('currentProduct', currentProduct);
    }
  }
});
