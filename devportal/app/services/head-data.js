import headData from 'ember-cli-head/services/head-data';
import ENV from 'devportal-client/config/environment';

/*
 * Extends the headData service for Twitch-specific functionality
 */
export default headData.extend({
  googleSiteVerification: ENV.GOOGLE_SITE_VERIFICATION,
  googlePublisherTag: ENV.GOOGLE_PUBLISHER_TAG,

  /* Title Tag */
  title: '',
  setTitle(value, hasTwitchSuffix) {
    if (hasTwitchSuffix) {
      this.set('title', `${value} - Twitch Developers`);
    } else {
      this.set('title', value);
    }
  }
});
