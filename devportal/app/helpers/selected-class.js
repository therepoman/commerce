import { helper } from '@ember/component/helper';

export function selectedClass(params/*, hash*/) {
  const [index, selectedIndex] = params;
  return (index === selectedIndex) ? 'selected' : '';
}

export default helper(selectedClass);
