import Route from '@ember/routing/route';
import RSVP from 'rsvp';
import { inject as injectService } from '@ember/service';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';

export default Route.extend(ApplicationRouteMixin, {
  currentUser: injectService(),
  intl: injectService(),
  store: injectService(),

  beforeModel() {
    // define the app's runtime locale
    // For example, here you would maybe do an API lookup to resolver
    // which locale the user should be targeted and perhaps lazily
    // load translations using XHR and calling intl's `addTranslation`/`addTranslations`
    // method with the results of the XHR request
    return RSVP.all([
      this._loadCurrentUser(),
      this.get('intl').setLocale('en-us')
    ]);
    // OR for those that sideload, an array is accepted to handle fallback lookups

    // en-ca is the primary locale, en-us is the fallback.
    // this is optional, and likely unnecessary if you define baseLocale (see below)
    // The primary usecase is if you side load all translations
    //
    // return this.get('intl').setLocale(['en-ca', 'en-us']);
  },

  sessionAuthenticated() {
    this._super(...arguments);
    this._loadCurrentUser().catch(this._invalidateSession.bind(this));
  },

  buildPageTitle() {
    return {
      title:'Twitch Developers',
      hasTwitchTitleSuffix: false
    };
  },

  _invalidateSession() {
    this.get('session').invalidate();
  },

  _loadCurrentUser() {
    return this.get('currentUser').load();
  }
});
