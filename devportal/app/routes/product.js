import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Route.extend(AuthenticatedRouteMixin, {
  model(params) {
    let dppId = params.dev_portal_product_id;
    return this.get('store').findRecord('dev-portal-product', dppId);
  }
});
