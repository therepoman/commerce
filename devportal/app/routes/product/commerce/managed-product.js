import Route from '@ember/routing/route';
import { inject } from '@ember/service';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import ENV from 'devportal-client/config/environment';

export default Route.extend(AuthenticatedRouteMixin, {
  ajax: inject(),
  session: inject(),

  model(params) {
    let dppId = this.paramsFor('product').dev_portal_product_id;
    this.store.adapterFor('managed-product').set('namespace', `v5/commerce/product/${dppId}`);
    return this.store.findRecord('managed-product', params.managed_product_id);
  },

  redirect(model) {
    this.transitionTo('product.commerce.managed-product.distribution');
  },

  actions: {
    saveProduct(product, onSuccess, onError) {
      product.save().then((response) => onSuccess(response));
    },

    submitProduct(product, onSuccess, onError) {
      let _this = this;
      _this.get('session').authorize('authorizer:oauth2', function(headerName, headerValue) {
        product.save().then(function() {
          let url = _this.getSubmitUrl(product);
          let headers = {};
          headers[headerName] = headerValue;

          _this.get('ajax').post(url, { headers: headers })
            .then((response) => onSuccess(response))
            .catch((error) => onError(error));
        });
      });
    }
  },

  getSubmitUrl(product) {
    let dppId = this.paramsFor('product').dev_portal_product_id;
    let mpId = product.id;
    return `${ENV.host}/v5/commerce/product/${dppId}/managed-products/${mpId}/submit`;
  }
});
