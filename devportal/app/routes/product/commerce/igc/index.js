import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Route.extend(AuthenticatedRouteMixin, {
  model() {
    let dppId = this.paramsFor('product').dev_portal_product_id;
    this.store.adapterFor('in-game-content').set('namespace', `v5/commerce/product/${dppId}`);
    return this.store.query('in-game-content', { type: 'ADG_IGC' });
  },

  actions: {
    createInGameContent() {
      let _this = this;
      let store = _this.get('store');
      let dppId = _this.paramsFor('product').dev_portal_product_id;
      let parent = _this.modelFor('product.commerce.managed-product');

      let newInGameContent = store.createRecord('in-game-content', {
        devPortalProductId: dppId,
        type: 'ADG_IGC',
        adgVendorId: parent.get('adgVendorId'),
        adgDomainId: parent.get('adgDomainId')
      });

      newInGameContent.save().then(() => {
        _this.refresh();
      });
    }
  },

  buildPageTitle() {
    return {
      title: 'Commerce - In-Game Content'
    };
  }
});
