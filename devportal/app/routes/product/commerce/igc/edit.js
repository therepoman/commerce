import Route from '@ember/routing/route';
import { inject } from '@ember/service';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import ENV from 'devportal-client/config/environment';

export default Route.extend(AuthenticatedRouteMixin, {
  ajax: inject(),
  session: inject(),

  model(params) {
    let dppId = this.paramsFor('product').dev_portal_product_id;
    this.store.adapterFor('in-game-content').set('namespace', `v5/commerce/product/${dppId}`);
    return this.store.findRecord('in-game-content', params.igc_id);
  },

  actions: {
    saveInGameContentItem(inGameContentItem, onSuccess, onError) {
      inGameContentItem.save().then((response) => onSuccess(response));
    },

    submitInGameContentItem(inGameContentItem, onSuccess, onError) {
      let _this = this;

      _this.get('session').authorize('authorizer:oauth2', function(headerName, headerValue) {
        inGameContentItem.save().then(function() {
          let url = _this.getSubmitUrl(inGameContentItem);
          let headers = {};
          headers[headerName] = headerValue;

          _this.get('ajax').post(url, { headers: headers })
            .then((response) => onSuccess(response))
            .catch((error) => onError(error));
        });
      });
    }
  },

  getSubmitUrl(inGameContentItem) {
    let dppId = this.paramsFor('product').dev_portal_product_id;
    let mpId = inGameContentItem.id;
    return `${ENV.host}/v5/commerce/product/${dppId}/managed-products/${mpId}/submit`;
  },

  buildPageTitle() {
    return {
      title: 'Commerce - Edit In-Game Content'
    };
  }
});
