import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Route.extend(AuthenticatedRouteMixin, {
  model() {
    return this.modelFor('product.commerce.managed-product');
  },

  actions: {
    createListing(locale) {
      let managedProduct = this.model();
      let newListing = this.store.createRecord('listing', {
        locale: locale,
        managedProduct: managedProduct
      });
      managedProduct.get('localeListings').pushObject(newListing);
    },
    deleteListing(locale) {
      let managedProduct = this.model();
      let listingToDelete = managedProduct.get('localeListings').find(function(listing) {
        return listing.get('locale') === locale;
      });
      managedProduct.get('localeListings').removeObject(listingToDelete);
      listingToDelete.deleteRecord();
    }
  },

  buildPageTitle() {
    return {
      title: 'Commerce - Game Media'
    };
  }
});
