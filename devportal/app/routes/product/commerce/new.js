import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Route.extend(AuthenticatedRouteMixin, {

  beforeModel() {
    let dppId = this.paramsFor('product').dev_portal_product_id;
    this.store.adapterFor('managed-product').set('namespace', `v5/commerce/product/${dppId}`);
    return this.store.findAll('dev-portal-product');
  },

  actions: {
    createNewProduct(adgVendorId) {
      let store = this.get('store');
      let dppId = this.paramsFor('product').dev_portal_product_id;

      let newProduct = store.createRecord('managed-product', {
        devPortalProductId: dppId,
        type: 'ADG_GAME',
        adgVendorId: adgVendorId,
        minimumPcRequirements: store.createFragment('managed-product-pc-requirements'),
        recommendedPcRequirements: store.createFragment('managed-product-pc-requirements'),
        esrbRating: store.createFragment('managed-product-rating')
      });

      newProduct.save().then(function(managedProduct) {
        this.transitionTo('product.commerce.managed-product', managedProduct.get('devPortalProductId'), managedProduct.get('id'));
      }.bind(this));
    },

    willTransition() {
      this.container.lookup('route:product.commerce').refresh();
    }
  },

  buildPageTitle() {
    return {
      title: 'Commerce - New Product'
    };
  }
});
