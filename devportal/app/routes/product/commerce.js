import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Route.extend(AuthenticatedRouteMixin, {

  model() {
    let dppId = this.paramsFor('product').dev_portal_product_id;
    this.store.adapterFor('managed-product').set('namespace', `v5/commerce/product/${dppId}`);
    return this.store.query('managed-product', { type: 'ADG_GAME', reload: true });
  },

  redirect(managedProducts) {
    if (managedProducts.get('length') > 0) {
      let mpId = managedProducts.get('firstObject').get('id');
      this.transitionTo('product.commerce.managed-product', mpId);
    } else {
      this.transitionTo('product.commerce.new');
    }
  }
});
