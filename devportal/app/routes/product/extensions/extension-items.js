import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Route.extend(AuthenticatedRouteMixin, {
  model() {
    let dppId = this.paramsFor('product').dev_portal_product_id;
    this.store.adapterFor('extension-item').set('namespace', `v5/commerce/product/${dppId}`);
    return this.store.query('extension-item', { type: 'ADG_EXTENSION_IAP' });
  },

  actions: {
    createExtensionItem() {
      let _this = this;
      let store = _this.get('store');
      let dppId = _this.paramsFor('product').dev_portal_product_id;
      let parentExtension = _this.modelFor('product.extensions').get('firstObject');

      let newExtensionItem = store.createRecord('extension-item', {
        devPortalProductId: dppId,
        type: 'ADG_EXTENSION_IAP',
        adgVendorId: parentExtension.get('adgVendorId'),
        adgDomainId: parentExtension.get('adgDomainId')
      });

      newExtensionItem.save().then(() => {
        _this.refresh();
      });
    }
  }
});
