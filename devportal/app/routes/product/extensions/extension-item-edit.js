import Route from '@ember/routing/route';
import { inject } from '@ember/service';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import ENV from 'devportal-client/config/environment';

export default Route.extend(AuthenticatedRouteMixin, {
  ajax: inject(),
  session: inject(),

  model(params) {
    let dppId = this.paramsFor('product').dev_portal_product_id;
    this.store.adapterFor('extension-item').set('namespace', `v5/commerce/product/${dppId}`);
    return this.store.findRecord('extension-item', params.extension_item_id);
  },

  actions: {
    saveExtensionItem(extensionItem, onSuccess, onError) {
      extensionItem.save().then((response) => onSuccess(response));
    },

    submitExtensionItem(extensionItem, onSuccess, onError) {
      let _this = this;

      _this.get('session').authorize('authorizer:oauth2', function(headerName, headerValue) {
        extensionItem.save().then(function() {
          let url = _this.getSubmitUrl(extensionItem);
          let headers = {};
          headers[headerName] = headerValue;

          _this.get('ajax').post(url, { headers: headers })
            .then((response) => onSuccess(response))
            .catch((error) => onError(error));
        });
      });
    }
  },

  getSubmitUrl(extensionItem) {
    let dppId = this.paramsFor('product').dev_portal_product_id;
    let mpId = extensionItem.id;
    return `${ENV.host}/v5/commerce/product/${dppId}/managed-products/${mpId}/submit`;
  }
});
