import Route from '@ember/routing/route';
import { inject } from '@ember/service';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import ENV from 'devportal-client/config/environment';

export default Route.extend(AuthenticatedRouteMixin, {
  ajax: inject(),
  session: inject(),

  model(params) {
    let dppId = this.paramsFor('product').dev_portal_product_id;
    this.store.adapterFor('extension').set('namespace', `v5/commerce/product/${dppId}`);
    return this.store.findRecord('extension', params.extension_id);
  },

  actions: {
    saveExtension(extension, onSuccess, onError) {
      extension.save().then((response) => onSuccess(response));
    },

    submitExtension(extension, onSuccess, onError) {
      let _this = this;

      _this.get('session').authorize('authorizer:oauth2', function(headerName, headerValue) {
        extension.save().then(function() {
          let url = _this.getSubmitUrl(extension);
          let headers = {};
          headers[headerName] = headerValue;

          _this.get('ajax').post(url, { headers: headers })
            .then((response) => onSuccess(response))
            .catch((error) => onError(error));
        });
      });
    }
  },

  getSubmitUrl(extension) {
    let dppId = this.paramsFor('product').dev_portal_product_id;
    let mpId = extension.id;
    return `${ENV.host}/v5/commerce/product/${dppId}/managed-products/${mpId}/submit`;
  }
});
