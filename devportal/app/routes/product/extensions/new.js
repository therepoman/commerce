import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Route.extend(AuthenticatedRouteMixin, {

  beforeModel() {
    let dppId = this.paramsFor('product').dev_portal_product_id;
    this.store.adapterFor('extension').set('namespace', `v5/commerce/product/${dppId}`);
  },

  actions: {
    createNewProduct(adgVendorId) {
      let store = this.get('store');
      let dppId = this.paramsFor('product').dev_portal_product_id;

      let newExtension = store.createRecord('extension', {
        devPortalProductId: dppId,
        type: 'ADG_EXTENSION',
        adgVendorId: adgVendorId
      });

      newExtension.save().then(function(extension) {
        this.transitionTo('product.extensions.extension', extension.get('devPortalProductId'), extension.get('id'));
      }.bind(this));
    },

    willTransition() {
      this.container.lookup('route:product.extensions').refresh();
    }
  },

  buildPageTitle() {
    return {
      title: 'Commerce - New Product'
    };
  }
});
