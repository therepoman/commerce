import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Route.extend(AuthenticatedRouteMixin, {
  model() {
    let dppId = this.paramsFor('product').dev_portal_product_id;
    this.store.adapterFor('extension').set('namespace', `v5/commerce/product/${dppId}`);
    return this.store.query('extension', { type: 'ADG_EXTENSION' });
  },

  redirect(extensions) {
    if (extensions.get('length') > 0) {
      let id = extensions.get('firstObject').get('id');
      this.transitionTo('product.extensions.extension', id);
    } else {
      this.transitionTo('product.extensions.new');
    }
  }
});
