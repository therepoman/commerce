import DS from 'ember-data';
import moment from 'moment';

export default DS.DateTransform.extend({
  serialize(date) {
    if (date instanceof Date) {
      return moment(date).utc().format();
    } else {
      return null;
    }
  }
});
