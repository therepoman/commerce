import AdaptiveStore from 'ember-simple-auth/session-stores/adaptive';

export default AdaptiveStore.extend({
  cookieName: 'twitch-devportal-token',
  localStorageKey: 'twitch-devportal-session'
});

