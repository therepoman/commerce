import JSONSerializer from 'ember-data/serializers/json';
import { underscore } from '@ember/string';

export default JSONSerializer.extend({
  primaryKey: '_id',
  keyForAttribute(attr) {
    return underscore(attr);
  }
});
