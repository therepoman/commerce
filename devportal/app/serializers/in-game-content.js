import ManagedProductSerializer from './managed-product';

export default ManagedProductSerializer.extend({
  payloadKeyFromModelName() {
    return 'managedProduct';
  }
});
