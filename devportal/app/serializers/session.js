import JSONSerializer from 'ember-data/serializers/json';

/**
 * Model representing the current active session. It can ONLY be retrieved for the current session.
 */
export default JSONSerializer.extend({
  attrs: {
    accountVerified: 'account_verified',
    chatOauthToken: 'chat_oauth_token',
    csrfToken: 'csrf_token',
    hasPremium: 'has_premium',
    hasTurbo: 'has_turbo',
    isAdmin: 'is_admin',
    isBroadcaster: 'is_broadcaster',
    isPartner: 'is_partner',
    isStaff: 'is_staff',
    lastBroadcastTime: 'last_broadcast_time',
    twitterConnected: 'twitter_connected'
  }
});
