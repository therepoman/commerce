import RESTSerializer from 'ember-data/serializers/rest';
import Inflector from 'ember-inflector';

export default RESTSerializer.extend({

  // Custom json root. The API returns objects in the "data" key.
  // We need to re-assign it to the singular version of the model name.
  // So {data: {name: foo}} becomes {post: {name: foo}}
  normalizeSingleResponse: function(store, primaryModelClass, payload, id, requestType) {
    let typeKey = primaryModelClass.modelName;
    payload[typeKey] = payload['managedProduct'];
    delete payload['managedProduct'];

    return this._normalizeResponse(store, primaryModelClass, payload, id, requestType, true);
  },
  // Custom json root. The API returns objects in the "data" key.
  // We need to re-assign it to the plural version of the model name.
  // So {data: [{post1}, {post2}]} becomes {posts: [{post1},{post2}]}
  normalizeArrayResponse: function(store, primaryModelClass, payload, id, requestType) {
    let pluralTypeKey = Inflector.inflector.pluralize(primaryModelClass.modelName);
    payload[pluralTypeKey] = payload['managedProducts'];
    delete payload['managedProducts'];

    return this._normalizeResponse(store, primaryModelClass, payload, id, requestType, false);
  },

  normalize: function(model, json) {
    this._normalizePcRequirementsInfo(json);
    this._normalizeContentRatingInfo(json);
    this._normalizeArrays(json);

    return this._super(...arguments);
  },

  serialize: function (snapshot, options) {
    let json = this._super(snapshot, options);

    this._serializePcRequirementsInfo(json);
    this._serializeContentRatingInfo(json);

    return json;
  },

  _normalizePcRequirementsInfo: function(json) {
    let pcRequirementsInfo = json.pcRequirementsInfo;
    delete json.pcRequirementsInfo;

    if (pcRequirementsInfo && typeof pcRequirementsInfo === 'object') {
      json.minimumPcRequirements = pcRequirementsInfo.minimumRequirements;
      json.recommendedPcRequirements = pcRequirementsInfo.recommendedRequirements;
      json.otherPcRequirements = pcRequirementsInfo.otherRequirements;
    }
  },

  _serializePcRequirementsInfo: function(json) {
    json.pcRequirementsInfo = {
      minimumRequirements: json.minimumPcRequirements,
      recommendedRequirements: json.recommendedPcRequirements,
      otherRequirements: json.otherPcRequirements
    };

    delete json.minimumPcRequirements;
    delete json.recommendedPcRequirements;
    delete json.otherPcRequirements;
  },

  _normalizeContentRatingInfo: function(json) {
    let contentRatingInfo = json.contentRatingInfo;
    delete json.contentRatingInfo;

    if (contentRatingInfo && typeof contentRatingInfo === 'object') {
      json.esrbRating = contentRatingInfo.esrbRatingInfo;
      json.pegiRating = contentRatingInfo.pegiRating;
      json.uskRating = contentRatingInfo.uskRating;
      json.amazonRating = contentRatingInfo.amazonRating;
    }
  },

  _serializeContentRatingInfo: function(json) {
    json.contentRatingInfo = {
      esrbRatingInfo: json.esrbRating,
      pegiRating: json.pegiRating,
      uskRating: json.uskRating,
      amazonRating: json.amazonRating
    };

    delete json.esrbRating;
    delete json.pegiRating;
    delete json.uskRating;
    delete json.amazonRating;
  },

  _normalizeArrays: function(json) {
    if (!json.localeListings) {
      json.localeListings = [];
    }

    if (!json.availabilityInfo) {
      json.availabilityInfo = [];
    }

    if (!json.languageSupport) {
      json.languageSupport = [];
    }

    if (!json.broadcasterVodUrls) {
      json.broadcasterVodUrls = [];
    }

    if (!json.newsAndReviewsUrls) {
      json.newsAndReviewsUrls = [];
    }
  }
});
