# Commerce Devportal

`Commerce Devportal` is the [devportal2.twitch.tv](https://devportal2.twitch.tv/) frontend, written in
[Ember.js](http://emberjs.com/).

**Visit `#fuel-devportal` on Slack if you have any questions, concerns, or need assistance.**

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
  * `brew update`
  * `brew install git`
* [Node.js](https://nodejs.org/)
  * `brew tap homebrew/versions`
  * `brew install homebrew/versions/node6-lts`
* [Yarn](https://yarnpkg.com)
  * `brew update`
  * `brew install yarn`
* `yarn global add bower`
* `yarn global add ember-cli`
* `yarn global add phantomjs-prebuilt`
* It is highly recommended that you install the [Ember Inspector](https://guides.emberjs.com/v2.9.0/ember-inspector/installation/).
* Developers should also follow the [Dev Environment Setup Docs](https://git-aws.internal.justin.tv/twitch/docs/blob/master/web/dev_environment_setup.md).

## Installation

* `git clone git@git-aws.internal.justin.tv:commerce/devportal.git` this repository
* `yarn install` -- Installs NPM and bower packages
* `yarn setup-local-osx` -- Makes changes to host file and installs SSL (runs the 'local' steps below)
* `ember s` -- builds the app and starts livereload server
* Visit [https://devportal-local.twitch.tv:4200](https://devportal-local.twitch.tv:4200)

### Local Hostname Setup (*nix Only)
  1. `yarn setup-local-osx:hosts` to add a host definition in /etc/hosts
  2. Provide your system password to edit the file.

### Local SSL Setup (OSX Only)

  1. `yarn setup-local-osx:ssl` to generate self-signed SSL certificate
  2. When prompted provide your system password to install and trust the cert.
  To run in non-SSL mode use:

  ```
  ember server --no-ssl --proxy="http://www.twitch.tv"
  ```

## Running / Development

* `ember serve`
* Visit your app at [http://localhost:4200](http://localhost:4200).
* Note: Need to add your tuid to the whitelist at https://git-aws.internal.justin.tv/gds/gds/blob/master/portal/dpp/dppconfig/data/prod.json to be able to see the devportal page.

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Images
#### SVGs

We use svg-jar to inline svgs.  They're stored in /public/img, and can be accessed with {{svg-jar 'img-name'}}

When local server is running, you can view the images [here](http://localhost:4200/ember-svg-jar/index.html)

### Updating packages

We are using `yarn` to manage our projects dependencies (see https://yarnpkg.com/ for info on the benefits over `npm`). This means there are a
few differences to updating dependencies that you may need to know.

* Updating a specific dependency: `yarn upgrade <package-name-here>`. This will update both `yarn.lock` and `package.json` to the newest version.
* Updating deps to newer versions: `yarn upgrade-interactive`.  This will provide a nice set of prompts for each outdated package in the project,
  and make updating both `package.json` and `yarn.lock` much easier.

### Helpful Scripts

* `yarn run clean` clears your bower cache as well as removing package directories and compiled/disted files.
This is most useful when changes were made to the app's `package.json` or `bower.json` and you'd like to install the new package versions.

### Running Tests

* `yarn lint` (linting only)
* `yarn test`
* `yarn test --server`

### Building

* `ember build` (development)
* `ember build --environment production` (production)

### Deploying

Specify what it takes to deploy your app.

### AWS Resources

* S3 Bucket configured for "Static website hosting"
* IAM Policy to read/write to S3 Bucket

```
{
    "Statement": [
        {
            "Sid": "Stmt1EmberCLIS3DeployPolicy",
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:PutObjectACL",
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::your-bucket-name",
                "arn:aws:s3:::your-bucket-name/*"
            ]
        }
    ]
}
```

* IAM User with IAM Policy attached

## Further Reading / Useful Links

* [ember.js](http://emberjs.com/)
* [ember-cli](https://ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)
