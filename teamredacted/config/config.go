package config

import (
	"fmt"
	"io/ioutil"
	"os"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/teamredacted/config/cloudwatch"
	"code.justin.tv/commerce/teamredacted/config/environment"
	"code.justin.tv/commerce/teamredacted/config/redis"
	"github.com/go-yaml/yaml"
)

const (
	LocalConfigFilePath            = "/src/code.justin.tv/commerce/teamredacted/config/data/%s.yaml"
	GlobalConfigFilePath           = "/etc/teamredacted/config/%s.yaml"
	LambdaConfigFilePath           = "etc/teamredacted/config/%s.yaml"
	EnvironmentEnvironmentVariable = "ENVIRONMENT"

	UnitTest  = Environment("unit-test")
	SmokeTest = Environment("smoke-test")
	Local     = Environment("local")
	Staging   = Environment("staging")
	Prod      = Environment("prod")
	Default   = Local
)

type Config struct {
	// Local variable to store env type we have
	environment Environment

	// The configuration of the environment, like name / region
	Environment *environment.Config `yaml:"environment"`

	// Configuration for cloudwatch metrics.
	CloudwatchMetrics *cloudwatch.Config `yaml:"cloudwatch"`

	// Configuration for setting up redis.
	Redis *redis.Config `yaml:"redis"`

	EventBusConfig *SQSConfig `yaml:"event-bus-queue"`

	PrismEndpoint string `yaml:"prism-endpoint"`
}

type SQSConfig struct {
	Disabled   bool   `yaml:"disabled"`
	Name       string `yaml:"name"`
	Region     string `yaml:"region"`
	URL        string `name:"url"`
	NumWorkers int    `yaml:"num-workers"`
}

type Environment string

var Environments = map[Environment]interface{}{UnitTest: nil, SmokeTest: nil, Local: nil, Staging: nil, Prod: nil}

func GetOrDefaultEnvironment(env string) Environment {
	if !isValidEnvironment(Environment(env)) {
		logrus.Errorf("Invalid environment: %s", env)
		logrus.Infof("Falling back to default environment: %s", string(Default))
		return Default
	}

	return Environment(env)
}

func (c *Config) GetEnvironment() Environment {
	return c.environment
}

func isValidEnvironment(env Environment) bool {
	_, ok := Environments[env]
	return ok
}

func LoadConfig(env Environment) (*Config, error) {
	if !isValidEnvironment(env) {
		logrus.Errorf("Invalid environment: %s. Falling back to local", env)
		env = Local
	}

	baseFileName := string(env)
	filePath, err := getConfigFilePath(baseFileName)
	if err != nil {
		return nil, err
	}

	cfg, err := loadConfig(filePath)
	if err != nil {
		return nil, err
	}

	cfg.environment = env

	return cfg, err
}

func loadConfig(path string) (*Config, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer func() {
		err := file.Close()
		if err != nil {
			logrus.WithError(err).Error("Error closing config file")
		}
	}()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var cfg Config
	err = yaml.Unmarshal(fileBytes, &cfg)
	if err != nil {
		return nil, err
	}
	return &cfg, nil
}

func getConfigFilePath(baseFilename string) (string, error) {
	localFname := os.Getenv("GOPATH") + fmt.Sprintf(LocalConfigFilePath, baseFilename)
	if _, err := os.Stat(localFname); !os.IsNotExist(err) {
		return localFname, nil
	}
	lambdaFname := fmt.Sprintf(LambdaConfigFilePath, baseFilename)
	if _, err := os.Stat(lambdaFname); !os.IsNotExist(err) {
		return lambdaFname, nil
	}
	globalFname := fmt.Sprintf(GlobalConfigFilePath, baseFilename)
	if _, err := os.Stat(globalFname); os.IsNotExist(err) {
		return "", err
	}
	return globalFname, nil
}
