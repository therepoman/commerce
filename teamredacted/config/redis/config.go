package redis

type Config struct {
	Endpoint        string `yaml:"endpoint"`
	ClusterEndpoint string `yaml:"cluster-endpoint"`
}
