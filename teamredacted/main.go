package main

import (
	"math/rand"
	"os"
	"time"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/splatter"
	"code.justin.tv/commerce/teamredacted/backend"
	"code.justin.tv/commerce/teamredacted/config"
	teamredacted "code.justin.tv/commerce/teamredacted/twirp"
	"github.com/codegangsta/cli"
	"github.com/twitchtv/twirp"
	goji_graceful "github.com/zenazn/goji/graceful"
	"goji.io"
	"goji.io/pat"
)

var environment string

func main() {
	app := cli.NewApp()
	app.Name = "TeamRedacted"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'prod', 'staging', 'local'. Defaults to 'staging' if unset.",
			Destination: &environment,
			EnvVar:      config.EnvironmentEnvironmentVariable,
		},
	}

	app.Action = runTeamRedactedService

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func runTeamRedactedService(c *cli.Context) {
	env := config.GetOrDefaultEnvironment(environment)

	rand.Seed(time.Now().UnixNano())

	cfg, err := config.LoadConfig(env)
	if err != nil || cfg == nil {
		logrus.WithError(err).Panic("Error loading config")
	}

	logrus.Infof("Loaded config: %s", cfg.Environment.Name)

	be, err := backend.NewBackend(cfg)
	if err != nil {
		logrus.WithError(err).Panic("Error initializing backend")
	}

	twirpStatsHook := twirp.ChainHooks(splatter.NewStatsdServerHooks(be.Statter()))
	twirpHandler := teamredacted.NewTeamRedactedServer(be.TwirpServer(), twirpStatsHook)
	defer func() {
		if err != nil {
			logrus.WithError(err).Error("could not close statter")
		}
	}()

	mux := goji.NewMux()

	mux.HandleFunc(pat.Get("/ping"), be.Ping)
	mux.Handle(pat.Post(teamredacted.TeamRedactedPathPrefix+"*"), twirpHandler)

	goji_graceful.HandleSignals()
	logrus.Info("service starting on 8000")
	err = goji_graceful.ListenAndServe(":8000", mux)
	if err != nil {
		logrus.WithError(err).Error("Mux listen and serve error")
	}

	logrus.Info("Initiated shutdown process")
}
