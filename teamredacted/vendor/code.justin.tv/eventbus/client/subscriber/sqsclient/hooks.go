package sqsclient

import (
	"log"

	"code.justin.tv/eventbus/client/internal/sqspoller"
)

// HookEvent encapsulates both informational and error messages coming out of the SQS poller.
// It's provided for logging of lifecycle events during the running of the poller.
type HookEvent interface {
	// All HookEvents try to convert themselves to string in a logger-friendly way.
	String() string

	// Error will return the error value if this message represents an error.
	// If this message is informational, Error() returns nil.
	Error() error
}

type HookEventPollError = sqspoller.HookEventPollError
type HookEventAckError = sqspoller.HookEventAckError
type HookEventAckPartialFail = sqspoller.HookEventAckPartialFail
type HookEventBatchDelivered = sqspoller.HookEventBatchDelivered

func wrapCallback(forward func(HookEvent)) sqspoller.HookCallback {
	return func(event sqspoller.HookEvent) {
		forward(event)
	}
}

// DefaultHookCallback prints out error messages to the stdlib logger and
// does nothing with informational messages.
var DefaultHookCallback = func(event HookEvent) {
	if err := event.Error(); err != nil {
		log.Printf("[SQS poller] error: %s", event.String())
	}
}
