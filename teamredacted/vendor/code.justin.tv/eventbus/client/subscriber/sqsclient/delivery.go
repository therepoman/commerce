package sqsclient

import (
	"encoding/json"

	"code.justin.tv/eventbus/client/internal/snsmarshal"
	"github.com/aws/aws-sdk-go/service/sqs"
)

func (c *SQSClient) deliverLoop() {
	shutdown := c.ctxRequests.Done()

	for {
		select {
		case <-shutdown:
			return
		case message, ok := <-c.deliver:
			if !ok {
				return
			}
			err := c.doDeliver(message)
			if err == nil {
				c.poller.Ack(*message.ReceiptHandle)
			}
		}
	}
}

func (c *SQSClient) doDeliver(message *sqs.Message) error {
	var target snsMessage
	err := json.Unmarshal([]byte(*message.Body), &target)
	if err != nil {
		return err
	}

	buf, err := snsmarshal.DecodeString(target.Message)
	if err != nil {
		return err
	}

	return c.dispatcher.Dispatch(c.ctxRequests, buf)
}

type snsMessage struct {
	Message string `json:"Message"`
}
