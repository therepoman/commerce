package wire

import "time"

// EncodeNoop gives a no-op payload.
func EncodeNoop(when time.Time) ([]byte, error) {
	header, err := defaultHeader("", when)
	if err != nil {
		return nil, err
	}

	header.Type = MessageType_NOOP

	return buildMessage(header, nil)
}

func IsNoop(h *HeaderV1) bool {
	return h.EventType == "" && h.Type == MessageType_NOOP
}
