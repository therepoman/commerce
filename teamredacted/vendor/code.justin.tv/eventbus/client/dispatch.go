package eventbus

import (
	"context"

	"code.justin.tv/eventbus/client/internal/wire"
)

// Dispatcher parses an event payload and routes it to the correct handlers.
type Dispatcher interface {
	// Dispatch an incoming message to be routed to a handler.
	// This primarily is used by transport implementations but can also be used for testing.
	Dispatch(ctx context.Context, msg []byte) error
}

// A Handler processes one message coming from the event bus.
//
// Handlers are expected to do whatever work a subscriber does and return success/failure.
// Returning nil error means we have successfully done our work and this message can be
// marked delivered (depending on transport). Returning an error is communicated to
// the underlying transport and the result is usually re-delivery (though maybe not to
// the same machine/process).
//
// Some transports may use the Context to communicate deadlines and cancellation.
type Handler func(ctx context.Context, message RawMessage) error

type dispatcher struct {
	handlers map[string]Handler
}

func (d *dispatcher) Dispatch(ctx context.Context, msg []byte) error {
	h, payload, err := wire.HeaderAndPayload(msg)
	if err != nil {
		return &decodeError{err}
	}

	if wire.IsNoop(h) {
		return nil
	}

	header, err := publicHeader(h)
	if err != nil {
		return err
	}

	handler, ok := d.handlers[header.EventType]
	if !ok {
		return &handlerNotFound{header.EventType}
	}

	message := RawMessage{
		Header:  header,
		Payload: payload,
	}
	return handler(ctx, message)
}

type Mux struct {
	handlers map[string]Handler
}

func NewMux() *Mux {
	return &Mux{
		handlers: make(map[string]Handler),
	}
}

// Register a handler to route a specific message type to a handler function.
// If a handler already exists for a message type, RegisterHandler panics.
func (m *Mux) RegisterHandler(msgType string, f Handler) {
	if m.handlers[msgType] != nil {
		panic("Cannot register two handlers for " + msgType)
	}
	m.handlers[msgType] = f
}

// Dispatcher creates a dispatcher from this mux.
func (m *Mux) Dispatcher() Dispatcher {
	clonedHandlers := make(map[string]Handler, len(m.handlers))
	for k, v := range m.handlers {
		clonedHandlers[k] = v
	}
	return &dispatcher{
		handlers: clonedHandlers,
	}
}
