package eventbus

import (
	"fmt"
)

// HandlerNotFound is returned from Dispatch when an event arrives for which
// there was no handler registered.
type HandlerNotFound interface {
	error
	HandlerNotFound() string
}

type handlerNotFound struct {
	msgType string
}

func (h handlerNotFound) Error() string {
	return fmt.Sprintf("Dispatch: could not find handler for %s", h.msgType)
}

func (h handlerNotFound) HandlerNotFound() string {
	return h.msgType
}

// DecodeError encapsulates any errors that happen during decoding the message bytes.
type DecodeError interface {
	error
	DecodeError()
	Cause() error
}

type decodeError struct {
	orig error
}

func (d decodeError) Error() string {
	return "Decode Error: " + d.orig.Error()
}

func (d decodeError) DecodeError() {}

func (d decodeError) Cause() error {
	return d.orig
}
