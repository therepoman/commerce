package eventbus

import (
	"time"

	uuid "github.com/gofrs/uuid"
	"github.com/golang/protobuf/ptypes"
	"github.com/pkg/errors"

	"code.justin.tv/eventbus/client/internal/wire"
)

// RawMessage is a partially decoded message coming from a subscriber implementation.
type RawMessage struct {
	Header  *Header
	Payload []byte
}

// Header describes some aspects of the message
type Header struct {
	// A v4 UUID that is assigned by the publisher.
	// This UUID can be used to implement your own deduplication logic
	MessageID uuid.UUID

	// The event type encapsulated in this message, as defined in the schema library.
	EventType string

	// When this message was created by the publisher library.
	CreatedAt time.Time
}

func publicHeader(input *wire.HeaderV1) (*Header, error) {
	created, err := ptypes.Timestamp(input.CreatedAt)
	if err != nil {
		return nil, errors.Wrap(err, "Could not decode timestamp portion of header")
	}
	msgID, err := uuid.FromBytes(input.MessageId)
	if err != nil {
		return nil, errors.Wrap(err, "Could not decode UUID from header")
	}
	h := &Header{
		MessageID: msgID,
		EventType: input.EventType,
		CreatedAt: created,
	}
	return h, nil
}
