// NOTE this is a generated file! do not edit!

package cheer

import (
	"context"

	"github.com/golang/protobuf/proto"

	eventbus "code.justin.tv/eventbus/client"
)

const (
	CreateEventType = "CheerCreate"
)

type Create = CheerCreate

type CheerCreateHandler func(context.Context, *eventbus.Header, *CheerCreate) error

func (h CheerCreateHandler) Handler() eventbus.Handler {
	return func(ctx context.Context, message eventbus.RawMessage) error {
		dst := &CheerCreate{}
		err := proto.Unmarshal(message.Payload, dst)
		if err != nil {
			return err
		}
		return h(ctx, message.Header, dst)
	}
}

func RegisterCheerCreateHandler(mux *eventbus.Mux, f CheerCreateHandler) {
	mux.RegisterHandler(CreateEventType, f.Handler())
}

func RegisterCreateHandler(mux *eventbus.Mux, f CheerCreateHandler) {
	RegisterCheerCreateHandler(mux, f)
}

func (*CheerCreate) EventBusName() string {
	return CreateEventType
}
