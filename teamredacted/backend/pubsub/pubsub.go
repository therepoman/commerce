package pubsub

import (
	"context"
	"encoding/json"
	"fmt"

	"code.justin.tv/commerce/teamredacted/backend/dynamo"

	prism "code.justin.tv/commerce/prism/rpc"
	"code.justin.tv/commerce/teamredacted/backend/dynamo/games"
	"code.justin.tv/commerce/teamredacted/backend/game_queue"
	"github.com/sirupsen/logrus"
)

type Publisher interface {
	Publish(ctx context.Context, gameID string)
}

type publisher struct {
	PrismClient      prism.Prism        `inject:""`
	GameQueueManager game_queue.Manager `inject:""`
	GamesDAO         games.DAO          `inject:""`
}

func NewPublisher() Publisher {
	return &publisher{}
}

type Model struct {
	Type       string             `json:"type"`
	GameID     string             `json:"game_id"`
	UserID     string             `json:"user_id"`
	OpponentID string             `json:"opponent_id"`
	GameQueue  []game_queue.Entry `json:"game_queue"`
	GameState  dynamo.GameState   `json:"game_state"`
}

func (p *publisher) Publish(ctx context.Context, gameID string) {
	gameQueue, err := p.GameQueueManager.GetQueue(gameID)
	if err != nil {
		logrus.WithError(err).Error("error getting queue")
		return
	}

	game, err := p.GamesDAO.GetGameByID(ctx, gameID)
	if err != nil {
		logrus.WithError(err).Error("error getting game from dynamo")
		return
	}

	if game == nil {
		logrus.Error("game not found in dynamo")
		return
	}

	model := Model{
		Type:       "teamredacted-gameupdate",
		GameID:     gameID,
		UserID:     game.UserID,
		OpponentID: game.OpponentID,
		GameQueue:  gameQueue,
		GameState:  game.State,
	}

	modelBytes, err := json.Marshal(&model)
	if err != nil {
		logrus.WithError(err).Error("error marshaling model to json")
		return
	}

	_, err = p.PrismClient.PublishPubsubMessage(ctx, &prism.PublishPubsubMessageReq{
		Topic:   fmt.Sprintf("channel-bit-events-public.%s", game.UserID),
		Payload: string(modelBytes),
	})

	if err != nil {
		logrus.WithError(err).Error("error publishing to pubsub")
		return
	}
}
