package games

import (
	"context"
	"fmt"

	db "code.justin.tv/commerce/teamredacted/backend/dynamo"
	"code.justin.tv/commerce/teamredacted/config"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/guregu/dynamo"
)

const (
	DBFormat          = "games-%s"
	UserIDIndexFormat = "games-by-user-id-%s"

	HashKey      = "game_id"
	IndexHashKey = "user_id"
)

type DAO interface {
	CreateGame(ctx context.Context, game *db.Game) error
	UpdateGame(ctx context.Context, game *db.Game) error
	GetActiveGameForUserID(ctx context.Context, userID string) (*db.Game, error)
	GetGameByID(ctx context.Context, gameID string) (*db.Game, error)
}

type dao struct {
	Session *dynamo.DB
	Config  *config.Config `inject:""`
}

func NewDAO(sess *session.Session, cfg *config.Config) DAO {
	return &dao{
		Session: dynamo.New(sess),
		Config:  cfg,
	}
}

func NewDAOWithClient(dynamoClient dynamodbiface.DynamoDBAPI) DAO {
	return &dao{
		Session: dynamo.NewFromIface(dynamoClient),
	}
}

func (d *dao) CreateGame(ctx context.Context, game *db.Game) error {
	mostRecentGame, err := d.GetActiveGameForUserID(ctx, game.UserID)
	if err != nil {
		return err
	}

	table := d.Session.Table(fmt.Sprintf(DBFormat, d.Config.Environment.DynamoSuffix))

	tx := d.Session.WriteTx()
	tx.Put(table.Put(game).If("attribute_not_exists('game_id')"))

	if mostRecentGame != nil && !mostRecentGame.State.Complete {
		mostRecentGame.State.Complete = true
		tx.Put(table.Put(mostRecentGame))
	}

	err = tx.RunWithContext(ctx)
	if err != nil {
		return err
	}

	return nil
}

func (d *dao) GetGameByID(ctx context.Context, gameID string) (*db.Game, error) {
	table := d.Session.Table(fmt.Sprintf(DBFormat, d.Config.Environment.DynamoSuffix))

	var game db.Game
	err := table.Get(HashKey, gameID).
		Consistent(true).
		OneWithContext(ctx, &game)

	if err == dynamo.ErrNotFound {
		return nil, nil
	}

	return &game, err
}

func (d *dao) GetActiveGameForUserID(ctx context.Context, userID string) (*db.Game, error) {
	table := d.Session.Table(fmt.Sprintf(DBFormat, d.Config.Environment.DynamoSuffix))

	var game db.Game
	err := table.Get(IndexHashKey, userID).
		Index(fmt.Sprintf(UserIDIndexFormat, d.Config.Environment.DynamoSuffix)).
		Filter("$.$ <> ?", "state", "complete", true).
		OneWithContext(ctx, &game)

	if err == dynamo.ErrNotFound {
		return nil, nil
	}

	return &game, err
}

func (d *dao) GetGame(ctx context.Context, gameID string) (*db.Game, error) {
	table := d.Session.Table(fmt.Sprintf(DBFormat, d.Config.Environment.DynamoSuffix))

	var game db.Game
	err := table.Get(HashKey, gameID).OneWithContext(ctx, &game)
	if err == dynamo.ErrNotFound {
		return nil, nil
	}

	return &game, err
}

func (d *dao) UpdateGame(ctx context.Context, game *db.Game) error {
	table := d.Session.Table(fmt.Sprintf(DBFormat, d.Config.Environment.DynamoSuffix))
	return table.Put(game).If("attribute_exists('game_id')").Run()
}
