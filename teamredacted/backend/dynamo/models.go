package dynamo

import (
	"encoding/json"
)

type SpaceState string

const (
	Broadcaster = SpaceState("b")
	Viewer      = SpaceState("v")
	Empty       = SpaceState("")

	Columns = 7
	Rows    = 6
)

type Game struct {
	ID         string    `dynamo:"game_id" json:"id"`
	UserID     string    `dynamo:"user_id" json:"user_id"`
	OpponentID string    `dynamo:"opponent_id" json:"opponent_id"`
	State      GameState `dynamo:"state" json:"state"`
}

type GameState struct {
	Winner   string `dynamo:"winner" json:"winner"`
	Complete bool   `dynamo:"complete" json:"complete"`
	Board    string `dynamo:"board" json:"board"`
}

func ConstructNewBoard() string {
	board := make([][]SpaceState, Columns)
	for column := range board {
		board[column] = make([]SpaceState, Rows)
	}

	boardJson, err := json.Marshal(board)
	if err != nil {
		return ""
	}

	return string(boardJson)
}

func UnmarshalBoard(boardJson string) ([][]string, error) {
	board := make([][]string, Columns)
	for column := range board {
		board[column] = make([]string, Rows)
	}
	err := json.Unmarshal([]byte(boardJson), &board)
	if err != nil {
		return nil, err
	}
	return board, nil
}
