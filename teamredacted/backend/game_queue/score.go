package game_queue

// Note: this file has been shamelessly copied from pantheon:
// https://git-aws.internal.justin.tv/commerce/pantheon/blob/master/backend/leaderboard/score/score.go

import (
	"math"
	"math/big"
	"time"

	mathUtils "code.justin.tv/commerce/gogogadget/math"
)

type EventTime uint64

type ModeratedFlag bool

type BaseScore int64

type TimeFraction float64

type Score float64

type EntryScore struct {
	ModeratedFlag ModeratedFlag
	BaseScore     BaseScore
	EventTime     EventTime
}

const (
	maximumTimeFraction = 0.9999999
	Moderated           = ModeratedFlag(true)
	NotModerated        = ModeratedFlag(false)
)

var MinimumEventTime = time.Date(2006, time.January, 1, 0, 0, 0, 0, time.UTC).UnixNano()
var MaximumEventTime = time.Date(2043, time.January, 1, 0, 0, 0, 0, time.UTC).UnixNano()

func EntryScoreEquals(es1 EntryScore, es2 EntryScore) bool {
	return es1.BaseScore == es2.BaseScore &&
		es1.EventTime == es2.EventTime &&
		es1.ModeratedFlag == es2.ModeratedFlag
}

func ToEntryScore(bs BaseScore, et EventTime, mf ModeratedFlag) EntryScore {
	return EntryScore{
		BaseScore:     bs,
		EventTime:     et,
		ModeratedFlag: mf,
	}
}

func (es EntryScore) EncodeScore() Score {
	return encodeScore(es.BaseScore, es.EventTime, es.ModeratedFlag)
}

func encodeScore(bs BaseScore, et EventTime, mf ModeratedFlag) Score {
	tf := eventTimeToTimeFraction(et)
	return combineScoreComponents(bs, tf, mf)
}

func getModerationMultiplier(mf ModeratedFlag) float64 {
	if mf == Moderated {
		return float64(-1.0)
	}
	return float64(1.0)
}

func combineScoreComponents(bs BaseScore, tf TimeFraction, mf ModeratedFlag) Score {
	return Score(math.Copysign(float64(bs)+float64(tf), getModerationMultiplier(mf)))
}

func eventTimeToTimeFraction(eventTime EventTime) TimeFraction {
	if int64(eventTime) <= MinimumEventTime {
		eventTime = EventTime(MinimumEventTime) + 1
	}

	if int64(eventTime) >= MaximumEventTime {
		eventTime = EventTime(MaximumEventTime) - 1
	}

	// TF  = Time Fraction: A floating point decimal between 1 and 0
	// ET = Event Time: Time of the event (unix nanoseconds)
	// MAX = Max Event Time: Time of the maximum allowed event time (unix nanoseconds)
	// MIN = Min Event Time: Time of the minimum allowed event time (unix nanoseconds)
	//
	// TF = 1 - ((ET - MIN) / (MAX - MIN))

	bOne := mathUtils.Uint64ToBigFloat(1)
	bEventTime := mathUtils.Uint64ToBigFloat(uint64(eventTime))
	bMinEventTime := mathUtils.Int64ToBigFloat(MinimumEventTime)
	bMaxEventTime := mathUtils.Int64ToBigFloat(MaximumEventTime)

	bNumerator := new(big.Float).Sub(bEventTime, bMinEventTime)
	bDenominator := new(big.Float).Sub(bMaxEventTime, bMinEventTime)
	bFraction := new(big.Float).Quo(bNumerator, bDenominator)
	bTimeFraction := new(big.Float).Sub(bOne, bFraction)

	bMaxTimeFraction := big.NewFloat(maximumTimeFraction)
	bMin := new(big.Float).Sub(bOne, bMaxTimeFraction)
	bMax := new(big.Float).Copy(bMaxTimeFraction)

	// Force the final score to be greater than 0 and less than 1
	finalTimeFraction, _ := mathUtils.ClampBigFloat(bTimeFraction, bMin, bMax).Float64()
	return TimeFraction(finalTimeFraction)
}
func (s Score) DecodeScore() EntryScore {
	bs, et, mf := decodeScore(s)
	return EntryScore{
		BaseScore:     bs,
		EventTime:     et,
		ModeratedFlag: mf,
	}
}

func decodeScore(s Score) (BaseScore, EventTime, ModeratedFlag) {
	bs, tf, mf := separateScoreComponents(s)
	et := timeFractionToEventTime(tf)
	return bs, et, mf

}

func getModeratedFlag(score Score) ModeratedFlag {
	if !math.Signbit(float64(score)) {
		return NotModerated
	} else {
		return Moderated
	}
}

func separateScoreComponents(score Score) (BaseScore, TimeFraction, ModeratedFlag) {
	absScore := math.Abs(float64(score))
	baseScore := math.Floor(absScore)
	timeFraction := absScore - baseScore
	return BaseScore(baseScore), TimeFraction(timeFraction), getModeratedFlag(score)
}

func timeFractionToEventTime(timeFraction TimeFraction) EventTime {
	if float64(timeFraction) <= 0.0 {
		timeFraction = 1.0 - maximumTimeFraction
	}

	if float64(timeFraction) >= maximumTimeFraction {
		timeFraction = maximumTimeFraction
	}

	// TF  = Time Fraction: A floating point decimal between 0 and 1
	// ET  = Event Time: Time of the event (unix nanoseconds)
	// MAX = Max Event Time: Time of the maximum allowed event time (unix nanoseconds)
	// MIN = Min Event Time: Time of the minimum allowed event time (unix nanoseconds)
	//
	// ET = (1 - TF) * MAX + TF * MIN

	bOne := mathUtils.Uint64ToBigFloat(1)
	bTF := big.NewFloat(float64(timeFraction))
	bMin := mathUtils.Int64ToBigFloat(MinimumEventTime)
	bMax := mathUtils.Int64ToBigFloat(MaximumEventTime)

	bOneMinusTF := new(big.Float).Sub(bOne, bTF)
	bLHS := new(big.Float).Mul(bOneMinusTF, bMax)
	bRHS := new(big.Float).Mul(bTF, bMin)

	et := new(big.Float).Add(bLHS, bRHS)

	// Force the final event time to be between the min and max
	finalEventTime, _ := mathUtils.ClampBigFloat(et, bMin, bMax).Int64()
	return EventTime(finalEventTime)
}

func (s Score) Add(scoreAddition EntryScore) Score {
	return add(s, scoreAddition.BaseScore, scoreAddition.EventTime)
}

func (s Score) Moderate() Score {
	bs, tf, _ := separateScoreComponents(s)
	return combineScoreComponents(bs, tf, Moderated)
}

func (s Score) Unmoderate() Score {
	bs, tf, _ := separateScoreComponents(s)
	return combineScoreComponents(bs, tf, NotModerated)
}

func add(prevScore Score, bs BaseScore, et EventTime) Score {
	tf := eventTimeToTimeFraction(et)
	prevBS, prevTF, prevMF := separateScoreComponents(prevScore)

	// legacy scores have the maximum possible time fraction
	if prevTF == 0 {
		prevTF = maximumTimeFraction
	}

	newBS := prevBS + bs
	newTF := TimeFraction(math.Min(float64(prevTF), float64(tf)))
	return combineScoreComponents(newBS, newTF, prevMF)

}
