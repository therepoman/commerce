package game_queue

import (
	"fmt"
	"time"

	"code.justin.tv/commerce/teamredacted/backend/clients/redis"
	go_redis "github.com/go-redis/redis"
)

const (
	maxQueueSize = 200
)

type Manager interface {
	GetQueueScore(gameID string, userID string) (Score, error)
	UpdateQueueScore(gameID string, userID string, scoreIncrease int64, timestamp *time.Time) error
	IsUserInQueue(gameID string, userID string) (bool, error)
	RemoveUserFromQueue(gameID string, userID string) error
	GetQueue(gameID string) ([]Entry, error)
	PopNextUserFromQueue(gameID string) (*Entry, error)
}

type manager struct {
	RedisClient redis.Client `inject:""`
}

type Entry struct {
	UserID    string    `json:"user_id"`
	Score     int64     `json:"score"`
	Timestamp time.Time `json:"timestamp"`
}

func NewManager() Manager {
	return &manager{}
}

func (m *manager) GetQueueScore(gameID string, userID string) (Score, error) {
	scoreFloat, err := m.RedisClient.ZScore(GetGameQueueKey(gameID), userID)
	if err != nil {
		return 0, err
	}
	return Score(scoreFloat), nil
}

func (m *manager) UpdateQueueScore(gameID string, userID string, scoreIncrease int64, timestamp *time.Time) error {
	score, err := m.GetQueueScore(gameID, userID)
	if err != nil && err != go_redis.Nil {
		return err
	}

	eventTime := time.Unix(0, 0)
	if timestamp != nil {
		eventTime = *timestamp
	}
	score = score.Add(EntryScore{
		BaseScore: BaseScore(scoreIncrease),
		EventTime: EventTime(eventTime.UnixNano()),
	})
	_, err = m.RedisClient.ZAdd(GetGameQueueKey(gameID), go_redis.Z{
		Score:  float64(score),
		Member: userID,
	})
	return err
}

func (m *manager) IsUserInQueue(gameID string, userID string) (bool, error) {
	_, err := m.GetQueueScore(gameID, userID)
	if err == go_redis.Nil {
		return false, nil
	} else if err != nil {
		return false, err
	}
	return true, nil
}

func (m *manager) RemoveUserFromQueue(gameID string, userID string) error {
	_, err := m.RedisClient.ZRem(GetGameQueueKey(gameID), userID)
	return err
}

func (m *manager) GetQueue(gameID string) ([]Entry, error) {
	return m.getQueue(gameID, maxQueueSize)
}

func (m *manager) getQueue(gameID string, n int64) ([]Entry, error) {
	zs, err := m.RedisClient.ZRevRangeWithScores(GetGameQueueKey(gameID), 0, n)
	if err != nil {
		return nil, err
	}
	return ToEntries(zs), nil
}

func (m *manager) PopNextUserFromQueue(gameID string) (*Entry, error) {
	zs, err := m.getQueue(gameID, 1)
	if err != nil {
		return nil, err
	}

	if len(zs) <= 0 {
		return nil, nil
	}

	topEntry := zs[0]
	err = m.RemoveUserFromQueue(gameID, topEntry.UserID)
	if err != nil {
		return nil, err
	}

	return &topEntry, nil

}

func GetGameQueueKey(gameID string) string {
	return fmt.Sprintf("game-queue-%s", gameID)
}

func ToEntries(zs []go_redis.Z) []Entry {
	entries := make([]Entry, 0)
	for _, z := range zs {
		entries = append(entries, ToEntry(z))
	}
	return entries
}

func ToEntry(z go_redis.Z) Entry {
	es := Score(z.Score).DecodeScore()
	ts := time.Unix(0, int64(es.EventTime))
	return Entry{
		UserID:    z.Member.(string),
		Score:     int64(es.BaseScore),
		Timestamp: ts,
	}
}
