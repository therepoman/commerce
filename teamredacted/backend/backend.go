package backend

import (
	"io"
	"net/http"
	"time"

	"code.justin.tv/commerce/teamredacted/backend/api/make_c4_move"

	"code.justin.tv/commerce/teamredacted/backend/pubsub"

	prism "code.justin.tv/commerce/prism/rpc"

	"code.justin.tv/commerce/teamredacted/backend/api/start_game"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/splatter"
	"code.justin.tv/commerce/teamredacted/backend/api/create_game"
	"code.justin.tv/commerce/teamredacted/backend/api/get_active_game_for_user"
	"code.justin.tv/commerce/teamredacted/backend/api/get_game_queue"
	"code.justin.tv/commerce/teamredacted/backend/api/join_game_queue"
	"code.justin.tv/commerce/teamredacted/backend/api/leave_game_queue"
	"code.justin.tv/commerce/teamredacted/backend/clients/redis"
	dynamo_games "code.justin.tv/commerce/teamredacted/backend/dynamo/games"
	"code.justin.tv/commerce/teamredacted/backend/eventbus"
	"code.justin.tv/commerce/teamredacted/backend/game_queue"
	"code.justin.tv/commerce/teamredacted/backend/server"
	"code.justin.tv/commerce/teamredacted/config"
	teamredacted "code.justin.tv/commerce/teamredacted/twirp"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/facebookgo/inject"
	"github.com/pkg/errors"
)

type Backend interface {
	Ping(w http.ResponseWriter, r *http.Request)
	TwirpServer() teamredacted.TeamRedacted
	Statter() statsd.Statter
}

type backend struct {
	// Twirp Server
	Server teamredacted.TeamRedacted `inject:""`

	SQS sqsiface.SQSAPI `inject:""`

	// Config
	Config *config.Config `inject:""`

	// Stats
	Stats statsd.Statter `inject:""`

	EventBusHandler eventbus.Handler `inject:"the-event-bus"`
}

func NewBackend(cfg *config.Config) (Backend, error) {
	b := &backend{}

	if cfg == nil {
		return nil, errors.New("received nil config")
	}
	cloudwatchStatter := splatter.NewBufferedTelemetryCloudWatchStatter(&splatter.BufferedTelemetryConfig{
		AWSRegion:         cfg.Environment.AWSRegion,
		ServiceName:       "teamredacted",
		Stage:             cfg.Environment.Name,
		BufferSize:        100000,
		AggregationPeriod: time.Minute,
		FlushPeriod:       time.Second * 30,
	}, map[string]bool{})

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(endpoints.UsWest2RegionID),
	})
	if err != nil {
		return nil, err
	}

	b.Stats = cloudwatchStatter
	sqsClient := sqs.New(sess, &aws.Config{
		Region: aws.String(endpoints.UsWest2RegionID),
	})

	redisClient := redis.NewClient(cfg)

	prismClient := prism.NewPrismProtobufClient(cfg.PrismEndpoint, http.DefaultClient)

	deps := []*inject.Object{
		{Value: b},
		{Value: cloudwatchStatter},
		{Value: cfg},

		// DAOs
		{Value: dynamo_games.NewDAO(sess, cfg)},

		// Twirp server
		{Value: server.NewServer()},

		// Clients
		{Value: redisClient},
		{Value: sqsClient},
		{Value: prismClient},
		{Value: pubsub.NewPublisher()},

		// APIs
		{Value: create_game.NewAPI(), Name: "create_game_api"},
		{Value: get_active_game_for_user.NewAPI(), Name: "get_active_game_for_user_api"},
		{Value: get_game_queue.NewAPI(), Name: "get_game_queue_api"},
		{Value: join_game_queue.NewAPI(), Name: "join_game_queue_api"},
		{Value: leave_game_queue.NewAPI(), Name: "leave_game_queue_api"},
		{Value: make_c4_move.NewAPI(), Name: "make_c4_move_api"},
		{Value: start_game.NewAPI(), Name: "start_game_api"},

		// Business Logic
		{Value: game_queue.NewManager()},

		// SQS handlers
		{Value: eventbus.NewHandler(cfg.EventBusConfig), Name: "the-event-bus"},
	}

	var graph inject.Graph
	err = graph.Provide(deps...)

	if err != nil {
		return nil, err
	}

	err = graph.Populate()
	if err != nil {
		return nil, err
	}

	log.Info("Backend initialization complete")

	return b, nil
}

func (b *backend) Ping(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	_, err := io.WriteString(w, "pong")
	if err != nil {
		log.WithError(err).Error("Error writing http response")
	}
}

func (b *backend) TwirpServer() teamredacted.TeamRedacted {
	return b.Server
}

func (b *backend) Statter() statsd.Statter {
	return b.Stats
}
