package get_game_queue

import (
	"context"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/teamredacted/backend/game_queue"
	teamredacted "code.justin.tv/commerce/teamredacted/twirp"
	"github.com/golang/protobuf/ptypes"
	"github.com/twitchtv/twirp"
)

type API interface {
	GetGameQueue(ctx context.Context, req *teamredacted.GetGameQueueRequest) (*teamredacted.GetGameQueueResponse, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	GameQueueManager game_queue.Manager `inject:""`
}

func (api *api) GetGameQueue(ctx context.Context, req *teamredacted.GetGameQueueRequest) (*teamredacted.GetGameQueueResponse, error) {
	if req == nil {
		return nil, twirp.NewError(twirp.InvalidArgument, "request cannot be nil")
	} else if req.GameId == "" {
		return nil, twirp.NewError(twirp.InvalidArgument, "game id cannot be blank")
	}

	entries, err := api.GameQueueManager.GetQueue(req.GameId)
	if err != nil {
		logrus.WithError(err).Error("error getting queue from queue manager")
		return nil, twirp.InternalErrorWith(err)
	}

	twirpEntries := make([]*teamredacted.GameQueueEntry, 0)
	for _, entry := range entries {
		ts, err := ptypes.TimestampProto(entry.Timestamp)
		if err != nil {
			logrus.WithError(err).Error("error converting golang timestamp to twirp type")
			return nil, twirp.InternalErrorWith(err)
		}

		twirpEntries = append(twirpEntries, &teamredacted.GameQueueEntry{
			UserId:    entry.UserID,
			Score:     entry.Score,
			Timestamp: ts,
		})
	}

	return &teamredacted.GetGameQueueResponse{
		GameQueue: twirpEntries,
	}, nil
}
