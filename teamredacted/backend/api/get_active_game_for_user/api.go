package get_active_game_for_user

import (
	"context"
	"encoding/json"

	"code.justin.tv/commerce/logrus"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/teamredacted/backend/dynamo/games"
	teamredacted "code.justin.tv/commerce/teamredacted/twirp"
)

type API interface {
	Get(ctx context.Context, req *teamredacted.GetActiveGameForUserRequest) (*teamredacted.GetActiveGameForUserResponse, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	DAO games.DAO `inject:""`
}

func (a *api) Get(ctx context.Context, req *teamredacted.GetActiveGameForUserRequest) (*teamredacted.GetActiveGameForUserResponse, error) {
	if req == nil {
		return nil, twirp.NewError(twirp.InvalidArgument, "request cannot be nil")
	} else if req.UserId == "" {
		return nil, twirp.NewError(twirp.InvalidArgument, "user id cannot be blank")
	}

	game, err := a.DAO.GetActiveGameForUserID(ctx, req.UserId)
	if err != nil {
		logrus.WithError(err).Error("error getting active game for user id from dynamo")
		return nil, err
	}

	if game == nil {
		return &teamredacted.GetActiveGameForUserResponse{}, nil
	}

	gameState, err := json.Marshal(&game.State)
	if err != nil {
		logrus.WithError(err).Error("error marshaling game state to json")
		return nil, err
	}

	return &teamredacted.GetActiveGameForUserResponse{
		GameId:     game.ID,
		State:      string(gameState),
		OpponentId: game.OpponentID,
	}, err
}
