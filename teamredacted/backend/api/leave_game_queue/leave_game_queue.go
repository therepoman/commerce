package leave_game_queue

import (
	"context"

	"code.justin.tv/commerce/logrus"

	"code.justin.tv/commerce/teamredacted/backend/pubsub"

	"code.justin.tv/commerce/teamredacted/backend/game_queue"
	teamredacted "code.justin.tv/commerce/teamredacted/twirp"
	"github.com/twitchtv/twirp"
)

type API interface {
	LeaveGameQueue(ctx context.Context, req *teamredacted.LeaveGameQueueRequest) (*teamredacted.LeaveGameQueueResponse, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	GameQueueManager game_queue.Manager `inject:""`
	PubsubPublisher  pubsub.Publisher   `inject:""`
}

func (api *api) LeaveGameQueue(ctx context.Context, req *teamredacted.LeaveGameQueueRequest) (*teamredacted.LeaveGameQueueResponse, error) {
	if req == nil {
		return nil, twirp.NewError(twirp.InvalidArgument, "request cannot be nil")
	} else if req.UserId == "" {
		return nil, twirp.NewError(twirp.InvalidArgument, "user id cannot be blank")
	} else if req.GameId == "" {
		return nil, twirp.NewError(twirp.InvalidArgument, "game id cannot be blank")
	}

	err := api.GameQueueManager.RemoveUserFromQueue(req.GameId, req.UserId)
	if err != nil {
		logrus.WithError(err).Error("error removing user from queue")
		return nil, twirp.InternalErrorWith(err)
	}

	api.PubsubPublisher.Publish(ctx, req.GameId)

	return &teamredacted.LeaveGameQueueResponse{}, nil
}
