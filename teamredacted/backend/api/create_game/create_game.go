package create_game

import (
	"context"
	"encoding/json"

	"github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/teamredacted/backend/pubsub"

	"code.justin.tv/commerce/teamredacted/backend/dynamo"
	dynamo_games "code.justin.tv/commerce/teamredacted/backend/dynamo/games"
	teamredacted "code.justin.tv/commerce/teamredacted/twirp"
	"github.com/google/uuid"
)

type API interface {
	Create(ctx context.Context, req *teamredacted.CreateGameRequest) (*teamredacted.CreateGameResponse, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	GamesDAO        dynamo_games.DAO `inject:""`
	PubsubPublisher pubsub.Publisher `inject:""`
}

func (c *api) Create(ctx context.Context, req *teamredacted.CreateGameRequest) (*teamredacted.CreateGameResponse, error) {
	if req == nil {
		return nil, twirp.NewError(twirp.InvalidArgument, "request cannot be nil")
	} else if req.BroadcasterId == "" {
		return nil, twirp.NewError(twirp.InvalidArgument, "broadcaster id cannot be blank")
	}

	gameID, err := uuid.NewUUID()
	if err != nil {
		logrus.WithError(err).Error("error generating gameID UUID")
		return nil, err
	}

	game := dynamo.Game{
		ID: gameID.String(),
		State: dynamo.GameState{
			Board:    dynamo.ConstructNewBoard(),
			Complete: false,
		},
		UserID: req.BroadcasterId,
	}
	err = c.GamesDAO.CreateGame(ctx, &game)
	if err != nil {
		logrus.WithError(err).Error("error creating game in dynamo")
		return nil, err
	}

	gameState, err := json.Marshal(&game.State)
	if err != nil {
		logrus.WithError(err).Error("error marshaling game state to json")
		return nil, err
	}

	c.PubsubPublisher.Publish(ctx, game.ID)

	return &teamredacted.CreateGameResponse{
		GameId: gameID.String(),
		State:  string(gameState),
	}, nil
}
