package make_c4_move

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/twitchtv/twirp"

	"code.justin.tv/commerce/teamredacted/backend/dynamo"
	"code.justin.tv/commerce/teamredacted/backend/dynamo/games"
	"code.justin.tv/commerce/teamredacted/backend/pubsub"
	teamredacted "code.justin.tv/commerce/teamredacted/twirp"
	"github.com/pkg/errors"
)

type API interface {
	MakeC4Move(ctx context.Context, request *teamredacted.MakeC4MoveRequest) (*teamredacted.MakeC4MoveResponse, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	DAO             games.DAO        `inject:""`
	PubsubPublisher pubsub.Publisher `inject:""`
}

func (m *api) MakeC4Move(ctx context.Context, request *teamredacted.MakeC4MoveRequest) (*teamredacted.MakeC4MoveResponse, error) {
	if request == nil {
		return nil, twirp.NewError(twirp.InvalidArgument, "request cannot be nil")
	} else if request.GameId == "" {
		return nil, twirp.NewError(twirp.InvalidArgument, "game id cannot be blank")
	}

	game, err := m.DAO.GetGameByID(ctx, request.GameId)
	if err != nil {
		return nil, err
	}

	if game.State.Complete {
		return nil, twirp.NewError(twirp.FailedPrecondition, "game is complete")
	}

	if game.OpponentID == "" {
		return nil, twirp.NewError(twirp.FailedPrecondition, "game must be started first (game does not yet have an opponent)")
	}

	var isBroadcaster bool
	if request.UserId == game.UserID {
		isBroadcaster = true
	} else if request.UserId != game.OpponentID {
		return nil, twirp.NewError(twirp.InvalidArgument, fmt.Sprintf("hey pardner, this ain't your game! this is a game belongin' to a feller with the ID %s", game.UserID))
	}

	board, err := dynamo.UnmarshalBoard(game.State.Board)
	if err != nil {
		return nil, err
	}

	col := int(request.Col)
	if col >= dynamo.Columns || col < 0 {
		return nil, errors.New("column out of range of board")
	}

	resp := teamredacted.MakeC4MoveResponse{}

	if isValidMove(board, int(request.Col)) {
		resp.IsValid = true

		var token dynamo.SpaceState
		if isBroadcaster {
			token = dynamo.Broadcaster
		} else {
			token = dynamo.Viewer
		}
		board, hasWin := boardAfterMove(board, col, token)

		fmt.Println(board, hasWin)

		game.State.Complete = hasWin
		if hasWin {
			if isBroadcaster {
				game.State.Winner = game.UserID
			} else {
				game.State.Winner = game.OpponentID
			}
		}

		boardJson, err := json.Marshal(board)
		if err != nil {
			return nil, err
		}

		game.State.Board = string(boardJson)

		if hasWin {
			game.OpponentID = ""
			game.State.Complete = false
			game.State.Board = dynamo.ConstructNewBoard()
			game.State.Winner = ""
		}

		err = m.DAO.UpdateGame(ctx, game)
		if err != nil {
			return nil, err
		}
	} else {
		resp.IsValid = false
	}

	m.PubsubPublisher.Publish(ctx, request.GameId)

	stateJson, err := json.Marshal(game.State)
	if err != nil {
		return nil, err
	}

	resp.State = string(stateJson)

	return &resp, nil
}

func isValidMove(board [][]string, col int) bool {
	return dynamo.SpaceState(board[col][0]) == dynamo.Empty
}

func boardAfterMove(board [][]string, col int, token dynamo.SpaceState) ([][]string, bool) {

	for row := range board[col] {
		if row == dynamo.Rows-1 && dynamo.SpaceState(board[col][row]) == dynamo.Empty {
			board[col][row] = string(token)
			break
		} else if dynamo.SpaceState(board[col][row]) != dynamo.Empty {
			board[col][row-1] = string(token)
			break
		}
	}

	return board, boardHasWin(board, token)
}

func boardHasWin(board [][]string, token dynamo.SpaceState) bool {
	for r := 0; r < dynamo.Rows; r++ {
		for c := 0; c < dynamo.Columns; c++ {
			if boardHasWinHelper(board, c, r, token) {
				return true
			}
		}
	}
	return false
}

func boardHasWinHelper(board [][]string, col int, row int, token dynamo.SpaceState) bool {
	if dynamo.SpaceState(board[col][row]) != token {
		return false
	}

	// look right
	if col+3 < dynamo.Columns {
		for i := col + 1; i <= col+3; i++ {
			if dynamo.SpaceState(board[i][row]) == token {
				if i == col+3 {
					return true
				}
			} else {
				break
			}
		}
	}

	// look left
	if col-3 >= 0 {
		for i := col - 1; i >= col-3; i-- {
			if dynamo.SpaceState(board[i][row]) == token {
				if i == col-3 {
					return true
				}
			} else {
				break
			}
		}
	}

	// look down
	if row+3 < dynamo.Rows {
		for i := row + 1; i <= row+3; i++ {
			if dynamo.SpaceState(board[col][i]) == token {
				if i == row+3 {
					return true
				}
			} else {
				break
			}
		}
	}

	// look down-left
	if row+3 < dynamo.Rows && col-3 >= 0 {
		for i := 1; i <= 3; i++ {
			if dynamo.SpaceState(board[col-i][row+i]) == token {
				if i == 3 {
					return true
				}
			} else {
				break
			}
		}
	}

	// look down-right
	if row+3 < dynamo.Rows && col+3 < dynamo.Columns {
		for i := 1; i <= 3; i++ {
			if dynamo.SpaceState(board[col+i][row+i]) == token {
				if i == 3 {
					return true
				}
			} else {
				break
			}
		}
	}

	// look up-left
	if row-3 >= 0 && col-3 >= 0 {
		for i := 1; i <= 3; i++ {
			if dynamo.SpaceState(board[col-i][row-i]) == token {
				if i == 3 {
					return true
				}
			} else {
				break
			}
		}
	}

	// look up-left
	if row-3 >= 0 && col+3 < dynamo.Columns {
		for i := 1; i <= 3; i++ {
			if dynamo.SpaceState(board[col+i][row-i]) == token {
				if i == 3 {
					return true
				}
			} else {
				break
			}
		}
	}

	return false
}
