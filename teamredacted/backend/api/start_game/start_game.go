package start_game

import (
	"context"

	"code.justin.tv/commerce/logrus"

	"code.justin.tv/commerce/teamredacted/backend/pubsub"

	"code.justin.tv/commerce/teamredacted/backend/dynamo/games"
	"code.justin.tv/commerce/teamredacted/backend/game_queue"
	teamredacted "code.justin.tv/commerce/teamredacted/twirp"
	"github.com/twitchtv/twirp"
)

type API interface {
	StartGame(ctx context.Context, req *teamredacted.StartGameRequest) (*teamredacted.StartGameResponse, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	GameQueueManager game_queue.Manager `inject:""`
	GameDAO          games.DAO          `inject:""`
	PubsubPublisher  pubsub.Publisher   `inject:""`
}

func (api *api) StartGame(ctx context.Context, req *teamredacted.StartGameRequest) (*teamredacted.StartGameResponse, error) {
	if req == nil {
		return nil, twirp.NewError(twirp.InvalidArgument, "request cannot be nil")
	} else if req.GameId == "" {
		return nil, twirp.NewError(twirp.InvalidArgument, "game id cannot be blank")
	}

	game, err := api.GameDAO.GetGameByID(ctx, req.GameId)
	if err != nil {
		logrus.WithError(err).Error("error getting game by id")
		return nil, twirp.InternalErrorWith(err)
	}

	if game == nil {
		return nil, twirp.NewError(twirp.InvalidArgument, "no game for game id")
	}

	topEntry, err := api.GameQueueManager.PopNextUserFromQueue(req.GameId)
	if err != nil {
		logrus.WithError(err).Error("error popping next user from queue")
		return nil, twirp.InternalErrorWith(err)
	}

	if topEntry == nil {
		return nil, twirp.NewError(twirp.InvalidArgument, "no users in the game queue")
	}

	game.OpponentID = topEntry.UserID

	err = api.GameDAO.UpdateGame(ctx, game)
	if err != nil {
		logrus.WithError(err).Error("error updating game in dynamo")
		return nil, twirp.InternalErrorWith(err)
	}

	api.PubsubPublisher.Publish(ctx, req.GameId)

	return &teamredacted.StartGameResponse{
		UserId: topEntry.UserID,
	}, nil
}
