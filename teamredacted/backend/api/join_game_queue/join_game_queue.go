package join_game_queue

import (
	"context"
	"time"

	"code.justin.tv/commerce/logrus"

	"code.justin.tv/commerce/teamredacted/backend/pubsub"

	"code.justin.tv/commerce/teamredacted/backend/game_queue"
	teamredacted "code.justin.tv/commerce/teamredacted/twirp"
	"github.com/twitchtv/twirp"
)

type API interface {
	JoinGameQueue(ctx context.Context, req *teamredacted.JoinGameQueueRequest) (*teamredacted.JoinGameQueueResponse, error)
}

func NewAPI() API {
	return &api{}
}

type api struct {
	GameQueueManager game_queue.Manager `inject:""`
	PubsubPublisher  pubsub.Publisher   `inject:""`
}

func (api *api) JoinGameQueue(ctx context.Context, req *teamredacted.JoinGameQueueRequest) (*teamredacted.JoinGameQueueResponse, error) {
	requestTime := time.Now()

	if req == nil {
		return nil, twirp.NewError(twirp.InvalidArgument, "request cannot be nil")
	} else if req.UserId == "" {
		return nil, twirp.NewError(twirp.InvalidArgument, "user id cannot be blank")
	} else if req.GameId == "" {
		return nil, twirp.NewError(twirp.InvalidArgument, "game id cannot be blank")
	}

	inQueue, err := api.GameQueueManager.IsUserInQueue(req.GameId, req.UserId)
	if err != nil {
		logrus.WithError(err).Error("error checking if user is in queue")
		return nil, twirp.InternalErrorWith(err)
	}

	if inQueue {
		return &teamredacted.JoinGameQueueResponse{}, nil
	}

	err = api.GameQueueManager.UpdateQueueScore(req.GameId, req.UserId, 1, &requestTime)
	if err != nil {
		logrus.WithError(err).Error("error updating user score in queue")
		return nil, twirp.InternalErrorWith(err)
	}

	api.PubsubPublisher.Publish(ctx, req.GameId)

	return &teamredacted.JoinGameQueueResponse{}, nil
}
