package eventbus

import (
	"context"

	"code.justin.tv/commerce/teamredacted/backend/pubsub"

	"code.justin.tv/commerce/gogogadget/math"
	"code.justin.tv/commerce/teamredacted/backend/dynamo/games"
	"code.justin.tv/commerce/teamredacted/backend/game_queue"

	log "code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/teamredacted/config"
	eventbus "code.justin.tv/eventbus/client"
	"code.justin.tv/eventbus/client/subscriber/sqsclient"
	"code.justin.tv/eventbus/schema/pkg/cheer"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/cactus/go-statsd-client/statsd"
	"github.com/sirupsen/logrus"
)

const (
	cheerMessagesReceived  = "cheer_messages_received"
	cheerMessagesProcessed = "cheer_messages_processed"
)

var channelWhitelist = map[string]interface{}{
	"267887052": nil, // qa_walrus_partner
	"265716088": nil, // qa_bonta_partner
	"108707191": nil, // seph
	"153829890": nil, // seph_affiliate
	"116076154": nil, // qa_bits_partner
}

type Handler interface {
}

type handler struct {
	GamesDAO         games.DAO          `inject:""`
	GameQueueManager game_queue.Manager `inject:""`
	Stats            statsd.Statter     `inject:""`
	PubsubPublisher  pubsub.Publisher   `inject:""`
}

func NewHandler(cfg *config.SQSConfig) Handler {
	dehandler := handler{}
	if cfg.Disabled {
		return dehandler
	}

	mux := eventbus.NewMux()
	cheer.RegisterCheerCreateHandler(mux, dehandler.Handle)

	_, err := sqsclient.New(sqsclient.Config{
		Session: session.Must(session.NewSession(&aws.Config{
			Region: aws.String(cfg.Region),
		})),
		Dispatcher: mux.Dispatcher(),
		QueueURL:   cfg.URL,
	})

	if err != nil {
		log.WithError(err).Panic("Failed to start SQS listener for event bus")
	}

	return &dehandler
}

func (h *handler) Handle(ctx context.Context, header *eventbus.Header, payload *cheer.CheerCreate) error {
	_ = h.Stats.Inc(cheerMessagesReceived, 1, 1.0)

	tuid := ""
	user := payload.GetUser()
	if user != nil {
		tuid = user.Id
	}

	channel := payload.GetToUserId()

	if _, ok := channelWhitelist[channel]; !ok {
		// not in whitelist
		return nil
	}

	logrus.WithFields(logrus.Fields{
		"messageId":     header.MessageID,
		"created":       header.CreatedAt.UnixNano(),
		"transactionId": payload.BitsTransactionId,
		"tuid":          tuid,
		"channel":       channel,
		"bitsAmount":    payload.Bits,
	}).Info("Message received from event bus")

	game, err := h.GamesDAO.GetActiveGameForUserID(ctx, channel)
	if err != nil {
		logrus.WithError(err).WithField("channel", channel).Error("error getting active games for channel")
		return err
	}

	if game == nil {
		logrus.WithField("channel", channel).Info("no active game found for channel")
		return nil
	}

	inQueue, err := h.GameQueueManager.IsUserInQueue(game.ID, tuid)
	if err != nil {
		logrus.WithFields(logrus.Fields{"user": tuid, "game_id": game.ID}).WithError(err).Error("error checking if user is already in game queue")
		return err
	}

	if inQueue {
		err = h.GameQueueManager.UpdateQueueScore(game.ID, tuid, math.Int64Abs(payload.GetBits()), nil)
		if err != nil {
			logrus.WithFields(logrus.Fields{"user": tuid, "game_id": game.ID}).WithError(err).Error("error updating users game queue score")
			return err
		}

		h.PubsubPublisher.Publish(ctx, game.ID)
	} else {
		logrus.WithFields(logrus.Fields{"user": tuid, "game_id": game.ID}).WithError(err).Info("user is not in game queue")
	}

	_ = h.Stats.Inc(cheerMessagesProcessed, 1, 1.0)
	return nil
}
