package redis

import (
	"time"

	"code.justin.tv/commerce/teamredacted/config"
	"github.com/go-redis/redis"
)

// Client represents a Redis client
type Client interface {
	Ping() error
	Get(key string) (string, error)
	Set(key string, value interface{}, expiration time.Duration) error
	Delete(key string) error
	ZAdd(key string, members ...redis.Z) (int64, error)
	ZCard(key string) (int64, error)
	ZCount(key, min string, max string) (int64, error)
	ZRem(key string, members ...interface{}) (int64, error)
	ZRevRank(key, member string) (int64, error)
	ZRevRangeWithScores(key string, start, stop int64) ([]redis.Z, error)
	ZScore(key, member string) (float64, error)
}

type client struct {
	redisClient redis.Cmdable
}

// NewClient creates a new Redis Client
func NewClient(cfg *config.Config) Client {
	if cfg.Redis.ClusterEndpoint != "" {
		redisClient := redis.NewClusterClient(&redis.ClusterOptions{
			Addrs: []string{cfg.Redis.ClusterEndpoint},
		})

		return &client{
			redis.Cmdable(redisClient),
		}
	}

	redisClient := redis.NewClient(&redis.Options{
		Addr: cfg.Redis.Endpoint,
	})

	return &client{
		redis.Cmdable(redisClient),
	}
}

// Ping ...
func (c *client) Ping() error {
	_, err := c.redisClient.Ping().Result()
	return err
}

// Get ...
func (c *client) Get(key string) (string, error) {
	value, err := c.redisClient.Get(key).Result()
	if err != nil {
		return "", err
	}
	return value, nil
}

// Set ...
func (c *client) Set(key string, value interface{}, expiration time.Duration) error {
	return c.redisClient.Set(key, value, expiration).Err()
}

// Delete ...
func (c *client) Delete(key string) error {
	return c.redisClient.Del(key).Err()
}

func (c *client) ZAdd(key string, members ...redis.Z) (int64, error) {
	return c.redisClient.ZAdd(key, members...).Result()
}

func (c *client) ZCard(key string) (int64, error) {
	return c.redisClient.ZCard(key).Result()
}

func (c *client) ZRem(key string, members ...interface{}) (int64, error) {
	return c.redisClient.ZRem(key, members...).Result()
}

func (c *client) ZRevRank(key, member string) (int64, error) {
	return c.redisClient.ZRevRank(key, member).Result()
}

func (c *client) ZRevRangeWithScores(key string, start, stop int64) ([]redis.Z, error) {
	return c.redisClient.ZRevRangeWithScores(key, start, stop).Result()
}

func (c *client) ZScore(key, member string) (float64, error) {
	return c.redisClient.ZScore(key, member).Result()
}

func (c *client) ZCount(key, min string, max string) (int64, error) {
	return c.redisClient.ZCount(key, min, max).Result()
}
