package server

import (
	"context"

	"code.justin.tv/commerce/teamredacted/backend/api/start_game"

	"code.justin.tv/commerce/teamredacted/backend/api/create_game"
	"code.justin.tv/commerce/teamredacted/backend/api/get_active_game_for_user"
	"code.justin.tv/commerce/teamredacted/backend/api/get_game_queue"
	"code.justin.tv/commerce/teamredacted/backend/api/join_game_queue"
	"code.justin.tv/commerce/teamredacted/backend/api/leave_game_queue"
	"code.justin.tv/commerce/teamredacted/backend/api/make_c4_move"
	teamredacted "code.justin.tv/commerce/teamredacted/twirp"
)

type server struct {
	CreateGameAPI     create_game.API              `inject:"create_game_api"`
	GetActiveGameAPI  get_active_game_for_user.API `inject:"get_active_game_for_user_api"`
	JoinGameQueueAPI  join_game_queue.API          `inject:"join_game_queue_api"`
	LeaveGameQueueAPI leave_game_queue.API         `inject:"leave_game_queue_api"`
	GetGameQueueAPI   get_game_queue.API           `inject:"get_game_queue_api"`
	MakeC4MoveAPI     make_c4_move.API             `inject:"make_c4_move_api"`
	StartGameAPI      start_game.API               `inject:"start_game_api"`
}

func NewServer() teamredacted.TeamRedacted {
	return &server{}
}

func (s *server) CreateGame(ctx context.Context, req *teamredacted.CreateGameRequest) (*teamredacted.CreateGameResponse, error) {
	return s.CreateGameAPI.Create(ctx, req)
}

func (s *server) StartGame(ctx context.Context, req *teamredacted.StartGameRequest) (*teamredacted.StartGameResponse, error) {
	return s.StartGameAPI.StartGame(ctx, req)
}

func (s *server) JoinGameQueue(ctx context.Context, req *teamredacted.JoinGameQueueRequest) (*teamredacted.JoinGameQueueResponse, error) {
	return s.JoinGameQueueAPI.JoinGameQueue(ctx, req)
}

func (s *server) LeaveGameQueue(ctx context.Context, req *teamredacted.LeaveGameQueueRequest) (*teamredacted.LeaveGameQueueResponse, error) {
	return s.LeaveGameQueueAPI.LeaveGameQueue(ctx, req)
}

func (s *server) GetGameQueue(ctx context.Context, req *teamredacted.GetGameQueueRequest) (*teamredacted.GetGameQueueResponse, error) {
	return s.GetGameQueueAPI.GetGameQueue(ctx, req)
}

func (s *server) GetGameState(context.Context, *teamredacted.GetGameStateRequest) (*teamredacted.GetGameStateResponse, error) {
	return &teamredacted.GetGameStateResponse{}, nil
}

func (s *server) UpdateGameState(context.Context, *teamredacted.UpdateGameStateRequest) (*teamredacted.UpdateGameStateResponse, error) {
	return &teamredacted.UpdateGameStateResponse{}, nil
}

func (s *server) GetActiveGameForUser(ctx context.Context, request *teamredacted.GetActiveGameForUserRequest) (*teamredacted.GetActiveGameForUserResponse, error) {
	return s.GetActiveGameAPI.Get(ctx, request)
}

func (s *server) MakeC4Move(ctx context.Context, request *teamredacted.MakeC4MoveRequest) (*teamredacted.MakeC4MoveResponse, error) {
	return s.MakeC4MoveAPI.MakeC4Move(ctx, request)
}
