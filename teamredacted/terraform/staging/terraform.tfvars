environment = "staging"

env = "staging"

aws_profile = "teamredacted-devo"

region = "us-west-2"

backup_region = "us-east-2"

vpc_id = "vpc-04df4465fecab8578"

security_group = "sg-00941b6d3cebe6374"

ec2_instances_security_group = "sg-02c5e1551acb0c915"

subnets = "subnet-06fa46940eb6b8c3d,subnet-09244cd199a58eeca,subnet-0eda4a2b44ef08f39"

subnet_ips = ["10.0.32.0/19", "10.0.96.0/19", "10.0.160.0/19"]

whitelisted_arns_for_privatelink = [
  "arn:aws:iam::021561903526:root", // payday
  "arn:aws:iam::645130450452:root", // graphql-dev
]
