variable "aws_profile" {}

variable "region" {}

variable "backup_region" {}

variable "env" {
  description = "The AWS environment. Must begin with one of the following:  devo (development),  staging (staging), or  prod (production)."
}

variable "environment" {}

provider "aws" {
  region  = "${var.region}"
  profile = "${var.aws_profile}"
}

provider "aws" {
  alias   = "backup"
  region  = "${var.backup_region}"
  profile = "${var.aws_profile}"
}

variable "vpc_id" {
  description = "VPC associated with the service. Setup by the systems/networking team"
}

variable "security_group" {
  description = "Security group associated with the service's VPC. Usally called 'twitch_subnets' and setup by the systems/networking team"
}

variable "ec2_instances_security_group" {
}

variable "subnets" {
  description = "Private subnets associated with the service's VPC. Setup by the systems/networking team"
}

variable "subnet_ips" {
  type = "list"
}

variable "whitelisted_arns_for_privatelink" {
  type    = "list"
  default = []
}