module "teamredacted" {
  source = "../modules/teamredacted"

  name          = "teamredacted"
  region        = "${var.region}"
  backup_region = "${var.backup_region}"
  environment   = "${var.environment}"
  env           = "${var.env}"

  cluster = "teamredacted-cluster"

  vpc_id         = "${var.vpc_id}"
  security_group = "${var.security_group}"
  ec2_instances_security_group = "${var.ec2_instances_security_group}"
  subnets        = "${var.subnets}"
  subnet_ips = "${var.subnet_ips}"

  autoscaling_role          = "arn:aws:iam::966663201828:role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable"
  dynamo_min_read_capacity  = "5"
  dynamo_min_write_capacity = "5"

  min_instances = "1"
  max_instances = "5"

  env_vars = [
    {
      name  = "ENVIRONMENT"
      value = "${var.env}"
    },
  ]

  whitelisted_arns_for_privatelink = "${var.whitelisted_arns_for_privatelink}"

  aws_profile = "${var.aws_profile}"

  min_aurora_capacity = 4
  max_aurora_capacity = 16

  sandstorm_role = "teamredacted-staging"

  elasticache_num_node_groups = "2"
  elasticache_replicas_per_node_group = "1"

  providers = {
    aws        = "aws"
    aws.backup = "aws.backup"
  }
}

terraform {
  backend "s3" {
    bucket  = "teamredacted-terraform-staging"
    key     = "tfstate/commerce/teamredacted/terraform/staging"
    region  = "us-west-2"
    profile = "teamredacted-devo"
  }
}