resource "aws_dynamodb_table" "games_table" {
  name           = "games-${var.env}"
  hash_key       = "game_id"
  read_capacity  = "${var.dynamo_min_read_capacity}"
  write_capacity = "${var.dynamo_min_write_capacity}"

  point_in_time_recovery {
    enabled = "true"
  }

  "attribute" {
    name = "game_id"
    type = "S"
  }

  "attribute" {
    name = "user_id"
    type = "S"
  }

  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  lifecycle {
    ignore_changes = ["read_capacity", "write_capacity"]
  }

  global_secondary_index {
    name = "games-by-user-id-${var.env}"
    projection_type = "ALL"
    hash_key = "user_id"
    range_key = "game_id"
    read_capacity = "${var.dynamo_min_read_capacity}"
    write_capacity = "${var.dynamo_min_write_capacity}"
  }
}

module "games_table_autoscaling" {
  source             = "modules/dynamo/dynamo_autoscaling"
  table_name         = "games-${var.env}"
  min_read_capacity  = "${var.dynamo_min_read_capacity}"
  min_write_capacity = "${var.dynamo_min_write_capacity}"
  autoscaling_role   = "${var.autoscaling_role}"
  region             = "${var.region}"
}

module "games_gsi_autoscaling" {
  source             = "modules/dynamo/dynamo_autoscaling"
  table_name         = "games-${var.env}/index/games-by-user-id-${var.env}"
  table_kind = "index"
  min_read_capacity  = "${var.dynamo_min_read_capacity}"
  min_write_capacity = "${var.dynamo_min_write_capacity}"
  autoscaling_role   = "${var.autoscaling_role}"
  region             = "${var.region}"
}