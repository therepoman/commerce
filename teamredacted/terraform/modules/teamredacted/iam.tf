resource "aws_iam_access_key" "tcs_user_key" {
  user = "${var.tcs_user_name}"
}

//resource "aws_iam_user_policy" "tcs_user_beanstalk_user_policy" {
//  name = "beanstalk"
//  user = "${var.tcs_user_name}"
//
//  policy = <<EOF
//{
//  "Version": "2012-10-17",
//  "Statement": [
//      {
//          "Effect": "Allow",
//          "Action": [
//              "elasticbeanstalk:*",
//              "ec2:*",
//              "ecs:*",
//              "elasticloadbalancing:*",
//              "autoscaling:*",
//              "cloudwatch:*",
//              "dynamodb:*",
//              "s3:*",
//              "sns:*",
//              "cloudformation:*",
//              "rds:*",
//              "sqs:*",
//              "iam:GetPolicyVersion",
//              "iam:GetRole",
//              "iam:PassRole",
//              "iam:ListRolePolicies",
//              "iam:ListAttachedRolePolicies",
//              "iam:ListInstanceProfiles",
//              "iam:ListRoles",
//              "iam:ListServerCertificates",
//              "acm:DescribeCertificate",
//              "acm:ListCertificates",
//              "logs:CreateLogGroup",
//              "logs:PutRetentionPolicy"
//          ],
//          "Resource": "*"
//      },
//      {
//          "Effect": "Allow",
//          "Action": [
//              "iam:AddRoleToInstanceProfile",
//              "iam:CreateInstanceProfile",
//              "iam:CreateRole"
//          ],
//          "Resource": [
//              "arn:aws:iam::*:role/aws-elasticbeanstalk*",
//              "arn:aws:iam::*:instance-profile/aws-elasticbeanstalk*"
//          ]
//      },
//      {
//          "Effect": "Allow",
//          "Action": [
//              "iam:AttachRolePolicy"
//          ],
//          "Resource": "*",
//          "Condition": {
//              "StringLike": {
//                  "iam:PolicyArn": [
//                      "arn:aws:iam::aws:policy/AWSElasticBeanstalk*",
//                      "arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalk*"
//                  ]
//              }
//          }
//      }
//  ]
//}
//EOF
//}

resource "aws_iam_user_policy" "tcs_user_ecr_user_policy" {
  name = "ecr"
  user = "${var.tcs_user_name}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Sid": "VisualEditor0",
          "Effect": "Allow",
          "Action": "ecr:*",
          "Resource": "*"
      }
  ]
}
EOF
}

resource "aws_iam_user_policy" "tcs_user_ecs_user_policy" {
  name = "ecs"
  user = "${var.tcs_user_name}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Sid": "VisualEditor0",
          "Effect": "Allow",
          "Action": "ecs:*",
          "Resource": "*"
      }
  ]
}
EOF
}