module "service" {
  source = "git::git+ssh://git@git-aws.internal.justin.tv/subs/terracode//twitch-service?ref=58fba67847d80abb7e7b4d04219bc8845e6bcb3c"

  name        = "${var.name}"
  image       = "${aws_ecr_repository.repository.repository_url}"
  memory      = "${var.memory}"
  logging     = "${var.logging}"
  cpu         = "${var.cpu}"
  alb_port    = "${var.alb_port}"
  env_vars    = "${var.env_vars}"
  command     = "${var.command}"
  launch_type = "${var.launch_type}"

  counts = {
    min = "${var.min_instances}"
    max = "${var.max_instances}"
  }

  port = 8000
  dns  = "*.${var.env}.${var.name}.internal.justin.tv"

  healthcheck = "/ping"

  cluster        = "${module.cluster.ecs_name}"
  security_group = "${var.security_group}"
  region         = "${var.region}"
  vpc_id         = "${var.vpc_id}"
  environment    = "${var.environment}"
  subnets        = "${var.subnets}"
}

resource "aws_iam_role_policy_attachment" "sqs_permission" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
  role = "${module.service.service_role}"
}

resource "aws_iam_role_policy_attachment" "dynamo_access" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
  role = "${module.service.service_role}"
}
