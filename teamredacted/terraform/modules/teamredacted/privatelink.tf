module "privatelink" {
  source = "git::git+ssh://git@git-aws.internal.justin.tv/subs/terracode//privatelink?ref=099a2d57ba837699caeee47ec079b4f0e4028e6f"

  region           = "${var.region}"
  name             = "${var.name}"
  environment      = "${var.env}"
  alb_dns          = "${module.service.dns}"
  cert_arn         = "${module.service.cert}"
  vpc_id           = "${var.vpc_id}"
  subnets          = ["${split(",", var.subnets)}"]
  whitelisted_arns = "${var.whitelisted_arns_for_privatelink}"
}