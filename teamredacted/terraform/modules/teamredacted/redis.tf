resource "aws_elasticache_replication_group" "redis_cluster" {
  automatic_failover_enabled    = "${var.elasticache_auto_failover}"
  replication_group_id          = "${var.name}-${var.env}"
  replication_group_description = "${var.name} redis cluster"
  engine                        = "redis"
  engine_version                = "5.0.4"
  node_type                     = "${var.elasticache_instance_type}"
  parameter_group_name          = "${aws_elasticache_parameter_group.redis_params.name}"
  port                          = "${var.redis_port}"
  subnet_group_name             = "${aws_elasticache_subnet_group.redis_subnet_group.name}"
  security_group_ids            = ["${aws_security_group.redis_sec_group.id}"]

  cluster_mode {
    replicas_per_node_group = "${var.elasticache_replicas_per_node_group}"
    num_node_groups         = "${var.elasticache_num_node_groups}"
  }
}

resource "aws_elasticache_parameter_group" "redis_params" {
  family = "redis5.0"
  name   = "${var.name}-params-${var.env}"

  parameter {
    name  = "maxmemory-policy"
    value = "allkeys-lru"
  }

  parameter {
    name  = "cluster-enabled"
    value = "yes"
  }
}

resource "aws_elasticache_subnet_group" "redis_subnet_group" {
  name       = "${var.name}-${var.env}-cache"
  subnet_ids = ["${split(",", var.subnets)}"]
}

resource "aws_security_group" "redis_sec_group" {
  name        = "${var.name}-${var.env}-redis"
  vpc_id      = "${var.vpc_id}"
  description = "Allows communication with redis server"

  ingress {
    from_port   = "${var.redis_port}"
    protocol    = "tcp"
    to_port     = "${var.redis_port}"
    cidr_blocks = "${var.subnet_ips}"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "teamredacted-${var.env}-redis"
  }
}
