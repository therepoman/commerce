provider "aws" {}

provider "aws" {
  alias = "backup"
}

variable "aws_profile" {
  type = "string"
}

variable "tag" {
  type    = "string"
  default = ""
}

variable "logging" {
  default     = "awslogs"
  description = "The logging driver to use (e.g., json, awslogs)"
}

variable "name" {
  type        = "string"
  description = "The name of the service"
}

variable "port" {
  default     = 3000
  description = "The port the container/application is serving traffic from."
}

variable "alb_port" {
  default     = 80
  description = "The port the load balancer is accepting traffic from."
}

variable "launch_type" {
  default     = "EC2"
  description = "EC2/Fargate"
}

variable "cluster" {
  description = "The ECS cluster name"
}

variable "security_group" {
  type = "string"
}

variable "ec2_instances_security_group" {
  type = "string"
}

variable "deployment" {
  type = "map"

  default = {
    minimum_percent = 100
    maximum_percent = 200
  }
}

variable "desired_count" {
  description = "Number of containers that should be running (autoscaling will override this)"
  default     = 3
}

variable "subnets" {
  type    = "string"
  default = ""
}

variable "subnet_ips" {
  type = "list"
  default = []
}

variable "env_vars" {
  type    = "list"
  default = []
}

variable "command" {
  type    = "list"
  default = []
}

variable "environment" {
  type        = "string"
  description = "The environment to run the service in (e.g, staging/production)"
}

variable "env" {
  description = "The AWS environment. Must begin with one of the following:  devo (development),  stage (staging), or  prod (production)."
}

variable "vpc_id" {
  type = "string"
}

variable "region" {
  default     = "us-west-2"
  description = "The AWS Region for the resources to live in"
}

variable "backup_region" {
  default     = "us-east-2"
  description = "The AWS Region for the resources to live in"
}

variable "cpu" {
  default     = 256
  description = "How many CPU units to use"
}

variable "memory" {
  default = 256
}

variable "canary_cpu" {
  default     = 256
  description = "How many CPU units to use for canary"
}

variable "canary_memory" {
  default = 256
}

variable "min_instances" {
  default = 3
}

variable "max_instances" {
  default = 3
}

variable "healthcheck" {
  type    = "string"
  default = "/ping"
}

variable "autoscaling_role" {
  description = "Role that will be used to perform dynamo autoscaling actions"
}

variable "dynamo_min_read_capacity" {
  description = "Min read capacity for dynamodb tables"
}

variable "dynamo_min_write_capacity" {
  description = "Min write capacity for dynamodb tables"
}

variable "whitelisted_arns_for_privatelink" {
  type    = "list"
  default = []
}

variable "min_aurora_capacity" {
  default = "4"
}

variable "max_aurora_capacity" {
  default = "16"
}

variable "sandstorm_role" {}

# AWS ElastiCache
variable "elasticache_instance_type" {
  description = "Describes the type of instance to be used in our elasticache cluster"
  default     = "cache.m4.large"
}

variable "elasticache_auto_failover" {
  description = "Describes the type of failover behavior for the elasticache groups"
  default     = true
}

variable "redis_port" {
  description = "The port used by the redis server"
  default     = 6379
}

variable "elasticache_replicas_per_node_group" {
  description = "Number of read replicas per elasticache node group"
}

variable "elasticache_num_node_groups" {
  description = "Number of elasticache node groups"
}

variable "tcs_user_name" {
  default = "teamredacted-devo-tcs-user"
}