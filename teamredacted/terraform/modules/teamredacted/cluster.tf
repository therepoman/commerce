module "cluster" {
  source = "git::git+ssh://git@git-aws.internal.justin.tv/subs/terracode//cluster?ref=58fba67847d80abb7e7b4d04219bc8845e6bcb3c"

  name           = "${var.cluster}"
  min            = "2"
  vpc_id         = "${var.vpc_id}"
  instance_type  = "t2.micro"
  environment    = "${var.environment}"
  subnets        = "${var.subnets}"
  security_group = "${var.ec2_instances_security_group}"
}
