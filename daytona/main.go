package main

import (
	"code.justin.tv/commerce/daytona/internal/cli"
)

func main() {
	cli.NewCLI().Init()
}
