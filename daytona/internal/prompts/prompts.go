package prompts

import (
	"errors"
	"fmt"
	"github.com/manifoldco/promptui"
	"strconv"
)

func PromptConfirmation() (string, error) {
	validate := func(input string) error {
		if input == "" {
			return errors.New("Confirmation cannot be blank")
		}
		return nil
	}

	prompt := promptui.Prompt{
		Label:    "Redrive? (y/n)",
		Validate: validate,
	}

	result, err := prompt.Run()

	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return "", err
	}

	return result, nil
}

func PromptMaxConcurrency() (int, error) {
	validate := func(input string) error {
		if input == "" {
			return nil
		}

		val, err := strconv.Atoi(input)
		if err != nil {
			return err
		}

		if val < 1 || val > 100 {
			return errors.New("out of range")
		}

		return nil
	}

	prompt := promptui.Prompt{
		Label: "Max concurrency? 1-100, blank == 1. Make sure your ulimit is sufficiently high if you pick a large number",
		Validate: validate,
	}

	result, err := prompt.Run()
	if err != nil {
		return 0, err
	}

	if result == "" {
		return 1, nil
	}

	val, err := strconv.Atoi(result)
	if err != nil {
		return 0, err
	}

	return val, nil
}

func PromptRateLimit() (int, error) {
	validate := func(input string) error {
		if input == "" {
			return nil
		}

		_, err := strconv.Atoi(input)
		if err != nil {
			return err
		}

		return nil
	}

	prompt := promptui.Prompt{
		Label:    "Max redrive TPS? blank == unlimited",
		Validate: validate,
	}

	result, err := prompt.Run()
	if err != nil {
		return 0, err
	}

	// unlimited
	if result == "" {
		return 0, nil
	}

	tps, err := strconv.Atoi(result)
	if err != nil {
		return 0, err
	}

	return tps, nil
}

