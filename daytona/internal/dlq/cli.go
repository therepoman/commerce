package dlq

import (
	"errors"
	"fmt"
	"os"

	"code.justin.tv/commerce/daytona/internal/args"
	"code.justin.tv/commerce/daytona/internal/prompts"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/manifoldco/promptui"
	log "github.com/sirupsen/logrus"
)

type DLQCLI interface {
	Init()
}

type cliImpl struct {
	DLQManager DLQManager
	SQSClient  *sqs.SQS
}

func NewDLQCLI(sqsClient *sqs.SQS) DLQCLI {
	return &cliImpl{
		SQSClient:  sqsClient,
		DLQManager: NewDLQManager(sqsClient),
	}
}

func promptQueueSubstring() (string, error) {
	prompt := promptui.Prompt{
		Label: "Queue Substring to Match? blank == *",
	}

	result, err := prompt.Run()

	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return "", err
	}

	return result, nil
}

func promptQueueSelection(dlqNames []string) (string, error) {
	prompt := promptui.Select{
		Label: "Queue to Redrive",
		Items: dlqNames,
	}

	_, result, err := prompt.Run()

	if err != nil {
		return "", err
	}

	return result, nil
}

func (c *cliImpl) getSourceQueue(selectedDLQ string) (string, error) {
	sourceQueues, err := c.DLQManager.GetDLQSourceQueueNames(selectedDLQ)

	if err != nil {
		return "", err
	}

	if len(sourceQueues) == 0 {
		return "", errors.New("Could not find a queue with a corresponding redrive policy. Are you sure you selected a DLQ?")
	}

	if len(sourceQueues) != 1 {
		return "", errors.New("DLQ must have exactly one source, aborting")
	}

	return sourceQueues[0], nil
}

func (c *cliImpl) startRedrive(selectedDLQ, sourceQueue string, approximateNumberOfMessages int64, maxTPS, maxConcurrency int) error {
	fmt.Println("Starting redrive...")
	messagesProcessed, err := c.DLQManager.Redrive(selectedDLQ, sourceQueue, approximateNumberOfMessages, maxTPS, maxConcurrency)
	fmt.Printf("%d of %d processed\n", messagesProcessed, approximateNumberOfMessages)

	if err != nil {
		return err
	}

	return nil
}

func (c *cliImpl) StepSelectDLQ(dlqs []string) {
	selectedDLQ, err := promptQueueSelection(dlqs)

	if err != nil {
		log.WithError(err).Error("queue prompt failed")
		os.Exit(1)
	}

	approximateNumberOfMessages, err := c.DLQManager.GetApproximateNumberOfMessages(selectedDLQ)

	if err != nil {
		log.WithError(err).Error("error getting count of messages in queue")
		os.Exit(1)
	}

	sourceQueue, err := c.getSourceQueue(selectedDLQ)
	if err != nil {
		log.WithError(err).Error("error getting source queue")
		c.StepFilter()
		return
	}

	fmt.Println("*********************")
	fmt.Printf("Selected DLQ: %s\n", selectedDLQ)
	fmt.Printf("Source Queue: %s\n", sourceQueue)
	fmt.Printf("Items in Queue: %d\n", approximateNumberOfMessages)
	fmt.Println("*********************")

	maxTPS, err := prompts.PromptRateLimit()
	if err != nil {
		log.WithError(err).Error("tps prompt failed")
		os.Exit(1)
	}

	maxConcurrency, err := prompts.PromptMaxConcurrency()
	if err != nil {
		log.WithError(err).Error("concurrency prompt failed")
		os.Exit(1)
	}

	redriveConfirmation, err := prompts.PromptConfirmation()

	if err != nil {
		log.WithError(err).Error("Confirmation prompt failed")
		os.Exit(1)
	}

	if redriveConfirmation != "y" {
		fmt.Println("Aborting redrive")
		c.StepFilter()
		return
	}

	err = c.startRedrive(selectedDLQ, sourceQueue, approximateNumberOfMessages, maxTPS, maxConcurrency)
	if err != nil {
		log.WithError(err).Error("redrive failed")
		c.StepFilter()
		return
	}

	fmt.Println("Redrive success. Choose another")
	c.StepFilter()
}

func (c *cliImpl) StepList(substring string) {
	dlqs, err := c.DLQManager.ListDeadletterQueues(substring)

	if err != nil {
		log.WithError(err).Error("error listing DLQs")
		os.Exit(1)
	}

	if len(dlqs) == 0 {
		fmt.Println("No queues match the provided pattern")
		c.StepFilter()
		return
	}

	c.StepSelectDLQ(dlqs)
}

func (c *cliImpl) StepFilter() {
	dlqSubstring, err := promptQueueSubstring()

	if err != nil {
		log.WithError(err).Error("Queue substring prompt failed")
		os.Exit(1)
	}

	c.StepList(dlqSubstring)
}

func (c *cliImpl) RunInNonInteractiveMode() {
	selectedDLQ := *args.QueueUrl
	if selectedDLQ == "" {
		log.Error("must provide a DLQ queue arn")
		os.Exit(1)
	}

	sourceQueue, err := c.getSourceQueue(selectedDLQ)
	if err != nil {
		log.WithError(err).Error("error getting source queue")
		os.Exit(1)
	}

	approximateNumberOfMessages, err := c.DLQManager.GetApproximateNumberOfMessages(selectedDLQ)

	if err != nil {
		log.WithError(err).Error("error getting count of messages in queue")
		os.Exit(1)
	}

	maxTPS := *args.MaxTPS
	maxConcurrency := *args.MaxRedriveConcurrency

	err = c.startRedrive(selectedDLQ, sourceQueue, approximateNumberOfMessages, maxTPS, maxConcurrency)
	if err != nil {
		log.WithError(err).Error("redrive failed")
		os.Exit(1)
	}

	os.Exit(0)
}

func (c *cliImpl) Init() {
	if !*args.Interactive {
		c.RunInNonInteractiveMode()
		return
	}

	c.StepFilter()
}
