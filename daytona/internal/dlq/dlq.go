package dlq

import (
	"context"
	"errors"
	"fmt"
	"math"
	"strconv"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	pb "github.com/cheggaaa/pb/v3"
	log "github.com/sirupsen/logrus"
	"golang.org/x/time/rate"
)

const (
	dlqMessageBatchMultiplier = 5
	maxMessagesPerReceive     = 10
)

type DLQManager interface {
	ListDeadletterQueues(substring string) ([]string, error)
	GetApproximateNumberOfMessages(queueURL string) (int64, error)
	GetDLQSourceQueueNames(queueURL string) ([]string, error)
	Redrive(dlqURL, sourceQueueURL string, approximateNumberOfMessages int64, maxTPS, maxConcurrency int) (messagesProcessed int64, err error)
}

type dlqManagerImpl struct {
	SQSClient *sqs.SQS
}

func NewDLQManager(sqsClient *sqs.SQS) DLQManager {
	return &dlqManagerImpl{
		SQSClient: sqsClient,
	}
}

func (s *dlqManagerImpl) ListDeadletterQueues(substring string) ([]string, error) {
	substring = strings.ToLower(substring)

	var dlqURLs []string
	var nextToken *string
	maxResults := int64(1000)

	for {
		output, err := s.SQSClient.ListQueues(&sqs.ListQueuesInput{MaxResults: &maxResults, NextToken: nextToken})

		if err != nil {
			return nil, err
		}

		for _, url := range output.QueueUrls {
			if url == nil {
				continue
			}

			// TODO: support fifo queues. Should we redrive with the same deduplicationID?
			if strings.HasSuffix(*url, "fifo") {
				continue
			}

			if substring != "" {
				lower := strings.ToLower(*url)

				if strings.Contains(lower, substring) {
					dlqURLs = append(dlqURLs, *url)
				}
			} else {
				dlqURLs = append(dlqURLs, *url)
			}
		}

		if nextToken = output.NextToken; nextToken == nil {
			break
		}
	}

	return dlqURLs, nil
}

func (s *dlqManagerImpl) GetApproximateNumberOfMessages(queueURL string) (int64, error) {
	output, err := s.SQSClient.GetQueueAttributes(&sqs.GetQueueAttributesInput{
		AttributeNames: []*string{aws.String(sqs.QueueAttributeNameApproximateNumberOfMessages)},
		QueueUrl:       aws.String(queueURL),
	})

	if err != nil {
		return 0, err
	}

	val, ok := output.Attributes[sqs.QueueAttributeNameApproximateNumberOfMessages]

	if !ok || val == nil {
		return 0, errors.New("GetQueueAttributes missing ApproximateNumberOfMessages")
	}

	count, err := strconv.ParseInt(*val, 10, 64)

	if err != nil {
		return 0, errors.New("error parsing ApproximateNumberOfMessages")
	}

	return count, nil
}

func (s *dlqManagerImpl) GetDLQSourceQueueNames(queueURL string) ([]string, error) {
	output, err := s.SQSClient.ListDeadLetterSourceQueues(&sqs.ListDeadLetterSourceQueuesInput{
		QueueUrl: aws.String(queueURL),
	})

	if err != nil {
		return nil, err
	}

	var urls []string

	for _, url := range output.QueueUrls {
		if url == nil {
			continue
		}

		urls = append(urls, *url)
	}

	return urls, nil
}

// Returns up to 5x the number of messages as we have workers. We need redrive fast
// enough to avoid hitting the DLQ visibility timeout.
func (s *dlqManagerImpl) accumulateDLQMessages(dlqURL string, maxConcurrency int) ([]*sqs.Message, error) {
	maxBatchSize := dlqMessageBatchMultiplier * int(math.Max(float64(maxConcurrency), float64(maxMessagesPerReceive)))
	var messages []*sqs.Message

	for {
		receiveOutput, err := s.SQSClient.ReceiveMessage(&sqs.ReceiveMessageInput{
			QueueUrl:            aws.String(dlqURL),
			MaxNumberOfMessages: aws.Int64(maxMessagesPerReceive),
			VisibilityTimeout:   aws.Int64(60),
		})

		if err != nil {
			return messages, err
		}

		messages = append(messages, receiveOutput.Messages...)

		messagesReceived := len(receiveOutput.Messages)

		// All done
		if messagesReceived == 0 || len(messages) >= maxBatchSize {
			break
		}
	}

	return messages, nil
}

type Result struct {
	err       error
	messageID string
}

// Could batch send and delete message, but this is pretty fast, and much easier to read/maintain
// when you don't have to worry about partial batch failures.
func (s *dlqManagerImpl) redriveItem(dlqURL, sourceQueueURL string, message *sqs.Message) Result {
	if message == nil {
		return Result{err: errors.New("nil message")}
	}

	// TODO: need to figure out what to do with deduplicationIDs for fifo queues
	_, err := s.SQSClient.SendMessage(&sqs.SendMessageInput{
		QueueUrl:          aws.String(sourceQueueURL),
		MessageAttributes: message.MessageAttributes,
		MessageBody:       message.Body,
	})

	if err != nil {
		return Result{err: err, messageID: *message.MessageId}
	}

	_, err = s.SQSClient.DeleteMessage(&sqs.DeleteMessageInput{
		QueueUrl:      aws.String(dlqURL),
		ReceiptHandle: message.ReceiptHandle,
	})

	if err != nil {
		log.WithError(err).
			WithField("message", message).
			Error("!!Needs manual intervention if your target queue is not idempotent!! Successfully sent the message to the source queue, but failed to delete the DLQ message. Halting redrive.")
		panic(err)
	}

	return Result{messageID: *message.MessageId}
}

func (s *dlqManagerImpl) Redrive(dlqURL, sourceQueueURL string, approximateNumberOfMessages int64, maxTPS, maxConcurrency int) (messagesProcessed int64, err error) {
	var out []string
	ctx := context.Background()
	tpsLimit := rate.Limit(maxTPS)
	if maxTPS == 0 {
		tpsLimit = rate.Inf
	}

	limiter := rate.NewLimiter(tpsLimit, 1)
	bar := pb.StartNew(int(approximateNumberOfMessages))

	for {
		dlqMessages, err := s.accumulateDLQMessages(dlqURL, maxConcurrency)
		if err != nil {
			return 0, err
		}

		if len(dlqMessages) == 0 {
			break
		}

		jobCount := len(dlqMessages)
		var jobs = make(chan *sqs.Message, jobCount)
		var results = make(chan Result, jobCount)

		for w := 1; w <= maxConcurrency; w++ {
			go func() {
				for j := range jobs {
					_ = limiter.Wait(ctx)
					results <- s.redriveItem(dlqURL, sourceQueueURL, j)
					bar.Increment()
				}
			}()
		}
		for _, message := range dlqMessages {
			jobs <- message
		}
		close(jobs)

		for a := 1; a <= jobCount; a++ {
			result := <-results

			messagesProcessed++

			if result.err != nil {
				out = append(out, fmt.Sprintf("error: %v redriving message %s", result.err, result.messageID))
				continue
			}
		}
	}

	bar.Finish()

	for _, o := range out {
		fmt.Println(o)
	}

	return messagesProcessed, nil
}
