package utils

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

const (
	redriveSuffix = "redrive"
	legacyRedriveSuffix = ".redrive"
	maxExecutionIDLength = 80
)

var (
	redriveNamePattern = regexp.MustCompile(`^(.*)\.redrive.([0-9]+$)`)
)

func MakeRedriveName(oldExecutionName string) string {
	// We used to append .redrive with every new execution. Check for this naming convention first for
	// backward compat
	if strings.HasSuffix(oldExecutionName, legacyRedriveSuffix) {
		return fmt.Sprintf("%s%s", oldExecutionName, legacyRedriveSuffix)
	}

	matches := redriveNamePattern.FindStringSubmatch(oldExecutionName)

	if len(matches) == 0 || len(matches) != 3 {
		return truncatePrefixAndAppendSuffix(oldExecutionName, fmt.Sprintf(".%s.%d", redriveSuffix, 1), maxExecutionIDLength)
	}

	prefix := matches[1]
	currentRedriveCountStr := matches[2]
	currentRedriveCount, err := strconv.ParseInt(currentRedriveCountStr, 10, 64)
	if err != nil {
		currentRedriveCount = 1
	}

	suffix := fmt.Sprintf(".%s.%d", redriveSuffix, currentRedriveCount + 1)
	return truncatePrefixAndAppendSuffix(prefix, suffix, maxExecutionIDLength)
}

func truncatePrefixAndAppendSuffix(prefix string, suffix string, maxTotalLength int) string {
	maxPrefixLength := maxTotalLength - len(suffix)
	prefix = firstNCharacters(prefix, maxPrefixLength)
	return fmt.Sprintf("%s%s", prefix, suffix)
}

func firstNCharacters(s string, n int) string {
	truncatedS := ""
	for i, char := range s {
		if i >= n {
			break
		}
		truncatedS += string(char)
	}
	return truncatedS
}