package utils

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMakeRedriveName(t *testing.T)  {
	// One character truncated
	newName := MakeRedriveName("MMGC-189113000-300074846-83edb350-3618-45f2-b4e1-30f95f2e415b-413034491")
	assert.Len(t, newName, maxExecutionIDLength)
	assert.Equal(t, "MMGC-189113000-300074846-83edb350-3618-45f2-b4e1-30f95f2e415b-41303449.redrive.1", newName)

	// Count increments
	newName = MakeRedriveName("MMGC-189113000-300074846-83edb350-3618-45f2-b4e1-30f95f2e415b-41303449.redrive.1")
	assert.Len(t, newName, maxExecutionIDLength)
	assert.Equal(t, "MMGC-189113000-300074846-83edb350-3618-45f2-b4e1-30f95f2e415b-41303449.redrive.2", newName)

	// Count increments and truncates one character
	newName = MakeRedriveName("MMGC-189113000-300074846-83edb350-3618-45f2-b4e1-30f95f2e415b-41303449.redrive.9")
	assert.Len(t, newName, maxExecutionIDLength)
	assert.Equal(t, "MMGC-189113000-300074846-83edb350-3618-45f2-b4e1-30f95f2e415b-4130344.redrive.10", newName)
}

