package sfn

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"time"

	"code.justin.tv/commerce/daytona/internal/sfn/utils"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/cheggaaa/pb/v3"
	"golang.org/x/time/rate"
)

type SFNManager interface {
	ListStateMachines(substring string) ([]string, error)
	ListExecutionFailures(stateMachine string, lookAfter time.Time) ([]*sfn.ExecutionListItem, error)
	Redrive(stateMachine string, executions []*sfn.ExecutionListItem, maxTPS, maxConcurrency int) (messagesProcessed int64, err error)
}

type sfnManagerImpl struct {
	SFNClient *sfn.SFN
}

func NewSFNManager(sfnClient *sfn.SFN) SFNManager {
	return &sfnManagerImpl{
		SFNClient: sfnClient,
	}
}

func (s *sfnManagerImpl) ListStateMachines(substring string) ([]string, error) {
	output, err := s.SFNClient.ListStateMachines(&sfn.ListStateMachinesInput{
		MaxResults: aws.Int64(1000),
	})

	if err != nil {
		return nil, err
	}

	var stateMachines []string

	for _, stateMachine := range output.StateMachines {
		if stateMachine == nil {
			continue
		}

		if substring != "" {
			lower := strings.ToLower(*stateMachine.StateMachineArn)

			if strings.Contains(lower, substring) {
				stateMachines = append(stateMachines, *stateMachine.StateMachineArn)
			}
		} else {
			stateMachines = append(stateMachines, *stateMachine.StateMachineArn)
		}
	}

	return stateMachines, nil
}

func (s *sfnManagerImpl) ListExecutionFailures(stateMachine string, lookAfter time.Time) ([]*sfn.ExecutionListItem, error) {
	var out []*sfn.ExecutionListItem

	err := s.SFNClient.ListExecutionsPages(&sfn.ListExecutionsInput{
		MaxResults:      aws.Int64(1000),
		StateMachineArn: aws.String(stateMachine),
		StatusFilter:    aws.String("FAILED"),
	}, func(page *sfn.ListExecutionsOutput, b bool) bool {
		out = append(out, page.Executions...)

		t := page.Executions[len(page.Executions)-1].StartDate
		return t != nil && lookAfter.Before(*t)
	})

	if err != nil {
		return nil, err
	}

	return out, nil
}

type Result struct {
	oldExecutionName string
	newExecutionName string
	dupRedrive       bool
	newRedrive       bool
	err              error
}

func (s *sfnManagerImpl) redriveItem(stateMachine string, exec *sfn.ExecutionListItem) Result {
	if exec == nil {
		return Result{err: errors.New("nil execution")}
	}

	oldExecutionName := *exec.Name

	gehResp, err := s.SFNClient.GetExecutionHistory(&sfn.GetExecutionHistoryInput{
		ExecutionArn: exec.ExecutionArn,
		MaxResults:   aws.Int64(1),
	})
	if err != nil {
		return Result{err: err, oldExecutionName: oldExecutionName}
	}

	input := gehResp.Events[0].ExecutionStartedEventDetails.Input

	redriveName := utils.MakeRedriveName(oldExecutionName)

	_, err = s.SFNClient.StartExecution(&sfn.StartExecutionInput{
		Input:           input,
		Name:            aws.String(redriveName),
		StateMachineArn: aws.String(stateMachine),
	})
	if err != nil {
		awsErr, ok := err.(awserr.Error)
		if !ok {
			return Result{err: err, oldExecutionName: oldExecutionName}
		}
		if awsErr.Code() == sfn.ErrCodeExecutionAlreadyExists {
			return Result{dupRedrive: true, oldExecutionName: oldExecutionName}
		} else {
			return Result{err: err, oldExecutionName: oldExecutionName}
		}
	}

	return Result{newRedrive: true, oldExecutionName: oldExecutionName, newExecutionName: redriveName}
}

func (s *sfnManagerImpl) Redrive(stateMachine string, executions []*sfn.ExecutionListItem, maxTPS, maxConcurrency int) (messagesProcessed int64, err error) {
	var out []string
	ctx := context.Background()

	tpsLimit := rate.Limit(maxTPS)
	if maxTPS == 0 {
		tpsLimit = rate.Inf
	}

	limiter := rate.NewLimiter(tpsLimit, 1)
	bar := pb.StartNew(len(executions))

	jobCount := len(executions)
	var jobs = make(chan *sfn.ExecutionListItem, jobCount)
	var results = make(chan Result, jobCount)

	for w := 1; w <= maxConcurrency; w++ {
		go func() {
			for j := range jobs {
				_ = limiter.Wait(ctx)
				results <- s.redriveItem(stateMachine, j)
				bar.Increment()
			}
		}()
	}
	for _, exec := range executions {
		jobs <- exec
	}
	close(jobs)

	errCount := 0
	newRedriveCount := 0
	dupRedriveCount := 0

	for a := 1; a <= jobCount; a++ {
		result := <-results

		messagesProcessed++

		if result.err != nil {
			out = append(out, fmt.Sprintf("error: %v redriving execution %s", result.err, result.oldExecutionName))
			errCount++
			continue
		}

		if result.newRedrive {
			out = append(out, fmt.Sprintf("created redrive: %s", result.newExecutionName))
			newRedriveCount++
			continue
		}

		if result.dupRedrive {
			out = append(out, fmt.Sprintf("ignored execution, already redriven: %s", result.oldExecutionName))
			dupRedriveCount++
			continue
		}
	}

	bar.Finish()

	for _, o := range out {
		fmt.Println(o)
	}

	fmt.Println("*********************")
	fmt.Println("Summary")
	fmt.Printf("Redrove: %d\n", newRedriveCount)
	fmt.Printf("Failed: %d\n", errCount)
	fmt.Printf("Duplicate: %d\n", dupRedriveCount)
	fmt.Println("*********************")

	return messagesProcessed, nil
}
