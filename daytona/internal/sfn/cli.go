package sfn

import (
	"errors"
	"fmt"
	"os"
	"time"

	"code.justin.tv/commerce/daytona/internal/args"
	"code.justin.tv/commerce/daytona/internal/prompts"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/manifoldco/promptui"
	log "github.com/sirupsen/logrus"
)

type DLQCLI interface {
	Init()
}

type cliImpl struct {
	SFNManager SFNManager
	SFNClient  *sfn.SFN
}

func NewSFNCLI(sfnClient *sfn.SFN) DLQCLI {
	return &cliImpl{
		SFNClient:  sfnClient,
		SFNManager: NewSFNManager(sfnClient),
	}
}

func (c *cliImpl) listExecutionsInTimeWindow(selectedStateMachine string, window time.Duration) ([]*sfn.ExecutionListItem, error) {
	lookForFailuresAfter := time.Now().Add(-window)

	executions, err := c.SFNManager.ListExecutionFailures(selectedStateMachine, lookForFailuresAfter)
	if err != nil {
		return nil, err
	}

	var executionsAfterDate []*sfn.ExecutionListItem

	for _, exec := range executions {
		if exec.StartDate.After(lookForFailuresAfter) {
			executionsAfterDate = append(executionsAfterDate, exec)
		}
	}

	return executionsAfterDate, nil
}

func (c *cliImpl) startRedrive(selectedStateMachine string, windowExecutions []*sfn.ExecutionListItem, maxTPS, maxConcurrency int) error {
	messagesProcessed, err := c.SFNManager.Redrive(selectedStateMachine, windowExecutions, maxTPS, maxConcurrency)
	fmt.Printf("%d of %d processed\n", messagesProcessed, len(windowExecutions))

	if err != nil {
		return err
	}

	return nil
}

func (c *cliImpl) StepPromptTimeWindow(selectedStateMachine string) {
	window, err := promptExecutionFailureTimeWindow()
	if err != nil {
		log.WithError(err).Error("minutes ago prompt failed")
		os.Exit(1)
	}

	windowExecutions, err := c.listExecutionsInTimeWindow(selectedStateMachine, window)
	if err != nil {
		log.WithError(err).Error("failed to list executions in time window")
		c.StepPromptTimeWindow(selectedStateMachine)
		return
	}

	if len(windowExecutions) == 0 {
		log.Info("Could not find executions after the provided time")
		c.StepPromptTimeWindow(selectedStateMachine)
		return
	}

	fmt.Println("*********************")
	fmt.Printf("Selected State Machine: %s\n", selectedStateMachine)
	fmt.Printf("Failed Executions: %d\n", len(windowExecutions))
	fmt.Println("*********************")

	maxTPS, err := prompts.PromptRateLimit()
	if err != nil {
		log.WithError(err).Error("tps prompt failed")
		os.Exit(1)
	}

	maxConcurrency, err := prompts.PromptMaxConcurrency()
	if err != nil {
		log.WithError(err).Error("concurrency prompt failed")
		os.Exit(1)
	}

	redriveConfirmation, err := prompts.PromptConfirmation()
	if err != nil {
		log.WithError(err).Error("confirmation prompt failed")
		os.Exit(1)
	}

	if redriveConfirmation != "y" {
		fmt.Println("Aborting redrive")
		c.StepPromptTimeWindow(selectedStateMachine)
		return
	}

	err = c.startRedrive(selectedStateMachine, windowExecutions, maxTPS, maxConcurrency)

	if err != nil {
		log.WithError(err).Error("redrive failed")
		c.StepPromptTimeWindow(selectedStateMachine)
		return
	}

	fmt.Println("Redrive success. Choose another")
	c.StepPromptTimeWindow(selectedStateMachine)
}

func (c *cliImpl) StepSelectStateMachine(stateMachines []string) {
	selectedStateMachine, err := promptStateMachineSelection(stateMachines)

	if err != nil {
		log.WithError(err).Error("state machine prompt failed")
		os.Exit(1)
	}

	c.StepPromptTimeWindow(selectedStateMachine)
}

func (c *cliImpl) StepListStateMachines(sfnSubstring string) {
	stateMachines, err := c.SFNManager.ListStateMachines(sfnSubstring)

	if err != nil {
		log.WithError(err).Error("error listing state machines")
		os.Exit(1)
	}

	if len(stateMachines) == 0 {
		fmt.Println("No state machines match the provided pattern")
		c.StepFilterStateMachines()
		return
	}

	c.StepSelectStateMachine(stateMachines)
}

func (c *cliImpl) StepFilterStateMachines() {
	sfnSubstring, err := promptSFNSubstring()

	if err != nil {
		log.WithError(err).Error("state machine substring prompt failed")
		os.Exit(1)
	}

	c.StepListStateMachines(sfnSubstring)
}

func (c *cliImpl) RunInNonInteractiveMode() {
	selectedStateMachine := *args.StateMachineArn
	if selectedStateMachine == "" {
		log.Error("must provide a state machine arn")
		os.Exit(1)
	}

	windowRaw := *args.Window
	if windowRaw == "" {
		log.Error("must provide a time window to search for state machine errors")
		os.Exit(1)
	}

	window, err := time.ParseDuration(windowRaw)
	if err != nil {
		log.WithError(err).Error("error parsing time window as time.Duration")
		os.Exit(1)
	}

	maxTPS := *args.MaxTPS
	maxConcurrency := *args.MaxRedriveConcurrency

	windowExecutions, err := c.listExecutionsInTimeWindow(selectedStateMachine, window)
	if err != nil {
		log.WithError(err).Error("failed to list executions in time window")
		os.Exit(1)
	}

	if len(windowExecutions) == 0 {
		log.Info("Could not find executions after the provided time")
		os.Exit(0)
	}

	err = c.startRedrive(selectedStateMachine, windowExecutions, maxTPS, maxConcurrency)
	if err != nil {
		log.WithError(err).Error("redrive failed")
		os.Exit(1)
	}

	os.Exit(0)
}

func (c *cliImpl) Init() {
	if !*args.Interactive {
		c.RunInNonInteractiveMode()
		return
	}

	printUsageWarning()
	c.StepFilterStateMachines()
}

func promptSFNSubstring() (string, error) {
	validate := func(input string) error {
		if input == "" {
			return errors.New("substring cannot be blank")
		}
		return nil
	}

	prompt := promptui.Prompt{
		Label:    "Queue Substring to Match",
		Validate: validate,
	}

	result, err := prompt.Run()

	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return "", err
	}

	return result, nil
}

func promptStateMachineSelection(stateMachines []string) (string, error) {
	prompt := promptui.Select{
		Label: "State Machine to Redrive",
		Items: stateMachines,
	}

	_, result, err := prompt.Run()

	if err != nil {
		return "", err
	}

	return result, nil
}

func promptExecutionFailureTimeWindow() (time.Duration, error) {
	validate := func(input string) error {
		if input == "" {
			return errors.New("duration cannot be blank")
		}

		_, err := time.ParseDuration(input)
		if err != nil {
			return err
		}

		return nil
	}

	prompt := promptui.Prompt{
		Label:    "How far back should I search (1s, 1m, 1h)?",
		Validate: validate,
	}

	result, err := prompt.Run()

	if err != nil {
		return 0, err
	}

	dur, err := time.ParseDuration(result)
	if err != nil {
		return 0, err
	}

	return dur, nil
}

func printUsageWarning() {
	fmt.Println(`!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! The SFN redrive tool avoids duplicate redrives by        !
! creating a new state machine with a .redrive.N suffix    !
! (leveraging SFN execution name idempotency). Redrive     !
! creation with some other naming scheme should be         !
! avoided (be it manual or otherwise) when using this tool !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!`)
}
