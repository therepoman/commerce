package args

import (
	"flag"
)

var (
	AwsRegion = flag.String("r", "", "AWS Region")
	AwsProfile = flag.String("p", "", "AWS Profile")
	RedriveType = flag.String("t", "", "Redrive type (SQS|SFN)")

	Interactive = flag.Bool("i", true, "Whether daytona should be started in interactive mode")

	// Required for non-interactive mode
	QueueUrl        = flag.String("qu", "", "[SQS] Queue URL")
	StateMachineArn = flag.String("sa", "", "[SFN] State machine ARN")
	Window          = flag.String("w", "", "[SFN] Time window to search for state machine failures (e.g. 1m, 5h)")
	MaxTPS          = flag.Int("tps", 0, "Max redrive TPS")
	MaxRedriveConcurrency = flag.Int("cc", 1, "Max concurrent goroutines to handle redrive work")
)


func init() {
	flag.Parse()
}