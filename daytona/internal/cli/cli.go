package cli

import (
	"code.justin.tv/commerce/daytona/internal/args"
	dlqcli "code.justin.tv/commerce/daytona/internal/dlq"
	sfncli "code.justin.tv/commerce/daytona/internal/sfn"
	"errors"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/manifoldco/promptui"
	log "github.com/sirupsen/logrus"
	"os"
)

const (
	SQSType = "SQS"
	SFNType = "SFN"
)

var validRedriveTypes = []string{
	SQSType,
	SFNType,
}

type CLI interface {
	Init()
}

type cliImpl struct {
	SQSClient *sqs.SQS
	SFNClient *sfn.SFN
}

func NewCLI() CLI {
	return &cliImpl{}
}

func promptAWSProfile() (string, error) {
	validate := func(input string) error {
		if input == "" {
			return errors.New("AWS_PROFILE cannot be blank")
		}
		return nil
	}

	prompt := promptui.Prompt{
		Label:    "AWS_PROFILE",
		Validate: validate,
	}

	result, err := prompt.Run()

	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return "", err
	}

	return result, nil
}

func promptRegion() (string, error) {
	prompt := promptui.Select{
		Label: "AWS Region",
		Items: []string{
			"us-west-2",
			"us-west-1",
			"us-east-2",
			"us-east-1",
			"ap-east-1",
			"ap-south-1",
			"ap-northeast-3",
			"ap-northeast-22",
			"ap-southeast-1",
			"ap-southeast-2",
			"ap-northeast-1",
			"ca-central-1",
			"eu-central-1",
			"eu-west-1",
			"eu-west-2",
			"eu-west-3",
			"eu-north-1",
			"me-south-1",
			"sa-east-1",
		},
	}

	_, result, err := prompt.Run()

	if err != nil {
		return "", err
	}

	return result, nil
}

func promptType() (string, error) {
	prompt := promptui.Select{
		Label: "",
		Items: validRedriveTypes,
	}

	_, result, err := prompt.Run()

	if err != nil {
		return "", err
	}

	return result, nil
}

func getAWSProfile() string {
	var awsProfile string

	if *args.AwsProfile != "" {
		awsProfile = *args.AwsProfile
	}

	if awsProfile == "" {
		if *args.Interactive {
			awsProfileFromPrompt, err := promptAWSProfile()

			if err != nil {
				log.WithError(err).Error("AWS_PROFILE prompt failed")
				os.Exit(1)
			}

			awsProfile = awsProfileFromPrompt
		} else {
			log.Error("must provide an AWS profile")
			os.Exit(1)
		}
	}

	return awsProfile
}

func getRegion() string {
	var region string

	if *args.AwsRegion != "" {
		region = *args.AwsRegion
	}

	if region == "" {
		if *args.Interactive {
			regionFromPrompt, err := promptRegion()

			if err != nil {
				log.WithError(err).Error("region prompt failed")
				os.Exit(1)
			}

			region = regionFromPrompt
		} else {
			log.Error("must provide a region")
			os.Exit(1)
		}
	}

	return region
}

func getRedriveType() string {
	var redriveType string

	if *args.RedriveType != "" {
		redriveType = *args.RedriveType
	}

	if redriveType == "" {
		if *args.Interactive {
			redriveTypeFromPrompt, err := promptType()

			if err != nil {
				log.WithError(err).Error("redriveType prompt failed")
				os.Exit(1)
			}

			redriveType = redriveTypeFromPrompt
		} else {
			log.Error("must provide a valid redrive type")
			os.Exit(1)
		}
	}

	return redriveType
}

func (c *cliImpl) Init() {
	awsProfile := getAWSProfile()

	err := os.Setenv("AWS_PROFILE", awsProfile)

	if err != nil {
		log.WithError(err).Error("error setting AWS_PROFILE")
		os.Exit(1)
	}

	region := getRegion()

	awsConfig := aws.NewConfig().WithRegion(region)
	sess, err := session.NewSession(awsConfig)

	if err != nil {
		log.WithError(err).Error("error creating new AWS session")
		os.Exit(1)
	}

	c.SQSClient = sqs.New(sess, awsConfig)
	c.SFNClient = sfn.New(sess, awsConfig)

	dlqCLI := dlqcli.NewDLQCLI(c.SQSClient)
	sfnCLI := sfncli.NewSFNCLI(c.SFNClient)

	redriveType := getRedriveType()

	switch redriveType {
	case SQSType:
		dlqCLI.Init()
	case SFNType:
		sfnCLI.Init()
	default:
		log.WithError(err).Error("unexpected redrive type")
		os.Exit(1)
	}

}