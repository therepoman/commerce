# daytona

Redriving DLQ messages and state machines via the console is a manual, painful, and time-consuming process. Daytona is an interactive CLI tool that makes this process much faster and simpler.  

![Daytona Demo](assets/daytona.gif)

## Usage

* Make sure the AWS account where the target resource lives is represented in your `~/.aws/credentials` file
* Run `./bin/daytona`
* Enter the `AWS_PROFILE` where the resource lives (as it appears in your `~/.aws/credentials` file)
* Enter the region where the resource lives

For SQS
* Enter a substring by which the target queue can be identified (or skip to list all queues)
* Select the queue from the list of matches. If a queue with a corresponding redrive policy is not found, this will fail.
* Confirm that you would like to redrive the queue
* Fin!

For SFN
* Enter a substring by which the target state machine can be identified
* Select the state machine from the list of matches
* Enter a time window to search for execution failures (time.Duration format)
* [Optional] Enter a max redrive TPS (blank == unlimited)
* Confirm that you would like to redrive the state machine failures
* Fin!

**Ctrl-c at any time to exit the app**

## Non-interactive mode and CLI Args

By default, daytona will start up in interactive mode. However, this can be avoided by providing the `-i=false` switch (in which case all arguments must be provided up front).

CLI Args

```
Usage of ./bin/daytona:
  -cc int
    	Max concurrent goroutines to handle redrive work (default 1)
  -i	Whether daytona should be started in interactive mode (default true)
  -p string
    	AWS Profile
  -qu string
    	[SQS] Queue URL
  -r string
    	AWS Region
  -sa string
    	[SFN] State machine ARN
  -t string
    	Redrive type (SQS|SFN)
  -tps int
    	Max redrive TPS
  -w string
    	[SFN] Time window to search for state machine failures (e.g. 1m, 5h)
```

Non-interactive mode usage

```bash
# Non-interactive SQS
./bin/daytona -i=false -p=pagle-devo -r=us-west-2 -t=SQS -qu=https://sqs.us-west-2.amazonaws.com/574977834358/staging-condition-participant-cleanup-queue_deadletter

# Non-interactive SFN
./bin/daytona -i=false -p=pagle-devo -r=us-west-2 -t=SFN -sa=arn:aws:states:us-west-2:574977834358:stateMachine:condition_run_processing_state_machine -w=100h
```

As a convenience, users can optionally specify aws_profile, region, and redrive type upfront to skip their respective prompts in interactive mode

```bash
# Interactive, but bypasses aws_profile prompt
./bin/daytona -p=pagle-devo

# Interactive, but bypasses aws_profile and region prompt
./bin/daytona -p=pagle-devo -r=us-west-2

# Interactive, but bypasses aws_profile, region prompt, and redrive type prompt
./bin/daytona -p=pagle-devo -r=us-west-2 -t=SQS
```

## TODO

`fifo` queues not currently supported