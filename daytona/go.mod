module code.justin.tv/commerce/daytona

go 1.14

require (
	github.com/aws/aws-sdk-go v1.37.27
	github.com/cheggaaa/pb/v3 v3.0.4
	github.com/fatih/color v1.9.0 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/lunixbochs/vtclean v1.0.0 // indirect
	github.com/manifoldco/promptui v0.7.0
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/mattn/go-runewidth v0.0.9-0.20200125155203-588506649b41 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.6.1
	golang.org/x/time v0.0.0-20200416051211-89c76fbcd5d1
)
