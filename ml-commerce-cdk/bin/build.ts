#!/usr/bin/env node

import { getLatestPluginVersion } from "conductor-managed-airflow-environment";
import { App } from "monocdk";
import { AwsAccountConfig, awsCommerceScience, fraudBeta, fraudProd } from "../lib/config";
import { InfraStack } from "../lib/stacks/infra";
import { LocustCdkFargateStack } from "../lib/stacks/locust";
import { MwaaClusterStack } from "../lib/stacks/mwaa_cluster";
import { MwaaResourcesStack } from "../lib/stacks/mwaa_resource";

const accountMap: { [index: string]: AwsAccountConfig } = {
  shared: awsCommerceScience,
  fraud_beta: fraudBeta,
  fraud_prod: fraudProd,
}

const env = process.env.ENV;
const accountName = process.env.ACCOUNT;
const stackName = process.env.STACK;

if (env != "beta" && env != "prod") {
  console.log(`invalid env: ${env}`);
  process.exit(1);
}

if (!accountName || !accountMap[accountName]) {
  console.log(`unknown account: ${accountName}`);
  process.exit(1);
}

const account = accountMap[accountName];

const prefix = `commerce-ml-${account.modelName}-${env}`;
const commonProps = {
  prefix,
  account,
  env,
};

const app = new App({});

const stackMap: { [index: string]: () => Promise<void> } = {
  infra: async () => { console.log('in infra'); new InfraStack(app, `${prefix}-infra`, commonProps) },
  mwaa_resources: async () => { new MwaaResourcesStack(app, `${prefix}-mwaa-resources`, commonProps) },
  mwaa_cluster: async () => {
    // TODO: DRY
    const resourceStack = new MwaaResourcesStack(app, `${prefix}-mwaa-resources`, commonProps)
    new MwaaClusterStack(app, `${prefix}-mwaa-cluster`, {
      ...commonProps,
      resources: resourceStack.resources,
      pluginsVersion: await getLatestPluginVersion(resourceStack.resources.bucketName, "plugins.zip"),
      requirementsVersion: await getLatestPluginVersion(resourceStack.resources.bucketName, "requirements.txt"),
    })
  },
  locust: async () => { new LocustCdkFargateStack(app, `${prefix}-locust`, commonProps) },
};

if (!stackName || !stackMap[stackName]) {
  console.log(`unknown stack name: ${stackName}`);
  process.exit(1);
}

console.log(`deploying ${stackName} to ${accountName}:${env}`);

(async () => {
  await stackMap[stackName]();
})().catch(err => {
  if (err.diagnosticText) {
    console.log(err.diagnosticText);
  } else {
    console.log(err);
  }
});