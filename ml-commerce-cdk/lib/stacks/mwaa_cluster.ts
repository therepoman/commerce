import { ConductorMwaaClusterEnvironment, MwaaAuxiliaryResources } from "conductor-managed-airflow-environment";
import { Construct, Stack } from "monocdk";
import { AwsAccountConfig } from "../config";
import { getSg, getVpc } from "../shared";

export interface MwaaClusterProps {
  resources: MwaaAuxiliaryResources,
  account: AwsAccountConfig,
  prefix: string,
  env: string,
  pluginsVersion: string,
  requirementsVersion: string,
}

export class MwaaClusterStack extends Stack {
  constructor(scope: Construct, id: string, props: MwaaClusterProps) {
    super(scope, id, {
      env: {
        region: props.account.region,
        account: props.account.accountId,
      },
      terminationProtection: props.env == "prod",
    });

    const stack = this;
    const vpc = getVpc(stack, props.account);
    const sg = getSg(stack, props.account);

    const subnetIds = vpc.privateSubnets.map(subnet => subnet.subnetId);

    const mwaa = new ConductorMwaaClusterEnvironment(stack, `mwaa-cluster`, {
      auxiliaryResources: props.resources,
      subnetIds: subnetIds.slice(0, 2),
      securityGroupIds: [sg.securityGroupId],
      pluginsS3ObjectVersion: props.pluginsVersion,
      requirementsS3ObjectVersion: props.requirementsVersion,
      environmentClass: "mw1.large",
      airflowVersion: "2.0.2",
      maxWorkers: 10,
    });
    mwaa.mwaaEnvironment.webserverAccessMode = "PUBLIC_ONLY"
  }
}