import * as cdk from 'monocdk';
import * as ec2 from 'monocdk/aws-ec2';
import * as ecs from 'monocdk/aws-ecs';
import * as ecs_patterns from 'monocdk/aws-ecs-patterns';
import * as path from 'path';
import { AwsAccountConfig } from "../config";
import { getSg, getVpc } from '../shared';

export interface InfraStackProps {
  account: AwsAccountConfig,
  env: string,
}

export class LocustCdkFargateStack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props: InfraStackProps) {
    super(scope, id, {
      env: {
        region: props.account.region,
        account: props.account.accountId,
      },
      terminationProtection: props.env == "prod",
    });

    const stack = this;
    const vpc = getVpc(stack, props.account);
    const sg = getSg(stack, props.account);

    // ECS Cluster
    const cluster = new ecs.Cluster(this, 'LocustFargateCluster', {
      vpc: vpc,
    });

    // Setup a Service Discovery Namespace to the Fargate cluster
    const privateDomainName = 'locust.local'
    cluster.addDefaultCloudMapNamespace({
      name: privateDomainName
    });

    // Task Definition(s)
    const primary_taskDefinition = new ecs.FargateTaskDefinition(this, 'LocustPrimaryTaskDefinition', {
      memoryLimitMiB: 8192,
      cpu: 4096,
    });

    const primary_container = primary_taskDefinition.addContainer('LocustPrimaryContainer', {
      image: ecs.ContainerImage.fromAsset(path.join(__dirname, '..', 'locust/locust-container')),
      logging: ecs.LogDrivers.awsLogs({ streamPrefix: 'LocustPrimaryCdkFargate' }),
      command: ["--master"],
    });
    // Add port to container definition
    primary_container.addPortMappings({ containerPort: 8089 });
    primary_container.addPortMappings({ containerPort: 5557 });
    primary_container.addPortMappings({ containerPort: 5558 });

    const worker_taskDefinition = new ecs.FargateTaskDefinition(this, 'LocustWorkerTaskDefinition', {
      memoryLimitMiB: 8192,
      cpu: 4096
    });

    const worker_container = worker_taskDefinition.addContainer('LocustPrimaryContainer', {
      image: ecs.ContainerImage.fromAsset(path.join(__dirname, '..', 'locust-container')),
      logging: ecs.LogDrivers.awsLogs({ streamPrefix: 'LocustWorkerCdkFargate' }),
      command: ["--worker", "--master-host", "primary.locust.local"],
    });
    // Add port to container definition
    worker_container.addPortMappings({ containerPort: 8089 });
    worker_container.addPortMappings({ containerPort: 5557 });
    worker_container.addPortMappings({ containerPort: 5558 });

    // Increase Number of Open file ulimits
    worker_container.addUlimits({
      name: ecs.UlimitName.NOFILE,
      softLimit: 65535,
      hardLimit: 65535,
    });

    // Setup Locust Primary service
    const privatePrimaryServiceName = 'primary'
    const primaryloadBalancedFargateService = new ecs_patterns.ApplicationLoadBalancedFargateService(this, 'LocustPrimary', {
      platformVersion: ecs.FargatePlatformVersion.VERSION1_4,
      cluster,
      memoryLimitMiB: 8192,
      cpu: 4096,
      desiredCount: 1,
      taskDefinition: primary_taskDefinition,
      openListener: false,
      vpc,
      securityGroups: [sg],
      cloudMapOptions: {
        name: privatePrimaryServiceName
      },
      serviceName: 'locust'
    });

    // We have port 8089 exposed on the LB but also want our workers to access 5557/5558
    primaryloadBalancedFargateService.service.connections.securityGroups[0].addIngressRule(
      ec2.Peer.ipv4(vpc.vpcCidrBlock),
      ec2.Port.tcpRange(5557, 5558),
      "Allow Locust workers to primary connections"
    );

    // Locust Worker Service - no ALB requried
    const privateWorkerServiceName = 'worker'
    const workerFargateService = new ecs.FargateService(this, "LocustWorkers", {
      platformVersion: ecs.FargatePlatformVersion.VERSION1_4,
      cluster: cluster,
      taskDefinition: worker_taskDefinition,
      desiredCount: 10,
      assignPublicIp: false,
      cloudMapOptions: {
        name: privateWorkerServiceName
      },
    });

    workerFargateService.connections.securityGroups[0].addIngressRule(
      ec2.Peer.ipv4(vpc.vpcCidrBlock),
      ec2.Port.tcpRange(5557, 5558),
      "Allow Locust primary to workers connections"
    );
  }
}
