import * as cdk from 'monocdk';
import * as ec2 from 'monocdk/aws-ec2';
import * as iam from 'monocdk/aws-iam';
import { AwsAccountConfig } from "../config";


export interface InfraStackProps {
  account: AwsAccountConfig,
  env: string,
}

export class InfraStack extends cdk.Stack {
  readonly vpc: ec2.Vpc;
  readonly sg: ec2.SecurityGroup;
  readonly awsVpcEndpointsConnections: ec2.Connections;

  constructor(scope: cdk.Construct, id: string, props: InfraStackProps) {
    super(scope, id, {
      env: {
        region: props.account.region,
        account: props.account.accountId,
      },
      terminationProtection: props.env == "prod",
    });

    const stack = this;

    this.vpc = new ec2.Vpc(stack, 'Resource', {
      enableDnsHostnames: true,
      enableDnsSupport: true,
      defaultInstanceTenancy: ec2.DefaultInstanceTenancy.DEFAULT,
      maxAzs: 3,
    });

    cdk.Tags.of(this.vpc).add('Name', 'primary-vpc');

    this.sg = new ec2.SecurityGroup(stack, 'AwsVpcEndpointsSg', {
      vpc: this.vpc,
      description: 'Controls access to all AWS services with VPC Endpoints within the VPC',
    });

    cdk.Tags.of(this.sg).add('Name', 'primary-sg');

    this.awsVpcEndpointsConnections = new ec2.Connections({
      securityGroups: [
        this.sg,
      ],
      defaultPort: ec2.Port.allTcp(),
    });

    this.awsVpcEndpointsConnections.connections.allowDefaultPortFrom(
      ec2.Peer.ipv4(this.vpc.vpcCidrBlock),
      'Allow all traffic from the VPC',
    );

    attachRecommendedAwsVpcEndpoints(this.vpc, this.awsVpcEndpointsConnections.securityGroups)

    cdk.Aspects.of(this.node.root).add(
      new AwsServiceVpcEndpointAutoConnector(this.awsVpcEndpointsConnections, this.vpc),
    );

    const jenkinsRole = new iam.Role(this, 'JenkinsRole', {
      roleName: "jenkins-execution-role",
      assumedBy: new iam.CompositePrincipal(
        new iam.ArnPrincipal('arn:aws:iam::043714768218:role/jenkins-prod-slave'),
        new iam.ArnPrincipal('arn:aws:iam::043714768218:role/jenkins-prod-master'),
      ),
      managedPolicies: [iam.ManagedPolicy.fromAwsManagedPolicyName("AdministratorAccess")],
    });
  }
}

/**
 * This CDK Aspect connects all Security Groups in a VPC to another given
 * Security Group in that VPC. We use this to make sure AWS VPC Endpoints
 * are automatically acceessible from any host within the VPC.
 *
 * Failing to connect the AWS VPC Endpoint Security Group could lead to timeouts when trying
 * to talk to AWS services, as DNS points to internal IPs blocked by the host's Security Group.
 */
class AwsServiceVpcEndpointAutoConnector implements cdk.IAspect {
  readonly awsEndpointConnection: ec2.Connections;
  readonly vpc: ec2.Vpc;

  constructor(awsEndpointConnection: ec2.Connections, vpc: ec2.Vpc) {
    this.awsEndpointConnection = awsEndpointConnection;
    this.vpc = vpc;
  }

  public visit(node: cdk.IConstruct): void {
    if (node instanceof ec2.SecurityGroup) {
      // Dont link us to ourselves
      if (this.awsEndpointConnection.securityGroups.includes(node)) {
        return;
      }
      // Skip redundant rule creation.
      // awsEndpointConnection will always allow all traffic from within the VPC, and thus
      // inbound rule creation is not necessary. And if a security group has allowAllOutbound
      // set, it's not neccessary to create an outbound rule.
      if ((node as ec2.SecurityGroup).allowAllOutbound) {
        return;
      }
      // Need to reach into the L2 Construct here to get a reference to the VPC
      // that was pased in, and not just a token to the VPC of the Security Group itself.
      // e.g. (roughly) `sg.securityGroupVpcId -> GetAttr: SGID, VpcId`
      // while we want `sg.node.defaultchild.vpcId -> GetAttr: Vpc, VpcId`
      const sgVpc = (node.node.defaultChild as ec2.CfnSecurityGroup).vpcId;
      if (sgVpc == this.vpc.vpcId) {
        node.connections.allowTo(
          this.awsEndpointConnection,
          ec2.Port.allTraffic(),
          'Allow connections to AWS VPC Endpoints (S3, SQS, etc.)',
        );
      }
    }
  }
}

function vpcEndpointInterfaceOptions(
  service: ec2.IInterfaceVpcEndpointService,
  sgs: ec2.ISecurityGroup[],
): ec2.InterfaceVpcEndpointOptions {
  return {
    service: service,
    securityGroups: sgs,
    open: true,
    privateDnsEnabled: true,
  } as ec2.InterfaceVpcEndpointOptions;
}

function attachRecommendedAwsVpcEndpoints(vpc: ec2.IVpc, sgs: ec2.ISecurityGroup[]): void {
  const g = ec2.GatewayVpcEndpointAwsService;
  vpc.addGatewayEndpoint('S3Vpce', { service: g.S3 });
  vpc.addGatewayEndpoint('DynamoDbVpce', { service: g.DYNAMODB });
  // UPDATE FUNCTION DOCSTRING IF YOU ADD MORE

  const i = ec2.InterfaceVpcEndpointAwsService;
  vpc.addInterfaceEndpoint('CwLogsVpce', vpcEndpointInterfaceOptions(i.CLOUDWATCH_LOGS, sgs));
  vpc.addInterfaceEndpoint('CwMonitoringVpce', vpcEndpointInterfaceOptions(i.CLOUDWATCH, sgs));
  vpc.addInterfaceEndpoint('ElbVpce', vpcEndpointInterfaceOptions(i.ELASTIC_LOAD_BALANCING, sgs));
  vpc.addInterfaceEndpoint('KinesisVpce', vpcEndpointInterfaceOptions(i.KINESIS_STREAMS, sgs));
  vpc.addInterfaceEndpoint('KmsVpce', vpcEndpointInterfaceOptions(i.KMS, sgs));
  vpc.addInterfaceEndpoint('SecretsManagerVpce', vpcEndpointInterfaceOptions(i.SECRETS_MANAGER, sgs));
  vpc.addInterfaceEndpoint('EcsVpce', vpcEndpointInterfaceOptions(i.ECS, sgs));
  vpc.addInterfaceEndpoint('SnsVpce', vpcEndpointInterfaceOptions(i.SNS, sgs));
  vpc.addInterfaceEndpoint('SqsVpce', vpcEndpointInterfaceOptions(i.SQS, sgs));
  vpc.addInterfaceEndpoint('SsmVpce', vpcEndpointInterfaceOptions(i.SSM, sgs));
  vpc.addInterfaceEndpoint('StsVpce', vpcEndpointInterfaceOptions(i.STS, sgs));
  vpc.addInterfaceEndpoint('EcrApiVpce', vpcEndpointInterfaceOptions(i.ECR, sgs));
  vpc.addInterfaceEndpoint('EcrDockerVpce', vpcEndpointInterfaceOptions(i.ECR_DOCKER, sgs));
  vpc.addInterfaceEndpoint('EcsAgentVpce', vpcEndpointInterfaceOptions(i.ECS_AGENT, sgs));
  vpc.addInterfaceEndpoint('EcsTelemetryVpce', vpcEndpointInterfaceOptions(i.ECS_TELEMETRY, sgs));
  // UPDATE FUNCTION DOCSTRING IF YOU ADD MORE
}