import { MwaaAuxiliaryResources } from "conductor-managed-airflow-environment";
import { Construct, Stack } from "monocdk";
import { AwsAccountConfig } from "../config";

export interface MwaaResourcesProps {
  account: AwsAccountConfig,
  env: string,
  prefix: string
}

export class MwaaResourcesStack extends Stack {
  readonly resources: MwaaAuxiliaryResources;

  constructor(scope: Construct, id: string, props: MwaaResourcesProps) {
    super(scope, id, {
      env: {
        region: props.account.region,
        account: props.account.accountId,
      },
      terminationProtection: props.env == "prod",
    });

    const stack = this;

    this.resources = new MwaaAuxiliaryResources(
      stack,
      `mwaa-resources`,
      {
        prefix: props.prefix,
      }
    );
  }
}