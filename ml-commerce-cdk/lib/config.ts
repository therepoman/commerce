export interface AwsAccountConfig {
    accountId: string;
    modelName: string;
    vpcId?: string;
    sg?: string
    privateSubnet1?: string;
    privateSubnet2?: string;
    privateSubnet3?: string;
    region: string;
    keyName: string;
    pagerdutyKey: string;
}

interface InstanceConfig {
    instanceType: string;
}

export const awsCommerceScience: AwsAccountConfig = {
    modelName: 'shared',
    vpcId: 'vpc-07e6129be33d5e802',
    sg: 'sg-044d6c23dff4b86d8',
    privateSubnet1: 'subnet-0c2eb9ceb9f707724',
    privateSubnet2: 'subnet-0d61927ff7db4c939',
    privateSubnet3: 'subnet-0428260edd5a8d7a8',
    region: 'us-west-2',
    keyName: 'commerce-science-aws',
    accountId: '348802193101',
    pagerdutyKey: '7bc91e9856ae4d03d0499493287891e9'
};

export const fraudBeta: AwsAccountConfig = {
    modelName: 'fraud',
    region: 'us-west-2',
    keyName: 'twitch-commerce-science-fraud-beta',
    accountId: '911861225579',
    pagerdutyKey: '7bc91e9856ae4d03d0499493287891e9',
    vpcId: 'vpc-0051d9a3710fd4ad2',
    sg: 'sg-0339b525c89ad14dd',
    privateSubnet1: 'subnet-00da10457c1358810',
    privateSubnet2: 'subnet-0ed56f9480d1f1fab',
    privateSubnet3: 'subnet-0b835f908bec373c3',
};

export const fraudProd: AwsAccountConfig = {
    modelName: 'fraud',
    region: 'us-west-2',
    keyName: 'twitch-commerce-science-fraud-prod',
    accountId: '410109475354',
    pagerdutyKey: '7bc91e9856ae4d03d0499493287891e9',
    vpcId: 'vpc-038712789acbf338c',
    sg: 'sg-0928629cbfe4de823',
    privateSubnet1: 'subnet-089d5884ea4c46562',
    privateSubnet2: 'subnet-0c48d61a69ce1fe57',
    privateSubnet3: 'subnet-0fc02b3ed2b3e7d2e',
};