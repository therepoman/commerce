import { aws_ec2 as ec2, Stack } from "monocdk";
import { AwsAccountConfig } from "./config";

export function getVpc(stack: Stack, accountConfig: AwsAccountConfig): ec2.IVpc {
  if (
    !accountConfig.vpcId ||
    !accountConfig.privateSubnet1 ||
    !accountConfig.privateSubnet2 ||
    !accountConfig.privateSubnet3
  ) {
    console.log(`account ${accountConfig.keyName} does not have vpc / subnet details which are required by this stack.`);
    process.exit(1);
  }
  return ec2.Vpc.fromVpcAttributes(stack, 'vpc', {
    vpcId: accountConfig.vpcId!,
    availabilityZones: [`${accountConfig.region}a`, `${accountConfig.region}b`, `${accountConfig.region}c`],
    privateSubnetIds: [accountConfig.privateSubnet1!, accountConfig.privateSubnet2!, accountConfig.privateSubnet3!],
  });
}

export function getSg(stack: Stack, accountConfig: AwsAccountConfig): ec2.ISecurityGroup {
  if (
    !accountConfig.sg
  ) {
    console.log(`account ${accountConfig.keyName} does not have sg details which are required by this stack.`);
    process.exit(1);
  }

  return ec2.SecurityGroup.fromSecurityGroupId(stack, 'sg', accountConfig.sg);
}

