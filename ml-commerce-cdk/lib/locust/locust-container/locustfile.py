import time
import json
import boto3
import sys
import os
from locust import User, TaskSet, events, task, stats, constant_throughput

from mypy_boto3_sagemaker_runtime.client import SageMakerRuntimeClient

stats.PERCENTILES_TO_REPORT = list(
    map(float, os.getenv('PERCENTILES_TO_REPORT', '0.95,0.98,0.99,0.999,0.9999,1.0').split(sep=',')))

@events.init_command_line_parser.add_listener
def _(parser):
    parser.add_argument("--endpoint_name", type=str, env_var="ENDPOINT_NAME", default="nominis-rpr-p0-features-dev-purrep-realtime", help="the name of sagemaker endpoint to test")


class SagemakerClient():
    client: SageMakerRuntimeClient
    endpoint_name: str

    def __init__(self, client, endpoint_name):
        self.client = client
        self.endpoint_name = endpoint_name

    def call(self, body):
        start_time = time.time()
        name = 'invoke_endpoint'
        try:
            result = self.client.invoke_endpoint(
                EndpointName=self.endpoint_name,
                Body=body,
                ContentType="application/json",
                Accept="application/json",
            )
        except:
            total_time = int((time.time() - start_time) * 1000)
            events.request_failure.fire(request_type="sagemaker", name=name, response_time=total_time, response_length=0, exception=sys.exc_info())
        else:
            total_time = int((time.time() - start_time) * 1000)
            events.request_success.fire(request_type="sagemaker", name=name, response_time=total_time, response_length=0)

# results:
# v1 m5-xl - 30rps per m5.xlarge machine, p95: 120ms
# v1.1 m5-xl - 55rps, p95: 50ms
# v1.1 m5-4x -70rps, p95: 55ms
# v1.1-m5-xl-x2 - 103rps, p95: 52ms
# v1.1-m5-12xl -
class SagemakerUser(User):
    test_body: str
    wait_time = constant_throughput(100)

    def __init__(self, *args, **kwargs):
        super(SagemakerUser, self).__init__(*args, **kwargs)
        self.client = SagemakerClient(
            client = boto3.client('sagemaker-runtime'),
            endpoint_name = self.environment.parsed_options.endpoint_name
        )
        with open('request_11.json') as fp:
            self.test_body = fp.read()

    @task
    def call_sagemaker(self):
        self.client.call(self.test_body)