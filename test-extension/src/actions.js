import * as TwitchExt from './twitch-ext'

const callbacksByTarget = {}

export const ACTION_TYPES = {
    LOADED: 'loaded',
    ON_AUTHORIZED: 'onAuthorized',
    ON_ERROR: 'onError',
    ON_CONTEXT: 'onContext',
    ON_MESSAGE: 'onMessage',
    ON_FOLLOW: 'onFollow',
    ON_LISTEN: 'onListen',
    ON_UNLISTEN: 'onUnlisten',
    SET_BITS_PRODUCTS: 'setBitsProducts',
    GET_BITS_PRODUCTS: 'getBitsProducts',
    USE_BITS: 'useBits',
    SET_LATEST_TRANSACTION: 'setLatestTransaction',
    SHOW_BITS_BALANCE: 'showBitsBalance',
    REQUEST_ID_SHARE: 'requestIdShare',
    FOLLOW_CHANNEL: 'followChannel',
}

export const setAuthorization = (authData) => ({
    type: ACTION_TYPES.ON_AUTHORIZED,
    data: authData
})

export const setContext = (contextData) => ({
    type: ACTION_TYPES.ON_CONTEXT,
    data: contextData
})

export const setError = (error) => ({
    type: ACTION_TYPES.ON_ERROR,
    data: error
})

export const setLoaded = () => ({
    type: ACTION_TYPES.LOADED
})

export const setMessage = (target, message) => ({
    type: ACTION_TYPES.ON_MESSAGE,
    target,
    message
})

export const setOnListen = (target) => ({
    type: ACTION_TYPES.ON_LISTEN,
    target,
})

export const setOnUnlisten = (target) => ({
    type: ACTION_TYPES.ON_UNLISTEN,
    target,
})

export const listen = (target) => {
    return (dispatch) => {
        if (!callbacksByTarget[target]) {
            callbacksByTarget[target] = (target, contentType, message) => {
                dispatch(setMessage(target, message))
            }
    
            TwitchExt.listen(target, callbacksByTarget[target])
            dispatch(setOnListen(target))
        }
    }
}

export const unlisten = (target) => {
    return (dispatch) => {
        if (callbacksByTarget[target]) {
            TwitchExt.unlisten(target, callbacksByTarget[target])
            callbacksByTarget[target] = null
            dispatch(setOnUnlisten(target))
        }
    }
}

export const send = (target, contentType, message) => {
    return (dispatch) => {
        TwitchExt.send(target, contentType, message);
    }
}

export const followChannel = (channelName) => {
    TwitchExt.actions.followChannel(channelName)
    return {
        type: ACTION_TYPES.FOLLOW_CHANNEL,
    }
}

export const setFollow = (data) => ({
    type: ACTION_TYPES.ON_FOLLOW,
    data,
})

export const requestIdShare = () => {
    TwitchExt.actions.requestIdShare()
    return {
        type: ACTION_TYPES.REQUEST_ID_SHARE
    }
}

export const setBitsProducts = (products) => {
    return {
        type: ACTION_TYPES.SET_BITS_PRODUCTS,
        products: products || [],
    }
}

export const getBitsProducts = () => {
    return (dispatch) => {
        TwitchExt.bits.getProducts().then(products => {
            dispatch(setBitsProducts(products));
        });
    }
}

export const useBits = (sku) => {
    return () => {
        TwitchExt.bits.useBits(sku);
    }
}

export const showBitsBalance = () => {
    return () => {
        TwitchExt.bits.showBitsBalance();
    }
}

export const setLatestTransaction = (transaction) => ({
    type: ACTION_TYPES.SET_LATEST_TRANSACTION,
    transaction,
})