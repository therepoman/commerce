import * as React from 'react'
import { DataDisplay } from '../data-display/component.jsx'

export class BitsApiComponent extends React.Component {
    constructor(props) {
        super(props);
        this.useBits = this.useBits.bind(this);
    }

    render () {
        const products = (this.props.products || []).map(product => {
            return (
                <div key={product.sku}>
                    <button onMouseEnter={this.props.showBitsBalance} onClick={this.useBits(product.sku)}>{product.displayName} {product.cost.amount} {product.cost.type}</button>
                </div>
            )
        })
        return (
            <div>
                <p><button onClick={this.props.showBitsBalance}>Show Bits Balance</button></p>
                {products.length ? <p>Bits Products</p> : <button onClick={this.props.getProducts}>Get Bits Products</button>}
                {products}
                <p>Latest Transaction</p>
                {this.props.transaction ? this.renderTransactionData(this.props.transaction) : null}
            </div>
        )
    }

    useBits (sku) {
        return () => {
            this.props.useBits(sku);
        }
    }

    renderTransactionData (transaction) {
        if (!transaction) return;

        return Object.keys(transaction).map(key => {
            return <DataDisplay key={key} label={key} value={transaction[key]} />
        })
    }
}