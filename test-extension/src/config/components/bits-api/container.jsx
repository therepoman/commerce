import { connect, Provider } from 'react-redux'
import * as Actions from '../../../actions'
import * as React from 'react'
import Store, { initialState } from '../../../store'
import { BitsApiComponent } from './component.jsx'

const mapStateToProps = (state) => {
    return {
        products: state && state.bitsProducts,
        transaction: state && state.latestTransaction,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        useBits: (sku) => dispatch(Actions.useBits(sku)),
        getProducts: () => dispatch(Actions.getBitsProducts()),
        showBitsBalance: () => dispatch(Actions.showBitsBalance()),
    }
}

const BitsApiRedux = connect(mapStateToProps, mapDispatchToProps)(BitsApiComponent)

export class BitsApi extends React.Component{
    render () {
        return (
            <Provider store={Store}>
                <BitsApiRedux />
            </Provider>
        );
    }
}
