import * as ReactDom from 'react-dom'
import * as React from 'react'
import { connect, Provider } from 'react-redux'
import * as Actions from '../actions'
import Store, { initialState } from '../store'
import { App } from './app.jsx'
import * as TwitchExt from '../twitch-ext'
import './index.css'

TwitchExt.onAuthorized((authData) => Store.dispatch(Actions.setAuthorization(authData)))
TwitchExt.onContext((contextData) => Store.dispatch(Actions.setContext(contextData)))
TwitchExt.onError((error) => Store.dispatch(Actions.setError(error)))
TwitchExt.actions.onFollow((didFollow, channelName) => Store.dispatch(Actions.setFollow({ didFollow, channelName})))
TwitchExt.bits.onTransactionComplete((transaction) => {
    console.log('latest transaction', transaction)
    Store.dispatch(Actions.setLatestTransaction(transaction))
})
    

const mapStateToProps = (state) => {
    return {
        ...initialState,
        ...state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        listen: target => dispatch(Actions.listen(target)),
        unlisten: target => dispatch(Actions.unlisten(target)),
        send: (target, contentType, message) => dispatch(Actions.send(target, contentType, message)),
        follow: (channelName) => dispatch(Actions.follow(channelName))
    }
}

const AppWithRedux = connect(mapStateToProps, mapDispatchToProps)(App)

ReactDom.render(
    <Provider store={Store}>
        <AppWithRedux />
    </Provider>,
    document.getElementById('app')
)
