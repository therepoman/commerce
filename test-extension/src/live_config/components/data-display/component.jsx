import * as React from 'react'
import './styles.css'

export class DataDisplay extends React.Component {
    render () {
        let value = this.props.value;

        try {
            value = JSON.stringify(value);
        } catch (e) {
            value = value.toString();
        }

        return (
            <div className="data-display">
                <span>{this.props.label}:</span>
                <span>{value}</span>
            </div>
        )
    }
}
