import { connect, Provider } from 'react-redux'
import * as Actions from '../../../actions'
import * as React from 'react'
import Store, { initialState } from '../../../store'
import { PublicApiComponent } from './component.jsx'

const mapStateToProps = (state) => {
    return {
        showUnlisten: !!(state && state.targets && state.targets.indexOf('broadcast') !== -1),
        messages: state && state.messages,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        listen: () => dispatch(Actions.listen('broadcast')),
        unlisten: () => dispatch(Actions.unlisten('broadcast')),
        send: () => dispatch(Actions.send('broadcast', 'application/json', 'Testing: ' + new Date().toLocaleString())),
        requestIdShare: () => dispatch(Actions.requestIdShare()),
        followChannel: () => dispatch(Actions.followChannel('rcking')),
    }
}

const PublicApiRedux = connect(mapStateToProps, mapDispatchToProps)(PublicApiComponent)

export class PublicApi extends React.Component{
    render () {
        return (
            <Provider store={Store}>
                <PublicApiRedux />
            </Provider>
        );
    }
}
