#!/bin/sh
# Duration
VEG_DURATION=10m

read -p "Run load tests at 50% load?(y/n) " continueTests
if [[ "$continueTests" == 'Y' || "$continueTests" == 'y' ]]; then
    
  VEG_LEADERBOARD_RATE=150
  VEG_CAMPAIGN_BY_ID_RATE=150
  VEG_USER_PRODUCT_SUBS_RATE=150
  source ./run-vegeta.sh
else
  exit 0
fi

read -p "Run load tests at 75% load?(y/n) " continueTests
if [[ "$continueTests" == 'Y' || "$continueTests" == 'y' ]]; then
    
  VEG_LEADERBOARD_RATE=225
  VEG_CAMPAIGN_BY_ID_RATE=225
  VEG_USER_PRODUCT_SUBS_RATE=225
  source ./run-vegeta.sh
else
  exit 0
fi

read -p "Run load tests at 100% load?(y/n) " continueTests
if [[ "$continueTests" == 'Y' || "$continueTests" == 'y' ]]; then
    
  VEG_LEADERBOARD_RATE=300
  VEG_CAMPAIGN_BY_ID_RATE=300
  VEG_USER_PRODUCT_SUBS_RATE=300
  source ./run-vegeta.sh
else
  exit 0
fi
