# fortuna-load-tests
Load generation scripts for Fortuna

## Instructions
1) Install [vegeta](https://github.com/tsenart/vegeta)
2) Determine the targets you want to run and set the required variables in your script (Refer owl-2019-loadtest.sh and run-vegeta.sh)
3) Run run-vegeta.sh

```
$ ./run-vegeta.sh
```