#!/bin/sh

if [[ -z "$VEG_DURATION" ]]; then
    echo "VEG_DURATION not specified. Exiting";
    exit 1;
fi


# Duration
echo "-----------------------------------"
echo "--Starting Vegeta load generation--"
echo "VEG_DURATION: $VEG_DURATION"
echo "-----------------------------------"

# Progress
if [[ ! -z "$VEG_PROGRESS_RATE" ]]; then
    echo "VEG_PROGRESS_RATE: $VEG_PROGRESS_RATE"
    vegeta attack \
    -targets=./targets/progression-targets.txt \
    -rate=${VEG_PROGRESS_RATE} \
    -duration=${VEG_DURATION} \
    | vegeta report&
fi

# Leaderboard
if [[ ! -z "$VEG_LEADERBOARD_RATE" ]]; then
    echo "VEG_LEADERBOARD_RATE: $VEG_LEADERBOARD_RATE"
    vegeta attack \
    -targets=./targets/leaderboard-targets.txt \
    -rate=${VEG_LEADERBOARD_RATE} \
    -duration=${VEG_DURATION} \
    | vegeta report&
fi

# Rewards
if [[ ! -z "$VEG_REWARD_RATE" ]]; then
    echo "VEG_REWARD_RATE: $VEG_REWARD_RATE"
    vegeta attack \
    -targets=./targets/reward-targets.txt \
    -rate=${VEG_REWARD_RATE} \
    -duration=${VEG_DURATION} \
    | vegeta report&
fi

# Milestones
if [[ ! -z "$VEG_MILESTONE_RATE" ]]; then
    echo "VEG_MILESTONE_RATE: $VEG_MILESTONE_RATE"
    vegeta attack \
    -targets=./targets/milestone-targets.txt \
    -rate=${VEG_MILESTONE_RATE} \
    -duration=${VEG_DURATION} \
    | vegeta report&
fi

# HATS
if [[ ! -z "$VEG_HATS_RATE" ]]; then
    echo "VEG_HATS_RATE: $VEG_HATS_RATE"
    vegeta attack \
    -targets=./targets/hats-targets.txt \
    -rate=${VEG_HATS_RATE} \
    -duration=${VEG_DURATION} \
    | vegeta report&
fi

# RewardsByTag
if [[ ! -z "$VEG_REWARD_TAG_RATE" ]]; then
    echo "VEG_REWARD_TAG_RATE: $VEG_REWARD_TAG_RATE"
    vegeta attack \
    -targets=./targets/reward-tag-targets.txt \
    -rate=${VEG_REWARD_TAG_RATE} \
    -duration=${VEG_DURATION} \
    | vegeta report&
fi

# GetObjectivesWithProgressionState
if [[ ! -z "$VEG_OBJECTIVE_RATE" ]]; then
    echo "VEG_OBJECTIVE_RATE: $VEG_OBJECTIVE_RATE"
    vegeta attack \
    -targets=./targets/objective-targets.txt \
    -rate=${VEG_OBJECTIVE_RATE} \
    -duration=${VEG_DURATION} \
    | vegeta report& 
fi

# GetChannelInfo
if [[ ! -z "$VEG_CHANNEL_INFO_RATE" ]]; then
    echo "VEG_CHANNEL_INFO_RATE: $VEG_CHANNEL_INFO_RATE"
    vegeta attack \
    -targets=./targets/channel-info-targets.txt \
    -rate=${VEG_CHANNEL_INFO_RATE} \
    -duration=${VEG_DURATION} \
    | vegeta report&
fi

# GetTriggers
if [[ ! -z "$VEG_TRIGGERS_RATE" ]]; then
    echo "VEG_TRIGGERS_RATE: $VEG_TRIGGERS_RATE"
    vegeta attack \
    -targets=./targets/get-triggers-targets.txt \
    -rate=${VEG_TRIGGERS_RATE} \
    -duration=${VEG_DURATION} \
    | vegeta report&
fi

#CampaignById
if [[ ! -z "$VEG_CAMPAIGN_BY_ID_RATE" ]]; then
    echo "VEG_CAMPAIGN_BY_ID_RATE: $VEG_CAMPAIGN_BY_ID_RATE"
    vegeta attack \
    -targets=./targets/campaigns-by-id-targets.txt \
    -rate=${VEG_CAMPAIGN_BY_ID_RATE} \
    -duration=${VEG_DURATION} \
    | vegeta report&
fi

#EmitDiscoveryEvent
if [[ ! -z "$VEG_EMIT_DISCOVERY_EVENT_RATE" ]]; then
    echo "VEG_EMIT_DISCOVERY_EVENT_RATE: $VEG_EMIT_DISCOVERY_EVENT_RATE"
    vegeta attack \
    -targets=./targets/discovery-targets.txt \
    -rate=${VEG_EMIT_DISCOVERY_EVENT_RATE} \
    -duration=${VEG_DURATION} \
    | vegeta report&
fi

#UserProductSubscriptions
if [[ ! -z "$VEG_USER_PRODUCT_SUBS_RATE" ]]; then
    echo "VEG_USER_PRODUCT_SUBS_RATE: $VEG_USER_PRODUCT_SUBS_RATE"
    vegeta attack \
    -targets=./targets/product-subs-targets.txt \
    -rate=${VEG_USER_PRODUCT_SUBS_RATE} \
    -duration=${VEG_DURATION} \
    | vegeta report&
fi

#GetConnection
if [[ ! -z "$VEG_GET_CONNECTIONS_RATE" ]]; then
    echo "VEG_GET_CONNECTIONS_RATE: $VEG_GET_CONNECTIONS_RATE"
    vegeta attack \
    -targets=./targets/get-connections-targets.txt \
    -rate=${VEG_GET_CONNECTIONS_RATE} \
    -duration=${VEG_DURATION} \
    | vegeta report&
fi

echo "------Vegeta is sending load-------"
wait
echo "-Vegeta has finished sending load--"
echo "-----------------------------------"
