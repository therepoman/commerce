/* Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved. */
package sd

import (
	"code.justin.tv/commerce/AAAGo/src/aaa"
	"bytes"
	"github.com/pkg/errors"
	"io"
	"io/ioutil"
	"math/rand"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"testing"
	"time"
)

const (
	testOperation = "TestOperation"
	testService   = "TestService"
)

func TestNewErrorCases(t *testing.T) {
	// No options
	empty := []func(*SdClient) error{}
	if _, err := NewSDClient(empty...); err == nil {
		t.Error("Expected an error for no options")
	}
	// Auto-Discovery will fail because we're not in an Apollo env
	if _, err := NewSDClient(SocketPathAutodiscovery()); err == nil {
		t.Error("Expected an error for constructing outside of an Apollo Environment")
	}
}

func TestEncodeRequest(t *testing.T) {
	rand.Seed(time.Now().Unix())
	suffix := rand.Int31()
	path := filepath.Join(os.TempDir(), "sd_test"+strconv.Itoa(int(suffix))+".sock")
	defer os.Remove(path)
	t.Log("Using socket path", path)
	server, err := net.ListenUnix("unix", &net.UnixAddr{Name: path, Net: "unix"})
	if err != nil {
		t.Fatal(err)
	}
	defer server.Close()

	sd, err := NewSDClient(StaticSocketPath(path))
	if err != nil {
		t.Fatal("Unable to create SD client", err)
	}

	ts := &testServer{}
	go ts.listen(server, t)

	// Verify that our connection is good
	if err := sd.SanityCheck(); err != nil {
		t.Fatal("Expected no error performing the SanityCheck, instead found", err)
	}

	tests := []struct {
		name       string
		body       io.ReadCloser
		serverData []byte
		serverErr  error
		retainConn bool
	}{
		{"Bad body", &erroringReader{}, nil, nil, false},
		{"Valid request", ioutil.NopCloser(bytes.NewBuffer([]byte(testOperation))), testValidEncodeData, nil, true},
		{"Valid request 2", ioutil.NopCloser(bytes.NewBuffer([]byte(testOperation))), testValidEncodeData, nil, true},
		{"Invalid data", ioutil.NopCloser(bytes.NewBuffer([]byte(testOperation))), testInvalidData, nil, false},
		{"Valid request 3", ioutil.NopCloser(bytes.NewBuffer([]byte(testOperation))), testValidEncodeData, nil, true},
		{"Invalid data 2", ioutil.NopCloser(bytes.NewBuffer([]byte(testOperation))), testInvalidData, nil, false},
		{"Valid request 4", ioutil.NopCloser(bytes.NewBuffer([]byte(testOperation))), testValidEncodeData, nil, true},
	}

	expectedConns := 0
	for _, test := range tests {
		if test.retainConn {
			expectedConns = 1
		} else {
			expectedConns = 0
		}
		r, _ := http.NewRequest("GET", "http://example.com", nil)
		r.Body = test.body
		ts.set(test.serverData, test.serverErr)
		ctx, err := sd.EncodeRequest(testService, testOperation, r)
		if test.retainConn != (err == nil) {
			t.Error(test.name, "- Unexpected error state:", err)
		}
		if test.retainConn != (ctx != nil) {
			t.Error(test.name, "- Unexpected context state:", ctx)
		}
		if len(sd.pool) != expectedConns {
			t.Error(test.name, "- Expected there to be", expectedConns, "pooled connections but found", len(sd.pool))
		}
	}
}

func TestDecodeResponse(t *testing.T) {
	rand.Seed(time.Now().Unix())
	suffix := rand.Int31()
	path := filepath.Join(os.TempDir(), "sd_test"+strconv.Itoa(int(suffix))+".sock")
	defer os.Remove(path)
	t.Log("Using socket path", path)
	server, err := net.ListenUnix("unix", &net.UnixAddr{Name: path, Net: "unix"})
	if err != nil {
		t.Fatal(err)
	}
	defer server.Close()

	sd, err := NewSDClient(StaticSocketPath(path))
	if err != nil {
		t.Fatal("Unable to create SD client", err)
	}

	ts := &testServer{}
	go ts.listen(server, t)

	// Verify that our connection is good
	if err := sd.SanityCheck(); err != nil {
		t.Fatal("Expected no error performing the SanityCheck, instead found", err)
	}

	clientCxt := &aaa.ClientContext{
		KeyId:      "keyId",
		KeyVersion: 1,
		Service:    testService,
		Operation:  testOperation,
	}

	tests := []struct {
		name       string
		body       io.ReadCloser
		serverData []byte
		serverErr  error
		retainConn bool
	}{
		{"Bad body", &erroringReader{}, nil, nil, false},
		{"Valid request", ioutil.NopCloser(bytes.NewBuffer([]byte(testOperation))), testValidDecodeData, nil, true},
		{"Valid request 2", ioutil.NopCloser(bytes.NewBuffer([]byte(testOperation))), testValidDecodeData, nil, true},
		{"Invalid data", ioutil.NopCloser(bytes.NewBuffer([]byte(testOperation))), testInvalidData, nil, false},
		{"Valid request 3", ioutil.NopCloser(bytes.NewBuffer([]byte(testOperation))), testValidDecodeData, nil, true},
		{"Invalid data 2", ioutil.NopCloser(bytes.NewBuffer([]byte(testOperation))), testInvalidData, nil, false},
		{"Valid request 4", ioutil.NopCloser(bytes.NewBuffer([]byte(testOperation))), testValidDecodeData, nil, true},
	}

	expectedConns := 0
	for _, test := range tests {
		if test.retainConn {
			expectedConns = 1
		} else {
			expectedConns = 0
		}
		r := &http.Response{Body: test.body}
		ts.set(test.serverData, test.serverErr)
		err := sd.DecodeResponse(clientCxt, r)
		if test.retainConn != (err == nil) {
			t.Error(test.name, "- Unexpected error state:", err)
		}
		if len(sd.pool) != expectedConns {
			t.Error(test.name, "- Expected there to be", expectedConns, "pooled connections but found", len(sd.pool))
		}
	}
}

// testServer allows us to start up a UnixListener that can accept multiple simultaneous
// connections while customizing what the output of each connection will be.
type testServer struct {
	data []byte
	err  error
}

func (ts *testServer) set(data []byte, err error) {
	ts.data = data
	ts.err = err
}

func (ts *testServer) listen(server *net.UnixListener, t *testing.T) {
	for {
		conn, err := server.AcceptUnix()
		if err == nil {
			go ts.serve(conn, t)
		} else {
			t.Log("Error accepting:", err)
			break
		}
	}
}

func (ts *testServer) serve(c net.Conn, t *testing.T) {
	defer c.Close()
	buf := make([]byte, 2048)
	for {
		if ts.err != nil {
			t.Log("ts.err is not nil, closing the connection")
			return
		} else if len(ts.data) > 0 {
			c.SetDeadline(time.Now().Add(250 * time.Millisecond))
			n, err := c.Read(buf)
			if n == 0 || (err != nil && err != io.EOF) {
				continue
			}
			t.Log("testServer read", n, "bytes")
			n, err = c.Write(ts.data[:16])
			t.Log("testServer wrote", n, "bytes")
			if err != nil {
				t.Log("writing failed, closing the connection", err)
				continue
			}
			if len(ts.data) > 24 {
				n, err = c.Write(ts.data[16:])
				t.Log("testServer wrote", n, "bytes")
				if err != nil {
					t.Log("writing failed, closing the connection", err)
					continue
				}
			}
		}
	}
}

type erroringReader struct {
}

func (*erroringReader) Read([]byte) (int, error) {
	return 0, errors.New("erroringReader")
}

func (*erroringReader) Close() error {
	return nil
}

var (
	testValidDecodeData = []byte{
		// Header
		66, 69, 0, 0, 0, 0, 3, 12, 1, 0, 1, 0, 0, 0, 0, 7,
		// Service layer
		146, 144, 146, 1, 205, 3, 5,
		// Data layer
		146, 144, 146, 1, 218, 2, 254, 0, 0, 2, 254, 153, 85, 238, 170, 8, 82, 101, 115, 117, 108, 116, 115, 0, 1, 122, 0, 0, 2,
		60, 153, 85, 238, 170, 15, 42, 118, 101, 99, 116, 111, 114, 108, 101, 110, 103, 116, 104, 42, 0, 11, 4, 0, 0, 0, 1, 4,
		118, 95, 48, 0, 1, 122, 0, 0, 2, 23, 153, 85, 238, 170, 10, 66, 83, 70, 68, 86, 97, 108, 105, 100, 0, 8, 7, 78, 111, 114,
		109, 97, 108, 0, 8, 42, 118, 97, 108, 117, 101, 42, 0, 1, 122, 0, 0, 1, 212, 153, 85, 238, 170, 14, 97, 99, 116, 105, 118,
		101, 83, 101, 115, 115, 105, 111, 110, 0, 9, 1, 0, 10, 118, 97, 108, 105, 100, 85, 66, 73, 68, 0, 8, 20, 56, 49, 48, 45, 51,
		50, 54, 52, 54, 55, 53, 45, 56, 49, 50, 52, 53, 53, 57, 0, 17, 42, 99, 108, 97, 115, 115, 72, 105, 101, 114, 97, 114, 99,
		104, 121, 42, 0, 1, 122, 0, 0, 0, 111, 153, 85, 238, 170, 15, 42, 118, 101, 99, 116, 111, 114, 108, 101, 110, 103, 116, 104,
		42, 0, 11, 4, 0, 0, 0, 2, 4, 118, 95, 48, 0, 8, 50, 105, 100, 101, 110, 116, 105, 116, 121, 46, 115, 101, 115, 115, 105, 111,
		110, 115, 101, 114, 118, 105, 99, 101, 118, 50, 46, 86, 97, 108, 105, 100, 97, 116, 101, 83, 101, 115, 115, 105, 111, 110,
		82, 101, 115, 112, 111, 110, 115, 101, 0, 4, 118, 95, 49, 0, 8, 17, 106, 97, 118, 97, 46, 108, 97, 110, 103, 46, 79, 98, 106,
		101, 99, 116, 0, 15, 118, 97, 108, 105, 100, 83, 101, 115, 115, 105, 111, 110, 73, 68, 0, 8, 20, 56, 49, 49, 45, 57, 55, 52,
		54, 48, 55, 48, 45, 53, 48, 50, 54, 49, 53, 50, 0, 8, 99, 111, 109, 109, 101, 110, 116, 0, 8, 79, 99, 111, 111, 107, 105, 101,
		83, 101, 115, 115, 105, 111, 110, 73, 100, 32, 105, 115, 32, 118, 97, 108, 105, 100, 59, 32, 105, 115, 67, 97, 99, 104, 101,
		97, 98, 108, 101, 83, 101, 115, 115, 105, 111, 110, 59, 32, 115, 101, 115, 115, 105, 111, 110, 39, 115, 32, 99, 117, 115, 116,
		111, 109, 101, 114, 32, 105, 100, 32, 105, 115, 32, 118, 97, 108, 105, 100, 59, 32, 0, 13, 99, 114, 101, 97, 116, 105, 111,
		110, 68, 97, 116, 101, 0, 13, 8, 65, 213, 219, 81, 54, 0, 0, 0, 16, 118, 97, 108, 105, 100, 67, 117, 115, 116, 111, 109, 101,
		114, 73, 68, 0, 8, 21, 65, 48, 55, 48, 57, 56, 55, 54, 75, 74, 87, 54, 53, 68, 56, 67, 78, 52, 85, 73, 0, 12, 42, 99, 108, 97,
		115, 115, 78, 97, 109, 101, 42, 0, 8, 50, 105, 100, 101, 110, 116, 105, 116, 121, 46, 115, 101, 115, 115, 105, 111, 110, 115,
		101, 114, 118, 105, 99, 101, 118, 50, 46, 86, 97, 108, 105, 100, 97, 116, 101, 83, 101, 115, 115, 105, 111, 110, 82, 101, 115,
		112, 111, 110, 115, 101, 0, 17, 99, 97, 99, 104, 101, 97, 98, 108, 101, 83, 101, 115, 115, 105, 111, 110, 0, 9, 1, 1, 17, 42,
		67, 111, 100, 105, 103, 111, 80, 114, 111, 116, 111, 99, 111, 108, 42, 0, 13, 8, 63, 241, 153, 153, 153, 153, 153, 154, 8, 83,
		101, 114, 118, 105, 99, 101, 0, 8, 17, 83, 101, 115, 115, 105, 111, 110, 83, 101, 114, 118, 105, 99, 101, 86, 50, 0, 7, 77,
		101, 116, 104, 111, 100, 0, 8, 16, 86, 97, 108, 105, 100, 97, 116, 101, 83, 101, 115, 115, 105, 111, 110, 0, 11, 73, 100, 101,
		110, 116, 105, 116, 105, 101, 115, 0, 1, 122, 0, 0, 0, 107, 153, 85, 238, 170, 12, 73, 109, 109, 101, 100, 67, 97, 108, 108,
		101, 114, 0, 1, 122, 0, 0, 0, 84, 153, 85, 238, 170, 10, 83, 117, 98, 106, 101, 99, 116, 73, 68, 0, 8, 47, 97, 109, 122, 110,
		49, 58, 115, 105, 100, 58, 48, 48, 48, 48, 48, 48, 48, 48, 45, 48, 48, 48, 48, 45, 48, 48, 48, 48, 45, 48, 48, 48, 48, 45, 48,
		48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 0, 5, 78, 97, 109, 101, 0, 8, 8, 117, 110, 107, 110, 111, 119, 110,
	}
	testInvalidData     = []byte{66, 69, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	testValidEncodeData = []byte{
		// Header
		66, 69, 0, 0, 0, 0, 3, 58, 1, 0, 1, 0, 0, 0, 0, 7,
		// Service layer
		146, 144, 146, 1, 205, 3, 51,
		// Data layer
		148, 144, 146, 1, 218, 1, 128, 153, 226, 217, 95, 24, 151, 80, 79, 129, 120, 120, 2, 60, 14, 162, 155, 70, 244,
		155, 96, 131, 103, 2, 237, 174, 100, 165, 247, 135, 57, 158, 174, 206, 191, 75, 11, 92, 200, 69, 64, 25, 56, 11,
		73, 79, 21, 179, 149, 143, 42, 118, 107, 5, 18, 121, 39, 14, 6, 155, 141, 77, 6, 40, 206, 171, 147, 184, 82, 30,
		72, 58, 94, 68, 56, 18, 231, 218, 97, 167, 225, 218, 242, 191, 222, 212, 88, 238, 192, 188, 239, 169, 119, 105,
		142, 70, 220, 132, 74, 8, 206, 230, 42, 14, 146, 234, 70, 252, 16, 9, 182, 71, 1, 97, 202, 6, 211, 121, 148,
		116, 20, 35, 131, 167, 134, 168, 238, 158, 211, 189, 179, 89, 18, 118, 166, 243, 249, 240, 149, 171, 138, 43,
		198, 252, 89, 240, 240, 97, 65, 57, 148, 71, 40, 47, 60, 205, 124, 65, 30, 185, 66, 191, 74, 100, 32, 0, 55, 59,
		18, 148, 149, 242, 174, 228, 147, 140, 37, 22, 179, 187, 173, 178, 79, 218, 120, 110, 170, 181, 215, 127, 78, 160,
		112, 172, 214, 191, 193, 143, 225, 157, 225, 222, 67, 40, 125, 25, 221, 109, 82, 88, 161, 66, 187, 216, 72, 90,
		236, 226, 218, 211, 10, 52, 204, 42, 191, 152, 198, 254, 45, 156, 53, 19, 9, 30, 142, 163, 210, 221, 230, 251,
		160, 72, 230, 167, 153, 227, 144, 120, 250, 170, 147, 48, 116, 66, 209, 36, 76, 213, 82, 240, 14, 111, 196, 92,
		81, 152, 73, 216, 38, 26, 228, 141, 89, 149, 188, 188, 99, 230, 182, 208, 28, 213, 150, 58, 249, 94, 58, 157,
		99, 248, 41, 143, 129, 101, 106, 223, 80, 198, 152, 231, 188, 169, 165, 235, 15, 135, 50, 113, 50, 253, 209,
		135, 185, 194, 87, 123, 109, 176, 244, 167, 75, 110, 243, 88, 26, 164, 139, 135, 215, 132, 253, 166, 162, 56,
		173, 172, 144, 27, 107, 228, 184, 209, 68, 60, 3, 238, 68, 2, 156, 183, 76, 243, 124, 206, 131, 4, 80, 216, 112,
		69, 145, 176, 112, 135, 209, 45, 215, 116, 191, 195, 254, 107, 65, 198, 41, 73, 61, 230, 76, 108, 181, 151, 59,
		121, 56, 146, 2, 147, 144, 146, 1, 1, 146, 2, 218, 0, 99, 99, 111, 109, 46, 97, 109, 97, 122, 111, 110, 46, 97,
		97, 97, 46, 83, 101, 115, 115, 105, 111, 110, 83, 101, 114, 118, 105, 99, 101, 86, 50, 46, 86, 83, 83, 76, 105,
		98, 101, 114, 116, 121, 69, 120, 97, 109, 112, 108, 101, 72, 122, 46, 97, 109, 122, 110, 49, 46, 97, 97, 97, 46,
		105, 100, 46, 110, 55, 122, 111, 112, 101, 117, 121, 98, 108, 113, 115, 104, 120, 106, 100, 102, 122, 116, 114,
		102, 99, 116, 122, 54, 117, 46, 68, 101, 102, 97, 117, 108, 116, 146, 4, 218, 1, 56, 65, 65, 65, 32, 105, 100,
		101, 110, 116, 105, 116, 121, 61, 99, 111, 109, 46, 97, 109, 97, 122, 111, 110, 46, 97, 97, 97, 46, 83, 101, 115,
		115, 105, 111, 110, 83, 101, 114, 118, 105, 99, 101, 86, 50, 46, 86, 83, 83, 76, 105, 98, 101, 114, 116, 121, 69,
		120, 97, 109, 112, 108, 101, 72, 122, 46, 97, 109, 122, 110, 49, 46, 97, 97, 97, 46, 105, 100, 46, 110, 55, 122,
		111, 112, 101, 117, 121, 98, 108, 113, 115, 104, 120, 106, 100, 102, 122, 116, 114, 102, 99, 116, 122, 54, 117,
		46, 68, 101, 102, 97, 117, 108, 116, 47, 49, 44, 32, 79, 112, 101, 114, 97, 116, 105, 111, 110, 61, 86, 97, 108,
		105, 100, 97, 116, 101, 83, 101, 115, 115, 105, 111, 110, 44, 32, 83, 101, 114, 118, 105, 99, 101, 61, 83, 101,
		115, 115, 105, 111, 110, 83, 101, 114, 118, 105, 99, 101, 86, 50, 44, 32, 69, 110, 99, 114, 121, 112, 116, 101,
		100, 61, 116, 114, 117, 101, 44, 32, 83, 105, 103, 110, 101, 100, 72, 101, 97, 100, 101, 114, 115, 61, 44, 32, 65,
		108, 103, 111, 114, 105, 116, 104, 109, 61, 72, 109, 97, 99, 83, 72, 65, 50, 53, 54, 44, 32, 83, 105, 103, 110,
		101, 100, 61, 116, 114, 117, 101, 44, 32, 83, 105, 103, 110, 97, 116, 117, 114, 101, 61, 70, 114, 65, 115, 83,
		120, 75, 104, 109, 73, 73, 103, 85, 116, 55, 78, 88, 87, 80, 75, 57, 105, 73, 117, 106, 104, 80, 50, 89, 119, 51,
		101, 77, 72, 77, 82, 56, 116, 78, 100, 52, 67, 107, 61, 44, 82, 101, 108, 97, 116, 105, 111, 110, 115, 104, 105,
		112, 86, 101, 114, 115, 105, 111, 110, 61, 48,
	}
)
