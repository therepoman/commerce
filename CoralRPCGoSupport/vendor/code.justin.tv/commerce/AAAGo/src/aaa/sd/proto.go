package sd

import (
	"code.justin.tv/commerce/GoLog/src/log"
	"code.justin.tv/commerce/AAAGo/src/aaa/internal/msgpack"
	"bytes"
	"encoding/binary"
	"github.com/pkg/errors"
	"io"
	"math"
)

const (
	magicNumber                  uint16 = 0x4245
	futureUse                    uint16 = 0x0000
	headerService                uint16 = 0x0100
	serviceVersion               uint16 = 0x0000
	serviceEncoding              uint16 = 0x0100
	protocolSignatureMessagePack uint16 = 0x0100
	frameSize                    int    = 16
	maxFrameLen                         = 64 * 1024
)

type aaaAction struct {
	num  byte
	name string
}

func (action *aaaAction) EncodeMsgPack(e *msgpack.Encoder) error {
	if err := e.EncodeOuterArrayStart(2); err != nil {
		return err
	}
	if err := e.EncodeStringField(1, action.name); err != nil {
		return err
	}
	return e.EncodeUInt8Field(2, uint8(action.num))
}

func (action *aaaAction) DecodeMsgPack(d *msgpack.Decoder) error {
	outerLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode aaaAction as outer array cannot be decoded: %v", err)
	}
	requiredFieldLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode aaaAction as requiredFields array cannot be decoded: %v", err)
	}
	if requiredFieldLen != 0 {
		return errors.Errorf("Cannot decode aaaAction as the requiredFields array length is %v. Expected is %v.", requiredFieldLen, 0)
	}

	for i := uint32(1); i < outerLen; i++ {
		innerLen, err := d.DecodeArrayLen()
		if err != nil {
			return errors.Errorf("Cannot decode aaaAction as the inner array at entry %v cannot be decoded: %v", i, err)
		}
		if innerLen != 2 {
			return errors.Errorf("Cannot decode aaaAction as the inner array at entry %v has a length of %v. Expected is %v.", i, innerLen, 0)
		}
		index, err := d.DecodeUint()
		if err != nil {
			return errors.Errorf("Cannot decode aaaAction as the index in inner array entry %v cannot be decoded: %v", i, err)
		}
		switch index {
		case 1:
			action.name, err = d.DecodeString()
			if err != nil {
				return errors.Errorf("Cannot decode aaaAction as the string in index %v cannot be decoded: %v", i, err)
			}
		case 2:
			i, err := d.DecodeUint()
			if err != nil {
				return errors.Errorf("Cannot decode aaaAction as the uint8 in index %v cannot be decoded: %v", i, err)
			}
			if i > math.MaxUint8 {
				return errors.Errorf("Cannot decode aaaAction as the num value is greater than a uint8. Actual value: %v", i)
			}
			action.num = byte(i)
		default:
			continue // OK, don't want to break if AAA adds extra fields.
		}
	}
	return nil
}

var (
	authRequest aaaAction = aaaAction{
		num:  2,
		name: "authorizeRequest",
	}
	encodeRequest aaaAction = aaaAction{
		num:  3,
		name: "encodeRequest",
	}
	decodeRequest aaaAction = aaaAction{
		num:  4,
		name: "decodeRequest",
	}
	encodeResponse aaaAction = aaaAction{
		num:  5,
		name: "encodeResponse",
	}
	decodeResponse aaaAction = aaaAction{
		num:  6,
		name: "decodeResponse",
	}
	additionalInfo aaaAction = aaaAction{
		num:  7,
		name: "getAdditionalInformation",
	}
)

type roadsideInput struct {
	length   uint32
	encoding uint16
	version  uint16
	action   *aaaAction
}

func (in *roadsideInput) EncodeMsgPack(e *msgpack.Encoder) error {
	if err := e.EncodeOuterArrayStart(4); err != nil {
		return err
	}
	if err := e.EncodeUInt32Field(1, in.length); err != nil {
		return err
	}
	if err := e.EncodeUInt16Field(2, in.encoding); err != nil {
		return err
	}
	if err := e.EncodeUInt16Field(3, in.version); err != nil {
		return err
	}
	return e.EncodeSubField(4, in.action)
}

type exceptionInfo struct {
	code    byte
	message string
}

func (ei *exceptionInfo) DecodeMsgPack(d *msgpack.Decoder) error {
	outerLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode exceptionInfo as outer array cannot be decoded: %v", err)
	}
	requiredFieldLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode exceptionInfo as requiredFields array cannot be decoded: %v", err)
	}
	if requiredFieldLen != 0 {
		return errors.Errorf("Cannot decode exceptionInfo as the requiredFields array length is %v. Expected is %v.", requiredFieldLen, 0)
	}

	for i := uint32(1); i < outerLen; i++ {
		innerLen, err := d.DecodeArrayLen()
		if err != nil {
			return errors.Errorf("Cannot decode exceptionInfo as the inner array at entry %v cannot be decoded: %v", i, err)
		}
		if innerLen != 2 {
			return errors.Errorf("Cannot decode exceptionInfo as the inner array at entry %v has a length of %v. Expected is %v.", i, innerLen, 0)
		}
		index, err := d.DecodeUint()
		if err != nil {
			return errors.Errorf("Cannot decode exceptionInfo as the index in inner array entry %v cannot be decoded: %v", i, err)
		}
		switch index {
		case 1:
			c, err := d.DecodeUint()
			if err != nil {
				return errors.Errorf("Cannot decode exceptionInfo as the uint8 in index %v cannot be decoded: %v", i, err)
			}
			if c > math.MaxUint8 {
				return errors.Errorf("Cannot decode exceptionInfo as the code value is greater than a uint8. Actual value: %v", i)
			}
			ei.code = byte(c)
		case 2:
			ei.message, err = d.DecodeString()
			return errors.Errorf("Cannot decode exceptionInfo as the string in index %v cannot be decoded: %v", i, err)
		default:
			continue // OK, don't want to break if AAA adds extra fields.
		}
	}
	return nil
}

type roadsideOutput struct {
	length        uint32
	exceptionInfo *exceptionInfo
	encoding      uint16
	version       uint16
	action        *aaaAction
}

func (out *roadsideOutput) DecodeMsgPack(d *msgpack.Decoder) error {
	outerLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode roadsideOutput as outer array cannot be decoded: %v", err)
	}
	requiredFieldLen, err := d.DecodeArrayLen()
	if err != nil {
		return errors.Errorf("Cannot decode roadsideOutput as requiredFields array cannot be decoded: %v", err)
	}
	if requiredFieldLen != 0 {
		return errors.Errorf("Cannot decode roadsideOutput as the requiredFields array length is %v. Expected is %v.", requiredFieldLen, 0)
	}

	for i := uint32(1); i < outerLen; i++ {
		innerLen, err := d.DecodeArrayLen()
		if err != nil {
			return errors.Errorf("Cannot decode roadsideOutput as the inner array at entry %v cannot be decoded: %v", i, err)
		}
		if innerLen != 2 {
			return errors.Errorf("Cannot decode roadsideOutput as the inner array at entry %v has a length of %v. Expected is %v.", i, innerLen, 0)
		}
		index, err := d.DecodeUint()
		if err != nil {
			return errors.Errorf("Cannot decode roadsideOutput as the index in inner array entry %v cannot be decoded: %v", i, err)
		}
		switch index {
		case 1:
			i, err := d.DecodeUint()
			if err != nil {
				return errors.Errorf("Cannot decode roadsideOutput as the uint32 in index %v cannot be decoded: %v", i, err)
			}
			out.length = uint32(i)
		case 2:
			ei := new(exceptionInfo)
			err = ei.DecodeMsgPack(d)
			if err != nil {
				return errors.Errorf("Cannot decode roadsideOutput as the exceptionInfo in index %v cannot be decoded: %v", i, err)
			}
			out.exceptionInfo = ei
		case 3:
			i, err := d.DecodeUint()
			if err != nil {
				return errors.Errorf("Cannot decode roadsideOutput as the uint16 in index %v cannot be decoded: %v", i, err)
			}
			out.encoding = uint16(i)
		case 4:
			i, err := d.DecodeUint()
			if err != nil {
				return errors.Errorf("Cannot decode roadsideOutput as the uint16 in index %v cannot be decoded: %v", i, err)
			}
			out.version = uint16(i)
		case 5:
			action := new(aaaAction)
			err = action.DecodeMsgPack(d)
			if err != nil {
				return errors.Errorf("Cannot decode roadsideOutput as the aaaAction in index %v cannot be decoded: %v", i, err)
			}
			out.action = action
		default:
			continue // OK, don't want to break if AAA adds extra fields.
		}
	}
	return nil
}

type frameLayer struct {
	frameLen        uint32
	serviceLayerLen uint32
}

func newFrameLayer(dataLayerLen, serviceLayerLen int) *frameLayer {
	return &frameLayer{
		frameLen:        uint32(serviceLayerLen + dataLayerLen),
		serviceLayerLen: uint32(serviceLayerLen),
	}
}

func (l *frameLayer) Bytes() []byte {
	buff := &bytes.Buffer{}
	buff.Write(uint16Bytes(magicNumber))
	buff.Write(uint16Bytes(futureUse))
	buff.Write(uint32Bytes(l.frameLen))
	buff.Write(uint16Bytes(headerService))
	buff.Write(uint16Bytes(serviceEncoding))
	buff.Write(uint32Bytes(l.serviceLayerLen))
	return buff.Bytes()
}

func buildReq(action *aaaAction, data msgpack.MsgPackEncoder) ([]byte, error) {
	dl, err := msgpack.Encode(data)
	if err != nil {
		return nil, errors.Errorf("Failed to marshal data layer to msgpack. %v", err)
	}
	ri := &roadsideInput{
		length:   uint32(len(dl)),
		encoding: protocolSignatureMessagePack,
		version:  serviceVersion,
		action:   action,
	}
	sl, err := msgpack.Encode(ri)
	if err != nil {
		return nil, errors.Errorf("Failed to marshal service layer to msgpack. %v", err)
	}
	fl := newFrameLayer(len(dl), len(sl))
	buff := &bytes.Buffer{}
	buff.Write(fl.Bytes())
	buff.Write(sl)
	buff.Write(dl)
	return buff.Bytes(), nil
}

func readResp(r io.Reader, data msgpack.MsgPackDecoder) (fl *frameLayer, sl *roadsideOutput, err error) {
	buff := make([]byte, frameSize)
	_, err = r.Read(buff)
	if err != nil {
		return nil, nil, errors.Errorf("Failed to read frame layer response: %v", err)
	}
	frameLen := buff[4:8]
	dataLayerLen := buff[12:16]
	fl = &frameLayer{
		frameLen:        binary.BigEndian.Uint32(frameLen),
		serviceLayerLen: binary.BigEndian.Uint32(dataLayerLen),
	}

	// If the frameLength is larger than this, then there is something obviously wrong and
	// we don't want to be allocating a huge block of memory
	if fl.frameLen > maxFrameLen || fl.frameLen <= 0 {
		return nil, nil, errors.Errorf("Invalid frame length %d", fl.frameLen)
	}

	buff = make([]byte, fl.frameLen)
	n, err := r.Read(buff)
	if err != nil {
		return nil, nil, errors.Errorf("Failed to read service layer response from socket: %v", err)
	}
	log.Trace("Read", n, "bytes from socket")

	sl = new(roadsideOutput)
	err = msgpack.Decode(bytes.NewBuffer(buff[0:fl.serviceLayerLen]), sl)
	if err != nil {
		return nil, nil, errors.Errorf("Failed to demarshal service layer from msgpack. %v", err)
	}

	err = msgpack.Decode(bytes.NewBuffer(buff[fl.serviceLayerLen:]), data)
	if err != nil {
		return nil, nil, errors.Errorf("Failed to demarshal data layer from msgpack. %v", err)
	}

	return fl, sl, nil
}
