package sd

import (
	"code.justin.tv/commerce/GoLog/src/log"
	"code.justin.tv/commerce/AAAGo/src/aaa"
	"code.justin.tv/commerce/GoApolloEnvironmentInfo/src/apollo"
	"bytes"
	"github.com/pkg/errors"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"os/exec"
	"os/user"
	"path/filepath"
	"strings"
	"sync"
	"time"
)

const (
	dialTimeout = 1 * time.Second
	maxAttempts = 3
)

type SdClient struct {
	sync.Mutex
	socketPath string
	pool       []net.Conn
}

// connect returns a socket connection with the Security Daemon.  A connection is
// returned from the connection pool if available, otherwise a new connection is created.
func (sd *SdClient) connect() (net.Conn, error) {
	sd.Lock()
	defer sd.Unlock()
	numPooled := len(sd.pool)
	log.Trace("Number of pooled connections", numPooled)
	if numPooled > 0 {
		conn := sd.pool[0]
		copy(sd.pool, sd.pool[1:])
		sd.pool = sd.pool[:numPooled-1]
		log.Trace("Using pooled connection", conn)
		return conn, nil
	}
	c, err := net.DialTimeout("unix", sd.socketPath, dialTimeout)
	if err != nil {
		return nil, errors.Errorf("Failed to establish connection to AAASecurityDaemon with path %v: %v", sd.socketPath, err)
	}
	log.Trace("Created new connection", c)
	return c, nil
}

// release puts the given connection back into the pool if it is not null.
// Connections that have caused an error should not be released.
func (sd *SdClient) release(conn net.Conn) {
	if conn == nil {
		return
	}
	sd.Lock()
	defer sd.Unlock()
	log.Trace("Returning connection to the pool", conn)
	sd.pool = append(sd.pool, conn)
}

func NewSDClient(options ...func(*SdClient) error) (*SdClient, error) {
	sd := &SdClient{pool: make([]net.Conn, 0, 5)}
	for _, option := range options {
		err := option(sd)
		if err != nil {
			log.Errorf("Error creating AAASecurityDaemon client. Error while setting options: %+v", err)
			return nil, errors.Errorf("Error creating AAASecurityDaemon client. Error while setting options: %v", err)
		}
	}
	if sd.socketPath == "" {
		log.Error("Error creating AAASecurityDaemon client. The socketPath must be specified.")
		return nil, errors.Errorf("Error creating AAASecurityDaemon client. The socketPath must be specified.")
	}
	return sd, nil
}

// StaticSocketPath is a constructor option that allows you to specify what path to use
// for the socket.
func StaticSocketPath(path string) func(*SdClient) error {
	return func(sd *SdClient) error {
		sd.socketPath = path
		return nil
	}
}

// SocketPathAutodiscovery is a constructor option that tells the client to attempt to determine the
// socket path to use from the current Apollo environment.  If no environment is found, then an error
// is returned which causes initialization of the client to fail.
func SocketPathAutodiscovery() func(*SdClient) error {
	return func(sd *SdClient) error {
		root, err := getEnvRoot()
		if err != nil {
			return errors.Errorf("Unable to determine root: %v", err)
		}
		log.Debug("Using root", root)
		beSockPath := filepath.Join(root, "var", "state", "AAA", "be.sock.txt")
		log.Debug("Reading socket file at", beSockPath)
		buff, err := ioutil.ReadFile(beSockPath)
		if err != nil {
			errors.Errorf("Unable to read the be.sock.txt file located at %v: %v", beSockPath, err)
		}
		sd.socketPath = string(buff)
		log.Debug("Set socketPath to", sd.socketPath)
		return nil
	}
}

func getEnvRoot() (string, error) {
	// First attempt to pull the environment root from Apollo.
	if env, err := apollo.CurEnvironment().PrimaryEnvironment(); err == nil {
		// If we have an environment, then we need to be able to discover the root.
		if root, err := env.Root(); err != nil {
			return "", errors.Errorf("Unable to determine root of primary Apollo environment: %v", err)
		} else {
			return root, nil
		}
	} else {
		log.Info("Unable to discover primary Apollo environment")
	}

	// Next try to pull the environment root for a desktop
	return getWorkspaceEnvRoot()

	// TODO: Attempt to pull the environmnet root for TOD.
}

func getWorkspaceEnvRoot() (string, error) {
	workspace, err := execBrazilPath("workspace-root")
	if err != nil {
		return "", errors.Wrap(err, "Unable to determine workspace-root")
	}

	packageName, err := execBrazilPath("package-name")
	if err != nil {
		return "", errors.Wrap(err, "Unable to determine package-name")
	}

	user, err := user.Current()
	if err != nil {
		return "", errors.Wrap(err, "Unable to determine current user")
	}

	root := strings.TrimSpace(workspace) + "/AAA/" + strings.TrimSpace(packageName) + "-" + user.Username
	return root, nil
}

func execBrazilPath(recipe string) (string, error) {
	cmd := exec.Command("/apollo/env/SDETools/bin/brazil-path", recipe)
	log.Debug("Executing", cmd.Path, recipe)
	output, err := cmd.Output()
	if err != nil {
		return "", errors.Wrap(err, "Unable to read from stdout")
	}
	return string(output), nil
}

func (sd *SdClient) SanityCheck() error {
	c, err := sd.connect()
	if c != nil {
		c.Close()
	}
	return err
}

// EncodeRequest satisfies aaa.Client
func (sd *SdClient) EncodeRequest(service, operation string, r *http.Request) (clientCxt *aaa.ClientContext, err error) {
	var payload []byte

	if r.Body != nil {
		defer r.Body.Close()
		payload, err = ioutil.ReadAll(r.Body)
		if err != nil {
			err = errors.Errorf("Failed to read request body. %v", err)
			return
		}
	} else {
		payload = []byte{}
	}
	log.Tracef("http.Header pre EncodeRequest: %+v", r.Header)
	in := &encodeRequestInput{
		payload: payload,
		rpc: &rpcInfo{
			service:   service,
			operation: operation,
		},
		httpRequest: &httpRequestInfo{
			verb: r.Method,
			uri:  r.URL.String(),
		},
		headers:     headers(r.Header),
		messageType: httpRequest,
	}

	attempts := 0
	for clientCxt, err = sd.encodeRequest(service, operation, in, r); err != nil && attempts < maxAttempts; attempts++ {
		log.Debug("Retrying call to encodeRequest due to error", err)
		clientCxt, err = sd.encodeRequest(service, operation, in, r)
	}
	if err != nil {
		log.Tracef("http.Header post EncodeRequest: %+v", r.Header)
	}
	return
}

// encodeRequest establishes a connection with the Security Daemon, sends it an encode request, reads the
// response, builds a ClientContext based on the response, and updates the given request with the
// required AAA headers and optionally encrypted body.
func (sd *SdClient) encodeRequest(service, operation string, in *encodeRequestInput, r *http.Request) (clientCxt *aaa.ClientContext, err error) {
	reqBuff, err := buildReq(&encodeRequest, in)
	if err != nil {
		return
	}

	c, err := sd.connect()
	if err != nil {
		return
	}

	// Clean up.  If there isn't an error, then return the connection to the pool
	defer func() {
		if err == nil {
			log.Trace("Releasing connection from encodeReqeust", c)
			sd.release(c)
		} else {
			log.Trace("Closing connection from encodeReqeust", c)
			c.Close()
		}
	}()

	n, err := io.Copy((c), bytes.NewReader(reqBuff))
	log.Trace("Copied", n, "bytes to the socket")
	if err != err {
		err = errors.Errorf("Failed to write encodeRequest to socket: %v", err)
		return
	}

	out := new(encodeRequestOutput)
	_, _, err = readResp((c), out)
	if err != nil {
		err = errors.Errorf("Failed to read encodeRequestResponse from socket: %v", err)
		return
	}

	clientCxt = &aaa.ClientContext{
		KeyId:      string(out.key.id),
		KeyVersion: out.key.version,
		Service:    service,
		Operation:  operation,
	}

	r.Header.Add(aaa.AaaAuthHeader, string(out.authHeader))
	if out.dateHeader != "" && r.Header.Get(aaa.AaaDateHeader) != "" {
		r.Header.Add(aaa.AaaDateHeader, out.dateHeader)
	}
	r.Body = ioutil.NopCloser(bytes.NewBuffer(out.payload))
	r.ContentLength = int64(len(out.payload))

	return
}

// DecodeRequest satisfies aaa.Client
func (sd *SdClient) DecodeResponse(clientCxt *aaa.ClientContext, r *http.Response) (err error) {
	var payload []byte

	if r.Body != nil {
		defer r.Body.Close()
		payload, err = ioutil.ReadAll(r.Body)
		if err != nil {
			err = errors.Errorf("Failed to read request body. %v", err)
			return
		}
	} else {
		payload = []byte{}
	}
	log.Tracef("http.Header pre DecodeResponse: %+v", r.Header)

	in := &decodeResponseInput{
		payload: payload,
		rpc: &rpcInfo{
			service:   clientCxt.Service,
			operation: clientCxt.Operation,
		},
		httpResponse: &httpResponseInfo{
			statusCode: int16(r.StatusCode),
		},
		key: &keyInfo{
			id:      clientCxt.KeyId,
			version: clientCxt.KeyVersion,
		},
		headers:     headers(r.Header),
		messageType: httpRequest,
	}

	attempts := 0
	for err = sd.decodeResponse(in, clientCxt, r); err != nil && attempts < maxAttempts; attempts++ {
		log.Debug("Retrying call to decodeResponse due to error", err)
		err = sd.decodeResponse(in, clientCxt, r)
	}
	if err != nil {
		log.Tracef("http.Header post DecodeResponse: %+v", r.Header)
	}
	return
}

// decodeResponse establishes a connection with the Security Daemon, sends it a decode request with
// the given decodeRequestInput
func (sd *SdClient) decodeResponse(in *decodeResponseInput, clientCxt *aaa.ClientContext, r *http.Response) (err error) {
	reqBuff, err := buildReq(&decodeResponse, in)
	if err != nil {
		return
	}

	c, err := sd.connect()
	if err != nil {
		return
	}

	// Clean up.  If there isn't an error, then return the connection to the pool
	defer func() {
		if err == nil {
			log.Trace("Releasing connection from decodeResponse", c)
			sd.release(c)
		} else {
			log.Trace("Closing connection from decodeResponse", c)
			c.Close()
		}
	}()

	n, err := io.Copy((c), bytes.NewReader(reqBuff))
	log.Trace("Copied", n, "bytes to the socket")
	if err != err {
		err = errors.Errorf("Failed to write decodeResponse to socket: %v", err)
		return err
	}

	out := new(decodeResponseOutput)
	_, sl, err := readResp((c), out)
	if err != nil {
		err = errors.Errorf("Failed to read decodeResponseResponse from socket: %v", err)
		return
	}

	if sl.exceptionInfo != nil && sl.exceptionInfo.code > 0 {
		err = errors.Errorf("Exception from SecurityDaemon when decoding response: %+v", sl.exceptionInfo)
		return
	}

	r.Body = ioutil.NopCloser(bytes.NewBuffer(out.payload))
	return
}
