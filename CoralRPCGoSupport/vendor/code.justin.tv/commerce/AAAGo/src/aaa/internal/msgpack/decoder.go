package msgpack

import (
	"code.justin.tv/commerce/AAAGo/src/aaa/internal/msgpack/code"
	"encoding/binary"
	"fmt"
	"github.com/pkg/errors"
	"io"
)

type MsgPackDecoder interface {
	DecodeMsgPack(*Decoder) error
}

type Peeker interface {
	Peek(n int) ([]byte, error)
}

type ReadPeeker interface {
	io.Reader
	Peeker
}

type Decoder struct {
	r io.Reader
}

func Decode(r io.Reader, d MsgPackDecoder) error {
	decoder := &Decoder{
		r: r,
	}
	return d.DecodeMsgPack(decoder)
}

func (d *Decoder) DecodeNil() error {
	c, err := d.readByte()
	if err != nil {
		return errors.Wrap(err, "Unable to decode nil")
	}
	switch c {
	case code.Nil:
		return nil
	default:
		return errors.Errorf("Cannot decode nil. Not the nil code: %#v", c)
	}
}

func (d *Decoder) DecodeBool() (bool, error) {
	c, err := d.readByte()
	if err != nil {
		return false, err
	}
	switch c {
	case code.BoolFalse:
		return false, nil
	case code.BoolTrue:
		return true, nil
	default:
		return false, fmt.Errorf("Cannot decode bool. Not an bool code: %#v", c)
	}
}

func (d *Decoder) DecodeUint() (uint64, error) {
	c, err := d.readByte()
	if err != nil {
		return 0, err
	}
	if c >= 0x00 && c <= code.PostFixNumMax {
		return uint64(c & code.PostFixNumMax), nil
	}
	switch c {
	case code.Uint8:
		b, err := d.readByte()
		return uint64(b), err
	case code.Uint16:
		i, err := d.readUint16()
		return uint64(i), err
	case code.Uint32:
		i, err := d.readUint32()
		return uint64(i), err
	case code.Uint64:
		return d.readUint64()
	default:
		return 0, fmt.Errorf("Cannot decode uint. Not an uint code: %#v", c)
	}
}

func (d *Decoder) DecodeUint8() (uint8, error) {
	c, err := d.readByte()
	if err != nil {
		return 0, err
	}
	if c != code.Uint8 {
		return 0, fmt.Errorf("Cannot decode uint8. Not the uint8 code: %#v", c)
	}
	return d.readByte()
}

func (d *Decoder) DecodeUint16() (uint16, error) {
	c, err := d.readByte()
	if err != nil {
		return 0, err
	}
	if c != code.Uint16 {
		return 0, fmt.Errorf("Cannot decode uint16. Not the uint16 code: %#v", c)
	}
	return d.readUint16()
}

func (d *Decoder) DecodeUint32() (uint32, error) {
	c, err := d.readByte()
	if err != nil {
		return 0, err
	}
	if c != code.Uint32 {
		return 0, fmt.Errorf("Cannot decode uint32. Not the uint32 code: %#v", c)
	}
	return d.readUint32()
}

func (d *Decoder) DecodeUint64() (uint64, error) {
	c, err := d.readByte()
	if err != nil {
		return 0, err
	}
	if c != code.Uint64 {
		return 0, fmt.Errorf("Cannot decode uint64. Not the uint64 code: %#v", c)
	}
	return d.readUint64()
}

func (d *Decoder) DecodeInt8() (int8, error) {
	c, err := d.readByte()
	if err != nil {
		return 0, err
	}
	if c != code.Int8 {
		return 0, fmt.Errorf("Cannot decode int8. Not the uint8 code: %#v", c)
	}
	b, err := d.readByte()
	if err != nil {
		return 0, err
	}
	return int8(b), nil
}

func (d *Decoder) DecodeInt16() (int16, error) {
	c, err := d.readByte()
	if err != nil {
		return 0, err
	}
	if c != code.Int16 {
		return 0, fmt.Errorf("Cannot decode int16. Not the uint16 code: %#v", c)
	}
	i, err := d.readUint16()
	if err != nil {
		return 0, err
	}
	return int16(i), nil
}

func (d *Decoder) DecodeString() (string, error) {
	b, err := d.DecodeBytes()
	if err != nil {
		return "", err
	}
	return string(b), nil
}

func (d *Decoder) DecodeBytes() ([]byte, error) {
	l, err := d.decodeRawLen()
	if err != nil {
		return nil, err
	}
	buff := make([]byte, int(l))

	if _, err = d.r.Read(buff); err != nil {
		return nil, err
	}
	return buff, nil
}

func (d *Decoder) decodeRawLen() (uint32, error) {
	c, err := d.readByte()
	if err != nil {
		return 0, err
	}
	if c >= code.FixRawLow && c <= code.FixRawHigh {
		return uint32(c & code.FixRawMask), nil
	}
	switch c {
	case code.Raw16:
		i, err := d.readUint16()
		return uint32(i), err
	case code.Raw32:
		return d.readUint32()
	default:
		return 0, fmt.Errorf("Cannot decode raw length. Not an raw code: %#v", c)
	}
}

func (d *Decoder) DecodeArrayLen() (uint32, error) {
	c, err := d.readByte()
	if err != nil {
		return 0, err
	}
	if c >= code.FixArrayLow && c <= code.FixArrayHigh {
		return uint32(c & code.FixArrayMask), nil
	}
	switch c {
	case code.Array16:
		i, err := d.readUint16()
		return uint32(i), err
	case code.Array32:
		return d.readUint32()
	default:
		return 0, fmt.Errorf("Cannot decode array. Not an array code: %#v", c)
	}
}

func (d *Decoder) readByte() (byte, error) {
	buff := make([]byte, 1)
	if _, err := d.r.Read(buff); err != nil {
		return 0, err
	}
	return buff[0], nil
}

func (d *Decoder) readUint16() (uint16, error) {
	buff := make([]byte, 2)
	if _, err := d.r.Read(buff); err != nil {
		return 0, err
	}
	return binary.BigEndian.Uint16(buff), nil
}

func (d *Decoder) readUint32() (uint32, error) {
	buff := make([]byte, 4)
	if _, err := d.r.Read(buff); err != nil {
		return 0, err
	}
	return binary.BigEndian.Uint32(buff), nil
}

func (d *Decoder) readUint64() (uint64, error) {
	buff := make([]byte, 8)
	if _, err := d.r.Read(buff); err != nil {
		return 0, err
	}
	return binary.BigEndian.Uint64(buff), nil
}
