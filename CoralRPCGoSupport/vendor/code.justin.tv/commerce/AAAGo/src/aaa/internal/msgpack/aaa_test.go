/* Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved. */
package msgpack_test

import (
	"code.justin.tv/commerce/AAAGo/src/aaa/internal/msgpack"
	"bytes"
	"fmt"
	"github.com/pkg/errors"
	"math"
	"reflect"
	"testing"
)

var (
	fakeCodecChild = &fakeAAACodec{8, math.MaxUint8, math.MaxUint16, math.MaxUint32, math.MaxUint64, math.MaxInt8,
		math.MaxInt16, testString1, testBytes1, nil}
	fakeCodecParent = &fakeAAACodec{9, math.MaxUint8 - 1, math.MaxUint16 - 1, math.MaxUint32 - 1, math.MaxUint64 - 1,
		math.MinInt8, math.MinInt16, testString2, testBytes1, fakeCodecChild}
)

func TestAAAHelpers(t *testing.T) {
	b, err := msgpack.Encode(fakeCodecParent)
	if err != nil {
		t.Fatal("Unexpected error encoding:", err)
	}
	fcp := &fakeAAACodec{}
	buff := bytes.NewBuffer(b)
	if err := msgpack.Decode(buff, fcp); err != nil {
		t.Error("Unexpected error decoding:", err)
	}
	if !reflect.DeepEqual(fakeCodecParent, fcp) {
		t.Error("Expected\n", *fakeCodecParent, "\nbut found\n", *fcp)
		t.Error("Expected child\n", fakeCodecParent.sub, "\nbut found\n", fcp.sub)
	}
}

type fakeAAACodec struct {
	numFields int
	u8        uint8
	u16       uint16
	u32       uint32
	u64       uint64
	i8        int8
	i16       int16
	st        string
	bt        []byte
	sub       *fakeAAACodec
}

func (f *fakeAAACodec) EncodeMsgPack(e *msgpack.Encoder) error {
	fmt.Println("Encoding numbields", f.numFields)
	if err := e.EncodeOuterArrayStart(f.numFields - 1); err != nil {
		return err
	}
	if err := e.EncodeUInt8Field(1, f.u8); err != nil {
		return err
	}
	if err := e.EncodeUInt16Field(2, f.u16); err != nil {
		return err
	}
	if err := e.EncodeUInt32Field(3, f.u32); err != nil {
		return err
	}
	if err := e.EncodeUInt64Field(4, f.u64); err != nil {
		return err
	}
	if err := e.EncodeInt8Field(5, f.i8); err != nil {
		return err
	}
	if err := e.EncodeInt16Field(6, f.i16); err != nil {
		return err
	}
	if err := e.EncodeStringField(7, f.st); err != nil {
		return err
	}
	if err := e.EncodeBytesField(8, f.bt); err != nil {
		return err
	}
	if f.sub != nil {
		fmt.Println("Encoding child")
		if err := e.EncodeSubField(9, f.sub); err != nil {
			return err
		}
	}
	return nil
}

func (f *fakeAAACodec) DecodeMsgPack(d *msgpack.Decoder) error {
	var err error
	// EncodeOuterArrayStart
	if val, err := d.DecodeArrayLen(); err != nil {
		return err
	} else {
		f.numFields = int(val)
	}
	if val, err := d.DecodeArrayLen(); err != nil || val != 0 {
		return errors.Errorf("Unexpected error or wrong array len: %v %d", err, val)
	}

	if err = decodeFieldHeader(1, d); err != nil {
		return errors.Wrap(err, "decodeFieldHeader uint8")
	}
	if f.u8, err = d.DecodeUint8(); err != nil {
		return err
	}

	if err = decodeFieldHeader(2, d); err != nil {
		return errors.Wrap(err, "decodeFieldHeader uint16")
	}
	if f.u16, err = d.DecodeUint16(); err != nil {
		return err
	}

	if err = decodeFieldHeader(3, d); err != nil {
		return errors.Wrap(err, "decodeFieldHeader uint32")
	}
	if f.u32, err = d.DecodeUint32(); err != nil {
		return err
	}

	if err = decodeFieldHeader(4, d); err != nil {
		return errors.Wrap(err, "decodeFieldHeader uint64")
	}
	if f.u64, err = d.DecodeUint64(); err != nil {
		return err
	}

	if err = decodeFieldHeader(5, d); err != nil {
		return errors.Wrap(err, "decodeFieldHeader int8")
	}
	if f.i8, err = d.DecodeInt8(); err != nil {
		return err
	}

	if err = decodeFieldHeader(6, d); err != nil {
		return errors.Wrap(err, "decodeFieldHeader int16")
	}
	if f.i16, err = d.DecodeInt16(); err != nil {
		return err
	}

	if err = decodeFieldHeader(7, d); err != nil {
		return errors.Wrap(err, "decodeFieldHeader string")
	}
	if f.st, err = d.DecodeString(); err != nil {
		return err
	}

	if err = decodeFieldHeader(8, d); err != nil {
		return errors.Wrap(err, "decodeFieldHeader bytes")
	}
	if f.bt, err = d.DecodeBytes(); err != nil {
		return err
	}

	if f.numFields == 9 {
		if err = decodeFieldHeader(9, d); err != nil {
			return errors.Wrap(err, "decodeFieldHeader sub")
		}
		f.sub = &fakeAAACodec{}
		if err = f.sub.DecodeMsgPack(d); err != nil {
			return errors.Wrap(err, "decode child")
		}
	}
	return nil
}

func decodeFieldHeader(index uint32, d *msgpack.Decoder) error {
	if n, err := d.DecodeArrayLen(); err != nil || n != 2 {
		return errors.Errorf("Unexpected error or wrong array len %v %d", err, n)
	}
	if i, err := d.DecodeUint32(); err != nil || i != index {
		return errors.Errorf("Unexpected error or wrong index %v %d %d", err, i, index)
	}
	return nil
}
