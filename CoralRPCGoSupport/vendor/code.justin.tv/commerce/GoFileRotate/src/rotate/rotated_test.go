package rotate

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
	"time"
)

const (
	validPath            = "/tmp/testFile.txt"
	invalidPathRights    = "/root/GoLogTest/testFile.txt"
	invalidPathDirectory = "/tmp"
	validTimeLocation    = "America/New_York"
	invalidTimeLocation  = "foo"
)

var (
	defaultTestMessage = []byte("test message\n")
)

func Test_NewTimeRotatingFile_Default(t *testing.T) {
	_, err := NewTimeRotatingFile(validPath)
	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}
}

func Test_NewTimeRotatingFile_NoOptionError(t *testing.T) {
	option := func(f *RotatingFile) error {
		return nil
	}
	_, err := NewTimeRotatingFile(validPath, option)
	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}
}

func Test_NewTimeRotatingFile_OptionError(t *testing.T) {
	option := func(f *RotatingFile) error {
		return errors.New("error")
	}
	_, err := NewTimeRotatingFile(validPath, option)
	if err == nil {
		t.Fatal("Expected an error, however none was returned.")
	}
}

func Test_NewTimeRotatingFile_InvalidPathDirectory(t *testing.T) {
	_, err := NewTimeRotatingFile(invalidPathDirectory)
	if err == nil {
		t.Fatal("Expected an error, however none was returned.")
	}
}

func Test_NewTimeRotatingFile_CreateDirectoryFail(t *testing.T) {
	_, err := NewTimeRotatingFile(invalidPathRights)
	if err == nil {
		t.Fatal("Expected an error, however none was returned.")
	}
}

func Test_RotationFreq(t *testing.T) {
	f := new(RotatingFile)
	freq := RotateHourly
	err := RotationFreq(freq)(f)
	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}
}

func Test_TimeLocation_Invalid(t *testing.T) {
	f := new(RotatingFile)
	err := TimeLocation(invalidTimeLocation)(f)
	if err == nil {
		t.Fatal("Expected an error, however none was returned.")
	}
}

func Test_TimeLocation_Valid(t *testing.T) {
	f := new(RotatingFile)
	err := TimeLocation(validTimeLocation)(f)
	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}
}

func Test_Write(t *testing.T) {
	f, err := NewTimeRotatingFile(validPath)
	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}
	n, err := f.Write(defaultTestMessage)
	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}
	if n != len(defaultTestMessage) {
		t.Fatalf("Failed to write all bytes. Expected: %v, Actual: %v", len(defaultTestMessage), n)
	}
}

func Test_TimedWrite_NilTime(t *testing.T) {
	f, err := NewTimeRotatingFile(validPath)
	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}
	_, err = f.TimedWrite(defaultTestMessage, nil)
	if err == nil {
		t.Fatal("Expected an error, however none was returned.")
	}
}

func Test_TimedWrite_NilP(t *testing.T) {
	f, err := NewTimeRotatingFile(validPath)
	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}
	ti := time.Now()
	n, err := f.TimedWrite(nil, &ti)
	if err != nil {
		t.Fatal("Expected an error, however none was returned")
	}
	if n != 0 {
		t.Fatalf("No bytes should have been written. Actual: %v. Expected: %v", n, 0)
	}
}

func Test_fhTimeOut_ResetTimer(t *testing.T) {
	f, err := NewTimeRotatingFile(validPath)
	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}
	postfix := time.Now().UTC().Format(string(f.freq))
	_, err = f.fetchFileHandle(postfix)
	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}
	f.fhTimeout(postfix)
	if file, ok := f.files[postfix]; ok {
		defer file.file.Close()
		if !file.timer.Stop() {
			t.Fatal("File Handler timeout timer was not correctly restarted.")
		}
	} else {
		t.Fatal("File Handler was incorrectly closed.")
	}
}

func Test_fhTimeOut_Valid(t *testing.T) {
	f, err := NewTimeRotatingFile(validPath)
	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}
	postfix := time.Now().UTC().Format(string(f.freq))
	_, err = f.fetchFileHandle(postfix)
	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}
	if file, ok := f.files[postfix]; ok {
		if !file.timer.Stop() {
			t.Fatal("Failed to stop the timeout timer.")
		}
	} else {
		t.Fatal("Failed to retrieve File Handler")
	}
	f.fhTimeout(postfix)
	if file, ok := f.files[postfix]; ok {
		defer file.file.Close()
		t.Fatal("fhTimeout did not properly close and remove File Handler")
	}
}

type timeNowSource struct {
	present   *time.Time
	frequency time.Duration
}

func newTimeNowSource(frequency string) *timeNowSource {
	t := time.Date(2000, time.January, int(time.Monday), 1, 0, 0, 0, time.UTC)
	f, err := time.ParseDuration(frequency)
	if err != nil {
		panic(err)
	}
	return &timeNowSource{
		present:   &t,
		frequency: f,
	}
}

func (ts *timeNowSource) now() time.Time {
	now := *ts.present
	*ts.present = ts.present.Add(ts.frequency)
	return now
}

func removeAll(dir string) {
	if err := os.RemoveAll(dir); err != nil {
		panic(err)
	}
}

type tempDir struct {
	dirname string
}

func newTempDir(prefix string) tempDir {
	var dirname string
	var err error
	if dirname, err = ioutil.TempDir("", fmt.Sprintf("%s", prefix)); err != nil {
		panic(err)
	}
	return tempDir{
		dirname: dirname,
	}

}

func (d tempDir) removeAll() {
	removeAll(d.dirname)
}

func (d tempDir) readDir() []os.FileInfo {
	is, err := ioutil.ReadDir(d.dirname)
	if err != nil {
		panic(err)
	}
	return is
}

func writeMessagesAtFrequency(t testing.TB, messagesPerInterval int, totalIntervals int, frequency string, validator func(testing.TB, []os.FileInfo)) {
	ts := newTimeNowSource(frequency)
	dir := newTempDir(frequency)
	defer dir.removeAll()

	logPrefix := fmt.Sprintf("every-%s", frequency)
	f, err := NewTimeRotatingFile(filepath.Join(dir.dirname, logPrefix), SkipCreateCheck())
	if err != nil {
		t.Fatalf("Unexpected error: %v", err)
	}
	for i := 1; i <= totalIntervals; i++ {
		now := ts.now().UTC()
		for j := 0; j < messagesPerInterval; j++ {
			msg := []byte(fmt.Sprintf("%s\n", now.String()))
			// Use timedWrite directly, to avoid incrementing time
			n, err := f.TimedWrite(msg, &now)
			if err != nil {
				t.Fatalf("Unexpected error from TimedWrite: %v", err)
			}
			if n != len(msg) {
				t.Fatalf("Failed to write all bytes. Expected: %v, Actual: %v", len(defaultTestMessage), n)
			}
		}
	}
	validator(t, dir.readDir())
}

func Test_OneEvery90Minutes(t *testing.T) {
	writeMessagesAtFrequency(t, 1, 10, "90m", func(t testing.TB, is []os.FileInfo) {
		expected := map[string]bool{
			"every-90m.2000-01-01-01": false,
			"every-90m.2000-01-01-02": false,
			"every-90m.2000-01-01-04": false,
			"every-90m.2000-01-01-05": false,
			"every-90m.2000-01-01-07": false,
			"every-90m.2000-01-01-08": false,
			"every-90m.2000-01-01-10": false,
			"every-90m.2000-01-01-11": false,
			"every-90m.2000-01-01-13": false,
			"every-90m.2000-01-01-14": false,
		}
		var expectedSize int64
		for _, fi := range is {
			if fi.Size() < 1 {
				t.Errorf("Zero-length file %s", fi.Name())
			}
			if expectedSize == 0 {
				expectedSize = fi.Size()
			} else {
				if fi.Size() != expectedSize {
					t.Errorf("Mismatched log file size %s: expected %d actual: %d", fi.Name(), expectedSize, fi.Size())
				}
			}

			if _, ok := expected[fi.Name()]; !ok {
				t.Errorf("Unexpected logfile left over: %s", fi.Name())
			} else {
				delete(expected, fi.Name())
			}
		}
		for k := range expected {
			t.Errorf("Logfile not found: %s", k)
		}
	})
}

func Test_Every60Minutes(t *testing.T) {
	expectedLogFiles := 100
	writeMessagesAtFrequency(t, 100, expectedLogFiles, "60m", func(t testing.TB, is []os.FileInfo) {
		if len(is) != expectedLogFiles {
			t.Errorf("Unexpected number of log files created. Expected: %d, actual: %d", expectedLogFiles, len(is))
		}
	})
}

func BenchmarkTimedWrite(b *testing.B) {
	b.SetParallelism(100)
	b.RunParallel(func(pb *testing.PB) {
		ts := newTimeNowSource("0.01s")
		dir := newTempDir("benchmarky")
		defer dir.removeAll()
		f, err := NewTimeRotatingFile(filepath.Join(dir.dirname, fmt.Sprintf("bench-%d", b.N)))
		if err != nil {
			b.Fatalf("Unexpected error: %v", err)
		}
		now := ts.now()
		for pb.Next() {
			f.TimedWrite([]byte("bench\n"), &now)
		}
	})
}
