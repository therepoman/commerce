package log

import (
	"code.justin.tv/commerce/GoFileRotate/src/rotate"
	"path/filepath"
)

const (
	logDir = "var/output/logs"
)

// Constructs a new time based rotated log file in the Amazon standard log directory.
// envDir: Root directory of the apollo environment.
// logName: Name of the log file.
// options: GoFileRotate/rotate RotatingFile constructor options.
func NewLogFile(envDir string, logName string, options ...func(*rotate.RotatingFile) error) (*rotate.RotatingFile, error) {
	path := filepath.Join(envDir, logDir, logName)
	return rotate.NewTimeRotatingFile(path, options...)
}
