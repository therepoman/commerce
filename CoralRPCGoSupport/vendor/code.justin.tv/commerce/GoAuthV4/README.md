# AWS AuthV4 library for Go.

Includes a Signer object for Go clients to sign http requests ( see: http://docs.aws.amazon.com/general/latest/gr/signature-version-4.html ).
The Signer passes the Signature Version 4 Test Suite ( see http://docs.aws.amazon.com/general/latest/gr/signature-v4-test-suite.html ).

Includes an Authorizer object for Go servers to use to authorize client AuthV4 connections via ARPS.
NOTE: Server deployments will need to have their accounts WhiteListed AND have CryptoOffload enabled.
See: https://w.amazon.com/index.php/AWSAuth/Onboarding

# Client Usage

	import (
	   "GoAuthV4/authv4"
	              ...
	)
	
	              ...
		
	req, err := http.NewRequest("GET", "http://localhost:5555/action", nil)
	if err != nil {
                      ...
	}

	// NOTE: likely you want to do this one time when your client initializes (not every request) and pass reference
	// NOTE: use Odin to fetch the accessKey and secretKey, see Brazil package OdinLocalGoClient
	signer := authv4.NewSigner(region, service, accessKey, secretKey, nil)
	signer.Sign(req)
        
	client := &http.Client{}

	res, err := client.Do(req)
	if err != nil {
                      ...
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
                      ...
	}


# Server Usage

	import (
	   "GoAuthV4/authv4/arps"
	              ...
	)

	// initialize
	accessKey, secretKey, err := odin.CredentialPair(config.mset)
	if err != nil {
	   ...
	}
	authorizer, err = arps.NewARPSAuthorizer(config.region, config.service, accessKey, secretKey, config.endpoint, config.whitelist)
	if err != nil {
	   ...
	}

	...

	// inside 'master' request handler in your server app
	authObj, err := authorizer.Authenticate(r)
	if err != nil {
	   authmsg = err.Error()
	   authorized = false
	}
	accessKey = authObj.AccessKey()
        

Also see authv4/sign_test.go and authv4/arps/arps_test.go for examples of usage.

# Test
	brazil-build test

# Code Coverage
See build/brazil-documentation/coverage after running `brazil-build test`.

