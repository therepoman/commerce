package authv4

// Review code coverage in: build/brazil-documentation/coverage

import (
	"bytes"
	"errors"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"time"
)

var (
	// set to true for verbose logging
	debug = false

	// from Amazon authv4 test suite
	region      = "us-east-1"
	service     = "host"
	accessKey   = "AKIDEXAMPLE"
	secretKey   = "wJalrXUtnFEMI/K7MDENG+bPxRfiCYEXAMPLEKEY"
	refdate     = "Mon, 09 Sep 2011 23:36:00 GMT"
	testdate, _ = time.Parse(time.RFC1123, refdate)
)

type TestLogger struct {
	logger *log.Logger
}

func NewTestLogger() *TestLogger {
	return &TestLogger{log.New(os.Stdout, "[test] ", 0)}
}
func (l *TestLogger) Debugf(format string, params ...interface{}) {
	if debug {
		l.logger.Printf(format, params...)
	}
}

// reads req file and creates an http request to match
func getTestReq(filename string) (*http.Request, []string, error) {

	reqtext, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, nil, err
	}

	reqarray := strings.Split(string(reqtext), "\n")

	// first line should be method and path
	first_line := strings.Split(reqarray[0], " ")
	method := strings.TrimSpace(first_line[0])
	path := strings.TrimSpace(first_line[1])

	// headers follow, we need to find the host to make the url
	host := ""
	for _, line := range reqarray {
		if strings.HasPrefix(line, "h") || strings.HasPrefix(line, "H") {
			parsed_line := strings.Split(line, ":")
			host = strings.TrimSpace(parsed_line[1])
			break
		}
	}
	if host == "" {
		return nil, nil, errors.New("No host header found in " + filename)
	}
	requrl := "http://" + host + path

	// now find the body
	body := ""
	for i, line := range reqarray {
		if strings.TrimSpace(line) == "" {
			for j := i + 1; j < len(reqarray); j++ {
				body += reqarray[j]
			}
			break
		}
	}

	// make an http request
	r, err := http.NewRequest(method, requrl, strings.NewReader(body))
	if err != nil {
		return r, nil, err
	}

	// add headers
	var headers []string
	for i, line := range reqarray {

		if i == 0 || len(line) == 0 {
			continue
		}
		if strings.HasPrefix(line, "\n\n") {
			break
		}

		parsed_line := strings.SplitN(line, ":", 2)
		if len(parsed_line) == 2 {
			h := strings.TrimSpace(parsed_line[0])
			v := strings.TrimSpace(parsed_line[1])
			r.Header.Add(h, v)
			headers = append(headers, h)
		}
	}

	return r, headers, err
}

func getTestAuthZ(filename string) (string, error) {
	authz, err := ioutil.ReadFile(filename)
	if err != nil {
		return "", err
	}
	return string(authz), err
}

func doTest(testname string, t *testing.T) {

	r, headers, err := getTestReq(testname + ".req")
	if err != nil {
		msg := err.Error()

		// the test 'post-vanilla-query-nonunreserved' has URL that Go refuses to parse, skip
		if strings.Contains(msg, "invalid URL escape") {
			return
		} else {
			t.Error("Fail ", testname, msg)
			return
		}
	}

	signer := NewSigner(region, service, accessKey, secretKey, headers)
	_, authz, _ := signer.sign(testdate, SignableRequest{r, true})

	// check
	reference, err := getTestAuthZ(testname + ".authz")
	if err != nil {
		t.Error("Fail ", testname, err)
		return
	}

	if authz != reference {
		t.Error("Fail ", testname, "Does not match expected authz")
		return
	}

}

func TestAWSv4Suite(t *testing.T) {

	dir := "testdata/aws4_testsuite/"

	if false {

		// cherry pick ones to debug
		doTest(dir+"get-vanilla-ut8-query", t)

	} else {

		// get the test file prefixes (read all files, strip suffix)
		testnames := make(map[string]bool, 1)
		fileinfos, err := ioutil.ReadDir(dir)
		if err != nil {
			t.Fatal(err)
		}
		for _, fileinfo := range fileinfos {
			testnames[strings.Split(fileinfo.Name(), ".")[0]] = true
		}

		// for each test, run
		for testname, _ := range testnames {
			if testname == "" {
				continue
			}
			t.Log(testname)
			doTest(dir+testname, t)
		}
	}
}

var (
	testAccessKey     = "AK"
	testAccountId     = "123"
	testClient        *AuthenticatedClient
	testClientNoStrip *AuthenticatedClient
)

type TestAuthenticator struct {
	alwaysFail       bool
	failOnSecondAuth bool
	dbg              bool
	counter          int
}

func NewTestAuthenticator() *TestAuthenticator {
	testClientNoStrip = nil // Don't disrupt other tests
	return &TestAuthenticator{false, false, debug, 0}
}

func NewTestAuthenticatorBadClient() *TestAuthenticator {
	testClientNoStrip = nil // Don't disrupt other tests
	return &TestAuthenticator{true, false, debug, 0}
}

func NewTestAuthenticatorBadClient2() *TestAuthenticator {
	testClientNoStrip = nil // Don't disrupt other tests
	return &TestAuthenticator{false, true, debug, 0}
}

func (ta *TestAuthenticator) Authenticate(claim *AuthV4Claim) (*AuthenticatedClient, error) {
	ta.counter++

	if ta.alwaysFail || (ta.failOnSecondAuth && ta.counter == 2) {
		return nil, errors.New("Authentication failed")
	} else if !claim.StripDefaultPorts && testClientNoStrip != nil {
		return testClientNoStrip, nil
	} else {
		return testClient, nil
	}
}

func (ta *TestAuthenticator) SetDebugLogger(debugLogger DebugLogger) {
	ta.dbg = true
}

func (ta *TestAuthenticator) DebugLoggerEnable() {
	ta.dbg = true
}

func (ta *TestAuthenticator) DebugLoggerDisable() {
	ta.dbg = false
}

func (ta *TestAuthenticator) DebugLoggerIsEnabled() bool {
	return ta.dbg
}

func TestHTTPRoundTripGET(t *testing.T) {

	testClient = NewAuthenticatedClient(testAccessKey, testAccountId, SigningKey(nil))

	authn := NewTestAuthenticator()
	authz := NewAuthorizer(authn, nil)

	logger := NewTestLogger()
	authz.SetDebugLogger(logger)

	// exercise logger
	authz.DebugLoggerDisable()
	if authz.DebugLoggerIsEnabled() {
		t.Error(errors.New("Logger should be disabled"))
	}
	authz.DebugLoggerEnable()
	if !authz.DebugLoggerIsEnabled() {
		t.Error(errors.New("Logger should be enabled"))
	}
	authz.SetDebugLogger(nil)
	authz.SetDebugLogger(logger)

	// exercise cache (and reset)
	cacheSize := 20
	authz.SetCacheExpiration(time.Hour * 24)
	if authz.CacheExpiration() != time.Hour*24 {
		t.Error(errors.New("Cache expiration is not correct"))
	}
	authz.SetCacheSize(cacheSize)
	if authz.CacheCapacity() != cacheSize {
		t.Error(errors.New("Cache size is not correct"))
	}
	if authz.CacheSize() != 0 { // should be empty
		t.Error(errors.New("Cache size is not correct"))
	}

	handler := func(w http.ResponseWriter, r *http.Request) {

		success := "true"

		// validate the request
		ac, err := authz.Authenticate(r)
		if err != nil {
			success = err.Error()
		}

		// exercise ac to get code coverage
		logger.Debugf("Authenticated User: %s,%s,%s,%s,%s,%s,%d,%d",
			ac.AccessKey(),
			ac.AccountId(),
			ac.Created().Format(iso8601BasicFmt),
			ac.LastUpdated().Format(iso8601BasicFmt),
			ac.Expires().Format(iso8601BasicFmt),
			ac.RequestCount(),
			ac.FailureCount())
		ac.SetExpires(ac.Created().Add(time.Hour * 24))

		// send status back
		_, err = w.Write([]byte(success))
		if err != nil {
			t.Fatal(err)
		}
	}

	signer := NewSigner(region, service, accessKey, secretKey, nil)

	signer.SetDebugLogger(logger)

	// excercise logger
	signer.DebugLoggerDisable()
	if signer.DebugLoggerIsEnabled() {
		t.Error(errors.New("Logger should be disabled"))
	}
	signer.DebugLoggerEnable()
	if !signer.DebugLoggerIsEnabled() {
		t.Error(errors.New("Logger should be enabled"))
	}

	req, err := http.NewRequest("GET", "http://localhost:5555/test?a=foo", nil)
	if err != nil {
		t.Fatal(err)
	}

	// exercise Sign()
	err = signer.Sign(req)
	if err != nil {
		t.Fatal(err)
	}

	signingKey, _, _ := signer.sign(time.Now(), SignableRequest{req, true})
	testClient.signingKey = signingKey // so they match

	w := httptest.NewRecorder()
	handler(w, req)

	if w.Code != http.StatusOK {
		t.Fatal("Bad status: ", w.Code)
	}

	success := w.Body.String()

	if success != "true" {
		t.Fatal("Error, round trip failed: ", success)
	}

}

func TestHTTPRoundTripPOST(t *testing.T) {

	testClient = NewAuthenticatedClient(testAccessKey, testAccountId, SigningKey(nil))

	authn := NewTestAuthenticator()
	authz := NewAuthorizer(authn, nil)

	logger := NewTestLogger()
	authz.SetDebugLogger(logger)

	handler := func(w http.ResponseWriter, r *http.Request) {

		success := "true"

		// validate the request
		_, err := authz.Authenticate(r)
		if err != nil {
			success = err.Error()
		}

		// send status back
		_, err = w.Write([]byte(success))
		if err != nil {
			t.Fatal(err)
		}
	}

	signer := NewSigner(region, service, accessKey, secretKey, nil)

	signer.SetDebugLogger(logger)

	req, err := http.NewRequest("POST", "http://localhost:5555/test", bytes.NewReader([]byte("hi")))
	if err != nil {
		t.Fatal(err)
	}

	signingKey, _, _ := signer.sign(time.Now(), SignableRequest{req, true})
	// Calling it twice should be idempotent
	signingKey, _, _ = signer.sign(time.Now(), SignableRequest{req, true})
	testClient.signingKey = signingKey // so they match

	w := httptest.NewRecorder()
	handler(w, req)

	if w.Code != http.StatusOK {
		t.Fatal("Bad status: ", w.Code)
	}

	success := w.Body.String()

	if success != "true" {
		t.Fatal("Error, round trip failed: ", success)
	}

}

func TestHTTPRoundTripReadSeeker(t *testing.T) {

	testClient = NewAuthenticatedClient(testAccessKey, testAccountId, SigningKey(nil))

	authn := NewTestAuthenticator()
	authz := NewAuthorizer(authn, nil)

	logger := NewTestLogger()
	authz.SetDebugLogger(logger)

	handler := func(w http.ResponseWriter, r *http.Request) {

		success := "true"

		// validate the request
		_, err := authz.Authenticate(r)
		if err != nil {
			success = err.Error()
		}

		// send status back
		_, err = w.Write([]byte(success))
		if err != nil {
			t.Fatal(err)
		}
	}

	signer := NewSigner(region, service, accessKey, secretKey, nil)

	signer.SetDebugLogger(logger)

	file, err := os.Open("/etc/passwd")
	if err != nil {
		t.Fatal(err.Error())
	}
	req, err := http.NewRequest("POST", "http://localhost:5555/test", file)
	if err != nil {
		t.Fatal(err)
	}

	signingKey, _, _ := signer.sign(time.Now(), SignableRequest{req, true})
	// Doing it twice should be idempotent
	signingKey, _, _ = signer.sign(time.Now(), SignableRequest{req, true})
	testClient.signingKey = signingKey // so they match

	w := httptest.NewRecorder()
	handler(w, req)

	if w.Code != http.StatusOK {
		t.Fatal("Bad status: ", w.Code)
	}

	success := w.Body.String()

	if success != "true" {
		t.Fatal("Error, round trip failed: ", success)
	}

}

type readSeekerFail struct {
	failSeek bool
	failRead bool
	counter  int
}

func (rs *readSeekerFail) Close() error {
	return nil
}
func (rs *readSeekerFail) Seek(o int64, w int) (int64, error) {
	if rs.failSeek {
		return o, errors.New("dog won't hunt!")
	} else {
		return o, nil
	}
}
func (rs *readSeekerFail) Read(p []byte) (n int, err error) {
	if rs.failRead {
		return 0, errors.New("dog won't hunt!")
	}

	if rs.counter == 0 {
		return 0, io.EOF
	} else {
		p[0] = 1
		rs.counter--
		return 1, nil
	}
}
func newReadSeekerFailSeek() *readSeekerFail {
	return &readSeekerFail{true, false, 1}
}
func newReadSeekerFailRead() *readSeekerFail {
	return &readSeekerFail{false, true, 1}
}

func TestReadSeekerFail(t *testing.T) {

	signer := NewSigner(region, service, accessKey, secretKey, nil)

	// fail seek
	req, err := http.NewRequest("POST", "http://localhost:5555/test", newReadSeekerFailSeek())
	if err != nil {
		t.Fatal(err)
	}
	_, _, err = signer.sign(time.Now(), SignableRequest{req, true})
	if err == nil {
		t.Fatal("Seek() should have failed but did not!")
	}

	// fail read
	req, err = http.NewRequest("POST", "http://localhost:5555/test", newReadSeekerFailRead())
	if err != nil {
		t.Fatal(err)
	}
	_, _, err = signer.sign(time.Now(), SignableRequest{req, true})
	if err == nil {
		t.Fatal("Read() should have failed but did not!")
	}

	// server side req testing ...
	authn := NewTestAuthenticator()
	authz := NewAuthorizer(authn, nil)

	// seek fail
	req, err = http.NewRequest("GET", "http://localhost:5555/test?a=foo", nil)
	if err != nil {
		t.Fatal(err)
	}
	signer.sign(time.Now(), SignableRequest{req, true})
	// now that we have a valid signed request, replace req.Body with seeker that will fail
	req.Body = newReadSeekerFailSeek()
	_, err = authz.Authenticate(req)
	if err == nil {
		t.Fatal("Seek() should have failed but did not!")
	}

}

func TestHTTPRoundTripGET2X(t *testing.T) {

	testClient = NewAuthenticatedClient(testAccessKey, testAccountId, SigningKey(nil))

	authn := NewTestAuthenticator()
	authz := NewAuthorizer(authn, nil)

	logger := NewTestLogger()
	authz.SetDebugLogger(logger)

	handler := func(w http.ResponseWriter, r *http.Request) {

		success := "true"

		// validate the request
		_, err := authz.Authenticate(r)
		if err != nil {
			success = err.Error()
		}

		// send status back
		_, err = w.Write([]byte(success))
		if err != nil {
			t.Fatal(err)
		}
	}

	signer := NewSigner(region, service, accessKey, secretKey, nil)

	signer.SetDebugLogger(logger)

	// do more than 1, force no caching of credentials to exercise that authz path
	testClient.SetExpires(time.Now().UTC())
	for i := 0; i < 2; i++ {

		req, err := http.NewRequest("GET", "http://localhost:5555/test?a=foo", nil)
		if err != nil {
			t.Fatal(err)
		}

		signingKey, _, _ := signer.sign(time.Now(), SignableRequest{req, true})
		testClient.signingKey = signingKey // so they match

		w := httptest.NewRecorder()
		handler(w, req)

		if w.Code != http.StatusOK {
			t.Fatal("Bad status: ", w.Code)
		}

		success := w.Body.String()

		if success != "true" {
			t.Fatal("Error, round trip failed: ", success)
		}

	}
}

func doGETStress(times int, t *testing.T) {

	// NO LOGGING!

	testClient = NewAuthenticatedClient(testAccessKey, testAccountId, SigningKey(nil))

	authn := NewTestAuthenticator()
	authz := NewAuthorizer(authn, nil)

	handler := func(w http.ResponseWriter, r *http.Request) {

		success := "true"

		// validate the request
		_, err := authz.Authenticate(r)
		if err != nil {
			success = err.Error()
		}

		// send status back
		_, err = w.Write([]byte(success))
		if err != nil {
			t.Fatal(err)
		}
	}

	signer := NewSigner(region, service, accessKey, secretKey, nil)

	// do more than 1, tests cacheing of credenticals
	for i := 0; i < times; i++ {

		req, err := http.NewRequest("GET", "http://localhost:5555/test?a=foo", nil)
		if err != nil {
			t.Error(err)
		}

		signingKey, _, _ := signer.sign(time.Now(), SignableRequest{req, true})
		testClient.signingKey = signingKey // so they match

		w := httptest.NewRecorder()
		handler(w, req)

		if w.Code != http.StatusOK {
			t.Error("Bad status: ", w.Code)
		}

		success := w.Body.String()

		if success != "true" {
			t.Error("Error, round trip failed: ", success)
		}

	}
}

func TestHTTPRoundTripGET20K(t *testing.T) {
	doGETStress(20000, t)
}

func TestHTTPRoundAuthFail(t *testing.T) {

	testClient = NewAuthenticatedClient(testAccessKey, testAccountId, SigningKey(nil))

	authn := NewTestAuthenticatorBadClient()
	authz := NewAuthorizer(authn, nil)

	logger := NewTestLogger()
	authz.SetDebugLogger(logger)

	handler := func(w http.ResponseWriter, r *http.Request) {

		success := "true"

		// validate the request
		_, err := authz.Authenticate(r)
		if err != nil {
			success = err.Error() // this is what we expect
		}

		// send status back
		_, err = w.Write([]byte(success))
		if err != nil {
			t.Fatal(err)
		}
	}

	signer := NewSigner(region, service, accessKey, secretKey, nil)

	signer.SetDebugLogger(logger)

	req, err := http.NewRequest("GET", "http://localhost:5555/test?a=foo", nil)
	if err != nil {
		t.Fatal(err)
	}

	signingKey, _, _ := signer.sign(time.Now(), SignableRequest{req, true})
	testClient.signingKey = signingKey // so they match

	w := httptest.NewRecorder()
	handler(w, req)

	if w.Code != http.StatusOK {
		t.Fatal("Bad status: ", w.Code)
	}

	success := w.Body.String()

	if success != "Authentication failed" {
		t.Fatal("Error, round trip failed: ", success)
	}

}

func TestHTTPRoundAuthFailStripPorts(t *testing.T) {

	testClient = NewAuthenticatedClient(testAccessKey, testAccountId, SigningKey(nil))

	authn := NewTestAuthenticator()
	authz := NewAuthorizer(authn, nil)

	logger := NewTestLogger()
	authz.SetDebugLogger(logger)

	handler := func(w http.ResponseWriter, r *http.Request) {

		success := "true"

		// validate the request
		_, err := authz.tryAuthenticate(r, false)
		if err != nil {
			success = err.Error() // this is what we expect
		}

		// send status back
		_, err = w.Write([]byte(success))
		if err != nil {
			t.Fatal(err)
		}
	}

	signer := NewSigner(region, service, accessKey, secretKey, nil)

	signer.SetDebugLogger(logger)

	req, err := http.NewRequest("GET", "https://localhost:443/test?a=foo", nil)
	if err != nil {
		t.Fatal(err)
	}

	signingKey, _, _ := signer.sign(time.Now(), SignableRequest{req, true})
	testClient.signingKey = signingKey // so they match

	w := httptest.NewRecorder()
	handler(w, req)

	if w.Code != http.StatusOK {
		t.Fatal("Bad status: ", w.Code)
	}

	success := w.Body.String()

	if success != "Signatures do not match" {
		t.Fatal("Error, round trip failed: ", success)
	}
}

func TestHTTPRoundAuthSuccessStripPorts(t *testing.T) {

	testClient = NewAuthenticatedClient(testAccessKey, testAccountId, SigningKey(nil))

	authn := NewTestAuthenticator()
	authz := NewAuthorizer(authn, nil)

	testClientNoStrip = NewAuthenticatedClient(testAccessKey, testAccountId, SigningKey(nil))

	logger := NewTestLogger()
	authz.SetDebugLogger(logger)

	handler := func(w http.ResponseWriter, r *http.Request) {

		success := "true"

		// validate the request
		_, err := authz.Authenticate(r) // Authenticate tries both
		if err != nil {
			success = err.Error() // this is what we expect
		}

		// send status back
		_, err = w.Write([]byte(success))
		if err != nil {
			t.Fatal(err)
		}
	}

	signer := NewSigner(region, service, accessKey, secretKey, nil)

	signer.SetDebugLogger(logger)

	req, err := http.NewRequest("GET", "https://localhost:443/test?a=foo", nil)
	if err != nil {
		t.Fatal(err)
	}

	signingKey, _, _ := signer.sign(time.Now(), SignableRequest{req, true})
	testClient.signingKey = signingKey        // so they match
	testClientNoStrip.signingKey = signingKey // so they match

	w := httptest.NewRecorder()
	handler(w, req)

	if w.Code != http.StatusOK {
		t.Fatal("Bad status: ", w.Code)
	}

	success := w.Body.String()

	if success != "true" {
		t.Fatal("Error, round trip failed: ", success)
	}

	// Try again with no stripping of ports

	req, err = http.NewRequest("GET", "https://localhost:443/test?a=foo", nil)
	if err != nil {
		t.Fatal(err)
	}

	signingKey, _, _ = signer.sign(time.Now(), SignableRequest{req, false})
	testClient.signingKey = signingKey        // so they match
	testClientNoStrip.signingKey = signingKey // so they match

	w = httptest.NewRecorder()
	handler(w, req)

	if w.Code != http.StatusOK {
		t.Fatal("Bad status: ", w.Code)
	}

	success = w.Body.String()

	if success != "true" {
		t.Fatal("Error, round trip failed: ", success)
	}
}

func TestHTTPRoundAuthFailAfterSuccess(t *testing.T) {

	testClient = NewAuthenticatedClient(testAccessKey, testAccountId, SigningKey(nil))

	authn := NewTestAuthenticatorBadClient2()
	//authn := NewTestAuthenticator()
	authz := NewAuthorizer(authn, nil)

	logger := NewTestLogger()
	authz.SetDebugLogger(logger)

	handler := func(w http.ResponseWriter, r *http.Request) {

		success := "true"

		// validate the request
		_, err := authz.tryAuthenticate(r, true) // Use try Authenticate so that the second call will fail properly
		if err != nil {
			success = err.Error()
		}

		// send status back
		_, err = w.Write([]byte(success))
		if err != nil {
			t.Fatal(err)
		}
	}

	signer := NewSigner(region, service, accessKey, secretKey, nil)

	signer.SetDebugLogger(logger)

	// should work ...

	req, err := http.NewRequest("GET", "http://localhost:5555/test?a=foo", nil)
	if err != nil {
		t.Fatal(err)
	}

	signingKey, _, _ := signer.sign(time.Now(), SignableRequest{req, true})
	testClient.signingKey = signingKey // so they match

	w := httptest.NewRecorder()
	handler(w, req)

	if w.Code != http.StatusOK {
		t.Fatal("Bad status: ", w.Code)
	}

	success := w.Body.String()

	if success != "true" {
		t.Fatal("Error, round trip failed: ", success)
	}

	// force the signature check to fail on the cached copy ...
	testClient = NewAuthenticatedClient(testAccessKey, testAccountId, SigningKey(nil))
	signer = NewSigner(region, service, accessKey, "foo", nil)

	signer.SetDebugLogger(logger)

	// this one should fail authentication (when forced due to signature mismatch)

	req, err = http.NewRequest("GET", "http://localhost:5555/test?a=foo", nil)
	if err != nil {
		t.Fatal(err)
	}

	signingKey, _, _ = signer.sign(time.Now(), SignableRequest{req, true})
	testClient.signingKey = signingKey // so they match

	w = httptest.NewRecorder()
	handler(w, req)

	if w.Code != http.StatusOK {
		t.Fatal("Bad status: ", w.Code)
	}

	success = w.Body.String()

	if success != "Authentication failed" {
		t.Fatal("Error, round trip failed: ", success)
	}

}

func TestHTTPRoundAuthStaleCache(t *testing.T) {

	// we expect the testclient to be cached with old signingkey, we now
	// make new signing key and it should still work, should re-authenticate

	testClient = NewAuthenticatedClient(testAccessKey, testAccountId, SigningKey(nil))

	authn := NewTestAuthenticator()
	authz := NewAuthorizer(authn, nil)

	logger := NewTestLogger()
	authz.SetDebugLogger(logger)

	handler := func(w http.ResponseWriter, r *http.Request) {

		success := "true"

		// validate the request
		_, err := authz.Authenticate(r)
		if err != nil {
			success = err.Error()
		}

		// send status back
		_, err = w.Write([]byte(success))
		if err != nil {
			t.Fatal(err)
		}
	}

	signer := NewSigner(region, service, accessKey, secretKey, nil)

	signer.SetDebugLogger(logger)

	req, err := http.NewRequest("GET", "http://localhost:5555/test?a=foo", nil)
	if err != nil {
		t.Fatal(err)
	}

	signingKey, _, _ := signer.sign(time.Now(), SignableRequest{req, true})
	testClient.signingKey = signingKey // so they match

	w := httptest.NewRecorder()
	handler(w, req)

	if w.Code != http.StatusOK {
		t.Fatal("Bad status: ", w.Code)
	}

	success := w.Body.String()

	if success != "true" {
		t.Fatal("Error, round trip failed: ", success)
	}

	// now we are cached, change the signingkey by changing secretKey and re-do

	signer = NewSigner(region, service, accessKey, "foo", nil)

	signer.SetDebugLogger(logger)

	req, err = http.NewRequest("GET", "http://localhost:5555/test?a=foo", nil)
	if err != nil {
		t.Fatal(err)
	}

	signingKey, _, _ = signer.sign(time.Now(), SignableRequest{req, true})
	// replace testClient with one with new signingKey, old authenticated client will be cached
	testClient = NewAuthenticatedClient(testAccessKey, testAccountId, signingKey)

	w = httptest.NewRecorder()
	handler(w, req)

	if w.Code != http.StatusOK {
		t.Fatal("Bad status: ", w.Code)
	}

	success = w.Body.String()

	if success != "true" {
		t.Fatal("Error, round trip failed: ", success)
	}

}

func TestHTTPRoundTripWhitelistFail(t *testing.T) {

	// this should fail whitelist testing

	whitelist := []string{"fakeAccountId"}

	testClient = NewAuthenticatedClient(testAccessKey, testAccountId, SigningKey(nil))

	authn := NewTestAuthenticator()
	authz := NewAuthorizer(authn, whitelist)

	logger := NewTestLogger()
	authz.SetDebugLogger(logger)

	// excercise cache
	cacheSize := 20
	authz.SetCacheSize(cacheSize)
	if authz.CacheCapacity() != cacheSize {
		t.Error(errors.New("Cache size is not correct"))
	}
	if authz.CacheSize() != 0 { // should be empty
		t.Error(errors.New("Cache size is not correct"))
	}

	handler := func(w http.ResponseWriter, r *http.Request) {

		success := "true"

		// validate the request
		_, err := authz.Authenticate(r)
		if err != nil {
			success = err.Error() // this is what we expect
		}

		// send status back
		_, err = w.Write([]byte(success))
		if err != nil {
			t.Fatal(err)
		}
	}

	signer := NewSigner(region, service, accessKey, secretKey, nil)

	signer.SetDebugLogger(logger)

	req, err := http.NewRequest("GET", "http://localhost:5555/test?a=foo", nil)
	if err != nil {
		t.Fatal(err)
	}

	signingKey, _, _ := signer.sign(time.Now(), SignableRequest{req, true})
	testClient.signingKey = signingKey // so they match

	w := httptest.NewRecorder()
	handler(w, req)

	if w.Code != http.StatusOK {
		t.Fatal("Bad status: ", w.Code)
	}

	success := w.Body.String()

	if success != "Client account not in whitelist" {
		t.Fatal("Error, round trip failed: ", success)
	}

}

func sendGETMangleHdr(t *testing.T, mangle func(*http.Request)) {

	testClient = NewAuthenticatedClient(testAccessKey, testAccountId, SigningKey(nil))

	authn := NewTestAuthenticator()
	authz := NewAuthorizer(authn, nil)

	logger := NewTestLogger()
	authz.SetDebugLogger(logger)

	handler := func(w http.ResponseWriter, r *http.Request) {

		success := "true"

		// validate the request
		_, err := authz.Authenticate(r)
		if err != nil {
			http.Error(w, "Not Authorized", http.StatusUnauthorized)
			return
		}

		// send status back
		_, err = w.Write([]byte(success))
		if err != nil {
			t.Fatal(err)
		}
	}

	signer := NewSigner(region, service, accessKey, secretKey, nil)

	signer.SetDebugLogger(logger)

	req, err := http.NewRequest("GET", "http://localhost:5555/test?a=foo", nil)
	if err != nil {
		t.Fatal(err)
	}

	signingKey, _, _ := signer.sign(time.Now(), SignableRequest{req, true})
	testClient.signingKey = signingKey // so they match

	// now mangle
	mangle(req)

	// just for code coverage ...
	claim, _ := NewAuthV4Claim(req, true)
	logger.Debugf(claim.GetStringToSign())

	w := httptest.NewRecorder()
	handler(w, req)

	if w.Code != http.StatusUnauthorized {
		t.Error("Bad status: ", w.Code)
	}
}

func TestHTTPRoundTripBadClaim(t *testing.T) {

	var mangle func(*http.Request)

	mangle = func(req *http.Request) { req.Header.Del("X-Amz-Date") }
	sendGETMangleHdr(t, mangle)

	mangle = func(req *http.Request) { req.Header.Set("X-Amz-Date", "not a date") }
	sendGETMangleHdr(t, mangle)

	// too old
	mangle = func(req *http.Request) { req.Header.Set("X-Amz-Date", iso8601BasicFmt) }
	sendGETMangleHdr(t, mangle)

	mangle = func(req *http.Request) { req.Header.Del("Authorization") }
	sendGETMangleHdr(t, mangle)

	mangle = func(req *http.Request) {
		newauth := strings.Replace(req.Header.Get("Authorization"), "=", "not an equals", -1)
		req.Header.Set("Authorization", newauth)
	}
	sendGETMangleHdr(t, mangle)

	mangle = func(req *http.Request) {
		newauth := strings.Replace(req.Header.Get("Authorization"), authv4Algorithm, "not an algorithm", -1)
		req.Header.Set("Authorization", newauth)
	}
	sendGETMangleHdr(t, mangle)

	mangle = func(req *http.Request) {
		newauth := strings.Replace(req.Header.Get("Authorization"), "Credential", "bad Credential", -1)
		req.Header.Set("Authorization", newauth)
	}
	sendGETMangleHdr(t, mangle)

	mangle = func(req *http.Request) {
		newauth := strings.Replace(req.Header.Get("Authorization"), "aws4_request", "not aws4_request", -1)
		req.Header.Set("Authorization", newauth)
	}
	sendGETMangleHdr(t, mangle)

	mangle = func(req *http.Request) {
		newauth := strings.Replace(req.Header.Get("Authorization"), "/", "not slash", -1)
		req.Header.Set("Authorization", newauth)
	}
	sendGETMangleHdr(t, mangle)

	mangle = func(req *http.Request) {
		newauth := strings.Replace(req.Header.Get("Authorization"), "SignedHeaders", "bad SignedHeaders", -1)
		req.Header.Set("Authorization", newauth)
	}
	sendGETMangleHdr(t, mangle)

	mangle = func(req *http.Request) {
		newauth := strings.Replace(req.Header.Get("Authorization"), "Signature", "bad Signature", -1)
		req.Header.Set("Authorization", newauth)
	}
	sendGETMangleHdr(t, mangle)

}

func TestHTTPRoundTripGETBadSig(t *testing.T) {

	testClient = NewAuthenticatedClient(testAccessKey, testAccountId, SigningKey(nil))

	authn := NewTestAuthenticator()
	authz := NewAuthorizer(authn, nil)

	logger := NewTestLogger()
	authz.SetDebugLogger(logger)

	handler := func(w http.ResponseWriter, r *http.Request) {

		success := "true"

		// validate the request
		_, err := authz.Authenticate(r)
		if err != nil {
			success = err.Error()
		}

		// send status back
		_, err = w.Write([]byte(success))
		if err != nil {
			t.Fatal(err)
		}
	}

	signer := NewSigner(region, service, accessKey, secretKey, nil)

	signer.SetDebugLogger(logger)

	req, err := http.NewRequest("GET", "http://localhost:5555/test?a=foo", nil)
	if err != nil {
		t.Fatal(err)
	}

	signer.sign(time.Now(), SignableRequest{req, true})
	testClient.signingKey = []byte("foo") // so they DON'T match

	w := httptest.NewRecorder()
	handler(w, req)

	if w.Code != http.StatusOK {
		t.Fatal("Bad status: ", w.Code)
	}

	success := w.Body.String()

	if success != "Signatures do not match" {
		t.Fatal("Error, round trip failed: ", success)
	}

}
