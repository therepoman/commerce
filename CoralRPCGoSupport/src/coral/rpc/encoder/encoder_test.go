package encoder

import (
	"reflect"
	"testing"
	"time"
)

type encodeTest struct {
	Foo *string     `coral:"foo"`
	Bar *string     `coral:"baz"`
	Baz *encodeTest `coral:"bar"`
}

func TestEncode(t *testing.T) {
	h, w := "Hello", "World"
	c, g := "Coral", "Go"
	val := reflect.ValueOf(&encodeTest{&h, &w, &encodeTest{&c, &g, nil}})
	m, ok := encode(val).(map[string]interface{})
	if !ok {
		t.Fatalf("Invalid type received from encode, want map[string]interface{}, got %T", encode(val))
	}
	if m["foo"] != h {
		t.Errorf(`m["foo"] = "%v", want "%s"`, m["foo"], h)
	}
	if m["baz"] != w {
		t.Errorf(`m["baz"] = %v", want "%s"`, m["baz"], w)
	}
	if baz, ok := m["bar"].(map[string]interface{}); ok {
		if baz["foo"] != c {
			t.Errorf(`baz["foo"] = "%v", want "%s"`, baz["foo"], c)
		}
		if baz["baz"] != g {
			t.Errorf(`baz["baz"] = %v, want "%s"`, baz["baz"], g)
		}
		if baz["bar"] != nil {
			t.Errorf(`baz["bar"] = %v, want nil`, baz["bar"])
		}
	} else {
		t.Errorf(`m["bar"] = %t, want map[string]interface{}`, m["bar"])
	}
}

type timeTest *time.Time

func TestTime(t *testing.T) {
	origTime := time.Now()
	var test timeTest = timeTest(&origTime)
	val := reflect.ValueOf(test)
	if f, ok := encode(val).(float64); !ok {
		t.Fatalf("Invalid Type received from encode, want float64, got %T", encode(val))
	} else if int64(f) != origTime.Unix() {
		t.Fatalf("Wrong time received from encode, got %v, want %v", int64(f), origTime.Unix())
	}
	var timeTestVal timeTest = new(time.Time)
	timeTestType := reflect.TypeOf(timeTestVal)
	newVal := decode(reflect.ValueOf(encode(val)), timeTestType).Interface()
	if timeVal, ok := newVal.(timeTest); !ok {
		t.Fatalf("Invalid Type received from decode, want timeTest, got %T", newVal)
	} else if (*time.Time)(timeVal).Unix() != origTime.Unix() {
		t.Fatalf("Wrong time received from decode, got %v, want %v", time.Time(*timeVal).Unix(), origTime.Unix())
	}

}
