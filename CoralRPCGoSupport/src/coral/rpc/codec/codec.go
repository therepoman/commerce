package codec

import (
	"bufio"
	"bytes"
	"code.justin.tv/commerce/AAAGo/src/aaa"
	cod "code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	encoding "code.justin.tv/commerce/CoralRPCGoSupport/src/coral/rpc/encoder"
	"code.justin.tv/commerce/GoAuthV4/src/authv4"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

type BasicAuth struct {
	Username, Password string
}

type RPCv1 struct {
	Host          string         // required, host or host:port
	Path          string         // optional
	SignerV4      *authv4.Signer // optional
	AuthInfo      *BasicAuth     // optional
	SecurityToken string         // optional, see https://w.amazon.com/index.php/Coral/Specifications/HttpSecurityToken
	AAAClient     aaa.Client     // optional
}

type Option func(*RPCv1)

/*
   Example: rpcv1 := codec.NewRPCv1("foo.com:8080", codec.SetSignerV4(signer))
*/
func NewRPCv1(host string, options ...Option) RPCv1 {

	rpcv1 := RPCv1{Host: host}

	for _, option := range options {
		option(&rpcv1)
	}

	return rpcv1
}

func SetPath(path string) Option {
	return (Option)(func(rpcv1 *RPCv1) {
		rpcv1.Path = path
	})
}

func SetSignerV4(signer *authv4.Signer) Option {
	return (Option)(func(rpcv1 *RPCv1) {
		rpcv1.SignerV4 = signer
	})
}

func SetBasicAuth(ba *BasicAuth) Option {
	return (Option)(func(rpcv1 *RPCv1) {
		rpcv1.AuthInfo = ba
	})
}

func SetSecurityToken(securityToken string) Option {
	return (Option)(func(rpcv1 *RPCv1) {
		rpcv1.SecurityToken = securityToken
	})
}

func SetAAAClient(aaaClient aaa.Client) Option {
	return (Option)(func(rpcv1 *RPCv1) {
		rpcv1.AAAClient = aaaClient
	})
}

//w/?Coral/Protocols#RPCv1
func (c RPCv1) RoundTrip(r *cod.Request, rw io.ReadWriter) error {

	path := c.Path
	if path == "" {
		path = "/"
	}

	b, err := encoding.Marshal(r.Input)
	if err != nil {
		return err
	}
	body := bytes.NewBuffer(b)
	request, err := http.NewRequest("POST", path, body)
	if err != nil {
		//TODO: Panic here? Suggests malformed input to NewRequest
		return err
	}

	if c.Host != "" {
		request.Host = c.Host
		request.URL.Host = c.Host
	}

	request.Header.Set("Accept", "application/json, text/javascript")
	request.Header.Set("Content-Type", "application/json; charset=UTF-8;")
	request.Header.Set("Content-Encoding", "amz-1.0")
	request.Header.Set("X-Amz-Target", r.Service.AsmName+"."+r.Service.ShapeName+"."+r.Operation.ShapeName)

	if c.SecurityToken != "" {
		request.Header.Set("X-Amz-Security-Token", c.SecurityToken)
	}

	if c.AuthInfo != nil {
		request.SetBasicAuth(c.AuthInfo.Username, c.AuthInfo.Password)
	}

	// v4 signing?
	if c.SignerV4 != nil {
		c.SignerV4.Sign(request)
	} else { // v4 signing will add this header
		request.Header.Set("X-Amz-Date", time.Now().UTC().Format(time.RFC822))
	}

	var clientCxt *aaa.ClientContext
	if c.AAAClient != nil {
		clientCxt, err = c.AAAClient.EncodeRequest(r.Service.ShapeName, r.Operation.ShapeName, request)
		if err != nil {
			return err
		}
	}

	err = request.Write(rw)
	if err != nil {
		return err
	}

	resp, err := http.ReadResponse(bufio.NewReader(rw), request)
	if err != nil {
		return err
	}

	if c.AAAClient != nil {
		err = c.AAAClient.DecodeResponse(clientCxt, resp)
		if err != nil {
			return err
		}
	}

	defer resp.Body.Close()
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return errors.New(string(respBody))
	}

	//Odin returns empty body with 200 sometimes which breaks Unmarshal
	if len(respBody) == 0 {
		return nil
	}

	return encoding.Unmarshal(respBody, r.Output)
}
