package client

import (
	"testing"
	"time"
)

func TestWithDefaultConnectionPoolSuccess(t *testing.T) {
	testClient := &client{dialer: &dialerMock{}}
	s := uint(5)
	ttl := time.Millisecond
	jitter := time.Microsecond
	WithDefaultConnectionPool(s, ttl, jitter)(testClient)

	if testClient.connPool == nil {
		t.Error("WithDefaultConnectionPool option didn't assign a defaultConnection pool to the client")
	}

	cp := testClient.connPool.(*defaultConnectionPool)

	atualCapacity := cap(cp.pool)
	if uint(atualCapacity) != s {
		t.Errorf("Expected pool size was %d, but it's %d", s, atualCapacity)
	}

	if cp.maxTTL != ttl {
		t.Errorf("Expected maxTTL was %v, but received %v", ttl, cp.maxTTL)
	}

	if cp.jitter != jitter {
		t.Errorf("Expected jitter was %v, but received %v", jitter, cp.jitter)
	}
}

func TestWithConnectionPool(t *testing.T) {
	d := &dialerMock{}
	testClient := &client{dialer: d}
	cp := newDefaultConnectionPool(DefaultPoolSize, DefaultMaxTTL, DefaultJitter, d.Dial)
	WithConnectionPool(cp)(testClient)

	if testClient.connPool != cp {
		t.Error("WithDefaultConnectionPool option didn't assign the ConnectionPool passed as parameter to the client")
	}

}
