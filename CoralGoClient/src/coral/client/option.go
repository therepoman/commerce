package client

import "time"

// Option is a configuration function that can be passed to NewClient constructor function in order to
// customize the client's properties.
// Options will be applied (invoked) at client's creation time.
type Option func(c *client)

// WithDefaultConnectionPool configures the default connection pool with custom settings. If a custom connection pool is
// desired, use WithConnectionPool option instead.
//
// Use jitter to add some randomness to the connection's expiration time, which can be useful to avoid potential
// thundering herds when dealing with large pools and high throughput applications.
//
// Connections in the pool will expire anywhere between (maxTTL - jitter) and  maxTTL after creation time.
// For example, if maxTTL=45*time.Second and jitter=time.Second, the connection will expire anywhere between 44 and 45
// seconds after the connection was first open.
//
// maxTTL must be positive, if a negative value is passed, the default of 45s will be used instead. Similarly, jitter
// must be greater than zero. If a value less or equal than zero is passed, the default 1s value  will be used instead.
//
// note: If multiple connection pool options are passed, only the last one is used.
func WithDefaultConnectionPool(size uint, maxTTL, jitter time.Duration) Option {
	return func(c *client) {
		c.connPool = newDefaultConnectionPool(size, maxTTL, jitter, c.dialer.Dial)
	}
}

// WithConnectionPool allows to set a custom ConnectionPool for the client. If a default connection pool is desired,
// use WithDefaultConnectionPool instead.
// note: If multiple connection pool options are passed, only the last one is used.
func WithConnectionPool(pool ConnectionPool) Option {
	return func(c *client) {
		c.connPool = pool
	}
}
