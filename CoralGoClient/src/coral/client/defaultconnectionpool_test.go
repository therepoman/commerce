package client

import (
	"sync"
	"testing"
	"time"
)

// Tests that:
// 1. Consume creates new connections when the pool is empty
// 2. Return puts connections into the pool and they remain open (if they're not expired)
// 3. Consume closes and discards expired connections
// 4. Return doesn't put expired connections back into the pool and closes them
func TestDefaultConnectionPool(t *testing.T) {
	poolSize := 5
	ttl := time.Millisecond
	jitter := time.Second * 0
	dialer := &dialerMock{}

	testPool := newDefaultConnectionPool(uint(poolSize), ttl, jitter, dialer.Dial).(*defaultConnectionPool)

	// (1) Consume 5 connections concurrently with an empty pool
	consumedConns := consumeConnsConcurrently(t, testPool, poolSize)

	// Return connections immediately
	returnConnectionsSafely(testPool, consumedConns)

	// (2) Check the connections are in the pool
	actualPoolLen := testPool.length()
	if actualPoolLen != poolSize {
		t.Fatalf("expected pool length to be %d but received %d", poolSize, actualPoolLen)
	}

	// (2) Verify connections in the pool remain open after being returned
	for _, c := range consumedConns {
		if c.ReadWriteCloser.(*connnMock).isClosed {
			t.Error("Connection in the pool should be open")
		}
	}

	// Wait for all the connections in the pool to expire, then consume connections again to renew the pool
	time.Sleep(ttl * 2)
	newConns := consumeConnsConcurrently(t, testPool, poolSize)

	// (3) Verify that the now-expired connections were closed by the pool
	for _, c := range consumedConns {
		if !c.ReadWriteCloser.(*connnMock).isClosed {
			t.Error("Expired connection should have been closed")
		}
	}

	// (3) At this point, the pool should be empty because the initial connections expired
	actualPoolLen = testPool.length()
	if actualPoolLen != 0 {
		t.Fatalf("expected pool to be empty but it contains %d connections", actualPoolLen)
	}

	// (4) Wait for the new connections to expire, then try to return them
	time.Sleep(ttl * 2)
	for _, c := range newConns {
		testPool.Return(c)
	}

	// (4) Check that the pool is still empty since we returned just expired connections
	actualPoolLen = testPool.length()
	if actualPoolLen != 0 {
		t.Fatalf("expected pool to be empty but it contains %d connections", actualPoolLen)
	}
}

func TestDefaultConnectionPoolEnforceDefaults(t *testing.T) {
	size := uint(0)
	testPool := newDefaultConnectionPool(size, -time.Microsecond, -time.Microsecond, (&dialerMock{}).Dial).(*defaultConnectionPool)

	atualCapacity := cap(testPool.pool)
	if uint(atualCapacity) != size {
		t.Errorf("Expected pool size was %d, but it's %d", size, atualCapacity)
	}

	if testPool.maxTTL != DefaultMaxTTL {
		t.Errorf("Expected maxTTL was %v, but received %v", DefaultMaxTTL, testPool.maxTTL)
	}

	if testPool.jitter != DefaultJitter {
		t.Errorf("Expected jitter was %v, but received %v", DefaultJitter, testPool.jitter)
	}
}

func TestDefaultConnectionPoolNilFactory(t *testing.T) {
	defer func() {
		t.Helper()
		if r := recover(); r == nil {
			t.Error("Expected newDefaultConnectionPool to panic because no factory was provided")
		}
	}()
	newDefaultConnectionPool(DefaultPoolSize, DefaultMaxTTL, DefaultJitter, nil)

}

func consumeConnsConcurrently(t *testing.T, testPool ConnectionPool, numCalls int) []*coralConnection {
	t.Helper()
	// This wait group will track the number of testClient.Call invocations made
	wgCallEnd := &sync.WaitGroup{}
	wgCallEnd.Add(numCalls)

	mux := &sync.Mutex{}
	consumedConns := make([]*coralConnection, 0)

	// Create worker go routines to consume connections concurrently
	for i := 0; i < numCalls; i++ {
		go func() {
			defer wgCallEnd.Done()
			c, err := testPool.Consume()
			if err != nil {
				t.Fatal("expected connection pool to not return an error")
			}
			mux.Lock()
			consumedConns = append(consumedConns, c.(*coralConnection))
			mux.Unlock()
		}()
	}

	// Wait for all the worker go routines to complete. After this, all new connections will be returned to the pool
	wgCallEnd.Wait()
	return consumedConns
}

// Since return checks for connection expiration, we need to force a big TTL before returning the connection in order to
// guarantee that the connection is actually put back into the pool even if more than 1ms (TTL used for testing) has passed.
// One reason this could happen is just go routine scheduling delays when the CPU is busy while the test is running.
func returnConnectionsSafely(testPool *defaultConnectionPool, conns []*coralConnection) {
	prevTtl := testPool.maxTTL
	testPool.maxTTL = time.Hour
	for _, c := range conns {
		testPool.Return(c)
	}
	testPool.maxTTL = prevTtl
}
