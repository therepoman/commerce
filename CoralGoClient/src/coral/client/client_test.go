package client

import (
	"code.justin.tv/commerce/CoralGoCodec/src/coral/codec"
	"errors"
	"io"
	"testing"
)

type roundTripHandler func(*codec.Request, io.ReadWriter) error

type testCodec struct {
	handler roundTripHandler
}

func (c *testCodec) RoundTrip(req *codec.Request, rw io.ReadWriter) error {
	return c.handler(req, rw)
}

func forceErrorRoundTrip(req *codec.Request, rw io.ReadWriter) error {
	return errors.New("force error")
}

func forceSuccessRoundTrip(req *codec.Request, rw io.ReadWriter) error {
	return nil
}

// prepareTestClient creates a new coral client with a default connection pool
func prepareTestClient(handler roundTripHandler, opts ...Option) (*client, error) {
	d := &dialerMock{}
	c := testCodec{
		handler: handler,
	}
	return NewClient("testAssembly", "testName", d, &c, opts...).(*client), nil
}

func TestClientErrorHandling(t *testing.T) {
	testCases := []struct {
		handler         roundTripHandler
		errorExpected   bool
		expectedPoolLen int
	}{
		{forceErrorRoundTrip, true, 0},
		{forceSuccessRoundTrip, false, 1},
	}

	for _, testCase := range testCases {
		testClient, err := prepareTestClient(testCase.handler)
		if err != nil {
			t.Fatal(err)
		}
		err = testClient.Call("this", "input", "doesn't", "matter")
		if err == nil && testCase.errorExpected {
			t.Fatal("expected the client to return an error")
		} else if err != nil && !testCase.errorExpected {
			t.Fatal("expected the client to not return an error")
		}
		poolLen := testClient.connPool.(*defaultConnectionPool).length()
		if poolLen != testCase.expectedPoolLen {
			t.Fatalf("expected the client's connection pool length to be %d but received %d", testCase.expectedPoolLen, poolLen)
		}
	}
}

func TestNewClient_applyOptions(t *testing.T) {

	expectedOptionsInvocations := 3
	actualOptionInvocations := 0
	testOptions := make([]Option, expectedOptionsInvocations)

	for i := 0; i < expectedOptionsInvocations; i++ {
		testOptions[i] = func(c *client) {
			actualOptionInvocations++
		}
	}

	_, err := prepareTestClient(forceSuccessRoundTrip, testOptions...)
	if err != nil {
		t.Fatal(err)
	}

	if actualOptionInvocations != expectedOptionsInvocations {
		t.Errorf("NewClient didn't apply the options. Expected %d options to be applied, but %d were applied instead", expectedOptionsInvocations, actualOptionInvocations)
	}
}
