package dialer

import (
	"crypto/tls"
	"fmt"
	"io"
	"net"
)

type Dialer interface {
	// Dial creates a new connection to the service.
	Dial() (io.ReadWriteCloser, error)
}

type DialerFunc func() (io.ReadWriteCloser, error)

func (f DialerFunc) Dial() (io.ReadWriteCloser, error) {
	return f()
}

// TCP creates a Dialer to the address and port using a TCP connection.
// A nil error is always returned and can be safely ignored; it is returned for backwards compatibility.
func TCP(addr string, port uint16) (Dialer, error) {
	return TCPDialer(addr, port, nil)
}

// TCPDialer creates a Dialer from a given net.Dialer to the address and port using a TCP connection.
// This is useful for configuring settings such as timeouts, keep-alive, etc.
// If the given dialer is nil a default net.Dialer will be used.
// A nil error is always returned and can be safely ignored; it is returned for backwards compatibility.
func TCPDialer(addr string, port uint16, dialer *net.Dialer) (Dialer, error) {
	if dialer == nil {
		dialer = new(net.Dialer)
	}
	address := fmt.Sprintf("%s:%d", addr, port)
	return DialerFunc(func() (io.ReadWriteCloser, error) {
		return dialer.Dial("tcp", address)
	}), nil
}

// TLS creates a Dialer to the address and port using a TLS (SSL) connection.
// A nil error is always returned and can be safely ignored; it is returned for backwards compatibility.
func TLS(addr string, port uint16, config *tls.Config) (Dialer, error) {
	return TLSDialer(addr, port, config, nil), nil
}

// TLSDialer creates a Dialer from a given net.Dialer to the address and port using a TLS (SSL) connection.
// This is useful for configuring settings such as timeouts, keep-alive, etc.
// If the given dialer is nil a default net.Dialer will be used.
func TLSDialer(addr string, port uint16, config *tls.Config, dialer *net.Dialer) Dialer {
	if dialer == nil {
		dialer = new(net.Dialer)
	}
	address := fmt.Sprintf("%s:%d", addr, port)
	return DialerFunc(func() (io.ReadWriteCloser, error) {
		return tls.DialWithDialer(dialer, "tcp", address, config)
	})
}
