package twirpserver

import (
	"context"

	phanto "code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phoenix/backend/audit"
	"code.justin.tv/commerce/phoenix/backend/authorization"
	phoenix "code.justin.tv/commerce/phoenix/rpc"
)

type twirpServer struct {
	PhantoClient phanto.Phanto
	Authorizer   authorization.Authorizer
	Auditor      audit.Auditor
}

func NewTwirpServer(authorizer authorization.Authorizer, auditor audit.Auditor, phantoClient phanto.Phanto) (phoenix.Phoenix, error) {
	return &twirpServer{
		PhantoClient: phantoClient,
		Authorizer:   authorizer,
		Auditor:      auditor,
	}, nil
}

func (s *twirpServer) HealthCheck(ctx context.Context, req *phoenix.HealthCheckReq) (*phoenix.HealthCheckResp, error) {
	return &phoenix.HealthCheckResp{
		Response: "OK",
	}, nil
}

func (s *twirpServer) PhantoGenerateKeyBatch(ctx context.Context, req *phanto.GenerateKeyBatchReq) (*phanto.GenerateKeyBatchResp, error) {
	err := s.Authorizer.Authorize(ctx, []authorization.LDAPGroup{authorization.TeamAdminPanel})

	if err != nil {
		return nil, err
	}

	err = s.Auditor.Record(ctx, "Phanto:GenerateKeyBatch", req)

	if err != nil {
		return nil, err
	}

	return s.PhantoClient.GenerateKeyBatch(ctx, req)
}

func (s *twirpServer) PhantoGetAllKeyPools(ctx context.Context, req *phanto.GetAllKeyPoolsReq) (*phanto.GetAllKeyPoolsResp, error) {
	err := s.Authorizer.Authorize(ctx, []authorization.LDAPGroup{authorization.TeamPayday})

	if err != nil {
		return nil, err
	}

	err = s.Auditor.Record(ctx, "Phanto:GetAllKeyPools", req)

	if err != nil {
		return nil, err
	}

	return s.PhantoClient.GetAllKeyPools(ctx, req)
}

func (s *twirpServer) PhantoGetKeyBatches(ctx context.Context, req *phanto.GetKeyBatchesReq) (*phanto.GetKeyBatchesResp, error) {
	err := s.Authorizer.Authorize(ctx, []authorization.LDAPGroup{authorization.TeamAdminPanel})

	if err != nil {
		return nil, err
	}

	err = s.Auditor.Record(ctx, "Phanto:GetKeyBatches", req)

	if err != nil {
		return nil, err
	}

	return s.PhantoClient.GetKeyBatchesS2S(ctx, req)
}
