package middleware

import (
	"context"
	"crypto/rand"
	"fmt"
	"net/http"

	"code.justin.tv/systems/guardian/guardian"
	asiimov "code.justin.tv/systems/guardian/middleware"
	"github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
)

type ContextKey string

const GuardianUserContextKey = ContextKey("guardianUser")
const GuardianUserTokenKey = ContextKey("guardianToken")

// SetGuardianUser sets a value for this package in the
// request values.
func setGuardianUser(r *http.Request, val *guardian.User) *http.Request {
	ctx := context.WithValue(r.Context(), GuardianUserContextKey, val)
	return r.WithContext(ctx)
}

// getGuardianUser returns a value for this package from the request values.
func GetGuardianUser(ctx context.Context) (*guardian.User, error) {
	v := ctx.Value(GuardianUserContextKey)
	user, ok := v.(*guardian.User)
	if !ok {
		return nil, fmt.Errorf("no guardianUser found in context")
	}
	return user, nil
}

func GuardianHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case "OPTIONS":
			return
		}

		oauthCfg := &oauth2.Config{
			ClientID:     "5fd7b172-f254-4237-9c63-b3bac3c41927",
			ClientSecret: "244fb8bf3a3aef8dc35e9122fe470642ca1c6955",
			RedirectURL:  "http://localhost:8080/handle_guardian_redirect",
			Endpoint: oauth2.Endpoint{
				AuthURL: "https://guardian.internal.justin.tv/authorize",
				// This is called from your server so it requires the private link entry.
				TokenURL: "https://guardian.internal.justin.tv/oauth2/token",
			},
		}

		asi := asiimov.New(logrus.New(), oauthCfg)
		asi.CheckTokenURL = "https://guardian.internal.justin.tv/oauth2/check_token"

		token := r.FormValue("token")
		if token != "" {
			r.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
		} else {
			code := r.FormValue("code")
			if code != "" {
				authToken, err := oauthCfg.Exchange(oauth2.NoContext, code)
				if err != nil {
					logrus.WithError(err).Errorf("failed to exchange token %+v", code)
					return
				}

				authToken.SetAuthHeader(r)
			}
		}

		user, err := asi.HandleGuardianRequest(r)
		if err != nil || user == nil {
			b := make([]byte, 32)
			_, _ = rand.Read(b)
			state := fmt.Sprintf("%032x", b)
			http.Redirect(w, r, oauthCfg.AuthCodeURL(state), http.StatusFound)
		}

		logrus.Debugf("Guardian user is %+v", user)
		h.ServeHTTP(w, setGuardianUser(r, user))
	})
}
