package authorization

import (
	"errors"
	"context"

	"code.justin.tv/commerce/phoenix/backend/middleware"
	"code.justin.tv/systems/guardian/guardian"
)

type Path string
type LDAPGroup string

type Mappings map[Path][]LDAPGroup

const (
	TeamPayday     = LDAPGroup("team-payday")
	TeamAdminPanel = LDAPGroup("team-admin-panel-contributors")
)

type Authorizer interface {
	GetAllowedPaths(user *guardian.User) []Path
	Authorize(ctx context.Context, allowedGroups []LDAPGroup) error
}

type authorizer struct {
	allowedGroupsByURLPath   map[Path][]LDAPGroup
	allowedGroupsByTwirpPath map[Path][]LDAPGroup
	allowedPathsByGroup      map[LDAPGroup][]Path
}

func NewAuthorizer() Authorizer {
	// Used for client side routing
	allowedGroupsByPath := Mappings{
		Path("/"):                      []LDAPGroup{TeamPayday, TeamAdminPanel},
		Path("/keys"):                  []LDAPGroup{TeamPayday, TeamAdminPanel},
		Path("/keys/pools"):            []LDAPGroup{TeamPayday},
		Path("/keys/batches"):          []LDAPGroup{TeamAdminPanel},
		Path("/keys/batches/:poll_id"): []LDAPGroup{TeamAdminPanel},
	}

	allowedPathsByGroup := make(map[LDAPGroup][]Path)
	for path, groups := range allowedGroupsByPath {
		for _, group := range groups {
			allowedPathsByGroup[group] = append(allowedPathsByGroup[group], path)
		}
	}

	return &authorizer{
		allowedGroupsByURLPath:   allowedGroupsByPath,
		allowedPathsByGroup:      allowedPathsByGroup,
	}
}

func (a *authorizer) GetAllowedPaths(user *guardian.User) []Path {
	if user == nil {
		return []Path{}
	}

	var allPaths []Path
	for _, group := range user.Groups {
		if paths, found := a.allowedPathsByGroup[LDAPGroup(group)]; found {
			allPaths = append(allPaths, paths...)
		}
	}

	return allPaths
}

func (a *authorizer) Authorize(ctx context.Context, allowedGroups []LDAPGroup) error {
	user, err := middleware.GetGuardianUser(ctx)

	if err != nil {
		return err
	}

	if user == nil {
		return errors.New("could not identify user")
	}

	for _, group := range user.Groups {
		for _, allowedGroup := range allowedGroups {
			if group == string(allowedGroup) {
				return nil
			}
		}
	}

	return errors.New("forbidden")
}