package main

import (
	"code.justin.tv/commerce/phoenix/backend/audit"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"net/http"
	"os"

	log "code.justin.tv/commerce/logrus"
	phanto "code.justin.tv/commerce/phanto/rpc"
	"code.justin.tv/commerce/phoenix/backend/authorization"
	"code.justin.tv/commerce/phoenix/backend/middleware"
	"code.justin.tv/commerce/phoenix/backend/server"
	"code.justin.tv/commerce/phoenix/backend/twirpserver"
	phoenix "code.justin.tv/commerce/phoenix/rpc"
	"code.justin.tv/sse/malachai/pkg/s2s/caller"
	asiimov "code.justin.tv/systems/guardian/middleware"
	"github.com/codegangsta/cli"
	goji "goji.io"
	"goji.io/pat"
)

var environment string
var tenantDomain string

const (
	phantoStagingURL     = "https://phanto-payday-staging.internal.justin.tv"
	phantoStagingS2SName = "phoenix-staging"
)

func main() {
	app := cli.NewApp()
	app.Name = "Phoenix"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "environment, e",
			Value:       "staging",
			Usage:       "Runtime config to use. Options are: 'prod', 'staging'. Defaults to 'staging' if unset.",
			Destination: &environment,
			EnvVar:      "ENVIRONMENT",
		},
	}

	app.Action = runPhoenix

	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}

func runPhoenix(c *cli.Context) {
	awsConfig := aws.NewConfig().WithRegion("us-west-2")
	sess, err := session.NewSession(awsConfig)

	authorizer := authorization.NewAuthorizer()
	auditor := audit.NewAuditor(sess)

	server, err := server.NewServer(server.Environment(environment), authorizer)

	if err != nil {
		log.WithError(err).Panic("Failed to create server")
	}

	phantoClient := makePhantoClient()

	twirpServer, err := twirpserver.NewTwirpServer(authorizer, auditor, phantoClient)
	if err != nil {
		log.WithError(err).Panic("Failed to create twirp server")
	}

	twirpHandler := phoenix.NewPhoenixServer(twirpServer, nil)

	mux := goji.NewMux()
	mux.HandleFunc(pat.Get("/hello"), server.Hello)
	mux.HandleFunc(pat.Get("/"), server.Root)
	mux.Handle(pat.Post(phoenix.PhoenixPathPrefix+"*"), twirpHandler)
	mux.HandleFunc(pat.Get("/handle_guardian_redirect"), GuardianCallbackHandler)
	mux.Use(logging)
	mux.Use(middleware.GuardianHandler)

	log.Info("service starting on 8080")
	http.ListenAndServe("localhost:8080", mux)
}

func logging(h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		log.Infof("Received request: %v\n", r.URL)
		h.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}

func makePhantoClient() phanto.Phanto {
	s2sConfig := caller.Config{}
	rt, err := caller.NewRoundTripper(phantoStagingS2SName, &s2sConfig, log.New())
	if err != nil {
		log.Panicf("Failed to create s2s round tripper: %v", err)
	}

	return phanto.NewPhantoProtobufClient(phantoStagingURL, &http.Client{
		Transport: rt,
	})
}

func GuardianCallbackHandler(w http.ResponseWriter, r *http.Request) {
	token := asiimov.GetToken(r)
	http.Redirect(w, r, fmt.Sprintf("/?token=%s", token), http.StatusFound)
}
