package server

import (
	"html/template"
	"io"
	"net/http"

	"code.justin.tv/commerce/logrus"
	"code.justin.tv/commerce/phoenix/backend/authorization"
	"code.justin.tv/commerce/phoenix/backend/middleware"
	asiimov "code.justin.tv/systems/guardian/middleware"
)

type Environment string

const (
	Production  = Environment("production")
	Staging     = Environment("staging")
	Development = Environment("development")
)

type Server interface {
	Hello(w http.ResponseWriter, r *http.Request)
	Root(w http.ResponseWriter, r *http.Request)
}

type serverImpl struct {
	env Environment

	templates *template.Template

	authorizer authorization.Authorizer
}

type User struct {
	Token        string
	Name         string
	AllowedPaths []string
}

func NewServer(env Environment, authorizer authorization.Authorizer) (Server, error) {
	templates, err := template.ParseFiles("./backend/templates/root.tmpl")
	if err != nil {
		logrus.WithError(err).Error("Failed to parse templates")
		return nil, err
	}

	return &serverImpl{
		env:        env,
		templates:  templates,
		authorizer: authorizer,
	}, nil
}

func (b *serverImpl) Hello(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	_, err := io.WriteString(w, "Hello back")
	if err != nil {
		logrus.WithError(err).Error("Error writing http response")
	}
}

func (b *serverImpl) Root(w http.ResponseWriter, r *http.Request) {
	user, err := middleware.GetGuardianUser(r.Context())
	if user == nil || err != nil {
		logrus.WithError(err).Error("error parsing guardian user")
		return
	}

	logrus.Debugf("Welcome %s", user.CN)
	token := asiimov.GetToken(r)

	authPaths := b.authorizer.GetAllowedPaths(user)
	allowedPaths := make([]string, 0, len(authPaths))
	for _, path := range authPaths {
		allowedPaths = append(allowedPaths, string(path))
	}

	t := b.templates.Lookup("root.tmpl")
	err = t.Execute(w, &User{
		Token:        token,
		Name:         user.CN,
		AllowedPaths: allowedPaths,
	})

	if err != nil {
		logrus.WithError(err).Error("Error executing template")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
