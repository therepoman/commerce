package audit

import (
	"context"
	"encoding/json"
	"fmt"
	"errors"

	"code.justin.tv/commerce/phoenix/backend/middleware"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/gofrs/uuid"
	"time"
)

const (
	tableNamePrefix = "audit-trail"
)

type Audit struct {
	AuditID string `dynamodbav:"audit_id"`
	CreatedAt time.Time `dynamodbav:"created_at"`
	LDAPUser string `dynamodbav:"ldap_user"`
	Payload string `dynamodbav:"payload"`
	Resource string `dynamodbav:"resource"`
}

type Auditor interface {
	Record(ctx context.Context, name string, payload interface{}) error
}

type auditor struct {
	*dynamodb.DynamoDB
	tableName string
}

func NewAuditor(sess *session.Session) Auditor {
	db := dynamodb.New(sess)
	tableName := fmt.Sprintf("%s-%s", tableNamePrefix, "staging")

	return &auditor{
		DynamoDB:  db,
		tableName: tableName,
	}
}

func (a *auditor) Record(ctx context.Context, name string, payload interface{}) error {
	jsonPayload, err := json.Marshal(payload)

	user, err := middleware.GetGuardianUser(ctx)

	if err != nil {
		return err
	}

	if user == nil {
		return errors.New("could not identify user")
	}

	if err != nil {
		return err
	}

	id, err := uuid.NewV4()

	if err != nil {
		return err
	}

	template := &Audit{
		AuditID:  id.String(),
		CreatedAt: time.Now(),
		Payload: string(jsonPayload),
		Resource: name,
		LDAPUser: user.CN,
	}

	dynamoConditionTemplate, err := dynamodbattribute.MarshalMap(template)

	if err != nil {
		return err
	}

	_, err = a.PutItem(&dynamodb.PutItemInput{
		TableName: aws.String(a.tableName),
		Item:      dynamoConditionTemplate,
	})

	if err != nil {
		return err
	}

	return nil
}