const path = require("path");

const DEV_SERVER_PORT = 3000;

module.exports = {
    entry: path.join(__dirname, "./frontend/main.tsx"),
    resolve: {
        extensions: [".js", ".jsx", ".ts", ".tsx"]
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'babel-loader',
            },
            {
                test: /\.js$/,
                use: ["source-map-loader"],
                enforce: "pre"
            },
            {
                test: /\.s?css$/,
                use: ["style-loader", "css-loader", "sass-loader"]
            }
        ]
    },
    devServer: {
        compress: true,
        port: DEV_SERVER_PORT
    }
};